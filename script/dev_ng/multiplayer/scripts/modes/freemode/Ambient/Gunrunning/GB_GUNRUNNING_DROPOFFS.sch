USING "globals.sch"
USING "net_include.sch"
USING "net_freemode_delivery_common.sch"


FUNC BOOL GUNRUNNING_DROPOFF_IS_VALID(FREEMODE_DELIVERY_DROPOFFS &eDropoffID)
	RETURN ( (ENUM_TO_INT(eDropoffID) >= ENUM_TO_INT(GUNRUNNING_DROPOFF_BUNKER_1)) AND (ENUM_TO_INT(eDropoffID) <= ENUM_TO_INT(GUNRUNNING_DROPOFF_PHANTOM_CITY_10)) )
ENDFUNC

FUNC GUNRUN_VARIATION GUNRUNNING_GET_MISSION_VARIATION_FROM_DROPOFF(FREEMODE_DELIVERY_DROPOFFS &eDropoffID)
	IF GUNRUNNING_DROPOFF_IS_VALID(eDropoffID)
		IF  ( (ENUM_TO_INT(eDropoffID) >= ENUM_TO_INT(GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_1)) AND (ENUM_TO_INT(eDropoffID) <= ENUM_TO_INT(GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_30)) )
			RETURN GRV_SELL_MOVE_WEAPONS
		ENDIF
		
		IF  ( (ENUM_TO_INT(eDropoffID) >= ENUM_TO_INT(GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_1)) AND (ENUM_TO_INT(eDropoffID) <= ENUM_TO_INT(GUNRUNNING_DROPOFF_FOLLOW_LEADER_CITY_10)) )
			RETURN GRV_SELL_FOLLOW_THE_LEADER
		ENDIF
		
		IF  ( (ENUM_TO_INT(eDropoffID) >= ENUM_TO_INT(GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_1)) AND (ENUM_TO_INT(eDropoffID) <= ENUM_TO_INT(GUNRUNNING_DROPOFF_AMBUSHED_CITY_30)) )
			RETURN GRV_SELL_AMBUSHED
		ENDIF
		
		IF  ( (ENUM_TO_INT(eDropoffID) >= ENUM_TO_INT(GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_1)) AND (ENUM_TO_INT(eDropoffID) <= ENUM_TO_INT(GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_50)) )
			RETURN GRV_SELL_HILL_CLIMB
		ENDIF
		
		IF  ( (ENUM_TO_INT(eDropoffID) >= ENUM_TO_INT(GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_1)) AND (ENUM_TO_INT(eDropoffID) <= ENUM_TO_INT(GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_50)) )
			RETURN GRV_SELL_ROUGH_TERRAIN
		ENDIF
		
		IF  ( (ENUM_TO_INT(eDropoffID) >= ENUM_TO_INT(GUNRUNNING_DROPOFF_PHANTOM_COUNTRY_1)) AND (ENUM_TO_INT(eDropoffID) <= ENUM_TO_INT(GUNRUNNING_DROPOFF_PHANTOM_CITY_10)) )
			RETURN GRV_SELL_PHANTOM
		ENDIF
	ENDIF
	
	RETURN GRV_INVALID
ENDFUNC

FUNC FACTORY_ID GET_GUNRUNNING_DROPOFF_FACTORY_ID(FREEMODE_DELIVERY_DROPOFFS eDropoffID)
	RETURN GET_FACTORY_ID_FROM_SIMPLE_INTERIOR_ID(GET_SIMPLE_INTERIOR_FROM_GUNRUNNING_DROPOFF(eDropoffID))
ENDFUNC

FUNC BOOL GUNRUNNING_DROPOFF_SHOULD_RUN_DELIVERY_SCRIPT(FREEMODE_DELIVERY_DROPOFFS eDropoffID)
	IF GUNRUNNING_GET_MISSION_VARIATION_FROM_DROPOFF(eDropoffID) = GRV_SELL_AMBUSHED
	OR GUNRUNNING_GET_MISSION_VARIATION_FROM_DROPOFF(eDropoffID) = GRV_SELL_FOLLOW_THE_LEADER
	OR GUNRUNNING_GET_MISSION_VARIATION_FROM_DROPOFF(eDropoffID) = GRV_SELL_ROUGH_TERRAIN
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC VECTOR GUNRUNNING_DROPOFF_GET_LOCATE_SIZE_MULTIPLIER(GUNRUN_VARIATION eMissionVar)
	VECTOR vReturn
		
		SWITCH eMissionVar
			CASE GRV_STEAL_MINIGUNS			vReturn = <<1.0, 1.0, 3.0>>		BREAK
			CASE GRV_SELL_AMBUSHED			vReturn = <<2.5, 2.5, 4.0>>		BREAK
			CASE GRV_SELL_PHANTOM			vReturn = <<2.0, 2.0, 4.0>>		BREAK
			CASE GRV_SELL_FOLLOW_THE_LEADER	vReturn = <<2.2, 2.2, 4.0>>		BREAK
			CASE GRV_SELL_MOVE_WEAPONS		vReturn = <<0.75, 0.75, 4.0>>	BREAK
			CASE GRV_SELL_ROUGH_TERRAIN		vReturn = <<2.0, 2.0, 4.0>>		BREAK
			CASE GRV_SELL_HILL_CLIMB		vReturn = <<2.0, 2.0, 4.0>>		BREAK
			
			DEFAULT
				vReturn = <<1.0, 1.0, 1.0>>
			BREAK
		ENDSWITCH
		
	RETURN vReturn
ENDFUNC

PROC GUNRUNNING_DROPOFF_WILL_ACCEPT_TYPE_OF_DELIVERY(FREEMODE_DELIVERY_DROPOFFS eDropoffID, FREEMODE_DELIVERABLE_TYPE eType, BOOL &bWillAcceptOnFoot, BOOL &bWillAcceptInCar)
	UNUSED_PARAMETER(eDropoffID)
	IF eType = GUNRUNNING_DELIVERABLE_VEHICLE
		bWillAcceptOnFoot = FALSE
		bWillAcceptInCar = TRUE
	ELIF eType = GUNRUNNING_DELIVERABLE_CRATE
	OR eType = GUNRUNNING_DELIVERABLE_WEAPON
		bWillAcceptOnFoot = TRUE
		bWillAcceptInCar = TRUE
	ELIF eType = FMC_DELIVERABLE
		bWillAcceptOnFoot = TRUE
		bWillAcceptInCar = TRUE
	ENDIF
ENDPROC

FUNC INT GUNRUNNING_GET_MAX_DROPOFFS_ON_ROUTE()	
	RETURN FREEMODE_DELIVERY_MAX_ACTIVE_GUNRUN_DROPOFFS
ENDFUNC

/// PURPOSE:
///    Finds the next freemode delivery dropoff in a delivery routes sequence
/// PARAMS:
///    eRoute - The current FREEMODE_DELIVERY_ROUTES in use
///    iDropoff - The next dropoff to find. Expects 1 - Max dropoffs for the given route
FUNC FREEMODE_DELIVERY_DROPOFFS GUNRUNNING_GET_DROPOFF_ON_ROUTE(FREEMODE_DELIVERY_ROUTES eRoute, INT iDropoff)
	
	FREEMODE_DELIVERY_DROPOFFS eDropoff = FREEMODE_DELIVERY_DROPOFF_INVALID
	INT iSelectedDropoff
	
	IF iDropoff > GUNRUNNING_GET_MAX_DROPOFFS_ON_ROUTE()
		CASSERTLN(DEBUG_AMBIENT, "GUNRUNNING_GET_DROPOFF_ON_ROUTE invalid dropoff requested: ", iDropoff)
		RETURN eDropoff
	ENDIF
	
	//CDEBUG1LN(DEBUG_AMBIENT, "[FM_DO_RES] GUNRUNNING_GET_DROPOFF_ON_ROUTE looking for dropoff: ", iDropoff, " on route: ", eRoute)
	
	SWITCH eRoute
		///////////////////////////////////
		///      Ambushed Routes
		CASE FREEMODE_DELIVERY_ROUTE_AMBUSHED_CITY_1
		CASE FREEMODE_DELIVERY_ROUTE_AMBUSHED_CITY_2
		CASE FREEMODE_DELIVERY_ROUTE_AMBUSHED_CITY_3
		CASE FREEMODE_DELIVERY_ROUTE_AMBUSHED_CITY_4
		CASE FREEMODE_DELIVERY_ROUTE_AMBUSHED_CITY_5
		CASE FREEMODE_DELIVERY_ROUTE_AMBUSHED_CITY_6
			iSelectedDropoff = ((ENUM_TO_INT(eRoute) - ENUM_TO_INT(FREEMODE_DELIVERY_ROUTE_AMBUSHED_CITY_1)) + 1)
			eDropoff = GUNRUNNING_DROPOFF_AMBUSHED_CITY_1
		BREAK
		
		CASE FREEMODE_DELIVERY_ROUTE_AMBUSHED_COUNTRY_1
		CASE FREEMODE_DELIVERY_ROUTE_AMBUSHED_COUNTRY_2
		CASE FREEMODE_DELIVERY_ROUTE_AMBUSHED_COUNTRY_3
		CASE FREEMODE_DELIVERY_ROUTE_AMBUSHED_COUNTRY_4
		CASE FREEMODE_DELIVERY_ROUTE_AMBUSHED_COUNTRY_5
		CASE FREEMODE_DELIVERY_ROUTE_AMBUSHED_COUNTRY_6
		CASE FREEMODE_DELIVERY_ROUTE_AMBUSHED_COUNTRY_7
			iSelectedDropoff = (ENUM_TO_INT(eRoute) - ENUM_TO_INT(FREEMODE_DELIVERY_ROUTE_AMBUSHED_COUNTRY_1) + 1)
			eDropoff = GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_1
		BREAK
		
		///////////////////////////////////
		///      Hill climb Routes
		CASE FREEMODE_DELIVERY_ROUTE_HILL_CLIMB_CITY_1
		CASE FREEMODE_DELIVERY_ROUTE_HILL_CLIMB_CITY_2
		CASE FREEMODE_DELIVERY_ROUTE_HILL_CLIMB_CITY_3
		CASE FREEMODE_DELIVERY_ROUTE_HILL_CLIMB_CITY_4
		CASE FREEMODE_DELIVERY_ROUTE_HILL_CLIMB_CITY_5
		CASE FREEMODE_DELIVERY_ROUTE_HILL_CLIMB_CITY_6
		CASE FREEMODE_DELIVERY_ROUTE_HILL_CLIMB_CITY_7
		CASE FREEMODE_DELIVERY_ROUTE_HILL_CLIMB_CITY_8
		CASE FREEMODE_DELIVERY_ROUTE_HILL_CLIMB_CITY_9
		CASE FREEMODE_DELIVERY_ROUTE_HILL_CLIMB_CITY_10
			iSelectedDropoff = (ENUM_TO_INT(eRoute) - ENUM_TO_INT(FREEMODE_DELIVERY_ROUTE_HILL_CLIMB_CITY_1) + 1)
			eDropoff = GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_1
		BREAK
		
		CASE FREEMODE_DELIVERY_ROUTE_HILL_CLIMB_COUNTRY_1
		CASE FREEMODE_DELIVERY_ROUTE_HILL_CLIMB_COUNTRY_2
		CASE FREEMODE_DELIVERY_ROUTE_HILL_CLIMB_COUNTRY_3
		CASE FREEMODE_DELIVERY_ROUTE_HILL_CLIMB_COUNTRY_4
		CASE FREEMODE_DELIVERY_ROUTE_HILL_CLIMB_COUNTRY_5
		CASE FREEMODE_DELIVERY_ROUTE_HILL_CLIMB_COUNTRY_6
		CASE FREEMODE_DELIVERY_ROUTE_HILL_CLIMB_COUNTRY_7
		CASE FREEMODE_DELIVERY_ROUTE_HILL_CLIMB_COUNTRY_8
		CASE FREEMODE_DELIVERY_ROUTE_HILL_CLIMB_COUNTRY_9
		CASE FREEMODE_DELIVERY_ROUTE_HILL_CLIMB_COUNTRY_10
			iSelectedDropoff = (ENUM_TO_INT(eRoute) - ENUM_TO_INT(FREEMODE_DELIVERY_ROUTE_HILL_CLIMB_COUNTRY_1) + 1)
			eDropoff = GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_1
		BREAK
		
		///////////////////////////////////
		///      Rough terrain Routes
		CASE FREEMODE_DELIVERY_ROUTE_ROUGH_TERRAIN_CITY_1
		CASE FREEMODE_DELIVERY_ROUTE_ROUGH_TERRAIN_CITY_2
		CASE FREEMODE_DELIVERY_ROUTE_ROUGH_TERRAIN_CITY_3
		CASE FREEMODE_DELIVERY_ROUTE_ROUGH_TERRAIN_CITY_4
		CASE FREEMODE_DELIVERY_ROUTE_ROUGH_TERRAIN_CITY_5
		CASE FREEMODE_DELIVERY_ROUTE_ROUGH_TERRAIN_CITY_6
		CASE FREEMODE_DELIVERY_ROUTE_ROUGH_TERRAIN_CITY_7
		CASE FREEMODE_DELIVERY_ROUTE_ROUGH_TERRAIN_CITY_8
		CASE FREEMODE_DELIVERY_ROUTE_ROUGH_TERRAIN_CITY_9
		CASE FREEMODE_DELIVERY_ROUTE_ROUGH_TERRAIN_CITY_10
			iSelectedDropoff = (ENUM_TO_INT(eRoute) - ENUM_TO_INT(FREEMODE_DELIVERY_ROUTE_ROUGH_TERRAIN_CITY_1) + 1)
			eDropoff = GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_1
		BREAK
		
		CASE FREEMODE_DELIVERY_ROUTE_ROUGH_TERRAIN_COUNTRY_1
		CASE FREEMODE_DELIVERY_ROUTE_ROUGH_TERRAIN_COUNTRY_2
		CASE FREEMODE_DELIVERY_ROUTE_ROUGH_TERRAIN_COUNTRY_3
		CASE FREEMODE_DELIVERY_ROUTE_ROUGH_TERRAIN_COUNTRY_4
		CASE FREEMODE_DELIVERY_ROUTE_ROUGH_TERRAIN_COUNTRY_5
		CASE FREEMODE_DELIVERY_ROUTE_ROUGH_TERRAIN_COUNTRY_6
		CASE FREEMODE_DELIVERY_ROUTE_ROUGH_TERRAIN_COUNTRY_7
		CASE FREEMODE_DELIVERY_ROUTE_ROUGH_TERRAIN_COUNTRY_8
		CASE FREEMODE_DELIVERY_ROUTE_ROUGH_TERRAIN_COUNTRY_9
		CASE FREEMODE_DELIVERY_ROUTE_ROUGH_TERRAIN_COUNTRY_10
			iSelectedDropoff = (ENUM_TO_INT(eRoute) - ENUM_TO_INT(FREEMODE_DELIVERY_ROUTE_ROUGH_TERRAIN_COUNTRY_1) + 1)
			eDropoff = GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_1
		BREAK
		
	ENDSWITCH
	
	//Find the last dropoff on the given route
	iSelectedDropoff *= GUNRUNNING_GET_MAX_DROPOFFS_ON_ROUTE()
	//Work back to the first dropoff on that route
	iSelectedDropoff -= GUNRUNNING_GET_MAX_DROPOFFS_ON_ROUTE()
	//Add the calculated value to the first dropoff of route 1
	iSelectedDropoff += ENUM_TO_INT(eDropoff)
	//Work out the correct first dropoff from given route
	iSelectedDropoff += (iDropoff - 1)
	//Assign the dropoff ID
	eDropoff = INT_TO_ENUM(FREEMODE_DELIVERY_DROPOFFS, iSelectedDropoff)	
	
//	CDEBUG1LN(DEBUG_AMBIENT, "[FM_DO_RES] GUNRUNNING_GET_DROPOFF_ON_ROUTE returning ", FREEMODE_DELIVERY_GET_DROPOFF_DEBUG_NAME(eDropoff), " iSelectedDropoff = ", iSelectedDropoff)
	
	RETURN eDropoff
ENDFUNC

FUNC VECTOR GUNRUNNING_GET_DROPOFF_LOCATE_SIZE(FREEMODE_DELIVERY_DROPOFFS eDropoffID, BOOL bOnFoot)

	VECTOR vReturnVector = <<0.0, 0.0, 0.0>>
	
	IF GUNNRUNNING_DROPOFF_IS_PLAYER_BUNKER(eDropoffID)
		IF bOnFoot
			vReturnVector = <<2.0, 2.0, 2.0>>
		ELSE
			vReturnVector = <<10.0, 10.0, 2.5>>
		ENDIF
	ELSE
		SWITCH GUNRUNNING_GET_MISSION_VARIATION_FROM_DROPOFF(eDropoffID)
			CASE GRV_STEAL_MINIGUNS
				vReturnVector = <<2.0, 2.0, 3.0>>
			BREAK
			CASE GRV_SELL_AMBUSHED
				vReturnVector = <<20.0, 20.0, 2.5>>
			BREAK
			CASE GRV_SELL_PHANTOM
				vReturnVector = <<20.0, 20.0, 2.5>>
			BREAK
				
			DEFAULT
				IF bOnFoot
					vReturnVector = <<2.0, 2.0, 2.0>>
				ELSE
					vReturnVector = <<10.0, 10.0, 2.5>>
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	
	RETURN vReturnVector
ENDFUNC

PROC GUNRUNNING_GET_MOVE_WEAPONS_DROPOFF_COORDS(FREEMODE_DELIVERY_DROPOFFS eDropoffID, FREEMODE_DELIVERY_DROPOFF_DETAILS &sDetails)
	SWITCH eDropoffID
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_1
			sDetails.vInCarCorona = <<17.2985, 6579.4985, 31.3584>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_2
			sDetails.vInCarCorona = <<408.3282, 6611.3882, 26.8760>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_3
			sDetails.vInCarCorona = <<21.6079, 6516.4932, 30.4948>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_4
			sDetails.vInCarCorona = <<-206.0430, 6455.7974, 30.1761>>	//<<-201.4071, 6462.3398, 30.1111>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_5
			sDetails.vInCarCorona = <<-225.5175, 6436.7056, 30.1974>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_6
			sDetails.vInCarCorona = <<-128.7701, 6342.2056, 30.4904>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_7
			sDetails.vInCarCorona = <<-353.8975, 6334.5972, 28.8339>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_8
			sDetails.vInCarCorona = <<-315.1533, 6312.4731, 31.3387>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_9
			sDetails.vInCarCorona = <<-433.5699, 6260.3721, 29.2542>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_10
			sDetails.vInCarCorona = <<-258.5444, 6223.6313, 30.4892>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_11
			sDetails.vInCarCorona = <<-274.8564, 6170.6479, 30.4898>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_12
			sDetails.vInCarCorona = <<-441.0088, 6144.6602, 30.4783>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_13
			sDetails.vInCarCorona = <<2244.2954, 5175.0464, 59.1204>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_14
			sDetails.vInCarCorona = <<2011.6484, 4969.7271, 40.5599>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_15
			sDetails.vInCarCorona = <<2309.9001, 4753.1064, 36.0693>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_16
			sDetails.vInCarCorona = <<-1351.0676, 4844.3882, 136.8821>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_17
			sDetails.vInCarCorona = <<1679.1039, 4653.3701, 42.3712>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_18
			sDetails.vInCarCorona = <<1712.9250, 4747.4141, 40.9356>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_19
			sDetails.vInCarCorona = <<2936.5869, 4487.7544, 46.8833>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_20
			sDetails.vInCarCorona = <<1881.4448, 3761.1494, 31.9154>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_21
			sDetails.vInCarCorona = <<1891.1360, 3711.4502, 31.8388>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_22
			sDetails.vInCarCorona = <<923.1298, 3646.5371, 31.5969>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_23
			sDetails.vInCarCorona = <<1360.1233, 3599.0972, 33.8908>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_24
			sDetails.vInCarCorona = <<436.2162, 3550.2480, 32.2386>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_25
			sDetails.vInCarCorona = <<-2235.7813, 3475.7849, 29.33>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_26
			sDetails.vInCarCorona = <<1206.7217, 2716.4758, 37.0042>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_27
			sDetails.vInCarCorona = <<994.0612, 2656.8391, 39.1282>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_28
			sDetails.vInCarCorona = <<2864.1184, 1473.1959, 23.5583>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_29
			sDetails.vInCarCorona = <<1775.0745, 3652.8435, 33.3731>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_30
			sDetails.vInCarCorona = <<-258.8979, 6251.8750, 30.4892>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_1
			sDetails.vInCarCorona = <<-2001.6438, 454.9861, 101.4866>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_2
			sDetails.vInCarCorona = <<-181.7756, 319.9248, 96.7999>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_3
			sDetails.vInCarCorona = <<-152.1834, 201.6567, 89.9091>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_4
			sDetails.vInCarCorona = <<-1935.1152, 183.2173, 83.5954>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_5
			sDetails.vInCarCorona = <<351.9804, 21.8583, 84.4991>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_6
			sDetails.vInCarCorona = <<195.2451, -157.1152, 55.6824>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_7
			sDetails.vInCarCorona = <<-1490.2102, -203.8835, 49.3974>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_8
			sDetails.vInCarCorona = <<941.5977, -247.444, 67.5999>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_9
			sDetails.vInCarCorona = <<-531.7662, -43.3292, 41.4131>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_10
			sDetails.vInCarCorona = <<-1308.9706, -410.3486, 33.557>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_11
			sDetails.vInCarCorona = <<914.3543, -489.5208, 58.0344>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_12
			sDetails.vInCarCorona = <<1242.4198, -578.4505, 68.4043>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_13
			sDetails.vInCarCorona = <<-1273.1748, -607.2695, 25.484>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_14
			sDetails.vInCarCorona = <<322.7755, -683.6807, 28.3018>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_15
			sDetails.vInCarCorona = <<916.0450, -641.1888, 56.8682>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_16
			sDetails.vInCarCorona = <<-1360.5935, -780.0303, 18.7525>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_17
			sDetails.vInCarCorona = <<-617.1512, -784.4778, 24.1344>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_18
			sDetails.vInCarCorona = <<-1364.3766, -898.3317, 11.4705>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_19
			sDetails.vInCarCorona = <<-1440.9740, -871.7243, 9.9175>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_20
			sDetails.vInCarCorona = <<450.0986, -934.5388, 27.58>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_21
			sDetails.vInCarCorona = <<1135.1428, -974.3087, 45.5844>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_22
			sDetails.vInCarCorona = <<-1327.618, -1027.4882, 6.7181>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_23
			sDetails.vInCarCorona = <<-600.9597, -1091.9869, 21.1781>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_24
			sDetails.vInCarCorona = <<-1248.9954, -1211.9359, 6.1084>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_25
			sDetails.vInCarCorona = <<-1097.9259, -1505.1464, 3.6901>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_26
			sDetails.vInCarCorona = <<1318.1313, -1663.0138, 50.2364>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_27
			sDetails.vInCarCorona = <<267.6798, -1721.8668, 28.2842>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_28
			sDetails.vInCarCorona = <<954.9901, -2004.1172, 29.2387>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_29
			sDetails.vInCarCorona = <<1003.8367, -2364.2512, 29.5096>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_30
			sDetails.vInCarCorona = <<-694.7249, -2453.9180, 12.8718>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
	ENDSWITCH
ENDPROC

PROC GUNRUNNING_GET_PHANTOM_DROPOFF_COORDS(FREEMODE_DELIVERY_DROPOFFS eDropoffID, FREEMODE_DELIVERY_DROPOFF_DETAILS &sDetails)
	SWITCH eDropoffID
		CASE GUNRUNNING_DROPOFF_PHANTOM_CITY_1
			sDetails.vInCarCorona = <<-725.8421, -2213.1409, 5.0030>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_PHANTOM_CITY_2
			sDetails.vInCarCorona = <<-315.8740, -2552.0081, 5.0006>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_PHANTOM_CITY_3
			sDetails.vInCarCorona = <<144.4716, -2807.3489, 5.0002>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_PHANTOM_CITY_4
			sDetails.vInCarCorona = <<1142.3071, -3277.4636, 4.9007>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_PHANTOM_CITY_5
			sDetails.vInCarCorona = <<791.4049, -2482.0208, 19.9127>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_PHANTOM_CITY_6
			sDetails.vInCarCorona = <<-34.7159, -2418.3035, 5.0000>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_PHANTOM_CITY_7
			sDetails.vInCarCorona = <<-947.4313, -3531.9360, 13.0788>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			SET_BIT(sDetails.iBS, FREEMODE_DELIVERY_DROPOFF_DETAILS_BS_DROPOFF_IN_AIRPORT)
		BREAK
		CASE GUNRUNNING_DROPOFF_PHANTOM_CITY_8
			sDetails.vInCarCorona = <<1482.0775, -2436.5615, 65.2259>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_PHANTOM_CITY_9
			sDetails.vInCarCorona = <<821.9077, -2937.0647, 4.9042>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_PHANTOM_CITY_10
			sDetails.vInCarCorona = <<-1157.3311, -2885.6250, 12.9456>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			SET_BIT(sDetails.iBS, FREEMODE_DELIVERY_DROPOFF_DETAILS_BS_DROPOFF_IN_AIRPORT)
		BREAK

		CASE GUNRUNNING_DROPOFF_PHANTOM_COUNTRY_1
			sDetails.vInCarCorona = <<-2884.5300, 3492.1609, 8.2728>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_PHANTOM_COUNTRY_2
			sDetails.vInCarCorona = <<1840.3181, 1813.8236, 66.5145>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_PHANTOM_COUNTRY_3
			sDetails.vInCarCorona = <<2074.8792, 4605.4492, 34.5385>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_PHANTOM_COUNTRY_4
			sDetails.vInCarCorona = <<2801.8464, 4749.3896, 45.4179>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_PHANTOM_COUNTRY_5
			sDetails.vInCarCorona = <<2766.3516, 1700.9596, 23.7017>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_PHANTOM_COUNTRY_6
			sDetails.vInCarCorona = <<-1543.1719, 2856.0461, 30.1456>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_PHANTOM_COUNTRY_7
			sDetails.vInCarCorona = <<-3131.0000, 1284.2914, 19.8153>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_PHANTOM_COUNTRY_8
			sDetails.vInCarCorona = <<-564.3720, 6363.4893, 2.2542>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_PHANTOM_COUNTRY_9
			sDetails.vInCarCorona = <<754.9817, 6609.0625, 1.3558>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_PHANTOM_COUNTRY_10
			sDetails.vInCarCorona = <<1026.2738, 4338.4229, 40.0694>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
	ENDSWITCH
ENDPROC

PROC GUNRUNNING_GET_FOLLOW_LEADER_DROPOFF_COORDS(FREEMODE_DELIVERY_DROPOFFS eDropoffID, FREEMODE_DELIVERY_DROPOFF_DETAILS &sDetails)
	SWITCH eDropoffID
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_1
			sDetails.vInCarCorona = <<-227.4169, 2062.3848, 138.4856>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_2
			sDetails.vInCarCorona = <<2772.3220, 2845.9636, 34.8882>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_3
			sDetails.vInCarCorona = <<2989.1829, 3466.0039, 70.3510>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_4
			sDetails.vInCarCorona = <<-2666.3462, 3455.6724, 14.1713>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_5
			sDetails.vInCarCorona = <<1492.6910, 3579.0828, 34.2291>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_6
			sDetails.vInCarCorona = <<-2379.7112, 4278.7549, 9.1551>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_7
			sDetails.vInCarCorona = <<2500.7375, 4294.3721, 38.1973>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_8
			sDetails.vInCarCorona = <<-1848.6257, 4419.5342, 48.1508>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_9
			sDetails.vInCarCorona = <<-1566.0024, 4485.8955, 20.2007>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_10
			sDetails.vInCarCorona = <<-386.5953, 6380.8369, 13.0622>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_CITY_1
			sDetails.vInCarCorona = <<1186.3239, -3241.4265, 5.0288>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_CITY_2
			sDetails.vInCarCorona = <<210.9870, -3127.6196, 4.7903>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_CITY_3
			sDetails.vInCarCorona = <<302.8576, -2857.0398, 5.0245>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_CITY_4
			sDetails.vInCarCorona = <<1635.4027, -2370.9561, 92.9864>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_CITY_5
			sDetails.vInCarCorona = <<853.4637, -2437.8184, 26.9939>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_CITY_6
			sDetails.vInCarCorona = <<-1226.4976, -2393.5740, 12.9452>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			SET_BIT(sDetails.iBS, FREEMODE_DELIVERY_DROPOFF_DETAILS_BS_DROPOFF_IN_AIRPORT)
		BREAK
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_CITY_7
			sDetails.vInCarCorona = <<900.6783, -1736.1044, 29.4254>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_CITY_8
			sDetails.vInCarCorona = <<1737.7461, -1534.7815, 111.6664>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_CITY_9
			sDetails.vInCarCorona = <<485.9290, -1394.4803, 28.2951>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_CITY_10
			sDetails.vInCarCorona = <<-146.8984, -1347.3796, 28.6772>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		BREAK
	ENDSWITCH
ENDPROC

PROC GUNRUNNING_GET_AMBUSHED_DROPOFF_COORDS(FREEMODE_DELIVERY_DROPOFFS eDropoffID, FREEMODE_DELIVERY_DROPOFF_DETAILS &sDetails)
	SWITCH eDropoffID
		CASE GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_1
			sDetails.vInCarCorona = <<2875.4678, 2806.3674, 53.7213>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_2
			sDetails.vInCarCorona = <<1052.2510, 2670.0659, 38.5510>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_3
			sDetails.vInCarCorona = <<-833.7290, 1714.5830, 191.5770>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_4
			sDetails.vInCarCorona = <<-2653.6760, 2422.1609, 3.9250>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_5
			sDetails.vInCarCorona = <<-3048.3418, 602.2847, 6.2691>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_6
			sDetails.vInCarCorona = <<1068.7200, 3588.0339, 30.6020>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_7
			sDetails.vInCarCorona = <<138.1180, 3119.2629, 41.6360>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_8
			sDetails.vInCarCorona = <<-1285.8090, 2593.4087, 11.3524>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_9
			sDetails.vInCarCorona = <<-2417.6721, 4110.0820, 17.9340>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_10
			sDetails.vInCarCorona = <<-292.0090, 6130.3320, 30.5010>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_11
			sDetails.vInCarCorona = <<-140.7524, 6356.9937, 30.4906>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_12
			sDetails.vInCarCorona = <<1449.2180, 6579.6899, 11.8940>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_13
			sDetails.vInCarCorona = <<2399.6760, 4911.9150, 41.4640>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_14
			sDetails.vInCarCorona = <<1706.0760, 3872.9080, 33.9040>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_15
			sDetails.vInCarCorona = <<435.2710, 3522.7959, 32.6260>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_16
			sDetails.vInCarCorona = <<1187.5710, 6561.2271, 1.9970>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_17
			sDetails.vInCarCorona = <<2486.7720, 4816.7578, 33.7680>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_18
			sDetails.vInCarCorona = <<1488.5470, 3701.1680, 33.3820>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_19
			sDetails.vInCarCorona = <<1522.5540, 1669.5120, 109.6980>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_20
			sDetails.vInCarCorona = <<2309.1799, 2518.3521, 45.6670>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_21
			sDetails.vInCarCorona = <<1095.3149, 2109.9319, 52.4200>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK				
		
		CASE GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_22
			sDetails.vInCarCorona = <<1376.6801, 3599.5110, 33.8810>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_23
			sDetails.vInCarCorona = <<2952.8540, 4253.1909, 52.7680>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_24
			sDetails.vInCarCorona = <<2263.0061, 5612.5132, 53.8170>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_25
			sDetails.vInCarCorona = <<1465.1860, 6356.0000, 22.8360>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_26
			sDetails.vInCarCorona = <<1929.9790, 4642.6050, 39.4440>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_27
			sDetails.vInCarCorona = <<1803.6591, 3731.6531, 32.8190>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_28
			sDetails.vInCarCorona = <<1034.9360, 2514.9380, 45.8280>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_29
			sDetails.vInCarCorona = <<1215.7310, 1819.2760, 77.9570>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_30
			sDetails.vInCarCorona = <<2700.9180, 1559.1110, 23.5270>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_31
			sDetails.vInCarCorona = <<186.9150, 3056.9209, 41.9590>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_32
			sDetails.vInCarCorona = <<-124.3542, 1883.4944, 196.7805>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_33
			sDetails.vInCarCorona = <<1433.6195, 1182.6066, 113.1940>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_34
			sDetails.vInCarCorona = <<2077.1995, 2340.7576, 93.3144>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_35
			sDetails.vInCarCorona = <<2869.9194, 4711.4854, 47.8156>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_AMBUSHED_CITY_1
			sDetails.vInCarCorona = <<-3196.8169, 962.4005, 17.1183>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_AMBUSHED_CITY_2
			sDetails.vInCarCorona = <<-3043.2593, 133.9678, 10.6028>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_AMBUSHED_CITY_3
			sDetails.vInCarCorona = <<-1657.0238, -224.0322, 54.0132>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_AMBUSHED_CITY_4
			sDetails.vInCarCorona = <<-1065.0024, -1443.4788, 4.4254>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_AMBUSHED_CITY_5
			sDetails.vInCarCorona = <<37.7520, -1608.5480, 28.3570>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_AMBUSHED_CITY_6
			sDetails.vInCarCorona = <<-1174.6304, -735.4771, 19.2095>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_AMBUSHED_CITY_7
			sDetails.vInCarCorona = <<-1336.9680, -1297.6281, 3.8380>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_AMBUSHED_CITY_8
			sDetails.vInCarCorona = <<372.9310, -2132.6140, 15.2837>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_AMBUSHED_CITY_9
			sDetails.vInCarCorona = <<164.8930, -981.9520, 29.0920>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_AMBUSHED_CITY_10
			sDetails.vInCarCorona = <<-633.3420, -71.9310, 39.4400>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_AMBUSHED_CITY_11
			sDetails.vInCarCorona = <<-3109.5259, 221.0460, 5.8140>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_AMBUSHED_CITY_12
			sDetails.vInCarCorona = <<-1917.5990, 629.5390, 124.0100>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_AMBUSHED_CITY_13
			sDetails.vInCarCorona = <<-1291.7880, -268.3340, 38.2980>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_AMBUSHED_CITY_14
			sDetails.vInCarCorona = <<-97.4760, 91.1440, 70.7770>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_AMBUSHED_CITY_15
			sDetails.vInCarCorona = <<860.4900, -916.9620, 24.9230>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_AMBUSHED_CITY_16
			sDetails.vInCarCorona = <<1359.7130, 1147.2898, 113.3128>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_AMBUSHED_CITY_17
			sDetails.vInCarCorona = <<643.5014, 195.1281, 95.1524>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_AMBUSHED_CITY_18
			sDetails.vInCarCorona = <<477.2894, -1158.1477, 28.2918>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_AMBUSHED_CITY_19
			sDetails.vInCarCorona = <<12.1334, -1742.4001, 28.3029>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_AMBUSHED_CITY_20
			sDetails.vInCarCorona = <<946.0427, -2185.8760, 29.5616>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_AMBUSHED_CITY_21
			sDetails.vInCarCorona = <<-672.3598, -1823.9357, 27.8611>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK				
		
		CASE GUNRUNNING_DROPOFF_AMBUSHED_CITY_22
			sDetails.vInCarCorona = <<18.1799, -2485.0813, 5.0068>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_AMBUSHED_CITY_23
			sDetails.vInCarCorona = <<1096.5426, -3142.2693, 4.9008>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_AMBUSHED_CITY_24
			sDetails.vInCarCorona = <<1129.2649, -2382.2844, 30.2488>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_AMBUSHED_CITY_25
			sDetails.vInCarCorona = <<1735.3330, -1431.0090, 111.6370>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_AMBUSHED_CITY_26
			sDetails.vInCarCorona = <<313.4510, -2022.1349, 19.4894>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_AMBUSHED_CITY_27
			sDetails.vInCarCorona = <<1142.6357, -2511.3953, 32.2916>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_AMBUSHED_CITY_28
			sDetails.vInCarCorona = <<1687.5365, -1583.2422, 111.5227>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_AMBUSHED_CITY_29
			sDetails.vInCarCorona = <<1374.8936, -739.6987, 66.2329>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_AMBUSHED_CITY_30
			sDetails.vInCarCorona = <<1902.0620, 418.8190, 162.1080>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
	ENDSWITCH
	
	IF eDropoffID != GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_5
	AND eDropoffID != GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_10
	AND eDropoffID != GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_15
	AND eDropoffID != GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_20
	AND eDropoffID != GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_25
	AND eDropoffID != GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_30
	AND eDropoffID != GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_35
	AND eDropoffID != GUNRUNNING_DROPOFF_AMBUSHED_CITY_5
	AND eDropoffID != GUNRUNNING_DROPOFF_AMBUSHED_CITY_10
	AND eDropoffID != GUNRUNNING_DROPOFF_AMBUSHED_CITY_15
	AND eDropoffID != GUNRUNNING_DROPOFF_AMBUSHED_CITY_20
	AND eDropoffID != GUNRUNNING_DROPOFF_AMBUSHED_CITY_25
	AND eDropoffID != GUNRUNNING_DROPOFF_AMBUSHED_CITY_30
		SET_BIT(sDetails.iBS, FREEMODE_DELIVERY_DROPOFF_DETAILS_BS_DROPOFF_CONDITION_DPAD_RIGHT)
	ENDIF
ENDPROC

PROC GUNRUNNING_GET_HILL_CLIMB_DROPOFF_COORDS(FREEMODE_DELIVERY_DROPOFFS eDropoffID, FREEMODE_DELIVERY_DROPOFF_DETAILS &sDetails)
	
	IF eDropoffID > GUNRUNNING_DROPOFF_AMBUSHED_CITY_30
	
		SWITCH eDropoffID
					
			//City Route 1
			CASE GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_1
				sDetails.vInCarCorona = <<-2191.6741, 904.8940, 228.6130>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_2
				sDetails.vInCarCorona = <<-2512.5520, 761.6240, 300.4040>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_3
				sDetails.vInCarCorona = <<-2554.8469, 279.2110, 184.8900>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_4
				sDetails.vInCarCorona = <<-2267.6101, -26.6110, 111.3090>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_5
				sDetails.vInCarCorona = <<-1705.9790, -82.0710, 79.0540>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			//City Route 2
			CASE GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_6
				sDetails.vInCarCorona = <<2581.8540, -775.5290, 84.9810>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_7
				sDetails.vInCarCorona = <<2155.9050, -1111.0070, 172.2830>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_8
				sDetails.vInCarCorona = <<2048.3350, -1619.8330, 234.0820>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_9
				sDetails.vInCarCorona = <<1880.6270, -1864.1390, 192.1800>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_10
				sDetails.vInCarCorona = <<1827.1890, -2349.4250, 146.4090>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			//City Route 3
			CASE GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_11
				sDetails.vInCarCorona = <<1893.6801, -885.7460, 117.7940>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_12
				sDetails.vInCarCorona = <<1495.9980, -1275.1130, 121.5340>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_13
				sDetails.vInCarCorona = <<1269.7300, -941.1880, 77.5840>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_14
				sDetails.vInCarCorona = <<1582.1591, -635.6420, 147.7270>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_15
				sDetails.vInCarCorona = <<1841.7930, -237.6300, 293.5300>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			//City Route 4
			CASE GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_16
				sDetails.vInCarCorona = <<2216.0920, 246.3560, 258.9160>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_17
				sDetails.vInCarCorona = <<2205.1550, 591.8840, 261.3700>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_18
				sDetails.vInCarCorona = <<2089.6101, 971.7730, 213.3140>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_19
				sDetails.vInCarCorona = <<1885.1720, 997.5830, 274.8150>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_20
				sDetails.vInCarCorona = <<1830.6450, 1496.9930, 119.0150>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			//City Route 5
			CASE GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_21
				sDetails.vInCarCorona = <<1050.0580, 1020.9090, 249.5950>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_22
				sDetails.vInCarCorona = <<1311.8700, 720.8120, 85.5030>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_23
				sDetails.vInCarCorona = <<1619.1700, 450.9710, 250.3820>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_24
				sDetails.vInCarCorona = <<1589.9160, 95.7610, 209.2720>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_25
				sDetails.vInCarCorona = <<1169.0670, -147.9110, 55.8140>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			//City Route 6
			CASE GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_26
				sDetails.vInCarCorona = <<980.9680, 716.3820, 192.1860>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_27
				sDetails.vInCarCorona = <<822.6540, 1314.4680, 362.3100>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_28
				sDetails.vInCarCorona = <<358.7490, 1186.1090, 266.8350>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_29
				sDetails.vInCarCorona = <<409.1620, 712.2790, 196.5070>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_30
				sDetails.vInCarCorona = <<568.2690, 431.2400, 171.5050>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			//City Route 7
			CASE GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_31
				sDetails.vInCarCorona = <<442.8510, 1711.1040, 258.1000>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_32
				sDetails.vInCarCorona = <<489.2310, 1446.3130, 350.1690>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_33
				sDetails.vInCarCorona = <<-39.0390, 1563.2590, 297.3520>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_34
				sDetails.vInCarCorona = <<-96.7330, 1265.5170, 302.8110>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_35
				sDetails.vInCarCorona = <<186.6770, 1038.2900, 243.2590>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			//City Route 8
			CASE GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_36
				sDetails.vInCarCorona = <<125.4270, 1435.4290, 261.4810>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_37
				sDetails.vInCarCorona = <<-471.3730, 1525.3890, 390.2280>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_38
				sDetails.vInCarCorona = <<-891.6700, 1297.3590, 300.1370>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_39
				sDetails.vInCarCorona = <<-1022.6120, 1641.0400, 258.4380>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_40
				sDetails.vInCarCorona = <<-1363.5300, 1662.0610, 176.5700>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			//City Route 9
			CASE GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_41
				sDetails.vInCarCorona = <<-1135.8101, 1901.4060, 175.3400>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_42
				sDetails.vInCarCorona = <<-1043.9540, 1592.7510, 265.3000>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_43
				sDetails.vInCarCorona = <<-1289.2230, 1127.8970, 280.1620>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_44
				sDetails.vInCarCorona = <<-1547.3090, 724.5210, 202.3980>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_45
				sDetails.vInCarCorona = <<-1766.1700, 613.0990, 179.4570>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			//City Route 10
			CASE GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_46
				sDetails.vInCarCorona = <<-1616.1130, 794.8800, 184.8680>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_47
				sDetails.vInCarCorona = <<-1948.8070, 1081.2900, 256.1940>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_48
				sDetails.vInCarCorona = <<-2354.8201, 1293.1490, 330.9530>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_49
				sDetails.vInCarCorona = <<-1947.7590, 1503.9000, 269.2740>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_50
				sDetails.vInCarCorona = <<-1736.6490, 1771.2321, 197.7020>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
		ENDSWITCH
	ENDIF
	
	//Country routes
	SWITCH eDropoffID
				
		//City Route 1
		CASE GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_1
			sDetails.vInCarCorona = <<2166.9600, 5773.1079, 176.9470>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_2
			sDetails.vInCarCorona = <<1794.6060, 5805.4951, 334.1240>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_3
			sDetails.vInCarCorona = <<1311.7360, 5810.8999, 471.0650>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_4
			sDetails.vInCarCorona = <<803.1150, 5696.3101, 696.7400>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_5
			sDetails.vInCarCorona = <<505.0280, 5524.1299, 775.3360>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		//COUNTRY Route 2
		CASE GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_6
			sDetails.vInCarCorona = <<2678.8770, 2657.5039, 81.4570>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_7
			sDetails.vInCarCorona = <<2950.0720, 2508.0281, 163.3500>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_8
			sDetails.vInCarCorona = <<2802.6699, 2057.8091, 123.4110>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_9
			sDetails.vInCarCorona = <<2383.2319, 1815.5770, 90.8900>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_10
			sDetails.vInCarCorona = <<2119.9792, 1718.6700, 100.1659>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		//COUNTRY Route 3
		CASE GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_11
			sDetails.vInCarCorona = <<2985.5649, 4871.6519, 126.0730>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_12
			sDetails.vInCarCorona = <<3193.9800, 4728.6538, 191.2780>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_13
			sDetails.vInCarCorona = <<3443.9231, 4200.0078, 239.3020>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_14
			sDetails.vInCarCorona = <<3090.4600, 3505.3640, 122.4880>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_15
			sDetails.vInCarCorona = <<2956.2661, 3130.2400, 170.5450>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		//COUNTRY Route 4
		CASE GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_16
			sDetails.vInCarCorona = <<-114.5166, 4366.3047, 66.5739>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_17
			sDetails.vInCarCorona = <<-389.1314, 4714.1729, 262.0202>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_18
			sDetails.vInCarCorona = <<-927.3663, 4830.7769, 308.1317>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_19
			sDetails.vInCarCorona = <<-947.9830, 4616.6699, 238.3430>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_20
			sDetails.vInCarCorona = <<-1193.1351, 4791.7871, 219.3900>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		//COUNTRY Route 5
		CASE GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_21
			sDetails.vInCarCorona = <<-547.3558, 3063.1890, 41.6977>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_22
			sDetails.vInCarCorona = <<-788.8897, 3439.2634, 163.2574>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_23
			sDetails.vInCarCorona = <<-693.7470, 3633.7327, 290.9690>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_24
			sDetails.vInCarCorona = <<-524.5084, 3545.8193, 235.8375>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_25
			sDetails.vInCarCorona = <<-299.4800, 3403.1494, 143.3759>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		//COUNTRY Route 6
		CASE GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_26
			sDetails.vInCarCorona = <<-757.9035, 3107.5188, 87.9309>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_27
			sDetails.vInCarCorona = <<-936.9541, 3343.8281, 214.9900>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_28
			sDetails.vInCarCorona = <<-976.6102, 3827.8462, 428.4182>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_29
			sDetails.vInCarCorona = <<-1172.5184, 3833.2788, 483.9537>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_30
			sDetails.vInCarCorona = <<-1296.5997, 3683.4307, 425.8118>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		//COUNTRY Route 7
		CASE GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_31
			sDetails.vInCarCorona = <<1806.7881, 6509.8589, 69.7009>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_32
			sDetails.vInCarCorona = <<2136.8179, 6427.5757, 153.6458>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_33
			sDetails.vInCarCorona = <<2412.8965, 6475.1431, 72.3587>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_34
			sDetails.vInCarCorona = <<2827.7612, 5967.5063, 350.3510>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_35
			sDetails.vInCarCorona = <<3061.1804, 5557.6099, 195.6185>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		//COUNTRY Route 8
		CASE GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_36
			sDetails.vInCarCorona = <<2626.2576, 3660.5015, 100.4183>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_37
			sDetails.vInCarCorona = <<1727.9740, 3046.8176, 58.9817>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_38
			sDetails.vInCarCorona = <<537.0970, 3362.1672, 98.3086>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_39
			sDetails.vInCarCorona = <<-314.4719, 3789.6050, 67.1712>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_40
			sDetails.vInCarCorona = <<-544.3684, 4198.8013, 191.2337>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		//COUNTRY Route 9
		CASE GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_41
			sDetails.vInCarCorona = <<-1634.4612, 1987.9780, 114.3933>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_42
			sDetails.vInCarCorona = <<-1946.9988, 1558.0131, 268.0190>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_43
			sDetails.vInCarCorona = <<-2367.7488, 1294.9072, 331.8613>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_44
			sDetails.vInCarCorona = <<-2725.2305, 1248.9304, 132.6765>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_45
			sDetails.vInCarCorona = <<-3247.4492, 1246.8092, 1.6992>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		//COUNTRY Route 10
		CASE GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_46
			sDetails.vInCarCorona = <<2487.3506, 5210.5659, 70.7697>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_47
			sDetails.vInCarCorona = <<2153.8425, 5382.0928, 164.5574>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_48
			sDetails.vInCarCorona = <<1663.6813, 5156.7280, 152.0881>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_49
			sDetails.vInCarCorona = <<1398.8035, 5181.0200, 224.9860>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_50
			sDetails.vInCarCorona = <<862.0489, 5172.7749, 456.8228>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
	ENDSWITCH
	
	IF eDropoffID != GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_5
	AND eDropoffID != GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_10
	AND eDropoffID != GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_15
	AND eDropoffID != GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_20
	AND eDropoffID != GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_25
	AND eDropoffID != GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_30
	AND eDropoffID != GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_35
	AND eDropoffID != GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_40
	AND eDropoffID != GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_45
	AND eDropoffID != GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_50
	AND eDropoffID != GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_5
	AND eDropoffID != GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_10
	AND eDropoffID != GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_15
	AND eDropoffID != GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_20
	AND eDropoffID != GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_25
	AND eDropoffID != GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_30
	AND eDropoffID != GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_35
	AND eDropoffID != GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_40
	AND eDropoffID != GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_45
	AND eDropoffID != GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_50
		SET_BIT(sDetails.iBS, FREEMODE_DELIVERY_DROPOFF_DETAILS_BS_DROPOFF_CONDITION_DPAD_RIGHT)
	ENDIF
ENDPROC

PROC GUNRUNNING_GET_ROUGH_TERRAIN_DROPOFF_COORDS(FREEMODE_DELIVERY_DROPOFFS eDropoffID, FREEMODE_DELIVERY_DROPOFF_DETAILS &sDetails)
	
	IF eDropoffID > GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_50
		SWITCH eDropoffID
					
			//City Route 1
			CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_1
				sDetails.vInCarCorona = <<-115.4032, 1416.1228, 294.134>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_2
				sDetails.vInCarCorona = <<156.6951, 703.3265, 207.1029>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_3
				sDetails.vInCarCorona = <<2538.2393, -199.7805, 72.551>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_4
				sDetails.vInCarCorona = <<659.4763, -491.1226, 14.688>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_5
				sDetails.vInCarCorona = <<1411.4135, 1051.7147, 113.3343>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
				sDetails.fInCarCoronaRadius = 15.0
			BREAK
			
			//City Route 2
			CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_6
				sDetails.vInCarCorona = <<-583.498, -1453.7261, 9.2096>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_7
				sDetails.vInCarCorona = <<1582.1805, -2009.4749, 92.7024>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_8
				sDetails.vInCarCorona = <<1563.9694, -2677.0903, 36.8862>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_9
				sDetails.vInCarCorona = <<1110.3373, -3080.7673, 4.8473>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_10
				sDetails.vInCarCorona = <<369.0472, -2431.2852, 5.0417>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
				sDetails.fInCarCoronaRadius = 15.0
			BREAK
			
			//City Route 3
			CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_11
				sDetails.vInCarCorona = <<-758.4178, -2595.9338, 12.8285>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_12
				sDetails.vInCarCorona = <<732.8478, -1518.7463, 18.7068>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_13
				sDetails.vInCarCorona = <<-815.6332, -1819.3187, 27.6752>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_14
				sDetails.vInCarCorona = <<1234.6619, -2689.2505, 1.4614>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_15
				sDetails.vInCarCorona = <<271.8187, -2501.8909, 5.4403>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
				sDetails.fInCarCoronaRadius = 15.0
			BREAK
			
			//City Route 4
			CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_16
				sDetails.vInCarCorona = <<395.6607, 32.3907, 90.4966>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_17
				sDetails.vInCarCorona = <<-1049.0134, -89.9484, 42.5869>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_18
				sDetails.vInCarCorona = <<1345.9629, -1025.3956, 38.9002>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_19
				sDetails.vInCarCorona = <<-1630.3453, -1124.8622, 1.0889>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_20
				sDetails.vInCarCorona = <<5.3001, -1102.7137, 37.152>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
				sDetails.fInCarCoronaRadius = 15.0
			BREAK
			
			//City Route 5
			CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_21
				sDetails.vInCarCorona = <<613.4843, -1108.7843, 9.1824>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_22
				sDetails.vInCarCorona = <<-350.6446, -1188.9392, 21.9375>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_23
				sDetails.vInCarCorona = <<2486.7141, -1989.5208, 66.1648>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_24
				sDetails.vInCarCorona = <<610.8346, -2850.1663, 2.0950>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_25
				sDetails.vInCarCorona = <<508.5753, -2309.0129, 4.9173>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
				sDetails.fInCarCoronaRadius = 15.0
			BREAK
			
			//City Route 6
			CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_26
				sDetails.vInCarCorona = <<865.9128, 831.9396, 167.0572>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_27
				sDetails.vInCarCorona = <<438.3826, 790.9186, 193.4617>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_28
				sDetails.vInCarCorona = <<2174.2297, 129.0715, 227.4601>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_29
				sDetails.vInCarCorona = <<-430.7544, -566.5062, 26.6955>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_30
				sDetails.vInCarCorona = <<820.8608, -487.1843, 29.3492>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
				sDetails.fInCarCoronaRadius = 15.0
			BREAK
			
			//City Route 7
			CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_31
				sDetails.vInCarCorona = <<-927.1212, -754.8340, 18.7713>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_32
				sDetails.vInCarCorona = <<2826.0066, -744.9128, 16.3986>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_33
				sDetails.vInCarCorona = <<-347.0302, -1714.7363, 0.1588>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_34
				sDetails.vInCarCorona = <<1165.3328, -2217.1558, 29.818>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_35
				sDetails.vInCarCorona = <<732.7329, -724.3032, 25.5982>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
				sDetails.fInCarCoronaRadius = 15.0
			BREAK
			
			//City Route 8
			CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_36
				sDetails.vInCarCorona = <<-534.8953, -893.4067, 23.3456>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_37
				sDetails.vInCarCorona = <<-1483.7996, -1478.8672, 1.5746>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_38
				sDetails.vInCarCorona = <<660.1437, -1662.9365, 8.7057>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_39
				sDetails.vInCarCorona = <<756.9075, -2560.2788, 9.1108>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_40
				sDetails.vInCarCorona = <<-688.3148, -2474.5986, 12.8285>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
				sDetails.fInCarCoronaRadius = 15.0
			BREAK
			
			//City Route 9
			CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_41
				sDetails.vInCarCorona = <<-344.8739, 1373.6272, 339.4823>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_42
				sDetails.vInCarCorona = <<679.5253, 1213.0343, 323.3946>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_43
				sDetails.vInCarCorona = <<402.2882, 1033.3969, 236.317>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_44
				sDetails.vInCarCorona = <<1994.4613, 497.7719, 162.3902>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_45
				sDetails.vInCarCorona = <<-71.2453, 184.6311, 86.3855>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
				sDetails.fInCarCoronaRadius = 15.0
			BREAK
			
			//City Route 10
			CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_46
				sDetails.vInCarCorona = <<-423.4632, 865.8475, 233.0571>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_47
				sDetails.vInCarCorona = <<-1040.6815, 918.1884, 168.968>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_48
				sDetails.vInCarCorona = <<-1540.5350, 332.4937, 84.6292>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_49
				sDetails.vInCarCorona = <<-1984.6556, -248.1744, 33.4115>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
			BREAK
			
			CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_50
				sDetails.vInCarCorona = <<-359.8619, -92.1446, 44.6620>>
				sDetails.vOnFootCorona = sDetails.vInCarCorona
				sDetails.fInCarCoronaRadius = 15.0
			BREAK
		ENDSWITCH
	ENDIF
	
	//Country routes
	SWITCH eDropoffID
				
		//City Route 1
		CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_1
			sDetails.vInCarCorona = <<2347.5264, 5563.6797, 38.5584>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_2
			sDetails.vInCarCorona = <<2732.7122, 5221.7402, 50.1412>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_3
			sDetails.vInCarCorona = <<-203.4516, 4426.9082, 45.1766>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_4
			sDetails.vInCarCorona = <<1402.9407, 2107.9316, 104.4429>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_5
			sDetails.vInCarCorona = <<1913.3085, 3726.8713, 31.4625>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.fInCarCoronaRadius = 15.0
		BREAK
		
		//COUNTRY Route 2
		CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_6
			sDetails.vInCarCorona = <<77.0293, 7077.3076, 0.9681>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_7
			sDetails.vInCarCorona = <<1468.9604, 4947.4727, 75.1823>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_8
			sDetails.vInCarCorona = <<-126.1526, 3546.554, 55.0922>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_9
			sDetails.vInCarCorona = <<1063.1115, 3275.5015, 36.6055>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_10
			sDetails.vInCarCorona = <<-509.5927, 5242.9717, 79.304>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.fInCarCoronaRadius = 15.0
		BREAK
		
		//COUNTRY Route 3
		CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_11
			sDetails.vInCarCorona = <<2351.0586, 5001.7998, 41.7707>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_12
			sDetails.vInCarCorona = <<1824.4545, 4733.5298, 32.5693>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_13
			sDetails.vInCarCorona = <<-1844.6897, 4556.438, 4.9786>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_14
			sDetails.vInCarCorona = <<-1376.3236, 4413.6875, 28.537>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_15
			sDetails.vInCarCorona = <<-18.1656, 6261.2383, 30.2358>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.fInCarCoronaRadius = 15.0
		BREAK
		
		//COUNTRY Route 4
		CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_16
			sDetails.vInCarCorona = <<570.2625, 4177.6675, 37.1188>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_17
			sDetails.vInCarCorona = <<-445.1836, 4015.9512, 80.0153>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_18
			sDetails.vInCarCorona = <<1440.2977, 2804.8059, 51.8026>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_19
			sDetails.vInCarCorona = <<-2103.8967, 2473.4958, 0.5534>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_20
			sDetails.vInCarCorona = <<-80.3433, 1879.3817, 196.2338>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.fInCarCoronaRadius = 15.0
		BREAK
		
		//COUNTRY Route 5
		CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_21
			sDetails.vInCarCorona = <<-494.5800, 5547.6592, 72.4075>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_22
			sDetails.vInCarCorona = <<1262.8695, 4384.9951, 43.9866>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_23
			sDetails.vInCarCorona = <<825.5037, 2137.7881, 51.2936>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_24
			sDetails.vInCarCorona = <<-2911.5293, 1592.0343, 28.2507>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_25
			sDetails.vInCarCorona = <<-1658.6764, 3045.8601, 30.8135>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.fInCarCoronaRadius = 15.0
		BREAK
		
		//COUNTRY Route 6
		CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_26
			sDetails.vInCarCorona = <<813.8033, 6638.8286, 1.0195>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_27
			sDetails.vInCarCorona = <<143.9241, 6317.5327, 30.474>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_28
			sDetails.vInCarCorona = <<-291.3325, 6303.2754, 30.4923>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_29
			sDetails.vInCarCorona = <<-575.9571, 5846.8691, 28.8116>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_30
			sDetails.vInCarCorona = <<2515.6079, 4962.8691, 43.5619>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.fInCarCoronaRadius = 15.0
		BREAK
		
		//COUNTRY Route 7
		CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_31
			sDetails.vInCarCorona = <<2900.8867, 4637.5098, 47.5426>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_32
			sDetails.vInCarCorona = <<1993.7156, 3934.4519, 31.2300>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_33
			sDetails.vInCarCorona = <<124.3457, 3004.6248, 47.3517>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_34
			sDetails.vInCarCorona = <<754.0544, 2336.9189, 49.0994>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_35
			sDetails.vInCarCorona = <<2746.9199, 1444.5044, 23.4889>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.fInCarCoronaRadius = 15.0
		BREAK
		
		//COUNTRY Route 8
		CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_36
			sDetails.vInCarCorona = <<1550.9885, 6543.8936, 21.4149>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_37
			sDetails.vInCarCorona = <<993.5634, 6403.8242, 28.4137>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_38
			sDetails.vInCarCorona = <<1623.7128, 3783.4639, 33.652>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_39
			sDetails.vInCarCorona = <<2623.7212, 1971.464, 29.141>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_40
			sDetails.vInCarCorona = <<2899.7607, 4382.5020, 49.3713>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.fInCarCoronaRadius = 15.0
		BREAK
		
		//COUNTRY Route 9
		CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_41
			sDetails.vInCarCorona = <<2581.925, 5608.7485, 59.9163>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_42
			sDetails.vInCarCorona = <<2236.6985, 4785.6587, 39.1814>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_43
			sDetails.vInCarCorona = <<1158.3419, 4584.4165, 73.9566>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_44
			sDetails.vInCarCorona = <<1495.8, 4599.0034, 50.9645>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_45
			sDetails.vInCarCorona = <<1988.4698, 3033.7903, 46.0563>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.fInCarCoronaRadius = 15.0
		BREAK
		
		//COUNTRY Route 10
		CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_46
			sDetails.vInCarCorona = <<-1640.2533, 4581.0366, 41.8798>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_47
			sDetails.vInCarCorona = <<2607.2266, 4441.9092, 39.0584>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_48
			sDetails.vInCarCorona = <<-1204.7871, 4448.3267, 30.2424>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_49
			sDetails.vInCarCorona = <<-2302.3362, 4116.8848, 27.3913>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
		BREAK
		
		CASE GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_50
			sDetails.vInCarCorona = <<468.1049, 3546.3735, 32.2386>>
			sDetails.vOnFootCorona = sDetails.vInCarCorona
			sDetails.fInCarCoronaRadius = 15.0
		BREAK
	ENDSWITCH
	
	IF eDropoffID != GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_5
	AND eDropoffID != GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_10
	AND eDropoffID != GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_15
	AND eDropoffID != GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_20
	AND eDropoffID != GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_25
	AND eDropoffID != GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_30
	AND eDropoffID != GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_35
	AND eDropoffID != GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_40
	AND eDropoffID != GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_45
	AND eDropoffID != GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_50
	AND eDropoffID != GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_5
	AND eDropoffID != GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_10
	AND eDropoffID != GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_15
	AND eDropoffID != GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_20
	AND eDropoffID != GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_25
	AND eDropoffID != GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_30
	AND eDropoffID != GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_35
	AND eDropoffID != GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_40
	AND eDropoffID != GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_45
	AND eDropoffID != GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_50
		SET_BIT(sDetails.iBS, FREEMODE_DELIVERY_DROPOFF_DETAILS_BS_DROPOFF_CONDITION_DPAD_RIGHT)
	ENDIF
ENDPROC

FUNC BOOL IS_DROPOFF_A_HILL_CLIMB_DROPOFF(FREEMODE_DELIVERY_DROPOFFS eDropoffID)
	IF eDropoffID = FREEMODE_DELIVERY_DROPOFF_INVALID
		RETURN FALSE
	ENDIF
	
	IF eDropoffID >= GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_1
	AND eDropoffID <= GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_50
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL IS_DROPOFF_A_MOVE_WEAPONS_DROPOFF(FREEMODE_DELIVERY_DROPOFFS eDropoffID)
	IF eDropoffID = FREEMODE_DELIVERY_DROPOFF_INVALID
		RETURN FALSE
	ENDIF
	
	IF eDropoffID >= GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_1
	AND eDropoffID <= GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_30
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_SMUGGLER_CUSTOM_SPAWN_POINT_COUNT(FREEMODE_DELIVERY_DROPOFFS eDropoffID)	

	IF IS_DROPOFF_A_MOVE_WEAPONS_DROPOFF(eDropoffID)
		// We're only returning the number of custom spawn points for move weapon 
		// city drop offs. Country drop offs are randomly selected.
		RETURN 16
	ENDIF
	
	RETURN 8
ENDFUNC

FUNC FREEMODE_DELIVERY_DROPOFF_DETAILS GUNRUNNING_DROPOFF_GET_DETAILS(FREEMODE_DELIVERY_DROPOFFS eDropoffID)
	FREEMODE_DELIVERY_DROPOFF_DETAILS details
	GUNRUN_VARIATION eMissionVariation = GUNRUNNING_GET_MISSION_VARIATION_FROM_DROPOFF(eDropoffID)
	
	IF GUNRUNNING_DROPOFF_IS_VALID(eDropoffID)
		details.txtScriptName = "GB_GUNRUNNING_DELIVERY"
		details.shouldDropoffLaunchDeliveryScript = &GUNRUNNING_DROPOFF_SHOULD_RUN_DELIVERY_SCRIPT
		details.willDropoffAcceptTypeOfDelivery = &GUNRUNNING_DROPOFF_WILL_ACCEPT_TYPE_OF_DELIVERY
		details.fOnFootCoronaRadius = 5.0
		details.fInCarCoronaRadius = 10.0
		
		IF GUNNRUNNING_DROPOFF_IS_PLAYER_BUNKER(eDropoffID)
			details.vOnFootCorona = g_SimpleInteriorData.vMainEntryLocates[ENUM_TO_INT(GET_SIMPLE_INTERIOR_FROM_GUNRUNNING_DROPOFF(eDropoffID))]
			details.vInCarCorona = GET_FACTORY_DROP_OFF_COORDS(GET_GUNRUNNING_DROPOFF_FACTORY_ID(eDropoffID))
			
			PED_INDEX ped
			ped = PLAYER_PED_ID()
			SWITCH FREEMODE_GET_PLAYER_DELIVERY_TYPE(ped)
				CASE FREEMODE_DELIVERY_TYPE_FOOT
					details.cutsceneToPlay = GUNRUNNING_CUTSCENE_BUNKER_FOOT
					FREEMODE_GET_PLAYER_DELIVERY_TYPE(ped)
				BREAK
				
				CASE FREEMODE_DELIVERY_TYPE_VEHICLE
					details.cutsceneToPlay = GUNRUNNING_CUTSCENE_BUNKER_VEHICLE
				BREAK
				
				CASE FREEMODE_DELIVERY_TYPE_HELI
					details.cutsceneToPlay = GUNRUNNING_CUTSCENE_BUNKER_HELI
					
					IF IS_PED_IN_ANY_VEHICLE_ATTACHED_TO_CARGOBOB(ped)
					OR IS_PED_IN_A_CARGOBOB_WITH_VEHICLE_ATTACHED(ped)
						details.cutsceneToPlay = GUNRUNNING_CUTSCENE_BUNKER_CARGOBOB_ATTACHED
					ENDIF
				BREAK
			ENDSWITCH

		ELSE
			SWITCH eMissionVariation			
				CASE GRV_SELL_MOVE_WEAPONS
					details.cutsceneToPlay = GUNRUNNING_CUTSCENE_MOVE_WEAPONS
					GUNRUNNING_GET_MOVE_WEAPONS_DROPOFF_COORDS(eDropoffID, details)
				BREAK
								
				CASE GRV_SELL_PHANTOM
					details.cutsceneToPlay = GUNRUNNING_CUTSCENE_PHANTOM
					GUNRUNNING_GET_PHANTOM_DROPOFF_COORDS(eDropoffID, details)
					details.fInCarCoronaRadius = 15.0
				BREAK
				
				CASE GRV_SELL_FOLLOW_THE_LEADER
					details.cutsceneToPlay = GUNRUNNING_CUTSCENE_FOLLOW_LEADER
					GUNRUNNING_GET_FOLLOW_LEADER_DROPOFF_COORDS(eDropoffID, details)
					details.fInCarCoronaRadius = 7.5
				BREAK
				
				CASE GRV_SELL_AMBUSHED
					details.cutsceneToPlay = GUNRUNNING_CUTSCENE_AMBUSHED
					GUNRUNNING_GET_AMBUSHED_DROPOFF_COORDS(eDropoffID, details)
				BREAK
				
				CASE GRV_SELL_HILL_CLIMB
					details.cutsceneToPlay = GUNRUNNING_CUTSCENE_HILL_CLIMB
					GUNRUNNING_GET_HILL_CLIMB_DROPOFF_COORDS(eDropoffID, details)
				BREAK
							
				CASE GRV_SELL_ROUGH_TERRAIN			
					details.cutsceneToPlay = GUNRUNNING_CUTSCENE_ROUGH_TERRAIN
					GUNRUNNING_GET_ROUGH_TERRAIN_DROPOFF_COORDS(eDropoffID, details)
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
	
	RETURN details
ENDFUNC

CONST_INT MAX_GUNRUNNING_DROPOFF_POOL 10	//The maximum amount of randomly selected dropoffs to be used when selecting a single dropoff

/// PURPOSE:
///    Used to fill a stuct with MAX_GUNRUNNING_DROPOFF_POOL dropoffs in a random order
/// PARAMS:
///    eVariation - The mission variation
///    eDropoffCandidates - The struct to fill
///    eLocation - City or countryside mission?
/// RETURNS:
///    True if we are able to successfully fill the struct
FUNC BOOL GENERATE_RANDOM_LIST_OF_GR_DROPOFFS(GUNRUN_VARIATION eVariation, FREEMODE_DELIVERY_DROPOFF_SELECTION_DATA &eDropoffCandidates[MAX_GUNRUNNING_DROPOFF_POOL], FREEMODE_DELIVERY_LOCATION eLocation)

	CONST_INT FREEMODE_DELIVERY_COUNT_OF_MOVE_WEAPONS_DROPOFFS 30
	
	INT i, iMinimum, iMaximum
	FREEMODE_DELIVERY_ROUTES eStartingRoute
	
	//Dropoff list used when selcting move weapons dropoffs
	FREEMODE_DELIVERY_DROPOFFS eDropoffPool[FREEMODE_DELIVERY_COUNT_OF_MOVE_WEAPONS_DROPOFFS] 
	
	SWITCH eVariation
		// Sell mission variations
		CASE GRV_SELL_MOVE_WEAPONS
			IF eLocation = FREEMODE_DELIVERY_LOCATION_CITY
				iMinimum = ENUM_TO_INT(GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_1)
				iMaximum = ENUM_TO_INT(GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_30)
			ELSE
				iMinimum = ENUM_TO_INT(GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_1)
				iMaximum = ENUM_TO_INT(GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_30)
			ENDIF
			
			IF (iMaximum - iMinimum) != (FREEMODE_DELIVERY_COUNT_OF_MOVE_WEAPONS_DROPOFFS - 1)
				CASSERTLN(DEBUG_AMBIENT, "GENERATE_RANDOM_LIST_OF_GR_DROPOFFS failed to find move weapons dropoffs. The number of dropoffs is out of sync!")
				RETURN FALSE
			ENDIF
			
			//Create a pool of move weapons dropoffs to pick from
			REPEAT FREEMODE_DELIVERY_COUNT_OF_MOVE_WEAPONS_DROPOFFS i
				eDropoffPool[i] = INT_TO_ENUM(FREEMODE_DELIVERY_DROPOFFS, (i + iMinimum))
			ENDREPEAT
			
		BREAK
		CASE GRV_SELL_FOLLOW_THE_LEADER
			IF eLocation = FREEMODE_DELIVERY_LOCATION_CITY
				iMinimum = ENUM_TO_INT(GUNRUNNING_DROPOFF_FOLLOW_LEADER_CITY_1)
			ELSE
				iMinimum = ENUM_TO_INT(GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_1)
			ENDIF
		BREAK
		CASE GRV_SELL_AMBUSHED
			IF eLocation = FREEMODE_DELIVERY_LOCATION_CITY
				eStartingRoute = FREEMODE_DELIVERY_ROUTE_AMBUSHED_CITY_1
			ELSE
				eStartingRoute = FREEMODE_DELIVERY_ROUTE_AMBUSHED_COUNTRY_1
			ENDIF
		BREAK
		CASE GRV_SELL_HILL_CLIMB
			IF eLocation = FREEMODE_DELIVERY_LOCATION_CITY
				eStartingRoute = FREEMODE_DELIVERY_ROUTE_HILL_CLIMB_CITY_1
			ELSE
				eStartingRoute = FREEMODE_DELIVERY_ROUTE_HILL_CLIMB_COUNTRY_1
			ENDIF
		BREAK
		CASE GRV_SELL_ROUGH_TERRAIN
			IF eLocation = FREEMODE_DELIVERY_LOCATION_CITY
				eStartingRoute = FREEMODE_DELIVERY_ROUTE_ROUGH_TERRAIN_CITY_1
			ELSE
				eStartingRoute = FREEMODE_DELIVERY_ROUTE_ROUGH_TERRAIN_COUNTRY_1
			ENDIF
		BREAK
		CASE GRV_SELL_PHANTOM
			IF eLocation = FREEMODE_DELIVERY_LOCATION_CITY
				iMinimum = ENUM_TO_INT(GUNRUNNING_DROPOFF_PHANTOM_CITY_1)
			ELSE
				iMinimum = ENUM_TO_INT(GUNRUNNING_DROPOFF_PHANTOM_COUNTRY_1)
			ENDIF			
		BREAK
		
		DEFAULT 
			CASSERTLN("GENERATE_RANDOM_LIST_OF_GR_DROPOFFS passed invalid mission variation: ", eVariation)
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	INT iMaxDropoffsForMissionType = MAX_GUNRUNNING_DROPOFF_POOL
	
	IF eVariation = GRV_SELL_AMBUSHED
		iMaxDropoffsForMissionType = 6
	ENDIF
		
	//Repeat once through to assign the 10 dropoff locations
	FOR i = 0 TO (iMaxDropoffsForMissionType - 1)
		IF eVariation = GRV_SELL_AMBUSHED
		OR eVariation = GRV_SELL_HILL_CLIMB
		OR eVariation = GRV_SELL_ROUGH_TERRAIN
			
			FREEMODE_DELIVERY_ROUTES eRoute = eStartingRoute + INT_TO_ENUM(FREEMODE_DELIVERY_ROUTES, i)
			
			eDropoffCandidates[i].eDropoffID = GUNRUNNING_GET_DROPOFF_ON_ROUTE(eRoute, 1)
			eDropoffCandidates[i].eRoute = eRoute
			
		ELIF eVariation = GRV_SELL_MOVE_WEAPONS
		
			INT iRandomDropoff = GET_RANDOM_INT_IN_RANGE(0, COUNT_OF(eDropoffPool))
						
			IF eDropoffPool[iRandomDropoff] != FREEMODE_DELIVERY_DROPOFF_INVALID
				//Assign the dropoff and remove it from the pool to avoid duplicates
				eDropoffCandidates[i].eDropoffID 	= eDropoffPool[iRandomDropoff]
				eDropoffPool[iRandomDropoff] 		= FREEMODE_DELIVERY_DROPOFF_INVALID			
				eDropoffCandidates[i].eRoute		= FREEMODE_DELIVERY_ROUTE_NO_ROUTE
			
				IF i = (MAX_GUNRUNNING_DROPOFF_POOL - 1)
					//No need to shuffle this list below as it is already random
					RETURN TRUE
				ENDIF
			ELSE
				i --
			ENDIF
		ELSE
			eDropoffCandidates[i].eDropoffID 	= INT_TO_ENUM(FREEMODE_DELIVERY_DROPOFFS, (i + iMinimum))
			eDropoffCandidates[i].eRoute		= FREEMODE_DELIVERY_ROUTE_NO_ROUTE
		ENDIF
	ENDFOR
	
	//Repeat once more to shuffle the list
	FOR i = 0 TO (iMaxDropoffsForMissionType - 1)
		FREEMODE_DELIVERY_ROUTES 	eSwapRoute
		FREEMODE_DELIVERY_DROPOFFS 	eSwapDropoff
		
		INT iRand = GET_RANDOM_INT_IN_RANGE(0, iMaxDropoffsForMissionType)
		
		eSwapDropoff 							= eDropoffCandidates[i].eDropoffID
		eSwapRoute								= eDropoffCandidates[i].eRoute
		eDropoffCandidates[i].eDropoffID 		= eDropoffCandidates[iRand].eDropoffID
		eDropoffCandidates[i].eRoute 			= eDropoffCandidates[iRand].eRoute
		eDropoffCandidates[iRand].eDropoffID	= eSwapDropoff
		eDropoffCandidates[iRand].eRoute		= eSwapRoute
	ENDFOR
	
	FOR i = 0 TO (MAX_GUNRUNNING_DROPOFF_POOL - 1)		
		CDEBUG1LN(DEBUG_AMBIENT, "[FM_DO_RES] GENERATE_RANDOM_LIST_OF_GR_DROPOFFS slot: ", i, " dropoff: ", FREEMODE_DELIVERY_GET_DROPOFF_DEBUG_NAME(eDropoffCandidates[i].eDropoffID))
	ENDFOR
	
	RETURN TRUE
ENDFUNC

FUNC INT GUNRUN_GET_MAX_DROPOFFS_FOR_MISSION_VARIATION(GUNRUN_VARIATION eVariation)
	SWITCH eVariation
		//Resupply/setup missions
		CASE GRV_SETUP
		CASE GRV_ALTRUISTS	
		CASE GRV_DESTROY_TRUCKS
		CASE GRV_DIVERSION
		CASE GRV_FLASHLIGHT
		CASE GRV_FLY_SWATTER
		CASE GRV_RIVAL_OPERATION
		CASE GRV_STEAL_APC
		CASE GRV_STEAL_MINIGUNS
		CASE GRV_STEAL_RAILGUNS
		CASE GRV_STEAL_RHINO
		CASE GRV_STEAL_TECHNICAL
		CASE GRV_STEAL_VAN
		CASE GRV_YACHT_SEARCH
			RETURN 1
		BREAK
		
		//Sell missions
		CASE GRV_SELL_HILL_CLIMB		RETURN 5
		CASE GRV_SELL_ROUGH_TERRAIN		RETURN 5
		CASE GRV_SELL_PHANTOM			RETURN 1
		CASE GRV_SELL_AMBUSHED			RETURN 5
		CASE GRV_SELL_FOLLOW_THE_LEADER	RETURN 1
		CASE GRV_SELL_MOVE_WEAPONS		RETURN 3
	ENDSWITCH
	
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Sets the data that describes how a dropoff or group of dropoffs should be used 
PROC GUNRUN_SET_DROPOFF_PROPERTIES(GUNRUN_VARIATION eVariation, FREEMODE_DELIVERY_ACTIVE_DROPOFF_PROPERTIES &sData)
	
	SWITCH eVariation
		CASE GRV_SELL_HILL_CLIMB		FREEMODE_FELIVERY_SET_DROPOFF_PROPERTIES(sData, DROPOFF_PROPERTY_ANY_ORDER)					BREAK
		CASE GRV_SELL_ROUGH_TERRAIN		FREEMODE_FELIVERY_SET_DROPOFF_PROPERTIES(sData, DROPOFF_PROPERTY_FINAL_DROPOFF_ACTIVE_LAST)	BREAK
		CASE GRV_SELL_PHANTOM			FREEMODE_FELIVERY_SET_DROPOFF_PROPERTIES(sData, DROPOFF_PROPERTY_SIMPLE)					BREAK
		CASE GRV_SELL_AMBUSHED			FREEMODE_FELIVERY_SET_DROPOFF_PROPERTIES(sData, DROPOFF_PROPERTY_SEQUENTIAL_DELIVERY)		BREAK
		CASE GRV_SELL_FOLLOW_THE_LEADER	FREEMODE_FELIVERY_SET_DROPOFF_PROPERTIES(sData, DROPOFF_PROPERTY_VEHICLE_SPECIFIC)			BREAK
		CASE GRV_SELL_MOVE_WEAPONS		FREEMODE_FELIVERY_SET_DROPOFF_PROPERTIES(sData, DROPOFF_PROPERTY_SIMPLE_MULTIPLE)			BREAK
		
		DEFAULT
			FREEMODE_FELIVERY_SET_DROPOFF_PROPERTIES(sData, DROPOFF_PROPERTY_BUNKER)
		BREAK
	ENDSWITCH
	
ENDPROC

FUNC BOOL SHOULD_DROPOFF_USE_PRESET_SPAWN_COORDS(FREEMODE_DELIVERY_DROPOFFS eDropoffID) 
	IF eDropoffID = FREEMODE_DELIVERY_DROPOFF_INVALID
		RETURN FALSE
	ENDIF

	IF eDropoffID = GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_11
	OR eDropoffID = GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_13
	OR (eDropoffID >= GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_8 AND eDropoffID <= GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_8)
	OR (eDropoffID >= GUNRUNNING_DROPOFF_FOLLOW_LEADER_CITY_1 AND eDropoffID <= GUNRUNNING_DROPOFF_FOLLOW_LEADER_CITY_10)
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_DROPOFF_SPAWN_PLAYER_AFTER_DELIVERY(FREEMODE_DELIVERY_DROPOFFS eDropoffID) 
	IF eDropoffID >= GUNRUNNING_DROPOFF_BUNKER_1
	AND eDropoffID <= GUNRUNNING_DROPOFF_BUNKER_12
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Returns the model name of prop required by a dropoff
/// PARAMS:
///    eDropoff - The dropoff
///    iPropNumber - prop 0, 1 or 2
FUNC MODEL_NAMES GUNRUN_GET_DROPOFF_EXTRA_MODEL(FREEMODE_DELIVERY_DROPOFFS eDropoff, INT iPropNumber)
	SWITCH eDropoff
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_CITY_3
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_1
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_2
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_4
		//CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_5
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_6
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_7
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_8		
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_9
			
			SWITCH iPropNumber
				//Prop one 
				CASE 0	RETURN MULE3				
				//Prop two 
				CASE 1	RETURN PROP_PORTACABIN01				
				//Prop three 
				CASE 2	RETURN PROP_WORKLIGHT_03B
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

FUNC VECTOR GUNRUN_GET_DROPOFF_EXTRA_MODEL_POSITION(FREEMODE_DELIVERY_DROPOFFS eDropoff, INT iPropNumber)
	SWITCH eDropoff
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_CITY_3			
			SWITCH iPropNumber
				//Prop one 
				CASE 0	RETURN <<1139.9301, -2517.9309, 32.2280>>			
				//Prop two
				CASE 1	RETURN <<1148.0750, -2514.7070, 32.3340>>			
				//Prop three 
				CASE 2	RETURN <<1154.2970, -2515.1870, 32.4680>>
			ENDSWITCH
		BREAK
		
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_1		
			SWITCH iPropNumber
				//Prop one
				CASE 0	RETURN <<-224.4910, 2076.8479, 137.6980>>			
				//Prop two
				CASE 1	RETURN <<-222.7300, 2055.8401, 138.9590>>			
				//Prop three
				CASE 2	RETURN <<-219.9570, 2064.5801, 139.2550>>
			ENDSWITCH
		BREAK
		
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_2		
			SWITCH iPropNumber
				//Prop one
				CASE 0	RETURN <<2775.6260, 2859.3340, 34.3700>>			
				//Prop two
				CASE 1	RETURN <<2783.6321, 2852.8191, 34.8990>>			
				//Prop three 
				CASE 2	RETURN <<2783.2900, 2843.6370, 35.1880>>
			ENDSWITCH
		BREAK
		
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_4		
			SWITCH iPropNumber
				//Prop one
				CASE 0	RETURN <<-2673.3210, 3455.6831, 14.1310>>			
				//Prop two
				CASE 1	RETURN <<-2660.1079, 3451.2339, 14.3920>>			
				//Prop three
				CASE 2	RETURN <<-2666.8689, 3453.4241, 14.4360>>
			ENDSWITCH
		BREAK
		
//		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_5	
//			SWITCH iPropNumber
//				//Prop one
//				CASE 0	RETURN <<1646.1174, 3537.4836, 35.0300>>			
//				//Prop two
//				CASE 1	RETURN <<1634.8910, 3508.0042, 35.6468>>			
//				//Prop three
//				CASE 2	RETURN <<1647.0258, 3529.4231, 34.9653>>
//			ENDSWITCH
//		BREAK
		
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_6
			SWITCH iPropNumber
				//Prop one
				CASE 0	RETURN <<-2370.1499, 4288.0640, 9.2180>>			
				//Prop two
				CASE 1	RETURN <<-2373.0830, 4278.5020, 9.4740>>			
				//Prop three
				CASE 2	RETURN <<-2377.6990, 4274.2749, 9.5790>>
			ENDSWITCH
		BREAK
		
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_7
			SWITCH iPropNumber
				//Prop one
				CASE 0	RETURN <<2492.3491, 4289.0688, 38.1240>>			
				//Prop two
				CASE 1	RETURN <<2500.4800, 4287.0010, 38.1630>>			
				//Prop three
				CASE 2	RETURN <<2507.7629, 4287.9360, 38.2780>>
			ENDSWITCH
		BREAK
		
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_8
			SWITCH iPropNumber
				//Prop one
				CASE 0	RETURN <<-1863.4659, 4429.1069, 47.1920>>			
				//Prop two
				CASE 1	RETURN <<-1853.2550, 4425.6040, 47.8230>>			
				//Prop three
				CASE 2	RETURN <<-1847.3030, 4423.5801, 47.8680>>
			ENDSWITCH
		BREAK
		
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_9
			SWITCH iPropNumber
				//Prop one
				CASE 0	RETURN <<-1568.7629, 4499.1890, 20.5200>>			
				//Prop two
				CASE 1	RETURN <<-1555.5551, 4475.9678, 18.4360>>			
				//Prop three
				CASE 2	RETURN <<-1559.4270, 4480.2251, 18.7500>>
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC VECTOR GUNRUN_GET_DROPOFF_EXTRA_MODEL_ROTATION(FREEMODE_DELIVERY_DROPOFFS eDropoff, INT iPropNumber)
	SWITCH eDropoff
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_CITY_3
			SWITCH iPropNumber
				//Prop one 
				CASE 0	RETURN <<0.0, 0.0, 193.0460>>		
				//Prop two
				CASE 1	RETURN <<0.0000, 0.0000, 13.0460>>			
				//Prop three 
				CASE 2	RETURN <<0.0000, 0.0000, -7.2000>>
			ENDSWITCH
		BREAK
		
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_1
			SWITCH iPropNumber
				//Prop one
				CASE 0	RETURN <<0.0, 0.0, 347.1970>>		
				//Prop two
				CASE 1	RETURN <<0.0000, 0.0000, -134.4020>>			
				//Prop three
				CASE 2	RETURN <<10.3240, -3.0800, -84.3240>>
			ENDSWITCH
		BREAK
		
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_2
			SWITCH iPropNumber
				//Prop one
				CASE 0	RETURN <<0.0, 0.0, 107.3990>>		
				//Prop two
				CASE 1	RETURN <<0.0000, -1.4000, -56.4010>>			
				//Prop three 
				CASE 2	RETURN <<0.0000, 0.0000, -152.0010>>
			ENDSWITCH
		BREAK
		
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_4
			SWITCH iPropNumber
				//Prop one
				CASE 0	RETURN <<0.0, 0.0, 230.7990>>			
				//Prop two
				CASE 1	RETURN <<1.0000, 0.0000, -178.7580>>		
				//Prop three
				CASE 2	RETURN <<0.0000, 0.0000, -178.7580>>
			ENDSWITCH
		BREAK
		
//		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_5
//			SWITCH iPropNumber
//				//Prop one
//				CASE 0	RETURN <<0.0, 0.0, 31.3999>>		
//				//Prop two
//				CASE 1	RETURN <<-5.0455, -1.5458, 30.3318>>		
//				//Prop three
//				CASE 2	RETURN <<0.0000, 0.0000, -19.4002>>
//			ENDSWITCH
//		BREAK
		
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_6
			SWITCH iPropNumber
				//Prop one
				CASE 0	RETURN <<0.0, 0.0, 269.2430>>			
				//Prop two
				CASE 1	RETURN <<0.0000, 0.0000, -116.4040>>		
				//Prop three
				CASE 2	RETURN <<0.0000, 0.0000, -147.4000>>
			ENDSWITCH
		BREAK
		
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_7
			SWITCH iPropNumber
				//Prop one
				CASE 0	RETURN <<0.0, 0.0, 328.9980>>		
				//Prop two
				CASE 1	RETURN <<0.0000, 0.0000, 166.3970>>			
				//Prop three
				CASE 2	RETURN <<0.0000, 0.0000, -160.0010>>
			ENDSWITCH
		BREAK
		
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_8
			SWITCH iPropNumber
				//Prop one
				CASE 0	RETURN <<0.0, 0.0, 9.8000>>	
				//Prop two
				CASE 1	RETURN <<-2.2000, 0.0000, -1.1580>>
				//Prop three
				CASE 2	RETURN <<-5.9910, -0.2140, -16.6110>>
			ENDSWITCH
		BREAK
		
		CASE GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_9
			SWITCH iPropNumber
				//Prop one
				CASE 0	RETURN <<0.0, 0.0, 201.3970>>		
				//Prop two
				CASE 1	RETURN <<-3.1000, 0.0000, 163.9980>>			
				//Prop three
				CASE 2	RETURN <<-5.8740, 5.0760, -153.5390>>
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

/// PURPOSE:
///    Returns any extra information required by the gunrunning mission scripts 
///    e.g. Model names of extra props to be created at the given dropoff points
/// PARAMS:
///    eRoutes - The dropoff to check
FUNC FREEMODE_DELIVERY_DROPOFF_PROPS GUNRUN_GET_DROPOFF_EXTRA_DATA(FREEMODE_DELIVERY_DROPOFFS eDropoff)
	
	INT i
	FREEMODE_DELIVERY_DROPOFF_PROPS sData
	
	REPEAT FREEMODE_DELIVERY_MAX_EXTRA_PROPS_PER_DROPOFF i
		sData.ePropModels[i]	= GUNRUN_GET_DROPOFF_EXTRA_MODEL(eDropoff, i)
		sData.vPropPosition[i]	= GUNRUN_GET_DROPOFF_EXTRA_MODEL_POSITION(eDropoff, i)
		sData.vPropRotation[i]	= GUNRUN_GET_DROPOFF_EXTRA_MODEL_ROTATION(eDropoff, i)
	ENDREPEAT
	
	RETURN sData
ENDFUNC

FUNC INT GUNRUN_GET_DROPOFF_COUNT_OF_PROPS_REQUIRED(FREEMODE_DELIVERY_DROPOFFS eDropoff)
	INT i = 0
	INT iNumProps = 0
	
	REPEAT FREEMODE_DELIVERY_MAX_EXTRA_PROPS_PER_DROPOFF i
		IF GUNRUN_GET_DROPOFF_EXTRA_MODEL(eDropoff, i) != DUMMY_MODEL_FOR_SCRIPT
			iNumProps ++
		ENDIF
	ENDREPEAT
	
	RETURN iNumProps
ENDFUNC

FUNC BOOL GUNRUN_CAN_WE_RESERVE_ALL_REQUIRED_NETWORK_ENTITIES(INT iNumMissionEntities, FREEMODE_DELIVERY_DROPOFFS eDropoff, GUNRUN_VARIATION eVariation)
	INT iDropoffProps	= GUNRUN_GET_DROPOFF_COUNT_OF_PROPS_REQUIRED(eDropoff)
	INT iNumVehicles 	= GB_GET_GUNRUN_NUM_VEH_REQUIRED(eVariation, GRS_INVALID, iNumMissionEntities)
	INT iNumProps 		= GB_GET_GUNRUN_NUM_OBJ_REQUIRED(eVariation, GRS_INVALID, iNumMissionEntities, iDropoffProps)
	
	PRINTLN("[FM_DO_RES] GUNRUN_CAN_WE_RESERVE_ALL_REQUIRED_NETWORK_ENTITIES - iDropoffProps = ", iDropoffProps, " iNumVehicles = ", iNumVehicles, " iNumProps = ", iNumProps)
	
	RETURN CAN_REGISTER_MISSION_ENTITIES(0, iNumVehicles, iNumProps, 0)
ENDFUNC

FUNC BOOL GUNRUN_DO_ALL_DROPOFFS_ON_ROUTE_MEET_DISTANCE_REQUIREMENTS(FREEMODE_DELIVERY_ROUTES eRoute, FLOAT fMinDistance, VECTOR vBunkerCoords)
	INT i
	VECTOR vDropoffCoords
	FREEMODE_DELIVERY_DROPOFFS eDropoff
	FREEMODE_DELIVERY_DROPOFF_DETAILS eDetails
	
	REPEAT GUNRUNNING_GET_MAX_DROPOFFS_ON_ROUTE() i
		eDropoff 		= GUNRUNNING_GET_DROPOFF_ON_ROUTE(eRoute, (i + 1))
		eDetails 		= GUNRUNNING_DROPOFF_GET_DETAILS(eDropoff)
		vDropoffCoords 	= eDetails.vInCarCorona
		
		IF eDropoff != FREEMODE_DELIVERY_DROPOFF_INVALID
		AND fMinDistance > VDIST(vDropoffCoords, vBunkerCoords)
			PRINTLN("[FM_DO_RES] GUNRUN_DO_ALL_DROPOFFS_ON_ROUTE_MEET_DISTANCE_REQUIREMENTS - Dropoff ", 
						FREEMODE_DELIVERY_GET_DROPOFF_DEBUG_NAME(eDropoff), " on route: ", 
						FREEMODE_DELIVERY_GET_ROUTE_DEBUG_NAME(eRoute), " failed the distance requirement. Distance is: ",
						VDIST(vDropoffCoords, vBunkerCoords), " minimum distance is: ", fMinDistance)
			RETURN FALSE
		ENDIF		
		
	ENDREPEAT
	
	PRINTLN("[FM_DO_RES] GUNRUN_DO_ALL_DROPOFFS_ON_ROUTE_MEET_DISTANCE_REQUIREMENTS - All dropoffs are all further away than the minimum distance of: ", fMinDistance)
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Selects a freemode delivery dropoff based on the mission variation.
///    Can only be called by the host of freemode
/// PARAMS:
///    piGangBoss - The id of the gang boss starting a mission
///    eOwnedFactory - The factory ID of the players owned bunker
///    eVariation - The mission variation selected
///    sData - The data struct to fill (FREEMODE_DELIVERY_ACTIVE_DROPOFF_LIST)
///    eLocation - city or countryside dropoff?
/// RETURNS:
///    True if a dropoff or multiple dropoffs were successfully selected and reserved
FUNC BOOL GET_DROP_OFF_FOR_GUNRUN_VARIATION(PLAYER_INDEX piGangBoss, FACTORY_ID eOwnedFactory, GUNRUN_VARIATION eVariation, FREEMODE_DELIVERY_ACTIVE_DROPOFF_PROPERTIES &sData, FREEMODE_DELIVERY_LOCATION eLocation)
	
	SIMPLE_INTERIORS eSimpleInteriorID = GET_SIMPLE_INTERIOR_ID_FROM_FACTORY_ID(eOwnedFactory)
	
	IF eSimpleInteriorID = SIMPLE_INTERIOR_INVALID
		RETURN FALSE
	ENDIF
	
	//Array of dropoffs that we select from
	FREEMODE_DELIVERY_DROPOFF_SELECTION_DATA eDropoffCandidates[MAX_GUNRUNNING_DROPOFF_POOL]
	//Dropoff details
	FREEMODE_DELIVERY_DROPOFF_DETAILS eDetails
	//Minimum distance the first dropoff must be from the bunker
	FLOAT fMinDistFromBunkerRoute = 600
	FLOAT fMinDistFromBunkerSingleDropoff = 1200
	FLOAT fTmpBunkerDist
	VECTOR vDropoffCoords, vBunkerCoords
	INT i, j, iMaxGunRunEnitiesForMission, iNumGoons, iProductPercentage
	
	vBunkerCoords = g_SimpleInteriorData.vMidPoints[eSimpleInteriorID]
	
	IF piGangBoss = INVALID_PLAYER_INDEX()
		CASSERTLN(DEBUG_AMBIENT, "GET_DROP_OFF_FOR_GUNRUN_VARIATION passed invalid boss id.")
		RETURN FALSE
	ELIF NOT GB_IS_PLAYER_BOSS_OF_A_GANG(piGangBoss)
		CASSERTLN(DEBUG_AMBIENT, "GET_DROP_OFF_FOR_GUNRUN_VARIATION passed non boss player.")
		RETURN FALSE
	ELIF eOwnedFactory = FACTORY_ID_INVALID
		CASSERTLN(DEBUG_AMBIENT, "GET_DROP_OFF_FOR_GUNRUN_VARIATION passed invalid owned bunker.")
		RETURN FALSE
	ELIF NOT IS_FACTORY_OF_TYPE_WEAPONS(eOwnedFactory)
		CASSERTLN(DEBUG_AMBIENT, "GET_DROP_OFF_FOR_GUNRUN_VARIATION passed a factory id that is not a bunker.")
		RETURN FALSE
	ELIF NETWORK_GET_HOST_OF_SCRIPT(FREEMODE_SCRIPT()) != PLAYER_ID()
		CASSERTLN(DEBUG_AMBIENT, "GET_DROP_OFF_FOR_GUNRUN_VARIATION needs to be called by the host of freemode.")
		RETURN FALSE
	ENDIF
	
	//Initalise the struct
	INIT_FREEMODE_ACTIVE_DROPOFF_PROPERTIES(sData)
	//Set the properties of the dropoff
	GUNRUN_SET_DROPOFF_PROPERTIES(eVariation, sData)
	
	iNumGoons 					= (GB_GET_NUM_GOONS_IN_PLAYER_GANG(piGangBoss) + 1)
	iProductPercentage 			= GET_PRODUCT_TOTAL_FOR_FACTORY(piGangBoss, eOwnedFactory)
	iMaxGunRunEnitiesForMission = GB_GET_NUM_GUNRUN_ENTITIES_FOR_VARIATION(eVariation, iNumGoons, iProductPercentage, DEFAULT #IF IS_DEBUG_BUILD , FALSE #ENDIF)
		
	#IF IS_DEBUG_BUILD
	IF g_FreemodeDeliveryData.eDbgSelectedDropoff[0] != FREEMODE_DELIVERY_DROPOFF_INVALID
	AND g_FreemodeDeliveryData.eDbgSelectedDropoff[0] < SMUGGLER_DROPOFF_LSIA_HANGAR_1
		IF eVariation = GUNRUNNING_GET_MISSION_VARIATION_FROM_DROPOFF(g_FreemodeDeliveryData.eDbgSelectedDropoff[0])
		
			CDEBUG1LN(DEBUG_AMBIENT, "[FM_DO_RES] GET_DROP_OFF_FOR_GUNRUN_VARIATION *DEBUG* Reserving dropoffs")
			
			REPEAT 3 i
				
				//Select the required number of dropoffs for this variation
				sData.eDropoffList.eDropoffID[sData.eDropoffList.iCount] = g_FreemodeDeliveryData.eDbgSelectedDropoff[i]					
				sData.eDropoffList.iCount ++
				
				eDetails = GUNRUNNING_DROPOFF_GET_DETAILS(g_FreemodeDeliveryData.eDbgSelectedDropoff[i])
				CDEBUG1LN(DEBUG_AMBIENT, "[FM_DO_RES] GET_DROP_OFF_FOR_GUNRUN_VARIATION Dropoff coord = ", eDetails.vInCarCorona, " for dropoff with ID: ", g_FreemodeDeliveryData.eDbgSelectedDropoff[i])
				
				//Exit when we have the number of dropoffs we need
				IF sData.eDropoffList.iCount >= iMaxGunRunEnitiesForMission
				OR sData.eDropoffList.iCount >= GUNRUN_GET_MAX_DROPOFFS_FOR_MISSION_VARIATION(eVariation)
					FREEMODE_DELIVERY_DEBUG_PRINT_FREEMODE_DELIVERY_DROPOFF_PROPERTIES(sData)
					
					g_FreemodeDeliveryData.eDbgSelectedDropoff[0]	= FREEMODE_DELIVERY_DROPOFF_INVALID
					g_FreemodeDeliveryData.eDbgSelectedDropoff[1]	= FREEMODE_DELIVERY_DROPOFF_INVALID
					g_FreemodeDeliveryData.eDbgSelectedDropoff[2]	= FREEMODE_DELIVERY_DROPOFF_INVALID
		
					RETURN TRUE
				ENDIF
				
			ENDREPEAT
		ENDIF
	ENDIF
	
	IF g_FreemodeDeliveryData.eDbgSelectedRoute	!= FREEMODE_DELIVERY_ROUTE_NO_ROUTE
	AND g_FreemodeDeliveryData.eDbgSelectedRoute < FREEMODE_DELIVERY_FLYING_FORTRESS_1
		FREEMODE_DELIVERY_DROPOFFS eDropoff = GUNRUNNING_GET_DROPOFF_ON_ROUTE(g_FreemodeDeliveryData.eDbgSelectedRoute, 1)
		IF eVariation = GUNRUNNING_GET_MISSION_VARIATION_FROM_DROPOFF(eDropoff)
			sData.eDropoffList.iCount = GUNRUNNING_GET_MAX_DROPOFFS_ON_ROUTE()
			CDEBUG1LN(DEBUG_AMBIENT, "[FM_DO_RES] GET_DROP_OFF_FOR_GUNRUN_VARIATION *DEBUG* Reserving dropoffs on route: : ", FREEMODE_DELIVERY_GET_ROUTE_DEBUG_NAME(g_FreemodeDeliveryData.eDbgSelectedRoute), ". Starting with dropoff: ", FREEMODE_DELIVERY_GET_DROPOFF_DEBUG_NAME(eDropoff), " . Maximum no. dropoffs: ", sData.eDropoffList.iCount)
					
			REPEAT sData.eDropoffList.iCount j
				sData.eDropoffList.eDropoffID[j] = GUNRUNNING_GET_DROPOFF_ON_ROUTE(g_FreemodeDeliveryData.eDbgSelectedRoute, (j + 1))
				
				IF j >= iMaxGunRunEnitiesForMission
				AND sData.eDropoffList.iCount < FREEMODE_DELIVERY_MAX_ACTIVE_GUNRUN_DROPOFFS
					//For missions that need dropoffs based on the number of vehicles being created
					//Currently all missions with 5 dropoffs need all of them regardless of the number of vehicles
					sData.eDropoffList.iCount = (j + 1)
				ENDIF				
			ENDREPEAT
			
			FREEMODE_DELIVERY_DEBUG_PRINT_FREEMODE_DELIVERY_DROPOFF_PROPERTIES(sData)
			
			g_FreemodeDeliveryData.eDbgSelectedRoute = FREEMODE_DELIVERY_ROUTE_NO_ROUTE
			
			RETURN TRUE	
		ENDIF
	ENDIF
	#ENDIF
	
	#IF IS_DEBUG_BUILD
		IF eVariation = GRV_SELL_HILL_CLIMB
		AND GET_COMMANDLINE_PARAM_EXISTS("sc_GUNRUNFlowPlayVariations")
			
			FREEMODE_DELIVERY_ROUTES eRoute
			
			IF eLocation = FREEMODE_DELIVERY_LOCATION_CITY
				eRoute = FREEMODE_DELIVERY_ROUTE_HILL_CLIMB_CITY_2
			ELSE
				eRoute = FREEMODE_DELIVERY_ROUTE_HILL_CLIMB_COUNTRY_2
			ENDIF
			
			REPEAT FREEMODE_DELIVERY_MAX_ACTIVE_GUNRUN_DROPOFFS j
				sData.eDropoffList.eDropoffID[j] = GUNRUNNING_GET_DROPOFF_ON_ROUTE(eRoute, (j + 1))
			ENDREPEAT
			
			sData.eDropoffList.iCount = FREEMODE_DELIVERY_MAX_ACTIVE_GUNRUN_DROPOFFS
			PRINTLN("[FM_DO_RES][DEBUG_HILL_CLIMB][FLOW_PLAY] GET_DROP_OFF_FOR_GUNRUN_VARIATION - Selecting dropoff for boss: ", GET_PLAYER_NAME(piGangBoss), " eLocation = ", eLocation)
			FREEMODE_DELIVERY_DEBUG_PRINT_FREEMODE_DELIVERY_DROPOFF_PROPERTIES(sData)
								
			RETURN TRUE
		ENDIF
	#ENDIF
	
	SWITCH eVariation
		CASE GRV_SETUP
		CASE GRV_ALTRUISTS	
		CASE GRV_DESTROY_TRUCKS
		CASE GRV_DIVERSION
		CASE GRV_FLASHLIGHT
		CASE GRV_FLY_SWATTER
		CASE GRV_RIVAL_OPERATION
		CASE GRV_STEAL_APC
		CASE GRV_STEAL_MINIGUNS
		CASE GRV_STEAL_RAILGUNS
		CASE GRV_STEAL_RHINO
		CASE GRV_STEAL_TECHNICAL
		CASE GRV_STEAL_VAN
		CASE GRV_YACHT_SEARCH
		CASE GRV_CRASH_SITE_2
			
			//We're not checking if we can reserve entities for this one as theese missions all use the bunker which does not reserve any props
			//Set the struct data - For resupply missions the final dropoff will always be the players bunker
			sData.eDropoffList.eDropoffID[0] = GET_GUNRUNNING_DROPOFF_SIMPLE_INTERIOR(eSimpleInteriorID)
			sData.eDropoffList.iCount = 1
			
			CDEBUG1LN(DEBUG_AMBIENT, "[FM_DO_RES] GET_DROP_OFF_FOR_GUNRUN_VARIATION Reserving bunker dropoff: ", sData.eDropoffList.eDropoffID[0], " for player: ", GET_PLAYER_NAME(piGangBoss), " owned bunker ID: ", eOwnedFactory, " simple interior: ", eSimpleInteriorID)
			//Always return true for the bunkers as multiple players could have the same bunker
			RETURN TRUE
			
		BREAK
		
		// Sell mission variations
		CASE GRV_SELL_HILL_CLIMB
		CASE GRV_SELL_ROUGH_TERRAIN
		CASE GRV_SELL_PHANTOM
		CASE GRV_SELL_AMBUSHED
		CASE GRV_SELL_FOLLOW_THE_LEADER
		CASE GRV_SELL_MOVE_WEAPONS
			GENERATE_RANDOM_LIST_OF_GR_DROPOFFS(eVariation, eDropoffCandidates, eLocation)
			
			REPEAT MAX_GUNRUNNING_DROPOFF_POOL i
				eDetails = GUNRUNNING_DROPOFF_GET_DETAILS(eDropoffCandidates[i].eDropoffID)
				
				vDropoffCoords = eDetails.vInCarCorona
				fTmpBunkerDist = VDIST(vDropoffCoords, vBunkerCoords)
				
				IF GUNRUNNING_DROPOFF_IS_VALID(eDropoffCandidates[i].eDropoffID)
				AND NOT GUNNRUNNING_DROPOFF_IS_PLAYER_BUNKER(eDropoffCandidates[i].eDropoffID)
				AND NOT IS_DROPOFF_RESERVED_ON_SERVER(eDropoffCandidates[i].eDropoffID)
					IF GUNRUN_CAN_WE_RESERVE_ALL_REQUIRED_NETWORK_ENTITIES(iMaxGunRunEnitiesForMission, eDropoffCandidates[i].eDropoffID, eVariation)
						//If it's a route the fill the list of dropoffs
						IF eVariation = GRV_SELL_AMBUSHED
						OR eVariation = GRV_SELL_HILL_CLIMB
						OR eVariation = GRV_SELL_ROUGH_TERRAIN
							
							IF GUNRUN_DO_ALL_DROPOFFS_ON_ROUTE_MEET_DISTANCE_REQUIREMENTS(eDropoffCandidates[i].eRoute, fMinDistFromBunkerRoute, vBunkerCoords)
								sData.eDropoffList.iCount = GUNRUNNING_GET_MAX_DROPOFFS_ON_ROUTE()
								
								REPEAT sData.eDropoffList.iCount j
									sData.eDropoffList.eDropoffID[j] = GUNRUNNING_GET_DROPOFF_ON_ROUTE(eDropoffCandidates[i].eRoute, (j + 1))
									
									IF (j + 1) >= iMaxGunRunEnitiesForMission
									AND sData.eDropoffList.iCount < FREEMODE_DELIVERY_MAX_ACTIVE_GUNRUN_DROPOFFS							
										//For missions that need dropoffs based on the number of vehicles being created
										//Currently all missions with 5 dropoffs need all of them regardless of the number of vehicles
										sData.eDropoffList.iCount = (j + 1)
										BREAKLOOP
									ENDIF
								ENDREPEAT
								
								#IF IS_DEBUG_BUILD
									FREEMODE_DELIVERY_DEBUG_PRINT_FREEMODE_DELIVERY_DROPOFF_PROPERTIES(sData)
								#ENDIF
								
								RETURN TRUE
							ELSE
								PRINTLN("[FM_DO_RES] GET_DROP_OFF_FOR_GUNRUN_VARIATION Route ", i, " has a dropoff that is too close to be selected for bunker: ", eOwnedFactory)
							ENDIF
						ELSE
							IF fTmpBunkerDist > fMinDistFromBunkerSingleDropoff
								//Select the required number of dropoffs for this variation
								sData.eDropoffList.eDropoffID[sData.eDropoffList.iCount] = eDropoffCandidates[i].eDropoffID						
								sData.eDropoffList.iCount ++
								
								//Exit when we have the number of dropoffs we need
								IF sData.eDropoffList.iCount >= iMaxGunRunEnitiesForMission
								OR sData.eDropoffList.iCount >= GUNRUN_GET_MAX_DROPOFFS_FOR_MISSION_VARIATION(eVariation)
									#IF IS_DEBUG_BUILD
										FREEMODE_DELIVERY_DEBUG_PRINT_FREEMODE_DELIVERY_DROPOFF_PROPERTIES(sData)
									#ENDIF
									RETURN TRUE
								ENDIF
							ELSE
								PRINTLN("[FM_DO_RES] GET_DROP_OFF_FOR_GUNRUN_VARIATION Dropoff ", i, " is too close to be selected for bunker: ", eOwnedFactory, " distance is: ", fTmpBunkerDist)
							ENDIF
							
						ENDIF
					ELSE
						CDEBUG1LN(DEBUG_AMBIENT, "[FM_DO_RES] GET_DROP_OFF_FOR_GUNRUN_VARIATION dropoff ", i, " failed checks as we can't reserve the number of entities required")
					ENDIF
					
				#IF IS_DEBUG_BUILD
				ELIF IS_DROPOFF_RESERVED_ON_SERVER(eDropoffCandidates[i].eDropoffID)
					CDEBUG1LN(DEBUG_AMBIENT, "[FM_DO_RES] GET_DROP_OFF_FOR_GUNRUN_VARIATION dropoff ", i, " failed checks as it is already active")
				ELSE 
					CDEBUG1LN(DEBUG_AMBIENT, "[FM_DO_RES] GET_DROP_OFF_FOR_GUNRUN_VARIATION dropoff ", i, " failed checks as it is too close to the bunker")
				#ENDIF
				ENDIF
				
			ENDREPEAT
			
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC


