
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        NIGHT_CLUB_PEDS.sc																					//
// Description: Script for managing the nightclub peds.																//
//				                                                                                                    //
//																													//
// Written by:  Online Technical Team: Philip O'Duffy																//
// Date:  		19/01/2018																							//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Mission Script -----------------------------------------//


USING "NightClubPedsHeader.sch"

NIGHT_CLUB_STRUCT nightClubStruct
SERVER_BROADCAST_DATA ServerBD
PLAYER_BROADCAST_DATA PlayerBD[NUM_NETWORK_PLAYERS]
SCRIPT_TIMER st_HostRequestTimer

PROC MAINTAIN_NIGHTCLUB_OWNER_AS_HOST()
	IF GET_OWNER_OF_THIS_NIGHTCLUB() = PLAYER_ID()
		IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
			IF NOT HAS_NET_TIMER_STARTED(st_HostRequestTimer)
				START_NET_TIMER(st_HostRequestTimer)
			ENDIF
			
			IF (HAS_NET_TIMER_STARTED(st_HostRequestTimer) 
			AND HAS_NET_TIMER_EXPIRED(st_HostRequestTimer, 3000))
				PRINTLN("Nightclubpeds: MAINTAIN_NIGHTCLUB_OWNER_AS_HOST - Requesting to be host.")
					
				NETWORK_REQUEST_TO_BE_HOST_OF_THIS_SCRIPT()
				
				RESET_NET_TIMER(st_HostRequestTimer)
			ENDIF
		ENDIF
	ENDIF

ENDPROC

SCRIPT

	
	IF NETWORK_IS_GAME_IN_PROGRESS()	
		PROCESS_PRE_GAME(ServerBD, PlayerBD)
	ENDIF
	
	RESET_EVERYTHING(nightClubStruct)
	#IF IS_DEBUG_BUILD
		InitForEditor()
	#ENDIF
	
	#IF IS_DEBUG_BUILD
		CREATE_NIGHT_CLUB_WIDGET(nightClubStruct, ServerBD, PlayerBD)
	#ENDIF
	g_eCurrentPlayingDJ = DJ_NULL
	WHILE TRUE
		IF ShouldDoWaitZero() // if we have changed state we want to call RUN_PEDS_UPDATE again
			WAIT(0) 
		ELSE
			CDEBUG1LN(DEBUG_NET_PED_DANCING, "skipping wait")
			g_ncLastState = g_ncState
		ENDIF
		
		RUN_PEDS_UPDATE(nightClubStruct, ServerBD, PlayerBD)
		
		MAINTAIN_NIGHTCLUB_OWNER_AS_HOST()
	ENDWHILE
	

ENDSCRIPT
