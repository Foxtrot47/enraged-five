//AM_PROSTITUTE
//Prostitutes- now in freemode!
//Author: Neil Beggs
//Date: 17/09/2012
USING "globals.sch"

// Game Headers
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_brains.sch"

// Network Headers
USING "net_include.sch"
USING "net_rank_ui.sch"
USING "net_hud_displays.sch"
USING "net_weaponswap.sch"
USING "minigame_uiinputs.sch"
USING "freemode_header.sch"
USING "FM_Unlocks_Header.sch"
USING "context_control_public.sch"
USING "net_wait_zero.sch"

#IF IS_DEBUG_BUILD
USING "net_debug_log.sch"
#ENDIF

ENUM MP_PROSTITUTE_PED_BRAIN_LAUNCHER_STATES
	MPPRO_LAUNCH_STATE_FIND_CLOSEST_PRO,
	MPPRO_LAUNCH_STATE_PB_RUNNING,
	MPPRO_LAUNCH_STATE_SHUT_DOWN_PB,
	MPPRO_LAUNCH_STATE_LAUNCH_PB,
	MPPRO_LAUNCH_STATE_KILL_THREAD,
	MPPRO_LAUNCH_STATE_ON_MISSION
ENDENUM

MP_PROSTITUTE_PED_BRAIN_LAUNCHER_STATES	eMPPRO_LAUNCH_STATE = MPPRO_LAUNCH_STATE_FIND_CLOSEST_PRO
INT iUpdateTime
INT iFrameCount	= 0
INT iPlayerProsBitfield = 0

BOOL		bAnimsRequested = FALSE
CONST_INT 	k_iUpdateTimeLong	500
CONST_INT 	k_iUpdateTimeMid	250
CONST_INT 	k_iUpdateTimeShort	0
CONST_INT	k_iAnimRequestDist	22500 // 150*150
CONST_INT	k_fDistPlayer2Call	11

THREADID currentProsThread = NULL

STRUCT MP_PROSTITUTE_PLAYER_BROADCAST
	PED_INDEX piProWithPB
	NETWORK_INDEX niProWithPB
	BOOL bProActive
ENDSTRUCT

MP_PROSTITUTE_PLAYER_BROADCAST	sPlayerBD[NUM_NETWORK_PLAYERS]

PROC BROADCAST_KILL_EVENT()

	STRUCT_EVENT_COMMON_DETAILS Details
	
	Details.Type = SCRIPT_EVENT_MP_PROSTITUTION_KILL_THREAD
	Details.FromPlayerIndex = PLAYER_ID()
	
	INT iSendToPlayersFlag = ALL_PLAYERS(TRUE, TRUE)
	
	IF iSendToPlayersFlag != 0
		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Details, SIZE_OF(Details), iSendToPlayersFlag)
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Do necessary pre game start ini.
PROC PROCESS_PRE_GAME(MP_MISSION_DATA missionScriptArgs)
	
	// This marks the script as a net script, and handles any instancing setup. 
	SETUP_MP_MISSION_FOR_NETWORK(GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(missionScriptArgs.mdID.idMission) , missionScriptArgs)
	
	// This makes sure the net script is active, waits until it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES( sPlayerBD, SIZE_OF( sPlayerBD ) )
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		BROADCAST_KILL_EVENT()
	ELSE
		FORCE_CLEANUP_FOR_ALL_THREADS_WITH_THIS_NAME( "pb_prostitute" )
	ENDIF
	
	// This script will not be paused if another script calls PAUSE_GAME
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
ENDPROC

FUNC BOOL TERMINATE_PROSTITUTE_SCRIPT()
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH( HASH("pb_prostitute") ) <= 0
	AND NOT NETWORK_IS_SCRIPT_ACTIVE("pb_prostitute", NETWORK_PLAYER_ID_TO_INT(), TRUE)
		RETURN TRUE
	ENDIF
	IF currentProsThread = NULL
		RETURN TRUE
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		BROADCAST_KILL_EVENT()
	ELSE
		FORCE_CLEANUP_FOR_ALL_THREADS_WITH_THIS_NAME( "pb_prostitute" )
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CLEANUP_SCRIPT()
	
	IF bAnimsRequested
		REMOVE_ANIM_DICT("mini@prostitutes@sexlow_veh")
		REMOVE_ANIM_DICT("mini@prostitutes@sexnorm_veh")
		REMOVE_ANIM_DICT("mini@prostitutes@sexlow_veh_first_person")
		REMOVE_ANIM_DICT("mini@prostitutes@sexnorm_veh_first_person")
		REMOVE_ANIM_DICT("anim@mini@prostitutes@sex@veh_vstr@")
	ENDIF
	
	IF !IS_PLAYER_USING_RC_TANK(PLAYER_ID())
	AND !IS_PLAYER_USING_RCBANDITO(PLAYER_ID())
	#IF FEATURE_DLC_1_2022
	AND !IS_PLAYER_TEST_DRIVING_A_VEHICLE(PLAYER_ID())
	#ENDIF //	#IF FEATURE_DLC_1_2022
	
		ENABLE_INTERACTION_MENU()
	ENDIF
	
	TERMINATE_PROSTITUTE_SCRIPT()
	IF NETWORK_IS_GAME_IN_PROGRESS()
		BROADCAST_KILL_EVENT()
	ELSE
		FORCE_CLEANUP_FOR_ALL_THREADS_WITH_THIS_NAME( "pb_prostitute" )
	ENDIF
	CPRINTLN(DEBUG_PROSTITUTE, "Killing AM_PROSTITUTE")
	MPGlobalsAmbience.bInitAMProstituteLaunch = FALSE
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

FUNC PED_INDEX GET_PRO_PED( INT i = -1 )

	IF i = -1
		RETURN sPlayerBD[NETWORK_PLAYER_ID_TO_INT()].piProWithPB
	ENDIF
			
	RETURN sPlayerBD[i].piProWithPB 
ENDFUNC

PROC SET_PRO_PED( PED_INDEX pedPro )
	sPlayerBD[NETWORK_PLAYER_ID_TO_INT()].piProWithPB = pedPro
ENDPROC

FUNC BOOL IS_PRO_ALREADY_STOLEN( PED_INDEX pedPro )
	INT i = 0
	FOR i = 0 TO NUM_NETWORK_PLAYERS - 1
		IF sPlayerBD[i].piProWithPB = pedPro
			RETURN TRUE
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PROSTITUTE_VALID(PED_INDEX pedCheck)
	
	INT iScriptInstance
	
	STRING sScript
	
	sScript = GET_ENTITY_SCRIPT(pedCheck, iScriptInstance)
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sScript)
		IF ARE_STRINGS_EQUAL(sScript, "GB_VEHICLE_EXPORT")
			CDEBUG3LN(DEBUG_PROSTITUTE, "[CAN_BE_A_PROSTITUTE] Ped can not be a prostitute - created by GB_VEHICLE_EXPORT, scriptInstance", iScriptInstance)
			RETURN FALSE
		ENDIF
		
		IF ARE_STRINGS_EQUAL(sScript, "BUSINESS_BATTLES_SELL")
			CDEBUG3LN(DEBUG_PROSTITUTE, "[CAN_BE_A_PROSTITUTE] Ped can not be a prostitute - created by BUSINESS_BATTLES_SELL, scriptInstance", iScriptInstance)
			RETURN FALSE
		ENDIF
		
		IF ARE_STRINGS_EQUAL(sScript, "BUSINESS_BATTLES")
			CDEBUG3LN(DEBUG_PROSTITUTE, "[CAN_BE_A_PROSTITUTE] Ped can not be a prostitute - created by BUSINESS_BATTLES, scriptInstance", iScriptInstance)
			RETURN FALSE
		ENDIF
		
		IF ARE_STRINGS_EQUAL(sScript, "GB_CASINO")
			CDEBUG3LN(DEBUG_PROSTITUTE, "[CAN_BE_A_PROSTITUTE] Ped can not be a prostitute - created by GB_CASINO, scriptInstance", iScriptInstance)
			RETURN FALSE
		ENDIF
		
		IF ARE_STRINGS_EQUAL(sScript, "GB_CASINO_HEIST")
			CDEBUG3LN(DEBUG_PROSTITUTE, "[CAN_BE_A_PROSTITUTE] Ped can not be a prostitute - created by GB_CASINO_HEIST, scriptInstance", iScriptInstance)
			RETURN FALSE
		ENDIF
		
		#IF FEATURE_COPS_N_CROOKS
		IF ARE_STRINGS_EQUAL(sScript, "FM_CONTENT_DISPATCH")
			CDEBUG3LN(DEBUG_PROSTITUTE, "[CAN_BE_A_PROSTITUTE] Ped can not be a prostitute - created by FM_CONTENT_DISPATCH, scriptInstance", iScriptInstance)
			RETURN FALSE
		ENDIF
		#ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC PED_INDEX FIND_CLOSEST_PRO_FROM_AMBIENT()
	
	PED_INDEX retVal = NULL
	
	PED_INDEX nearbyPedsArray[8]
	INT iNumOfNearPeds = GET_PED_NEARBY_PEDS(PLAYER_PED_ID(), nearbyPedsArray)
		
	IF iNumOfNearPeds > 0
		INT i
		FOR i = 0 TO 7
			IF NOT IS_PED_INJURED(nearbyPedsArray[i])
				IF IS_PED_USING_SCENARIO(nearbyPedsArray[i], "WORLD_HUMAN_PROSTITUTE_HIGH_CLASS")
				OR IS_PED_USING_SCENARIO(nearbyPedsArray[i], "WORLD_HUMAN_PROSTITUTE_LOW_CLASS")
					IF IS_PROSTITUTE_VALID(nearbyPedsArray[i])
						IF NOT IS_PRO_ALREADY_STOLEN( nearbyPedsArray[i] )
							IF NOT IS_PED_FLEEING(nearbyPedsArray[i])																		
								PRINTLN("FOUND A PROSTITUTE")	
								RETURN nearbyPedsArray[i]
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	
	RETURN retVal
		
ENDFUNC

FUNC PED_INDEX FIND_CLOSEST_PRO()
	PED_INDEX retVal = FIND_CLOSEST_PRO_FROM_AMBIENT()
	INT iPlayerID = NETWORK_PLAYER_ID_TO_INT()

	IF NOT DOES_ENTITY_EXIST( GET_PRO_PED( iPlayerID ) )
		CDEBUG3LN(DEBUG_PROSTITUTE, "[FIND_CLOSEST_PRO] sPlayerBD[iPlayerID].piProWithPB is Null, returning retVal")
		SET_PRO_PED( NULL )
		RETURN retVal
	ENDIF
		
	// Checks to see if the prostitute from the previous ped brain launch is in the car.  If she is, she's definitely the closest.
	IF currentProsThread != NULL AND IS_THREAD_ACTIVE( currentProsThread )
		IF sPlayerBD[iPlayerID].bProActive
			RETURN GET_PRO_PED( iPlayerID )
		ENDIF
		
		IF DOES_ENTITY_EXIST( PLAYER_PED_ID() )
			IF IS_PED_IN_ANY_VEHICLE( PLAYER_PED_ID() )
				PED_INDEX piPlayer = PLAYER_PED_ID()
				IF IS_PED_DRIVING_ANY_VEHICLE(piPlayer)
					VEHICLE_INDEX viPlayerVeh = GET_VEHICLE_PED_IS_IN( PLAYER_PED_ID() )
					IF DOES_ENTITY_EXIST( viPlayerVeh )
						IF GET_PRO_PED( iPlayerID ) = GET_PED_IN_VEHICLE_SEAT(viPlayerVeh, VS_FRONT_RIGHT)
							OR IS_PED_IN_VEHICLE( GET_PRO_PED( iPlayerID ), viPlayerVeh, TRUE )
							CDEBUG3LN(DEBUG_PROSTITUTE, "[FIND_CLOSEST_PRO] Pro in vehicle")
							sPlayerBD[iPlayerID].bProActive = TRUE
							RETURN GET_PRO_PED( iPlayerID )
						ENDIF			
					ENDIF
					
					IF retVal = NULL
						CDEBUG3LN(DEBUG_PROSTITUTE, "[FIND_CLOSEST_PRO] No one is closer")
						RETURN GET_PRO_PED( iPlayerID )
					ENDIF
					
					VECTOR v_piProWithPB = GET_ENTITY_COORDS( GET_PRO_PED( iPlayerID ), FALSE )
					VECTOR v_retVal = GET_ENTITY_COORDS( retVal, FALSE )
					VECTOR v_player = GET_ENTITY_COORDS( PLAYER_PED_ID(), FALSE )
					FLOAT f_piProWithPB = VDIST( v_piProWithPB, v_player )
					FLOAT f_retVal = VDIST( v_player, v_retVal )
					IF f_piProWithPB > f_retVal
						IF ( f_piProWithPB - f_retVal ) > 25.0
							CDEBUG3LN(DEBUG_PROSTITUTE, "[FIND_CLOSEST_PRO] Distance f_piProWithPB - f_retVal = ", (f_piProWithPB - f_retVal), ", returning retVal.")	
							RETURN retVal
						ELIF f_retVal < 6 AND f_piProWithPB > k_fDistPlayer2Call
							CDEBUG3LN(DEBUG_PROSTITUTE, "[FIND_CLOSEST_PRO] Distance f_piProWithPB = ", f_piProWithPB, " f_retVal = ", f_retVal, ", returning retVal.")	
							RETURN retVal						
						ELSE
							CDEBUG3LN(DEBUG_PROSTITUTE, "[FIND_CLOSEST_PRO] Original still within threshold")	
							RETURN GET_PRO_PED( iPlayerID )
						ENDIF
					ELSE
						CDEBUG3LN(DEBUG_PROSTITUTE, "[FIND_CLOSEST_PRO] Original is closer")
						RETURN GET_PRO_PED( iPlayerID )
					ENDIF	
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	CDEBUG3LN(DEBUG_PROSTITUTE, "[FIND_CLOSEST_PRO] Thread not active")	
	RETURN retVal

ENDFUNC

PROC UPDATE_SEARCH_NEARBY_SERVICED_PLAYERS()	
	INT iIndex
	INT iPlayerID = NETWORK_PLAYER_ID_TO_INT()
	PED_INDEX piPlayer
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iIndex
		IF iFrameCount % iIndex = 0
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iIndex))
				piPlayer = GET_PLAYER_PED( NETWORK_GET_PLAYER_INDEX( INT_TO_PARTICIPANTINDEX(iIndex) ) )
				
				IF NOT IS_ENTITY_DEAD(piPlayer)
					IF sPlayerBD[iIndex].bProActive
						IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_ENTITY_COORDS(piPlayer)) <= k_iAnimRequestDist					
							IF NOT bAnimsRequested
								REQUEST_ANIM_DICT("mini@prostitutes@sexlow_veh")
								REQUEST_ANIM_DICT("mini@prostitutes@sexnorm_veh")
								REQUEST_ANIM_DICT("mini@prostitutes@sexlow_veh_first_person")
								REQUEST_ANIM_DICT("mini@prostitutes@sexnorm_veh_first_person")
								REQUEST_ANIM_DICT("anim@mini@prostitutes@sex@veh_vstr@")
								bAnimsRequested = TRUE
							ENDIF
							IF NOT IS_BIT_SET(iPlayerProsBitfield, iIndex)
								SET_BIT(iPlayerProsBitfield, iIndex)
							ENDIF
						ELIF IS_BIT_SET(iPlayerProsBitfield, iIndex)
							CLEAR_BIT(iPlayerProsBitfield, iIndex)
						ENDIF
					ELIF IS_BIT_SET(iPlayerProsBitfield, iIndex)
						CLEAR_BIT(iPlayerProsBitfield, iIndex)
					ENDIF
				ELIF IS_BIT_SET(iPlayerProsBitfield, iIndex)
					CLEAR_BIT(iPlayerProsBitfield, iIndex)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT

	IF iPlayerProsBitfield = 0
		AND bAnimsRequested
		AND NOT sPlayerBD[iPlayerID].bProActive
		REMOVE_ANIM_DICT("mini@prostitutes@sexlow_veh")
		REMOVE_ANIM_DICT("mini@prostitutes@sexnorm_veh")
		REMOVE_ANIM_DICT("mini@prostitutes@sexlow_veh_first_person")
		REMOVE_ANIM_DICT("mini@prostitutes@sexnorm_veh_first_person")
		REMOVE_ANIM_DICT("anim@mini@prostitutes@sex@veh_vstr@")
		bAnimsRequested = FALSE
	ENDIF
ENDPROC

FUNC BOOL SHOULD_BLOCK_PROSTITUTES()
	IF IS_LOCAL_PLAYER_DELIVERING_BOUNTY()
		RETURN TRUE
	ENDIF
	
	IF GB_GET_MEGA_BUSINESS_VARIATION_PLAYER_IS_ON(PLAYER_ID()) != MBV_INVALID
		RETURN TRUE
	ENDIF
	
	IF GB_IS_PLAYER_ON_GB_CASINO_MISSION(PLAYER_ID())
		RETURN TRUE
	ENDIF	
	
	RETURN FALSE
ENDFUNC

PROC UPDATE_MP_PROC_PB_LAUNCHER()

	IF IS_ENTITY_DEAD( PLAYER_PED_ID() )
		EXIT
	ENDIF
	
	IF iUpdateTime > GET_GAME_TIMER()  
		EXIT
	ENDIF
	
	PED_INDEX piClosestProThisFrame = NULL
	PED_INDEX piPlayer
	INT iPlayerID = NETWORK_PLAYER_ID_TO_INT()
	BOOL bIsDriving = FALSE
	
	IF DOES_ENTITY_EXIST( PLAYER_PED_ID() )
		IF IS_PED_IN_ANY_VEHICLE( PLAYER_PED_ID() )
			piPlayer = PLAYER_PED_ID()
			IF IS_PED_DRIVING_ANY_VEHICLE(piPlayer)
				bIsDriving = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	SWITCH eMPPRO_LAUNCH_STATE
		CASE MPPRO_LAUNCH_STATE_FIND_CLOSEST_PRO
			IF IS_PLAYER_ON_ANY_FM_MISSION( PLAYER_ID() )
			OR IS_FM_MISSION_LAUNCH_IN_PROGRESS()
			OR NOT bIsDriving
			OR SHOULD_BLOCK_PROSTITUTES()
				eMPPRO_LAUNCH_STATE = MPPRO_LAUNCH_STATE_KILL_THREAD
			ELSE
				piClosestProThisFrame = FIND_CLOSEST_PRO()
				IF piClosestProThisFrame != GET_PRO_PED( iPlayerID )
					AND piClosestProThisFrame != NULL
					SET_PRO_PED( piClosestProThisFrame )
					iUpdateTime = GET_GAME_TIMER() + k_iUpdateTimeShort
					eMPPRO_LAUNCH_STATE = MPPRO_LAUNCH_STATE_SHUT_DOWN_PB
					CPRINTLN(DEBUG_PROSTITUTE, "[AM_PROSTITUTE] Moving to MPPRO_LAUNCH_STATE_SHUT_DOWN_PB")	
				ELSE
					iUpdateTime = GET_GAME_TIMER() + k_iUpdateTimeLong
				ENDIF
			ENDIF
		BREAK
		CASE MPPRO_LAUNCH_STATE_SHUT_DOWN_PB
			IF TERMINATE_PROSTITUTE_SCRIPT()
				g_fDistToClosestProstitute = 0
				REQUEST_SCRIPT("pb_prostitute")
				iUpdateTime = GET_GAME_TIMER() + k_iUpdateTimeMid
				sPlayerBD[iPlayerID].bProActive = FALSE
				eMPPRO_LAUNCH_STATE = MPPRO_LAUNCH_STATE_LAUNCH_PB				
				CPRINTLN(DEBUG_PROSTITUTE, "[AM_PROSTITUTE] All pb_prostitutes shut down.  Number of pb_prostitute running = ", GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH( HASH("pb_prostitute") ) )
				CPRINTLN(DEBUG_PROSTITUTE, "[AM_PROSTITUTE] Moving to MPPRO_LAUNCH_STATE_LAUNCH_PB")
			ELSE
				iUpdateTime = GET_GAME_TIMER() + k_iUpdateTimeMid
			ENDIF
		BREAK
		CASE MPPRO_LAUNCH_STATE_LAUNCH_PB
			IF HAS_SCRIPT_LOADED("pb_prostitute")
			AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH( HASH("pb_prostitute") ) <= 0
			AND NOT NETWORK_IS_SCRIPT_ACTIVE("pb_prostitute", NETWORK_PLAYER_ID_TO_INT(), TRUE)
				PED_INDEX tempIndex
				tempIndex = GET_PRO_PED( iPlayerID )
				CPRINTLN(DEBUG_PROSTITUTE, "[AM_PROSTITUTE] About to launch new pb_prostitute.  Number of pb_prostitute running = ", GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH( HASH("pb_prostitute") ) )
				currentProsThread = START_NEW_SCRIPT_WITH_ARGS("pb_prostitute", tempIndex, SIZE_OF(tempIndex), FRIEND_STACK_SIZE)
				SET_SCRIPT_AS_NO_LONGER_NEEDED("pb_prostitute")
				eMPPRO_LAUNCH_STATE = MPPRO_LAUNCH_STATE_FIND_CLOSEST_PRO
				CPRINTLN(DEBUG_PROSTITUTE, "[AM_PROSTITUTE] Moving to MPPRO_LAUNCH_STATE_FIND_CLOSEST_PRO")
			ENDIF					
			iUpdateTime = GET_GAME_TIMER() + k_iUpdateTimeShort
		BREAK
		CASE MPPRO_LAUNCH_STATE_KILL_THREAD
			IF TERMINATE_PROSTITUTE_SCRIPT()
				sPlayerBD[iPlayerID].bProActive = FALSE
				iUpdateTime = GET_GAME_TIMER() + k_iUpdateTimeMid
				eMPPRO_LAUNCH_STATE = MPPRO_LAUNCH_STATE_ON_MISSION
			ELSE
				iUpdateTime = GET_GAME_TIMER() + k_iUpdateTimeLong	
			ENDIF		
		BREAK
		CASE MPPRO_LAUNCH_STATE_ON_MISSION
			IF NOT IS_PLAYER_ON_ANY_FM_MISSION( PLAYER_ID() )
			AND NOT IS_FM_MISSION_LAUNCH_IN_PROGRESS()
			AND bIsDriving
			AND NOT IS_LOCAL_PLAYER_DELIVERING_BOUNTY()
				iUpdateTime = GET_GAME_TIMER() + k_iUpdateTimeMid
				eMPPRO_LAUNCH_STATE = MPPRO_LAUNCH_STATE_FIND_CLOSEST_PRO
			ELSE
				iUpdateTime = GET_GAME_TIMER() + k_iUpdateTimeLong	
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                      Main Loop                             ////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
SCRIPT(MP_MISSION_DATA missionScriptArgs) 
	
	// Carry out all the initial game starting duties. 
	IF NETWORK_IS_GAME_IN_PROGRESS()
		PROCESS_PRE_GAME(missionScriptArgs)
	ELSE
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "=== PROSTITUTE === LAUNCH FAIL TIME IS ", GET_CLOCK_HOURS())
		#ENDIF
		CLEANUP_SCRIPT()
	ENDIF
		
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_NET_AMBIENT, "=== PROSTITUTE === Started Script!")
	#ENDIF
	iUpdateTime = GET_GAME_TIMER() 
			
	// Main loop.		
	WHILE TRUE

		// One wait to rule them all. This can be the ONLY wait from here on in.....
		MP_LOOP_WAIT_ZERO() 
		
		// If we have a match end event, bail.
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		OR NETWORK_IS_IN_TUTORIAL_SESSION()
		OR (Is_Player_Currently_On_MP_Heist(PLAYER_ID())
		OR Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID()))
		OR IS_PLAYER_IN_BUSINESS_HUB(PLAYER_ID())
			CLEANUP_SCRIPT()
		ENDIF
		
		IF NETWORK_IS_GAME_IN_PROGRESS()	//is network game
			UPDATE_MP_PROC_PB_LAUNCHER()
			
			UPDATE_SEARCH_NEARBY_SERVICED_PLAYERS()
			++iFrameCount
			
			IF iFrameCount >= NETWORK_GET_MAX_NUM_PARTICIPANTS()
				iFrameCount = 0
			ENDIF
		ELSE
			CLEANUP_SCRIPT()
		ENDIF
		
	ENDWHILE
ENDSCRIPT
