
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// MISSION_PLACEHOLDER_VEHICLES.sch																						//
// Handles the creation and processing of non mission critical vehicles													//
// Written by:  Martin McMillan & Ryan Elliott.																			//
// Date: 		23/07/2019																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#IF IS_DEBUG_BUILD

//----------------------
//	INCLUDES
//----------------------

USING "rage_builtins.sch"
USING "globals.sch"

USING "MISSION_PLACEHOLDER_COMMON.sch"
USING "MISSION_PLACEHOLDER_AMBUSH.sch"
USING "MISSION_PLACEHOLDER_LAUNCHING.sch"

//////////////////////////////////////////
////  	CRITICAL ENTITY FUNCTIONS 	  ////
//////////////////////////////////////////  
FUNC BOOL SHOULD_VARIATION_HAVE_PASSENGER_SEAT_BOMBS()
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_MISSION_ALLOW_PASSENGER_CHAFF()
	
	IF SHOULD_VARIATION_HAVE_PASSENGER_SEAT_BOMBS()
		RETURN FALSE
	ENDIF

	RETURN FALSE
ENDFUNC

PROC SETUP_VEHICLE_MODS(VEHICLE_INDEX& vehId)
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			IF GET_NUM_MOD_KITS(vehId) > 0
				SET_VEHICLE_MOD_KIT(vehId, 0)
				
				IF GET_NUM_VEHICLE_MODS(vehId, MOD_CHASSIS) > 0
					SET_VEHICLE_MOD(vehId, MOD_CHASSIS, 0)
				ENDIF
				IF GET_NUM_VEHICLE_MODS(vehId, MOD_ROOF) > 0	// Machines Guns
					SET_VEHICLE_MOD(vehId, MOD_ROOF, 0)
				ENDIF
				IF GET_NUM_VEHICLE_MODS(vehId, MOD_EXHAUST) > 0	// JTOL
					SET_VEHICLE_MOD(vehId, MOD_EXHAUST, 0)
				ENDIF
			ENDIF
		BREAK	
	ENDSWITCH
ENDPROC

FUNC FLOAT GET_RANDOM_DIRT_LEVEL()

	FLOAT fReturn
	
	INT iRand = GET_RANDOM_INT_IN_RANGE(0, 10000)
	
	IF iRand > 8000
		fReturn = 15.00
	ELIF iRand > 4000
		fReturn = 10.00
	ELIF iRand > 2000
		fReturn = 7.50
	ELSE
		fReturn = 5.00
	ENDIF

	RETURN fReturn
ENDFUNC

FUNC FLOAT GET_MISSION_ENTITY_DAMAGE_SCALE(MODEL_NAMES eModel)
	
	#IF IS_DEBUG_BUILD
	IF MPGlobalsAmbience.sMagnateGangBossData.fWidgetCarrierDamageScale > 0.0
		PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - GET_MISSION_ENTITY_DAMAGE_SCALE - Using widget damage scale = ", MPGlobalsAmbience.sMagnateGangBossData.fWidgetCarrierDamageScale)
		RETURN MPGlobalsAmbience.sMagnateGangBossData.fWidgetCarrierDamageScale
	ENDIF
	#ENDIF
	
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			RETURN 1.0
	ENDSWITCH
	
	SWITCH eModel
		CASE INSURGENT3
			RETURN 1.0
	ENDSWITCH
	
	RETURN 0.2
ENDFUNC

FUNC BOOL SHOULD_ADJUST_DAMAGE_SCALE(MODEL_NAMES eModel)
	IF GET_MISSION_ENTITY_DAMAGE_SCALE(eModel) != 1.0
		RETURN TRUE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_PLANE_BE_UNBREAKABLE(MODEL_NAMES eModel)
	UNUSED_PARAMETER(eModel)
	
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			RETURN FALSE
	ENDSWITCH

	RETURN TRUE
ENDFUNC
			

/////////////////////////////////////////////////
////  			VEHICLE ATTRIBUTES 	 		 ////
/////////////////////////////////////////////////  

FUNC FLOAT GET_SUPPORT_DAMAGE_SCALE(MODEL_NAMES eModel)
	UNUSED_PARAMETER(eModel)
	
	SWITCH eModel
		CASE MOGUL		RETURN 0.5
	ENDSWITCH
	
	RETURN 0.3
ENDFUNC

PROC SET_SUPPORT_VEHICLE_COSMETIC_ATTRIBUTES(VEHICLE_INDEX vehId)
	UNUSED_PARAMETER(vehID)
ENDPROC

PROC SET_SUPPORT_VEHICLE_SPAWN_ATTRIBUTES(INT iVehicle, VEHICLE_INDEX vehId)
	MODEL_NAMES eModel = GET_ENTITY_MODEL(vehId)
	PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_SUPPORT_VEHICLE_SPAWN_ATTRIBUTES - Setting up support vehicle ", GET_SUPPORT_VEHICLE_INDEX(iVehicle), " with model ", GET_MODEL_NAME_FOR_DEBUG(eModel))
	
	// Damage Modifiers
	SET_VEHICLE_TYRES_CAN_BURST(vehId, FALSE)
	IF GET_SUPPORT_DAMAGE_SCALE(eModel) != 1.0
		SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE(vehId, FALSE)
		SET_VEHICLE_STRONG(vehId, TRUE)
		SET_VEHICLE_DAMAGE_SCALE(vehId, GET_SUPPORT_DAMAGE_SCALE(eModel))
		PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] SET_SUPPORT_VEHICLE_SPAWN_ATTRIBUTES - Set vehicle to take additional explosive damage. Damage scale = ", GET_SUPPORT_DAMAGE_SCALE(eModel))
	ENDIF
	IF IS_THIS_MODEL_A_PLANE(eModel)
		SET_VEHICLE_CAN_BREAK(vehId, FALSE)
	ENDIF
		
	// Cosmetic
	SET_SUPPORT_VEHICLE_COSMETIC_ATTRIBUTES(vehID)
	
	// Lock state
	SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(vehId, TRUE)
	SET_VEHICLE_RESPECTS_LOCKS_WHEN_HAS_DRIVER(vehId, TRUE)
	SET_VEHICLE_DOORS_LOCKED_FOR_NON_SCRIPT_PLAYERS(vehID, TRUE)
	
	// Mission specific support vehicle attributes
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX	
		BREAK
	ENDSWITCH

	UNUSED_PARAMETER(iVehicle)
ENDPROC

FUNC BOOL SHOULD_LOCK_CARRIER_VEHICLE_ON_SPAWN()
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			RETURN TRUE
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_CARRIER_VEHICLE_NEED_HOTWIRED()
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_CARRIER_VEHICLE_NEED_BROKEN_INTO(MODEL_NAMES eModel)
	
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			RETURN TRUE
	ENDSWITCH
	
	SWITCH eModel
		CASE HALFTRACK
		CASE INSURGENT3
			RETURN FALSE
	ENDSWITCH

	RETURN FALSE

ENDFUNC

FUNC BOOL SHOULD_LOCK_CARRIER_VEHICLE_REAR_DOORS(INT iVehicle)
	UNUSED_PARAMETER(iVehicle)
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_SET_CARRIER_VEHICLE_STRONG()
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_SET_CARRIER_VEHICLE_STRONG_TYRES()
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_SET_CARRIER_VEHICLE_MP_DAMAGE()
	RETURN TRUE
ENDFUNC

FUNC BOOL PREVENT_EXPLODE_VEHICLE_ON_BODY_DMG()
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_VEHICLE_SETUP_MP_SET_LOCK_STATUS(INT iVehicle)
	UNUSED_PARAMETER(iVehicle)
	
	RETURN FALSE
ENDFUNC

PROC SET_FMP_DECORATOR_ON_VEHICLE(VEHICLE_INDEX vehId)

	IF DECOR_IS_REGISTERED_AS_TYPE("CSHVehicle", DECOR_TYPE_BOOL)
		DECOR_SET_BOOL(vehID, "CSHVehicle", TRUE)
		PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_FMP_DECORATOR_ON_VEHICLE")
	ENDIF
		
ENDPROC

FUNC BOOL SHOULD_BLOCK_ALLOWING_ENTRY_IF_LOCKED()
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			RETURN FALSE
	ENDSWITCH

	RETURN TRUE
ENDFUNC

PROC SETUP_GETAWAY_VEHICLE_COSMETICS(INT iVehicle, VEHICLE_INDEX vehId)

	IF GET_MISSION_ENTITY_VEHICLE_IS_CARRYING(iVehicle) = -1
		EXIT
	ENDIF
	
	INT iIndex = GET_MISSION_ENTITY_VEHICLE_IS_CARRYING(iVehicle)
	
	MODEL_NAMES vehModel = GET_ENTITY_MODEL(vehID)
	
	VEHICLE_SETUP_STRUCT_MP sData
	sData.VehicleSetup.eModel = DUMMY_MODEL_FOR_SCRIPT
	
	SWITCH vehModel
		CASE ISSI3
			SWITCH iIndex
				CASE 0
					sData.VehicleSetup.eModel = ISSI3
					sData.VehicleSetup.tlPlateText = "83OSX036"
					sData.VehicleSetup.iPlateIndex = 4
					sData.VehicleSetup.iColour1 = 29
					sData.VehicleSetup.iColour2 = 111
					sData.VehicleSetup.iColourExtra1 = 7
					sData.VehicleSetup.iColourExtra2 = 122
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWindowTintColour = 3
					sData.VehicleSetup.iWheelType = 1
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
					sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
					sData.VehicleSetup.iModIndex[MOD_GRILL] = 2
					sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
					sData.VehicleSetup.iModIndex[MOD_WHEELS] = 1
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 4
				BREAK
				CASE 1
					sData.VehicleSetup.eModel = ISSI3
					sData.VehicleSetup.tlPlateText = "87LWG803"
					sData.VehicleSetup.iPlateIndex = 4
					sData.VehicleSetup.iColour1 = 73
					sData.VehicleSetup.iColour2 = 111
					sData.VehicleSetup.iColourExtra1 = 0
					sData.VehicleSetup.iColourExtra2 = 122
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWindowTintColour = 3
					sData.VehicleSetup.iWheelType = 1
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
					sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
					sData.VehicleSetup.iModIndex[MOD_GRILL] = 2
					sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
					sData.VehicleSetup.iModIndex[MOD_WHEELS] = 1
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 4
				BREAK
			ENDSWITCH
		BREAK
		CASE ASBO
			SWITCH iIndex
				CASE 0
					sData.VehicleSetup.eModel = ASBO
					sData.VehicleSetup.tlPlateText = "47BQT444"
					sData.VehicleSetup.iColour1 = 67
					sData.VehicleSetup.iColour2 = 67
					sData.VehicleSetup.iColourExtra1 = 67
					sData.VehicleSetup.iColourExtra2 = 156
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWheelType = 7
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					sData.VehicleSetup.iModIndex[MOD_SPOILER] = 7
					sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 3
					sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
					sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 3
					sData.VehicleSetup.iModIndex[MOD_BONNET] = 5
					sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
					sData.VehicleSetup.iModIndex[MOD_ROOF] = 6
					sData.VehicleSetup.iModIndex[MOD_WHEELS] = 5
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 6
				BREAK
				CASE 1
					sData.VehicleSetup.eModel = ASBO
					sData.VehicleSetup.tlPlateText = "03QPK213"
					sData.VehicleSetup.iColour1 = 92
					sData.VehicleSetup.iColour2 = 92
					sData.VehicleSetup.iColourExtra1 = 92
					sData.VehicleSetup.iColourExtra2 = 156
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWindowTintColour = 1
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					sData.VehicleSetup.iModIndex[MOD_SPOILER] = 15
					sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 5
					sData.VehicleSetup.iModIndex[MOD_SKIRT] = 10
					sData.VehicleSetup.iModIndex[MOD_GRILL] = 2
					sData.VehicleSetup.iModIndex[MOD_BONNET] = 5
					sData.VehicleSetup.iModIndex[MOD_WING_L] = 4
					sData.VehicleSetup.iModIndex[MOD_WHEELS] = 46
					sData.VehicleSetup.iModVariation[0] = 1
				BREAK
			ENDSWITCH
		BREAK
		CASE KANJO
			SWITCH iIndex
				CASE 0
					sData.VehicleSetup.eModel = KANJO
					sData.VehicleSetup.tlPlateText = "04DZM900"
					sData.VehicleSetup.iPlateIndex = 4
					sData.VehicleSetup.iColour1 = 136
					sData.VehicleSetup.iColour2 = 136
					sData.VehicleSetup.iColourExtra1 = 5
					sData.VehicleSetup.iColourExtra2 = 156
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWheelType = 5
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					sData.VehicleSetup.iModIndex[MOD_SPOILER] = 2
					sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 4
					sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 3
					sData.VehicleSetup.iModIndex[MOD_SKIRT] = 4
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 9
					sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 2
					sData.VehicleSetup.iModIndex[MOD_GRILL] = 5
					sData.VehicleSetup.iModIndex[MOD_BONNET] = 9
					sData.VehicleSetup.iModIndex[MOD_WING_R] = 1
					sData.VehicleSetup.iModIndex[MOD_WHEELS] = 5
					sData.VehicleSetup.iModIndex[MOD_INTERIOR1] = 1
					sData.VehicleSetup.iModIndex[MOD_CHASSIS2] = 2
					sData.VehicleSetup.iModIndex[MOD_DOOR_L] = 1
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 6
					sData.VehicleSetup.iModVariation[0] = 1
				BREAK
				CASE 1
					sData.VehicleSetup.eModel = KANJO
					sData.VehicleSetup.tlPlateText = "65LNN730"
					sData.VehicleSetup.iColour1 = 89
					sData.VehicleSetup.iColour2 = 89
					sData.VehicleSetup.iColourExtra1 = 88
					sData.VehicleSetup.iColourExtra2 = 0
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWheelType = 1
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					sData.VehicleSetup.iModIndex[MOD_SPOILER] = 8
					sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 7
					sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 7
					sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
					sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 6
					sData.VehicleSetup.iModIndex[MOD_GRILL] = 2
					sData.VehicleSetup.iModIndex[MOD_WING_R] = 1
					sData.VehicleSetup.iModIndex[MOD_WHEELS] = 14
					sData.VehicleSetup.iModIndex[MOD_INTERIOR1] = 1
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 1
				BREAK
			ENDSWITCH
		BREAK
		CASE SENTINEL3
			SWITCH iIndex
				CASE 0
					sData.VehicleSetup.eModel = sentinel3
					sData.VehicleSetup.tlPlateText = "88WFR722"
					sData.VehicleSetup.iColour1 = 111
					sData.VehicleSetup.iColour2 = 29
					sData.VehicleSetup.iColourExtra1 = 145
					sData.VehicleSetup.iColourExtra2 = 156
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWindowTintColour = 3
					sData.VehicleSetup.iWheelType = 5
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					sData.VehicleSetup.iModIndex[MOD_SPOILER] = 7
					sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 6
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
					sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 5
					sData.VehicleSetup.iModIndex[MOD_GRILL] = 4
					sData.VehicleSetup.iModIndex[MOD_BONNET] = 8
					sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
					sData.VehicleSetup.iModIndex[MOD_WHEELS] = 2
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 7
					sData.VehicleSetup.iModVariation[0] = 1
				BREAK
				CASE 1
					sData.VehicleSetup.eModel = sentinel3
					sData.VehicleSetup.tlPlateText = "63CMS702"
					sData.VehicleSetup.iPlateIndex = 1
					sData.VehicleSetup.iColour1 = 92
					sData.VehicleSetup.iColour2 = 92
					sData.VehicleSetup.iColourExtra1 = 0
					sData.VehicleSetup.iColourExtra2 = 0
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWindowTintColour = 1
					sData.VehicleSetup.iWheelType = 5
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					sData.VehicleSetup.iModIndex[MOD_SPOILER] = 14
					sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 9
					sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
					sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 4
					sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
					sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
					sData.VehicleSetup.iModIndex[MOD_BONNET] = 12
					sData.VehicleSetup.iModIndex[MOD_ROOF] = 13
					sData.VehicleSetup.iModIndex[MOD_WHEELS] = 6
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 6
					sData.VehicleSetup.iModVariation[0] = 1
				BREAK
			ENDSWITCH
		BREAK
		
		CASE RETINUE2
			SWITCH iIndex
				CASE 0
					sData.VehicleSetup.eModel = RETINUE2
					sData.VehicleSetup.tlPlateText = "23GVN817"
					sData.VehicleSetup.iPlateIndex = 1
					sData.VehicleSetup.iColour1 = 29
					sData.VehicleSetup.iColour2 = 111
					sData.VehicleSetup.iColourExtra1 = 138
					sData.VehicleSetup.iColourExtra2 = 0
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWheelType = 5
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					sData.VehicleSetup.iModIndex[MOD_SPOILER] = 2
					sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 5
					sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
					sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 2
					sData.VehicleSetup.iModIndex[MOD_GRILL] = 2
					sData.VehicleSetup.iModIndex[MOD_BONNET] = 7
					sData.VehicleSetup.iModIndex[MOD_WING_L] = 3
					sData.VehicleSetup.iModIndex[MOD_WING_R] = 2
					sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
					sData.VehicleSetup.iModIndex[MOD_WHEELS] = 22
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 8
					sData.VehicleSetup.iModVariation[0] = 1
				BREAK
				CASE 1
					sData.VehicleSetup.eModel = RETINUE2
					sData.VehicleSetup.tlPlateText = "86ACK549"
					sData.VehicleSetup.iPlateIndex = 4
					sData.VehicleSetup.iColour1 = 12
					sData.VehicleSetup.iColour2 = 39
					sData.VehicleSetup.iColourExtra1 = 0
					sData.VehicleSetup.iColourExtra2 = 122
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWheelType = 5
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					sData.VehicleSetup.iModIndex[MOD_SPOILER] = 5
					sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 6
					sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
					sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 3
					sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
					sData.VehicleSetup.iModIndex[MOD_BONNET] = 5
					sData.VehicleSetup.iModIndex[MOD_WING_L] = 5
					sData.VehicleSetup.iModIndex[MOD_ROOF] = 2
					sData.VehicleSetup.iModIndex[MOD_WHEELS] = 23
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 3
				BREAK
			ENDSWITCH
		BREAK
		CASE YOSEMITE2
			SWITCH iIndex
				CASE 0
					sData.VehicleSetup.eModel = YOSEMITE2
					sData.VehicleSetup.tlPlateText = "60UYD615"
					sData.VehicleSetup.iColour1 = 34
					sData.VehicleSetup.iColour2 = 1
					sData.VehicleSetup.iColourExtra1 = 135
					sData.VehicleSetup.iColourExtra2 = 0
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWindowTintColour = 1
					sData.VehicleSetup.iWheelType = 1
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					sData.VehicleSetup.iModIndex[MOD_SPOILER] = 8
					sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 11
					sData.VehicleSetup.iModIndex[MOD_SKIRT] = 5
					sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 3
					sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
					sData.VehicleSetup.iModIndex[MOD_BONNET] = 4
					sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 3
				BREAK
				CASE 1
					sData.VehicleSetup.eModel = YOSEMITE2
					sData.VehicleSetup.tlPlateText = "05SKJ131"
					sData.VehicleSetup.iPlateIndex = 1
					sData.VehicleSetup.iColour1 = 91
					sData.VehicleSetup.iColour2 = 1
					sData.VehicleSetup.iColourExtra1 = 0
					sData.VehicleSetup.iColourExtra2 = 0
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWheelType = 1
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
					sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 2
					sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
					sData.VehicleSetup.iModIndex[MOD_SKIRT] = 4
					sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 2
					sData.VehicleSetup.iModIndex[MOD_GRILL] = 4
					sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 4
				BREAK
			ENDSWITCH
		BREAK
		CASE SUGOI
			SWITCH iIndex
				CASE 0
					sData.VehicleSetup.eModel = Sugoi
					sData.VehicleSetup.tlPlateText = "01SRP848"
					sData.VehicleSetup.iColour1 = 112
					sData.VehicleSetup.iColour2 = 12
					sData.VehicleSetup.iColourExtra1 = 0
					sData.VehicleSetup.iColourExtra2 = 156
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWindowTintColour = 1
					sData.VehicleSetup.iWheelType = 7
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					sData.VehicleSetup.iModIndex[MOD_SPOILER] = 6
					sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 9
					sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 5
					sData.VehicleSetup.iModIndex[MOD_SKIRT] = 2
					sData.VehicleSetup.iModIndex[MOD_GRILL] = 7
					sData.VehicleSetup.iModIndex[MOD_BONNET] = 8
					sData.VehicleSetup.iModIndex[MOD_WING_L] = 4
					sData.VehicleSetup.iModIndex[MOD_ROOF] = 3
					sData.VehicleSetup.iModIndex[MOD_WHEELS] = 22
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 4
					sData.VehicleSetup.iModVariation[0] = 1
				BREAK
				CASE 1
					sData.VehicleSetup.eModel = Sugoi
					sData.VehicleSetup.tlPlateText = "28OKO870"
					sData.VehicleSetup.iPlateIndex = 1
					sData.VehicleSetup.iColour1 = 74
					sData.VehicleSetup.iColour2 = 74
					sData.VehicleSetup.iColourExtra1 = 74
					sData.VehicleSetup.iColourExtra2 = 122
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWheelType = 5
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					sData.VehicleSetup.iModIndex[MOD_SPOILER] = 3
					sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 9
					sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 6
					sData.VehicleSetup.iModIndex[MOD_SKIRT] = 7
					sData.VehicleSetup.iModIndex[MOD_GRILL] = 5
					sData.VehicleSetup.iModIndex[MOD_BONNET] = 11
					sData.VehicleSetup.iModIndex[MOD_ROOF] = 4
					sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
					sData.VehicleSetup.iModIndex[MOD_WHEELS] = 9
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 8
					sData.VehicleSetup.iModVariation[0] = 1
				BREAK
			ENDSWITCH
		BREAK
		CASE JUGULAR
			SWITCH iIndex
				CASE 0
					sData.VehicleSetup.eModel = JUGULAR
					sData.VehicleSetup.tlPlateText = "82ATK858"
					sData.VehicleSetup.iColour1 = 5
					sData.VehicleSetup.iColour2 = 0
					sData.VehicleSetup.iColourExtra1 = 111
					sData.VehicleSetup.iColourExtra2 = 111
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					sData.VehicleSetup.iModIndex[MOD_SPOILER] = 8
					sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 3
					sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 2
					sData.VehicleSetup.iModIndex[MOD_SKIRT] = 4
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
					sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
					sData.VehicleSetup.iModIndex[MOD_BONNET] = 5
					sData.VehicleSetup.iModIndex[MOD_WHEELS] = 24
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 6
					sData.VehicleSetup.iModVariation[0] = 1
				BREAK
				CASE 1
					sData.VehicleSetup.eModel = JUGULAR
					sData.VehicleSetup.tlPlateText = "63LUV750"
					sData.VehicleSetup.iPlateIndex = 1
					sData.VehicleSetup.iColour1 = 38
					sData.VehicleSetup.iColour2 = 38
					sData.VehicleSetup.iColourExtra1 = 37
					sData.VehicleSetup.iColourExtra2 = 156
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					sData.VehicleSetup.iModIndex[MOD_SPOILER] = 2
					sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
					sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
					sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
					sData.VehicleSetup.iModIndex[MOD_GRILL] = 3
					sData.VehicleSetup.iModIndex[MOD_BONNET] = 2
					sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
					sData.VehicleSetup.iModIndex[MOD_WHEELS] = 17
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 1
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SULTAN2
			SWITCH iIndex
				CASE 0
					sData.VehicleSetup.eModel = SULTAN2
					sData.VehicleSetup.tlPlateText = "28AAK928"
					sData.VehicleSetup.iColour1 = 112
					sData.VehicleSetup.iColour2 = 112
					sData.VehicleSetup.iColourExtra1 = 0
					sData.VehicleSetup.iColourExtra2 = 156
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWheelType = 5
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					sData.VehicleSetup.iModIndex[MOD_SPOILER] = 15
					sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 4
					sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 4
					sData.VehicleSetup.iModIndex[MOD_SKIRT] = 4
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 9
					sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 4
					sData.VehicleSetup.iModIndex[MOD_GRILL] = 3
					sData.VehicleSetup.iModIndex[MOD_BONNET] = 8
					sData.VehicleSetup.iModIndex[MOD_WING_R] = 2
					sData.VehicleSetup.iModIndex[MOD_WHEELS] = 24
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 8
					sData.VehicleSetup.iModVariation[0] = 1
				BREAK
				CASE 1
					sData.VehicleSetup.eModel = SULTAN2
					sData.VehicleSetup.tlPlateText = "68BPK026"
					sData.VehicleSetup.iColour1 = 68
					sData.VehicleSetup.iColour2 = 68
					sData.VehicleSetup.iColourExtra1 = 68
					sData.VehicleSetup.iColourExtra2 = 156
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 5
					sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 4
					sData.VehicleSetup.iModIndex[MOD_SKIRT] = 2
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 9
					sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 3
					sData.VehicleSetup.iModIndex[MOD_BONNET] = 5
					sData.VehicleSetup.iModIndex[MOD_WING_R] = 2
					sData.VehicleSetup.iModIndex[MOD_WHEELS] = 22
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 9
					sData.VehicleSetup.iModVariation[0] = 1
				BREAK
			ENDSWITCH
		BREAK
		CASE GAUNTLET3
			SWITCH iIndex
				CASE 0
					sData.VehicleSetup.eModel = GAUNTLET3
					sData.VehicleSetup.tlPlateText = "01JCP664"
					sData.VehicleSetup.iColour1 = 0
					sData.VehicleSetup.iColour2 = 13
					sData.VehicleSetup.iColourExtra1 = 134
					sData.VehicleSetup.iColourExtra2 = 13
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWheelType = 1
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
					sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 4
					sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 2
					sData.VehicleSetup.iModIndex[MOD_SKIRT] = 3
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 3
					sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 6
					sData.VehicleSetup.iModIndex[MOD_GRILL] = 5
					sData.VehicleSetup.iModIndex[MOD_BONNET] = 11
					sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
					sData.VehicleSetup.iModIndex[MOD_WING_R] = 2
					sData.VehicleSetup.iModIndex[MOD_ROOF] = 7
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 8
				BREAK
				CASE 1
					sData.VehicleSetup.eModel = GAUNTLET3
					sData.VehicleSetup.tlPlateText = "28OAE229"
					sData.VehicleSetup.iColour1 = 70
					sData.VehicleSetup.iColour2 = 13
					sData.VehicleSetup.iColourExtra1 = 70
					sData.VehicleSetup.iColourExtra2 = 156
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWindowTintColour = 3
					sData.VehicleSetup.iWheelType = 1
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
					sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 2
					sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
					sData.VehicleSetup.iModIndex[MOD_SKIRT] = 7
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 5
					sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 2
					sData.VehicleSetup.iModIndex[MOD_GRILL] = 2
					sData.VehicleSetup.iModIndex[MOD_BONNET] = 5
					sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
					sData.VehicleSetup.iModIndex[MOD_WING_R] = 2
					sData.VehicleSetup.iModIndex[MOD_ROOF] = 4
					sData.VehicleSetup.iModIndex[MOD_WHEELS] = 31
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 2
				BREAK
			ENDSWITCH
		BREAK
		CASE ELLIE
			SWITCH iIndex
				CASE 0
					sData.VehicleSetup.eModel = ELLIE
					sData.VehicleSetup.tlPlateText = "43TBV742"
					sData.VehicleSetup.iColour1 = 69
					sData.VehicleSetup.iColour2 = 111
					sData.VehicleSetup.iColourExtra1 = 74
					sData.VehicleSetup.iColourExtra2 = 156
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWindowTintColour = 2
					sData.VehicleSetup.iWheelType = 1
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 2
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 6
					sData.VehicleSetup.iModIndex[MOD_ROOF] = 4
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 9
				BREAK
				CASE 1
					sData.VehicleSetup.eModel = ELLIE
					sData.VehicleSetup.tlPlateText = "89FAZ643"
					sData.VehicleSetup.iPlateIndex = 1
					sData.VehicleSetup.iColour1 = 28
					sData.VehicleSetup.iColour2 = 111
					sData.VehicleSetup.iColourExtra1 = 0
					sData.VehicleSetup.iColourExtra2 = 0
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWheelType = 1
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					sData.VehicleSetup.iModIndex[MOD_ROOF] = 2
					sData.VehicleSetup.iModIndex[MOD_WHEELS] = 4
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 10
					sData.VehicleSetup.iModVariation[0] = 1
				BREAK
			ENDSWITCH
		BREAK
		CASE KOMODA
			SWITCH iIndex
				CASE 0
					sData.VehicleSetup.eModel = KOMODA
					sData.VehicleSetup.tlPlateText = "48OQX480"
					sData.VehicleSetup.iColour1 = 138
					sData.VehicleSetup.iColour2 = 111
					sData.VehicleSetup.iColourExtra1 = 89
					sData.VehicleSetup.iColourExtra2 = 0
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWheelType = 7
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					sData.VehicleSetup.iModIndex[MOD_SPOILER] = 24
					sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 12
					sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 4
					sData.VehicleSetup.iModIndex[MOD_SKIRT] = 12
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 4
					sData.VehicleSetup.iModIndex[MOD_BONNET] = 15
					sData.VehicleSetup.iModIndex[MOD_WING_L] = 2
					sData.VehicleSetup.iModIndex[MOD_WING_R] = 2
					sData.VehicleSetup.iModIndex[MOD_ROOF] = 10
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 5
				BREAK
				CASE 1
					sData.VehicleSetup.eModel = KOMODA
					sData.VehicleSetup.tlPlateText = "08PPT772"
					sData.VehicleSetup.iPlateIndex = 1
					sData.VehicleSetup.iColour1 = 112
					sData.VehicleSetup.iColour2 = 111
					sData.VehicleSetup.iColourExtra1 = 0
					sData.VehicleSetup.iColourExtra2 = 89
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWheelType = 7
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					sData.VehicleSetup.iModIndex[MOD_SPOILER] = 21
					sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
					sData.VehicleSetup.iModIndex[MOD_BONNET] = 10
					sData.VehicleSetup.iModIndex[MOD_WING_L] = 3
					sData.VehicleSetup.iModIndex[MOD_WING_R] = 1
					sData.VehicleSetup.iModIndex[MOD_ROOF] = 3
					sData.VehicleSetup.iModIndex[MOD_WHEELS] = 16
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 8
					sData.VehicleSetup.iModVariation[0] = 1
				BREAK
			ENDSWITCH
		BREAK
		
		CASE MANCHEZ
			SWITCH iIndex
				CASE 0
					sData.VehicleSetup.eModel = MANCHEZ
					sData.VehicleSetup.tlPlateText = "43HDN501"
					sData.VehicleSetup.iColour1 = 64
					sData.VehicleSetup.iColour2 = 111
					sData.VehicleSetup.iColourExtra1 = 68
					sData.VehicleSetup.iColourExtra2 = 156
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWheelType = 6
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 2
					sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 3
					sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 2
					sData.VehicleSetup.iModIndex[MOD_BONNET] = 1
					sData.VehicleSetup.iModIndex[MOD_ROOF] = 3
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 5
				BREAK
				CASE 1
					sData.VehicleSetup.eModel = MANCHEZ
					sData.VehicleSetup.tlPlateText = "27TIB223"
					sData.VehicleSetup.iColour1 = 92
					sData.VehicleSetup.iColour2 = 50
					sData.VehicleSetup.iColourExtra1 = 92
					sData.VehicleSetup.iColourExtra2 = 156
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWheelType = 6
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 3
					sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
					sData.VehicleSetup.iModIndex[MOD_ROOF] = 2
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 2
				BREAK
				CASE 2
					sData.VehicleSetup.eModel = MANCHEZ
					sData.VehicleSetup.tlPlateText = "42WKq996"
					sData.VehicleSetup.iColour1 = 29
					sData.VehicleSetup.iColour2 = 111
					sData.VehicleSetup.iColourExtra1 = 28
					sData.VehicleSetup.iColourExtra2 = 156
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWheelType = 6
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 3
					sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
					sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
					sData.VehicleSetup.iModIndex[MOD_BONNET] = 1
					sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 1
				BREAK
				CASE 3
					sData.VehicleSetup.eModel = MANCHEZ
					sData.VehicleSetup.tlPlateText = "06THT542"
					sData.VehicleSetup.iColour1 = 145
					sData.VehicleSetup.iColour2 = 71
					sData.VehicleSetup.iColourExtra1 = 74
					sData.VehicleSetup.iColourExtra2 = 156
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWheelType = 6
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 2
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
					sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 2
					sData.VehicleSetup.iModIndex[MOD_BONNET] = 1
					sData.VehicleSetup.iModIndex[MOD_ROOF] = 3
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 9
				BREAK
			ENDSWITCH
		BREAK
		CASE STRYDER
			SWITCH iIndex
				CASE 0
					sData.VehicleSetup.eModel = Stryder
					sData.VehicleSetup.tlPlateText = "45YZY588"
					sData.VehicleSetup.iColour1 = 112
					sData.VehicleSetup.iColour2 = 89
					sData.VehicleSetup.iColourExtra1 = 0
					sData.VehicleSetup.iColourExtra2 = 1
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWheelType = 7
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 5
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 5
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 7
				BREAK
				CASE 1
					sData.VehicleSetup.eModel = Stryder
					sData.VehicleSetup.tlPlateText = "05VIC098"
					sData.VehicleSetup.iColour1 = 27
					sData.VehicleSetup.iColour2 = 73
					sData.VehicleSetup.iColourExtra1 = 0
					sData.VehicleSetup.iColourExtra2 = 1
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWheelType = 7
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 2
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 4
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 8
				BREAK
				CASE 2
					sData.VehicleSetup.eModel = Stryder
					sData.VehicleSetup.tlPlateText = "81BJX061"
					sData.VehicleSetup.iColour1 = 92
					sData.VehicleSetup.iColour2 = 111
					sData.VehicleSetup.iColourExtra1 = 92
					sData.VehicleSetup.iColourExtra2 = 1
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWheelType = 7
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 9
				BREAK
				CASE 3
					sData.VehicleSetup.eModel = Stryder
					sData.VehicleSetup.tlPlateText = "61XWF939"
					sData.VehicleSetup.iColour1 = 135
					sData.VehicleSetup.iColour2 = 111
					sData.VehicleSetup.iColourExtra1 = 135
					sData.VehicleSetup.iColourExtra2 = 1
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWheelType = 7
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 4
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 5
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 10
				BREAK
			ENDSWITCH
		BREAK
		CASE DEFILER
			SWITCH iIndex
				CASE 0
					sData.VehicleSetup.eModel = DEFILER
					sData.VehicleSetup.tlPlateText = "85PFY881"
					sData.VehicleSetup.iColour1 = 73
					sData.VehicleSetup.iColour2 = 70
					sData.VehicleSetup.iColourExtra1 = 73
					sData.VehicleSetup.iColourExtra2 = 156
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWheelType = 6
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
					sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 2
					sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 4
					sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 2
					sData.VehicleSetup.iModIndex[MOD_BONNET] = 1
					sData.VehicleSetup.iModIndex[MOD_ROOF] = 2
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 6
				BREAK
				CASE 1
					sData.VehicleSetup.eModel = DEFILER
					sData.VehicleSetup.tlPlateText = "09QNN844"
					sData.VehicleSetup.iColour1 = 112
					sData.VehicleSetup.iColour2 = 145
					sData.VehicleSetup.iColourExtra1 = 0
					sData.VehicleSetup.iColourExtra2 = 156
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWheelType = 6
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					sData.VehicleSetup.iModIndex[MOD_SPOILER] = 2
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
					sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 3
					sData.VehicleSetup.iModIndex[MOD_ROOF] = 3
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 5
				BREAK
				CASE 2
					sData.VehicleSetup.eModel = DEFILER
					sData.VehicleSetup.tlPlateText = "28BPV442"
					sData.VehicleSetup.iColour1 = 38
					sData.VehicleSetup.iColour2 = 111
					sData.VehicleSetup.iColourExtra1 = 37
					sData.VehicleSetup.iColourExtra2 = 156
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWheelType = 6
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 3
					sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
					sData.VehicleSetup.iModIndex[MOD_ROOF] = 4
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 1
				BREAK
				CASE 3
					sData.VehicleSetup.eModel = DEFILER
					sData.VehicleSetup.tlPlateText = "85LDY796"
					sData.VehicleSetup.iColour1 = 150
					sData.VehicleSetup.iColour2 = 111
					sData.VehicleSetup.iColourExtra1 = 150
					sData.VehicleSetup.iColourExtra2 = 156
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWheelType = 6
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					sData.VehicleSetup.iModIndex[MOD_SPOILER] = 2
					sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
					sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 3
					sData.VehicleSetup.iModIndex[MOD_BONNET] = 1
					sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 12
				BREAK
			ENDSWITCH
		BREAK
		CASE LECTRO
			SWITCH iIndex
				CASE 0
					sData.VehicleSetup.eModel = LECTRO
					sData.VehicleSetup.tlPlateText = "89BVQ075"
					sData.VehicleSetup.iPlateIndex = 3
					sData.VehicleSetup.iColour1 = 111
					sData.VehicleSetup.iColour2 = 64
					sData.VehicleSetup.iColourExtra1 = 0
					sData.VehicleSetup.iColourExtra2 = 156
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWheelType = 6
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
				BREAK
				CASE 1
					sData.VehicleSetup.eModel = LECTRO
					sData.VehicleSetup.tlPlateText = "61KCQ803"
					sData.VehicleSetup.iPlateIndex = 3
					sData.VehicleSetup.iColour1 = 135
					sData.VehicleSetup.iColour2 = 118
					sData.VehicleSetup.iColourExtra1 = 135
					sData.VehicleSetup.iColourExtra2 = 156
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWheelType = 6
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
				BREAK
				CASE 2
					sData.VehicleSetup.eModel = LECTRO
					sData.VehicleSetup.tlPlateText = "28DJW797"
					sData.VehicleSetup.iPlateIndex = 3
					sData.VehicleSetup.iColour1 = 89
					sData.VehicleSetup.iColour2 = 138
					sData.VehicleSetup.iColourExtra1 = 88
					sData.VehicleSetup.iColourExtra2 = 156
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWheelType = 6
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
				BREAK
				CASE 3
					sData.VehicleSetup.eModel = LECTRO
					sData.VehicleSetup.tlPlateText = "63ASU750"
					sData.VehicleSetup.iPlateIndex = 3
					sData.VehicleSetup.iColour1 = 53
					sData.VehicleSetup.iColour2 = 5
					sData.VehicleSetup.iColourExtra1 = 59
					sData.VehicleSetup.iColourExtra2 = 156
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWheelType = 6
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
				BREAK
			ENDSWITCH
		BREAK
		
		CASE ZHABA
			SWITCH iIndex
				CASE 0
					sData.VehicleSetup.eModel = ZHABA
					sData.VehicleSetup.tlPlateText = "88GCM611"
					sData.VehicleSetup.iColour1 = 154
					sData.VehicleSetup.iColour2 = 0
					sData.VehicleSetup.iColourExtra1 = 0
					sData.VehicleSetup.iColourExtra2 = 111
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_1)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_2)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_BULLETPROOF_TYRES)
					sData.VehicleSetup.iModIndex[MOD_SPOILER] = 2
					sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 2
					sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 2
					sData.VehicleSetup.iModIndex[MOD_SKIRT] = 2
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 9
					sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
					sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
					sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
					sData.VehicleSetup.iModIndex[MOD_WING_R] = 1
					sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
				BREAK
				CASE 1
					sData.VehicleSetup.eModel = ZHABA
					sData.VehicleSetup.tlPlateText = "46AOM725"
					sData.VehicleSetup.iColour1 = 5
					sData.VehicleSetup.iColour2 = 111
					sData.VehicleSetup.iColourExtra1 = 5
					sData.VehicleSetup.iColourExtra2 = 111
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_1)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_2)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_BULLETPROOF_TYRES)
					sData.VehicleSetup.iModIndex[MOD_SPOILER] = 5
					sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 3
					sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 3
					sData.VehicleSetup.iModIndex[MOD_SKIRT] = 5
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 7
					sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 3
					sData.VehicleSetup.iModIndex[MOD_GRILL] = 6
					sData.VehicleSetup.iModIndex[MOD_WING_R] = 1
					sData.VehicleSetup.iModIndex[MOD_ROOF] = 6
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 17
				BREAK
			ENDSWITCH
		BREAK
		CASE VAGRANT
			SWITCH iIndex
				CASE 0
					sData.VehicleSetup.eModel = VAGRANT
					sData.VehicleSetup.tlPlateText = "22IUD069"
					sData.VehicleSetup.iColour1 = 138
					sData.VehicleSetup.iColour2 = 138
					sData.VehicleSetup.iColourExtra1 = 0
					sData.VehicleSetup.iColourExtra2 = 112
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 3
					sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
					sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
					sData.VehicleSetup.iModIndex[MOD_BONNET] = 2
					sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
					sData.VehicleSetup.iModIndex[MOD_WING_R] = 1
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 23
				BREAK
				CASE 1
					sData.VehicleSetup.eModel = VAGRANT
					sData.VehicleSetup.tlPlateText = "84USA531"
					sData.VehicleSetup.iColour1 = 111
					sData.VehicleSetup.iColour2 = 106
					sData.VehicleSetup.iColourExtra1 = 0
					sData.VehicleSetup.iColourExtra2 = 112
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 4
					sData.VehicleSetup.iModIndex[MOD_WING_L] = 3
					sData.VehicleSetup.iModIndex[MOD_WING_R] = 1
					sData.VehicleSetup.iModIndex[MOD_ROOF] = 2
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 2
				BREAK
			ENDSWITCH
		BREAK
		CASE OUTLAW
			SWITCH iIndex
				CASE 0
					sData.VehicleSetup.eModel = OUTLAW
					sData.VehicleSetup.tlPlateText = "68ADW760"
					sData.VehicleSetup.iColour1 = 3
					sData.VehicleSetup.iColour2 = 0
					sData.VehicleSetup.iColourExtra1 = 6
					sData.VehicleSetup.iColourExtra2 = 0
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWheelType = 4
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 17
				BREAK
				CASE 1
					sData.VehicleSetup.eModel = OUTLAW
					sData.VehicleSetup.tlPlateText = "02AFC413"
					sData.VehicleSetup.iColour1 = 36
					sData.VehicleSetup.iColour2 = 111
					sData.VehicleSetup.iColourExtra1 = 0
					sData.VehicleSetup.iColourExtra2 = 0
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWheelType = 4
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 1
				BREAK
			ENDSWITCH
		BREAK
		CASE EVERON
			SWITCH iIndex
				CASE 0
					sData.VehicleSetup.eModel = EVERON
					sData.VehicleSetup.tlPlateText = "46ZLJ093"
					sData.VehicleSetup.iColour1 = 29
					sData.VehicleSetup.iColour2 = 0
					sData.VehicleSetup.iColourExtra1 = 35
					sData.VehicleSetup.iColourExtra2 = 0
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWheelType = 3
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 4
					sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 2
					sData.VehicleSetup.iModIndex[MOD_SKIRT] = 2
					sData.VehicleSetup.iModIndex[MOD_BONNET] = 14
					sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
					sData.VehicleSetup.iModIndex[MOD_WING_R] = 5
					sData.VehicleSetup.iModIndex[MOD_ROOF] = 6
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 4
				BREAK
				CASE 1
					sData.VehicleSetup.eModel = EVERON
					sData.VehicleSetup.tlPlateText = "82AKQ481"
					sData.VehicleSetup.iColour1 = 111
					sData.VehicleSetup.iColour2 = 0
					sData.VehicleSetup.iColourExtra1 = 0
					sData.VehicleSetup.iColourExtra2 = 0
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWheelType = 3
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
					sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 7
					sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
					sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
					sData.VehicleSetup.iModIndex[MOD_BONNET] = 14
					sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
					sData.VehicleSetup.iModIndex[MOD_WING_R] = 3
					sData.VehicleSetup.iModIndex[MOD_ROOF] = 3
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 9
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	IF sData.VehicleSetup.eModel != DUMMY_MODEL_FOR_SCRIPT
		PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - SETUP_GETAWAY_VEHICLE_COSMETICS - set cosmetics for getaway vehicle iVehicle #", iVehicle, " with iMissionEntity #", iIndex, ", model ", GET_MODEL_NAME_FOR_DEBUG(sData.VehicleSetup.eModel))
		SET_VEHICLE_SETUP_MP(vehID, sData)
	ENDIF

ENDPROC

PROC SET_CARRIER_VEHICLE_SPAWN_ATTRIBUTES(INT iVehicle, VEHICLE_INDEX vehId)

	PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_CARRIER_VEHICLE_SPAWN_ATTRIBUTES")

	MODEL_NAMES eModel = GET_ENTITY_MODEL(vehId)
	
	SWITCH GET_VARIATION()	
		CASE PLACEHOLDERVAR_MAX
			//SET_PLACEHOLDER_VEHICLE_COSMETICS(iVehicle, vehID)
		BREAK
	ENDSWITCH
	
	SETUP_GETAWAY_VEHICLE_COSMETICS(iVehicle, vehID)
	
	SET_ENTITY_LOD_DIST(vehId, 1200)
	
	IF SHOULD_SET_CARRIER_VEHICLE_STRONG_TYRES()
		SET_VEHICLE_TYRES_CAN_BURST(vehId, FALSE)
		PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_CARRIER_VEHICLE_SPAWN_ATTRIBUTES - SHOULD_SET_CARRIER_VEHICLE_STRONG_TYRES")
	ENDIF
	IF SHOULD_SET_CARRIER_VEHICLE_STRONG()
		SET_VEHICLE_STRONG(vehId, TRUE)
		PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_CARRIER_VEHICLE_SPAWN_ATTRIBUTES - SHOULD_SET_CARRIER_VEHICLE_STRONG")
	ENDIF
	IF SHOULD_SET_CARRIER_VEHICLE_MP_DAMAGE()
		SET_VEHICLE_USES_MP_PLAYER_DAMAGE_MULTIPLIER(vehId, FALSE)
		PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_CARRIER_VEHICLE_SPAWN_ATTRIBUTES - SHOULD_SET_CARRIER_VEHICLE_MP_DAMAGE")
	ENDIF
	
	IF SHOULD_ADJUST_DAMAGE_SCALE(GET_ENTITY_MODEL(vehId))
		SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE(vehId, FALSE)
	
		SET_VEHICLE_DAMAGE_SCALE(vehId, GET_MISSION_ENTITY_DAMAGE_SCALE(GET_ENTITY_MODEL(vehId)))
		
		IF IS_THIS_MODEL_A_PLANE(eModel)
			//SET_PLANE_RESIST_TO_EXPLOSION(vehId, TRUE)
			IF SHOULD_PLANE_BE_UNBREAKABLE(eModel)
				SET_VEHICLE_CAN_BREAK(vehId, FALSE)
			ENDIF
		ENDIF
	
		SET_ENTITY_HEALTH(vehId, 1000)
		PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_CARRIER_VEHICLE_SPAWN_ATTRIBUTES - SHOULD_ADJUST_DAMAGE_SCALE")
	ENDIF
	
	IF PREVENT_EXPLODE_VEHICLE_ON_BODY_DMG()
		SET_DISABLE_EXPLODE_FROM_BODY_DAMAGE_ON_COLLISION(vehId, TRUE)
		PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_CARRIER_VEHICLE_SPAWN_ATTRIBUTES - SET_DISABLE_EXPLODE_FROM_BODY_DAMAGE_ON_COLLISION")
	ENDIF
	
	CALL_SET_ALLOW_RAMMING_SOOP_OR_RAMP(vehId)
	
	SET_MENTAL_STATE_WILL_IGNORE_THIS_VEHICLE(vehId, TRUE)
	
	SET_FMP_DECORATOR_ON_VEHICLE(vehId)
	
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			SET_VEHICLE_ALLOW_HOMING_MISSLE_LOCKON_SYNCED(vehId, TRUE)
			PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_CARRIER_VEHICLE_SPAWN_ATTRIBUTES - Allow homing missile lock-on.")
		BREAK
		
		DEFAULT
			SET_VEHICLE_ALLOW_HOMING_MISSLE_LOCKON_SYNCED(vehId, FALSE)
		BREAK
	ENDSWITCH
	
	SET_VEHICLE_ALLOW_NO_PASSENGERS_LOCKON(vehId, FALSE)
	
	IF SHOULD_LOCK_CARRIER_VEHICLE_ON_SPAWN()
		SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(vehId, TRUE)
		SET_VEHICLE_DISABLE_TOWING(vehId, TRUE)
	ENDIF
	
	IF SHOULD_LOCK_CARRIER_VEHICLE_REAR_DOORS(iVehicle)
		SET_VEHICLE_INDIVIDUAL_DOORS_LOCKED(vehId, ENUM_TO_INT(SC_DOOR_REAR_LEFT), VEHICLELOCK_LOCKED)
		SET_VEHICLE_INDIVIDUAL_DOORS_LOCKED(vehId, ENUM_TO_INT(SC_DOOR_REAR_RIGHT), VEHICLELOCK_LOCKED)
		SET_OPEN_REAR_DOORS_ON_EXPLOSION(vehId, FALSE)
	ENDIF
	
	IF SHOULD_BLOCK_ALLOWING_ENTRY_IF_LOCKED()
		SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(vehId, TRUE)
	ENDIF
	SET_VEHICLE_RESPECTS_LOCKS_WHEN_HAS_DRIVER(vehId, TRUE)
	SET_VEHICLE_DOORS_LOCKED_FOR_NON_SCRIPT_PLAYERS(vehID, TRUE)
	
	IF SHOULD_CARRIER_VEHICLE_NEED_HOTWIRED()
		SET_VEHICLE_NEEDS_TO_BE_HOTWIRED(vehId, TRUE)
	ENDIF
	
	IF SHOULD_CARRIER_VEHICLE_NEED_BROKEN_INTO(eModel)
		SET_VEHICLE_DOORS_LOCKED(vehId, VEHICLELOCK_LOCKED_BUT_CAN_BE_DAMAGED)
	ENDIF
	
	IF eModel = PHANTOM2
	OR eModel = DUNE5
		VEHICLE_SET_RAMP_AND_RAMMING_CARS_TAKE_DAMAGE(vehId, FALSE)
		PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_CARRIER_VEHICLE_SPAWN_ATTRIBUTES - VEHICLE_SET_RAMP_AND_RAMMING_CARS_TAKE_DAMAGE(vehId, FALSE)")
	ENDIF
ENDPROC

FUNC BOOL SHOULD_SET_UP_CARRIER_VEHICLE_SPAWN_ATTRIBUTES(INT iVehicle)
	IF IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_I_AM_CARRIER_VEHICLE)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC TURN_ON_VEHICLE_RADIO(VEHICLE_INDEX vehID)

	SET_VEHICLE_ENGINE_ON(vehId, TRUE, TRUE)
	SET_VEHICLE_RADIO_ENABLED(vehId, TRUE)
	//SET_VEHICLE_RADIO_LOUD(vehId, TRUE)
	SET_VEH_FORCED_RADIO_THIS_FRAME(vehId)
	SET_VEH_RADIO_STATION(vehId, "RADIO_03_HIPHOP_NEW")
	
ENDPROC

FUNC BOOL SHOULD_SET_VEHICLE_LOCATION_AFTER_CREATION(INT iVehicle)
	UNUSED_PARAMETER(iVehicle)
	
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			RETURN TRUE
	ENDSWITCH

	RETURN FALSE

ENDFUNC

PROC SET_VEHICLE_ATTRIBUTES_FOR_SPAWN(INT iVehicle)
	
	VEHICLE_INDEX vehId = NET_TO_VEH(serverBD.sVehicle[iVehicle].netId)
	
	IF IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_INSIDE_MISSION_INTERIOR)
		NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(NET_TO_ENT(serverBD.sVehicle[iVehicle].netId), TRUE)
	ENDIF
	
	// Reduce turbulence for mission planes
	IF IS_VEHICLE_A_SUPPORT_VEHICLE(iVehicle)
	OR IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_I_AM_CARRIER_VEHICLE)
		IF IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(vehId))
			SET_PLANE_TURBULENCE_MULTIPLIER(vehId, 0.0)
			PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_VEHICLE_ATTRIBUTES_FOR_SPAWN - SET_PLANE_TURBULENCE_MULTIPLIER to 0 for iVehicle #", iVehicle)
		ENDIF
	ENDIF
	
	IF DECOR_IS_REGISTERED_AS_TYPE("Not_Allow_As_Saved_Veh", DECOR_TYPE_INT)
    	DECOR_SET_INT(vehId, "Not_Allow_As_Saved_Veh", MP_NOT_ALLOWED_AS_SAVED_VEHICLE_DEC_SIMEON)
		PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] SET_VEHICLE_ATTRIBUTES_FOR_SPAWN - Set decorator 'Not_Allow_As_Saved_Veh' on iVehicle #", iVehicle)
	ENDIF
		
	IF SHOULD_SET_VEHICLE_LOCATION_AFTER_CREATION(iVehicle)
		VECTOR vSpawnCoords = GET_FMP_VEHICLE_SPAWN_COORDS(GET_VARIATION(), GET_SUBVARIATION(), iVehicle, DEFAULT, GET_EXTRA_PARAM_FOR_VEHICLE_SPAWN(iVehicle))
		FLOAT fHeading = GET_FMP_VEHICLE_SPAWN_HEADING(GET_VARIATION(), GET_SUBVARIATION(), iVehicle, DEFAULT, GET_EXTRA_PARAM_FOR_VEHICLE_SPAWN(iVehicle))
		SET_ENTITY_COORDS_NO_OFFSET(vehId, vSpawnCoords)
		SET_ENTITY_HEADING(vehId, fHeading)
		PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] SET_VEHICLE_ATTRIBUTES_FOR_SPAWN - SHOULD_SET_VEHICLE_LOCATION_AFTER_CREATION - iVehicle #", iVehicle, " moved to ", vSpawnCoords)
	ENDIF

	IF IS_VEHICLE_A_SUPPORT_VEHICLE(iVehicle)
		SET_SUPPORT_VEHICLE_SPAWN_ATTRIBUTES(iVehicle, vehId)
		EXIT
	ENDIF

	IF SHOULD_SET_UP_CARRIER_VEHICLE_SPAWN_ATTRIBUTES(iVehicle)
		SET_CARRIER_VEHICLE_SPAWN_ATTRIBUTES(iVehicle, vehId)
		EXIT
	ENDIF
	
	SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(vehId, TRUE)
	SET_VEHICLE_RESPECTS_LOCKS_WHEN_HAS_DRIVER(vehId, TRUE)
	SET_VEHICLE_DOORS_LOCKED_FOR_NON_SCRIPT_PLAYERS(vehID, TRUE)
	
	// Add in mission specific vehicle attributes here
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			//SET_PLACEHOLDER_VEHICLE_COSMETICS(iVehicle, vehID)
		BREAK
	ENDSWITCH
ENDPROC

PROC INITIALISE_AMBUSH_VEHICLE_BITSETS()
	IF GET_AMBUSH_VEHICLE_ARRAY_INDEX(eVEHICLEBITSET_I_AM_AMBUSH_VEHICLE_0) != (-1)
		SET_VEHICLE_BIT(GET_AMBUSH_VEHICLE_ARRAY_INDEX(eVEHICLEBITSET_I_AM_AMBUSH_VEHICLE_0), eVEHICLEBITSET_I_AM_AMBUSH_VEHICLE_0)
	ENDIF
	
	IF GET_AMBUSH_VEHICLE_ARRAY_INDEX(eVEHICLEBITSET_I_AM_AMBUSH_VEHICLE_1) != (-1)
		SET_VEHICLE_BIT(GET_AMBUSH_VEHICLE_ARRAY_INDEX(eVEHICLEBITSET_I_AM_AMBUSH_VEHICLE_1), eVEHICLEBITSET_I_AM_AMBUSH_VEHICLE_1)
	ENDIF
ENDPROC

FUNC BOOL SHOULD_SET_CARRIER_VEHICLE_BIT_FROM_START()
	RETURN TRUE
ENDFUNC

PROC INITIALISE_VEHICLE_BITSETS_FOR_MODE_START()
//	INT iSupportCount = GB_GET_FMP_NUM_SUPPORT_VEHICLES_REQUIRED(GET_VARIATION(), serverBD.iOrganisationSizeOnLaunch)
	
	IF DOES_FMP_VARIATION_HAVE_AMBUSH(GET_VARIATION(), GET_SUBVARIATION())
		INITIALISE_AMBUSH_VEHICLE_BITSETS()
	ENDIF
	
	// set carrier bits
	INT iMissionEntity
	REPEAT GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() iMissionEntity
		IF DOES_MISSION_ENTITY_HAVE_CARRIER_VEHICLE(iMissionEntity)
		AND SHOULD_SET_CARRIER_VEHICLE_BIT_FROM_START()
			SET_VEHICLE_BIT(serverBD.iCarrierVehicle[iMissionEntity], eVEHICLEBITSET_I_AM_CARRIER_VEHICLE)
		ENDIF
	ENDREPEAT
	
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
		BREAK
	ENDSWITCH

ENDPROC

PROC RESET_SERVER_VEHICLE_SPAWN_DATA()
	serverBd.iCurrentNonMissionEntityVehicleSearchingForSpawnCoords = (-1)
	serverBd.vSpawnNonMissionEntityVehicleCoords = << 0.0, 0.0, 0.0 >>
	serverBd.fSpawnNonMissionEntityVehicleHeading = 0.0
	serverBd.iGetSpawnNonMissionEntityVehicleCoordsStage = 0
	serverBd.iCircCount = 0
	serverBD.vVehicleSpawnCoord = << 0.0, 0.0, 0.0 >>
	PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - RESET_SERVER_VEHICLE_SPAWN_DATA - Vehicle data cleared.")
ENDPROC

FUNC BOOL SHOULD_DELETE_VEHICLE_ON_CLEANUP(INT iVehicle)

	IF IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_INSIDE_MISSION_INTERIOR)
	AND NOT IS_ANY_PLAYER_IN_VEHICLE(NET_TO_VEH(serverBD.sVehicle[iVehicle].netId))
		RETURN TRUE
	ENDIF
	
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE

ENDFUNC

PROC CLEANUP_VEHICLES()
	
	INT i
	
	REPEAT NUM_VARIATION_VEHICLES i
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[i].netId)
			//IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[i].netId)
				IF NET_TO_VEH(serverBD.sVehicle[i].netId) = GET_LAST_DRIVEN_VEHICLE()
					PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] CLEANUP_VEHICLES - non-MissionEntity iVehicle #", i, " was last driven vehicle, clearing last driven vehicle")
					CLEAR_LAST_DRIVEN_VEHICLE()
				ENDIF
				IF SHOULD_DELETE_VEHICLE_ON_CLEANUP(i)
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[i].netId)
						DELETE_NET_ID(serverBD.sVehicle[i].netId)
						PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] CLEANUP_VEHICLES - set non-MissionEntity iVehicle #", i, " as deleted")
					ENDIF
				ELSE
					CLEANUP_NET_ID(serverBD.sVehicle[i].netId)
					PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] CLEANUP_VEHICLES - set non-MissionEntity iVehicle #", i, " as no longer needed")
				ENDIF
			//ENDIF
		ENDIF
	ENDREPEAT	
	
ENDPROC

FUNC BOOL SHOULD_VEHICLE_BE_CREATED_AT_START(INT iVehicle)
	IF IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_I_AM_CARRIER_VEHICLE)
		RETURN FALSE
	ENDIF
	
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			RETURN FALSE
	ENDSWITCH
		
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_CREATE_CARRIER_VEHICLES()

	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			RETURN FALSE
	ENDSWITCH

	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_VEHICLES_BE_CREATED_ON_MISSION_START()

	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			RETURN FALSE
	ENDSWITCH

	RETURN TRUE

ENDFUNC

FUNC BOOL SHOULD_VEHICLE_CREATION_IGNORE_CARRIER_VEHICLES()
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			RETURN FALSE
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_CARRIER_VEHICLE_BE_CREATED_AT_START(INT iVehicle)
	UNUSED_PARAMETER(iVehicle)
	
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			RETURN FALSE
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_CARRIER_VEHICLES_BE_CREATED_MID_MISSION()
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_CARRIER_VEHICLE_BE_CREATED_MID_MISSION(INT iVehicle)
	UNUSED_PARAMETER(iVehicle)
	
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			SWITCH iVehicle
				CASE 2
					IF GET_VEHICLE_STATE(1) > eVEHICLESTATE_INACTIVE
					AND HAS_NET_TIMER_EXPIRED(stCarrierVehicleSpawnTimer, 30000)
						RETURN TRUE
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_VEHICLES_BE_CREATED_MID_MISSION()
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_VEHICLE_BE_CREATED_MID_MISSION(INT iVehicle)
	
	UNUSED_PARAMETER(iVehicle)
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_ACTIVATING_VEHICLES()
	
	INT iVehicle
	
	// Ambush vehicle activation
	// Shouldn't need to touch this, just add your mission to the wrappers
	IF DOES_FMP_VARIATION_HAVE_AMBUSH(GET_VARIATION(), GET_SUBVARIATION())
		IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_ACTIVATED_VEHICLES_FOR_AMBUSH)
		AND SHOULD_AMBUSH_ACTIVATE()
			IF GET_AMBUSH_VEHICLE_ARRAY_INDEX(eVEHICLEBITSET_I_AM_AMBUSH_VEHICLE_0) != (-1)
				SET_VEHICLE_STATE(GET_AMBUSH_VEHICLE_ARRAY_INDEX(eVEHICLEBITSET_I_AM_AMBUSH_VEHICLE_0), eVEHICLESTATE_CREATE)
			ENDIF
			IF GET_AMBUSH_VEHICLE_ARRAY_INDEX(eVEHICLEBITSET_I_AM_AMBUSH_VEHICLE_1) != (-1)
				SET_VEHICLE_STATE(GET_AMBUSH_VEHICLE_ARRAY_INDEX(eVEHICLEBITSET_I_AM_AMBUSH_VEHICLE_1), eVEHICLESTATE_CREATE)
			ENDIF
			SET_SERVER_BIT(eSERVERBITSET_ACTIVATED_VEHICLES_FOR_AMBUSH)
		ENDIF
	ENDIF
	
	// Bunker Support Vehicles
	IF DOES_FMP_VARIATION_HAVE_SUPPORT_VEHICLES(GET_VARIATION(), GET_SUBVARIATION())
		IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_ACTIVATED_SUPPORT_VEHICLES)
			IF GB_GET_FMP_NUM_SUPPORT_VEHICLES_REQUIRED(GET_VARIATION(), serverBD.iOrganisationSizeOnLaunch) > 0
				REPEAT NUM_VARIATION_VEHICLES iVehicle
					IF IS_VEHICLE_A_SUPPORT_VEHICLE(iVehicle)
						SET_VEHICLE_STATE(iVehicle, eVEHICLESTATE_CREATE)
					ENDIF
				ENDREPEAT
				SET_SERVER_BIT(eSERVERBITSET_ACTIVATED_SUPPORT_VEHICLES)
			ENDIF
		ENDIF
	ENDIF
	
	// Carrier vehicles
	IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_ACTIVATED_CARRIER_VEHICLES)
		IF SHOULD_CREATE_CARRIER_VEHICLES()
			IF DOES_MISSION_ENTITY_HAVE_CARRIER_VEHICLE(0)	// Carrier vehicles are used on this mission
				REPEAT NUM_VARIATION_VEHICLES iVehicle
					IF IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_I_AM_CARRIER_VEHICLE)
					AND SHOULD_CARRIER_VEHICLE_BE_CREATED_AT_START(iVehicle)
						SET_VEHICLE_STATE(iVehicle, eVEHICLESTATE_CREATE)
					ENDIF
				ENDREPEAT
				SET_SERVER_BIT(eSERVERBITSET_ACTIVATED_CARRIER_VEHICLES)
			ENDIF
		ENDIF
	ENDIF
	
	IF SHOULD_CARRIER_VEHICLES_BE_CREATED_MID_MISSION()
		IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_ACTIVATED_CARRIER_VEHICLES_MID_MISSION)
			BOOL bAllCreated = TRUE
			
			REPEAT NUM_VARIATION_VEHICLES iVehicle
				IF IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_I_AM_CARRIER_VEHICLE)
				AND GET_VEHICLE_STATE(iVehicle) = eVEHICLESTATE_INACTIVE
					IF SHOULD_CARRIER_VEHICLE_BE_CREATED_MID_MISSION(iVehicle)
						SET_VEHICLE_STATE(iVehicle, eVEHICLESTATE_CREATE)
					ELSE
						bAllCreated = FALSE
					ENDIF
				ENDIF
			ENDREPEAT
			
			IF bAllCreated
				SET_SERVER_BIT(eSERVERBITSET_ACTIVATED_CARRIER_VEHICLES_MID_MISSION)
			ENDIF
		ENDIF
	ENDIF
	
	IF SHOULD_VEHICLES_BE_CREATED_ON_MISSION_START()
		IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_ACTIVATED_VEHICLES_FOR_MISSION_START)
		AND (HAVE_ALL_MISSION_ENTITIES_BEEN_CREATED()
		OR ARE_MISSION_ENTITIES_MADE_DURING_MISSION())

			REPEAT GB_GET_FMP_NUM_VEH_REQUIRED(GET_VARIATION(), GET_SUBVARIATION(), GB_GET_LOCAL_PLAYER_GANG_BOSS(), TRUE, GET_EXTRA_PARAM_FOR_VEHICLE_SPAWN(), TRUE, SHOULD_VEHICLE_CREATION_IGNORE_CARRIER_VEHICLES()) iVehicle
				IF SHOULD_VEHICLE_BE_CREATED_AT_START(iVehicle)
					SET_VEHICLE_STATE(iVehicle, eVEHICLESTATE_CREATE)
				ENDIF
			ENDREPEAT
			SET_SERVER_BIT(eSERVERBITSET_ACTIVATED_VEHICLES_FOR_MISSION_START)
		ENDIF
		
		IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_CREATED_VEHICLES_FOR_MISSION_START)
			BOOL bAllCreated = TRUE
			
			REPEAT GB_GET_FMP_NUM_VEH_REQUIRED(GET_VARIATION(), GET_SUBVARIATION(), GB_GET_LOCAL_PLAYER_GANG_BOSS(), TRUE, GET_EXTRA_PARAM_FOR_VEHICLE_SPAWN(), TRUE, SHOULD_VEHICLE_CREATION_IGNORE_CARRIER_VEHICLES()) iVehicle
				IF SHOULD_VEHICLE_BE_CREATED_AT_START(iVehicle)
				AND GET_VEHICLE_STATE(iVehicle) <= eVEHICLESTATE_CREATE
					bAllCreated = FALSE
				ENDIF
			ENDREPEAT
			
			IF bAllCreated
				SET_SERVER_BIT(eSERVERBITSET_CREATED_VEHICLES_FOR_MISSION_START)
			ENDIF
		ENDIF
	ENDIF
	
	IF SHOULD_VEHICLES_BE_CREATED_MID_MISSION()
		IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_ACTIVATED_VEHICLES_MID_MISSION)
			BOOL bAllCreated = TRUE
			
			REPEAT GB_GET_FMP_NUM_VEH_REQUIRED(GET_VARIATION(), GET_SUBVARIATION(), GB_GET_LOCAL_PLAYER_GANG_BOSS(), TRUE, GET_EXTRA_PARAM_FOR_VEHICLE_SPAWN(), TRUE, TRUE) iVehicle
				IF GET_VEHICLE_STATE(iVehicle) = eVEHICLESTATE_INACTIVE
					IF SHOULD_VEHICLE_BE_CREATED_MID_MISSION(iVehicle)
						SET_VEHICLE_STATE(iVehicle, eVEHICLESTATE_CREATE)
					ELSE
						bAllCreated = FALSE
					ENDIF
				ENDIF
			ENDREPEAT
			
			IF bAllCreated
				SET_SERVER_BIT(eSERVERBITSET_ACTIVATED_VEHICLES_MID_MISSION)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_SPAWN_VEHICLES()
	RETURN TRUE
ENDFUNC

PROC RESET_VEHICLE_DATA_FOR_RESPAWN(INT iVehicle)

	IF DOES_FMP_VARIATION_HAVE_AMBUSH(GET_VARIATION(), GET_SUBVARIATION())
		serverBd.iCurrentNonMissionEntityVehicleSearchingForSpawnCoords = (-1)
		serverBd.vSpawnNonMissionEntityVehicleCoords = << 0.0, 0.0, 0.0 >>
		serverBd.fSpawnNonMissionEntityVehicleHeading = 0.0
		ServerBd.iAmbushGetNonMissionEntityCoordsAttempts = 0
		serverBd.iGetSpawnNonMissionEntityVehicleCoordsStage = 0
		RESET_MISSION_ENTITY_TARGET_FOR_VEHICLE(iVehicle)
		
		PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - RESET_VEHICLE_DATA_FOR_RESPAWN - iVehicle #", iVehicle)
	ENDIF
	
ENDPROC

FUNC BOOL DOES_VARIATION_HAVE_VEHICLES()
	IF IS_THIS_VARIATION_A_SELL_MISSION()
		RETURN TRUE
	ENDIF
	
	IF GB_GET_FMP_NUM_SUPPORT_VEHICLES_REQUIRED(GET_VARIATION(), serverBD.iOrganisationSizeOnLaunch) > 0
		RETURN TRUE
	ENDIF
	
	IF DOES_FMP_VARIATION_HAVE_AMBUSH(GET_VARIATION(), GET_SUBVARIATION())
		RETURN TRUE
	ENDIF
	
	IF GET_FMP_CARRIER_VEHICLE_INDEX(0, GET_SUBVARIATION()) != -1
		RETURN TRUE
	ENDIF
	
	// Add your mission here if it has vehicles
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_USE_GROUND_Z_FOR_AIR_SPAWNING()
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_CONSIDER_HIGHWAYS_FOR_VARIATION(INT iVehicle)
	UNUSED_PARAMETER(iVehicle)
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_ALLOW_OFF_ROAD_NODES(INT iVehicle)
	UNUSED_PARAMETER(iVehicle)
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_IGNORE_CUSTOM_NODES()
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_SPREAD_VEHICLES_APART()
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_DROP_OFF_NEED_LARGER_SPAWN_SEARCH_AREA()
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_DROP_OFF_NEED_CUSTOM_SPAWN_SEARCH_AREA()
	RETURN FALSE
ENDFUNC

FUNC FLOAT GET_DROP_OFF_CUSTOM_MAX_SPAWN_SEARCH_AREA_SIZE()
	RETURN 0.0
ENDFUNC

FUNC FLOAT GET_DROP_OFF_CUSTOM_MIN_SPAWN_SEARCH_AREA_SIZE()
	RETURN 0.0
ENDFUNC

FUNC VECTOR GET_FAVOURED_FACING(VECTOR vSpawnCoords)
	UNUSED_PARAMETER(vSpawnCoords)
	RETURN vSpawnCoords
ENDFUNC

FUNC FLOAT GET_VEHICLE_SPAWN_LOCATION_MAX_DISTANCE(BOOL bLargerSize = FALSE)
	UNUSED_PARAMETER(bLargerSize)
	RETURN 150.0
ENDFUNC

FUNC FLOAT GET_VEHICLE_SPAWN_LOCATION_MIN_DISTANCE(BOOL bLargerSize = FALSE)
	UNUSED_PARAMETER(bLargerSize)
	RETURN 0.0
ENDFUNC

FUNC VECTOR GET_RANDOM_SPAWN_COORD_FOR_VEHICLE(INT iVehicle, VECTOR vCentralCoord)
	INT iXCoord, iYCoord
	VECTOR vNewVector
	INT iMaxDist = 600
	INT iMinDist = 250
	INT iMinDistBetween = 200
	INT iLoop
	INT iNumVehs = GB_GET_FMP_NUM_VEH_REQUIRED(GET_VARIATION(), GET_SUBVARIATION(), GB_GET_LOCAL_PLAYER_GANG_BOSS(), TRUE, GET_EXTRA_PARAM_FOR_VEHICLE_SPAWN(), TRUE, TRUE)
	FLOAT fDistBetween
	VECTOR vTempCoord
	
	// Get a random number between -iMaxDist and iMaxDist to get the x coord
	iXCoord = GET_RANDOM_INT_IN_RANGE((iMaxDist - (iMaxDist*2)), iMaxDist)
	
	// Get a random number between the rest of the value between xCoord and iMaxDist for the y coord
	iYCoord = GET_RANDOM_INT_IN_RANGE((iMaxDist - iXCoord), iMaxDist)
	
	// Should this go positive or negative on y from the x coord?
	iYCoord = PICK_INT(GET_RANDOM_BOOL(), iYCoord, (iYCoord - (iYCoord*2)))
	
	// Now get the new vector based on the grabbed coord offsets from the central coord
	vNewVector = <<vCentralCoord.x+iXCoord, vCentralCoord.y+iYCoord, 0.0>>
	vNewVector.z = GET_APPROX_HEIGHT_FOR_POINT(vNewVector.x, vNewVector.y)
	
	fDistBetween = GET_DISTANCE_BETWEEN_COORDS(vNewVector, vCentralCoord, FALSE)
	IF fDistBetween < iMinDist
		PRINTLN("GET_RANDOM_SPAWN_COORD_FOR_VEHICLE - vNewVector (", vNewVector, ") is too close to the centre of the search area (",vCentralCoord,"), fDistBetween = ", fDistBetween)
		vNewVector = <<0.0, 0.0, 0.0>>
	ENDIF
	
	// Dont bother looping through other vehicles, the coord we tried is too close to the centre of the search area
	IF NOT IS_VECTOR_ZERO(vNewVector)
	
		// Check to make sure its not near any of the other vehicles that have been created so far
		REPEAT iNumVehs iLoop
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[iLoop].netId)
				vTempCoord = GET_ENTITY_COORDS(NET_TO_VEH(serverBD.sVehicle[iLoop].netId), FALSE)
				fDistBetween = GET_DISTANCE_BETWEEN_COORDS(vTempCoord, vNewVector, FALSE)
				PRINTLN("GET_RANDOM_SPAWN_COORD_FOR_VEHICLE - fDistBetween iVehicle #", iVehicle, " and ", iLoop, " = ", fDistBetween)
				IF fDistBetween < iMinDistBetween
					PRINTLN("GET_RANDOM_SPAWN_COORD_FOR_VEHICLE - new vector at ", vNewVector," is too close to iVehicle #", iLoop, " located at ", vTempCoord)
					vNewVector = <<0.0, 0.0, 0.0>>
					BREAKLOOP
				ENDIF
			ELSE
				IF iLoop > 0
				AND iLoop < iVehicle
					PRINTLN("GET_RANDOM_SPAWN_COORD_FOR_VEHICLE - iVehicle #", iLoop, " doesn't exist, cannot compare them.")
					vNewVector = <<0.0, 0.0, 0.0>>
					BREAKLOOP
				ENDIF
			ENDIF
		ENDREPEAT
	
	ENDIF
	
	IF IS_VECTOR_ZERO(vNewVector)
		PRINTLN("GET_RANDOM_SPAWN_COORD_FOR_VEHICLE - returning zero vector.")
	ENDIF
	
	RETURN vNewVector
ENDFUNC

FUNC BOOL SHOULD_VEHICLE_BE_INVINCIBLE_AT_START(INT iVeh)

	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			RETURN FALSE
	ENDSWITCH
	
	IF SHOULD_SET_UP_CARRIER_VEHICLE_SPAWN_ATTRIBUTES(iVeh)
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC VECTOR GET_CREATE_FROM_OFFSET_VEHICLE_COORD(INT iVehicle)
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX		
			SWITCH iVehicle
				CASE 0
					RETURN <<0.0, 0.0, 0.0>>
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC FLOAT GET_CREATE_FROM_OFFSET_VEHICLE_HEADING(INT iVehicle)
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			SWITCH iVehicle	
				CASE 0
					RETURN 0.0
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN 0.0
ENDFUNC

FUNC VECTOR GET_CREATE_FROM_OFFSET_OFFSET(INT iVehicle)
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			SWITCH iVehicle	
				CASE 0
					RETURN <<0.0,0.0,0.0>>
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC BOOL SHOULD_VEHICLE_USE_SHARED_SPAWN_COORD(INT iVehicle)
	IF IS_VEHICLE_A_SUPPORT_VEHICLE(iVehicle)
	AND USE_ZANCUDO_SUPPORT_VEHICLE_SPAWNS()
		RETURN TRUE
	ENDIF
	
	IF IS_VEHICLE_A_CARRIER_VEHICLE(iVehicle)
	AND DOES_FMP_VARIATION_USE_SHARED_SPAWN_COORDS_FOR_CARRIER_VEHICLES(GET_VARIATION())
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC FLOAT GET_VEHICLE_SPAWN_DISTANCE(INT iVehicle)
	UNUSED_PARAMETER(iVehicle)
	
	RETURN 200.0
ENDFUNC

FUNC BOOL GET_FMP_VEHICLE_SHOULD_BOOT_BE_OPEN()
	RETURN FALSE
ENDFUNC

FUNC BOOL CLEAR_FREEZE_COLLISION_FLAG_FOR_VEHICLES(INT iVehicle)
	UNUSED_PARAMETER(iVehicle)
	
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			RETURN TRUE
	ENDSWITCH

	RETURN FALSE
ENDFUNC

FUNC BOOL CREATE_VEHICLE_CREATION(INT iVehicle, BOOL bShouldFindSpawnLocation, BOOL bShouldSpawnInAir, BOOL bDoOkCheck, BOOL bCreateFromOffset)
	
	IF IS_VEHICLE_AN_AMBUSH_VEHICLE(iVehicle)
		CREATE_AMBUSH_VEHICLE(iVehicle)
	ELSE
		IF serverBD.iCurrentPedSearchingForSpawnCoords = -1
			IF serverBd.iCurrentNonMissionEntityVehicleSearchingForSpawnCoords = (-1)
			OR serverBd.iCurrentNonMissionEntityVehicleSearchingForSpawnCoords = iVehicle
			
				IF serverBd.iCurrentNonMissionEntityVehicleSearchingForSpawnCoords != iVehicle
					serverBd.iCurrentNonMissionEntityVehicleSearchingForSpawnCoords = iVehicle
					PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - CREATE_VEHICLE_CREATION - iVehicle #", iVehicle, " now searching for spawn coordinates.")
				ENDIF
				
				MODEL_NAMES vehModel = GET_FMP_VEHICLE_MODEL(GET_VARIATION(),GET_SUBVARIATION(), iVehicle, GET_EXTRA_PARAM_FOR_VEHICLE_SPAWN(iVehicle), GB_GET_LOCAL_PLAYER_GANG_BOSS())
				
				IF NOT REQUEST_LOAD_MODEL(vehModel)
					PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] eVEHICLESTATE_CREATE - Loading iVehicle #", iVehicle, " model ", GET_MODEL_NAME_FOR_DEBUG(vehModel))
					RETURN FALSE
				ENDIF
				
				IF IS_VECTOR_ZERO(serverBd.vSpawnNonMissionEntityVehicleCoords)
					IF SHOULD_VEHICLE_USE_SHARED_SPAWN_COORD(iVehicle)
						VECTOR vCoords, vSpawnHeading
						FLOAT fHeading
						SWITCH serverBd.iGetSpawnNonMissionEntityVehicleCoordsStage
							CASE 0
								CLEAR_CUSTOM_VEHICLE_NODES()
								
								INT iLoop
								WHILE NOT IS_VECTOR_ZERO(GET_FMP_SHARED_VEHICLE_SPAWN_COORD(GET_VARIATION(), iLoop, IS_VEHICLE_A_SUPPORT_VEHICLE(iVehicle)))
									vCoords = GET_FMP_SHARED_VEHICLE_SPAWN_COORD(GET_VARIATION(), iLoop, IS_VEHICLE_A_SUPPORT_VEHICLE(iVehicle))
									fHeading = GET_FMP_SHARED_VEHICLE_SPAWN_HEADING(GET_VARIATION(), iLoop, IS_VEHICLE_A_SUPPORT_VEHICLE(iVehicle))
									IF NOT IS_VECTOR_ZERO(vCoords)
										ADD_CUSTOM_VEHICLE_NODE(vCoords, fHeading)		
										PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] [SHARED_SPAWN] - CREATE_VEHICLE_CREATION - Adding custom vehicle node ", iLoop, " at ", vCoords, " for iVehicle #", iVehicle)
									ENDIF
									iLoop++
								ENDWHILE	
								serverBd.iGetSpawnNonMissionEntityVehicleCoordsStage++
								PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] [SHARED_SPAWN] - CREATE_VEHICLE_CREATION - Added custom vehicle nodes. Moving on to find spawn coord.")
							BREAK
							
							CASE 1
								VEHICLE_SPAWN_LOCATION_PARAMS Params
								Params.bEnforceMinDistForCustomNodes 		= FALSE
								Params.bStartOfMissionPVSpawn 				= FALSE
								Params.bIgnoreCustomNodesForArea 			= TRUE
								Params.bAvoidSpawningInExclusionZones  		= FALSE
								Params.bIgnoreCustomNodesForMissionLaunch 	= TRUE
								Params.bCheckEntityArea 					= TRUE
								
								vCoords = GET_FMP_SHARED_VEHICLE_SPAWN_COORD(GET_VARIATION(), iLoop, IS_VEHICLE_A_SUPPORT_VEHICLE(iVehicle))
								vSpawnHeading = <<0.0, 0.0, 0.0>>
								
								IF HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(vCoords, vSpawnHeading, vehModel, FALSE, serverBd.vSpawnNonMissionEntityVehicleCoords, serverBd.fSpawnNonMissionEntityVehicleHeading, Params)
									PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] [SHARED_SPAWN] - CREATE_VEHICLE_CREATION - iVehicle #", iVehicle, " has got safe spawn at ", serverBd.vSpawnNonMissionEntityVehicleCoords)
									serverBd.iGetSpawnNonMissionEntityVehicleCoordsStage++
								ELSE
									PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] [SHARED_SPAWN] - CREATE_VEHICLE_CREATION - Failed to find spawn for iVehicle #", iVehicle)
								ENDIF
							BREAK
							
							CASE 2
								
							BREAK
						ENDSWITCH
					ELSE
						IF bDoOkCheck
							VECTOR vCoords = GET_FMP_VEHICLE_SPAWN_COORDS(GET_VARIATION(), GET_SUBVARIATION(), iVehicle, DEFAULT, GET_EXTRA_PARAM_FOR_VEHICLE_SPAWN(iVehicle), GET_RESPAWN_LOCATION())
							FLOAT fRadius = GET_RADIUS_FROM_CONTRABAND_ENTITY(vehModel)
							IF NOT IS_POINT_OK_FOR_NET_ENTITY_CREATION(vCoords,fRadius,fRadius,fRadius,0.0,FALSE,FALSE,FALSE,0.0,FALSE,DEFAULT,FALSE,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
								bShouldFindSpawnLocation = TRUE
								PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - CREATE_VEHICLE_CREATION - iVehicle #", iVehicle, " - bShouldFindSpawnLocation set to true as location not safe.")
							ENDIF
						ENDIF
						
						IF bCreateFromOffset
							VECTOR vOffsetCoord = GET_CREATE_FROM_OFFSET_VEHICLE_COORD(iVehicle)
							FLOAT fOffsetHeading = GET_CREATE_FROM_OFFSET_VEHICLE_HEADING(iVehicle)
							serverBD.vSpawnNonMissionEntityVehicleCoords = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vOffsetCoord, fOffsetHeading, GET_CREATE_FROM_OFFSET_OFFSET(iVehicle)) 
							serverBd.fSpawnNonMissionEntityVehicleHeading = fOffsetHeading
							PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - CREATE_VEHICLE_CREATION - iVehicle #", iVehicle, " being created from offset. Offset coord: ", vOffsetCoord)
						// If we're spawning a vehicle up in the area somewhere
						ELIF bShouldSpawnInAir
							VECTOR vCoordInAir = GET_VEHICLE_FIND_SPAWN_CENTRE(iVehicle)
							
							IF bShouldFindSpawnLocation
								PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - CREATE_VEHICLE_CREATION - iVehicle #", iVehicle, " finding spawn location in air using centre point ", vCoordInAir)
								vCoordInAir = GET_OFFSET_FROM_COORD_IN_WORLD_COORDS(vCoordInAir, 0.0, <<(-1*GET_VEHICLE_SPAWN_DISTANCE(iVehicle) * SIN(TO_FLOAT(serverBD.iCircCount)*30)), (GET_VEHICLE_SPAWN_DISTANCE(iVehicle) * COS(TO_FLOAT(serverBD.iCircCount)*30)), 0.0>>)
							ENDIF
							
							// Handle special spawn cases e.g. above water
							IF SHOULD_USE_GROUND_Z_FOR_AIR_SPAWNING()
								BOOL bGotExactHeight
								IF GET_GROUND_Z_FOR_3D_COORD(vCoordInAir, vCoordInAir.z, TRUE)
									bGotExactHeight = TRUE
									PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - CREATE_VEHICLE_CREATION - iVehicle #", iVehicle, " got ", vCoordInAir.z, " as exact ground z.")
								ENDIF
								IF NOT bGotExactHeight
		        					vCoordInAir.z = GET_APPROX_HEIGHT_FOR_POINT(vCoordInAir.x, vCoordInAir.y)
									PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - CREATE_VEHICLE_CREATION - iVehicle #", iVehicle, " couldn't get exact ground z. Approx height is ", vCoordInAir.z)
								ENDIF
							ENDIF
		        			vCoordInAir.z += GET_SPAWN_IN_AIR_HEIGHT(iVehicle)
		        
		        			IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(vCoordInAir, 20.0, 1, 1, 15, TRUE, TRUE, TRUE, 180)
		            			serverBD.vSpawnNonMissionEntityVehicleCoords = vCoordInAir
								serverBd.fSpawnNonMissionEntityVehicleHeading = GET_VEHICLE_FIND_SPAWN_HEADING(iVehicle)
		        			ELSE
		        				serverBD.iCircCount++
					            IF serverBD.iCircCount >= 12
					                serverBD.iCircCount = 0
					            ENDIF
							ENDIF
							
						// If we're spawning a vehicle up on the ground somewhere
						ELIF bShouldFindSpawnLocation
							
							VEHICLE_SPAWN_LOCATION_PARAMS Params
							Params.bConsiderHighways = SHOULD_CONSIDER_HIGHWAYS_FOR_VARIATION(iVehicle)
							Params.bConsiderOnlyActiveNodes = !SHOULD_ALLOW_OFF_ROAD_NODES(iVehicle)
							Params.bAvoidSpawningInExclusionZones = TRUE
							Params.bAllowFallbackToInactiveNodes = FALSE
							Params.bCheckEntityArea = TRUE
							Params.fMaxDistance = GET_VEHICLE_SPAWN_LOCATION_MAX_DISTANCE()
							Params.fMinDistFromCoords = GET_VEHICLE_SPAWN_LOCATION_MIN_DISTANCE()
							Params.bIgnoreCustomNodesForArea = SHOULD_IGNORE_CUSTOM_NODES()
							VECTOR vSpawnCoords = GET_VEHICLE_FIND_SPAWN_CENTRE(iVehicle)
							IF NOT IS_VEHICLE_A_SUPPORT_VEHICLE(iVehicle)
								IF IS_VECTOR_ZERO(serverBD.vVehicleSpawnCoord)
									serverBD.vVehicleSpawnCoord = GET_RANDOM_SPAWN_COORD_FOR_VEHICLE(iVehicle, vSpawnCoords)
								ENDIF
								vSpawnCoords = serverBD.vVehicleSpawnCoord
							ENDIF
							IF NOT IS_VECTOR_ZERO(vSpawnCoords)
								#IF IS_DEBUG_BUILD
								IF IS_VEHICLE_A_SUPPORT_VEHICLE(iVehicle)
									PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] [SUPPORT] - CREATE_VEHICLE_CREATION - iVehicle #", iVehicle, " is support vehicle ", GET_SUPPORT_VEHICLE_INDEX(iVehicle), ". Should be created near ", vSpawnCoords)
								ELSE
									PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - CREATE_VEHICLE_CREATION - iVehicle #", iVehicle, " should be created near ", vSpawnCoords)
								ENDIF
								#ENDIF
								
								HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(vSpawnCoords, vSpawnCoords, vehModel, FALSE, serverBD.vSpawnNonMissionEntityVehicleCoords, serverBD.fSpawnNonMissionEntityVehicleHeading, Params)
							ELSE
								PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - CREATE_VEHICLE_CREATION - vSpawnCoords is zero vector ", vSpawnCoords)
							ENDIF
						// If we're spawning a vehicle at exact coordinates
						ELSE
							serverBd.vSpawnNonMissionEntityVehicleCoords = GET_FMP_VEHICLE_SPAWN_COORDS(GET_VARIATION(), GET_SUBVARIATION(), iVehicle, GET_CURRENT_DROP_OFF(GET_DROP_OFF_INT_FOR_SPAWN_COORDS(iVehicle)), GET_EXTRA_PARAM_FOR_VEHICLE_SPAWN(iVehicle),GET_RESPAWN_LOCATION())
							serverBd.fSpawnNonMissionEntityVehicleHeading = GET_FMP_VEHICLE_SPAWN_HEADING(GET_VARIATION(), GET_SUBVARIATION(), iVehicle, GET_CURRENT_DROP_OFF(GET_DROP_OFF_INT_FOR_SPAWN_COORDS(iVehicle)), GET_EXTRA_PARAM_FOR_VEHICLE_SPAWN(iVehicle),GET_RESPAWN_LOCATION())
							PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - CREATE_VEHICLE_CREATION - iVehicle #", iVehicle, " should be created at server coords ", serverBd.vSpawnNonMissionEntityVehicleCoords)
							PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - CREATE_VEHICLE_CREATION - iVehicle #", iVehicle, " should be created at server heading ", serverBd.fSpawnNonMissionEntityVehicleHeading)
						ENDIF
					ENDIF
				ELSE
					DELETE_VEHICLE_GEN_VEHICLES_IN_AREA(serverBD.vSpawnNonMissionEntityVehicleCoords, 30.0)
					CLEAR_AREA(serverBD.vSpawnNonMissionEntityVehicleCoords, 30.0, FALSE, FALSE, FALSE, TRUE)
					PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - CREATE_VEHICLE_CREATION - Calling CREATE_NET_VEHICLE on iVehicle #", iVehicle, " with coords ", serverBd.vSpawnNonMissionEntityVehicleCoords)
					IF CREATE_NET_VEHICLE(serverBD.sVehicle[iVehicle].netId, vehModel, serverBd.vSpawnNonMissionEntityVehicleCoords, serverBd.fSpawnNonMissionEntityVehicleHeading, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, GET_FMP_VEHICLE_SHOULD_BOOT_BE_OPEN())	
						PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - CREATE_VEHICLE_CREATION - Created iVehicle #", iVehicle, " with model ", GET_MODEL_NAME_FOR_DEBUG(vehModel)," at ", serverBd.vSpawnNonMissionEntityVehicleCoords)
						IF bCreateFromOffset 
							SET_ENTITY_COORDS_NO_OFFSET(NET_TO_VEH(serverBD.sVehicle[iVehicle].netId), serverBD.vSpawnNonMissionEntityVehicleCoords)
						ENDIF
						SET_VEHICLE_BITSET_DECORATOR(NET_TO_VEH(serverBD.sVehicle[iVehicle].netId), MP_DECORATOR_BS_NON_MODDABLE_VEHICLE)
						SET_VEHICLE_CANNOT_BE_STORED_IN_PROPERTIES(NET_TO_VEH(serverBD.sVehicle[iVehicle].netId))
						IF SHOULD_VEHICLE_BE_INVINCIBLE_AT_START(iVehicle)
							SET_ENTITY_INVINCIBLE(NET_TO_VEH(serverBD.sVehicle[iVehicle].netId), TRUE)
						ENDIF
						NETWORK_ALLOW_REMOTE_ATTACHMENT_MODIFICATION(NET_TO_ENT(serverBD.sVehicle[iVehicle].netId), TRUE)
						ACTIVATE_DAMAGE_TRACKER_ON_NETWORK_ID(serverBD.sVehicle[iVehicle].netId, TRUE)
						serverBD.iCurrentNonMissionEntityVehicleSearchingForSpawnCoords = (-1)
						SET_MODEL_AS_NO_LONGER_NEEDED(vehModel)
						CLEAR_VEHICLE_BIT(iVehicle, eVEHICLEBITSET_I_HAVE_PERFORMED_MODEL_OVERRIDE_CHECK)
						
						IF CLEAR_FREEZE_COLLISION_FLAG_FOR_VEHICLES(iVehicle)
							SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(NET_TO_ENT(serverBD.sVehicle[iVehicle].netId), FALSE)
							PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - CREATE_VEHICLE_CREATION, SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(FALSE) - iVehicle #", iVehicle, " with model ", GET_MODEL_NAME_FOR_DEBUG(vehModel))
						ENDIF
						
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_CREATE_VEHICLE_FROM_OFFSET(INT iVehicle)
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			SWITCH iVehicle
				CASE 0
					RETURN TRUE
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC CREATE_MISSION_VEHICLE(INT iVehicle)

	IF DOES_VARIATION_HAVE_VEHICLES()
		IF SHOULD_SPAWN_VEHICLES()
			IF CREATE_VEHICLE_CREATION(iVehicle, SHOULD_FIND_SPAWN_LOCATION_FOR_VEHICLE(iVehicle), SHOULD_SPAWN_VEHICLE_IN_THE_AIR(iVehicle), SHOULD_DO_OK_TO_SPAWN_CHECK(iVehicle), SHOULD_CREATE_VEHICLE_FROM_OFFSET(iVehicle))
				SET_VEHICLE_ATTRIBUTES_FOR_SPAWN(iVehicle)
				SET_VEHICLE_STATE(iVehicle, GET_AFTER_CREATION_VEHICLE_STATE(iVehicle))
				PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - CREATE_MISSION_VEHICLE - Created iVehicle #", iVehicle, " at ", serverBd.vSpawnNonMissionEntityVehicleCoords)
				RESET_SERVER_VEHICLE_SPAWN_DATA()
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL SHOULD_FAIL_MODE_FOR_DESTROYED_PREREQ_VEHICLE()

	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			RETURN TRUE
	ENDSWITCH

	RETURN FALSE
ENDFUNC

FUNC BOOL IS_IT_SAFE_TO_CLEANUP_VEHICLE(INT iVeh)
	IF IS_VEHICLE_AN_AMBUSH_VEHICLE(iVeh)
	OR IS_VEHICLE_A_SUPPORT_VEHICLE(iVeh)
		RETURN TRUE
	ENDIF
	
	IF IS_VEHICLE_A_CARRIER_VEHICLE(iVeh)
		SWITCH GET_VARIATION()
			CASE PLACEHOLDERVAR_MAX
				RETURN HAVE_ALL_MISSION_ENTITIES_LEFT_CARRIER_VEHICLE()
		ENDSWITCH
	
		RETURN FALSE
	ENDIF
	
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			RETURN FALSE
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_VEHICLES_RESPAWN_ON_VARIATION()
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_VEHICLE_RESPAWN(INT iVehicle)
	
	IF IS_VEHICLE_A_SUPPORT_VEHICLE(iVehicle)
	OR IS_VEHICLE_AN_AMBUSH_VEHICLE(iVehicle)
	OR IS_VEHICLE_A_CARRIER_VEHICLE(iVehicle)
		RETURN FALSE
	ENDIF
	
	IF GET_VEHICLE_STATE(iVehicle) < eVEHICLESTATE_NOT_DRIVEABLE
		RETURN FALSE
	ENDIF

	VECTOR vVehCoords = GET_FMP_VEHICLE_SPAWN_COORDS(GET_VARIATION(), GET_SUBVARIATION(), iVehicle, GET_CURRENT_DROP_OFF(GET_CURRENT_DROP_OFF_INT(TRUE)),GET_EXTRA_PARAM_FOR_VEHICLE_SPAWN(iVehicle),GET_RESPAWN_LOCATION())

	IF IS_VECTOR_ZERO(vVehCoords)
		RETURN FALSE
	ENDIF
		
	IF NOT IS_POINT_OK_FOR_NET_ENTITY_CREATION(vVehCoords,2,2,2,5.0,TRUE,TRUE,TRUE,15.0,FALSE,DEFAULT,FALSE,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE

ENDFUNC

FUNC BOOL DOES_VARIATION_VEHICLE_NEED_CRATE(INT iVehicle)
	UNUSED_PARAMETER(iVehicle)
	
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE

ENDFUNC

INT iTintIndex = -1
PROC SET_VEHICLE_CRATE_TINT(OBJECT_INDEX objId, MODEL_NAMES eModel)
	IF iTintIndex = (-1)
		iTintIndex = GET_RANDOM_INT_IN_RANGE(0, 4)
	ENDIF
	
	IF eModel = INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_GR_rsply_crate04a"))
	OR eModel = INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_GR_rsply_crate04b"))	
		SET_OBJECT_TINT_INDEX(objId, iTintIndex)
	ENDIF
ENDPROC

FUNC MODEL_NAMES GET_CRATE_MODEL_FOR_VEHICLE(MODEL_NAMES eVehicle)
	SWITCH eVehicle 
		CASE FLATBED		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Arcade_Collect_01a"))
		CASE WASTELANDER	RETURN INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_auto_salvage_stromberg"))
		CASE AMBULANCE		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_x17_Bag_Med_01a")) 
		CASE RIOT			RETURN hei_prop_carrier_crate_01a 
		
		CASE BALLER2		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_vw_ped_Business_01a"))
		CASE LANDSTALKER	RETURN INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_vw_ped_Epsilon_01a"))
		CASE TORNADO2		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_vw_ped_Hooker_01a"))
		CASE EMPEROR2		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_vw_ped_Hillbilly_01a"))
		CASE BISON2			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Toolbox_01a"))
		CASE BURRITO2		RETURN prop_cardbordbox_05a
		
		CASE MESA3			RETURN ZANCUDO_SHIPMENT_ITEM_MODEL()
		
		CASE BURRITO		RETURN PROP_TOOLCHEST_02
		CASE TROPIC2		RETURN xm_prop_rsply_crate04a
		
		CASE STOCKADE		RETURN xm_prop_rsply_crate04a
		
		CASE FBI2			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_crate_01a"))
		
		CASE DINGHY2		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Ch_Crate_Empty_01a"))
		CASE DODO			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_crate_01a"))
		
		CASE MULE4			RETURN prop_boxpile_05a
		
		CASE MINIVAN		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_duffelbag_01x"))
		CASE TULA			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Ch_Crate_01a"))
		
		CASE FUGITIVE		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_ped_rug_01a"))
		CASE BOXVILLE		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Ch_ChemSet_01b"))
	ENDSWITCH
	RETURN Prop_box_wood04a
ENDFUNC

FUNC BOOL SHOULD_DISABLE_COLLISIONS_FOR_VEHICLE_CRATE(INT iMissionEntity, VEHICLE_INDEX vehID)

	UNUSED_PARAMETER(iMissionEntity)

	IF GET_ENTITY_MODEL(vehID) = INSURGENT2
	OR GET_ENTITY_MODEL(vehID) = BURRITO
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC INT GET_LOD_DIST_FOR_VEHICLE_CRATE(MODEL_NAMES crateModel)
	IF crateModel = INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_auto_salvage_stromberg"))
	OR crateModel = INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_x17_Bag_Med_01a"))
		RETURN 1200
	ENDIF
	
	RETURN 100
ENDFUNC

FUNC BOOL CREATE_CRATE_FOR_VEHICLE(INT iVehicle, VEHICLE_INDEX vehID)

	MODEL_NAMES crateModel = GET_CRATE_MODEL_FOR_VEHICLE(GET_ENTITY_MODEL(vehID))

	IF crateModel != DUMMY_MODEL_FOR_SCRIPT
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.vehCrate[iVehicle])
			IF CAN_REGISTER_MISSION_OBJECTS( 1 )
				REQUEST_MODEL(crateModel)
				IF HAS_MODEL_LOADED(crateModel)
					IF CREATE_NET_OBJ(serverBD.vehCrate[iVehicle],crateModel, GET_ENTITY_COORDS(vehID)+<<0.0,0.0,150.0>>)
						NETWORK_ALLOW_REMOTE_ATTACHMENT_MODIFICATION(NET_TO_ENT(serverBD.vehCrate[iVehicle]), TRUE)
						PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - CREATE_CRATE_FOR_VEHICLE - Crate created, now we'll attach it...")
						IF SHOULD_DISABLE_COLLISIONS_FOR_VEHICLE_CRATE(iVehicle, vehID)
							PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - CREATE_CRATE_FOR_VEHICLE - SHOULD_DISABLE_COLLISIONS_FOR_VEHICLE_CRATE is TRUE, disabling collisions for crate in iVehicle #", iVehicle)
							SET_ENTITY_COMPLETELY_DISABLE_COLLISION(NET_TO_OBJ(serverBD.vehCrate[iVehicle]), FALSE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF TAKE_CONTROL_OF_NET_ID(serverBD.vehCrate[iVehicle])
				IF ATTACH_CRATE_TO_VEHICLE(NET_TO_OBJ(serverBD.vehCrate[iVehicle]), vehID)
					PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - CREATE_CRATE_FOR_VEHICLE - Crate attached!")
					SET_VEHICLE_CRATE_TINT(NET_TO_OBJ(serverBD.vehCrate[iVehicle]), crateModel)
					SET_MODEL_AS_NO_LONGER_NEEDED(crateModel)
					SET_ENTITY_LOD_DIST(NET_TO_OBJ(serverBD.vehCrate[iVehicle]), GET_LOD_DIST_FOR_VEHICLE_CRATE(crateModel))
					SET_ENTITY_VISIBLE(NET_TO_OBJ(serverBD.vehCrate[iVehicle]),TRUE)
					SET_ENTITY_INVINCIBLE(NET_TO_OBJ(serverBD.vehCrate[iVehicle]),TRUE)
					IF DOES_ENTITY_HAVE_PHYSICS(NET_TO_OBJ(serverBD.vehCrate[iVehicle])) 
					AND GET_IS_ENTITY_A_FRAG(NET_TO_OBJ(serverBD.vehCrate[iVehicle]))
						SET_DISABLE_BREAKING(NET_TO_OBJ(serverBD.vehCrate[iVehicle]), TRUE)
						SET_DISABLE_FRAG_DAMAGE(NET_TO_OBJ(serverBD.vehCrate[iVehicle]), TRUE)
					ENDIF
					PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - CREATE_CRATE_FOR_VEHICLE - SET_DISABLE_BREAKING(NET_TO_OBJ(serverBD.vehCrate[iVehicle]), TRUE)")
					//SET_ENTITY_DYNAMIC(NET_TO_OBJ(serverBD.vehCrate[iVehicle]),FALSE)
					SET_ENTITY_PROOFS(NET_TO_OBJ(serverBD.vehCrate[iVehicle]),TRUE,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ELSE
		PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - CREATE_CRATE_FOR_VEHICLE - crateModel = DUMMY_MODEL_FOR_SCRIPT")
		RETURN FALSE
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC INT GET_VEHICLE_TO_ATTACH_TO(INT iVehicle)

	UNUSED_PARAMETER(iVehicle)
	
	RETURN -1

ENDFUNC

FUNC VECTOR GET_OFFSET_FOR_VEHICLE_ATTACHMENT(MODEL_NAMES vehModel, MODEL_NAMES vehModelToAttachTo)

	SWITCH(vehModelToAttachTo)
		CASE FLATBED
			SWITCH vehModel
				CASE SANDKING		RETURN <<0.0, -2.5, 1.4>>
				DEFAULT				RETURN <<0.0, -2.5, 1.15>>
			ENDSWITCH
		BREAK
	ENDSWITCH

	RETURN <<0.0, -1.25, -0.1>>

ENDFUNC

FUNC VECTOR GET_ROTATION_FOR_VEHICLE_ATTACHMENT(MODEL_NAMES vehModelToAttachTo)
	
	SWITCH(vehModelToAttachTo)
		CASE FLATBED		RETURN <<0.0, 0.0, 180.0>>
	ENDSWITCH

	RETURN <<0.0, 0.0, 90.0>>

ENDFUNC

FUNC BOOL ATTACH_VEHICLE_TO_VEHICLE(VEHICLE_INDEX vehID, VEHICLE_INDEX vehicleToAttachTo)

	VECTOR vOffset = GET_OFFSET_FOR_VEHICLE_ATTACHMENT(GET_ENTITY_MODEL(vehID), GET_ENTITY_MODEL(vehicleToAttachTo))

	IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(vehID, vehicleToAttachTo)
		STRING sBone = "chassis_dummy"
		ATTACH_ENTITY_TO_ENTITY(vehID, vehicleToAttachTo, GET_ENTITY_BONE_INDEX_BY_NAME(vehicleToAttachTo, sBone), vOffset, GET_ROTATION_FOR_VEHICLE_ATTACHMENT(GET_ENTITY_MODEL(vehicleToAttachTo)), FALSE, FALSE, TRUE)
		PRINTLN("ATTACH_VEHICLE_TO_VEHICLE: Attaching vehicle to vehicle using ATTACH_ENTITY_TO_ENTITY. Bone = ", sBone)
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL SHOULD_DETACH_VEHICLE_FROM_VEHICLE(INT iVehicle)

	UNUSED_PARAMETER(iVehicle)

	RETURN FALSE

ENDFUNC

PROC MAINTAIN_ATTACH_VEHICLE_TO_VEHICLE(INT iVehicle)

	INT iVehicleToAttachTo = GET_VEHICLE_TO_ATTACH_TO(iVehicle)
	
	IF iVehicleToAttachTo = -1
		EXIT
	ENDIF
	
	VEHICLE_INDEX vehID = NET_TO_VEH(serverBD.sVehicle[iVehicle].netId)
	
	IF NOT IS_VEHICLE_BIT_SET(iVehicle,  eVEHICLEBITSET_ATTACHED_VEHICLE_TO_VEHICLE)
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[iVehicleToAttachTo].netId)
		AND TAKE_CONTROL_OF_NET_ID(serverBD.sVehicle[iVehicleToAttachTo].netId)
			VEHICLE_INDEX vehIDToAttachTo = NET_TO_VEH(serverBD.sVehicle[iVehicleToAttachTo].netId)
			IF IS_VEHICLE_DRIVEABLE(vehID)
			AND IS_VEHICLE_DRIVEABLE(vehIDToAttachTo)

				IF ATTACH_VEHICLE_TO_VEHICLE(vehID, vehIDToAttachTo)
					PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_ATTACH_VEHICLE_TO_VEHICLE - Attached iVehicle #", iVehicle," to vehicle ", iVehicleToAttachTo)
					SET_VEHICLE_BIT(iVehicle, eVEHICLEBITSET_ATTACHED_VEHICLE_TO_VEHICLE)
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_DETACHED_VEHICLE_TO_VEHICLE)
			IF SHOULD_DETACH_VEHICLE_FROM_VEHICLE(iVehicle)
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[iVehicleToAttachTo].netId)
					IF IS_VEHICLE_DRIVEABLE(vehID)
						DETACH_ENTITY(vehID, FALSE, FALSE)
						VECTOR vEntityCoords = GET_ENTITY_COORDS(vehID)
						
						SET_ENTITY_COORDS_NO_OFFSET(vehID, vEntityCoords)
						SET_VEHICLE_BIT(iVehicle, eVEHICLEBITSET_DETACHED_VEHICLE_TO_VEHICLE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

FUNC BOOL SHOULD_CLEANUP_VEHICLE(INT iVehicle)
	UNUSED_PARAMETER(iVehicle)
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_SEARCH_AREA_FROM_VEHICLE(INT iVehicle)
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			SWITCH iVehicle
				CASE 0		RETURN ciSEARCH_AREA_TWO
				CASE 1		RETURN ciSEARCH_AREA_THREE
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN -1
ENDFUNC

FUNC BOOL SHOULD_WARP_VEHICLE_ON_STUCK_FLAG_BE_CLEARED_ON_ENTRY(INT iVehicle)
	UNUSED_PARAMETER(iVehicle)
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_VEHICLE_NEED_VEHICLE_FIRE(INT iVehicle)
	UNUSED_PARAMETER(iVehicle)
	
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			RETURN TRUE
	ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_VEHICLE_FIRE(INT iVehicle)
	IF NOT DOES_VEHICLE_NEED_VEHICLE_FIRE(iVehicle)
		EXIT
	ENDIF
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
		VEHICLE_INDEX vehID = NET_TO_VEH(serverBD.sVehicle[iVehicle].netId)
		IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_VEHICLE_FIRES_STARTED)			
			IF NOT REQUEST_PTFX_ASSETS()
				EXIT
			ENDIF
			USE_PARTICLE_FX_ASSET("scr_xm_riotvan")			
			serverBD.vehPtfxID[ciPTFX_VEH_FRONT] = START_NETWORKED_PARTICLE_FX_LOOPED_ON_ENTITY("scr_xm_riotvan_fire_front", vehID, <<0.0,0.0,0.0>>, <<0.0,0.0,0.0>>)
			
			USE_PARTICLE_FX_ASSET("scr_xm_riotvan")
			serverBD.vehPtfxID[ciPTFX_VEH_BACK] = START_NETWORKED_PARTICLE_FX_LOOPED_ON_ENTITY("scr_xm_riotvan_fire_back", vehID, <<0.0,0.0,0.0>>, <<0.0,0.0,0.0>>)
					
			SET_PARTICLE_FX_LOOPED_EVOLUTION(serverBD.vehPtfxID[ciPTFX_VEH_FRONT], "strength", 1, FALSE)
			SET_PARTICLE_FX_LOOPED_EVOLUTION(serverBD.vehPtfxID[ciPTFX_VEH_BACK], "strength", 1, FALSE)
			
			IF DOES_PARTICLE_FX_LOOPED_EXIST(serverBD.vehPtfxID[ciPTFX_VEH_FRONT])
				PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_VEHICLE_FIRE - Setting iVehicle #", iVehicle, " on fire due to ciFMMC_VEHICLE7_SPAWN_BURNING.")
				SET_SERVER_BIT(eSERVERBITSET_VEHICLE_FIRES_STARTED)
				
				PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_VEHICLE_FIRE - Adding decal in slot 0.")
				VECTOR vVanOffset = -GET_ENTITY_FORWARD_VECTOR(vehID) * 2.5
				VECTOR vDecalPos = GET_ENTITY_COORDS(vehID) + vVanOffset
				VECTOR vDecalRot = GET_ENTITY_FORWARD_VECTOR(vehID)
				
				decalId[0] = ADD_DECAL(DECAL_RSID_GENERIC_SCORCH, vDecalPos, vDecalRot, vDecalRot, 2.5, 2.5, 1.0, 1.0, 1.0, 1.0, 99000)
			ELSE
				PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_VEHICLE_FIRE - Trying to spawn the effect again.")
			ENDIF
		ELSE
			
			IF IS_SERVER_BIT_SET(eSERVERBITSET_START_VEHICLE_FIRES_EXTINGUISH_TIMER)
			AND NOT IS_SERVER_BIT_SET(eSERVERBITSET_VEHICLE_FIRES_SHOULD_EXTINGUISH)
				IF NOT HAS_NET_TIMER_STARTED(serverBD.stGenericMissionTimer)
					START_NET_TIMER(serverBD.stGenericMissionTimer)
					PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_VEHICLE_FIRE - Extinguish timer started.")
				ELSE
					IF HAS_NET_TIMER_EXPIRED(serverBD.stGenericMissionTimer, 5000)
						PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_VEHICLE_FIRE - Extinguish timer expired.")
						SET_SERVER_BIT(eSERVERBITSET_VEHICLE_FIRES_SHOULD_EXTINGUISH)
					ENDIF
				ENDIF
			ENDIF
			
			//Making the fire go out when it has lost all strength
			IF IS_SERVER_BIT_SET(eSERVERBITSET_VEHICLE_FIRES_SHOULD_EXTINGUISH)
			AND NOT IS_SERVER_BIT_SET(eSERVERBITSET_VEHICLE_FIRES_EXTINGUISHED)
				IF DOES_PARTICLE_FX_LOOPED_EXIST(serverBD.vehPtfxID[ciPTFX_VEH_FRONT])
					IF NOT REQUEST_PTFX_ASSETS()
						EXIT
					ENDIF
					USE_PARTICLE_FX_ASSET("scr_xm_riotvan")
					START_NETWORKED_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_xm_riotvan_extinguish", vehID, <<0.0,2.0,0.0>>, <<0.0,0.0,0.0>>)
					STOP_PARTICLE_FX_LOOPED(serverBD.vehPtfxID[ciPTFX_VEH_FRONT])
					PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_VEHICLE_FIRE - Getting rid of front flames on iVehicle #", iVehicle)
				ENDIF
				
				IF DOES_PARTICLE_FX_LOOPED_EXIST(serverBD.vehPtfxID[ciPTFX_VEH_BACK])
					IF NOT REQUEST_PTFX_ASSETS()
						EXIT
					ENDIF
					USE_PARTICLE_FX_ASSET("scr_xm_riotvan")
					START_NETWORKED_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_xm_riotvan_extinguish", vehID, <<0.0,-2.0,0.0>>, <<0.0,0.0,0.0>>)
					STOP_PARTICLE_FX_LOOPED(serverBD.vehPtfxID[ciPTFX_VEH_BACK])
					PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_VEHICLE_FIRE - Getting rid of back flames on iVehicle #", iVehicle)
				ENDIF	
				
				SET_SERVER_BIT(eSERVERBITSET_VEHICLE_FIRES_EXTINGUISHED)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC DELETE_CRATES_AFTER_VEH_DESTROYED(INT iVehicle)
	IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.vehCrate[iVehicle])
		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.vehCrate[iVehicle])
		OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.vehCrate[iVehicle]) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
			IF TAKE_CONTROL_OF_NET_ID(serverBD.vehCrate[iVehicle])
				SET_ENTITY_INVINCIBLE(NET_TO_OBJ(serverBD.vehCrate[iVehicle]), FALSE)
				DELETE_NET_ID(serverBD.vehCrate[iVehicle])
				PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - DELETE_CRATES_AFTER_VEH_DESTROYED, DELETE_NET_ID, iVehicle #", iVehicle)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_CHECK_VEHICLE_IS_INTERIOR(INT iVehicle)
	UNUSED_PARAMETER(iVehicle)
	
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_VEHICLE_INTERIOR_CHECKS(INT iVehicle)
	IF NOT SHOULD_CHECK_VEHICLE_IS_INTERIOR(iVehicle)
		EXIT
	ENDIF
	
	IF GET_VEHICLE_STATE(iVehicle) > eVEHICLESTATE_CREATE
	AND GET_VEHICLE_STATE(iVehicle) < eVEHICLESTATE_NOT_DRIVEABLE
		IF IS_VEHICLE_IN_ANY_STORED_MISSION_INTERIOR(iVehicle)
			SET_VEHICLE_BIT(iVehicle, eVEHICLEBITSET_INSIDE_MISSION_INTERIOR)
		ELSE
			CLEAR_VEHICLE_BIT(iVehicle, eVEHICLEBITSET_INSIDE_MISSION_INTERIOR)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_FAIL_MISSION_ON_CRITICAL_VEHICLE_DEATH()
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			RETURN GET_MODE_STATE() != eMODESTATE_LEAVE_AREA
	ENDSWITCH
	
	RETURN GET_END_REASON() = eENDREASON_NO_REASON_YET
ENDFUNC

FUNC BOOL SHOULD_DELETE_MISSION_ENTITY_PICKUP_ON_VEHICLE_DESTROYED()
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX			
			RETURN TRUE
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_MISSION_ENTITY_BE_DELETED_ON_VEHICLE_DESTROYED(INT iMissionEntity, INT iVehicle)
	UNUSED_PARAMETER(iMissionEntity)
	UNUSED_PARAMETER(iVehicle)
	
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX			
			RETURN TRUE
	ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_VEHICLE_BRAINS()

	INT iVehicle
	INT iSupportVehCount, iCarrierVehCount, iActiveTotalVehCount, iTargetVehDestroyedCount
	
//	VEHICLE_INDEX vehId
//	MODEL_NAMES eModel
	
	REPEAT serverBD.iNumMissionVehicles iVehicle
		
		IF SHOULD_CLEANUP_VEHICLE(iVehicle)
			SET_VEHICLE_STATE(iVehicle, eVEHICLESTATE_NO_LONGER_NEEDED)
		ENDIF
		
		MAINTAIN_VEHICLE_FIRE(iVehicle)
		MAINTAIN_VEHICLE_INTERIOR_CHECKS(iVehicle)
		
		IF IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_TARGET_VEHICLE)
		AND IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_DESTROYED)
			iTargetVehDestroyedCount++
		ENDIF
		
		SWITCH GET_VEHICLE_STATE(iVehicle)
			
			CASE eVEHICLESTATE_INACTIVE
				
			BREAK
			
			CASE eVEHICLESTATE_WAITING_FOR_RESPAWN
				IF DOES_FMP_VARIATION_HAVE_AMBUSH(GET_VARIATION(), GET_SUBVARIATION())
				AND IS_VEHICLE_AN_AMBUSH_VEHICLE(iVehicle)
					IF HAS_AMBUSH_RESPAWN_DELAY_PASSED(TRUE)
						SET_VEHICLE_STATE(iVehicle, eVEHICLESTATE_CREATE)
						PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] [AMBUSH] - eVEHICLESTATE_WAITING_FOR_RESPAWN - iVehicle #", iVehicle, " moved to eVEHICLESTATE_CREATE after respawn delay.")
					ENDIF
				ENDIF
			BREAK
			
			CASE eVEHICLESTATE_CREATE
				
				IF OK_TO_CREATE_MISSION_VEHICLE(iVehicle)
					MAINTAIN_AMBUSH_VEHICLE_MODEL_SETUP(iVehicle)
					CREATE_MISSION_VEHICLE(iVehicle)
				ENDIF
				
			BREAK
			
			CASE eVEHICLESTATE_DRIVEABLE
				
				// Need to do this once nearby.
				IF IS_NET_VEHICLE_DRIVEABLE(serverBD.sVehicle[iVehicle].netId)
				
//					vehId = NET_TO_VEH(serverBD.sVehicle[iVehicle].netId)
//					
//					eModel = GET_ENTITY_MODEL(vehId)
					
					iActiveTotalVehCount++
					IF IS_VEHICLE_A_SUPPORT_VEHICLE(iVehicle)
						iSupportVehCount++
					ELIF IS_VEHICLE_A_CARRIER_VEHICLE(iVehicle)
						iCarrierVehCount++
					ENDIF
					
					IF HAS_NET_TIMER_EXPIRED(stDogfightAmbushActionTimer, 5000)
						IF NOT IS_VEHICLE_A_SUPPORT_VEHICLE(iVehicle)
						AND NOT IS_VEHICLE_AN_AMBUSH_VEHICLE(iVehicle)
							RESET_NET_TIMER(stDogfightAmbushActionTimer)
						ENDIF
					ENDIF
					
					IF IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_I_AM_RADIO_VEHICLE)
						IF NOT IS_VEHICLE_EMPTY_AND_STATIONARY(NET_TO_VEH(serverBD.sVehicle[iVehicle].netId))	
							CLEAR_VEHICLE_BIT(iVehicle, eVEHICLEBITSET_I_AM_RADIO_VEHICLE)
						ENDIF
					ENDIF
					
					IF IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_WARP_VEHICLE_ON_STUCK)
						IF SHOULD_WARP_VEHICLE_ON_STUCK_FLAG_BE_CLEARED_ON_ENTRY(iVehicle)
							IF GET_MISSION_ENTITY_VEHICLE_IS_CARRYING(iVehicle) != -1
							AND IS_MISSION_ENTITY_BIT_SET(GET_MISSION_ENTITY_VEHICLE_IS_CARRYING(iVehicle), eMISSIONENTITYBITSET_I_HAVE_BEEN_COLLECTED_FOR_FIRST_TIME)
								PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] eVEHICLESTATE_DRIVEABLE - iVehicle #", iVehicle, " has been entered, clearing eVEHICLEBITSET_WARP_VEHICLE_ON_STUCK.")
								CLEAR_VEHICLE_BIT(iVehicle, eVEHICLEBITSET_WARP_VEHICLE_ON_STUCK)
							ENDIF
						ENDIF
					ENDIF

				ELSE
					IF IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_I_AM_PREREQUISITE_VEHICLE)
						IF SHOULD_FAIL_MODE_FOR_DESTROYED_PREREQ_VEHICLE()
							MOVE_MISSION_TO_REWARDS(eENDREASON_PREREQUISITE_VEHICLE_DESTROYED)
						ENDIF
					ENDIF
					IF IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_I_AM_CARRIER_VEHICLE)
						IF NOT HAVE_ALL_MISSION_ENTITIES_IN_CARRIER_VEHICLE_BEEN_DELIVERED(iVehicle,TRUE)
							IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.sVehicle[iVehicle].netId)
								SET_SERVER_BIT(eSERVERBITSET_ANY_CARRIER_VEHICLE_DESTROYED)
							ENDIF
						ENDIF
					ENDIF
					SET_VEHICLE_STATE(iVehicle, eVEHICLESTATE_NOT_DRIVEABLE)
				ENDIF	
			BREAK
			
			CASE eVEHICLESTATE_NOT_DRIVEABLE
				
				INT iMissionEntity
				IF SHOULD_DELETE_MISSION_ENTITY_PICKUP_ON_CARRIER_DESTROYED()
					REPEAT GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() iMissionEntity
						IF DOES_MISSION_ENTITY_HAVE_CARRIER_VEHICLE(iMissionEntity)
							IF iVehicle = GET_MISSION_ENTITY_CARRIER_VEHICLE(iMissionEntity)
							AND NOT IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_DELIVERED)
								SET_MISSION_ENTITY_BIT(iMissionEntity, eMISSIONENTITYBITSET_DESTROYED)
								SET_MISSION_ENTITY_BIT(iMissionEntity, eMISSIONENTITYBITSET_RECEIVED_DESTROYED_DAMAGE_EVENT)
							ENDIF
						ENDIF
					ENDREPEAT
				ELIF SHOULD_DELETE_MISSION_ENTITY_PICKUP_ON_VEHICLE_DESTROYED()
					REPEAT GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() iMissionEntity
						IF SHOULD_MISSION_ENTITY_BE_DELETED_ON_VEHICLE_DESTROYED(iMissionEntity, iVehicle)
							SET_MISSION_ENTITY_BIT(iMissionEntity, eMISSIONENTITYBITSET_DESTROYED)
							SET_MISSION_ENTITY_BIT(iMissionEntity, eMISSIONENTITYBITSET_RECEIVED_DESTROYED_DAMAGE_EVENT)
						ENDIF
					ENDREPEAT
				ENDIF
				
				IF IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_I_AM_MISSION_CRITICAL_VEHICLE)
					IF SHOULD_FAIL_MISSION_ON_CRITICAL_VEHICLE_DEATH()
						MOVE_MISSION_TO_REWARDS(eENDREASON_VEHICLE_DESTROYED)
					ENDIF
				ENDIF
				
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
					
					IF IS_IT_SAFE_TO_CLEANUP_VEHICLE(iVehicle)
						CLEANUP_NET_ID(serverBD.sVehicle[iVehicle].netId)
						PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - eVEHICLESTATE_NOT_DRIVEABLE - iVehicle #", iVehicle, " cleaned up as no longer driveable.")
					ENDIF
					
				ENDIF
				
				IF DOES_FMP_VARIATION_HAVE_AMBUSH(GET_VARIATION(), GET_SUBVARIATION())
				AND IS_VEHICLE_AN_AMBUSH_VEHICLE(iVehicle)
					IF SHOULD_RESPAWN_AMBUSH_VEHICLE(iVehicle)
						RESET_VEHICLE_DATA_FOR_RESPAWN(iVehicle)
						SET_VEHICLE_STATE(iVehicle, eVEHICLESTATE_WAITING_FOR_RESPAWN)
						PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] [AMBUSH] - eVEHICLESTATE_NOT_DRIVEABLE - iVehicle #", iVehicle, " set to eVEHICLESTATE_WAITING_FOR_RESPAWN.")
					ENDIF
				ENDIF
				
				IF GET_SEARCH_AREA_FROM_VEHICLE(iVehicle) != ciSEARCH_AREA_NONE
					IF NOT HAS_SEARCH_AREA_BEEN_SHRUNK(GET_SEARCH_AREA_FROM_VEHICLE(iVehicle))
						SET_SEARCH_AREA_BIT(GET_SEARCH_AREA_FROM_VEHICLE(iVehicle), eSEARCHAREABITSET_AREA_SHRUNK)
						PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] [AMBUSH] - eVEHICLESTATE_NOT_DRIVEABLE - iVehicle #", iVehicle, " is no longer driveable, search area ", GET_SEARCH_AREA_FROM_VEHICLE(iVehicle), " set to shrunk.")
					ENDIF
				ENDIF
			BREAK
			
			CASE eVEHICLESTATE_WARP
				
			BREAK
			
			CASE eVEHICLESTATE_NO_LONGER_NEEDED

			BREAK
			
			CASE eVEHICLESTATE_RESET
				
			BREAK
			
		ENDSWITCH
		
		//If vehicles should respawn
		IF SHOULD_VEHICLES_RESPAWN_ON_VARIATION()
			IF SHOULD_VEHICLE_RESPAWN(iVehicle)
				SET_VEHICLE_STATE(iVehicle, eVEHICLESTATE_CREATE)
			ENDIF
		ENDIF
		
		// Detect if the ped is dead and set state accordingly.
		IF GET_VEHICLE_STATE(iVehicle) > eVEHICLESTATE_CREATE
			IF GET_VEHICLE_STATE(iVehicle) != eVEHICLESTATE_NOT_DRIVEABLE
			AND GET_VEHICLE_STATE(iVehicle) != eVEHICLESTATE_WARP
				IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
					PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - iVehicle #", iVehicle, " does not exist.")
					SET_VEHICLE_STATE(iVehicle, eVEHICLESTATE_NOT_DRIVEABLE)
				ELSE
					IF IS_ENTITY_DEAD(NET_TO_VEH(serverBD.sVehicle[iVehicle].netId))
					OR NOT IS_NET_VEHICLE_DRIVEABLE(serverBD.sVehicle[iVehicle].netId)
						PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - iVehicle #", iVehicle, " is dead.")
						SET_VEHICLE_STATE(iVehicle, eVEHICLESTATE_NOT_DRIVEABLE)
					ELIF IS_VEHICLE_AN_AMBUSH_VEHICLE(iVehicle) 
					AND ARE_ALL_AMBUSH_PEDS_IN_VEHICLE_DEAD(iVehicle)
						PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] [AMBUSH] - iVehicle #", iVehicle, " cleaned up since all its peds are dead.")
						SET_VEHICLE_STATE(iVehicle, eVEHICLESTATE_NOT_DRIVEABLE)
					ENDIF
				ENDIF

			ENDIF
			
			IF GET_VEHICLE_STATE(iVehicle) >= eVEHICLESTATE_NOT_DRIVEABLE
				DELETE_CRATES_AFTER_VEH_DESTROYED(iVehicle)
			ENDIF
		ENDIF	
			
	ENDREPEAT
	
	IF serverBD.iTargetVehDestroyedCount <> iTargetVehDestroyedCount
		serverBD.iTargetVehDestroyedCount = iTargetVehDestroyedCount
	ENDIF
	
	IF serverBD.iActiveCarrierVehCount <> iCarrierVehCount
		serverBD.iActiveCarrierVehCount = iCarrierVehCount
	ENDIF
	
	IF serverBD.iActiveSuppportVehCount <> iSupportVehCount
		serverBD.iActiveSuppportVehCount = iSupportVehCount
	ENDIF
	
	IF serverBd.iActiveTotalVehCount <> iActiveTotalVehCount
		serverBd.iActiveTotalVehCount = iActiveTotalVehCount
	ENDIF
ENDPROC

FUNC BOOL SHOULD_REMOVE_ANCHOR_ON_NO_LONGER_NEEDED(INT iVehicle)
	UNUSED_PARAMETER(iVehicle)
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_UNLOCK_VEHICLE_DOORS_FOR_ALL_PLAYERS_ON_NO_LONGER_NEEDED(INT iVehicle)
	UNUSED_PARAMETER(iVehicle)
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_DESTROY_UNDRIVEABLE_VEHICLES_IN_VARIATION(INT iVehicle)

	UNUSED_PARAMETER(iVehicle)

	RETURN FALSE
ENDFUNC

PROC MAINTAIN_DESTROY_UNDRIVEABLE_VEHICLE(INT iVehicle)
	IF SHOULD_DESTROY_UNDRIVEABLE_VEHICLES_IN_VARIATION(iVehicle)
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
			IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
				IF IS_ENTITY_ALIVE(NET_TO_VEH(serverBD.sVehicle[iVehicle].netId))
					IF NOT IS_VEHICLE_DRIVEABLE(NET_TO_VEH(serverBD.sVehicle[iVehicle].netId))
					OR GET_ENTITY_HEALTH(NET_TO_VEH(serverBD.sVehicle[iVehicle].netId)) < (GET_ENTITY_MAX_HEALTH(NET_TO_VEH(serverBD.sVehicle[iVehicle].netId)) / 5)
					OR GET_VEHICLE_ENGINE_HEALTH(NET_TO_VEH(serverBD.sVehicle[iVehicle].netId)) < 150
						PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_DESTROY_UNDRIVEABLE_VEHICLE - iVehicle #", iVehicle, " is no longer driveable, destroying it.")
						NETWORK_EXPLODE_VEHICLE(NET_TO_VEH(serverBD.sVehicle[iVehicle].netId))
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_DO_DESTROY_HELICOPTER_CHECKS(INT iVehicle)
	UNUSED_PARAMETER(iVehicle)
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_HELICOPTER_TOO_LOW(VEHICLE_INDEX vehId #IF IS_DEBUG_BUILD , INT iVehicle #ENDIF )
	VECTOR vHelicoords = GET_ENTITY_COORDS(vehId, FALSE)
	FLOAT fZPosition
	
	IF GET_GROUND_Z_FOR_3D_COORD(vHelicoords, fZPosition, TRUE)
		IF vHelicoords.z < fZPosition + 30
			PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_SHOULD_DESTROY_HELICOPTER - IS_HELICOPTER_TOO_LOW(", iVehicle, ") - Heli Z: ", vHelicoords.z, " - Ground Z: ", fZPosition)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_SHOULD_DESTROY_HELICOPTER(INT iVehicle)
	IF NOT SHOULD_DO_DESTROY_HELICOPTER_CHECKS(iVehicle)
		EXIT
	ENDIF
	
	IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
		VEHICLE_INDEX vehId = NET_TO_VEH(serverBD.sVehicle[iVehicle].netId)
		IF NOT IS_ENTITY_DEAD(vehId)
			IF /*IS_HELICOPTER_DAMAGED(vehId #IF IS_DEBUG_BUILD , iVehicle #ENDIF )
			OR*/ IS_HELICOPTER_TOO_LOW(vehId #IF IS_DEBUG_BUILD , iVehicle #ENDIF )
				NETWORK_EXPLODE_HELI(vehId, TRUE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

INT iLastScriptVehicleInside = -1
PROC MAINTAIN_WARPING_VEHICLES_ON_DELIVERY(INT iVehicle)
	
	IF IS_NET_PLAYER_OK(PLAYER_ID())
	AND IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), GET_DROP_OFF_COORDS(), 10.0)
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			VEHICLE_INDEX vehID = NET_TO_VEH(serverBD.sVehicle[iVehicle].netId)
			IF vehID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			AND iLastScriptVehicleInside != iVehicle
				iLastScriptVehicleInside = iVehicle
				PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] eVEHICLESTATE_WARP - Player is inside iVehicle #", iVehicle, " near bunker.")
			ENDIF
		ENDIF
	ELIF iLastScriptVehicleInside != (-1)
	AND NOT g_FreemodeDeliveryData.bDeliveryScriptTriggeredCutScene
	AND NOT NETWORK_IS_IN_MP_CUTSCENE()
		iLastScriptVehicleInside = -1
		PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] eVEHICLESTATE_WARP - Clearing last vehicle index. Player is no longer in range of bunker.")
	ENDIF
	
	IF iVehicle = iLastScriptVehicleInside
		IF g_FreemodeDeliveryData.bDeliveryScriptTriggeredCutScene
		AND NETWORK_IS_IN_MP_CUTSCENE()
			BROADCAST_EVENT_WARP_BUYSELL_SCRIPT_VEHICLE(iVehicle, FALSE, serverBD.iLaunchPosix)
			iLastScriptVehicleInside = -1
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_CARRIER_VEHICLES_BE_MADE_DESTROYABLE_FOR_VARIATION()
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			RETURN TRUE
	ENDSWITCH
	
	RETURN HAS_ANY_MISSION_ENTITY_BEEN_COLLECTED_FOR_THE_FIRST_TIME()
ENDFUNC

FUNC BOOL SHOULD_VEHICLE_BE_MADE_DESTROYABLE(INT iVehicle)
	IF IS_PED_SITTING_IN_THIS_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.sVehicle[iVehicle].netId))
		IF IS_VEHICLE_BIT_SET(iVehicle,eVEHICLEBITSET_I_AM_PREREQUISITE_VEHICLE)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_VEHICLE_BIT_SET(iVehicle,eVEHICLEBITSET_I_AM_CARRIER_VEHICLE)
		IF SHOULD_CARRIER_VEHICLES_BE_MADE_DESTROYABLE_FOR_VARIATION()
			INT iMissionEntity
			REPEAT GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() iMissionEntity
				IF DOES_MISSION_ENTITY_HAVE_CARRIER_VEHICLE(iMissionEntity)
					IF iVehicle = GET_MISSION_ENTITY_CARRIER_VEHICLE(iMissionEntity)
						IF IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_SOMEONE_IS_NEAR_ME)
						OR IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_I_HAVE_BEEN_COLLECTED_FOR_FIRST_TIME)
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
	
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_TIME_STUCK_TIL_RESET()
	RETURN 10000
ENDFUNC

FUNC INT GET_TIME_JAMMED_TIL_RESET()
	RETURN 15000
ENDFUNC

FUNC BOOL IS_VEHICLE_STUCK(VEHICLE_INDEX vehID)

	RETURN IS_VEHICLE_STUCK_TIMER_UP(vehID, VEH_STUCK_ON_ROOF, GET_TIME_STUCK_TIL_RESET())
	OR IS_VEHICLE_STUCK_TIMER_UP(vehID, VEH_STUCK_ON_SIDE, GET_TIME_STUCK_TIL_RESET())
	OR IS_VEHICLE_STUCK_TIMER_UP(vehID, VEH_STUCK_JAMMED, GET_TIME_JAMMED_TIL_RESET())
	OR IS_VEHICLE_STUCK_TIMER_UP(vehID, VEH_STUCK_HUNG_UP, GET_TIME_STUCK_TIL_RESET())

ENDFUNC

FUNC BOOL SHOULD_DO_WARP_HELP()
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_VEHICLE_BE_WARPED(VEHICLE_INDEX vehID, BOOL &bDoHelpOnWarp)

	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
		BREAK
	ENDSWITCH

	IF IS_VEHICLE_STUCK(vehID)
		bDoHelpOnWarp = SHOULD_DO_WARP_HELP()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC FLOAT GET_MIN_DIST_VEHICLE_WARP_COORDS()

	RETURN 20.0

ENDFUNC

FUNC FLOAT GET_MAX_DIST_VEHICLE_WARP_COORDS()

	RETURN 150.0

ENDFUNC

PROC MAINTAIN_WARPING_VEHICLES(NETWORK_INDEX netVeh)

	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(netVeh)

		VEHICLE_INDEX vehID = NET_TO_VEH(netVeh)
		MODEL_NAMES model = GET_ENTITY_MODEL(vehID)
		BOOL bDoHelpOnWarp
		
		IF DOES_ENTITY_EXIST(vehID)
		AND NOT IS_ENTITY_DEAD(vehID,TRUE)
			
			IF SHOULD_VEHICLE_BE_WARPED(vehID, bDoHelpOnWarp)
				
				IF bDoHelpOnWarp
					IF IS_LOCAL_PLAYER_IN_PARTICIPATION_RANGE(150.0)
						TRIGGER_HELP(eHELPTEXT_STUCK_VEHICLE_WARPED)
					ENDIF
				ENDIF
				
				IF MAINTAIN_CONTROL_OF_NETWORK_ID(netVeh)
					PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_WARPING_STUCK_VEHICLES - Vehicle is stuck. Finding warp location.")
					
					VECTOR vSpawnLocation
					FLOAT fSpawnHeading
					VEHICLE_SPAWN_LOCATION_PARAMS vehicleSpawnLocationParams
					
					vehicleSpawnLocationParams.fMinDistFromCoords = GET_MIN_DIST_VEHICLE_WARP_COORDS()
					vehicleSpawnLocationParams.fMaxDistance = GET_MAX_DIST_VEHICLE_WARP_COORDS()
					vehicleSpawnLocationParams.bConsiderHighways = TRUE
					vehicleSpawnLocationParams.bCheckEntityArea = TRUE
					vehicleSpawnLocationParams.bCheckOwnVisibility = FALSE
					vehicleSpawnLocationParams.bConsiderOnlyActiveNodes = TRUE
					
					IF HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(GET_ENTITY_COORDS(vehID), <<0.0, 0.0, 0.0>>, model, TRUE, vSpawnLocation, fSpawnHeading, vehicleSpawnLocationParams)									  
						PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_WARPING_STUCK_VEHICLES - Warping vehicle to ", vSpawnLocation)
						
						NETWORK_FADE_IN_ENTITY(vehID, TRUE)
						SET_ENTITY_COORDS_NO_OFFSET(vehID, vSpawnLocation)
						SET_ENTITY_HEADING(vehID, fSpawnHeading)
						SET_VEHICLE_ON_GROUND_PROPERLY(vehID)
						
						SET_CLIENT_BIT(eCLIENTBITSET_VEHICLE_WARPED)
					ENDIF
					
				ENDIF
			ENDIF
		ENDIF

	ENDIF
	
ENDPROC

FUNC BOOL SHOULD_VEHICLE_BE_UNLOCKED_ON_CONDITION(INT iVehicle)
	UNUSED_PARAMETER(iVehicle)
	
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_VEHICLE_SPEED_BE_ALTERED(INT iVehicle, VEHICLE_INDEX vehID)
	UNUSED_PARAMETER(iVehicle)
	UNUSED_PARAMETER(vehId)
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_REMOVE_ANCHOR(INT iVehicle, VEHICLE_INDEX vehID)
	UNUSED_PARAMETER(iVehicle)
	UNUSED_PARAMETER(vehId)
	
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC FLOAT GET_ALTERED_VEHICLE_SPEED(VEHICLE_INDEX vehID)
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			FLOAT fMaxDistanceSquared
			FLOAT fMinDistanceSquared
			FLOAT fAlpha
			
			fMaxDistanceSquared = ciCELEB_DISPOSE_OF_CAR_MAX_ALTER_VEHICLE_SPEED_DISTANCE_M * ciCELEB_DISPOSE_OF_CAR_MAX_ALTER_VEHICLE_SPEED_DISTANCE_M
			fMinDistanceSquared = ciCELEB_DISPOSE_OF_CAR_MIN_ALTER_VEHICLE_SPEED_DISTANCE_M * ciCELEB_DISPOSE_OF_CAR_MIN_ALTER_VEHICLE_SPEED_DISTANCE_M
			fAlpha = FMAX(0.0, VDIST2(GET_ENTITY_COORDS(vehID), <<-1101.266602,4924.347656,215.437164>>) - fMinDistanceSquared) / (fMaxDistanceSquared - fMinDistanceSquared)
			
			RETURN LERP_FLOAT(ciCELEB_DISPOSE_OF_CAR_ALTER_VEHICLE_SPEED_MIN, ciCELEB_DISPOSE_OF_CAR_ALTER_VEHICLE_SPEED_MAX, fAlpha)
	ENDSWITCH
	
	RETURN 30.0
ENDFUNC

FUNC BOOL SHOULD_EMPTY_AND_LOCK_VEHICLE_ON_END(MODEL_NAMES eModel, INT iVehicle)
	UNUSED_PARAMETER(eModel)
	
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			IF GET_END_REASON() = eENDREASON_TIME_UP
				RETURN IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_I_AM_CARRIER_VEHICLE)
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_RUN_VEHICLE_DISTANCE_CHECKS()

	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			RETURN TRUE
	ENDSWITCH

	RETURN FALSE
ENDFUNC

FUNC BOOL IS_VEHICLE_VALID_FOR_CLOSEST_CHECK(INT iVehicle)
	UNUSED_PARAMETER(iVehicle)
	
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			RETURN IS_VEHICLE_A_CARRIER_VEHICLE(iVehicle)
	ENDSWITCH	
		
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_ANY_PED_IN_VEHICLE(INT iVeh)
	PED_INDEX tempPed
	VEHICLE_INDEX tempVeh = NET_TO_VEH(serverBD.sVehicle[iVeh].netId)
	INT i
	
	IF DOES_ENTITY_EXIST(tempVeh)
		FOR i= -1 TO ENUM_TO_INT(VS_EXTRA_RIGHT_3)
			tempPed = GET_PED_IN_VEHICLE_SEAT(tempVeh, INT_TO_ENUM(VEHICLE_SEAT, i))
			IF DOES_ENTITY_EXIST(tempPed)
				IF NOT IS_ENTITY_DEAD(tempPed)
				AND NOT IS_PED_INJURED(tempPed)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SUPPORT_VEHICLE_BLIP_CHECK(INT iVehicle)

	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			RETURN FALSE
	ENDSWITCH

	RETURN IS_VEHICLE_EMPTY(NET_TO_VEH(serverBD.sVehicle[iVehicle].netId))
ENDFUNC

FUNC INT TRAILER_ID_FOR_VEHICLE(INT iVehicle)
	UNUSED_PARAMETER(iVehicle)
	RETURN 0
ENDFUNC

FUNC INT TRAILER_ID_FOR_MISSION_ENTITY(INT iMissionEntity)
	UNUSED_PARAMETER(iMissionEntity)
	RETURN 0
ENDFUNC

FUNC BOOL SHOULD_VEHICLE_DISPLAY_UNIQUE_BLIP(INT iVehicle)

	UNUSED_PARAMETER(iVehicle)

	IF NOT bSafeToDisplay
		RETURN FALSE
	ENDIF
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
	AND IS_NET_VEHICLE_DRIVEABLE(serverBD.sVehicle[iVehicle].netId)

		IF IS_VEHICLE_A_SUPPORT_VEHICLE(iVehicle)
			IF SUPPORT_VEHICLE_BLIP_CHECK(iVehicle)
				RETURN TRUE
			ELSE
				RETURN FALSE
			ENDIF
		ENDIF
		
		SWITCH GET_VARIATION()
			CASE PLACEHOLDERVAR_MAX
				RETURN TRUE
		ENDSWITCH
		
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_VEH_BLIP_FLASH_CONTINUOUSLY(INT iVehicle)
	UNUSED_PARAMETER(iVehicle)
	
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_VEH_BLIP_FLASH_ON_CREATION(INT iVehicle)
	UNUSED_PARAMETER(iVehicle)
	
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_RADAR_FLASH_WITH_VEH_BLIP(INT iVehicle)
	UNUSED_PARAMETER(iVehicle)
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_FADE_BLIP_BASED_ON_DIST(INT iVehicle)
	IF IS_VEHICLE_A_SUPPORT_VEHICLE(iVehicle)
		RETURN TRUE
	ENDIF
	
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC HUD_COLOURS GET_SUPPORT_BLIP_COLOUR()
	RETURN HUD_COLOUR_WHITE
ENDFUNC

FUNC BLIP_SPRITE GET_UNIQUE_VEHICLE_BLIP_SPRITE(INT iVehicle, VEHICLE_INDEX vehId)
	UNUSED_PARAMETER(iVehicle)
	UNUSED_PARAMETER(vehId)

	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX	
			RETURN RADAR_TRACE_ENEMY_HELI_SPIN
	ENDSWITCH

	RETURN RADAR_TRACE_GANG_VEHICLE
ENDFUNC

FUNC BOOL SHOULD_ROTATE_BLIP(INT iVehicle, VEHICLE_INDEX vehId)
	
	IF (IS_VEHICLE_A_SUPPORT_VEHICLE(iVehicle)
	AND NOT IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(vehId))
	AND USE_ZANCUDO_SUPPORT_VEHICLE_SPAWNS())
	OR GET_UNIQUE_VEHICLE_BLIP_SPRITE(iVehicle, vehId) = RADAR_TRACE_PLANE_DROP
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC HUD_COLOURS GET_VEHICLE_BLIP_COLOUR(INT iVehicle)
	UNUSED_PARAMETER(iVehicle)

	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			RETURN HUD_COLOUR_RED
	ENDSWITCH
	
	RETURN HUD_COLOUR_BLUE
ENDFUNC

FUNC STRING GET_VEHICLE_BLIP_TEXT_LABEL(INT iVehicle)
	UNUSED_PARAMETER(iVehicle)
	
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			RETURN "CSH_BLIP_VEH"
	ENDSWITCH
	
	RETURN "CSH_BLIP_ENY"
ENDFUNC

FUNC BOOL SHOULD_UPDATE_VEHICLE_BLIP_COLOUR(INT iVehicle)
	UNUSED_PARAMETER(iVehicle)
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX		
			IF GET_BLIP_COLOUR(blipVariationVehBlip[iVehicle]) != GET_BLIP_COLOUR_FROM_HUD_COLOUR(GET_VEHICLE_BLIP_COLOUR(iVehicle))
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC REMOVE_VEHICLE_UNIQUE_BLIPS()
	INT iVehicle
	REPEAT GB_GET_FMP_NUM_VEH_REQUIRED(GET_VARIATION(), GET_SUBVARIATION(), GB_GET_LOCAL_PLAYER_GANG_BOSS(), FALSE, GET_EXTRA_PARAM_FOR_VEHICLE_SPAWN(), TRUE, FALSE) iVehicle
		IF DOES_BLIP_EXIST(blipVariationVehBlip[iVehicle])
			REMOVE_BLIP(blipVariationVehBlip[iVehicle])
			PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] REMOVE_VEHICLE_UNIQUE_BLIPS - removed blip from sVehicle[", iVehicle, "].netId")
		ENDIF
	ENDREPEAT
ENDPROC

PROC MAINTAIN_VEHICLE_UNIQUE_BLIP(INT iVehicle)

	//PRINTLN("[MAINTAIN_VEHICLE_UNIQUE_BLIP] iVehicle ",iVehicle)
	BOOL bExists = FALSE
	BOOL bAlive = FALSE

	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
		bExists = TRUE
		IF IS_VEHICLE_DRIVEABLE(NET_TO_VEH(serverBD.sVehicle[iVehicle].netId))
			bAlive = TRUE
		ENDIF
	ENDIF
	
	IF ((bExists AND bAlive) OR (IS_BLIP_FLASHING_ON_DESTRUCTION(blipVariationVehBlip[iVehicle],!bAlive,iVehFlashingBS,iVehicle)))
	AND SHOULD_VEHICLE_DISPLAY_UNIQUE_BLIP(iVehicle)
		IF NOT DOES_BLIP_EXIST(blipVariationVehBlip[iVehicle])
			IF IS_VEHICLE_A_SUPPORT_VEHICLE(iVehicle)
				ADD_MISSION_BLIP_FOR_ENTITY(blipVariationVehBlip[iVehicle], NET_TO_ENT(serverBD.sVehicle[iVehicle].netId), GET_SUPPORT_VEHICLE_BLIP_SPRITE(), GET_SUPPORT_BLIP_COLOUR(), GET_SUPPORT_VEHICLE_BLIP_NAME(FALSE), FALSE, FALSE, (NOT HAS_HELP_BEEN_DONE(eHELPTEXT_EXPLAIN_SUPPORT_VEHICLES)))
			ELSE
				SWITCH GET_VARIATION()
					CASE PLACEHOLDERVAR_MAX
						ADD_MISSION_BLIP_FOR_ENTITY(blipVariationVehBlip[iVehicle], NET_TO_ENT(serverBD.sVehicle[iVehicle].netId), GET_UNIQUE_VEHICLE_BLIP_SPRITE(iVehicle, NET_TO_VEH(serverBD.sVehicle[iVehicle].netId)), GET_VEHICLE_BLIP_COLOUR(iVehicle), GET_VEHICLE_BLIP_TEXT_LABEL(iVehicle))
					BREAK
				ENDSWITCH
				
				IF SHOULD_RADAR_FLASH_WITH_VEH_BLIP(iVehicle)
					FLASH_MINIMAP_DISPLAY()
				ENDIF
				
				IF SHOULD_VEH_BLIP_FLASH_ON_CREATION(iVehicle)
					FLASH_MISSION_BLIP(blipVariationVehBlip[iVehicle])
				ENDIF
			ENDIF
			#IF IS_DEBUG_BUILD
			INT ent = NATIVE_TO_INT(NET_TO_ENT(serverBD.sVehicle[iVehicle].netId))
			PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_VEHICLE_UNIQUE_BLIP - added blip for sVehicle[", iVehicle, "].netId, and entity - ", ent)
			#ENDIF
		ELSE
			IF SHOULD_ROTATE_BLIP(iVehicle, NET_TO_VEH(serverBD.sVehicle[iVehicle].netId))
				SET_BLIP_ROTATION(blipVariationVehBlip[iVehicle], ROUND(GET_ENTITY_HEADING_FROM_EULERS(NET_TO_VEH(serverBD.sVehicle[iVehicle].netId))))
			ENDIF
			
			IF SHOULD_UPDATE_VEHICLE_BLIP_COLOUR(iVehicle)
				SET_BLIP_COLOUR_FROM_HUD_COLOUR(blipVariationVehBlip[iVehicle], GET_VEHICLE_BLIP_COLOUR(iVehicle))
			ENDIF
			
			IF SHOULD_FADE_BLIP_BASED_ON_DIST(iVehicle)
				IF IS_VEHICLE_A_SUPPORT_VEHICLE(iVehicle)
					SET_BLIP_ALPHA_BASED_ON_DIST_FROM_PLAYER(blipVariationVehBlip[iVehicle], 50)
				ELSE
					SET_BLIP_ALPHA_BASED_ON_DIST_FROM_PLAYER(blipVariationVehBlip[iVehicle], 100, 5000)
				ENDIF
			ENDIF
			
			IF SHOULD_VEH_BLIP_FLASH_CONTINUOUSLY(iVehicle)
				PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_VEHICLE_UNIQUE_BLIP - SHOULD_VEH_BLIP_FLASH_CONTINUOUSLY, iVehicle #", iVehicle)
				IF NOT IS_BLIP_FLASHING(blipVariationVehBlip[iVehicle])
					FLASH_MISSION_BLIP(blipVariationVehBlip[iVehicle])
				ENDIF
			ELIF SHOULD_VEH_BLIP_FLASH_ON_CREATION(iVehicle)
				// Let it stop by itself
			ELSE
				IF IS_BLIP_FLASHING(blipVariationVehBlip[iVehicle])
					SET_BLIP_FLASHES(blipVariationVehBlip[iVehicle], FALSE)
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF DOES_BLIP_EXIST(blipVariationVehBlip[iVehicle])
			REMOVE_BLIP(blipVariationVehBlip[iVehicle])
			#IF IS_DEBUG_BUILD
			INT ent = NATIVE_TO_INT(NET_TO_ENT(serverBD.sVehicle[iVehicle].netId))
			PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_VEHICLE_UNIQUE_BLIP - removed blip from sVehicle[", iVehicle, "].netId, and entity - ", ent)
			#ENDIF
		ENDIF
	ENDIF

ENDPROC

FUNC BOOL SHOULD_ALLOW_USE_AFTER_MISSION(MODEL_NAMES vehModel, INT iVehicle)
	UNUSED_PARAMETER(vehModel)
	IF IS_VEHICLE_A_SUPPORT_VEHICLE(iVehicle)
		
	ENDIF
	
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			IF GET_END_REASON() = eENDREASON_TIME_UP
				IF IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_I_AM_CARRIER_VEHICLE)
					RETURN FALSE
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_USE_PMSD_FOR_VEHICLE(MODEL_NAMES eModel)
	SWITCH eModel
		CASE TULA	RETURN TRUE
	ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC SET_VEHICLE_UNLOCK_STATE_FOR_VARIATION(NETWORK_INDEX netId, VEHICLE_INDEX vehId)
	
	IF MAINTAIN_CONTROL_OF_NETWORK_ID(netId)
		IF SHOULD_CARRIER_VEHICLE_NEED_BROKEN_INTO(GET_ENTITY_MODEL(vehId))
			SET_VEHICLE_DOORS_LOCKED(vehId, VEHICLELOCK_LOCKED_BUT_CAN_BE_DAMAGED)
		ELSE
			SET_VEHICLE_DOORS_LOCKED(vehID, VEHICLELOCK_UNLOCKED)	
		ENDIF
		SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(vehId, FALSE)
		SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(vehID, PLAYER_ID(), FALSE)
		SET_VEHICLE_DOORS_LOCKED_FOR_NON_SCRIPT_PLAYERS(vehId, TRUE)
		SET_VEHICLE_DISABLE_TOWING(vehId, FALSE)

		PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] SET_VEHICLE_UNLOCK_STATE_FOR_VARIATION - Set.")
	ENDIF
ENDPROC

FUNC NETWORK_INDEX GET_TRAILER_VEHICLE_NEEDS_ATTACHED_TO(INT iVehicle)
	UNUSED_PARAMETER(iVehicle)
	RETURN INT_TO_NATIVE(NETWORK_INDEX, -1)
ENDFUNC

FUNC VECTOR ATTACH_VEHICLE_OFFSET()

	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			RETURN <<0.0, -3.0, 0.690>>
	ENDSWITCH

	RETURN <<0.0, -5.5, 0.7>>
ENDFUNC

FUNC VECTOR ATTACH_VEHICLE_ROTATION()

	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			RETURN <<0.0, 0.0, 180.0>>
	ENDSWITCH

	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

PROC MAINTAIN_ATTACHING_VEHICLE_LOGIC(INT iVehicle, VEHICLE_INDEX vehID)
	IF NOT IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_NEEDS_ATTACHED_TO_TRAILER)
	OR IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_ATTACHED_TO_TRAILER)
	OR IS_VEHICLE_CLIENT_BIT_SET(iVehicle, PARTICIPANT_ID(), eVEHICLECLIENTBITSET_ATTACHED_TO_TRAILER)
		EXIT
	ENDIF
	
	NETWORK_INDEX trailerNetID = GET_TRAILER_VEHICLE_NEEDS_ATTACHED_TO(iVehicle)
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(trailerNetID)
	AND IS_NET_VEHICLE_DRIVEABLE(trailerNetID)
	AND NOT IS_ENTITY_DEAD(vehID)
		VEHICLE_INDEX trailerVehId = NET_TO_VEH(trailerNetID)
		IF IS_THIS_MODEL_A_TRUCK_CAB(GET_ENTITY_MODEL(vehID))
			IF NOT IS_VEHICLE_ATTACHED_TO_TRAILER(vehID)
				ATTACH_VEHICLE_TO_TRAILER(vehID, trailerVehId, 1.0)
				PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_ATTACHING_VEHICLE_LOGIC - Called ATTACH_VEHICLE_TO_TRAILER on iVehicle #", iVehicle)
			ELSE
				SET_VEHICLE_CLIENT_BIT(iVehicle, eVEHICLECLIENTBITSET_ATTACHED_TO_TRAILER)
				PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_ATTACHING_VEHICLE_LOGIC - Attached iVehicle #", iVehicle, " to trailer.")
			ENDIF
		ELSE
			IF NOT IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(vehID)
				//VECTOR vTrailerRot = GET_ENTITY_ROTATION(trailerVehId)
				ATTACH_ENTITY_TO_ENTITY(vehID, trailerVehId, GET_ENTITY_BONE_INDEX_BY_NAME(trailerVehId, "chassis_dummy"), ATTACH_VEHICLE_OFFSET(), ATTACH_VEHICLE_ROTATION(), FALSE, FALSE, FALSE)
				PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_ATTACHING_VEHICLE_LOGIC - Called ATTACH_ENTITY_TO_ENTITY on iVehicle #", iVehicle)
			ELSE
				SET_VEHICLE_CLIENT_BIT(iVehicle, eVEHICLECLIENTBITSET_ATTACHED_TO_TRAILER)
				PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_ATTACHING_VEHICLE_LOGIC - Attached iVehicle #", iVehicle, " to vehicle.")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_USE_PMSD_FOR_CARRIER_VEHICLES()

	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			RETURN TRUE
	ENDSWITCH	

	RETURN FALSE
ENDFUNC

PROC MAINTAIN_PMSD_FOR_CARRIER_VEHICLES(INT iVehicle, VEHICLE_INDEX vehId)
	IF NOT SHOULD_USE_PMSD_FOR_CARRIER_VEHICLES()
	OR NOT IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_I_AM_CARRIER_VEHICLE)
	OR GET_END_REASON() = eENDREASON_NO_REASON_YET
		EXIT
	ENDIF

	IF IS_VEHICLE_DRIVEABLE(vehId)
		INT iMissionEntity
		REPEAT GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() iMissionEntity
			IF iVehicle = GET_MISSION_ENTITY_CARRIER_VEHICLE(iMissionEntity)
				IF NOT IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_DELIVERED)
				AND NOT IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_DESTROYED)
					IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
						SET_ENTITY_INVINCIBLE(vehId, FALSE)
						SET_VEHICLE_ENGINE_HEALTH(vehId, -1000.0)
						PRINTLN("[PLACEHOLDER_PRINT] [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_PMSD_FOR_CARRIER_VEHICLES - Killing engine for iVehicle #", iVehicle, " as it is carrying iMissionEntity #", iMissionEntity)
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

FUNC BOOL SHOULD_ENTERING_BE_CONSIDERED_IN(INT iVehicle)
	UNUSED_PARAMETER(iVehicle)
	
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_PEDS_REACT_ON_VEHICLE_ENTRY(INT iVehicle)
	UNUSED_PARAMETER(iVehicle)
	
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_PEDS_REACT_ON_PLAYER_GETTING_IN_TO_VEHICLE(INT iVehicle)
	UNUSED_PARAMETER(iVehicle)
	
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX	
			RETURN !GET_GROUP_SHOULD_REACT(0)
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_PED_TO_REACT_ON_VEHICLE_ENTRY(INT iVehicle)
	UNUSED_PARAMETER(iVehicle)
	
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			RETURN 0
	ENDSWITCH

	RETURN -1
ENDFUNC

FUNC BOOL SHOULD_VEHICLE_SIREN_ACTIVATE(INT iVehicle)
	UNUSED_PARAMETER(iVehicle)
	
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			RETURN TRUE
	ENDSWITCH

	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_VEHICLE_SIREN_BE_MUTED(INT iVehicle)
	UNUSED_PARAMETER(iVehicle)
	
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			RETURN TRUE
	ENDSWITCH

	RETURN FALSE
ENDFUNC

FUNC FLOAT GET_NEAR_VEHICLE_RANGE(INT iVehicle)
	UNUSED_PARAMETER(iVehicle)
	
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			RETURN 3.0
	ENDSWITCH
	
	RETURN 30.0
ENDFUNC

FUNC BOOL GET_NEAR_VEHICLE_REQUIRES_LINE_OF_SIGHT(INT iVehicle)
	UNUSED_PARAMETER(iVehicle)
	
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_NEAR_VEHICLE(VEHICLE_INDEX vehID, FLOAT fRange, BOOL bCheckLoS = FALSE)
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
		BREAK
	ENDSWITCH

	IF VDIST2(GET_ENTITY_COORDS(vehID, FALSE), GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)) <= fRange*fRange
		IF bCheckLoS
			RETURN HAS_ENTITY_CLEAR_LOS_TO_ENTITY(PLAYER_PED_ID(), vehID, SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_VEHICLE|SCRIPT_INCLUDE_PED|SCRIPT_INCLUDE_RAGDOLL|SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_FOLIAGE)
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL MARK_SEARCH_AREA_FOUND_NEAR_VEHICLE()
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_CHECK_FOR_PLAYER_NEAR_VEHICLE(INT iVehicle)
	UNUSED_PARAMETER(iVehicle)
	
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_IS_PLAYER_NEAR_VEHICLE(INT iVehicle, VEHICLE_INDEX vehID)
	IF NOT SHOULD_CHECK_FOR_PLAYER_NEAR_VEHICLE(iVehicle)
		EXIT
	ENDIF
	
	IF NOT IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_PLAYER_IS_NEAR_ME)
	AND NOT IS_VEHICLE_CLIENT_BIT_SET(iVehicle, PARTICIPANT_ID(), eVEHICLECLIENTBITSET_PLAYER_IS_NEAR_ME)
		IF IS_PLAYER_NEAR_VEHICLE(vehID, GET_NEAR_VEHICLE_RANGE(iVehicle), GET_NEAR_VEHICLE_REQUIRES_LINE_OF_SIGHT(iVehicle))
			SET_VEHICLE_CLIENT_BIT(iVehicle, eVEHICLECLIENTBITSET_PLAYER_IS_NEAR_ME)
			PRINTLN("[PLACEHOLDER_PRINT] [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_IS_PLAYER_NEAR_VEHICLE - Player is near iVehicle #", iVehicle)
			
			SWITCH GET_VARIATION()
				CASE PLACEHOLDERVAR_MAX
					SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerEventData
					TickerEventData.Details.FromPlayerIndex = PLAYER_ID()
					TickerEventData.TickerEvent = TICKER_EVENT_CAR_LOCATED
					BROADCAST_TICKER_EVENT(TickerEventData, ALL_PLAYERS_IN_MY_GANG(TRUE))		
				BREAK
			ENDSWITCH
			
			IF GET_SEARCH_AREA_FROM_VEHICLE(iVehicle) != -1
				IF NOT IS_SEARCH_AREA_BIT_SET(GET_SEARCH_AREA_FROM_VEHICLE(iVehicle), eSEARCHAREABITSET_AREA_SHRUNK)
					SET_SEARCH_AREA_CLIENT_BIT(GET_SEARCH_AREA_FROM_VEHICLE(iVehicle), eSEARCHAREACLIENTBITSET_AREA_SHRUNK)
					PRINTLN("[PLACEHOLDER_PRINT] [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_IS_PLAYER_NEAR_VEHICLE - Search area ", GET_SEARCH_AREA_FROM_VEHICLE(iVehicle)," should shrink because player is near iVehicle #", iVehicle)
				ENDIF
			ENDIF
			
			IF MARK_SEARCH_AREA_FOUND_NEAR_VEHICLE()
				IF NOT IS_SEARCH_AREA_CLIENT_BIT_SET(PARTICIPANT_ID(), GET_SEARCH_AREA_FROM_VEHICLE(iVehicle), eSEARCHAREACLIENTBITSET_AREA_SEARCHED)
					SET_SEARCH_AREA_CLIENT_BIT(GET_SEARCH_AREA_FROM_VEHICLE(iVehicle), eSEARCHAREACLIENTBITSET_AREA_SEARCHED)
					PRINTLN("[PLACEHOLDER_PRINT] [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_IS_PLAYER_NEAR_VEHICLE - Search area ", GET_SEARCH_AREA_FROM_VEHICLE(iVehicle)," eSEARCHAREACLIENTBITSET_AREA_SEARCHED iVehicle #", iVehicle)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_SPOOK_VEHICLE(INT iVehicle, VEHICLE_INDEX vehID)

	IF NOT RUN_SPOOK_LOGIC(iVehicle)
		EXIT
	ENDIF
	
	BOOL bPlayerInAirVeh = PLAYER_USING_OPPRESSOR() OR IS_PED_IN_ANY_HELI(PLAYER_PED_ID()) OR IS_PED_IN_ANY_PLANE(PLAYER_PED_ID()) 
	
	FLOAT fSpookDist = BAD_PRESS_SPOOK_DIST
	
	IF bPlayerInAirVeh 
		fSpookDist = fSpookDist * 4
	ENDIF
	
	IF IS_PLAYER_NEAR_VEHICLE(vehID, fSpookDist)
		IF NOT IS_VEHICLE_CLIENT_BIT_SET(iVehicle, PARTICIPANT_ID(), eVEHICLECLIENTBITSET_SPOOKED)
			SET_VEHICLE_CLIENT_BIT(iVehicle, eVEHICLECLIENTBITSET_SPOOKED)
			PRINTLN("[PLACEHOLDER_PRINT] [", GET_CURRENT_VARIATION_STRING(), "] - [SPOOK] MAINTAIN_SPOOK_VEHICLE - iVehicle #", iVehicle, " is spooked.")
		ENDIF
	ELSE
		IF IS_VEHICLE_CLIENT_BIT_SET(iVehicle, PARTICIPANT_ID(), eVEHICLECLIENTBITSET_SPOOKED)
			CLEAR_VEHICLE_CLIENT_BIT(iVehicle, eVEHICLECLIENTBITSET_SPOOKED)
			PRINTLN("[PLACEHOLDER_PRINT] [", GET_CURRENT_VARIATION_STRING(), "] - [SPOOK] MAINTAIN_SPOOK_VEHICLE - iVehicle #", iVehicle, " is not spooked.")
		ENDIF
	ENDIF				

ENDPROC

PROC MAINTAIN_TAIL_VEHICLE(INT iVehicle, VEHICLE_INDEX vehID)
	IF NOT SHOULD_UPDATE_TAIL_VEHICLE()
	OR NOT SHOULD_TAIL_VEHICLE(iVehicle)
		EXIT
	ENDIF
	
	FLOAT fFailDistance = TO_FLOAT(ciMAX_TAIL_VEHICLE_DISTANCE_M)
		
	IF IS_PLAYER_NEAR_VEHICLE(vehID, fFailDistance)
		IF NOT IS_VEHICLE_CLIENT_BIT_SET(iVehicle, PARTICIPANT_ID(), eVEHICLECLIENTBITSET_TAILED)
			SET_VEHICLE_CLIENT_BIT(iVehicle, eVEHICLECLIENTBITSET_TAILED)
			PRINTLN("[PLACEHOLDER_PRINT] [", GET_CURRENT_VARIATION_STRING(), "] - [TAIL] MAINTAIN_TAIL_VEHICLE - This client is tailing iVehicle #", iVehicle)
		ENDIF
	ELSE
		IF IS_VEHICLE_CLIENT_BIT_SET(iVehicle, PARTICIPANT_ID(), eVEHICLECLIENTBITSET_TAILED)
			CLEAR_VEHICLE_CLIENT_BIT(iVehicle, eVEHICLECLIENTBITSET_TAILED)
			PRINTLN("[PLACEHOLDER_PRINT] [", GET_CURRENT_VARIATION_STRING(), "] - [TAIL] MAINTAIN_TAIL_VEHICLE - This client has lost iVehicle #", iVehicle)
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_FREEZE_VEHICLE_POSITION(INT iVehicle,VEHICLE_INDEX vehID)
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			SWITCH GET_SUBVARIATION()
				CASE PLACEHOLDERSUB_MAX
					IF NOT IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_FROZEN)
						IF DOES_ENTITY_EXIST(vehID)
						AND GET_ENTITY_MODEL(vehID) = JOURNEY
						AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPed[0].netId)
							FREEZE_ENTITY_POSITION(vehID, TRUE)
							PRINTLN("[PLACEHOLDER_PRINT] [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_FREEZE_VEHICLE_POSITION - Frozen position of iVehicle #", iVehicle)
							SET_VEHICLE_CLIENT_BIT(iVehicle, eVEHICLECLIENTBITSET_FROZEN)
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

PROC MAINTAIN_VEHICLE_INVINCIBILITY(INT iVehicle, VEHICLE_INDEX vehID)
	UNUSED_PARAMETER(iVehicle)
	UNUSED_PARAMETER(vehID)
ENDPROC

FUNC BOOL SHOULD_VEHICLE_BE_DESTROYED(INT iVehicle, VEHICLE_INDEX vehID)
	UNUSED_PARAMETER(iVehicle)
	UNUSED_PARAMETER(vehID)

	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
		BREAK
	ENDSWITCH
	
	RETURN FALSE

ENDFUNC

FUNC BOOL SHOULD_SET_RADIO_STATION_FOR_VEHICLE(INT iVehicle)
	UNUSED_PARAMETER(iVehicle)
	
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC STRING GET_RADIO_STATION_NAME_FOR_VEHICLE(INT iVehicle)
	UNUSED_PARAMETER(iVehicle)
	
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			RETURN "RADIO_13_JAZZ"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

PROC MAINTAIN_VEHICLE_HEALTH_DAMAGE(INT iVehicle, VEHICLE_INDEX vehID)

	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			IF GET_MODE_STATE() = eMODESTATE_PROTECT_ENTITY
				IF IS_VEHICLE_A_CARRIER_VEHICLE(iVehicle)
					IF HAS_NET_TIMER_EXPIRED(stVehicleDamageTimer, 1000)
						IF fDistanceFromClosestVehicle > 1000.0
							FLOAT fHealth
							fHealth = GET_VEHICLE_BODY_HEALTH(vehID)
							SET_VEHICLE_BODY_HEALTH(vehID, fHealth - 50.0)
							PRINTLN("[PLACEHOLDER_PRINT] [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_VEHICLE_HEALTH_DAMAGE - iVehicle #", iVehicle, " out of range, doing 50.0 damage to vehicle.")
						ENDIF
						RESET_NET_TIMER(stVehicleDamageTimer)
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH

ENDPROC

FUNC BOOL SHOULD_REDUCE_TRAFFIC_AROUND_VEHICLE(INT iVehicle)
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			IF IS_VEHICLE_A_CARRIER_VEHICLE(iVehicle)
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

VECTOR vCurrentPopZoneCoords = <<5000,8500,0>> //Off the map
CONST_FLOAT POP_ZONE_RADIUS				500.0
CONST_FLOAT POP_ZONE_EXIT_DISTANCE		550.0
INT iCurrentPopZone = -1
PROC MAINTAIN_TRAFFIC_REDUCTION_SPHERE_AROUND_VEHICLE(INT iVehicle, VEHICLE_INDEX vehId)
	IF NOT SHOULD_REDUCE_TRAFFIC_AROUND_VEHICLE(iVehicle)
	OR GB_GET_LOCAL_PLAYER_MISSION_HOST() != PLAYER_ID()
		EXIT
	ENDIF
	
	VECTOR vVehicleCoords = GET_ENTITY_COORDS(vehId, FALSE)
	FLOAT fRadius = POP_ZONE_RADIUS
	
	IF VDIST2(vVehicleCoords, vCurrentPopZoneCoords) > (POP_ZONE_EXIT_DISTANCE * POP_ZONE_EXIT_DISTANCE)
		IF iCurrentPopZone != -1
			REMOVE_POP_MULTIPLIER_SPHERE(iCurrentPopZone, FALSE)
		ENDIF
		vCurrentPopZoneCoords = vVehicleCoords
		iCurrentPopZone = ADD_POP_MULTIPLIER_SPHERE(vCurrentPopZoneCoords, fRadius, 1.0, 0.02, FALSE)
		PRINTLN("[PLACEHOLDER_PRINT] [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_TRAFFIC_REDUCTION_SPHERE_AROUND_VEHICLE - Updated pop multiplier zone to ", vCurrentPopZoneCoords)
	ENDIF
ENDPROC

PROC CLEANUP_TRAFFIC_REDUCTION_SPHERE_AROUND_VEHICLE()
	IF iCurrentPopZone != -1
		IF DOES_POP_MULTIPLIER_SPHERE_EXIST(iCurrentPopZone)
			REMOVE_POP_MULTIPLIER_SPHERE(iCurrentPopZone,FALSE)
			PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] [SCRIPT_CLEANUP] CLEANUP_TRAFFIC_REDUCTION_SPHERE_AROUND_VEHICLE - Removed pop multiplier zone.")
			iCurrentPopZone = -1
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_MODIFY_CARGOBOB_ROPE_LENGTH(INT iVehicle)
	
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			RETURN IS_VEHICLE_A_CARRIER_VEHICLE(iVehicle)
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_CARGOBOB_ROPE_LENGTH(INT iVehicle, VEHICLE_INDEX vehID)
	IF SHOULD_MODIFY_CARGOBOB_ROPE_LENGTH(iVehicle)
		IF NOT IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_INCREASED_ROPE_LENGTH)
			IF NOT IS_VEHICLE_CLIENT_BIT_SET(iVehicle, PARTICIPANT_ID(), eVEHICLECLIENTBITSET_INCREASED_ROPE_LENGTH)
				IF NOT IS_ENTITY_DEAD(vehID)
					IF DOES_CARGOBOB_HAVE_PICK_UP_ROPE(vehID)
						IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
							SET_PICKUP_ROPE_LENGTH_FOR_CARGOBOB(vehID, 5.0, 5.0, FALSE)
							SET_VEHICLE_CLIENT_BIT(iVehicle, eVEHICLECLIENTBITSET_INCREASED_ROPE_LENGTH)
							PRINTLN("[PLACEHOLDER_PRINT] [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_CARGOBOB_ROPE_LENGTH - iVehicle #", iVehicle)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_MAINTAIN_OPEN_VEHICLE_DOORS(INT iVehicle)
	#IF IS_DEBUG_BUILD
	IF bDebugVehicleDoorOpen
		RETURN iVehicle = iDebugVehicleDoorVehicleIndex
	ENDIF
	#ENDIF
	
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			SWITCH iVehicle
				CASE 0
					RETURN TRUE
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_DO_OPEN_VEHICLE_DOORS_HELP_TEXT(INT iVehicle)
	RETURN IS_VEHICLE_CLIENT_BIT_SET(iVehicle, PARTICIPANT_ID(), eVEHICLECLIENTBITSET_PLAYER_IS_NEAR_ME)
ENDFUNC

PROC DO_OPEN_VEHICLE_DOORS_HELP_TEXT()
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			TRIGGER_HELP(eHELPTEXT_RIOT_VAN_DOORS)
		BREAK
	ENDSWITCH
ENDPROC

FUNC INT GET_VEHICLE_LOCK_PROP(INT iVehicle)
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			SWITCH iVehicle
				CASE 4	RETURN 4
				CASE 5	RETURN 5
				CASE 6	RETURN 6
				CASE 7	RETURN 7
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN -1
ENDFUNC

FUNC BOOL ARE_VEHICLE_DOORS_OPEN_OR_DAMAGED(INT iLockProp, VEHICLE_INDEX vehID)
	IF iLockProp != -1
	AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sProp[iLockProp].netID)
		ENTITY_INDEX entLockProp = NET_TO_ENT(serverBD.sProp[iLockProp].netID)
		
		#IF IS_DEBUG_BUILD
		IF bDebugShowVehicleLockDebug
			TEXT_LABEL_15 tl15Health = "Health: "
			tl15Health += GET_ENTITY_HEALTH(entLockProp)
			DRAW_DEBUG_TEXT_ABOVE_ENTITY(entLockProp, tl15Health, 0.0, 255, 255, 255)
			
			IF NOT IS_ENTITY_VISIBLE(entLockProp)
			AND TAKE_CONTROL_OF_NET_ID(serverBD.sProp[iLockProp].netID)
				PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] ARE_VEHICLE_DOORS_OPEN_OR_DAMAGED - SET_ENTITY_VISIBLE(NET_TO_ENT(serverBD.sProp[iLockProp].netID), TRUE) - Lock prop iProp #", iLockProp)
				SET_ENTITY_VISIBLE(NET_TO_ENT(serverBD.sProp[iLockProp].netID), TRUE)
			ENDIF
		ELSE
			IF IS_ENTITY_VISIBLE(entLockProp)
			AND TAKE_CONTROL_OF_NET_ID(serverBD.sProp[iLockProp].netID)
				SET_ENTITY_VISIBLE(entLockProp, FALSE)
				PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] ARE_VEHICLE_DOORS_OPEN_OR_DAMAGED - SET_ENTITY_VISIBLE(NET_TO_ENT(serverBD.sProp[iLockProp].netID), FALSE) - Lock prop iProp #", iLockProp)
			ENDIF
		ENDIF
		#ENDIF
		
		IF NOT IS_ENTITY_ALIVE(entLockProp)
			PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] ARE_VEHICLE_DOORS_OPEN_OR_DAMAGED - NOT IS_ENTITY_ALIVE(entLockProp) - Lock prop iProp #", iLockProp)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF NOT IS_ENTITY_ALIVE(vehID)
		PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] ARE_VEHICLE_DOORS_OPEN_OR_DAMAGED - NOT IS_ENTITY_ALIVE(vehID) - Lock prop iProp #", iLockProp)
		RETURN TRUE
	ENDIF
	
	IF IS_VEHICLE_DOOR_DAMAGED(vehID, SC_DOOR_REAR_LEFT)
		PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] ARE_VEHICLE_DOORS_OPEN_OR_DAMAGED - IS_VEHICLE_DOOR_DAMAGED(vehID, SC_DOOR_REAR_LEFT) - Lock prop iProp #", iLockProp)
		RETURN TRUE
	ELSE
		IF GET_VEHICLE_DOOR_ANGLE_RATIO(vehID, SC_DOOR_REAR_LEFT) > 0.0
			PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] ARE_VEHICLE_DOORS_OPEN_OR_DAMAGED - GET_VEHICLE_DOOR_ANGLE_RATIO(vehID, SC_DOOR_REAR_LEFT) > 0.0 - Lock prop iProp #", iLockProp)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_VEHICLE_DOOR_DAMAGED(vehID, SC_DOOR_REAR_RIGHT)
		PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] ARE_VEHICLE_DOORS_OPEN_OR_DAMAGED - IS_VEHICLE_DOOR_DAMAGED(vehID, SC_DOOR_REAR_RIGHT) - Lock prop iProp #", iLockProp)
		RETURN TRUE
	ELSE
		IF GET_VEHICLE_DOOR_ANGLE_RATIO(vehID, SC_DOOR_REAR_RIGHT) > 0.0
			PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] ARE_VEHICLE_DOORS_OPEN_OR_DAMAGED - GET_VEHICLE_DOOR_ANGLE_RATIO(vehID, SC_DOOR_REAR_RIGHT) > 0.0 - Lock prop iProp #", iLockProp)
			RETURN TRUE
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF bDebugShowVehicleLockDebug
		DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehID, <<0.0, -3.5, 1.25>>), 1.0, 128, 128, 0, 128)
	ENDIF
	#ENDIF
	
	IF IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehID, <<0.0, -3.5, 1.25>>), 1.0)
		PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] ARE_VEHICLE_DOORS_OPEN_OR_DAMAGED - IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehID, <<0.0, -3.5, 1.25>>), 1.0) - Lock iProp #", iLockProp)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_OPEN_VEHICLE_DOORS(INT iVehicle)
	#IF IS_DEBUG_BUILD
	IF bDebugVehicleDoorOpen
		RETURN iVehicle = iDebugVehicleDoorVehicleIndex
	ENDIF
	#ENDIF
	
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			SWITCH iVehicle
				CASE 4
				CASE 5
				CASE 6
				CASE 7
					RETURN TRUE
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_OPEN_VEHICLE_DOOR(INT iVehicle, VEHICLE_INDEX vehID, SC_DOOR_LIST eVehicleDoor)
	#IF IS_DEBUG_BUILD
	IF bDebugVehicleDoorOpen
		RETURN eVehicleDoor = INT_TO_ENUM(SC_DOOR_LIST, iDebugVehicleDoor)
	ENDIF
	#ENDIF
	
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			SWITCH iVehicle
				CASE 4
				CASE 5
				CASE 6
				CASE 7
					SWITCH eVehicleDoor
						CASE SC_DOOR_REAR_LEFT
						CASE SC_DOOR_REAR_RIGHT
							RETURN ARE_VEHICLE_DOORS_OPEN_OR_DAMAGED(GET_VEHICLE_LOCK_PROP(iVehicle), vehID)
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC OPEN_VEHICLE_DOOR(VEHICLE_INDEX vehID, SC_DOOR_LIST eVehicleDoor)
	#IF IS_DEBUG_BUILD
	IF bDebugVehicleDoorOpen
		IF bUseSetVehicleDoorControl
			SET_VEHICLE_DOOR_CONTROL(vehId, eVehicleDoor, INT_TO_ENUM(DOOR_DAMAGE, iDebugVehicleDoorControlDamageType), 1.0)
		ELSE
			SET_VEHICLE_DOOR_OPEN(vehID, eVehicleDoor)
		ENDIF
		
		EXIT
	ENDIF
	#ENDIF
	
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			SET_VEHICLE_DOOR_CONTROL(vehId, eVehicleDoor, DT_DOOR_NO_RESET, 1.0)
		BREAK
		
		DEFAULT
			SET_VEHICLE_DOOR_OPEN(vehID, eVehicleDoor)
		BREAK
	ENDSWITCH
ENDPROC

PROC MAINTAIN_OPEN_VEHICLE_DOORS(INT iVehicle, VEHICLE_INDEX vehID)
	IF NOT SHOULD_MAINTAIN_OPEN_VEHICLE_DOORS(iVehicle)
		EXIT
	ENDIF
	
	IF IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_OPENED_DOORS)
	OR IS_VEHICLE_CLIENT_BIT_SET(iVehicle, PARTICIPANT_ID(), eVEHICLECLIENTBITSET_OPENED_DOORS)
		EXIT
	ENDIF
	
	IF SHOULD_DO_OPEN_VEHICLE_DOORS_HELP_TEXT(iVehicle)
		DO_OPEN_VEHICLE_DOORS_HELP_TEXT()
	ENDIF
	
	IF SHOULD_OPEN_VEHICLE_DOORS(iVehicle)
	AND MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
		INT i
		REPEAT COUNT_OF(SC_DOOR_LIST) i
			SC_DOOR_LIST eVehicleDoor = INT_TO_ENUM(SC_DOOR_LIST, i)
			
			IF SHOULD_OPEN_VEHICLE_DOOR(iVehicle, vehID, eVehicleDoor)
				PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_OPEN_VEHICLE_DOORS - Opening eVehicleDoor = ", GET_VEHICLE_DOOR_NAME(eVehicleDoor), " for iVehicle #", iVehicle)
				OPEN_VEHICLE_DOOR(vehID, eVehicleDoor)
			ENDIF
		ENDREPEAT
		
		#IF IS_DEBUG_BUILD
		IF bDebugVehicleDoorOpen
			bDebugVehicleDoorOpen = FALSE
			EXIT
		ENDIF
		#ENDIF
		
		SET_VEHICLE_CLIENT_BIT(iVehicle, eVEHICLECLIENTBITSET_OPENED_DOORS)
	ENDIF
ENDPROC

FUNC BOOL SHOULD_MAINTAIN_CLOSE_VEHICLE_DOORS(INT iVehicle)
	#IF IS_DEBUG_BUILD
	IF bDebugVehicleDoorClose
		RETURN iVehicle = iDebugVehicleDoorVehicleIndex
	ENDIF
	#ENDIF
	
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			SWITCH iVehicle
				CASE 4
				CASE 5
				CASE 6
				CASE 7
					RETURN TRUE
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_CLOSE_VEHICLE_DOOR_DELAY_MS()
	RETURN 5000
ENDFUNC

FUNC BOOL SHOULD_CLOSE_VEHICLE_DOORS(INT iVehicle)
	#IF IS_DEBUG_BUILD
	IF bDebugVehicleDoorClose
		RETURN iVehicle = iDebugVehicleDoorVehicleIndex
	ENDIF
	#ENDIF
	
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			SWITCH iVehicle
				CASE 4
				CASE 5
				CASE 6
				CASE 7
					IF IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_OPENED_DOORS)
					AND HAS_NET_TIMER_EXPIRED(stCloseVehicleDoorsDelay[iVehicle], GET_CLOSE_VEHICLE_DOOR_DELAY_MS())
						RETURN TRUE
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_CLOSE_VEHICLE_DOOR(INT iVehicle, SC_DOOR_LIST eVehicleDoor)
	#IF IS_DEBUG_BUILD
	IF bDebugVehicleDoorClose
		RETURN eVehicleDoor = INT_TO_ENUM(SC_DOOR_LIST, iDebugVehicleDoor)
	ENDIF
	#ENDIF
	
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			SWITCH iVehicle
				CASE 4
				CASE 5
				CASE 6
				CASE 7
					SWITCH eVehicleDoor
						CASE SC_DOOR_REAR_LEFT
						CASE SC_DOOR_REAR_RIGHT
							RETURN TRUE
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_VEHICLE_DOOR_OPEN(VEHICLE_INDEX vehID, SC_DOOR_LIST eVehicleDoor)
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			SWITCH eVehicleDoor
				CASE SC_DOOR_REAR_LEFT
				CASE SC_DOOR_REAR_RIGHT
					RETURN FALSE
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN GET_VEHICLE_DOOR_ANGLE_RATIO(vehID, eVehicleDoor) > 0.01
ENDFUNC

FUNC FLOAT GET_CLOSE_VEHICLE_DOOR_TIME_S()
	#IF IS_DEBUG_BUILD
	IF bDebugVehicleDoorClose
		RETURN fDebugVehicleDoorCloseTimeS
	ENDIF
	#ENDIF
	
	RETURN 1.0
ENDFUNC

PROC CLOSE_VEHICLE_DOOR(VEHICLE_INDEX vehID, SC_DOOR_LIST eVehicleDoor)
	#IF IS_DEBUG_BUILD
	IF bDebugVehicleDoorClose
		IF bUseSetVehicleDoorControl
			SET_VEHICLE_DOOR_CONTROL(vehId, eVehicleDoor, INT_TO_ENUM(DOOR_DAMAGE, iDebugVehicleDoorControlDamageType), 0.0)
		ELSE
			SET_VEHICLE_DOOR_SHUT(vehID, eVehicleDoor)
		ENDIF
		
		SET_VEHICLE_DOOR_LATCHED(vehID, eVehicleDoor, TRUE, TRUE)
		EXIT
	ENDIF
	#ENDIF
	
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			SET_VEHICLE_DOOR_CONTROL(vehID, eVehicleDoor, DT_DOOR_SWINGING_FREE, 1.0)
			SET_VEHICLE_DOOR_LATCHED(vehID, eVehicleDoor, FALSE, FALSE, FALSE)
		BREAK
		
		DEFAULT
			SET_VEHICLE_DOOR_SHUT(vehID, eVehicleDoor)
			SET_VEHICLE_DOOR_LATCHED(vehID, eVehicleDoor, TRUE, TRUE)
		BREAK
	ENDSWITCH
ENDPROC

FUNC DOOR_DAMAGE GET_VEHICLE_DOOR_CLOSING_DAMAGE_TYPE()
	#IF IS_DEBUG_BUILD
	IF bDebugVehicleDoorKeepOpen
		RETURN INT_TO_ENUM(DOOR_DAMAGE, iDebugVehicleDoorClosingDamageType)
	ENDIF
	#ENDIF
	
	RETURN DT_DOOR_INTACT
ENDFUNC

PROC MAINTAIN_CLOSE_VEHICLE_DOORS(INT iVehicle, VEHICLE_INDEX vehID)
	IF NOT SHOULD_MAINTAIN_CLOSE_VEHICLE_DOORS(iVehicle)
		EXIT
	ENDIF
	
	IF IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_CLOSED_DOORS)
	OR IS_VEHICLE_CLIENT_BIT_SET(iVehicle, PARTICIPANT_ID(), eVEHICLECLIENTBITSET_CLOSED_DOORS)
		EXIT
	ENDIF
	
	IF SHOULD_CLOSE_VEHICLE_DOORS(iVehicle)
	AND MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
		BOOL bAllDoorsShut = TRUE
		FLOAT fDoorOpenRatio = 0.0
		FLOAT fDoorCloseTime = 0.0
		FLOAT fAngleRatioDelta = 0.0
		
		INT i
		REPEAT COUNT_OF(SC_DOOR_LIST) i
			SC_DOOR_LIST eVehicleDoor = INT_TO_ENUM(SC_DOOR_LIST, i)
			
			IF SHOULD_CLOSE_VEHICLE_DOOR(iVehicle, eVehicleDoor)
				IF IS_VEHICLE_DOOR_OPEN(vehID, eVehicleDoor)
					bAllDoorsShut = FALSE
					fDoorOpenRatio = GET_VEHICLE_DOOR_ANGLE_RATIO(vehID, eVehicleDoor)
					fDoorCloseTime = GET_CLOSE_VEHICLE_DOOR_TIME_S()
					fAngleRatioDelta = GET_FRAME_TIME() / (fDoorCloseTime * (1.0 - fDoorOpenRatio)) //Get the amount we should close the door this frame by dividing the last frame time by the amount of time we want to take to finish closing the door.
					SET_VEHICLE_DOOR_CONTROL(vehID, eVehicleDoor, GET_VEHICLE_DOOR_CLOSING_DAMAGE_TYPE(), FMAX(0.0, fDoorOpenRatio - fAngleRatioDelta))
					
					PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_CLOSE_VEHICLE_DOORS - iVehicle #", iVehicle, ", eVehicleDoor = ", ENUM_TO_INT(eVehicleDoor), ", fDoorOpenRatio = ", fDoorOpenRatio, ", fDoorCloseTime = ", fDoorCloseTime, ", fAngleRatioDelta = ", fAngleRatioDelta)
				ELSE
					CLOSE_VEHICLE_DOOR(vehID, eVehicleDoor)
					
					PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_CLOSE_VEHICLE_DOORS - iVehicle #", iVehicle, " has closed eVehicleDoor = ", ENUM_TO_INT(eVehicleDoor))
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF bAllDoorsShut
			#IF IS_DEBUG_BUILD
			IF bDebugVehicleDoorClose
				bDebugVehicleDoorClose = FALSE
				EXIT
			ENDIF
			#ENDIF
			
			SET_VEHICLE_CLIENT_BIT(iVehicle, eVEHICLECLIENTBITSET_CLOSED_DOORS)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_MAINTAIN_KEEP_VEHICLE_DOORS_OPEN(INT iVehicle)
	#IF IS_DEBUG_BUILD
	IF bDebugVehicleDoorKeepOpen
		RETURN iVehicle = iDebugVehicleDoorVehicleIndex
	ENDIF
	#ENDIF
	
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			SWITCH iVehicle
				CASE 0
					RETURN TRUE
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_KEEP_VEHICLE_DOOR_OPEN(INT iVehicle, SC_DOOR_LIST eVehicleDoor)
	#IF IS_DEBUG_BUILD
	IF bDebugVehicleDoorKeepOpen
		RETURN eVehicleDoor = INT_TO_ENUM(SC_DOOR_LIST, iDebugVehicleDoor)
	ENDIF
	#ENDIF
	
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			SWITCH iVehicle
				CASE 0
					SWITCH eVehicleDoor
						CASE SC_DOOR_BOOT
							RETURN TRUE
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC FLOAT GET_KEEP_VEHICLE_DOOR_OPEN_THRESHOLD_ANGLE_RATIO_THRESHOLD()
	#IF IS_DEBUG_BUILD
	IF bDebugVehicleDoorKeepOpen
		RETURN fDebugVehicleDoorThresholdAngleRatio
	ENDIF
	#ENDIF
	
	RETURN 0.8
ENDFUNC

FUNC DOOR_DAMAGE GET_KEEP_VEHICLE_DOOR_OPEN_DAMAGE_TYPE()
	#IF IS_DEBUG_BUILD
	IF bDebugVehicleDoorKeepOpen
		RETURN INT_TO_ENUM(DOOR_DAMAGE, iDebugVehicleDoorControlDamageType)
	ENDIF
	#ENDIF
	
	RETURN DT_DOOR_INTACT
ENDFUNC

FUNC FLOAT GET_KEEP_VEHICLE_DOOR_OPEN_TARGET_ANGLE_RATIO()
	#IF IS_DEBUG_BUILD
	IF bDebugVehicleDoorKeepOpen
		RETURN fDebugVehicleDoorTargetAngleRatio
	ENDIF
	#ENDIF
	
	RETURN 0.8
ENDFUNC

PROC MAINTAIN_KEEP_VEHICLE_DOORS_OPEN(INT iVehicle, VEHICLE_INDEX vehID)
	IF NOT SHOULD_MAINTAIN_KEEP_VEHICLE_DOORS_OPEN(iVehicle)
		EXIT
	ENDIF
	
	INT i
	REPEAT COUNT_OF(SC_DOOR_LIST) i
		SC_DOOR_LIST eVehicleDoor = INT_TO_ENUM(SC_DOOR_LIST, i)
		
		IF SHOULD_KEEP_VEHICLE_DOOR_OPEN(iVehicle, eVehicleDoor)
		AND GET_VEHICLE_DOOR_ANGLE_RATIO(vehID, eVehicleDoor) < GET_KEEP_VEHICLE_DOOR_OPEN_THRESHOLD_ANGLE_RATIO_THRESHOLD()
		AND MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
			SET_VEHICLE_DOOR_CONTROL(vehId, eVehicleDoor, GET_KEEP_VEHICLE_DOOR_OPEN_DAMAGE_TYPE(), GET_KEEP_VEHICLE_DOOR_OPEN_TARGET_ANGLE_RATIO())
		ENDIF
	ENDREPEAT
ENDPROC

FUNC BOOL SHOULD_MAINTAIN_SET_VEHICLE_DOORS_TO_AUTO_LOCK()
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_SET_VEHICLE_DOORS_TO_AUTO_LOCK(VEHICLE_INDEX vehID)
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehID)
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_VEHICLE_DOOR_AUTO_LOCK(SC_DOOR_LIST eVehicleDoor)
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			SWITCH eVehicleDoor
				CASE SC_DOOR_REAR_LEFT
				CASE SC_DOOR_REAR_RIGHT
					RETURN TRUE
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_SET_VEHICLE_DOORS_TO_AUTO_LOCK(INT iVehicle, VEHICLE_INDEX vehID)
	IF NOT SHOULD_MAINTAIN_SET_VEHICLE_DOORS_TO_AUTO_LOCK()
		EXIT
	ENDIF
	
	IF NOT IS_VEHICLE_CLIENT_BIT_SET(iVehicle, PARTICIPANT_ID(), eVEHICLECLIENTBITSET_DOORS_SET_TO_AUTO_LOCK)
	AND SHOULD_SET_VEHICLE_DOORS_TO_AUTO_LOCK(vehID)
		INT i
		REPEAT COUNT_OF(SC_DOOR_LIST) i
			SC_DOOR_LIST eVehicleDoor = INT_TO_ENUM(SC_DOOR_LIST, i)
			
			IF SHOULD_VEHICLE_DOOR_AUTO_LOCK(eVehicleDoor)
				SET_VEHICLE_DOOR_AUTO_LOCK(vehID, eVehicleDoor, TRUE)
				PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_SET_VEHICLE_DOORS_TO_AUTO_LOCK - SET_VEHICLE_DOOR_AUTO_LOCK called for iVehicle #", iVehicle, ", eVehicleDoor = ", ENUM_TO_INT(eVehicleDoor))
			ENDIF
		ENDREPEAT
		
		SET_VEHICLE_CLIENT_BIT(iVehicle, eVEHICLECLIENTBITSET_DOORS_SET_TO_AUTO_LOCK)
	ENDIF
ENDPROC

FUNC BOOL SHOULD_MAINTAIN_ALTERED_VEHICLE_SPEED(INT iVehicle)
	UNUSED_PARAMETER(iVehicle)
	
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_ALTER_VEHICLE_SPEED(INT iVehicle, VEHICLE_INDEX vehID)
	UNUSED_PARAMETER(iVehicle)
	
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			IF IS_ENTITY_IN_RANGE_COORDS(vehID, <<-1101.266602,4924.347656,215.437164>>, ciCELEB_DISPOSE_OF_CAR_MAX_ALTER_VEHICLE_SPEED_DISTANCE_M)
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_ALTERED_VEHICLE_SPEED(INT iVehicle, VEHICLE_INDEX vehID)
	IF NOT SHOULD_MAINTAIN_ALTERED_VEHICLE_SPEED(iVehicle)
		EXIT
	ENDIF
	
	IF IS_ENTITY_ALIVE(vehID)
	AND IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(), vehID)
	AND GET_PED_IN_VEHICLE_SEAT(vehID) = PLAYER_PED_ID()
	AND SHOULD_ALTER_VEHICLE_SPEED(iVehicle, vehID)
		IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
			SET_VEHICLE_MAX_SPEED(vehID, GET_ALTERED_VEHICLE_SPEED(vehID))
		ENDIF
	ELSE
		IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
			SET_VEHICLE_MAX_SPEED(vehID, -1)
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_VEHICLE_EXISTS_FOR_NON_PARTICIPANTS(INT iVehicle)
	IF NOT SHOULD_CHECK_VEHICLE_IS_INTERIOR(iVehicle)
		EXIT
	ENDIF
	
	IF NOT IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_INSIDE_MISSION_INTERIOR)
		IF NOT IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_MADE_EXIST_FOR_NON_PARTICIPANTS)
		AND NOT IS_VEHICLE_CLIENT_BIT_SET(iVehicle, PARTICIPANT_ID(), eVEHICLECLIENTBITSET_MADE_EXIST_FOR_NON_PARTICIPANTS)
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
			AND MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
				NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(NET_TO_ENT(serverBD.sVehicle[iVehicle].netId), FALSE)
				SET_NETWORK_ID_EXISTS_ON_ALL_MACHINES(serverBD.sVehicle[iVehicle].netId, TRUE)
				
				SET_VEHICLE_CLIENT_BIT(iVehicle, eVEHICLECLIENTBITSET_MADE_EXIST_FOR_NON_PARTICIPANTS)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_SEND_PREREQUISITE_VEHICLE_TICKER()
	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			IF NOT bInPrequisteVehicleLastFrame
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_VEHICLE_BODIES(BOOL bDelayUi)

	INT iVehicle
	VECTOR vSpawnLocation
	FLOAT fSpawnHeading
	VEHICLE_SPAWN_LOCATION_PARAMS vehicleSpawnLocationParams
	VEHICLE_INDEX vehID
	BOOL bEmptySeatAvailable
	
	fDistanceFromClosestVehicle = 99999.0
	iVehInside = -1
	
	bInPrequisteVehicleLastFrame = IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_I_AM_DRIVER_OF_PREREQUISITE_VEHICLE)
	CLEAR_CLIENT_BIT(eCLIENTBITSET_I_AM_IN_PREREQUISITE_VEHICLE #IF IS_DEBUG_BUILD , TRUE #ENDIF)
	CLEAR_CLIENT_BIT(eCLIENTBITSET_I_AM_IN_FRONT_RIGHT_OF_PREREQUISITE_VEHICLE #IF IS_DEBUG_BUILD , TRUE #ENDIF)
	CLEAR_CLIENT_BIT(eCLIENTBITSET_I_AM_DRIVER_OF_PREREQUISITE_VEHICLE #IF IS_DEBUG_BUILD , TRUE #ENDIF)
	CLEAR_CLIENT_BIT(eCLIENTBITSET_I_AM_ANY_PASSENGER_OF_PREREQUISITE_VEHICLE #IF IS_DEBUG_BUILD , TRUE #ENDIF)
	
	REPEAT GB_GET_FMP_NUM_VEH_REQUIRED(GET_VARIATION(), GET_SUBVARIATION(), GB_GET_LOCAL_PLAYER_GANG_BOSS(), FALSE, GET_EXTRA_PARAM_FOR_VEHICLE_SPAWN(), TRUE, FALSE) iVehicle
		
		IF NOT bDelayUi
			MAINTAIN_VEHICLE_UNIQUE_BLIP(iVehicle)
		ENDIF
		MAINTAIN_DESTROY_UNDRIVEABLE_VEHICLE(iVehicle)
		MAINTAIN_VEHICLE_EXISTS_FOR_NON_PARTICIPANTS(iVehicle)

		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
			vehID = NET_TO_VEH(serverBD.sVehicle[iVehicle].netId)
			
			MAINTAIN_ATTACHING_VEHICLE_LOGIC(iVehicle, vehID)
			MAINTAIN_SHOULD_DESTROY_HELICOPTER(iVehicle)
			MAINTAIN_WARPING_VEHICLES_ON_DELIVERY(iVehicle)
			MAINTAIN_PMSD_FOR_CARRIER_VEHICLES(iVehicle, vehID)
			MAINTAIN_IS_PLAYER_NEAR_VEHICLE(iVehicle, vehID)
			MAINTAIN_SPOOK_VEHICLE(iVehicle, vehID)
			MAINTAIN_TAIL_VEHICLE(iVehicle, vehID)
			MAINTAIN_FREEZE_VEHICLE_POSITION(iVehicle, vehID)
			MAINTAIN_VEHICLE_INVINCIBILITY(iVehicle, vehID)
			MAINTAIN_CARGOBOB_ROPE_LENGTH(iVehicle, vehID)
			MAINTAIN_OPEN_VEHICLE_DOORS(iVehicle, vehID)
			MAINTAIN_CLOSE_VEHICLE_DOORS(iVehicle, vehID)
			MAINTAIN_KEEP_VEHICLE_DOORS_OPEN(iVehicle, vehID)
			MAINTAIN_SET_VEHICLE_DOORS_TO_AUTO_LOCK(iVehicle, vehID)
			MAINTAIN_ALTERED_VEHICLE_SPEED(iVehicle, vehID)
	
			IF NOT IS_VEHICLE_CLIENT_BIT_SET(iVehicle, PARTICIPANT_ID(), eVEHICLECLIENTBITSET_DESTROYED)
				IF GET_VEHICLE_STATE(iVehicle) > eVEHICLESTATE_CREATE
					IF NOT IS_NET_VEHICLE_DRIVEABLE(serverBD.sVehicle[iVehicle].netId)
						SET_VEHICLE_CLIENT_BIT(iVehicle, eVEHICLECLIENTBITSET_DESTROYED)
					ENDIF
				ENDIF
			ENDIF
						
			SWITCH GET_VEHICLE_STATE(iVehicle)
				
				CASE eVEHICLESTATE_INACTIVE

				BREAK
				
				CASE eVEHICLESTATE_CREATE
					
				BREAK
				
				CASE eVEHICLESTATE_DRIVEABLE
				
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
						MAINTAIN_TRAFFIC_REDUCTION_SPHERE_AROUND_VEHICLE(iVehicle, vehID)
					
						IF NOT IS_VEHICLE_CLIENT_BIT_SET(iVehicle, PARTICIPANT_ID(), eVEHICLECLIENTBITSET_SET_FORCED_SEAT_USAGE)
							SWITCH GET_VARIATION()
								CASE PLACEHOLDERVAR_MAX
									SET_PED_VEHICLE_FORCED_SEAT_USAGE(PLAYER_PED_ID(), vehID, 0, ENUM_TO_INT(VC_FORCE_USE_FRONT_SEATS))
								BREAK
							ENDSWITCH
							
							SET_VEHICLE_CLIENT_BIT(iVehicle, eVEHICLECLIENTBITSET_SET_FORCED_SEAT_USAGE)
						ENDIF
						
						IF SHOULD_SET_RADIO_STATION_FOR_VEHICLE(iVehicle)
						AND NOT IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_SET_RADIO_STATION)
						AND NOT IS_VEHICLE_CLIENT_BIT_SET(iVehicle, PARTICIPANT_ID(), eVEHICLECLIENTBITSET_SET_RADIO_STATION)
						AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBd.sVehicle[iVehicle].netId)
						AND NOT IS_VEHICLE_SEAT_FREE(vehID)
						AND GET_IS_VEHICLE_ENGINE_RUNNING(vehID)
							SET_VEHICLE_ENGINE_ON(vehId, TRUE, TRUE)
							SET_VEHICLE_RADIO_ENABLED(vehId, TRUE)
							SET_VEHICLE_RADIO_LOUD(vehId, FALSE)
							SET_VEH_FORCED_RADIO_THIS_FRAME(vehId)
							SET_VEH_RADIO_STATION(vehId, GET_RADIO_STATION_NAME_FOR_VEHICLE(iVehicle))		
							SET_RADIO_TO_STATION_NAME(GET_RADIO_STATION_NAME_FOR_VEHICLE(iVehicle))
							SET_VEHICLE_CLIENT_BIT(iVehicle, eVEHICLECLIENTBITSET_SET_RADIO_STATION)
							PRINTLN("[PLACEHOLDER_PRINT] [", GET_CURRENT_VARIATION_STRING(), "] - iVehicle #", iVehicle, " radio station set to: ", GET_RADIO_STATION_NAME_FOR_VEHICLE(iVehicle))
						ENDIF
						
						IF IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_I_AM_CARRIER_VEHICLE)
							IF NOT IS_VEHICLE_CLIENT_BIT_SET(iVehicle, PARTICIPANT_ID(), eVEHICLECLIENTBITSET_ATTEMPTED_CARRIER_VEHICLE_ENTRY)
								IF GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()) = vehID
								AND IS_PED_TRYING_TO_ENTER_A_LOCKED_VEHICLE(PLAYER_PED_ID())
									SET_VEHICLE_CLIENT_BIT(iVehicle, eVEHICLECLIENTBITSET_ATTEMPTED_CARRIER_VEHICLE_ENTRY)
								ENDIF
							ENDIF
						ENDIF
						
						IF SHOULD_TRIGGER_CARRIER_DESTROYED_HELP_TEXT()
						AND IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_I_AM_CARRIER_VEHICLE)
						AND NOT IS_VEHICLE_DRIVEABLE(vehID)
						AND GET_CLIENT_MODE_STATE() >= eMODESTATE_COLLECT_MISSION_ENTITY
							TRIGGER_HELP(eHELPTEXT_VEHICLE_DESTROYED_COLLECT_EQUIPMENT)
						ENDIF
						
						IF GET_END_REASON() != eENDREASON_NO_REASON_YET
						AND IS_ENTITY_ALIVE(vehID)

							MODEL_NAMES eVehModel
							eVehModel = GET_ENTITY_MODEL(vehID)
							IF NOT SHOULD_ALLOW_USE_AFTER_MISSION(eVehModel, iVehicle)
								IF NOT SHOULD_EMPTY_AND_LOCK_VEHICLE_ON_END(eVehModel, iVehicle)
									IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
									AND GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()) = vehId
										IF SHOULD_USE_PMSD_FOR_VEHICLE(eVehModel)
											GB_SET_GLOBAL_CLIENT_BIT1(eGB_GLOBAL_CLIENT_BITSET_1_INSIDE_POST_MISSION_SELF_DESTRUCT_VEHICLE)
											MPGlobalsAmbience.sPostMissionSelfDestruct.bIgnoreMessages = TRUE
										ELSE
											IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
												SET_VEHICLE_ENGINE_HEALTH(vehId, -1000.0)	// Kill engine if completely empty
											ENDIF
										ENDIF
									ELIF IS_VEHICLE_EMPTY(vehId, TRUE, TRUE)		// Check empty of players
										IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
											IF IS_VEHICLE_EMPTY(vehId, TRUE)		// Check empty of all peds
												SET_VEHICLE_ENGINE_HEALTH(vehId, -1000.0)	// Kill engine if completely empty
											ELIF IS_VEHICLE_AN_AMBUSH_VEHICLE(iVehicle)
												SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(vehID, TRUE)	// Lock the doors if it's an ambush vehicle and peds are still inside it
											ENDIF
										ENDIF
									ENDIF
								ELSE
									IF EMPTY_AND_LOCK_VEHICLE(vehID, timeLeaveEventVeh[iVehicle], FALSE)
										PRINTLN("[PLACEHOLDER_PRINT] [", GET_CURRENT_VARIATION_STRING(), "] - iVehicle #", iVehicle, " has been emptied and locked.")
									ENDIF
									SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(vehID, TRUE)
																		
								ENDIF
							ELSE
								PRINTLN("[PLACEHOLDER_PRINT] [", GET_CURRENT_VARIATION_STRING(), "] - iVehicle #", iVehicle, " is allowed for use after mission. Not locking or starting kill switch.")
							ENDIF
						ENDIF

						IF IS_ENTITY_ALIVE(vehID)
							IF SHOULD_RUN_VEHICLE_DISTANCE_CHECKS()
								IF IS_VEHICLE_VALID_FOR_CLOSEST_CHECK(iVehicle)
									IF IS_NET_VEHICLE_DRIVEABLE(serverBD.sVehicle[iVehicle].netId)
										FLOAT fDistance
										fDistance = GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),vehID)
										IF fDistance < fDistanceFromClosestVehicle
											fDistanceFromClosestVehicle = fDistance
											iClosestVehicle = iClosestVehicle // To please release compiler
											iClosestVehicle = iVehicle
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						
							IF SHOULD_VEHICLE_BE_MADE_DESTROYABLE(iVehicle)	
							AND NOT IS_VEHICLE_CLIENT_BIT_SET(iVehicle, PARTICIPANT_ID(), eVEHICLECLIENTBITSET_MADE_VEHICLE_DESTROYABLE)
								IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
									SET_ENTITY_INVINCIBLE(vehID,FALSE)							
									SET_VEHICLE_CLIENT_BIT(iVehicle, eVEHICLECLIENTBITSET_MADE_VEHICLE_DESTROYABLE)
								ENDIF
							ENDIF
							
							IF IS_VEHICLE_BIT_SET(iVehicle,eVEHICLEBITSET_WARP_VEHICLE_ON_STUCK)
								MAINTAIN_WARPING_VEHICLES(serverBD.sVehicle[iVehicle].netId) 
							ENDIF

							IF SHOULD_VEHICLE_BE_UNLOCKED_ON_CONDITION(iVehicle)	
							AND (GET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(vehId, PLAYER_ID())
							OR GET_VEHICLE_DOOR_LOCK_STATUS(vehId) = VEHICLELOCK_LOCKED)
								SET_VEHICLE_UNLOCK_STATE_FOR_VARIATION(serverBD.sVehicle[iVehicle].netId, vehID)
								SET_VEHICLE_CLIENT_BIT(iVehicle, eVEHICLECLIENTBITSET_I_AM_UNLOCKED)
							ENDIF
							
							IF IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_I_AM_RADIO_VEHICLE)
								IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
									TURN_ON_VEHICLE_RADIO(vehID)
									PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - Turned on vehicle radio for iVehicle #", iVehicle)
								ENDIF
							ENDIF
													
							IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehID, SHOULD_ENTERING_BE_CONSIDERED_IN(iVehicle))
								iVehInside = iVehicle
								SET_VEHICLE_CLIENT_BIT(iVehicle, eVEHICLECLIENTBITSET_I_HAVE_ENTERED_VEHICLE)
								IF SHOULD_PEDS_REACT_ON_VEHICLE_ENTRY(iVehicle)
									SET_I_WANT_PEDS_TO_REACT(GET_PED_TO_REACT_ON_VEHICLE_ENTRY(iVehicle),TRUE)
								ENDIF					
							ENDIF
							
							IF SHOULD_PEDS_REACT_ON_PLAYER_GETTING_IN_TO_VEHICLE(iVehicle)
								IF IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID()) AND GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()) = vehID
									IF SHOULD_PEDS_REACT_ON_VEHICLE_ENTRY(iVehicle)
										SET_I_WANT_PEDS_TO_REACT(GET_PED_TO_REACT_ON_VEHICLE_ENTRY(iVehicle),TRUE)
									ENDIF
								ENDIF
							ENDIF
							
							IF SHOULD_VEHICLE_BE_DESTROYED(iVehicle, vehID)
								IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
									NETWORK_EXPLODE_VEHICLE(vehID)
									PRINTLN("[PLACEHOLDER_PRINT] [", GET_CURRENT_VARIATION_STRING(), "] - SHOULD_VEHICLE_BE_DESTROYED - Exploded iVehicle #", iVehicle)
								ENDIF
							ENDIF
							
							IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
							
								IF SHOULD_VEHICLE_SIREN_ACTIVATE(iVehicle)
								AND NOT IS_VEHICLE_SIREN_ON(vehID)
									SET_VEHICLE_SIREN(vehID, TRUE)
									SET_VEHICLE_HAS_MUTED_SIRENS(vehID, SHOULD_VEHICLE_SIREN_BE_MUTED(iVehicle))
									PRINTLN("[PLACEHOLDER_PRINT] [", GET_CURRENT_VARIATION_STRING(), "] - SHOULD_VEHICLE_SIREN_ACTIVATE - Turned on vehicle siren for iVehicle #", iVehicle)
								ENDIF
								
								MAINTAIN_VEHICLE_HEALTH_DAMAGE(iVehicle, vehID)
								
								MAINTAIN_ATTACH_VEHICLE_TO_VEHICLE(iVehicle)
								
								IF SHOULD_REMOVE_ANCHOR(iVehicle, vehID)
									IF IS_BOAT_ANCHORED(vehID)
										SET_BOAT_ANCHOR(vehID, FALSE)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					
//						IF IS_ENTITY_ALIVE(vehID)
//							IF SHOULD_VARIATION_HAVE_PASSENGER_SEAT_BOMBS()
//								IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//									IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehID)
//										IF IS_VEHICLE_A_SUPPORT_VEHICLE(iVehicle)
//											IF NOT MPGlobalsAmbience.bEnablePassengerBombing
//												MPGlobalsAmbience.bEnablePassengerBombing = TRUE
//												PRINTLN("[PLACEHOLDER_PRINT] [", GET_CURRENT_VARIATION_STRING(), "] - iVehicle #", iVehicle, " has enabled passenger bombs.")
//											ENDIF
//										ENDIF
//									ENDIF
//								ELSE
//									IF MPGlobalsAmbience.bEnablePassengerBombing
//										MPGlobalsAmbience.bEnablePassengerBombing = FALSE
//										PRINTLN("[PLACEHOLDER_PRINT] [", GET_CURRENT_VARIATION_STRING(), "] - iVehicle #", iVehicle, " has disabled passenger bombs.")
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
					
						IF IS_VEHICLE_BIT_SET(iVehicle,eVEHICLEBITSET_I_AM_PREREQUISITE_VEHICLE)
							IF IS_NET_VEHICLE_DRIVEABLE(serverBD.sVehicle[iVehicle].netId)
								IF IS_NET_PLAYER_OK(PLAYER_ID(),FALSE)
									fDistanceFromPrerequisiteVehicle = GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),vehID)
									IF fDistanceFromPrerequisiteVehicle = fDistanceFromPrerequisiteVehicle // Stop compiler moaning. May be useful later. 
										fDistanceFromPrerequisiteVehicle = fDistanceFromPrerequisiteVehicle
									ENDIF

									IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehID)
										SET_CLIENT_BIT(eCLIENTBITSET_I_AM_IN_PREREQUISITE_VEHICLE #IF IS_DEBUG_BUILD , TRUE #ENDIF)
										
										VEHICLE_SEAT seat
										seat = GET_SEAT_PED_IS_IN(PLAYER_PED_ID())
										
										IF seat = VS_DRIVER
											IF SHOULD_SEND_PREREQUISITE_VEHICLE_TICKER()
												SEND_BUSINESS_ENTITY_COLLECTED_DROPPED_TICKER(FMMC_TYPE_MISSION_PLACEHOLDER, PLAYER_ID(), GB_GET_LOCAL_PLAYER_GANG_BOSS(), FALSE, FALSE, ENUM_TO_INT(GET_VARIATION()))
											ENDIF
											SET_CLIENT_BIT(eCLIENTBITSET_I_AM_DRIVER_OF_PREREQUISITE_VEHICLE #IF IS_DEBUG_BUILD , TRUE #ENDIF)
										ELIF seat = VS_FRONT_RIGHT
											SET_CLIENT_BIT(eCLIENTBITSET_I_AM_IN_FRONT_RIGHT_OF_PREREQUISITE_VEHICLE #IF IS_DEBUG_BUILD , TRUE #ENDIF)
											SET_CLIENT_BIT(eCLIENTBITSET_I_AM_ANY_PASSENGER_OF_PREREQUISITE_VEHICLE #IF IS_DEBUG_BUILD , TRUE #ENDIF)
										ELIF seat = VS_BACK_LEFT
											SET_CLIENT_BIT(eCLIENTBITSET_I_AM_ANY_PASSENGER_OF_PREREQUISITE_VEHICLE #IF IS_DEBUG_BUILD , TRUE #ENDIF)
										ELIF seat = VS_BACK_RIGHT
											SET_CLIENT_BIT(eCLIENTBITSET_I_AM_ANY_PASSENGER_OF_PREREQUISITE_VEHICLE #IF IS_DEBUG_BUILD , TRUE #ENDIF)
										ELIF seat = VS_ANY_PASSENGER
											SET_CLIENT_BIT(eCLIENTBITSET_I_AM_ANY_PASSENGER_OF_PREREQUISITE_VEHICLE #IF IS_DEBUG_BUILD , TRUE #ENDIF)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF	
					
					ENDIF
					
				BREAK
				
				CASE eVEHICLESTATE_NOT_DRIVEABLE
				
					// Lock ambush vehicle when they're cleaned up 
					IF IS_VEHICLE_AN_AMBUSH_VEHICLE(iVehicle)
					AND IS_NET_VEHICLE_DRIVEABLE(serverBD.sVehicle[iVehicle].netId)
						IF GET_VEHICLE_DOOR_LOCK_STATUS(vehID) != VEHICLELOCK_LOCKED
						AND GET_ENTITY_MODEL(vehID) = TECHNICAL
							IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
								SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(NET_TO_VEH(serverBD.sVehicle[iVehicle].netId), TRUE)
							ENDIF
						ENDIF
					ENDIF
					
				BREAK
				
				CASE eVEHICLESTATE_WARP
					IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
						vehicleSpawnLocationParams.fMinDistFromCoords = 20
						vehicleSpawnLocationParams.fMaxDistance = 100
						vehicleSpawnLocationParams.bConsiderHighways = TRUE
						vehicleSpawnLocationParams.bCheckEntityArea = TRUE
						vehicleSpawnLocationParams.bCheckOwnVisibility = FALSE
						vehicleSpawnLocationParams.bConsiderOnlyActiveNodes = FALSE
						vehID = NET_TO_VEH(serverBD.sVehicle[iVehicle].netId)
						IF HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(GET_ENTITY_COORDS(vehID), <<0.0, 0.0, 0.0>>, GET_ENTITY_MODEL(vehID), TRUE, vSpawnLocation, fSpawnHeading, vehicleSpawnLocationParams)									  
							NETWORK_FADE_IN_ENTITY(vehID, TRUE)
							SET_ENTITY_COORDS_NO_OFFSET(vehID, vSpawnLocation)
							SET_ENTITY_HEADING(vehID, fSpawnHeading)
							SET_VEHICLE_ON_GROUND_PROPERLY(vehID)
							BROADCAST_EVENT_WARP_BUYSELL_SCRIPT_VEHICLE(iVehicle, TRUE, serverBD.iLaunchPosix)
							PRINTLN("[PLACEHOLDER_PRINT] [", GET_CURRENT_VARIATION_STRING(), "] eVEHICLESTATE_WARP - Warping iVehicle #", iVehicle, " to ", vSpawnLocation, " as local player is delivering in it.")
						ENDIF
					ENDIF
				BREAK
				
				CASE eVEHICLESTATE_NO_LONGER_NEEDED
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
						IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
							IF SHOULD_REMOVE_ANCHOR_ON_NO_LONGER_NEEDED(iVehicle)
								SET_BOAT_ANCHOR(vehID, FALSE)
							ENDIF
							
							IF SHOULD_UNLOCK_VEHICLE_DOORS_FOR_ALL_PLAYERS_ON_NO_LONGER_NEEDED(iVehicle)
								SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(vehID, FALSE)
							ENDIF
							
							CLEANUP_NET_ID(serverBD.sVehicle[iVehicle].netId)
						ENDIF
					ENDIF
				BREAK
				
				CASE eVEHICLESTATE_RESET
					
				BREAK
				
			ENDSWITCH
		ENDIF
	ENDREPEAT
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF bEmptySeatAvailable
			IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_PREREQ_SEATS_AVAILABLE)
				SET_SERVER_BIT(eSERVERBITSET_PREREQ_SEATS_AVAILABLE)
			ENDIF
		ELSE
			IF IS_SERVER_BIT_SET(eSERVERBITSET_PREREQ_SEATS_AVAILABLE)
				CLEAR_SERVER_BIT(eSERVERBITSET_PREREQ_SEATS_AVAILABLE)
			ENDIF
		ENDIF
		ENDIF
	
	fLastDistanceFromClosestVehicle = fDistanceFromClosestVehicle
	fDistanceFromClosestVehicle = fLastDistanceFromClosestVehicle		//Stupid compiler
	
ENDPROC

FUNC BOOL KICK_OUT_VEH()

	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			RETURN SHOULD_HALT_PLAYER_VEHICLE()
	ENDSWITCH

	RETURN FALSE
ENDFUNC

FUNC BOOL LOCK_VEH_AFTER_HALT()

	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			RETURN TRUE
	ENDSWITCH

	RETURN FALSE
ENDFUNC

FUNC BOOL KICK_ALL_PLAYERS_OUT_VEH()

	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_KICK_OUT_VEH_AFTER_HALT()

	SWITCH GET_VARIATION()
		CASE PLACEHOLDERVAR_MAX
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE

ENDFUNC

PROC MAINTAIN_HALTING_PLAYER_VEHICLE()
	IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_PLAYER_HALTED)
		IF SHOULD_HALT_PLAYER_VEHICLE()
			IF BRING_PLAYER_VEHICLE_TO_HALT()
			
				SET_LOCAL_BIT(eLOCALBITSET_PLAYER_HALTED)
				
				IF IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_I_AM_IN_PREREQUISITE_VEHICLE)
					SET_CLIENT_BIT(eCLIENTBITSET_PREREQUISITE_VEHICLE_HALTED)
				ENDIF				
			ENDIF
			
			IF KICK_OUT_VEH()
				SET_LOCAL_BIT(eLOCALBITSET_KICK_PLAYER_OUT_VEH)
			ENDIF
			
		ENDIF
	ELSE
		IF NOT SHOULD_HALT_PLAYER_VEHICLE()
			CLEAR_LOCAL_BIT(eLOCALBITSET_PLAYER_HALTED)
		ENDIF
	ENDIF
	
	IF IS_LOCAL_BIT_SET(eLOCALBITSET_KICK_PLAYER_OUT_VEH)
	OR SHOULD_KICK_OUT_VEH_AFTER_HALT()
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF KICK_ALL_PLAYERS_OUT_VEH()
				BROADCAST_LEAVE_VEHICLE(ALL_PLAYERS_IN_VEHICLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())), FALSE, 0.0, 0)
			ELSE
				TASK_LEAVE_VEHICLE(PLAYER_PED_ID(), GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
			ENDIF
			
			IF LOCK_VEH_AFTER_HALT()
				SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), TRUE)
			ENDIF
			
			PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_HALTING_PLAYER_VEHICLE - Kicked player out.")
			
			CLEAR_LOCAL_BIT(eLOCALBITSET_KICK_PLAYER_OUT_VEH)
		ELSE
			CLEAR_LOCAL_BIT(eLOCALBITSET_KICK_PLAYER_OUT_VEH)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL GET_ANY_OTHER_VEHICLE_AT_COORDS(VECTOR vTruck, NETWORK_INDEX niVeh )
	VEHICLE_INDEX viVeh, viVeh2
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(niVeh)
		viVeh = NET_TO_VEH(niVeh)
	ENDIF
	viVeh2 = GET_CLOSEST_VEHICLE(vTruck, 2.0, DUMMY_MODEL_FOR_SCRIPT,
		VEHICLE_SEARCH_FLAG_RETURN_LAW_ENFORCER_VEHICLES 
		| VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES 
		| VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES 
		| VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_GROUP_MEMBERS 
		| VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_PLAYER                                                           
		| VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_DEAD_OR_DYING_PED                                   
		| VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_WITH_PEDS_ENTERING_OR_EXITING                                         
		| VEHICLE_SEARCH_FLAG_ALLOW_VEHICLE_OCCUPANTS_TO_BE_PERFORMING_A_SCRIPTED_TASK      
		| VEHICLE_SEARCH_FLAG_ALLOW_VEHICLE_OCCUPANTS_TO_BE_PERFORMING_A_NON_DEFAULT_TASK)
	IF DOES_ENTITY_EXIST(viVeh2)
	AND viVeh2 != viVeh
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_PREREQUISITE_VEHICLE_MOVE_TO_DESTINATION()
	RETURN FALSE
ENDFUNC

FUNC VECTOR GET_PREREQUISITE_VEHICLE_GO_TO_COORDS()
	RETURN <<0.0,0.0,0.0>>
ENDFUNC

FUNC BOOL IS_HELICOPTER_DAMAGED(VEHICLE_INDEX vehId #IF IS_DEBUG_BUILD , INT iVehicle #ENDIF )
	
	IF NOT IS_VEHICLE_DRIVEABLE(vehId)
		PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_SHOULD_DESTROY_HELICOPTER - IS_HELICOPTER_DAMAGED(", iVehicle, ") - NOT IS_VEHICLE_DRIVEABLE")
		RETURN TRUE
	ENDIF
	
	IF GET_ENTITY_HEALTH(vehId) < 500
		PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_SHOULD_DESTROY_HELICOPTER - IS_HELICOPTER_DAMAGED(", iVehicle, ") - GET_ENTITY_HEALTH <= 500")
		RETURN TRUE
	ENDIF
	
	IF IS_HELI_PART_BROKEN(vehId, TRUE, TRUE, TRUE)
		PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_SHOULD_DESTROY_HELICOPTER - IS_HELICOPTER_DAMAGED(", iVehicle, ") - IS_HELI_PART_BROKEN")
		RETURN TRUE
	ENDIF
	
	IF GET_HELI_TAIL_ROTOR_HEALTH(vehId) <= 30
		PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_SHOULD_DESTROY_HELICOPTER - IS_HELICOPTER_DAMAGED(", iVehicle, ") - GET_HELI_TAIL_ROTOR_HEALTH <= 0")
		RETURN TRUE
	ENDIF
	
	IF GET_HELI_MAIN_ROTOR_HEALTH(vehId) <= 30
		PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_SHOULD_DESTROY_HELICOPTER - IS_HELICOPTER_DAMAGED(", iVehicle, ") - GET_HELI_MAIN_ROTOR_HEALTH <= 0")
		RETURN TRUE
	ENDIF
	
	IF GET_VEHICLE_ENGINE_HEALTH(vehId) <= 30
		PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_SHOULD_DESTROY_HELICOPTER - IS_HELICOPTER_DAMAGED(", iVehicle, ") - GET_VEHICLE_ENGINE_HEALTH <= 0")
		RETURN TRUE
	ENDIF
	
	IF GET_VEHICLE_PETROL_TANK_HEALTH(vehId) <= 30
		PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_SHOULD_DESTROY_HELICOPTER - IS_HELICOPTER_DAMAGED(", iVehicle, ") - GET_VEHICLE_PETROL_TANK_HEALTH <= 0")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_REPAIR_FLAG()
	
ENDPROC

/*
PROC MAINTAIN_CLEAR_DROP_OFF_AREA_OF_UNDRIVEABLE_VEHICLES()
	
	BOOL bReset
	
	IF NOT HAS_NET_TIMER_STARTED(dropOffBlockedTimerA)
	
		PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - [CLRDROP] - started dropOffBlockedTimerA")
		START_NET_TIMER(dropOffBlockedTimerA)
			
	ELIF HAS_NET_TIMER_EXPIRED(dropOffBlockedTimerA, CHECK_BLOCKED_DROP_OFF_TIME_LIMIT)
		
		IF NOT HAS_NET_TIMER_STARTED(dropOffBlockedTimerB)
		
			PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - [CLRDROP] - dropOffBlockedTimerA expired = ", CHECK_BLOCKED_DROP_OFF_TIME_LIMIT)
			PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - [CLRDROP] - started dropOffBlockedTimerB")
			START_NET_TIMER(dropOffBlockedTimerB)
			vehicleInDropOffA = GET_VEHICLE_BLOCKING_VEHICLE_DROP_OFF()
	
			#IF IS_DEBUG_BUILD
			PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - [CLRDROP] - vehicleInDropOffA = GET_VEHICLE_BLOCKING_VEHICLE_DROP_OFF = ", NATIVE_TO_INT(vehicleInDropOffA))
			IF DOES_ENTITY_EXIST(vehicleInDropOffA)
				PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - [CLRDROP] - vehicleInDropOffA = DOES_ENTITY_EXIST(vehicleInDropOffA) = TRUE")
				IF IS_VEHICLE_DRIVEABLE(vehicleInDropOffA)
					PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - [CLRDROP] - vehicleInDropOffA = IS_VEHICLE_DRIVEABLE(vehicleInDropOffA) = TRUE")
				ELSE
					PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - [CLRDROP] - vehicleInDropOffA = IS_VEHICLE_DRIVEABLE(vehicleInDropOffA) = FALSE")
				ENDIF
			ELSE
				PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - [CLRDROP] - vehicleInDropOffA = DOES_ENTITY_EXIST(vehicleInDropOffA) = FALSE")
			ENDIF
			#ENDIF

		ELIF HAS_NET_TIMER_EXPIRED(dropOffBlockedTimerB, RECHECK_BLOCKED_DROP_OFF_TIME_LIMIT)
			
			PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - [CLRDROP] - dropOffBlockedTimerB expired = ", RECHECK_BLOCKED_DROP_OFF_TIME_LIMIT)
			
			IF DOES_ENTITY_EXIST(vehicleInDropOffA)
				IF NOT IS_VEHICLE_DRIVEABLE(vehicleInDropOffA)
			
					vehicleInDropOffB = GET_VEHICLE_BLOCKING_VEHICLE_DROP_OFF()
					PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - [CLRDROP] - vehicleInDropOffB = GET_VEHICLE_BLOCKING_VEHICLE_DROP_OFF = ", NATIVE_TO_INT(vehicleInDropOffB))
					
					#IF IS_DEBUG_BUILD
					IF DOES_ENTITY_EXIST(vehicleInDropOffB)
						PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - [CLRDROP] - vehicleInDropOffB = DOES_ENTITY_EXIST(vehicleInDropOffB) = TRUE")
						IF IS_VEHICLE_DRIVEABLE(vehicleInDropOffB)
							PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - [CLRDROP] - vehicleInDropOffB = IS_VEHICLE_DRIVEABLE(vehicleInDropOffB) = TRUE")
						ELSE
							PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - [CLRDROP] - vehicleInDropOffB = IS_VEHICLE_DRIVEABLE(vehicleInDropOffB) = FALSE")
						ENDIF
					ELSE
						PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - [CLRDROP] - vehicleInDropOffB = DOES_ENTITY_EXIST(vehicleInDropOffB) = FALSE")
					ENDIF
					#ENDIF
					
					IF DOES_ENTITY_EXIST(vehicleInDropOffB)
						IF (vehicleInDropOffA = vehicleInDropOffB)
							PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - [CLRDROP] - vehicleInDropOffA = vehicleInDropOffB. Calling CLEAR_AREA_LEAVE_VEHICLE_HEALTH(GET_DROP_OFF_COORDS(), GET_DROP_RADIUS(), FALSE, FALSE, FALSE, TRUE)")
							CLEAR_AREA_LEAVE_VEHICLE_HEALTH(GET_DROP_OFF_COORDS(), GET_DROP_RADIUS(), FALSE, FALSE, FALSE, TRUE)
						ENDIF
					ENDIF
					
					PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - [CLRDROP] - done. bReset = TRUE")
					bReset = TRUE
					
				ELSE
		
					PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - [CLRDROP] - IS_VEHICLE_DRIVEABLE(vehicleInDropOffA) = TRUE, cannot be cleared. bReset = TRUE")
					bReset = TRUE
				
				ENDIF
				
			ELSE

				PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - [CLRDROP] -DOES_ENTITY_EXIST(vehicleInDropOffA) = FALSE, cannot be cleared. bReset = TRUE")
				bReset = TRUE
				
			ENDIF
			
		ENDIF
		
	ENDIF
	
	IF bReset
		PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] - [CLRDROP] - bReset = TRUE, doing reset.")
		VEHICLE_INDEX tempVeh
		vehicleInDropOffA = tempVeh
		vehicleInDropOffB = tempVeh
		RESET_NET_TIMER(dropOffBlockedTimerA)
		RESET_NET_TIMER(dropOffBlockedTimerB)
	ENDIF
	
ENDPROC
*/

TIME_DATATYPE timeLeaveEvent
PROC MAINTAIN_PREVENT_VEHICLE_IN_AREA()
	IF SHOULD_PREVENT_VEHICLE_IN_AREA()
		SET_VEHICLE_ENGINE_ON(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), FALSE, FALSE)
		IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), 20.0, DEFAULT, DEFAULT, DEFAULT, FALSE, TRUE)
			IF EMPTY_AND_LOCK_VEHICLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), timeLeaveEvent, FALSE, FALSE)
				PRINTLN("[PLACEHOLDER_PRINT] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_PREVENT_VEHICLE_IN_AREA - Slow vehicle for local player and kicked them off.")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

#ENDIF
