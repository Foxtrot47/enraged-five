
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// MISSION_PLACEHOLDER_COORDS.sch																							//
// Contains all mission variation data																					//
// Written by:  Martin McMillan & Ryan Elliott.																			//
// Date: 		23/07/2019																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#IF IS_DEBUG_BUILD

USING "globals.sch"
USING "net_prints.sch"
USING "net_include.sch"
USING "commands_misc.sch"

FUNC CSH_ITEM_TYPE GET_FMP_ITEM_TYPE_FROM_VARIATION(PLACEHOLDER_VARIATION eVariation)

	SWITCH eVariation
		CASE PLACEHOLDERVAR_MAX			RETURN CIT_ARCADE_CABINETS
	ENDSWITCH
	
	RETURN CIT_INVALID

ENDFUNC

////////////////////////////////////
/// MISSION ENTITY SPAWN COORDS ///
////////////////////////////////////

STRUCT FMP_MISSION_ENTITY_DATA

	MODEL_NAMES model
	VECTOR vCoords
	FLOAT fHeading
	VECTOR vRotation

ENDSTRUCT

FUNC FMP_MISSION_ENTITY_DATA GET_FMP_MISSION_ENTITY_DATA(INT iMissionEntity, PLACEHOLDER_VARIATION eVariation, PLACEHOLDER_SUBVARIATION eSubvariation, INT iExtraParam, INT iExtraParam1)

	UNUSED_PARAMETER(iMissionEntity)
	UNUSED_PARAMETER(eVariation)
	UNUSED_PARAMETER(eSubvariation)
	UNUSED_PARAMETER(iExtraParam)
	UNUSED_PARAMETER(iExtraParam1)
	
	FMP_MISSION_ENTITY_DATA data
	MODEL_NAMES model = DUMMY_MODEL_FOR_SCRIPT
	VECTOR vCoords = <<0.0, 0.0, 0.0>>
	FLOAT fHeading = 0.0
	VECTOR vRotate = <<0.0, 0.0, 0.0>>

	// If using a carrier vehicle, mission entity coords will be determined by the vehicle, otherwise add them here
	SWITCH eVariation
		CASE PLACEHOLDERVAR_PLACEHOLDER
			SWITCH eSubvariation
				CASE PLACEHOLDERSUB_PLACEHOLDER
					SWITCH iMissionEntity
						CASE 0
							model = EX_PROP_ADV_CASE_SM
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH

	data.model = model
	data.vCoords = vCoords
	data.fHeading = fHeading
	data.vRotation = vRotate
	
	RETURN data

ENDFUNC

FUNC MODEL_NAMES GET_FMP_ENTITY_MODEL(PLACEHOLDER_VARIATION eVariation, PLACEHOLDER_SUBVARIATION eSubvariation)
	
	FMP_MISSION_ENTITY_DATA data = GET_FMP_MISSION_ENTITY_DATA(0, eVariation, eSubvariation, 0, 0)
	RETURN data.model
	
ENDFUNC

CONST_INT MAX_NUM_FMP_ENTITY_COMMON_SPAWN_COORDS		1
FUNC VECTOR GET_FMP_ENTITY_COMMON_SPAWN_COORDS(INT iSpawnCoord)
	
	SWITCH iSpawnCoord
		CASE 0			RETURN 0.0
	ENDSWITCH

	RETURN <<0.0,0.0,0.0>>

ENDFUNC

FUNC FLOAT GET_FMP_ENTITY_COMMON_SPAWN_HEADING(INT iSpawnCoord)

	SWITCH iSpawnCoord
		CASE 0			RETURN 0.0
	ENDSWITCH

	RETURN 0.0

ENDFUNC

/////////////////////////////////////////////////
/// 	VARIATION VEHICLE SPAWN COORDS 		  ///
/////////////////////////////////////////////////  

STRUCT FMP_VEHICLE_DATA

	MODEL_NAMES model
	VECTOR vCoords
	FLOAT fHeading

ENDSTRUCT

FUNC FMP_VEHICLE_DATA GET_FMP_VEHICLE_DATA(INT iVehicle, PLACEHOLDER_VARIATION eVariation, PLACEHOLDER_SUBVARIATION eSubvariation, INT iExtraParam)
	UNUSED_PARAMETER(iVehicle)
	UNUSED_PARAMETER(eVariation)
	UNUSED_PARAMETER(eSubvariation)
	UNUSED_PARAMETER(iExtraParam)

	MODEL_NAMES model = DUMMY_MODEL_FOR_SCRIPT
	VECTOR vCoords = <<0.0, 0.0, 0.0>>
	FLOAT fHeading = 0.0
	
	SWITCH eVariation
		CASE PLACEHOLDERVAR_PLACEHOLDER
			SWITCH eSubvariation
				CASE PLACEHOLDERSUB_PLACEHOLDER
					SWITCH iVehicle
						CASE 0
							model = PHOENIX
							vCoords = <<602.9517, -1865.8826, 23.7372>>
							fHeading = 283.2223
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	FMP_VEHICLE_DATA data
	data.model = model
	data.vCoords = vCoords
	data.fHeading = fHeading
	
	RETURN data
ENDFUNC

FUNC VECTOR GET_FMP_VEHICLE_SPAWN_COORDS(PLACEHOLDER_VARIATION eVariation, PLACEHOLDER_SUBVARIATION eSubvariation, INT iVehicle, FREEMODE_DELIVERY_DROPOFFS eDropOff = FREEMODE_DELIVERY_DROPOFF_INVALID, INT iExtraParam = -1, INT iRespawnLocation = 0)

	UNUSED_PARAMETER(eDropOff)
	UNUSED_PARAMETER(iRespawnLocation)
	UNUSED_PARAMETER(iExtraParam)

	FMP_VEHICLE_DATA data = GET_FMP_VEHICLE_DATA(iVehicle, eVariation, eSubvariation, iExtraParam)
	RETURN data.vCoords
	
ENDFUNC

FUNC FLOAT GET_FMP_VEHICLE_SPAWN_HEADING(PLACEHOLDER_VARIATION eVariation, PLACEHOLDER_SUBVARIATION eSubvariation, INT iVehicle, FREEMODE_DELIVERY_DROPOFFS eDropOff = FREEMODE_DELIVERY_DROPOFF_INVALID, INT iExtraParam = -1, INT iRespawnLocation = 0)

	UNUSED_PARAMETER(eDropOff)
	UNUSED_PARAMETER(iRespawnLocation)
	UNUSED_PARAMETER(iExtraParam)

	FMP_VEHICLE_DATA data = GET_FMP_VEHICLE_DATA(iVehicle, eVariation, eSubvariation, iExtraParam)
	RETURN data.fHeading

ENDFUNC

FUNC VECTOR GET_FMP_ENTITY_SPAWN_COORDS(PLACEHOLDER_VARIATION eVariation, PLACEHOLDER_SUBVARIATION eSubvariation, INT iMissionEntity, INT iExtraParam = -1, INT iExtraParam1 = -1)

	UNUSED_PARAMETER(iExtraParam)
	UNUSED_PARAMETER(iExtraParam1)
	
	// Carrier vehicles just return the vehicle spawns
	INT iCarrierVehicle = GET_FMP_CARRIER_VEHICLE_INDEX(iMissionEntity, eSubvariation) 
	IF iCarrierVehicle != -1
		RETURN GET_FMP_VEHICLE_SPAWN_COORDS(eVariation, eSubvariation, iCarrierVehicle, DEFAULT, iExtraParam)
	ENDIF
	
	FMP_MISSION_ENTITY_DATA data = GET_FMP_MISSION_ENTITY_DATA(iMissionEntity, eVariation, eSubvariation, iExtraParam, iExtraParam1)
	RETURN data.vCoords
	
ENDFUNC

FUNC FLOAT GET_FMP_ENTITY_SPAWN_HEADING(PLACEHOLDER_VARIATION eVariation, PLACEHOLDER_SUBVARIATION eSubvariation, INT iMissionEntity, INT iExtraParam = -1, INT iExtraParam1 = -1)

	UNUSED_PARAMETER(iExtraParam)
	UNUSED_PARAMETER(iExtraParam1)
	
	// If entity is spawned in carrier vehicle, use coords of vehicle
	INT iCarrierVehicle = GET_FMP_CARRIER_VEHICLE_INDEX(iMissionEntity, eSubvariation) 
	IF iCarrierVehicle != -1
		RETURN GET_FMP_VEHICLE_SPAWN_HEADING(eVariation, eSubvariation, iCarrierVehicle, DEFAULT, iExtraParam)
	ENDIF
	
	FMP_MISSION_ENTITY_DATA data = GET_FMP_MISSION_ENTITY_DATA(iMissionEntity, eVariation, eSubvariation, iExtraParam, iExtraParam1)
	RETURN data.fHeading
	
ENDFUNC

FUNC VECTOR GET_FMP_ENTITY_PICKUP_ROTATION(INT iMissionEntity, PLACEHOLDER_VARIATION eVariation, PLACEHOLDER_SUBVARIATION eSubvariation, INT iExtraParam = -1, INT iExtraParam1 = -1)

	UNUSED_PARAMETER(iExtraParam)
	UNUSED_PARAMETER(iExtraParam1)

	FMP_MISSION_ENTITY_DATA data = GET_FMP_MISSION_ENTITY_DATA(iMissionEntity, eVariation, eSubvariation, iExtraParam, iExtraParam1)
	RETURN data.vRotation
	
ENDFUNC

////////////////////////////////////////
/// 		 VARIATION VEHICLE MODEL ///
////////////////////////////////////////

FUNC MODEL_NAMES GET_FMP_AMBUSH_VEHICLE_MODEL(PLACEHOLDER_VARIATION eVariation, PLACEHOLDER_SUBVARIATION eSubvariation, INT iAmbushArrayIndex, INT iExtraParam = -1)

	UNUSED_PARAMETER(iExtraParam)
	UNUSED_PARAMETER(eSubvariation)
	UNUSED_PARAMETER(iAmbushArrayIndex)
	
	SWITCH eVariation
		CASE PLACEHOLDERVAR_MAX
			SWITCH eSubvariation
				CASE PLACEHOLDERSUB_MAX				RETURN FACTION
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

CONST_INT MAX_NUM_FMP_COMMON_CARRIER_VEHICLE_MODELS		11
FUNC MODEL_NAMES GET_FMP_COMMON_CARRIER_VEHICLE_MODEL(INT iModel)

	SWITCH iModel
		CASE 0		RETURN BARRAGE
		CASE 1		RETURN HALFTRACK
		CASE 2		RETURN INSURGENT3
		CASE 3		RETURN LIMO2
		CASE 4		RETURN TAMPA3
		CASE 5		RETURN TECHNICAL3
		CASE 6		RETURN RUINER2
		CASE 7		RETURN BOXVILLE5
		CASE 8		RETURN DUNE3
		CASE 9		RETURN DUNE5
		CASE 10		RETURN CARACARA
	ENDSWITCH

	RETURN RUINER2

ENDFUNC

FUNC MODEL_NAMES GET_FMP_VEHICLE_MODEL(PLACEHOLDER_VARIATION eVariation, PLACEHOLDER_SUBVARIATION eSubvariation1, INT iVehicle, INT iExtraParam, PLAYER_INDEX playerID)
	UNUSED_PARAMETER(iExtraParam)
	UNUSED_PARAMETER(playerID)
	
	FMP_VEHICLE_DATA data = GET_FMP_VEHICLE_DATA(iVehicle, eVariation, eSubvariation1, iExtraParam)
	RETURN data.model
ENDFUNC

/////////////////////////////////
/// MISSION PED SPAWN COORDS ///
/////////////////////////////////

STRUCT FMP_PED_DATA

	MODEL_NAMES model
	VECTOR vCoords
	FLOAT fHeading
	WEAPON_TYPE weapon
	STRING sScenario

ENDSTRUCT

FUNC FMP_PED_DATA GET_FMP_PED_DATA(INT iPed, PLACEHOLDER_VARIATION eVariation, PLACEHOLDER_SUBVARIATION eSubvariation, INT iExtraParam = -1)
	UNUSED_PARAMETER(iPed)
	UNUSED_PARAMETER(eVariation)
	UNUSED_PARAMETER(eSubvariation)
	UNUSED_PARAMETER(iExtraParam)
	
	MODEL_NAMES model = DUMMY_MODEL_FOR_SCRIPT
	VECTOR vCoords = <<0.0, 0.0, 0.0>>
	FLOAT fHeading = 0.0
	WEAPON_TYPE weapon = WEAPONTYPE_UNARMED
	STRING sScenario = ""

	FMP_PED_DATA data
	data.model = model
	data.vCoords = vCoords
	data.fHeading = fHeading
	data.weapon = weapon
	data.sScenario = sScenario
	
	RETURN data
ENDFUNC

FUNC VECTOR GET_FMP_PED_SPAWN_COORDS(INT iPed, PLACEHOLDER_VARIATION eVariation, PLACEHOLDER_SUBVARIATION eSubvariation, FREEMODE_DELIVERY_DROPOFFS eDropOff = FREEMODE_DELIVERY_DROPOFF_INVALID, INT iRespawnLocation = 0, INT iExtraParam = -1)

	UNUSED_PARAMETER(eDropOff)
	UNUSED_PARAMETER(iRespawnLocation)
	UNUSED_PARAMETER(iExtraParam)

	FMP_PED_DATA data = GET_FMP_PED_DATA(iPed, eVariation, eSubvariation, iExtraParam)
	RETURN data.vCoords

ENDFUNC

FUNC FLOAT GET_FMP_PED_SPAWN_HEADING(INT iPed, PLACEHOLDER_VARIATION eVariation, PLACEHOLDER_SUBVARIATION eSubvariation, FREEMODE_DELIVERY_DROPOFFS eDropOff = FREEMODE_DELIVERY_DROPOFF_INVALID, INT iRespawnLocation = 0, INT iExtraParam = -1)

	UNUSED_PARAMETER(eDropOff)
	UNUSED_PARAMETER(iRespawnLocation)
	UNUSED_PARAMETER(iExtraParam)

	FMP_PED_DATA data = GET_FMP_PED_DATA(iPed, eVariation, eSubvariation, iExtraParam)
	RETURN data.fHeading
	
ENDFUNC

FUNC MODEL_NAMES GET_FMP_AMBUSH_PED_MODEL(INT iAmbushPedBit, PLACEHOLDER_VARIATION eVariation, PLACEHOLDER_SUBVARIATION eSubvariation, MODEL_NAMES eVehicleModel = DUMMY_MODEL_FOR_SCRIPT, INT iExtraParam = -1)
	UNUSED_PARAMETER(eSubvariation)
	UNUSED_PARAMETER(iAmbushPedBit)
	UNUSED_PARAMETER(eVehicleModel)
	UNUSED_PARAMETER(iExtraParam)
	
	SWITCH eVariation
		CASE PLACEHOLDERVAR_MAX
			SWITCH eSubvariation
				CASE PLACEHOLDERSUB_MAX				RETURN G_M_Y_BallaOrig_01
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

FUNC MODEL_NAMES GET_FMP_PED_MODEL(INT iPed, PLACEHOLDER_VARIATION eVariation, PLACEHOLDER_SUBVARIATION eSubvariation, INT iExtraParam = -1)

	UNUSED_PARAMETER(iExtraParam)
	
	FMP_PED_DATA data = GET_FMP_PED_DATA(iPed, eVariation, eSubvariation, iExtraParam)
	RETURN data.model

ENDFUNC

FUNC VECTOR GET_FMP_PED_RESPAWN_COORDS(INT iPed, PLACEHOLDER_VARIATION eVariation, PLACEHOLDER_SUBVARIATION eSubvariation, FREEMODE_DELIVERY_DROPOFFS eDropOff = FREEMODE_DELIVERY_DROPOFF_INVALID, INT iRespawnLocation = 0)
	
	UNUSED_PARAMETER(iPed)
	UNUSED_PARAMETER(eVariation)
	UNUSED_PARAMETER(eSubvariation)
	UNUSED_PARAMETER(eDropOff)
	UNUSED_PARAMETER(iRespawnLocation)

//	SWITCH eVariation
//		
//	ENDSWITCH
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC FLOAT GET_FMP_PED_RESPAWN_HEADING(INT iPed, PLACEHOLDER_VARIATION eVariation, PLACEHOLDER_SUBVARIATION eSubvariation, FREEMODE_DELIVERY_DROPOFFS eDropOff = FREEMODE_DELIVERY_DROPOFF_INVALID, INT iRespawnLocation = 0)

	UNUSED_PARAMETER(iPed)
	UNUSED_PARAMETER(eVariation)
	UNUSED_PARAMETER(eSubvariation)
	UNUSED_PARAMETER(eDropOff)
	UNUSED_PARAMETER(iRespawnLocation)

//	SWITCH eVariation
//		
//	ENDSWITCH
	
	RETURN 0.0
ENDFUNC

FUNC VECTOR GET_FMP_PED_COWER_TASK_COORDS(INT iPed, PLACEHOLDER_VARIATION eVariation, PLACEHOLDER_SUBVARIATION eSubvariation, INT iExtraParam = -1)

	UNUSED_PARAMETER(iExtraParam)
	UNUSED_PARAMETER(iPed)

	SWITCH eVariation
		CASE PLACEHOLDERVAR_MAX
			SWITCH eSubvariation
				CASE PLACEHOLDERSUB_MAX
				
				BREAK
			ENDSWITCH		
		BREAK
	ENDSWITCH

	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

STRUCT FMP_PROP_DATA

	MODEL_NAMES model
	VECTOR vCoords
	FLOAT fHeading
	VECTOR vRotation

ENDSTRUCT

FUNC FMP_PROP_DATA GET_FMP_PROP_DATA(INT iProp, PLACEHOLDER_VARIATION eVariation, PLACEHOLDER_SUBVARIATION eSubvariation, INT iExtraParam)
	UNUSED_PARAMETER(iExtraParam)
	
	MODEL_NAMES model = DUMMY_MODEL_FOR_SCRIPT
	VECTOR vCoords = <<0.0, 0.0, 0.0>>
	FLOAT fHeading = 0.0
	VECTOR vRotate = <<0.0, 0.0, 0.0>>

	SWITCH eVariation
		CASE PLACEHOLDERVAR_PLACEHOLDER
			SWITCH eSubvariation
				CASE PLACEHOLDERSUB_PLACEHOLDER
					SWITCH iProp				
						CASE 0			
							model = 		PROP_WOODEN_BARREL
							vCoords = 		<<603.8352, -1863.6123, 23.7547>>
							fHeading = 		180.999
							vRotate = 		<<0.0000, 0.0000, 180.999>>
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH

	FMP_PROP_DATA data
	data.model = model
	data.vCoords = vCoords
	data.fHeading = fHeading
	data.vRotation = vRotate
	
	RETURN data

ENDFUNC

FUNC MODEL_NAMES GET_FMP_PROP_MODEL(INT iProp, PLACEHOLDER_VARIATION eVariation, PLACEHOLDER_SUBVARIATION eSubvariation, INT iExtraParam = -1)
	
	UNUSED_PARAMETER(iExtraParam)
	
	FMP_PROP_DATA data = GET_FMP_PROP_DATA(iProp, eVariation, eSubvariation, iExtraParam)
	RETURN data.model
	
ENDFUNC

FUNC VECTOR GET_FMP_PROP_COORDS(INT iProp, PLACEHOLDER_VARIATION eVariation, PLACEHOLDER_SUBVARIATION eSubvariation, INT iExtraParam = -1)
	
	UNUSED_PARAMETER(iExtraParam)
	
	FMP_PROP_DATA data = GET_FMP_PROP_DATA(iProp, eVariation, eSubvariation, iExtraParam)
	RETURN data.vCoords
	
ENDFUNC

FUNC FLOAT GET_FMP_PROP_HEADING(INT iProp, PLACEHOLDER_VARIATION eVariation, PLACEHOLDER_SUBVARIATION eSubvariation, INT iExtraParam = -1)
	
	UNUSED_PARAMETER(iExtraParam)
	
	FMP_PROP_DATA data = GET_FMP_PROP_DATA(iProp, eVariation, eSubvariation, iExtraParam)
	RETURN data.fHeading
	
ENDFUNC

FUNC VECTOR GET_FMP_PROP_ROTATION(INT iProp, PLACEHOLDER_VARIATION eVariation, PLACEHOLDER_SUBVARIATION eSubvariation, INT iExtraParam = -1)
	
	UNUSED_PARAMETER(iExtraParam)
	
	FMP_PROP_DATA data = GET_FMP_PROP_DATA(iProp, eVariation, eSubvariation, iExtraParam)
	RETURN data.vRotation
	
ENDFUNC

FUNC VECTOR GET_FMP_SUPPORT_VEHICLE_SPAWN_COORDS(INT iVehicle)
	
	SWITCH iVehicle
		CASE 0			RETURN <<-2364.3950, 3351.1047, 31.8329>>
		CASE 1			RETURN <<-2316.0933, 3353.1084, 31.8368>>
		CASE 2			RETURN <<-2334.4307, 3363.3738, 31.8321>>
		CASE 3			RETURN <<-2353.2769, 3374.9424, 31.8329>>
		CASE 4			RETURN <<-2372.0039, 3385.2944, 31.8329>>
		CASE 5			RETURN <<-2224.3394, 3248.8999, 31.8102>>
		CASE 6			RETURN <<-2238.1506, 3223.9824, 31.8102>>
		CASE 7			RETURN <<-2251.9897, 3200.8274, 31.8100>>
		CASE 8			RETURN <<-2262.2456, 3183.0215, 31.8100>>
		CASE 9			RETURN <<-2383.7446, 3361.9934, 31.8329>>
	ENDSWITCH
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC FLOAT GET_FMP_SUPPORT_VEHICLE_SPAWN_HEADING(INT iVehicle)

	// Specific locations have specific coords for spawning support vehicles
	SWITCH iVehicle
		CASE 0			RETURN 329.9994
		CASE 1			RETURN 150.7997
		CASE 2			RETURN 150.7997
		CASE 3			RETURN 150.3997
		CASE 4			RETURN 150.3997
		CASE 5			RETURN 239.3995
		CASE 6			RETURN 238.5995
		CASE 7			RETURN 238.5995
		CASE 8			RETURN 238.5995
		CASE 9			RETURN 331.7995
	ENDSWITCH
	
	RETURN 0.0
ENDFUNC

FUNC VECTOR GET_FMP_SHARED_VEHICLE_SPAWN_COORD(PLACEHOLDER_VARIATION eVariation, INT iVehicle, BOOL bSupportVehicle = FALSE)
	
	UNUSED_PARAMETER(eVariation)
	
	IF bSupportVehicle
		RETURN GET_FMP_SUPPORT_VEHICLE_SPAWN_COORDS(iVehicle)
	ENDIF

	SWITCH iVehicle
		CASE 0			RETURN <<910.9286, 45.9237, 79.8989>>
		CASE 1			RETURN <<918.8007, 58.7535, 79.8989>>
		CASE 2			RETURN <<903.7056, 34.5147, 79.1406>>
		CASE 3			RETURN <<896.7097, 23.5363, 78.1498>>
		CASE 4			RETURN <<926.4149, 71.0739, 78.3736>>
		CASE 5			RETURN <<913.8373, 35.9826, 79.6427>>
		CASE 6			RETURN <<903.3990, 19.5460, 78.1507>>
		CASE 7			RETURN <<929.6061, 61.3647, 79.3707>>
		CASE 8			RETURN <<908.7288, 28.0629, 78.9269>>
	ENDSWITCH
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC FLOAT GET_FMP_SHARED_VEHICLE_SPAWN_HEADING(PLACEHOLDER_VARIATION eVariation, INT iVehicle, BOOL bSupportVehicle = FALSE)
	
	UNUSED_PARAMETER(eVariation)
	
	IF bSupportVehicle
		RETURN GET_FMP_SUPPORT_VEHICLE_SPAWN_HEADING(iVehicle)
	ENDIF

	SWITCH iVehicle
		CASE 0			RETURN 148.3990
		CASE 1			RETURN 148.5990
		CASE 2			RETURN 148.3990
		CASE 3			RETURN 148.3990
		CASE 4			RETURN 148.5990
		CASE 5			RETURN 327.9990
		CASE 6			RETURN 327.5990
		CASE 7			RETURN 327.9990
		CASE 8			RETURN 327.5990
	ENDSWITCH
	
	RETURN 0.0
ENDFUNC

#ENDIF
