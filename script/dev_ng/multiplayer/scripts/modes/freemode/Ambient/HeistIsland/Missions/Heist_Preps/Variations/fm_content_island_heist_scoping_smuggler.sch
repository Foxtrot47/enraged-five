USING "globals.sch"
USING "rage_builtins.sch"
USING "heist_preps/fm_content_island_heist_core.sch"

ENUM SCOPING_SMUGGLER_MODE_STATE
	eSCOPINGSMUGGLER_STATE_GO_TO_PLANE= 0,
	eSCOPINGSMUGGLER_STATE_STEAL_PLANE,
	eSCOPINGSMUGGLER_STATE_FLY_TO_ISLAND,
	eSCOPINGSMUGGLER_STATE_END
ENDENUM

ENUM SCOPING_SMUGGLER_CLIENT_MODE_STATE
	eSCOPINGSMUGGLER_STATECLIENT_GO_TO_PLANE = 0,
	eSCOPINGSMUGGLER_STATECLIENT_STEAL_PLANE,
	eSCOPINGSMUGGLER_STATECLIENT_ENTER_PLANE,
	eSCOPINGSMUGGLER_STATECLIENT_WAIT_IN_PLANE,
	eSCOPINGSMUGGLER_STATECLIENT_DELIVER_PLANE,
	eSCOPINGSMUGGLER_STATECLIENT_HELP_DELIVER_PLANE,
	eSCOPINGSMUGGLER_STATECLIENT_END
ENDENUM

ENUM SCOPING_SMUGGLER_DIALOGUE
	eSCOPINGSMUGGLER_DIALOGUE_OPENING_FIRST_TIME = 0,
	eSCOPINGSMUGGLER_DIALOGUE_OPENING,
	eSCOPINGSMUGGLER_DIALOGUE_PLANE_STOLEN_FOLLOW_UP_FIRST_TIME,
	eSCOPINGSMUGGLER_DIALOGUE_PLANE_STOLEN,

	eSCOPINGSMUGGLER_DIALOGUE_MAX
ENDENUM

ENUM SCOPING_SMUGGLER_HELP_TEXT
	eSCOPINGSMUGGLER_HELPTEXT_TIME= 0,
	eSCOPINGSMUGGLER_HELPTEXT_MAX
ENDENUM

CONST_INT SCOPING_SMUGGLER_VELUM	1
CONST_INT SCOPING_SMUGGLER_MUSIC_ID_GRAPESEED			0
CONST_INT SCOPING_SMUGGLER_MUSIC_ID_STEAL_PLANE			1
CONST_INT SCOPING_SMUGGLER_MUSIC_ID_FLY_TO_ISLAND		2

FUNC SCOPING_SMUGGLER_MODE_STATE SCOPING_SMUGGLER_GET_MODE_STATE()
	RETURN INT_TO_ENUM(SCOPING_SMUGGLER_MODE_STATE, GET_MODE_STATE_ID())
ENDFUNC

FUNC SCOPING_SMUGGLER_CLIENT_MODE_STATE SCOPING_SMUGGLER_GET_CLIENT_MODE_STATE()
	RETURN INT_TO_ENUM(SCOPING_SMUGGLER_CLIENT_MODE_STATE, GET_CLIENT_MODE_STATE_ID())
ENDFUNC

// Dialogue

FUNC STRING SCOPING_SMUGGLER_DIALOGUE_ROOT(INT iDialogue)

	SWITCH INT_TO_ENUM(SCOPING_SMUGGLER_DIALOGUE, iDialogue)
		CASE eSCOPINGSMUGGLER_DIALOGUE_OPENING_FIRST_TIME					RETURN "HS4PA_SP_1B"
		CASE eSCOPINGSMUGGLER_DIALOGUE_OPENING
			SWITCH serverBD.sMissionData.iRandomDialogue[iDialogue]
				CASE 0		RETURN "HS4PA_SP_1C"
				CASE 1		RETURN "HS4PA_SP_1D"
				CASE 2		RETURN "HS4PA_SP_1E"
			ENDSWITCH
		BREAK
		CASE eSCOPINGSMUGGLER_DIALOGUE_PLANE_STOLEN_FOLLOW_UP_FIRST_TIME	RETURN "HS4PA_STY_1C"
		CASE eSCOPINGSMUGGLER_DIALOGUE_PLANE_STOLEN
			SWITCH serverBD.sMissionData.iRandomDialogue[iDialogue]
				CASE 0		RETURN "HS4PA_SP_4A"
				CASE 1		RETURN "HS4PA_SP_4B"
				CASE 2		RETURN "HS4PA_SP_4C"
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN ""

ENDFUNC

FUNC STRING SCOPING_SMUGGLER_DIALOGUE_SUBTITLE_GROUP(INT iDialogue)

	UNUSED_PARAMETER(iDialogue)
	
	RETURN "HS4PAAU"

ENDFUNC

FUNC STRING SCOPING_SMUGGLER_DIALOGUE_CHARACTER(INT iDialogue, INT iSpeaker)

	UNUSED_PARAMETER(iDialogue)
	UNUSED_PARAMETER(iSpeaker)
	
	RETURN "HS4_PAVEL"

ENDFUNC

FUNC INT SCOPING_SMUGGLER_DIALOGUE_OPENING()
	
	IF IS_TERTIARY_PLAYTHROUGH()
		RETURN ENUM_TO_INT(eSCOPINGSMUGGLER_DIALOGUE_OPENING)
	ENDIF
	
	RETURN ENUM_TO_INT(eSCOPINGSMUGGLER_DIALOGUE_OPENING_FIRST_TIME)

ENDFUNC

FUNC BOOL SCOPING_SMUGGLER_DIALOGUE_ENABLE()	
	RETURN TRUE
ENDFUNC

FUNC BOOL SCOPING_SMUGGLER_DIALOGUE_TRIGGER(INT iDialogue)

	SWITCH INT_TO_ENUM(SCOPING_SMUGGLER_DIALOGUE, iDialogue)
		CASE eSCOPINGSMUGGLER_DIALOGUE_OPENING_FIRST_TIME					RETURN NOT IS_TERTIARY_PLAYTHROUGH()
		CASE eSCOPINGSMUGGLER_DIALOGUE_OPENING								RETURN IS_TERTIARY_PLAYTHROUGH()

		CASE eSCOPINGSMUGGLER_DIALOGUE_PLANE_STOLEN							RETURN (SCOPING_SMUGGLER_GET_CLIENT_MODE_STATE() = eSCOPINGSMUGGLER_STATECLIENT_DELIVER_PLANE
																				OR SCOPING_SMUGGLER_GET_CLIENT_MODE_STATE() = eSCOPINGSMUGGLER_STATECLIENT_HELP_DELIVER_PLANE)
																				AND HAS_NET_TIMER_EXPIRED(sDialogue.stTimer, 2000)
																		
		CASE eSCOPINGSMUGGLER_DIALOGUE_PLANE_STOLEN_FOLLOW_UP_FIRST_TIME	RETURN NOT IS_TERTIARY_PLAYTHROUGH() 
																		 		AND HAS_DIALOGUE_FINISHED(ENUM_TO_INT(eSCOPINGSMUGGLER_DIALOGUE_PLANE_STOLEN))
																		 		AND HAS_NET_TIMER_EXPIRED(sDialogue.stTimer, 15000)
	ENDSWITCH
		
	RETURN FALSE

ENDFUNC

FUNC STRING SCOPING_SMUGGLER_GET_MUSIC_EVENT(INT iMusicEvent)
	SWITCH iMusicEvent
		CASE SCOPING_SMUGGLER_MUSIC_ID_GRAPESEED
			RETURN "HEI4_INTRO_TO_GRAPESEED"
		
		CASE SCOPING_SMUGGLER_MUSIC_ID_STEAL_PLANE
			RETURN "HEI4_INTRO_STEAL_PLANE"
		
		CASE SCOPING_SMUGGLER_MUSIC_ID_FLY_TO_ISLAND
			RETURN "HEI4_INTRO_FLY_TO_ISLAND"
		
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC INT SCOPING_SMUGGLER_MUSIC_EVENT(INT iCurrentEvent)
	SWITCH iCurrentEvent
		CASE -1
			IF SCOPING_SMUGGLER_GET_MODE_STATE() >= eSCOPINGSMUGGLER_STATE_GO_TO_PLANE
				RETURN SCOPING_SMUGGLER_MUSIC_ID_GRAPESEED
			ENDIF
		BREAK
		
		CASE SCOPING_SMUGGLER_MUSIC_ID_GRAPESEED
			IF SCOPING_SMUGGLER_GET_MODE_STATE() >= eSCOPINGSMUGGLER_STATE_STEAL_PLANE
				RETURN SCOPING_SMUGGLER_MUSIC_ID_STEAL_PLANE
			ENDIF
		BREAK
		
		CASE SCOPING_SMUGGLER_MUSIC_ID_STEAL_PLANE
			IF SCOPING_SMUGGLER_GET_MODE_STATE() >= eSCOPINGSMUGGLER_STATE_FLY_TO_ISLAND
				RETURN SCOPING_SMUGGLER_MUSIC_ID_FLY_TO_ISLAND
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN -1
ENDFUNC

FUNC BOOL SCOPING_SMUGGLER_SHOULD_DELAY_UI()

	SWITCH SCOPING_SMUGGLER_GET_CLIENT_MODE_STATE()
		CASE eSCOPINGSMUGGLER_STATECLIENT_DELIVER_PLANE
		CASE eSCOPINGSMUGGLER_STATECLIENT_HELP_DELIVER_PLANE
			RETURN NOT HAS_DIALOGUE_TRIGGERED(ENUM_TO_INT(eSCOPINGSMUGGLER_DIALOGUE_PLANE_STOLEN)) 
				OR NOT HAS_NET_TIMER_EXPIRED(sLocalVariables.stMissionTimer, 5000)
	ENDSWITCH

	RETURN FALSE

ENDFUNC

FUNC BOOL SCOPING_SMUGGLER_GET_NEXT_MISSION(MISSION_REQUEST_DATA &missionData)
	IF NOT IS_GENERIC_BIT_SET(eGENERICBITSET_I_WON)
		RETURN FALSE
	ENDIF
	
	missionData.iFMMC = FMMC_TYPE_ISLAND_HEIST_PREP
	missionData.iVariation = ENUM_TO_INT(IHV_SCOPING_ISLAND)
	RETURN TRUE

ENDFUNC

PROC SCOPING_SMUGGLER_PED_ATTRIBUTES(INT iPed, PED_INDEX pedID, BOOL bAmbush)

	UNUSED_PARAMETER(pedID)
	UNUSED_PARAMETER(bAmbush)

	SWITCH iPed
		CASE 2
			SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_HEAD, 0, 1)
			SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_TORSO, 0, 2)
			SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_LEG, 0, 0)
			SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_SPECIAL2, 1, 0)
		BREAK
		CASE 3
			SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_HEAD, 1, 2)
			SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_TORSO, 0, 3)
			SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_LEG, 0, 2)
		BREAK
		CASE 4
			SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_HEAD, 0, 0)
			SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_TORSO, 0, 0)
			SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_LEG, 0, 3)
		BREAK
	ENDSWITCH

ENDPROC

FUNC BOOL SCOPING_SMUGGLER_VEHICLE_MODS(INT iVehicle, VEHICLE_SETUP_STRUCT_MP &sData)

	SWITCH data.Vehicle.Vehicles[iVehicle].model
		CASE VELUM2
			SCOPING_SET_VELUM_MODS(sData)
		BREAK
		DEFAULT
			RETURN FALSE
	ENDSWITCH

	RETURN TRUE

ENDFUNC

PROC SCOPING_SMUGGLER_VEHICLE_ATTRIBUTES(INT iVehicle, VEHICLE_INDEX vehId)

	SWITCH iVehicle
		CASE SCOPING_SMUGGLER_VELUM
			HOLD_VEHICLE_DOOR_OPEN(vehId, SC_DOOR_FRONT_LEFT, FALSE, TRUE, DT_DOOR_NO_RESET)
		BREAK
	ENDSWITCH

ENDPROC

FUNC BLIP_SPRITE SCOPING_SMUGGLER_MISSION_ENTITY_BLIP_SPRITE(INT iEntity)
	UNUSED_PARAMETER(iEntity)
	RETURN RADAR_TRACE_PLANE_DROP
ENDFUNC

FUNC STRING SCOPING_SMUGGLER_MISSION_ENTITY_BLIP_NAME(INT iEntity)
	UNUSED_PARAMETER(iEntity)
	RETURN "ILH_BLP_VELUM"
ENDFUNC

FUNC BOOL SCOPING_SMUGGLER_MISSION_ENTITY_BLIP_ROTATE(INT iEntity)
	UNUSED_PARAMETER(iEntity)
	RETURN TRUE
ENDFUNC

FUNC STRING SCOPING_SMUGGLER_HELP_TEXT_STRING(INT iHelp)

	SWITCH INT_TO_ENUM(SCOPING_SMUGGLER_HELP_TEXT, iHelp)
		CASE eSCOPINGSMUGGLER_HELPTEXT_TIME		RETURN "ILH_OT_TIME"
	ENDSWITCH
	
	RETURN ""

ENDFUNC

FUNC BOOL SCOPING_SMUGGLER_HELP_TEXT_TRIGGER(INT iHelp)

	SWITCH INT_TO_ENUM(SCOPING_SMUGGLER_HELP_TEXT, iHelp)
		CASE eSCOPINGSMUGGLER_HELPTEXT_TIME		RETURN IS_GENERIC_BIT_SET(eGENERICBITSET_NEAR_MISSION_END)
	ENDSWITCH
	
	RETURN FALSE

ENDFUNC

FUNC BOOL SCOPING_SMUGGLER_DELIVERY_SHOW_LOCATE()
	RETURN FALSE
ENDFUNC

FUNC STRING SCOPING_SMUGGLER_GET_GOTO_LABEL()

	SWITCH GET_SUBVARIATION()
		CASE IHS_SCO_SMG_LOCATION_1		RETURN "FMC_LOC_LZ"
		CASE IHS_SCO_SMG_LOCATION_2		RETURN "FMC_LOC_PALCOV"
		CASE IHS_SCO_SMG_LOCATION_3		RETURN "FMC_LOC_PROCOP"
		CASE IHS_SCO_SMG_LOCATION_4		RETURN "FMC_LOC_GRPSD"
		CASE IHS_SCO_SMG_LOCATION_5		RETURN "FMC_LOC_GRPSD"
	ENDSWITCH
	
	RETURN ""

ENDFUNC

PROC SCOPING_SMUGGLER_GO_TO_OBJECTIVE()
	Print_Objective_Text_With_Coloured_Text_Label("FMCOT_GOTO", SCOPING_SMUGGLER_GET_GOTO_LABEL(), HUD_COLOUR_YELLOW)
ENDPROC

PROC SCOPING_SMUGGLER_STEAL_PLANE_OBJECTIVE()
	Print_Objective_Text_With_Coloured_Text_Label("CSH_OT_STEAL_T", "ILH_OT_VELUM", HUD_COLOUR_BLUE)
ENDPROC

PROC SCOPING_SMUGGLER_ENTER_PLANE_OBJECTIVE()
	Print_Objective_Text("ILH_OT_ENTVE")
ENDPROC

PROC SCOPING_SMUGGLER_WAIT_IN_PLANE_OBJECTIVE()
	Print_Objective_Text("ILH_OT_WTENT")
ENDPROC

PROC SCOPING_SMUGGLER_DELIVER_PLANE_OBJECTIVE()
	Print_Objective_Text("ILH_OT_FLYVI")
ENDPROC

PROC SCOPING_SMUGGLER_HELP_DELIVER_PLANE_OBJECTIVE()
	Print_Objective_Text("ILH_OT_HFLYVI")
ENDPROC

FUNC BOOL SCOPING_SMUGGLER_CLIENT_PROGRESS_TO_ENTER()

	IF SCOPING_SMUGGLER_GET_MODE_STATE() <= eSCOPINGSMUGGLER_STATE_STEAL_PLANE
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_POSSESSION_OF_ANY_MISSION_ENTITY(LOCAL_PLAYER_INDEX)
		RETURN FALSE
	ENDIF

	IF IS_VEHICLE_CLIENT_BIT_SET(GET_MISSION_ENTITY_CARRIER(0), LOCAL_PARTICIPANT_INDEX, eVEHICLECLIENTBITSET_INSIDE_VEHICLE)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE

ENDFUNC

FUNC BOOL SCOPING_SMUGGLER_CLIENT_PROGRESS_TO_WAIT()
	
	IF SCOPING_SMUGGLER_GET_MODE_STATE() <= eSCOPINGSMUGGLER_STATE_STEAL_PLANE
		RETURN FALSE
	ENDIF
	
	IF IS_MISSION_SERVER_BIT_SET(eMISSIONSERVERBITSET_ALL_PLAYERS_IN_VEHICLE)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE

ENDFUNC

FUNC BOOL SCOPING_SMUGGLER_CLIENT_PROGRESS_TO_DELIVER()

	IF SCOPING_SMUGGLER_GET_MODE_STATE() <= eSCOPINGSMUGGLER_STATE_STEAL_PLANE
		RETURN FALSE
	ENDIF

	IF NOT IS_PLAYER_IN_POSSESSION_OF_ANY_MISSION_ENTITY(LOCAL_PLAYER_INDEX)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE

ENDFUNC

FUNC BOOL SCOPING_SMUGGLER_CLIENT_PROGRESS_TO_HELP_DELIVER()

	IF SCOPING_SMUGGLER_GET_MODE_STATE() <= eSCOPINGSMUGGLER_STATE_STEAL_PLANE
		RETURN FALSE
	ENDIF

	IF IS_PLAYER_IN_POSSESSION_OF_ANY_MISSION_ENTITY(LOCAL_PLAYER_INDEX)
		RETURN FALSE
	ENDIF

	RETURN TRUE

ENDFUNC

PROC SCOPING_SMUGGLER_SETUP_STATES()

	ADD_MODE_STATE(eSCOPINGSMUGGLER_STATE_GO_TO_PLANE, eMODESTATE_GO_TO_POINT)
		ADD_MODE_STATE_TRANSITION_DEFAULT(eSCOPINGSMUGGLER_STATE_GO_TO_PLANE, eSCOPINGSMUGGLER_STATE_STEAL_PLANE)
		
	ADD_MODE_STATE(eSCOPINGSMUGGLER_STATE_STEAL_PLANE, eMODESTATE_COLLECT_MISSION_ENTITY)
		ADD_MODE_STATE_TRANSITION_DEFAULT(eSCOPINGSMUGGLER_STATE_STEAL_PLANE, eSCOPINGSMUGGLER_STATE_FLY_TO_ISLAND)
		
	ADD_MODE_STATE(eSCOPINGSMUGGLER_STATE_FLY_TO_ISLAND, eMODESTATE_DELIVER_MISSION_ENTITY)
		ADD_MODE_STATE_TRANSITION_DEFAULT(eSCOPINGSMUGGLER_STATE_FLY_TO_ISLAND, eSCOPINGSMUGGLER_STATE_END)
		
	ADD_MODE_STATE(eSCOPINGSMUGGLER_STATE_END, eMODESTATE_END)

ENDPROC

PROC SCOPING_SMUGGLER_SETUP_CLIENT_STATES()

	ADD_CLIENT_MODE_STATE(eSCOPINGSMUGGLER_STATECLIENT_GO_TO_PLANE, eMODESTATE_GO_TO_POINT, &SCOPING_SMUGGLER_GO_TO_OBJECTIVE)
		ADD_CLIENT_MODE_STATE_TRANSITION_LINKED(eSCOPINGSMUGGLER_STATECLIENT_GO_TO_PLANE, eSCOPINGSMUGGLER_STATECLIENT_STEAL_PLANE, eSCOPINGSMUGGLER_STATE_STEAL_PLANE)
		
	ADD_CLIENT_MODE_STATE(eSCOPINGSMUGGLER_STATECLIENT_STEAL_PLANE, eMODESTATE_COLLECT_MISSION_ENTITY, &SCOPING_SMUGGLER_STEAL_PLANE_OBJECTIVE)
		ADD_CLIENT_MODE_STATE_TRANSITION_CUSTOM(eSCOPINGSMUGGLER_STATECLIENT_STEAL_PLANE, eSCOPINGSMUGGLER_STATECLIENT_ENTER_PLANE, &SCOPING_SMUGGLER_CLIENT_PROGRESS_TO_ENTER)
		ADD_CLIENT_MODE_STATE_TRANSITION_CUSTOM(eSCOPINGSMUGGLER_STATECLIENT_STEAL_PLANE, eSCOPINGSMUGGLER_STATECLIENT_WAIT_IN_PLANE, &SCOPING_SMUGGLER_CLIENT_PROGRESS_TO_WAIT)
		ADD_CLIENT_MODE_STATE_TRANSITION_CUSTOM(eSCOPINGSMUGGLER_STATECLIENT_STEAL_PLANE, eSCOPINGSMUGGLER_STATECLIENT_DELIVER_PLANE, &SCOPING_SMUGGLER_CLIENT_PROGRESS_TO_DELIVER)
		ADD_CLIENT_MODE_STATE_TRANSITION_CUSTOM(eSCOPINGSMUGGLER_STATECLIENT_STEAL_PLANE, eSCOPINGSMUGGLER_STATECLIENT_HELP_DELIVER_PLANE, &SCOPING_SMUGGLER_CLIENT_PROGRESS_TO_HELP_DELIVER)
		
	ADD_CLIENT_MODE_STATE(eSCOPINGSMUGGLER_STATECLIENT_ENTER_PLANE, eMODESTATE_ENTER_VEHICLE, &SCOPING_SMUGGLER_ENTER_PLANE_OBJECTIVE)
		ADD_CLIENT_MODE_STATE_TRANSITION_CUSTOM(eSCOPINGSMUGGLER_STATECLIENT_ENTER_PLANE, eSCOPINGSMUGGLER_STATECLIENT_ENTER_PLANE, &SCOPING_SMUGGLER_CLIENT_PROGRESS_TO_ENTER)
		ADD_CLIENT_MODE_STATE_TRANSITION_CUSTOM(eSCOPINGSMUGGLER_STATECLIENT_ENTER_PLANE, eSCOPINGSMUGGLER_STATECLIENT_WAIT_IN_PLANE, &SCOPING_SMUGGLER_CLIENT_PROGRESS_TO_WAIT)
		ADD_CLIENT_MODE_STATE_TRANSITION_CUSTOM(eSCOPINGSMUGGLER_STATECLIENT_ENTER_PLANE, eSCOPINGSMUGGLER_STATECLIENT_DELIVER_PLANE, &SCOPING_SMUGGLER_CLIENT_PROGRESS_TO_DELIVER)
		ADD_CLIENT_MODE_STATE_TRANSITION_CUSTOM(eSCOPINGSMUGGLER_STATECLIENT_ENTER_PLANE, eSCOPINGSMUGGLER_STATECLIENT_HELP_DELIVER_PLANE, &SCOPING_SMUGGLER_CLIENT_PROGRESS_TO_HELP_DELIVER)
			
	ADD_CLIENT_MODE_STATE(eSCOPINGSMUGGLER_STATECLIENT_WAIT_IN_PLANE, eMODESTATE_WAIT, &SCOPING_SMUGGLER_WAIT_IN_PLANE_OBJECTIVE)
		ADD_CLIENT_MODE_STATE_TRANSITION_CUSTOM(eSCOPINGSMUGGLER_STATECLIENT_WAIT_IN_PLANE, eSCOPINGSMUGGLER_STATECLIENT_ENTER_PLANE, &SCOPING_SMUGGLER_CLIENT_PROGRESS_TO_ENTER)
		ADD_CLIENT_MODE_STATE_TRANSITION_CUSTOM(eSCOPINGSMUGGLER_STATECLIENT_WAIT_IN_PLANE, eSCOPINGSMUGGLER_STATECLIENT_WAIT_IN_PLANE, &SCOPING_SMUGGLER_CLIENT_PROGRESS_TO_WAIT)
		ADD_CLIENT_MODE_STATE_TRANSITION_CUSTOM(eSCOPINGSMUGGLER_STATECLIENT_WAIT_IN_PLANE, eSCOPINGSMUGGLER_STATECLIENT_DELIVER_PLANE, &SCOPING_SMUGGLER_CLIENT_PROGRESS_TO_DELIVER)
		ADD_CLIENT_MODE_STATE_TRANSITION_CUSTOM(eSCOPINGSMUGGLER_STATECLIENT_WAIT_IN_PLANE, eSCOPINGSMUGGLER_STATECLIENT_HELP_DELIVER_PLANE, &SCOPING_SMUGGLER_CLIENT_PROGRESS_TO_HELP_DELIVER)
		
	ADD_CLIENT_MODE_STATE(eSCOPINGSMUGGLER_STATECLIENT_DELIVER_PLANE, eMODESTATE_DELIVER_MISSION_ENTITY, &SCOPING_SMUGGLER_DELIVER_PLANE_OBJECTIVE)
		ADD_CLIENT_MODE_STATE_TRANSITION_CUSTOM(eSCOPINGSMUGGLER_STATECLIENT_DELIVER_PLANE, eSCOPINGSMUGGLER_STATECLIENT_ENTER_PLANE, &SCOPING_SMUGGLER_CLIENT_PROGRESS_TO_ENTER)
		ADD_CLIENT_MODE_STATE_TRANSITION_CUSTOM(eSCOPINGSMUGGLER_STATECLIENT_DELIVER_PLANE, eSCOPINGSMUGGLER_STATECLIENT_WAIT_IN_PLANE, &SCOPING_SMUGGLER_CLIENT_PROGRESS_TO_WAIT)
		ADD_CLIENT_MODE_STATE_TRANSITION_CUSTOM(eSCOPINGSMUGGLER_STATECLIENT_DELIVER_PLANE, eSCOPINGSMUGGLER_STATECLIENT_HELP_DELIVER_PLANE, &SCOPING_SMUGGLER_CLIENT_PROGRESS_TO_HELP_DELIVER)
		
	ADD_CLIENT_MODE_STATE(eSCOPINGSMUGGLER_STATECLIENT_HELP_DELIVER_PLANE, eMODESTATE_HELP_DELIVER_MISSION_ENTITY, &SCOPING_SMUGGLER_HELP_DELIVER_PLANE_OBJECTIVE)
		ADD_CLIENT_MODE_STATE_TRANSITION_CUSTOM(eSCOPINGSMUGGLER_STATECLIENT_HELP_DELIVER_PLANE, eSCOPINGSMUGGLER_STATECLIENT_ENTER_PLANE, &SCOPING_SMUGGLER_CLIENT_PROGRESS_TO_ENTER)
		ADD_CLIENT_MODE_STATE_TRANSITION_CUSTOM(eSCOPINGSMUGGLER_STATECLIENT_HELP_DELIVER_PLANE, eSCOPINGSMUGGLER_STATECLIENT_WAIT_IN_PLANE, &SCOPING_SMUGGLER_CLIENT_PROGRESS_TO_WAIT)
		ADD_CLIENT_MODE_STATE_TRANSITION_CUSTOM(eSCOPINGSMUGGLER_STATECLIENT_HELP_DELIVER_PLANE, eSCOPINGSMUGGLER_STATECLIENT_DELIVER_PLANE, &SCOPING_SMUGGLER_CLIENT_PROGRESS_TO_DELIVER)
									
	ADD_CLIENT_MODE_STATE(eSCOPINGSMUGGLER_STATECLIENT_END, eMODESTATE_END, &EMPTY)

ENDPROC

PROC SCOPING_SMUGGLER_PRE_LOOP()

	IF NOT IS_LOCAL_HOST
		EXIT
	ENDIF

	SET_MISSION_SERVER_BIT(eMISSIONSERVERBITSET_ALL_PLAYERS_IN_VEHICLE #IF IS_DEBUG_BUILD , TRUE #ENDIF )
ENDPROC

PROC SCOPING_SMUGGLER_PARTICIPANT_LOOP(INT iParticipant, PARTICIPANT_INDEX participantID)
	
	UNUSED_PARAMETER(iParticipant)
	
	IF NOT IS_LOCAL_HOST
		EXIT
	ENDIF
	
	INT iCarrier = GET_MISSION_ENTITY_CARRIER(0)
	
	IF iCarrier = -1
		EXIT
	ENDIF
	
	IF NOT IS_VEHICLE_CLIENT_BIT_SET(iCarrier, participantID, eVEHICLECLIENTBITSET_INSIDE_VEHICLE)
		CLEAR_MISSION_SERVER_BIT(eMISSIONSERVERBITSET_ALL_PLAYERS_IN_VEHICLE #IF IS_DEBUG_BUILD , TRUE #ENDIF )
	ENDIF
	
ENDPROC

PROC SCOPING_SMUGGLER_CLEANUP()

	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF IS_GENERIC_BIT_SET(eGENERICBITSET_I_WON)
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			SCOPING_TRAVEL_TO_ISLAND(HEIST_ISLAND_JOIN_FADE_OUT_AND_IN)
			SET_PACKED_STAT_BOOL(PACKED_STAT_BOOL_FINALE_SMUGGLER_OUTFIT, TRUE)
		ENDIF
	ENDIF

ENDPROC

FUNC BOOL SCOPING_SMUGGLER_INIT_CLIENT()	
	RETURN TRUE
ENDFUNC

PROC SCOPING_SMUGGLER_SETUP_DATA()
	data.ModeTimer.iTimeLimit = 15
	data.ModeTimer.eDisplay = eMODETIMERDISPLAYTYPE_NEAR_END
	
	SET_DATA_BIT(eDATABITSET_IS_START_FLOW_MISSION)
	SET_DATA_BIT(eDATABITSET_DISABLE_DELIVERY_TICKERS)
ENDPROC

PROC SCOPING_SMUGGLER_SETUP_LOGIC()

	logic.Data.Setup = &SCOPING_SMUGGLER_SETUP_DATA

	logic.StateMachine.SetupStates = &SCOPING_SMUGGLER_SETUP_STATES
	logic.StateMachine.SetupClientStates = &SCOPING_SMUGGLER_SETUP_CLIENT_STATES

	logic.Maintain.Participant.PreLoop = &SCOPING_SMUGGLER_PRE_LOOP
	logic.Maintain.Participant.Loop = &SCOPING_SMUGGLER_PARTICIPANT_LOOP
		
	logic.Init.Client = &SCOPING_SMUGGLER_INIT_CLIENT

	logic.Cleanup.ScriptEnd = &SCOPING_SMUGGLER_CLEANUP
	
	logic.Ped.Attributes = &SCOPING_SMUGGLER_PED_ATTRIBUTES
	
	logic.MissionEntity.Blip.Sprite = &SCOPING_SMUGGLER_MISSION_ENTITY_BLIP_SPRITE
	logic.MissionEntity.Blip.Name = &SCOPING_SMUGGLER_MISSION_ENTITY_BLIP_NAME
	logic.MissionEntity.Blip.Rotate = &SCOPING_SMUGGLER_MISSION_ENTITY_BLIP_ROTATE
	
	logic.Vehicle.Mods = &SCOPING_SMUGGLER_VEHICLE_MODS
	logic.Vehicle.Attributes = &SCOPING_SMUGGLER_VEHICLE_ATTRIBUTES
	
	logic.Dialogue.Root = &SCOPING_SMUGGLER_DIALOGUE_ROOT
	logic.Dialogue.SubtitleGroup = &SCOPING_SMUGGLER_DIALOGUE_SUBTITLE_GROUP
	logic.Dialogue.ShouldTrigger = &SCOPING_SMUGGLER_DIALOGUE_TRIGGER
	logic.Dialogue.Character = &SCOPING_SMUGGLER_DIALOGUE_CHARACTER
	logic.Dialogue.Opening = &SCOPING_SMUGGLER_DIALOGUE_OPENING
	logic.Dialogue.Enable = &SCOPING_SMUGGLER_DIALOGUE_ENABLE
	
	logic.Audio.MusicTrigger.EventString = &SCOPING_SMUGGLER_GET_MUSIC_EVENT
	logic.Audio.MusicTrigger.Event = &SCOPING_SMUGGLER_MUSIC_EVENT
	logic.Audio.MusicTrigger.StopString = &GET_ISLAND_HEIST_MUSIC_STOP_EVENT
	logic.Audio.MusicTrigger.FailString = &GET_ISLAND_HEIST_MUSIC_FAIL_EVENT
	
	logic.Hud.ShouldDelayUI = &SCOPING_SMUGGLER_SHOULD_DELAY_UI

	logic.Reward.WinnerRP = &SCOPING_WINNER_RP
	logic.Reward.MinPartRp = &SCOPING_WINNER_RP

	logic.HelpText.Text = &SCOPING_SMUGGLER_HELP_TEXT_STRING
	logic.HelpText.Trigger = &SCOPING_SMUGGLER_HELP_TEXT_TRIGGER
	
	logic.Delivery.ShowLocate = &SCOPING_SMUGGLER_DELIVERY_SHOW_LOCATE
	
	logic.Mission.Next = &SCOPING_SMUGGLER_GET_NEXT_MISSION
	
ENDPROC
