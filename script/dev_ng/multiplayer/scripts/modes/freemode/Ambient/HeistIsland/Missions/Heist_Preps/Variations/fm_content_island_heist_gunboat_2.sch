USING "globals.sch"
USING "rage_builtins.sch"
USING "heist_preps/fm_content_island_heist_core.sch"

//////////////////////////
///    CONST INTS 
//////////////////////////

CONST_INT GB2_DIALOGUE_ID_INTRO				0
CONST_INT GB2_DIALOGUE_ID_ARRIVE			1
CONST_INT GB2_DIALOGUE_ID_BLIPPED			2
CONST_INT GB2_DIALOGUE_ID_COLLECT			3
CONST_INT GB2_DIALOGUE_ID_DELIVER			4
CONST_INT GB2_DIALOGUE_ID_TIME_RUNNING_OUT	5

CONST_INT GB2_HELP_ID_TAKE_OUT				0
CONST_INT GB2_HELP_ID_RIVER					1
CONST_INT GB2_HELP_ID_BOAT_FLEE				2

CONST_INT GB2_VEH_ID_AMBUSH_1				5
CONST_INT GB2_VEH_ID_AMBUSH_2				6

CONST_INT GB2_PED_ID_PATROL_BOAT_DRIVER		9		
CONST_INT GB2_PED_ID_PATROL_BOAT_GUNNER		10
CONST_INT GB2_PED_ID_MADRAZO				18

CONST_INT GB2_PED_ID_AMBUSH_1_DRIVER		11
CONST_INT GB2_PED_ID_AMBUSH_1_PASSENGER		12
CONST_INT GB2_PED_ID_AMBUSH_1_PASSENGER_2	13
CONST_INT GB2_PED_ID_AMBUSH_2_DRIVER		14
CONST_INT GB2_PED_ID_AMBUSH_2_PASSENGER		15
CONST_INT GB2_PED_ID_AMBUSH_2_PASSENGER_2	16

CONST_INT GB2_AMBUSH_ID_1					0
CONST_INT GB2_AMBUSH_ID_2					1

CONST_INT GB2_MISSION_ENTITY_ID				0

CONST_INT GB2_MUSIC_ID_SUSPENSE_START					0	
CONST_INT GB2_MUSIC_ID_GUNFIGHT_ARRIVE					1
CONST_INT GB2_MUSIC_ID_MED_INTENSITY_STEAL				2
CONST_INT GB2_MUSIC_ID_VEHICLE_ACTION_AMBUSH			3
CONST_INT GB2_MUSIC_ID_MED_INTENSITY_AMBUSH_CLEAR		4

//////////////////////////
///    ENUMS 
//////////////////////////

ENUM GBOAT2_MODE_STATE
	eGBOAT2STATE_GO_TO_BEACH = 0,
	eGBOAT2STATE_TAKE_OUT_TARGETS = 1,
	eGBOAT2STATE_STEAL_BOAT = 2,
	eGBOAT2STATE_DELIVER = 3,
	eGBOAT2STATE_END
ENDENUM

ENUM GBOAT2_CLIENT_MODE_STATE
	eGBOAT2STATECLIENT_GO_TO_BEACH = 0,
	eGBOAT2STATECLIENT_TAKE_OUT_TARGETS = 1,
	eGBOAT2STATECLIENT_STEAL_BOAT = 2,
	eGBOAT2STATECLIENT_DELIVER = 3,
	eGBOAT2STATECLIENT_HELP_DELIVER = 4,
	eGBOAT2STATECLIENT_RECOVER = 5,
	eGBOAT2STATECLIENT_END = 6
ENDENUM

ENUM GBOAT2_GUARD_TASK
	eGBOAT2GUARD_IDLE = 0,
	eGBOAT2GUARD_SCENARIO = 1,
	eGBOAT2GUARD_ATTACK = 2,
	eGBOAT2GUARD_ENTER_VEHICLE = 3
ENDENUM

ENUM GBOAT2_DRIVER_TASK
	eGBOAT2DRIVER_IDLE = 0,
	eGBOAT2DRIVER_GO_TO = 1,
	eGBOAT2DRIVER_FLEE = 2
ENDENUM

//////////////////////////
///    LOCAL VARIABLES 
//////////////////////////
  
BLIP_INDEX blipOcean

//////////////////////////
///    LOOKUPS 
//////////////////////////
  
FUNC VECTOR GBOAT2_GET_PATROL_BOAT_GO_TO_COORD(INT iCheckpoint)
	SWITCH GET_SUBVARIATION()
		CASE IHS_GB2_LOCATION_1				// BUILDING
			SWITCH iCheckpoint
				CASE 0			RETURN <<1441.6711, 3890.3782, 23.4201>>
				//CASE 1			RETURN <<1449.1276, 3810.4314, 28.5591>>
			ENDSWITCH
		BREAK
		CASE IHS_GB2_LOCATION_2				// BEACH
			SWITCH iCheckpoint
				CASE 0			RETURN <<2003.1326, 3990.7241, 27.0879>>
			ENDSWITCH
		BREAK
		CASE IHS_GB2_LOCATION_3				// DOCK	
			SWITCH iCheckpoint
				CASE 0			RETURN <<1317.8361, 4196.9595, 26.7098>>
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC BOOL GBOAT2_ARE_ALL_TARGETS_DEAD()
	RETURN (sTakeOutTargetLocal.iTargetsRemaining = 0) AND sTakeOutTargetLocal.iTargetsRegistered > 0
ENDFUNC

//////////////////////////
///    OBJECTIVE TEXT  
//////////////////////////

PROC GBOAT2_OBJTEXT_GO_TO()
	SWITCH GET_SUBVARIATION()
		CASE IHS_GB2_LOCATION_1
			Print_Objective_Text_With_Coloured_Text_Label("ILH_GOTO", "FMC_LOC_SS", HUD_COLOUR_YELLOW)
		BREAK
		CASE IHS_GB2_LOCATION_2
			Print_Objective_Text_With_Coloured_Text_Label("ILH_GOTO", "FMC_LOC_MARDRV", HUD_COLOUR_YELLOW)
		BREAK
		CASE IHS_GB2_LOCATION_3
			Print_Objective_Text_With_Coloured_Text_Label("ILH_GOTO", "FMC_LOC_GALIL", HUD_COLOUR_YELLOW)
		BREAK
	ENDSWITCH
ENDPROC

PROC GBOAT2_OBJTEXT_TAKE_OUT_TARGETS()
	Print_Objective_Text_With_Coloured_Text_Label("ILH_OT_DISRUPT", "ILH_TKE_SMUG", HUD_COLOUR_RED)
ENDPROC

PROC GBOAT2_OBJTEXT_STEAL_PATROL_BOAT()
	IF NOT IS_MISSION_SERVER_BIT_SET(eMISSIONSERVERBITSET_MOVE_TO_STEAL_PATROL_BOAT)
		Print_Objective_Text("ILH_OT_WAITGB")
	ELSE
		IF HAS_DIALOGUE_FINISHED(GB2_DIALOGUE_ID_BLIPPED)
			GET_STEAL_OBJECTIVE_TEXT_BLUE()
		ENDIF
	ENDIF
ENDPROC

PROC GBOAT2_OBJTEXT_DELIVER_PATROL_BOAT()
	IF NOT IS_MISSION_SERVER_BIT_SET(eMISSIONSERVERBITSET_EXITED_RIVER)
		IF HAS_DIALOGUE_FINISHED(GB2_DIALOGUE_ID_COLLECT) 
			Print_Objective_Text("ILH_OT_ESCOCN")
		ELSE
			Clear_Any_Objective_Text_From_This_Script()
		ENDIF
	ELSE
		IF HAS_DIALOGUE_FINISHED(GB2_DIALOGUE_ID_DELIVER) 
			GET_DELIVER_OBJECTIVE_TEXT()
		ELSE
			Clear_Any_Objective_Text_From_This_Script()
		ENDIF
	ENDIF
ENDPROC

PROC GBOAT2_OBJTEXT_HELP_DELIVER_PATROL_BOAT()
	IF NOT IS_MISSION_SERVER_BIT_SET(eMISSIONSERVERBITSET_EXITED_RIVER)
		IF HAS_DIALOGUE_FINISHED(GB2_DIALOGUE_ID_COLLECT) 
			GET_HELP_DELIVER_OBJECTIVE_TEXT()
		ELSE
			Clear_Any_Objective_Text_From_This_Script()
		ENDIF
	ELSE
		IF HAS_DIALOGUE_FINISHED(GB2_DIALOGUE_ID_DELIVER) 
			GET_HELP_DELIVER_OBJECTIVE_TEXT()
		ELSE
			Clear_Any_Objective_Text_From_This_Script()
		ENDIF
	ENDIF
ENDPROC

//////////////////////////
///    MODE STATES  
//////////////////////////

FUNC BOOL GBOAT2_MOVE_ON_FROM_TAKE_OUT()
	IF IS_PED_BIT_SET(GB2_PED_ID_PATROL_BOAT_DRIVER, ePEDBITSET_I_HAVE_REACHED_FINAL_CHECKPOINT)
	OR GBOAT2_ARE_ALL_TARGETS_DEAD()
	OR HAS_MISSION_ENTITY_BEEN_COLLECTED_FOR_FIRST_TIME(GB2_MISSION_ENTITY_ID)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC GBOAT2_SETUP_MODE_STATES()
	ADD_MODE_STATE(eGBOAT2STATE_GO_TO_BEACH, eMODESTATE_GO_TO_POINT)
		ADD_MODE_STATE_TRANSITION_DEFAULT(eGBOAT2STATE_GO_TO_BEACH, eGBOAT2STATE_TAKE_OUT_TARGETS)
		
	ADD_MODE_STATE(eGBOAT2STATE_TAKE_OUT_TARGETS, eMODESTATE_TAKE_OUT_TARGETS)
		ADD_MODE_STATE_TRANSITION_CUSTOM(eGBOAT2STATE_TAKE_OUT_TARGETS, eGBOAT2STATE_STEAL_BOAT, &GBOAT2_MOVE_ON_FROM_TAKE_OUT)
		
	ADD_MODE_STATE(eGBOAT2STATE_STEAL_BOAT, eMODESTATE_COLLECT_MISSION_ENTITY)
		ADD_MODE_STATE_TRANSITION_DEFAULT(eGBOAT2STATE_STEAL_BOAT, eGBOAT2STATE_DELIVER)
	
	ADD_MODE_STATE(eGBOAT2STATE_DELIVER, eMODESTATE_DELIVER_MISSION_ENTITY)
		ADD_MODE_STATE_TRANSITION_DEFAULT(eGBOAT2STATE_DELIVER, eGBOAT2STATE_END)
	
	ADD_MODE_STATE(eGBOAT2STATE_END, eMODESTATE_END)
ENDPROC

PROC GBOAT2_SETUP_CLIENT_MODE_STATES()
	ADD_CLIENT_MODE_STATE(eGBOAT2STATECLIENT_GO_TO_BEACH, eMODESTATE_GO_TO_POINT, &GBOAT2_OBJTEXT_GO_TO)
		ADD_CLIENT_MODE_STATE_TRANSITION_LINKED(eGBOAT2STATECLIENT_GO_TO_BEACH, eGBOAT2STATECLIENT_TAKE_OUT_TARGETS, eGBOAT2STATE_TAKE_OUT_TARGETS)
		
	ADD_CLIENT_MODE_STATE(eGBOAT2STATECLIENT_TAKE_OUT_TARGETS, eMODESTATE_TAKE_OUT_TARGETS, &GBOAT2_OBJTEXT_TAKE_OUT_TARGETS)
		ADD_CLIENT_MODE_STATE_TRANSITION_LINKED(eGBOAT2STATECLIENT_TAKE_OUT_TARGETS, eGBOAT2STATECLIENT_STEAL_BOAT, eGBOAT2STATE_STEAL_BOAT)

	ADD_CLIENT_MODE_STATES_DELIVERY(eGBOAT2STATECLIENT_STEAL_BOAT, eGBOAT2STATECLIENT_DELIVER, eGBOAT2STATECLIENT_HELP_DELIVER, eGBOAT2STATECLIENT_RECOVER, eGBOAT2STATECLIENT_END,
		&GBOAT2_OBJTEXT_STEAL_PATROL_BOAT, &GBOAT2_OBJTEXT_DELIVER_PATROL_BOAT, &GBOAT2_OBJTEXT_HELP_DELIVER_PATROL_BOAT, &GET_RECOVER_OBJECTIVE_TEXT)
	
	ADD_CLIENT_MODE_STATE(eGBOAT2STATECLIENT_END, eMODESTATE_END, &EMPTY)
ENDPROC

FUNC BOOL GBOAT2_ENABLE_TOT_HUD(INT iStage)
	UNUSED_PARAMETER(iStage)
	RETURN FALSE
ENDFUNC

FUNC BOOL GBOAT2_DRAW_TOT_PED_ARROWS(INT iPed)
	UNUSED_PARAMETER(iPed)
	RETURN FALSE
ENDFUNC

//////////////////////////
///    VEHICLES  
//////////////////////////

PROC GBOAT2_VEHICLE_ATTRIBUTES(INT iVehicle, VEHICLE_INDEX vehId)
	IF IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_AMBUSH_VEHICLE)
		EXIT
	ENDIF
	
	IF data.Vehicle.Vehicles[iVehicle].model = PATROLBOAT
		SET_BOAT_ANCHOR(vehId, FALSE)
		SET_VEHICLE_ALLOW_NO_PASSENGERS_LOCKON(vehId, FALSE)
		SET_VEHICLE_ALLOW_HOMING_MISSLE_LOCKON(vehId, FALSE, TRUE)
		SET_VEHICLE_ALLOW_HOMING_MISSLE_LOCKON_SYNCED(vehId, FALSE, TRUE) 
	ENDIF
	
	SET_VEHICLE_ENGINE_ON(vehId, TRUE, TRUE)
	SET_VEHICLE_LIGHTS(vehId, FORCE_VEHICLE_LIGHTS_ON)
ENDPROC

//////////////////////////
///    PEDS  
//////////////////////////

FUNC BOOL GBOAT2_IS_PED_DRIVER(INT iPed)
	IF iPed = GB2_PED_ID_PATROL_BOAT_DRIVER
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL GBOAT2_IS_PED_GUNNER(INT iPed)
	IF data.Ped.Peds[iPed].eSeat != VS_DRIVER
	AND data.Ped.Peds[iPed].iVehicle != (-1)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC GBOAT2_PED_TRIGGERS(INT iPed)
	ADD_PED_TRIGGER(iPed, &PED_TRIGGER_HAS_PED_BEEN_DAMAGED)
	ADD_PED_TRIGGER(iPed, &PED_TRIGGER_HAS_PED_VEHICLE_BEEN_DAMAGED)
	ADD_PED_TRIGGER(iPed, &PED_TRIGGER_HAS_PED_RECEIVED_PED_EVENT)
	ADD_PED_TRIGGER(iPed, &PED_TRIGGER_HAS_PED_BEEN_KNOCKED_INTO)
	ADD_PED_TRIGGER(iPed, &PED_TRIGGER_HAS_PED_TRIGGER_AREA_BEEN_ACTIVATED)
	ADD_PED_TRIGGER(iPed, &PED_TRIGGER_HAS_PED_BEEN_TARGETTED)
ENDPROC

FUNC BOOL GBOAT2_PED_TO_ATTACK(INT iPed, PED_INDEX pedId)
	UNUSED_PARAMETER(pedId)
	IF IS_PED_DATA_BIT_SET(iPed, ePEDDATABITSET_I_AM_A_CIVILIAN)
		RETURN FALSE
	ENDIF
	
	IF HAS_PED_BEEN_TRIGGERED(iPed)
		IF GBOAT2_IS_PED_GUNNER(iPed)
			RETURN IS_PED_IN_ANY_VEHICLE(pedId)
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL GBOAT2_PED_TO_GO_TO(INT iPed, PED_INDEX pedId)
	UNUSED_PARAMETER(iPed)
	UNUSED_PARAMETER(pedId)
	
	IF GET_MODE_STATE_ID() > ENUM_TO_INT(eGBOAT2STATE_GO_TO_BEACH)
	AND NOT IS_PED_BIT_SET(iPed, ePEDBITSET_I_HAVE_REACHED_FINAL_CHECKPOINT)
		IF (sTakeOutTargetLocal.iTargetsRemaining < data.TakeOutTarget[0].iCount)
		AND sTakeOutTargetLocal.iTargetsRegistered > 0
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL GBOAT2_BOAT_PED_TO_FLEE(INT iPed, PED_INDEX pedId)
	UNUSED_PARAMETER(pedId)
	RETURN IS_PED_BIT_SET(iPed, ePEDBITSET_I_HAVE_REACHED_FINAL_CHECKPOINT)
ENDFUNC

FUNC BOOL GBOAT2_GUNNER_PED_BACK_ON_GUN(INT iPed, PED_INDEX pedId)
	IF GBOAT2_IS_PED_GUNNER(iPed)
		RETURN NOT IS_PED_IN_ANY_VEHICLE(pedId)
	ENDIF
	RETURN FALSE
ENDFUNC

PROC GBOAT2_SETUP_PED_TASKS(INT iBehaviour)

	SWITCH iBehaviour
		CASE 0
			ADD_PED_TASK(iBehaviour, eGBOAT2GUARD_IDLE, ePEDTASK_IDLE)
				ADD_PED_TASK_TRANSITION(iBehaviour, eGBOAT2GUARD_IDLE, eGBOAT2GUARD_ATTACK, &GBOAT2_PED_TO_ATTACK)
				ADD_PED_TASK_TRANSITION(iBehaviour, eGBOAT2GUARD_IDLE, eGBOAT2GUARD_SCENARIO, &DOES_PED_HAVE_SCENARIO)
				
			ADD_PED_TASK(iBehaviour, eGBOAT2GUARD_SCENARIO, ePEDTASK_DO_SCENARIOS)
				ADD_PED_TASK_TRANSITION(iBehaviour, eGBOAT2GUARD_SCENARIO, eGBOAT2GUARD_ATTACK, &GBOAT2_PED_TO_ATTACK)
			
			ADD_PED_TASK(iBehaviour, eGBOAT2GUARD_ATTACK, ePEDTASK_ATTACK_HATED_TARGETS)
				ADD_PED_TASK_TRANSITION(iBehaviour, eGBOAT2GUARD_ATTACK, eGBOAT2GUARD_ENTER_VEHICLE, &GBOAT2_GUNNER_PED_BACK_ON_GUN)
				
			ADD_PED_TASK(iBehaviour, eGBOAT2GUARD_ENTER_VEHICLE, ePEDTASK_ENTER_VEHICLE)
				ADD_PED_TASK_TRANSITION(iBehaviour, eGBOAT2GUARD_ENTER_VEHICLE, eGBOAT2GUARD_ATTACK, &GBOAT2_PED_TO_ATTACK)
		BREAK
		CASE 1
			ADD_PED_TASK(iBehaviour, eGBOAT2DRIVER_IDLE, ePEDTASK_IDLE)
				ADD_PED_TASK_TRANSITION(iBehaviour, eGBOAT2DRIVER_IDLE, eGBOAT2DRIVER_GO_TO, &GBOAT2_PED_TO_GO_TO)
			
			ADD_PED_TASK(iBehaviour, eGBOAT2DRIVER_GO_TO, ePEDTASK_VEHICLE_GO_TO_POINT)
				ADD_PED_TASK_TRANSITION(iBehaviour, eGBOAT2DRIVER_GO_TO, eGBOAT2DRIVER_FLEE, &GBOAT2_BOAT_PED_TO_FLEE)
				
			ADD_PED_TASK(iBehaviour, eGBOAT2DRIVER_FLEE, ePEDTASK_FLEE_IN_VEHICLE)	
			//ADD_PED_TASK(iBehaviour, eGBOAT2DRIVER_ATTACK, ePEDTASK_ATTACK_HATED_TARGETS)
		BREAK
	ENDSWITCH

ENDPROC

FUNC INT GBOAT2_GET_PED_BEHAVIOUR(INT iPed)

	IF GBOAT2_IS_PED_DRIVER(iPed)
		RETURN 1
	ENDIF
	
	RETURN 0

ENDFUNC

PROC GBOAT2_SET_RANDOM_PED_COMPONENTS(PED_INDEX pedId)
	SWITCH GET_RANDOM_INT_IN_RANGE(0, 4)
		CASE 0
			SET_PED_COMPONENT_VARIATION(pedId, PED_COMP_HEAD, 0, 1)
			SET_PED_COMPONENT_VARIATION(pedId, PED_COMP_TORSO, 0, 2)
			SET_PED_COMPONENT_VARIATION(pedId, PED_COMP_LEG, 0, 0)
		BREAK
		CASE 1
			SET_PED_COMPONENT_VARIATION(pedId, PED_COMP_HEAD, 1, 2)
			SET_PED_COMPONENT_VARIATION(pedId, PED_COMP_TORSO, 0, 3)
			SET_PED_COMPONENT_VARIATION(pedId, PED_COMP_LEG, 0, 2)
		BREAK
		CASE 2
			SET_PED_COMPONENT_VARIATION(pedId, PED_COMP_HEAD, 0, 0)
			SET_PED_COMPONENT_VARIATION(pedId, PED_COMP_TORSO, 0, 0)
			SET_PED_COMPONENT_VARIATION(pedId, PED_COMP_LEG, 0, 3)
		BREAK
		CASE 3
			SET_PED_COMPONENT_VARIATION(pedId, PED_COMP_HEAD, 0, 0)
			SET_PED_COMPONENT_VARIATION(pedId, PED_COMP_TORSO, 0, 1)
			SET_PED_COMPONENT_VARIATION(pedId, PED_COMP_LEG, 0, 1)
		BREAK
	ENDSWITCH
ENDPROC

PROC GBOAT2_PED_ATTRIBUTES(INT iPed, PED_INDEX pedId, BOOL bAmbush)
	UNUSED_PARAMETER(iPed)
	UNUSED_PARAMETER(pedId)
	UNUSED_PARAMETER(bAmbush)
	
	IF NOT bAmbush
		SET_PED_COMBAT_ATTRIBUTES(pedId, CA_USE_VEHICLE, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(pedId, CA_LEAVE_VEHICLES, FALSE)
		SET_PED_COMBAT_ATTRIBUTES(pedID, CA_FORCE_CHECK_ATTACK_ANGLE_FOR_MOUNTED_GUNS, TRUE)
		
		IF data.Ped.Peds[iPed].iVehicle != (-1)
			IF data.Ped.Peds[iPed].eSeat = VS_DRIVER
				SET_TREAT_AS_AMBIENT_PED_FOR_DRIVER_LOCKON(pedId, TRUE)
				SET_PED_CAN_BE_TARGETTED(pedId, FALSE)
			ENDIF
			SET_PED_CAN_RAGDOLL(pedId, FALSE)
		ENDIF
	ENDIF
	
	IF iPed = GB2_PED_ID_PATROL_BOAT_GUNNER
		SET_PED_FIRING_PATTERN(pedId, FIRING_PATTERN_BURST_FIRE)
	ENDIF
	
	IF data.Ped.Peds[iPed].model = A_M_Y_BEACH_01
		GBOAT2_SET_RANDOM_PED_COMPONENTS(pedId)
	ENDIF
ENDPROC

PROC GBOAT2_PED_SERVER_MAINTAIN(INT iPed, PED_INDEX pedID, BOOL bDead)
	UNUSED_PARAMETER(pedID)
	
	IF IS_MISSION_SERVER_BIT_SET(eMISSIONSERVERBITSET_MOVE_TO_STEAL_PATROL_BOAT)
		EXIT
	ENDIF
	
	IF iPed = GB2_PED_ID_PATROL_BOAT_DRIVER
		IF IS_PED_BIT_SET(iPed, ePEDBITSET_I_HAVE_REACHED_FINAL_CHECKPOINT)
			SET_MISSION_SERVER_BIT(eMISSIONSERVERBITSET_MOVE_TO_STEAL_PATROL_BOAT)
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] GBOAT2_PED_SERVER_MAINTAIN - Patrol boat driver has reached destination correctly, moving on.")
		ENDIF
		
		IF bDead
			SET_MISSION_SERVER_BIT(eMISSIONSERVERBITSET_MOVE_TO_STEAL_PATROL_BOAT)
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] GBOAT2_PED_SERVER_MAINTAIN - Patrol boat driver has been killed early, moving on.")
		ENDIF
		
		IF HAS_MISSION_ENTITY_BEEN_COLLECTED_FOR_FIRST_TIME(GB2_MISSION_ENTITY_ID)
			SET_MISSION_SERVER_BIT(eMISSIONSERVERBITSET_MOVE_TO_STEAL_PATROL_BOAT)
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] GBOAT2_PED_SERVER_MAINTAIN - Mission entity collected early, moving on.")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL GBOAT2_SHOULD_BLIP_PED(INT iPed)
	SWITCH iPed
		CASE GB2_PED_ID_MADRAZO			RETURN (GET_MODE_STATE_ID() = ENUM_TO_INT(eGBOAT2STATE_DELIVER))
	ENDSWITCH
	
	RETURN (IS_PED_BIT_SET(iPed, ePEDBITSET_TARGET_PED) AND GET_CLIENT_MODE_STATE() = eMODESTATE_TAKE_OUT_TARGETS) AND GET_PED_STATE(iPed) = ePEDSTATE_ACTIVE
ENDFUNC

FUNC STRING GBOAT2_GET_PED_BLIP_NAME(INT iPed)
	SWITCH iPed
		CASE GB2_PED_ID_MADRAZO			RETURN "ILH_BLP_MADCAR"
	ENDSWITCH
	RETURN "ILH_BLP_SMUGG"
ENDFUNC

FUNC BOOL GBOAT2_SHOULD_BLIP_PED_SHORT_RANGE(INT iPed)
	SWITCH iPed
		CASE GB2_PED_ID_MADRAZO			RETURN TRUE
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL GBOAT2_SHOULD_PED_BLIP_FLASH_ON_CREATION(INT iPed)
	SWITCH iPed
		CASE GB2_PED_ID_MADRAZO			RETURN FALSE
	ENDSWITCH
	RETURN IS_PED_BIT_SET(iPed, ePEDBITSET_TARGET_PED)
ENDFUNC

FUNC FLOAT GBOAT2_GET_PED_BLIP_SCALE(INT iPed)
	SWITCH iPed
		CASE GB2_PED_ID_MADRAZO			RETURN 0.5
	ENDSWITCH
	RETURN 0.75
ENDFUNC

FUNC BLIP_SPRITE GBOAT2_GET_PED_BLIP_SPRITE(INT iPed)
	SWITCH iPed
		CASE GB2_PED_ID_MADRAZO			RETURN RADAR_TRACE_INVALID
	ENDSWITCH
	RETURN RADAR_TRACE_TEMP_4
ENDFUNC


//////////////////////////
///    MAINTAIN
//////////////////////////

PROC GBOAT2_REMOVE_OCEAN_BLIP()
	IF DOES_BLIP_EXIST(blipOcean)
		REMOVE_BLIP(blipOcean)
	ENDIF
ENDPROC

FUNC VECTOR GBOAT2_GET_OCEAN_COORD()
	RETURN <<-3654.4668, 3932.4048, 0.0>>
ENDFUNC

PROC GBOAT2_CLIENT_MAINTAIN()
	IF HAS_DIALOGUE_FINISHED(GB2_DIALOGUE_ID_COLLECT) 
		IF NOT IS_MISSION_SERVER_BIT_SET(eMISSIONSERVERBITSET_EXITED_RIVER)
			IF bSafeForUI
			AND GET_END_REASON() = eENDREASON_NO_REASON_YET
				IF NOT DOES_BLIP_EXIST(blipOcean)
					blipOcean = ADD_BLIP_FOR_COORD(GBOAT2_GET_OCEAN_COORD())
					SET_BLIP_PRIORITY(blipOcean, BLIP_PRIORITY_HIGHEST_SPECIAL_HIGH)
					SET_BLIP_ROUTE(blipOcean, FALSE)
				ENDIF
			ELSE
				GBOAT2_REMOVE_OCEAN_BLIP()
			ENDIF
			
			IF IS_LOCAL_PLAYER_IN_POSSESSION_OF_ANY_MISSION_ENTITY()
				IF IS_ENTITY_IN_ANGLED_AREA(LOCAL_PED_INDEX, <<-1942.382568,4573.389160,-2.102951>>, <<-1817.570313,4692.034180,34.509293>>, 39.250000)	// Raton
				OR IS_ENTITY_IN_ANGLED_AREA(LOCAL_PED_INDEX, <<-2678.028564,2493.468994,-2.574283>>, <<-2614.934326,2918.898926,33.345146>>, 28.250000) // Zancudo
				OR VDIST_2D(LOCAL_PLAYER_COORD, GBOAT2_GET_OCEAN_COORD()) < 1500.0		// Failsafe
					SET_MISSION_CLIENT_BIT(eMISSIONCLIENTBITSET_EXITED_RIVER)
				ENDIF
			ENDIF
		ELSE
			GBOAT2_REMOVE_OCEAN_BLIP()
		ENDIF
	ENDIF
ENDPROC

//////////////////////////
///    MISSION ENTITY  
//////////////////////////


//////////////////////////
///    PLACEMENT DATA 
//////////////////////////

PROC GBOAT2_PLACEMENT_DATA()
	data.ModeTimer.iTimeLimit = 30
	data.ModeTimer.eDisplay = eMODETIMERDISPLAYTYPE_NEAR_END
	
	data.TakeOutTarget[0].iCount = 10
	data.TakeOutTarget[0].TakeOutTargets[9].eType = ET_PED
	data.TakeOutTarget[0].TakeOutTargets[9].iIndex = 17
	
	INT iLoop
	REPEAT data.Vehicle.iCount iLoop
		CLEAR_VEHICLE_DATA_BIT(iLoop, eVEHICLEDATABITSET_SPAWN_FROZEN)
	ENDREPEAT
	
	REPEAT data.TriggerArea.iCount iLoop
		IF IS_TRIGGER_AREA_DATA_BIT_SET(iLoop, eTRIGGERAREADATABITSET_WARNING_AREA) 
			SET_TRIGGER_AREA_DATA_BIT(iLoop, eTRIGGERAREADATABITSET_RESTRICTED_AREA)
		ENDIF
	ENDREPEAT
	
	data.Ped.Peds[GB2_PED_ID_PATROL_BOAT_GUNNER].eSeat = VS_BACK_RIGHT
	
	SET_PED_DATA_BIT(GB2_PED_ID_MADRAZO, ePEDDATABITSET_I_AM_A_CIVILIAN)
	
	SETUP_AMBUSH_PED_DRIVER_DATA(GB2_AMBUSH_ID_1, 	GB2_PED_ID_AMBUSH_1_DRIVER,	  GB2_VEH_ID_AMBUSH_1)
	SETUP_AMBUSH_PED_PASSENGER_DATA(GB2_AMBUSH_ID_1, GB2_PED_ID_AMBUSH_1_PASSENGER, GB2_VEH_ID_AMBUSH_1, VS_BACK_LEFT)
	SETUP_AMBUSH_PED_PASSENGER_DATA(GB2_AMBUSH_ID_1, GB2_PED_ID_AMBUSH_1_PASSENGER_2, GB2_VEH_ID_AMBUSH_1, VS_BACK_RIGHT)
	
	SETUP_AMBUSH_PED_DRIVER_DATA(GB2_AMBUSH_ID_2, 	GB2_PED_ID_AMBUSH_2_DRIVER,	  GB2_VEH_ID_AMBUSH_2)
	SETUP_AMBUSH_PED_PASSENGER_DATA(GB2_AMBUSH_ID_2, GB2_PED_ID_AMBUSH_2_PASSENGER, GB2_VEH_ID_AMBUSH_2, VS_BACK_LEFT)
	SETUP_AMBUSH_PED_PASSENGER_DATA(GB2_AMBUSH_ID_2, GB2_PED_ID_AMBUSH_2_PASSENGER_2, GB2_VEH_ID_AMBUSH_2, VS_BACK_RIGHT)
	
	OVERRIDE_AMBUSH_WAVE_AND_SPAWN_DATA(4, DEFAULT, 90000, 15000)
ENDPROC

//////////////////////////
///   HELP TEXT
//////////////////////////

FUNC BOOL GBOAT2_HELP_TRIGGERS(INT iHelp)
	UNUSED_PARAMETER(iHelp)
	SWITCH iHelp
		CASE GB2_HELP_ID_TAKE_OUT			RETURN GET_CLIENT_MODE_STATE_ID() = ENUM_TO_INT(eGBOAT2STATECLIENT_TAKE_OUT_TARGETS)
		CASE GB2_HELP_ID_BOAT_FLEE			RETURN HAS_DIALOGUE_FINISHED(GB2_DIALOGUE_ID_BLIPPED) AND GET_MODE_STATE_ID() = ENUM_TO_INT(eGBOAT2STATE_STEAL_BOAT)
		CASE GB2_HELP_ID_RIVER				RETURN HAS_DIALOGUE_FINISHED(GB2_DIALOGUE_ID_COLLECT) AND GET_MODE_STATE_ID() = ENUM_TO_INT(eGBOAT2STATE_DELIVER)
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC STRING GBOAT2_HELP_TEXT(INT iHelp)
	UNUSED_PARAMETER(iHelp)
	SWITCH iHelp
		CASE GB2_HELP_ID_TAKE_OUT			RETURN "ILH_HT_G2TKE"	
		CASE GB2_HELP_ID_BOAT_FLEE			RETURN "ILH_HT_G2FLEE"
		CASE GB2_HELP_ID_RIVER				RETURN "ILH_HT_G2RIV"
	ENDSWITCH
	RETURN ""
ENDFUNC

PROC GBOAT2_HELP_TEXT_ON_DISPLAY(INT iHelp)
	UNUSED_PARAMETER(iHelp)
//	SWITCH iHelp
//		
//	ENDSWITCH
ENDPROC

//////////////////////////
///   DIALOGUE
//////////////////////////
    
FUNC STRING GBOAT2_DIALOGUE_ROOT(INT iDialogue)
	IF iDialogue = GB2_DIALOGUE_ID_TIME_RUNNING_OUT
		RETURN "HS4PA_GEN_6"
	ENDIF

	SWITCH GET_SUBVARIATION()
		CASE IHS_GB2_LOCATION_1
			SWITCH iDialogue
				CASE GB2_DIALOGUE_ID_INTRO		RETURN "HS4PA_VG2_1A"
				CASE GB2_DIALOGUE_ID_ARRIVE		RETURN "HS4PA_VG2_2A"
				CASE GB2_DIALOGUE_ID_BLIPPED	RETURN "HS4PA_VG2_3A"
				CASE GB2_DIALOGUE_ID_COLLECT	RETURN "HS4PA_VG2_4A"
				CASE GB2_DIALOGUE_ID_DELIVER	RETURN "HS4PA_VG2_5A"
			ENDSWITCH	
		BREAK
		
		CASE IHS_GB2_LOCATION_2
			SWITCH iDialogue
				CASE GB2_DIALOGUE_ID_INTRO		RETURN "HS4PA_VG2_1B"
				CASE GB2_DIALOGUE_ID_ARRIVE		RETURN "HS4PA_VG2_2A"
				CASE GB2_DIALOGUE_ID_BLIPPED	RETURN "HS4PA_VG2_3B"
				CASE GB2_DIALOGUE_ID_COLLECT	RETURN "HS4PA_VG2_4B"
				CASE GB2_DIALOGUE_ID_DELIVER	RETURN "HS4PA_VG2_5B"
			ENDSWITCH	
		BREAK
		
		CASE IHS_GB2_LOCATION_3
			SWITCH iDialogue
				CASE GB2_DIALOGUE_ID_INTRO		RETURN "HS4PA_VG2_1C"
				CASE GB2_DIALOGUE_ID_ARRIVE		RETURN "HS4PA_VG2_2A"
				CASE GB2_DIALOGUE_ID_BLIPPED	RETURN "HS4PA_VG2_3C"
				CASE GB2_DIALOGUE_ID_COLLECT	RETURN "HS4PA_VG2_4C"
				CASE GB2_DIALOGUE_ID_DELIVER	RETURN "HS4PA_VG2_5C"
			ENDSWITCH	
		BREAK
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC STRING GBOAT2_DIALOGUE_SUBTITLE_GROUP(INT iDialogue)
	UNUSED_PARAMETER(iDialogue)
	RETURN "HS4PAAU"
ENDFUNC

FUNC STRING GBOAT2_DIALOGUE_CHARACTER(INT iDialogue, INT iSpeaker)
	UNUSED_PARAMETER(iDialogue)
	UNUSED_PARAMETER(iSpeaker)
	RETURN "HS4_PAVEL"
ENDFUNC

FUNC BOOL GBOAT2_DIALOGUE_TRIGGER(INT iDialogue)
	SWITCH iDialogue
		CASE GB2_DIALOGUE_ID_INTRO		RETURN TRUE
		CASE GB2_DIALOGUE_ID_ARRIVE		RETURN HAS_DIALOGUE_FINISHED(GB2_DIALOGUE_ID_INTRO) AND GET_MODE_STATE_ID() = ENUM_TO_INT(eGBOAT2STATE_TAKE_OUT_TARGETS)
		CASE GB2_DIALOGUE_ID_BLIPPED		
			IF IS_PED_BIT_SET(GB2_PED_ID_PATROL_BOAT_DRIVER, ePEDBITSET_I_HAVE_REACHED_FINAL_CHECKPOINT)
			AND GET_MODE_STATE_ID() = ENUM_TO_INT(eGBOAT2STATE_STEAL_BOAT)
			AND HAS_DIALOGUE_FINISHED(GB2_DIALOGUE_ID_ARRIVE)
				RETURN TRUE
			ENDIF
		BREAK
		CASE GB2_DIALOGUE_ID_COLLECT		RETURN HAS_ANY_MISSION_ENTITY_BEEN_COLLECTED_FOR_THE_FIRST_TIME() AND HAS_NET_TIMER_EXPIRED(sDialogue.stTimer, 3000)
		CASE GB2_DIALOGUE_ID_DELIVER		RETURN IS_MISSION_SERVER_BIT_SET(eMISSIONSERVERBITSET_EXITED_RIVER)
		
		CASE GB2_DIALOGUE_ID_TIME_RUNNING_OUT		RETURN IS_GENERIC_BIT_SET(eGENERICBITSET_NEAR_MISSION_END)
	ENDSWITCH	
	RETURN FALSE
ENDFUNC

FUNC VECTOR GBOAT2_BOAT_GO_TO_COORD(INT iPed, INT iCheckpoint)
	IF iPed = GB2_PED_ID_PATROL_BOAT_DRIVER
		RETURN GBOAT2_GET_PATROL_BOAT_GO_TO_COORD(iCheckpoint)
	ENDIF
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC INT GBOAT2_NUM_BOAT_GO_TO_COORDS(INT iPed)
	IF iPed = GB2_PED_ID_PATROL_BOAT_DRIVER
		SWITCH GET_SUBVARIATION()
			CASE IHS_GB2_LOCATION_1		RETURN 1
			CASE IHS_GB2_LOCATION_2		RETURN 1
			CASE IHS_GB2_LOCATION_3		RETURN 1
		ENDSWITCH
	ENDIF
	RETURN 0
ENDFUNC

FUNC BOATMODE GBOAT2_GO_TO_BOAT_SETTINGS(INT iPed)
	UNUSED_PARAMETER(iPed)
	RETURN BCF_OPENOCEANSETTINGS
ENDFUNC

FUNC VEHICLE_INDEX GBOAT2_GET_BOAT_TO_FLEE_IN(INT iPed)
	IF data.Ped.Peds[iPed].iVehicle != (-1)
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBd.sVehicle[data.Ped.Peds[iPed].iVehicle].netId)
			RETURN NET_TO_VEH(serverBd.sVehicle[data.Ped.Peds[iPed].iVehicle].netId)
		ENDIF
	ENDIF
	RETURN INT_TO_NATIVE(VEHICLE_INDEX, -1)
ENDFUNC

//////////////////////////
///   MISSION ENTITY
//////////////////////////

FUNC BOOL GBOAT2_SHOULD_BLIP_MISSION_ENTITY(INT iMissionEntity)
	UNUSED_PARAMETER(iMissionEntity)
	
	SWITCH GET_CLIENT_MODE_STATE() 
		CASE eMODESTATE_COLLECT_MISSION_ENTITY
		CASE eMODESTATE_DELIVER_MISSION_ENTITY
		CASE eMODESTATE_HELP_DELIVER_MISSION_ENTITY
		CASE eMODESTATE_RECOVER_MISSION_ENTITY
			RETURN HAS_DIALOGUE_FINISHED(GB2_DIALOGUE_ID_BLIPPED)
	ENDSWITCH
	RETURN FALSE
ENDFUNC


//////////////////////////
///   FOCUS CAM
//////////////////////////

FUNC BOOL GBOAT2_ENABLE_FOCUS_CAM(INT iCam)
	SWITCH iCam
		CASE 0			RETURN GET_MODE_STATE_ID() = ENUM_TO_INT(eGBOAT2STATE_STEAL_BOAT)
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC STRING GBOAT2_FOCUS_CAM_STRING(INT iCam)
	SWITCH iCam
		CASE 0			RETURN "ILH_HT_G2FOC"
	ENDSWITCH
	RETURN "GR_TOGFOCUS"
ENDFUNC

//////////////////////////
///   DELIVERY
//////////////////////////

FUNC BOOL GBOAT2_SHOULD_SHOW_DELIVERY_UI()
	RETURN IS_MISSION_SERVER_BIT_SET(eMISSIONSERVERBITSET_EXITED_RIVER) AND HAS_DIALOGUE_FINISHED(GB2_DIALOGUE_ID_DELIVER)
ENDFUNC
   
//////////////////////////
///   PARTICIPANT LOOP
//////////////////////////

PROC GBOAT2_PARTICIPANT_LOOP(INT iParticipant, PARTICIPANT_INDEX participantID)
	UNUSED_PARAMETER(iParticipant)
	
	IF NOT IS_LOCAL_HOST
		EXIT
	ENDIF
		
	IF IS_MISSION_CLIENT_BIT_SET(participantID, eMISSIONCLIENTBITSET_EXITED_RIVER)
		SET_MISSION_SERVER_BIT(eMISSIONSERVERBITSET_EXITED_RIVER)
	ENDIF
ENDPROC

//////////////////////////
///   MUSIC EVENTS
//////////////////////////

FUNC STRING GBOAT2_MUSIC_EVENT_STRINGS(INT iMusicEvent)
	SWITCH iMusicEvent
		CASE GB2_MUSIC_ID_SUSPENSE_START				RETURN "HEI4_SUSPENSE_START"
		CASE GB2_MUSIC_ID_GUNFIGHT_ARRIVE				RETURN "HEI4_GUNFIGHT"
		CASE GB2_MUSIC_ID_MED_INTENSITY_STEAL			RETURN "HEI4_MED_INTENSITY"	
		CASE GB2_MUSIC_ID_VEHICLE_ACTION_AMBUSH			RETURN "HEI4_VEHICLE_ACTION"
		CASE GB2_MUSIC_ID_MED_INTENSITY_AMBUSH_CLEAR	RETURN "HEI4_MED_INTENSITY"
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC STRING GBOAT2_MUSIC_STOP_STRING()
	RETURN "HEI4_MUSIC_STOP"
ENDFUNC

FUNC STRING GBOAT2_MUSIC_FAIL_STRING()
	RETURN "HEI4_FAIL"
ENDFUNC

FUNC INT GBOAT2_MUSIC_EVENT(INT iCurrentEvent)
	SWITCH iCurrentEvent
		CASE -1
			IF GET_CLIENT_MODE_STATE_ID() >= ENUM_TO_INT(eGBOAT2STATECLIENT_GO_TO_BEACH)
				RETURN GB2_MUSIC_ID_SUSPENSE_START
			ENDIF
		BREAK
		
		CASE GB2_MUSIC_ID_SUSPENSE_START
			IF HAS_ANY_PED_GROUP_BEEN_TRIGGERED()
				RETURN GB2_MUSIC_ID_GUNFIGHT_ARRIVE
			ENDIF
		BREAK
		
		// Arrive at smugglers, take out stage
		CASE GB2_MUSIC_ID_GUNFIGHT_ARRIVE
			IF GET_CLIENT_MODE_STATE_ID() >= ENUM_TO_INT(eGBOAT2STATECLIENT_STEAL_BOAT)
				RETURN GB2_MUSIC_ID_MED_INTENSITY_STEAL
			ENDIF
		BREAK
		
		// Player told to steal the boat
		CASE GB2_MUSIC_ID_MED_INTENSITY_STEAL
			IF HAS_AMBUSH_ACTIVATED()
				RETURN GB2_MUSIC_ID_VEHICLE_ACTION_AMBUSH
			ENDIF
		BREAK
		
		// Ambush helis chase player in boat
		CASE GB2_MUSIC_ID_VEHICLE_ACTION_AMBUSH
			IF IS_AMBUSH_OVER() OR sMissionEntityLocal.sMissionEntity[0].fDistanceFromDropoff < GET_AMBUSH_DROPOFF_DISMISS_RANGE()
				RETURN GB2_MUSIC_ID_MED_INTENSITY_AMBUSH_CLEAR // Ambush waves are finished OR close enough to dropoff
			ENDIF
		BREAK												
	ENDSWITCH
	
	RETURN -1
ENDFUNC

//////////////////////////
///   AMBUSH
////////////////////////// 

FUNC BOOL GBOAT2_FORCE_AMBUSH_CLEANUP()
	RETURN IS_MISSION_SERVER_BIT_SET(eMISSIONSERVERBITSET_EXITED_RIVER)
ENDFUNC
  
//////////////////////////
///   THE GUTS
//////////////////////////

PROC GBOAT2_SETUP_LOGIC()
	// Data Init
	logic.Data.Setup = &GBOAT2_PLACEMENT_DATA
	
	// Client Maintain
	logic.Maintain.Client = &GBOAT2_CLIENT_MAINTAIN
	
	// States
	logic.StateMachine.SetupStates = &GBOAT2_SETUP_MODE_STATES
	logic.StateMachine.SetupClientStates = &GBOAT2_SETUP_CLIENT_MODE_STATES
	
	// Delivery
	logic.Delivery.ShowUI = &GBOAT2_SHOULD_SHOW_DELIVERY_UI
	
	// Take Out Target
	logic.TakeOutTarget.BottomRightHUD.Enable = &GBOAT2_ENABLE_TOT_HUD
	logic.TakeOutTarget.DrawPedArrow = &GBOAT2_DRAW_TOT_PED_ARROWS
	
	// Mission Entity
	logic.MissionEntity.Blip.Enable = &GBOAT2_SHOULD_BLIP_MISSION_ENTITY
	
	// Peds
	logic.Ped.Triggers.Setup = &GBOAT2_PED_TRIGGERS
	logic.Ped.Behaviour.Setup = &GBOAT2_SETUP_PED_TASKS
	logic.Ped.Behaviour.Get = &GBOAT2_GET_PED_BEHAVIOUR
	logic.Ped.Attributes = &GBOAT2_PED_ATTRIBUTES
	logic.Ped.Server = &GBOAT2_PED_SERVER_MAINTAIN
	
	logic.Ped.Blip.Enable = &GBOAT2_SHOULD_BLIP_PED
	logic.Ped.Blip.Name = &GBOAT2_GET_PED_BLIP_NAME
	logic.Ped.Blip.ShortRange = &GBOAT2_SHOULD_BLIP_PED_SHORT_RANGE
	logic.Ped.Blip.Sprite = &GBOAT2_GET_PED_BLIP_SPRITE
	logic.Ped.Blip.Scale = &GBOAT2_GET_PED_BLIP_SCALE
	logic.Ped.Blip.FlashOnCreation = &GBOAT2_SHOULD_PED_BLIP_FLASH_ON_CREATION
	
	logic.Ped.Task.VehicleGoToPoint.Coords = &GBOAT2_BOAT_GO_TO_COORD
	logic.Ped.Task.VehicleGoToPoint.NumCoords = &GBOAT2_NUM_BOAT_GO_TO_COORDS
	logic.Ped.Task.VehicleGoToPoint.BoatSettings = &GBOAT2_GO_TO_BOAT_SETTINGS
	
	logic.Ped.Task.FleeInVehicle.VehToUse = &GBOAT2_GET_BOAT_TO_FLEE_IN
	
	// Vehicles
	logic.Vehicle.Attributes = &GBOAT2_VEHICLE_ATTRIBUTES
	
	// Ambush
	logic.Ambush.ForceCleanup = &GBOAT2_FORCE_AMBUSH_CLEANUP
	
	// Help Text
	logic.HelpText.Trigger = &GBOAT2_HELP_TRIGGERS
	logic.HelpText.Text = &GBOAT2_HELP_TEXT
	logic.HelpText.OnDisplay = &GBOAT2_HELP_TEXT_ON_DISPLAY
	
	// Focus Cam
	logic.FocusCam.Enable = &GBOAT2_ENABLE_FOCUS_CAM
	logic.FocusCam.Prompt = &GBOAT2_FOCUS_CAM_STRING
	
	// Participant Loop
	logic.Maintain.Participant.Loop = &GBOAT2_PARTICIPANT_LOOP
	
	// Dialogue
	logic.Dialogue.Root = &GBOAT2_DIALOGUE_ROOT
	logic.Dialogue.SubtitleGroup = &GBOAT2_DIALOGUE_SUBTITLE_GROUP
	logic.Dialogue.ShouldTrigger = &GBOAT2_DIALOGUE_TRIGGER
	logic.Dialogue.Character = &GBOAT2_DIALOGUE_CHARACTER
	
	// Music
	logic.Audio.MusicTrigger.EventString = &GBOAT2_MUSIC_EVENT_STRINGS
	logic.Audio.MusicTrigger.StopString = &GBOAT2_MUSIC_STOP_STRING
	logic.Audio.MusicTrigger.FailString = &GBOAT2_MUSIC_FAIL_STRING
	logic.Audio.MusicTrigger.Event = &GBOAT2_MUSIC_EVENT
ENDPROC
