


// ___________________________________________
//
//	Game: 
//	------
//	GTAV.
//
//
//  Script: 
//	-------
// 	Backup heli launching.
// 
//
//  Original Author: 
//	-------
//	William.Kennedy@RockstarNorth.com
//
//  Adaption Author: 
//	-------
//	Tom Turner
//
// 	Description: 
//	------------
//	Player can call a backup helicopter to help them in the heist island finale
//  adapted from net_backup_heli_launching.sch
// ___________________________________________ 

USING "globals.sch"
USING "script_ped.sch"
USING "commands_player.sch"
USING "script_network.sch"
USING "commands_ped.sch"
USING "commands_task.sch"
USING "net_script_timers.sch"
USING "net_mission.sch"

USING "net_include.sch"
USING "fm_unlocks_header.sch"
USING "net_freemode_cut.sch"

#IF FEATURE_HEIST_ISLAND

ENUM ISLAND_BACKUP_HELI_LAUNCHING_STATE
	ISLAND_BACKUP_HELI_LAUNCHING_STATE_INIT = 0,
	ISLAND_BACKUP_HELI_LAUNCHING_STATE_WAIT,
	ISLAND_BACKUP_HELI_LAUNCHING_STATE_LAUNCHING,
	ISLAND_BACKUP_HELI_LAUNCHING_STATE_RUNNING
ENDENUM

STRUCT ISLAND_BACKUP_HELI_LAUNCHING_DATA
	ISLAND_BACKUP_HELI_LAUNCHING_STATE eState
ENDSTRUCT

#IF IS_DEBUG_BUILD
FUNC STRING GET_ISLAND_BACKUP_HELI_LAUNCHING_STATE_NAME(ISLAND_BACKUP_HELI_LAUNCHING_STATE eState)
	
	SWITCH eState
		
		CASE ISLAND_BACKUP_HELI_LAUNCHING_STATE_INIT			RETURN "INIT"
		CASE ISLAND_BACKUP_HELI_LAUNCHING_STATE_WAIT			RETURN "WAIT"
		CASE ISLAND_BACKUP_HELI_LAUNCHING_STATE_LAUNCHING		RETURN "LAUNCHING"
		CASE ISLAND_BACKUP_HELI_LAUNCHING_STATE_RUNNING			RETURN "RUNNING"
	ENDSWITCH
	
	RETURN "NOT_IN_SWITCH"
	
ENDFUNC
#ENDIF

PROC SET_ISLAND_BACKUP_HELI_LAUNCHING_STATE(ISLAND_BACKUP_HELI_LAUNCHING_DATA &sData, ISLAND_BACKUP_HELI_LAUNCHING_STATE eNewState)
	
	#IF IS_DEBUG_BUILD
		IF sData.eState != eNewState
			NET_PRINT("[ISLAND_BACKUP_HELI][LAUNCHING] - Backup Heli Launching - changing state to ")NET_PRINT(GET_ISLAND_BACKUP_HELI_LAUNCHING_STATE_NAME(eNewState))NET_NL()
		ENDIF
	#ENDIF
	
	sData.eState = eNewState
	
ENDPROC

/// PURPOSE:
///    Sets the global for launching the island backup heli, when this is set to true
///    the backup heli script will be launched
/// PARAMS:
///    bLaunch - if the island backup heli should be launched
PROC _HEIST_ISLAND__GLOBAL_SET_LAUNCH_ISLAND_BACKUP_HELI(BOOL bLaunch)
	MPGlobalsAmbience.bLaunchIslandBackupHeli = bLaunch
	PRINTLN("[ISLAND_BACKUP_HELI][LAUNCHING] GLOBAL_SET_LAUNCH_ISLAND_BACKUP_HELI - bLaunchIslandBackupHeli: ", bLaunch)
	DEBUG_PRINTCALLSTACK()
ENDPROC

/// PURPOSE:
///    Sets the global for if the island backup helicopter should be cancelled and made to leave the area
///    this prints the debug callstack so we can keep track of calls
/// PARAMS:
///    bCancel - if the helicopter should be cancelled
PROC _HEIST_ISLAND__GLOBAL_SET_CANCEL_ISLAND_BACKUP_HELI(BOOL bCancel)
	MPGlobalsAmbience.bCancelIslandBackupHeli = bCancel
	PRINTLN("[ISLAND_BACKUP_HELI][LAUNCHING] GLOBAL_SET_CANCEL_ISLAND_BACKUP_HELI - bCancelIslandBackupHeli: ", bCancel)
	DEBUG_PRINTCALLSTACK()
ENDPROC

PROC _HEIST_ISLAND__GLOBAL_SET_TERMINATE_ISLAND_BACKUP_HELI(BOOL bTerminate)
	MPGlobalsAmbience.bTerminateIslandBackupHeli = bTerminate
	PRINTLN("[ISLAND_BACKUP_HELI][LAUNCHING] GLOBAL_SET_TERMINATE_ISLAND_BACKUP_HELI - bTerminateIslandBackupHeli: ", bTerminate)
	DEBUG_PRINTCALLSTACK()
ENDPROC


/// PURPOSE:
///    Checks if a cancel request has been made to the island backup helicopter
///    when this is true we should make the helicopter leave
/// RETURNS:
///    TRUE if the backup helicopter should be cancelled
FUNC BOOL HEIST_ISLAND_BACKUP_HELI__IS_CANCELED()
	RETURN MPGlobalsAmbience.bCancelIslandBackupHeli
ENDFUNC

/// PURPOSE:
///    Checks if the heist island finale backup helicopter is currently active
///    This checks if the script is active, meaning the helicopter may still be leaving the area
/// RETURNS:
///    TRUE if the island backup helicopter script is active
FUNC BOOL HEIST_ISLAND_BACKUP_HELI__IS_ACTIVE()
	RETURN GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_ISLAND_BACKUP_HELI")) > 0
ENDFUNC

/// PURPOSE:
///    Requests the heist island finale backup helicopter to spawn and fly to the players position
///    The heli will target peds that are angry at the player then leave after a set duration.
PROC HEIST_ISLAND_BACKUP_HELI__CALL_BACKUP()
	_HEIST_ISLAND__GLOBAL_SET_LAUNCH_ISLAND_BACKUP_HELI(TRUE)
	PRINTLN("[ISLAND_BACKUP_HELI][LAUNCHING] CALL_BACKUP - Requested heli")
ENDPROC

/// PURPOSE:
///    Cancels the island backup helicopter making it leave the area and kill
///    the script when the heli has despawned
PROC HEIST_ISLAND_BACKUP_HELI__CANCEL_BACKUP()
	IF HEIST_ISLAND_BACKUP_HELI__IS_ACTIVE()
		_HEIST_ISLAND__GLOBAL_SET_CANCEL_ISLAND_BACKUP_HELI(TRUE)
		PRINTLN("[ISLAND_BACKUP_HELI][LAUNCHING] CANCEL_BACKUP - Cancelled heli backup")
	ENDIF
ENDPROC

/// PURPOSE:
///    Immediately terminates the heli script deleting the helicopter instantly without waiting for it to leave.
///    best used for cleanup scenarios
PROC HEIST_ISLAND_BACKUP_HELI__TERMINATE()
	IF HEIST_ISLAND_BACKUP_HELI__IS_ACTIVE()
		_HEIST_ISLAND__GLOBAL_SET_TERMINATE_ISLAND_BACKUP_HELI(TRUE)
		PRINTLN("[ISLAND_BACKUP_HELI][LAUNCHING] TERMINATE - Terminated heli backup")
	ENDIF
ENDPROC

/// PURPOSE:
///    Gets the helicopter being used for the heist island backup helicopter ability
/// RETURNS:
///    The vehicle index of the backup heli OR NULL if it does not exist
FUNC VEHICLE_INDEX HEIST_ISLAND_BACKUP_HELI__GET_HELI_VEHICLE()
	RETURN mpglobals.sIslandBackupHeli.viHeli
ENDFUNC

/// PURPOSE:
///    Gets the pilot ped being used for the heist island backup helicopter ability
/// RETURNS:
///    The ped piloting backup heli OR NULL if it does not exist
FUNC PED_INDEX HEIST_ISLAND_BACKUP_HELI__GET_PILOT()
	RETURN mpglobals.sIslandBackupHeli.viPeds[0]
ENDFUNC

/// PURPOSE:
///    Gets the first gunner ped being used for the heist island backup helicopter ability
/// RETURNS:
///    The ped acting as first gunner for the backup heli OR NULL if it does not exist
FUNC PED_INDEX HEIST_ISLAND_BACKUP_HELI__GET_GUNNER_ONE()
	RETURN mpglobals.sIslandBackupHeli.viPeds[1]
ENDFUNC

/// PURPOSE:
///    Gets the second gunner ped being used for the heist island backup helicopter ability
/// RETURNS:
///    The ped acting as second gunner for the backup heli OR NULL if it does not exist
FUNC PED_INDEX HEIST_ISLAND_BACKUP_HELI__GET_GUNNER_TWO()
	RETURN mpglobals.sIslandBackupHeli.viPeds[2]
ENDFUNC

/// PURPOSE:
///    Gets the health of the island backup helicopter, returns -1 if the heli doesn't exist
/// RETURNS:
///    THe health of the backup heli OR -1 if it does not exist
FUNC INT HEIST_ISLAND_BACKUP_HELI__GET_HELI_HEALTH()
	IF NOT DOES_ENTITY_EXIST(mpglobals.sIslandBackupHeli.viHeli)
		RETURN -1
	ENDIF
	
	RETURN GET_ENTITY_HEALTH(mpglobals.sIslandBackupHeli.viHeli)
ENDFUNC

/// PURPOSE:
///    Gets the max health of the island backup helicopter, returns -1 if the heli doesn't exist
/// RETURNS:
///    The max health of the backup heli OR -1 if it does not exist
FUNC INT HEIST_ISLAND_BACKUP_HELI__GET_HELI_MAX_HEALTH()
	IF NOT DOES_ENTITY_EXIST(mpglobals.sIslandBackupHeli.viHeli)
		RETURN -1
	ENDIF
	
	RETURN GET_ENTITY_MAX_HEALTH(mpglobals.sIslandBackupHeli.viHeli)
ENDFUNC

/// PURPOSE:
///    Launches the island backup heli script for players that are in the same gang as the local player
///    and the same tutorial session if one is active
/// PARAMS:
///    iInstance - the script intsance for the player to join
PROC _ISLAND_BACKUP_HELI__LAUNCH_SCRIPT_FOR_GANG_MEMBERS(INT iInstance)
	INT iParticipant
	INT iMaxParticipant = NETWORK_GET_MAX_NUM_PARTICIPANTS()
	INT iPlayersToRunScript
	
	PRINTLN("[ISLAND_BACKUP_HELI][LAUNCHING] LAUNCH_SCRIPT_FOR_GANG_MEMBERS - iMaxParticipant: ", iMaxParticipant)
	
	PARTICIPANT_INDEX piParticipant
	PLAYER_INDEX piPlayer
	PLAYER_INDEX piLocalPlayer = PLAYER_ID()
	
	REPEAT iMaxParticipant iParticipant
		piParticipant = INT_TO_PARTICIPANTINDEX(iParticipant)
		
		IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(piParticipant)
     		RELOOP
		ENDIF
	
		piPlayer = NETWORK_GET_PLAYER_INDEX(piParticipant)
		
		IF NOT IS_NET_PLAYER_OK(piPlayer)
			RELOOP
		ENDIF
		
		IF piPlayer = piLocalPlayer
			RELOOP
		ENDIF
		
		IF NOT GB_ARE_PLAYERS_MEMBERS_OF_SAME_GANG(piPlayer, piLocalPlayer)
			RELOOP
		ENDIF
		
		IF NETWORK_IS_IN_TUTORIAL_SESSION()
			IF NETWORK_GET_PLAYER_TUTORIAL_SESSION_INSTANCE(piPlayer) != NETWORK_GET_PLAYER_TUTORIAL_SESSION_INSTANCE(piLocalPlayer)
				RELOOP
			ENDIF
		ENDIF
		
		SET_BIT(iPlayersToRunScript, NATIVE_TO_INT(piPlayer))
		PRINTLN("[ISLAND_BACKUP_HELI][LAUNCHING] LAUNCH_SCRIPT_FOR_GANG_MEMBERS - adding (", NATIVE_TO_INT(piPlayer), ") ", GET_PLAYER_NAME(piPlayer))	
	ENDREPEAT		
	
	BROADCAST_LAUNCH_ISLAND_BACKUP_HELI(iInstance, iPlayersToRunScript)
ENDPROC

/// PURPOSE:
///    Maintains requests for launching the heist island finale backup helicopter
/// PARAMS:
///    sData - the backup heli launch data
PROC MAINTAIN_ISLAND_BACKUP_HELI_LAUNCHING(ISLAND_BACKUP_HELI_LAUNCHING_DATA &sData)
	CONST_INT ciScriptHash HASH("AM_ISLAND_BACKUP_HELI")
	
	SWITCH sData.eState
		
		CASE ISLAND_BACKUP_HELI_LAUNCHING_STATE_INIT
			SET_ISLAND_BACKUP_HELI_LAUNCHING_STATE(sData, ISLAND_BACKUP_HELI_LAUNCHING_STATE_WAIT)
		BREAK
		
		CASE ISLAND_BACKUP_HELI_LAUNCHING_STATE_WAIT
			IF MPGlobalsAmbience.bLaunchIslandBackupHeli
				NET_PRINT("[ISLAND_BACKUP_HELI][LAUNCHING] - Backup Heli Launching - MPGlobalsAmbience.bLaunchBackUpHeli = TRUE.")NET_NL()
				SET_ISLAND_BACKUP_HELI_LAUNCHING_STATE(sData, ISLAND_BACKUP_HELI_LAUNCHING_STATE_LAUNCHING)
			ENDIF
		BREAK
		
		CASE ISLAND_BACKUP_HELI_LAUNCHING_STATE_LAUNCHING
			
			// Script launched, go to running state
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(ciScriptHash) > 0
				NET_PRINT("[ISLAND_BACKUP_HELI][LAUNCHING] - Backup Heli Launching - script AM_ISLAND_ISLAND_BACKUP_HELI now running.")NET_NL()
				SET_ISLAND_BACKUP_HELI_LAUNCHING_STATE(sData, ISLAND_BACKUP_HELI_LAUNCHING_STATE_RUNNING)
				EXIT
			ENDIF
			
			// Maintain launching script
			MP_MISSION_DATA sLaunchdata
			sLaunchdata.mdID.idMission = eAM_ISLAND_BACKUP_HELI
			sLaunchdata.iInstanceId = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(PLAYER_ID()))
				
			IF NET_LOAD_AND_LAUNCH_SCRIPT_WITH_ARGS(sLaunchdata)
				NET_PRINT("[ISLAND_BACKUP_HELI][LAUNCHING] - Backup Heli Launching - script AM_ISLAND_ISLAND_BACKUP_HELI now running.")NET_NL()
				SET_ISLAND_BACKUP_HELI_LAUNCHING_STATE(sData, ISLAND_BACKUP_HELI_LAUNCHING_STATE_RUNNING)
				_ISLAND_BACKUP_HELI__LAUNCH_SCRIPT_FOR_GANG_MEMBERS(sLaunchdata.iInstanceId)
			#IF IS_DEBUG_BUILD
			ELSE
				NET_PRINT("[ISLAND_BACKUP_HELI][LAUNCHING] - Backup Heli Launching - launching script AM_ISLAND_ISLAND_BACKUP_HELI.")NET_NL()
			#ENDIF
			ENDIF
		
		BREAK
		
		CASE ISLAND_BACKUP_HELI_LAUNCHING_STATE_RUNNING
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(ciScriptHash) <= 0
				NET_PRINT("[ISLAND_BACKUP_HELI][LAUNCHING] - Backup Heli Launching - script AM_ISLAND_ISLAND_BACKUP_HELI has terminated.")NET_NL()
				_HEIST_ISLAND__GLOBAL_SET_LAUNCH_ISLAND_BACKUP_HELI(FALSE)
				SET_ISLAND_BACKUP_HELI_LAUNCHING_STATE(sData, ISLAND_BACKUP_HELI_LAUNCHING_STATE_INIT)
			ENDIF
		BREAK
			
	ENDSWITCH
	
ENDPROC
#ENDIF // FEATURE_HEIST_ISLAND
