
USING "globals.sch"
USING "rage_builtins.sch"

CONST_INT ENABLE_CONTENT	FEATURE_HEIST_ISLAND

#IF ENABLE_CONTENT
USING "variations/fm_content_island_heist_scoping_intro.sch"
USING "variations/fm_content_island_heist_scoping_smuggler.sch"
USING "variations/fm_content_island_heist_scoping_party.sch"
USING "variations/fm_content_island_heist_scoping_return.sch"
USING "variations/fm_content_island_heist_scoping_island.sch"
USING "variations/fm_content_island_heist_stealth_helicopter_1.sch"
USING "variations/fm_content_island_heist_stealth_helicopter_2.sch"
USING "variations/fm_content_island_heist_gunboat_1.sch"
USING "variations/fm_content_island_heist_gunboat_2.sch"
USING "variations/fm_content_island_heist_smuggler_boat.sch"
USING "variations/fm_content_island_heist_smuggler_plane.sch"
USING "variations/fm_content_island_heist_military_plane.sch"
USING "variations/fm_content_island_heist_pilot_1.sch"
USING "variations/fm_content_island_heist_pilot_2.sch"
USING "variations/fm_content_island_heist_demolition_charges.sch"
USING "variations/fm_content_island_heist_acetylene_torch.sch"
USING "variations/fm_content_island_heist_plasma_cutter.sch"
USING "variations/fm_content_island_heist_safe_codes.sch"
USING "variations/fm_content_island_heist_fingerprint_cracker.sch"
USING "variations/fm_content_island_heist_sonar_jammer.sch"
USING "variations/fm_content_island_heist_weapons_1.sch"
USING "variations/fm_content_island_heist_weapons_2.sch"
USING "variations/fm_content_island_heist_disrupt_weapons.sch"
USING "variations/fm_content_island_heist_disrupt_armour.sch"
USING "variations/fm_content_island_heist_disrupt_backup_response.sch"

/// PURPOSE: Use to set up the logic pointers for your mission
///    
PROC SETUP_MISSION_LOGIC()
	SWITCH GET_VARIATION()
		CASE IHV_SCOPING_INTRO				SCOPING_INTRO_SETUP_LOGIC()				BREAK
		CASE IHV_SCOPING_SMUGGLER			SCOPING_SMUGGLER_SETUP_LOGIC()			BREAK
		CASE IHV_SCOPING_PARTY				SCOPING_PARTY_SETUP_LOGIC()				BREAK
		CASE IHV_SCOPING_RETURN				SCOPING_RETURN_SETUP_LOGIC()			BREAK
		CASE IHV_SCOPING_ISLAND				SCOPING_ISLAND_SETUP_LOGIC()			BREAK
		CASE IHV_DISRUPT_ARMOUR				DISARM_SETUP_LOGIC()					BREAK
		CASE IHV_DISRUPT_WEAPONS			DISWPN_SETUP_LOGIC()					BREAK
		CASE IHV_STEALTH_HELICOPTER_2		STHELTWO_SETUP_LOGIC()					BREAK
		CASE IHV_PLASMA_CUTTER				PLASMA_SETUP_LOGIC()					BREAK
		CASE IHV_DISRUPT_BACKUP_RESPONSE	BACKUP_SETUP_LOGIC()					BREAK
		CASE IHV_SMUGGLER_PLANE				SMP_SETUP_LOGIC()						BREAK
		CASE IHV_PILOT_2					PILOT_2_SETUP_LOGIC()					BREAK
		CASE IHV_SMUGGLER_BOAT				SMUGBOAT_SETUP_LOGIC()					BREAK
		CASE IHV_ACETYLENE_TORCH			ACETORCH_SETUP_LOGIC()					BREAK
		CASE IHV_SAFE_CODES					SAFECODE_SETUP_LOGIC()					BREAK
		CASE IHV_WEAPONS_1					WEAPONS_1_SETUP_LOGIC()					BREAK
		CASE IHV_DEMOLITION_CHARGES			DC_SETUP_LOGIC()						BREAK
		CASE IHV_FINGERPRINT_CRACKER		FINGERPRINT_CRACKER_SETUP_LOGIC()		BREAK
		CASE IHV_SONAR_JAMMER				SONJAM_SETUP_LOGIC()					BREAK
		CASE IHV_STEALTH_HELICOPTER_1		STHELONE_SETUP_LOGIC()					BREAK
		CASE IHV_WEAPONS_2					WEAP2_SETUP_LOGIC()						BREAK
		CASE IHV_GUNBOAT_1					GUNBOAT_1_SETUP_LOGIC()					BREAK
		CASE IHV_MILITARY_PLANE				MP_SETUP_LOGIC()						BREAK
		CASE IHV_PILOT_1					PILOT_1_SETUP_LOGIC()					BREAK
		CASE IHV_GUNBOAT_2					GBOAT2_SETUP_LOGIC()					BREAK
	ENDSWITCH
ENDPROC
#ENDIF
