USING "globals.sch"
USING "rage_builtins.sch"
USING "dj_missions/fm_content_island_dj_core.sch"

CONST_INT PT_EQUIPMENT_DELAY_HELP_TEXT_STOP_THIEF	3000
CONST_INT PT_EQUIPMENT_HEALTH_THIEF					400

CONST_INT PT_EQUIPMENT_MUSIC_ID_SUSPENSE_START		0
CONST_INT PT_EQUIPMENT_MUSIC_ID_MED_INTENSITY		1
CONST_INT PT_EQUIPMENT_MUSIC_ID_DELIVERING			2

CONST_INT PT_EQUIPMENT_GO_TO_PIRATE_MUSIC_ID		0

CONST_INT PT_EQUIPMENT_HELP_ID_GO_TO_PIRATE_MUSIC	0
CONST_INT PT_EQUIPMENT_HELP_ID_STOP_THIEF			1
CONST_INT PT_EQUIPMENT_MISSION_ENTITY_ID_EQUIPMENT	0
CONST_INT PT_EQUIPMENT_GROUP_ID_GOONS				1
CONST_INT PT_EQUIPMENT_PED_ID_GUNMAN				0
CONST_INT PT_EQUIPMENT_PED_ID_HOSTAGE				1
CONST_INT PT_EQUIPMENT_PED_ID_THIEF					2
CONST_INT PT_EQUIPMENT_PED_ID_DEAD					3
CONST_INT PT_EQUIPMENT_VEH_ID_VAN					0
CONST_INT PT_EQUIPMENT_VEH_ID_ROBBERS				1
CONST_INT PT_EQUIPMENT_TXTMSG_ID_ACCEPT_MISSION		0

SCRIPT_TIMER stPtEquipmentHelpTimer

ENUM PT_EQUIPMENT_MODE_STATE
	ePTEQUIPMENTSTATE_GO_TO_PIRATE_MUSIC = 0,
	ePTEQUIPMENTSTATE_TAKE_OUT_ROBBERS = 1,
	ePTEQUIPMENTSTATE_COLLECT = 2,
	ePTEQUIPMENTSTATE_DELIVER = 3,
	ePTEQUIPMENTSTATE_END = 4
ENDENUM

ENUM PT_EQUIPMENT_CLIENT_MODE_STATE
	ePTEQUIPMENTSTATECLIENT_GO_TO_PIRATE_MUSIC = 0,
	ePTEQUIPMENTSTATECLIENT_TAKE_OUT_ROBBERS = 1,
	ePTEQUIPMENTSTATECLIENT_COLLECT = 2,
	ePTEQUIPMENTSTATECLIENT_DELIVER = 3,
	ePTEQUIPMENTSTATECLIENT_HELP_DELIVER = 4,
	ePTEQUIPMENTSTATECLIENT_RECOVER = 5,
	ePTEQUIPMENTSTATECLIENT_END = 6
ENDENUM

ENUM PT_EQUIPMENT_PED_TASK_GUNMAN
	ePTEQUIPMENT_PEDTASK_GUNMAN_DO_SCENARIOS = 0,
	ePTEQUIPMENT_PEDTASK_GUNMAN_AIM = 1,
	ePTEQUIPMENT_PEDTASK_GUNMAN_ATTACK = 2
ENDENUM

ENUM PT_EQUIPMENT_PED_TASK_HOSTAGE
	ePTEQUIPMENT_PEDTASK_HOSTAGE_ANIM = 0
ENDENUM

ENUM PT_EQUIPMENT_PED_TASK_THIEF
	ePTEQUIPMENT_PEDTASK_THIEF_DO_SCENARIOS = 0,
	ePTEQUIPMENT_PEDTASK_THIEF_FOLLOW_ROUTE = 1,
	ePTEQUIPMENT_PEDTASK_THIEF_ATTACK = 2
ENDENUM

ENUM PT_EQUIPMENT_PED_TASK_GOONS
	ePTEQUIPMENT_PEDTASK_GOONS_DO_SCENARIOS = 0,
	ePTEQUIPMENT_PEDTASK_GOONS_ATTACK = 1
ENDENUM

FUNC PT_EQUIPMENT_MODE_STATE PT_EQUIPMENT_GET_MODE_STATE()
    RETURN INT_TO_ENUM(PT_EQUIPMENT_MODE_STATE, GET_MODE_STATE_ID())
ENDFUNC

FUNC PT_EQUIPMENT_CLIENT_MODE_STATE PT_EQUIPMENT_GET_CLIENT_MODE_STATE()
    RETURN INT_TO_ENUM(PT_EQUIPMENT_CLIENT_MODE_STATE, GET_CLIENT_MODE_STATE_ID())
ENDFUNC

/////////////////////
///    SETUP DATA
/////////////////////

PROC PT_EQUIPMENT_SETUP_DATA()
	data.MissionEntity.iCount = 1
	data.MissionEntity.MissionEntities[PT_EQUIPMENT_MISSION_ENTITY_ID_EQUIPMENT].model = INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_h4_Michael_Backpack"))
	
	data.ModeTimer.iTimeLimit = 30
	data.ModeTimer.eDisplay = eMODETIMERDISPLAYTYPE_NEAR_END
	
	SET_VEHICLE_DATA_BIT(PT_EQUIPMENT_VEH_ID_VAN, eVEHICLEDATABITSET_CHECK_NEAR_VEHICLE)
ENDPROC

PROC PT_EQUIPMENT_PED_ATTRIBUTES(INT iPed, PED_INDEX pedID, BOOL bAmbush)
	UNUSED_PARAMETER(bAmbush)
	
	SWITCH iPed
		CASE PT_EQUIPMENT_PED_ID_GUNMAN
			SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_HEAD, 0, 2)
			SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_TORSO, 1, 0)
			SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_LEG, 0, 1)
		BREAK
		CASE PT_EQUIPMENT_PED_ID_HOSTAGE
			SET_PED_CAN_BE_TARGETTED(pedID, FALSE)
			SET_PED_TREATED_AS_FRIENDLY(pedID, TRUE)
		BREAK
		CASE PT_EQUIPMENT_PED_ID_THIEF
			SET_ENTITY_MAX_HEALTH(pedID, PT_EQUIPMENT_HEALTH_THIEF)
			SET_ENTITY_HEALTH(pedID, PT_EQUIPMENT_HEALTH_THIEF)
			
			SET_PED_CAN_BE_TARGETTED(pedID, FALSE)
			
			SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_SPECIAL, 1, 0)	// Backpack
		BREAK
		CASE PT_EQUIPMENT_PED_ID_DEAD
			SET_ENTITY_HEALTH(pedID, 0)
		BREAK
	ENDSWITCH
ENDPROC

//////////////////
///    DROPOFF
//////////////////

FUNC VECTOR PT_EQUIPMENT_OVERRIDE_DROPOFF_COORDS()
	RETURN FREEMODE_DELIVERY_DROPOFF_GET_ON_FOOT_COORDS(GET_DROPOFF())
ENDFUNC

////////////////////
///    FOCUS CAM
////////////////////

FUNC BOOL PT_EQUIPMENT_FOCUS_CAM_ENABLE(INT iCam)
	UNUSED_PARAMETER(iCam)
	RETURN PT_EQUIPMENT_GET_MODE_STATE() = ePTEQUIPMENTSTATE_TAKE_OUT_ROBBERS
ENDFUNC

FUNC BOOL PT_EQUIPMENT_FOCUS_CAM_SHOW_HINT(INT iCam)
	UNUSED_PARAMETER(iCam)
	RETURN TRUE
ENDFUNC

FUNC STRING PT_EQUIPMENT_FOCUS_CAM_PROMPT(INT iCam)
	UNUSED_PARAMETER(iCam)
	RETURN "IDJ_PT_FOCUS"
ENDFUNC

///////////////////
///   HELP TEXT
///////////////////

FUNC BOOL PT_EQUIPMENT_HELP_TRIGGERS(INT iHelp)
	SWITCH iHelp
		CASE PT_EQUIPMENT_HELP_ID_GO_TO_PIRATE_MUSIC
			RETURN PT_EQUIPMENT_GET_MODE_STATE() = ePTEQUIPMENTSTATE_GO_TO_PIRATE_MUSIC
		BREAK
		CASE PT_EQUIPMENT_HELP_ID_STOP_THIEF
			RETURN PT_EQUIPMENT_GET_MODE_STATE() = ePTEQUIPMENTSTATE_TAKE_OUT_ROBBERS AND HAS_NET_TIMER_EXPIRED(stPtEquipmentHelpTimer, PT_EQUIPMENT_DELAY_HELP_TEXT_STOP_THIEF)
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC STRING PT_EQUIPMENT_HELP_TEXT(INT iHelp)
	SWITCH iHelp
		CASE PT_EQUIPMENT_HELP_ID_GO_TO_PIRATE_MUSIC	RETURN "IDJ_PT_HLP_DJEQ"
		CASE PT_EQUIPMENT_HELP_ID_STOP_THIEF			RETURN "IDJ_PT_HLP_THIE"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

//////////////////////////
///   TEXT MESSAGES
//////////////////////////

FUNC enumCharacterList PT_EQUIPMENT_TEXT_MESSAGES_CHARACTER(INT iText)
	UNUSED_PARAMETER(iText)
	RETURN CHAR_CASINO_TOMCONNORS
ENDFUNC

FUNC BOOL PT_EQUIPMENT_TEXT_MESSAGES_SHOULD_SEND(INT iText)
	SWITCH iText
		CASE PT_EQUIPMENT_TXTMSG_ID_ACCEPT_MISSION		RETURN PT_EQUIPMENT_GET_MODE_STATE() = ePTEQUIPMENTSTATE_GO_TO_PIRATE_MUSIC AND HAS_NET_TIMER_EXPIRED(DJCoreTextMessageAcceptTomDelayTimer, DJ_CORE_TEXT_MESSAGE_ACCEPT_TOM_DELAY_TIMER)
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC STRING PT_EQUIPMENT_TEXT_MESSAGES_LABEL(INT iText)
	SWITCH iText
		CASE PT_EQUIPMENT_TXTMSG_ID_ACCEPT_MISSION		RETURN "IDJ_TX_P_PTEQ_A"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

/////////////////////////
///    MISSION_ENTITY
/////////////////////////

FUNC BOOL PT_EQUIPMENT_MISSION_ENTITY_SHOULD_SPAWN(INT iMissionEntity)
	UNUSED_PARAMETER(iMissionEntity)
	RETURN serverBD.sPed[PT_EQUIPMENT_PED_ID_THIEF].eState = ePEDSTATE_DEAD
ENDFUNC

FUNC STRING PT_EQUIPMENT_MISSION_ENTITY_BLIP_NAME(INT iEntity)
	UNUSED_PARAMETER(iEntity)
	RETURN "IDJ_PT_EQ_BLIP"
ENDFUNC

FUNC VECTOR PT_EQUIPMENT_MISSION_ENTITY_DYNAMIC_SPAWN_COORDS(INT iMissionEntity)
	UNUSED_PARAMETER(iMissionEntity)
	
	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPed[PT_EQUIPMENT_PED_ID_THIEF].netId)
		RETURN <<0, 0, 0>>
	ENDIF
	
	RETURN GET_ENTITY_COORDS(NET_TO_PED(serverBD.sPed[PT_EQUIPMENT_PED_ID_THIEF].netId), FALSE)
ENDFUNC

FUNC BLIP_SPRITE PT_EQUIPMENT_MISSION_ENTITY_BLIP_SPRITE(INT iEntity)
	UNUSED_PARAMETER(iEntity)
	RETURN RADAR_TRACE_RUCKSACK
ENDFUNC

FUNC HUD_COLOURS PT_EQUIPMENT_MISSION_ENTITY_BLIP_COLOUR(INT iEntity)
	UNUSED_PARAMETER(iEntity)
	RETURN HUD_COLOUR_GREEN
ENDFUNC

PROC PT_EQUIPMENT_MISSION_ENTITY_ON_COLLECT(INT iMissionEntity)
	UNUSED_PARAMETER(iMissionEntity)
	
	MISSION_TICKER_DATA sTickerData
	sTickerData.iInt1 = ENUM_TO_INT(eDJ_TICKER_DJ_EQUIPMENT_COLLECTED)
	SEND_FM_CONTENT_TICKER_EVENT_WITH_DATA(eFMCTICKER_CUSTOM, sTickerData)
ENDPROC

////////////////
///    MUSIC
////////////////

FUNC STRING PT_EQUIPMENT_MUSIC_EVENT_STRING(INT iMusicEvent)
	SWITCH iMusicEvent
		CASE PT_EQUIPMENT_MUSIC_ID_SUSPENSE_START	RETURN "HEI4_SUSPENSE_START"
		CASE PT_EQUIPMENT_MUSIC_ID_MED_INTENSITY	RETURN "HEI4_MED_INTENSITY"
		CASE PT_EQUIPMENT_MUSIC_ID_DELIVERING		RETURN "HEI4_DELIVERING"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC INT PT_EQUIPMENT_MUSIC_EVENT(INT iCurrentEvent)
	SWITCH iCurrentEvent
		CASE -1
			IF PT_EQUIPMENT_GET_CLIENT_MODE_STATE() >= ePTEQUIPMENTSTATECLIENT_GO_TO_PIRATE_MUSIC
				RETURN PT_EQUIPMENT_MUSIC_ID_SUSPENSE_START
			ENDIF
		BREAK
		
		CASE PT_EQUIPMENT_MUSIC_ID_SUSPENSE_START
			IF HAS_ANY_PED_GROUP_BEEN_TRIGGERED()
				RETURN PT_EQUIPMENT_MUSIC_ID_MED_INTENSITY
			ENDIF
		BREAK
		
		CASE PT_EQUIPMENT_MUSIC_ID_MED_INTENSITY
			IF PT_EQUIPMENT_GET_CLIENT_MODE_STATE() >= ePTEQUIPMENTSTATECLIENT_DELIVER
				RETURN PT_EQUIPMENT_MUSIC_ID_DELIVERING
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN -1
ENDFUNC

///////////////
///    PEDS
///////////////

FUNC ENTITY_INDEX PT_EQUIPMENT_PED_AIM_AT_ENTITY(INT iPed)
	UNUSED_PARAMETER(iPed)
	
	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPed[PT_EQUIPMENT_PED_ID_HOSTAGE].netId)
		RETURN NULL
	ENDIF
	
	RETURN NET_TO_ENT(serverBD.sPed[PT_EQUIPMENT_PED_ID_HOSTAGE].netId)
ENDFUNC

FUNC FLOAT PT_EQUIPMENT_GO_TO_COORD_ANY_MEANS_EXTRA_DISTANCE_TO_PREFER_VEHICLE(INT iPed)
	UNUSED_PARAMETER(iPed)
	RETURN 1000.0
ENDFUNC

FUNC STRING PT_EQUIPMENT_PED_BLIP_NAME(INT iEntity)
	UNUSED_PARAMETER(iEntity)
	RETURN "IDJ_PT_THIEF_B"
ENDFUNC

PROC PT_EQUIPMENT_PED_ON_DAMAGED(INT iEntity, STRUCT_ENTITY_DAMAGE_EVENT sEvent, PLAYER_INDEX damager)
	UNUSED_PARAMETER(iEntity)
	UNUSED_PARAMETER(damager)
	
	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.sPed[PT_EQUIPMENT_PED_ID_THIEF].netId)
		EXIT
	ENDIF
	
	IF sEvent.VictimDestroyed
	AND sEvent.VictimIndex = NET_TO_ENT(serverbd.sPed[PT_EQUIPMENT_PED_ID_THIEF].netId)
		// Remove backpack
		SET_PED_COMPONENT_VARIATION(NET_TO_PED(serverbd.sPed[PT_EQUIPMENT_PED_ID_THIEF].netId), PED_COMP_SPECIAL, 0, 0)
	ENDIF
ENDPROC

FUNC INT PT_EQUIPMENT_PED_NUM_COORDS(INT iPed)
	UNUSED_PARAMETER(iPed)
	RETURN 15
ENDFUNC

FUNC VECTOR PT_EQUIPMENT_PED_GO_TO_COORDS(INT iPed, INT iCheckpointStage)
	UNUSED_PARAMETER(iPed)
	
	SWITCH iCheckpointStage
		CASE 0	RETURN <<-591.0623, -1166.5387, 21.1782>>
		CASE 1	RETURN <<-620.3408, -1165.1166, 21.1782>>
		CASE 2	RETURN <<-621.4808, -1101.9069, 21.1781>>
		CASE 3	RETURN <<-614.5312, -1073.4216, 21.3785>>
		CASE 4	RETURN <<-616.4337, -1034.0421, 20.7875>>
		CASE 5	RETURN <<-623.31, -996.9479, 20.0253>>
		CASE 6	RETURN <<-668.8128, -1031.3969, 16.6584>>
		CASE 7	RETURN <<-713.4483, -1060.3820, 13.0133>>
		CASE 8	RETURN <<-738.2045, -1016.5109, 13.9066>>
		CASE 9	RETURN <<-760.2473, -978.1690, 14.6440>>
		CASE 10	RETURN <<-726.4073, -968.7161, 17.1713>>
		CASE 11	RETURN <<-695.4406, -947.1296, 18.8838>>
		CASE 12	RETURN <<-695.2433, -923.0771, 22.0752>>
		CASE 13	RETURN <<-695.4212, -910.3208, 22.0774>>
		CASE 14	RETURN <<-692.1169, -908.2917, 22.6719>>
		
	ENDSWITCH
	
	RETURN <<0, 0, 0>>
ENDFUNC

////////////////////////
///    TRIGGER AREAS
////////////////////////

FUNC BOOL PT_EQUIPMENT_TRIGGER_AREA_ACTIVE(INT iArea, eTRIGGER_AREA_DATA_BITSET eType, BOOL bInAirVehicle)
	UNUSED_PARAMETER(iArea)
	UNUSED_PARAMETER(eType)
	UNUSED_PARAMETER(bInAirVehicle)
	
	RETURN PT_EQUIPMENT_GET_MODE_STATE() != ePTEQUIPMENTSTATE_GO_TO_PIRATE_MUSIC
ENDFUNC

//////////////////
///    VEHICLE
//////////////////

FUNC BOOL PT_EQUIPMENT_VEHICLE_MODS(INT iVehicle, VEHICLE_SETUP_STRUCT_MP &sData)
	IF data.Vehicle.Vehicles[iVehicle].model = PONY
		sData.VehicleSetup.eModel = PONY
		sData.VehicleSetup.tlPlateText = "43GSP782"
		sData.VehicleSetup.iPlateIndex = 3
		sData.VehicleSetup.iColour1 = 16
		sData.VehicleSetup.iColour2 = 16
		sData.VehicleSetup.iColourExtra1 = 16
		sData.VehicleSetup.iColourExtra2 = 156
		sData.iColour5 = 1
		sData.iColour6 = 132
		sData.iLivery2 = 0
		sData.VehicleSetup.iLivery = 0
		sData.VehicleSetup.iWheelType = 1
		sData.VehicleSetup.iTyreR = 255
		sData.VehicleSetup.iTyreG = 255
		sData.VehicleSetup.iTyreB = 255
		sData.VehicleSetup.iNeonR = 255
		sData.VehicleSetup.iNeonB = 255
		SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_1)
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC FLOAT PT_EQUIPMENT_VEHICLE_NEAR_RANGE(INT iVehicle)
	UNUSED_PARAMETER(iVehicle)
	RETURN 50.0
ENDFUNC

PROC PT_EQUIPMENT_VEHICLE_ATTRIBUTES(INT iVehicle, VEHICLE_INDEX vehId)
	SWITCH iVehicle
		CASE PT_EQUIPMENT_VEH_ID_VAN
			SET_VEHICLE_DOOR_OPEN(vehId, SC_DOOR_FRONT_RIGHT, TRUE)
		BREAK
		CASE PT_EQUIPMENT_VEH_ID_ROBBERS
			SET_VEHICLE_DOOR_OPEN(vehId, SC_DOOR_FRONT_LEFT, TRUE)
			SET_VEHICLE_DOOR_OPEN(vehId, SC_DOOR_FRONT_RIGHT, TRUE)
		BREAK
	ENDSWITCH
ENDPROC

/////////////////////////
///    OBJECTIVE TEXT
/////////////////////////

PROC PT_EQUIPMENT_OBJECTIVE_TEXT_GO_TO()
	Print_Objective_Text_With_Coloured_Text_Label("ILH_GOTO_THE", "IDJ_LOC_PIRATE", HUD_COLOUR_YELLOW)
ENDPROC

PROC PT_EQUIPMENT_OBJECTIVE_TEXT_TAKE_OUT_TARGETS()
	Print_Objective_Text_With_Coloured_Text_Label("ILH_STOP_THE", "IDJ_THIEVES", HUD_COLOUR_RED)
ENDPROC

PROC PT_EQUIPMENT_OBJECTIVE_TEXT_COLLECT()
	Print_Objective_Text_With_Coloured_Text_Label("IDJ_RECOVER", "IDJ_PT_EQUIP", HUD_COLOUR_GREEN)	
ENDPROC

PROC PT_EQUIPMENT_OBJECTIVE_TEXT_DELIVER()
	Print_Objective_Text_With_Two_Strings_Coloured("IDJ_DELIVER_THE", "IDJ_PT_EQUIP", "IDJ_LOC_MUSIC", HUD_COLOUR_WHITE, HUD_COLOUR_YELLOW)
ENDPROC

PROC PT_EQUIPMENT_OBJECTIVE_TEXT_HELP_DELIVER()
	Print_Objective_Text_With_Two_Strings_Coloured("IDJ_HLPDELV_T", "IDJ_PT_EQUIP", "IDJ_LOC_MUSIC", HUD_COLOUR_WHITE, HUD_COLOUR_YELLOW)
ENDPROC

//////////////////
///    HUD
//////////////////

FUNC BOOL PT_EQUIPMENT_HUD_SHOULD_DELAY_UI()
	IF PT_EQUIPMENT_GET_MODE_STATE() = ePTEQUIPMENTSTATE_GO_TO_PIRATE_MUSIC
		RETURN NOT HAS_NET_TIMER_EXPIRED(DJCoreTextMessageAcceptTomDelayTimer, DJ_CORE_TEXT_MESSAGE_ACCEPT_SHOULD_DELAY_UI_TIMER)
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC STRING PT_EQUIPMENT_HUD_TICKER_CUSTOM_LOCAL(MISSION_TICKER_DATA &sTickerData)
	IF sTickerData.iInt1 != -1
		SWITCH INT_TO_ENUM(DJ_TICKER_EVENTS, sTickerData.iInt1)
			CASE eDJ_TICKER_DJ_EQUIPMENT_COLLECTED	RETURN "IDJ_T_PT_EQ_EQa"
		ENDSWITCH
	ENDIF
	
	RETURN ""
ENDFUNC

FUNC STRING PT_EQUIPMENT_HUD_TICKER_CUSTOM_REMOTE(MISSION_TICKER_DATA &sTickerData)
	IF sTickerData.iInt1 != -1
		SWITCH INT_TO_ENUM(DJ_TICKER_EVENTS, sTickerData.iInt1)
			CASE eDJ_TICKER_DJ_EQUIPMENT_COLLECTED	RETURN "IDJ_T_PT_EQ_EQb"
		ENDSWITCH
	ENDIF
	
	RETURN ""
ENDFUNC

/////////////////////////
///    PED CONDITIONS
/////////////////////////

FUNC BOOL PT_EQUIPMENT_PED_TRANSITION_FROM_SCENARIO(INT iPed, PED_INDEX pedId)
	UNUSED_PARAMETER(iPed)
	UNUSED_PARAMETER(pedId)
	RETURN IS_GENERIC_SERVER_BIT_SET(eGENERICSERVERBITSET_REACHED_GOTO_LOCATION)
ENDFUNC

FUNC BOOL PT_EQUIPMENT_PED_TRANSITION_TO_ATTACK(INT iPed, PED_INDEX pedId)
	UNUSED_PARAMETER(pedId)
	RETURN HAS_PED_BEEN_TRIGGERED(iPed)
ENDFUNC

FUNC BOOL PT_EQUIPMENT_PED_TRANSITION_FROM_FOLLOW_ROUTE(INT iPed, PED_INDEX pedId)
	UNUSED_PARAMETER(pedId)
	RETURN IS_PED_BIT_SET(iPed, ePEDBITSET_I_HAVE_REACHED_FINAL_CHECKPOINT)
	AND HAS_PED_GROUP_BEEN_TRIGGERED(PT_EQUIPMENT_GROUP_ID_GOONS)
ENDFUNC

////////////////////
///    PED TASKS
////////////////////

PROC PT_EQUIPMENT_PED_TASK_SETUP(INT iBehaviour)
	SWITCH iBehaviour
		CASE 0
			ADD_PED_TASK(iBehaviour, ePTEQUIPMENT_PEDTASK_GOONS_DO_SCENARIOS, ePEDTASK_DO_SCENARIOS)
				ADD_PED_TASK_TRANSITION(iBehaviour, ePTEQUIPMENT_PEDTASK_GOONS_DO_SCENARIOS, ePTEQUIPMENT_PEDTASK_GOONS_ATTACK, &PT_EQUIPMENT_PED_TRANSITION_TO_ATTACK)
			
			ADD_PED_TASK(iBehaviour, ePTEQUIPMENT_PEDTASK_GOONS_ATTACK, ePEDTASK_ATTACK_HATED_TARGETS)
		BREAK
		CASE 1
			ADD_PED_TASK(iBehaviour, ePTEQUIPMENT_PEDTASK_GUNMAN_DO_SCENARIOS, ePEDTASK_DO_SCENARIOS)
				ADD_PED_TASK_TRANSITION(iBehaviour, ePTEQUIPMENT_PEDTASK_GUNMAN_DO_SCENARIOS, ePTEQUIPMENT_PEDTASK_GUNMAN_AIM, &PT_EQUIPMENT_PED_TRANSITION_FROM_SCENARIO)
			
			ADD_PED_TASK(iBehaviour, ePTEQUIPMENT_PEDTASK_GUNMAN_AIM, ePEDTASK_AIM_AT_ENTITY)
				ADD_PED_TASK_TRANSITION(iBehaviour, ePTEQUIPMENT_PEDTASK_GUNMAN_AIM, ePTEQUIPMENT_PEDTASK_GUNMAN_ATTACK, &PT_EQUIPMENT_PED_TRANSITION_TO_ATTACK)
			
			ADD_PED_TASK(iBehaviour, ePTEQUIPMENT_PEDTASK_GUNMAN_ATTACK, ePEDTASK_ATTACK_HATED_TARGETS)
		BREAK
		
		CASE 2
			ADD_PED_TASK(iBehaviour, ePTEQUIPMENT_PEDTASK_HOSTAGE_ANIM, ePEDTASK_PLAY_ANIM)
		BREAK
		
		CASE 3
			ADD_PED_TASK(iBehaviour, ePTEQUIPMENT_PEDTASK_THIEF_DO_SCENARIOS, ePEDTASK_DO_SCENARIOS)
				ADD_PED_TASK_TRANSITION(iBehaviour, ePTEQUIPMENT_PEDTASK_THIEF_DO_SCENARIOS, ePTEQUIPMENT_PEDTASK_THIEF_FOLLOW_ROUTE, &PT_EQUIPMENT_PED_TRANSITION_FROM_SCENARIO)
			
			ADD_PED_TASK(iBehaviour, ePTEQUIPMENT_PEDTASK_THIEF_FOLLOW_ROUTE, ePEDTASK_GO_TO_COORD_ANY_MEANS)
				ADD_PED_TASK_TRANSITION(iBehaviour, ePTEQUIPMENT_PEDTASK_THIEF_FOLLOW_ROUTE, ePTEQUIPMENT_PEDTASK_THIEF_ATTACK, &PT_EQUIPMENT_PED_TRANSITION_FROM_FOLLOW_ROUTE)
			
			ADD_PED_TASK(iBehaviour, ePTEQUIPMENT_PEDTASK_THIEF_ATTACK, ePEDTASK_ATTACK_HATED_TARGETS)
		BREAK
	ENDSWITCH
ENDPROC

FUNC INT PT_EQUIPMENT_GET_PED_BEHAVIOUR(INT iPed)

	SWITCH iPed
		CASE PT_EQUIPMENT_PED_ID_GUNMAN		RETURN 1
		CASE PT_EQUIPMENT_PED_ID_HOSTAGE	RETURN 2
		CASE PT_EQUIPMENT_PED_ID_THIEF		RETURN 3
	ENDSWITCH
	
	RETURN 0

ENDFUNC

///////////////////
///    MAINTAIN
///////////////////

PROC PT_EQUIPMENT_MAINTAIN_CLIENT()
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[PT_EQUIPMENT_VEH_ID_VAN].netId)
		VEHICLE_INDEX vehId = NET_TO_VEH(serverBD.sVehicle[PT_EQUIPMENT_VEH_ID_VAN].netId)
		IF IS_VEHICLE_BIT_SET(PT_EQUIPMENT_VEH_ID_VAN, eVEHICLEBITSET_PLAYER_IS_NEAR_ME)
		AND NETWORK_HAS_CONTROL_OF_ENTITY(vehId)
		AND IS_VEHICLE_SEAT_FREE(vehId, VS_DRIVER)
		AND NOT IS_ENTITY_DEAD(vehId)
			SET_VEHICLE_ENGINE_ON(vehId, TRUE, TRUE)
			SET_VEHICLE_RADIO_ENABLED(vehId, TRUE)
			SET_VEHICLE_RADIO_LOUD(vehId, TRUE)
			SET_VEH_FORCED_RADIO_THIS_FRAME(vehId)
			SET_VEH_RADIO_STATION(vehId, "RADIO_35_DLC_HEI4_MLR")
		ENDIF
	ENDIF
ENDPROC

//////////////////////////
///    MODE CONDITIONS
//////////////////////////

FUNC BOOL PT_EQUIPMENT_TRANSITION_TO_COLLECT()
	RETURN serverBD.sPed[PT_EQUIPMENT_PED_ID_THIEF].eState = ePEDSTATE_DEAD
ENDFUNC

//////////////////////
///    MODE STATES
//////////////////////

PROC PT_EQUIPMENT_SETUP_MODE_STATES()
	ADD_MODE_STATE(ePTEQUIPMENTSTATE_GO_TO_PIRATE_MUSIC, eMODESTATE_GO_TO_POINT)
		ADD_MODE_STATE_TRANSITION_DEFAULT(ePTEQUIPMENTSTATE_GO_TO_PIRATE_MUSIC, ePTEQUIPMENTSTATE_TAKE_OUT_ROBBERS)
	
	ADD_MODE_STATE(ePTEQUIPMENTSTATE_TAKE_OUT_ROBBERS, eMODESTATE_TAKE_OUT_TARGETS)
		ADD_MODE_STATE_TRANSITION_CUSTOM(ePTEQUIPMENTSTATE_TAKE_OUT_ROBBERS, ePTEQUIPMENTSTATE_COLLECT, &PT_EQUIPMENT_TRANSITION_TO_COLLECT)
	
	ADD_MODE_STATE(ePTEQUIPMENTSTATE_COLLECT, eMODESTATE_COLLECT_MISSION_ENTITY)
		ADD_MODE_STATE_TRANSITION_DEFAULT(ePTEQUIPMENTSTATE_COLLECT, ePTEQUIPMENTSTATE_DELIVER)
	
	ADD_MODE_STATE(ePTEQUIPMENTSTATE_DELIVER, eMODESTATE_DELIVER_MISSION_ENTITY)
		ADD_MODE_STATE_TRANSITION_DEFAULT(ePTEQUIPMENTSTATE_DELIVER, ePTEQUIPMENTSTATE_END)
	
	ADD_MODE_STATE(ePTEQUIPMENTSTATE_END, eMODESTATE_END)
ENDPROC	

PROC PT_EQUIPMENT_SETUP_CLIENT_MODE_STATES()
	ADD_CLIENT_MODE_STATE(ePTEQUIPMENTSTATECLIENT_GO_TO_PIRATE_MUSIC, eMODESTATE_GO_TO_POINT, &PT_EQUIPMENT_OBJECTIVE_TEXT_GO_TO)
		ADD_CLIENT_MODE_STATE_TRANSITION_LINKED(ePTEQUIPMENTSTATECLIENT_GO_TO_PIRATE_MUSIC, ePTEQUIPMENTSTATECLIENT_TAKE_OUT_ROBBERS, ePTEQUIPMENTSTATE_TAKE_OUT_ROBBERS)
	
	ADD_CLIENT_MODE_STATE(ePTEQUIPMENTSTATECLIENT_TAKE_OUT_ROBBERS, eMODESTATE_TAKE_OUT_TARGETS, &PT_EQUIPMENT_OBJECTIVE_TEXT_TAKE_OUT_TARGETS)
		ADD_CLIENT_MODE_STATE_TRANSITION_LINKED(ePTEQUIPMENTSTATECLIENT_TAKE_OUT_ROBBERS, ePTEQUIPMENTSTATECLIENT_COLLECT, ePTEQUIPMENTSTATE_COLLECT)
	
	ADD_CLIENT_MODE_STATES_DELIVERY(ePTEQUIPMENTSTATECLIENT_COLLECT, ePTEQUIPMENTSTATECLIENT_DELIVER, ePTEQUIPMENTSTATECLIENT_HELP_DELIVER, ePTEQUIPMENTSTATECLIENT_RECOVER, ePTEQUIPMENTSTATECLIENT_END,
		&PT_EQUIPMENT_OBJECTIVE_TEXT_COLLECT, &PT_EQUIPMENT_OBJECTIVE_TEXT_DELIVER, &PT_EQUIPMENT_OBJECTIVE_TEXT_HELP_DELIVER, &PT_EQUIPMENT_OBJECTIVE_TEXT_COLLECT)
	
	ADD_CLIENT_MODE_STATE(ePTEQUIPMENTSTATECLIENT_END, eMODESTATE_END, &EMPTY)
ENDPROC


///////////////////////
/// LOGIC OVERRIDES
///////////////////////

PROC PT_EQUIPMENT_SETUP_LOGIC()
	logic.Data.Setup = &PT_EQUIPMENT_SETUP_DATA
	
	logic.Maintain.Client = &PT_EQUIPMENT_MAINTAIN_CLIENT
	
	logic.StateMachine.SetupStates = &PT_EQUIPMENT_SETUP_MODE_STATES
	logic.StateMachine.SetupClientStates = &PT_EQUIPMENT_SETUP_CLIENT_MODE_STATES
	
	logic.Audio.MusicTrigger.EventString = &PT_EQUIPMENT_MUSIC_EVENT_STRING
	logic.Audio.MusicTrigger.FailString = &DJ_CORE_MUSIC_FAIL_STRING
	logic.Audio.MusicTrigger.StopString = &DJ_CORE_MUSIC_STOP_STRING
	logic.Audio.MusicTrigger.Event = &PT_EQUIPMENT_MUSIC_EVENT
	
	logic.Delivery.Dropoff.OverrideCoords = &PT_EQUIPMENT_OVERRIDE_DROPOFF_COORDS
	
	logic.FocusCam.Enable = &PT_EQUIPMENT_FOCUS_CAM_ENABLE
	logic.FocusCam.Prompt = &PT_EQUIPMENT_FOCUS_CAM_PROMPT
	logic.FocusCam.ShouldShowHint = &PT_EQUIPMENT_FOCUS_CAM_SHOW_HINT
	
	logic.HelpText.Text = &PT_EQUIPMENT_HELP_TEXT
	logic.HelpText.Trigger = &PT_EQUIPMENT_HELP_TRIGGERS
	
	logic.TextMessages.Character = &PT_EQUIPMENT_TEXT_MESSAGES_CHARACTER
	logic.TextMessages.ShouldSend = &PT_EQUIPMENT_TEXT_MESSAGES_SHOULD_SEND
	logic.TextMessages.Label = &PT_EQUIPMENT_TEXT_MESSAGES_LABEL
	
	logic.Hud.ShouldDelayUI = &PT_EQUIPMENT_HUD_SHOULD_DELAY_UI
	logic.Hud.Tickers.CustomStringLocal = &PT_EQUIPMENT_HUD_TICKER_CUSTOM_LOCAL
	logic.Hud.Tickers.CustomStringRemote = &PT_EQUIPMENT_HUD_TICKER_CUSTOM_REMOTE
	
	logic.MissionEntity.Blip.Colour = &PT_EQUIPMENT_MISSION_ENTITY_BLIP_COLOUR
	logic.MissionEntity.Blip.Name = &PT_EQUIPMENT_MISSION_ENTITY_BLIP_NAME
	logic.MissionEntity.Blip.Sprite = &PT_EQUIPMENT_MISSION_ENTITY_BLIP_SPRITE
	logic.MissionEntity.GetDynamicSpawnCoords = &PT_EQUIPMENT_MISSION_ENTITY_DYNAMIC_SPAWN_COORDS
	logic.MissionEntity.OnCollect = &PT_EQUIPMENT_MISSION_ENTITY_ON_COLLECT
	logic.MissionEntity.ShouldSpawn = &PT_EQUIPMENT_MISSION_ENTITY_SHOULD_SPAWN
	
	logic.Ped.Attributes = &PT_EQUIPMENT_PED_ATTRIBUTES
	logic.Ped.Blip.Name = &PT_EQUIPMENT_PED_BLIP_NAME
	logic.Ped.OnDamaged = &PT_EQUIPMENT_PED_ON_DAMAGED
	logic.Ped.Task.AimAtEntity.Entity = &PT_EQUIPMENT_PED_AIM_AT_ENTITY
	logic.Ped.Task.GoToCoordAnyMeans.NumCoords = &PT_EQUIPMENT_PED_NUM_COORDS
	logic.Ped.Task.GoToCoordAnyMeans.Coords = &PT_EQUIPMENT_PED_GO_TO_COORDS
	logic.Ped.Task.GoToCoordAnyMeans.ExtraDistanceToPreferVehicle = &PT_EQUIPMENT_GO_TO_COORD_ANY_MEANS_EXTRA_DISTANCE_TO_PREFER_VEHICLE
	logic.Ped.Behaviour.Setup = &PT_EQUIPMENT_PED_TASK_SETUP
	logic.Ped.Behaviour.Get = &PT_EQUIPMENT_GET_PED_BEHAVIOUR
	
	logic.TriggerArea.Enable = &PT_EQUIPMENT_TRIGGER_AREA_ACTIVE
	
	logic.Vehicle.Attributes = &PT_EQUIPMENT_VEHICLE_ATTRIBUTES
	logic.Vehicle.Mods = &PT_EQUIPMENT_VEHICLE_MODS
	logic.Vehicle.NearRange = &PT_EQUIPMENT_VEHICLE_NEAR_RANGE
	
	logic.Wanted.Level = &DJ_CORE_WANTED_MAX_LEVEL
ENDPROC
