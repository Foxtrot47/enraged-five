USING "globals.sch"
USING "rage_builtins.sch"
USING "heist_preps/fm_content_island_heist_core.sch"

//////////////////////
///    MISSION CONSTS
//////////////////////

/// DIALOGUES
CONST_INT SMP_DIALOGUE_ID_OPENING						0
CONST_INT SMP_DIALOGUE_ID_CLOSE_TO_PLANE				1
CONST_INT SMP_DIALOGUE_ID_COMBAT_SMUGGLERS				2
CONST_INT SMP_DIALOGUE_ID_ENTER_PLANE					3
CONST_INT SMP_DIALOGUE_ID_DELIVER_PLANE					4
CONST_INT SMP_DIALOGUE_ID_NEAR_END						5

CONST_INT SMP_SMUGGLERS_CAR								1

/// MUSIC CUES

CONST_INT SMP_MUSIC_ID_DELIVERING_START					0
CONST_INT SMP_MUSIC_ID_VEHICLE_ACTION					1
CONST_INT SMP_MUSIC_ID_MED_INTENSITY					2

//////////////////////
/// LOGIC OVERRIDES
//////////////////////

ENUM SMP_MODE_STATE
	eSMPSTATE_GO_TO = 0,
	eSMPSTATE_COLLECT = 1,
	eSMPSTATE_DELIVER = 2,
	eSMPSTATE_END = 3
ENDENUM

ENUM SMP_CLIENT_MODE_STATE
	eSMPSTATECLIENT_GO_TO = 0,
	eSMPSTATECLIENT_COLLECT = 1,
	eSMPSTATECLIENT_DELIVER = 2,
	eSMPSTATECLIENT_HELP_DELIVER = 3,
	eSMPSTATECLIENT_RECOVER = 4,
	eSMPSTATECLIENT_END = 5
ENDENUM

///////////////////////
///    OBJECTIVE TEXT
///////////////////////

PROC SMP_OBJECTIVE_TEXT_GO_TO()
	SWITCH GET_SUBVARIATION()
		CASE 	IHS_SMP_LOCATION_1		Print_Objective_Text_With_Coloured_Text_Label("FMCOT_GOTO", "FMC_LOC_SSA", HUD_COLOUR_YELLOW)		BREAK
		CASE	IHS_SMP_LOCATION_2		Print_Objective_Text_With_Coloured_Text_Label("FMCOT_GOTO", "FMC_LOC_MCKA", HUD_COLOUR_YELLOW)		BREAK
		CASE	IHS_SMP_LOCATION_3		Print_Objective_Text_With_Coloured_Text_Label("FMCOT_GOTO", "FMC_LOC_LZ", HUD_COLOUR_YELLOW)		BREAK
	ENDSWITCH 
ENDPROC

FUNC STRING SMP_DROPOFF_LOCATION_LABEL()
	RETURN "FMC_LOC_MIGH"
ENDFUNC

PROC SMP_SET_PLACEMENT_DATA()

	data.ModeTimer.iTimeLimit = 30
	data.ModeTimer.eDisplay = eMODETIMERDISPLAYTYPE_NEAR_END
	
	
	INT iPed
	REPEAT MAX_NUM_PEDS iPed
		IF data.Ped.Peds[iPed].iGroup = 0
			data.Ped.Peds[iPed].iMaxReactionDelay = 2000
		ENDIF
	ENDREPEAT

	SET_DATA_BIT(eDATABITSET_DRAW_MARKERS_ABOVE_CARRIERS)

ENDPROC

PROC SMP_GET_STEAL_OBJECTIVE_TEXT_BLUE
	IF HAS_DIALOGUE_FINISHED(SMP_DIALOGUE_ID_CLOSE_TO_PLANE)
		GET_STEAL_OBJECTIVE_TEXT_BLUE()
	ELSE
		CLEAR_OBJECTIVE()
	ENDIF
ENDPROC

PROC SMP_GET_HELP_DELIVER_OBJECTIVE_TEXT
	IF HAS_DIALOGUE_FINISHED(SMP_DIALOGUE_ID_DELIVER_PLANE)
		GET_HELP_DELIVER_OBJECTIVE_TEXT()
	ELSE
		CLEAR_OBJECTIVE()
	ENDIF
ENDPROC

PROC SMP_GET_DELIVER_OBJECTIVE_TEXT
	IF HAS_DIALOGUE_FINISHED(SMP_DIALOGUE_ID_DELIVER_PLANE)
		Print_Objective_Text_With_Two_Strings_Coloured("IDJ_DELIVER_THE", GET_ITEM_LABEL_FOR_MISSION(), "FMC_LOC_MIGH", HUD_COLOUR_WHITE, HUD_COLOUR_YELLOW)
	ELSE
		CLEAR_OBJECTIVE()
	ENDIF
ENDPROC

PROC SMP_SETUP_CLIENT_STATES()
		
	ADD_CLIENT_MODE_STATE(eSMPSTATECLIENT_GO_TO, eMODESTATE_GO_TO_POINT, &SMP_OBJECTIVE_TEXT_GO_TO)
		ADD_CLIENT_MODE_STATE_TRANSITION_LINKED(eSMPSTATECLIENT_GO_TO, eSMPSTATECLIENT_COLLECT, eSMPSTATE_COLLECT)

	ADD_CLIENT_MODE_STATES_DELIVERY(eSMPSTATECLIENT_COLLECT, eSMPSTATECLIENT_DELIVER, eSMPSTATECLIENT_HELP_DELIVER, eSMPSTATECLIENT_RECOVER, eSMPSTATECLIENT_END,
		&SMP_GET_STEAL_OBJECTIVE_TEXT_BLUE, &SMP_GET_DELIVER_OBJECTIVE_TEXT, &GET_HELP_DELIVER_OBJECTIVE_TEXT, &GET_RECOVER_OBJECTIVE_TEXT)
	
	ADD_CLIENT_MODE_STATE(eSMPSTATECLIENT_END, eMODESTATE_END, &EMPTY)

ENDPROC

PROC SMP_SETUP_STATES()
	
	ADD_MODE_STATE(eSMPSTATE_GO_TO, eMODESTATE_GO_TO_POINT)
		ADD_MODE_STATE_TRANSITION_DEFAULT(eSMPSTATE_GO_TO, eSMPSTATE_COLLECT)

	ADD_MODE_STATE(eSMPSTATE_COLLECT, eMODESTATE_COLLECT_MISSION_ENTITY)
		ADD_MODE_STATE_TRANSITION_DEFAULT(eSMPSTATE_COLLECT, eSMPSTATE_DELIVER)
		
	ADD_MODE_STATE(eSMPSTATE_DELIVER, eMODESTATE_DELIVER_MISSION_ENTITY)
		ADD_MODE_STATE_TRANSITION_DEFAULT(eSMPSTATE_DELIVER, eSMPSTATE_END)
	
	ADD_MODE_STATE(eSMPSTATE_END, eMODESTATE_END)
ENDPROC	

FUNC FLOAT SMP_SET_GOTO_POINT_RADIUS(INT iGoToPoint)
	UNUSED_PARAMETER(iGoToPoint)
	IF IS_PED_IN_ANY_HELI(LOCAL_PED_INDEX)
	OR IS_PED_IN_ANY_PLANE(LOCAL_PED_INDEX)
		RETURN 600.0
	ENDIF
	RETURN -1.0
ENDFUNC

FUNC BOOL SMP_BACKUP_VEHICLE_MODS(INT iVehicle, VEHICLE_SETUP_STRUCT_MP &sData)
	SWITCH data.Vehicle.Vehicles[iVehicle].model
		CASE VELUM2
			sData.VehicleSetup.eModel = velum2 // VELUM2
			sData.VehicleSetup.tlPlateText = "24SGX816"
			sData.VehicleSetup.iColour1 = 111
			sData.VehicleSetup.iColour2 = 27
			sData.VehicleSetup.iColourExtra1 = 111
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iLivery = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
		BREAK
		DEFAULT
			RETURN FALSE

	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

PROC SMP_MAINTAIN_DOORS(INT iVehicle, VEHICLE_INDEX vehID)
	IF iVehicle = SMP_SMUGGLERS_CAR
		IF NOT IS_VEHICLE_DOOR_DAMAGED(vehID, SC_DOOR_BOOT)
			IF GET_VEHICLE_DOOR_ANGLE_RATIO(vehID, SC_DOOR_BOOT) < 0.8
			AND NETWORK_HAS_CONTROL_OF_ENTITY(vehID)
				HOLD_VEHICLE_DOOR_OPEN(vehID, SC_DOOR_BOOT, FALSE, TRUE, DT_DOOR_NO_RESET)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SMP_MISSION_ENTITY_BLIP_ENABLE(INT iMissionEntity)
	UNUSED_PARAMETER(iMissionEntity)
	RETURN HAS_DIALOGUE_FINISHED(SMP_DIALOGUE_ID_CLOSE_TO_PLANE)
ENDFUNC

PROC SMP_PED_ATTRIBUTES(INT iPed, PED_INDEX pedID, BOOL bAmbush)
	UNUSED_PARAMETER(bAmbush)
	SWITCH iPed
		CASE 2
			SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_HEAD, 0, 1)
			SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_TORSO, 0, 2)
			SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_LEG, 0, 0)
		BREAK
		CASE 3
			SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_HEAD, 1, 2)
			SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_TORSO, 0, 3)
			SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_LEG, 0, 2)
		BREAK
		CASE 4
			SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_HEAD, 0, 0)
			SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_TORSO, 0, 0)
			SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_LEG, 0, 3)
		BREAK
	ENDSWITCH
	GET_VARIATION()
ENDPROC


FUNC STRING SMP_GET_DIALOGUE_ROOT(INT iDialogue)
	SWITCH GET_SUBVARIATION()
		CASE IHS_SMP_LOCATION_1
			SWITCH iDialogue
				CASE SMP_DIALOGUE_ID_OPENING					RETURN "HS4PA_VPL_1A"
				CASE SMP_DIALOGUE_ID_CLOSE_TO_PLANE				RETURN "HS4PA_VPL_2A"
				CASE SMP_DIALOGUE_ID_COMBAT_SMUGGLERS			RETURN "HS4PA_VPL_5"
				CASE SMP_DIALOGUE_ID_ENTER_PLANE				RETURN "HS4PA_VPL_3A"
				CASE SMP_DIALOGUE_ID_DELIVER_PLANE				RETURN "HS4PA_VPL_6A"
				CASE SMP_DIALOGUE_ID_NEAR_END					RETURN "HS4PA_GEN_6"
			ENDSWITCH
		BREAK
		CASE IHS_SMP_LOCATION_2
			SWITCH iDialogue
				CASE SMP_DIALOGUE_ID_OPENING					RETURN "HS4PA_VPL_1B"
				CASE SMP_DIALOGUE_ID_CLOSE_TO_PLANE				RETURN "HS4PA_VPL_2A"
				CASE SMP_DIALOGUE_ID_COMBAT_SMUGGLERS			RETURN "HS4PA_VPL_5"
				CASE SMP_DIALOGUE_ID_ENTER_PLANE				RETURN "HS4PA_VPL_3A"
				CASE SMP_DIALOGUE_ID_DELIVER_PLANE				RETURN "HS4PA_VPL_6B"
				CASE SMP_DIALOGUE_ID_NEAR_END					RETURN "HS4PA_GEN_6"
			ENDSWITCH
		BREAK
		CASE IHS_SMP_LOCATION_3
			SWITCH iDialogue
				CASE SMP_DIALOGUE_ID_OPENING					RETURN "HS4PA_VPL_1C"
				CASE SMP_DIALOGUE_ID_CLOSE_TO_PLANE				RETURN "HS4PA_VPL_2A"
				CASE SMP_DIALOGUE_ID_COMBAT_SMUGGLERS			RETURN "HS4PA_VPL_5"
				CASE SMP_DIALOGUE_ID_ENTER_PLANE				RETURN "HS4PA_VPL_3A"
				CASE SMP_DIALOGUE_ID_DELIVER_PLANE				RETURN "HS4PA_VPL_6C"
				CASE SMP_DIALOGUE_ID_NEAR_END					RETURN "HS4PA_GEN_6"
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC STRING SMP_GET_DIALOGUE_SUBTITLE_GROUP(INT iDialogue)
	UNUSED_PARAMETER(iDialogue)
	
	RETURN "HS4PAAU"
ENDFUNC

FUNC STRING SMP_GET_DIALOGUE_CHARACTER(INT iDialogue, INT iSpeaker)
	UNUSED_PARAMETER(iDialogue)
	UNUSED_PARAMETER(iSpeaker)
	
	RETURN "HS4_PAVEL"
ENDFUNC

FUNC BOOL SMP_SHOULD_TRIGGER_DIALOGUE(INT iDialogue)
	SWITCH iDialogue
		CASE SMP_DIALOGUE_ID_OPENING					RETURN TRUE
		CASE SMP_DIALOGUE_ID_CLOSE_TO_PLANE				RETURN GET_MODE_STATE_ID() = ENUM_TO_INT(eSMPSTATE_COLLECT)
		CASE SMP_DIALOGUE_ID_COMBAT_SMUGGLERS			RETURN GET_MODE_STATE_ID() = ENUM_TO_INT(eSMPSTATE_COLLECT) AND HAS_ANY_PED_GROUP_BEEN_TRIGGERED() AND HAS_NET_TIMER_EXPIRED(sDialogue.stTimer, 3000)
		CASE SMP_DIALOGUE_ID_ENTER_PLANE				RETURN GET_MODE_STATE_ID() = ENUM_TO_INT(eSMPSTATE_DELIVER) AND HAS_ANY_MISSION_ENTITY_BEEN_COLLECTED_FOR_THE_FIRST_TIME() AND HAS_NET_TIMER_EXPIRED(sDialogue.stTimer, 3000)
		CASE SMP_DIALOGUE_ID_DELIVER_PLANE				RETURN GET_MODE_STATE_ID() = ENUM_TO_INT(eSMPSTATE_DELIVER) AND HAS_NET_TIMER_EXPIRED(sDialogue.stTimer, 10000)
		CASE SMP_DIALOGUE_ID_NEAR_END					RETURN IS_GENERIC_BIT_SET(eGENERICBITSET_NEAR_MISSION_END)
		
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC STRING SMP_MUSIC_EVENT_STRINGS(INT iMusicEvent)
	SWITCH iMusicEvent
		CASE SMP_MUSIC_ID_DELIVERING_START				RETURN "HEI4_DELIVERING_START"
		CASE SMP_MUSIC_ID_VEHICLE_ACTION				RETURN "HEI4_GUNFIGHT"
		CASE SMP_MUSIC_ID_MED_INTENSITY					RETURN "HEI4_MED_INTENSITY"
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC STRING SMP_MUSIC_STOP_STRING()
	RETURN "HEI4_MUSIC_STOP"
ENDFUNC

FUNC STRING SMP_MUSIC_FAIL_STRING()
	RETURN "HEI4_FAIL"
ENDFUNC

FUNC INT SMP_MUSIC_EVENT(INT iCurrentEvent)
	SWITCH iCurrentEvent
		CASE -1
			IF GET_CLIENT_MODE_STATE_ID() >= ENUM_TO_INT(eSMPSTATECLIENT_GO_TO)
				RETURN SMP_MUSIC_ID_DELIVERING_START
			ENDIF
		BREAK
		
		CASE SMP_MUSIC_ID_DELIVERING_START
			IF HAS_PED_GROUP_BEEN_TRIGGERED(0)
				RETURN SMP_MUSIC_ID_VEHICLE_ACTION
			ENDIF
		BREAK
		
		CASE SMP_MUSIC_ID_VEHICLE_ACTION
			IF GET_CLIENT_MODE_STATE_ID() >= ENUM_TO_INT(eSMPSTATECLIENT_DELIVER)
				RETURN SMP_MUSIC_ID_MED_INTENSITY
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN -1
ENDFUNC

PROC SMP_CLIENT_PROCESSING()
ENDPROC

PROC SMP_SERVER_PROCESSING()
ENDPROC

PROC SMP_SETUP_LOGIC()
	
	logic.Data.Setup = &SMP_SET_PLACEMENT_DATA
	
	logic.StateMachine.SetupStates = &SMP_SETUP_STATES
	logic.StateMachine.SetupClientStates = &SMP_SETUP_CLIENT_STATES
	
	logic.GoToPoint.Radius = &SMP_SET_GOTO_POINT_RADIUS
					
	logic.Vehicle.Mods = &SMP_BACKUP_VEHICLE_MODS
	logic.Vehicle.Doors = &SMP_MAINTAIN_DOORS
	
	logic.MissionEntity.Blip.Enable = &SMP_MISSION_ENTITY_BLIP_ENABLE
	
	logic.Ped.Attributes = &SMP_PED_ATTRIBUTES

	logic.Dialogue.Root = &SMP_GET_DIALOGUE_ROOT
	logic.Dialogue.SubtitleGroup = &SMP_GET_DIALOGUE_SUBTITLE_GROUP
	logic.Dialogue.Character = &SMP_GET_DIALOGUE_CHARACTER
	logic.Dialogue.ShouldTrigger = &SMP_SHOULD_TRIGGER_DIALOGUE

	logic.Audio.MusicTrigger.EventString = &SMP_MUSIC_EVENT_STRINGS
	logic.Audio.MusicTrigger.StopString = &SMP_MUSIC_STOP_STRING
	logic.Audio.MusicTrigger.FailString = &SMP_MUSIC_FAIL_STRING
	logic.Audio.MusicTrigger.Event = &SMP_MUSIC_EVENT

	logic.Maintain.Client = &SMP_CLIENT_PROCESSING
	logic.Maintain.Server = &SMP_SERVER_PROCESSING
		
ENDPROC
