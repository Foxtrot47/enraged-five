
USING "globals.sch"
USING "rage_builtins.sch"

CONST_INT ENABLE_CONTENT	FEATURE_HEIST_ISLAND

#IF ENABLE_CONTENT
USING "variations/fm_content_island_dj_keinemusik_clothes.sch"
USING "variations/fm_content_island_dj_keinemusik_pizza.sch"
USING "variations/fm_content_island_dj_keinemusik_stones.sch"
USING "variations/fm_content_island_dj_moodyman_friends.sch"
USING "variations/fm_content_island_dj_moodyman_merch.sch"
USING "variations/fm_content_island_dj_palmstrax_equipment.sch"
USING "variations/fm_content_island_dj_palmstrax_friends.sch"

/// PURPOSE: Use to set up the logic pointers for your mission
///    
PROC SETUP_MISSION_LOGIC()
	SWITCH GET_VARIATION()
		CASE IDJV_KEINEMUSIK_CLOTHES		KM_CLOTHES_SETUP_LOGIC()		BREAK
		CASE IDJV_KEINEMUSIK_PIZZA			KM_PIZZA_SETUP_LOGIC()			BREAK
		CASE IDJV_KEINEMUSIK_STONES			KM_STONES_SETUP_LOGIC()			BREAK
		
		CASE IDJV_MOODYMAN_MERCH			MM_MERCH_SETUP_LOGIC()			BREAK
		CASE IDJV_MOODYMAN_RENTALCAR		MM_RENTALCAR_SETUP_LOGIC()		BREAK
		
		CASE IDJV_PALMSTRAX_CHAMPAGNE		PT_CHAMPAGNE_SETUP_LOGIC()		BREAK
		CASE IDJV_PALMSTRAX_EQUIPMENT		PT_EQUIPMENT_SETUP_LOGIC()		BREAK
	ENDSWITCH
ENDPROC
#ENDIF
