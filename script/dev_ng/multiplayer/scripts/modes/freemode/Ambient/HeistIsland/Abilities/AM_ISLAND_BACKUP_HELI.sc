
// ___________________________________________
//
//	Game: 
//	------
//	GTAV.
//
//
//  Script: 
//	-------
// 	Backup heli for the heist island finale, adapted from the AM_backup_heli script
//
//  Original Author: 
//	-------
//	William.Kennedy@RockstarNorth.com 
//
//	Adaption Author:
//  -------
//  Tom Turner
//
// 	Description: 
//	------------
//	Player can call in a helicopter to help them in the heist finale
//
// ___________________________________________ 

USING "globals.sch"

// Game Headers
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_camera.sch"
USING "commands_hud.sch"
USING "commands_weapon.sch"
USING "commands_clock.sch"
USING "commands_fire.sch"
USING "lineactivation.sch"
USING "fm_relationships.sch"
USING "net_mission.sch"
USING "net_gang_angry.sch"

// Network Headers
USING "net_include.sch"
USING "freemode_header.sch"

USING "net_wait_zero.sch"

CONST_INT FEATURE_ISLAND_BACKUP_HELI_SECOND_GUNNER 0

#IF FEATURE_HEIST_ISLAND
USING "net_island_backup_heli_launching.sch"

#IF IS_DEBUG_BUILD 
USING "net_debug.sch"
USING "profiler.sch"
USING "net_debug_log.sch"
#ENDIF

// ***********
// VARIABLES.
// ***********

// Game states.
CONST_INT GAME_STATE_INI						0
CONST_INT GAME_STATE_INI_SPAWN					1
CONST_INT GAME_STATE_RUNNING					2
CONST_INT GAME_STATE_END						3

CONST_INT HELI_LEAVETIME						20000 // 20 seconds.

BLIP_INDEX biHeliBlip

BOOL bOnMissionOnLaunch
BOOL bHeliAngryWithMe
BOOL bHeliPed0
BOOL bHeliPed1
#IF FEATURE_ISLAND_BACKUP_HELI_SECOND_GUNNER
BOOL bHeliPed2
#ENDIF

INT iNumVehiclesToReserve
INT iNumPedsToReserve

ENUM HELI_PEDS
	HELI_PED_PILOT
	,HELI_PED_GUNNER_ONE
	#IF FEATURE_ISLAND_BACKUP_HELI_SECOND_GUNNER
	,HELI_PED_GUNNER_TWO
	#ENDIF
ENDENUM

FLOAT fHeightMapHeight = -1
SCRIPT_TIMER stHeightMapTimer
FLOAT fCurrentFlightHeight[COUNT_OF(HELI_PEDS)]
VEHICLE_MISSION eHeliMission = MISSION_PROTECT

ENUM eHELI_STATE
    eHELISTATE_NOT_EXIST = 0,
    eHELISTATE_SPAWNING,
    eHELISTATE_GIVING_BACKUP,
	eHELISTATE_LEAVE,
	eHELISTATE_FAR_AWAY_FROM_EVERYONE,
    eHELISTATE_DEAD
ENDENUM

STRUCT STRUCT_SURVIVAL_HELI_PED
    NETWORK_INDEX netId
    MODEL_NAMES eModel = A_M_M_Hillbilly_02
ENDSTRUCT

STRUCT STRUCT_SURVIVAL_HELI_DATA
    NETWORK_INDEX netId
    MODEL_NAMES eModel = BUZZARD2
    eHELI_STATE eState
    STRUCT_SURVIVAL_HELI_PED sPed[COUNT_OF(HELI_PEDS)]
    VECTOR vSpawnCoords
    FLOAT fHeading
    BOOL bActive
    INT iCircCount
    INT iActivatingStage
	SCRIPT_TIMER stLifeSpanTimer
	SCRIPT_TIMER stLeaveTimer
ENDSTRUCT

SCRIPT_TIMER stTargetCycleTimer
PLAYER_INDEX piLastProtectedPlayer
INT iProtectedPlayerStagger = 0

// ****************
// BROADCAST DATA.
// ****************

// The server broadcast data.
// Everyone can read this data, only the server can update it. 
STRUCT ServerBroadcastData

	INT iServerGameState
	STRUCT_SURVIVAL_HELI_DATA sHeli
	BOOL bTerminate
	
ENDSTRUCT
ServerBroadcastData serverBD

// The client broadcast data.
// Everyone can read this data, only the local player only the server can update it. 
STRUCT PlayerBroadcastData

	INT iGameState
	
ENDSTRUCT
PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]

// ****************
// Widgets & Debug.
// ****************
#IF IS_DEBUG_BUILD

	BOOL bHostEndMissionNow
	
	FUNC STRING GET_HELI_STATE_NAME(eHELI_STATE eNewState)
	
		SWITCH eNewState
			CASE eHELISTATE_NOT_EXIST 				RETURN "NOT_EXIST"
		    CASE eHELISTATE_SPAWNING 				RETURN "SPAWNING"
		    CASE eHELISTATE_GIVING_BACKUP 			RETURN "GIVING_BACKUP "
			CASE eHELISTATE_LEAVE					RETURN "LEAVE"
			CASE eHELISTATE_FAR_AWAY_FROM_EVERYONE 	RETURN "FAR_AWAY_FROM_EVERYONE"
		    CASE eHELISTATE_DEAD 					RETURN "DEAD"
		ENDSWITCH
		
		RETURN "NOT_IN_SWITCH"
	ENDFUNC
	
#ENDIF

PROC GOTO_HELI_STATE(eHELI_STATE eNewState)
	
	#IF IS_DEBUG_BUILD
	IF serverBD.sHeli.eState != eNewState
		NET_PRINT("[ISLAND_BACKUP_HELI] - Backup Heli - setting state to ")NET_PRINT(GET_HELI_STATE_NAME(eNewState))NET_NL()
	ENDIF
	#ENDIF
	
	serverBD.sHeli.eState = eNewState
	
ENDPROC


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  MAIN PROCS                            ////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


FUNC BOOL CONTROL_CHECKS(NETWORK_INDEX netId)
	
	IF IS_NETWORK_ID_OWNED_BY_PARTICIPANT(netId)
		RETURN TRUE
	ENDIF
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(netId)
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Gets if the time of day means the enemy helis should have their searchlights on.
/// RETURNS:
///    BOOL - TRUE if should have searchlights on, FALSE if not.
FUNC BOOL DOES_TOD_NEED_HELI_SEARCHLIGHTS()
	
	IF GET_CLOCK_HOURS() >= 22
		RETURN TRUE
	ENDIF
	
	IF GET_CLOCK_HOURS() <= 6
	AND GET_CLOCK_HOURS() >= 0
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC REMOVE_HELI_BLIP()
    
    IF DOES_BLIP_EXIST(biHeliBlip)
        REMOVE_BLIP(biHeliBlip)
    ENDIF
    
ENDPROC

PROC MAINTAIN_HELI_MINIMUM_FLIGHT_HEIGHT()
	
	BOOL bCalculateHeight
	VECTOR vPlayerCoords
	
	IF fHeightMapHeight = (-1)
		bCalculateHeight = TRUE
	ELSE
		IF NOT HAS_NET_TIMER_STARTED(stHeightMapTimer)
			START_NET_TIMER(stHeightMapTimer)
		ELSE
			IF HAS_NET_TIMER_EXPIRED(stHeightMapTimer, 5000)
				bCalculateHeight = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF bCalculateHeight
		vPlayerCoords = GET_PLAYER_COORDS(PLAYER_ID())
		fHeightMapHeight = GET_APPROX_HEIGHT_FOR_POINT(vPlayerCoords.x, vPlayerCoords.y)
		IF fHeightMapHeight < g_sMPTunables.fIslandBackupHeliMinHeight
			fHeightMapHeight = g_sMPTunables.fIslandBackupHeliMinHeight
		ENDIF
		RESET_NET_TIMER(stHeightMapTimer)
	ENDIF
	
ENDPROC

FUNC BOOL FIND_SURVIVAL_HELI_SPAWN_COORDS()
    
    VECTOR vtemp
    
    IF IS_VECTOR_ZERO(serverBD.sHeli.vSpawnCoords)
        
		IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				
		        vtemp = GET_OFFSET_FROM_COORD_IN_WORLD_COORDS(  GET_PLAYER_COORDS(PLAYER_ID()),
		                                                        0.0,
		                                                        << (-1*200 * SIN(TO_FLOAT(serverBD.sHeli.iCircCount)*30)), (200 * COS(TO_FLOAT(serverBD.sHeli.iCircCount)*30)), 0 >>      )
		        
		        vtemp.z = GET_APPROX_HEIGHT_FOR_POINT(Vtemp.x, Vtemp.y)
		        
		        vtemp.z += 30.0
		        
		        IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(vtemp, 20.0, 1, 1, 15, TRUE, TRUE, TRUE, 180)
		                
		            serverBD.sHeli.vSpawnCoords = vtemp
		           	RETURN TRUE
					
		        ELSE
		        
		            serverBD.sHeli.iCircCount++
		            
		            IF serverBD.sHeli.iCircCount >= 12
		                serverBD.sHeli.iCircCount = 0
		            ENDIF
		            
		        ENDIF
            
			ENDIF
		ENDIF
		
    ELSE
    
        RETURN TRUE
    
    ENDIF
    
    RETURN FALSE
    
ENDFUNC

PROC SETUP_HELI_RELATIONSHIPS(NETWORK_INDEX pedNetId)
	
	REL_GROUP_HASH relGroup
	
	IF NOT IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID(), FALSE)
	AND NOT IS_FM_MISSION_LAUNCH_IN_PROGRESS()
		
		// If in free mode main session, want heli to only like me.
		relGroup = rgFM_AiAmbientGangMerc[AmbientBackupHeliMerc]
		
	ELSE
		
		// If on a mission, set the heli to do whatever my rel group tels them to do.
		relGroup = GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID())
		
	ENDIF
	
	SET_PED_RELATIONSHIP_GROUP_HASH(NET_TO_PED(pedNetId), relGroup)
	
ENDPROC

PROC FIND_NEW_PROTECT_TARGET()
	PLAYER_INDEX piPlayer 
	PLAYER_INDEX piLocalPlayer = PLAYER_ID()
	PED_INDEX pedPlayerPed
	PED_INDEX pedHeliPed = NET_TO_PED(serverBD.sHeli.sPed[HELI_PED_PILOT].netId)
	VEHICLE_INDEX viHeli = NET_TO_VEH(serverBD.sHeli.netId)
	PLAYER_INDEX piGangBoss = GB_GET_PLAYER_ALLIED_BOSS(PLAYER_ID())
	PED_INDEX pedGangBoss = GET_PLAYER_PED(piGangBoss)
	
	// Set to invalid so we keep going until we have a valid player
	piLastProtectedPlayer = INVALID_PLAYER_INDEX()
	
	// Try next player
	++iProtectedPlayerStagger
	iProtectedPlayerStagger = IWRAP_INDEX(iProtectedPlayerStagger, NUM_NETWORK_PLAYERS)

	piPlayer = INT_TO_NATIVE(PLAYER_INDEX, iProtectedPlayerStagger)
	PRINTLN("[ISLAND_BACKUP_HELI] FIND_NEW_PROTECT_TARGET - checking player: ", iProtectedPlayerStagger)
	
	IF piPlayer = INVALID_PLAYER_INDEX()
		PRINTLN("[ISLAND_BACKUP_HELI] FIND_NEW_PROTECT_TARGET - Invalid")
		EXIT
	ENDIF
	
	// We shouldn't ever hit htis but do it just in case
	IF piPlayer = piLastProtectedPlayer
		PRINTLN("[ISLAND_BACKUP_HELI] FIND_NEW_PROTECT_TARGET - is already protected")
		EXIT
	ENDIF
	
	IF NOT IS_NET_PLAYER_OK(piPlayer)
		PRINTLN("[ISLAND_BACKUP_HELI] FIND_NEW_PROTECT_TARGET - not ok")
		EXIT
	ENDIF
	
	pedPlayerPed = GET_PLAYER_PED(piPlayer)
	
	IF NOT IS_ENTITY_ALIVE(pedPlayerPed)
		PRINTLN("[ISLAND_BACKUP_HELI] FIND_NEW_PROTECT_TARGET - not alive")
		EXIT
	ENDIF
	
	IF IS_ENTITY_ALIVE(pedGangBoss)
		IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(pedGangBoss), GET_ENTITY_COORDS(pedPlayerPed), FALSE) > g_fIslandBackupHeliCycleDistance
			PRINTLN("[ISLAND_BACKUP_HELI] FIND_NEW_PROTECT_TARGET - to far away from host player")
			EXIT
		ENDIF
	ENDIF
	
	IF GB_ARE_PLAYERS_IN_SAME_GANG(piLocalPlayer, piPlayer) OR ARE_PLAYERS_ON_SAME_TEAM(piLocalPlayer, piPlayer)
		TASK_HELI_MISSION(pedHeliPed, viHeli, NULL, pedPlayerPed, <<0.0,0.0,0.0>>, eHeliMission, 20.0, 40.0, -1.0, CEIL(fCurrentFlightHeight[0]), 10)
		PRINTLN("[ISLAND_BACKUP_HELI] FIND_NEW_PROTECT_TARGET - now protecting: ", GET_PLAYER_NAME(piPlayer))
		
		// Set selected player so we can skip over them next time
		piLastProtectedPlayer = piPlayer
	ENDIF		
ENDPROC

PROC MAINTAIN_PROTECTING_GANG_MEMBERS()
	// In case everything breaks we have a tunable to turn it off
	IF NOT g_bIslandBackupHeliCycleTargets
		EXIT
	ENDIF
	
	// Only the player who owns the heli should be doing this
	IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sHeli.netId)
		EXIT
	ENDIF
	
	VEHICLE_INDEX viHeli = NET_TO_VEH(serverBD.sHeli.netId)
	
	IF IS_VEHICLE_FUCKED(viHeli)
		PRINTLN("[ISLAND_BACKUP_HELI] MAINTAIN_PROTECTING_GANG_MEMBERS - Heli is undriveable")
		EXIT
	ENDIF
	
	IF NOT IS_ENTITY_ALIVE(viHeli)
		PRINTLN("[ISLAND_BACKUP_HELI] MAINTAIN_PROTECTING_GANG_MEMBERS - Heli is dead")
		EXIT
	ENDIF
	
	// Process until we find a player
	IF piLastProtectedPlayer = INVALID_PLAYER_INDEX()
		FIND_NEW_PROTECT_TARGET()
		EXIT
	ENDIF
	
	PED_INDEX pedLastProtected = GET_PLAYER_PED(piLastProtectedPlayer)
	
	// Only do this check once, as soon as we are in range the timer will start
	IF NOT HAS_NET_TIMER_STARTED(stTargetCycleTimer)
		IF piLastProtectedPlayer != INVALID_PLAYER_INDEX() AND IS_ENTITY_ALIVE(pedLastProtected)
			// Wait until we arrive at player before attempting to cycle to next
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(pedLastProtected), GET_ENTITY_COORDS(viHeli), FALSE) > 50.0
				PRINTLN("[ISLAND_BACKUP_HELI] MAINTAIN_PROTECTING_GANG_MEMBERS - Heli hasn't arrived at target yet")
				EXIT
			ENDIF
		ENDIF
	ENDIF
	
	// Only do this so often
	IF NOT HAS_NET_TIMER_EXPIRED(stTargetCycleTimer, g_iIslandBackupHeliCycleTargetDelayMS)
		EXIT
	ENDIF
	
	// Reset timer for next attempt
	RESET_NET_TIMER(stTargetCycleTimer)
	
	INT iPed
	INT iMaxPed = COUNT_OF(serverBD.sHeli.sPed)
	BOOL bInCombat = FALSE
	PED_INDEX pedHeliPed
	
	// Loop through all heli peds, we only want to find a new ttarget if non of them are in combat
	REPEAT iMaxPed iPed
		pedHeliPed = NET_TO_PED(serverBD.sHeli.sPed[iPed].netId)
		
		IF IS_ENTITY_ALIVE(pedHeliPed) AND IS_PED_IN_COMBAT(pedHeliPed)
			bInCombat = TRUE
			BREAKLOOP
		ENDIF
	ENDREPEAT
	
	// If we aren't in combat
	IF NOT bInCombat
		PRINTLN("[ISLAND_BACKUP_HELI] MAINTAIN_PROTECTING_GANG_MEMBERS - finding new target")
		piLastProtectedPlayer = INVALID_PLAYER_INDEX()
	ENDIF
ENDPROC

PROC SET_HELI_PED_COMBAT_AI(PED_INDEX pedId, INT iPed, BOOL bInvincible = FALSE, BOOL bSetHealth = TRUE)
	
	GIVE_DELAYED_WEAPON_TO_PED(pedId, WEAPONTYPE_ASSAULTRIFLE, 300, TRUE)
    SET_PED_COMBAT_ATTRIBUTES(pedId, CA_ALWAYS_FIGHT, TRUE)
	SET_PED_COMBAT_ATTRIBUTES(pedId, CA_LEAVE_VEHICLES, FALSE)
	SET_PED_COMBAT_MOVEMENT(pedId, CM_WILLADVANCE)
	SET_PED_COMBAT_ABILITY(pedId, CAL_PROFESSIONAL)
    SET_PED_COMBAT_RANGE(pedId, CR_FAR)
	SET_PED_TARGET_LOSS_RESPONSE(pedId, TLR_NEVER_LOSE_TARGET)
	SET_PED_HIGHLY_PERCEPTIVE(pedId, TRUE)
	SET_PED_CAN_BE_TARGETTED(pedId, TRUE)
	SET_PED_SEEING_RANGE(pedId, fCurrentFlightHeight[iPed]+100.0)
	SET_PED_VISUAL_FIELD_PERIPHERAL_RANGE(pedId, 400.0)
	
	SET_COMBAT_FLOAT(pedId, CCF_HELI_SENSES_RANGE, 400.0)
	
	// Make peds a bit tougher.
	IF NOT bInvincible
		IF bSetHealth
			SET_ENTITY_MAX_HEALTH(pedId, 250)
			SET_ENTITY_HEALTH(pedId, 250)
			SET_PED_ARMOUR(pedId, 250)
		ENDIF
	ELSE
		SET_ENTITY_INVINCIBLE(pedId, TRUE)
	ENDIF
	
ENDPROC

FUNC BOOL SPAWN_SURVIVAL_HELI()
    MODEL_NAMES ePilotModel = A_M_M_FARMER_01 // TODO: get pilot from missions
	
    IF REQUEST_LOAD_MODEL(serverBD.sHeli.eModel)
        IF REQUEST_LOAD_MODEL(serverBD.sHeli.sPed[HELI_PED_GUNNER_ONE].eModel) AND REQUEST_LOAD_MODEL(ePilotModel)
            
            IF NOT IS_NET_VEHICLE_DRIVEABLE(serverBD.sHeli.netId)
        
                IF CREATE_NET_VEHICLE(serverBD.sHeli.netId, serverBD.sHeli.eModel, serverBD.sHeli.vSpawnCoords, 0.0, TRUE,TRUE, TRUE, FALSE, FALSE)
                    VEHICLE_INDEX viHeli = NET_TO_VEH(serverBD.sHeli.netId)
					IF GET_NUM_MOD_KITS(viHeli) > 0
						SET_VEHICLE_MOD_KIT(viHeli, 0)
					ENDIF
					SET_VEHICLE_COLOURS(viHeli, 127, 0)
					SET_HELI_BLADES_FULL_SPEED(viHeli)
					SET_VEHICLE_ENGINE_ON(viHeli, TRUE, TRUE)
					SET_VEHICLE_DOORS_LOCKED(viHeli, VEHICLELOCK_UNLOCKED)
					ACTIVATE_PHYSICS(viHeli)
					SET_ENTITY_DYNAMIC(viHeli, TRUE)
					SET_ATTRIBUTE_DAMAGE_DECORATOR_BIT(viHeli, ATTRIBUTE_DAMAGE_BS_BACKUP_HELI)
					mpglobals.sIslandBackupHeli.viHeli = viHeli
					SET_ENTITY_HEALTH(viHeli, g_sMPTunables.iIslandBackupHeliHealth)
					
                   	NET_PRINT("[ISLAND_BACKUP_HELI] - Backup Heli - SPAWN_SURVIVAL_HELI - created heli.")NET_NL()
					 
                    IF CREATE_NET_PED_IN_VEHICLE(serverBD.sHeli.sPed[HELI_PED_PILOT].netId, serverBD.sHeli.netId, PEDTYPE_CIVMALE, ePilotModel, VS_DRIVER, TRUE)
                        PED_INDEX pedPilot = NET_TO_PED(serverBD.sHeli.sPed[HELI_PED_PILOT].netId)
                        // Set driver properties.
                        SETUP_HELI_RELATIONSHIPS(serverBD.sHeli.sPed[HELI_PED_PILOT].netId)
						SET_HELI_PED_COMBAT_AI(pedPilot, 0)
                        NETWORK_SET_ATTRIBUTE_DAMAGE_TO_PLAYER(pedPilot, PLAYER_ID())
						SET_ATTRIBUTE_DAMAGE_DECORATOR_BIT(pedPilot, ATTRIBUTE_DAMAGE_BS_BACKUP_HELI)
						mpglobals.sIslandBackupHeli.viPeds[HELI_PED_PILOT] = pedPilot
						SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_HEAD, 1, 1)
						SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_HAIR, 1, 1)
						SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_TORSO, 0, 0)
						SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_LEG, 0, 0)
						SET_PED_COMPONENT_VARIATION(pedPilot, PED_COMP_SPECIAL, 0, 1)
						SET_PED_PROP_INDEX(pedPilot, ANCHOR_EYES, 0, 0)
						SET_PED_PROP_INDEX(pedPilot, ANCHOR_HEAD, 0, 0)
						
						NET_PRINT("[ISLAND_BACKUP_HELI] - Backup Heli - SPAWN_SURVIVAL_HELI - created pilot.")NET_NL()
						
                        IF CREATE_NET_PED_IN_VEHICLE(serverBD.sHeli.sPed[HELI_PED_GUNNER_ONE].netId, serverBD.sHeli.netId, PEDTYPE_CIVMALE, serverBD.sHeli.sPed[HELI_PED_GUNNER_ONE].eModel, VS_BACK_LEFT, TRUE)
                            
                            // Set driver properties.
							SETUP_HELI_RELATIONSHIPS(serverBD.sHeli.sPed[HELI_PED_GUNNER_ONE].netId)
                            SET_HELI_PED_COMBAT_AI(NET_TO_PED(serverBD.sHeli.sPed[HELI_PED_GUNNER_ONE].netId), 1, TRUE)
							NETWORK_SET_ATTRIBUTE_DAMAGE_TO_PLAYER(NET_TO_PED(serverBD.sHeli.sPed[HELI_PED_GUNNER_ONE].netId), PLAYER_ID())
							SET_ATTRIBUTE_DAMAGE_DECORATOR_BIT(NET_TO_PED(serverBD.sHeli.sPed[HELI_PED_GUNNER_ONE].netId), ATTRIBUTE_DAMAGE_BS_BACKUP_HELI)
							mpglobals.sIslandBackupHeli.viPeds[HELI_PED_GUNNER_ONE] = NET_TO_PED(serverBD.sHeli.sPed[HELI_PED_GUNNER_ONE].netId)
							
							NET_PRINT("[ISLAND_BACKUP_HELI] - Backup Heli - SPAWN_SURVIVAL_HELI - created co-pilot.")NET_NL()
	                        
							#IF NOT FEATURE_ISLAND_BACKUP_HELI_SECOND_GUNNER
								SET_MODEL_AS_NO_LONGER_NEEDED(serverBD.sHeli.sPed[HELI_PED_GUNNER_ONE].eModel)
								SET_MODEL_AS_NO_LONGER_NEEDED(ePilotModel)
								SET_MODEL_AS_NO_LONGER_NEEDED(serverBD.sHeli.eModel)
								RETURN TRUE
							#ENDIF
							
							#IF FEATURE_ISLAND_BACKUP_HELI_SECOND_GUNNER
 							IF CREATE_NET_PED_IN_VEHICLE(serverBD.sHeli.sPed[HELI_PED_GUNNER_TWO].netId, serverBD.sHeli.netId, PEDTYPE_CIVMALE, serverBD.sHeli.sPed[HELI_PED_GUNNER_TWO].eModel, VS_BACK_RIGHT, TRUE)
	                            
	                            // Set driver properties.
								SETUP_HELI_RELATIONSHIPS(serverBD.sHeli.sPed[HELI_PED_GUNNER_TWO].netId)
	                            SET_HELI_PED_COMBAT_AI(NET_TO_PED(serverBD.sHeli.sPed[HELI_PED_GUNNER_TWO].netId), 2, TRUE)
								NETWORK_SET_ATTRIBUTE_DAMAGE_TO_PLAYER(NET_TO_PED(serverBD.sHeli.sPed[HELI_PED_GUNNER_TWO].netId), PLAYER_ID())
								SET_ATTRIBUTE_DAMAGE_DECORATOR_BIT(NET_TO_PED(serverBD.sHeli.sPed[HELI_PED_GUNNER_TWO].netId), ATTRIBUTE_DAMAGE_BS_BACKUP_HELI)
								mpglobals.sIslandBackupHeli.viPeds[HELI_PED_GUNNER_TWO] = NET_TO_PED(serverBD.sHeli.sPed[HELI_PED_GUNNER_TWO].netId)
								
								NET_PRINT("[WJK] - Backup Heli - SPAWN_SURVIVAL_HELI - created sniper 2, returning true.")NET_NL()
								
								SET_MODEL_AS_NO_LONGER_NEEDED(serverBD.sHeli.sPed[HELI_PED_GUNNER_ONE].eModel)
								SET_MODEL_AS_NO_LONGER_NEEDED(ePilotModel)
								SET_MODEL_AS_NO_LONGER_NEEDED(serverBD.sHeli.eModel)
	                            RETURN TRUE
	                          
                            ENDIF
							#ENDIF
	                            
                            
                        ENDIF
                        
                    ENDIF
                    
                ENDIF
                
            ENDIF
    
        ENDIF
    ENDIF
    
    RETURN FALSE
    
ENDFUNC

PROC MAINTAIN_RESERVATION()
	
	iNumVehiclesToReserve = 0
	iNumPedsToReserve = 0

	IF IS_NET_VEHICLE_DRIVEABLE(serverBD.sHeli.netId)
		iNumVehiclesToReserve++
	ELSE
		IF DOES_ENTITY_EXIST(NET_TO_VEH(serverBD.sHeli.netId))
			CLEANUP_NET_ID(serverBD.sHeli.netId)
			iNumVehiclesToReserve++
		ENDIF
	ENDIF
	
	IF IS_NET_PED_INJURED(serverBD.sHeli.sPed[HELI_PED_PILOT].netId)
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sHeli.sPed[HELI_PED_PILOT].netId)
			CLEANUP_NET_ID(serverBD.sHeli.sPed[HELI_PED_PILOT].netId)
			iNumPedsToReserve++
		ENDIF
	ELSE
		iNumPedsToReserve++
	ENDIF
	
	IF IS_NET_PED_INJURED(serverBD.sHeli.sPed[HELI_PED_GUNNER_ONE].netId)
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sHeli.sPed[HELI_PED_GUNNER_ONE].netId)
			CLEANUP_NET_ID(serverBD.sHeli.sPed[HELI_PED_GUNNER_ONE].netId)
			iNumPedsToReserve++
		ENDIF
	ELSE
		iNumPedsToReserve++
	ENDIF
	
	#IF FEATURE_ISLAND_BACKUP_HELI_SECOND_GUNNER
	IF IS_NET_PED_INJURED(serverBD.sHeli.sPed[HELI_PED_GUNNER_TWO].netId)
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sHeli.sPed[HELI_PED_GUNNER_TWO].netId)
			CLEANUP_NET_ID(serverBD.sHeli.sPed[HELI_PED_GUNNER_TWO].netId)
			iNumPedsToReserve++
		ENDIF
	ELSE
		iNumPedsToReserve++
	ENDIF
	#ENDIF
	
	
	IF GET_NUM_RESERVED_MISSION_VEHICLES() != iNumVehiclesToReserve
		IF CAN_RESERVE_NETWORK_VEHICLES_FOR_THIS_SCRIPT(iNumVehiclesToReserve, FALSE, TRUE)
			RESERVE_NETWORK_MISSION_VEHICLES(iNumVehiclesToReserve)
		ENDIF
	ENDIF
	
	IF GET_NUM_RESERVED_MISSION_PEDS() != iNumPedsToReserve
		IF CAN_RESERVE_NETWORK_PEDS_FOR_THIS_SCRIPT(iNumPedsToReserve, FALSE, TRUE)
			RESERVE_NETWORK_MISSION_PEDS(iNumPedsToReserve)
		ENDIF
	ENDIF
	
ENDPROC

PROC MAINTAIN_ANGRY_TRACKING()
		
	IF mpglobals.bIKilledABackupHeliEntity
		SET_GANG_ANGRY_AT_PLAYER(GANG_CALL_TYPE_SPECIAL_MERRYWEATHER, GANG_ANGRY_TIME_LONG)
		bHeliAngryWithMe = TRUE
	ENDIF
	
	IF mpglobals.fBackUpHeliDamageByMe >= 250.0
		SET_GANG_ANGRY_AT_PLAYER(GANG_CALL_TYPE_SPECIAL_MERRYWEATHER, GANG_ANGRY_TIME_LONG)
		bHeliAngryWithMe = TRUE
	ENDIF
	
ENDPROC

PROC PROCESS_HELI_BRAIN()
    
    SWITCH serverBD.sHeli.eState
        
        CASE eHELISTATE_NOT_EXIST
            
         	GOTO_HELI_STATE(eHELISTATE_SPAWNING)
            
        BREAK
        
        CASE eHELISTATE_SPAWNING
            
            IF FIND_SURVIVAL_HELI_SPAWN_COORDS()
                IF SPAWN_SURVIVAL_HELI()
					START_NET_TIMER(serverBD.sHeli.stLifeSpanTimer)
                    GOTO_HELI_STATE(eHELISTATE_GIVING_BACKUP)
                ELSE
                    NET_PRINT("[ISLAND_BACKUP_HELI] - Backup Heli - found suitable spawn coords, spawning heli.")NET_NL()
                ENDIF
            ELSE
                NET_PRINT("[ISLAND_BACKUP_HELI] - Backup Heli - finding heli spawn coords.")NET_NL()
            ENDIF
               
        BREAK
        
        CASE eHELISTATE_GIVING_BACKUP
            
			MAINTAIN_RESERVATION()
			MAINTAIN_ANGRY_TRACKING()
			
			IF HEIST_ISLAND_BACKUP_HELI__IS_CANCELED()
				NET_PRINT("[ISLAND_BACKUP_HELI] - Backup Heli - Requested backup to be canceled")NET_NL()
				GOTO_HELI_STATE(eHELISTATE_LEAVE)
				_HEIST_ISLAND__GLOBAL_SET_CANCEL_ISLAND_BACKUP_HELI(FALSE)
			ENDIF	
			
            IF NOT IS_NET_VEHICLE_DRIVEABLE(serverBD.sHeli.netId)
				IF IS_NET_PED_INJURED(serverBD.sHeli.sPed[HELI_PED_PILOT].netId)
				AND IS_NET_PED_INJURED(serverBD.sHeli.sPed[HELI_PED_GUNNER_ONE].netId)
				#IF FEATURE_ISLAND_BACKUP_HELI_SECOND_GUNNER
				AND IS_NET_PED_INJURED(serverBD.sHeli.sPed[HELI_PED_GUNNER_TWO].netId)
				#ENDIF
					NET_PRINT("[ISLAND_BACKUP_HELI] - Backup Heli - heli wrecked.")NET_NL()
	                GOTO_HELI_STATE(eHELISTATE_DEAD)
				ENDIF
            ELIF IS_NET_PED_INJURED(serverBD.sHeli.sPed[HELI_PED_PILOT].netId)
				NET_PRINT("[ISLAND_BACKUP_HELI] - Backup Heli - pilot dead.")NET_NL()
				IF IS_NET_PED_INJURED(serverBD.sHeli.sPed[HELI_PED_GUNNER_ONE].netId)
	                GOTO_HELI_STATE(eHELISTATE_DEAD)
				ENDIF
			ELIF IS_NET_PED_INJURED(serverBD.sHeli.sPed[HELI_PED_GUNNER_ONE].netId) 
			#IF FEATURE_ISLAND_BACKUP_HELI_SECOND_GUNNER
			AND IS_NET_PED_INJURED(serverBD.sHeli.sPed[HELI_PED_GUNNER_TWO].netId)
			#ENDIF
				NET_PRINT("[ISLAND_BACKUP_HELI] - Backup Heli - both snipers dead.")NET_NL()
				GOTO_HELI_STATE(eHELISTATE_LEAVE)
			ELSE
				IF HAS_NET_TIMER_STARTED(serverBD.sHeli.stLifeSpanTimer)
					IF HAS_NET_TIMER_EXPIRED(serverBD.sHeli.stLifeSpanTimer,  g_sMPTunables.iIslandBackupHeliLifespanMS)
						NET_PRINT("[ISLAND_BACKUP_HELI] - Backup Heli - heli been providing backup for ")NET_PRINT_INT( g_sMPTunables.iIslandBackupHeliLifespanMS)NET_PRINT(" seconds.")NET_NL()
						GOTO_HELI_STATE(eHELISTATE_LEAVE)
					ENDIF
				ENDIF
				IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(),MPAM_TYPE_CINEMA)
					NET_PRINT("[ISLAND_BACKUP_HELI] - Backup Heli - player is on cinema script.")NET_NL()
					GOTO_HELI_STATE(eHELISTATE_LEAVE)
				ENDIF
				IF bHeliAngryWithMe
					NET_PRINT("[ISLAND_BACKUP_HELI] - Backup Heli - heli is angry with me, leaving.")NET_NL()
					GOTO_HELI_STATE(eHELISTATE_LEAVE)
				ENDIF
				IF IS_FM_PLAYER_CRITICAL_TO_THIS_FM_EVENT(PLAYER_ID(), FMMC_TYPE_PENNED_IN)
					NET_PRINT("[ISLAND_BACKUP_HELI] - Backup Heli - player is on penned in.")NET_NL()
					GOTO_HELI_STATE(eHELISTATE_LEAVE)
				ENDIF
							
				
            ENDIF
                
        BREAK
		
        CASE eHELISTATE_LEAVE
			
			MAINTAIN_RESERVATION()
			
			IF NOT IS_NET_VEHICLE_DRIVEABLE(serverBD.sHeli.netId)
				IF IS_NET_PED_INJURED(serverBD.sHeli.sPed[HELI_PED_PILOT].netId)
				AND IS_NET_PED_INJURED(serverBD.sHeli.sPed[HELI_PED_GUNNER_ONE].netId)
				#IF FEATURE_ISLAND_BACKUP_HELI_SECOND_GUNNER
				AND IS_NET_PED_INJURED(serverBD.sHeli.sPed[HELI_PED_GUNNER_TWO].netId)
				#ENDIF
	                GOTO_HELI_STATE(eHELISTATE_DEAD)
				ENDIF
            ELIF IS_NET_PED_INJURED(serverBD.sHeli.sPed[HELI_PED_PILOT].netId)
				IF IS_NET_PED_INJURED(serverBD.sHeli.sPed[HELI_PED_GUNNER_ONE].netId)
				#IF FEATURE_ISLAND_BACKUP_HELI_SECOND_GUNNER
				AND IS_NET_PED_INJURED(serverBD.sHeli.sPed[HELI_PED_GUNNER_TWO].netId)
				#ENDIF
	                GOTO_HELI_STATE(eHELISTATE_DEAD)
				ENDIF
			ENDIF
			
		BREAK
		
        CASE eHELISTATE_DEAD
            
			
			
        BREAK
        
    ENDSWITCH
    
ENDPROC

PROC KILL_HELI_CREW()
	
	PED_INDEX pedId
	
	IF NOT IS_NET_PED_INJURED(serverBD.sHeli.sPed[HELI_PED_PILOT].netId)
		IF CONTROL_CHECKS(serverBD.sHeli.sPed[HELI_PED_PILOT].netId)
            IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sHeli.sPed[HELI_PED_PILOT].netId)
                pedId = NET_TO_PED(serverBD.sHeli.sPed[HELI_PED_PILOT].netId)
                SET_ENTITY_HEALTH(pedId, 0)
            ENDIF
		ENDIF
    ENDIF
    
    IF NOT IS_NET_PED_INJURED(serverBD.sHeli.sPed[HELI_PED_GUNNER_ONE].netId)
		IF CONTROL_CHECKS(serverBD.sHeli.sPed[HELI_PED_GUNNER_ONE].netId)
            IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sHeli.sPed[HELI_PED_GUNNER_ONE].netId)
            	pedId = NET_TO_PED(serverBD.sHeli.sPed[HELI_PED_GUNNER_ONE].netId)
               	SET_ENTITY_HEALTH(pedId, 0)
            ENDIF
		ENDIF
    ENDIF
    
	#IF FEATURE_ISLAND_BACKUP_HELI_SECOND_GUNNER
    IF NOT IS_NET_PED_INJURED(serverBD.sHeli.sPed[HELI_PED_GUNNER_TWO].netId)
		IF CONTROL_CHECKS(serverBD.sHeli.sPed[HELI_PED_GUNNER_TWO].netId)
            IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sHeli.sPed[HELI_PED_GUNNER_TWO].netId)
                pedId = NET_TO_PED(serverBD.sHeli.sPed[HELI_PED_GUNNER_TWO].netId)
                SET_ENTITY_HEALTH(pedId, 0)
            ENDIF
		ENDIF
    ENDIF
	#ENDIF
	
ENDPROC

PROC CREATE_HELI_BLIP()
	VEHICLE_INDEX viHeli = NET_TO_VEH(serverBD.sHeli.netId)
	IF NOT DOES_BLIP_EXIST(biHeliBlip) AND DOES_ENTITY_EXIST(viHeli)
        biHeliBlip = ADD_BLIP_FOR_ENTITY(viHeli)
		SET_BLIP_SPRITE(biHeliBlip, RADAR_TRACE_PLAYER_HELI)
		SHOW_HEIGHT_ON_BLIP(biHeliBlip, FALSE)
		SET_BLIP_NAME_FROM_TEXT_FILE(biHeliBlip, "MPCT_MERRY3")
		SET_BLIP_COLOUR_FROM_HUD_COLOUR(biHeliBlip, HUD_COLOUR_WHITE)
    ENDIF
ENDPROC

PROC PROCESS_HELI_BODY()
    
    PED_INDEX pedId
    VEHICLE_INDEX niHeli
	VEHICLE_MISSION eVehicleMission
	
    SWITCH serverBD.sHeli.eState
            
            CASE eHELISTATE_NOT_EXIST
                    
                REMOVE_HELI_BLIP()
                    
            BREAK
            
            CASE eHELISTATE_SPAWNING
                    
                REMOVE_HELI_BLIP()
                    
            BREAK
            
            CASE eHELISTATE_GIVING_BACKUP
				
				// If heli is stil flying.
                IF IS_NET_VEHICLE_DRIVEABLE(serverBD.sHeli.netId)
                    
					niHeli = NET_TO_VEH(serverBD.sHeli.netId)
					CREATE_HELI_BLIP()
					
					IF CONTROL_CHECKS(serverBD.sHeli.netId)
						IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sHeli.netId)
							
							IF DOES_VEHICLE_HAVE_SEARCHLIGHT(niHeli)
								IF DOES_TOD_NEED_HELI_SEARCHLIGHTS()
									IF NOT IS_VEHICLE_SEARCHLIGHT_ON(niHeli)
										SET_VEHICLE_SEARCHLIGHT(niHeli, TRUE)
									ENDIF
								ELSE
									IF IS_VEHICLE_SEARCHLIGHT_ON(niHeli)
										SET_VEHICLE_SEARCHLIGHT(niHeli, FALSE)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF		
					
					// Make sure we always engage enemies of gang members
					MAINTAIN_PROTECTING_GANG_MEMBERS()
					
					IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.sHeli.sPed[HELI_PED_PILOT].netId)
						pedId = NET_TO_PED(serverBD.sHeli.sPed[HELI_PED_PILOT].netId)
						IF NOT IS_PED_INJURED(pedId)
							
							eVehicleMission = GET_ACTIVE_VEHICLE_MISSION_TYPE(niHeli)
							
							IF eVehicleMission != eHeliMission
							OR (fCurrentFlightHeight[0] != fHeightMapHeight) // Sorted in MAINTAIN_HELI_MINIMUM_FLIGHT_HEIGHT().
								#IF IS_DEBUG_BUILD
									IF eVehicleMission != eHeliMission
										NET_PRINT("[ISLAND_BACKUP_HELI] - Backup Heli - eVehicleMission != MISSION_PROTECT.")NET_NL()
									ENDIF
									IF (fCurrentFlightHeight[0] != fHeightMapHeight)
										NET_PRINT("[ISLAND_BACKUP_HELI] - Backup Heli - (fCurrentFlightHeight[0] != fHeightMapHeight).")NET_NL()
									ENDIF
								#ENDIF
								IF CONTROL_CHECKS(serverBD.sHeli.netId)
			                        IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sHeli.sPed[HELI_PED_PILOT].netId)
										fCurrentFlightHeight[0] = fHeightMapHeight
		                                SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedId, TRUE)
										SET_HELI_PED_COMBAT_AI(pedId, 0, FALSE, FALSE)
										SET_HELI_BLADES_FULL_SPEED(niHeli)
										SET_VEHICLE_ENGINE_ON(niHeli, TRUE, TRUE)
										IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
											IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
												TASK_HELI_MISSION(pedId, niHeli, NULL, PLAYER_PED_ID(), <<0.0,0.0,0.0>>, eHeliMission, 20.0, 40.0, -1.0, CEIL(fCurrentFlightHeight[0]), 10)
												NET_PRINT("[ISLAND_BACKUP_HELI] - Backup Heli - given TASK_HELI_MISSION(pedId, niHeli, NULL, PLAYER_PED_ID(), <<0.0,0.0,0.0>>, MISSION_PROTECT, 20.0, 40.0, -1.0, CEIL(fCurrentFlightHeight[0]), 10)")NET_NL()
												NET_PRINT("[ISLAND_BACKUP_HELI] - Backup Heli - fCurrentFlightHeight[0] = ")NET_PRINT_FLOAT(fCurrentFlightHeight[0])NET_NL()
											ENDIF
										ENDIF
									ENDIF
								ENDIF
	                        ENDIF
							
						ELSE
							NET_PRINT("[ISLAND_BACKUP_HELI] - Backup Heli - pilot is dead.")NET_NL()
	                    ENDIF
                    ELSE
						NET_PRINT("[ISLAND_BACKUP_HELI] - Backup Heli - pilot does not exist.")NET_NL()
					ENDIF
					
					
					
					IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.sHeli.sPed[HELI_PED_GUNNER_ONE].netId)
						pedId = NET_TO_PED(serverBD.sHeli.sPed[HELI_PED_GUNNER_ONE].netId)
						IF NOT IS_PED_INJURED(pedId)
							IF NOT IS_PED_IN_COMBAT(pedId)
							OR (fCurrentFlightHeight[1] != fHeightMapHeight) // Sorted in MAINTAIN_HELI_MINIMUM_FLIGHT_HEIGHT().
								IF CONTROL_CHECKS(serverBD.sHeli.netId)
			                        IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sHeli.sPed[HELI_PED_GUNNER_ONE].netId)
										fCurrentFlightHeight[1] = fHeightMapHeight
		                                SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedId, FALSE)
										SET_HELI_PED_COMBAT_AI(pedId, 1, FALSE, FALSE)
		                            ENDIF
								ENDIF
	                        ENDIF
						ELSE
							NET_PRINT("[ISLAND_BACKUP_HELI] - Backup Heli - sniper 1 is dead.")NET_NL()
	                    ENDIF
					ELSE
						NET_PRINT("[ISLAND_BACKUP_HELI] - Backup Heli - sniper 1 does not exist.")NET_NL()
					ENDIF
					
					#IF FEATURE_ISLAND_BACKUP_HELI_SECOND_GUNNER
					IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.sHeli.sPed[HELI_PED_GUNNER_TWO].netId)
						pedId = NET_TO_PED(serverBD.sHeli.sPed[HELI_PED_GUNNER_TWO].netId)
						IF NOT IS_PED_INJURED(pedId)
							IF NOT IS_PED_IN_COMBAT(pedId)
							OR (fCurrentFlightHeight[HELI_PED_GUNNER_TWO] != fHeightMapHeight) // Sorted in MAINTAIN_HELI_MINIMUM_FLIGHT_HEIGHT().
								IF CONTROL_CHECKS(serverBD.sHeli.netId)
			                        IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sHeli.sPed[HELI_PED_GUNNER_TWO].netId)
										fCurrentFlightHeight[HELI_PED_GUNNER_TWO] = fHeightMapHeight
		                                SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedId, FALSE)
										SET_HELI_PED_COMBAT_AI(pedId, 2, FALSE, FALSE)
//		                                TASK_COMBAT_HATED_TARGETS_IN_AREA(pedId, GET_ENTITY_COORDS(pedId), 299.0)
		                            ENDIF
								ENDIF
	                        ENDIF
						ELSE
							NET_PRINT("[WJK] - Backup Heli - sniper 2 is dead.")NET_NL()
	                    ENDIF
					ELSE
						NET_PRINT("[WJK] - Backup Heli - sniper 2 does not exist.")NET_NL()
					ENDIF
					#ENDIF
					
                ENDIF
                
				// If heli or pilot are dead, kill crew.
				IF NOT IS_NET_VEHICLE_DRIVEABLE(serverBD.sHeli.netId)
				OR IS_NET_PED_INJURED(serverBD.sHeli.sPed[HELI_PED_PILOT].netId)
					KILL_HELI_CREW()
				ENDIF
				
            BREAK
            
			CASE eHELISTATE_LEAVE
				
				REMOVE_HELI_BLIP()
				
				IF NOT HAS_NET_TIMER_STARTED(serverBD.sHeli.stLeaveTimer)
					PRINTSTRING("Starting stLeaveTimer now") PRINTNL()
					START_NET_TIMER(serverBD.sHeli.stLeaveTimer)
				ENDIF
				
				IF IS_NET_VEHICLE_DRIVEABLE(serverBD.sHeli.netId)
                    
					niHeli = NET_TO_VEH(serverBD.sHeli.netId)
					
                    IF NOT IS_NET_PED_INJURED(serverBD.sHeli.sPed[HELI_PED_PILOT].netId)
						pedId = NET_TO_PED(serverBD.sHeli.sPed[HELI_PED_PILOT].netId)

						IF bHeliPed0 = FALSE
							IF CONTROL_CHECKS(serverBD.sHeli.netId)
		                        IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sHeli.sPed[HELI_PED_PILOT].netId)
	                                SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedId, FALSE)
									CLEAR_PED_TASKS(pedId)
									SET_PED_RELATIONSHIP_GROUP_HASH(pedId, rgFM_AiLike)
									IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
										IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
											TASK_HELI_MISSION(pedId, niHeli, NULL, PLAYER_PED_ID(), <<0.0,0.0,0.0>>, MISSION_FLEE, 20.0, 500.0, -1.0, 40, 10)
											SET_PED_KEEP_TASK(pedId, TRUE)
											bHeliPed0 = TRUE
											PRINTSTRING("TASK_HELI_MISSION MISSION_FLEE Being given to heli pilot") PRINTNL()
										ENDIF
									ENDIF
	                            ENDIF
							ENDIF
                        ENDIF
                    ENDIF
                    
					IF NOT IS_NET_PED_INJURED(serverBD.sHeli.sPed[HELI_PED_GUNNER_ONE].netId)
						IF bHeliPed1 = FALSE
							pedId = NET_TO_PED(serverBD.sHeli.sPed[HELI_PED_GUNNER_ONE].netId)
							IF CONTROL_CHECKS(serverBD.sHeli.netId)
		                        IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sHeli.sPed[HELI_PED_GUNNER_ONE].netId)
									CLEAR_PED_TASKS(pedId)
	                                SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedId, TRUE)
									SET_PED_KEEP_TASK(pedId, TRUE)
									bHeliPed1 = TRUE
									PRINTSTRING("bHeliPed1 = TRUE")
	                            ENDIF
							ENDIF
						ENDIF
                    ENDIF
					
					#IF FEATURE_ISLAND_BACKUP_HELI_SECOND_GUNNER
					IF NOT IS_NET_PED_INJURED(serverBD.sHeli.sPed[HELI_PED_GUNNER_TWO].netId)						
						IF bHeliPed2 = FALSE
							pedId = NET_TO_PED(serverBD.sHeli.sPed[HELI_PED_GUNNER_TWO].netId)
							IF CONTROL_CHECKS(serverBD.sHeli.netId)
		                        IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sHeli.sPed[HELI_PED_GUNNER_TWO].netId)
									CLEAR_PED_TASKS(pedId)
	                                SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedId, TRUE)
									SET_PED_KEEP_TASK(pedId, TRUE)
									bHeliPed2 = TRUE
									PRINTSTRING("bHeliPed2 = TRUE")
	                            ENDIF
							ENDIF
						ENDIF
                    ENDIF
					#ENDIF
					
				ENDIF
				
				// If heli or pilot are dead, kill crew.
				IF NOT IS_NET_VEHICLE_DRIVEABLE(serverBD.sHeli.netId)
				OR IS_NET_PED_INJURED(serverBD.sHeli.sPed[HELI_PED_PILOT].netId)
					KILL_HELI_CREW()
				ENDIF
				
			BREAK
			
			CASE eHELISTATE_DEAD
                    
                REMOVE_HELI_BLIP()
                    
            BREAK
            
    ENDSWITCH
    
ENDPROC

/// PURPOSE:
///    Server only function that checks to see if the game mode should now end. For this test script, it if time has passed.
/// RETURNS:
///    True if the the mode has been active for ROUND_DURATION 
FUNC BOOL HAVE_MISSION_END_CONDITIONS_BEEN_MET()
	
	#IF IS_DEBUG_BUILD
		IF bHostEndMissionNow
			RETURN TRUE
		ENDIF
	#ENDIF	
	
	RETURN FALSE
	
ENDFUNC
 
/// PURPOSE:
///    Helper function to get a clients game/mission state
/// PARAMS:
///    iPlayer - The player's game state you want.
/// RETURNS:
///    The game state.
FUNC INT GET_CLIENT_MISSION_STATE(INT iPlayer)
	RETURN playerBD[iPlayer].iGameState
ENDFUNC


/// PURPOSE:
///    Helper function to get the servers game/mission state
/// RETURNS:
///    The server game state.
FUNC INT GET_SERVER_MISSION_STATE()
	RETURN serverBD.iServerGameState
ENDFUNC

/// PURPOSE:
///    Processes security van script server logic.
PROC PROCESS_SERVER()
	
	PROCESS_HELI_BRAIN()
	
	IF serverBD.sHeli.eState = eHELISTATE_DEAD
		NET_PRINT("[ISLAND_BACKUP_HELI] - Backup Heli - heli is dead, initiating script termination.")NET_NL()
		serverBD.iServerGameState = GAME_STATE_END
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(serverBD.sHeli.stLeaveTimer)
		IF HAS_NET_TIMER_EXPIRED(serverBD.sHeli.stLeaveTimer, HELI_LEAVETIME)
			NET_PRINT("[ISLAND_BACKUP_HELI] - Backup Heli - heli has been leaving for 20 seconds, should be far enough away now for cleanup, initiating script termination.")NET_NL()
			serverBD.iServerGameState = GAME_STATE_END
		ENDIF
	ENDIF
	
	//Set to end state if player enters passive mode whilst airstrike is active.
	IF IS_MP_PASSIVE_MODE_ENABLED()
		PRINTLN("[ISLAND_BACKUP_HELI] - GAME_STATE END 1")
		serverBD.iServerGameState = GAME_STATE_END
	ENDIF	
	
	IF bOnMissionOnLaunch
		IF NOT NETWORK_IS_IN_TUTORIAL_SESSION()
			NET_PRINT("[ISLAND_BACKUP_HELI] - Backup Heli - was on NETWORK_IS_IN_TUTORIAL_SESSION() when heli was called, now not on mission, initiating script termination.")NET_NL()
			serverBD.iServerGameState = GAME_STATE_END
		ENDIF
	ELSE
		IF NETWORK_IS_IN_TUTORIAL_SESSION()
			NET_PRINT("[ISLAND_BACKUP_HELI] - Backup Heli - was not on NETWORK_IS_IN_TUTORIAL_SESSION() when heli was called, now on mission, initiating script termination.")NET_NL()
			serverBD.iServerGameState = GAME_STATE_END
		ENDIF
	ENDIF
	
	//2043884
	IF IS_CORONA_INITIALISING_A_QUICK_RESTART()
		NET_PRINT("[ISLAND_BACKUP_HELI] - Backup Heli - Doing a quick restart, kill script.")NET_NL()
		serverBD.iServerGameState = GAME_STATE_END
	ENDIF
	
ENDPROC

PROC MAINTAIN_LOCAL_HELI_BLIP()
	IF serverBD.sHeli.eState = eHELISTATE_GIVING_BACKUP
		CREATE_HELI_BLIP()
	ELSE
		REMOVE_HELI_BLIP()
	ENDIF
ENDPROC

/// PURPOSE:
///    Processes security van client logic.
PROC PROCESS_CLIENT()
	PROCESS_HELI_BODY()
	MAINTAIN_LOCAL_HELI_BLIP()
ENDPROC

/// PURPOSE:
///    Script cleanup.
PROC SCRIPT_CLEANUP()
	IF DOES_ENTITY_EXIST(mpglobals.sIslandBackupHeli.viPeds[HELI_PED_PILOT])
		DELETE_PED(mpglobals.sIslandBackupHeli.viPeds[HELI_PED_PILOT])
		NET_PRINT("[ISLAND_BACKUP_HELI] - Backup Heli - Deleted pilot")NET_NL()
	ENDIF
	
	IF DOES_ENTITY_EXIST(mpglobals.sIslandBackupHeli.viPeds[HELI_PED_GUNNER_ONE])
		DELETE_PED(mpglobals.sIslandBackupHeli.viPeds[HELI_PED_GUNNER_ONE])
		NET_PRINT("[ISLAND_BACKUP_HELI] - Backup Heli - Deleted gunner one")NET_NL()
	ENDIF
	
	#IF FEATURE_ISLAND_BACKUP_HELI_SECOND_GUNNER
	IF DOES_ENTITY_EXIST(mpglobals.sIslandBackupHeli.viPeds[HELI_PED_GUNNER_TWO])
		DELETE_PED(mpglobals.sIslandBackupHeli.viPeds[HELI_PED_GUNNER_TWO])
		NET_PRINT("[ISLAND_BACKUP_HELI] - Backup Heli - Deleted gunner two")NET_NL()
	ENDIF
	#ENDIF
	
	IF DOES_ENTITY_EXIST(mpglobals.sIslandBackupHeli.viHeli)
		DELETE_VEHICLE(mpglobals.sIslandBackupHeli.viHeli)
		NET_PRINT("[ISLAND_BACKUP_HELI] - Backup Heli - Deleted heli")NET_NL()
	ENDIF
	
	mpglobals.sIslandBackupHeli.fDamageByMe = 0.0
	mpGlobals.sIslandBackupHeli.bIKilledABackupHeliEntity = FALSE
	mpGlobals.sIslandBackupHeli.iBackupHeliKillCount = 0
	_HEIST_ISLAND__GLOBAL_SET_TERMINATE_ISLAND_BACKUP_HELI(FALSE)
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()  // call this to terminate your script.
ENDPROC	

/// PURPOSE:
///    Widgets and debug.
#IF IS_DEBUG_BUILD
	
	BOOL bKillHeli
	BOOL bCancelHeli
	BOOL bUpdateHealth
	BOOL bKillScript
	
	/// PURPOSE:
	///    Creates widgets.
	PROC CREATE_WIDGETS()
		
		START_WIDGET_GROUP("AM_ISLAND_BACKUP_HELI")
			ADD_WIDGET_BOOL("Cancel Backup", bCancelHeli)
			ADD_WIDGET_BOOL("Kill Heli", bKillHeli)
			ADD_WIDGET_FLOAT_SLIDER("Heli Min Hover Height", g_sMPTunables.fIslandBackupHeliMinHeight, 0.0, 300.0, 1.0)
			ADD_WIDGET_INT_SLIDER("Heli Lifespan (MS)",  g_sMPTunables.iIslandBackupHeliLifespanMS, 0, 9999999, 1000)
			ADD_WIDGET_INT_SLIDER("Heli Health", g_sMPTunables.iIslandBackupHeliHealth, 1, 9999, 10)
			ADD_WIDGET_BOOL("Reset Heli Health", bUpdateHealth)
			ADD_WIDGET_BOOL("Kill Script", bKillScript)
		STOP_WIDGET_GROUP()
		
	ENDPROC		
	
	/// PURPOSE:
	///    Updates widgets.
	PROC UPDATE_WIDGETS()		
		
		IF bKillScript
			SCRIPT_CLEANUP()
			bKillScript = FALSE
			EXIT
		ENDIF
		
		IF bKillHeli
			IF IS_NET_VEHICLE_DRIVEABLE(serverBD.sHeli.netId)
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sHeli.netId)
					SET_ENTITY_HEALTH(NET_TO_VEH(serverBD.sHeli.netId), 0)
				ELSE
					NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.sHeli.netId)
				ENDIF
			ELSE
				bKillHeli = FALSE
			ENDIF
		ENDIF
		
		IF bUpdateHealth
			IF IS_NET_VEHICLE_DRIVEABLE(serverBD.sHeli.netId)
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sHeli.netId)
					SET_ENTITY_HEALTH(NET_TO_VEH(serverBD.sHeli.netId), g_sMPTunables.iIslandBackupHeliHealth)
				ELSE
					NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.sHeli.netId)
				ENDIF
			ELSE
				bUpdateHealth = FALSE
			ENDIF
		ENDIF
		
		IF bCancelHeli
			HEIST_ISLAND_BACKUP_HELI__CANCEL_BACKUP()
			bCancelHeli = FALSE
		ENDIF
		
	ENDPROC

#ENDIF



/// PURPOSE:
///    Do necessary pre game start ini.
PROC PROCESS_PRE_GAME(MP_MISSION_DATA missionScriptArgs)
	_HEIST_ISLAND__GLOBAL_SET_TERMINATE_ISLAND_BACKUP_HELI(FALSE)
	
	// This marks the script as a net script, and handles any instancing setup. 
	SETUP_MP_MISSION_FOR_NETWORK(NUM_NETWORK_PLAYERS,  missionScriptArgs )
	
	RESERVE_NETWORK_MISSION_PEDS(COUNT_OF(HELI_PEDS))
	RESERVE_NETWORK_MISSION_VEHICLES(1)
	
	// This makes sure the net script is active, waits until it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
	
	// This script will not be paused if another script calls PAUSE_GAME
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	
	// KGM: Wait for the first network broadcast before moving on - call this as the last instruction in pre-game
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		NET_PRINT("[ISLAND_BACKUP_HELI] - Backup Heli - Failed to receive initial network broadcast. Cleaning up.")NET_NL()
		SCRIPT_CLEANUP()
	ENDIF
	
	IF NETWORK_IS_IN_TUTORIAL_SESSION()
		bOnMissionOnLaunch = TRUE
	ENDIF
	
	piLastProtectedPlayer = PLAYER_ID()
	playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_INI	
ENDPROC


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                      Main Loop                             ////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///    

#ENDIF // FEATURE_HEIST_ISLAND

SCRIPT(MP_MISSION_DATA missionScriptArgs)
	#IF FEATURE_HEIST_ISLAND
	#IF IS_DEBUG_BUILD	
		NET_PRINT_TIME() NET_PRINT_THREAD_ID() NET_PRINT("[ISLAND_BACKUP_HELI] Starting AM_ISLAND_BACKUP_HELI \n") NET_NL()
	#ENDIF
	
	// Carry out all the initial game starting duties. 
	IF NETWORK_IS_GAME_IN_PROGRESS()
		
		PROCESS_PRE_GAME(missionScriptArgs)	

		// Setup widgets. Make sure this is done after PROCESS_PRE_GAME()
		#IF IS_DEBUG_BUILD
			CREATE_WIDGETS()
		#ENDIF
		
	ENDIF
	
	// Main loop.
	WHILE TRUE

		// One wait to rule them all. This can be the ONLY wait from here on in.
		MP_LOOP_WAIT_ZERO() 
		
		// Initiate script profiler.
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		SCRIPT_PROFILER_START_OF_FRAME()
		#ENDIF
		#ENDIF
		
		
		// If we have a match end event, bail.
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
     		PRINTLN("[ISLAND_BACKUP_HELI] - Script Cleanup 1)")
			SCRIPT_CLEANUP()
		ENDIF	
		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			IF MPGlobalsAmbience.bTerminateIslandBackupHeli
				serverBD.bTerminate = TRUE
				IF NETWORK_GET_NUM_PARTICIPANTS() = 1
					PRINTLN("[ISLAND_BACKUP_HELI] - Script Cleanup - Called terminate")
					SCRIPT_CLEANUP()
				ENDIF
			ENDIF
		ELSE
			IF serverBD.bTerminate
				SCRIPT_CLEANUP()
				PRINTLN("[ISLAND_BACKUP_HELI] - Script Cleanup - Host terminated")
			ENDIF
		ENDIF
		
		IF NETWORK_IS_GAME_IN_PROGRESS()
			
			// Maintain height heli should fly at.
			MAINTAIN_HELI_MINIMUM_FLIGHT_HEIGHT()
			
			// Deal with the debug.
			#IF IS_DEBUG_BUILD
				UPDATE_WIDGETS()
			#ENDIF		
		
			// -----------------------------------
			// Process your game logic.....
			SWITCH GET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT())
							
				// Wait until the server gives the all go before moving on.
				CASE GAME_STATE_INI		
					IF GET_SERVER_MISSION_STATE() > GAME_STATE_INI	
						playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_RUNNING					
					ENDIF				
				BREAK
				
				// Main gameplay state.
				CASE GAME_STATE_RUNNING			
					
					PROCESS_CLIENT()
					
					// Look for the server say the mission has ended.
					IF GET_SERVER_MISSION_STATE() = GAME_STATE_END
						PRINTLN("[ISLAND_BACKUP_HELI] - GAME_STATE END 2")
						playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END			
					ENDIF	
					
				BREAK	
	
				CASE GAME_STATE_END	
					PRINTLN("[ISLAND_BACKUP_HELI] - Script Cleanup 2)")
					SCRIPT_CLEANUP()
				BREAK
				
			ENDSWITCH
			
			// -----------------------------------
			// Process server game logic		
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				
				SWITCH GET_SERVER_MISSION_STATE()
					
					CASE GAME_STATE_INI	
						serverBD.iServerGameState = GAME_STATE_RUNNING
					BREAK
					
					// Look for game end conditions.
					CASE GAME_STATE_RUNNING		
						
						// Process security van script server logic.
						PROCESS_SERVER()
						
						#IF IS_DEBUG_BUILD
							IF bHostEndMissionNow	
								PRINTLN("[ISLAND_BACKUP_HELI] - GAME_STATE END 3")
								serverBD.iServerGameState = GAME_STATE_END
							ENDIF
						#ENDIF	
						
					BREAK
					
					CASE GAME_STATE_END			
										
						// We'd want to upload scores here, show results, do whatever.
						
					BREAK	
					
				ENDSWITCH

			ENDIF
		
		ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
			SCRIPT_PROFILER_END_OF_FRAME()
		#ENDIF
		#ENDIF
		
		
	ENDWHILE
	#ENDIF // FEATURE_HEIST_ISLAND
	
	#IF NOT FEATURE_HEIST_ISLAND
		UNUSED_PARAMETER(missionScriptArgs)
		TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
	#ENDIF // NOT FEATURE_HEIST_ISLAND
	
// End of Mission
ENDSCRIPT
