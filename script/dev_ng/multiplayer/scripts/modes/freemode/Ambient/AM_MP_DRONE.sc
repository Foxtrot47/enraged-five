//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        AM_MP_DRONE.sc																						//
//																													//
// Description: Drone camera which would be attached to prop. 														//
//              Call START_DRONE to start a drone check out description in net_drone.sch							//
//																													//
// Drone types: DRONE_TYPE_HACKER_TRUCK, DRONE_TYPE_ARENA_WARS_ONE, DRONE_TYPE_ARENA_WARS_TWO,						//
//	   			DRONE_TYPE_ARENA_WARS_MISSILES																		//
//																													//
// Written by:  Ata																									//
// Date:  		22/01/2018																							//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_event.sch"
USING "net_include.sch"
USING "net_events.sch"
USING "net_spawn.sch"
USING "net_ambience.sch"
USING "net_wait_zero.sch"
USING "freemode_events_header.sch"
USING "net_common_functions.sch"
USING "turret_manager_public.sch"

USING "arena_box_seat_areas.sch"
USING "net_arena_wars_career.sch"

#IF IS_DEBUG_BUILD
USING "net_debug_log.sch"
#ENDIF

USING "net_interior_access_control_public.sch"
USING "net_drone.sch"
USING "script_weapon_common.sch"

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ DATA STRUCTURES ╞═════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

STRUCT ServerBroadcastData
	INT iServerBS
ENDSTRUCT

STRUCT PlayerBroadcastData
	INT iLocalBS
	DRONE_TYPE eType
	NETWORK_INDEX netID_remoteDrone
ENDSTRUCT

#IF IS_DEBUG_BUILD
STRUCT DroneDebug
	FLOAT fPropCamOffsetX, fPropCamOffsetY, fPropCamOffsetZ
	FLOAT fPropRotX, fPropRotY, fPropRotZ, fDebugBoost, fDebugUpDownSpeed
	BOOL bShowShapeTests, bPrintPitchRoll
	BOOL bResetTimer, byPassAllCoolDowns
	BOOL bDisplayDroneSpeed
	FLOAT fMissileVFXx, fMissileVFXy, fMissileVFXz, fMissileVFXScale
	FLOAT fMissileTurnSpeed, fMissileUpSpeed
	BOOL bResetMissileVFX, bResetMissileLanuchVFX, bActivateVFXDebug
	VECTOR vPreviousCoord
ENDSTRUCT
#ENDIF

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡    CONSTS    ╞════════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

// iLocalBS 
CONST_INT DRONE_LOCAL_BS_PROP_CREATED						0
CONST_INT DRONE_LOCAL_BS_CAMERA_FUZZ						1
CONST_INT DRONE_LOCAL_BS_CAMERA_EXPLOSION					2
CONST_INT DRONE_LOCAL_BS_CAMERA_EMP							3
CONST_INT DRONE_LOCAL_BS_CAMERA_STUN_GUN					4
CONST_INT DRONE_LOCAL_BS_CHARGING_EXPLOSIVE					5
CONST_INT DRONE_LOCAL_BS_RESERVED_FAKE_PED_INDEX			6
CONST_INT DRONE_LOCAL_BS_EXIT_DRONE							7
CONST_INT DRONE_LOCAL_BS_GET_DRONE_MINIMAP_DATA				8
CONST_INT DRONE_LOCAL_BS_DRONE_COLLIDE_WITH_VEH				9
CONST_INT DRONE_LOCAL_BS_RESET_STUN_CHARGER					10
CONST_INT DRONE_LOCAL_BS_AIMING_AT_PLAYER					11
CONST_INT DRONE_LOCAL_BS_ROTATE_X_AXIS						12
CONST_INT DRONE_LOCAL_BS_INIT_SOUND_WAVE_SCALEFORM			13
CONST_INT DRONE_LOCAL_BS_CHECK_ACTUAL_SPAWN_COORD			14
CONST_INT DRONE_LOCAL_BS_CHECK_OFFSET_SPAWN_COORD			15
CONST_INT DRONE_LOCAL_BS_START_INSTRUCTIONAL_PROMPT			16
CONST_INT DRONE_LOCAL_BS_SHOULD_SHOW_PC_PROMPTS				17
CONST_INT DRONE_LOCAL_BS_START_DRONE_LOAD_SCENE				18
CONST_INT DRONE_LOCAL_BS_FOUND_SAFE_SPAWN_POINT				19
CONST_INT DRONE_LOCAL_BS_FAKE_VEH_CREATION					20
CONST_INT DRONE_LOCAL_BS_BOOST_ACTIVATED					21
CONST_INT DRONE_LOCAL_BS_BOOST_DIR_FORWARD					22
CONST_INT DRONE_LOCAL_BS_AIMING_AT_PED						23
CONST_INT DRONE_LOCAL_BS_PLAYER_FIRED_STUN					24
CONST_INT DRONE_LOCAL_BS_DRONE_FREEZED						25
CONST_INT DRONE_LOCAL_BS_DRONE_TARGET_DAMAGE_RECEIVED		26
CONST_INT DRONE_LOCAL_BS_DRONE_COOL_DOWN_BROADCAST_FAILED	27
CONST_INT DRONE_LOCAL_BS_DRONE_COOL_DOWN_BROADCAST_WAIT 	28
CONST_INT DRONE_LOCAL_BS_TURN_ON_DRONE_COLLISION		 	29
CONST_INT DRONE_LOCAL_BS_RESERVE_OBJECT				 		30
CONST_INT DRONE_LOCAL_BS_DETONATE_RECHARGER_PAUSED	 		31

// iLocalBSTwo
CONST_INT DRONE_LOCAL_BS_TWO_START_TC				 		0
CONST_INT DRONE_LOCAL_BS_TWO_DETONATE_DRAINING				1 
CONST_INT DRONE_LOCAL_BS_TWO_MISSILE_CLEANUP_EXPLOSIVE		2
CONST_INT DRONE_LOCAL_BS_TWO_RESERVE_PHONE_OBJECT			3
CONST_INT DRONE_LOCAL_BS_TWO_PLAYER_FIRED_TRANQ_GUN			4
CONST_INT DRONE_LOCAL_BS_TWO_START_TRANQ_GUN_TIMER			5
#IF FEATURE_HEIST_ISLAND
CONST_INT DRONE_LOCAL_BS_TWO_TAKE_CONTROL_OF_MISSILE		6
#ENDIF

// PlayerBroadcastData iLocalBS
CONST_INT DRONE_PLAYER_BD_URING_DRONE						0

//Analogue stick axis indexes.
CONST_INT DRONE_LEFT_X										0
CONST_INT DRONE_LEFT_Y										1
CONST_INT DRONE_RIGHT_X										2
CONST_INT DRONE_RIGHT_Y										3

CONST_INT DRONE_CAMERA_ZOOM_SPEED							5

CONST_INT DRONE_CLEANUP_REASON_RETURNED_TO_PLAYER			0
CONST_INT DRONE_CLEANUP_REASON_COLLISION					1
CONST_INT DRONE_CLEANUP_REASON_DESTROYED_BY_ANOTHER_PLAYER	2
CONST_INT DRONE_CLEANUP_REASON_DROPPED_IN_WATER				3
CONST_INT DRONE_CLEANUP_REASON_DETROYED_BY_OWN_DETONATE		4
CONST_INT DRONE_CLEANUP_REASON_DISTANCE_LIMIT				5
CONST_INT DRONE_CLEANUP_REASON_PLAYER_INJURED				6
CONST_INT DRONE_CLEANUP_REASON_PLAYER_ANIM_INTERRUPTED		7

CONST_INT REMOTE_DRONE_START_LIMIT							700
CONST_INT REMOTE_DRONE_TERMINATE_LIMIT						710


CONST_FLOAT DRONE_INTERP_SPEED								40.0

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡    VARIABLES    ╞═════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]
ServerBroadcastData serverBD

DRONE_CONTROLS_STRUCT sDroneControl

#IF IS_DEBUG_BUILD
DroneDebug droneDebugStruct
#ENDIF

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡     CLEANUP     ╞══════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

PROC CLEANUP_DRONE_CAMERA()
	IF DOES_CAM_EXIST(sDroneControl.droneCamera)
		DESTROY_CAM(sDroneControl.droneCamera)
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		IF !IS_DRONE_FORCED_EXTERNAL_CLEAN_UP()
		OR IS_BIT_SET(sDroneControl.iLocalBSTwo, DRONE_LOCAL_BS_TWO_START_TC)
			CLEAR_TIMECYCLE_MODIFIER()
		ENDIF	
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
FUNC STRING GET_DRONE_STATE_NAME(DRONE_STATE eState)
	
	SWITCH eState
		CASE DRONE_STATE_IDLE 				RETURN "DRONE_STATE_IDLE" 
		CASE DRONE_STATE_INIT 				RETURN "DRONE_STATE_INIT" 
		CASE DRONE_STATE_FLYING				RETURN "DRONE_STATE_FLYING"
		CASE DRONE_STATE_START_ANIMATION	RETURN "DRONE_STATE_START_ANIMATION"
		CASE DRONE_STATE_FUZZ_CAM			RETURN "DRONE_STATE_FUZZ_CAM"
		CASE DRONE_STATE_CLEANUP			RETURN "DRONE_STATE_CLEANUP"
		CASE DRONE_STATE_REMOTE_DRONE		RETURN "DRONE_STATE_REMOTE_DRONE"
		#IF FEATURE_HEIST_ISLAND
		CASE DRONE_STATE_LAUNCH_VFX			RETURN "DRONE_STATE_LAUNCH_VFX"
		#ENDIF
	ENDSWITCH
	
	RETURN "INVALID STATE"
	
ENDFUNC

FUNC STRING GET_DRONE_SNAPMATIC_STATE_NAME(DRONE_SNAPMATIC_STATE eState)
	
	SWITCH eState
		CASE DRONE_SNAPMATIC_STATE_IDLE 		RETURN "DRONE_SNAPMATIC_STATE_IDLE" 
		CASE DRONE_SNAPMATIC_STATE_INIT 		RETURN "DRONE_SNAPMATIC_STATE_INIT" 
		CASE DRONE_SNAPMATIC_STATE_ACTIVE		RETURN "DRONE_SNAPMATIC_STATE_ACTIVE"
		CASE DRONE_SNAPMATIC_STATE_CLEANUP		RETURN "DRONE_SNAPMATIC_STATE_CLEANUP"
	ENDSWITCH
	
	RETURN "INVALID STATE"
	
ENDFUNC
#ENDIF

PROC SET_DRONE_STATE(DRONE_STATE eState)
	IF sDroneControl.eDroneState != eState
		sDroneControl.eDroneState = eState
		PRINTLN("[AM_MP_DRONE] - SET_DRONE_STATE: ", GET_DRONE_STATE_NAME(eState))
	ENDIF
ENDPROC

PROC SET_DRONE_SNAMPMATIC_STATE(DRONE_SNAPMATIC_STATE eState)
	IF sDroneControl.eDroneSnapmaticState != eState
		sDroneControl.eDroneSnapmaticState = eState
		PRINTLN("[AM_MP_DRONE] - SET_DRONE_SNAMPMATIC_STATE: ", GET_DRONE_SNAPMATIC_STATE_NAME(eState))
	ENDIF
ENDPROC

#IF FEATURE_HEIST_ISLAND
PROC SET_CAN_TAKE_CONTROL_OF_SUBMARINE_MISSILE(BOOL bTakeControl)
	IF NOT bTakeControl
		IF IS_BIT_SET(sDroneControl.iLocalBSTwo, DRONE_LOCAL_BS_TWO_TAKE_CONTROL_OF_MISSILE)
			CLEAR_BIT(sDroneControl.iLocalBSTwo, DRONE_LOCAL_BS_TWO_TAKE_CONTROL_OF_MISSILE)
			PRINTLN("[AM_MP_DRONE] - SET_CAN_TAKE_CONTROL_OF_SUBMARINE_MISSILE TRUE")
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(sDroneControl.iLocalBSTwo, DRONE_LOCAL_BS_TWO_TAKE_CONTROL_OF_MISSILE)
			SET_BIT(sDroneControl.iLocalBSTwo, DRONE_LOCAL_BS_TWO_TAKE_CONTROL_OF_MISSILE)
			PRINTLN("[AM_MP_DRONE] - SET_CAN_TAKE_CONTROL_OF_SUBMARINE_MISSILE FALSE")
		ENDIF
	ENDIF
ENDPROC	

FUNC BOOL CAN_TAKE_CONTROL_OF_SUBMARINE_MISSILE()
	RETURN IS_BIT_SET(sDroneControl.iLocalBSTwo, DRONE_LOCAL_BS_TWO_TAKE_CONTROL_OF_MISSILE)
ENDFUNC
#ENDIF

FUNC SCALEFORM_INDEX REQUEST_ARENA_WARS_MISSILE_SCALEFORM_MOVE()
	STRING sScaleformName = ""
	SWITCH g_FMMC_STRUCT.sArenaInfo.iArena_Theme
		CASE ARENA_THEME_DYSTOPIAN 			sScaleformName = "ARENA_GUN_CAM_APOCALYPSE"	BREAK
		CASE ARENA_THEME_SCIFI				sScaleformName = "ARENA_GUN_CAM_SCIFI"		BREAK
		CASE ARENA_THEME_CONSUMERWASTELAND 	sScaleformName = "ARENA_GUN_CAM_CONSUMER"	BREAK	
		DEFAULT 							sScaleformName = "INVALID_THEME" 			BREAK
	ENDSWITCH
	
	CDEBUG1LN(DEBUG_NET_DRONE, "[AM_MP_DRONE] - REQUEST_ARENA_WARS_MISSILE_SCALEFORM_MOVE Loading scaleform ", sScaleformName)
	RETURN REQUEST_SCALEFORM_MOVIE(sScaleformName)
ENDFUNC

FUNC SCALEFORM_INDEX REQUEST_SUBMARINE_MISSILE_SCALEFORM_MOVE()
	STRING sScaleformName = "SUBMARINE_MISSILES"
	CDEBUG1LN(DEBUG_NET_DRONE, "[AM_MP_DRONE] - REQUEST_ARENA_WARS_MISSILE_SCALEFORM_MOVE Loading scaleform ", sScaleformName)
	RETURN REQUEST_SCALEFORM_MOVIE(sScaleformName)
ENDFUNC

PROC INITIALISE_RECHARGER_BARS()
	IF NOT sDroneControl.bDroneEMPCoolDown
		sDroneControl.iStunRechargerFill 	= 100
	ENDIF
	
	IF IS_DRONE_STARTED_FROM_CASINO_HEIST_MISSON()
		sDroneControl.iTranqRechargerFill  	= 100
	ENDIF	

	sDroneControl.iDetonateFill 		= 0

	sDroneControl.iBoostFill 			= 100		
	
	sDroneControl.fBaseCamFov			= 90
	sDroneControl.fCamFOVCurrent		= 90
	
	IF NOT IS_DRONE_USED_AS_GUIDED_MISSILE()
		sDroneControl.droneStunSF 		= REQUEST_SCALEFORM_MOVIE("DRONE_CAM")
	ELSE
		#IF FEATURE_HEIST_ISLAND
		IF IS_DRONE_STARTED_FROM_SUBMARINE_MISSILE()
			sDroneControl.droneStunSF = REQUEST_SUBMARINE_MISSILE_SCALEFORM_MOVE()
		ELSE
		#ENDIF
			sDroneControl.droneStunSF = REQUEST_ARENA_WARS_MISSILE_SCALEFORM_MOVE()
		#IF FEATURE_HEIST_ISLAND
		ENDIF
		#ENDIF
	ENDIF	

	IF IS_DRONE_STARTED_FROM_ARENA_WARS_TWO()
		sDroneControl.sSoundSet				= "DLC_Arena_Drone_Sounds"
	ELIF IS_DRONE_STARTED_FROM_ARENA_WARS_ONE()
		sDroneControl.sSoundSet				= "DLC_Arena_Battle_Drone_Sounds"
	ELIF IS_DRONE_STARTED_FROM_ARENA_WARS_MISSILE()
		sDroneControl.sSoundSet				= "DLC_Arena_Piloted_Missile_Sounds"
	#IF FEATURE_HEIST_ISLAND
	ELIF IS_DRONE_STARTED_FROM_SUBMARINE_MISSILE()
		sDroneControl.sSoundSet				= "DLC_H4_Piloted_Missile_Sounds"
	#ENDIF
	ELSE
		sDroneControl.sSoundSet				= "DLC_BTL_Drone_Sounds"
	ENDIF	
ENDPROC

#IF FEATURE_GEN9_EXCLUSIVE
PROC SET_AUDIO_RAIN_HAPTIC_FLAG(BOOL bSet)
	IF bSet
		SET_AUDIO_FLAG("DisableRainHaptics", TRUE)
	ELSE
		SET_AUDIO_FLAG("DisableRainHaptics", FALSE)
	ENDIF
ENDPROC
#ENDIF

PROC STOP_SCANNER_SOUND()
	IF !HAS_SOUND_FINISHED(sDroneControl.iScannerLoop)
		STOP_SOUND(sDroneControl.iScannerLoop)
		RELEASE_SOUND_ID(sDroneControl.iScannerLoop)
		sDroneControl.iScannerLoop = -1
	ENDIF
ENDPROC

PROC STOP_SOUNDS()
	
	#IF FEATURE_GEN9_EXCLUSIVE
	SET_AUDIO_RAIN_HAPTIC_FLAG(FALSE)
	#ENDIF
	
	IF !HAS_SOUND_FINISHED(sDroneControl.iHudLoopSoundID)
		STOP_SOUND(sDroneControl.iHudLoopSoundID)
		RELEASE_SOUND_ID(sDroneControl.iHudLoopSoundID)
		sDroneControl.iHudLoopSoundID = -1
	ENDIF
	
	IF !HAS_SOUND_FINISHED(sDroneControl.iSoundID)
		STOP_SOUND(sDroneControl.iSoundID)
		RELEASE_SOUND_ID(sDroneControl.iSoundID)
		sDroneControl.iSoundID = -1
	ENDIF
	
	IF !HAS_SOUND_FINISHED(sDroneControl.iFlightLoopSoundID)
		STOP_SOUND(sDroneControl.iFlightLoopSoundID)
		RELEASE_SOUND_ID(sDroneControl.iFlightLoopSoundID)
		sDroneControl.iFlightLoopSoundID = -1
	ENDIF
	
	IF !HAS_SOUND_FINISHED(sDroneControl.iStunSoundID)
		STOP_SOUND(sDroneControl.iStunSoundID)
		RELEASE_SOUND_ID(sDroneControl.iStunSoundID)
		sDroneControl.iStunSoundID = -1
	ENDIF
	
	IF !HAS_SOUND_FINISHED(sDroneControl.iDetonateSoundID)
		STOP_SOUND(sDroneControl.iDetonateSoundID)
		RELEASE_SOUND_ID(sDroneControl.iDetonateSoundID)
		sDroneControl.iDetonateSoundID = -1
	ENDIF
	
	IF !HAS_SOUND_FINISHED(sDroneControl.iStaticSoundID)
		STOP_SOUND(sDroneControl.iStaticSoundID)
		RELEASE_SOUND_ID(sDroneControl.iStaticSoundID)
		sDroneControl.iStaticSoundID = -1
	ENDIF

	IF !HAS_SOUND_FINISHED(sDroneControl.iStartUpSound)
		STOP_SOUND(sDroneControl.iStartUpSound)
		RELEASE_SOUND_ID(sDroneControl.iStartUpSound)
		sDroneControl.iStartUpSound = -1
	ENDIF

	IF !HAS_SOUND_FINISHED(sDroneControl.iCamZoomSound)
		STOP_SOUND(sDroneControl.iCamZoomSound)
		RELEASE_SOUND_ID(sDroneControl.iCamZoomSound)
		sDroneControl.iCamZoomSound = -1
	ENDIF

	IF !HAS_SOUND_FINISHED(sDroneControl.iHudDisconnectSound)
		STOP_SOUND(sDroneControl.iHudDisconnectSound)
		RELEASE_SOUND_ID(sDroneControl.iHudDisconnectSound)
		sDroneControl.iHudDisconnectSound = -1
	ENDIF

	IF !HAS_SOUND_FINISHED(sDroneControl.iAlarmLoopSound)
		STOP_SOUND(sDroneControl.iAlarmLoopSound)
		RELEASE_SOUND_ID(sDroneControl.iAlarmLoopSound)
		sDroneControl.iAlarmLoopSound = -1
	ENDIF

	IF !HAS_SOUND_FINISHED(sDroneControl.iDestroyedSound)
		STOP_SOUND(sDroneControl.iDestroyedSound)
		RELEASE_SOUND_ID(sDroneControl.iDestroyedSound)
		sDroneControl.iDestroyedSound = -1
	ENDIF

	IF !HAS_SOUND_FINISHED(sDroneControl.iFireShock)
		STOP_SOUND(sDroneControl.iFireShock)
		RELEASE_SOUND_ID(sDroneControl.iFireShock)
		sDroneControl.iFireShock = -1
	ENDIF

	IF !HAS_SOUND_FINISHED(sDroneControl.iBoostLoopSoundID)
		STOP_SOUND(sDroneControl.iBoostLoopSoundID)
		RELEASE_SOUND_ID(sDroneControl.iBoostLoopSoundID)
		sDroneControl.iBoostLoopSoundID = -1
	ENDIF
	
	IF !HAS_SOUND_FINISHED(sDroneControl.iBoostRechargerSoundID)
		STOP_SOUND(sDroneControl.iBoostRechargerSoundID)
		RELEASE_SOUND_ID(sDroneControl.iBoostRechargerSoundID)
		sDroneControl.iBoostRechargerSoundID = -1
	ENDIF
	
	IF !HAS_SOUND_FINISHED(sDroneControl.iFireTranq)
		STOP_SOUND(sDroneControl.iFireTranq)
		RELEASE_SOUND_ID(sDroneControl.iFireTranq)
		sDroneControl.iFireTranq = -1
	ENDIF
	
	#IF FEATURE_HEIST_ISLAND
	IF !HAS_SOUND_FINISHED(sDroneControl.iMissileLaunchSound)
		STOP_SOUND(sDroneControl.iMissileLaunchSound)
		RELEASE_SOUND_ID(sDroneControl.iMissileLaunchSound)
		sDroneControl.iMissileLaunchSound = -1
	ENDIF
	#ENDIF
	
	IF IS_REMOTE_DRONE_STARTED()
		INT iNumSound
		FOR iNumSound = 0 TO NUM_NETWORK_PLAYERS - 1
			IF sDroneControl.iRemoteFlightLoopSoundID[iNumSound] != -1
				STOP_SOUND(sDroneControl.iRemoteFlightLoopSoundID[iNumSound])
				RELEASE_SOUND_ID(sDroneControl.iRemoteFlightLoopSoundID[iNumSound])
				sDroneControl.iRemoteFlightLoopSoundID[iNumSound] = -1
			ENDIF
		ENDFOR
	ENDIF
	
	STOP_SCANNER_SOUND()
	
	IF IS_AUDIO_SCENE_ACTIVE("DLC_BTL_Hacker_Drone_HUD_Scene")
		STOP_AUDIO_SCENE("DLC_BTL_Hacker_Drone_HUD_Scene")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("dlc_aw_arena_piloted_missile_scene")
		STOP_AUDIO_SCENE("dlc_aw_arena_piloted_missile_scene")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("DLC_Arena_Battle_Drone_HUD_Scene")
		STOP_AUDIO_SCENE("DLC_Arena_Battle_Drone_HUD_Scene")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("DLC_Arena_Spectator_Drone_HUD_Scene")
		STOP_AUDIO_SCENE("DLC_Arena_Spectator_Drone_HUD_Scene")
	ENDIF
	
	#IF FEATURE_HEIST_ISLAND
	IF IS_AUDIO_SCENE_ACTIVE("dlc_hei4_submarine_guided_missile_Scene")
		STOP_AUDIO_SCENE("dlc_hei4_submarine_guided_missile_Scene")
	ENDIF	
	#ENDIF
ENDPROC	

PROC SCALEFORM_SET_ELEMENT_VISIBLE(STRING sMethodName, BOOL bVisible)
	BEGIN_SCALEFORM_MOVIE_METHOD(sDroneControl.droneStunSF, sMethodName)
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bVisible)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC SCALEFORM_SET_ZOOM_LEVEL(INT iZoomIndex, STRING sTextLabel)
	BEGIN_SCALEFORM_MOVIE_METHOD(sDroneControl.droneStunSF, "SET_ZOOM_LABEL")
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iZoomIndex)
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sTextLabel)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

#IF IS_DEBUG_BUILD
PROC CREATE_DEBUG_WIDGET()
	START_WIDGET_GROUP("Drone")
		
		START_WIDGET_GROUP("Camera")
			ADD_WIDGET_FLOAT_SLIDER("Camera prop X offset Limit", droneDebugStruct.fPropCamOffsetX, -100, 100, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Camera prop Y offset Limit", droneDebugStruct.fPropCamOffsetY, -100, 100, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Camera prop Z offset Limit", droneDebugStruct.fPropCamOffsetZ, -100, 100, 0.1)
		STOP_WIDGET_GROUP()

		START_WIDGET_GROUP("Drone Pitch & Roll")
			ADD_WIDGET_FLOAT_SLIDER("Drone rotation x", droneDebugStruct.fPropRotX, -100, 100, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Drone rotation y", droneDebugStruct.fPropRotY, -100, 100, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Drone rotation z", droneDebugStruct.fPropRotZ, -100, 100, 0.1)
			ADD_WIDGET_BOOL("Print drone pitch & roll", droneDebugStruct.bPrintPitchRoll)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Collision")
			ADD_WIDGET_BOOL("Show shapetests", droneDebugStruct.bShowShapeTests)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Stun Gun Recharger bar")
			ADD_WIDGET_INT_SLIDER("Stun recharger", sDroneControl.iStunRechargerFill, 0, 100, 0)
			ADD_WIDGET_INT_SLIDER("Detonate recharger", sDroneControl.iDetonateFill, 0, 100, 0)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Drone Boost")
			ADD_WIDGET_FLOAT_SLIDER("Boost multiplier", droneDebugStruct.fDebugBoost, 0, 1000, 0.1)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Reset cool down timer")
			ADD_WIDGET_BOOL("Reset timer", droneDebugStruct.bResetTimer)
			ADD_WIDGET_BOOL("Bypass all cooldowns", droneDebugStruct.byPassAllCoolDowns)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Display Speed")
			ADD_WIDGET_BOOL("Display drone speed", droneDebugStruct.bDisplayDroneSpeed)
		STOP_WIDGET_GROUP()	
		START_WIDGET_GROUP("Modify Controls")
			ADD_WIDGET_FLOAT_SLIDER("General speed", sDroneControl.fDroneForwardSpeed, 0, 200, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Drone Turn speed", sDroneControl.fInterpSpeed, 0, 90, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Up/Down speed", droneDebugStruct.fDebugUpDownSpeed, 0, 200, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Missile Turn speed", droneDebugStruct.fMissileTurnSpeed, 0, 90, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Missile up speed", droneDebugStruct.fMissileUpSpeed, 0, 90, 0.1)
		STOP_WIDGET_GROUP()	
		START_WIDGET_GROUP("Missile VFX")
			ADD_WIDGET_BOOL("Activate  Missile VFX", droneDebugStruct.bActivateVFXDebug)
			ADD_WIDGET_FLOAT_SLIDER("VFX X", droneDebugStruct.fMissileVFXx, -200.0, 200.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("VFX Y", droneDebugStruct.fMissileVFXy, -200.0, 200.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("VFX Z", droneDebugStruct.fMissileVFXz, -200.0, 200.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("VFX Scale", droneDebugStruct.fMissileVFXScale, 0, 100, 0.1)
			ADD_WIDGET_BOOL("Reset Missile VFX", droneDebugStruct.bResetMissileVFX)
			ADD_WIDGET_BOOL("Reset Missile Launch VFX", droneDebugStruct.bResetMissileLanuchVFX)
		STOP_WIDGET_GROUP()		
		#IF SCRIPT_PROFILER_ACTIVE
		CREATE_SCRIPT_PROFILER_WIDGET()
		#ENDIF
	STOP_WIDGET_GROUP()
ENDPROC

FUNC STRING GET_DRONE_CLEANUP_REASON(INT iReason)
	
	SWITCH iReason
	 	CASE DRONE_CLEANUP_REASON_RETURNED_TO_PLAYER				RETURN "DRONE_CLEANUP_REASON_RETURNED_TO_PLAYER"			
	 	CASE DRONE_CLEANUP_REASON_COLLISION							RETURN "DRONE_CLEANUP_REASON_COLLISION"					
	 	CASE DRONE_CLEANUP_REASON_DESTROYED_BY_ANOTHER_PLAYER		RETURN "DRONE_CLEANUP_REASON_DESTROYED_BY_ANOTHER_PLAYER"	
	 	CASE DRONE_CLEANUP_REASON_DROPPED_IN_WATER					RETURN "DRONE_CLEANUP_REASON_DROPPED_IN_WATER"				
	 	CASE DRONE_CLEANUP_REASON_DETROYED_BY_OWN_DETONATE			RETURN "DRONE_CLEANUP_REASON_DETROYED_BY_OWN_DETONATE"		
	 	CASE DRONE_CLEANUP_REASON_DISTANCE_LIMIT					RETURN "DRONE_CLEANUP_REASON_DISTANCE_LIMIT"				
	ENDSWITCH
	
	RETURN ""
ENDFUNC 
#ENDIF

FUNC BOOL IS_REMOTE_DRONE_STARTED_FROM_TRUCK(PLAYER_INDEX remotePlayer)
	IF remotePlayer != INVALID_PLAYER_INDEX()
		IF playerBD[NATIVE_TO_INT(remotePlayer)].eType = DRONE_TYPE_HACKER_TRUCK
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_REMOTE_DRONE_STARTED_FROM_ARENA_WARS_ONE(PLAYER_INDEX remotePlayer)
	IF remotePlayer != INVALID_PLAYER_INDEX()
		IF playerBD[NATIVE_TO_INT(remotePlayer)].eType = DRONE_TYPE_ARENA_WARS_ONE
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_REMOTE_DRONE_STARTED_FROM_ARENA_WARS_TWO(PLAYER_INDEX remotePlayer)
	IF remotePlayer != INVALID_PLAYER_INDEX()
		IF playerBD[NATIVE_TO_INT(remotePlayer)].eType = DRONE_TYPE_ARENA_WARS_TWO
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC 

FUNC BOOL IS_REMOTE_DRONE_STARTED_FROM_ARENA_WARS_MISSILE(PLAYER_INDEX remotePlayer)
	IF remotePlayer != INVALID_PLAYER_INDEX()
		IF playerBD[NATIVE_TO_INT(remotePlayer)].eType = DRONE_TYPE_ARENA_WARS_MISSILES
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

#IF FEATURE_HEIST_ISLAND
FUNC BOOL IS_REMOTE_DRONE_STARTED_FROM_SUBMARINE_MISSILE(PLAYER_INDEX remotePlayer)
	IF remotePlayer != INVALID_PLAYER_INDEX()
		IF playerBD[NATIVE_TO_INT(remotePlayer)].eType = DRONE_TYPE_SUBMARINE_MISSILE
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC
#ENDIF

PROC INITIALISE(BOOL bCreateWidget = TRUE, BOOL bGrabInitialCoord = TRUE)
	sDroneControl.fDroneForwardSpeed		= g_sMPTunables.fBB_TERRORBYTE_DRONE_FORWARD_SPEED
	sDroneControl.fInterpSpeed 				= DRONE_INTERP_SPEED
	
	#IF FEATURE_GEN9_RELEASE
	IF IS_PROSPERO_VERSION()
		sDroneControl.fControlShakeMultiplier   = 0.5
	ENDIF
	#ENDIF
	
	IF IS_DRONE_STARTED_FROM_ARENA_WARS_TWO()
	OR IS_DRONE_STARTED_FROM_ARENA_WARS_ONE()
		sDroneControl.fDroneForwardSpeed 	*= 4.5
	ELIF IS_DRONE_STARTED_FROM_ARENA_WARS_MISSILE()
		sDroneControl.fDroneForwardSpeed 	= 100.0
		sDroneControl.fInterpSpeed			= 26
	ELIF IS_DRONE_STARTED_FROM_HEIST_FREEMODE()
		sDroneControl.fDroneForwardSpeed 	= 100.0	
		sDroneControl.fInterpSpeed 			= 70.0
	ELIF IS_DRONE_STARTED_FROM_CASINO_HEIST_MISSON()
		sDroneControl.fDroneForwardSpeed 	= 10.0	
		sDroneControl.fInterpSpeed 			= 35.0
	#IF FEATURE_HEIST_ISLAND
	ELIF IS_DRONE_STARTED_FROM_SUBMARINE_MISSILE()
		sDroneControl.fDroneForwardSpeed 	= 149.0	
		sDroneControl.fInterpSpeed			= 16
	#ENDIF
	ENDIF

	IF IS_DRONE_STARTED_FROM_ARENA_WARS_TWO()
		sDroneControl.dronePropModel 		= INT_TO_ENUM(MODEL_NAMES, HASH("xs_Prop_Arena_Drone_01"))
	ELIF IS_DRONE_STARTED_FROM_ARENA_WARS_ONE()
		sDroneControl.dronePropModel 		= INT_TO_ENUM(MODEL_NAMES, HASH("xs_Prop_Arena_Drone_02"))
	ELIF IS_DRONE_STARTED_FROM_ARENA_WARS_MISSILE()
		sDroneControl.dronePropModel 		= INT_TO_ENUM(MODEL_NAMES, HASH("xs_Prop_Arena_AirMissile_01a"))
		ENABLE_DRONE_INSTANT_COLLISION(TRUE)
	ELIF IS_DRONE_STARTED_FROM_HEIST_FREEMODE()
		IF NOT SHOULD_START_DRONE_FROM_PLAYER_PHONE()
			ARCADE_CAB_MANAGER_SLOT eCabinetSlot = INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, GET_ARCADE_CABINET_LOCATE_DISPLAY_SLOT_PLAYER_IS_IN(PLAYER_ID()))
			SWITCH eCabinetSlot
				CASE ACM_SLOT_38 		sDroneControl.dronePropModel 		=	INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Arcade_Drone_01d"))			BREAK
				CASE ACM_SLOT_39 		sDroneControl.dronePropModel 		=	INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Arcade_Drone_01a"))			BREAK
				CASE ACM_SLOT_40 		sDroneControl.dronePropModel 		=	INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Arcade_Drone_01c"))			BREAK
				CASE ACM_SLOT_41 		sDroneControl.dronePropModel 		=	INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Arcade_Drone_01b"))			BREAK
			ENDSWITCH
		ELSE
			sDroneControl.dronePropModel 		= INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Casino_Drone_01a"))
		ENDIF	
	ELIF IS_DRONE_STARTED_FROM_CASINO_HEIST_MISSON()
		sDroneControl.dronePropModel 		= INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Casino_Drone_01a"))
	#IF FEATURE_COPS_N_CROOKS
	ELIF IS_DRONE_STARTED_FROM_ARCADE_COP()
		sDroneControl.dronePropModel 		= INT_TO_ENUM(MODEL_NAMES, HASH("ac_Prop_Ch_Cops_Drones_01a"))
	#ENDIF	
	#IF FEATURE_HEIST_ISLAND
	ELIF IS_DRONE_STARTED_FROM_SUBMARINE_MISSILE()
		sDroneControl.dronePropModel 		= INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_h4_AirMissile_01a"))
		ENABLE_DRONE_INSTANT_COLLISION(TRUE)
	#ENDIF
	ELSE
		sDroneControl.dronePropModel 		= INT_TO_ENUM(MODEL_NAMES, HASH("ba_prop_battle_drone_quad"))
	ENDIF
	
	IF bGrabInitialCoord
		sDroneControl.vDroneStartCoord 		= sDroneControl.vDroneInitialStartCoord 
	ENDIF
	
	g_sDroneGlobals.iDroneCollisionTime		= 1500
	
	INITIALISE_RECHARGER_BARS()
	
	// Activate drone blip 
	ACTIVATE_DRONE_RADAR(TRUE)
	
	// Disbale hover for now!
	DISABLE_DRONE_HOVER_MODE(TRUE)
	
	IF IS_DRONE_STARTED_FROM_ARENA_WARS_TWO()
	OR IS_DRONE_USED_AS_GUIDED_MISSILE()
		DISABLE_DRONE_WEAPONS(TRUE)
	ELIF IS_DRONE_STARTED_FROM_ARENA_WARS_ONE()
		DISABLE_DRONE_WEAPONS(FALSE)
	ENDIF
	
	IF SHOULD_START_DRONE_FROM_PLAYER_PHONE()
		sDroneControl.sAnimDicName = "ANIM_HEIST@HS3F@IG4_DRONE@MALE@"
		REQUEST_ANIM_DICT(sDroneControl.sAnimDicName)
	ENDIF

	IF IS_DRONE_STARTED_FROM_ARENA_WARS_ONE()
		REQUEST_NAMED_PTFX_ASSET("scr_xs_dr")
	ENDIF	
	
	IF SHOULD_START_DRONE_FROM_PLAYER_PHONE()
		SET_DRONE_DISTANCE_LIMIT(g_sMPTunables.fCH_NANO_DRONE_DISTANCE_LIMIT)
	ELIF IS_DRONE_STARTED_FROM_HEIST_FREEMODE()
	AND NOT SHOULD_START_DRONE_FROM_PLAYER_PHONE()
		SET_DRONE_DISTANCE_LIMIT(g_sMPTunables.fCH_ARCADE_DRONE_DISTANCE_LIMIT)	
	#IF FEATURE_HEIST_ISLAND
	ELIF IS_DRONE_STARTED_FROM_SUBMARINE_MISSILE()
		SET_DRONE_DISTANCE_LIMIT(TO_FLOAT(g_sMPTunables.iIH_SUBMARINE_MISSILES_DISTANCE))	
		
		INT iNumSound
		FOR iNumSound = 0 TO NUM_NETWORK_PLAYERS - 1
			sDroneControl.iRemoteFlightLoopSoundID[iNumSound] = -1
		ENDFOR
	#ENDIF	
	ELSE
		IF !IS_DRONE_STARTED_FROM_TRUCK()
		AND !IS_DRONE_STARTED_FROM_DEBUG()
			SET_DRONE_DISTANCE_LIMIT(200.0)
		ENDIF
	ENDIF
	
	SET_DRONE_RELATIONSHIP_GROUP(rgFM_AiLike)
	
	IF bCreateWidget
		#IF IS_DEBUG_BUILD
		CREATE_DEBUG_WIDGET()
		#ENDIF
	ENDIF	
ENDPROC

PROC INITIALISE_REMOTE_PLAYER_DRONE()
	INT iNumSound
	FOR iNumSound = 0 TO NUM_NETWORK_PLAYERS - 1
		sDroneControl.iRemoteFlightLoopSoundID[iNumSound] = -1
	ENDFOR
	
	SET_DRONE_STATE(DRONE_STATE_REMOTE_DRONE)
ENDPROC 

FUNC BOOL IS_DRONE_SNAPMATIC_STATE(DRONE_SNAPMATIC_STATE eState)
	RETURN sDroneControl.eDroneSnapmaticState = eState
ENDFUNC

PROC PROCESS_SNAPMATIC_CLEANUP_STATE()
	HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
	IF NOT IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
		DO_SCREEN_FADE_IN(500)
	ENDIF	
	SET_EXIT_DRONE_SNAPMATIC(FALSE)
	SET_START_EXIT_DRONE_SNAPMATIC(FALSE)
	SET_DRONE_SNAMPMATIC_STATE(DRONE_SNAPMATIC_STATE_IDLE)
ENDPROC

FUNC BOOL SHOULD_TURN_ON_PLAYER_CONTROL_ON_DRONE_CLEANUP()
	IF !IS_SKYSWOOP_AT_GROUND()
		PRINTLN("[AM_MP_DRONE] - SHOULD_TURN_ON_PLAYER_CONTROL_ON_DRONE_CLEANUP - IS_SKYSWOOP_AT_GROUND false return false")
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
		PRINTLN("[AM_MP_DRONE] - SHOULD_TURN_ON_PLAYER_CONTROL_ON_DRONE_CLEANUP - player walking in/out return false")
		RETURN FALSE
	ENDIF
	
	IF !IS_LOCAL_PLAYER_USING_DRONE()
		PRINTLN("[AM_MP_DRONE] - SHOULD_TURN_ON_PLAYER_CONTROL_ON_DRONE_CLEANUP - IS_LOCAL_PLAYER_USING_DRONE false")
		RETURN FALSE
	ENDIF
	
	IF IS_DRONE_USED_AS_GUIDED_MISSILE()
	#IF IS_DEBUG_BUILD
	AND NETWORK_IS_ACTIVITY_SESSION()
	#ENDIF
		PRINTLN("[AM_MP_DRONE] - SHOULD_TURN_ON_PLAYER_CONTROL_ON_DRONE_CLEANUP - player is using missile drone return false")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Stops the current sync scene
/// PARAMS:
///    bClearPedTasks - clear ped tasks if synced scene is not active 
PROC STOP_CURRENT_DRONE_SYNCHED_SCENE(BOOL bClearPedTasks = FALSE)
	INT iLocalSceneID
	iLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iLocalSceneID)
	IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSceneID)
		DETACH_SYNCHRONIZED_SCENE(iLocalSceneID)
		NETWORK_STOP_SYNCHRONISED_SCENE(iLocalSceneID)
		iLocalSceneID = -1
		#IF IS_DEBUG_BUILD
		PRINTLN("[AM_MP_DRONE] - STOP_CURRENT_DRONE_SYNCHED_SCENE - called")
		DEBUG_PRINTCALLSTACK()
		#ENDIF
	ELSE
		IF bClearPedTasks
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
				CLEAR_PED_TASKS(PLAYER_PED_ID())
				PRINTLN("[AM_MP_DRONE] - STOP_CURRENT_DRONE_SYNCHED_SCENE - CLEAR_PED_TASKS")
			ENDIF	
		ENDIF	
	ENDIF
ENDPROC

FUNC FLOAT GET_DRONE_MISSILE_EXPLOSION_RADIUS()
	#IF FEATURE_HEIST_ISLAND
	IF IS_DRONE_STARTED_FROM_SUBMARINE_MISSILE()
		RETURN 1.0
	ENDIF
	#ENDIF
	
	RETURN 0.5
ENDFUNC

FUNC VECTOR GET_DRONE_MISSILE_EXPLOSION_COORD()
	#IF FEATURE_HEIST_ISLAND
	IF IS_DRONE_STARTED_FROM_SUBMARINE_MISSILE()
		RETURN GET_CAM_COORD(sDroneControl.droneCamera)
	ENDIF
	#ENDIF
	
	RETURN sDroneControl.vDroneCoords
ENDFUNC

FUNC EXPLOSION_TAG GET_DRONE_EXPLOSION_TAG()
	#IF FEATURE_HEIST_ISLAND
	IF IS_DRONE_STARTED_FROM_SUBMARINE_MISSILE()
		RETURN EXP_TAG_SCRIPT_MISSILE_LARGE
	ENDIF
	#ENDIF

	RETURN EXP_TAG_SCRIPT_MISSILE
ENDFUNC

PROC UPDATE_SIMPLE_INTERIOR_BLIPS()
	IF DOES_CAM_EXIST(sDroneControl.droneCamera)
		IF IS_PLAYER_IN_ANY_SIMPLE_INTERIOR(PLAYER_ID())
			FORCE_SIMPLE_INTERIOR_BLIPS_UPDATE()
		ENDIF
	ENDIF
ENDPROC

PROC LOCAL_DRONE_SCRIPT_CLEANUP(BOOL bReset = FALSE)
	
	PRINTLN("[AM_MP_DRONE] - LOCAL_DRONE_SCRIPT_CLEANUP - bReset: ", bReset)
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sDroneControl.cameraProp)
		SET_NETWORK_ID_CAN_MIGRATE(sDroneControl.cameraProp, TRUE)
		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(sDroneControl.cameraProp)
			
			IF !IS_BIT_SET(sDroneControl.iLocalBSTwo, DRONE_LOCAL_BS_TWO_MISSILE_CLEANUP_EXPLOSIVE)
				IF IS_DRONE_USED_AS_GUIDED_MISSILE()
					IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
						ADD_OWNED_EXPLOSION(PLAYER_PED_ID(), GET_DRONE_MISSILE_EXPLOSION_COORD(), GET_DRONE_EXPLOSION_TAG(), GET_DRONE_MISSILE_EXPLOSION_RADIUS())
					ELSE
						ADD_EXPLOSION(GET_DRONE_MISSILE_EXPLOSION_COORD(), GET_DRONE_EXPLOSION_TAG(), GET_DRONE_MISSILE_EXPLOSION_RADIUS())
					ENDIF
					SET_CONTROL_SHAKE(PLAYER_CONTROL, 300, ROUND(200 * sDroneControl.fControlShakeMultiplier))

					SET_BIT(sDroneControl.iLocalBSTwo, DRONE_LOCAL_BS_TWO_MISSILE_CLEANUP_EXPLOSIVE)
					PRINTLN("[AM_MP_DRONE] - LOCAL_DRONE_SCRIPT_CLEANUP adding explosive on cleanup")
				ENDIF	
			ENDIF
			
			CLEAR_FOCUS()
			
			OBJECT_INDEX cameraProp = NET_TO_OBJ(sDroneControl.cameraProp)
			NETWORK_FADE_OUT_ENTITY(cameraProp, FALSE, TRUE)
			DELETE_OBJECT(cameraProp)
		ELSE
			NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(sDroneControl.cameraProp)
			EXIT
		ENDIF
	ENDIF
	
	IF IS_DRONE_STARTED_FROM_HEIST_FREEMODE()
	AND NOT SHOULD_START_DRONE_FROM_PLAYER_PHONE()
		ARCADE_CABINET_ANIM_EVENT_STRUCT sArcadeCabinetAnimEvent
		sArcadeCabinetAnimEvent.bHoldLastFrame = FALSE
		sArcadeCabinetAnimEvent.bLoopAnim = FALSE
		PLAY_ARCADE_CABINET_ANIMATION(sArcadeCabinetAnimEvent, ARCADE_CABINET_ANIM_CLIP_EXIT)
	ENDIF
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DRONE_TRIG")
		CLEAR_HELP()
	ENDIF

	IF IS_ENTITY_ALIVE(sDroneControl.fakePed)
		PED_INDEX fakeped = sDroneControl.fakePed
		DELETE_PED(fakeped)
	ENDIF	
	
	IF DOES_ENTITY_EXIST(sDroneControl.targetObject)
		DELETE_OBJECT(sDroneControl.targetObject)
	ENDIF
		
	IF DOES_BLIP_EXIST(sDroneControl.droneBlip)
		REMOVE_BLIP(sDroneControl.droneBlip)
	ENDIF
	
	IF DOES_BLIP_EXIST(sDroneControl.playerBlip)
		REMOVE_BLIP(sDroneControl.playerBlip)
	ENDIF
	
	IF DOES_ENTITY_EXIST(sDroneControl.fakeVeh)
		DELETE_VEHICLE(sDroneControl.fakeVeh)
	ENDIF	
	
	THEFEED_SHOW()
	UNLOCK_MINIMAP_ANGLE() 
	UNLOCK_MINIMAP_POSITION()
	SET_RADAR_ZOOM_PRECISE(0)
	
	IF IS_RADAR_MAP_DISABLED()
		DISABLE_RADAR_MAP(FALSE)
	ENDIF
	
	UPDATE_SIMPLE_INTERIOR_BLIPS()
	CLEANUP_MENU_ASSETS(TRUE)
	CLEANUP_DRONE_CAMERA()
	
	IF SHOULD_START_DRONE_FROM_PLAYER_PHONE()
		STOP_CURRENT_DRONE_SYNCHED_SCENE()
	ENDIF	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sDroneControl.phoneObject)
		OBJECT_INDEX phoneObject = NET_TO_OBJ(sDroneControl.phoneObject)
		DELETE_OBJECT(phoneObject)
	ENDIF
	IF !bReset
		IF !IS_STRING_NULL_OR_EMPTY(sDroneControl.sAnimDicName)
			REMOVE_ANIM_DICT(sDroneControl.sAnimDicName)
		ENDIF	
		SET_START_DRONE_FROM_PLAYER_PHONE(FALSE)
	ENDIF	

	SET_BOSS_PROXIMITY_BLIP_AS_DISABLED(FALSE)
	
	IF SHOULD_TURN_ON_PLAYER_CONTROL_ON_DRONE_CLEANUP()
	AND (NOT IS_PLAYER_SPECTATING(PLAYER_ID()) OR IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE())
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
	ENDIF
	
	sDroneControl.fDistanceBetDroneAndControl 		= 0
	sDroneControl.fHeightDistanceBetDroneGround	= 0
	
	sDroneControl.iDroneCollisionHitSomthing	= DRONE_CAM_AREA_CHECK_INIT
	sDroneControl.iDronePedDetecHitSomthing 	= DRONE_CAM_AREA_CHECK_INIT
	sDroneControl.iDroneHoverHitSomthing 		= DRONE_CAM_AREA_CHECK_INIT
	sDroneControl.iDroneSpawnHitSomthing 		= DRONE_CAM_AREA_CHECK_INIT
	
	RESET_NET_TIMER(sDroneControl.sFuzzCycleTimer)
	RESET_NET_TIMER(sDroneControl.sCollisionTimer)
	IF NOT sDroneControl.bDroneEMPCoolDown
		RESET_NET_TIMER(sDroneControl.sStunGunTimer)
	ENDIF
	RESET_NET_TIMER(sDroneControl.iBoostChargerTimer)
	RESET_NET_TIMER(sDroneControl.sBoostTimer)
	RESET_NET_TIMER(sDroneControl.sMissionDronKillTimer)
	RESET_NET_TIMER(sDroneControl.sRelshipGroupTimer)
	RESET_NET_TIMER(sDroneControl.sShockEventTimer)
	RESET_NET_TIMER(sDroneControl.sDroneBroadcastWaitTimer)
	RESET_NET_TIMER(sDroneControl.sDroneBroadcastTimer)
	RESET_NET_TIMER(sDroneControl.sDroneSnapMaticStartTimer)
	RESET_NET_TIMER(sDroneControl.sMissileDroneCollision)
	RESET_NET_TIMER(sDroneControl.sCameraGridFailtimer)
	RESET_NET_TIMER(sDroneControl.sSelectorTimer)
	RESET_NET_TIMER(sDroneControl.sPCZoomDelay)
	RESET_NET_TIMER(sDroneControl.sTranqGunTimer)
	RESET_NET_TIMER(sDroneControl.sAirSpaceTimer)
	#IF FEATURE_HEIST_ISLAND
	IF PLAYER_ID() != INVALID_PLAYER_INDEX()
		RESET_NET_TIMER(sDroneControl.vMissileLaunchVFXTimer[NATIVE_TO_INT(PLAYER_ID())])
	ENDIF	
	#ENDIF
	
	sDroneControl.iDroneShockEvent 			= 0
	sDroneControl.iLocalBS 					= 0
	sDroneControl.iLocalBSTwo				= 0
	g_sDroneGlobals.iDroneRelationShipGroup = -1
	
	OVERRIDE_DRONE_MINIMAP_ZOOM(-1)
	SET_DRONE_DISTANCE_LIMIT(0)
	HIDE_ALL_SHOP_BLIPS(FALSE)
	ACTIVATE_DRONE_RADAR(FALSE)
	SET_KILL_DRONE_SCRIPT(FALSE)
	DISABLE_DRONE_WEAPONS(FALSE)
	RETURN_DRONE_TO_PLAYER(FALSE)
	SHOW_DRONE_TICKER(FALSE)
	SET_DRONE_TARGET_HIT(FALSE)
	SET_KEEP_INTERIOR_RUNNING_FOR_DRONE(FALSE)
	SET_IGNORE_DISTANCE_BETWEEN_PLAYER_AND_DRONE_CHECK(FALSE)
	RETURN_DRONE_TO_PLAYER_AND_KILL_SCRIPT(FALSE)
	HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
	SET_EXIT_DRONE_SNAPMATIC(FALSE)
	SET_START_EXIT_DRONE_SNAPMATIC(FALSE)
	SET_DRONE_SNAPMATIC_ACTIVE(FALSE)
	ENABLE_DRONE_INSTANT_COLLISION(FALSE)
	
	SET_DRONE_COORD_AND_ROTATION(<<0,0,0>>, <<0,0,0>>)
	
	SHOW_ALL_DRONE_UI()

	g_sDroneGlobals.droneInterior = INT_TO_NATIVE(INTERIOR_INSTANCE_INDEX, -1)
	
	CLEAR_BIT(g_iArenaSpectatorBS, GLOBAL_ARENA_SPECTATOR_BOX_BS_LAUNCH_DRONE)
	CLEAR_BIT(g_iSpecialSpectatorBS2, g_ciSpecial_Spectator_BS2_Drone_Initializing)
	
	SET_DRONE_STATE(DRONE_STATE_IDLE)
	
	IF !IS_DRONE_SNAPMATIC_STATE(DRONE_SNAPMATIC_STATE_IDLE)
		PROCESS_SNAPMATIC_CLEANUP_STATE()
	ENDIF
	
	#IF IS_DEBUG_BUILD
	g_bStartDroneScript = FALSE
	g_bCleanupDroneScript = FALSE
	#ENDIF
	
	IF IS_LOCAL_PLAYER_USING_DRONE()	
		SCALEFORM_SET_ELEMENT_VISIBLE("SET_WARNING_IS_VISIBLE", FALSE)
		
		// Broadcast drone usage for cool down timer 
		IF IS_DRONE_STARTED_FROM_TRUCK()
		#IF FEATURE_HEIST_ISLAND
		OR IS_DRONE_STARTED_FROM_SUBMARINE_MISSILE()
		#ENDIF
			PLAYER_INDEX pPropertyOwner 
			IF GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner != INVALID_PLAYER_INDEX()
			AND NETWORK_IS_PLAYER_ACTIVE(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner)
				pPropertyOwner = GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner 
				PRINTLN("[AM_MP_DRONE] - LOCAL_DRONE_SCRIPT_CLEANUP pPropertyOwner = GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner: ", GET_PLAYER_NAME(pPropertyOwner))
			ELSE
				IF IS_DRONE_STARTED_FROM_TRUCK()
				AND g_OwnerOfHackerTruckPropertyIAmIn != INVALID_PLAYER_INDEX()
				AND NETWORK_IS_PLAYER_ACTIVE(g_OwnerOfHackerTruckPropertyIAmIn)
					pPropertyOwner = g_OwnerOfHackerTruckPropertyIAmIn
					PRINTLN("[AM_MP_DRONE] - LOCAL_DRONE_SCRIPT_CLEANUP pPropertyOwner = g_OwnerOfHackerTruckPropertyIAmIn: ", GET_PLAYER_NAME(pPropertyOwner))
				#IF FEATURE_HEIST_ISLAND
				ELIF IS_DRONE_STARTED_FROM_SUBMARINE_MISSILE()
				AND g_OwnerOfSubmarinePropertyIAmIn != INVALID_PLAYER_INDEX()
				AND NETWORK_IS_PLAYER_ACTIVE(g_OwnerOfSubmarinePropertyIAmIn)
					pPropertyOwner = g_OwnerOfSubmarinePropertyIAmIn
					PRINTLN("[AM_MP_DRONE] - LOCAL_DRONE_SCRIPT_CLEANUP pPropertyOwner = g_OwnerOfSubmarinePropertyIAmIn: ", GET_PLAYER_NAME(pPropertyOwner))
				#ENDIF
				ELSE
					IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE)
						IF GB_GET_LOCAL_PLAYER_GANG_BOSS() != INVALID_PLAYER_INDEX()
						AND NETWORK_IS_PLAYER_ACTIVE(GB_GET_LOCAL_PLAYER_GANG_BOSS())
							pPropertyOwner = GB_GET_LOCAL_PLAYER_GANG_BOSS()
							PRINTLN("[AM_MP_DRONE] - LOCAL_DRONE_SCRIPT_CLEANUP pPropertyOwner = GB_GET_LOCAL_PLAYER_GANG_BOSS: ", GET_PLAYER_NAME(pPropertyOwner))
						ENDIF	
					ENDIF
				ENDIF
			ENDIF
			
			IF pPropertyOwner != INVALID_PLAYER_INDEX()
				IF NETWORK_IS_PLAYER_ACTIVE(pPropertyOwner)
				AND NETWORK_IS_GAME_IN_PROGRESS()
					IF IS_DRONE_STARTED_FROM_TRUCK()
						BROADCAST_USED_DRONE_STATION(pPropertyOwner, FALSE #IF FEATURE_HEIST_ISLAND , ENUM_TO_INT(DRONE_TYPE_HACKER_TRUCK) , 0 #ENDIF )
					#IF FEATURE_HEIST_ISLAND
					ELIF IS_DRONE_STARTED_FROM_SUBMARINE_MISSILE()
						BROADCAST_USED_DRONE_STATION(pPropertyOwner, FALSE #IF FEATURE_HEIST_ISLAND , ENUM_TO_INT(DRONE_TYPE_SUBMARINE_MISSILE) , g_iShouldLaunchSubmarineSeat #ENDIF )
					#ENDIF
					ENDIF
					SET_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_DRONE_COOL_DOWN_BROADCAST_WAIT)
				ELSE
					#IF IS_DEBUG_BUILD
					IF NOT NETWORK_IS_GAME_IN_PROGRESS()
						PRINTLN("[AM_MP_DRONE] - LOCAL_DRONE_SCRIPT_CLEANUP NETWORK_IS_GAME_IN_PROGRESS false")
					ENDIF
					#ENDIF
					SET_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_DRONE_COOL_DOWN_BROADCAST_FAILED)
					PRINTLN("[AM_MP_DRONE] - LOCAL_DRONE_SCRIPT_CLEANUP owner is not active")
				ENDIF
			ELSE
				SET_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_DRONE_COOL_DOWN_BROADCAST_FAILED)
				PRINTLN("[AM_MP_DRONE] - LOCAL_DRONE_SCRIPT_CLEANUP setting drone use to false but property owner is invalid")
			ENDIF
		ELIF IS_DRONE_STARTED_FROM_HEIST_FREEMODE()
		OR IS_DRONE_STARTED_FROM_CASINO_HEIST_MISSON()
			RESET_NET_TIMER(g_sDroneCoolDownTimer)
			OVERRIDE_DRONE_COOL_DOWN_TIMER(300000)
			SET_DRONE_IN_COOL_DOWN(TRUE)
			SET_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_DRONE_COOL_DOWN_BROADCAST_WAIT)
			PRINTLN("[AM_MP_DRONE] - LOCAL_DRONE_SCRIPT_CLEANUP setting heist drone to cool down")
		ENDIF
		
		SET_LOCAL_PLAYER_USING_DRONE(FALSE)
		SET_PLAYER_USING_DRONE(FALSE)
		CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iLocalBS, DRONE_PLAYER_BD_URING_DRONE)
		
		INT iDroneUsageStat = ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), sDroneControl.droneUsageStat))
		
		IF IS_DRONE_STARTED_FROM_HEIST_FREEMODE()
		OR IS_DRONE_STARTED_FROM_CASINO_HEIST_MISSON()
			STRUCT_HEIST3_DRONE sHeistDrone 
			sHeistDrone.cpCollected = g_HeistTelemetryData.iHeistHashedMac
			sHeistDrone.playthroughId = g_HeistTelemetryData.iHeistPosixTime
		
			IF NETWORK_IS_ACTIVITY_SESSION()
				sHeistDrone.missionName = g_FMMC_STRUCT.iRootContentIDHash
			ELSE
				sHeistDrone.missionName = ENUM_TO_INT(GB_GET_GB_CASINO_VARIATION_PLAYER_IS_ON(PLAYER_ID())) 
			ENDIF	
			
			sHeistDrone.nano = TRUE
			sHeistDrone.totalDroneTases = sDroneControl.iDroneTotalTases          
			sHeistDrone.totalDroneTranq = sDroneControl.iDroneTotalTranq
			sHeistDrone.time = iDroneUsageStat
			sHeistDrone.endReason = sDroneControl.iDroneStatEndReason
			PLAYSTATS_HEIST3_DRONE(sHeistDrone)
		ELSE
			PLAYSTATS_DRONE_USAGE(iDroneUsageStat, sDroneControl.iDroneStatEndReason, sDroneControl.iDroneTotalTases)
			PRINTLN("[AM_MP_DRONE] - LOCAL_DRONE_SCRIPT_CLEANUP PLAYSTATS_DRONE_USAGE time: ", iDroneUsageStat, " end reason: ", GET_DRONE_CLEANUP_REASON(sDroneControl.iDroneStatEndReason), " Total Tases: ", sDroneControl.iDroneTotalTases)
		ENDIF
		
		#IF FEATURE_HEIST_ISLAND
		IF IS_DRONE_STARTED_FROM_SUBMARINE_MISSILE()
			SEND_SUBMARINE_WEAPONS_TELEMETRICS(SWT_GUIDED_MISSILE, IS_OWNER_OF_THIS_SUB())
		ENDIF	
		#ENDIF
		
	ENDIF
	


	STOP_SOUNDS()
	SET_PLAYER_INITIALISING_DRONE(FALSE)
	SET_DRONE_FORCE_EXTERNAL_CLEANING_UP(FALSE)
	SET_DRONE_TRANQUILIZER_AMMO(0)

	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sDroneControl.droneStunSF)
	
	IF GET_DRONE_TARGET_MODEL(TRUE) != DUMMY_MODEL_FOR_SCRIPT
		SET_MODEL_AS_NO_LONGER_NEEDED(GET_DRONE_TARGET_MODEL(TRUE))
	ENDIF
	
	IF GET_DRONE_TARGET_MODEL() != DUMMY_MODEL_FOR_SCRIPT
		SET_MODEL_AS_NO_LONGER_NEEDED(GET_DRONE_TARGET_MODEL())
	ENDIF
	
	SET_DRONE_CLEANING_UP(FALSE)
	
	CLEANUP_MENU_ASSETS()
	
	sDroneControl.iDroneStatEndReason = 0
	sDroneControl.iDroneTotalTases = 0
	sDroneControl.iDroneTotalTranq = 0
	
	REMOVE_SHOCKING_EVENT(sDroneControl.iDroneShockEvent)
	
	IF DOES_PARTICLE_FX_LOOPED_EXIST(sDroneControl.missilePFX[NATIVE_TO_INT(PLAYER_ID())])
		STOP_PARTICLE_FX_LOOPED(sDroneControl.missilePFX[NATIVE_TO_INT(PLAYER_ID())])	
	ENDIF
	
	#IF FEATURE_HEIST_ISLAND
	IF DOES_PARTICLE_FX_LOOPED_EXIST(sDroneControl.missileLaunchPFX[NATIVE_TO_INT(PLAYER_ID())])
		STOP_PARTICLE_FX_LOOPED(sDroneControl.missileLaunchPFX[NATIVE_TO_INT(PLAYER_ID())])
	ENDIF
	#ENDIF
	
	IF !bReset
		IF NOT GB_IS_PLAYER_ON_DRONE_MISSION(PLAYER_ID())
			SET_DRONE_TARGET(<<0, 0, 0>>, 0.0)
		ENDIF
		
		g_sDroneGlobals.iDroneTypeBS = 0
		
		TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
	ELSE
		INITIALISE(FALSE, TRUE)
	ENDIF
	
ENDPROC

PROC REMOTE_DRONE_SCRIPT_CLEANUP()
	PRINTLN("[AM_MP_DRONE] - REMOTE_DRONE_SCRIPT_CLEANUP")
	
	g_sDroneGlobals.remoteDroneOwner = INVALID_PLAYER_INDEX()
	RESET_NET_TIMER(sDroneControl.sParticipantTimer)
	SET_PLAYER_STARTED_REMOTE_DRONE_SCRIPT(FALSE)
	SET_TERMINATE_REMOTE_DRONE_SCRIPT(FALSE)
	SET_START_REMOTE_DRONE_SCRIPT(FALSE)
	START_REMOTE_DRONE(FALSE)
	
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

PROC SCRIPT_CLEANUP(BOOL bReset = FALSE)
	IF !IS_REMOTE_DRONE_STARTED()
		LOCAL_DRONE_SCRIPT_CLEANUP(bReset)
	ELSE
		REMOTE_DRONE_SCRIPT_CLEANUP()
	ENDIF
ENDPROC

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡     DEBUG     ╞═══════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

#IF IS_DEBUG_BUILD
PROC UPDATE_DEBUG_WIDGETS()
	IF g_bCleanupDroneScript
		SCRIPT_CLEANUP()
	ENDIF
	
	IF droneDebugStruct.fPropCamOffsetX != 0.0
	OR droneDebugStruct.fPropCamOffsetY != 0.0
	OR droneDebugStruct.fPropCamOffsetZ != 0.0
		HARD_ATTACH_CAM_TO_ENTITY(sDroneControl.droneCamera, NET_TO_OBJ(sDroneControl.cameraProp), <<0, 0, 0>>, <<droneDebugStruct.fPropCamOffsetX,droneDebugStruct.fPropCamOffsetY,droneDebugStruct.fPropCamOffsetZ>>)
	ENDIF
	
	IF droneDebugStruct.bPrintPitchRoll
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sDroneControl.cameraProp)
			OBJECT_INDEX droneProp = NET_TO_OBJ(sDroneControl.cameraProp)
			CDEBUG1LN(DEBUG_NET_DRONE, "[AM_MP_DRONE] - Drone roll: ", GET_ENTITY_ROLL(droneProp))
			CDEBUG1LN(DEBUG_NET_DRONE, "[AM_MP_DRONE] - Drone pitch: ", GET_ENTITY_PITCH(droneProp))
			VECTOR vRot = GET_ENTITY_ROTATION(droneProp)
			CDEBUG1LN(DEBUG_NET_DRONE, "[AM_MP_DRONE] - Drone rotation: ", vRot)
			droneDebugStruct.bPrintPitchRoll = FALSE
		ENDIF	
	ENDIF
	
	IF droneDebugStruct.fPropRotX != 0.0
	OR droneDebugStruct.fPropRotY != 0.0
	OR droneDebugStruct.fPropRotZ != 0.0
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sDroneControl.cameraProp)
			SET_ENTITY_ROTATION(NET_TO_OBJ(sDroneControl.cameraProp),<<droneDebugStruct.fPropRotX, droneDebugStruct.fPropRotY, droneDebugStruct.fPropRotZ>>)
		ENDIF
	ENDIF
	
	IF droneDebugStruct.bResetTimer 
		SET_DRONE_IN_COOL_DOWN(FALSE)
		droneDebugStruct.bResetTimer = FALSE
	ENDIF
	
	IF droneDebugStruct.bDisplayDroneSpeed
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sDroneControl.cameraProp)
			FLOAT fDistance = (VDIST(GET_ENTITY_COORDS(NET_TO_ENT(sDroneControl.cameraProp)), droneDebugStruct.vPreviousCoord)) / (GET_FRAME_TIME())
			
			droneDebugStruct.vPreviousCoord = GET_ENTITY_COORDS(NET_TO_ENT(sDroneControl.cameraProp))
			DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_FLOAT(fDistance), <<0.5, 0.4, 0.5>>, 255, 0, 0)
		ENDIf
	ENDIf
ENDPROC
#ENDIF

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡    PROCEDURES   ╞═════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

FUNC BOOL IS_DRONE_STATE(DRONE_STATE eState)
	RETURN sDroneControl.eDroneState = eState
ENDFUNC

PROC ACTIVATE_DRONE_BOOST(BOOL bActivate)
	IF bActivate
		IF NOT IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_BOOST_ACTIVATED)
			SET_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_BOOST_ACTIVATED)
			PRINTLN("ACTIVATE_DRONE_BOOST TRUE")
		ENDIF
	ELSE
		IF IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_BOOST_ACTIVATED)
			CLEAR_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_BOOST_ACTIVATED)
			PRINTLN("ACTIVATE_DRONE_BOOST FALSE")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_DRONE_BOOST_ACTIVATED()
	RETURN IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_BOOST_ACTIVATED)
ENDFUNC

PROC DRONE_BOOST_DIR_FORWARD(BOOL bActivate)
	IF bActivate
		IF NOT IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_BOOST_DIR_FORWARD)
			SET_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_BOOST_DIR_FORWARD)
			PRINTLN("DRONE_BOOST_DIR_FORWARD TRUE")
		ENDIF
	ELSE
		IF IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_BOOST_DIR_FORWARD)
			CLEAR_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_BOOST_DIR_FORWARD)
			PRINTLN("DRONE_BOOST_DIR_FORWARD FALSE")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_DRONE_BOOST_DIR_FORWARD()
	RETURN IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_BOOST_DIR_FORWARD)
ENDFUNC

PROC SCRIPT_INITIALISE(DRONE_LAUNCHER_STRUCT missionScriptArgs)
	#IF IS_DEBUG_BUILD
	PRINTLN("[AM_MP_DRONE] - SCRIPT_INITIALISE - Launching instance: ", missionScriptArgs.iInstanceId, " Type: ", missionScriptArgs.iType)
	#ENDIF
	
	CDEBUG1LN(DEBUG_NET_DRONE, "[AM_MP_DRONE] - SCRIPT_INITIALISE missionScriptArgs.vTriggerCoord: ", missionScriptArgs.vTriggerCoord)
		
	NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE, missionScriptArgs.iInstanceId)
	
	// This makes sure the net script is active, waits untill it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))

	// KGM: Wait for the first network broadcast before moving on
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		PRINTLN("[AM_MP_DRONE] - SCRIPT_INITIALISE FAILED TO RECEIVE INITIAL NETWORK BROADCAST - SCRIPT CLEANUP")
		SCRIPT_CLEANUP()
	ENDIF

	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF !IS_REMOTE_DRONE_STARTED()
			sDroneControl.vDroneTriggerCoord = missionScriptArgs.vTriggerCoord
			
			SWITCH INT_TO_ENUM(DRONE_TYPE, missionScriptArgs.iType)
				CASE DRONE_TYPE_HACKER_TRUCK	
					IF DOES_ENTITY_EXIST(GET_HACKER_TRUCK_INDEX_PED_IS_IN_INTERIOR())
					AND NOT IS_ENTITY_DEAD(GET_HACKER_TRUCK_INDEX_PED_IS_IN_INTERIOR())
						sDroneControl.vDroneInitialStartCoord = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(GET_HACKER_TRUCK_INDEX_PED_IS_IN_INTERIOR())
																												, GET_ENTITY_HEADING(GET_HACKER_TRUCK_INDEX_PED_IS_IN_INTERIOR()), <<0,1.6,2.9>>)
						sDroneControl.vDroneStartCoord = sDroneControl.vDroneInitialStartCoord
					ENDIF
					START_DRONE_FROM_TRUCK(TRUE)
					playerBD[NATIVE_TO_INT(PLAYER_ID())].eType = DRONE_TYPE_HACKER_TRUCK
					GlobalplayerBD_FM_4[NATIVE_TO_INT(PLAYER_ID())].eDroneType = DRONE_TYPE_HACKER_TRUCK
				BREAK
				CASE DRONE_TYPE_ARENA_WARS_ONE	
					START_DRONE_FROM_ARENA_WARS_ONE(TRUE)
					sDroneControl.vDroneInitialStartCoord = missionScriptArgs.vDroneStartCoord
					playerBD[NATIVE_TO_INT(PLAYER_ID())].eType = DRONE_TYPE_ARENA_WARS_ONE
					GlobalplayerBD_FM_4[NATIVE_TO_INT(PLAYER_ID())].eDroneType = DRONE_TYPE_ARENA_WARS_ONE
				BREAK
				CASE DRONE_TYPE_ARENA_WARS_TWO	
					START_DRONE_FROM_ARENA_WARS_TWO(TRUE)
					sDroneControl.vDroneInitialStartCoord = missionScriptArgs.vDroneStartCoord
					playerBD[NATIVE_TO_INT(PLAYER_ID())].eType = DRONE_TYPE_ARENA_WARS_TWO
					GlobalplayerBD_FM_4[NATIVE_TO_INT(PLAYER_ID())].eDroneType = DRONE_TYPE_ARENA_WARS_TWO
				BREAK
				CASE DRONE_TYPE_ARENA_WARS_MISSILES
					START_DRONE_FROM_ARENA_WARS_MISSILE(TRUE)
					sDroneControl.vDroneInitialStartCoord = missionScriptArgs.vDroneStartCoord
					playerBD[NATIVE_TO_INT(PLAYER_ID())].eType = DRONE_TYPE_ARENA_WARS_MISSILES
					GlobalplayerBD_FM_4[NATIVE_TO_INT(PLAYER_ID())].eDroneType = DRONE_TYPE_ARENA_WARS_MISSILES
				BREAK
				CASE DRONE_TYPE_HEIST_FREEMODE
					START_DRONE_FROM_HEIST_FREEMODE(TRUE)
					sDroneControl.vDroneInitialStartCoord = missionScriptArgs.vDroneStartCoord
					playerBD[NATIVE_TO_INT(PLAYER_ID())].eType = DRONE_TYPE_HEIST_FREEMODE
					GlobalplayerBD_FM_4[NATIVE_TO_INT(PLAYER_ID())].eDroneType = DRONE_TYPE_HEIST_FREEMODE
				BREAK
				CASE DRONE_TYPE_CASINO_HEIST_MISSION
					START_DRONE_FROM_CASINO_HEIST_MISSON(TRUE)
					sDroneControl.vDroneInitialStartCoord = missionScriptArgs.vDroneStartCoord
					playerBD[NATIVE_TO_INT(PLAYER_ID())].eType = DRONE_TYPE_CASINO_HEIST_MISSION
					GlobalplayerBD_FM_4[NATIVE_TO_INT(PLAYER_ID())].eDroneType = DRONE_TYPE_CASINO_HEIST_MISSION
				BREAK	
				#IF FEATURE_COPS_N_CROOKS
				CASE DRONE_TYPE_ARCADE_COP
					START_DRONE_FROM_ARCADE_COP(TRUE)
					sDroneControl.vDroneInitialStartCoord = missionScriptArgs.vDroneStartCoord
					playerBD[NATIVE_TO_INT(PLAYER_ID())].eType = DRONE_TYPE_ARCADE_COP
					GlobalplayerBD_FM_4[NATIVE_TO_INT(PLAYER_ID())].eDroneType = DRONE_TYPE_ARCADE_COP
				BREAK
				#ENDIF
				#IF FEATURE_HEIST_ISLAND
				CASE DRONE_TYPE_SUBMARINE_MISSILE
					START_DRONE_FROM_SUBMARINE_MISSILE(TRUE)
					sDroneControl.vDroneInitialStartCoord = missionScriptArgs.vDroneStartCoord
					playerBD[NATIVE_TO_INT(PLAYER_ID())].eType = DRONE_TYPE_SUBMARINE_MISSILE
					GlobalplayerBD_FM_4[NATIVE_TO_INT(PLAYER_ID())].eDroneType = DRONE_TYPE_SUBMARINE_MISSILE
				BREAK
				#ENDIF
				CASE DRONE_TYPE_REMOTE
					START_REMOTE_DRONE(TRUE)
					playerBD[NATIVE_TO_INT(PLAYER_ID())].eType = DRONE_TYPE_REMOTE
					GlobalplayerBD_FM_4[NATIVE_TO_INT(PLAYER_ID())].eDroneType = DRONE_TYPE_REMOTE
				BREAK
				DEFAULT 
					START_DRONE_FROM_DEBUG(TRUE)
					sDroneControl.vDroneInitialStartCoord = missionScriptArgs.vDroneStartCoord
				BREAK
			ENDSWITCH

			sDroneControl.vTargetCoord = missionScriptArgs.vTargetCoord
			
			IF IS_VECTOR_ZERO(GET_DRONE_OVERRIDE_COORDS())
				SET_DRONE_COORD_AND_ROTATION(sDroneControl.vDroneInitialStartCoord, <<0,0,0>>)
			ENDIF
			
			PRINTLN("[AM_MP_DRONE] INITIALISED sDroneControl.vDroneStartCoord: ", sDroneControl.vDroneInitialStartCoord)
			PRINTLN("[AM_MP_DRONE] INITIALISED sDroneControl.vDroneTriggerCoord: ", sDroneControl.vDroneTriggerCoord)
			PRINTLN("[AM_MP_DRONE] INITIALISED sDroneControl.vTargetCoord: ", sDroneControl.vTargetCoord) 
			PRINTLN("[AM_MP_DRONE] INITIALISED missionScriptArgs.iType: ", missionScriptArgs.iType) 
		ELSE
			PRINTLN("[AM_MP_DRONE] INITIALISED remote drone script")
		ENDIF
		
		// This script will not be paused if another script calls PAUSE_GAME
		PRINTLN("[AM_MP_DRONE] - INITIALISED")
	ELSE
		PRINTLN("[AM_MP_DRONE] - INITIALISED NETWORK_IS_GAME_IN_PROGRESS = FALSE - SCRIPT CLEANUP")
		SCRIPT_CLEANUP()
	ENDIF
	
	IF !IS_REMOTE_DRONE_STARTED()
		INITIALISE()
	ELSE
		INITIALISE_REMOTE_PLAYER_DRONE()
	ENDIF	
ENDPROC

/// PURPOSE:
///    Shapetest twice size of drone prop to catch any collisions in spawn coords
PROC PROCESS_DRONE_SPAWN_SHAPETEST(VECTOR vSpawnCoord)
	
	SWITCH sDroneControl.idroneSpawnShapeTest
	    CASE DRONE_SHAPETEST_INIT
	       	ENTITY_INDEX ignoreEntity 
			IF IS_DRONE_STARTED_FROM_TRUCK()
				IF IS_ENTITY_ALIVE(GET_HACKER_TRUCK_INDEX_PED_IS_IN_INTERIOR())
					ignoreEntity = GET_HACKER_TRUCK_INDEX_PED_IS_IN_INTERIOR()
				ENDIF
			ENDIF
			
			// See if we can collide with anything around drone
			FLOAT fRadius 
			
			IF IS_DRONE_STARTED_FROM_ARENA_WARS_ONE()
			OR IS_DRONE_STARTED_FROM_ARENA_WARS_TWO()
				fRadius = GET_MODEL_HEIGHT(sDroneControl.dronePropModel) 
			ELSE
				fRadius = GET_MODEL_HEIGHT(sDroneControl.dronePropModel) * 11
			ENDIF
			
			sDroneControl.droneSpawnShapeTest = START_SHAPE_TEST_CAPSULE(vSpawnCoord, vSpawnCoord, fRadius, SCRIPT_INCLUDE_ALL, ignoreEntity)
			
			IF NATIVE_TO_INT(sDroneControl.droneSpawnShapeTest) != 0
				sDroneControl.idroneSpawnShapeTest 		= DRONE_SHAPETEST_PROCESS
				PRINTLN("[AM_MP_DRONE] - PROCESS_DRONE_SPAWN_SHAPETEST sDroneControl.idroneSpawnShapeTest INIT to PROCESS ")
			ENDIF	
			
	    BREAK
	    CASE DRONE_SHAPETEST_PROCESS
	    
	        INT iHitSomething
	        VECTOR vNormalAtPosHit, vDroneCollisionHitPos
	        ENTITY_INDEX entityHit
			 
	       	SHAPETEST_STATUS shapetestStatus 
			shapetestStatus = GET_SHAPE_TEST_RESULT(sDroneControl.droneSpawnShapeTest, iHitSomething, vDroneCollisionHitPos, vNormalAtPosHit, entityHit)
			
			// result is ready
	        IF shapetestStatus = SHAPETEST_STATUS_RESULTS_READY
	            #IF IS_DEBUG_BUILD
				//PRINTLN("[AM_MP_DRONE] - PROCESS_DRONE_SPAWN_SHAPETEST - We got shapetest results!")
				#ENDIF
				
				IF iHitSomething = 0
					
					sDroneControl.iDroneSpawnHitSomthing = DRONE_CAM_AREA_CHECK_NOT_HIT_ANYTHING
					PRINTLN("[AM_MP_DRONE] - PROCESS_DRONE_SPAWN_SHAPETEST DRONE_CAM_AREA_CHECK_NOT_HIT_ANYTHING ")
					vDroneCollisionHitPos = <<0,0,0>> 

	            ELSE

	                sDroneControl.iDroneSpawnHitSomthing = DRONE_CAM_AREA_CHECK_HIT_SOMETHING

					sDroneControl.droneSpawnShapeTest = INT_TO_NATIVE(SHAPETEST_INDEX, 0)
					sDroneControl.idroneSpawnShapeTest 	= DRONE_SHAPETEST_INIT  
					
	            ENDIF
				 
			ELIF shapetestStatus	= SHAPETEST_STATUS_NONEXISTENT
				sDroneControl.idroneSpawnShapeTest 	= DRONE_SHAPETEST_INIT    
	        ENDIF

	    BREAK
	ENDSWITCH	
ENDPROC

FUNC BOOL IS_SPAWN_COORD_IN_RISTRICTED_AREA(VECTOR vCoordToCheck)
	IF IS_POINT_IN_ANGLED_AREA(vCoordToCheck, <<-1036.269897,-228.704025,53.764351>>, <<-1096.765015,-258.690002,35.778076>>, 20.000000 )
		PRINTLN("[AM_MP_DRONE] - IS_SPAWN_COORD_IN_RISTRICTED_AREA TRUE vCoordToCheck: ", vCoordToCheck)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC 

FUNC BOOL HAS_FOUND_SAFE_SPAWN_COORD()
	IF sDroneControl.iDroneSpawnHitSomthing = DRONE_CAM_AREA_CHECK_INIT
		IF NOT IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_CHECK_ACTUAL_SPAWN_COORD)
			IF NOT GB_IS_PLAYER_ON_DRONE_MISSION(PLAYER_ID())
			AND IS_DRONE_STARTED_FROM_TRUCK()
			AND IS_ENTITY_ALIVE(GET_HACKER_TRUCK_INDEX_PED_IS_IN_INTERIOR())
				sDroneControl.vDroneStartCoord = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(GET_HACKER_TRUCK_INDEX_PED_IS_IN_INTERIOR()), GET_ENTITY_HEADING(GET_HACKER_TRUCK_INDEX_PED_IS_IN_INTERIOR()), <<-0.7, 1.6, 4.0>>)
				SET_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_CHECK_ACTUAL_SPAWN_COORD)
				PRINTLN("[AM_MP_DRONE] - HAS_FOUND_SAFE_SPAWN_COORD DRONE_LOCAL_BS_CHECK_ACTUAL_SPAWN_COORD ")	
			ELSE
				IF IS_DRONE_STARTED_FROM_TRUCK() 
				AND IS_ENTITY_ALIVE(GET_HACKER_TRUCK_INDEX_PED_IS_IN_INTERIOR())
					sDroneControl.vDroneStartCoord = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(GET_HACKER_TRUCK_INDEX_PED_IS_IN_INTERIOR()), GET_ENTITY_HEADING(GET_HACKER_TRUCK_INDEX_PED_IS_IN_INTERIOR()), <<-0.7, 1.6, 4.0>>)
				ENDIF
				sDroneControl.vDroneStartCoord.x = GET_RANDOM_FLOAT_IN_RANGE(sDroneControl.vDroneStartCoord.x - 1.5 , sDroneControl.vDroneStartCoord.x + 1.5)
				sDroneControl.vDroneStartCoord.y = GET_RANDOM_FLOAT_IN_RANGE(sDroneControl.vDroneStartCoord.y - 1.5 , sDroneControl.vDroneStartCoord.y + 1.5)
				sDroneControl.vDroneStartCoord.z = GET_RANDOM_FLOAT_IN_RANGE(sDroneControl.vDroneStartCoord.z + 0.5 , sDroneControl.vDroneStartCoord.z + 1.5)
				IF NOT IS_SPAWN_COORD_IN_RISTRICTED_AREA(sDroneControl.vDroneStartCoord)
					SET_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_CHECK_ACTUAL_SPAWN_COORD)
				ENDIF	
			ENDIF
		ELSE
			IF IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_CHECK_OFFSET_SPAWN_COORD)
				sDroneControl.vDroneStartCoord.x = GET_RANDOM_FLOAT_IN_RANGE(sDroneControl.vDroneStartCoord.x - 1.5 , sDroneControl.vDroneStartCoord.x + 1.5)
				sDroneControl.vDroneStartCoord.y = GET_RANDOM_FLOAT_IN_RANGE(sDroneControl.vDroneStartCoord.y - 1.5 , sDroneControl.vDroneStartCoord.y + 1.5)
				sDroneControl.vDroneStartCoord.z = GET_RANDOM_FLOAT_IN_RANGE(sDroneControl.vDroneStartCoord.z + 0.5 , sDroneControl.vDroneStartCoord.z + 1.5)
				IF NOT IS_SPAWN_COORD_IN_RISTRICTED_AREA(sDroneControl.vDroneStartCoord)
					CLEAR_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_CHECK_OFFSET_SPAWN_COORD)
					PRINTLN("[AM_MP_DRONE] - HAS_FOUND_SAFE_SPAWN_COORD sDroneControl.vDroneStartCoord: ", sDroneControl.vDroneStartCoord)
				ENDIF	
			ENDIF	
		ENDIF		
		
		PROCESS_DRONE_SPAWN_SHAPETEST(sDroneControl.vDroneStartCoord)
	ELSE
		IF sDroneControl.iDroneSpawnHitSomthing  = DRONE_CAM_AREA_CHECK_NOT_HIT_ANYTHING
		AND NOT IS_SPAWN_COORD_IN_RISTRICTED_AREA(sDroneControl.vDroneStartCoord)
			PRINTLN("[AM_MP_DRONE] - HAS_FOUND_SAFE_SPAWN_COORD DRONE_CAM_AREA_CHECK_NOT_HIT_ANYTHING ")
			RETURN TRUE
		ELSE
			sDroneControl.iDroneSpawnHitSomthing = DRONE_CAM_AREA_CHECK_INIT
			SET_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_CHECK_OFFSET_SPAWN_COORD)
			PRINTLN("[AM_MP_DRONE] - HAS_FOUND_SAFE_SPAWN_COORD Not safe pick another coord")
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_FIND_SAFE_SPAWN_COORD_FOR_DRONE()
	IF IS_DRONE_STARTED_FROM_TRUCK()
		RETURN TRUE
	ENDIF
	
	IF IS_DRONE_STARTED_FROM_HEIST_FREEMODE()
	AND NOT SHOULD_START_DRONE_FROM_PLAYER_PHONE()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_SPAWN_DRONE_INVISIBLE()
	IF SHOULD_START_DRONE_FROM_PLAYER_PHONE()
		RETURN TRUE
	ENDIF
	
	#IF FEATURE_HEIST_ISLAND
	IF IS_DRONE_STARTED_FROM_SUBMARINE_MISSILE()
		RETURN TRUE
	ENDIF
	#ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_PROP_CREATION()
	MODEL_NAMES VehModel = BLAZER5
	IF REQUEST_LOAD_MODEL(sDroneControl.dronePropModel)
	AND REQUEST_LOAD_MODEL(VehModel)
		IF NOT IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_PROP_CREATED)
			IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sDroneControl.cameraProp)
				
				IF NOT IS_DRONE_STARTED_FROM_DEBUG()
					VECTOR vLoadSceneCoord
					IF IS_DRONE_STARTED_FROM_TRUCK()
						IF IS_ENTITY_ALIVE(GET_HACKER_TRUCK_INDEX_PED_IS_IN_INTERIOR())
							vLoadSceneCoord = GET_ENTITY_COORDS(GET_HACKER_TRUCK_INDEX_PED_IS_IN_INTERIOR())
						ENDIF
					ELSE
						vLoadSceneCoord = sDroneControl.vDroneStartCoord
					ENDIF
					
					IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
						NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_ALLOW_PLAYER_DAMAGE)
					ENDIF
					
					IF NOT IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_START_DRONE_LOAD_SCENE)
						IF NEW_LOAD_SCENE_START_SPHERE(vLoadSceneCoord, 100, NEWLOADSCENE_FLAG_REQUIRE_COLLISION)
							SET_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_START_DRONE_LOAD_SCENE)
							PRINTLN("[AM_MP_DRONE] - MAINTAIN_PROP_CREATION DRONE_LOCAL_BS_START_DRONE_LOAD_SCENE TRUE")	
						ELSE
							PRINTLN("[AM_MP_DRONE] - MAINTAIN_PROP_CREATION NEW_LOAD_SCENE_START_SPHERE FALSE")	
						ENDIF
					ELSE	
						IF IS_NEW_LOAD_SCENE_LOADED()
							IF NOT IS_BIT_SET(sDroneControl.iLocalBS,  DRONE_LOCAL_BS_FOUND_SAFE_SPAWN_POINT)
								IF SHOULD_FIND_SAFE_SPAWN_COORD_FOR_DRONE()
									IF HAS_FOUND_SAFE_SPAWN_COORD()
										SET_BIT(sDroneControl.iLocalBS,  DRONE_LOCAL_BS_FOUND_SAFE_SPAWN_POINT)
										PRINTLN("[AM_MP_DRONE] - MAINTAIN_PROP_CREATION HAS_FOUND_SAFE_SPAWN_COORD TRUE")	
									ENDIF
								ELSE
									IF NOT IS_VECTOR_ZERO(GET_DRONE_OVERRIDE_COORDS())
										sDroneControl.vDroneStartCoord = GET_DRONE_OVERRIDE_COORDS()
									ENDIF	
									SET_BIT(sDroneControl.iLocalBS,  DRONE_LOCAL_BS_FOUND_SAFE_SPAWN_POINT)
									PRINTLN("[AM_MP_DRONE] - MAINTAIN_PROP_CREATION HAS_FOUND_SAFE_SPAWN_COORD TRUE DroneControl.vDroneStartCoord: ", sDroneControl.vDroneStartCoord)	
									
									#IF FEATURE_HEIST_ISLAND
									IF IS_DRONE_STARTED_FROM_SUBMARINE_MISSILE()
										sDroneControl.vDroneStartCoord.z += 1000
									ENDIF
									#ENDIF
								ENDIF	
							ENDIF	
						ELSE
							PRINTLN("[AM_MP_DRONE] - MAINTAIN_PROP_CREATION IS_NEW_LOAD_SCENE_LOADED FALSE")	
						ENDIF	
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(sDroneControl.iLocalBS,  DRONE_LOCAL_BS_FOUND_SAFE_SPAWN_POINT)
				OR IS_DRONE_STARTED_FROM_DEBUG()
					IF !IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_FAKE_VEH_CREATION)
						sDroneControl.fakeVeh = CREATE_VEHICLE(VehModel, sDroneControl.vDroneStartCoord , 0, FALSE, FALSE)
						SET_ENTITY_INVINCIBLE(sDroneControl.fakeVeh, TRUE)
						//SET_ENTITY_LOAD_COLLISION_FLAG(sDroneControl.fakeVeh, TRUE)
						SET_ENTITY_COORDS_NO_OFFSET(sDroneControl.fakeVeh, sDroneControl.vDroneStartCoord)
						SET_ENTITY_COMPLETELY_DISABLE_COLLISION(sDroneControl.fakeVeh, FALSE)
						SET_ENTITY_VISIBLE(sDroneControl.fakeVeh, FALSE)
						SET_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_FAKE_VEH_CREATION)
						PRINTLN("[AM_MP_DRONE] - MAINTAIN_PROP_CREATION DRONE_LOCAL_BS_FAKE_VEH_CREATION TRUE")	
					ELSE
						IF CAN_RESERVE_NETWORK_OBJECTS_FOR_THIS_SCRIPT(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE, RESERVATION_LOCAL_ONLY) + 1, FALSE, TRUE)
							IF NOT IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_RESERVE_OBJECT)
								RESERVE_LOCAL_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE, RESERVATION_LOCAL_ONLY) + 1)
								
								SET_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_RESERVE_OBJECT)
								PRINTLN("[AM_MP_DRONE] - MAINTAIN_PROP_CREATION DRONE_LOCAL_BS_RESERVE_OBJECT TRUE")	
							ENDIF
							
							IF CAN_REGISTER_MISSION_OBJECTS(1)
								
								BOOL bInVisible = SHOULD_SPAWN_DRONE_INVISIBLE()
								
								IF SHOULD_START_DRONE_FROM_PLAYER_PHONE()
									sDroneControl.vAnimCoords = GET_PLAYER_COORDS(PLAYER_ID())
									GET_GROUND_Z_FOR_3D_COORD(sDroneControl.vAnimCoords, sDroneControl.vAnimCoords.z)
									sDroneControl.vDroneStartCoord = GET_ANIM_INITIAL_OFFSET_POSITION(sDroneControl.sAnimDicName, "ENTER", sDroneControl.vAnimCoords, GET_ENTITY_ROTATION(PLAYER_PED_ID()))
								ENDIF
	
								IF CREATE_NET_OBJ(sDroneControl.cameraProp, sDroneControl.dronePropModel, sDroneControl.vDroneStartCoord, FALSE, TRUE, TRUE, TRUE, TRUE, DEFAULT, bInVisible)
									FLOAT fHeading 
									g_sDroneGlobals.DroneProp = NET_TO_OBJ(sDroneControl.cameraProp)
			
									SET_ENTITY_INVINCIBLE(NET_TO_OBJ(sDroneControl.cameraProp), TRUE)

									IF IS_DRONE_STARTED_FROM_TRUCK()
										IF IS_ENTITY_ALIVE(GET_HACKER_TRUCK_INDEX_PED_IS_IN_INTERIOR())
											SET_ENTITY_ROTATION(GET_LOCAL_DRONE_PROP_OBJ_INDEX(), GET_ENTITY_ROTATION(GET_HACKER_TRUCK_INDEX_PED_IS_IN_INTERIOR()))
											fHeading = GET_ENTITY_HEADING(GET_HACKER_TRUCK_INDEX_PED_IS_IN_INTERIOR()) + 180
										ENDIF
									ENDIF

									GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].vDroneCoords 			= sDroneControl.vDroneStartCoord
									GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].fDroneHeading 			= fHeading
									playerBD[NATIVE_TO_INT(PLAYER_ID())].netID_remoteDrone 				= sDroneControl.cameraProp
									
									SET_NETWORK_ID_ALWAYS_EXISTS_FOR_PLAYER(sDroneControl.cameraProp, PLAYER_ID(), TRUE)
									
									FORCE_ACTIVATING_TRACKING_ON_ENTITY(NET_TO_ENT(sDroneControl.cameraProp), TRUE)
									SET_ENTITY_LOAD_COLLISION_FLAG(NET_TO_ENT(sDroneControl.cameraProp), TRUE)
	
									SET_ENTITY_COORDS_NO_OFFSET(GET_LOCAL_DRONE_PROP_OBJ_INDEX(), sDroneControl.vDroneStartCoord)
									SET_ENTITY_HEADING(GET_LOCAL_DRONE_PROP_OBJ_INDEX(), fHeading)
									SET_ENTITY_GHOSTED_FOR_GHOST_PLAYERS(GET_LOCAL_DRONE_PROP_OBJ_INDEX(), TRUE)
									
									IF NOT IS_VECTOR_ZERO(GET_DRONE_OVERRIDE_ROTATION())
										SET_ENTITY_ROTATION(GET_LOCAL_DRONE_PROP_OBJ_INDEX(), GET_DRONE_OVERRIDE_ROTATION())
									ENDIF
									
									IF IS_DRONE_STARTED_FROM_ARENA_WARS_MISSILE()
									#IF FEATURE_HEIST_ISLAND
									OR IS_DRONE_STARTED_FROM_SUBMARINE_MISSILE()	
									#ENDIF
										SET_ENTITY_COMPLETELY_DISABLE_COLLISION(GET_LOCAL_DRONE_PROP_OBJ_INDEX(), FALSE)
									ENDIF
									
									NETWORK_USE_HIGH_PRECISION_BLENDING(sDroneControl.cameraProp, TRUE)
									
									IF GB_IS_PLAYER_ON_DRONE_MISSION(PLAYER_ID())
										SET_ENTITY_HEALTH(GET_LOCAL_DRONE_PROP_OBJ_INDEX(), g_sMPTunables.iBB_TERRORBYTE_DRONE_HEALTH * 5)
									#IF FEATURE_HEIST_ISLAND
									ELIF IS_DRONE_STARTED_FROM_SUBMARINE_MISSILE()	
										SET_ENTITY_HEALTH(GET_LOCAL_DRONE_PROP_OBJ_INDEX(), 1)
										SET_NETWORK_ID_EXISTS_ON_ALL_MACHINES(sDroneControl.cameraProp, TRUE)
										SET_ENTITY_LOD_DIST(NET_TO_ENT(sDroneControl.cameraProp), 700)
									#ENDIF
									ELSE	
										SET_ENTITY_HEALTH(GET_LOCAL_DRONE_PROP_OBJ_INDEX(), g_sMPTunables.iBB_TERRORBYTE_DRONE_HEALTH)
									ENDIF

									SET_NETWORK_ID_CAN_MIGRATE(sDroneControl.cameraProp, FALSE)
									SET_ENTITY_RECORDS_COLLISIONS(NET_TO_ENT(sDroneControl.cameraProp), TRUE)
									SET_POP_CONTROL_SPHERE_THIS_FRAME(sDroneControl.vDroneStartCoord, 100, 200)
									SET_FOCUS_POS_AND_VEL(GET_ENTITY_COORDS(NET_TO_ENT(sDroneControl.cameraProp)), GET_ENTITY_ROTATION(NET_TO_ENT(sDroneControl.cameraProp)))
									SET_MODEL_AS_NO_LONGER_NEEDED(sDroneControl.dronePropModel)
									SET_MODEL_AS_NO_LONGER_NEEDED(VehModel)
									PRINTLN("[AM_MP_DRONE] - MAINTAIN_PROP_CREATION creating camera prop")	
								ENDIF	
							ENDIF
						ELSE
							PRINTLN("[AM_MP_DRONE] - MAINTAIN_PROP_CREATION CAN_RESERVE_NETWORK_OBJECTS_FOR_THIS_SCRIPT false")	
						ENDIF	
					ENDIF	
				ENDIF	
			ELSE	
				SET_ENTITY_HAS_GRAVITY(NET_TO_OBJ(sDroneControl.cameraProp), FALSE)
				IF IS_ENTITY_ALIVE(sDroneControl.fakeVeh)
					IF IS_ENTITY_ALIVE(NET_TO_ENT(sDroneControl.cameraProp))
						ATTACH_ENTITY_TO_ENTITY(sDroneControl.fakeVeh, NET_TO_OBJ(sDroneControl.cameraProp), -1, <<0,0,-0.8>>, <<0,0,0>>)
						//SET_OBJECT_PHYSICS_PARAMS( NET_TO_OBJ(sDroneControl.cameraProp), 100, 10.0, <<0,0,0>>, <<999999.0, 999999.0, 999999.0>>)
						SET_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_PROP_CREATED)
						PRINTLN("[AM_MP_DRONE] - MAINTAIN_PROP_CREATION DRONE_LOCAL_BS_PROP_CREATED = TRUE")
					ELSE
						PRINTLN("[AM_MP_DRONE] - MAINTAIN_PROP_CREATION cameraProp Not alive")
					ENDIF
				ELSE
					PRINTLN("[AM_MP_DRONE] - MAINTAIN_PROP_CREATION fakeVeh Not alive")
				ENDIF	
			ENDIF
		ENDIF	
	ENDIF
ENDPROC

FUNC MODEL_NAMES GET_DRONE_PED_MODEL()
	RETURN G_M_M_ChiGoon_01
ENDFUNC

PROC MAINTAIN_FAKE_PED_CREATION()

	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sDroneControl.cameraProp)
		IF NOT DOES_ENTITY_EXIST(sDroneControl.fakePed)
		
			REQUEST_MODEL(GET_DRONE_PED_MODEL())

			IF HAS_MODEL_LOADED(GET_DRONE_PED_MODEL())
				IF NOT IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_RESERVED_FAKE_PED_INDEX)
					IF CAN_RESERVE_NETWORK_OBJECTS_FOR_THIS_SCRIPT(GET_NUM_RESERVED_MISSION_OBJECTS()+ 1, FALSE, TRUE)
						RESERVE_NETWORK_MISSION_PEDS(GET_NUM_CREATED_MISSION_PEDS() + 1) 
						SET_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_RESERVED_FAKE_PED_INDEX)
					ENDIF
				ELSE
					IF CAN_REGISTER_MISSION_PEDS(1)
						sDroneControl.fakePed = CREATE_PED(PEDTYPE_MISSION, GET_DRONE_PED_MODEL(), sDroneControl.vDroneCoords, GET_ENTITY_HEADING(NET_TO_OBJ(sDroneControl.cameraProp)), FALSE, FALSE)
						SET_ENTITY_INVINCIBLE(sDroneControl.fakePed, TRUE)
						SET_ENTITY_VISIBLE(sDroneControl.fakePed, FALSE)
						SET_ENTITY_HAS_GRAVITY(sDroneControl.fakePed, FALSE)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sDroneControl.fakePed, TRUE)
						SET_MODEL_AS_NO_LONGER_NEEDED(GET_DRONE_PED_MODEL())
						SET_PED_CONFIG_FLAG(sDroneControl.fakePed, PCF_RunFromFiresAndExplosions, FALSE)
						SET_PED_CONFIG_FLAG(sDroneControl.fakePed, PCF_DontActivateRagdollFromExplosions, TRUE)
						SET_PED_CONFIG_FLAG(sDroneControl.fakePed, PCF_DisableExplosionReactions, TRUE)
						SET_ENTITY_PROOFS(sDroneControl.fakePed, TRUE, TRUE, TRUE, TRUE, TRUE)
						ATTACH_ENTITY_TO_ENTITY(sDroneControl.fakePed, NET_TO_OBJ(sDroneControl.cameraProp), -1, <<0,0,-0.25>>, <<0,0,0>>)
						SET_ENTITY_COMPLETELY_DISABLE_COLLISION(sDroneControl.fakePed, FALSE)
						g_sDroneGlobals.DroneFakePed = sDroneControl.fakePed
						PRINTLN("[AM_MP_DRONE] - MAINTAIN_FAKE_PED_CREATION, DONE")
					ENDIF
				ENDIF
			ENDIF
		
		ELSE
			IF IS_ENTITY_ALIVE(sDroneControl.fakePed)
				IF IS_ENTITY_VISIBLE(sDroneControl.fakePed)
					SET_ENTITY_VISIBLE(sDroneControl.fakePed, FALSE)
				ENDIF
				
				IF !GB_IS_PLAYER_PERMANENT_PARTICIPANT(PLAYER_ID())
				AND NOT FM_EVENT_IS_PLAYER_PERMANENT_PARTICIPANT_IN_BUSINESS_BATTLE(PLAYER_ID())
					IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
						SET_DRONE_RELATIONSHIP_GROUP(rgFM_HateEveryOne)
					ENDIF
				ELSE
					IF g_sDroneGlobals.iDroneRelationShipGroup = -1
					OR g_sDroneGlobals.iDroneRelationShipGroup = 0
					OR g_sDroneGlobals.iDroneRelationShipGroup = ENUM_TO_INT(rgFM_AiLike)
						SET_DRONE_RELATIONSHIP_GROUP(rgFM_HateEveryOne)
					ENDIF	
				ENDIF
				IF GET_PED_RELATIONSHIP_GROUP_DEFAULT_HASH(sDroneControl.fakePed) != GET_DRONE_RELATIONSHIP_GROUP()
					SET_PED_RELATIONSHIP_GROUP_HASH(sDroneControl.fakePed, GET_DRONE_RELATIONSHIP_GROUP())
				ENDIF
				IF NOT IS_ENTITY_ATTACHED(sDroneControl.fakePed)
					ATTACH_ENTITY_TO_ENTITY(sDroneControl.fakePed, NET_TO_OBJ(sDroneControl.cameraProp), -1, <<0,0,-0.25>>, <<0,0,0>>)
					SET_ENTITY_COMPLETELY_DISABLE_COLLISION(sDroneControl.fakePed, FALSE)
					PRINTLN("[AM_MP_DRONE] - MAINTAIN_FAKE_PED_CREATION, ATTACH_ENTITY_TO_ENTITY ")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_BY_PASS_DRONE_SEAT_CHECK()
	
	#IF IS_DEBUG_BUILD
	IF NOT IS_PLAYER_IN_HACKER_TRUCK(PLAYER_ID())
	AND IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), sDroneControl.vDroneTriggerCoord, 1.5)
		RETURN TRUE
	ENDIF 
	#ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_PLAYING_DRONE_STATION_ANIMATION()
	IF g_iShouldLaunchTruckTurret != -1
	OR g_iInteriorTurretSeat != -1

		IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@FACILITY@LAUNCH_CONTROLS@", "COMPUTER_idle")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@FACILITY@LAUNCH_CONTROLS@", "COMPUTER_idle_control")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@FACILITY@LAUNCH_CONTROLS@", "COMPUTER_enter_control")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@FACILITY@LAUNCH_CONTROLS@", "COMPUTER_enter")
			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "IS_PLAYER_PLAYING_DRONE_STATION_ANIMATION: returning TRUE, player is playing computer anim")
			RETURN TRUE
		
		ENDIF
	
		IF SHOULD_BAIL_FROM_TURRET()
			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "IS_PLAYER_PLAYING_DRONE_STATION_ANIMATION: returning false, SHOULD_BAIL_FROM_TURRET ")
			RETURN FALSE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_DRONE_IN_INVALID_INTERIOR_COORDS()
	IF sDroneControl.InteriorNameHash  = -1945204837
		IF IS_ENTITY_ALIVE(NET_TO_OBJ(sDroneControl.cameraProp))
			IF GET_ROOM_KEY_FROM_ENTITY(NET_TO_OBJ(sDroneControl.cameraProp)) = -25693127
				VECTOR vDroneCoord = GET_ENTITY_COORDS(NET_TO_OBJ(sDroneControl.cameraProp))
				IF vDroneCoord.z >= 47
					PRINTLN("[AM_MP_DRONE] - IS_DRONE_IN_INVALID_INTERIOR_COORDS Drone is in v_faceoffice interior and flying high")
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF	
	ENDIF
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sDroneControl.cameraProp)
		IF IS_ENTITY_ALIVE(NET_TO_OBJ(sDroneControl.cameraProp))
			IF IS_ENTITY_IN_ANGLED_AREA( NET_TO_OBJ(sDroneControl.cameraProp), <<-1071.257935,-242.548416,48.021328>>, <<-1069.456055,-245.923401,43.879833>>, 3.000000)
				PRINTLN("[AM_MP_DRONE] - IS_DRONE_IN_INVALID_INTERIOR_COORDS Drone is interior under the sairs trying to wallbreach")
				RETURN TRUE
			ENDIF
			
			IF IS_ENTITY_IN_ANGLED_AREA( NET_TO_OBJ(sDroneControl.cameraProp), <<2494.397949,-276.2,-69.09>>, <<2494.342285,-277.482422,-67.987564>>, 0.700000)
				PRINTLN("[AM_MP_DRONE] - IS_DRONE_IN_INVALID_INTERIOR_COORDS Drone is interior stealth_2c_side trying to go through window")
				RETURN TRUE
			ENDIF
		ENDIF	
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_DRONE_OUT_OF_WORLD_LIMIT()
	VECTOR vMinWorldLimitCoords = <<-90000, -90000, -1600>>
	VECTOR vMaxWorldLimitCoords = <<90000, 90000, 2600>>
	
	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sDroneControl.cameraProp)
		RETURN FALSE
	ENDIF
	
	IF NOT IS_ENTITY_IN_AREA(NET_TO_OBJ(sDroneControl.cameraProp), vMinWorldLimitCoords, vMaxWorldLimitCoords)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    This will skip 1 second delay before fuzz camera
FUNC BOOL SHOULD_CLEANUP_DRONE_CAMERA_INSTANTLY()
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sDroneControl.cameraProp)
	AND IS_ENTITY_DEAD(NET_TO_OBJ(sDroneControl.cameraProp))
	AND NOT IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_CAMERA_EXPLOSION)
		PRINTLN("[AM_MP_DRONE] - SHOULD_CLEANUP_DRONE_CAMERA_INSTANTLY Drone is dead")
		RETURN TRUE
	ENDIF
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sDroneControl.cameraProp)
		IF GB_IS_PLAYER_ON_DRONE_MISSION(PLAYER_ID())
		AND IS_BIT_SET(g_sHackingData.bsHacking, 0)
			// Skip
			PRINTLN("[AM_MP_DRONE] - SHOULD_CLEANUP_DRONE_CAMERA_INSTANTLY don't check collision while hacking")
		ELSE
			IF SHOULD_ENABLE_DRONE_INSTANT_COLLISION()
				IF HAS_ENTITY_COLLIDED_WITH_ANYTHING(NET_TO_OBJ(sDroneControl.cameraProp))
				OR GET_LAST_MATERIAL_HIT_BY_ENTITY(NET_TO_OBJ(sDroneControl.cameraProp)) != INVALID_MATERIAL
					sDroneControl.iDroneStatEndReason = DRONE_CLEANUP_REASON_COLLISION
					PRINTLN("[AM_MP_DRONE] - SHOULD_CLEANUP_DRONE_CAMERA_INSTANTLY HAS_ENTITY_COLLIDED_WITH_ANYTHING TRUE")
					RETURN TRUE
				ENDIF
			ENDIF	
		ENDIF	
	ENDIF	
	
	IF IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_CAMERA_EXPLOSION)
		sDroneControl.iDroneStatEndReason = DRONE_CLEANUP_REASON_DETROYED_BY_OWN_DETONATE
		PRINTLN("[AM_MP_DRONE] - SHOULD_CLEANUP_DRONE_CAMERA_INSTANTLY player exploded drone")
		RETURN TRUE
	ENDIF 
	
	IF IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_DRONE_COLLIDE_WITH_VEH)
		sDroneControl.iDroneStatEndReason = DRONE_CLEANUP_REASON_COLLISION
		PRINTLN("[AM_MP_DRONE] - SHOULD_CLEANUP_DRONE_CAMERA_INSTANTLY drone collided with moving vehicle")
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_EXIT_DRONE)
		sDroneControl.iDroneStatEndReason = DRONE_CLEANUP_REASON_RETURNED_TO_PLAYER
		PRINTLN("[AM_MP_DRONE] - SHOULD_CLEANUP_DRONE_CAMERA_INSTANTLY DRONE_LOCAL_BS_EXIT_DRONE TRUE")
		RETURN TRUE
	ENDIF
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sDroneControl.cameraProp)
		IF IS_ENTITY_IN_WATER(NET_TO_OBJ(sDroneControl.cameraProp))
		OR IS_ENTITY_IN_DEEP_WATER(NET_TO_ENT(sDroneControl.cameraProp))
			IF IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_TURN_ON_DRONE_COLLISION)
				sDroneControl.iDroneStatEndReason = DRONE_CLEANUP_REASON_DROPPED_IN_WATER
				PRINTLN("[AM_MP_DRONE] - SHOULD_CLEANUP_DRONE_CAMERA_INSTANTLY drone is in water TRUE")
				RETURN TRUE
			ENDIF	
		ENDIF
	ENDIF	
	
	IF IS_PLAYER_WALKING_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
		sDroneControl.iDroneStatEndReason = DRONE_CLEANUP_REASON_RETURNED_TO_PLAYER
		PRINTLN("[AM_MP_DRONE] - SHOULD_CLEANUP_DRONE_CAMERA_INSTANTLY player walking in or out TRUE")
		RETURN TRUE
	ENDIF
	
	IF IS_DRONE_STARTED_FROM_TRUCK()
		IF g_OwnerOfHackerTruckPropertyIAmIn != INVALID_PLAYER_INDEX()
			IF IS_OWNERS_HACKER_TRUCK_INSIDE_BUSINESS_HUB(g_OwnerOfHackerTruckPropertyIAmIn)
				sDroneControl.iDroneStatEndReason = DRONE_CLEANUP_REASON_RETURNED_TO_PLAYER
				PRINTLN("[AM_MP_DRONE] - SHOULD_CLEANUP_DRONE_CAMERA_INSTANTLY truck is in business hub")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_DRONE_IN_INVALID_INTERIOR_COORDS()
		sDroneControl.iDroneStatEndReason = DRONE_CLEANUP_REASON_COLLISION
		PRINTLN("[AM_MP_DRONE] - SHOULD_CLEANUP_DRONE_CAMERA_INSTANTLY IS_DRONE_IN_INVALID_INTERIOR_COORDS TRUE")
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_IN_PASSIVE_MODE(PLAYER_ID())
		sDroneControl.iDroneStatEndReason = DRONE_CLEANUP_REASON_RETURNED_TO_PLAYER
		PRINTLN("[AM_MP_DRONE] - SHOULD_CLEANUP_DRONE_CAMERA_INSTANTLY IS_PLAYER_IN_PASSIVE_MODE TRUE")
		RETURN TRUE
	ENDIF
	
	IF SHOULD_RETURN_DRONE_TO_PLAYER()
		sDroneControl.iDroneStatEndReason = DRONE_CLEANUP_REASON_RETURNED_TO_PLAYER
		PRINTLN("[AM_MP_DRONE] - SHOULD_CLEANUP_DRONE_CAMERA_INSTANTLY SHOULD_RETURN_DRONE_TO_PLAYER TRUE")
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_CHANGING_CLOTHES()
		sDroneControl.iDroneStatEndReason = DRONE_CLEANUP_REASON_RETURNED_TO_PLAYER
		PRINTLN("[AM_MP_DRONE] - SHOULD_CLEANUP_DRONE_CAMERA_INSTANTLY IS_PLAYER_CHANGING_CLOTHES TRUE")
		RETURN TRUE
	ENDIF
	
	IF SHOULD_RETURN_DRONE_TO_PLAYER_AND_KILL_SCRIPT()
		sDroneControl.iDroneStatEndReason = DRONE_CLEANUP_REASON_RETURNED_TO_PLAYER
		PRINTLN("[AM_MP_DRONE] - SHOULD_RETURN_DRONE_TO_PLAYER_AND_KILL_SCRIPT SHOULD_RETURN_DRONE_TO_PLAYER_AND_KILL_SCRIPT TRUE")
		RETURN TRUE
	ENDIF
	
	IF IS_DRONE_FORCED_EXTERNAL_CLEAN_UP()
		sDroneControl.iDroneStatEndReason = DRONE_CLEANUP_REASON_RETURNED_TO_PLAYER
		PRINTLN("[AM_MP_DRONE] - SHOULD_RETURN_DRONE_TO_PLAYER_AND_KILL_SCRIPT IS_DRONE_FORCED_EXTERNAL_CLEAN_UP TRUE")
		RETURN TRUE
	ENDIF
	
	IF SHOULD_START_DRONE_FROM_PLAYER_PHONE()
		IF IS_PED_DEAD_OR_DYING(PLAYER_PED_ID())
			sDroneControl.iDroneStatEndReason = DRONE_CLEANUP_REASON_PLAYER_INJURED
			PRINTLN("[AM_MP_DRONE] - SHOULD_RETURN_DRONE_TO_PLAYER_AND_KILL_SCRIPT player is dead or dying TRUE")
			RETURN TRUE
		ENDIF
		
		IF IS_PED_INJURED(PLAYER_PED_ID())
		OR IS_PED_HURT(PLAYER_PED_ID())
			sDroneControl.iDroneStatEndReason = DRONE_CLEANUP_REASON_PLAYER_INJURED
			PRINTLN("[AM_MP_DRONE] - SHOULD_RETURN_DRONE_TO_PLAYER_AND_KILL_SCRIPT player is injured TRUE")
			RETURN TRUE
		ENDIF

		IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), sDroneControl.vAnimCoords) > 2.0
			sDroneControl.iDroneStatEndReason = DRONE_CLEANUP_REASON_PLAYER_ANIM_INTERRUPTED
			PRINTLN("[AM_MP_DRONE] - SHOULD_RETURN_DRONE_TO_PLAYER_AND_KILL_SCRIPT player is far from anim coords TRUE")
			RETURN TRUE
		ENDIF	
	ENDIF
	
	IF IS_DRONE_OUT_OF_WORLD_LIMIT()
		PRINTLN("[AM_MP_DRONE] - SHOULD_RETURN_DRONE_TO_PLAYER_AND_KILL_SCRIPT player is out of world bounds")
		RETURN TRUE
	ENDIF
	
	#IF FEATURE_HEIST_ISLAND
	IF IS_DRONE_STARTED_FROM_SUBMARINE_MISSILE()
		IF IS_DOING_SUBMARINE_FAST_TRAVEL()
			PRINTLN("[AM_MP_DRONE] - SHOULD_RETURN_DRONE_TO_PLAYER_AND_KILL_SCRIPT fast travel in progress")
			RETURN TRUE
		ENDIF
		
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AIRDEF_WARN")
			IF HAS_NET_TIMER_EXPIRED(sDroneControl.sAirSpaceTimer, 2000)
				RETURN TRUE
				PRINTLN("[AM_MP_DRONE] - SHOULD_RETURN_DRONE_TO_PLAYER_AND_KILL_SCRIPT missile in yacht air defence area")
			ENDIF
		ELSE
			RESET_NET_TIMER(sDroneControl.sAirSpaceTimer)
		ENDIF
	ENDIF
	#ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_DRONE_MAX_ZOOM()
 	IF IS_DRONE_STARTED_FROM_TRUCK()
		SWITCH sDroneControl.iCamZoomLevel
			CASE 0
				sDroneControl.fMaxZoom 		= 90
				sDroneControl.fCamFOVTarget = 90
			BREAK
			CASE 1
				sDroneControl.fMaxZoom 		= 75.0
				sDroneControl.fCamFOVTarget = 75.0
			BREAK
			CASE 2
				sDroneControl.fMaxZoom 		= 45
				sDroneControl.fCamFOVTarget = 45
			BREAK
		ENDSWITCH
	ELSE
		SWITCH sDroneControl.iCamZoomLevel
			CASE 0
				sDroneControl.fMaxZoom 		= 90
				sDroneControl.fCamFOVTarget = 90
			BREAK
			CASE 1
				sDroneControl.fMaxZoom 		= 80.0
				sDroneControl.fCamFOVTarget = 80.0
			BREAK
			CASE 2
				sDroneControl.fMaxZoom 		= 70.0
				sDroneControl.fCamFOVTarget = 70.0
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

/// PURPOSE:
///    Update drone camera movement
PROC UPDATE_DRONE_CAMERA()
	
	IF IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_CAMERA_FUZZ)
	OR SHOULD_CLEANUP_DRONE_CAMERA_INSTANTLY()
	OR IS_DRONE_USED_AS_GUIDED_MISSILE()
		EXIT
	ENDIF
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sDroneControl.cameraProp)
	AND NOT IS_ENTITY_DEAD(NET_TO_OBJ(sDroneControl.cameraProp))
		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(sDroneControl.cameraProp)
			CONST_FLOAT FPC_ROT_INTERP			0.050
			CONST_FLOAT FPC_FOV_INTERP			0.060
			FLOAT fPitchRollSpeed = 40.0
			FLOAT fRollLimit = 19
			FLOAT fCamYLimit = 14
			INT fMouseSensitivity = 1
			FLOAT fDoneRoll = GET_ENTITY_ROLL(NET_TO_OBJ(sDroneControl.cameraProp))
			FLOAT fPropPitch = GET_ENTITY_PITCH(NET_TO_OBJ(sDroneControl.cameraProp))

			IF IS_DRONE_STARTED_FROM_HEIST_FREEMODE()
				fRollLimit = 10
			ENDIF
			
			//*** Steps all camera interpolated values towards their target values. ***
			//Scale by timestep to ensure this is frame rate independent.
			FLOAT fFrameRateModifier = 30.0 * TIMESTEP()
			
			MAINTAIN_DRONE_MAX_ZOOM()
			
			ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(FRONTEND_CONTROL)
			
			//Get analogue stick positions.
			GET_CONTROL_VALUE_OF_ANALOGUE_STICKS_UNBOUND(sDroneControl.iStickPosition[DRONE_LEFT_X],
											 	 sDroneControl.iStickPosition[DRONE_LEFT_Y],
											 	 sDroneControl.iStickPosition[DRONE_RIGHT_X],
											 	 sDroneControl.iStickPosition[DRONE_RIGHT_Y])
			
			IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
				fMouseSensitivity = 5
				sDroneControl.iStickPosition[DRONE_RIGHT_X] = sDroneControl.iStickPosition[DRONE_RIGHT_X] * fMouseSensitivity
				sDroneControl.iStickPosition[DRONE_RIGHT_Y] = sDroneControl.iStickPosition[DRONE_RIGHT_Y] * fMouseSensitivity
			ENDIF									 					
												 
			// Invert gamepad stick if look is inverted in settings.
			IF IS_LOOK_INVERTED()
				 sDroneControl.iStickPosition[DRONE_RIGHT_Y] *= -1
			ENDIF
			
			//Interpolate camera FOV.
			BOOL bZoomControl, bScrollUp
			
			IF HAS_NET_TIMER_EXPIRED(sDroneControl.sPCZoomDelay, 750)
				IF !IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
					bZoomControl = IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_FRONTEND_RS)
				ELSE
					bZoomControl = IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_CURSOR_SCROLL_UP) | IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_CURSOR_SCROLL_DOWN)
					IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_CURSOR_SCROLL_UP)
						bScrollUp = TRUE
					ENDIF
				ENDIF
				IF bZoomControl
					REINIT_NET_TIMER(sDroneControl.sPCZoomDelay)
				ENDIF	
			ENDIF
			
			IF bZoomControl
			AND NOT IS_DRONE_BOOST_ACTIVATED()
				IF !IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
					sDroneControl.iCamZoomLevel ++
				ELSE
					IF bScrollUp
						sDroneControl.iCamZoomLevel --
					ELSE
						sDroneControl.iCamZoomLevel ++
					ENDIF
				ENDIF
				IF HAS_SOUND_FINISHED(sDroneControl.iCamZoomSound)
					sDroneControl.iCamZoomSound = GET_SOUND_ID()
					PLAY_SOUND_FRONTEND(sDroneControl.iCamZoomSound, "HUD_Zoom_Change", sDroneControl.sSoundSet)
				ENDIF	
				IF sDroneControl.iCamZoomLevel > 2
					sDroneControl.iCamZoomLevel = 0
				ELIF sDroneControl.iCamZoomLevel < 0
					sDroneControl.iCamZoomLevel = 2
				ENDIF	
			ELSE
				IF HAS_SOUND_FINISHED(sDroneControl.iCamZoomSound)
					STOP_SOUND(sDroneControl.iCamZoomSound)
					RELEASE_SOUND_ID(sDroneControl.iCamZoomSound)
					sDroneControl.iCamZoomSound = -1
				ENDIF
			ENDIF

			sDroneControl.fCamFOVCurrent += (sDroneControl.fCamFOVTarget - sDroneControl.fCamFOVCurrent) * FPC_FOV_INTERP * fFrameRateModifier
			
			SET_CAM_FOV(sDroneControl.droneCamera, sDroneControl.fCamFOVCurrent)
	
			//Update the camera prop with interpolated values.
			IF (sDroneControl.iStickPosition[DRONE_RIGHT_X] != 0 
			OR sDroneControl.iStickPosition[DRONE_RIGHT_Y] != 0)
			
				FLOAT rotationZAngle, rotationXAngle
				IF sDroneControl.iStickPosition[DRONE_RIGHT_X] != 0
					rotationZAngle = 1.0 / (127.0 / sDroneControl.iStickPosition[DRONE_RIGHT_X])
				ELSE
					rotationZAngle = 0
				ENDIF
				
				IF sDroneControl.iStickPosition[DRONE_RIGHT_Y] != 0
					rotationXAngle = 1.0 / (127.0 / sDroneControl.iStickPosition[DRONE_RIGHT_Y])
				ELSE
					rotationXAngle = 0
				ENDIF
				
				FLOAT vCameraRotationAngleX, vCameraRotationAngleZ, vCameraRotationAngleY
				VECTOR vPropRot = GET_ENTITY_ROTATION(NET_TO_OBJ(sDroneControl.cameraProp))
				vCameraRotationAngleX = (rotationXAngle * FPC_ROT_INTERP * fFrameRateModifier * sDroneControl.fInterpSpeed) 
				vCameraRotationAngleZ = -(rotationZAngle * FPC_ROT_INTERP * fFrameRateModifier * sDroneControl.fInterpSpeed)

				VECTOR vCameraRotationAngle
				
				// Drone roll check
				IF (fDoneRoll != 0.0 
				OR sDroneControl.iStickPosition[DRONE_LEFT_X] != 0)
				AND NOT IS_DRONE_SNAPMATIC_ACTIVE()
				
					FLOAT rotationYAngle
					IF sDroneControl.iStickPosition[DRONE_LEFT_X] != 0	
						rotationYAngle = 1.0 / (127.0 / sDroneControl.iStickPosition[DRONE_LEFT_X])
						vCameraRotationAngleY = -(rotationYAngle * FPC_ROT_INTERP * fFrameRateModifier * (fPitchRollSpeed - 25)) 
					ELSE
						IF fDoneRoll > 0.1
						OR fDoneRoll < -0.1
							IF fDoneRoll > 0
								rotationYAngle = -1
							ELSE
								rotationYAngle = 1
							ENDIF
						ENDIF	
						
						IF vPropRot.y != 0.0
						IF (vPropRot.y < 1.5
						AND vPropRot.y > 0.0)
							rotationYAngle = 0.001
						ELIF (vPropRot.y > -1.5
						AND vPropRot.y < 0.0)
							rotationYAngle = -0.001
							ENDIF
						ELSE
							rotationYAngle = 0
						ENDIF
						
						vCameraRotationAngleY = -(rotationYAngle * FPC_ROT_INTERP * fFrameRateModifier * (fPitchRollSpeed - 25)) 
					ENDIF
				ELSE
				 	vCameraRotationAngleY = 0
				ENDIF	
				
				vCameraRotationAngle = << vCameraRotationAngleX, vCameraRotationAngleY, vCameraRotationAngleZ >> + vPropRot
				
				// Drone roll limit check
				IF fDoneRoll != 0.0
					IF sDroneControl.iStickPosition[DRONE_LEFT_X] = 0
						IF vCameraRotationAngle.y > fRollLimit
							vCameraRotationAngle.y = fRollLimit
						ELIF vCameraRotationAngle.y < -fRollLimit
							vCameraRotationAngle.y = -fRollLimit
						ENDIF
					ELSE
						IF vCameraRotationAngle.y > fRollLimit
							vCameraRotationAngle.y = fRollLimit
						ELIF vCameraRotationAngle.y < -fRollLimit
							vCameraRotationAngle.y = -fRollLimit
						ENDIF
					ENDIF
				ENDIF
				
				IF vCameraRotationAngle.x > fCamYLimit
					vCameraRotationAngle.x = fCamYLimit
				ELIF vCameraRotationAngle.x < -fCamYLimit
					vCameraRotationAngle.x = -fCamYLimit
				ENDIF
				
				IF NOT IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_ROTATE_X_AXIS)
					SET_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_ROTATE_X_AXIS)
				ENDIF
				
				SET_ENTITY_ROTATION(NET_TO_OBJ(sDroneControl.cameraProp),<<0,0,0>> + <<vCameraRotationAngle.x, vCameraRotationAngle.y, vCameraRotationAngle.z>>)	
					
			// Add roll to camera movement 
			ELIF ((sDroneControl.iStickPosition[DRONE_LEFT_X] != 0 
			OR sDroneControl.iStickPosition[DRONE_LEFT_Y] != 0) AND NOT IS_DRONE_BOOST_ACTIVATED())
			AND NOT IS_DRONE_SNAPMATIC_ACTIVE()

				VECTOR vPropRot = GET_ENTITY_ROTATION(NET_TO_OBJ(sDroneControl.cameraProp))
				
				FLOAT rotationYAngle, rotationXAngle
				IF IS_DRONE_BOOST_ACTIVATED() 
					IF IS_DRONE_BOOST_DIR_FORWARD()
						rotationXAngle = -1
						sDroneControl.fRotationalSpeedForSound = rotationXAngle
					ENDIF	
				ELSE	
					IF sDroneControl.iStickPosition[DRONE_LEFT_X] != 0
						rotationYAngle = 1.0 / (127.0 / sDroneControl.iStickPosition[DRONE_LEFT_X])
						sDroneControl.fRotationalSpeedForSound = rotationYAngle
					ELSE
						rotationYAngle = 0
					ENDIF
					
					IF sDroneControl.iStickPosition[DRONE_LEFT_Y] != 0
						rotationXAngle = 1.0 / (127.0 / sDroneControl.iStickPosition[DRONE_LEFT_Y])
						sDroneControl.fRotationalSpeedForSound = rotationYAngle
					ELSE
						rotationXAngle = 0
					ENDIF
				ENDIF 	
				
				FLOAT vCameraRotationAngleX, vCameraRotationAngleY
				vCameraRotationAngleX = -(rotationXAngle * FPC_ROT_INTERP * fFrameRateModifier * fPitchRollSpeed) 
				
				vCameraRotationAngleY = -(rotationYAngle * FPC_ROT_INTERP * fFrameRateModifier * fPitchRollSpeed)
				
				// Drone roll check
				IF fDoneRoll != 0.0
					IF (sDroneControl.iStickPosition[DRONE_LEFT_X] = 0 AND NOT IS_DRONE_SNAPMATIC_ACTIVE())
					OR IS_DRONE_BOOST_ACTIVATED()
						IF fDoneRoll > 1.0
						OR fDoneRoll < -1.0
							IF fDoneRoll > 0
								rotationYAngle = -1
							ELSE
								rotationYAngle = 1
							ENDIF
						ENDIF	
						
						IF (vPropRot.y < 1.5
						AND vPropRot.y > 0.0)
							rotationYAngle = 0.001
						ELIF (vPropRot.y > -1.5
						AND vPropRot.y < 0.0)
							rotationYAngle = -0.001
						ENDIF
							
						vCameraRotationAngleY = -(rotationYAngle * FPC_ROT_INTERP * fFrameRateModifier * (fPitchRollSpeed - 25)) 	
					ENDIF
				ENDIF	
				
				VECTOR vCameraRotationAngle = << vCameraRotationAngleX , vCameraRotationAngleY , 0 >> + vPropRot
				
				IF vCameraRotationAngle.y > fRollLimit
					vCameraRotationAngle.y = fRollLimit
				ELIF vCameraRotationAngle.y < -fRollLimit
					vCameraRotationAngle.y = -fRollLimit
				ENDIF
				
				IF vCameraRotationAngle.x > fCamYLimit
					vCameraRotationAngle.x = fCamYLimit
				ELIF vCameraRotationAngle.x < -fCamYLimit
					vCameraRotationAngle.x = -fCamYLimit
				ENDIF
				
				IF IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_ROTATE_X_AXIS)
					CLEAR_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_ROTATE_X_AXIS)
				ENDIF
					
				SET_ENTITY_ROTATION(NET_TO_OBJ(sDroneControl.cameraProp),<<0,0,0>> + <<vCameraRotationAngle.x, vCameraRotationAngle.y, vPropRot.z>>)
				
			ELSE
				IF !IS_DRONE_BOOST_DIR_FORWARD() 
				AND NOT IS_DRONE_BOOST_ACTIVATED()
					sDroneControl.fRotationalSpeedForSound = 0
					
					VECTOR vPropRot = GET_ENTITY_ROTATION(NET_TO_OBJ(sDroneControl.cameraProp))
					
					IF fDoneRoll != 0.0
					OR fPropPitch != 0.0
					
						FLOAT rotationYAngle, rotationXAngle
						
						IF vPropRot.y != 0.0
							IF vPropRot.y < 0
								rotationYAngle = -1
							ELSE
								rotationYAngle = 1
							ENDIF
						ELSE
							rotationYAngle = 0
						ENDIF
						
						IF fPropPitch != 0.0
							IF vPropRot.x < 0
								rotationXAngle = -1
							ELSE
								rotationXAngle =  1
							ENDIF
						ELSE	
							rotationXAngle = 0
						ENDIF	
						
						
						// Smaller value to smother angle between 0 and 1 or 0 and -1
						IF vPropRot.y != 0.0
							IF (vPropRot.y < 1.5
							AND vPropRot.y > 0.0)
								rotationYAngle = 0.001
							ELIF (vPropRot.y > -1.5
							AND vPropRot.y < 0.0)
								rotationYAngle = -0.001
							ENDIF
						ELSE
							rotationYAngle = 0
						ENDIF	
						
						IF vPropRot.x != 0.0
							IF (vPropRot.x < 1.5
							AND vPropRot.x > 0.0)
								rotationXAngle = 0.001
							ELIF (vPropRot.x > -1.5
							AND vPropRot.x < 0.0)
								rotationXAngle = -0.001
							ENDIF
						ELSE
							rotationXAngle = 0
						ENDIF	
						
						FLOAT vCameraRotationAngleX, vCameraRotationAngleY
						vCameraRotationAngleX = NORMALIZE_ANGLE(-(rotationXAngle * FPC_ROT_INTERP * fFrameRateModifier * (fPitchRollSpeed - 25))) 		// Pitch
						
						vCameraRotationAngleY = NORMALIZE_ANGLE(-(rotationYAngle * FPC_ROT_INTERP * fFrameRateModifier * (fPitchRollSpeed - 25)))		// Roll
						
						VECTOR vCameraRotationAngle = << vCameraRotationAngleX , vCameraRotationAngleY , 0 >> + vPropRot
						
						IF IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_ROTATE_X_AXIS)
							// We are not going rotate x if Right Stick is changed
							vCameraRotationAngle.x =  vPropRot.x
						ENDIF
						
						SET_ENTITY_ROTATION(NET_TO_OBJ(sDroneControl.cameraProp),<<0,0,0>> + <<vCameraRotationAngle.x, vCameraRotationAngle.y, vPropRot.z>>)
					ENDIF
				ENDIF
			ENDIF
			
		ELSE
			NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(sDroneControl.cameraProp)
		ENDIF
	ENDIF	
ENDPROC

PROC GET_VECTOR_FROM_ROTATION(VECTOR &InVec)
      
    FLOAT CosAngle
    FLOAT SinAngle
    VECTOR ReturnVec

    // Rotation about the x axis 
    CosAngle = COS(0)
    SinAngle = SIN(0)
    ReturnVec.x = InVec.x
    ReturnVec.y = (CosAngle * InVec.y) - (SinAngle * InVec.z)
    ReturnVec.z = (SinAngle * InVec.y) + (CosAngle * InVec.z)
    InVec = ReturnVec

    // Rotation about the y axis
    CosAngle = COS(0)
    SinAngle = SIN(0)
    ReturnVec.x = (CosAngle * InVec.x) + (SinAngle * InVec.z)
    ReturnVec.y = InVec.y
    ReturnVec.z = (CosAngle * InVec.z) - (SinAngle * InVec.x) 
    InVec = ReturnVec
    
    // Rotation about the z axis 
    CosAngle = COS(0)
    SinAngle = SIN(0)
    ReturnVec.x = (CosAngle * InVec.x) - (SinAngle * InVec.y)
    ReturnVec.y = (SinAngle * InVec.x) + (CosAngle * InVec.y)
    ReturnVec.z = InVec.z
    InVec = ReturnVec
ENDPROC

FUNC VECTOR GET_CAMERA_INIT_FORWARD_VECTOR()
	VECTOR vEnd
	//end vector - pointing north
	vEnd	= <<0.0, 1.0, 0.0>>
	GET_VECTOR_FROM_ROTATION(vEnd)
	RETURN vEnd
ENDFUNC

FUNC BOOL HAS_DRONE_REACHED_HEIGHT_LIMIT()
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sDroneControl.cameraProp)
	AND !IS_ENTITY_DEAD(NET_TO_OBJ(sDroneControl.cameraProp))
		FLOAT fGroundZero 
		
		INT iLimit = g_sMPTunables.iBB_TERRORBYTE_DRONE_HEIGHT_LIMIT
		#IF FEATURE_HEIST_ISLAND
		IF IS_DRONE_STARTED_FROM_SUBMARINE_MISSILE()
			iLimit *= 10
		ENDIF
		#ENDIF
		
		IF	GET_GROUND_Z_FOR_3D_COORD(sDroneControl.vDroneCoords, fGroundZero, TRUE)
			sDroneControl.fHeightDistanceBetDroneGround = sDroneControl.vDroneCoords.z - fGroundZero 
			IF sDroneControl.fHeightDistanceBetDroneGround > iLimit
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Check if drone is going far from player or truck
/// RETURNS:
///    
FUNC BOOL HAS_DRONE_REACHED_PLAYER_DISTANCE()
	
	IF SHOULD_IGNORE_DISTANCE_BETWEEN_PLAYER_AND_DRONE_CHECK()
		RETURN FALSE
	ENDIF
	
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sDroneControl.cameraProp)
		AND !IS_ENTITY_DEAD(NET_TO_OBJ(sDroneControl.cameraProp))
			VECTOR controlCoordsToCheck
			IF IS_DRONE_STARTED_FROM_TRUCK()
				IF DOES_ENTITY_EXIST(GET_HACKER_TRUCK_INDEX_PED_IS_IN_INTERIOR())
				AND NOT IS_ENTITY_DEAD(GET_HACKER_TRUCK_INDEX_PED_IS_IN_INTERIOR())
					controlCoordsToCheck = GET_ENTITY_COORDS(GET_HACKER_TRUCK_INDEX_PED_IS_IN_INTERIOR())
				ENDIF
			ELIF IS_DRONE_STARTED_FROM_HEIST_FREEMODE()
				IF NOT SHOULD_START_DRONE_FROM_PLAYER_PHONE()
					SIMPLE_INTERIORS ownedSimpleInterior =  GET_OWNED_ARCADE_SIMPLE_INTERIOR(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner)
					SWITCH	ownedSimpleInterior
						CASE SIMPLE_INTERIOR_ARCADE_PALETO_BAY		controlCoordsToCheck = <<-245.64, 6210.96, 35.94>>		BREAK
						CASE SIMPLE_INTERIOR_ARCADE_GRAPESEED		controlCoordsToCheck =  <<1695.88, 4783.87, 47.02>>		BREAK
						CASE SIMPLE_INTERIOR_ARCADE_DAVIS			controlCoordsToCheck =  <<-115.15, -1771.65, 38.86>>	BREAK
						CASE SIMPLE_INTERIOR_ARCADE_WEST_VINEWOOD	controlCoordsToCheck =  <<-600.96, 280.47, 87.04>>		BREAK
						CASE SIMPLE_INTERIOR_ARCADE_ROCKFORD_HILLS	controlCoordsToCheck =  <<-1269.72, -304.09, 40.00>>	BREAK
						CASE SIMPLE_INTERIOR_ARCADE_LA_MESA			controlCoordsToCheck =  <<758.46, -814.57, 30.30>>		BREAK
					ENDSWITCH
				ELSE
					controlCoordsToCheck = GET_ENTITY_COORDS(PLAYER_PED_ID())
				ENDIF
			#IF FEATURE_HEIST_ISLAND
			ELIF IS_DRONE_STARTED_FROM_SUBMARINE_MISSILE()
				IF IS_ENTITY_ALIVE(GET_SUBMARINE_INDEX_PED_IS_IN_INTERIOR())
					controlCoordsToCheck = GET_ENTITY_COORDS(GET_SUBMARINE_INDEX_PED_IS_IN_INTERIOR())
				ENDIF
			#ENDIF	
			ELSE
				controlCoordsToCheck = GET_ENTITY_COORDS(PLAYER_PED_ID())
			ENDIF
			IF IS_ENTITY_ALIVE(NET_TO_OBJ(sDroneControl.cameraProp))
				sDroneControl.fDistanceBetDroneAndControl = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(NET_TO_OBJ(sDroneControl.cameraProp), controlCoordsToCheck)
				IF	sDroneControl.fDistanceBetDroneAndControl > GET_DRONE_DISTANCE_LIMIT()
					PRINTLN("[AM_MP_DRONE] - HAS_DRONE_REACHED_PLAYER_DISTANCE sDroneControl.fDistanceBetDroneAndControl: ", sDroneControl.fDistanceBetDroneAndControl, " DRONE_DISTANCE_LIMIT: ", GET_DRONE_DISTANCE_LIMIT())
					RETURN TRUE
				ENDIF
			ENDIF	
		ENDIF
	ENDIF	
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_PROCESS_DRONE_CLEANUP()
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sDroneControl.cameraProp)
	AND IS_ENTITY_DEAD(NET_TO_OBJ(sDroneControl.cameraProp))
	AND NOT IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_CAMERA_EXPLOSION)
		PRINTLN("[AM_MP_DRONE] - SHOULD_PROCESS_DRONE_CLEANUP Drone is dead")
		RETURN TRUE
	ENDIF
	
	IF sDroneControl.iDroneCollisionHitSomthing = DRONE_CAM_AREA_CHECK_HIT_SOMETHING
		sDroneControl.iDroneStatEndReason = DRONE_CLEANUP_REASON_COLLISION
		PRINTLN("[AM_MP_DRONE] - SHOULD_PROCESS_DRONE_CLEANUP Drone hit something")
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_CAMERA_FUZZ)
		PRINTLN("[AM_MP_DRONE] - SHOULD_PROCESS_DRONE_CLEANUP Drone camera fuzz already started")
		RETURN TRUE
	ENDIF
	
	IF HAS_DRONE_REACHED_HEIGHT_LIMIT()
		sDroneControl.iDroneStatEndReason = DRONE_CLEANUP_REASON_DISTANCE_LIMIT
		PRINTLN("[AM_MP_DRONE] - SHOULD_PROCESS_DRONE_CLEANUP Drone reached height limit")
		RETURN TRUE
	ENDIF
	
	IF IS_DRONE_STARTED_FROM_TRUCK()
	AND HAS_DRONE_REACHED_PLAYER_DISTANCE()
		sDroneControl.iDroneStatEndReason = DRONE_CLEANUP_REASON_DISTANCE_LIMIT
		PRINTLN("[AM_MP_DRONE] - SHOULD_PROCESS_DRONE_CLEANUP truck Drone is too far from player")
		RETURN TRUE
	ENDIF
	
	IF SHOULD_START_DRONE_FROM_PLAYER_PHONE()
	#IF FEATURE_HEIST_ISLAND
	OR IS_DRONE_STARTED_FROM_SUBMARINE_MISSILE()
	#ENDIF
		IF HAS_DRONE_REACHED_PLAYER_DISTANCE()
			sDroneControl.iDroneStatEndReason = DRONE_CLEANUP_REASON_DISTANCE_LIMIT
			PRINTLN("[AM_MP_DRONE] - SHOULD_PROCESS_DRONE_CLEANUP Drone is too far from player")
			RETURN TRUE
		ENDIF
	ENDIF

	IF IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_CAMERA_EXPLOSION)
		sDroneControl.iDroneStatEndReason = DRONE_CLEANUP_REASON_DETROYED_BY_OWN_DETONATE
		PRINTLN("[AM_MP_DRONE] - SHOULD_PROCESS_DRONE_CLEANUP Drone exploded by own detonate")
		RETURN TRUE
	ENDIF
	
	IF HAS_DRONE_TARGET_HIT()
		IF NOT HAS_NET_TIMER_STARTED(sDroneControl.sMissionDronKillTimer)
			START_NET_TIMER(sDroneControl.sMissionDronKillTimer)
		ELSE
			IF HAS_NET_TIMER_EXPIRED(sDroneControl.sMissionDronKillTimer, 3000)
				sDroneControl.iDroneStatEndReason = DRONE_CLEANUP_REASON_RETURNED_TO_PLAYER
				PRINTLN("[AM_MP_DRONE] - SHOULD_PROCESS_DRONE_CLEANUP HAS_DRONE_TARGET_HIT TRUE")
				RETURN TRUE
			ENDIF
		ENDIF	
	ENDIF
	
	IF IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_EXIT_DRONE)
		sDroneControl.iDroneStatEndReason = DRONE_CLEANUP_REASON_RETURNED_TO_PLAYER
		PRINTLN("[AM_MP_DRONE] - SHOULD_PROCESS_DRONE_CLEANUP DRONE_LOCAL_BS_EXIT_DRONE TRUE")
		RETURN TRUE
	ENDIF
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sDroneControl.cameraProp)
		IF IS_ENTITY_IN_WATER(NET_TO_OBJ(sDroneControl.cameraProp))
		OR IS_ENTITY_IN_DEEP_WATER(NET_TO_ENT(sDroneControl.cameraProp))
			IF IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_TURN_ON_DRONE_COLLISION)
				sDroneControl.iDroneStatEndReason = DRONE_CLEANUP_REASON_DROPPED_IN_WATER
				PRINTLN("[AM_MP_DRONE] - SHOULD_PROCESS_DRONE_CLEANUP drone is in water TRUE")
				RETURN TRUE
			ENDIF	
		ENDIF
	ENDIF
	
	IF !IS_DRONE_STARTED_FROM_TRUCK()
	AND !IS_DRONE_STARTED_FROM_DEBUG()
		IF sDroneControl.vDroneCoords.z >= GET_DRONE_DISTANCE_LIMIT()		
			sDroneControl.iDroneStatEndReason = DRONE_CLEANUP_REASON_DISTANCE_LIMIT
			PRINTLN("[AM_MP_DRONE] - SHOULD_PROCESS_DRONE_CLEANUP Drone hit height limit")
			RETURN TRUE
		ENDIF	
	ENDIF
	
	RETURN FALSE
ENDFUNC 

PROC UPDATE_LOCAL_VARIABLES()
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sDroneControl.cameraProp)
		IF NOT IS_ENTITY_DEAD(NET_TO_VEH(sDroneControl.cameraProp))
			sDroneControl.vDroneCoords 	= GET_ENTITY_COORDS(NET_TO_VEH(sDroneControl.cameraProp))
			sDroneControl.fDroneHeading	= GET_ENTITY_HEADING(NET_TO_VEH(sDroneControl.cameraProp))
			IF NOT HAS_NET_TIMER_STARTED(sDroneControl.sDroneBroadcastTimer)
				START_NET_TIMER(sDroneControl.sDroneBroadcastTimer)
			ELSE
				IF HAS_NET_TIMER_EXPIRED(sDroneControl.sDroneBroadcastTimer, 1000)
					IF !ARE_VECTORS_EQUAL(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].vDroneCoords, sDroneControl.vDroneCoords)
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].vDroneCoords = sDroneControl.vDroneCoords
					ENDIF	
					IF GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].fDroneHeading != GET_ENTITY_HEADING(NET_TO_VEH(sDroneControl.cameraProp))
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].fDroneHeading = GET_ENTITY_HEADING(NET_TO_VEH(sDroneControl.cameraProp))
					ENDIF	
					RESET_NET_TIMER(sDroneControl.sDroneBroadcastTimer)
				ENDIF
			ENDIF	
		ENDIF
	ENDIF
	
	IF IS_ENTITY_ALIVE(sDroneControl.fakeVeh)
		IF IS_ENTITY_VISIBLE(sDroneControl.fakeVeh)
			SET_ENTITY_VISIBLE(sDroneControl.fakeVeh, FALSE)
		ENDIF
	ENDIF
	IF IS_ENTITY_ALIVE(sDroneControl.fakePed)
		IF IS_ENTITY_VISIBLE(sDroneControl.fakePed)
			SET_ENTITY_VISIBLE(sDroneControl.fakePed, FALSE)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Check if detonate option should be blocked for drone
/// PARAMS:
///    bTempBlocked - temporary blocked waiting on stun gun 
/// RETURNS:
///    
FUNC BOOL SHOULD_BLOCK_EXPLOSIVE(BOOL bTempBlocked = TRUE)
	
	IF IS_DRONE_STARTED_FROM_TRUCK()
		IF bTempBlocked
			IF IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_CAMERA_STUN_GUN)
				RETURN TRUE
			ENDIF
		ENDIF	
	ENDIF
	
	IF IS_DRONE_STARTED_FROM_CASINO_HEIST_MISSON()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC 

FUNC BOOL SHOULD_DRONE_HAVE_TRANQUILLISER_GUN_EQUIPPED()
	IF GET_DRONE_TRANQUILIZER_AMMO() <= 0
		RETURN FALSE
	ENDIF	
	
	IF IS_DRONE_STARTED_FROM_CASINO_HEIST_MISSON()
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC 

PROC SCALEFORM_SET_TRANQUILLISER_PERCENTAGE(INT iPecentage)
	BEGIN_SCALEFORM_MOVIE_METHOD(sDroneControl.droneStunSF, "SET_TRANQUILIZE_PERCENTAGE")
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPecentage)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC SCALEFORM_SET_SHOCK_PERCENTAGE(INT iPecentage)
	BEGIN_SCALEFORM_MOVIE_METHOD(sDroneControl.droneStunSF, "SET_SHOCK_PERCENTAGE")
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPecentage)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC SCALEFORM_SET_BOOST_PERCENTAGE(INT iPecentage)
	BEGIN_SCALEFORM_MOVIE_METHOD(sDroneControl.droneStunSF, "SET_BOOST_PERCENTAGE")
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPecentage)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC SCALEFORM_SET_DETONATE_PERCENTAGE(INT iPecentage)
	BEGIN_SCALEFORM_MOVIE_METHOD(sDroneControl.droneStunSF, "SET_DETONATE_PERCENTAGE")
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPecentage)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC SCALEFORM_SET_EMP_PERCENTAGE(INT iPecentage)
	BEGIN_SCALEFORM_MOVIE_METHOD(sDroneControl.droneStunSF, "SET_EMP_PERCENTAGE")
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPecentage)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC SCALEFORM_SET_RETICLE_PERCENTAGE(INT iPecentage)
	BEGIN_SCALEFORM_MOVIE_METHOD(sDroneControl.droneStunSF, "SET_RETICLE_PERCENTAGE")
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPecentage)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC SCALEFORM_SET_HEADING(INT iHeadingAngle)
	BEGIN_SCALEFORM_MOVIE_METHOD(sDroneControl.droneStunSF, "SET_HEADING")
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iHeadingAngle)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC SCALEFORM_SET_ZOOM(INT iZoom)
	BEGIN_SCALEFORM_MOVIE_METHOD(sDroneControl.droneStunSF, "SET_ZOOM")
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iZoom)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

/// PURPOSE:
///    Set reticle target colour
/// PARAMS:
///    bOnTarget - True for red , False for white
PROC SCALEFORM_SET_RETICLE_ON_TARGET(BOOL bOnTarget)
	BEGIN_SCALEFORM_MOVIE_METHOD(sDroneControl.droneStunSF, "SET_RETICLE_ON_TARGET")
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bOnTarget)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

/// PURPOSE:
///    Set Reticle state
/// PARAMS:
///    INT iState 0: Normal (diagonal lines are hidden, centre circle doesn't flash) 1: Fire (diagonal lines are shown briefly, outer curves animate) 2: Charging (centre circle flashes)
PROC SCALEFORM_SET_RETICLE_STATE(INT iState)
	BEGIN_SCALEFORM_MOVIE_METHOD(sDroneControl.droneStunSF, "SET_RETICLE_STATE")
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iState)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC SCALEFORM_SET_PERSONAL_INFO(INT iRank, INT iEarnings, INT iKills, INT iDeaths, INT iSuicides, FLOAT fAccuracy, STRING sRadioStation, STRING sWeapon, STRING sVehicle, INT iNumSexActs)
	BEGIN_SCALEFORM_MOVIE_METHOD(sDroneControl.droneStunSF, "SET_INFO_LIST_DATA")
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iRank)
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iEarnings)
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iKills)
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iDeaths)
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSuicides)
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fAccuracy)
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sRadioStation)
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sWeapon)
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sVehicle)
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iNumSexActs)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC SCALEFORM_ATTENUATE_SOUND_WAVE(FLOAT fAttenuation)
	BEGIN_SCALEFORM_MOVIE_METHOD(sDroneControl.droneStunSF, "ATTENUATE_SOUND_WAVE")
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fAttenuation)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC SCALEFORM_WARNING_FLASH_RATE(FLOAT fFlashRate)
	BEGIN_SCALEFORM_MOVIE_METHOD(sDroneControl.droneStunSF, "SET_WARNING_FLASH_RATE")
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fFlashRate)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

FUNC FLOAT GET_SCALEFORM_SOUND_WAVE_VALUE()
	IF GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = ENUM_TO_INT(FMMC_TYPE_FMBB_TARGET_PURSUIT) 
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sDroneControl.cameraProp)
		AND NOT IS_ENTITY_DEAD(NET_TO_OBJ(sDroneControl.cameraProp))
			
			FLOAT fDis = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(NET_TO_OBJ(sDroneControl.cameraProp), GET_START_VECTOR_BUSINESS_BATTLES_TARGET_PURSUIT())

			IF fDis >= 200
				RETURN 0.0
			ELIF fDis >= 180 
			AND fDis < 200
				RETURN 0.1
			ELIF fDis >= 160
			AND fDis < 180
				RETURN 0.2
			ELIF fDis >= 140
			AND fDis < 160
				RETURN 0.3
			ELIF fDis >= 120
			AND fDis < 140
				RETURN 0.4
			ELIF fDis >= 100
			AND fDis < 120
				RETURN 0.5	
			ELIF fDis >= 80
			AND fDis < 100
				RETURN 0.6
			ELIF fDis >= 60
			AND fDis < 80
				RETURN 0.7
			ELIF fDis >= 40
			AND fDis < 60
				RETURN 0.8
			ELIF fDis >= 20
			AND fDis < 40
				RETURN 0.9
			ELIF fDis <= 20
				RETURN 1.0	
			ENDIF
			
		ENDIF
	ENDIF
	
	RETURN 0.0
ENDFUNC

PROC PLAY_SCANNER_SOUND(FLOAT fSignalStrenghtSound)
	IF HAS_SOUND_FINISHED(sDroneControl.iScannerLoop)
		sDroneControl.iScannerLoop = GET_SOUND_ID()
		PLAY_SOUND_FRONTEND(sDroneControl.iScannerLoop, "Scanner_Loop", "DLC_BTL_Target_Pursuit_Sounds")
		SET_VARIABLE_ON_SOUND(sDroneControl.iScannerLoop, "signalstrength", fSignalStrenghtSound)
	ELSE
		SET_VARIABLE_ON_SOUND(sDroneControl.iScannerLoop, "signalstrength", fSignalStrenghtSound)
	ENDIF
ENDPROC

PROC MANAGE_DRONE_SCALEFORM_ZOOM_LEVEL()
	SWITCH sDroneControl.iCamZoomLevel
		CASE 0 
			SCALEFORM_SET_ZOOM(0)
		BREAK	
		CASE 1
			SCALEFORM_SET_ZOOM(2)
		BREAK	
		CASE 2
			SCALEFORM_SET_ZOOM(4)
		BREAK
	ENDSWITCH	
ENDPROC 

PROC MANAGE_DRONE_SCALEFORM_STUN_GUN_RECHARGER()
	IF IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_CAMERA_STUN_GUN)
	OR (IS_BIT_SET(sDroneControl.iLocalBS,DRONE_LOCAL_BS_CAMERA_EMP) AND IS_DRONE_STARTED_FROM_ARENA_WARS_ONE())
		IF sDroneControl.iStunRechargerFill = 100
			SCALEFORM_SET_RETICLE_STATE(1)
		ELIF sDroneControl.iStunRechargerFill != 0 
		AND sDroneControl.iStunRechargerFill != 100
			SCALEFORM_SET_RETICLE_STATE(2)
		ENDIF	
	ELSE
		SCALEFORM_SET_RETICLE_STATE(0)
	ENDIF
ENDPROC	

PROC MANAGE_DRONE_SCALEFORM_TARGET_COLOUR()
	IF IS_DRONE_STARTED_FROM_ARENA_WARS_ONE()
		IF IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_AIMING_AT_PLAYER)
		AND !sDroneControl.bDroneEMPCoolDown
			SCALEFORM_SET_RETICLE_ON_TARGET(TRUE)
		ELSE
			SCALEFORM_SET_RETICLE_ON_TARGET(FALSE)
		ENDIF
	ELSE
		IF IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_AIMING_AT_PLAYER)
		OR IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_AIMING_AT_PED)
			SCALEFORM_SET_RETICLE_ON_TARGET(TRUE)
		ELSE
			SCALEFORM_SET_RETICLE_ON_TARGET(FALSE)
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_DRONE_DISTANCE_HELP()
	IF IS_SCREEN_FADED_IN()
	AND NOT IS_BIT_SET(g_SimpleInteriorData.iForthBS, BS4_SIMPLE_INTERIOR_TRUCK_DRONE_DISTANCE_HELP)
		IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
			INT iHintHelpCount = GET_PACKED_STAT_INT(PACKED_MP_INT_HACKER_TRUCK_DRONE_DISTANCE_LIMIT_HELP)
			IF iHintHelpCount < 4
				PRINT_HELP("HACK_DRONE_DIS")
				PRINTLN("[AM_MP_DRONE] - MAINTAIN_DRONE_DISTANCE_HELP iHintHelpCount: ", iHintHelpCount)
				iHintHelpCount++
				SET_PACKED_STAT_INT(PACKED_MP_INT_HACKER_TRUCK_DRONE_DISTANCE_LIMIT_HELP, iHintHelpCount)
				SET_BIT(g_SimpleInteriorData.iForthBS, BS4_SIMPLE_INTERIOR_TRUCK_DRONE_DISTANCE_HELP)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MANAGE_TRUCK_DRONE_SCALEFORM_DISTANCE_WARNING()
	IF sDroneControl.fDistanceBetDroneAndControl >= GET_DRONE_DISTANCE_LIMIT() - 50
	OR sDroneControl.fHeightDistanceBetDroneGround >= g_sMPTunables.iBB_TERRORBYTE_DRONE_HEIGHT_LIMIT - 50
		MAINTAIN_DRONE_DISTANCE_HELP()
		IF HAS_SOUND_FINISHED(sDroneControl.iAlarmLoopSound)
			sDroneControl.iAlarmLoopSound = GET_SOUND_ID()
			PLAY_SOUND_FRONTEND(sDroneControl.iAlarmLoopSound, "Out_Of_Bounds_Alarm_Loop", sDroneControl.sSoundSet)	
		ENDIF
		IF (sDroneControl.fDistanceBetDroneAndControl >= GET_DRONE_DISTANCE_LIMIT() - 50
		AND sDroneControl.fDistanceBetDroneAndControl < GET_DRONE_DISTANCE_LIMIT() - 45)
		OR (sDroneControl.fHeightDistanceBetDroneGround >= g_sMPTunables.iBB_TERRORBYTE_DRONE_HEIGHT_LIMIT - 50
		AND sDroneControl.fHeightDistanceBetDroneGround < g_sMPTunables.iBB_TERRORBYTE_DRONE_HEIGHT_LIMIT - 45) 
			SCALEFORM_SET_ELEMENT_VISIBLE("SET_WARNING_IS_VISIBLE", TRUE)
			SCALEFORM_WARNING_FLASH_RATE(0.1)
		ELIF (sDroneControl.fDistanceBetDroneAndControl >= GET_DRONE_DISTANCE_LIMIT() - 45
		AND sDroneControl.fDistanceBetDroneAndControl < GET_DRONE_DISTANCE_LIMIT() - 40)
		OR (sDroneControl.fHeightDistanceBetDroneGround >= g_sMPTunables.iBB_TERRORBYTE_DRONE_HEIGHT_LIMIT - 45
		AND sDroneControl.fHeightDistanceBetDroneGround < g_sMPTunables.iBB_TERRORBYTE_DRONE_HEIGHT_LIMIT - 40)
			SCALEFORM_SET_ELEMENT_VISIBLE("SET_WARNING_IS_VISIBLE", TRUE)
			SCALEFORM_WARNING_FLASH_RATE(0.2)
		ELIF (sDroneControl.fDistanceBetDroneAndControl >= GET_DRONE_DISTANCE_LIMIT() - 40
		AND sDroneControl.fDistanceBetDroneAndControl < GET_DRONE_DISTANCE_LIMIT() - 35)
		OR (sDroneControl.fHeightDistanceBetDroneGround >= g_sMPTunables.iBB_TERRORBYTE_DRONE_HEIGHT_LIMIT - 40
		AND sDroneControl.fHeightDistanceBetDroneGround < g_sMPTunables.iBB_TERRORBYTE_DRONE_HEIGHT_LIMIT - 35)
			SCALEFORM_SET_ELEMENT_VISIBLE("SET_WARNING_IS_VISIBLE", TRUE)
			SCALEFORM_WARNING_FLASH_RATE(0.3)	
		ELIF (sDroneControl.fDistanceBetDroneAndControl >= GET_DRONE_DISTANCE_LIMIT() - 35
		AND sDroneControl.fDistanceBetDroneAndControl < GET_DRONE_DISTANCE_LIMIT() - 30)
		OR (sDroneControl.fHeightDistanceBetDroneGround >= g_sMPTunables.iBB_TERRORBYTE_DRONE_HEIGHT_LIMIT - 35
		AND sDroneControl.fHeightDistanceBetDroneGround < g_sMPTunables.iBB_TERRORBYTE_DRONE_HEIGHT_LIMIT - 30)
			SCALEFORM_SET_ELEMENT_VISIBLE("SET_WARNING_IS_VISIBLE", TRUE)
			SCALEFORM_WARNING_FLASH_RATE(0.4)
		ELIF (sDroneControl.fDistanceBetDroneAndControl >= GET_DRONE_DISTANCE_LIMIT() - 30
		AND sDroneControl.fDistanceBetDroneAndControl < GET_DRONE_DISTANCE_LIMIT() - 25)
		OR (sDroneControl.fHeightDistanceBetDroneGround >= g_sMPTunables.iBB_TERRORBYTE_DRONE_HEIGHT_LIMIT - 30
		AND sDroneControl.fHeightDistanceBetDroneGround < g_sMPTunables.iBB_TERRORBYTE_DRONE_HEIGHT_LIMIT - 25)
			SCALEFORM_SET_ELEMENT_VISIBLE("SET_WARNING_IS_VISIBLE", TRUE)
			SCALEFORM_WARNING_FLASH_RATE(0.5)
		ELIF (sDroneControl.fDistanceBetDroneAndControl >= GET_DRONE_DISTANCE_LIMIT() - 25
		AND sDroneControl.fDistanceBetDroneAndControl < GET_DRONE_DISTANCE_LIMIT() - 20)
		OR (sDroneControl.fHeightDistanceBetDroneGround >= g_sMPTunables.iBB_TERRORBYTE_DRONE_HEIGHT_LIMIT - 25
		AND sDroneControl.fHeightDistanceBetDroneGround < g_sMPTunables.iBB_TERRORBYTE_DRONE_HEIGHT_LIMIT - 20)
			SCALEFORM_SET_ELEMENT_VISIBLE("SET_WARNING_IS_VISIBLE", TRUE)
			SCALEFORM_WARNING_FLASH_RATE(0.6)
		ELIF (sDroneControl.fDistanceBetDroneAndControl >= GET_DRONE_DISTANCE_LIMIT() - 20
		AND sDroneControl.fDistanceBetDroneAndControl < GET_DRONE_DISTANCE_LIMIT() - 15)
		OR (sDroneControl.fHeightDistanceBetDroneGround >= g_sMPTunables.iBB_TERRORBYTE_DRONE_HEIGHT_LIMIT - 20
		AND sDroneControl.fHeightDistanceBetDroneGround < g_sMPTunables.iBB_TERRORBYTE_DRONE_HEIGHT_LIMIT - 15)
			SCALEFORM_SET_ELEMENT_VISIBLE("SET_WARNING_IS_VISIBLE", TRUE)
			SCALEFORM_WARNING_FLASH_RATE(0.7)
		ELIF (sDroneControl.fDistanceBetDroneAndControl >= GET_DRONE_DISTANCE_LIMIT() - 15
		AND sDroneControl.fDistanceBetDroneAndControl < GET_DRONE_DISTANCE_LIMIT() - 10)
		OR (sDroneControl.fHeightDistanceBetDroneGround >= g_sMPTunables.iBB_TERRORBYTE_DRONE_HEIGHT_LIMIT - 15
		AND sDroneControl.fHeightDistanceBetDroneGround < g_sMPTunables.iBB_TERRORBYTE_DRONE_HEIGHT_LIMIT - 10)
			SCALEFORM_SET_ELEMENT_VISIBLE("SET_WARNING_IS_VISIBLE", TRUE)
			SCALEFORM_WARNING_FLASH_RATE(0.8)
		ELIF (sDroneControl.fDistanceBetDroneAndControl >= GET_DRONE_DISTANCE_LIMIT() - 10
		AND sDroneControl.fDistanceBetDroneAndControl < GET_DRONE_DISTANCE_LIMIT() - 5)
		OR (sDroneControl.fHeightDistanceBetDroneGround >= g_sMPTunables.iBB_TERRORBYTE_DRONE_HEIGHT_LIMIT - 10
		AND sDroneControl.fHeightDistanceBetDroneGround < g_sMPTunables.iBB_TERRORBYTE_DRONE_HEIGHT_LIMIT - 5)
			SCALEFORM_SET_ELEMENT_VISIBLE("SET_WARNING_IS_VISIBLE", TRUE)
			SCALEFORM_WARNING_FLASH_RATE(0.9)
		ELIF (sDroneControl.fDistanceBetDroneAndControl >= GET_DRONE_DISTANCE_LIMIT() - 5
		AND sDroneControl.fDistanceBetDroneAndControl < GET_DRONE_DISTANCE_LIMIT())
		OR (sDroneControl.fHeightDistanceBetDroneGround >= g_sMPTunables.iBB_TERRORBYTE_DRONE_HEIGHT_LIMIT - 5
		AND sDroneControl.fHeightDistanceBetDroneGround < g_sMPTunables.iBB_TERRORBYTE_DRONE_HEIGHT_LIMIT)
			SCALEFORM_SET_ELEMENT_VISIBLE("SET_WARNING_IS_VISIBLE", TRUE)
			SCALEFORM_WARNING_FLASH_RATE(1.0)	
		ENDIF	
	ELSE
		SCALEFORM_SET_ELEMENT_VISIBLE("SET_WARNING_IS_VISIBLE", FALSE)
		IF !HAS_SOUND_FINISHED(sDroneControl.iAlarmLoopSound)
			STOP_SOUND(sDroneControl.iAlarmLoopSound)
			RELEASE_SOUND_ID(sDroneControl.iAlarmLoopSound)
			sDroneControl.iAlarmLoopSound = -1
		ENDIF
	ENDIF
ENDPROC	

PROC MANAGE_DRONE_SCALEFORM_DISTANCE_WARNING()
	INT iMultiplier = 1
	#IF FEATURE_HEIST_ISLAND
	IF IS_DRONE_STARTED_FROM_SUBMARINE_MISSILE()
		iMultiplier = 10
	ENDIF	
	#ENDIF
	FLOAT fDistanceBetDroneAndPlayer = sDroneControl.fDistanceBetDroneAndControl
	IF fDistanceBetDroneAndPlayer >= GET_DRONE_DISTANCE_LIMIT() - (50 * iMultiplier)
		IF HAS_SOUND_FINISHED(sDroneControl.iAlarmLoopSound)
			sDroneControl.iAlarmLoopSound = GET_SOUND_ID()
			PLAY_SOUND_FRONTEND(sDroneControl.iAlarmLoopSound, "Out_Of_Bounds_Alarm_Loop", sDroneControl.sSoundSet)	
		ENDIF
		IF (fDistanceBetDroneAndPlayer >= GET_DRONE_DISTANCE_LIMIT() - (50 * iMultiplier)
		AND fDistanceBetDroneAndPlayer < GET_DRONE_DISTANCE_LIMIT() - (45 * iMultiplier))
			SCALEFORM_SET_ELEMENT_VISIBLE("SET_WARNING_IS_VISIBLE", TRUE)
			SCALEFORM_WARNING_FLASH_RATE(0.1)
		ELIF (fDistanceBetDroneAndPlayer >= GET_DRONE_DISTANCE_LIMIT() - (45 * iMultiplier)
		AND fDistanceBetDroneAndPlayer < GET_DRONE_DISTANCE_LIMIT() - (40 * iMultiplier))
			SCALEFORM_SET_ELEMENT_VISIBLE("SET_WARNING_IS_VISIBLE", TRUE)
			SCALEFORM_WARNING_FLASH_RATE(0.2)
		ELIF (fDistanceBetDroneAndPlayer >= GET_DRONE_DISTANCE_LIMIT() - (40 * iMultiplier)
		AND fDistanceBetDroneAndPlayer < GET_DRONE_DISTANCE_LIMIT() - (35 * iMultiplier))
			SCALEFORM_SET_ELEMENT_VISIBLE("SET_WARNING_IS_VISIBLE", TRUE)
			SCALEFORM_WARNING_FLASH_RATE(0.3)	
		ELIF (fDistanceBetDroneAndPlayer >= GET_DRONE_DISTANCE_LIMIT() - (35 * iMultiplier)
		AND fDistanceBetDroneAndPlayer < GET_DRONE_DISTANCE_LIMIT() - (30 * iMultiplier))
			SCALEFORM_SET_ELEMENT_VISIBLE("SET_WARNING_IS_VISIBLE", TRUE)
			SCALEFORM_WARNING_FLASH_RATE(0.4)
		ELIF (fDistanceBetDroneAndPlayer >= GET_DRONE_DISTANCE_LIMIT() - (30 * iMultiplier)
		AND fDistanceBetDroneAndPlayer < GET_DRONE_DISTANCE_LIMIT() - (25 * iMultiplier))
			SCALEFORM_SET_ELEMENT_VISIBLE("SET_WARNING_IS_VISIBLE", TRUE)
			SCALEFORM_WARNING_FLASH_RATE(0.5)
		ELIF (fDistanceBetDroneAndPlayer >= GET_DRONE_DISTANCE_LIMIT() - (25 * iMultiplier)
		AND fDistanceBetDroneAndPlayer < GET_DRONE_DISTANCE_LIMIT() - (20 * iMultiplier))
			SCALEFORM_SET_ELEMENT_VISIBLE("SET_WARNING_IS_VISIBLE", TRUE)
			SCALEFORM_WARNING_FLASH_RATE(0.6)
		ELIF (fDistanceBetDroneAndPlayer >= GET_DRONE_DISTANCE_LIMIT() - (20 * iMultiplier)
		AND fDistanceBetDroneAndPlayer < GET_DRONE_DISTANCE_LIMIT() - (15 * iMultiplier))
			SCALEFORM_SET_ELEMENT_VISIBLE("SET_WARNING_IS_VISIBLE", TRUE)
			SCALEFORM_WARNING_FLASH_RATE(0.7)
		ELIF (fDistanceBetDroneAndPlayer >= GET_DRONE_DISTANCE_LIMIT() - (15 * iMultiplier)
		AND fDistanceBetDroneAndPlayer < GET_DRONE_DISTANCE_LIMIT() - (10 * iMultiplier))
			SCALEFORM_SET_ELEMENT_VISIBLE("SET_WARNING_IS_VISIBLE", TRUE)
			SCALEFORM_WARNING_FLASH_RATE(0.8)
		ELIF (fDistanceBetDroneAndPlayer >= GET_DRONE_DISTANCE_LIMIT() - (10 * iMultiplier)
		AND fDistanceBetDroneAndPlayer < GET_DRONE_DISTANCE_LIMIT() - (5 * iMultiplier))
			SCALEFORM_SET_ELEMENT_VISIBLE("SET_WARNING_IS_VISIBLE", TRUE)
			SCALEFORM_WARNING_FLASH_RATE(0.9)
		ELIF (fDistanceBetDroneAndPlayer >= GET_DRONE_DISTANCE_LIMIT() - (5 * iMultiplier)
		AND fDistanceBetDroneAndPlayer < GET_DRONE_DISTANCE_LIMIT())
			SCALEFORM_SET_ELEMENT_VISIBLE("SET_WARNING_IS_VISIBLE", TRUE)
			SCALEFORM_WARNING_FLASH_RATE(1.0)	
		ENDIF	
	ELSE
		SCALEFORM_SET_ELEMENT_VISIBLE("SET_WARNING_IS_VISIBLE", FALSE)
		IF !HAS_SOUND_FINISHED(sDroneControl.iAlarmLoopSound)
			STOP_SOUND(sDroneControl.iAlarmLoopSound)
			RELEASE_SOUND_ID(sDroneControl.iAlarmLoopSound)
			sDroneControl.iAlarmLoopSound = -1
		ENDIF
	ENDIF
ENDPROC

PROC PLAY_OUT_OF_BOUND_ALARM_FOR_DRONE_MISSILE()
	IF IS_DRONE_STARTED_FROM_ARENA_WARS_MISSILE()
		IF sDroneControl.vDroneCoords.z >= GET_DRONE_DISTANCE_LIMIT() - 20
			IF HAS_SOUND_FINISHED(sDroneControl.iAlarmLoopSound)
				sDroneControl.iAlarmLoopSound = GET_SOUND_ID()
				PLAY_SOUND_FRONTEND(sDroneControl.iAlarmLoopSound, "Out_Of_Bounds_Alarm_Loop", sDroneControl.sSoundSet)	
			ENDIF
		ELSE
			IF !HAS_SOUND_FINISHED(sDroneControl.iAlarmLoopSound)
				STOP_SOUND(sDroneControl.iAlarmLoopSound)
				RELEASE_SOUND_ID(sDroneControl.iAlarmLoopSound)
				sDroneControl.iAlarmLoopSound = -1
			ENDIF
		ENDIF
	ENDIF	
ENDPROC

PROC MANAGE_ARENA_DRONE_SCALEFORM_DISTANCE_WARNING()
	IF sDroneControl.vDroneCoords.z >= GET_DRONE_DISTANCE_LIMIT() - 20
		IF HAS_SOUND_FINISHED(sDroneControl.iAlarmLoopSound)
			sDroneControl.iAlarmLoopSound = GET_SOUND_ID()
			PLAY_SOUND_FRONTEND(sDroneControl.iAlarmLoopSound, "Out_Of_Bounds_Alarm_Loop", sDroneControl.sSoundSet)	
		ENDIF
		IF (sDroneControl.vDroneCoords.z >= GET_DRONE_DISTANCE_LIMIT() - 20.0
		AND sDroneControl.vDroneCoords.z < GET_DRONE_DISTANCE_LIMIT() - 16.0)
			SCALEFORM_SET_ELEMENT_VISIBLE("SET_WARNING_IS_VISIBLE", TRUE)
			SCALEFORM_WARNING_FLASH_RATE(0.1)
		ELIF (sDroneControl.vDroneCoords.z >= GET_DRONE_DISTANCE_LIMIT() - 16.0
		AND sDroneControl.vDroneCoords.z < GET_DRONE_DISTANCE_LIMIT() - 13.0)
			SCALEFORM_SET_ELEMENT_VISIBLE("SET_WARNING_IS_VISIBLE", TRUE)
			SCALEFORM_WARNING_FLASH_RATE(0.2)
		ELIF (sDroneControl.vDroneCoords.z >= GET_DRONE_DISTANCE_LIMIT() - 13.0
		AND sDroneControl.vDroneCoords.z < GET_DRONE_DISTANCE_LIMIT() - 10.0)
			SCALEFORM_SET_ELEMENT_VISIBLE("SET_WARNING_IS_VISIBLE", TRUE)
			SCALEFORM_WARNING_FLASH_RATE(0.3)	
		ELIF (sDroneControl.vDroneCoords.z >= GET_DRONE_DISTANCE_LIMIT() - 10.0
		AND sDroneControl.vDroneCoords.z < GET_DRONE_DISTANCE_LIMIT() - 8.0)
			SCALEFORM_SET_ELEMENT_VISIBLE("SET_WARNING_IS_VISIBLE", TRUE)
			SCALEFORM_WARNING_FLASH_RATE(0.4)
		ELIF (sDroneControl.vDroneCoords.z >= GET_DRONE_DISTANCE_LIMIT() - 8.0
		AND sDroneControl.vDroneCoords.z < GET_DRONE_DISTANCE_LIMIT() - 6.0)
			SCALEFORM_SET_ELEMENT_VISIBLE("SET_WARNING_IS_VISIBLE", TRUE)
			SCALEFORM_WARNING_FLASH_RATE(0.5)
		ELIF (sDroneControl.vDroneCoords.z >= GET_DRONE_DISTANCE_LIMIT() - 6.0
		AND sDroneControl.vDroneCoords.z < GET_DRONE_DISTANCE_LIMIT() - 4.0)
			SCALEFORM_SET_ELEMENT_VISIBLE("SET_WARNING_IS_VISIBLE", TRUE)
			SCALEFORM_WARNING_FLASH_RATE(0.6)
		ELIF (sDroneControl.vDroneCoords.z >= GET_DRONE_DISTANCE_LIMIT() - 4.0
		AND sDroneControl.vDroneCoords.z < GET_DRONE_DISTANCE_LIMIT() - 3.0)
			SCALEFORM_SET_ELEMENT_VISIBLE("SET_WARNING_IS_VISIBLE", TRUE)
			SCALEFORM_WARNING_FLASH_RATE(0.7)
		ELIF (sDroneControl.vDroneCoords.z >= GET_DRONE_DISTANCE_LIMIT() - 3.0
		AND sDroneControl.vDroneCoords.z < GET_DRONE_DISTANCE_LIMIT() - 2.0)
			SCALEFORM_SET_ELEMENT_VISIBLE("SET_WARNING_IS_VISIBLE", TRUE)
			SCALEFORM_WARNING_FLASH_RATE(0.8)
		ELIF (sDroneControl.vDroneCoords.z >= GET_DRONE_DISTANCE_LIMIT() - 2.0
		AND sDroneControl.vDroneCoords.z < GET_DRONE_DISTANCE_LIMIT() - 1.0)
			SCALEFORM_SET_ELEMENT_VISIBLE("SET_WARNING_IS_VISIBLE", TRUE)
			SCALEFORM_WARNING_FLASH_RATE(0.9)
		ELIF (sDroneControl.vDroneCoords.z >= GET_DRONE_DISTANCE_LIMIT() - 1.0
		AND sDroneControl.vDroneCoords.z < GET_DRONE_DISTANCE_LIMIT())
			SCALEFORM_SET_ELEMENT_VISIBLE("SET_WARNING_IS_VISIBLE", TRUE)
			SCALEFORM_WARNING_FLASH_RATE(1.0)	
		ENDIF	
	ELSE
		SCALEFORM_SET_ELEMENT_VISIBLE("SET_WARNING_IS_VISIBLE", FALSE)
		IF !HAS_SOUND_FINISHED(sDroneControl.iAlarmLoopSound)
			STOP_SOUND(sDroneControl.iAlarmLoopSound)
			RELEASE_SOUND_ID(sDroneControl.iAlarmLoopSound)
			sDroneControl.iAlarmLoopSound = -1
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL DOES_THIS_DRONE_HAVE_BOOST()
	IF IS_DRONE_STARTED_FROM_CASINO_HEIST_MISSON()
		IF IS_VALID_INTERIOR(g_sDroneGlobals.droneInterior)
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC DRAW_DRONE_SCALEFORM()
	
	IF NOT HAS_SCALEFORM_MOVIE_LOADED(sDroneControl.droneStunSF)
		sDroneControl.droneStunSF = REQUEST_SCALEFORM_MOVIE("DRONE_CAM")
		CDEBUG1LN(DEBUG_NET_DRONE, "[AM_MP_DRONE] - DRAW_DRONE_SCALEFORM scaleform not loaded")
		EXIT
	ENDIF
	
	IF IS_DRONE_STARTED_FROM_ARENA_WARS_TWO()
	OR IS_DRONE_SNAPMATIC_ACTIVE()
	OR SHOULD_BLOCK_EXPLOSIVE(FALSE)
		SCALEFORM_SET_ELEMENT_VISIBLE("SET_DETONATE_METER_IS_VISIBLE", FALSE)
	ELSE
		SCALEFORM_SET_ELEMENT_VISIBLE("SET_DETONATE_METER_IS_VISIBLE", TRUE)
	ENDIF
	
	IF IS_DRONE_STARTED_FROM_ARENA_WARS_TWO()
	OR IS_DRONE_SNAPMATIC_ACTIVE()
	OR IS_DRONE_STARTED_FROM_ARENA_WARS_ONE()
		SCALEFORM_SET_ELEMENT_VISIBLE("SET_SHOCK_METER_IS_VISIBLE", FALSE)
	ELSE
		SCALEFORM_SET_ELEMENT_VISIBLE("SET_SHOCK_METER_IS_VISIBLE", TRUE)
	ENDIF
	
	IF IS_DRONE_STARTED_FROM_ARENA_WARS_ONE()
		SCALEFORM_SET_ELEMENT_VISIBLE("SET_EMP_METER_IS_VISIBLE", TRUE)
	ELSE
		SCALEFORM_SET_ELEMENT_VISIBLE("SET_EMP_METER_IS_VISIBLE", FALSE)
	ENDIF
	
	IF !IS_DRONE_STARTED_FROM_ARENA_WARS_TWO()
	AND !IS_DRONE_SNAPMATIC_ACTIVE()
		SCALEFORM_SET_ELEMENT_VISIBLE("SET_RETICLE_IS_VISIBLE", TRUE)
	ELSE
		SCALEFORM_SET_ELEMENT_VISIBLE("SET_RETICLE_IS_VISIBLE", FALSE)
	ENDIF
	
	SCALEFORM_SET_ELEMENT_VISIBLE("SET_HEADING_METER_IS_VISIBLE", TRUE)
	
	SCALEFORM_SET_ELEMENT_VISIBLE("SET_ZOOM_METER_IS_VISIBLE", TRUE)
	
	IF !IS_DRONE_STARTED_FROM_ARENA_WARS_TWO()
	AND !IS_DRONE_SNAPMATIC_ACTIVE()
	AND !IS_DRONE_STARTED_FROM_ARENA_WARS_ONE()
	AND DOES_THIS_DRONE_HAVE_BOOST()
		SCALEFORM_SET_ELEMENT_VISIBLE("SET_BOOST_METER_IS_VISIBLE", TRUE)
	ELSE
		SCALEFORM_SET_ELEMENT_VISIBLE("SET_BOOST_METER_IS_VISIBLE", FALSE)
	ENDIF
	
	IF IS_DRONE_STARTED_FROM_CASINO_HEIST_MISSON()
		SCALEFORM_SET_ELEMENT_VISIBLE("SET_TRANQUILIZE_METER_IS_VISIBLE", TRUE)
	ELSE
		SCALEFORM_SET_ELEMENT_VISIBLE("SET_TRANQUILIZE_METER_IS_VISIBLE", FALSE)
	ENDIF
	
	SCALEFORM_SET_ELEMENT_VISIBLE("SET_MISSILE_METER_IS_VISIBLE", FALSE) 
	
	SCALEFORM_SET_ELEMENT_VISIBLE("SET_BOTTOM_LEFT_CORNER_IS_VISIBLE", FALSE)
	
	IF GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = ENUM_TO_INT(FMMC_TYPE_FMBB_TARGET_PURSUIT) 
		IF sDroneControl.fDistanceBetDroneAndControl >= GET_DRONE_DISTANCE_LIMIT() - 50
		OR sDroneControl.fHeightDistanceBetDroneGround >= g_sMPTunables.iBB_TERRORBYTE_DRONE_HEIGHT_LIMIT - 50
			SCALEFORM_SET_ELEMENT_VISIBLE("SET_SOUND_WAVE_IS_VISIBLE", FALSE)
			STOP_SCANNER_SOUND()
			IF IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_INIT_SOUND_WAVE_SCALEFORM)
				CLEAR_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_INIT_SOUND_WAVE_SCALEFORM)
			ENDIF
		ELSE
			PLAY_SCANNER_SOUND(GET_SCALEFORM_SOUND_WAVE_VALUE())
			IF NOT IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_INIT_SOUND_WAVE_SCALEFORM)
				SCALEFORM_SET_ELEMENT_VISIBLE("SET_SOUND_WAVE_IS_VISIBLE", TRUE)
				SET_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_INIT_SOUND_WAVE_SCALEFORM)
			ENDIF	
		ENDIF	
	ELSE	
		SCALEFORM_SET_ELEMENT_VISIBLE("SET_SOUND_WAVE_IS_VISIBLE", FALSE)
		STOP_SCANNER_SOUND()
	ENDIF
	
	SCALEFORM_SET_ELEMENT_VISIBLE("SET_INFO_LIST_IS_VISIBLE", FALSE)
	
	IF !IS_DRONE_STARTED_FROM_ARENA_WARS_TWO()
	AND !IS_DRONE_SNAPMATIC_ACTIVE()
		IF NOT IS_DRONE_STARTED_FROM_ARENA_WARS_ONE()
			SCALEFORM_SET_SHOCK_PERCENTAGE(sDroneControl.iStunRechargerFill)
		ELSE
			SCALEFORM_SET_EMP_PERCENTAGE(sDroneControl.iStunRechargerFill)
		ENDIF
		
		IF !SHOULD_BLOCK_EXPLOSIVE(FALSE)		
			SCALEFORM_SET_DETONATE_PERCENTAGE(sDroneControl.iDetonateFill)
		ENDIF	
	ENDIF
	
	IF IS_DRONE_STARTED_FROM_CASINO_HEIST_MISSON()
		SCALEFORM_SET_TRANQUILLISER_PERCENTAGE(sDroneControl.iTranqRechargerFill)
	ENDIF
	
	IF DOES_THIS_DRONE_HAVE_BOOST()
		SCALEFORM_SET_BOOST_PERCENTAGE(sDroneControl.iBoostFill)
	ENDIF
	
	SCALEFORM_SET_ZOOM_LEVEL(0, "DRONE_ZOOM_1")
	SCALEFORM_SET_ZOOM_LEVEL(1, "")
	SCALEFORM_SET_ZOOM_LEVEL(2, "DRONE_ZOOM_2")
	SCALEFORM_SET_ZOOM_LEVEL(3, "")
	SCALEFORM_SET_ZOOM_LEVEL(4, "DRONE_ZOOM_3")
	
	// Drone zoom level
	MANAGE_DRONE_SCALEFORM_ZOOM_LEVEL()
	
	// Stun gun recharger 
	MANAGE_DRONE_SCALEFORM_STUN_GUN_RECHARGER()
	
	// Aiming peds will turn reticle to red
	MANAGE_DRONE_SCALEFORM_TARGET_COLOUR()
	
	// Drone heading
	SCALEFORM_SET_HEADING(ROUND(GET_ENTITY_HEADING(NET_TO_OBJ(sDroneControl.cameraProp)) + 180))
	
	IF GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = ENUM_TO_INT(FMMC_TYPE_FMBB_TARGET_PURSUIT) 
		SCALEFORM_ATTENUATE_SOUND_WAVE(GET_SCALEFORM_SOUND_WAVE_VALUE())
	ENDIF
	
	IF IS_DRONE_STARTED_FROM_TRUCK()
		MANAGE_TRUCK_DRONE_SCALEFORM_DISTANCE_WARNING()
	ELIF IS_DRONE_STARTED_FROM_ARENA_WARS_ONE()
	OR IS_DRONE_STARTED_FROM_ARENA_WARS_TWO()
		MANAGE_ARENA_DRONE_SCALEFORM_DISTANCE_WARNING()
	ELIF SHOULD_START_DRONE_FROM_PLAYER_PHONE()
		MANAGE_DRONE_SCALEFORM_DISTANCE_WARNING()
	ENDIF
	
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
	DRAW_SCALEFORM_MOVIE_FULLSCREEN(sDroneControl.droneStunSF, 255, 255, 255, 0)
	
ENDPROC

PROC DRAW_DRONE_HELP_KEY()
	
	IF IS_DRONE_SNAPMATIC_ACTIVE()
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_START_INSTRUCTIONAL_PROMPT)
		IF LOAD_MENU_ASSETS()
			REMOVE_MENU_HELP_KEYS()
			
			ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_SCRIPT_RSTICK_ALL, "DRONE_MOVE")
			ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_SCRIPT_LSTICK_ALL, "DRONE_POSITION")
			
			IF !IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
				ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_RS, "CELL_284")
				IF NOT IS_DRONE_STARTED_FROM_ARENA_WARS_TWO()
				AND NOT IS_DRONE_STARTED_FROM_ARENA_WARS_ONE()
				AND DOES_THIS_DRONE_HAVE_BOOST()
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_LS, "BOOST_DRONE_E")
				ENDIF
				ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_RT, "MOVE_DRONE_UP")
				ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_LT, "MOVE_DRONE_DO")
			ELSE
				ADD_MENU_HELP_KEY_GROUP(INPUTGROUP_CURSOR_SCROLL, "CELL_284")
				IF NOT IS_DRONE_STARTED_FROM_ARENA_WARS_TWO()
				AND NOT IS_DRONE_STARTED_FROM_ARENA_WARS_ONE()
				AND DOES_THIS_DRONE_HAVE_BOOST()
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_X, "BOOST_DRONE_E")
				ENDIF
				ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_LS, "MOVE_DRONE_UP")
				ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_RS, "MOVE_DRONE_DO")
			ENDIF
			
			IF NOT IS_DRONE_STARTED_FROM_ARENA_WARS_TWO()
				CONTROL_ACTION stunControl = INPUT_FRONTEND_RB
				
				IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
					stunControl = INPUT_CURSOR_ACCEPT
				ENDIF
				
				IF IS_DRONE_STARTED_FROM_ARENA_WARS_ONE()
					ADD_MENU_HELP_KEY_CLICKABLE(stunControl, "MOVE_DRONE_EM")
				ELSE
					ADD_MENU_HELP_KEY_CLICKABLE(stunControl, "MOVE_DRONE_ST")
				ENDIF
				
				CONTROL_ACTION explosiveControl = INPUT_FRONTEND_LB
				IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
					explosiveControl = INPUT_CURSOR_CANCEL
				ENDIF
			
				IF !SHOULD_BLOCK_EXPLOSIVE(FALSE)
					ADD_MENU_HELP_KEY_CLICKABLE(explosiveControl, "MOVE_DRONE_EX")
				ELIF SHOULD_DRONE_HAVE_TRANQUILLISER_GUN_EQUIPPED()
					ADD_MENU_HELP_KEY_CLICKABLE(explosiveControl, "MOVE_DRONE_TRG")
				ENDIF	
			ENDIF
			
			IF NOT IS_DRONE_STARTED_FROM_ARENA_WARS_ONE()
				IF IS_SELECTOR_ONSCREEN(FALSE)
				OR IS_SELECTOR_ONSCREEN()
				OR IS_REPLAY_RECORDING_FEED_BUTTONS_ONSCREEN()
				OR IS_REPLAY_RECORDING()
				OR GB_IS_PLAYER_ON_DRONE_MISSION(PLAYER_ID())
					// Skip
				ELSE	
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "DRONE_PHOTO")
				ENDIF	
			ENDIF
			
			ADD_MENU_HELP_KEY_CLICKABLE(INPUT_VEH_CIN_CAM, "MOVE_DRONE_RE")
			
			SET_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_START_INSTRUCTIONAL_PROMPT)
		ENDIF
	ELSE
		DRAW_MENU_HELP_SCALEFORM(0) 
	ENDIF
	
	IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
		IF NOT IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_SHOULD_SHOW_PC_PROMPTS)
			SET_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_SHOULD_SHOW_PC_PROMPTS)
			CLEAR_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_START_INSTRUCTIONAL_PROMPT)
		ENDIF	
	ELSE
		IF IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_SHOULD_SHOW_PC_PROMPTS)
			CLEAR_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_SHOULD_SHOW_PC_PROMPTS)
			CLEAR_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_START_INSTRUCTIONAL_PROMPT)
		ENDIF
	ENDIF
	
ENDPROC

PROC DRAW_MISSILE_DRONE_HELP_KEY()
	#IF FEATURE_HEIST_ISLAND
	IF IS_DRONE_STARTED_FROM_SUBMARINE_MISSILE()
		IF NOT CAN_TAKE_CONTROL_OF_SUBMARINE_MISSILE()
			EXIT
		ENDIF
	ENDIF	
	#ENDIF
	
	IF NOT IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_START_INSTRUCTIONAL_PROMPT)
		IF LOAD_MENU_ASSETS()
			REMOVE_MENU_HELP_KEYS()
			
			ADD_MENU_HELP_KEY_GROUP(INPUTGROUP_SCRIPT_LSTICK_ALL, "DRONE_SPACE")
			ADD_MENU_HELP_KEY_GROUP(INPUTGROUP_SCRIPT_RSTICK_ALL, "DRONE_POSITION")
			
			IF IS_DRONE_STARTED_FROM_ARENA_WARS_MISSILE()
				IF !IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_RT, "DRONE_SPEEDU")
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_LT, "DRONE_SLOWD")
				ELSE
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_LS, "DRONE_SPEEDU")
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_RS, "DRONE_SLOWD")
				ENDIF
			ENDIF
			
			ADD_MENU_HELP_KEY_CLICKABLE(INPUT_VEH_EXIT, "MOVE_DRONE_RE")
			
			SET_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_START_INSTRUCTIONAL_PROMPT)
		ENDIF
	ELSE
		DRAW_MENU_HELP_SCALEFORM(0) 
	ENDIF
	
	IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
		IF NOT IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_SHOULD_SHOW_PC_PROMPTS)
			SET_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_SHOULD_SHOW_PC_PROMPTS)
			CLEAR_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_START_INSTRUCTIONAL_PROMPT)
		ENDIF	
	ELSE
		IF IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_SHOULD_SHOW_PC_PROMPTS)
			CLEAR_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_SHOULD_SHOW_PC_PROMPTS)
			CLEAR_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_START_INSTRUCTIONAL_PROMPT)
		ENDIF
	ENDIF
ENDPROC

PROC DRAW_MISSILE_DRONE_SCALEFORM()

	IF NOT HAS_SCALEFORM_MOVIE_LOADED(sDroneControl.droneStunSF)
		#IF FEATURE_HEIST_ISLAND
		IF IS_DRONE_STARTED_FROM_SUBMARINE_MISSILE()
			sDroneControl.droneStunSF = REQUEST_SUBMARINE_MISSILE_SCALEFORM_MOVE()
		ELSE
		#ENDIF
			sDroneControl.droneStunSF = REQUEST_ARENA_WARS_MISSILE_SCALEFORM_MOVE()
		#IF FEATURE_HEIST_ISLAND
		ENDIF
		#ENDIF
		CDEBUG1LN(DEBUG_NET_DRONE, "[AM_MP_DRONE] - DRAW_MISSILE_DRONE_SCALEFORM scaleform not loaded")
		EXIT
	ENDIF
	
	OBJECT_INDEX propDrone = GET_LOCAL_DRONE_PROP_OBJ_INDEX()
	IF NATIVE_TO_INT(propDrone) < 0
	OR NOT IS_ENTITY_ALIVE(propDrone)
		CDEBUG1LN(DEBUG_NET_DRONE, "[AM_MP_DRONE] - DRAW_MISSILE_DRONE_SCALEFORM Drone prop index < 0 OR not alive. propDrone = ", NATIVE_TO_INT(propDrone))
		EXIT	
	ENDIF
	
	#IF FEATURE_HEIST_ISLAND
	IF IS_DRONE_STARTED_FROM_SUBMARINE_MISSILE()
		MANAGE_DRONE_SCALEFORM_DISTANCE_WARNING()
	ENDIF
	#ENDIF
	
	// Hide zoom & weapon icons
	ARENA_HUD_SET_ZOOM_IS_VISIBLE(sDroneControl.droneStunSF, FALSE)
	ARENA_HUD_SET_WEAPON_ICONS(sDroneControl.droneStunSF, AHWI_HIDDEN, AHWI_HIDDEN, AHWI_HIDDEN)
	UPDATE_HUD_TURRET_CAM(sDroneControl.droneStunSF, 0, 0, 0, 0, GET_ENTITY_HEADING(propDrone) + 180)

	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
	DRAW_SCALEFORM_MOVIE_FULLSCREEN(sDroneControl.droneStunSF, 255, 255, 255, 0)
ENDPROC

PROC DRAW_DRONE_HUD()
	
	IF IS_ALL_DRONE_UI_HIDDEN()
		EXIT
	ENDIF
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sDroneControl.cameraProp)
	AND NOT IS_ENTITY_DEAD(NET_TO_OBJ(sDroneControl.cameraProp))
		IF DOES_CAM_EXIST(sDroneControl.droneCamera)
		AND IS_CAM_RENDERING(sDroneControl.droneCamera)
			IF NOT IS_DRONE_USED_AS_GUIDED_MISSILE()
				DRAW_DRONE_HELP_KEY()
				DRAW_DRONE_SCALEFORM()
			ELSE
				DRAW_MISSILE_DRONE_HELP_KEY()
				DRAW_MISSILE_DRONE_SCALEFORM()
			ENDIF
			REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()
			SET_INSTRUCTIONAL_BUTTONS_UNDER_HUD_THIS_FRAME()
			SET_LOADING_ICON_SUBTITLES_OFFSET_SHIFT_THIS_FRAME(2) 
		ENDIF
	ENDIF	
ENDPROC

/// PURPOSE:
///    Check if drone functionality should paused
FUNC BOOL SHOULD_PAUSE_DRONE(BOOL bAllowSelector = FALSE)
	IF IS_PAUSE_MENU_ACTIVE()
	OR IS_COMMERCE_STORE_OPEN()
		PRINTLN("[AM_MP_DRONE] - SHOULD_PAUSE_DRONE - pause or store is open")
		RETURN TRUE
	ENDIF
	
	IF  NETWORK_TEXT_CHAT_IS_TYPING()
		PRINTLN("[AM_MP_DRONE] - SHOULD_PAUSE_DRONE - NETWORK_TEXT_CHAT_IS_TYPING is true")
		RETURN TRUE
	ENDIF
	
	IF  IS_ALL_DRONE_UI_HIDDEN()
		PRINTLN("[AM_MP_DRONE] - SHOULD_PAUSE_DRONE - IS_ALL_DRONE_UI_HIDDEN is true")
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_SPECTATING(PLAYER_ID())
	AND NOT IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
		PRINTLN("[AM_MP_DRONE] - SHOULD_PAUSE_DRONE - IS_PLAYER_SPECTATING")
		RETURN TRUE
	ENDIF
	
	IF GB_IS_PLAYER_ON_DRONE_MISSION(PLAYER_ID())
		IF IS_BIT_SET(g_sHackingData.bsHacking, 0) // BS_IS_HACKING
			PRINTLN("[AM_MP_DRONE] - SHOULD_PAUSE_DRONE - player is hacking")
		RETURN TRUE
		ENDIF
	ENDIF
	
	IF g_bCelebrationScreenIsActive
	OR g_bMissionEnding
		SET_DRONE_FORCE_EXTERNAL_CLEANING_UP(TRUE)
		PRINTLN("[AM_MP_DRONE] - SHOULD_PAUSE_DRONE - g_bCelebrationScreenIsActive or g_bMissionEnding")
		RETURN TRUE
	ENDIF

	IF (IS_SELECTOR_UI_BUTTON_PRESSED(FALSE)
	OR IS_SELECTOR_ONSCREEN()
	OR IS_REPLAY_RECORDING_FEED_BUTTONS_ONSCREEN())
	AND NOT bAllowSelector
		DISABLE_CELLPHONE_THIS_FRAME_ONLY()
		HIDE_PLAYER_ARROW_THIS_FRAME()	
		// Allow hud
		DRAW_DRONE_HUD()

		PRINTLN("[AM_MP_DRONE] - SHOULD_PAUSE_DRONE - IS_SELECTOR_ONSCREEN")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_FREEZE_DRONE_COORDS()
	
	IF GB_IS_PLAYER_ON_DRONE_MISSION(PLAYER_ID())
		IF IS_BIT_SET(g_sHackingData.bsHacking, 0) // BS_IS_HACKING
			PRINTLN("[AM_MP_DRONE] - SHOULD_FREEZE_DRONE_COORDS - player is hacking")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_DRONE_FREEZE_POSITION()
	IF SHOULD_FREEZE_DRONE_COORDS()
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sDroneControl.cameraProp)
		AND NOT IS_ENTITY_DEAD(NET_TO_OBJ(sDroneControl.cameraProp))
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(sDroneControl.cameraProp)
				IF NOT IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_DRONE_FREEZED)
					FREEZE_ENTITY_POSITION(NET_TO_OBJ(sDroneControl.cameraProp), TRUE)
					SET_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_DRONE_FREEZED)
					PRINTLN("[AM_MP_DRONE] - MAINTAIN_DRONE_FREEZE_POSITION - DRONE_LOCAL_BS_DRONE_FREEZED TRUE")
				ENDIF	
			ELSE
				NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(sDroneControl.cameraProp)
			ENDIF
		ENDIF	
	ELSE
		IF IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_DRONE_FREEZED)
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sDroneControl.cameraProp)
			AND NOT IS_ENTITY_DEAD(NET_TO_OBJ(sDroneControl.cameraProp))
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(sDroneControl.cameraProp)
					FREEZE_ENTITY_POSITION(NET_TO_OBJ(sDroneControl.cameraProp), FALSE)
					CLEAR_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_DRONE_FREEZED)
					PRINTLN("[AM_MP_DRONE] - MAINTAIN_DRONE_FREEZE_POSITION - DRONE_LOCAL_BS_DRONE_FREEZED FALSE")
				ELSE
					NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(sDroneControl.cameraProp)
				ENDIF
			ENDIF	
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_ALLOW_BOOST_MOVEMENT()
	
	IF SHOULD_PAUSE_DRONE()
		RETURN FALSE
	ENDIF

	IF (IS_DRONE_BOOST_ACTIVATED() AND !IS_DRONE_BOOST_DIR_FORWARD())
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

//// PURPOSE:
 ///    multipiler which we need to apply when moving forward/backward 
 /// RETURNS:
 ///    
FUNC FLOAT GET_DRONE_UP_MULTIPLIER()
	IF IS_DRONE_STARTED_FROM_ARENA_WARS_ONE()
	OR IS_DRONE_STARTED_FROM_ARENA_WARS_TWO()
		RETURN 40.0
	ELIF IS_DRONE_STARTED_FROM_HEIST_FREEMODE()
		RETURN 30.0
	ELIF IS_DRONE_STARTED_FROM_CASINO_HEIST_MISSON()
		RETURN 2.5
	ENDIF
	RETURN 6.4
ENDFUNC

/// PURPOSE:
///    Updates drone movement Up/Down left/right forward/backward
PROC  UPDATE_DRONE_MOVEMENT()
	
	IF IS_DRONE_USED_AS_GUIDED_MISSILE()
		EXIT
	ENDIF
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sDroneControl.cameraProp)
	AND NOT IS_ENTITY_DEAD(NET_TO_OBJ(sDroneControl.cameraProp))
	AND DOES_CAM_EXIST(sDroneControl.droneCamera)
		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(sDroneControl.cameraProp)
			FLOAT fBoostMultiplier = 0
			
			IF NOT IS_DRONE_STARTED_FROM_ARENA_WARS_TWO()
			AND NOT IS_DRONE_STARTED_FROM_ARENA_WARS_ONE()
				IF NOT IS_DRONE_BOOST_ACTIVATED()
					IF HAS_NET_TIMER_STARTED(sDroneControl.iBoostChargerTimer)
						INT iBoosChargerTime = g_sMPTunables.iBB_TERRORBYTE_DRONE_BOOST_RECHARGE_TIME
						
						IF IS_DRONE_STARTED_FROM_ARENA_WARS_ONE()
							iBoosChargerTime = iBoosChargerTime / 3
						ENDIF
						
						IF !HAS_NET_TIMER_EXPIRED(sDroneControl.iBoostChargerTimer, iBoosChargerTime)
							INT iBoosTimer = (100 * ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), sDroneControl.iBoostChargerTimer.Timer)))
							sDroneControl.iBoostFill =  iBoosTimer / iBoosChargerTime
						ELSE
							IF IS_ENTITY_ALIVE(sDroneControl.fakePed)
								ATTACH_ENTITY_TO_ENTITY(sDroneControl.fakePed, NET_TO_OBJ(sDroneControl.cameraProp), -1, <<0,0,-0.25>>, <<0,0,0>>)
							ENDIF	
							sDroneControl.iBoostFill = 100
							RESET_NET_TIMER(sDroneControl.iBoostChargerTimer)

							IF !HAS_SOUND_FINISHED(sDroneControl.iBoostRechargerSoundID)
								STOP_SOUND(sDroneControl.iBoostRechargerSoundID)
								RELEASE_SOUND_ID(sDroneControl.iBoostRechargerSoundID)
								sDroneControl.iBoostRechargerSoundID = -1
							ENDIF
						ENDIF
					ENDIF	
					
					BOOL bBoostControl
					IF !IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
						bBoostControl = IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_FRONTEND_LS)
					ELSE
						bBoostControl = IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_FRONTEND_X)
					ENDIF
					
					IF bBoostControl
					AND sDroneControl.iBoostFill = 100
					AND NOT SHOULD_PAUSE_DRONE()
					AND NOT IS_DRONE_SNAPMATIC_ACTIVE()
					AND DOES_THIS_DRONE_HAVE_BOOST()
						fBoostMultiplier = 120
						IF IS_DRONE_STARTED_FROM_ARENA_WARS_ONE()
							fBoostMultiplier = 110
						ELIF IS_DRONE_STARTED_FROM_HEIST_FREEMODE()
							fBoostMultiplier = 49
						ENDIF
						IF sDroneControl.iStickPosition[DRONE_LEFT_Y] != 0.0
							DRONE_BOOST_DIR_FORWARD(TRUE)
						ELSE
							DRONE_BOOST_DIR_FORWARD(FALSE)
						ENDIF
						
						ANIMPOSTFX_PLAY("RaceTurbo", 0, FALSE)
						START_NET_TIMER(sDroneControl.sBoostTimer)
						ACTIVATE_DRONE_BOOST(TRUE)
						
						IF HAS_SOUND_FINISHED(sDroneControl.iBoostLoopSoundID)
							sDroneControl.iBoostLoopSoundID = GET_SOUND_ID()
							PLAY_SOUND_FRONTEND(sDroneControl.iBoostLoopSoundID, "HUD_Boost_Loop", sDroneControl.sSoundSet)
						ENDIF
						
					ENDIF
				ELSE
					fBoostMultiplier = 120
					IF IS_DRONE_STARTED_FROM_ARENA_WARS_ONE()
						fBoostMultiplier = 115
					ELIF IS_DRONE_STARTED_FROM_HEIST_FREEMODE()
						fBoostMultiplier = 49
					ENDIF
					
					IF DOES_CAM_EXIST(sDroneControl.droneCamera)
						IF NOT IS_CAM_SHAKING(sDroneControl.droneCamera)
							SHAKE_CAM(sDroneControl.droneCamera, "DRONE_BOOST_SHAKE")
							SET_CAM_SHAKE_AMPLITUDE(sDroneControl.droneCamera, 0.15)
						ENDIF	
					ENDIF	
					IF HAS_NET_TIMER_STARTED(sDroneControl.sBoostTimer)	
						IF HAS_NET_TIMER_EXPIRED(sDroneControl.sBoostTimer, g_sMPTunables.iBB_TERRORBYTE_DRONE_BOOST_TIME)
							sDroneControl.iBoostFill = 0
							DRONE_BOOST_DIR_FORWARD(FALSE)
							ACTIVATE_DRONE_BOOST(FALSE)
							RESET_NET_TIMER(sDroneControl.iBoostChargerTimer)
							START_NET_TIMER(sDroneControl.iBoostChargerTimer)
							STOP_CAM_SHAKING(sDroneControl.droneCamera, TRUE)
							STOP_CONTROL_SHAKE(PLAYER_CONTROL)
							ANIMPOSTFX_STOP("RaceTurbo")
							RESET_NET_TIMER(sDroneControl.sBoostTimer)
							IF !HAS_SOUND_FINISHED(sDroneControl.iBoostLoopSoundID)
								STOP_SOUND(sDroneControl.iBoostLoopSoundID)
								RELEASE_SOUND_ID(sDroneControl.iBoostLoopSoundID)
								sDroneControl.iBoostLoopSoundID = -1
							ENDIF
							
							IF HAS_SOUND_FINISHED(sDroneControl.iBoostRechargerSoundID)
								sDroneControl.iBoostRechargerSoundID = GET_SOUND_ID()
								PLAY_SOUND_FRONTEND(sDroneControl.iBoostRechargerSoundID, "HUD_Boost_Recharge_Loop", sDroneControl.sSoundSet)
							ENDIF
						ELSE
							FLOAT iBoosFillTimer =  (100.0 / (TO_FLOAT(g_sMPTunables.iBB_TERRORBYTE_DRONE_BOOST_TIME) / GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), sDroneControl.sBoostTimer.Timer)))
							sDroneControl.iBoostFill = 100 - ROUND(iBoosFillTimer)
							SET_CONTROL_SHAKE(PLAYER_CONTROL, g_sMPTunables.iBB_TERRORBYTE_DRONE_BOOST_TIME, ROUND(255 * sDroneControl.fControlShakeMultiplier))
						ENDIF
					ENDIF
				ENDIF	
			ENDIF
			
			#IF IS_DEBUG_BUILD
				IF droneDebugStruct.fDebugBoost != 0
					fBoostMultiplier = droneDebugStruct.fDebugBoost
				ENDIF
			#ENDIF
			
			VECTOR vCameraPropCoord = sDroneControl.vDroneCoords

			IF IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_CAMERA_FUZZ)
			OR SHOULD_CLEANUP_DRONE_CAMERA_INSTANTLY()
				EXIT
			ENDIF

			//Apply positional motion
			VECTOR vForward = NORMALISE_VECTOR(GET_CAMERA_INIT_FORWARD_VECTOR())
			VECTOR vUp = NORMALISE_VECTOR(GET_UP_VECTOR(GET_CAMERA_INIT_FORWARD_VECTOR()))
			
			FLOAT fGroundZero 		
			GET_GROUND_Z_FOR_3D_COORD(vCameraPropCoord, fGroundZero, TRUE)
			
			// Hover
			IF NOT IS_DRONE_HOVER_MODEL_DISABELED()
				IF sDroneControl.iDroneHoverHitSomthing = DRONE_CAM_AREA_CHECK_HIT_SOMETHING
				OR vCameraPropCoord.z - fGroundZero < 2
					
					FLOAT fUpForce = 24
					IF sDroneControl.iStickPosition[DRONE_LEFT_Y] != 0 
						DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_LT)
					ENDIF 
					
					IF GET_CONTROL_UNBOUND_NORMAL(FRONTEND_CONTROL, INPUT_FRONTEND_LT) != 0.0
					OR sDroneControl.iStickPosition[DRONE_LEFT_Y] != 0 
						IF vCameraPropCoord.z - fGroundZero < 0.5
						OR GET_CONTROL_UNBOUND_NORMAL(FRONTEND_CONTROL, INPUT_FRONTEND_LT) != 0.0
							fUpForce = 24
						ELSE	
							fUpForce = 10
						ENDIF	
					ELSE
						fUpForce = 3
					ENDIF
					
					APPLY_FORCE_TO_ENTITY_CENTER_OF_MASS(NET_TO_OBJ(sDroneControl.cameraProp), APPLY_TYPE_FORCE, <<0,0,fUpForce>>  , 0, TRUE, TRUE)
					//CDEBUG1LN(DEBUG_NET_DRONE, "UPDATE_PLAYER_CONTROL - DRONE_CAM_AREA_CHECK_HIT_SOMETHING ")
				ENDIF
			ENDIF
			
			IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
				IF sDroneControl.iStickPosition[DRONE_RIGHT_X] >= 127
					sDroneControl.iStickPosition[DRONE_RIGHT_X] = 127
				ELIF sDroneControl.iStickPosition[DRONE_RIGHT_X] <= -127
					sDroneControl.iStickPosition[DRONE_RIGHT_X] = -127
				ENDIF	
				
				IF sDroneControl.iStickPosition[DRONE_LEFT_X] >= 127
					sDroneControl.iStickPosition[DRONE_LEFT_X] = 127
				ELIF sDroneControl.iStickPosition[DRONE_LEFT_X] <= -127
					sDroneControl.iStickPosition[DRONE_LEFT_X]  = -127
				ENDIF
			ENDIF
			
			// Move up and down 
			BOOL bMoveUpControl, bMoveDownControl
			IF !IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
				IF GET_CONTROL_UNBOUND_NORMAL(FRONTEND_CONTROL, INPUT_FRONTEND_RT) != 0.0
					bMoveUpControl = TRUE
				ENDIF
				IF GET_CONTROL_UNBOUND_NORMAL(FRONTEND_CONTROL, INPUT_FRONTEND_LT) != 0.0
					bMoveDownControl = TRUE
				ENDIF
			ELSE
				IF GET_CONTROL_UNBOUND_NORMAL(FRONTEND_CONTROL, INPUT_FRONTEND_LS) != 0.0 
					bMoveUpControl = TRUE
				ENDIF
				IF GET_CONTROL_UNBOUND_NORMAL(FRONTEND_CONTROL, INPUT_FRONTEND_RS) != 0.0
					bMoveDownControl = TRUE
				ENDIF
			ENDIF
			
			IF (bMoveUpControl AND SHOULD_ALLOW_BOOST_MOVEMENT() AND NOT IS_DRONE_SNAPMATIC_ACTIVE())
			OR (IS_DRONE_BOOST_ACTIVATED() AND !IS_DRONE_BOOST_DIR_FORWARD())
				FLOAT fSpeed, fSpeedMultiplier, fUpDownSpeed  
				
				fUpDownSpeed = sDroneControl.fDroneForwardSpeed
				
				#IF IS_DEBUG_BUILD
				IF droneDebugStruct.fDebugUpDownSpeed != 0.0
					fUpDownSpeed = droneDebugStruct.fDebugUpDownSpeed
				ENDIF
				#ENDIF
				
				IF IS_DRONE_STARTED_FROM_CASINO_HEIST_MISSON()
					fUpDownSpeed = 5.0
				ENDIF
				
				IF IS_DRONE_STARTED_FROM_ARENA_WARS_TWO()
				OR IS_DRONE_STARTED_FROM_ARENA_WARS_ONE()
					fSpeedMultiplier = 10.0
				ENDIF
				
				IF (IS_DRONE_BOOST_ACTIVATED() AND !IS_DRONE_BOOST_DIR_FORWARD())
					IF !IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
						fSpeed = (fUpDownSpeed + fBoostMultiplier) / (1.0 / GET_CONTROL_UNBOUND_NORMAL(FRONTEND_CONTROL, INPUT_FRONTEND_RT))
					ELSE
						fSpeed = (fUpDownSpeed + fBoostMultiplier) / (1.0 / GET_CONTROL_UNBOUND_NORMAL(FRONTEND_CONTROL, INPUT_FRONTEND_LS))
					ENDIF
				ELSE
					IF !IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
						fSpeed = (fUpDownSpeed + fSpeedMultiplier) / (1.0 / GET_CONTROL_UNBOUND_NORMAL(FRONTEND_CONTROL, INPUT_FRONTEND_RT))
					ELSE	
						fSpeed = (fUpDownSpeed + fSpeedMultiplier) / (1.0 / GET_CONTROL_UNBOUND_NORMAL(FRONTEND_CONTROL, INPUT_FRONTEND_LS))
					ENDIF
				ENDIF
				
				// Max force we can apply is 150 
				VECTOR vForce = vUp * fSpeed
				IF vForce.z > 149 
					vForce.z = 149
				ENDIF	
				APPLY_FORCE_TO_ENTITY_CENTER_OF_MASS(NET_TO_OBJ(sDroneControl.cameraProp), APPLY_TYPE_FORCE, vForce, 0, TRUE, TRUE)
				
				#IF IS_DEBUG_BUILD
				IF droneDebugStruct.bDisplayDroneSpeed
					DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_VECTOR(vForce), <<0.5, 0.5, 0.5>>, 0, 255, 0)
				ENDIF
				#ENDIF
			ELIF (bMoveDownControl AND NOT IS_DRONE_SNAPMATIC_ACTIVE())
			AND SHOULD_ALLOW_BOOST_MOVEMENT()
			AND NOT SHOULD_PAUSE_DRONE()
				FLOAT fSpeed, fSpeedMultiplier 
				
				IF IS_DRONE_STARTED_FROM_ARENA_WARS_TWO()
				OR IS_DRONE_STARTED_FROM_ARENA_WARS_ONE()
					fSpeedMultiplier = 10.0
				ENDIF

				IF !IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
					fSpeed = (sDroneControl.fDroneForwardSpeed + fSpeedMultiplier) / (1.0 / GET_CONTROL_UNBOUND_NORMAL(FRONTEND_CONTROL, INPUT_FRONTEND_LT))
				ELSE
					fSpeed = (sDroneControl.fDroneForwardSpeed + fSpeedMultiplier) / (1.0 / GET_CONTROL_UNBOUND_NORMAL(FRONTEND_CONTROL, INPUT_FRONTEND_RS))
				ENDIF
				APPLY_FORCE_TO_ENTITY_CENTER_OF_MASS(NET_TO_OBJ(sDroneControl.cameraProp), APPLY_TYPE_FORCE, vUp * - fSpeed , 0, TRUE, TRUE)
			ENDIF
			
			// Move forward and backward
			IF sDroneControl.iStickPosition[DRONE_LEFT_Y] > 0 
			AND NOT IS_DRONE_BOOST_ACTIVATED()
			AND NOT SHOULD_PAUSE_DRONE()
			AND NOT IS_DRONE_SNAPMATIC_ACTIVE()
				// backward
				FLOAT fSpeed, fUpspeed, fUpMultiplier
				fUpMultiplier = GET_DRONE_UP_MULTIPLIER()
				fSpeed = sDroneControl.fDroneForwardSpeed / (127.0 / sDroneControl.iStickPosition[DRONE_LEFT_Y])
				fUpspeed = fUpMultiplier / (127.0 / sDroneControl.iStickPosition[DRONE_LEFT_Y])
				APPLY_FORCE_TO_ENTITY_CENTER_OF_MASS(NET_TO_OBJ(sDroneControl.cameraProp), APPLY_TYPE_FORCE, vForward * fSpeed, 0, TRUE, TRUE)	
				APPLY_FORCE_TO_ENTITY_CENTER_OF_MASS(NET_TO_OBJ(sDroneControl.cameraProp), APPLY_TYPE_FORCE, <<0, 0, fUpspeed>>, 0, TRUE, TRUE)
			ELIF (sDroneControl.iStickPosition[DRONE_LEFT_Y] < 0 AND NOT IS_DRONE_BOOST_ACTIVATED() AND NOT SHOULD_PAUSE_DRONE() AND NOT IS_DRONE_SNAPMATIC_ACTIVE())
			OR (IS_DRONE_BOOST_ACTIVATED() AND IS_DRONE_BOOST_DIR_FORWARD())
				// Forward
				FLOAT fSpeed, fUpspeed, fUpMultiplier 
				IF IS_DRONE_BOOST_ACTIVATED() AND IS_DRONE_BOOST_DIR_FORWARD()
					fUpMultiplier = 50.0
					IF IS_DRONE_STARTED_FROM_ARENA_WARS_ONE()
						fSpeed = (sDroneControl.fDroneForwardSpeed) / (127/-127)
					ELSE
						fSpeed = (sDroneControl.fDroneForwardSpeed + fBoostMultiplier) / (127/-127)
					ENDIF
					fUpspeed = fUpMultiplier / (127/-127)
				ELSE
					fUpMultiplier = GET_DRONE_UP_MULTIPLIER()
					IF IS_DRONE_STARTED_FROM_ARENA_WARS_ONE()
						fSpeed = (sDroneControl.fDroneForwardSpeed) / (127.0 / sDroneControl.iStickPosition[DRONE_LEFT_Y])
					ELSE
						fSpeed = (sDroneControl.fDroneForwardSpeed + fBoostMultiplier) / (127.0 / sDroneControl.iStickPosition[DRONE_LEFT_Y])
					ENDIF
					fUpspeed = fUpMultiplier / (127.0 / sDroneControl.iStickPosition[DRONE_LEFT_Y])
				ENDIF	
				
				IF IS_DRONE_STARTED_FROM_ARENA_WARS_ONE()
					IF NOT IS_DRONE_BOOST_DIR_FORWARD()
						APPLY_FORCE_TO_ENTITY_CENTER_OF_MASS(NET_TO_OBJ(sDroneControl.cameraProp), APPLY_TYPE_FORCE, <<0, 0, -fUpspeed>>, 0, TRUE, TRUE)
						APPLY_FORCE_TO_ENTITY_CENTER_OF_MASS(NET_TO_OBJ(sDroneControl.cameraProp), APPLY_TYPE_FORCE, vForward * fSpeed, 0, TRUE, TRUE)
					ELSE
						APPLY_FORCE_TO_ENTITY_CENTER_OF_MASS(NET_TO_OBJ(sDroneControl.cameraProp), APPLY_TYPE_FORCE, <<0, 0, 41.0>>, 0, TRUE, TRUE)
						APPLY_FORCE_TO_ENTITY_CENTER_OF_MASS(NET_TO_OBJ(sDroneControl.cameraProp), APPLY_TYPE_FORCE, <<0.0, -149.0, 0.0>>, 0, TRUE, TRUE)
					ENDIF	
				ELSE	
					APPLY_FORCE_TO_ENTITY_CENTER_OF_MASS(NET_TO_OBJ(sDroneControl.cameraProp), APPLY_TYPE_FORCE, <<0, 0, -fUpspeed>>, 0, TRUE, TRUE)
					APPLY_FORCE_TO_ENTITY_CENTER_OF_MASS(NET_TO_OBJ(sDroneControl.cameraProp), APPLY_TYPE_FORCE, vForward * fSpeed, 0, TRUE, TRUE)
				ENDIF
			ENDIF

			VECTOR vRight = CROSS_PRODUCT(vForward, vUp)
			
			// Move Left and right
			IF NOT IS_DRONE_BOOST_ACTIVATED()
			AND NOT SHOULD_PAUSE_DRONE()
			AND NOT IS_DRONE_SNAPMATIC_ACTIVE()
				IF sDroneControl.iStickPosition[DRONE_LEFT_X] > 0 
					FLOAT fSpeed, fUpspeed
					fSpeed = -(sDroneControl.fDroneForwardSpeed / (127.0 / sDroneControl.iStickPosition[DRONE_LEFT_X]))
					fUpspeed = 8 / (127.0 / sDroneControl.iStickPosition[DRONE_LEFT_X])
					APPLY_FORCE_TO_ENTITY_CENTER_OF_MASS(NET_TO_OBJ(sDroneControl.cameraProp), APPLY_TYPE_FORCE, vRight * fSpeed , 0, TRUE, TRUE)
					APPLY_FORCE_TO_ENTITY_CENTER_OF_MASS(NET_TO_OBJ(sDroneControl.cameraProp), APPLY_TYPE_FORCE, <<0, 0, fUpspeed>>, 0, TRUE, TRUE)
				ELIF sDroneControl.iStickPosition[DRONE_LEFT_X] < 0 
					FLOAT fSpeed, fUpspeed
					fSpeed = -(sDroneControl.fDroneForwardSpeed / (127.0 / sDroneControl.iStickPosition[DRONE_LEFT_X]))
					fUpspeed = 8 / (127.0 / sDroneControl.iStickPosition[DRONE_LEFT_X])
					APPLY_FORCE_TO_ENTITY_CENTER_OF_MASS(NET_TO_OBJ(sDroneControl.cameraProp), APPLY_TYPE_FORCE, vRight * fSpeed , 0, TRUE, TRUE)
					APPLY_FORCE_TO_ENTITY_CENTER_OF_MASS(NET_TO_OBJ(sDroneControl.cameraProp), APPLY_TYPE_FORCE, <<0, 0, -fUpspeed>>, 0, TRUE, TRUE)
				ENDIF
			ENDIF	
		ELSE
			NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(sDroneControl.cameraProp)
		ENDIF
	ENDIF	
ENDPROC

PROC UPDATE_MISSILE_DRONE_MOVEMENT()
	
	IF NOT IS_DRONE_USED_AS_GUIDED_MISSILE()
		EXIT
	ENDIF

	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sDroneControl.cameraProp)
	AND NOT IS_ENTITY_DEAD(NET_TO_OBJ(sDroneControl.cameraProp))
	AND DOES_CAM_EXIST(sDroneControl.droneCamera)
		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(sDroneControl.cameraProp)
			IF IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_CAMERA_FUZZ)
			OR SHOULD_CLEANUP_DRONE_CAMERA_INSTANTLY()
				EXIT
			ENDIF

			VECTOR vForward 		= NORMALISE_VECTOR(GET_CAMERA_INIT_FORWARD_VECTOR())
			
			// Speed up and down 
			BOOL bMoveUpControl, bMoveDownControl
			BOOL bAllowMovmentInSelector = FALSE
			BOOL bScaleByMass = TRUE
			
			APPLY_FORCE_TYPE eForceType	= APPLY_TYPE_FORCE
			
			#IF FEATURE_HEIST_ISLAND
			IF IS_DRONE_STARTED_FROM_SUBMARINE_MISSILE()
				bAllowMovmentInSelector = TRUE
				bScaleByMass = FALSE
				eForceType = APPLY_TYPE_FORCE
			ENDIF
			#ENDIF

			IF IS_DRONE_STARTED_FROM_ARENA_WARS_MISSILE()
				IF !IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
					IF GET_CONTROL_UNBOUND_NORMAL(FRONTEND_CONTROL, INPUT_FRONTEND_RT) != 0.0
						bMoveUpControl = TRUE
					ENDIF
					IF GET_CONTROL_UNBOUND_NORMAL(FRONTEND_CONTROL, INPUT_FRONTEND_LT) != 0.0
						bMoveDownControl = TRUE
					ENDIF
				ELSE
					IF GET_CONTROL_UNBOUND_NORMAL(FRONTEND_CONTROL, INPUT_FRONTEND_LS) != 0.0 
						bMoveUpControl = TRUE
					ENDIF
					IF GET_CONTROL_UNBOUND_NORMAL(FRONTEND_CONTROL, INPUT_FRONTEND_RS) != 0.0
						bMoveDownControl = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			IF bMoveUpControl
			AND NOT SHOULD_PAUSE_DRONE(bAllowMovmentInSelector)
				sDroneControl.fDroneForwardSpeed 	= 140.0
				
				#IF IS_DEBUG_BUILD
				IF droneDebugStruct.bDisplayDroneSpeed
					DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_FLOAT(sDroneControl.fDroneForwardSpeed), <<0.5, 0.5, 0.5>>, 0, 255, 0)
				ENDIF
				#ENDIF
			ELIF bMoveDownControl
			AND NOT SHOULD_PAUSE_DRONE(bAllowMovmentInSelector)
				sDroneControl.fDroneForwardSpeed 	= 60.0
				
				#IF IS_DEBUG_BUILD
				IF droneDebugStruct.bDisplayDroneSpeed
					DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_FLOAT(sDroneControl.fDroneForwardSpeed), <<0.5, 0.5, 0.5>>, 255, 0, 0)
				ENDIF
				#ENDIF
			ELSE
				#IF FEATURE_HEIST_ISLAND
				IF NOT IS_DRONE_STARTED_FROM_SUBMARINE_MISSILE()
				#ENDIF
					sDroneControl.fDroneForwardSpeed = 100.0
				#IF FEATURE_HEIST_ISLAND
				ENDIF
				#ENDIF
				
				#IF IS_DEBUG_BUILD
				IF droneDebugStruct.bDisplayDroneSpeed
					DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_FLOAT(sDroneControl.fDroneForwardSpeed), <<0.5, 0.5, 0.5>>, 255, 255, 255)
				ENDIf
				#ENDIF
			ENDIF

			APPLY_FORCE_TO_ENTITY_CENTER_OF_MASS(NET_TO_OBJ(sDroneControl.cameraProp), eForceType, vForward * -sDroneControl.fDroneForwardSpeed, 0, TRUE, bScaleByMass)
		ELSE
			NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(sDroneControl.cameraProp)
		ENDIF
	ENDIF	
ENDPROC
 
PROC UPDATE_MISSILE_DRONE_CAMERA()
	
	IF NOT IS_DRONE_USED_AS_GUIDED_MISSILE()
	OR SHOULD_CLEANUP_DRONE_CAMERA_INSTANTLY()
		EXIT
	ENDIF
	
	IF IS_DRONE_STARTED_FROM_ARENA_WARS_MISSILE()
	AND SHOULD_PAUSE_DRONE()
		EXIT
	ENDIF
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sDroneControl.cameraProp)
	AND NOT IS_ENTITY_DEAD(NET_TO_OBJ(sDroneControl.cameraProp))
		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(sDroneControl.cameraProp)
			CONST_FLOAT FPC_ROT_INTERP			0.050
			CONST_FLOAT FPC_FOV_INTERP			0.060
			FLOAT fPitchRollSpeed = 40.0
			FLOAT fRollLimit = 30
			FLOAT fCamYLimit = 30
			INT fMouseSensitivity = 1
			FLOAT fDoneRoll = GET_ENTITY_ROLL(NET_TO_OBJ(sDroneControl.cameraProp))
			FLOAT fPropPitch = GET_ENTITY_PITCH(NET_TO_OBJ(sDroneControl.cameraProp))
			BOOL bAllowPlayerControlUpdate = TRUE
			
			#IF FEATURE_HEIST_ISLAND
			IF IS_DRONE_STARTED_FROM_SUBMARINE_MISSILE()
				IF SHOULD_PAUSE_DRONE(TRUE)
					 bAllowPlayerControlUpdate = FALSE
				ENDIF 
			ELSE
			#ENDIF
				IF SHOULD_PAUSE_DRONE()
					 bAllowPlayerControlUpdate = FALSE
				ENDIF 
			#IF FEATURE_HEIST_ISLAND
			ENDIF
			#ENDIF

			#IF FEATURE_HEIST_ISLAND
			IF IS_DRONE_STARTED_FROM_SUBMARINE_MISSILE()
				IF NOT CAN_TAKE_CONTROL_OF_SUBMARINE_MISSILE()
					bAllowPlayerControlUpdate = FALSE
				ENDIF
				
				 fCamYLimit = 80
			ENDIF	
			#ENDIF
			
			//*** Steps all camera interpolated values towards their target values. ***
			//Scale by timestep to ensure this is frame rate independent.
			FLOAT fFrameRateModifier = 30.0 * GET_FRAME_TIME()
			
			MAINTAIN_DRONE_MAX_ZOOM()
			
			ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(FRONTEND_CONTROL)
				
			//Get analogue stick positions.
			GET_CONTROL_VALUE_OF_ANALOGUE_STICKS_UNBOUND(sDroneControl.iStickPosition[DRONE_LEFT_X],
											 	 sDroneControl.iStickPosition[DRONE_LEFT_Y],
											 	 sDroneControl.iStickPosition[DRONE_RIGHT_X],
											 	 sDroneControl.iStickPosition[DRONE_RIGHT_Y])
			
			IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
				fMouseSensitivity = 2
				#IF FEATURE_HEIST_ISLAND
				IF IS_DRONE_STARTED_FROM_SUBMARINE_MISSILE()
					fMouseSensitivity = 3
				ENDIF
				#ENDIF
				sDroneControl.iStickPosition[DRONE_RIGHT_X] = sDroneControl.iStickPosition[DRONE_RIGHT_X] * fMouseSensitivity
				sDroneControl.iStickPosition[DRONE_RIGHT_Y] = sDroneControl.iStickPosition[DRONE_RIGHT_Y] * fMouseSensitivity
			ENDIF									 					
												 
			// Invert gamepad stick if look is inverted in settings.
			IF IS_LOOK_INVERTED()
				sDroneControl.iStickPosition[DRONE_RIGHT_Y] *= -1
				sDroneControl.iStickPosition[DRONE_LEFT_Y] *= -1
			ENDIF
			
			IF sDroneControl.iStickPosition[DRONE_RIGHT_Y] != 0
			AND sDroneControl.iStickPosition[DRONE_RIGHT_X] != 0
				INT iStickValue = IMAX(ABSI(sDroneControl.iStickPosition[DRONE_RIGHT_Y]), ABSI(sDroneControl.iStickPosition[DRONE_RIGHT_X]))
				sDroneControl.fRotationalSpeedForSound = 1.0 / (127.0 / iStickValue)
			ELIF sDroneControl.iStickPosition[DRONE_LEFT_Y] != 0
			AND sDroneControl.iStickPosition[DRONE_LEFT_X] != 0
				INT iStickValue = IMAX(ABSI(sDroneControl.iStickPosition[DRONE_LEFT_Y]), ABSI(sDroneControl.iStickPosition[DRONE_LEFT_X]))
				sDroneControl.fRotationalSpeedForSound = 1.0 / (127.0 / iStickValue)
			ELIF sDroneControl.iStickPosition[DRONE_RIGHT_Y] != 0
				sDroneControl.fRotationalSpeedForSound = 1.0 / (127.0 / sDroneControl.iStickPosition[DRONE_RIGHT_Y])
			ELIF sDroneControl.iStickPosition[DRONE_RIGHT_X] != 0	
				sDroneControl.fRotationalSpeedForSound = 1.0 / (127.0 / sDroneControl.iStickPosition[DRONE_RIGHT_X])
			ELIF sDroneControl.iStickPosition[DRONE_LEFT_Y] != 0
				sDroneControl.fRotationalSpeedForSound = 1.0 / (127.0 / sDroneControl.iStickPosition[DRONE_LEFT_Y])
			ELIF  sDroneControl.iStickPosition[DRONE_LEFT_X] != 0	
				sDroneControl.fRotationalSpeedForSound = 1.0 / (127.0 / sDroneControl.iStickPosition[DRONE_LEFT_X])	
			ELSE
				sDroneControl.fRotationalSpeedForSound = 0
			ENDIF
			
			//Update the camera prop with interpolated values.
			IF (bAllowPlayerControlUpdate)
			AND ((sDroneControl.iStickPosition[DRONE_RIGHT_X] != 0 
			OR sDroneControl.iStickPosition[DRONE_RIGHT_Y] != 0)
			OR (sDroneControl.iStickPosition[DRONE_LEFT_X] != 0 
			OR sDroneControl.iStickPosition[DRONE_LEFT_Y] != 0))
			
				FLOAT rotationZAngle, rotationXAngle
				IF sDroneControl.iStickPosition[DRONE_RIGHT_X] != 0
					rotationZAngle = 1.0 / (127.0 / sDroneControl.iStickPosition[DRONE_RIGHT_X])
				ELIF  sDroneControl.iStickPosition[DRONE_LEFT_X] != 0
					rotationZAngle = 1.0 / (127.0 / sDroneControl.iStickPosition[DRONE_LEFT_X])
				ELSE
					rotationZAngle = 0
				ENDIF
				
				IF sDroneControl.iStickPosition[DRONE_RIGHT_Y] != 0
					rotationXAngle = 1.0 / (127.0 / sDroneControl.iStickPosition[DRONE_RIGHT_Y])
				ELIF sDroneControl.iStickPosition[DRONE_LEFT_Y] != 0
					rotationXAngle = 1.0 / (127.0 / sDroneControl.iStickPosition[DRONE_LEFT_Y])
				ELSE
					rotationXAngle = 0
				ENDIF
				
				FLOAT vCameraRotationAngleX, vCameraRotationAngleZ, vCameraRotationAngleY
				VECTOR vPropRot = GET_ENTITY_ROTATION(NET_TO_OBJ(sDroneControl.cameraProp))
				vCameraRotationAngleX = (rotationXAngle * FPC_ROT_INTERP * fFrameRateModifier * sDroneControl.fInterpSpeed) 
				vCameraRotationAngleZ = -(rotationZAngle * FPC_ROT_INTERP * fFrameRateModifier * sDroneControl.fInterpSpeed)

				VECTOR vCameraRotationAngle
				
				// Drone roll check
				IF (fDoneRoll != 0.0 
				OR sDroneControl.iStickPosition[DRONE_RIGHT_X] != 0
				OR sDroneControl.iStickPosition[DRONE_LEFT_X] != 0)
				
					FLOAT rotationYAngle
					IF sDroneControl.iStickPosition[DRONE_RIGHT_X] != 0	
						rotationYAngle = 1.0 / (127.0 / sDroneControl.iStickPosition[DRONE_RIGHT_X])
						vCameraRotationAngleY = -(rotationYAngle * FPC_ROT_INTERP * fFrameRateModifier * (fPitchRollSpeed - 25)) 
					ELIF sDroneControl.iStickPosition[DRONE_LEFT_X] != 0	
						rotationYAngle = 1.0 / (127.0 / sDroneControl.iStickPosition[DRONE_LEFT_X])
						vCameraRotationAngleY = -(rotationYAngle * FPC_ROT_INTERP * fFrameRateModifier * (fPitchRollSpeed - 25)) 
					ELSE
						IF fDoneRoll > 0.1
						OR fDoneRoll < -0.1
							IF fDoneRoll > 0
								rotationYAngle = -1
							ELSE
								rotationYAngle = 1
							ENDIF
						ENDIF	
						
						IF vPropRot.y != 0.0
						IF (vPropRot.y < 2.0
						AND vPropRot.y > 0.0)
							rotationYAngle = 0.0001
						ELIF (vPropRot.y > -2.0
						AND vPropRot.y < 0.0)
							rotationYAngle = -0.0001
							ENDIF
						ELSE
							rotationYAngle = 0
						ENDIF
						
						vCameraRotationAngleY = -(rotationYAngle * FPC_ROT_INTERP * fFrameRateModifier * (fPitchRollSpeed - 25)) 
					ENDIF
				ELSE
				 	vCameraRotationAngleY = 0
				ENDIF	
				
				vCameraRotationAngle = << vCameraRotationAngleX, vCameraRotationAngleY, vCameraRotationAngleZ >> + vPropRot
				
				// Drone roll limit check
				IF fDoneRoll != 0.0
					IF sDroneControl.iStickPosition[DRONE_RIGHT_X] = 0
					AND sDroneControl.iStickPosition[DRONE_LEFT_X] = 0
						IF vCameraRotationAngle.y > fRollLimit
							vCameraRotationAngle.y = fRollLimit
						ELIF vCameraRotationAngle.y < -fRollLimit
							vCameraRotationAngle.y = -fRollLimit
						ENDIF
					ELSE
						IF vCameraRotationAngle.y > fRollLimit
							vCameraRotationAngle.y = fRollLimit
						ELIF vCameraRotationAngle.y < -fRollLimit
							vCameraRotationAngle.y = -fRollLimit
						ENDIF
					ENDIF
				ENDIF
				
				IF vCameraRotationAngle.x > fCamYLimit
					vCameraRotationAngle.x = fCamYLimit
				ELIF vCameraRotationAngle.x < -fCamYLimit
					vCameraRotationAngle.x = -fCamYLimit
				ENDIF
				
				IF NOT IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_ROTATE_X_AXIS)
					SET_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_ROTATE_X_AXIS)
				ENDIF
				
				SET_ENTITY_ROTATION(NET_TO_OBJ(sDroneControl.cameraProp),<<0,0,0>> + <<vCameraRotationAngle.x, vCameraRotationAngle.y, vCameraRotationAngle.z>>)	
					
			// Add roll to camera movement 
			ELIF (bAllowPlayerControlUpdate)
			AND (sDroneControl.iStickPosition[DRONE_RIGHT_X] != 0 
			OR sDroneControl.iStickPosition[DRONE_RIGHT_Y] != 0
			OR sDroneControl.iStickPosition[DRONE_LEFT_X] != 0 
			OR sDroneControl.iStickPosition[DRONE_LEFT_Y] != 0)

				VECTOR vPropRot = GET_ENTITY_ROTATION(NET_TO_OBJ(sDroneControl.cameraProp))
				
				FLOAT rotationYAngle, rotationXAngle
				IF IS_DRONE_BOOST_ACTIVATED() 
					IF IS_DRONE_BOOST_DIR_FORWARD()
						rotationXAngle = -1
					ENDIF	
				ELSE	
					IF sDroneControl.iStickPosition[DRONE_RIGHT_X] != 0
						rotationYAngle = 1.0 / (127.0 / sDroneControl.iStickPosition[DRONE_RIGHT_X])
					ELIF sDroneControl.iStickPosition[DRONE_LEFT_X] != 0
						rotationYAngle = 1.0 / (127.0 / sDroneControl.iStickPosition[DRONE_LEFT_X])
					ELSE
						rotationYAngle = 0
					ENDIF
					
					IF sDroneControl.iStickPosition[DRONE_RIGHT_Y] != 0
						rotationXAngle = 1.0 / (127.0 / sDroneControl.iStickPosition[DRONE_RIGHT_Y])
					ELIF sDroneControl.iStickPosition[DRONE_LEFT_Y] != 0
						rotationXAngle = 1.0 / (127.0 / sDroneControl.iStickPosition[DRONE_LEFT_Y])
					ELSE
						rotationXAngle = 0
					ENDIF
				ENDIF 	
				
				FLOAT vCameraRotationAngleX, vCameraRotationAngleY
				vCameraRotationAngleX = -(rotationXAngle * FPC_ROT_INTERP * fFrameRateModifier * fPitchRollSpeed) 
				
				vCameraRotationAngleY = -(rotationYAngle * FPC_ROT_INTERP * fFrameRateModifier * fPitchRollSpeed)
				
				// Drone roll check
				IF fDoneRoll != 0.0
					IF sDroneControl.iStickPosition[DRONE_RIGHT_X] = 0
					AND sDroneControl.iStickPosition[DRONE_LEFT_X] = 0
						IF fDoneRoll > 1.0
						OR fDoneRoll < -1.0
							IF fDoneRoll > 0
								rotationYAngle = -1
							ELSE
								rotationYAngle = 1
							ENDIF
						ENDIF	
						
						IF (vPropRot.y < 2.0
						AND vPropRot.y > 0.0)
							rotationYAngle = 0.0001
						ELIF (vPropRot.y > -2.0
						AND vPropRot.y < 0.0)
							rotationYAngle = -0.0001
						ENDIF
							
						vCameraRotationAngleY = -(rotationYAngle * FPC_ROT_INTERP * fFrameRateModifier * (fPitchRollSpeed - 25)) 	
					ENDIF
				ENDIF	
				
				VECTOR vCameraRotationAngle = << vCameraRotationAngleX , vCameraRotationAngleY , 0 >> + vPropRot
				
				IF vCameraRotationAngle.y > fRollLimit
					vCameraRotationAngle.y = fRollLimit
				ELIF vCameraRotationAngle.y < -fRollLimit
					vCameraRotationAngle.y = -fRollLimit
				ENDIF
				
				IF vCameraRotationAngle.x > fCamYLimit
					vCameraRotationAngle.x = fCamYLimit
				ELIF vCameraRotationAngle.x < -fCamYLimit
					vCameraRotationAngle.x = -fCamYLimit
				ENDIF
				
				IF IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_ROTATE_X_AXIS)
					CLEAR_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_ROTATE_X_AXIS)
				ENDIF
					
				SET_ENTITY_ROTATION(NET_TO_OBJ(sDroneControl.cameraProp),<<0,0,0>> + <<vCameraRotationAngle.x, vCameraRotationAngle.y, vPropRot.z>>)
				
			ELSE

				VECTOR vPropRot = GET_ENTITY_ROTATION(NET_TO_OBJ(sDroneControl.cameraProp))

				IF fDoneRoll != 0.0
				OR fPropPitch != 0.0
				
					FLOAT rotationYAngle, rotationXAngle
					
					IF vPropRot.y != 0.0
						IF vPropRot.y < 0
							rotationYAngle = -1
						ELSE
							rotationYAngle = 1
						ENDIF
					ELSE
						rotationYAngle = 0
					ENDIF
					
					IF fPropPitch != 0.0
						IF vPropRot.x < 0
							rotationXAngle = -1
						ELSE
							rotationXAngle =  1
						ENDIF
					ELSE	
						rotationXAngle = 0
					ENDIF	
					
					// Smaller value to smother angle between 0 and 1 or 0 and -1
					IF vPropRot.y != 0.0
						IF (vPropRot.y < 2.0
						AND vPropRot.y > 0.0)
							rotationYAngle = 0.0001
						ELIF (vPropRot.y > -2.0
						AND vPropRot.y < 0.0)
							rotationYAngle = -0.0001
						ENDIF
					ELSE
						rotationYAngle = 0
					ENDIF	
					
					IF vPropRot.x != 0.0
						IF (vPropRot.x < 2.0
						AND vPropRot.x > 0.0)
							rotationXAngle = 0.0001
						ELIF (vPropRot.x > -2.0
						AND vPropRot.x < 0.0)
							rotationXAngle = -0.0001
						ENDIF
					ELSE
						rotationXAngle = 0
					ENDIF	
					
					FLOAT vCameraRotationAngleX, vCameraRotationAngleY
					vCameraRotationAngleX = NORMALIZE_ANGLE(-(rotationXAngle * FPC_ROT_INTERP * fFrameRateModifier * (fPitchRollSpeed - 25))) 		// Pitch up/down
					
					vCameraRotationAngleY = NORMALIZE_ANGLE(-(rotationYAngle * FPC_ROT_INTERP * fFrameRateModifier * (fPitchRollSpeed - 25)))		// Roll right/left

					VECTOR vCameraRotationAngle = << vCameraRotationAngleX , vCameraRotationAngleY , 0 >> + vPropRot
					
					IF IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_ROTATE_X_AXIS)
						// We are not going rotate x if Right Stick is changed
						vCameraRotationAngle.x =  vPropRot.x
					ENDIF
					
					FLOAT fRotz = vPropRot.z
					#IF FEATURE_HEIST_ISLAND
					IF IS_DRONE_STARTED_FROM_SUBMARINE_MISSILE()
						IF NOT CAN_TAKE_CONTROL_OF_SUBMARINE_MISSILE()
							fRotz = sDroneControl.fSubmarineMissileHeading
							vCameraRotationAngle.y = 0.0
						ENDIF
					ENDIF
					#ENDIF
					
					SET_ENTITY_ROTATION(NET_TO_OBJ(sDroneControl.cameraProp), <<0,0,0>> + <<vCameraRotationAngle.x, vCameraRotationAngle.y, fRotz>>)
					
					#IF FEATURE_HEIST_ISLAND
					IF IS_DRONE_STARTED_FROM_SUBMARINE_MISSILE()
						IF ABSF(fDoneRoll) < 2.5
						AND ABSF(fPropPitch) < 2.5
							SET_CAN_TAKE_CONTROL_OF_SUBMARINE_MISSILE(TRUE)
						ENDIF
					ENDIF	
					#ENDIF
				ELSE
					#IF FEATURE_HEIST_ISLAND
					IF IS_DRONE_STARTED_FROM_SUBMARINE_MISSILE()
						SET_CAN_TAKE_CONTROL_OF_SUBMARINE_MISSILE(TRUE)
					ENDIF	
					#ENDIF
				ENDIF
			ENDIF
		ELSE
			NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(sDroneControl.cameraProp)
		ENDIF
	ENDIF	
ENDPROC

/// PURPOSE:
///    Shapetest to check if drone is touching ground so that we can apply force to keep it floating
PROC PROCESS_DRONE_HOVER_SHAPETEST()
	
	IF IS_DRONE_HOVER_MODEL_DISABELED()
		EXIT
	ENDIF
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sDroneControl.cameraProp)
	AND NOT IS_ENTITY_DEAD(NET_TO_OBJ(sDroneControl.cameraProp))
		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(sDroneControl.cameraProp)
			SWITCH sDroneControl.idroneHoverShapeTest
		        CASE DRONE_SHAPETEST_INIT
		           
					// See if we can collide with ground

					VECTOR vEndPointCam
					vEndPointCam = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(NET_TO_OBJ(sDroneControl.cameraProp)), GET_ENTITY_HEADING(NET_TO_OBJ(sDroneControl.cameraProp)), <<0, 0.0, -sDroneControl.fHoverHeight>>)

					sDroneControl.droneHoverShapeTest = START_SHAPE_TEST_CAPSULE(sDroneControl.vDroneCoords, vEndPointCam, GET_MODEL_HEIGHT(GET_ENTITY_MODEL(NET_TO_OBJ(sDroneControl.cameraProp)))  , SCRIPT_INCLUDE_ALL, NET_TO_OBJ(sDroneControl.cameraProp))
					
					#IF IS_DEBUG_BUILD
					IF droneDebugStruct.bShowShapeTests
						DRAW_DEBUG_SPHERE(sDroneControl.vDroneCoords, GET_MODEL_HEIGHT(GET_ENTITY_MODEL(NET_TO_OBJ(sDroneControl.cameraProp)))  , 0, 0, 255, 64)
						DRAW_DEBUG_SPHERE(vEndPointCam, GET_MODEL_HEIGHT(GET_ENTITY_MODEL(NET_TO_OBJ(sDroneControl.cameraProp)))  , 255, 0, 0, 64)
					ENDIF
					#ENDIF
					
					IF NATIVE_TO_INT(sDroneControl.droneHoverShapeTest) != 0
						sDroneControl.idroneHoverShapeTest 		= DRONE_SHAPETEST_PROCESS
						PRINTLN("[AM_MP_DRONE] - PROCESS_DRONE_HOVER_SHAPETEST sDroneControl.idroneHoverShapeTest INIT to PROCESS ")
					ENDIF	
						
		        BREAK
		        CASE DRONE_SHAPETEST_PROCESS
		        
		            INT iHitSomething
		            VECTOR vNormalAtPosHit
		            ENTITY_INDEX entityHit
					
		           	SHAPETEST_STATUS shapetestStatus 
					shapetestStatus = GET_SHAPE_TEST_RESULT(sDroneControl.droneHoverShapeTest, iHitSomething, sDroneControl.vDroneHoverHitPos, vNormalAtPosHit, entityHit)
					
					// result is ready for left area
		            IF shapetestStatus = SHAPETEST_STATUS_RESULTS_READY
		                #IF IS_DEBUG_BUILD
						PRINTLN("[AM_MP_DRONE] - PROCESS_DRONE_HOVER_SHAPETEST - We got shapetest results!")
						#ENDIF
						
						IF iHitSomething = 0
							
							sDroneControl.iDroneHoverHitSomthing = DRONE_CAM_AREA_CHECK_NOT_HIT_ANYTHING
							
							sDroneControl.vDroneHoverHitPos = <<0,0,0>> 
							#IF IS_DEBUG_BUILD
							IF GET_FRAME_COUNT() % 120 = 0
								PRINTLN("[AM_MP_DRONE] - PROCESS_DRONE_HOVER_SHAPETEST DRONE_CAM_AREA_CHECK_NOT_HIT_ANYTHING ")
							ENDIF
							IF droneDebugStruct.bShowShapeTests
								DRAW_DEBUG_TEXT_ABOVE_ENTITY(NET_TO_OBJ(sDroneControl.cameraProp), "Hover shape test hit somthing = FALSE", 1,0,255,0)		
							ENDIF	
							#ENDIF
							
		                ELSE
						
		                    sDroneControl.iDroneHoverHitSomthing = DRONE_CAM_AREA_CHECK_HIT_SOMETHING
							PRINTLN("[AM_MP_DRONE] - PROCESS_DRONE_HOVER_SHAPETEST  DRONE_CAM_AREA_CHECK_HIT_SOMETHING ")

							#IF IS_DEBUG_BUILD
							IF droneDebugStruct.bShowShapeTests
								DRAW_DEBUG_TEXT_ABOVE_ENTITY(NET_TO_OBJ(sDroneControl.cameraProp), "Hover shape test hit somthing = TRUE", 1)
							ENDIF
							#ENDIF
							sDroneControl.droneHoverShapeTest = INT_TO_NATIVE(SHAPETEST_INDEX, 0)
							sDroneControl.idroneHoverShapeTest 	= DRONE_SHAPETEST_INIT  
							
		                ENDIF
						 
					ELIF shapetestStatus	= SHAPETEST_STATUS_NONEXISTENT
						sDroneControl.idroneHoverShapeTest 	= DRONE_SHAPETEST_INIT    
		            ENDIF

		        BREAK
		    ENDSWITCH
		ELSE	
			NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(sDroneControl.cameraProp)
		ENDIF	
    ENDIF
ENDPROC

FUNC  STRING GET_EXIT_TIME_CYCLE_NAME()
	#IF FEATURE_HEIST_ISLAND
	IF IS_DRONE_STARTED_FROM_SUBMARINE_MISSILE()
		RETURN "MissileOutOfRange"
	ENDIF
	#ENDIF
	
	RETURN "RemixDrone"
ENDFUNC

PROC RETURN_TO_PLAYER_CONTROLS()
	
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_STREET_NAME)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_DISTRICT_NAME)
	HIDE_HUD_AND_RADAR_THIS_FRAME()
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_FRONTEND_PAUSE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
	HIDE_PLAYER_ARROW_THIS_FRAME()	
	REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
	DISABLE_CELLPHONE_THIS_FRAME_ONLY(TRUE)
	
	IF NOT IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_CAMERA_FUZZ)
		
		IF !IS_PLAYER_IN_DRONE_SEAT_IN_HACKER_TRUCK()
		AND NOT IS_LOCAL_PLAYER_IN_ARENA_SPECTATOR_BOX()
			IF SHOULD_START_DRONE_WITHOUT_SEAT()
			OR IS_DRONE_STARTED_FROM_ARENA_WARS_ONE()
			OR IS_DRONE_STARTED_FROM_ARENA_WARS_TWO()
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				PRINTLN("[AM_MP_DRONE] - RETURN_TO_PLAYER_CONTROLS DRONE_LOCAL_BS_CAMERA_FUZZ clear ped task")
			ENDIF
		ENDIF
		
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sDroneControl.cameraProp)
			SET_ENTITY_HAS_GRAVITY(NET_TO_OBJ(sDroneControl.cameraProp), TRUE)
		ENDIF
		
		IF HAS_SOUND_FINISHED(sDroneControl.iStaticSoundID)
			sDroneControl.iStaticSoundID = GET_SOUND_ID()
			PLAY_SOUND_FRONTEND(sDroneControl.iStaticSoundID, "HUD_Static_Loop", sDroneControl.sSoundSet)
		ENDIF	
		
		IF !IS_DRONE_FORCED_EXTERNAL_CLEAN_UP()
			#IF IS_DEBUG_BUILD
			IF g_bForceStopRenderDroneCam
				// skip
			ELSE
			#ENDIF
				SET_CONTROL_SHAKE(PLAYER_CONTROL, 300, 100)
				SET_TIMECYCLE_MODIFIER(GET_EXIT_TIME_CYCLE_NAME())
				SET_BIT(sDroneControl.iLocalBSTwo, DRONE_LOCAL_BS_TWO_START_TC)
			#IF IS_DEBUG_BUILD
			ENDIF
			#ENDIF
		ENDIF
		
		SET_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_CAMERA_FUZZ)
		PRINTLN("[AM_MP_DRONE] - RETURN_TO_PLAYER_CONTROLS DRONE_LOCAL_BS_CAMERA_FUZZ TRUE")
	ELSE
		IF NOT HAS_NET_TIMER_STARTED(sDroneControl.sFuzzCycleTimer)
			START_NET_TIMER(sDroneControl.sFuzzCycleTimer)
			PRINTLN("[AM_MP_DRONE] - RETURN_TO_PLAYER_CONTROLS START_NET_TIMER sFuzzCycleTimer")
		ELSE
			INT iFuzzTime = 1000
			IF IS_DRONE_STARTED_FROM_ARENA_WARS_MISSILE()
				iFuzzTime = 500
			#IF FEATURE_HEIST_ISLAND
			ELIF (IS_DRONE_STARTED_FROM_SUBMARINE_MISSILE() AND IS_DOING_SUBMARINE_FAST_TRAVEL())
				iFuzzTime = 150
			#ENDIF
			ENDIF
			IF HAS_NET_TIMER_EXPIRED(sDroneControl.sFuzzCycleTimer, iFuzzTime)
			OR IS_DRONE_FORCED_EXTERNAL_CLEAN_UP()
				IF sDroneControl.iHudDisconnectSound = -1
					IF HAS_SOUND_FINISHED(sDroneControl.iHudDisconnectSound)
						sDroneControl.iHudDisconnectSound = GET_SOUND_ID()
						PLAY_SOUND_FRONTEND(sDroneControl.iHudDisconnectSound, "HUD_Disconnect", sDroneControl.sSoundSet)
					ENDIF
				ENDIF	
				PRINTLN("[AM_MP_DRONE] - RETURN_TO_PLAYER_CONTROLS Done with sFuzzCycleTimer")
				SET_DRONE_CLEANING_UP(TRUE)
				SET_DRONE_STATE(DRONE_STATE_CLEANUP)
			ENDIF
		ENDIF
	ENDIF	

ENDPROC

FUNC STRING GET_DRONE_ANIM_NAME(DRONE_ANIMATION_NAME eDroneAnimName)
	SWITCH eDroneAnimName
		CASE DRONE_ANIM_ENTER		RETURN "ENTER"
		CASE DRONE_ANIM_BASE		RETURN "BASE"
		CASE DRONE_ANIM_USE_01		RETURN "USE_01"
		CASE DRONE_ANIM_USE_02		RETURN "USE_02"
		CASE DRONE_ANIM_USE_03		RETURN "USE_03"
		CASE DRONE_ANIM_FAIL		RETURN "FAIL"
		CASE DRONE_ANIM_EXIT		RETURN "EXIT"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

PROC PERFORM_DRONE_SYNC_SCENE(DRONE_ANIMATION_NAME eDroneAnimName, BOOL bLoopAnim = FALSE, BOOL bHoldLastFrame = FALSE)
	TEXT_LABEL_63 sClip = GET_DRONE_ANIM_NAME(eDroneAnimName)
	SYNCED_SCENE_PLAYBACK_FLAGS eFlag = SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT
	
	IF bLoopAnim
		eFlag = SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_LOOP_WITHIN_SCENE | SYNCED_SCENE_DONT_INTERRUPT
	ENDIF
	
	FLOAT fBlendIn 		= NORMAL_BLEND_IN
	FLOAT fBlendOut 	= NORMAL_BLEND_OUT
	
	sDroneControl.iDroneSyncScene = NETWORK_CREATE_SYNCHRONISED_SCENE(sDroneControl.vAnimCoords , <<0, 0, sDroneControl.vAnimHeading>>, DEFAULT, bHoldLastFrame, bLoopAnim)
	NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), sDroneControl.iDroneSyncScene, sDroneControl.sAnimDicName, sClip, fBlendIn, fBlendOut, eFlag)
	
	
	IF eDroneAnimName = DRONE_ANIM_ENTER
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sDroneControl.cameraProp)
			sClip += "_DRONE" 

			NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_ENT(sDroneControl.cameraProp), sDroneControl.iDroneSyncScene, sDroneControl.sAnimDicName, sClip, fBlendIn, fBlendOut, SYNCED_SCENE_ON_ABORT_STOP_SCENE)
		ENDIF	
	ENDIF
	
	sClip = GET_DRONE_ANIM_NAME(eDroneAnimName)
	sClip += "_PHONE"
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sDroneControl.phoneObject)
		//NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_ENT(sDroneControl.phoneObject), sDroneControl.iDroneSyncScene, sDroneControl.sAnimDicName, sClip, fBlendIn, fBlendOut, eFlag)
		PLAY_ENTITY_ANIM(NET_TO_ENT(sDroneControl.phoneObject), sClip, sDroneControl.sAnimDicName, fBlendIn, bLoopAnim, bHoldLastFrame, FALSE, 0)
		FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(NET_TO_ENT(sDroneControl.phoneObject))
	ENDIF
	
	NETWORK_START_SYNCHRONISED_SCENE(sDroneControl.iDroneSyncScene)
	PRINTLN("[AM_MP_DRONE] - PERFORM_DRONE_SYNC_SCENE ", sClip)
ENDPROC

PROC PROCESS_DRONE_COLLISION()
	
	IF IS_DRONE_USED_AS_GUIDED_MISSILE()
		g_sDroneGlobals.iDroneCollisionTime = 0
	ENDIF
	
	IF SHOULD_PROCESS_DRONE_CLEANUP()
	OR SHOULD_CLEANUP_DRONE_CAMERA_INSTANTLY()
		IF NOT HAS_NET_TIMER_STARTED(sDroneControl.sCollisionTimer)
			START_NET_TIMER(sDroneControl.sCollisionTimer)
		ELSE
			IF HAS_NET_TIMER_EXPIRED(sDroneControl.sCollisionTimer, g_sDroneGlobals.iDroneCollisionTime)
			OR SHOULD_CLEANUP_DRONE_CAMERA_INSTANTLY()
				RESET_NET_TIMER(sDroneControl.sFuzzCycleTimer)
				IF IS_DRONE_USED_AS_GUIDED_MISSILE()
					IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
						ADD_OWNED_EXPLOSION(PLAYER_PED_ID(), GET_DRONE_MISSILE_EXPLOSION_COORD(), GET_DRONE_EXPLOSION_TAG(), GET_DRONE_MISSILE_EXPLOSION_RADIUS())
					ELSE
						ADD_EXPLOSION(GET_DRONE_MISSILE_EXPLOSION_COORD(), GET_DRONE_EXPLOSION_TAG(), GET_DRONE_MISSILE_EXPLOSION_RADIUS())
						PRINTLN("[AM_MP_DRONE] - PROCESS_DRONE_COLLISION player is dead ADD_EXPLOSION")
					ENDIF
					SET_CONTROL_SHAKE(PLAYER_CONTROL, 300, ROUND(200 * sDroneControl.fControlShakeMultiplier))
					
					SET_NETWORK_ID_CAN_MIGRATE(sDroneControl.cameraProp, TRUE)
					CLEAR_FOCUS()
					OBJECT_INDEX cameraProp = NET_TO_OBJ(sDroneControl.cameraProp)
					DELETE_OBJECT(cameraProp)
					
					SET_BIT(sDroneControl.iLocalBSTwo, DRONE_LOCAL_BS_TWO_MISSILE_CLEANUP_EXPLOSIVE)
				ENDIF
				
				IF SHOULD_START_DRONE_FROM_PLAYER_PHONE()
					STOP_CURRENT_DRONE_SYNCHED_SCENE(TRUE)
					IF sDroneControl.iDroneStatEndReason = DRONE_CLEANUP_REASON_RETURNED_TO_PLAYER
						PERFORM_DRONE_SYNC_SCENE(DRONE_ANIM_EXIT)
					ELSE
						PERFORM_DRONE_SYNC_SCENE(DRONE_ANIM_FAIL)
					ENDIF
				ENDIF	
				
				SET_DRONE_CLEANING_UP(TRUE)
				SET_DRONE_STATE(DRONE_STATE_FUZZ_CAM)
				PRINTLN("[AM_MP_DRONE] - PROCESS_DRONE_COLLISION Drone collided")
			ENDIF
		ENDIF
	ELSE
		IF HAS_NET_TIMER_STARTED(sDroneControl.sCollisionTimer)
			REINIT_NET_TIMER(sDroneControl.sCollisionTimer)
		ENDIF
	ENDIF	
ENDPROC

FUNC BOOL SHOULD_CLEANUP_DRONE_WHEN_COLLIDE_WITH_MODEL(MODEL_NAMES entityModel)
	SWITCH entityModel
		CASE CHERNOBOG
		CASE DUNE4
		CASE DUNE5
		CASE SKYLIFT
		CASE TACO
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	IF IS_VEHICLE_A_SUPERMOD_MODEL(entityModel, SUPERMOD_FLAG_HAS_HYDRAULICS)	
		RETURN TRUE
	ENDIF
	
	IF IS_THIS_MODEL_A_HELI(entityModel)
	OR IS_THIS_MODEL_A_PLANE(entityModel)
		RETURN TRUE
	ENDIF 
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Shapetest twice size of drone prop to catch any collisions
PROC PROCESS_DRONE_COLLISION_SHAPETEST()
	
	IF IS_DRONE_STARTED_FROM_ARENA_WARS_MISSILE()
	OR IS_DRONE_STARTED_FROM_ARENA_WARS_ONE()
	OR IS_DRONE_STARTED_FROM_ARENA_WARS_TWO()
	#IF FEATURE_HEIST_ISLAND
	OR IS_DRONE_STARTED_FROM_SUBMARINE_MISSILE()
	#ENDIF
		IF NOT IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_TURN_ON_DRONE_COLLISION)
			EXIT
		ENDIF
	ENDIF	

	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sDroneControl.cameraProp)
	AND NOT IS_ENTITY_DEAD(NET_TO_OBJ(sDroneControl.cameraProp))
		
		IF IS_ENTITY_IN_GHOST_COLLISION(NET_TO_OBJ(sDroneControl.cameraProp))
			EXIT
		ENDIF
		
		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(sDroneControl.cameraProp)
			INT iRand
			VECTOR vRocketOffset, vRot, vDir, vOffset 		
			
			SWITCH sDroneControl.idroneCollisionShapeTest
		        CASE DRONE_SHAPETEST_INIT
		           
					// See if we can collide with anything around drone
					FLOAT fRadius 
					VECTOR vStartCoord, vEndCoord
					
					IF IS_DRONE_STARTED_FROM_ARENA_WARS_ONE()
					OR IS_DRONE_STARTED_FROM_ARENA_WARS_TWO()
					OR IS_DRONE_STARTED_FROM_ARENA_WARS_MISSILE()
						fRadius = 0.3
					#IF FEATURE_HEIST_ISLAND
					ELIF IS_DRONE_STARTED_FROM_SUBMARINE_MISSILE()
						fRadius = 0.65
					#ENDIF
					ELSE	
						fRadius = GET_MODEL_HEIGHT(sDroneControl.dronePropModel) * 1.5
					ENDIF
					
					IF IS_DRONE_USED_AS_GUIDED_MISSILE()
						vStartCoord = sDroneControl.vDroneCoords
							
						vRocketOffset 	= GET_FINAL_RENDERED_CAM_COORD()
						vRot 			= GET_FINAL_RENDERED_CAM_ROT()
						vDir 			= <<-SIN(vRot.z) * COS(vRot.x), COS(vRot.z) * COS(vRot.x), SIN(vRot.x)>>
						vOffset 		= <<0.900, 0.900, 0.900>>
						#IF FEATURE_HEIST_ISLAND
						IF IS_DRONE_STARTED_FROM_SUBMARINE_MISSILE()
							vOffset 		= <<1.9, 1.9, 1.9>>
						ENDIF	
						#ENDIF
						vEndCoord		= vRocketOffset + (vDir*vOffset)
					ELSE
						vStartCoord = sDroneControl.vDroneCoords
						vEndCoord 	= sDroneControl.vDroneCoords
					ENDIF
					
					sDroneControl.droneCollisionShapeTest = START_SHAPE_TEST_CAPSULE(vStartCoord, vEndCoord, fRadius, SCRIPT_INCLUDE_ALL, NET_TO_OBJ(sDroneControl.cameraProp))
					
					#IF IS_DEBUG_BUILD
					IF droneDebugStruct.bShowShapeTests	
						DRAW_DEBUG_SPHERE(vStartCoord, fRadius, 255)
						DRAW_DEBUG_SPHERE(vEndCoord, fRadius, 255)
					ENDIF
					#ENDIF
					
					IF NATIVE_TO_INT(sDroneControl.droneCollisionShapeTest) != 0
						sDroneControl.idroneCollisionShapeTest 		= DRONE_SHAPETEST_PROCESS
						//PRINTLN("[AM_MP_DRONE] - PROCESS_DRONE_COLLISION_SHAPETEST sDroneControl.idroneCollisionShapeTest INIT to PROCESS ")
					ENDIF	
						
		        BREAK
		        CASE DRONE_SHAPETEST_PROCESS
		        
		            INT iHitSomething
		            VECTOR vNormalAtPosHit, vDroneCollisionHitPos
		            ENTITY_INDEX entityHit
					 
		           	SHAPETEST_STATUS shapetestStatus 
					shapetestStatus = GET_SHAPE_TEST_RESULT(sDroneControl.droneCollisionShapeTest, iHitSomething, vDroneCollisionHitPos, vNormalAtPosHit, entityHit)
					
					// result is ready
		            IF shapetestStatus = SHAPETEST_STATUS_RESULTS_READY
		                #IF IS_DEBUG_BUILD
						//PRINTLN("[AM_MP_DRONE] - PROCESS_DRONE_COLLISION_SHAPETEST - We got shapetest results!")
						#ENDIF
						
						IF iHitSomething = 0
							
							sDroneControl.iDroneCollisionHitSomthing = DRONE_CAM_AREA_CHECK_NOT_HIT_ANYTHING
							//PRINTLN("[AM_MP_DRONE] - PROCESS_DRONE_COLLISION_SHAPETEST DRONE_CAM_AREA_CHECK_NOT_HIT_ANYTHING ")
							vDroneCollisionHitPos = <<0,0,0>> 
							#IF IS_DEBUG_BUILD
							//DRAW_DEBUG_TEXT_2D("Left area hit somthing = FALSE", <<0.1,0.1,0.5>>,0,255,0)
							//DRAW_DEBUG_TEXT_ABOVE_ENTITY(NET_TO_OBJ(sDroneControl.cameraProp), "Drone collision hit somthing = FALSE", 1,0,255,0)		
							#ENDIF
							
		                ELSE
							IF DOES_ENTITY_EXIST(entityHit)
								IF IS_ENTITY_A_VEHICLE(entityHit)
								 	IF !IS_ENTITY_DEAD(entityHit)
									AND GET_ENTITY_MODEL(entityHit) != sDroneControl.dronePropModel
										IF GET_ENTITY_SPEED(entityHit) > 0.5
										OR SHOULD_CLEANUP_DRONE_WHEN_COLLIDE_WITH_MODEL(GET_ENTITY_MODEL(entityHit))
											IF NOT IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_DRONE_COLLIDE_WITH_VEH)
												SET_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_DRONE_COLLIDE_WITH_VEH)
												PRINTLN("[AM_MP_DRONE] - PROCESS_DRONE_COLLISION_SHAPETEST DRONE_LOCAL_BS_DRONE_COLLIDE_WITH_VEH TRUE ")
											ENDIF
										ENDIF
									ENDIF	
								ELIF IS_ENTITY_A_PED(entityHit)
									IF !IS_ENTITY_DEAD(entityHit)
										 IF NOT IS_PED_A_PLAYER(GET_PED_INDEX_FROM_ENTITY_INDEX(entityHit))
											IF sDroneControl.iDroneShockEvent = 0
												EVENT_NAMES shockingEvent 
												iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
												IF iRand = 0
													shockingEvent = EVENT_SHOCKING_DRIVING_ON_PAVEMENT
												ELIF iRand = 1
													shockingEvent = EVENT_SHOCKING_GUN_FIGHT
												ELSE
													shockingEvent = EVENT_SHOCKING_CAR_CRASH
												ENDIF
												sDroneControl.iDroneShockEvent = ADD_SHOCKING_EVENT_AT_POSITION(shockingEvent, sDroneControl.vDroneCoords, 5000)
												REINIT_NET_TIMER(sDroneControl.sShockEventTimer)
												PRINTLN("[AM_MP_DRONE] - PROCESS_DRONE_COLLISION_SHAPETEST ADD_SHOCKING_EVENT_AT_POSITION")
											ENDIF	
										ENDIF
									ENDIF	
								ENDIF
								
								#IF IS_DEBUG_BUILD
								IF IS_ENTITY_AN_OBJECT(entityHit)
								OR IS_ENTITY_A_VEHICLE(entityHit)
								OR IS_ENTITY_A_PED(entityHit)
									IF !IS_ENTITY_DEAD(entityHit)
										IF GET_ENTITY_MODEL(entityHit) != DUMMY_MODEL_FOR_SCRIPT
											PRINTLN("[AM_MP_DRONE] - PROCESS_DRONE_COLLISION_SHAPETEST we are hitting: " , GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(entityHit)))
										ENDIF
									ELSE
										PRINTLN("[AM_MP_DRONE] - PROCESS_DRONE_COLLISION_SHAPETEST we are hitting dead entity")
									ENDIF
								ENDIF	
								#ENDIF
								
							ENDIF	
							
		                    sDroneControl.iDroneCollisionHitSomthing = DRONE_CAM_AREA_CHECK_HIT_SOMETHING
							
							SET_CONTROL_SHAKE(PLAYER_CONTROL, 300, 50)
							
							#IF IS_DEBUG_BUILD
								DRAW_DEBUG_TEXT_ABOVE_ENTITY(NET_TO_OBJ(sDroneControl.cameraProp), "Drone collision hit somthing = TRUE", 1)
								PRINTLN("[AM_MP_DRONE] - PROCESS_DRONE_COLLISION_SHAPETEST  DRONE_CAM_AREA_CHECK_HIT_SOMETHING ")
								//DRAW_DEBUG_TEXT_2D("Left area hit somthing = TRUE", <<0.1,0.1,0.5>>,255,0,0)
							#ENDIF
							sDroneControl.droneCollisionShapeTest = INT_TO_NATIVE(SHAPETEST_INDEX, 0)
							sDroneControl.idroneCollisionShapeTest 	= DRONE_SHAPETEST_INIT  
							
		                ENDIF
						 
					ELIF shapetestStatus	= SHAPETEST_STATUS_NONEXISTENT
						sDroneControl.idroneCollisionShapeTest 	= DRONE_SHAPETEST_INIT    
		            ENDIF

		        BREAK
		    ENDSWITCH
			
			IF sDroneControl.iDroneShockEvent != 0
				IF HAS_NET_TIMER_STARTED(sDroneControl.sShockEventTimer)
					IF HAS_NET_TIMER_EXPIRED(sDroneControl.sShockEventTimer, 5000)
						REMOVE_SHOCKING_EVENT(sDroneControl.iDroneShockEvent)
						RESET_NET_TIMER(sDroneControl.sShockEventTimer)
						sDroneControl.iDroneShockEvent = 0
						PRINTLN("[AM_MP_DRONE] - PROCESS_DRONE_COLLISION_SHAPETEST REMOVE_SHOCKING_EVENT")
					ENDIF
				ENDIF	
			ENDIF	
			
		ELSE	
			NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(sDroneControl.cameraProp)
		ENDIF	
    ENDIF
ENDPROC

FUNC BOOL IS_PED_A_COP(PED_INDEX pedId)
	IF DOES_ENTITY_EXIST(pedId)
		IF GET_PED_TYPE(pedId) = PEDTYPE_COP
		OR GET_PED_TYPE(pedId) = PEDTYPE_ARMY
		OR GET_PED_TYPE(pedId) = PEDTYPE_SWAT
		OR GET_PED_RELATIONSHIP_GROUP_DEFAULT_HASH(pedId) =	RELGROUPHASH_AMBIENT_ARMY
		OR GET_PED_RELATIONSHIP_GROUP_DEFAULT_HASH(pedId) =	RELGROUPHASH_COP
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PED_IN_A_GANG(PED_INDEX pedId)
	IF DOES_ENTITY_EXIST(pedId)
		IF GET_PED_TYPE(pedId) =  PEDTYPE_GANG1
		OR GET_PED_TYPE(pedId) =  PEDTYPE_GANG2
		OR GET_PED_TYPE(pedId) =  PEDTYPE_GANG3
		OR GET_PED_TYPE(pedId) =  PEDTYPE_GANG4
		OR GET_PED_TYPE(pedId) =  PEDTYPE_GANG5
		OR GET_PED_TYPE(pedId) =  PEDTYPE_GANG6
		OR GET_PED_TYPE(pedId) =  PEDTYPE_GANG7
		OR GET_PED_TYPE(pedId) =  PEDTYPE_GANG8
		OR GET_PED_TYPE(pedId) =  PEDTYPE_GANG9
		OR GET_PED_TYPE(pedId) =  PEDTYPE_GANG10
		OR GET_PED_TYPE(pedId) =  PEDTYPE_GANG_CHINESE_JAPANESE
		OR GET_PED_TYPE(pedId) =  PEDTYPE_GANG_PUERTO_RICAN
		OR GET_PED_TYPE(pedId) =  PEDTYPE_DEALER
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PED_A_PRISON_GUARD(PED_INDEX pedId)
	IF DOES_ENTITY_EXIST(pedId)
		IF GET_PED_RELATIONSHIP_GROUP_DEFAULT_HASH(pedId) = RELGROUPHASH_SECURITY_GUARD
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC	

PROC CLEAR_HATED_RELATIONSHIP()
	IF HAS_NET_TIMER_STARTED(sDroneControl.sRelshipGroupTimer)
		IF HAS_NET_TIMER_EXPIRED(sDroneControl.sRelshipGroupTimer, 60000)
			RESET_NET_TIMER(sDroneControl.sRelshipGroupTimer)
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) <= 0
				SET_DRONE_RELATIONSHIP_GROUP(rgFM_AiLike)
				PRINTLN("[AM_MP_DRONE] - CLEAR_HATED_RELATIONSHIP  SET_DRONE_RELATIONSHIP_GROUP(rgFM_AiLike)")
			ENDIF	
		ENDIF
	ENDIF	
ENDPROC 

/// PURPOSE:
///    Check if we should send an event to remote player to start the drone script
///    bStartDroneScriptEarly - true if remote player should start the script before drone exists
FUNC BOOL SHOULD_REMOTE_PLAYER_START_DRONE_SCRIPT(PLAYER_INDEX playerToCheck #IF FEATURE_HEIST_ISLAND , BOOL bStartDroneScriptEarly #ENDIF )
	
	IF IS_PLAYER_IN_PROPERTY(playerToCheck, TRUE, TRUE)
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(playerToCheck)
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(playerToCheck)
		RETURN FALSE
	ENDIF
	
	VECTOR vRemotePlayerCoods = GET_PLAYER_COORDS(playerToCheck)
	
	#IF FEATURE_HEIST_ISLAND
	IF bStartDroneScriptEarly
		IF IS_DRONE_STARTED_FROM_SUBMARINE_MISSILE()
			IF IS_ENTITY_ALIVE(GET_SUBMARINE_INDEX_PED_IS_IN_INTERIOR())
				IF VDIST2(vRemotePlayerCoods, GET_ENTITY_COORDS(GET_SUBMARINE_INDEX_PED_IS_IN_INTERIOR())) < REMOTE_DRONE_START_LIMIT * REMOTE_DRONE_START_LIMIT 
					RETURN TRUE
				ENDIF
			ENDIF	
		ENDIF
	ELSE	
	#ENDIF
		IF VDIST2(vRemotePlayerCoods, sDroneControl.vDroneCoords) < REMOTE_DRONE_START_LIMIT * REMOTE_DRONE_START_LIMIT 
			RETURN TRUE
		ENDIF
	#IF FEATURE_HEIST_ISLAND
	ENDIF
	#ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Request remote players to start the script if they are in range of my drone
PROC MAINTAIN_REQUEST_DRONE_REMOTE_SCRIPT()
	
	PLAYER_INDEX remotePlayerIndex = INT_TO_PLAYERINDEX(sDroneControl.iPlayerDetectionStagger)
	IF PLAYER_ID() != remotePlayerIndex 
		IF IS_NET_PLAYER_OK(remotePlayerIndex)
			IF IS_ENTITY_ALIVE(GET_LOCAL_DRONE_PROP_OBJ_INDEX())
				IF IS_BIT_SET(sDroneControl.iDroneRemoteScriptBS , sDroneControl.iPlayerDetectionStagger)
					IF NOT NETWORK_IS_PLAYER_A_PARTICIPANT(remotePlayerIndex)
					AND NOT NETWORK_IS_PLAYER_A_PARTICIPANT_ON_SCRIPT(remotePlayerIndex, "AM_MP_DRONE", 0)
						CLEAR_BIT(sDroneControl.iDroneRemoteScriptBS , sDroneControl.iPlayerDetectionStagger)
						PRINTLN("[AM_MP_DRONE] - MAINTAIN_REQUEST_DRONE_REMOTE_SCRIPT ", GET_PLAYER_NAME(remotePlayerIndex), " is not participant")
					ENDIF
				ENDIF	
				
				IF SHOULD_REMOTE_PLAYER_START_DRONE_SCRIPT(remotePlayerIndex #IF FEATURE_HEIST_ISLAND, FALSE #ENDIF )
					IF NOT IS_BIT_SET(sDroneControl.iDroneRemoteScriptBS , sDroneControl.iPlayerDetectionStagger)
					AND CAN_REMOTE_PLAYER_START_REMOTE_DRONE_SCRIPT(remotePlayerIndex)
						BROADCAST_REMOTE_DRONE_SCRIPT(remotePlayerIndex, TRUE)
						SET_NETWORK_ID_ALWAYS_EXISTS_FOR_PLAYER(sDroneControl.cameraProp, remotePlayerIndex, TRUE)
						SET_BIT(sDroneControl.iDroneRemoteScriptBS , sDroneControl.iPlayerDetectionStagger)
						PRINTLN("[AM_MP_DRONE] - MAINTAIN_REQUEST_DRONE_REMOTE_SCRIPT  request to start for ", GET_PLAYER_NAME(remotePlayerIndex))
					ENDIF	
				ELIF VDIST2(GET_PLAYER_COORDS(remotePlayerIndex), sDroneControl.vDroneCoords) > REMOTE_DRONE_TERMINATE_LIMIT * REMOTE_DRONE_TERMINATE_LIMIT 
				#IF FEATURE_HEIST_ISLAND
				AND NOT IS_DRONE_STATE(DRONE_STATE_INIT)		// For submarine missile we move the it higher 
				AND NOT IS_DRONE_STATE(DRONE_STATE_LAUNCH_VFX)
				#ENDIF
					IF IS_BIT_SET(sDroneControl.iDroneRemoteScriptBS , sDroneControl.iPlayerDetectionStagger)
						BROADCAST_REMOTE_DRONE_SCRIPT(remotePlayerIndex, FALSE)
						SET_NETWORK_ID_ALWAYS_EXISTS_FOR_PLAYER(sDroneControl.cameraProp, remotePlayerIndex, FALSE)
						CLEAR_BIT(sDroneControl.iDroneRemoteScriptBS , sDroneControl.iPlayerDetectionStagger)
						CLEAR_BIT(sDroneControl.iDroneRemoteScriptStartedWithoutDronePropBS , sDroneControl.iPlayerDetectionStagger)
						PRINTLN("[AM_MP_DRONE] - MAINTAIN_REQUEST_DRONE_REMOTE_SCRIPT request to terminate for ", GET_PLAYER_NAME(remotePlayerIndex))
					ENDIF	
				ENDIF
			ELSE
				IF SHOULD_REMOTE_PLAYER_START_DRONE_SCRIPT(remotePlayerIndex #IF FEATURE_HEIST_ISLAND, TRUE #ENDIF )
				AND CAN_REMOTE_PLAYER_START_REMOTE_DRONE_SCRIPT(remotePlayerIndex)
					IF NOT IS_BIT_SET(sDroneControl.iDroneRemoteScriptStartedWithoutDronePropBS , sDroneControl.iPlayerDetectionStagger)
						BROADCAST_REMOTE_DRONE_SCRIPT(remotePlayerIndex, TRUE)
						SET_BIT(sDroneControl.iDroneRemoteScriptStartedWithoutDronePropBS , sDroneControl.iPlayerDetectionStagger)
						PRINTLN("[AM_MP_DRONE] - MAINTAIN_REQUEST_DRONE_REMOTE_SCRIPT request to start before creating drone for ", GET_PLAYER_NAME(remotePlayerIndex))
					ENDIF	
				ELSE
					IF IS_BIT_SET(sDroneControl.iDroneRemoteScriptStartedWithoutDronePropBS , sDroneControl.iPlayerDetectionStagger)
						BROADCAST_REMOTE_DRONE_SCRIPT(remotePlayerIndex, FALSE)
						CLEAR_BIT(sDroneControl.iDroneRemoteScriptStartedWithoutDronePropBS , sDroneControl.iPlayerDetectionStagger)
						CLEAR_BIT(sDroneControl.iDroneRemoteScriptBS , sDroneControl.iPlayerDetectionStagger)
						PRINTLN("[AM_MP_DRONE] - MAINTAIN_REQUEST_DRONE_REMOTE_SCRIPT request to terminate before creating drone for ", GET_PLAYER_NAME(remotePlayerIndex))
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF	
ENDPROC

PROC MAINTAIN_DRONE_EMP_PLAYER_DETECTION()
	IF IS_DRONE_STARTED_FROM_ARENA_WARS_ONE()
		PLAYER_INDEX playerIndex = INT_TO_PLAYERINDEX(sDroneControl.iPlayerDetectionStagger)
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sDroneControl.cameraProp)
		AND IS_NET_PLAYER_OK(playerIndex)
			IF IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(playerIndex))
				IF IS_ENTITY_IN_RANGE_ENTITY(NET_TO_OBJ(sDroneControl.cameraProp), GET_PLAYER_PED(playerIndex), g_sMPTunables.fAW_DRONE_EMP_RANGE)
					IF !GB_ARE_PLAYERS_IN_SAME_GANG(PLAYER_ID(), playerIndex)
						IF GET_PLAYER_TEAM(PLAYER_ID()) != GET_PLAYER_TEAM(playerIndex)
						OR GET_PLAYER_TEAM(PLAYER_ID()) = -1
							SET_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_AIMING_AT_PLAYER)
							EXIT
						ENDIF	
					ENDIF	
				ENDIF
			ENDIF	
		ENDIF
		
		CLEAR_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_AIMING_AT_PLAYER)
	ENDIF	
ENDPROC

/// PURPOSE:
///    Shapetest to detect if drone is aiming at ped
PROC PROCESS_DRONE_PED_DETECTION_SHAPETEST()
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sDroneControl.cameraProp)
	AND NOT IS_ENTITY_DEAD(NET_TO_OBJ(sDroneControl.cameraProp))
		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(sDroneControl.cameraProp)
			
			VECTOR vStartPoint = sDroneControl.vDroneCoords
			VECTOR vRocketOffset, vRot, vDir, vOffset, vTarget 					

			SWITCH sDroneControl.idronePedDetectShapeTest
		        CASE DRONE_SHAPETEST_INIT
		           
					// See if we can collide with anything peds
					vRocketOffset 	= GET_FINAL_RENDERED_CAM_COORD()
					vRot 			= GET_FINAL_RENDERED_CAM_ROT()
					vDir 			= <<-SIN(vRot.z) * COS(vRot.x), COS(vRot.z) * COS(vRot.x), SIN(vRot.x)>>
					vOffset 		= <<10.0, 10.0, 10.0>>
					
					IF IS_DRONE_STARTED_FROM_ARENA_WARS_ONE()
						vOffset 	= <<g_sMPTunables.fAW_DRONE_EMP_RANGE, g_sMPTunables.fAW_DRONE_EMP_RANGE, g_sMPTunables.fAW_DRONE_EMP_RANGE>>
					ENDIF	
					
					vTarget 		= vRocketOffset + (vDir*vOffset)
					
					vStartPoint = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(sDroneControl.vDroneCoords, GET_ENTITY_HEADING(NET_TO_OBJ(sDroneControl.cameraProp)), <<0.0, -0.1, 0.0>>)
					
					sDroneControl.dronePedDetectShapeTest = START_SHAPE_TEST_LOS_PROBE(vStartPoint, vTarget, SCRIPT_INCLUDE_PICKUP | SCRIPT_INCLUDE_RAGDOLL | SCRIPT_INCLUDE_MOVER | SCRIPT_INCLUDE_OBJECT | SCRIPT_INCLUDE_GLASS | SCRIPT_INCLUDE_VEHICLE, NET_TO_OBJ(sDroneControl.cameraProp))
					
					#IF IS_DEBUG_BUILD
					IF droneDebugStruct.bShowShapeTests	
						DRAW_DEBUG_SPHERE(vTarget, 0.1 , 0, 255, 0, 255)
						DRAW_DEBUG_SPHERE(vStartPoint, 0.1 , 255, 0, 0, 255)
					ENDIF
					#ENDIF
					
					IF NATIVE_TO_INT(sDroneControl.dronePedDetectShapeTest) != 0
						sDroneControl.idronePedDetectShapeTest 		= DRONE_SHAPETEST_PROCESS
						//PRINTLN("[AM_MP_DRONE] - PROCESS_DRONE_PED_DETECTION_SHAPETEST sDroneControl.idronePedDetectShapeTest INIT to PROCESS ")
					ENDIF	

		        BREAK
		        CASE DRONE_SHAPETEST_PROCESS
		        
		            INT iHitSomething
		            VECTOR vNormalAtPosHit, vDronePedDetectHitPos
		            ENTITY_INDEX entityHit

					sDroneControl.pedDetectTestResult = GET_SHAPE_TEST_RESULT(sDroneControl.dronePedDetectShapeTest, iHitSomething, vDronePedDetectHitPos, vNormalAtPosHit, entityHit)

					// result is ready
		            IF sDroneControl.pedDetectTestResult = SHAPETEST_STATUS_RESULTS_READY
		                #IF IS_DEBUG_BUILD
						//PRINTLN("[AM_MP_DRONE] - PROCESS_DRONE_PED_DETECTION_SHAPETEST - We got shapetest results!")
						#ENDIF
						
						IF iHitSomething = 0
							
							sDroneControl.iDronePedDetecHitSomthing = DRONE_CAM_AREA_CHECK_NOT_HIT_ANYTHING
							//PRINTLN("[AM_MP_DRONE] - PROCESS_DRONE_PED_DETECTION_SHAPETEST DRONE_CAM_AREA_CHECK_NOT_HIT_ANYTHING ")
							vDronePedDetectHitPos = <<0,0,0>> 
							
							IF IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_AIMING_AT_PLAYER)
							AND NOT IS_DRONE_STARTED_FROM_ARENA_WARS_ONE()
								CLEAR_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_AIMING_AT_PLAYER)
							ENDIF
							
							IF IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_AIMING_AT_PED)
								CLEAR_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_AIMING_AT_PED)
							ENDIF
							
							CLEAR_HATED_RELATIONSHIP()	
							
		                ELSE
							
		                    sDroneControl.iDronePedDetecHitSomthing = DRONE_CAM_AREA_CHECK_HIT_SOMETHING
							BOOL bSameGangMember
							
							IF DOES_ENTITY_EXIST(entityHit)
								IF IS_ENTITY_A_PED(entityHit)
									IF NOT IS_ENTITY_DEAD(entityHit)
										IF IS_PED_A_PLAYER(GET_PED_INDEX_FROM_ENTITY_INDEX(entityHit))
											IF GB_ARE_PLAYERS_IN_SAME_GANG(PLAYER_ID(), NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_PED_INDEX_FROM_ENTITY_INDEX(entityHit)))
												bSameGangMember = TRUE
											ENDIF
										ELSE
											IF NOT IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_AIMING_AT_PED)
												SET_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_AIMING_AT_PED)
												IF IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_AIMING_AT_PLAYER)
												AND NOT IS_DRONE_STARTED_FROM_ARENA_WARS_ONE()
													CLEAR_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_AIMING_AT_PLAYER)
												ENDIF
												PRINTLN("[AM_MP_DRONE] - PROCESS_DRONE_PED_DETECTION_SHAPETEST  DRONE_LOCAL_BS_AIMING_AT_PED TRUE ")
											ENDIF
											IF !GB_IS_PLAYER_PERMANENT_PARTICIPANT(PLAYER_ID())
											AND NOT FM_EVENT_IS_PLAYER_PERMANENT_PARTICIPANT_IN_BUSINESS_BATTLE(PLAYER_ID())
												IF IS_PED_A_COP(GET_PED_INDEX_FROM_ENTITY_INDEX(entityHit))
												OR IS_PED_IN_A_GANG(GET_PED_INDEX_FROM_ENTITY_INDEX(entityHit))
												OR IS_PED_A_PRISON_GUARD(GET_PED_INDEX_FROM_ENTITY_INDEX(entityHit))
													SET_DRONE_RELATIONSHIP_GROUP(rgFM_HateEveryOne)
													REINIT_NET_TIMER(sDroneControl.sRelshipGroupTimer)
													PRINTLN("[AM_MP_DRONE] - PROCESS_DRONE_PED_DETECTION_SHAPETEST  SET_DRONE_RELATIONSHIP_GROUP(rgFM_HateEveryOne) ")
												ELSE
													CLEAR_HATED_RELATIONSHIP()	
												ENDIF
											ENDIF	
										ENDIF
										IF !IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_AIMING_AT_PLAYER)
										AND NOT bSameGangMember
										AND IS_ENTITY_ALIVE(g_sDroneGlobals.DroneFakePed)
										AND HAS_ENTITY_CLEAR_LOS_TO_ENTITY(g_sDroneGlobals.DroneFakePed, entityHit, SCRIPT_INCLUDE_ALL)
										AND (IS_PED_A_PLAYER(GET_PED_INDEX_FROM_ENTITY_INDEX(entityHit)) AND PLAYER_PED_ID() != GET_PED_INDEX_FROM_ENTITY_INDEX(entityHit))
											SET_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_AIMING_AT_PLAYER)
											PRINTLN("[AM_MP_DRONE] - PROCESS_DRONE_PED_DETECTION_SHAPETEST  DRONE_LOCAL_BS_AIMING_AT_PLAYER TRUE ")
											IF IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_AIMING_AT_PED)
												CLEAR_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_AIMING_AT_PED)
											ENDIF
										ENDIF
									ENDIF
								ELSE
									IF NOT IS_DRONE_STARTED_FROM_ARENA_WARS_ONE()
										IF IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_AIMING_AT_PLAYER)
											CLEAR_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_AIMING_AT_PLAYER)
										ENDIF
									ENDIF	
									IF IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_AIMING_AT_PED)
										CLEAR_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_AIMING_AT_PED)
										PRINTLN("[AM_MP_DRONE] - PROCESS_DRONE_PED_DETECTION_SHAPETEST  DRONE_LOCAL_BS_AIMING_AT_PED FALSE ")
									ENDIF
									CLEAR_HATED_RELATIONSHIP()	
								ENDIF
							ELSE
								CLEAR_HATED_RELATIONSHIP()	
							ENDIF
							
							sDroneControl.dronePedDetectShapeTest = INT_TO_NATIVE(SHAPETEST_INDEX, 0)
							sDroneControl.idronePedDetectShapeTest 	= DRONE_SHAPETEST_INIT  
							
		                ENDIF
						 
					ELIF sDroneControl.pedDetectTestResult	= SHAPETEST_STATUS_NONEXISTENT
						sDroneControl.idronePedDetectShapeTest 	= DRONE_SHAPETEST_INIT    
		            ENDIF

		        BREAK
		    ENDSWITCH
		ELSE	
			NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(sDroneControl.cameraProp)
		ENDIF	
    ENDIF
ENDPROC

PROC PROCESS_DRONE_STUN_SHOT()
	IF IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_PLAYER_FIRED_STUN)
		IF sDroneControl.idronePedDetectShapeTest = DRONE_SHAPETEST_PROCESS
			IF sDroneControl.pedDetectTestResult = SHAPETEST_STATUS_RESULTS_READY
				VECTOR vRocketOffset 	= GET_FINAL_RENDERED_CAM_COORD()
				VECTOR vRot 			= GET_FINAL_RENDERED_CAM_ROT()
				VECTOR vDir 			= <<-SIN(vRot.z) * COS(vRot.x), COS(vRot.z) * COS(vRot.x), SIN(vRot.x)>>
				VECTOR vOffset 			= <<10.0, 10.0, 10.0>>
				VECTOR vTarget 			= vRocketOffset + (vDir*vOffset)
				INT iDamage				= g_sMPTunables.iBB_TERRORBYTE_DRONE_SHOCK_DAMAGE_PLAYERS  
				
				IF IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_AIMING_AT_PED)
					iDamage = g_sMPTunables.iBB_TERRORBYTE_DRONE_SHOCK_DAMAGE_PEDS
				ENDIF
				
				IF IS_DRONE_STARTED_FROM_CASINO_HEIST_MISSON()
					iDamage = 1
				ENDIF
				
				SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY_NEW(sDroneControl.vDroneCoords + <<0, 0, 0>>, vTarget, iDamage, TRUE, WEAPONTYPE_STUNGUN, 
																		PLAYER_PED_ID(), DEFAULT, DEFAULT, DEFAULT, NET_TO_OBJ(sDroneControl.cameraProp))
				sDroneControl.iStunRechargerFill = 100
				sDroneControl.iDetonateFill = 0
				SET_CONTROL_SHAKE(PLAYER_CONTROL, 300, ROUND(150 * sDroneControl.fControlShakeMultiplier))
				IF HAS_SOUND_FINISHED(sDroneControl.iFireShock)
					sDroneControl.iFireShock = GET_SOUND_ID()
					PLAY_SOUND_FROM_ENTITY(sDroneControl.iFireShock, "Shock_Fire", NET_TO_OBJ(sDroneControl.cameraProp), sDroneControl.sSoundSet, TRUE)
				ENDIF
				IF sDroneControl.iDetonateSoundID != -1
					STOP_SOUND(sDroneControl.iDetonateSoundID)
					RELEASE_SOUND_ID(sDroneControl.iDetonateSoundID)
					sDroneControl.iDetonateSoundID = -1
				ENDIF
				SET_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_CAMERA_STUN_GUN)
				START_NET_TIMER(sDroneControl.sStunGunTimer)
				CDEBUG1LN(DEBUG_NET_DRONE, "[AM_MP_DRONE] - PROCESS_DRONE_STUN_SHOT - shoot stun ")
			ELSE
				CDEBUG1LN(DEBUG_NET_DRONE, "[AM_MP_DRONE] - PROCESS_DRONE_STUN_SHOT - waiting on sDroneControl.pedDetectTestResult ")	
			ENDIF
		ELSE
			CDEBUG1LN(DEBUG_NET_DRONE, "[AM_MP_DRONE] - PROCESS_DRONE_STUN_SHOT - sDroneControl.idronePedDetectShapeTest: ", sDroneControl.idronePedDetectShapeTest)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_DRONE_EMP()
	IF IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_PLAYER_FIRED_STUN)
	AND NOT IS_BIT_SET(sDroneControl.iLocalBS,DRONE_LOCAL_BS_CAMERA_EMP)
	AND NOT sDroneControl.bDroneEMPCoolDown
		IF HAS_NAMED_PTFX_ASSET_LOADED("scr_xs_dr")
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sDroneControl.cameraProp)
				USE_PARTICLE_FX_ASSET("scr_xs_dr")
				START_NETWORKED_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_xs_dr_emp",NET_TO_OBJ(sDroneControl.cameraProp), <<0.0,-0.5,0>>, <<0,0,0>>, 3)
			ENDIF	
		ELSE
			PRINTLN("[AM_MP_DRONE] - PROCESS_DRONE_EMP HAS_NAMED_PTFX_ASSET_LOADED(scr_xs_dr) fl;se")
		ENDIF
		sDroneControl.iStunRechargerFill = 100
		SET_CONTROL_SHAKE(PLAYER_CONTROL, 300, ROUND(150 * sDroneControl.fControlShakeMultiplier))
		IF HAS_SOUND_FINISHED(sDroneControl.iFireShock)
			sDroneControl.iFireShock = GET_SOUND_ID()
			PLAY_SOUND_FROM_ENTITY(sDroneControl.iFireShock, "Shock_Fire", NET_TO_OBJ(sDroneControl.cameraProp), sDroneControl.sSoundSet, TRUE)
		ENDIF
		IF sDroneControl.iDetonateSoundID != -1
			STOP_SOUND(sDroneControl.iDetonateSoundID)
			RELEASE_SOUND_ID(sDroneControl.iDetonateSoundID)
			sDroneControl.iDetonateSoundID = -1
		ENDIF
		BROADCAST_DRONE_EMP(GET_ENTITY_COORDS(NET_TO_OBJ(sDroneControl.cameraProp)))
		SET_BIT(sDroneControl.iLocalBS,DRONE_LOCAL_BS_CAMERA_EMP)
		sDroneControl.bDroneEMPCoolDown = TRUE
		START_NET_TIMER(sDroneControl.sStunGunTimer)
		
		CDEBUG1LN(DEBUG_NET_DRONE, "[AM_MP_DRONE] - PROCESS_DRONE_EMP - shoot EMP")
	ENDIF
ENDPROC

/// PURPOSE:
///     Maintain drone detonate and detonate recharger bar
PROC MAINTAIN_DRONE_DETONATE()
	sDroneControl.sExplosion.control 	= FRONTEND_CONTROL
	
	IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
		sDroneControl.sExplosion.action = INPUT_CURSOR_CANCEL
	ELSE
		sDroneControl.sExplosion.action = INPUT_FRONTEND_LB
	ENDIF
	
	INT iDetonateTimer = g_sMPTunables.iBB_TERRORBYTE_DRONE_DETONATE_TIME
	
	IF !IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_DETONATE_RECHARGER_PAUSED)
		sDroneControl.iDetonateBarLimit	 = 100
	ENDIF					
	
	IF IS_DRONE_STARTED_FROM_ARENA_WARS_ONE()
		// Also update sDroneControl.iDetonatePausedTime for draining bar
		iDetonateTimer /= 2
		IF IS_BIT_SET(sDroneControl.iLocalBSTwo, DRONE_LOCAL_BS_TWO_DETONATE_DRAINING)
			sDroneControl.iDetonatePausedTime = (sDroneControl.iDetonatePausedFill * (g_sMPTunables.iBB_TERRORBYTE_DRONE_DETONATE_TIME/2)) / 100
			sDroneControl.iDetonatePausedTime = g_sMPTunables.iBB_TERRORBYTE_DRONE_DETONATE_TIME/2 - sDroneControl.iDetonatePausedTime
			CLEAR_BIT(sDroneControl.iLocalBSTwo, DRONE_LOCAL_BS_TWO_DETONATE_DRAINING)
		ENDIF	
		
		IF IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_DETONATE_RECHARGER_PAUSED)
			iDetonateTimer = sDroneControl.iDetonatePausedTime
		ENDIF	
	ELSE
		sDroneControl.iDetonateBarLimit	 = 100
	ENDIF
	
	IF NOT SHOULD_BLOCK_EXPLOSIVE()	
		IF NOT IS_DRONE_SNAPMATIC_ACTIVE()
		AND IS_CONTROL_HELD(sDroneControl.sExplosion, TRUE, iDetonateTimer)
			IF HAS_SOUND_FINISHED(sDroneControl.iDestroyedSound)
				sDroneControl.iDestroyedSound = GET_SOUND_ID()
				PLAY_SOUND_FROM_ENTITY(sDroneControl.iDestroyedSound, "Destroyed", NET_TO_OBJ(sDroneControl.cameraProp), sDroneControl.sSoundSet, TRUE)
			ENDIF
			THEFEED_SHOW()
			
			EXPLOSION_TAG explosiveType = EXP_TAG_GRENADE
			IF IS_DRONE_STARTED_FROM_ARENA_WARS_ONE()
				explosiveType = EXP_TAG_SCRIPT_DRONE
			ENDIF
			
			ADD_OWNED_EXPLOSION(PLAYER_PED_ID(), sDroneControl.vDroneCoords, explosiveType, 0.5)
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < 1
				SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
				SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())	
			ENDIF
			SET_CONTROL_SHAKE(PLAYER_CONTROL, 300, ROUND(200 * sDroneControl.fControlShakeMultiplier))
			SET_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_CAMERA_EXPLOSION)	
			CDEBUG1LN(DEBUG_NET_DRONE, "[AM_MP_DRONE] - PROCESS_DRONE_WEAPONS - explosion ")
		ENDIF
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(sDroneControl.sExplosion.sTimer)
		
		SET_CONTROL_SHAKE(PLAYER_CONTROL, 300, 20)	
		
		IF NOT IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_CHARGING_EXPLOSIVE)
			SET_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_CHARGING_EXPLOSIVE)
		ENDIF
		
		RESET_NET_TIMER(sDroneControl.sDetonateDrainingTimer)
		
		// Explosive recharger bar
		IF !HAS_NET_TIMER_EXPIRED(sDroneControl.sExplosion.sTimer, iDetonateTimer)
			INT iExplosiveTimerTwo = (sDroneControl.iDetonateBarLimit * ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), sDroneControl.sExplosion.sTimer.Timer)))
			IF !IS_DRONE_STARTED_FROM_ARENA_WARS_ONE()
				sDroneControl.iDetonateFill =  iExplosiveTimerTwo / iDetonateTimer
			ELSE
				IF !IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_DETONATE_RECHARGER_PAUSED)
					sDroneControl.iDetonateFill =  iExplosiveTimerTwo / iDetonateTimer	
				ELSE
					sDroneControl.iDetonateFill = sDroneControl.iDetonatePausedFill + (iExplosiveTimerTwo / iDetonateTimer)
				ENDIF
			ENDIF
			IF HAS_SOUND_FINISHED(sDroneControl.iDetonateSoundID)
				sDroneControl.iDetonateSoundID = GET_SOUND_ID()
				PLAY_SOUND_FRONTEND(sDroneControl.iDetonateSoundID, "HUD_Detonate_Charge", sDroneControl.sSoundSet)	
			ENDIF	
		ENDIF	
	ELSE
		IF IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_CHARGING_EXPLOSIVE)
			CLEAR_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_CHARGING_EXPLOSIVE)
			IF !IS_DRONE_STARTED_FROM_ARENA_WARS_ONE()
				sDroneControl.iDetonateFill = 0
			ELSE
				// Pause detonate time
				SET_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_DETONATE_RECHARGER_PAUSED)
				sDroneControl.iDetonateBarLimit = 100 - sDroneControl.iDetonateFill
				sDroneControl.iDetonatePausedTime = iDetonateTimer - ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), sDroneControl.sExplosion.sTimer.Timer))
				sDroneControl.iDetonatePausedFill = sDroneControl.iDetonateFill
				RESET_NET_TIMER(sDroneControl.sDetonateDrainingTimer)
			ENDIF
			IF sDroneControl.iDetonateSoundID != -1
				STOP_SOUND(sDroneControl.iDetonateSoundID)
				RELEASE_SOUND_ID(sDroneControl.iDetonateSoundID)
				sDroneControl.iDetonateSoundID = -1
			ENDIF
		ELSE
			// Detonate draining bar
			IF IS_DRONE_STARTED_FROM_ARENA_WARS_ONE()
			
				IF sDroneControl.iDetonateFill > 0								
					IF sDroneControl.iDetonateFill <= 2
						// Hack :)
						sDroneControl.iDetonateFill = 0
					ENDIF	
					
					IF !HAS_NET_TIMER_STARTED(sDroneControl.sDetonateDrainingTimer)
						START_NET_TIMER(sDroneControl.sDetonateDrainingTimer)
					ELSE
						INT iDrainingTime = (g_sMPTunables.iBB_TERRORBYTE_DRONE_DETONATE_TIME)
						IF !HAS_NET_TIMER_EXPIRED(sDroneControl.sDetonateDrainingTimer, iDrainingTime)
							FLOAT iTimeRemaining = ((100.0 - sDroneControl.iDetonateBarLimit) / TO_FLOAT((iDrainingTime / ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), sDroneControl.sDetonateDrainingTimer.Timer)))))
							sDroneControl.iDetonateFill = sDroneControl.iDetonateFill - ROUND(iTimeRemaining)
							sDroneControl.iDetonatePausedFill = sDroneControl.iDetonateFill
							sDroneControl.iDetonateBarLimit = 100 - sDroneControl.iDetonateFill
							SET_BIT(sDroneControl.iLocalBSTwo, DRONE_LOCAL_BS_TWO_DETONATE_DRAINING)
						ELSE
							sDroneControl.iDetonateFill = 0
						ENDIF
					ENDIF
				ELSE
					RESET_NET_TIMER(sDroneControl.sExplosion.sTimer)
					CLEAR_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_DETONATE_RECHARGER_PAUSED)
					CLEAR_BIT(sDroneControl.iLocalBSTwo, DRONE_LOCAL_BS_TWO_DETONATE_DRAINING)
				ENDIF
			ENDIF	
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_DRONE_TRANQUILLISER_GUN_COOL_DOWN()
	IF GET_DRONE_TRANQUILIZER_AMMO() > 0
		IF HAS_NET_TIMER_STARTED(sDroneControl.sTranqGunTimer)
			INT iDronePrimaryWeaponTimer = g_sMPTunables.iBB_TERRORBYTE_DRONE_SHOCK_COOLDOWN_TIME

			IF HAS_NET_TIMER_EXPIRED(sDroneControl.sTranqGunTimer, iDronePrimaryWeaponTimer)
				
				CLEAR_BIT(sDroneControl.iLocalBSTwo, DRONE_LOCAL_BS_TWO_START_TRANQ_GUN_TIMER)
				CLEAR_BIT(sDroneControl.iLocalBSTwo, DRONE_LOCAL_BS_TWO_PLAYER_FIRED_TRANQ_GUN)
				
				RESET_NET_TIMER(sDroneControl.sTranqGunTimer)
				sDroneControl.iTranqRechargerFill  	= 100

				IF HAS_SOUND_FINISHED(sDroneControl.iFireShock)
					STOP_SOUND(sDroneControl.iFireShock)
					RELEASE_SOUND_ID(sDroneControl.iFireShock)
					sDroneControl.iFireShock = -1
				ENDIF

				CDEBUG1LN(DEBUG_NET_DRONE, "[AM_MP_DRONE] - MAINTAIN_DRONE_TRANQUILLISER_GUN_COOL_DOWN - timer expired clear DRONE_LOCAL_BS_CAMERA_STUN_GUN")
			ELSE
				// Stun gun recharger bar
				INT iTimer = (100 * ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), sDroneControl.sTranqGunTimer.Timer)))
				sDroneControl.iTranqRechargerFill = iTimer / iDronePrimaryWeaponTimer
				
	//			IF HAS_SOUND_FINISHED(sDroneControl.iStunSoundID)
	//				sDroneControl.iStunSoundID = GET_SOUND_ID()
	//				PLAY_SOUND_FRONTEND(sDroneControl.iStunSoundID, "HUD_Shock_Recharge", sDroneControl.sSoundSet)
	//				SET_VARIABLE_ON_SOUND(sDroneControl.iStunSoundID, "Time", iDronePrimaryWeaponTimer/1000.0)
	//			ENDIF		
				//CDEBUG1LN(DEBUG_NET_DRONE, "[AM_MP_DRONE] - PROCESS_DRONE_WEAPONS - sDroneControl.iTranqRechargerFill: ", sDroneControl.iTranqRechargerFill , "  Time: ", ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), sDroneControl.sTranqGunTimer.Timer)) )
			ENDIF
		ENDIF
	ENDIF	
ENDPROC

PROC MAINTAIN_DRONE_TRANQUILLISER_GUN()
	IF SHOULD_DRONE_HAVE_TRANQUILLISER_GUN_EQUIPPED()
		CONTROL_ACTION tranqGunControl
		IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
			tranqGunControl = INPUT_CURSOR_CANCEL
		ELSE
			tranqGunControl = INPUT_FRONTEND_LB
		ENDIF
		IF !IS_BIT_SET(sDroneControl.iLocalBSTwo, DRONE_LOCAL_BS_TWO_START_TRANQ_GUN_TIMER)
			IF !IS_BIT_SET(sDroneControl.iLocalBSTwo, DRONE_LOCAL_BS_TWO_PLAYER_FIRED_TRANQ_GUN)
				IF (IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, tranqGunControl)
				OR IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, tranqGunControl))
				
					SET_BIT(sDroneControl.iLocalBSTwo, DRONE_LOCAL_BS_TWO_PLAYER_FIRED_TRANQ_GUN)
				ENDIF
			ELSE
				IF sDroneControl.idronePedDetectShapeTest = DRONE_SHAPETEST_PROCESS
					IF sDroneControl.pedDetectTestResult = SHAPETEST_STATUS_RESULTS_READY
						VECTOR vRocketOffset 	= GET_FINAL_RENDERED_CAM_COORD()
						VECTOR vRot 			= GET_FINAL_RENDERED_CAM_ROT()
						VECTOR vDir 			= <<-SIN(vRot.z) * COS(vRot.x), COS(vRot.z) * COS(vRot.x), SIN(vRot.x)>>
						VECTOR vOffset 			= <<10.0, 10.0, 10.0>>
						VECTOR vTarget 			= vRocketOffset + (vDir*vOffset)
						INT iDamage				= g_sMPTunables.iBB_TERRORBYTE_DRONE_SHOCK_DAMAGE_PLAYERS  
						
						IF IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_AIMING_AT_PED)
							iDamage = g_sMPTunables.iBB_TERRORBYTE_DRONE_SHOCK_DAMAGE_PEDS
						ENDIF
						
						SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY_NEW(sDroneControl.vDroneCoords + <<0, 0, 0>>, vTarget, iDamage, TRUE, WEAPONTYPE_DLC_TRANQUILIZER, 
																				PLAYER_PED_ID(), DEFAULT, DEFAULT, DEFAULT, NET_TO_OBJ(sDroneControl.cameraProp))
						sDroneControl.iTranqRechargerFill = 100
						
						SET_CONTROL_SHAKE(PLAYER_CONTROL, 300, ROUND(150 * sDroneControl.fControlShakeMultiplier))
						
						IF HAS_SOUND_FINISHED(sDroneControl.iFireTranq)
							sDroneControl.iFireTranq = GET_SOUND_ID()
							PLAY_SOUND_FROM_ENTITY(sDroneControl.iFireTranq, "Remote_Perspective_Fire", NET_TO_OBJ(sDroneControl.cameraProp), "DLC_H3_Drone_Tranq_Weapon_Sounds", TRUE)
						ENDIF
						
						PLAY_SOUND_FRONTEND(-1, "Pilot_Perspective_Fire", "DLC_H3_Drone_Tranq_Weapon_Sounds")
						
						IF IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_AIMING_AT_PLAYER)
						OR IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_AIMING_AT_PED)
							sDroneControl.iDroneTotalTranq++
						ENDIF
						
						SET_DRONE_TRANQUILIZER_AMMO(GET_DRONE_TRANQUILIZER_AMMO() - 1)
						
						IF GET_DRONE_TRANQUILIZER_AMMO() <= 0
							sDroneControl.iTranqRechargerFill  = 0
							CLEAR_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_START_INSTRUCTIONAL_PROMPT)
						ENDIF
						
						SET_BIT(sDroneControl.iLocalBSTwo, DRONE_LOCAL_BS_TWO_START_TRANQ_GUN_TIMER)
						START_NET_TIMER(sDroneControl.sTranqGunTimer)
						CDEBUG1LN(DEBUG_NET_DRONE, "[AM_MP_DRONE] - MAINTAIN_DRONE_TRANQUILLISER_GUN - shoot stun ")
					ELSE
						CDEBUG1LN(DEBUG_NET_DRONE, "[AM_MP_DRONE] - MAINTAIN_DRONE_TRANQUILLISER_GUN - waiting on sDroneControl.pedDetectTestResult ")	
					ENDIF
				ELSE
					CDEBUG1LN(DEBUG_NET_DRONE, "[AM_MP_DRONE] - MAINTAIN_DRONE_TRANQUILLISER_GUN - sDroneControl.idronePedDetectShapeTest: ", sDroneControl.idronePedDetectShapeTest)
				ENDIF
			ENDIF	
		ELSE
			MAINTAIN_DRONE_TRANQUILLISER_GUN_COOL_DOWN()
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_DRONE_WEAPONS()
	
	IF IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_CAMERA_FUZZ)
	OR SHOULD_CLEANUP_DRONE_CAMERA_INSTANTLY()
	OR IS_DRONE_WEAPONS_DISABLED()
		sDroneControl.iStunRechargerFill  	= 0
		sDroneControl.iDetonateFill 		= 0	
		IF NOT IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_RESET_STUN_CHARGER)
			SET_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_RESET_STUN_CHARGER)
		ENDIF
		EXIT
	ENDIF
	
	IF NOT IS_DRONE_WEAPONS_DISABLED()
		IF IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_RESET_STUN_CHARGER)
			sDroneControl.iStunRechargerFill  = 100
			CLEAR_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_RESET_STUN_CHARGER)
		ENDIF
	ENDIF
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sDroneControl.cameraProp)
	AND NOT IS_ENTITY_DEAD(NET_TO_OBJ(sDroneControl.cameraProp))
		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(sDroneControl.cameraProp)
			IF DOES_CAM_EXIST(sDroneControl.droneCamera)
			AND IS_CAM_RENDERING(sDroneControl.droneCamera)
			AND NOT IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_CAMERA_EXPLOSION)	

				IF NOT IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_CAMERA_STUN_GUN)
				AND NOT IS_BIT_SET(sDroneControl.iLocalBS,DRONE_LOCAL_BS_CAMERA_EMP)
				AND NOT sDroneControl.bDroneEMPCoolDown
					// Stun Gun or EMP
					CONTROL_ACTION stunGunControl 		= INPUT_FRONTEND_RB
					IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
						stunGunControl 					= INPUT_CURSOR_ACCEPT
					ENDIF
					IF (IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, stunGunControl)
					OR IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, stunGunControl))
					AND NOT IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_CHARGING_EXPLOSIVE)
					AND NOT IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_PLAYER_FIRED_STUN)
					AND NOT IS_DRONE_SNAPMATIC_ACTIVE()
						SET_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_PLAYER_FIRED_STUN)
						IF IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_AIMING_AT_PLAYER)
						OR IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_AIMING_AT_PED)
							sDroneControl.iDroneTotalTases++
						ENDIF	
						CDEBUG1LN(DEBUG_NET_DRONE, "[AM_MP_DRONE] - PROCESS_DRONE_WEAPONS - DRONE_LOCAL_BS_PLAYER_FIRED_STUN TRUE sDroneControl.iDroneTotalTases: ", sDroneControl.iDroneTotalTases)
					ENDIF
					
					IF IS_DRONE_STARTED_FROM_ARENA_WARS_ONE()
						PROCESS_DRONE_EMP()	
					ELSE
						PROCESS_DRONE_STUN_SHOT()
					ENDIF
				ELSE
					IF HAS_NET_TIMER_STARTED(sDroneControl.sStunGunTimer)
						INT iDronePrimaryWeaponTimer
						
						IF IS_DRONE_STARTED_FROM_ARENA_WARS_ONE()
							iDronePrimaryWeaponTimer = g_sMPTunables.iAW_DRONE_EMP_COOLDOWN_TIME
						ELSE
							iDronePrimaryWeaponTimer = g_sMPTunables.iBB_TERRORBYTE_DRONE_SHOCK_COOLDOWN_TIME 
						ENDIF
						
						IF HAS_NET_TIMER_EXPIRED(sDroneControl.sStunGunTimer, iDronePrimaryWeaponTimer)
							
							RESET_NET_TIMER(sDroneControl.sStunGunTimer)
							sDroneControl.iStunRechargerFill  	= 100
							IF sDroneControl.iStunSoundID != -1
								STOP_SOUND(sDroneControl.iStunSoundID)
								RELEASE_SOUND_ID(sDroneControl.iStunSoundID)
								sDroneControl.iStunSoundID = -1
							ENDIF	
							
							IF HAS_SOUND_FINISHED(sDroneControl.iFireShock)
								STOP_SOUND(sDroneControl.iFireShock)
								RELEASE_SOUND_ID(sDroneControl.iFireShock)
								sDroneControl.iFireShock = -1
							ENDIF
							
							CLEAR_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_CAMERA_STUN_GUN)
							CLEAR_BIT(sDroneControl.iLocalBS,DRONE_LOCAL_BS_CAMERA_EMP)	
							
							CLEAR_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_PLAYER_FIRED_STUN)
							sDroneControl.bDroneEMPCoolDown = FALSE
							CDEBUG1LN(DEBUG_NET_DRONE, "[AM_MP_DRONE] - PROCESS_DRONE_WEAPONS - timer expired clear DRONE_LOCAL_BS_CAMERA_STUN_GUN")
						ELSE
							// Stun gun recharger bar
							INT iTimer = (100 * ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), sDroneControl.sStunGunTimer.Timer)))
							sDroneControl.iStunRechargerFill = iTimer / iDronePrimaryWeaponTimer
							IF NOT IS_DRONE_STARTED_FROM_ARENA_WARS_ONE()
								IF HAS_SOUND_FINISHED(sDroneControl.iStunSoundID)
									sDroneControl.iStunSoundID = GET_SOUND_ID()
									PLAY_SOUND_FRONTEND(sDroneControl.iStunSoundID, "HUD_Shock_Recharge", sDroneControl.sSoundSet)
									SET_VARIABLE_ON_SOUND(sDroneControl.iStunSoundID, "Time", iDronePrimaryWeaponTimer/1000.0)
								ENDIF		
							ENDIF	
							//CDEBUG1LN(DEBUG_NET_DRONE, "[AM_MP_DRONE] - PROCESS_DRONE_WEAPONS - sDroneControl.iStunRechargerFill: ", sDroneControl.iStunRechargerFill , "  Time: ", ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), sDroneControl.sStunGunTimer.Timer)) )
						ENDIF
					ENDIF
				ENDIF	
				
				// Explosive 
				MAINTAIN_DRONE_DETONATE()
				MAINTAIN_DRONE_TRANQUILLISER_GUN()
			ENDIF	
		ELSE
			NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(sDroneControl.cameraProp)
		ENDIF	
	ENDIF	
ENDPROC 

PROC PROCESS_LOCAL_DRONE_EMP()
	
	IF NOT IS_BIT_SET(MPGlobalsAmbience.iNGAmbBitSet, iNGABI_PROCESS_RANDOM_PED_VEHICLE_EMP)
	AND DOES_ENTITY_EXIST(sDroneControl.fakePed)
		PED_INDEX peds[15]
		INT iPeds = GET_PED_NEARBY_PEDS(sDroneControl.fakePed, peds)
		INT i 
		
		REPEAT iPeds i
			IF DOES_ENTITY_EXIST(peds[i]) AND NOT IS_ENTITY_DEAD(peds[i])
				IF NETWORK_HAS_CONTROL_OF_ENTITY(peds[i])
					IF IS_PED_IN_ANY_VEHICLE(peds[i])
					AND GET_SEAT_PED_IS_IN(peds[i]) = VS_DRIVER
						INT iRandom = GET_RANDOM_INT_IN_RANGE(0, 3)
						VEHICLE_INDEX pedVeh = GET_VEHICLE_PED_IS_IN(peds[i])
						IF GET_ENTITY_SPEED(pedVeh) > 0.5
							SET_VEHICLE_ENGINE_ON(pedVeh, FALSE, TRUE)	
							SWITCH iRandom
								CASE 0
									CLEAR_PED_TASKS(peds[i])
									TASK_VEHICLE_TEMP_ACTION(peds[i], pedVeh, TEMPACT_HANDBRAKETURNLEFT, 15000)
									CDEBUG1LN(DEBUG_NET_DRONE, "[AM_MP_DRONE] - MAINTAIN_DRONE_EMP - TASK_VEHICLE_TEMP_ACTION Random Ped TEMPACT_HANDBRAKETURNLEFT")
								BREAK	
								CASE 1
									CLEAR_PED_TASKS(peds[i])
									TASK_VEHICLE_TEMP_ACTION(peds[i], pedVeh, TEMPACT_HANDBRAKETURNRIGHT, 15000)
									CDEBUG1LN(DEBUG_NET_DRONE, "[AM_MP_DRONE] - MAINTAIN_DRONE_EMP - TASK_VEHICLE_TEMP_ACTION Random Ped TEMPACT_HANDBRAKETURNRIGHT")
								BREAK
								CASE 2
									CLEAR_PED_TASKS(peds[i])
									TASK_VEHICLE_TEMP_ACTION(peds[i], pedVeh, TEMPACT_HANDBRAKESTRAIGHT_INTELLIGENT, 15000)
									CDEBUG1LN(DEBUG_NET_DRONE, "[AM_MP_DRONE] - MAINTAIN_DRONE_EMP - TASK_VEHICLE_TEMP_ACTION Random Ped TEMPACT_HANDBRAKETURNRIGHT")
								BREAK
							ENDSWITCH
						ENDIF	
					ENDIF	
				ENDIF
			ENDIF
		ENDREPEAT
		
		SET_BIT(MPGlobalsAmbience.iNGAmbBitSet, iNGABI_PROCESS_RANDOM_PED_VEHICLE_EMP)
		CDEBUG1LN(DEBUG_NET_DRONE, "[AM_MP_DRONE] - PROCESS_LOCAL_DRONE_EMP - iNGABI_PROCESS_RANDOM_PED_VEHICLE_EMP TRUE")
	ENDIF

ENDPROC

FUNC BOOL IS_ANY_PARTICIPANT_FLYING_DRONE()
	INT iNumParticipant
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iNumParticipant
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iNumParticipant))
			IF IS_BIT_SET(playerBD[iNumParticipant].iLocalBS, DRONE_PLAYER_BD_URING_DRONE)
				RETURN TRUE
			ENDIF
		ENDIF	
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_KEEP_REMOTE_DRONE_SCRIPT_WITHOUT_ANY_FLYING_PARTICIPANT()
	#IF FEATURE_HEIST_ISLAND
	IF g_sDroneGlobals.remoteDroneOwner != INVALID_PLAYER_INDEX()
		IF GlobalPlayerBD[NATIVE_TO_INT(g_sDroneGlobals.remoteDroneOwner)].SimpleInteriorBD.propertyOwner != INVALID_PLAYER_INDEX() 
			IF IS_ENTITY_ALIVE(g_vehSubmarine[NATIVE_TO_INT(GlobalPlayerBD[NATIVE_TO_INT(g_sDroneGlobals.remoteDroneOwner)].SimpleInteriorBD.propertyOwner)])
				IF IS_NET_PLAYER_OK(PLAYER_ID())
					IF VDIST2(GET_PLAYER_COORDS(PLAYER_ID()), GET_ENTITY_COORDS(g_vehSubmarine[NATIVE_TO_INT(GlobalPlayerBD[NATIVE_TO_INT(g_sDroneGlobals.remoteDroneOwner)].SimpleInteriorBD.propertyOwner)])) < REMOTE_DRONE_START_LIMIT * REMOTE_DRONE_START_LIMIT 
						RETURN TRUE
					ENDIF
				ENDIF	
			ENDIF	
		ENDIF	
	ENDIF
	#ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_KILL_THIS_SCRIPT()
	IF SHOULD_KILL_DRONE_SCRIPT()
		PRINTLN("[AM_MP_DRONE] - SHOULD_KILL_THIS_SCRIPT - SHOULD_KILL_DRONE_SCRIPT TRUE")
		RETURN TRUE
	ENDIF
	
	IF NOT IS_SKYSWOOP_AT_GROUND()
		PRINTLN("[AM_MP_DRONE] - SHOULD_KILL_THIS_SCRIPT - IS_SKYSWOOP_AT_GROUND is false - TRUE")
		RETURN TRUE
	ENDIF
	
	IF PLAYER_ID() != INVALID_PLAYER_INDEX()
		IF NOT IS_REMOTE_DRONE_STARTED()
			IF IS_PLAYER_SPECTATING(PLAYER_ID())
			AND NOT IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
				PRINTLN("[AM_MP_DRONE] - SHOULD_KILL_THIS_SCRIPT - IS_PLAYER_SPECTATING is true - TRUE")
				RETURN TRUE
			ENDIF
		ENDIF	
	ENDIF
	
	IF IS_PLAYER_WALKING_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
		PRINTLN("[AM_MP_DRONE] - SHOULD_KILL_THIS_SCRIPT - IS_PLAYER_WALKING_OUT_OF_SIMPLE_INTERIOR is true - TRUE")
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(PLAYER_ID())
		PRINTLN("[AM_MP_DRONE] - SHOULD_KILL_THIS_SCRIPT - IS_PLAYER_ENTERING_OR_EXITING_PROPERTY is true - TRUE")
		RETURN TRUE
	ENDIF
	
	IF IS_REMOTE_DRONE_STARTED()
		IF SHOULD_TERMINATE_REMOTE_DRONE_SCRIPT()
			PRINTLN("[AM_MP_DRONE] - SHOULD_KILL_THIS_SCRIPT - SHOULD_TERMINATE_REMOTE_DRONE_SCRIPT is true - TRUE")
			RETURN TRUE
		ENDIF
		
		IF IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE, TRUE)
			PRINTLN("[AM_MP_DRONE] - SHOULD_KILL_THIS_SCRIPT - remote drone IS_PLAYER_IN_PROPERTY is true - TRUE")
			RETURN TRUE
		ENDIF
		
		IF IS_LOCAL_PLAYER_IN_ARENA_SPECTATOR_BOX()
			PRINTLN("[AM_MP_DRONE] - SHOULD_KILL_THIS_SCRIPT - remote drone IS_LOCAL_PLAYER_IN_ARENA_SPECTATOR_BOX is true - TRUE")
			RETURN TRUE
		ENDIF
		
		IF NOT IS_ANY_PARTICIPANT_FLYING_DRONE()
		AND NOT SHOULD_KEEP_REMOTE_DRONE_SCRIPT_WITHOUT_ANY_FLYING_PARTICIPANT()
			IF HAS_NET_TIMER_EXPIRED(sDroneControl.sParticipantTimer, 10000)
				PRINTLN("[AM_MP_DRONE] - SHOULD_KILL_THIS_SCRIPT - NETWORK_GET_NUM_PARTICIPANTS is 1 - TRUE")
				RETURN TRUE
			ENDIF	
		ELSE
			RESET_NET_TIMER(sDroneControl.sParticipantTimer)
		ENDIF	 
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_BLOCK_DRONE()
	
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1420.915039,-3009.132324,-79.999939>>, <<-1421.980347,-3009.121338,-77.749939>>, 1.000000)
		PRINTLN("[AM_MP_DRONE] - SHOULD_BLOCK_DRONE - player is in hacker truck exit area - TRUE")
		RETURN TRUE
	ENDIF
	
	IF IS_CUSTOM_MENU_ON_SCREEN()
		PRINTLN("[AM_MP_DRONE] - SHOULD_BLOCK_DRONE - IS_CUSTOM_MENU_ON_SCREEN is true - TRUE")
		RETURN TRUE
	ENDIF
	
	IF IS_INTERACTION_MENU_OPEN()
		PRINTLN("[AM_MP_DRONE] - SHOULD_BLOCK_DRONE - IS_INTERACTION_MENU_OPEN is true - TRUE")
		RETURN TRUE
	ENDIF
	
	IF IS_PAUSE_MENU_ACTIVE()
		PRINTLN("[AM_MP_DRONE] - SHOULD_BLOCK_DRONE - IS_PAUSE_MENU_ACTIVE is true - TRUE")
		RETURN TRUE
	ENDIF
	
	IF IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
		PRINTLN("[AM_MP_DRONE] - SHOULD_BLOCK_DRONE - IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR is true - TRUE")
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_IN_HACKER_TRUCK(PLAYER_ID())
		IF IS_OWNERS_HACKER_TRUCK_INSIDE_BUSINESS_HUB(g_OwnerOfHackerTruckPropertyIAmIn)
			PRINTLN("[AM_MP_DRONE] - SHOULD_BLOCK_DRONE - IS_OWNERS_HACKER_TRUCK_INSIDE_BUSINESS_HUB is true - TRUE")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_DRONE_STARTED_FROM_TRUCK()
		IF SHOULD_CHECK_FOR_DRONE_COOL_DOWN()
			IF IS_OWNERS_DRONE_IN_COOL_DOWN()
			#IF IS_DEBUG_BUILD
			AND NOT droneDebugStruct.byPassAllCoolDowns
			#ENDIF
				PRINTLN("[AM_MP_DRONE] - SHOULD_BLOCK_DRONE - drone is in cool down - TRUE")
				RETURN TRUE
			ENDIF
			IF IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_DRONE_COOL_DOWN_BROADCAST_WAIT)
				PRINTLN("[AM_MP_DRONE] - SHOULD_BLOCK_DRONE - drone is in waiting on cool down broadcast - TRUE")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF	
	
	#IF FEATURE_HEIST_ISLAND
	IF IS_DRONE_STARTED_FROM_SUBMARINE_MISSILE()
		IF ((IS_OWNERS_SUBMARINE_GUIDED_MISSILE_ONE_IN_COOL_DOWN() AND g_iShouldLaunchSubmarineSeat = 1)
		OR (IS_OWNERS_SUBMARINE_GUIDED_MISSILE_TWO_IN_COOL_DOWN() AND g_iShouldLaunchSubmarineSeat = 2))
		#IF IS_DEBUG_BUILD
		AND NOT droneDebugStruct.byPassAllCoolDowns
		#ENDIF
			PRINTLN("[AM_MP_DRONE] - SHOULD_BLOCK_DRONE - submarine missile is in cool down - TRUE")
			RETURN TRUE
		ENDIF
		IF IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_DRONE_COOL_DOWN_BROADCAST_WAIT)
			PRINTLN("[AM_MP_DRONE] - SHOULD_BLOCK_DRONE - submarine missile is in waiting on cool down broadcast - TRUE")
			RETURN TRUE
		ENDIF
		IF NOT IS_ENTITY_ALIVE(GET_SUBMARINE_INDEX_PED_IS_IN_INTERIOR())
			PRINTLN("[AM_MP_DRONE] - SHOULD_BLOCK_DRONE - submarine doesn't exist or dead")
			RETURN TRUE
		ENDIF
		
		VECTOR vSubCoords = GET_ENTITY_COORDS(GET_SUBMARINE_INDEX_PED_IS_IN_INTERIOR())
		IF vSubCoords.z <= SUBMARINE_Z_UNDER_WATER
			PRINTLN("[AM_MP_DRONE] - SHOULD_BLOCK_DRONE - submarine in deep water")
			RETURN TRUE
		ENDIF
		
		PED_INDEX subDriver = GET_PED_IN_VEHICLE_SEAT(GET_SUBMARINE_INDEX_PED_IS_IN_INTERIOR())
		IF IS_ENTITY_ALIVE(subDriver)
			IF IS_PED_A_PLAYER(subDriver)
				IF IS_PLAYER_IN_PASSIVE_MODE(NETWORK_GET_PLAYER_INDEX_FROM_PED(subDriver))
					PRINTLN("[AM_MP_DRONE] - SHOULD_BLOCK_DRONE - submarine driver is in passive mode")
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	#ENDIF
	
	IF IS_PLAYER_IN_PASSIVE_MODE(PLAYER_ID())
		PRINTLN("[AM_MP_DRONE] - SHOULD_BLOCK_DRONE - player in passive mode - TRUE")
		RETURN TRUE
	ENDIF
	
	IF IS_PHONE_ONSCREEN()
		PRINTLN("[AM_MP_DRONE] - SHOULD_BLOCK_DRONE - IS_PHONE_ONSCREEN - TRUE")
		RETURN TRUE
	ENDIF	
	
	IF IS_CELLPHONE_CAMERA_IN_USE()
		PRINTLN("[AM_MP_DRONE] - SHOULD_BLOCK_DRONE - IS_CELLPHONE_CAMERA_IN_USE - TRUE")
		RETURN TRUE
	ENDIF
	
	IF IS_DRONE_STARTED_FROM_TRUCK()
		IF SHOULD_START_DRONE_WITHOUT_SEAT()
			IF IS_NET_PLAYER_OK(PLAYER_ID())
				IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), <<-1422.1885, -3015.9255, -80.1554>>) < 1.5
					PRINTLN("[AM_MP_DRONE] - SHOULD_BLOCK_DRONE - too close to weapon station - TRUE")
					RETURN TRUE
				ENDIF
				
				IF IS_PLAYER_IN_HACKER_TRUCK_COMPUTER_LOCATE()
					PRINTLN("[AM_MP_DRONE] - SHOULD_BLOCK_DRONE - too close to touch screen - TRUE")
					RETURN TRUE
				ENDIF
			ENDIF	
			
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
			OR IS_PED_TRYING_TO_ENTER_OR_EXIT_VEHICLE(PLAYER_PED_ID())
				PRINTLN("[AM_MP_DRONE] - SHOULD_BLOCK_DRONE - player is vehicle or entering - TRUE")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF	
	
	IF IS_PLAYER_CHANGING_CLOTHES()
		PRINTLN("[AM_MP_DRONE] - SHOULD_BLOCK_DRONE - player is changing clothes - TRUE")
		RETURN TRUE
	ENDIF
	
	IF g_sMPTunables.bBB_TERRORBYTE_DISABLE_DISABLE_DRONE_STATION   
		PRINTLN("[AM_MP_DRONE] - SHOULD_BLOCK_DRONE - bBB_TERRORBYTE_DISABLE_DISABLE_DRONE_STATION - TRUE")
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_DRONE_COOL_DOWN_BROADCAST_FAILED)
		PRINTLN("[AM_MP_DRONE] - SHOULD_BLOCK_DRONE - DRONE_LOCAL_BS_DRONE_COOL_DOWN_BROADCAST_FAILED - TRUE")
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_IN_HACKER_TRUCK(PLAYER_ID())
		IF IS_ENTITY_ALIVE(GET_HACKER_TRUCK_INDEX_PED_IS_IN_INTERIOR())
			IF IS_ENTITY_IN_ANGLED_AREA(GET_HACKER_TRUCK_INDEX_PED_IS_IN_INTERIOR(), <<-136.161423,4617.175781,124.513405>>, <<-490.305756,4924.749023,159.067673>>, 19.500000)
			OR IS_ENTITY_IN_ANGLED_AREA(GET_HACKER_TRUCK_INDEX_PED_IS_IN_INTERIOR(), <<908.465027,34.857258,79.354294>>, <<926.413940,64.377426,87.992180>>, 19.000000)
			OR IS_ENTITY_IN_ANGLED_AREA(GET_HACKER_TRUCK_INDEX_PED_IS_IN_INTERIOR(), <<946.893677,-6.119501,77.907738>>, <<962.069824,-5.454865,88.655807>>, 10.750000)
				PRINTLN("[AM_MP_DRONE] - SHOULD_BLOCK_DRONE - truck is in tunnel")
				RETURN TRUE
			ENDIF
		ELSE
			PRINTLN("[AM_MP_DRONE] - SHOULD_BLOCK_DRONE - truck doesn't exist")
			RETURN TRUE
		ENDIF
	ENDIF	
	
	IF g_bBustedWarpInProgress
		PRINTLN("[AM_MP_DRONE] SHOULD_BLOCK_DRONE - g_bBustedWarpInProgress true - TRUE")
		RETURN TRUE
	ENDIF
	
	IF g_bRequestingLiveStream
		PRINTLN("[AM_MP_DRONE] SHOULD_BLOCK_DRONE - g_bRequestingLiveStream true - TRUE")
		RETURN TRUE
	ENDIF
	
	IF g_bUsingCCTV
		PRINTLN("[AM_MP_DRONE] SHOULD_BLOCK_DRONE - g_bUsingCCTV true - TRUE")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_DISPLAY_HELP_WITHOUT_SEAT()
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		PRINTLN("[AM_MP_DRONE] - SHOULD_DISPLAY_HELP_WITHOUT_SEAT - player is in vehicle FALSE")
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_DRONE_SEAT_IN_HACKER_TRUCK()
	OR IS_PLAYER_IN_SEAT_LOCATE()
		PRINTLN("[AM_MP_DRONE] - SHOULD_DISPLAY_HELP_WITHOUT_SEAT - IS_PLAYER_IN_DRONE_SEAT_IN_HACKER_TRUCK OR IS_PLAYER_IN_SEAT_LOCATE return FALSE")
		RETURN FALSE
	ENDIF
	
	IF SHOULD_BY_PASS_DRONE_SEAT_CHECK()
		PRINTLN("[AM_MP_DRONE] - SHOULD_DISPLAY_HELP_WITHOUT_SEAT - SHOULD_BY_PASS_DRONE_SEAT_CHECK return FALSE")
		RETURN FALSE
	ENDIF	
	
	RETURN TRUE
ENDFUNC

PROC MAINTAIN_HACKER_TRUCK_DRONE_TRIGGER()
	IF NOT SHOULD_BLOCK_DRONE()
		IF IS_PLAYER_IN_DRONE_SEAT_IN_HACKER_TRUCK()
			IF SHOULD_DISPLAY_HELP_WITHOUT_SEAT()
				IF !IS_HELP_MESSAGE_BEING_DISPLAYED()
				AND NOT IS_HELP_MESSAGE_ON_SCREEN()
					PRINT_HELP_FOREVER("DRONE_TRIG")
				ENDIF	
			ELSE
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DRONE_TRIG")
					CLEAR_HELP()
				ENDIF
			ENDIF
			IF IS_PLAYER_IN_DRONE_SEAT_IN_HACKER_TRUCK()
			OR SHOULD_BY_PASS_DRONE_SEAT_CHECK()
				IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_CELLPHONE_SELECT)
				OR IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_CELLPHONE_SELECT)
					SET_PLAYER_INITIALISING_DRONE(TRUE)
					SET_DRONE_STATE(DRONE_STATE_INIT)
					CLEAR_HELP()
				ENDIF
			ELSE
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DRONE_TRIG")
					IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_CONTEXT)
					OR IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_CONTEXT)
					OR IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						sDroneControl.bDroneEMPCoolDown = FALSE
						SET_PLAYER_INITIALISING_DRONE(TRUE)
						SET_DRONE_STATE(DRONE_STATE_INIT)
						CLEAR_HELP()
					ENDIF
				ENDIF
			ENDIF
		ELIF SHOULD_START_DRONE_WITHOUT_SEAT()
		AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(),<<-1420.684326,-3010.616943,-77.999939>> , <<-1423.142090,-3012.295410,-77.749939>> ,2.0)
		AND SHOULD_DISPLAY_HELP_WITHOUT_SEAT()
			IF !IS_HELP_MESSAGE_BEING_DISPLAYED()
			AND NOT IS_HELP_MESSAGE_ON_SCREEN()
				PRINT_HELP_FOREVER("DRONE_TRIG")
			ENDIF
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DRONE_TRIG")
				IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_CONTEXT)
				OR IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_CONTEXT)
					sDroneControl.bDroneEMPCoolDown = FALSE
					SET_PLAYER_INITIALISING_DRONE(TRUE)
					SET_DRONE_STATE(DRONE_STATE_INIT)
					CLEAR_HELP()
				ENDIF
			ENDIF
		ELSE
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DRONE_TRIG")
				CLEAR_HELP()
			ENDIF
		ENDIF

		IF IS_PLAYER_TRYING_TO_SEAT_IN_DRONE_STATION()
		OR IS_PLAYER_IN_DRONE_SEAT_IN_HACKER_TRUCK()
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DRONE_TRIG")
				CLEAR_HELP()
			ENDIF
		ENDIF	
	ELSE
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DRONE_TRIG")
			CLEAR_HELP()
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_TABLET_SPECTATOR_DRONE_ALLOWED()
	IF CONTENT_IS_USING_ARENA()
	AND NOT IS_THIS_MISSION_ROCKSTAR_CREATED()
		RETURN TRUE
	ENDIF
	
	INT iTeam
	INT iRule
	
	iTeam = GET_PLAYER_TEAM(PLAYER_ID())
	
	IF iTeam >= FMMC_MAX_TEAMS
	OR iTeam < 0
		iTeam = 0
	ENDIF
	
	iRule = gBG_MC_serverBD_VARS.iCurrentHighestPriority[iTeam]
	
	IF iRule < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwelve[iRule], ciBS_RULE12_ROAM_SPEC_DISABLE_DRONE)
		AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwelve[iRule], ciBS_RULE12_ROAM_SPEC_DISABLE_RCCAR)
		AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwelve[iRule], ciBS_RULE12_ROAM_SPEC_DISABLE_TRAPCAM)
		AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwelve[iRule], ciBS_RULE12_ROAM_SPEC_DISABLE_TURRETCAM)
		AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyTwo, ciOptionsBS22_Spectator_Disable_Reward_Wheel)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyTwo, ciOptionsBS22_Spectator_Disable_Reward_Wheel)
		RETURN TRUE
	ELSE
		IF IS_PLAYER_USING_ARENA_BOX_SPECTATORS_TABLE()
		OR DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(PLAYER_ID())
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC VECTOR GET_SPECTATOR_DRONE_SPAWN_VECTOR()
	FLOAT fHead = 0.0
	FLOAT fSpaceX = 6.0
	FLOAT fSpaceY = 6.0
	
	VECTOR vPos = <<2800, -3800.2, 170.0>>
	
	IF g_FMMC_STRUCT.sArenaInfo.iArena_Variation = ARENA_VARIATION_DYST_4
	OR g_FMMC_STRUCT.sArenaInfo.iArena_Variation = ARENA_VARIATION_SCIFI_4
	OR g_FMMC_STRUCT.sArenaInfo.iArena_Variation = ARENA_VARIATION_CONSUMER_4
		vPos.y = -3860.0
	ENDIF
	
	fSpaceX *= PARTICIPANT_ID_TO_INT()
	fSpaceY *= (PARTICIPANT_ID_TO_INT() / 8)
	fSpaceX -= (8 * fSpaceY)
	
	RETURN GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPos, fHead, <<fSpaceX, fSpaceY, 0>>)
ENDFUNC

PROC MAINTAIN_ARENA_BOX_DRONE_TRIGGER()
	IF NOT SHOULD_BLOCK_DRONE()
		IF (IS_PLAYER_IN_ARENA_BOX_TABLET_SEAT()
		OR IS_PLAYER_USING_ARENA_BOX_SPECTATORS_TABLE())
		AND NOT g_bCelebrationScreenIsActive
		AND IS_TABLET_SPECTATOR_DRONE_ALLOWED()
		AND NOT IS_BIT_SET(g_iArenaSpectatorBS, GLOBAL_ARENA_SPECTATOR_BOX_BS_STANDING_UP)
		AND (g_bMissionClientGameStateRunning #IF IS_DEBUG_BUILD OR g_bDebugPlacedArenaBox #ENDIF )
		AND NOT (IS_PLAYER_IN_ARENA_BOX_TABLET_SEAT() AND g_sMPTunables.bAW_TABLET_DISABLE_SPECTATOR_DRONE)
			IF (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RLEFT)
			OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RLEFT))
			AND NOT IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RDOWN)
			AND NOT IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RDOWN)
			AND NOT IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_RIGHT)
			AND NOT IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_RIGHT)
			AND NOT IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CHARACTER_WHEEL)
				IF NETWORK_CAN_SPEND_MONEY(g_sMPTunables.iAW_TABLET_SPECTATOR_DRONE_COST, FALSE, FALSE, TRUE)
				OR IS_BIT_SET(g_iArenaSpectatorBS, GLOBAL_ARENA_SPECTATOR_BOX_BS_PAID_DRONE)
					SET_BIT(g_iArenaSpectatorBS, GLOBAL_ARENA_SPECTATOR_BOX_BS_LAUNCH_DRONE)
					INITIALISE(FALSE, FALSE)
					SET_PLAYER_INITIALISING_DRONE(TRUE)
					SET_DRONE_STATE(DRONE_STATE_INIT)
					
					INCREMENT_ARENA_SPECTATOR_DRONES_USED()
					
					INCREMENT_MP_INT_CHARACTER_AWARD(MP_AWARD_THROUGH_A_LENS)
					SET_LOCAL_PLAYER_ARENA_GARAGE_TROPHY_LENS_DATA()
					
					VECTOR vDronePos = GET_SPECTATOR_DRONE_SPAWN_VECTOR()
					
					SET_DRONE_COORD_AND_ROTATION(vDronePos, <<0.0, 0.0, 0.0>>)
					
					IF IS_PLAYER_IN_ARENA_BOX_TABLET_SEAT()
						INT iSoundID
						PLAY_SOUND_FROM_ENTITY(iSoundID, "Select_Spec_Drone", PLAYER_PED_ID(), "DLC_AW_Spectator_Tablet_Sounds", TRUE)
					ENDIF
				ELSE
					IF NOT IS_BIT_SET(g_iArenaSpectatorBS, GLOBAL_ARENA_SPECTATOR_BOX_BS_WARN_DRONE)
						SET_BIT(g_iArenaSpectatorBS, GLOBAL_ARENA_SPECTATOR_BOX_BS_WARN_DRONE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

#IF FEATURE_HEIST_ISLAND
/// PURPOSE:
///    Gets submarien missile launch hatch name
/// PARAMS:
///    playerToCheck - player who fires the missile
FUNC STRING GET_SUBMARINE_MISSILE_LAUNCH_BONE_NAME(PLAYER_INDEX playerToCheck)
	IF playerToCheck = INVALID_PLAYER_INDEX()
		PRINTLN("[AM_MP_DRONE] - GET_SUBMARINE_MISSILE_LAUNCH_BONE_NAME playerToCheck = INVALID_PLAYER_INDEX")
		RETURN ""
	ENDIF

	SWITCH sDroneControl.iSubmarineHatch[NATIVE_TO_INT(playerToCheck)]
		CASE 1	RETURN "sub_hatch1"
		CASE 2	RETURN "sub_hatch2"
		CASE 3	RETURN "sub_hatch3"
		CASE 4	RETURN "sub_hatch4"
		CASE 5	RETURN "sub_hatch5"
		CASE 6	RETURN "sub_hatch6"
		CASE 7	RETURN "sub_hatch7"
		CASE 8	RETURN "sub_hatch8"
	ENDSWITCH	

	RETURN "sub_hatch4"
ENDFUNC

PROC MAINTAIN_SUBMARINE_MISSILE_TRIGGER()
	IF IS_PLAYER_IN_MISSILE_SEAT_IN_SUBMARINE()
	AND NOT IS_PLAYER_TRYING_TO_SEAT_IN_SUBMARINE_MISSILE_STATION()
	AND NOT SHOULD_BLOCK_DRONE()
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		OR IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)

			IF g_iShouldLaunchSubmarineSeat = 1
				sDroneControl.iSubmarineHatch[NATIVE_TO_INT(PLAYER_ID())] = GET_RANDOM_INT_IN_RANGE(1, 5)
			ELSE
				sDroneControl.iSubmarineHatch[NATIVE_TO_INT(PLAYER_ID())] = GET_RANDOM_INT_IN_RANGE(5, 9)
			ENDIF
			
			PRINTLN("[AM_MP_DRONE] - MAINTAIN_SUBMARINE_MISSILE_TRIGGER - submarine hatch to use: ", sDroneControl.iSubmarineHatch[NATIVE_TO_INT(PLAYER_ID())])
			
			INT iBone 
			VECTOR vInitialStartCoord
			IF g_iShouldLaunchSubmarineSeat = 1
				iBone = GET_ENTITY_BONE_INDEX_BY_NAME(GET_SUBMARINE_INDEX_PED_IS_IN_INTERIOR(), GET_SUBMARINE_MISSILE_LAUNCH_BONE_NAME(PLAYER_ID()))
				vInitialStartCoord = GET_ENTITY_BONE_POSTION(GET_SUBMARINE_INDEX_PED_IS_IN_INTERIOR(), iBone)
				sDroneControl.vDroneInitialStartCoord = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vInitialStartCoord, GET_ENTITY_HEADING(GET_SUBMARINE_INDEX_PED_IS_IN_INTERIOR()), <<0.0, 0.0, -2.6>>)
				PRINTLN("[AM_MP_DRONE] - MAINTAIN_SUBMARINE_MISSILE_TRIGGER - submarine g_iShouldLaunchSubmarineSeat = 1 sDroneControl.vDroneInitialStartCoord: ", sDroneControl.vDroneInitialStartCoord)
			ELIF g_iShouldLaunchSubmarineSeat = 2
				iBone = GET_ENTITY_BONE_INDEX_BY_NAME(GET_SUBMARINE_INDEX_PED_IS_IN_INTERIOR(), GET_SUBMARINE_MISSILE_LAUNCH_BONE_NAME(PLAYER_ID()))																						
				vInitialStartCoord = GET_ENTITY_BONE_POSTION(GET_SUBMARINE_INDEX_PED_IS_IN_INTERIOR(), iBone)
				sDroneControl.vDroneInitialStartCoord = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vInitialStartCoord, GET_ENTITY_HEADING(GET_SUBMARINE_INDEX_PED_IS_IN_INTERIOR()), <<0.0, 0.0, -2.6>>)
				PRINTLN("[AM_MP_DRONE] - MAINTAIN_SUBMARINE_MISSILE_TRIGGER - submarine g_iShouldLaunchSubmarineSeat = 2 sDroneControl.vDroneInitialStartCoord: ", sDroneControl.vDroneInitialStartCoord)
			ENDIF

			VECTOR vSubmarineRot = GET_ENTITY_ROTATION(GET_SUBMARINE_INDEX_PED_IS_IN_INTERIOR())
			SET_DRONE_COORD_AND_ROTATION(sDroneControl.vDroneInitialStartCoord, <<vSubmarineRot.x + 90, 180.0, vSubmarineRot.z>>)
			SET_PLAYER_INITIALISING_DRONE(TRUE)
			BROADCAST_SUBMARINE_MISSILE_LAUNCH_VFX(sDroneControl.iSubmarineHatch[NATIVE_TO_INT(PLAYER_ID())])
			SET_DRONE_STATE(DRONE_STATE_INIT)
			CLEAR_SUBMARINE_USED_WEAPON_DATA()
			INCREMENT_SUBMARINE_PROJECTILE_FIRED()
			CLEAR_HELP()
			PRINTLN("[AM_MP_DRONE] - MAINTAIN_SUBMARINE_MISSILE_TRIGGER - Launching guided missile from submarine")
		ENDIF
	ELSE
		IF NOT IS_ENTITY_ALIVE(GET_SUBMARINE_INDEX_PED_IS_IN_INTERIOR())
			PRINTLN("[AM_MP_DRONE] - MAINTAIN_SUBMARINE_MISSILE_TRIGGER - submarine is dead")
		ENDIF
	ENDIF

	IF IS_PLAYER_TRYING_TO_SEAT_IN_SUBMARINE_MISSILE_STATION()
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MP_SUBSEAT_2")
			CLEAR_HELP()
		ENDIF
	ENDIF	
ENDPROC
#ENDIF

PROC MAINTAIN_DRONE_TRIGGER()
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		
		sDroneControl.vTargetCoord 	    	= GET_DRONE_TARGET_POSITION(sDroneControl)
		
		IF NOT IS_VECTOR_ZERO(GET_DRONE_OVERRIDE_COORDS())
			sDroneControl.vDroneStartCoord 	= GET_DRONE_OVERRIDE_COORDS()
		ELSE
			sDroneControl.vDroneStartCoord 	= GET_PLAYER_COORDS(PLAYER_ID())
			sDroneControl.vDroneStartCoord 	= <<sDroneControl.vDroneStartCoord.x, sDroneControl.vDroneStartCoord.y, sDroneControl.vDroneStartCoord.z + 1.5>>
		ENDIF
		
		IF IS_PLAYER_IN_HACKER_TRUCK(PLAYER_ID())
			START_DRONE_FROM_TRUCK(TRUE)
		ENDIF
	
		IF IS_PLAYER_IN_HACKER_TRUCK(PLAYER_ID())
			MAINTAIN_HACKER_TRUCK_DRONE_TRIGGER()
		
		// Is the player firing from a turret?
		ELIF (TURRET_MANAGER_GET_GROUP(PLAYER_ID()) = TGT_ARENA_CONTESTANT	
		OR TURRET_MANAGER_GET_GROUP(PLAYER_ID()) = TGT_ARENA_SPECTATOR)
		AND !IS_BIT_SET(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Request_Use_Drone)	
		AND !IS_BIT_SET(g_iSpecialSpectatorBS2, g_ciSpecial_Spectator_BS2_Drone_Initializing) 
			IF IS_BIT_SET(g_iBsTurretCam, ci_TURRET_CAM_BS_LAUNCH_DRONE_MISSILE)	
				START_DRONE_FROM_ARENA_WARS_MISSILE(TRUE)
				INITIALISE(FALSE, FALSE)
				TURRET_CAM_SET_HAVE_FIRED_DRONE_MISSILE()
				SET_PLAYER_INITIALISING_DRONE(TRUE)
				SET_DRONE_STATE(DRONE_STATE_INIT)		
				PRINTLN("[AM_MP_DRONE] - MAINTAIN_DRONE_TRIGGER - Launching turret_cam drone missile for turret type ", TURRET_MANAGER_GET_GROUP(PLAYER_ID()))
			ENDIF
			
		// Spectator box tablet
		ELIF IS_LOCAL_PLAYER_IN_ARENA_SPECTATOR_BOX()
		AND IS_TABLET_SPECTATOR_DRONE_ALLOWED()
		AND NOT IS_BIT_SET(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Request_Use_Drone)
			MAINTAIN_ARENA_BOX_DRONE_TRIGGER()
			PRINTLN("[AM_MP_DRONE] - MAINTAIN_DRONE_TRIGGER - MAINTAIN_ARENA_BOX_DRONE_TRIGGER")
		
		// Spectator box spin tabel 	
		ELIF IS_BIT_SET(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Request_Use_Drone)	
		AND IS_BIT_SET(g_iSpecialSpectatorBS2, g_ciSpecial_Spectator_BS2_Drone_Initializing) 	
			
			INITIALISE(FALSE, FALSE)
			SET_PLAYER_INITIALISING_DRONE(TRUE)
			SET_DRONE_STATE(DRONE_STATE_INIT)
			PRINTLN("[AM_MP_DRONE] - MAINTAIN_DRONE_TRIGGER - Launching drone from wheel spin prize")
		#IF FEATURE_HEIST_ISLAND
		ELIF IS_DRONE_STARTED_FROM_SUBMARINE_MISSILE()
			MAINTAIN_SUBMARINE_MISSILE_TRIGGER()
		#ENDIF
		
		ELSE
			IF SHOULD_START_DRONE_FROM_PLAYER_PHONE()
			AND !IS_DRONE_AREA_SAFE_TO_SPAWN()
				CLEAR_HELP()
				PRINT_HELP("PIM_DRONAMOS")
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				SET_DRONE_STATE(DRONE_STATE_CLEANUP)
			ELSE	
				IF IS_VECTOR_ZERO(sDroneControl.vDroneTriggerCoord)
					sDroneControl.bDroneEMPCoolDown = FALSE
					SET_PLAYER_INITIALISING_DRONE(TRUE)
					SET_DRONE_STATE(DRONE_STATE_INIT)
					PRINTLN("[AM_MP_DRONE] - MAINTAIN_DRONE_TRIGGER - sDroneControl.vDroneTriggerCoord is zero")
				ENDIF	
			ENDIF	
		ENDIF
	ENDIF	
ENDPROC

PROC MAINTAIN_TARGET_CREATION()
	IF IS_PLAYER_IN_HACKER_TRUCK(PLAYER_ID())
		MODEL_NAMES droneProp = GET_DRONE_TARGET_MODEL()
		
		sDroneControl.vTargetCoord = GET_DRONE_TARGET_POSITION(sDroneControl)
		
		IF NOT DOES_ENTITY_EXIST(sDroneControl.targetObject)
			IF NOT IS_VECTOR_ZERO(sDroneControl.vTargetCoord)
				IF REQUEST_LOAD_MODEL(droneProp)
					sDroneControl.targetObject = CREATE_OBJECT(droneProp, sDroneControl.vTargetCoord, FALSE, FALSE, TRUE)
					SET_ENTITY_COORDS_NO_OFFSET(sDroneControl.targetObject, sDroneControl.vTargetCoord)
					SET_ENTITY_COLLISION(sDroneControl.targetObject, TRUE)
					SET_ENTITY_LOAD_COLLISION_FLAG(sDroneControl.targetObject, TRUE)
					SET_ENTITY_RECORDS_COLLISIONS(sDroneControl.targetObject, TRUE)
					SET_ENTITY_HEADING(sDroneControl.targetObject, GET_DRONE_TARGET_HEADING())
					FREEZE_ENTITY_POSITION(sDroneControl.targetObject, TRUE)
					SET_ENTITY_VISIBLE(sDroneControl.targetObject, FALSE)
					SET_MODEL_AS_NO_LONGER_NEEDED(droneProp)
					PRINTLN("[AM_MP_DRONE] - MAINTAIN_TARGET_CREATION - TRUE")	
				ENDIF	
			ENDIF	
		ELSE
			OBJECT_INDEX placedPropTarget = GET_CLOSEST_OBJECT_OF_TYPE(sDroneControl.vTargetCoord, 2 ,droneProp, FALSE, FALSE, FALSE)
			IF DOES_ENTITY_EXIST(placedPropTarget)
			AND placedPropTarget != sDroneControl.targetObject
				IF NETWORK_HAS_CONTROL_OF_ENTITY(placedPropTarget)
					SET_ENTITY_COMPLETELY_DISABLE_COLLISION(placedPropTarget, FALSE)
					FREEZE_ENTITY_POSITION(placedPropTarget, TRUE)
					SET_ENTITY_VISIBLE(placedPropTarget, TRUE)
					PRINTLN("[AM_MP_DRONE] - MAINTAIN_TARGET_CREATION - interior prop set to invisible")
				ELSE
					NETWORK_REQUEST_CONTROL_OF_ENTITY(placedPropTarget)
				ENDIF
			ENDIF	
		ENDIF	
	ELSE
		sDroneControl.vTargetCoord = GET_DRONE_TARGET_POSITION(sDroneControl)
	ENDIF	
ENDPROC

PROC PROCESS_TARGET_DAMAGE()
	
	IF SHOULD_IGNORE_TARGET_DAMAGE()
	OR NOT GB_IS_PLAYER_ON_DRONE_MISSION(PLAYER_ID())
	OR NOT NETWORK_IS_GAME_IN_PROGRESS()
		EXIT
	ENDIF
	
	MODEL_NAMES droneDamageProp = GET_DRONE_TARGET_MODEL(TRUE)
	REQUEST_LOAD_MODEL(droneDamageProp)
	MODEL_NAMES droneProp = GET_DRONE_TARGET_MODEL()	
	OBJECT_INDEX placedPropTarget = GET_CLOSEST_OBJECT_OF_TYPE(sDroneControl.vTargetCoord, 2 ,droneProp, FALSE, FALSE, FALSE)
	
	IF DOES_ENTITY_EXIST(placedPropTarget)
	AND placedPropTarget != sDroneControl.targetObject
		IF HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(placedPropTarget, WEAPONTYPE_STUNGUN)
		OR GET_ENTITY_HEALTH(placedPropTarget) = 999
			IF NOT IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_DRONE_TARGET_DAMAGE_RECEIVED)
				BROADCAST_USED_DRONE_STATION(GB_GET_LOCAL_PLAYER_GANG_BOSS(), TRUE #IF FEATURE_HEIST_ISLAND , ENUM_TO_INT(DRONE_TYPE_HACKER_TRUCK) , 0 #ENDIF )
				SET_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_DRONE_TARGET_DAMAGE_RECEIVED)
				PRINTLN("[AM_MP_DRONE] - PROCESS_TARGET_DAMAGE - interior prop damage received")
			ENDIF
		ENDIF	
	ENDIF
	
	IF DOES_ENTITY_EXIST(sDroneControl.targetObject)
		IF HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(sDroneControl.targetObject, WEAPONTYPE_STUNGUN)
		OR GET_ENTITY_HEALTH(sDroneControl.targetObject) = 999
			IF NOT IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_DRONE_TARGET_DAMAGE_RECEIVED)
				BROADCAST_USED_DRONE_STATION(GB_GET_LOCAL_PLAYER_GANG_BOSS(), TRUE #IF FEATURE_HEIST_ISLAND , ENUM_TO_INT(DRONE_TYPE_HACKER_TRUCK) , 0 #ENDIF )
				SET_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_DRONE_TARGET_DAMAGE_RECEIVED)
				PRINTLN("[AM_MP_DRONE] - PROCESS_TARGET_DAMAGE - damage received")
			ENDIF
		ENDIF	
	ENDIF
	
	IF  IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_DRONE_TARGET_DAMAGE_RECEIVED)
 
		OBJECT_INDEX placedDamagedPropTarget = GET_CLOSEST_OBJECT_OF_TYPE(sDroneControl.vTargetCoord, 2 ,droneDamageProp, FALSE, FALSE, FALSE)
		IF DOES_ENTITY_EXIST(placedDamagedPropTarget)	
			IF DOES_ENTITY_EXIST(sDroneControl.targetObject)
				SET_ENTITY_VISIBLE(sDroneControl.targetObject, FALSE)
			ENDIF
			IF DOES_ENTITY_EXIST(placedPropTarget)
				SET_ENTITY_VISIBLE(placedPropTarget, FALSE)
			ENDIF
			SET_DRONE_TARGET_HIT(TRUE)				
		ELSE
			PRINTLN("[AM_MP_DRONE] - PROCESS_TARGET_DAMAGE - waiting on placedPropTarget")
		ENDIF		
	ENDIF	
ENDPROC

#IF FEATURE_HEIST_ISLAND
FUNC BOOL CAN_EXIT_DRONE_USING_PLAYER_CONTROL()
	IF IS_DRONE_STARTED_FROM_SUBMARINE_MISSILE()
		IF NOT CAN_TAKE_CONTROL_OF_SUBMARINE_MISSILE()
			RETURN FALSE		
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC
#ENDIF

PROC PROCESS_DRONE_EXIT()
	#IF FEATURE_HEIST_ISLAND
	IF NOT CAN_EXIT_DRONE_USING_PLAYER_CONTROL()
		EXIT
	ENDIF
	#ENDIF
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sDroneControl.cameraProp)
	AND NOT IS_ENTITY_DEAD(NET_TO_OBJ(sDroneControl.cameraProp))
		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(sDroneControl.cameraProp)
			IF DOES_CAM_EXIST(sDroneControl.droneCamera)
			AND IS_CAM_RENDERING(sDroneControl.droneCamera)
			AND NOT IS_DRONE_SNAPMATIC_ACTIVE()	
				CONTROL_ACTION droneExitAction = INPUT_VEH_CIN_CAM
				IF IS_DRONE_USED_AS_GUIDED_MISSILE()
					droneExitAction = INPUT_VEH_EXIT
				ENDIF
				
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,droneExitAction)
				OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,droneExitAction)
					IF NOT IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_EXIT_DRONE)						
						SET_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_EXIT_DRONE)
						PRINTLN("[AM_MP_DRONE] - PROCESS_DRONE_EXIT - DRONE_LOCAL_BS_EXIT_DRONE true")
					ENDIF	
				ENDIF
			ENDIF
		ENDIF
	ENDIF	
ENDPROC

FUNC BOOL SHOULD_TERMINATE_DRONE_SCRIPT()
	IF SHOULD_RETURN_DRONE_TO_PLAYER_AND_KILL_SCRIPT()
		RETURN TRUE
	ENDIF
	
	IF SHOULD_START_DRONE_FROM_PLAYER_PHONE()
		RETURN TRUE
	ENDIF
	
	IF IS_DRONE_STARTED_FROM_HEIST_FREEMODE()
	AND IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC 

PROC MAINTAIN_DRONE_CLEANUP()
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sDroneControl.cameraProp)
		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(sDroneControl.cameraProp)
			IF NOT SHOULD_TERMINATE_DRONE_SCRIPT()
				SCRIPT_CLEANUP(TRUE)
			ELSE	
				SCRIPT_CLEANUP()
			ENDIF	
		ELSE
			NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(sDroneControl.cameraProp)
		ENDIF
	ELSE
		IF NOT SHOULD_TERMINATE_DRONE_SCRIPT()
			SCRIPT_CLEANUP(TRUE)
		ELSE	
			SCRIPT_CLEANUP()
		ENDIF	
	ENDIF	
ENDPROC

PROC MAINTAIN_DRONE_ENVIROMENT_CONTROL()
	IF IS_DRONE_STARTED_FROM_TRUCK()
		IF IS_DRONE_STATE(DRONE_STATE_INIT)
			IF IS_ENTITY_ALIVE(GET_HACKER_TRUCK_INDEX_PED_IS_IN_INTERIOR())
				VECTOR vTruckCoord = GET_ENTITY_COORDS(GET_HACKER_TRUCK_INDEX_PED_IS_IN_INTERIOR())
				SET_POP_CONTROL_SPHERE_THIS_FRAME(vTruckCoord, 200, 200)
				SET_SCENARIO_PEDS_SPAWN_IN_SPHERE_AREA(sDroneControl.vDroneCoords, 60, 30)
			ENDIF	
		ENDIF	
	ENDIF	
	
	IF IS_DRONE_STATE(DRONE_STATE_INIT)
	OR IS_DRONE_STATE(DRONE_STATE_FLYING)
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sDroneControl.cameraProp)
			IF DOES_CAM_EXIST(sDroneControl.droneCamera)
			AND IS_CAM_RENDERING(sDroneControl.droneCamera)
				IF IS_DRONE_STATE(DRONE_STATE_FLYING)		
					IF NOT IS_VECTOR_ZERO(sDroneControl.vDroneCoords)
						SET_POP_CONTROL_SPHERE_THIS_FRAME(sDroneControl.vDroneCoords, 60, 200)
						ACTIVATE_INTERIOR_GROUPS_USING_CAMERA()
						SET_FOCUS_POS_AND_VEL(sDroneControl.vDroneCoords, GET_ENTITY_VELOCITY(NET_TO_OBJ(sDroneControl.cameraProp)))
						
						IF GET_FRAME_COUNT() % 120 = 0
							SET_SCENARIO_PEDS_SPAWN_IN_SPHERE_AREA(sDroneControl.vDroneCoords, 60, 30)
						ENDIF	
					ENDIF	
				ENDIF	
			ENDIF
		ENDIF	
	ENDIF
ENDPROC 

PROC PROCESS_DRONE_PLAYER_CONTROLS()
	IF IS_DRONE_STATE(DRONE_STATE_FLYING)
		
		// Need to do this mess because replay is unavailable when controls are off
		IF IS_DRONE_STARTED_FROM_TRUCK()
		OR IS_DRONE_STARTED_FROM_ARENA_WARS_ONE()
		OR IS_DRONE_STARTED_FROM_ARENA_WARS_TWO()
		OR (IS_DRONE_STARTED_FROM_HEIST_FREEMODE() AND NOT SHOULD_START_DRONE_FROM_PLAYER_PHONE())
		#IF FEATURE_HEIST_ISLAND
		OR IS_DRONE_STARTED_FROM_SUBMARINE_MISSILE()
		#ENDIF
			IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			ENDIF
		ENDIF
		
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON, FALSE)
		
		#IF FEATURE_HEIST_ISLAND
		IF IS_DRONE_STARTED_FROM_SUBMARINE_MISSILE()
			DISABLE_SELECTOR_THIS_FRAME()
		ENDIF
		#ENDIF
		
		IF NOT IS_DRONE_STARTED_FROM_ARENA_WARS_MISSILE()
			DISABLE_ALL_CONTROLS_FOR_PLAYER_THIS_FRAME()
			
			ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CHARACTER_WHEEL)
			ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_CHARACTER_MICHAEL)
			ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_CHARACTER_FRANKLIN)
			ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_CHARACTER_TREVOR)
			ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_CHARACTER_MULTIPLAYER)
			
			ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X) 
			ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y) 
			ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_X)
			ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_Y)
			
			ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_LB) 
			ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_RB) 
			ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_LT)
			ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_RT)
			ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_LS)
			ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_RS)
			
			ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
			ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_CONTEXT)
			ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
			ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
			ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
			ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
			ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
			ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
			ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
			
			#IF FEATURE_HEIST_ISLAND
			IF IS_DRONE_STARTED_FROM_SUBMARINE_MISSILE()
				ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_MULTIPLAYER_INFO)
			ENDIF
			#ENDIF
			
			IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
				ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_CANCEL)
				ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_ACCEPT) 
				ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_RIGHT)
				ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_LEFT)
				ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_LOOK_LR)
				ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_CELLPHONE_LEFT)
				ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_CELLPHONE_RIGHT)
				ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CELLPHONE_LEFT)
				ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CELLPHONE_RIGHT)
				ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MP_TEXT_CHAT_ALL)
				ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MP_TEXT_CHAT_TEAM)		
				ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MP_TEXT_CHAT_FRIENDS)	
				ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MP_TEXT_CHAT_CREW)	
			ENDIF				
			
			IF IS_PAUSE_MENU_ACTIVE()
				ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_FRONTEND_ACCEPT)
				ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_FRONTEND_CANCEL) 
				ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_FRONTEND_UP)
				ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_FRONTEND_DOWN)
				ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_FRONTEND_LEFT)
				ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_X)
				ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_Y)
				ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT_AXIS_Y)
				ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT_AXIS_X)
				ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_SELECT)
			ENDIF				
		ENDIF
	ENDIF
ENDPROC

FUNC BLIP_SPRITE GET_DRONE_BLIP_SPRITE()
	#IF FEATURE_HEIST_ISLAND
	IF IS_DRONE_STARTED_FROM_SUBMARINE_MISSILE()
		RETURN RADAR_TRACE_PICKUP_HOMING_ROCKET
	ENDIF
	#ENDIF
	
	RETURN RADAR_TRACE_BAT_DRONE
ENDFUNC

PROC MAINTAIN_DRONE_BLIP()
	IF IS_DRONE_STARTED_FROM_ARENA_WARS_MISSILE()
		EXIT
	ENDIF	
	
	IF IS_DRONE_RADAR_ACTIVE()
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sDroneControl.cameraProp)
			IF NOT DOES_BLIP_EXIST(sDroneControl.droneBlip)
				sDroneControl.droneBlip =  CREATE_BLIP_FOR_COORD(sDroneControl.vDroneCoords)
				SET_BLIP_SPRITE(sDroneControl.droneBlip , GET_DRONE_BLIP_SPRITE())
				PRINTLN("[AM_MP_DRONE] - MAINTAIN_DRONE_BLIP - created blip true")
				
				IF IS_DRONE_STARTED_FROM_HEIST_FREEMODE()
				AND NOT DOES_BLIP_EXIST(sDroneControl.playerBlip)
					sDroneControl.playerBlip = CREATE_BLIP_FOR_ENTITY(PLAYER_PED_ID())
					
					SET_BLIP_SPRITE(sDroneControl.playerBlip, INT_TO_ENUM(BLIP_SPRITE, 6))
					
					SHOW_HEIGHT_ON_BLIP(sDroneControl.playerBlip, FALSE)	
					
					SET_BLIP_SCALE(sDroneControl.playerBlip, PLAYER_ARROW_BLIP_SCALE)	
					
					SET_BLIP_PRIORITY(sDroneControl.playerBlip, INT_TO_ENUM(BLIP_PRIORITY, (ENUM_TO_INT(BLIPPRIORITY_OVER_CENTRE_BLIP)-1)))
					
					IF GET_GANG_ID_FOR_PLAYER(PLAYER_ID()) != -1
						SET_BLIP_COLOUR_FROM_HUD_COLOUR(sDroneControl.playerBlip, GET_HUD_COLOUR_FOR_GANG_ID(GET_GANG_ID_FOR_PLAYER(PLAYER_ID())))
					ELSE
						SET_BLIP_COLOUR_FROM_HUD_COLOUR(sDroneControl.playerBlip, FRIENDLY_ARROW_COLOUR())
					ENDIF
					
					SET_BLIP_HIDDEN_ON_LEGEND(sDroneControl.playerBlip, TRUE)
				ENDIF
			ELSE
				FLOAT fHeading = GET_INVERSE_HEADING(sDroneControl.fDroneHeading)
				
				FLOAT x, y
				
				x = sDroneControl.vDroneCoords.x
				y = sDroneControl.vDroneCoords.y
				SET_BLIP_DISPLAY(sDroneControl.droneBlip, DISPLAY_BLIP)
				SET_BLIP_COORDS(sDroneControl.droneBlip, sDroneControl.vDroneCoords)
				
				IF (IS_DRONE_STARTED_FROM_ARENA_WARS_ONE()
				OR IS_DRONE_STARTED_FROM_ARENA_WARS_TWO())
				AND IS_PAUSE_MENU_ACTIVE()
				AND NETWORK_IS_ACTIVITY_SESSION()
					IF NOT IS_PAUSEMAP_IN_INTERIOR_MODE()
						x = -323.1
						y = -1970.9 
						SET_BLIP_COORDS(sDroneControl.droneBlip, <<x, y, 0>>)
						SET_FAKE_PAUSEMAP_PLAYER_POSITION_THIS_FRAME(x, y)
						HIDE_MINIMAP_INTERIOR_MAP_THIS_FRAME()
					ENDIF	
				ENDIF
				
				LOCK_MINIMAP_POSITION(x, y)
				SET_BLIP_ROTATION(sDroneControl.droneBlip, ROUND(fHeading))
				SET_BLIP_SCALE(sDroneControl.droneBlip, 1.0)
				SET_BLIP_PRIORITY(sDroneControl.droneBlip, BLIPPRIORITY_HIGHEST)
				LOCK_MINIMAP_ANGLE(ROUND(fHeading))
				
				IF DOES_BLIP_EXIST(sDroneControl.playerBlip)
					SET_BLIP_ROTATION(sDroneControl.playerBlip, GET_PROPER_BLIP_ROTATION_FOR_PED(PLAYER_PED_ID()))
				ENDIF
			ENDIF
		ENDIF
	ENDIF	
ENDPROC

FUNC BOOL IS_FLOAT_IN_RANGE_FOR_FLOOR_CAL(FLOAT fValue, FLOAT fMinV, FLOAT fMaxV)
	IF fValue >= fMinV
	AND fValue < fMaxV 
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

PROC UPDATE_DRONE_MINIMAP(BOOL bInitUpdate = FALSE)
	IF IS_DRONE_STARTED_FROM_ARENA_WARS_MISSILE()
		EXIT
	ENDIF
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sDroneControl.cameraProp)
		IF DOES_CAM_EXIST(sDroneControl.droneCamera)
		AND IS_CAM_ACTIVE(sDroneControl.droneCamera)
			IF IS_CAM_RENDERING(sDroneControl.droneCamera)
			OR bInitUpdate
				IF NOT IS_DRONE_SNAPMATIC_ACTIVE()
					DISABLE_CELLPHONE_THIS_FRAME_ONLY()
				ENDIF	
				SET_BOSS_PROXIMITY_BLIP_AS_DISABLED(TRUE)
				
				IF (NOT IS_DRONE_RADAR_ACTIVE())
				OR IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_CAMERA_FUZZ)
				OR SHOULD_CLEANUP_DRONE_CAMERA_INSTANTLY()
					HIDE_HUD_AND_RADAR_THIS_FRAME()
				ELSE

					HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_STREET_NAME)
					HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
					HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_DISTRICT_NAME)
					
					HIDE_PLAYER_ARROW_THIS_FRAME()	
					HIDE_ALL_SHOP_BLIPS(TRUE)
					
					INTERIOR_INSTANCE_INDEX interiorInstanceIndex = GET_INTERIOR_FROM_ENTITY(NET_TO_ENT(sDroneControl.cameraProp))
					IF IS_VALID_INTERIOR(interiorInstanceIndex)
						
						g_sDroneGlobals.droneInterior = interiorInstanceIndex
						
						IF NOT IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_GET_DRONE_MINIMAP_DATA)
							sDroneControl.fInteriorHeading = RAD_TO_DEG(GET_INTERIOR_HEADING(interiorInstanceIndex))
							GET_INTERIOR_LOCATION_AND_NAMEHASH(interiorInstanceIndex, sDroneControl.vInteriorPos, sDroneControl.InteriorNameHash)
							SET_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_GET_DRONE_MINIMAP_DATA)
						ENDIF
						
						INT iFloor = 0
						
						IF IS_FLOAT_IN_RANGE_FOR_FLOOR_CAL(sDroneControl.vDroneCoords.z, 43, 48.7)
							iFloor = 1
						ENDIF
						
						IF g_sDroneGlobals.fOverrideDroneMapZoom = -1
							SET_RADAR_ZOOM_PRECISE(1.0)
						ELSE
							SET_RADAR_ZOOM_PRECISE(g_sDroneGlobals.fOverrideDroneMapZoom)
						ENDIF
						
						IF !GB_IS_PLAYER_ON_DRONE_MISSION(PLAYER_ID())
						AND NOT IS_PLAYER_CRITICAL_TO_ANY_EVENT(PLAYER_ID())
						#IF FEATURE_HEIST_ISLAND
						AND NOT IS_DRONE_STARTED_FROM_SUBMARINE_MISSILE()
						#ENDIF
							IF !IS_RADAR_MAP_DISABLED()
								DISABLE_RADAR_MAP(TRUE)
							ENDIF
						ENDIF	
						
						#IF FEATURE_HEIST_ISLAND
						IF NOT IS_DRONE_STARTED_FROM_SUBMARINE_MISSILE()
						#ENDIF
							SET_BIGMAP_ACTIVE(FALSE, FALSE)
						#IF FEATURE_HEIST_ISLAND
						ENDIF
						#ENDIF
						
					   	SET_RADAR_AS_INTERIOR_THIS_FRAME(sDroneControl.InteriorNameHash, sDroneControl.vInteriorPos.x, sDroneControl.vInteriorPos.y, FLOOR(sDroneControl.fInteriorHeading), iFloor)
					ELSE
						
						sDroneControl.InteriorNameHash = -1
						
						IF g_sDroneGlobals.fOverrideDroneMapZoom = -1
							SET_RADAR_ZOOM_PRECISE(0)
						ELSE
							SET_RADAR_ZOOM_PRECISE(g_sDroneGlobals.fOverrideDroneMapZoom)
						ENDIF
						
						IF !IS_RADAR_MAP_DISABLED()
						#IF FEATURE_HEIST_ISLAND
						AND NOT IS_DRONE_STARTED_FROM_SUBMARINE_MISSILE()
						#ENDIF
							DISABLE_RADAR_MAP(TRUE)
						ENDIF
						
						#IF FEATURE_HEIST_ISLAND
						IF NOT IS_DRONE_STARTED_FROM_SUBMARINE_MISSILE()
						#ENDIF
							SET_BIGMAP_ACTIVE(FALSE, FALSE)
						#IF FEATURE_HEIST_ISLAND
						ENDIF
						#ENDIF
						
						g_sDroneGlobals.droneInterior = INT_TO_NATIVE(INTERIOR_INSTANCE_INDEX, -1)
						
						IF IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_GET_DRONE_MINIMAP_DATA)
							CLEAR_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_GET_DRONE_MINIMAP_DATA)
						ENDIF	
					ENDIF	
				ENDIF	
			ENDIF	
		ENDIF
	ENDIF	
ENDPROC 

FUNC BOOL IS_DRONE_READY_TO_FLY()
	
	IF SHOULD_START_DRONE_WITHOUT_SEAT()
	AND NOT IS_PLAYER_IN_DRONE_SEAT_IN_HACKER_TRUCK()
		PRINTLN("[AM_MP_DRONE] - IS_DRONE_READY_TO_FLY SHOULD_START_DRONE_WITHOUT_SEAT TRUE")
		RETURN TRUE
	ENDIF
	
	IF SHOULD_DISPLAY_HELP_WITHOUT_SEAT()
		PRINTLN("[AM_MP_DRONE] - IS_DRONE_READY_TO_FLY SHOULD_DISPLAY_HELP_WITHOUT_SEAT")
		RETURN TRUE
	ENDIF
	
	IF SHOULD_BY_PASS_DRONE_SEAT_CHECK()
		PRINTLN("[AM_MP_DRONE] - IS_DRONE_READY_TO_FLY SHOULD_BY_PASS_DRONE_SEAT_CHECK")
		RETURN TRUE
	ENDIF
	
	IF (IS_PLAYER_IN_DRONE_SEAT_IN_HACKER_TRUCK()
	AND IS_PLAYER_PLAYING_DRONE_STATION_ANIMATION())
		PRINTLN("[AM_MP_DRONE] - IS_DRONE_READY_TO_FLY IS_PLAYER_IN_DRONE_SEAT_IN_HACKER_TRUCK")
		RETURN TRUE
	ENDIF
	
	#IF FEATURE_HEIST_ISLAND
	IF (IS_PLAYER_IN_MISSILE_SEAT_IN_SUBMARINE()
	AND IS_PLAYER_PLAYING_DRONE_STATION_ANIMATION())
		PRINTLN("[AM_MP_DRONE] - IS_DRONE_READY_TO_FLY IS_PLAYER_IN_MISSILE_SEAT_IN_SUBMARINE")
		RETURN TRUE
	ENDIF
	#ENDIF
	
	IF NOT IS_PLAYER_IN_HACKER_TRUCK(PLAYER_ID())
	AND NOT IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
		PRINTLN("[AM_MP_DRONE] - IS_DRONE_READY_TO_FLY not is IS_PLAYER_IN_HACKER_TRUCK")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC START_DRONE_AUDIO_SCENE()
	IF IS_DRONE_STARTED_FROM_TRUCK()
	OR SHOULD_START_DRONE_FROM_PLAYER_PHONE()
		IF !IS_AUDIO_SCENE_ACTIVE("DLC_BTL_Hacker_Drone_HUD_Scene")
			START_AUDIO_SCENE("DLC_BTL_Hacker_Drone_HUD_Scene")
		ENDIF
	ELIF IS_DRONE_STARTED_FROM_ARENA_WARS_ONE()
		IF !IS_AUDIO_SCENE_ACTIVE("DLC_Arena_Battle_Drone_HUD_Scene")
			START_AUDIO_SCENE("DLC_Arena_Battle_Drone_HUD_Scene")
		ENDIF
	ELIF IS_DRONE_STARTED_FROM_ARENA_WARS_TWO()
		IF !IS_AUDIO_SCENE_ACTIVE("DLC_Arena_Spectator_Drone_HUD_Scene")
			START_AUDIO_SCENE("DLC_Arena_Spectator_Drone_HUD_Scene")
		ENDIF
	ELIF IS_DRONE_STARTED_FROM_ARENA_WARS_MISSILE()
		IF !IS_AUDIO_SCENE_ACTIVE("dlc_aw_arena_piloted_missile_scene")
			START_AUDIO_SCENE("dlc_aw_arena_piloted_missile_scene")
		ENDIF
	#IF FEATURE_HEIST_ISLAND
	ELIF IS_DRONE_STARTED_FROM_SUBMARINE_MISSILE()
		IF !IS_AUDIO_SCENE_ACTIVE("dlc_hei4_submarine_guided_missile_Scene")
			START_AUDIO_SCENE("dlc_hei4_submarine_guided_missile_Scene")
		ENDIF
	#ENDIF
	ENDIF
ENDPROC

FUNC FLOAT GET_DRONE_ANIMATION_EXIT_PHASE()
	RETURN 0.95
ENDFUNC

FUNC BOOL HAS_DRONE_ANIMATION_CLIP_FINISHED(BOOL bStopAnim = FALSE)
	PED_INDEX pedToCheck = PLAYER_PED_ID()

	IF IS_ENTITY_ALIVE(pedToCheck)
		FLOAT fExitAnimPhase = GET_DRONE_ANIMATION_EXIT_PHASE()
		INT iLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sDroneControl.iDroneSyncScene)
		
		IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSceneID)
		AND GET_SYNCHRONIZED_SCENE_PHASE(iLocalSceneID) >= fExitAnimPhase
			IF bStopAnim
				STOP_CURRENT_DRONE_SYNCHED_SCENE()
			ENDIF	
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC VECTOR GET_DRONE_CAMERA_OFFSET()
	IF IS_DRONE_STARTED_FROM_ARENA_WARS_ONE()
	OR IS_DRONE_STARTED_FROM_ARENA_WARS_TWO()
		RETURN <<0, -0.099, -0.020>>
	ELIF IS_DRONE_STARTED_FROM_ARENA_WARS_MISSILE()	
		RETURN <<0, -0.900, 0>> 
	ELIF IS_DRONE_STARTED_FROM_HEIST_FREEMODE()
	OR IS_DRONE_STARTED_FROM_CASINO_HEIST_MISSON()
		RETURN <<0, -0.038, -0.006>> 
	#IF FEATURE_COPS_N_CROOKS
	ELIF IS_DRONE_STARTED_FROM_ARCADE_COP()
		RETURN <<0, -0.130, 0.00>> 
	#ENDIF
	#IF FEATURE_HEIST_ISLAND
	ELIF IS_DRONE_STARTED_FROM_SUBMARINE_MISSILE()
		RETURN <<0, -5.480, 0.00>> 
	#ENDIF
	ENDIF
					
	RETURN  <<0, -0.038, -0.004>>
ENDFUNC


PROC MAINTAIN_DRONE_CAMERA_CREATION()
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sDroneControl.cameraProp)
		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(sDroneControl.cameraProp)
			IF NOT DOES_CAM_EXIST(sDroneControl.droneCamera)
			AND IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_PROP_CREATED)	
			
				IF IS_DRONE_READY_TO_FLY()
					IF IS_DRONE_STARTED_FROM_DEBUG()
						NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_FREEZE_POSITION | NSPC_ALLOW_PLAYER_DAMAGE)
					ELIF NOT IS_DRONE_STARTED_FROM_ARENA_WARS_MISSILE()
						NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_ALLOW_PLAYER_DAMAGE)
					ENDIF
					
					IF NOT SHOULD_SHOW_DRONE_TICKER()
						THEFEED_HIDE()
					ELSE
						THEFEED_SHOW()
					ENDIF

					sDroneControl.droneCamera = CREATE_CAMERA(DEFAULT, TRUE)

					SET_CAM_FOV(sDroneControl.droneCamera, sDroneControl.fBaseCamFov)
					SET_CAM_NEAR_CLIP(sDroneControl.droneCamera, 0.01)
					SET_CAM_NEAR_DOF(sDroneControl.droneCamera, 0.01)
					
					CLEAR_TIMECYCLE_MODIFIER()
					IF IS_DRONE_STARTED_FROM_ARENA_WARS_MISSILE()
						SET_TIMECYCLE_MODIFIER("eyeinthesky")
					ENDIF
					
					VECTOR vOffset = GET_DRONE_CAMERA_OFFSET()

					HARD_ATTACH_CAM_TO_ENTITY(sDroneControl.droneCamera, NET_TO_OBJ(sDroneControl.cameraProp), <<0, 0, 180>> , vOffset)
					
					IF IS_DRONE_STARTED_FROM_ARENA_WARS_MISSILE()	
						SET_CAM_ROT(sDroneControl.droneCamera, GET_DRONE_OVERRIDE_ROTATION() - <<180, -180, 0.0>>)
					ENDIF	
					
					SET_POP_CONTROL_SPHERE_THIS_FRAME(sDroneControl.vDroneCoords, 75, 75)
					SET_FOCUS_POS_AND_VEL(sDroneControl.vDroneCoords, GET_CAM_ROT(sDroneControl.droneCamera))
					
					#IF FEATURE_HEIST_ISLAND
					IF NOT IS_DRONE_STARTED_FROM_SUBMARINE_MISSILE()
					#ENDIF
						SET_ENTITY_INVINCIBLE(NET_TO_OBJ(sDroneControl.cameraProp), FALSE)
					#IF FEATURE_HEIST_ISLAND
					ENDIF
					#ENDIF
					
					IF NOT IS_DRONE_STARTED_FROM_DEBUG()
						IF IS_NEW_LOAD_SCENE_LOADED()
							NEW_LOAD_SCENE_STOP()
						ENDIF	
					ENDIF
					
					// Call these this frame to avoid any pops
					IF NOT SHOULD_START_DRONE_FROM_PLAYER_PHONE()
					#IF FEATURE_HEIST_ISLAND
					AND NOT IS_DRONE_STARTED_FROM_SUBMARINE_MISSILE()
					#ENDIF
						START_DRONE_AUDIO_SCENE()
						
						IF HAS_SOUND_FINISHED(sDroneControl.iStartUpSound)
							sDroneControl.iStartUpSound = GET_SOUND_ID()
							PLAY_SOUND_FRONTEND(sDroneControl.iStartUpSound, "HUD_Startup", sDroneControl.sSoundSet)
						ENDIF
					
						MAINTAIN_DRONE_BLIP()
						IF IS_DRONE_USED_AS_GUIDED_MISSILE()
							DRAW_MISSILE_DRONE_SCALEFORM()
						ELSE
							DRAW_DRONE_SCALEFORM()
						ENDIF
					
						UPDATE_DRONE_MINIMAP(TRUE)
						UPDATE_DRONE_CAMERA()
						UPDATE_DRONE_MOVEMENT()
						
						RENDER_SCRIPT_CAMS(TRUE, FALSE, 0 , TRUE, TRUE)
					
						sDroneControl.droneUsageStat = GET_NETWORK_TIME()
					ENDIF
		
					SET_LOCAL_PLAYER_USING_DRONE(TRUE)
					SET_PLAYER_USING_DRONE(TRUE)
					SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_AM_FLY_DRONE)
					SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iLocalBS, DRONE_PLAYER_BD_URING_DRONE)
					
					IF NOT SHOULD_START_DRONE_FROM_PLAYER_PHONE()
					#IF FEATURE_HEIST_ISLAND
					AND NOT IS_DRONE_STARTED_FROM_SUBMARINE_MISSILE()
					#ENDIF
						#IF FEATURE_GEN9_EXCLUSIVE
						SET_AUDIO_RAIN_HAPTIC_FLAG(TRUE)
						#ENDIF
					
						SET_DRONE_STATE(DRONE_STATE_FLYING)
					#IF FEATURE_HEIST_ISLAND
					ELIF IS_DRONE_STARTED_FROM_SUBMARINE_MISSILE()
						SET_DRONE_STATE(DRONE_STATE_LAUNCH_VFX)
					#ENDIF
					ELSE	
						sDroneControl.vAnimCoords = GET_PLAYER_COORDS(PLAYER_ID())
						GET_GROUND_Z_FOR_3D_COORD(sDroneControl.vAnimCoords, sDroneControl.vAnimCoords.z)
						sDroneControl.vAnimHeading = GET_ENTITY_HEADING(PLAYER_PED_ID())

						HOLSTER_WEAPON_IN_SIMPLE_INTERIOR()
					
						sDroneControl.vAnimCoords  = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(sDroneControl.vAnimCoords, sDroneControl.vAnimHeading, << -0.0695723, 0.177497, 0.0 >>)

						SET_DRONE_STATE(DRONE_STATE_START_ANIMATION)
					ENDIF
				ELSE
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
					ENDIF
					PRINTLN("[AM_MP_DRONE] - MAINTAIN_DRONE_CAMERA_CREATION IS_DRONE_READY_TO_FLY FALSE")
				ENDIF	
			ELSE
				IF DOES_CAM_EXIST(sDroneControl.droneCamera)
					PRINTLN("[AM_MP_DRONE] - MAINTAIN_DRONE_CAMERA_CREATION sDroneControl.droneCamera don't exist")
				ENDIF
				IF NOT IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_PROP_CREATED)
					PRINTLN("[AM_MP_DRONE] - MAINTAIN_DRONE_CAMERA_CREATION DRONE_LOCAL_BS_PROP_CREATED FALSE")
				ENDIF
			ENDIF	
		ELSE
			NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(sDroneControl.cameraProp)
		ENDIF	
	ENDIF
ENDPROC

PROC UPDATE_PAUSE_MENU_MAP()
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sDroneControl.cameraProp)
		IF NOT IS_ENTITY_DEAD(NET_TO_VEH(sDroneControl.cameraProp))
			FLOAT x, y
			
			IF IS_DRONE_STARTED_FROM_ARENA_WARS_ONE()
			OR IS_DRONE_STARTED_FROM_ARENA_WARS_TWO()
			OR IS_DRONE_STARTED_FROM_ARENA_WARS_MISSILE()
				x = -323.1
				y = -1970.9 
			ELSE
				x = sDroneControl.vDroneCoords.x
				y = sDroneControl.vDroneCoords.y
			ENDIF
			
			SET_FAKE_PAUSEMAP_PLAYER_POSITION_THIS_FRAME(x, y)
			SET_FAKE_GPS_PLAYER_POSITION_THIS_FRAME(x, y)
			
			IF NOT IS_VALID_INTERIOR(g_sDroneGlobals.droneInterior)
			AND NOT IS_DRONE_STARTED_FROM_ARENA_WARS_MISSILE() // B* 5493648

				IF NOT IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_GET_DRONE_MINIMAP_DATA)
					HIDE_MINIMAP_INTERIOR_MAP_THIS_FRAME()
				ENDIF	
				
				SET_RADAR_AS_EXTERIOR_THIS_FRAME()
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PLAY_FLIGHT_LOOP_SOUND()
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sDroneControl.cameraProp)
		IF HAS_SOUND_FINISHED(sDroneControl.iFlightLoopSoundID)
			sDroneControl.iFlightLoopSoundID = GET_SOUND_ID()
			PLAY_SOUND_FROM_ENTITY(sDroneControl.iFlightLoopSoundID, "Flight_Loop", NET_TO_OBJ(sDroneControl.cameraProp), sDroneControl.sSoundSet)
			SET_VARIABLE_ON_SOUND(sDroneControl.iFlightLoopSoundID, "DroneRotationalSpeed", sDroneControl.fRotationalSpeedForSound)
		ELSE
			SET_VARIABLE_ON_SOUND(sDroneControl.iFlightLoopSoundID, "DroneRotationalSpeed", sDroneControl.fRotationalSpeedForSound)
		ENDIF
		
		//DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_FLOAT(sDroneControl.fRotationalSpeedForSound), <<0.5, 0.5, 0.5>>, 255, 255, 255)
		
		IF HAS_SOUND_FINISHED(sDroneControl.iStartUpSound)
			RELEASE_SOUND_ID(sDroneControl.iStartUpSound)
			sDroneControl.iStartUpSound = -1
		ENDIF
	ENDIF	
ENDPROC

PROC PLAY_REMOTE_FLIGHT_LOOP_SOUND()
	IF NOT IS_DRONE_STATE(DRONE_STATE_REMOTE_DRONE)
	AND NOT IS_PLAYER_IN_SUBMARINE_DRIVER_SEAT()
		EXIT	
	ENDIF
	
	IF sDroneControl.iPlayerDetectionStagger != -1
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(playerBD[sDroneControl.iPlayerDetectionStagger].netID_remoteDrone)
		AND IS_ENTITY_ALIVE(NET_TO_OBJ(playerBD[sDroneControl.iPlayerDetectionStagger].netID_remoteDrone))
			IF IS_REMOTE_DRONE_STARTED_FROM_ARENA_WARS_TWO(INT_TO_NATIVE(PLAYER_INDEX, sDroneControl.iPlayerDetectionStagger))
				sDroneControl.sSoundSet				= "DLC_Arena_Drone_Sounds"
			ELIF IS_REMOTE_DRONE_STARTED_FROM_ARENA_WARS_ONE(INT_TO_NATIVE(PLAYER_INDEX, sDroneControl.iPlayerDetectionStagger))
				sDroneControl.sSoundSet				= "DLC_Arena_Battle_Drone_Sounds"
			ELIF IS_REMOTE_DRONE_STARTED_FROM_ARENA_WARS_MISSILE(INT_TO_NATIVE(PLAYER_INDEX, sDroneControl.iPlayerDetectionStagger))
				sDroneControl.sSoundSet				= "DLC_Arena_Piloted_Missile_Sounds"
			#IF FEATURE_HEIST_ISLAND
			ELIF IS_REMOTE_DRONE_STARTED_FROM_SUBMARINE_MISSILE(INT_TO_NATIVE(PLAYER_INDEX, sDroneControl.iPlayerDetectionStagger))
				sDroneControl.sSoundSet				= "DLC_H4_Piloted_Missile_Sounds"
			#ENDIF
			ELSE
				sDroneControl.sSoundSet				= "DLC_BTL_Drone_Sounds"
			ENDIF	
			IF sDroneControl.iRemoteFlightLoopSoundID[sDroneControl.iPlayerDetectionStagger] = -1
				sDroneControl.iRemoteFlightLoopSoundID[sDroneControl.iPlayerDetectionStagger] = GET_SOUND_ID()
				PLAY_SOUND_FROM_ENTITY(sDroneControl.iRemoteFlightLoopSoundID[sDroneControl.iPlayerDetectionStagger], "Flight_Loop", NET_TO_OBJ(playerBD[sDroneControl.iPlayerDetectionStagger].netID_remoteDrone), sDroneControl.sSoundSet)
				SET_VARIABLE_ON_SOUND(sDroneControl.iRemoteFlightLoopSoundID[sDroneControl.iPlayerDetectionStagger], "DroneRotationalSpeed", 1.0)
			ELSE
				SET_VARIABLE_ON_SOUND(sDroneControl.iRemoteFlightLoopSoundID[sDroneControl.iPlayerDetectionStagger], "DroneRotationalSpeed", 1.0)
			ENDIF
		ELSE
			IF sDroneControl.iRemoteFlightLoopSoundID[sDroneControl.iPlayerDetectionStagger] != -1
				STOP_SOUND(sDroneControl.iRemoteFlightLoopSoundID[sDroneControl.iPlayerDetectionStagger])
				RELEASE_SOUND_ID(sDroneControl.iRemoteFlightLoopSoundID[sDroneControl.iPlayerDetectionStagger])
				sDroneControl.iRemoteFlightLoopSoundID[sDroneControl.iPlayerDetectionStagger] = -1
			ENDIF
		ENDIF	
	ENDIF	
ENDPROC

PROC MAINTAIN_REMOTE_MISSILE_PARTICLE_PFX()
	IF sDroneControl.iPlayerDetectionStagger = -1
		EXIT
	ENDIF
	
//	IF NOT IS_DRONE_STATE(DRONE_STATE_REMOTE_DRONE)
//	AND NOT IS_PLAYER_IN_SUBMARINE_DRIVER_SEAT()
//		EXIT	
//	ENDIF
	
	IF NOT IS_REMOTE_DRONE_STARTED_FROM_ARENA_WARS_MISSILE(INT_TO_NATIVE(PLAYER_INDEX, sDroneControl.iPlayerDetectionStagger))
	#IF FEATURE_HEIST_ISLAND
	AND NOT IS_REMOTE_DRONE_STARTED_FROM_SUBMARINE_MISSILE(INT_TO_NATIVE(PLAYER_INDEX, sDroneControl.iPlayerDetectionStagger))
	#ENDIF
		EXIT
	ENDIF
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(playerBD[sDroneControl.iPlayerDetectionStagger].netID_remoteDrone)
		STRING sMissileParticleFX = "scr_xs_props"
		
		REQUEST_NAMED_PTFX_ASSET(sMissileParticleFX)
		
		IF HAS_NAMED_PTFX_ASSET_LOADED(sMissileParticleFX)
			IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(sDroneControl.missilePFX[sDroneControl.iPlayerDetectionStagger])
				USE_PARTICLE_FX_ASSET(sMissileParticleFX)
				IF IS_REMOTE_DRONE_STARTED_FROM_ARENA_WARS_MISSILE(INT_TO_NATIVE(PLAYER_INDEX, sDroneControl.iPlayerDetectionStagger))
					sDroneControl.missilePFX[sDroneControl.iPlayerDetectionStagger] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_xs_guided_missile_trail"
																					, NET_TO_OBJ(playerBD[sDroneControl.iPlayerDetectionStagger].netID_remoteDrone)	, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
				ELSE
					sDroneControl.missilePFX[sDroneControl.iPlayerDetectionStagger] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_xs_guided_missile_trail"
																					, NET_TO_OBJ(playerBD[sDroneControl.iPlayerDetectionStagger].netID_remoteDrone), <<0.0, 0.250, 0.0>>, <<0.0, 0.0, 0.0>>, 3.0)
				ENDIF
			ENDIF	
		ENDIF	
	ELSE
		IF DOES_PARTICLE_FX_LOOPED_EXIST(sDroneControl.missilePFX[sDroneControl.iPlayerDetectionStagger])
			STOP_PARTICLE_FX_LOOPED(sDroneControl.missilePFX[sDroneControl.iPlayerDetectionStagger])
		ENDIF
	ENDIF	
ENDPROC 

#IF FEATURE_HEIST_ISLAND
FUNC VECTOR GET_MISSILE_VFX_OFFSET(PLAYER_INDEX playerToCheck)
	IF sDroneControl.iSubmarineHatch[NATIVE_TO_INT(playerToCheck)] > 4
		RETURN <<0.0, 0.0, 1.5>>
	ENDIF
	
	RETURN <<0.0, 0.0, 1.5>>
ENDFUNC

PROC MAINTAIN_REMOTE_MISSILE_LAUNCH_PARTICLE_PFX()
	IF sDroneControl.iPlayerDetectionStagger = -1
		EXIT
	ENDIF
	
	PLAYER_INDEX playerFiredMissle = INT_TO_NATIVE(PLAYER_INDEX, sDroneControl.iPlayerDetectionStagger)
	
	IF NOT IS_NET_PLAYER_OK(playerFiredMissle)
		EXIT
	ENDIF

	IF NOT IS_DRONE_STATE(DRONE_STATE_REMOTE_DRONE)
	AND NOT IS_PLAYER_IN_SUBMARINE_DRIVER_SEAT()
		EXIT	
	ENDIF

	IF IS_BIT_SET(sDroneControl.iSubmarineVFXBS, sDroneControl.iPlayerDetectionStagger)
		VEHICLE_INDEX subVehIndex = GET_SUBMARINE_VEHICLE_INDEX_OF_PLAYER(GlobalPlayerBD[sDroneControl.iPlayerDetectionStagger].SimpleInteriorBD.propertyOwner)
		
		IF NOT IS_ENTITY_ALIVE(subVehIndex)
			EXIT
		ENDIF	
		
		STRING sMissileLaunchParticleFX = "scr_ih_sub"
	
		REQUEST_NAMED_PTFX_ASSET(sMissileLaunchParticleFX)
		
		IF HAS_NAMED_PTFX_ASSET_LOADED(sMissileLaunchParticleFX)

			IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(sDroneControl.missileLaunchPFX[NATIVE_TO_INT(playerFiredMissle)])
				USE_PARTICLE_FX_ASSET(sMissileLaunchParticleFX)
				
				STRING sBoneName = GET_SUBMARINE_MISSILE_LAUNCH_BONE_NAME(playerFiredMissle)
				INT iBone = GET_ENTITY_BONE_INDEX_BY_NAME(subVehIndex, sBoneName)

				sDroneControl.missileLaunchPFX[NATIVE_TO_INT(playerFiredMissle)] = START_PARTICLE_FX_LOOPED_ON_ENTITY_BONE("scr_ih_sub_missile_launch", subVehIndex, GET_MISSILE_VFX_OFFSET(playerFiredMissle), <<0.0, 0.0, 0.0>>, iBone, 5.0)
				
				PRINTLN("[AM_MP_DRONE] - MAINTAIN_REMOTE_MISSILE_LAUNCH_PARTICLE_PFX start VFX for player: ", GET_PLAYER_NAME(playerFiredMissle))
			ELSE
				IF NOT HAS_NET_TIMER_STARTED(sDroneControl.vMissileLaunchVFXTimer[NATIVE_TO_INT(playerFiredMissle)])
					START_NET_TIMER(sDroneControl.vMissileLaunchVFXTimer[NATIVE_TO_INT(playerFiredMissle)])
					SET_PARTICLE_FX_LOOPED_EVOLUTION(sDroneControl.missileLaunchPFX[NATIVE_TO_INT(playerFiredMissle)], "flame", 0.15)
				ENDIF	
			ENDIF	
			
			IF HAS_NET_TIMER_STARTED(sDroneControl.vMissileLaunchVFXTimer[NATIVE_TO_INT(playerFiredMissle)])
				IF HAS_NET_TIMER_EXPIRED(sDroneControl.vMissileLaunchVFXTimer[NATIVE_TO_INT(playerFiredMissle)], 3000)
					IF DOES_PARTICLE_FX_LOOPED_EXIST(sDroneControl.missileLaunchPFX[NATIVE_TO_INT(playerFiredMissle)])
						STOP_PARTICLE_FX_LOOPED(sDroneControl.missileLaunchPFX[NATIVE_TO_INT(playerFiredMissle)])
					ENDIF
					RESET_NET_TIMER(sDroneControl.vMissileLaunchVFXTimer[NATIVE_TO_INT(playerFiredMissle)])
					CLEAR_BIT(sDroneControl.iSubmarineVFXBS, sDroneControl.iPlayerDetectionStagger)
					PRINTLN("[AM_MP_DRONE] - MAINTAIN_REMOTE_MISSILE_LAUNCH_PARTICLE_PFX stop VFX for player: ", GET_PLAYER_NAME(playerFiredMissle))
				ELIF HAS_NET_TIMER_EXPIRED(sDroneControl.vMissileLaunchVFXTimer[NATIVE_TO_INT(playerFiredMissle)], 2000)
					SET_PARTICLE_FX_LOOPED_EVOLUTION(sDroneControl.missileLaunchPFX[NATIVE_TO_INT(playerFiredMissle)], "flame", 0.0)
				ENDIF
			ENDIF
		ENDIF	
	ENDIF
ENDPROC
#ENDIF

PROC PLAY_HUD_LOOP_SOUND()
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sDroneControl.cameraProp)
		IF HAS_SOUND_FINISHED(sDroneControl.iHudLoopSoundID)
		AND sDroneControl.iHudLoopSoundID = -1 
			sDroneControl.iHudLoopSoundID = GET_SOUND_ID()
			PLAY_SOUND_FRONTEND(sDroneControl.iHudLoopSoundID, "HUD_Loop", sDroneControl.sSoundSet)
		ENDIF
	ENDIF	
ENDPROC

PROC PLAYER_DRONE_RESET_FLAGS()
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_UsingDrone, TRUE)
	ENDIF	
ENDPROC

PROC STOP_DETONATE_SOUND()
	IF sDroneControl.iDetonateSoundID != -1
		STOP_SOUND(sDroneControl.iDetonateSoundID)
		RELEASE_SOUND_ID(sDroneControl.iDetonateSoundID)
		sDroneControl.iDetonateSoundID = -1
	ENDIF
ENDPROC

/// PURPOSE:
///    IF drone user fails to broadcast cool down info try again, and block drone until this is resolved.
PROC MAINTAIN_FAILED_COOL_DOWN_BROADCAST()
	IF IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_DRONE_COOL_DOWN_BROADCAST_FAILED)
		PLAYER_INDEX pPropertyOwner
		
		IF GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner != INVALID_PLAYER_INDEX()
			pPropertyOwner = GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner 
			PRINTLN("[AM_MP_DRONE] - MAINTAIN_FAILED_COOL_DOWN_BROADCAST pPropertyOwner = GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner: ", GET_PLAYER_NAME(pPropertyOwner))
		ELSE
			IF g_OwnerOfHackerTruckPropertyIAmIn != INVALID_PLAYER_INDEX()
				pPropertyOwner = g_OwnerOfHackerTruckPropertyIAmIn
				PRINTLN("[AM_MP_DRONE] - MAINTAIN_FAILED_COOL_DOWN_BROADCAST pPropertyOwner = g_OwnerOfHackerTruckPropertyIAmIn: ", GET_PLAYER_NAME(pPropertyOwner))
			ELSE
				IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE)
					IF GB_GET_LOCAL_PLAYER_GANG_BOSS() != INVALID_PLAYER_INDEX()
						pPropertyOwner = GB_GET_LOCAL_PLAYER_GANG_BOSS()
						PRINTLN("[AM_MP_DRONE] - MAINTAIN_FAILED_COOL_DOWN_BROADCAST pPropertyOwner = GB_GET_LOCAL_PLAYER_GANG_BOSS: ", GET_PLAYER_NAME(pPropertyOwner))
					ENDIF	
				ENDIF
			ENDIF
		ENDIF
		
		IF pPropertyOwner != INVALID_PLAYER_INDEX()
			IF NETWORK_IS_PLAYER_ACTIVE(pPropertyOwner)
				IF NETWORK_IS_GAME_IN_PROGRESS()
					BROADCAST_USED_DRONE_STATION(pPropertyOwner, FALSE #IF FEATURE_HEIST_ISLAND , ENUM_TO_INT(DRONE_TYPE_HACKER_TRUCK) , 0 #ENDIF )
					CLEAR_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_DRONE_COOL_DOWN_BROADCAST_FAILED)
					PRINTLN("[AM_MP_DRONE] - MAINTAIN_FAILED_COOL_DOWN_BROADCAST Done")
				ELSE
					PRINTLN("[AM_MP_DRONE] - MAINTAIN_FAILED_COOL_DOWN_BROADCAST NETWORK_IS_GAME_IN_PROGRESS false")
				ENDIF
			ELSE
				PRINTLN("[AM_MP_DRONE] - MAINTAIN_FAILED_COOL_DOWN_BROADCAST owner is not active")
			ENDIF
		ELSE
			PRINTLN("[AM_MP_DRONE] - MAINTAIN_FAILED_COOL_DOWN_BROADCAST setting drone use to false but property owner is invalid")
		ENDIF
	ENDIF
ENDPROC

PROC CHECK_RESTRICTED_INTERIOR_FOR_DRONE()
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sDroneControl.cameraProp)
		IF IS_ENTITY_ALIVE(NET_TO_OBJ(sDroneControl.cameraProp))
			MP_RESTRICTED_INTERIORS theResIntDroneIsIn  = GET_RESTRICTED_INTERIOR_ENTITY_IS_IN(NET_TO_OBJ(sDroneControl.cameraProp))
			IF theResIntDroneIsIn != RESTRICTED_INTERIOR_INVALID
				IF NOT IS_ARRAYED_BIT_SET(GlobalServerBD_FM.restrictedInterior.iRestrictedInteriorBitSet, ENUM_TO_INT(theResIntDroneIsIn))
				AND NOT IS_ARRAYED_BIT_SET(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].restrictedInterior.iRestrictedInteriorBitSet,ENUM_TO_INT(theResIntDroneIsIn))
					PRINTLN("[AM_MP_DRONE] - CHECK_RESTRICTED_INTERIOR_FOR_DRONE: returning drone to, it is inside a restricted interior")
					RETURN_DRONE_TO_PLAYER(TRUE) 
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_COOL_DOWN_BROADCAST_DATA_WAIT()
	IF IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_DRONE_COOL_DOWN_BROADCAST_WAIT)
		IF NOT HAS_NET_TIMER_STARTED(sDroneControl.sDroneBroadcastWaitTimer)
			START_NET_TIMER(sDroneControl.sDroneBroadcastWaitTimer)
			PRINTLN("[AM_MP_DRONE] - MAINTAIN_COOL_DOWN_BROADCAST_DATA_WAIT start timer")
		ELSE
			IF HAS_NET_TIMER_EXPIRED(sDroneControl.sDroneBroadcastWaitTimer, 5000)
				CLEAR_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_DRONE_COOL_DOWN_BROADCAST_WAIT)
				RESET_NET_TIMER(sDroneControl.sDroneBroadcastWaitTimer)
				PRINTLN("[AM_MP_DRONE] - MAINTAIN_COOL_DOWN_BROADCAST_DATA_WAIT DRONE_LOCAL_BS_DRONE_COOL_DOWN_BROADCAST_WAIT FALSE")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_DRONE_DAMAGE_EVENT(INT iCount)
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sDroneControl.cameraProp)
		STRUCT_ENTITY_DAMAGE_EVENT sei 
		
		IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sei, SIZE_OF(sei))
			IF DOES_ENTITY_EXIST(sei.VictimIndex)
			AND NET_TO_ENT(sDroneControl.cameraProp) = sei.VictimIndex
				IF DOES_ENTITY_EXIST(sei.DamagerIndex)
					IF IS_ENTITY_A_PED(sei.DamagerIndex)
						IF IS_PED_A_PLAYER(GET_PED_INDEX_FROM_ENTITY_INDEX(sei.DamagerIndex))
						AND PLAYER_PED_ID() != GET_PED_INDEX_FROM_ENTITY_INDEX(sei.DamagerIndex)
							sDroneControl.iDroneStatEndReason = DRONE_CLEANUP_REASON_DESTROYED_BY_ANOTHER_PLAYER
							PRINTLN("[AM_MP_DRONE] - PROCESS_DRONE_DAMAGE_EVENT drone damaged by: ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_PED_INDEX_FROM_ENTITY_INDEX(sei.DamagerIndex))))
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF	
	ENDIF	
ENDPROC

#IF FEATURE_HEIST_ISLAND
PROC PROCESS_SUBMARINE_MISSLIE_LAUNCH_VFX_EVENT(INT iCount)
	IF !IS_DRONE_STATE(DRONE_STATE_REMOTE_DRONE)
	AND NOT IS_PLAYER_IN_SUBMARINE_DRIVER_SEAT()
		EXIT
	ENDIF
	
	SCRIPT_EVENT_DATA_SUBMARINE_MISSILE_LAUNCH_VFX Event 
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, Event, SIZE_OF(Event))
		IF Event.Details.FromPlayerIndex != INVALID_PLAYER_INDEX()
			sDroneControl.iSubmarineHatch[NATIVE_TO_INT(Event.Details.FromPlayerIndex)] = Event.iLaunchHatch
			SET_BIT(sDroneControl.iSubmarineVFXBS, NATIVE_TO_INT(Event.Details.FromPlayerIndex))
			PRINTLN("[AM_MP_DRONE] - PROCESS_SUBMARINE_MISSLIE_LAUNCH_VFX_EVENT start VFX for player: ", GET_PLAYER_NAME(Event.Details.FromPlayerIndex), " iLaunchHatch: ", Event.iLaunchHatch)
		ELSE
			PRINTLN("[AM_MP_DRONE] - PROCESS_SUBMARINE_MISSLIE_LAUNCH_VFX_EVENT sender property owner is invalid")
		ENDIF
	ENDIF	
ENDPROC
#ENDIF

PROC PROCESS_DRONE_EVENTS()

    INT iCount
    EVENT_NAMES ThisScriptEvent
	STRUCT_EVENT_COMMON_DETAILS Details
	
    REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iCount

        ThisScriptEvent = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iCount)

        SWITCH ThisScriptEvent
			CASE EVENT_NETWORK_DAMAGE_ENTITY
				IF !IS_DRONE_STATE(DRONE_STATE_REMOTE_DRONE)
					PROCESS_DRONE_DAMAGE_EVENT(iCount)
				ENDIF	
			BREAK
			CASE EVENT_NETWORK_SCRIPT_EVENT
				GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, Details, SIZE_OF(Details))
				#IF FEATURE_HEIST_ISLAND
				SWITCH Details.Type
					CASE SCRIPT_EVENT_SUBMARINE_MISSLIE_LAUNCH_VFX
						IF g_sBlockedEvents.bSCRIPT_EVENT_SUBMARINE_MISSLIE_LAUNCH_VFX
							EXIT
						ENDIF	
						PROCESS_SUBMARINE_MISSLIE_LAUNCH_VFX_EVENT(iCount)
					BREAK	
				ENDSWITCH
				#ENDIF
			BREAK
        ENDSWITCH
            
    ENDREPEAT
ENDPROC

FUNC BOOL IS_DRONE_SNAPMATIC_AVAILABLE()
	IF IS_DRONE_SNAPMATIC_ACTIVE()
		PRINTLN("[AM_MP_DRONE] - IS_DRONE_SNAPMATIC_AVAILABLE IS_DRONE_SNAPMATIC_ACTIVE true")
		RETURN FALSE
	ENDIF
	
	IF IS_PAUSE_MENU_ACTIVE()
	 	PRINTLN("[AM_MP_DRONE] - IS_DRONE_SNAPMATIC_AVAILABLE IS_PAUSE_MENU_ACTIVE true")
		RETURN FALSE
	ENDIF
	
	IF IS_SELECTOR_ONSCREEN(FALSE)
	OR IS_REPLAY_RECORDING_FEED_BUTTONS_ONSCREEN()
	OR IS_REPLAY_RECORDING()
		RESET_NET_TIMER(sDroneControl.sSelectorTimer)
		START_NET_TIMER(sDroneControl.sSelectorTimer)
		CLEAR_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_START_INSTRUCTIONAL_PROMPT)
		DRAW_DRONE_HUD()
		PRINTLN("[AM_MP_DRONE] - IS_DRONE_SNAPMATIC_AVAILABLE IS_SELECTOR_ONSCREEN true")
		RETURN FALSE
	ELSE
		IF HAS_NET_TIMER_STARTED(sDroneControl.sSelectorTimer)
			IF !HAS_NET_TIMER_EXPIRED(sDroneControl.sSelectorTimer, 2000)
				PRINTLN("[AM_MP_DRONE] - IS_DRONE_SNAPMATIC_AVAILABLE waiting on selector timer")
				RETURN FALSE
			ELSE
				CLEAR_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_START_INSTRUCTIONAL_PROMPT)
				RESET_NET_TIMER(sDroneControl.sSelectorTimer)
			ENDIF
		ENDIF
	ENDIF
	
	IF GB_IS_PLAYER_ON_DRONE_MISSION(PLAYER_ID())
		PRINTLN("[AM_MP_DRONE] - IS_DRONE_SNAPMATIC_AVAILABLE player in drone misison")
		RETURN FALSE
	ENDIF	
	
	RETURN TRUE
ENDFUNC

PROC PROCESS_SNAPMATIC_IDLE_STATE()
	IF IS_DRONE_SNAPMATIC_AVAILABLE()
		IF (IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		OR IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT))
			IF !IS_CELLPHONE_CAMERA_IN_USE()
				IF IS_SCREEN_FADED_IN()
					SET_DRONE_SNAPMATIC_ACTIVE(TRUE)
					REMOVE_MENU_HELP_KEYS()
					NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_ALLOW_PLAYER_DAMAGE)			
					DO_SCREEN_FADE_OUT(500)
					CLEANUP_MENU_ASSETS()

					LAUNCH_CELLPHONE_APPLICATION(AppCamera, TRUE, TRUE, FALSE)

					START_NET_TIMER(sDroneControl.sDroneSnapMaticStartTimer)
					START_NET_TIMER(sDroneControl.sCameraGridFailtimer)
					SET_DRONE_SNAMPMATIC_STATE(DRONE_SNAPMATIC_STATE_INIT)
				ENDIF	
			ENDIF	
		ENDIF
	ENDIF	
ENDPROC

PROC PROCESS_SNAPMATIC_INIT_STATE()
	IF IS_SCREEN_FADED_OUT()
		IF HAS_NET_TIMER_EXPIRED(sDroneControl.sDroneSnapMaticStartTimer, 3000)
			IF IS_CELLPHONE_CAMERA_IN_USE()
				IF IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_REMOVE_SNAPMATIC_GRID)
				OR HAS_NET_TIMER_EXPIRED(sDroneControl.sCameraGridFailtimer, 8000)
					IF !IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_REMOVE_SNAPMATIC_GRID)
						PRINTLN("[AM_MP_DRONE] - PROCESS_SNAPMATIC_INIT_STATE g_BSTU_REMOVE_SNAPMATIC_GRID false time out")
					ENDIF
					DO_SCREEN_FADE_IN(500)
					NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					RESET_NET_TIMER(sDroneControl.sDroneSnapMaticStartTimer)
					RESET_NET_TIMER(sDroneControl.sCameraGridFailtimer)
					SET_DRONE_SNAMPMATIC_STATE(DRONE_SNAPMATIC_STATE_ACTIVE)
				ELSE
					PRINTLN("[AM_MP_DRONE] - PROCESS_SNAPMATIC_INIT_STATE waiting on g_BSTU_REMOVE_SNAPMATIC_GRID")
				ENDIF
			ELSE
				IF HAS_NET_TIMER_EXPIRED(sDroneControl.sCameraGridFailtimer, 15000)
					RESET_NET_TIMER(sDroneControl.sDroneSnapMaticStartTimer)
					RESET_NET_TIMER(sDroneControl.sCameraGridFailtimer)
					HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
					SET_DRONE_SNAPMATIC_ACTIVE(FALSE)
					CLEAR_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_START_INSTRUCTIONAL_PROMPT)
					SET_DRONE_SNAMPMATIC_STATE(DRONE_SNAPMATIC_STATE_CLEANUP)
					PRINTLN("[AM_MP_DRONE] - PROCESS_SNAPMATIC_INIT_STATE IS_CELLPHONE_CAMERA_IN_USE is false for 15 seconds lets bail out")
				ENDIF
			ENDIF
		ENDIF
	ELSE
		PRINTLN("[AM_MP_DRONE] - PROCESS_SNAPMATIC_INIT_STATE waiting on screen to fade out")
	ENDIF
ENDPROC

PROC PROCESS_SNAPMATIC_ACTIVE_STATE()
	IF SHOULD_EXIT_DRONE_SNAPMATIC()
	OR IS_PAUSE_MENU_ACTIVE()
		HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
		SET_DRONE_SNAPMATIC_ACTIVE(FALSE)
		CLEAR_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_START_INSTRUCTIONAL_PROMPT)
		SET_DRONE_SNAMPMATIC_STATE(DRONE_SNAPMATIC_STATE_CLEANUP)
	ENDIF
ENDPROC

PROC MAINTAIN_DRONE_SNAPMATIC()
	IF IS_DRONE_STARTED_FROM_ARENA_WARS_TWO()
	OR IS_DRONE_STARTED_FROM_TRUCK()
	OR IS_DRONE_STARTED_FROM_HEIST_FREEMODE()
	OR IS_DRONE_STARTED_FROM_CASINO_HEIST_MISSON()
		SWITCH sDroneControl.eDroneSnapmaticState
			CASE DRONE_SNAPMATIC_STATE_IDLE
				PROCESS_SNAPMATIC_IDLE_STATE()
			BREAK
			CASE DRONE_SNAPMATIC_STATE_INIT
				PROCESS_SNAPMATIC_INIT_STATE()
			BREAK
			CASE DRONE_SNAPMATIC_STATE_ACTIVE
				PROCESS_SNAPMATIC_ACTIVE_STATE()
			BREAK
			CASE DRONE_SNAPMATIC_STATE_CLEANUP
				PROCESS_SNAPMATIC_CLEANUP_STATE()
			BREAK
		ENDSWITCH	
	ENDIF
ENDPROC

PROC MAINTAIN_MISSILE_PARTICLE_FX()
	
	IF NOT IS_DRONE_USED_AS_GUIDED_MISSILE()
		EXIT
	ENDIF
	
	STRING sMissileParticleFX = "scr_xs_props"
	
	REQUEST_NAMED_PTFX_ASSET(sMissileParticleFX)
	
	IF HAS_NAMED_PTFX_ASSET_LOADED(sMissileParticleFX)
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sDroneControl.cameraProp)
			IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(sDroneControl.missilePFX[NATIVE_TO_INT(PLAYER_ID())])
				USE_PARTICLE_FX_ASSET(sMissileParticleFX)
				#IF IS_DEBUG_BUILD
				IF droneDebugStruct.bActivateVFXDebug
					sDroneControl.missilePFX[NATIVE_TO_INT(PLAYER_ID())] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_xs_guided_missile_trail", NET_TO_OBJ(sDroneControl.cameraProp)
																			, <<droneDebugStruct.fMissileVFXx, droneDebugStruct.fMissileVFXy, droneDebugStruct.fMissileVFXz>>, <<0.0, 0.0, 0.0>>, droneDebugStruct.fMissileVFXScale)
				ELSE
				#ENDIF
					IF IS_DRONE_STARTED_FROM_ARENA_WARS_MISSILE()
						sDroneControl.missilePFX[NATIVE_TO_INT(PLAYER_ID())] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_xs_guided_missile_trail", NET_TO_OBJ(sDroneControl.cameraProp), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
					ELSE
						sDroneControl.missilePFX[NATIVE_TO_INT(PLAYER_ID())] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_xs_guided_missile_trail", NET_TO_OBJ(sDroneControl.cameraProp), <<0.0, 0.250, 0.0>>, <<0.0, 0.0, 0.0>>, 3.0)
					ENDIF
				#IF IS_DEBUG_BUILD
				ENDIF
				#ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
				IF droneDebugStruct.bResetMissileVFX
					STOP_PARTICLE_FX_LOOPED(sDroneControl.missilePFX[NATIVE_TO_INT(PLAYER_ID())])
					droneDebugStruct.bResetMissileVFX = FALSE
				ENDIF
				#ENDIF
			ENDIF
		ENDIF	
	ENDIF	
ENDPROC 

PROC MAINTAIN_MISSILE_DRONE_COLLISION()
	IF IS_DRONE_STARTED_FROM_ARENA_WARS_MISSILE()
	OR IS_DRONE_STARTED_FROM_ARENA_WARS_ONE()
	OR IS_DRONE_STARTED_FROM_ARENA_WARS_TWO()
	#IF FEATURE_HEIST_ISLAND
	OR IS_DRONE_STARTED_FROM_SUBMARINE_MISSILE()
	#ENDIF
		
		INT iTime = 1000
		IF IS_DRONE_STARTED_FROM_ARENA_WARS_ONE()
		OR IS_DRONE_STARTED_FROM_ARENA_WARS_TWO()
			iTime = 7000
		#IF FEATURE_HEIST_ISLAND
		ELIF IS_DRONE_STARTED_FROM_SUBMARINE_MISSILE()
			iTime = 1100
		#ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(sDroneControl.iLocalBS, DRONE_LOCAL_BS_TURN_ON_DRONE_COLLISION)
			IF DOES_CAM_EXIST(sDroneControl.droneCamera)
			AND IS_CAM_RENDERING(sDroneControl.droneCamera)
				IF IS_ENTITY_ALIVE(GET_LOCAL_DRONE_PROP_OBJ_INDEX())
					IF HAS_NET_TIMER_EXPIRED(sDroneControl.sMissileDroneCollision, iTime)
						SET_ENTITY_COMPLETELY_DISABLE_COLLISION(GET_LOCAL_DRONE_PROP_OBJ_INDEX(), TRUE)
						SET_ENTITY_COLLISION(GET_LOCAL_DRONE_PROP_OBJ_INDEX(), TRUE)
						SET_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_TURN_ON_DRONE_COLLISION)
						PRINTLN("[AM_MP_DRONE] - MAINTAIN_MISSILE_DRONE_COLLISION DRONE_LOCAL_BS_TURN_ON_DRONE_COLLISION TRUE")
					ENDIF
				ENDIF	
			ENDIF	
		ENDIF
	ENDIF	
ENDPROC

PROC MAINTAIN_EMP_COOL_DOWN()
	IF sDroneControl.bDroneEMPCoolDown
		IF HAS_NET_TIMER_STARTED(sDroneControl.sStunGunTimer)
			INT iDronePrimaryWeaponTimer
			
			IF IS_DRONE_STARTED_FROM_ARENA_WARS_ONE()
				iDronePrimaryWeaponTimer = g_sMPTunables.iAW_DRONE_EMP_COOLDOWN_TIME
			ELSE
				iDronePrimaryWeaponTimer = g_sMPTunables.iBB_TERRORBYTE_DRONE_SHOCK_COOLDOWN_TIME 
			ENDIF
			
			IF HAS_NET_TIMER_EXPIRED(sDroneControl.sStunGunTimer, iDronePrimaryWeaponTimer)
				
				RESET_NET_TIMER(sDroneControl.sStunGunTimer)
				sDroneControl.iStunRechargerFill  	= 100

				CLEAR_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_CAMERA_STUN_GUN)
				CLEAR_BIT(sDroneControl.iLocalBS,DRONE_LOCAL_BS_CAMERA_EMP)	
				
				CLEAR_BIT(sDroneControl.iLocalBS, DRONE_LOCAL_BS_PLAYER_FIRED_STUN)
				sDroneControl.bDroneEMPCoolDown = FALSE
				PRINTLN("[AM_MP_DRONE] - MAINTAIN_EMP_COOL_DOWN sDroneControl.bDroneEMPCoolDown = FALSE")
			ENDIF
		ENDIF	
	ENDIF
ENDPROC		

PROC MAINTAIN_PLAYER_STAGGER_VALUE()
	sDroneControl.iPlayerDetectionStagger += 1
	IF sDroneControl.iPlayerDetectionStagger >= NUM_NETWORK_PLAYERS
		sDroneControl.iPlayerDetectionStagger = 0
	ENDIF
ENDPROC	

FUNC BOOL IS_DRONE_SYNC_SCENE_EVENT_FIRED_ON_PLAYER_ANIM(DRONE_ANIMATION_NAME eClip, STRING fTag)
	UNUSED_PARAMETER(eClip)
	//TEXT_LABEL_63 sClip = GET_DRONE_ANIM_NAME(eClip)
	INT iLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sDroneControl.iDroneSyncScene)
	IF iLocalSceneID != -1
		//IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), sDroneControl.sAnimDicName, sClip)
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSceneID)
				IF HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), GET_HASH_KEY(fTag))
				OR HAS_ANIM_EVENT_FIRED(NET_TO_OBJ(sDroneControl.phoneObject), GET_HASH_KEY(fTag))
				OR HAS_ANIM_EVENT_FIRED(NET_TO_OBJ(sDroneControl.cameraProp), GET_HASH_KEY(fTag))
					PRINTLN("[AM_MP_DRONE] -  IS_DRONE_SYNC_SCENE_EVENT_FIRED_ON_PLAYER_ANIM  true")
					RETURN TRUE
				ENDIF
			ENDIF
		//ENDIF	
	ENDIF
	RETURN FALSE 
ENDFUNC

PROC MAINTAIN_START_DRONE_ANIMATION()
	
	IF IS_DRONE_CLEANING_UP()
	OR !SHOULD_START_DRONE_FROM_PLAYER_PHONE()
		EXIT
	ENDIF	
	
	SWITCH sDroneControl.iDroneAnimStage
		CASE 0
			IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sDroneControl.phoneObject)
				MODEL_NAMES phoneModel
				phoneModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_CH_Phone_ING_01a"))
				IF REQUEST_LOAD_MODEL(phoneModel)
					IF CAN_RESERVE_NETWORK_OBJECTS_FOR_THIS_SCRIPT(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE, RESERVATION_LOCAL_ONLY) + 1, FALSE, TRUE)
						IF NOT IS_BIT_SET(sDroneControl.iLocalBSTwo, DRONE_LOCAL_BS_TWO_RESERVE_PHONE_OBJECT)
							RESERVE_LOCAL_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE, RESERVATION_LOCAL_ONLY) + 1)
							SET_BIT(sDroneControl.iLocalBSTwo, DRONE_LOCAL_BS_TWO_RESERVE_PHONE_OBJECT)
							PRINTLN("[AM_MP_DRONE] - MAINTAIN_START_DRONE_ANIMATION DRONE_LOCAL_BS_TWO_RESERVE_PHONE_OBJECT TRUE")	
						ENDIF
						
						IF CAN_REGISTER_MISSION_OBJECTS(1)
							IF CREATE_NET_OBJ(sDroneControl.phoneObject, phoneModel, GET_ENTITY_COORDS(PLAYER_PED_ID()), FALSE, TRUE, TRUE, FALSE, DEFAULT, DEFAULT, TRUE)

								SET_NETWORK_ID_ALWAYS_EXISTS_FOR_PLAYER(sDroneControl.phoneObject, PLAYER_ID(), TRUE)
								
								INT iBone
								iBone = GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_PH_R_HAND)
								ATTACH_ENTITY_TO_ENTITY(NET_TO_OBJ(sDroneControl.phoneObject), PLAYER_PED_ID(), iBone, <<0, 0, 0>>, <<0, 0, 0>>, TRUE, TRUE)

								SET_MODEL_AS_NO_LONGER_NEEDED(phoneModel)
								PRINTLN("[AM_MP_DRONE] - MAINTAIN_START_DRONE_ANIMATION creating phone prop")	
							ENDIF	
						ENDIF
					ELSE
						PRINTLN("[AM_MP_DRONE] - MAINTAIN_START_DRONE_ANIMATION CAN_RESERVE_NETWORK_OBJECTS_FOR_THIS_SCRIPT false")	
					ENDIF	
				ENDIF	
			ELSE
				sDroneControl.iDroneAnimStage = 1
				PRINTLN("[AM_MP_DRONE] - MAINTAIN_START_DRONE_ANIMATION sDroneControl.iDroneAnimStage = 1")
			ENDIF
		BREAK
		CASE 1
			IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) != WAITING_TO_START_TASK
			AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) != PERFORMING_TASK
				PERFORM_DRONE_SYNC_SCENE(DRONE_ANIM_ENTER, FALSE)
				sDroneControl.iDroneAnimStage = 2
				PRINTLN("[AM_MP_DRONE] - MAINTAIN_START_DRONE_ANIMATION sDroneControl.iDroneAnimStage = 2")
			ENDIF	
		BREAK	
		CASE 2	
			
			IF IS_DRONE_SYNC_SCENE_EVENT_FIRED_ON_PLAYER_ANIM(DRONE_ANIM_ENTER, "CREATE")
				PLAY_FLIGHT_LOOP_SOUND()
				IF TAKE_CONTROL_OF_NET_ID(sDroneControl.phoneObject)
					IF NOT IS_ENTITY_VISIBLE(NET_TO_OBJ(sDroneControl.phoneObject))
						SET_ENTITY_VISIBLE(NET_TO_OBJ(sDroneControl.phoneObject), TRUE)
						
					ENDIF	
				ENDIF
				IF TAKE_CONTROL_OF_NET_ID(sDroneControl.cameraProp)
					IF NOT IS_ENTITY_VISIBLE(NET_TO_OBJ(sDroneControl.cameraProp))
						SET_ENTITY_VISIBLE(NET_TO_OBJ(sDroneControl.cameraProp), TRUE)
						SET_ENTITY_VISIBLE(sDroneControl.fakeVeh, FALSE)
					ENDIF	
				ENDIF	
			ENDIF
			
			IF HAS_DRONE_ANIMATION_CLIP_FINISHED(TRUE)
				ATTACH_ENTITY_TO_ENTITY(sDroneControl.fakeVeh, NET_TO_OBJ(sDroneControl.cameraProp), -1, <<0,0,-0.8>>, <<0,0,0>>)
				SET_ENTITY_VISIBLE(sDroneControl.fakeVeh, FALSE)
				
				FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(NET_TO_ENT(sDroneControl.cameraProp))
				PLAY_OBJECT_AUTO_START_ANIM(NET_TO_OBJ(sDroneControl.cameraProp))
				
				MAINTAIN_DRONE_BLIP()
				DRAW_DRONE_SCALEFORM()
				UPDATE_DRONE_MINIMAP(TRUE)
				UPDATE_DRONE_CAMERA()
				UPDATE_DRONE_MOVEMENT()
				
				START_DRONE_AUDIO_SCENE()
				
				IF HAS_SOUND_FINISHED(sDroneControl.iStartUpSound)
					sDroneControl.iStartUpSound = GET_SOUND_ID()
					PLAY_SOUND_FRONTEND(sDroneControl.iStartUpSound, "HUD_Startup", sDroneControl.sSoundSet)
				ENDIF
				
				RENDER_SCRIPT_CAMS(TRUE, FALSE, 0 , TRUE, TRUE)

				#IF IS_DEBUG_BUILD
				IF g_bForceStopRenderDroneCam
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
				ENDIF
				#ENDIF
				
				sDroneControl.droneUsageStat = GET_NETWORK_TIME()
				
				#IF FEATURE_GEN9_EXCLUSIVE
				SET_AUDIO_RAIN_HAPTIC_FLAG(TRUE)
				#ENDIF
				
				SET_DRONE_STATE(DRONE_STATE_FLYING)
				PERFORM_DRONE_SYNC_SCENE(DRONE_ANIM_BASE, TRUE)
				
				sDroneControl.iDroneAnimStage = 3
				PRINTLN("[AM_MP_DRONE] - MAINTAIN_START_DRONE_ANIMATION sDroneControl.iDroneAnimStage = 3")
			ENDIF
		BREAK
		CASE 3
			IF HAS_DRONE_ANIMATION_CLIP_FINISHED(FALSE)
				INT iRand
				iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
				SWITCH iRand 
					CASE 0		
						PERFORM_DRONE_SYNC_SCENE(DRONE_ANIM_USE_01, FALSE)	
						sDroneControl.eCurrentAnimation = DRONE_ANIM_USE_01
					BREAK
					CASE 1		
						PERFORM_DRONE_SYNC_SCENE(DRONE_ANIM_USE_02, FALSE)	
						sDroneControl.eCurrentAnimation = DRONE_ANIM_USE_02
					BREAK
					CASE 2		
						PERFORM_DRONE_SYNC_SCENE(DRONE_ANIM_USE_03, FALSE)	
						sDroneControl.eCurrentAnimation = DRONE_ANIM_USE_03
					BREAK
				ENDSWITCH	
				sDroneControl.iDroneAnimStage = 4
				PRINTLN("[AM_MP_DRONE] - MAINTAIN_START_DRONE_ANIMATION sDroneControl.iDroneAnimStage = 4")
			ENDIF
		BREAK
		CASE 4
			IF HAS_DRONE_ANIMATION_CLIP_FINISHED(FALSE)
				PERFORM_DRONE_SYNC_SCENE(DRONE_ANIM_BASE, TRUE)
				sDroneControl.iDroneAnimStage = 3
				PRINTLN("[AM_MP_DRONE] - MAINTAIN_START_DRONE_ANIMATION sDroneControl.iDroneAnimStage = 3 from 4")
			ENDIF
		BREAK
	ENDSWITCH	
	
	#IF IS_DEBUG_BUILD
	IF g_bForceRenderDroneCam
		IF DOES_CAM_EXIST(sDroneControl.droneCamera)
		AND !IS_CAM_RENDERING(sDroneControl.droneCamera)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			g_bForceRenderDroneCam = FALSE
		ENDIF	
	ENDIF
	#ENDIF
ENDPROC		

#IF FEATURE_HEIST_ISLAND
PROC SET_SUBMARINE_MISSILE_INITIAL_COORDS_AND_ROT()
	INT iBone
	STRING sBoneName = GET_SUBMARINE_MISSILE_LAUNCH_BONE_NAME(PLAYER_ID())
	VECTOR vInitialStartCoord
	IF g_iShouldLaunchSubmarineSeat = 1
		iBone = GET_ENTITY_BONE_INDEX_BY_NAME(GET_SUBMARINE_INDEX_PED_IS_IN_INTERIOR(), sBoneName)
		vInitialStartCoord = GET_ENTITY_BONE_POSTION(GET_SUBMARINE_INDEX_PED_IS_IN_INTERIOR(), iBone)
		sDroneControl.vDroneInitialStartCoord = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vInitialStartCoord, GET_ENTITY_HEADING(GET_SUBMARINE_INDEX_PED_IS_IN_INTERIOR()), <<0.0, 0.0, -2.6>>)
		PRINTLN("[AM_MP_DRONE] - SET_SUBMARINE_MISSILE_INITIAL_COORDS - submarine g_iShouldLaunchSubmarineSeat = 1 sDroneControl.vDroneInitialStartCoord: ", sDroneControl.vDroneInitialStartCoord)
	ELIF g_iShouldLaunchSubmarineSeat = 2
		iBone = GET_ENTITY_BONE_INDEX_BY_NAME(GET_SUBMARINE_INDEX_PED_IS_IN_INTERIOR(), sBoneName)																						
		vInitialStartCoord = GET_ENTITY_BONE_POSTION(GET_SUBMARINE_INDEX_PED_IS_IN_INTERIOR(), iBone)
		sDroneControl.vDroneInitialStartCoord = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vInitialStartCoord, GET_ENTITY_HEADING(GET_SUBMARINE_INDEX_PED_IS_IN_INTERIOR()), <<0.0, 0.0, -2.6>>)
		PRINTLN("[AM_MP_DRONE] - SET_SUBMARINE_MISSILE_INITIAL_COORDS - submarine g_iShouldLaunchSubmarineSeat = 2 sDroneControl.vDroneInitialStartCoord: ", sDroneControl.vDroneInitialStartCoord)
	ENDIF
	
	VECTOR vSubmarineRot = GET_ENTITY_ROTATION(GET_SUBMARINE_INDEX_PED_IS_IN_INTERIOR())
	SET_ENTITY_COORDS(NET_TO_OBJ(sDroneControl.cameraProp), sDroneControl.vDroneInitialStartCoord)

	SET_DRONE_COORD_AND_ROTATION(sDroneControl.vDroneInitialStartCoord, <<vSubmarineRot.x + 90, 180.0, 0.0>>)
	
	VECTOR vCoord = GET_ENTITY_COORDS(GET_SUBMARINE_INDEX_PED_IS_IN_INTERIOR())
	VECTOR vTarget = <<0,0,0>>
	sDroneControl.fSubmarineMissileHeading = GET_HEADING_BETWEEN_VECTORS_2D(vTarget, vCoord)
	
	SET_ENTITY_HEADING(NET_TO_OBJ(sDroneControl.cameraProp), sDroneControl.fSubmarineMissileHeading)
	SET_ENTITY_ROTATION(NET_TO_OBJ(sDroneControl.cameraProp), <<vSubmarineRot.x - 90, 0.0, sDroneControl.fSubmarineMissileHeading>>)
	
	IF HAS_SOUND_FINISHED(sDroneControl.iMissileLaunchSound)
		sDroneControl.iMissileLaunchSound = GET_SOUND_ID()
		PLAY_SOUND_FROM_ENTITY(sDroneControl.iMissileLaunchSound, "Missile_Launch", NET_TO_OBJ(sDroneControl.cameraProp), sDroneControl.sSoundSet, TRUE)
	ENDIF
ENDPROC

PROC MAINTAIN_START_MISSILE_LAUNCH_VFX()

	STRING sMissileLaunchParticleFX = "scr_ih_sub"
	
	REQUEST_NAMED_PTFX_ASSET(sMissileLaunchParticleFX)
	
	IF HAS_NAMED_PTFX_ASSET_LOADED(sMissileLaunchParticleFX)
		IF IS_ENTITY_ALIVE(GET_SUBMARINE_INDEX_PED_IS_IN_INTERIOR())	
		AND PLAYER_ID() != INVALID_PLAYER_INDEX()
			IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(sDroneControl.missileLaunchPFX[NATIVE_TO_INT(PLAYER_ID())])
				USE_PARTICLE_FX_ASSET(sMissileLaunchParticleFX)
				
				STRING sBoneName = GET_SUBMARINE_MISSILE_LAUNCH_BONE_NAME(PLAYER_ID())
				INT iBone = GET_ENTITY_BONE_INDEX_BY_NAME(GET_SUBMARINE_INDEX_PED_IS_IN_INTERIOR(), sBoneName)
				
				#IF IS_DEBUG_BUILD
				IF droneDebugStruct.bActivateVFXDebug
					sDroneControl.missileLaunchPFX[NATIVE_TO_INT(PLAYER_ID())] = START_PARTICLE_FX_LOOPED_ON_ENTITY_BONE("scr_ih_sub_missile_launch", GET_SUBMARINE_INDEX_PED_IS_IN_INTERIOR()
																			, <<droneDebugStruct.fMissileVFXx, droneDebugStruct.fMissileVFXy, droneDebugStruct.fMissileVFXz>>, <<0.0, 0.0, 0.0>>, iBone, droneDebugStruct.fMissileVFXScale)
				ELSE
				#ENDIF
					sDroneControl.missileLaunchPFX[NATIVE_TO_INT(PLAYER_ID())] = START_PARTICLE_FX_LOOPED_ON_ENTITY_BONE("scr_ih_sub_missile_launch", GET_SUBMARINE_INDEX_PED_IS_IN_INTERIOR()
																					, GET_MISSILE_VFX_OFFSET(PLAYER_ID()), <<0.0, 0.0, 0.0>>, iBone, 5.0)
				#IF IS_DEBUG_BUILD
				ENDIF
				#ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
				IF droneDebugStruct.bResetMissileLanuchVFX
					STOP_PARTICLE_FX_LOOPED(sDroneControl.missileLaunchPFX[NATIVE_TO_INT(PLAYER_ID())])
					droneDebugStruct.bResetMissileLanuchVFX = FALSE
				ENDIF
				#ENDIF

				START_NET_TIMER(sDroneControl.vMissileLaunchVFXTimer[NATIVE_TO_INT(PLAYER_ID())])
			ENDIF	
			
			IF HAS_NET_TIMER_STARTED(sDroneControl.vMissileLaunchVFXTimer[NATIVE_TO_INT(PLAYER_ID())])
				IF HAS_NET_TIMER_EXPIRED(sDroneControl.vMissileLaunchVFXTimer[NATIVE_TO_INT(PLAYER_ID())], 1000)
					SET_PARTICLE_FX_LOOPED_EVOLUTION(sDroneControl.missileLaunchPFX[NATIVE_TO_INT(PLAYER_ID())], "flame", 0.15)
					SET_SUBMARINE_MISSILE_INITIAL_COORDS_AND_ROT()
					
					UPDATE_MISSILE_DRONE_CAMERA()
					UPDATE_MISSILE_DRONE_MOVEMENT()
					
					IF NOT IS_ENTITY_VISIBLE(NET_TO_OBJ(sDroneControl.cameraProp))
						SET_ENTITY_VISIBLE(NET_TO_OBJ(sDroneControl.cameraProp), TRUE)
					ENDIF

					START_DRONE_AUDIO_SCENE()
						
					IF HAS_SOUND_FINISHED(sDroneControl.iStartUpSound)
						sDroneControl.iStartUpSound = GET_SOUND_ID()
						PLAY_SOUND_FRONTEND(sDroneControl.iStartUpSound, "HUD_Startup", sDroneControl.sSoundSet)
					ENDIF
					
					MAINTAIN_DRONE_BLIP()
					DRAW_MISSILE_DRONE_SCALEFORM()
					
					UPDATE_DRONE_MINIMAP(TRUE)
					
					CLEAR_TIMECYCLE_MODIFIER()
					SET_TIMECYCLE_MODIFIER("IslandPeriscope")
					
					RENDER_SCRIPT_CAMS(TRUE, FALSE, 0 , TRUE, TRUE)
					
					sDroneControl.droneUsageStat = GET_NETWORK_TIME()

					SET_ENTITY_INVINCIBLE(NET_TO_OBJ(sDroneControl.cameraProp), FALSE)
					
					#IF FEATURE_GEN9_EXCLUSIVE
					SET_AUDIO_RAIN_HAPTIC_FLAG(TRUE)
					#ENDIF
					
					SET_DRONE_STATE(DRONE_STATE_FLYING)
				ENDIF	
			ENDIF	
		ELSE		
			SET_DRONE_CLEANING_UP(TRUE)
			SET_DRONE_STATE(DRONE_STATE_CLEANUP)
			PRINTLN("[AM_MP_DRONE] - MAINTAIN_START_MISSILE_LAUNCH_VFX submarine is dead")
		ENDIF	
	ENDIF	
ENDPROC
#ENDIF

PROC MAINTAIN_DRONE_GLOBAL_BD_OBJECT_INDEX()
	IF sDroneControl.iPlayerDetectionStagger != -1
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(playerBD[sDroneControl.iPlayerDetectionStagger].netID_remoteDrone)
			IF GlobalplayerBD_FM_4[NATIVE_TO_INT(PLAYER_ID())].objID_remoteDrone[sDroneControl.iPlayerDetectionStagger] != NET_TO_OBJ(playerBD[sDroneControl.iPlayerDetectionStagger].netID_remoteDrone)
				GlobalplayerBD_FM_4[NATIVE_TO_INT(PLAYER_ID())].objID_remoteDrone[sDroneControl.iPlayerDetectionStagger] = NET_TO_OBJ(playerBD[sDroneControl.iPlayerDetectionStagger].netID_remoteDrone)
			ENDIF	
		ENDIF
	ENDIF	
ENDPROC

PROC MAINTAIN_PLAYER_TARGETING_FLAG()
	IF IS_DRONE_STARTED_FROM_HEIST_FREEMODE()
		IF SHOULD_START_DRONE_FROM_PLAYER_PHONE()
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
				SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_CannotBeTargetedByAI, TRUE)
			ENDIF	
		ENDIF
	ENDIF	
ENDPROC

#IF FEATURE_HEIST_ISLAND
PROC MAINTAIN_SUBMARINE_MISSILE_VFX()
	IF NOT IS_DRONE_STARTED_FROM_SUBMARINE_MISSILE()
	OR PLAYER_ID() = INVALID_PLAYER_INDEX()
		EXIT	
	ENDIF
	
	IF HAS_NET_TIMER_EXPIRED(sDroneControl.vMissileLaunchVFXTimer[NATIVE_TO_INT(PLAYER_ID())], 2500)
		IF DOES_PARTICLE_FX_LOOPED_EXIST(sDroneControl.missileLaunchPFX[NATIVE_TO_INT(PLAYER_ID())])
			STOP_PARTICLE_FX_LOOPED(sDroneControl.missileLaunchPFX[NATIVE_TO_INT(PLAYER_ID())])
		ENDIF
		RESET_NET_TIMER(sDroneControl.vMissileLaunchVFXTimer[NATIVE_TO_INT(PLAYER_ID())])
	ENDIF
ENDPROC		
#ENDIF

PROC RUN_MAIN_CLIENT_LOGIC()
	IF !IS_DRONE_STATE(DRONE_STATE_REMOTE_DRONE)
	
		MAINTAIN_DRONE_ENVIROMENT_CONTROL()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_DRONE_ENVIROMENT_CONTROL")
		#ENDIF	
		#ENDIF
		
		MAINTAIN_FAILED_COOL_DOWN_BROADCAST()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_FAILED_COOL_DOWN_BROADCAST")
		#ENDIF	
		#ENDIF
		
		MAINTAIN_COOL_DOWN_BROADCAST_DATA_WAIT()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_COOL_DOWN_BROADCAST_DATA_WAIT")
		#ENDIF	
		#ENDIF
		
		PROCESS_DRONE_PLAYER_CONTROLS()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("PROCESS_DRONE_PLAYER_CONTROLS")
		#ENDIF	
		#ENDIF

		UPDATE_LOCAL_VARIABLES()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("UPDATE_LOCAL_VARIABLES")
		#ENDIF	
		#ENDIF
	ENDIF
	
	PROCESS_DRONE_EVENTS()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("PROCESS_DRONE_EVENTS")
	#ENDIF	
	#ENDIF
	
	#IF FEATURE_HEIST_ISLAND
	MAINTAIN_REMOTE_MISSILE_LAUNCH_PARTICLE_PFX()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_REMOTE_MISSILE_LAUNCH_PARTICLE_PFX")
	#ENDIF	
	#ENDIF
	#ENDIF
	
	PLAY_REMOTE_FLIGHT_LOOP_SOUND()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("PLAY_REMOTE_FLIGHT_LOOP_SOUND")
	#ENDIF	
	#ENDIF
	
	MAINTAIN_REMOTE_MISSILE_PARTICLE_PFX()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_REMOTE_MISSILE_PARTICLE_PFX")
	#ENDIF	
	#ENDIF
	
	// State DRONE_STATE_IDLE: wait for flag to start flying drone
	IF IS_DRONE_STATE(DRONE_STATE_IDLE)
	
		MAINTAIN_DRONE_TRIGGER()
	
	// State DRONE_STATE_INIT: Initialize drone prop, dummy vehicle, camera and etc.
	ELIF IS_DRONE_STATE(DRONE_STATE_INIT)
		
		IF !IS_LOCAL_PLAYER_WALKING_OUT_OF_SIMPLE_INTERIOR()
			MAINTAIN_PROP_CREATION()
			MAINTAIN_DRONE_CAMERA_CREATION()
		ELSE
			SET_DRONE_CLEANING_UP(TRUE)
			SET_DRONE_STATE(DRONE_STATE_CLEANUP)
		ENDIF
	
	// State DRONE_STATE_START_ANIMATION: maintain drone start animation
	ELIF IS_DRONE_STATE(DRONE_STATE_START_ANIMATION)
		MAINTAIN_START_DRONE_ANIMATION()
	#IF FEATURE_HEIST_ISLAND
	// State DRONE_STATE_LAUNCH_VFX: VFX before we fire the missile
	ELIF IS_DRONE_STATE(DRONE_STATE_LAUNCH_VFX)
		MAINTAIN_START_MISSILE_LAUNCH_VFX()
	#ENDIF
	// State DRONE_STATE_FLYING: everything after player starts flying drone maintained here
	ELIF IS_DRONE_STATE(DRONE_STATE_FLYING)
		
		#IF FEATURE_HEIST_ISLAND
		MAINTAIN_SUBMARINE_MISSILE_VFX()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_MISSILE_DRONE_COLLISION")
		#ENDIF	
		#ENDIF
		#ENDIF
		
		MAINTAIN_MISSILE_DRONE_COLLISION()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_MISSILE_DRONE_COLLISION")
		#ENDIF	
		#ENDIF

		MAINTAIN_DRONE_EMP_PLAYER_DETECTION()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_DRONE_EMP_PLAYER_DETECTION")
		#ENDIF	
		#ENDIF
		
		PLAYER_DRONE_RESET_FLAGS()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("PLAYER_DRONE_RESET_FLAGS")
		#ENDIF	
		#ENDIF
		
		PLAY_FLIGHT_LOOP_SOUND()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("PLAY_FLIGHT_LOOP_SOUND")
		#ENDIF	
		#ENDIF
		
		PLAY_HUD_LOOP_SOUND()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("PLAY_HUD_LOOP_SOUND")
		#ENDIF	
		#ENDIF
		
		UPDATE_PAUSE_MENU_MAP()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("UPDATE_PAUSE_MENU_MAP")
		#ENDIF	
		#ENDIF
		
		UPDATE_DRONE_MOVEMENT()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("UPDATE_DRONE_MOVEMENT")
		#ENDIF	
		#ENDIF
		
		UPDATE_MISSILE_DRONE_MOVEMENT()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("UPDATE_MISSILE_DRONE_MOVEMENT")
		#ENDIF	
		#ENDIF
		
		PROCESS_DRONE_COLLISION()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("PROCESS_DRONE_COLLISION")
		#ENDIF	
		#ENDIF
		
		MAINTAIN_DRONE_FREEZE_POSITION()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_DRONE_FREEZE_POSITION")
		#ENDIF	
		#ENDIF
		
		MAINTAIN_DRONE_SNAPMATIC()	
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_DRONE_SNAPMATIC")
		#ENDIF	
		#ENDIF
		
		PLAY_OUT_OF_BOUND_ALARM_FOR_DRONE_MISSILE()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("PLAY_OUT_OF_BOUND_ALARM_FOR_DRONE_MISSILE")
		#ENDIF	
		#ENDIF
		
		MAINTAIN_DRONE_BLIP()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_DRONE_BLIP")
		#ENDIF	
		#ENDIF
		
		MAINTAIN_START_DRONE_ANIMATION()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_DRONE_BLIP")
		#ENDIF	
		#ENDIF
		
		UPDATE_MISSILE_DRONE_CAMERA()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("UPDATE_MISSILE_DRONE_CAMERA")
		#ENDIF	
		#ENDIF
		
		////////////////////////////////////////////// *****	PAUSE FUNCTIONALITY AFTER THIS POINT  ***** /////////////////////////////////////////////////
		IF SHOULD_PAUSE_DRONE()
			STOP_DETONATE_SOUND()
			EXIT
		ENDIF
		////////////////////////////////////////////// *****	PAUSE FUNCTIONALITY AFTER THIS POINT  ***** //////////////////////////////////////////////////
		
		MAINTAIN_TARGET_CREATION()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_TARGET_CREATION")
		#ENDIF	
		#ENDIF

		UPDATE_DRONE_CAMERA()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("UPDATE_DRONE_CAMERA")
		#ENDIF	
		#ENDIF

		UPDATE_DRONE_MINIMAP()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("UPDATE_DRONE_MINIMAP")
		#ENDIF	
		#ENDIF
		
		CHECK_RESTRICTED_INTERIOR_FOR_DRONE()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("CHECK_RESTRICTED_INTERIOR_FOR_DRONE")
		#ENDIF	
		#ENDIF
		
		PROCESS_DRONE_COLLISION_SHAPETEST()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("PROCESS_DRONE_COLLISION_SHAPETEST")
		#ENDIF	
		#ENDIF
		
		PROCESS_DRONE_PED_DETECTION_SHAPETEST()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("PROCESS_DRONE_PED_DETECTION_SHAPETEST")
		#ENDIF	
		#ENDIF
		
		PROCESS_DRONE_WEAPONS()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("PROCESS_DRONE_WEAPONS")
		#ENDIF	
		#ENDIF
		
		PROCESS_TARGET_DAMAGE()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("PROCESS_TARGET_DAMAGE")
		#ENDIF	
		#ENDIF
		
		PROCESS_DRONE_EXIT()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("PROCESS_DRONE_EXIT")
		#ENDIF	
		#ENDIF

		DRAW_DRONE_HUD()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("DRAW_DRONE_HUD")
		#ENDIF	
		#ENDIF
		
		MAINTAIN_FAKE_PED_CREATION()	
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_FAKE_PED_CREATION")
		#ENDIF	
		#ENDIF
		
		MAINTAIN_MISSILE_PARTICLE_FX()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_MISSILE_PARTICLE_FX")
		#ENDIF	
		#ENDIF
		
		MAINTAIN_PLAYER_TARGETING_FLAG()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_PLAYER_TARGETING_FLAG")
		#ENDIF	
		#ENDIF
		
	// State DRONE_STATE_FUZZ_CAM: Camera fuzz PFX before screen clean up for drone user
	ELIF IS_DRONE_STATE(DRONE_STATE_FUZZ_CAM)
	
		RETURN_TO_PLAYER_CONTROLS()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("RETURN_TO_PLAYER_CONTROLS")
		#ENDIF	
		#ENDIF
		
	// State DRONE_STATE_CLEANUP: clean up drone script
	ELIF IS_DRONE_STATE(DRONE_STATE_CLEANUP)
	
		MAINTAIN_DRONE_CLEANUP()
	
	ENDIF

	IF !IS_DRONE_STATE(DRONE_STATE_FLYING)
	AND NOT IS_DRONE_STATE(DRONE_STATE_REMOTE_DRONE)
		MAINTAIN_EMP_COOL_DOWN()
	ENDIF
	
	MAINTAIN_REQUEST_DRONE_REMOTE_SCRIPT()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_REQUEST_DRONE_REMOTE_SCRIPT")
	#ENDIF	
	#ENDIF
	
	MAINTAIN_DRONE_GLOBAL_BD_OBJECT_INDEX()
	MAINTAIN_PLAYER_STAGGER_VALUE()
ENDPROC

PROC RUN_MAIN_SERVER_LOGIC()
	
ENDPROC

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡    MAIN LOOP    ╞═════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

SCRIPT  (DRONE_LAUNCHER_STRUCT missionScriptArgs) 
	
	IF NETWORK_IS_GAME_IN_PROGRESS() 
		SCRIPT_INITIALISE(missionScriptArgs)	
	ELSE
		SCRIPT_CLEANUP()
	ENDIF
	
	// Main loop
	WHILE TRUE

		MP_LOOP_WAIT_ZERO()
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		SCRIPT_PROFILER_START_OF_FRAME() 
		#ENDIF
		#ENDIF	
			
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			PRINTLN("[AM_MP_DRONE] - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE = TRUE")
			SCRIPT_CLEANUP()
		ENDIF
		
		IF SHOULD_KILL_THIS_SCRIPT()
			PRINTLN("[AM_MP_DRONE] - SHOULD_KILL_THIS_SCRIPT = TRUE")
			SCRIPT_CLEANUP()
		ENDIF
		
		RUN_MAIN_CLIENT_LOGIC()

		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			RUN_MAIN_SERVER_LOGIC()
		ENDIF
		
		#IF IS_DEBUG_BUILD
		UPDATE_DEBUG_WIDGETS()
		
		#IF SCRIPT_PROFILER_ACTIVE
		SCRIPT_PROFILER_END_OF_FRAME() 
		#ENDIF
		#ENDIF

	ENDWHILE
ENDSCRIPT



