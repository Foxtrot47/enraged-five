//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        AM_MP_ARCADE_PEDS.sch																					//
// Description: Script for maintaining the Arcade peds.																	//
// Written by:  Online Technical Team: Scott Ranken																		//
// Date:  		05/09/19																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "net_arcade_peds_header.sch"
USING "website_public.sch"
USING "net_simple_cutscene_common.sch"
USING "rc_helper_functions.sch"
#IF FEATURE_CASINO_HEIST

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ VARIABLES ╞══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

ARCADE_PED_DATA PedData
SERVER_BROADCAST_DATA ServerBD

//╒══════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════════╡ CLEANUP ╞═══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

PROC DELETE_ARCADE_PED(PED_DATA &Data)
	IF DOES_ENTITY_EXIST(Data.PedID)
		DELETE_PED(Data.PedID)
	ENDIF
	
	INT iObject
	REPEAT MAX_NUM_ARCADE_PED_OBJECTS iObject
		IF DOES_ENTITY_EXIST(Data.ObjectID[iObject])
			DELETE_OBJECT(Data.ObjectID[iObject])
		ENDIF
	ENDREPEAT
ENDPROC

PROC CLEANUP_ALL_ARCADE_PEDS()
	INT iPed
	REPEAT MAX_NUM_ARCADE_PEDS iPed
		DELETE_ARCADE_PED(PedData.ArcadePed[iPed])
	ENDREPEAT
ENDPROC

PROC CLEANUP_ARCADE_STANDALONE_PROPS()
	IF DOES_ENTITY_EXIST(PedData.objBroom)
		DELETE_OBJECT(PedData.objBroom)
	ENDIF
	IF DOES_ENTITY_EXIST(PedData.objHackerChair)
		DELETE_OBJECT(PedData.objHackerChair)
	ENDIF
	IF DOES_ENTITY_EXIST(PedData.objWeaponExpertChair)
		DELETE_OBJECT(PedData.objWeaponExpertChair)
	ENDIF
ENDPROC

PROC SCRIPT_CLEANUP()
	IF IS_STREAM_PLAYING()
		PRINTLN("[AM_MP_ARCADE_PEDS] SCRIPT_CLEANUP - Calling STOP_STREAM")
		STOP_STREAM()
	ENDIF

	PRINTLN("[AM_MP_ARCADE_PEDS] SCRIPT_CLEANUP")
	
	g_bInitArcadePedsCreated = FALSE
	g_bSetArcadePedsVisibleAfterCutscene = FALSE
	g_iArcadeJimmySpawnLocation = -1
	
	CLEANUP_ALL_ARCADE_PEDS()
	CLEANUP_ARCADE_STANDALONE_PROPS()
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════╡ REQUEST ASSETS ╞════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE: Unique peds like Jimmy are still to be created. Only patron peds are removed.
FUNC BOOL SHOULD_ARCADE_PED_BE_CREATED_IN_PRIVATE_MODE(INT iPed)
	RETURN (iPed <= 4)
ENDFUNC

FUNC INT GET_OWNER_ARCADE_PED_LEVEL()
	IF serverBD.iOwnedArcadeTypes >= PED_LEVEL_6_OWNED_ARCADE_MACHINE_TYPES
	AND serverBD.iInstalledArcadeMachines >= PED_LEVEL_6_INSTALLED_ARCADE_MACHINES
		RETURN 6
	ELIF serverBD.iOwnedArcadeTypes >= PED_LEVEL_5_OWNED_ARCADE_MACHINE_TYPES
	AND serverBD.iInstalledArcadeMachines >= PED_LEVEL_5_INSTALLED_ARCADE_MACHINES
		RETURN 5
	ELIF serverBD.iOwnedArcadeTypes >= PED_LEVEL_4_OWNED_ARCADE_MACHINE_TYPES
	AND serverBD.iInstalledArcadeMachines >= PED_LEVEL_4_INSTALLED_ARCADE_MACHINES
		RETURN 4
	ELIF serverBD.iOwnedArcadeTypes >= PED_LEVEL_3_OWNED_ARCADE_MACHINE_TYPES
	AND serverBD.iInstalledArcadeMachines >= PED_LEVEL_3_INSTALLED_ARCADE_MACHINES
		RETURN 3
	ELIF serverBD.iOwnedArcadeTypes >= PED_LEVEL_2_OWNED_ARCADE_MACHINE_TYPES
	AND serverBD.iInstalledArcadeMachines >= PED_LEVEL_2_INSTALLED_ARCADE_MACHINES
		RETURN 2
	ELIF serverBD.iOwnedArcadeTypes >= PED_LEVEL_1_OWNED_ARCADE_MACHINE_TYPES
	AND serverBD.iInstalledArcadeMachines >= PED_LEVEL_1_INSTALLED_ARCADE_MACHINES
		RETURN 1
	ENDIF
	RETURN 0
ENDFUNC

FUNC BOOL SHOULD_CREATE_ARCADE_PED(PED_DATA &Data, INT iPed)
	IF Data.bSkipPed
		RETURN FALSE
	ENDIF
	
	IF PedData.bPrivateModeActive 
	AND NOT SHOULD_ARCADE_PED_BE_CREATED_IN_PRIVATE_MODE(iPed)
	AND NOT IS_BIT_SET(PedData.iBS, BS_ARCADE_PEDS_PRIVATE_MODE_CHANGED)
		RETURN FALSE
	ENDIF
	
	IF PedData.bEnteredViaBasement
		IF NOT Data.bBasementPed
			RETURN g_bArcadeSecondaryInteriorReady AND (Data.iLevel <= serverBD.iLevel)
		ENDIF
	ELSE
		IF Data.bBasementPed
			RETURN g_bArcadeSecondaryInteriorReady AND (Data.iLevel <= serverBD.iLevel)
		ENDIF
	ENDIF
	
	RETURN (Data.iLevel <= serverBD.iLevel)
ENDFUNC

FUNC BOOL HAVE_ARCADE_PED_ASSETS_LOADED(PED_DATA &Data, INT iPed)
	IF SHOULD_CREATE_ARCADE_PED(Data, iPed)
		MODEL_NAMES pedModel = GET_ARCADE_PED_MODEL(iPed, Data.bFemale, PedData.eHeistDriver, PedData.eHeistHacker, PedData.eHeistWeaponExpert, Data.iPedModelVariation)
		
		IF IS_MODEL_VALID(pedModel)
			IF HAS_MODEL_LOADED(pedModel)
			AND HAS_ANIM_DICT_LOADED(GET_ARCADE_ANIM_DICTIONARY_FOR_ACTIVITY())
				IF DOES_ARCADE_ACTIVITY_HAVE_PROP(Data)
					INT iObject
					INT iPropsLoaded = 0
					REPEAT MAX_NUM_ARCADE_PED_OBJECTS iObject
						MODEL_NAMES propModel = GET_ARCADE_PROP_FOR_ACTIVITY(Data, iObject)
						IF IS_MODEL_VALID(propModel)
							IF HAS_MODEL_LOADED(propModel)
								iPropsLoaded++
							ENDIF
						ELSE
							iPropsLoaded++
						ENDIF
					ENDREPEAT
					RETURN (iPropsLoaded = MAX_NUM_ARCADE_PED_OBJECTS)
				ELSE
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
		RETURN FALSE
	ENDIF	
	RETURN TRUE
ENDFUNC

PROC REQUEST_ARCADE_PED_ASSETS(PED_DATA &Data, INT iPed)
	IF SHOULD_CREATE_ARCADE_PED(Data, iPed)
		REQUEST_MODEL(GET_ARCADE_PED_MODEL(iPed, Data.bFemale, PedData.eHeistDriver, PedData.eHeistHacker, PedData.eHeistWeaponExpert, Data.iPedModelVariation))
		REQUEST_ANIM_DICT(GET_ARCADE_ANIM_DICTIONARY_FOR_ACTIVITY())
		IF DOES_ARCADE_ACTIVITY_HAVE_PROP(Data)
			INT iObject
			REPEAT MAX_NUM_ARCADE_PED_OBJECTS iObject
				MODEL_NAMES ePropModel = GET_ARCADE_PROP_FOR_ACTIVITY(Data, iObject)
				IF ePropModel != DUMMY_MODEL_FOR_SCRIPT
					REQUEST_MODEL(ePropModel)
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
ENDPROC

FUNC INT GET_MAX_NUM_ARCADE_PEDS()
	IF NOT HAS_PLAYER_COMPLETED_FULL_ARCADE_SETUP(PedData.arcadeOwner)
		RETURN MAX_NUM_ARCADE_PEDS_PRE_SETUP
	ENDIF
	RETURN MAX_NUM_ARCADE_PEDS
ENDFUNC

PROC REQUEST_ALL_ARCADE_ASSETS()
	INT iPed
	REPEAT GET_MAX_NUM_ARCADE_PEDS() iPed
		REQUEST_ARCADE_PED_ASSETS(PedData.ArcadePed[iPed], iPed)	
	ENDREPEAT
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ INITIALISE ╞══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE: Jimmys locations post setup
FUNC INT GET_JIMMY_SPAWN_LOCATION()
	INT iSpawnLocation = GET_RANDOM_INT_IN_RANGE(0, MAX_NUM_ARCADE_JIMMY_SPAWN_LOCATIONS)
	
	#IF IS_DEBUG_BUILD
	IF g_iDebugArcadeJimmySpawnLocation > 0
		iSpawnLocation = (g_iDebugArcadeJimmySpawnLocation-1)
	ENDIF
	#ENDIF
	
	RETURN iSpawnLocation
ENDFUNC

FUNC INT GET_HEIST_DRIVER_SPAWN_LOCATION()
	
	INT iAvailableAcitvities[MAX_NUM_ARCADE_HEIST_DRIVER_SPAWN_LOCATIONS]
	iAvailableAcitvities[0] = 1
	iAvailableAcitvities[1] = 1
	
	// Third driver activity is leaning against wall talking on phone.
	// Dont include this in the available pool is Jimmy is doing this in same place. 
	IF (serverBD.iJimmySpawnLocation != 9)
		iAvailableAcitvities[2] = 1
	ENDIF
	
	// Drivers last activity has them observing a getaway vehicle. 
	// Ensure vehicle exists before adding activity to available pool.
	IF PedData.bAcquiredGetawayVehicles
	AND PedData.eHeistDriver != CASINO_HEIST_DRIVER__NONE
	AND GET_HEIST_VEHICLE_MODEL(1, PedData.eHeistDriver, PedData.arcadeOwner) != DUMMY_MODEL_FOR_SCRIPT
		iAvailableAcitvities[3] = 1
	ENDIF
	
	INT iRandActivity = GET_RANDOM_INT_IN_RANGE(0, MAX_NUM_ARCADE_HEIST_DRIVER_SPAWN_LOCATIONS)
	WHILE (iAvailableAcitvities[iRandActivity] = 0)
		iRandActivity = GET_RANDOM_INT_IN_RANGE(0, MAX_NUM_ARCADE_HEIST_DRIVER_SPAWN_LOCATIONS)
	ENDWHILE
	
	#IF IS_DEBUG_BUILD
	IF g_iDebugArcadeHeistDriverSpawnLocation > 0
		iRandActivity = (g_iDebugArcadeHeistDriverSpawnLocation-1)
	ENDIF
	#ENDIF
	
	RETURN iRandActivity
ENDFUNC

FUNC INT GET_HEIST_HACKER_SPAWN_LOCATION()
	INT iMaxSpawnLocations = MAX_NUM_ARCADE_HEIST_HACKER_SPAWN_LOCATIONS
	
	// Last hacker anim is sitting on ledge. 
	// Don't use if Jimmy already smoking bong on ledge
	IF (serverBD.iJimmySpawnLocation = 7)
		iMaxSpawnLocations = (MAX_NUM_ARCADE_HEIST_HACKER_SPAWN_LOCATIONS-1)
	ENDIF
	
	INT iRandActivity = GET_RANDOM_INT_IN_RANGE(0, iMaxSpawnLocations)
	
	#IF IS_DEBUG_BUILD
	IF g_iDebugArcadeHeistHackerSpawnLocation > 0
		iRandActivity = (g_iDebugArcadeHeistHackerSpawnLocation-1)
	ENDIF
	#ENDIF
	
	RETURN iRandActivity
ENDFUNC

FUNC INT GET_HEIST_WEAPON_EXPERT_SPAWN_LOCATION()
	INT iMaxSpawnLocations = (MAX_NUM_ARCADE_HEIST_WEAPON_EXPERT_SPAWN_LOCATIONS-1)
	
	// Only use leaning against call for Karl Abolaii weapon expert
	IF (PedData.eHeistWeaponExpert = CASINO_HEIST_WEAPON_EXPERT__KARL_ABOLAJI)
		iMaxSpawnLocations = MAX_NUM_ARCADE_HEIST_WEAPON_EXPERT_SPAWN_LOCATIONS
	ENDIF
	
	INT iRandActivity = GET_RANDOM_INT_IN_RANGE(0, iMaxSpawnLocations)
	
	#IF IS_DEBUG_BUILD
	IF g_iDebugArcadeHeistWeaponsExpertrSpawnLocation > 0
		iRandActivity = (g_iDebugArcadeHeistWeaponsExpertrSpawnLocation-1)
	ENDIF
	#ENDIF
	
	RETURN iRandActivity
ENDFUNC

FUNC INT GET_OWNER_ARCADE_CABINET_TYPES()
	
	INT iOwnedType
	INT iArcadeType
	FOR iArcadeType = ENUM_TO_INT(ARCADE_CABINET_CH_GG_SPACE_MONKEY) TO ENUM_TO_INT(ARCADE_CABINET_CH_LOVE_METER)
		IF HAS_PLAYER_PURCHASED_ARCADE_MACHINE_DELIVERED(PedData.arcadeOwner, INT_TO_ENUM(ARCADE_CABINETS, iArcadeType), ACP_ARCADE)
			iOwnedType++
		ENDIF
	ENDFOR
	
	RETURN iOwnedType
ENDFUNC

PROC SET_SERVER_BD_DATA()
	WHILE (serverBD.bServerDataInitialised = FALSE)
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			serverBD.iLayout = 0
			serverBD.bServerDataInitialised = TRUE
			serverBD.iJimmySpawnLocation = GET_JIMMY_SPAWN_LOCATION()
			serverBD.iHeistDriverSpawnLocation = GET_HEIST_DRIVER_SPAWN_LOCATION()
			serverBD.iHeistHackerSpawnLocation = GET_HEIST_HACKER_SPAWN_LOCATION()
			serverBD.iHeistWeaponExpertSpawnLocation = GET_HEIST_WEAPON_EXPERT_SPAWN_LOCATION()
			serverBD.iOwnedArcadeTypes = GET_OWNER_ARCADE_CABINET_TYPES()
			serverBD.iInstalledArcadeMachines = GET_NUM_INSTALLED_ARCADE_CABINETS_IN_ARCADE_PROPERTY(PedData.arcadeOwner, ACP_ARCADE)
			serverBD.iLevel = GET_OWNER_ARCADE_PED_LEVEL()
			serverBD.bTVOn = g_bTVOn
			
			PRINTLN("[AM_MP_ARCADE_PEDS] SET_SERVER_BD_DATA - iLevel set to ", serverBD.iLevel)
			PRINTLN("[AM_MP_ARCADE_PEDS] SET_SERVER_BD_DATA - iLayout set to ", serverBD.iLayout)
			PRINTLN("[AM_MP_ARCADE_PEDS] SET_SERVER_BD_DATA - iJimmySpawnLocation set to ", serverBD.iJimmySpawnLocation)
			PRINTLN("[AM_MP_ARCADE_PEDS] SET_SERVER_BD_DATA - iHeistDriverSpawnLocation set to ", serverBD.iHeistDriverSpawnLocation)
			PRINTLN("[AM_MP_ARCADE_PEDS] SET_SERVER_BD_DATA - iHeistHackerSpawnLocation set to ", serverBD.iHeistHackerSpawnLocation)
			PRINTLN("[AM_MP_ARCADE_PEDS] SET_SERVER_BD_DATA - iHeistWeaponExpertSpawnLocation set to ", serverBD.iHeistWeaponExpertSpawnLocation)
			PRINTLN("[AM_MP_ARCADE_PEDS] SET_SERVER_BD_DATA - iOwnedArcadeTypes set to ", serverBD.iOwnedArcadeTypes)
			PRINTLN("[AM_MP_ARCADE_PEDS] SET_SERVER_BD_DATA - iInstalledArcadeMachines set to ", serverBD.iInstalledArcadeMachines)
			PRINTLN("[AM_MP_ARCADE_PEDS] SET_SERVER_BD_DATA - bTVOn set to ", serverBD.bTVOn)
		ELSE
			PRINTLN("[AM_MP_ARCADE_PEDS] SET_SERVER_BD_DATA - waiting for bServerDataInitialised")
			WAIT(0)
		ENDIF
	ENDWHILE
ENDPROC

PROC SET_ARCADE_OWNER_DATA(ARCADE_PED_OWNER_DATA OwnerData)
	PedData.arcadeOwner = OwnerData.OwnerID
	PedData.bCutscenePlaying = OwnerData.bCutscenePlaying
	PedData.bCabinetsSetupMissionComplete = HAS_PLAYER_COMPLETED_ARCADE_PROPERTY_CABINETS_SETUP(OwnerData.OwnerID)
	PedData.bScopeOutSetupMissionComplete = HAS_PLAYER_COMPLETED_INITIAL_CASINO_SCOPE_OUT(OwnerData.OwnerID)
	PedData.eHeistDriver = GET_PLAYER_CASINO_HEIST_DRIVER(OwnerData.OwnerID)
	PedData.eHeistHacker = GET_PLAYER_CASINO_HEIST_HACKER(OwnerData.OwnerID)
	PedData.eHeistWeaponExpert = GET_PLAYER_CASINO_HEIST_WEAPONS_EXPERT(OwnerData.OwnerID)
	PedData.bAcquiredGetawayVehicles = HAS_PLAYER_ACQUIRED_CASINO_HEIST_VEHICLES(OwnerData.OwnerID)
	PedData.bEnteredViaBasement = OwnerData.bEnteredViaBasement
	PedData.bPrivateModeActive = GET_PLAYER_ARCADE_PRIVATE_MODE(OwnerData.OwnerID)
	
	PRINTLN("[AM_MP_ARCADE_PEDS] SET_ARCADE_OWNER_DATA - Owner ID: ", NATIVE_TO_INT(OwnerData.OwnerID))
	PRINTLN("[AM_MP_ARCADE_PEDS] SET_ARCADE_OWNER_DATA - Cutscene Playing: ", OwnerData.bCutscenePlaying)
	PRINTLN("[AM_MP_ARCADE_PEDS] SET_ARCADE_OWNER_DATA - Cabinets Setup Mission Complete: ", PedData.bCabinetsSetupMissionComplete)
	PRINTLN("[AM_MP_ARCADE_PEDS] SET_ARCADE_OWNER_DATA - Scope Out Setup Mission Complete: ", PedData.bScopeOutSetupMissionComplete)
	PRINTLN("[AM_MP_ARCADE_PEDS] SET_ARCADE_OWNER_DATA - Heist Driver: ", CASINO_HEIST_MISSION_DATA__DRIVER_STRING_FOR_DEBUG(PedData.eHeistDriver))
	PRINTLN("[AM_MP_ARCADE_PEDS] SET_ARCADE_OWNER_DATA - Heist Hacker: ", CASINO_HEIST_MISSION_DATA__HACKER_STRING_FOR_DEBUG(PedData.eHeistHacker))
	PRINTLN("[AM_MP_ARCADE_PEDS] SET_ARCADE_OWNER_DATA - Heist Weapon Expert: ", CASINO_HEIST_MISSION_DATA__WEAPONS_EXPERT_STRING_FOR_DEBUG(PedData.eHeistWeaponExpert))
	PRINTLN("[AM_MP_ARCADE_PEDS] SET_ARCADE_OWNER_DATA - Acquired Getaway Vehicles: ", PedData.bAcquiredGetawayVehicles)
	PRINTLN("[AM_MP_ARCADE_PEDS] SET_ARCADE_OWNER_DATA - Local Player Entered Via Basement: ", PedData.bEnteredViaBasement)
	PRINTLN("[AM_MP_ARCADE_PEDS] SET_ARCADE_OWNER_DATA - Owner Private Mode Active: ", PedData.bPrivateModeActive)
ENDPROC

PROC INITIALISE_PED_DATA()
	
	INT iPed
	REPEAT GET_MAX_NUM_ARCADE_PEDS() iPed
		GET_INITIAL_DATA_FOR_PED(ServerBD, PedData.ArcadePed[iPed], iPed, PedData.bCabinetsSetupMissionComplete, PedData.bScopeOutSetupMissionComplete, PedData.eHeistDriver, PedData.eHeistHacker, PedData.eHeistWeaponExpert)
		
		#IF IS_DEBUG_BUILD
		IF PedData.ArcadePed[iPed].iLevel > 0
			PedData.iCount_Level[PedData.ArcadePed[iPed].iLevel-1]++
		ENDIF
		#ENDIF
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
	PedData.iCount_Level[0] -= 5 // Minus the unique peds
	#ENDIF
ENDPROC

PROC INITIALISE_ARCADE_PEDS(BOOL bInitData = TRUE)
	CLEANUP_ALL_ARCADE_PEDS()
	
	IF bInitData
		INITIALISE_PED_DATA()
	ENDIF
	
	REQUEST_ALL_ARCADE_ASSETS()
	
	g_iArcadeJimmySpawnLocation = serverBD.iJimmySpawnLocation
	
	#IF IS_DEBUG_BUILD
	PedData.iCurrentLayout = ServerBD.iLayout
	PedData.iLastLayout = ServerBD.iLayout
	
	PedData.iCurrentLevel = ServerBD.iLevel
	PedData.iLastLevel = ServerBD.iLevel
	#ENDIF
ENDPROC

PROC CREATE_ARCADE_STANDALONE_PROPS()

	// Jimmy pre cabinets setup mission broom
	IF NOT PedData.bCabinetsSetupMissionComplete
		IF NOT DOES_ENTITY_EXIST(PedData.objBroom)
			PedData.objBroom = CREATE_OBJECT_NO_OFFSET(PROP_TOOL_BROOM, <<2733.130, -387.578, -49.330>>, FALSE, FALSE)
			SET_ENTITY_COORDS_NO_OFFSET(PedData.objBroom, <<2733.130, -387.578, -49.330>>)
			SET_ENTITY_ROTATION(PedData.objBroom, <<-12.0, 0.0, 225.0>>)
			SET_ENTITY_INVINCIBLE(PedData.objBroom, TRUE)
			SET_ENTITY_CAN_BE_DAMAGED(PedData.objBroom, FALSE)
			FREEZE_ENTITY_POSITION(PedData.objBroom, TRUE)
			SET_MODEL_AS_NO_LONGER_NEEDED(PROP_TOOL_BROOM)
			
			IF PedData.bCutscenePlaying
				SET_ENTITY_VISIBLE(PedData.objBroom, FALSE)
			ENDIF
		ENDIF
	ENDIF
	
	// Hacker desk chair
	IF PedData.bCabinetsSetupMissionComplete
	AND PedData.bScopeOutSetupMissionComplete
	AND PedData.eHeistHacker != CASINO_HEIST_HACKER__NONE
	AND (serverBD.iHeistHackerSpawnLocation = 0)
		IF NOT DOES_ENTITY_EXIST(PedData.objHackerChair)
			PedData.objHackerChair = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, HASH("ex_prop_offchair_exec_03")), <<2712.377, -352.966, -55.587>>, FALSE, FALSE)
			SET_ENTITY_COORDS_NO_OFFSET(PedData.objHackerChair, <<2712.377, -352.966, -55.587>>)
			SET_ENTITY_ROTATION(PedData.objHackerChair, <<0.0, 0.0, 180.0>>)
			SET_ENTITY_INVINCIBLE(PedData.objHackerChair, TRUE)
			SET_ENTITY_CAN_BE_DAMAGED(PedData.objHackerChair, FALSE)
			FREEZE_ENTITY_POSITION(PedData.objHackerChair, TRUE)
			SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, HASH("ex_prop_offchair_exec_03")))
			
			IF PedData.bCutscenePlaying
				SET_ENTITY_VISIBLE(PedData.objHackerChair, FALSE)
			ENDIF
		ENDIF
	ENDIF
	
	// Weapon Expert desk chair
	IF PedData.bCabinetsSetupMissionComplete
	AND PedData.bScopeOutSetupMissionComplete
	AND PedData.eHeistWeaponExpert != CASINO_HEIST_WEAPON_EXPERT__NONE
	AND (serverBD.iHeistWeaponExpertSpawnLocation = 0)
		IF NOT DOES_ENTITY_EXIST(PedData.objWeaponExpertChair)
			PedData.objWeaponExpertChair = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, HASH("ex_prop_offchair_exec_03")), <<2714.403, -355.905, -55.587>>, FALSE, FALSE)
			SET_ENTITY_COORDS_NO_OFFSET(PedData.objWeaponExpertChair, <<2714.403, -355.905, -55.587>>)
			SET_ENTITY_ROTATION(PedData.objWeaponExpertChair, <<0.0, 0.0, -55.000>>)
			SET_ENTITY_INVINCIBLE(PedData.objWeaponExpertChair, TRUE)
			SET_ENTITY_CAN_BE_DAMAGED(PedData.objWeaponExpertChair, FALSE)
			FREEZE_ENTITY_POSITION(PedData.objWeaponExpertChair, TRUE)
			SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, HASH("ex_prop_offchair_exec_03")))
			
			// Ped capsule is increased so player can't walk through chair 
			SET_ENTITY_COLLISION(PedData.objWeaponExpertChair, FALSE)
			
			IF PedData.bCutscenePlaying
				SET_ENTITY_VISIBLE(PedData.objWeaponExpertChair, FALSE)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC INITIALISE_ARCADE_PED_GLOBALS()
	g_bTurnOnArcadePeds = TRUE
	g_bInitArcadePedsCreated = FALSE
	g_bSetArcadePedsVisibleAfterCutscene = FALSE
	g_iArcadeJimmySpawnLocation = -1
ENDPROC

PROC SCRIPT_INITIALISE()
	PRINTLN("[AM_MP_ARCADE_PEDS] SCRIPT_INITIALISE - Initialising")
	
	WHILE NOT NETWORK_TRY_TO_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE, g_iArcadePedScriptInstance)
		PRINTLN("[AM_MP_ARCADE_PEDS] SCRIPT_INITIALISE - Waiting to be able to launch")
		WAIT(0)
	ENDWHILE
	
	g_iArcadePedScriptInstance++
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(ServerBD, SIZE_OF(ServerBD))
	
	IF NOT Wait_For_First_Network_Broadcast()
		PRINTLN("[AM_MP_ARCADE_PEDS] SCRIPT_INITIALISE - Failed to receive initial network broadcast. Terminating script.")
		SCRIPT_CLEANUP()
	ENDIF
	
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	PRINTLN("[AM_MP_ARCADE_PEDS] SCRIPT_INITIALISE - Complete")
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ PED AUDIO ╞══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

PROC UPDATE_AMBIENT_SOUND()
	
	IF NOT PedData.bCabinetsSetupMissionComplete
	OR NOT PedData.bScopeOutSetupMissionComplete
		EXIT
	ENDIF
	
	IF PedData.bPrivateModeActive
		IF IS_STREAM_PLAYING()
			PRINTLN("[AM_MP_ARCADE_PEDS] UPDATE_AMBIENT_SOUND - Private mode active, calling STOP_STREAM")
			STOP_STREAM()
		ENDIF
		PedData.iAudioState = 0
	ELSE
		IF (PedData.iAudioState = 0)
			IF (serverBD.iLevel != 0)
				IF LOAD_STREAM("Walla_Normal", "DLC_H3_Arcade_Walla_Sounds")
					PRINTLN("[AM_MP_ARCADE_PEDS] UPDATE_AMBIENT_SOUND - Loaded")
					PedData.iAudioState++
				ELSE
					PRINTLN("[AM_MP_ARCADE_PEDS] UPDATE_AMBIENT_SOUND - Loading")
				ENDIF
			ENDIF
		ENDIF
		
		IF (PedData.iAudioState = 1)
			PLAY_STREAM_FROM_POSITION(<<2729.5894, -383.9195, -48.9951>>)	// Centre of arcade room
			PRINTLN("[AM_MP_ARCADE_PEDS] UPDATE_AMBIENT_SOUND - Playing stream")
			PedData.iAudioState++
		ENDIF
		
		IF (PedData.iAudioState = 2)
			IF IS_STREAM_PLAYING()
				FLOAT fPedDensity = TO_FLOAT(ServerBD.iLevel) / 6.0
				SET_VARIABLE_ON_STREAM("PedDensity", fPedDensity)
				PRINTLN("[AM_MP_ARCADE_PEDS] UPDATE_AMBIENT_SOUND - Setting ped density: ", fPedDensity)
				PedData.iAudioState++
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════╡ PED CREATION ╞═════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

PROC SET_ARCADE_PED_PROPERTIES(PED_DATA &Data)
	SET_ENTITY_CAN_BE_DAMAGED(Data.PedID, FALSE)
	SET_PED_AS_ENEMY(Data.PedID, FALSE)
	SET_CURRENT_PED_WEAPON(Data.PedID, WEAPONTYPE_UNARMED, TRUE)
	
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(Data.PedID, TRUE)
	SET_PED_RESET_FLAG(Data.PedID, PRF_DisablePotentialBlastReactions, TRUE)
	SET_PED_CONFIG_FLAG(Data.PedID, PCF_UseKinematicModeWhenStationary, TRUE)
	SET_PED_CONFIG_FLAG(Data.PedID, PCF_DontActivateRagdollFromExplosions, TRUE)
	SET_PED_CONFIG_FLAG(Data.PedID, PCF_DontActivateRagdollFromVehicleImpact, TRUE)
	
	SET_PED_CAN_EVASIVE_DIVE(Data.PedID, FALSE)
	SET_TREAT_AS_AMBIENT_PED_FOR_DRIVER_LOCKON(Data.PedID, TRUE)
	SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(Data.PedID, FALSE)
	SET_PED_CAN_RAGDOLL(Data.PedID, FALSE)
	SET_PED_CONFIG_FLAG(Data.PedID, PCF_DisableExplosionReactions, TRUE)
	CLEAR_PED_TASKS(Data.PedID)
ENDPROC

PROC SET_ARCADE_PED_PROP_INDEXES(PED_DATA &Data, INT iPed)
	SWITCH iPed
		CASE 1	// Wendy
			SET_PED_PROP_INDEX(Data.PedID, ANCHOR_HEAD, 0)
			SET_PED_PROP_INDEX(Data.PedID, ANCHOR_EYES, 0)
		BREAK
		CASE 2	// Heist Driver
			SWITCH PedData.eHeistDriver
				CASE CASINO_HEIST_DRIVER__KARIM_DENZ
					SET_PED_DEFAULT_COMPONENT_VARIATION(Data.PedID)
					SET_PED_COMPONENT_VARIATION(Data.PedID,PED_COMP_HEAD,1,0)
					SET_PED_COMPONENT_VARIATION(Data.PedID,PED_COMP_HAIR,0,0)
					SET_PED_COMPONENT_VARIATION(Data.PedID,PED_COMP_TORSO,4,2)
					SET_PED_COMPONENT_VARIATION(Data.PedID,PED_COMP_LEG,3,2)
					SET_PED_COMPONENT_VARIATION(Data.PedID,PED_COMP_FEET,0,0)
					SET_PED_COMPONENT_VARIATION(Data.PedID,PED_COMP_SPECIAL,1,0)
					SET_PED_COMPONENT_VARIATION(Data.PedID,PED_COMP_SPECIAL2,0,0)
					SET_PED_COMPONENT_VARIATION(Data.PedID,PED_COMP_DECL,1,0)
				BREAK
				CASE CASINO_HEIST_DRIVER__TALIANA_MARTINEZ
					SET_PED_DEFAULT_COMPONENT_VARIATION(Data.PedID)
					SET_PED_COMPONENT_VARIATION(Data.PedID,PED_COMP_HEAD,2,0)
					SET_PED_COMPONENT_VARIATION(Data.PedID,PED_COMP_HAIR,2,0)
					SET_PED_COMPONENT_VARIATION(Data.PedID,PED_COMP_TORSO,2,2)
					SET_PED_COMPONENT_VARIATION(Data.PedID,PED_COMP_LEG,2,2)
					SET_PED_COMPONENT_VARIATION(Data.PedID,PED_COMP_FEET,0,0)
					SET_PED_COMPONENT_VARIATION(Data.PedID,PED_COMP_SPECIAL,1,0)
					SET_PED_COMPONENT_VARIATION(Data.PedID,PED_COMP_SPECIAL2,0,0)
					SET_PED_COMPONENT_VARIATION(Data.PedID,PED_COMP_DECL,1,0)
				BREAK
				CASE CASINO_HEIST_DRIVER__ZACH
					SET_PED_DEFAULT_COMPONENT_VARIATION(Data.PedID)
					SET_PED_COMPONENT_VARIATION(Data.PedID,PED_COMP_HEAD,1,1)
					SET_PED_COMPONENT_VARIATION(Data.PedID,PED_COMP_HAIR,1,0)
					SET_PED_COMPONENT_VARIATION(Data.PedID,PED_COMP_TORSO,0,1)
					SET_PED_COMPONENT_VARIATION(Data.PedID,PED_COMP_LEG,1,1)
					SET_PED_COMPONENT_VARIATION(Data.PedID,PED_COMP_SPECIAL,0,0)
					SET_PED_COMPONENT_VARIATION(Data.PedID,PED_COMP_DECL,1,0)
				BREAK
				CASE CASINO_HEIST_DRIVER__WEAPONS_EXPERT
					SET_PED_PROP_INDEX(Data.PedID, ANCHOR_HEAD, 0, 0)
				BREAK
			ENDSWITCH
		BREAK
		CASE 3	// Heist Hacker
			SWITCH PedData.eHeistHacker
				CASE CASINO_HEIST_HACKER__RICKIE_LUKENS
					SET_PED_DEFAULT_COMPONENT_VARIATION(Data.PedID)
					SET_PED_COMPONENT_VARIATION(Data.PedID,PED_COMP_HEAD,2,0)
					SET_PED_COMPONENT_VARIATION(Data.PedID,PED_COMP_HAIR,3,0)
					SET_PED_COMPONENT_VARIATION(Data.PedID,PED_COMP_TORSO,3,0)
					SET_PED_COMPONENT_VARIATION(Data.PedID,PED_COMP_LEG,3,0)
					SET_PED_COMPONENT_VARIATION(Data.PedID,PED_COMP_HAND,2,0)
					SET_PED_COMPONENT_VARIATION(Data.PedID,PED_COMP_SPECIAL,0,0)
					SET_PED_PROP_INDEX(Data.PedID,ANCHOR_EYES,0,0)
					SET_PED_PROP_INDEX(Data.PedID,ANCHOR_EARS,0,0)
				BREAK
				CASE CASINO_HEIST_HACKER__CHRISTIAN_FELTZ
					SET_PED_DEFAULT_COMPONENT_VARIATION(Data.PedID)
					SET_PED_COMPONENT_VARIATION(Data.PedID,PED_COMP_HEAD,0,0)
					SET_PED_COMPONENT_VARIATION(Data.PedID,PED_COMP_HAIR,0,0)
					SET_PED_COMPONENT_VARIATION(Data.PedID,PED_COMP_TORSO,0,0)
					SET_PED_COMPONENT_VARIATION(Data.PedID,PED_COMP_LEG,0,0)
					SET_PED_COMPONENT_VARIATION(Data.PedID,PED_COMP_HAND,0,0)
					SET_PED_COMPONENT_VARIATION(Data.PedID,PED_COMP_SPECIAL,0,0)
				BREAK
				CASE CASINO_HEIST_HACKER__YOHAN
					SET_PED_DEFAULT_COMPONENT_VARIATION(Data.PedID)
					SET_PED_COMPONENT_VARIATION(Data.PedID,PED_COMP_HEAD,5,0)
					SET_PED_COMPONENT_VARIATION(Data.PedID,PED_COMP_TORSO,5,0)
					SET_PED_COMPONENT_VARIATION(Data.PedID,PED_COMP_LEG,5,0)
					SET_PED_COMPONENT_VARIATION(Data.PedID,PED_COMP_SPECIAL,4,0)
				BREAK
			ENDSWITCH
		BREAK
		CASE 4	// Heist Weapon Expert
			SWITCH PedData.eHeistWeaponExpert
				CASE CASINO_HEIST_WEAPON_EXPERT__KARL_ABOLAJI
					SET_PED_DEFAULT_COMPONENT_VARIATION(Data.PedID)
					SET_PED_COMPONENT_VARIATION(Data.PedID,PED_COMP_HEAD,1,0)
					SET_PED_COMPONENT_VARIATION(Data.PedID,PED_COMP_BERD,1,0)
					SET_PED_COMPONENT_VARIATION(Data.PedID,PED_COMP_HAIR,1,0)
					SET_PED_COMPONENT_VARIATION(Data.PedID,PED_COMP_TORSO,6,2)
					SET_PED_COMPONENT_VARIATION(Data.PedID,PED_COMP_LEG,6,0)
					SET_PED_COMPONENT_VARIATION(Data.PedID,PED_COMP_FEET,1,0)
					SET_PED_COMPONENT_VARIATION(Data.PedID,PED_COMP_SPECIAL,0,0)
					SET_PED_COMPONENT_VARIATION(Data.PedID,PED_COMP_SPECIAL2,0,0)
					SET_PED_COMPONENT_VARIATION(Data.PedID,PED_COMP_DECL,1,0)
				BREAK
				CASE CASINO_HEIST_WEAPON_EXPERT__GUSTAVO_MOTA
					SET_PED_DEFAULT_COMPONENT_VARIATION(Data.PedID)
					SET_PED_COMPONENT_VARIATION(Data.PedID,PED_COMP_HEAD,4,0)
					SET_PED_COMPONENT_VARIATION(Data.PedID,PED_COMP_BERD,1,0)
					SET_PED_COMPONENT_VARIATION(Data.PedID,PED_COMP_HAIR,1,0)
					SET_PED_COMPONENT_VARIATION(Data.PedID,PED_COMP_TORSO,6,1)
					SET_PED_COMPONENT_VARIATION(Data.PedID,PED_COMP_LEG,6,1)
					SET_PED_COMPONENT_VARIATION(Data.PedID,PED_COMP_FEET,1,0)
					SET_PED_COMPONENT_VARIATION(Data.PedID,PED_COMP_SPECIAL,0,0)
					SET_PED_COMPONENT_VARIATION(Data.PedID,PED_COMP_SPECIAL2,0,0)
					SET_PED_COMPONENT_VARIATION(Data.PedID,PED_COMP_DECL,1,0)
				BREAK
				CASE CASINO_HEIST_WEAPON_EXPERT__PACKIE_MCREARY
					SET_PED_DEFAULT_COMPONENT_VARIATION(Data.PedID)
					SET_PED_COMPONENT_VARIATION(Data.PedID,PED_COMP_HEAD,5,0)
					SET_PED_COMPONENT_VARIATION(Data.PedID,PED_COMP_BERD,1,0)
					SET_PED_COMPONENT_VARIATION(Data.PedID,PED_COMP_HAIR,1,0)
					SET_PED_COMPONENT_VARIATION(Data.PedID,PED_COMP_TORSO,6,0)
					SET_PED_COMPONENT_VARIATION(Data.PedID,PED_COMP_LEG,6,2)
					SET_PED_COMPONENT_VARIATION(Data.PedID,PED_COMP_FEET,1,0)
					SET_PED_COMPONENT_VARIATION(Data.PedID,PED_COMP_SPECIAL,0,0)
				BREAK
				CASE CASINO_HEIST_WEAPON_EXPERT__WEAPONS_EXPERT
					SET_PED_PROP_INDEX(Data.PedID, ANCHOR_HEAD, 0, 0)
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE: Unique peds, e.g. Jimmy, do not use the generic ped clothing component data
FUNC BOOL DOES_PED_USE_COMPONENT_DATA(INT iPed)
	RETURN (iPed > 4)
ENDFUNC

/// PURPOSE: Changing Private Mode state will delete/create peds. This func ensures ped isn't created if intersecting with a player.
FUNC BOOL IS_ANY_PLAYER_BLOCKING_ARCADE_PED_COORDS(PED_DATA &Data)
	INT iParticipant
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
		    PLAYER_INDEX playerID = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
		    IF IS_NET_PLAYER_OK(playerID)
				IF ARE_VECTORS_ALMOST_EQUAL(GET_PLAYER_COORDS(playerID), Data.vPosition, 0.75, TRUE)
					RETURN TRUE
				ENDIF
			ENDIF
	    ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

PROC CREATE_ARCADE_ACTIVITY_PROP(PED_DATA &Data)
	INT iObject
	REPEAT MAX_NUM_ARCADE_PED_OBJECTS iObject
		IF NOT DOES_ENTITY_EXIST(Data.ObjectID[iObject])
		AND (GET_ARCADE_PROP_FOR_ACTIVITY(Data, iObject) != DUMMY_MODEL_FOR_SCRIPT)
			Data.ObjectID[iObject] = CREATE_OBJECT(GET_ARCADE_PROP_FOR_ACTIVITY(Data, iObject), GET_PED_BONE_COORDS(Data.PedID, GET_BONE_FOR_ARCADE_ACTIVITY_PROP(Data, iObject), <<0,0,0>>), FALSE, FALSE)
			
			IF IS_BIT_SET(PedData.iBS, BS_ARCADE_PEDS_PRIVATE_MODE_CHANGED)
				SET_ENTITY_ALPHA(Data.ObjectID[iObject], 0, FALSE)
			ENDIF
			
			IF SHOULD_ARCADE_PROP_BE_ATTACHED_FOR_ACTIVITY(Data)
				ATTACH_ENTITY_TO_ENTITY(Data.ObjectID[iObject], Data.PedID, GET_PED_BONE_INDEX(Data.PedID, GET_BONE_FOR_ARCADE_ACTIVITY_PROP(Data, iObject)), GET_BONE_OFFSET_POSITION_TO_ATTACH_TO(Data), GET_BONE_OFFSET_ROTATION_TO_ATTACH_TO(Data), TRUE, TRUE)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC CREATE_ARCADE_PED(PED_DATA &Data, INT iPed)
	
	IF (PedData.iPedsCreatedThisFrame < MAX_NUM_ARCADE_PEDS_CREATED_PER_FRAME)
		IF NOT IS_ANY_PLAYER_BLOCKING_ARCADE_PED_COORDS(Data)
			REQUEST_ARCADE_PED_ASSETS(Data, iPed)
			IF HAVE_ARCADE_PED_ASSETS_LOADED(Data, iPed)
				
				Data.PedID = CREATE_PED(PEDTYPE_MISSION, GET_ARCADE_PED_MODEL(iPed, Data.bFemale, PedData.eHeistDriver, PedData.eHeistHacker, PedData.eHeistWeaponExpert, Data.iPedModelVariation), Data.vPosition, Data.fHeading, FALSE, FALSE)
				
				IF DOES_ENTITY_EXIST(Data.PedID)
					IF DOES_PED_USE_COMPONENT_DATA(iPed)
						IF IS_PED_COMPONENT_DATA_VALID(Data.iPackedDrawable, Data.iPackedTexture)
							APPLY_PACKED_PED_COMPONENTS(Data.PedID, Data.iPackedDrawable, Data.iPackedTexture)
						ELSE
							GET_PACKED_PED_COMPONENT_CONFIG(Data.PedID, Data.iPackedDrawable, Data.iPackedTexture)
						ENDIF
					ENDIF
					
					SET_ARCADE_PED_PROPERTIES(Data)
					SET_ARCADE_PED_PROP_INDEXES(Data, iPed)
					
					IF DOES_ARCADE_ACTIVITY_HAVE_PROP(Data)
						CREATE_ARCADE_ACTIVITY_PROP(Data)
					ENDIF
					
					IF IS_ARCADE_ANIM_MULTI_STAGED(Data)
						SET_PED_ALTERNATE_MOVEMENT_ANIM(Data.PedID, AAT_IDLE, GET_ARCADE_ANIM_DICTIONARY_FOR_ACTIVITY(), GET_ARCADE_ANIM_CLIP_FOR_ACTIVITY_BASE(Data))
						PRINTLN("[AM_MP_ARCADE_PEDS] CREATE_ARCADE_PED - Setting alternate idle")
					ENDIF
					
					IF PedData.bCutscenePlaying
						SET_ENTITY_VISIBLE(Data.PedID, FALSE)
					ENDIF
					
					IF IS_BIT_SET(PedData.iBS, BS_ARCADE_PEDS_PRIVATE_MODE_CHANGED)
						SET_ENTITY_ALPHA(Data.PedID, 0, FALSE)
					ENDIF
					
					PedData.iPedsCreatedThisFrame++
					PRINTLN("[AM_MP_ARCADE_PEDS] CREATE_ARCADE_PED - Created ped: ", iPed, " this frame")
				ENDIF
			ENDIF
		ELSE
			PRINTLN("[AM_MP_ARCADE_PEDS] CREATE_ARCADE_PED - A player is intersecting with Ped: ", iPed, "'s spawn coords")
		ENDIF
	ELSE
		PRINTLN("[AM_MP_ARCADE_PEDS] CREATE_ARCADE_PED - Already created ", MAX_NUM_ARCADE_PEDS_CREATED_PER_FRAME, " peds this frame")
	ENDIF
	
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════╡ PED MAINTAIN ╞═════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

FUNC INT GET_PED_ACTIVITY_CLIP_TOTAL(ARCADE_ACTIVITY eActivity)
	SWITCH eActivity
		CASE AA_JIMMY_LYING_DOWN_SMOKING			RETURN 4
		CASE AA_JIMMY_SOFA_DRINKING_WATCHING_TV		RETURN 7
	ENDSWITCH
	RETURN 0
ENDFUNC

PROC UPDATE_PED_SYNC_SCENE(PED_DATA &Data)
	
	STRING sDict = GET_ARCADE_ANIM_DICTIONARY_FOR_ACTIVITY()
	STRING sClip = GET_ARCADE_ANIM_CLIP_FOR_ACTIVITY(Data)
	STRING sPropClip = GET_ARCADE_PROP_ANIM_CLIP_FOR_ACTIVITY(Data)
	
	// Set properties for one off anims and loop
	IF IS_ARCADE_ACTIVITY_SYCNED_SCENE_LOOPED(Data)
		IF (Data.iSyncSceneID = -1)
			Data.iSyncSceneID = CREATE_SYNCHRONIZED_SCENE(Data.vPosition, <<0.0, 0.0, Data.fHeading>>)
			
			IF DOES_ENTITY_EXIST(Data.PedID)
			AND NOT IS_STRING_NULL_OR_EMPTY(sDict)
			AND NOT IS_STRING_NULL_OR_EMPTY(sClip)
				TASK_SYNCHRONIZED_SCENE(Data.PedID, Data.iSyncSceneID, sDict, sClip, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			ENDIF
			
			IF DOES_ENTITY_EXIST(Data.ObjectID[0])
			AND DOES_ENTITY_HAVE_DRAWABLE(Data.ObjectID[0])
			AND NOT IS_STRING_NULL_OR_EMPTY(sDict)
			AND NOT IS_STRING_NULL_OR_EMPTY(sPropClip)
				PLAY_SYNCHRONIZED_ENTITY_ANIM(Data.ObjectID[0], Data.iSyncSceneID, sPropClip, sDict, INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
			ENDIF
			
			SET_SYNCHRONIZED_SCENE_LOOPED(Data.iSyncSceneID,TRUE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(Data.PedID, TRUE)
			SET_PED_KEEP_TASK(Data.PedID, TRUE)
		ENDIF
	ELSE
		// Update the scene manually as not looped
		SCRIPTTASKSTATUS TaskStatus = GET_SCRIPT_TASK_STATUS(Data.PedID, SCRIPT_TASK_SYNCHRONIZED_SCENE)
		IF (TaskStatus != WAITING_TO_START_TASK AND TaskStatus != PERFORMING_TASK)
		OR (GET_SYNCHRONIZED_SCENE_PHASE(Data.iSyncSceneID) >= 1.0)
			
			// Randomly select a new clip
			INT iRandClip = GET_RANDOM_INT_IN_RANGE(0, GET_PED_ACTIVITY_CLIP_TOTAL(INT_TO_ENUM(ARCADE_ACTIVITY, Data.iActivity)))
			sClip = GET_ARCADE_ANIM_CLIP_FOR_ACTIVITY(Data, iRandClip)
			sPropClip = GET_ARCADE_PROP_ANIM_CLIP_FOR_ACTIVITY(Data, iRandClip)
			
			// Scene and IK Control flags
			IK_CONTROL_FLAGS ikControlFlags = AIK_NONE
			SYNCED_SCENE_PLAYBACK_FLAGS SceneFlags = (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			
			IF DOES_ARCADE_ACTIVITY_NEED_EXPANDED_CAPSULE(Data)
				ikControlFlags = AIK_DISABLE_LEG_IK
				SceneFlags = (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON)
			ENDIF
			
			Data.iSyncSceneID = CREATE_SYNCHRONIZED_SCENE(Data.vPosition, <<0.0, 0.0, Data.fHeading>>)
			
			IF DOES_ENTITY_EXIST(Data.PedID)
			AND NOT IS_STRING_NULL_OR_EMPTY(sDict)
			AND NOT IS_STRING_NULL_OR_EMPTY(sClip)
				TASK_SYNCHRONIZED_SCENE(Data.PedID, Data.iSyncSceneID, sDict, sClip, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SceneFlags, DEFAULT, DEFAULT, ikControlFlags)
			ENDIF
			
			IF DOES_ENTITY_EXIST(Data.ObjectID[0])
			AND DOES_ENTITY_HAVE_DRAWABLE(Data.ObjectID[0])
			AND NOT IS_STRING_NULL_OR_EMPTY(sDict)
			AND NOT IS_STRING_NULL_OR_EMPTY(sPropClip)
				PLAY_SYNCHRONIZED_ENTITY_ANIM(Data.ObjectID[0], Data.iSyncSceneID, sPropClip, sDict, INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
			ENDIF
			
			SET_SYNCHRONIZED_SCENE_LOOPED(Data.iSyncSceneID, FALSE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(Data.PedID, TRUE)
		ENDIF
	ENDIF
ENDPROC

PROC UPDATE_PED_ACTIVITY(PED_DATA &Data)
	
	IF NOT DOES_ENTITY_EXIST(Data.PedID)
	OR IS_ENTITY_DEAD(Data.PedID)
		EXIT
	ENDIF
	
	STRING sDict = GET_ARCADE_ANIM_DICTIONARY_FOR_ACTIVITY()
	STRING sClip = GET_ARCADE_ANIM_CLIP_FOR_ACTIVITY(Data)
	FLOAT fRand = GET_RANDOM_FLOAT_IN_RANGE(0, 0.7)
	SCRIPTTASKSTATUS TaskStatus
	
	IF DOES_ARCADE_ACTIVITY_USE_SYCNED_SCENE(Data)
		UPDATE_PED_SYNC_SCENE(Data)
	ELSE
		
		TaskStatus = GET_SCRIPT_TASK_STATUS(Data.PedID, SCRIPT_TASK_PERFORM_SEQUENCE)
		IF (TaskStatus != WAITING_TO_START_TASK)
		AND (TaskStatus != PERFORMING_TASK)		
			IF HAS_ANIM_DICT_LOADED(sDict)			
				SEQUENCE_INDEX seq
				OPEN_SEQUENCE_TASK(seq)
					IF NOT IS_ARCADE_ANIM_MULTI_STAGED(Data)
						IF IS_ARCADE_ACTIVITY_SEATED(Data)
							IF DOES_ARCADE_ACTIVITY_NEED_EXPANDED_CAPSULE(Data)
								TASK_PLAY_ANIM_ADVANCED(NULL, sDict, sClip, Data.vPosition, <<0,0,Data.fHeading>>, DEFAULT, DEFAULT, DEFAULT, AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION | AF_EXPAND_PED_CAPSULE_FROM_SKELETON, fRand, DEFAULT, AIK_DISABLE_LEG_IK)
							ELSE
								TASK_PLAY_ANIM_ADVANCED(NULL, sDict, sClip, Data.vPosition, <<0,0,Data.fHeading>>, DEFAULT, DEFAULT, DEFAULT, AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION, fRand, DEFAULT, AIK_DISABLE_LEG_IK)
							ENDIF
						ELSE
							TASK_PLAY_ANIM(NULL, sDict, sClip, DEFAULT, DEFAULT, DEFAULT, AF_LOOPING, fRand)
						ENDIF
					ELSE
						
						INT iTask[MAX_NUM_ARCADE_PED_ANIM_CLIPS_IN_ACTIVITY] 
						iTask[0] = 0
						iTask[1] = 1
						iTask[2] = 2
						iTask[3] = 3		
						
						// shuffle tasks so they play in a different order
						INT iRd1, iRd2
						INT i
						INT iStored
						REPEAT 10 i
							iRd1 = GET_RANDOM_INT_IN_RANGE(0, MAX_NUM_ARCADE_PED_ANIM_CLIPS_IN_ACTIVITY)	
							iRd2 = GET_RANDOM_INT_IN_RANGE(0, MAX_NUM_ARCADE_PED_ANIM_CLIPS_IN_ACTIVITY)
							iStored = iTask[iRd1]
							iTask[iRd1] = iTask[iRd2] 
							iTask[iRd2] = iStored
						ENDREPEAT					
						
						// Activites that have three clips (no idle_d)
						IF DOES_ARCADE_ACTIVITY_HAVE_REDUCED_CLIPS(Data)
							INT iClip
							REPEAT MAX_NUM_ARCADE_PED_ANIM_CLIPS_IN_ACTIVITY iClip
								IF iTask[iClip] = 3
									iTask[iClip] = -1
								ENDIF
							ENDREPEAT
						ENDIF
						
						REPEAT MAX_NUM_ARCADE_PED_ANIM_CLIPS_IN_ACTIVITY i
							TASK_PLAY_ANIM(NULL, sDict, GET_ARCADE_ANIM_CLIP_FOR_ACTIVITY(Data, iTask[i]), DEFAULT, DEFAULT,DEFAULT, AF_DEFAULT, 0)
						ENDREPEAT
						
					ENDIF
					SET_SEQUENCE_TO_REPEAT(seq, REPEAT_FOREVER)
				CLOSE_SEQUENCE_TASK(seq)				
				TASK_PERFORM_SEQUENCE(Data.PedID, seq)	
				CLEAR_SEQUENCE_TASK(seq)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC SET_ARCADE_PEDS_CREATED_FLAG()
	IF NOT g_bInitArcadePedsCreated
		
		INT iPed
		INT iPedsCreated = 0
		REPEAT GET_MAX_NUM_ARCADE_PEDS() iPed
			IF (DOES_ENTITY_EXIST(PedData.ArcadePed[iPed].PedID) 
				AND (GET_SCRIPT_TASK_STATUS(PedData.ArcadePed[iPed].PedID, SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK 
				OR GET_SCRIPT_TASK_STATUS(PedData.ArcadePed[iPed].PedID, SCRIPT_TASK_SYNCHRONIZED_SCENE) = PERFORMING_TASK))
			OR PedData.ArcadePed[iPed].bSkipPed
			OR (PedData.ArcadePed[iPed].iLevel > serverBD.iLevel)
			OR (PedData.bEnteredViaBasement AND NOT PedData.ArcadePed[iPed].bBasementPed AND NOT g_bArcadeSecondaryInteriorReady)
			OR (NOT PedData.bEnteredViaBasement AND PedData.ArcadePed[iPed].bBasementPed AND NOT g_bArcadeSecondaryInteriorReady)
			OR (NOT SHOULD_ARCADE_PED_BE_CREATED_IN_PRIVATE_MODE(iPed) AND PedData.bPrivateModeActive)
				iPedsCreated++
			ENDIF
		ENDREPEAT
		
		PRINTLN("[AM_MP_ARCADE_PEDS] SET_ARCADE_PEDS_CREATED_FLAG - Peds Created: ", iPedsCreated, "/", GET_MAX_NUM_ARCADE_PEDS())
		IF (iPedsCreated = GET_MAX_NUM_ARCADE_PEDS())
			g_bInitArcadePedsCreated = TRUE	// Let AM_MP_ARCADE.sc know the peds have been created
		ENDIF
	ENDIF
ENDPROC

PROC SET_ALL_ARCADE_PEDS_VISIBLE_STATE(BOOL bVisible)
	INT iPed
	REPEAT GET_MAX_NUM_ARCADE_PEDS() iPed
		IF DOES_ENTITY_EXIST(PedData.ArcadePed[iPed].PedID)
			SET_ENTITY_VISIBLE(PedData.ArcadePed[iPed].PedID, bVisible)
		ENDIF
	ENDREPEAT
ENDPROC

PROC SET_ALL_ARCADE_PED_PROP_VISIBLE_STATE(BOOL bVisible)
	IF DOES_ENTITY_EXIST(PedData.objBroom)
		SET_ENTITY_VISIBLE(PedData.objBroom, bVisible)
	ENDIF
	IF DOES_ENTITY_EXIST(PedData.objHackerChair)
		SET_ENTITY_VISIBLE(PedData.objHackerChair, bVisible)
	ENDIF
	IF DOES_ENTITY_EXIST(PedData.objWeaponExpertChair)
		SET_ENTITY_VISIBLE(PedData.objWeaponExpertChair, bVisible)
	ENDIF
ENDPROC

/// PURPOSE: Maintain peds during cutscenes
PROC MAINTAIN_ARCADE_PEDS_CUTSCENE()
	// Unhide the peds/props after cutscenes
	IF g_bSetArcadePedsVisibleAfterCutscene
		SET_ALL_ARCADE_PEDS_VISIBLE_STATE(TRUE)
		SET_ALL_ARCADE_PED_PROP_VISIBLE_STATE(TRUE)
		PedData.bCutscenePlaying = FALSE
		g_bSetArcadePedsVisibleAfterCutscene = FALSE
	ELSE
		// Safety net to hide peds during cutscene
		IF NOT PedData.bCutscenePlaying
			IF IS_CUTSCENE_PLAYING()
				PRINTLN("[AM_MP_ARCADE_PEDS] MAINTAIN_ARCADE_PEDS_CUTSCENE - Cutscene is running but bCutscenePlaying flag is false! Hiding peds now.")
				SET_ALL_ARCADE_PEDS_VISIBLE_STATE(FALSE)
				SET_ALL_ARCADE_PED_PROP_VISIBLE_STATE(FALSE)
				PedData.bCutscenePlaying = TRUE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

// url:bugstar:6126959 - Arcade - Would it be possible to hide the peds when in the arcade management menu? 
PROC MAINTAIN_ARCADE_PEDS_CABINET_MANAGEMENT_MENU()
	
	IF IS_LOCAL_PLAYER_USING_ARCADE_MANAGEMENT_MENU()
		IF NOT IS_BIT_SET(PedData.iBS, BS_ARCADE_PEDS_HIDE_PEDS_FOR_CABINET_MANAGEMENT_MENU)
			SET_ALL_ARCADE_PEDS_VISIBLE_STATE(FALSE)
			PRINTLN("[AM_MP_ARCADE_PEDS] MAINTAIN_ARCADE_PEDS_CABINET_MANAGEMENT_MENU - Hiding peds as cabinet management menu is active")
			SET_BIT(PedData.iBS, BS_ARCADE_PEDS_HIDE_PEDS_FOR_CABINET_MANAGEMENT_MENU)
		ENDIF
	ELSE
		IF IS_BIT_SET(PedData.iBS, BS_ARCADE_PEDS_HIDE_PEDS_FOR_CABINET_MANAGEMENT_MENU)
			SET_ALL_ARCADE_PEDS_VISIBLE_STATE(TRUE)
			PRINTLN("[AM_MP_ARCADE_PEDS] MAINTAIN_ARCADE_PEDS_CABINET_MANAGEMENT_MENU - Unhiding peds as cabinet management menu is no longer active")
			CLEAR_BIT(PedData.iBS, BS_ARCADE_PEDS_HIDE_PEDS_FOR_CABINET_MANAGEMENT_MENU)
		ENDIF
	ENDIF
	
ENDPROC

PROC MAINTAIN_ARCADE_PEDS_COLLISION_CAPSULE()
	// Increase weapon experts capsule size when sitting in chair
	IF (serverBD.iHeistWeaponExpertSpawnLocation = 0)
	AND IS_ENTITY_ALIVE(PedData.ArcadePed[4].PedID)
		SET_PED_CAPSULE(PedData.ArcadePed[4].PedID, 0.5)
	ENDIF
ENDPROC

PROC MAINTAIN_PED_SPEECH_BUFFER_TIMERS()
	IF HAS_NET_TIMER_STARTED(serverBD.stJimmyBufferSpeechTimer)
	AND HAS_NET_TIMER_EXPIRED(serverBD.stJimmyBufferSpeechTimer, PED_SPEECH_BUFFER_TIME_MS)
		RESET_NET_TIMER(serverBD.stJimmyBufferSpeechTimer)
	ENDIF
	IF HAS_NET_TIMER_STARTED(serverBD.stDriverBufferSpeechTimer)
	AND HAS_NET_TIMER_EXPIRED(serverBD.stDriverBufferSpeechTimer, PED_SPEECH_BUFFER_TIME_MS)
		RESET_NET_TIMER(serverBD.stDriverBufferSpeechTimer)
	ENDIF
	IF HAS_NET_TIMER_STARTED(serverBD.stHackerBufferSpeechTimer)
	AND HAS_NET_TIMER_EXPIRED(serverBD.stHackerBufferSpeechTimer, PED_SPEECH_BUFFER_TIME_MS)
		RESET_NET_TIMER(serverBD.stHackerBufferSpeechTimer)
	ENDIF
	IF HAS_NET_TIMER_STARTED(PedData.stLoiterTimerBuffer)
	AND HAS_NET_TIMER_EXPIRED(PedData.stLoiterTimerBuffer, PED_SPEECH_BUFFER_TIME_MS)
		RESET_NET_TIMER(PedData.stLoiterTimerBuffer)
	ENDIF
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════╡ PRIVATE MODE ╞═════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

PROC SET_ARCADE_PED_FADE_TIMER(PED_DATA &Data)
	IF NOT IS_BIT_SET(Data.iBS, BS_ARCADE_PED_DATA_SET_FADE_TIMER)
		Data.tdPedFadeTimer = GET_TIME_OFFSET(GET_NETWORK_TIME(), ARCADE_PATRON_PED_FADE_TIME_MS)
		SET_BIT(Data.iBS, BS_ARCADE_PED_DATA_SET_FADE_TIMER)
	ENDIF
ENDPROC

PROC SET_ARCADE_PED_PROPS_ALPHA(PED_DATA &Data, INT iAlpha)
	INT iObject
	REPEAT MAX_NUM_ARCADE_PED_OBJECTS iObject
		IF DOES_ENTITY_EXIST(Data.ObjectID[iObject])
			SET_ENTITY_ALPHA(Data.ObjectID[iObject], iAlpha, FALSE)
		ENDIF
	ENDREPEAT
ENDPROC

PROC UPDATE_ARCADE_PED_FADE(PED_DATA &Data, BOOL bFadeIn)
	IF DOES_ENTITY_EXIST(Data.PedID)
		INT iTimeDifference = ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), Data.tdPedFadeTimer))
		INT iAlpha = ROUND((TO_FLOAT(iTimeDifference) / ARCADE_PATRON_PED_FADE_TIME_MS) * 255)
		
		IF bFadeIn
			iAlpha = (255 - (ROUND((TO_FLOAT(iTimeDifference) / ARCADE_PATRON_PED_FADE_TIME_MS) * 255)))
		ENDIF
		
		SET_ENTITY_ALPHA(Data.PedID, iAlpha, FALSE)
		SET_ARCADE_PED_PROPS_ALPHA(Data, iAlpha)
	ENDIF
ENDPROC

PROC FINALISE_ARCADE_PED_FADE(PED_DATA &Data, BOOL bFadeIn)
	IF DOES_ENTITY_EXIST(Data.PedID)
		IF bFadeIn
			SET_ENTITY_ALPHA(Data.PedID, 255, FALSE)
			SET_ARCADE_PED_PROPS_ALPHA(Data, 255)
		ELSE
			SET_ENTITY_ALPHA(Data.PedID, 0, FALSE)
			SET_ARCADE_PED_PROPS_ALPHA(Data, 0)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL PERFORM_ARCADE_PED_FADE(PED_DATA &Data, BOOL bFadeIn)
	SET_ARCADE_PED_FADE_TIMER(Data)
	
	IF IS_TIME_LESS_THAN(GET_NETWORK_TIME(), Data.tdPedFadeTimer)
		UPDATE_ARCADE_PED_FADE(Data, bFadeIn)
	ELSE
		FINALISE_ARCADE_PED_FADE(Data, bFadeIn)
		CLEAR_BIT(Data.iBS, BS_ARCADE_PED_DATA_SET_FADE_TIMER)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Disabled means peds should be present. This functions fades the peds back in once created.
PROC MAINTAIN_ARCADE_PEDS_PRIVATE_MODE_DISABLED()
	
	INT iPed
	INT iPedsFadedIn = 0
	FOR iPed = ARCADE_PATRON_PED_STARTING_INDEX TO (GET_MAX_NUM_ARCADE_PEDS()-1)
		// Fade peds in when each one is ready. Players can block spawn coords so fade on case by case basis.
		IF SHOULD_CREATE_ARCADE_PED(PedData.ArcadePed[iPed], iPed)
			IF (DOES_ENTITY_EXIST(PedData.ArcadePed[iPed].PedID) 
			AND (GET_SCRIPT_TASK_STATUS(PedData.ArcadePed[iPed].PedID, SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK 
			OR GET_SCRIPT_TASK_STATUS(PedData.ArcadePed[iPed].PedID, SCRIPT_TASK_SYNCHRONIZED_SCENE) = PERFORMING_TASK))
				IF NOT IS_BIT_SET(PedData.ArcadePed[iPed].iBS, BS_ARCADE_PED_DATA_FADE_COMPLETE)
					IF PERFORM_ARCADE_PED_FADE(PedData.ArcadePed[iPed], TRUE)
						SET_BIT(PedData.ArcadePed[iPed].iBS, BS_ARCADE_PED_DATA_FADE_COMPLETE)
					ENDIF
				ELSE
					iPedsFadedIn++
				ENDIF
			ENDIF
		ELSE
			iPedsFadedIn++
		ENDIF
	ENDFOR
	
	// All peds have been faded in
	IF iPedsFadedIn = (GET_MAX_NUM_ARCADE_PEDS()-ARCADE_PATRON_PED_STARTING_INDEX)
		FOR iPed = ARCADE_PATRON_PED_STARTING_INDEX TO (GET_MAX_NUM_ARCADE_PEDS()-1)
			CLEAR_BIT(PedData.ArcadePed[iPed].iBS, BS_ARCADE_PED_DATA_FADE_COMPLETE)
		ENDFOR
		CLEAR_BIT(PedData.iBS, BS_ARCADE_PEDS_PRIVATE_MODE_CHANGED)
	ENDIF
ENDPROC

/// PURPOSE: Disabled means peds shouldn't be present. This functions fades the peds out and deletes them.
PROC MAINTAIN_ARCADE_PEDS_PRIVATE_MODE_ENABLED()
	
	INT iPed
	INT iPedsFadedOut = 0
	FOR iPed = ARCADE_PATRON_PED_STARTING_INDEX TO (GET_MAX_NUM_ARCADE_PEDS()-1)
		IF SHOULD_CREATE_ARCADE_PED(PedData.ArcadePed[iPed], iPed)
			IF NOT IS_BIT_SET(PedData.ArcadePed[iPed].iBS, BS_ARCADE_PED_DATA_FADE_COMPLETE)
				IF PERFORM_ARCADE_PED_FADE(PedData.ArcadePed[iPed], FALSE)
					SET_BIT(PedData.ArcadePed[iPed].iBS, BS_ARCADE_PED_DATA_FADE_COMPLETE)
				ENDIF
			ELSE
				iPedsFadedOut++
			ENDIF
		ELSE
			iPedsFadedOut++
		ENDIF
	ENDFOR
	
	IF iPedsFadedOut = (GET_MAX_NUM_ARCADE_PEDS()-ARCADE_PATRON_PED_STARTING_INDEX)
		FOR iPed = ARCADE_PATRON_PED_STARTING_INDEX TO (GET_MAX_NUM_ARCADE_PEDS()-1)
			CLEAR_BIT(PedData.ArcadePed[iPed].iBS, BS_ARCADE_PED_DATA_FADE_COMPLETE)
			DELETE_ARCADE_PED(PedData.ArcadePed[iPed])
		ENDFOR
		CLEAR_BIT(PedData.iBS, BS_ARCADE_PEDS_PRIVATE_MODE_CHANGED)
	ENDIF
ENDPROC

PROC MAINTAIN_ARCADE_PEDS_PRIVATE_MODE()
	IF NOT g_bInitArcadePedsCreated
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(PedData.iBS, BS_ARCADE_PEDS_PRIVATE_MODE_CHANGED)
		IF PedData.bPrivateModeActive <> GET_PLAYER_ARCADE_PRIVATE_MODE(PedData.arcadeOwner)
			PedData.bPrivateModeActive = GET_PLAYER_ARCADE_PRIVATE_MODE(PedData.arcadeOwner)
			SET_BIT(PedData.iBS, BS_ARCADE_PEDS_PRIVATE_MODE_CHANGED)
		ENDIF
	ELSE
		// New private mode state
		IF PedData.bPrivateModeActive
			MAINTAIN_ARCADE_PEDS_PRIVATE_MODE_ENABLED()
		ELSE
			MAINTAIN_ARCADE_PEDS_PRIVATE_MODE_DISABLED()
		ENDIF
		
		// Private mode state changed during fade. Reset data and update accordingly
		IF PedData.bPrivateModeActive <> GET_PLAYER_ARCADE_PRIVATE_MODE(PedData.arcadeOwner)
			PedData.bPrivateModeActive = GET_PLAYER_ARCADE_PRIVATE_MODE(PedData.arcadeOwner)
			INT iPed
			FOR iPed = ARCADE_PATRON_PED_STARTING_INDEX TO (GET_MAX_NUM_ARCADE_PEDS()-1)
				CLEAR_BIT(PedData.ArcadePed[iPed].iBS, BS_ARCADE_PED_DATA_SET_FADE_TIMER)
				CLEAR_BIT(PedData.ArcadePed[iPed].iBS, BS_ARCADE_PED_DATA_FADE_COMPLETE)
			ENDFOR
		ENDIF
	ENDIF
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ PED SPEECH ╞══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

FUNC STRING GET_ARCADE_PED_SPEECH_TYPE_STRING(PED_SPEECH_TYPE eSpeechType)
	SWITCH eSpeechType
		CASE PST_INVALID		RETURN "PST_INVALID"
		CASE PST_GREETING		RETURN "PST_GREETING"
		CASE PST_BYE			RETURN "PST_BYE"
		CASE PST_BUMP			RETURN "PST_BUMP"
		CASE PST_LOITERING		RETURN "PST_LOITERING"
		CASE PST_TV_TURNED_OFF	RETURN "PST_TV_TURNED_OFF"
		CASE PST_ACTIVITY		RETURN "PST_ACTIVITY"
		CASE PST_POPULARITY		RETURN "PST_POPULARITY"
		CASE PST_CABINETS		RETURN "PST_CABINETS"
		CASE PST_CABINET_TYPE	RETURN "PST_CABINET_TYPE"
		CASE PST_WHATS_UP		RETURN "PST_WHATS_UP"
		CASE PST_IDLE			RETURN "PST_IDLE"
		CASE PST_NO_VEHICLE		RETURN "PST_NO_VEHICLE"
		CASE PST_UPGRADE		RETURN "PST_UPGRADE"
		CASE PST_TOTAL			RETURN "PST_TOTAL"
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC BOOL IS_PED_CURRENTLY_PLAYING_SPEECH(INT iPed, BOOL bIncludeBufferTimers = TRUE)
	BOOL bPlayingSpeech = FALSE
	
	IF IS_ENTITY_ALIVE(PedData.ArcadePed[iPed].PedID)
		IF IS_ANY_SPEECH_PLAYING(PedData.ArcadePed[iPed].PedID)
			bPlayingSpeech = TRUE
		ENDIF
	ENDIF
	
	IF bIncludeBufferTimers
		SWITCH iPed
			CASE 0	// JImmy
				IF (PedData.iLoiterPedID = iPed AND HAS_NET_TIMER_STARTED(PedData.stLoiterTimerBuffer))
				OR HAS_NET_TIMER_STARTED(serverBD.stJimmyBufferSpeechTimer)
					bPlayingSpeech = TRUE
				ENDIF
			BREAK
			CASE 2	// Driver
				IF (PedData.iLoiterPedID = iPed AND HAS_NET_TIMER_STARTED(PedData.stLoiterTimerBuffer))
				OR HAS_NET_TIMER_STARTED(serverBD.stDriverBufferSpeechTimer)
					bPlayingSpeech = TRUE
				ENDIF
			BREAK
			CASE 3	// Hacker
				IF (PedData.iLoiterPedID = iPed AND HAS_NET_TIMER_STARTED(PedData.stLoiterTimerBuffer))
				OR HAS_NET_TIMER_STARTED(serverBD.stHackerBufferSpeechTimer)
					bPlayingSpeech = TRUE
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	
	RETURN bPlayingSpeech
ENDFUNC

/// PURPOSE: Only the unquie peds play dialogue
FUNC BOOL DOES_PED_PLAY_SPEECH(INT iPed)
	RETURN (iPed >= 0 AND iPed <= 4)
ENDFUNC

FUNC BOOL IS_PED_SPEECH_TYPE_VALID(PED_SPEECH_TYPE eSpeechType)
	RETURN (eSpeechType != PST_INVALID AND eSpeechType != PST_TOTAL)
ENDFUNC

FUNC FLOAT GET_PED_LISTEN_DISTANCE(INT iPed)
	SWITCH iPed
		CASE 0	// Jimmy
		CASE 1	// Wendy
			RETURN PED_JIMMY_LISTEN_DISTANCE
	ENDSWITCH
	RETURN PED_LISTEN_DISTANCE
ENDFUNC

/// PURPOSE: TEMP FUNCTION. Checks if player is near ped about to speak.
///    		 Can currently hear ped speaking in Arcade when local player in Basement. Audio issue?
FUNC BOOL IS_LOCAL_PLAYER_IN_LISTEN_DISTANCE_TO_PED(PED_DATA &Data, INT iPed)
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
	AND IS_ENTITY_ALIVE(Data.PedID)
		RETURN (GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(Data.PedID)) < GET_PED_LISTEN_DISTANCE(iPed))
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC FLOAT GET_PED_SPEECH_AREA(INT iPed, PED_SPEECH_TYPE eSpeechType)
	FLOAT fGreetArea = 0.0
	SWITCH iPed
		CASE 0	// Jimmy
			SWITCH eSpeechType
				CASE PST_GREETING	fGreetArea = PED_GREET_DISTANCE					BREAK
				CASE PST_BYE		fGreetArea = PED_BYE_DISTANCE					BREAK
			ENDSWITCH
		BREAK
		CASE 1	// Wendy
			SWITCH eSpeechType
				CASE PST_GREETING	fGreetArea = PED_GREET_DISTANCE					BREAK
				CASE PST_BYE		fGreetArea = PED_BYE_DISTANCE					BREAK
			ENDSWITCH
		BREAK
		CASE 2	// Driver
			SWITCH eSpeechType
				CASE PST_GREETING	fGreetArea = PED_DRIVER_GREET_DISTANCE			BREAK
				CASE PST_BYE		fGreetArea = PED_DRIVER_BYE_DISTANCE			BREAK
			ENDSWITCH
		BREAK	
		CASE 3	// Hacker
			SWITCH eSpeechType
				CASE PST_GREETING	fGreetArea = PED_HACKER_GREET_DISTANCE			BREAK
				CASE PST_BYE		fGreetArea = PED_HACKER_BYE_DISTANCE			BREAK
			ENDSWITCH
		BREAK
		CASE 4	// Weapon Expert
			SWITCH eSpeechType
				CASE PST_GREETING	fGreetArea = PED_WEAPON_EXPERT_GREET_DISTANCE	BREAK
				CASE PST_BYE		fGreetArea = PED_WEAPON_EXPERT_BYE_DISTANCE		BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN fGreetArea
ENDFUNC

FUNC BOOL DOES_PED_PLAY_SPEECH_TYPE(INT iPed, PED_SPEECH_TYPE eSpeechType)
	SWITCH iPed
		CASE 0	// Jimmy
			SWITCH eSpeechType
				CASE PST_GREETING
				CASE PST_BYE
				CASE PST_BUMP
				CASE PST_LOITERING
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 1	// Wendy
			SWITCH eSpeechType
				CASE PST_GREETING
				CASE PST_BYE
				CASE PST_BUMP
				CASE PST_LOITERING
					IF NOT PedData.bCabinetsSetupMissionComplete
					OR NOT PedData.bScopeOutSetupMissionComplete
						RETURN TRUE
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 2	// Driver
			SWITCH PedData.eHeistDriver
				CASE CASINO_HEIST_DRIVER__KARIM_DENZ
					SWITCH eSpeechType
						CASE PST_GREETING
						CASE PST_BYE
						CASE PST_BUMP
						CASE PST_LOITERING
							RETURN TRUE
					ENDSWITCH
				BREAK
				CASE CASINO_HEIST_DRIVER__TALIANA_MARTINEZ
					SWITCH eSpeechType
						CASE PST_GREETING
						CASE PST_BYE
						CASE PST_BUMP
							RETURN TRUE
					ENDSWITCH
				BREAK
				CASE CASINO_HEIST_DRIVER__EDDIE_TOH
					SWITCH eSpeechType
						CASE PST_GREETING
						CASE PST_BYE
						CASE PST_BUMP
							RETURN TRUE
					ENDSWITCH
				BREAK
				CASE CASINO_HEIST_DRIVER__ZACH
					SWITCH eSpeechType
						CASE PST_GREETING
						CASE PST_BYE
						CASE PST_BUMP
							RETURN TRUE
					ENDSWITCH
				BREAK
				CASE CASINO_HEIST_DRIVER__WEAPONS_EXPERT
					SWITCH eSpeechType
						CASE PST_GREETING
						CASE PST_BYE
						CASE PST_BUMP
							RETURN TRUE
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE 3	// Hacker
			SWITCH PedData.eHeistHacker
				CASE CASINO_HEIST_HACKER__RICKIE_LUKENS
					SWITCH eSpeechType
						CASE PST_GREETING
						CASE PST_BYE
						CASE PST_BUMP
							RETURN TRUE
					ENDSWITCH
				BREAK
				CASE CASINO_HEIST_HACKER__CHRISTIAN_FELTZ
					SWITCH eSpeechType
						CASE PST_GREETING
						CASE PST_BYE
							RETURN TRUE
					ENDSWITCH
				BREAK
				CASE CASINO_HEIST_HACKER__YOHAN
					SWITCH eSpeechType
						CASE PST_GREETING
						CASE PST_BYE
						CASE PST_BUMP
							RETURN TRUE
					ENDSWITCH
				BREAK
				CASE CASINO_HEIST_HACKER__AVI_SCHWARTZMAN
					SWITCH eSpeechType
						CASE PST_GREETING
						CASE PST_BYE
						CASE PST_BUMP
						CASE PST_LOITERING
							RETURN TRUE
					ENDSWITCH
				BREAK
				CASE CASINO_HEIST_HACKER__PAIGE_HARRIS
					SWITCH eSpeechType
						CASE PST_GREETING
						CASE PST_BYE
						CASE PST_BUMP
						CASE PST_LOITERING
							RETURN TRUE
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE 4	// Weapon Expert
			SWITCH PedData.eHeistWeaponExpert
				CASE CASINO_HEIST_WEAPON_EXPERT__KARL_ABOLAJI
					SWITCH eSpeechType
						CASE PST_GREETING
						CASE PST_BYE
						CASE PST_BUMP
							RETURN TRUE
					ENDSWITCH
				BREAK
				CASE CASINO_HEIST_WEAPON_EXPERT__GUSTAVO_MOTA
					SWITCH eSpeechType
						CASE PST_GREETING
						CASE PST_BYE
						CASE PST_BUMP
							RETURN TRUE
					ENDSWITCH
				BREAK
				CASE CASINO_HEIST_WEAPON_EXPERT__CHARLIE
					SWITCH eSpeechType
						CASE PST_GREETING
						CASE PST_BYE
						CASE PST_BUMP
							RETURN TRUE
					ENDSWITCH
				BREAK
				CASE CASINO_HEIST_WEAPON_EXPERT__WEAPONS_EXPERT
					SWITCH eSpeechType
						CASE PST_GREETING
						CASE PST_BYE
						CASE PST_BUMP
						CASE PST_LOITERING
							RETURN TRUE
					ENDSWITCH
				BREAK
				CASE CASINO_HEIST_WEAPON_EXPERT__PACKIE_MCREARY
					SWITCH eSpeechType
						CASE PST_GREETING
						CASE PST_BYE
						CASE PST_BUMP
							RETURN TRUE
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC STRING GET_PED_SPEECH_PLAYED_BIT_NAME(PED_SPEECH_TYPE eSpeechType)
	SWITCH eSpeechType
		CASE PST_GREETING	RETURN "BS_ARCADE_PEDS_SPEECH_GREETING_PLAYED"
		CASE PST_BYE		RETURN "BS_ARCADE_PEDS_SPEECH_BYE_PLAYED"
		CASE PST_BUMP		RETURN "BS_ARCADE_PEDS_SPEECH_BUMP_PLAYED"
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC INT GET_PED_SPEECH_PLAYED_BIT(PED_SPEECH_TYPE eSpeechType)
	SWITCH eSpeechType
		CASE PST_GREETING	RETURN BS_ARCADE_PEDS_SPEECH_GREETING_PLAYED
		CASE PST_BYE		RETURN BS_ARCADE_PEDS_SPEECH_BYE_PLAYED
		CASE PST_BUMP		RETURN BS_ARCADE_PEDS_SPEECH_BUMP_PLAYED
	ENDSWITCH
	RETURN -1
ENDFUNC

PROC SET_PED_SPEECH_PLAYED(PED_DATA &Data, PED_SPEECH_TYPE eSpeechType, INT iPed, BOOL bSet)
	INT iBit = GET_PED_SPEECH_PLAYED_BIT(eSpeechType)
	IF (iBit = -1)
	OR (iPed = -1)
		EXIT
	ENDIF
	
	IF bSet
		IF NOT IS_BIT_SET(Data.iBS, iBit)
			SET_BIT(Data.iBS, iBit)
			PRINTLN("[AM_MP_ARCADE_PEDS] SET_PED_SPEECH_PLAYED - Ped: ", iPed, " Setting bit: ", GET_PED_SPEECH_PLAYED_BIT_NAME(eSpeechType))
		ENDIF
	ELSE
		IF IS_BIT_SET(Data.iBS, iBit)
			CLEAR_BIT(Data.iBS, iBit)
			PRINTLN("[AM_MP_ARCADE_PEDS] SET_PED_SPEECH_PLAYED - Ped: ", iPed, " Clearing bit: ", GET_PED_SPEECH_PLAYED_BIT_NAME(eSpeechType))
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Each speech type has a corresponding bit which gets set if the dialogue has played.
FUNC BOOL HAS_PED_SPEECH_PLAYED(PED_DATA &Data, PED_SPEECH_TYPE eSpeechType)
	INT iSpeechBit = GET_PED_SPEECH_PLAYED_BIT(eSpeechType)
	IF (iSpeechBit != -1)
		RETURN IS_BIT_SET(Data.iBS, GET_PED_SPEECH_PLAYED_BIT(eSpeechType))
	ENDIF
	RETURN FALSE
ENDFUNC




FUNC STRING GET_PED_SPEECH_TRIGGERED_BIT_NAME(PED_SPEECH_TYPE eSpeechType)
	SWITCH eSpeechType
		CASE PST_GREETING	RETURN "BS_ARCADE_PEDS_SPEECH_GREETING_TRIGGERED"
		CASE PST_BYE		RETURN "BS_ARCADE_PEDS_SPEECH_BYE_TRIGGERED"
		CASE PST_BUMP		RETURN "BS_ARCADE_PEDS_SPEECH_BUMP_TRIGGERED"
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC INT GET_PED_SPEECH_TRIGGERED_BIT(PED_SPEECH_TYPE eSpeechType)
	SWITCH eSpeechType
		CASE PST_GREETING	RETURN BS_ARCADE_PEDS_SPEECH_GREETING_TRIGGERED
		CASE PST_BYE		RETURN BS_ARCADE_PEDS_SPEECH_BYE_TRIGGERED
		CASE PST_BUMP		RETURN BS_ARCADE_PEDS_SPEECH_BUMP_TRIGGERED
	ENDSWITCH
	RETURN -1
ENDFUNC

PROC SET_PED_SPEECH_TRIGGERED(PED_DATA &Data, PED_SPEECH_TYPE eSpeechType, INT iPed, BOOL bSet)
	INT iBit = GET_PED_SPEECH_TRIGGERED_BIT(eSpeechType)
	IF (iBit = -1)
	OR (iPed = -1)
		EXIT
	ENDIF
	
	IF bSet
		IF NOT IS_BIT_SET(Data.iBS, iBit)
			SET_BIT(Data.iBS, iBit)
			PRINTLN("[AM_MP_ARCADE_PEDS] SET_PED_SPEECH_TRIGGERED - Ped: ", iPed, " Setting bit: ", GET_PED_SPEECH_TRIGGERED_BIT_NAME(eSpeechType))
		ENDIF
	ELSE
		IF IS_BIT_SET(Data.iBS, iBit)
			CLEAR_BIT(Data.iBS, iBit)
			PRINTLN("[AM_MP_ARCADE_PEDS] SET_PED_SPEECH_TRIGGERED - Ped: ", iPed, " Clearing bit: ", GET_PED_SPEECH_TRIGGERED_BIT_NAME(eSpeechType))
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Each speech type has a corresponding bit which gets set if the dialogue has played.
FUNC BOOL HAS_PED_SPEECH_TRIGGERED(PED_DATA &Data, PED_SPEECH_TYPE eSpeechType)
	INT iSpeechBit = GET_PED_SPEECH_TRIGGERED_BIT(eSpeechType)
	IF (iSpeechBit != -1)
		RETURN IS_BIT_SET(Data.iBS, GET_PED_SPEECH_TRIGGERED_BIT(eSpeechType))
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_TOUCHING_PED(PED_DATA &Data, INT iPed)
	SWITCH iPed
		CASE 3	// Haacker
			IF (serverBD.iHeistHackerSpawnLocation = 0)	// On Computer
				IF DOES_ENTITY_EXIST(PedData.objHackerChair)
					IF IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), Data.PedID)
					OR IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), PedData.objHackerChair)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	RETURN IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), Data.PedID)
ENDFUNC

FUNC BOOL IS_ARCADE_PED_A_HEIST_CREW_MEMBER(INT iPed)
	RETURN (iPed >= 2 AND iPed <= 4)
ENDFUNC

FUNC BOOL SHOULD_TRIGGER_PED_SPEECH(PED_DATA &Data, PED_SPEECH_TYPE eSpeechType, INT iPed)
	
	IF NOT DOES_PED_PLAY_SPEECH_TYPE(iPed, eSpeechType)	// Can ped play this type of speech?
		RETURN FALSE
	ENDIF
	
	SWITCH eSpeechType
		CASE PST_GREETING
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
			AND IS_ENTITY_ALIVE(Data.PedID)
				IF IS_ARCADE_PED_A_HEIST_CREW_MEMBER(iPed)
					SWITCH iPed
						CASE 2	// Driver
							IF NOT HAS_NET_TIMER_STARTED(PedData.stDriverGreetingTimer)
								RETURN (GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(Data.PedID)) < GET_PED_SPEECH_AREA(iPed, eSpeechType))
							ELSE
								IF HAS_NET_TIMER_EXPIRED(PedData.stDriverGreetingTimer, CREW_GREETING_BUFFER_TIME_MS)
									RESET_NET_TIMER(PedData.stDriverGreetingTimer)
								ENDIF
							ENDIF
						BREAK
						CASE 3	// Hacker
							IF NOT HAS_NET_TIMER_STARTED(PedData.stHackerGreetingTimer)
								RETURN (GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(Data.PedID)) < GET_PED_SPEECH_AREA(iPed, eSpeechType))
							ELSE
								IF HAS_NET_TIMER_EXPIRED(PedData.stHackerGreetingTimer, CREW_GREETING_BUFFER_TIME_MS)
									RESET_NET_TIMER(PedData.stHackerGreetingTimer)
								ENDIF
							ENDIF
						BREAK
						CASE 4	// Weapons Expert
							IF NOT HAS_NET_TIMER_STARTED(PedData.stWeaponsExpertGreetingTimer)
								RETURN (GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(Data.PedID)) < GET_PED_SPEECH_AREA(iPed, eSpeechType))
							ELSE
								IF HAS_NET_TIMER_EXPIRED(PedData.stWeaponsExpertGreetingTimer, CREW_GREETING_BUFFER_TIME_MS)
									RESET_NET_TIMER(PedData.stWeaponsExpertGreetingTimer)
								ENDIF
							ENDIF
						BREAK
					ENDSWITCH
				ELSE
					RETURN (GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(Data.PedID)) < GET_PED_SPEECH_AREA(iPed, eSpeechType))
				ENDIF
			ENDIF
		BREAK
		CASE PST_BYE
			IF HAS_PED_SPEECH_PLAYED(Data, PST_GREETING)
				IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
				AND IS_ENTITY_ALIVE(Data.PedID)
					RETURN (GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(Data.PedID)) > GET_PED_SPEECH_AREA(iPed, eSpeechType))
				ENDIF
			ENDIF
		BREAK
		CASE PST_LOITERING
			IF NOT HAS_NET_TIMER_STARTED(PedData.stLoiterTimerBuffer)
				IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
				AND IS_ENTITY_ALIVE(Data.PedID)
					
					// Need to loiter closer to heist crew due to their close coords to each other
					PED_SPEECH_TYPE eDistanceTrigger 
					eDistanceTrigger= PST_BYE
					IF IS_ARCADE_PED_A_HEIST_CREW_MEMBER(iPed)
						eDistanceTrigger = PST_GREETING
					ENDIF
					
					IF (GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(Data.PedID)) < GET_PED_SPEECH_AREA(iPed, eDistanceTrigger))
						IF NOT IS_BIT_SET(Data.iBS, BS_ARCADE_PEDS_LOITER_TIMER_SET)
							SET_BIT(Data.iBS, BS_ARCADE_PEDS_LOITER_TIMER_SET)
							START_NET_TIMER(PedData.stLoiterTimer)
							PedData.iLoiterPedID = iPed
							
							#IF IS_DEBUG_BUILD
							PedData.iLoiterTimeSet = GET_GAME_TIMER()
							#ENDIF
						ELSE
							IF (PedData.iLoiterPedID = iPed)
								IF HAS_NET_TIMER_STARTED(PedData.stLoiterTimer)
								AND HAS_NET_TIMER_EXPIRED(PedData.stLoiterTimer, PED_SPEECH_LOITER_TIME_MS)
									// Keep hitting until buffer timers have finished and ready to play
									IF NOT IS_PED_CURRENTLY_PLAYING_SPEECH(iPed)
										RETURN TRUE
									ENDIF
								ENDIF
							ELSE
								IF IS_ENTITY_ALIVE(PedData.ArcadePed[PedData.iLoiterPedID].PedID)
									IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(Data.PedID)) < GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(PedData.ArcadePed[PedData.iLoiterPedID].PedID))
										REINIT_NET_TIMER(PedData.stLoiterTimer)
										PedData.iLoiterPedID = iPed
										
										#IF IS_DEBUG_BUILD
										PedData.iLoiterTimeSet = GET_GAME_TIMER()
										#ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF (PedData.iLoiterPedID = iPed)
						AND IS_BIT_SET(Data.iBS, BS_ARCADE_PEDS_LOITER_TIMER_SET)
							CLEAR_BIT(Data.iBS, BS_ARCADE_PEDS_LOITER_TIMER_SET)
							RESET_NET_TIMER(PedData.stLoiterTimer)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF HAS_NET_TIMER_EXPIRED(PedData.stLoiterTimerBuffer, PED_SPEECH_BUFFER_TIME_MS)
					RESET_NET_TIMER(PedData.stLoiterTimerBuffer)
				ENDIF
			ENDIF
		BREAK
		CASE PST_BUMP
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
			AND IS_ENTITY_ALIVE(Data.PedID)
				IF NOT HAS_PED_SPEECH_PLAYED(Data, eSpeechType)
					IF IS_PLAYER_TOUCHING_PED(Data, iPed)
						RETURN TRUE
					ENDIF
				ELSE
					IF NOT IS_PLAYER_TOUCHING_PED(Data, iPed)
						SET_PED_SPEECH_PLAYED(Data, eSpeechType, iPed, FALSE)
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC STRING GET_PED_SPEECH_CONTEXT(PED_SPEECH_TYPE eSpeechType, INT iPed)
	STRING sContext = ""
	SWITCH iPed
		CASE 0	// Jimmy
			SWITCH eSpeechType
				CASE PST_GREETING		sContext = "ARCADE_HI"				BREAK
				CASE PST_BYE			sContext = "ARCADE_BYE"				BREAK
				CASE PST_BUMP			sContext = "ARCADE_BUMP"			BREAK
				CASE PST_LOITERING		sContext = "ARCADE_LOITER"			BREAK
				CASE PST_TV_TURNED_OFF	sContext = "ARCADE_TV_TURNEDOFF"	BREAK
				CASE PST_WHATS_UP		sContext = "ARCADE_HOWSITGOING"		BREAK
				CASE PST_ACTIVITY
					SWITCH serverBD.iJimmySpawnLocation
						CASE 0	// Arcade - Sofa - Watching TV
						CASE 1	// Arcade - Sofa - Drinking and Watching TV
							sContext = "ARCADE_TV_OFF"
							IF g_bTVOn
								sContext = "ARCADE_WATCHING_TV"
							ENDIF
						BREAK
						CASE 3	// Arcade - Standing - Phone Call
							sContext = "ARCADE_PHONE"
						BREAK
						CASE 4	// Arcade - Standing - Texting
							sContext = "ARCADE_TEXTING"
						BREAK
						CASE 5	// Arcade - Lying down behind bar - Smoking
							sContext = "ARCADE_SMOKING"
						BREAK
					ENDSWITCH
					
					// Pre-setup speech
					IF NOT PedData.bCabinetsSetupMissionComplete
					AND NOT PedData.bScopeOutSetupMissionComplete
						sContext = "ARCADE_SETUP"
						
					ELIF PedData.bCabinetsSetupMissionComplete
					AND NOT PedData.bScopeOutSetupMissionComplete
						sContext = "ARCADE_TEXTING"
					ENDIF
				BREAK
				CASE PST_POPULARITY
					sContext = "ARCADE_LOW"
					IF (serverBD.iLevel >= PED_SPEECH_POPULARITY_LEVEL)
						sContext = "ARCADE_HIGH"
					ENDIF
				BREAK
				CASE PST_CABINETS
					sContext = "ARCADE_CABINETS"
					IF serverBD.iOwnedArcadeTypes = MAX_NUM_ARCADE_MACHINE_TYPES
						sContext = "ARCADE_ALL_CABINETS"
					ENDIF
				BREAK
				CASE PST_CABINET_TYPE
					SWITCH serverBD.eJimmyCabinetSpeech
						CASE ARCADE_CABINET_CH_GG_SPACE_MONKEY			sContext = "ARCADE_SPACE"		BREAK
						CASE ARCADE_CABINET_CH_LAST_GUNSLINGERS			sContext = "ARCADE_LIGHTGUN"	BREAK
						CASE ARCADE_CABINET_CH_NIGHT_DRIVE_CAR			sContext = "ARCADE_DRIVING"		BREAK
						CASE ARCADE_CABINET_CH_NIGHT_DRIVE_TRUCK		sContext = "ARCADE_DRIVING"		BREAK
						CASE ARCADE_CABINET_CH_NIGHT_DRIVE_BIKE			sContext = "ARCADE_DRIVING"		BREAK
						CASE ARCADE_CABINET_CA_INVADE_AND_PERSUADE		sContext = "ARCADE_INVADE"		BREAK
						CASE ARCADE_CABINET_CH_FORTUNE_TELLER			sContext = "ARCADE_FORTUNE"		BREAK
						CASE ARCADE_CABINET_CH_CLAW_CRANE				sContext = "ARCADE_CRANE"		BREAK
						CASE ARCADE_CABINET_CH_LOVE_METER				sContext = "ARCADE_LOVETEST"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE 1	// Wendy
			SWITCH eSpeechType
				CASE PST_GREETING		sContext = "ARCADE_PLAYER_HI"		BREAK
				CASE PST_BYE			sContext = "ARCADE_PLAYER_BYE"		BREAK
				CASE PST_BUMP			sContext = "BUMP"					BREAK
				CASE PST_LOITERING		sContext = "ARCADE_LOITER"			BREAK
			ENDSWITCH
		BREAK
		CASE 2	// Driver
			SWITCH eSpeechType
				CASE PST_GREETING	sContext = "ARCADE_HI"		BREAK
				CASE PST_BYE		sContext = "ARCADE_BYE"		BREAK
				CASE PST_BUMP		sContext = "ARCADE_BUMP"	BREAK
				CASE PST_LOITERING	sContext = "ARCADE_LOITER"	BREAK
				CASE PST_IDLE		sContext = "ARCADE_IDLE"	BREAK
				CASE PST_ACTIVITY
					IF (serverBD.iHeistDriverSpawnLocation = 2)	// Phone Call
						sContext = "ARCADE_PHONE"
					ENDIF
				BREAK
				CASE PST_NO_VEHICLE
					SWITCH PedData.eHeistDriver
						CASE CASINO_HEIST_DRIVER__TALIANA_MARTINEZ
							IF NOT PedData.bAcquiredGetawayVehicles
								sContext = "ARCADE_NOVEHICLE"
							ENDIF
						BREAK
						CASE CASINO_HEIST_DRIVER__EDDIE_TOH
							IF NOT PedData.bAcquiredGetawayVehicles
								sContext = "ARCADE_NOVEHICLE"
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				CASE PST_UPGRADE
					SWITCH PedData.eHeistDriver
						CASE CASINO_HEIST_DRIVER__ZACH
							IF g_iArcadeModMenuPurchaseOption = GETAWAY_VEH_MOD_UPGRADE2
								sContext = "ARCADE_FIRST_UPGRADE"
							ELIF g_iArcadeModMenuPurchaseOption = GETAWAY_VEH_MOD_UPGRADE3
								sContext = "ARCADE_SECOND_UPGRADE"
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE 3	// Hacker
			SWITCH eSpeechType
				CASE PST_GREETING	sContext = "ARCADE_HI"		BREAK
				CASE PST_BYE		sContext = "ARCADE_BYE"		BREAK
				CASE PST_BUMP		sContext = "ARCADE_BUMP"	BREAK
				CASE PST_LOITERING	sContext = "ARCADE_LOITER"	BREAK
				CASE PST_ACTIVITY
					IF (serverBD.iHeistHackerSpawnLocation = 0)	// On Computer
						sContext = "ARCADE_COMPUTER"
					ENDIF
				BREAK
			ENDSWITCH	
		BREAK
		CASE 4	// Weapon Expert
			SWITCH eSpeechType
				CASE PST_GREETING	sContext = "ARCADE_HI"		BREAK
				CASE PST_BYE		sContext = "ARCADE_BYE"		BREAK
				CASE PST_BUMP		sContext = "ARCADE_BUMP"	BREAK
				CASE PST_LOITERING	sContext = "ARCADE_LOITER"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN sContext
ENDFUNC

FUNC STRING GET_PED_VOICE_NAME(INT iPed)
	STRING sVoice = ""
	SWITCH iPed
		CASE 0	// Jimmy
			sVoice = "JIMMY"
		BREAK
		CASE 1	// Wendy
			sVoice = "HS3_WENDY"
		BREAK
		CASE 2	// Driver
			SWITCH PedData.eHeistDriver
				CASE CASINO_HEIST_DRIVER__KARIM_DENZ				sVoice = "KARIM"			BREAK
				CASE CASINO_HEIST_DRIVER__TALIANA_MARTINEZ			sVoice = "TALINA"			BREAK
				CASE CASINO_HEIST_DRIVER__EDDIE_TOH					sVoice = "EDDIE"			BREAK
				CASE CASINO_HEIST_DRIVER__ZACH						sVoice = "BIKE_MECHANIC"	BREAK
				CASE CASINO_HEIST_DRIVER__WEAPONS_EXPERT			sVoice = "WEPSEXP"			BREAK
			ENDSWITCH
		BREAK
		CASE 3	// Hacker
			SWITCH PedData.eHeistHacker
				CASE CASINO_HEIST_HACKER__RICKIE_LUKENS				sVoice = "LIENGINEER"		BREAK
				CASE CASINO_HEIST_HACKER__CHRISTIAN_FELTZ			sVoice = "CHRISTIAN"		BREAK
				CASE CASINO_HEIST_HACKER__YOHAN						sVoice = "BTL_YOHAN"		BREAK
				CASE CASINO_HEIST_HACKER__AVI_SCHWARTZMAN			sVoice = "AVI"				BREAK
				CASE CASINO_HEIST_HACKER__PAIGE_HARRIS				sVoice = "PAIGE"			BREAK
			ENDSWITCH
		BREAK
		CASE 4	// Weapon Expert
			SWITCH PedData.eHeistWeaponExpert
				CASE CASINO_HEIST_WEAPON_EXPERT__KARL_ABOLAJI		sVoice = "KARL"				BREAK
				CASE CASINO_HEIST_WEAPON_EXPERT__GUSTAVO_MOTA		sVoice = "GUSTAVO"			BREAK
				CASE CASINO_HEIST_WEAPON_EXPERT__CHARLIE			sVoice = "AIRMECH"			BREAK
				CASE CASINO_HEIST_WEAPON_EXPERT__WEAPONS_EXPERT		sVoice = "WEPSEXP"			BREAK
				CASE CASINO_HEIST_WEAPON_EXPERT__PACKIE_MCREARY		sVoice = "PACKIE"			BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN sVoice
ENDFUNC

FUNC BOOL CAN_PLAY_PED_SPEECH(INT iPed, BOOL bIncludeBufferTimers = TRUE, PED_SPEECH_TYPE eSpeech = PST_INVALID, BOOL bIncludeModMenuSpeech = FALSE)
	IF iPed = 0	// Jimmy
	AND IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSFive, BS_SIMPLE_INTERIOR_GLOBAL_BS_FIVE_USING_BAR)
		PRINTLN("[AM_MP_ARCADE_PEDS] CAN_PLAY_PED_SPEECH - Bail Reason: Player has ordered a drink")
		#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF iPed = 0	// Jimmy
	AND IS_CUSTOM_MENU_ON_SCREEN()
		INT iMenuHeaderHash = GET_HASH_KEY(g_sMenuData.tl15Title)
		
		IF iMenuHeaderHash = HASH("BAR_MENU_TITLE")
			PRINTLN("[AM_MP_ARCADE_PEDS] CAN_PLAY_PED_SPEECH - Bail Reason: Player is using Bar menu")
			#IF IS_DEBUG_BUILD
			DEBUG_PRINTCALLSTACK()
			#ENDIF
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF iPed = 0	// Jimmy
	OR iPed = 1	// Wendy
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(g_niArcadeBartender)
		AND NOT IS_PED_INJURED(NET_TO_PED(g_niArcadeBartender))
		AND IS_ANY_SPEECH_PLAYING(NET_TO_PED(g_niArcadeBartender))
			PRINTLN("[AM_MP_ARCADE_PEDS] CAN_PLAY_PED_SPEECH - Bail Reason: Wendy is speaking")
			#IF IS_DEBUG_BUILD
			DEBUG_PRINTCALLSTACK()
			#ENDIF
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF NOT g_bInitArcadePedsCreated
		PRINTLN("[AM_MP_ARCADE_PEDS] CAN_PLAY_PED_SPEECH - Bail Reason: Waiting for all peds to be created first")
		#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
		PRINTLN("[AM_MP_ARCADE_PEDS] CAN_PLAY_PED_SPEECH - Bail Reason: Player is walking in or out of interior")
		#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF bIncludeBufferTimers
	AND IS_PED_CURRENTLY_PLAYING_SPEECH(iPed, bIncludeBufferTimers)
		PRINTLN("[AM_MP_ARCADE_PEDS] CAN_PLAY_PED_SPEECH - Bail Reason: Ped is currently playing speech")
		#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_SCREEN_FADING_OUT()
	OR IS_SCREEN_FADED_OUT()
		PRINTLN("[AM_MP_ARCADE_PEDS] CAN_PLAY_PED_SPEECH - Bail Reason: Screen is fading out")
		#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_BROWSER_OPEN()
		PRINTLN("[AM_MP_ARCADE_PEDS] CAN_PLAY_PED_SPEECH - Bail Reason: Browser is open")
		#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF NETWORK_IS_IN_MP_CUTSCENE()
	OR IS_PLAYER_IN_SIMPLE_CUTSCENE(PLAYER_ID())
		PRINTLN("[AM_MP_ARCADE_PEDS] CAN_PLAY_PED_SPEECH - Bail Reason: Cutscene is active")
		#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_CORONA()
	OR IS_TRANSITION_SESSION_LAUNCHING()
	OR IS_TRANSITION_SESSION_RESTARTING()
		PRINTLN("[AM_MP_ARCADE_PEDS] CAN_PLAY_PED_SPEECH - Bail Reason: Player in corona")
		#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF g_bDontCrossRunning
		PRINTLN("[AM_MP_ARCADE_PEDS] CAN_PLAY_PED_SPEECH - Bail Reason: Player is playing Dont Cross The Line")
		#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(GlobalplayerBD_CasinoHeist[NETWORK_PLAYER_ID_TO_INT()].iGeneralBitset, ciCASINO_HEIST_GENERAL_BITSET__INTERACTING_WITH_BOARDS) 
		PRINTLN("[AM_MP_ARCADE_PEDS] CAN_PLAY_PED_SPEECH - Bail Reason: Player is interacting with the heist board")
		#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF bIncludeModMenuSpeech
		IF iPed = 2												// Driver
		AND serverBD.iHeistDriverSpawnLocation = 3				// Next to getaway vehicle
		AND PedData.eHeistDriver = CASINO_HEIST_DRIVER__ZACH
			IF IS_PLAYER_USING_ARCADE_MOD_MENU(PLAYER_ID())
			AND eSpeech != PST_UPGRADE
				PRINTLN("[AM_MP_ARCADE_PEDS] CAN_PLAY_PED_SPEECH - Bail Reason: Player using arcade mod menu, only allowing PST_UPGRADE speech here")
				#IF IS_DEBUG_BUILD
				DEBUG_PRINTCALLSTACK()
				#ENDIF
				RETURN FALSE								// Block all speech except PST_UPGRADE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_PED_AVAILABLE_FOR_SPEECH(INT iPed, BOOL bIncludeBufferTimers = TRUE)
	IF NOT DOES_PED_PLAY_SPEECH(iPed)						// This ped does not play any dialogue
	OR NOT CAN_PLAY_PED_SPEECH(iPed, bIncludeBufferTimers)	// Local player cant process ped speech right now
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

PROC RESET_PED_EVENT_SAFETY_TIMER(PED_DATA &Data)
	// The event safety timer is to stop multiple events of the same type
	// being processed if triggered within 2 seconds of the inital event.
	// Skips processing the event inside PROCESS_PED_SPEECH_EVENTS if timer is active.
	IF HAS_NET_TIMER_STARTED(Data.stEventSafetyTimer)
	AND HAS_NET_TIMER_EXPIRED(Data.stEventSafetyTimer, EVENT_SAFETY_TIME_MS)
		RESET_NET_TIMER(Data.stEventSafetyTimer)
	ENDIF
ENDPROC

FUNC BOOL PLAY_PED_SPEECH(PED_DATA &Data, PED_SPEECH_TYPE eSpeechType, INT iPed)
	
	STRING sContext = GET_PED_SPEECH_CONTEXT(eSpeechType, iPed)
	IF IS_STRING_NULL_OR_EMPTY(sContext)
		PRINTLN("[AM_MP_ARCADE_PEDS] PLAY_PED_SPEECH - Bail Reason: sContext is null")
		RETURN FALSE
	ENDIF
	
	STRING sVoice = GET_PED_VOICE_NAME(iPed)
	IF IS_STRING_NULL_OR_EMPTY(sVoice)
		PRINTLN("[AM_MP_ARCADE_PEDS] PLAY_PED_SPEECH - Bail Reason: sVoice is null")
		RETURN FALSE
	ENDIF
	
	PRINTLN("[AM_MP_ARCADE_PEDS] PLAY_PED_SPEECH - Calling PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE - sContext: ", sContext, " sVoice: ", sVoice)
	PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(Data.PedID, sContext, sVoice, AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE_NORMAL), FALSE)	//SPEECH_PARAMS_FORCE_NORMAL_CLEAR
	
	RETURN TRUE
ENDFUNC

/// PURPOSE: Sets speech bits based on speech conditions
PROC SET_PED_SPEECH_DATA(PED_DATA &Data, PED_SPEECH_TYPE eSpeechType, INT iPed)
	
	SWITCH eSpeechType
		CASE PST_GREETING
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
			AND IS_ENTITY_ALIVE(Data.PedID)
				IF HAS_PED_SPEECH_TRIGGERED(Data, eSpeechType)																									// If trigger condition bit is set
				OR (GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(Data.PedID)) < GET_PED_SPEECH_AREA(iPed, PST_BYE))		// Or within bounds to trigger bye when exiting area
					SET_PED_SPEECH_TRIGGERED(Data, eSpeechType, iPed, FALSE)
					SET_PED_SPEECH_PLAYED(Data, PST_GREETING, iPed, TRUE)
					SET_PED_SPEECH_PLAYED(Data, PST_BYE, iPed, FALSE)
					CLEAR_BIT(Data.iBS, BS_ARCADE_PEDS_SPEECH_EVENT_SENT)
					
					IF IS_ARCADE_PED_A_HEIST_CREW_MEMBER(iPed)
						SWITCH iPed
							CASE 2	// Driver
								REINIT_NET_TIMER(PedData.stDriverGreetingTimer)
								#IF IS_DEBUG_BUILD
								PedData.iDriverGreetingBufferTimerSet = GET_GAME_TIMER()
								#ENDIF
							BREAK
							CASE 3	// Hacker
								REINIT_NET_TIMER(PedData.stHackerGreetingTimer)
								#IF IS_DEBUG_BUILD
								PedData.iHackerGreetingBufferTimerSet = GET_GAME_TIMER()
								#ENDIF
							BREAK
							CASE 4	// Weapons Expert
								REINIT_NET_TIMER(PedData.stWeaponsExpertGreetingTimer)
								#IF IS_DEBUG_BUILD
								PedData.iWeaponsExpertGreetingBufferTimerSet = GET_GAME_TIMER()
								#ENDIF
							BREAK
						ENDSWITCH
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE PST_BYE
			IF HAS_PED_SPEECH_TRIGGERED(Data, eSpeechType)
				SET_PED_SPEECH_TRIGGERED(Data, eSpeechType, iPed, FALSE)
				SET_PED_SPEECH_PLAYED(Data, PST_BYE, iPed, TRUE)
				SET_PED_SPEECH_PLAYED(Data, PST_GREETING, iPed, FALSE)
				CLEAR_BIT(Data.iBS, BS_ARCADE_PEDS_SPEECH_EVENT_SENT)
			ENDIF
		BREAK
		CASE PST_LOITERING
			RESET_NET_TIMER(PedData.stLoiterTimer)
			REINIT_NET_TIMER(PedData.stLoiterTimerBuffer)
			CLEAR_BIT(Data.iBS, BS_ARCADE_PEDS_LOITER_TIMER_SET)
			CLEAR_BIT(Data.iBS, BS_ARCADE_PEDS_SPEECH_EVENT_SENT)
			
			#IF IS_DEBUG_BUILD
			PedData.iLoiterBufferTimerSet = GET_GAME_TIMER()
			#ENDIF
		BREAK
		CASE PST_BUMP
			IF HAS_PED_SPEECH_TRIGGERED(Data, eSpeechType)
				SET_PED_SPEECH_TRIGGERED(Data, eSpeechType, iPed, FALSE)
				SET_PED_SPEECH_PLAYED(Data, eSpeechType, iPed, TRUE)
				CLEAR_BIT(Data.iBS, BS_ARCADE_PEDS_SPEECH_EVENT_SENT)
			ENDIF
		BREAK
		CASE PST_TV_TURNED_OFF
			// Nothing
		BREAK
		CASE PST_UPGRADE
			g_iArcadeModMenuPurchaseOption = 0
			CLEAR_BIT(Data.iBS, BS_ARCADE_PEDS_SPEECH_UPGRADE_TRIGGERED)
		BREAK
		DEFAULT
			// Controller speech
			SWITCH iPed
				CASE 0	// Jimmy
					REINIT_NET_TIMER(serverBD.stJimmyBufferSpeechTimer)
					#IF IS_DEBUG_BUILD
					PedData.iJimmyBufferTimerSet = GET_GAME_TIMER()
					#ENDIF
				BREAK
				CASE 2	// Driver
					REINIT_NET_TIMER(serverBD.stDriverBufferSpeechTimer)
					#IF IS_DEBUG_BUILD
					PedData.iDriverBufferTimerSet = GET_GAME_TIMER()
					#ENDIF
				BREAK
				CASE 3	// Hacker
					REINIT_NET_TIMER(serverBD.stHackerBufferSpeechTimer)
					#IF IS_DEBUG_BUILD
					PedData.iHackerBufferTimerSet = GET_GAME_TIMER()
					#ENDIF
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
ENDPROC

/// PURPOSE: This gets called after a successful or failed attempt at playing dialogue.
///    		 Don't try and replay the dialougue if it fails, set that it has played and continue to next speech.
PROC SET_SPEECH_DATA(PED_DATA &Data, SCRIPT_EVENT_DATA_ARCADE_PEDS_SPEECH_UPDATE Event, BOOL bStartSafetyTimer = TRUE)
	SET_PED_SPEECH_DATA(Data, Event.eSpeech, Event.iPed)
	Data.eCurrentSpeech = Event.eSpeech
	
	IF bStartSafetyTimer
		START_NET_TIMER(Data.stEventSafetyTimer)
	ENDIF
ENDPROC

FUNC BOOL SHOULD_PED_SPEECH_PLAYING_INCLUDE_BUFFERS(PED_SPEECH_TYPE eSpeech)
	SWITCH eSpeech
		CASE PST_UPGRADE
			RETURN FALSE
	ENDSWITCH
	RETURN TRUE
ENDFUNC

PROC PROCESS_PED_SPEECH_EVENTS(PED_DATA &Data, INT iPed, INT iEventID)
	
	SCRIPT_EVENT_DATA_ARCADE_PEDS_SPEECH_UPDATE Event
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
		IF NETWORK_IS_PLAYER_A_PARTICIPANT(Event.Details.FromPlayerIndex)
			IF (Event.iPed = iPed)																								// Is the dialogue intended for this ped
				IF NOT HAS_NET_TIMER_STARTED(Data.stEventSafetyTimer)															// Safety timer stops multiple triggers of the same speech type in quick succesion
				OR (Event.eSpeech != Data.eCurrentSpeech)																		// If speech is different though and safety timer is active; continue to play new dialogue
					IF IS_LOCAL_PLAYER_IN_LISTEN_DISTANCE_TO_PED(Data, iPed)													// Is local player within listening distance to ped
					AND CAN_PLAY_PED_SPEECH(iPed, SHOULD_PED_SPEECH_PLAYING_INCLUDE_BUFFERS(Event.eSpeech), Event.eSpeech, TRUE)
						PLAY_PED_SPEECH(Data, Event.eSpeech, Event.iPed)
						SET_SPEECH_DATA(Data, Event)
					ELSE
						SET_SPEECH_DATA(Data, Event)
					ENDIF
				ELSE
					IF HAS_NET_TIMER_STARTED(Data.stEventSafetyTimer)
						SET_SPEECH_DATA(Data, Event, FALSE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_PED_EVENTS(PED_DATA &Data, INT iPed)
	
	INT iEventID
	EVENT_NAMES ThisScriptEvent
	STRUCT_EVENT_COMMON_DETAILS Details
	
	REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iEventID
	    ThisScriptEvent = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iEventID)
	   	
	    SWITCH ThisScriptEvent
			CASE EVENT_NETWORK_SCRIPT_EVENT
				GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Details, SIZE_OF(Details))
				SWITCH Details.Type
					CASE SCRIPT_EVENT_UPDATE_ARCADE_PEDS_SPEECH
						IF g_sBlockedEvents.bSCRIPT_EVENT_UPDATE_ARCADE_PEDS_SPEECH
							EXIT
						ENDIF	
						PROCESS_PED_SPEECH_EVENTS(Data, iPed, iEventID)
					BREAK
				ENDSWITCH
			BREAK
		ENDSWITCH
	ENDREPEAT
	
ENDPROC

FUNC INT GET_PED_SPEECH_TYPE_BIT(PED_SPEECH_TYPE eSpeechType, INT iPed)
	SWITCH iPed
		CASE 0	// Jimmy
			SWITCH eSpeechType
				CASE PST_POPULARITY		RETURN BS_SERVER_ARCADE_PEDS_SPEECH_CONTROLLER_TYPE_POPULARITY_SET_JIMMY
				CASE PST_CABINETS		RETURN BS_SERVER_ARCADE_PEDS_SPEECH_CONTROLLER_TYPE_CABINETS_SET_JIMMY
				CASE PST_CABINET_TYPE	RETURN BS_SERVER_ARCADE_PEDS_SPEECH_CONTROLLER_TYPE_CABINET_TYPE_SET_JIMMY
				CASE PST_WHATS_UP		RETURN BS_SERVER_ARCADE_PEDS_SPEECH_CONTROLLER_TYPE_WHATS_UP_SET_JIMMY
				CASE PST_ACTIVITY		RETURN BS_SERVER_ARCADE_PEDS_SPEECH_CONTROLLER_TYPE_ACTIVITY_SET_JIMMY
			ENDSWITCH
		BREAK
		CASE 2	// Driver
			SWITCH eSpeechType
				CASE PST_ACTIVITY		RETURN BS_SERVER_ARCADE_PEDS_SPEECH_CONTROLLER_TYPE_ACTIVITY_SET_DRIVER
				CASE PST_IDLE			RETURN BS_SERVER_ARCADE_PEDS_SPEECH_CONTROLLER_TYPE_IDLE_SET_DRIVER
				CASE PST_NO_VEHICLE		RETURN BS_SERVER_ARCADE_PEDS_SPEECH_CONTROLLER_TYPE_NO_VEHICLE_SET_DRIVER
			ENDSWITCH
		BREAK
		CASE 3	// Hacker
			SWITCH eSpeechType
				CASE PST_ACTIVITY		RETURN BS_SERVER_ARCADE_PEDS_SPEECH_CONTROLLER_TYPE_ACTIVITY_SET_HACKER
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN -1
ENDFUNC

FUNC PED_SPEECH_TYPE GET_RANDOM_PED_SPEECH_TYPE(INT iPed)
	INT iRandType = ENUM_TO_INT(PST_INVALID)
	SWITCH iPed
		CASE 0	// Jimmy
			iRandType = GET_RANDOM_INT_IN_RANGE(ENUM_TO_INT(PST_ACTIVITY), ENUM_TO_INT(PST_WHATS_UP)+1)
		BREAK
		CASE 2	// Driver
			
			INT iIdleBit
			INT iActivityBit
			INT iNoVehicleBit
			
			iIdleBit = GET_PED_SPEECH_TYPE_BIT(PST_IDLE, iPed)
			iActivityBit = GET_PED_SPEECH_TYPE_BIT(PST_ACTIVITY, iPed)
			iNoVehicleBit = GET_PED_SPEECH_TYPE_BIT(PST_NO_VEHICLE, iPed)
			
			SWITCH PedData.eHeistDriver
				CASE CASINO_HEIST_DRIVER__KARIM_DENZ
					IF (serverBD.iHeistDriverSpawnLocation = 2)	// Phone call
						iRandType = GET_RANDOM_INT_IN_RANGE(0, 2)
						IF iRandType = 0
							IF NOT IS_BIT_SET(serverBD.iBS, iIdleBit)
								iRandType = ENUM_TO_INT(PST_IDLE)
							ELIF NOT IS_BIT_SET(serverBD.iBS, iActivityBit)
								iRandType = ENUM_TO_INT(PST_ACTIVITY)
							ENDIF
						ELSE
							IF NOT IS_BIT_SET(serverBD.iBS, iActivityBit)
								iRandType = ENUM_TO_INT(PST_ACTIVITY)
							ELIF NOT IS_BIT_SET(serverBD.iBS, iIdleBit)
								iRandType = ENUM_TO_INT(PST_IDLE)
							ENDIF
						ENDIF
					ELSE
						iRandType = ENUM_TO_INT(PST_IDLE)
					ENDIF
				BREAK
				CASE CASINO_HEIST_DRIVER__TALIANA_MARTINEZ
				CASE CASINO_HEIST_DRIVER__EDDIE_TOH
					IF (serverBD.iHeistDriverSpawnLocation = 2)	// Phone call
						IF NOT PedData.bAcquiredGetawayVehicles
							iRandType = GET_RANDOM_INT_IN_RANGE(0, 2)
							IF iRandType = 0
								IF NOT IS_BIT_SET(serverBD.iBS, iNoVehicleBit)
									iRandType = ENUM_TO_INT(PST_NO_VEHICLE)
								ELIF NOT IS_BIT_SET(serverBD.iBS, iActivityBit)
									iRandType = ENUM_TO_INT(PST_ACTIVITY)
								ENDIF
							ELSE
								IF NOT IS_BIT_SET(serverBD.iBS, iActivityBit)
									iRandType = ENUM_TO_INT(PST_ACTIVITY)
								ELIF NOT IS_BIT_SET(serverBD.iBS, iNoVehicleBit)
									iRandType = ENUM_TO_INT(PST_NO_VEHICLE)
								ENDIF
							ENDIF
						ELSE
							iRandType = ENUM_TO_INT(PST_ACTIVITY)
						ENDIF
					ELSE
						IF NOT PedData.bAcquiredGetawayVehicles
							iRandType = ENUM_TO_INT(PST_NO_VEHICLE)
						ENDIF
					ENDIF
				BREAK
				CASE CASINO_HEIST_DRIVER__ZACH
					iRandType = ENUM_TO_INT(PST_ACTIVITY)
				BREAK
				CASE CASINO_HEIST_DRIVER__WEAPONS_EXPERT
					iRandType = ENUM_TO_INT(PST_ACTIVITY)
				BREAK
			ENDSWITCH
		BREAK
		CASE 3	// Hacker
			IF (serverBD.iHeistHackerSpawnLocation = 0)	// On Computer
				iRandType = ENUM_TO_INT(PST_ACTIVITY)
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN INT_TO_ENUM(PED_SPEECH_TYPE, iRandType)
ENDFUNC

FUNC INT GET_RANDOM_PED_SPEECH_TIME_OFFSET_MS(INT iPed)
	INT iRandTimeOffsetMS = GET_RANDOM_INT_IN_RANGE(PED_SPEECH_MINIMUM_TIME_OFFSET_MS, PED_SPEECH_MAXIMUM_TIME_OFFSET_MS)
	
	IF (iPed = 3)  // Hacker
		IF (serverBD.iHeistHackerSpawnLocation = 0)  // On Computer
			// More frequent occurrence for On Computer speech
			iRandTimeOffsetMS = GET_RANDOM_INT_IN_RANGE(PED_SPEECH_HACKER_COMPUTER_MINIMUM_TIME_OFFSET_MS, PED_SPEECH_HACKER_COMPUTER_MAXIMUM_TIME_OFFSET_MS)
		ENDIF
	ENDIF
	
	RETURN iRandTimeOffsetMS
ENDFUNC

FUNC BOOL IS_ANY_PLAYER_WITHIN_ARCADE_PED_LISTEN_RANGE(PED_DATA &Data, INT iPed)
	INT iParticipant
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
		    PLAYER_INDEX playerID = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
		    IF IS_NET_PLAYER_OK(playerID)
				IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
					IF (GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(GET_PLAYER_PED(playerID)), Data.vPosition) < GET_PED_LISTEN_DISTANCE(iPed))
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
	    ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_PED_ACTIVITY_HAVE_SPEECH(ARCADE_ACTIVITY eActivity)
	SWITCH eActivity
		CASE AA_JIMMY_STANDING_DRINKING
			RETURN FALSE
	ENDSWITCH
	RETURN TRUE
ENDFUNC

FUNC BOOL HAS_ALL_AMBIENT_PED_SPEECH_TRIGGERED(INT iPed)
	
	INT iBit
	INT iType
	INT iTypePlayed = 0
	INT iIdleBit = GET_PED_SPEECH_TYPE_BIT(PST_IDLE, iPed)
	INT iActivityBit = GET_PED_SPEECH_TYPE_BIT(PST_ACTIVITY, iPed)
	INT iNoVehicleBit = GET_PED_SPEECH_TYPE_BIT(PST_NO_VEHICLE, iPed)
	
	SWITCH iPed
		CASE 0	// Jimmy
			FOR iType = ENUM_TO_INT(PST_ACTIVITY) TO ENUM_TO_INT(PST_WHATS_UP)
				iBit = GET_PED_SPEECH_TYPE_BIT(INT_TO_ENUM(PED_SPEECH_TYPE, iType), iPed)
				IF IS_BIT_SET(serverBD.iBS, iBit)
					iTypePlayed++
				ENDIF
			ENDFOR
			RETURN (iTypePlayed = ((ENUM_TO_INT(PST_WHATS_UP)+1)-ENUM_TO_INT(PST_ACTIVITY)))
			
		CASE 2	// Driver
			SWITCH PedData.eHeistDriver
				CASE CASINO_HEIST_DRIVER__KARIM_DENZ
					IF (serverBD.iHeistDriverSpawnLocation = 2)	// Phone call
						RETURN IS_BIT_SET(serverBD.iBS, iIdleBit) AND IS_BIT_SET(serverBD.iBS, iActivityBit)
					ENDIF
					RETURN IS_BIT_SET(serverBD.iBS, iIdleBit)
					
				CASE CASINO_HEIST_DRIVER__TALIANA_MARTINEZ
				CASE CASINO_HEIST_DRIVER__EDDIE_TOH
					IF (serverBD.iHeistDriverSpawnLocation = 2)	// Phone call
						IF NOT PedData.bAcquiredGetawayVehicles
							RETURN IS_BIT_SET(serverBD.iBS, iNoVehicleBit) AND IS_BIT_SET(serverBD.iBS, iActivityBit)
						ENDIF
						RETURN IS_BIT_SET(serverBD.iBS, iActivityBit)
					ELSE
						IF NOT PedData.bAcquiredGetawayVehicles
							RETURN IS_BIT_SET(serverBD.iBS, iNoVehicleBit)
						ENDIF
					ENDIF
				BREAK
				CASE CASINO_HEIST_DRIVER__ZACH
					RETURN IS_BIT_SET(serverBD.iBS, iActivityBit)
				BREAK	
				CASE CASINO_HEIST_DRIVER__WEAPONS_EXPERT
					RETURN IS_BIT_SET(serverBD.iBS, iActivityBit)
					
			ENDSWITCH
		BREAK
		
		CASE 3	// Hacker
			RETURN IS_BIT_SET(serverBD.iBS, iActivityBit)
			
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC CLEAR_HEIST_DRIVER_CONTROLLER_SPEECH(INT iPed)
	
	INT iIdleBit = GET_PED_SPEECH_TYPE_BIT(PST_IDLE, iPed)
	INT iActivityBit = GET_PED_SPEECH_TYPE_BIT(PST_ACTIVITY, iPed)
	INT iNoVehicleBit = GET_PED_SPEECH_TYPE_BIT(PST_NO_VEHICLE, iPed)
	
	SWITCH PedData.eHeistDriver
		CASE CASINO_HEIST_DRIVER__KARIM_DENZ
			CLEAR_BIT(serverBD.iBS, iIdleBit) 
			CLEAR_BIT(serverBD.iBS, iActivityBit)
		BREAK
		CASE CASINO_HEIST_DRIVER__TALIANA_MARTINEZ
		CASE CASINO_HEIST_DRIVER__EDDIE_TOH
			CLEAR_BIT(serverBD.iBS, iNoVehicleBit) 
			CLEAR_BIT(serverBD.iBS, iActivityBit)
		BREAK
		CASE CASINO_HEIST_DRIVER__ZACH
			CLEAR_BIT(serverBD.iBS, iActivityBit)
		BREAK
		CASE CASINO_HEIST_DRIVER__WEAPONS_EXPERT
			CLEAR_BIT(serverBD.iBS, iActivityBit)
		BREAK
	ENDSWITCH
ENDPROC

PROC CLEAR_HEIST_HACKER_CONTROLLER_SPEECH(INT iPed)
	INT iActivityBit = GET_PED_SPEECH_TYPE_BIT(PST_ACTIVITY, iPed)
	CLEAR_BIT(serverBD.iBS, iActivityBit)
ENDPROC

FUNC INT GET_CABINET_TYPE_SPEECH_BIT(ARCADE_CABINETS eCabinet)
	SWITCH eCabinet
		CASE ARCADE_CABINET_CH_GG_SPACE_MONKEY			RETURN BS_SERVER_ARCADE_PEDS_SPEECH_CONTROLLER_CABINET_TYPE_SPACE_MONKEY_SET
		CASE ARCADE_CABINET_CH_LAST_GUNSLINGERS			RETURN BS_SERVER_ARCADE_PEDS_SPEECH_CONTROLLER_CABINET_TYPE_LAST_GUNSLINGER_SET
		CASE ARCADE_CABINET_CH_WIZARDS_SLEVE			RETURN BS_SERVER_ARCADE_PEDS_SPEECH_CONTROLLER_CABINET_TYPE_WIZARDS_SLEVE_SET
		CASE ARCADE_CABINET_CH_NIGHT_DRIVE_CAR			RETURN BS_SERVER_ARCADE_PEDS_SPEECH_CONTROLLER_CABINET_TYPE_NIGHT_DRIVE_CAR_SET
		CASE ARCADE_CABINET_CH_NIGHT_DRIVE_TRUCK		RETURN BS_SERVER_ARCADE_PEDS_SPEECH_CONTROLLER_CABINET_TYPE_NIGHT_DRIVE_TRUCK_SET
		CASE ARCADE_CABINET_CH_NIGHT_DRIVE_BIKE			RETURN BS_SERVER_ARCADE_PEDS_SPEECH_CONTROLLER_CABINET_TYPE_NIGHT_DRIVE_BIKE_SET
		CASE ARCADE_CABINET_CH_DEFENDER_OF_THE_FAITH	RETURN BS_SERVER_ARCADE_PEDS_SPEECH_CONTROLLER_CABINET_TYPE_DEFENDER_OF_FAITH_SET
		CASE ARCADE_CABINET_CH_MONKEYS_PARADISE			RETURN BS_SERVER_ARCADE_PEDS_SPEECH_CONTROLLER_CABINET_TYPE_MONKEYS_PARADISE_SET
		CASE ARCADE_CABINET_CH_PENETRATOR				RETURN BS_SERVER_ARCADE_PEDS_SPEECH_CONTROLLER_CABINET_TYPE_PENETRATOR_SET
		CASE ARCADE_CABINET_CA_INVADE_AND_PERSUADE		RETURN BS_SERVER_ARCADE_PEDS_SPEECH_CONTROLLER_CABINET_TYPE_INVADE_AND_PERSUADE_SET
		CASE ARCADE_CABINET_CA_STREET_CRIMES			RETURN BS_SERVER_ARCADE_PEDS_SPEECH_CONTROLLER_CABINET_TYPE_STREET_CRIMES_SET
		CASE ARCADE_CABINET_CH_FORTUNE_TELLER			RETURN BS_SERVER_ARCADE_PEDS_SPEECH_CONTROLLER_CABINET_TYPE_FORTUNE_TELLER_SET
		CASE ARCADE_CABINET_CH_CLAW_CRANE				RETURN BS_SERVER_ARCADE_PEDS_SPEECH_CONTROLLER_CABINET_TYPE_CLAW_CRANE_SET
		CASE ARCADE_CABINET_CH_LOVE_METER				RETURN BS_SERVER_ARCADE_PEDS_SPEECH_CONTROLLER_CABINET_TYPE_LOVE_METER_SET
	ENDSWITCH
	RETURN -1
ENDFUNC

FUNC BOOL DOES_ARCADE_CABINET_HAVE_PED_SPEECH(ARCADE_CABINETS eCabient)
	SWITCH eCabient
		CASE ARCADE_CABINET_CH_GG_SPACE_MONKEY
		CASE ARCADE_CABINET_CH_LAST_GUNSLINGERS
		CASE ARCADE_CABINET_CH_NIGHT_DRIVE_CAR
		CASE ARCADE_CABINET_CH_NIGHT_DRIVE_TRUCK
		CASE ARCADE_CABINET_CH_NIGHT_DRIVE_BIKE
		CASE ARCADE_CABINET_CA_INVADE_AND_PERSUADE
		CASE ARCADE_CABINET_CH_FORTUNE_TELLER
		CASE ARCADE_CABINET_CH_CLAW_CRANE
		CASE ARCADE_CABINET_CH_LOVE_METER
			RETURN TRUE
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_PED_PLAY_CABINET_TYPE_SPEECH()
	
	INT iPropType
	INT iCabinetType
	INT iCabinetsInstalled
	
	FOR iCabinetType = ENUM_TO_INT(ARCADE_CABINET_CH_GG_SPACE_MONKEY) TO ENUM_TO_INT(ARCADE_CABINET_CH_LOVE_METER)
		FOR iPropType = 0 TO 3
			IF IS_ARCADE_CABINET_INSTALLED(PedData.arcadeOwner, INT_TO_ENUM(ARCADE_CABINETS, iCabinetType), iPropType, ACP_ARCADE)
			AND DOES_ARCADE_CABINET_HAVE_PED_SPEECH(INT_TO_ENUM(ARCADE_CABINETS, iCabinetType))
				iCabinetsInstalled++
			ENDIF
		ENDFOR
	ENDFOR
	
	RETURN (iCabinetsInstalled > 0)
ENDFUNC

PROC SET_PED_CABINET_TYPE_SPEECH()
	
	INT iPropType
	INT iCabinetType
	INT iCabinetBit
	INT iAttemps
	BOOL bCabinetFound = FALSE
	
	INT iRandCabinet
	INT iArraySize = 0
	INT iArrayIndex = 0
	ARCADE_CABINETS eCabinetsInstalled[MAX_NUM_ARCADE_MACHINE_TYPES]
	
	// Initalise array to ARCADE_CABINET_INVALID
	REPEAT MAX_NUM_ARCADE_MACHINE_TYPES iCabinetType
		eCabinetsInstalled[iCabinetType] = ARCADE_CABINET_INVALID
	ENDREPEAT
	
	// Populate array with installed cabinets
	FOR iCabinetType = ENUM_TO_INT(ARCADE_CABINET_CH_GG_SPACE_MONKEY) TO ENUM_TO_INT(ARCADE_CABINET_CH_LOVE_METER)
		FOR iPropType = 0 TO 3
			IF IS_ARCADE_CABINET_INSTALLED(PedData.arcadeOwner, INT_TO_ENUM(ARCADE_CABINETS, iCabinetType), iPropType, ACP_ARCADE)
			AND DOES_ARCADE_CABINET_HAVE_PED_SPEECH(INT_TO_ENUM(ARCADE_CABINETS, iCabinetType))
				eCabinetsInstalled[iArrayIndex] = INT_TO_ENUM(ARCADE_CABINETS, iCabinetType)
				iArrayIndex++
			ENDIF
		ENDFOR
	ENDFOR
	
	// Get size of valid cabinets in array
	REPEAT MAX_NUM_ARCADE_MACHINE_TYPES iCabinetType
		IF eCabinetsInstalled[iCabinetType] != ARCADE_CABINET_INVALID
			iArraySize++
		ELSE
			BREAKLOOP
		ENDIF
	ENDREPEAT
	
	// Select random cabinet from valid array slots
	iRandCabinet = GET_RANDOM_INT_IN_RANGE(0, iArraySize)
	iCabinetBit = GET_CABINET_TYPE_SPEECH_BIT(eCabinetsInstalled[iRandCabinet])
	
	// Ensure this cabinets speech hasn't been played before
	WHILE IS_BIT_SET(serverBD.iBS, iCabinetBit) AND iAttemps < 10
		iRandCabinet = GET_RANDOM_INT_IN_RANGE(0, iArraySize)
		iCabinetBit = GET_CABINET_TYPE_SPEECH_BIT(eCabinetsInstalled[iRandCabinet])
		iAttemps++
	ENDWHILE
	
	// Could not find cabinet speech to play, manually set it if possible
	IF (iAttemps >= 10)
		PRINTLN("[AM_MP_ARCADE_PEDS] SET_PED_CABINET_TYPE_SPEECH - Unable to find cabinet speech, manually checking...")
		
		REPEAT iArraySize iCabinetType
			iCabinetBit = GET_CABINET_TYPE_SPEECH_BIT(eCabinetsInstalled[iCabinetType])
			IF NOT IS_BIT_SET(serverBD.iBS, iCabinetBit)
				// Found cabinet speech to play
				SET_BIT(serverBD.iBS, iCabinetBit)
				serverBD.eJimmyCabinetSpeech = eCabinetsInstalled[iCabinetType]
				bCabinetFound = TRUE
				PRINTLN("[AM_MP_ARCADE_PEDS] SET_PED_CABINET_TYPE_SPEECH - Found cabinet speech via manually checking. Cabinet: ", GET_ARCADE_CABINET_NAME(serverBD.eJimmyCabinetSpeech))
				BREAKLOOP
			ENDIF
		ENDREPEAT
		
		PRINTLN("[AM_MP_ARCADE_PEDS] SET_PED_CABINET_TYPE_SPEECH - Unable to find cabinet speech via manual check. Clearing bits and setting random from available cabinets")
		
		// Unable to manually set, clear bits and randomly select
		IF NOT bCabinetFound
			FOR iCabinetType = ENUM_TO_INT(ARCADE_CABINET_CH_GG_SPACE_MONKEY) TO ENUM_TO_INT(ARCADE_CABINET_CH_LOVE_METER)
				CLEAR_BIT(serverBD.iBS, GET_CABINET_TYPE_SPEECH_BIT(INT_TO_ENUM(ARCADE_CABINETS, iCabinetType)))
			ENDFOR
			
			iRandCabinet = GET_RANDOM_INT_IN_RANGE(0, iArraySize)
			iCabinetBit = GET_CABINET_TYPE_SPEECH_BIT(eCabinetsInstalled[iRandCabinet])
			
			// This is out cabinet speech
			SET_BIT(serverBD.iBS, iCabinetBit)
			serverBD.eJimmyCabinetSpeech = eCabinetsInstalled[iRandCabinet]
			PRINTLN("[AM_MP_ARCADE_PEDS] SET_PED_CABINET_TYPE_SPEECH - Cabinet speech finally found. Cabinet: ", GET_ARCADE_CABINET_NAME(serverBD.eJimmyCabinetSpeech))
		ENDIF
	ELSE
		SET_BIT(serverBD.iBS, iCabinetBit)
		serverBD.eJimmyCabinetSpeech = eCabinetsInstalled[iRandCabinet]
		PRINTLN("[AM_MP_ARCADE_PEDS] SET_PED_CABINET_TYPE_SPEECH - Found cabinet speech first time. Cabinet: ", GET_ARCADE_CABINET_NAME(serverBD.eJimmyCabinetSpeech))
	ENDIF
	
ENDPROC

/// PURPOSE: Only host of script processes this. Sets ambient dialogue to play.
PROC SET_PED_SPEECH_AMBIENT_DIALOGUE(PED_DATA &Data, INT iPed)
	
	INT iAttempts
	BOOL bSetControllerSpeech = TRUE
	PED_SPEECH_TYPE eRandType
	
	SWITCH iPed
		CASE 0	// Jimmy
			
			// Speech already set and waiting to be triggered
			IF (serverBD.eJimmySpeechType != PST_INVALID)
				bSetControllerSpeech = FALSE
			ENDIF
			
			IF HAS_NET_TIMER_STARTED(serverBD.stJimmyBufferSpeechTimer)
				bSetControllerSpeech = FALSE
			ENDIF
			
			IF NOT bSetControllerSpeech
				EXIT
			ENDIF
			
			INT iType
			
			// Clear bits if all speech has been triggered.
			// This will go on to random order and trigger speech again.
			IF HAS_ALL_AMBIENT_PED_SPEECH_TRIGGERED(iPed)
				FOR iType = ENUM_TO_INT(PST_ACTIVITY) TO ENUM_TO_INT(PST_WHATS_UP)
					CLEAR_BIT(serverBD.iBS, GET_PED_SPEECH_TYPE_BIT(INT_TO_ENUM(PED_SPEECH_TYPE, iType), iPed))
				ENDFOR
			ENDIF
			
			// Some activities do not have specialised speech.
			// Set the activity bit for these cases so it doesn't try to play nothing.
			IF NOT DOES_PED_ACTIVITY_HAVE_SPEECH(INT_TO_ENUM(ARCADE_ACTIVITY, Data.iActivity))
				SET_BIT(serverBD.iBS, GET_PED_SPEECH_TYPE_BIT(PST_ACTIVITY, iPed))
			ENDIF
			
			// Pre setup speech only uses PST_ACTIVITY AND PST_WHATS_UP
			IF NOT PedData.bCabinetsSetupMissionComplete 
			OR NOT PedData.bScopeOutSetupMissionComplete
				FOR iType = ENUM_TO_INT(PST_POPULARITY) TO ENUM_TO_INT(PST_CABINET_TYPE)
					SET_BIT(serverBD.iBS, GET_PED_SPEECH_TYPE_BIT(INT_TO_ENUM(PED_SPEECH_TYPE, iType), iPed))
				ENDFOR
			ENDIF
			
			// Randomly pick a type of dialogue
			eRandType = GET_RANDOM_PED_SPEECH_TYPE(iPed)
			WHILE (IS_BIT_SET(serverBD.iBS, GET_PED_SPEECH_TYPE_BIT(eRandType, iPed)) AND iAttempts < 10)
				// Dialogue has already played (bit is set). Find another type of dialogue to play (only once all types have played does the bitset get cleared)
				eRandType = GET_RANDOM_PED_SPEECH_TYPE(iPed)
				iAttempts++
			ENDWHILE
			
			// Unable to find dialogue type. Manually set it
			IF (iAttempts >= 10)
				FOR iType = ENUM_TO_INT(PST_ACTIVITY) TO ENUM_TO_INT(PST_WHATS_UP)
					IF NOT IS_BIT_SET(serverBD.iBS, GET_PED_SPEECH_TYPE_BIT(INT_TO_ENUM(PED_SPEECH_TYPE, iType), iPed))
						eRandType = INT_TO_ENUM(PED_SPEECH_TYPE, iType)
					ENDIF
				ENDFOR
			ENDIF
			
			// Set speech type and time offset
			serverBD.eJimmySpeechType = eRandType
			serverBD.iJimmySpeechTimeOffsetMS = GET_RANDOM_PED_SPEECH_TIME_OFFSET_MS(iPed)
			REINIT_NET_TIMER(serverBD.stJimmySpeechTimer)
			
			#IF IS_DEBUG_BUILD
			PedData.iGameTimeJimmySpeechSet = GET_GAME_TIMER()
			#ENDIF
			
			PRINTLN("[AM_MP_ARCADE_PEDS] MAINTAIN_PED_SPEECH_CONTROLLER - Ped: ", iPed, " Setting next speech type to play: ", GET_ARCADE_PED_SPEECH_TYPE_STRING(eRandType), " Time offset until plays (ms): ", serverBD.iJimmySpeechTimeOffsetMS)
		BREAK
		CASE 1	// Wendy
			// No controller speech for Wendy
		BREAK
		CASE 2	// Driver
			
			// Speech already set and waiting to be triggered
			IF (serverBD.eDriverSpeechType != PST_INVALID)
				bSetControllerSpeech = FALSE
			ENDIF
			
			IF HAS_NET_TIMER_STARTED(serverBD.stDriverBufferSpeechTimer)
				bSetControllerSpeech = FALSE
			ENDIF
			
			IF NOT bSetControllerSpeech
				EXIT
			ENDIF
			
			// Clear bits if all speech has been triggered.
			// This will go on to random order and trigger speech again.
			IF HAS_ALL_AMBIENT_PED_SPEECH_TRIGGERED(iPed)
				CLEAR_HEIST_DRIVER_CONTROLLER_SPEECH(iPed)
			ENDIF
			
			// Randomly pick a type of dialogue
			eRandType = GET_RANDOM_PED_SPEECH_TYPE(iPed)
			
			// Set speech type and time offset
			serverBD.eDriverSpeechType = eRandType
			serverBD.iDriverSpeechTimeOffsetMS = GET_RANDOM_PED_SPEECH_TIME_OFFSET_MS(iPed)
			REINIT_NET_TIMER(serverBD.stDriverSpeechTimer)
			
			#IF IS_DEBUG_BUILD
			PedData.iGameTimeDriverSpeechSet = GET_GAME_TIMER()
			#ENDIF
			
			PRINTLN("[AM_MP_ARCADE_PEDS] MAINTAIN_PED_SPEECH_CONTROLLER - Ped: ", iPed, " Setting next speech type to play: ", GET_ARCADE_PED_SPEECH_TYPE_STRING(eRandType), " Time offset until plays (ms): ", serverBD.iDriverSpeechTimeOffsetMS)
		BREAK
		CASE 3	// Hacker
			
			// Speech already set and waiting to be triggered
			IF (serverBD.eHackerSpeechType != PST_INVALID)
				bSetControllerSpeech = FALSE
			ENDIF
			
			IF HAS_NET_TIMER_STARTED(serverBD.stHackerBufferSpeechTimer)
				bSetControllerSpeech = FALSE
			ENDIF
			
			IF NOT bSetControllerSpeech
				EXIT
			ENDIF
			
			// Clear bits if all speech has been triggered.
			// This will go on to random order and trigger speech again.
			IF HAS_ALL_AMBIENT_PED_SPEECH_TRIGGERED(iPed)
				CLEAR_HEIST_HACKER_CONTROLLER_SPEECH(iPed)
			ENDIF
			
			// Randomly pick a type of dialogue
			eRandType = GET_RANDOM_PED_SPEECH_TYPE(iPed)
			
			// Set speech type and time offset
			serverBD.eHackerSpeechType = eRandType
			serverBD.iHackerSpeechTimeOffsetMS = GET_RANDOM_PED_SPEECH_TIME_OFFSET_MS(iPed)
			REINIT_NET_TIMER(serverBD.stHackerSpeechTimer)
			
			#IF IS_DEBUG_BUILD
			PedData.iGameTimeHackerSpeechSet = GET_GAME_TIMER()
			#ENDIF
			
			PRINTLN("[AM_MP_ARCADE_PEDS] MAINTAIN_PED_SPEECH_CONTROLLER - Ped: ", iPed, " Setting next speech type to play: ", GET_ARCADE_PED_SPEECH_TYPE_STRING(eRandType), " Time offset until plays (ms): ", serverBD.iHackerSpeechTimeOffsetMS)
		BREAK
		CASE 4	// Weapon Expert
			// No controller speech for the weapon expert
		BREAK
	ENDSWITCH
ENDPROC

PROC MAINTAIN_JIMMY_SPEECH_TIMERS(PED_DATA &Data, INT iPed)
	
	// Trigger Jimmy Dialogue
	IF HAS_NET_TIMER_STARTED(serverBD.stJimmySpeechTimer)
	AND HAS_NET_TIMER_EXPIRED(serverBD.stJimmySpeechTimer, serverBD.iJimmySpeechTimeOffsetMS)
		IF IS_ANY_PLAYER_WITHIN_ARCADE_PED_LISTEN_RANGE(Data, iPed)
			IF NOT IS_PED_CURRENTLY_PLAYING_SPEECH(iPed)
				BOOL bBroadcastEvent = TRUE
				
				// Need to set cabinet type speech here as owner 
				// can uninstall cabinet during speech timer
				IF serverBD.eJimmySpeechType = PST_CABINET_TYPE
					IF CAN_PED_PLAY_CABINET_TYPE_SPEECH()
						SET_PED_CABINET_TYPE_SPEECH()
					ELSE
						bBroadcastEvent = FALSE
					ENDIF
				ENDIF
				
				IF bBroadcastEvent
					BROADCAST_ARCADE_PEDS_SPEECH_DATA(serverBD.eJimmySpeechType, 0)
					PRINTLN("[AM_MP_ARCADE_PEDS] MAINTAIN_PED_SPEECH_CONTROLLER - JIMMY - Trigger event for speech type: ", GET_ARCADE_PED_SPEECH_TYPE_STRING(serverBD.eJimmySpeechType))
				ELSE
					PRINTLN("[AM_MP_ARCADE_PEDS] MAINTAIN_PED_SPEECH_CONTROLLER - JIMMY - bBroadcastEvent is FALSE, not sending speech event for type: ", GET_ARCADE_PED_SPEECH_TYPE_STRING(serverBD.eJimmySpeechType))
				ENDIF
				
				INT iSpeechTypeBit = GET_PED_SPEECH_TYPE_BIT(serverBD.eJimmySpeechType, iPed)
				SET_BIT(serverBD.iBS, iSpeechTypeBit)
				
				RESET_NET_TIMER(serverBD.stJimmySpeechTimer)
				serverBD.eJimmySpeechType = PST_INVALID
				
			ELSE
				REINIT_NET_TIMER(serverBD.stJimmySpeechTimer)
				#IF IS_DEBUG_BUILD
				PedData.iGameTimeJimmySpeechSet = GET_GAME_TIMER()
				#ENDIF
				PRINTLN("[AM_MP_ARCADE_PEDS] MAINTAIN_PED_SPEECH_CONTROLLER - JIMMY - Jimmy is currently speaking. Don't send speech event. Resetting ambient timer.")
			ENDIF
		ELSE
			REINIT_NET_TIMER(serverBD.stJimmySpeechTimer)
			#IF IS_DEBUG_BUILD
			PedData.iGameTimeJimmySpeechSet = GET_GAME_TIMER()
			#ENDIF
			PRINTLN("[AM_MP_ARCADE_PEDS] MAINTAIN_PED_SPEECH_CONTROLLER - JIMMY - No player's are in listen range of Jimmy. Don't send speech event. Resetting ambient timer.")
		ENDIF
	ENDIF
	
ENDPROC

PROC MAINTAIN_HEIST_DRIVER_SPEECH_TIMERS(PED_DATA &Data, INT iPed)
	
	// Trigger Driver Dialogue
	IF HAS_NET_TIMER_STARTED(serverBD.stDriverSpeechTimer)
	AND HAS_NET_TIMER_EXPIRED(serverBD.stDriverSpeechTimer, serverBD.iDriverSpeechTimeOffsetMS)
		IF IS_ANY_PLAYER_WITHIN_ARCADE_PED_LISTEN_RANGE(Data, iPed)
			BOOL bDriverNextToHacker = TRUE
			IF (serverBD.iHeistDriverSpawnLocation = 3) // Next to getaway vehicles
				bDriverNextToHacker = FALSE
			ENDIF
			
			IF (bDriverNextToHacker AND NOT IS_PED_CURRENTLY_PLAYING_SPEECH(2, FALSE) AND NOT IS_PED_CURRENTLY_PLAYING_SPEECH(3, FALSE))	// Make sure driver and hacker both aren't speaking before triggering new driver speech
			OR (NOT bDriverNextToHacker AND NOT IS_PED_CURRENTLY_PLAYING_SPEECH(2, FALSE))													// Driver is not near hacker, just make sure driver isnt already speaking
				
				BROADCAST_ARCADE_PEDS_SPEECH_DATA(serverBD.eDriverSpeechType, 2)
				PRINTLN("[AM_MP_ARCADE_PEDS] MAINTAIN_PED_SPEECH_CONTROLLER - DRIVER - Trigger event for speech type: ", GET_ARCADE_PED_SPEECH_TYPE_STRING(serverBD.eDriverSpeechType))
				
				INT iSpeechTypeBit = GET_PED_SPEECH_TYPE_BIT(serverBD.eDriverSpeechType, iPed)
				SET_BIT(serverBD.iBS, iSpeechTypeBit)
				
				RESET_NET_TIMER(serverBD.stDriverSpeechTimer)
				serverBD.eDriverSpeechType = PST_INVALID
				
			ELSE
				REINIT_NET_TIMER(serverBD.stDriverSpeechTimer)
				#IF IS_DEBUG_BUILD
				PedData.iGameTimeDriverSpeechSet = GET_GAME_TIMER()
				#ENDIF
				PRINTLN("[AM_MP_ARCADE_PEDS] MAINTAIN_PED_SPEECH_CONTROLLER - DRIVER - Heist Driver is currently speaking. Don't send speech event. Resetting ambient timer.")
			ENDIF
		ELSE
			REINIT_NET_TIMER(serverBD.stDriverSpeechTimer)
			#IF IS_DEBUG_BUILD
			PedData.iGameTimeDriverSpeechSet = GET_GAME_TIMER()
			#ENDIF
			PRINTLN("[AM_MP_ARCADE_PEDS] MAINTAIN_PED_SPEECH_CONTROLLER - DRIVER - No player's are in listen range of Heist Driver. Don't send speech event. Resetting ambient timer.")
		ENDIF
	ENDIF
	
ENDPROC

PROC MAINTAIN_HEIST_HACKER_SPEECH_TIMERS(PED_DATA &Data, INT iPed)
	
	// Trigger Hacker Dialogue
	IF HAS_NET_TIMER_STARTED(serverBD.stHackerSpeechTimer)
	AND HAS_NET_TIMER_EXPIRED(serverBD.stHackerSpeechTimer, serverBD.iHackerSpeechTimeOffsetMS)
		IF IS_ANY_PLAYER_WITHIN_ARCADE_PED_LISTEN_RANGE(Data, iPed)
			BOOL bDriverNextToHacker = TRUE
			IF (serverBD.iHeistDriverSpawnLocation = 3) // Next to getaway vehicles
				bDriverNextToHacker = FALSE
			ENDIF
			
			IF (bDriverNextToHacker AND NOT IS_PED_CURRENTLY_PLAYING_SPEECH(2, FALSE) AND NOT IS_PED_CURRENTLY_PLAYING_SPEECH(3, FALSE))	// Make sure driver and hacker both aren't speaking before triggering new hacker speech
			OR (NOT bDriverNextToHacker AND NOT IS_PED_CURRENTLY_PLAYING_SPEECH(3, FALSE))	// Driver is not near hacker, just make sure hacker isnt already speaking
				
				BROADCAST_ARCADE_PEDS_SPEECH_DATA(serverBD.eHackerSpeechType, 3)
				PRINTLN("[AM_MP_ARCADE_PEDS] MAINTAIN_PED_SPEECH_CONTROLLER - HACKER - Trigger event for speech type: ", GET_ARCADE_PED_SPEECH_TYPE_STRING(serverBD.eHackerSpeechType))
				
				INT iSpeechTypeBit = GET_PED_SPEECH_TYPE_BIT(serverBD.eHackerSpeechType, iPed)
				SET_BIT(serverBD.iBS, iSpeechTypeBit)
				
				RESET_NET_TIMER(serverBD.stHackerSpeechTimer)
				serverBD.eHackerSpeechType = PST_INVALID
				
			ELSE
				REINIT_NET_TIMER(serverBD.stHackerSpeechTimer)
				#IF IS_DEBUG_BUILD
				PedData.iGameTimeHackerSpeechSet = GET_GAME_TIMER()
				#ENDIF
				PRINTLN("[AM_MP_ARCADE_PEDS] MAINTAIN_PED_SPEECH_CONTROLLER - HACKER - Heist Hacker is currently speaking. Don't send speech event. Resetting ambient timer.")
			ENDIF
		ELSE
			REINIT_NET_TIMER(serverBD.stHackerSpeechTimer)
			#IF IS_DEBUG_BUILD
			PedData.iGameTimeHackerSpeechSet = GET_GAME_TIMER()
			#ENDIF
			PRINTLN("[AM_MP_ARCADE_PEDS] MAINTAIN_PED_SPEECH_CONTROLLER - HACKER - No player's are in listen range of Heist Driver. Don't send speech event. Resetting ambient timer.")
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE: Only host of script processes this. Maintains the ambient dialogue timers and whether ambient dialogue should play.
PROC MAINTAIN_PED_SPEECH_TIMERS(PED_DATA &Data, INT iPed)
	SWITCH iPed
		CASE 0	// Jimmy
			MAINTAIN_JIMMY_SPEECH_TIMERS(Data, iPed)
		BREAK
		CASE 2	// Driver
			MAINTAIN_HEIST_DRIVER_SPEECH_TIMERS(Data, iPed)
		BREAK
		CASE 3	// Hacker
			MAINTAIN_HEIST_HACKER_SPEECH_TIMERS(Data, iPed)
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE: For scenarios that are triggered instantly, e.g a player turning the TV off while Jimmy is watching.
///    		 Resets the timers and attempts to play instantly.
PROC MAINTAIN_PED_SPEECH_INSTANT_TRIGGERS(PED_DATA &Data, INT iPed)
	SWITCH iPed
		CASE 0	// Jimmy
			
			// Jimmy watching TV
			IF Data.iActivity = ENUM_TO_INT(AA_JIMMY_SOFA_WATCHING_TV)
			OR Data.iActivity = ENUM_TO_INT(AA_JIMMY_SOFA_DRINKING_WATCHING_TV)
				
				IF serverBD.bTVOn != g_bTVOn
					serverBD.bTVOn = g_bTVOn
					
					// TV has just been turned off
					IF NOT serverBD.bTVOn
						IF IS_ANY_PLAYER_WITHIN_ARCADE_PED_LISTEN_RANGE(Data, iPed)
						AND NOT IS_PED_CURRENTLY_PLAYING_SPEECH(iPed)
							BROADCAST_ARCADE_PEDS_SPEECH_DATA(PST_TV_TURNED_OFF, 0)
							REINIT_NET_TIMER(serverBD.stJimmySpeechTimer)
							
							#IF IS_DEBUG_BUILD
							PedData.iGameTimeJimmySpeechSet = GET_GAME_TIMER()
							#ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		CASE 2	// Driver
			
			IF serverBD.iHeistDriverSpawnLocation = 3  // Next to getaway vehicle
			AND PedData.eHeistDriver = CASINO_HEIST_DRIVER__ZACH
			AND g_iArcadeModMenuPurchaseOption != 0
			AND NOT IS_PED_CURRENTLY_PLAYING_SPEECH(2, FALSE)
			AND NOT IS_BIT_SET(Data.iBS, BS_ARCADE_PEDS_SPEECH_UPGRADE_TRIGGERED)
				
				IF IS_PLAYER_USING_ARCADE_MOD_MENU(PLAYER_ID())
					BROADCAST_ARCADE_PEDS_SPEECH_DATA(PST_UPGRADE, 2)
					SET_BIT(Data.iBS, BS_ARCADE_PEDS_SPEECH_UPGRADE_TRIGGERED)
				ELSE
					g_iArcadeModMenuPurchaseOption = 0
				ENDIF
			ENDIF
			
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL DOES_PED_HAVE_CONTROLLER_SPEECH(INT iPed)
	SWITCH iPed
		CASE 0	// Jimmy
			RETURN TRUE
			
		CASE 1	// Wendy
			RETURN FALSE
			
		CASE 2	// Driver
			SWITCH PedData.eHeistDriver
				CASE CASINO_HEIST_DRIVER__KARIM_DENZ
					RETURN TRUE
				CASE CASINO_HEIST_DRIVER__TALIANA_MARTINEZ
				CASE CASINO_HEIST_DRIVER__EDDIE_TOH
					IF (serverBD.iHeistDriverSpawnLocation = 2)	// Phone call
					OR NOT PedData.bAcquiredGetawayVehicles
						RETURN TRUE
					ENDIF
				BREAK
				CASE CASINO_HEIST_DRIVER__ZACH
					IF (serverBD.iHeistDriverSpawnLocation = 2)	// Phone call
					OR (g_iArcadeModMenuPurchaseOption != 0)
						RETURN TRUE
					ENDIF
				BREAK
				CASE CASINO_HEIST_DRIVER__WEAPONS_EXPERT
					RETURN (serverBD.iHeistDriverSpawnLocation = 2)	// Phone call
			ENDSWITCH
		BREAK
		
		CASE 3	// Hacker
			RETURN (serverBD.iHeistHackerSpawnLocation = 0)	// On Computer
		
		CASE 4	// Weapons Expert
			RETURN FALSE
	ENDSWITCH
	RETURN FALSE
ENDFUNC

/// PURPOSE: Only host of script processes this. Maintains setting speech types to play and timers on when they should trigger.
PROC MAINTAIN_PED_SPEECH_CONTROLLER(PED_DATA &Data, INT iPed)
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF DOES_PED_HAVE_CONTROLLER_SPEECH(iPed)
			SET_PED_SPEECH_AMBIENT_DIALOGUE(Data, iPed)
			MAINTAIN_PED_SPEECH_TIMERS(Data, iPed)
			MAINTAIN_PED_SPEECH_INSTANT_TRIGGERS(Data, iPed)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Speech triggered by the local player interacting with peds
PROC MAINTAIN_PED_SPEECH_PLAYER_TRIGGERED(PED_DATA &Data, INT iPed)
	
	IF IS_BIT_SET(Data.iBS, BS_ARCADE_PEDS_SPEECH_EVENT_SENT)	// Speech event has been sent for this ped and is waiting to be received (processed in PROCESS_PED_SPEECH_EVENTS)
		EXIT
	ENDIF
	
	INT iSpeechType
	FOR iSpeechType = ENUM_TO_INT(PST_GREETING) TO ENUM_TO_INT(PST_LOITERING)
		PED_SPEECH_TYPE eSpeechType = INT_TO_ENUM(PED_SPEECH_TYPE, iSpeechType)
		IF IS_PED_SPEECH_TYPE_VALID(eSpeechType)
			IF SHOULD_TRIGGER_PED_SPEECH(Data, eSpeechType, iPed)								// Have the conditions been met to trigger the dialogue
			AND NOT HAS_PED_SPEECH_PLAYED(Data, eSpeechType)									// Ensure dialogue has not already been played (bit still set, conditions not cleared)
				SET_BIT(Data.iBS, BS_ARCADE_PEDS_SPEECH_EVENT_SENT)								// Bit to ensure event is only sent once
				SET_PED_SPEECH_TRIGGERED(Data, eSpeechType, iPed, TRUE)							// Set bit here incase player no longer meets conditions inside SHOULD_TRIGGER_PED_SPEECH when event is processed
				BROADCAST_ARCADE_PEDS_SPEECH_DATA(eSpeechType, iPed)							// Broadcast event to everyone on script to play the triggered dialogue on their local ped
			ENDIF
			
		ENDIF
	ENDFOR
	
	RESET_PED_EVENT_SAFETY_TIMER(Data)
ENDPROC

PROC UPDATE_PED_SPEECH(PED_DATA &Data, INT iPed)
	IF IS_PED_AVAILABLE_FOR_SPEECH(iPed, FALSE)
		MAINTAIN_PED_SPEECH_PLAYER_TRIGGERED(Data, iPed)	// Player triggered ped speech
		MAINTAIN_PED_SPEECH_CONTROLLER(Data, iPed)			// Host maintains ambient speech (everything player doesn't trigger themselves)
	ENDIF
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════╡ MAIN UPDATE FUNCTION ╞═════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

PROC UPDATE_ARCADE_PEDS()
	
	INT iPed
	REPEAT GET_MAX_NUM_ARCADE_PEDS() iPed
		IF SHOULD_CREATE_ARCADE_PED(PedData.ArcadePed[iPed], iPed)
			
			IF NOT DOES_ENTITY_EXIST(PedData.ArcadePed[iPed].PedID)
				CREATE_ARCADE_PED(PedData.ArcadePed[iPed], iPed)
			ELSE
				IF IS_ENTITY_DEAD(PedData.ArcadePed[iPed].PedID)
					DELETE_ARCADE_PED(PedData.ArcadePed[iPed])	
				ENDIF
			ENDIF
			
			PROCESS_PED_EVENTS(PedData.ArcadePed[iPed], iPed)
			UPDATE_PED_ACTIVITY(PedData.ArcadePed[iPed])
			UPDATE_PED_SPEECH(PedData.ArcadePed[iPed], iPed)
			
			#IF IS_DEBUG_BUILD
			IF (PedData.ArcadePed[iPed].bApplyChanges)
				DELETE_ARCADE_PED(PedData.ArcadePed[iPed])	
				PedData.ArcadePed[iPed].bApplyChanges = FALSE
			ENDIF
			#ENDIF
			
		ENDIF
	ENDREPEAT
	
	SET_ARCADE_PEDS_CREATED_FLAG()
	MAINTAIN_ARCADE_PEDS_CUTSCENE()
	MAINTAIN_ARCADE_PEDS_CABINET_MANAGEMENT_MENU()
	MAINTAIN_ARCADE_PEDS_PRIVATE_MODE()
	MAINTAIN_ARCADE_PEDS_COLLISION_CAPSULE()
	MAINTAIN_PED_SPEECH_BUFFER_TIMERS()
	
	PedData.iPedsCreatedThisFrame = 0	// Reset for next frame
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════╡ DEBUG WIDGETS ╞═════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

#IF IS_DEBUG_BUILD
FUNC STRING DEBUG_GET_HEIST_DRIVER_COMBO_BOX_TEXT(INT iOption)
	SWITCH iOption
		CASE 1	RETURN "Inspecting"
		CASE 2	RETURN "Idle"
		CASE 3	RETURN "Phone Call"
		CASE 4	RETURN "Clipboard"
	ENDSWITCH
	RETURN "-"
ENDFUNC

FUNC STRING DEBUG_GET_HEIST_HACKER_COMBO_BOX_TEXT(INT iOption)
	SWITCH iOption
		CASE 1	RETURN "Computer"
		CASE 2	RETURN "Texting"
		CASE 3	RETURN "Coffee"
		CASE 4	RETURN "Ledge"
	ENDSWITCH
	RETURN "-"
ENDFUNC

FUNC STRING DEBUG_GET_HEIST_WEAPONS_EXPERT_COMBO_BOX_TEXT(INT iOption)
	SWITCH iOption
		CASE 1	RETURN "Sat With Gun"
		CASE 2	RETURN "Testing Gun"
		CASE 3	RETURN "Idle on Phone"
	ENDSWITCH
	RETURN "-"
ENDFUNC

PROC RENDER_PED_INFO(PED_DATA &Data, INT iID, BOOL bDoHighlight = FALSE)
	
	IF Data.bSkipPed
	OR (Data.iLevel > serverBD.iLevel)
		EXIT
	ENDIF
	
	HUD_COLOURS HudColour
	
	INT iRow = 0
	INT iAlpha = 255
	INT iRed, iGreen, iBlue
	FLOAT fRowZ = 0.06
	VECTOR vActualPedPos, vDrawPos
	
	TEXT_LABEL_63 str
	str = "ArcadePed["
	str += iID
	str += "]"
	
	IF (bDoHighlight)
		HudColour = HUD_COLOUR_ORANGE
	ELSE
		HudColour = HUD_COLOUR_PURE_WHITE
	ENDIF
	GET_HUD_COLOUR(HudColour, iRed, iGreen, iBlue, iAlpha)
	
	vActualPedPos = Data.vPosition
	IF DOES_ENTITY_EXIST(Data.PedID)
		vActualPedPos = GET_ENTITY_COORDS(Data.PedID, FALSE)
	ENDIF
	vDrawPos = vActualPedPos
	
	DRAW_DEBUG_SPAWN_POINT(Data.vPosition, Data.fHeading, HudColour, 0, 0.5)
	DRAW_DEBUG_TEXT(str, <<vDrawPos.x, vDrawPos.y, vDrawPos.z + (fRowZ* TO_FLOAT(iRow))>>, iRed, iGreen, iBlue, iAlpha)
	
	iRow++
	str = "Activity: "
	str += ARCADE_ACTIVITY_TO_STRING(INT_TO_ENUM(ARCADE_ACTIVITY, Data.iActivity))
	DRAW_DEBUG_TEXT(str, <<vDrawPos.x, vDrawPos.y, vDrawPos.z + (fRowZ* TO_FLOAT(iRow))>>, iRed, iGreen, iBlue, iAlpha)
	
	iRow++
	str = "iLevel: "
	str += Data.iLevel
	DRAW_DEBUG_TEXT(str, <<vDrawPos.x, vDrawPos.y, vDrawPos.z + (fRowZ* TO_FLOAT(iRow))>>, iRed, iGreen, iBlue, iAlpha)	
	
ENDPROC

PROC CREATE_ARCADE_PED_WIDGET(PED_DATA &Data, INT iID)
	
	TEXT_LABEL_63 str
	str = "ArcadePed["
	str += iID
	str += "]"
	
	START_WIDGET_GROUP(str)
		ADD_WIDGET_BOOL("bFemale", Data.bFemale)
		ADD_WIDGET_VECTOR_SLIDER("Position", Data.vPosition, -9999.9, 9999.9, 0.01)
		ADD_WIDGET_FLOAT_SLIDER("Heading", Data.fHeading, -360.0, 360.0, 0.1)
		ADD_WIDGET_INT_SLIDER("iActivity", Data.iActivity, 0, ENUM_TO_INT(AA_TOTAL)-1, 1)
		ADD_WIDGET_INT_SLIDER("iPackedDrawable", Data.iPackedDrawable, 0, HIGHEST_INT, 1)
		ADD_WIDGET_INT_SLIDER("iPackedTexture", Data.iPackedTexture, 0, HIGHEST_INT, 1)
		ADD_WIDGET_INT_SLIDER("iLevel", Data.iLevel, 0, 6, 1)
		ADD_WIDGET_BOOL("bApplyChanges", Data.bApplyChanges)
	STOP_WIDGET_GROUP()
	
ENDPROC	

PROC CREATE_ARCADE_WIDGETS()
	
	INT iPed
	INT iActivity
	START_WIDGET_GROUP("AM_MP_ARCADE_PEDS")
		
		START_WIDGET_GROUP("ServerBD")
			ADD_WIDGET_BOOL("bServerDataInitialised", ServerBD.bServerDataInitialised)			
			ADD_WIDGET_INT_READ_ONLY("iLayout", ServerBD.iLayout)
			ADD_WIDGET_INT_READ_ONLY("iLevel", ServerBD.iLevel)
		STOP_WIDGET_GROUP()
		
		ADD_WIDGET_STRING("Changing the Layout will lose any changes to the ped data!")
		ADD_WIDGET_INT_SLIDER("Layout", PedData.iCurrentLayout, -1, 0, 0)
		ADD_WIDGET_INT_SLIDER("Level", PedData.iCurrentLevel, -1, 6, 1)
		
		ADD_WIDGET_BOOL("Output Ped Data To File", PedData.bOutputToFile)
		ADD_WIDGET_BOOL("Output Clothing Comps", PedData.bOutputClothingComps)
		ADD_WIDGET_BOOL("bShowDebug", PedData.bShowDebug)
		ADD_WIDGET_BOOL("bRecreatePeds", PedData.bRecreatePeds)
		
		ADD_WIDGET_INT_READ_ONLY("Count Level 1", PedData.iCount_Level[0])
		ADD_WIDGET_INT_READ_ONLY("Count Level 2", PedData.iCount_Level[1])
		ADD_WIDGET_INT_READ_ONLY("Count Level 3", PedData.iCount_Level[2])
		ADD_WIDGET_INT_READ_ONLY("Count Level 4", PedData.iCount_Level[3])
		ADD_WIDGET_INT_READ_ONLY("Count Level 5", PedData.iCount_Level[4])
		ADD_WIDGET_INT_READ_ONLY("Count Level 6", PedData.iCount_Level[5])
		
		START_WIDGET_GROUP("Active Peds")
			REPEAT MAX_NUM_ARCADE_PEDS iPed
				CREATE_ARCADE_PED_WIDGET(PedData.ArcadePed[iPed], iPed)
			ENDREPEAT
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Spawn Locations")
			START_WIDGET_GROUP("Jimmy Post Setup Spawn Locations")
				PedData.twJimmySpawnLocation = ADD_TEXT_WIDGET("Current Spawn Location: ")
				START_NEW_WIDGET_COMBO()
					ADD_TO_WIDGET_COMBO("-")
					FOR iActivity = ENUM_TO_INT(AA_JIMMY_SOFA_WATCHING_TV) TO ENUM_TO_INT(AA_JIMMY_LYING_DOWN_SMOKING)
						ADD_TO_WIDGET_COMBO(ARCADE_ACTIVITY_TO_STRING(INT_TO_ENUM(ARCADE_ACTIVITY, iActivity)))
					ENDFOR
				STOP_WIDGET_COMBO("Spawn Location", g_iDebugArcadeJimmySpawnLocation)
				ADD_WIDGET_BOOL("Restart Ped Script", g_bDebugResetArcadePedScript)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Heist Driver Spawn Locations")
				PedData.twHeistDriverSpawnLocation = ADD_TEXT_WIDGET("Current Spawn Location: ")
				START_NEW_WIDGET_COMBO()
					REPEAT (MAX_NUM_ARCADE_HEIST_DRIVER_SPAWN_LOCATIONS+1) iActivity
						ADD_TO_WIDGET_COMBO(DEBUG_GET_HEIST_DRIVER_COMBO_BOX_TEXT(iActivity))
					ENDREPEAT
				STOP_WIDGET_COMBO("Spawn Location", g_iDebugArcadeHeistDriverSpawnLocation)
				ADD_WIDGET_BOOL("Restart Ped Script", g_bDebugResetArcadePedScript)	
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Heist Hacker Spawn Locations")
				PedData.twHeistHackerSpawnLocation = ADD_TEXT_WIDGET("Current Spawn Location: ")
				START_NEW_WIDGET_COMBO()
					REPEAT (MAX_NUM_ARCADE_HEIST_HACKER_SPAWN_LOCATIONS+1) iActivity
						ADD_TO_WIDGET_COMBO(DEBUG_GET_HEIST_HACKER_COMBO_BOX_TEXT(iActivity))
					ENDREPEAT
				STOP_WIDGET_COMBO("Spawn Location", g_iDebugArcadeHeistHackerSpawnLocation)
				ADD_WIDGET_BOOL("Restart Ped Script", g_bDebugResetArcadePedScript)	
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Heist Weapons Expert Spawn Locations")
				PedData.twHeistWeaponExpertSpawnLocation = ADD_TEXT_WIDGET("Current Spawn Location: ")
				START_NEW_WIDGET_COMBO()
					REPEAT (MAX_NUM_ARCADE_HEIST_WEAPON_EXPERT_SPAWN_LOCATIONS+1) iActivity
						ADD_TO_WIDGET_COMBO(DEBUG_GET_HEIST_WEAPONS_EXPERT_COMBO_BOX_TEXT(iActivity))
					ENDREPEAT
				STOP_WIDGET_COMBO("Spawn Location", g_iDebugArcadeHeistWeaponsExpertrSpawnLocation)
				ADD_WIDGET_BOOL("Restart Ped Script", g_bDebugResetArcadePedScript)	
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Speech")
			ADD_WIDGET_BOOL("Draw Ped Speech Areas", PedData.bDrawSpeechAreas)
			ADD_WIDGET_INT_READ_ONLY("Loiter Timer (MS)", PedData.iLoiterTimerMS)
			ADD_WIDGET_INT_READ_ONLY("Loiter Buffer Time (MS)", PedData.iLoiterBufferTimeMS)
			START_WIDGET_GROUP("Controller Triggered Speech")
				START_WIDGET_GROUP("Jimmy")
					PedData.twJimmySpeechType = ADD_TEXT_WIDGET("Next Speech Type")
					ADD_WIDGET_INT_READ_ONLY("Speech Offset (MS)", serverBD.iJimmySpeechTimeOffsetMS)
					ADD_WIDGET_INT_READ_ONLY("Time Until Speech (MS)", PedData.iTimeUntilJimmySpeechMS)
					ADD_WIDGET_INT_READ_ONLY("Buffer Time (MS)", PedData.iJimmyBufferTimeMS)
					PedData.twJimmyCabinetSpeechType = ADD_TEXT_WIDGET("Cabinet Speech Type")
				STOP_WIDGET_GROUP()
				START_WIDGET_GROUP("Driver")
					PedData.twDriverSpeechType = ADD_TEXT_WIDGET("Next Speech Type")
					ADD_WIDGET_INT_READ_ONLY("Speech Offset (MS)", serverBD.iDriverSpeechTimeOffsetMS)
					ADD_WIDGET_INT_READ_ONLY("Time Until Speech (MS)", PedData.iTimeUntilDriverSpeechMS)
					ADD_WIDGET_INT_READ_ONLY("Buffer Time (MS)", PedData.iDriverBufferTimeMS)
				STOP_WIDGET_GROUP()
				START_WIDGET_GROUP("Hacker")
					PedData.twHackerSpeechType = ADD_TEXT_WIDGET("Next Speech Type")
					ADD_WIDGET_INT_READ_ONLY("Speech Offset (MS)", serverBD.iHackerSpeechTimeOffsetMS)
					ADD_WIDGET_INT_READ_ONLY("Time Until Speech (MS)", PedData.iTimeUntilHackerSpeechMS)
					ADD_WIDGET_INT_READ_ONLY("Buffer Time (MS)", PedData.iHackerBufferTimeMS)
				STOP_WIDGET_GROUP()
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Player Triggered Speech")
				START_WIDGET_GROUP("Driver")
					ADD_WIDGET_INT_READ_ONLY("Greeting Buffer Time (MS)", PedData.iDriverGreetingBufferTimeMS)
				STOP_WIDGET_GROUP()
				START_WIDGET_GROUP("Hacker")
					ADD_WIDGET_INT_READ_ONLY("Greeting Buffer Time (MS)", PedData.iHackerGreetingBufferTimeMS)
				STOP_WIDGET_GROUP()
				START_WIDGET_GROUP("Weapons Expert")
					ADD_WIDGET_INT_READ_ONLY("Greeting Buffer Time (MS)", PedData.iWeaponsExpertGreetingBufferTimeMS)
				STOP_WIDGET_GROUP()
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
ENDPROC

FUNC BOOL IS_DATA_EQUAL(PED_DATA& Data1, PED_DATA &Data2)
	IF (VDIST(Data1.vPosition,Data2.vPosition) < 0.01)
	AND (Data1.fHeading = Data2.fHeading)
	AND (Data1.iPackedDrawable = Data2.iPackedDrawable)
	AND (Data1.iPackedTexture = Data2.iPackedTexture)
	AND (Data1.bFemale = Data2.bFemale)
	AND (Data1.iActivity = Data2.iActivity)
	AND (Data1.iLevel = Data2.iLevel)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC OUTPUT_PED_DATA(PED_DATA &Data, INT iID, STRING sPath, STRING sFile)
	
	TEXT_LABEL_63 str = "CASE "
	str += iID
	
	IF (Data.bHasBeenEdited)
		str += " // edited"	
	ENDIF
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE(str, sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	Data.vPosition = ", sPath, sFile)
	SAVE_VECTOR_TO_NAMED_DEBUG_FILE(Data.vPosition, sPath, sFile) 
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	Data.fHeading = ", sPath, sFile)
	SAVE_FLOAT_TO_NAMED_DEBUG_FILE(Data.fHeading, sPath, sFile) 
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)

	SAVE_STRING_TO_NAMED_DEBUG_FILE("	Data.bFemale = ", sPath, sFile)
	SAVE_BOOL_TO_NAMED_DEBUG_FILE(Data.bFemale, sPath, sFile) 
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	Data.iPackedDrawable = ", sPath, sFile)
	SAVE_INT_TO_NAMED_DEBUG_FILE(Data.iPackedDrawable, sPath, sFile) 
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)	
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	Data.iPackedTexture = ", sPath, sFile)
	SAVE_INT_TO_NAMED_DEBUG_FILE(Data.iPackedTexture, sPath, sFile) 
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)	
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	Data.iActivity = ", sPath, sFile)
	SAVE_INT_TO_NAMED_DEBUG_FILE(Data.iActivity, sPath, sFile) 
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)	
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	Data.iLevel = ", sPath, sFile)
	SAVE_INT_TO_NAMED_DEBUG_FILE(Data.iLevel, sPath, sFile) 
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)	
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	Data.iPedModelVariation = ", sPath, sFile)
	SAVE_INT_TO_NAMED_DEBUG_FILE(Data.iPedModelVariation, sPath, sFile) 
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)

ENDPROC

PROC DEBUG_GET_PED_SPEECH_CIRCLE_RGB(INT &iR, INT &iG, INT &iB, PED_SPEECH_TYPE eSpeechType)
	SWITCH eSpeechType
		CASE PST_GREETING	
			iR = 0
			iG = 255
			iB = 0
		BREAK
		CASE PST_BYE	
			iR = 255
			iG = 0
			iB = 0
		BREAK
	ENDSWITCH
ENDPROC

PROC MAINTAIN_WIDGET()
	
	INT iPed
	PED_DATA data
	
	REPEAT MAX_NUM_ARCADE_PEDS iPed
		IF PedData.ArcadePed[iPed].bHasBeenEdited = FALSE
			GET_INITIAL_DATA_FOR_PED(ServerBD, data, iPed, PedData.bCabinetsSetupMissionComplete, PedData.bScopeOutSetupMissionComplete, PedData.eHeistDriver, PedData.eHeistHacker, PedData.eHeistWeaponExpert)
			IF NOT IS_DATA_EQUAL(data, PedData.ArcadePed[iPed])
				PedData.ArcadePed[iPed].bHasBeenEdited = TRUE
			ENDIF
		ENDIF
		
		IF PedData.bDrawSpeechAreas
			IF DOES_PED_PLAY_SPEECH(iPed)
			AND IS_ENTITY_ALIVE(PedData.ArcadePed[iPed].PedID)
				INT iSpeechType
				REPEAT PST_TOTAL iSpeechType
					PED_SPEECH_TYPE eSpeechType = INT_TO_ENUM(PED_SPEECH_TYPE, iSpeechType)
					IF IS_PED_SPEECH_TYPE_VALID(eSpeechType)
						INT iR = 0
						INT iG = 0
						INT iB = 0
						DEBUG_GET_PED_SPEECH_CIRCLE_RGB(iR, iG, iB, eSpeechType)
						DRAW_DEBUG_FLOAT_CIRCLE(GET_ENTITY_COORDS(PedData.ArcadePed[iPed].PedID), GET_PED_SPEECH_AREA(iPed, eSpeechType), iR, iG, iB, 255)
					ENDIF
				ENDREPEAT
				DRAW_DEBUG_FLOAT_CIRCLE(GET_ENTITY_COORDS(PedData.ArcadePed[iPed].PedID), GET_PED_LISTEN_DISTANCE(iPed), 255, 255, 255, 255)
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF (PedData.bOutputToFile)
		
		INT iYear, iMonth, iDay, iHour, iMins, iSec
		GET_LOCAL_TIME(iYear, iMonth, iDay, iHour, iMins, iSec)
		
		TEXT_LABEL_63 tlFile = "arcade_ped_data_"
		STRING sPath = "X:/gta5/titleupdate/dev_ng/"
		
		IF (iHour < 10)
			tlFile += 0
		ENDIF
		tlFile += iHour
		tlFile += "."
		IF (iMins < 10)
			tlFile += 0
		ENDIF
		tlFile += iMins
		tlFile += "."
		IF (iSec < 10)
			tlFile += 0
		ENDIF
		tlFile += iSec
		tlFile += ".txt"

		CLEAR_NAMED_DEBUG_FILE(sPath, tlFile)
		OPEN_NAMED_DEBUG_FILE(sPath, tlFile)
	
		SAVE_STRING_TO_NAMED_DEBUG_FILE("iCurrentLayout = ", sPath, tlFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE(PedData.iCurrentLayout, sPath, tlFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)	
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)	
	
		REPEAT MAX_NUM_ARCADE_PEDS iPed
			OUTPUT_PED_DATA(PedData.ArcadePed[iPed], iPed, sPath, tlFile)
		ENDREPEAT
	
		CLOSE_DEBUG_FILE()
		PedData.bOutputToFile = FALSE
	ENDIF
	
	IF PedData.bOutputClothingComps
		
		INT iYear, iMonth, iDay, iHour, iMins, iSec
		GET_LOCAL_TIME(iYear, iMonth, iDay, iHour, iMins, iSec)
		
		TEXT_LABEL_63 tlFile = "arcade_ped_clothing_comp_"
		STRING sPath = "X:/gta5/titleupdate/dev_ng/"
		
		IF (iHour < 10)
			tlFile += 0
		ENDIF
		tlFile += iHour
		tlFile += "."
		IF (iMins < 10)
			tlFile += 0
		ENDIF
		tlFile += iMins
		tlFile += "."
		IF (iSec < 10)
			tlFile += 0
		ENDIF
		tlFile += iSec
		tlFile += ".txt"

		CLEAR_NAMED_DEBUG_FILE(sPath, tlFile)
		OPEN_NAMED_DEBUG_FILE(sPath, tlFile)
		
		INT iComp
		INT iTexture
		INT iDrawable
		INT iPackedTexture
		INT iPackedDrawable
		PED_COMPONENT_CONFIG ComponentConfig
		
		REPEAT MAX_NUM_ARCADE_PEDS iPed
			IF IS_ENTITY_ALIVE(PedData.ArcadePed[iPed].PedID)
				
				REPEAT NUM_PED_COMPONENTS iComp
					iTexture = GET_PED_TEXTURE_VARIATION(PedData.ArcadePed[iPed].PedID, INT_TO_ENUM(PED_COMPONENT, iComp))
					iDrawable = GET_PED_DRAWABLE_VARIATION(PedData.ArcadePed[iPed].PedID, INT_TO_ENUM(PED_COMPONENT, iComp))
					IF (iComp < MAX_NUM_OF_SETTABLE_COMPONENTS)																				
						ComponentConfig.ComponentData[iComp].iDrawable = iDrawable
						ComponentConfig.ComponentData[iComp].iTexture = iTexture
					ENDIF									
				ENDREPEAT
				
				PACK_PED_COMPONENT_CONFIG(ComponentConfig, iPackedDrawable, iPackedTexture)
				
				TEXT_LABEL_63 str = "CASE "
				str += iPed
				
				SAVE_STRING_TO_NAMED_DEBUG_FILE(str, sPath, tlFile)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
				
				SAVE_STRING_TO_NAMED_DEBUG_FILE("	Data.iPackedDrawable = ",sPath, tlFile)
				SAVE_INT_TO_NAMED_DEBUG_FILE(iPackedDrawable,sPath, tlFile)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("	Data.iPackedTexture = ",sPath, tlFile)
				SAVE_INT_TO_NAMED_DEBUG_FILE(iPackedTexture,sPath, tlFile)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
				
				SAVE_STRING_TO_NAMED_DEBUG_FILE("BREAK", sPath, tlFile)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
				
			ENDIF
		ENDREPEAT
		
		CLOSE_DEBUG_FILE()
		PedData.bOutputClothingComps = FALSE
	ENDIF
	
	IF (PedData.bShowDebug)
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
		REPEAT MAX_NUM_ARCADE_PEDS iPed
			RENDER_PED_INFO(PedData.ArcadePed[iPed], iPed)
		ENDREPEAT
	ENDIF

	IF (PedData.bRecreatePeds)
		CLEANUP_ALL_ARCADE_PEDS()
		PedData.bRecreatePeds = FALSE
	ENDIF
	
	IF (PedData.iCurrentLayout != PedData.iLastLayout)
		INITIALISE_ARCADE_PEDS()
	ENDIF
	IF (PedData.iCurrentLevel != PedData.iLastLevel)
		INITIALISE_ARCADE_PEDS(FALSE)
	ENDIF
	
	IF DOES_TEXT_WIDGET_EXIST(PedData.twJimmySpawnLocation)
		IF NOT PedData.bCabinetsSetupMissionComplete
		OR NOT PedData.bScopeOutSetupMissionComplete
			SET_CONTENTS_OF_TEXT_WIDGET(PedData.twJimmySpawnLocation, "-")
		ELSE
			SET_CONTENTS_OF_TEXT_WIDGET(PedData.twJimmySpawnLocation, ARCADE_ACTIVITY_TO_STRING(INT_TO_ENUM(ARCADE_ACTIVITY, ServerBD.iJimmySpawnLocation+ENUM_TO_INT(AA_JIMMY_SOFA_WATCHING_TV))))
		ENDIF
	ENDIF
	
	IF DOES_TEXT_WIDGET_EXIST(PedData.twHeistDriverSpawnLocation)
		IF NOT PedData.bCabinetsSetupMissionComplete
		OR NOT PedData.bScopeOutSetupMissionComplete
			SET_CONTENTS_OF_TEXT_WIDGET(PedData.twHeistDriverSpawnLocation, "-")
		ELSE
			SET_CONTENTS_OF_TEXT_WIDGET(PedData.twHeistDriverSpawnLocation, DEBUG_GET_HEIST_DRIVER_COMBO_BOX_TEXT(ServerBD.iHeistDriverSpawnLocation+1))
		ENDIF
	ENDIF
	
	IF DOES_TEXT_WIDGET_EXIST(PedData.twHeistHackerSpawnLocation)
		IF NOT PedData.bCabinetsSetupMissionComplete
		OR NOT PedData.bScopeOutSetupMissionComplete
			SET_CONTENTS_OF_TEXT_WIDGET(PedData.twHeistHackerSpawnLocation, "-")
		ELSE
			SET_CONTENTS_OF_TEXT_WIDGET(PedData.twHeistHackerSpawnLocation, DEBUG_GET_HEIST_HACKER_COMBO_BOX_TEXT(ServerBD.iHeistHackerSpawnLocation+1))
		ENDIF
	ENDIF
	
	IF DOES_TEXT_WIDGET_EXIST(PedData.twHeistWeaponExpertSpawnLocation)
		IF NOT PedData.bCabinetsSetupMissionComplete
		OR NOT PedData.bScopeOutSetupMissionComplete
			SET_CONTENTS_OF_TEXT_WIDGET(PedData.twHeistWeaponExpertSpawnLocation, "-")
		ELSE
			SET_CONTENTS_OF_TEXT_WIDGET(PedData.twHeistWeaponExpertSpawnLocation, DEBUG_GET_HEIST_WEAPONS_EXPERT_COMBO_BOX_TEXT(ServerBD.iHeistWeaponExpertSpawnLocation+1))
		ENDIF
	ENDIF
	
	IF DOES_TEXT_WIDGET_EXIST(PedData.twJimmySpeechType)
		SET_CONTENTS_OF_TEXT_WIDGET(PedData.twJimmySpeechType, GET_ARCADE_PED_SPEECH_TYPE_STRING(serverBD.eJimmySpeechType))
	ENDIF
	
	IF DOES_TEXT_WIDGET_EXIST(PedData.twJimmyCabinetSpeechType)
		SET_CONTENTS_OF_TEXT_WIDGET(PedData.twJimmyCabinetSpeechType, GET_ARCADE_CABINET_NAME(serverBD.eJimmyCabinetSpeech))
	ENDIF
	
	PedData.iTimeUntilJimmySpeechMS = serverBD.iJimmySpeechTimeOffsetMS - (GET_GAME_TIMER() - PedData.iGameTimeJimmySpeechSet)
	IF PedData.iTimeUntilJimmySpeechMS < 0
		PedData.iTimeUntilJimmySpeechMS = 0
	ENDIF
	
	IF DOES_TEXT_WIDGET_EXIST(PedData.twDriverSpeechType)
		SET_CONTENTS_OF_TEXT_WIDGET(PedData.twDriverSpeechType, GET_ARCADE_PED_SPEECH_TYPE_STRING(serverBD.eDriverSpeechType))
	ENDIF
	
	PedData.iTimeUntilDriverSpeechMS = serverBD.iDriverSpeechTimeOffsetMS - (GET_GAME_TIMER() - PedData.iGameTimeDriverSpeechSet)
	IF PedData.iTimeUntilDriverSpeechMS < 0
		PedData.iTimeUntilDriverSpeechMS = 0
	ENDIF
	
	IF DOES_TEXT_WIDGET_EXIST(PedData.twHackerSpeechType)
		SET_CONTENTS_OF_TEXT_WIDGET(PedData.twHackerSpeechType, GET_ARCADE_PED_SPEECH_TYPE_STRING(serverBD.eHackerSpeechType))
	ENDIF
	
	PedData.iTimeUntilHackerSpeechMS = serverBD.iHackerSpeechTimeOffsetMS - (GET_GAME_TIMER() - PedData.iGameTimeHackerSpeechSet)
	IF PedData.iTimeUntilHackerSpeechMS < 0
		PedData.iTimeUntilHackerSpeechMS = 0
	ENDIF
	
	
	PedData.iJimmyBufferTimeMS = PED_SPEECH_BUFFER_TIME_MS - (GET_GAME_TIMER() - PedData.iJimmyBufferTimerSet)
	IF PedData.iJimmyBufferTimeMS < 0
		PedData.iJimmyBufferTimeMS = 0
	ENDIF
	
	PedData.iDriverBufferTimeMS = PED_SPEECH_BUFFER_TIME_MS - (GET_GAME_TIMER() - PedData.iDriverBufferTimerSet)
	IF PedData.iDriverBufferTimeMS < 0
		PedData.iDriverBufferTimeMS = 0
	ENDIF
	
	PedData.iHackerBufferTimeMS = PED_SPEECH_BUFFER_TIME_MS - (GET_GAME_TIMER() - PedData.iHackerBufferTimerSet)
	IF PedData.iHackerBufferTimeMS < 0
		PedData.iHackerBufferTimeMS = 0
	ENDIF
	
	PedData.iLoiterBufferTimeMS = PED_SPEECH_BUFFER_TIME_MS - (GET_GAME_TIMER() - PedData.iLoiterBufferTimerSet)
	IF PedData.iLoiterBufferTimeMS < 0
		PedData.iLoiterBufferTimeMS = 0
	ENDIF
	
	PedData.iDriverGreetingBufferTimeMS = CREW_GREETING_BUFFER_TIME_MS - (GET_GAME_TIMER() - PedData.iDriverGreetingBufferTimerSet)
	IF PedData.iDriverGreetingBufferTimeMS < 0
		PedData.iDriverGreetingBufferTimeMS = 0
	ENDIF
	
	PedData.iHackerGreetingBufferTimeMS = CREW_GREETING_BUFFER_TIME_MS - (GET_GAME_TIMER() - PedData.iHackerGreetingBufferTimerSet)
	IF PedData.iHackerGreetingBufferTimeMS < 0
		PedData.iHackerGreetingBufferTimeMS = 0
	ENDIF
	
	PedData.iWeaponsExpertGreetingBufferTimeMS = CREW_GREETING_BUFFER_TIME_MS - (GET_GAME_TIMER() - PedData.iWeaponsExpertGreetingBufferTimerSet)
	IF PedData.iWeaponsExpertGreetingBufferTimeMS < 0
		PedData.iWeaponsExpertGreetingBufferTimeMS = 0
	ENDIF
	
	IF IS_BIT_SET(PedData.ArcadePed[0].iBS, BS_ARCADE_PEDS_LOITER_TIMER_SET)
	OR IS_BIT_SET(PedData.ArcadePed[1].iBS, BS_ARCADE_PEDS_LOITER_TIMER_SET)
	OR IS_BIT_SET(PedData.ArcadePed[2].iBS, BS_ARCADE_PEDS_LOITER_TIMER_SET)
	OR IS_BIT_SET(PedData.ArcadePed[3].iBS, BS_ARCADE_PEDS_LOITER_TIMER_SET)
	OR IS_BIT_SET(PedData.ArcadePed[4].iBS, BS_ARCADE_PEDS_LOITER_TIMER_SET)
		PedData.iLoiterTimerMS = PED_SPEECH_LOITER_TIME_MS - (GET_GAME_TIMER() - PedData.iLoiterTimeSet)
		IF PedData.iLoiterTimerMS < 0
			PedData.iLoiterTimerMS = 0
		ENDIF
	ELSE
		PedData.iLoiterTimerMS = 0
	ENDIF
	
	IF g_bDebugResetArcadePedScript
		// Kills the script and relaunchs in AM_MP_ARCADE.sc
		g_bTurnOnArcadePeds = FALSE
	ENDIF
	
ENDPROC
#ENDIF //IS_DEBUG_BUILD

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════╡ SCRIPT MAINTAIN ╞════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

FUNC BOOL SHOULD_ARCADE_PEDS_SCRIPT_TERMINATE()
	BOOL bTerminate = FALSE
	
	IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		PRINTLN("[AM_MP_ARCADE_PEDS] SHOULD_ARCADE_PEDS_SCRIPT_TERMINATE - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE is returning TRUE")
		bTerminate = TRUE
	ENDIF
	
	IF NOT IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
	AND NOT IS_PLAYER_WALKING_INTO_SIMPLE_INTERIOR(PLAYER_ID())
	AND NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBS, BS_SIMPLE_INTERIOR_GLOBAL_PLAYER_BD_SPAWNING_INTO_SIMPLE_INTERIOR)
		PRINTLN("[AM_MP_ARCADE_PEDS] SHOULD_ARCADE_PEDS_SCRIPT_TERMINATE - Player is no longer inside the Arcade")
		bTerminate = TRUE
	ENDIF
	
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY("AM_MP_ARCADE")) < 1
		PRINTLN("[AM_MP_ARCADE_PEDS] SHOULD_ARCADE_PEDS_SCRIPT_TERMINATE - Parent script (AM_MP_ARCADE) has died")
		bTerminate = TRUE
	ENDIF
	
	IF NETWORK_IS_ACTIVITY_SESSION()
		PRINTLN("[AM_MP_ARCADE_PEDS] SHOULD_ARCADE_PEDS_SCRIPT_TERMINATE - NETWORK_IS_ACTIVITY_SESSION is returning TRUE")
		bTerminate = TRUE
	ENDIF
	
	IF g_bTurnOnArcadePeds = FALSE
		PRINTLN("[AM_MP_ARCADE_PEDS] SHOULD_ARCADE_PEDS_SCRIPT_TERMINATE - g_bTurnOnArcadePeds is set to FALSE")
		bTerminate = TRUE
	ENDIF
	
	RETURN bTerminate
ENDFUNC
#ENDIF //FEATURE_CASINO_HEIST

SCRIPT(ARCADE_PED_OWNER_DATA OwnerData)
	
	#IF FEATURE_CASINO_HEIST
	INITIALISE_ARCADE_PED_GLOBALS()
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		SCRIPT_INITIALISE()
	ELSE
		SCRIPT_CLEANUP()
	ENDIF
	
	SET_ARCADE_OWNER_DATA(OwnerData)
	SET_SERVER_BD_DATA()
	INITIALISE_ARCADE_PEDS()
	CREATE_ARCADE_STANDALONE_PROPS()
	
	#IF IS_DEBUG_BUILD
	CREATE_ARCADE_WIDGETS()
	#ENDIF
	
	WHILE TRUE
		MP_LOOP_WAIT_ZERO()
		
		IF SHOULD_ARCADE_PEDS_SCRIPT_TERMINATE()
			SCRIPT_CLEANUP()
		ENDIF
		
		#IF IS_DEBUG_BUILD
		MAINTAIN_WIDGET()
		#ENDIF
		
		UPDATE_ARCADE_PEDS()
		UPDATE_AMBIENT_SOUND()
	ENDWHILE
	
	#ENDIF //FEATURE_CASINO_HEIST
	
ENDSCRIPT
