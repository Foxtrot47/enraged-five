//////////////////////////////////////////////////////////////////////////////////////////
// Name:        AM_PLANE_TAKEDOWN.sc													//
// Description: Controls a Plane flying around that players have to shoot down.			//
// Written by:  Ryan Baker																//
// Date: 31/07/2014																		//
//////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"

// Game Headers
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_path.sch"
USING "commands_event.sch"

// Network Headers
USING "net_include.sch"
USING "net_events.sch"
USING "net_spawn.sch"

USING "net_mission.sch"
//
USING "net_scoring_common.sch"
USING "net_ambience.sch"

//USING "help_at_location.sch"
USING "commands_object.sch"
USING "net_xp_animation.sch"

USING "net_wait_zero.sch"

USING "net_cash_transactions.sch"

#IF IS_DEBUG_BUILD
USING "net_debug_log.sch"
#ENDIF

USING "net_gang_boss.sch"

// Game States. 
CONST_INT GAME_STATE_INI 				0
CONST_INT GAME_STATE_RUNNING			1
CONST_INT GAME_STATE_LEAVE				2
CONST_INT GAME_STATE_TERMINATE_DELAY	3
CONST_INT GAME_STATE_END				4

CONST_INT PLANE_SPEED					30
CONST_INT MAX_NUM_CHECKPOINTS			4

ENUM TAKEDOWN_STAGE_ENUM
	eAD_TAKEDOWN,
	eAD_CLEANUP
ENDENUM

//Server Bitset Data
CONST_INT biS_UsingHelicopter		0
CONST_INT biS_SendFailedText		1
CONST_INT biS_DoFlyaway				2

// The server broadcast data.
// Everyone can read this data, only the server can update it
STRUCT ServerBroadcastData
	INT iServerGameState
		
	INT iServerBitSet
	
	NETWORK_INDEX niPlane
	NETWORK_INDEX niPilot
	
	MODEL_NAMES PlaneModel = CUBAN800
	MODEL_NAMES PilotModel = A_M_M_SkidRow_01
	
	VECTOR vStartLocation
	VECTOR vCheckpoint[MAX_NUM_CHECKPOINTS]
	
	INT iCurrentCheckpoint
	
	TAKEDOWN_STAGE_ENUM eTakedownStage = eAD_TAKEDOWN
	
	SCRIPT_TIMER MissionTerminateDelayTimer
	SCRIPT_TIMER CrashCheckDelayTimer
ENDSTRUCT
ServerBroadcastData serverBD


CONST_INT biP_DestroyedPlane		0
CONST_INT biP_SendPlaneFlyaway		1

// The client broadcast data.
// Everyone can read this data, only the local player only the server can update it
STRUCT PlayerBroadcastData
	INT iGameState
	
	INT iPlayerBitSet
	
	TAKEDOWN_STAGE_ENUM eTakedownStage = eAD_TAKEDOWN
ENDSTRUCT
PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]

///// MISSION VARIABLES /////
//INT iBoolsBitSet
//CONST_INT biSequencesCreated			0
//CONST_INT biDestinationNodeFound		1

//BLIP_INDEX PlaneBlip

SEQUENCE_INDEX seqRoute

//INT iDestinationNodeCount
//
//CONST_INT START_NODE		20
//INT iSpawnNodeCount = START_NODE
//INT iCheckCount

INT iStaggeredParticipant
//BOOL bDoEndStaggeredLoopChecks

BLIP_INDEX PlaneBlip

CONST_FLOAT PLANE_TARGET_RADIUS		50.0
CONST_FLOAT PLANE_NORMAL_SPEED		20.0		//25.0
CONST_INT PLANE_HEIGHT_ABOVE_DROP	120
CONST_INT PLANE_LOW_HEIGHT			60
CONST_INT PLANE_NORMAL_HEIGHT		90

// Widgets & Debug.
#IF IS_DEBUG_BUILD
	BOOL bHostEndMissionNow
	BOOL bWarpToEnd
	BOOL bDebugBlip// = TRUE
#ENDIF

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  MAIN PROCS                            ////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Helper function to get the servers game/mission state
FUNC INT GET_SERVER_MISSION_STATE()
	RETURN serverBD.iServerGameState
ENDFUNC

FUNC BOOL SHOULD_HIDE_LOCAL_UI()	
	// Deprecated piece of content
	PRINTLN("     ---------->     PLANE TAKEDOWN -  Deprecated Content, hiding")
	RETURN TRUE
ENDFUNC

//PURPOSE: Clears all the sequences needed for this script
PROC CLEAR_SEQUENCES()
	CLEAR_SEQUENCE_TASK(seqRoute)
	NET_PRINT_TIME() NET_PRINT("     ---------->     PLANE TAKEDOWN -  CLEAR_SEQUENCES   <----------     ") NET_NL()
ENDPROC

//Cleanup Mission Data
PROC SCRIPT_CLEANUP()
	
	IF DOES_BLIP_EXIST(PlaneBlip)
		REMOVE_BLIP(PlaneBlip)
	ENDIF
	
	//Send Fail Text
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_SendFailedText)
		Send_MP_Txtmsg_From_CharSheetID(CHAR_TREVOR, "PTD_FAIL")
		NET_PRINT_TIME() NET_PRINT("     ---------->     PLANE TAKEDOWN -  SEND FAIL TEXT MESSAGE      <----------     ") NET_NL()
	ENDIF
	
	CLEAR_SEQUENCES()
	
	IF GET_SERVER_MISSION_STATE() = GAME_STATE_END
	AND serverBD.PlaneModel != DUMMY_MODEL_FOR_SCRIPT
		SET_VEHICLE_MODEL_IS_SUPPRESSED(serverBD.PlaneModel, FALSE)
		NET_PRINT_TIME() NET_PRINT("     ---------->     PLANE TAKEDOWN -  SET_VEHICLE_MODEL_IS_SUPPRESSED - FALSE      <----------     ") NET_NL()
	ENDIF
	
	SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_PLANE_TAKEDOWN, FALSE)
	
	NET_PRINT_TIME() NET_PRINT("     ---------->     PLANE TAKEDOWN -  CLEANUP MISSION      <----------     ") NET_NL()
	#IF IS_DEBUG_BUILD NET_LOG("SCRIPT_CLEANUP")	#ENDIF
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC


//PURPOSE: Locations Above 3500
FUNC VECTOR GET_RANDOM_TOP_COORD()
	SWITCH GET_RANDOM_INT_IN_RANGE(0, 13)
		CASE 0		RETURN <<1015.5989, 4350.6685, 41.4840>>	//NORTH OF BIG LAKE
		CASE 1		RETURN <<-1731.0601, 4959.4839, 3.8134>>	//SECLUDED BEACH - NORTH WEST
		CASE 2		RETURN <<-2436.2498, 4181.9131, 7.7719>>	//BEACH - NEAR MILITARY BASE
		CASE 3		RETURN <<2463.1421, 3769.6511, 40.3280>>	//ALIEN HIPPY
		CASE 4		RETURN <<1888.6101, 4626.8149, 37.4665>>	//FARM - FAR NORTH
		CASE 5		RETURN <<-388.8584, 4353.4780, 54.3806>>	//RIVER ENTRANCE - COUNTRY
		CASE 6		RETURN <<3800, 4462, 5>>	//BOATS ON BEACH
		CASE 7		RETURN <<3350, 5152, 20>>	//LIGHTHOUSE
		CASE 8		RETURN <<2200, 5600, 54>>	//FARM
		CASE 9		RETURN <<1410, 6560, 20>>	//NEAR HOBOS
		CASE 10		RETURN <<-330, 6100, 32>>	//PALETO AMMUNATION
		CASE 11		RETURN <<-1365, 4380, 42>>	//RIVER EXIT
		CASE 12		RETURN <<700, 3900, 30>>	//MIDDLE OF LAKE
	ENDSWITCH
	
	RETURN <<700, 3900, 30>>
ENDFUNC

//PURPOSE: Locations Between 700 and 3500
FUNC VECTOR GET_RANDOM_MIDDLE_COORD()
	SWITCH GET_RANDOM_INT_IN_RANGE(0, 13)
		CASE 0		RETURN <<2312.7073, 1531.2584, 59.8792>>	//WINDFARM
		CASE 1		RETURN <<1069.5798, 2366.3628, 43.0396>>	//BUILDING SITE (COUNTRY)
		CASE 2		RETURN <<-1979.7805, 2584.1609, 2.2587>>	//RIVER (NEAR MILITARY AIRPORT)
		CASE 3		RETURN <<1458.3934, 1113.0664, 113.3340>>	//STABLES
		CASE 4		RETURN <<817.5361, 1316.5696, 362.0491>>	//VINEWOOD SIGN
		CASE 5		RETURN <<1671.2318, 3041.1763, 53.0351>>	//SMALL DESERT HOUSE
		CASE 6		RETURN <<-3163.3643, 756.8276, 2.1183>>		//BEACH - NEAR HOUSES - WEST
		CASE 7		RETURN <<-799.6782, 885.0139, 202.1319>>	//<<-818.7764, 865.3253, 201.9869>>	//SMALL PARK - NORTH OF CITY
		CASE 8		RETURN <<-401.2717, 1211.6768, 324.9297>>	//OBSERVATORY CAR PARK
		CASE 9		RETURN <<2909.1543, 770.3181, 21.1684>>		//GREEN - OCEAN ARCH
		CASE 10		RETURN <<2778.9092, 2853.2659, 34.7828>>	//QUARRY
		CASE 11		RETURN <<585.3987, 2893.7783, 38.5297>>		//OIL PUMPS - COUNTRY
		CASE 12		RETURN <<-498.3305, 3006.9063, 27.4341>>	//RIVER - COVERED BRIDGE
	ENDSWITCH
	
	RETURN <<-498.3305, 3006.9063, 27.4341>>
ENDFUNC

//PURPOSE: Locations Below 700
FUNC VECTOR GET_RANDOM_BOTTOM_COORD()
	SWITCH GET_RANDOM_INT_IN_RANGE(0, 13)
		CASE 0		RETURN <<1915.9016, 563.9669, 174.4925>>	//DAM
		CASE 1		RETURN <<1402.8578, -2148.4177, 57.9675>>	//OIL FIELD
		CASE 2		RETURN <<1104.8152, -3142.3347, 4.9010>>	//DOCKS CONTAINERS
		CASE 3		RETURN <<545.7347, -3025.3271, 5.0591>>		//DOCKS SMALL
		CASE 4		RETURN <<1376.0369, -740.2166, 66.2331>>	//NEW ESTATE DEADEND
		CASE 5		RETURN <<1147.1620, 126.3452, 80.8693>>		//HORSE RACING COURSE
		CASE 6		RETURN <<-1731.0846, -989.8386, 4.4152>>	//BEACH - NEAR FUNFAIR PIER
		CASE 7		RETURN <<728.2132, -1532.4799, 18.7348>>	//STORM DRAIN
		CASE 8		RETURN <<1140.9230, -1285.6388, 33.6091>>	//WAREHOUSE
		CASE 9		RETURN <<2811.4529, -668.6710, 1.5810>>		//BEACH RUINS
		CASE 10		RETURN <<-1706.0131, -183.4387, 56.3712>>	//-1729.3708, -193.2892, 57.4816>>	//GRAVEYARD
		CASE 11		RETURN <<-134.0402, -869.4509, 43.2175>>	//MAZE BANK - CITY					//BUSINESS PACK
		CASE 12		RETURN <<1098.2463, -544.2941, 56.4061>>	//MIRROR PARK - CITY				//HIPSTER PACK
	ENDSWITCH
	
	RETURN <<1098.2463, -544.2941, 56.4061>>
ENDFUNC


FUNC VECTOR GET_RANDOM_COORD(INT iLocation)
	//Cap iRand
	IF iLocation < 0
		iLocation = 2
	ELIF iLocation > 2
		iLocation = 0
	ENDIF
	
	//Get Coords based on Location
	SWITCH iLocation
		CASE 0  RETURN GET_RANDOM_TOP_COORD()
		CASE 1  RETURN GET_RANDOM_MIDDLE_COORD()
		CASE 2 	RETURN GET_RANDOM_BOTTOM_COORD()
	ENDSWITCH
	
	RETURN <<3250, 1500, 0>>
ENDFUNC

//PURPOSE: Grabs Plane Start Location
PROC GET_START_LOCATION()
	serverBD.vStartLocation = GET_RANDOM_COORD(GET_RANDOM_INT_IN_RANGE(0, 3))
	serverBD.vStartLocation += <<0, 0, 120>>
	PRINTLN("     ---------->     PLANE TAKEDOWN -  GET_START_LOCATION - vStartLocation = ", serverBD.vStartLocation)
ENDPROC

//PURPOSE: Grabs Checkpoint Locations
PROC GET_CHECKPOINT_LOCATIONS()
	INT i, iRand
	REPEAT MAX_NUM_CHECKPOINTS i
		iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
		serverBD.vCheckpoint[i] = GET_RANDOM_COORD(iRand)
		
		//Vector is close to last Vector try to grab a new one
		IF i > 0
			IF ARE_VECTORS_ALMOST_EQUAL(serverBD.vCheckpoint[i], serverBD.vCheckpoint[i-1], 250)
				serverBD.vCheckpoint[i] = GET_RANDOM_COORD(iRand+1)
			ENDIF
		ENDIF
		
		PRINTLN("     ---------->     PLANE TAKEDOWN -  GET_START_LOCATION - vCheckpoint[", i, "] = ", serverBD.vCheckpoint[i])
	ENDREPEAT
ENDPROC






//Do necessary pre game start ini.
PROC PROCESS_PRE_GAME( MP_MISSION_DATA missionScriptArgs )
	
	// This marks the script as a net script, and handles any instancing setup. 
	SETUP_MP_MISSION_FOR_NETWORK(GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(missionScriptArgs.mdID.idMission) ,  missionScriptArgs)	//NUM_NETWORK_PLAYERS, missionScriptArgs)	//
	//NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE, PARTICIPANT_ID_TO_INT())  
	
	//Reserve Entities
	RESERVE_NETWORK_MISSION_PEDS(1)
	RESERVE_NETWORK_MISSION_VEHICLES(1)

	// This makes sure the net script is active, waits untill it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
	
	// KGM: Wait for the first network broadcast before moving on
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		#IF IS_DEBUG_BUILD NET_LOG("PLANE TAKEDOWN -  FAILED TO RECEIVE INITIAL NETWORK BROADCAST")	#ENDIF
		NET_PRINT_TIME() NET_PRINT("     ---------->     PLANE TAKEDOWN -  MISSION END - FAILED TO RECEIVE INITIAL NETWORK BROADCAST - SCRIPT CLEANUP D     <----------     ") NET_NL()
		SCRIPT_CLEANUP()
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		// This script will not be paused if another script calls PAUSE_GAME
		SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			INT iRand = GET_RANDOM_INT_IN_RANGE(0, 6)
			SWITCH iRand
				CASE 0 serverBD.PlaneModel = CUBAN800		serverBD.PilotModel = A_M_M_SkidRow_01		BREAK
				CASE 1 serverBD.PlaneModel = STUNT			serverBD.PilotModel = A_M_Y_EastSA_02		BREAK
				CASE 2 serverBD.PlaneModel = DUSTER			serverBD.PilotModel = A_M_M_ProlHost_01		BREAK
				CASE 3 serverBD.PlaneModel = MAMMATUS		serverBD.PilotModel = A_M_Y_Vinewood_02		BREAK
				CASE 4 serverBD.PlaneModel = MAVERICK		serverBD.PilotModel = A_M_M_EastSA_02		SET_BIT(serverBD.iServerBitSet, biS_UsingHelicopter)	BREAK
				CASE 5 serverBD.PlaneModel = SWIFT			serverBD.PilotModel = A_M_Y_BusiCas_01		SET_BIT(serverBD.iServerBitSet, biS_UsingHelicopter)	BREAK
			ENDSWITCH
			PRINTLN("     ---------->     PLANE TAKEDOWN -  SET MODELS TO COMBO ", iRand)
			
			IF serverBD.PlaneModel != DUMMY_MODEL_FOR_SCRIPT
				SET_VEHICLE_MODEL_IS_SUPPRESSED(serverBD.PlaneModel, TRUE)
			ENDIF
			
			GET_START_LOCATION()
			GET_CHECKPOINT_LOCATIONS()
		ENDIF
		
		SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_PLANE_TAKEDOWN, TRUE)
		
		playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_INI
		NET_PRINT_TIME() NET_PRINT("     ---------->     PLANE TAKEDOWN -  PRE_GAME DONE     <----------     ") NET_NL()
	ELSE
		#IF IS_DEBUG_BUILD NET_LOG("PLANE TAKEDOWN -  NETWORK_IS_GAME_IN_PROGRESS = FALSE")	#ENDIF
		NET_PRINT_TIME() NET_PRINT("     ---------->     PLANE TAKEDOWN -  MISSION END - NETWORK_IS_GAME_IN_PROGRESS = FALSE - SCRIPT CLEANUP Q     <----------     ") NET_NL()
		SCRIPT_CLEANUP()
	ENDIF
ENDPROC

//Player leaves area
FUNC BOOL MISSION_END_CHECK()
	//Plane Out of Bounds Check
	IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niPlane)
		IF serverBD.iCurrentCheckpoint > 0
			IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niPlane)
				IF NOT IS_ENTITY_IN_ANGLED_AREA(NET_TO_VEH(serverBD.niPlane), <<400, -3850, -50>>, <<400, 7950, 1000>>, 4050)
					
					//Send Ticker
					SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerEventData
					IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_UsingHelicopter)
						TickerEventData.TickerEvent = TICKER_EVENT_PLANE_TAKEDOWN_ESCAPE
					ELSE
						TickerEventData.TickerEvent = TICKER_EVENT_PLANE_TAKEDOWN_ESCAPE_HELI
					ENDIF
					
					BROADCAST_TICKER_EVENT(TickerEventData, ALL_PLAYERS_ON_SCRIPT())
					
					SET_BIT(serverBD.iServerBitSet, biS_SendFailedText)
					
					#IF IS_DEBUG_BUILD
						VECTOR vLoc = GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niPlane))
						PRINTLN("     ---------->     PLANE TAKEDOWN -  MISSION END - PLANE HAS LEFT LOS SANTOS - LOCATION = ", vLoc)
					#ENDIF
					
					RETURN TRUE		
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//Plane Crashed
	IF serverBD.eTakedownStage = eAD_TAKEDOWN
		IF serverBD.iServerGameState != GAME_STATE_END
			//IF NOT IS_NET_VEHICLE_DRIVEABLE(serverBD.niPlane)
			IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niPlane)
				IF IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niPlane))
					IF HAS_NET_TIMER_EXPIRED(serverBD.CrashCheckDelayTimer, 3000)	//6000
						//Send Ticker
						SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerEventData
						IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_UsingHelicopter)
							TickerEventData.TickerEvent = TICKER_EVENT_PLANE_TAKEDOWN_CRASHED
						ELSE
							TickerEventData.TickerEvent = TICKER_EVENT_PLANE_TAKEDOWN_CRASHED_HELI
						ENDIF
						BROADCAST_TICKER_EVENT(TickerEventData, ALL_PLAYERS_ON_SCRIPT())
						
						NET_PRINT_TIME() NET_PRINT("     ---------->     PLANE TAKEDOWN -  MISSION END - PLANE HAS CRASHED     <----------     ") NET_NL()
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//Server only function that checks to see if the game mode should now end. For this test script, it if time has passed.
FUNC BOOL HAVE_MISSION_END_CONDITIONS_BEEN_MET()
	
	#IF IS_DEBUG_BUILD
		IF bHostEndMissionNow
			RETURN TRUE
		ENDIF
	#ENDIF	
	
	IF MISSION_END_CHECK()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


//Helper function to get a clients game/mission state
FUNC INT GET_CLIENT_MISSION_STATE(INT iPlayer)
	RETURN playerBD[iPlayer].iGameState
ENDFUNC

#IF IS_DEBUG_BUILD
PROC CREATE_WIDGETS()
	INT iPlayer
	TEXT_LABEL_15 tl63

	START_WIDGET_GROUP("PLANE TAKEDOWN")  
		ADD_WIDGET_BOOL("bDebugBlip", bDebugBlip)
		ADD_WIDGET_BOOL("Warp to Plane", bWarpToEnd)
				
		// Gameplay debug, sever only.
		START_WIDGET_GROUP("Server Only Gameplay") 
			ADD_WIDGET_BOOL("End mission", bHostEndMissionNow)			
		STOP_WIDGET_GROUP()		
		
		// Data about the server
		START_WIDGET_GROUP("Server BD") 
			ADD_WIDGET_INT_SLIDER("S. Game state", serverBD.iServerGameState,-1, HIGHEST_INT,1)
		STOP_WIDGET_GROUP()	

		// Data about the clients. * = You.
		START_WIDGET_GROUP("Client BD")  				
			REPEAT NUM_NETWORK_PLAYERS iPlayer
				tl63 = "Player "
				tl63 += iPlayer
				IF iPlayer = PARTICIPANT_ID_TO_INT()
					tl63 += "*"
				ENDIF
				START_WIDGET_GROUP(tl63)
					ADD_WIDGET_INT_SLIDER("Game state", playerBD[iPlayer].iGameState,-1, HIGHEST_INT,1)
				STOP_WIDGET_GROUP()
			ENDREPEAT				
		STOP_WIDGET_GROUP()	
		
	STOP_WIDGET_GROUP()
ENDPROC		

PROC UPDATE_WIDGETS()
	//Warp player to Joyrider
	IF bWarpToEnd = TRUE
		IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niPlane)
			//serverBD.iCurrentCheckpoint = 1
			//SET_ENTITY_COORDS(NET_TO_VEH(serverBD.niPlane), <<0, 0, -100>>)
			SET_ENTITY_COORDS(PLAYER_PED_ID(), GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niPlane))+<<0, -10, 0>>)
			NET_PRINT_TIME() NET_PRINT("     ---------->     PLANE TAKEDOWN -  UPDATE_WIDGETS - bWarpToEnd DONE    <----------     ") NET_NL()
			bWarpToEnd = FALSE
			/*IF NET_WARP_TO_COORD(GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niPlane))+<<0, -10, 0>>, 0, TRUE, FALSE)
				NET_PRINT_TIME() NET_PRINT("     ---------->     PLANE TAKEDOWN -  UPDATE_WIDGETS - bWarpToEnd DONE    <----------     ") NET_NL()
				bWarpToEnd = FALSE
			ELSE
				NET_PRINT_TIME() NET_PRINT("     ---------->     PLANE TAKEDOWN -  UPDATE_WIDGETS - bWarpToEnd IN PROGRESS    <----------     ") NET_NL()
			ENDIF*/
		ENDIF
	ENDIF
ENDPROC

#ENDIF

//PURPOSE: Adds a blip to the Plane and maintains its roatation
PROC MAINTAIN_PLANE_BLIP()
	//IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niPlane)
	IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niPlane)
	AND NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niPlane))
		IF NOT DOES_BLIP_EXIST(PlaneBlip)
			PlaneBlip = ADD_BLIP_FOR_ENTITY(NET_TO_VEH(serverBD.niPlane))
			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_UsingHelicopter)
				SET_BLIP_SPRITE(PlaneBlip, RADAR_TRACE_PLANE_DROP)
				SET_BLIP_NAME_FROM_TEXT_FILE(PlaneBlip, "PTD_BLIPN")
			ELSE
				SET_BLIP_SPRITE(PlaneBlip, RADAR_TRACE_HELICOPTER)
				SET_BLIP_NAME_FROM_TEXT_FILE(PlaneBlip, "PTD_BLIPH")	//Smuggler Helicopter
			ENDIF
			SET_BLIP_COLOUR(PlaneBlip, BLIP_COLOUR_RED)
			//SHOW_HEIGHT_ON_BLIP(PlaneBlip, TRUE)
			SET_BLIP_FLASH_TIMER(PlaneBlip, 7000)
			SET_BLIP_PRIORITY(PlaneBlip, BLIPPRIORITY_MED_HIGH)
			IF SHOULD_HIDE_LOCAL_UI()
				SET_BLIP_DISPLAY(PlaneBlip, DISPLAY_NOTHING)
			ENDIF
			NET_PRINT_TIME() NET_PRINT("     ---------->     PLANE TAKEDOWN -  PLANE BLIP ADDED ") NET_NL()
		ELSE
			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_UsingHelicopter)
				SET_BLIP_ROTATION(PlaneBlip, ROUND(GET_ENTITY_HEADING(NET_TO_VEH(serverBD.niPlane))))
			ENDIF
			IF SHOULD_HIDE_LOCAL_UI()
			OR ARE_TEMP_HIDDEN_BLIPS_ACTIVE()
				SET_BLIP_DISPLAY(PlaneBlip, DISPLAY_NOTHING)
			ELSE
				SET_BLIP_DISPLAY(PlaneBlip, DISPLAY_BOTH)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Gives the pilot the flight task
PROC CONTROL_PILOT_FLIGHT_TASK()
	IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niPlane)
	AND NOT IS_NET_PED_INJURED(serverBD.niPilot)
		IF IS_PED_IN_VEHICLE(NET_TO_PED(serverBD.niPilot), NET_TO_VEH(serverBD.niPlane))
			//IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niPlane)
			//OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.niPlane) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
				IF serverBD.iCurrentCheckpoint < MAX_NUM_CHECKPOINTS
//					IF GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.niPilot), SCRIPT_TASK_VEHICLE_MISSION) != PERFORMING_TASK
//					AND GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.niPilot), SCRIPT_TASK_VEHICLE_MISSION) != WAITING_TO_START_TASK
						IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_UsingHelicopter)
							TASK_PLANE_MISSION(NET_TO_PED(serverBD.niPilot), NET_TO_VEH(serverBD.niPlane), NULL, NULL, serverBD.vCheckpoint[serverBD.iCurrentCheckpoint]+<<0, 0, PLANE_HEIGHT_ABOVE_DROP>>, MISSION_GOTO, PLANE_NORMAL_SPEED, PLANE_TARGET_RADIUS, -1, PLANE_NORMAL_HEIGHT, PLANE_LOW_HEIGHT)
						ELSE
							TASK_HELI_MISSION(NET_TO_PED(serverBD.niPilot), NET_TO_VEH(serverBD.niPlane), NULL, NULL, serverBD.vCheckpoint[serverBD.iCurrentCheckpoint]+<<0, 0, PLANE_HEIGHT_ABOVE_DROP>>, MISSION_GOTO, PLANE_NORMAL_SPEED, PLANE_TARGET_RADIUS, -1, PLANE_NORMAL_HEIGHT, PLANE_LOW_HEIGHT)
						ENDIF
						PRINTLN("     ---------->     PLANE TAKEDOWN -  CONTROL_PILOT_FLIGHT_TASK - FLY TO CHECKPOINT ",serverBD.iCurrentCheckpoint, " AT ", (serverBD.vCheckpoint[serverBD.iCurrentCheckpoint]+<<0, 0, PLANE_HEIGHT_ABOVE_DROP>>))
//					ENDIF
				ELSE
//					IF GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.niPilot), SCRIPT_TASK_VEHICLE_MISSION) != PERFORMING_TASK
//					AND GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.niPilot), SCRIPT_TASK_VEHICLE_MISSION) != WAITING_TO_START_TASK						
					
					IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_DoFlyaway)
					AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_SendPlaneFlyaway)
						VECTOR vDest = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(NET_TO_VEH(serverBD.niPlane), <<0, 12000, 0>>)
						
						IF vDest.x < -3700
							vDest.x = -3700
						ELIF vDest.x > 4500
							vDest.x = 4500	
						ENDIF
						IF vDest.y < -3900
							vDest.y = -3900
						ELIF vDest.y > 8000
							vDest.y = 8000	
						ENDIF
						vDest.z = 100
						
						IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_UsingHelicopter)
							TASK_PLANE_MISSION(NET_TO_PED(serverBD.niPilot), NET_TO_VEH(serverBD.niPlane), NULL, NULL, vDest, MISSION_GOTO, PLANE_NORMAL_SPEED, -1, -1, 100, 100)
						ELSE
							TASK_HELI_MISSION(NET_TO_PED(serverBD.niPilot), NET_TO_VEH(serverBD.niPlane), NULL, NULL, vDest, MISSION_GOTO, PLANE_NORMAL_SPEED, -1, -1, 100, 100)
						ENDIF					
						
						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_SendPlaneFlyaway)
						
						NET_PRINT_TIME() NET_PRINT("     ---------->     PLANE TAKEDOWN -  CONTROL_PILOT_FLIGHT_TASK - LEAVE SEQUENCE - DEST = ") NET_PRINT_VECTOR(vDest) NET_NL()
					ENDIF
					
				ENDIF
			//ENDIF
		ENDIF
	ENDIF
ENDPROC

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  ENTITY CREATION                  /////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////
///    		PLANE AND PILOT    			///
///////////////////////////////////////////
//PURPOSE: Create the Plane
FUNC BOOL CREATE_PLANE()
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niPlane)
		IF REQUEST_LOAD_MODEL(serverBD.PlaneModel)
			IF CREATE_NET_VEHICLE(serverBD.niPlane, serverBD.PlaneModel, serverBD.vStartLocation, GET_HEADING_BETWEEN_VECTORS_2D(serverBD.vStartLocation, serverBD.vCheckpoint[0]))	
				
				//SET_ENTITY_HEALTH(NET_TO_VEH(serverBD.niPlane), serverBD.CrateDropData.PlaneData.Plane.iHealth)
				//SET_VEHICLE_ENGINE_HEALTH(NET_TO_VEH(serverBD.niPlane), TO_FLOAT(serverBD.CrateDropData.PlaneData.Plane.iHealth))
				//SET_VEHICLE_PETROL_TANK_HEALTH(NET_TO_VEH(serverBD.niPlane), TO_FLOAT(serverBD.CrateDropData.PlaneData.Plane.iHealth))
				
				SET_VEHICLE_DOORS_LOCKED(NET_TO_VEH(serverBD.niPlane), VEHICLELOCK_LOCKED)
				
				FREEZE_ENTITY_POSITION(NET_TO_VEH(serverBD.niPlane), FALSE)
				SET_ENTITY_DYNAMIC(NET_TO_VEH(serverBD.niPlane), TRUE)
				ACTIVATE_PHYSICS(NET_TO_VEH(serverBD.niPlane))
				SET_VEHICLE_FORWARD_SPEED(NET_TO_VEH(serverBD.niPlane), PLANE_SPEED)     
				SET_HELI_BLADES_FULL_SPEED(NET_TO_VEH(serverBD.niPlane))
				SET_VEHICLE_ENGINE_ON(NET_TO_VEH(serverBD.niPlane), TRUE, TRUE)
				SET_VEHICLE_ENGINE_CAN_DEGRADE(NET_TO_VEH(serverBD.niPlane), FALSE)
				
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(NET_TO_VEH(serverBD.niPlane), FALSE)
								
				IF serverBD.PlaneModel = CUBAN800
					CONTROL_LANDING_GEAR(NET_TO_VEH(serverBD.niPlane), LGC_RETRACT_INSTANT)	
				ENDIF
				
				//Prevents the vehicle from being used for activities that use passive mode - MJM 
				IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
					INT iDecoratorValue
					IF DECOR_EXIST_ON(NET_TO_VEH(serverBD.niPlane), "MPBitset")
						iDecoratorValue = DECOR_GET_INT(NET_TO_VEH(serverBD.niPlane), "MPBitset")
					ENDIF
					SET_BIT(iDecoratorValue, MP_DECORATOR_BS_NON_PASSIVE_MODE_VEHICLE)
					DECOR_SET_INT(NET_TO_VEH(serverBD.niPlane), "MPBitset", iDecoratorValue)
				ENDIF
				
				NET_PRINT_TIME() NET_PRINT("     ---------->     PLANE TAKEDOWN -  CREATED PLANE - ") NET_PRINT_VECTOR(serverBD.vStartLocation) NET_NL()
			ENDIF
		ENDIF
	ENDIF
	
	//Check we have created the vehicle
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niPlane)
		//NET_PRINT_TIME() NET_PRINT("     ---------->     PLANE TAKEDOWN -  FAILING ON PLANE CREATION") NET_NL()
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

//PURPOSE: Create the Pilot
FUNC BOOL CREATE_PILOT()
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niPilot)
	AND REQUEST_LOAD_MODEL(serverBD.PilotModel)
	AND NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niPlane)
		IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niPlane)
			IF CREATE_NET_PED_IN_VEHICLE(serverBD.niPilot, serverBD.niPlane, PEDTYPE_CRIMINAL, serverBD.PilotModel, VS_DRIVER)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NET_TO_PED(serverBD.niPilot), TRUE)
				SET_PED_RELATIONSHIP_GROUP_HASH(NET_TO_PED(serverBD.niPilot), rgFM_AiDislike)
				SET_PED_RANDOM_COMPONENT_VARIATION(NET_TO_PED(serverBD.niPilot))
				
				SET_PED_KEEP_TASK(NET_TO_PED(serverBD.niPilot), TRUE)
				
				//SET_ENTITY_HEALTH(NET_TO_PED(serverBD.niPilot), ROUND(200*g_sMPTunables.fAiHealthModifier))
				NET_PRINT_TIME() NET_PRINT("     ---------->     PLANE TAKEDOWN - CREATED PILOT ") NET_NL()
			ENDIF
		ENDIF
	ENDIF
	
	//Check we have created the ped
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niPilot)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

//PURPOSE: Creates the Plane and Pilot that will deliver the Vehicle
FUNC BOOL CREATE_PLANE_AND_PILOT()
	IF REQUEST_LOAD_MODEL(serverBD.PlaneModel)
	AND REQUEST_LOAD_MODEL(serverBD.PilotModel)
		IF CREATE_PLANE()
		AND CREATE_PILOT()
			SET_MODEL_AS_NO_LONGER_NEEDED(serverBD.PlaneModel)
			SET_MODEL_AS_NO_LONGER_NEEDED(serverBD.PilotModel)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Runs through the max num participents and does relevant checks
PROC MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER()
//	INT iParticipant
//	
//	//Initialise Staggered Loop
//	IF iStaggeredParticipant = 0
//		//bDoEndStaggeredLoopChecks = FALSE
//		//NET_PRINT_TIME() NET_PRINT(" ---> PLANE TAKEDOWN -  MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER - iStaggeredParticipant RESET") NET_NL()
//	ENDIF
	
	SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerEventData
	
	//For now we only need to do this check once the Plane is destroyed.
	//IF NOT IS_VEHICLE_DRIVEABLE(NET_TO_VEH(serverBD.niPlane))	//IF NOT IS_NET_VEHICLE_DRIVEABLE(serverBD.niPlane)
	IF IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niPlane))
		IF serverBD.iServerGameState != GAME_STATE_END
			REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iStaggeredParticipant
				IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iStaggeredParticipant))
					PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iStaggeredParticipant))
					//PED_INDEX PlayerPedId = GET_PLAYER_PED(PlayerId)
					
					//** CHECKS THAT DON'T NEED THE PLAYER TO BE ALIVE **//
					//Check if anybody has destroyed the Plane
					IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_DestroyedPlane)
						
						//Send Ticker
						IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_UsingHelicopter)
							TickerEventData.TickerEvent = TICKER_EVENT_PLANE_TAKEDOWN_PLAYER
						ELSE
							TickerEventData.TickerEvent = TICKER_EVENT_PLANE_TAKEDOWN_PLAYER_HELI
						ENDIF
						TickerEventData.playerID = PlayerId
						BROADCAST_TICKER_EVENT(TickerEventData, ALL_PLAYERS_ON_SCRIPT())
						
						serverBD.iServerGameState = GAME_STATE_END
						PRINTLN("     ---------->     PLANE TAKEDOWN - serverBD.iServerGameState = GAME_STATE_END 999 - ", GET_PLAYER_NAME(PlayerId), " DESTROYED THE PLANE")
						EXIT
					ENDIF
					
					IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_SendPlaneFlyaway)
						IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_DoFlyaway)
							SET_BIT(serverBD.iServerBitSet, biS_DoFlyaway)
						ENDIF
					ENDIF
					
					
					
					//** CHECKS THAT REQUIRE THE PLAYER TO BE ALIVE **//
					//IF IS_NET_PLAYER_OK(PlayerId)
						
					//ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
	
//	iStaggeredParticipant++
//	
//	//Reset Staggered Loop
//	IF iStaggeredParticipant = NETWORK_GET_MAX_NUM_PARTICIPANTS()
//		iStaggeredParticipant = 0
//		//bDoEndStaggeredLoopChecks = TRUE
//		//NET_PRINT_TIME() NET_PRINT(" ---> PLANE TAKEDOWN -  MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER - iStaggeredParticipant RESET") NET_NL()
//	ENDIF
ENDPROC

//PURPOSE: Returns TRUE if the Plane is stuck or not int the air
FUNC BOOL IS_PLANE_STUCK()
	/*IF NOT IS_ENTITY_IN_AIR(NET_TO_VEH(serverBD.niPlane))
		NET_PRINT_TIME() NET_PRINT("     ---------->     PLANE TAKEDOWN -  IS_PLANE_STUCK - TRUE - NOT IN AIR ") NET_NL()
		RETURN TRUE
	ENDIF
	
	IF IS_ENTITY_IN_WATER(NET_TO_VEH(serverBD.niPlane))
		NET_PRINT_TIME() NET_PRINT("     ---------->     PLANE TAKEDOWN -  IS_PLANE_STUCK - TRUE - IN WATER ") NET_NL()
		RETURN TRUE
	ENDIF*/
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niPlane)
		IF IS_VEHICLE_STUCK_TIMER_UP(NET_TO_VEH(serverBD.niPlane), VEH_STUCK_ON_ROOF, ROOF_TIME)
			NET_PRINT_TIME() NET_PRINT("     ---------->     PLANE TAKEDOWN -  IS_PLANE_STUCK - TRUE - VEH_STUCK_ON_ROOF ") NET_NL()
			RETURN TRUE
		ENDIF
		
		IF IS_VEHICLE_STUCK_TIMER_UP(NET_TO_VEH(serverBD.niPlane), VEH_STUCK_ON_SIDE, SIDE_TIME)
			NET_PRINT_TIME() NET_PRINT("     ---------->     PLANE TAKEDOWN -  IS_PLANE_STUCK - TRUE - VEH_STUCK_ON_SIDE ") NET_NL()
			RETURN TRUE
		ENDIF
		
		IF IS_VEHICLE_STUCK_TIMER_UP(NET_TO_VEH(serverBD.niPlane), VEH_STUCK_JAMMED, JAMMED_TIME)
			NET_PRINT_TIME() NET_PRINT("     ---------->     PLANE TAKEDOWN -  IS_PLANE_STUCK - TRUE - VEH_STUCK_JAMMED ") NET_NL()
			RETURN TRUE
		ENDIF
		
		IF IS_VEHICLE_STUCK_TIMER_UP(NET_TO_VEH(serverBD.niPlane), VEH_STUCK_HUNG_UP, HUNG_UP_TIME)
			NET_PRINT_TIME() NET_PRINT("     ---------->     PLANE TAKEDOWN -  IS_PLANE_STUCK - TRUE -  ") NET_NL()
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Checks for the plane (destroyed, stuck, etc)
PROC CONTROL_PLANE_BODY()
	IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niPlane)
		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DestroyedPlane)
			
			// this check doesn't return false when the vehicle is on fire which was causing bug 1151056
			//IF NOT IS_NET_VEHICLE_DRIVEABLE(serverBD.niPlane)	
			
			IF NOT IS_VEHICLE_DRIVEABLE(NET_TO_VEH(serverBD.niPlane))
				PRINTLN("     ---------->     PLANE TAKEDOWN -  CONTROL_PLANE_BODY - NOT IS_VEHICLE_DRIVEABLE")
			ENDIF
			IF IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niPlane))
				PRINTLN("     ---------->     PLANE TAKEDOWN -  CONTROL_PLANE_BODY - IS_ENTITY_DEAD")
			ENDIF
			
			//IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niPlane)
			//AND DOES_ENTITY_EXIST(NET_TO_VEH(serverBD.niPlane))
			//IF NOT IS_VEHICLE_DRIVEABLE(NET_TO_VEH(serverBD.niPlane))
			IF IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niPlane))
				//Check if player was Killer
				WEAPON_TYPE TempWeapon
				IF PLAYER_ID() = NETWORK_GET_DESTROYER_OF_NETWORK_ID(serverBD.niPlane, TempWeapon)
					INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_TIMES_COMPLETE_PLANETAKE)
					INT iTimesCompletedForCash = GET_MP_INT_CHARACTER_STAT(MP_STAT_TIMES_COMPLETE_PLANETAKE)
					INT iTimesCompletedForXP = iTimesCompletedForCash
					PRINTLN("     ---------->     PLANE TAKEDOWN -  CONTROL_PLANE_BODY - iTimesCompleted = ", iTimesCompletedForCash)
					
					//XP REWARD
					IF iTimesCompletedForXP > g_sMPTunables.iPlane_Takedown_RP_Max_Times
						iTimesCompletedForXP = g_sMPTunables.iPlane_Takedown_RP_Max_Times
						PRINTLN("     ---------->     PLANE TAKEDOWN -  CONTROL_PLANE_BODY - FOR CALCULATION CAP iTimesCompletedForXP AT ", g_sMPTunables.iPlane_Takedown_RP_Max_Times)
					ENDIF
					INT iXP = (g_sMPTunables.iPlane_Takedown_rewards_XP*iTimesCompletedForXP)
					GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD, PLAYER_PED_ID(), "XPT_KAIE", XPTYPE_SKILL, XPCATEGORY_COMPLETE_PLANE_TAKEDOWN, iXP, 1)
					
					//CASH REWARD
					IF iTimesCompletedForCash > g_sMPTunables.iPlane_Takedown_Cash_Max_Times
						iTimesCompletedForCash = g_sMPTunables.iPlane_Takedown_Cash_Max_Times
						PRINTLN("     ---------->     PLANE TAKEDOWN -  CONTROL_PLANE_BODY - FOR CALCULATION CAP iTimesCompletedForCash AT ", g_sMPTunables.iPlane_Takedown_Cash_Max_Times)
					ENDIF
					INT iCash = (g_sMPTunables.iPlane_Takedown_rewards_CASH*iTimesCompletedForCash)
					GIVE_LOCAL_PLAYER_CASH_END_OF_MISSION(iCash)
					
					AMBIENT_JOB_DATA AJdata
					g_i_cashForEndEventShard = iCash
										
					PRINTLN("     ---------->     PLANE TAKEDOWN - CONTROL_PLANE_BODY - [MAGNATE_GANG_BOSS] - Cash before boss cut: $", iCash)
					GB_HANDLE_GANG_BOSS_CUT(iCash)
					PRINTLN("     ---------->     PLANE TAKEDOWN - CONTROL_PLANE_BODY - [MAGNATE_GANG_BOSS] - Cash after boss cut: $", iCash)
					
					IF iCash > 0
						IF USE_SERVER_TRANSACTIONS()
							INT iTransactionID
							TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_AMBIENT_JOB_PLANE_TAKEDOWN, iCash, iTransactionID)
						ELSE
							NETWORK_EARN_FROM_AMBIENT_JOB(iCash, "AM_PLANE_TAKEDOWN", AJdata)
						ENDIF
					ENDIF
					
					//Awards
					INCREMENT_BY_MP_INT_CHARACTER_AWARD(MP_AWARD_TAKEDOWNSMUGPLANE, 1)
					
					//Text Message
					IF GET_RANDOM_BOOL()
						Send_MP_Txtmsg_From_CharSheetID(CHAR_TREVOR, "PTD_PASS0")
					ELSE
						Send_MP_Txtmsg_From_CharSheetID(CHAR_TREVOR, "PTD_PASS1")
					ENDIF
					
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DestroyedPlane)
					PRINTLN("     ---------->     PLANE TAKEDOWN -  CONTROL_PLANE_BODY - I DESTROYED THE PLANE!")
					PRINTLN("     ---------->     PLANE TAKEDOWN -  CONTROL_PLANE_BODY - REWARD XP = ", iXP)
					PRINTLN("     ---------->     PLANE TAKEDOWN -  CONTROL_PLANE_BODY - REWARD CASH = $", iCash)
				ENDIF
			ELSE
				
				//Server Check if Plane has Reached Checkpoint
				IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
					IF serverBD.iCurrentCheckpoint < MAX_NUM_CHECKPOINTS
						IF IS_ENTITY_AT_COORD(NET_TO_VEH(serverBD.niPlane), serverBD.vCheckpoint[serverBD.iCurrentCheckpoint], <<PLANE_TARGET_RADIUS, PLANE_TARGET_RADIUS, 500>>)
							PRINTLN("     ---------->     PLANE TAKEDOWN - CONTROL_PLANE_BODY - PLANE HAS REACHED CHECKPOINT ", serverBD.iCurrentCheckpoint, " AT ", serverBD.vCheckpoint[serverBD.iCurrentCheckpoint])
							serverBD.iCurrentCheckpoint++
						ENDIF
					ENDIF
				ENDIF
				//In Control Participant deal with Plane
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niPlane)
					
					CONTROL_PILOT_FLIGHT_TASK()
					
					//Stuck Checks
					IF IS_PLANE_STUCK()
						IF TAKE_CONTROL_OF_NET_ID(serverBD.niPlane)
						AND TAKE_CONTROL_OF_NET_ID(serverBD.niPilot) //** TWH - RLB - (fix for B*1417115 - cleanup pilot as well as plane).
							NETWORK_EXPLODE_VEHICLE(NET_TO_VEH(serverBD.niPlane), TRUE, TRUE)
							NET_PRINT_TIME() NET_PRINT("     ---------->     PLANE TAKEDOWN -  CONTROL_PLANE_BODY - NETWORK_EXPLODE_VEHICLE - PLANE STUCK ") NET_PRINT_VECTOR(GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niPlane))) NET_NL()
							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPlane)
								CLEANUP_NET_ID(serverBD.niPlane)
							ENDIF
							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPilot)
								SET_PED_KEEP_TASK(NET_TO_PED(serverBD.niPilot), TRUE)
								CLEANUP_NET_ID(serverBD.niPilot)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC




/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  MISSION PROCS                  //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////



//PURPOSE: Process the JOYRIDER stages for the Client
PROC PROCESS_TAKEDOWN_CLIENT()
	SWITCH playerBD[PARTICIPANT_ID_TO_INT()].eTakedownStage		
		CASE eAD_TAKEDOWN
			MAINTAIN_PLANE_BLIP()
			CONTROL_PLANE_BODY()
			IF serverBD.eTakedownStage > eAD_TAKEDOWN
				playerBD[PARTICIPANT_ID_TO_INT()].eTakedownStage = eAD_CLEANUP
				NET_PRINT_TIME() NET_PRINT("     ---------->     PLANE TAKEDOWN -  PLAYER STAGE = eAD_CLEANUP    <----------     ") NET_NL()
			ENDIF
		BREAK
						
		CASE eAD_CLEANUP
			NET_PRINT_TIME() NET_PRINT("     ---------->     PLANE TAKEDOWN -  SCRIPT CLEANUP B     <----------     ") NET_NL()
			SCRIPT_CLEANUP()
		BREAK
	ENDSWITCH
ENDPROC

//PURPOSE: Process the JOYRIDER stages for the Server
PROC PROCESS_TAKEDOWN_SERVER()
	SWITCH serverBD.eTakedownStage		
		CASE eAD_TAKEDOWN
			
		BREAK
						
		CASE eAD_CLEANUP
			
		BREAK
	ENDSWITCH
ENDPROC

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                      Main Loop                      //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
SCRIPT(MP_MISSION_DATA missionScriptArgs)
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
	AND IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE)
		#IF IS_DEBUG_BUILD	
			NET_PRINT_TIME() NET_PRINT("MP: Starting mission ") NET_NL()
		#ENDIF
		
		// Planery out all the initial game starting duties. 
		PROCESS_PRE_GAME(missionScriptArgs)	
		
		// Setup widgets. Make sure this is done after PROCESS_PRE_GAME()
		#IF IS_DEBUG_BUILD
			CREATE_WIDGETS()
		#ENDIF
	ELSE
		#IF IS_DEBUG_BUILD NET_LOG("PLANE TAKEDOWN -  NETWORK_IS_GAME_IN_PROGRESS = FALSE")	#ENDIF
		NET_PRINT_TIME() NET_PRINT("     ---------->     PLANE TAKEDOWN -  MISSION END - NETWORK_IS_GAME_IN_PROGRESS = FALSE - SCRIPT CLEANUP P     <----------     ") NET_NL()
		SCRIPT_CLEANUP()
	ENDIF
	
	// Main loop
	WHILE TRUE

		// One wait to rule them all. This can be the ONLY wait from here on in.....
		MP_LOOP_WAIT_ZERO()
				
		// If we have a match end event, bail
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		OR SHOULD_MP_AMBIENT_SCRIPT_TERMINATE(MPAM_TYPE_PLANE_TAKEDOWN)
			#IF IS_DEBUG_BUILD NET_LOG("SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE")	#ENDIF
			NET_PRINT_TIME() NET_PRINT("     ---------->     PLANE TAKEDOWN -  MISSION END - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE - SCRIPT CLEANUP B     <----------     ") NET_NL()
			SCRIPT_CLEANUP()
		ENDIF
		
		//Check if player is in a tutorial session and end
		IF NETWORK_IS_IN_TUTORIAL_SESSION()
			#IF IS_DEBUG_BUILD NET_LOG("PLAYER IN TUT SESSION")	#ENDIF
			NET_PRINT_TIME() NET_PRINT("     ---------->     PLANE TAKEDOWN -  MISSION END - PLAYER IN TUTORIAL SESSION - SCRIPT CLEANUP C     <----------     ") NET_NL()
			SCRIPT_CLEANUP()
		ENDIF
		
		// Deal with the debug
		#IF IS_DEBUG_BUILD		
			UPDATE_WIDGETS()
		#ENDIF		
	
		// If we need to leave the mission, make sure we just straight to the end stage and terminate the script - DON'T NEED FOR AMBIENT MISSION
		/*IF SHOULD_PLAYER_LEAVE_MP_MISSION() 
			#IF IS_DEBUG_BUILD
			NET_PRINT_TIME() NET_PRINT( GET_MP_MISSION_NAME(thisMission), " Leave mission = TRUE \n")
			#ENDIF
			playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_LEAVE
			#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_LEAVE")	#ENDIF
		ENDIF*/
		
		// -----------------------------------
		// Process your game logic.....
		SWITCH GET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT())
			
			// Wait until the server gives the all go before moving on
			CASE GAME_STATE_INI
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					
					IF HAS_PLAYER_BEEN_IN_SESSION_THIS_LONG(CRATE_DROP_DISPLAY_DELAY)
						IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niPlane)
							IF IS_VEHICLE_DRIVEABLE(NET_TO_VEH(serverBD.niPlane))
								IF !SHOULD_HIDE_LOCAL_UI()
									IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_UsingHelicopter)
										PRINT_HELP("PTD_HELP1")		//A smuggler's plane is flying around Los Santos. Bring the plane down before it leaves.
									ELSE
										PRINT_HELP("PTD_HELP1H")	//A smuggler's helicopter is flying around Los Santos. Bring the plane down before it leaves.
									ENDIF
									NET_PRINT_TIME() NET_PRINT("     ---------->     PLANE TAKEDOWN -  HELP TEXT DONE    <----------     ") NET_NL()
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_RUNNING
					NET_PRINT_TIME() NET_PRINT("     ---------->     PLANE TAKEDOWN -  playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_RUNNING    <----------     ") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_RUNNING")	#ENDIF
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY
					NET_PRINT_TIME() NET_PRINT("     ---------->     PLANE TAKEDOWN -  playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY 1    <----------     ") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_TERMINATE_DELAY")	#ENDIF
				ENDIF
			BREAK
			
			// Main gameplay state.
			CASE GAME_STATE_RUNNING
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					PROCESS_TAKEDOWN_CLIENT()
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY
					NET_PRINT_TIME() NET_PRINT("     ---------->     PLANE TAKEDOWN -  playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY 2   <----------     ") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_TERMINATE_DELAY")	#ENDIF
				ENDIF
			BREAK
			
			CASE GAME_STATE_TERMINATE_DELAY 
				SET_MISSION_FINISHED(serverBD.MissionTerminateDelayTimer)
		      	IF IS_MISSION_READY_TO_CLEANUP(serverBD.MissionTerminateDelayTimer)
		            playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
					NET_PRINT_TIME() NET_PRINT("     ---------->     PLANE TAKEDOWN -  playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END 3    <----------     ") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_END 3")	#ENDIF
		      	ENDIF
			BREAK
			
			// The game stage the local player is placed when we want him to leave a mission
			CASE GAME_STATE_LEAVE
				//LEAVE_MY_MP_MISSION(thisMission)
				playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
				NET_PRINT_TIME() NET_PRINT("     ---------->     PLANE TAKEDOWN -  playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END 2    <----------     ") NET_NL()
				#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_END 2")	#ENDIF
			FALLTHRU
			
			//Cleans up then terminates the mission
			CASE GAME_STATE_END
				NET_PRINT_TIME() NET_PRINT("     ---------->     PLANE TAKEDOWN -  SCRIPT CLEANUP A     <----------     ") NET_NL()
				SCRIPT_CLEANUP()
			BREAK

		ENDSWITCH
		
		// -----------------------------------
		// Process server game logic		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			
			SWITCH GET_SERVER_MISSION_STATE()
				
				// For CnC group missions, we may want to wait for all players to have launched the script
				CASE GAME_STATE_INI
					IF CREATE_PLANE_AND_PILOT()
						serverBD.iServerGameState = GAME_STATE_RUNNING
						NET_PRINT_TIME() NET_PRINT("     ---------->     PLANE TAKEDOWN -  serverBD.iServerGameState = GAME_STATE_RUNNING    <----------     ") NET_NL()
						#IF IS_DEBUG_BUILD NET_LOG_Host("iServerGameState = GAME_STATE_RUNNING")	#ENDIF
					ENDIF
				BREAK
				
				// Look for game end conditions
				CASE GAME_STATE_RUNNING
					
					MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER()
					PROCESS_TAKEDOWN_SERVER()
					
					#IF IS_DEBUG_BUILD
						IF bHostEndMissionNow
						OR MPGlobalsAmbience.bKillActiveEvent
						MPGlobalsAmbience.bKillActiveEvent = FALSE
							serverBD.iServerGameState = GAME_STATE_END
							NET_PRINT_TIME() NET_PRINT("     ---------->     PLANE TAKEDOWN -  serverBD.iServerGameState = GAME_STATE_END 1    <----------     ") NET_NL()
							#IF IS_DEBUG_BUILD NET_LOG_Host("iServerGameState = GAME_STATE_END 1")	#ENDIF
						ENDIF
					#ENDIF	
					
					IF HAVE_MISSION_END_CONDITIONS_BEEN_MET()
						serverBD.iServerGameState = GAME_STATE_END
						NET_PRINT_TIME() NET_PRINT("     ---------->     PLANE TAKEDOWN -  serverBD.iServerGameState = GAME_STATE_END 4    <----------     ") NET_NL()
						#IF IS_DEBUG_BUILD NET_LOG_Host("iServerGameState = GAME_STATE_END 4")	#ENDIF
					ENDIF
					
				BREAK
				
				CASE GAME_STATE_END
				BREAK
				
			ENDSWITCH
			
		ENDIF
		
	ENDWHILE
	
ENDSCRIPT
