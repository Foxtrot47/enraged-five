/// Freemode Car Mod tutorial
///    Dave W



USING "globals.sch"

// Game Headers
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_camera.sch"
USING "commands_hud.sch"

// Network Headers
USING "net_include.sch"
USING "net_events.sch"

// Headers

USING "net_mission.sch"
USING "net_scoring_common.sch"

USING "commands_path.sch"
USING "net_blips.sch"
USING "commands_zone.sch"


USING "hud_drawing.sch"
USING "net_ambience.sch"


USING "shop_public.sch"
USING "net_garages.sch"



USING "net_mission_details_overlay.sch"

USING "net_hud_displays.sch"

USING "fm_hold_up_tut.sch"

USING "net_wait_zero.sch"

#IF IS_DEBUG_BUILD
	USING "profiler.sch"
#ENDIF

// Game States
CONST_INT GAME_STATE_INI 		0
CONST_INT GAME_STATE_INI_SPAWN	1
CONST_INT GAME_STATE_RUNNING	2
CONST_INT GAME_STATE_LEAVE		3
CONST_INT GAME_STATE_FAILED		4
CONST_INT GAME_STATE_TERMINATE_DELAY 5
CONST_INT GAME_STATE_END		6

//MP_MISSION thisMission = eFM_HOLD_UP_TUT

CONST_INT biS_AllPlayersFinished		0

// Everyone can read this data, only the server can update it. 
STRUCT ServerBroadcastData
	INT iServerGameState
	INT iServerBitSet
	
	SCRIPT_TIMER timeTerminate
	HOLD_UP_TUT_SERVER_STRUCT holdUpTutServer
	
	#IF IS_DEBUG_BUILD
		INT iSceneID
		NETWORK_INDEX niPedSync
	#ENDIF
ENDSTRUCT
ServerBroadcastData serverBD


INT iServerStaggeredLoopCount

CONST_INT iCarModProgGetCarModded			0

CONST_INT biP_ModdedCar						0
CONST_INT biP_FinishedCarModTut				1

STRUCT PlayerBroadcastData
	INT iGameState
	INT iCarModTutProg
	INT iPlayerBitSet
ENDSTRUCT
PlayerBroadcastData PlayerBD[NUM_NETWORK_PLAYERS]


SHOP_NAME_ENUM shopClosestCarMod = EMPTY_SHOP

INT iBoolsBitSet

CONST_INT biL_ServerLoopedThroughEveryone		0
CONST_INT biL_StaggeredAllPlayersFinished		1
CONST_INT biL_ToldToGetCar						2
CONST_INT biL_ToldToGoToModshop					3
CONST_INT biL_UseCarsHelp						4
CONST_INT biL_TextCleared						5
CONST_INT biL_OnMission							6
CONST_INT biL_ToldToFindAnother					7
CONST_INT biL_DoneValidCarHelp					8
CONST_INT biL_BrowsingSHop						9
CONST_INT biL_LoseCops							10
CONST_INT biL_PremiumCar						11


SCRIPT_TIMER timeSuitableCar
SCRIPT_TIMER timeSinceOnMission
SCRIPT_TIMER timePremiumCar

BOOL bLocalPurchasedAllItems
//INT iLocalMeetLoc
#IF IS_DEBUG_BUILD
	BOOL bDoDebug = FALSE
	BOOL bDisableAllDebug = FALSE
	PROC NET_DW_PRINT(STRING sText)
		IF NOT bDisableAllDebug

			TEXT_LABEL_31 tl31
			IF NETWORK_IS_GAME_IN_PROGRESS()
				tl31 = GET_TIME_AS_STRING(GET_NETWORK_TIME())
			ENDIF
			CPRINTLN(DEBUG_MP_TUTORIAL,tl31 ," [dsw] [FM_CarModTut] ", sText)
		ENDIF
	ENDPROC 

	PROC NET_DW_PRINT_STRING_INT(STRING sText1, INT i)
		IF NOT bDisableAllDebug
			
			TEXT_LABEL_31 tl31
			IF NETWORK_IS_GAME_IN_PROGRESS()
				tl31 = GET_TIME_AS_STRING(GET_NETWORK_TIME())
			ENDIF
			CPRINTLN(DEBUG_MP_TUTORIAL,tl31 ," [dsw] [FM_CarModTut] ", sText1, i)
		ENDIF
	ENDPROC

	PROC NET_DW_PRINT_STRING_FLOAT(STRING sText1, FLOAT f)
		IF NOT bDisableAllDebug
		//	NET_PRINT_TIME() NET_PRINT("[FM_CarModTut] [DSW] ") NET_PRINT_STRING_FLOAT(sText1, f) NET_NL()
			TEXT_LABEL_31 tl31
			IF NETWORK_IS_GAME_IN_PROGRESS()
				tl31 = GET_TIME_AS_STRING(GET_NETWORK_TIME())
			ENDIF
			CPRINTLN(DEBUG_MP_TUTORIAL,tl31 ," [dsw] [FM_CarModTut] ", sText1, f)
		ENDIF
	ENDPROC

	PROC NET_DW_PRINT_STRING_VECTOR(STRING sText1, VECTOR v)
		IF NOT bDisableAllDebug
		//	NET_PRINT_TIME() NET_PRINT("[FM_CarModTut] [DSW] ") NET_PRINT_STRING_VECTOR(sText1, v) NET_NL()
			TEXT_LABEL_31 tl31
			IF NETWORK_IS_GAME_IN_PROGRESS()
				tl31 = GET_TIME_AS_STRING(GET_NETWORK_TIME())
			ENDIF
			CPRINTLN(DEBUG_MP_TUTORIAL,tl31 ," [dsw] [FM_CarModTut] ", sText1, v)
		ENDIF
	ENDPROC
	
	PROC NET_DW_PRINT_STRINGS(STRING sText1, STRING sText2)
		IF NOT bDisableAllDebug
		//	NET_PRINT_TIME() NET_PRINT("[FM_CarModTut] [DSW] ") NET_PRINT_STRINGS(sText1, sText2) NET_NL()
			TEXT_LABEL_31 tl31
			IF NETWORK_IS_GAME_IN_PROGRESS()
				tl31 = GET_TIME_AS_STRING(GET_NETWORK_TIME())
			ENDIF
			CPRINTLN(DEBUG_MP_TUTORIAL,tl31 ," [dsw] [FM_CarModTut] ", sText1, sText2)
		ENDIF
	ENDPROC
	
	PROC NET_DW_PRINT_STRING_WITH_PLAYER_NAME(STRING sText, PLAYER_INDEX player)
		IF NOT bDisableAllDebug
		//	NET_PRINT_TIME() NET_PRINT("[FM_CarModTut] [DSW] ") NET_PRINT_STRINGS(" ", sText) NET_PRINT_STRINGS(" ", GET_PLAYER_NAME(player)) NET_NL()
			TEXT_LABEL_31 tl31
			IF NETWORK_IS_GAME_IN_PROGRESS()
				tl31 = GET_TIME_AS_STRING(GET_NETWORK_TIME())
			ENDIF
			CPRINTLN(DEBUG_MP_TUTORIAL,tl31 ," [dsw] [FM_CarModTut] ", sText, GET_PLAYER_NAME(player))
		ENDIF
	ENDPROC
	
	BOOL bWdDoStaggeredDebug
	BOOL bWdUnlockModshops
	BOOL bWdShowUpgrade
	BOOL bWdInANyModSHop
	BOOL bWdBrowsingShop
	BOOL bWdModShopChecks
	BOOL bWdDoSyncSceneTest
	BOOL bWdResetSyncScene
	BOOL bWdPurchasedItems
	INT iWdSyncSceneTestProg
	FLOAT fWdUpgradeLevel
	FLOAT fWdMaxUpgradeLevel
	PROC CREATE_WIDGETS()
		START_WIDGET_GROUP("Car mod Tut")
			//Profiling widgets
			#IF SCRIPT_PROFILER_ACTIVE
			CREATE_SCRIPT_PROFILER_WIDGET()
			#ENDIF		
		/*	START_WIDGET_GROUP("Player")
				ADD_WIDGET_BOOL("SHow player info", bWdShowPlayerInfo)
				ADD_WIDGET_INT_READ_ONLY("Tut Prog", iWdTutProg)
				ADD_WIDGET_INT_READ_ONLY("Partner", iWdPartner)
				ADD_WIDGET_INT_READ_ONLY("Loc", iWdLoc)
				ADD_WIDGET_BOOL("Clear stat", bResetTut)
			STOP_WIDGET_GROUP() */
			ADD_WIDGET_BOOL("Unlock modshops", bWdUnlockModshops)
			START_WIDGET_GROUP("Player")
				ADD_WIDGET_BOOL("Do debug",bDoDebug)
				ADD_BIT_FIELD_WIDGET("Local", iBoolsBitSet)
				ADD_BIT_FIELD_WIDGET("Player", PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet)
				ADD_WIDGET_FLOAT_SLIDER("Upgrade Level", fWdUpgradeLevel, -100.0, 100.0, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("Max Upgrade Level", fWdMaxUpgradeLevel, 0.0, 1000.0, 0.1)
				ADD_WIDGET_BOOL("Show upgrade level", bWdShowUpgrade)
				ADD_WIDGET_BOOL("In any mod shop?", bWdInANyModSHop)
				ADD_WIDGET_BOOL("Browsing ?", bWdBrowsingShop)
				ADD_WIDGET_BOOL("Mod shop checks", bWdModShopChecks)
				ADD_WIDGET_BOOL("Purchased items?", bWdPurchasedItems)
				ADD_WIDGET_BOOL("Sync Scene test", bWdDoSyncSceneTest)
				ADD_WIDGET_BOOL("Reset sync scene", bWdResetSyncScene)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Server")
				
				ADD_WIDGET_BOOL("Staggered debug ", bWdDoStaggeredDebug)
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP() 
	ENDPROC
	
	
#ENDIF

FUNC INT GET_CLIENT_MISSION_STATE(INT iPlayer)
	RETURN playerBD[iPlayer].iGameState
ENDFUNC

FUNC INT GET_SERVER_MISSION_STATE()
	RETURN serverBD.iServerGameState
ENDFUNC
FUNC BOOL IS_CAR_MOD_SHOP_VALID(SHOP_NAME_ENUM shopCarMod)
	
	BOOL bValid
	SWITCH shopCarMod
		CASE CARMOD_SHOP_01_AP
			bValid =  DOES_BLIP_EXIST(g_sShopSettings.blipID[CARMOD_SHOP_01_AP])
		BREAK

		CASE CARMOD_SHOP_05_ID2
			bValid =  DOES_BLIP_EXIST(g_sShopSettings.blipID[CARMOD_SHOP_05_ID2])
		
		BREAK
		
		CASE CARMOD_SHOP_06_BT1			// Car Mod - AMB3 (v_carmod)
			bValid =  DOES_BLIP_EXIST(g_sShopSettings.blipID[CARMOD_SHOP_06_BT1])
		BREAK
		
		CASE CARMOD_SHOP_07_CS1			// Car Mod - AMB4 (v_carmod3)
			bValid =  DOES_BLIP_EXIST(g_sShopSettings.blipID[CARMOD_SHOP_07_CS1])
		BREAK
		
		CASE CARMOD_SHOP_08_CS6			// Car Mod - AMB5 (v_carmod3)
			bValid =  DOES_BLIP_EXIST(g_sShopSettings.blipID[CARMOD_SHOP_08_CS6])
		BREAK
		
		CASE CARMOD_SHOP_SUPERMOD		// Car Mod - AMB6 (lr_supermod_int)
			bValid =  DOES_BLIP_EXIST(g_sShopSettings.blipID[CARMOD_SHOP_SUPERMOD])
		BREAK
			
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
//		NET_DW_PRINT_STRING_INT("IS_CAR_MOD_SHOP_VALID... Mod shop not valid! Shop int... ", ENUM_TO_INT(shopCarMod))
	#ENDIF
	
	RETURN bValid
ENDFUNC

PROC SET_CAR_MOD_BLIP_LONG_RANGE(SHOP_NAME_ENUM shopCarMod, BOOL bSetAsLongRange = TRUE)

//	BOOL bSetAsShortRange = NOT(bSetAsLongRange)
//	IF bSetAsShortRange ENDIF
	SWITCH shopCarMod
		CASE CARMOD_SHOP_01_AP
			SET_SHOP_BLIP_LONG_RANGE(CARMOD_SHOP_01_AP  ,bSetAsLongRange)
		
		BREAK

		CASE CARMOD_SHOP_05_ID2
			SET_SHOP_BLIP_LONG_RANGE(CARMOD_SHOP_05_ID2  ,bSetAsLongRange)
	
		BREAK
		
		CASE CARMOD_SHOP_06_BT1			// Car Mod - AMB3 (v_carmod)
			SET_SHOP_BLIP_LONG_RANGE(CARMOD_SHOP_06_BT1,bSetAsLongRange )
		BREAK
		
		CASE CARMOD_SHOP_07_CS1			// Car Mod - AMB4 (v_carmod3)
			SET_SHOP_BLIP_LONG_RANGE(CARMOD_SHOP_07_CS1,bSetAsLongRange)
		BREAK
		
		CASE CARMOD_SHOP_08_CS6			// Car Mod - AMB5 (v_carmod3)
			SET_SHOP_BLIP_LONG_RANGE(CARMOD_SHOP_08_CS6,bSetAsLongRange)
		BREAK
		
		CASE CARMOD_SHOP_SUPERMOD		// Car Mod - AMB6 (lr_supermod_int)
			SET_SHOP_BLIP_LONG_RANGE(CARMOD_SHOP_SUPERMOD,bSetAsLongRange)
		BREAK
		
		DEFAULT
			NET_SCRIPT_ASSERT("SET_CAR_MOD_BLIP_LONG_RANGE Didn't get carmod blip!")
		BREAK
	ENDSWITCH
ENDPROC

PROC SET_GPS_ROUTE_TO_CAR_MOD(SHOP_NAME_ENUM shopCarMod, BOOL bSetRoute = TRUE)
//	#IF IS_DEBUG_BUILD
//		
//		IF bSetRoute
//			NET_DW_PRINT("[SET_GPS_ROUTE_TO_CAR_MOD] called with bSetRoute = TRUE ")
//		ELSE
//			NET_DW_PRINT("[SET_GPS_ROUTE_TO_CAR_MOD] called with bSetRoute = FALSE ")
//		ENDIF
//	#ENDIF
	IF bSetRoute
	ENDIF
	IF DOES_BLIP_EXIST(g_sShopSettings.blipID[shopCarMod])
	//	SET_BLIP_ROUTE(g_sShopSettings.blipID[shopCarMod], bSetRoute)
		#IF IS_DEBUG_BUILD
			NET_DW_PRINT_STRINGS("[SET_GPS_ROUTE_TO_CAR_MOD] blip exists, setting gps to shop... ", GET_SHOP_BLIP_NAME(shopCarMod))
		#ENDIF	
	ELSE
		#IF IS_DEBUG_BUILD
			NET_DW_PRINT("[SET_GPS_ROUTE_TO_CAR_MOD] Blip doesn't exist! ")
		#ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Handles the cleanup of the script
PROC CLEANUP_SCRIPT()	
	
	#IF IS_DEBUG_BUILD
		NET_DW_PRINT("Running cleanup")
	#ENDIF
	Clear_Any_Objective_Text_From_This_Script()
	SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_CAR_MOD_TUT, FALSE)
	IF HAS_INTRO_TO_GARAGES_CUTSCENE_BEEN_DONE()
		CLEAR_TUTORIAL_INVITES_ARE_BLOCKED() 
		#IF IS_DEBUG_BUILD NET_DW_PRINT("Script cleanup clearing invites blocked as garage cutscene done already") #ENDIF
	ENDIF
//	SET_FM_UNLOCKS_BIT_SET()
	
	IF shopClosestCarMod <> EMPTY_SHOP
		SET_CAR_MOD_BLIP_LONG_RANGE(shopClosestCarMod, FALSE)
		SET_GPS_ROUTE_TO_CAR_MOD(shopClosestCarMod, FALSE)
		#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("Cleanup Clearing SET_SHOP_BLIP_LONG_RANGE for shop ", ENUM_TO_INT(shopClosestCarMod)) #ENDIF
	ENDIF
	
	IF NOT HAS_FM_CAR_MOD_TUT_BEEN_DONE()
		IF HAS_PLAYER_PURCHASED_ALL_MOD_SHOP_TUTORIAL_ITEMS(PLAYER_ID())
		OR bLocalPurchasedAllItems
			CPRINTLN(DEBUG_MP_TUTORIAL," [dsw] [FM_CarModTut] setting complete from CLEANUP_SCRIPT")
			SET_MP_BOOL_CHARACTER_STAT(MP_STAT_FM_CMODTUT_DONE, TRUE)
//			SET_FREEMODE_PROLOGUE_PROFILE_SETTINGS(PSFP_FM_CMODTUT_DONE)
		ELSE
			CPRINTLN(DEBUG_MP_TUTORIAL," [dsw] [FM_CarModTut] cleanup - Else HAS_PLAYER_PURCHASED_ALL_MOD_SHOP_TUTORIAL_ITEMS")
		ENDIF
	ELSE
		CPRINTLN(DEBUG_MP_TUTORIAL," [dsw] [FM_CarModTut] cleanup - Else HAS_FM_CAR_MOD_TUT_BEEN_DONE")
	ENDIF
	
	SET_LOCAL_PLAYER_DOING_MOD_SHOP_TUTORIAL(FALSE)
	//TERMINATE_THIS_THREAD()
	//RESET_FMMC_MISSION_VARIABLES(FALSE, FALSE, ciFMMC_END_OF_MISSION_STATUS_PASSED)
	
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

/// PURPOSE:
///    Do necessary pre game start ini.
PROC PROCESS_PRE_GAME(MP_MISSION_DATA missionScriptArgs)
	// This marks the script as a net script, and handles any instancing setup. 
	SETUP_MP_MISSION_FOR_NETWORK(GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(missionScriptArgs.mdID.idMission) , missionScriptArgs)
	
	// This makes sure the net script is active, waits until it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
	
	// This script will not be paused if another script calls PAUSE_GAME
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	
	// KGM: Wait for the first network broadcast before moving on - call this as the last instruction in pre-game
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		#IF IS_DEBUG_BUILD NET_DW_PRINT("Car mod tut failed to receive initial network broadcast. Cleaning up.") #ENDIF
		CLEANUP_SCRIPT()
	ENDIF
	
	SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_CAR_MOD_TUT, TRUE)
	SET_TUTORIAL_INVITES_ARE_BLOCKED()
	
	//SET_FM_UNLOCKS_BIT_SET()
	#IF IS_DEBUG_BUILD
		NET_DW_PRINT("Launching....V1")
	#ENDIF
ENDPROC

FUNC BOOL HAVE_MISSION_END_CONDITIONS_BEEN_MET()
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_AllPlayersFinished)
		#IF IS_DEBUG_BUILD NET_DW_PRINT("[HAVE_MISSION_END_CONDITIONS_BEEN_MET] True because biS_AllPlayersFinished") #ENDIF
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC


PROC MAINTAIN_SERVER_STAGGERED_LOOP()
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF iServerStaggeredLoopCount = 0
			CLEAR_BIT(iBoolsBitSet, biL_ServerLoopedThroughEveryone)
			SET_BIT(iBoolsBitSet, biL_StaggeredAllPlayersFinished)
			#IF IS_DEBUG_BUILD
				IF bWdDoStaggeredDebug
					NET_DW_PRINT("[ServStag] At Start of server staggered loop")
				ENDIF
			#ENDIF
		ENDIF
		
		
		
		
	
		PARTICIPANT_INDEX part = INT_TO_NATIVE(PARTICIPANT_INDEX, iServerStaggeredLoopCount )
		
		IF NETWORK_IS_PARTICIPANT_ACTIVE(part)
			PLAYER_INDEX player = NETWORK_GET_PLAYER_INDEX(part)
			IF NOT IS_BIT_SET(PlayerBD[iServerStaggeredLoopCount].iPlayerBitSet, biP_ModdedCar)
				CLEAR_BIT(iBoolsBitSet, biL_StaggeredAllPlayersFinished)
				#IF IS_DEBUG_BUILD
					IF bWdDoStaggeredDebug
						NET_DW_PRINT_STRING_WITH_PLAYER_NAME("[ServStag] Clearing biL_StaggeredAllPlayersFinished because this player not finished ", player)
					ENDIF
				#ENDIF
			ENDIF
			
			IF IS_NET_PLAYER_OK(player, FALSE)
				IF NOT IS_BIT_SET(GlobalplayerBD_FM[iServerStaggeredLoopCount].iHoldUpTutBitset, biHT_CompletedHoldUpTut)
					
				ENDIF
			ENDIF
		ENDIF
		
		iServerStaggeredLoopCount++
		IF iServerStaggeredLoopCount = NUM_NETWORK_PLAYERS
			#IF IS_DEBUG_BUILD
				IF bWdDoStaggeredDebug
					NET_DW_PRINT("[ServStag] At End of server staggered loop")
				ENDIF
			#ENDIF
			IF IS_BIT_SET(iBoolsBitSet, biL_StaggeredAllPlayersFinished)
				SET_BIT(serverBD.iServerBitSet, biS_AllPlayersFinished)
				#IF IS_DEBUG_BUILD NET_DW_PRINT("[ServStag] Setting biS_AllPlayersFinished because all players finished") #ENDIF
			ENDIF
			SET_BIT(iBoolsBitSet, biL_ServerLoopedThroughEveryone)
			iServerStaggeredLoopCount = 0
		ENDIF
	ENDIF
ENDPROC	



PROC MAINTAIN_CAR_MOD_TUTORIAL_SERVER()
	
	
	
//	BOOL bFoundAPairing
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		
		
	ENDIF
ENDPROC


FUNC BOOL SHOULD_CAR_MOD_TUTORIAL_COMPLETE()
	IF IS_BIT_SET(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ModdedCar)
		#IF IS_DEBUG_BUILD NET_DW_PRINT("[SHOULD_CAR_MOD_TUTORIAL_COMPLETE] True because biP_ModdedCar") #ENDIF
		RETURN TRUE
	ENDIF
	#IF IS_DEBUG_BUILD 
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S) 
			IF NOT IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
			AND NOT IS_FM_MISSION_LAUNCH_IN_PROGRESS()
				SET_LOCAL_PLAYER_EXITED_MP_MOD_SHOP_FOR_TUTORIAL(TRUE)
				SET_MP_BOOL_CHARACTER_STAT(MP_STAT_FM_CMODTUT_DONE, TRUE)
//				SET_FREEMODE_PROLOGUE_PROFILE_SETTINGS(PSFP_FM_CMODTUT_DONE)
				NET_DW_PRINT("[SHOULD_CAR_MOD_TUTORIAL_COMPLETE] True because S-Passed")
			 	RETURN TRUE
			ENDIF
		ENDIF
	#ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL NEED_TO_GET_A_CAR()
	BOOL bNeedToGetAcar
	IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		bNeedToGetAcar = TRUE
	ELSE	
		IF NOT IS_THIS_MODEL_A_CAR(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())))
			bNeedToGetAcar = TRUE
		ENDIF
	ENDIF
	RETURN bNeedToGetAcar
ENDFUNC


FUNC SHOP_NAME_ENUM GET_CLOSEST_CARMOD(VECTOR vCoords, SHOP_TYPE_ENUM eType = SHOP_TYPE_CARMOD, INT iRange = -1)

	INT iCount
	FLOAT fCurrentDist
	FLOAT fShortDist = 999999.99
	SHOP_NAME_ENUM eShop = EMPTY_SHOP
	SHOP_NAME_ENUM carMod
	FOR iCount = 0 TO NUMBER_OF_SHOPS-1
		IF eType = SHOP_TYPE_EMPTY
		OR eType = GET_SHOP_TYPE_ENUM(INT_TO_ENUM(SHOP_NAME_ENUM, iCount))	
			carMod = INT_TO_ENUM(SHOP_NAME_ENUM, iCount)
			IF IS_CAR_MOD_SHOP_VALID(carMod)
		
				fCurrentDist = GET_DISTANCE_BETWEEN_COORDS(vCoords, GET_SHOP_COORDS(INT_TO_ENUM(SHOP_NAME_ENUM, iCount)))
				IF fCurrentDist < fShortDist
				AND (fCurrentDist <= iRange OR iRange = -1)
				
					fShortDist = fCurrentDist
					eShop = INT_TO_ENUM(SHOP_NAME_ENUM, iCount)
				
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN eShop
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_IN_ANY_MOD_SHOP()
	
	IF IS_PLAYER_IN_SHOP(CARMOD_SHOP_01_AP )// Car Mod - AMB1
	OR IS_PLAYER_IN_SHOP(CARMOD_SHOP_05_ID2)// Car Mod - AMB2
	OR IS_PLAYER_IN_SHOP(CARMOD_SHOP_06_BT1)// Car Mod - AMB3
	OR IS_PLAYER_IN_SHOP(CARMOD_SHOP_07_CS1)// Car Mod - AMB4
	OR IS_PLAYER_IN_SHOP(CARMOD_SHOP_08_CS6)// Car Mod - AMB5
	OR IS_PLAYER_IN_SHOP(CARMOD_SHOP_SUPERMOD)// Car Mod - AMB6
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_BROWSING_ANY_MOD_SHOP()	
	
	IF IS_PLAYER_BROWSING_ITEMS_IN_SHOP(CARMOD_SHOP_01_AP )// Car Mod - AMB1
	OR IS_PLAYER_BROWSING_ITEMS_IN_SHOP(CARMOD_SHOP_05_ID2)// Car Mod - AMB2
	OR IS_PLAYER_BROWSING_ITEMS_IN_SHOP(CARMOD_SHOP_06_BT1)// Car Mod - AMB3
	OR IS_PLAYER_BROWSING_ITEMS_IN_SHOP(CARMOD_SHOP_07_CS1)// Car Mod - AMB4
	OR IS_PLAYER_BROWSING_ITEMS_IN_SHOP(CARMOD_SHOP_08_CS6)// Car Mod - AMB5
	OR IS_PLAYER_BROWSING_ITEMS_IN_SHOP(CARMOD_SHOP_SUPERMOD)// Car Mod - AMB6
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_LOCAL_PLAYER_PURCHASED_ANY_ITEM_IN_ANY_MOD_SHOP()
	IF HAS_PLAYER_PURCHASED_ITEM_FROM_SHOP(CARMOD_SHOP_01_AP )// Car Mod - AMB1
	OR HAS_PLAYER_PURCHASED_ITEM_FROM_SHOP(CARMOD_SHOP_05_ID2)// Car Mod - AMB2
	OR HAS_PLAYER_PURCHASED_ITEM_FROM_SHOP(CARMOD_SHOP_06_BT1)// Car Mod - AMB3
	OR HAS_PLAYER_PURCHASED_ITEM_FROM_SHOP(CARMOD_SHOP_07_CS1)// Car Mod - AMB4
	OR HAS_PLAYER_PURCHASED_ITEM_FROM_SHOP(CARMOD_SHOP_08_CS6)// Car Mod - AMB5
	OR HAS_PLAYER_PURCHASED_ITEM_FROM_SHOP(CARMOD_SHOP_SUPERMOD)// Car Mod - AMB6
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ANY_CAR_MOD_SHOP_INTRO_RUNNING()
	IF IS_SHOP_INTRO_RUNNING(CARMOD_SHOP_01_AP )
	OR IS_SHOP_INTRO_RUNNING(CARMOD_SHOP_05_ID2)
	OR IS_SHOP_INTRO_RUNNING(CARMOD_SHOP_06_BT1)
	OR IS_SHOP_INTRO_RUNNING(CARMOD_SHOP_07_CS1)
	OR IS_SHOP_INTRO_RUNNING(CARMOD_SHOP_08_CS6)
	OR IS_SHOP_INTRO_RUNNING(CARMOD_SHOP_SUPERMOD)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC
FUNC BOOL IS_PLAYER_DRIVER_OF_VEHICLE(VEHICLE_INDEX veh)
	PED_INDEX ped
	IF IS_VEHICLE_DRIVEABLE(veh)
		ped = GET_PED_IN_VEHICLE_SEAT(veh)
		IF ped = PLAYER_PED_ID()
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC
FUNC BOOL IS_OK_TO_PRINT_HELP()
	IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
	OR IS_PED_INJURED(PLAYER_PED_ID())
	OR NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
	OR  IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
	OR IS_SCRIPT_HUD_DISPLAYING(HUDPART_MISSIONBRIEF)
	OR IS_MP_MISSION_DETAILS_OVERLAY_ON_DISPLAY()
	OR IS_SCRIPT_HUD_DISPLAYING(HUDPART_MISSIONBOX)
	OR IS_HELP_MESSAGE_BEING_DISPLAYED()
	OR IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
	OR IS_FM_MISSION_LAUNCH_IN_PROGRESS()
	OR IS_CUSTOM_MENU_ON_SCREEN() 
	OR IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
	OR IS_MISSION_SUMMARY_SCREEN_DISPLAYED()
	OR IS_PAUSE_MENU_ACTIVE()
		RETURN(FALSE)
	ENDIF
	
	
	RETURN(TRUE)
ENDFUNC

INT iCarModTutHelpProg
SCRIPT_TIMER timeCarMod
PROC DO_CAR_MOD_TUT_HELP_TEXT()
	IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) <> 0
		EXIT
	ENDIF
	
	IF IS_LOCAL_PLAYER_IN_ANY_MOD_SHOP()
		EXIT
	ENDIF
	SWITCH iCarModTutHelpProg
		CASE 0
		//	IF IS_BIT_SET(iBoolsBitSet, biL_ToldToGoToModshop)
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					PRINT_HELP("FM_IHELP_CAR") // Steal a vehicle to use as your own. This vehicle will be available to you in future Online sessions once you have fitted a 'Tracker' in Los Santos Customs.
					iCarModTutHelpProg++
					#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_CAR_MOD_TUT_HELP_TEXT] iCarModTutHelpProg = ", iCarModTutHelpProg) #ENDIF
						
				ENDIF
		//	ENDIF
		BREAK
		CASE 1	
			IF IS_BIT_SET(iBoolsBitSet, biL_ToldToGoToModshop)
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					PRINT_HELP("FM_IHELP_MOD") // Mod shops are shown in the city by ~BLIP_CAR_MOD_SHOP~.
					iCarModTutHelpProg++
					#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_CAR_MOD_TUT_HELP_TEXT] iCarModTutHelpProg = ", iCarModTutHelpProg) #ENDIF
						
				ENDIF
			ENDIF
		BREAK	 
		CASE 2
			//-- Add gps
			IF IS_BIT_SET(iBoolsBitSet, biL_ToldToGoToModshop)
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
							IF NOT HAS_NET_TIMER_STARTED(timeCarMod)
								START_NET_TIMER(timeCarMod)
								#IF IS_DEBUG_BUILD NET_DW_PRINT("[DO_CAR_MOD_TUT_HELP_TEXT] started timeCarMod") #ENDIF
							ELSE
								IF HAS_NET_TIMER_EXPIRED(timeCarMod, 1000)
									PRINT_HELP("FM_CMOD_GPS") // Use the Interaction Menu to set a quick GPS to a location. Press and hold ~INPUT_NEXT_CAMERA~ to bring up the Interaction Menu.
									iCarModTutHelpProg++
									#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_CAR_MOD_TUT_HELP_TEXT] iCarModTutHelpProg = ", iCarModTutHelpProg) #ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
					
			ENDIF
		BREAK
		CASE 3
			//-- Stolen vehicle
			IF IS_BIT_SET(iBoolsBitSet, biL_ToldToGoToModshop)
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					IF NOT HAS_NET_TIMER_STARTED(timeCarMod)
						START_NET_TIMER(timeCarMod)
						#IF IS_DEBUG_BUILD NET_DW_PRINT("[DO_CAR_MOD_TUT_HELP_TEXT] started timeCarMod") #ENDIF
					ELSE
						IF HAS_NET_TIMER_EXPIRED(timeCarMod, 1000)
							PRINT_HELP("FM_CMOD_STOL") // If the Cops spot you in a stolen vehicle you will gain a Wanted Level.
							iCarModTutHelpProg++
							#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_CAR_MOD_TUT_HELP_TEXT] iCarModTutHelpProg = ", iCarModTutHelpProg) #ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 4
			//-- Friend blip help
			IF IS_BIT_SET(iBoolsBitSet, biL_ToldToGoToModshop)
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					IF NOT HAS_NET_TIMER_STARTED(timeCarMod)
						START_NET_TIMER(timeCarMod)
						#IF IS_DEBUG_BUILD NET_DW_PRINT("[DO_CAR_MOD_TUT_HELP_TEXT] started timeCarMod") #ENDIF
					ELSE
						IF HAS_NET_TIMER_EXPIRED(timeCarMod, 5000)
							PRINT_HELP("FM_IHELP_BLP") // Other players are shown on the radar by ~BLIP_LEVEL~. If they are your friend it will have a blue outline.
							iCarModTutHelpProg++
					//		#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_CAR_MOD_TUT_HELP_TEXT] iCarModTutHelpProg = ", iCarModTutHelpProg) #ENDIF
						ENDIF
					ENDIF
				ENDIF	
			ENDIF
		BREAK
		
		CASE 5
			//-- Car app help
			IF IS_BIT_SET(iBoolsBitSet, biL_ToldToGoToModshop)
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				//	PRINT_HELP("FM_IHELP_CAP") // You can download the Los Santos Customs app to your personal mobile phone device or tablet to modify your car and buy customized license plates. Visit www.rockstargames.com/V for details.
					iCarModTutHelpProg++
				//	#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_CAR_MOD_TUT_HELP_TEXT] iCarModTutHelpProg = ", iCarModTutHelpProg) #ENDIF
					
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL HAS_PLAYER_PURCHASED_TRACKER()
	VEHICLE_INDEX veh
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		veh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		IF IS_VEHICLE_MY_PERSONAL_VEHICLE(veh)
		OR g_sShopSettings.bTutorialTrackerAddedToBasket
			RETURN TRUE
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
			NET_DW_PRINT("[HAS_PLAYER_PURCHASED_TRACKER] Player not in a vehicle!")
		#ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_PLAYER_PURCHASED_INSURANCE()
	IF g_sShopSettings.bTutorialInsuranceAddedToBasket
		RETURN TRUE
	ENDIF
	IF CURRENT_SAVED_VEHICLE_SLOT() >= 0
		IF IS_BIT_SET(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS, MP_SAVED_VEHICLE_INSURED)
			RETURN TRUE
		ENDIF 
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_PLAYER_RESPRAYED_VEHICLE()
	VEHICLE_INDEX veh
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		veh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		IF GET_IS_VEHICLE_SPRAYED(veh)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_PLAYER_PURCHSED_CARMOD_TUT_ITEMS()
	IF NOT HAS_PLAYER_PURCHASED_TRACKER()
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_PLAYER_PURCHASED_INSURANCE()
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_PLAYER_RESPRAYED_VEHICLE()
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC DO_CAR_MOD_COMPLETED_CLEANUP()
	IF shopClosestCarMod <> EMPTY_SHOP
		SET_CAR_MOD_BLIP_LONG_RANGE(shopClosestCarMod, FALSE)
		SET_GPS_ROUTE_TO_CAR_MOD(shopClosestCarMod, FALSE)
		shopClosestCarMod = EMPTY_SHOP
	ENDIF
	SET_BIT(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ModdedCar)
	SET_BIT(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_FinishedCarModTut)
	SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_CAR_MOD_TUT, FALSE)
	SET_LOCAL_PLAYER_DOING_MOD_SHOP_TUTORIAL(FALSE)
	SET_FM_UNLOCKS_BIT_SET()
	Clear_Any_Objective_Text_From_This_Script()
	SET_SHOP_IS_AVAILABLE(CARMOD_SHOP_08_CS6, 		TRUE, FALSE)
	SET_SHOP_IS_AVAILABLE(CARMOD_SHOP_07_CS1, TRUE, FALSE)
	//#IF FEATURE_LOWRIDER_CONTENT
	//SET_SHOP_IS_AVAILABLE(CARMOD_SHOP_SUPERMOD, 		TRUE, FALSE)
	//#ENDIF
ENDPROC
PROC PROCESS_CAR_MOD_TUTORIAL_CLIENT()	
	VEHICLE_INDEX vehPlayer
	
	#IF IS_DEBUG_BUILD
		IF bDoDebug
			NET_DW_PRINT("Hit 34")
		ENDIF
	#ENDIF
	SWITCH PlayerBD[PARTICIPANT_ID_TO_INT()].iCarModTutProg
		CASE 0
			#IF IS_DEBUG_BUILD
				IF bDoDebug
					NET_DW_PRINT("Hit 76")
				ENDIF
			#ENDIF
	
			IF NOT IS_BIT_SET(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ModdedCar)
				#IF IS_DEBUG_BUILD
					IF bDoDebug
						NET_DW_PRINT("Hit 77")
					ENDIF
				#ENDIF
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					
				//	IF NEED_TO_GET_A_CAR()
					IF NOT IS_BIT_SET(iBoolsBitSet, biL_TextCleared)
						
						IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
							IF IS_BIT_SET(iBoolsBitSet, biL_ToldToGoToModshop)
								CLEAR_BIT(iBoolsBitSet, biL_LoseCops)
							ENDIF
							IF NOT IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
								IF IS_BIT_SET(iBoolsBitSet, biL_ToldToGoToModshop)
									CLEAR_BIT(iBoolsBitSet, biL_ToldToGoToModshop)
								ENDIF
								IF IS_BIT_SET(iBoolsBitSet, biL_ToldToFindAnother)
									CLEAR_BIT(iBoolsBitSet, biL_ToldToFindAnother)
								ENDIF
								
								IF IS_BIT_SET(iBoolsBitSet, biL_DoneValidCarHelp)
									CLEAR_BIT(iBoolsBitSet, biL_DoneValidCarHelp)
								ENDIF
								IF IS_BIT_SET(iBoolsBitSet, biL_PremiumCar)
									CLEAR_BIT(iBoolsBitSet, biL_PremiumCar)
								ENDIF
								IF NOT IS_BIT_SET(iBoolsBitSet, biL_ToldToGetCar)
									IF shopClosestCarMod <> EMPTY_SHOP
										SET_CAR_MOD_BLIP_LONG_RANGE(shopClosestCarMod, FALSE)
										SET_GPS_ROUTE_TO_CAR_MOD(shopClosestCarMod, FALSE)
										shopClosestCarMod = EMPTY_SHOP
									ENDIF
									CLEAR_BIT(iBoolsBitSet, biL_ToldToGoToModshop)
									Print_Objective_Text("FM_CTUT_CAR") // Get in a car.
									SET_BIT(iBoolsBitSet, biL_ToldToGetCar)
									#IF IS_DEBUG_BUILD NET_DW_PRINT("Doing get a car objective text") #ENDIF
								ENDIF
							ELSE
								//-- Player in a vehicle
								IF IS_BIT_SET(iBoolsBitSet, biL_ToldToGetCar)
									CLEAR_BIT(iBoolsBitSet, biL_ToldToGetCar)
									#IF IS_DEBUG_BUILD NET_DW_PRINT("Clearing biL_ToldToGetCar as I don't need a car") #ENDIF
								ENDIF
								
								vehPlayer = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
								VEHICLE_MODEL_FAIL_ENUM failReason
								
								IF NOT IS_VEHICLE_SAFE_FOR_MOD_SHOP(vehPlayer, FALSE, failReason)
								OR NOT IS_PLAYER_DRIVER_OF_VEHICLE(vehPlayer)
								OR IS_MP_PREMIUM_VEHICLE(GET_ENTITY_MODEL(vehPlayer))
									IF IS_LOCAL_PLAYER_IN_VALID_CAR_FOR_MP_MODSHOP_TUTORIAL()
										SET_LOCAL_PLAYER_IN_VALID_CAR_FOR_MP_MODSHOP_TUTORIAL(FALSE)
									ENDIF
									
									IF IS_BIT_SET(iBoolsBitSet, biL_ToldToGoToModshop)
										CLEAR_BIT(iBoolsBitSet, biL_ToldToGoToModshop)
									ENDIF
									IF NOT IS_BIT_SET(iBoolsBitSet, biL_ToldToFindAnother)
										Print_Objective_Text("FM_CTUT_ANO") // Find another vehicle
										SET_BIT(iBoolsBitSet, biL_ToldToFindAnother)	
										#IF IS_DEBUG_BUILD NET_DW_PRINT("Told player to find another vehicle") #ENDIF
									ENDIF
									IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneValidCarHelp)
										IF IS_OK_TO_PRINT_HELP()
											IF IS_VEHICLE_DRIVEABLE(vehPlayer)
												IF iCarModTutHelpProg > 0 // Wait for steal a vehicle help
													IF IS_MP_PREMIUM_VEHICLE(GET_ENTITY_MODEL(vehPlayer))
														PRINT_HELP("FM_CTUT_PRM") // You can only apply a tracker to this vehicle if it was purchased online. Find another vehicle
														SET_BIT(iBoolsBitSet, biL_DoneValidCarHelp)
														SET_BIT(iBoolsBitSet, biL_PremiumCar)
														START_NET_TIMER(timePremiumCar)
														#IF IS_DEBUG_BUILD NET_DW_PRINT("Doing premium model help") #ENDIF
													ELIF NOT IS_PLAYER_DRIVER_OF_VEHICLE(vehPlayer)
														SET_BIT(iBoolsBitSet, biL_DoneValidCarHelp)
														PRINT_HELP("FM_CTUT_DRI") // Only the driver can modify a vehicle.
														#IF IS_DEBUG_BUILD NET_DW_PRINT("Doing not driver of vehicle help") #ENDIF
													ELIF NOT IS_VEHICLE_SAFE_FOR_MOD_SHOP(vehPlayer, FALSE, failReason)	
														
														SET_BIT(iBoolsBitSet, biL_DoneValidCarHelp)
														PRINT_HELP("FM_CTUT_HMD") // Not all vehicles can be modified
														#IF IS_DEBUG_BUILD NET_DW_PRINT("Doing invalid vehicle help") #ENDIF
													
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ELSE
										IF IS_BIT_SET(iBoolsBitSet, biL_PremiumCar)
											IF HAS_NET_TIMER_EXPIRED(timePremiumCar,30000)
												IF IS_MP_PREMIUM_VEHICLE(GET_ENTITY_MODEL(vehPlayer))
													
													IF IS_OK_TO_PRINT_HELP()
														#IF IS_DEBUG_BUILD NET_DW_PRINT("Redisplaying premium car help") #ENDIF
														PRINT_HELP("FM_CTUT_PRM") // You can only apply a tracker to this vehicle if it was purchased online. Find another vehicle
														RESET_NET_TIMER(timePremiumCar)
														START_NET_TIMER(timePremiumCar)
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ELSE
									//-- Car can be modded
									
									IF IS_BIT_SET(iBoolsBitSet, biL_ToldToFindAnother)
										CLEAR_BIT(iBoolsBitSet, biL_ToldToFindAnother)
									ENDIF
									
									IF NOT IS_LOCAL_PLAYER_IN_VALID_CAR_FOR_MP_MODSHOP_TUTORIAL()
										SET_LOCAL_PLAYER_IN_VALID_CAR_FOR_MP_MODSHOP_TUTORIAL(TRUE)
									ENDIF
									
									IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM_CTUT_HMD")
										CLEAR_HELP()
										#IF IS_DEBUG_BUILD NET_DW_PRINT("Cleared FM_CTUT_HMD help as I'm in a valid vehicle") #ENDIF
									ENDIF
									
									IF NOT IS_LOCAL_PLAYER_IN_ANY_MOD_SHOP()
									OR (IS_LOCAL_PLAYER_IN_ANY_MOD_SHOP() AND IS_PLAYER_CONTROL_ON(PLAYER_ID()))
										IF NOT IS_BIT_SET(iBoolsBitSet, biL_ToldToGoToModshop)
											Print_Objective_Text("FM_CTUT_MOD") // Get to a Mod shop.
											SET_BIT(iBoolsBitSet, biL_ToldToGoToModshop)
											RESET_NET_TIMER(timeSuitableCar)
											START_NET_TIMER(timeSuitableCar)
											
											IF NOT IS_PLAYER_DOING_MP_MOD_SHOP_TUTORIAL(PLAYER_ID())
												 SET_LOCAL_PLAYER_DOING_MOD_SHOP_TUTORIAL(TRUE)
											ENDIF
											#IF IS_DEBUG_BUILD 
												NET_DW_PRINT("Doing get to mod shop") 
												NET_DW_PRINT("Started timeSuitableCar")
											#ENDIF
											
											IF shopClosestCarMod = EMPTY_SHOP
												shopClosestCarMod = GET_CLOSEST_CARMOD(GET_ENTITY_COORDS(PLAYER_PED_ID()))
												IF shopClosestCarMod <> EMPTY_SHOP

													#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("Setting mod shop blip long range.. ", ENUM_TO_INT(shopClosestCarMod)) #ENDIF
													SET_CAR_MOD_BLIP_LONG_RANGE(shopClosestCarMod)
													SET_GPS_ROUTE_TO_CAR_MOD(shopClosestCarMod)
													

												ENDIF
												
											ENDIF
										ENDIF
									ELSE
										IF (IS_LOCAL_PLAYER_IN_ANY_MOD_SHOP() AND NOT IS_PLAYER_CONTROL_ON(PLAYER_ID()))
											IF NOT IS_BIT_SET(iBoolsBitSet, biL_ToldToGoToModshop)
												SET_BIT(iBoolsBitSet, biL_ToldToGoToModshop)
												#IF IS_DEBUG_BUILD 
													NET_DW_PRINT("Set biL_ToldToGoToModshop - already in mod shop") 
													
												#ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
							ENDIF
							
							
							//-- Help text about being able to use cars in races
							IF NOT IS_BIT_SET(iBoolsBitSet, biL_UseCarsHelp)
								IF IS_BIT_SET(iBoolsBitSet, biL_ToldToGoToModshop)
									IF HAS_NET_TIMER_EXPIRED(timeSuitableCar, 10000)
										IF NOT IS_PAUSE_MENU_ACTIVE()
											IF NOT IS_TRANSITION_ACTIVE()
												IF IS_OK_TO_PRINT_HELP()
												//	PRINT_HELP("FM_CTUT_USE")
													
														
													//-- Now done in MAINTAIN_AFTER_CAR_MOD_TUT_HELP
														
													SET_BIT(iBoolsBitSet, biL_UseCarsHelp)
												//	#IF IS_DEBUG_BUILD NET_DW_PRINT("Done using cars in races help") #ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ELSE
							// Got a wanted level
							IF NOT IS_BIT_SET(iBoolsBitSet, biL_LoseCops)
								IF NOT IS_BIT_SET(iBoolsBitSet, biL_BrowsingSHop)
									#IF IS_DEBUG_BUILD NET_DW_PRINT("Doing lose cops text") #ENDIF
									Print_Objective_Text("FM_IHELP_LCP") // lose the cops
									
									
									IF shopClosestCarMod <> EMPTY_SHOP
										SET_CAR_MOD_BLIP_LONG_RANGE(shopClosestCarMod, FALSE)
										SET_GPS_ROUTE_TO_CAR_MOD(shopClosestCarMod, FALSE)
										shopClosestCarMod = EMPTY_SHOP
									ENDIF
									
									CLEAR_BIT(iBoolsBitSet, biL_ToldToGoToModshop)
									CLEAR_BIT(iBoolsBitSet, biL_ToldToGetCar)
									CLEAR_BIT(iBoolsBitSet, biL_ToldToFindAnother)
									CLEAR_BIT(iBoolsBitSet, biL_DoneValidCarHelp)
									SET_BIT(iBoolsBitSet, biL_LoseCops)
								ELSE
									#IF IS_DEBUG_BUILD
										IF bDoDebug
											NET_DW_PRINT("Hit 79")
										ENDIF
									#ENDIF
								ENDIF
							ELSE
								#IF IS_DEBUG_BUILD
									IF bDoDebug
										NET_DW_PRINT("Hit 78")
									ENDIF
								#ENDIF
							ENDIF
							
							IF IS_LOCAL_PLAYER_IN_VALID_CAR_FOR_MP_MODSHOP_TUTORIAL()
								IF NOT IS_PED_INJURED(PLAYER_PED_ID())
									IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
										vehPlayer = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
										VEHICLE_MODEL_FAIL_ENUM failReason
										
										IF NOT IS_VEHICLE_SAFE_FOR_MOD_SHOP(vehPlayer, FALSE, failReason)
										OR NOT IS_PLAYER_DRIVER_OF_VEHICLE(vehPlayer)
										OR IS_MP_PREMIUM_VEHICLE(GET_ENTITY_MODEL(vehPlayer))
											#IF IS_DEBUG_BUILD NET_DW_PRINT("Not in valid car - with wanted level") #ENDIF
											SET_LOCAL_PLAYER_IN_VALID_CAR_FOR_MP_MODSHOP_TUTORIAL(FALSE)
										ENDIF
									ELSE
										#IF IS_DEBUG_BUILD NET_DW_PRINT("Not in valid car - with wanted level - 2") #ENDIF
										SET_LOCAL_PLAYER_IN_VALID_CAR_FOR_MP_MODSHOP_TUTORIAL(FALSE)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					//-- Check for car being resprayed
					IF IS_BIT_SET(iBoolsBitSet, biL_ToldToGoToModshop)
					OR ( IS_LOCAL_PLAYER_IN_ANY_MOD_SHOP() AND IS_BIT_SET(iBoolsBitSet, biL_LoseCops))						
					
						#IF IS_DEBUG_BUILD
							IF bDoDebug
								NET_DW_PRINT("Hit 1")
							ENDIF
						#ENDIF
						IF IS_LOCAL_PLAYER_IN_ANY_MOD_SHOP()	
					//	AND NOT HAS_PLAYER_EXITIED_MOD_SHOP_FOR_TUTORIAL(PLAYER_ID())
							
							#IF IS_DEBUG_BUILD
								IF bDoDebug
									NET_DW_PRINT("Hit 5")
								ENDIF
							#ENDIF
							
							IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM_IHELP_CAR")
							OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM_IHELP_MOD")
							OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM_CMOD_GPS")
							OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM_CMOD_STOL")
								CLEAR_HELP()
							ENDIF
							
						
//							IF IS_BIT_SET(iBoolsBitSet, biL_LoseCops)
//								CLEAR_BIT(iBoolsBitSet, biL_LoseCops)
//								
//							ENDIF
							IF Is_This_The_Current_Objective_Text("FM_IHELP_LCP")
								Clear_Any_Objective_Text_From_This_Script()
								#IF IS_DEBUG_BUILD NET_DW_PRINT("Clearing lose cops objective") #ENDIF
							ENDIF
							IF NOT IS_BIT_SET(iBoolsBitSet, biL_BrowsingSHop)
								IF IS_LOCAL_PLAYER_BROWSING_ANY_MOD_SHOP()
									SET_BIT(iBoolsBitSet, biL_BrowsingSHop)
									SET_BIT(MPGlobalsAmbience.iFmNmhBitSet4, BI_FM_NMH4_CARMOD_PLYR_CTRL)
									#IF IS_DEBUG_BUILD NET_DW_PRINT("Set BI_FM_NMH4_CARMOD_PLYR_CTRL - 1") #ENDIF
									IF Is_This_The_Current_Objective_Text("FM_CTUT_MOD")
										Clear_Any_Objective_Text_From_This_Script()
									ENDIF
									#IF IS_DEBUG_BUILD NET_DW_PRINT("Setting biL_BrowsingSHop as I'm browsing shop") #ENDIF
								ENDIF
							ENDIF
							
							IF HAS_PLAYER_PURCHSED_CARMOD_TUT_ITEMS()
							//	SET_BIT(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ModdedCar)
								IF NOT IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet4, BI_FM_NMH4_CARMOD_PLYR_CTRL)
									SET_BIT(MPGlobalsAmbience.iFmNmhBitSet4, BI_FM_NMH4_CARMOD_PLYR_CTRL)
									#IF IS_DEBUG_BUILD 
										NET_DW_PRINT("Set BI_FM_NMH4_CARMOD_PLYR_CTRL - 3") 
									#ENDIF
								ENDIF
								
								//-- Used in carmod_shop.sc to setup warping to the cutscene
								IF NOT HAS_PLAYER_EXITIED_MOD_SHOP_FOR_TUTORIAL(PLAYER_ID())
									IF NOT HAS_PLAYER_PURCHASED_ALL_MOD_SHOP_TUTORIAL_ITEMS(PLAYER_ID())
										SET_LOCAL_PLAYER_PURCHASED_ALL_MP_MOD_SHOP_TUTORIAL_ITEMS(TRUE)
										
										bLocalPurchasedAllItems = TRUE
																
																		
										SET_FM_UNLOCKS_BIT_SET()
									ENDIF
								ENDIF
								IF Is_This_The_Current_Objective_Text("FM_CTUT_RSP")
								OR Is_This_The_Current_Objective_Text("FM_IHELP_PTRK")
								OR Is_This_The_Current_Objective_Text("FM_IHELP_PINS")
									Clear_Any_Objective_Text_From_This_Script()
								ENDIF
//								IF NOT Is_This_The_Current_Objective_Text("FM_CTUT_LLS")
//									Print_Objective_Text("FM_CTUT_LLS") // Leave Los Santos Customs.
//									#IF IS_DEBUG_BUILD NET_DW_PRINT("Doing leave garage objective") #ENDIF
//								ENDIF
							ELSE
								IF IS_BIT_SET(iBoolsBitSet, biL_BrowsingSHop)
									IF HAS_PLAYER_COMPLETED_MOD_SHOP_TUTORIAL_HELP(PLAYER_ID())
									OR (NOT IS_ANY_CAR_MOD_SHOP_INTRO_RUNNING() AND IS_CUSTOM_MENU_ON_SCREEN())
										INT iRepairCost
										iRepairCost = GET_CARMOD_REPAIR_COST(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
										IF iRepairCost > 0
											IF NOT Is_This_The_Current_Objective_Text("FM_CTUT_REP")
												Print_Objective_Text("FM_CTUT_REP") // Get ypur vehicle repaired
												#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("Doing repair objective - 1 because repair cost is", iRepairCost) #ENDIF
											ENDIF
										ELIF NOT HAS_PLAYER_RESPRAYED_VEHICLE()
											IF NOT Is_This_The_Current_Objective_Text("FM_CTUT_RSP")
												Print_Objective_Text("FM_CTUT_RSP") // Get ypur vehicle resprayed
												#IF IS_DEBUG_BUILD NET_DW_PRINT("Doing respray objective - 2") #ENDIF
											ENDIF
										ELIF NOT HAS_PLAYER_PURCHASED_TRACKER()
											IF NOT Is_This_The_Current_Objective_Text("FM_IHELP_PTRK")
												Print_Objective_Text("FM_IHELP_PTRK") // Purchase a tracker for your vehicle
												#IF IS_DEBUG_BUILD NET_DW_PRINT("Doing purchase tracker objective") #ENDIF
											ENDIF
										ELIF NOT HAS_PLAYER_PURCHASED_INSURANCE()
											IF NOT Is_This_The_Current_Objective_Text("FM_IHELP_PINS")
												Print_Objective_Text("FM_IHELP_PINS") // Purchase insurance for your vehicle
												#IF IS_DEBUG_BUILD NET_DW_PRINT("Doing purchase insurance objective") #ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
//							IF GET_VEHICLE_UPGRADE_LEVEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) > 0
//							AND NOT IS_LOCAL_PLAYER_BROWSING_ANY_MOD_SHOP()
//								
//								SET_BIT(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ModdedCar)
//								SET_BIT(MPGlobalsAmbience.iFmNmhBitSet4, BI_FM_NMH4_CARMOD_PLYR_CTRL)
//								#IF IS_DEBUG_BUILD NET_DW_PRINT("Set BI_FM_NMH4_CARMOD_PLYR_CTRL - 2") #ENDIF
//								#IF IS_DEBUG_BUILD NET_DW_PRINT("Finishing tut as I've modded my car") #ENDIF
//							ELIF (IS_BIT_SET(iBoolsBitSet, biL_BrowsingSHop)
//							AND NOT IS_LOCAL_PLAYER_BROWSING_ANY_MOD_SHOP())
//
//								SET_BIT(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ModdedCar)
//								
//								#IF IS_DEBUG_BUILD NET_DW_PRINT("Finishing tut as I've stopped browsing mod shop") #ENDIF 
//							ENDIF
						ELSE
							#IF IS_DEBUG_BUILD
								IF bDoDebug
									NET_DW_PRINT("Hit 2")
								ENDIF
							#ENDIF
							IF IS_BIT_SET(iBoolsBitSet, biL_BrowsingSHop)
								#IF IS_DEBUG_BUILD
									IF bDoDebug
										NET_DW_PRINT("Hit 4")
									ENDIF
								#ENDIF
						
								IF NOT HAS_PLAYER_PURCHSED_CARMOD_TUT_ITEMS()
									IF IS_SCREEN_FADED_IN()
										//-- Haven't purchased items, go back to mid shop
										CLEAR_BIT(iBoolsBitSet, biL_BrowsingSHop)
										CLEAR_BIT(iBoolsBitSet, biL_ToldToGoToModshop)
										CLEAR_BIT(iBoolsBitSet, biL_LoseCops)
										Clear_Any_Objective_Text_From_This_Script()
										shopClosestCarMod = EMPTY_SHOP
										NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
										#IF IS_DEBUG_BUILD
											NET_DW_PRINT("Player didn't buy items, go back to mod shop....")
										#ENDIF
									ENDIF
								ELSE
									IF NOT IS_BIT_SET(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ModdedCar)
								//	OR HAS_PLAYER_EXITIED_MOD_SHOP_FOR_TUTORIAL(PLAYER_ID())
										SET_BIT(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ModdedCar)
										#IF IS_DEBUG_BUILD NET_DW_PRINT("Finishing tut as I've stopped browsing mod shop - 2") #ENDIF
									ENDIF
								ENDIF
							ELSE
								#IF IS_DEBUG_BUILD
									IF bDoDebug
										NET_DW_PRINT("Hit 3")
									ENDIF
								#ENDIF
							ENDIF
						ENDIF
					ELSE
					
						IF IS_BIT_SET(iBoolsBitSet, biL_BrowsingSHop)
							CLEAR_BIT(iBoolsBitSet, biL_BrowsingSHop)
							CLEAR_BIT(iBoolsBitSet, biL_ToldToGoToModshop)
							CLEAR_BIT(iBoolsBitSet, biL_LoseCops)
							Clear_Any_Objective_Text_From_This_Script()
							shopClosestCarMod = EMPTY_SHOP
							#IF IS_DEBUG_BUILD
								NET_DW_PRINT("Clearing biL_BrowsingSHop - 1")
							#ENDIF
						ENDIF
						
						#IF IS_DEBUG_BUILD
							IF bDoDebug
								NET_DW_PRINT("Hit 92")
							ENDIF
						#ENDIF
					ENDIF
					
					
					//-- Check for player control being off
					IF NOT IS_BIT_SET(iBoolsBitSet, biL_TextCleared)
						IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
						OR IS_FM_MISSION_LAUNCH_IN_PROGRESS()
						OR GET_LOCAL_PLAYER_CORONA_POS_STATE() > 0
						OR NOT IS_SKYSWOOP_AT_GROUND()
						AND NOT IS_LOCAL_PLAYER_IN_ANY_MOD_SHOP()
							SET_BIT(iBoolsBitSet, biL_TextCleared)
							Clear_Any_Objective_Text_From_This_Script()
								
							#IF IS_DEBUG_BUILD NET_DW_PRINT("Setting biL_TextCleared as player on mission / in corona") #ENDIF
						ELSE	
							
						ENDIF
					ELSE
						IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
						OR IS_LOCAL_PLAYER_IN_ANY_MOD_SHOP()
							IF IS_SCREEN_FADED_IN()
								IF NOT IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
								AND NOT IS_FM_MISSION_LAUNCH_IN_PROGRESS()
								AND GET_LOCAL_PLAYER_CORONA_POS_STATE() = 0
								AND IS_SKYSWOOP_AT_GROUND()
								OR IS_LOCAL_PLAYER_IN_ANY_MOD_SHOP()
									IF NOT HAS_NET_TIMER_STARTED(timeSinceOnMission)
										START_NET_TIMER(timeSinceOnMission)
										#IF IS_DEBUG_BUILD NET_DW_PRINT("Started timeSinceOnMission ") #ENDIF
									ELSE
										IF HAS_NET_TIMER_EXPIRED(timeSinceOnMission, 7000)
										OR IS_LOCAL_PLAYER_IN_ANY_MOD_SHOP()
											CLEAR_BIT(iBoolsBitSet, biL_TextCleared)
											CLEAR_BIT(iBoolsBitSet, biL_ToldToGetCar)
											CLEAR_BIT(iBoolsBitSet, biL_ToldToGoToModshop)
											CLEAR_BIT(iBoolsBitSet, biL_ToldToFindAnother)
											CLEAR_BIT(iBoolsBitSet, biL_LoseCops)
											RESET_NET_TIMER(timeSinceOnMission)
											IF shopClosestCarMod <> EMPTY_SHOP
												SET_CAR_MOD_BLIP_LONG_RANGE(shopClosestCarMod, FALSE)
												SET_GPS_ROUTE_TO_CAR_MOD(shopClosestCarMod, FALSE)
												shopClosestCarMod = EMPTY_SHOP
											ENDIF
											
											#IF IS_DEBUG_BUILD NET_DW_PRINT("Clearing biL_TextCleared as player control is on") #ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					/*	IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
							IF NOT IS_LOCAL_PLAYER_IN_ANY_MOD_SHOP()
								SET_BIT(iBoolsBitSet, biL_TextCleared)
								Clear_Any_Objective_Text_From_This_Script()
								
								#IF IS_DEBUG_BUILD NET_DW_PRINT("Setting biL_TextCleared as player control is off") #ENDIF
							ENDIF
						ELSE	
							IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
							OR IS_FM_MISSION_LAUNCH_IN_PROGRESS()
								
									SET_BIT(iBoolsBitSet, biL_TextCleared)
									Clear_Any_Objective_Text_From_This_Script()
							
									#IF IS_DEBUG_BUILD NET_DW_PRINT("Setting biL_TextCleared as player on mission / launch in progress") #ENDIF
								ELSE
								
							ENDIF
						ENDIF
					ELSE
						IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
							IF IS_SCREEN_FADED_IN()
								IF NOT IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
								AND NOT IS_FM_MISSION_LAUNCH_IN_PROGRESS()
									IF NOT HAS_NET_TIMER_STARTED(timeSinceOnMission)
										START_NET_TIMER(timeSinceOnMission)
										#IF IS_DEBUG_BUILD NET_DW_PRINT("Started timeSinceOnMission ") #ENDIF
									ELSE
										IF HAS_NET_TIMER_EXPIRED(timeSinceOnMission, 7000)
											CLEAR_BIT(iBoolsBitSet, biL_TextCleared)
											CLEAR_BIT(iBoolsBitSet, biL_ToldToGetCar)
											CLEAR_BIT(iBoolsBitSet, biL_ToldToGoToModshop)
											CLEAR_BIT(iBoolsBitSet, biL_ToldToFindAnother)
											RESET_NET_TIMER(timeSinceOnMission)
											IF shopClosestCarMod <> EMPTY_SHOP
												SET_CAR_MOD_BLIP_LONG_RANGE(shopClosestCarMod, FALSE)
												shopClosestCarMod = EMPTY_SHOP
											ENDIF
											
											#IF IS_DEBUG_BUILD NET_DW_PRINT("Clearing biL_TextCleared as player control is on") #ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF */
					
					//-- Check for player being on a mission
				//	IF NOT IS_BIT_SET(iBoolsBitSet, biL_OnMission)
				//		IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
				//	ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	
	IF NOT IS_BIT_SET(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_FinishedCarModTut)
		IF SHOULD_CAR_MOD_TUTORIAL_COMPLETE()
			DO_CAR_MOD_COMPLETED_CLEANUP()
//			IF shopClosestCarMod <> EMPTY_SHOP
//				SET_CAR_MOD_BLIP_LONG_RANGE(shopClosestCarMod, FALSE)
//				SET_GPS_ROUTE_TO_CAR_MOD(shopClosestCarMod, FALSE)
//				shopClosestCarMod = EMPTY_SHOP
//			ENDIF
//			SET_BIT(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ModdedCar)
//			SET_BIT(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_FinishedCarModTut)
//			SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_CAR_MOD_TUT, FALSE)
//			SET_LOCAL_PLAYER_DOING_MOD_SHOP_TUTORIAL(FALSE)
//		//	SET_MP_BOOL_CHARACTER_STAT(MP_STAT_FM_CMODTUT_DONE, TRUE)
//			SET_FM_UNLOCKS_BIT_SET()
//		//	REQUEST_SAVE(STAT_SAVETYPE_PROLOGUE)
//			Clear_Any_Objective_Text_From_This_Script()
//			SET_SHOP_IS_AVAILABLE(CARMOD_SHOP_08_CS6, 		TRUE, FALSE)
//			SET_SHOP_IS_AVAILABLE(CARMOD_SHOP_07_CS1, TRUE, FALSE)
		ELSE	
			DO_CAR_MOD_TUT_HELP_TEXT()
		ENDIF
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
	PROC DO_CAR_MOD_TUT_J_SKIPS()
		
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
				IF IS_NET_PLAYER_OK(PLAYER_ID())
				/*	NET_DW_PRINT_STRING_INT("Player ok for j-skip, hold up stage... ", holdUpTut.iHoldUpTutProg)
					SWITCH holdUpTut.iHoldUpTutProg
						CASE HOLD_UP_TUT_STAGE_GET_TO_HOLD_UP
							iMyMeetLocLoc = serverBD.holdUpTutServer.iHoldUpToUse[NATIVE_TO_INT(PLAYER_ID())]
							IF iMyMeetLocLoc > -1
								J_SKIP_MP(GET_HOLD_UP_MEET_LOCATION(iMyMeetLocLoc), <<2.0, 2.0, 2.0>>)
							ENDIF
							//J_SKIP_MP
						BREAK
					ENDSWITCH */
				ENDIF
			ENDIF
		
	ENDPROC
#ENDIF



#IF IS_DEBUG_BUILD
	FUNC PLAYER_INDEX GET_CARMOD_PLAYER_PED()
		PLAYER_INDEX playerNearest = INVALID_PLAYER_INDEX()
		PLAYER_INDEX player
		PARTICIPANT_INDEX part
		INT i
		FOR i = 0 TO NUM_NETWORK_PLAYERS - 1
			part = INT_TO_PARTICIPANTINDEX(i)
			IF playerNearest = INVALID_PLAYER_INDEX()
				IF i <> PARTICIPANT_ID_TO_INT()
					IF NETWORK_IS_PARTICIPANT_ACTIVE(part)
						player = NETWORK_GET_PLAYER_INDEX(part)
						IF IS_NET_PLAYER_OK(player) 
							playerNearest = player
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
		RETURN playerNearest
	ENDFUNC
	
	
	//pedIndex pedSyncScene
	PLAYER_INDEX playerSyncScene
	PROC DO_SYNC_SCENE_TEST()
		VECTOR vTemp
		STRING sAnimDict = "mp_ped_interaction"
		MODEL_NAMES mSyncPed = MP_M_FREEMODE_01
		//ENTITY_INDEX ent
		
		//FLOAT fTemp
		SWITCH iWdSyncSceneTestProg
			CASE 0
				REQUEST_ANIM_DICT(sAnimDict)
				REQUEST_MODEL(mSyncPed)
				IF HAS_ANIM_DICT_LOADED(sAnimDict)
				AND HAS_MODEL_LOADED(mSyncPed)
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())	
//						RESERVE_NETWORK_MISSION_PEDS(1)
//						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.niPedSync)
//							ent = NET_TO_PED(serverbd.niPedSync)
//							DELETE_ENTITY(ent)
//						ENDIF
//						vTemp = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<0.0, 5.0, 0.0>>)
//						CREATE_NET_PED(serverbd.niPedSync, PEDTYPE_MISSION,mSyncPed, vTemp, 0.0)  
//						// CREATE_PED(PEDTYPE_MISSION, GET_ENTITY_MODEL(PLAYER_PED_ID()), vTemp, 0.0, FALSE, FALSE)
//						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NET_TO_PED(serverbd.niPedSync), TRUE)
						
						
						vTemp = GET_ENTITY_COORDS(PLAYER_PED_ID())
						playerSyncScene = GET_CARMOD_PLAYER_PED()
						IF playerSyncScene <> INVALID_PLAYER_INDEX()
							serverBD.iSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(vTemp, <<0.0, 0.0, 0.0>>, EULER_YXZ, TRUE)
							NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), serverBD.iSceneID, "mp_ped_interaction", "HIGHFIVE_guy_a", SLOW_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_PRESERVE_VELOCITY | SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE, RBF_PLAYER_IMPACT)
							NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(GET_PLAYER_PED(playerSyncScene), serverBD.iSceneID, "mp_ped_interaction", "HIGHFIVE_guy_b", SLOW_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_PRESERVE_VELOCITY | SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE, RBF_PLAYER_IMPACT)
							NETWORK_ADD_SYNCHRONISED_SCENE_CAMERA(serverBD.iSceneID, "mp_ped_interaction", "HIGHFIVE_cam")
							iWdSyncSceneTestProg++
							PRINTLN("[dsw] [DO_SYNC_SCENE_TEST] iWdSyncSceneTestProg = ", iWdSyncSceneTestProg)
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 1
				IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RIGHT)
				OR (IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY)
				AND IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY_SECONDARY)
				AND IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_AIMING_AT_PLAYER, playerSyncScene))
					
					NETWORK_START_SYNCHRONISED_SCENE(serverBD.iSceneID)
					iWdSyncSceneTestProg++
					PRINTLN("[dsw] [DO_SYNC_SCENE_TEST] iWdSyncSceneTestProg = ", iWdSyncSceneTestProg)
				ENDIF
			BREAK
		ENDSWITCH
		
	ENDPROC
	
	PROC RESET_SYNC_SCENE_TEST()
		iWdSyncSceneTestProg = 0
		NETWORK_STOP_SYNCHRONISED_SCENE(serverBD.iSceneID)
	ENDPROC
	
	PROC UPDATE_WIDGETS()
		VEHICLE_INDEX veh
		
		IF bWdUnlockModshops
			SET_CAR_MOD_GARAGES_FOR_FM(TRUE)
			bWdUnlockModshops = FALSE
		ENDIF
		
		IF bWdShowUpgrade
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				veh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())	
				fWdUpgradeLevel = GET_VEHICLE_UPGRADE_LEVEL(veh)
				fWdMaxUpgradeLevel = GET_MAX_VEHICLE_UPGRADE_LEVEL(veh)
			ENDIF
		ENDIF
		
		IF bWdModShopChecks
			bWdInANyModSHop = IS_LOCAL_PLAYER_IN_ANY_MOD_SHOP()
			bWdBrowsingShop = IS_LOCAL_PLAYER_BROWSING_ANY_MOD_SHOP()
		ENDIF 
		
		IF bWdPurchasedItems
			IF HAS_PLAYER_PURCHSED_CARMOD_TUT_ITEMS()
			ENDIF
		ENDIF
		
		IF bWdDoSyncSceneTest
			DO_SYNC_SCENE_TEST()
		ENDIF
		
		IF bWdResetSyncScene
			bWdResetSyncScene = FALSE
			bWdDoSyncSceneTest = FALSE
			RESET_SYNC_SCENE_TEST()
		ENDIF
	ENDPROC
#ENDIF
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                      Main Loop                          ///////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

SCRIPT(MP_MISSION_DATA missionScriptArgs)
	
	
	PROCESS_PRE_GAME(missionScriptArgs)	
	
	#IF IS_DEBUG_BUILD
		CREATE_WIDGETS()	
	#ENDIF
	
	
	
	WHILE TRUE
	
		MP_LOOP_WAIT_ZERO()
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
			SCRIPT_PROFILER_START_OF_FRAME()
		#ENDIF
		#ENDIF		
		
		

		// If we have a match end event, bail.
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
	//	OR NOT IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE)
			CLEANUP_SCRIPT()
		ENDIF	
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER( "Cleanup checks")
		#ENDIF
		#ENDIF 
		
		#IF IS_DEBUG_BUILD
			UPDATE_WIDGETS()
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER( "UPDATE_WIDGETS")
			#ENDIF
		#ENDIF
		
		
// *****************************************************************************************************************************************************************	
// *****************************************************************************************************************************************************************	
//												LOCAL PROCESSING
// *****************************************************************************************************************************************************************	
// *****************************************************************************************************************************************************************
		
		
		// -----------------------------------
		// Process your game logic.....
		SWITCH GET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT())
			
			// Wait untill the server gives the all go before moving on
			CASE GAME_STATE_INI
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_RUNNING
					#IF IS_DEBUG_BUILD NET_DW_PRINT("playerBD[PARTICIPANT_ID_TO_INT()].iGameState GAME_STATE_INI >  GAME_STATE_RUNNING") #ENDIF
					
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY
					
					#IF IS_DEBUG_BUILD NET_DW_PRINT("playerBD[PARTICIPANT_ID_TO_INT()].iGameState GAME_STATE_INI >  GAME_STATE_TERMINATE_DELAY") #ENDIF
					
				ENDIF
			BREAK
			
			// Main gameplay state.
			CASE GAME_STATE_RUNNING
				
				
				
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING	
					IF NOT HAS_FM_CAR_MOD_TUT_BEEN_DONE()
					#IF IS_DEBUG_BUILD OR g_bDebugLaunchCarModTut #ENDIF
						PROCESS_CAR_MOD_TUTORIAL_CLIENT()
					ELSE
						IF NOT IS_BIT_SET(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_FinishedCarModTut)
							#IF IS_DEBUG_BUILD 
								NET_DW_PRINT("Car mod complete stat, set, but biP_FinishedCarModTut not set!") 
							#ENDIF
							DO_CAR_MOD_COMPLETED_CLEANUP()
						ENDIF
					ENDIF
				
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY
					#IF IS_DEBUG_BUILD NET_DW_PRINT("playerBD[PARTICIPANT_ID_TO_INT()].iGameState GAME_STATE_RUNNING >  GAME_STATE_TERMINATE_DELAY") #ENDIF
				ENDIF
				
				#IF IS_DEBUG_BUILD
					IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
						playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
						#IF IS_DEBUG_BUILD NET_DW_PRINT("playerBD[PARTICIPANT_ID_TO_INT()].iGameState GAME_STATE_RUNNING > GAME_STATE_END as F-skipped ") #ENDIF
					
					ENDIF
				#ENDIF
				//-- Leave if we start a mission
		/*		IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
					#IF IS_DEBUG_BUILD NET_DW_PRINT("playerBD[PARTICIPANT_ID_TO_INT()].iGameState GAME_STATE_RUNNING > GAME_STATE_END as on mission ") #ENDIF
					
				ENDIF
		*/						
			BREAK
			
			CASE GAME_STATE_TERMINATE_DELAY 
				SET_MISSION_FINISHED(serverBD.timeTerminate)
		      	IF IS_MISSION_READY_TO_CLEANUP(serverBD.timeTerminate)
		            playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
					#IF IS_DEBUG_BUILD NET_DW_PRINT("playerBD[PARTICIPANT_ID_TO_INT()].iGameState GAME_STATE_TERMINATE_DELAY >  GAME_STATE_END") #ENDIF
		      	ENDIF
			BREAK
			
			// The game stage the local player is placed when we want him to leave a mission
			CASE GAME_STATE_LEAVE

				playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
				#IF IS_DEBUG_BUILD NET_DW_PRINT("playerBD[PARTICIPANT_ID_TO_INT()].iGameState GAME_STATE_LEAVE >  GAME_STATE_END") #ENDIF

			FALLTHRU
			
			//Cleans up then terminates the mission
			CASE GAME_STATE_END
				#IF IS_DEBUG_BUILD NET_DW_PRINT("About to run cleanup from GAME_STATE_END") #ENDIF
				CLEANUP_SCRIPT()
			BREAK

		ENDSWITCH
		
	 	

		
// *****************************************************************************************************************************************************************	
// *****************************************************************************************************************************************************************	
//												SERVER PROCESSING
// *****************************************************************************************************************************************************************	
// *****************************************************************************************************************************************************************	
		
		
		
		// -----------------------------------
		// Process server game logic		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			
			SWITCH GET_SERVER_MISSION_STATE()
				
				// For CnC group missions, we may want to wait for all players to have launched the script
				CASE GAME_STATE_INI
					
					serverBD.iServerGameState = GAME_STATE_RUNNING
					#IF IS_DEBUG_BUILD NET_DW_PRINT("serverBD.iServerGameState = GAME_STATE_RUNNING") #ENDIF
						
				BREAK
				
				// Look for game end conditions
				CASE GAME_STATE_RUNNING
					MAINTAIN_SERVER_STAGGERED_LOOP()
					MAINTAIN_CAR_MOD_TUTORIAL_SERVER()
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE
						ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_CAR_MOD_TUTORIAL_SERVER")
					#ENDIF
					#ENDIF		

					IF HAVE_MISSION_END_CONDITIONS_BEEN_MET()
						serverBD.iServerGameState = GAME_STATE_END
						#IF IS_DEBUG_BUILD NET_DW_PRINT ("serverBD.iServerGameState GAME_STATE_RUNNING > GAME_STATE_END ")#ENDIF
						
					ENDIF
					
				BREAK
				
				CASE GAME_STATE_END
				BREAK
				
			ENDSWITCH
			
		ENDIF
		
		
		
		#IF IS_DEBUG_BUILD 
			#IF SCRIPT_PROFILER_ACTIVE
				SCRIPT_PROFILER_END_OF_FRAME()
			#ENDIF
		#ENDIF			
	
	ENDWHILE

ENDSCRIPT

