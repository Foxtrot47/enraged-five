/// Freemode Lester cutscene
///    Dave W



USING "globals.sch"

// Game Headers
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_camera.sch"
USING "commands_hud.sch"

// Network Headers
USING "net_include.sch"
USING "net_events.sch"

// Headers

USING "net_mission.sch"
USING "net_scoring_common.sch"

USING "commands_path.sch"
USING "net_blips.sch"
USING "commands_zone.sch"


USING "hud_drawing.sch"
USING "net_ambience.sch"


USING "shop_public.sch"
USING "net_garages.sch"



USING "net_mission_details_overlay.sch"

USING "net_hud_displays.sch"

USING "fm_hold_up_tut.sch"

USING "net_wait_zero.sch"

#IF IS_DEBUG_BUILD
	USING "profiler.sch"
#ENDIF
USING "net_gang_boss.sch"

// Game States
CONST_INT GAME_STATE_INI 		0
CONST_INT GAME_STATE_INI_SPAWN	1
CONST_INT GAME_STATE_RUNNING	2
CONST_INT GAME_STATE_LEAVE		3
CONST_INT GAME_STATE_FAILED		4
CONST_INT GAME_STATE_TERMINATE_DELAY 5
CONST_INT GAME_STATE_END		6

//MP_MISSION thisMission = eFM_HOLD_UP_TUT

CONST_INT biS_AllPlayersFinished		0

// Everyone can read this data, only the server can update it. 
STRUCT ServerBroadcastData
	INT iServerGameState
	INT iServerBitSet
	INT iPartWithLester
	SCRIPT_TIMER timeTerminate
	HOLD_UP_TUT_SERVER_STRUCT holdUpTutServer
ENDSTRUCT
ServerBroadcastData serverBD


INT iServerStaggeredLoopCount

CONST_INT iCarModProgGetCarModded			0


CONST_INT biP_DoneCutscene					0
CONST_INT biP_WantToSeeLester				1

STRUCT PlayerBroadcastData
	INT iGameState
	INT iLesterCutProg
	INT iPlayerBitSet
ENDSTRUCT
PlayerBroadcastData PlayerBD[NUM_NETWORK_PLAYERS]

INT iLesterCutProg
PED_INDEX pedLester
PED_INDEX pedPlayer
OBJECT_INDEX oWheelChair
INT iSceneID


//CAMERA_INDEX camLester
//SCRIPT_TIMER timeLester, timeSkipLesterCut
structPedsForConversation speechFmLesterCut

BOOL bDoMocap = FALSE //TRUE

BOOL bDoPlayerFade = TRUE
INT iBoolsBitSet

CONST_INT biL_ServerLoopedThroughEveryone		0
CONST_INT biL_StaggeredAllPlayersFinished		1
CONST_INT biL_ToldToGoToLester					2
CONST_INT biL_RequestedCut						3
CONST_INT biL_SkippedMocap						4
CONST_INT biL_TextCleared						5
CONST_INT biL_OnMission							6
CONST_INT biL_SkippedCut						7
CONST_INT biL_ToldToLoseCops					8
CONST_INT biL_DoneBlipHelp						9
CONST_INT biL_ReqCutAssets						10
CONST_INT biL_SetEndCutPos						11
CONST_INT biL_ServerStagSomeoneWithLester		12
CONST_INT biL_LesterBusy						13
CONST_INT biL_LesterBlipInactive				14
CONST_INT biL_DrawScaleform						15
CONST_INT biL_LaunchMocap						16
CONST_INT biL_UnlockedDoor						17
CONST_INT biL_GetToLester						18
CONST_INT biL_DisabledWeaponSelect				19
CONST_INT biL_StartLesterScene					20
CONST_INT biL_GivenLookAt						21
CONST_INT biL_SetDoorResetFlags					22
CONST_INT biL_CreateLester						23
CONST_INT biL_StoreDisabled						24
CONST_INT biL_SetInvicible						25
CONST_INT biL_SetCamExitState					26



//VECTOR g_vLesterHouse = <<1274.7963, -1721.3986, 53.6550>>
STRING sLesterCut = "MP_INTRO_MCS_13" //""
//BLIP_INDEX g_blipLesterHouse
BLIP_INDEX blipLester

SCRIPT_TIMER timeSinceOnMission
//SCRIPT_TIMER timeMocapSkip

INTERIOR_INSTANCE_INDEX interiorLester


#IF IS_DEBUG_BUILD
	BOOL bDisableAllDebug = FALSE
	PROC NET_DW_PRINT(STRING sText)
		IF NOT bDisableAllDebug
			NET_NL() NET_PRINT_TIME() NET_PRINT("[FM_LesterCut] [DSW] ") NET_PRINT_STRINGS(" ", sText) NET_NL()
		ENDIF
	ENDPROC 

	PROC NET_DW_PRINT_STRING_INT(STRING sText1, INT i)
		IF NOT bDisableAllDebug
			NET_NL() NET_PRINT_TIME() NET_PRINT("[FM_LesterCut] [DSW] ") NET_PRINT_STRING_INT(sText1, i) NET_NL()
		ENDIF
	ENDPROC

	PROC NET_DW_PRINT_STRING_FLOAT(STRING sText1, FLOAT f)
		IF NOT bDisableAllDebug
			NET_NL() NET_PRINT_TIME() NET_PRINT("[FM_LesterCut] [DSW] ") NET_PRINT_STRING_FLOAT(sText1, f) NET_NL()
		ENDIF
	ENDPROC

	PROC NET_DW_PRINT_STRING_VECTOR(STRING sText1, VECTOR v)
		IF NOT bDisableAllDebug
			NET_NL() NET_PRINT_TIME() NET_PRINT("[FM_LesterCut] [DSW] ") NET_PRINT_STRING_VECTOR(sText1, v) NET_NL()
		ENDIF
	ENDPROC
	
	PROC NET_DW_PRINT_STRINGS(STRING sText1, STRING sText2)
		IF NOT bDisableAllDebug
			NET_NL() NET_PRINT_TIME() NET_PRINT("[FM_LesterCut] [DSW] ") NET_PRINT_STRINGS(sText1, sText2) NET_NL()
		ENDIF
	ENDPROC
	
	PROC NET_DW_PRINT_STRING_WITH_PLAYER_NAME(STRING sText, PLAYER_INDEX player)
		IF NOT bDisableAllDebug
			NET_NL() NET_PRINT_TIME() NET_PRINT("[FM_LesterCut] [DSW] ") NET_PRINT_STRINGS(" ", sText) NET_PRINT_STRINGS(" ", GET_PLAYER_NAME(player)) NET_NL()
		ENDIF
	ENDPROC
	
	BOOL bWdDoStaggeredDebug
	BOOL bWdCreateLester
	BOOL bWDUnlockDoor
	BOOL bWdPlayLine
	PROC CREATE_WIDGETS()
		START_WIDGET_GROUP("Lester Cut")
			//Profiling widgets
			#IF SCRIPT_PROFILER_ACTIVE
			CREATE_SCRIPT_PROFILER_WIDGET()
			#ENDIF		
			ADD_WIDGET_BOOL("Create Lester", bWdCreateLester)
			ADD_WIDGET_BOOL("Unlock door", bWDUnlockDoor)
			ADD_WIDGET_BOOL("Play line", bWdPlayLine)
			START_WIDGET_GROUP("Player")
				ADD_BIT_FIELD_WIDGET("Local", iBoolsBitSet)
				ADD_BIT_FIELD_WIDGET("Player", PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet)
				
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Server")
				
				ADD_WIDGET_BOOL("Staggered debug ", bWdDoStaggeredDebug)
				ADD_WIDGET_INT_READ_ONLY("iPartWithLester", serverbd.iPartWithLester)
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP() 
	ENDPROC
	
	
	

	
#ENDIF

FUNC BOOL SHOULD_UI_BE_HIDDEN()
	BOOL bSHouldHide
	RETURN FALSE
		
		IF SHOULD_HIDE_JOB_BLIP(CI_TYPE_M3_HIDE_JOB_MISSION)
			bSHouldHide = TRUE
		ENDIF

	
//	IF bSHouldHide 
//		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_HidingEvent)
//			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_HidingEvent)
//			PRINTLN("     ---------->     URBAN WARFARE [SHOULD_UI_BE_HIDDEN] SET biP_HidingEvent  <---------- ")
//		ENDIF
//	ELSE
//		IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_HidingEvent)
//			CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_HidingEvent)
//			PRINTLN("     ---------->     URBAN WARFARE [SHOULD_UI_BE_HIDDEN] CLEAR biP_HidingEvent  <---------- ")
//		ENDIF
//	ENDIF
	
	RETURN bSHouldHide
ENDFUNC

FUNC BOOL REQUESTED_LESTER_CUTSCENE()
//	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//		IF NOT IS_BIT_SET(iBoolsBitSet, biL_RequestedCut)
//		
//			IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), g_vLesterHouse) < 625 // 25m
//				REQUEST_CUTSCENE(sLesterCut)
//				SET_BIT(iBoolsBitSet, biL_RequestedCut)
//				
//				#IF IS_DEBUG_BUILD NET_DW_PRINT("I have requested Lester cutscene") #ENDIF
//				
//				interiorLester = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<1274.3647, -1711.9680, 53.7715>>, "v_lesters") 
//				IF interiorLester <> NULL
//					PIN_INTERIOR_IN_MEMORY(interiorLester)
//					#IF IS_DEBUG_BUILD NET_DW_PRINT("Pinned Lester's interior")#ENDIF
//				ENDIF
//				
//			ENDIF
//		ELSE
//			IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), g_vLesterHouse) > 1225 // 35m
//				REMOVE_CUTSCENE()
//				
//				IF interiorLester <> NULL
//					UNPIN_INTERIOR(interiorLester)
//				ENDIF
//				
//				CLEAR_BIT(iBoolsBitSet, biL_RequestedCut)
//				#IF IS_DEBUG_BUILD NET_DW_PRINT("I have removed lester cutscene as too far away") #ENDIF
//			ENDIF
//			
//			IF NOT IS_BIT_SET(iBoolsBitSet, biL_ReqCutAssets)
//				IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
//					IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01  //IS_PLAYER_FEMALE()
//					//	SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("MP_1", PLAYER_PED_ID())	
//						SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_1", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
//					ELSE
//					//	SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("MP_1", PLAYER_PED_ID())
//						SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_1", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
//						
//					ENDIF
//					SET_BIT(iBoolsBitSet, biL_ReqCutAssets)
//					#IF IS_DEBUG_BUILD NET_DW_PRINT("Set biL_ReqCutAssets") #ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
	
	RETURN (IS_BIT_SET(iBoolsBitSet, biL_RequestedCut))
ENDFUNC	

#IF IS_DEBUG_BUILD
	PROC DO_LESTER_CUTSCENE_J_SKIPS()
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
			IF IS_NET_PLAYER_OK(PLAYER_ID())
				IF NOT IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
					NET_DW_PRINT("Player ok for j-skip")
					SWITCH PlayerBD[PARTICIPANT_ID_TO_INT()].iLesterCutProg	
						CASE 0
							NET_DW_PRINT("Running Case 0 j-skip")
						//	REQUESTED_LESTER_CUTSCENE()
							J_SKIP_MP( <<1277.3188, -1725.9181, 53.6552>>, <<1.0, 1.0, 1.0>>)
						BREAK
					ENDSWITCH
				ENDIF
			ENDIF
		ENDIF
		
	ENDPROC
#ENDIF

FUNC INT GET_CLIENT_MISSION_STATE(INT iPlayer)
	RETURN playerBD[iPlayer].iGameState
ENDFUNC

FUNC INT GET_SERVER_MISSION_STATE()
	RETURN serverBD.iServerGameState
ENDFUNC



PROC CLEANUP_LESTER_PED()
	ENTITY_INDEX ent
	IF DOES_BLIP_EXIST(blipLester)
		REMOVE_BLIP(blipLester)
	ENDIF
	IF DOES_ENTITY_EXIST(pedLester)
		ent = pedLester
		DELETE_ENTITY(ent)
	ENDIF
	
	IF DOES_ENTITY_EXIST(oWheelChair)
		ent = oWheelChair
		DELETE_ENTITY(ent)
	ENDIF
	
	Clear_Any_Objective_Text_From_This_Script()
	
	SET_MODEL_AS_NO_LONGER_NEEDED(GET_NPC_PED_MODEL(CHAR_LESTER))
	SET_MODEL_AS_NO_LONGER_NEEDED(Prop_WheelChair_01_S)
	REMOVE_ANIM_DICT("missfinale_c2leadinoutfin_c_int") //MissLester1ALeadInOut
ENDPROC
/// PURPOSE:
///    Handles the cleanup of the script
PROC CLEANUP_SCRIPT()
	
	ENTITY_INDEX ent
	
	#IF IS_DEBUG_BUILD
		NET_DW_PRINT("Running cleanup")
	#ENDIF
	Clear_Any_Objective_Text_From_This_Script()
	SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_LESTER_CUTSCENE, FALSE)
	CLEAR_TUTORIAL_INVITES_ARE_BLOCKED() 
	
//	SET_INTERIOR_CAPPED( INTERIOR_V_LESTERS, TRUE )
	
	//SET_FM_UNLOCKS_BIT_SET()
	IF interiorLester <> NULL
		UNPIN_INTERIOR(interiorLester)
	ENDIF
	
	IF DOES_ENTITY_EXIST(pedPlayer)
		ent = pedPlayer
		DELETE_ENTITY(ent)
		
		#IF IS_DEBUG_BUILD
			NET_DW_PRINT("[CLEANUP_SCRIPT] Cleaned up Clone ped")
		#ENDIF 
	ENDIF
	
	IF DOES_BLIP_EXIST(g_blipLesterHouse)
		SET_BLIP_ROUTE(g_blipLesterHouse, FALSE)
		REMOVE_BLIP(g_blipLesterHouse)
	ENDIF
	
	IF IS_BIT_SET(iBoolsBitSet, biL_UnlockedDoor)
		IF IS_DOOR_REGISTERED_WITH_SYSTEM(MP_DOOR_CS_ONLY_INT_LESTER)
			DOOR_SYSTEM_SET_DOOR_STATE(MP_DOOR_CS_ONLY_INT_LESTER, DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE)
			#IF IS_DEBUG_BUILD NET_DW_PRINT("Cleanup locking Lester's door") #ENDIF
		ELSE
			#IF IS_DEBUG_BUILD NET_DW_PRINT("Cleanup not locking Lester's door as IS_DOOR_REGISTERED_WITH_SYSTEM") #ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD NET_DW_PRINT("Cleanup not locking Lester's door as it wasn't unlocked") #ENDIF
	ENDIF
	
	IF IS_BIT_SET(iBoolsBitSet, biL_DisabledWeaponSelect)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_NEXT)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_PREV)
	ENDIF
	
	IF IS_KILL_YOURSELF_OPTION_DISABLED()
		ENABLE_KILL_YOURSELF_OPTION()
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		CLEAR_BIT(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_WantToSeeLester)
	ENDIF
	
	IF IS_BIT_SET(iBoolsBitSet, biL_SetInvicible)
		SET_PLAYER_INVINCIBLE(PLAYER_ID(), FALSE)
	ENDIF
	
	IF IS_BIT_SET(iBoolsBitSet, biL_StoreDisabled)
		SET_STORE_ENABLED(TRUE)
	ENDIF
	CLEANUP_LESTER_PED()
	REMOVE_CUTSCENE()
	
	//TERMINATE_THIS_THREAD()
	//RESET_FMMC_MISSION_VARIABLES(FALSE, FALSE, ciFMMC_END_OF_MISSION_STATUS_PASSED)
	
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

/// PURPOSE:
///    Do necessary pre game start ini.
PROC PROCESS_PRE_GAME(MP_MISSION_DATA missionScriptArgs)
	// This marks the script as a net script, and handles any instancing setup. 
	SETUP_MP_MISSION_FOR_NETWORK(GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(missionScriptArgs.mdID.idMission) , missionScriptArgs)
	
	// This makes sure the net script is active, waits until it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
	
	// This script will not be paused if another script calls PAUSE_GAME
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	
	// KGM: Wait for the first network broadcast before moving on - call this as the last instruction in pre-game
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		#IF IS_DEBUG_BUILD
		NET_DW_PRINT("Failed to receive initial network broadcast. Cleaning up.")
		#ENDIF
		CLEANUP_SCRIPT()
	ENDIF
	
	//SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_LESTER_CUTSCENE, TRUE)
	//SET_TUTORIAL_INVITES_ARE_BLOCKED()

	
	
//	SET_FM_UNLOCKS_BIT_SET()
//	SET_INTERIOR_DISABLED(INTERIOR_V_LESTERS, FALSE)
	#IF IS_DEBUG_BUILD
		NET_DW_PRINT("Launching....V3")
		
		IF SHOULD_UI_BE_HIDDEN()
			NET_DW_PRINT("Launching hidden!")
		ENDIF
	#ENDIF
ENDPROC

FUNC BOOL HAVE_MISSION_END_CONDITIONS_BEEN_MET()
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_AllPlayersFinished)
		#IF IS_DEBUG_BUILD NET_DW_PRINT("[HAVE_MISSION_END_CONDITIONS_BEEN_MET] True because biS_AllPlayersFinished") #ENDIF
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC


PROC MAINTAIN_SERVER_STAGGERED_LOOP()
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF iServerStaggeredLoopCount = 0
			CLEAR_BIT(iBoolsBitSet, biL_ServerLoopedThroughEveryone)
			SET_BIT(iBoolsBitSet, biL_StaggeredAllPlayersFinished)
			
			IF serverbd.iPartWithLester <> -1
				IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(serverbd.iPartWithLester))
					#IF IS_DEBUG_BUILD
						NET_DW_PRINT_STRING_INT("This part was with Lester, but part no longer active: ", serverbd.iPartWithLester)
					#ENDIF
					
					serverbd.iPartWithLester = -1
					
					
				ENDIF
			ENDIF
			
			#IF IS_DEBUG_BUILD
				IF bWdDoStaggeredDebug
					NET_DW_PRINT("[ServStag] At Start of server staggered loop")
				ENDIF
			#ENDIF
		ENDIF
		
		
		
		
	
		PARTICIPANT_INDEX part = INT_TO_NATIVE(PARTICIPANT_INDEX, iServerStaggeredLoopCount )
		
		IF NETWORK_IS_PARTICIPANT_ACTIVE(part)
			PLAYER_INDEX player = NETWORK_GET_PLAYER_INDEX(part)
			
			
			
			IF NOT IS_BIT_SET(PlayerBD[iServerStaggeredLoopCount].iPlayerBitSet, biP_DoneCutscene)
				CLEAR_BIT(iBoolsBitSet, biL_StaggeredAllPlayersFinished)
			ENDIF
			IF IS_NET_PLAYER_OK(player, FALSE)	
				IF IS_BIT_SET(PlayerBD[iServerStaggeredLoopCount].iPlayerBitSet, biP_WantToSeeLester)	
					SET_BIT(iBoolsBitSet, biL_ServerStagSomeoneWithLester)
					
					#IF IS_DEBUG_BUILD
						IF bWdDoStaggeredDebug
							NET_DW_PRINT_STRING_WITH_PLAYER_NAME("This player wantes to see Lester... ", player)
						ENDIF
					#ENDIF
					
					IF serverbd.iPartWithLester = -1
						IF serverbd.iPartWithLester <> iServerStaggeredLoopCount
							serverbd.iPartWithLester = iServerStaggeredLoopCount
							#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_WITH_PLAYER_NAME("[ServStag] Setting this player can see Lester... ", player) #ENDIF
						ENDIF	
					ENDIF
				ELSE
					IF serverbd.iPartWithLester = iServerStaggeredLoopCount
						serverbd.iPartWithLester = -1
						#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("Setting serverbd.iPartWithLester as this part no longer with Lester ", iServerStaggeredLoopCount) #ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		iServerStaggeredLoopCount++
		IF iServerStaggeredLoopCount = NUM_NETWORK_PLAYERS
			#IF IS_DEBUG_BUILD
				IF bWdDoStaggeredDebug
					NET_DW_PRINT("[ServStag] At End of server staggered loop")
				ENDIF
			#ENDIF
			IF IS_BIT_SET(iBoolsBitSet, biL_StaggeredAllPlayersFinished)
				SET_BIT(serverBD.iServerBitSet, biS_AllPlayersFinished)
				#IF IS_DEBUG_BUILD NET_DW_PRINT("[ServStag] Setting biS_AllPlayersFinished because all players finished") #ENDIF
			ENDIF
			
			IF NOT IS_BIT_SET(iBoolsBitSet, biL_ServerStagSomeoneWithLester)
				IF serverbd.iPartWithLester <> -1
					serverbd.iPartWithLester = -1
					#IF IS_DEBUG_BUILD NET_DW_PRINT("[ServStag] serverbd.iPartWithLester = -1 as no one with LESTER") #ENDIF
				ENDIF
			ENDIF
			SET_BIT(iBoolsBitSet, biL_ServerLoopedThroughEveryone)
			iServerStaggeredLoopCount = 0
		ENDIF
	ENDIF
ENDPROC	



PROC MAINTAIN_LESTER_CUTSCENE_SERVER()
	
	
	
//	BOOL bFoundAPairing
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		
		
	ENDIF
ENDPROC


FUNC BOOL SHOULD_LESTER_CUTSCENE_COMPLETE()
	IF IS_BIT_SET(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DoneCutscene)
		#IF IS_DEBUG_BUILD NET_DW_PRINT("[SHOULD_LESTER_CUTSCENE_COMPLETE] True because biP_DoneCutscene") #ENDIF
		RETURN TRUE
	ENDIF
	#IF IS_DEBUG_BUILD 
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S) 
			IF iLesterCutProg < 1
				IF NOT IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
				AND NOT IS_FM_MISSION_LAUNCH_IN_PROGRESS()
					NET_DW_PRINT("[SHOULD_LESTER_CUTSCENE_COMPLETE] True because S-Passed")
				 	RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	#ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_OK_TO_PRINT_HELP()
	IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
	OR IS_PED_INJURED(PLAYER_PED_ID())
	OR NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
	OR  IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
	OR IS_SCRIPT_HUD_DISPLAYING(HUDPART_MISSIONBRIEF)
	OR IS_MP_MISSION_DETAILS_OVERLAY_ON_DISPLAY()
	OR IS_SCRIPT_HUD_DISPLAYING(HUDPART_MISSIONBOX)
	OR IS_HELP_MESSAGE_BEING_DISPLAYED()
	OR IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
	OR IS_FM_MISSION_LAUNCH_IN_PROGRESS()
	OR IS_CUSTOM_MENU_ON_SCREEN() 
	OR IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
	OR IS_MISSION_SUMMARY_SCREEN_DISPLAYED()
	OR IS_PAUSE_MENU_ACTIVE()
		RETURN(FALSE)
	ENDIF
	
	
	RETURN(TRUE)
ENDFUNC

FUNC BOOL CREATE_PLAYER_PED_COPY_FOR_LESTER_CUT()
	IF NOT DOES_ENTITY_EXIST(pedPlayer)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			MODEL_NAMES mPlayer = GET_ENTITY_MODEL(PLAYER_PED_ID())
			pedPlayer = CREATE_PED(PEDTYPE_CIVMALE, mPlayer, <<1273.8271, -1717.8727, 53.7715>>, 19.2225, FALSE, FALSE)
			SET_ENTITY_INVINCIBLE(pedPlayer, TRUE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedPlayer, TRUE)
			CLONE_PED_TO_TARGET(PLAYER_PED_ID(), pedPlayer)
		ENDIF
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(pedPlayer)
		RETURN FALSE
	ENDIF
	
	IF NOT IS_PED_INJURED(pedPlayer)
		IF NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedPlayer)
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL HAS_PLAYER_REACHED_LESTERS_HOUSE()
	ENTITY_INDEX ent
	
//	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//		IF bDoPlayerFade
//			//-- Create the clone ped in advance, as we have to wait for all its streaming requests to finish before the cutscene can start
//			IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()),  <<1274.5474, -1720.6945, 53.6807>>) < 500
//				IF CREATE_PLAYER_PED_COPY_FOR_LESTER_CUT()
//				ENDIF
//			ELSE
//				
//				//-- Clean up the clone ped if player is no longer near Lester's
//				IF DOES_ENTITY_EXIST(pedPlayer)
//					ent = pedPlayer
//					DELETE_ENTITY(ent)
//					
//					#IF IS_DEBUG_BUILD
//						NET_DW_PRINT("[HAS_PLAYER_REACHED_LESTERS_HOUSE] Cleaned up Clone ped")
//					#ENDIF 
//				ENDIF
//				
//				RETURN FALSE
//			ENDIF
//		ENDIF
//		
//		IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<1273.804321,-1722.466064,53.217487>>, <<1277.147217,-1720.779053,56.092487>>, 3.437500)
//			RETURN TRUE
//		ENDIF
//	
//	ENDIF
	
	IF IS_PED_INJURED(PLAYER_PED_ID())
		RETURN FALSE
	ENDIF
	
	IF bDoPlayerFade
		//-- Approaching Lester's House?
		IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()),  <<1274.5474, -1720.6945, 53.6807>>) > 500
			//-- No
			
			
			//-- Clean up the clone ped if player is no longer near Lester's
			IF DOES_ENTITY_EXIST(pedPlayer)
				ent = pedPlayer
				DELETE_ENTITY(ent)
				
				#IF IS_DEBUG_BUILD
					NET_DW_PRINT("[HAS_PLAYER_REACHED_LESTERS_HOUSE] Cleaned up Clone ped")
				#ENDIF 
			ENDIF
			
			RETURN FALSE
		ENDIF
		
		//-- Approaching Lester's, create the clone ped in advance, as we have to wait for all its streaming requests to finish before the cutscene can start
		CREATE_PLAYER_PED_COPY_FOR_LESTER_CUT()
	ENDIF
	
	//-- Near Lester's front door?
	IF NOT IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<1273.804321,-1722.466064,53.217487>>, <<1277.147217,-1720.779053,56.092487>>, 3.437500)
		//-- No
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC



FUNC BOOL HAS_LESTER_CUT_LOADED()
	IF REQUESTED_LESTER_CUTSCENE()
		IF HAS_CUTSCENE_LOADED()
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC



FUNC BOOL CREATE_LESTER_PED()
	BOOL bPlayingANim
	REQUEST_MODEL(GET_NPC_PED_MODEL(CHAR_LESTER))
	REQUEST_MODEL(Prop_WheelChair_01_S)
	REQUEST_ANIM_DICT("missfinale_c2leadinoutfin_c_int")  //MissLester1ALeadInOut
	IF NOT DOES_ENTITY_EXIST(pedLester)
		IF HAS_MODEL_LOADED(GET_NPC_PED_MODEL(CHAR_LESTER)) 
		//	IF CREATE_NPC_PED_ON_FOOT(pedLester, CHAR_LESTER, <<1275.3646, -1710.7744, 53.7715>>, 333.7887)
			IF CREATE_NPC_PED_ON_FOOT(pedLester, CHAR_LESTER, << 1276.390, -1712.845, 54.372 >>, 338.0729)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedLester, TRUE)
				SET_ENTITY_INVINCIBLE(pedLester, TRUE)
				ADD_PED_FOR_DIALOGUE(speechFmLesterCut, 8, pedLester, "Lester")
				SET_PED_PROP_INDEX(pedLester, ANCHOR_EYES, 0,0)
				#IF IS_DEBUG_BUILD NET_DW_PRINT("[CREATE_LESTER_PED] Created Lester ") #ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(oWheelChair)
		IF HAS_MODEL_LOADED(Prop_WheelChair_01_S)
		//	oWheelChair =  CREATE_OBJECT(Prop_WheelChair_01_S, <<1275.3646, -1710.7744, 53.7715>>, FALSE, FALSE)
			oWheelChair =  CREATE_OBJECT(Prop_WheelChair_01_S, << 1276.390, -1712.845, 54.372 >>, FALSE, FALSE)
			#IF IS_DEBUG_BUILD NET_DW_PRINT("[CREATE_LESTER_PED] Created wheelchar ") #ENDIF
		ENDIF
	ENDIF
	
	
	IF NOT DOES_ENTITY_EXIST(pedLester)
		RETURN FALSE
	ENDIF
	
	IF NOT IS_PED_INJURED(pedLester)
		IF NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedLester)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(oWheelChair)
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_ANIM_DICT_LOADED("missfinale_c2leadinoutfin_c_int")
		RETURN FALSE
	ENDIF
	
	IF NOT IS_PED_INJURED(pedLester)
	//	iSceneID = CREATE_SYNCHRONIZED_SCENE(<<1277.661, -1713.688, 54.410>>, <<0,0,-151.560>>)
		iSceneID = CREATE_SYNCHRONIZED_SCENE(<< 1276.390, -1712.845, 54.372 >>,  << 0.000, 0.000, -155.520 >>)
		TASK_SYNCHRONIZED_SCENE(pedLester, iSceneID, "missfinale_c2leadinoutfin_c_int", "_LEADIN_LOOP1_LESTER", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT) //Lester_1_INT_LeadIn_loop_Lester
		PLAY_SYNCHRONIZED_ENTITY_ANIM(oWheelChair, 	iSceneID, "_LEADIN_LOOP1_WCHAIR", "missfinale_c2leadinoutfin_c_int", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, enum_to_int(SYNCED_SCENE_DONT_INTERRUPT)) //Lester_1_INT_LeadIn_loop_wChair
		SET_SYNCHRONIZED_SCENE_LOOPED(iSceneID, TRUE)
		#IF IS_DEBUG_BUILD NET_DW_PRINT("[CREATE_LESTER_PED] Playing anim ") #ENDIF
		bPlayingANim = TRUE
	ENDIF
	
	IF NOT bPlayingANim
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC



//FUNC BOOL DO_LESTER_SCRIPTED_CUT()
//	PLAYER_SPAWN_LOCATION eSpawnLoc = SPAWN_LOCATION_CUSTOM_SPAWN_POINTS //SPAWN_LOCATION_NEAR_SPECIFIC_COORDS
//	
//	SWITCH iLesterCutProg
//		CASE 0
//			IF IS_PLAYER_OK_TO_START_MP_CUTSCENE()	
//				
//				IF NOT IS_SCREEN_FADED_OUT()
//				AND NOT IS_SCREEN_FADING_OUT()
//					
//					DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
//					
//				ENDIF
//				//SET_INTERIOR_CAPPED( INTERIOR_V_LESTERS, FALSE )
//				interiorLester = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<1274.3647, -1711.9680, 53.7715>>, "v_lesters") 
//				REQUEST_MODEL(GET_NPC_PED_MODEL(CHAR_LESTER))
//				#IF IS_DEBUG_BUILD
//					IF interiorLester = NULL
//						NET_DW_PRINT("[DO_LESTER_SCRIPTED_CUT] interiorLester is NULL!")
//					ENDIF
//				#ENDIF
//			ENDIF
//			iLesterCutProg = 1
//			#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_LESTER_SCRIPTED_CUT] iLesterCutProg = ", iLesterCutProg) #ENDIF
//		BREAK
//		
//		CASE 1
//			IF IS_SCREEN_FADED_OUT()
//				MAKE_PLAYER_SAFE_FOR_MP_CUTSCENE(FALSE)
//				START_MP_CUTSCENE()	
//				
//				// Dogs barking
//				SET_AMBIENT_ZONE_LIST_STATE_PERSISTENT("AZL_LESTERS_DOGS", FALSE, TRUE)
//				IF interiorLester <> NULL
//					PIN_INTERIOR_IN_MEMORY(interiorLester)
//					#IF IS_DEBUG_BUILD NET_DW_PRINT("Pinned Lester's interior")#ENDIF
//				ENDIF
//				
//				NEW_LOAD_SCENE_START(<<1275.5996, -1709.4865, 56.1878>>,  CONVERT_ROTATION_TO_DIRECTION_VECTOR(<<-21.4841, 0.0000, 145.6310>>), 20.0)
//				iLesterCutProg = 2
//				#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_LESTER_SCRIPTED_CUT] iLesterCutProg = ", iLesterCutProg) #ENDIF
//			ENDIF
//		BREAK
//		
//		CASE 2
//			IF IS_NEW_LOAD_SCENE_LOADED()
//				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//					SET_ENTITY_COORDS(PLAYER_PED_ID(), <<1275.9810, -1714.0739, 53.7715>>)
//				ENDIF
//				iLesterCutProg = 3
//				#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_LESTER_SCRIPTED_CUT] iLesterCutProg = ", iLesterCutProg) #ENDIF
//			ELSE	
//				#IF IS_DEBUG_BUILD NET_DW_PRINT("Waiting for Lester Load scene") #ENDIF
//			ENDIF
//		BREAK
//		
//		CASE 3
//			IF CREATE_LESTER_PED()
//			AND CREATE_PLAYER_PED_COPY_FOR_LESTER_CUT()
//				START_NET_TIMER(timeSkipLesterCut)
//				camLester = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE) 
//				SET_CAM_ACTIVE(camLester, TRUE)								
//				SET_CAM_PARAMS(camLester, <<1275.5996, -1709.4865, 56.1878>>, <<-21.4841, 0.0000, 145.6310>>,  44.9352)
//				SET_CAM_PARAMS(camLester, <<1275.5996, -1709.4865, 56.1878>>, <<-21.4841, 0.0000, 138.8586>>, 44.9352, 30000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)										
//				
//				RENDER_SCRIPT_CAMS(TRUE, FALSE)
//				NEW_LOAD_SCENE_STOP()
//				
//				IF IS_SCREEN_FADED_OUT()
//					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
//				ENDIF
//				
//				RESET_NET_TIMER(timeLester)
//				
//				iLesterCutProg = 4
//				#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_LESTER_SCRIPTED_CUT] iLesterCutProg = ", iLesterCutProg) #ENDIF
//			ENDIF
//		BREAK
//		
//		CASE 4
//			IF CREATE_CONVERSATION (speechFmLesterCut, "FM_1AU", "FM_LEST", CONV_PRIORITY_MEDIUM)
//				iLesterCutProg = 5
//				#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_LESTER_SCRIPTED_CUT] iLesterCutProg = ", iLesterCutProg) #ENDIF
//			ENDIF
//			
//		BREAK
//		
//		CASE 5
//			IF NOT HAS_NET_TIMER_STARTED(timeLester)
//				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//				#IF IS_DEBUG_BUILD OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RIGHT)#ENDIF
//					START_NET_TIMER(timeLester)
//				ENDIF
//			ELSE
//				IF HAS_NET_TIMER_EXPIRED(timeLester, 2000)
//					//SETUP_SPECIFIC_SPAWN_LOCATION(<<1276.6888, -1725.1228, 53.6552>>, 208.9770, 25.0, TRUE)
//					USE_CUSTOM_SPAWN_POINTS(TRUE) 
//					ADD_CUSTOM_SPAWN_POINT(<<1276.1385, -1723.6451, 53.6551>>,206.0283)
//					ADD_CUSTOM_SPAWN_POINT(<<1279.7800, -1722.7466, 53.6554>>,300.3913)
//					ADD_CUSTOM_SPAWN_POINT(<<1283.3549, -1721.1990, 53.6554>>,197.0761)
//					ADD_CUSTOM_SPAWN_POINT(<<1280.3962, -1724.6205, 53.6579>>,217.4622)
//					ADD_CUSTOM_SPAWN_POINT(<<1279.2388, -1729.8618, 51.5068>>,208.8080)
//					ADD_CUSTOM_SPAWN_POINT(<<1286.3494, -1726.2413, 52.0359>>,212.1051)
//					DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
//					iLesterCutProg = 6
//					#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_LESTER_SCRIPTED_CUT] iLesterCutProg = ", iLesterCutProg) #ENDIF
//				ENDIF
//			ENDIF
//		BREAK
//		
//		CASE 6
//			IF IS_SCREEN_FADED_OUT()
//				IF WARP_TO_SPAWN_LOCATION(eSpawnLoc, FALSE)
//			//	IF NET_WARP_TO_COORD(<<1276.6888, -1725.1228, 53.6552>>, 208.9770)
//					iLesterCutProg = 99
//					
//					#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_LESTER_SCRIPTED_CUT] iLesterCutProg = ", iLesterCutProg) #ENDIF
//				ENDIF
//			ENDIF
//		BREAK
//		
//		CASE 99
//			IF IS_SCREEN_FADED_OUT()
//				KILL_FACE_TO_FACE_CONVERSATION()
//				IF interiorLester <> NULL
//					UNPIN_INTERIOR(interiorLester)
//				ENDIF
//				
//				// Dogs barking
//				SET_AMBIENT_ZONE_LIST_STATE_PERSISTENT("AZL_LESTERS_DOGS", TRUE, TRUE)
//				
//				CLEANUP_MP_CUTSCENE()
//			//	CLEAR_SPECIFIC_SPAWN_LOCATION()
//				USE_CUSTOM_SPAWN_POINTS(FALSE) 
//				CLEAR_CUSTOM_SPAWN_POINTS()
//				IF DOES_ENTITY_EXIST(pedLester)
//					ENTITY_INDEX ent 
//					ent = pedLester
//					DELETE_ENTITY(ent)
//				ENDIF
//				IF DOES_ENTITY_EXIST(pedPlayer)
//					ENTITY_INDEX ent 
//					ent = pedPlayer
//					DELETE_ENTITY(ent)
//				ENDIF
//				DESTROY_ALL_CAMS()
//				RENDER_SCRIPT_CAMS(FALSE, FALSE)
//				
//				// SET_INTERIOR_CAPPED( INTERIOR_V_LESTERS, TRUE )
//				
//				
//				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
//				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
//				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
//				iLesterCutProg = 100
//				#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_LESTER_SCRIPTED_CUT] iLesterCutProg = ", iLesterCutProg) #ENDIF
//			ENDIF
//		BREAK
//		
//		CASE 100
//			RETURN TRUE
//		BREAK
//	ENDSWITCH
//	
//	IF NOT IS_BIT_SET(iBoolsBitSet, biL_SkippedCut)
//		IF iLesterCutProg > 4
//		AND iLesterCutProg < 6
//			IF IS_SCREEN_FADED_IN()
//				IF HAS_NET_TIMER_STARTED(timeSkipLesterCut)
//					IF HAS_NET_TIMER_EXPIRED(timeSkipLesterCut, 500)
//						IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED()
//							KILL_FACE_TO_FACE_CONVERSATION()
//						//	SETUP_SPECIFIC_SPAWN_LOCATION(<<1276.6888, -1725.1228, 53.6552>>, 208.9770, 50.0, TRUE)
//							USE_CUSTOM_SPAWN_POINTS(TRUE) 
//							ADD_CUSTOM_SPAWN_POINT(<<1276.1385, -1723.6451, 53.6551>>,206.0283)
//							ADD_CUSTOM_SPAWN_POINT(<<1279.7800, -1722.7466, 53.6554>>,300.3913)
//							ADD_CUSTOM_SPAWN_POINT(<<1283.3549, -1721.1990, 53.6554>>,197.0761)
//							ADD_CUSTOM_SPAWN_POINT(<<1280.3962, -1724.6205, 53.6579>>,217.4622)
//							ADD_CUSTOM_SPAWN_POINT(<<1279.2388, -1729.8618, 51.5068>>,208.8080)
//							ADD_CUSTOM_SPAWN_POINT(<<1286.3494, -1726.2413, 52.0359>>,212.1051)
//							DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
//							SET_BIT(iBoolsBitSet, biL_SkippedCut)
//							iLesterCutProg = 6
//							#IF IS_DEBUG_BUILD NET_DW_PRINT("Cutscene skipped!") #ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
//	
//	RETURN FALSE
//ENDFUNC

PROC MANAGE_PLAYER_TALKING()
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisableVoiceDrivenMouthMovement , TRUE)
		IF NETWORK_PLAYER_HAS_HEADSET(PLAYER_ID())
			IF NETWORK_IS_PLAYER_TALKING(PLAYER_ID()) 
			//	SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_EnableVoiceDrivenMouthMovement, TRUE)
			//	#IF IS_DEBUG_BUILD NET_DW_PRINT("Talking!") #ENDIF
			ENDIF 
		ENDIF 
	ENDIF
	
ENDPROC


BOOL bEndCutsceneSkipped
FUNC BOOL DO_LESTER_MOCAP()
	
	
	SWITCH iLesterCutProg
		CASE 0
			IF IS_CUTSCENE_PLAYING()
//				MAKE_PLAYER_SAFE_FOR_MP_CUTSCENE(FALSE, FALSE)
//				SET_LOCAL_PLAYER_VISIBLE_IN_CUTSCENE(TRUE, FALSE)
//				START_MP_CUTSCENE(TRUE)
				
				iLesterCutProg++
				#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_LESTER_MOCAP] iLesterCutProg = ", iLesterCutProg) #ENDIF
			ENDIF
		BREAK
		
		CASE 1
			IF IS_CUTSCENE_PLAYING()
				MANAGE_PLAYER_TALKING()
//			    IF IS_CUTSCENE_SKIP_BUTTON_PRESSED()
//					NETWORK_SET_MOCAP_CUTSCENE_CAN_BE_SKIPPED(TRUE)
//	                DO_SCREEN_FADE_OUT(500)
//					STOP_CUTSCENE()
//					bEndCutsceneSkipped = TRUE
//					iLesterCutProg++
//					#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_LESTER_MOCAP] Cut skipped iLesterCutProg = ", iLesterCutProg) #ENDIF
//	            ENDIF
			ELSE
				iLesterCutProg++
				#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_LESTER_MOCAP] iLesterCutProg = ", iLesterCutProg) #ENDIF
			ENDIF
		BREAK
		
		CASE 2
			IF bEndCutsceneSkipped
				IF NOT IS_CUTSCENE_ACTIVE()
					IF IS_SCREEN_FADED_OUT()
					OR IS_SCREEN_FADING_OUT()
						DO_SCREEN_FADE_IN(500)
					ENDIF
					bEndCutsceneSkipped = FALSE
				ENDIF
			ELSE
				IF HAS_CUTSCENE_FINISHED()
					CLEANUP_MP_CUTSCENE()
					SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
					NETWORK_SET_VOICE_ACTIVE(TRUE)
					#IF IS_DEBUG_BUILD NET_DW_PRINT("NETWORK_SET_VOICE_ACTIVE - TRUE") #ENDIF
					IF interiorLester <> NULL
						UNPIN_INTERIOR(interiorLester)
					ENDIF
					CLEAR_BIT(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_WantToSeeLester)
					iLesterCutProg++
					#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_LESTER_MOCAP] iLesterCutProg = ", iLesterCutProg) #ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 3
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	IF NOT IS_BIT_SET(iBoolsBitSet, biL_SetEndCutPos)
		 IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("MP_1")
             //Do stuff.
            IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<1275.2192, -1722.5026, 53.6550>>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 228.4235)
				SET_BIT(iBoolsBitSet, biL_SetEndCutPos)
				#IF IS_DEBUG_BUILD NET_DW_PRINT("Set player pos at cutscene end") #ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

INT iArriveLesterCutProg
SCALEFORM_INDEX scaleLester
CAMERA_INDEX camArriveLester
SCRIPT_TIMER timeArriveLesterCut

PROC CLEANUP_LESTER_ARRIVE_CUT()
	ENTITY_INDEX ent
	IF DOES_CAM_EXIST(camArriveLester)
		SET_CAM_ACTIVE(camArriveLester, FALSE)
		DESTROY_CAM(camArriveLester)
	ENDIF
	
	CLEAR_BIT(iBoolsBitSet, biL_DrawScaleform)
	
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
	
	IF DOES_ENTITY_EXIST(pedPlayer)
		ent = pedPlayer
		DELETE_ENTITY(ent)
	ENDIF
	
//	CLEANUP_MP_CUTSCENE()
	
	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(scaleLester)
	CLEAR_TIMECYCLE_MODIFIER()
	
//	NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
ENDPROC


FUNC BOOL DO_LESTER_MOCAP_V2()
	
	
	SWITCH iLesterCutProg
		CASE 0
			
			//-- get to Lester objective text
			
			IF NOT IS_BIT_SET(iBoolsBitSet, biL_GetToLester)
				IF NOT IS_PED_INJURED(pedLester)
					blipLester = ADD_BLIP_FOR_ENTITY(pedLester)
					SET_BLIP_AS_FRIENDLY(blipLester, TRUE)
					SET_BLIP_NAME_FROM_TEXT_FILE(blipLester, "FM_LEST_BLP")
					Print_Objective_Text("FM_LEST_GTL") //Go to ~b~Lester.
					SET_BIT(iBoolsBitSet, biL_GetToLester)
					#IF IS_DEBUG_BUILD NET_DW_PRINT("[DO_LESTER_MOCAP_V2] Added blip for Lester") #ENDIF
				ENDIF
			ENDIF
			
			
			//-- Lester turns to face player
			IF NOT IS_BIT_SET(iBoolsBitSet, biL_StartLesterScene)
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF NOT IS_PED_INJURED(pedLester)
						IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<1272.747192,-1715.223145,53.458969>>, <<1273.731934,-1717.426758,55.708969>>, 2.000000)
							iSceneID = CREATE_SYNCHRONIZED_SCENE(<< 1276.390, -1712.845, 54.372 >>,  << 0.000, 0.000, -155.520 >>)
							TASK_SYNCHRONIZED_SCENE (pedLester, iSceneID, "missfinale_c2leadinoutfin_c_int", "_LEADIN_ACTION_LESTER", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT, RBF_PLAYER_IMPACT )
							PLAY_SYNCHRONIZED_ENTITY_ANIM(oWheelChair, iSceneID, "_LEADIN_ACTION_WCHAIR", "missfinale_c2leadinoutfin_c_int", INSTANT_BLEND_IN, 8, ENUM_TO_INT(SYNCED_SCENE_DONT_INTERRUPT))
				
							SET_BIT(iBoolsBitSet, biL_StartLesterScene)
							
							#IF IS_DEBUG_BUILD NET_DW_PRINT("[DO_LESTER_MOCAP_V2] Doing Lester turn synch scene") #ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//-- Request Mocap
			IF NOT IS_BIT_SET(iBoolsBitSet, biL_RequestedCut)
				REQUEST_CUTSCENE(sLesterCut)
				SET_BIT(iBoolsBitSet, biL_RequestedCut)
				#IF IS_DEBUG_BUILD NET_DW_PRINT("[DO_LESTER_MOCAP_V2] Requested cutscene") #ENDIF
			ELSE
				IF NOT IS_BIT_SET(iBoolsBitSet, biL_ReqCutAssets)
					IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
						SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_1", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
						SET_BIT(iBoolsBitSet, biL_ReqCutAssets)
						#IF IS_DEBUG_BUILD NET_DW_PRINT("[DO_LESTER_MOCAP_V2] Set biL_ReqCutAssets") #ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			
			//-- Check for being near Lester
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<1275.094604,-1715.769287,53.708969>>, <<1274.382080,-1714.218506,56.021469>>, 1.375000)
					IF IS_BIT_SET(iBoolsBitSet, biL_ReqCutAssets)
						iLesterCutProg++
						#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_LESTER_MOCAP] iLesterCutProg = ", iLesterCutProg) #ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK	
		
		CASE 1
			IF HAS_CUTSCENE_LOADED() 
				
				
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01  //IS_PLAYER_FEMALE()
						REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "MP_1", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)

					ELSE
						REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "MP_1", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD NET_DW_PRINT("[DO_LESTER_MOCAP] Failed to register player ped for mocap as player ped is injured!") #ENDIF
				ENDIF
				#IF IS_DEBUG_BUILD NET_DW_PRINT("[DO_LESTER_MOCAP] Turning off player control for mocap.") #ENDIF
		//		NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, TRUE, TRUE, TRUE, TRUE)
				HIDE_CELLPHONE_SIGNIFIERS_FOR_CUTSCENE(TRUE) // Gets reset in CLEANUP_MP_CUTSCENE in case 4 below
				START_CUTSCENE()
				NETWORK_SET_VOICE_ACTIVE(FALSE)
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_CLEAR_TASKS  | NSPC_FREEZE_POSITION)
				HANG_UP_AND_PUT_AWAY_PHONE()
				#IF IS_DEBUG_BUILD NET_DW_PRINT("NETWORK_SET_VOICE_ACTIVE - FALSE") #ENDIF
				//MAKE_PLAYER_SAFE_FOR_MP_CUTSCENE(FALSE, FALSE, TRUE, FALSE )
				//SET_LOCAL_PLAYER_VISIBLE_IN_CUTSCENE(TRUE, FALSE)
				
				iLesterCutProg++
				#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_LESTER_MOCAP] iLesterCutProg = ", iLesterCutProg) #ENDIF
			ENDIF
		BREAK
		CASE 2
			IF IS_CUTSCENE_PLAYING()
//				MAKE_PLAYER_SAFE_FOR_MP_CUTSCENE(FALSE, FALSE)
//				SET_LOCAL_PLAYER_VISIBLE_IN_CUTSCENE(TRUE, FALSE)
//				START_MP_CUTSCENE(TRUE)
				//CLEANUP_LESTER_ARRIVE_CUT()
				CLEANUP_LESTER_PED()
				iLesterCutProg++
				#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_LESTER_MOCAP] iLesterCutProg = ", iLesterCutProg) #ENDIF
			ENDIF
		BREAK
		
		CASE 3
			IF IS_CUTSCENE_PLAYING()
				MANAGE_PLAYER_TALKING()
//			    IF IS_CUTSCENE_SKIP_BUTTON_PRESSED()
//					NETWORK_SET_MOCAP_CUTSCENE_CAN_BE_SKIPPED(TRUE)
//	                DO_SCREEN_FADE_OUT(500)
//					STOP_CUTSCENE()
//					bEndCutsceneSkipped = TRUE
//					iLesterCutProg++
//					#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_LESTER_MOCAP] Cut skipped iLesterCutProg = ", iLesterCutProg) #ENDIF
//	            ENDIF
			ELSE
				iLesterCutProg++
				#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_LESTER_MOCAP] iLesterCutProg = ", iLesterCutProg) #ENDIF
			ENDIF
		BREAK
		
		CASE 4
			IF bEndCutsceneSkipped
				IF NOT IS_CUTSCENE_ACTIVE()
					IF IS_SCREEN_FADED_OUT()
					OR IS_SCREEN_FADING_OUT()
						DO_SCREEN_FADE_IN(500)
					ENDIF
					bEndCutsceneSkipped = FALSE
				ENDIF
			ELSE
				IF HAS_CUTSCENE_FINISHED()
					CLEANUP_MP_CUTSCENE()
					
					
					SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
				//	NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					NETWORK_SET_VOICE_ACTIVE(TRUE)
					
					IF IS_BIT_SET(iBoolsBitSet, biL_SetInvicible)
						SET_PLAYER_INVINCIBLE(PLAYER_ID(), FALSE)
					ENDIF
					#IF IS_DEBUG_BUILD NET_DW_PRINT("NETWORK_SET_VOICE_ACTIVE - TRUE") #ENDIF
					IF interiorLester <> NULL
						UNPIN_INTERIOR(interiorLester)
					ENDIF
					CLEAR_BIT(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_WantToSeeLester)
					
					REQUEST_SYSTEM_ACTIVITY_TYPE_MET_LESTER()
					
					iLesterCutProg++
					#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_LESTER_MOCAP] iLesterCutProg = ", iLesterCutProg) #ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 5
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	IF NOT IS_BIT_SET(iBoolsBitSet, biL_SetEndCutPos)
		 IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("MP_1")
             //Do stuff.
            IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<1275.2192, -1722.5026, 53.6550>>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 228.4235)
				SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
				
				SET_BIT(iBoolsBitSet, biL_SetEndCutPos)
				#IF IS_DEBUG_BUILD NET_DW_PRINT("Set player pos at cutscene end") #ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(iBoolsBitSet, biL_SetCamExitState)
		IF iLesterCutProg > 2
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
					SET_BIT(iBoolsBitSet, biL_SetCamExitState)
					#IF IS_DEBUG_BUILD NET_DW_PRINT("Set player visible at cutscene end") #ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	IF IS_BIT_SET(iBoolsBitSet, biL_DisabledWeaponSelect)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_NEXT) 
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_PREV)

	ENDIF
	RETURN FALSE
ENDFUNC




//FUNC BOOL DO_ARRIVE_LESTER_CUT()
//	SWITCH iArriveLesterCutProg
//		CASE 0
//			IF NOT IS_BIT_SET(iBoolsBitSet, biL_SetInvicible)
//				#IF IS_DEBUG_BUILD NET_DW_PRINT("[DO_ARRIVE_LESTER_CUT] Set invincible Case 0 ") #ENDIF
//				SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
//				SET_BIT(iBoolsBitSet, biL_SetInvicible)
//			ENDIF
//			
//			scaleLester = REQUEST_SCALEFORM_MOVIE("SECURITY_CAM")
//			SET_TRANSITION_SESSIONS_CLEAN_UP_ON_CALL_NOW_IF_NEEDED() // 1854634
//			iArriveLesterCutProg++
//			#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_ARRIVE_LESTER_CUT] iArriveLesterCutProg = ", iArriveLesterCutProg) #ENDIF
//		BREAK
//		
//		CASE 1
//			IF HAS_SCALEFORM_MOVIE_LOADED(scaleLester)
//				DISABLE_KILL_YOURSELF_OPTION()
//				
//				// (BOOL bLeavePedCopyBehind=TRUE, BOOL bPlayerInvisible = TRUE, BOOL bPlayerHasCollision = TRUE, BOOL bPlayerFrozen = FALSE
//				MAKE_PLAYER_SAFE_FOR_MP_CUTSCENE(FALSE, FALSE, TRUE, FALSE ) 
//				SET_LOCAL_PLAYER_VISIBLE_IN_CUTSCENE(TRUE)
//				START_MP_CUTSCENE()
//				
//				//  bool bHasControl, BOOL bVisible = TRUE, BOOL bClearTasks = TRUE, BOOL bHasCollision = FALSE
//				NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_CLEAR_TASKS ) 
//				
//				
//				
//				/*
//				MAKE_PLAYER_SAFE_FOR_MP_CUTSCENE(FALSE, FALSE, TRUE, FALSE )
//				
//				NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, FALSE, TRUE, TRUE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
//				SET_LOCAL_PLAYER_VISIBLE_IN_CUTSCENE(TRUE, FALSE)
//				START_MP_CUTSCENE(FALSE)
//				
//				*/
//				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//					#IF IS_DEBUG_BUILD NET_DW_PRINT("[DO_ARRIVE_LESTER_CUT] Set invincible Case 1 ") #ENDIF
//					SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
//					SET_BIT(iBoolsBitSet, biL_SetInvicible)
//					CLEAR_AREA(<<1273.8845, -1718.0037, 53.7715>>, 3.0, TRUE)
//					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
//				//	SET_ENTITY_COORDS(PLAYER_PED_ID(),  <<1275.3485, -1722.6693, 53.6550>>)
//					SET_ENTITY_COORDS(PLAYER_PED_ID(),   <<1275.2563, -1722.3683, 53.6550>>)
//					SET_ENTITY_HEADING(PLAYER_PED_ID(), 12.6638)
//					
//				//	TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(),<<1274.8942, -1721.1792, 53.6807>>, PEDMOVE_WALK)
//				
//					FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK)
//					FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
//					
//					SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
//				ENDIF
//				camArriveLester = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE) 
//				SET_CAM_ACTIVE(camArriveLester, TRUE)
//			//	SET_CAM_PARAMS(camArriveLester,<<1276.4088, -1719.9183, 56.1054>>, <<-37.8168, -0.0000, 137.9132>>, 50.0000)
//				SET_CAM_PARAMS(camArriveLester,<<1276.4088, -1719.9183, 56.1054>>, <<-29.9320, -0.0000, 137.9132>>, 50.0000)
//				RENDER_SCRIPT_CAMS(TRUE, FALSE)
//				
//				BEGIN_SCALEFORM_MOVIE_METHOD(scaleLester, "SET_DETAILS")
//					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("FM_LEST_CAM")
//				END_SCALEFORM_MOVIE_METHOD()
//				
//				BEGIN_SCALEFORM_MOVIE_METHOD(scaleLester, "SET_LOCATION")
//					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("FM_LEST_CAM2")
//				END_SCALEFORM_MOVIE_METHOD()
//					
//				IF GET_TIMECYCLE_MODIFIER_INDEX() = -1
//					SET_TIMECYCLE_MODIFIER("scanline_cam")
//				ENDIF
//				
//				SET_BIT(iBoolsBitSet, biL_DrawScaleform)
//				START_NET_TIMER(timeArriveLesterCut)
//				
//				REQUEST_MODEL(GET_NPC_PED_MODEL(CHAR_LESTER))
//				REQUEST_MODEL(Prop_WheelChair_01_S)
//				REQUEST_ANIM_DICT("MissLester1ALeadInOut")
//				
//			//	SET_BIT(iBoolsBitSet, biL_SetDoorResetFlags)
//				SET_BIT(iBoolsBitSet, biL_CreateLester)
//				IF NOT IS_BIT_SET(iBoolsBitSet, biL_UnlockedDoor)
//					IF IS_DOOR_REGISTERED_WITH_SYSTEM(MP_DOOR_CS_ONLY_INT_LESTER)
//						DOOR_SYSTEM_SET_DOOR_STATE(MP_DOOR_CS_ONLY_INT_LESTER, DOORSTATE_UNLOCKED, FALSE)
//						SET_BIT(iBoolsBitSet, biL_UnlockedDoor)
//						#IF IS_DEBUG_BUILD NET_DW_PRINT("unlocked door") #ENDIF
//					ENDIF
//				ENDIF
//		//		REQUEST_CUTSCENE(sLesterCut)
//				
//				iArriveLesterCutProg = 2
//				#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_ARRIVE_LESTER_CUT] iArriveLesterCutProg = ", iArriveLesterCutProg) #ENDIF
//			ENDIF
//		BREAK
//		
//		CASE 2
//			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//				IF NOT IS_BIT_SET(iBoolsBitSet, biL_GivenLookAt)
//					IF HAS_NET_TIMER_EXPIRED(timeArriveLesterCut, 1000)
//						TASK_LOOK_AT_COORD(PLAYER_PED_ID(), <<1276.4088, -1719.9183, 56.1054>>, -1)
//						SET_BIT(iBoolsBitSet, biL_GivenLookAt)
//					ENDIF
//				ENDIF
//			ENDIF
//			
//			
//			IF HAS_NET_TIMER_EXPIRED(timeArriveLesterCut, 1500)
//			#IF IS_DEBUG_BUILD OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RIGHT) #ENDIF
//				IF IS_BIT_SET(iBoolsBitSet, biL_UnlockedDoor)
//					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//						IF NOT IS_PED_INJURED(pedLester)
//					//		IF CREATE_CONVERSATION (speechFmLesterCut, "LES1A", "LES1A_BRAC", CONV_PRIORITY_MEDIUM)
//					//		IF PLAY_SINGLE_LINE_FROM_CONVERSATION(speechFmLesterCut, "LS1AAUD", "LES1A_INTL2", "LES1A_INTL2_1", CONV_PRIORITY_MEDIUM)
//							IF CREATE_CONVERSATION (speechFmLesterCut, "FM_1AU", "FM_LESTCUT", CONV_PRIORITY_MEDIUM)
////								SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_OpenDoorArmIK, TRUE)
////								SET_BIT(iBoolsBitSet, biL_SetDoorResetFlags)
////								TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
////								TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(),  <<1273.5762, -1718.4594, 53.7715>>, PEDMOVE_WALK)
////								
//								#IF IS_DEBUG_BUILD
//									NET_DW_PRINT("[DO_ARRIVE_LESTER_CUT] Played new dialogue")
//								#ENDIF
//
//								RESET_NET_TIMER(timeArriveLesterCut)
//								START_NET_TIMER(timeArriveLesterCut)
//								
//								iArriveLesterCutProg++
//								#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_ARRIVE_LESTER_CUT] iArriveLesterCutProg = ", iArriveLesterCutProg) #ENDIF
//							ENDIF
//						ENDIF
//					ELSE
//						#IF IS_DEBUG_BUILD NET_DW_PRINT("[DO_ARRIVE_LESTER_CUT] Case 2 Player is injured ") #ENDIF
//					ENDIF
//					
//				ELSE 
//					#IF IS_DEBUG_BUILD NET_DW_PRINT("[DO_ARRIVE_LESTER_CUT] Waitning for door to unlock ") #ENDIF
//				ENDIF
//				
//			ENDIF
//		BREAK
//		
//		CASE 3
//			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//					SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_OpenDoorArmIK, TRUE)
//					SET_BIT(iBoolsBitSet, biL_SetDoorResetFlags)
//					TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
//					TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(),  <<1273.5762, -1718.4594, 53.7715>>, PEDMOVE_WALK)
//					
//					RESET_NET_TIMER(timeArriveLesterCut)
//					START_NET_TIMER(timeArriveLesterCut)
//					
//					iArriveLesterCutProg++
//					#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_ARRIVE_LESTER_CUT] iArriveLesterCutProg = ", iArriveLesterCutProg) #ENDIF
//						
//				ENDIF
//			ENDIF
//		BREAK
//		
//		CASE 4
//			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//				IF HAS_NET_TIMER_EXPIRED(timeArriveLesterCut, 5000)
//				OR IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<1275.137817,-1718.425537,53.729942>>, <<1272.450684,-1719.626709,55.458969>>, 1.562500)
//				#IF IS_DEBUG_BUILD OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RIGHT) #ENDIF
//				//	IF CREATE_LESTER_PED() 
//
////						SET_ENTITY_COORDS(PLAYER_PED_ID(), <<1274.2146, -1719.6804, 53.7715>>)
////						SET_ENTITY_HEADING(PLAYER_PED_ID(),  6.9293)
//						SET_ENTITY_COORDS(PLAYER_PED_ID(), <<1273.8845, -1718.0037, 53.7715>>)
//						SET_ENTITY_HEADING(PLAYER_PED_ID(),  21.8709)
//						SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
//					//	FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK)
//					//	FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
//						CLEANUP_LESTER_ARRIVE_CUT()
//						CLEANUP_MP_CUTSCENE()
//						CLEAR_BIT(iBoolsBitSet, biL_SetDoorResetFlags)
//						SET_GAMEPLAY_CAM_RELATIVE_HEADING()
//						SET_GAMEPLAY_CAM_RELATIVE_PITCH()
//						NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//						
//						SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
//						SET_BIT(iBoolsBitSet, biL_SetInvicible)
//						
//						SET_STORE_ENABLED(FALSE)
//						SET_BIT(iBoolsBitSet, biL_StoreDisabled)
//						iArriveLesterCutProg = 99
//						#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_ARRIVE_LESTER_CUT] iArriveLesterCutProg = ", iArriveLesterCutProg) #ENDIF
//						
//				//	ENDIF
//				ENDIF
//			ENDIF
//		BREAK
//		CASE 99
//			IF IS_DOOR_REGISTERED_WITH_SYSTEM(MP_DOOR_CS_ONLY_INT_LESTER)
//				DOOR_SYSTEM_SET_DOOR_STATE(MP_DOOR_CS_ONLY_INT_LESTER, DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE)
//				
//				SET_BIT(iBoolsBitSet, biL_DisabledWeaponSelect)
//				iArriveLesterCutProg = 100
//				#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_ARRIVE_LESTER_CUT] iArriveLesterCutProg = ", iArriveLesterCutProg) #ENDIF
//			ENDIF
//		BREAK
//		
//		CASE 100
//			RETURN TRUE
//		BREAK
//	ENDSWITCH
//	
//	IF IS_BIT_SET(iBoolsBitSet, biL_DrawScaleform)
//		IF HAS_SCALEFORM_MOVIE_LOADED(scaleLester)
//			
//			BEGIN_SCALEFORM_MOVIE_METHOD(scaleLester, "SET_TIME")
//			IF GET_CLOCK_HOURS() >= 0 AND GET_CLOCK_HOURS() <= 12
//				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CLOCK_HOURS())
//			ELSE
//				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CLOCK_HOURS() - 12)
//			ENDIF
//			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CLOCK_MINUTES())
//			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(00)
//			IF GET_CLOCK_HOURS() >= 0 AND GET_CLOCK_HOURS() < 12
//				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("FM_LEST_AM")
//			ELSE
//				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("FM_LEST_PM")
//			ENDIF
//			END_SCALEFORM_MOVIE_METHOD()
//			DRAW_SCALEFORM_MOVIE_FULLSCREEN(scaleLester, 255, 255, 255, 255)
//		ELSE
//			#IF IS_DEBUG_BUILD NET_DW_PRINT("Still waiting for scaleform!") #ENDIF
//		ENDIF
//	ENDIF
//	
//	IF IS_BIT_SET(iBoolsBitSet, biL_SetDoorResetFlags)
//		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_OpenDoorArmIK, TRUE)
//			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_SearchForClosestDoor, TRUE)
//			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_OpenDoorArmIK, TRUE)
//			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_AllowOpenDoorIkBeforeFullMovement , TRUE)
//		ENDIF
//	ENDIF
//	
//	IF IS_BIT_SET(iBoolsBitSet, biL_CreateLester)
//		IF CREATE_LESTER_PED()	
//			CLEAR_BIT(iBoolsBitSet, biL_CreateLester)
//		ENDIF
//	ENDIF
//	
//	RETURN FALSE
//ENDFUNC



FUNC BOOL DO_ARRIVE_LESTER_CUT_V2()
//	PRINTLN("[DO_ARRIVE_LESTER_CUT] Running iArriveLesterCutProg = ", iArriveLesterCutProg)
	SWITCH iArriveLesterCutProg
//		CASE 0
//		//	scaleLester = REQUEST_SCALEFORM_MOVIE("SECURITY_CAM")
//			SET_TRANSITION_SESSIONS_CLEAN_UP_ON_CALL_NOW_IF_NEEDED() // 1854634
//			iArriveLesterCutProg++
//			#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_ARRIVE_LESTER_CUT] iArriveLesterCutProg = ", iArriveLesterCutProg) #ENDIF
//		BREAK
		
		CASE 0
			IF NOT IS_BIT_SET(iBoolsBitSet, biL_SetInvicible)
				#IF IS_DEBUG_BUILD NET_DW_PRINT("[DO_ARRIVE_LESTER_CUT] Set invincible Case 0 ") #ENDIF
				SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
				SET_BIT(iBoolsBitSet, biL_SetInvicible)
			ELSE
				#IF IS_DEBUG_BUILD NET_DW_PRINT("[DO_ARRIVE_LESTER_CUT] Case 0 biL_SetInvicible already set!") #ENDIF
			ENDIF
			
			IF HAS_SCALEFORM_MOVIE_LOADED(scaleLester)
			AND CREATE_PLAYER_PED_COPY_FOR_LESTER_CUT()
				SET_TRANSITION_SESSIONS_CLEAN_UP_ON_CALL_NOW_IF_NEEDED() // 1854634
				
				DISABLE_KILL_YOURSELF_OPTION()
				
				// (BOOL bLeavePedCopyBehind=TRUE, BOOL bPlayerInvisible = TRUE, BOOL bPlayerHasCollision = TRUE, BOOL bPlayerFrozen = FALSE
				MAKE_PLAYER_SAFE_FOR_MP_CUTSCENE(FALSE, FALSE, TRUE, FALSE ) 
				
				//  bool bHasControl, BOOL bVisible = TRUE, BOOL bClearTasks = TRUE, BOOL bHasCollision = FALSE
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_SET_INVISIBLE | NSPC_CLEAR_TASKS )
				
				SET_LOCAL_PLAYER_VISIBLE_IN_CUTSCENE(FALSE, TRUE)
				
				NETWORK_FADE_OUT_ENTITY(PLAYER_PED_ID(), TRUE, TRUE)
				START_MP_CUTSCENE()
				
				
				
				
				
				IF NOT IS_PED_INJURED(pedPlayer)
					CLEAR_AREA(<<1273.8845, -1718.0037, 53.7715>>, 3.0, TRUE)
					CLEAR_PED_TASKS_IMMEDIATELY(pedPlayer)

					SET_ENTITY_COORDS(pedPlayer,   <<1275.2563, -1722.3683, 53.6550>>)
					SET_ENTITY_HEADING(pedPlayer, 12.6638)
					

				
					FORCE_PED_MOTION_STATE(pedPlayer, MS_ON_FOOT_WALK)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
					
					SET_CURRENT_PED_WEAPON(pedPlayer, WEAPONTYPE_UNARMED, TRUE)
				ENDIF
				camArriveLester = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE) 
				SET_CAM_ACTIVE(camArriveLester, TRUE)

				SET_CAM_PARAMS(camArriveLester,<<1276.4088, -1719.9183, 56.1054>>, <<-29.9320, -0.0000, 137.9132>>, 50.0000)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				
				BEGIN_SCALEFORM_MOVIE_METHOD(scaleLester, "SET_DETAILS")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("FM_LEST_CAM")
				END_SCALEFORM_MOVIE_METHOD()
				
				BEGIN_SCALEFORM_MOVIE_METHOD(scaleLester, "SET_LOCATION")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("FM_LEST_CAM2")
				END_SCALEFORM_MOVIE_METHOD()
					
				IF GET_TIMECYCLE_MODIFIER_INDEX() = -1
					SET_TIMECYCLE_MODIFIER("scanline_cam")
				ENDIF
				
				SET_BIT(iBoolsBitSet, biL_DrawScaleform)
				START_NET_TIMER(timeArriveLesterCut)
				
				REQUEST_MODEL(GET_NPC_PED_MODEL(CHAR_LESTER))
				REQUEST_MODEL(Prop_WheelChair_01_S)
				REQUEST_ANIM_DICT("MissLester1ALeadInOut")
				

				SET_BIT(iBoolsBitSet, biL_CreateLester)
				IF NOT IS_BIT_SET(iBoolsBitSet, biL_UnlockedDoor)
					IF IS_DOOR_REGISTERED_WITH_SYSTEM(MP_DOOR_CS_ONLY_INT_LESTER)
						DOOR_SYSTEM_SET_DOOR_STATE(MP_DOOR_CS_ONLY_INT_LESTER, DOORSTATE_UNLOCKED, FALSE)
						SET_BIT(iBoolsBitSet, biL_UnlockedDoor)
						#IF IS_DEBUG_BUILD NET_DW_PRINT("unlocked door") #ENDIF
					ENDIF
				ENDIF

				
				iArriveLesterCutProg = 1
				#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_ARRIVE_LESTER_CUT] iArriveLesterCutProg = ", iArriveLesterCutProg) #ENDIF
			ELSE
				// Waiting for assets
				#IF IS_DEBUG_BUILD
					IF NOT HAS_SCALEFORM_MOVIE_LOADED(scaleLester)
						NET_DW_PRINT("Waiting for scaleform")
					ENDIF
					
					IF NOT CREATE_PLAYER_PED_COPY_FOR_LESTER_CUT()
						NET_DW_PRINT("Waiting for clone")
					ENDIF
				#ENDIF
			ENDIF
		BREAK
		
		CASE 1
			IF NOT IS_PED_INJURED(pedPlayer)
				IF NOT IS_BIT_SET(iBoolsBitSet, biL_GivenLookAt)
					IF HAS_NET_TIMER_EXPIRED(timeArriveLesterCut, 1000)
						TASK_LOOK_AT_COORD(pedPlayer, <<1276.4088, -1719.9183, 56.1054>>, -1)
						SET_BIT(iBoolsBitSet, biL_GivenLookAt)
					ENDIF
				ENDIF
			ENDIF
			
			
			IF HAS_NET_TIMER_EXPIRED(timeArriveLesterCut, 1500)
			#IF IS_DEBUG_BUILD OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RIGHT) #ENDIF
				IF IS_BIT_SET(iBoolsBitSet, biL_UnlockedDoor)
					IF NOT IS_PED_INJURED(pedPlayer)
						IF NOT IS_PED_INJURED(pedLester)
					//		IF CREATE_CONVERSATION (speechFmLesterCut, "LES1A", "LES1A_BRAC", CONV_PRIORITY_MEDIUM)
					//		IF PLAY_SINGLE_LINE_FROM_CONVERSATION(speechFmLesterCut, "LS1AAUD", "LES1A_INTL2", "LES1A_INTL2_1", CONV_PRIORITY_MEDIUM)
							IF CREATE_CONVERSATION (speechFmLesterCut, "FM_1AU", "FM_LESTCUT", CONV_PRIORITY_MEDIUM)
//								SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_OpenDoorArmIK, TRUE)
//								SET_BIT(iBoolsBitSet, biL_SetDoorResetFlags)
//								TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
//								TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(),  <<1273.5762, -1718.4594, 53.7715>>, PEDMOVE_WALK)
//								
								#IF IS_DEBUG_BUILD
									NET_DW_PRINT("[DO_ARRIVE_LESTER_CUT] Played new dialogue")
								#ENDIF
								
								RESET_NET_TIMER(timeArriveLesterCut)
								START_NET_TIMER(timeArriveLesterCut)
								
								iArriveLesterCutProg++
								#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_ARRIVE_LESTER_CUT] iArriveLesterCutProg = ", iArriveLesterCutProg) #ENDIF
							ENDIF
						ELSE
							#IF IS_DEBUG_BUILD NET_DW_PRINT("[DO_ARRIVE_LESTER_CUT] Waitning for IS_PED_INJURED(pedLester) ") #ENDIF
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD NET_DW_PRINT("[DO_ARRIVE_LESTER_CUT] Waitning for IS_PED_INJURED(pedPlayer) ") #ENDIF
					ENDIF
					
				ELSE 
					#IF IS_DEBUG_BUILD NET_DW_PRINT("[DO_ARRIVE_LESTER_CUT] Waitning for door to unlock ") #ENDIF
				ENDIF
				
			ENDIF
		BREAK
		
		CASE 2
			IF NOT IS_PED_INJURED(pedPlayer)
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					SET_PED_CONFIG_FLAG(pedPlayer, PCF_OpenDoorArmIK, TRUE)
					SET_BIT(iBoolsBitSet, biL_SetDoorResetFlags)
					TASK_CLEAR_LOOK_AT(pedPlayer)
					TASK_FOLLOW_NAV_MESH_TO_COORD(pedPlayer,  <<1273.5762, -1718.4594, 53.7715>>, PEDMOVE_WALK)
					
					RESET_NET_TIMER(timeArriveLesterCut)
					START_NET_TIMER(timeArriveLesterCut)
					
					iArriveLesterCutProg++
					#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_ARRIVE_LESTER_CUT] iArriveLesterCutProg = ", iArriveLesterCutProg) #ENDIF
					
				ELSE
					#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_ARRIVE_LESTER_CUT] - IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() = TRUE - iArriveLesterCutProg = ", iArriveLesterCutProg) #ENDIF
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_ARRIVE_LESTER_CUT] - IS_PED_INJURED(pedPlayer) = TRUE - iArriveLesterCutProg = ", iArriveLesterCutProg) #ENDIF
			ENDIF
		BREAK
		
		CASE 3
			IF NOT IS_PED_INJURED(pedPlayer)
				IF HAS_NET_TIMER_EXPIRED(timeArriveLesterCut, 5000)
				OR IS_ENTITY_IN_ANGLED_AREA( pedPlayer, <<1275.137817,-1718.425537,53.729942>>, <<1272.450684,-1719.626709,55.458969>>, 1.562500)
				#IF IS_DEBUG_BUILD OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RIGHT) #ENDIF
				//	IF CREATE_LESTER_PED() 

						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							SET_ENTITY_COORDS(PLAYER_PED_ID(), <<1273.8845, -1718.0037, 53.7715>>)
							SET_ENTITY_HEADING(PLAYER_PED_ID(),  21.8709)
							SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
							IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON

								FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_AIMING)
								FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
								#IF IS_DEBUG_BUILD NET_DW_PRINT("[DO_ARRIVE_LESTER_CUT] Forced update") #ENDIF
							ENDIF
						ENDIF

						CLEANUP_LESTER_ARRIVE_CUT()
						CLEANUP_MP_CUTSCENE()
						CLEAR_BIT(iBoolsBitSet, biL_SetDoorResetFlags)
						SET_GAMEPLAY_CAM_RELATIVE_HEADING()
						SET_GAMEPLAY_CAM_RELATIVE_PITCH()
						NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						
						SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
						SET_BIT(iBoolsBitSet, biL_SetInvicible)
						
						SET_STORE_ENABLED(FALSE)
						SET_BIT(iBoolsBitSet, biL_StoreDisabled)
						iArriveLesterCutProg = 99
						#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_ARRIVE_LESTER_CUT] iArriveLesterCutProg = ", iArriveLesterCutProg) #ENDIF
						
				//	ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 99
			IF IS_DOOR_REGISTERED_WITH_SYSTEM(MP_DOOR_CS_ONLY_INT_LESTER)
				DOOR_SYSTEM_SET_DOOR_STATE(MP_DOOR_CS_ONLY_INT_LESTER, DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE)
				
				SET_BIT(iBoolsBitSet, biL_DisabledWeaponSelect)
				iArriveLesterCutProg = 100
				#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_ARRIVE_LESTER_CUT] iArriveLesterCutProg = ", iArriveLesterCutProg) #ENDIF
			ENDIF
		BREAK
		
		CASE 100
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	IF IS_BIT_SET(iBoolsBitSet, biL_DrawScaleform)
		IF HAS_SCALEFORM_MOVIE_LOADED(scaleLester)
			
			BEGIN_SCALEFORM_MOVIE_METHOD(scaleLester, "SET_TIME")
			IF GET_CLOCK_HOURS() >= 0 AND GET_CLOCK_HOURS() <= 12
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CLOCK_HOURS())
			ELSE
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CLOCK_HOURS() - 12)
			ENDIF
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CLOCK_MINUTES())
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(00)
			IF GET_CLOCK_HOURS() >= 0 AND GET_CLOCK_HOURS() < 12
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("FM_LEST_AM")
			ELSE
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("FM_LEST_PM")
			ENDIF
			END_SCALEFORM_MOVIE_METHOD()
			DRAW_SCALEFORM_MOVIE_FULLSCREEN(scaleLester, 255, 255, 255, 255)
		ELSE
			#IF IS_DEBUG_BUILD NET_DW_PRINT("Still waiting for scaleform!") #ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iBoolsBitSet, biL_SetDoorResetFlags)
		IF NOT IS_PED_INJURED(pedPlayer)
			SET_PED_CONFIG_FLAG(pedPlayer, PCF_OpenDoorArmIK, TRUE)
			SET_PED_RESET_FLAG(pedPlayer, PRF_SearchForClosestDoor, TRUE)
			SET_PED_RESET_FLAG(pedPlayer, PRF_OpenDoorArmIK, TRUE)
			SET_PED_RESET_FLAG(pedPlayer, PRF_AllowOpenDoorIkBeforeFullMovement , TRUE)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iBoolsBitSet, biL_CreateLester)
		IF CREATE_LESTER_PED()	
			CLEAR_BIT(iBoolsBitSet, biL_CreateLester)
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_LESTER_CUTSCENE_CLIENT()	
	#IF IS_DEBUG_BUILD
		DO_LESTER_CUTSCENE_J_SKIPS()
	#ENDIF
	SWITCH PlayerBD[PARTICIPANT_ID_TO_INT()].iLesterCutProg
		CASE 0
			IF NOT IS_BIT_SET(iBoolsBitSet, biL_ToldToGoToLester)
				IF NOT IS_BIT_SET(iBoolsBitSet, biL_TextCleared)
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF NOT IS_CELLPHONE_CONVERSATION_PLAYING() //IS_MOBILE_PHONE_CALL_ONGOING()
							IF NOT Is_MP_Comms_Still_Playing()
								IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
									IF NOT IS_PHONE_ONSCREEN()
										IF NOT IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
											IF NOT IS_FM_MISSION_LAUNCH_IN_PROGRESS()
												IF GET_LOCAL_PLAYER_CORONA_POS_STATE() = 0
													
											//		Print_Objective_Text("FM_LCUT_GOL") // Go to Lester's ~y~house.
//													g_blipLesterHouse = ADD_BLIP_FOR_COORD(g_vLesterHouse)
//											
//													SET_BLIP_SPRITE(g_blipLesterHouse, RADAR_TRACE_LESTER_FAMILY)
//													SET_BIT(iBoolsBitSet, biL_ToldToGoToLester)
//													#IF IS_DEBUG_BUILD NET_DW_PRINT("Added Lester house blip") #ENDIF
													//		SET_BLIP_ROUTE(g_blipLesterHouse, TRUE)
													
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ELSE
								#IF IS_DEBUG_BUILD NET_DW_PRINT("Mp comms on going") #ENDIF
							ENDIF
						ELSE
							#IF IS_DEBUG_BUILD NET_DW_PRINT("Phonecall on going") #ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT IS_BIT_SET(iBoolsBitSet, biL_TextCleared)
				
				IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
				OR IS_FM_MISSION_LAUNCH_IN_PROGRESS()
				OR GET_LOCAL_PLAYER_CORONA_POS_STATE() > 0
				OR IS_TRANSITION_ACTIVE()	
				OR SHOULD_UI_BE_HIDDEN()
				OR SHOULD_HIDE_ALL_JOB_BLIPS_FOR_YACHT()
				OR ARE_FLOW_MESSAGES_SUPPRESSED_IN_CURRENT_INTERIOR()
					SET_BIT(iBoolsBitSet, biL_TextCleared)
					Clear_Any_Objective_Text_From_This_Script()
					IF DOES_BLIP_EXIST(g_blipLesterHouse)
						SET_BLIP_ROUTE(g_blipLesterHouse, FALSE)
						REMOVE_BLIP(g_blipLesterHouse)
						
					ENDIF
				//	IF REQUESTED_LESTER_CUTSCENE()
				//		REMOVE_CUTSCENE()
				//	ENDIF
					#IF IS_DEBUG_BUILD NET_DW_PRINT("Setting biL_TextCleared as player on mission / launch in progress / SHOULD_UI_BE_HIDDEN()") #ENDIF
					
				ELSE	
//					IF IS_MP_PASSIVE_MODE_ENABLED()
//						IF IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
//							IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
//								PRINT_HELP("FM_IHELP_NPA")
//							ENDIF
//						ENDIF
//					ENDIF
				//	IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
						IF DOES_BLIP_EXIST(g_blipLesterHouse)
							IF NOT IS_BIT_SET(iBoolsBitSet, biL_LesterBlipInactive)					
								IF serverbd.iPartWithLester <> -1
									IF serverbd.iPartWithLester <> PARTICIPANT_ID_TO_INT()
										SET_BLIP_COLOUR(g_blipLesterHouse ,BLIP_COLOUR_INACTIVE_MISSION)
										SET_BLIP_SCALE(g_blipLesterHouse, BLIP_SIZE_NETWORK_PED)
										SET_BIT(iBoolsBitSet, biL_LesterBlipInactive)
								//		SET_BLIP_PRIORITY(g_blipLesterHouse, BLIPPRIORITY_HIGH)
										#IF IS_DEBUG_BUILD NET_DW_PRINT("Setting biL_LesterBlipInactive as LESTER busy") #ENDIF
									ENDIF
								ENDIF
							ELSE
								IF serverbd.iPartWithLester = -1
								OR serverbd.iPartWithLester = PARTICIPANT_ID_TO_INT()
									SET_BLIP_COLOUR(g_blipLesterHouse ,BLIP_COLOUR_DEFAULT)
									SET_BLIP_SCALE(g_blipLesterHouse, BLIP_SIZE_NETWORK_COORD)
									CLEAR_BIT(iBoolsBitSet, biL_LesterBlipInactive)
									#IF IS_DEBUG_BUILD NET_DW_PRINT("Clearing biL_LesterBlipInactive as LESTER no longer busy") #ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						IF IS_BIT_SET(iBoolsBitSet, biL_ToldToLoseCops)
							CLEAR_BIT(iBoolsBitSet, biL_ToldToLoseCops)
						ENDIF
						IF HAS_PLAYER_REACHED_LESTERS_HOUSE()
							IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
								IF bDoMocap AND HAS_CUTSCENE_LOADED()
								OR NOT bDoMocap
									IF NOT IS_BIT_SET(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_WantToSeeLester)
										SET_BIT(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_WantToSeeLester)
										#IF IS_DEBUG_BUILD NET_DW_PRINT("Setting I want to see Lester") #ENDIF
									ENDIF
									
									IF IS_BIT_SET(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_WantToSeeLester)
										IF serverbd.iPartWithLester = PARTICIPANT_ID_TO_INT()
											Clear_Any_Objective_Text_From_This_Script()
											IF DOES_BLIP_EXIST(g_blipLesterHouse)
												SET_BLIP_ROUTE(g_blipLesterHouse, FALSE)
												REMOVE_BLIP(g_blipLesterHouse)
												#IF IS_DEBUG_BUILD NET_DW_PRINT("removed Lester house blip as I'm in house") #ENDIF
											ENDIF
											CLEAR_HELP()
											SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_LESTER_CUTSCENE, TRUE)
											SET_TUTORIAL_INVITES_ARE_BLOCKED()
											//MAKE_PLAYER_SAFE_FOR_MP_CUTSCENE(FALSE, FALSE)
											SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
											SET_BIT(iBoolsBitSet, biL_SetInvicible)
											CLEAR_AREA(<<1274.8540, -1721.1538, 53.6808>>, 2.0, TRUE, DEFAULT, DEFAULT, TRUE)
											IF bDoPlayerFade
												CREATE_PLAYER_PED_COPY_FOR_LESTER_CUT()
												scaleLester = REQUEST_SCALEFORM_MOVIE("SECURITY_CAM")
											ENDIF

											
											PlayerBD[PARTICIPANT_ID_TO_INT()].iLesterCutProg = 1
											#IF IS_DEBUG_BUILD NET_DW_PRINT("iLesterCutProg = 1 as I've reached Lester's house. Blocking invites") #ENDIF
										ELSE
											IF serverbd.iPartWithLester <> -1
												IF NOT IS_BIT_SET(iBoolsBitSet, biL_LesterBusy)
													IF IS_OK_TO_PRINT_FREEMODE_HELP()
														PRINT_HELP("FM_LCUT_LRB")
														#IF IS_DEBUG_BUILD NET_DW_PRINT("Doing LESTER busy help") #ENDIF
														SET_BIT(iBoolsBitSet, biL_LesterBusy)
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
									IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM_LCUT_LCP")
										PRINT_HELP("FM_LCUT_LCP") // Lose the cops
									ENDIF
								ENDIF
							ENDIF
						ELSE
							//-- Blip help
							IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneBlipHelp)
								IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE)
									IF NOT IS_TRANSITION_ACTIVE()
										PRINT_HELP("FM_LCUT_LBLP") // Lester's house is marked on the map
										IF NOT DOES_BLIP_EXIST(g_blipLesterHouse )
											g_blipLesterHouse = ADD_BLIP_FOR_COORD(g_vLesterHouse)
									
											SET_BLIP_SPRITE(g_blipLesterHouse, RADAR_TRACE_LESTER_FAMILY)
											SET_BLIP_FLASHES(g_blipLesterHouse, TRUE)
											SET_BLIP_FLASH_TIMER(g_blipLesterHouse, 7000)
											#IF IS_DEBUG_BUILD NET_DW_PRINT("Added Lester house blip") #ENDIF
										ENDIF
										SET_BIT(iBoolsBitSet, biL_ToldToGoToLester)
										
										SET_BIT(iBoolsBitSet, biL_DoneBlipHelp)
										#IF IS_DEBUG_BUILD NET_DW_PRINT("Set biL_DoneBlipHelp") #ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							IF IS_BIT_SET(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_WantToSeeLester)
								CLEAR_BIT(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_WantToSeeLester)
								#IF IS_DEBUG_BUILD NET_DW_PRINT("Clearing biP_WantToSeeLester as I'm no longer at Lester's") #ENDIF
							ENDIF
							
							IF IS_BIT_SET(iBoolsBitSet, biL_LesterBusy)

								#IF IS_DEBUG_BUILD NET_DW_PRINT("Clearing biL_LesterBusy as I'm not at Trevor's") #ENDIF
								CLEAR_BIT(iBoolsBitSet, biL_LesterBusy)
								
							ENDIF
						ENDIF

				ENDIF
				
			ELSE
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
						IF IS_SCREEN_FADED_IN()
							IF NOT IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
							AND NOT IS_FM_MISSION_LAUNCH_IN_PROGRESS()
							AND GET_LOCAL_PLAYER_CORONA_POS_STATE() = 0
							AND NOT IS_TRANSITION_ACTIVE()
							AND NOT SHOULD_UI_BE_HIDDEN()
							AND NOT ARE_FLOW_MESSAGES_SUPPRESSED_IN_CURRENT_INTERIOR()
								IF NOT HAS_NET_TIMER_STARTED(timeSinceOnMission)
									START_NET_TIMER(timeSinceOnMission)
									#IF IS_DEBUG_BUILD NET_DW_PRINT("Started timeSinceOnMission ") #ENDIF
								ELSE
									IF HAS_NET_TIMER_EXPIRED(timeSinceOnMission, 10)
										CLEAR_BIT(iBoolsBitSet, biL_TextCleared)
										CLEAR_BIT(iBoolsBitSet, biL_ToldToGoToLester)
										CLEAR_BIT(iBoolsBitSet, biL_RequestedCut)
										CLEAR_BIT(iBoolsBitSet, biL_ToldToLoseCops)
										RESET_NET_TIMER(timeSinceOnMission)
										
										IF NOT DOES_BLIP_EXIST(g_blipLesterHouse)
											g_blipLesterHouse = ADD_BLIP_FOR_COORD(g_vLesterHouse)				
											SET_BLIP_SPRITE(g_blipLesterHouse, RADAR_TRACE_LESTER_FAMILY)
											#IF IS_DEBUG_BUILD NET_DW_PRINT("Re-adding lester blip") #ENDIF
										ENDIF
										
										#IF IS_DEBUG_BUILD NET_DW_PRINT("Clearing biL_TextCleared as player control is on") #ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF bDoMocap
		//		REQUESTED_LESTER_CUTSCENE()
			ENDIF
		BREAK
		
		CASE 1
	/*		IF IS_CUTSCENE_ACTIVE()
				START_NET_TIMER(timeMocapSkip)
				PlayerBD[PARTICIPANT_ID_TO_INT()].iLesterCutProg = 2
				#IF IS_DEBUG_BUILD NET_DW_PRINT("iLesterCutProg = 2 as cutscene is active") #ENDIF
			ENDIF
			*/
			IF bDoMocap
				IF DO_LESTER_MOCAP()
					PlayerBD[PARTICIPANT_ID_TO_INT()].iLesterCutProg = 2
					#IF IS_DEBUG_BUILD NET_DW_PRINT("iLesterCutProg = 2 as Mocap finished") #ENDIF
				ENDIF
			ELSE
				IF DO_ARRIVE_LESTER_CUT_V2()
					PlayerBD[PARTICIPANT_ID_TO_INT()].iLesterCutProg = 2
					#IF IS_DEBUG_BUILD NET_DW_PRINT("iLesterCutProg = 2 as I'm in lester's house") #ENDIF
//					IF IS_SCREEN_FADED_IN()
//						PlayerBD[PARTICIPANT_ID_TO_INT()].iLesterCutProg = 2
//						#IF IS_DEBUG_BUILD NET_DW_PRINT("iLesterCutProg = 2 as cutscene finished") #ENDIF
//					ENDIF
				ENDIF
				
//				IF IS_BIT_SET(iBoolsBitSet, biL_LaunchMocap)
//					IF DO_LESTER_MOCAP_V2()
//						PlayerBD[PARTICIPANT_ID_TO_INT()].iLesterCutProg = 2
//						#IF IS_DEBUG_BUILD NET_DW_PRINT("iLesterCutProg = 2 as Mocap finished") #ENDIF
//					ENDIF
//				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			IF DO_LESTER_MOCAP_V2()
				PlayerBD[PARTICIPANT_ID_TO_INT()].iLesterCutProg = 3
				#IF IS_DEBUG_BUILD NET_DW_PRINT("iLesterCutProg = 3 as cutscene is finished") #ENDIF
			ENDIF
		BREAK
		
		CASE 3
			IF NOT IS_BIT_SET(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DoneCutscene)
				SET_BIT(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DoneCutscene)
				#IF IS_DEBUG_BUILD NET_DW_PRINT("Set biP_DoneCutscene as cutscene finished") #ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	IF SHOULD_LESTER_CUTSCENE_COMPLETE()
		#IF IS_DEBUG_BUILD
			g_bDebugLaunchLesterCut = FALSE
		#ENDIF
		IF DOES_BLIP_EXIST(g_blipLesterHouse)
			REMOVE_BLIP(g_blipLesterHouse)
		ENDIF
		Clear_Any_Objective_Text_From_This_Script()
		//INT iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_CUT_DONE)
		INT iStatInt = g_MP_STAT_FM_CUT_DONE[GET_SLOT_NUMBER(-1)]
		SET_BIT(iStatInt, biFmCut_LesterCutscene)
		SET_BIT(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DoneCutscene)
//		SET_FREEMODE_PROLOGUE_PROFILE_SETTINGS(PSFP_FM_LESTERCUTSCENE)
		SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_FM_CUT_DONE, iStatInt)
		SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_LESTER_CUTSCENE, FALSE)
		IF bDoMocap
			REMOVE_CUTSCENE()
		ENDIF
		CLEAR_TUTORIAL_INVITES_ARE_BLOCKED() 
		SET_FM_UNLOCKS_BIT_SET()
//		REQUEST_SAVE(STAT_SAVETYPE_PROLOGUE)

	ENDIF
ENDPROC





#IF IS_DEBUG_BUILD
	PROC UPDATE_WIDGETS()
		
		IF bWdCreateLester
			REQUEST_MODEL(GET_NPC_PED_MODEL(CHAR_LESTER))
			IF CREATE_LESTER_PED()
				bWdCreateLester = FALSE
			ENDIF
		ENDIF
		
		IF bWDUnlockDoor
			IF IS_DOOR_REGISTERED_WITH_SYSTEM(MP_DOOR_CS_ONLY_INT_LESTER)
				DOOR_SYSTEM_SET_DOOR_STATE(MP_DOOR_CS_ONLY_INT_LESTER, DOORSTATE_UNLOCKED, FALSE)
				bWDUnlockDoor = FALSE
			ENDIF
		ENDIF
		
		IF bWdPlayLine
			IF PLAY_SINGLE_LINE_FROM_CONVERSATION(speechFmLesterCut, "LS1AAUD", "LES1A_INTL2", "LES1A_INTL2_1", CONV_PRIORITY_MEDIUM)
				bWdPlayLine = FALSE
			ENDIF
		ENDIF
	ENDPROC
	
	PROC UPDATE_DEBUG_KEYS()
		
	ENDPROC
#ENDIF
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                      Main Loop                          ///////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

SCRIPT(MP_MISSION_DATA missionScriptArgs)
	
	
	PROCESS_PRE_GAME(missionScriptArgs)	
	
	#IF IS_DEBUG_BUILD
		CREATE_WIDGETS()	
	#ENDIF
	
	
	
	WHILE TRUE
	
		MP_LOOP_WAIT_ZERO()
		
		
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
			SCRIPT_PROFILER_START_OF_FRAME()
		#ENDIF
		#ENDIF		
		
		

		// If we have a match end event, bail.
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
	//	OR NOT IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE)
			#IF IS_DEBUG_BUILD NET_DW_PRINT("About to run cleanup from SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE") #ENDIF
			CLEANUP_SCRIPT()
		ENDIF	
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER( "Cleanup checks")
		#ENDIF
		#ENDIF 
		
		#IF IS_DEBUG_BUILD
			UPDATE_WIDGETS()
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER( "UPDATE_WIDGETS")
			#ENDIF
			
			UPDATE_DEBUG_KEYS()
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER( "UPDATE_DEBUG_KEYS")
			#ENDIF
		#ENDIF
		
		GB_MAKE_BLIPS_SHORT_RANGE(g_blipLesterHouse)
		
// *****************************************************************************************************************************************************************	
// *****************************************************************************************************************************************************************	
//												LOCAL PROCESSING
// *****************************************************************************************************************************************************************	
// *****************************************************************************************************************************************************************
		
		
		// -----------------------------------
		// Process your game logic.....
		SWITCH GET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT())
			
			// Wait untill the server gives the all go before moving on
			CASE GAME_STATE_INI
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_RUNNING
					#IF IS_DEBUG_BUILD NET_DW_PRINT("playerBD[PARTICIPANT_ID_TO_INT()].iGameState GAME_STATE_INI >  GAME_STATE_RUNNING") #ENDIF
					
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY
					
					#IF IS_DEBUG_BUILD NET_DW_PRINT("playerBD[PARTICIPANT_ID_TO_INT()].iGameState GAME_STATE_INI >  GAME_STATE_TERMINATE_DELAY") #ENDIF
					
				ENDIF
			BREAK
			
			// Main gameplay state.
			CASE GAME_STATE_RUNNING
				
				
				
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING	
				//	DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.14, "STRING", "Still Running")
					IF NOT HAS_PLAYER_COMPLETED_LESTER_CUTSCENE(PLAYER_ID())
					#IF IS_DEBUG_BUILD OR g_bDebugLaunchLesterCut #ENDIF
						PROCESS_LESTER_CUTSCENE_CLIENT()
					ELSE	
						IF DOES_BLIP_EXIST(g_blipLesterHouse)
							Clear_Any_Objective_Text_From_This_Script()
						
							SET_BLIP_ROUTE(g_blipLesterHouse, FALSE)
							REMOVE_BLIP(g_blipLesterHouse)
							IF IS_BIT_SET(iBoolsBitSet, biL_RequestedCut)
								REMOVE_CUTSCENE()
							ENDIF
							SET_BIT(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DoneCutscene)
						ENDIF
						
					ENDIF
					
					//-- Cleanup if we're critical on new Ambeinet events
					IF IS_PLAYER_CRITICAL_TO_ANY_EVENT(PLAYER_ID())
						playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_LEAVE
						MPGlobalsAmbience.bInitLesterCutLaunch = FALSE
						#IF IS_DEBUG_BUILD NET_DW_PRINT("playerBD[PARTICIPANT_ID_TO_INT()].iGameState GAME_STATE_RUNNING >  GAME_STATE_LEAVE as IS_PLAYER_CRITICAL_TO_ANY_EVENT") #ENDIF
						
					ENDIF
					
		
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY
					#IF IS_DEBUG_BUILD NET_DW_PRINT("playerBD[PARTICIPANT_ID_TO_INT()].iGameState GAME_STATE_RUNNING >  GAME_STATE_TERMINATE_DELAY") #ENDIF
				ENDIF
				
				
				//-- Leave if we start a mission
		/*		IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
					#IF IS_DEBUG_BUILD NET_DW_PRINT("playerBD[PARTICIPANT_ID_TO_INT()].iGameState GAME_STATE_RUNNING > GAME_STATE_END as on mission ") #ENDIF
					
				ENDIF
		*/						
			BREAK
			
			CASE GAME_STATE_TERMINATE_DELAY 
				SET_MISSION_FINISHED(serverBD.timeTerminate)
		      	IF IS_MISSION_READY_TO_CLEANUP(serverBD.timeTerminate)
		            playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
					#IF IS_DEBUG_BUILD NET_DW_PRINT("playerBD[PARTICIPANT_ID_TO_INT()].iGameState GAME_STATE_TERMINATE_DELAY >  GAME_STATE_END") #ENDIF
		      	ENDIF
			BREAK
			
			// The game stage the local player is placed when we want him to leave a mission
			CASE GAME_STATE_LEAVE

				playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
				#IF IS_DEBUG_BUILD NET_DW_PRINT("playerBD[PARTICIPANT_ID_TO_INT()].iGameState GAME_STATE_LEAVE >  GAME_STATE_END") #ENDIF

			FALLTHRU
			
			//Cleans up then terminates the mission
			CASE GAME_STATE_END
				#IF IS_DEBUG_BUILD NET_DW_PRINT("About to run cleanup from GAME_STATE_END") #ENDIF
				CLEANUP_SCRIPT()
			BREAK

		ENDSWITCH
		
	 	

		
// *****************************************************************************************************************************************************************	
// *****************************************************************************************************************************************************************	
//												SERVER PROCESSING
// *****************************************************************************************************************************************************************	
// *****************************************************************************************************************************************************************	
		
		
		
		// -----------------------------------
		// Process server game logic		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			
			SWITCH GET_SERVER_MISSION_STATE()
				
				
				CASE GAME_STATE_INI
					serverbd.iPartWithLester = -1
					serverBD.iServerGameState = GAME_STATE_RUNNING
					#IF IS_DEBUG_BUILD NET_DW_PRINT("serverBD.iServerGameState = GAME_STATE_RUNNING") #ENDIF
						
				BREAK
				
				// Look for game end conditions
				CASE GAME_STATE_RUNNING
					MAINTAIN_SERVER_STAGGERED_LOOP()
					MAINTAIN_LESTER_CUTSCENE_SERVER()
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE
						ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_LESTER_CUTSCENE_SERVER")
					#ENDIF
					#ENDIF		

					IF HAVE_MISSION_END_CONDITIONS_BEEN_MET()
						serverBD.iServerGameState = GAME_STATE_END
						#IF IS_DEBUG_BUILD NET_DW_PRINT ("serverBD.iServerGameState GAME_STATE_RUNNING > GAME_STATE_END ")#ENDIF
						
					ENDIF
					
				BREAK
				
				CASE GAME_STATE_END
				BREAK
				
			ENDSWITCH
			
		ENDIF
		
		
		
		#IF IS_DEBUG_BUILD 
			#IF SCRIPT_PROFILER_ACTIVE
				SCRIPT_PROFILER_END_OF_FRAME()
			#ENDIF
		#ENDIF			
	
	ENDWHILE

ENDSCRIPT

