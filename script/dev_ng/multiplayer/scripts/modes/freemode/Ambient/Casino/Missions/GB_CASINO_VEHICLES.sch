
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GB_CASINO_VEHICLES.sch																								//
// Handles the creation and processing of non mission critical vehicles													//
// Written by:  Martin McMillan & Ryan Elliott.																			//
// Date: 		07/02/2019																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#IF FEATURE_CASINO

//----------------------
//	INCLUDES
//----------------------

USING "rage_builtins.sch"
USING "globals.sch"

USING "GB_CASINO_COMMON.sch"



//////////////////////////////////////////
////  	CRITICAL ENTITY FUNCTIONS 	  ////
//////////////////////////////////////////  
FUNC BOOL SHOULD_VARIATION_HAVE_PASSENGER_SEAT_BOMBS()
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_MISSION_ALLOW_PASSENGER_CHAFF()
	
	IF SHOULD_VARIATION_HAVE_PASSENGER_SEAT_BOMBS()
		RETURN FALSE
	ENDIF

	RETURN FALSE
ENDFUNC

PROC SETUP_VEHICLE_MODS(VEHICLE_INDEX& vehId)
	SWITCH GET_GBC_VARIATION()
		CASE CSV_MAX
			IF GET_NUM_MOD_KITS(vehId) > 0
				SET_VEHICLE_MOD_KIT(vehId, 0)
				
				IF GET_NUM_VEHICLE_MODS(vehId, MOD_CHASSIS) > 0
					SET_VEHICLE_MOD(vehId, MOD_CHASSIS, 0)
				ENDIF
				IF GET_NUM_VEHICLE_MODS(vehId, MOD_ROOF) > 0	// Machines Guns
					SET_VEHICLE_MOD(vehId, MOD_ROOF, 0)
				ENDIF
				IF GET_NUM_VEHICLE_MODS(vehId, MOD_EXHAUST) > 0	// JTOL
					SET_VEHICLE_MOD(vehId, MOD_EXHAUST, 0)
				ENDIF
			ENDIF
		BREAK	
	ENDSWITCH
ENDPROC

FUNC BOOL DOES_MISSION_ENTITY_VEHICLE_NEED_HOTWIRED(INT iMissionEntity)
	
	UNUSED_PARAMETER(iMissionEntity)
	
	IF IS_THIS_VARIATION_A_SELL_MISSION()
	OR IS_THIS_VARIATION_A_SETUP_MISSION()
		RETURN FALSE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL DOES_MISSION_ENTITY_VEHICLE_NEED_BROKEN_INTO(INT iMissionEntity)
	
	UNUSED_PARAMETER(iMissionEntity)
	
	RETURN FALSE
	
ENDFUNC

FUNC FLOAT GET_RANDOM_DIRT_LEVEL()

	FLOAT fReturn
	
	INT iRand = GET_RANDOM_INT_IN_RANGE(0, 10000)
	
	IF iRand > 8000
		fReturn = 15.00
	ELIF iRand > 4000
		fReturn = 10.00
	ELIF iRand > 2000
		fReturn = 7.50
	ELSE
		fReturn = 5.00
	ENDIF

	RETURN fReturn
ENDFUNC

/// PURPOSE: USed to set the mission vehicle visual attributes, like engine on, door open, lights on, etc.  
PROC SET_MISSION_VEHICLE_COSMETIC_ATTRIBUTES_ON_SPAWN(INT iVehicle, VEHICLE_INDEX vehId)
	
	UNUSED_PARAMETER(iVehicle)
	
	MODEL_NAMES eModel = GET_ENTITY_MODEL(vehId)
	
	SWITCH eModel
		CASE DUMMY_MODEL_FOR_SCRIPT
		
		BREAK
	ENDSWITCH
	
	IF IS_THIS_VARIATION_A_SETUP_MISSION()
//		SET_VEHICLE_DOOR_OPEN(vehId, SC_DOOR_FRONT_LEFT)
		SET_VEHICLE_DIRT_LEVEL(vehId, 15.00)	
		PRINTLN("SET_MISSION_VEHICLE_COSMETIC_ATTRIBUTES_ON_SPAWN, SET_VEHICLE_DIRT_LEVEL ")
	ENDIF

ENDPROC

FUNC BOOL SHOULD_VEHICLE_BE_LOCKED_ON_SPAWN(INT iMissionEntity)
	
	UNUSED_PARAMETER(iMissionEntity)
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL SHOULD_LOCK_VEHICLE_REAR_DOORS(INT iMissionEntity)

	UNUSED_PARAMETER(iMissionEntity)
	
//	SWITCH GET_GBC_VARIATION()
//		CASE CSV_MISSING_DELIVERY	RETURN TRUE
//	ENDSWITCH

	RETURN FALSE
ENDFUNC

FUNC FLOAT GET_MISSION_ENTITY_DAMAGE_SCALE(MODEL_NAMES eModel)
	
	#IF IS_DEBUG_BUILD
	IF MPGlobalsAmbience.sMagnateGangBossData.fWidgetCarrierDamageScale > 0.0
		PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - GET_MISSION_ENTITY_DAMAGE_SCALE - Using widget damage scale = ", MPGlobalsAmbience.sMagnateGangBossData.fWidgetCarrierDamageScale)
		RETURN MPGlobalsAmbience.sMagnateGangBossData.fWidgetCarrierDamageScale
	ENDIF
	#ENDIF
	
	SWITCH GET_GBC_VARIATION()
		CASE CSV_HIGH_ROLLER			RETURN 1.0
		CASE CSV_UNDER_THE_INFLUENCE	RETURN 1.0
		CASE CSV_RECOVER_LUXURY_CAR		RETURN 1.2
		CASE CSV_ESCORT_DUTY			RETURN 1.2
		CASE CSV_INTIMIDATE_JUDGE		RETURN 1.0
	ENDSWITCH
	
	SWITCH eModel
		CASE INSURGENT3
			RETURN 1.0
	ENDSWITCH
	
	RETURN 0.2
ENDFUNC

FUNC BOOL SHOULD_ADJUST_DAMAGE_SCALE(MODEL_NAMES eModel)
	IF GET_MISSION_ENTITY_DAMAGE_SCALE(eModel) != 1.0
		RETURN TRUE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL PREVENT_EXPLODE_VEHICLE_ON_BODY_DMG()

	SWITCH GET_GBC_VARIATION()
		CASE CSV_RECOVER_LUXURY_CAR		RETURN TRUE
	ENDSWITCH

	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_RESIST_EXPLOSION(MODEL_NAMES model)

	SWITCH model
		CASE BOMBUSHKA
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE

ENDFUNC

FUNC FLOAT GET_PLANE_ENGINE_HEALTH(MODEL_NAMES model)

	SWITCH model
		CASE BOMBUSHKA
			RETURN 5000.0
	ENDSWITCH
	
	RETURN -1.0

ENDFUNC

FUNC BOOL SHOULD_WARP_MISSION_ENTITY_ON_STUCK()
	
	RETURN FALSE

ENDFUNC

FUNC BOOL SHOULD_OVERRIDE_NUMBER_PLATE(INT iMissionEntity)
	UNUSED_PARAMETER(iMissionEntity)
	RETURN FALSE
ENDFUNC

FUNC STRING GET_NUMBER_PLATE_OVERRIDE_STRING(INT iMissionEntity)
	UNUSED_PARAMETER(iMissionEntity)
	RETURN ""
ENDFUNC

FUNC BOOL SHOULD_MISSION_ENTITY_VEHICLE_START_MOVING_ON_SPAWN()
	
	RETURN FALSE

ENDFUNC

PROC SET_MISSION_ENTITY_VEHICLE_SPAWN_ATTRIBUTES(INT iMissionEntity, MODEL_NAMES eModel, NETWORK_INDEX netID, BOOL bForFakeVehicle, BOOL bForMissionTrailerCab = FALSE)
	
	VEHICLE_INDEX vehId = NET_TO_VEH(netID)
	
	IF SHOULD_SPAWN_MISSION_ENTITY_INSIDE_INTERIOR()
		NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(NET_TO_ENT(netID), TRUE)
		SET_MISSION_ENTITY_BIT(iMissionEntity, eMISSIONENTITYBITSET_ONLY_EXISTS_FOR_PARTICIPANTS)
		SET_MISSION_ENTITY_BIT(iMissionEntity, eMISSIONENTITYBITSET_I_AM_IN_A_MISSION_INTERIOR)
	ENDIF
	
	// Health Stuff
	IF NOT bForFakeVehicle
	OR bForMissionTrailerCab
		SET_ENTITY_INVINCIBLE(vehId, TRUE)
	ENDIF
	SET_VEHICLE_TYRES_CAN_BURST(vehId, FALSE)
	PRINTLN("SET_MISSION_ENTITY_VEHICLE_SPAWN_ATTRIBUTES - SET_VEHICLE_TYRES_CAN_BURST(vehId, FALSE) - vehId = ", NATIVE_TO_INT(vehId))
	
	SET_VEHICLE_STRONG(vehId, TRUE)
	PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] [STRONG] - SET_MISSION_ENTITY_VEHICLE_SPAWN_ATTRIBUTES - Setting vehicle strong.")
	
	SET_VEHICLE_USES_MP_PLAYER_DAMAGE_MULTIPLIER(vehId, FALSE)
	PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_MISSION_ENTITY_VEHICLE_SPAWN_ATTRIBUTES - SET_VEHICLE_USES_MP_PLAYER_DAMAGE_MULTIPLIER = FALSE")
	
	IF SHOULD_ADJUST_DAMAGE_SCALE(GET_ENTITY_MODEL(vehId))
		SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE(vehId, FALSE)
		PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_MISSION_ENTITY_VEHICLE_SPAWN_ATTRIBUTES - SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE = FALSE")
	
		SET_VEHICLE_DAMAGE_SCALE(vehId, GET_MISSION_ENTITY_DAMAGE_SCALE(GET_ENTITY_MODEL(vehId)))
		PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_MISSION_ENTITY_VEHICLE_SPAWN_ATTRIBUTES - SET_VEHICLE_DAMAGE_SCALE = ",GET_MISSION_ENTITY_DAMAGE_SCALE(GET_ENTITY_MODEL(vehId)))
		
		IF IS_THIS_MODEL_A_PLANE(eModel)
			IF SHOULD_RESIST_EXPLOSION(eModel)
				SET_PLANE_RESIST_TO_EXPLOSION(vehId, TRUE)
			ENDIF
			SET_VEHICLE_CAN_BREAK(vehId, FALSE)
		ENDIF
	
		SET_ENTITY_HEALTH(vehId, 1000)
		PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_MISSION_ENTITY_VEHICLE_SPAWN_ATTRIBUTES - SET_ENTITY_HEALTH = 1000")
	ENDIF
	
	IF IS_THIS_MODEL_A_PLANE(eModel)
		SET_PLANE_TURBULENCE_MULTIPLIER(vehId, 0.0)
		PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_MISSION_ENTITY_VEHICLE_SPAWN_ATTRIBUTES - SET_PLANE_TURBULENCE_MULTIPLIER to 0 for mission vehicle ",iMissionEntity)
		
		IF GET_PLANE_ENGINE_HEALTH(eModel) != -1.0
			SET_PLANE_ENGINE_HEALTH(vehId, GET_PLANE_ENGINE_HEALTH(eModel))
		ENDIF
	ENDIF

	CALL_SET_ALLOW_RAMMING_SOOP_OR_RAMP(vehId)
	
	IF GET_ENTITY_MODEL(vehId) = GET_PHANTOM_TRAILER_MODEL()
		SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE(vehId, FALSE)
		SET_ENTITY_PROOFS(vehId, FALSE, FALSE, FALSE, TRUE, FALSE, FALSE, FALSE, FALSE)
		PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_MISSION_ENTITY_VEHICLE_SPAWN_ATTRIBUTES - set phantom tailer does not explode on high explosion damage - SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE(vehId, FALSE)")
		PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_MISSION_ENTITY_VEHICLE_SPAWN_ATTRIBUTES - set phantom tailer does not have collision damage - SET_ENTITY_PROOFS(vehId, FALSE, FALSE, FALSE, TRUE, FALSE, FALSE, FALSE, FALSE)")
	ENDIF
	
//	IF NOT bForFakeVehicle
//	AND NOT bForMissionTrailerCab
//		GB_SET_VEHICLE_AS_CONTRABAND(NET_TO_VEH(serverBD.sMissionEntities.netId[iMissionEntity]), iMissionEntity, GBCT_GBC)
//	ENDIF
	
	SET_MENTAL_STATE_WILL_IGNORE_THIS_VEHICLE(vehId, TRUE)
	
	// Lock stuff and entering
	SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(vehId, TRUE)
	SET_VEHICLE_RESPECTS_LOCKS_WHEN_HAS_DRIVER(vehId, TRUE)
	IF DOES_MISSION_ENTITY_VEHICLE_NEED_HOTWIRED(iMissionEntity)
		SET_VEHICLE_NEEDS_TO_BE_HOTWIRED(vehId, TRUE)
	ENDIF
	
	IF DOES_MISSION_ENTITY_VEHICLE_NEED_BROKEN_INTO(iMissionEntity)
		IF GET_GBC_ENTITY_MODEL_FOR_THIS_VARIATION() != BTYPE3 // Add vehicles to this list if we want it unlocked and enterable with no break in anims.
		AND GET_GBC_ENTITY_MODEL_FOR_THIS_VARIATION() != ZTYPE
		AND GET_GBC_ENTITY_MODEL_FOR_THIS_VARIATION() != FELTZER3
			SET_VEHICLE_DOORS_LOCKED(vehId, VEHICLELOCK_LOCKED_BUT_CAN_BE_DAMAGED)
		ENDIF 
	ENDIF
	
	BOOL bLock = SHOULD_VEHICLE_BE_LOCKED_ON_SPAWN(iMissionEntity)
	SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(vehId, bLock)
	SET_VEHICLE_DISABLE_TOWING(vehId, bLock)

	SET_VEHICLE_DOORS_LOCKED_FOR_NON_SCRIPT_PLAYERS(vehId, TRUE)
	
	IF SHOULD_LOCK_VEHICLE_REAR_DOORS(iMissionEntity)
		SET_VEHICLE_INDIVIDUAL_DOORS_LOCKED(vehId, ENUM_TO_INT(SC_DOOR_REAR_LEFT), VEHICLELOCK_LOCKED)
		SET_VEHICLE_INDIVIDUAL_DOORS_LOCKED(vehId, ENUM_TO_INT(SC_DOOR_REAR_RIGHT), VEHICLELOCK_LOCKED)
		SET_OPEN_REAR_DOORS_ON_EXPLOSION(vehId, FALSE)
	ENDIF
	
	// Lock on stuff
	SET_VEHICLE_ALLOW_HOMING_MISSLE_LOCKON_SYNCED(vehId, FALSE)
	SET_VEHICLE_ALLOW_NO_PASSENGERS_LOCKON(vehId, FALSE)
	
	// Cosmetic stuff
	IF NOT bForMissionTrailerCab
		SETUP_VEHICLE_MODS(vehId)
		SET_MISSION_VEHICLE_COSMETIC_ATTRIBUTES_ON_SPAWN(iMissionEntity, vehId)
	ENDIF
	
	SWITCH GET_GBC_VARIATION()
		CASE CSV_MAX
		
		BREAK
	ENDSWITCH

	SET_ENTITY_LOD_DIST(vehId, 1200)
	
	// KW 23/3/17 For bug 3437744 Sell - Phantom - Should the rear middle seat of the Phantom be blocked for MC / org members for this mission?
	IF bForMissionTrailerCab
		IF DOES_VARIATION_VEHICLE_HAVE_DOOR(vehId, SC_DOOR_REAR_LEFT)
			SET_VEHICLE_INDIVIDUAL_DOORS_LOCKED(vehId, ENUM_TO_INT(SC_DOOR_REAR_LEFT), VEHICLELOCK_LOCKED)
		ENDIF
		
		IF DOES_VARIATION_VEHICLE_HAVE_DOOR(vehId, SC_DOOR_REAR_RIGHT)
			SET_VEHICLE_INDIVIDUAL_DOORS_LOCKED(vehId, ENUM_TO_INT(SC_DOOR_REAR_RIGHT), VEHICLELOCK_LOCKED)
		ENDIF
	ENDIF 
	
	GB_SET_FREEMODE_DELIVERABLE_ID_DECOR(NET_TO_ENT(serverBD.sMissionEntities.netId[iMissionEntity]), serverBD.deliverableMissionIds[iMissionEntity].iIndex)
	SET_VEHICLE_ON_GROUND_PROPERLY(vehId)
	
	IF SHOULD_MISSION_ENTITY_VEHICLE_START_MOVING_ON_SPAWN()
		SET_VEHICLE_ENGINE_ON(vehID, TRUE, TRUE)
		//SET_VEHICLE_FORWARD_SPEED(vehId, 30.0)
		PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_MISSION_ENTITY_VEHICLE_SPAWN_ATTRIBUTES - SHOULD_MISSION_ENTITY_VEHICLE_START_MOVING_ON_SPAWN for mission vehicle ",iMissionEntity)
	ENDIF
	
	// Decorator stuff
	IF NOT bForFakeVehicle
	AND NOT bForMissionTrailerCab
		IF DECOR_IS_REGISTERED_AS_TYPE("Not_Allow_As_Saved_Veh", DECOR_TYPE_INT)
	    	DECOR_SET_INT(vehId, "Not_Allow_As_Saved_Veh", MP_NOT_ALLOWED_AS_SAVED_VEHICLE_DEC_SIMEON)
			PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] Contraband vehicle set as 'Not_Allow_As_Saved_Veh'.")
		ENDIF
		
		IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
			INT iDecoratorValue
			IF DECOR_EXIST_ON(vehId, "MPBitset")
				iDecoratorValue = DECOR_GET_INT(vehId, "MPBitset")
			ENDIF
			SET_BIT(iDecoratorValue, MP_DECORATOR_BS_NON_PASSIVE_MODE_VEHICLE)
			SET_BIT(iDecoratorValue, MP_DECORATOR_BS_NON_MODDABLE_VEHICLE)
			DECOR_SET_INT(vehId, "MPBitset", iDecoratorValue)
			
			PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] Contraband vehicle set as 'MP_DECORATOR_BS_NON_PASSIVE_MODE_VEHICLE'.")
			PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] Contraband vehicle set as 'MP_DECORATOR_BS_NON_MODDABLE_VEHICLE'.")
		ENDIF
	ENDIF
	
	UNUSED_PARAMETER(eModel)
ENDPROC

/////////////////////////////////////////////////
////  	NON CRITICAL VEHICLE ATTRIBUTES 	 ////
/////////////////////////////////////////////////  

FUNC FLOAT GET_SUPPORT_DAMAGE_SCALE(MODEL_NAMES eModel)
	UNUSED_PARAMETER(eModel)
	
	SWITCH eModel
		CASE MOGUL		RETURN 0.5
	ENDSWITCH
	
	RETURN 0.3
ENDFUNC

PROC SET_SUPPORT_VEHICLE_COSMETIC_ATTRIBUTES(VEHICLE_INDEX vehId)
	UNUSED_PARAMETER(vehID)
ENDPROC

PROC SET_SUPPORT_VEHICLE_SPAWN_ATTRIBUTES(INT iVehicle, VEHICLE_INDEX vehId)
	MODEL_NAMES eModel = GET_ENTITY_MODEL(vehId)
	PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_SUPPORT_VEHICLE_SPAWN_ATTRIBUTES - Setting up support vehicle ", GET_SUPPORT_VEHICLE_INDEX(iVehicle), " with model ", GET_MODEL_NAME_FOR_DEBUG(eModel))
	
	// Damage Modifiers
	SET_VEHICLE_TYRES_CAN_BURST(vehId, FALSE)
	IF GET_SUPPORT_DAMAGE_SCALE(eModel) != 1.0
		SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE(vehId, FALSE)
		SET_VEHICLE_STRONG(vehId, TRUE)
		SET_VEHICLE_DAMAGE_SCALE(vehId, GET_SUPPORT_DAMAGE_SCALE(eModel))
		PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] SET_SUPPORT_VEHICLE_SPAWN_ATTRIBUTES - Set vehicle to take additional explosive damage. Damage scale = ", GET_SUPPORT_DAMAGE_SCALE(eModel))
	ENDIF
	IF IS_THIS_MODEL_A_PLANE(eModel)
		SET_VEHICLE_CAN_BREAK(vehId, FALSE)
	ENDIF
		
	// Cosmetic
	SET_SUPPORT_VEHICLE_COSMETIC_ATTRIBUTES(vehID)
	
	// Lock state
	SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(vehId, TRUE)
	SET_VEHICLE_RESPECTS_LOCKS_WHEN_HAS_DRIVER(vehId, TRUE)
	SET_VEHICLE_DOORS_LOCKED_FOR_NON_SCRIPT_PLAYERS(vehID, TRUE)
	
	// Mission specific support vehicle attributes
	SWITCH GET_GBC_VARIATION()
		CASE CSV_MAX	
		
		BREAK
	ENDSWITCH

	UNUSED_PARAMETER(iVehicle)
ENDPROC

FUNC BOOL SHOULD_LOCK_CARRIER_VEHICLE_ON_SPAWN()

	SWITCH GET_GBC_VARIATION()
		CASE CSV_RECOVER_LUXURY_CAR
		CASE CSV_SLOT_MACHINES
		CASE CSV_ESCORT_DUTY
		CASE CSV_INTIMIDATE_JUDGE
			RETURN TRUE
	ENDSWITCH

	RETURN FALSE

ENDFUNC

FUNC BOOL SHOULD_CARRIER_VEHICLE_NEED_HOTWIRED()

	SWITCH GET_GBC_VARIATION()
		CASE CSV_UNDER_THE_INFLUENCE
		CASE CSV_BODY_DISPOSAL
		CASE CSV_HIGH_ROLLER
		CASE CSV_RECOVER_LUXURY_CAR
		CASE CSV_PASSED_OUT
		CASE CSV_MISSING_DELIVERY
		CASE CSV_SLOT_MACHINES
		CASE CSV_ESCORT_DUTY
		CASE CSV_HIGH_ROLLER_TOUR
		CASE CSV_INTIMIDATE_JUDGE
			RETURN FALSE
	ENDSWITCH

	RETURN TRUE

ENDFUNC

FUNC BOOL SHOULD_CARRIER_VEHICLE_NEED_BROKEN_INTO(MODEL_NAMES eModel)

	SWITCH GET_GBC_VARIATION()
		CASE CSV_UNDER_THE_INFLUENCE
		CASE CSV_BODY_DISPOSAL
		CASE CSV_HIGH_ROLLER
		CASE CSV_RECOVER_LUXURY_CAR
		CASE CSV_PASSED_OUT
		CASE CSV_MISSING_DELIVERY
		CASE CSV_SLOT_MACHINES
		CASE CSV_ESCORT_DUTY
		CASE CSV_HIGH_ROLLER_TOUR
		CASE CSV_INTIMIDATE_JUDGE
			RETURN FALSE
	ENDSWITCH
	
	SWITCH eModel
		CASE HALFTRACK
		CASE INSURGENT3
			RETURN FALSE
	ENDSWITCH

	RETURN TRUE

ENDFUNC

FUNC BOOL SHOULD_LOCK_CARRIER_VEHICLE_REAR_DOORS(INT iVehicle)
	UNUSED_PARAMETER(iVehicle)

	SWITCH GET_GBC_VARIATION()
		CASE CSV_PASSED_OUT
		CASE CSV_MISSING_DELIVERY
			RETURN TRUE
	ENDSWITCH

	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_SET_CARRIER_VEHICLE_STRONG()
	SWITCH GET_GBC_VARIATION()
		CASE CSV_RECOVER_LUXURY_CAR
		CASE CSV_INTIMIDATE_JUDGE
			RETURN FALSE
	ENDSWITCH

	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_SET_CARRIER_VEHICLE_STRONG_TYRES()
	SWITCH GET_GBC_VARIATION()
		CASE CSV_INTIMIDATE_JUDGE
			RETURN FALSE
	ENDSWITCH

	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_SET_CARRIER_VEHICLE_MP_DAMAGE()
	SWITCH GET_GBC_VARIATION()
		CASE CSV_RECOVER_LUXURY_CAR
			RETURN FALSE
	ENDSWITCH

	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_VEHICLE_SETUP_MP_SET_LOCK_STATUS(INT iVehicle)
	SWITCH GET_GBC_VARIATION()
		CASE CSV_HIGH_ROLLER
			SWITCH iVehicle
				CASE 2
				CASE 3	RETURN FALSE
			ENDSWITCH
		BREAK
		CASE CSV_ESCORT_DUTY
			RETURN FALSE
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

PROC SET_GBC_DECORATOR_ON_VEHICLE(VEHICLE_INDEX vehId)

	IF DECOR_IS_REGISTERED_AS_TYPE("GBCVehicle", DECOR_TYPE_BOOL)
		DECOR_SET_BOOL(vehID, "GBCVehicle", TRUE)
		PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_GBC_DECORATOR_ON_VEHICLE")
	ENDIF
		
ENDPROC

PROC SET_CARRIER_VEHICLE_SPAWN_ATTRIBUTES(INT iVehicle, VEHICLE_INDEX vehId)

	PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_CARRIER_VEHICLE_SPAWN_ATTRIBUTES")

	MODEL_NAMES eModel = GET_ENTITY_MODEL(vehId)
	
	VEHICLE_SETUP_STRUCT_MP sData // Use for any mods
	BOOL bDataSet = FALSE
	
	SWITCH GET_GBC_VARIATION()	
		CASE CSV_BAD_PRESS
			SET_VEHICLE_DISABLE_TOWING(vehId, TRUE)
		BREAK
		CASE CSV_BODY_DISPOSAL
			SET_VEHICLE_DISABLE_TOWING(vehId, TRUE)
		BREAK
		CASE CSV_MISSING_DELIVERY
			SWITCH GET_ENTITY_MODEL(vehId) 
				CASE STOCKADE
					bDataSet = TRUE
					sData.VehicleSetup.iColour1 = 24
					sData.VehicleSetup.iColourExtra2 = 155
					sData.iColour5 = 1
					sData.iColour6 = 132
					
					SET_VEHICLE_DISABLE_TOWING(vehId, TRUE)
				BREAK
			ENDSWITCH
		BREAK
		CASE CSV_ESCORT_DUTY
			SET_ENTITY_ONLY_DAMAGED_BY_RELATIONSHIP_GROUP(vehId, TRUE, rgFM_AiHate)
			
			bDataSet = TRUE
			sData.VehicleSetup.iColour1 = 24
			sData.VehicleSetup.iColourExtra2 = 155
			sData.iColour5 = 1
			sData.iColour6 = 132
		BREAK
		CASE CSV_RECOVER_LUXURY_CAR
		
			SET_VEHICLE_DISABLE_TOWING(vehId, TRUE)
			
			SWITCH GET_ENTITY_MODEL(vehId) 
				CASE ENTITY2
					bDataSet = TRUE
					sData.VehicleSetup.eModel = entity2 // ENTITY2							
					sData.VehicleSetup.iColour1 = 145							
					sData.VehicleSetup.iColourExtra1 = 70							
					sData.VehicleSetup.iColourExtra2 = 145							
					sData.iColour5 = 1							
					sData.iColour6 = 132							
					sData.iLivery2 = 0							
					sData.VehicleSetup.iWindowTintColour = 3							
					sData.VehicleSetup.iWheelType = 7							
					sData.VehicleSetup.iTyreR = 255							
					sData.VehicleSetup.iTyreG = 255							
					sData.VehicleSetup.iTyreB = 255							
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_BULLETPROOF_TYRES)							
					sData.VehicleSetup.iModIndex[MOD_SPOILER] = 2							
					sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 2							
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2							
					sData.VehicleSetup.iModIndex[MOD_BONNET] = 3							
					sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4							
					sData.VehicleSetup.iModIndex[MOD_WHEELS] = 5							
				BREAK
				CASE SCHLAGEN
					bDataSet = TRUE
					sData.VehicleSetup.eModel = schlagen // SCHLAGEN							
					sData.VehicleSetup.iColour1 = 5							
					sData.VehicleSetup.iColour2 = 12							
					sData.VehicleSetup.iColourExtra1 = 111							
					sData.iColour5 = 1							
					sData.iColour6 = 132							
					sData.iLivery2 = 0							
					sData.VehicleSetup.iWindowTintColour = 1							
					sData.VehicleSetup.iWheelType = 7							
					sData.VehicleSetup.iTyreR = 255							
					sData.VehicleSetup.iTyreG = 255							
					sData.VehicleSetup.iTyreB = 255							
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_BULLETPROOF_TYRES)							
					sData.VehicleSetup.iModIndex[MOD_SPOILER] = 2							
					sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 5							
					sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 4							
					sData.VehicleSetup.iModIndex[MOD_SKIRT] = 6							
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1							
					sData.VehicleSetup.iModIndex[MOD_GRILL] = 5							
					sData.VehicleSetup.iModIndex[MOD_BONNET] = 3							
					sData.VehicleSetup.iModIndex[MOD_WING_R] = 2							
					sData.VehicleSetup.iModIndex[MOD_ROOF] = 3							
					sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1							
					sData.VehicleSetup.iModIndex[MOD_WHEELS] = 10							
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 1							
				BREAK
				CASE ITALIGTO
					bDataSet = TRUE
					sData.VehicleSetup.eModel = italigto // ITALIGTO							
					sData.VehicleSetup.iColour1 = 150							
					sData.VehicleSetup.iColourExtra1 = 29							
					sData.VehicleSetup.iColourExtra2 = 122							
					sData.iColour5 = 1							
					sData.iColour6 = 132							
					sData.iLivery2 = 0							
					sData.VehicleSetup.iWindowTintColour = 3							
					sData.VehicleSetup.iWheelType = 7							
					sData.VehicleSetup.iTyreR = 255							
					sData.VehicleSetup.iTyreG = 255							
					sData.VehicleSetup.iTyreB = 255							
					sData.VehicleSetup.iNeonR = 255							
					sData.VehicleSetup.iNeonG = 1							
					sData.VehicleSetup.iNeonB = 1							
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)							
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)							
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)							
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)							
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_BULLETPROOF_TYRES)							
					sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1							
					sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 9							
					sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 3							
					sData.VehicleSetup.iModIndex[MOD_SKIRT] = 5							
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 4							
					sData.VehicleSetup.iModIndex[MOD_GRILL] = 2							
					sData.VehicleSetup.iModIndex[MOD_BONNET] = 7							
					sData.VehicleSetup.iModIndex[MOD_WING_L] = 3							
					sData.VehicleSetup.iModIndex[MOD_ROOF] = 1							
					sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 3							
					sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 14							
					sData.VehicleSetup.iModIndex[MOD_WHEELS] = 5							
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 8							
					sData.VehicleSetup.iModVariation[0] = 1							
				BREAK
			ENDSWITCH
		BREAK
		CASE CSV_PASSED_OUT
			SWITCH GET_GBC_SUBVARIATION()
				CASE CSS_PO_CHILIAD
				CASE CSS_PO_HARMONY
					IF GET_ENTITY_MODEL(vehID) = MULE
						VECTOR vDamageCoord
						vDamageCoord = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(vehID), GET_ENTITY_HEADING(vehID), <<0.5, 0.6, 0.0>>)
						// Beat the crap out of the front end
						SET_VEHICLE_DAMAGE(vehID, vDamageCoord, 250.0, 250.0, TRUE)
						SET_VEHICLE_DAMAGE(vehID, vDamageCoord, 250.0, 250.0, TRUE)
						SET_VEHICLE_DAMAGE(vehID, vDamageCoord, 250.0, 250.0, TRUE)
						SET_VEHICLE_DAMAGE(vehID, vDamageCoord, 250.0, 250.0, TRUE)
						SET_VEHICLE_DAMAGE(vehID, vDamageCoord, 250.0, 250.0, TRUE)
						SET_VEHICLE_DAMAGE(vehID, vDamageCoord, 250.0, 250.0, TRUE)
						SET_VEHICLE_DAMAGE(vehID, vDamageCoord, 250.0, 250.0, TRUE)
						PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_CARRIER_VEHICLE_SPAWN_ATTRIBUTES - SET_VEHICLE_DAMAGE, 205.0, 250.0 x7")
					ENDIF
				BREAK
			ENDSWITCH
			
			IF DOES_EXTRA_EXIST(vehId, 1)
				SET_VEHICLE_EXTRA(vehId, 1, TRUE)
			ENDIF
			IF DOES_EXTRA_EXIST(vehId, 2)
				SET_VEHICLE_EXTRA(vehId, 2, TRUE)
			ENDIF
			IF DOES_EXTRA_EXIST(vehId, 3)
				SET_VEHICLE_EXTRA(vehId, 3, TRUE)
			ENDIF
			IF DOES_EXTRA_EXIST(vehId, 4)
				SET_VEHICLE_EXTRA(vehId, 4, TRUE)
			ENDIF
			IF DOES_EXTRA_EXIST(vehId, 5)
				SET_VEHICLE_EXTRA(vehId, 5, TRUE)
			ENDIF
			IF DOES_EXTRA_EXIST(vehId, 6)
				SET_VEHICLE_EXTRA(vehId, 6, TRUE)
			ENDIF
			IF DOES_EXTRA_EXIST(vehId, 7)
				SET_VEHICLE_EXTRA(vehId, 7, FALSE)
				PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_CARRIER_VEHICLE_SPAWN_ATTRIBUTES - SET_VEHICLE_EXTRA(vehId, 7, FALSE) - Pisswasser set")
			ENDIF
			
			SET_VEHICLE_DIRT_LEVEL(vehId, 15.0)
			PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_CARRIER_VEHICLE_SPAWN_ATTRIBUTES - SET_VEHICLE_DIRT_LEVEL, 15.0")
		BREAK
		CASE CSV_HIGH_ROLLER
			SWITCH iVehicle
				CASE 2
					bDataSet = TRUE
					sData.VehicleSetup.eModel = STRETCH
					sData.VehicleSetup.tlPlateText = "DIAMOND1"
					sData.VehicleSetup.iPlateIndex = 4
					sData.VehicleSetup.iColour1 = 111
					sData.VehicleSetup.iColourExtra1 = 112
					sData.VehicleSetup.iColourExtra2 = 156
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWindowTintColour = 1
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_0)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_11)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_BULLETPROOF_TYRES)
					sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
					SET_VEHICLE_INDIVIDUAL_DOORS_LOCKED(vehId, ENUM_TO_INT(SC_DOOR_REAR_LEFT), VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
					SET_VEHICLE_INDIVIDUAL_DOORS_LOCKED(vehId, ENUM_TO_INT(SC_DOOR_REAR_RIGHT), VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
					SET_VEHICLE_DISABLE_TOWING(vehId, TRUE)
					SET_ENTITY_CAN_ONLY_BE_DAMAGED_BY_SCRIPT_PARTICIPANTS(vehID, TRUE)
				BREAK
				
				CASE 3
					bDataSet = TRUE
					sData.VehicleSetup.eModel = STRETCH
					sData.VehicleSetup.tlPlateText = "DIAMOND2"
					sData.VehicleSetup.iPlateIndex = 1
					sData.VehicleSetup.iColourExtra2 = 156
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWindowTintColour = 1
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_0)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_11)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_BULLETPROOF_TYRES)
					sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
					SET_VEHICLE_INDIVIDUAL_DOORS_LOCKED(vehId, ENUM_TO_INT(SC_DOOR_REAR_LEFT), VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
					SET_VEHICLE_INDIVIDUAL_DOORS_LOCKED(vehId, ENUM_TO_INT(SC_DOOR_REAR_RIGHT), VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
					SET_VEHICLE_DISABLE_TOWING(vehId, TRUE)
					SET_ENTITY_CAN_ONLY_BE_DAMAGED_BY_SCRIPT_PARTICIPANTS(vehID, TRUE)
				BREAK
			ENDSWITCH
		BREAK
		CASE CSV_HIGH_ROLLER_TOUR
			SWITCH iVehicle
				CASE 0
					FREEZE_ENTITY_POSITION(vehId, TRUE)
					SET_VEHICLE_INDIVIDUAL_DOORS_LOCKED(vehId, ENUM_TO_INT(SC_DOOR_REAR_LEFT), VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
					SET_VEHICLE_INDIVIDUAL_DOORS_LOCKED(vehId, ENUM_TO_INT(SC_DOOR_REAR_RIGHT), VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
				BREAK
			ENDSWITCH
		BREAK
		CASE CSV_UNDER_THE_INFLUENCE
			SET_VEHICLE_DISABLE_TOWING(vehId, TRUE)
		BREAK
	ENDSWITCH
	SWITCH GET_GBC_SUBVARIATION()
		CASE CSS_BD_VARIATION_1		
			IF GET_ENTITY_MODEL(vehID) = BALLER2
				bDataSet = TRUE
				sData.VehicleSetup.eModel = BALLER2						
				sData.VehicleSetup.tlPlateText = "43WCE752"						
				sData.VehicleSetup.iColour1 = 5						
				sData.VehicleSetup.iColourExtra1 = 111						
				sData.VehicleSetup.iColourExtra2 = 156						
				sData.iColour5 = 1						
				sData.iColour6 = 132						
				sData.iLivery2 = 0						
				sData.VehicleSetup.iWindowTintColour = 2						
				sData.VehicleSetup.iWheelType = 3						
				sData.VehicleSetup.iTyreR = 255						
				sData.VehicleSetup.iTyreG = 255						
				sData.VehicleSetup.iTyreB = 255						
				sData.VehicleSetup.iNeonR = 255						
				sData.VehicleSetup.iNeonB = 255						
				SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_9)	
				PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_CARRIER_VEHICLE_SPAWN_ATTRIBUTES - CSS_BD_VARIATION_1 for iVehicle = ", iVehicle)
			ENDIF
		BREAK
		CASE CSS_BD_VARIATION_2		
			IF GET_ENTITY_MODEL(vehID) = LANDSTALKER
				bDataSet = TRUE
				sData.VehicleSetup.eModel = LANDSTALKER						
				sData.VehicleSetup.tlPlateText = "67AGF834"						
				sData.VehicleSetup.iColour1 = 157						
				sData.VehicleSetup.iColour2 = 157						
				sData.VehicleSetup.iColourExtra1 = 87						
				sData.VehicleSetup.iColourExtra2 = 157						
				sData.iColour5 = 1						
				sData.iColour6 = 132						
				sData.iLivery2 = 0						
				sData.VehicleSetup.iWindowTintColour = 2						
				sData.VehicleSetup.iWheelType = 3						
				sData.VehicleSetup.iTyreR = 255						
				sData.VehicleSetup.iTyreG = 255						
				sData.VehicleSetup.iTyreB = 255						
				sData.VehicleSetup.iNeonR = 255						
				sData.VehicleSetup.iNeonB = 255						
				SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_9)						
				sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1						
				sData.VehicleSetup.iModIndex[MOD_ROOF] = 1	
				PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_CARRIER_VEHICLE_SPAWN_ATTRIBUTES - CSS_BD_VARIATION_2 for iVehicle = ", iVehicle)
			ENDIF
		BREAK		
		CASE CSS_BD_VARIATION_3		
			IF GET_ENTITY_MODEL(vehID) = TORNADO2
				bDataSet = TRUE
				sData.VehicleSetup.eModel = TORNADO2						
				sData.VehicleSetup.tlPlateText = "84WMY831"						
				sData.VehicleSetup.iPlateIndex = 1						
				sData.VehicleSetup.iColour1 = 40						
				sData.VehicleSetup.iColourExtra2 = 156						
				sData.iColour5 = 1						
				sData.iColour6 = 132						
				sData.iLivery2 = 0						
				sData.VehicleSetup.iWheelType = 2						
				sData.VehicleSetup.iTyreR = 255						
				sData.VehicleSetup.iTyreG = 255						
				sData.VehicleSetup.iTyreB = 255						
				sData.VehicleSetup.iNeonR = 255						
				sData.VehicleSetup.iNeonB = 255						
				SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_10)						
				sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1						
				sData.VehicleSetup.iModIndex[MOD_BONNET] = 1						
				sData.VehicleSetup.iModIndex[MOD_WING_L] = 3	
				PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_CARRIER_VEHICLE_SPAWN_ATTRIBUTES - CSS_BD_VARIATION_3 for iVehicle = ", iVehicle)
			ENDIF
		BREAK		
		CASE CSS_BD_VARIATION_4		
			IF GET_ENTITY_MODEL(vehID) = EMPEROR
				bDataSet = TRUE
				sData.VehicleSetup.eModel = EMPEROR						
				sData.VehicleSetup.tlPlateText = "09DAM225"						
				sData.VehicleSetup.iColour1 = 152						
				sData.VehicleSetup.iColour2 = 14						
				sData.VehicleSetup.iColourExtra2 = 156						
				sData.iColour5 = 1						
				sData.iColour6 = 132						
				sData.iLivery2 = 0						
				sData.VehicleSetup.iWheelType = 2						
				sData.VehicleSetup.iTyreR = 255						
				sData.VehicleSetup.iTyreG = 255						
				sData.VehicleSetup.iTyreB = 255						
				sData.VehicleSetup.iNeonR = 255						
				sData.VehicleSetup.iNeonB = 255						
				SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_11)
				PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_CARRIER_VEHICLE_SPAWN_ATTRIBUTES - CSS_BD_VARIATION_4 for iVehicle = ", iVehicle)
			ENDIF
		BREAK	
	ENDSWITCH
	
	SET_ENTITY_LOD_DIST(vehId, 1200)
	
	IF SHOULD_SET_CARRIER_VEHICLE_STRONG_TYRES()
		SET_VEHICLE_TYRES_CAN_BURST(vehId, FALSE)
		PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_CARRIER_VEHICLE_SPAWN_ATTRIBUTES - SHOULD_SET_CARRIER_VEHICLE_STRONG_TYRES")
	ENDIF
	IF SHOULD_SET_CARRIER_VEHICLE_STRONG()
		SET_VEHICLE_STRONG(vehId, TRUE)
		PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_CARRIER_VEHICLE_SPAWN_ATTRIBUTES - SHOULD_SET_CARRIER_VEHICLE_STRONG")
	ENDIF
	IF SHOULD_SET_CARRIER_VEHICLE_MP_DAMAGE()
		SET_VEHICLE_USES_MP_PLAYER_DAMAGE_MULTIPLIER(vehId, FALSE)
		PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_CARRIER_VEHICLE_SPAWN_ATTRIBUTES - SHOULD_SET_CARRIER_VEHICLE_MP_DAMAGE")
	ENDIF
	
	IF SHOULD_ADJUST_DAMAGE_SCALE(GET_ENTITY_MODEL(vehId))
		SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE(vehId, FALSE)
	
		SET_VEHICLE_DAMAGE_SCALE(vehId, GET_MISSION_ENTITY_DAMAGE_SCALE(GET_ENTITY_MODEL(vehId)))
		
		IF IS_THIS_MODEL_A_PLANE(eModel)
			//SET_PLANE_RESIST_TO_EXPLOSION(vehId, TRUE)
			SET_VEHICLE_CAN_BREAK(vehId, FALSE)
		ENDIF
	
		SET_ENTITY_HEALTH(vehId, 1000)
		PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_CARRIER_VEHICLE_SPAWN_ATTRIBUTES - SHOULD_ADJUST_DAMAGE_SCALE")
	ENDIF
	
	IF PREVENT_EXPLODE_VEHICLE_ON_BODY_DMG()
		SET_DISABLE_EXPLODE_FROM_BODY_DAMAGE_ON_COLLISION(vehId, TRUE)
		PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_CARRIER_VEHICLE_SPAWN_ATTRIBUTES - SET_DISABLE_EXPLODE_FROM_BODY_DAMAGE_ON_COLLISION")
	ENDIF
	
	CALL_SET_ALLOW_RAMMING_SOOP_OR_RAMP(vehId)
	
	SET_MENTAL_STATE_WILL_IGNORE_THIS_VEHICLE(vehId, TRUE)
	
	SET_GBC_DECORATOR_ON_VEHICLE(vehId)
	
	SET_VEHICLE_ALLOW_HOMING_MISSLE_LOCKON_SYNCED(vehId, FALSE)
	SET_VEHICLE_ALLOW_NO_PASSENGERS_LOCKON(vehId, FALSE)
	
	IF SHOULD_LOCK_CARRIER_VEHICLE_ON_SPAWN()
		SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(vehId, TRUE)
		SET_VEHICLE_DISABLE_TOWING(vehId, TRUE)
	ENDIF
	
	IF SHOULD_LOCK_CARRIER_VEHICLE_REAR_DOORS(iVehicle)
		SET_VEHICLE_INDIVIDUAL_DOORS_LOCKED(vehId, ENUM_TO_INT(SC_DOOR_REAR_LEFT), VEHICLELOCK_LOCKED)
		SET_VEHICLE_INDIVIDUAL_DOORS_LOCKED(vehId, ENUM_TO_INT(SC_DOOR_REAR_RIGHT), VEHICLELOCK_LOCKED)
		SET_OPEN_REAR_DOORS_ON_EXPLOSION(vehId, FALSE)
	ENDIF
	
	SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(vehId, TRUE)
	SET_VEHICLE_RESPECTS_LOCKS_WHEN_HAS_DRIVER(vehId, TRUE)
	SET_VEHICLE_DOORS_LOCKED_FOR_NON_SCRIPT_PLAYERS(vehID, TRUE)
	
	IF SHOULD_CARRIER_VEHICLE_NEED_HOTWIRED()
		SET_VEHICLE_NEEDS_TO_BE_HOTWIRED(vehId, TRUE)
	ENDIF
	
	IF SHOULD_CARRIER_VEHICLE_NEED_BROKEN_INTO(eModel)
		SET_VEHICLE_DOORS_LOCKED(vehId, VEHICLELOCK_LOCKED_BUT_CAN_BE_DAMAGED)
	ENDIF
	
	IF eModel = PHANTOM2
	OR eModel = DUNE5
		VEHICLE_SET_RAMP_AND_RAMMING_CARS_TAKE_DAMAGE(vehId, FALSE)
		PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_CARRIER_VEHICLE_SPAWN_ATTRIBUTES - VEHICLE_SET_RAMP_AND_RAMMING_CARS_TAKE_DAMAGE(vehId, FALSE)")
	ENDIF
	
	IF bDataSet
		SET_VEHICLE_SETUP_MP(vehID, sData, SHOULD_VEHICLE_SETUP_MP_SET_LOCK_STATUS(iVehicle))
		PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_CARRIER_VEHICLE_SPAWN_ATTRIBUTES - Set modifiers for vehicle with model ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(eModel))
	ENDIF
ENDPROC

FUNC BOOL SHOULD_SET_UP_CARRIER_VEHICLE_SPAWN_ATTRIBUTES(INT iVehicle)
	IF IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_I_AM_CARRIER_VEHICLE)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC TURN_ON_VEHICLE_RADIO(VEHICLE_INDEX vehID)

	SET_VEHICLE_ENGINE_ON(vehId, TRUE, TRUE)
	SET_VEHICLE_RADIO_ENABLED(vehId, TRUE)
	//SET_VEHICLE_RADIO_LOUD(vehId, TRUE)
	SET_VEH_FORCED_RADIO_THIS_FRAME(vehId)
	SET_VEH_RADIO_STATION(vehId, "RADIO_03_HIPHOP_NEW")
	
ENDPROC

PROC SET_TRACKING_CHIPS_VEHICLE_COSMETICS(VEHICLE_INDEX vehID)
	MODEL_NAMES vehModel = GET_ENTITY_MODEL(vehID)
	
	VEHICLE_SETUP_STRUCT_MP sData
	BOOL bDataSet = FALSE
	
	SWITCH vehModel
		CASE RHAPSODY
			bDataSet = TRUE
			sData.VehicleSetup.tlPlateText = "05SBF291"
			sData.VehicleSetup.iColour1 = 38
			sData.VehicleSetup.iColourExtra1 = 37
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 1
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
		BREAK
		CASE VINDICATOR
			bDataSet = TRUE
			sData.VehicleSetup.tlPlateText = "20DWF158"
			sData.VehicleSetup.iColour1 = 89
			sData.VehicleSetup.iColourExtra1 = 88
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 6
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
		BREAK
		CASE NINEF2
			bDataSet = TRUE
			sData.VehicleSetup.tlPlateText = "03OHO142"
			sData.VehicleSetup.iColour1 = 111
			sData.VehicleSetup.iColour2 = 1
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
		BREAK
		CASE FAGGIO2
			bDataSet = TRUE
			sData.VehicleSetup.tlPlateText = "47OKY642"
			sData.VehicleSetup.iPlateIndex = 2
			sData.VehicleSetup.iColour1 = 136
			sData.VehicleSetup.iColour2 = 111
			sData.VehicleSetup.iColourExtra1 = 5
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 6
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
		BREAK
	ENDSWITCH
	
	IF bDataSet
		SET_VEHICLE_SETUP_MP(vehID, sData)
		PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_TRACKING_CHIPS_VEHICLE_COSMETICS - Set modifiers for vehicle with model ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(vehModel))
	ENDIF
ENDPROC

PROC SET_HEISTERS_VEHICLE_COSMETICS(INT iVehicle, VEHICLE_INDEX vehID)

	VEHICLE_SETUP_STRUCT_MP sData
	sData.VehicleSetup.iLivery = 1

	SWITCH GET_GBC_SUBVARIATION()
		CASE CSS_HE_BUGSTAR
			SWITCH iVehicle
				CASE 0
					SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(vehId, TRUE)
				BREAK
				CASE 1
					SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(vehId, TRUE)
				BREAK
			ENDSWITCH
		BREAK
		CASE CSS_HE_HAYES_AUTOS
			sData.VehicleSetup.iLivery = 3
		
			SWITCH iVehicle
				CASE 0
					sData.VehicleSetup.iColour1 = 131
					SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(vehId, TRUE)
				BREAK
				CASE 1
					sData.VehicleSetup.iColour1 = 14
					SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(vehId, TRUE)
				BREAK
				CASE 2
					sData.VehicleSetup.iColour1 = 131
				BREAK
			ENDSWITCH
		BREAK
		CASE CSS_HE_LIQUOR_ACE
			SWITCH iVehicle
				CASE 0
					sData.VehicleSetup.iColour1 = 131
					SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(vehId, TRUE)
				BREAK
				CASE 1
					sData.VehicleSetup.iColour1 = 14
					SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(vehId, TRUE)
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	SET_VEHICLE_SETUP_MP(vehID, sData)
	PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_HEISTERS_VEHICLE_COSMETICS - Set modifiers for vehicle ", iVehicle)

ENDPROC

PROC SET_HIGH_ROLLER_VEHICLE_COSMETICS(INT iVehicle, VEHICLE_INDEX vehId)
	SWITCH GET_GBC_SUBVARIATION()
		CASE CSS_HR_LSIA
			SWITCH iVehicle
				CASE 0
					SET_ENTITY_CAN_ONLY_BE_DAMAGED_BY_SCRIPT_PARTICIPANTS(vehId, TRUE)
				BREAK
				
				CASE 1
					SET_ENTITY_CAN_ONLY_BE_DAMAGED_BY_SCRIPT_PARTICIPANTS(vehId, TRUE)
				BREAK
			ENDSWITCH
		BREAK
		
		CASE CSS_HR_SANDY_SHORES
			SWITCH iVehicle
				CASE 0
					SET_ENTITY_CAN_ONLY_BE_DAMAGED_BY_SCRIPT_PARTICIPANTS(vehId, TRUE)
				BREAK
				
				CASE 1
					SET_ENTITY_CAN_ONLY_BE_DAMAGED_BY_SCRIPT_PARTICIPANTS(vehId, TRUE)
					SET_BOAT_ANCHOR(vehId, TRUE)
					PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_HIGH_ROLLER_VEHICLE_COSMETICS - SET_BOAT_ANCHOR(vehId, TRUE) for iVehicle = ", iVehicle)
				BREAK
			ENDSWITCH
		BREAK
		
		CASE CSS_HR_MARINA
			SWITCH iVehicle
				CASE 0
					SET_ENTITY_CAN_ONLY_BE_DAMAGED_BY_SCRIPT_PARTICIPANTS(vehId, TRUE)
					SET_BOAT_ANCHOR(vehId, TRUE)
					PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_HIGH_ROLLER_VEHICLE_COSMETICS - SET_BOAT_ANCHOR(vehId, TRUE) for iVehicle = ", iVehicle)
				BREAK
				
				CASE 1
					SET_ENTITY_CAN_ONLY_BE_DAMAGED_BY_SCRIPT_PARTICIPANTS(vehId, TRUE)
				BREAK
			ENDSWITCH
		BREAK
		
		CASE CSS_HR_MOVIE_STUDIO
			SWITCH iVehicle
				CASE 0
					SET_ENTITY_CAN_ONLY_BE_DAMAGED_BY_SCRIPT_PARTICIPANTS(vehId, TRUE)
					SET_VEHICLE_ON_GROUND_PROPERLY(vehId)
					FREEZE_ENTITY_POSITION(vehId, TRUE)
					PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_HIGH_ROLLER_VEHICLE_COSMETICS - FREEZE_ENTITY_POSITION(vehId, TRUE) for iVehicle = ", iVehicle)
				BREAK
				
				CASE 1
					SET_ENTITY_CAN_ONLY_BE_DAMAGED_BY_SCRIPT_PARTICIPANTS(vehId, TRUE)
				BREAK
				
				CASE 2
					SET_ENTITY_CAN_ONLY_BE_DAMAGED_BY_SCRIPT_PARTICIPANTS(vehId, TRUE)
				BREAK
				
				CASE 3
					SET_ENTITY_CAN_ONLY_BE_DAMAGED_BY_SCRIPT_PARTICIPANTS(vehId, TRUE)
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(vehId, TRUE)
	SET_VEHICLE_DISABLE_TOWING(vehId, TRUE)
ENDPROC

PROC SET_BAD_PRESS_VEHICLE_COSMETICS(INT iVehicle, VEHICLE_INDEX vehId)

	VEHICLE_SETUP_STRUCT_MP sData

	IF iVehicle = BAD_PRESS_TARGET_VAN()
		sData.VehicleSetup.eModel = RUMPO						
		sData.VehicleSetup.tlPlateText = "4NC0RM4N"						
		sData.VehicleSetup.iColour1 = 112						
		sData.VehicleSetup.iColourExtra1 = 5						
		sData.VehicleSetup.iColourExtra2 = 156						
		sData.iColour5 = 1						
		sData.iColour6 = 132						
		sData.iLivery2 = 0						
		sData.VehicleSetup.iLivery = 0						
		sData.VehicleSetup.iWheelType = 1						
		sData.VehicleSetup.iTyreR = 255						
		sData.VehicleSetup.iTyreG = 255						
		sData.VehicleSetup.iTyreB = 255						
		sData.VehicleSetup.iNeonR = 255						
		sData.VehicleSetup.iNeonB = 255		
	ELSE
		sData.VehicleSetup.eModel = RUMPO						
		sData.VehicleSetup.tlPlateText = "85ZBO520"						
		sData.VehicleSetup.iColour1 = 26						
		sData.VehicleSetup.iColourExtra1 = 5						
		sData.VehicleSetup.iColourExtra2 = 156						
		sData.iColour5 = 1						
		sData.iColour6 = 132						
		sData.iLivery2 = 0						
		sData.VehicleSetup.iLivery = 0						
		sData.VehicleSetup.iWheelType = 1						
		sData.VehicleSetup.iTyreR = 255						
		sData.VehicleSetup.iTyreG = 255						
		sData.VehicleSetup.iTyreB = 255						
		sData.VehicleSetup.iNeonR = 255						
		sData.VehicleSetup.iNeonB = 255	
	ENDIF
	
	SET_VEHICLE_SETUP_MP(vehID, sData)
	
	SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(vehId, TRUE)
	SET_VEHICLE_DISABLE_TOWING(vehId, TRUE)
	
	SET_VEHICLE_ALLOW_HOMING_MISSLE_LOCKON_SYNCED(vehId, FALSE)
	SET_VEHICLE_ALLOW_NO_PASSENGERS_LOCKON(vehId, FALSE)
ENDPROC

PROC SET_DEFEND_CASINO_VEHICLE_COSMETICS(INT iVehicle, VEHICLE_INDEX vehID)
	SWITCH iVehicle
		CASE 1
			SET_VEHICLE_COLOURS(vehID, 0, 0)
			SET_VEHICLE_DOOR_OPEN(vehID, SC_DOOR_REAR_LEFT)
			SET_VEHICLE_DOOR_OPEN(vehID, SC_DOOR_REAR_RIGHT)
		BREAK
		
		CASE 2
			SET_VEHICLE_COLOURS(vehID, 4, 0)
			SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(vehID, TRUE)
			SET_VEHICLE_ALLOW_HOMING_MISSLE_LOCKON_SYNCED(vehID, FALSE)
		BREAK
		
		CASE 4
		CASE 5
			SET_GENERIC_HELI_VEHICLE_ATTRIBUTES_FOR_SPAWN_IN_AIR(vehID)
		BREAK
	ENDSWITCH
ENDPROC

PROC SET_ESCORT_DUTY_VEHICLE_COSMETICS(INT iVehicle, VEHICLE_INDEX vehId)

	UNUSED_PARAMETER(iVehicle)
	VEHICLE_SETUP_STRUCT_MP sData

	SWITCH GET_ENTITY_MODEL(vehID)
		CASE HEXER
			SET_VEHICLE_COLOURS(vehID, 0, 0)
		BREAK
		CASE CAVALCADE
			sData.VehicleSetup.iModIndex[MOD_ENGINE] = 4
			sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 3
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TURBO] = 1
			SET_VEHICLE_SETUP_MP(vehID, sData)
			SET_VEHICLE_COLOURS(vehID, 0, 0)
		BREAK
		CASE GBURRITO
		CASE KURUMA
			sData.VehicleSetup.iModIndex[MOD_ENGINE] = 4
			sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 3
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TURBO] = 1
			SET_VEHICLE_SETUP_MP(vehID, sData)
		BREAK
	ENDSWITCH
	
	SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(vehId, TRUE)

ENDPROC

PROC SET_COUNTERFEIT_CHIPS_VEHICLE_COSMETICS(INT iVehicle, VEHICLE_INDEX vehID)
	SWITCH GET_GBC_SUBVARIATION()
		CASE CSS_CC_VARIATION_1
			SWITCH iVehicle
				CASE 0
					SET_VEHICLE_COLOURS(vehID, 6, 0)
				BREAK
				
				CASE 1
					SET_VEHICLE_COLOURS(vehID, 0, 0)
				BREAK
				
				CASE 2
					SET_VEHICLE_COLOURS(vehID, 6, 0)
				BREAK
			ENDSWITCH
		BREAK
		
		CASE CSS_CC_VARIATION_2
			SWITCH iVehicle
				CASE 0
					SET_VEHICLE_COLOURS(vehID, 6, 0)
				BREAK
				
				CASE 1
					SET_VEHICLE_COLOURS(vehID, 0, 0)
				BREAK
				
				CASE 2
					SET_VEHICLE_COLOURS(vehID, 27, 0)
				BREAK
			ENDSWITCH
		BREAK
		
		CASE CSS_CC_VARIATION_3
			SWITCH iVehicle
				CASE 2
					SET_VEHICLE_COLOURS(vehID, 64, 0)
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

PROC SET_STAFF_PROBLEMS_VEHICLE_COSMETICS(INT iVehicle, VEHICLE_INDEX vehId)

	UNUSED_PARAMETER(iVehicle)
	VEHICLE_SETUP_STRUCT_MP sData

	SWITCH iVehicle
		CASE 0
		
			sData.VehicleSetup.eModel = SANCHEZ2
			sData.VehicleSetup.tlPlateText = "23RNV734"
			sData.VehicleSetup.iPlateIndex = 4
			sData.VehicleSetup.iColour1 = 30
			sData.VehicleSetup.iColour2 = 2
			sData.VehicleSetup.iColourExtra1 = 36
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 6
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_BULLETPROOF_TYRES)
			sData.VehicleSetup.iModIndex[MOD_ENGINE] = 4
			sData.VehicleSetup.iModIndex[MOD_BRAKES] = 3
			sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 3
			sData.VehicleSetup.iModIndex[MOD_ARMOUR] = 5
			SET_VEHICLE_SETUP_MP(vehID, sData)

			// Hide before cutscene
			SET_ENTITY_HIDING_FLAGS(vehID)
			SET_VEHICLE_TYRES_CAN_BURST(vehId, FALSE)
			SET_VEHICLE_ALLOW_HOMING_MISSLE_LOCKON_SYNCED(vehId, FALSE)
			SET_VEHICLE_ALLOW_NO_PASSENGERS_LOCKON(vehId, FALSE)
			
			SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(vehID, TRUE)
			SET_VEHICLE_DISABLE_TOWING(vehId, TRUE)
			SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(vehId, TRUE)
			SET_VEHICLE_RESPECTS_LOCKS_WHEN_HAS_DRIVER(vehId, TRUE)
			SET_VEHICLE_DOORS_LOCKED_FOR_NON_SCRIPT_PLAYERS(vehID, TRUE)
			
			SET_VEHICLE_TYRES_CAN_BURST(vehId, FALSE)
			SET_VEHICLE_STRONG(vehId, TRUE)
		BREAK
		CASE 1
			SET_VEHICLE_ENGINE_ON(vehID, TRUE, TRUE)
			SET_VEHICLE_DAMAGE_SCALE(vehID, 1.2)
			SET_VEHICLE_BODY_HEALTH(vehID, 700.0)
			
			SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(vehID, TRUE)
			SET_VEHICLE_DISABLE_TOWING(vehId, TRUE)
			SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(vehId, TRUE)
			SET_VEHICLE_RESPECTS_LOCKS_WHEN_HAS_DRIVER(vehId, TRUE)
			SET_VEHICLE_DOORS_LOCKED_FOR_NON_SCRIPT_PLAYERS(vehID, TRUE)
		BREAK
		CASE 3
			sData.VehicleSetup.eModel = SANCHEZ2
			sData.VehicleSetup.tlPlateText = "23RNV734"
			sData.VehicleSetup.iPlateIndex = 4
			sData.VehicleSetup.iColour1 = 89
			sData.VehicleSetup.iColour2 = 89
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 6
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_BULLETPROOF_TYRES)
			sData.VehicleSetup.iModIndex[MOD_ENGINE] = 1
			SET_VEHICLE_SETUP_MP(vehID, sData)
		BREAK
	ENDSWITCH
	
	IF GET_ENTITY_MODEL(vehID) = DINGHY
		SET_BOAT_ANCHOR(vehID, TRUE)
	ENDIF

ENDPROC

PROC SET_NON_MISSION_ENTITY_VEHICLE_ATTRIBUTES_FOR_SPAWN(INT iVehicle)
	
	VEHICLE_INDEX vehId = NET_TO_VEH(serverBD.sVehicle[iVehicle].netId)
	
	IF IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_INSIDE_MISSION_INTERIOR)
		NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(NET_TO_ENT(serverBD.sVehicle[iVehicle].netId), TRUE)
	ENDIF
	
	// Reduce turbulence for mission planes
	IF IS_VEHICLE_A_SUPPORT_VEHICLE(iVehicle)
	OR IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_I_AM_CARRIER_VEHICLE)
		IF IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(vehId))
			SET_PLANE_TURBULENCE_MULTIPLIER(vehId, 0.0)
			PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_NON_MISSION_ENTITY_VEHICLE_ATTRIBUTES_FOR_SPAWN - SET_PLANE_TURBULENCE_MULTIPLIER to 0 for vehicle ",iVehicle)
		ENDIF
	ENDIF
	
	IF DECOR_IS_REGISTERED_AS_TYPE("Not_Allow_As_Saved_Veh", DECOR_TYPE_INT)
    	DECOR_SET_INT(vehId, "Not_Allow_As_Saved_Veh", MP_NOT_ALLOWED_AS_SAVED_VEHICLE_DEC_SIMEON)
		PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] SET_NON_MISSION_ENTITY_VEHICLE_ATTRIBUTES_FOR_SPAWN - Set decorator 'Not_Allow_As_Saved_Veh' on vehicle #", iVehicle)
	ENDIF
	
	IF IS_VEHICLE_A_SUPPORT_VEHICLE(iVehicle)
		SET_SUPPORT_VEHICLE_SPAWN_ATTRIBUTES(iVehicle, vehId)
		EXIT
	ENDIF

	IF SHOULD_SET_UP_CARRIER_VEHICLE_SPAWN_ATTRIBUTES(iVehicle)
		SET_CARRIER_VEHICLE_SPAWN_ATTRIBUTES(iVehicle, vehId)
		EXIT
	ENDIF
	
	SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(vehId, TRUE)
	SET_VEHICLE_RESPECTS_LOCKS_WHEN_HAS_DRIVER(vehId, TRUE)
	SET_VEHICLE_DOORS_LOCKED_FOR_NON_SCRIPT_PLAYERS(vehID, TRUE)
	
	// Add in mission specific vehicle attributes here
	SWITCH GET_GBC_VARIATION()
		CASE CSV_UNDER_THE_INFLUENCE						
			SET_VEHICLE_ENGINE_ON(vehID, TRUE, TRUE)
			SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(vehID, TRUE)
			PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] SET_NON_MISSION_ENTITY_VEHICLE_ATTRIBUTES_FOR_SPAWN - SET_VEHICLE_ENGINE_ON on vehicle #", iVehicle)
		BREAK
		CASE CSV_BODY_DISPOSAL
			SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(vehID, TRUE)
			SET_VEHICLE_ENGINE_ON(vehID, TRUE, TRUE)
			PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] SET_NON_MISSION_ENTITY_VEHICLE_ATTRIBUTES_FOR_SPAWN - SET_VEHICLE_ENGINE_ON on vehicle #", iVehicle)
		BREAK
		CASE CSV_TRACKING_CHIPS
			SET_TRACKING_CHIPS_VEHICLE_COSMETICS(vehID)
		BREAK
		CASE CSV_PASSED_OUT
			SWITCH GET_ENTITY_MODEL(vehID)
				CASE AMBULANCE
					SET_VEHICLE_DOOR_OPEN(vehID, SC_DOOR_REAR_LEFT, FALSE, TRUE)
					SET_VEHICLE_DOOR_OPEN(vehID, SC_DOOR_REAR_RIGHT, FALSE, TRUE)
					SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehID, SC_DOOR_REAR_LEFT, FALSE)
					SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehID, SC_DOOR_REAR_RIGHT, FALSE)
					SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(vehID, TRUE)
				BREAK
				CASE PRAIRIE
					SET_ENTITY_ROTATION(vehID, <<0.0, 180.0, 0.0>>)
					SET_VEHICLE_DOOR_OPEN(vehID, SC_DOOR_FRONT_LEFT, TRUE, TRUE)
					SET_VEHICLE_ENGINE_ON(vehID, TRUE, TRUE)
					SET_DISABLE_VEHICLE_ENGINE_FIRES(vehID, FALSE)
					SET_VEHICLE_ENGINE_HEALTH(vehID, 3)
				BREAK
				CASE JOURNEY
					SET_ENTITY_ROTATION(vehID, <<-10.0, 160.0, 0.0>>)
					SET_VEHICLE_DOOR_OPEN(vehID, SC_DOOR_FRONT_LEFT, TRUE, TRUE)
					SET_VEHICLE_ENGINE_ON(vehID, TRUE, TRUE)
					SET_DISABLE_VEHICLE_ENGINE_FIRES(vehID, FALSE)
					SET_VEHICLE_ENGINE_HEALTH(vehID, 3)
				BREAK
			ENDSWITCH
		BREAK
		CASE CSV_HEISTERS
			SET_HEISTERS_VEHICLE_COSMETICS(iVehicle, vehID)
		BREAK
		CASE CSV_HIGH_ROLLER
			SET_HIGH_ROLLER_VEHICLE_COSMETICS(iVehicle, vehID)
		BREAK
		CASE CSV_BAD_PRESS
			SET_BAD_PRESS_VEHICLE_COSMETICS(iVehicle, vehID)
		BREAK
		CASE CSV_STAFF_PROBLEMS
			SET_STAFF_PROBLEMS_VEHICLE_COSMETICS(iVehicle, vehID)
		BREAK
		CASE CSV_DEFEND_CASINO
			SET_DEFEND_CASINO_VEHICLE_COSMETICS(iVehicle, vehID)
		BREAK
		CASE CSV_ESCORT_DUTY
			SET_ESCORT_DUTY_VEHICLE_COSMETICS(iVehicle, vehID)
		BREAK
		CASE CSV_COUNTERFEIT_CHIPS
			SET_COUNTERFEIT_CHIPS_VEHICLE_COSMETICS(iVehicle, vehID)
		BREAK
		CASE CSV_MISSING_DELIVERY
			SET_MISSING_DELIVERY_VEHICLE_COSMETICS(iVehicle, vehID)
		BREAK
		CASE CSV_SLOT_MACHINES
			SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(vehID, TRUE)
		BREAK
	ENDSWITCH
ENDPROC

PROC INITIALISE_AMBUSH_VEHICLE_BITSETS()
	IF GET_AMBUSH_VEHICLE_ARRAY_INDEX(eVEHICLEBITSET_I_AM_AMBUSH_VEHICLE_0) != (-1)
		SET_VEHICLE_BIT(GET_AMBUSH_VEHICLE_ARRAY_INDEX(eVEHICLEBITSET_I_AM_AMBUSH_VEHICLE_0), eVEHICLEBITSET_I_AM_AMBUSH_VEHICLE_0)
	ENDIF
	
	IF GET_AMBUSH_VEHICLE_ARRAY_INDEX(eVEHICLEBITSET_I_AM_AMBUSH_VEHICLE_1) != (-1)
		SET_VEHICLE_BIT(GET_AMBUSH_VEHICLE_ARRAY_INDEX(eVEHICLEBITSET_I_AM_AMBUSH_VEHICLE_1), eVEHICLEBITSET_I_AM_AMBUSH_VEHICLE_1)
	ENDIF
ENDPROC

FUNC BOOL SHOULD_SET_CARRIER_VEHICLE_BIT_FROM_START()
	RETURN TRUE
ENDFUNC

PROC INITIALISE_VEHICLE_BITSETS_FOR_MODE_START()
//	INT iSupportCount = GB_GET_GBC_NUM_SUPPORT_VEHICLES_REQUIRED(GET_GBC_VARIATION(), serverBD.iOrganisationSizeOnLaunch)
	
	IF DOES_GBC_VARIATION_HAVE_AMBUSH(GET_GBC_VARIATION(), GET_GBC_SUBVARIATION())
		INITIALISE_AMBUSH_VEHICLE_BITSETS()
	ENDIF
	
	// set carrier bits
	INT iMissionEntity
	REPEAT GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() iMissionEntity
		IF DOES_MISSION_ENTITY_HAVE_CARRIER_VEHICLE(iMissionEntity)
		AND SHOULD_SET_CARRIER_VEHICLE_BIT_FROM_START()
			SET_VEHICLE_BIT(serverBD.iCarrierVehicle[iMissionEntity], eVEHICLEBITSET_I_AM_CARRIER_VEHICLE)
		ENDIF
	ENDREPEAT
	
	SWITCH GET_GBC_VARIATION()
		CASE CSV_BODY_DISPOSAL
			SET_VEHICLE_BIT(1, eVEHICLEBITSET_I_AM_POLICE_CAR)
			SET_VEHICLE_BIT(2, eVEHICLEBITSET_I_AM_POLICE_CAR)
			SET_VEHICLE_BIT(3, eVEHICLEBITSET_I_AM_POLICE_CAR)
			SET_VEHICLE_BIT(4, eVEHICLEBITSET_I_AM_POLICE_CAR)
			SET_VEHICLE_BIT(5, eVEHICLEBITSET_I_AM_POLICE_CAR)
			SET_VEHICLE_BIT(6, eVEHICLEBITSET_I_AM_POLICE_CAR)
			SET_VEHICLE_BIT(7, eVEHICLEBITSET_I_AM_POLICE_CAR)
			SET_VEHICLE_BIT(8, eVEHICLEBITSET_I_AM_POLICE_CAR)
			SET_VEHICLE_BIT(9, eVEHICLEBITSET_I_AM_POLICE_CAR)
			SET_VEHICLE_BIT(10, eVEHICLEBITSET_I_AM_POLICE_CAR)
		BREAK
		
		CASE CSV_UNDER_THE_INFLUENCE
			SET_VEHICLE_BIT(0, eVEHICLEBITSET_I_AM_POLICE_CAR)
			SET_VEHICLE_BIT(1, eVEHICLEBITSET_I_AM_POLICE_CAR)
			SET_VEHICLE_BIT(2, eVEHICLEBITSET_I_AM_POLICE_CAR)
			SET_VEHICLE_BIT(3, eVEHICLEBITSET_I_AM_POLICE_CAR)
			SET_VEHICLE_BIT(4, eVEHICLEBITSET_I_AM_POLICE_CAR)
			SET_VEHICLE_BIT(5, eVEHICLEBITSET_I_AM_POLICE_CAR)
			SET_VEHICLE_BIT(6, eVEHICLEBITSET_I_AM_POLICE_CAR)
			SET_VEHICLE_BIT(7, eVEHICLEBITSET_I_AM_POLICE_CAR)
			SET_VEHICLE_BIT(8, eVEHICLEBITSET_I_AM_POLICE_CAR)
			SET_VEHICLE_BIT(9, eVEHICLEBITSET_I_AM_POLICE_CAR)
			SET_VEHICLE_BIT(10, eVEHICLEBITSET_I_AM_POLICE_CAR)
			SET_VEHICLE_BIT(11, eVEHICLEBITSET_I_AM_POLICE_CAR)
			SET_VEHICLE_BIT(12, eVEHICLEBITSET_I_AM_POLICE_CAR)
			SET_VEHICLE_BIT(13, eVEHICLEBITSET_I_AM_POLICE_CAR)
			SET_VEHICLE_BIT(14, eVEHICLEBITSET_I_AM_POLICE_CAR)
		BREAK
		CASE CSV_BAD_PRESS
			SET_VEHICLE_BIT(0, eVEHICLEBITSET_POTENTIAL_TARGET_VEHICLE)
			SET_VEHICLE_BIT(1, eVEHICLEBITSET_POTENTIAL_TARGET_VEHICLE)
			SET_VEHICLE_BIT(2, eVEHICLEBITSET_POTENTIAL_TARGET_VEHICLE)
			SET_VEHICLE_BIT(3, eVEHICLEBITSET_POTENTIAL_TARGET_VEHICLE)
			SET_VEHICLE_BIT(4, eVEHICLEBITSET_POTENTIAL_TARGET_VEHICLE)
		BREAK
		CASE CSV_STAFF_PROBLEMS
			SET_VEHICLE_BIT(0, eVEHICLEBITSET_WARP_VEHICLE_ON_STUCK)
		BREAK
		CASE CSV_PASSED_OUT
			SET_VEHICLE_BIT(0, eVEHICLEBITSET_WARP_VEHICLE_ON_STUCK)
		BREAK
		CASE CSV_ESCORT_DUTY
			SET_VEHICLE_BIT(serverBD.iCarrierVehicle[MISSION_ENTITY_ONE], eVEHICLEBITSET_WARP_VEHICLE_ON_STUCK)
		BREAK
	ENDSWITCH

ENDPROC

PROC RESET_SERVER_NON_MISSION_ENTITY_VEHICLE_SPAWN_DATA()
	serverBd.iCurrentNonMissionEntityVehicleSearchingForSpawnCoords = (-1)
	serverBd.vSpawnNonMissionEntityVehicleCoords = << 0.0, 0.0, 0.0 >>
	serverBd.fSpawnNonMissionEntityVehicleHeading = 0.0
	serverBd.iGetSpawnNonMissionEntityVehicleCoordsStage = 0
	serverBd.iCircCount = 0
	serverBD.vVehicleSpawnCoord = << 0.0, 0.0, 0.0 >>
	PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - RESET_SERVER_NON_MISSION_ENTITY_VEHICLE_SPAWN_DATA - Vehicle data cleared.")
ENDPROC

FUNC BOOL SHOULD_DELETE_VEHICLE_ON_CLEANUP(INT iVehicle)

	UNUSED_PARAMETER(iVehicle)
	
	RETURN FALSE

ENDFUNC

PROC CLEANUP_VEHICLES()
	
	INT i
	
	REPEAT NUM_VARIATION_VEHICLES i
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[i].netId)
			//IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[i].netId)
				IF NET_TO_VEH(serverBD.sVehicle[i].netId) = GET_LAST_DRIVEN_VEHICLE()
					PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] CLEANUP_VEHICLES - non-MissionEntity vehicle ", i, " was last driven vehicle, clearing last driven vehicle")
					CLEAR_LAST_DRIVEN_VEHICLE()
				ENDIF
				IF SHOULD_DELETE_VEHICLE_ON_CLEANUP(i)
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[i].netId)
						DELETE_NET_ID(serverBD.sVehicle[i].netId)
						PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] CLEANUP_VEHICLES - set non-MissionEntity vehicle ", i, " as deleted")
					ENDIF
				ELSE
					CLEANUP_NET_ID(serverBD.sVehicle[i].netId)
					PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] CLEANUP_VEHICLES - set non-MissionEntity vehicle ", i, " as no longer needed")
				ENDIF
			//ENDIF
		ENDIF
	ENDREPEAT	
	
ENDPROC

FUNC BOOL SHOULD_VEHICLE_BE_CREATED_AT_START(INT iVehicle)
	IF IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_I_AM_CARRIER_VEHICLE)
		RETURN FALSE
	ENDIF
	
	SWITCH GET_GBC_VARIATION()
		CASE CSV_HIGH_ROLLER
			SWITCH GET_GBC_SUBVARIATION()
				CASE CSS_HR_SANDY_SHORES
				CASE CSS_HR_MARINA
					SWITCH iVehicle
						CASE 1	RETURN serverBD.iOrganisationSizeOnLaunch > 1
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE CSV_DEFEND_CASINO
			SWITCH iVehicle
				CASE 2
				CASE 4
				CASE 5	RETURN FALSE
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_CREATE_CARRIER_VEHICLES()
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_VEHICLES_BE_CREATED_ON_MISSION_START()

	SWITCH GET_GBC_VARIATION()
		CASE CSV_MAX
			RETURN FALSE
	ENDSWITCH

	RETURN TRUE

ENDFUNC

FUNC BOOL SHOULD_VEHICLES_BE_CREATED_MID_MISSION()
	SWITCH GET_GBC_VARIATION()
		CASE CSV_DEFEND_CASINO	RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_VEHICLE_BE_CREATED_MID_MISSION(INT iVehicle)
	SWITCH GET_GBC_VARIATION()
		CASE CSV_DEFEND_CASINO
			SWITCH iVehicle
				CASE 2
					IF IS_SERVER_BIT_SET(eSERVERBITSET_TARGETS_ELIMINATED)
					AND NOT SHOULD_PEDS_GROUP_REACT(9) //Create this vehicle only if the hideout has not already been alerted.
						RETURN TRUE
					ENDIF
				BREAK
				
				CASE 4
				CASE 5
					IF SHOULD_PEDS_GROUP_REACT(8)
					AND NOT SHOULD_PEDS_GROUP_REACT(9)
						//Create these vehicles after a delay only if the Speedo driver has been alerted, but the hideout has not.
						RETURN HAS_NET_TIMER_EXPIRED(serverBD.stEscortSpawnDelay, ciESCORT_SPAWN_DELAY_MS)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_ACTIVATING_NON_MISSION_ENTITY_VEHICLES()
	
	INT iVehicle
	
	// Ambush vehicle activation
	// Shouldn't need to touch this, just add your mission to the wrappers
	IF DOES_GBC_VARIATION_HAVE_AMBUSH(GET_GBC_VARIATION(), GET_GBC_SUBVARIATION())
		IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_ACTIVATED_VEHICLES_FOR_AMBUSH)
		AND SHOULD_AMBUSH_ACTIVATE()
			IF GET_AMBUSH_VEHICLE_ARRAY_INDEX(eVEHICLEBITSET_I_AM_AMBUSH_VEHICLE_0) != (-1)
				SET_VEHICLE_STATE(GET_AMBUSH_VEHICLE_ARRAY_INDEX(eVEHICLEBITSET_I_AM_AMBUSH_VEHICLE_0), eVEHICLESTATE_CREATE)
			ENDIF
			IF GET_AMBUSH_VEHICLE_ARRAY_INDEX(eVEHICLEBITSET_I_AM_AMBUSH_VEHICLE_1) != (-1)
				SET_VEHICLE_STATE(GET_AMBUSH_VEHICLE_ARRAY_INDEX(eVEHICLEBITSET_I_AM_AMBUSH_VEHICLE_1), eVEHICLESTATE_CREATE)
			ENDIF
			SET_SERVER_BIT(eSERVERBITSET_ACTIVATED_VEHICLES_FOR_AMBUSH)
		ENDIF
	ENDIF
	
	// Bunker Support Vehicles
	IF DOES_GBC_VARIATION_HAVE_SUPPORT_VEHICLES(GET_GBC_VARIATION(), GET_GBC_SUBVARIATION())
		IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_ACTIVATED_SUPPORT_VEHICLES)
			IF GB_GET_GBC_NUM_SUPPORT_VEHICLES_REQUIRED(GET_GBC_VARIATION(), serverBD.iOrganisationSizeOnLaunch) > 0
				REPEAT NUM_VARIATION_VEHICLES iVehicle
					IF IS_VEHICLE_A_SUPPORT_VEHICLE(iVehicle)
						SET_VEHICLE_STATE(iVehicle, eVEHICLESTATE_CREATE)
					ENDIF
				ENDREPEAT
				SET_SERVER_BIT(eSERVERBITSET_ACTIVATED_SUPPORT_VEHICLES)
			ENDIF
		ENDIF
	ENDIF
	
	// Carrier vehicles
	IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_ACTIVATED_CARRIER_VEHICLES)
		IF SHOULD_CREATE_CARRIER_VEHICLES()
			IF DOES_MISSION_ENTITY_HAVE_CARRIER_VEHICLE(0)	// Carrier vehicles are used on this mission
				REPEAT NUM_VARIATION_VEHICLES iVehicle
					IF IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_I_AM_CARRIER_VEHICLE)
						SET_VEHICLE_STATE(iVehicle, eVEHICLESTATE_CREATE)
					ENDIF
				ENDREPEAT
				SET_SERVER_BIT(eSERVERBITSET_ACTIVATED_CARRIER_VEHICLES)
			ENDIF
		ENDIF
	ENDIF
	
	IF SHOULD_VEHICLES_BE_CREATED_ON_MISSION_START()
		IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_ACTIVATED_VEHICLES_FOR_MISSION_START)
		AND (HAVE_ALL_MISSION_ENTITIES_BEEN_CREATED()
		OR ARE_MISSION_ENTITIES_MADE_DURING_MISSION())

			REPEAT GB_GET_GBC_NUM_VEH_REQUIRED(GET_GBC_VARIATION(), GET_GBC_SUBVARIATION(), GB_GET_LOCAL_PLAYER_GANG_BOSS(), TRUE, GET_EXTRA_PARAM_FOR_VEHICLE_SPAWN(), TRUE, TRUE) iVehicle
				IF SHOULD_VEHICLE_BE_CREATED_AT_START(iVehicle)
					SET_VEHICLE_STATE(iVehicle, eVEHICLESTATE_CREATE)
				ENDIF
			ENDREPEAT
			SET_SERVER_BIT(eSERVERBITSET_ACTIVATED_VEHICLES_FOR_MISSION_START)
		ENDIF
	ENDIF
	
	IF SHOULD_VEHICLES_BE_CREATED_MID_MISSION()
		IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_ACTIVATED_VEHICLES_MID_MISSION)
			BOOL bAllCreated = TRUE
			
			REPEAT GB_GET_GBC_NUM_VEH_REQUIRED(GET_GBC_VARIATION(), GET_GBC_SUBVARIATION(), GB_GET_LOCAL_PLAYER_GANG_BOSS(), TRUE, GET_EXTRA_PARAM_FOR_VEHICLE_SPAWN(), TRUE, TRUE) iVehicle
				IF GET_VEHICLE_STATE(iVehicle) = eVEHICLESTATE_INACTIVE
					IF SHOULD_VEHICLE_BE_CREATED_MID_MISSION(iVehicle)
						SET_VEHICLE_STATE(iVehicle, eVEHICLESTATE_CREATE)
					ELSE
						bAllCreated = FALSE
					ENDIF
				ENDIF
			ENDREPEAT
			
			IF bAllCreated
				SET_SERVER_BIT(eSERVERBITSET_ACTIVATED_VEHICLES_MID_MISSION)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_SPAWN_VARIATION_VEHICLES()
	RETURN TRUE
ENDFUNC

PROC RESET_VEHICLE_DATA_FOR_RESPAWN(INT iVehicle)

	IF DOES_GBC_VARIATION_HAVE_AMBUSH(GET_GBC_VARIATION(), GET_GBC_SUBVARIATION())
		serverBd.iCurrentNonMissionEntityVehicleSearchingForSpawnCoords = (-1)
		serverBd.vSpawnNonMissionEntityVehicleCoords = << 0.0, 0.0, 0.0 >>
		serverBd.fSpawnNonMissionEntityVehicleHeading = 0.0
		ServerBd.iAmbushGetNonMissionEntityCoordsAttempts = 0
		serverBd.iGetSpawnNonMissionEntityVehicleCoordsStage = 0
		RESET_MISSION_ENTITY_TARGET_FOR_VEHICLE(iVehicle)
		
		PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - RESET_VEHICLE_DATA_FOR_RESPAWN - Vehicle = ", iVehicle)
	ENDIF
	
ENDPROC

FUNC BOOL DOES_VARIATION_HAVE_VARIATION_VEHICLES()
	IF IS_THIS_VARIATION_A_SELL_MISSION()
		RETURN TRUE
	ENDIF
	
	IF GB_GET_GBC_NUM_SUPPORT_VEHICLES_REQUIRED(GET_GBC_VARIATION(), serverBD.iOrganisationSizeOnLaunch) > 0
		RETURN TRUE
	ENDIF
	
	IF DOES_GBC_VARIATION_HAVE_AMBUSH(GET_GBC_VARIATION(), GET_GBC_SUBVARIATION())
		RETURN TRUE
	ENDIF
	
	IF GET_GBC_CARRIER_VEHICLE_INDEX(0, GET_GBC_SUBVARIATION()) != -1
		RETURN TRUE
	ENDIF
	
	// Add your mission here if it has vehicles
	SWITCH GET_GBC_VARIATION()
		CASE CSV_STAFF_PROBLEMS
		CASE CSV_UNDER_THE_INFLUENCE
		CASE CSV_BODY_DISPOSAL
		CASE CSV_TRACKING_CHIPS
		CASE CSV_RECOVER_LUXURY_CAR
		CASE CSV_HEISTERS
		CASE CSV_BAD_PRESS
		CASE CSV_SLOT_MACHINES
		CASE CSV_MISSING_DELIVERY
		CASE CSV_DEFEND_CASINO
		CASE CSV_ESCORT_DUTY
		CASE CSV_COUNTERFEIT_CHIPS
		CASE CSV_INTIMIDATE_JUDGE
		CASE CSV_HIGH_ROLLER_TOUR
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_USE_GROUND_Z_FOR_AIR_SPAWNING()
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_CONSIDER_HIGHWAYS_FOR_VARIATION(INT iVehicle)
	UNUSED_PARAMETER(iVehicle)
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_ALLOW_OFF_ROAD_NODES(INT iVehicle)
	UNUSED_PARAMETER(iVehicle)
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_IGNORE_CUSTOM_NODES()
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_SPREAD_VEHICLES_APART()
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_DROP_OFF_NEED_LARGER_SPAWN_SEARCH_AREA()
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_DROP_OFF_NEED_CUSTOM_SPAWN_SEARCH_AREA()
	RETURN FALSE
ENDFUNC

FUNC FLOAT GET_DROP_OFF_CUSTOM_MAX_SPAWN_SEARCH_AREA_SIZE()
	RETURN 0.0
ENDFUNC

FUNC FLOAT GET_DROP_OFF_CUSTOM_MIN_SPAWN_SEARCH_AREA_SIZE()
	RETURN 0.0
ENDFUNC

FUNC VECTOR GET_FAVOURED_FACING(VECTOR vSpawnCoords)
	UNUSED_PARAMETER(vSpawnCoords)
	RETURN vSpawnCoords
ENDFUNC

FUNC FLOAT GET_VEHICLE_SPAWN_LOCATION_MAX_DISTANCE(BOOL bLargerSize = FALSE)
	UNUSED_PARAMETER(bLargerSize)
	RETURN 150.0
ENDFUNC

FUNC FLOAT GET_VEHICLE_SPAWN_LOCATION_MIN_DISTANCE(BOOL bLargerSize = FALSE)
	UNUSED_PARAMETER(bLargerSize)
	RETURN 0.0
ENDFUNC

FUNC VECTOR GET_RANDOM_SPAWN_COORD_FOR_VEHICLE(INT iVehicle, VECTOR vCentralCoord)
	INT iXCoord, iYCoord
	VECTOR vNewVector
	INT iMaxDist = 600
	INT iMinDist = 250
	INT iMinDistBetween = 200
	INT iLoop
	INT iNumVehs = GB_GET_GBC_NUM_VEH_REQUIRED(GET_GBC_VARIATION(), GET_GBC_SUBVARIATION(), GB_GET_LOCAL_PLAYER_GANG_BOSS(), TRUE, GET_EXTRA_PARAM_FOR_VEHICLE_SPAWN(), TRUE, TRUE)
	FLOAT fDistBetween
	VECTOR vTempCoord
	
	// Get a random number between -iMaxDist and iMaxDist to get the x coord
	iXCoord = GET_RANDOM_INT_IN_RANGE((iMaxDist - (iMaxDist*2)), iMaxDist)
	
	// Get a random number between the rest of the value between xCoord and iMaxDist for the y coord
	iYCoord = GET_RANDOM_INT_IN_RANGE((iMaxDist - iXCoord), iMaxDist)
	
	// Should this go positive or negative on y from the x coord?
	iYCoord = PICK_INT(GET_RANDOM_BOOL(), iYCoord, (iYCoord - (iYCoord*2)))
	
	// Now get the new vector based on the grabbed coord offsets from the central coord
	vNewVector = <<vCentralCoord.x+iXCoord, vCentralCoord.y+iYCoord, 0.0>>
	vNewVector.z = GET_APPROX_HEIGHT_FOR_POINT(vNewVector.x, vNewVector.y)
	
	fDistBetween = GET_DISTANCE_BETWEEN_COORDS(vNewVector, vCentralCoord, FALSE)
	IF fDistBetween < iMinDist
		PRINTLN("GET_RANDOM_SPAWN_COORD_FOR_VEHICLE - vNewVector (", vNewVector, ") is too close to the centre of the search area (",vCentralCoord,"), fDistBetween = ", fDistBetween)
		vNewVector = <<0.0, 0.0, 0.0>>
	ENDIF
	
	// Dont bother looping through other vehicles, the coord we tried is too close to the centre of the search area
	IF NOT IS_VECTOR_ZERO(vNewVector)
	
		// Check to make sure its not near any of the other vehicles that have been created so far
		REPEAT iNumVehs iLoop
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[iLoop].netId)
				vTempCoord = GET_ENTITY_COORDS(NET_TO_VEH(serverBD.sVehicle[iLoop].netId), FALSE)
				fDistBetween = GET_DISTANCE_BETWEEN_COORDS(vTempCoord, vNewVector, FALSE)
				PRINTLN("GET_RANDOM_SPAWN_COORD_FOR_VEHICLE - fDistBetween vehicle ", iVehicle, " and ", iLoop, " = ", fDistBetween)
				IF fDistBetween < iMinDistBetween
					PRINTLN("GET_RANDOM_SPAWN_COORD_FOR_VEHICLE - new vector at ", vNewVector," is too close to vehicle ", iLoop, " located at ", vTempCoord)
					vNewVector = <<0.0, 0.0, 0.0>>
					BREAKLOOP
				ENDIF
			ELSE
				IF iLoop > 0
				AND iLoop < iVehicle
					PRINTLN("GET_RANDOM_SPAWN_COORD_FOR_VEHICLE - vehicle ", iLoop, " doesnt exist, cannot compare them")
					vNewVector = <<0.0, 0.0, 0.0>>
					BREAKLOOP
				ENDIF
			ENDIF
		ENDREPEAT
	
	ENDIF
	
	IF IS_VECTOR_ZERO(vNewVector)
		PRINTLN("GET_RANDOM_SPAWN_COORD_FOR_VEHICLE - returning zero vector")
	ENDIF
	
	RETURN vNewVector
ENDFUNC

FUNC BOOL SHOULD_VEHICLE_BE_INVINCIBLE_AT_START(INT iVeh)
	
	SWITCH GET_GBC_VARIATION()
		CASE CSV_DEFEND_CASINO
			SWITCH iVeh
				CASE 2	RETURN TRUE
			ENDSWITCH
		BREAK
		CASE CSV_INTIMIDATE_JUDGE
			RETURN FALSE
	ENDSWITCH
	
	IF SHOULD_SET_UP_CARRIER_VEHICLE_SPAWN_ATTRIBUTES(iVeh)
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL CREATE_NET_NON_MISSION_ENTITY_VEHICLE(MODEL_NAMES vehModel, INT iVehicle)

	RETURN CREATE_NET_VEHICLE(serverBD.sVehicle[iVehicle].netId, vehModel, serverBd.vSpawnNonMissionEntityVehicleCoords, serverBd.fSpawnNonMissionEntityVehicleHeading)	

ENDFUNC

FUNC VECTOR GET_CREATE_FROM_OFFSET_VEHICLE_COORD(INT iVehicle)
	SWITCH GET_GBC_VARIATION()
		CASE CSV_MAX		
			SWITCH iVehicle
				CASE 0		RETURN <<0.0, 0.0, 0.0>>
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC FLOAT GET_CREATE_FROM_OFFSET_VEHICLE_HEADING(INT iVehicle)
	SWITCH GET_GBC_VARIATION()
		CASE CSV_MAX
			SWITCH iVehicle	
				CASE 0			RETURN 0.0
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN 0.0
ENDFUNC

FUNC VECTOR GET_CREATE_FROM_OFFSET_OFFSET(INT iVehicle)
	SWITCH GET_GBC_VARIATION()
		CASE CSV_MAX
			SWITCH iVehicle	
				CASE 0			RETURN <<0.0,0.0,0.0>>
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC BOOL SHOULD_VEHICLE_USE_SHARED_SPAWN_COORD(INT iVehicle)
	IF IS_VEHICLE_A_SUPPORT_VEHICLE(iVehicle)
		RETURN TRUE
	ENDIF
	
	IF IS_VEHICLE_A_CARRIER_VEHICLE(iVehicle)
	AND DOES_VARIATION_USE_SHARED_SPAWN_COORDS_FOR_CARRIER_VEHICLES(GET_GBC_VARIATION())
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC FLOAT GET_NON_MISSION_ENTITY_VEHICLE_SPAWN_DISTANCE(INT iVehicle)
	SWITCH GET_GBC_VARIATION()
		CASE CSV_DEFEND_CASINO	
			SWITCH iVehicle
				CASE 4
				CASE 5	RETURN 125.0
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN 200.0
ENDFUNC

FUNC BOOL CREATE_NON_MISSION_ENTITY_VEHICLE_CREATION(INT iVehicle, MODEL_NAMES vehModel, BOOL bShouldFindSpawnLocation, BOOL bShouldSpawnInAir, BOOL bDoOkCheck, BOOL bCreateFromOffset)
	
	IF IS_VEHICLE_AN_AMBUSH_VEHICLE(iVehicle)
		CREATE_AMBUSH_VEHICLE(iVehicle)
	ELSE
		IF serverBD.iCurrentPedSearchingForSpawnCoords = -1
			IF serverBd.iCurrentNonMissionEntityVehicleSearchingForSpawnCoords = (-1)
			OR serverBd.iCurrentNonMissionEntityVehicleSearchingForSpawnCoords = iVehicle
			
				IF serverBd.iCurrentNonMissionEntityVehicleSearchingForSpawnCoords != iVehicle
					serverBd.iCurrentNonMissionEntityVehicleSearchingForSpawnCoords = iVehicle
					PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - CREATE_NON_MISSION_ENTITY_VEHICLE_CREATION - Vehicle #", iVehicle, " now searching for spawn coordinates.")
				ENDIF
				
				IF IS_VECTOR_ZERO(serverBd.vSpawnNonMissionEntityVehicleCoords)
					IF SHOULD_VEHICLE_USE_SHARED_SPAWN_COORD(iVehicle)
						VECTOR vCoords, vSpawnHeading
						FLOAT fHeading
						SWITCH serverBd.iGetSpawnNonMissionEntityVehicleCoordsStage
							CASE 0
								CLEAR_CUSTOM_VEHICLE_NODES()
								
								INT iLoop
								WHILE NOT IS_VECTOR_ZERO(GET_GBC_SHARED_VEHICLE_SPAWN_COORD(GET_GBC_VARIATION(), iLoop, IS_VEHICLE_A_SUPPORT_VEHICLE(iVehicle)))
									vCoords = GET_GBC_SHARED_VEHICLE_SPAWN_COORD(GET_GBC_VARIATION(), iLoop, IS_VEHICLE_A_SUPPORT_VEHICLE(iVehicle))
									fHeading = GET_GBC_SHARED_VEHICLE_SPAWN_HEADING(GET_GBC_VARIATION(), iLoop, IS_VEHICLE_A_SUPPORT_VEHICLE(iVehicle))
									IF NOT IS_VECTOR_ZERO(vCoords)
										ADD_CUSTOM_VEHICLE_NODE(vCoords, fHeading)		
										PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] [SHARED_SPAWN] - CREATE_NON_MISSION_ENTITY_VEHICLE_CREATION - Adding custom vehicle node ", iLoop, " at ", vCoords, " for vehicle #", iVehicle)
									ENDIF
									iLoop++
								ENDWHILE	
								serverBd.iGetSpawnNonMissionEntityVehicleCoordsStage++
								PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] [SHARED_SPAWN] - CREATE_NON_MISSION_ENTITY_VEHICLE_CREATION - Added custom vehicle nodes. Moving on to find spawn coord.")
							BREAK
							
							CASE 1
								VEHICLE_SPAWN_LOCATION_PARAMS Params
								Params.bEnforceMinDistForCustomNodes 		= FALSE
								Params.bStartOfMissionPVSpawn 				= FALSE
								Params.bIgnoreCustomNodesForArea 			= TRUE
								Params.bAvoidSpawningInExclusionZones  		= FALSE
								Params.bIgnoreCustomNodesForMissionLaunch 	= TRUE
								Params.bCheckEntityArea 					= TRUE
								
								vCoords = GET_GBC_SHARED_VEHICLE_SPAWN_COORD(GET_GBC_VARIATION(), iLoop, IS_VEHICLE_A_SUPPORT_VEHICLE(iVehicle))
								vSpawnHeading = <<0.0, 0.0, 0.0>>
								
								IF HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(vCoords, vSpawnHeading, vehModel, FALSE, serverBd.vSpawnNonMissionEntityVehicleCoords, serverBd.fSpawnNonMissionEntityVehicleHeading, Params)
									PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] [SHARED_SPAWN] - CREATE_NON_MISSION_ENTITY_VEHICLE_CREATION - Vehicle #", iVehicle, " has got safe spawn at ", serverBd.vSpawnNonMissionEntityVehicleCoords)
									serverBd.iGetSpawnNonMissionEntityVehicleCoordsStage++
								ELSE
									PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] [SHARED_SPAWN] - CREATE_NON_MISSION_ENTITY_VEHICLE_CREATION - Failed to find spawn for vehicle #", iVehicle)
								ENDIF
							BREAK
							
							CASE 2
								
							BREAK
						ENDSWITCH
					ELSE
						IF bDoOkCheck
							VECTOR vCoords = GET_GBC_VEHICLE_SPAWN_COORDS(GET_GBC_VARIATION(), GET_GBC_SUBVARIATION(), iVehicle, DEFAULT, GET_EXTRA_PARAM_FOR_VEHICLE_SPAWN(iVehicle), GET_RESPAWN_LOCATION())
							FLOAT fRadius = GET_RADIUS_FROM_CONTRABAND_ENTITY(vehModel)
							IF NOT IS_POINT_OK_FOR_NET_ENTITY_CREATION(vCoords,fRadius,fRadius,fRadius,0.0,FALSE,FALSE,FALSE,0.0,FALSE,DEFAULT,FALSE,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
								bShouldFindSpawnLocation = TRUE
								PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - CREATE_NON_MISSION_ENTITY_VEHICLE_CREATION - Vehicle #", iVehicle, " - bShouldFindSpawnLocation set to true as location not safe.")
							ENDIF
						ENDIF
						
						IF bCreateFromOffset
							VECTOR vOffsetCoord = GET_CREATE_FROM_OFFSET_VEHICLE_COORD(iVehicle)
							FLOAT fOffsetHeading = GET_CREATE_FROM_OFFSET_VEHICLE_HEADING(iVehicle)
							serverBD.vSpawnNonMissionEntityVehicleCoords = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vOffsetCoord, fOffsetHeading, GET_CREATE_FROM_OFFSET_OFFSET(iVehicle)) 
							serverBd.fSpawnNonMissionEntityVehicleHeading = fOffsetHeading
							PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - CREATE_NON_MISSION_ENTITY_VEHICLE_CREATION - Vehicle #", iVehicle, " being created from offset. Offset coord: ", vOffsetCoord)
						// If we're spawning a vehicle up in the area somewhere
						ELIF bShouldSpawnInAir
							VECTOR vCoordInAir = GET_NON_MISSION_ENTITY_VEHICLE_FIND_SPAWN_CENTRE(iVehicle)
							
							IF bShouldFindSpawnLocation
								PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - CREATE_NON_MISSION_ENTITY_VEHICLE_CREATION - Vehicle #", iVehicle, " finding spawn location in air using centre point ", vCoordInAir)
								vCoordInAir = GET_OFFSET_FROM_COORD_IN_WORLD_COORDS(vCoordInAir, 0.0, <<(-1*GET_NON_MISSION_ENTITY_VEHICLE_SPAWN_DISTANCE(iVehicle) * SIN(TO_FLOAT(serverBD.iCircCount)*30)), (GET_NON_MISSION_ENTITY_VEHICLE_SPAWN_DISTANCE(iVehicle) * COS(TO_FLOAT(serverBD.iCircCount)*30)), 0.0>>)
							ENDIF
							
							// Handle special spawn cases e.g. above water
							IF SHOULD_USE_GROUND_Z_FOR_AIR_SPAWNING()
								BOOL bGotExactHeight
								IF GET_GROUND_Z_FOR_3D_COORD(vCoordInAir, vCoordInAir.z, TRUE)
									bGotExactHeight = TRUE
									PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - CREATE_NON_MISSION_ENTITY_VEHICLE_CREATION - Vehicle #", iVehicle, " got ", vCoordInAir.z, " as exact ground z.")
								ENDIF
								IF NOT bGotExactHeight
		        					vCoordInAir.z = GET_APPROX_HEIGHT_FOR_POINT(vCoordInAir.x, vCoordInAir.y)
									PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - CREATE_NON_MISSION_ENTITY_VEHICLE_CREATION - Vehicle #", iVehicle, " couldn't get exact ground z. Approx height is ", vCoordInAir.z)
								ENDIF
							ENDIF
		        			vCoordInAir.z += GET_SPAWN_IN_AIR_HEIGHT(iVehicle)
		        
		        			IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(vCoordInAir, 20.0, 1, 1, 15, TRUE, TRUE, TRUE, 180)
		            			serverBD.vSpawnNonMissionEntityVehicleCoords = vCoordInAir
								serverBd.fSpawnNonMissionEntityVehicleHeading = GET_NON_MISSION_ENTITY_VEHICLE_FIND_SPAWN_HEADING(iVehicle)
		        			ELSE
		        				serverBD.iCircCount++
					            IF serverBD.iCircCount >= 12
					                serverBD.iCircCount = 0
					            ENDIF
							ENDIF
							
						// If we're spawning a vehicle up on the ground somewhere
						ELIF bShouldFindSpawnLocation
							
							VEHICLE_SPAWN_LOCATION_PARAMS Params
							Params.bConsiderHighways = SHOULD_CONSIDER_HIGHWAYS_FOR_VARIATION(iVehicle)
							Params.bConsiderOnlyActiveNodes = !SHOULD_ALLOW_OFF_ROAD_NODES(iVehicle)
							Params.bAvoidSpawningInExclusionZones = TRUE
							Params.bAllowFallbackToInactiveNodes = FALSE
							Params.bCheckEntityArea = TRUE
							Params.fMaxDistance = GET_VEHICLE_SPAWN_LOCATION_MAX_DISTANCE()
							Params.fMinDistFromCoords = GET_VEHICLE_SPAWN_LOCATION_MIN_DISTANCE()
							Params.bIgnoreCustomNodesForArea = SHOULD_IGNORE_CUSTOM_NODES()
							VECTOR vSpawnCoords = GET_NON_MISSION_ENTITY_VEHICLE_FIND_SPAWN_CENTRE(iVehicle)
							IF NOT IS_VEHICLE_A_SUPPORT_VEHICLE(iVehicle)
								IF IS_VECTOR_ZERO(serverBD.vVehicleSpawnCoord)
									serverBD.vVehicleSpawnCoord = GET_RANDOM_SPAWN_COORD_FOR_VEHICLE(iVehicle, vSpawnCoords)
								ENDIF
								vSpawnCoords = serverBD.vVehicleSpawnCoord
							ENDIF
							IF NOT IS_VECTOR_ZERO(vSpawnCoords)
								#IF IS_DEBUG_BUILD
								IF IS_VEHICLE_A_SUPPORT_VEHICLE(iVehicle)
									PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] [SUPPORT] - CREATE_NON_MISSION_ENTITY_VEHICLE_CREATION - Vehicle #", iVehicle, " is support vehicle #", GET_SUPPORT_VEHICLE_INDEX(iVehicle), ". Should be created near ", vSpawnCoords)
								ELSE
									PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - CREATE_NON_MISSION_ENTITY_VEHICLE_CREATION - Vehicle #", iVehicle, " should be created near ", vSpawnCoords)
								ENDIF
								#ENDIF
								
								HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(vSpawnCoords, vSpawnCoords, vehModel, FALSE, serverBD.vSpawnNonMissionEntityVehicleCoords, serverBD.fSpawnNonMissionEntityVehicleHeading, Params)
							ELSE
								PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - CREATE_NON_MISSION_ENTITY_VEHICLE_CREATION - vSpawnCoords is zero vector ", vSpawnCoords)
							ENDIF
						// If we're spawning a vehicle at exact coordinates
						ELSE
							serverBd.vSpawnNonMissionEntityVehicleCoords = GET_GBC_VEHICLE_SPAWN_COORDS(GET_GBC_VARIATION(), GET_GBC_SUBVARIATION(), iVehicle, GET_CURRENT_DROP_OFF(GET_DROP_OFF_INT_FOR_SPAWN_COORDS(iVehicle)), GET_EXTRA_PARAM_FOR_VEHICLE_SPAWN(iVehicle),GET_RESPAWN_LOCATION())
							serverBd.fSpawnNonMissionEntityVehicleHeading = GET_GBC_VEHICLE_SPAWN_HEADING(GET_GBC_VARIATION(), GET_GBC_SUBVARIATION(), iVehicle, GET_CURRENT_DROP_OFF(GET_DROP_OFF_INT_FOR_SPAWN_COORDS(iVehicle)), GET_EXTRA_PARAM_FOR_VEHICLE_SPAWN(iVehicle),GET_RESPAWN_LOCATION())
							PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - CREATE_NON_MISSION_ENTITY_VEHICLE_CREATION - Vehicle #", iVehicle, " should be created at server coords ", serverBd.vSpawnNonMissionEntityVehicleCoords)
							PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - CREATE_NON_MISSION_ENTITY_VEHICLE_CREATION - Vehicle #", iVehicle, " should be created at server heading ", serverBd.fSpawnNonMissionEntityVehicleHeading)
						ENDIF
					ENDIF
				ELSE
					DELETE_VEHICLE_GEN_VEHICLES_IN_AREA(serverBD.vSpawnNonMissionEntityVehicleCoords, 30.0)
					CLEAR_AREA(serverBD.vSpawnNonMissionEntityVehicleCoords, 30.0, FALSE, FALSE, FALSE, TRUE)
					PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - CREATE_NON_MISSION_ENTITY_VEHICLE_CREATION - Calling CREATE_NET_VEHICLE on vehicle #", iVehicle, " with coords ", serverBd.vSpawnNonMissionEntityVehicleCoords)
					IF CREATE_NET_NON_MISSION_ENTITY_VEHICLE(vehModel, iVehicle)
						PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - CREATE_NON_MISSION_ENTITY_VEHICLE_CREATION - Created vehicle #", iVehicle, " with model ", GET_MODEL_NAME_FOR_DEBUG(vehModel)," at ", serverBd.vSpawnNonMissionEntityVehicleCoords)
						IF bCreateFromOffset 
							SET_ENTITY_COORDS_NO_OFFSET(NET_TO_VEH(serverBD.sVehicle[iVehicle].netId), serverBD.vSpawnNonMissionEntityVehicleCoords)
						ENDIF
						SET_VEHICLE_BITSET_DECORATOR(NET_TO_VEH(serverBD.sVehicle[iVehicle].netId), MP_DECORATOR_BS_NON_MODDABLE_VEHICLE)
						SET_VEHICLE_CANNOT_BE_STORED_IN_PROPERTIES(NET_TO_VEH(serverBD.sVehicle[iVehicle].netId))
						IF SHOULD_VEHICLE_BE_INVINCIBLE_AT_START(iVehicle)
							SET_ENTITY_INVINCIBLE(NET_TO_VEH(serverBD.sVehicle[iVehicle].netId), TRUE)
						ENDIF
						ACTIVATE_DAMAGE_TRACKER_ON_NETWORK_ID(serverBD.sVehicle[iVehicle].netId, TRUE)
						serverBD.iCurrentNonMissionEntityVehicleSearchingForSpawnCoords = (-1)
						SET_MODEL_AS_NO_LONGER_NEEDED(vehModel)
						CLEAR_VEHICLE_BIT(iVehicle, eVEHICLEBITSET_I_HAVE_PERFORMED_MODEL_OVERRIDE_CHECK)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_CREATE_VEHICLE_FROM_OFFSET(INT iVehicle)
	SWITCH GET_GBC_VARIATION()
		CASE CSV_MAX
			SWITCH iVehicle
				CASE 0		RETURN TRUE
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC CREATE_NON_MISSION_ENTITY_VEHICLE(INT iVehicle, MODEL_NAMES vehModel)

	IF DOES_VARIATION_HAVE_VARIATION_VEHICLES()
		IF SHOULD_SPAWN_VARIATION_VEHICLES()
			IF CREATE_NON_MISSION_ENTITY_VEHICLE_CREATION(iVehicle, vehModel, SHOULD_FIND_SPAWN_LOCATION_FOR_VEHICLE(iVehicle), SHOULD_SPAWN_VEHICLE_IN_THE_AIR(iVehicle), SHOULD_DO_OK_TO_SPAWN_CHECK(iVehicle), SHOULD_CREATE_VEHICLE_FROM_OFFSET(iVehicle))
				SET_NON_MISSION_ENTITY_VEHICLE_ATTRIBUTES_FOR_SPAWN(iVehicle)
				SET_VEHICLE_STATE(iVehicle, GET_AFTER_CREATION_VEHICLE_STATE(iVehicle))
				PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - CREATE_NON_MISSION_ENTITY_VEHICLE - Created vehicle #", iVehicle, " at ", serverBd.vSpawnNonMissionEntityVehicleCoords)
				RESET_SERVER_NON_MISSION_ENTITY_VEHICLE_SPAWN_DATA()
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL SHOULD_FAIL_MODE_FOR_DESTROYED_PREREQ_VEHICLE()
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_IT_SAFE_TO_CLEANUP_VEHICLE(INT iVeh)
	IF IS_VEHICLE_AN_AMBUSH_VEHICLE(iVeh)
	OR IS_VEHICLE_A_SUPPORT_VEHICLE(iVeh)
		RETURN TRUE
	ENDIF
	
	IF IS_VEHICLE_A_CARRIER_VEHICLE(iVeh)
		RETURN FALSE
	ENDIF
	
	SWITCH GET_GBC_VARIATION()
		CASE CSV_DEFEND_CASINO
		CASE CSV_COUNTERFEIT_CHIPS	RETURN FALSE
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_VEHICLES_RESPAWN_ON_VARIATION()
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_VEHICLE_RESPAWN(INT iVehicle)
	
	IF IS_VEHICLE_A_SUPPORT_VEHICLE(iVehicle)
	OR IS_VEHICLE_AN_AMBUSH_VEHICLE(iVehicle)
	OR IS_VEHICLE_A_CARRIER_VEHICLE(iVehicle)
		RETURN FALSE
	ENDIF
	
	IF GET_VEHICLE_STATE(iVehicle) < eVEHICLESTATE_NOT_DRIVEABLE
		RETURN FALSE
	ENDIF

	VECTOR vVehCoords = GET_GBC_VEHICLE_SPAWN_COORDS(GET_GBC_VARIATION(), GET_GBC_SUBVARIATION(), iVehicle, GET_CURRENT_DROP_OFF(GET_CURRENT_DROP_OFF_INT(TRUE)),DEFAULT,GET_RESPAWN_LOCATION())

	IF IS_VECTOR_ZERO(vVehCoords)
		RETURN FALSE
	ENDIF
		
	IF NOT IS_POINT_OK_FOR_NET_ENTITY_CREATION(vVehCoords,2,2,2,5.0,TRUE,TRUE,TRUE,15.0,FALSE,DEFAULT,FALSE,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE

ENDFUNC

FUNC BOOL DOES_VARIATION_VEHICLE_NEED_CRATE(INT iVehicle)

	SWITCH GET_GBC_VARIATION()
		CASE CSV_BODY_DISPOSAL
	
			RETURN IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_I_AM_CARRIER_VEHICLE)
			
		CASE CSV_MISSING_DELIVERY
			RETURN IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_I_AM_CARRIER_VEHICLE)
			
		CASE CSV_ESCORT_DUTY
			RETURN IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_I_AM_CARRIER_VEHICLE)
	ENDSWITCH
	
	RETURN FALSE

ENDFUNC

INT iTintIndex = -1
PROC SET_VEHICLE_CRATE_TINT(OBJECT_INDEX objId, MODEL_NAMES eModel)
	IF iTintIndex = (-1)
		iTintIndex = GET_RANDOM_INT_IN_RANGE(0, 4)
	ENDIF
	
	IF eModel = INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_GR_rsply_crate04a"))
	OR eModel = INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_GR_rsply_crate04b"))	
		SET_OBJECT_TINT_INDEX(objId, iTintIndex)
	ENDIF
ENDPROC

FUNC MODEL_NAMES GET_CRATE_MODEL_FOR_VEHICLE(MODEL_NAMES eVehicle)
	SWITCH eVehicle 
		CASE FLATBED		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_auto_salvage_stromberg"))
		CASE WASTELANDER	RETURN INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_auto_salvage_stromberg"))
		CASE AMBULANCE		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_x17_Bag_Med_01a")) 
		CASE RIOT			RETURN hei_prop_carrier_crate_01a 
		
		CASE BALLER2		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_vw_ped_Business_01a"))
		CASE LANDSTALKER	RETURN INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_vw_ped_Epsilon_01a"))
		CASE TORNADO2		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_vw_ped_Hooker_01a"))
		CASE EMPEROR2		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_vw_ped_Hillbilly_01a"))
		CASE STOCKADE		
			SWITCH GET_GBC_VARIATION()
				CASE CSV_ESCORT_DUTY				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("vw_Prop_VW_Crate_01a"))
				CASE CSV_MISSING_DELIVERY			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("vw_Prop_VW_Crate_02a"))
			ENDSWITCH
		BREAK
		
	ENDSWITCH
	RETURN Prop_box_wood04a
ENDFUNC

FUNC BOOL SHOULD_DISABLE_COLLISIONS_FOR_VEHICLE_CRATE(INT iMissionEntity, VEHICLE_INDEX vehID)

	UNUSED_PARAMETER(iMissionEntity)

	IF GET_ENTITY_MODEL(vehID) = INSURGENT2
	OR GET_ENTITY_MODEL(vehID) = BURRITO
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC INT GET_LOD_DIST_FOR_VEHICLE_CRATE(MODEL_NAMES crateModel)
	IF crateModel = INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_auto_salvage_stromberg"))
	OR crateModel = INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_x17_Bag_Med_01a"))
		RETURN 1200
	ENDIF
	
	RETURN 100
ENDFUNC

FUNC BOOL CREATE_CRATE_FOR_VEHICLE(INT iVehicle, VEHICLE_INDEX vehID)

	MODEL_NAMES crateModel = GET_CRATE_MODEL_FOR_VEHICLE(GET_ENTITY_MODEL(vehID))

	IF crateModel != DUMMY_MODEL_FOR_SCRIPT
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.vehCrate[iVehicle])
			IF CAN_REGISTER_MISSION_OBJECTS( 1 )
				REQUEST_MODEL(crateModel)
				IF HAS_MODEL_LOADED(crateModel)
					IF CREATE_NET_OBJ(serverBD.vehCrate[iVehicle],crateModel, GET_ENTITY_COORDS(vehID)+<<0.0,0.0,150.0>>)
						PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - CREATE_CRATE_FOR_VEHICLE - Crate created, now we'll attach it...")
						IF SHOULD_DISABLE_COLLISIONS_FOR_VEHICLE_CRATE(iVehicle, vehID)
							PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - CREATE_CRATE_FOR_VEHICLE - SHOULD_DISABLE_COLLISIONS_FOR_VEHICLE_CRATE is TRUE, disabling collisions for crate ", iVehicle)
							SET_ENTITY_COMPLETELY_DISABLE_COLLISION(NET_TO_OBJ(serverBD.vehCrate[iVehicle]), FALSE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF TAKE_CONTROL_OF_NET_ID(serverBD.vehCrate[iVehicle])
				IF ATTACH_CRATE_TO_VEHICLE(NET_TO_OBJ(serverBD.vehCrate[iVehicle]), vehID)
					PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - CREATE_CRATE_FOR_VEHICLE - Crate attached!")
					SET_VEHICLE_CRATE_TINT(NET_TO_OBJ(serverBD.vehCrate[iVehicle]), crateModel)
					SET_MODEL_AS_NO_LONGER_NEEDED(crateModel)
					SET_ENTITY_LOD_DIST(NET_TO_OBJ(serverBD.vehCrate[iVehicle]), GET_LOD_DIST_FOR_VEHICLE_CRATE(crateModel))
					SET_ENTITY_VISIBLE(NET_TO_OBJ(serverBD.vehCrate[iVehicle]),TRUE)
					SET_ENTITY_INVINCIBLE(NET_TO_OBJ(serverBD.vehCrate[iVehicle]),TRUE)
					IF DOES_ENTITY_HAVE_PHYSICS(NET_TO_OBJ(serverBD.vehCrate[iVehicle])) 
					AND GET_IS_ENTITY_A_FRAG(NET_TO_OBJ(serverBD.vehCrate[iVehicle]))
						SET_DISABLE_BREAKING(NET_TO_OBJ(serverBD.vehCrate[iVehicle]), TRUE)
						SET_DISABLE_FRAG_DAMAGE(NET_TO_OBJ(serverBD.vehCrate[iVehicle]), TRUE)
					ENDIF
					PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - CREATE_CRATE_FOR_VEHICLE - SET_DISABLE_BREAKING(NET_TO_OBJ(serverBD.vehCrate[iVehicle]), TRUE)")
					//SET_ENTITY_DYNAMIC(NET_TO_OBJ(serverBD.vehCrate[iVehicle]),FALSE)
					SET_ENTITY_PROOFS(NET_TO_OBJ(serverBD.vehCrate[iVehicle]),TRUE,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ELSE
		PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - CREATE_CRATE_FOR_VEHICLE - crateModel = DUMMY_MODEL_FOR_SCRIPT")
		RETURN FALSE
	ENDIF
	
	RETURN FALSE

ENDFUNC

PROC MAINTAIN_VARIATION_VEHICLE_CREATE_CRATE(INT iVehicle)

	IF NOT DOES_GBC_VARIATION_REQUIRE_VEHICLE_CRATE(GET_GBC_VARIATION())
		EXIT
	ENDIF

	IF NOT IS_VEHICLE_BIT_SET(iVehicle,  eVEHICLEBITSET_CREATED_CRATE_IN_VEHICLE)
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
			VEHICLE_INDEX vehID = NET_TO_VEH(serverBD.sVehicle[iVehicle].netId)
			IF IS_VEHICLE_DRIVEABLE(vehID)
				IF DOES_VARIATION_VEHICLE_NEED_CRATE(iVehicle)
					IF CREATE_CRATE_FOR_VEHICLE(iVehicle, vehID)
						PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_VARIATION_VEHICLE_CREATE_CRATE - Created a crate for vehicle ", iVehicle)
						SET_VEHICLE_BIT(iVehicle, eVEHICLEBITSET_CREATED_CRATE_IN_VEHICLE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL SHOULD_CLEANUP_VEHICLE(INT iVehicle)
	SWITCH GET_GBC_VARIATION()
		CASE CSV_HIGH_ROLLER
			SWITCH GET_GBC_SUBVARIATION()
				CASE CSS_HR_SANDY_SHORES
				CASE CSS_HR_MARINA		
					SWITCH iVehicle
						CASE 1	RETURN GET_PED_STATE(7) = ePEDSTATE_IDLE //Clean up the vehicle once ped 7 is inside.
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_SEARCH_AREA_FROM_VEHICLE(INT iVehicle)
	SWITCH GET_GBC_VARIATION()
		CASE CSV_TRACKING_CHIPS
			SWITCH iVehicle
				CASE 0		RETURN ciSEARCH_AREA_TWO
				CASE 1		RETURN ciSEARCH_AREA_THREE
			ENDSWITCH
		BREAK
		CASE CSV_MISSING_DELIVERY	RETURN ciSEARCH_AREA_ONE
	ENDSWITCH
	
	RETURN -1
ENDFUNC

FUNC BOOL SHOULD_WARP_VEHICLE_ON_STUCK_FLAG_BE_CLEARED_ON_ENTRY(INT iVehicle)
	UNUSED_PARAMETER(iVehicle)
	SWITCH GET_GBC_VARIATION()
		CASE CSV_PASSED_OUT
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_NON_MISSION_ENTITY_VEHICLE_BRAINS()

	INT iVehicle
	INT iSupportVehCount, iCarrierVehCount, iActiveTotalVehCount
	
//	VEHICLE_INDEX vehId
//	MODEL_NAMES eModel
	
	REPEAT serverBD.iNumMissionVehicles iVehicle
		
		IF SHOULD_CLEANUP_VEHICLE(iVehicle)
			SET_VEHICLE_STATE(iVehicle, eVEHICLESTATE_NO_LONGER_NEEDED)
		ENDIF
		
		SWITCH GET_VEHICLE_STATE(iVehicle)
			
			CASE eVEHICLESTATE_INACTIVE
				
			BREAK
			
			CASE eVEHICLESTATE_WAITING_FOR_RESPAWN
				IF DOES_GBC_VARIATION_HAVE_AMBUSH(GET_GBC_VARIATION(), GET_GBC_SUBVARIATION())
				AND IS_VEHICLE_AN_AMBUSH_VEHICLE(iVehicle)
					IF HAS_AMBUSH_RESPAWN_DELAY_PASSED(TRUE)
						SET_VEHICLE_STATE(iVehicle, eVEHICLESTATE_CREATE)
						PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] [AMBUSH] - eVEHICLESTATE_WAITING_FOR_RESPAWN - Vehicle #", iVehicle, " moved to eVEHICLESTATE_CREATE after respawn delay.")
					ENDIF
				ENDIF
			BREAK
			
			CASE eVEHICLESTATE_CREATE
				
				IF OK_TO_CREATE_NON_MISSION_ENTITY_VEHICLE(iVehicle)
					MAINTAIN_AMBUSH_VEHICLE_MODEL_SETUP(iVehicle)
					
					MODEL_NAMES eVehicleModel
					eVehicleModel = GET_GBC_VEHICLE_MODEL(GET_GBC_VARIATION(),GET_GBC_SUBVARIATION(), iVehicle, GET_EXTRA_PARAM_FOR_VEHICLE_SPAWN(iVehicle))
					IF IS_VEHICLE_AN_AMBUSH_VEHICLE(iVehicle)
						eVehicleModel = GET_GBC_AMBUSH_VEHICLE_MODEL(GET_GBC_VARIATION(), GET_GBC_SUBVARIATION(), GET_AMBUSH_VEHICLE_NUMBER(iVehicle), serverBD.iRandomModeInt0)
						IF serverBD.eAmbushVehicleModel[GET_AMBUSH_VEHICLE_NUMBER(iVehicle)] != DUMMY_MODEL_FOR_SCRIPT 
							eVehicleModel = serverBD.eAmbushVehicleModel[GET_AMBUSH_VEHICLE_NUMBER(iVehicle)]
						ENDIF
					ENDIF
					
					IF REQUEST_LOAD_MODEL(eVehicleModel)
						PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] eVEHICLESTATE_CREATE - Loaded Vehicle #", iVehicle, " model ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel))
						CREATE_NON_MISSION_ENTITY_VEHICLE(iVehicle, eVehicleModel)
					ENDIF
				ENDIF
				
			BREAK
			
			CASE eVEHICLESTATE_DRIVEABLE
				
				// Need to do this once nearby.
				IF IS_NET_VEHICLE_DRIVEABLE(serverBD.sVehicle[iVehicle].netId)
				
//					vehId = NET_TO_VEH(serverBD.sVehicle[iVehicle].netId)
//					
//					eModel = GET_ENTITY_MODEL(vehId)
					
					iActiveTotalVehCount++
					IF IS_VEHICLE_A_SUPPORT_VEHICLE(iVehicle)
						iSupportVehCount++
					ELIF IS_VEHICLE_A_CARRIER_VEHICLE(iVehicle)
						iCarrierVehCount++
					ENDIF
					
					IF HAS_NET_TIMER_EXPIRED(stDogfightAmbushActionTimer, 5000)
						IF NOT IS_VEHICLE_A_SUPPORT_VEHICLE(iVehicle)
						AND NOT IS_VEHICLE_AN_AMBUSH_VEHICLE(iVehicle)
							RESET_NET_TIMER(stDogfightAmbushActionTimer)
						ENDIF
					ENDIF
					
					IF IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_I_AM_RADIO_VEHICLE)
						IF NOT IS_VEHICLE_EMPTY_AND_STATIONARY(NET_TO_VEH(serverBD.sVehicle[iVehicle].netId))	
							CLEAR_VEHICLE_BIT(iVehicle, eVEHICLEBITSET_I_AM_RADIO_VEHICLE)
						ENDIF
					ENDIF
					
					IF IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_WARP_VEHICLE_ON_STUCK)
						IF SHOULD_WARP_VEHICLE_ON_STUCK_FLAG_BE_CLEARED_ON_ENTRY(iVehicle)
							IF GET_MISSION_ENTITY_VEHICLE_IS_CARRYING(iVehicle) != -1
							AND IS_MISSION_ENTITY_BIT_SET(GET_MISSION_ENTITY_VEHICLE_IS_CARRYING(iVehicle), eMISSIONENTITYBITSET_I_HAVE_BEEN_COLLECTED_FOR_FIRST_TIME)
								PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] eVEHICLESTATE_DRIVEABLE - Vehicle #", iVehicle, " has been entered, clearing eVEHICLEBITSET_WARP_VEHICLE_ON_STUCK")
								CLEAR_VEHICLE_BIT(iVehicle, eVEHICLEBITSET_WARP_VEHICLE_ON_STUCK)
							ENDIF
						ENDIF
					ENDIF
					
					MAINTAIN_VARIATION_VEHICLE_CREATE_CRATE(iVehicle)
					
				ELSE
					IF IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_I_AM_PREREQUISITE_VEHICLE)
						IF SHOULD_FAIL_MODE_FOR_DESTROYED_PREREQ_VEHICLE()
							MOVE_MISSION_TO_REWARDS(eENDREASON_PREREQUISITE_VEHICLE_DESTROYED)
						ENDIF
					ENDIF
					IF IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_I_AM_CARRIER_VEHICLE)
						IF NOT HAVE_ALL_MISSION_ENTITIES_IN_CARRIER_VEHICLE_BEEN_DELIVERED(iVehicle,TRUE)
							IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.sVehicle[iVehicle].netId)
								SET_SERVER_BIT(eSERVERBITSET_ANY_CARRIER_VEHICLE_DESTROYED)
							ENDIF
						ENDIF
					ENDIF
					SET_VEHICLE_STATE(iVehicle, eVEHICLESTATE_NOT_DRIVEABLE)
				ENDIF	
			BREAK
			
			CASE eVEHICLESTATE_NOT_DRIVEABLE
				
				IF SHOULD_DELETE_MISSION_ENTITY_PICKUP_ON_CARRIER_DESTROYED()
					INT iMissionEntity
					REPEAT GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() iMissionEntity
						IF DOES_MISSION_ENTITY_HAVE_CARRIER_VEHICLE(iMissionEntity)
							IF iVehicle = GET_MISSION_ENTITY_CARRIER_VEHICLE(iMissionEntity)
							AND NOT IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_DELIVERED)
								SET_MISSION_ENTITY_BIT(iMissionEntity, eMISSIONENTITYBITSET_DESTROYED)
								SET_MISSION_ENTITY_BIT(iMissionEntity, eMISSIONENTITYBITSET_RECEIVED_DESTROYED_DAMAGE_EVENT)
							ENDIF
						ENDIF
					ENDREPEAT
				ENDIF
				
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
					
					IF IS_IT_SAFE_TO_CLEANUP_VEHICLE(iVehicle)
						CLEANUP_NET_ID(serverBD.sVehicle[iVehicle].netId)
						PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - eVEHICLESTATE_NOT_DRIVEABLE - Vehicle #", iVehicle, " cleaned up as no longer driveable.")
					ENDIF
					
					SWITCH GET_GBC_VARIATION()
						CASE CSV_BAD_PRESS	
							IF iVehicle = BAD_PRESS_TARGET_VAN()
								IF GET_MODE_STATE() = eMODESTATE_GO_TO_ENTITY
								OR GET_MODE_STATE() = eMODESTATE_FOLLOW_ENTITY
									PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] eVEHICLESTATE_NOT_DRIVEABLE - eENDREASON_PREREQUISITE_VEHICLE_DESTROYED. Set end reason and move to rewards.")
									MOVE_MISSION_TO_REWARDS(eENDREASON_PREREQUISITE_VEHICLE_DESTROYED)
								ENDIF
							ENDIF
						BREAK
					ENDSWITCH
				ENDIF
				
				IF DOES_GBC_VARIATION_HAVE_AMBUSH(GET_GBC_VARIATION(), GET_GBC_SUBVARIATION())
				AND IS_VEHICLE_AN_AMBUSH_VEHICLE(iVehicle)
					IF SHOULD_RESPAWN_AMBUSH_VEHICLE(iVehicle)
						RESET_VEHICLE_DATA_FOR_RESPAWN(iVehicle)
						SET_VEHICLE_STATE(iVehicle, eVEHICLESTATE_WAITING_FOR_RESPAWN)
						PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] [AMBUSH] - eVEHICLESTATE_NOT_DRIVEABLE - Vehicle #", iVehicle, " set to eVEHICLESTATE_WAITING_FOR_RESPAWN.")
					ENDIF
				ENDIF
				
				IF GET_SEARCH_AREA_FROM_VEHICLE(iVehicle) != ciSEARCH_AREA_NONE
					IF NOT HAS_SEARCH_AREA_BEEN_SHRUNK(GET_SEARCH_AREA_FROM_VEHICLE(iVehicle))
						SET_SEARCH_AREA_BIT(GET_SEARCH_AREA_FROM_VEHICLE(iVehicle), eSEARCHAREABITSET_AREA_SHRUNK)
						PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] [AMBUSH] - eVEHICLESTATE_NOT_DRIVEABLE - Vehicle #", iVehicle, " is no longer driveable, search area ", GET_SEARCH_AREA_FROM_VEHICLE(iVehicle), " set to shrunk")
					ENDIF
				ENDIF
			BREAK
			
			CASE eVEHICLESTATE_WARP
				
			BREAK
			
			CASE eVEHICLESTATE_NO_LONGER_NEEDED
			
			BREAK
			
			CASE eVEHICLESTATE_RESET
				
			BREAK
			
		ENDSWITCH
		
		//If vehicles should respawn
		IF SHOULD_VEHICLES_RESPAWN_ON_VARIATION()
			IF SHOULD_VEHICLE_RESPAWN(iVehicle)
				SET_VEHICLE_STATE(iVehicle, eVEHICLESTATE_CREATE)
			ENDIF
		ENDIF
		
		// Detect if the ped is dead and set state accordingly.
		IF GET_VEHICLE_STATE(iVehicle) > eVEHICLESTATE_CREATE
			IF GET_VEHICLE_STATE(iVehicle) != eVEHICLESTATE_NOT_DRIVEABLE
			AND GET_VEHICLE_STATE(iVehicle) != eVEHICLESTATE_WARP
				IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
					PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - Vehicle #", iVehicle, " does not exist.")
					SET_VEHICLE_STATE(iVehicle, eVEHICLESTATE_NOT_DRIVEABLE)
				ELSE
					IF IS_ENTITY_DEAD(NET_TO_VEH(serverBD.sVehicle[iVehicle].netId))
					OR NOT IS_NET_VEHICLE_DRIVEABLE(serverBD.sVehicle[iVehicle].netId)
						PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - Vehicle #", iVehicle, " is dead.")
						SET_VEHICLE_STATE(iVehicle, eVEHICLESTATE_NOT_DRIVEABLE)
					ELIF IS_VEHICLE_AN_AMBUSH_VEHICLE(iVehicle) 
					AND ARE_ALL_AMBUSH_PEDS_IN_VEHICLE_DEAD(iVehicle)
						PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] [AMBUSH] - Vehicle #", iVehicle, " cleaned up since all it's peds are dead.")
						SET_VEHICLE_STATE(iVehicle, eVEHICLESTATE_NOT_DRIVEABLE)
					ENDIF
				ENDIF

			ENDIF
		ENDIF	
			
	ENDREPEAT
	
	IF serverBD.iActiveCarrierVehCount <> iCarrierVehCount
		serverBD.iActiveCarrierVehCount = iCarrierVehCount
	ENDIF
	
	IF serverBD.iActiveSuppportVehCount <> iSupportVehCount
		serverBD.iActiveSuppportVehCount = iSupportVehCount
	ENDIF
	
	IF serverBd.iActiveTotalVehCount <> iActiveTotalVehCount
		serverBd.iActiveTotalVehCount = iActiveTotalVehCount
	ENDIF
ENDPROC

FUNC BOOL SHOULD_REMOVE_ANCHOR_ON_NO_LONGER_NEEDED(INT iVehicle)
	SWITCH GET_GBC_VARIATION()
		CASE CSV_HIGH_ROLLER
			SWITCH GET_GBC_SUBVARIATION()
				CASE CSS_HR_SANDY_SHORES
					SWITCH iVehicle
						CASE 1	RETURN TRUE
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_UNLOCK_VEHICLE_DOORS_FOR_ALL_PLAYERS_ON_NO_LONGER_NEEDED(INT iVehicle)
	SWITCH GET_GBC_VARIATION()
		CASE CSV_HIGH_ROLLER
			SWITCH GET_GBC_SUBVARIATION()
				CASE CSS_HR_SANDY_SHORES
					SWITCH iVehicle
						CASE 1	RETURN TRUE //Ambient peds are forced to leave non-heli/non-plane/non-train vehicles if they are locked for players, so unlock it if peds are required to stay inside when released.
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_DESTROY_UNDRIVEABLE_NON_MISSION_ENTITIES_IN_VARIATION(INT iVehicle)

	SWITCH GET_GBC_VARIATION() 
		CASE CSV_STAFF_PROBLEMS
			SWITCH iVehicle
				CASE 1		RETURN TRUE
			ENDSWITCH
		BREAK
		CASE CSV_BODY_DISPOSAL
			SWITCH GET_GBC_SUBVARIATION()
				CASE CSS_BD_VARIATION_4
					SWITCH iVehicle
						CASE MISSION_ENTITY_ONE		RETURN GET_MODE_STATE() = eMODESTATE_DESTROY_MISSION_ENTITY
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CSV_COUNTERFEIT_CHIPS
			SWITCH iVehicle
				CASE 4
				CASE 5	RETURN TRUE
			ENDSWITCH
		BREAK
	ENDSWITCH

	RETURN FALSE
ENDFUNC

PROC MAINTAIN_DESTROY_UNDRIVEABLE_NON_MISSION_ENTITY_VEHICLE(INT iVehicle)
	IF SHOULD_DESTROY_UNDRIVEABLE_NON_MISSION_ENTITIES_IN_VARIATION(iVehicle)
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
			IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
				IF IS_ENTITY_ALIVE(NET_TO_VEH(serverBD.sVehicle[iVehicle].netId))
					IF NOT IS_VEHICLE_DRIVEABLE(NET_TO_VEH(serverBD.sVehicle[iVehicle].netId))
					OR GET_ENTITY_HEALTH(NET_TO_VEH(serverBD.sVehicle[iVehicle].netId)) < (GET_ENTITY_MAX_HEALTH(NET_TO_VEH(serverBD.sVehicle[iVehicle].netId)) / 5)
					OR GET_VEHICLE_ENGINE_HEALTH(NET_TO_VEH(serverBD.sVehicle[iVehicle].netId)) < 150
						PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_DESTROY_UNDRIVEABLE_NON_MISSION_ENTITY_VEHICLE - Vehicle #", iVehicle, " is no longer driveable, destroying it")
						NETWORK_EXPLODE_VEHICLE(NET_TO_VEH(serverBD.sVehicle[iVehicle].netId))
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_DO_DESTROY_HELICOPTER_CHECKS(INT iVehicle)
	UNUSED_PARAMETER(iVehicle)
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_HELICOPTER_TOO_LOW(VEHICLE_INDEX vehId #IF IS_DEBUG_BUILD , INT iVehicle #ENDIF )
	VECTOR vHelicoords = GET_ENTITY_COORDS(vehId, FALSE)
	FLOAT fZPosition
	
	IF GET_GROUND_Z_FOR_3D_COORD(vHelicoords, fZPosition, TRUE)
		IF vHelicoords.z < fZPosition + 30
			PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_SHOULD_DESTROY_HELICOPTER - IS_HELICOPTER_TOO_LOW(", iVehicle, ") - Heli Z: ", vHelicoords.z, " - Ground Z: ", fZPosition)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_SHOULD_DESTROY_HELICOPTER(INT iVehicle)
	IF NOT SHOULD_DO_DESTROY_HELICOPTER_CHECKS(iVehicle)
		EXIT
	ENDIF
	
	IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
		VEHICLE_INDEX vehId = NET_TO_VEH(serverBD.sVehicle[iVehicle].netId)
		IF NOT IS_ENTITY_DEAD(vehId)
			IF /*IS_HELICOPTER_DAMAGED(vehId #IF IS_DEBUG_BUILD , iVehicle #ENDIF )
			OR*/ IS_HELICOPTER_TOO_LOW(vehId #IF IS_DEBUG_BUILD , iVehicle #ENDIF )
				NETWORK_EXPLODE_HELI(vehId, TRUE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

INT iLastScriptVehicleInside = -1
PROC MAINTAIN_WARPING_VEHICLES_ON_DELIVERY(INT iVehicle)
	
	IF IS_NET_PLAYER_OK(PLAYER_ID())
	AND IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), GET_DROP_OFF_COORDS(), 10.0)
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			VEHICLE_INDEX vehID = NET_TO_VEH(serverBD.sVehicle[iVehicle].netId)
			IF vehID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			AND iLastScriptVehicleInside != iVehicle
				iLastScriptVehicleInside = iVehicle
				PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] eVEHICLESTATE_WARP - Player is inside vehicle #", iVehicle, " near bunker.")
			ENDIF
		ENDIF
	ELIF iLastScriptVehicleInside != (-1)
	AND NOT g_FreemodeDeliveryData.bDeliveryScriptTriggeredCutScene
	AND NOT NETWORK_IS_IN_MP_CUTSCENE()
		iLastScriptVehicleInside = -1
		PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] eVEHICLESTATE_WARP - Clearing last vehicle index. Player is no longer in range of bunker.")
	ENDIF
	
	IF iVehicle = iLastScriptVehicleInside
		IF g_FreemodeDeliveryData.bDeliveryScriptTriggeredCutScene
		AND NETWORK_IS_IN_MP_CUTSCENE()
			BROADCAST_EVENT_WARP_BUYSELL_SCRIPT_VEHICLE(iVehicle, FALSE, serverBD.iLaunchPosix)
			iLastScriptVehicleInside = -1
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_CARRIER_VEHICLES_BE_MADE_DESTROYABLE_FOR_VARIATION()
	SWITCH GET_GBC_VARIATION()
		CASE CSV_MAX		
			IF GET_MODE_STATE() < eMODESTATE_COLLECT_MISSION_ENTITY
				RETURN FALSE
			ENDIF
		BREAK
	ENDSWITCH
	RETURN HAS_ANY_MISSION_ENTITY_BEEN_COLLECTED_FOR_THE_FIRST_TIME()
ENDFUNC

FUNC BOOL SHOULD_VEHICLE_BE_MADE_DESTROYABLE(INT iVehicle)
	IF IS_PED_SITTING_IN_THIS_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.sVehicle[iVehicle].netId))
		IF IS_VEHICLE_BIT_SET(iVehicle,eVEHICLEBITSET_I_AM_PREREQUISITE_VEHICLE)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_VEHICLE_BIT_SET(iVehicle,eVEHICLEBITSET_I_AM_CARRIER_VEHICLE)
		IF SHOULD_CARRIER_VEHICLES_BE_MADE_DESTROYABLE_FOR_VARIATION()
			INT iMissionEntity
			REPEAT GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() iMissionEntity
				IF DOES_MISSION_ENTITY_HAVE_CARRIER_VEHICLE(iMissionEntity)
					IF iVehicle = GET_MISSION_ENTITY_CARRIER_VEHICLE(iMissionEntity)
						IF IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_SOMEONE_IS_NEAR_ME)
						OR IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_I_HAVE_BEEN_COLLECTED_FOR_FIRST_TIME)
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
	
	SWITCH GET_GBC_VARIATION()
		CASE CSV_ESCORT_DUTY		RETURN GET_MODE_STATE() = eMODESTATE_PROTECT_ENTITY
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_TIME_STUCK_TIL_RESET()
	RETURN 10000
ENDFUNC

FUNC INT GET_TIME_JAMMED_TIL_RESET()
	RETURN 15000
ENDFUNC

FUNC BOOL IS_VEHICLE_STUCK(VEHICLE_INDEX vehID)

	RETURN IS_VEHICLE_STUCK_TIMER_UP(vehID, VEH_STUCK_ON_ROOF, GET_TIME_STUCK_TIL_RESET())
	OR IS_VEHICLE_STUCK_TIMER_UP(vehID, VEH_STUCK_ON_SIDE, GET_TIME_STUCK_TIL_RESET())
	OR IS_VEHICLE_STUCK_TIMER_UP(vehID, VEH_STUCK_JAMMED, GET_TIME_JAMMED_TIL_RESET())
	OR IS_VEHICLE_STUCK_TIMER_UP(vehID, VEH_STUCK_HUNG_UP, GET_TIME_STUCK_TIL_RESET())

ENDFUNC

FUNC BOOL SHOULD_DO_WARP_HELP()
	SWITCH GET_GBC_VARIATION()
		CASE CSV_PASSED_OUT
			RETURN FALSE
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_VEHICLE_BE_WARPED(VEHICLE_INDEX vehID, BOOL &bDoHelpOnWarp)

	SWITCH GET_GBC_VARIATION()
		CASE CSV_STAFF_PROBLEMS	
			IF GET_MODE_STATE() = eMODESTATE_ANIM_SYNCED_SCENE
				IF NOT IS_CLIENT_BIT_SET(PARTICIPANT_ID(), eCLIENTBITSET_VEHICLE_WARPED)
				AND NOT IS_POINT_OK_FOR_NET_ENTITY_CREATION(GET_GBC_VEHICLE_SPAWN_COORDS(GET_GBC_VARIATION(),GET_GBC_SUBVARIATION(),0),1.0,0.0,0.0,0.0,FALSE,FALSE,FALSE,0.0,FALSE,DEFAULT,FALSE,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
					RETURN TRUE
				ELSE
					SET_CLIENT_BIT(eCLIENTBITSET_VEHICLE_WARPED)
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH

	IF IS_VEHICLE_STUCK(vehID)
		bDoHelpOnWarp = SHOULD_DO_WARP_HELP()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC FLOAT GET_MIN_DIST_VEHICLE_WARP_COORDS()

	SWITCH GET_GBC_VARIATION()
		CASE CSV_STAFF_PROBLEMS				RETURN 0.0
	ENDSWITCH

	RETURN 20.0

ENDFUNC

FUNC FLOAT GET_MAX_DIST_VEHICLE_WARP_COORDS()

	SWITCH GET_GBC_VARIATION()
		CASE CSV_STAFF_PROBLEMS				RETURN 50.0
	ENDSWITCH

	RETURN 150.0

ENDFUNC

PROC MAINTAIN_WARPING_VEHICLES(NETWORK_INDEX netVeh)

	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(netVeh)

		VEHICLE_INDEX vehID = NET_TO_VEH(netVeh)
		MODEL_NAMES model = GET_ENTITY_MODEL(vehID)
		BOOL bDoHelpOnWarp
		
		IF DOES_ENTITY_EXIST(vehID)
		AND NOT IS_ENTITY_DEAD(vehID,TRUE)
			
			IF SHOULD_VEHICLE_BE_WARPED(vehID, bDoHelpOnWarp)
				
				IF bDoHelpOnWarp
					IF IS_LOCAL_PLAYER_IN_PARTICIPATION_RANGE(150.0)
						TRIGGER_HELP(eHELPTEXT_STUCK_VEHICLE_WARPED)
					ENDIF
				ENDIF
				
				IF MAINTAIN_CONTROL_OF_NETWORK_ID(netVeh)
					PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_WARPING_STUCK_VEHICLES - Vehicle is stuck. Finding warp location.")
					
					VECTOR vSpawnLocation
					FLOAT fSpawnHeading
					VEHICLE_SPAWN_LOCATION_PARAMS vehicleSpawnLocationParams
					
					vehicleSpawnLocationParams.fMinDistFromCoords = GET_MIN_DIST_VEHICLE_WARP_COORDS()
					vehicleSpawnLocationParams.fMaxDistance = GET_MAX_DIST_VEHICLE_WARP_COORDS()
					vehicleSpawnLocationParams.bConsiderHighways = TRUE
					vehicleSpawnLocationParams.bCheckEntityArea = TRUE
					vehicleSpawnLocationParams.bCheckOwnVisibility = FALSE
					vehicleSpawnLocationParams.bConsiderOnlyActiveNodes = TRUE
					
					IF HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(GET_ENTITY_COORDS(vehID), <<0.0, 0.0, 0.0>>, model, TRUE, vSpawnLocation, fSpawnHeading, vehicleSpawnLocationParams)									  
						PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_WARPING_STUCK_VEHICLES - Warping vehicle to ", vSpawnLocation)
						
						NETWORK_FADE_IN_ENTITY(vehID, TRUE)
						SET_ENTITY_COORDS_NO_OFFSET(vehID, vSpawnLocation)
						SET_ENTITY_HEADING(vehID, fSpawnHeading)
						SET_VEHICLE_ON_GROUND_PROPERLY(vehID)
						
						SET_CLIENT_BIT(eCLIENTBITSET_VEHICLE_WARPED)
					ENDIF
					
				ENDIF
			ENDIF
		ENDIF

	ENDIF
	
ENDPROC

FUNC BOOL SHOULD_VEHICLE_BE_UNLOCKED_ON_CONDITION(INT iVehicle)
	
	UNUSED_PARAMETER(iVehicle)
	
	SWITCH GET_GBC_VARIATION()
		CASE CSV_RECOVER_LUXURY_CAR		
			IF iVehicle = GET_GBC_CARRIER_VEHICLE_INDEX(MISSION_ENTITY_ONE, GET_GBC_SUBVARIATION())
			AND GET_MODE_STATE() >= eMODESTATE_COLLECT_MISSION_ENTITY
			
				RETURN IS_VEHICLE_BIT_SET(MISSION_ENTITY_ONE, eVEHICLEBITSET_FOB_USED)
			ENDIF
		BREAK
		CASE CSV_SLOT_MACHINES
			IF iVehicle = GET_GBC_CARRIER_VEHICLE_INDEX(0, GET_GBC_SUBVARIATION())
				RETURN IS_SERVER_BIT_SET(eSERVERBITSET_PHOTO_SENT_TO_CONTACT)
			ENDIF
		BREAK
		CASE CSV_INTIMIDATE_JUDGE
			IF IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_I_AM_CARRIER_VEHICLE)
				RETURN GET_PED_STATE(6) = ePEDSTATE_FLEE_IN_VEHICLE
			ENDIF
		BREAK
		CASE CSV_STAFF_PROBLEMS
			SWITCH iVehicle
				CASE 1		RETURN GET_MODE_STATE() >= eMODESTATE_COLLECT_MISSION_ENTITY
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC SC_DOOR_LIST VEH_DOOR_TO_OPEN()

	SWITCH GET_GBC_VARIATION()
		CASE CSV_MISSING_DELIVERY		RETURN SC_DOOR_REAR_LEFT
		CASE CSV_ESCORT_DUTY			RETURN SC_DOOR_REAR_LEFT
		CASE CSV_RECOVER_LUXURY_CAR		RETURN SC_DOOR_FRONT_LEFT
	ENDSWITCH

	RETURN SC_DOOR_BOOT
ENDFUNC

FUNC SC_DOOR_LIST VEH_DOOR_TO_OPEN_SECOND_DOOR()

	SWITCH GET_GBC_VARIATION()
		CASE CSV_MISSING_DELIVERY	RETURN SC_DOOR_REAR_RIGHT
		CASE CSV_ESCORT_DUTY		RETURN SC_DOOR_REAR_RIGHT	
	ENDSWITCH

	RETURN SC_DOOR_INVALID
ENDFUNC

PROC MAINTAIN_VEHICLE_DOOR_OPENING(INT iVehicle, VEHICLE_INDEX vehID)
	IF NOT IS_VEHICLE_CLIENT_BIT_SET(iVehicle, PARTICIPANT_ID(), eVEHICLECLIENTBITSET_OPENED_BOOT)

		SET_VEHICLE_DOOR_OPEN(vehID, VEH_DOOR_TO_OPEN())
		SET_VEHICLE_DOOR_CONTROL(vehId, VEH_DOOR_TO_OPEN(), DT_DOOR_SWINGING_FREE, 1.0)
		
		IF VEH_DOOR_TO_OPEN_SECOND_DOOR() != SC_DOOR_INVALID
			SET_VEHICLE_DOOR_OPEN(vehID, VEH_DOOR_TO_OPEN_SECOND_DOOR())
			SET_VEHICLE_DOOR_CONTROL(vehId, VEH_DOOR_TO_OPEN_SECOND_DOOR(), DT_DOOR_SWINGING_FREE, 1.0)
		ENDIF
		
		IF GET_GBC_VARIATION() != CSV_BODY_DISPOSAL
			SET_VEHICLE_CLIENT_BIT(iVehicle, eVEHICLECLIENTBITSET_OPENED_BOOT)
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_VEHICLE_ROOF_RAISE(INT iVehicle, VEHICLE_INDEX vehID)
	IF IS_VEHICLE_A_CONVERTIBLE(vehID)
		SWITCH GET_CONVERTIBLE_ROOF_STATE(vehID)
			CASE CRS_LOWERED
			CASE CRS_ROOF_STUCK_LOWERED
				IF NOT IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_ROOF_TOGGLED)
					IF NOT IS_VEHICLE_CLIENT_BIT_SET(iVehicle, PARTICIPANT_ID(), eVEHICLECLIENTBITSET_OPENED_ROOF)
						IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
							PRINTLN("[MAINTAIN_VEHICLE_ROOF_RAISE] eVEHICLECLIENTBITSET_OPENED_ROOF ")
							SET_VEHICLE_CLIENT_BIT(iVehicle, eVEHICLECLIENTBITSET_OPENED_ROOF)
						ENDIF	
					ENDIF
				ENDIF
			BREAK
			CASE CRS_RAISED
				IF NOT IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_ROOF_TOGGLED)
					LOWER_CONVERTIBLE_ROOF(vehID, TRUE)
					PRINTLN("[MAINTAIN_VEHICLE_ROOF_RAISE] LOWER_CONVERTIBLE_ROOF ")
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

FUNC BOOL SHOULD_RAISE_VEHICLE_ROOF(INT iVehicle, VEHICLE_INDEX vehID)

	UNUSED_PARAMETER(vehID)

	SWITCH GET_GBC_VARIATION()
		CASE CSV_BODY_DISPOSAL		
			SWITCH GET_GBC_SUBVARIATION()
				CASE CSS_BD_VARIATION_3
					IF NOT IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_ROOF_TOGGLED)
						IF IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_I_AM_CARRIER_VEHICLE)
							RETURN TRUE
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_VEHICLE_SPEED_BE_ALTERED(INT iVehicle, VEHICLE_INDEX vehID)
	UNUSED_PARAMETER(iVehicle)
	UNUSED_PARAMETER(vehId)
	RETURN FALSE
ENDFUNC

FUNC FLOAT GET_ALTERED_VEHICLE_SPEED()
	RETURN 30.0
ENDFUNC

FUNC BOOL SHOULD_EMPTY_AND_LOCK_VEHICLE_ON_END(MODEL_NAMES eModel, INT iVehicle)
	UNUSED_PARAMETER(eModel)
	
	SWITCH GET_GBC_VARIATION()
		CASE CSV_BODY_DISPOSAL
			IF GET_END_REASON() = eENDREASON_TIME_UP
				RETURN IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_I_AM_CARRIER_VEHICLE)
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_RUN_VEHICLE_DISTANCE_CHECKS()

	SWITCH GET_GBC_VARIATION()
		CASE CSV_ESCORT_DUTY
			RETURN TRUE
	ENDSWITCH

	RETURN FALSE
ENDFUNC

FUNC BOOL IS_VEHICLE_VALID_FOR_CLOSEST_CHECK(INT iVehicle)
	UNUSED_PARAMETER(iVehicle)
	
	SWITCH GET_GBC_VARIATION()
		CASE CSV_ESCORT_DUTY
			RETURN IS_VEHICLE_A_CARRIER_VEHICLE(iVehicle)
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_ANY_PED_IN_NON_MISSION_ENTITY_VEHICLE(INT iVeh)
	PED_INDEX tempPed
	VEHICLE_INDEX tempVeh = NET_TO_VEH(serverBD.sVehicle[iVeh].netId)
	INT i
	
	IF DOES_ENTITY_EXIST(tempVeh)
		FOR i= -1 TO ENUM_TO_INT(VS_EXTRA_RIGHT_3)
			tempPed = GET_PED_IN_VEHICLE_SEAT(tempVeh, INT_TO_ENUM(VEHICLE_SEAT, i))
			IF DOES_ENTITY_EXIST(tempPed)
				IF NOT IS_ENTITY_DEAD(tempPed)
				AND NOT IS_PED_INJURED(tempPed)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SUPPORT_VEHICLE_BLIP_CHECK(INT iVehicle)

	SWITCH GET_GBC_VARIATION()
		CASE CSV_MAX
			IF GET_CLIENT_MODE_STATE() < eMODESTATE_COLLECT_MISSION_ENTITY
				RETURN FALSE
			ENDIF
		BREAK
	ENDSWITCH

	RETURN IS_VEHICLE_EMPTY(NET_TO_VEH(serverBD.sVehicle[iVehicle].netId))
ENDFUNC

FUNC BOOL SHOULD_NON_MISSION_ENTITY_VEHICLE_DISPLAY_UNIQUE_BLIP(INT iVehicle)

	IF NOT bSafeToDisplay
		RETURN FALSE
	ENDIF
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
	AND IS_NET_VEHICLE_DRIVEABLE(serverBD.sVehicle[iVehicle].netId)

		IF IS_VEHICLE_A_SUPPORT_VEHICLE(iVehicle)
			IF SUPPORT_VEHICLE_BLIP_CHECK(iVehicle)
				RETURN TRUE
			ELSE
				RETURN FALSE
			ENDIF
		ENDIF
		
		SWITCH GET_GBC_VARIATION()
			CASE CSV_BAD_PRESS
				SWITCH GET_CLIENT_MODE_STATE()
					CASE eMODESTATE_GO_TO_ENTITY 	
						IF IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_PLAYER_IS_NEAR_ME)
						AND iVehicle != BAD_PRESS_TARGET_VAN()
							RETURN FALSE
						ENDIF
						IF (iVehicle <= 4)
							RETURN TRUE
						ENDIF
					BREAK
					CASE eMODESTATE_FOLLOW_ENTITY	RETURN iVehicle = BAD_PRESS_TARGET_VAN()
				ENDSWITCH
			BREAK
			CASE CSV_DEFEND_CASINO
				SWITCH iVehicle
					CASE 2	RETURN NOT IS_PED_BIT_SET(8, ePEDBITSET_I_HAVE_REACHED_FINAL_CHECKPOINT) //Blip the vehicle only if the driver has not yet reached their destination.
					CASE 4
					CASE 5	RETURN NOT IS_VEHICLE_EMPTY(NET_TO_VEH(serverBD.sVehicle[iVehicle].netId), TRUE)
				ENDSWITCH
			BREAK
			CASE CSV_INTIMIDATE_JUDGE
				IF IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_I_AM_CARRIER_VEHICLE)
					IF GET_PED_STATE(6) = ePEDSTATE_FLEE_IN_VEHICLE
						RETURN TRUE
					ENDIF
				ENDIF
				
			BREAK
		ENDSWITCH
		
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_VEH_BLIP_FLASH_CONTINUOUSLY(INT iVehicle)
	SWITCH GET_GBC_VARIATION()
		CASE CSV_DEFEND_CASINO
			SWITCH iVehicle
				CASE 2	RETURN HAS_NET_TIMER_STARTED(serverBD.stVehicleLostDelay)
			ENDSWITCH
		BREAK
		CASE CSV_BAD_PRESS
			IF iVehicle = BAD_PRESS_TARGET_VAN()
					
				IF HAS_NET_TIMER_STARTED(serverBD.stVehicleLostDelay)
					RETURN TRUE
				ENDIF

			ENDIF
//			RETURN (BAD_PRESS_VEHICLE_BLIP(iVehicle) = RADAR_TRACE_SECURITY_VAN)
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_VEH_BLIP_FLASH_ON_CREATION(INT iVehicle)
	SWITCH GET_GBC_VARIATION()
		CASE CSV_BAD_PRESS
			
			RETURN (BAD_PRESS_VEHICLE_BLIP(iVehicle) = RADAR_TRACE_SECURITY_VAN)
		CASE CSV_INTIMIDATE_JUDGE
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_RADAR_FLASH_WITH_VEH_BLIP(INT iVehicle)
	SWITCH GET_GBC_VARIATION()
		CASE CSV_BAD_PRESS
		
			RETURN (BAD_PRESS_VEHICLE_BLIP(iVehicle) = RADAR_TRACE_SECURITY_VAN)
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_FADE_BLIP_BASED_ON_DIST(INT iVehicle)
	IF IS_VEHICLE_A_SUPPORT_VEHICLE(iVehicle)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC HUD_COLOURS GET_SUPPORT_BLIP_COLOUR()
	RETURN HUD_COLOUR_WHITE
ENDFUNC

FUNC BOOL SHOULD_ROTATE_BLIP(INT iVehicle, VEHICLE_INDEX vehId)
	
	IF IS_VEHICLE_A_SUPPORT_VEHICLE(iVehicle)
	AND NOT IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(vehId))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC HUD_COLOURS GET_VEHICLE_BLIP_COLOUR(INT iVehicle)
	SWITCH GET_GBC_VARIATION()
		CASE CSV_DEFEND_CASINO
			SWITCH iVehicle
				CASE 4
				CASE 5	RETURN HUD_COLOUR_RED
			ENDSWITCH
		BREAK
		CASE CSV_BAD_PRESS
			IF GET_MODE_STATE() = eMODESTATE_GO_TO_ENTITY
			
				RETURN HUD_COLOUR_YELLOW
			ENDIF
		BREAK
		CASE CSV_INTIMIDATE_JUDGE		RETURN HUD_COLOUR_RED
	ENDSWITCH
	
	RETURN HUD_COLOUR_BLUE
ENDFUNC

FUNC STRING GET_VEHICLE_BLIP_TEXT_LABEL(INT iVehicle)
	SWITCH GET_GBC_VARIATION()
		CASE CSV_DEFEND_CASINO
			SWITCH iVehicle
				CASE 2	RETURN "GBC_BLIP_SPD"
			ENDSWITCH
		BREAK
		CASE CSV_INTIMIDATE_JUDGE		RETURN "GBC_BLIP_JUD"
	ENDSWITCH
	
	RETURN "GBC_BLIP_ENY"
ENDFUNC

FUNC BOOL SHOULD_UPDATE_VEHICLE_BLIP_COLOUR(INT iVehicle)
	UNUSED_PARAMETER(iVehicle)
	SWITCH GET_GBC_VARIATION()
		CASE CSV_MAX		
			IF GET_BLIP_COLOUR(blipVariationVehBlip[iVehicle]) != GET_BLIP_COLOUR_FROM_HUD_COLOUR(GET_VEHICLE_BLIP_COLOUR(iVehicle))
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC REMOVE_NON_MISSION_ENTITY_VEHICLE_UNIQUE_BLIPS()
	INT iVehicle
	REPEAT GB_GET_GBC_NUM_VEH_REQUIRED(GET_GBC_VARIATION(), GET_GBC_SUBVARIATION(), GB_GET_LOCAL_PLAYER_GANG_BOSS(), FALSE, GET_EXTRA_PARAM_FOR_VEHICLE_SPAWN(), TRUE, FALSE) iVehicle
		IF DOES_BLIP_EXIST(blipVariationVehBlip[iVehicle])
			REMOVE_BLIP(blipVariationVehBlip[iVehicle])
			PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] REMOVE_NON_MISSION_ENTITY_VEHICLE_UNIQUE_BLIPS - removed blip from sVehicle[", iVehicle, "].netId")
		ENDIF
	ENDREPEAT
ENDPROC

FUNC STRING NON_MISSION_ENTITY_VEHICLE_BLIP_NAME()

	SWITCH GET_GBC_VARIATION()
		CASE CSV_BAD_PRESS
			
			IF GET_MODE_STATE() = eMODESTATE_GO_TO_ENTITY
				RETURN "GBC_BLIP_LCS"
			ENDIF
		
			RETURN "GBC_BLIP_VAN"

	ENDSWITCH

	RETURN "GBC_BLIP_VAN"
ENDFUNC

PROC MAINTAIN_NON_MISSION_ENTITY_VEHICLE_UNIQUE_BLIP(INT iVehicle)

	BOOL bExists = FALSE
	BOOL bAlive = FALSE

	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
		bExists = TRUE
		IF IS_VEHICLE_DRIVEABLE(NET_TO_VEH(serverBD.sVehicle[iVehicle].netId))
			bAlive = TRUE
		ENDIF
	ENDIF
	
	FLOAT fScale = 1.0

	IF ((bExists AND bAlive) OR (IS_BLIP_FLASHING_ON_DESTRUCTION(blipVariationVehBlip[iVehicle],!bAlive,iVehFlashingBS,iVehicle)))
	AND SHOULD_NON_MISSION_ENTITY_VEHICLE_DISPLAY_UNIQUE_BLIP(iVehicle)
		IF NOT DOES_BLIP_EXIST(blipVariationVehBlip[iVehicle])
			IF IS_VEHICLE_A_SUPPORT_VEHICLE(iVehicle)
				ADD_MISSION_BLIP_FOR_ENTITY(blipVariationVehBlip[iVehicle], NET_TO_ENT(serverBD.sVehicle[iVehicle].netId), GET_SUPPORT_VEHICLE_BLIP_SPRITE(), GET_SUPPORT_BLIP_COLOUR(), GET_SUPPORT_VEHICLE_BLIP_NAME(FALSE), FALSE, FALSE, (NOT HAS_HELP_BEEN_DONE(eHELPTEXT_EXPLAIN_SUPPORT_VEHICLES)))
			ELSE
				SWITCH GET_GBC_VARIATION()
					CASE CSV_BAD_PRESS
					
						IF BAD_PRESS_VEHICLE_BLIP(iVehicle) != RADAR_TRACE_SECURITY_VAN
							fScale = 0.75
						ENDIF
						ADD_MISSION_BLIP_FOR_ENTITY(blipVariationVehBlip[iVehicle], NET_TO_ENT(serverBD.sVehicle[iVehicle].netId), BAD_PRESS_VEHICLE_BLIP(iVehicle), GET_VEHICLE_BLIP_COLOUR(iVehicle), NON_MISSION_ENTITY_VEHICLE_BLIP_NAME(), DEFAULT, DEFAULT, SHOULD_VEH_BLIP_FLASH_ON_CREATION(iVehicle), DEFAULT, fScale)
					BREAK
					
					CASE CSV_DEFEND_CASINO
						ADD_MISSION_BLIP_FOR_ENTITY(blipVariationVehBlip[iVehicle], NET_TO_ENT(serverBD.sVehicle[iVehicle].netId), DEFEND_CASINO_VEHICLE_BLIP(iVehicle), GET_VEHICLE_BLIP_COLOUR(iVehicle), GET_VEHICLE_BLIP_TEXT_LABEL(iVehicle))
					BREAK
					
					CASE CSV_INTIMIDATE_JUDGE
						ADD_MISSION_BLIP_FOR_ENTITY(blipVariationVehBlip[iVehicle], NET_TO_ENT(serverBD.sVehicle[iVehicle].netId), RADAR_TRACE_GANG_VEHICLE, HUD_COLOUR_RED, GET_VEHICLE_BLIP_TEXT_LABEL(iVehicle), FALSE, FAlSE, TRUE)
					BREAK
				ENDSWITCH
				
				IF SHOULD_RADAR_FLASH_WITH_VEH_BLIP(iVehicle)
					FLASH_MINIMAP_DISPLAY()
				ENDIF
				
			ENDIF
		ELSE
			IF SHOULD_ROTATE_BLIP(iVehicle, NET_TO_VEH(serverBD.sVehicle[iVehicle].netId))
				SET_BLIP_ROTATION(blipVariationVehBlip[iVehicle], ROUND(GET_ENTITY_HEADING_FROM_EULERS(NET_TO_VEH(serverBD.sVehicle[iVehicle].netId))))
			ENDIF
			
			IF SHOULD_UPDATE_VEHICLE_BLIP_COLOUR(iVehicle)
				SET_BLIP_COLOUR_FROM_HUD_COLOUR(blipVariationVehBlip[iVehicle], GET_VEHICLE_BLIP_COLOUR(iVehicle))
			ENDIF
			
			IF SHOULD_FADE_BLIP_BASED_ON_DIST(iVehicle)
				IF IS_VEHICLE_A_SUPPORT_VEHICLE(iVehicle)
					SET_BLIP_ALPHA_BASED_ON_DIST_FROM_PLAYER(blipVariationVehBlip[iVehicle], 50)
				ELSE
					SET_BLIP_ALPHA_BASED_ON_DIST_FROM_PLAYER(blipVariationVehBlip[iVehicle], 100, 5000)
				ENDIF
			ENDIF
			
			IF SHOULD_VEH_BLIP_FLASH_CONTINUOUSLY(iVehicle)
				IF NOT IS_BLIP_FLASHING(blipVariationVehBlip[iVehicle])
					FLASH_MISSION_BLIP(blipVariationVehBlip[iVehicle])
				ENDIF
			ELIF SHOULD_VEH_BLIP_FLASH_ON_CREATION(iVehicle)
				// Let it stop by itself
			ELSE
				IF IS_BLIP_FLASHING(blipVariationVehBlip[iVehicle])
					SET_BLIP_FLASHES(blipVariationVehBlip[iVehicle], FALSE)
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF DOES_BLIP_EXIST(blipVariationVehBlip[iVehicle])
			REMOVE_BLIP(blipVariationVehBlip[iVehicle])
			PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_NON_MISSION_ENTITY_VEHICLE_UNIQUE_BLIP - removed blip from sVehicle[", iVehicle, "].netId")
		ENDIF
	ENDIF

ENDPROC

FUNC BOOL SHOULD_ALLOW_USE_AFTER_MISSION(MODEL_NAMES vehModel, INT iVehicle)
	UNUSED_PARAMETER(vehModel)
	IF IS_VEHICLE_A_SUPPORT_VEHICLE(iVehicle)
		
	ENDIF
	
	SWITCH GET_GBC_VARIATION()
		CASE CSV_BODY_DISPOSAL
			IF GET_END_REASON() = eENDREASON_TIME_UP
				IF IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_I_AM_CARRIER_VEHICLE)
					RETURN FALSE
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_USE_PMSD_FOR_VEHICLE(MODEL_NAMES eModel)
	SWITCH eModel
		CASE TULA	RETURN TRUE
	ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC SET_VEHICLE_UNLOCK_STATE_FOR_VARIATION(NETWORK_INDEX netId, VEHICLE_INDEX vehId)
	
	IF MAINTAIN_CONTROL_OF_NETWORK_ID(netId)
		IF SHOULD_CARRIER_VEHICLE_NEED_BROKEN_INTO(GET_ENTITY_MODEL(vehId))
			SET_VEHICLE_DOORS_LOCKED(vehId, VEHICLELOCK_LOCKED_BUT_CAN_BE_DAMAGED)
		ELSE
			SET_VEHICLE_DOORS_LOCKED(vehID, VEHICLELOCK_UNLOCKED)	
		ENDIF
		SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(vehId, FALSE)
		SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(vehID, PLAYER_ID(), FALSE)
		SET_VEHICLE_DOORS_LOCKED_FOR_NON_SCRIPT_PLAYERS(vehId, TRUE)
		IF GET_GBC_VARIATION() != CSV_RECOVER_LUXURY_CAR
			SET_VEHICLE_DISABLE_TOWING(vehId, FALSE)
		ENDIF
		PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] SET_VEHICLE_UNLOCK_STATE_FOR_VARIATION - Set.")
	ENDIF
ENDPROC

FUNC NETWORK_INDEX GET_TRAILER_VEHICLE_NEEDS_ATTACHED_TO(INT iVehicle)
	SWITCH GET_GBC_VARIATION()
		CASE CSV_MAX
			SWITCH iVehicle	
				CASE 0	
					RETURN serverBD.sVehicle[0].netId
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN INT_TO_NATIVE(NETWORK_INDEX, -1)
ENDFUNC

PROC MAINTAIN_ATTACHING_VEHICLE_LOGIC(INT iVehicle, VEHICLE_INDEX vehID)
	IF NOT IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_NEEDS_ATTACHED_TO_TRAILER)
	OR IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_ATTACHED_TO_TRAILER)
	OR IS_VEHICLE_CLIENT_BIT_SET(iVehicle, PARTICIPANT_ID(), eVEHICLECLIENTBITSET_ATTACHED_TO_TRAILER)
		EXIT
	ENDIF
	
	NETWORK_INDEX trailerNetID = GET_TRAILER_VEHICLE_NEEDS_ATTACHED_TO(iVehicle)
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(trailerNetID)
	AND IS_NET_VEHICLE_DRIVEABLE(trailerNetID)
	AND NOT IS_ENTITY_DEAD(vehID)
		VEHICLE_INDEX trailerVehId = NET_TO_VEH(trailerNetID)
		IF IS_THIS_MODEL_A_TRUCK_CAB(GET_ENTITY_MODEL(vehID))
			IF NOT IS_VEHICLE_ATTACHED_TO_TRAILER(vehID)
				ATTACH_VEHICLE_TO_TRAILER(vehID, trailerVehId, 1.0)
				PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_ATTACHING_VEHICLE_LOGIC - Called ATTACH_VEHICLE_TO_TRAILER on vehicle #", iVehicle)
			ELSE
				SET_VEHICLE_CLIENT_BIT(iVehicle, eVEHICLECLIENTBITSET_ATTACHED_TO_TRAILER)
				PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_ATTACHING_VEHICLE_LOGIC - Attached vehicle #", iVehicle, " to trailer.")
			ENDIF
		ELSE
			IF NOT IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(vehID)
				//VECTOR vTrailerRot = GET_ENTITY_ROTATION(trailerVehId)
				ATTACH_ENTITY_TO_ENTITY(vehID, trailerVehId, GET_ENTITY_BONE_INDEX_BY_NAME(trailerVehId, "chassis_dummy"), <<0.0, -5.5, 0.7>>, <<0.0, 0.0, 0.0>>, FALSE, FALSE, FALSE)
				PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_ATTACHING_VEHICLE_LOGIC - Called ATTACH_ENTITY_TO_ENTITY on vehicle #", iVehicle)
			ELSE
				SET_VEHICLE_CLIENT_BIT(iVehicle, eVEHICLECLIENTBITSET_ATTACHED_TO_TRAILER)
				PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_ATTACHING_VEHICLE_LOGIC - Attached vehicle #", iVehicle, " to vehicle.")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_USE_PMSD_FOR_CARRIER_VEHICLES()
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_PMSD_FOR_CARRIER_VEHICLES(INT iVehicle, VEHICLE_INDEX vehId)
	IF NOT SHOULD_USE_PMSD_FOR_CARRIER_VEHICLES()
	OR NOT IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_I_AM_CARRIER_VEHICLE)
	OR GET_END_REASON() = eENDREASON_NO_REASON_YET
		EXIT
	ENDIF

	IF IS_VEHICLE_DRIVEABLE(vehId)
		INT iMissionEntity
		REPEAT GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() iMissionEntity
			IF iVehicle = GET_MISSION_ENTITY_CARRIER_VEHICLE(iMissionEntity)
				IF NOT IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_DELIVERED)
				AND NOT IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_DESTROYED)
					IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
						SET_ENTITY_INVINCIBLE(vehId, FALSE)
						SET_VEHICLE_ENGINE_HEALTH(vehId, -1000.0)
						PRINTLN("[GB_CASINO] [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_PMSD_FOR_CARRIER_VEHICLES - Killing engine for vehicle #", iVehicle, " as it is carrying mission entity #", iVehicle)
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

FUNC BOOL SHOULD_PEDS_REACT_ON_VEHICLE_ENTRY(INT iVehicle)

	UNUSED_PARAMETER(iVehicle)
	
	RETURN FALSE

ENDFUNC

FUNC INT GET_PED_TO_REACT_ON_VEHICLE_ENTRY(INT iVehicle)

	UNUSED_PARAMETER(iVehicle)

	RETURN -1

ENDFUNC

FUNC BOOL SHOULD_VEHICLE_SIREN_ACTIVATE(INT iVehicle)

	SWITCH GET_GBC_VARIATION()
		CASE CSV_UNDER_THE_INFLUENCE
		CASE CSV_BODY_DISPOSAL
			IF SHOULD_PEDS_GROUP_REACT(iVehicle)
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH

	RETURN FALSE
ENDFUNC

FUNC FLOAT GET_NEAR_VEHICLE_RANGE(INT iVehicle)
	SWITCH GET_GBC_VARIATION()
		CASE CSV_BAD_PRESS
			IF iVehicle = BAD_PRESS_TARGET_VAN()
				RETURN 90.00
			ENDIF
		BREAK
		CASE CSV_MISSING_DELIVERY
			RETURN 50.00
		CASE CSV_TRACKING_CHIPS
			RETURN 20.00
		CASE CSV_UNDER_THE_INFLUENCE
			IF iVehicle = GET_MISSION_ENTITY_CARRIER_VEHICLE(MISSION_ENTITY_ONE)
				RETURN 150.0
			ENDIF
		BREAK
	ENDSWITCH
	RETURN 30.0
ENDFUNC

FUNC BOOL GET_NEAR_VEHICLE_REQUIRES_LINE_OF_SIGHT(INT iVehicle)
	UNUSED_PARAMETER(iVehicle)
	SWITCH GET_GBC_VARIATION()
		CASE CSV_TRACKING_CHIPS
		CASE CSV_MISSING_DELIVERY
			RETURN TRUE
			
		CASE CSV_BAD_PRESS
//			IF iVehicle != BAD_PRESS_TARGET_VAN()
				RETURN TRUE
//			ENDIF
//		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_NEAR_VEHICLE(VEHICLE_INDEX vehID, FLOAT fRange, BOOL bCheckLoS = FALSE)
	IF VDIST2(GET_ENTITY_COORDS(vehID, FALSE), GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)) <= fRange*fRange
		IF bCheckLoS
			RETURN HAS_ENTITY_CLEAR_LOS_TO_ENTITY(PLAYER_PED_ID(), vehID, SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_VEHICLE|SCRIPT_INCLUDE_PED|SCRIPT_INCLUDE_RAGDOLL|SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_FOLIAGE)
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL MARK_SEARCH_AREA_FOUND_NEAR_VEHICLE()

	SWITCH GET_GBC_VARIATION()
		CASE CSV_MISSING_DELIVERY 	RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC BAD_PRESS_VAN_HELP(INT iVehicle)
	SWITCH GET_GBC_VARIATION()
		CASE CSV_BAD_PRESS
		
			IF iVehicle = BAD_PRESS_TARGET_VAN()
			OR GET_MODE_STATE() != eMODESTATE_GO_TO_ENTITY
				EXIT
			ENDIF
		
			SWITCH serverBD.iNumVehiclesFound
				CASE 0	TRIGGER_HELP(eHELPTEXT_BP_VAN_1) BREAK
				CASE 1	TRIGGER_HELP(eHELPTEXT_BP_VAN_2) BREAK
				CASE 2	TRIGGER_HELP(eHELPTEXT_BP_VAN_3) BREAK
				CASE 3	TRIGGER_HELP(eHELPTEXT_BP_VAN_4) BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

PROC MAINTAIN_IS_PLAYER_NEAR_VEHICLE(INT iVehicle, VEHICLE_INDEX vehID)
	IF NOT IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_PLAYER_IS_NEAR_ME)
		IF IS_PLAYER_NEAR_VEHICLE(vehID, GET_NEAR_VEHICLE_RANGE(iVehicle), GET_NEAR_VEHICLE_REQUIRES_LINE_OF_SIGHT(iVehicle))
			SET_VEHICLE_CLIENT_BIT(iVehicle, eVEHICLECLIENTBITSET_PLAYER_IS_NEAR_ME)
			PRINTLN("[GB_CASINO] [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_IS_PLAYER_NEAR_VEHICLE - Player is near vehicle #", iVehicle)
			
			IF GET_SEARCH_AREA_FROM_VEHICLE(iVehicle) != -1
				IF NOT IS_SEARCH_AREA_BIT_SET(GET_SEARCH_AREA_FROM_VEHICLE(iVehicle), eSEARCHAREABITSET_AREA_SHRUNK)
					SET_SEARCH_AREA_CLIENT_BIT(GET_SEARCH_AREA_FROM_VEHICLE(iVehicle), eSEARCHAREACLIENTBITSET_AREA_SHRUNK)
					PRINTLN("[GB_CASINO] [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_IS_PLAYER_NEAR_VEHICLE - Search area ", GET_SEARCH_AREA_FROM_VEHICLE(iVehicle)," should shrink because player is near vehicle #", iVehicle)
				ENDIF
			ENDIF
			
			IF MARK_SEARCH_AREA_FOUND_NEAR_VEHICLE()
				IF NOT IS_SEARCH_AREA_CLIENT_BIT_SET(PARTICIPANT_ID(), GET_SEARCH_AREA_FROM_VEHICLE(iVehicle), eSEARCHAREACLIENTBITSET_AREA_SEARCHED)
					SET_SEARCH_AREA_CLIENT_BIT(GET_SEARCH_AREA_FROM_VEHICLE(iVehicle), eSEARCHAREACLIENTBITSET_AREA_SEARCHED)
					PRINTLN("[GB_CASINO] [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_IS_PLAYER_NEAR_VEHICLE - Search area ", GET_SEARCH_AREA_FROM_VEHICLE(iVehicle)," eSEARCHAREACLIENTBITSET_AREA_SEARCHED vehicle #", iVehicle)
				ENDIF
			ENDIF
			
			BAD_PRESS_VAN_HELP(iVehicle)
			
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_SPOOK_VEHICLE(INT iVehicle, VEHICLE_INDEX vehID)

	IF NOT RUN_SPOOK_LOGIC(iVehicle)
		EXIT
	ENDIF
	
	BOOL bPlayerInAirVeh = PLAYER_USING_OPPRESSOR() OR IS_PED_IN_ANY_HELI(PLAYER_PED_ID()) OR IS_PED_IN_ANY_PLANE(PLAYER_PED_ID()) 
	
	FLOAT fSpookDist = BAD_PRESS_SPOOK_DIST
	
	IF bPlayerInAirVeh 
		fSpookDist = fSpookDist * 4
	ENDIF
	
	IF IS_PLAYER_NEAR_VEHICLE(vehID, fSpookDist)
		IF NOT IS_VEHICLE_CLIENT_BIT_SET(iVehicle, PARTICIPANT_ID(), eVEHICLECLIENTBITSET_SPOOKED)
			SET_VEHICLE_CLIENT_BIT(iVehicle, eVEHICLECLIENTBITSET_SPOOKED)
			PRINTLN("[GB_CASINO] [", GET_CURRENT_VARIATION_STRING(), "] - [SPOOK] MAINTAIN_SPOOK_VEHICLE - Vehicle is spooked #", iVehicle)
		ENDIF
	ELSE
		IF IS_VEHICLE_CLIENT_BIT_SET(iVehicle, PARTICIPANT_ID(), eVEHICLECLIENTBITSET_SPOOKED)
			CLEAR_CLIENT_VEHICLE_BIT(iVehicle, eVEHICLECLIENTBITSET_SPOOKED)
			PRINTLN("[GB_CASINO] [", GET_CURRENT_VARIATION_STRING(), "] - [SPOOK] MAINTAIN_SPOOK_VEHICLE - Vehicle not spooked #", iVehicle)
		ENDIF
	ENDIF				

ENDPROC

PROC MAINTAIN_TAIL_VEHICLE(INT iVehicle, VEHICLE_INDEX vehID)
	IF NOT SHOULD_UPDATE_TAIL_VEHICLE()
	OR NOT SHOULD_TAIL_VEHICLE(iVehicle)
		EXIT
	ENDIF
	
	FLOAT fFailDistance = TO_FLOAT(ciMAX_TAIL_VEHICLE_DISTANCE_M)
	
	SWITCH GET_GBC_VARIATION()
		CASE CSV_BAD_PRESS
			fFailDistance = 800.00
		BREAK
	ENDSWITCH
	
	IF IS_PLAYER_NEAR_VEHICLE(vehID, fFailDistance)
		IF NOT IS_VEHICLE_CLIENT_BIT_SET(iVehicle, PARTICIPANT_ID(), eVEHICLECLIENTBITSET_TAILED)
			SET_VEHICLE_CLIENT_BIT(iVehicle, eVEHICLECLIENTBITSET_TAILED)
			PRINTLN("[GB_CASINO] [", GET_CURRENT_VARIATION_STRING(), "] - [TAIL] MAINTAIN_TAIL_VEHICLE - This client is tailing vehicle #", iVehicle)
		ENDIF
	ELSE
		IF IS_VEHICLE_CLIENT_BIT_SET(iVehicle, PARTICIPANT_ID(), eVEHICLECLIENTBITSET_TAILED)
			CLEAR_CLIENT_VEHICLE_BIT(iVehicle, eVEHICLECLIENTBITSET_TAILED)
			PRINTLN("[GB_CASINO] [", GET_CURRENT_VARIATION_STRING(), "] - [TAIL] MAINTAIN_TAIL_VEHICLE - This client has lost vehicle #", iVehicle)
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_FREEZE_VEHICLE_POSITION(INT iVehicle,VEHICLE_INDEX vehID)
	SWITCH GET_GBC_VARIATION()
		CASE CSV_PASSED_OUT
			SWITCH GET_GBC_SUBVARIATION()
				CASE CSS_PO_TONGVA_HILLS
					IF NOT IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_FROZEN)
						IF DOES_ENTITY_EXIST(vehID)
						AND GET_ENTITY_MODEL(vehID) = JOURNEY
						AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPed[0].netId)
							FREEZE_ENTITY_POSITION(vehID, TRUE)
							PRINTLN("[GB_CASINO] [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_FREEZE_VEHICLE_POSITION - Frozen position of vehicle #", iVehicle)
							SET_VEHICLE_CLIENT_BIT(iVehicle, eVEHICLECLIENTBITSET_FROZEN)
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

PROC MAINTAIN_VEHICLE_INVINCIBILITY(INT iVehicle, VEHICLE_INDEX vehID)
	SWITCH GET_GBC_VARIATION()
		CASE CSV_DEFEND_CASINO
			SWITCH iVehicle
				CASE 2
					IF NOT IS_ENTITY_DEAD(vehID) AND NOT GET_ENTITY_CAN_BE_DAMAGED(vehID)
					AND IS_PED_BIT_SET(8, ePEDBITSET_I_HAVE_REACHED_FINAL_CHECKPOINT)
					AND MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
						SET_ENTITY_INVINCIBLE(vehID, FALSE) //Remove the vehicle's invincibility only if it has reached its destination.
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL SHOULD_VEHICLE_BE_DESTROYED(INT iVehicle, VEHICLE_INDEX vehID)

	SWITCH GET_GBC_VARIATION()
		CASE CSV_ESCORT_DUTY
			IF IS_VEHICLE_A_CARRIER_VEHICLE(iVehicle)			
				RETURN GET_VEHICLE_BODY_HEALTH(vehID) <= 0
			ENDIF
		BREAK
		CASE CSV_BODY_DISPOSAL
			SWITCH GET_GBC_SUBVARIATION()
				CASE CSS_BD_VARIATION_4
					IF IS_VEHICLE_A_CARRIER_VEHICLE(iVehicle)
						RETURN GET_VEHICLE_BODY_HEALTH(vehID) <= 0
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
//		CASE CSV_RECOVER_LUXURY_CAR 
//			IF IS_VEHICLE_A_CARRIER_VEHICLE(iVehicle)
//				FLOAT fHealthPercentage 
//				fHealthPercentage = GET_VEHICLE_HEALTH_PERCENTAGE(vehID)
//				INT iCurrentHealthPercentage 
//				iCurrentHealthPercentage = CEIL(fHealthPercentage)
//			
//				RETURN iCurrentHealthPercentage <= 0
//			ENDIF
//		BREAK
	ENDSWITCH
	
	RETURN FALSE

ENDFUNC

FUNC BOOL SHOULD_SET_RADIO_STATION_FOR_VEHICLE(INT iVehicle)
	SWITCH GET_GBC_VARIATION()
		CASE CSV_HIGH_ROLLER
			SWITCH iVehicle
				CASE 2
				CASE 3	RETURN TRUE
			ENDSWITCH
		BREAK
//		CASE CSV_RECOVER_LUXURY_CAR RETURN iVehicle = MISSION_ENTITY_ONE
		CASE CSV_HIGH_ROLLER_TOUR	RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC STRING GET_RADIO_STATION_NAME_FOR_VEHICLE(INT iVehicle)
	SWITCH GET_GBC_VARIATION()
		CASE CSV_HIGH_ROLLER
			SWITCH iVehicle
				CASE 2
				CASE 3	RETURN "RADIO_13_JAZZ"
			ENDSWITCH
		BREAK
		CASE CSV_RECOVER_LUXURY_CAR
			SWITCH GET_GBC_SUBVARIATION()
				CASE CSS_RLC_IMPOUND		
					SWITCH iVehicle
						CASE 0 	RETURN "RADIO_02_POP"	
					ENDSWITCH
				BREAK	
				CASE CSS_RLC_VESPUCCI		
					SWITCH iVehicle
						CASE 0 	RETURN "RADIO_16_SILVERLAKE"	
					ENDSWITCH
				BREAK	
				CASE CSS_RLC_RANCHO			
					SWITCH iVehicle
						CASE 0 	RETURN "RADIO_03_HIPHOP_NEW"	
					ENDSWITCH
				BREAK	
			ENDSWITCH
		BREAK
		CASE CSV_HIGH_ROLLER_TOUR	RETURN "RADIO_13_JAZZ"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

PROC MAINTAIN_VEHICLE_HEALTH_DAMAGE(INT iVehicle, VEHICLE_INDEX vehID)

	SWITCH GET_GBC_VARIATION()
		CASE CSV_ESCORT_DUTY
			IF GET_MODE_STATE() = eMODESTATE_PROTECT_ENTITY
				IF IS_VEHICLE_A_CARRIER_VEHICLE(iVehicle)
					IF HAS_NET_TIMER_EXPIRED(stVehicleDamageTimer, 1000)
						IF fDistanceFromClosestVehicle > 1000.0
							FLOAT fHealth
							fHealth = GET_VEHICLE_BODY_HEALTH(vehID)
							SET_VEHICLE_BODY_HEALTH(vehID, fHealth - 50.0)
							PRINTLN("[GB_CASINO] [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_VEHICLE_HEALTH_DAMAGE - Vehicle #", iVehicle, " out of range, doing 50.0 damage to vehicle.")
						ENDIF
						RESET_NET_TIMER(stVehicleDamageTimer)
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH

ENDPROC

FUNC BOOL SHOULD_REDUCE_TRAFFIC_AROUND_VEHICLE(INT iVehicle)
	SWITCH GET_GBC_VARIATION()
		CASE CSV_ESCORT_DUTY
			IF IS_VEHICLE_A_CARRIER_VEHICLE(iVehicle)
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

VECTOR vCurrentPopZoneCoords = <<5000,8500,0>> //Off the map
CONST_FLOAT POP_ZONE_RADIUS				500.0
CONST_FLOAT POP_ZONE_EXIT_DISTANCE		550.0
INT iCurrentPopZone = -1
PROC MAINTAIN_TRAFFIC_REDUCTION_SPHERE_AROUND_VEHICLE(INT iVehicle, VEHICLE_INDEX vehId)
	IF NOT SHOULD_REDUCE_TRAFFIC_AROUND_VEHICLE(iVehicle)
	OR GB_GET_LOCAL_PLAYER_MISSION_HOST() != PLAYER_ID()
		EXIT
	ENDIF
	
	VECTOR vVehicleCoords = GET_ENTITY_COORDS(vehId, FALSE)
	FLOAT fRadius = POP_ZONE_RADIUS
	
	IF VDIST2(vVehicleCoords, vCurrentPopZoneCoords) > (POP_ZONE_EXIT_DISTANCE * POP_ZONE_EXIT_DISTANCE)
		IF iCurrentPopZone != -1
			REMOVE_POP_MULTIPLIER_SPHERE(iCurrentPopZone, FALSE)
		ENDIF
		vCurrentPopZoneCoords = vVehicleCoords
		iCurrentPopZone = ADD_POP_MULTIPLIER_SPHERE(vCurrentPopZoneCoords, fRadius, 1.0, 0.02, FALSE)
		PRINTLN("[GB_CASINO] [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_TRAFFIC_REDUCTION_SPHERE_AROUND_VEHICLE - Updated pop multiplier zone to ", vCurrentPopZoneCoords)
	ENDIF
ENDPROC

PROC CLEANUP_TRAFFIC_REDUCTION_SPHERE_AROUND_VEHICLE()
	IF iCurrentPopZone != -1
		IF DOES_POP_MULTIPLIER_SPHERE_EXIST(iCurrentPopZone)
			REMOVE_POP_MULTIPLIER_SPHERE(iCurrentPopZone,FALSE)
			PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] [SCRIPT_CLEANUP] CLEANUP_TRAFFIC_REDUCTION_SPHERE_AROUND_VEHICLE - Removed pop multiplier zone. ")
			iCurrentPopZone = -1
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_NON_MISSION_ENTITY_VEHICLE_BODIES()

	INT iVehicle
	VECTOR vSpawnLocation
	FLOAT fSpawnHeading
	VEHICLE_SPAWN_LOCATION_PARAMS vehicleSpawnLocationParams
	VEHICLE_INDEX vehID
	BOOL bEmptySeatAvailable
	
	fDistanceFromClosestVehicle = 99999.0
	CLEAR_CLIENT_BIT(eCLIENTBITSET_I_AM_IN_PREREQUISITE_VEHICLE #IF IS_DEBUG_BUILD , TRUE #ENDIF)
	CLEAR_CLIENT_BIT(eCLIENTBITSET_I_AM_IN_FRONT_RIGHT_OF_PREREQUISITE_VEHICLE #IF IS_DEBUG_BUILD , TRUE #ENDIF)
	CLEAR_CLIENT_BIT(eCLIENTBITSET_I_AM_DRIVER_OF_PREREQUISITE_VEHICLE #IF IS_DEBUG_BUILD , TRUE #ENDIF)
	CLEAR_CLIENT_BIT(eCLIENTBITSET_I_AM_ANY_PASSENGER_OF_PREREQUISITE_VEHICLE #IF IS_DEBUG_BUILD , TRUE #ENDIF)
	
	REPEAT GB_GET_GBC_NUM_VEH_REQUIRED(GET_GBC_VARIATION(), GET_GBC_SUBVARIATION(), GB_GET_LOCAL_PLAYER_GANG_BOSS(), FALSE, GET_EXTRA_PARAM_FOR_VEHICLE_SPAWN(), TRUE, FALSE) iVehicle
	
		MAINTAIN_NON_MISSION_ENTITY_VEHICLE_UNIQUE_BLIP(iVehicle)
		MAINTAIN_DESTROY_UNDRIVEABLE_NON_MISSION_ENTITY_VEHICLE(iVehicle)

		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
			vehID = NET_TO_VEH(serverBD.sVehicle[iVehicle].netId)
			
			MAINTAIN_ATTACHING_VEHICLE_LOGIC(iVehicle, vehID)
			MAINTAIN_SHOULD_DESTROY_HELICOPTER(iVehicle)
			MAINTAIN_WARPING_VEHICLES_ON_DELIVERY(iVehicle)
			MAINTAIN_PMSD_FOR_CARRIER_VEHICLES(iVehicle, vehID)
			MAINTAIN_IS_PLAYER_NEAR_VEHICLE(iVehicle, vehID)
			MAINTAIN_SPOOK_VEHICLE(iVehicle, vehID)
			MAINTAIN_TAIL_VEHICLE(iVehicle, vehID)
			MAINTAIN_FREEZE_VEHICLE_POSITION(iVehicle, vehID)
			MAINTAIN_VEHICLE_INVINCIBILITY(iVehicle, vehID)
	
			IF NOT IS_VEHICLE_CLIENT_BIT_SET(iVehicle, PARTICIPANT_ID(), eVEHICLECLIENTBITSET_DESTROYED)
				IF GET_VEHICLE_STATE(iVehicle) > eVEHICLESTATE_CREATE
					IF NOT IS_NET_VEHICLE_DRIVEABLE(serverBD.sVehicle[iVehicle].netId)
						SET_VEHICLE_CLIENT_BIT(iVehicle, eVEHICLECLIENTBITSET_DESTROYED)
					ENDIF
				ENDIF
			ENDIF
						
			SWITCH GET_VEHICLE_STATE(iVehicle)
				
				CASE eVEHICLESTATE_INACTIVE

				BREAK
				
				CASE eVEHICLESTATE_CREATE
					
				BREAK
				
				CASE eVEHICLESTATE_DRIVEABLE
				
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
						MAINTAIN_TRAFFIC_REDUCTION_SPHERE_AROUND_VEHICLE(iVehicle, vehID)
					
						IF NOT IS_VEHICLE_CLIENT_BIT_SET(iVehicle, PARTICIPANT_ID(), eVEHICLECLIENTBITSET_SET_FORCED_SEAT_USAGE)
							SWITCH GET_GBC_VARIATION()
								CASE CSV_HIGH_ROLLER
									SWITCH iVehicle
										CASE 2
										CASE 3
											SET_PED_VEHICLE_FORCED_SEAT_USAGE(PLAYER_PED_ID(), vehID, 0, ENUM_TO_INT(VC_FORCE_USE_FRONT_SEATS))
										BREAK
									ENDSWITCH
								BREAK
							ENDSWITCH
							
							SET_VEHICLE_CLIENT_BIT(iVehicle, eVEHICLECLIENTBITSET_SET_FORCED_SEAT_USAGE)
						ENDIF
						
						IF SHOULD_SET_RADIO_STATION_FOR_VEHICLE(iVehicle)
						AND NOT IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_SET_RADIO_STATION)
						AND NOT IS_VEHICLE_CLIENT_BIT_SET(iVehicle, PARTICIPANT_ID(), eVEHICLECLIENTBITSET_SET_RADIO_STATION)
						AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBd.sVehicle[iVehicle].netId)
						AND NOT IS_VEHICLE_SEAT_FREE(vehID)
						AND GET_IS_VEHICLE_ENGINE_RUNNING(vehID)
							SET_VEHICLE_ENGINE_ON(vehId, TRUE, TRUE)
							SET_VEHICLE_RADIO_ENABLED(vehId, TRUE)
							SET_VEHICLE_RADIO_LOUD(vehId, FALSE)
							SET_VEH_FORCED_RADIO_THIS_FRAME(vehId)
							SET_VEH_RADIO_STATION(vehId, GET_RADIO_STATION_NAME_FOR_VEHICLE(iVehicle))		
							SET_RADIO_TO_STATION_NAME(GET_RADIO_STATION_NAME_FOR_VEHICLE(iVehicle))
							PRINTLN("SET_VEH_RADIO_STATION TO ", GET_RADIO_STATION_NAME_FOR_VEHICLE(iVehicle), " iVehicle = ", iVehicle)
							SET_VEHICLE_CLIENT_BIT(iVehicle, eVEHICLECLIENTBITSET_SET_RADIO_STATION)
						ENDIF
						
						IF IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_I_AM_CARRIER_VEHICLE)
							IF NOT IS_VEHICLE_CLIENT_BIT_SET(iVehicle, PARTICIPANT_ID(), eVEHICLECLIENTBITSET_ATTEMPTED_CARRIER_VEHICLE_ENTRY)
								IF GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()) = vehID
								AND IS_PED_TRYING_TO_ENTER_A_LOCKED_VEHICLE(PLAYER_PED_ID())
									SET_VEHICLE_CLIENT_BIT(iVehicle, eVEHICLECLIENTBITSET_ATTEMPTED_CARRIER_VEHICLE_ENTRY)
								ENDIF
							ENDIF
						ENDIF
						
						IF SHOULD_TRIGGER_CARRIER_DESTROYED_HELP_TEXT()
						AND IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_I_AM_CARRIER_VEHICLE)
						AND NOT IS_VEHICLE_DRIVEABLE(vehID)
						AND GET_CLIENT_MODE_STATE() >= eMODESTATE_COLLECT_MISSION_ENTITY
							TRIGGER_HELP(eHELPTEXT_CARRIER_DESTROYED)
						ENDIF
						
						IF GET_END_REASON() != eENDREASON_NO_REASON_YET
						AND IS_ENTITY_ALIVE(vehID)

							MODEL_NAMES eVehModel
							eVehModel = GET_ENTITY_MODEL(vehID)
							IF NOT SHOULD_ALLOW_USE_AFTER_MISSION(eVehModel, iVehicle)
								IF NOT SHOULD_EMPTY_AND_LOCK_VEHICLE_ON_END(eVehModel, iVehicle)
									IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
									AND GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()) = vehId
										IF SHOULD_USE_PMSD_FOR_VEHICLE(eVehModel)
											GB_SET_GLOBAL_CLIENT_BIT1(eGB_GLOBAL_CLIENT_BITSET_1_INSIDE_POST_MISSION_SELF_DESTRUCT_VEHICLE)
											MPGlobalsAmbience.sPostMissionSelfDestruct.bIgnoreMessages = TRUE
										ELSE
											IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
												SET_VEHICLE_ENGINE_HEALTH(vehId, -1000.0)	// Kill engine if completely empty
											ENDIF
										ENDIF
									ELIF IS_VEHICLE_EMPTY(vehId, TRUE, TRUE)		// Check empty of players
										IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
											IF IS_VEHICLE_EMPTY(vehId, TRUE)		// Check empty of all peds
												SET_VEHICLE_ENGINE_HEALTH(vehId, -1000.0)	// Kill engine if completely empty
											ELIF IS_VEHICLE_AN_AMBUSH_VEHICLE(iVehicle)
												SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(vehID, TRUE)	// Lock the doors if it's an ambush vehicle and peds are still inside it
											ENDIF
										ENDIF
									ENDIF
								ELSE
									IF EMPTY_AND_LOCK_VEHICLE(vehID, timeLeaveEventVeh[iVehicle], FALSE)
										PRINTLN("[GB_CASINO] [", GET_CURRENT_VARIATION_STRING(), "] - Vehicle #", iVehicle, " has been emptied and locked.")
									ENDIF
									SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(vehID, TRUE)
									
									// Empty, lock and destroy for this mode
									SWITCH GET_GBC_VARIATION()
										CASE CSV_BODY_DISPOSAL
											IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
												SET_VEHICLE_ENGINE_HEALTH(vehId, -1000.0)	// Kill engine if completely empty
											ENDIF
										BREAK
									ENDSWITCH
									
								ENDIF
							ELSE
								PRINTLN("[GB_CASINO] [", GET_CURRENT_VARIATION_STRING(), "] - Vehicle #", iVehicle, " is allowed for use after mission. Not locking or starting kill switch.")
							ENDIF
						ENDIF

						IF IS_ENTITY_ALIVE(vehID)
							IF SHOULD_RUN_VEHICLE_DISTANCE_CHECKS()
								IF IS_VEHICLE_VALID_FOR_CLOSEST_CHECK(iVehicle)
									IF IS_NET_VEHICLE_DRIVEABLE(serverBD.sVehicle[iVehicle].netId)
										FLOAT fDistance
										fDistance = GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),vehID)
										IF fDistance < fDistanceFromClosestVehicle
											fDistanceFromClosestVehicle = fDistance
											iClosestVehicle = iClosestVehicle // To please release compiler
											iClosestVehicle = iVehicle
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						
							IF SHOULD_VEHICLE_BE_MADE_DESTROYABLE(iVehicle)	
							AND NOT IS_VEHICLE_CLIENT_BIT_SET(iVehicle, PARTICIPANT_ID(), eVEHICLECLIENTBITSET_MADE_VEHICLE_DESTROYABLE)
								IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
									SET_ENTITY_INVINCIBLE(vehID,FALSE)							
									SET_VEHICLE_CLIENT_BIT(iVehicle, eVEHICLECLIENTBITSET_MADE_VEHICLE_DESTROYABLE)
								ENDIF
							ENDIF
							
							IF IS_VEHICLE_BIT_SET(iVehicle,eVEHICLEBITSET_WARP_VEHICLE_ON_STUCK)
								MAINTAIN_WARPING_VEHICLES(serverBD.sVehicle[iVehicle].netId) 
							ENDIF

							IF SHOULD_VEHICLE_BE_UNLOCKED_ON_CONDITION(iVehicle)	
							AND (GET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(vehId, PLAYER_ID())
							OR GET_VEHICLE_DOOR_LOCK_STATUS(vehId) = VEHICLELOCK_LOCKED)
								SET_VEHICLE_UNLOCK_STATE_FOR_VARIATION(serverBD.sVehicle[iVehicle].netId, vehID)
								SET_VEHICLE_CLIENT_BIT(iVehicle, eVEHICLECLIENTBITSET_I_AM_UNLOCKED)
							ENDIF
							
							IF IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_I_AM_RADIO_VEHICLE)
								IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
									TURN_ON_VEHICLE_RADIO(vehID)
									PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_CAR_MEET_VEHICLE_ATTRIBUTES - Turned on vehicle radio for vehicle ",iVehicle)
								ENDIF
							ENDIF
							
							IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehID)
								SET_VEHICLE_CLIENT_BIT(iVehicle, eVEHICLECLIENTBITSET_I_HAVE_ENTERED_VEHICLE)
								IF SHOULD_PEDS_REACT_ON_VEHICLE_ENTRY(iVehicle)
									SET_I_WANT_PEDS_TO_REACT(GET_PED_TO_REACT_ON_VEHICLE_ENTRY(iVehicle),TRUE)
								ENDIF
							ENDIF
							
							IF SHOULD_VEHICLE_BE_DESTROYED(iVehicle, vehID)
								IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
									NETWORK_EXPLODE_VEHICLE(vehID)
									PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - SHOULD_VEHICLE_BE_DESTROYED - Vehicle has been exploded ",iVehicle)
								ENDIF
							ENDIF
							
							IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
							
								IF SHOULD_VEHICLE_SIREN_ACTIVATE(iVehicle)
								AND NOT IS_VEHICLE_SIREN_ON(vehID)
									SET_VEHICLE_SIREN(vehID, TRUE)
									PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_CAR_MEET_VEHICLE_ATTRIBUTES - Turned on vehicle siren for vehicle ",iVehicle)
								ENDIF
							
								IF SHOULD_VEHICLE_DOOR_OPEN(iVehicle, vehID)
									MAINTAIN_VEHICLE_DOOR_OPENING(iVehicle, vehID)
								ENDIF
								
								IF SHOULD_RAISE_VEHICLE_ROOF(iVehicle, vehID)
									MAINTAIN_VEHICLE_ROOF_RAISE(iVehicle, vehID)
								ENDIF
								
								MAINTAIN_VEHICLE_HEALTH_DAMAGE(iVehicle, vehID)
							
//								IF SHOULD_VEHICLE_SPEED_BE_ALTERED(iVehicle,vehID)
//									SET_VEHICLE_MAX_SPEED(vehID,GET_ALTERED_VEHICLE_SPEED())			
//								ELSE
//									SET_VEHICLE_MAX_SPEED(vehID,-1)
//								ENDIF
							ENDIF
						ENDIF
					
//						IF IS_ENTITY_ALIVE(vehID)
//							IF SHOULD_VARIATION_HAVE_PASSENGER_SEAT_BOMBS()
//								IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//									IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehID)
//										IF IS_VEHICLE_A_SUPPORT_VEHICLE(iVehicle)
//											IF NOT MPGlobalsAmbience.bEnablePassengerBombing
//												MPGlobalsAmbience.bEnablePassengerBombing = TRUE
//												PRINTLN("[GB_CASINO] [", GET_CURRENT_VARIATION_STRING(), "] - Vehicle #", iVehicle, " has enabled passenger bombs")
//											ENDIF
//										ENDIF
//									ENDIF
//								ELSE
//									IF MPGlobalsAmbience.bEnablePassengerBombing
//										MPGlobalsAmbience.bEnablePassengerBombing = FALSE
//										PRINTLN("[GB_CASINO] [", GET_CURRENT_VARIATION_STRING(), "] - Vehicle #", iVehicle, " has disabled passenger bombs")
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
					
						IF IS_VEHICLE_BIT_SET(iVehicle,eVEHICLEBITSET_I_AM_PREREQUISITE_VEHICLE)
							IF IS_NET_VEHICLE_DRIVEABLE(serverBD.sVehicle[iVehicle].netId)
								IF IS_NET_PLAYER_OK(PLAYER_ID(),FALSE)
									fDistanceFromPrerequisiteVehicle = GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),vehID)
									IF fDistanceFromPrerequisiteVehicle = fDistanceFromPrerequisiteVehicle // Stop compiler moaning. May be useful later. 
										fDistanceFromPrerequisiteVehicle = fDistanceFromPrerequisiteVehicle
									ENDIF

									IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehID)
										SET_CLIENT_BIT(eCLIENTBITSET_I_AM_IN_PREREQUISITE_VEHICLE #IF IS_DEBUG_BUILD , TRUE #ENDIF)
										
										VEHICLE_SEAT seat
										seat = GET_SEAT_PED_IS_IN(PLAYER_PED_ID())
										
										IF seat = VS_DRIVER
											SET_CLIENT_BIT(eCLIENTBITSET_I_AM_DRIVER_OF_PREREQUISITE_VEHICLE #IF IS_DEBUG_BUILD , TRUE #ENDIF)
										ELIF seat = VS_FRONT_RIGHT
											SET_CLIENT_BIT(eCLIENTBITSET_I_AM_IN_FRONT_RIGHT_OF_PREREQUISITE_VEHICLE #IF IS_DEBUG_BUILD , TRUE #ENDIF)
											SET_CLIENT_BIT(eCLIENTBITSET_I_AM_ANY_PASSENGER_OF_PREREQUISITE_VEHICLE #IF IS_DEBUG_BUILD , TRUE #ENDIF)
										ELIF seat = VS_BACK_LEFT
											SET_CLIENT_BIT(eCLIENTBITSET_I_AM_ANY_PASSENGER_OF_PREREQUISITE_VEHICLE #IF IS_DEBUG_BUILD , TRUE #ENDIF)
										ELIF seat = VS_BACK_RIGHT
											SET_CLIENT_BIT(eCLIENTBITSET_I_AM_ANY_PASSENGER_OF_PREREQUISITE_VEHICLE #IF IS_DEBUG_BUILD , TRUE #ENDIF)
										ELIF seat = VS_ANY_PASSENGER
											SET_CLIENT_BIT(eCLIENTBITSET_I_AM_ANY_PASSENGER_OF_PREREQUISITE_VEHICLE #IF IS_DEBUG_BUILD , TRUE #ENDIF)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF	
					
					ENDIF
					
				BREAK
				
				CASE eVEHICLESTATE_NOT_DRIVEABLE
				
					// Lock ambush vehicle when they're cleaned up 
					IF IS_VEHICLE_AN_AMBUSH_VEHICLE(iVehicle)
					AND IS_NET_VEHICLE_DRIVEABLE(serverBD.sVehicle[iVehicle].netId)
						IF GET_VEHICLE_DOOR_LOCK_STATUS(vehID) != VEHICLELOCK_LOCKED
						AND GET_ENTITY_MODEL(vehID) = TECHNICAL
							IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
								SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(NET_TO_VEH(serverBD.sVehicle[iVehicle].netId), TRUE)
							ENDIF
						ENDIF
					ENDIF
					
				BREAK
				
				CASE eVEHICLESTATE_WARP
					IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
						vehicleSpawnLocationParams.fMinDistFromCoords = 20
						vehicleSpawnLocationParams.fMaxDistance = 100
						vehicleSpawnLocationParams.bConsiderHighways = TRUE
						vehicleSpawnLocationParams.bCheckEntityArea = TRUE
						vehicleSpawnLocationParams.bCheckOwnVisibility = FALSE
						vehicleSpawnLocationParams.bConsiderOnlyActiveNodes = FALSE
						vehID = NET_TO_VEH(serverBD.sVehicle[iVehicle].netId)
						IF HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(GET_ENTITY_COORDS(vehID), <<0.0, 0.0, 0.0>>, GET_ENTITY_MODEL(vehID), TRUE, vSpawnLocation, fSpawnHeading, vehicleSpawnLocationParams)									  
							NETWORK_FADE_IN_ENTITY(vehID, TRUE)
							SET_ENTITY_COORDS_NO_OFFSET(vehID, vSpawnLocation)
							SET_ENTITY_HEADING(vehID, fSpawnHeading)
							SET_VEHICLE_ON_GROUND_PROPERLY(vehID)
							BROADCAST_EVENT_WARP_BUYSELL_SCRIPT_VEHICLE(iVehicle, TRUE, serverBD.iLaunchPosix)
							PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] eVEHICLESTATE_WARP - Warping vehicle #", iVehicle, " to ", vSpawnLocation, " as local player is delivering in it.")
						ENDIF
					ENDIF
				BREAK
				
				CASE eVEHICLESTATE_NO_LONGER_NEEDED
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
						IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
							IF SHOULD_REMOVE_ANCHOR_ON_NO_LONGER_NEEDED(iVehicle)
								SET_BOAT_ANCHOR(vehID, FALSE)
							ENDIF
							
							IF SHOULD_UNLOCK_VEHICLE_DOORS_FOR_ALL_PLAYERS_ON_NO_LONGER_NEEDED(iVehicle)
								SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(vehID, FALSE)
							ENDIF
							
							CLEANUP_NET_ID(serverBD.sVehicle[iVehicle].netId)
						ENDIF
					ENDIF
				BREAK
				
				CASE eVEHICLESTATE_RESET
					
				BREAK
				
			ENDSWITCH
		ENDIF
	ENDREPEAT
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF bEmptySeatAvailable
			IF NOT IS_SERVER_BIT_SET(eSERVERBITSET_PREREQ_SEATS_AVAILABLE)
				SET_SERVER_BIT(eSERVERBITSET_PREREQ_SEATS_AVAILABLE)
			ENDIF
		ELSE
			IF IS_SERVER_BIT_SET(eSERVERBITSET_PREREQ_SEATS_AVAILABLE)
				CLEAR_SERVER_BIT(eSERVERBITSET_PREREQ_SEATS_AVAILABLE)
			ENDIF
		ENDIF
		ENDIF
	
	fLastDistanceFromClosestVehicle = fDistanceFromClosestVehicle
	fDistanceFromClosestVehicle = fLastDistanceFromClosestVehicle		//Stupid compiler
	
ENDPROC

FUNC BOOL KICK_OUT_VEH()

	SWITCH GET_GBC_SUBVARIATION()
		CASE CSS_BD_VARIATION_1
		CASE CSS_BD_VARIATION_3
		CASE CSS_BD_VARIATION_4
		CASE CSS_RLC_IMPOUND
		CASE CSS_RLC_VESPUCCI
		CASE CSS_RLC_RANCHO
			RETURN SHOULD_HALT_PLAYER_VEHICLE(FALSE)
	ENDSWITCH

	RETURN FALSE
ENDFUNC

FUNC BOOL LOCK_VEH_AFTER_HALT()

	SWITCH GET_GBC_SUBVARIATION()
		CASE CSS_BD_VARIATION_1
		CASE CSS_BD_VARIATION_3
		CASE CSS_BD_VARIATION_4
		CASE CSS_RLC_IMPOUND
		CASE CSS_RLC_VESPUCCI
		CASE CSS_RLC_RANCHO
			RETURN TRUE
	ENDSWITCH

	RETURN FALSE
ENDFUNC

PROC MAINTAIN_HALTING_PLAYER_VEHICLE()
	IF NOT IS_LOCAL_BIT_SET(eLOCALBITSET_PLAYER_HALTED)
		IF SHOULD_HALT_PLAYER_VEHICLE()
			IF BRING_PLAYER_VEHICLE_TO_HALT()
			
				SET_LOCAL_BIT(eLOCALBITSET_PLAYER_HALTED)
				
			ENDIF
			
			IF KICK_OUT_VEH()
				SET_LOCAL_BIT(eLOCALBITSET_KICK_PLAYER_OUT_VEH)
			ENDIF
			
		ENDIF
	ELSE
		IF NOT SHOULD_HALT_PLAYER_VEHICLE()
			CLEAR_LOCAL_BIT(eLOCALBITSET_PLAYER_HALTED)
		ENDIF
	ENDIF
	
	IF IS_LOCAL_BIT_SET(eLOCALBITSET_KICK_PLAYER_OUT_VEH)
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			TASK_LEAVE_VEHICLE(PLAYER_PED_ID(), GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
			
			IF LOCK_VEH_AFTER_HALT()
				SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), TRUE)
			ENDIF
			
			CLEAR_LOCAL_BIT(eLOCALBITSET_KICK_PLAYER_OUT_VEH)
		ELSE
			CLEAR_LOCAL_BIT(eLOCALBITSET_KICK_PLAYER_OUT_VEH)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL GET_ANY_OTHER_VEHICLE_AT_COORDS(VECTOR vTruck, NETWORK_INDEX niVeh )
	VEHICLE_INDEX viVeh, viVeh2
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(niVeh)
		viVeh = NET_TO_VEH(niVeh)
	ENDIF
	viVeh2 = GET_CLOSEST_VEHICLE(vTruck, 2.0, DUMMY_MODEL_FOR_SCRIPT,
		VEHICLE_SEARCH_FLAG_RETURN_LAW_ENFORCER_VEHICLES 
		| VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES 
		| VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES 
		| VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_GROUP_MEMBERS 
		| VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_PLAYER                                                           
		| VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_DEAD_OR_DYING_PED                                   
		| VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_WITH_PEDS_ENTERING_OR_EXITING                                         
		| VEHICLE_SEARCH_FLAG_ALLOW_VEHICLE_OCCUPANTS_TO_BE_PERFORMING_A_SCRIPTED_TASK      
		| VEHICLE_SEARCH_FLAG_ALLOW_VEHICLE_OCCUPANTS_TO_BE_PERFORMING_A_NON_DEFAULT_TASK)
	IF DOES_ENTITY_EXIST(viVeh2)
	AND viVeh2 != viVeh
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_PREREQUISITE_VEHICE_MOVE_TO_DESTINATION()
	RETURN FALSE
ENDFUNC

FUNC VECTOR GET_PREREQUISITE_VEHICE_GO_TO_COORDS()
	RETURN <<0.0,0.0,0.0>>
ENDFUNC

FUNC BOOL IS_HELICOPTER_DAMAGED(VEHICLE_INDEX vehId #IF IS_DEBUG_BUILD , INT iVehicle #ENDIF )
	
	IF NOT IS_VEHICLE_DRIVEABLE(vehId)
		PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_SHOULD_DESTROY_HELICOPTER - IS_HELICOPTER_DAMAGED(", iVehicle, ") - NOT IS_VEHICLE_DRIVEABLE")
		RETURN TRUE
	ENDIF
	
	IF GET_ENTITY_HEALTH(vehId) < 500
		PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_SHOULD_DESTROY_HELICOPTER - IS_HELICOPTER_DAMAGED(", iVehicle, ") - GET_ENTITY_HEALTH <= 500")
		RETURN TRUE
	ENDIF
	
	IF IS_HELI_PART_BROKEN(vehId, TRUE, TRUE, TRUE)
		PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_SHOULD_DESTROY_HELICOPTER - IS_HELICOPTER_DAMAGED(", iVehicle, ") - IS_HELI_PART_BROKEN")
		RETURN TRUE
	ENDIF
	
	IF GET_HELI_TAIL_ROTOR_HEALTH(vehId) <= 30
		PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_SHOULD_DESTROY_HELICOPTER - IS_HELICOPTER_DAMAGED(", iVehicle, ") - GET_HELI_TAIL_ROTOR_HEALTH <= 0")
		RETURN TRUE
	ENDIF
	
	IF GET_HELI_MAIN_ROTOR_HEALTH(vehId) <= 30
		PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_SHOULD_DESTROY_HELICOPTER - IS_HELICOPTER_DAMAGED(", iVehicle, ") - GET_HELI_MAIN_ROTOR_HEALTH <= 0")
		RETURN TRUE
	ENDIF
	
	IF GET_VEHICLE_ENGINE_HEALTH(vehId) <= 30
		PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_SHOULD_DESTROY_HELICOPTER - IS_HELICOPTER_DAMAGED(", iVehicle, ") - GET_VEHICLE_ENGINE_HEALTH <= 0")
		RETURN TRUE
	ENDIF
	
	IF GET_VEHICLE_PETROL_TANK_HEALTH(vehId) <= 30
		PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_SHOULD_DESTROY_HELICOPTER - IS_HELICOPTER_DAMAGED(", iVehicle, ") - GET_VEHICLE_PETROL_TANK_HEALTH <= 0")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_WARPING_STUCK_MISSION_ENTITIES(INT iMissionEntity)
	
	IF (IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_WARP_VEHICLE_ON_STUCK)
	OR SHOULD_WARP_MISSION_ENTITY_ON_STUCK())
	AND NOT HAS_MISSION_ENTITY_BEEN_DELIVERED_OR_DESTROYED(iMissionEntity)
		MAINTAIN_WARPING_VEHICLES(serverBD.sMissionEntities.netId[iMissionEntity])
	ENDIF
	
ENDPROC

FUNC BOOL HAVE_ALL_TARGET_VEHICLES_BEEN_ELIMINATED()

	SWITCH GET_GBC_VARIATION()
		CASE CSV_MAX
			INT iVeh
			REPEAT GB_GET_GBC_NUM_VEH_REQUIRED(GET_GBC_VARIATION(), GET_GBC_SUBVARIATION(), GB_GET_LOCAL_PLAYER_GANG_BOSS(), TRUE, GET_EXTRA_PARAM_FOR_VEHICLE_SPAWN(), TRUE, TRUE) iVeh
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[iVeh].netId)
					IF NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBD.sVehicle[iVeh].netId))
						RETURN FALSE
					ENDIF
				ENDIF
			ENDREPEAT
		BREAK
	ENDSWITCH

	RETURN TRUE
ENDFUNC

PROC MAINTAIN_REPAIR_FLAG()
	SWITCH GET_GBC_VARIATION()
		CASE CSV_RECOVER_LUXURY_CAR
		CASE CSV_MISSING_DELIVERY	
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[MISSION_ENTITY_ONE].netId)
			
				VEHICLE_INDEX vehID 
				vehID = NET_TO_VEH(serverBD.sVehicle[MISSION_ENTITY_ONE].netId)
				FLOAT fHealthPercentage 
				fHealthPercentage = GET_VEHICLE_HEALTH_PERCENTAGE(vehID)
				INT iCurrentHealthPercentage		
				iCurrentHealthPercentage = CEIL(fHealthPercentage)	
				
				IF iCurrentHealthPercentage <= VEHICLE_DAMAGE_LIMIT()
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[MISSION_ENTITY_ONE].netId) // url:bugstar:5691739
						IF NOT IS_VEHICLE_CLIENT_BIT_SET(MISSION_ENTITY_ONE, PARTICIPANT_ID(), eVEHICLECLIENTBITSET_NEEDS_REPAIRED)
							PRINTLN("[REPAIR] SET_VEHICLE_CLIENT_BIT(MISSION_ENTITY_ONE, eVEHICLECLIENTBITSET_NEEDS_REPAIRED) ")
							SET_VEHICLE_CLIENT_BIT(MISSION_ENTITY_ONE, eVEHICLECLIENTBITSET_NEEDS_REPAIRED)
						ENDIF
					ENDIF
				ELSE
					IF IS_VEHICLE_CLIENT_BIT_SET(MISSION_ENTITY_ONE, PARTICIPANT_ID(), eVEHICLECLIENTBITSET_NEEDS_REPAIRED)
						PRINTLN("[REPAIR] CLEAR_CLIENT_VEHICLE_BIT(MISSION_ENTITY_ONE, eVEHICLECLIENTBITSET_NEEDS_REPAIRED) ")
						CLEAR_CLIENT_VEHICLE_BIT(MISSION_ENTITY_ONE, eVEHICLECLIENTBITSET_NEEDS_REPAIRED)
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

/*
PROC MAINTAIN_CLEAR_DROP_OFF_AREA_OF_UNDRIVEABLE_VEHICLES()
	
	BOOL bReset
	
	IF NOT HAS_NET_TIMER_STARTED(dropOffBlockedTimerA)
	
		PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - [CLRDROP] - started dropOffBlockedTimerA")
		START_NET_TIMER(dropOffBlockedTimerA)
			
	ELIF HAS_NET_TIMER_EXPIRED(dropOffBlockedTimerA, CHECK_BLOCKED_DROP_OFF_TIME_LIMIT)
		
		IF NOT HAS_NET_TIMER_STARTED(dropOffBlockedTimerB)
		
			PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - [CLRDROP] - dropOffBlockedTimerA expired = ", CHECK_BLOCKED_DROP_OFF_TIME_LIMIT)
			PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - [CLRDROP] - started dropOffBlockedTimerB")
			START_NET_TIMER(dropOffBlockedTimerB)
			vehicleInDropOffA = GET_VEHICLE_BLOCKING_VEHICLE_DROP_OFF()
	
			#IF IS_DEBUG_BUILD
			PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - [CLRDROP] - vehicleInDropOffA = GET_VEHICLE_BLOCKING_VEHICLE_DROP_OFF = ", NATIVE_TO_INT(vehicleInDropOffA))
			IF DOES_ENTITY_EXIST(vehicleInDropOffA)
				PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - [CLRDROP] - vehicleInDropOffA = DOES_ENTITY_EXIST(vehicleInDropOffA) = TRUE")
				IF IS_VEHICLE_DRIVEABLE(vehicleInDropOffA)
					PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - [CLRDROP] - vehicleInDropOffA = IS_VEHICLE_DRIVEABLE(vehicleInDropOffA) = TRUE")
				ELSE
					PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - [CLRDROP] - vehicleInDropOffA = IS_VEHICLE_DRIVEABLE(vehicleInDropOffA) = FALSE")
				ENDIF
			ELSE
				PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - [CLRDROP] - vehicleInDropOffA = DOES_ENTITY_EXIST(vehicleInDropOffA) = FALSE")
			ENDIF
			#ENDIF

		ELIF HAS_NET_TIMER_EXPIRED(dropOffBlockedTimerB, RECHECK_BLOCKED_DROP_OFF_TIME_LIMIT)
			
			PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - [CLRDROP] - dropOffBlockedTimerB expired = ", RECHECK_BLOCKED_DROP_OFF_TIME_LIMIT)
			
			IF DOES_ENTITY_EXIST(vehicleInDropOffA)
				IF NOT IS_VEHICLE_DRIVEABLE(vehicleInDropOffA)
			
					vehicleInDropOffB = GET_VEHICLE_BLOCKING_VEHICLE_DROP_OFF()
					PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - [CLRDROP] - vehicleInDropOffB = GET_VEHICLE_BLOCKING_VEHICLE_DROP_OFF = ", NATIVE_TO_INT(vehicleInDropOffB))
					
					#IF IS_DEBUG_BUILD
					IF DOES_ENTITY_EXIST(vehicleInDropOffB)
						PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - [CLRDROP] - vehicleInDropOffB = DOES_ENTITY_EXIST(vehicleInDropOffB) = TRUE")
						IF IS_VEHICLE_DRIVEABLE(vehicleInDropOffB)
							PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - [CLRDROP] - vehicleInDropOffB = IS_VEHICLE_DRIVEABLE(vehicleInDropOffB) = TRUE")
						ELSE
							PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - [CLRDROP] - vehicleInDropOffB = IS_VEHICLE_DRIVEABLE(vehicleInDropOffB) = FALSE")
						ENDIF
					ELSE
						PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - [CLRDROP] - vehicleInDropOffB = DOES_ENTITY_EXIST(vehicleInDropOffB) = FALSE")
					ENDIF
					#ENDIF
					
					IF DOES_ENTITY_EXIST(vehicleInDropOffB)
						IF (vehicleInDropOffA = vehicleInDropOffB)
							PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - [CLRDROP] - vehicleInDropOffA = vehicleInDropOffB. Calling CLEAR_AREA_LEAVE_VEHICLE_HEALTH(GET_DROP_OFF_COORDS(), GET_DROP_RADIUS(), FALSE, FALSE, FALSE, TRUE)")
							CLEAR_AREA_LEAVE_VEHICLE_HEALTH(GET_DROP_OFF_COORDS(), GET_DROP_RADIUS(), FALSE, FALSE, FALSE, TRUE)
						ENDIF
					ENDIF
					
					PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - [CLRDROP] - done. bReset = TRUE")
					bReset = TRUE
					
				ELSE
		
					PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - [CLRDROP] - IS_VEHICLE_DRIVEABLE(vehicleInDropOffA) = TRUE, cannot be cleared. bReset = TRUE")
					bReset = TRUE
				
				ENDIF
				
			ELSE

				PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - [CLRDROP] -DOES_ENTITY_EXIST(vehicleInDropOffA) = FALSE, cannot be cleared. bReset = TRUE")
				bReset = TRUE
				
			ENDIF
			
		ENDIF
		
	ENDIF
	
	IF bReset
		PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] - [CLRDROP] - bReset = TRUE, doing reset.")
		VEHICLE_INDEX tempVeh
		vehicleInDropOffA = tempVeh
		vehicleInDropOffB = tempVeh
		RESET_NET_TIMER(dropOffBlockedTimerA)
		RESET_NET_TIMER(dropOffBlockedTimerB)
	ENDIF
	
ENDPROC
*/

TIME_DATATYPE timeLeaveEvent
PROC MAINTAIN_PREVENT_VEHICLE_IN_AREA()
	IF SHOULD_PREVENT_VEHICLE_IN_AREA()
		SET_VEHICLE_ENGINE_ON(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), FALSE, FALSE)
		IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), 20.0, DEFAULT, DEFAULT, DEFAULT, FALSE, TRUE)
			IF EMPTY_AND_LOCK_VEHICLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), timeLeaveEvent, FALSE, FALSE)
				PRINTLN("[GB_CASINO] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_PREVENT_VEHICLE_IN_AREA - Slow vehicle for local player and kicked them off.")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

#ENDIF
