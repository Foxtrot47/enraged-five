//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        AM_MP_CASINO_VALET_GARAGE_VAL_GARAGE_VALET_GARAGE.sc										//
// Description: Script for managing the interior of the Casino. access, spawning etc. 						//
//				is managed by AM_MP_SMPL_INTERIOR_* script while this script is launched by simple interior	//
//				script to handle anything specific to the Casino.											//
// Written by:  Online Technical Team: Mark Richardson,														//
// Date:  		06/02/2019																					//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_path.sch"
USING "commands_event.sch"
USING "net_include.sch"
USING "net_events.sch"
USING "net_spawn.sch"
USING "net_ambience.sch"
USING "net_wait_zero.sch"
USING "net_simple_interior.sch"

#IF FEATURE_CASINO
USING "net_simple_interior.sch"
USING "net_realty_casino.sch"
USING "net_simple_interior_casino_valet_garage.sch"

#IF IS_DEBUG_BUILD
USING "net_debug_log.sch"
#ENDIF

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ VARIABLES ╞═════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

//////////////////////////////////////////////////////////////
// LOCAL SCRIPT BIT SETS

//m_InteriorData.ibs
CONST_INT BS_CASINO_VAL_GARAGE_INTERIOR_REFRESH_ON_INIT				0
CONST_INT BS_CASINO_VAL_GARAGE_LOAD_SCENE_STARTED					1
CONST_INT BS_CASINO_VAL_GARAGE_IN_CORONA							2
CONST_INT BS_CASINO_VAL_GARAGE_CALLED_CLEAR_HELP					3
CONST_INT BS_CASINO_VAL_GARAGE_EXITING_IN_VEH						4
CONST_INT BS_CASINO_VAL_GARAGE_WARP_AFTER_ELEVATOR_SCENE			6
CONST_INT BS_CASINO_VAL_GARAGE_PLAY_ELEVATOR_OPENING_SOUND			7
CONST_INT BS_CASINO_VAL_GARAGE_PLAY_ELEVATOR_CLOSING_SOUND			8
CONST_INT BS_CASINO_VAL_GARAGE_PLAY_ELEVATOR_OPENED_SOUND			9
CONST_INT BS_CASINO_VAL_GARAGE_PLAY_ELEVATOR_CLOSED_SOUND			10
CONST_INT BS_CASINO_VAL_GARAGE_PLAY_ELEVATOR_BUTTON_SOUND			11
CONST_INT BS_CASINO_VAL_GARAGE_ELEVATOR_CUTSCENE_FRONT				12
CONST_INT BS_CASINO_VAL_GARAGE_PRINT_EXIT_W_CAR_HELP				13
CONST_INT BS_CASINO_VAL_GARAGE_TRIGGER_VEH_WARP_IN					14
CONST_INT BS_CASINO_VAL_GARAGE_WAS_IN_VEH_ON_ENTRY					15

//////////////////////////////////////////////////////////////
// BROADCAST DATA BIT SETS

// m_PlayerBD.iBS - Use with SET_LOCAL_PLAYER_BROADCAST_BIT
CONST_INT BS_CASINO_VAL_GARAGE_PLAYER_BD_READY_TO_WARP_OUT				0
CONST_INT BS_CASINO_VAL_GARAGE_PLAYER_BD_ELEVATOR_CUTSCENE_COMPLETE		1

CONST_INT BS_CASINO_VAL_GARAGE_SERVER_BD_CREATE_VEHICLES				0
CONST_INT BS_CASINO_VAL_GARAGE_SERVER_BD_VEH_LIST						1

/// End bit set const INTs
//////////////////////////////////////////////////////////////

// iServerBS
//CONST_INT SERVER_BS_												0

VECTOR vSafeCoords
BOOL bWalkingOutOfWay

SCRIPT_TIMER getOutOfwayTimer

CONST_INT WALK_OUT_SAFETY_TIME 10000

ENUM CASINO_VAL_GARAGE_SERVER_STATE
	CASINO_VAL_GARAGE_SERVER_STATE_LOADING,
	CASINO_VAL_GARAGE_SERVER_STATE_IDLE
ENDENUM

ENUM CASINO_VAL_GARAGE_CLIENT_STATE
	CASINO_VAL_GARAGE_STATE_LOADING,
	CASINO_VAL_GARAGE_STATE_IDLE
ENDENUM

#IF FEATURE_HEIST_ISLAND
CONST_INT MAX_VEHICLE_VARIATIONS 16
#ENDIF

#IF NOT FEATURE_HEIST_ISLAND
CONST_INT MAX_VEHICLE_VARIATIONS 16
#ENDIF

STRUCT ServerBroadcastData
	CASINO_VAL_GARAGE_SERVER_STATE eState = CASINO_VAL_GARAGE_SERVER_STATE_LOADING	
	INT iServerBS
	NETWORK_INDEX ambientVeh[7]
	INT iVehicleArray[MAX_VEHICLE_VARIATIONS]
ENDSTRUCT

CONST_INT PLAYER_BD_BS_READY_TO_WARP_OUT_IN_VEH		0
CONST_INT PLAYER_BD_BS_READY_TO_WARP_VEH_IN			1
CONST_INT PLAYER_BD_BS_WARPED_IN_VEH				2

STRUCT PlayerBroadcastData
	INT iBS
ENDSTRUCT

ENUM ELEVATOR_STATE
	ELEVATOR_STATE_IDLE,
	ELEVATOR_STATE_INIT,
	ELEVATOR_STATE_LOAD_ANIMS,
	ELEVATOR_STATE_RUN,
	ELEVATOR_STATE_RUN_ELEVATOR,
	ELEVATOR_STATE_FINISHED
ENDENUM

STRUCT CASINO_VAL_GARAGE_DATA
	//Bit sets
	INT iBS
		
	//Script state
	INT iScriptInstance
	CASINO_VAL_GARAGE_CLIENT_STATE eState
	
	//Interior data
	SIMPLE_INTERIORS eSimpleInteriorID = SIMPLE_INTERIOR_CASINO
	SIMPLE_INTERIOR_DETAILS	SimpleInteriorDetails
	INTERIOR_INSTANCE_INDEX iInteriorID
	VECTOR vInteriorPosition
	FLOAT fInteriorHeading
	TEXT_LABEL_63 sInteriorType
	BOOL bScriptRelaunched
	
	//Loading data
	SCRIPT_TIMER tPinInMemTimer
	INT iPinInMemTimeToExpire = 10000
	
	CAMERA_INDEX camElevatorCutscene
	ELEVATOR_STATE eElevatorState = ELEVATOR_STATE_IDLE
	INT iNonNetSceneID = -1
	PED_INDEX pedClone
	OBJECT_INDEX objElevatorDoor[4]
	
	BLIP_INDEX blipElevator
	BLIP_INDEX blipElevator2
ENDSTRUCT

CASINO_VAL_GARAGE_DATA m_InteriorData
ServerBroadcastData m_ServerBD
PlayerBroadcastData m_PlayerBD[NUM_NETWORK_PLAYERS]

SCRIPT_TIMER sExitDelayTimer
SCRIPT_TIMER sPhoneOnScreenTimer

//VEHICLE_INDEX lastVeh
VEHICLE_INDEX currentVeh



//#IF IS_DEBUG_BUILD
//STRUCT CASINO_VAL_GARAGE_DEBUG_DATA
//	BOOL bResetCutscenes = FALSE
//ENDSTRUCT
//CASINO_VAL_GARAGE_DEBUG_DATA debugData
//#ENDIF

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ FUCTIONS ╞══════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

#IF IS_DEBUG_BUILD
FUNC STRING DEBUG_GET_CASINO_VAL_GARAGE_STATE_NAME(CASINO_VAL_GARAGE_CLIENT_STATE eState)
	SWITCH eState 
		CASE CASINO_VAL_GARAGE_STATE_LOADING	RETURN "CASINO_VAL_GARAGE_STATE_LOADING"
		CASE CASINO_VAL_GARAGE_STATE_IDLE		RETURN "CASINO_VAL_GARAGE_STATE_IDLE"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC STRING DEBUG_GET_CASINO_VAL_GARAGE_SERVER_STATE_NAME(CASINO_VAL_GARAGE_SERVER_STATE eState)
	SWITCH eState
		CASE CASINO_VAL_GARAGE_SERVER_STATE_LOADING	RETURN "CASINO_VAL_GARAGE_SERVER_STATE_LOADING"
		CASE CASINO_VAL_GARAGE_SERVER_STATE_IDLE	RETURN "CASINO_VAL_GARAGE_SERVER_STATE_IDLE"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC STRING DEBUG_GET_PLAYER_BROADCAST_BIT_NAME(INT iBit)
	SWITCH iBit
		CASE 0				RETURN "UNUSED"
	ENDSWITCH
	
	RETURN "NULL"
ENDFUNC

//PROC CREATE_DEBUG_WIDGETS()
//	START_WIDGET_GROUP("AM_MP_CASINO_VALET_GARAGE")
//
//	STOP_WIDGET_GROUP()
//ENDPROC

//PROC UPDATE_DEBUG_WIDGETS()
//
//ENDPROC
#ENDIF

FUNC BOOL IS_CASINO_VAL_GARAGE_STATE(CASINO_VAL_GARAGE_CLIENT_STATE eState)
	RETURN m_InteriorData.eState = eState
ENDFUNC

FUNC BOOL IS_CASINO_VAL_GARAGE_SERVER_STATE(CASINO_VAL_GARAGE_SERVER_STATE eState)
	RETURN m_ServerBD.eState = eState
ENDFUNC

PROC SET_CASINO_VAL_GARAGE_STATE(CASINO_VAL_GARAGE_CLIENT_STATE eState)
	#IF IS_DEBUG_BUILD
	CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_CASINO_VALET_GARAGE - SET_CASINO_VAL_GARAGE_STATE - New state: ", DEBUG_GET_CASINO_VAL_GARAGE_STATE_NAME(eState))
	#ENDIF
	m_InteriorData.eState = eState
ENDPROC

PROC SET_CASINO_VAL_GARAGE_SERVER_STATE(CASINO_VAL_GARAGE_SERVER_STATE eState)
	#IF IS_DEBUG_BUILD
	CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_CASINO_VALET_GARAGE - SET_CASINO_VAL_GARAGE_SERVER_STATE - New state: ", DEBUG_GET_CASINO_VAL_GARAGE_SERVER_STATE_NAME(eState))
	#ENDIF
	m_ServerBD.eState = eState
ENDPROC

PROC SET_LOCAL_PLAYER_BROADCAST_BIT(INT iBit, BOOL bSet)
	IF bSet
		IF NOT IS_BIT_SET(m_PlayerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, iBit)
			#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_CASINO_VALET_GARAGE - SET_LOCAL_PLAYER_BROADCAST_BIT - Setting bit ", DEBUG_GET_PLAYER_BROADCAST_BIT_NAME(iBit))
			#ENDIF
			SET_BIT(m_PlayerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, iBit)
		ENDIF
	ELSE
		IF IS_BIT_SET(m_PlayerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, iBit)	
			#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_CASINO_VALET_GARAGE - SET_LOCAL_PLAYER_BROADCAST_BIT - Clearing bit ", DEBUG_GET_PLAYER_BROADCAST_BIT_NAME(iBit))
			#ENDIF	
			CLEAR_BIT(m_PlayerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, iBit)
		ENDIF
	ENDIF
ENDPROC

PROC CLEANUP_AUDIO_EMITTERS()
ENDPROC

PROC CLEANUP_CASINO_VAL_GARAGE_BLIPS()	
ENDPROC

PROC CLEANUP_ELEVATOR_CUTSCENE(BOOL bClearFlag = FALSE)
	IF m_InteriorData.iNonNetSceneID != -1
		IF IS_SYNCHRONIZED_SCENE_RUNNING(m_InteriorData.iNonNetSceneID)
			IF GET_SYNCHRONIZED_SCENE_PHASE(m_InteriorData.iNonNetSceneID) < 1.0
				SET_SYNCHRONIZED_SCENE_PHASE(m_InteriorData.iNonNetSceneID, 1.0)
			ENDIF
		ENDIF
		
		m_InteriorData.iNonNetSceneID = -1
	ENDIF
	
	IF DOES_ENTITY_EXIST(m_InteriorData.pedClone)
		DELETE_PED(m_InteriorData.pedClone)
	ENDIF
	
	IF DOES_CAM_EXIST(m_InteriorData.camElevatorCutscene)
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		
		DESTROY_CAM(m_InteriorData.camElevatorCutscene)
	ENDIF
	
	IF NETWORK_IS_IN_MP_CUTSCENE()
	AND IS_BIT_SET(g_SimpleInteriorData.iForthBS, BS4_SIMPLE_INTERIOR_USED_ELEVATOR)
		CLEANUP_MP_CUTSCENE(FALSE, FALSE)
	ENDIF
	
	CLEAR_BIT(m_InteriorData.iBS, BS_CASINO_VAL_GARAGE_PLAY_ELEVATOR_OPENING_SOUND)
	CLEAR_BIT(m_InteriorData.iBS, BS_CASINO_VAL_GARAGE_PLAY_ELEVATOR_CLOSING_SOUND)
	CLEAR_BIT(m_InteriorData.iBS, BS_CASINO_VAL_GARAGE_PLAY_ELEVATOR_OPENED_SOUND)
	CLEAR_BIT(m_InteriorData.iBS, BS_CASINO_VAL_GARAGE_PLAY_ELEVATOR_CLOSED_SOUND)
	CLEAR_BIT(m_InteriorData.iBS, BS_CASINO_VAL_GARAGE_PLAY_ELEVATOR_BUTTON_SOUND)
	CLEAR_BIT(m_InteriorData.iBS, BS_CASINO_VAL_GARAGE_ELEVATOR_CUTSCENE_FRONT)
	
	IF bClearFlag
		CLEAR_BIT(g_SimpleInteriorData.iForthBS, BS4_SIMPLE_INTERIOR_USED_ELEVATOR)
		CLEAR_BIT(g_simpleInteriorData.iSixthBS, BS6_SIMPLE_INTERIOR_EXIT_FROM_ELEVATOR)
		CLEAR_BIT(g_SimpleInteriorData.iSixthBS, BS6_SIMPLE_INTERIOR_SWITCH_FLOOR_FROM_ELEVATOR_TO_SAME_INTERIOR)
	ENDIF
ENDPROC

PROC CLEANUP_ELEVATOR_DOORS()
	IF DOES_ENTITY_EXIST(m_InteriorData.objElevatorDoor[0])
		DELETE_OBJECT(m_InteriorData.objElevatorDoor[0])
	ENDIF
	
	IF DOES_ENTITY_EXIST(m_InteriorData.objElevatorDoor[1])
		DELETE_OBJECT(m_InteriorData.objElevatorDoor[1])
	ENDIF
	
	IF DOES_ENTITY_EXIST(m_InteriorData.objElevatorDoor[2])
		DELETE_OBJECT(m_InteriorData.objElevatorDoor[2])
	ENDIF
	
	IF DOES_ENTITY_EXIST(m_InteriorData.objElevatorDoor[3])
		DELETE_OBJECT(m_InteriorData.objElevatorDoor[3])
	ENDIF
ENDPROC

PROC SCRIPT_CLEANUP()
	CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_CASINO_VALET_GARAGE - SCRIPT_CLEANUP")
	
	RESET_NET_TIMER(g_SimpleInteriorData.tIntAccessFallback)
		
	SET_RADAR_ZOOM_PRECISE(0)
	
	SIMPLE_INTERIOR_CLEAR_ENTERING_WITH_VEHICLE()
	SIMPLE_INTERIOR_CLEAR_ENTERING_WITH_VEH_AS_PASSENGER()
	
//	BOOL bReturnControl = TRUE
//	
//	IF IS_SKYSWOOP_MOVING()
//	OR IS_SKYSWOOP_IN_SKY()
//	OR IS_TRANSITION_ACTIVE()
//	OR IS_TRANSITION_RUNNING()
//	OR IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
//		bReturnControl = FALSE
//	ENDIF
	
//	CLEANUP_AUDIO_EMITTERS()
//	CLEANUP_CASINO_VAL_GARAGE_BLIPS()
	
	CLEANUP_ELEVATOR_CUTSCENE(TRUE)
	CLEANUP_ELEVATOR_DOORS()
	
	CLEAR_BIT(g_SimpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_WAITING_IN_ELEVATOR)
	
	IF IS_BIT_SET(m_InteriorData.iBS, BS_CASINO_VAL_GARAGE_PRINT_EXIT_W_CAR_HELP)
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CASINO_EXIT_0PC")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CASINO_EXIT_0")
			CLEAR_HELP()
		ENDIF
		CLEAR_BIT(m_InteriorData.iBS, BS_CASINO_VAL_GARAGE_PRINT_EXIT_W_CAR_HELP)
	ENDIF
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_NotAllowedToJackAnyPlayers, FALSE)
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableStartEngine, FALSE)
		SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
		SET_ENTITY_CAN_BE_DAMAGED(PLAYER_PED_ID(), TRUE)
		SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(PLAYER_PED_ID(), TRUE)
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID() ,PCF_DontActivateRagdollFromVehicleImpact,FALSE)
	ENDIF
	
	SAFE_REMOVE_BLIP(m_InteriorData.blipElevator)
	SAFE_REMOVE_BLIP(m_InteriorData.blipElevator2)
	
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

PROC HIDE_ELEVATOR_DOORS()
	CREATE_MODEL_HIDE_EXCLUDING_SCRIPT_OBJECTS(<<1380.8552, 179.6634, -49.9991>>, 2.0, INT_TO_ENUM(MODEL_NAMES, HASH("v_ilev_garageliftdoor")), TRUE)
	CREATE_MODEL_HIDE_EXCLUDING_SCRIPT_OBJECTS(<<1379.3513, 179.6640, -49.9991>>, 2.0, INT_TO_ENUM(MODEL_NAMES, HASH("v_ilev_garageliftdoor")), TRUE)
	CREATE_MODEL_HIDE_EXCLUDING_SCRIPT_OBJECTS(<<1379.2699, 258.2452, -49.9988>>, 2.0, INT_TO_ENUM(MODEL_NAMES, HASH("v_ilev_garageliftdoor")), TRUE)
	CREATE_MODEL_HIDE_EXCLUDING_SCRIPT_OBJECTS(<<1380.7738, 258.2452, -49.9988>>, 2.0, INT_TO_ENUM(MODEL_NAMES, HASH("v_ilev_garageliftdoor")), TRUE)
ENDPROC

PROC INITIALISE()
	IF m_InteriorData.eSimpleInteriorID != SIMPLE_INTERIOR_INVALID
		g_bPlayerLeavingCurrentInteriorInVeh = FALSE
		
		GET_SIMPLE_INTERIOR_DETAILS(m_InteriorData.eSimpleInteriorID, m_InteriorData.SimpleInteriorDetails, FALSE)
		
		IF g_SimpleInteriorData.iExitMenuOption > -1
			g_SimpleInteriorData.iExitMenuOption = -1
		ENDIF
		
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_NotAllowedToJackAnyPlayers, TRUE)
			SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
			SET_ENTITY_CAN_BE_DAMAGED(PLAYER_PED_ID(), FALSE)
			SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(PLAYER_PED_ID(), FALSE)
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DontActivateRagdollFromVehicleImpact,TRUE)
		ENDIF
		
//		#IF IS_DEBUG_BUILD
//		CREATE_DEBUG_WIDGETS()
//		CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_CASINO_VALET_GARAGE - INITIALISE - Done.")
//		#ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_CASINO_VALET_GARAGE - Unable to initialise, invalid simple interior index.")
		#ENDIF
	ENDIF
ENDPROC

PROC SCRIPT_INITIALISE(SIMPLE_INTERIOR_CHILD_SCRIPT_DATA &scriptData)
	
	#IF IS_DEBUG_BUILD
	CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_CASINO_VALET_GARAGE - SCRIPT_INITIALISE - Launching instance ", m_InteriorData.iScriptInstance)
	#ENDIF

	m_InteriorData.eSimpleInteriorID 	= scriptData.eSimpleInteriorID
	m_InteriorData.iScriptInstance 		= scriptData.iScriptInstance
	
	NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE, m_InteriorData.iScriptInstance)
	
	// Ensures net script is active, waits until it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(m_ServerBD, SIZE_OF(m_ServerBD))
	
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(m_PlayerBD, SIZE_OF(m_PlayerBD))
	
	IF NOT Wait_For_First_Network_Broadcast() 
		#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_CASINO_VALET_GARAGE - SCRIPT_INITIALISE - Failed to receive initial network broadcast.")
		#ENDIF
		SCRIPT_CLEANUP()
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_CASINO_VALET_GARAGE - SCRIPT_INITIALISE - Initialised.")
		#ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_CASINO_VALET_GARAGE - SCRIPT_INITIALISE - NETWORK_IS_GAME_IN_PROGRESS = FALSE")
		#ENDIF
		SCRIPT_CLEANUP()
	ENDIF
	
	SET_BIT(m_PlayerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_CASINO_VAL_GARAGE_PLAYER_BD_ELEVATOR_CUTSCENE_COMPLETE)
	
	HIDE_ELEVATOR_DOORS()
	
	g_bLaunchedMissionFrmSMPLIntLaptop = FALSE
	INITIALISE()
ENDPROC

FUNC BOOL CREATE_ELEVATOR_DOORS()
	IF NOT IS_VALID_INTERIOR(m_InteriorData.iInteriorID)
	OR NOT IS_INTERIOR_READY(m_InteriorData.iInteriorID)
		PRINTLN("AM_MP_CASINO_VALET_GARAGE - CREATE_ELEVATOR_DOORS - Interior not ready")
		
		RETURN FALSE
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(m_InteriorData.objElevatorDoor[0])
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("v_ilev_garageliftdoor")))
		
		IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("v_ilev_garageliftdoor")))
			m_InteriorData.objElevatorDoor[0] = CREATE_OBJECT(INT_TO_ENUM(MODEL_NAMES, HASH("v_ilev_garageliftdoor")), <<1380.8552, 179.6634, -49.9991>>, FALSE, FALSE, TRUE)
			
			IF DOES_ENTITY_EXIST(m_InteriorData.objElevatorDoor[0])
				SET_ENTITY_COORDS(m_InteriorData.objElevatorDoor[0], <<1380.8552, 179.6634, -49.9991>>)
				SET_ENTITY_HEADING(m_InteriorData.objElevatorDoor[0], 180.0)
				
				FREEZE_ENTITY_POSITION(m_InteriorData.objElevatorDoor[0], TRUE)
				
				SET_ENTITY_LOD_DIST(m_InteriorData.objElevatorDoor[0], 300)
				
				FORCE_ROOM_FOR_ENTITY(m_InteriorData.objElevatorDoor[0], m_InteriorData.iInteriorID, CASINO_VAL_GARAGE_ROOM_KEY)
				
				SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, HASH("v_ilev_garageliftdoor")))
			ENDIF
		ELSE
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(m_InteriorData.objElevatorDoor[1])
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("v_ilev_garageliftdoor")))
		
		IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("v_ilev_garageliftdoor")))
			m_InteriorData.objElevatorDoor[1] = CREATE_OBJECT(INT_TO_ENUM(MODEL_NAMES, HASH("v_ilev_garageliftdoor")), <<1379.3513, 179.6640, -49.9991>>, FALSE, FALSE, TRUE)
			
			IF DOES_ENTITY_EXIST(m_InteriorData.objElevatorDoor[1])
				SET_ENTITY_COORDS(m_InteriorData.objElevatorDoor[1], <<1379.3513, 179.6640, -49.9991>>)
				SET_ENTITY_HEADING(m_InteriorData.objElevatorDoor[1], 0.0)
				
				FREEZE_ENTITY_POSITION(m_InteriorData.objElevatorDoor[1], TRUE)
				
				SET_ENTITY_LOD_DIST(m_InteriorData.objElevatorDoor[1], 300)
				
				FORCE_ROOM_FOR_ENTITY(m_InteriorData.objElevatorDoor[1], m_InteriorData.iInteriorID, CASINO_VAL_GARAGE_ROOM_KEY)
				
				SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, HASH("v_ilev_garageliftdoor")))
			ENDIF
		ELSE
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(m_InteriorData.objElevatorDoor[2])
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("v_ilev_garageliftdoor")))
		
		IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("v_ilev_garageliftdoor")))
			m_InteriorData.objElevatorDoor[2] = CREATE_OBJECT(INT_TO_ENUM(MODEL_NAMES, HASH("v_ilev_garageliftdoor")), <<1379.2699, 258.2452, -49.9988>>, FALSE, FALSE, TRUE)
			
			IF DOES_ENTITY_EXIST(m_InteriorData.objElevatorDoor[2])
				SET_ENTITY_COORDS(m_InteriorData.objElevatorDoor[2], <<1379.2699, 258.2452, -49.9988>>)
				SET_ENTITY_HEADING(m_InteriorData.objElevatorDoor[2], 0.0)
				
				FREEZE_ENTITY_POSITION(m_InteriorData.objElevatorDoor[2], TRUE)
				
				SET_ENTITY_LOD_DIST(m_InteriorData.objElevatorDoor[2], 300)
				
				FORCE_ROOM_FOR_ENTITY(m_InteriorData.objElevatorDoor[2], m_InteriorData.iInteriorID, CASINO_VAL_GARAGE_ROOM_KEY)
				
				SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, HASH("v_ilev_garageliftdoor")))
			ENDIF
		ELSE
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(m_InteriorData.objElevatorDoor[3])
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("v_ilev_garageliftdoor")))
		
		IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("v_ilev_garageliftdoor")))
			m_InteriorData.objElevatorDoor[3] = CREATE_OBJECT(INT_TO_ENUM(MODEL_NAMES, HASH("v_ilev_garageliftdoor")), <<1380.7738, 258.2452, -49.9988>>, FALSE, FALSE, TRUE)
			
			IF DOES_ENTITY_EXIST(m_InteriorData.objElevatorDoor[3])
				SET_ENTITY_COORDS(m_InteriorData.objElevatorDoor[3], <<1380.7738, 258.2452, -49.9988>>)
				SET_ENTITY_HEADING(m_InteriorData.objElevatorDoor[3], 180.0)
				
				FREEZE_ENTITY_POSITION(m_InteriorData.objElevatorDoor[3], TRUE)
				
				SET_ENTITY_LOD_DIST(m_InteriorData.objElevatorDoor[3], 300)
				
				FORCE_ROOM_FOR_ENTITY(m_InteriorData.objElevatorDoor[3], m_InteriorData.iInteriorID, CASINO_VAL_GARAGE_ROOM_KEY)
				
				SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, HASH("v_ilev_garageliftdoor")))
			ENDIF
		ELSE
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL ARE_ALL_PLAYERS_IN_VEHICLE_READY_TO_WARP()
	PED_INDEX PedID
	PLAYER_INDEX PlayerID
		
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		PlayerID = INT_TO_NATIVE(PLAYER_INDEX, i)
		
		IF PlayerID != INVALID_PLAYER_INDEX()
		AND IS_NET_PLAYER_OK(PlayerID)
		AND PlayerID != PLAYER_ID()
			PedID = GET_PLAYER_PED(PlayerID)
			
			IF DOES_ENTITY_EXIST(PedID)
			AND IS_ENTITY_ALIVE(PedID)
				IF IS_PED_IN_ANY_VEHICLE(PedID)
				AND GET_VEHICLE_PED_IS_IN(PedID) = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					IF NOT IS_BIT_SET(m_PlayerBD[NATIVE_TO_INT(PlayerID)].iBS, PLAYER_BD_BS_READY_TO_WARP_VEH_IN)
						PRINTLN("ARE_ALL_PLAYERS_IN_VEHICLE_READY_TO_WARP: waitying for player ", GET_PLAYER_NAME(PlayerID))
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN TRUE
ENDFUNC

//PROC FORCE_CAR_VISIBLE_THIS_FRAME()
//	(m_InteriorData.iBS,BS_CASINO_VAL_GARAGE_TRIGGER_VEH_WARP_IN)
//	VEHICLE_INDEX vehIndex
//	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//		vehIndex = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
//		IF DOES_ENTITY_EXIST(vehIndex)
//			SET_ENTITY_LOCALLY_VISIBLE(vehIndex)
//			PRINTLN("FORCE_CAR_VISIBLE_THIS_FRAME: setting current vehicle visible locally")
//		ENDIF
//	ENDIF
//ENDPROC

FUNC BOOL HAS_PLAYER_WARPED_WITH_OWNERS_PV_INTO_GARAGE()
	VEHICLE_INDEX theVeh
	PLAYER_INDEX vehOwnerID
	BOOL bAbortiveEntry
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())	
		SET_BIT(m_InteriorData.iBS,BS_CASINO_VAL_GARAGE_WAS_IN_VEH_ON_ENTRY)
		theVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		SET_BIT(m_PlayerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, PLAYER_BD_BS_READY_TO_WARP_VEH_IN)
		vehOwnerID = GET_OWNER_OF_PERSONAL_VEHICLE(theVeh)
		IF vehOwnerID != INVALID_PLAYER_INDEX()
		AND IS_NET_PLAYER_OK(vehOwnerID)
			IF NOT IS_BIT_SET(m_PlayerBD[NATIVE_TO_INT(vehOwnerID)].iBS, PLAYER_BD_BS_WARPED_IN_VEH)
				IF DOES_ENTITY_EXIST(MPGlobalsAmbience.vehPersonalVehicle)	
				AND MPGlobalsAmbience.vehPersonalVehicle = theVeh
					IF NOT IS_BIT_SET(m_InteriorData.iBS,BS_CASINO_VAL_GARAGE_TRIGGER_VEH_WARP_IN)
						IF ARE_ALL_PLAYERS_IN_VEHICLE_READY_TO_WARP()
							INIT_WARP_INTO_CASINO_VALET_CAR_PARK(FALSE)
							PRINTLN("AM_MP_CASINO_VALET_GARAGE - HAS_PLAYER_WARPED_WITH_OWNERS_PV_INTO_GARAGE- INIT_WARP_INTO_CASINO_VALET_CAR_PARK set on local player")
							SET_BIT(m_InteriorData.iBS,BS_CASINO_VAL_GARAGE_TRIGGER_VEH_WARP_IN)
						ENDIF
					ELSE	
						IF IS_BIT_SET(MPGlobals.VehicleData.iBSWarpIntoCasinoCarPark,CASINO_CAR_PARK_BS_VEH_IN)
							PRINTLN("AM_MP_CASINO_VALET_GARAGE - HAS_PLAYER_WARPED_WITH_OWNERS_PV_INTO_GARAGE- clearing SET_PV_WARPING_INTO_SHARED_SPACE player's veh is inside.")
							// Holster weapon if weapons are disabled
							SET_BIT(m_PlayerBD[NATIVE_TO_INT(vehOwnerID)].iBS, PLAYER_BD_BS_WARPED_IN_VEH)
						ELSE
							IF HAS_PLAYER_STARTED_WARP_INTO_CASINO_VALET_CAR_PARK()
								IF NOT IS_BIT_SET(MPGlobals.VehicleData.iBSWarpIntoCasinoCarPark,CASINO_CAR_PARK_BS_VEH_IN)
									PRINTLN("AM_MP_CASINO_VALET_GARAGE - HAS_PLAYER_WARPED_WITH_OWNERS_PV_INTO_GARAGE - not ready waiting for PV warping into shared space")
								ENDIF
							ELSE
								PRINTLN("AM_MP_CASINO_VALET_GARAGE - HAS_PLAYER_WARPED_WITH_OWNERS_PV_INTO_GARAGE - clearing SET_PV_WARPING_INTO_SHARED_SPACE player no longer requesting")
								SET_PV_WARPING_INTO_SHARED_SPACE(FALSE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF

				PRINTLN("AM_MP_CASINO_VALET_GARAGE - HAS_PLAYER_WARPED_WITH_OWNERS_PV_INTO_GARAGE - waiting for DRIVER_WARPED_INSIDE_VALET_GARAGE.")
				RETURN FALSE
			ELSE
				PRINTLN("AM_MP_CASINO_VALET_GARAGE - HAS_PLAYER_WARPED_WITH_OWNERS_PV_INTO_GARAGE - driver has moved vehicle inside- proceeding")
				CLEAR_BIT(m_InteriorData.iBS,BS_CASINO_VAL_GARAGE_WAS_IN_VEH_ON_ENTRY)
			ENDIF
		ELSE
			PRINTLN("AM_MP_CASINO_VALET_GARAGE - HAS_PLAYER_WARPED_WITH_OWNERS_PV_INTO_GARAGE - vehicle owner no longer valid - proceeding")
			bAbortiveEntry = TRUE
		ENDIF
	ELSE	
		PRINTLN("AM_MP_CASINO_VALET_GARAGE - HAS_PLAYER_WARPED_WITH_OWNERS_PV_INTO_GARAGE - not in a vehicle - proceeding")
		bAbortiveEntry = TRUE
	ENDIF
	IF bAbortiveEntry
	AND IS_BIT_SET(m_InteriorData.iBS,BS_CASINO_VAL_GARAGE_WAS_IN_VEH_ON_ENTRY)
		SET_ENTITY_COORDS(PLAYER_PED_ID(),<<1335.1205, 183.6249, -48.8762>>)
		SET_ENTITY_HEADING(PLAYER_PED_ID(),271.4400)
		CLEAR_BIT(m_InteriorData.iBS,BS_CASINO_VAL_GARAGE_WAS_IN_VEH_ON_ENTRY)
		PRINTLN("AM_MP_CASINO_VALET_GARAGE - HAS_PLAYER_WARPED_WITH_OWNERS_PV_INTO_GARAGE - abortive entry placing by door")
	ENDIF
	RETURN TRUE
ENDFUNC


PROC GET_AMBIENT_VEHICLE_DETAILS(INT iVeh,MODEL_NAMES &ModelName)
	SWITCH iVeh
		CASE 0
			ModelName = Windsor2
		BREAK
		CASE 1
			ModelName = GT500
		BREAK
		CASE 2
			ModelName = Tempesta
		BREAK
		CASE 3
			ModelName = Schafter3
		BREAK
		CASE 4
			ModelName = Baller3
		BREAK
		CASE 5
			ModelName = Carbonizzare
		BREAK
		CASE 6
			ModelName = Ellie
		BREAK
		CASE 7
			ModelName = Issi3
		BREAK
		CASE 8
			ModelName = Felon
		BREAK
		CASE 9
			ModelName = Oracle
		BREAK
		CASE 10
			ModelName = Pariah
		BREAK
		CASE 11
			ModelName = Raiden
		BREAK
		CASE 12
			ModelName = Mamba
		BREAK
		CASE 13
			ModelName = Casco
		BREAK
		CASE 14
			ModelName = Superd
		BREAK
		#IF FEATURE_HEIST_ISLAND
		CASE 15
			ModelName = GAUNTLET4		
		BREAK
		#ENDIF
	ENDSWITCH
	IF iVeh = 99
		ModelName = FURIA
	ENDIF
ENDPROC

FUNC VECTOR GET_AMBIENT_VEHICLE_POS(INT iPos)
	SWITCH iPos
		CASE 0 RETURN <<1379.4100, 246.1934, -50.0944>> BREAK
		CASE 1 RETURN <<1379.5962, 233.7324, -50.0945>> BREAK
		CASE 2 RETURN <<1379.9943, 229.5488, -50.0945>> BREAK
		CASE 3 RETURN <<1380.0768, 216.9163, -50.0945>> BREAK
		CASE 4 RETURN <<1379.6085, 212.6747, -50.0945>> BREAK
		CASE 5 RETURN <<1365.9913, 200.3108, -50.0945>> BREAK
		CASE 6 RETURN <<1393.9297, 200.3178, -50.0945>> BREAK
	ENDSWITCH
	SCRIPT_ASSERT("GET_AMBIENT_VEHICLE_POS: Invalid position passed into function. See Conor")
	PRINTLN("GET_AMBIENT_VEHICLE_POS: Invalid position passed into function. iPos = ",iPos)
	RETURN <<0,0,0>>
ENDFUNC

FUNC FLOAT GET_AMBIENT_VEHICLE_HEADING(INT iPos)
	SWITCH iPos
		CASE 0	RETURN 90.1988  BREAK
		CASE 1	RETURN 88.7987  BREAK
		CASE 2	RETURN 267.7984 BREAK
		CASE 3	RETURN 274.9984 BREAK
		CASE 4	RETURN 88.5980  BREAK
		CASE 5	RETURN 91.5980  BREAK
		CASE 6	RETURN 91.5980  BREAK
	ENDSWITCH
	RETURN 0.0
ENDFUNC

PROC MAINTAIN_AMBIENT_VEHICLES()
	BOOL bWaitingForCreation
	INT i, x
	INT iTemp
	MODEL_NAMES theModel
	VEHICLE_SETUP_STRUCT_MP setupVeh
	VEHICLE_INDEX tempVehicle
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF NOT IS_BIT_SET(m_ServerBD.iServerBS,BS_CASINO_VAL_GARAGE_SERVER_BD_VEH_LIST)
			
			REPEAT MAX_VEHICLE_VARIATIONS i
				m_ServerBD.iVehicleArray[i] = i
			ENDREPEAT
			REPEAT MAX_VEHICLE_VARIATIONS i
				x = GET_RANDOM_INT_IN_RANGE(0,MAX_VEHICLE_VARIATIONS)
				iTemp = m_ServerBD.iVehicleArray[i]
				m_ServerBD.iVehicleArray[i] = m_ServerBD.iVehicleArray[x]
				m_ServerBD.iVehicleArray[x] = iTemp
			ENDREPEAT
			
			IF g_sMPTunables.bAllowCelebValetVeh
			#IF IS_DEBUG_BUILD
			OR GET_COMMANDLINE_PARAM_EXISTS("sc_ForceCelebVehicle")
			#ENDIF
				m_ServerBD.iVehicleArray[5] = 99
			ENDIF
			
			#IF FEATURE_HEIST_ISLAND
			IF g_sMPTunables.bAllowMoodyValetVeh
			#IF IS_DEBUG_BUILD
			OR GET_COMMANDLINE_PARAM_EXISTS("sc_ForceMoodyVehicle")
			#ENDIF
				#IF IS_DEBUG_BUILD
				IF GET_COMMANDLINE_PARAM_EXISTS("sc_ForceMoodyVehicle")
					BOOL bMoodyVehicleActive
					REPEAT 7 i
						IF m_ServerBD.iVehicleArray[i] = 15
							bMoodyVehicleActive = TRUE
						ENDIF
					ENDREPEAT
					IF NOT bMoodyVehicleActive
						m_ServerBD.iVehicleArray[5] = 15
					ENDIF
				ENDIF
				#ENDIF
				IF m_ServerBD.iVehicleArray[5] = 15
					m_ServerBD.iVehicleArray[5] = m_ServerBD.iVehicleArray[6]
					m_ServerBD.iVehicleArray[6] = 15
				ENDIF
			ENDIF
			#ENDIF
			
			#IF IS_DEBUG_BUILD
			PRINTLN("VALET_GAR: MAINTAIN_AMBIENT_VEHICLES: generated array of vehicles")
			REPEAT MAX_VEHICLE_VARIATIONS i
				PRINTLN("VALET_GAR: MAINTAIN_AMBIENT_VEHICLES: m_ServerBD.iVehicleArray[",i,"] = ",m_ServerBD.iVehicleArray[i])
			ENDREPEAT
			#ENDIF
			IF CAN_RESERVE_NETWORK_VEHICLES_FOR_THIS_SCRIPT(7, FALSE, TRUE)
				RESERVE_LOCAL_NETWORK_MISSION_VEHICLES(7)
				SET_BIT(m_ServerBD.iServerBS,BS_CASINO_VAL_GARAGE_SERVER_BD_VEH_LIST)
				PRINTLN("VALET_GAR: MAINTAIN_AMBIENT_VEHICLES: reserved vehicles")
			ENDIF
		ELSE
			IF NOT IS_BIT_SET(m_ServerBD.iServerBS,BS_CASINO_VAL_GARAGE_SERVER_BD_CREATE_VEHICLES)
				REPEAT 7 i
					IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(m_ServerBD.ambientVeh[i])
						bWaitingForCreation = TRUE
						GET_AMBIENT_VEHICLE_DETAILS(m_ServerBD.iVehicleArray[i],theModel)
						IF REQUEST_LOAD_MODEL(theModel)
							IF CAN_REGISTER_MISSION_VEHICLES(1)
								IF NETWORK_IS_IN_MP_CUTSCENE()
									SET_NETWORK_CUTSCENE_ENTITIES(TRUE)
								ENDIF
								
								PRINTLN("VALET_GAR: MAINTAIN_AMBIENT_VEHICLES: - Creating vehicle: ", i, " vehicle index m_ServerBD.iVehicleArray[i]")
								
								tempVehicle = CREATE_VEHICLE(theModel, GET_AMBIENT_VEHICLE_POS(i),GET_AMBIENT_VEHICLE_HEADING(i), TRUE, TRUE)
								
								m_ServerBD.ambientVeh[i] = VEH_TO_NET(tempVehicle)
								IF theModel = Mamba
									SET_VEHICLE_NUMBER_PLATE_TEXT(tempVehicle, "G4BRIEL")
								ELIF theModel = Tempesta
									SET_VEHICLE_NUMBER_PLATE_TEXT(tempVehicle, "USH1")
								ELIF theModel = Ellie
									SET_VEHICLE_NUMBER_PLATE_TEXT(tempVehicle, "C0WB0Y")
								#IF FEATURE_HEIST_ISLAND
								ELIF theModel = GAUNTLET4
									setupVeh.VehicleSetup.eModel = gauntlet4 // GAUNTLET4
									setupVeh.VehicleSetup.tlPlateText = "MOODYMAN"
									setupVeh.VehicleSetup.iColour1 = 111
									setupVeh.VehicleSetup.iColour2 = 111
									setupVeh.VehicleSetup.iColourExtra1 = 0
									setupVeh.VehicleSetup.iColourExtra2 = 122
									setupVeh.iColour5 = 1
									setupVeh.iColour6 = 132
									setupVeh.iLivery2 = 0
									setupVeh.VehicleSetup.iWheelType = 11
									setupVeh.VehicleSetup.iTyreR = 255
									setupVeh.VehicleSetup.iTyreG = 255
									setupVeh.VehicleSetup.iTyreB = 255
									setupVeh.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
									setupVeh.VehicleSetup.iModIndex[MOD_SKIRT] = 1
									setupVeh.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
									setupVeh.VehicleSetup.iModIndex[MOD_GRILL] = 3
									setupVeh.VehicleSetup.iModIndex[MOD_BONNET] = 4
									setupVeh.VehicleSetup.iModIndex[MOD_SUSPENSION] = 1
									setupVeh.VehicleSetup.iModIndex[MOD_WHEELS] = 15
									SET_VEHICLE_SETUP_MP(tempVehicle,setupVeh)
								#ENDIF
								ENDIF
								
								IF m_ServerBD.iVehicleArray[i] = 99
									setupVeh.VehicleSetup.tlPlateText = "ANCESTOR"
									setupVeh.VehicleSetup.eModel = FURIA
									setupVeh.VehicleSetup.iPlateIndex = 4
									setupVeh.VehicleSetup.iColour1 = 138
									setupVeh.VehicleSetup.iColour2 = 138
									setupVeh.VehicleSetup.iColourExtra1 = 89
									setupVeh.VehicleSetup.iColourExtra2 = 21
									setupVeh.iColour5 = 1
									setupVeh.iColour6 = 132
									setupVeh.iLivery2 = 0
									setupVeh.VehicleSetup.iWheelType = 7
									setupVeh.VehicleSetup.iTyreR = 255
									setupVeh.VehicleSetup.iTyreG = 255
									setupVeh.VehicleSetup.iTyreB = 255
									setupVeh.VehicleSetup.iNeonR = 255
									setupVeh.VehicleSetup.iNeonB = 255
									setupVeh.VehicleSetup.iModIndex[MOD_BUMPER_F] = 6
									setupVeh.VehicleSetup.iModIndex[MOD_BUMPER_R] = 8
									setupVeh.VehicleSetup.iModIndex[MOD_SKIRT] = 1
									setupVeh.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
									setupVeh.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
									setupVeh.VehicleSetup.iModIndex[MOD_GRILL] = 5
									setupVeh.VehicleSetup.iModIndex[MOD_BONNET] = 8
									setupVeh.VehicleSetup.iModIndex[MOD_WING_R] = 2
									setupVeh.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
									SET_VEHICLE_SETUP_MP(tempVehicle,setupVeh)
								ENDIF
								
								SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(tempVehicle, TRUE)
								NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(tempVehicle, TRUE)
								SET_VEHICLE_NOT_STEALABLE_AMBIENTLY(tempVehicle, FALSE)
								SET_VEHICLE_FULLBEAM(tempVehicle, FALSE)
								SET_VEHICLE_LIGHTS(tempVehicle, FORCE_VEHICLE_LIGHTS_OFF)
								SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(tempVehicle, TRUE)
								SET_VEHICLE_FIXED(tempVehicle)
						        SET_ENTITY_HEALTH(tempVehicle, 1000)
						        SET_VEHICLE_ENGINE_HEALTH(tempVehicle, 1000)
						        SET_VEHICLE_PETROL_TANK_HEALTH(tempVehicle, 1000)
								SET_VEHICLE_DIRT_LEVEL(tempVehicle, 0.0)
				                SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(tempVehicle, TRUE)
								SET_VEHICLE_RESPECTS_LOCKS_WHEN_HAS_DRIVER(tempVehicle, TRUE)
								SET_ENTITY_CAN_BE_DAMAGED(tempVehicle, FALSE)
								SET_VEHICLE_RADIO_ENABLED(tempVehicle, FALSE)
								SET_VEHICLE_ON_GROUND_PROPERLY(tempVehicle,0)
								
								IF NETWORK_IS_IN_MP_CUTSCENE()
									SET_NETWORK_CUTSCENE_ENTITIES(FALSE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
				IF NOT bWaitingForCreation
					SET_BIT(m_ServerBD.iServerBS,BS_CASINO_VAL_GARAGE_SERVER_BD_CREATE_VEHICLES)
					PRINTLN("VALET_GAR: MAINTAIN_AMBIENT_VEHICLES: server finished creating ambient vehicles")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC LOAD_INTERIOR()
	
	IF NOT IS_BIT_SET(m_InteriorData.iBS, BS_CASINO_VAL_GARAGE_INTERIOR_REFRESH_ON_INIT)
	
		IF NOT m_InteriorData.bScriptRelaunched 
			
			GET_SIMPLE_INTERIOR_INTERIOR_TYPE_AND_POSITION(m_InteriorData.eSimpleInteriorID, m_InteriorData.sInteriorType, m_InteriorData.vInteriorPosition, m_InteriorData.fInteriorHeading, TRUE)
			m_InteriorData.iInteriorID = GET_INTERIOR_AT_COORDS_WITH_TYPE(m_InteriorData.vInteriorPosition, m_InteriorData.sInteriorType)
			
			IF IS_VALID_INTERIOR(m_InteriorData.iInteriorID) AND IS_INTERIOR_READY(m_InteriorData.iInteriorID)
				
				BOOL bReady = TRUE
				
				IF bReady 
					IF IS_PLAYER_IN_CASINO(PLAYER_ID())
					
						INT iRoomKey = GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID())
					
						IF iRoomKey = ENUM_TO_INT(CASINO_VAL_GARAGE_ROOM_KEY)
							FORCE_ROOM_FOR_ENTITY(PLAYER_PED_ID(), m_InteriorData.iInteriorID, iRoomKey)
							#IF IS_DEBUG_BUILD
							CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_CASINO_VALET_GARAGE - LOAD_INTERIOR - Forcing room for player, we are inside the interior.")
							#ENDIF
						ELSE
							#IF IS_DEBUG_BUILD
							CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_CASINO_VALET_GARAGE - LOAD_INTERIOR - Player is in the CASINO but unable to determine if we should call FORCE_ROOM_FOR_ENTITY")
							#ENDIF
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
						CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_CASINO_VALET_GARAGE - LOAD_INTERIOR - Preventing room force, not inside interior yet.")
						#ENDIF
					ENDIF
					
					REPIN_AND_REFRESH_SIMPLE_INTERIOR()
					
					SET_SIMPLE_INTERIOR_CHILD_SCRIPT_ALLOWS_FOR_WARP(TRUE)
					SET_BIT(m_InteriorData.iBS, BS_CASINO_VAL_GARAGE_INTERIOR_REFRESH_ON_INIT)
					#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_CASINO_VALET_GARAGE - LOAD_INTERIOR - Setting BS_CASINO_VAL_GARAGE_INTERIOR_REFRESH_ON_INIT")
					#ENDIF
					
					RESET_NET_TIMER(m_InteriorData.tPinInMemTimer)
				ENDIF
			ELSE
				IF NOT HAS_NET_TIMER_STARTED(m_InteriorData.tPinInMemTimer)
					#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_CASINO_VALET_GARAGE - LOAD_INTERIOR - Pinning interior: ", NATIVE_TO_INT(m_InteriorData.iInteriorID), " in memory.")
					#ENDIF
					PIN_INTERIOR_IN_MEMORY_VALIDATE(m_InteriorData.iInteriorID)
					START_NET_TIMER(m_InteriorData.tPinInMemTimer)
				ENDIF
				
				IF HAS_NET_TIMER_EXPIRED(m_InteriorData.tPinInMemTimer, m_InteriorData.iPinInMemTimeToExpire)
					RESET_NET_TIMER(m_InteriorData.tPinInMemTimer)
				ENDIF
				
				#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_CASINO_VALET_GARAGE - LOAD_INTERIOR - Waiting for a valid and ready interior with ID: ", NATIVE_TO_INT(m_InteriorData.iInteriorID), " valid? ",
					IS_VALID_INTERIOR(m_InteriorData.iInteriorID), " ready? ", IS_INTERIOR_READY(m_InteriorData.iInteriorID), " disabled? ", IS_INTERIOR_DISABLED(m_InteriorData.iInteriorID))
				#ENDIF
			ENDIF
		ELSE
			SET_SIMPLE_INTERIOR_CHILD_SCRIPT_ALLOWS_FOR_WARP(TRUE)
			SET_BIT(m_InteriorData.iBS, BS_CASINO_VAL_GARAGE_INTERIOR_REFRESH_ON_INIT)
			#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_CASINO_VALET_GARAGE - LOAD_INTERIOR - Setting BS_CASINO_VAL_GARAGE_INTERIOR_REFRESH_ON_INIT")
			#ENDIF
		ENDIF
	ENDIF
	
	IF CAN_SIMPLE_INTERIOR_CHILD_SCRIPT_TAKE_CONTROL()
	AND IS_BIT_SET(m_InteriorData.iBS, BS_CASINO_VAL_GARAGE_INTERIOR_REFRESH_ON_INIT)
		
		BOOL bReady = TRUE
	
		IF NOT IS_BIT_SET(m_InteriorData.iBS, BS_CASINO_VAL_GARAGE_LOAD_SCENE_STARTED)
			
			IF IS_PLAYER_SWITCH_IN_PROGRESS()
				PRINTLN("AM_MP_CASINO_VALET_GARAGE - LOAD_INTERIOR - Can't start load scene player switch is in progress")
				EXIT
			ENDIF
			
			VECTOR vLoadCoords = <<1380.8516, 220.6906, -49.9945>>
			
//			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
//				vLoadCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
//			ELSE
//				PRINTLN("AM_MP_CASINO_VALET_GARAGE - LOAD_INTERIOR - Can't start load scene player is dead")
//				EXIT
//			ENDIF
			
			NEW_LOAD_SCENE_START_SPHERE(vLoadCoords, 50.0)
			
			SET_BIT(m_InteriorData.iBS, BS_CASINO_VAL_GARAGE_LOAD_SCENE_STARTED)
			
			PRINTLN("AM_MP_CASINO_VALET_GARAGE - LOAD_INTERIOR - Setting BS_CASINO_VAL_GARAGE_LOAD_SCENE_STARTED")			
			bReady = FALSE
		ELSE
			IF IS_NEW_LOAD_SCENE_ACTIVE()
				
				IF NOT IS_NEW_LOAD_SCENE_LOADED()
					#IF IS_DEBUG_BUILD
					PRINTLN("AM_MP_CASINO_VALET_GARAGE - LOAD_INTERIOR - IS_NEW_LOAD_SCENE_LOADED = FALSE")
					#ENDIF
					bReady = FALSE
				ELSE
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())	 
					AND NOT HAS_PLAYER_WARPED_WITH_OWNERS_PV_INTO_GARAGE()
						EXIT
					ENDIF
					
					GET_SIMPLE_INTERIOR_INTERIOR_TYPE_AND_POSITION(m_InteriorData.eSimpleInteriorID, m_InteriorData.sInteriorType, m_InteriorData.vInteriorPosition, m_InteriorData.fInteriorHeading)
					m_InteriorData.iInteriorID = GET_INTERIOR_AT_COORDS_WITH_TYPE(m_InteriorData.vInteriorPosition, m_InteriorData.sInteriorType)
					#IF IS_DEBUG_BUILD
					PRINTLN("AM_MP_CASINO_VALET_GARAGE - LOAD_INTERIOR -  load scene is ready.")
					#ENDIF
					NEW_LOAD_SCENE_STOP()
				ENDIF
			ELSE
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())	 
				AND NOT HAS_PLAYER_WARPED_WITH_OWNERS_PV_INTO_GARAGE()
					EXIT
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(m_ServerBD.iServerBS,BS_CASINO_VAL_GARAGE_SERVER_BD_CREATE_VEHICLES)
			bReady = FALSE
			#IF IS_DEBUG_BUILD
			PRINTLN("AM_MP_CASINO_VALET_GARAGE - LOAD_INTERIOR - waiting for server to create ambient vehicles")
			#ENDIF
		ENDIF
		
		IF bReady
		AND CREATE_ELEVATOR_DOORS()
			SET_SIMPLE_INTERIOR_CHILD_SCRIPT_READY(TRUE)
			SET_SIMPLE_INTERIOR_CHILD_SCRIPT_ALLOWS_FOR_WARP(TRUE)
			KILL_SIMPLE_INTERIOR_BUSY_SPINNER()
			START_NET_TIMER(sExitDelayTimer)
			SET_CASINO_VAL_GARAGE_STATE(CASINO_VAL_GARAGE_STATE_IDLE)
		ENDIF
	ELSE
		CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_CASINO_VALET_GARAGE - LOAD_INTERIOR - CAN_SIMPLE_INTERIOR_CHILD_SCRIPT_TAKE_CONTROL = ", CAN_SIMPLE_INTERIOR_CHILD_SCRIPT_TAKE_CONTROL(), ", BS_CASINO_VAL_GARAGE_INTERIOR_REFRESH_ON_INIT = ", IS_BIT_SET(m_InteriorData.iBS, BS_CASINO_VAL_GARAGE_INTERIOR_REFRESH_ON_INIT))
	ENDIF
ENDPROC

PROC MAINTAIN_PLAYER_FLAGS()
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
	AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
	AND NOT g_bEnableJumpingOffHalfTrackInProperty
		SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisablePlayerJumping, TRUE)
		SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisablePlayerVaulting, TRUE)
		SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisablePlayerAutoVaulting, TRUE)
	ENDIF
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
	
	IF NOT NETWORK_IS_ACTIVITY_SESSION()
	AND NOT IS_PAUSE_MENU_ACTIVE_EX()
	AND IS_CASINO_VAL_GARAGE_STATE(CASINO_VAL_GARAGE_STATE_LOADING)
		SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_COVER)
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_COVER)
	ENDIF
	
	IF IS_CINEMATIC_CAM_RENDERING()
		SET_CINEMATIC_MODE_ACTIVE(FALSE)
	ENDIF
ENDPROC

FUNC BOOL ARE_ALL_PEDS_IN_VEHICLE_READY(VEHICLE_INDEX theVeh)
	PLAYER_INDEX thePlayer
	
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		thePlayer = INT_TO_PLAYERINDEX(i)
		
		IF thePlayer != PLAYER_ID()
		AND thePlayer != INVALID_PLAYER_INDEX()
		AND NETWORK_IS_PLAYER_A_PARTICIPANT(thePlayer)
			IF IS_NET_PLAYER_OK(thePlayer)
				IF IS_PED_IN_VEHICLE(GET_PLAYER_PED(thePlayer), theVeh, FALSE)
					IF NOT IS_BIT_SET(m_PlayerBD[NATIVE_TO_INT(thePlayer)].iBS, PLAYER_BD_BS_READY_TO_WARP_OUT_IN_VEH)
					OR GET_SCRIPT_TASK_STATUS(GET_PLAYER_PED(thePlayer), SCRIPT_TASK_LEAVE_VEHICLE) = PERFORMING_TASK
					OR GET_SCRIPT_TASK_STATUS(GET_PLAYER_PED(thePlayer), SCRIPT_TASK_LEAVE_ANY_VEHICLE) = PERFORMING_TASK
					OR IS_PLAYER_WALKING_INTO_SIMPLE_INTERIOR(thePlayer)
						PRINTLN("AM_MP_CASINO_VALET_GARAGE - ARE_ALL_PEDS_IN_VEHICLE_READY - Player in vehicle is not ready: ", i)
						
						RETURN FALSE
					ENDIF
				ELSE
					IF IS_PED_IN_VEHICLE(GET_PLAYER_PED(thePlayer), theVeh, TRUE)
					OR IS_ENTITY_ATTACHED_TO_ENTITY(GET_PLAYER_PED(thePlayer), theVeh)
						PRINTLN("AM_MP_CASINO_VALET_GARAGE - ARE_ALL_PEDS_IN_VEHICLE_READY - Player is not ready: ", i)
						
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

PROC MAINTAIN_CAN_DRIVE_OUT()
	IF IS_NET_PLAYER_OK(PLAYER_ID())
	AND IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
	AND NOT IS_PLAYER_WALKING_INTO_SIMPLE_INTERIOR(PLAYER_ID())
//	AND (NOT IS_SCREEN_FADED_OUT() OR IS_PASSENGER_FADED_OUT_IN_MOD_SHOP() OR IS_PLAYER_LEAVING_CASINO_APT_FROM_MOD_SHOP(PLAYER_ID()))
//	AND (NOT IS_SCREEN_FADING_IN() OR IS_PLAYER_LEAVING_CASINO_APT_FROM_MOD_SHOP(PLAYER_ID()))
		DISABLE_SELECTOR_THIS_FRAME()
		
		IF NOT g_MultiplayerSettings.g_bSuicide
			SET_BIT(m_PlayerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, PLAYER_BD_BS_READY_TO_WARP_OUT_IN_VEH)
		ELSE
			CLEAR_BIT(m_PlayerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, PLAYER_BD_BS_READY_TO_WARP_OUT_IN_VEH)
		ENDIF
	ELSE
		CLEAR_BIT(m_PlayerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, PLAYER_BD_BS_READY_TO_WARP_OUT_IN_VEH)
	ENDIF
	
ENDPROC

FUNC BOOL CAN_PLAYER_DRIVE_OUT()
	IF IS_PHONE_ONSCREEN()
	OR IS_SELECTOR_ONSCREEN()
	OR PROPERTY_HAS_JUST_ACCEPTED_A_MISSION()
	OR IS_PAUSE_MENU_ACTIVE_EX()
	OR IS_COMMERCE_STORE_OPEN()
	OR IS_CELLPHONE_CAMERA_IN_USE()
	OR IS_BROWSER_OPEN()
	OR IS_SYSTEM_UI_BEING_DISPLAYED()
	OR IS_INTERACTION_MENU_OPEN()
	//OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
	//OR IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
		IF NOT HAS_NET_TIMER_STARTED(sPhoneOnScreenTimer)
			START_NET_TIMER(sPhoneOnScreenTimer)
		ELSE
			REINIT_NET_TIMER(sPhoneOnScreenTimer)
		ENDIF
		
		PRINTLN("AM_MP_CASINO_VALET_GARAGE - CAN_PLAYER_DRIVE_OUT - Phone is on screen.")
		
		RETURN FALSE
	ELSE
		IF HAS_NET_TIMER_STARTED(sPhoneOnScreenTimer)
			IF HAS_NET_TIMER_EXPIRED(sPhoneOnScreenTimer, 3000)
				RESET_NET_TIMER(sPhoneOnScreenTimer)
			ELSE
				PRINTLN("AM_MP_CASINO_VALET_GARAGE - CAN_PLAYER_DRIVE_OUT - Phone was on screen.")
				
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT ARE_ALL_PEDS_IN_VEHICLE_READY(currentVeh)
		PRINTLN("AM_MP_CASINO_VALET_GARAGE - CAN_PLAYER_DRIVE_OUT - ARE_ALL_PEDS_IN_VEHICLE_READY is FALSE.")
		
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_STARTED_TO_USE_PERSONAL_CAR_MOD(PLAYER_ID())
		PRINTLN("AM_MP_CASINO_VALET_GARAGE - CAN_PLAYER_DRIVE_OUT - IS_PLAYER_STARTED_TO_USE_PERSONAL_CAR_MOD is TRUE.")
		
		RETURN FALSE
	ENDIF
	
	IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) = PERFORMING_TASK
	OR GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) = WAITING_TO_START_TASK
		PRINTLN("AM_MP_CASINO_VALET_GARAGE - CAN_PLAYER_DRIVE_OUT - Player is exiting vehicle.")
		
		RETURN FALSE
	ENDIF
	
	IF g_bDisableCasinoValetGarageVehicleExit
		PRINTLN("AM_MP_CASINO_VALET_GARAGE - CAN_PLAYER_DRIVE_OUT - g_bDisableCasinoValetGarageVehicleExit is TRUE.")
		
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_SWITCH_IN_PROGRESS()
		PRINTLN("AM_MP_CASINO_VALET_GARAGE - CAN_PLAYER_DRIVE_OUT - IS_PLAYER_SWITCH_IN_PROGRESS is TRUE.")
		
		RETURN FALSE
	ENDIF
	
	IF DOES_SCRIPT_EXIST("appMPJobListNEW")
	AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appMPJobListNEW")) > 0
		PRINTLN("AM_MP_CASINO_VALET_GARAGE - CAN_PLAYER_DRIVE_OUT - appMPJobListNEW is running.")
		
		RETURN FALSE
	ENDIF
	
	IF DOES_SCRIPT_EXIST("appJIPMP")
	AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appJIPMP")) > 0
		PRINTLN("AM_MP_CASINO_VALET_GARAGE - CAN_PLAYER_DRIVE_OUT - appJIPMP is running.")
		
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_SCTV(PLAYER_ID())
		PRINTLN("AM_MP_CASINO_VALET_GARAGE - CAN_PLAYER_DRIVE_OUT - IS_PLAYER_SCTV is TRUE.")
		
		RETURN FALSE
	ENDIF
	
	IF IS_STORE_PENDING_NETWORK_SHUTDOWN_TO_OPEN()
		PRINTLN("AM_MP_CASINO_VALET_GARAGE - CAN_PLAYER_DRIVE_OUT - IS_STORE_PENDING_NETWORK_SHUTDOWN_TO_OPEN is TRUE.")
		
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
		PRINTLN("AM_MP_CASINO_VALET_GARAGE - CAN_PLAYER_DRIVE_OUT - IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP is TRUE.")
		
		RETURN FALSE
	ENDIF

	IF GET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_IsSeatShuffling)
		PRINTLN("AM_MP_CASINO_VALET_GARAGE - CAN_PLAYER_DRIVE_OUT - Player is shuffling seats.")
		
		RETURN FALSE
	ENDIF
	
	IF SHOULD_LAUNCH_CRIMINAL_STARTER_PACK_BROWSER()
		PRINTLN("AM_MP_CASINO_VALET_GARAGE - CAN_PLAYER_DRIVE_OUT - SHOULD_LAUNCH_CRIMINAL_STARTER_PACK_BROWSER is TRUE.")
		
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_WALKING_INTO_SIMPLE_INTERIOR(PLAYER_ID())
		PRINTLN("AM_MP_CASINO_VALET_GARAGE - CAN_PLAYER_DRIVE_OUT - IS_PLAYER_WALKING_INTO_SIMPLE_INTERIOR is TRUE.")
		
		RETURN FALSE
	ENDIF	
	
	IF IS_SCREEN_FADED_OUT()
		PRINTLN("AM_MP_CASINO_VALET_GARAGE - CAN_PLAYER_DRIVE_OUT - IS_SCREEN_FADED_OUT is TRUE.")
		
		RETURN FALSE
	ENDIF
	
	IF IS_SCREEN_FADING_IN()
		PRINTLN("AM_MP_CASINO_VALET_GARAGE - CAN_PLAYER_DRIVE_OUT - IS_SCREEN_FADING_IN is TRUE.")
		
		RETURN FALSE
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(sExitDelayTimer)
		PRINTLN("AM_MP_CASINO_VALET_GARAGE - CAN_PLAYER_DRIVE_OUT - sExitDelayTimer is running.")
		
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

		

PROC MAINTAIN_PERSONAL_VEHICLE_LOCKS()
	g_bPlayerLeavingCurrentInteriorInVeh = FALSE
	BOOL bKeepContextIntention
	CONTROL_ACTION exitControl
	IF HAS_NET_TIMER_STARTED(sExitDelayTimer)
	AND HAS_NET_TIMER_EXPIRED(sExitDelayTimer, 3000)
		RESET_NET_TIMER(sExitDelayTimer)
	ENDIF
	PLAYER_INDEX vehicleOwner
	IF SHOULD_THIS_SIMPLE_INTERIOR_CHILD_THREAD_TERMINATE(m_InteriorData.iScriptInstance, m_InteriorData.eSimpleInteriorID)
		EXIT
	ENDIF
	IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), FALSE)
	AND IS_BIT_SET(MPGlobals.VehicleData.iBSWarpIntoCasinoCarPark,CASINO_CAR_PARK_BS_RADIO_OFF)
		IF NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
			IF IS_VEHICLE_DRIVEABLE(MPGlobalsAmbience.vehPersonalVehicle)
				IF NETWORK_HAS_CONTROL_OF_ENTITY(MPGlobalsAmbience.vehPersonalVehicle)
					SET_VEHICLE_RADIO_ENABLED(MPGlobalsAmbience.vehPersonalVehicle, TRUE)
					IF NOT gb_bBGScriptRunVehicleStopCasinoValetGarage
						STOP_BRINGING_VEHICLE_TO_HALT(MPGlobalsAmbience.vehPersonalVehicle)
					ENDIF
					CLEAR_BIT(MPGlobals.VehicleData.iBSWarpIntoCasinoCarPark,CASINO_CAR_PARK_BS_RADIO_OFF)
					PRINTLN("AM_MP_CASINO_VALET_GARAGE - MAINTAIN_PERSONAL_VEHICLE_LOCKS - turning back on vehicle radio")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), FALSE)
	AND g_iSimpleInteriorState = SIMPLE_INT_STATE_IDLE
		currentVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(), TRUE)
		vehicleOwner = GET_OWNER_OF_PERSONAL_VEHICLE(currentVeh)
		IF IS_VEHICLE_MODEL(currentVeh, TEZERACT)
		OR IS_VEHICLE_MODEL(currentVeh, OPPRESSOR)
		OR IS_VEHICLE_MODEL(currentVeh, OPPRESSOR2)
		OR IS_VEHICLE_MODEL(currentVeh, SCARAB)
		OR IS_VEHICLE_MODEL(currentVeh, SCARAB2)
		OR IS_VEHICLE_MODEL(currentVeh, SCARAB3)
		OR IS_VEHICLE_MODEL(currentVeh, DEATHBIKE)
		OR IS_VEHICLE_MODEL(currentVeh, DEATHBIKE2)
		OR IS_VEHICLE_MODEL(currentVeh, DEATHBIKE3)
		    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ACCELERATE) 
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BRAKE) 
		ENDIF 
		IF GET_PED_IN_VEHICLE_SEAT(currentVeh, VS_DRIVER) = GET_PLAYER_PED(vehicleOwner)
			IF IS_VEHICLE_MODEL(currentVeh, OPPRESSOR2)
				IF GET_IS_VEHICLE_ENGINE_RUNNING(currentVeh)
				AND g_iSimpleInteriorState = SIMPLE_INT_STATE_IDLE
					SET_VEHICLE_ENGINE_ON(currentVeh, FALSE, TRUE)
				ENDIF
				
				SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableStartEngine, TRUE)
			ELSE
				SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableStartEngine, FALSE)
			ENDIF
			IF IS_VEHICLE_A_PERSONAL_VEHICLE(currentVeh)
				IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
					//IF ///(GET_IS_VEHICLE_ENGINE_RUNNING(currentVeh) OR IS_VEHICLE_MODEL(currentVeh, OPPRESSOR2))
					IF NETWORK_HAS_CONTROL_OF_ENTITY(currentVeh)
						SET_VEHICLE_FORWARD_SPEED(currentVeh,0)
					ENDIF
					IF PLAYER_PED_ID() = GET_PED_IN_VEHICLE_SEAT(currentVeh)
					AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
						IF CAN_PLAYER_DRIVE_OUT()
							IF NOT g_MultiplayerSettings.g_bSuicide
							AND NOT IS_SELECTOR_ONSCREEN()
								bKeepContextIntention = TRUE
								IF NOT IS_BIT_SET(m_InteriorData.iBS, BS_CASINO_VAL_GARAGE_EXITING_IN_VEH)
								AND NOT g_bPlayerLeavingCurrentInteriorInVeh
									IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
										exitControl = INPUT_FRONTEND_RS
									ELSE
										exitControl = INPUT_CONTEXT
									ENDIF
									IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
									AND NOT IS_HELP_MESSAGE_ON_SCREEN()
									AND NOT IS_BIT_SET(m_InteriorData.iBS, BS_CASINO_VAL_GARAGE_PRINT_EXIT_W_CAR_HELP)
										IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
											PRINT_HELP("CASINO_EXIT_0PC")
											PRINTLN("AM_MP_CASINO_VALET_GARAGE - MAINTAIN_PERSONAL_VEHICLE_LOCKS - printing CASINO_EXIT_0PC")
										ELSE
											PRINT_HELP("CASINO_EXIT_0")
											PRINTLN("AM_MP_CASINO_VALET_GARAGE - MAINTAIN_PERSONAL_VEHICLE_LOCKS - printing CASINO_EXIT_0")
										ENDIF	
										SET_BIT(m_InteriorData.iBS, BS_CASINO_VAL_GARAGE_PRINT_EXIT_W_CAR_HELP)
									ENDIF
									IF IS_CONTROL_JUST_RELEASED(PLAYER_CONTROL,exitControl)
									OR IS_DISABLED_CONTROL_JUST_RELEASED(PLAYER_CONTROL,exitControl)
										PRINTLN("AM_MP_CASINO_VALET_GARAGE - MAINTAIN_PERSONAL_VEHICLE_LOCKS - g_bPlayerLeavingCurrentInteriorInVeh is TRUE")
										g_bPlayerLeavingCurrentInteriorInVeh = TRUE
										SET_BIT(m_InteriorData.iBS, BS_CASINO_VAL_GARAGE_EXITING_IN_VEH)
									ENDIF
								ENDIF
//								IF GET_DISABLED_CONTROL_UNBOUND_NORMAL(PLAYER_CONTROL, INPUT_VEH_ACCELERATE) != 0
//								OR GET_DISABLED_CONTROL_UNBOUND_NORMAL(PLAYER_CONTROL, INPUT_VEH_BRAKE) != 0
//								OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
//								OR IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
//								OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
//								OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_BRAKE)
//								OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_BRAKE)
//								OR IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_BRAKE)
//								OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_BRAKE)
//									PRINTLN("AM_MP_CASINO_VALET_GARAGE - MAINTAIN_PERSONAL_VEHICLE_LOCKS - g_bPlayerLeavingCurrentInteriorInVeh is TRUE")
//									
//									IF IS_VEHICLE_MODEL(currentVeh, OPPRESSOR2)
//										SET_VEHICLE_ENGINE_ON(currentVeh, TRUE, TRUE)
//									ENDIF
//									
//									g_bPlayerLeavingCurrentInteriorInVeh = TRUE
//									//m_InteriorData.bDrivingOut = TRUE
//								ENDIF
							ENDIF
						ELSE
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BRAKE)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
						ENDIF
					ELSE
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BRAKE)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	IF NOT bKeepContextIntention
		CLEAR_BIT(m_InteriorData.iBS, BS_CASINO_VAL_GARAGE_EXITING_IN_VEH)
		IF IS_BIT_SET(m_InteriorData.iBS, BS_CASINO_VAL_GARAGE_PRINT_EXIT_W_CAR_HELP)
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CASINO_EXIT_0PC")
			OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CASINO_EXIT_0")
				CLEAR_HELP()
			ENDIF
			CLEAR_BIT(m_InteriorData.iBS, BS_CASINO_VAL_GARAGE_PRINT_EXIT_W_CAR_HELP)
		ENDIF
	ENDIF
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════╡ ELEVATOR CUTSCENE ╞═════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

FUNC VECTOR GET_ELEVATOR_SCENE_CAM_ROT()
	IF IS_BIT_SET(m_InteriorData.iBS, BS_CASINO_VAL_GARAGE_ELEVATOR_CUTSCENE_FRONT)
		RETURN <<-12.6185, -0.0000, -145.9632>>
	ENDIF
	
	RETURN <<-10.6456, -0.0000, 27.7174>>
ENDFUNC

FUNC VECTOR GET_ELEVATOR_SCENE_CAM_POS()
	IF IS_BIT_SET(m_InteriorData.iBS, BS_CASINO_VAL_GARAGE_ELEVATOR_CUTSCENE_FRONT)
		RETURN <<1378.9702, 181.7238, -48.1101>>
	ENDIF
	
	RETURN <<1381.1157, 255.9078, -48.0196>>
ENDFUNC

FUNC VECTOR GET_ELEVATOR_SCENE_ROT()
	IF IS_BIT_SET(m_InteriorData.iBS, BS_CASINO_VAL_GARAGE_ELEVATOR_CUTSCENE_FRONT)
		RETURN <<0.0, 0.0, 0.0>>
	ENDIF
	
	RETURN <<0.0, 0.0, 180.0>>
ENDFUNC

FUNC VECTOR GET_ELEVATOR_SCENE_POS()
	IF IS_BIT_SET(m_InteriorData.iBS, BS_CASINO_VAL_GARAGE_ELEVATOR_CUTSCENE_FRONT)
		RETURN <<1380.176, 180.079, -49.9954>>
	ENDIF
	
	RETURN <<1379.950, 257.779, -49.9954>>
ENDFUNC

PROC ADD_CLONE_TO_SYNCED_SCENE()
	IF DOES_ENTITY_EXIST(m_InteriorData.pedClone)
	AND NOT IS_PED_INJURED(m_InteriorData.pedClone)
		FREEZE_ENTITY_POSITION(m_InteriorData.pedClone, FALSE)
		SET_ENTITY_ROTATION(m_InteriorData.pedClone, GET_ANIM_INITIAL_OFFSET_ROTATION("anim@apt_trans@elevator", "elev_1", GET_ELEVATOR_SCENE_POS(), GET_ELEVATOR_SCENE_ROT(), 0.0))
		SET_ENTITY_COORDS_NO_OFFSET(m_InteriorData.pedClone, GET_ANIM_INITIAL_OFFSET_POSITION("anim@apt_trans@elevator", "elev_1", GET_ELEVATOR_SCENE_POS(), GET_ELEVATOR_SCENE_ROT(), 0.0))
		TASK_SYNCHRONIZED_SCENE(m_InteriorData.pedClone, m_InteriorData.iNonNetSceneID, "anim@apt_trans@elevator", "elev_1", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_NONE)
	ENDIF
ENDPROC

PROC CREATE_CLONE_OF_PLAYER(PLAYER_INDEX PlayerID, VECTOR vOffset)
	IF NOT DOES_ENTITY_EXIST(m_InteriorData.pedClone)
		IF IS_NET_PLAYER_OK(PlayerID)
			IF NOT IS_PLAYER_FEMALE()
				m_InteriorData.pedClone = CREATE_PED(PEDTYPE_CIVMALE, GET_ENTITY_MODEL(GET_PLAYER_PED(PlayerID)), GET_ENTITY_COORDS(GET_PLAYER_PED(PlayerID)) + vOffset, GET_ENTITY_HEADING(GET_PLAYER_PED(PlayerID)), FALSE, FALSE)
			ELSE
				m_InteriorData.pedClone = CREATE_PED(PEDTYPE_CIVFEMALE, GET_ENTITY_MODEL(GET_PLAYER_PED(PlayerID)), GET_ENTITY_COORDS(GET_PLAYER_PED(PlayerID)) + vOffset, GET_ENTITY_HEADING(GET_PLAYER_PED(PlayerID)), FALSE, FALSE)
			ENDIF
			
			CLONE_PED_TO_TARGET(GET_PLAYER_PED(PlayerID), m_InteriorData.pedClone)
			
			IF IS_VALID_INTERIOR(GET_INTERIOR_FROM_ENTITY(GET_PLAYER_PED(PlayerID)))
				IF IS_INTERIOR_READY(GET_INTERIOR_FROM_ENTITY(GET_PLAYER_PED(PlayerID)))
					FORCE_ROOM_FOR_ENTITY(m_InteriorData.pedClone, GET_INTERIOR_FROM_ENTITY(GET_PLAYER_PED(PlayerID)), GET_ROOM_KEY_FROM_ENTITY(GET_PLAYER_PED(PlayerID)))
				ENDIF
			ENDIF
			
			FREEZE_ENTITY_POSITION(m_InteriorData.pedClone, TRUE)
			
			SET_CURRENT_PED_WEAPON(m_InteriorData.pedClone, WEAPONTYPE_UNARMED, TRUE)
			SET_FORCE_FOOTSTEP_UPDATE(m_InteriorData.pedClone, TRUE)
		ENDIF
	ENDIF
ENDPROC

CONST_FLOAT MAX_ELEVATOR_MOVEMENT 0.4

FLOAT fElevatorDoorMovementAmt = 0.4

FUNC FLOAT GET_ELEVATOR_CLOSED_Y(INT iDoorIndex)
	SWITCH iDoorIndex
		CASE 0	RETURN 179.6634
		CASE 1	RETURN 179.6640
		CASE 2	RETURN 258.2452
		CASE 3	RETURN 258.2452
	ENDSWITCH
	
	RETURN 0.0
ENDFUNC

FUNC FLOAT GET_ELEVATOR_CLOSED_X(INT iDoorIndex)
	SWITCH iDoorIndex
		CASE 0	RETURN 1380.8552
		CASE 1	RETURN 1379.3513
		CASE 2	RETURN 1379.2699
		CASE 3	RETURN 1380.7738
	ENDSWITCH
	
	RETURN 0.0
ENDFUNC

FUNC FLOAT GET_ELEVATOR_OPEN_Y(INT iDoorIndex)
	SWITCH iDoorIndex
		CASE 0	RETURN 179.6634
		CASE 1	RETURN 179.6640
		CASE 2	RETURN 258.2452
		CASE 3	RETURN 258.2452
	ENDSWITCH
	
	RETURN 0.0
ENDFUNC

FUNC FLOAT GET_ELEVATOR_OPEN_X(INT iDoorIndex)
	SWITCH iDoorIndex
		CASE 0	RETURN 1381.5963
		CASE 1	RETURN 1378.6102
		CASE 2	RETURN 1378.5288
		CASE 3	RETURN 1381.5149
	ENDSWITCH
	
	RETURN 0.0
ENDFUNC

PROC CLOSE_ELEVATOR_DOOR(INT iDoorIndex)
	FLOAT xPosDis = GET_ELEVATOR_CLOSED_X(iDoorIndex) - GET_ELEVATOR_OPEN_X(iDoorIndex)
	FLOAT yPosDis = GET_ELEVATOR_CLOSED_Y(iDoorIndex) - GET_ELEVATOR_OPEN_Y(iDoorIndex)
	
	FLOAT fDivisionRatioX = 1
	FLOAT fDivisionRatioY = 1
	
	IF ABSF(xPosDis) > ABSF(yPosDis)
		fDivisionRatioY = ABSF(yPosDis) / ABSF(xPosDis)
	ELIF ABSF(yPosDis) > ABSF(xPosDis)
		fDivisionRatioX = ABSF(xPosDis) / ABSF(yPosDis)
	ENDIF
	
	IF DOES_ENTITY_EXIST(m_InteriorData.objElevatorDoor[iDoorIndex])
		VECTOR vCoords = GET_ENTITY_COORDS(m_InteriorData.objElevatorDoor[iDoorIndex])
		
		IF vCoords.x = GET_ELEVATOR_CLOSED_X(iDoorIndex)
		AND vCoords.y = GET_ELEVATOR_CLOSED_Y(iDoorIndex)
			IF NOT IS_BIT_SET(m_InteriorData.iBS, BS_CASINO_VAL_GARAGE_PLAY_ELEVATOR_CLOSED_SOUND)
				SET_BIT(m_InteriorData.iBS, BS_CASINO_VAL_GARAGE_PLAY_ELEVATOR_CLOSED_SOUND)
				
				PLAY_SOUND_FRONTEND(-1, "CLOSED", "MP_PROPERTIES_ELEVATOR_DOORS")
			ENDIF
		ENDIF
		
		IF vCoords.x = GET_ELEVATOR_OPEN_X(iDoorIndex)
		AND vCoords.y = GET_ELEVATOR_OPEN_Y(iDoorIndex)
			IF NOT IS_BIT_SET(m_InteriorData.iBS, BS_CASINO_VAL_GARAGE_PLAY_ELEVATOR_CLOSING_SOUND)
				SET_BIT(m_InteriorData.iBS, BS_CASINO_VAL_GARAGE_PLAY_ELEVATOR_CLOSING_SOUND)
				
				PLAY_SOUND_FRONTEND(-1, "CLOSING", "MP_PROPERTIES_ELEVATOR_DOORS")
			ENDIF
		ENDIF
		
		fElevatorDoorMovementAmt = MAX_ELEVATOR_MOVEMENT * fDivisionRatioX
		
		IF xPosDis < 0
			vCoords.x = vCoords.x -@ fElevatorDoorMovementAmt
			
			IF vCoords.x < GET_ELEVATOR_CLOSED_X(iDoorIndex)
				vCoords.x = GET_ELEVATOR_CLOSED_X(iDoorIndex)
			ENDIF
		ENDIF
		
		IF xPosDis > 0
			vCoords.x = vCoords.x +@ fElevatorDoorMovementAmt
			
			IF vCoords.x > GET_ELEVATOR_CLOSED_X(iDoorIndex)
				vCoords.x = GET_ELEVATOR_CLOSED_X(iDoorIndex)
			ENDIF
		ENDIF
		
		fElevatorDoorMovementAmt = MAX_ELEVATOR_MOVEMENT * fDivisionRatioY
		
		IF yPosDis < 0
			vCoords.y = vCoords.y -@ fElevatorDoorMovementAmt
			
			IF vCoords.y < GET_ELEVATOR_CLOSED_Y(iDoorIndex)
				vCoords.y = GET_ELEVATOR_CLOSED_Y(iDoorIndex)
			ENDIF
		ENDIF
		
		IF yPosDis > 0
			vCoords.y = vCoords.y +@ fElevatorDoorMovementAmt
			
			IF vCoords.y > GET_ELEVATOR_CLOSED_Y(iDoorIndex)
				vCoords.y = GET_ELEVATOR_CLOSED_Y(iDoorIndex)
			ENDIF
		ENDIF
		
		SET_ENTITY_COORDS(m_InteriorData.objElevatorDoor[iDoorIndex], vCoords)
	ENDIF
ENDPROC

PROC OPEN_ELEVATOR_DOOR(INT iDoorIndex)
	FLOAT xPosDis = GET_ELEVATOR_CLOSED_X(iDoorIndex) - GET_ELEVATOR_OPEN_X(iDoorIndex)
	FLOAT yPosDis = GET_ELEVATOR_CLOSED_Y(iDoorIndex) - GET_ELEVATOR_OPEN_Y(iDoorIndex)
	
	FLOAT fDivisionRatioX = 1
	FLOAT fDivisionRatioY = 1
	
	IF ABSF(xPosDis) > ABSF(yPosDis)
		fDivisionRatioY = ABSF(yPosDis) / ABSF(xPosDis)
	ELIF ABSF(yPosDis) > ABSF(xPosDis)
		fDivisionRatioX = ABSF(xPosDis) / ABSF(yPosDis)
	ENDIF
	
	IF DOES_ENTITY_EXIST(m_InteriorData.objElevatorDoor[iDoorIndex])
		VECTOR vCoords = GET_ENTITY_COORDS(m_InteriorData.objElevatorDoor[iDoorIndex])
		
		IF vCoords.x = GET_ELEVATOR_CLOSED_X(iDoorIndex)
		AND vCoords.y = GET_ELEVATOR_CLOSED_Y(iDoorIndex)
			IF NOT IS_BIT_SET(m_InteriorData.iBS, BS_CASINO_VAL_GARAGE_PLAY_ELEVATOR_OPENING_SOUND)
				SET_BIT(m_InteriorData.iBS, BS_CASINO_VAL_GARAGE_PLAY_ELEVATOR_OPENING_SOUND)
				
				PLAY_SOUND_FRONTEND(-1, "OPENING", "MP_PROPERTIES_ELEVATOR_DOORS")
			ENDIF
		ENDIF
		
		IF vCoords.x = GET_ELEVATOR_OPEN_X(iDoorIndex)
		AND vCoords.y = GET_ELEVATOR_OPEN_Y(iDoorIndex)
			IF NOT IS_BIT_SET(m_InteriorData.iBS, BS_CASINO_VAL_GARAGE_PLAY_ELEVATOR_OPENED_SOUND)
				SET_BIT(m_InteriorData.iBS, BS_CASINO_VAL_GARAGE_PLAY_ELEVATOR_OPENED_SOUND)
				
				PLAY_SOUND_FRONTEND(-1, "OPENED", "MP_PROPERTIES_ELEVATOR_DOORS")
			ENDIF
		ENDIF
		
		fElevatorDoorMovementAmt = MAX_ELEVATOR_MOVEMENT * fDivisionRatioX
		
		IF xPosDis > 0
			vCoords.x = vCoords.x -@ fElevatorDoorMovementAmt
			
			IF vCoords.x < GET_ELEVATOR_OPEN_X(iDoorIndex)
				vCoords.x = GET_ELEVATOR_OPEN_X(iDoorIndex)
			ENDIF
		ENDIF
		
		IF xPosDis < 0
			vCoords.x = vCoords.x +@ fElevatorDoorMovementAmt
			
			IF vCoords.x > GET_ELEVATOR_OPEN_X(iDoorIndex)
				vCoords.x = GET_ELEVATOR_OPEN_X(iDoorIndex)
			ENDIF
		ENDIF
		
		fElevatorDoorMovementAmt = MAX_ELEVATOR_MOVEMENT * fDivisionRatioY
		
		IF yPosDis > 0
			vCoords.y = vCoords.y -@ fElevatorDoorMovementAmt
			
			IF vCoords.y < GET_ELEVATOR_OPEN_Y(iDoorIndex)
				vCoords.y = GET_ELEVATOR_OPEN_Y(iDoorIndex)
			ENDIF
		ENDIF
		
		IF yPosDis < 0
			vCoords.y = vCoords.y +@ fElevatorDoorMovementAmt
			
			IF vCoords.y > GET_ELEVATOR_OPEN_Y(iDoorIndex)
				vCoords.y = GET_ELEVATOR_OPEN_Y(iDoorIndex)
			ENDIF
		ENDIF
		
		SET_ENTITY_COORDS(m_InteriorData.objElevatorDoor[iDoorIndex], vCoords)
	ENDIF
ENDPROC

PROC MAINTAIN_ELEVATOR_CUTSCENE()
	IF g_SimpleInteriorData.g_bKillElevatorCutscene
		m_InteriorData.eElevatorState = ELEVATOR_STATE_IDLE
		
		CLEANUP_ELEVATOR_CUTSCENE(TRUE)
		
		g_SimpleInteriorData.g_bKillElevatorCutscene = FALSE
	ENDIF
	
	IF SHOULD_SIMPLE_INTERIOR_PLAY_FAKE_ELE_ARIVAL_SOUND()
	AND IS_SCREEN_FADED_IN()
	AND IS_PLAYER_CONTROL_ON(PLAYER_ID())
	AND IS_PLAYER_IN_CASINO_VALET_GARAGE(PLAYER_ID())
	AND NOT SHOULD_KEEP_EXTERIOR_CAM_FOR_THIS_SIMPLE_INTERIOR(m_InteriorData.eSimpleInteriorID)
	AND NOT SHOULD_TRANSITION_TO_THIS_INTERIOR_FROM_EXT_MENU(m_InteriorData.eSimpleInteriorID)
		PLAY_SOUND_FRONTEND(-1, "FAKE_ARRIVE", "MP_PROPERTIES_ELEVATOR_DOORS")
		
		CLEAR_SIMPLE_INTERIOR_PLAY_FAKE_ELE_ARIVAL_SOUND()
	ENDIF
	
	IF m_InteriorData.eElevatorState > ELEVATOR_STATE_IDLE
		DISABLE_FRONTEND_THIS_FRAME()
		
		DISABLE_CELLPHONE_THIS_FRAME_ONLY(TRUE)
		
		HIDE_HUD_AND_RADAR_THIS_FRAME()
	ENDIF
	
	SWITCH m_InteriorData.eElevatorState
		CASE ELEVATOR_STATE_IDLE
			IF SHOULD_SIMPLE_INTERIOR_TRANSITION_VIA_ELEVATOR()
			OR IS_BIT_SET(g_simpleInteriorData.iSixthBS, BS6_SIMPLE_INTERIOR_EXIT_FROM_ELEVATOR)
			OR IS_BIT_SET(g_simpleInteriorData.iSixthBS, BS6_SIMPLE_INTERIOR_EXIT_TO_CASINO_HELIPAD)
			OR IS_BIT_SET(g_simpleInteriorData.iSixthBS, BS6_SIMPLE_INTERIOR_SWITCH_FLOOR_FROM_ELEVATOR_TO_SAME_INTERIOR)
			OR IS_BIT_SET(g_simpleInteriorData.iEighthBS, BS8_SIMPLE_INTERIOR_TRIGGER_ELEVATOR_SCENE_FOR_MISSION)
				IF GET_SIMPLE_INTERIOR_ENTRY_POINT_LOCAL_PLAYER_USED() != ciCASINO_APT_GAR_ENTRANCE_SHUTTER
				OR IS_BIT_SET(g_simpleInteriorData.iEighthBS, BS8_SIMPLE_INTERIOR_TRIGGER_ELEVATOR_SCENE_FOR_MISSION)
					HIDE_HUD_AND_RADAR_THIS_FRAME()
					
					m_InteriorData.eElevatorState = ELEVATOR_STATE_INIT
					
					CLEAR_BIT(m_PlayerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_CASINO_VAL_GARAGE_PLAYER_BD_ELEVATOR_CUTSCENE_COMPLETE)
					
					SET_BIT(g_SimpleInteriorData.iForthBS, BS4_SIMPLE_INTERIOR_USED_ELEVATOR)
					
					PRINTLN("MAINTAIN_ELEVATOR_CUTSCENE - eElevatorState = ELEVATOR_STATE_INIT")
				ELSE
					m_InteriorData.eElevatorState = ELEVATOR_STATE_FINISHED
					
					PRINTLN("MAINTAIN_ELEVATOR_CUTSCENE - eElevatorState = ELEVATOR_STATE_FINISHED")
				ENDIF
			ENDIF
		BREAK
		
		CASE ELEVATOR_STATE_INIT
			CREATE_CLONE_OF_PLAYER(PLAYER_ID(), <<0.0, 0.0, -20.0>>)
			
			m_InteriorData.eElevatorState = ELEVATOR_STATE_LOAD_ANIMS
			
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<1379.9875, 180.2417, -49.9955>>) < GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<1380.0007, 257.9860, -49.9955>>)
				SET_BIT(m_InteriorData.iBS, BS_CASINO_VAL_GARAGE_ELEVATOR_CUTSCENE_FRONT)
			ELSE
				CLEAR_BIT(m_InteriorData.iBS, BS_CASINO_VAL_GARAGE_ELEVATOR_CUTSCENE_FRONT)
			ENDIF
			
			PRINTLN("MAINTAIN_ELEVATOR_CUTSCENE - eElevatorState = ELEVATOR_STATE_LOAD_ANIMS")
		BREAK
		
		CASE ELEVATOR_STATE_LOAD_ANIMS
			REQUEST_ANIM_DICT("anim@apt_trans@elevator")
			
			IF NOT HAS_ANIM_DICT_LOADED("anim@apt_trans@elevator")
				EXIT
			ENDIF
			
			IF DOES_ENTITY_EXIST(m_InteriorData.pedClone)
			AND NOT IS_PED_INJURED(m_InteriorData.pedClone)
			AND IS_ENTITY_VISIBLE(m_InteriorData.pedClone)
			AND IS_ENTITY_VISIBLE_TO_SCRIPT(m_InteriorData.pedClone)
				IF NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(m_InteriorData.pedClone)
					EXIT
				ENDIF
			ELSE
				EXIT
			ENDIF
			
			m_InteriorData.eElevatorState = ELEVATOR_STATE_RUN
			
			PRINTLN("MAINTAIN_ELEVATOR_CUTSCENE - eElevatorState = ELEVATOR_STATE_RUN")
		BREAK
		
		CASE ELEVATOR_STATE_RUN
			IF NOT IS_SCREEN_FADED_OUT()
			AND NOT IS_SCREEN_FADING_OUT()
				IF m_InteriorData.iNonNetSceneID = -1
					NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_NO_COLLISION | NSPC_FREEZE_POSITION)
					
					FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
					
					SET_ENTITY_COORDS(PLAYER_PED_ID(), <<1379.4364, 183.2077, -49.9955>>)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), 180.0)
					
					CLEAR_PED_TASKS(PLAYER_PED_ID())
					
					NETWORK_FADE_OUT_ENTITY(PLAYER_PED_ID(), TRUE, TRUE)
					
					SET_LOCAL_PLAYER_VISIBLE_IN_CUTSCENE(FALSE, TRUE)
					
					START_MP_CUTSCENE(TRUE)
					
					m_InteriorData.camElevatorCutscene = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
					
					SET_CAM_FAR_CLIP(m_InteriorData.camElevatorCutscene, 1000)
					
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
					
					m_InteriorData.iNonNetSceneID = CREATE_SYNCHRONIZED_SCENE(GET_ELEVATOR_SCENE_POS(), GET_ELEVATOR_SCENE_ROT(), EULER_YXZ)
					
					ADD_CLONE_TO_SYNCED_SCENE()
					
					SET_SYNCHRONIZED_SCENE_PHASE(m_InteriorData.iNonNetSceneID, 0.3)
					
					IF DOES_CAM_EXIST(m_InteriorData.camElevatorCutscene)
						SET_CAM_PARAMS(m_InteriorData.camElevatorCutscene, GET_ELEVATOR_SCENE_CAM_POS(), GET_ELEVATOR_SCENE_CAM_ROT(), 50.0)
						SHAKE_CAM(m_InteriorData.camElevatorCutscene, "HAND_SHAKE", 0.25)
					ENDIF
				ELSE
					IF GET_SYNCHRONIZED_SCENE_PHASE(m_InteriorData.iNonNetSceneID) >= 0.9
						DO_SCREEN_FADE_OUT(500)
					ELIF GET_SYNCHRONIZED_SCENE_PHASE(m_InteriorData.iNonNetSceneID) >= 0.7
						IF IS_BIT_SET(m_InteriorData.iBS, BS_CASINO_VAL_GARAGE_ELEVATOR_CUTSCENE_FRONT)
							CLOSE_ELEVATOR_DOOR(0)
							CLOSE_ELEVATOR_DOOR(1)
						ELSE
							CLOSE_ELEVATOR_DOOR(2)
							CLOSE_ELEVATOR_DOOR(3)
						ENDIF
					ELSE
						IF GET_SYNCHRONIZED_SCENE_PHASE(m_InteriorData.iNonNetSceneID) >= 0.68
							IF NOT IS_BIT_SET(m_InteriorData.iBS, BS_CASINO_VAL_GARAGE_PLAY_ELEVATOR_BUTTON_SOUND)
								SET_BIT(m_InteriorData.iBS, BS_CASINO_VAL_GARAGE_PLAY_ELEVATOR_BUTTON_SOUND)
								
								PLAY_SOUND_FRONTEND(-1, "BUTTON", "MP_PROPERTIES_ELEVATOR_DOORS")
							ENDIF
						ENDIF
						
						IF IS_BIT_SET(m_InteriorData.iBS, BS_CASINO_VAL_GARAGE_ELEVATOR_CUTSCENE_FRONT)
							OPEN_ELEVATOR_DOOR(0)
							OPEN_ELEVATOR_DOOR(1)
						ELSE
							OPEN_ELEVATOR_DOOR(2)
							OPEN_ELEVATOR_DOOR(3)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF IS_SCREEN_FADED_OUT()
				AND IS_PLAYER_IN_CASINO_VALET_GARAGE(PLAYER_ID())
					CLEANUP_ELEVATOR_CUTSCENE()
					
					SET_BIT(m_PlayerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_CASINO_VAL_GARAGE_PLAYER_BD_ELEVATOR_CUTSCENE_COMPLETE)
					
					m_InteriorData.eElevatorState = ELEVATOR_STATE_FINISHED
					
					PRINTLN("MAINTAIN_ELEVATOR_CUTSCENE - eElevatorState = ELEVATOR_STATE_FINISHED")
				ENDIF
			ENDIF
		BREAK
		
		CASE ELEVATOR_STATE_FINISHED
			IF IS_BIT_SET(g_simpleInteriorData.iEighthBS, BS8_SIMPLE_INTERIOR_TRIGGER_ELEVATOR_SCENE_FOR_MISSION)
				SET_BIT(g_simpleInteriorData.iEighthBS, BS8_SIMPLE_INTERIOR_FINISHED_ELEVATOR_SCENE_FOR_MISSION)
			ELIF SHOULD_SIMPLE_INTERIOR_TRANSITION_VIA_ELEVATOR()
				IF GET_SIMPLE_INTERIOR_TO_TRANSITION_TO_VIA_ELEVATOR() = SIMPLE_INTERIOR_CASINO_APT
					IF GET_INTERIOR_FLOOR_INDEX() != ciCASINO_APT_FLOOR_APARTMENT
					AND GET_SIMPLE_INTERIOR_ENTRY_POINT_LOCAL_PLAYER_USED() != ciCASINO_APT_GAR_ENTRANCE_SHUTTER
						SET_SIMPLE_INTERIOR_PLAY_FAKE_ELE_ARIVAL_SOUND()
					ENDIF
				ELSE
					SET_SIMPLE_INTERIOR_PLAY_FAKE_ELE_ARIVAL_SOUND()
				ENDIF
				
				SET_PLAYER_DATA_DOING_INTERIOR_WARP_FROM_CUTSCENE(TRUE)
				
				//Build the lookup table to allow us to grab the correct function pointer
				SIMPLE_INTERIOR_INTERFACE lookUpTable
				BUILD_SIMPLE_INTERIOR_LOOK_UP_TABLE(GET_SIMPLE_INTERIOR_TO_TRANSITION_TO_VIA_ELEVATOR(), lookUpTable, E_SET_SIMPLE_INTERIOR_ACCESS_BS)
				
				//Trigger the transition to the selected interior
				TRIGGER_AUTOWARP_FROM_INTERIOR_TO_INTERIOR(PLAYER_ID(), GET_SIMPLE_INTERIOR_TO_TRANSITION_TO_VIA_ELEVATOR(), lookUpTable.setSimpleInteriorAccessBS)
				CLEAR_SIMPLE_INTERIOR_TO_TRANSITION_TO_VIA_ELEVATOR()
			ELIF IS_BIT_SET(g_simpleInteriorData.iSixthBS, BS6_SIMPLE_INTERIOR_EXIT_FROM_ELEVATOR)
			OR IS_BIT_SET(g_simpleInteriorData.iSixthBS, BS6_SIMPLE_INTERIOR_EXIT_TO_CASINO_HELIPAD)
				CLEAR_BIT(g_simpleInteriorData.iSixthBS, BS6_SIMPLE_INTERIOR_EXIT_FROM_ELEVATOR)
				
				TRIGGER_EXIT_FROM_SIMPLE_INTERIOR_NOW()
				
				IF NOT IS_BIT_SET(g_simpleInteriorData.iSixthBS, BS6_SIMPLE_INTERIOR_EXIT_TO_CASINO_HELIPAD)
					SET_BIT(g_simpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_SPAWN_LOCATION_CASINO_ROOF)
				ENDIF
			ELIF IS_BIT_SET(g_SimpleInteriorData.iSixthBS, BS6_SIMPLE_INTERIOR_SWITCH_FLOOR_FROM_ELEVATOR_TO_SAME_INTERIOR)
				SET_PLAYER_DATA_DOING_INTERIOR_WARP_FROM_CUTSCENE(TRUE)
				
				CLEAR_BIT(g_SimpleInteriorData.iSixthBS, BS6_SIMPLE_INTERIOR_SWITCH_FLOOR_FROM_ELEVATOR_TO_SAME_INTERIOR)
				
				TRIGGER_SWITCH_BETWEEN_SIMPLE_INTERIOR_INTERIORS(m_InteriorData.eSimpleInteriorID)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC MAINTAIN_VEHICLE_ZOOM()
	IF NOT IS_PLAYER_IN_ZOOMED_OUT_RADAR_OR_MAP(PLAYER_ID())
	AND IS_PLAYER_IN_CASINO_VALET_GARAGE(PLAYER_ID())
		SET_RADAR_ZOOM_PRECISE(1)
	ELSE
		SET_RADAR_ZOOM_PRECISE(0)
	ENDIF
ENDPROC

PROC MAINTAIN_ELEVATOR_DOORS()
	IF IS_VALID_INTERIOR(m_InteriorData.iInteriorID)
	AND IS_INTERIOR_READY(m_InteriorData.iInteriorID)
		INT i
		REPEAT 4 i
			IF DOES_ENTITY_EXIST(m_InteriorData.objElevatorDoor[i])
				IF GET_ROOM_KEY_FROM_ENTITY(m_InteriorData.objElevatorDoor[i]) != CASINO_VAL_GARAGE_ROOM_KEY
					FORCE_ROOM_FOR_ENTITY(m_InteriorData.objElevatorDoor[i], m_InteriorData.iInteriorID, CASINO_VAL_GARAGE_ROOM_KEY)
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

FUNC BOOL OK_TO_BLIP_ELEVATOR_TWO()

	#IF FEATURE_HEIST_ISLAND
	IF GB_GET_ISLAND_HEIST_PREP_PLAYER_IS_ON(PLAYER_ID()) = IHV_SAFE_CODES
		RETURN FALSE
	ENDIF
	#ENDIF

	RETURN TRUE
ENDFUNC

PROC MAINTAIN_CASINO_VALET_GARAGE_BLIPS()
	IF NOT IS_PLAYER_IN_ZOOMED_OUT_RADAR_OR_MAP(PLAYER_ID())
		ADD_BASIC_BLIP(m_InteriorData.blipElevator, <<1380.0728, 178.5524, -48.2886>>, RADAR_TRACE_ELEVATOR, "CAS_BLP_EL", 0.8, TRUE)
		IF OK_TO_BLIP_ELEVATOR_TWO()
			ADD_BASIC_BLIP(m_InteriorData.blipElevator2, <<1380.3077, 259.3023, -48.2891>>, RADAR_TRACE_ELEVATOR, "CAS_BLP_EL", 0.8, TRUE)
		ENDIF
	ELSE
		SAFE_REMOVE_BLIP(m_InteriorData.blipElevator)
		SAFE_REMOVE_BLIP(m_InteriorData.blipElevator2)
	ENDIF
ENDPROC

FUNC VECTOR GET_SAFE_WALK_TO_COORD(VECTOR vPlayerPos)
	IF vPlayerPos.x <= 1380
		vPlayerPos.x = 1373.0
	ELSE
		vPlayerPos.x = 1386.9
	ENDIF
	
	RETURN vPlayerPos
ENDFUNC

FUNC BOOL IS_PLAYER_CLEAR_OF_VEHICLE(VECTOR vPos)
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF ARE_VECTORS_ALMOST_EQUAL(vPos, GET_ENTITY_COORDS(PLAYER_PED_ID()))
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_GET_OUT_OF_WAY()
	PLAYER_INDEX PlayerID
		
	INT i
	INT iSpot = -1
	INT iSlowPlayerLoop
	
	VECTOR vPlayerPos
	VECTOR vCreationPos
	
	FLOAT fHeading, fDistance

	PlayerID = INT_TO_PLAYERINDEX(iSlowPlayerLoop)
	IF IS_BIT_SET(GlobalplayerBD[iSlowPlayerLoop].iBSTwo, BSTWO_PV_IS_WARPING_TO_CAR_PARK)
	AND IS_BIT_SET(GlobalServerBD.g_iBSCasinoParkingAssigned,iSlowPlayerLoop)
		IF IS_NET_PLAYER_OK(PlayerID,FALSE)
			REPEAT NUM_NETWORK_PLAYERS i
				IF iSpot = -1
					IF GlobalServerBD.g_iAssignedCasinoParkingSpots[i] = iSlowPlayerLoop
						iSpot = i
						i = NUM_NETWORK_PLAYERS
						PRINTLN("AM_MP_CASINO_VALET_GARAGE: MAINTAIN_GET_OUT_OF_WAY - player warping into spot #",iSpot)
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
	iSlowPlayerLoop++
	IF iSlowPlayerLoop >= NUM_NETWORK_PLAYERS
		iSlowPlayerLoop = 0
	ENDIF
	IF NOT bWalkingOutOfWay
		IF iSpot != -1
			IF IS_NET_PLAYER_OK(PLAYER_ID())
			AND DOES_ENTITY_EXIST(PLAYER_PED_ID())
			AND IS_SKYSWOOP_AT_GROUND()
				GET_CASINO_PARKING_SPOT_LOCATION(iSpot,vCreationPos, fHeading)
				vPlayerPos = GET_PLAYER_COORDS(PLAYER_ID())
				
				fDistance = GET_DISTANCE_BETWEEN_COORDS(vPlayerPos, vCreationPos, FALSE)				
				IF fDistance < 5
					NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_CLEAR_TASKS)
					vSafeCoords = GET_SAFE_WALK_TO_COORD(GET_ENTITY_COORDS(PLAYER_PED_ID()))
					TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), vSafeCoords, PEDMOVE_WALK, WALK_OUT_SAFETY_TIME)
					bWalkingOutOfWay = TRUE
					REINIT_NET_TIMER(getOutOfwayTimer,TRUE)
					PRINTLN("AM_MP_CASINO_VALET_GARAGE: MAINTAIN_GET_OUT_OF_WAY - Tasking to coords: <<", vSafeCoords.x, ", ", vSafeCoords.y, ", ", vSafeCoords.z, ">>")
				ELSE
					PRINTLN("AM_MP_CASINO_VALET_GARAGE: MAINTAIN_GET_OUT_OF_WAY - player not in the way of coords : ",vCreationPos," vPos = ",vPlayerPos)
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF HAS_NET_TIMER_EXPIRED(getOutOfwayTimer, WALK_OUT_SAFETY_TIME, TRUE)
		OR IS_PLAYER_CLEAR_OF_VEHICLE(vSafeCoords)
			IF IS_NET_PLAYER_OK(PLAYER_ID())
				IF NOT IS_PLAYER_CLEAR_OF_VEHICLE(vSafeCoords)
					IF GET_DISTANCE_BETWEEN_COORDS(vSafeCoords, GET_ENTITY_COORDS(PLAYER_PED_ID())) > 0.5
						SET_ENTITY_COORDS_NO_OFFSET(PLAYER_PED_ID(), vSafeCoords)
						PRINTLN("AM_MP_CASINO_VALET_GARAGE: MAINTAIN_GET_OUT_OF_WAY - Setting coords: <<", vSafeCoords.x, ", ", vSafeCoords.y, ", ", vSafeCoords.z, ">>")
					ENDIF
				ENDIF
			ENDIF
			RESET_NET_TIMER(getOutOfwayTimer)
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			bWalkingOutOfWay = FALSE
			vSafeCoords = <<0,0,0>>
			PRINTLN("AM_MP_CASINO_VALET_GARAGE: MAINTAIN_GET_OUT_OF_WAY player out of the way")
		ELSE
			IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) != PERFORMING_TASK
			AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) != WAITING_TO_START_TASK
				TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), vSafeCoords , PEDMOVE_WALK, WALK_OUT_SAFETY_TIME)
				
				PRINTLN("AM_MP_CASINO_VALET_GARAGE: MAINTAIN_GET_OUT_OF_WAY - Re-tasking to coords: <<", vSafeCoords.x, ", ", vSafeCoords.y, ", ", vSafeCoords.z, ">>")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_ELEVATOR_SAFETY_WARP()
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1378.805664,178.242950,-49.969391>>, <<1381.350952,178.244125,-47.993500>>, 2.5)
			SET_ENTITY_COORDS_NO_OFFSET(PLAYER_PED_ID(), <<1380.2441, 185.5523, -49.9955>>)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), 358.6089)
			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			SET_GAMEPLAY_CAM_RELATIVE_PITCH()
		ENDIF
		
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1381.318481,259.596588,-49.991581>>, <<1378.735962,259.593597,-47.993500>>, 2.5)
			SET_ENTITY_COORDS_NO_OFFSET(PLAYER_PED_ID(), <<1380.1345, 251.5347, -49.9955>>)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), 166.7005)
			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			SET_GAMEPLAY_CAM_RELATIVE_PITCH()
		ENDIF
	ENDIF
ENDPROC

PROC FORCE_ZERO_MASS_FOR_ALL_PLAYERS_IN_GARAGE()
	INT i
	PLAYER_INDEX playerID
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
			playerID = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
			IF IS_NET_PLAYER_OK(playerID)
				FORCE_ZERO_MASS_IN_COLLISIONS(GET_PLAYER_PED(playerID))
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════╡ MAIN LOGIC PROC ╞══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC RUN_MAIN_CLIENT_LOGIC()
	// NOTE: Only update procedures should be called from 
	// within RUN_MAIN_CLIENT_LOGIC. Custom logic should be 
	// placed inside of its own procedure and referenced here.
	
	MAINTAIN_ELEVATOR_SAFETY_WARP()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_ELEVATOR_SAFETY_WARP")
	#ENDIF	
	#ENDIF
	
	MAINTAIN_PLAYER_FLAGS()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_PERSONAL_CAR_MOD_NET_CUT_SCENE")
	#ENDIF	
	#ENDIF
	
	MAINTAIN_ELEVATOR_CUTSCENE()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_ELEVATOR_CUTSCENE")
	#ENDIF
	#ENDIF
	
	MAINTAIN_VEHICLE_ZOOM()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_VEHICLE_ZOOM")
	#ENDIF
	#ENDIF
	
	MAINTAIN_ELEVATOR_DOORS()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_ELEVATOR_DOORS")
	#ENDIF
	#ENDIF
	
	IF IS_CASINO_VAL_GARAGE_STATE(CASINO_VAL_GARAGE_STATE_LOADING)
		
		LOAD_INTERIOR()
	
	ELIF IS_CASINO_VAL_GARAGE_STATE(CASINO_VAL_GARAGE_STATE_IDLE)
		
		FORCE_ZERO_MASS_FOR_ALL_PLAYERS_IN_GARAGE()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("FORCE_ZERO_MASS_FOR_ALL_PLAYERS_IN_GARAGE")
		#ENDIF
		#ENDIF
		
		MAINTAIN_CASINO_VALET_GARAGE_BLIPS()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_CASINO_VALET_GARAGE_BLIPS")
		#ENDIF
		#ENDIF
		
		MAINTAIN_PERSONAL_VEHICLE_LOCKS()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_PERSONAL_VEHICLE_LOCKS")
		#ENDIF	
		#ENDIF
		
		MAINTAIN_CAN_DRIVE_OUT()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_CAN_DRIVE_OUT")
		#ENDIF	
		#ENDIF
		
		MAINTAIN_GET_OUT_OF_WAY()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_GET_OUT_OF_WAY")
		#ENDIF	
		#ENDIF

		IF NETWORK_IS_ACTIVITY_SESSION()
			
			//Only update logic for an activity session.
			
		ELSE
			
			IF NOT IS_BIT_SET(m_InteriorData.iBS, BS_CASINO_VAL_GARAGE_CALLED_CLEAR_HELP)
				
				IF NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
				AND IS_PLAYER_IN_CASINO(PLAYER_ID())
				//AND NOT SHOULD_KEEP_EXTERIOR_CAM_FOR_THIS_SIMPLE_INTERIOR(m_InteriorData.eSimpleInteriorID)
					IF IS_SCREEN_FADED_IN()
						CLEAR_HELP()
						SET_BIT(m_InteriorData.iBS, BS_CASINO_VAL_GARAGE_CALLED_CLEAR_HELP)
						PRINTLN("AM_MP_CASINO_VALET_GARAGE - Calling clear help")
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
	ENDIF
ENDPROC

PROC RUN_MAIN_SERVER_LOGIC()
	
	IF IS_CASINO_VAL_GARAGE_SERVER_STATE(CASINO_VAL_GARAGE_SERVER_STATE_LOADING)
		SET_CASINO_VAL_GARAGE_SERVER_STATE(CASINO_VAL_GARAGE_SERVER_STATE_IDLE)
	ELIF IS_CASINO_VAL_GARAGE_SERVER_STATE(CASINO_VAL_GARAGE_SERVER_STATE_IDLE)
		MAINTAIN_AMBIENT_VEHICLES()
	ENDIF
ENDPROC
#ENDIF // FEATURE_CASINO

SCRIPT #IF FEATURE_CASINO (SIMPLE_INTERIOR_CHILD_SCRIPT_DATA scriptData) #ENDIF
	
	#IF FEATURE_CASINO
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		SCRIPT_INITIALISE(scriptData)		
	ELSE
		SCRIPT_CLEANUP()
	ENDIF
	
	WHILE TRUE
		MP_LOOP_WAIT_ZERO()
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		SCRIPT_PROFILER_START_OF_FRAME() 
		#ENDIF
		#ENDIF
		
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_CASINO_VALET_GARAGE - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE = TRUE")
			#ENDIF
			SCRIPT_CLEANUP()
		ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE")
		#ENDIF	
		#ENDIF
		
		IF SHOULD_THIS_SIMPLE_INTERIOR_CHILD_THREAD_TERMINATE(scriptData.iScriptInstance, scriptData.eSimpleInteriorID)
			#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_CASINO_VALET_GARAGE - SHOULD_THIS_SIMPLE_INTERIOR_CHILD_THREAD_TERMINATE = TRUE")
			#ENDIF
			SCRIPT_CLEANUP()
		ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("SHOULD_THIS_SIMPLE_INTERIOR_CHILD_THREAD_TERMINATE")
		#ENDIF	
		#ENDIF
		
		IF NETWORK_GET_HOST_OF_THIS_SCRIPT() != INT_TO_NATIVE(PARTICIPANT_INDEX , -1)
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iSimpleInteriorHost = NATIVE_TO_INT(NETWORK_GET_PLAYER_INDEX(NETWORK_GET_HOST_OF_THIS_SCRIPT()))
		ENDIF
		
		IF IS_CASINO_VAL_GARAGE_SERVER_STATE(CASINO_VAL_GARAGE_SERVER_STATE_IDLE)
			RUN_MAIN_CLIENT_LOGIC()
		ENDIF		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("RUN_MAIN_CLIENT_LOGIC")
		#ENDIF	
		#ENDIF
		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			RUN_MAIN_SERVER_LOGIC()
		ENDIF			
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("RUN_MAIN_SERVER_LOGIC")
		#ENDIF	
		#ENDIF
		
		#IF IS_DEBUG_BUILD
//		UPDATE_DEBUG_WIDGETS()
		
		#IF SCRIPT_PROFILER_ACTIVE
		SCRIPT_PROFILER_END_OF_FRAME() 
		#ENDIF
		
		#ENDIF
		
	ENDWHILE
	#ENDIF	// FEATURE_CASINO
	
ENDSCRIPT
