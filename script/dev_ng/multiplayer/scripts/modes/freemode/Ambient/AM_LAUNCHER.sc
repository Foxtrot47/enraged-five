// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	AM_LAUNCHER.sc
//		AUTHOR			:	Martin McMillan
//		DESCRIPTION		:	A launcher for ambient scripts.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

//Timings
//Currently set to 12 mins between events

//----------------------
//	INCLUDES
//----------------------
USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_camera.sch"
USING "commands_pad.sch"
USING "commands_script.sch"
USING "flow_public_core_override.sch"
USING "net_wait_zero.sch"
USING "net_mission.sch"
USING "mp_skycam.sch"
USING "net_ambience_launcher.sch"
USING "fm_content_launching.sch"

//Mission specific includes
USING "net_joyrider.sch"
USING "net_GameDir.sch"


//----------------------
//	ENUM
//----------------------

//----------------------
//	CONSTANTS
//----------------------

CONST_INT ONE_HOUR						120000 // 2 mins, 1 ingame hour
CONST_INT CLIENT_CHECK_TIME				10000 // 10 Seconds
CONST_INT CLIENT_CHECK_TIME_SHORT		500 // 1/2 Second

CONST_INT SERVER_DELAYED_LAUNCH_TIME	120000	//REPLACE WITH TUNABLE

//Special Crate Drop
CONST_INT SPECIAL_CRATE_WAIT_1			360000	//6 mins, 3 ingame hours
CONST_INT SPECIAL_CRATE_WAIT_2			600000  //10 mins, 5 ingame hours
CONST_INT SPECIAL_CRATE_WAIT_3			1200000 //20 mins, 10 ingame hours

//----------------------
//	Local Bitset
//----------------------

INT iLocalBitSet
CONST_INT AML_BS_INITIAL_DONE	0
CONST_INT AML_BS_SHORT_DELAY	1

//----------------------
//	STRUCT
//----------------------



//----------------------
//	VARIABLES
//----------------------

//Debug Variables
#IF IS_DEBUG_BUILD
	
	CONST_INT NUM_PLAYLIST_EVENTS	6

	WIDGET_GROUP_ID widgetGroup
	BOOL bQuit
	BOOL bSkipTimeDelay
	BOOL bIgnoreHistory
	BOOL bIgnoreMinPlayers
	BOOL bSkipSPCcrateTimeDelay
	BOOL bDisableEvents
	BOOL bEnablePlaylist
	BOOL bSkipPlaylistTime
#ENDIF

MP_MISSION_DATA AM_MissionData

INT iReadyPlayers

SCRIPT_TIMER clientCheckTimer

//Security Van
INT iSpawnNodeCount = 40
VECTOR vVanSpawnLocation
FLOAT fSpawnHeading
INT iCheckCount

#IF IS_DEBUG_BUILD
PRINT_HOST_PLAYER_STRUCT phStruct
#ENDIF

//----------------------
//	GAME STATE
//----------------------

CONST_INT GAME_STATE_INIT 									0
CONST_INT GAME_STATE_RUNNING								1
CONST_INT GAME_STATE_TERMINATE_DELAY						2
CONST_INT GAME_STATE_END									3

//----------------------
//	BROADCAST VARIABLES
//----------------------

// Everyone can read this data, only the server can update it

//iServerBitSet

//CONST_INT SERVER_BS_CLEANUP_DONE		0
CONST_INT SERVER_BS_SKIP_CLIENT_CHECKS	1

//Crate Drop
CONST_INT SERVER_BS_SCD_TIMER_RESET		3
CONST_INT SERVER_BS_DO_SPC_CD			4

// The client broadcast data.
// Everyone can read this data, only the local player only the server can update it
STRUCT PlayerBroadcastData
	
	INT iClientGameState = GAME_STATE_INIT
	
	INT iClientBitSet
	AML_RUN_STAGE eAML_Stage = AML_AWAITING_START
	
ENDSTRUCT

PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]

//iClientBitSet
CONST_INT CLIENT_BS_REQUESTED_LAUNCH	0
CONST_INT CLIENT_BS_FAILED_LAUNCH		1
CONST_INT CLIENT_BS_LAUNCHING			2
CONST_INT CLIENT_BS_RESET_CHECKS		3
CONST_INT CLIENT_BS_DONT_COUNT		4

FUNC BOOL IS_SERVER_BIT_SET(INT iBit)
	RETURN IS_BIT_SET(g_AML_serverBD.iServerBitSet,iBit)
ENDFUNC

PROC SET_SERVER_BIT(INT iBit)
	IF NOT IS_BIT_SET(g_AML_serverBD.iServerBitSet,iBit)
		SET_BIT(g_AML_serverBD.iServerBitSet,iBit)
		PRINTLN("[AM_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] SET_SERVER_BIT - ",iBit)
	ENDIF
ENDPROC

PROC CLEAR_SERVER_BIT(INT iBit)
	IF IS_BIT_SET(g_AML_serverBD.iServerBitSet,iBit)
		CLEAR_BIT(g_AML_serverBD.iServerBitSet,iBit)
		PRINTLN("[AM_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] CLEAR_SERVER_BIT - ",iBit)
	ENDIF
ENDPROC

FUNC BOOL IS_CLIENT_BIT_SET(PLAYER_INDEX playerID, INT iBit)
	RETURN IS_BIT_SET(playerBD[NATIVE_TO_INT(playerID)].iClientBitSet,iBit)
ENDFUNC

PROC SET_CLIENT_BIT(INT iBit)
	IF NOT IS_BIT_SET(playerBD[NETWORK_PLAYER_ID_TO_INT()].iClientBitSet,iBit)
		SET_BIT(playerBD[NETWORK_PLAYER_ID_TO_INT()].iClientBitSet,iBit)
		PRINTLN("[AM_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] SET_CLIENT_BIT - ",iBit)
	ENDIF
ENDPROC

PROC CLEAR_CLIENT_BIT(INT iBit)
	IF IS_BIT_SET(playerBD[NETWORK_PLAYER_ID_TO_INT()].iClientBitSet,iBit)
		CLEAR_BIT(playerBD[NETWORK_PLAYER_ID_TO_INT()].iClientBitSet,iBit)
		PRINTLN("[AM_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] CLEAR_CLIENT_BIT - ",iBit)
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
//----------------------
//	DBG FUNCTIONS
//----------------------

/// PURPOSE:
/// 	Initializes Debug Widgets
PROC SETUP_DEBUG_WIDGETS()
	INT i

	MPGlobalsAmbience.iChallengesChallenge = -1

	IF NOT GET_COMMANDLINE_PARAM_EXISTS("sc_AutoProfileMissionController")
		SET_PROFILING_OF_THIS_SCRIPT(TRUE)
	ENDIF
	widgetGroup = START_WIDGET_GROUP("AM_LAUNCHER")
		ADD_WIDGET_STRING("Currently Running: ")
		ADD_WIDGET_STRING(GET_MP_MISSION_NAME(g_AML_serverBD.AM_MissionToLaunch.missionScript))
		ADD_WIDGET_BOOL("Skip time delay",bSkipTimeDelay)
		ADD_WIDGET_BOOL("Ignore history list",bIgnoreHistory)
		ADD_WIDGET_BOOL("Ignore min players check",bIgnoreMinPlayers)
		ADD_WIDGET_BOOL("Disable All Events",bDisableEvents)
		ADD_WIDGET_BOOL("Enable Playlist",bEnablePlaylist)
		ADD_WIDGET_BOOL("Skip Playlist Time",bSkipPlaylistTime)
		ADD_WIDGET_BOOL("Quit Script", bQuit)
		
		ADD_WIDGET_BOOL("Skip SPC crate drop time delay",bSkipSPCcrateTimeDelay)
				
		REPEAT AML_SCRIPT_MAX i
			STRING scriptName = GET_AML_MISSION_NAME(INT_TO_ENUM(AML_SCRIPT_TYPE,i))
			ADD_WIDGET_BOOL(scriptName,g_AML_serverBD.launcherScripts[i].bDebugActive)
			IF INT_TO_ENUM(AML_SCRIPT_TYPE,i) = AML_SCRIPT_CHALLENGES
				ADD_WIDGET_INT_SLIDER("Set Challenge",MPGlobalsAmbience.iChallengesChallenge,-1,18,1)
				ADD_WIDGET_INT_SLIDER("Set Challenge Warning Time",MPGlobalsAmbience.iChallengeWarningTime,0,240000,10000)
			ENDIF
			ADD_WIDGET_FLOAT_SLIDER(scriptName,g_AML_serverBD.launcherScripts[i].fLaunchWeighting,0,3,0.1)
		ENDREPEAT	

	STOP_WIDGET_GROUP()
ENDPROC

/// PURPOSE:
///		Removes Debug Widgets
PROC CLEANUP_DEBUG_WIDGETS()
	IF DOES_WIDGET_GROUP_EXIST(widgetGroup)
		DELETE_WIDGET_GROUP(widgetGroup)
	ENDIF
ENDPROC 

PROC UPDATE_F9_VALUES()

	MPGlobalsAmbience.iAML_ServerBitSet = g_AML_serverBD.iServerBitSet
	
	IF g_AML_serverBD.AM_MissionToLaunch.scriptType != AML_SCRIPT_NULL
	AND g_AML_serverBD.eAML_Stage != AML_AWAITING_START
	AND g_AML_serverBD.eAML_Stage != AML_SET_WEIGHTINGS
	AND g_AML_serverBD.eAML_Stage != AML_GET_NEXT_EVENT
		MPGlobalsAmbience.sAML_CurrentlyRunning = GET_AML_MISSION_NAME(g_AML_serverBD.AM_MissionToLaunch.scriptType)
	ELSE
		MPGlobalsAmbience.sAML_CurrentlyRunning = ""
	ENDIF
	
	INT iTemp
	
	IF g_sMPTunables.inumber_of_scripts_in_history_list > 0 AND g_sMPTunables.inumber_of_scripts_in_history_list <= ENUM_TO_INT(AML_SCRIPT_MAX)
		REPEAT g_sMPTunables.inumber_of_scripts_in_history_list iTemp
			MPGlobalsAmbience.sAML_EventHistory[iTemp] = GET_AML_MISSION_NAME(g_AML_serverBD.eventHistory[iTemp])
		ENDREPEAT
	ENDIF
	
	IF bDisableEvents
		INT i
		REPEAT AML_SCRIPT_MAX i
			g_AML_serverBD.launcherScripts[i].bDebugActive = FALSE
		ENDREPEAT
		bDisableEvents = FALSE
	ENDIF
	
//	INT iLauncherTime = g_AML_serverBD.iLaunchTime - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(g_AML_serverBD.timeToLaunch)
//	PRINTLN("[AM_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] iLauncherTime = ",iLauncherTime)
	
ENDPROC


/// PURPOSE:
///    For launching the Lowrider ambient missions from the 'm' mission menu.
PROC MAINTAIN_LOWRIDER_EVENT_MISSION_MENU_LAUNCH()

	INT iScriptCount, iScript
	
	REPEAT ENUM_TO_INT(AML_SCRIPT_MAX) iScript
	
		IF IS_BIT_SET(MPGlobalsAmbience.iEventLaunchBS,iScript)
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				bSkipTimeDelay = TRUE
				bIgnoreHistory = TRUE
				bIgnoreMinPlayers = TRUE
				
				REPEAT AML_SCRIPT_MAX iScriptCount
					IF INT_TO_ENUM(AML_SCRIPT_TYPE,iScriptCount) <> INT_TO_ENUM(AML_SCRIPT_TYPE,iScript)
						g_AML_serverBD.launcherScripts[iScriptCount].bDebugActive = FALSE
					ELSE
						g_AML_serverBD.launcherScripts[iScriptCount].bDebugActive = TRUE
					ENDIF
				ENDREPEAT
				
				CLEAR_BIT(MPGlobalsAmbience.iEventLaunchBS,iScript)
				g_AML_serverBD.bLaunchedFromMissionMenu = TRUE
			ELSE
				CLEAR_BIT(MPGlobalsAmbience.iEventLaunchBS,iScript)
			ENDIF
		ENDIF
	
	ENDREPEAT
		
ENDPROC

PROC SET_AMP_STAGE(AML_PLAYLIST_STAGE eAMP_Stage)
	IF g_AML_serverBD.eAMP_Stage != eAMP_Stage
		g_AML_serverBD.eAMP_Stage = eAMP_Stage
		PRINTLN("[AM_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] Server Playlist stage = ",GET_AMP_STAGE_STRING(eAMP_Stage))
	ENDIF
ENDPROC

FUNC BOOL LAUNCH_PLAYLIST_EVENT(INT iEvent)

	SWITCH(iEvent)
	
		CASE 0
			BROADCAST_DEBUG_LAUNCH_PENNED_IN(-1,ALL_PLAYERS())
		BREAK
	
		CASE 1
			BROADCAST_DEBUG_LAUNCH_CHALLENGES(ENUM_TO_INT(AM_CHALLENGE_LONGEST_WHEELIE),ALL_PLAYERS())
		BREAK
		
		CASE 2
			BROADCAST_DEBUG_LAUNCH_KING_OF_THE_CASTLE(-1,ALL_PLAYERS())
		BREAK
		
		CASE 3
			BROADCAST_DEBUG_LAUNCH_CHALLENGES(ENUM_TO_INT(AM_CHALLENGE_LONGEST_SKYDIVE),ALL_PLAYERS())
		BREAK

		CASE 4
			BROADCAST_DEBUG_LAUNCH_URBAN_WARFARE(-1,-1,TRUE,ALL_PLAYERS())
		BREAK
				
		CASE 5
			BROADCAST_DEBUG_LAUNCH_CHALLENGES(ENUM_TO_INT(AM_CHALLENGE_PVP_HEADSHOTS),ALL_PLAYERS())
		BREAK
				
		DEFAULT
			PRINTLN("[AM_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] LAUNCH_PLAYLIST_EVENT - No Event Found!!")
			RETURN FALSE
		BREAK
		
	ENDSWITCH

	PRINTLN("[AM_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] LAUNCH_PLAYLIST_EVENT - ",iEvent)
	RETURN TRUE

ENDFUNC

PROC PROCESS_EVENT_PLAYLIST()

	IF bEnablePlaylist
	OR g_sMPTunables.bEnableEventPlaylist
		SWITCH(g_AML_serverBD.eAMP_Stage)

			CASE AMP_INIT

				bDisableEvents = TRUE
				bIgnoreMinPlayers = TRUE
				g_AML_serverBD.iCurrentPlaylistEvent = 0
				
				SET_AMP_STAGE(AMP_LAUNCH)
			BREAK
			
			CASE AMP_LAUNCH
			
				IF LAUNCH_PLAYLIST_EVENT(g_AML_serverBD.iCurrentPlaylistEvent)
					RESET_NET_TIMER(g_AML_serverBD.playlistTimer)
					SET_AMP_STAGE(AMP_RUN)
				ENDIF
				g_AML_serverBD.iCurrentPlaylistEvent++
	
			BREAK
			
			CASE AMP_RUN
				//Do nothing
			BREAK

			CASE AMP_END
				IF g_AML_serverBD.iCurrentPlaylistEvent >= NUM_PLAYLIST_EVENTS
					SET_AMP_STAGE(AMP_INIT)
				ELSE	
					IF HAS_NET_TIMER_EXPIRED(g_AML_serverBD.playlistTimer,GET_PLAYLIST_WAIT_TIME(TRUE))
					#IF IS_DEBUG_BUILD
					OR bSkipPlaylistTime
					#ENDIF
						SET_AMP_STAGE(AMP_LAUNCH)
					ENDIF
				ENDIF
			BREAK

		ENDSWITCH
	ENDIF

ENDPROC

 // FEATURE_NEW_AMBIENT_EVENTS

#ENDIF

//----------------------
//	FUNCTIONS
//----------------------

FUNC MP_MISSION GET_AML_MISSION(AML_SCRIPT_TYPE amlType)
	
	SWITCH(amlType)
		CASE AML_SCRIPT_JOYRIDER				RETURN eAM_JOYRIDER
		CASE AML_SCRIPT_CRATEDROP				RETURN eAM_CRATE_DROP
		CASE AML_SCRIPT_IMPORTEXPORT			RETURN eAM_FM_IMP_EXP
		CASE AML_SCRIPT_LESTERKILLTARGET		RETURN eAM_SMALL_MISSION
		CASE AML_SCRIPT_PLANETAKEDOWN			RETURN eAM_PLANE_TAKEDOWN
		CASE AML_SCRIPT_DISTRACTCOPS			RETURN eAM_DISTRACT_COPS
		CASE AML_SCRIPT_DESTROYVEHICLE			RETURN eAM_DESTROY_VEH
		CASE AML_SCRIPT_CPCOLLECTION			RETURN eAM_CP_COLLECTION
		CASE AML_SCRIPT_CHALLENGES				RETURN eAM_CHALLENGES
		CASE AML_SCRIPT_HOT_TARGET				RETURN eAM_HOT_TARGET
		CASE AML_SCRIPT_URBAN_WARFARE			RETURN eAM_KILL_LIST
		CASE AML_SCRIPT_PENNED_IN				RETURN eAM_PENNED_IN		
		CASE AML_SCRIPT_PASS_THE_PARCEL			RETURN eAM_PASS_THE_PARCEL
		CASE AML_SCRIPT_HOT_PROPERTY			RETURN eAM_HOT_PROPERTY
		CASE AML_SCRIPT_DEAD_DROP				RETURN eAM_DEAD_DROP
		CASE AML_SCRIPT_KING_OF_THE_CASTLE		RETURN eAM_KING_OF_THE_CASTLE
		CASE AML_SCRIPT_CRIMINAL_DAMAGE			RETURN eAM_CRIMINAL_DAMAGE
		CASE AML_SCRIPT_HUNT_THE_BEAST			RETURN eAM_HUNT_THE_BEAST
		CASE AML_SCRIPT_BUSINESS_BATTLES    	RETURN eAM_BUSINESS_BATTLES
		CASE AML_SCRIPT_FMC_BUSINESS_BATTLES    RETURN eAM_FMC_BUSINESS_BATTLES
		#IF FEATURE_COPS_N_CROOKS
		CASE AML_SCRIPT_SERVE_AND_PROTECT    	RETURN eAM_SERVE_AND_PROTECT
		#ENDIF
	ENDSWITCH
	
	RETURN eNULL_MISSION
	
ENDFUNC

//Set the weighting of an event
PROC SET_SCRIPT_LAUNCH_WEIGHTING(AML_SCRIPT_TYPE scriptIndex,FLOAT fWeighting)

	PRINTLN("[AM_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] - WEIGHTINGS - [",GET_AML_MISSION_NAME(scriptIndex),"] - ", fWeighting)

	IF scriptIndex != AML_SCRIPT_NULL
		g_AML_serverBD.launcherScripts[scriptIndex].fLaunchWeighting = fWeighting
	ELSE
		SCRIPT_ASSERT("=== AM_LAUNCHER === SET_SCRIPT_LAUNCH_WEIGHTING - scriptIndex = AML_SCRIPT_NULL")
		PRINTLN("[AM_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] SET_SCRIPT_LAUNCH_WEIGHTING - scriptIndex = AML_SCRIPT_NULL")
	ENDIF
ENDPROC

//Helper proc for setting server stage
PROC SET_SERVER_AML_STAGE(AML_RUN_STAGE stage)
	g_AML_serverBD.eAML_Stage = stage
	PRINTLN("[AM_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] SET_SERVER_AML_STAGE - ",GET_AML_STAGE_STRING(stage))
ENDPROC

//Helper proc for setting client stage
PROC SET_CLIENT_AML_STAGE(AML_RUN_STAGE stage)
	playerBD[NETWORK_PLAYER_ID_TO_INT()].eAML_Stage = stage
	PRINTLN("[AM_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] SET_CLIENT_AML_STAGE - ",GET_AML_STAGE_STRING(stage))
ENDPROC

FUNC INT GET_MISSION_TO_LAUNCH()
	RETURN Convert_MissionID_To_FM_Mission_Type(g_AML_serverBD.AM_MissionToLaunch.missionScript)
ENDFUNC

FUNC INT CHECK_ENOUGH_PLAYERS_FOR_SCRIPT(AML_SCRIPT launchingScript)
	INT iTemp
	iReadyPlayers = 0

	REPEAT NUM_NETWORK_REAL_PLAYERS() iTemp
		IF NETWORK_IS_PLAYER_ACTIVE(INT_TO_PLAYERINDEX(iTemp))
		AND IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(iTemp))
			IF IS_BIT_SET(playerBD[iTemp].iClientBitSet,CLIENT_BS_REQUESTED_LAUNCH)
			
				IF NOT IS_BIT_SET(playerBD[iTemp].iClientBitSet,CLIENT_BS_DONT_COUNT)
					iReadyPlayers++
				ENDIF
				
				PRINTLN("[AM_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] CHECK_ENOUGH_PLAYERS_FOR_SCRIPT - CLIENT_BS_REQUESTED_LAUNCH - ",GET_PLAYER_NAME(INT_TO_PLAYERINDEX(iTemp)))
				
				IF launchingScript.missionScript = eAM_CRATE_DROP
					IF NETWORK_GET_NUM_PARTICIPANTS() >= GET_AML_MISSION_MIN_PLAYERS(INT_TO_ENUM(AML_SCRIPT_TYPE,launchingScript.scriptType))
					#IF IS_DEBUG_BUILD 
					OR bIgnoreMinPlayers 
					OR GET_COMMANDLINE_PARAM_EXISTS("sc_IgnoreMinPlayersToLaunchEvents")
					#ENDIF
						RETURN 1
					ELSE
						RETURN 2
					ENDIF
				ENDIF
				
				//Check if enough players
				IF iReadyPlayers >= GET_AML_MISSION_MIN_PLAYERS(INT_TO_ENUM(AML_SCRIPT_TYPE,launchingScript.scriptType))
				#IF IS_DEBUG_BUILD 
				OR bIgnoreMinPlayers 
				OR GET_COMMANDLINE_PARAM_EXISTS("sc_IgnoreMinPlayersToLaunchEvents")
				#ENDIF
					PRINTLN("[AM_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] CHECK_ENOUGH_PLAYERS_FOR_SCRIPT - iReadyPlayers >= launchingScript.iMinPlayers ",iReadyPlayers," >= ",GET_AML_MISSION_MIN_PLAYERS(INT_TO_ENUM(AML_SCRIPT_TYPE,launchingScript.scriptType)) )

					RETURN 1
				ENDIF
				
			ELIF IS_BIT_SET(playerBD[iTemp].iClientBitSet,CLIENT_BS_FAILED_LAUNCH)
				PRINTLN("[AM_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] CHECK_ENOUGH_PLAYERS_FOR_SCRIPT - CLIENT_BS_FAILED_LAUNCH - ",GET_PLAYER_NAME(INT_TO_PLAYERINDEX(iTemp)))
				
			ELIF NOT(IS_BIT_SET(playerBD[iTemp].iClientBitSet,CLIENT_BS_FAILED_LAUNCH))
				RETURN 0
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN 2
	
ENDFUNC

FUNC INT GET_EVENT_INDEX_FROM_WEIGHTINGS()
	INT iTemp
	FLOAT iTotalWeight,iCurrentRange
	
	REPEAT AML_SCRIPT_MAX iTemp
		iTotalWeight += g_AML_serverBD.launcherScripts[iTemp].fLaunchWeighting
	ENDREPEAT
	
	FLOAT iSelection = GET_RANDOM_FLOAT_IN_RANGE(0,iTotalWeight)
	
	REPEAT AML_SCRIPT_MAX iTemp
		iCurrentRange += g_AML_serverBD.launcherScripts[iTemp].fLaunchWeighting
		IF iSelection < iCurrentRange
			RETURN iTemp
		ENDIF
	ENDREPEAT

	RETURN -1
	
ENDFUNC

/// PURPOSE:
///    Finds coords to spawn the security van at.
/// RETURNS:
///    BOOL - TRUE if found cords, FALSE if not. 
FUNC INT FIND_VAN_SPAWN_COORDS()
	
	BOOL bFoundSuitableNode
	INT iFindStage = 0
	INT iNumLanes
	INT iDensity
	INT iPropertyFlags
	VECTOR vPlayerPedCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
	
	
	IF NOT bFoundSuitableNode
		IF GET_NTH_CLOSEST_VEHICLE_NODE_WITH_HEADING(vPlayerPedCoords, iSpawnNodeCount, vVanSpawnLocation, fSpawnHeading, iNumLanes, NF_NONE)
			IF GET_DISTANCE_BETWEEN_COORDS(<<-1367.5571, -3220.5977, 12.9448>>, vPlayerPedCoords) >= 600.0 // Don't spawn in aeroplane hangars or runways.
				IF GET_DISTANCE_BETWEEN_COORDS(vPlayerPedCoords, vVanSpawnLocation) >= 75.0
					REQUEST_PATH_NODES_IN_AREA_THIS_FRAME(vPlayerPedCoords.x-250.0, vPlayerPedCoords.y-250.0, vPlayerPedCoords.x+250.0, vPlayerPedCoords.y+250.0)
					IF GET_VEHICLE_NODE_PROPERTIES(vVanSpawnLocation, iDensity, iPropertyFlags)
						IF ((iPropertyFlags & ENUM_TO_INT(VNP_OFF_ROAD)) = 0)
							PRINTLN("[AM_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] SECURITY VAN - ((iPropertyFlags & ENUM_TO_INT(VNP_OFF_ROAD)) = 0)")
							IF iCheckCount <= 0
								IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(vVanSpawnLocation)
									PRINTLN("[Armoured Van] [Server] - found suitable node to spawn van, spawning. iSpawnNodeCount = ", iSpawnNodeCount)
									bFoundSuitableNode = TRUE
									iFindStage = 1
								ENDIF
							ELSE
								bFoundSuitableNode = TRUE
							ENDIF
						ENDIF
					ENDIF
				ELSE
					PRINTLN("[Armoured Van] [Server] - spawn coords < 75.0m away, node cannot be used.")
					PRINTLN("[Armoured Van] [Server] - iSpawnNodeCount = ", iSpawnNodeCount)
					PRINTLN("[Armoured Van] [Server] - vPlayerPedCoords = ", vPlayerPedCoords)
					PRINTLN("[Armoured Van] [Server] - vVanSpawnLocation = ", vVanSpawnLocation)
					PRINTLN("[Armoured Van] [Server] - difference = ", GET_DISTANCE_BETWEEN_COORDS(vPlayerPedCoords, vVanSpawnLocation))
				ENDIF
			ELSE
				PRINTLN("[Armoured Van] [Server] - spawn coords <= 600.0m from airport plane area.")NET_NL()
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT bFoundSuitableNode
		PRINTLN("[AM_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] SECURITY VAN - NOT bFoundSuitableNode")
		iSpawnNodeCount += 3
		IF iSpawnNodeCount >= 150
			PRINTLN("[Armoured Van] [Server] going back to 40, iSpawnNodeCount = ", iSpawnNodeCount)
			iSpawnNodeCount = 40
			iCheckCount++
			PRINTLN("[AM_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] SECURITY VAN - iCheckCount++ ",iCheckCount)
		ENDIF
	ENDIF
	
	IF iCheckCount > 0
		PRINTLN("[Armoured Van] [Server] - could not find node that is ok for net entity creation after 10 checks")
		iFindStage = 2
	ENDIF
	
	RETURN iFindStage
	
ENDFUNC

PROC CHECK_FOR_SPECIAL_CRATE_DROP
		//Check if this should be a Special Crate Drop
		BROADCAST_AML_RUN_SPC_CRATE(SPECIFIC_PLAYER(NETWORK_GET_HOST_OF_SCRIPT(FREEMODE_SCRIPT())),FALSE)
		PRINTLN("[AM_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] CRATE DROP - SPECIAL CRATE CHECK -  g_sMPTunables.iCrateDropSpecial_crate_drop_percentage = ",  g_sMPTunables.iCrateDropSpecial_crate_drop_percentage)
		IF GET_RANDOM_INT_IN_RANGE(0, 100) < g_sMPTunables.iCrateDropSpecial_crate_drop_percentage
			PRINTLN("   >>>   CRATE DROP - SPECIAL CRATE CHECK - NETWORK_GET_NUM_PARTICIPANTS() = ", NETWORK_GET_NUM_PARTICIPANTS(), " REQUIRED = ", g_sMPTunables.iCrateDropSpecial_min_players)
			IF NETWORK_GET_NUM_PARTICIPANTS() >= g_sMPTunables.iCrateDropSpecial_min_players
				BROADCAST_AML_RUN_SPC_CRATE(SPECIFIC_PLAYER(NETWORK_GET_HOST_OF_SCRIPT(FREEMODE_SCRIPT())),TRUE)
				SET_SERVER_BIT(SERVER_BS_DO_SPC_CD)
			ELSE
				PRINTLN("[AM_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] CRATE DROP - SPECIAL CRATE CHECK - FAILED - NUM MIN PLAYERS CHECK")
			ENDIF
		ELSE
			PRINTLN("[AM_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] CRATE DROP - SPECIAL CRATE CHECK - FAILED - PERCENTAGE CHECK - TUNABLE = ", g_sMPTunables.iCrateDropSpecial_crate_drop_percentage)
		ENDIF		
ENDPROC

//PURPOSE: Displays lead up messages for a Special Crate Drop
PROC PROCESS_SPECIAL_CRATE_DROP_MESSAGES_CLIENT()
	PRINTLN("[AM_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] PROCESS_SPECIAL_CRATE_DROP_MESSAGES_CLIENT")
	IF GlobalserverBD_BlockB.CrateDropServerLaunchData.bUseSpecialCrate = TRUE
		PRINTLN("[AM_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] Globalg_AML_serverBD_BlockB.CrateDropServerLaunchData.bUseSpecialCrate = TRUE) ")
		IF NOT IS_BIT_SET(MPGlobalsAmbience.CrateDropLaunchData.iBitSet, biCDL_SpecialCrateTicker1)
			//3 hours OR less
			PRINTLN("[AM_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] SPC TIMER - ",(SPECIAL_CRATE_WAIT_3-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(g_AML_serverBD.specialCrateCountdown))," < ",SPECIAL_CRATE_WAIT_1)
			IF ((g_sMPTunables.iCrateDropSpecialFrequency * ONE_HOUR -GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(g_AML_serverBD.specialCrateCountdown)) < SPECIAL_CRATE_WAIT_1)
				IF NOT IS_BIT_SET(MPGlobalsAmbience.CrateDropLaunchData.iBitSet, biCDL_SpecialCrateTicker1)
					IF IS_FM_TYPE_UNLOCKED(FMMC_TYPE_CRATE_DROP)
					AND NOT IS_TRANSITION_ACTIVE()
						IF g_sMPTunables.iCrateDropSpecialFrequency < SPECIAL_CRATE_WAIT_1 / ONE_HOUR
							PRINT_TICKER_WITH_INT("ACD_TSCH", g_sMPTunables.iCrateDropSpecialFrequency)
							NET_PRINT_TIME() NET_PRINT("     ----->   CRATE DROP - PROCESS_SPECIAL_CRATE_DROP_MESSAGES_CLIENT - g_sMPTunables.iCrateDropSpecialFrequency = ") NET_PRINT_INT(g_sMPTunables.iCrateDropSpecialFrequency) NET_NL()
						ELSE
							PRINT_TICKER_WITH_INT("ACD_TSCH", SPECIAL_CRATE_WAIT_1/ ONE_HOUR)
							NET_PRINT_TIME() NET_PRINT("     ----->   CRATE DROP - PROCESS_SPECIAL_CRATE_DROP_MESSAGES_CLIENT - 3 hours   <-----     ") NET_NL()
						ENDIF
					ENDIF
					SET_BIT(MPGlobalsAmbience.CrateDropLaunchData.iBitSet, biCDL_SpecialCrateTicker1)
				ENDIF
			//5 hours
			ELIF ((g_sMPTunables.iCrateDropSpecialFrequency * ONE_HOUR -GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(g_AML_serverBD.specialCrateCountdown)) < SPECIAL_CRATE_WAIT_2)
				IF NOT IS_BIT_SET(MPGlobalsAmbience.CrateDropLaunchData.iBitSet, biCDL_SpecialCrateTicker2)
					IF IS_FM_TYPE_UNLOCKED(FMMC_TYPE_CRATE_DROP)
					AND NOT IS_TRANSITION_ACTIVE()
						PRINT_TICKER_WITH_INT("ACD_TSCH", SPECIAL_CRATE_WAIT_2/ ONE_HOUR)
						NET_PRINT_TIME() NET_PRINT("     ----->   CRATE DROP - PROCESS_SPEICAL_CRATE_DROP_MESSAGES_CLIENT - 5 hours  <-----     ") NET_NL()
					ENDIF
					SET_BIT(MPGlobalsAmbience.CrateDropLaunchData.iBitSet, biCDL_SpecialCrateTicker2)
				ENDIF
			//10 hours
			ELIF ((g_sMPTunables.iCrateDropSpecialFrequency * ONE_HOUR -GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(g_AML_serverBD.specialCrateCountdown)) < SPECIAL_CRATE_WAIT_3)
				IF NOT IS_BIT_SET(MPGlobalsAmbience.CrateDropLaunchData.iBitSet, biCDL_SpecialCrateTicker3)
					IF IS_FM_TYPE_UNLOCKED(FMMC_TYPE_CRATE_DROP)
					AND NOT IS_TRANSITION_ACTIVE()
						PRINT_TICKER_WITH_INT("ACD_TSCH", SPECIAL_CRATE_WAIT_3/ ONE_HOUR)
						NET_PRINT_TIME() NET_PRINT("     ----->   CRATE DROP - PROCESS_SPEICAL_CRATE_DROP_MESSAGES_CLIENT - 10 hours  <-----     ") NET_NL()
					ENDIF
					SET_BIT(MPGlobalsAmbience.CrateDropLaunchData.iBitSet, biCDL_SpecialCrateTicker3)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


PROC ADD_TO_HISTORY(AML_SCRIPT_TYPE amlType)
	INT iTemp
	
	IF amlType != AML_SCRIPT_CHALLENGES
	AND NOT IS_AML_EVENT_A_BUSINESS_BATTLE(amlType)
	#IF FEATURE_COPS_N_CROOKS
	AND amlType != AML_SCRIPT_SERVE_AND_PROTECT
	#ENDIF
		IF g_sMPTunables.inumber_of_scripts_in_history_list > 0 AND g_sMPTunables.inumber_of_scripts_in_history_list <= ENUM_TO_INT(AML_SCRIPT_MAX)
			IF NOT(g_AML_serverBD.eventHistory[g_sMPTunables.inumber_of_scripts_in_history_list-1] = AML_SCRIPT_NULL)
				REPEAT g_sMPTunables.inumber_of_scripts_in_history_list -1 iTemp
					g_AML_serverBD.eventHistory[iTemp] = g_AML_serverBD.eventHistory[iTemp+1]
					//g_AML_serverBD.iEventHistoryVaration[iTemp] = g_AML_serverBD.iEventHistoryVaration[iTemp+1]
				ENDREPEAT
			ENDIF
			
			g_AML_serverBD.eventHistory[g_sMPTunables.inumber_of_scripts_in_history_list-1] = amlType
			//g_AML_serverBD.iEventHistoryVaration[g_sMPTunables.inumber_of_scripts_in_history_list-1] = -1
		ENDIF
	ENDIF
	
	//Move the array up one
	FOR iTemp = (ciMAX_EVENT_HISTORY_LIST - 2) TO 0 STEP -1
		g_AML_serverBD.sHistory[iTemp+1] = g_AML_serverBD.sHistory[iTemp]
		#IF IS_DEBUG_BUILD 
		IF g_AML_serverBD.sHistory[iTemp+1].event != AML_SCRIPT_NULL
		PRINTLN("[AM_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] - ADD_TO_HISTORY g_AML_serverBD.sHistory[", iTemp+1, "].event     = ", GET_AML_SCRIPT_TYPE_DEBUG_PRINT(g_AML_serverBD.sHistory[iTemp+1].event),  " - g_AML_serverBD.sHistory[", iTemp+1, "].iVaration = ", g_AML_serverBD.sHistory[iTemp+1].iVaration)
		ENDIF
		#ENDIF
	ENDFOR
	//put the new mission in the empty array
	g_AML_serverBD.sHistory[0].event = amlType
	g_AML_serverBD.sHistory[0].iVaration = -1

	g_AML_serverBD.ePreviousEvent = amlType
	
	PRINTLN("[AM_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] ADD_TO_HISTORY - ePreviousEvent = ",GET_AML_MISSION_NAME(g_AML_serverBD.ePreviousEvent))
	
ENDPROC

FUNC BOOL IS_EVENT_IN_HISTORY(AML_SCRIPT_TYPE amlType)
	INT iTemp
	
	#IF IS_DEBUG_BUILD
	IF bIgnoreHistory
		RETURN FALSE
	ENDIF
	#ENDIF
	
	IF amlType = AML_SCRIPT_CHALLENGES
	OR IS_AML_EVENT_A_BUSINESS_BATTLE(amlType)
	#IF FEATURE_COPS_N_CROOKS
	OR amlType = AML_SCRIPT_SERVE_AND_PROTECT
	#ENDIF
		RETURN FALSE
	ENDIF
	
	IF g_sMPTunables.inumber_of_scripts_in_history_list > 0 AND g_sMPTunables.inumber_of_scripts_in_history_list <= ENUM_TO_INT(AML_SCRIPT_MAX)
		IF NOT(g_AML_serverBD.eventHistory[g_sMPTunables.inumber_of_scripts_in_history_list -1] = AML_SCRIPT_NULL)
			REPEAT g_sMPTunables.inumber_of_scripts_in_history_list  iTemp
				IF g_AML_serverBD.eventHistory[iTemp] = amlType
					RETURN TRUE
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_EVENT_SCRIPT_ALREADY_RUNNING(AML_SCRIPT_TYPE amlType)

	IF amlType = AML_SCRIPT_PENNED_IN	
		IF NETWORK_IS_SCRIPT_ACTIVE("AM_PENNED_IN")
			PRINTLN("[AM_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] IS_EVENT_SCRIPT_ALREADY_RUNNING")
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE

ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_SAFE_TO_LAUNCH_EVENT()

	RETURN NOT NETWORK_IS_IN_TUTORIAL_SESSION()
		AND NOT IS_PLAYER_ON_ANY_FM_MISSION_THAT_IS_NOT_GANG_ATTACK(PLAYER_ID())
		AND NOT IS_FM_MISSION_LAUNCH_IN_PROGRESS()
		AND NOT IS_PLAYER_ON_IMPROMPTU_DM()
		AND NOT IS_TRANSITION_ACTIVE()
		AND NOT IS_PLAYER_IN_CORONA()
		AND NOT IS_THIS_PLAYER_ACTIVE_IN_CORONA_PAST_TRANSITION_SESSION(PLAYER_ID())
		AND NOT NETWORK_IS_ACTIVITY_SESSION()
		AND NOT IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID())
		AND IS_SKYSWOOP_AT_GROUND()
		AND NOT IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_CINEMA)
		AND NOT IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_STRIPCLUB)
		AND NOT IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_TIME_TRIAL)
		AND NOT IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_RC_TIME_TRIAL)
		AND (NOT IS_PLAYER_IN_MP_PROPERTY(PLAYER_ID(), FALSE) OR g_AML_serverBD.AM_MissionToLaunch.scriptType = AML_SCRIPT_URBAN_WARFARE) //url:bugstar:2490432
		AND IS_NET_PLAYER_OK(PLAYER_ID())
		AND NOT IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(PLAYER_ID())
		AND HAS_PLAYER_COMPLETED_INITIAL_AMBIENT_TUTORIALS(PLAYER_ID())
		#IF FEATURE_CASINO
		AND NOT IS_PLAYER_IN_CASINO(PLAYER_ID())
		AND NOT IS_PLAYER_IN_CASINO_VALET_GARAGE(PLAYER_ID())
		AND NOT IS_PLAYER_IN_CASINO_APARTMENT(PLAYER_ID())
		#ENDIF
		AND NOT IS_PLAYER_ON_HEIST_ISLAND(PLAYER_ID())
		AND NOT HEIST_ISLAND_TRAVEL__IS_TRANSITION_IN_PROGRESS()

ENDFUNC

FUNC BOOL IS_PLAYER_IN_SUITABLE_GANG_FOR_MINIMUM_CHECK()

	IF IS_AML_EVENT_A_BUSINESS_BATTLE(g_AML_serverBD.AM_MissionToLaunch.scriptType)
		RETURN NOT GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG(FALSE)
	ENDIF
	
	RETURN TRUE

ENDFUNC

FUNC BOOL SHOULD_LOCAL_PLAYER_BE_COUNTED_FOR_MINIMUM_CHECK()

	INT iHide
	iHide = GET_AML_HIDE_MENU_INDEX(g_AML_serverBD.AM_MissionToLaunch.scriptType)
	
	IF (iHide >= 0 AND (SHOULD_HIDE_AMBIENT_EVENT(iHide)))
	OR FM_EVENT_IS_PLAYER_RESTRICTED_WITH_HIDE_OPTION(PLAYER_ID())
	OR IS_PLAYER_SPECTATING(PLAYER_ID())
	OR NOT HAS_PLAYER_COMPLETED_INITIAL_AMBIENT_TUTORIALS(PLAYER_ID())
	OR NOT IS_PLAYER_IN_SUITABLE_GANG_FOR_MINIMUM_CHECK()
		PRINTLN("[AM_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] SHOULD_LOCAL_PLAYER_BE_COUNTED_FOR_MINIMUM_CHECK - FALSE - Hide = ",iHide)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE

ENDFUNC

FUNC BOOL HAS_LOCAL_PLAYER_UNLOCKED_EVENT(AML_SCRIPT_TYPE type)

	RETURN IS_FM_TYPE_UNLOCKED(GET_FMMC_TYPE_FROM_AML_SCRIPT_TYPE(type))

ENDFUNC

FUNC BOOL SHOULD_ALLOW_EVENT_LAUNCH_WITHOUT_UNLOCK(AML_SCRIPT_TYPE type)

	SWITCH type
		CASE AML_SCRIPT_JOYRIDER			
		CASE AML_SCRIPT_CRATEDROP
		CASE AML_SCRIPT_SECURITYVAN
		CASE AML_SCRIPT_IMPORTEXPORT
		CASE AML_SCRIPT_LESTERKILLTARGET
		CASE AML_SCRIPT_PLANETAKEDOWN
		CASE AML_SCRIPT_DISTRACTCOPS
		CASE AML_SCRIPT_DESTROYVEHICLE
			RETURN FALSE
	
		CASE AML_SCRIPT_CPCOLLECTION
		CASE AML_SCRIPT_CHALLENGES	
		CASE AML_SCRIPT_HOT_TARGET
		CASE AML_SCRIPT_URBAN_WARFARE	
		CASE AML_SCRIPT_PENNED_IN
		CASE AML_SCRIPT_PASS_THE_PARCEL
		CASE AML_SCRIPT_HOT_PROPERTY
		CASE AML_SCRIPT_DEAD_DROP
		CASE AML_SCRIPT_KING_OF_THE_CASTLE
		CASE AML_SCRIPT_CRIMINAL_DAMAGE
		CASE AML_SCRIPT_HUNT_THE_BEAST
			RETURN IS_PLAYER_SCTV(PLAYER_ID())
	ENDSWITCH

	RETURN TRUE

ENDFUNC

FUNC BOOL CAN_LOCAL_PLAYER_LAUNCH_THIS_EVENT(AML_SCRIPT_TYPE type)

	IF NOT SHOULD_ALLOW_EVENT_LAUNCH_WITHOUT_UNLOCK(type)
	AND NOT HAS_LOCAL_PLAYER_UNLOCKED_EVENT(type)
		PRINTLN("[AM_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] CAN_LOCAL_PLAYER_LAUNCH_THIS_EVENT - FALSE - NOT HAS_LOCAL_PLAYER_UNLOCKED_EVENT")
		RETURN FALSE
	ENDIF

	SWITCH type
						
		CASE AML_SCRIPT_JOYRIDER			
			RETURN CAN_RUN_JOYRIDER_FREEMODE_CHECKS()
			AND IS_PLAYER_CLOSE_TO_VEHICLE_NODE(GET_PLAYER_COORDS(PLAYER_ID()), <<40, 40, 40>>)
			AND GET_ENTITY_SPEED(PLAYER_PED_ID()) < 10
			AND NOT IS_PLAYER_AT_DOCKS_JR()		
		
		CASE AML_SCRIPT_LESTERKILLTARGET	
			RETURN IS_FM_TYPE_UNLOCKED(FMMC_TYPE_SECURITY_VAN)
			AND NOT IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_GANGHIDEOUT)
			AND NOT IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_RACETOPOINT)
			AND NOT IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_HOLDUP)
			AND NOT MPGlobals.bStartedMPCutscene
			AND NOT Is_A_Lester_Heist_Strand_Active()

	ENDSWITCH
	
	RETURN TRUE

ENDFUNC

FUNC BOOL SHOULD_RUN_CLIENT_IMPORT_EXPORT_HPV_LAUNCH_CHECKS()

	IF g_AML_serverBD.AM_MissionToLaunch.missionScript != eAM_FM_IMP_EXP
		RETURN FALSE
	ENDIF
	
	IF MPGlobalsAmbience.bPlayerOnImportExportHpv
		IF g_AML_serverBD.eAML_Stage = AML_EVENT_RUNNING
			PRINTLN("[AM_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] LAUNCHING HIGH PRIORITY VEHICLE")
			SET_CLIENT_AML_STAGE(AML_EVENT_RUNNING)
		ENDIF
	ELIF MPGlobalsAmbience.bPlayerFailedHPVChecks
		MPGlobalsAmbience.bPlayerFailedHPVChecks = FALSE
		PRINTLN("[AM_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] CLIENT CHECKS FAILED")
		SET_CLIENT_AML_STAGE(AML_AWAITING_START)
		SET_BIT(playerBD[NETWORK_PLAYER_ID_TO_INT()].iClientBitSet,CLIENT_BS_FAILED_LAUNCH)	
	ENDIF	
	
	RETURN TRUE

ENDFUNC

FUNC BOOL SHOULD_RUN_SERVER_IMPORT_EXPORT_HPV_LAUNCH_CHECKS()

	IF g_AML_serverBD.AM_MissionToLaunch.missionScript != eAM_FM_IMP_EXP
		RETURN FALSE
	ENDIF

	IF NETWORK_GET_HOST_OF_SCRIPT("AM_IMP_EXP") != INVALID_PLAYER_INDEX()
		BROADCAST_AML_SET_HPV_RUNNING(SPECIFIC_PLAYER(NETWORK_GET_HOST_OF_SCRIPT("AM_IMP_EXP")),TRUE)
		MPGlobalsAmbience.bCleanupImportExportHpv = FALSE

		SET_SERVER_AML_STAGE(AML_EVENT_RUNNING)
		//Send an event to reset globals
		#IF IS_DEBUG_BUILD
		BROADCAST_GENERAL_EVENT(GENERAL_EVENT_RESET_EVENT_PLAYLIST_DEBUG, ALL_PLAYERS())
		#ENDIF
		ADD_TO_HISTORY(g_AML_serverBD.AM_MissionToLaunch.scriptType)
	ENDIF
	
	RETURN TRUE

ENDFUNC

FUNC BOOL SHOULD_RUN_SERVER_IMPORT_EXPORT_HPV_RUNNING_CHECKS()

	IF g_AML_serverBD.AM_MissionToLaunch.missionScript != eAM_FM_IMP_EXP
		RETURN FALSE
	ENDIF

	IF MPGlobalsAmbience.bCleanupImportExportHpv
		SET_SERVER_AML_STAGE(AML_CLEANUP)
		MPGlobalsAmbience.bCleanupImportExportHpv = FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL SHOULD_RUN_SERVER_CRATE_DROP_COUNTDOWN_CHECKS()

	IF g_AML_serverBD.AM_MissionToLaunch.missionScript != eAM_CRATE_DROP
		RETURN FALSE
	ENDIF

	IF IS_SERVER_BIT_SET(SERVER_BS_DO_SPC_CD)
		
		IF NOT IS_SERVER_BIT_SET(SERVER_BS_SCD_TIMER_RESET)
			RESET_NET_TIMER(g_AML_serverBD.specialCrateCountdown)
			PRINTLN("[AM_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] RESET_NET_TIMER(g_AML_serverBD.specialCrateCountdown)")
			SET_SERVER_BIT(SERVER_BS_SCD_TIMER_RESET)
		ELSE
			IF HAS_NET_TIMER_EXPIRED(g_AML_serverBD.specialCrateCountdown,g_sMPTunables.iCrateDropSpecialFrequency * ONE_HOUR)
			#IF IS_DEBUG_BUILD
			OR bSkipSPCcrateTimeDelay
			#ENDIF
				PRINTLN("[AM_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] HAS_NET_TIMER_EXPIRED(g_AML_serverBD.specialCrateCountdown,g_sMPTunables.iCrateDropSpecialFrequency * ONE_HOUR)")
				CLEAR_SERVER_BIT(SERVER_BS_SCD_TIMER_RESET)
				
				SET_SERVER_AML_STAGE(AML_LAUNCH_EVENT)
			ENDIF
		ENDIF
	ELSE
		PRINTLN("[AM_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] NOT SPECIAL CRATEDROP - SKIP AML_COUNTDOWN")
		SET_SERVER_AML_STAGE(AML_LAUNCH_EVENT)
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

PROC CLIENT_PROCESSING()

	#IF IS_DEBUG_BUILD
		IF FM_EVENT_IS_PLAYER_ON_ANY_FM_EVENT(PLAYER_ID())
			IF IS_DEBUG_KEY_JUST_PRESSED(KEY_NUMPAD7,KEYBOARD_MODIFIER_CTRL_SHIFT,"Move current freemode event to end stage.")
				MPGlobalsAmbience.bEndCurrentFreemodeEvent = TRUE
				PRINTLN("[AM_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] I pressed CTRL_SHIFT 7 to end the event!!!!")
			ENDIF
		ENDIF
	#ENDIF
	
	INT iTimeOut = CLIENT_CHECK_TIME
	SWITCH playerBD[NETWORK_PLAYER_ID_TO_INT()].eAML_Stage
	
		CASE AML_AWAITING_START
			IF g_AML_serverBD.eAML_Stage >= AML_FINAL_CHECKS
			AND g_AML_serverBD.eAML_Stage != AML_CLEANUP
				
				IF IS_SERVER_BIT_SET(SERVER_BS_SKIP_CLIENT_CHECKS)
					PRINTLN("[AM_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] IS_SERVER_BIT_SET(SERVER_BS_SKIP_CLIENT_CHECKS)")
					SET_BIT(playerBD[NETWORK_PLAYER_ID_TO_INT()].iClientBitSet,CLIENT_BS_REQUESTED_LAUNCH)
					CLEAR_BIT(playerBD[NETWORK_PLAYER_ID_TO_INT()].iClientBitSet,CLIENT_BS_FAILED_LAUNCH)
				ENDIF

				IF NOT IS_CLIENT_BIT_SET(PLAYER_ID(), CLIENT_BS_FAILED_LAUNCH)
					SET_CLIENT_AML_STAGE(AML_FINAL_CHECKS)
					
				ELSE 	//Failed initial check, stagger checks from now on
					IF NOT IS_CLIENT_BIT_SET(PLAYER_ID(), CLIENT_BS_RESET_CHECKS)
						RESET_NET_TIMER(clientCheckTimer)
						SET_BIT(playerBD[NETWORK_PLAYER_ID_TO_INT()].iClientBitSet,CLIENT_BS_RESET_CHECKS)
						PRINTLN("[AM_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] SET_BIT(playerBD[NETWORK_PLAYER_ID_TO_INT()].iClientBitSet,CLIENT_BS_RESET_CHECKS)")
					ELSE
						IF IS_BIT_SET(iLocalBitSet, AML_BS_SHORT_DELAY)
							iTimeOut = CLIENT_CHECK_TIME_SHORT
						ENDIF
						IF HAS_NET_TIMER_EXPIRED(clientCheckTimer,iTimeOut)
							CLEAR_BIT(iLocalBitSet, AML_BS_SHORT_DELAY)	
							SET_CLIENT_AML_STAGE(AML_FINAL_CHECKS)
							CLEAR_BIT(playerBD[NETWORK_PLAYER_ID_TO_INT()].iClientBitSet,CLIENT_BS_RESET_CHECKS)
						ENDIF
					ENDIF
				ENDIF
				
			ELSE
				IF IS_CLIENT_BIT_SET(PLAYER_ID(), CLIENT_BS_FAILED_LAUNCH)
					PRINTLN("[AM_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] CLEAR_BIT(playerBD[NETWORK_PLAYER_ID_TO_INT()].iClientBitSet,CLIENT_BS_FAILED_LAUNCH)")
					CLEAR_BIT(playerBD[NETWORK_PLAYER_ID_TO_INT()].iClientBitSet,CLIENT_BS_FAILED_LAUNCH)
				ENDIF
			ENDIF
			
		BREAK 
		
		CASE AML_FINAL_CHECKS
			
			IF IS_CLIENT_BIT_SET(PLAYER_ID(), CLIENT_BS_REQUESTED_LAUNCH)
				IF g_AML_serverBD.eAML_Stage = AML_CLEANUP
					SET_CLIENT_AML_STAGE(AML_CLEANUP)
					CLEAR_BIT(playerBD[NETWORK_PLAYER_ID_TO_INT()].iClientBitSet,CLIENT_BS_REQUESTED_LAUNCH)
					CLEAR_BIT(playerBD[NETWORK_PLAYER_ID_TO_INT()].iClientBitSet,CLIENT_BS_DONT_COUNT)
				ELIF g_AML_serverBD.eAML_Stage = AML_COUNTDOWN
					SET_CLIENT_AML_STAGE(AML_COUNTDOWN)
					CLEAR_BIT(playerBD[NETWORK_PLAYER_ID_TO_INT()].iClientBitSet,CLIENT_BS_REQUESTED_LAUNCH)
					CLEAR_BIT(playerBD[NETWORK_PLAYER_ID_TO_INT()].iClientBitSet,CLIENT_BS_DONT_COUNT)
				ELIF g_AML_serverBD.eAML_Stage >= AML_LAUNCH_EVENT
					SET_CLIENT_AML_STAGE(AML_LAUNCH_EVENT)
					CLEAR_BIT(playerBD[NETWORK_PLAYER_ID_TO_INT()].iClientBitSet,CLIENT_BS_REQUESTED_LAUNCH)
					CLEAR_BIT(playerBD[NETWORK_PLAYER_ID_TO_INT()].iClientBitSet,CLIENT_BS_DONT_COUNT)
				ELIF g_AML_serverBD.eAML_Stage = AML_GET_NEXT_EVENT
					SET_CLIENT_AML_STAGE(AML_AWAITING_START)
					CLEAR_BIT(playerBD[NETWORK_PLAYER_ID_TO_INT()].iClientBitSet,CLIENT_BS_REQUESTED_LAUNCH)
					CLEAR_BIT(playerBD[NETWORK_PLAYER_ID_TO_INT()].iClientBitSet,CLIENT_BS_DONT_COUNT)
				ENDIF
			ELSE
			
				IF IS_LOCAL_PLAYER_SAFE_TO_LAUNCH_EVENT()
					IF NOT SHOULD_LOCAL_PLAYER_BE_COUNTED_FOR_MINIMUM_CHECK()
						SET_BIT(playerBD[NETWORK_PLAYER_ID_TO_INT()].iClientBitSet,CLIENT_BS_DONT_COUNT)
						PRINTLN("[AM_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] SET_BIT - CLIENT_BS_DONT_COUNT")
					ENDIF
					
					IF CAN_LOCAL_PLAYER_LAUNCH_THIS_EVENT(g_AML_serverBD.AM_MissionToLaunch.scriptType)
						IF NOT IS_CLIENT_BIT_SET(PLAYER_ID(), CLIENT_BS_REQUESTED_LAUNCH)
							SET_BIT(playerBD[NETWORK_PLAYER_ID_TO_INT()].iClientBitSet,CLIENT_BS_REQUESTED_LAUNCH)
							PRINTLN("[AM_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] SET_BIT(playerBD[NETWORK_PLAYER_ID_TO_INT()].iClientBitSet,CLIENT_BS_REQUESTED_LAUNCH)")
						ENDIF
					ENDIF
				ENDIF
				
				// Failed to launch
				IF NOT IS_CLIENT_BIT_SET(PLAYER_ID(), CLIENT_BS_REQUESTED_LAUNCH)
					//If it failed be cause the sky cam was not at the ground and because we've not done the initial launch then set the flag to skip the 10s delay
					IF NOT IS_SKYSWOOP_AT_GROUND()
					AND NOT IS_BIT_SET(iLocalBitSet, AML_BS_INITIAL_DONE)
						PRINTLN("[AM_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] SET_BIT(iLocalBitSet, AML_BS_SHORT_DELAY)")
						SET_BIT(iLocalBitSet, AML_BS_SHORT_DELAY)	
					ENDIF
					PRINTLN("[AM_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] CLIENT CHECKS FAILED")
					SET_CLIENT_AML_STAGE(AML_AWAITING_START)
					SET_BIT(playerBD[NETWORK_PLAYER_ID_TO_INT()].iClientBitSet,CLIENT_BS_FAILED_LAUNCH)	
					CLEAR_BIT(playerBD[NETWORK_PLAYER_ID_TO_INT()].iClientBitSet,CLIENT_BS_DONT_COUNT)
				ENDIF
			ENDIF
		BREAK
		
		CASE AML_COUNTDOWN
			IF g_AML_serverBD.eAML_Stage = AML_LAUNCH_EVENT
				CLEAR_BIT(MPGlobalsAmbience.CrateDropLaunchData.iBitSet, biCDL_SpecialCrateTicker1)
				CLEAR_BIT(MPGlobalsAmbience.CrateDropLaunchData.iBitSet, biCDL_SpecialCrateTicker2)
				CLEAR_BIT(MPGlobalsAmbience.CrateDropLaunchData.iBitSet, biCDL_SpecialCrateTicker3)
				
				SET_CLIENT_AML_STAGE(AML_LAUNCH_EVENT)
			ELSE
				IF IS_SERVER_BIT_SET(SERVER_BS_SCD_TIMER_RESET)
				AND HAS_NET_TIMER_STARTED(g_AML_serverBD.specialCrateCountdown)
					PROCESS_SPECIAL_CRATE_DROP_MESSAGES_CLIENT()
				ENDIF
			ENDIF
			
		BREAK
		
		CASE AML_LAUNCH_EVENT
		
			TEXT_LABEL_63 sMissionName 
			sMissionName = GET_MP_MISSION_NAME(g_AML_serverBD.AM_MissionToLaunch.missionScript)
			
			#IF IS_DEBUG_BUILD
			IF IS_STRING_NULL_OR_EMPTY(sMissionName)
			OR GET_HASH_KEY(sMissionName) = 0
				PRINTLN("[AM_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] Something has gone wrong - sMissionName = ",sMissionName)
				PRINTLN("[AM_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] Something has gone wrong - GET_HASH_KEY(sMissionName) = ",GET_HASH_KEY(sMissionName))
				PRINTLN("[AM_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] Something has gone wrong - g_AML_serverBD.AM_MissionToLaunch.missionScript = ",g_AML_serverBD.AM_MissionToLaunch.missionScript)
			ENDIF
			#ENDIF
			
			IF NOT SHOULD_RUN_CLIENT_IMPORT_EXPORT_HPV_LAUNCH_CHECKS()
				IF NOT IS_CLIENT_BIT_SET(PLAYER_ID(), CLIENT_BS_LAUNCHING)
					AM_MissionData.iInstanceId = DEFAULT_MISSION_INSTANCE

					IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(sMissionName)) = 0
						IF NOT NETWORK_IS_SCRIPT_ACTIVE(sMissionName)
						OR NETWORK_GET_NUM_SCRIPT_PARTICIPANTS(sMissionName) < NUM_NETWORK_PLAYERS
							IF NOT NETWORK_IS_SCRIPT_ACTIVE(sMissionName, DEFAULT, TRUE)
								AM_MissionData.mdID.idMission = g_AML_serverBD.AM_MissionToLaunch.missionScript
								#IF IS_DEBUG_BUILD
									IF g_AML_serverBD.bLaunchedFromMissionMenu
										g_AML_serverBD.bLaunchedFromMissionMenuForScript = TRUE
									ELSE
										g_AML_serverBD.bLaunchedFromMissionMenuForScript = FALSE
									ENDIF
								#ENDIF
								IF NET_LOAD_AND_LAUNCH_SCRIPT_WITH_ARGS(AM_MissionData)
									PRINTLN("[AM_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] LAUNCHING ",sMissionName)
									SET_BIT(playerBD[NETWORK_PLAYER_ID_TO_INT()].iClientBitSet,CLIENT_BS_LAUNCHING)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					//Move On now script is running
					IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(sMissionName)) != 0
						IF g_AML_serverBD.eAML_Stage = AML_EVENT_RUNNING
							SET_CLIENT_AML_STAGE(AML_EVENT_RUNNING)
							//Set that we've done the initial launch
							SET_BIT(iLocalBitSet, AML_BS_INITIAL_DONE)
							CLEAR_BIT(playerBD[NETWORK_PLAYER_ID_TO_INT()].iClientBitSet,CLIENT_BS_LAUNCHING)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
		CASE AML_EVENT_RUNNING
			IF g_AML_serverBD.eAML_Stage != AML_EVENT_RUNNING
				SET_CLIENT_AML_STAGE(AML_CLEANUP)
			ENDIF
		BREAK
		
		CASE AML_CLEANUP
			#IF IS_DEBUG_BUILD
			IF MPGlobalsAmbience.bEndCurrentFreemodeEvent
				MPGlobalsAmbience.bEndCurrentFreemodeEvent = FALSE
				PRINTLN("[AM_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] MPGlobalsAmbience.bEndCurrentFreemodeEvent = FALSE")
			ENDIF
			#ENDIF
			SET_CLIENT_AML_STAGE(AML_AWAITING_START)
		BREAK

	ENDSWITCH

ENDPROC

FUNC BOOL DOES_EVENT_PASS_SPECIFIC_CONDITIONS_FOR_LAUNCH(AML_SCRIPT_TYPE currentScriptType)
	UNUSED_PARAMETER(currentScriptType)
	RETURN TRUE
ENDFUNC

FUNC INT GET_LAUNCH_TIME()

	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_DisableAMLauncherTimer")
		RETURN HIGHEST_INT
	ENDIF
	#ENDIF
	
	RETURN g_AML_serverBD.iLaunchTime

ENDFUNC

FUNC BOOL IS_TIME_TO_LAUNCH_EVENT()
	
	RETURN HAS_NET_TIMER_EXPIRED(g_AML_serverBD.timeToLaunch, GET_LAUNCH_TIME())
	#IF IS_DEBUG_BUILD
	OR bSkipTimeDelay
	AND NOT ((bEnablePlaylist OR g_sMPTunables.bEnableEventPlaylist) AND g_AML_serverBD.eAMP_Stage != AMP_RUN)
	#ENDIF

ENDFUNC

FUNC BOOL DID_EVENT_TYPE_RUN_PREVIOUSLY(AML_SCRIPT_TYPE currentScriptType)
	#IF IS_DEBUG_BUILD
	IF bIgnoreHistory
		RETURN FALSE
	ENDIF
	#ENDIF

	IF g_AML_serverBD.ePreviousEvent = AML_SCRIPT_NULL
		RETURN FALSE
	ENDIF

	IF IS_AML_EVENT_A_BUSINESS_BATTLE(g_AML_serverBD.ePreviousEvent)
		IF IS_AML_EVENT_A_BUSINESS_BATTLE(currentScriptType)
			RETURN TRUE
		ENDIF
	ELSE
		IF NOT IS_AML_EVENT_A_BUSINESS_BATTLE(currentScriptType)
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE

ENDFUNC

FUNC BOOL IS_EVENT_SAFE_TO_LAUNCH(AML_SCRIPT_TYPE currentScriptType)

	IF IS_EVENT_SCRIPT_ALREADY_RUNNING(currentScriptType)
		PRINTLN("IS_EVENT_SAFE_TO_LAUNCH - [",GET_AML_MISSION_NAME(currentScriptType),"] - FALSE - IS_EVENT_SCRIPT_ALREADY_RUNNING")
		RETURN FALSE
	ENDIF
	
	IF IS_EVENT_IN_HISTORY(currentScriptType)
		PRINTLN("IS_EVENT_SAFE_TO_LAUNCH - [",GET_AML_MISSION_NAME(currentScriptType),"] - FALSE - IS_EVENT_IN_HISTORY")
		RETURN FALSE
	ENDIF
	
	IF DID_EVENT_TYPE_RUN_PREVIOUSLY(currentScriptType)
		PRINTLN("IS_EVENT_SAFE_TO_LAUNCH - [",GET_AML_MISSION_NAME(currentScriptType),"] - FALSE - DID_EVENT_TYPE_RUN_PREVIOUSLY")
		RETURN FALSE
	ENDIF
		
	IF NOT ARE_NETWORK_ENTITIES_AVAILABLE_FOR_SCRIPT_LAUNCH(GET_AML_NUM_PEDS_REQUIRED(currentScriptType),GET_AML_NUM_VEHICLES_REQUIRED(currentScriptType),GET_AML_NUM_OBJECTS_REQUIRED(currentScriptType),TRUE,TRUE)
		PRINTLN("IS_EVENT_SAFE_TO_LAUNCH - [",GET_AML_MISSION_NAME(currentScriptType),"] - FALSE - NOT ARE_NETWORK_ENTITIES_AVAILABLE_FOR_SCRIPT_LAUNCH")
		RETURN FALSE
	ENDIF
	
	IF IS_AML_EVENT_DISABLED(currentScriptType)
		PRINTLN("IS_EVENT_SAFE_TO_LAUNCH - [",GET_AML_MISSION_NAME(currentScriptType),"] - FALSE - IS_AML_EVENT_DISABLED")
		RETURN FALSE
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF NOT g_AML_serverBD.launcherScripts[ENUM_TO_INT(currentScriptType)].bDebugActive
		PRINTLN("IS_EVENT_SAFE_TO_LAUNCH - [",GET_AML_MISSION_NAME(currentScriptType),"] - FALSE - NOT bDebugActive")
		RETURN FALSE
	ENDIF
	#ENDIF	
	
	IF NOT DOES_EVENT_PASS_SPECIFIC_CONDITIONS_FOR_LAUNCH(currentScriptType)
		PRINTLN("IS_EVENT_SAFE_TO_LAUNCH - [",GET_AML_MISSION_NAME(currentScriptType),"] - FALSE - NOT DOES_EVENT_PASS_SPECIFIC_CONDITIONS_FOR_LAUNCH")
		RETURN FALSE
	ENDIF
				
	RETURN TRUE

ENDFUNC

FUNC BOOL DOES_AML_SCRIPT_NEED_VARIATION_SELECTION(AML_SCRIPT_TYPE currentScriptType)

	IF GET_FREEMODE_CONTENT_TYPE(GET_FMMC_TYPE_FROM_AML_SCRIPT_TYPE(currentScriptType)) = eFMCONTENTTYPE_FREEMODE_EVENT
		RETURN TRUE
	ENDIF

	RETURN FALSE

ENDFUNC

PROC SERVER_PROCESSING()
	
	SWITCH g_AML_serverBD.eAML_Stage
	
		CASE AML_AWAITING_START
		
			IF IS_TIME_TO_LAUNCH_EVENT()
				RESET_NET_TIMER(g_AML_serverBD.timeToLaunch)
				SET_SERVER_AML_STAGE(AML_SET_WEIGHTINGS)
			ENDIF
	
		BREAK 
		
		CASE AML_SET_WEIGHTINGS
		
			//Common Checks go here
			IF NETWORK_IS_GAME_IN_PROGRESS()
				
				INT iCurrentScript
				AML_SCRIPT_TYPE currentScriptType
								
				//Set Weightings here
				REPEAT AML_SCRIPT_MAX iCurrentScript
					currentScriptType = INT_TO_ENUM(AML_SCRIPT_TYPE,iCurrentScript)
					
					IF IS_EVENT_SAFE_TO_LAUNCH(currentScriptType)
						SET_SCRIPT_LAUNCH_WEIGHTING(currentScriptType,GET_AML_BASE_WEIGHTING(currentScriptType))
					ELSE
						SET_SCRIPT_LAUNCH_WEIGHTING(currentScriptType,0.0)
					ENDIF
					
				ENDREPEAT
		
				SET_SERVER_AML_STAGE(AML_GET_NEXT_EVENT)
			ELSE
				PRINTLN("[AM_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] - NOT NETWORK_IS_GAME_IN_PROGRESS")	
				SET_SERVER_AML_STAGE(AML_AWAITING_START)	
			ENDIF
			
		BREAK
				
		CASE AML_GET_NEXT_EVENT
		
			//Randomly pick event based on weightings here
			INT iEventIndex
			iEventIndex = GET_EVENT_INDEX_FROM_WEIGHTINGS()
			IF iEventIndex != -1
				
				g_AML_serverBD.AM_MissionToLaunch = g_AML_serverBD.launcherScripts[iEventIndex]
				
				PRINTLN("[AM_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] AML_GET_NEXT_EVENT - Mission = ",GET_AML_MISSION_NAME(g_AML_serverBD.AM_MissionToLaunch.scriptType))
				SET_SERVER_AML_STAGE(AML_FINAL_CHECKS)
			ELSE
				PRINTLN("[AM_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] AML_GET_NEXT_EVENT - NO EVENT AVAILABLE TO RUN")
				g_AML_serverBD.iLaunchTime = g_sMPTunables.itime_event_frequency_ambient_launcher / 2
				SET_SERVER_AML_STAGE(AML_AWAITING_START)
			ENDIF
			
		BREAK
		
		CASE AML_FINAL_CHECKS
		
			SWITCH CHECK_ENOUGH_PLAYERS_FOR_SCRIPT(g_AML_serverBD.AM_MissionToLaunch)
			
				CASE 0 //Waiting for clients

				BREAK
				
				CASE 1 //Enough players, launch event
				
					//Timing specific checks for event here		
					SWITCH(g_AML_serverBD.AM_MissionToLaunch.scriptType)
						
						CASE AML_SCRIPT_CRATEDROP
							CHECK_FOR_SPECIAL_CRATE_DROP()
							IF IS_SERVER_BIT_SET(SERVER_BS_DO_SPC_CD)
								SET_SERVER_AML_STAGE(AML_COUNTDOWN)
							ELSE
								SET_SERVER_AML_STAGE(AML_LAUNCH_EVENT)
							ENDIF
						BREAK
						
						DEFAULT
							IF DOES_AML_SCRIPT_NEED_VARIATION_SELECTION(g_AML_serverBD.AM_MissionToLaunch.scriptType)
							
								// Read from debug variables
								g_AML_serverBD.iFMEventVariation = MPGlobalsAmbience.iFMEventForcedVariation
								g_AML_serverBD.iFMEventSubvariation = MPGlobalsAmbience.iFMEventForcedSubvariation
								g_AML_serverBD.iFMEventCargoType = MPGlobalsAmbience.iFMEventForcedCargoType
							
								IF GET_FREEMODE_CONTENT_VARIATION_DATA(GET_MISSION_TO_LAUNCH(), g_AML_serverBD.iFMEventVariation, g_AML_serverBD.iFMEventSubvariation, INVALID_PLAYER_INDEX())
									SET_SERVER_AML_STAGE(AML_LAUNCH_EVENT)
								ELSE
									SET_SCRIPT_LAUNCH_WEIGHTING(g_AML_serverBD.AM_MissionToLaunch.scriptType, 0.0)
									SET_SERVER_AML_STAGE(AML_GET_NEXT_EVENT) // Try and find a different event
								ENDIF
							ELSE
								SET_SERVER_AML_STAGE(AML_LAUNCH_EVENT)
							ENDIF
						BREAK
						
					ENDSWITCH

				BREAK
				
				CASE 2 //Not enough players, return and pick another event
					PRINTLN("[AM_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] - [",GET_AML_MISSION_NAME(g_AML_serverBD.AM_MissionToLaunch.scriptType),"] - NOT ENOUGH PLAYERS TO START EVENT")
					SET_SCRIPT_LAUNCH_WEIGHTING(g_AML_serverBD.AM_MissionToLaunch.scriptType,0.0)
					SET_SERVER_AML_STAGE(AML_GET_NEXT_EVENT)
				BREAK
			
			ENDSWITCH
		BREAK
		
		CASE AML_COUNTDOWN

			IF NOT SHOULD_RUN_SERVER_CRATE_DROP_COUNTDOWN_CHECKS()
				PRINTLN("[AM_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] NOT CRATEDROP - SKIP AML_COUNTDOWN")
				SET_SERVER_AML_STAGE(AML_LAUNCH_EVENT)
			ENDIF
		
		BREAK
		
		CASE AML_LAUNCH_EVENT
		
			IF NOT SHOULD_RUN_SERVER_IMPORT_EXPORT_HPV_LAUNCH_CHECKS()
				IF NETWORK_IS_SCRIPT_ACTIVE(GET_MP_MISSION_NAME(g_AML_serverBD.AM_MissionToLaunch.missionScript))
					SET_SERVER_AML_STAGE(AML_EVENT_RUNNING)
					//Send an event to reset globals
					#IF IS_DEBUG_BUILD
					BROADCAST_GENERAL_EVENT(GENERAL_EVENT_RESET_EVENT_PLAYLIST_DEBUG, ALL_PLAYERS())
					#ENDIF
					ADD_TO_HISTORY(g_AML_serverBD.AM_MissionToLaunch.scriptType)
					
					#IF IS_DEBUG_BUILD
						IF g_AML_serverBD.bLaunchedFromMissionMenu
							g_AML_serverBD.bLaunchedFromMissionMenu = FALSE
							bSkipTimeDelay = FALSE
							bIgnoreHistory = FALSE
							bIgnoreMinPlayers = FALSE
						ENDIF
					#ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
		CASE AML_EVENT_RUNNING
	
			IF NOT SHOULD_RUN_SERVER_IMPORT_EXPORT_HPV_RUNNING_CHECKS()
				IF NOT NETWORK_IS_SCRIPT_ACTIVE(GET_MP_MISSION_NAME(g_AML_serverBD.AM_MissionToLaunch.missionScript))
					SET_SERVER_AML_STAGE(AML_CLEANUP)
				ENDIF
			ENDIF

		BREAK
		
		CASE AML_CLEANUP
			
			//Do cleanup
			g_AML_serverBD.iLaunchTime = g_sMPTunables.itime_event_frequency_ambient_launcher
			
			g_AML_serverBD.launcherScripts[g_AML_serverBD.AM_MissionToLaunch.scriptType].bHasRunInThisSession = TRUE
			
			IF IS_SERVER_BIT_SET(SERVER_BS_SKIP_CLIENT_CHECKS)
				CLEAR_SERVER_BIT(SERVER_BS_SKIP_CLIENT_CHECKS)
			ENDIF
			
			IF IS_SERVER_BIT_SET(SERVER_BS_DO_SPC_CD)
				CLEAR_SERVER_BIT(SERVER_BS_DO_SPC_CD)
			ENDIF
			
			g_AML_serverBD.AM_MissionToLaunch.scriptType = AML_SCRIPT_NULL
			g_AML_serverBD.AM_MissionToLaunch.missionScript = eNULL_MISSION
			
			g_AML_serverBD.iFMEventVariation = -1
			g_AML_serverBD.iFMEventSubvariation = -1
			g_AML_serverBD.iFMEventCargoType = -1
			
			#IF IS_DEBUG_BUILD
			IF bEnablePlaylist
			OR g_sMPTunables.bEnableEventPlaylist
			SET_AMP_STAGE(AMP_END)
			ENDIF
			#ENDIF
			
			SET_SERVER_AML_STAGE(AML_AWAITING_START)
			
		BREAK

	ENDSWITCH

ENDPROC

FUNC BOOL SHOULD_LAUNCHER_SCRIPT_END()
	RETURN FALSE
ENDFUNC

FUNC BOOL INIT_CLIENT()
	#IF IS_DEBUG_BUILD
		SETUP_DEBUG_WIDGETS()
	#ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL INIT_SERVER()
	INT iCurrentScript
	INT iHistory
	
	g_AML_serverBD.iLaunchTime = g_sMPTunables.itime_event_frequency_ambient_launcher
	
	REPEAT AML_SCRIPT_MAX iCurrentScript 
		g_AML_serverBD.launcherScripts[iCurrentScript].scriptType = INT_TO_ENUM(AML_SCRIPT_TYPE,iCurrentScript)
		g_AML_serverBD.launcherScripts[iCurrentScript].missionScript = GET_AML_MISSION(INT_TO_ENUM(AML_SCRIPT_TYPE,iCurrentScript))
		g_AML_serverBD.launcherScripts[iCurrentScript].fLaunchWeighting = 1.0
				
		#IF IS_DEBUG_BUILD
			g_AML_serverBD.launcherScripts[iCurrentScript].bDebugActive = TRUE
		#ENDIF
	ENDREPEAT
	
	IF g_sMPTunables.inumber_of_scripts_in_history_list > 0 AND g_sMPTunables.inumber_of_scripts_in_history_list <= ENUM_TO_INT(AML_SCRIPT_MAX)
		REPEAT g_sMPTunables.inumber_of_scripts_in_history_list iHistory
			g_AML_serverBD.eventHistory[iHistory] = AML_SCRIPT_NULL
		ENDREPEAT
		REPEAT ciMAX_EVENT_HISTORY_LIST iHistory
			g_AML_serverBD.sHistory[iHistory].event = AML_SCRIPT_NULL
			g_AML_serverBD.sHistory[iHistory].iVaration = -1
		ENDREPEAT
	ENDIF
	
	RETURN TRUE
ENDFUNC

//Helper function to get a clients game/mission state
FUNC INT GET_CLIENT_MISSION_STATE(INT iPlayer)
	RETURN playerBD[iPlayer].iClientGameState
ENDFUNC

//Helper function to get the servers game/mission state
FUNC INT GET_SERVER_MISSION_STATE()
	RETURN g_AML_serverBD.iServerGameState
ENDFUNC

// -----------------------------------------------------------------------------------------------------------
//		Mission Cleanup
// -----------------------------------------------------------------------------------------------------------

PROC SCRIPT_CLEANUP()

	#IF IS_DEBUG_BUILD
		CLEANUP_DEBUG_WIDGETS()
	#ENDIF

	//Clear HPV bools
	MPGlobalsAmbience.bPlayerOnImportExportHpv = FALSE
	MPGlobalsAmbience.bPlayerFailedHPVChecks = FALSE
	MPGlobalsAmbience.bCleanupImportExportHpv = FALSE
	MPGlobalsAmbience.bLaunchImportExportHpv = FALSE
	
	//Clear special crate drop bits/bools
	CLEAR_BIT(MPGlobalsAmbience.CrateDropLaunchData.iBitSet, biCDL_SpecialCrateTicker1)
	CLEAR_BIT(MPGlobalsAmbience.CrateDropLaunchData.iBitSet, biCDL_SpecialCrateTicker2)
	CLEAR_BIT(MPGlobalsAmbience.CrateDropLaunchData.iBitSet, biCDL_SpecialCrateTicker3)

	PRINTLN("[AM_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] SCRIPT_CLEANUP")
	
	TERMINATE_THIS_THREAD()
	
ENDPROC

PROC CLEAR_GlobalServerBD_AmbientLauncher()
	PRINTLN("[AM_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] CLEAR_GlobalServerBD_AmbientLauncher")
	AML_ServerBroadcastData EmptyAML_ServerBD
	g_AML_serverBD = EmptyAML_serverBD
ENDPROC

FUNC BOOL PROCESS_PRE_GAME(MP_MISSION_DATA missionScriptArgs)

	PRINTLN("[AM_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] AM_LAUNCHER - PROCESS_PRE_GAME")
	
	CLEAR_GlobalServerBD_AmbientLauncher()
	
	// This marks the script as a net script, and handles any instancing setup. 
	SETUP_MP_MISSION_FOR_NETWORK(GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(missionScriptArgs.mdID.idMission), missionScriptArgs)
	
	//Reserve Entities
	RESERVE_NETWORK_MISSION_OBJECTS(0)
	
	// This makes sure the net script is active, waits until it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(g_AML_serverBD,SIZE_OF(g_AML_serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD,SIZE_OF(playerBD))
	
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	
	playerBD[NETWORK_PLAYER_ID_TO_INT()].iClientGameState = GAME_STATE_INIT
	SET_CLIENT_AML_STAGE(AML_AWAITING_START)
	
	RETURN TRUE
	
ENDFUNC

//----------------------
//	MAIN SCRIPT
//----------------------

SCRIPT(MP_MISSION_DATA missionScriptArgs)

	PRINTLN("[AM_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] AM_LAUNCHER - START")
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF NOT PROCESS_PRE_GAME(missionScriptArgs)
			SCRIPT_CLEANUP()
		ENDIF	
	ENDIF
	
	WHILE (TRUE)
	
		MP_LOOP_WAIT_ZERO()
		
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			SCRIPT_CLEANUP()
			PRINTLN("AM_LAUNCHER - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE = TRUE")
		ENDIF
		
		#IF IS_DEBUG_BUILD
		PRINT_SCRIPT_HOST_PLAYER(phStruct)
		#ENDIF
		
		SWITCH(GET_CLIENT_MISSION_STATE(NETWORK_PLAYER_ID_TO_INT()))
		
			CASE GAME_STATE_INIT
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					IF INIT_CLIENT()
						playerBD[NETWORK_PLAYER_ID_TO_INT()].iClientGameState = GAME_STATE_RUNNING
						PRINTLN("[AM_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] SET_CLIENT_GAME_STATE - GAME_STATE_RUNNING")
					ENDIF
				ENDIF
			BREAK
			
			CASE GAME_STATE_RUNNING
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					CLIENT_PROCESSING()
					#IF IS_DEBUG_BUILD
					UPDATE_F9_VALUES()
					MAINTAIN_LOWRIDER_EVENT_MISSION_MENU_LAUNCH()
					#ENDIF
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					playerBD[NETWORK_PLAYER_ID_TO_INT()].iClientGameState = GAME_STATE_END
					PRINTLN("[AM_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] SET_CLIENT_GAME_STATE - GAME_STATE_END")
					
				ENDIF
			BREAK
			
			CASE GAME_STATE_END
				SCRIPT_CLEANUP()
			BREAK
			
		ENDSWITCH
	
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		
			SWITCH(GET_SERVER_MISSION_STATE())
			
				CASE GAME_STATE_INIT
					IF INIT_SERVER()
						g_AML_serverBD.iServerGameState = GAME_STATE_RUNNING
						PRINTLN("[AM_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] SET_SERVER_GAME_STATE - GAME_STATE_RUNNING")
					ENDIF
				BREAK
				
				CASE GAME_STATE_RUNNING
					IF SHOULD_LAUNCHER_SCRIPT_END()
						g_AML_serverBD.iServerGameState = GAME_STATE_END
						PRINTLN("[AM_LAUNCH ",GET_CLOUD_TIME_AS_INT(),"] SET_SERVER_GAME_STATE - GAME_STATE_END")
					ENDIF
					
					SERVER_PROCESSING()
					
					#IF IS_DEBUG_BUILD
					PROCESS_EVENT_PLAYLIST()
					#ENDIF
				BREAK
				
				CASE GAME_STATE_END
					SCRIPT_CLEANUP()
				BREAK
				
			ENDSWITCH
		
		ENDIF
		
		#IF IS_DEBUG_BUILD
		g_sMPTunables.bEnableEventPlaylist = GlobalServerBD.bUseDebugFmEventPlayList
		IF (bQuit)
			SCRIPT_CLEANUP()
		ENDIF	
		#ENDIF
		
	ENDWHILE

// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT
