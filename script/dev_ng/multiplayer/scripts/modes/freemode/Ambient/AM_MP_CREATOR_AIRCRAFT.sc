//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        AM_MP_CREATOR_AIRCRAFT.sc																					//
// Description: Script for managing the interior of a AM_MP_CREATOR_AIRCRAFT. AM_MP_CREATOR_AIRCRAFT access, spawning etc.	//
//				is managed by AM_MP_SMPL_INTERIOR_* script while this script is launched by simple interior					//
//				script to handle anything specific to AM_MP_CREATOR_AIRCRAFT.												//
// Written by:  Online Technical Team: Ata																					//
// Date:  		27/06/2017																									//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"

USING "commands_network.sch"
USING "script_player.sch"
USING "commands_path.sch"
USING "commands_event.sch"

USING "net_include.sch"
USING "net_events.sch"
USING "net_spawn.sch"
USING "net_ambience.sch"

USING "net_wait_zero.sch"
USING "freemode_events_header.sch"

USING "net_MP_Radio.sch"
USING "net_activity_creator_activities.sch"
USING "net_include.sch"
USING "net_simple_interior.sch"
//USING "net_realty_CREATOR_AIRCRAFT.sch"

#IF IS_DEBUG_BUILD
USING "net_debug_log.sch"
#ENDIF


//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ VARIABLES ╞═════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

STRUCT ServerBroadcastData
	INT iBS
	MP_RADIO_SERVER_DATA_STRUCT MPRadioServer
	MP_PROP_ACT_SERVER_CONTROL_STRUCT activityControl
	SERVER_CREATOR_ACTIVITY_PROPS activityProps[iMaxCreatorActivities]
ENDSTRUCT
ServerBroadcastData serverBD

STRUCT PlayerBroadcastData
	INT iBS
	MP_RADIO_CLIENT_DATA_STRUCT MPRadioClient
	INT iActivityRequested
ENDSTRUCT
PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]

ENUM CREATOR_AIRCRAFT_SCRIPT_STATE
	CREATOR_AIRCRAFT_STATE_LOADING,
	CREATOR_AIRCRAFT_STATE_IDLE,
	CREATOR_AIRCRAFT_STATE_PLAYING_INTRO,
	CREATOR_AIRCRAFT_STATE_EXIT
ENDENUM

STRUCT CREATOR_AIRCRAFT_DATA
	INT iBS
	CREATOR_AIRCRAFT_SCRIPT_STATE eState
	INT iID // ID of this ArmoryAircraft
	SIMPLE_INTERIORS eSimpleInteriorID
	INT iScriptInstance
	INT iInvitingPlayer
	INT iBikerSaveSlot
	
	PLAYER_INDEX pOwner
	SIMPLE_INTERIOR_DETAILS interiorDetails
	VEHICLE_INDEX ownerVeh
	
	STRING sChildOfChildScript	
	THREADID CarModThread
	
	SCRIPT_TIMER tOwnerNotOk
	SCRIPT_TIMER sCarModScriptRunTimer
	SCRIPT_TIMER tPinInMemTimer
	
	INTERIOR_INSTANCE_INDEX iInteriorIndex
	
	BOOL bScriptRelaunched
	
	BLIP_INDEX bTurretBlips[3]
ENDSTRUCT
CREATOR_AIRCRAFT_DATA thisCreatorAircraft

#IF IS_DEBUG_BUILD
STRUCT CREATOR_AIRCRAFT_DEBUG_DATA
	STRING sWidgetName
ENDSTRUCT
CREATOR_AIRCRAFT_DEBUG_DATA debugData
#ENDIF

SERVER_CREATOR_ACTIVITY_PEDS activityPeds[iMaxCreatorActivities]
ACTIVITY_INTERIOR_STRUCT interiorStruct
CHECK_CREATOR_ACTIVITIES_STRUCT activityCheck[iMaxCreatorActivities]
ACTIVITY_CONTROLLER_STRUCT activityControllerStruct

// PlayerBroadcastData iBS
CONST_INT BS_CREATOR_AIRCRAFT_READY_TO_WARP_OUT_W_OWNER					0

// thisCreatorAircraft.iBS
CONST_INT BS_CREATOR_AIRCRAFT_IS_CAR_MOD_SCRIPT_READY 					0
CONST_INT BS_CREATOR_AIRCRAFT_CALLED_CLEAR_HELP							1
CONST_INT BS_CREATOR_AIRCRAFT_INTERIOR_REFRESHED_ON_INIT				2
CONST_INT BS_CREATOR_AIRCRAFT_KILL_LOAD_SCENE_WHEN_INSIDE				3
CONST_INT BS_CREATOR_AIRCRAFT_LOAD_SCENE_STARTED						4
CONST_INT BS_CREATOR_AIRCRAFT_CAN_NOT_LEAVE_TO_COCKPIT_PASSENGER		5
CONST_INT BS_CREATOR_AIRCRAFT_CAN_LEAVE_TO_COCKPIT_PASSENGER			6
CONST_INT BS_CREATOR_AIRCRAFT_INIT_FADE_IN_TIMER						7

TWEAK_FLOAT m_vehicle_zoomed_in_value									1.0
TWEAK_FLOAT m_vehicle_zoomed_out_value									0.0

SCRIPT_TIMER sFadeInTimer

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ PROCEDURES ╞═════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

FUNC BOOL IS_CREATOR_AIRCRAFT_STATE(CREATOR_AIRCRAFT_SCRIPT_STATE eState)
	RETURN thisCreatorAircraft.eState = eState
ENDFUNC

PROC SET_CREATOR_AIRCRAFT_STATE(CREATOR_AIRCRAFT_SCRIPT_STATE eState)
	CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] AM_MP_CREATOR_AIRCRAFT - SET_CREATOR_AIRCRAFT_STATE - New state: ", ENUM_TO_INT(eState))
	thisCreatorAircraft.eState = eState
ENDPROC

PROC SCRIPT_CLEANUP()
	SET_VEHICLE_SEAT_UNAVAILABLE(FALSE)
	CLEAR_TIMECYCLE_MODIFIER()
	#IF IS_DEBUG_BUILD
	CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_CREATOR_AIRCRAFT - SCRIPT_CLEANUP")
	#ENDIF
	
	IF DOES_BLIP_EXIST(thisCreatorAircraft.bTurretBlips[0])
		REMOVE_BLIP(thisCreatorAircraft.bTurretBlips[0])
	ENDIF
	
	IF DOES_BLIP_EXIST(thisCreatorAircraft.bTurretBlips[1])
		REMOVE_BLIP(thisCreatorAircraft.bTurretBlips[1])
	ENDIF
	
	IF DOES_BLIP_EXIST(thisCreatorAircraft.bTurretBlips[2])
		REMOVE_BLIP(thisCreatorAircraft.bTurretBlips[2])
	ENDIF
	
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════════╡ DEBUG ╞═══════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

#IF IS_DEBUG_BUILD
PROC CREATE_DEBUG_WIDGETS()
	debugData.sWidgetName = "AM_MP_CREATOR_AIRCRAFT"
	
	START_WIDGET_GROUP(debugData.sWidgetName)
	STOP_WIDGET_GROUP()
ENDPROC

PROC UPDATE_DEBUG_WIDGETS()

ENDPROC
#ENDIF

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ INITIALIZE ╞═════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛


FUNC VECTOR GET_CREATOR_AIRCRAFT_INTERIOR_COORDS()
	RETURN <<520.0001, 4750.0000, -70.0000>>
ENDFUNC

FUNC STRING GET_CREATOR_AIRCRAFT_INTERIOR_NAME()
	RETURN "xm_x17dlc_int_01"
ENDFUNC

PROC ADD_CREATOR_AIRCRAFT_ENTITY_SET(STRING sEntitySet)
	INTERIOR_INSTANCE_INDEX armoryAircraftInteriorID = GET_INTERIOR_AT_COORDS_WITH_TYPE(GET_CREATOR_AIRCRAFT_INTERIOR_COORDS(), GET_CREATOR_AIRCRAFT_INTERIOR_NAME())

	IF IS_VALID_INTERIOR(armoryAircraftInteriorID)
		IF NOT IS_STRING_NULL_OR_EMPTY(sEntitySet)
			IF NOT IS_INTERIOR_ENTITY_SET_ACTIVE(armoryAircraftInteriorID, sEntitySet)
				ACTIVATE_INTERIOR_ENTITY_SET(armoryAircraftInteriorID, sEntitySet)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ADD_CREATOR_AIRCRAFT_ENTITY_SET - Activating: ", sEntitySet)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC REMOVE_CREATOR_AIRCRAFT_ENTITY_SET(STRING sEntitySet)
	INTERIOR_INSTANCE_INDEX armoryAircraftInteriorID = GET_INTERIOR_AT_COORDS_WITH_TYPE(GET_CREATOR_AIRCRAFT_INTERIOR_COORDS(), GET_CREATOR_AIRCRAFT_INTERIOR_NAME())
	
	IF IS_VALID_INTERIOR(armoryAircraftInteriorID)
		IF NOT IS_STRING_NULL_OR_EMPTY(sEntitySet)
			IF IS_INTERIOR_ENTITY_SET_ACTIVE(armoryAircraftInteriorID, sEntitySet)
				DEACTIVATE_INTERIOR_ENTITY_SET(armoryAircraftInteriorID, sEntitySet)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "REMOVE_CREATOR_AIRCRAFT_ENTITY_SET - Deactivating: ", sEntitySet)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CONFIGURE_CREATOR_AIRCRAFT_ENTITY_SET(BOOL bRemove = FALSE)
	IF bRemove
		REMOVE_CREATOR_AIRCRAFT_ENTITY_SET("CONTROL_1")
		REMOVE_CREATOR_AIRCRAFT_ENTITY_SET("CONTROL_2")
		REMOVE_CREATOR_AIRCRAFT_ENTITY_SET("CONTROL_3")
		REMOVE_CREATOR_AIRCRAFT_ENTITY_SET("WEAPONS_MOD")
		REMOVE_CREATOR_AIRCRAFT_ENTITY_SET("VEHICLE_MOD")
		REMOVE_CREATOR_AIRCRAFT_ENTITY_SET("SHELL_TINT")
	ENDIF	
	ADD_CREATOR_AIRCRAFT_ENTITY_SET("CONTROL_1")
	ADD_CREATOR_AIRCRAFT_ENTITY_SET("CONTROL_2")
	ADD_CREATOR_AIRCRAFT_ENTITY_SET("CONTROL_3")
	ADD_CREATOR_AIRCRAFT_ENTITY_SET("WEAPONS_MOD")
	ADD_CREATOR_AIRCRAFT_ENTITY_SET("VEHICLE_MOD")
	ADD_CREATOR_AIRCRAFT_ENTITY_SET("SHELL_TINT")
ENDPROC

OBJECT_INDEX oChairOne
OBJECT_INDEX oChairTwo
OBJECT_INDEX oChairThree
SCRIPT_TIMER sTintFailTimer
FUNC BOOL LOAD_INTERIOR_TINTS(BOOL bChairsOnly = FALSE)
	INT iTint = 1
	
	IF thisCreatorAircraft.pOwner = INVALID_PLAYER_INDEX()
		RETURN FALSE
	ENDIF
	
	IF !bChairsOnly
		IF NOT HAS_NET_TIMER_STARTED(sTintFailTimer)
			START_NET_TIMER(sTintFailTimer)
		ENDIF	
	ENDIF

	BOOL bReady = TRUE
	
	IF NOT bChairsOnly
		PRINTLN("LOAD_INTERIOR_TINTS iTint: ", iTint)
		
		IF !SET_TINT_INDEX_CLOSEST_BUILDING_OF_TYPE(<<523.0380,4750.5483,-67.6263>>, 50 , INT_TO_ENUM(MODEL_NAMES, HASH("xm_int_x17DLC_Tint_Ribs_01a")), iTint )
			PRINTLN("LOAD_INTERIOR_TINTS xm_int_x17DLC_Tint_Ribs_01a false")
			bReady = FALSE
		ENDIF
		IF !SET_TINT_INDEX_CLOSEST_BUILDING_OF_TYPE(<<523.0380,4750.5483,-67.6263>>, 50 , INT_TO_ENUM(MODEL_NAMES, HASH("xm_int_x17DLC_Tint_Fabric_01a")), iTint )
			PRINTLN("LOAD_INTERIOR_TINTS xm_int_x17DLC_Tint_Fabric_01a false")
			bReady = FALSE
		ENDIF
		IF !SET_TINT_INDEX_CLOSEST_BUILDING_OF_TYPE(<<523.0380,4750.5483,-67.6263>>, 50 , INT_TO_ENUM(MODEL_NAMES, HASH("xm_Int_x17DLC_Tint_Ribs_02a")), iTint )
			PRINTLN("LOAD_INTERIOR_TINTS xm_Int_x17DLC_Tint_Ribs_02a false")
			bReady = FALSE
		ENDIF
		IF !SET_TINT_INDEX_CLOSEST_BUILDING_OF_TYPE(<<523.0380,4750.5483,-67.6263>>, 50 , INT_TO_ENUM(MODEL_NAMES, HASH("xm_Int_x17DLC_Tint_Strut_01a")), iTint )
			PRINTLN("LOAD_INTERIOR_TINTS xm_Int_x17DLC_Tint_Strut_01a false")
			bReady = FALSE
		ENDIF
//		IF !SET_TINT_INDEX_CLOSEST_BUILDING_OF_TYPE(<<523.0380,4750.5483,-67.6263>>, 50 , INT_TO_ENUM(MODEL_NAMES, HASH("xm_Int_x17DLC_Tint_Pipes_02a")), iTint )
//			PRINTLN("LOAD_INTERIOR_TINTS xm_Int_x17DLC_Tint_Pipes_02a false")
//			bReady = FALSE
//		ENDIF
		IF !SET_TINT_INDEX_CLOSEST_BUILDING_OF_TYPE(<<523.0380,4750.5483,-67.6263>>, 50 , INT_TO_ENUM(MODEL_NAMES, HASH("xm_Int_x17DLC_Tint_Runners_01a")), iTint )
			PRINTLN("LOAD_INTERIOR_TINTS xm_Int_x17DLC_Tint_Runners_01a false")
			bReady = FALSE
		ENDIF
		IF !SET_TINT_INDEX_CLOSEST_BUILDING_OF_TYPE(<<523.0380,4750.5483,-67.6263>>, 50 , INT_TO_ENUM(MODEL_NAMES, HASH("xm_Int_x17DLC_Tint_Shell_01a")), iTint )
			PRINTLN("LOAD_INTERIOR_TINTS xm_Int_x17DLC_Tint_Shell_01a false")
			bReady = FALSE
		ENDIF
		IF !SET_TINT_INDEX_CLOSEST_BUILDING_OF_TYPE(<<523.0380,4750.5483,-67.6263>>, 50 , INT_TO_ENUM(MODEL_NAMES, HASH("xm_Int_x17DLC_Tint_Shell_01a")), iTint )
			PRINTLN("LOAD_INTERIOR_TINTS xm_Int_x17DLC_Tint_Shell_01a false")
			bReady = FALSE
		ENDIF
		IF !SET_TINT_INDEX_CLOSEST_BUILDING_OF_TYPE(<<514.8364,4750.5464,-69.3338>>, 50 , INT_TO_ENUM(MODEL_NAMES, HASH("xm_Int_x17DLC_Tint_seat_01b")), iTint )
			PRINTLN("LOAD_INTERIOR_TINTS xm_Int_x17DLC_Tint_Shell_01a false")
			bReady = FALSE
		ENDIF
		IF !SET_TINT_INDEX_CLOSEST_BUILDING_OF_TYPE(<<514.8749,4748.1060,-70.0003>>, 50 , INT_TO_ENUM(MODEL_NAMES, HASH("xm_Int_x17DLC_cctv_unit")), iTint )
			PRINTLN("LOAD_INTERIOR_TINTS xm_Int_x17DLC_Tint_Shell_01a false")
			bReady = FALSE
		ENDIF
		IF !SET_TINT_INDEX_CLOSEST_BUILDING_OF_TYPE(<<513.3058,4750.4287,-68.2650>>, 50 , INT_TO_ENUM(MODEL_NAMES, HASH("xm_Int_x17DLC_tint_netting_01a")), iTint )
			PRINTLN("LOAD_INTERIOR_TINTS xm_Int_x17DLC_Tint_Shell_01a false")
			bReady = FALSE
		ENDIF
		IF !SET_TINT_INDEX_CLOSEST_BUILDING_OF_TYPE(<<523.0380,4750.5483,-67.6263>>, 50 , INT_TO_ENUM(MODEL_NAMES, HASH("xm_OSP_Control_Station_01a")), iTint )
			PRINTLN("LOAD_INTERIOR_TINTS xm_OSP_Control_Station_01a false")
			bReady = FALSE
		ENDIF
		IF !SET_TINT_INDEX_CLOSEST_BUILDING_OF_TYPE(<<523.0380,4750.5483,-67.6263>>, 50 , INT_TO_ENUM(MODEL_NAMES, HASH("xm_OSP_Control_Station_01b")), iTint )
			PRINTLN("LOAD_INTERIOR_TINTS xm_OSP_Control_Station_01b false")
			bReady = FALSE
		ENDIF
		IF !SET_TINT_INDEX_CLOSEST_BUILDING_OF_TYPE(<<523.0380,4750.5483,-67.6263>>, 50 , INT_TO_ENUM(MODEL_NAMES, HASH("xm_OSP_Control_Station_01c")), iTint )
			PRINTLN("LOAD_INTERIOR_TINTS xm_OSP_Control_Station_01c false")
			bReady = FALSE
		ENDIF
		IF !SET_TINT_INDEX_CLOSEST_BUILDING_OF_TYPE(<<523.0380,4750.5483,-67.6263>>, 50 , INT_TO_ENUM(MODEL_NAMES, HASH("xm_OSP_Gun_Mod_Station")), iTint )
			PRINTLN("LOAD_INTERIOR_TINTS xm_OSP_Gun_Mod_Station false")
			bReady = FALSE
		ENDIF
		IF !SET_TINT_INDEX_CLOSEST_BUILDING_OF_TYPE(<<523.0380,4750.5483,-67.6263>>, 50 , INT_TO_ENUM(MODEL_NAMES, HASH("xm_OSP_Vehicle_Mod_Station")), iTint )
			PRINTLN("LOAD_INTERIOR_TINTS xm_OSP_Vehicle_Mod_Station false")
			bReady = FALSE
		ENDIF
		
		IF iTint != 0
			TEXT_LABEL_31 sTimerCycle = "mp_x17dlc_int_01_tint"
			sTimerCycle += iTint
			PRINTLN("LOAD_INTERIOR_TINTS time cycle: ", sTimerCycle)
			SET_TIMECYCLE_MODIFIER(sTimerCycle)	
		ENDIF
		
	ELSE
		
		IF DOES_ENTITY_EXIST(oChairOne)
			IF NETWORK_HAS_CONTROL_OF_ENTITY(oChairOne)
				SET_OBJECT_TINT_INDEX(oChairOne, iTint)
			ENDIF	
		ELSE
			oChairOne = GET_CLOSEST_OBJECT_OF_TYPE(<<513.2440,4749.9961,-69.3940>>, 5 ,INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_x17_avengerchair")), FALSE, FALSE, FALSE)
		ENDIF
		
		IF DOES_ENTITY_EXIST(oChairTwo)
			IF NETWORK_HAS_CONTROL_OF_ENTITY(oChairTwo)
				SET_OBJECT_TINT_INDEX(oChairTwo, iTint)
			ENDIF	
		ELSE
			oChairTwo = GET_CLOSEST_OBJECT_OF_TYPE(<<512.4060,4749.7319,-69.3940>>, 5 ,INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_x17_avengerchair")), FALSE, FALSE, FALSE)
		ENDIF
		
		IF DOES_ENTITY_EXIST(oChairThree)
			IF NETWORK_HAS_CONTROL_OF_ENTITY(oChairThree)
				SET_OBJECT_TINT_INDEX(oChairThree, iTint)
			ENDIF	
		ELSE
			oChairThree = GET_CLOSEST_OBJECT_OF_TYPE(<<515.9560,4751.0449,-69.3940>>, 5 ,INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_x17_avengerchair")), FALSE, FALSE, FALSE)
		ENDIF
			
	ENDIF
	
	IF !bChairsOnly
		IF HAS_NET_TIMER_STARTED(sTintFailTimer)
			IF HAS_NET_TIMER_EXPIRED(sTintFailTimer, 10000)
				IF !bReady 
					bReady = TRUE
					PRINTLN("LOAD_INTERIOR_TINTS - hitting fail timer...")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN bReady
ENDFUNC	

PROC INITIALISE_ENTITY_SETS()
	VECTOR vInteriorPosition
	FLOAT fInteriorHeading
	TEXT_LABEL_63 sInteriorType
	
	GET_SIMPLE_INTERIOR_INTERIOR_TYPE_AND_POSITION(thisCreatorAircraft.eSimpleInteriorID, sInteriorType, vInteriorPosition, fInteriorHeading)
	thisCreatorAircraft.iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(vInteriorPosition, sInteriorType)
	CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_CREATOR_AIRCRAFT - INITIALISE_ENTITY_SETS - thisCreatorAircraft.iInteriorIndex = ", NATIVE_TO_INT(thisCreatorAircraft.iInteriorIndex))
	
	IF IS_VALID_INTERIOR(thisCreatorAircraft.iInteriorIndex)
		CONFIGURE_CREATOR_AIRCRAFT_ENTITY_SET(TRUE)
	ENDIF
ENDPROC

PROC INITIALISE()	
	// Get simple interior details
	GET_SIMPLE_INTERIOR_DETAILS(thisCreatorAircraft.eSimpleInteriorID, thisCreatorAircraft.interiorDetails)
	
	INITIALISE_ENTITY_SETS()	
	
	SET_EMPTY_AVENGER_HOLD(FALSE)
	SET_KICK_EVERYONE_OUT_OF_AVENGER(FALSE)
	
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF GET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID())
			SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
		ENDIF
	ENDIF	
	RESET_ADAPTATION(1)
	#IF IS_DEBUG_BUILD
	CREATE_DEBUG_WIDGETS()
	CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_CREATOR_AIRCRAFT - INITIALISE - Done.")
	#ENDIF
ENDPROC

FUNC BOOL IS_LOCAL_PLAYER_WITHIN_CREATOR_AIRCRAFT_BOUNDS()
	SIMPLE_INTERIOR_INSIDE_BOUNDING_BOX bounds
	GET_SIMPLE_INTERIOR_INSIDE_BOUNDING_BOX(thisCreatorAircraft.eSimpleInteriorID, bounds)
	
	BOOL bInBounds = IS_ENTITY_IN_AREA(PLAYER_PED_ID(), bounds.vInsideBBoxMin, bounds.vInsideBBoxMax)
	
	#IF IS_DEBUG_BUILD
	VECTOR vPlayerCoords = GET_PLAYER_COORDS(PLAYER_ID())
	CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_CREATOR_AIRCRAFT - IS_LOCAL_PLAYER_WITHIN_CREATOR_AIRCRAFT_BOUNDS - Player Position: ", vPlayerCoords)
	CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_CREATOR_AIRCRAFT - IS_LOCAL_PLAYER_WITHIN_CREATOR_AIRCRAFT_BOUNDS - Bounds : Min = ", bounds.vInsideBBoxMin, ", Max = ", bounds.vInsideBBoxMax)
	#ENDIF
	
	RETURN bInBounds
ENDFUNC

FUNC BOOL DETERMINE_INTERIOR_OWNER()
		IF IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_CREATOR_TRAILER_ACCESS_BS_OWNER)
	OR GB_GET_LOCAL_PLAYER_GANG_BOSS() = PLAYER_ID()
		thisCreatorAircraft.pOwner = PLAYER_ID()
		g_ownerOfArmoryAircraftPropertyIAmIn = thisCreatorAircraft.pOwner
		PRINTLN("AM_MP_CREATOR_AIRCRAFT - DETERMINE_INTERIOR_OWNER - I ", GET_PLAYER_NAME(thisCreatorAircraft.pOwner), " am the owner. access BS =  ", g_SimpleInteriorData.iAccessBS)
		RETURN TRUE
	ELIF g_SimpleInteriorData.iAccessBS > 0
	OR IS_PLAYER_MOVING_FROM_BASE_TO_AIRCRAFT()
		IF globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner != INVALID_PLAYER_INDEX()
			thisCreatorAircraft.pOwner = globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner
			g_ownerOfArmoryAircraftPropertyIAmIn = thisCreatorAircraft.pOwner
			PRINTLN("AM_MP_CREATOR_AIRCRAFT - DETERMINE_INTERIOR_OWNER - I am not the owner. The owner is: ", GET_PLAYER_NAME(thisCreatorAircraft.pOwner))
			RETURN TRUE
		ENDIF
		IF IS_PLAYER_MOVING_FROM_BASE_TO_AIRCRAFT()
			IF IS_PLAYER_NEED_TO_CHECK_FOR_NEARBY_PLAYERS_IN_BASE(PLAYER_ID())
				thisCreatorAircraft.pOwner = PLAYER_ID()
				g_ownerOfArmoryAircraftPropertyIAmIn = PLAYER_ID()
				SET_BIT(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_CREATOR_AIRCRAFT_ACCESS_BS_OWNER)
				SET_LOCAL_PLAYER_CHECK_FOR_NEARBY_PLAYERS_IN_BASE(FALSE)
				PRINTLN("AM_MP_ARMORY_TRUCK - DETERMINE_INTERIOR_OWNER - IS_PLAYER_MOVING_FROM_BASE_TO_AIRCRAFT I'm owner.")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	PRINTLN("AM_MP_CREATOR_AIRCRAFT - DETERMINE_INTERIOR_OWNER - g_SimpleInteriorData.iAccessBS = ", g_SimpleInteriorData.iAccessBS)
	#ENDIF
	
	IF (GET_FRAME_COUNT() % 60) = 0
		PRINTLN("AM_MP_CREATOR_AIRCRAFT - DETERMINE_CREATOR_TRAILER_OWNER - Trying to find the owner")
	ENDIF
	
	RETURN FALSE
ENDFUNC 

PROC SCRIPT_INITIALISE(SIMPLE_INTERIOR_CHILD_SCRIPT_DATA& scriptData)
	
	#IF IS_DEBUG_BUILD
	CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][CREATOR_AIRCRAFT] AM_MP_CREATOR_AIRCRAFT - SCRIPT_INITIALISE - Launching instance ", thisCreatorAircraft.iScriptInstance)
	#ENDIF
	
	IF IS_PHONE_ONSCREEN()
		HANG_UP_AND_PUT_AWAY_PHONE()
	ENDIF
	
	WHILE NOT DETERMINE_INTERIOR_OWNER()
		IF NOT NETWORK_IS_GAME_IN_PROGRESS()
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_CREATOR_AIRCRAFT - SCRIPT_INITIALISE - NETWORK_IS_GAME_IN_PROGRESS = FALSE")
			SCRIPT_CLEANUP()
		ENDIF
		
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_CREATOR_AIRCRAFT - SCRIPT_INITIALISE - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE = TRUE")
			SCRIPT_CLEANUP()
		ENDIF
		
		IF SHOULD_THIS_SIMPLE_INTERIOR_CHILD_THREAD_TERMINATE(scriptData.iScriptInstance, scriptData.eSimpleInteriorID)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_CREATOR_AIRCRAFT - SCRIPT_INITIALISE - SHOULD_THIS_SIMPLE_INTERIOR_CHILD_THREAD_TERMINATE = TRUE")
			SCRIPT_CLEANUP()
		ENDIF
		
		IF IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_CREATOR_AIRCRAFT_ACCESS_BS_OWNER_LEFT_GAME)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_CREATOR_AIRCRAFT - SCRIPT_INITIALISE - SIMPLE_INTERIOR_CREATOR_AIRCRAFT_ACCESS_BS_OWNER_LEFT_GAME = TRUE")
			SCRIPT_CLEANUP()
		ENDIF
		
		WAIT(0)
	ENDWHILE
	
	thisCreatorAircraft.eSimpleInteriorID 	= scriptData.eSimpleInteriorID
	thisCreatorAircraft.iScriptInstance 	= scriptData.iScriptInstance
	thisCreatorAircraft.iInvitingPlayer 	= scriptData.iInvitingPlayer
	
	g_iGunModInstance = thisCreatorAircraft.iScriptInstance
	
	IF thisCreatorAircraft.pOwner = INVALID_PLAYER_INDEX()
		#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][CREATOR_AIRCRAFT] AM_MP_CREATOR_AIRCRAFT - SCRIPT_INITIALISE - Owner of this CREATOR_AIRCRAFT is invalid, exiting...")
		#ENDIF
		SET_SIMPLE_INTERIOR_CHILD_SCRIPT_READY(TRUE)
		SCRIPT_CLEANUP()
	ENDIF
	
	NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE, thisCreatorAircraft.iScriptInstance)
	
	// This makes sure the net script is active, waits untill it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
	
	// KGM: Wait for the first network broadcast before moving on
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		PRINTLN("AM_MP_CREATOR_AIRCRAFT - FAILED TO RECEIVE INITIAL NETWORK BROADCAST - SCRIPT CLEANUP")
		SCRIPT_CLEANUP()
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		// This script will not be paused if another script calls PAUSE_GAME
		PRINTLN("AM_MP_CREATOR_AIRCRAFT - INITIALISED")
	ELSE
		PRINTLN("AM_MP_CREATOR_AIRCRAFT - NETWORK_IS_GAME_IN_PROGRESS = FALSE - SCRIPT CLEANUP")
		SCRIPT_CLEANUP()
	ENDIF
	
	INITIALISE()
	
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════╡ MAIN LOGIC PROC ╞══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC MAINTAIN_INTERIOR_LOAD()
	VECTOR vInteriorPosition
	FLOAT fInteriorHeading
	TEXT_LABEL_63 sInteriorType
	
	IF NOT IS_BIT_SET(thisCreatorAircraft.iBS, BS_CREATOR_AIRCRAFT_INTERIOR_REFRESHED_ON_INIT)
		IF NOT thisCreatorAircraft.bScriptRelaunched
			
			GET_SIMPLE_INTERIOR_INTERIOR_TYPE_AND_POSITION(thisCreatorAircraft.eSimpleInteriorID, sInteriorType, vInteriorPosition, fInteriorHeading)
			thisCreatorAircraft.iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(vInteriorPosition, sInteriorType)
			
			IF IS_VALID_INTERIOR(thisCreatorAircraft.iInteriorIndex) AND IS_INTERIOR_READY(thisCreatorAircraft.iInteriorIndex)
				BOOL bReady = TRUE
				
				IF bReady
					IF IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
						FORCE_ROOM_FOR_ENTITY(PLAYER_PED_ID(), thisCreatorAircraft.iInteriorIndex, ARMORY_AIRCRAFT_ROOM_KEY)
						PRINTLN("AM_MP_CREATOR_AIRCRAFT - MAINTAIN_INTERIOR_LOAD - Forcing room for player, we are inside the creator aircraft.")
					ELSE
						PRINTLN("AM_MP_CREATOR_AIRCRAFT - MAINTAIN_INTERIOR_LOAD - Preventing room force, not inside the creator aircraft yet.")
					ENDIF
					
					REPIN_AND_REFRESH_SIMPLE_INTERIOR()
					
					SET_SIMPLE_INTERIOR_CHILD_SCRIPT_ALLOWS_FOR_WARP(TRUE)
					SET_BIT(thisCreatorAircraft.iBS, BS_CREATOR_AIRCRAFT_INTERIOR_REFRESHED_ON_INIT)
					CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_CREATOR_AIRCRAFT - MAINTAIN_INTERIOR_LOAD - Setting BS_CREATOR_AIRCRAFT_INTERIOR_REFRESHED_ON_INIT")
					
					RESET_NET_TIMER(thisCreatorAircraft.tPinInMemTimer)
				ENDIF
			ELSE
				IF NOT HAS_NET_TIMER_STARTED(thisCreatorAircraft.tPinInMemTimer)
					PRINTLN("AM_MP_CREATOR_AIRCRAFT - MAINTAIN_INTERIOR_LOAD - Pinning interior with ID: ", NATIVE_TO_INT(thisCreatorAircraft.iInteriorIndex))
					PIN_INTERIOR_IN_MEMORY_VALIDATE(thisCreatorAircraft.iInteriorIndex)
					START_NET_TIMER(thisCreatorAircraft.tPinInMemTimer)
				ENDIF
				
				IF HAS_NET_TIMER_EXPIRED(thisCreatorAircraft.tPinInMemTimer, 12500)
					RESET_NET_TIMER(thisCreatorAircraft.tPinInMemTimer)
				ENDIF
				
				PRINTLN("AM_MP_CREATOR_AIRCRAFT - MAINTAIN_INTERIOR_LOAD - Waiting for a valid and ready interior with ID: ", NATIVE_TO_INT(thisCreatorAircraft.iInteriorIndex), " valid? ", 
					IS_VALID_INTERIOR(thisCreatorAircraft.iInteriorIndex), " ready? ", IS_INTERIOR_READY(thisCreatorAircraft.iInteriorIndex), " disabled? ", IS_INTERIOR_DISABLED(thisCreatorAircraft.iInteriorIndex))
			ENDIF
		ELSE
			SET_SIMPLE_INTERIOR_CHILD_SCRIPT_ALLOWS_FOR_WARP(TRUE)
			SET_BIT(thisCreatorAircraft.iBS, BS_CREATOR_AIRCRAFT_INTERIOR_REFRESHED_ON_INIT)
			CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_CREATOR_AIRCRAFT - MAINTAIN_INTERIOR_LOAD - Setting BS_CREATOR_AIRCRAFT_INTERIOR_REFRESHED_ON_INIT")
		ENDIF	
	ENDIF
	
	IF CAN_SIMPLE_INTERIOR_CHILD_SCRIPT_TAKE_CONTROL()
	AND IS_BIT_SET(thisCreatorAircraft.iBS, BS_CREATOR_AIRCRAFT_INTERIOR_REFRESHED_ON_INIT)	
		BOOL bReady = TRUE
		
		IF NOT IS_BIT_SET(thisCreatorAircraft.iBS, BS_CREATOR_AIRCRAFT_LOAD_SCENE_STARTED)
			VECTOR vBaseCoords
			FLOAT fBaseHeading
			
			SIMPLE_INTERIOR_INSIDE_BOUNDING_BOX bounds
			GET_SIMPLE_INTERIOR_INSIDE_BOUNDING_BOX(thisCreatorAircraft.eSimpleInteriorID, bounds)
			GET_SIMPLE_INTERIOR_INTERIOR_TYPE_AND_POSITION(thisCreatorAircraft.eSimpleInteriorID, sInteriorType, vBaseCoords, fBaseHeading)			
			vBaseCoords.z = vBaseCoords.z + 1
			
			SET_BIT(thisCreatorAircraft.iBS, BS_CREATOR_AIRCRAFT_LOAD_SCENE_STARTED)
			PRINTLN("AM_MP_CREATOR_AIRCRAFT - MAINTAIN_INTERIOR_LOAD - Setting BS_CREATOR_AIRCRAFT_LOAD_SCENE_STARTED")
			
			bReady = FALSE
		ELSE
			IF IS_NEW_LOAD_SCENE_ACTIVE()
				IF NOT IS_NEW_LOAD_SCENE_LOADED()
					PRINTLN("AM_MP_CREATOR_AIRCRAFT - MAINTAIN_INTERIOR_LOAD - BS_CREATOR_AIRCRAFT_LOAD_SCENE_STARTED - IS_NEW_LOAD_SCENE_LOADED = FALSE")
					bReady = FALSE
				ELSE
					GET_SIMPLE_INTERIOR_INTERIOR_TYPE_AND_POSITION(thisCreatorAircraft.eSimpleInteriorID, sInteriorType, vInteriorPosition, fInteriorHeading)
					thisCreatorAircraft.iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(vInteriorPosition, sInteriorType)
					
					IF IS_VALID_INTERIOR(thisCreatorAircraft.iInteriorIndex)
					AND IS_INTERIOR_READY(thisCreatorAircraft.iInteriorIndex)
					AND IS_LOCAL_PLAYER_WITHIN_CREATOR_AIRCRAFT_BOUNDS()
						PRINTLN("AM_MP_CREATOR_AIRCRAFT - MAINTAIN_INTERIOR_LOAD - Inside creator aircraft bounds, load scene is ready.")
						NEW_LOAD_SCENE_STOP()
					ELSE
						SET_BIT(thisCreatorAircraft.iBS, BS_CREATOR_AIRCRAFT_KILL_LOAD_SCENE_WHEN_INSIDE)
						PRINTLN("AM_MP_CREATOR_AIRCRAFT - MAINTAIN_INTERIOR_LOAD - Setting BS_CREATOR_AIRCRAFT_KILL_LOAD_SCENE_WHEN_INSIDE")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT LOAD_INTERIOR_TINTS()
			bReady = FALSE
			PRINTLN("[SIMPLE_INTERIOR] AM_MP_CREATOR_AIRCRAFT - MAINTAIN_INTERIOR_LOAD - waiting on LOAD_INTERIOR_TINTS")
		ENDIF
		
		IF IS_BIT_SET(thisCreatorAircraft.iBS, BS_CREATOR_AIRCRAFT_INIT_FADE_IN_TIMER)
			IF NOT HAS_NET_TIMER_STARTED(sFadeInTimer)
				START_NET_TIMER(sFadeInTimer)
			ELSE	
				IF HAS_NET_TIMER_EXPIRED(sFadeInTimer, 1000)
					PRINTLN("[SIMPLE_INTERIOR] AM_MP_CREATOR_AIRCRAFT - we hit 1 seconds")
				ELSE
					PRINTLN("[SIMPLE_INTERIOR] AM_MP_CREATOR_AIRCRAFT - waiting on start timer")
					RESET_ADAPTATION(1)
					bReady = FALSE
				ENDIF
			ENDIF	
		ENDIF
		
		IF bReady
			SET_SIMPLE_INTERIOR_CHILD_SCRIPT_ALLOWS_FOR_WARP(TRUE)
			SET_SIMPLE_INTERIOR_CHILD_SCRIPT_READY(TRUE)
			SET_CONCEAL_PLAYER_FOR_PILOT_SEAT_TRANS(FALSE)
			SET_CREATOR_AIRCRAFT_STATE(CREATOR_AIRCRAFT_STATE_IDLE)
		ENDIF
	ELSE
		BOOL bResult = IS_BIT_SET(thisCreatorAircraft.iBS, BS_CREATOR_AIRCRAFT_INTERIOR_REFRESHED_ON_INIT)
		PRINTLN("AM_MP_CREATOR_AIRCRAFT - MAINTAIN_INTERIOR_LOAD - CAN_SIMPLE_INTERIOR_CHILD_SCRIPT_TAKE_CONTROL = FALSE & BS_CREATOR_AIRCRAFT_INTERIOR_REFRESHED_ON_INIT = ", bResult)
	ENDIF
ENDPROC

PROC RUN_CLIENT_ACTIVITIES()
	interiorStruct.pFactoryOwner 	= thisCreatorAircraft.pOwner
	interiorStruct.vInteriorPos 	= <<1103.562,-3014.000,-40.000>>
	interiorStruct.iInterior 		= GET_HASH_KEY("FACTORY_TRUCK")
//	IF interiorStruct.iInterior != 0
		INIT_ACTIVITY_LAUNCH_SCRIPT_CHECK_FOR_OSPREY(activityControllerStruct, activityCheck, serverBD.activityProps, interiorStruct)
		MAINTAIN_APARTMENT_ACTIVITIES_CREATOR(activityControllerStruct, activityCheck, serverBD.activityProps, activityPeds)
//	ENDIF
ENDPROC

PROC UPDATE_AVENGER_EXIT_MENU_OPTION()
	IF NOT IS_BIT_SET(thisCreatorAircraft.iBS ,BS_CREATOR_AIRCRAFT_CAN_LEAVE_TO_COCKPIT_PASSENGER)
		IF CAN_PLAYER_MOVE_TO_CREATOR_AIRCRAFT_PILOT_SEAT()
			CLEAR_BIT(thisCreatorAircraft.iBS ,BS_CREATOR_AIRCRAFT_CAN_NOT_LEAVE_TO_COCKPIT_PASSENGER)
			SET_BIT(thisCreatorAircraft.iBS ,BS_CREATOR_AIRCRAFT_CAN_LEAVE_TO_COCKPIT_PASSENGER)
			g_SimpleInteriorData.bRefreshInteriorMenu = TRUE
		ENDIF	
	ENDIF
	IF NOT IS_BIT_SET(thisCreatorAircraft.iBS ,BS_CREATOR_AIRCRAFT_CAN_NOT_LEAVE_TO_COCKPIT_PASSENGER)
		IF !CAN_PLAYER_MOVE_TO_CREATOR_AIRCRAFT_PILOT_SEAT()	
			SET_BIT(thisCreatorAircraft.iBS ,BS_CREATOR_AIRCRAFT_CAN_NOT_LEAVE_TO_COCKPIT_PASSENGER)
			CLEAR_BIT(thisCreatorAircraft.iBS ,BS_CREATOR_AIRCRAFT_CAN_LEAVE_TO_COCKPIT_PASSENGER)
			g_SimpleInteriorData.bRefreshInteriorMenu = TRUE
		ENDIF	
	ENDIF
ENDPROC

PROC MAINTAIN_CREATOR_AIRCRAFT_BLIPS()
	IF NOT IS_PLAYER_IN_ZOOMED_OUT_RADAR_OR_MAP(PLAYER_ID())
	AND NOT g_bActiveInGunTurret
		IF NOT DOES_BLIP_EXIST(thisCreatorAircraft.bTurretBlips[0])
		AND IS_AVENGER_TURRET_STATION_B_AVAILABLE()
			thisCreatorAircraft.bTurretBlips[0] = CREATE_BLIP_FOR_COORD(<<510.7982, 4749.6372, -70.0>>)
			SET_BLIP_SPRITE(thisCreatorAircraft.bTurretBlips[0], RADAR_TRACE_NHP_TURRET_CONSOLE)
			SET_BLIP_NAME_FROM_TEXT_FILE(thisCreatorAircraft.bTurretBlips[0], "BLIP_606")
			SET_BLIP_DISPLAY(thisCreatorAircraft.bTurretBlips[0], DISPLAY_BOTH)
		ENDIF
		
		IF NOT DOES_BLIP_EXIST(thisCreatorAircraft.bTurretBlips[1])
		AND IS_AVENGER_TURRET_STATION_A_AVAILABLE()
			thisCreatorAircraft.bTurretBlips[1] = CREATE_BLIP_FOR_COORD(<<513.2801, 4748.1621, -70.0>>)
			SET_BLIP_SPRITE(thisCreatorAircraft.bTurretBlips[1], RADAR_TRACE_NHP_TURRET_CONSOLE)
			SET_BLIP_NAME_FROM_TEXT_FILE(thisCreatorAircraft.bTurretBlips[1], "BLIP_606")
			SET_BLIP_DISPLAY(thisCreatorAircraft.bTurretBlips[1], DISPLAY_BOTH)
		ENDIF
		
		IF NOT DOES_BLIP_EXIST(thisCreatorAircraft.bTurretBlips[2])
		AND IS_AVENGER_TURRET_STATION_C_AVAILABLE()
			thisCreatorAircraft.bTurretBlips[2] = CREATE_BLIP_FOR_COORD(<<515.8378, 4752.7524, -70.0>>)
			SET_BLIP_SPRITE(thisCreatorAircraft.bTurretBlips[2], RADAR_TRACE_NHP_TURRET_CONSOLE)
			SET_BLIP_NAME_FROM_TEXT_FILE(thisCreatorAircraft.bTurretBlips[2], "BLIP_606")
			SET_BLIP_DISPLAY(thisCreatorAircraft.bTurretBlips[2], DISPLAY_BOTH)
		ENDIF
	ELSE
		IF DOES_BLIP_EXIST(thisCreatorAircraft.bTurretBlips[0])
			REMOVE_BLIP(thisCreatorAircraft.bTurretBlips[0])
		ENDIF
		
		IF DOES_BLIP_EXIST(thisCreatorAircraft.bTurretBlips[1])
			REMOVE_BLIP(thisCreatorAircraft.bTurretBlips[1])
		ENDIF
		
		IF DOES_BLIP_EXIST(thisCreatorAircraft.bTurretBlips[2])
			REMOVE_BLIP(thisCreatorAircraft.bTurretBlips[2])
		ENDIF
	ENDIF
ENDPROC

PROC MANAGE_TURRET_AUDIO_SCENE()
	IF g_bPlayerViewingTurretHud
		IF IS_AUDIO_SCENE_ACTIVE("dlc_xm_avenger_interior_scene")
			STOP_AUDIO_SCENE("dlc_xm_avenger_interior_scene")
		ENDIF
	ELSE
		IF NOT IS_AUDIO_SCENE_ACTIVE("dlc_xm_avenger_interior_scene")
			START_AUDIO_SCENE("dlc_xm_avenger_interior_scene")
		ENDIF
	ENDIF	
ENDPROC


PROC MAINTAIN_VEHICLE_ZOOM()
	IF NOT IS_PLAYER_IN_ZOOMED_OUT_RADAR_OR_MAP(PLAYER_ID())
	AND (IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
	OR IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID()))
		SET_RADAR_ZOOM_PRECISE(m_vehicle_zoomed_in_value)
	ELSE
		SET_RADAR_ZOOM_PRECISE(m_vehicle_zoomed_out_value)	
	ENDIF
ENDPROC

PROC MAINTAIN_COCKPIT_ENTRY_HELP()
	IF IS_SCREEN_FADED_IN()
		IF IS_VEHICLE_SEAT_UNAVAILABLE()
			IF NOT IS_HELP_MESSAGE_ON_SCREEN()
			AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				PRINT_HELP("AV_COCK_UNAV")
				SET_VEHICLE_SEAT_UNAVAILABLE(FALSE)
			ENDIF
		ENDIF
	ENDIF	
ENDPROC

PROC RUN_MAIN_CLIENT_LOGIC()
	
	IF IS_CREATOR_AIRCRAFT_STATE(CREATOR_AIRCRAFT_STATE_LOADING)

		MAINTAIN_INTERIOR_LOAD()
		
	ELIF IS_CREATOR_AIRCRAFT_STATE(CREATOR_AIRCRAFT_STATE_IDLE)
		//MAINTAIN_CREATOR_AIRCRAFT_EXIT_CHECKS()
		UPDATE_AVENGER_EXIT_MENU_OPTION()
		RUN_CLIENT_ACTIVITIES()
		LOAD_INTERIOR_TINTS(TRUE)
		IF NOT IS_BIT_SET(thisCreatorAircraft.iBS, BS_CREATOR_AIRCRAFT_CALLED_CLEAR_HELP)
			IF IS_SCREEN_FADED_IN()
			AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
				CLEAR_HELP()
				SET_BIT(thisCreatorAircraft.iBS, BS_CREATOR_AIRCRAFT_CALLED_CLEAR_HELP)
				PRINTLN("AM_MP_CREATOR_AIRCRAFT - Calling clear help")
			ENDIF
		ENDIF
		MAINTAIN_COCKPIT_ENTRY_HELP()
		MANAGE_TURRET_AUDIO_SCENE()
		MAINTAIN_CREATOR_AIRCRAFT_BLIPS()
		MAINTAIN_VEHICLE_ZOOM()
	ENDIF
	
	
	IF NETWORK_GET_HOST_OF_THIS_SCRIPT() != INT_TO_NATIVE(PARTICIPANT_INDEX , -1)	
		g_iHostOfam_mp_armory_aircraft = NATIVE_TO_INT(NETWORK_GET_PLAYER_INDEX(NETWORK_GET_HOST_OF_THIS_SCRIPT()))
	ENDIF
	
ENDPROC

PROC RUN_MAIN_SERVER_LOGIC()
	
ENDPROC


//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ MAIN LOOP ╞═════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

SCRIPT  (SIMPLE_INTERIOR_CHILD_SCRIPT_DATA scriptData) 
	
	
	IF NETWORK_IS_GAME_IN_PROGRESS() 
		SCRIPT_INITIALISE(scriptData)	
	ELSE
		SCRIPT_CLEANUP()
	ENDIF
	
	// Main loop
	WHILE TRUE

		// One wait to rule them all. This can be the ONLY wait from here on in.....
		MP_LOOP_WAIT_ZERO()
				
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			PRINTLN("AM_MP_CREATOR_AIRCRAFT - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE = TRUE")
			SCRIPT_CLEANUP()
		ENDIF
		
		IF SHOULD_THIS_SIMPLE_INTERIOR_CHILD_THREAD_TERMINATE(scriptData.iScriptInstance, scriptData.eSimpleInteriorID)
			PRINTLN("AM_MP_CREATOR_AIRCRAFT - SHOULD_THIS_SIMPLE_INTERIOR_CHILD_THREAD_TERMINATE = TRUE")
			SCRIPT_CLEANUP()
		ENDIF
		
		RUN_MAIN_CLIENT_LOGIC()
		
		// -----------------------------------
		// Process server game logic		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			RUN_MAIN_SERVER_LOGIC()
		ENDIF
		
		#IF IS_DEBUG_BUILD
		UPDATE_DEBUG_WIDGETS()
		#ENDIF
		
	ENDWHILE
	

ENDSCRIPT
