/// Freemode Lester Heist intro cutscene
///    Dave W



USING "globals.sch"

// Game Headers
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_camera.sch"
USING "commands_hud.sch"

// Network Headers
USING "net_include.sch"
USING "net_events.sch"

// Headers

USING "net_mission.sch"
USING "net_scoring_common.sch"

USING "commands_path.sch"
USING "net_blips.sch"
USING "commands_zone.sch"


USING "hud_drawing.sch"
USING "net_ambience.sch"


USING "shop_public.sch"
USING "net_garages.sch"



USING "net_mission_details_overlay.sch"

USING "net_hud_displays.sch"

USING "fm_hold_up_tut.sch"

USING "net_wait_zero.sch"

#IF IS_DEBUG_BUILD
	USING "profiler.sch"
#ENDIF

USING "net_gang_boss.sch"

// Game States
CONST_INT GAME_STATE_INI 		0
CONST_INT GAME_STATE_INI_SPAWN	1
CONST_INT GAME_STATE_RUNNING	2
CONST_INT GAME_STATE_LEAVE		3
CONST_INT GAME_STATE_FAILED		4
CONST_INT GAME_STATE_TERMINATE_DELAY 5
CONST_INT GAME_STATE_END		6

//MP_MISSION thisMission = eFM_HOLD_UP_TUT

CONST_INT biS_AllPlayersFinished		0
CONST_INT biS_DoorBlocked				1
CONST_INT biS_StaggeredDoorBlocked		2

// Everyone can read this data, only the server can update it. 
STRUCT ServerBroadcastData
	INT iServerGameState
	INT iServerBitSet
	INT iPartWithLester
	SCRIPT_TIMER timeClearDoor
	SCRIPT_TIMER timeTerminate
	HOLD_UP_TUT_SERVER_STRUCT holdUpTutServer
	NETWORK_INDEX vehHeli
	#IF IS_DEBUG_BUILD
		NETWORK_INDEX niObj
	#ENDIF
ENDSTRUCT
ServerBroadcastData serverBD


INT iServerStaggeredLoopCount

CONST_INT iCarModProgGetCarModded			0


CONST_INT biP_DoneCutscene					0
CONST_INT biP_WantToSeeLester				1
CONST_INT biP_DoorBlocked					2 

STRUCT PlayerBroadcastData
	INT iGameState
	INT iLesterCutProg
	INT iPlayerBitSet
ENDSTRUCT
PlayerBroadcastData PlayerBD[NUM_NETWORK_PLAYERS]

INT iLesterCutProg
PED_INDEX pedLester
PED_INDEX pedPlayer
OBJECT_INDEX oWheelChair
INT iSceneID

SCRIPT_TIMER timeCheckForFade
SCRIPT_TIMER timeOccupied

structPedsForConversation speechFmLesterCut

INT iEnterWhCutProg
CAMERA_INDEX camWhCut
SCRIPT_TIMER timeWhCut


INT iBoolsBitSet

CONST_INT biL_ServerLoopedThroughEveryone		0
CONST_INT biL_StaggeredAllPlayersFinished		1
CONST_INT biL_ToldToGoToLester					2
CONST_INT biL_RequestedCut						3
CONST_INT biL_SkippedMocap						4
CONST_INT biL_TextCleared						5
CONST_INT biL_OnMission							6
CONST_INT biL_SkippedCut						7
CONST_INT biL_ToldToLoseCops					8
CONST_INT biL_DoneBlipHelp						9
CONST_INT biL_ReqCutAssets						10
CONST_INT biL_SetEndCutPos						11
CONST_INT biL_ServerStagSomeoneWithLester		12
CONST_INT biL_LesterBusy						13
CONST_INT biL_LesterBlipInactive				14
CONST_INT biL_DrawScaleform						15
CONST_INT biL_LaunchMocap						16
CONST_INT biL_UnlockedDoor						17
CONST_INT biL_GetToLester						18
CONST_INT biL_DisabledWeaponSelect				19
CONST_INT biL_StartLesterScene					20
CONST_INT biL_GivenLookAt						21
CONST_INT biL_SetDoorResetFlags					22
CONST_INT biL_CreateLester						23
CONST_INT biL_StoreDisabled						24
CONST_INT biL_SetInvicible						25
CONST_INT biL_FinishedExitCut					26
CONST_INT biL_StartWalkOut						27
CONST_INT biL_DoLesterMocap						28
CONST_INT biL_OkToStartMocap					29
CONST_INT biL_WarpedPlayerForEnd				30
CONST_INT biL_StartWarpPlayerForEnd				31

INT iBoolsBitSet2

CONST_INT biL2_PauseMenuWasActive				0

BOOL bDoScriptedCUtIntoWarehouse = TRUE

//VECTOR g_vLesterWarehouse = <<1274.7963, -1721.3986, 53.6550>>
STRING sLesterCut = "HEIST_INT" //""
//BLIP_INDEX g_blipLesterWarehouse
BLIP_INDEX blipLester

SCRIPT_TIMER timeSinceOnMission
//SCRIPT_TIMER timeMocapSkip

INTERIOR_INSTANCE_INDEX interiorLester

BOOL bPlayerNearLestersAfterHeist = FALSE
BOOL bDoPostHeistCheck


#IF IS_DEBUG_BUILD
	BOOL bDisableAllDebug = FALSE
	PROC NET_DW_PRINT(STRING sText)
		IF NOT bDisableAllDebug
			NET_NL() NET_PRINT_TIME() NET_PRINT("[am_Heist_Int] [DSW] ") NET_PRINT_STRINGS(" ", sText) NET_NL()
		ENDIF
	ENDPROC 

	PROC NET_DW_PRINT_STRING_INT(STRING sText1, INT i)
		IF NOT bDisableAllDebug
			NET_NL() NET_PRINT_TIME() NET_PRINT("[am_Heist_Int] [DSW] ") NET_PRINT_STRING_INT(sText1, i) NET_NL()
		ENDIF
	ENDPROC

	PROC NET_DW_PRINT_STRING_FLOAT(STRING sText1, FLOAT f)
		IF NOT bDisableAllDebug
			NET_NL() NET_PRINT_TIME() NET_PRINT("[am_Heist_Int] [DSW] ") NET_PRINT_STRING_FLOAT(sText1, f) NET_NL()
		ENDIF
	ENDPROC

	PROC NET_DW_PRINT_STRING_VECTOR(STRING sText1, VECTOR v)
		IF NOT bDisableAllDebug
			NET_NL() NET_PRINT_TIME() NET_PRINT("[am_Heist_Int] [DSW] ") NET_PRINT_STRING_VECTOR(sText1, v) NET_NL()
		ENDIF
	ENDPROC
	
	PROC NET_DW_PRINT_STRINGS(STRING sText1, STRING sText2)
		IF NOT bDisableAllDebug
			NET_NL() NET_PRINT_TIME() NET_PRINT("[am_Heist_Int] [DSW] ") NET_PRINT_STRINGS(sText1, sText2) NET_NL()
		ENDIF
	ENDPROC
	
	PROC NET_DW_PRINT_STRING_WITH_PLAYER_NAME(STRING sText, PLAYER_INDEX player)
		IF NOT bDisableAllDebug
			NET_NL() NET_PRINT_TIME() NET_PRINT("[am_Heist_Int] [DSW] ") NET_PRINT_STRINGS(" ", sText) NET_PRINT_STRINGS(" ", GET_PLAYER_NAME(player)) NET_NL()
		ENDIF
	ENDPROC
	
	BOOL bWdDoStaggeredDebug
	BOOL bWdCreateLester
	BOOL bWDUnlockDoor
	BOOL bWdPlayLine
	FLOAT fWdAreaToClear
	BOOL bWdClearDoor
	BOOL bWdTimeTillHelp
	INT iWdTimeTillHelp
	BOOL bWdRunObjectTest
	INT iWdObjectTest
	OBJECT_INDEX oWdObj
	PROC CREATE_WIDGETS()
		START_WIDGET_GROUP("Heist Int Cut")
			//Profiling widgets
			#IF SCRIPT_PROFILER_ACTIVE
			CREATE_SCRIPT_PROFILER_WIDGET()
			#ENDIF		
			ADD_WIDGET_BOOL("Create Lester", bWdCreateLester)
			ADD_WIDGET_BOOL("Unlock door", bWDUnlockDoor)
			ADD_WIDGET_BOOL("Play line", bWdPlayLine)
			ADD_WIDGET_BOOL("Show time until help", bWdTimeTillHelp)

			ADD_WIDGET_INT_SLIDER("Time ", iWdTimeTillHelp, 0, 1000000, 1)
			START_WIDGET_GROUP("Player")
				ADD_BIT_FIELD_WIDGET("Local", iBoolsBitSet)
				ADD_BIT_FIELD_WIDGET("Player", PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet)
				
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Server")
				ADD_WIDGET_FLOAT_SLIDER("Clear area size", fWdAreaToClear, 0.0, 100.0, 1.0)
				ADD_WIDGET_BOOL("Clear Door", bWdClearDoor)
				ADD_WIDGET_BOOL("Staggered debug ", bWdDoStaggeredDebug)
				ADD_WIDGET_INT_READ_ONLY("iPartWithLester", serverbd.iPartWithLester)
				
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Obj test")
				ADD_WIDGET_BOOL("Run object test", bWdRunObjectTest)
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP() 
	ENDPROC
	
	
	

	
#ENDIF

BOOL bReserved
PROC TEST_VALKYRIE()
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		EXIT
	ENDIF
	
	IF NOT bReserved
		bReserved = TRUE
		RESERVE_NETWORK_MISSION_VEHICLES(1)
		
	ELSE
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.vehHeli)
			REQUEST_MODEL(VELUM2)
			IF HAS_MODEL_LOADED(VELUM2)
				IF CREATE_NET_VEHICLE(serverBD.vehHeli, VELUM2,  <<708.6902, -984.4289, 23.1112>>, 143.3371, DEFAULT, DEFAULT, DEFAULT, TRUE)
					PRINTLN("Created Valkyrie, Display name = ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(VELUM2), " Hash = ", ENUM_TO_INT(VELUM2))
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC
FUNC BOOL REQUESTED_LESTER_CUTSCENE()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	OPEN_SCRIPT_PROFILE_MARKER_GROUP("REQUESTED_LESTER_CUTSCENE") // MUST BE AT START OF FUNCTION
	#ENDIF
	#ENDIF
	
//	TEST_VALKYRIE()
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF NOT g_bCelebrationScreenIsActive
			IF NOT IS_BIT_SET(iBoolsBitSet, biL_RequestedCut)
			
				IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), g_vLesterWarehouse ) < 625 // 25m
					IF NOT g_bCelebrationScreenIsActive
						IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
							interiorLester = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<706.8047, -965.5617, 29.4179>>, "v_sweat")
							IF interiorLester <> NULL
								PIN_INTERIOR_IN_MEMORY(interiorLester)
								#IF IS_DEBUG_BUILD NET_DW_PRINT("Pinned Interior!") #ENDIF
							ELSE
								#IF IS_DEBUG_BUILD NET_DW_PRINT("Failed to Pin Interior!") #ENDIF
							ENDIF
						//	SET_USE_DLC_DIALOGUE(TRUE)
							REQUEST_CUTSCENE(sLesterCut)
							
							SET_BIT(iBoolsBitSet, biL_RequestedCut)
							CLEAR_BIT(iBoolsBitSet, biL_ReqCutAssets)
							#IF IS_DEBUG_BUILD NET_DW_PRINT_STRINGS("[REQUESTED_LESTER_CUTSCENE] I have requested cutscene ", sLesterCut) #ENDIF
						ELSE
							#IF IS_DEBUG_BUILD NET_DW_PRINT("[REQUESTED_LESTER_CUTSCENE] Want to request cut but player control not on!") #ENDIF
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD NET_DW_PRINT("[REQUESTED_LESTER_CUTSCENE] Want to request cut but g_bCelebrationScreenIsActive!") #ENDIF
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_BIT_SET(iBoolsBitSet, biL_ReqCutAssets)
					IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()

						
						SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_1", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
							
						SET_BIT(iBoolsBitSet, biL_ReqCutAssets)
						#IF IS_DEBUG_BUILD NET_DW_PRINT("[REQUESTED_LESTER_CUTSCENE] Set biL_ReqCutAssets") #ENDIF
					ELSE
						#IF IS_DEBUG_BUILD NET_DW_PRINT("[REQUESTED_LESTER_CUTSCENE] CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY Failing!") #ENDIF
					ENDIF
				ENDIF

				IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), g_vLesterWarehouse ) > 1225 // 35m
				//	IF HAS_THIS_CUTSCENE_LOADED(sLesterCut)
					IF IS_BIT_SET(iBoolsBitSet, biL_RequestedCut)
						#IF IS_DEBUG_BUILD NET_DW_PRINT("REMOVE_CUTSCENE - 2") #ENDIF
						REMOVE_CUTSCENE()
					ENDIF
			//		SET_USE_DLC_DIALOGUE(FALSE)
					IF interiorLester <> NULL
						UNPIN_INTERIOR(interiorLester)
						interiorLester = NULL
						#IF IS_DEBUG_BUILD NET_DW_PRINT("Unpinned interior as player moved away from cutscene trigger") #ENDIF
					ENDIF
					CLEAR_BIT(iBoolsBitSet, biL_RequestedCut)
					CLEAR_BIT(iBoolsBitSet, biL_ReqCutAssets)
					#IF IS_DEBUG_BUILD NET_DW_PRINT("[REQUESTED_LESTER_CUTSCENE] I have removed Lester Heist Int cutscene as too far away") #ENDIF
				ENDIF
			ENDIF
		ELSE
			IF IS_BIT_SET(iBoolsBitSet, biL_RequestedCut)
			//	IF HAS_THIS_CUTSCENE_LOADED(sLesterCut)
					#IF IS_DEBUG_BUILD NET_DW_PRINT("REMOVE_CUTSCENE - 3") #ENDIF
					REMOVE_CUTSCENE()
			//	ENDIF
				IF interiorLester <> NULL
					UNPIN_INTERIOR(interiorLester)
					interiorLester = NULL
					#IF IS_DEBUG_BUILD NET_DW_PRINT("Unpinned interior as player moved away from cutscene trigger") #ENDIF
				ENDIF
				CLEAR_BIT(iBoolsBitSet, biL_RequestedCut)
				CLEAR_BIT(iBoolsBitSet, biL_ReqCutAssets)
				#IF IS_DEBUG_BUILD NET_DW_PRINT("[REQUESTED_LESTER_CUTSCENE] I have removed Lester Heist Int cutscene as g_bCelebrationScreenIsActive is set") #ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("REQUEST_CUTSCENE")
	#ENDIF
	#ENDIF	
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	CLOSE_SCRIPT_PROFILE_MARKER_GROUP() 
	#ENDIF
	#ENDIF
	
	RETURN (IS_BIT_SET(iBoolsBitSet, biL_RequestedCut))
ENDFUNC	

#IF IS_DEBUG_BUILD
	PROC DO_LESTER_CUTSCENE_J_SKIPS()
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
			IF IS_NET_PLAYER_OK(PLAYER_ID())
				IF NOT IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
					NET_DW_PRINT("Player ok for j-skip")
					SWITCH PlayerBD[PARTICIPANT_ID_TO_INT()].iLesterCutProg	
						CASE 0
							NET_DW_PRINT("Running Case 0 j-skip")
							J_SKIP_MP( <<717.7156, -979.5143, 23.1185>>, <<1.0, 1.0, 1.0>>)
						BREAK
					ENDSWITCH
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_DOWN)
			REMOVE_MP_HEIST_GEAR(PLAYER_PED_ID(), HEIST_GEAR_NIGHTVISION)
		ENDIF
	ENDPROC
#ENDIF


FUNC BOOL SHOULD_UI_BE_HIDDEN()
	RETURN FALSE
	
	BOOL bSHouldHide
	
		IF SHOULD_HIDE_JOB_BLIP(CI_TYPE_M3_HIDE_JOB_MISSION)
			bSHouldHide = TRUE
		ENDIF

	
//	IF bSHouldHide 
//		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_HidingEvent)
//			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_HidingEvent)
//			PRINTLN("     ---------->     URBAN WARFARE [SHOULD_UI_BE_HIDDEN] SET biP_HidingEvent  <---------- ")
//		ENDIF
//	ELSE
//		IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_HidingEvent)
//			CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_HidingEvent)
//			PRINTLN("     ---------->     URBAN WARFARE [SHOULD_UI_BE_HIDDEN] CLEAR biP_HidingEvent  <---------- ")
//		ENDIF
//	ENDIF
	
	RETURN bSHouldHide
ENDFUNC
FUNC INT GET_CLIENT_MISSION_STATE(INT iPlayer)
	RETURN playerBD[iPlayer].iGameState
ENDFUNC

FUNC INT GET_SERVER_MISSION_STATE()
	RETURN serverBD.iServerGameState
ENDFUNC



PROC CLEANUP_LESTER_PED()
	ENTITY_INDEX ent
	IF DOES_BLIP_EXIST(blipLester)
		REMOVE_BLIP(blipLester)
	ENDIF
	IF DOES_ENTITY_EXIST(pedLester)
		ent = pedLester
		DELETE_ENTITY(ent)
	ENDIF
	
	IF DOES_ENTITY_EXIST(oWheelChair)
		ent = oWheelChair
		DELETE_ENTITY(ent)
	ENDIF
	
	Clear_Any_Objective_Text_From_This_Script()
	
	SET_MODEL_AS_NO_LONGER_NEEDED(GET_NPC_PED_MODEL(CHAR_LESTER))
	SET_MODEL_AS_NO_LONGER_NEEDED(Prop_WheelChair_01_S)
	REMOVE_ANIM_DICT("missfinale_c2leadinoutfin_c_int") //MissLester1ALeadInOut
ENDPROC

PROC CLEANUP_ENTER_WAREHOUSE_CUT()
	#IF IS_DEBUG_BUILD 
		NET_DW_PRINT("[CLEANUP_ENTER_WAREHOUSE_CUT] Called. Callstack...")
		DEBUG_PRINTCALLSTACK()
	#ENDIF
	IF DOES_CAM_EXIST(camWhCut)
		SET_CAM_ACTIVE(camWhCut, FALSE)
		DESTROY_CAM(camWhCut)
	ENDIF
	
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
	
	CLEAR_BIT(iBoolsBitSet, biL_SetDoorResetFlags)
ENDPROC

PROC CLEANUP_CLONE_PED()
	ENTITY_INDEX ent
	IF DOES_ENTITY_EXIST(pedPlayer)
		ent = pedPlayer
		DELETE_ENTITY(ent)
		
		#IF IS_DEBUG_BUILD
			NET_DW_PRINT("[CLEANUP_CLONE_PED] Cleaned up Clone ped")
		#ENDIF 
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles the cleanup of the script
PROC CLEANUP_SCRIPT()
	
	
	#IF IS_DEBUG_BUILD
		NET_DW_PRINT("Running cleanup")
	#ENDIF
	Clear_Any_Objective_Text_From_This_Script()
	SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_LESTER_WAREHOUSE_CUTSCENE, FALSE)
	CLEAR_TUTORIAL_INVITES_ARE_BLOCKED() 
	
//	SET_INTERIOR_CAPPED( INTERIOR_V_LESTERS, TRUE )
	
	//SET_FM_UNLOCKS_BIT_SET()
	IF interiorLester <> NULL
		UNPIN_INTERIOR(interiorLester)
	ENDIF
	
	
	IF DOES_BLIP_EXIST(g_blipLesterWarehouse)
		SET_BLIP_ROUTE(g_blipLesterWarehouse, FALSE)
		REMOVE_BLIP(g_blipLesterWarehouse)
	ENDIF
	
	IF IS_BIT_SET(iBoolsBitSet, biL_UnlockedDoor)
		IF IS_DOOR_REGISTERED_WITH_SYSTEM(MP_DOOR_SWEATS_LEFT)
			DOOR_SYSTEM_SET_DOOR_STATE(MP_DOOR_SWEATS_LEFT, DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE)
			#IF IS_DEBUG_BUILD NET_DW_PRINT("Cleanup locking Lester's Left door") #ENDIF
		ELSE
			#IF IS_DEBUG_BUILD NET_DW_PRINT("Cleanup not locking Lester's Left door as IS_DOOR_REGISTERED_WITH_SYSTEM") #ENDIF
		ENDIF
		
		IF IS_DOOR_REGISTERED_WITH_SYSTEM(MP_DOOR_SWEATS_RIGHT)
			DOOR_SYSTEM_SET_DOOR_STATE(MP_DOOR_SWEATS_RIGHT, DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE)
			#IF IS_DEBUG_BUILD NET_DW_PRINT("Cleanup locking Lester's Right door") #ENDIF
		ELSE
			#IF IS_DEBUG_BUILD NET_DW_PRINT("Cleanup not locking Lester's Right door as IS_DOOR_REGISTERED_WITH_SYSTEM") #ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD NET_DW_PRINT("Cleanup not locking Lester's door as it wasn't unlocked") #ENDIF
	ENDIF
	
	IF IS_BIT_SET(iBoolsBitSet, biL_DisabledWeaponSelect)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_NEXT)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_PREV)
	ENDIF
	
	IF IS_KILL_YOURSELF_OPTION_DISABLED()
		ENABLE_KILL_YOURSELF_OPTION()
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		CLEAR_BIT(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_WantToSeeLester)
	ENDIF
	
	IF IS_BIT_SET(iBoolsBitSet, biL_SetInvicible)
		SET_PLAYER_INVINCIBLE(PLAYER_ID(), FALSE)
	ENDIF
	
	IF IS_BIT_SET(iBoolsBitSet, biL_StoreDisabled)
		SET_STORE_ENABLED(TRUE)
	ENDIF
	
	CLEANUP_CLONE_PED()
	
	CLEANUP_LESTER_PED()
	IF HAS_THIS_CUTSCENE_LOADED(sLesterCut)
		REMOVE_CUTSCENE()
	ENDIF

	
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

/// PURPOSE:
///    Do necessary pre game start ini.
PROC PROCESS_PRE_GAME(MP_MISSION_DATA missionScriptArgs)
	// This marks the script as a net script, and handles any instancing setup. 
	SETUP_MP_MISSION_FOR_NETWORK(GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(missionScriptArgs.mdID.idMission) , missionScriptArgs)
	PRINTLN("4618786 - PROCESS_PRE_GAME Count Of PlayerBD = ", COUNT_OF(PlayerBD))
	
	// This makes sure the net script is active, waits until it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	PRINTLN("4618786 - HANDLE_NET_SCRIPT_INITIALISATION Count Of PlayerBD = ", COUNT_OF(PlayerBD))
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
	PRINTLN("4618786 - NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES Count Of PlayerBD = ", COUNT_OF(PlayerBD))
	
	// This script will not be paused if another script calls PAUSE_GAME
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	
	// KGM: Wait for the first network broadcast before moving on - call this as the last instruction in pre-game
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		#IF IS_DEBUG_BUILD
		NET_DW_PRINT("Failed to receive initial network broadcast. Cleaning up.")
		#ENDIF
		CLEANUP_SCRIPT()
	ENDIF
	PRINTLN("4618786 - Wait_For_First_Network_Broadcast Count Of PlayerBD = ", COUNT_OF(PlayerBD))
	
	
//	#IF IS_DEBUG_BUILD
//		RESERVE_NETWORK_MISSION_OBJECTS(1)
//	#ENDIF
	
	
	#IF IS_DEBUG_BUILD
		NET_DW_PRINT("Launching...")
		
		IF SHOULD_UI_BE_HIDDEN()
			NET_DW_PRINT("Launching hidden!")
		ENDIF
	#ENDIF
	
//	SET_BUILDING_STATE(BUILDINGNAME_IPL_SWEATSHOP_NO_INTERIOR, BUILDINGSTATE_DESTROYED)
//	SET_BUILDING_STATE(BUILDINGNAME_IPL_SWEATSHOP_WITH_INTERIOR, BUILDINGSTATE_DESTROYED) 
//	SET_BUILDING_STATE(BUILDINGNAME_IPL_SWEATSHOP_LOD_DOOR, BUILDINGSTATE_DESTROYED) 
//	SET_BUILDING_STATE(BUILDINGNAME_IPL_SWEATSHOP_BURNT, BUILDINGSTATE_NORMAL) 
//	SET_BUILDING_STATE(BUILDINGNAME_IPL_SWEATSHOP_WINDOW_LIGHTS, BUILDINGSTATE_NORMAL)

ENDPROC

FUNC BOOL HAVE_MISSION_END_CONDITIONS_BEEN_MET()
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_AllPlayersFinished)
		#IF IS_DEBUG_BUILD NET_DW_PRINT("[HAVE_MISSION_END_CONDITIONS_BEEN_MET] True because biS_AllPlayersFinished") #ENDIF
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC


PROC MAINTAIN_SERVER_STAGGERED_LOOP()
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF iServerStaggeredLoopCount = 0
			CLEAR_BIT(iBoolsBitSet, biL_ServerLoopedThroughEveryone)
			SET_BIT(iBoolsBitSet, biL_StaggeredAllPlayersFinished)
			
			CLEAR_BIT(serverBD.iServerBitSet, biS_StaggeredDoorBlocked)
			IF serverbd.iPartWithLester <> -1
				IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(serverbd.iPartWithLester))
					#IF IS_DEBUG_BUILD
						NET_DW_PRINT_STRING_INT("[MAINTAIN_SERVER_STAGGERED_LOOP] This part was with Lester, but part no longer active: ", serverbd.iPartWithLester)
					#ENDIF
					
					serverbd.iPartWithLester = -1
					
					
				ENDIF
			ENDIF
			
			#IF IS_DEBUG_BUILD
				IF bWdDoStaggeredDebug
					NET_DW_PRINT("[MAINTAIN_SERVER_STAGGERED_LOOP] At Start of server staggered loop")
				ENDIF
			#ENDIF
		ENDIF
		
		
		
		
	
		PARTICIPANT_INDEX part = INT_TO_NATIVE(PARTICIPANT_INDEX, iServerStaggeredLoopCount )
		
		IF NETWORK_IS_PARTICIPANT_ACTIVE(part)
			PLAYER_INDEX player = NETWORK_GET_PLAYER_INDEX(part)
			
			
			
			IF NOT IS_BIT_SET(PlayerBD[iServerStaggeredLoopCount].iPlayerBitSet, biP_DoneCutscene)
				CLEAR_BIT(iBoolsBitSet, biL_StaggeredAllPlayersFinished)
			ENDIF
			IF IS_NET_PLAYER_OK(player, FALSE)	
//				IF IS_BIT_SET(PlayerBD[iServerStaggeredLoopCount].iPlayerBitSet, biP_WantToSeeLester)	
//					SET_BIT(iBoolsBitSet, biL_ServerStagSomeoneWithLester)
//					
//					#IF IS_DEBUG_BUILD
//						IF bWdDoStaggeredDebug
//							NET_DW_PRINT_STRING_WITH_PLAYER_NAME("This player wantes to see Lester... ", player)
//						ENDIF
//					#ENDIF
//					
//					IF serverbd.iPartWithLester = -1
//						IF serverbd.iPartWithLester <> iServerStaggeredLoopCount
//							serverbd.iPartWithLester = iServerStaggeredLoopCount
//							#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_WITH_PLAYER_NAME("[ServStag] Setting this player can see Lester... ", player) #ENDIF
//						ENDIF	
//					ENDIF
//				ELSE
//					IF serverbd.iPartWithLester = iServerStaggeredLoopCount
//						serverbd.iPartWithLester = -1
//						#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("Setting serverbd.iPartWithLester as this part no longer with Lester ", iServerStaggeredLoopCount) #ENDIF
//					ENDIF
//				ENDIF


				
				//-- Check for the warhouse door being blocked
				IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_StaggeredDoorBlocked)
					IF IS_BIT_SET(PlayerBD[iServerStaggeredLoopCount].iPlayerBitSet, biP_DoorBlocked)
						SET_BIT(serverBD.iServerBitSet, biS_StaggeredDoorBlocked)
						#IF IS_DEBUG_BUILD 
							NET_DW_PRINT_STRING_WITH_PLAYER_NAME("[MAINTAIN_SERVER_STAGGERED_LOOP] Server thinks this player thinks door is blocked ", player)
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		
		
		iServerStaggeredLoopCount++
		IF iServerStaggeredLoopCount = NUM_NETWORK_PLAYERS
			#IF IS_DEBUG_BUILD
				IF bWdDoStaggeredDebug
					NET_DW_PRINT("[MAINTAIN_SERVER_STAGGERED_LOOP] At End of server staggered loop")
				ENDIF
			#ENDIF
			
			//-- Warehouse door blocked?
			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_DoorBlocked)
				IF IS_BIT_SET(serverBD.iServerBitSet, biS_StaggeredDoorBlocked)
					#IF IS_DEBUG_BUILD 
						NET_DW_PRINT("[MAINTAIN_SERVER_STAGGERED_LOOP] Server setting door locked")
					#ENDIF
					SET_BIT(serverBD.iServerBitSet, biS_DoorBlocked)
				ENDIF
			ELSE
				IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_StaggeredDoorBlocked)
					#IF IS_DEBUG_BUILD 
						NET_DW_PRINT("[MAINTAIN_SERVER_STAGGERED_LOOP] Server no longer thinks door blocked ")
					#ENDIF
					CLEAR_BIT(serverBD.iServerBitSet, biS_DoorBlocked)
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(iBoolsBitSet, biL_StaggeredAllPlayersFinished)
				SET_BIT(serverBD.iServerBitSet, biS_AllPlayersFinished)
				#IF IS_DEBUG_BUILD NET_DW_PRINT("[MAINTAIN_SERVER_STAGGERED_LOOP] Setting biS_AllPlayersFinished because all players finished") #ENDIF
			ENDIF
			

			SET_BIT(iBoolsBitSet, biL_ServerLoopedThroughEveryone)
			iServerStaggeredLoopCount = 0
		ENDIF
	ENDIF
ENDPROC	


PROC MAINTAIN_SERVER_NON_STAGGERED_LOOP()
	INT i
	PARTICIPANT_INDEX part
	PLAYER_INDEX player
	
	CLEAR_BIT(iBoolsBitSet, biL_ServerStagSomeoneWithLester)
	
	REPEAT NUM_NETWORK_PLAYERS i
		part = INT_TO_NATIVE(PARTICIPANT_INDEX, i )
		
		IF NETWORK_IS_PARTICIPANT_ACTIVE(part)
			player = NETWORK_GET_PLAYER_INDEX(part)
			IF IS_NET_PLAYER_OK(player, FALSE)	
				IF IS_BIT_SET(PlayerBD[i].iPlayerBitSet, biP_WantToSeeLester)	
					SET_BIT(iBoolsBitSet, biL_ServerStagSomeoneWithLester)
					
					IF serverbd.iPartWithLester = -1
						IF serverbd.iPartWithLester <> i
					//		serverbd.iPartWithLester = i
					//		#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_WITH_PLAYER_NAME("[ServNonStag] Setting this player can see Lester... ", player) #ENDIF
						ENDIF	
					ENDIF
				ELSE
					IF serverbd.iPartWithLester = i
						serverbd.iPartWithLester = -1
						#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("Setting serverbd.iPartWithLester as this part no longer with Lester ", i) #ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF NOT IS_BIT_SET(iBoolsBitSet, biL_ServerStagSomeoneWithLester)
		IF serverbd.iPartWithLester <> -1
			serverbd.iPartWithLester = -1
			#IF IS_DEBUG_BUILD NET_DW_PRINT("[ServNonStag] serverbd.iPartWithLester = -1 as no one with LESTER") #ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Want to periodically clear the area in front of the warehouse if it's blocked
PROC MAINTAIN_BLOCKED_DOOR_HOST()
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF IS_BIT_SET(serverBD.iServerBitSet, biS_DoorBlocked)
			IF NOT HAS_NET_TIMER_STARTED(serverbd.timeClearDoor)
				START_NET_TIMER(serverbd.timeClearDoor)
				#IF IS_DEBUG_BUILD NET_DW_PRINT("[MAINTAIN_BLOCKED_DOOR_HOST] Started timer") #ENDIF
			ELSE
				IF HAS_NET_TIMER_EXPIRED(serverbd.timeClearDoor,60000)
					#IF IS_DEBUG_BUILD NET_DW_PRINT("[MAINTAIN_BLOCKED_DOOR_HOST] Clearing area") #ENDIF
					CLEAR_AREA_OF_VEHICLES(<<718.3742, -977.4440, 23.1256>>,  1.0, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
					RESET_NET_TIMER(serverbd.timeClearDoor)
				ENDIF
			ENDIF
		ELSE
			IF HAS_NET_TIMER_STARTED(serverbd.timeClearDoor)
				RESET_NET_TIMER(serverbd.timeClearDoor)
				#IF IS_DEBUG_BUILD NET_DW_PRINT("[MAINTAIN_BLOCKED_DOOR_HOST] Reset timer as no longer think door is blocked") #ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_LESTER_CUTSCENE_SERVER()
	
	
	
//	BOOL bFoundAPairing
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		MAINTAIN_BLOCKED_DOOR_HOST()
		
	ENDIF
ENDPROC


FUNC BOOL SHOULD_LESTER_CUTSCENE_COMPLETE()
	IF IS_BIT_SET(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DoneCutscene)
		#IF IS_DEBUG_BUILD NET_DW_PRINT("[SHOULD_LESTER_CUTSCENE_COMPLETE] True because biP_DoneCutscene") #ENDIF
		RETURN TRUE
	ENDIF
	#IF IS_DEBUG_BUILD 
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S) 
			IF iLesterCutProg < 1
				IF NOT IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
				AND NOT IS_FM_MISSION_LAUNCH_IN_PROGRESS()
					NET_DW_PRINT("[SHOULD_LESTER_CUTSCENE_COMPLETE] True because S-Passed")
				 	RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	#ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_OK_TO_PRINT_HELP()
	IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
	OR IS_PED_INJURED(PLAYER_PED_ID())
	OR NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
	OR  IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
	OR IS_SCRIPT_HUD_DISPLAYING(HUDPART_MISSIONBRIEF)
	OR IS_MP_MISSION_DETAILS_OVERLAY_ON_DISPLAY()
	OR IS_SCRIPT_HUD_DISPLAYING(HUDPART_MISSIONBOX)
	OR IS_HELP_MESSAGE_BEING_DISPLAYED()
	OR IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
	OR IS_FM_MISSION_LAUNCH_IN_PROGRESS()
	OR IS_CUSTOM_MENU_ON_SCREEN() 
	OR IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
	OR IS_MISSION_SUMMARY_SCREEN_DISPLAYED()
	OR IS_PAUSE_MENU_ACTIVE()
		RETURN(FALSE)
	ENDIF
	
	
	RETURN(TRUE)
ENDFUNC

FUNC BOOL CREATE_PLAYER_PED_COPY_FOR_LESTER_CUT()
	IF NOT DOES_ENTITY_EXIST(pedPlayer)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			MODEL_NAMES mPlayer = GET_ENTITY_MODEL(PLAYER_PED_ID())
			pedPlayer = CREATE_PED(PEDTYPE_CIVMALE, mPlayer,  <<713.2610, -966.2629, 29.3953>>, 62.1074, FALSE, FALSE)
			SET_ENTITY_INVINCIBLE(pedPlayer, TRUE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedPlayer, TRUE)
			CLONE_PED_TO_TARGET(PLAYER_PED_ID(), pedPlayer)
		//	REMOVE_PED_HELMET(pedPlayer, TRUE)
			SET_ENTITY_VISIBLE(pedPlayer, FALSE)
		ENDIF
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(pedPlayer)
		RETURN FALSE
	ENDIF
	
	IF NOT IS_PED_INJURED(pedPlayer)
		IF NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedPlayer)
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC



//BOOL bUseExteriorLocate = TRUE
BOOL bDoPlayerFade = TRUE
FUNC BOOL HAS_PLAYER_REACHED_LESTERS_WAREHOUSE()
	
//	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//		IF bUseExteriorLocate
//
//			//IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<716.435364,-975.928345,23.325346>>, <<719.681335,-975.931030,26.852324>>, 1.000000)
//			IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<716.305542,-976.049805,22.995344>>, <<719.921204,-976.125061,27.358212>>, 2.500000)
//				RETURN TRUE
//			ENDIF
//		ELSE
//			IF IS_ENTITY_IN_ANGLED_AREA(  PLAYER_PED_ID(), <<710.610657,-965.034790,29.270332>>, <<710.576050,-963.393982,31.882881>>, 1.000000)
//				RETURN TRUE
//			ENDIF
//		ENDIF
//	ENDIF
//	
//	RETURN FALSE
	
	
	IF IS_PED_INJURED(PLAYER_PED_ID())
		RETURN FALSE
	ENDIF
	
	IF bDoPlayerFade
		//-- Approaching Lester's Warehouse?
		IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()),  <<717.7453, -975.7217, 23.9148>>) > 500
			//-- No
			
			
			//-- Clean up the clone ped if player is no longer near Lester's
			CLEANUP_CLONE_PED()
			
			RETURN FALSE
		ENDIF
		
		//-- Approaching Lester's, create the clone ped in advance, as we have to wait for all its streaming requests to finish before the cutscene can start
		CREATE_PLAYER_PED_COPY_FOR_LESTER_CUT()
	ENDIF
	
	IF NOT IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<716.305542,-976.049805,22.995344>>, <<719.921204,-976.125061,27.358212>>, 2.500000)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC


FUNC BOOL CHECK_FOR_PLAYER_NEAR_LESTERS_DUE_TO_PACIFIC_STANDARD_HEIST_FINISHING()
	IF IS_POST_MISSION_SCENE_ACTIVE()
		bDoPostHeistCheck = TRUE
		#IF IS_DEBUG_BUILD NET_DW_PRINT("[CHECK_FOR_PLAYER_NEAR_LESTERS_DUE_TO_PACIFIC_STANDARD_HEIST_FINISHING] bDoPostHeistCheck = TRUE - IS_POST_MISSION_SCENE_ACTIVE") #ENDIF
	ELIF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistCelebration
		bDoPostHeistCheck = TRUE
		#IF IS_DEBUG_BUILD NET_DW_PRINT("[CHECK_FOR_PLAYER_NEAR_LESTERS_DUE_TO_PACIFIC_STANDARD_HEIST_FINISHING] bDoPostHeistCheck = TRUE - bDoingHeistCelebration") #ENDIF
	ELIF IS_PLAYER_SPECTATING(PLAYER_ID())
		bDoPostHeistCheck = TRUE
		#IF IS_DEBUG_BUILD NET_DW_PRINT("[CHECK_FOR_PLAYER_NEAR_LESTERS_DUE_TO_PACIFIC_STANDARD_HEIST_FINISHING] bDoPostHeistCheck = TRUE - IS_PLAYER_SPECTATING") #ENDIF
	ENDIF
	
	IF bDoPostHeistCheck
		IF NOT g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistCelebration
			IF NOT IS_PLAYER_SPECTATING(PLAYER_ID())
				IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<716.305542,-976.049805,22.995344>>, <<719.921204,-976.125061,27.358212>>, 2.500000)
					IF NOT bPlayerNearLestersAfterHeist 
						bPlayerNearLestersAfterHeist = TRUE
						#IF IS_DEBUG_BUILD NET_DW_PRINT("[CHECK_FOR_PLAYER_NEAR_LESTERS_DUE_TO_PACIFIC_STANDARD_HEIST_FINISHING] bPlayerNearLestersAfterHeist = TRUE") #ENDIF	
					ENDIF
				ELSE
					IF bPlayerNearLestersAfterHeist
						bPlayerNearLestersAfterHeist = FALSE
						#IF IS_DEBUG_BUILD NET_DW_PRINT("[CHECK_FOR_PLAYER_NEAR_LESTERS_DUE_TO_PACIFIC_STANDARD_HEIST_FINISHING] bPlayerNearLestersAfterHeist = FALSE") #ENDIF	
					ENDIF
					bDoPostHeistCheck = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN bPlayerNearLestersAfterHeist
ENDFUNC


FUNC BOOL HAS_LESTER_CUT_LOADED()
	IF REQUESTED_LESTER_CUTSCENE()
		IF HAS_CUTSCENE_LOADED()
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC



FUNC BOOL CREATE_LESTER_PED()
	BOOL bPlayingANim
	REQUEST_MODEL(GET_NPC_PED_MODEL(CHAR_LESTER))
	REQUEST_MODEL(Prop_WheelChair_01_S)
	REQUEST_ANIM_DICT("missfinale_c2leadinoutfin_c_int") 
	IF NOT DOES_ENTITY_EXIST(pedLester)
		IF HAS_MODEL_LOADED(GET_NPC_PED_MODEL(CHAR_LESTER)) 
		
			IF CREATE_NPC_PED_ON_FOOT(pedLester, CHAR_LESTER, << 1276.390, -1712.845, 54.372 >>, 338.0729)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedLester, TRUE)
				SET_ENTITY_INVINCIBLE(pedLester, TRUE)
				ADD_PED_FOR_DIALOGUE(speechFmLesterCut, 8, pedLester, "Lester")
				#IF IS_DEBUG_BUILD NET_DW_PRINT("[CREATE_LESTER_PED] Created Lester ") #ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(oWheelChair)
		IF HAS_MODEL_LOADED(Prop_WheelChair_01_S)
		
			oWheelChair =  CREATE_OBJECT(Prop_WheelChair_01_S, << 1276.390, -1712.845, 54.372 >>, FALSE, FALSE)
			#IF IS_DEBUG_BUILD NET_DW_PRINT("[CREATE_LESTER_PED] Created wheelchar ") #ENDIF
		ENDIF
	ENDIF
	
	
	IF NOT DOES_ENTITY_EXIST(pedLester)
		RETURN FALSE
	ENDIF
	
	IF NOT IS_PED_INJURED(pedLester)
		IF NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedLester)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(oWheelChair)
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_ANIM_DICT_LOADED("missfinale_c2leadinoutfin_c_int")
		RETURN FALSE
	ENDIF
	
	IF NOT IS_PED_INJURED(pedLester)
	
		iSceneID = CREATE_SYNCHRONIZED_SCENE(<< 1276.390, -1712.845, 54.372 >>,  << 0.000, 0.000, -155.520 >>)
		TASK_SYNCHRONIZED_SCENE(pedLester, iSceneID, "missfinale_c2leadinoutfin_c_int", "_LEADIN_LOOP1_LESTER", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT) //Lester_1_INT_LeadIn_loop_Lester
		PLAY_SYNCHRONIZED_ENTITY_ANIM(oWheelChair, 	iSceneID, "_LEADIN_LOOP1_WCHAIR", "missfinale_c2leadinoutfin_c_int", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, enum_to_int(SYNCED_SCENE_DONT_INTERRUPT)) //Lester_1_INT_LeadIn_loop_wChair
		SET_SYNCHRONIZED_SCENE_LOOPED(iSceneID, TRUE)
		#IF IS_DEBUG_BUILD NET_DW_PRINT("[CREATE_LESTER_PED] Playing anim ") #ENDIF
		bPlayingANim = TRUE
	ENDIF
	
	IF NOT bPlayingANim
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC



PROC MANAGE_PLAYER_TALKING()
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisableVoiceDrivenMouthMovement , TRUE)
		IF NETWORK_PLAYER_HAS_HEADSET(PLAYER_ID())
			IF NETWORK_IS_PLAYER_TALKING(PLAYER_ID()) 
			//	SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_EnableVoiceDrivenMouthMovement, TRUE)
			//	#IF IS_DEBUG_BUILD NET_DW_PRINT("Talking!") #ENDIF
			ENDIF 
		ENDIF 
	ENDIF
	
ENDPROC





//INT iArriveLesterCutProg
SCALEFORM_INDEX scaleLester
CAMERA_INDEX camArriveLester
//SCRIPT_TIMER timeArriveLesterCut

PROC CLEANUP_LESTER_ARRIVE_CUT()
	IF DOES_CAM_EXIST(camArriveLester)
		SET_CAM_ACTIVE(camArriveLester, FALSE)
		DESTROY_CAM(camArriveLester)
	ENDIF
	
	CLEAR_BIT(iBoolsBitSet, biL_DrawScaleform)
	
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
//	CLEANUP_MP_CUTSCENE()
	
	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(scaleLester)
	CLEAR_TIMECYCLE_MODIFIER()
	
//	NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
ENDPROC


INT iExitCutsceneProg
CAMERA_INDEX camExitCut
SCRIPT_TIMER timeExitCUt
FUNC BOOL MAINTAIN_EXIT_CUTSCENE()
//	SWITCH iExitCutsceneProg
//		CASE 0
//			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("MP_1")
//			OR (NOT IS_CUTSCENE_AUTHORIZED(sLesterCut) AND IS_BIT_SET(iBoolsBitSet, biL_OkToStartMocap))
//				//Do stuff.
//				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//					IF NOT IS_BIT_SET(iBoolsBitSet, biL_SetEndCutPos)
//						SET_ENTITY_COORDS(PLAYER_PED_ID(), <<717.4829, -972.1786, 25.1411>>)
//						SET_ENTITY_HEADING(PLAYER_PED_ID(), 182.5413)
//						SET_BIT(iBoolsBitSet, biL_SetEndCutPos)
//					ENDIF
//				ENDIF
//			ENDIF
//			
//			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
//			OR (NOT IS_CUTSCENE_AUTHORIZED(sLesterCut) AND IS_BIT_SET(iBoolsBitSet, biL_OkToStartMocap))
//				IF NOT DOES_CAM_EXIST(camExitCut)
//					CLEANUP_ENTER_WAREHOUSE_CUT()
//					
//					camExitCut = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE) 
//					SET_CAM_ACTIVE(camExitCut, TRUE)
//
//					SET_CAM_PARAMS(camExitCut,<<711.9433, -985.5894, 24.5366>>, <<38.7449, 0.0000, -40.5193>>, 43.0748)
//					SET_CAM_PARAMS(camExitCut, <<711.9433, -985.5894, 24.5366>>, <<9.2499, 0.0000, -40.5193>>, 43.0748, 7000)
//					RENDER_SCRIPT_CAMS(TRUE, FALSE)
//				ENDIF
//			ENDIF
//			
//			IF DOES_CAM_EXIST(camExitCut)
//			AND IS_BIT_SET(iBoolsBitSet, biL_SetEndCutPos)
//			
//				
//				
//				IF NOT IS_BIT_SET(iBoolsBitSet, biL_UnlockedDoor)
//					IF IS_DOOR_REGISTERED_WITH_SYSTEM(MP_DOOR_SWEATS_LEFT)
//						DOOR_SYSTEM_SET_DOOR_STATE(MP_DOOR_SWEATS_LEFT, DOORSTATE_UNLOCKED, FALSE)
//						SET_BIT(iBoolsBitSet, biL_UnlockedDoor)
//						#IF IS_DEBUG_BUILD NET_DW_PRINT("[MAINTAIN_EXIT_CUTSCENE] Unlocked door") #ENDIF
//					ENDIF
//				ENDIF
//				
//				
//				START_NET_TIMER(timeExitCUt)
//				
//				iExitCutsceneProg++
//					
//				#IF IS_DEBUG_BUILD
//					NET_DW_PRINT_STRING_INT("[MAINTAIN_EXIT_CUTSCENE] iExitCutsceneProg = ", iExitCutsceneProg)
//				#ENDIF
//			ENDIF
//		BREAK
//		
//		CASE 1
//			IF NOT IS_BIT_SET(iBoolsBitSet, biL_StartWalkOut)
//				IF HAS_NET_TIMER_EXPIRED(timeExitCUt, 1000)
//					IF NOT IS_PED_INJURED(PLAYER_PED_ID()) 
//					//	TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(),  <<717.4128, -981.2715, 23.1228>>, PEDMOVE_WALK)
//						TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(),  <<718.0134, -980.4674, 23.1217>>, PEDMOVE_WALK)
//						SET_BIT(iBoolsBitSet, biL_SetDoorResetFlags)
//					ENDIF
//					SET_BIT(iBoolsBitSet, biL_StartWalkOut)
//				ENDIF
//			ENDIF
//			IF HAS_NET_TIMER_EXPIRED(timeExitCUt, 6500)
//			#IF IS_DEBUG_BUILD OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RIGHT)	#ENDIF
//				SET_BIT(iBoolsBitSet, biL_FinishedExitCut)
//				CLEAR_BIT(iBoolsBitSet, biL_SetDoorResetFlags)
//				
//				iExitCutsceneProg = 100
//					
//				#IF IS_DEBUG_BUILD
//					NET_DW_PRINT_STRING_INT("[MAINTAIN_EXIT_CUTSCENE] iExitCutsceneProg = ", iExitCutsceneProg)
//				#ENDIF
//				
//			ENDIF
//		BREAK
//		
//		CASE 100
//			RETURN TRUE
//		BREAK
//	ENDSWITCH
//	
//	IF IS_BIT_SET(iBoolsBitSet, biL_SetDoorResetFlags)
//		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_OpenDoorArmIK, TRUE)
//			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_SearchForClosestDoor, TRUE)
//			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_OpenDoorArmIK, TRUE)
//			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_AllowOpenDoorIkBeforeFullMovement , TRUE)
//		ENDIF
//	ENDIF
	
	RETURN FALSE
ENDFUNC

BOOL bAddedSpawnPoints
PROC MAINTAIN_LESTER_SPAWN_PLAYER()
	IF IS_BIT_SET(iBoolsBitSet, biL_WarpedPlayerForEnd)
		EXIT
	ENDIF
	
	IF NOT bAddedSpawnPoints
		USE_CUSTOM_SPAWN_POINTS(TRUE)
		ADD_CUSTOM_SPAWN_POINT(<<718.3798, -981.3898, 23.1250>>,182.231)
		
		ADD_CUSTOM_SPAWN_POINT(<<721.6624, -980.0242, 23.1271>>, 182.7342, 0.5)
		ADD_CUSTOM_SPAWN_POINT(<<714.7198, -979.3767, 23.1146>>, 172.7043, 0.5)
		ADD_CUSTOM_SPAWN_POINT(<<712.7794, -982.4444, 23.1148>>, 169.8146, 0.5)
		ADD_CUSTOM_SPAWN_POINT(<<721.5182, -984.2596, 23.1581>>, 167.1816, 0.5)
		ADD_CUSTOM_SPAWN_POINT(<<725.1901, -981.3561, 23.1471>>, 243.0385, 0.1)
		ADD_CUSTOM_SPAWN_POINT(<<711.1748, -980.7484, 23.1103>>, 131.5259, 0.1)

		bAddedSpawnPoints = TRUE
		#IF IS_DEBUG_BUILD
			NET_DW_PRINT("[MAINTAIN_LESTER_SPAWN_PLAYER Added Cutsom Spawn points")
		#ENDIF
	ELSE
		IF WARP_TO_SPAWN_LOCATION(SPAWN_LOCATION_CUSTOM_SPAWN_POINTS,FALSE,FALSE,FALSE,TRUE)
			CLEAR_CUSTOM_SPAWN_POINTS()
			USE_CUSTOM_SPAWN_POINTS(FALSE)
			SET_BIT(iBoolsBitSet, biL_WarpedPlayerForEnd)
			#IF IS_DEBUG_BUILD
				NET_DW_PRINT("[MAINTAIN_LESTER_SPAWN_PLAYER Finished spawning")
			#ENDIF
		ENDIF
	ENDIF
ENDPROC


FUNC BOOL MAINTAIN_EXIT_CUTSCENE_V2()
	IF IS_BIT_SET(iBoolsBitSet, biL_StartWarpPlayerForEnd)
		MAINTAIN_LESTER_SPAWN_PLAYER()
	ENDIF
	
	
	SWITCH iExitCutsceneProg
		CASE 0
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("MP_1")
			OR (NOT IS_CUTSCENE_AUTHORIZED(sLesterCut) AND IS_BIT_SET(iBoolsBitSet, biL_OkToStartMocap))
				//Do stuff.
				IF NOT IS_PED_INJURED(pedPlayer)
					IF NOT IS_BIT_SET(iBoolsBitSet, biL_SetEndCutPos)
						SET_ENTITY_COORDS(pedPlayer, <<717.4829, -972.1786, 25.1411>>)
						SET_ENTITY_HEADING(pedPlayer, 182.5413)
						SET_BIT(iBoolsBitSet, biL_SetEndCutPos)
						CLEAR_AREA_OF_PROJECTILES(<<717.7623, -977.8769, 23.1224>>, 5.0, TRUE)
						#IF IS_DEBUG_BUILD NET_DW_PRINT("[MAINTAIN_EXIT_CUTSCENE] Clearing exit of projectiles") #ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
			OR (NOT IS_CUTSCENE_AUTHORIZED(sLesterCut) AND IS_BIT_SET(iBoolsBitSet, biL_OkToStartMocap))
				IF NOT DOES_CAM_EXIST(camExitCut)
					CLEANUP_ENTER_WAREHOUSE_CUT()
					
					camExitCut = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE) 
					SET_CAM_ACTIVE(camExitCut, TRUE)

					SET_CAM_PARAMS(camExitCut,<<711.9433, -985.5894, 24.5366>>, <<38.7449, 0.0000, -40.5193>>, 43.0748)
					SET_CAM_PARAMS(camExitCut, <<711.9433, -985.5894, 24.5366>>, <<9.2499, 0.0000, -40.5193>>, 43.0748, 7000)
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
				ENDIF
			ENDIF
			
			IF DOES_CAM_EXIST(camExitCut)
			AND IS_BIT_SET(iBoolsBitSet, biL_SetEndCutPos)
			
				
				
				IF NOT IS_BIT_SET(iBoolsBitSet, biL_UnlockedDoor)
					IF IS_DOOR_REGISTERED_WITH_SYSTEM(MP_DOOR_SWEATS_LEFT)
						DOOR_SYSTEM_SET_DOOR_STATE(MP_DOOR_SWEATS_LEFT, DOORSTATE_UNLOCKED, FALSE)
						SET_BIT(iBoolsBitSet, biL_UnlockedDoor)
						#IF IS_DEBUG_BUILD NET_DW_PRINT("[MAINTAIN_EXIT_CUTSCENE] Unlocked Left door") #ENDIF
					ENDIF
					
					IF IS_DOOR_REGISTERED_WITH_SYSTEM(MP_DOOR_SWEATS_RIGHT)
						SET_BIT(iBoolsBitSet, biL_UnlockedDoor)
						DOOR_SYSTEM_SET_DOOR_STATE(MP_DOOR_SWEATS_RIGHT, DOORSTATE_UNLOCKED, FALSE)
						#IF IS_DEBUG_BUILD NET_DW_PRINT("[MAINTAIN_EXIT_CUTSCENE] Unlocked Right door") #ENDIF
					ENDIF
				ENDIF
				
				//-- Move the player ped out of the path of the temp ped
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					SET_ENTITY_COORDS(PLAYER_PED_ID(), <<720.3045, -974.5632, 23.9142>>)
					#IF IS_DEBUG_BUILD NET_DW_PRINT("[MAINTAIN_EXIT_CUTSCENE] moved player out the way") #ENDIF
				ENDIF
				START_NET_TIMER(timeExitCUt)
				
				iExitCutsceneProg++
					
				#IF IS_DEBUG_BUILD
					NET_DW_PRINT_STRING_INT("[MAINTAIN_EXIT_CUTSCENE] iExitCutsceneProg = ", iExitCutsceneProg)
				#ENDIF
			ENDIF
		BREAK
		
		CASE 1
			IF NOT IS_BIT_SET(iBoolsBitSet, biL_StartWalkOut)
				IF HAS_NET_TIMER_EXPIRED(timeExitCUt, 1000)
					IF NOT IS_PED_INJURED(pedPlayer) 
					//	TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(),  <<717.4128, -981.2715, 23.1228>>, PEDMOVE_WALK)
						TASK_FOLLOW_NAV_MESH_TO_COORD(pedPlayer,  <<718.0134, -980.4674, 23.1217>>, PEDMOVE_WALK)
						SET_BIT(iBoolsBitSet, biL_SetDoorResetFlags)
					ENDIF
					SET_BIT(iBoolsBitSet, biL_StartWalkOut)
				ENDIF
			ENDIF
			
			IF HAS_NET_TIMER_EXPIRED(timeExitCUt, 3000)
				IF NOT IS_BIT_SET(iBoolsBitSet, biL_StartWarpPlayerForEnd)
					SET_BIT(iBoolsBitSet, biL_StartWarpPlayerForEnd)
					#IF IS_DEBUG_BUILD
						NET_DW_PRINT("[MAINTAIN_EXIT_CUTSCENE SetbiL_StartWarpPlayerForEnd")
					#ENDIF
				ENDIF
			ENDIF
			
			IF HAS_NET_TIMER_EXPIRED(timeExitCUt, 6500)
			#IF IS_DEBUG_BUILD OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RIGHT)	#ENDIF
				SET_BIT(iBoolsBitSet, biL_FinishedExitCut)
				CLEAR_BIT(iBoolsBitSet, biL_SetDoorResetFlags)
				

				iExitCutsceneProg = 100
					
				#IF IS_DEBUG_BUILD
					NET_DW_PRINT_STRING_INT("[MAINTAIN_EXIT_CUTSCENE] iExitCutsceneProg = ", iExitCutsceneProg)
				#ENDIF
				
			ENDIF
		BREAK
		
		CASE 100
			RETURN TRUE
		BREAK
	ENDSWITCH
	
//	IF iExitCutsceneProg > 0
//		CLEAR_AREA_OF_PROJECTILES()
//	ENDIF
	
	IF IS_BIT_SET(iBoolsBitSet, biL_SetDoorResetFlags)
		IF NOT IS_PED_INJURED(pedPlayer)
			SET_PED_CONFIG_FLAG(pedPlayer, PCF_OpenDoorArmIK, TRUE)
			SET_PED_RESET_FLAG(pedPlayer, PRF_SearchForClosestDoor, TRUE)
			SET_PED_RESET_FLAG(pedPlayer, PRF_OpenDoorArmIK, TRUE)
			SET_PED_RESET_FLAG(pedPlayer, PRF_AllowOpenDoorIkBeforeFullMovement , TRUE)
		ENDIF
	ENDIF
	
	
	RETURN FALSE
ENDFUNC



FUNC BOOL DO_ENTER_WAREHOUSE_CUT()
//	SWITCH iEnterWhCutProg
//		CASE 0
//			IF IS_NET_PLAYER_OK(PLAYER_ID())
//				IF IS_PLAYER_OK_TO_START_MP_CUTSCENE()
//			
//					MAKE_PLAYER_SAFE_FOR_MP_CUTSCENE(FALSE)
//					NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE) // Has collision
//					
//					SET_LOCAL_PLAYER_VISIBLE_IN_CUTSCENE(TRUE, FALSE)	
//					
//					START_MP_CUTSCENE()
//					
//					camWhCut = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE) 
//					SET_CAM_ACTIVE(camWhCut, TRUE)
//					
//					SET_CAM_PARAMS(camWhCut, <<711.9433, -985.5894, 24.5366>>, <<9.2499, 0.0000, -40.5193>>, 43.0748)
//					SET_CAM_PARAMS(camWhCut,<<711.9433, -985.5894, 24.5366>>, <<38.7449, 0.0000, -40.5193>>, 43.0748, 7000)
//					
//					RENDER_SCRIPT_CAMS(TRUE, FALSE)
//					
//					START_NET_TIMER(timeWhCut)
//					
//					SET_ENTITY_COORDS(PLAYER_PED_ID(), <<717.7723, -977.2475, 23.3085>>)
//					SET_ENTITY_HEADING(PLAYER_PED_ID(),1.5561)
//					
//					TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(),<<717.1046, -971.8569, 25.3436>>, PEDMOVE_WALK)
//					FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK)
//					FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
//					
//					SET_BIT(iBoolsBitSet, biL_SetDoorResetFlags)
//					
//					IF IS_DOOR_REGISTERED_WITH_SYSTEM(MP_DOOR_SWEATS_LEFT)
//						DOOR_SYSTEM_SET_DOOR_STATE(MP_DOOR_SWEATS_LEFT, DOORSTATE_UNLOCKED, FALSE)
//						
//						#IF IS_DEBUG_BUILD NET_DW_PRINT("[DO_ENTER_WAREHOUSE_CUT] Unlocked door") #ENDIF
//					ENDIF
//					
//					
//					iEnterWhCutProg++
//					#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_ENTER_WAREHOUSE_CUT] iEnterWhCutProg = ", iEnterWhCutProg)	#ENDIF
//				ENDIF
//			ENDIF
//		BREAK
//		
//		CASE 1
//			IF HAS_NET_TIMER_EXPIRED(timeWhCut, 7000)
//			#IF IS_DEBUG_BUILD	OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RIGHT) #ENDIF
//				SET_BIT(iBoolsBitSet, biL_OkToStartMocap)
//				iEnterWhCutProg = 99
//				#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_ENTER_WAREHOUSE_CUT] iEnterWhCutProg = ", iEnterWhCutProg)	#ENDIF
//			ENDIF
//		BREAK
//		
//		CASE 99
//		BREAK
//		
//		CASE 100
//			RETURN TRUE
//		BREAK
//	ENDSWITCH
//	
//	IF IS_BIT_SET(iBoolsBitSet, biL_SetDoorResetFlags)
//		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_OpenDoorArmIK, TRUE)
//			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_SearchForClosestDoor, TRUE)
//			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_OpenDoorArmIK, TRUE)
//			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_AllowOpenDoorIkBeforeFullMovement , TRUE)
//		ENDIF
//	ENDIF
//	
	RETURN FALSE
ENDFUNC


FUNC BOOL DO_ENTER_WAREHOUSE_CUT_V2()
	SWITCH iEnterWhCutProg
		CASE 0
			IF IS_NET_PLAYER_OK(PLAYER_ID())
				IF IS_PLAYER_OK_TO_START_MP_CUTSCENE()
					IF CREATE_PLAYER_PED_COPY_FOR_LESTER_CUT()
						IF NOT IS_PED_INJURED(pedPlayer)
							// (BOOL bLeavePedCopyBehind=TRUE, BOOL bPlayerInvisible = TRUE, BOOL bPlayerHasCollision = TRUE, BOOL bPlayerFrozen = FALSE
							MAKE_PLAYER_SAFE_FOR_MP_CUTSCENE(FALSE, FALSE, TRUE, FALSE ) 
							
							//  bool bHasControl, BOOL bVisible = TRUE, BOOL bClearTasks = TRUE, BOOL bHasCollision = FALSE
							NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_SET_INVISIBLE | NSPC_CLEAR_TASKS )
						//	SET_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_DEACTIVATE_NIGHTVISION)
							
							CLEAR_ALL_BIG_MESSAGES()
							
							SET_LOCAL_PLAYER_VISIBLE_IN_CUTSCENE(FALSE, TRUE)
							
							NETWORK_FADE_OUT_ENTITY(PLAYER_PED_ID(), TRUE, TRUE)
							
							START_MP_CUTSCENE()
							
							camWhCut = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE) 
							SET_CAM_ACTIVE(camWhCut, TRUE)
							
							SET_CAM_PARAMS(camWhCut, <<711.9433, -985.5894, 24.5366>>, <<9.2499, 0.0000, -40.5193>>, 43.0748)
							SET_CAM_PARAMS(camWhCut,<<711.9433, -985.5894, 24.5366>>, <<38.7449, 0.0000, -40.5193>>, 43.0748, 7000)
							
							RENDER_SCRIPT_CAMS(TRUE, FALSE)
							
							START_NET_TIMER(timeWhCut)
							
							IF IS_MP_HEIST_GEAR_EQUIPPED(PLAYER_PED_ID(), HEIST_GEAR_NIGHTVISION)
								#IF IS_DEBUG_BUILD NET_DW_PRINT("[DO_ENTER_WAREHOUSE_CUT_V2] Removing night vision from player ped") #ENDIF
								REMOVE_MP_HEIST_GEAR(PLAYER_PED_ID(), HEIST_GEAR_NIGHTVISION)
							ENDIF
							IF IS_MP_HEIST_GEAR_EQUIPPED(pedPlayer, HEIST_GEAR_NIGHTVISION)
								#IF IS_DEBUG_BUILD NET_DW_PRINT("[DO_ENTER_WAREHOUSE_CUT_V2] Removing night vision from cutscene ped") #ENDIF
								REMOVE_MP_HEIST_GEAR(pedPlayer, HEIST_GEAR_NIGHTVISION)
							ENDIF
							 // FEATURE_HEIST_PLANNING
							

	
							SET_ENTITY_COORDS(pedPlayer, <<717.7723, -977.2475, 23.3085>>)
							SET_ENTITY_HEADING(pedPlayer,1.5561)
							SET_ENTITY_VISIBLE(pedPlayer, TRUE)
							
							//-- Remove hats // MOVED TO DO_LESTER_MOCAP
						//	SET_PED_COMP_ITEM_CURRENT_MP(pedPlayer, COMP_TYPE_PROPS, PROPS_HEAD_NONE, FALSE)
						//	SET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_PROPS, PROPS_HEAD_NONE, FALSE)
							
							TASK_GO_STRAIGHT_TO_COORD(pedPlayer,<<717.1046, -971.8569, 25.3436>>, PEDMOVE_WALK)
							FORCE_PED_MOTION_STATE(pedPlayer, MS_ON_FOOT_WALK)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(pedPlayer)
							
							SET_BIT(iBoolsBitSet, biL_SetDoorResetFlags)
							
							REMOVE_PED_HELMET(PLAYER_PED_ID(), TRUE)
							SET_ENTITY_COORDS(PLAYER_PED_ID(), <<722.2921, -977.3157, 23.1306>>)
							IF IS_DOOR_REGISTERED_WITH_SYSTEM(MP_DOOR_SWEATS_LEFT)
								DOOR_SYSTEM_SET_DOOR_STATE(MP_DOOR_SWEATS_LEFT, DOORSTATE_UNLOCKED, FALSE)
								
								#IF IS_DEBUG_BUILD NET_DW_PRINT("[DO_ENTER_WAREHOUSE_CUT_V2] Unlocked door") #ENDIF
							ENDIF
							
							
							iEnterWhCutProg++
							#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_ENTER_WAREHOUSE_CUT_V2] iEnterWhCutProg = ", iEnterWhCutProg)	#ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
			IF HAS_NET_TIMER_EXPIRED(timeWhCut, 7000)
			#IF IS_DEBUG_BUILD	OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RIGHT) #ENDIF
				
				SET_BIT(iBoolsBitSet, biL_OkToStartMocap)
				iEnterWhCutProg = 99
				#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_ENTER_WAREHOUSE_CUT_V2] iEnterWhCutProg = ", iEnterWhCutProg)	#ENDIF
			ENDIF
		BREAK
		
		CASE 99
		BREAK
		
		CASE 100
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	IF IS_BIT_SET(iBoolsBitSet, biL_SetDoorResetFlags)
		IF NOT IS_PED_INJURED(pedPlayer)
			SET_PED_CONFIG_FLAG(pedPlayer, PCF_OpenDoorArmIK, TRUE)
			SET_PED_RESET_FLAG(pedPlayer, PRF_SearchForClosestDoor, TRUE)
			SET_PED_RESET_FLAG(pedPlayer, PRF_OpenDoorArmIK, TRUE)
			SET_PED_RESET_FLAG(pedPlayer, PRF_AllowOpenDoorIkBeforeFullMovement , TRUE)
		ENDIF
	ENDIF
	
	IF iEnterWhCutProg > 0
		IF IS_PAUSE_MENU_ACTIVE()
			SET_FRONTEND_ACTIVE(FALSE)
			SET_BIT(iBoolsBitSet2, biL2_PauseMenuWasActive)
			#IF IS_DEBUG_BUILD NET_DW_PRINT(" SET_FRONTEND_ACTIVE(FALSE)") #ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DO_LESTER_MOCAP()
	IF bDoScriptedCUtIntoWarehouse 
		IF bDoPlayerFade
			DO_ENTER_WAREHOUSE_CUT_V2()
		ELSE
			DO_ENTER_WAREHOUSE_CUT()
		ENDIF
	ENDIF
	
	SWITCH iLesterCutProg
		CASE 0
			


			IF IS_BIT_SET(iBoolsBitSet, biL_OkToStartMocap) // set by DO_ENTER_WAREHOUSE_CUT()
			OR NOT bDoScriptedCUtIntoWarehouse 
				#IF IS_DEBUG_BUILD NET_DW_PRINT("[DO_LESTER_MOCAP] Case 0 Trying to setup mocap...") #ENDIF
				
				//-- Request Mocap
				IF NOT IS_BIT_SET(iBoolsBitSet, biL_RequestedCut)
					REQUEST_CUTSCENE(sLesterCut)
					SET_BIT(iBoolsBitSet, biL_RequestedCut)
					#IF IS_DEBUG_BUILD NET_DW_PRINT("[DO_LESTER_MOCAP] Requested cutscene") #ENDIF
				ELSE
					IF NOT IS_BIT_SET(iBoolsBitSet, biL_ReqCutAssets)
						IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
							SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_1", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
							SET_BIT(iBoolsBitSet, biL_ReqCutAssets)
							#IF IS_DEBUG_BUILD NET_DW_PRINT("[DO_LESTER_MOCAP] Set biL_ReqCutAssets") #ENDIF
						ELSE
							#IF IS_DEBUG_BUILD NET_DW_PRINT("[DO_LESTER_MOCAP] CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY failing!") #ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				
				//-- Check for being near Lester
			//	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			//		IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<1275.094604,-1715.769287,53.708969>>, <<1274.382080,-1714.218506,56.021469>>, 1.375000)
						IF IS_BIT_SET(iBoolsBitSet, biL_ReqCutAssets)
							iLesterCutProg++
							#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_LESTER_MOCAP] iLesterCutProg = ", iLesterCutProg) #ENDIF
						ENDIF
			//		ENDIF
			//	ENDIF
			ENDIF
		BREAK	
		
		CASE 1
			IF HAS_CUTSCENE_LOADED() 
				IF NOT bDoPlayerFade
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01  //IS_PLAYER_FEMALE()
							REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "MP_1", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)

						ELSE
							REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "MP_1", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD NET_DW_PRINT("[DO_LESTER_MOCAP] Failed to register player ped for mocap as player ped is injured!") #ENDIF
					ENDIF
				ELSE
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						IF NOT IS_PED_INJURED(pedPlayer)
							//-- Remove hats
							SET_PED_COMP_ITEM_CURRENT_MP(pedPlayer, COMP_TYPE_PROPS, PROPS_HEAD_NONE, FALSE)
							SET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_PROPS, PROPS_HEAD_NONE, FALSE)
							REMOVE_PED_HELMET(pedPlayer, TRUE)
							IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01  //IS_PLAYER_FEMALE()
								REGISTER_ENTITY_FOR_CUTSCENE(pedPlayer, "MP_1", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)

							ELSE
								REGISTER_ENTITY_FOR_CUTSCENE(pedPlayer, "MP_1", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
							ENDIF
						ELSE
							#IF IS_DEBUG_BUILD NET_DW_PRINT("[DO_LESTER_MOCAP] Failed to register CLONED player ped for mocap as player ped is injured!") #ENDIF
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD NET_DW_PRINT("[DO_LESTER_MOCAP] Failed to register player ped for mocap as player ped is injured!") #ENDIF
					ENDIF
				ENDIF
				#IF IS_DEBUG_BUILD NET_DW_PRINT("[DO_LESTER_MOCAP] Turning off player control for mocap.") #ENDIF
				
				SET_TRANSITION_SESSIONS_CLEAN_UP_ON_CALL_NOW_IF_NEEDED() // 1854634
				
				START_CUTSCENE()
				SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
				MAKE_PLAYER_SAFE_FOR_MP_CUTSCENE(FALSE, FALSE, TRUE, FALSE )
				
				IF NOT bDoScriptedCUtIntoWarehouse 
					//-- DOn't need this twice, it's set in the scripted cut
					SET_LOCAL_PLAYER_VISIBLE_IN_CUTSCENE(TRUE, FALSE)
				ENDIF
				
				NETWORK_SET_VOICE_ACTIVE(FALSE)
				START_MP_CUTSCENE()

				HANG_UP_AND_PUT_AWAY_PHONE()
				#IF IS_DEBUG_BUILD NET_DW_PRINT("NETWORK_SET_VOICE_ACTIVE - FALSE") #ENDIF

				
				iLesterCutProg++
				#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_LESTER_MOCAP] iLesterCutProg = ", iLesterCutProg) #ENDIF
			ENDIF
		BREAK
		CASE 2
			IF IS_CUTSCENE_PLAYING()
				CLEANUP_ENTER_WAREHOUSE_CUT()
			//	CLEANUP_LESTER_PED()
				iLesterCutProg++
				#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_LESTER_MOCAP] iLesterCutProg = ", iLesterCutProg) #ENDIF
			ELIF NOT IS_CUTSCENE_AUTHORIZED(sLesterCut)
				iLesterCutProg = 4
				#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_LESTER_MOCAP] Cutscene not authorised! iLesterCutProg = ", iLesterCutProg) #ENDIF
			ENDIF
		BREAK
		
		CASE 3
			IF IS_CUTSCENE_PLAYING()

			ELSE
				iLesterCutProg++
				#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_LESTER_MOCAP] iLesterCutProg = ", iLesterCutProg) #ENDIF
			ENDIF
		BREAK
		
		CASE 4

			IF HAS_CUTSCENE_FINISHED()
			OR NOT IS_CUTSCENE_AUTHORIZED(sLesterCut)
				IF IS_BIT_SET(iBoolsBitSet, biL_FinishedExitCut)
					IF IS_BIT_SET(iBoolsBitSet, biL_WarpedPlayerForEnd)
						IF DOES_CAM_EXIST(camExitCut)
							RENDER_SCRIPT_CAMS(FALSE, FALSE)	
							DESTROY_CAM(camExitCut)
							#IF IS_DEBUG_BUILD NET_DW_PRINT("[DO_LESTER_MOCAP] Cleaned up exit cam") #ENDIF
							
							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
								CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
								IF bDoPlayerFade
									CLEANUP_CLONE_PED()
									NETWORK_FADE_IN_ENTITY(PLAYER_PED_ID(), TRUE)
									FORCE_PLAYER_LOCALLY_VISIBLE_FOR_FADE()
								ENDIF
						//		SET_ENTITY_COORDS(PLAYER_PED_ID(), <<718.3798, -981.3898, 23.1250>>)
						//		SET_ENTITY_HEADING(PLAYER_PED_ID(), 182.2312)
								FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK)
								FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
								SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
								SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
							ENDIF
						ENDIF
						
						IF IS_BIT_SET(iBoolsBitSet, biL_UnlockedDoor)
							IF IS_DOOR_REGISTERED_WITH_SYSTEM(MP_DOOR_SWEATS_LEFT)
								DOOR_SYSTEM_SET_DOOR_STATE(MP_DOOR_SWEATS_LEFT, DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE)
								#IF IS_DEBUG_BUILD NET_DW_PRINT("[DO_LESTER_MOCAP] locking Lester's left door") #ENDIF
							ELSE
								#IF IS_DEBUG_BUILD NET_DW_PRINT("[DO_LESTER_MOCAP] not locking Lester's left door as IS_DOOR_REGISTERED_WITH_SYSTEM") #ENDIF
							ENDIF
							
							IF IS_DOOR_REGISTERED_WITH_SYSTEM(MP_DOOR_SWEATS_RIGHT)
								DOOR_SYSTEM_SET_DOOR_STATE(MP_DOOR_SWEATS_RIGHT, DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE)
								#IF IS_DEBUG_BUILD NET_DW_PRINT("[DO_LESTER_MOCAP] locking Lester's right door") #ENDIF
							ELSE
								#IF IS_DEBUG_BUILD NET_DW_PRINT("[DO_LESTER_MOCAP] not locking Lester's right door as IS_DOOR_REGISTERED_WITH_SYSTEM") #ENDIF
							ENDIF
						ELSE
							#IF IS_DEBUG_BUILD NET_DW_PRINT("[DO_LESTER_MOCAP] not locking Lester's door as it wasn't unlocked") #ENDIF
						ENDIF
		
						SET_LOCAL_PLAYER_SHOULD_DISPLAY_POST_HEIST_INTRO_HELP(TRUE)
						
						CLEANUP_MP_CUTSCENE()
						SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)

						NETWORK_SET_VOICE_ACTIVE(TRUE)
						
						IF IS_BIT_SET(iBoolsBitSet, biL_SetInvicible)
							SET_PLAYER_INVINCIBLE(PLAYER_ID(), FALSE)
						ENDIF
						#IF IS_DEBUG_BUILD NET_DW_PRINT("NETWORK_SET_VOICE_ACTIVE - TRUE") #ENDIF
						IF interiorLester <> NULL
							UNPIN_INTERIOR(interiorLester)
						ENDIF
						CLEAR_BIT(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_WantToSeeLester)
						iLesterCutProg++
						#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_LESTER_MOCAP] iLesterCutProg = ", iLesterCutProg) #ENDIF

					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
		CASE 5
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	IF NOT bDoPlayerFade
		MAINTAIN_EXIT_CUTSCENE()
	ELSE
		MAINTAIN_EXIT_CUTSCENE_V2()
	ENDIF
	
//	IF NOT IS_BIT_SET(iBoolsBitSet, biL_SetEndCutPos)
//		 IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("MP_1")
//             //Do stuff.
//            IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<718.3798, -981.3898, 23.1250>>)
//				SET_ENTITY_HEADING(PLAYER_PED_ID(), 182.2312)
//				
//				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
//				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
//				
//				SET_BIT(iBoolsBitSet, biL_SetEndCutPos)
//				#IF IS_DEBUG_BUILD NET_DW_PRINT("Set player pos at cutscene end") #ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
	
	DISABLE_FRONTEND_THIS_FRAME()
	
	IF IS_BIT_SET(iBoolsBitSet, biL_DisabledWeaponSelect)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_NEXT) 
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_PREV)

	ENDIF
	RETURN FALSE
ENDFUNC




FUNC BOOL DO_ARRIVE_LESTER_CUT()	
	
//	SWITCH iArriveLesterCutProg
//		CASE 0
//			scaleLester = REQUEST_SCALEFORM_MOVIE("SECURITY_CAM")
//			
//			iArriveLesterCutProg++
//			#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_ARRIVE_LESTER_CUT] iArriveLesterCutProg = ", iArriveLesterCutProg) #ENDIF
//		BREAK
//		
//		CASE 1
//			IF HAS_SCALEFORM_MOVIE_LOADED(scaleLester)
//				DISABLE_KILL_YOURSELF_OPTION()
//				
//				MAKE_PLAYER_SAFE_FOR_MP_CUTSCENE(FALSE, FALSE, TRUE, FALSE )
//				SET_LOCAL_PLAYER_VISIBLE_IN_CUTSCENE(TRUE)
//				START_MP_CUTSCENE()
//				NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_CLEAR_TASKS )
//				
//				
//				
//				/*
//				MAKE_PLAYER_SAFE_FOR_MP_CUTSCENE(FALSE, FALSE, TRUE, FALSE )
//				
//				NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, FALSE, TRUE, TRUE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
//				SET_LOCAL_PLAYER_VISIBLE_IN_CUTSCENE(TRUE, FALSE)
//				START_MP_CUTSCENE(FALSE)
//				
//				*/
//				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//					CLEAR_AREA(<<1273.8845, -1718.0037, 53.7715>>, 3.0, TRUE)
//					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
//				//	SET_ENTITY_COORDS(PLAYER_PED_ID(),  <<1275.3485, -1722.6693, 53.6550>>)
//					SET_ENTITY_COORDS(PLAYER_PED_ID(),   <<1275.2563, -1722.3683, 53.6550>>)
//					SET_ENTITY_HEADING(PLAYER_PED_ID(), 12.6638)
//					
//				//	TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(),<<1274.8942, -1721.1792, 53.6807>>, PEDMOVE_WALK)
//				
//					FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK)
//					FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
//					
//					SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
//				ENDIF
//				camArriveLester = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE) 
//				SET_CAM_ACTIVE(camArriveLester, TRUE)
//			//	SET_CAM_PARAMS(camArriveLester,<<1276.4088, -1719.9183, 56.1054>>, <<-37.8168, -0.0000, 137.9132>>, 50.0000)
//				SET_CAM_PARAMS(camArriveLester,<<1276.4088, -1719.9183, 56.1054>>, <<-29.9320, -0.0000, 137.9132>>, 50.0000)
//				RENDER_SCRIPT_CAMS(TRUE, FALSE)
//				
//				BEGIN_SCALEFORM_MOVIE_METHOD(scaleLester, "SET_DETAILS")
//					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("FM_LEST_CAM")
//				END_SCALEFORM_MOVIE_METHOD()
//				
//				BEGIN_SCALEFORM_MOVIE_METHOD(scaleLester, "SET_LOCATION")
//					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("FM_LEST_CAM2")
//				END_SCALEFORM_MOVIE_METHOD()
//					
//				IF GET_TIMECYCLE_MODIFIER_INDEX() = -1
//					SET_TIMECYCLE_MODIFIER("scanline_cam")
//				ENDIF
//				
//				SET_BIT(iBoolsBitSet, biL_DrawScaleform)
//				START_NET_TIMER(timeArriveLesterCut)
//				
//				REQUEST_MODEL(GET_NPC_PED_MODEL(CHAR_LESTER))
//				REQUEST_MODEL(Prop_WheelChair_01_S)
//				REQUEST_ANIM_DICT("MissLester1ALeadInOut")
//				
//			//	SET_BIT(iBoolsBitSet, biL_SetDoorResetFlags)
//				SET_BIT(iBoolsBitSet, biL_CreateLester)
//				IF NOT IS_BIT_SET(iBoolsBitSet, biL_UnlockedDoor)
//					IF IS_DOOR_REGISTERED_WITH_SYSTEM(MP_DOOR_CS_ONLY_INT_LESTER)
//						DOOR_SYSTEM_SET_DOOR_STATE(MP_DOOR_CS_ONLY_INT_LESTER, DOORSTATE_UNLOCKED, FALSE)
//						SET_BIT(iBoolsBitSet, biL_UnlockedDoor)
//						#IF IS_DEBUG_BUILD NET_DW_PRINT("unlocked door") #ENDIF
//					ENDIF
//				ENDIF
//		//		REQUEST_CUTSCENE(sLesterCut)
//				
//				iArriveLesterCutProg = 2
//				#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_ARRIVE_LESTER_CUT] iArriveLesterCutProg = ", iArriveLesterCutProg) #ENDIF
//			ENDIF
//		BREAK
//		
//		CASE 2
//			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//				IF NOT IS_BIT_SET(iBoolsBitSet, biL_GivenLookAt)
//					IF HAS_NET_TIMER_EXPIRED(timeArriveLesterCut, 1000)
//						TASK_LOOK_AT_COORD(PLAYER_PED_ID(), <<1276.4088, -1719.9183, 56.1054>>, -1)
//						SET_BIT(iBoolsBitSet, biL_GivenLookAt)
//					ENDIF
//				ENDIF
//			ENDIF
//			
//			
//			IF HAS_NET_TIMER_EXPIRED(timeArriveLesterCut, 1500)
//			#IF IS_DEBUG_BUILD OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RIGHT) #ENDIF
//				IF IS_BIT_SET(iBoolsBitSet, biL_UnlockedDoor)
//					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//						IF NOT IS_PED_INJURED(pedLester)
//					//		IF CREATE_CONVERSATION (speechFmLesterCut, "LES1A", "LES1A_BRAC", CONV_PRIORITY_MEDIUM)
//							IF PLAY_SINGLE_LINE_FROM_CONVERSATION(speechFmLesterCut, "LS1AAUD", "LES1A_INTL2", "LES1A_INTL2_1", CONV_PRIORITY_MEDIUM)
////								SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_OpenDoorArmIK, TRUE)
////								SET_BIT(iBoolsBitSet, biL_SetDoorResetFlags)
////								TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
////								TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(),  <<1273.5762, -1718.4594, 53.7715>>, PEDMOVE_WALK)
////								
//								RESET_NET_TIMER(timeArriveLesterCut)
//								START_NET_TIMER(timeArriveLesterCut)
//								
//								iArriveLesterCutProg++
//								#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_ARRIVE_LESTER_CUT] iArriveLesterCutProg = ", iArriveLesterCutProg) #ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//					
//				ELSE 
//					#IF IS_DEBUG_BUILD NET_DW_PRINT("[DO_ARRIVE_LESTER_CUT] Waitning for door to unlock ") #ENDIF
//				ENDIF
//				
//			ENDIF
//		BREAK
//		
//		CASE 3
//			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//					SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_OpenDoorArmIK, TRUE)
//					SET_BIT(iBoolsBitSet, biL_SetDoorResetFlags)
//					TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
//					TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(),  <<1273.5762, -1718.4594, 53.7715>>, PEDMOVE_WALK)
//					
//					RESET_NET_TIMER(timeArriveLesterCut)
//					START_NET_TIMER(timeArriveLesterCut)
//					
//					iArriveLesterCutProg++
//					#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_ARRIVE_LESTER_CUT] iArriveLesterCutProg = ", iArriveLesterCutProg) #ENDIF
//						
//				ENDIF
//			ENDIF
//		BREAK
//		
//		CASE 4
//			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//				IF HAS_NET_TIMER_EXPIRED(timeArriveLesterCut, 5000)
//				OR IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<1275.137817,-1718.425537,53.729942>>, <<1272.450684,-1719.626709,55.458969>>, 1.562500)
//				#IF IS_DEBUG_BUILD OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RIGHT) #ENDIF
//				//	IF CREATE_LESTER_PED() 
//
////						SET_ENTITY_COORDS(PLAYER_PED_ID(), <<1274.2146, -1719.6804, 53.7715>>)
////						SET_ENTITY_HEADING(PLAYER_PED_ID(),  6.9293)
//						SET_ENTITY_COORDS(PLAYER_PED_ID(), <<1273.8845, -1718.0037, 53.7715>>)
//						SET_ENTITY_HEADING(PLAYER_PED_ID(),  21.8709)
//						SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
//					//	FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK)
//					//	FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
//						CLEANUP_LESTER_ARRIVE_CUT()
//						CLEANUP_MP_CUTSCENE()
//						CLEAR_BIT(iBoolsBitSet, biL_SetDoorResetFlags)
//						SET_GAMEPLAY_CAM_RELATIVE_HEADING()
//						SET_GAMEPLAY_CAM_RELATIVE_PITCH()
//						NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//						
//						SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
//						SET_BIT(iBoolsBitSet, biL_SetInvicible)
//						
//						SET_STORE_ENABLED(FALSE)
//						SET_BIT(iBoolsBitSet, biL_StoreDisabled)
//						iArriveLesterCutProg = 99
//						#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_ARRIVE_LESTER_CUT] iArriveLesterCutProg = ", iArriveLesterCutProg) #ENDIF
//						
//				//	ENDIF
//				ENDIF
//			ENDIF
//		BREAK
//		CASE 99
//			IF IS_DOOR_REGISTERED_WITH_SYSTEM(MP_DOOR_CS_ONLY_INT_LESTER)
//				DOOR_SYSTEM_SET_DOOR_STATE(MP_DOOR_CS_ONLY_INT_LESTER, DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE)
//				
//				SET_BIT(iBoolsBitSet, biL_DisabledWeaponSelect)
//				iArriveLesterCutProg = 100
//				#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[DO_ARRIVE_LESTER_CUT] iArriveLesterCutProg = ", iArriveLesterCutProg) #ENDIF
//			ENDIF
//		BREAK
//		
//		CASE 100
//			RETURN TRUE
//		BREAK
//	ENDSWITCH
//	
//	IF IS_BIT_SET(iBoolsBitSet, biL_DrawScaleform)
//		IF HAS_SCALEFORM_MOVIE_LOADED(scaleLester)
//			
//			BEGIN_SCALEFORM_MOVIE_METHOD(scaleLester, "SET_TIME")
//			IF GET_CLOCK_HOURS() >= 0 AND GET_CLOCK_HOURS() <= 12
//				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CLOCK_HOURS())
//			ELSE
//				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CLOCK_HOURS() - 12)
//			ENDIF
//			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CLOCK_MINUTES())
//			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(00)
//			IF GET_CLOCK_HOURS() >= 0 AND GET_CLOCK_HOURS() < 12
//				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("FM_LEST_AM")
//			ELSE
//				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("FM_LEST_PM")
//			ENDIF
//			END_SCALEFORM_MOVIE_METHOD()
//			DRAW_SCALEFORM_MOVIE_FULLSCREEN(scaleLester, 255, 255, 255, 255)
//		ELSE
//			#IF IS_DEBUG_BUILD NET_DW_PRINT("Still waiting for scaleform!") #ENDIF
//		ENDIF
//	ENDIF
//	
//	IF IS_BIT_SET(iBoolsBitSet, biL_SetDoorResetFlags)
//		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_OpenDoorArmIK, TRUE)
//			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_SearchForClosestDoor, TRUE)
//			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_OpenDoorArmIK, TRUE)
//			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_AllowOpenDoorIkBeforeFullMovement , TRUE)
//		ENDIF
//	ENDIF
//	
//	IF IS_BIT_SET(iBoolsBitSet, biL_CreateLester)
//		IF CREATE_LESTER_PED()	
//			CLEAR_BIT(iBoolsBitSet, biL_CreateLester)
//		ENDIF
//	ENDIF
//	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Want to periodically clear the area in front of the warehouse if it's blocked
PROC MAINTAIN_DOOR_BLOCKED_CLIENT()
	IF NOT IS_BIT_SET(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DoorBlocked)
		IF NOT HAS_NET_TIMER_STARTED(timeOccupied)
			START_NET_TIMER(timeOccupied)
		ELSE
			IF HAS_NET_TIMER_EXPIRED(timeOccupied, 5000)
				IF IS_POSITION_OCCUPIED( <<718.3742, -977.4440, 23.1256>>, 3.0, FALSE, TRUE, FALSE, FALSE, FALSE)
					SET_BIT(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DoorBlocked)
					#IF IS_DEBUG_BUILD NET_DW_PRINT("I think the warehouse door is blocked!") #ENDIF
				ENDIF
				
				RESET_NET_TIMER(timeOccupied)
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_POSITION_OCCUPIED( <<718.3742, -977.4440, 23.1256>>, 3.0, FALSE, TRUE, FALSE, FALSE, FALSE)
			CLEAR_BIT(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DoorBlocked)
			#IF IS_DEBUG_BUILD NET_DW_PRINT("I no longer think the warehouse door is blocked!") #ENDIF
		ENDIF
	ENDIF
ENDPROC


INT iWarehouseHelpBitset

CONST_INT	WAREHOUSE_HELP_1		0
CONST_INT	WAREHOUSE_HELP_2		1
CONST_INT	WAREHOUSE_HELP_3		2

FUNC BOOL NEED_TO_DO_LESTER_WAREHOUSE_HELP(BOOL &bShouldFlash)
	INT iStatInt
	IF NOT HAS_NET_TIMER_STARTED(g_timeLesterWarehouse)
	OR HAS_NET_TIMER_EXPIRED(g_timeLesterWarehouse, 60 * 60000, TRUE)
		#IF IS_DEBUG_BUILD
			IF NOT HAS_NET_TIMER_STARTED(g_timeLesterWarehouse)
				NET_DW_PRINT("[NEED_TO_DO_LESTER_WAREHOUSE_HELP] Timer not started")
			ENDIF
			
			IF HAS_NET_TIMER_EXPIRED(g_timeLesterWarehouse,60 * 60000, TRUE)
				NET_DW_PRINT("[NEED_TO_DO_LESTER_WAREHOUSE_HELP] Timer expired!")
			ENDIF
		#ENDIF
		
		REINIT_NET_TIMER(g_timeLesterWarehouse, TRUE) // Non-network timer
		
		IF NOT IS_BIT_SET(iWarehouseHelpBitset, WAREHOUSE_HELP_1)
			iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT6)
			IF NOT IS_BIT_SET(iStatInt, biNmh6_LestWh1)
				bShouldFlash = TRUE
				RETURN TRUE
			ELSE
				SET_BIT(iWarehouseHelpBitset, WAREHOUSE_HELP_1)
				#IF IS_DEBUG_BUILD NET_DW_PRINT("[dsw] [NEED_TO_DO_LESTER_WAREHOUSE_HELP] Already done once") NET_NL() #ENDIF
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(iWarehouseHelpBitset, WAREHOUSE_HELP_2)
			iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT6)
			IF NOT IS_BIT_SET(iStatInt, biNmh6_LestWh2)
				RETURN TRUE
			ELSE
				SET_BIT(iWarehouseHelpBitset, WAREHOUSE_HELP_2)
				#IF IS_DEBUG_BUILD NET_DW_PRINT("[dsw] [NEED_TO_DO_LESTER_WAREHOUSE_HELP]  Already done two times") NET_NL() #ENDIF
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(iWarehouseHelpBitset, WAREHOUSE_HELP_3)
			iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT6)
			IF NOT IS_BIT_SET(iStatInt, biNmh6_LestWh3)
				RETURN TRUE
			ELSE
				SET_BIT(iWarehouseHelpBitset, WAREHOUSE_HELP_3)
				#IF IS_DEBUG_BUILD NET_DW_PRINT("[dsw] [NEED_TO_DO_LESTER_WAREHOUSE_HELP]  Already done three times") NET_NL() #ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_DONE_LESTER_WAREHOUSE_HELP()
	#IF IS_DEBUG_BUILD NET_DW_PRINT("[dsw] [SET_DONE_LESTER_WAREHOUSE_HELP] Called") NET_NL() #ENDIF
	
	INT iStatInt
	
	IF NOT IS_BIT_SET(iWarehouseHelpBitset, WAREHOUSE_HELP_1)
		iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT6)
		IF NOT IS_BIT_SET(iStatInt, biNmh6_LestWh1)
			SET_BIT(iStatInt, biNmh6_LestWh1)
			SET_BIT(iWarehouseHelpBitset, WAREHOUSE_HELP_1)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT6, iStatInt)
			#IF IS_DEBUG_BUILD NET_DW_PRINT("[dsw] [SET_DONE_LESTER_WAREHOUSE_HELP]  Set done once") NET_NL() #ENDIF
			EXIT
		ELSE
			#IF IS_DEBUG_BUILD NET_DW_PRINT("[dsw] [SET_DONE_LESTER_WAREHOUSE_HELP]  Already done once") NET_NL() #ENDIF
			SET_BIT(iWarehouseHelpBitset, WAREHOUSE_HELP_1)
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(iWarehouseHelpBitset, WAREHOUSE_HELP_2)
		iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT6)
		IF NOT IS_BIT_SET(iStatInt, biNmh6_LestWh2)
			SET_BIT(iStatInt, biNmh6_LestWh2)
			SET_BIT(iWarehouseHelpBitset, WAREHOUSE_HELP_2)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT6, iStatInt)
			#IF IS_DEBUG_BUILD NET_DW_PRINT("[dsw] [SET_DONE_LESTER_WAREHOUSE_HELP]  Set done two times") NET_NL() #ENDIF
			EXIT
		ELSE
			#IF IS_DEBUG_BUILD NET_DW_PRINT("[dsw] [SET_DONE_LESTER_WAREHOUSE_HELP] Already done two times") NET_NL() #ENDIF
			SET_BIT(iWarehouseHelpBitset, WAREHOUSE_HELP_2)
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(iWarehouseHelpBitset, WAREHOUSE_HELP_3)
		iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT6)
		IF NOT IS_BIT_SET(iStatInt, biNmh6_LestWh3)
			SET_BIT(iStatInt, biNmh6_LestWh3)
			SET_BIT(iWarehouseHelpBitset, WAREHOUSE_HELP_3)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT6, iStatInt)
			#IF IS_DEBUG_BUILD NET_DW_PRINT("[dsw] [SET_DONE_LESTER_WAREHOUSE_HELP] Set done three times") NET_NL() #ENDIF
			EXIT
		ELSE
			#IF IS_DEBUG_BUILD NET_DW_PRINT("[dsw] [SET_DONE_LESTER_WAREHOUSE_HELP]  Already done three times") NET_NL() #ENDIF
			SET_BIT(iWarehouseHelpBitset, WAREHOUSE_HELP_3)
		ENDIF
	ENDIF
	
	
ENDPROC

PROC MAINTAIN_LESTER_WAREHOUSE_HELP()
//
//	IF IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
//		EXIT
//	ENDIF
//
//	BOOL bNeedToFlashBlip
//	IF DOES_BLIP_EXIST(g_blipLesterWarehouse)
//		IF NEED_TO_DO_LESTER_WAREHOUSE_HELP(bNeedToFlashBlip)
//			IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE)
//			
//				#IF IS_DEBUG_BUILD
//					NET_DW_PRINT("[MAINTAIN_LESTER_WAREHOUSE_HELP] Printing help")
//				#ENDIF
//				
//				IF bNeedToFlashBlip
//					// Flash first time only
//					SET_BLIP_FLASHES(g_blipLesterWarehouse, TRUE)
//					SET_BLIP_FLASH_TIMER(g_blipLesterWarehouse, 7000)
//					
//				ENDIF
//				PRINT_HELP("FM_LCUT_LBWH") //Visit Lester at ~BLIP_LESTER_FAMILY~ to discuss a potential job.
//				
//				SET_DONE_LESTER_WAREHOUSE_HELP()
//			ENDIF
//		ENDIF
//	ENDIF
ENDPROC

PROC PROCESS_LESTER_CUTSCENE_CLIENT()	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	OPEN_SCRIPT_PROFILE_MARKER_GROUP("PROCESS_LESTER_CUTSCENE_CLIENT") // MUST BE AT START OF FUNCTION
	#ENDIF
	#ENDIF
	
	#IF IS_DEBUG_BUILD
		DO_LESTER_CUTSCENE_J_SKIPS()
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("DO_LESTER_CUTSCENE_J_SKIPS")
		#ENDIF
		#ENDIF
		
//		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_LEFT)
//			MODEL_NAMES mPlayer
//			mplayer = GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
//			PRINTLN("[dsw] [model] = ", ENUM_TO_INT(mplayer))
//		ENDIF
	#ENDIF
	SWITCH PlayerBD[PARTICIPANT_ID_TO_INT()].iLesterCutProg
		CASE 0
			CHECK_FOR_PLAYER_NEAR_LESTERS_DUE_TO_PACIFIC_STANDARD_HEIST_FINISHING()
			
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("CHECK_FOR_PLAYER_NEAR_LESTERS")
			#ENDIF
			#ENDIF
			
//			IF NOT IS_BIT_SET(iBoolsBitSet, biL_ToldToGoToLester)
//				IF NOT IS_BIT_SET(iBoolsBitSet, biL_TextCleared)
//					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//						IF NOT IS_CELLPHONE_CONVERSATION_PLAYING() //IS_MOBILE_PHONE_CALL_ONGOING()
//							IF NOT Is_MP_Comms_Still_Playing()
//								IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
//									IF NOT IS_PHONE_ONSCREEN()
//										IF NOT IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
//											IF NOT IS_FM_MISSION_LAUNCH_IN_PROGRESS()
//												IF GET_LOCAL_PLAYER_CORONA_POS_STATE() = 0
//													
//											//		Print_Objective_Text("FM_LCUT_GOL") // Go to Lester's ~y~house.
////													g_blipLesterWarehouse = ADD_BLIP_FOR_COORD(g_vLesterWarehouse)
////											
////													SET_BLIP_SPRITE(g_blipLesterWarehouse, RADAR_TRACE_LESTER_FAMILY)
////													SET_BIT(iBoolsBitSet, biL_ToldToGoToLester)
////													#IF IS_DEBUG_BUILD NET_DW_PRINT("Added Lester house blip") #ENDIF
//													//		SET_BLIP_ROUTE(g_blipLesterWarehouse, TRUE)
//													
//												ENDIF
//											ENDIF
//										ENDIF
//									ENDIF
//								ENDIF
//							ELSE
//								#IF IS_DEBUG_BUILD NET_DW_PRINT("Mp comms on going") #ENDIF
//							ENDIF
//						ELSE
//							#IF IS_DEBUG_BUILD NET_DW_PRINT("Phonecall on going") #ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
			
			IF NOT HAS_PLAYER_PURCHASED_HIGH_END_APARTMENT()
				IF DOES_BLIP_EXIST(g_blipLesterWarehouse)
					Clear_Any_Objective_Text_From_This_Script()
					SET_BLIP_ROUTE(g_blipLesterWarehouse, FALSE)
					REMOVE_BLIP(g_blipLesterWarehouse)
				ENDIF
				EXIT
			ENDIF
			
			IF g_bFlashLesterWarehouseBlip
				IF DOES_BLIP_EXIST(g_blipLesterWarehouse)
					SET_BLIP_FLASHES(g_blipLesterWarehouse, TRUE)
					SET_BLIP_FLASH_INTERVAL(g_blipLesterWarehouse, BLIP_FLASHING_TIME)
					SET_BLIP_FLASH_TIMER(g_blipLesterWarehouse, FLASH_CONTRABAND_BLIP_DEFAULT_TIME)
				ENDIF
				g_bFlashLesterWarehouseBlip = FALSE
			ENDIF
			
			IF NOT IS_BIT_SET(iBoolsBitSet, biL_TextCleared)
				
				IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
				OR IS_FM_MISSION_LAUNCH_IN_PROGRESS()
				OR GET_LOCAL_PLAYER_CORONA_POS_STATE() > 0
				OR IS_TRANSITION_ACTIVE()	
				OR SHOULD_UI_BE_HIDDEN()
				OR SHOULD_HIDE_ALL_JOB_BLIPS_FOR_YACHT()
				OR IS_LOCAL_PLAYER_DELIVERING_BOUNTY()
				OR ARE_FLOW_MESSAGES_SUPPRESSED_IN_CURRENT_INTERIOR()
					SET_BIT(iBoolsBitSet, biL_TextCleared)
					Clear_Any_Objective_Text_From_This_Script()
					IF DOES_BLIP_EXIST(g_blipLesterWarehouse)
						SET_BLIP_ROUTE(g_blipLesterWarehouse, FALSE)
						REMOVE_BLIP(g_blipLesterWarehouse)
						
					ENDIF
				//	IF REQUESTED_LESTER_CUTSCENE()
				//		REMOVE_CUTSCENE()
				//	ENDIF
					#IF IS_DEBUG_BUILD 
						IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
							NET_DW_PRINT("Setting biL_TextCleared as IS_PLAYER_ON_ANY_FM_MISSION") 
						ELIF IS_FM_MISSION_LAUNCH_IN_PROGRESS()
							NET_DW_PRINT("Setting biL_TextCleared as IS_PLAYER_ON_ANY_FM_MISSION") 
						ELIF GET_LOCAL_PLAYER_CORONA_POS_STATE() > 0	
							NET_DW_PRINT("Setting biL_TextCleared as GET_LOCAL_PLAYER_CORONA_POS_STATE") 
						ELIF IS_TRANSITION_ACTIVE()	
							NET_DW_PRINT("Setting biL_TextCleared as IS_TRANSITION_ACTIVE") 
						ELIF SHOULD_UI_BE_HIDDEN()
							NET_DW_PRINT("Setting biL_TextCleared as SHOULD_UI_BE_HIDDEN") 
						ELIF SHOULD_HIDE_ALL_JOB_BLIPS_FOR_YACHT()
							NET_DW_PRINT("Setting biL_TextCleared as SHOULD_HIDE_ALL_JOB_BLIPS_FOR_YACHT") 
						ELIF  IS_LOCAL_PLAYER_DELIVERING_BOUNTY()
							NET_DW_PRINT("Setting biL_TextCleared as IS_LOCAL_PLAYER_DELIVERING_BOUNTY") 
						ENDIF
					#ENDIF
					
				ELSE	
//					IF IS_MP_PASSIVE_MODE_ENABLED()
//						IF IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
//							IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
//								PRINT_HELP("FM_IHELP_NPA")
//							ENDIF
//						ENDIF
//					ENDIF
				//	IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
						IF DOES_BLIP_EXIST(g_blipLesterWarehouse)
							IF NOT IS_BIT_SET(iBoolsBitSet, biL_LesterBlipInactive)					
								IF serverbd.iPartWithLester <> -1
									IF serverbd.iPartWithLester <> PARTICIPANT_ID_TO_INT()
										SET_BLIP_COLOUR(g_blipLesterWarehouse ,BLIP_COLOUR_INACTIVE_MISSION)
										SET_BLIP_SCALE(g_blipLesterWarehouse, BLIP_SIZE_NETWORK_PED)
										SET_BIT(iBoolsBitSet, biL_LesterBlipInactive)
								//		SET_BLIP_PRIORITY(g_blipLesterWarehouse, BLIPPRIORITY_HIGH)
										#IF IS_DEBUG_BUILD NET_DW_PRINT("Setting biL_LesterBlipInactive as LESTER busy") #ENDIF
									ENDIF
								ENDIF
							ELSE
								IF serverbd.iPartWithLester = -1
								OR serverbd.iPartWithLester = PARTICIPANT_ID_TO_INT()
									SET_BLIP_COLOUR(g_blipLesterWarehouse ,BLIP_COLOUR_DEFAULT)
									SET_BLIP_SCALE(g_blipLesterWarehouse, BLIP_SIZE_NETWORK_COORD)
									CLEAR_BIT(iBoolsBitSet, biL_LesterBlipInactive)
									#IF IS_DEBUG_BUILD NET_DW_PRINT("Clearing biL_LesterBlipInactive as LESTER no longer busy") #ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						
						#IF IS_DEBUG_BUILD
						#IF SCRIPT_PROFILER_ACTIVE
							ADD_SCRIPT_PROFILE_MARKER("g_blipLesterWarehouse")
						#ENDIF
						#ENDIF
						
						IF IS_BIT_SET(iBoolsBitSet, biL_ToldToLoseCops)
							CLEAR_BIT(iBoolsBitSet, biL_ToldToLoseCops)
						ENDIF
						IF HAS_PLAYER_REACHED_LESTERS_WAREHOUSE()
							IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
								IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
									IF NOT IS_PLAYER_USING_BALLISTIC_EQUIPMENT(PLAYER_ID())
									AND NOT IS_PLAYER_AN_ANIMAL(PLAYER_ID())
										IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) 
											IF NOT IS_CUSTOM_MENU_ON_SCREEN()
											AND NOT IS_PHONE_ONSCREEN()
											AND IS_SKYSWOOP_AT_GROUND()
											AND NOT IS_TRANSITION_ACTIVE() 
											AND NOT  IS_MP_PAUSE_MENU_SKYCAM_UP()
											AND NOT collectables_missiondata_main.bturnbacktohuman 
												IF NOT CHECK_FOR_PLAYER_NEAR_LESTERS_DUE_TO_PACIFIC_STANDARD_HEIST_FINISHING()
													IF NOT IS_BIT_SET(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_WantToSeeLester)
														SET_BIT(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_WantToSeeLester)
														#IF IS_DEBUG_BUILD NET_DW_PRINT("Setting I want to see Lester") #ENDIF
													ELSE	
															
													//	IF serverbd.iPartWithLester = PARTICIPANT_ID_TO_INT()
															Clear_Any_Objective_Text_From_This_Script()
															IF DOES_BLIP_EXIST(g_blipLesterWarehouse)
																SET_BLIP_ROUTE(g_blipLesterWarehouse, FALSE)
																REMOVE_BLIP(g_blipLesterWarehouse)
																#IF IS_DEBUG_BUILD NET_DW_PRINT("removed Lester house blip as I'm in house") #ENDIF
															ENDIF
															CLEAR_HELP()
															SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_LESTER_WAREHOUSE_CUTSCENE, TRUE)
															SET_TUTORIAL_INVITES_ARE_BLOCKED()
															//MAKE_PLAYER_SAFE_FOR_MP_CUTSCENE(FALSE, FALSE)
															SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
															SET_BIT(iBoolsBitSet, biL_SetInvicible)

															SET_BIT(iBoolsBitSet, biL_DoLesterMocap)
															PlayerBD[PARTICIPANT_ID_TO_INT()].iLesterCutProg = 1
															#IF IS_DEBUG_BUILD NET_DW_PRINT("iLesterCutProg = 1 as I've reached Lester's house. Blocking invites") #ENDIF
		//												ELSE
		//													IF serverbd.iPartWithLester <> -1
		//														IF NOT IS_BIT_SET(iBoolsBitSet, biL_LesterBusy)
		//															IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
		//																IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
		//																	IF IS_SKYSWOOP_AT_GROUND()
		//																		PRINT_HELP("FM_LCUT_LRB")
		//																		#IF IS_DEBUG_BUILD NET_DW_PRINT("Doing LESTER busy help") #ENDIF
		//																		SET_BIT(iBoolsBitSet, biL_LesterBusy)
		//																	ELSE
		//																		#IF IS_DEBUG_BUILD NET_DW_PRINT("Doing LESTER busy help - IS_SKYSWOOP_AT_GROUND") #ENDIF
		//																	ENDIF
		//																ELSE
		//																	#IF IS_DEBUG_BUILD NET_DW_PRINT("Doing LESTER busy help - IS_PLAYER_CONTROL_ON") #ENDIF
		//																ENDIF
		//															ELSE
		//																#IF IS_DEBUG_BUILD NET_DW_PRINT("Doing LESTER busy help - IS_HELP_MESSAGE_BEING_DISPLAYED") #ENDIF
		//															ENDIF
		//														ENDIF
		//													ENDIF
		//												ENDIF
													ENDIF
												ELSE
													#IF IS_DEBUG_BUILD 
														NET_DW_PRINT("Player at Lester's but CHECK_FOR_PLAYER_NEAR_LESTERS_DUE_TO_PACIFIC_STANDARD_HEIST_FINISHING")
													#ENDIF
												ENDIF
											ELSE
												#IF IS_DEBUG_BUILD 
													IF IS_CUSTOM_MENU_ON_SCREEN()
														NET_DW_PRINT("Player at Lester's but IS_CUSTOM_MENU_ON_SCREEN")
													ENDIF
													IF IS_PHONE_ONSCREEN()
														NET_DW_PRINT("Player at Lester's but IS_PHONE_ONSCREEN")
													ENDIF
													IF NOT IS_SKYSWOOP_AT_GROUND()
														NET_DW_PRINT("Player at Lester's but IS_SKYSWOOP_AT_GROUND")
													ENDIF
												#ENDIF
											ENDIF
										ELSE
											#IF IS_DEBUG_BUILD NET_DW_PRINT("Player at Lester's but in a vehicle") #ENDIF
										ENDIF
									ELSE
									
										IF IS_PLAYER_USING_BALLISTIC_EQUIPMENT(PLAYER_ID())
											IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM_LCUTBALL")
												PRINT_HELP("FM_LCUTBALL") // Remove ballistics
											ENDIF
											#IF IS_DEBUG_BUILD NET_DW_PRINT("Player at Lester's but in Ballistic outfit") #ENDIF
										ENDIF

									ENDIF
								ELSE
									PRINTLN("Player at Lester's but  IS_PLAYER_CONTROL_ON = OFF")
								ENDIF
								
							ELSE
								IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
									IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM_LCUT_LCP")
										PRINT_HELP("FM_LCUT_LCP") // Lose the cops
									ENDIF
								ENDIF
							ENDIF
						ELSE
							//-- Blip help
							IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneBlipHelp)
								IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE)
									IF NOT IS_TRANSITION_ACTIVE()
									//	PRINT_HELP("FM_LCUT_LBLP") // Lester's house is marked on the map
										IF NOT DOES_BLIP_EXIST(g_blipLesterWarehouse )
											g_blipLesterWarehouse = ADD_BLIP_FOR_COORD(g_vLesterWarehouse)
									
											SET_BLIP_SPRITE(g_blipLesterWarehouse, RADAR_TRACE_LESTER_FAMILY)
											SET_BLIP_COLOUR_FROM_HUD_COLOUR(g_blipLesterWarehouse, HUD_COLOUR_GREEN)
											SET_BLIP_NAME_FROM_TEXT_FILE(g_blipLesterWarehouse, "DLCC_CASHT_CT")
//											IF (GET_MP_INT_CHARACTER_STAT(MP_STAT_GANGOPS_FLOW_MISSION_PROG) != 0
//											OR GET_MP_INT_CHARACTER_STAT(MP_STAT_GANGOPS_FM_MISSION_PROG) != 0)
//												#IF IS_DEBUG_BUILD NET_DW_PRINT("SET_BLIP_AS_SHORT_RANGE g_blipLesterWarehouse") #ENDIF
												SET_BLIP_AS_SHORT_RANGE(g_blipLesterWarehouse, TRUE)
//												SET_BLIP_FLASHES(g_blipLesterWarehouse, TRUE)
//												SET_BLIP_FLASH_TIMER(g_blipLesterWarehouse, 7000)
//											ENDIF
											#IF IS_DEBUG_BUILD NET_DW_PRINT("Added Lester house blip") #ENDIF
										ENDIF
										SET_BIT(iBoolsBitSet, biL_ToldToGoToLester)
										
										SET_BIT(iBoolsBitSet, biL_DoneBlipHelp)
										#IF IS_DEBUG_BUILD NET_DW_PRINT("Set biL_DoneBlipHelp") #ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							IF IS_BIT_SET(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_WantToSeeLester)
								CLEAR_BIT(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_WantToSeeLester)
								#IF IS_DEBUG_BUILD NET_DW_PRINT("Clearing biP_WantToSeeLester as I'm no longer at Lester's") #ENDIF
							ENDIF
							
							IF IS_BIT_SET(iBoolsBitSet, biL_LesterBusy)

								#IF IS_DEBUG_BUILD NET_DW_PRINT("Clearing biL_LesterBusy as I'm not at Trevor's") #ENDIF
								CLEAR_BIT(iBoolsBitSet, biL_LesterBusy)
								
							ENDIF
							
							MAINTAIN_LESTER_WAREHOUSE_HELP()
							
							#IF IS_DEBUG_BUILD
							#IF SCRIPT_PROFILER_ACTIVE
								ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_LESTER_WAREHOUSE_HELP")
							#ENDIF
							#ENDIF
						ENDIF
//					ELSE
//						IF NOT IS_BIT_SET(iBoolsBitSet, biL_ToldToLoseCops)
//							#IF IS_DEBUG_BUILD NET_DW_PRINT("Doing lose cops text") #ENDIF
//							
//							IF DOES_BLIP_EXIST(g_blipLesterWarehouse)
//								SET_BLIP_ROUTE(g_blipLesterWarehouse, FALSE)
//								REMOVE_BLIP(g_blipLesterWarehouse)
//							ENDIF
//							SET_BIT(iBoolsBitSet, biL_ToldToLoseCops)
//							CLEAR_BIT(iBoolsBitSet, biL_ToldToGoToLester)
//						//	Print_Objective_Text("FM_IHELP_LCP")
///						ENDIF
//					ENDIF
				ENDIF
				
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE
					ADD_SCRIPT_PROFILE_MARKER("HAS_PLAYER_REACHED_LESTERS_WAREHOUSE")
				#ENDIF
				#ENDIF
			ELSE
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
						IF IS_SCREEN_FADED_IN()
							IF NOT IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
							AND NOT IS_FM_MISSION_LAUNCH_IN_PROGRESS()
							AND GET_LOCAL_PLAYER_CORONA_POS_STATE() = 0
							AND NOT IS_TRANSITION_ACTIVE()
							AND NOT SHOULD_UI_BE_HIDDEN()
							AND NOT SHOULD_HIDE_ALL_JOB_BLIPS_FOR_YACHT()
							AND NOT IS_LOCAL_PLAYER_DELIVERING_BOUNTY()
							AND NOT ARE_FLOW_MESSAGES_SUPPRESSED_IN_CURRENT_INTERIOR()
								IF NOT HAS_NET_TIMER_STARTED(timeSinceOnMission)
									START_NET_TIMER(timeSinceOnMission)
									#IF IS_DEBUG_BUILD NET_DW_PRINT("Started timeSinceOnMission ") #ENDIF
								ELSE
									IF HAS_NET_TIMER_EXPIRED(timeSinceOnMission, 10)
										CLEAR_BIT(iBoolsBitSet, biL_TextCleared)
										CLEAR_BIT(iBoolsBitSet, biL_ToldToGoToLester)
										CLEAR_BIT(iBoolsBitSet, biL_RequestedCut)
										CLEAR_BIT(iBoolsBitSet, biL_ToldToLoseCops)
										RESET_NET_TIMER(timeSinceOnMission)
										
										IF NOT DOES_BLIP_EXIST(g_blipLesterWarehouse)
											g_blipLesterWarehouse = ADD_BLIP_FOR_COORD(g_vLesterWarehouse)				
											SET_BLIP_SPRITE(g_blipLesterWarehouse, RADAR_TRACE_LESTER_FAMILY)
											SET_BLIP_COLOUR_FROM_HUD_COLOUR(g_blipLesterWarehouse, HUD_COLOUR_GREEN)
											SET_BLIP_NAME_FROM_TEXT_FILE(g_blipLesterWarehouse, "DLCC_CASHT_CT")
//											IF (GET_MP_INT_CHARACTER_STAT(MP_STAT_GANGOPS_FLOW_MISSION_PROG) != 0
//											OR GET_MP_INT_CHARACTER_STAT(MP_STAT_GANGOPS_FM_MISSION_PROG) != 0)
												SET_BLIP_AS_SHORT_RANGE(g_blipLesterWarehouse, TRUE)
//												SET_BLIP_FLASHES(g_blipLesterWarehouse, TRUE)
//												SET_BLIP_FLASH_TIMER(g_blipLesterWarehouse, 7000)
//												#IF IS_DEBUG_BUILD NET_DW_PRINT("SET_BLIP_AS_SHORT_RANGE g_blipLesterWarehouse 2") #ENDIF
//											ENDIF
											#IF IS_DEBUG_BUILD NET_DW_PRINT("Re-adding lester blip") #ENDIF
										ENDIF
										
										#IF IS_DEBUG_BUILD NET_DW_PRINT("Clearing biL_TextCleared as player control is on") #ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT IS_BIT_SET(iBoolsBitSet, biL_TextCleared)
				REQUESTED_LESTER_CUTSCENE()
			ENDIF
			
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("REQUESTED_LESTER_CUTSCENE")
			#ENDIF
			#ENDIF
		BREAK
		
//		CASE 1
//
//		//	IF DO_ARRIVE_LESTER_CUT()
//				PlayerBD[PARTICIPANT_ID_TO_INT()].iLesterCutProg = 2
//				#IF IS_DEBUG_BUILD NET_DW_PRINT("iLesterCutProg = 2 as I'm in lester's house") #ENDIF
//
//		//	ENDIF
//		BREAK
		
		CASE 1
//			IF DO_LESTER_MOCAP()
//				START_NET_TIMER(timeCheckForFade)
				
				RESET_NET_TIMER(g_timeLesterWarehouse)
				REQUESTED_LESTER_CUTSCENE()
				PlayerBD[PARTICIPANT_ID_TO_INT()].iLesterCutProg = 2
				#IF IS_DEBUG_BUILD NET_DW_PRINT("iLesterCutProg = 2 as cutscene is finished") #ENDIF
//			ENDIF
		BREAK
		
		CASE 2
			REQUESTED_LESTER_CUTSCENE()
			IF HAS_NET_TIMER_STARTED(timeCheckForFade)
				IF HAS_NET_TIMER_EXPIRED(timeCheckForFade, 0)
					IF IS_SCREEN_FADED_OUT()
						DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
					ENDIF
					PlayerBD[PARTICIPANT_ID_TO_INT()].iLesterCutProg = 3
					#IF IS_DEBUG_BUILD NET_DW_PRINT("iLesterCutProg = 3 as cutscene is finished") #ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 3
			IF NOT IS_BIT_SET(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DoneCutscene)
				SET_BIT(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DoneCutscene)
				#IF IS_DEBUG_BUILD NET_DW_PRINT("Set biP_DoneCutscene as cutscene finished") #ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH
	
	IF IS_BIT_SET(iBoolsBitSet, biL_DoLesterMocap)
		IF DO_LESTER_MOCAP()
			CLEAR_BIT(iBoolsBitSet, biL_DoLesterMocap)
			START_NET_TIMER(timeCheckForFade)
			#IF IS_DEBUG_BUILD NET_DW_PRINT("Mocap finished, clearing biL_DoLesterMocap") #ENDIF
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("DO_LESTER_MOCAP")
	#ENDIF
	#ENDIF
	
	MAINTAIN_DOOR_BLOCKED_CLIENT()

	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_DOOR_BLOCKED_CLIENT")
	#ENDIF
	#ENDIF
	
	IF SHOULD_LESTER_CUTSCENE_COMPLETE()
		#IF IS_DEBUG_BUILD
			g_bDebugLaunchLesterCut = FALSE
		#ENDIF
		IF DOES_BLIP_EXIST(g_blipLesterWarehouse)
			REMOVE_BLIP(g_blipLesterWarehouse)
		ENDIF
		Clear_Any_Objective_Text_From_This_Script()
	
		INT iStatInt = g_MP_STAT_FM_CUT_DONE[GET_SLOT_NUMBER(-1)]
		SET_BIT(iStatInt, biFmCut_HeistInt)
		SET_BIT(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DoneCutscene)

		SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_FM_CUT_DONE, iStatInt)
		SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_LESTER_WAREHOUSE_CUTSCENE, FALSE)
		SET_LOCAL_PLAYER_FINISHED_INITIAL_HEIST_PHONECALL(TRUE)
		CLEAR_TUTORIAL_INVITES_ARE_BLOCKED() 
		SET_FM_UNLOCKS_BIT_SET()
		IF NOT IS_JOB_ACTIVITY_PROFILE_SETTINGS_SET(PSJA_HEIST_CUTSCENE)
			#IF IS_DEBUG_BUILD NET_DW_PRINT("Setting profile save") #ENDIF
			SET_JOB_ACTIVITY_PROFILE_SETTINGS(PSJA_HEIST_CUTSCENE)
		ELSE
			#IF IS_DEBUG_BUILD NET_DW_PRINT("Profile setting already done") #ENDIF
		ENDIF
		
		#IF FEATURE_GEN9_EXCLUSIVE
		NETWORK_POST_UDS_ACTIVITY_END("FleecaMeetLesterFactory", UDS_ACTIVITY_END_REASON_COMPLETED)
		#ENDIF
		
		// KGM 21/2/15 [BUG 2246472]: To allow checking for Replay Board becoming available after Lester 'intro to heists' cutscene complete
		PRINTLN(".KGM [Heist][DO_LESTER_MOCAP]: Set g_updateFlag_HasBoardBeenUnlocked = TRUE - Allow the g_hasReplayBoardBeenUnlocked to be updated after Lester Intro Cutscene")
		g_updateFlag_HasBoardBeenUnlocked = TRUE
	ENDIF
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("SHOULD_LESTER_CUTSCENE_COMPLETE")
	#ENDIF
	#ENDIF
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	CLOSE_SCRIPT_PROFILE_MARKER_GROUP() 
	#ENDIF
	#ENDIF
ENDPROC





#IF IS_DEBUG_BUILD
	PROC DO_OBJECT_TEST()
		SWITCH iWdObjectTest
			CASE 0
				REQUEST_MODEL(HEI_PROP_HEI_TIMETABLE)
				iWdObjectTest++
			BREAK
			
			CASE 1
				IF HAS_MODEL_LOADED(HEI_PROP_HEI_TIMETABLE)
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						oWdObj = CREATE_OBJECT( HEI_PROP_HEI_TIMETABLE, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<0.0, 2.0, 0.0>>) )
						serverBD.niObj = OBJ_TO_NET( oWdObj )
						SET_NETWORK_ID_EXISTS_ON_ALL_MACHINES(serverBD.niObj,TRUE)
						iWdObjectTest++
					ENDIF
				ENDIF
			BREAK
			
			CASE 2
				ATTACH_ENTITY_TO_ENTITY( NET_TO_OBJ(serverBD.niObj), PLAYER_PED_ID(), GET_PED_BONE_INDEX( PLAYER_PED_ID(), BONETAG_PH_L_HAND ),
									<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, TRUE, TRUE )
				SET_ENTITY_VISIBLE(NET_TO_OBJ(serverBD.niObj), FALSE)
				iWdObjectTest++
			BREAK
		ENDSWITCH
	ENDPROC
	
	PROC UPDATE_WIDGETS()
		
		IF bWdCreateLester
			REQUEST_MODEL(GET_NPC_PED_MODEL(CHAR_LESTER))
			IF CREATE_LESTER_PED()
				bWdCreateLester = FALSE
			ENDIF
		ENDIF
		
		IF bWDUnlockDoor
			IF IS_DOOR_REGISTERED_WITH_SYSTEM(MP_DOOR_CS_ONLY_INT_LESTER)
				DOOR_SYSTEM_SET_DOOR_STATE(MP_DOOR_CS_ONLY_INT_LESTER, DOORSTATE_UNLOCKED, FALSE)
				bWDUnlockDoor = FALSE
			ENDIF
		ENDIF
		
		IF bWdPlayLine
			IF PLAY_SINGLE_LINE_FROM_CONVERSATION(speechFmLesterCut, "LS1AAUD", "LES1A_INTL2", "LES1A_INTL2_1", CONV_PRIORITY_MEDIUM)
				bWdPlayLine = FALSE
			ENDIF
		ENDIF
		
		IF bWdClearDoor
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				NET_DW_PRINT("Clearing doorway via widget.")
				bWdClearDoor = FALSE
				CLEAR_AREA_OF_VEHICLES(<<718.3742, -977.4440, 23.1256>>, fWdAreaToClear, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
			ENDIF
		ENDIF
		
		IF bWdTimeTillHelp
			iWdTimeTillHelp = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(g_timeLesterWarehouse, TRUE)
		ENDIF
		
		IF bWdRunObjectTest
			DO_OBJECT_TEST()
		ENDIF
	ENDPROC
	
	PROC UPDATE_DEBUG_KEYS()
		
	ENDPROC
#ENDIF
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                      Main Loop                          ///////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

SCRIPT(MP_MISSION_DATA missionScriptArgs)
	
	PRINTLN("4618786 - Count Of PlayerBD = ", COUNT_OF(PlayerBD))
	
	#IF IS_DEBUG_BUILD 
	MONITOR_SCRIPT_ARRAY_FOR_SIZE_OVERWRITE(PlayerBD)
	PRINTLN("4618786 - MONITOR_SCRIPT_ARRAY_FOR_SIZE_OVERWRITE(PlayerBD)")
	#ENDIF
	
	PROCESS_PRE_GAME(missionScriptArgs)	
	
	#IF IS_DEBUG_BUILD
		CREATE_WIDGETS()	
	#ENDIF
	
	
	PRINTLN("4618786 - WHILE Count Of PlayerBD = ", COUNT_OF(PlayerBD))
	
	WHILE TRUE
	
		MP_LOOP_WAIT_ZERO()
		
		//PRINTLN("4618786 - Start Count Of PlayerBD = ", COUNT_OF(PlayerBD))
		
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
			SCRIPT_PROFILER_START_OF_FRAME()
		#ENDIF
		#ENDIF		
		
		

		// If we have a match end event, bail.
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
	//	OR NOT IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE)
			#IF IS_DEBUG_BUILD NET_DW_PRINT("About to run cleanup from SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE") #ENDIF
			CLEANUP_SCRIPT()
		ENDIF	
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER( "Cleanup checks")
		#ENDIF
		#ENDIF 
		
		#IF IS_DEBUG_BUILD
			UPDATE_WIDGETS()
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER( "UPDATE_WIDGETS")
			#ENDIF
			
			UPDATE_DEBUG_KEYS()
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER( "UPDATE_DEBUG_KEYS")
			#ENDIF
		#ENDIF
		
		
// *****************************************************************************************************************************************************************	
// *****************************************************************************************************************************************************************	
//												LOCAL PROCESSING
// *****************************************************************************************************************************************************************	
// *****************************************************************************************************************************************************************
		
//		IF GET_MP_INT_CHARACTER_STAT(MP_STAT_GANGOPS_FLOW_MISSION_PROG) = 0
//		AND GET_MP_INT_CHARACTER_STAT(MP_STAT_GANGOPS_FM_MISSION_PROG) = 0
//			GB_MAKE_BLIPS_SHORT_RANGE(g_blipLesterWarehouse)
//		ENDIF
		
		// -----------------------------------
		// Process your game logic.....
		SWITCH GET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT())
			
			// Wait untill the server gives the all go before moving on
			CASE GAME_STATE_INI
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_RUNNING
					#IF IS_DEBUG_BUILD NET_DW_PRINT("playerBD[PARTICIPANT_ID_TO_INT()].iGameState GAME_STATE_INI >  GAME_STATE_RUNNING") #ENDIF
					
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY
					
					#IF IS_DEBUG_BUILD NET_DW_PRINT("playerBD[PARTICIPANT_ID_TO_INT()].iGameState GAME_STATE_INI >  GAME_STATE_TERMINATE_DELAY") #ENDIF
					
				ENDIF
			BREAK
			
			// Main gameplay state.
			CASE GAME_STATE_RUNNING
				
				
				
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING	
					IF NOT HAS_PLAYER_COMPLETED_INITIAL_APARTMENT_HEIST_PHONECALL(PLAYER_ID())
				//	#IF IS_DEBUG_BUILD OR g_bDebugLaunchLesterCut #ENDIF
						PROCESS_LESTER_CUTSCENE_CLIENT()
					ELSE	
						IF DOES_BLIP_EXIST(g_blipLesterWarehouse)
							Clear_Any_Objective_Text_From_This_Script()
						
							SET_BLIP_ROUTE(g_blipLesterWarehouse, FALSE)
							REMOVE_BLIP(g_blipLesterWarehouse)
							IF IS_BIT_SET(iBoolsBitSet, biL_RequestedCut)
							//	IF HAS_THIS_CUTSCENE_LOADED(sLesterCut)
									REMOVE_CUTSCENE()
									#IF IS_DEBUG_BUILD NET_DW_PRINT("REMOVE_CUTSCENE - 1") #ENDIF
							//	ENDIF
							ENDIF
							SET_BIT(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DoneCutscene)
						ENDIF
						
					ENDIF
					
						
					//-- Cleanup if we're critical on new Ambeinet events
					IF IS_PLAYER_CRITICAL_TO_ANY_EVENT(PLAYER_ID())
						IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoLesterMocap)
							MPGlobalsAmbience.bInitHeistCutLaunch = FALSE
							playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_LEAVE
							#IF IS_DEBUG_BUILD NET_DW_PRINT("playerBD[PARTICIPANT_ID_TO_INT()].iGameState GAME_STATE_RUNNING >  GAME_STATE_LEAVE as IS_PLAYER_CRITICAL_TO_ANY_EVENT") #ENDIF
						ELSE
							#IF IS_DEBUG_BUILD NET_DW_PRINT("IS_PLAYER_CRITICAL_TO_ANY_EVENT but cutscene already started") #ENDIF
						ENDIF
					ENDIF
					
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE
						ADD_SCRIPT_PROFILE_MARKER("PROCESS_LESTER_CUTSCENE_CLIENT")
					#ENDIF
					#ENDIF
					
					#IF IS_DEBUG_BUILD
						IF IS_BIT_SET(g_iDebugMenuTutorialLaunch, DEBUG_MENU_HEIST_LAUNCH_INTRO) 
							playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_LEAVE
							#IF IS_DEBUG_BUILD NET_DW_PRINT("playerBD[PARTICIPANT_ID_TO_INT()].iGameState GAME_STATE_RUNNING >  GAME_STATE_LEAVE as DEBUG_MENU_HEIST_LAUNCH_INTRO") #ENDIF
						ENDIF
					#ENDIF
					
					
		
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY
					#IF IS_DEBUG_BUILD NET_DW_PRINT("playerBD[PARTICIPANT_ID_TO_INT()].iGameState GAME_STATE_RUNNING >  GAME_STATE_TERMINATE_DELAY") #ENDIF
				ENDIF
				
			BREAK
			
			CASE GAME_STATE_TERMINATE_DELAY 
				SET_MISSION_FINISHED(serverBD.timeTerminate)
		      	IF IS_MISSION_READY_TO_CLEANUP(serverBD.timeTerminate)
		            playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
					#IF IS_DEBUG_BUILD NET_DW_PRINT("playerBD[PARTICIPANT_ID_TO_INT()].iGameState GAME_STATE_TERMINATE_DELAY >  GAME_STATE_END") #ENDIF
		      	ENDIF
			BREAK
			
			// The game stage the local player is placed when we want him to leave a mission
			CASE GAME_STATE_LEAVE

				playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
				#IF IS_DEBUG_BUILD NET_DW_PRINT("playerBD[PARTICIPANT_ID_TO_INT()].iGameState GAME_STATE_LEAVE >  GAME_STATE_END") #ENDIF

			FALLTHRU
			
			//Cleans up then terminates the mission
			CASE GAME_STATE_END
				#IF IS_DEBUG_BUILD NET_DW_PRINT("About to run cleanup from GAME_STATE_END") #ENDIF
				CLEANUP_SCRIPT()
			BREAK

		ENDSWITCH
		
	 	

		
// *****************************************************************************************************************************************************************	
// *****************************************************************************************************************************************************************	
//												SERVER PROCESSING
// *****************************************************************************************************************************************************************	
// *****************************************************************************************************************************************************************	
		
		
		
		// -----------------------------------
		// Process server game logic		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			
			SWITCH GET_SERVER_MISSION_STATE()
				
				
				CASE GAME_STATE_INI
					serverbd.iPartWithLester = -1
					serverBD.iServerGameState = GAME_STATE_RUNNING
					#IF IS_DEBUG_BUILD NET_DW_PRINT("serverBD.iServerGameState = GAME_STATE_RUNNING") #ENDIF
						
				BREAK
				
				// Look for game end conditions
				CASE GAME_STATE_RUNNING
					MAINTAIN_SERVER_STAGGERED_LOOP()
					
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE
						ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_SERVER_STAGGERED_LOOP")
					#ENDIF
					#ENDIF	
					
					MAINTAIN_SERVER_NON_STAGGERED_LOOP()
					
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE
						ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_SERVER_NON_STAGGERED_LOOP")
					#ENDIF
					#ENDIF	
					
					MAINTAIN_LESTER_CUTSCENE_SERVER()
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE
						ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_LESTER_CUTSCENE_SERVER")
					#ENDIF
					#ENDIF		

					IF HAVE_MISSION_END_CONDITIONS_BEEN_MET()
						serverBD.iServerGameState = GAME_STATE_END
						#IF IS_DEBUG_BUILD NET_DW_PRINT ("serverBD.iServerGameState GAME_STATE_RUNNING > GAME_STATE_END ")#ENDIF
						
					ENDIF
					
				BREAK
				
				CASE GAME_STATE_END
				BREAK
				
			ENDSWITCH
			
		ENDIF
		
		
		
		#IF IS_DEBUG_BUILD 
			#IF SCRIPT_PROFILER_ACTIVE
				SCRIPT_PROFILER_END_OF_FRAME()
			#ENDIF
		#ENDIF			
		
		//PRINTLN("4618786 - End Count Of PlayerBD = ", COUNT_OF(PlayerBD))
	
	ENDWHILE

ENDSCRIPT


