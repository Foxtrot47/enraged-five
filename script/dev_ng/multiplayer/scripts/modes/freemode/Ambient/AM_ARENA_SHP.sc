//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        AM_ARENA_SHP.sc																			//
// Description: This is launched when player requests a cash transaction									//
// Date:  		19/10/2018																					//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "net_include.sch"
USING "net_wait_zero.sch"
USING "mp_scaleform_functions.sch"

INT m_iTransactionSlot
CASH_TRANSACTIONS_DETAILS m_TransactionDetails
SCALEFORM_LOADING_ICON sCashTransactionIconStruct

BOOL bRunCashTransactionPendingIcon = FALSE

#IF IS_DEBUG_BUILD
SCRIPT_TIMER stScriptRunTime
#ENDIF

PROC SCRIPT_CLEANUP()
	PRINTLN("AM_ARENA_SHP - SCRIPT_CLEANUP")
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
	
	#IF IS_DEBUG_BUILD
		INT iTimeUsed = GET_TIME_USED_BY_SCRIPT_TIMER_IN_SECONDS(stScriptRunTime)
		PRINTLN("AM_ARENA_SHP - Script run time = ", iTimeUsed, " seconds.")
	#ENDIF
ENDPROC

PROC SCRIPT_INITIALISE(CASH_TRANSACTION_EVENT scriptData)
	
	IF g_sMPTunables.bBLOCK_NS_TRANS
		PRINTLN("AM_ARENA_SHP - SCRIPT_CLEANUP - bBLOCK_NS_TRANS")
		SCRIPT_CLEANUP()
	ENDIF
	
	PRINTLN("AM_ARENA_SHP - SCRIPT_INITIALISE")
	m_TransactionDetails.eEssentialData = scriptData.eDetails
	m_TransactionDetails.cashInfo 		= scriptData.cashInfo
	//Grab the last piece of extra data as it is too big for the event
	m_TransactionDetails.eExtraData		= g_cashTransactionData[scriptData.iSlot].eExtraData
	m_iTransactionSlot					= scriptData.iSlot
	
	IF scriptData.iSlot < 0
		PRINTLN("AM_ARENA_SHP - SCRIPT_CLEANUP - iSlot < 0")
		SCRIPT_CLEANUP()
	ENDIF
	
	IF NOT VALIDATE_CASH_TRANSACTION_EVENT_DATA(scriptData.eDetails, scriptData.iSlot)
		PRINTLN("AM_ARENA_SHP - SCRIPT_CLEANUP - VALIDATE_CASH_TRANSACTION_EVENT_DATA = FALSE")
		DELETE_CASH_TRANSACTION(scriptData.iSlot)
		SCRIPT_CLEANUP()
	ENDIF
	
	IF m_TransactionDetails.eEssentialData.eTransactionStatus != CASH_TRANSACTION_STATUS_NULL
		PRINTLN("[CASH] AM_ARENA_SHP - Transaction assigned to slot in use: ", scriptData.iSlot)
		m_TransactionDetails.eEssentialData.eTransactionStatus = CASH_TRANSACTION_STATUS_PENDING
	ENDIF
	
	#IF IS_DEBUG_BUILD
		START_NET_TIMER(stScriptRunTime)
	#ENDIF
ENDPROC

FUNC BOOL IS_PENDING_CASH_TRANSACTION_VALID_FOR_SPINNER(CASH_TRANSACTIONS_DETAILS &sCashTransactionDetail)
	IF sCashTransactionDetail.eEssentialData.eTransactionService = SERVICE_SPEND_CASH_DROP
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC RUN_TRANSACTION_SCRIPT()
	BOOL bIsCashTransactionCurrentlyPending = FALSE
	
	INT iTransactionError
		
	// Validate
	IF g_bValidateCashTransactionEvent
		IF sTransactionEventData[m_iTransactionSlot].bValidate
			VALIDATE_CASH_TRANSACTION_SUCCESS_RESULT(m_iTransactionSlot)
			sTransactionEventData[m_iTransactionSlot].bValidate = FALSE
		ENDIF
	ENDIF
	
	// Fake events
	IF m_TransactionDetails.eEssentialData.bTriggerFakeEvent
		PROCESS_EVENT_NETWORK_SHOP_TRANSACTION(m_TransactionDetails.eEssentialData.iTransactionId)
	ENDIF
	
	// Spinner
	IF m_TransactionDetails.eEssentialData.eTransactionStatus = CASH_TRANSACTION_STATUS_PENDING
	AND g_bPendingCashTransactionCommunicatingWithServer
	AND IS_PENDING_CASH_TRANSACTION_VALID_FOR_SPINNER(m_TransactionDetails)
		bIsCashTransactionCurrentlyPending = TRUE
	ENDIF
	
	//Check if we're done with this transaction
	IF g_cashTransactionData[m_iTransactionSlot].eEssentialData.iTransactionId = NET_SHOP_INVALID_ID
	AND m_TransactionDetails.eEssentialData.bTransactionStarted
		IF bIsCashTransactionCurrentlyPending
			SET_LOADING_ICON_INACTIVE()
		ENDIF
		
		SCRIPT_CLEANUP()
	ENDIF
	
	// Flag stale transactions
	IF m_TransactionDetails.eEssentialData.iEventFrameCount > 0
	AND NOT IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
		INT iFramesPerSec = 1000/ROUND(GET_FRAME_TIME()*1000.0)
		IF (GET_FRAME_COUNT() - m_TransactionDetails.eEssentialData.iEventFrameCount) > iFramesPerSec * 10 // Time out after 10s
			PRINTLN("[CASH] agh ### RUN_TRANSACTION_SCRIPT - Transaction has not been cleaned up by script. Add bug for Kenneth R.")
			PRINTLN("...m_TransactionDetails.eEssentialData.iTransactionId = ", m_TransactionDetails.eEssentialData.iTransactionId)
			PRINTLN("...eTransactionType = ", GET_CASH_TRANSACTION_TYPE_DEBUG_STRING(m_TransactionDetails.eEssentialData.eTransactionType))
			PRINTLN("...eTransactionService = ", GET_CASH_TRANSACTION_SERVICE_NAME(m_TransactionDetails.eEssentialData.eTransactionService))
			PRINTLN("...eActionType = ", GET_CASH_TRANSACTION_ACTION_TYPE_DEBUG_STRING(m_TransactionDetails.eEssentialData.eActionType))
			
			SCRIPT_ASSERT("AM_ARENA_SHP - RUN_TRANSACTION_SCRIPT - Transaction has not been cleaned up by script. Send logs to Kenneth R.")
			m_TransactionDetails.eEssentialData.iEventFrameCount = -1
		ENDIF
	ENDIF
	
	// Process transactions passed to this script
	IF USE_SERVER_TRANSACTIONS()
	AND m_TransactionDetails.eEssentialData.eTransactionStatus = CASH_TRANSACTION_STATUS_PENDING
	AND m_TransactionDetails.eEssentialData.bTransactionReady
	AND NOT m_TransactionDetails.eEssentialData.bTransactionStarted
	
		IF (GET_FRAME_COUNT() - m_TransactionDetails.eEssentialData.iTransactionDelay) >= GET_TRANSACTION_FRAME_DELAY_FOR_TYPE(m_TransactionDetails.eEssentialData.eCashTransactionType)
			
			iTransactionError = 0
			
			IF NOT NET_GAMESERVER_IS_SESSION_VALID(GET_ACTIVE_CHARACTER_SLOT())
			OR NET_GAMESERVER_IS_SESSION_REFRESH_PENDING()
				iTransactionError = 1
			ELIF m_TransactionDetails.eEssentialData.eTransactionStatus != CASH_TRANSACTION_STATUS_PENDING
				iTransactionError = 2
			ELIF m_TransactionDetails.eEssentialData.eCashTransactionType != CASH_TRANSACTION_TYPE_BASKET
			AND NOT NET_GAMESERVER_BEGIN_SERVICE(m_TransactionDetails.eEssentialData.iTransactionId, m_TransactionDetails.eEssentialData.eTransactionCategory, m_TransactionDetails.eEssentialData.eTransactionService, m_TransactionDetails.eEssentialData.eActionType, m_TransactionDetails.eEssentialData.iCost, m_TransactionDetails.eEssentialData.eFlags)
				iTransactionError = 3
			ELIF NOT NET_GAMESERVER_CHECKOUT_START(m_TransactionDetails.eEssentialData.iTransactionId)
				iTransactionError = 4
			ELSE
				PRINTLN("[CASH] AM_ARENA_SHP - RUN_TRANSACTION_SCRIPT - Transaction started [", m_TransactionDetails.eEssentialData.iTransactionId, "]")
				m_TransactionDetails.eEssentialData.bTransactionStarted 						= TRUE
				g_cashTransactionData[m_iTransactionSlot].eEssentialData.bTransactionStarted 	= TRUE
				g_cashTransactionData[m_iTransactionSlot].eEssentialData.iTransactionId 		= m_TransactionDetails.eEssentialData.iTransactionId
			ENDIF
			
			IF iTransactionError != 0
				PRINTLN("[CASH] AM_ARENA_SHP - RUN_TRANSACTION_SCRIPT - Transaction aborted [", m_TransactionDetails.eEssentialData.iTransactionId, "], code ", iTransactionError)
				DELETE_CASH_TRANSACTION(m_iTransactionSlot)
				SCRIPT_CLEANUP()
			ENDIF
		ELSE
			PRINTLN("[CASH] AM_ARENA_SHP - RUN_TRANSACTION_SCRIPT - Transaction queued [", m_TransactionDetails.eEssentialData.iTransactionId, "]")
		ENDIF
	ENDIF	
	
	//#2053093 - Add some kind of visual showing the transaction is in process
	IF NOT bRunCashTransactionPendingIcon
		IF bIsCashTransactionCurrentlyPending
			REFRESH_SCALEFORM_LOADING_ICON(sCashTransactionIconStruct)
			bRunCashTransactionPendingIcon = TRUE
		ENDIF
	ELSE
		IF bIsCashTransactionCurrentlyPending
			sCashTransactionIconStruct.sMainStringSlot = "HUD_TRANSP"	//Transaction Pending
			RUN_SCALEFORM_LOADING_ICON(sCashTransactionIconStruct, SHOULD_REFRESH_SCALEFORM_LOADING_ICON(sCashTransactionIconStruct))
		ELSE
			bRunCashTransactionPendingIcon = FALSE
			SET_LOADING_ICON_INACTIVE()
		ENDIF
	ENDIF
ENDPROC

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                      Main Loop                      //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
SCRIPT(CASH_TRANSACTION_EVENT scriptData)
	
	IF NETWORK_IS_GAME_IN_PROGRESS() 
		SCRIPT_INITIALISE(scriptData)	
	ELSE
		SCRIPT_CLEANUP()
	ENDIF
	
	// Main loop
	WHILE TRUE
		
		// One wait to rule them all. This can be the ONLY wait from here on in.....
		MP_LOOP_WAIT_ZERO()
		
		IF g_sMPTunables.bBLOCK_NS_TRANS
			PRINTLN("AM_ARENA_SHP - SCRIPT_CLEANUP - bBLOCK_NS_TRANS")
			SCRIPT_CLEANUP()
		ENDIF
		
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			PRINTLN("AM_ARENA_SHP - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE = TRUE")
			SCRIPT_CLEANUP()
		ENDIF
		
		RUN_TRANSACTION_SCRIPT()
		
	ENDWHILE
	
ENDSCRIPT