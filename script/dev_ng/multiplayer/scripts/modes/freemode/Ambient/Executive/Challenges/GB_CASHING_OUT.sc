
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:		GB_CASHING_OUT.sc																									//
// Written by:  Steve Tiley																											//
// Date: 		08/02/2016																											//
//																																	//
// Description: Members of the organisation compete against one another to steal the most cash from ATMs using hacking mini-games	//
//																																	//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "net_mission.sch"
USING "net_common_functions.sch"


//----------------------
//	INCLUDES
//----------------------
USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_camera.sch"
USING "commands_pad.sch"
USING "commands_script.sch"
USING "flow_public_core_override.sch"
USING "net_wait_zero.sch"
USING "net_mission.sch"
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_event.sch"
USING "net_include.sch"
USING "net_events.sch"
USING "net_spawn.sch"
USING "net_ambience.sch"
USING "net_gang_boss.sch"
USING "am_common_ui.sch"
USING "DM_Leaderboard.sch"
USING "net_gang_boss_spectate.sch"
USING "hacking_public.sch"


//----------------------
//	ENUM
//----------------------

ENUM eMODE_STATE
	eMODESTATE_INIT = 0,
	eMODESTATE_RUN,
	eMODESTATE_REWARDS,
	eMODESTATE_END
ENDENUM

ENUM eEND_REASON
	eENDREASON_NO_REASON_YET = 0,
	eENDREASON_TIME_UP,
	eENDREASON_NO_BOSS_LEFT,
	eENDREASON_ALL_GOONS_LEFT,
	eENDREASON_WIN_CONDITION_TRIGGERED
ENDENUM

ENUM eSERVER_BITSET_0
	eSERVERBITSET0_ALL_PARTICIPANTS_COMPLETED_REWARDS = 0,
	eSERVERBITSET0_CHAL_END_WITH_WINNER,
	eSERVERBITSET0_CHAL_END_NO_CASH_COLLECTED,
	eSERVERBITSET0_BOSS_LEFT,
	eSERVERBITSET0_ALL_ATMS_ROBBED,
	
	eSERVERBITSET0_END // Keep this at the end.
ENDENUM

ENUM eCLIENT_BITSET_0
	eCLIENTBITSET0_COMPLETED_REWARDS = 0,
	eCLIENTBITSET0_HACKING_ATM,
	eCLIENTBITSET0_KILLED_WITH_CASH,
	eCLIENTBITSET0_NEAR_AN_ATM,
	eCLIENTBITSET0_HACKED_AN_ATM,
	eCLIENTBITSET0_END // Keep this at the end.
ENDENUM

ENUM eLOCAL_BITSET_0
	eLOCALBITSET0_EXECUTED_END_TELEMTRY = 0,
	eLOCALBITSET0_CALCULATED_REWARDS,
	eLOCALBITSET0_CALLED_COMMON_SETUP,
	eLOCALBITSET0_CALLED_SAFE_FOR_SPEC_CAM_SETUP,
	eLOCALBITSET0_CALLED_START_COMMON_TELEMETRY,
	eLOCALBITSET0_TAKE_COMBINED_TOTAL,
	eLOCALBITSET0_DISABLED_CONTROL,
	eLOCALBITSET0_MODE_SETUP,
	eLOCALBITSET0_QUALIFYING_PARTICIPANT,
	eLOCALBITSET0_SPECTATING_GANG,
	eLOCALBITSET0_END // Keep this at the end.
ENDENUM

// Custom Enum
ENUM MISSION_STAGE_ENUM
	eMS_INIT = 0,
	eMS_COLLECT,
	
	eMS_END // Keep this at the end.
ENDENUM

ENUM HELP_TEXT_ENUM
	eHELP_INTRO = 0,
	eHELP_PRESS_RIGHT,
	eHELP_CASH_DROPPED_WHEN_KILLED,
	eHELP_WANTED_LEVEL_APPLIED,
	eHELP_BEATEN_TO_HACK,
	
	eHELP_END // Keep this at the end.
ENDENUM

ENUM BIG_MESSAGE_ENUM
	eBIGM_INTRO = 0,
	eBIGM_YOU_WON,
	eBIGM_YOU_LOST,
	eBIGM_NO_CASH_STOLEN,
	eBIGM_BOSS_LEFT,
	
	eBIGM_END // Keep this at the end.
ENDENUM

ENUM MAP_QUADRANT_ENUM
	eMQ_TOP_LEFT = 0,
	eMQ_TOP_RIGHT,
	eMQ_BOTTOM_LEFT,
	eMQ_BOTTOM_RIGHT,
	
	eMQ_INVALID
ENDENUM

ENUM HACKING_OUTCOME
	eHACK_OUTCOME_SUCCESS = 0,
	eHACK_OUTCOME_FAILURE,
	eHACK_OUTCOME_ABANDON,
	eHACK_OUTCOME_KILLED,
	
	eHACK_OUTCOME_UNKNOWN // Keep this at the end.
ENDENUM

ENUM FINAL_COUNTDOWN_STAGE_ENUM
	eFCOUNTDOWN_PREP = 0,
	eFCOUNTDOWN_START,
	eFCOUNTDOWN_END_OR_KILL
ENDENUM


//----------------------
//	DEBUG
//----------------------

#IF IS_DEBUG_BUILD

ENUM eSERVER_DEBUG_BITSET_0
	eSERVERDEBUGBITSET0_TEMP = 0,
	eSERVERDEBUGBITSET0_PLAYER_S_PASSED,
	eSERVERDEBUGBITSET0_PLAYER_F_FAILED,
	
	eSERVERDEBUGBITSET0_END // Keep this at the end.
ENDENUM

ENUM eCLIENT_DEBUG_BITSET_0
	eCLIENTDEBUGBITSET0_I_HAVE_S_PASSED = 0,
	eCLIENTDEBUGBITSET0_I_HAVE_F_FAILED,
	
	eCLIENTDEBUGBITSET0_END // Keep this at the end.
ENDENUM

ENUM eLOCAL_DEBUG_BITSET_0
	eLOCALDEBUGBITSET0_TERMINATE_SCRIPT_NOW = 0,
	
	eLOCALDEBUGBITSET0_END // Keep this at the end.
ENDENUM

#ENDIF


//----------------------
//	MUSIC
//----------------------

FINAL_COUNTDOWN_STAGE_ENUM eFinalCountdownStage
INT iFinalCountdownBitSet
CONST_INT iFCBS_TriggeredPreCountdown	0
CONST_INT iFCBS_TriggeredCountdown		1
CONST_INT iFCBS_ReEnabledRadioControl	2
CONST_INT iFCBS_TriggeredCountdownKill	3
CONST_INT iFCBS_SuppressWanted			4
CONST_INT iFCBS_Trigger5s				5

//----------------------
//	CONSTANTS
//----------------------

CONST_INT GAME_STATE_INIT 								0
CONST_INT GAME_STATE_RUNNING							1
CONST_INT GAME_STATE_TERMINATE_DELAY					2
CONST_INT GAME_STATE_END								3

CONST_INT TRACK_GOON_DELAY								1000*10

CONST_INT HACKING_IP_CONNECT							0
CONST_INT HACKING_BRUTEFORCE							1
CONST_INT HACKING_NEW_MG								2
CONST_INT MAX_NUM_HACKING_MG							3

CONST_INT NUM_MINI_LEADERBOARD_PLAYERS					4

CONST_INT TEN_SECONDS									1000*10
CONST_INT TWENTY_SECONDS								TEN_SECONDS*2
CONST_INT THIRTY_SECONDS								TWENTY_SECONDS+TEN_SECONDS
CONST_INT FORTY_SECONDS									THIRTY_SECONDS+TEN_SECONDS
CONST_INT FIFTY_SECONDS									FORTY_SECONDS+TEN_SECONDS
CONST_INT ONE_MINUTE									FIFTY_SECONDS+TEN_SECONDS

CONST_INT MAX_NUM_ATMs					107

CONST_INT NUM_TOP_LEFT_QUAD_ATMs 		20
CONST_INT NUM_TOP_RIGHT_QUAD_ATMs		21
CONST_INT NUM_BOTTOM_LEFT_QUAD_ATMs		45
CONST_INT NUM_BOTTOM_RIGHT_QUAD_ATMs	21

CONST_INT ciFM_EVENT_BLIP_FLASH_TIME 7000

//----------------------
//	STRUCT
//----------------------

//----------------------
//	LOCAL VARIABLES
//----------------------

GANG_BOSS_MANAGE_REWARDS_DATA sGangBossManageRewardsData
GB_MAINTAIN_SPECTATE_VARS sSpecVars
GB_STRUCT_BOSS_END_UI sEndUI

INT iLocalBitset0

INT iFocusParticipant

INT iLocalBigMessageBitset0
INT iLocalHelpBitset0

INT iTimeRemaining
INT iOvertime = 0

INT localSyncID
INT iMyLastPosition
INT serverStaggeredPlayerLoop

INT iMinATMCash
INT iMaxATMCash

INT iCleanupBossSpecStep

BLIP_INDEX ATMBlip[MAX_NUM_ATMs]

BOOL bBeenKilled = FALSE
BOOL bSet


// Hacking minigames ---

HACKING_OUTCOME eHackingOutcome
S_HACKING_DATA hackCashingOut
INT iHackingProg = -1
INT iMyHackMG = -1
INT iHackSpeed
SCRIPT_TIMER hackTimer
SCRIPT_TIMER EndOfHackTimer
SCRIPT_TIMER localHackStartTimer
INT iHackTime
INT iHackContextIntention
INT iContextStage = 0
INT iATM_beingHacked = -1

// --


#IF IS_DEBUG_BUILD
INT iLocalDebugBitset
BOOL bShowSpammyPrints = FALSE
#ENDIF


//----------------------
//	BROADCAST VARIABLES
//----------------------

// Everyone can read this data, only the server can update it
STRUCT ServerBroadcastData

	INT iServerGameState = GAME_STATE_INIT
	INT iServerBitSet
	SCRIPT_TIMER modeTimer
	eMODE_STATE eModeState
	eEND_REASON eEndReason
	
	PLAYER_INDEX piGangBoss
	PLAYER_INDEX piWinner
	
	INT iATMsRobbed = 0
	INT iWinningScore
	
	INT iATMBitSet0
	INT iATMBitSet1
	INT iATMBitSet2
	INT iATMBitSet3
	
	// Leaderboard sorting
	INT	iPlayerBitSet
	INT	iServerSyncID
	INT leaderboardPlayerScore[GB_MAX_GANG_SIZE_INCLUDING_BOSS_EXEC]
	AMBIENT_ACTIVITY_LEADERBOARD_STRUCT sSortedLeaderboard[GB_MAX_GANG_SIZE_INCLUDING_BOSS_EXEC]
	
	#IF IS_DEBUG_BUILD
	INT iDebugBitset
	INT iSPassPart = -1
	INT iFFailPart = -1
	BOOL bFakeEndOfModeTimer
	#ENDIF
	
	INT					iMatchId1
	INT					iMatchId2
	
ENDSTRUCT

ServerBroadcastData serverBD

// The client broadcast data.
// Everyone can read this data, only the local player only the server can update it
STRUCT PlayerBroadcastData
	
	INT iClientGameState = GAME_STATE_INIT
	INT iClientBitSet
	
	INT iCashCollected
	INT iNumSuccessfulHacks = 0
	INT iTotalHacks = 0
	INT iHighestWantedRating = 0
	
	MISSION_STAGE_ENUM eClientStage
	
	#IF IS_DEBUG_BUILD
	INT iDebugBitset
	#ENDIF
	
ENDSTRUCT

PlayerBroadcastData playerBD[GB_MAX_GANG_SIZE_INCLUDING_BOSS_EXEC]

//----------------------
//	FUNCTIONS
//----------------------

#IF IS_DEBUG_BUILD

// Bitset names ----------------------------------------------------->|

FUNC STRING GET_SERVER_BIT0_NAME(eSERVER_BITSET_0 eBitset)
	
	SWITCH eBitset
		CASE eSERVERBITSET0_ALL_PARTICIPANTS_COMPLETED_REWARDS	RETURN "eSERVERBITSET0_ALL_PARTICIPANTS_COMPLETED_REWARDS"
		CASE eSERVERBITSET0_CHAL_END_WITH_WINNER				RETURN "eSERVERBITSET0_CHAL_END_WITH_WINNER"
		CASE eSERVERBITSET0_CHAL_END_NO_CASH_COLLECTED			RETURN "eSERVERBITSET0_CHAL_END_NO_CASH_COLLECTED"
		CASE eSERVERBITSET0_BOSS_LEFT							RETURN "eSERVERBITSET0_BOSS_LEFT"
		CASE eSERVERBITSET0_ALL_ATMS_ROBBED						RETURN "eSERVERBITSET0_ALL_ATMS_ROBBED"
		CASE eSERVERBITSET0_END									RETURN "eSERVERBITSET0_END"
	ENDSWITCH
	
	RETURN "***INVALID***"
	
ENDFUNC

FUNC STRING GET_LOCAL_BIT0_NAME(eLOCAL_BITSET_0 eBitset)
	
	SWITCH eBitset
		CASE eLOCALBITSET0_EXECUTED_END_TELEMTRY			RETURN "eLOCALBITSET0_EXECUTED_END_TELEMTRY"
		CASE eLOCALBITSET0_CALCULATED_REWARDS				RETURN "eLOCALBITSET0_CALCULATED_REWARDS"
		CASE eLOCALBITSET0_CALLED_COMMON_SETUP				RETURN "eLOCALBITSET0_CALLED_COMMON_SETUP"
		CASE eLOCALBITSET0_CALLED_SAFE_FOR_SPEC_CAM_SETUP	RETURN "eLOCALBITSET0_CALLED_SAFE_FOR_SPEC_CAM_SETUP"
		CASE eLOCALBITSET0_TAKE_COMBINED_TOTAL				RETURN "eLOCALBITSET0_TAKE_COMBINED_TOTAL"
		CASE eLOCALBITSET0_DISABLED_CONTROL					RETURN "eLOCALBITSET0_DISABLED_CONTROL"
		CASE eLOCALBITSET0_MODE_SETUP						RETURN "eLOCALBITSET0_MODE_SETUP"
		CASE eLOCALBITSET0_QUALIFYING_PARTICIPANT			RETURN "eLOCALBITSET0_QUALIFYING_PARTICIPANT"
		CASE eLOCALBITSET0_SPECTATING_GANG					RETURN "eLOCALBITSET0_SPECTATING_GANG"
		CASE eLOCALBITSET0_END								RETURN "eLOCALBITSET0_END"
	ENDSWITCH
	
	RETURN "***INVALID***"
	
ENDFUNC

FUNC STRING GET_CLIENT_BIT0_NAME(eCLIENT_BITSET_0 eBitset)
	
	SWITCH eBitset
		CASE eCLIENTBITSET0_COMPLETED_REWARDS				RETURN "eCLIENTBITSET0_COMPLETED_REWARDS"
		CASE eCLIENTBITSET0_HACKING_ATM						RETURN "eCLIENTBITSET0_HACKING_ATM"
		CASE eCLIENTBITSET0_KILLED_WITH_CASH				RETURN "eCLIENTBITSET0_KILLED_WITH_CASH"
		CASE eCLIENTBITSET0_NEAR_AN_ATM						RETURN "eCLIENTBITSET0_NEAR_AN_ATM"
		CASE eCLIENTBITSET0_HACKED_AN_ATM					RETURN "eCLIENTBITSET0_HACKED_AN_ATM"
		CASE eCLIENTBITSET0_END								RETURN "eCLIENTBITSET0_END"
	ENDSWITCH
	
	RETURN "***INVALID***"
	
ENDFUNC

// ------------------------------------------------------------------|<

#ENDIF

// Server Bitset ---------------------------------------------------->|

FUNC BOOL IS_SERVER_BIT0_SET(eSERVER_BITSET_0 eBitset)
	RETURN IS_BIT_SET(serverBD.iServerBitSet, ENUM_TO_INT(eBitset))
ENDFUNC

PROC SET_SERVER_BIT0(eSERVER_BITSET_0 eBitset #IF IS_DEBUG_BUILD , BOOL bBlockLogPrints = FALSE #ENDIF)

	#IF IS_DEBUG_BUILD
	IF NOT bBlockLogPrints
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, ENUM_TO_INT(eBitset))
			STRING strTemp = GET_SERVER_BIT0_NAME(eBitset)
			PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] GET_SERVER_BIT0_NAME - ", strTemp)
		ENDIF
	ENDIF
	#ENDIF
	
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		#IF IS_DEBUG_BUILD
		SCRIPT_ASSERT("[EXEC1] [GB CASHING OUT] [ST EXEC] calling SET_SERVER_BIT0 on a non host.")
		PRINTLN("")
		#ENDIF
		EXIT
	ENDIF
	
	SET_BIT(serverBD.iServerBitSet, ENUM_TO_INT(eBitset))
	
ENDPROC

PROC CLEAR_SERVER_BIT0(eSERVER_BITSET_0 eBitset #IF IS_DEBUG_BUILD , BOOL bBlockLogPrints = FALSE #ENDIF)

	#IF IS_DEBUG_BUILD
	IF NOT bBlockLogPrints
		IF IS_BIT_SET(serverBD.iServerBitSet, ENUM_TO_INT(eBitset))
			STRING strTemp = GET_SERVER_BIT0_NAME(eBitset)
			PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] CLEAR_SERVER_BIT0 - ", strTemp)
		ENDIF
	ENDIF
	#ENDIF
	
	CLEAR_BIT(serverBD.iServerBitSet, ENUM_TO_INT(eBitset))
	
ENDPROC

// ------------------------------------------------------------------|<

// Client Bitset ---------------------------------------------------->|

FUNC BOOL IS_CLIENT_BIT0_SET(PARTICIPANT_INDEX partId, eCLIENT_BITSET_0 eBitset)
	RETURN IS_BIT_SET(playerBD[NATIVE_TO_INT(partId)].iClientBitSet, ENUM_TO_INT(eBitset))
ENDFUNC

PROC SET_CLIENT_BIT0(eCLIENT_BITSET_0 eBitset #IF IS_DEBUG_BUILD , BOOL bBlockLogPrints = FALSE #ENDIF)

	#IF IS_DEBUG_BUILD
	IF NOT bBlockLogPrints
		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, ENUM_TO_INT(eBitset))
			STRING strTemp = GET_CLIENT_BIT0_NAME(eBitset)
			PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] SET_CLIENT_BIT0 - ", strTemp)
		ENDIF
	ENDIF
	#ENDIF
	
	SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, ENUM_TO_INT(eBitset))
	
ENDPROC

PROC CLEAR_CLIENT_BIT0(eCLIENT_BITSET_0 eBitset #IF IS_DEBUG_BUILD , BOOL bBlockLogPrints = FALSE #ENDIF)

	#IF IS_DEBUG_BUILD
	IF NOT bBlockLogPrints
		IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, ENUM_TO_INT(eBitset))
			STRING strTemp = GET_CLIENT_BIT0_NAME(eBitset)
			PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] CLEAR_CLIENT_BIT0 - ", strTemp)
		ENDIF
	ENDIF
	#ENDIF
	
	CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, ENUM_TO_INT(eBitset))
	
ENDPROC

// ------------------------------------------------------------------|<

// Local Bitset ----------------------------------------------------->|

FUNC BOOL IS_LOCAL_BIT0_SET(eLOCAL_BITSET_0 eBitset)
	RETURN IS_BIT_SET(iLocalBitset0, ENUM_TO_INT(eBitset))
ENDFUNC

PROC SET_LOCAL_BIT0(eLOCAL_BITSET_0 eBitset #IF IS_DEBUG_BUILD , BOOL bBlockLogPrints = FALSE #ENDIF)

	#IF IS_DEBUG_BUILD
	IF NOT bBlockLogPrints
		IF NOT IS_BIT_SET(iLocalBitset0, ENUM_TO_INT(eBitset))
			STRING strTemp = GET_LOCAL_BIT0_NAME(eBitset)
			PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] SET_LOCAL_BIT0 - ", strTemp)
		ENDIF
	ENDIF
	#ENDIF
	
	SET_BIT(iLocalBitset0, ENUM_TO_INT(eBitset))
	
ENDPROC

PROC CLEAR_LOCAL_BIT0(eLOCAL_BITSET_0 eBitset #IF IS_DEBUG_BUILD , BOOL bBlockLogPrints = FALSE #ENDIF)

	#IF IS_DEBUG_BUILD
	IF NOT bBlockLogPrints
		IF IS_BIT_SET(iLocalBitset0, ENUM_TO_INT(eBitset))
			STRING strTemp = GET_LOCAL_BIT0_NAME(eBitset)
			PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] CLEAR_LOCAL_BIT0 - ", strTemp)
		ENDIF
	ENDIF
	#ENDIF
	
	CLEAR_BIT(iLocalBitset0, ENUM_TO_INT(eBitset))
	
ENDPROC

// ------------------------------------------------------------------|<

// Local Help Text Bitset ------------------------------------------->|

FUNC BOOL IS_LOCAL_HELPTEXT_BIT0_SET(INT iBitset)
	RETURN IS_BIT_SET(iLocalHelpBitset0, iBitset)
ENDFUNC

PROC SET_LOCAL_HELPTEXT_BIT0(INT iBitset #IF IS_DEBUG_BUILD , BOOL bBlockLogPrints = FALSE #ENDIF)

	#IF IS_DEBUG_BUILD
	IF NOT bBlockLogPrints
		IF NOT IS_BIT_SET(iLocalBitset0, iBitset)
			PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] SET_LOCAL_HELPTEXT_BIT0 - ", iBitset)
		ENDIF
	ENDIF
	#ENDIF
	
	SET_BIT(iLocalHelpBitset0, iBitset)
	
ENDPROC

PROC CLEAR_LOCAL_HELPTEXT_BIT0(INT iBitset #IF IS_DEBUG_BUILD , BOOL bBlockLogPrints = FALSE #ENDIF)

	#IF IS_DEBUG_BUILD
	IF NOT bBlockLogPrints
		IF IS_BIT_SET(iLocalBitset0, iBitset)
			PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] CLEAR_LOCAL_HELPTEXT_BIT0 - ", iBitset)
		ENDIF
	ENDIF
	#ENDIF
	
	CLEAR_BIT(iLocalHelpBitset0, iBitset)
	
ENDPROC

// ------------------------------------------------------------------|<

// Local Big Message Bitset ------------------------------------------->|

FUNC BOOL IS_LOCAL_BIGMESSAGE_BIT0_SET(INT iBitset)
	RETURN IS_BIT_SET(iLocalBigMessageBitset0, iBitset)
ENDFUNC

PROC SET_LOCAL_BIGMESSAGE_BIT0(INT iBitset #IF IS_DEBUG_BUILD , BOOL bBlockLogPrints = FALSE #ENDIF)

	#IF IS_DEBUG_BUILD
	IF NOT bBlockLogPrints
		IF NOT IS_BIT_SET(iLocalBitset0, iBitset)
			PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] SET_LOCAL_BIGMESSAGE_BIT0 - ", iBitset)
		ENDIF
	ENDIF
	#ENDIF
	
	SET_BIT(iLocalBigMessageBitset0, iBitset)
	
ENDPROC

PROC CLEAR_LOCAL_BIGMESSAGE_BIT0(INT iBitset #IF IS_DEBUG_BUILD , BOOL bBlockLogPrints = FALSE #ENDIF)

	#IF IS_DEBUG_BUILD
	IF NOT bBlockLogPrints
		IF IS_BIT_SET(iLocalBitset0, iBitset)
			PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] CLEAR_LOCAL_BIGMESSAGE_BIT0 - ", iBitset)
		ENDIF
	ENDIF
	#ENDIF
	
	CLEAR_BIT(iLocalBigMessageBitset0, iBitset)
	
ENDPROC

// ------------------------------------------------------------------|<

#IF IS_DEBUG_BUILD

// Debug Bitset Names ----------------------------------------------->|

FUNC STRING GET_SERVER_DEBUG_BIT0_NAME(eSERVER_DEBUG_BITSET_0 eBitset)
	
	SWITCH eBitset
		CASE eSERVERDEBUGBITSET0_TEMP	RETURN "eSERVERDEBUGBITSET0_TEMP"
		CASE eSERVERDEBUGBITSET0_END	RETURN "eSERVERDEBUGBITSET0_END"
	ENDSWITCH
	
	RETURN "***INVALID***"
	
ENDFUNC

FUNC STRING GET_CLIENT_DEBUG_BIT0_NAME(eCLIENT_DEBUG_BITSET_0 eBitset)
	
	SWITCH eBitset
		CASE eCLIENTDEBUGBITSET0_I_HAVE_S_PASSED	RETURN "eCLIENTDEBUGBITSET0_I_HAVE_S_PASSED"
		CASE eCLIENTDEBUGBITSET0_I_HAVE_F_FAILED	RETURN "eCLIENTDEBUGBITSET0_I_HAVE_F_FAILED"
		CASE eCLIENTDEBUGBITSET0_END				RETURN "eCLIENTDEBUGBITSET0_END"
	ENDSWITCH
	
	RETURN "***INVALID***"
	
ENDFUNC

FUNC STRING GET_LOCAL_DEBUG_BIT0_NAME(eLOCAL_DEBUG_BITSET_0 eBitset)
	
	SWITCH eBitset
		CASE eLOCALDEBUGBITSET0_TERMINATE_SCRIPT_NOW	RETURN "eLOCALDEBUGBITSET0_TERMINATE_SCRIPT_NOW"
		CASE eLOCALDEBUGBITSET0_END						RETURN "eLOCALBITSET0_END"
	ENDSWITCH
	
	RETURN "***INVALID***"
	
ENDFUNC

// ------------------------------------------------------------------|<

// Server Debug Bitset ---------------------------------------------->|

FUNC BOOL IS_SERVER_DEBUG_BIT0_SET(eSERVER_DEBUG_BITSET_0 eBitset)
	RETURN IS_BIT_SET(serverBD.iDebugBitset, ENUM_TO_INT(eBitset))
ENDFUNC

PROC SET_SERVER_DEBUG_BIT0(eSERVER_DEBUG_BITSET_0 eBitset, BOOL bBlockLogPrints = FALSE)
	
	IF NOT bBlockLogPrints
		IF NOT IS_BIT_SET(serverBD.iDebugBitset, ENUM_TO_INT(eBitset))
			STRING strTemp = GET_SERVER_DEBUG_BIT0_NAME(eBitset)
			PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] GET_SERVER_DEBUG_BIT0_NAME - ", strTemp)
		ENDIF
	ENDIF
	
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		SCRIPT_ASSERT("[EXEC1] [GB CASHING OUT] [ST EXEC] calling SET_SERVER_DEBUG_BIT0 on a non host.")
		PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] calling SET_SERVER_DEBUG_BIT0 on a non host.")
		EXIT
	ENDIF
	
	SET_BIT(serverBD.iDebugBitset, ENUM_TO_INT(eBitset))
	
ENDPROC

PROC CLEAR_SERVER_DEBUG_BIT0(eCLIENT_DEBUG_BITSET_0 eBitset, BOOL bBlockLogPrints = FALSE)

	IF NOT bBlockLogPrints
		IF IS_BIT_SET(serverBD.iDebugBitset, ENUM_TO_INT(eBitset))
			STRING strTemp = GET_SERVER_DEBUG_BIT0_NAME(eBitset)
			PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] CLEAR_SERVER_DEBUG_BIT0 - ", strTemp)
		ENDIF
	ENDIF
	
	CLEAR_BIT(serverBD.iDebugBitset, ENUM_TO_INT(eBitset))
	
ENDPROC

// ------------------------------------------------------------------|<

// Client Debug Bitset ---------------------------------------------->|



FUNC BOOL IS_CLIENT_DEBUG_BIT0_SET(PARTICIPANT_INDEX partId, eCLIENT_DEBUG_BITSET_0 eBitset)
	RETURN IS_BIT_SET(playerBD[NATIVE_TO_INT(partId)].iDebugBitset, ENUM_TO_INT(eBitset))
ENDFUNC

PROC SET_CLIENT_DEBUG_BIT0(eCLIENT_DEBUG_BITSET_0 eBitset, BOOL bBlockLogPrints = FALSE)

	IF NOT bBlockLogPrints
		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iDebugBitset, ENUM_TO_INT(eBitset))
			STRING strTemp = GET_CLIENT_DEBUG_BIT0_NAME(eBitset)
			PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] SET_CLIENT_DEBUG_BIT0 - ", strTemp)
		ENDIF
	ENDIF
	
	SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iDebugBitset, ENUM_TO_INT(eBitset))
	
ENDPROC

PROC CLEAR_CLIENT_DEBUG_BIT0(eCLIENT_DEBUG_BITSET_0 eBitset, BOOL bBlockLogPrints = FALSE)

	IF NOT bBlockLogPrints
		IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iDebugBitset, ENUM_TO_INT(eBitset))
			STRING strTemp = GET_CLIENT_DEBUG_BIT0_NAME(eBitset)
			PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] CLEAR_CLIENT_DEBUG_BIT0 - ", strTemp)
		ENDIF
	ENDIF
	
	CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iDebugBitset, ENUM_TO_INT(eBitset))
	
ENDPROC

// ------------------------------------------------------------------|<

// Local Debug Bitset ----------------------------------------------->|

FUNC BOOL IS_LOCAL_DEBUG_BIT0_SET(eLOCAL_DEBUG_BITSET_0 eBitset)
	RETURN IS_BIT_SET(iLocalDebugBitset, ENUM_TO_INT(eBitset))
ENDFUNC

PROC SET_LOCAL_DEBUG_BIT0(eLOCAL_DEBUG_BITSET_0 eBitset, BOOL bBlockLogPrints = FALSE)

	IF NOT bBlockLogPrints
		IF NOT IS_BIT_SET(iLocalDebugBitset, ENUM_TO_INT(eBitset))
			STRING strTemp = GET_LOCAL_DEBUG_BIT0_NAME(eBitset)
			PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] SET_LOCAL_DEBUG_BIT0 - ", strTemp)
		ENDIF
	ENDIF
	
	SET_BIT(iLocalDebugBitset, ENUM_TO_INT(eBitset))
	
ENDPROC

PROC CLEAR_LOCAL_DEBUG_BIT0(eLOCAL_DEBUG_BITSET_0 eBitset, BOOL bBlockLogPrints = FALSE)

	IF NOT bBlockLogPrints
		IF IS_BIT_SET(iLocalDebugBitset, ENUM_TO_INT(eBitset))
			STRING strTemp = GET_LOCAL_BIT0_NAME(eBitset)
			PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] CLEAR_LOCAL_DEBUG_BIT0 - ", strTemp)
		ENDIF
	ENDIF
	
	CLEAR_BIT(iLocalDebugBitset, ENUM_TO_INT(eBitset))
	
ENDPROC

#ENDIF

// ------------------------------------------------------------------|<

// Game State ------------------------------------------------------->|

FUNC STRING GET_GAME_STATE_NAME(INT iState)

	SWITCH(iState)
		CASE GAME_STATE_INIT 				RETURN "GAME_STATE_INIT"
		CASE GAME_STATE_RUNNING 			RETURN "GAME_STATE_RUNNING"
		CASE GAME_STATE_TERMINATE_DELAY 	RETURN "GAME_STATE_TERMINATE_DELAY"
		CASE GAME_STATE_END 				RETURN "GAME_STATE_END"
	ENDSWITCH

	RETURN "***INVALID***"

ENDFUNC

//Helper function to get a clients game/mission state
FUNC INT GET_CLIENT_GAME_STATE(INT iPlayer)
	IF iPlayer <> -1
		RETURN playerBD[iPlayer].iClientGameState
	ENDIF
	
	RETURN GAME_STATE_INIT
ENDFUNC

//Helper function to get the servers game/mission state
FUNC INT GET_SERVER_GAME_STATE()
	RETURN serverBD.iServerGameState
ENDFUNC

//Helper function to set a clients game/mission state
PROC SET_CLIENT_GAME_STATE(INT iPlayer, INT iState)
	playerBD[iPlayer].iClientGameState = iState
	PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] SET_CLIENT_GAME_STATE = ",GET_GAME_STATE_NAME(iState))
ENDPROC

//Helper function to set the servers game/mission state
PROC SET_SERVER_GAME_STATE(INT iState)
	serverBD.iServerGameState = iState
	PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] SET_SERVER_GAME_STATE = ",GET_GAME_STATE_NAME(iState))
ENDPROC

// ------------------------------------------------------------------|<

// Mode State ------------------------------------------------------->|

#IF IS_DEBUG_BUILD 
FUNC STRING GET_MODE_STATE_NAME(eMODE_STATE eState)
	
	SWITCH eState
		CASE eMODESTATE_INIT	RETURN "eMODESTATE_INIT"
		CASE eMODESTATE_RUN		RETURN "eMODESTATE_RUN"
		CASE eMODESTATE_REWARDS	RETURN "eMODESTATE_REWARDS"
		CASE eMODESTATE_END		RETURN "eMODESTATE_END"
	ENDSWITCH
	
	RETURN "***INVALID***"
	
ENDFUNC
#ENDIF

FUNC eMODE_STATE GET_MODE_STATE()
	RETURN serverBD.eModeState
ENDFUNC

PROC SET_MODE_STATE(eMODE_STATE eState)
	
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
	
		#IF IS_DEBUG_BUILD
		TEXT_LABEL_63 errorMsg = "[EXEC1] [GB CASHING OUT] [ST EXEC] - Client set mode state. Client = "
		errorMsg += PARTICIPANT_ID_TO_INT()
		SCRIPT_ASSERT(errorMsg)
		#ENDIF
		
		EXIT
		
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF eState != GET_MODE_STATE()
		STRING strStateName = GET_MODe_STATE_NAME(eState)
		PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - mode state going to ", strStateName)
		DEBUG_PRINTCALLSTACK()
	ENDIF
	#ENDIF
	
	serverBD.eModeState = eState	
	
ENDPROC

// ------------------------------------------------------------------|<

// End Reason ------------------------------------------------------->|

#IF IS_DEBUG_BUILD 
FUNC STRING GET_END_REASON_NAME(eEND_REASON eEndReason)
	
	SWITCH eEndReason
		CASE eENDREASON_NO_REASON_YET						RETURN "eENDREASON_NO_REASON_YET"
		CASE eENDREASON_TIME_UP								RETURN "TIME_UP"
		CASE eENDREASON_NO_BOSS_LEFT						RETURN "NO_BOSS_LEFT"
		CASE eENDREASON_ALL_GOONS_LEFT						RETURN "ALL_GOONS_LEFT"
		CASE eENDREASON_WIN_CONDITION_TRIGGERED				RETURN "eENDREASON_WIN_CONDITION_TRIGGERED"
	ENDSWITCH
	
	RETURN "***INVALID***"
	
ENDFUNC
#ENDIF

FUNC eEND_REASON GET_END_REASON()
	RETURN serverBD.eEndReason
ENDFUNC

PROC SET_END_REASON(eEND_REASON eEndReason)
	
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
	
		#IF IS_DEBUG_BUILD
		TEXT_LABEL_63 errorMsg = "[EXEC1] [GB CASHING OUT] [ST EXEC] - Client set end reason. Client = "
		errorMsg += PARTICIPANT_ID_TO_INT()
		SCRIPT_ASSERT(errorMsg)
		#ENDIF
		
		EXIT
		
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF eEndReason != GET_END_REASON()
		STRING strReason = GET_END_REASON_NAME(eEndReason)
		PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - set end reason to ", strReason)
		DEBUG_PRINTCALLSTACK()
	ENDIF
	#ENDIF
	
	serverBD.eEndReason = eEndReason	
	
ENDPROC

// ------------------------------------------------------------------|<

// Mission Stage ---------------------------------------------------->|
#IF IS_DEBUG_BUILD
FUNC STRING GET_MISSION_STAGE_NAME(MISSION_STAGE_ENUM eStage)
	SWITCH eStage
		CASE eMS_INIT			RETURN "eMS_INIT"
		CASE eMS_COLLECT		RETURN "eMS_COLLECT"
	ENDSWITCH
	
	RETURN "***INVALID MISSION STAGE***"
ENDFUNC
#ENDIF

FUNC MISSION_STAGE_ENUM GET_CLIENT_MISSION_STAGE(INT iPlayer)
	RETURN playerBD[iPlayer].eClientStage
ENDFUNC

PROC SET_CLIENT_MISSION_STAGE(MISSION_STAGE_ENUM newStage)
	PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - SET_CLIENT_MISSION_STAGE - Client setting mission stage to ", GET_MISSION_STAGE_NAME(newStage))
	playerBD[PARTICIPANT_ID_TO_INT()].eClientStage = newStage
ENDPROC

// ------------------------------------------------------------------|<

// Telemetry -------------------------------------------------------->|

FUNC INT GET_END_REASON_AS_TELEMTRY_TYPE(eEND_REASON eEndReason, BOOL bIWon, BOOL bNobodyWon, BOOL bForcedEndOfMode)
	
	IF bForcedEndOfMode
		PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - GET_END_REASON_AS_TELEMTRY_TYPE - bExternallyForcedEndOfMode = TRUE, returning GB_TELEMETRY_END_FORCED")
		RETURN GB_TELEMETRY_END_FORCED
	ENDIF
	
	SWITCH eEndReason
		
		CASE eENDREASON_NO_BOSS_LEFT
			PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - [TELEMETRY] - GET_END_REASON_AS_TELEMTRY_TYPE - eEndReason = eENDREASON_NO_BOSS_LEFT, returning GB_TELEMETRY_END_BOSS_LEFT")
			RETURN GB_TELEMETRY_END_BOSS_LEFT	
		
		CASE eENDREASON_ALL_GOONS_LEFT	
			PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - [TELEMETRY] - GET_END_REASON_AS_TELEMTRY_TYPE - eEndReason = eENDREASON_ALL_GOONS_LEFT, returning GB_TELEMETRY_END_LOW_NUMBERS")
			RETURN GB_TELEMETRY_END_LOW_NUMBERS
		
		CASE eENDREASON_TIME_UP
		CASE eENDREASON_WIN_CONDITION_TRIGGERED
			IF bIWon	
				PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - [TELEMETRY] - GET_END_REASON_AS_TELEMTRY_TYPE - eEndReason = eENDREASON_TIME_UP and bIWon = TRUE, returning GB_TELEMETRY_END_WON")
				RETURN GB_TELEMETRY_END_WON		
			ELIF bNobodyWon
				PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - [TELEMETRY] - GET_END_REASON_AS_TELEMTRY_TYPE - eEndReason = eENDREASON_TIME_UP and bNobodyWon = TRUE, returning GB_TELEMETRY_END_TIME_OUT")
				RETURN GB_TELEMETRY_END_TIME_OUT	
			ELSE
				PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - [TELEMETRY] - GET_END_REASON_AS_TELEMTRY_TYPE - eEndReason = eENDREASON_TIME_UP and bWon = FALSE and bNobodyWon = FALSE, returning GB_TELEMETRY_END_LOST")
				RETURN GB_TELEMETRY_END_LOST
			ENDIF
		BREAK
		
	ENDSWITCH
	
	PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - [TELEMETRY] - GET_END_REASON_AS_TELEMTRY_TYPE - all other telemetry type checks failed, must be leaving of own choice, returning GB_TELEMETRY_END_LEFT")
	RETURN GB_TELEMETRY_END_LEFT
	
ENDFUNC

PROC EXECUTE_COMMON_END_TELEMTRY(BOOL bIwon, BOOL bForcedEndOfMode)
	
	IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_EXECUTED_END_TELEMTRY)
		
		BOOL bNobodyWon // Set based on mode specific logic.
		INT iEndType = GET_END_REASON_AS_TELEMTRY_TYPE(GET_END_REASON(), bIWon, bNobodyWon, bForcedEndOfMode)
		
		PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - [TELEMETRY] - EXECUTE_COMMON_END_TELEMTRY - bForcedEndOfMode = ", bForcedEndOfMode)
		PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - [TELEMETRY] - EXECUTE_COMMON_END_TELEMTRY - bIWon = ", bIWon)
		PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - [TELEMETRY] - EXECUTE_COMMON_END_TELEMTRY - bNobodyWon = ", bNobodyWon)
		PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - [TELEMETRY] - EXECUTE_COMMON_END_TELEMTRY - iEndType = ", iEndType)
		
		GB_SET_COMMON_TELEMETRY_DATA_ON_END(bIWon, iEndType)
		PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - [TELEMETRY] - EXECUTE_COMMON_END_TELEMTRY - called GB_SET_COMMON_TELEMETRY_DATA_ON_END(", bIWon,", ", iEndType, ")")
		
		SET_LOCAL_BIT0(eLOCALBITSET0_EXECUTED_END_TELEMTRY)
		
	ENDIF
	
ENDPROC

// ------------------------------------------------------------------|<

FUNC BOOL IS_MINIGAME_DISABLED(INT iMinigame)
	SWITCH iMinigame
		CASE 0 RETURN g_sMPTunables.bexec_cashing_disable_numbers
		CASE 1 RETURN g_sMPTunables.bexec_cashing_disable_bruteforce
		CASE 2 RETURN g_sMPTunables.bexec_cashing_disable_datacrack
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_NEXT_AVAILABLE_MINIGAME(INT iCurrentGame)
	IF iCurrentGame = HACKING_IP_CONNECT
		IF g_sMPTunables.bexec_cashing_disable_bruteforce
			IF g_sMPTunables.bexec_cashing_disable_datacrack
				PRINTLN("[cashing out tuneable checks] - Previous Game: HACKING_IP_CONNECT - Both brute force and data crack disabled, returning IP connect")
				RETURN HACKING_IP_CONNECT
			ELSE
				PRINTLN("[cashing out tuneable checks] - Previous Game: HACKING_IP_CONNECT - Brute force disabled, returning data crack")
				RETURN HACKING_NEW_MG
			ENDIF
		ELSE
			PRINTLN("[cashing out tuneable checks] - Previous Game: HACKING_IP_CONNECT - Returning brute force as it is not disabled")
			RETURN HACKING_BRUTEFORCE
		ENDIF
	ELIF iCurrentGame = HACKING_BRUTEFORCE
		IF g_sMPTunables.bexec_cashing_disable_datacrack
			IF g_sMPTunables.bexec_cashing_disable_numbers
				PRINTLN("[cashing out tuneable checks] - Previous Game: HACKING_BRUTEFORCE - Both data crack and ip connect disabled, returning brute force")
				RETURN HACKING_BRUTEFORCE
			ELSE
				PRINTLN("[cashing out tuneable checks] - Previous Game: HACKING_BRUTEFORCE - data crack disabled, returning ip connect")
				RETURN HACKING_IP_CONNECT
			ENDIF
		ELSE
			PRINTLN("[cashing out tuneable checks] - Previous Game: HACKING_BRUTEFORCE - Returning data crack as it is not disabled")
			RETURN HACKING_NEW_MG
		ENDIF
	ELIF iCurrentGame = HACKING_NEW_MG
		IF g_sMPTunables.bexec_cashing_disable_numbers
			IF g_sMPTunables.bexec_cashing_disable_bruteforce
				PRINTLN("[cashing out tuneable checks] - Previous Game: HACKING_NEW_MG - Both ip connect and brute force disabled, returning data crack")
				RETURN HACKING_NEW_MG
			ELSE
				PRINTLN("[cashing out tuneable checks] - Previous Game: HACKING_NEW_MG - Ip connect disabled, returning brute force")
				RETURN HACKING_BRUTEFORCE
			ENDIF
		ELSE
			PRINTLN("[cashing out tuneable checks] - Previous Game: HACKING_NEW_MG - Returning IP connect as it is not disabled")
			RETURN HACKING_IP_CONNECT
		ENDIF
	ENDIF
	
	PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - GET_NEXT_AVAILABLE_MINIGAME - DEFAULT - returning IP CONNECT")
	RETURN HACKING_IP_CONNECT
ENDFUNC

///
///    HELPER FUNCTIONS
///    

// Tunables --------------------------------------------------------->|
/// PURPOSE:
///    Minimum amount of cash that can be stolen from an ATM
/// RETURNS:
///    
FUNC INT CASHING_OUT_TUNABLE_GET_MIN_ATM_CASH()
	RETURN g_sMPTunables.iexec_cashing_hack_reward_lower_limit
ENDFUNC

/// PURPOSE:
///    Max amount of cash that can be stolen from an ATM
/// RETURNS:
///    
FUNC INT CASHING_OUT_TUNABLE_GET_MAX_ATM_CASH()
	RETURN g_sMPTunables.iexec_cashing_hack_reward_upper_limit
ENDFUNC

/// PURPOSE:
///    Duration of the challenge
/// RETURNS:
///    
FUNC INT CASHING_OUT_TUNABLE_GET_CHALLENGE_DURATION()
	RETURN  g_sMPTunables.iexec_cashing_time_limit
ENDFUNC

/// PURPOSE:
///    Amount of time in a minigame before failing (not sure if we want/need this, need to confirm with Butch)
/// RETURNS:
///    
FUNC INT CASHING_OUT_TUNABLE_GET_MINIGAME_TIME()
	RETURN g_sMPTunables.iexec_cashing_hack_timer
ENDFUNC

/// PURPOSE:
///    Number of lives in minigame to lose (not sure if this works, may need to speak to Adam)
/// RETURNS:
///    
FUNC INT CASHING_OUT_TUNABLE_GET_MINIGAME_LIVES()
	RETURN 2
ENDFUNC

/// PURPOSE:
///    Max wanted level given from robbing ATMs
/// RETURNS:
///    
FUNC INT CASHING_OUT_TUNABLE_MAX_WANTED_LEVEL_GIVEN()
	RETURN g_sMPTunables.iexec_cashing_wanted_fail
ENDFUNC

/// PURPOSE:
///    How much cash the player drops when they are killed
/// RETURNS:
///    
FUNC FLOAT CASHING_OUT_TUNABLE_GET_CASH_DROP_PERCENTAGE()
	RETURN g_sMPTunables.fexec_cashing_cash_dropped
ENDFUNC

/// PURPOSE:
///    Maximum amount of cash that a player can win through combined total
/// RETURNS:
///    
FUNC INT CASHING_OUT_TUNABLE_GET_MAX_COMBINED_TOTAL()
	RETURN g_sMPTunables.iexec_cashing_max_combined_total
ENDFUNC

/// PURPOSE:
///    The minimum amount of time that needs to pass before a player can hack the next atm
///    (small for now to ensure players dont immediately see prompt when finishing a hack right next to
///    another ATM)
/// RETURNS:
///    
FUNC INT CASHING_OUT_TUNABLE_TIME_BETWEEN_HACKS()
	RETURN g_sMPTunables.iexec_cashing_time_between_hacks
ENDFUNC

FUNC BOOL CASHING_OUT_TUNABLE_DATA_CRACK_DISABLED()
	RETURN g_sMPTunables.bexec_cashing_disable_datacrack
ENDFUNC

FUNC BOOL CASHING_OUT_TUNABLE_BRUTE_FORCE_DISABLED()
	RETURN g_sMPTunables.bexec_cashing_disable_bruteforce
ENDFUNC

FUNC BOOL CASHING_OUT_IP_CONNECT_DISABLED()
	RETURN g_sMPTunables.bexec_cashing_disable_numbers
ENDFUNC

FUNC INT CASHING_OUT_WANTED_LEVEL_COMPLETION()
	RETURN g_sMPTunables.iexec_cashing_wanted_complete
ENDFUNC

FUNC FLOAT CASHING_OUT_TUNABLE_UNDER_10S_CHANCE()
	RETURN g_sMPTunables.fexec_cashing_wanted_chance_under10s
ENDFUNC

FUNC FLOAT CASHING_OUT_TUNABLE_10S_TO_20S_CHANCE()
	RETURN g_sMPTunables.fexec_cashing_wanted_chance_10to20s
ENDFUNC

FUNC FLOAT CASHING_OUT_TUNABLE_20S_TO_30S_CHANCE()
	RETURN g_sMPTunables.fexec_cashing_wanted_chance_20to30s
ENDFUNC

FUNC FLOAT CASHING_OUT_TUNABLE_30S_TO_40S_CHANCE()
	RETURN g_sMPTunables.fexec_cashing_wanted_chance_30to40s
ENDFUNC

FUNC FLOAT CASHING_OUT_TUNABLE_40S_TO_50S_CHANCE()
	RETURN g_sMPTunables.fexec_cashing_wanted_chance_40to50s
ENDFUNC

FUNC FLOAT CASHING_OUT_TUNABLE_50S_TO_59S_CHANCE()
	RETURN g_sMPTunables.fexec_cashing_wanted_chance_50to59s
ENDFUNC

FUNC FLOAT CASHING_OUT_TUNABLE_TIME_EXPIRE_CHANCE()
	RETURN g_sMPTunables.fexec_cashing_wanted_chance_timerexpire
ENDFUNC
// ------------------------------------------------------------------|<

// UI Checks -------------------------------------------------------->|
FUNC BOOL SHOULD_UI_BE_HIDDEN()
	IF GB_SHOULD_HIDE_GANG_BOSS_EVENT(FMMC_TYPE_GB_CASHING_OUT)
		PRINTLN("[GB CASHING OUT SPAM] - SHOULD_UI_BE_HIDDEN - Yes - GB_SHOULD_HIDE_GANG_BOSS_EVENT(FMMC_TYPE_GB_CASHING_OUT)")
		RETURN TRUE
	ENDIF
	
	IF GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE()
		PRINTLN("[GB CASHING OUT SPAM] - SHOULD_UI_BE_HIDDEN - Yes - GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE()")
		RETURN TRUE
	ENDIF
	
	IF NOT MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI
		PRINTLN("[GB CASHING OUT SPAM] - SHOULD_UI_BE_HIDDEN - Yes - NOT MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

// ------------------------------------------------------------------|<

// Gang info -------------------------------------------------------->|

FUNC PLAYER_INDEX GET_GANG_BOSS_PLAYER_INDEX()
	RETURN serverBD.piGangBoss
ENDFUNC

FUNC BOOL AM_I_THE_GANG_BOSS()
	RETURN (PLAYER_ID() = GET_GANG_BOSS_PLAYER_INDEX())
ENDFUNC

FUNC BOOL IS_THIS_PLAYER_THE_GANG_BOSS(PLAYER_INDEX thisPlayer)
	RETURN (thisPlayer = GET_GANG_BOSS_PLAYER_INDEX())
ENDFUNC

FUNC BOOL AM_I_A_GANG_GOON()
	RETURN GB_IS_PLAYER_MEMBER_OF_THIS_GANG(PLAYER_ID(), GET_GANG_BOSS_PLAYER_INDEX(), FALSE)
ENDFUNC

FUNC BOOL IS_THIS_PLAYER_A_GANG_GOON(PLAYER_INDEX thisPlayer)
	RETURN GB_IS_PLAYER_MEMBER_OF_THIS_GANG(thisPlayer, GET_GANG_BOSS_PLAYER_INDEX(), FALSE)
ENDFUNC

FUNC BOOL IS_THIS_PLAYER_IN_THE_GANG(PLAYER_INDEX thisPlayer)
	IF IS_THIS_PLAYER_A_GANG_GOON(thisPlayer)
	OR IS_THIS_PLAYER_THE_GANG_BOSS(thisPlayer)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAVE_I_HACKED_AT_LEAST_ONE_ATM()
	RETURN IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), eCLIENTBITSET0_HACKED_AN_ATM)
ENDFUNC

FUNC BOOL IS_PLAYER_HACKING_ATM(PARTICIPANT_INDEX thisPart)
	IF thisPart != INVALID_PARTICIPANT_INDEX()
		RETURN IS_CLIENT_BIT0_SET(thisPart, eCLIENTBITSET0_HACKING_ATM)
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC PLAYER_INDEX GET_WINNER()
	RETURN serverBD.piWinner
ENDFUNC

FUNC INT GET_PLAYER_CASH_COLLECTED(INT thisPlayer)
	IF thisPlayer > -1 AND thisPlayer < GB_MAX_GANG_SIZE_INCLUDING_BOSS_EXEC
		RETURN playerBD[thisPlayer].iCashCollected
	ENDIF
	
	SCRIPT_ASSERT("[EXEC1] [GB CASHING OUT] [ST EXEC] - GET_PLAYER_CASH_COLLECTED - INVALID PLAYER PASSED - thisPlayer < 0 OR >= 4")
	RETURN 0
ENDFUNC

FUNC BOOL HAS_PLAYER_FULLY_SPAWNED()
	IF IS_SCREEN_FADED_IN()
	AND GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iRespawnState = RESPAWN_STATE_PLAYING
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL AM_I_SPECTATING_THE_GANG()
	RETURN IS_LOCAL_BIT0_SET(eLOCALBITSET0_SPECTATING_GANG)
ENDFUNC

FUNC BOOL HAS_SPECTATOR_FOCUS_PLAYER_HACKED_AT_LEAST_ONE_ATM()
	IF iFocusParticipant != -1
		IF IS_CLIENT_BIT0_SET(INT_TO_PARTICIPANTINDEX(iFocusParticipant), eCLIENTBITSET0_HACKED_AN_ATM)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_SPECTATOR_FOCUS_PLAYER_NEAR_AN_ATM()
	IF iFocusParticipant != -1
		RETURN IS_CLIENT_BIT0_SET(INT_TO_PARTICIPANTINDEX(iFocusParticipant), eCLIENTBITSET0_NEAR_AN_ATM)
	ENDIF
	
	RETURN FALSE
ENDFUNC

// ------------------------------------------------------------------|<

// ATM -------------------------------------------------------------->|

FUNC BOOL IS_ATM_BIT_SET(INT iatmbit)

	IF iatmbit = -1
		RETURN FALSE
	ENDIF

	INT iBitSet = (iatmbit / 32)
	INT iBitVal = (iatmbit % 32)
	
	IF iBitSet = 0
		RETURN IS_BIT_SET(serverBD.iATMBitSet0, iBitVal)
	ELIF iBitSet = 1
		RETURN IS_BIT_SET(serverBD.iATMBitSet1, iBitVal)
	ELIF iBitSet = 2
		RETURN IS_BIT_SET(serverBD.iATMBitSet2, iBitVal)
	ELSE
		RETURN IS_BIT_SET(serverBD.iATMBitSet3, iBitVal)
	ENDIF
	
ENDFUNC

PROC SET_ATM_BIT(INT iatmbit)

	IF iatmbit = -1
		EXIT
	ENDIF

	INT iBitSet = (iatmbit / 32)
	INT iBitVal = (iatmbit % 32)
	
	IF iBitSet = 0
		SET_BIT(serverBD.iATMBitSet0, iBitVal)
	ELIF iBitSet = 1
		SET_BIT(serverBD.iATMBitSet1, iBitVal)
	ELIF iBitSet = 2
		SET_BIT(serverBD.iATMBitSet2, iBitVal)
	ELSE
		SET_BIT(serverBD.iATMBitSet3, iBitVal)
	ENDIF
ENDPROC

PROC CLEAR_ATM_BIT(INT iatmbit)

	IF iatmbit = -1
		EXIT
	ENDIF

	INT iBitSet = (iatmbit / 32)
	INT iBitVal = (iatmbit % 32)
	
	IF iBitSet = 0
		CLEAR_BIT(serverBD.iATMBitSet0, iBitVal)
	ELIF iBitSet = 1
		CLEAR_BIT(serverBD.iATMBitSet1, iBitVal)
	ELIF iBitSet = 2
		CLEAR_BIT(serverBD.iATMBitSet2, iBitVal)
	ELSE
		CLEAR_BIT(serverBD.iATMBitSet3, iBitVal)
	ENDIF
ENDPROC

FUNC VECTOR GET_ATM_COORDS(int index)

	SWITCH index
		
		// Top Left (neg x, pos y)
		CASE 0  RETURN <<-165.5844,	234.7659,	93.92897>>
		CASE 1  RETURN <<-165.5844,	232.6955,	93.92897>>
		CASE 2  RETURN <<-386.4596,	6046.411,	30.47399>>
		CASE 3  RETURN <<-282.7141,	6226.43,	30.49648>>
		CASE 4  RETURN <<-132.6663,	6366.876,	30.47258>>
		CASE 5  RETURN <<-95.87029,	6457.462,	30.47394>>
		CASE 6  RETURN <<-97.63721,	6455.732,	30.46793>>
		CASE 7  RETURN <<-2956.848,	487.2158,	14.478>>
		CASE 8  RETURN <<-2958.977,	487.3071,	14.478>>
		CASE 9  RETURN <<-2974.586,	380.1269,	14.32212>>
		CASE 10 RETURN <<-1091.887,	2709.053,	17.91941>>
		CASE 11 RETURN <<-2295.853,	357.9348,	173.6014>>
		CASE 12 RETURN <<-2295.069,	356.2556,	173.6014>>
		CASE 13 RETURN <<-2294.3,	354.6056,	173.6014>>
		CASE 14 RETURN <<-3043.835,	594.1639,	6.732796>>
		CASE 15 RETURN <<-3144.887,	1127.811,	19.83804>>
		CASE 16 RETURN <<-3241.455,	997.9085,	11.66582>>
		CASE 17 RETURN <<-3240.5955, 1008.8966, 11.8307>>
		CASE 18 RETURN <<-3040.7744, 593.1677, 6.9089>>
		CASE 19 RETURN <<-1827.3083, 785.1384, 137.3020>>
		
		// Top Right (pos x, pos y)
		CASE 20 RETURN <<158.7965,	234.7452,	105.6433>>
		CASE 21 RETURN <<228.0324,	337.8501,	104.5013>>
		CASE 22 RETURN <<285.3485,	142.9751,	103.1623>>
		CASE 23 RETURN <<357.1284,	174.0836,	102.0597>>
		CASE 24 RETURN <<89.81339,	2.880329,	67.35216>>
		CASE 25 RETURN <<156.1886,	6643.2,		30.59372>>
		CASE 26 RETURN <<173.8246,	6638.217,	30.59372>>
		CASE 27 RETURN <<1687.395,	4815.9	,	41.00647>>
		CASE 28 RETURN <<1700.694,	6426.762,	31.63297>>
		CASE 29 RETURN <<1822.971,	3682.577,	33.26745>>
		CASE 30 RETURN <<1171.523,	2703.139,	37.1477>>
		CASE 31 RETURN <<1172.457,	2703.139,	37.1477>>
		CASE 32 RETURN <<2564,		2584.553,	37.06807>>
		CASE 33 RETURN <<2558.324,	350.988,	107.5975>>
		CASE 34 RETURN <<1735.4027, 6410.5439, 34.0372>>
		CASE 35 RETURN <<1702.9856, 4933.4932, 41.0637>>
		CASE 36 RETURN <<1967.9336, 3743.5725, 31.3438>>
		CASE 37 RETURN <<2682.9255, 3286.4329, 54.2411>>
		CASE 38 RETURN <<540.4974, 2671.0779, 41.1565>>
		CASE 39 RETURN <<2558.4229, 389.8128, 107.6230>>
		CASE 40 RETURN <<380.9930, 323.4247, 102.5664>>
		
		// Bottom Left (neg x, neg y)
		CASE 41 RETURN <<-301.6573,	-829.5886,	31.41977>>
		CASE 42 RETURN <<-303.2257,	-829.3121,	31.41977>>
		CASE 43 RETURN <<-204.0193,	-861.0091,	29.27133>>
		CASE 44 RETURN <<-256.6386,	-715.8899,	32.7883>>
		CASE 45 RETURN <<-259.2767,	-723.2651,	32.70155>>
		CASE 46 RETURN <<-254.5219,	-692.8869,	32.57825>>
		CASE 47 RETURN <<-27.89034,	-724.1089,	43.22287>>
		CASE 48 RETURN <<-30.09956,	-723.2863,	43.22287>>
		CASE 49 RETURN <<-57.17029,	-92.37918,	56.75069>>
		CASE 50 RETURN <<-1205.378,	-326.5286,	36.85104>>
		CASE 51 RETURN <<-1206.142,	-325.0316,	36.85104>>
		CASE 52 RETURN <<-846.6537,	-341.509,	37.6685>>
		CASE 53 RETURN <<-847.204,	-340.4291,	37.6793>>
		CASE 54 RETURN <<-720.6288,	-415.5243,	33.97996>>
		CASE 55 RETURN <<-867.013,	-187.9928,	36.88218>>
		CASE 56 RETURN <<-867.9745,	-186.3419,	36.88218>>
		CASE 57 RETURN <<-1415.48,	-212.3324,	45.49542>>
		CASE 58 RETURN <<-1430.663,	-211.3587,	45.47162>>
		CASE 59 RETURN <<-1410.736,	-98.92789,	51.39701>>
		CASE 60 RETURN <<-1410.183,	-100.6454,	51.39652>>
		CASE 61 RETURN <<-1282.098,	-210.5599,	41.43031>>
		CASE 62 RETURN <<-1286.704,	-213.7827,	41.43031>>
		CASE 63 RETURN <<-1289.742,	-227.165,	41.43031>>
		CASE 64 RETURN <<-1285.136,	-223.9422,	41.43031>>
		CASE 65 RETURN <<-1569.84,	-547.0309,	33.93216>>
		CASE 66 RETURN <<-1570.765,	-547.7035,	33.93216>>
		CASE 67 RETURN <<-1305.708,	-706.6881,	24.31447>>
		CASE 68 RETURN <<-1315.416,	-834.431,	15.95233>>
		CASE 69 RETURN <<-1314.466,	-835.6913,	15.95233>>
		CASE 70 RETURN <<-2071.928,	-317.2862,	12.31808>>
		CASE 71 RETURN <<-821.8936,	-1081.555,	10.13664>>
		CASE 72 RETURN <<-1110.228,	-1691.154,	3.378483>>
		CASE 73 RETURN <<-712.9357,	-818.4827,	22.74066>>
		CASE 74 RETURN <<-710.0828,	-818.4756,	22.73634>>
		CASE 75 RETURN <<-617.8035,	-708.8591,	29.04321>>
		CASE 76 RETURN <<-617.8035,	-706.8521,	29.04321>>
		CASE 77 RETURN <<-614.5187,	-705.5981,	30.224>>
		CASE 78 RETURN <<-611.8581,	-705.5981,	30.224>>
		CASE 79 RETURN <<-660.6763,	-854.4882,	23.45663>>
		CASE 80 RETURN <<-537.8052,	-854.9357,	28.27543>>
		CASE 81 RETURN <<-594.6144,	-1160.852,	21.33351>>
		CASE 82 RETURN <<-596.1251,	-1160.85,	21.3336>>
		CASE 83 RETURN <<-526.7791,	-1223.374,	17.45272>>
		CASE 84 RETURN <<-56.6565, -1751.8821, 28.4210>>
		CASE 85 RETURN <<-717.5778, -915.8334, 18.2156>>
		
		// Bottom Right (pos x, neg y)
		CASE 86 RETURN <<118.6416,	-883.5695,	30.13945>>
		CASE 87 RETURN <<24.5933,	-945.543,	28.33305>>
		CASE 88 RETURN <<5.686035,	-919.9551,	28.48088>>
		CASE 89 RETURN <<296.1756,	-896.2318,	28.29015>>
		CASE 90 RETURN <<296.8775,	-894.3196,	28.26148>>
		CASE 91 RETURN <<147.4731,	-1036.218,	28.38707>>
		CASE 92 RETURN <<145.8392,	-1035.625,	28.38707>>
		CASE 93 RETURN <<112.4762,	-819.8081,	30.33955>>
		CASE 94 RETURN <<111.3886,	-774.8402,	30.43766>>
		CASE 95 RETURN <<114.5474,	-775.972,	30.41737>>
		CASE 96 RETURN <<289.53,	-1256.788,	28.44057>>
		CASE 97 RETURN <<289.2679,	-1282.32,	28.65519>>
		CASE 98 RETURN <<527.7776,	-160.6609,	56.13671>>
		CASE 99 RETURN <<1077.779,	-776.9664,	57.25652>>
		CASE 100 RETURN <<1137.811,	-468.8625,	65.69865>>
		CASE 101 RETURN <<1167.06,	-455.6541,	65.81857>>
		CASE 102 RETURN <<129.54539, -1290.94666, 29.18351>>
		CASE 103 RETURN <<129.97639, -1291.67761, 29.18987>>
		CASE 104 RETURN <<130.40971, -1292.40979, 29.21587>>
		CASE 105 RETURN <<1153.6635, -326.7430, 68.2051>>
		CASE 106 RETURN <<32.9772, -1348.1593, 28.4970>>
	
	ENDSWITCH
	
	RETURN <<0.0, 0.0, 0.0>>

ENDFUNC

/// PURPOSE:
///    Gets which quadrant of the map the player is currently in
/// RETURNS:
///    
FUNC MAP_QUADRANT_ENUM GET_LOCAL_PLAYER_CURRENT_MAP_QUADRANT()

	IF IS_NET_PLAYER_OK(PLAYER_ID())
		VECTOR localCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
		
		IF ((localCoords.x < 0) AND (localCoords.y > 0))
			PRINTLN("atm cash spam - player is in top left ")
			RETURN eMQ_TOP_LEFT
		ELIF ((localCoords.x > 0) AND (localCoords.y > 0))
			PRINTLN("atm cash spam - player is in top right ")
			RETURN eMQ_TOP_RIGHT
		ELIF ((localCoords.x < 0) AND (localCoords.y < 0))
			PRINTLN("atm cash spam - player is in bottom left ")
			RETURN eMQ_BOTTOM_LEFT
		ELIF ((localCoords.x > 0) AND (localCoords.y < 0))
			PRINTLN("atm cash spam - player is in bottom right ")
			RETURN eMQ_BOTTOM_RIGHT
		ENDIF
	ENDIF

	RETURN eMQ_INVALID

ENDFUNC

/// PURPOSE:
///    Gets the world coords of the closest atm to the player, based on
///    which quadrant of the map they are in
/// PARAMS:
///    atmIndex - 
/// RETURNS:
///    
FUNC VECTOR GET_CLOSEST_ATM_COORDS(INT &atmIndex)
	FLOAT fNewDistance
	FLOAT fShortestDistance = 9999.9
	VECTOR vFromLocation = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
	VECTOR vClosestATM
	INT iLoop
	
	SWITCH GET_LOCAL_PLAYER_CURRENT_MAP_QUADRANT()
		CASE eMQ_INVALID
			vClosestATM = <<0.0, 0.0, 0.0>>
			atmIndex = -1
		BREAK
		
		CASE eMQ_TOP_LEFT
			FOR iLoop = 0 TO NUM_TOP_LEFT_QUAD_ATMs
				fNewDistance = GET_DISTANCE_BETWEEN_COORDS(vFromLocation, GET_ATM_COORDS(iLoop))
				
				IF fNewDistance < fShortestDistance
					fShortestDistance = fNewDistance
					vClosestATM = GET_ATM_COORDS(iLoop)
					atmIndex = iLoop
				ENDIF 
			ENDFOR
		BREAK
		
		CASE eMQ_TOP_RIGHT
			FOR iLoop = 20 TO (20 + NUM_TOP_RIGHT_QUAD_ATMs)
				fNewDistance = GET_DISTANCE_BETWEEN_COORDS(vFromLocation, GET_ATM_COORDS(iLoop))
				
				IF fNewDistance < fShortestDistance
					fShortestDistance = fNewDistance
					vClosestATM = GET_ATM_COORDS(iLoop)
					atmIndex = iLoop
				ENDIF 
			ENDFOR
		BREAK
		
		CASE eMQ_BOTTOM_LEFT
			FOR iLoop = 41 TO (41 + NUM_BOTTOM_LEFT_QUAD_ATMs)
				fNewDistance = GET_DISTANCE_BETWEEN_COORDS(vFromLocation, GET_ATM_COORDS(iLoop))
				
				IF fNewDistance < fShortestDistance
					fShortestDistance = fNewDistance
					vClosestATM = GET_ATM_COORDS(iLoop)
					atmIndex = iLoop
				ENDIF 
			ENDFOR
		BREAK
		
		CASE eMQ_BOTTOM_RIGHT
			FOR iLoop = 86 TO (MAX_NUM_ATMs - 1) // avoid array overrun
				fNewDistance = GET_DISTANCE_BETWEEN_COORDS(vFromLocation, GET_ATM_COORDS(iLoop))
				
				IF fNewDistance < fShortestDistance
					fShortestDistance = fNewDistance
					vClosestATM = GET_ATM_COORDS(iLoop)
					atmIndex = iLoop
				ENDIF 
			ENDFOR
		BREAK
	ENDSWITCH
	
	PRINTLN("atm cash spam - closest atm is number ", atmIndex, " at ", vClosestATM)
	RETURN vClosestATM
	
ENDFUNC

/// PURPOSE:
///    Checks if the ATM has been blocked by someone attempting or succeeding in hacking it
/// PARAMS:
///    index - 
/// RETURNS:
///    
FUNC BOOL IS_ATM_BLOCKED(INT index)
	RETURN IS_ATM_BIT_SET(index)
ENDFUNC

/// PURPOSE:
///    Checks to see if the server bit is set for all ATMs being hacked
/// RETURNS:
///    
FUNC BOOL HAVE_ALL_ATMS_BEEN_ROBBED()
	RETURN IS_SERVER_BIT0_SET(eSERVERBITSET0_ALL_ATMS_ROBBED)
ENDFUNC

FUNC VECTOR GET_CLOSEST_UNBLOCKED_ATM(INT &atmIndex)
	FLOAT fNewDist
	FLOAT fShortestDist = 99999.99
	VECTOR vFromLoc = GET_ENTITY_COORDS(PLAYER_PED_ID())
	VECTOR vClosestATM = <<0.0, 0.0, 0.0>>
	INT iLoop
	
	REPEAT MAX_NUM_ATMs iLoop
		fNewDist = GET_DISTANCE_BETWEEN_COORDS(vFromLoc, GET_ATM_COORDS(iLoop))
		
		IF NOT IS_ATM_BLOCKED(iLoop)
			IF fNewDist < fShortestDist
				fShortestDist = fNewDist
				vClosestATM = GET_ATM_COORDS(iLoop)
				atmIndex = iLoop
			ENDIF
		ENDIF
	ENDREPEAT
	
	PRINTLN("[GB AIRFREIGHT] [ST EXEC] [DEBUG] - GET_CLOSEST_UNBLOCKED_ATM - Closest atm is atm ", atmIndex, " at ", vClosestATM)
	RETURN vClosestATM
ENDFUNC

FUNC BOOL IS_PLAYER_BROWSING_INTERNET()
	RETURN g_bBrowserVisible
ENDFUNC

/// PURPOSE:
///    Checks if its safe for the player to hack the ATM
/// RETURNS:
///    
FUNC BOOL IS_LOCAL_PLAYER_SAFE_TO_HACK()

//	IF NOT IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), eCLIENTBITSET0_HACKING_ATM)
//		IF IS_SCENARIO_OCCUPIED(tVec, 2.0, TRUE)
//			#IF IS_DEBUG_BUILD
//			IF bShowSpammyPrints
//				PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] [DEBUG] - IS_PLAYER_USING_ATM - No - Scenario is occupied by someone else")
//			ENDIF
//			#ENDIF
//			RETURN FALSE
//		ENDIF
//	ENDIF

	IF IS_PLAYER_USING_ATM()
		#IF IS_DEBUG_BUILD
		IF bShowSpammyPrints
			PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] [DEBUG] - IS_PLAYER_USING_ATM - No - Ped is using an atm")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF

	IF IS_PED_USING_ANY_SCENARIO(PLAYER_PED_ID())
		#IF IS_DEBUG_BUILD
		IF bShowSpammyPrints
			PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] [DEBUG] - IS_LOCAL_PLAYER_SAFE_TO_HACK - No - Ped is using a scenario")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_PED_RAGDOLL(PLAYER_PED_ID())
		#IF IS_DEBUG_BUILD
		IF bShowSpammyPrints
			PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] [DEBUG] - IS_LOCAL_PLAYER_SAFE_TO_HACK - No - Ped is ragdoll")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_PHONE_ONSCREEN()
		#IF IS_DEBUG_BUILD
		IF bShowSpammyPrints
			PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] [DEBUG] - IS_LOCAL_PLAYER_SAFE_TO_HACK - No - Phone is on screen")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_PED_IN_COVER(PLAYER_PED_ID())
		#IF IS_DEBUG_BUILD
		IF bShowSpammyPrints
			PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] [DEBUG] - IS_LOCAL_PLAYER_SAFE_TO_HACK - No - Ped is in cover")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF Is_Player_Timetable_Scene_In_Progress()
		#IF IS_DEBUG_BUILD
		IF bShowSpammyPrints
			PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] [DEBUG] - IS_LOCAL_PLAYER_SAFE_TO_HACK - No - Player timetable scene is in progress")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_DRIVING_ANY_VEHICLE()
		#IF IS_DEBUG_BUILD
		IF bShowSpammyPrints
			PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] [DEBUG] - IS_LOCAL_PLAYER_SAFE_TO_HACK - No - player is driving a vehicle")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
//	IF IS_PED_WALKING(PLAYER_PED_ID())
//		#IF IS_DEBUG_BUILD
//		IF bShowSpammyPrints
//			PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] [DEBUG] - IS_LOCAL_PLAYER_SAFE_TO_HACK - No - Player is walking")
//		ENDIF
//		#ENDIF
//		RETURN FALSE
//	ENDIF
	
//	IF GET_ENTITY_SPEED(PLAYER_PED_ID()) > 0.5
//		#IF IS_DEBUG_BUILD
//		IF bShowSpammyPrints
//			PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] [DEBUG] - IS_LOCAL_PLAYER_SAFE_TO_HACK - No - Player is moving faster than 0.5 mph")
//		ENDIF
//		#ENDIF
//		RETURN FALSE
//	ENDIF
	
	IF NOT HAS_PLAYER_FULLY_SPAWNED()
		#IF IS_DEBUG_BUILD
		IF bShowSpammyPrints
			PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] [DEBUG] - IS_LOCAL_PLAYER_SAFE_TO_HACK - No - Player has not fully spawned")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_SPECTATING(PLAYER_ID())
		#IF IS_DEBUG_BUILD
		IF bShowSpammyPrints
			PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] [DEBUG] - IS_LOCAL_PLAYER_SAFE_TO_HACK - No - Player is spectating")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_PED_IN_COVER(PLAYER_PED_ID())
		#IF IS_DEBUG_BUILD
		IF bShowSpammyPrints
			PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] [DEBUG] - IS_LOCAL_PLAYER_SAFE_TO_HACK - No - Ped is in cover")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_BROWSING_INTERNET()
		#IF IS_DEBUG_BUILD
		IF bShowSpammyPrints
			PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] [DEBUG] - IS_LOCAL_PLAYER_SAFE_TO_HACK - No - Player is browsing internet")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	RETURN TRUE		
ENDFUNC

/// PURPOSE:
///    Checks if the player is within 4 meters of the nearest ATM to them
/// RETURNS:
///    
FUNC BOOL IS_PLAYER_NEAR_AN_ATM()
	INT iATMIndex
	VECTOR tempVec

	IF IS_NET_PLAYER_OK(PLAYER_ID())
		tempVec = GET_CLOSEST_ATM_COORDS(iATMIndex)
		IF NOT IS_VECTOR_ZERO(tempVec)
			IF NOT IS_ATM_BLOCKED(iATMIndex)
				IF NOT IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), eCLIENTBITSET0_HACKED_AN_ATM)
				OR HAS_NET_TIMER_EXPIRED(EndOfHackTimer, CASHING_OUT_TUNABLE_TIME_BETWEEN_HACKS())
					IF IS_LOCAL_PLAYER_SAFE_TO_HACK()
						VECTOR vPedCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
						IF VDIST2(vPedCoords, tempVec) < 4
							#IF IS_DEBUG_BUILD
							IF bShowSpammyPrints
							PRINTLN("atm cash spam - player is near an atm ")
							ENDIF
							#ENDIF
							RETURN TRUE
						ELSE
							#IF IS_DEBUG_BUILD
							IF bShowSpammyPrints
							PRINTLN("atm cash spam - player is not close enough to be considered near")
							ENDIF
							#ENDIF
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
						IF bShowSpammyPrints
						PRINTLN("atm cash spam - player is not considered safe to hack")
						ENDIF
						#ENDIF
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
					IF bShowSpammyPrints
					PRINTLN("atm cash spam - waiting for end of hack timer to expire")
					ENDIF
					#ENDIF
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
				IF bShowSpammyPrints
				PRINTLN("atm cash spam - closest atm is blocked ")
				ENDIF
				#ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			IF bShowSpammyPrints
			PRINTLN("atm cash spam - vector is zero ")
			ENDIF
			#ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

// ------------------------------------------------------------------|<

// Help, Shards and Tickers ----------------------------------------->|

FUNC BOOL IS_SAFE_FOR_HELP_TEXT()
	IF  NOT IS_CUSTOM_MENU_ON_SCREEN()
	AND NOT IS_PAUSE_MENU_ACTIVE()
	AND NOT IS_RADAR_HIDDEN()
	AND IS_NET_PLAYER_OK(PLAYER_ID())
	AND NOT IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC DO_HELP_TEXT(HELP_TEXT_ENUM thisHelpText)
	IF SHOULD_UI_BE_HIDDEN()
		
		EXIT
	ENDIF

	IF IS_SAFE_FOR_HELP_TEXT()
		IF NOT IS_LOCAL_HELPTEXT_BIT0_SET(ENUM_TO_INT(thisHelpText))
			SWITCH thisHelpText
				CASE eHELP_INTRO
					PRINT_HELP_NO_SOUND("GB_COUT_HP_00")
					GB_SET_GANG_BOSS_HELP_BACKGROUND()
					SET_LOCAL_HELPTEXT_BIT0(ENUM_TO_INT(thisHelpText))
				BREAK
				
				CASE eHELP_PRESS_RIGHT
					PRINT_HELP_FOREVER_NO_SOUND("GB_COUT_HP_01")
					GB_SET_GANG_BOSS_HELP_BACKGROUND()
					SET_LOCAL_HELPTEXT_BIT0(ENUM_TO_INT(thisHelpText))
				BREAK
				
				CASE eHELP_CASH_DROPPED_WHEN_KILLED
					PRINT_HELP_NO_SOUND("GB_COUT_HP_02")
					GB_SET_GANG_BOSS_HELP_BACKGROUND()
					SET_LOCAL_HELPTEXT_BIT0(ENUM_TO_INT(thisHelpText))
				BREAK
				
				CASE eHELP_WANTED_LEVEL_APPLIED
					PRINT_HELP_NO_SOUND("GB_COUT_HP_03")
					GB_SET_GANG_BOSS_HELP_BACKGROUND()
					SET_LOCAL_HELPTEXT_BIT0(ENUM_TO_INT(thisHelpText))
				BREAK
				
				CASE eHELP_BEATEN_TO_HACK
					PRINT_HELP_NO_SOUND("GB_COUT_HP_04")
					GB_SET_GANG_BOSS_HELP_BACKGROUND()
					SET_LOCAL_HELPTEXT_BIT0(ENUM_TO_INT(thisHelpText))
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks if its safe to draw shard
/// RETURNS:
///    
FUNC BOOL IS_SAFE_FOR_BIG_MESSAGE()	

	IF IS_NET_PLAYER_OK(PLAYER_ID())
	AND NOT IS_PAUSE_MENU_ACTIVE()
	AND NOT IS_CUSTOM_MENU_ON_SCREEN()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

#IF IS_DEBUG_BUILD
/// PURPOSE:
///    Prints information about the big message that has just been requested to draw
/// PARAMS:
///    iBigMessage - the index of the message that was drawn
///    title - the title of the message
///    strapline - the subtitle of the message
PROC PRINT_BIG_MESSAGE_DEBUG_INFORMATION(INT iBigMessage, STRING title, STRING strapline)
	PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - ************ NEW SHARD HAS BEEN SETUP *************")
	PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - PRINT_BIG_MESSAGE_DEBUG_INFORMATION - Big Message:	", iBigMessage)
	PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - PRINT_BIG_MESSAGE_DEBUG_INFORMATION - Title: 			", GET_FILENAME_FOR_AUDIO_CONVERSATION(title))
	PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - PRINT_BIG_MESSAGE_DEBUG_INFORMATION - Strapline:		", GET_FILENAME_FOR_AUDIO_CONVERSATION(strapline))
	PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - ***************************************************")
ENDPROC
#ENDIF

/// PURPOSE:
///    Sets up and draws the passed shard to the screen
/// PARAMS:
///    thisMessage - The enum corresponding to the shard to draw
PROC DO_BIG_MESSAGE(BIG_MESSAGE_ENUM thisMessage)
	IF SHOULD_UI_BE_HIDDEN()
	
		EXIT
	ENDIF
	
	IF IS_SAFE_FOR_BIG_MESSAGE()
		IF NOT IS_LOCAL_BIGMESSAGE_BIT0_SET(ENUM_TO_INT(thisMessage))
			SWITCH thisMessage
				CASE eBIGM_INTRO
					CLEAR_HELP()
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_START_OF_JOB, "GB_COUT_BMT_00", "GB_COUT_BMS_00")
					SET_LOCAL_BIGMESSAGE_BIT0(ENUM_TO_INT(thisMessage))
					
					#IF IS_DEBUG_BUILD	
					PRINT_BIG_MESSAGE_DEBUG_INFORMATION(ENUM_TO_INT(thisMessage), "GB_COUT_BMT_00", "GB_COUT_BMS_00") 
					#ENDIF
				BREAK
				
				CASE eBIGM_YOU_WON
					CLEAR_HELP()
					SETUP_NEW_BIG_MESSAGE_WITH_INT_AND_STRING(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS, 
																GET_PLAYER_CASH_COLLECTED(PARTICIPANT_ID_TO_INT()), 
																"GB_COUT_BMS_01", 
																"GB_WINNER")
					SET_LOCAL_BIGMESSAGE_BIT0(ENUM_TO_INT(thisMessage))
					
					#IF IS_DEBUG_BUILD	
					PRINT_BIG_MESSAGE_DEBUG_INFORMATION(ENUM_TO_INT(thisMessage), "GB_WINNER", "GB_COUT_BMS_01") 
					#ENDIF
				BREAK
				
				CASE eBIGM_YOU_LOST
					IF GET_WINNER() != INVALID_PLAYER_INDEX()
					AND serverBD.iWinningScore > 0
						CLEAR_HELP()
						SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(BIG_MESSAGE_GB_END_OF_JOB_OVER, GET_WINNER(), serverBD.iWinningScore, "GB_COUT_BMS_02", "GB_CHAL_OVER")
						SET_LOCAL_BIGMESSAGE_BIT0(ENUM_TO_INT(thisMessage))
						
						#IF IS_DEBUG_BUILD	
						PRINT_BIG_MESSAGE_DEBUG_INFORMATION(ENUM_TO_INT(thisMessage), "GB_CHAL_OVER", "GB_COUT_BMS_02") 
						#ENDIF
					ENDIF
				BREAK
				
				CASE eBIGM_NO_CASH_STOLEN
					CLEAR_HELP()
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_CHAL_OVER", "GB_COUT_BMS_03")
					SET_LOCAL_BIGMESSAGE_BIT0(ENUM_TO_INT(thisMessage))
					
					#IF IS_DEBUG_BUILD	
					PRINT_BIG_MESSAGE_DEBUG_INFORMATION(ENUM_TO_INT(thisMessage), "GB_CHAL_OVER", "GB_COUT_BMS_03") 
					#ENDIF
				BREAK
				
				CASE eBIGM_BOSS_LEFT
					CLEAR_HELP()
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_CHAL_OVER", "GB_SNG_BMS6")
					SET_LOCAL_BIGMESSAGE_BIT0(ENUM_TO_INT(thisMessage))
					
					#IF IS_DEBUG_BUILD	
					PRINT_BIG_MESSAGE_DEBUG_INFORMATION(ENUM_TO_INT(thisMessage), "GB_CHAL_OVER", "GB_SNG_BMS6") 
					#ENDIF
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF

ENDPROC

PROC DO_ATM_ROB_TICKER(int value)

	SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerData
	TickerData.TickerEvent = TICKER_EVENT_GB_CASHING_OUT_ROBBED_ATM
	TickerData.playerID = PLAYER_ID()
	TickerData.dataInt = value
	BROADCAST_TICKER_EVENT(TickerData, ALL_PLAYERS_ON_SCRIPT(TRUE))

ENDPROC

PROC DO_DROP_CASH_TICKER(int value)

	SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerData
	TickerData.TickerEvent = TICKER_EVENT_GB_CASHING_OUT_DROPPED_CASH_KILLED
	TickerData.playerID = PLAYER_ID()
	TickerData.dataInt = value
	BROADCAST_TICKER_EVENT(TickerData, ALL_PLAYERS_ON_SCRIPT(TRUE))

ENDPROC

// ------------------------------------------------------------------|<

// Blip Management -------------------------------------------------->|

PROC ADD_ALL_ATM_BLIPS()
	INT i
	REPEAT MAX_NUM_ATMs i
		IF NOT DOES_BLIP_EXIST(ATMBlip[i])
			ATMBlip[i] = ADD_BLIP_FOR_COORD(GET_ATM_COORDS(i))
			SET_BLIP_SPRITE(ATMBlip[i], RADAR_TRACE_CASH_PICKUP)
			SET_BLIP_COLOUR_FROM_HUD_COLOUR(ATMBlip[i], HUD_COLOUR_GREEN)
			SET_BLIP_PRIORITY(ATMBlip[i], BLIP_PRIORITY_HIGHEST_SPECIAL_LOW)
			SET_BLIP_AS_SHORT_RANGE(ATMBlip[i], FALSE)
			SET_BLIP_NAME_FROM_TEXT_FILE(ATMBlip[i], "GB_COUT_ATM")
			SET_BLIP_FLASH_TIMER(ATMBlip[i], ciFM_EVENT_BLIP_FLASH_TIME)
//			SET_BLIP_SCALE(ATMBlip[i], 1.5)
		ENDIF
	ENDREPEAT
ENDPROC

PROC ADD_ATM_BLIP(INT thisATM)
	IF NOT DOES_BLIP_EXIST(ATMBlip[thisATM])
		ATMBlip[thisATM] = ADD_BLIP_FOR_COORD(GET_ATM_COORDS(thisATM))
		SET_BLIP_COLOUR_FROM_HUD_COLOUR(ATMBlip[thisATM], HUD_COLOUR_GREEN)
		SET_BLIP_PRIORITY(ATMBlip[thisATM], BLIP_PRIORITY_HIGHEST_SPECIAL_LOW)
		SET_BLIP_AS_SHORT_RANGE(ATMBlip[thisATM], FALSE)
		SET_BLIP_NAME_FROM_TEXT_FILE(ATMBlip[thisATM], "GB_COUT_ATM")
//		SET_BLIP_SCALE(ATMBlip[thisATM], 1.5)
	ENDIF
ENDPROC

PROC REMOVE_ALL_ATM_BLIPS()
	INT i
	REPEAT MAX_NUM_ATMs i
		IF DOES_BLIP_EXIST(ATMBlip[i])
			REMOVE_BLIP(ATMBlip[i])
		ENDIF
	ENDREPEAT
ENDPROC

PROC REMOVE_ATM_BLIP(INT thisATM)
	IF DOES_BLIP_EXIST(ATMBlip[thisATM])
		REMOVE_BLIP(ATMBlip[thisATM])
	ENDIF
ENDPROC

PROC DISABLE_MELEE_THIS_FRAME()

	DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_MELEE_ATTACK_HEAVY)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_MELEE_ATTACK_LIGHT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_MELEE_ATTACK_ALTERNATE)

ENDPROC

// ------------------------------------------------------------------|<


///
///    HACKING GAMES
///    

// Run the hacking minigames ---------------------------------------->|

PROC CLEAN_UP_NEW_HACKING_GAME_SOUNDS()
	PRINTLN("[HACKING] CLEAN_UP_NEW_HACKING_GAME_SOUNDS()")
	IF IS_BIT_SET(iNewMiniGameSoundBitSet,0)
		IF iNewMiniGameSoundID[0] != -1
			IF NOT HAS_SOUND_FINISHED(iNewMiniGameSoundID[0])
				STOP_SOUND(iNewMiniGameSoundID[0])
				RELEASE_SOUND_ID(iNewMiniGameSoundID[0])
				PRINTLN("[HACKING] STOPPING SOUND iNewMiniGameSoundID[0]")
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iNewMiniGameSoundBitSet,1)
		IF iNewMiniGameSoundID[1] != -1
			IF NOT HAS_SOUND_FINISHED(iNewMiniGameSoundID[1])
				STOP_SOUND(iNewMiniGameSoundID[1])
				RELEASE_SOUND_ID(iNewMiniGameSoundID[1])
				PRINTLN("[HACKING] STOPPING SOUND iNewMiniGameSoundID[1]")
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iNewMiniGameSoundBitSet,2)
		IF iNewMiniGameSoundID[2] != -1
			IF NOT HAS_SOUND_FINISHED(iNewMiniGameSoundID[2])
				STOP_SOUND(iNewMiniGameSoundID[2])
				RELEASE_SOUND_ID(iNewMiniGameSoundID[2])
				PRINTLN("[HACKING] STOPPING SOUND iNewMiniGameSoundID[2]")
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iNewMiniGameSoundBitSet,3)
		IF iNewMiniGameSoundID[3] != -1
			IF NOT HAS_SOUND_FINISHED(iNewMiniGameSoundID[3])
				STOP_SOUND(iNewMiniGameSoundID[3])
				RELEASE_SOUND_ID(iNewMiniGameSoundID[3])
				PRINTLN("[HACKING] STOPPING SOUND iNewMiniGameSoundID[3]")
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iNewMiniGameSoundBitSet,4)
		IF iNewMiniGameSoundID[4] != -1
			IF NOT HAS_SOUND_FINISHED(iNewMiniGameSoundID[4])
				STOP_SOUND(iNewMiniGameSoundID[4])
				RELEASE_SOUND_ID(iNewMiniGameSoundID[4])
				PRINTLN("[HACKING] STOPPING SOUND iNewMiniGameSoundID[4]")
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iNewMiniGameSoundBitSet,5)
		IF iNewMiniGameSoundID[5] != -1
			IF NOT HAS_SOUND_FINISHED(iNewMiniGameSoundID[5])
				STOP_SOUND(iNewMiniGameSoundID[5])
				RELEASE_SOUND_ID(iNewMiniGameSoundID[5])
				PRINTLN("[HACKING] STOPPING SOUND iNewMiniGameSoundID[5]")
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iNewMiniGameSoundBitSet,6)
		IF iNewMiniGameSoundID[6] != -1
			IF NOT HAS_SOUND_FINISHED(iNewMiniGameSoundID[6])
				STOP_SOUND(iNewMiniGameSoundID[6])
				RELEASE_SOUND_ID(iNewMiniGameSoundID[6])
				PRINTLN("[HACKING] STOPPING SOUND iNewMiniGameSoundID[6]")
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iNewMiniGameSoundBitSet,7)
		IF iNewMiniGameSoundID[7] != -1
			IF NOT HAS_SOUND_FINISHED(iNewMiniGameSoundID[7])
				STOP_SOUND(iNewMiniGameSoundID[7])
				RELEASE_SOUND_ID(iNewMiniGameSoundID[7])
				PRINTLN("[HACKING] STOPPING SOUND iNewMiniGameSoundID[7]")
			ENDIF
		ENDIF
	ENDIF
	
	
	int HackingInt = 0
	
	FOR HackingInt = 0 to(MAX_SOUNDS - 1)
		IF sHacking[HackingInt] != -1 
			IF NOT HAS_SOUND_FINISHED(sHacking[HackingInt])
			STOP_SOUND(sHacking[HackingInt])
			RELEASE_SOUND_ID(sHacking[HackingInt])
			sHacking[HackingInt] = -1 
			PRINTLN("[HACKING] STOPPING SOUND sHacking[HackingInt]")
			ENDIF
		ENDIF
	ENDFOR
	
	CLEAR_BIT(iNewMiniGameSoundBitSet,0)
	CLEAR_BIT(iNewMiniGameSoundBitSet,1)
	CLEAR_BIT(iNewMiniGameSoundBitSet,2)
	CLEAR_BIT(iNewMiniGameSoundBitSet,3)
	CLEAR_BIT(iNewMiniGameSoundBitSet,4)
	CLEAR_BIT(iNewMiniGameSoundBitSet,5)
	CLEAR_BIT(iNewMiniGameSoundBitSet,6)
	CLEAR_BIT(iNewMiniGameSoundBitSet,7)
	
ENDPROC

/// PURPOSE:
///    Rungs general cleanup at the end of the hacking minigame
/// PARAMS:
///    bSuccess - If the player beat the game
///    bKilled - If the reason we're cleaning up is because the player was killed
PROC CLEANUP_HACKING(BOOL bSuccess = FALSE, BOOL bKilled = FALSE)

	IF bSuccess
		FORCE_QUIT_PASS_HACKING_MINIGAME(hackCashingOut)
		
		INT iNewHack = GET_NEXT_AVAILABLE_MINIGAME(iMyHackMG)
		iMyHackMG = iNewHack
	ENDIF

	CLEAR_BIT(hackCashingOut.bsHacking,BS_IS_HACKING)
	
	RESET_NEW_HACKING_MINIGAME_DATA(hackCashingOut)
	
	CLEAN_UP_NEW_HACKING_GAME_SOUNDS()
	
	IF bKilled
		FORCE_QUIT_FAIL_HACKING_MINIGAME(hackCashingOut)
	ENDIF

	IF GB_IS_GLOBAL_NON_BD_BIT0_SET(eGB_GLOBAL_NON_BD_BITSET_0_BLOCK_DPAD_DOWN_LB)
		GB_CLEAR_GLOBAL_NON_BD_BIT0(eGB_GLOBAL_NON_BD_BITSET_0_BLOCK_DPAD_DOWN_LB)
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(hackTimer)
		RESET_NET_TIMER(hackTimer)
	ENDIF
	
	IF iHackingProg <> 0
		iHackingProg = 0
	ENDIF

	IF IS_LOCAL_BIT0_SET(eLOCALBITSET0_DISABLED_CONTROL)
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		CLEAR_LOCAL_BIT0(eLOCALBITSET0_DISABLED_CONTROL)
	ENDIF
	
	DISABLE_RADAR_MAP(FALSE)
ENDPROC	

/// PURPOSE:
///    Sets up and runs a hacking mini game
/// PARAMS:
///    outcome - the outcome of the minigame (success/failure)
///    iTimeToHack - The amount of time it took to complete or fail
/// RETURNS:
///    TRUE when the player beats the mini game or fails by running out of lives or out of time
FUNC BOOL RUN_HACKING_MINI_GAME(HACKING_OUTCOME &outcome, INT &iTimeToHack)

	IF iHackingProg > 0
		IF IS_PED_INJURED(PLAYER_PED_ID())
		OR IS_PED_DEAD_OR_DYING(PLAYER_PED_ID())
			FORCE_QUIT_FAIL_HACKING_MINIGAME(hackCashingOut)
		
			CLEANUP_HACKING(FALSE, TRUE)
			
			PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - RUN_HACKING_MINI_GAME - Player has been killed whilst hacking")
			
			iHackingProg = 0
			
			outcome = eHACK_OUTCOME_KILLED
			RETURN TRUE
		ELSE
			DISABLE_MELEE_THIS_FRAME()
		ENDIF
	ENDIF

	SWITCH iHackingProg
	
		CASE 0
			REQUEST_HACKING_MINI_GAME()
			REQUEST_ADDITIONAL_TEXT("HACK", MINIGAME_TEXT_SLOT)
			
			
			PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - RUN_HACKING_MINI_GAME - iHackingProg = ", iHackingProg, ", Minigame and additional text requested")
			iHackingProg++
		BREAK
		
		CASE 1
			IF HAVE_HACKING_ASSETS_LOADED()
			AND HAS_ADDITIONAL_TEXT_LOADED(MINIGAME_TEXT_SLOT)
				SET_BIT(hackCashingOut.bsHacking, BS_IS_HACKING)
				SET_BIT(hackCashingOut.iBS2Hacking, BS2_LAUNCH_MG_IMMEDIATIALY)
				
				IF iMyHackMG < HACKING_IP_CONNECT OR iMyHackMG >= MAX_NUM_HACKING_MG
					PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - hacking out of range, seeing iMyHack to HACKING_IP_CONNECT")
					iMyHackMG = GET_NEXT_AVAILABLE_MINIGAME(iMyHackMG)
				ENDIF
				
				IF iMyHackMG = HACKING_NEW_MG
					hackCashingOut.bTestHacking = TRUE // For new hacking game
					iHackSpeed = 10
				ELIF iMyHackMG = HACKING_BRUTEFORCE
					hackCashingOut.bTestHacking = FALSE
					iHackSpeed = 77
				ELIF iMyHackMG = HACKING_IP_CONNECT
					hackCashingOut.bTestHacking = FALSE
					iHackSpeed = 50
				ENDIF
				
				IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_DISABLED_CONTROL)
					NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_ALLOW_PLAYER_DAMAGE | NSPC_CAN_BE_TARGETTED)
					SET_LOCAL_BIT0(eLOCALBITSET0_DISABLED_CONTROL)
				ENDIF
				
				DISABLE_RADAR_MAP(TRUE)
				
				PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - RUN_HACKING_MINI_GAME - iHackingProg = ", iHackingProg, ", Hack speed set and Hacking bits set")
				iHackingProg++				
			ELSE
				IF NOT HAVE_HACKING_ASSETS_LOADED()
					PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - RUN_HACKING_MINI_GAME - iHackingProg = ", iHackingProg, ", Waiting for hacking assets to load...")
				ENDIF
				IF NOT HAS_ADDITIONAL_TEXT_LOADED(MINIGAME_TEXT_SLOT)
					PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - RUN_HACKING_MINI_GAME - iHackingProg = ", iHackingProg, ", Waiting for additional text to load...")
				ENDIF
			ENDIF		
		BREAK
		
		CASE 2
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			HIDE_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_MP_RANK_BAR)
			
			// Prevent player from opening Dpad down leaderboard
			IF NOT GB_IS_GLOBAL_NON_BD_BIT0_SET(eGB_GLOBAL_NON_BD_BITSET_0_BLOCK_DPAD_DOWN_LB)
				GB_SET_GLOBAL_NON_BD_BIT0(eGB_GLOBAL_NON_BD_BITSET_0_BLOCK_DPAD_DOWN_LB)
			ENDIF
			
			SWITCH iMyHackMG
			
				CASE HACKING_IP_CONNECT
					IF NOT HAS_NET_TIMER_STARTED(hackTimer)
						START_NET_TIMER(hackTimer)
					ENDIF
				
					RUN_HACKING_MINIGAME_WITH_PARAMETERS(hackCashingOut,
														CASHING_OUT_TUNABLE_GET_MINIGAME_LIVES(),
														CASHING_OUT_TUNABLE_GET_MINIGAME_LIVES(),
														iHackSpeed,
														DEFAULT,
														DEFAULT,
														DEFAULT,
														DEFAULT,
														TRUE,
														DEFAULT, 
														DEFAULT)
					
					IF HAS_PLAYER_BEAT_HACK_CONNECT(hackCashingOut, TRUE)
					#IF IS_DEBUG_BUILD
					OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_DOWN)
					#ENDIF
					
						PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - RUN_HACKING_MINI_GAME - Player has beated IP Connect!")
						outcome = eHACK_OUTCOME_SUCCESS
						iTimeToHack = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(hackTimer)
						CLEANUP_HACKING(TRUE)
						RETURN TRUE
						
//					ELIF HAS_NET_TIMER_EXPIRED(hackTimer, CASHING_OUT_TUNABLE_GET_MINIGAME_TIME())
//					
//						PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - RUN_HACKING_MINI_GAME - Player has failed to hack IP Connect - ran out of time")
//						outcome = eHACK_OUTCOME_FAILURE
//						iTimeToHack = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(hackTimer)
//						CLEANUP_HACKING(FALSE)
//						RETURN TRUE
					
					ELIF HAS_PLAYER_FAILED_HACKING(hackCashingOut, TRUE)
					#IF IS_DEBUG_BUILD
					OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_UP)
					#ENDIF
					
						PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - RUN_HACKING_MINI_GAME - Player has failed to hack IP Connect - ran out of lives")
						outcome = eHACK_OUTCOME_FAILURE
						iTimeToHack = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(hackTimer)
						CLEANUP_HACKING(FALSE)
						RETURN TRUE
						
					ENDIF
				BREAK
				
				CASE HACKING_BRUTEFORCE
					IF NOT HAS_NET_TIMER_STARTED(hackTimer)
						START_NET_TIMER(hackTimer)
					ENDIF
				
					RUN_HACKING_MINIGAME_WITH_PARAMETERS(hackCashingOut, 
														CASHING_OUT_TUNABLE_GET_MINIGAME_LIVES(),
														CASHING_OUT_TUNABLE_GET_MINIGAME_LIVES(),
														iHackSpeed,
														DEFAULT,
														DEFAULT, 
														DEFAULT, 
														DEFAULT, 
														DEFAULT, 
														TRUE, 
														DEFAULT,
														DEFAULT,
														DEFAULT,
														DEFAULT,
														DEFAULT,
														DEFAULT,
														FALSE)
														
					IF HAS_PLAYER_BEAT_BRUTEFORCE(hackCashingOut, TRUE)
					#IF IS_DEBUG_BUILD
					OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_DOWN)
					#ENDIF
					
						PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - RUN_HACKING_MINI_GAME - Player has beaten Bruteforce!")
						outcome = eHACK_OUTCOME_SUCCESS
						iTimeToHack = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(hackTimer)
						CLEANUP_HACKING(TRUE)
						RETURN TRUE
						
//					ELIF HAS_NET_TIMER_EXPIRED(hackTimer, CASHING_OUT_TUNABLE_GET_MINIGAME_TIME())
//					
//						PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - RUN_HACKING_MINI_GAME - Player has failed to hack brute force - ran out of time")
//						outcome = eHACK_OUTCOME_FAILURE
//						iTimeToHack = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(hackTimer)
//						CLEANUP_HACKING(FALSE)
//						RETURN TRUE
					
					ELIF HAS_PLAYER_FAILED_HACKING(hackCashingOut, TRUE)
					#IF IS_DEBUG_BUILD
					OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_UP)
					#ENDIF
					
						PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - RUN_HACKING_MINI_GAME - Player has failed to hack Bruteforce - ran out of lives")
						outcome = eHACK_OUTCOME_FAILURE
						iTimeToHack = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(hackTimer)
						CLEANUP_HACKING(FALSE)
						RETURN TRUE
						
					ENDIF
				BREAK
				
				CASE HACKING_NEW_MG
					IF NOT HAS_NET_TIMER_STARTED(hackTimer)
						START_NET_TIMER(hackTimer)
					ENDIF
				
					RUN_HACKING_MINIGAME_WITH_PARAMETERS(hackCashingOut, 
									 					CASHING_OUT_TUNABLE_GET_MINIGAME_LIVES(),
														CASHING_OUT_TUNABLE_GET_MINIGAME_LIVES(),
														iHackSpeed,
														DEFAULT,
														DEFAULT,
														DEFAULT,
														DEFAULT,
														DEFAULT,
														DEFAULT,
														DEFAULT,
														DEFAULT,
														DEFAULT,
														DEFAULT,
														DEFAULT,
														DEFAULT,
														FALSE) 
														
					IF HAS_PLAYER_BEAT_NEW_HACKING(hackCashingOut, TRUE)
					#IF IS_DEBUG_BUILD
					OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_DOWN)
					#ENDIF
					
						PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - RUN_HACKING_MINI_GAME - Player has beaten new hacking!")
						outcome = eHACK_OUTCOME_SUCCESS
						iTimeToHack = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(hackTimer)
						CLEANUP_HACKING(TRUE)
						RETURN TRUE
					
//					ELIF HAS_NET_TIMER_EXPIRED(hackTimer, CASHING_OUT_TUNABLE_GET_MINIGAME_TIME())
//					
//						PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - RUN_HACKING_MINI_GAME - Player has failed to hack new hacking - ran out of time")
//						outcome = eHACK_OUTCOME_FAILURE
//						iTimeToHack = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(hackTimer)
//						CLEANUP_HACKING(FALSE)
//						RETURN TRUE
					
					ELIF HAS_PLAYER_FAILED_HACKING(hackCashingOut, TRUE)
					#IF IS_DEBUG_BUILD
					OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_UP)
					#ENDIF
					
						PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - RUN_HACKING_MINI_GAME - Player has failed to hack new hacking - ran out of lives")
						outcome = eHACK_OUTCOME_FAILURE
						iTimeToHack = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(hackTimer)
						CLEANUP_HACKING(FALSE)
						RETURN TRUE
						
						
						
					ENDIF													
				BREAK
				
				// Run IP Connect by default
				DEFAULT
					PRINTLN("[cashing out spam] - running IP connect by default because iMyHackMG = ", iMyHackMG)
					
					IF NOT HAS_NET_TIMER_STARTED(hackTimer)
						START_NET_TIMER(hackTimer)
					ENDIF
				
					RUN_HACKING_MINIGAME_WITH_PARAMETERS(hackCashingOut,
														CASHING_OUT_TUNABLE_GET_MINIGAME_LIVES(),
														CASHING_OUT_TUNABLE_GET_MINIGAME_LIVES(),
														iHackSpeed,
														DEFAULT,
														DEFAULT,
														DEFAULT,
														DEFAULT,
														TRUE,
														DEFAULT, 
														DEFAULT,
														DEFAULT,
														DEFAULT,
														DEFAULT,
														DEFAULT,
														DEFAULT,
														FALSE)
					
					IF HAS_PLAYER_BEAT_HACK_CONNECT(hackCashingOut, TRUE)
					#IF IS_DEBUG_BUILD
					OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_DOWN)
					#ENDIF
					
						PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - RUN_HACKING_MINI_GAME - Player has beated IP Connect!")
						outcome = eHACK_OUTCOME_SUCCESS
						iTimeToHack = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(hackTimer)
						CLEANUP_HACKING(TRUE)
						RETURN TRUE
						
					ELIF HAS_NET_TIMER_EXPIRED(hackTimer, CASHING_OUT_TUNABLE_GET_MINIGAME_TIME())
					
						PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - RUN_HACKING_MINI_GAME - Player has failed to hack IP Connect - ran out of time")
						outcome = eHACK_OUTCOME_FAILURE
						iTimeToHack = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(hackTimer)
						CLEANUP_HACKING(FALSE)
						RETURN TRUE
					
					ELIF HAS_PLAYER_FAILED_HACKING(hackCashingOut, TRUE)
					#IF IS_DEBUG_BUILD
					OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_UP)
					#ENDIF
					
						PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - RUN_HACKING_MINI_GAME - Player has failed to hack IP Connect - ran out of lives")
						outcome = eHACK_OUTCOME_FAILURE
						iTimeToHack = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(hackTimer)
						CLEANUP_HACKING(FALSE)
						RETURN TRUE
						
					ENDIF
				BREAK
			
			ENDSWITCH
			
			IF IS_BIT_SET(hackCashingOut.iBS2Hacking, BS2_QUIT_MG_IMMEDIATIALY)
				CLEAR_BIT(hackCashingOut.bsHacking, BS_IS_HACKING)
				
				RESET_NEW_HACKING_MINIGAME_DATA(hackCashingOut)

				NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				
				IF GB_IS_GLOBAL_NON_BD_BIT0_SET(eGB_GLOBAL_NON_BD_BITSET_0_BLOCK_DPAD_DOWN_LB)
					GB_CLEAR_GLOBAL_NON_BD_BIT0(eGB_GLOBAL_NON_BD_BITSET_0_BLOCK_DPAD_DOWN_LB)
				ENDIF
				
				IF HAS_NET_TIMER_STARTED(hackTimer)
					RESET_NET_TIMER(hackTimer)
				ENDIF
				
				PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - RUN_HACKING_MINI_GAME - IS_BIT_SET(hackCashingOut.iBS2Hacking, BS2_QUIT_MG_IMMEDIATIALY), setting outcome to ABANDON")
				
				iHackingProg = 0
				
				IF IS_PED_INJURED(PLAYER_PED_ID())
					outcome = eHACK_OUTCOME_KILLED
				ELSE				
					outcome = eHACK_OUTCOME_ABANDON
				ENDIF
				
				IF outcome = eHACK_OUTCOME_KILLED
					CLEANUP_HACKING(FALSE,TRUE)
				ELSE
					CLEANUP_HACKING()
				ENDIF
				
				DISABLE_RADAR_MAP(FALSE)
				
				RETURN TRUE
				
			ENDIF			
		BREAK
	
	ENDSWITCH

	RETURN FALSE
ENDFUNC

PROC HANDLE_APPLY_WANTED_BASED_ON_HACK_TIME()
	INT iRand

	// Hack took less than 10 seconds, 10% chance of wanted
	IF iHackTime < TEN_SECONDS
		PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - HANDLE_APPLY_WANTED_BASED_ON_HACK_TIME - Player hacked in ", iHackTime / 1000, " seconds, 10% chance of being wanted")

		iRand = GET_RANDOM_INT_IN_RANGE(0, 100)
		
		IF iRand >= CASHING_OUT_TUNABLE_UNDER_10S_CHANCE()
			PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - HANDLE_APPLY_WANTED_BASED_ON_HACK_TIME - Player hacked in ", iHackTime / 1000, " seconds, 10% chance missed, no wanted level applied")
		ELSE
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < CASHING_OUT_TUNABLE_MAX_WANTED_LEVEL_GIVEN()
				IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < CASHING_OUT_WANTED_LEVEL_COMPLETION()
					PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - HANDLE_APPLY_WANTED_BASED_ON_HACK_TIME - Player hacked in ", iHackTime / 1000, " seconds, 10% chance hit, ", CASHING_OUT_WANTED_LEVEL_COMPLETION(), "-star wanted level applied")
					SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(), CASHING_OUT_WANTED_LEVEL_COMPLETION())
					SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
					IF playerBD[PARTICIPANT_ID_TO_INT()].iHighestWantedRating < CASHING_OUT_WANTED_LEVEL_COMPLETION()
						playerBD[PARTICIPANT_ID_TO_INT()].iHighestWantedRating = CASHING_OUT_WANTED_LEVEL_COMPLETION()
					ENDIF
				ELSE
					PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - HANDLE_APPLY_WANTED_BASED_ON_HACK_TIME - Player hacked in ", iHackTime / 1000, " seconds, 10% chance hit, player already has ", CASHING_OUT_WANTED_LEVEL_COMPLETION(), " stars, increasing to ", (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) + 1))
					SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(), (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) + 1))
					SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
					IF playerBD[PARTICIPANT_ID_TO_INT()].iHighestWantedRating < (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) + 1)
						playerBD[PARTICIPANT_ID_TO_INT()].iHighestWantedRating = (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) + 1)
					ENDIF
				ENDIF
			ELSE
				PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - HANDLE_APPLY_WANTED_BASED_ON_HACK_TIME - Player hacked in ", iHackTime / 1000, " seconds, 10% chance hit, no wanted level applied as player already has 3 stars")
			ENDIF
		ENDIF

	// Between 10 and 20, 20% chance of wanted
	ELIF iHackTime >= TEN_SECONDS AND iHackTime < TWENTY_SECONDS
		PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - HANDLE_APPLY_WANTED_BASED_ON_HACK_TIME - Player hacked in ", iHackTime / 1000, " seconds, 20% chance of being wanted")
		
		iRand = GET_RANDOM_INT_IN_RANGE(0, 100)
		
		IF iRand >= CASHING_OUT_TUNABLE_10S_TO_20S_CHANCE()
			PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - HANDLE_APPLY_WANTED_BASED_ON_HACK_TIME - Player hacked in ", iHackTime / 1000, " seconds, 20% chance missed, no wanted level applied")
		ELSE
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < CASHING_OUT_TUNABLE_MAX_WANTED_LEVEL_GIVEN()
				IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < CASHING_OUT_WANTED_LEVEL_COMPLETION()
					PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - HANDLE_APPLY_WANTED_BASED_ON_HACK_TIME - Player hacked in ", iHackTime / 1000, " seconds, 20% chance hit, ", CASHING_OUT_WANTED_LEVEL_COMPLETION(), "-star wanted level applied")
					SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(), CASHING_OUT_WANTED_LEVEL_COMPLETION())
					SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
					IF playerBD[PARTICIPANT_ID_TO_INT()].iHighestWantedRating < CASHING_OUT_WANTED_LEVEL_COMPLETION()
						playerBD[PARTICIPANT_ID_TO_INT()].iHighestWantedRating = CASHING_OUT_WANTED_LEVEL_COMPLETION()
					ENDIF
				ELSE
					PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - HANDLE_APPLY_WANTED_BASED_ON_HACK_TIME - Player hacked in ", iHackTime / 1000, " seconds, 20% chance hit, player already has ", CASHING_OUT_WANTED_LEVEL_COMPLETION(), " stars, increasing to ", (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) + 1))
					SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(), (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) + 1))
					SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
					IF playerBD[PARTICIPANT_ID_TO_INT()].iHighestWantedRating < (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) + 1)
						playerBD[PARTICIPANT_ID_TO_INT()].iHighestWantedRating = (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) + 1)
					ENDIF
				ENDIF
			ELSE
				PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - HANDLE_APPLY_WANTED_BASED_ON_HACK_TIME - Player hacked in ", iHackTime / 1000, " seconds, 20% chance hit, no wanted level applied as player already has 3 stars")
			ENDIF
		ENDIF

	// Between 20 and 30, 40% chance of wanted
	ELIF iHackTime >= TWENTY_SECONDS AND iHackTime < THIRTY_SECONDS
		PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - HANDLE_APPLY_WANTED_BASED_ON_HACK_TIME - Player hacked in ", iHackTime / 1000, " seconds, 40% chance of being wanted")
		
		iRand = GET_RANDOM_INT_IN_RANGE(0, 100)
		
		IF iRand >= CASHING_OUT_TUNABLE_20S_TO_30S_CHANCE()
			PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - HANDLE_APPLY_WANTED_BASED_ON_HACK_TIME - Player hacked in ", iHackTime / 1000, " seconds, 40% chance missed, no wanted level applied")
		ELSE
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < CASHING_OUT_TUNABLE_MAX_WANTED_LEVEL_GIVEN()
				IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < CASHING_OUT_WANTED_LEVEL_COMPLETION()
					PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - HANDLE_APPLY_WANTED_BASED_ON_HACK_TIME - Player hacked in ", iHackTime / 1000, " seconds, 40% chance hit, ", CASHING_OUT_WANTED_LEVEL_COMPLETION(), "-star wanted level applied")
					SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(), CASHING_OUT_WANTED_LEVEL_COMPLETION())
					SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
					IF playerBD[PARTICIPANT_ID_TO_INT()].iHighestWantedRating < CASHING_OUT_WANTED_LEVEL_COMPLETION()
						playerBD[PARTICIPANT_ID_TO_INT()].iHighestWantedRating = CASHING_OUT_WANTED_LEVEL_COMPLETION()
					ENDIF
				ELSE
					PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - HANDLE_APPLY_WANTED_BASED_ON_HACK_TIME - Player hacked in ", iHackTime / 1000, " seconds, 40% chance hit, player already has ", CASHING_OUT_WANTED_LEVEL_COMPLETION(), " stars, increasing to ", (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) + 1))
					SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(), (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) + 1))
					SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
					IF playerBD[PARTICIPANT_ID_TO_INT()].iHighestWantedRating < (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) + 1)
						playerBD[PARTICIPANT_ID_TO_INT()].iHighestWantedRating = (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) + 1)
					ENDIF
				ENDIF
			ELSE
				PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - HANDLE_APPLY_WANTED_BASED_ON_HACK_TIME - Player hacked in ", iHackTime / 1000, " seconds, 40% chance hit, no wanted level applied as player already has 3 stars")
			ENDIF
		ENDIF


	// Between 30 and 40, 60% chance of 2 star wanted
	ELIF iHackTime >= THIRTY_SECONDS AND iHackTime < FORTY_SECONDS
		PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - HANDLE_APPLY_WANTED_BASED_ON_HACK_TIME - Player hacked in ", iHackTime / 1000, " seconds, 60% chance of being wanted")
		
		iRand = GET_RANDOM_INT_IN_RANGE(0, 100)
		
		IF iRand >= CASHING_OUT_TUNABLE_30S_TO_40S_CHANCE()
			PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - HANDLE_APPLY_WANTED_BASED_ON_HACK_TIME - Player hacked in ", iHackTime / 1000, " seconds, 60% chance missed, no wanted level applied")
		ELSE
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < CASHING_OUT_TUNABLE_MAX_WANTED_LEVEL_GIVEN()
				IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < CASHING_OUT_WANTED_LEVEL_COMPLETION()
					PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - HANDLE_APPLY_WANTED_BASED_ON_HACK_TIME - Player hacked in ", iHackTime / 1000, " seconds, 60% chance hit, ", CASHING_OUT_WANTED_LEVEL_COMPLETION(), "-star wanted level applied")
					SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(), CASHING_OUT_WANTED_LEVEL_COMPLETION())
					SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
					IF playerBD[PARTICIPANT_ID_TO_INT()].iHighestWantedRating < CASHING_OUT_WANTED_LEVEL_COMPLETION()
						playerBD[PARTICIPANT_ID_TO_INT()].iHighestWantedRating = CASHING_OUT_WANTED_LEVEL_COMPLETION()
					ENDIF
				ELSE
					PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - HANDLE_APPLY_WANTED_BASED_ON_HACK_TIME - Player hacked in ", iHackTime / 1000, " seconds, 60% chance hit, player already has ", CASHING_OUT_WANTED_LEVEL_COMPLETION(), " stars, increasing to ", (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) + 1))
					SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(), (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) + 1))
					SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
					IF playerBD[PARTICIPANT_ID_TO_INT()].iHighestWantedRating < (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) + 1)
						playerBD[PARTICIPANT_ID_TO_INT()].iHighestWantedRating = (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) + 1)
					ENDIF
				ENDIF
			ELSE
				PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - HANDLE_APPLY_WANTED_BASED_ON_HACK_TIME - Player hacked in ", iHackTime / 1000, " seconds, 60% chance hit, no wanted level applied as player already has 3 stars")
			ENDIF
		ENDIF
		
	// Between 40 and 50, 80% chance of 2 star wanted
	ELIF iHackTime >= FORTY_SECONDS AND iHackTime < FIFTY_SECONDS
		PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - HANDLE_APPLY_WANTED_BASED_ON_HACK_TIME - Player hacked in ", iHackTime / 1000, " seconds, 80% chance of being wanted")
		
		iRand = GET_RANDOM_INT_IN_RANGE(0, 100)
		
		IF iRand >= CASHING_OUT_TUNABLE_40S_TO_50S_CHANCE()
			PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - HANDLE_APPLY_WANTED_BASED_ON_HACK_TIME - Player hacked in ", iHackTime / 1000, " seconds, 80% chance missed, no wanted level applied")
		ELSE
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < CASHING_OUT_TUNABLE_MAX_WANTED_LEVEL_GIVEN()
				IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < CASHING_OUT_WANTED_LEVEL_COMPLETION()
					PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - HANDLE_APPLY_WANTED_BASED_ON_HACK_TIME - Player hacked in ", iHackTime / 1000, " seconds, 80% chance hit, ", CASHING_OUT_WANTED_LEVEL_COMPLETION(), "-star wanted level applied")
					SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(), CASHING_OUT_WANTED_LEVEL_COMPLETION())
					SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
					IF playerBD[PARTICIPANT_ID_TO_INT()].iHighestWantedRating < CASHING_OUT_WANTED_LEVEL_COMPLETION()
						playerBD[PARTICIPANT_ID_TO_INT()].iHighestWantedRating = CASHING_OUT_WANTED_LEVEL_COMPLETION()
					ENDIF
				ELSE
					PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - HANDLE_APPLY_WANTED_BASED_ON_HACK_TIME - Player hacked in ", iHackTime / 1000, " seconds, 80% chance hit, player already has ", CASHING_OUT_WANTED_LEVEL_COMPLETION(), " stars, increasing to ", (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) + 1))
					SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(), (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) + 1))
					SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
					IF playerBD[PARTICIPANT_ID_TO_INT()].iHighestWantedRating < (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) + 1)
						playerBD[PARTICIPANT_ID_TO_INT()].iHighestWantedRating = (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) + 1)
					ENDIF
				ENDIF
			ELSE
				PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - HANDLE_APPLY_WANTED_BASED_ON_HACK_TIME - Player hacked in ", iHackTime / 1000, " seconds, 80% chance hit, no wanted level applied as player already has 3 stars")
			ENDIF
		ENDIF
		
	// Between 50 and 60, 100% chance of 2 star wanted
	ELIF iHackTime >= FIFTY_SECONDS AND iHackTime < ONE_MINUTE
		
		iRand = GET_RANDOM_INT_IN_RANGE(0, 100)
		
		IF iRand >= CASHING_OUT_TUNABLE_50S_TO_59S_CHANCE()
			PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - HANDLE_APPLY_WANTED_BASED_ON_HACK_TIME - Player hacked in ", iHackTime / 1000, " seconds, 80% chance missed, no wanted level applied")
		ELSE
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < CASHING_OUT_TUNABLE_MAX_WANTED_LEVEL_GIVEN()
				IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < CASHING_OUT_WANTED_LEVEL_COMPLETION()
					PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - HANDLE_APPLY_WANTED_BASED_ON_HACK_TIME - Player hacked in ", iHackTime / 1000, " seconds, 100% chance of wanted, ", CASHING_OUT_WANTED_LEVEL_COMPLETION(), "-star wanted level applied")
					SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(), CASHING_OUT_WANTED_LEVEL_COMPLETION())
					SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
					IF playerBD[PARTICIPANT_ID_TO_INT()].iHighestWantedRating < CASHING_OUT_WANTED_LEVEL_COMPLETION()
						playerBD[PARTICIPANT_ID_TO_INT()].iHighestWantedRating = CASHING_OUT_WANTED_LEVEL_COMPLETION()
					ENDIF
				ELSE
					PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - HANDLE_APPLY_WANTED_BASED_ON_HACK_TIME - Player hacked in ", iHackTime / 1000, " seconds, 100% chance of wanted, player already has ", CASHING_OUT_WANTED_LEVEL_COMPLETION(), " stars, increasing to ", (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) + 1))
					SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(), (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) + 1))
					SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
					IF playerBD[PARTICIPANT_ID_TO_INT()].iHighestWantedRating < (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) + 1)
						playerBD[PARTICIPANT_ID_TO_INT()].iHighestWantedRating = (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) + 1)
					ENDIF
				ENDIF
			ELSE
				PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - HANDLE_APPLY_WANTED_BASED_ON_HACK_TIME - Player hacked in ", iHackTime / 1000, " seconds, 100% chance of wanted, no wanted level applied as player already has 3 stars")
			ENDIF
		ENDIF
		
	// Time ran out, 100% chance of wanted
	ELSE
		
		iRand = GET_RANDOM_INT_IN_RANGE(0, 100)
	
		IF iRand >= CASHING_OUT_TUNABLE_TIME_EXPIRE_CHANCE()
			PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - HANDLE_APPLY_WANTED_BASED_ON_HACK_TIME - Player hacked in ", iHackTime / 1000, " seconds, 80% chance missed, no wanted level applied")
		ELSE
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < CASHING_OUT_TUNABLE_MAX_WANTED_LEVEL_GIVEN()
				PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - HANDLE_APPLY_WANTED_BASED_ON_HACK_TIME - Player hacked in ", iHackTime / 1000, " seconds, 100% chance hit, wanted level applied")
				SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(), CASHING_OUT_TUNABLE_MAX_WANTED_LEVEL_GIVEN())
				SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
				IF playerBD[PARTICIPANT_ID_TO_INT()].iHighestWantedRating < CASHING_OUT_WANTED_LEVEL_COMPLETION()
						playerBD[PARTICIPANT_ID_TO_INT()].iHighestWantedRating = CASHING_OUT_WANTED_LEVEL_COMPLETION()
					ENDIF
			ELSE
				PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - HANDLE_APPLY_WANTED_BASED_ON_HACK_TIME - Player hacked in ", iHackTime / 1000, " seconds, 100% chance hit, no wanted level applied as player already has ", CASHING_OUT_TUNABLE_MAX_WANTED_LEVEL_GIVEN(), " stars")
			ENDIF
		ENDIF
	ENDIF

ENDPROC

FUNC INT GET_CASH_TO_DROP(INT iInitialCash)
	
	IF iInitialCash <= 100
	AND iInitialCash >= 10
		PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - HANDLE_LOSE_CASH_UPON_DEATH - GET_CASH_TO_DROP: Player has less than $100, dropping $10")
		RETURN 10
	ENDIF
	
	INT toDrop = ROUND((iInitialCash / 100) * CASHING_OUT_TUNABLE_GET_CASH_DROP_PERCENTAGE())
	
	IF toDrop <= 0
		PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - HANDLE_LOSE_CASH_UPON_DEATH - GET_CASH_TO_DROP: $", toDrop, ", returning 0")
		RETURN 0
	ENDIF
	
	PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - HANDLE_LOSE_CASH_UPON_DEATH - GET_CASH_TO_DROP: $", toDrop)
	RETURN toDrop
ENDFUNC

PROC HANDLE_LOSE_CASH_UPON_DEATH()
	IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
		IF bBeenKilled = FALSE
			PRINTLN("[GB CASHING_OUT] [ST EXEC] - HANDLE_LOSE_CASH_UPON_DEATH - local player has died.")
			INT iCash = GET_CASH_TO_DROP( GET_PLAYER_CASH_COLLECTED(PARTICIPANT_ID_TO_INT()) )
			IF iCash > 0
			//Consider using  NETWORK_CAN_SPEND_MONEY	
				IF USE_SERVER_TRANSACTIONS()
					INT iTransactionSlot
					TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_SPEND_CASH_DROP, iCash, iTransactionSlot)
				ELSE
					NETWORK_SPENT_CASH_DROP(iCash)
				ENDIF
			
				DO_DROP_CASH_TICKER(iCash)
				SET_CLIENT_BIT0(eCLIENTBITSET0_KILLED_WITH_CASH)
				playerBD[PARTICIPANT_ID_TO_INT()].iCashCollected -= iCash
			ENDIF
			bBeenKilled = TRUE
		ENDIF
	ELSE
		IF IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), eCLIENTBITSET0_KILLED_WITH_CASH)
			IF HAS_PLAYER_FULLY_SPAWNED()
				IF bBeenKilled = TRUE
					DO_HELP_TEXT(eHELP_CASH_DROPPED_WHEN_KILLED)
					bBeenKilled = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC HANDLE_HACKING_CLEANUP()
	IF NATIVE_TO_INT(PARTICIPANT_ID()) != -1
		IF IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), eCLIENTBITSET0_HACKING_ATM)
			PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - HANDLE_HACKING_CLEANUP - called")
			
			FORCE_QUIT_FAIL_HACKING_MINIGAME(hackCashingOut)
			
			// Run hacking cleanup
			CLEANUP_HACKING()
			
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				//Do help
//				INT iATM
//				GET_CLOSEST_ATM_COORDS(iATM)
				IF iATM_beingHacked != -1
					IF IS_ATM_BLOCKED(iATM_beingHacked)
						DO_HELP_TEXT(eHELP_BEATEN_TO_HACK)
					ENDIF
				ENDIF
			ENDIF
			
//			IF NOT IS_ENTITY_IN_STRIPCLUB(PLAYER_PED_ID())
//				SET_PED_SHOULD_PLAY_NORMAL_SCENARIO_EXIT(PLAYER_PED_ID())
//				CLEAR_PED_TASKS(PLAYER_PED_ID())
//			ENDIF
			
			iATM_beingHacked = -1
			
			// Player is no longer hacking an ATM
			CLEAR_CLIENT_BIT0(eCLIENTBITSET0_HACKING_ATM)
		ENDIF
	ENDIF
ENDPROC
// ------------------------------------------------------------------|<

// Maintains -------------------------------------------------------->|

PROC MAINTAIN_BLIPS()
	IF SHOULD_UI_BE_HIDDEN()
		EXIT
	ENDIF
	
	SWITCH GET_CLIENT_MISSION_STAGE(PARTICIPANT_ID_TO_INT())
		CASE eMS_INIT
			ADD_ALL_ATM_BLIPS()
			
			HIDE_ALL_SHOP_BLIPS(TRUE)
		BREAK
		
		CASE eMS_COLLECT
			INT i
			REPEAT MAX_NUM_ATMs i
				IF DOES_BLIP_EXIST(ATMBlip[i])
					SET_BLIP_ALPHA_BASED_ON_DIST_FROM_PLAYER(ATMBlip[i], 25)
				ENDIF
			ENDREPEAT
		BREAK
	ENDSWITCH
ENDPROC

PROC MAINTAIN_OBJECTIVE_TEXT()
	IF SHOULD_UI_BE_HIDDEN()
		Clear_Any_Objective_Text_From_This_Script()
		EXIT
	ENDIF
	
	SWITCH GET_CLIENT_MISSION_STAGE(PARTICIPANT_ID_TO_INT())
	
		CASE eMS_INIT
		
		BREAK
		
		CASE eMS_COLLECT
			IF IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), eCLIENTBITSET0_NEAR_AN_ATM)
			OR (AM_I_SPECTATING_THE_GANG() AND IS_SPECTATOR_FOCUS_PLAYER_NEAR_AN_ATM())
				IF IS_CONTEXT_INTENTION_HELP_DISPLAYING(iHackContextIntention)
				OR IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), eCLIENTBITSET0_HACKING_ATM)
					IF NOT Has_This_MP_Objective_Text_Been_Received("GB_COUT_OT_01")
						Print_Objective_Text("GB_COUT_OT_01")
					ENDIF
				ELSE
					IF HAVE_I_HACKED_AT_LEAST_ONE_ATM()
					OR (AM_I_SPECTATING_THE_GANG() AND HAS_SPECTATOR_FOCUS_PLAYER_HACKED_AT_LEAST_ONE_ATM())
						IF NOT Has_This_MP_Objective_Text_Been_Received("GB_COUT_OT_02")
							Print_Objective_Text("GB_COUT_OT_02")
						ENDIF
					ELSE
						IF NOT Has_This_MP_Objective_Text_Been_Received("GB_COUT_OT_00")
							Print_Objective_Text("GB_COUT_OT_00")
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF HAVE_I_HACKED_AT_LEAST_ONE_ATM()
				OR (AM_I_SPECTATING_THE_GANG() AND HAS_SPECTATOR_FOCUS_PLAYER_HACKED_AT_LEAST_ONE_ATM())
					IF NOT Has_This_MP_Objective_Text_Been_Received("GB_COUT_OT_02")
						Print_Objective_Text("GB_COUT_OT_02")
					ENDIF
				ELSE
					IF NOT Has_This_MP_Objective_Text_Been_Received("GB_COUT_OT_00")
						Print_Objective_Text("GB_COUT_OT_00")
					ENDIF
				ENDIF
			ENDIF
		BREAK
	
	ENDSWITCH
	
ENDPROC

PROC MAINTAIN_COUNTDOWN_MUSIC()
	IF HAS_NET_TIMER_STARTED(serverBD.modeTimer)
		IF iTimeRemaining  <= 35000
			SWITCH eFinalCountdownStage
				CASE eFCOUNTDOWN_PREP
					// IF within a second of hitting the end prep period
					IF iTimeRemaining <= (35000)
					AND iTimeRemaining >= (33000)
						IF PREPARE_MUSIC_EVENT("APT_PRE_COUNTDOWN_STOP")
							PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - MAINTAIN_COUNTDOWN_MUSIC - eFCOUNTDOWN_PREP - APT_PRE_COUNTDOWN_STOP has been triggered")
							SET_AUDIO_FLAG("DisableFlightMusic", TRUE)
							TRIGGER_MUSIC_EVENT("APT_PRE_COUNTDOWN_STOP")
							eFinalCountdownStage = eFCOUNTDOWN_START
						ENDIF
					ENDIF
				BREAK
				
				CASE eFCOUNTDOWN_START
					IF NOT IS_BIT_SET(iFinalCountdownBitSet, iFCBS_TriggeredCountdown)
						IF NOT IS_BIT_SET(iFinalCountdownBitSet, iFCBS_SuppressWanted)
							IF iTimeRemaining <= 33000
							AND iTimeRemaining > 30000
								SET_AUDIO_FLAG("WantedMusicDisabled", TRUE)
								PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - MAINTAIN_COUNTDOWN_MUSIC - eFCOUNTDOWN_START - suppressing wanted music")
								SET_BIT(iFinalCountdownBitSet, iFCBS_SuppressWanted)
							ENDIF
						ENDIF
						IF iTimeRemaining <= 30000
							IF PREPARE_MUSIC_EVENT("APT_COUNTDOWN_30S")
								SET_AUDIO_FLAG("AllowScoreAndRadio", TRUE)
								SET_USER_RADIO_CONTROL_ENABLED(FALSE)
								TRIGGER_MUSIC_EVENT("APT_COUNTDOWN_30S")
								PREPARE_MUSIC_EVENT("APT_COUNTDOWN_30S_KILL")
								PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - MAINTAIN_COUNTDOWN_MUSIC - eFCOUNTDOWN_START - APT_COUNTDOWN_30S has been triggered")
								SET_BIT(iFinalCountdownBitSet, iFCBS_TriggeredCountdown)
							ENDIF
						ENDIF
					ELSE
						IF NOT IS_BIT_SET(iFinalCountdownBitSet, iFCBS_ReEnabledRadioControl)
							IF iTimeRemaining <= 27000
								SET_USER_RADIO_CONTROL_ENABLED(TRUE)
								SET_AUDIO_FLAG("AllowScoreAndRadio", FALSE)
								PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - MAINTAIN_COUNTDOWN_MUSIC - eFCOUNTDOWN_START - Radio control re-enabled")
								SET_BIT(iFinalCountdownBitSet, iFCBS_ReEnabledRadioControl)
								eFinalCountdownStage = eFCOUNTDOWN_END_OR_KILL
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE eFCOUNTDOWN_END_OR_KILL
					IF NOT IS_BIT_SET(iFinalCountdownBitSet, iFCBS_TriggeredCountdownKill)
						IF NOT IS_BIT_SET(iFinalCountdownBitSet, iFCBS_Trigger5s)
							IF iTimeRemaining <= 5000
								PLAY_SOUND_FRONTEND(-1, "5s", "MP_MISSION_COUNTDOWN_SOUNDSET", FALSE)
								SET_BIT(iFinalCountdownBitSet, iFCBS_Trigger5s)
							ENDIF
						ENDIF
					
						IF iTimeRemaining <= 0
							IF PREPARE_MUSIC_EVENT("APT_FADE_IN_RADIO")
								TRIGGER_MUSIC_EVENT("APT_FADE_IN_RADIO")
								CANCEL_MUSIC_EVENT("APT_COUNTDOWN_30S_KILL")
								PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - MAINTAIN_COUNTDOWN_MUSIC - eFCOUNTDOWN_END_OR_KILL - Time ran out, APT_FADE_IN_RADIO has been triggered")
								SET_BIT(iFinalCountdownBitSet, iFCBS_TriggeredCountdownKill)
							ENDIF
						ELSE
							IF IS_SERVER_BIT0_SET(eSERVERBITSET0_BOSS_LEFT)
								IF PREPARE_MUSIC_EVENT("APT_FADE_IN_RADIO")
									CANCEL_MUSIC_EVENT("APT_COUNTDOWN_30S")
									TRIGGER_MUSIC_EVENT("APT_COUNTDOWN_30S_KILL")
									TRIGGER_MUSIC_EVENT("APT_FADE_IN_RADIO")
									PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - MAINTAIN_COUNTDOWN_MUSIC - eFCOUNTDOWN_END_OR_KILL - Mode ended before timeout, APT_COUNTDOWN_30S_KILL and APT_FADE_IN_RADIO have been triggered")
									SET_BIT(iFinalCountdownBitSet, iFCBS_TriggeredCountdownKill)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					IF IS_BIT_SET(iFinalCountdownBitSet, iFCBS_SuppressWanted)
						IF iOvertime <= -2000
							SET_AUDIO_FLAG("WantedMusicDisabled", FALSE)
							SET_AUDIO_FLAG("DisableFlightMusic", FALSE)
							PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - MAINTAIN_COUNTDOWN_MUSIC - eFCOUNTDOWN_END_OR_KILL - unsuppressing wanted music")
							CLEAR_BIT(iFinalCountdownBitSet, iFCBS_SuppressWanted)
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_BOTTOM_RIGHT_UI()
	IF SHOULD_UI_BE_HIDDEN()
		EXIT
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(serverBD.modeTimer)
		INT iCombinedTotal = -1
		BOOL bForce = FALSE
		HUD_COLOURS timeColour
		
		iTimeRemaining = (CASHING_OUT_TUNABLE_GET_CHALLENGE_DURATION() - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.modeTimer))
		
		
		IF iTimeRemaining > 30000
			timeColour = HUD_COLOUR_WHITE
		ELSE
			timeColour = HUD_COLOUR_RED
		ENDIF
		
		IF iTimeRemaining <= 0
			iOvertime = iTimeRemaining
			iTimeRemaining = 0
		ENDIF
		
		IF serverBD.sSortedLeaderboard[0].playerID != INVALID_PLAYER_INDEX()
		AND serverBD.sSortedLeaderboard[0].iScore > 0
			IF serverBD.sSortedLeaderboard[1].playerID != INVALID_PLAYER_INDEX()
			AND serverBD.sSortedLeaderboard[1].iScore > 0
				// Do help text explaining combined total
			ENDIF
		ENDIF
		
		IF serverBD.sSortedLeaderboard[0].iScore > 0
			iCombinedTotal += 1
			iCombinedTotal += serverBD.sSortedLeaderboard[0].iScore
		ENDIF
		IF serverBD.sSortedLeaderboard[1].iScore > 0
			iCombinedTotal += serverBD.sSortedLeaderboard[1].iScore
		ENDIF
		IF serverBD.sSortedLeaderboard[2].iScore > 0
			iCombinedTotal += serverBD.sSortedLeaderboard[2].iScore
		ENDIF
		IF serverBD.sSortedLeaderboard[3].iScore > 0
			iCombinedTotal += serverBD.sSortedLeaderboard[3].iScore
		ENDIF
		HUD_COLOURS MyScoreColour = HUD_COLOUR_PURE_WHITE
		IF GB_IS_THIS_PLAYER_USING_SPECTATE(PLAYER_ID())
			MyScoreColour = HUD_COLOUR_GREY
		ENDIF
		
		BOTTOM_RIGHT_UI_1STCASH_2NDCASH_3RDCASH_4THCASH_CASH1_CASH2_TIMER(	serverBD.sSortedLeaderboard[0].playerID,
																				serverBD.sSortedLeaderboard[1].playerID,
																				serverBD.sSortedLeaderboard[2].playerID,
																				serverBD.sSortedLeaderboard[3].playerID,
																				serverBD.sSortedLeaderboard[0].iScore,
																				serverBD.sSortedLeaderboard[1].iScore,
																				serverBD.sSortedLeaderboard[2].iScore,
																				serverBD.sSortedLeaderboard[3].iScore,
																				iCombinedTotal,
																				serverBD.leaderboardPlayerScore[PARTICIPANT_ID_TO_INT()],
																				iTimeRemaining, bForce, timeColour, "", "CARJCK_CSH1TTL", "GB_SNG_HT1",
																				DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, MyScoreColour)
	ENDIF
ENDPROC

PROC MAINTAIN_SORTED_PARTICIPANTS()

	IF localSyncID = serverBD.iServerSyncID
		EXIT
	ENDIF
	
	g_GBLeaderboardStruct.dpadVariables.eDpadVarType = DPAD_VAR_CASH
	
	INT i
	REPEAT NUM_MINI_LEADERBOARD_PLAYERS i
		g_GBLeaderboardStruct.challengeLbdStruct[i].playerID = serverBD.sSortedLeaderboard[i].playerID
		g_GBLeaderboardStruct.challengeLbdStruct[i].iScore = serverBD.sSortedLeaderboard[i].iScore
	ENDREPEAT
	
//	IF iMyLastScore != GET_PLAYER_CASH_COLLECTED(PARTICIPANT_ID_TO_INT())
//		PLAY_SOUND_FRONTEND(-1,)
//		iMyLastScore = GET_PLAYER_CASH_COLLECTED(PARTICIPANT_ID_TO_INT())
//	ENDIF
	
	IF g_GBLeaderboardStruct.challengeLbdStruct[0].playerID = PLAYER_ID()
		IF iMyLastPosition != 0 // If I've moved up leaderboard
		OR localSyncID = 0		// Or i was first to collect
			PLAY_SOUND_FRONTEND(-1, "Enter_1st", "GTAO_Magnate_Boss_Modes_Soundset", FALSE)
			iMyLastPosition = 0
		ENDIF
	ELSE
		IF iMyLastPosition = 0
			PLAY_SOUND_FRONTEND(-1, "Lose_1st", "GTAO_Magnate_Boss_Modes_Soundset", FALSE)
			iMyLastPosition = -1
		ENDIF
	ENDIF
	
	// Sync us with the server
	localSyncID = serverBD.iServerSyncID
	
	#IF IS_DEBUG_BUILD
	PRINTLN("[CASHING OUT LEADERBOARD UPDATE] [ST EXEC] [CLIENT] - MAINTAIN_SORTED_PARTICIPANTS - Performing a leaderboard update. LocalSyncID updated to ", localSyncID)
	INT iDebugLoop
	REPEAT GB_GET_MAX_GANG_SIZE_INCLUDING_BOSS()	iDebugLoop
		IF g_GBLeaderboardStruct.challengeLbdStruct[iDebugLoop].iParticipant > -1
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(g_GBLeaderboardStruct.challengeLbdStruct[iDebugLoop].iParticipant))
				PRINTLN("[CASHING OUT LEADERBOARD UPDATE] [ST EXEC] [CLIENT] - MAINTAIN_SORTED_PARTICIPANTS - |--------------------------- >| ")
				PRINTLN("[CASHING OUT LEADERBOARD UPDATE] [ST EXEC] [CLIENT] - MAINTAIN_SORTED_PARTICIPANTS - Position: ", iDebugLoop)
				PRINTLN("[CASHING OUT LEADERBOARD UPDATE] [ST EXEC] [CLIENT] - MAINTAIN_SORTED_PARTICIPANTS - Player: ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(g_GBLeaderboardStruct.challengeLbdStruct[iDebugLoop].iParticipant))))
				PRINTLN("[CASHING OUT LEADERBOARD UPDATE] [ST EXEC] [CLIENT] - MAINTAIN_SORTED_PARTICIPANTS - Score: ", g_GBLeaderboardStruct.challengeLbdStruct[iDebugLoop].iScore)
				PRINTLN("[CASHING OUT LEADERBOARD UPDATE] [ST EXEC] [CLIENT] - MAINTAIN_SORTED_PARTICIPANTS - |--------------------------- |< ")
			ENDIF
		ENDIF
	ENDREPEAT
	#ENDIF

ENDPROC

PROC MAINTAIN_DPAD_LEADERBOARD()
	DRAW_CHALLENGE_DPAD_LBD(g_GBLeaderboardStruct.challengeLbdStruct, g_GBLeaderboardStruct.siDpadMovie, SUB_BOSS, g_GBLeaderboardStruct.dpadVariables, 
						g_GBLeaderboardStruct.fmDpadStruct)
ENDPROC

PROC MAINTAIN_DISTANCE_CHECKS()
	IF GET_SERVER_GAME_STATE() = GAME_STATE_RUNNING
	AND serverBD.sSortedLeaderboard[0].iScore > 0
		GB_MAINTAIN_BOSS_WORK_DISTANT_CHECKS(FMMC_TYPE_GB_CASHING_OUT, GET_PLAYER_COORDS(PLAYER_ID()), bSet)
	ENDIF
ENDPROC

// ------------------------------------------------------------------|<

// Rewards ---------------------------------------------------------->|

PROC GIVE_LOCAL_PLAYER_ATM_CASH(INT iCashToGive)

	// THIS NEEDS TO CHANGE TO NOT BE A PICKUP
												
	IF USE_SERVER_TRANSACTIONS()
		PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - GIVE_LOCAL_PLAYER_ATM_CASH - PC BUILD - service transaction requested, value: ", iCashToGive)
		INT iTransactionSlot
		TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_CASHING_OUT, iCashToGive, iTransactionSlot)
	ELSE
		PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - GIVE_LOCAL_PLAYER_ATM_CASH - CONSOLE BUILD - cash given to player, value: ", iCashToGive)
		NETWORK_EARN_FROM_CASHING_OUT(iCashToGive)
	ENDIF
ENDPROC

PROC GIVE_END_CASH_TO_WINNER()
	IF NOT IS_SERVER_BIT0_SET(eSERVERBITSET0_CHAL_END_NO_CASH_COLLECTED)
		IF GET_WINNER() = PLAYER_ID()
			IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_TAKE_COMBINED_TOTAL)
//				INT i
				INT iTotalCash
				
//				REPEAT GB_MAX_GANG_SIZE_INCLUDING_BOSS_EXEC i
//					IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_NATIVE(PARTICIPANT_INDEX, i))
//						PARTICIPANT_INDEX PartID = INT_TO_NATIVE(PARTICIPANT_INDEX, i)
//						PLAYER_INDEX PlayerID = NETWORK_GET_PLAYER_INDEX(PartID)
//						
//						IF IS_THIS_PLAYER_IN_THE_GANG(PlayerID)
//							IF GET_PLAYER_CASH_COLLECTED(i) > 0
//								iTotalCash += GET_PLAYER_CASH_COLLECTED(i)
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDREPEAT
				
				IF serverBD.sSortedLeaderboard[0].iScore > 0
					iTotalCash += serverBD.sSortedLeaderboard[0].iScore
				ENDIF
				IF serverBD.sSortedLeaderboard[1].iScore > 0
					iTotalCash += serverBD.sSortedLeaderboard[1].iScore
				ENDIF
				IF serverBD.sSortedLeaderboard[2].iScore > 0
					iTotalCash += serverBD.sSortedLeaderboard[2].iScore
				ENDIF
				IF serverBD.sSortedLeaderboard[3].iScore > 0
					iTotalCash += serverBD.sSortedLeaderboard[3].iScore
				ENDIF
				
				IF iTotalCash > CASHING_OUT_TUNABLE_GET_MAX_COMBINED_TOTAL()
					iTotalCash = CASHING_OUT_TUNABLE_GET_MAX_COMBINED_TOTAL()
				ENDIF
				
				IF iTotalCash > 0
					sGangBossManageRewardsData.iExtraCash += iTotalCash
					PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - GIVE_END_CASH_TO_WINNER - sRewardsData.iExtraCash = $", iTotalCash)
				ELSE
					PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - GIVE_END_CASH_TO_WINNER - iTotalCash = 0, no cash being given to boss")
				ENDIF
				
				SET_LOCAL_BIT0(eLOCALBITSET0_TAKE_COMBINED_TOTAL)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_SAFE_FOR_EVENT_END_UI()
	IF IS_NET_PLAYER_OK(PLAYER_ID(), TRUE)
	AND NOT IS_RADAR_HIDDEN()
	AND HAS_PLAYER_FULLY_SPAWNED()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_END_REWARDS()
	
	BOOL bIWon
	
	IF GET_END_REASON() != eENDREASON_NO_REASON_YET
	
		IF MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI
			
			IF NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE()
			AND NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_PASSIVE()
			AND NOT GB_SHOULD_HIDE_GANG_BOSS_EVENT(FMMC_TYPE_GB_CASHING_OUT)
			
				IF IS_SAFE_FOR_EVENT_END_UI()
					
					SWITCH GET_END_REASON()
						CASE eENDREASON_NO_BOSS_LEFT
							#IF IS_DEBUG_BUILD
							IF bShowSpammyPrints
							PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - no boss left on script.")
							ENDIF
							#ENDIF
							
							// Display end shard here.
							DO_BIG_MESSAGE(eBIGM_BOSS_LEFT)
						BREAK
						CASE eENDREASON_ALL_GOONS_LEFT
							#IF IS_DEBUG_BUILD
							IF bShowSpammyPrints
							PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - no goons left on script")
							ENDIF
							#ENDIF
							
							// Display end shard here.
						BREAK
						CASE eENDREASON_TIME_UP
							#IF IS_DEBUG_BUILD
							IF bShowSpammyPrints
							PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - mode time expired")
							ENDIF
							#ENDIF
							
							// Display end shard here
							IF IS_SERVER_BIT0_SET(eSERVERBITSET0_CHAL_END_NO_CASH_COLLECTED)
								DO_BIG_MESSAGE(eBIGM_NO_CASH_STOLEN)
							ENDIF
							IF IS_SERVER_BIT0_SET(eSERVERBITSET0_CHAL_END_WITH_WINNER)
								IF PLAYER_ID() = GET_WINNER()
									DO_BIG_MESSAGE(eBIGM_YOU_WON)
								ELSE
									DO_BIG_MESSAGE(eBIGM_YOU_LOST)
								ENDIF
							ENDIF
						BREAK
						CASE eENDREASON_WIN_CONDITION_TRIGGERED
							#IF IS_DEBUG_BUILD
							IF bShowSpammyPrints
							PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - somebody won")
							ENDIF
							#ENDIF
							// Display end shard here
							// IF local player won set bIWon = TRUE here.
							
							
							IF IS_SERVER_BIT0_SET(eSERVERBITSET0_ALL_ATMS_ROBBED)
								IF PLAYER_ID() = GET_WINNER()
									DO_BIG_MESSAGE(eBIGM_YOU_WON)
								ELSE
									DO_BIG_MESSAGE(eBIGM_YOU_LOST)
								ENDIF
							ENDIF
						BREAK
					ENDSWITCH
				ENDIF	
					
				IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_CALCULATED_REWARDS)
				
					PLAYER_INDEX playerId = GET_WINNER() // Set the winning player here before passing into wanted cleanup function. Using PLAYER_ID() as placeholder right now.
					GANG_BOSS_MANAGE_WANTED_CLEAN_UP(TRUE, playerId)
					
					IF GET_WINNER() = PLAYER_ID()
						bIWon = TRUE
					ENDIF
					
					IF bIWon
						// Do any extra processing here for if the locla player won, before passing into GANG_BOSS_MANAGE_REWARDS
						// sGangBossManageRewardsData etc.
					ENDIF
					
					GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_CASHING_OUT, bIWon, sGangBossManageRewardsData)
					
					EXECUTE_COMMON_END_TELEMTRY(bIWon, FALSE)
					
					SET_LOCAL_BIT0(eLOCALBITSET0_CALCULATED_REWARDS)
				
				ENDIF	
								
			ENDIF			
		ENDIF	
	ENDIF
	
	IF GB_MAINTAIN_BOSS_END_UI(sEndUI)
		SET_CLIENT_BIT0(eCLIENTBITSET0_COMPLETED_REWARDS)
	ENDIF
	
ENDPROC

PROC RESET_CONTEXT()
	IF HAS_NET_TIMER_STARTED(localHackStartTimer)
		RESET_NET_TIMER(localHackStartTimer)
	ENDIF

	IF iContextStage != 0
		PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - RESET_CONTEXT - called")
		iContextStage = 0
	ENDIF
ENDPROC

PROC CLEANUP_CONTEXT(BOOL bCompletely = FALSE)
	IF bCompletely
		RESET_CONTEXT()
	ENDIF
	IF iHackContextIntention != (-1)
		PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - CLEANUP_CONTEXT - hack context intention released")
		RELEASE_CONTEXT_INTENTION(iHackContextIntention)
	ENDIF
ENDPROC

PROC PROCESS_HACKING()
	IF IS_PLAYER_NEAR_AN_ATM()
		IF NOT IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), eCLIENTBITSET0_NEAR_AN_ATM)
			SET_CLIENT_BIT0(eCLIENTBITSET0_NEAR_AN_ATM)
		ENDIF
		IF NOT IS_PLAYER_HACKING_ATM(PARTICIPANT_ID())
			SWITCH iContextStage
				CASE 0
					IF NOT HAS_NET_TIMER_STARTED(localHackStartTimer)
						START_NET_TIMER(localHackStartTimer)
					ELSE
						// Half a second delay to make sure players are stood still
						IF HAS_NET_TIMER_EXPIRED(localHackStartTimer, 500)
							// Register context intention
							CLEAR_HELP()
							iHackContextIntention = NEW_CONTEXT_INTENTION
							REGISTER_CONTEXT_INTENTION(iHackContextIntention, CP_MAXIMUM_PRIORITY, "GB_COUT_HP_01")
							PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - PROCESS_HACKING - Context intention registered")
							
							iContextStage++
						ENDIF
					ENDIF
				BREAK
				
				CASE 1
					// If its on screen and its pressed, move to stage 2
					IF iHackContextIntention != (-1)
						IF IS_CONTEXT_INTENTION_HELP_DISPLAYING(iHackContextIntention)
						AND HAS_CONTEXT_BUTTON_TRIGGERED(iHackContextIntention)
							PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - PROCESS_HACKING - Context button triggered")
							CLEANUP_CONTEXT()
							iContextStage++
						ENDIF
					ENDIF
				BREAK
				
				CASE 2
					IF NOT IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), eCLIENTBITSET0_HACKED_AN_ATM)
						PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - PROCESS_HACKING - eCLIENTBITSET0_HACKED_AN_ATM set")
						SET_CLIENT_BIT0(eCLIENTBITSET0_HACKED_AN_ATM)
					ENDIF
					
//					IF NOT IS_ENTITY_IN_STRIPCLUB(PLAYER_PED_ID())
//						TASK_USE_NEAREST_SCENARIO_TO_COORD(PLAYER_PED_ID(), GET_ENTITY_COORDS(PLAYER_PED_ID()), 5)
//					ENDIF
					
					// Store out the atm we're about to hack
					GET_CLOSEST_ATM_COORDS(iATM_beingHacked)
					
					// Set bit to indicate I am now hacking
					PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - PROCESS_HACKING - Player has pressed dpad right, setting eCLIENTBITSET0_HACKING_ATM")
					SET_CLIENT_BIT0(eCLIENTBITSET0_HACKING_ATM)
				BREAK
			ENDSWITCH
		ELSE
			CLEANUP_CONTEXT()
			
			// **********************
			// RUN THE HACKING GAME
			// **********************
			
			IF RUN_HACKING_MINI_GAME(eHackingOutcome, iHackTime)
			
				IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_QUALIFYING_PARTICIPANT)
					GB_SET_PLAYER_AS_QUALIFYING_PARTICIPANT()
					SET_LOCAL_BIT0(eLOCALBITSET0_QUALIFYING_PARTICIPANT)
				ENDIF
				
				INT iATM
				INT iATMValue
				SWITCH eHackingOutcome
					CASE eHACK_OUTCOME_SUCCESS
						PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - CLIENT_PROCESSING - Player has finished hacking. Outcome: eHACK_OUTCOME_SUCCESS")
					
						// Reward the player with cash
						iATMValue = GET_RANDOM_INT_IN_RANGE(iMinATMCash, (iMaxATMCash+1))
						playerBD[PARTICIPANT_ID_TO_INT()].iCashCollected += iATMValue
						playerBD[PARTICIPANT_ID_TO_INT()].iNumSuccessfulHacks++
						playerBD[PARTICIPANT_ID_TO_INT()].iTotalHacks++
						GIVE_LOCAL_PLAYER_ATM_CASH(iATMValue)
						
						// Apply wanted level if needed
						HANDLE_APPLY_WANTED_BASED_ON_HACK_TIME()
						
						// Broadcast to the server that the atm has been hacked
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							GET_CLOSEST_ATM_COORDS(iATM)
							BROADCAST_CASHING_OUT_UPDATE_BLOCKED_ATMS(NETWORK_GET_PLAYER_INDEX(NETWORK_GET_HOST_OF_THIS_SCRIPT()), iATM)
						ENDIF
						
						// Print the ticker to say how much you robbed from the ATM
						DO_ATM_ROB_TICKER(iATMValue)
						
						// Notify the player about wanted rating potential (only done once)
						DO_HELP_TEXT(eHELP_WANTED_LEVEL_APPLIED)
						
						IF NOT HAS_NET_TIMER_STARTED(EndOfHackTimer)
							START_NET_TIMER(EndOfHackTimer)
						ELSE
							RESET_NET_TIMER(EndOfHackTimer)
							START_NET_TIMER(EndOfHackTimer)
						ENDIF
						
						RESET_CONTEXT()
						
//						IF NOT IS_ENTITY_IN_STRIPCLUB(PLAYER_PED_ID())
//							SET_PED_SHOULD_PLAY_NORMAL_SCENARIO_EXIT(PLAYER_PED_ID())
//							CLEAR_PED_TASKS(PLAYER_PED_ID())
//						ENDIF
						
						iATM_beingHacked = -1
						
						// Player is no longer hacking an ATM
						CLEAR_CLIENT_BIT0(eCLIENTBITSET0_HACKING_ATM)
					BREAK
					
					CASE eHACK_OUTCOME_FAILURE
						PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - CLIENT_PROCESSING - Player has finished hacking. Outcome: eHACK_OUTCOME_FAILURE")
					
						playerBD[PARTICIPANT_ID_TO_INT()].iTotalHacks++
					
						// Give the player a wanted rating
						HANDLE_APPLY_WANTED_BASED_ON_HACK_TIME()
						
						// Broadcast to the server that the atm has been hacked
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							GET_CLOSEST_ATM_COORDS(iATM)
							BROADCAST_CASHING_OUT_UPDATE_BLOCKED_ATMS(NETWORK_GET_PLAYER_INDEX(NETWORK_GET_HOST_OF_THIS_SCRIPT()), iATM)
						ENDIF
						
						// Notify the player about wanted rating potential (only done once)
						DO_HELP_TEXT(eHELP_WANTED_LEVEL_APPLIED)
						
						IF NOT HAS_NET_TIMER_STARTED(EndOfHackTimer)
							START_NET_TIMER(EndOfHackTimer)
						ELSE
							RESET_NET_TIMER(EndOfHackTimer)
							START_NET_TIMER(EndOfHackTimer)
						ENDIF
					
						RESET_CONTEXT()
						
//						IF NOT IS_ENTITY_IN_STRIPCLUB(PLAYER_PED_ID())
//							SET_PED_SHOULD_PLAY_NORMAL_SCENARIO_EXIT(PLAYER_PED_ID())
//							CLEAR_PED_TASKS(PLAYER_PED_ID())
//						ENDIF
						
						iATM_beingHacked = -1
					
						// Player is no longer hacking an ATM
						CLEAR_CLIENT_BIT0(eCLIENTBITSET0_HACKING_ATM)
					BREAK
					
					CASE eHACK_OUTCOME_ABANDON
						PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - CLIENT_PROCESSING - Player has finished hacking. Outcome: eHACK_OUTCOME_ABANDON")
					
						// Give the player a wanted rating
						HANDLE_APPLY_WANTED_BASED_ON_HACK_TIME()
												
						// Notify the player about wanted rating potential (only done once)
						DO_HELP_TEXT(eHELP_WANTED_LEVEL_APPLIED)
						
						IF NOT HAS_NET_TIMER_STARTED(EndOfHackTimer)
							START_NET_TIMER(EndOfHackTimer)
						ELSE
							RESET_NET_TIMER(EndOfHackTimer)
							START_NET_TIMER(EndOfHackTimer)
						ENDIF
					
						RESET_CONTEXT()
						
//						IF NOT IS_ENTITY_IN_STRIPCLUB(PLAYER_PED_ID())
//							SET_PED_SHOULD_PLAY_NORMAL_SCENARIO_EXIT(PLAYER_PED_ID())
//							CLEAR_PED_TASKS(PLAYER_PED_ID())
//						ENDIF
						
						iATM_beingHacked = -1
					
						// Player is no longer hacking an ATM
						CLEAR_CLIENT_BIT0(eCLIENTBITSET0_HACKING_ATM)
					BREAK
					
					CASE eHACK_OUTCOME_KILLED
						PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - CLIENT_PROCESSING - Player has finished hacking. Outcome: eHACK_OUTCOME_ABANDON OR eHACK_OUTCOME_KILLED")
					
						RESET_CONTEXT()
						
						iATM_beingHacked = -1
					
						// Unsure, ask Scott
						CLEAR_CLIENT_BIT0(eCLIENTBITSET0_HACKING_ATM)
					BREAK
					
					CASE eHACK_OUTCOME_UNKNOWN
						PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - CLIENT_PROCESSING - Player has finished hacking. Outcome: eHACK_OUTCOME_UNKNOWN")
						SCRIPT_ASSERT("[GB CASHING OUT] - Unknown hacking outcome, bug for Steve Tiley")
						DEBUG_PRINTCALLSTACK()
						
						// Cleanup and force quit just in case
						FORCE_QUIT_FAIL_HACKING_MINIGAME(hackCashingOut)
			
						// Run hacking cleanup
						CLEANUP_HACKING()
						
						RESET_CONTEXT()
						
						iATM_beingHacked = -1
						
						CLEAR_CLIENT_BIT0(eCLIENTBITSET0_HACKING_ATM)
					BREAK
				ENDSWITCH
			ENDIF
		ENDIF
	ELSE
		HANDLE_HACKING_CLEANUP()
	
		IF IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), eCLIENTBITSET0_NEAR_AN_ATM)
			CLEAR_CLIENT_BIT0(eCLIENTBITSET0_NEAR_AN_ATM)
		ENDIF
		
		IF HAS_NET_TIMER_STARTED(localHackStartTimer)
			RESET_NET_TIMER(localHackStartTimer)
		ENDIF
		
		// Clear the press context help
		CLEANUP_CONTEXT(TRUE)
	ENDIF
ENDPROC

// ------------------------------------------------------------------|<

FUNC BOOL INIT_CLIENT()
	
	INT iGame = GET_RANDOM_INT_IN_RANGE(0, MAX_NUM_HACKING_MG)
	
	IF NOT IS_MINIGAME_DISABLED(iGame)
		iMyHackMG = iGame
		iHackingProg = 0
		iMinATMCash = CASHING_OUT_TUNABLE_GET_MIN_ATM_CASH()
		iMaxATMCash = CASHING_OUT_TUNABLE_GET_MAX_ATM_CASH()
		
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

PROC CLIENT_PROCESSING()
	
	SWITCH GET_MODE_STATE()
	
		CASE eMODESTATE_INIT
			// ********************************************************************
			// Server says when omode has intialised and moves us onto run state.
			// ********************************************************************
		BREAK
		
		CASE eMODESTATE_RUN
			
			// ***********************
			// Do common start calls
			// ***********************
			
			IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_CALLED_COMMON_SETUP)
				GB_COMMON_BOSS_MISSION_SETUP(FMMC_TYPE_GB_CASHING_OUT)
				SET_LOCAL_BIT0(eLOCALBITSET0_CALLED_COMMON_SETUP)
			ELSE
				IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_CALLED_SAFE_FOR_SPEC_CAM_SETUP)
					IF HAS_NET_TIMER_EXPIRED(serverBD.modeTimer, TRACK_GOON_DELAY)
						GB_SET_ITS_SAFE_FOR_SPEC_CAM()
						SET_LOCAL_BIT0(eLOCALBITSET0_CALLED_SAFE_FOR_SPEC_CAM_SETUP)
					ENDIF
				ENDIF
			ENDIF
			
			// *******************************************
			// Main mode logic here, after setup is done
			// *******************************************
			
			IF NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE()
			AND NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_PASSIVE()
			// AND NOT GB_SHOULD_HIDE_GANG_BOSS_EVENT(// Add your mode here)
				
				IF GET_END_REASON() = eENDREASON_NO_REASON_YET
					
					IF MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI
						
						IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_CALLED_START_COMMON_TELEMETRY)
							
							GB_SET_COMMON_TELEMETRY_DATA_ON_START()
							SET_LOCAL_BIT0(eLOCALBITSET0_CALLED_START_COMMON_TELEMETRY)
							PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - [TELEMETRY] - called GB_SET_COMMON_TELEMETRY_DATA_ON_START()")
						ENDIF
						
						// ********************************************
						// Run main mode logic here.
						// ********************************************
						
						MAINTAIN_SORTED_PARTICIPANTS()
						
						MAINTAIN_DPAD_LEADERBOARD()
						
						MAINTAIN_BLIPS()
								
						MAINTAIN_OBJECTIVE_TEXT()
						
						MAINTAIN_COUNTDOWN_MUSIC()
						
						MAINTAIN_BOTTOM_RIGHT_UI()
						
						HANDLE_LOSE_CASH_UPON_DEATH()
						
						SWITCH GET_CLIENT_MISSION_STAGE(PARTICIPANT_ID_TO_INT())
							CASE eMS_INIT
								// Do mode setup here
								
								IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_MODE_SETUP)
									GB_SET_LOCAL_PLAYER_CRITICAL_TO_JOB(TRUE)
									SET_FREEMODE_FLOW_COMMS_DISABLED(TRUE)
									SET_LOCAL_BIT0(eLOCALBITSET0_MODE_SETUP)
									START_NET_TIMER(EndOfHackTimer)
								ENDIF
								
								SET_CLIENT_MISSION_STAGE(eMS_COLLECT)
							BREAK
							
							CASE eMS_COLLECT
								DO_BIG_MESSAGE(eBIGM_INTRO)
								DO_HELP_TEXT(eHELP_INTRO)
								PROCESS_HACKING()
							BREAK
						ENDSWITCH
						
					ENDIF
					
				ENDIF
				
			ELSE
			
				Clear_Any_Objective_Text_From_This_Script() // Clear objective text if player is retricted for hide or passive.
				
			ENDIF
			
		BREAK
		
		CASE eMODESTATE_REWARDS
			
			IF GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID())
				GB_SET_LOCAL_PLAYER_CRITICAL_TO_JOB(FALSE)
			ENDIF
			
			IF GB_SHOULD_RUN_SPEC_CAM()
				PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - eMODESTATE_REWARDS - Boss - Player is spectating, calling GB_SET_SHOULD_CLOSE_SPECTATE")
				GB_SET_SHOULD_CLOSE_SPECTATE()
			ENDIF
			
			REMOVE_ALL_ATM_BLIPS()
			Clear_Any_Objective_Text_From_This_Script() // Clear god text, mode is over.
			
			CLEANUP_CONTEXT(TRUE)
			
			HANDLE_HACKING_CLEANUP()
			
			GIVE_END_CASH_TO_WINNER()
			PROCESS_END_REWARDS() // Handle end of mode rewards.
			MAINTAIN_DPAD_LEADERBOARD()
			
			// Server will move us onto end state once all end of mode logic is complete.
			
		BREAK
		
		CASE eMODESTATE_END
			
		BREAK
		
	ENDSWITCH

ENDPROC


PROC SORT_PLAYER_SCORES(PLAYER_INDEX playerID, INT iPlayerScore)
	SORT_AMBIENT_ACTIVITY_LEADERBOARD(serverBD.sSortedLeaderboard, playerID, iPlayerScore, AAL_SORT_NON_UNIFORM_SCORING)
	
	serverBD.iServerSyncID++
ENDPROC

PROC HANDLE_PLAYER_LEAVING_LEADERBOARD_UPDATE()
	IF MAINTAIN_PLAYER_LEAVING_AMBIENT_ACTIVITY_LEADERBOARD(serverBD.sSortedLeaderboard, serverStaggeredPlayerLoop, AAL_SORT_NON_UNIFORM_SCORING)
		serverBD.iServerSyncId++
	ENDIF
ENDPROC

PROC HANDLE_LEADERBOARD_UPDATES()
	INT iLeaderboardStagger
	REPEAT GB_MAX_GANG_SIZE_INCLUDING_BOSS_EXEC iLeaderboardStagger
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iLeaderboardStagger))
			PLAYER_INDEX PlayerID = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iLeaderboardStagger))
			IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(PlayerID, GET_GANG_BOSS_PLAYER_INDEX())
				IF serverBD.leaderboardPlayerScore[iLeaderboardStagger] != GET_PLAYER_CASH_COLLECTED(iLeaderboardStagger)
					PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] [SERVER] - HANDLE_LEADERBOARD_UPDATES - Player ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iLeaderboardStagger))), "'s score has changed, updating leaderboard")
					SORT_PLAYER_SCORES(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iLeaderboardStagger)), GET_PLAYER_CASH_COLLECTED(iLeaderboardStagger))
					serverBD.leaderboardPlayerScore[iLeaderboardStagger] = GET_PLAYER_CASH_COLLECTED(iLeaderboardStagger)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC


PROC PROCESS_PLAYER_JOIN(INT iCount)
	
	STRUCT_PLAYER_SCRIPT_EVENTS data 
	
	GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, data, SIZE_OF(data))

	// Check player is valid
	IF data.PlayerIndex != INVALID_PLAYER_INDEX()

		// If we haven't yet had knowledge of this player then loop over threads ensuring they have joined this script
		IF NOT IS_BIT_SET(serverBD.iPlayerBitSet, NATIVE_TO_INT(data.PlayerIndex))
			INT iThread
			REPEAT data.NumThreads iThread

				IF data.Threads[iThread] = GET_ID_OF_THIS_THREAD()
					
					SORT_PLAYER_SCORES(data.PlayerIndex, 0)	
					
					SET_BIT(serverBD.iPlayerBitSet, NATIVE_TO_INT(data.PlayerIndex))
				ENDIF       
			ENDREPEAT
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL HAS_END_CONDITION_BEEN_MET()

	IF HAVE_ALL_ATMS_BEEN_ROBBED()
		PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - HAS_END_CONDITION_BEEN_MET - TRUE - All ATMs have been robbed")
		RETURN TRUE
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF IS_SERVER_DEBUG_BIT0_SET(eSERVERDEBUGBITSET0_PLAYER_S_PASSED)
		PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - HAS_END_CONDITION_BEEN_MET - TRUE - DEBUG - Player has S Passed")
		RETURN TRUE
	ENDIF
	
	IF IS_SERVER_DEBUG_BIT0_SET(eSERVERDEBUGBITSET0_PLAYER_F_FAILED)
		PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - HAS_END_CONDITION_BEEN_MET - TRUE - DEBUG - Player has F Failed")
		RETURN TRUE
	ENDIF
	#ENDIF

	RETURN FALSE
ENDFUNC

PROC HANDLE_BOSS_LEAVE_SESSION()
	IF NOT IS_SERVER_BIT0_SET(eSERVERBITSET0_BOSS_LEFT)
		IF NOT NETWORK_IS_PLAYER_ACTIVE(GET_GANG_BOSS_PLAYER_INDEX())
			SET_SERVER_BIT0(eSERVERBITSET0_BOSS_LEFT)
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_REMAINING_ATMS()
	IF NOT IS_SERVER_BIT0_SET(eSERVERBITSET0_ALL_ATMS_ROBBED)
		IF serverBD.iATMsRobbed = MAX_NUM_ATMs
			SET_SERVER_BIT0(eSERVERBITSET0_ALL_ATMS_ROBBED)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL DETERMINE_WINNER()
	
	#IF IS_DEBUG_BUILD
	IF IS_SERVER_DEBUG_BIT0_SET(eSERVERDEBUGBITSET0_PLAYER_F_FAILED)
		PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - DETERMINE_WINNER - DEBUG - Player has F Failed, setting eSERVERBITSET0_CHAL_END_NO_CASH_COLLECTED")
		SET_SERVER_BIT0(eSERVERBITSET0_CHAL_END_NO_CASH_COLLECTED)
		RETURN TRUE
	ENDIF
	#ENDIF
	
	INT i
	INT iHighScore = 0
	REPEAT GB_MAX_GANG_SIZE_INCLUDING_BOSS_EXEC i
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
			PLAYER_INDEX Player = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
			IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(Player, GET_GANG_BOSS_PLAYER_INDEX())
				IF GET_PLAYER_CASH_COLLECTED(i) > iHighScore
					iHighScore = GET_PLAYER_CASH_COLLECTED(i)
					serverBD.piWinner = Player
					serverBD.iWinningScore = iHighScore
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	REPEAT GB_MAX_GANG_SIZE_INCLUDING_BOSS_EXEC i
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
			PLAYER_INDEX Player = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
			IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(Player, GET_GANG_BOSS_PLAYER_INDEX())
				IF GET_PLAYER_CASH_COLLECTED(i) = 0
					IF Player != serverBD.piWinner
						SORT_PLAYER_SCORES(Player, 0)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF iHighScore = 0
		PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - DETERMINE_WINNER - No players collected cash, setting eSERVERBITSET0_CHAL_END_NO_CASH_COLLECTED")
		SET_SERVER_BIT0(eSERVERBITSET0_CHAL_END_NO_CASH_COLLECTED)
	ELSE	
		PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - DETERMINE_WINNER - ", GET_PLAYER_NAME(serverBD.piWinner), " won! Setting eSERVERBITSET0_CHAL_END_WITH_WINNER")
		SET_SERVER_BIT0(eSERVERBITSET0_CHAL_END_WITH_WINNER)
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL HAVE_ALL_PLAYERS_COMPLETED_REWARDS()
	INT i
	REPEAT GB_MAX_GANG_SIZE_INCLUDING_BOSS_EXEC i
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
			PARTICIPANT_INDEX PartID = INT_TO_PARTICIPANTINDEX(i)
			PLAYER_INDEX PlayerID = NETWORK_GET_PLAYER_INDEX(PartID)
			
			IF IS_THIS_PLAYER_IN_THE_GANG(PlayerID)
				IF NOT IS_CLIENT_BIT0_SET(PartID, eCLIENTBITSET0_COMPLETED_REWARDS)
					RETURN FALSE
				ENDIF
			ENDIF
			
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

FUNC BOOL INIT_SERVER()
	
	INT iPlayer
	REPEAT GB_MAX_GANG_SIZE_INCLUDING_BOSS_EXEC iPlayer
		serverBD.leaderboardPlayerScore[iPlayer] = -1
	ENDREPEAT
	
	serverBD.piGangBoss = PLAYER_ID()
	serverBD.piWinner = INVALID_PLAYER_INDEX()
	
	// Initialise the leaderboard
	INITIALISE_AMBIENT_ACTIVITY_LEADERBOARD(serverBD.sSortedLeaderboard)
	
	SORT_PLAYER_SCORES(PLAYER_ID(), 0)

	RETURN TRUE
ENDFUNC

PROC SERVER_PROCESSING()
	
	SWITCH GET_MODE_STATE()
	
		CASE eMODESTATE_INIT
			
			// ********************************************
			// Initialise server data here.
			// Will remain in here until server moves on.
			// ********************************************
			
			// Move onto next stage.
			SET_MODE_STATE(eMODESTATE_RUN)
			
			// Get MatchIds for Telemetry
			PLAYSTATS_CREATE_MATCH_HISTORY_ID_2(serverBD.iMatchId1, serverBD.iMatchId2)
			
		BREAK
		
		CASE eMODESTATE_RUN
			
			// Stay in run state until we have an end reason.
			IF GET_END_REASON() = eENDREASON_NO_REASON_YET
				
				// Widget for forcing time up at end of mode.
				#IF IS_DEBUG_BUILD
				IF serverBd.bFakeEndOfModeTimer
					PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] serverBd.bFakeEndOfModeTimer = TRUE.")
					SET_END_REASON(eENDREASON_TIME_UP)
				ENDIF
				#ENDIF
				
				// If the mode timer expires, set end reason to time up.
				IF NOT HAS_NET_TIMER_STARTED(serverBd.modeTimer)
					START_NET_TIMER(serverBd.modeTimer)
				ELSE
				
					// *******************************
					// Handle main server logic here
					// *******************************
					
					HANDLE_PLAYER_LEAVING_LEADERBOARD_UPDATE()
					HANDLE_LEADERBOARD_UPDATES()
					HANDLE_BOSS_LEAVE_SESSION()
					MAINTAIN_REMAINING_ATMS()
				
					IF HAS_END_CONDITION_BEEN_MET()
						IF DETERMINE_WINNER()
							SET_END_REASON(eENDREASON_WIN_CONDITION_TRIGGERED)
						ENDIF
					ENDIF
					
					IF IS_SERVER_BIT0_SET(eSERVERBITSET0_BOSS_LEFT)
						SET_END_REASON(eENDREASON_NO_BOSS_LEFT)
					ENDIF
					
					IF HAS_NET_TIMER_EXPIRED(serverBd.modeTimer, CASHING_OUT_TUNABLE_GET_CHALLENGE_DURATION())
						IF DETERMINE_WINNER()
							SET_END_REASON(eENDREASON_TIME_UP)
						ENDIF
					ENDIF
				ENDIF
				
			ELSE
				
				// Move onto rewards once we have an end reason (therefore mode has ended). 
				SET_MODE_STATE(eMODESTATE_REWARDS)
				
			ENDIF
			
		BREAK
		
		CASE eMODESTATE_REWARDS
			
			// Once all clients have handled the end of the mode, allow the server to move to the end. 
			IF HAVE_ALL_PLAYERS_COMPLETED_REWARDS()
				PRINTLN("[KINGCASTLE] - [REWARDS] - all participants completed rewards.")
				SET_MODE_STATE(eMODESTATE_END)
			ENDIF
			
		BREAK
		
		CASE eMODESTATE_END
			// Do anything required at very end of mode once rewards and gameplay are opver, then end.
			SET_SERVER_GAME_STATE(GAME_STATE_END)
		BREAK
		
	ENDSWITCH

ENDPROC
#IF IS_DEBUG_BUILD
PROC MAINTAIN_DEBUG_END_CHECKS()
	
	INT iParticipant
	PLAYER_INDEX player
	PARTICIPANT_INDEX part
		
	// Refill data.
	REPEAT GB_MAX_GANG_SIZE_INCLUDING_BOSS_EXEC iParticipant
		
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_NATIVE(PARTICIPANT_INDEX, iParticipant))
			part = INT_TO_NATIVE(PARTICIPANT_INDEX, iParticipant)
			player = NETWORK_GET_PLAYER_INDEX(part)
			
			IF IS_NET_PLAYER_OK(player, FALSE)
				
				IF NOT IS_SERVER_DEBUG_BIT0_SET(eSERVERDEBUGBITSET0_PLAYER_S_PASSED)
				AND NOT IS_SERVER_DEBUG_BIT0_SET(eSERVERDEBUGBITSET0_PLAYER_F_FAILED)
					IF IS_THIS_PLAYER_A_GANG_GOON(player)
					OR IS_THIS_PLAYER_THE_GANG_BOSS(player)
				
						IF IS_CLIENT_DEBUG_BIT0_SET(part, eCLIENTDEBUGBITSET0_I_HAVE_S_PASSED)
							SET_SERVER_DEBUG_BIT0(eSERVERDEBUGBITSET0_PLAYER_S_PASSED)
							PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] player ", GET_PLAYER_NAME(player), " has S passed. Setting eSERVERDEBUGBITSET0_PLAYER_S_PASSED")
						ENDIF 
						
						IF IS_CLIENT_DEBUG_BIT0_SET(part, eCLIENTDEBUGBITSET0_I_HAVE_F_FAILED)
							SET_SERVER_DEBUG_BIT0(eSERVERDEBUGBITSET0_PLAYER_F_FAILED)
							PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] player ", GET_PLAYER_NAME(player), " has F failed. Setting eSERVERDEBUGBITSET0_PLAYER_F_FAILED")
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF
		ENDIF
	ENDREPEAT
	
ENDPROC
#ENDIF
// -----------------------------------------------------------------------------------------------------------
//		Mission Cleanup
// -----------------------------------------------------------------------------------------------------------

FUNC BOOL IS_SAFE_TO_CLEANUP()
	IF AM_I_THE_GANG_BOSS()
		SWITCH iCleanupBossSpecStep
			CASE 0
				IF GB_SHOULD_RUN_SPEC_CAM()
					PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - IS_SAFE_TO_CLEANUP - Boss - Player is spectating, calling GB_SET_SHOULD_CLOSE_SPECTATE, moving to next stage")
					GB_SET_SHOULD_CLOSE_SPECTATE()
					iCleanupBossSpecStep = 1
				ELSE
					iCleanupBossSpecStep = 2
				ENDIF
			BREAK
			
			CASE 1
				IF NOT GB_SHOULD_RUN_SPEC_CAM()
					PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - IS_SAFE_TO_CLEANUP - Boss - NOT GB_SET_SHOULD_CLOSE_SPECTATE, moving to next stage")
					iCleanupBossSpecStep = 2
				ENDIF
			BREAK
			
			CASE 2
				PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - IS_SAFE_TO_CLEANUP - Boss - Return true")
				RETURN TRUE
			BREAK
		ENDSWITCH
	ELSE
		PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - IS_SAFE_TO_CLEANUP - Goon - Return true")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SCRIPT_CLEANUP(BOOL bImmediately = FALSE)
	IF bImmediately
	OR IS_SAFE_TO_CLEANUP()

		PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] SCRIPT_CLEANUP")
		
		CLEANUP_CONTEXT(TRUE)
		
		HANDLE_HACKING_CLEANUP()
		
		IF NETWORK_IS_GAME_IN_PROGRESS()
		AND GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID())
			GB_SET_LOCAL_PLAYER_CRITICAL_TO_JOB(FALSE)
		ENDIF
		
		HIDE_ALL_SHOP_BLIPS(FALSE)
		
		g_sGb_Telemetry_data.sdata.m_match1 = serverBD.iMatchId1
		g_sGb_Telemetry_data.sdata.m_match2 = serverBD.iMatchId2
		PROCESS_CURRENT_BOSS_CHALLENGE_PLAYSTATS(	g_sGb_Telemetry_data.sdata,
													playerBD[PARTICIPANT_ID_TO_INT()].iNumSuccessfulHacks,	//	iOptional0 = machinesHacked successfully
													playerBD[PARTICIPANT_ID_TO_INT()].iTotalHacks,			//	iOptional1 = total hacked
													playerBD[PARTICIPANT_ID_TO_INT()].iHighestWantedRating)	//	iOptional2 = wantedLevelReached
		PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - [TELEMETRY] - called PROCESS_CURRENT_BOSS_CHALLENGE_PLAYSTATS")
		
		GB_COMMON_BOSS_MISSION_CLEANUP()	
		
		Clear_Any_Objective_Text_From_This_Script()
		
		GB_TIDYUP_SPECTATOR_CAM()
		
		TERMINATE_THIS_THREAD()
	ENDIF
ENDPROC

FUNC BOOL PROCESS_PRE_GAME(MP_MISSION_DATA missionScriptArgs)

	PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] PROCESS_PRE_GAME")
	
	// This marks the script as a net script, and handles any instancing setup. 
	SETUP_MP_MISSION_FOR_NETWORK(GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(eAM_CHAL_CASHOUT), missionScriptArgs)
	
	//Reserve Entities
	RESERVE_NETWORK_MISSION_OBJECTS(GB_GET_BOSS_MISSION_NUM_OBJ_REQUIRED(FMMC_TYPE_GB_CASHING_OUT))
	RESERVE_NETWORK_MISSION_PEDS(GB_GET_BOSS_MISSION_NUM_PEDS_REQUIRED(FMMC_TYPE_GB_CASHING_OUT))
	RESERVE_NETWORK_MISSION_VEHICLES(GB_GET_BOSS_MISSION_NUM_VEH_REQUIRED(FMMC_TYPE_GB_CASHING_OUT))
	// This makes sure the net script is active, waits until it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD,SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD,SIZE_OF(playerBD))
	
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	
	GB_COMMON_BOSS_MISSION_PREGAME()
	SET_MAX_WANTED_LEVEL(g_sMPTunables.iexec_cashing_max_wanted_level)
	PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] SET_MAX_WANTED_LEVEL(", g_sMPTunables.iexec_cashing_max_wanted_level, ")")
	SET_BIT(g_GB_WORK_VARS.iEventBitSet, ciGB_WORK_WANTED_SUPRESSED_BY_EVENT)
	
	int HackingInt = 0
	
	FOR HackingInt = 0 to(MAX_SOUNDS - 1)
		sHacking[HackingInt] = -1 
		PRINTLN("[HACKING] RESETTING SOUND sHacking[", HackingInt,"]")
	ENDFOR
	
	RETURN TRUE
	
ENDFUNC

PROC PROCESS_ATM_BLOCKED_UPDATE(INT iCount)
	STRUCT_SCRIPT_EVENT_CASHING_OUT_UPDATE_BLOCKED_ATMS sEvent
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sEvent, SIZE_OF(sEvent))
		IF NOT IS_ATM_BIT_SET(sEvent.iBlockedATM)
			PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - PROCESS_ATM_BLOCKED_UPDATE - atm ", sEvent.iBlockedATM, " has been blocked by ", GET_PLAYER_NAME(sEvent.Details.FromPlayerIndex), ", setting as blocked")
			SET_ATM_BIT(sEvent.iBlockedATM)
			serverBD.iATMsRobbed++
			BROADCAST_CASHING_OUT_UPDATE_ATM_BLIPS(ALL_PLAYERS_ON_SCRIPT(TRUE), sEvent.iBlockedATM)
		ELSE
			PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - PROCESS_ATM_BLOCKED_UPDATE - atm ", sEvent.iBlockedATM, " has already been blocked - bug for Steve Tiley")
		ENDIF
		
		PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - PROCESS_ATM_BLOCKED_UPDATE - Event has been processed successfully")
	ELSE
		PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - PROCESS_ATM_BLOCKED_UPDATE - failed to get event data")
	ENDIF
ENDPROC

PROC PROCESS_UPDATE_ATM_BLIP(INT iCount)
	STRUCT_SCRIPT_EVENT_CASHING_OUT_UPDATE_ATM_BLIPS sEvent
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sEvent, SIZE_OF(sEvent))
		REMOVE_ATM_BLIP(sEvent.iATM)
		PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - PROCESS_UPDATE_ATM_BLIP - Event has been processed successfully")
	ELSE
		PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - PROCESS_UPDATE_ATM_BLIP - failed to get event data")
	ENDIF
ENDPROC

PROC PROCESS_EVENTS()

    INT iCount
    EVENT_NAMES ThisScriptEvent
	STRUCT_EVENT_COMMON_DETAILS Details
   
    REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iCount

        ThisScriptEvent = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iCount)
       	
        SWITCH ThisScriptEvent
            
			CASE EVENT_NETWORK_PLAYER_JOIN_SCRIPT
				IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
					PROCESS_PLAYER_JOIN(iCount)
				ENDIF
			BREAK
			
			CASE EVENT_NETWORK_SCRIPT_EVENT
				
				GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, Details, SIZE_OF(Details))
				
				SWITCH Details.Type
					
					 CASE SCRIPT_EVENT_CASHING_OUT_UPDATE_BLOCKED_ATMS
					 	PROCESS_ATM_BLOCKED_UPDATE(iCount)
					 BREAK
					 
					 CASE SCRIPT_EVENT_CASHING_OUT_UPDATE_ATM_BLIP
					 	PROCESS_UPDATE_ATM_BLIP(iCount)
					 BREAK
				ENDSWITCH
				
			BREAK
			
			CASE EVENT_NETWORK_DAMAGE_ENTITY
			
				//SWITCH Details.Type
					// Damage events.
				//ENDSWITCH
				
			BREAK
			
        ENDSWITCH
            
    ENDREPEAT
    
ENDPROC

#IF IS_DEBUG_BUILD

PROC DEBUG_WARP_TO_NEAREST_ATM()
	INT iATM
	VECTOR vClosestATM = GET_CLOSEST_UNBLOCKED_ATM(iATM)
	
	// Only warp if more than 25m away
	IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vClosestATM) > 25
	
		SCRIPT_EVENT_DATA_TICKER_MESSAGE sTicker
		sTicker.TickerEvent = TICKER_EVENT_PLAYER_CHEATED_WARP
	
		IF NET_WARP_TO_COORD(vClosestATM + <<0.0,0.0,0.5>>, 0.0, FALSE, FALSE, FALSE, FALSE, TRUE, TRUE)
			PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] [DEBUG] - DEBUG_WARP_TO_NEAREST_ATM - Player has warped to the nearest atm")
			
			BROADCAST_TICKER_EVENT(sTicker, ALL_PLAYERS_ON_SCRIPT())
		ENDIF
	ENDIF
ENDPROC

PROC DEBUG_WARP_TO_NEXT_ATM()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		// Get closest
		INT iATM
		VECTOR vTemp = GET_CLOSEST_ATM_COORDS(iATM)
		
		// move to next in list
		iATM += 1
		
		// move to start of list if at end
		IF iATM = MAX_NUM_ATMs
			iATM = 0
		ENDIF
		
		vTemp = GET_ATM_COORDS(iATM)
		
		SCRIPT_EVENT_DATA_TICKER_MESSAGE sTicker
		sTicker.TickerEvent = TICKER_EVENT_PLAYER_CHEATED_WARP

		IF NET_WARP_TO_COORD(vTemp + <<0.0,0.0,0.5>>, 0.0, FALSE, FALSE, FALSE, FALSE, TRUE, TRUE)
			PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] [DEBUG] - DEBUG_WARP_TO_NEXT_ATM - Player has warped to the next atm")
			
			BROADCAST_TICKER_EVENT(sTicker, ALL_PLAYERS_ON_SCRIPT())
		ENDIF
	ELSE
		PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] [DEBUG] - DEBUG_WARP_TO_NEXT_ATM - Ped is injured")
	ENDIF
ENDPROC

PROC MAINTAIN_J_SKIPS()

	// End the mission, S Pass will end the mode normally, making the player with the most robbed cash the winner
	// F Failing will end the mode as if no one stole any cash, making no one the winner
	IF NOT IS_CLIENT_DEBUG_BIT0_SET(PARTICIPANT_ID(), eCLIENTDEBUGBITSET0_I_HAVE_S_PASSED)
	AND NOT IS_CLIENT_DEBUG_BIT0_SET(PARTICIPANT_ID(), eCLIENTDEBUGBITSET0_I_HAVE_F_FAILED)
	
		// Pass
		IF IS_DEBUG_KEY_JUST_RELEASED(KEY_S, KEYBOARD_MODIFIER_NONE, "S Pass")
			PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] [DEBUG] - MAINTAIN_J_SKIPS - KEY_S, KEYBOARD_MODIFIER_NONE just released - S Pass initiated")
			SCRIPT_EVENT_DATA_TICKER_MESSAGE sTicker
			sTicker.TickerEvent = TICKER_EVENT_PLAYER_CHEATED_SPASS
			sTicker.playerID = PLAYER_ID()
			BROADCAST_TICKER_EVENT(sTicker, ALL_PLAYERS_ON_SCRIPT())
			SET_CLIENT_DEBUG_BIT0(eCLIENTDEBUGBITSET0_I_HAVE_S_PASSED)
		ENDIF
		
		// Fail
		IF IS_DEBUG_KEY_JUST_RELEASED(KEY_F, KEYBOARD_MODIFIER_NONE, "F Fail")
			PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] [DEBUG] - MAINTAIN_J_SKIPS - KEY_F, KEYBOARD_MODIFIER_NONE just released - F Fail initiated")
			SCRIPT_EVENT_DATA_TICKER_MESSAGE sTicker
			sTicker.TickerEvent = TICKER_EVENT_PLAYER_CHEATED_FFAIL
			sTicker.playerID = PLAYER_ID()
			BROADCAST_TICKER_EVENT(sTicker, ALL_PLAYERS_ON_SCRIPT())
			SET_CLIENT_DEBUG_BIT0(eCLIENTDEBUGBITSET0_I_HAVE_F_FAILED)
		ENDIF
	ENDIF
	
	// Warp player to the nearest ATM
	IF IS_DEBUG_KEY_JUST_RELEASED(KEY_J, KEYBOARD_MODIFIER_NONE, "J Skip")
		PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] [DEBUG] - MAINTAIN_J_SKIPS - KEY_J, KEYBOARD_MODIFIER_NONE just released - J Skip to closest unblocked atm initiated")
	
		DEBUG_WARP_TO_NEAREST_ATM()
	ENDIF
	
	// Warp player to the next atm in the list of atms
	IF IS_DEBUG_KEY_JUST_RELEASED(KEY_J, KEYBOARD_MODIFIER_SHIFT, "Shift-J Skip")
		PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] [DEBUG] - MAINTAIN_J_SKIPS - KEY_J, KEYBOARD_MODIFIER_SHIFT just released - J Skip to next atm in list initiated")
	
		DEBUG_WARP_TO_NEXT_ATM()
	ENDIF
	
ENDPROC

BOOL bWarpToNearestATM = FALSE
BOOL bWarpToNextATM = FALSE
BOOL bEndNowSPass = FALSE
BOOL bEndNowFFail = FALSE
BOOL bTerminateScriptNow = FALSE
BOOL bToggleSpammyPrints = FALSE

PROC CREATE_WIDGETS()
	INT iPlayer
	TEXT_LABEL_63 tl63
	
	START_WIDGET_GROUP("GB Cashing Out")
		ADD_WIDGET_BOOL("Locally terminate Script Now", bTerminateScriptNow)
		ADD_WIDGET_BOOL("Fake End Of Mode Timer", serverBD.bFakeEndOfModeTimer)
		START_WIDGET_GROUP(" Game State")
			START_WIDGET_GROUP("Server") 
				ADD_WIDGET_INT_SLIDER("Game state", serverBD.iServerGameState,-1, HIGHEST_INT,1)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Client")  				
				REPEAT GB_MAX_GANG_SIZE_INCLUDING_BOSS_EXEC iPlayer
					tl63 = "Player "
					tl63 += iPlayer
					IF iPlayer = PARTICIPANT_ID_TO_INT()
						tl63 += "*"
					ENDIF
					START_WIDGET_GROUP(tl63)
						ADD_WIDGET_INT_SLIDER("Game state", playerBD[iPlayer].iClientGameState,-1, HIGHEST_INT,1)
					STOP_WIDGET_GROUP()
				ENDREPEAT
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("ATM Warping")
			ADD_WIDGET_BOOL("Warp to the nearest unblocked ATM", bWarpToNearestATM)
			ADD_WIDGET_BOOL("Warp to the next ATM in the list of ATMs", bWarpToNextATM)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("S Pass and F Fail")
			ADD_WIDGET_BOOL("End the mode now for all players, Highest Score Wins (S Pass)", bEndNowSPass)
			ADD_WIDGET_BOOL("End the mode now for all players, No Cash Collected (F Fail)", bEndNowFFail)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Debugging")
			ADD_WIDGET_BOOL("Use spammy prints", bToggleSpammyPrints)
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
ENDPROC

PROC UPDATE_WIDGETS()
	IF NOT IS_LOCAL_DEBUG_BIT0_SET(eLOCALDEBUGBITSET0_TERMINATE_SCRIPT_NOW)
		IF bTerminateScriptNow
			PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] widget bTerminateScriptNow = TRUE.")
			SET_LOCAL_DEBUG_BIT0(eLOCALDEBUGBITSET0_TERMINATE_SCRIPT_NOW)
		ENDIF
	ENDIF
	
	IF bWarpToNearestATM
		PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] [DEBUG ONLY] - UPDATE_WIDGETS - bWarpToNearestATM = TRUE")
		DEBUG_WARP_TO_NEAREST_ATM()
		bWarpToNearestATM = FALSE
	ENDIF
	
	IF bWarpToNextATM
		PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] [DEBUG ONLY] - bWarpToNextATM = TRUE")
		DEBUG_WARP_TO_NEXT_ATM()
		bWarpToNextATM = FALSE
	ENDIF
	
	IF bToggleSpammyPrints
		bShowSpammyPrints = !bShowSpammyPrints
		PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] [DEBUG ONLY] - bShowSpammyPrints = ", GET_STRING_FROM_BOOL(bShowSpammyPrints))
	ENDIF
	
	IF bEndNowSPass
		PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] [DEBUG] - UPDATE_WIDGETS - bEndNowSPass = TRUE")
		
		SCRIPT_EVENT_DATA_TICKER_MESSAGE sTicker
		sTicker.TickerEvent = TICKER_EVENT_PLAYER_CHEATED_SPASS
		sTicker.playerID = PLAYER_ID()
		BROADCAST_TICKER_EVENT(sTicker, ALL_PLAYERS_ON_SCRIPT())
		SET_CLIENT_DEBUG_BIT0(eCLIENTDEBUGBITSET0_I_HAVE_S_PASSED)
	
		bEndNowSPass = FALSE
	ENDIF
	
	IF bEndNowFFail
		PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] [DEBUG] - UPDATE_WIDGETS - bEndNowFFail = TRUE")
		
		SCRIPT_EVENT_DATA_TICKER_MESSAGE sTicker
		sTicker.TickerEvent = TICKER_EVENT_PLAYER_CHEATED_FFAIL
		sTicker.playerID = PLAYER_ID()
		BROADCAST_TICKER_EVENT(sTicker, ALL_PLAYERS_ON_SCRIPT())
		SET_CLIENT_DEBUG_BIT0(eCLIENTDEBUGBITSET0_I_HAVE_F_FAILED)
		
		bEndNowFFail = FALSE
	ENDIF
ENDPROC

#ENDIF

PROC UPDATE_FOCUS_PARTICIPANT()

	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF NOT IS_PLAYER_SPECTATING(PLAYER_ID())
			IF iFocusParticipant != -1
				iFocusParticipant = -1
			ENDIF
			CLEAR_LOCAL_BIT0(eLOCALBITSET0_SPECTATING_GANG)
		ELSE
			IF IS_A_SPECTATOR_CAM_RUNNING()
			AND NOT IS_SPECTATOR_HUD_HIDDEN()
				PED_INDEX specTargetPed = GET_SPECTATOR_SELECTED_PED()
				IF IS_PED_A_PLAYER(specTargetPed)
					PLAYER_INDEX specPlayerTemp = NETWORK_GET_PLAYER_INDEX_FROM_PED(specTargetPed)
					IF NETWORK_IS_PLAYER_A_PARTICIPANT(specPlayerTemp)
						PARTICIPANT_INDEX specTargetParticipant = NETWORK_GET_PARTICIPANT_INDEX(specPlayerTemp)
						iFocusParticipant = NATIVE_TO_INT(specTargetParticipant)
						SET_LOCAL_BIT0(eLOCALBITSET0_SPECTATING_GANG)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC MAINTAIN_CLEANUP_CHECKS()

	IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE")
		EXECUTE_COMMON_END_TELEMTRY(FALSE, TRUE)
		SCRIPT_CLEANUP(TRUE)
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF IS_LOCAL_DEBUG_BIT0_SET(eLOCALDEBUGBITSET0_TERMINATE_SCRIPT_NOW)
		PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] eLOCALDEBUGBITSET0_TERMINATE_SCRIPT_NOW is set")
		EXECUTE_COMMON_END_TELEMTRY(FALSE, TRUE)
		SCRIPT_CLEANUP()
	ENDIF
	#ENDIF
			
	IF GB_SHOULD_QUIT_ACTIVE_BOSS_MISSION()
		PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - GB_SHOULD_QUIT_ACTIVE_BOSS_MISSION = TRUE")
		EXECUTE_COMMON_END_TELEMTRY(FALSE, TRUE)
		SCRIPT_CLEANUP()
	ENDIF
	
	
	IF GET_GANG_BOSS_PLAYER_INDEX() != INVALID_PLAYER_INDEX()
		IF GB_SHOULD_BAIL_BOSS_CHALLENGE_DUE_TO_NO_OR_LOW_GOONS(GET_GANG_BOSS_PLAYER_INDEX())
			GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LOW_NUMBERS)
			PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - GB_SHOULD_BAIL_BOSS_CHALLENGE_DUE_TO_NO_OR_LOW_GOONS - TRUE, calling script cleanup")
			SCRIPT_CLEANUP()
		ENDIF
	ENDIF
ENDPROC




//----------------------
//	MAIN SCRIPT
//----------------------

SCRIPT(MP_MISSION_DATA missionScriptArgs)


	PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] START")
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE)
			
			IF NOT PROCESS_PRE_GAME(missionScriptArgs)
				EXECUTE_COMMON_END_TELEMTRY(FALSE, TRUE)
				SCRIPT_CLEANUP()
			ENDIF
			// Setup widgets. Make sure this is done after PROCESS_PRE_GAME()
			#IF IS_DEBUG_BUILD
				CREATE_WIDGETS()
			#ENDIF
		ELSE
			
			PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] calling SCRIPT_CLEANUP() - IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE) = FALSE")
			SCRIPT_CLEANUP()
			
		ENDIF
	ELSE
		
		PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] calling SCRIPT_CLEANUP() - NETWORK_IS_GAME_IN_PROGRESS() = FALSE")
		EXECUTE_COMMON_END_TELEMTRY(FALSE, TRUE)
		SCRIPT_CLEANUP()
		
	ENDIF
	
	WHILE (TRUE)
	
		MP_LOOP_WAIT_ZERO()
		
		MAINTAIN_CLEANUP_CHECKS()
		
		MAINTAIN_DISTANCE_CHECKS()	
		
		UPDATE_FOCUS_PARTICIPANT()
		
		PROCESS_EVENTS()
		
		GB_MAINTAIN_SPECTATE(sSpecVars)
		
		// Deal with the debug
		#IF IS_DEBUG_BUILD		
			UPDATE_WIDGETS()
		#ENDIF
		
		SWITCH(GET_CLIENT_GAME_STATE(PARTICIPANT_ID_TO_INT()))
		
			CASE GAME_STATE_INIT
				IF GET_SERVER_GAME_STATE() = GAME_STATE_RUNNING
					GB_SET_ITS_SAFE_FOR_SPEC_CAM()
					IF GB_LAUNCH_CHALLENGE_WITH_SPEC_CAM_IS_ACTIVE(sSpecVars)
						IF INIT_CLIENT()
							SET_CLIENT_GAME_STATE(PARTICIPANT_ID_TO_INT(),GAME_STATE_RUNNING)
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE GAME_STATE_RUNNING
				IF GET_SERVER_GAME_STATE() = GAME_STATE_RUNNING
				
					#IF IS_DEBUG_BUILD
					MAINTAIN_J_SKIPS()
					#ENDIF
				
					CLIENT_PROCESSING()
					
				ELIF GET_SERVER_GAME_STATE() = GAME_STATE_END
					SET_CLIENT_GAME_STATE(PARTICIPANT_ID_TO_INT(), GAME_STATE_END)
				ENDIF
			BREAK
			
			CASE GAME_STATE_END
				SCRIPT_CLEANUP()
			BREAK
			
		ENDSWITCH
	
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		
			IF GB_SHOULD_KILL_ACTIVE_BOSS_MISSION()
				PRINTLN("[EXEC1] [GB CASHING OUT] [ST EXEC] - GB_SHOULD_KILL_ACTIVE_BOSS_MISSION = TRUE")
				SET_SERVER_GAME_STATE(GAME_STATE_END)
			ENDIF
		
			SWITCH(GET_SERVER_GAME_STATE())
			
				CASE GAME_STATE_INIT
					IF INIT_SERVER()
						SET_SERVER_GAME_STATE(GAME_STATE_RUNNING)
					ENDIF
				BREAK
				
				CASE GAME_STATE_RUNNING
					SERVER_PROCESSING()
					
					#IF IS_DEBUG_BUILD
					MAINTAIN_DEBUG_END_CHECKS()
					#ENDIF
				BREAK
				
				CASE GAME_STATE_END
					SCRIPT_CLEANUP()
				BREAK
				
			ENDSWITCH
		
		ENDIF
		
	ENDWHILE

	
	
// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT


