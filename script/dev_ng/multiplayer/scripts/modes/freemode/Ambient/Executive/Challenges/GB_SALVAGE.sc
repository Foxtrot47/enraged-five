//////////////////////////////////////////////////////////////////////////////////////////
// Name:        SALVAGE.sc													//
// Description:  Boss launches the challenge and a random location is picked - e.g.		//
// Grapeseed Downtown, El Burro, etc. This is represented by an area blip on the radar.	//
// A package is created at a random hidden position within the selected location,		//
// and the area it could be in is blipped on the radar for the Boss and Goons. 			//
// The first to find it within the Challenge time limit is the winner.					//
// Written by:  Kevin Wong																//
// Date: 6/10/2015																		//
//////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "net_mission.sch"



//----------------------
//	INCLUDES
//----------------------
USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_camera.sch"
USING "commands_pad.sch"
USING "commands_script.sch"
USING "flow_public_core_override.sch"
USING "net_wait_zero.sch"
USING "net_mission.sch"
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_event.sch"
USING "net_include.sch"
USING "net_events.sch"
USING "net_gang_boss.sch"
USING  "AM_Common_UI.sch"  
#IF IS_DEBUG_BUILD
USING "net_debug_log.sch"
#ENDIF
USING "net_gang_boss_spectate.sch"
USING "DM_Leaderboard.sch"


//----------------------
//	Local Bitset
//----------------------


//----------------------
//	VARIABLES
//----------------------


CONST_INT NUM_CHECKPOINTS 50
CONST_INT MAX_NUM_REBREATHERS 10


//----------------------
//	GAME STATE
//----------------------

CONST_INT GAME_STATE_INIT 									0
CONST_INT GAME_STATE_RUNNING								1
CONST_INT GAME_STATE_TERMINATE_DELAY						2
CONST_INT GAME_STATE_END									3
CONST_INT MAXNUMJETSKIS 4
ENUM GEN_RUN_STAGE
	GEN_START = 0,
	GEN_IN_PACKAGE_AREA = 1,
	GEN_SEARCH_FOR_PACKAGE_IN_AREA = 2
ENDENUM





ENUM MAP_AREA_ZONE
	CONTAINERSHIP = 0,		
	PLANENORTH,	
	PADDLESTEAMER,
	CARGOSHIP,
	PLANESOUTH,
	MILITARYDEBRIS,
	SUBMARINE,
	MILITARYPLANE,
	BATHYSCAPE,
	TUGBOAT,
	GENERAL


ENDENUM

STRUCT LOCAL_CHECKPOINT_DATA

	BLIP_INDEX cpBlip
	CHECKPOINT_INDEX checkpointObj
	INT iAlpha = 0 //url:bugstar:2414057
	BOOL has_checkpoint_been_collected = FALSE
	
ENDSTRUCT

//----------------------
//	BLIPS
//----------------------
BLIP_INDEX PackageBlipArea
BLIP_INDEX PackageBlipAreaLongRange



LOCAL_CHECKPOINT_DATA localCheckpointData[NUM_CHECKPOINTS]
BLIP_INDEX bliprebreather[NUM_CHECKPOINTS]




ENUM SERVER_ENDING_STATES
	STILL_PLAYING = 0,		
	ALL_PACKAGES_COLLECTED,
	TIME_EXPIRES
	
ENDENUM
//----------------------
//	BROADCAST VARIABLES
//----------------------

STRUCT SERVER_CHECKPOINT_DATA

	//VECTOR vLocation
	INT iCollectedBy = -1
	INT iCheckpointIndex //Unique identifier
	INT iCheckpointCollectedBS
ENDSTRUCT
// Everyone can read this data, only the server can update it
STRUCT ServerBroadcastData
	INT iServerGameState = GAME_STATE_INIT

	INT ipackagecounter	= 0
	//VECTOR vPackageLocation[NUM_CHECKPOINTS+1]
	SERVER_CHECKPOINT_DATA serverCheckpointData[NUM_CHECKPOINTS]
	VECTOR vareablipcoordinates
	BOOL has_a_checkpoint_been_passed
	//INT player_who_package_been_pickedup[NUM_CHECKPOINTS]

	SCRIPT_TIMER soundpackagetonetimer
	SCRIPT_TIMER Starttimer
	PLAYER_INDEX piGangBoss
	BOOL brandomcoordinatesgenerated = FALSE
	INT iServerSyncId = 0
	MAP_AREA_ZONE MapArea
	GEN_RUN_STAGE eGEN_Stage = GEN_START
	AMBIENT_ACTIVITY_LEADERBOARD_STRUCT sSortedPackagesCollected[GB_MAX_GANG_SIZE_INCLUDING_BOSS_EXEC]
	SERVER_ENDING_STATES eServer_ending_stat =  STILL_PLAYING
	
	INT iMatchId1
	INT iMatchId2
	INT inumberofcheckpoints			= 50
	INT inumberofcheckpointsRemaining	= 50
	BOOL bcreate_bjetskis
	INT inumberofrebreathers	// number of players minus 1
	BOOL bcreate_rebreathers
	INT 	iPlayerBitSet
	OBJECT_INDEX objrebreather[MAX_NUM_REBREATHERS]
	NETWORK_INDEX network_index_rebreather[MAX_NUM_REBREATHERS]
	NETWORK_INDEX salvagelocalplayersjetski[MAXNUMJETSKIS]
	INT iclientscore[NUM_NETWORK_PLAYERS] 
ENDSTRUCT
INT iLocalCheckpointCreated[CHECKPOINT_BS_SIZE]
INT iLocalCheckpointCollected[CHECKPOINT_BS_SIZE]
//INT iLocalCheckpointCollectedBS[CHECKPOINT_BS_SIZE]
INT temprebreathernumber[MAX_NUM_REBREATHERS] 
SCRIPT_TIMER helptimer
SCRIPT_TIMER rebreathertimer
SCRIPT_TIMER rebreathersoundtimer
SCRIPT_TIMER saftycheckpointtimer 
NETWORK_INDEX salvagelocalplayersjetski
ServerBroadcastData serverBD
GB_MAINTAIN_SPECTATE_VARS sSpecVars
GANG_BOSS_MANAGE_REWARDS_DATA sRewardsData
BOOL bcreatejetskiblips
BOOL bcreate_rebreather_blips
BOOL bGIVE_PLAYER_REBREATHER = FALSE
BOOL bmarkalljetskistobedeleted = FALSE
BOOL bdisplayseasharkhelp
BOOL bhavejetskisbeendeleted = FALSE
BOOL initialrebreathercheck
BOOL cleanupcountdownmusic
INT inum_of_rebreathers_at_start
PLAYER_INDEX initial_player_boss
//PTFX_ID flare_ptfx
//PTFX_ID flare_smoke_ptfx
BLIP_INDEX   blipjetski[MAXNUMJETSKIS]

//----------------------
//	STRUCT
//----------------------
// The server broadcast data.
// Everyone can read this data, only the server can update it


BOOL bcreatepackageareablip[NUM_CHECKPOINTS]
BOOL bhas_the_package_been_created = FALSE

//INT iTargetLoop
BOOL bHasdisplayedfinalshard = FALSE
INT  iSoundfrompackage = GET_SOUND_ID()
INT  iSoundfromflare= GET_SOUND_ID()
BOOL bdisplay_blip_near_end_of_expire_time
BOOL bAddBlipforpackage
//BOOL bhasfired1minutefireworks
SCRIPT_TIMER fireworkstimer 
//BOOL has_flare_fx_been_created = FALSE

SCRIPT_TIMER rumbletimer
CONST_FLOAT cfHTB_MIN_RUMBLE_DISTANCE 		5.0

//Flare
OBJECT_INDEX objFlare
PTFX_ID players_underwater_flare
STRING strHeistFX = "scr_biolab_heist"
BOOL bEquippedRebreather = FALSE

CONST_INT ciHTB_MIN_SHAKE_VALUE 			25 // Any lower is actually ultra-high frequency for some reason?
CONST_INT ciHTB_MAX_SHAKE_VALUE	 			250
INT ciHTB_RUMBLE_DURATION 					= 1000
INT ciHTB_RUMBLE_DURATION_MULTIPLIER 		= 145
INT bRUMBLE_HELP1 							= 0
INT inumberofrebreatherscollected 			= 0
CONST_INT ciHTB_TRIGGER_RADIUS				250
CONST_INT ciHTB_BLIP_ALPHA_INSIDE			70
/*
INT ilastgenerated_location_number	= -1
INT ilastgenerated_location_number2 = -1
INT ilastgenerated_location_number3 = -1
INT ilastgenerated_location_number4 = -1*/
INT initialisemusic
BOOL display_pickupmoney_help
INT iBlipAlphaCached
INT serverStaggeredPlayerLoop
BOOL bSet

BOOL bplayerworemaskatstart= FALSE
INT irumbleduration = 5000
SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerEventData
GB_STRUCT_BOSS_END_UI BOSS_END_UI

// The client broadcast data.
// Everyone can read this data, only the local player only the server can update it
STRUCT PlayerBroadcastData
	INT iClientGameState = GAME_STATE_INIT
	int iClientBitSet
	GEN_RUN_STAGE eGEN_Stage = GEN_START
	BOOL this_player_has_picked_up_package
	BOOL this_player_in_salvage_area
	INT itempcheckpoingnumber
	
	
ENDSTRUCT
PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]


FLOAT flare_offset_x = -0.285 //-0.260
FLOAT flare_offset_y = -0.078  // -0.010
FLOAT flare_offset_z = -0.169

FLOAT flare_ROT_x = -0.0
FLOAT flare_ROT_y = -0.451
FLOAT flare_ROT_z = -342.7
BOOL flare_attach = FALSE
INT SAFETYTIMER = 1000

FUNC PLAYER_INDEX GET_GANG_BOSS_PLAYER_INDEX()
	RETURN serverBD.piGangBoss
ENDFUNC

//Helper function to get a clients game/mission state
FUNC INT GET_CLIENT_MISSION_STATE(INT iPlayer)
	RETURN playerBD[iPlayer].iClientGameState
ENDFUNC

/// PURPOSE:
///    Start countdown timer amount.
FUNC INT GET_CHALLENGE_EXPIRE_TIME()
	RETURN g_sMPTunables.iexec_salvage_time_limit                                 
	//RETURN 600000
	// g_sMPTunables.iDestroy_Vehicle_Expiry_Time iChallenge_Event_Start_Countdown_Time
ENDFUNC


FUNC BOOL IS_GAME_A_DRAW()
	INT i
	REPEAT GB_GET_MAX_GANG_SIZE_INCLUDING_BOSS() i
		IF i != 0
			//IF serverBD.sSortedPackagesCollected[0].iScore != 0
				IF serverBD.sSortedPackagesCollected[0].iScore = serverBD.sSortedPackagesCollected[i].iScore 
					PRINTLN("[salvage]  IS_GAME_A_DRAW = TRUE serverBD.sSortedPackagesCollected[0].iScore = ", serverBD.sSortedPackagesCollected[0].iScore )
					PRINTLN("[salvage]  IS_GAME_A_DRAW = TRUE serverBD.sSortedPackagesCollected[i].iScore = ", serverBD.sSortedPackagesCollected[i].iScore )
					RETURN TRUE
				ENDIF
			//ENDIF
		ELSE
			IF serverBD.sSortedPackagesCollected[1].iScore = serverBD.sSortedPackagesCollected[i].iScore 
				PRINTLN("[salvage]  IS_GAME_A_DRAW 1 = TRUE serverBD.sSortedPackagesCollected[0].iScore = ", serverBD.sSortedPackagesCollected[0].iScore )
				PRINTLN("[salvage]  IS_GAME_A_DRAW 1 = TRUE serverBD.sSortedPackagesCollected[i].iScore = ", serverBD.sSortedPackagesCollected[i].iScore )
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	PRINTLN("[salvage]  IS_GAME_A_DRAW = FALSE serverBD.sSortedPackagesCollected[0]. = ", serverBD.sSortedPackagesCollected[0].iScore )
	RETURN FALSE
ENDFUNC

//----------------------
//	Client Functions
//----------------------



PROC CHECKPOINT_FLASH_FADE(CHECKPOINT_INDEX &checkpoint,INT &iPrevAlpha, BLIP_INDEX &blip,INT iCheckpoint)
	
	INT iR, iG, iB, iA
	IF checkpoint != NULL
		iPrevAlpha -= 25
	    IF iPrevAlpha > 0
	        GET_HUD_COLOUR(HUD_COLOUR_WHITE, iR, iG, iB, iA)
	        SET_CHECKPOINT_RGBA(checkpoint,iR, iG, iB, iPrevAlpha)
			SET_CHECKPOINT_RGBA2(checkpoint,iR, iG, iB, iPrevAlpha)
			PRINTLN("[JS] - FLASH FADE - GET_HUD_COLOUR[salvage]  ")
	    ELSE
			IF DOES_BLIP_EXIST(blip)
				REMOVE_BLIP(blip)
			ENDIF
	       	DELETE_CHECKPOINT(checkpoint)
			checkpoint = NULL
			CLEAR_BIT(iLocalCheckpointCreated[GET_LONG_BITSET_INDEX(iCheckpoint)],GET_LONG_BITSET_BIT(iCheckpoint))
	  		PRINTLN("[JS] - FLASH FADE - DELETE_CHECKPOINT[salvage] ")
		ENDIF
	ELSE
		PRINTLN("[JS] - FLASH FADE - CHECKPOINT NULL[salvage] ")
	ENDIF
ENDPROC

FUNC BOOL CHECKPOINT_FLASH_FADE_IN(CHECKPOINT_INDEX &checkpoint, INT &iNextAlpha)
	INT iR, iG, iB, iA
    IF iNextAlpha < 175
		iNextAlpha += 10
        GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iR, iG, iB, iA)
        SET_CHECKPOINT_RGBA(checkpoint,iR, iG, iB, iNextAlpha)
		SET_CHECKPOINT_RGBA2(checkpoint,iR, iG, iB, iNextAlpha)
		RETURN FALSE
	ELSE
		RETURN TRUE
    ENDIF
		PRINTLN("[JS] FADE IN - something broke [salvage]  ")
	RETURN TRUE
ENDFUNC

FUNC BOOL CHECKPOINT_FLASH_FADE_OUT(CHECKPOINT_INDEX &checkpoint, INT &iNextAlpha)
	INT iR, iG, iB, iA
	iNextAlpha -= 10
    IF iNextAlpha > 0
        GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iR, iG, iB, iA)
        SET_CHECKPOINT_RGBA(checkpoint,iR, iG, iB, iNextAlpha)
		SET_CHECKPOINT_RGBA2(checkpoint,iR, iG, iB, iNextAlpha)
		RETURN FALSE
	ELSE
		RETURN TRUE
    ENDIF
		PRINTLN("[JS] FADE OUT - something broke [salvage] ")
	RETURN TRUE
ENDFUNC

PROC DRAW_CHECKPOINT_LIGHTS(VECTOR &vLocation)

	INT iR, iG, iB
	iR = 239
	iG = 250
	iB = 187
	
	FLOAT fIntensity = 5.0
	
	FLOAT fFallOffDist	
	FLOAT fFallOffExp
	
	fFallOffExp = 64.00
	

	fFallOffDist = 15.00	
	

	IF NOT IS_VECTOR_ZERO(vLocation)
		vLocation += MATC_CORONA_LIGHTS_ABOVE_GROUND_m
		DRAW_LIGHT_WITH_RANGEEX(vLocation, iR, iG, iB, fFallOffDist, fIntensity, fFallOffExp)
	ENDIF
ENDPROC


/// PURPOSE: Maintain the drawing of blips and checkpoints
///    
/// PARAMS:
///    currentCheckpoint - Contains the local checkpoint objects 
///    serverCheckpoint - Contains the server checkpoint data
///    iCheckpoint - The current checkpoint index
PROC DRAW_CHECKPOINTS(LOCAL_CHECKPOINT_DATA &currentCheckpoint,VECTOR serverCheckpoint,INT iCheckpoint)
	
	VECTOR vDrawPosition = serverCheckpoint

	INT iR, iG, iB, iA
	GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iR, iG, iB, iA)
	
	//If checkpoint has not been collected by local player and has not been collected by anyone else
	//IF NOT IS_BIT_SET(iLocalCheckpointCollectedBS[GET_LONG_BITSET_INDEX(iCheckpoint)],GET_LONG_BITSET_BIT(iCheckpoint))
	IF NOT IS_BIT_SET(serverBD.serverCheckpointData[GET_LONG_BITSET_INDEX(iCheckpoint)].iCheckpointCollectedBS,GET_LONG_BITSET_BIT(iCheckpoint))
		
		//Manage Blips
		IF DOES_BLIP_EXIST(currentCheckpoint.cpBlip)
			//Fade blips based on range
			IF IS_PAUSE_MENU_ACTIVE()
				SET_BLIP_ALPHA(currentCheckpoint.cpBlip,255)
			ELSE
				SET_BLIP_ALPHA_BASED_ON_DIST_FROM_PLAYER(currentCheckpoint.cpBlip,25)
			ENDIF
		ENDIF
		
		//Manage Checkpoints
		
		IF NOT IS_BIT_SET(iLocalCheckpointCreated[GET_LONG_BITSET_INDEX(iCheckpoint)],GET_LONG_BITSET_BIT(iCheckpoint))
			CHECKPOINT_TYPE checkpointType
			checkpointType = CHECKPOINT_MONEY


			currentCheckpoint.checkpointObj = CREATE_CHECKPOINT(checkpointType, vDrawPosition, vDrawPosition, 5.0 ,iR, iG, iB, currentCheckpoint.iAlpha)
			SET_CHECKPOINT_CYLINDER_HEIGHT(currentCheckpoint.checkpointObj, 2.5, 2.5, 50)
			CPRINTLN(DEBUG_NET_AMBIENT, "=== [salvage] DRAW_CHECKPOINTS ===  NOT IS_AIR_CHECKPOINT ",vDrawPosition)

			SET_BIT(iLocalCheckpointCreated[GET_LONG_BITSET_INDEX(iCheckpoint)],GET_LONG_BITSET_BIT(iCheckpoint))
			CPRINTLN(DEBUG_NET_AMBIENT, "=== [salvage] DRAW_CHECKPOINTS ===  CHECKPOINT ADDED AT ",vDrawPosition)
		
			GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iR, iG, iB, iA)
		    SET_CHECKPOINT_RGBA(currentCheckpoint.checkpointObj,iR, iG, iB, 175)
			SET_CHECKPOINT_RGBA2(currentCheckpoint.checkpointObj,iR, iG, iB, 175)
			
			IF NOT ARE_VECTORS_EQUAL(vDrawPosition, <<0, 0, 0>>)
			AND NOT ARE_VECTORS_EQUAL(vDrawPosition, <<0, 0, -2000>>)
				currentCheckpoint.cpBlip = ADD_BLIP_FOR_COORD(vDrawPosition)
				SET_BLIP_SPRITE(currentCheckpoint.cpBlip, RADAR_TRACE_TEMP_3)
				SET_BLIP_PRIORITY(currentCheckpoint.cpBlip, BLIPPRIORITY_HIGHEST)
				SET_BLIP_NAME_FROM_TEXT_FILE(currentCheckpoint.cpBlip, "CPC_BLIP")
				SET_BLIP_ALPHA_BASED_ON_DIST_FROM_PLAYER(currentCheckpoint.cpBlip,25)
				SET_BLIP_COLOUR_FROM_HUD_COLOUR(currentCheckpoint.cpBlip,HUD_COLOUR_YELLOW)
				SHOW_HEIGHT_ON_BLIP(currentCheckpoint.cpBlip,TRUE)
				SET_BLIP_AS_SHORT_RANGE(currentCheckpoint.cpBlip, TRUE)
				CPRINTLN(DEBUG_NET_AMBIENT, "[salvage] DRAW_CHECKPOINTS ===  ===  BLIP ADDED AT ",vDrawPosition)
			ENDIF
		ELSE
			//Fade in checkpoint
			 CHECKPOINT_FLASH_FADE_IN(currentCheckpoint.checkpointObj,currentCheckpoint.iAlpha)
		ENDIF
		
		DRAW_CHECKPOINT_LIGHTS(serverCheckpoint)

	//Checkpoint has been collected
	ELSE
		IF IS_BIT_SET(iLocalCheckpointCreated[GET_LONG_BITSET_INDEX(iCheckpoint)],GET_LONG_BITSET_BIT(iCheckpoint))
			//Fade out checkpoint and delete when faded
			//CHECKPOINT_FLASH_FADE(currentCheckpoint.checkpointObj,currentCheckpoint.iAlpha,currentCheckpoint.cpBlip,iCheckpoint)
			DELETE_CHECKPOINT(currentCheckpoint.checkpointObj)
			IF DOES_BLIP_EXIST(currentCheckpoint.cpBlip)
				REMOVE_BLIP(currentCheckpoint.cpBlip)
			ENDIF
	       
			currentCheckpoint.checkpointObj = NULL
			currentCheckpoint.has_checkpoint_been_collected = TRUE
			CLEAR_BIT(iLocalCheckpointCreated[GET_LONG_BITSET_INDEX(iCheckpoint)],GET_LONG_BITSET_BIT(iCheckpoint))
		ELSE
			//Remove blip
			IF DOES_BLIP_EXIST(currentCheckpoint.cpBlip)
				REMOVE_BLIP(currentCheckpoint.cpBlip)
				CPRINTLN(DEBUG_NET_AMBIENT, "[salvage] === CHECKPOINT_COLLECTION === BLIP AND CHECKPOINT REMOVED - REMOVE_BLIP(currentCheckpoint.cpBlip ")
				currentCheckpoint.has_checkpoint_been_collected = TRUE
				DELETE_CHECKPOINT(currentCheckpoint.checkpointObj )
				currentCheckpoint.checkpointObj = NULL
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(serverBD.serverCheckpointData[GET_LONG_BITSET_INDEX(iCheckpoint)].iCheckpointCollectedBS,GET_LONG_BITSET_BIT(iCheckpoint))
			//DELETE_CHECKPOINT(currentCheckpoint.checkpointObj)
			IF DOES_BLIP_EXIST(currentCheckpoint.cpBlip)
				REMOVE_BLIP(currentCheckpoint.cpBlip)
				currentCheckpoint.has_checkpoint_been_collected = TRUE
				CPRINTLN(DEBUG_NET_AMBIENT, "[salvage] === CHECKPOINT_COLLECTION === BLIP AND CHECKPOINT REMOVED - CHECKPOINT COLLECTED by someone at ",vDrawPosition)
			ENDIF
			
		ENDIF
		
		//CPRINTLN(DEBUG_NET_AMBIENT, "[salvage] === CHECKPOINT_COLLECTION === BLIP AND CHECKPOINT REMOVED - CHECKPOINT COLLECTED at ",vDrawPosition)
	ENDIF
		
ENDPROC
//----------------------
//	DBG FUNCTIONS
//----------------------


FUNC VECTOR GET_AREA_BLIP_COORDINATES(MAP_AREA_ZONE mapzone)

	SWITCH mapzone
		CASE CONTAINERSHIP
			RETURN  <<3172.2461, -337.4840, -14.5003>>
			//Kortz
		BREAK
		
		CASE PLANENORTH
			RETURN  <<-928.4571, 6581.1729, -12.8434>>	 // <<-978.4571, 6603.1729, -12.8434>>			
		BREAK		

		CASE PADDLESTEAMER
			RETURN  <<2671.5374, -1398.6479, -8.4314>>
		BREAK
		
		CASE CARGOSHIP   
			RETURN  <<-203.7591, -2863.4998, -0.9841>>
		BREAK	
		
		CASE PLANESOUTH   
			RETURN  <<1794.1816, -2968.3179, -3.0297>>
		BREAK	
		
		CASE MILITARYDEBRIS   
			RETURN  <<4128.1602, 3537.0808, -26.9639>>
		BREAK
		
		CASE SUBMARINE   
			RETURN  <<2651.7754, 6647.8843, -3.8573>>
		BREAK	
		
				
		CASE MILITARYPLANE   
			RETURN <<-3242.6138, 3669.3164, -16.2196>>

		BREAK	
		
		
				
		CASE BATHYSCAPE   
			RETURN  <<-2835.3994, -519.6956, -42.4877>>

		BREAK	
		
		
				
		CASE TUGBOAT   
			RETURN  <<-3178.1387, 3039.4556, -27.8788>>

		BREAK	
		
		
	ENDSWITCH
	
	
	SCRIPT_ASSERT("GET_AREA_BLIP_COORDINATES INVALID ZONE PASSED IN.")
	
	RETURN <<0.0,0.0,0.0>>
ENDFUNC




//PURPOSE: Get Random Location for checkpoints
FUNC VECTOR GET_CHECKPOINT_LOCATION(  INT checkpoint_number )
		//serverBD.MapArea

		IF  serverBD.MapArea = CONTAINERSHIP
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				serverBD.brandomcoordinatesgenerated = TRUE
				serverBD.vareablipcoordinates =  GET_AREA_BLIP_COORDINATES(CONTAINERSHIP)			//Kortz	
				serverBD.inumberofcheckpoints = NUM_CHECKPOINTS		
			ENDIF
			SWITCH checkpoint_number
				CASE 0   RETURN 	<<3135.3657, -354.4541, -7.0000>>	 BREAK //<<3135.3657, -354.4541, -2.2983>>		BREAK
				CASE 1   RETURN     <<3147.6912, -359.3427, -20.7122>>   BREAK //  <<3147.6912, -359.3427, -20.7122>>      BREAK
				CASE 2   RETURN     <<3186.5730, -357.5428, -8.8624>>    BREAK // <<3186.5730, -357.5428, -8.8624>>       BREAK
				CASE 3   RETURN     <<3173.9028, -325.9884, -19.1843>>   BREAK //  <<3173.9028, -325.9884, -19.1843>>      BREAK
				CASE 4   RETURN     <<3170.2520, -313.4249, -8.7975>>    BREAK // <<3170.2520, -313.4249, -8.7975>>       BREAK
				CASE 5   RETURN     <<3196.7742, -379.4515, -23.8401>>   BREAK //  <<3196.7742, -379.4515, -23.8401>>      BREAK
				CASE 6   RETURN     <<3140.8335, -312.4993, -19.6948>>   BREAK //  <<3140.8335, -312.4993, -19.6948>>      BREAK
				CASE 7   RETURN     <<3219.6697, -421.9644, -27.6030>>   BREAK //  <<3219.6697, -421.9644, -27.6030>>      BREAK
				CASE 8   RETURN     <<3209.5959, -397.0404, -30.8351>>   BREAK //  <<3209.5959, -397.0404, -30.8351>>      BREAK
				CASE 9   RETURN     <<3201.9934, -388.6151, -8.2073>>    BREAK // <<3201.9934, -388.6151, -8.2073>>       BREAK
				CASE 10  RETURN     <<3135.2490, -283.5456, -15.2094>>   BREAK //  <<3135.2490, -283.5456, -15.2094>>      BREAK
				CASE 11  RETURN     <<3147.4255, -266.2146, -24.7057>>   BREAK //  <<3147.4255, -266.2146, -24.7057>>      BREAK
				CASE 12  RETURN     <<3127.4033, -221.6972, -23.2968>>   BREAK //  <<3127.4033, -221.6972, -23.2968>>      BREAK
				CASE 13  RETURN     <<3126.3667, -307.3863, -11.9712>>   BREAK //  <<3126.3667, -307.3863, -11.9712>>      BREAK
				CASE 14  RETURN     <<3152.1917, -394.2682, -10.2259>>   BREAK //  <<3152.1917, -394.2682, -10.2259>>      BREAK
				CASE 15  RETURN     <<3190.4556, -399.7629, -24.9871>>   BREAK //  <<3190.4556, -399.7629, -24.9871>>      BREAK
				CASE 16  RETURN     <<3199.3650, -314.4537, -5.2628>>    BREAK // <<3199.3650, -314.4537, -5.2628>>       BREAK
				CASE 17  RETURN     <<3156.1699, -281.5862, -5.5440>>    BREAK // <<3156.1699, -281.5862, -5.5440>>       BREAK
				CASE 18  RETURN     <<3176.5042, -251.6724, -14.6202>>   BREAK //  <<3176.5042, -251.6724, -14.6202>>      BREAK
				CASE 19  RETURN     <<3103.8032, -250.1026, -3.3526>>    BREAK // <<3103.8032, -250.1026, -3.3526>>       BREAK
				CASE 20  RETURN     <<3159.5354, -297.5987, -22.2973>>   BREAK //  <<3159.5354, -297.5987, -22.2973>>      BREAK
				CASE 21  RETURN     <<3144.7231, -298.9697, -8.0000>>   BREAK // <<3144.7231, -298.9697, -2.2412>>       BREAK
				CASE 22  RETURN     <<3169.7163, -324.8933, -3.3222>>    BREAK // <<3169.7163, -324.8933, -3.3222>>       BREAK
				CASE 23  RETURN     <<3109.2214, -351.5621, -18.0474>>   BREAK //  <<3109.2214, -351.5621, -18.0474>>      BREAK
				CASE 24  RETURN     <<3185.5476, -417.2469, -5.8910>>    BREAK // <<3185.5476, -417.2469, -5.8910>>       BREAK
				CASE 25  RETURN     <<3086.0469, -175.0174, -19.8420>>   BREAK //  <<3086.0469, -175.0174, -19.8420>>      BREAK
				CASE 26  RETURN     <<3238.8218, -374.8987, -17.5602>>   BREAK //  <<3238.8218, -374.8987, -17.5602>>      BREAK
				CASE 27  RETURN     <<3178.9041, -293.3883, -15.1235>>   BREAK //  <<3178.9041, -293.3883, -15.1235>>      BREAK
				CASE 28  RETURN     <<3127.9429, -395.7200, -21.3886>>   BREAK //  <<3127.9429, -395.7200, -21.3886>>      BREAK
				CASE 29  RETURN     <<3201.6145, -256.6116, -4.0000>>   BREAK // <<3201.6145, -256.6116, -2.8936>>       BREAK
				CASE 30  RETURN     <<3118.2673, -194.1014, -19.5541>>   BREAK //  <<3118.2673, -194.1014, -19.5541>>      BREAK
				CASE 31  RETURN     <<3078.6411, -147.0168, -16.0298>>   BREAK //  <<3078.6411, -147.0168, -16.0298>>      BREAK
				CASE 32  RETURN     <<3053.7427, -243.9219, -4.0000>>    BREAK // <<3053.7427, -243.9219, -2.3611>>       BREAK
				CASE 33  RETURN     <<3021.0601, -413.0354, -8.4086>>    BREAK // <<3021.0601, -413.0354, -8.4086>>       BREAK
				CASE 34  RETURN     <<3102.27, -400.6252, -26.5297>>     BREAK //<<3102.27, -400.6252, -26.5297>>        BREAK
				CASE 35  RETURN     <<3027.9985, -459.912, -23.7804>>    BREAK // <<3027.9985, -459.912, -23.7804>>       BREAK
				CASE 36  RETURN     <<3076.0696, -491.1312, -21.1518>>   BREAK //  <<3076.0696, -491.1312, -21.1518>>      BREAK
				CASE 37  RETURN 	<<3132.9353, -434.1494, -36.4362>>	 BREAK //<<3132.9353, -434.1494, -36.4362>>	    BREAK
				CASE 38  RETURN     <<3185.1057, -468.8703, -23.9478>>   BREAK //  <<3185.1057, -468.8703, -23.9478>>      BREAK
				CASE 39  RETURN     <<3346.8518, -432.3822, -44.1609>>   BREAK //  <<3346.8518, -432.3822, -44.1609>>      BREAK
				CASE 40  RETURN     <<3324.2793, -369.7613, -64.3232>>   BREAK //  <<3324.2793, -369.7613, -64.3232>>      BREAK
				CASE 41  RETURN     <<3269.3613, -238.7943, -8.1386>>    BREAK // <<3269.3613, -238.7943, -8.1386>>       BREAK
				CASE 42  RETURN     <<3054.6138, -468.0908, -38.0196>>   BREAK //  <<3054.6138, -468.0908, -38.0196>>      BREAK
				CASE 43  RETURN     <<3095.7271, -470.5118, -26.1035>>   BREAK //  <<3095.7271, -470.5118, -26.1035>>      BREAK
				CASE 44  RETURN     <<3211.5771, -478.0594, -45.7608>>   BREAK //  <<3211.5771, -478.0594, -45.7608>>      BREAK
				CASE 45  RETURN     <<3257.281, -417.4969, -36.4075>>    BREAK // <<3257.281, -417.4969, -36.4075>>       BREAK
				CASE 46  RETURN     <<3160.0386, -429.4074, -21.2846>>   BREAK //  <<3160.0386, -429.4074, -21.2846>>      BREAK
				CASE 47  RETURN   	<<3256.0093, -395.3117, -23.4135>>	 BREAK //<<3256.0093, -395.3117, -23.4135>>		BREAK
				CASE 48  RETURN     <<3242.3472, -357.9663, -49.5796>>   BREAK //  <<3242.3472, -357.9663, -49.5796>>      BREAK
				CASE 49  RETURN     <<3161.9741, -320.1731, -19.2641>>   BREAK //  <<3161.9741, -320.1731, -19.2641>>      BREAK
				
				
				
				
				
				
				
				
			ENDSWITCH
		ENDIF
		
		IF  serverBD.MapArea = PLANENORTH
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				serverBD.brandomcoordinatesgenerated = TRUE
				serverBD.vareablipcoordinates =  GET_AREA_BLIP_COORDINATES(PLANENORTH)			//PADDLESTEAMER	
				serverBD.inumberofcheckpoints = NUM_CHECKPOINTS		
			ENDIF
			SWITCH checkpoint_number
				CASE 0   RETURN 		<<-940.6840, 6558.5083, -25.2199>>		BREAK
				CASE 1   RETURN         <<-1020.4067, 6571.3989, -23.3120>>      BREAK
				CASE 2   RETURN         <<-1015.4128, 6506.8438, -29.3461>>      BREAK
				CASE 3   RETURN         <<-1032.7959, 6506.2920, -17.9195>>      BREAK
				CASE 4   RETURN         <<-902.9766, 6606.6680, -27.9263>>      BREAK
				CASE 5   RETURN         <<-987.1236, 6567.1973, -8.0592>>      BREAK
				CASE 6   RETURN         <<-946.2492, 6659.7373, -20.8487>>      BREAK
				CASE 7   RETURN         <<-855.8766, 6613.1421, -6.4414>>      BREAK
				CASE 8   RETURN         <<-881.7035, 6633.5645, -19.5167>>      BREAK
				CASE 9   RETURN         <<-911.9145, 6654.6045, -28.2485>>      BREAK
				CASE 10  RETURN         <<-802.5884, 6665.6699, -8.6216>>      BREAK
				CASE 11  RETURN         <<-907.0857, 6718.4390, -16.6377>>      BREAK
				CASE 12  RETURN         <<-920.1297, 6681.9951, -22.0983>>      BREAK
				CASE 13  RETURN         <<-981.9054, 6699.1987,  -27.5578>>      BREAK
				CASE 14  RETURN         <<-942.4011, 6713.8462, -40.4849>>      BREAK
				CASE 15  RETURN         <<-978.6621, 6640.1069, -25.0549>>      BREAK
				CASE 16  RETURN         <<-1040.0034, 6576.2139, -7.7223>>      BREAK
				CASE 17  RETURN         <<-808.7715, 6595.1519, -21.9147>>      BREAK
				CASE 18  RETURN         <<-837.3305, 6634.9102, -22.2648>>      BREAK
				CASE 19  RETURN         <<-990.1726, 6725.2500, -39.5781>>      BREAK
				CASE 20  RETURN         <<-992.2587, 6666.2461, -30.4054>>      BREAK
				CASE 21  RETURN         <<-1029.7080, 6608.9102, -25.4515>>      BREAK
				CASE 22  RETURN         <<-888.2024, 6528.3452, -15.7453>>      BREAK
				CASE 23  RETURN         <<-932.7153, 6480.5415, -17.2071>>      BREAK
				CASE 24  RETURN         <<-1047.8470, 6475.6240, -10.5532>>      BREAK
				CASE 25  RETURN         <<-1052.6670, 6544.7563, -26.1796>>      BREAK
				CASE 26  RETURN         <<-917.3282, 6625.9258, -22.2058>>      BREAK
				CASE 27  RETURN         <<-766.0142, 6611.6714, -13.7582>>      BREAK
				CASE 28  RETURN         <<-936.9161, 6544.0371, -4.6000>>     BREAK
				CASE 29  RETURN         <<-1035.7434, 6570.1060, -7.8606>>      BREAK
				CASE 30  RETURN         <<-1003.443, 6708.4683, -58.9846>>      BREAK
				CASE 31  RETURN         <<-995.2881, 6704.0405, -41.1943>>      BREAK
				CASE 32  RETURN         <<-1061.6298, 6692.3784, -43.392>>      BREAK
				CASE 33  RETURN         <<-1055.6711, 6651.2886, -41.1978>>      BREAK
				CASE 34  RETURN         <<-1092.413, 6608.6079, -33.0847>>      BREAK
				CASE 35  RETURN         <<-1117.7777, 6580.1128, -9.8624>>      BREAK
				CASE 36  RETURN         <<-1087.6035, 6498.543, -25.4028>>      BREAK
				CASE 37  RETURN 		<<-1042.3569, 6478.0234, -25.6903>>	    BREAK
				CASE 38  RETURN         <<-1017.3608, 6409.9644, -14.2984>>      BREAK
				CASE 39  RETURN         <<-992.8898, 6387.5522, -16.2148>>      BREAK
				CASE 40  RETURN         <<-982.5092, 6422.2734, -11.8426>>      BREAK
				CASE 41  RETURN         <<-927.6414, 6430.3569, -16.7373>>      BREAK
				CASE 42  RETURN         <<-972.7563, 6476.6621, -25.8281>>      BREAK
				CASE 43  RETURN         <<-879.3123, 6463.0464, -11.2318>>      BREAK
				CASE 44  RETURN         <<-804.7411, 6542.3496, -19.5496>>      BREAK
				CASE 45  RETURN         <<-755.4528, 6575.4229, -8.7002>>      BREAK
				CASE 46  RETURN         <<-772.2038, 6711.6064, -23.5659>>      BREAK
				CASE 47  RETURN   		<<-846.384, 6744.7744, -20.1361>>		BREAK
				CASE 48  RETURN         <<-866.293, 6697.9824, -18.1454>>      BREAK
				CASE 49  RETURN         <<-938.1859, 6785.8535, -32.4493>>      BREAK  
			ENDSWITCH
		ENDIF	
		

		IF  serverBD.MapArea = PADDLESTEAMER
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				serverBD.brandomcoordinatesgenerated = TRUE
				serverBD.vareablipcoordinates =  GET_AREA_BLIP_COORDINATES(PADDLESTEAMER)			//PADDLESTEAMER	
				serverBD.inumberofcheckpoints = NUM_CHECKPOINTS		
			ENDIF
			SWITCH checkpoint_number
				CASE 0   RETURN 		<<2652.2661, -1418.4449, -4.0544>>		BREAK
				CASE 1   RETURN         <<2654.3518, -1448.4978, -15.3399>>     BREAK
				CASE 2   RETURN         <<2666.7666, -1427.1753, -20.4170>>     BREAK
				CASE 3   RETURN         <<2658.1392, -1420.1147, -16.0462>>     BREAK
				CASE 4   RETURN         <<2643.4148, -1391.1132, -5.9669>>    	BREAK
				CASE 5   RETURN         <<2689.4155, -1408.0809, -8.2130>>  	 BREAK
				CASE 6   RETURN         <<2698.8721, -1368.3978, -10.9564>>    BREAK
				CASE 7   RETURN         <<2685.2461, -1398.0529, -17.2692>>   BREAK
				CASE 8   RETURN         <<2661.2402, -1396.7802, -17.5751>>    BREAK
				CASE 9   RETURN         <<2638.0996, -1426.5590, -17.6362>>    BREAK
				CASE 10  RETURN         <<2680.4631, -1355.8827, -10.6394>>   BREAK
				CASE 11  RETURN         <<2675.4177, -1361.2708, -17.9876>>    BREAK
				CASE 12  RETURN         <<2631.8098, -1369.9413, -9.6194>>    BREAK
				CASE 13  RETURN         <<2625.3137, -1401.5372, -11.7442>>     BREAK
				CASE 14  RETURN         <<2702.9819, -1435.5287, -14.8009>>    BREAK
				CASE 15  RETURN         <<2727.1418, -1365.1554, -12.2438>>    BREAK
				CASE 16  RETURN         <<2649.8838, -1424.1425, -18.2053>>    BREAK
				CASE 17  RETURN         <<2638.3977, -1314.7654, -15.1045>>    BREAK
				CASE 18  RETURN         <<2727.0430, -1444.6843, -8.6098>>    BREAK
				CASE 19  RETURN         <<2589.5774, -1459.8925, -8.4694>>    BREAK
				CASE 20  RETURN         <<2641.3074, -1441.0742, -14.6031>>    BREAK
				CASE 21  RETURN         <<2645.7212, -1488.5498, -19.2787>>     BREAK
				CASE 22  RETURN         <<2649.4368, -1375.3959, -15.9989>>    BREAK
				CASE 23  RETURN         <<2601.1318, -1409.9150, -5.8628>>    BREAK
				CASE 24  RETURN         <<2693.9204, -1474.9280, -13.5258>>     BREAK
				CASE 25  RETURN         <<2733.8940, -1413.8340, -9.2974>>     BREAK
				CASE 26  RETURN         <<2604.1470, -1437.5265, -12.8387>>    BREAK
				CASE 27  RETURN         <<2665.8901, -1493.6519, -16.7828>>    BREAK
				CASE 28  RETURN         <<2750.2664, -1346.6560, -4.0896>>   BREAK
				CASE 29  RETURN         <<2610.1858, -1327.5623, -3.4805>>    BREAK
				CASE 30  RETURN         <<2693.2979, -1518.5035, -13.9086>>    BREAK
				CASE 31  RETURN         <<2576.1875, -1534.4918, -11.9347>>    BREAK
				CASE 32  RETURN         <<2635.0295, -1540.928, -11.61>>   	 BREAK
				CASE 33  RETURN         <<2560.1077, -1462.671, -9.8545>>     BREAK
				CASE 34  RETURN         <<2604.9817, -1384.6818, -9.6481>>    BREAK
				CASE 35  RETURN         <<2724.7134, -1256.7545, -18.9802>>    BREAK
				CASE 36  RETURN         <<2726.7712, -1200.4049, -13.2008>>    BREAK
				CASE 37  RETURN 		<<2753.0391, -1260.0951, -14.3599>>    BREAK
				CASE 38  RETURN         <<2691.3081, -1307.2642, -18.7307>>     BREAK
				CASE 39  RETURN         <<2693.6826, -1423.661, -5.9968>>    	BREAK
				CASE 40  RETURN         <<2659.946, -1279.5763, -19.4202>>    	BREAK
				CASE 41  RETURN         <<2653.2209, -1329.4376, -17.5979>>    BREAK
				CASE 42  RETURN         <<2693.2224, -1379.7386, -16.9807>>    BREAK
				CASE 43  RETURN         <<2718.5437, -1393.1926, -16.1313>>    BREAK
				CASE 44  RETURN         <<2633.145, -1540.8651, -4.2618>>    BREAK
				CASE 45  RETURN         <<2679.1069, -1566.7404, -9.1924>>   BREAK
				CASE 46  RETURN         <<2648.0991, -1341.8434, -11.7505>>    BREAK
				CASE 47  RETURN   		<<2645.3352, -1274.2909, -14.0425>>		BREAK
				CASE 48  RETURN         <<2735.6624, -1329.7449, -12.1964>>   BREAK
				CASE 49  RETURN         <<2767.3633, -1201.9155, -17.5772>>    BREAK
				
				
			ENDSWITCH
		ENDIF	
		
		IF  serverBD.MapArea = CARGOSHIP
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				serverBD.brandomcoordinatesgenerated = TRUE
				serverBD.vareablipcoordinates =  GET_AREA_BLIP_COORDINATES(CARGOSHIP)			//CARGOSHIP	
				serverBD.inumberofcheckpoints = NUM_CHECKPOINTS		
			ENDIF
			SWITCH checkpoint_number
				CASE 0   RETURN 		<<-123.4617, -2869.9399, -16.3995>>		BREAK
				CASE 1   RETURN         <<-138.7137, -2871.6477, -13.6277>>     BREAK
				CASE 2   RETURN         <<-148.4602, -2858.5728, -14.1256>>     BREAK
				CASE 3   RETURN         <<-159.7547, -2858.2988, -11.114>>      BREAK
				CASE 4   RETURN         <<-180.8156, -2856.6563, -13.2429>>     BREAK
				CASE 5   RETURN         <<-178.0286, -2836.46, -11.5266>>       BREAK
				CASE 6   RETURN         <<-170.6358, -2884.3755, -21.4938>>     BREAK
				CASE 7   RETURN         <<-107.0947, -2872.7434, -18.7651>>     BREAK
				CASE 8   RETURN         <<-197.4001, -2876.7644, -16.9466>>     BREAK
				CASE 9   RETURN         <<-211.4926, -2853.5381, -9.8861>>      BREAK
				CASE 10  RETURN         <<-241.3681, -2861.2898, -17.7971>>     BREAK
				CASE 11  RETURN         <<-242.2805, -2879.2493, -21.514>>      BREAK
				CASE 12  RETURN         <<-232.0783, -2886.665, -19.3111>>      BREAK
				CASE 13  RETURN         <<-279.0308, -2894.552, -13.4571>>      BREAK
				CASE 14  RETURN         <<-267.7975, -2896.8467, -8.563>>       BREAK
				CASE 15  RETURN         <<-303.1421, -2895.1946, -12.8636>>     BREAK
				CASE 16  RETURN         <<-284.3269, -2879.3818, -13.198>>      BREAK
				CASE 17  RETURN         <<-270.1964, -2874.6824, -15.5478>>     BREAK
				CASE 18  RETURN         <<-230.8368, -2883.7959, -5.773>>       BREAK
				CASE 19  RETURN         <<-122.9077, -2856.7639, -7.3161>>      BREAK
				CASE 20  RETURN         <<-108.1399, -2848.085, -5.1325>>       BREAK
				CASE 21  RETURN         <<-130.6012, -2839.3906, -11.9248>>     BREAK
				CASE 22  RETURN         <<-187.7467, -2764.2751, -12.927>>      BREAK
				CASE 23  RETURN         <<-187.997, -2800.6997, -14.3917>>      BREAK
				CASE 24  RETURN         <<-203.4084, -2811.9695, -12.1949>>     BREAK
				CASE 25  RETURN         <<-241.163, -2822.9177, -16.996>>       BREAK
				CASE 26  RETURN         <<-272.5608, -2844.2, -13.733>>         BREAK
				CASE 27  RETURN         <<-317.5323, -2846.3394, -7.7371>>      BREAK
				CASE 28  RETURN         <<-322.6858, -2916.3984, -13.2445>>     BREAK
				CASE 29  RETURN         <<-329.1637, -2945.7625, -15.1612>>     BREAK
				CASE 30  RETURN         <<-296.3756, -2964.9553, -16.5131>>     BREAK
				CASE 31  RETURN         <<-281.2891, -2930.6091, -16.9224>>     BREAK
				CASE 32  RETURN         <<-250.3996, -2915.875, -22.7967>>      BREAK
				CASE 33  RETURN         <<-212.148, -2939.8044, -27.3775>>      BREAK
				CASE 34  RETURN         <<-161.8391, -2918.9211, -18.3542>>     BREAK
				CASE 35  RETURN         <<-123.5734, -2889.8223, -24.0321>>     BREAK
				CASE 36  RETURN         <<-40.3317, -2871.4438, -21.5325>>      BREAK
				CASE 37  RETURN 		<<-28.3143, -2840.5818, -10.0000>>       BREAK
				CASE 38  RETURN         <<-74.5391, -2831.7007, -12.069>>       BREAK
				CASE 39  RETURN         <<-97.5883, -2808.7834, -10.6454>>      BREAK
				CASE 40  RETURN         <<-121.2626, -2820.7939, -13.3995>>     BREAK
				CASE 41  RETURN         <<-133.0665, -2788.7717, -11.1374>>     BREAK
				CASE 42  RETURN         <<-153.0454, -2814.6365, -12.3484>>     BREAK
				CASE 43  RETURN         <<-169.498, -2782.3015, -11.2845>>      BREAK
				CASE 44  RETURN         <<-182.149, -2816.4048, -8.201>>        BREAK
				CASE 45  RETURN         <<-222.4776, -2791.6038, -16.0357>>     BREAK
				CASE 46  RETURN         <<-227.1795, -2818.8826, -18.2681>>     BREAK
				CASE 47  RETURN   		<<-254.0901, -2836.053, -15.0898>>      BREAK
				CASE 48  RETURN         <<-174.4692, -2955.3035, -24.8742>>     BREAK
				CASE 49  RETURN         <<-119.8888, -2933.4971, -27.3452>>     BREAK
				
				
			ENDSWITCH           
		ENDIF                   
		
		IF  serverBD.MapArea = MILITARYDEBRIS
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				serverBD.brandomcoordinatesgenerated = TRUE
				serverBD.vareablipcoordinates =  GET_AREA_BLIP_COORDINATES(MILITARYDEBRIS)			//MILITARYDEBRIS	
				serverBD.inumberofcheckpoints = NUM_CHECKPOINTS	
			ENDIF
			SWITCH checkpoint_number
				CASE 0   RETURN 		<<4128.6455, 3536.8157, -27.2726>>		BREAK
				CASE 1   RETURN         <<4142.2495, 3513.8633, -35.5099>>      BREAK
				CASE 2   RETURN         <<4124.6582, 3509.5256, -29.0893>>      BREAK
				CASE 3   RETURN         <<4156.7993, 3518.2576, -40.8704>>      BREAK
				CASE 4   RETURN         <<4157.6187, 3503.0051, -33.465>>       BREAK
				CASE 5   RETURN         <<4142.7183, 3528.0044, -37.9942>>      BREAK
				CASE 6   RETURN         <<4169.4155, 3538.0305, -46.6281>>      BREAK
				CASE 7   RETURN         <<4174.0557, 3520.2874, -25.6862>>      BREAK
				CASE 8   RETURN         <<4108.3271, 3514.6907, -26.2199>>      BREAK
				CASE 9   RETURN         <<4138.3901, 3549.1982, -32.0103>>      BREAK
				CASE 10  RETURN         <<4149.3594, 3575.9976, -40.3522>>      BREAK
				CASE 11  RETURN         <<4202.2612, 3579.7288, -50.913>>       BREAK
				CASE 12  RETURN         <<4196.5366, 3600.012, -43.763>>        BREAK
				CASE 13  RETURN         <<4187.8345, 3579.0437, -42.4474>>      BREAK
				CASE 14  RETURN         <<4167.6143, 3487.6716, -21.3586>>      BREAK
				CASE 15  RETURN         <<4194.7886, 3481.7371, -14.6297>>      BREAK
				CASE 16  RETURN         <<4136.9409, 3493.0679, -16.5569>>      BREAK
				CASE 17  RETURN         <<4114.3413, 3497.0859, -27.7748>>      BREAK
				CASE 18  RETURN         <<4111.5249, 3507.9795, -17.8111>>      BREAK
				CASE 19  RETURN         <<4102.7275, 3529.9946, -22.0589>>      BREAK
				CASE 20  RETURN         <<4102.7271, 3529.9946, -12.9261>>      BREAK
				CASE 21  RETURN         <<4138.7583, 3562.23, -30.3732>>        BREAK
				CASE 22  RETURN         <<4164.3096, 3568.0574, -7.4719>>       BREAK
				CASE 23  RETURN         <<4160.3154, 3562.0583, -10.2153>>       BREAK
				CASE 24  RETURN         <<4296.9575, 3511.3936, -51.261>>       BREAK
				CASE 25  RETURN         <<4190.939, 3554.3811, -30.0441>>       BREAK
				CASE 26  RETURN         <<4108.9399, 3655.3833, -14.8135>>      BREAK
				CASE 27  RETURN         <<4239.1357, 3599.4829, -44.3814>>      BREAK
				CASE 28  RETURN         <<4238.8828, 3586.7542, -25.5911>>      BREAK
				CASE 29  RETURN         <<4243.8335, 3505.8335, -20.6198>>      BREAK
				CASE 30  RETURN         <<4197.8359, 3562.8972, -25.189>>       BREAK
				CASE 31  RETURN         <<4158.5879, 3665.2769, -33.9307>>      BREAK
				CASE 32  RETURN         <<4200.7139, 3424.8804, -20.2091>>      BREAK
				CASE 33  RETURN         <<4162.6216, 3634.2463, -33.5816>>      BREAK
				CASE 34  RETURN         <<4162.7822, 3617.425, -12.2804>>       BREAK
				CASE 35  RETURN         <<4160.0576, 3605.7239, -32.3609>>      BREAK
				CASE 36  RETURN         <<4221.9116, 3553.8462, -23.0533>>      BREAK
				CASE 37  RETURN 		<<4221.9043, 3558.8525, -17.8789>>      BREAK
				CASE 38  RETURN         <<4080.092, 3479.7007, -14.235>>        BREAK
				CASE 39  RETURN         <<4066.0889, 3526.0659, -15.4203>>      BREAK
				CASE 40  RETURN         <<4154.0923, 3411.0771, -46.2615>>      BREAK
				CASE 41  RETURN         <<4072.0947, 3614.0728, -9.9735>>      BREAK
				CASE 42  RETURN         <<4066.0957, 3536.072, -10.7735>>       BREAK
				CASE 43  RETURN         <<4113.668, 3597.4861, -31.9989>>       BREAK
				CASE 44  RETURN         <<4096.7964, 3604.8677, -16.0383>>      BREAK
				CASE 45  RETURN         <<4135.1133, 3615.6663, -17.9848>>      BREAK
				CASE 46  RETURN         <<4133.8428, 3640.7646, -16.4483>>      BREAK
				CASE 47  RETURN   		<<4116.5127, 3627.5146, -26.1424>>      BREAK
				CASE 48  RETURN         <<4148.9253, 3621.0298, -15.3541>>      BREAK
				CASE 49  RETURN         <<4172.7407, 3640.3933, -19.4547>>      BREAK
			ENDSWITCH          
		ENDIF	               
		
		
		IF  serverBD.MapArea = PLANESOUTH
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				serverBD.brandomcoordinatesgenerated = TRUE
				serverBD.vareablipcoordinates =  GET_AREA_BLIP_COORDINATES(PLANESOUTH)			//PLANESOUTH	
				serverBD.inumberofcheckpoints = NUM_CHECKPOINTS	
			ENDIF
			SWITCH checkpoint_number
				CASE 0   RETURN 	<<1788.7787, -2959.7141, -38.8306>>		BREAK
				CASE 1   RETURN     <<1762.0347, -2989.6985, -39.914>>     BREAK
				CASE 2   RETURN     <<1786.4927, -2975.6926, -42.3752>>     BREAK
				CASE 3   RETURN     <<1821.0055, -2965.8862, -45.5229>>     BREAK
				CASE 4   RETURN     <<1829.8605, -2939.6309, -46.0756>>     BREAK
				CASE 5   RETURN     <<1835.6147, -2963.9944, -46.8441>>     BREAK
				CASE 6   RETURN     <<1834.27, -2990.7771, -48.3743>>       BREAK
				CASE 7   RETURN     <<1792.9769, -2940.7688, -38.3727>>     BREAK
				CASE 8   RETURN     <<1861.3779, -2944.7083, -40.415>>     BREAK
				CASE 9   RETURN     <<1861.5988, -2937.7041, -43.0719>>     BREAK
				CASE 10  RETURN     <<1888.6262, -2951.3494, -40.0834>>     BREAK
				CASE 11  RETURN     <<1893.4778, -2937.9402, -35.4192>>     BREAK
				CASE 12  RETURN     <<1814.0315, -2945.8594, -39.3598>>     BREAK
				CASE 13  RETURN     <<1798.4218, -2916.2942, -30.0976>>     BREAK
				CASE 14  RETURN     <<1756.9698, -2992.4194, -24.831>>     BREAK
				CASE 15  RETURN     <<1799.1189, -2902.2722, -28.3129>>     BREAK
				CASE 16  RETURN     <<1866.496, -2928.1221, -40.468>>      BREAK
				CASE 17  RETURN     <<1883.4008, -2925.5789, -31.1202>>     BREAK
				CASE 18  RETURN     <<1855.723, -2976.4497, -50.0471>>      BREAK
				CASE 19  RETURN     <<1857.5791, -2976.0193, -25.141>>     BREAK
				CASE 20  RETURN     <<1777.9792, -2905.3662, -12.1964>>     BREAK
				CASE 21  RETURN     <<1791.0074, -3004.0635, -36.1629>>     BREAK
				CASE 22  RETURN     <<1745.0244, -2993.7517, -53.8849>>     BREAK
				CASE 23  RETURN     <<1750.0521, -3016.4512, -50.4857>>     BREAK
				CASE 24  RETURN     <<1798.3268, -3046.9465, -43.0121>>     BREAK
				CASE 25  RETURN     <<1825.4607, -3032.8965, -30.4531>>     BREAK
				CASE 26  RETURN     <<1896.5208, -2995.2253, -16.2614>>     BREAK
				CASE 27  RETURN     <<1888.6799, -2907.6248, -20.249>>     BREAK
				CASE 28  RETURN     <<1870.2258, -2881.2803, -24.3322>>     BREAK
				CASE 29  RETURN     <<1861.718, -2868.9587, -19.209>>      BREAK
				CASE 30  RETURN     <<1813.0112, -2881.74, -14.7041>>       BREAK
				CASE 31  RETURN     <<1790.4539, -2886.9924, -10.1967>>     BREAK
				CASE 32  RETURN     <<1759.0607, -2897.5251, -24.7666>>     BREAK
				CASE 33  RETURN     <<1756.3274, -2945.521, -29.9931>>      BREAK
				CASE 34  RETURN     <<1814.3345, -2932.6968, -21.4609>>     BREAK
				CASE 35  RETURN     <<1913.3959, -2986.4114, -24.5091>>     BREAK
				CASE 36  RETURN     <<1765.0681, -3045.4082, -44.3087>>     BREAK
				CASE 37  RETURN 	<<1724.7415, -3014.0247, -39.0973>>     BREAK
				CASE 38  RETURN     <<1768.7816, -3027.7827, -44.8443>>     BREAK
				CASE 39  RETURN     <<1938.8844, -2959.583, -30.9051>>      BREAK
				CASE 40  RETURN     <<1903.1936, -2922.4639, -9.1981>>     BREAK
				CASE 41  RETURN     <<1858.0865, -2902.4063, -15.2284>>       BREAK
				CASE 42  RETURN     <<1731.573, -2956.3555, -25.0112>>      BREAK
				CASE 43  RETURN     <<1763.3627, -2912.459, -27.5669>>      BREAK
				CASE 44  RETURN     <<1802.7463, -2984.4978, -43.4314>>     BREAK
				CASE 45  RETURN     <<1771.8168, -3067.3589, -59.3156>>     BREAK
				CASE 46  RETURN     <<1792.9351, -3087.551, -20.4934>>      BREAK
				CASE 47  RETURN   	<<1826.7393, -3083.127, -39.8522>>      BREAK
				CASE 48  RETURN     <<1865.4401, -2873.1184, -11.6081>>     BREAK
				CASE 49  RETURN     <<1853.0006, -3005.0791, -35.9146>>     BREAK
                                                    
			ENDSWITCH         
		ENDIF	              
		
		IF  serverBD.MapArea = SUBMARINE
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				serverBD.brandomcoordinatesgenerated = TRUE
				serverBD.vareablipcoordinates =  GET_AREA_BLIP_COORDINATES(SUBMARINE)			//SUBMARINE	
				serverBD.inumberofcheckpoints = NUM_CHECKPOINTS		
			ENDIF
			SWITCH checkpoint_number
				CASE 0   RETURN 	<<2672.4363, 6650.9878, -22.6184>>		BREAK
				CASE 1   RETURN     <<2652.9883, 6656.752, -23.2354>>     BREAK
				CASE 2   RETURN     <<2658.5671, 6638.355, -20.0689>>     BREAK
				CASE 3   RETURN     <<2676.1489, 6691.3003, -6.5556>>     BREAK
				CASE 4   RETURN     <<2672.1191, 6642.4902, -19.7414>>     BREAK
				CASE 5   RETURN     <<2710.7083, 6631.0215, -26.8888>>     BREAK
				CASE 6   RETURN     <<2711.4824, 6632.9795, -10.609>>     BREAK
				CASE 7   RETURN     <<2727.6541, 6647.5591, -23.194>>     BREAK
				CASE 8   RETURN     <<2713.0579, 6675.0903, -22.1789>>     BREAK
				CASE 9   RETURN     <<2674.4756, 6693.5415, -23.6009>>     BREAK
				CASE 10  RETURN     <<2641.6038, 6688.5957, -22.4951>>     BREAK
				CASE 11  RETURN     <<2660.5562, 6669.3579, -31.0305>>     BREAK
				CASE 12  RETURN     <<2638.3625, 6651.4712, -18.2788>>     BREAK
				CASE 13  RETURN     <<2683.2371, 6605.4839, -18.9837>>     BREAK
				CASE 14  RETURN     <<2717.0051, 6598.3584, -15.7131>>     BREAK
				CASE 15  RETURN     <<2737.6011, 6626.9756, -18.4517>>     BREAK
				CASE 16  RETURN     <<2756.2141, 6614.9956, -14.3817>>     BREAK
				CASE 17  RETURN     <<2754.5671, 6656.9023, -27.2893>>     BREAK
				CASE 18  RETURN     <<2614.5745, 6660.5386, -10.9642>>     BREAK
				CASE 19  RETURN     <<2619.6345, 6622.2974, -8.2971>>     BREAK
				CASE 20  RETURN     <<2647.5107, 6581.7354, -6.9138>>     BREAK
				CASE 21  RETURN     <<2697.8953, 6584.8003, -13.0691>>     BREAK
				CASE 22  RETURN     <<2739.21, 6591.8315, -13.1861>>     BREAK
				CASE 23  RETURN     <<2788.2087, 6620.3413, -20.8984>>     BREAK
				CASE 24  RETURN     <<2781.0984, 6649.3447, -10.9847>>     BREAK
				CASE 25  RETURN     <<2769.9355, 6676.397, -18.6507>>     BREAK
				CASE 26  RETURN     <<2738.5251, 6710.3774, -53.6591>>     BREAK
				CASE 27  RETURN     <<2726.0278, 6716.9497, -46.2289>>     BREAK
				CASE 28  RETURN     <<2687.5005, 6716.5869, -37.4639>>     BREAK
				CASE 29  RETURN     <<2628.6057, 6704.6812, -8.6546>>     BREAK
				CASE 30  RETURN     <<2579.1726, 6696.1494, -8.3973>>     BREAK
				CASE 31  RETURN     <<2594.4395, 6671.167, -10.1626>>     BREAK
				CASE 32  RETURN     <<2650.4004, 6742.4175, -49.6593>>     BREAK
				CASE 33  RETURN     <<2614.2781, 6747.9937, -30.2251>>     BREAK
				CASE 34  RETURN     <<2586.1401, 6737.2134, -18.2017>>     BREAK
				CASE 35  RETURN     <<2534.0286, 6675.5039, -17.3698>>     BREAK
				CASE 36  RETURN     <<2730.9282, 6576, -9.7398>>     BREAK
				CASE 37  RETURN 	<<2763.9316, 6589.6216, -13.4522>>     BREAK
				CASE 38  RETURN     <<2753.1101, 6569.8369, -9.4955>>     BREAK
				CASE 39  RETURN     <<2789.7864, 6671.5879, -26.5903>>     BREAK
				CASE 40  RETURN     <<2744.5547, 6667.3892, -26.1127>>     BREAK
				CASE 41  RETURN     <<2691.2639, 6665.334, -27.879>>     BREAK
				CASE 42  RETURN     <<2704.6826, 6714.3687, -54.7546>>     BREAK
				CASE 43  RETURN     <<2654.5793, 6705.4448, -26.775>>     BREAK
				CASE 44  RETURN     <<2625.5205, 6716.481, -25.5891>>     BREAK
				CASE 45  RETURN     <<2603.2063, 6710.2109, -21.4665>>     BREAK
				CASE 46  RETURN     <<2658.2629, 6600.373, -11.9487>>     BREAK
				CASE 47  RETURN   	<<2777.1631, 6631.2759, -20.4348>>     BREAK
				CASE 48  RETURN     <<2652.1406, 6617.9331, -18.9142>>     BREAK
				CASE 49  RETURN     <<2754.8911, 6681.7178, -26.7887>>     BREAK		
			ENDSWITCH            
		ENDIF	                 
			
		IF  serverBD.MapArea = MILITARYPLANE
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				serverBD.brandomcoordinatesgenerated = TRUE
				serverBD.vareablipcoordinates =  GET_AREA_BLIP_COORDINATES(MILITARYPLANE)			//MILITARYPLANE	
				serverBD.inumberofcheckpoints = NUM_CHECKPOINTS	
			ENDIF
			SWITCH checkpoint_number
				CASE 0   RETURN 	<<-3246.5149, 3669.7549, -30.2516>>		BREAK
				CASE 1   RETURN     <<-3282.4548, 3676.8379, -38.8842>>    BREAK
				CASE 2   RETURN     <<-3215.3323, 3669.156, -17.34>>    BREAK
				CASE 3   RETURN     <<-3228.2278, 3668.937, -18.7083>>    BREAK
				CASE 4   RETURN     <<-3218.2114, 3657.0703, -10.2778>>     BREAK
				CASE 5   RETURN     <<-3241.502, 3673.1274, -34.6267>>     BREAK
				CASE 6   RETURN     <<-3251.7153, 3690.4949, -24.8453>>    BREAK
				CASE 7   RETURN     <<-3263.1685, 3652.2498, -21.1263>>    BREAK
				CASE 8   RETURN     <<-3220.8542, 3637.2988, -34.6968>>     BREAK
				CASE 9   RETURN     <<-3265.7434, 3630.0962, -24.6169>>     BREAK
				CASE 10  RETURN     <<-3178.3459, 3690.1228, -7.8235>>     BREAK
				CASE 11  RETURN     <<-3228.4023, 3696.978, -16.5639>>     BREAK
				CASE 12  RETURN     <<-3198.7617, 3689.1438, -27.2023>>     BREAK
				CASE 13  RETURN     <<-3238.3293, 3711.1655, -41.9236>>     BREAK
				CASE 14  RETURN     <<-3184.9341, 3718.2632, -39.717>>     BREAK
				CASE 15  RETURN     <<-3214.3865, 3743.1648, -48.5735>>     BREAK
				CASE 16  RETURN     <<-3328.4321, 3616.2795, -52.7051>>     BREAK
				CASE 17  RETURN     <<-3265.9753, 3667.4148, -28.762>>     BREAK
				CASE 18  RETURN     <<-3265.8486, 3685.5654, -42.8826>>     BREAK
				CASE 19  RETURN     <<-3149.5488, 3629.4958, -18.7436>>    BREAK
				CASE 20  RETURN     <<-3168.2473, 3633.0078, -34.8681>>    BREAK
				CASE 21  RETURN     <<-3190.376, 3632.1267, -35.0561>>     BREAK
				CASE 22  RETURN     <<-3229.8459, 3568.4717, -24.091>>    BREAK
				CASE 23  RETURN     <<-3241.3584, 3566.7429, -14.1235>>     BREAK
				CASE 24  RETURN    <<-3238.9204, 3596.2209, -30.501>>      BREAK
				CASE 25  RETURN    <<-3175.9653, 3655.2839, -25.3718>>     BREAK
				CASE 26  RETURN    <<-3243.3218, 3623.8816, -32.8644>>      BREAK
				CASE 27  RETURN    <<-3286.5654, 3637.9048, -20.75>>      BREAK
				CASE 28  RETURN    <<-3287.0408, 3702.0332, -22.8537>>      BREAK
				CASE 29  RETURN    <<-3296.3403, 3659.3223, -13.6941>>     BREAK
				CASE 30  RETURN    <<-3293.8728, 3591.7991, -22.9375>>     BREAK
				CASE 31  RETURN    <<-3264.2705, 3585.8022, -21.8316>>     BREAK
				CASE 32  RETURN    <<-3215.4348, 3594.9497, -15.7747>>      BREAK
				CASE 33  RETURN    <<-3185.9956, 3604.0122, -19.0794>>      BREAK
				CASE 34  RETURN    <<-3153.5156, 3658.7744, -23.8419>>      BREAK
				CASE 35  RETURN    <<-3219.9114, 3713.4395, -18.3682>>      BREAK
				CASE 36  RETURN    <<-3207.4695, 3572.783, -19.8693>>    BREAK
				CASE 37  RETURN <<-3307.7856, 3599.4214, -22.347>> 	    BREAK
				CASE 38  RETURN    <<-3205.9634, 3759.5269, -25.0518>>      BREAK
				CASE 39  RETURN    <<-3178.5632, 3742.8982, -11.5727>>      BREAK
				CASE 40  RETURN    <<-3159.8792, 3728.7048, -25.1143>>      BREAK
				CASE 41  RETURN    <<-3154.8752, 3697.2456, -22.9222>>      BREAK
				CASE 42  RETURN    <<-3139.8276, 3669.6021, -21.5616>>      BREAK
				CASE 43  RETURN    <<-3169.7202, 3611.7202, -21.208>>     BREAK
				CASE 44  RETURN    <<-3261.8149, 3559.0398, -13.1608>>     BREAK
				CASE 45  RETURN    <<-3277.7822, 3703.1013, -19.8983>>      BREAK
				CASE 46  RETURN    <<-3309.3582, 3673.3, -18.6977>>     BREAK
				CASE 47  RETURN   <<-3309.0198, 3686.7458, -24.8755>>	     BREAK
				CASE 48  RETURN    <<-3224.4651, 3608.0884, -20.2441>>      BREAK
				CASE 49  RETURN    <<-3250.9675, 3757.1399, -29.3826>>      BREAK		
			ENDSWITCH            
		ENDIF	  
			
		IF  serverBD.MapArea = BATHYSCAPE
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				serverBD.brandomcoordinatesgenerated = TRUE
				serverBD.vareablipcoordinates =  GET_AREA_BLIP_COORDINATES(BATHYSCAPE)			//BATHYSCAPE	
				serverBD.inumberofcheckpoints = NUM_CHECKPOINTS		
			ENDIF
			SWITCH checkpoint_number
				CASE 0   RETURN 	<<-2835.728, -482.3605, -30.0288>>		BREAK
				CASE 1   RETURN     <<-2839.0781, -438.0658, -29.7548>>    BREAK
				CASE 2   RETURN     <<-2825.1208, -412.9836, -18.7924>> BREAK
				CASE 3   RETURN     <<-2857.5549, -415.7047, -32.1892>>   BREAK
				CASE 4   RETURN     <<-2836.2773, -469.6712, -44.3442>>     BREAK
				CASE 5   RETURN     <<-2840.4155, -428.2162, -33.3264>>    BREAK
				CASE 6   RETURN     <<-2853.7854, -445.6909, -35.8564>>    BREAK
				CASE 7   RETURN     <<-2834.699, -490.2503, -61.9216>>    BREAK
				CASE 8   RETURN     <<-2835.5803, -520.8021, -54.9401>>     BREAK
				CASE 9   RETURN     <<-2833.896, -547.5646, -53.9204>>     BREAK
				CASE 10  RETURN     <<-2832.0117, -625.4982, -52.1343>>    BREAK
				CASE 11  RETURN     <<-2767.8115, -585.6926, -42.2241>>    BREAK
				CASE 12  RETURN     <<-2835.8755, -507.0797, -40.8141>>     BREAK
				CASE 13  RETURN     <<-2842.7405, -460.0384, -12.8564>>     BREAK
				CASE 14  RETURN     <<-2794.9966, -452.9641, -29.8533>>    BREAK
				CASE 15  RETURN     <<-2791.6936, -440.0353, -8.7679>>     BREAK
				CASE 16  RETURN     <<-2795.5725, -486.5534, -49.2621>>     BREAK
				CASE 17  RETURN     <<-2858.9238, -503.7948, -54.9654>>    BREAK
				CASE 18  RETURN     <<-2875.5867, -493.8596, -41.5212>>     BREAK
				CASE 19  RETURN     <<-2878.0208, -395.8189, -28.0632>>    BREAK
				CASE 20  RETURN     <<-2897.2778, -427.1053, -35.0445>>    BREAK
				CASE 21  RETURN     <<-2873.7686, -439.9445, -34.7849>>    BREAK
				CASE 22  RETURN     <<-2772.9868, -469.3011, -41.8144>>   BREAK
				CASE 23  RETURN     <<-2776.6851, -533.2573, -48.0774>>     BREAK
				CASE 24  RETURN     <<-2769.8662, -560.6915, -38.9118>>    BREAK
				CASE 25  RETURN     <<-2790.9841, -633.6831, -42.1443>>    BREAK
				CASE 26  RETURN     <<-2798.7085, -585.2871, -46.9006>>     BREAK
				CASE 27  RETURN     <<-2835.1199, -533.6248, -35.2752>>    BREAK
				CASE 28  RETURN     <<-2905.103, -450.1064, -34.1608>>     BREAK
				CASE 29  RETURN     <<-2748.3013, -412.1993, -14.6037>>    BREAK
				CASE 30  RETURN     <<-2896.709, -483.8218, -43.4204>>    BREAK
				CASE 31  RETURN     <<-2874.8511, -461.9164, -21.9398>>    BREAK
				CASE 32  RETURN     <<-2884.325, -495.3757, -13.8974>>     BREAK
				CASE 33  RETURN     <<-2812.7571, -503.7162, -15.1477>>     BREAK
				CASE 34  RETURN     <<-2802.8823, -501.8399, -36.2526>>     BREAK
				CASE 35  RETURN     <<-2807.2922, -536.3256, -29.5052>>     BREAK
				CASE 36  RETURN     <<-2868.4082, -526.4774, -27.7767>>    BREAK
				CASE 37  RETURN 	<<-2832.6116, -583.6776, -23.6061>>    BREAK
				CASE 38  RETURN     <<-2807.0701, -564.2064, -31.064>>     BREAK
				CASE 39  RETURN     <<-2866.6667, -568.1177, -31.1873>>     BREAK
				CASE 40  RETURN     <<-2832.2354, -591.0103, -23.8917>>     BREAK
				CASE 41  RETURN     <<-2808.283, -642.5566, -15.098>>     BREAK
				CASE 42  RETURN     <<-2807.7163, -539.4503, -39.4314>>     BREAK
				CASE 43  RETURN     <<-2774.5808, -502.4204, -19.1766>>   BREAK
				CASE 44  RETURN     <<-2931.3008, -497.4826, -10.0121>>    BREAK
				CASE 45  RETURN     <<-2900.0676, -496.1484, -12.6503>>     BREAK
				CASE 46  RETURN     <<-2895.1111, -529.889, -14.6029>>     BREAK
				CASE 47  RETURN   	<<-2843.1355, -379.7443, -9.5852>>     BREAK
				CASE 48  RETURN     <<-2780.6504, -563.4147, -19.1329>>     BREAK
				CASE 49  RETURN     <<-2828.6555, -519.7512, -14.6729>>     BREAK		
			ENDSWITCH            
		ENDIF	                 
			
		IF  serverBD.MapArea = TUGBOAT
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				serverBD.brandomcoordinatesgenerated = TRUE
				serverBD.vareablipcoordinates =  GET_AREA_BLIP_COORDINATES(TUGBOAT)			//TUGBOAT	
				serverBD.inumberofcheckpoints = NUM_CHECKPOINTS		
			ENDIF
			SWITCH checkpoint_number
				CASE 0   RETURN 	<<-3172.6934, 3034.6174, -28.2233>>		BREAK
				CASE 1   RETURN     <<-3173.2673, 3024.9021, -34.0257>>    BREAK
				CASE 2   RETURN     <<-3172.4429, 3040.7568, -38.3969>>   BREAK
				CASE 3   RETURN     <<-3177.3442, 3045.8103, -39.0048>>   BREAK
				CASE 4   RETURN     <<-3185.6328, 3057.3875, -39.3901>>     BREAK
				CASE 5   RETURN     <<-3186.3213, 3040.9063, -37.4284>>    BREAK
				CASE 6   RETURN     <<-3187.5962, 3025.6272, -38.1375>>    BREAK
				CASE 7   RETURN     <<-3195.2217, 3040.5286, -30.8778>>    BREAK
				CASE 8   RETURN     <<-3182.2112, 3011.1641, -39.9141>>     BREAK
				CASE 9   RETURN     <<-3224.8984, 3022.0352, -37.851>>     BREAK
				CASE 10  RETURN     <<-3200.9067, 2993.6294, -33.5806>>    BREAK
				CASE 11  RETURN     <<-3191.4988, 3065.6079, -38.6213>>    BREAK
				CASE 12  RETURN     <<-3206.0396, 3077.2566, -41.1165>>     BREAK
				CASE 13  RETURN     <<-3173.8933, 3067.6301, -34.2896>>     BREAK
				CASE 14  RETURN     <<-3145.676, 3063.0173, -25.598>>    BREAK
				CASE 15  RETURN     <<-3161.01, 3089.9026, -30.8359>>     BREAK
				CASE 16  RETURN     <<-3148.3643, 3015.7847, -31.2774>>     BREAK
				CASE 17  RETURN     <<-3144.4924, 2980.4634, -32.6345>>    BREAK
				CASE 18  RETURN     <<-3127.061, 3016.437, -22.3345>>     BREAK
				CASE 19  RETURN     <<-3124.4331, 3104.2632, -12.6705>>    BREAK
				CASE 20  RETURN     <<-3097.8447, 3084.7646, -12.0425>>    BREAK
				CASE 21  RETURN     <<-3144.9717, 3126.4272, -10.0939>>    BREAK
				CASE 22  RETURN     <<-3219.9224, 3049.3987, -44.7865>>   BREAK
				CASE 23  RETURN     <<-3242.707, 3036.2312, -52.2086>>     BREAK
				CASE 24  RETURN     <<-3246.9993, 2994.8862, -33.0739>>    BREAK
				CASE 25  RETURN     <<-3276.4011, 3035.7341, -11.4063>>    BREAK
				CASE 26  RETURN     <<-3169.6191, 2941.907, -36.9934>>     BREAK
				CASE 27  RETURN     <<-3220.6526, 2931.9487, -40.711>>   BREAK
				CASE 28  RETURN     <<-3269.1782, 2969.1443, -35.0162>>     BREAK
				CASE 29  RETURN     <<-3228.8743, 2968.1189, -36.6465>>    BREAK
				CASE 30  RETURN     <<-3203.1526, 3118.6755, -14.4662>>    BREAK
				CASE 31  RETURN     <<-3258.2788, 3066.6221, -24.7998>>    BREAK
				CASE 32  RETURN     <<-3272.6802, 3019.5703, -38.4661>>     BREAK
				CASE 33  RETURN     <<-3192.4814, 2956.1265, -17.2506>>     BREAK
				CASE 34  RETURN     <<-3150.4363, 2963.3936, -34.3075>>     BREAK
				CASE 35  RETURN     <<-3174.3506, 2978.5659, -26.1099>>     BREAK
				CASE 36  RETURN     <<-3097.6042, 2966.0244, -29.8447>>    BREAK
				CASE 37  RETURN 	<<-3057.2805, 3008.0942, -13.5868>>    BREAK
				CASE 38  RETURN     <<-3082.2813, 3036.3635, -10.8164>>     BREAK
				CASE 39  RETURN     <<-3135.1724, 3084.1721, -14.785>>     BREAK
				CASE 40  RETURN     <<-3221.4453, 2986.2803, -31.0734>>     BREAK
				CASE 41  RETURN     <<-3247.3982, 3017.5686, -50.7397>>     BREAK
				CASE 42  RETURN     <<-3245.6609, 3100.2815, -26.8515>>     BREAK
				CASE 43  RETURN     <<-3268.6873, 3007.6472, -41.2257>>    BREAK
				CASE 44  RETURN     <<-3257.3589, 2981.7202, -14.632>>    BREAK
				CASE 45  RETURN     <<-3240.511, 2946.8235, -21.1369>>     BREAK
				CASE 46  RETURN     <<-3277.3054, 3056.7249, -40.447>>    BREAK
				CASE 47  RETURN   	<<-3234.2822, 3072.6963, -32.0938>>     BREAK
				CASE 48  RETURN     <<-3191.0776, 3092.2983, -15.6324>>     BREAK
				CASE 49  RETURN     <<-3161.5208, 3117.5503, -17.6407>>     BREAK		
			ENDSWITCH            
		ENDIF	                 
			
			
			
			
			
			
		
		SCRIPT_ASSERT("GET_CHECKPOINT_LOCATION outside range icounter")
		RETURN	 <<-2823.9104, -596.8481, -72.6573>> 
ENDFUNC

//PURPOSE: Get Random Location for checkpoints
FUNC BOOL CREATE_SALVAGE_CHECKPOINT_BY_LOCATION()

		INT i

		
		REPEAT serverBD.inumberofcheckpoints i
			DRAW_CHECKPOINTS(localCheckpointData[i], GET_CHECKPOINT_LOCATION(i),i)
		ENDREPEAT	                   
			
		
		PRINTLN("[SALVAGE] CREATE_SALVAGE_CHECKPOINT()_BY_LOCATION done")
		RETURN	TRUE
ENDFUNC

INT localSyncID

PROC MAINTAIN_SORTED_PARTICIPANTS()

	// No need to update if we're in sync with the server
	IF localSyncID = serverBD.iServerSyncID
		EXIT
	ENDIF
	
	IF g_GBLeaderboardStruct.dpadVariables.eDpadVarType <> DPAD_VAR_NONE
		g_GBLeaderboardStruct.dpadVariables.eDpadVarType = DPAD_VAR_NONE
	ENDIF
	
	INT i
	REPEAT GB_GET_MAX_GANG_SIZE_INCLUDING_BOSS() i
		g_GBLeaderboardStruct.challengeLbdStruct[i].playerID = serverBD.sSortedPackagesCollected[i].playerID
		g_GBLeaderboardStruct.challengeLbdStruct[i].iScore = serverBD.sSortedPackagesCollected[i].iScore
		g_GBLeaderboardStruct.challengeLbdStruct[i].iFinishTime = -1
	ENDREPEAT
	
	// Update our sync ID to be the same as the servers
	localSyncID = serverBD.iServerSyncID
	
	#IF IS_DEBUG_BUILD
	PRINTLN("[PROTECTION RACKET LBD UPDATE] [STMGB] [CLIENT] - MAINTAIN_PROTECTION_RACKET_SORTED_PARTICIPANTS - Performing a leaderboard update. LocalSyncID updated to ", localSyncID)
	INT iDebugLoop
	REPEAT GB_GET_MAX_GANG_SIZE_INCLUDING_BOSS()	iDebugLoop
		IF g_GBLeaderboardStruct.challengeLbdStruct[iDebugLoop].playerID != INVALID_PLAYER_INDEX()
			//IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(g_GBLeaderboardStruct.challengeLbdStruct[iDebugLoop].iParticipant))
				PRINTLN("[PROTECTION RACKET LBD UPDATE] [STMGB] [CLIENT] - MAINTAIN_PROTECTION_RACKET_SORTED_PARTICIPANTS - |--------------------------- >| ")
				PRINTLN("[PROTECTION RACKET LBD UPDATE] [STMGB] [CLIENT] - MAINTAIN_PROTECTION_RACKET_SORTED_PARTICIPANTS - Position: ", iDebugLoop)
				PRINTLN("[PROTECTION RACKET LBD UPDATE] [STMGB] [CLIENT] - MAINTAIN_PROTECTION_RACKET_SORTED_PARTICIPANTS - Player: ", GET_PLAYER_NAME(g_GBLeaderboardStruct.challengeLbdStruct[iDebugLoop].playerID))
				PRINTLN("[PROTECTION RACKET LBD UPDATE] [STMGB] [CLIENT] - MAINTAIN_PROTECTION_RACKET_SORTED_PARTICIPANTS - Score: ", g_GBLeaderboardStruct.challengeLbdStruct[iDebugLoop].iScore)
				PRINTLN("[PROTECTION RACKET LBD UPDATE] [STMGB] [CLIENT] - MAINTAIN_PROTECTION_RACKET_SORTED_PARTICIPANTS - |--------------------------- |< ")
			//ENDIF
		ENDIF
	ENDREPEAT
	#ENDIF
ENDPROC

PROC CREATE_SALVAGE_CHECKPOINT()
	IF NOT bhas_the_package_been_created
	

			gfinderskeeperslocalplayerhascollectedpickup = FALSE
			RESET_NET_TIMER(fireworkstimer)
			RESET_NET_TIMER(helptimer)
			irumbleduration = 5000
			

			CREATE_SALVAGE_CHECKPOINT_BY_LOCATION()

	ENDIF
ENDPROC

FUNC VECTOR GETJETSKISPAWN_COORDS(int counterjetski)

	IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
		//counterjetski = 3
	//ELSE
		//counterjetski = GB_GET_INDEX_OF_GOON(GET_GANG_BOSS_PLAYER_INDEX(), player_ped_ID())
	ENDIF
	
	PRINTLN("GETJETSKISPAWN_COORDS  counterjetski = ", counterjetski)
	PRINTLN("serverBD.MapArea =  ", ENUM_TO_INT(serverBD.MapArea ))
	
		SWITCH  serverBD.MapArea 
		CASE CONTAINERSHIP
		CASE PADDLESTEAMER
		
			IF counterjetski = 0
				RETURN <<2859.3069, -644.3696, -2.5415>> 
			ELIF counterjetski = 1
				RETURN <<2855.4456, -618.0198, -2.3131>> 

				
			ELIF counterjetski = 2
				RETURN <<2849.8909, -604.3315, -1.6402>> 


			ELIF counterjetski = 3
				RETURN <<2845.8909, -599.3315, -1.6402>> 

			ELIF counterjetski = 4
				RETURN <<2840.8909, -601.3315, -1.6402>> 

			ELIF counterjetski = 5
				RETURN <<2845.8909, -599.3315, -1.6402>> 

				
			ENDIF
		BREAK
		CASE PLANENORTH
			IF counterjetski = 0
				RETURN <<-722.0089, 6143.8281, -0.0725>>

			ELIF counterjetski = 1
				RETURN <<-720.0872, 6145.9482, -0.1022>>

			ELIF counterjetski = 2
				RETURN <<-718.3804, 6147.8433, -0.0667>>

			ELIF counterjetski = 3
				RETURN <<-716.8694, 6149.5313, -0.0463>> 
			ELIF counterjetski = 4
				RETURN <<-724.8694, 6140.5313, -0.0463>> 
			ELIF counterjetski = 5
				RETURN <<-715.8694, 6147.5313, -0.0463>> 
			ENDIF
		
		BREAK
		
		CASE CARGOSHIP
		
			IF counterjetski = 0
				RETURN <<-94.5899, -2780.1401, -0.2103>>

			ELIF counterjetski = 1
				RETURN <<-110.3913, -2778.9321, -0.0374>>

			ELIF counterjetski = 2
				RETURN <<-107.0318, -2775.7676, -0.1162>>

			ELIF counterjetski = 3
				RETURN <<-104.8190, -2774.1895, -0.5056>>
			ELIF counterjetski = 4
				RETURN <<-97.8694, -2779.5313, -0.0463>> 
			ELIF counterjetski = 5
				RETURN <<-112.8694, -2780.5313, -0.0463>> 
			ENDIF
		
		BREAK
		
		CASE   SUBMARINE
				
		
			IF counterjetski = 0
				RETURN <<1521.6560, 6657.6465, -1.0190>>

			ELIF counterjetski = 1
				RETURN <<1527.1831, 6660.3472, -1.2808>>

			ELIF counterjetski = 2
				RETURN <<1533.4044, 6664.4292, -1.8564>>

			ELIF counterjetski = 3
				RETURN <<1540.9460, 6670.6665, -0.2959>>
			ELIF counterjetski = 4
				RETURN <<-1542.8694, 6671.5313, -0.0463>> 
			ELIF counterjetski = 5
				RETURN <<-1519.8694, 6655.5313, -0.0463>> 
			ENDIF
			
		BREAK
		
		CASE   MILITARYDEBRIS	
		
			IF counterjetski = 0
				RETURN   <<3870.3850, 4439.9731, -7.2256>>

			ELIF counterjetski = 1
				RETURN  <<3851.8359, 4483.7769, -3.0963>>

			ELIF counterjetski = 2
				RETURN  <<3862.3071, 4485.8015, -0.9069>>

			ELIF counterjetski = 3
				RETURN  <<3868.0801, 4475.3215, -0.7179>>
			ELIF counterjetski = 4
				RETURN <<3872.8694, 4438.5313, -0.0463>> 
			ELIF counterjetski = 5
				RETURN <<3869.8694, 4435.5313, -0.0463>> 
			ENDIF
		
		BREAK
		
		CASE  PLANESOUTH 
			IF counterjetski = 0
				RETURN  <<1018.1042, -2683.4199, -1.7048>>

			ELIF counterjetski = 1
				RETURN <<1020.3278, -2681.3982, -1.2738>>

			ELIF counterjetski = 2
				RETURN  <<1009.0112, -2686.8506, -4.6334>>

			ELIF counterjetski = 3
				RETURN   <<1011.9038, -2692.4639, -5.5581>>
			ELIF counterjetski = 4
				RETURN <<1015.8694, -2682.5313, -0.0463>> 
			ELIF counterjetski = 5
				RETURN <<1014.8694, -2680.5313, -0.0463>> 
			ENDIF
			
					
		BREAK
		
		CASE   MILITARYPLANE
			IF counterjetski = 0
				RETURN  <<-3148.3691, 3338.4189, -1.5630>>

			ELIF counterjetski = 1
				RETURN <<-3145.2490, 3342.1257, -1.6426>>

			ELIF counterjetski = 2
				RETURN  <<-3144.8489, 3344.4915, -1.7292>>

			ELIF counterjetski = 3
				RETURN  <<-3140.8479, 3348.7825, -1.5683>>
			ELIF counterjetski = 4
				RETURN  <<-3150.3691, 3337.4189, -1.5630>>
			ELIF counterjetski = 5
				RETURN  <<-3138.3691, 3350.4189, -1.5630>>

			ENDIF
	
		BREAK
		
		CASE   BATHYSCAPE
			IF counterjetski = 0
				RETURN <<-2566.6841, -269.2132, -1.7097>>
			ELIF counterjetski = 1
				RETURN <<-2563.9312, -270.9359, -1.7388>>

			ELIF counterjetski = 2
				RETURN  <<-2561.7402, -275.2886, -2.1442>>

			ELIF counterjetski = 3
				RETURN  <<-2558.8489, -278.5610, -2.3748>>
			ELIF counterjetski = 4
				RETURN  <<-2569.6841, -268.2132, -1.7097>>
			ELIF counterjetski = 5
				RETURN  <<-2555.6841, -280.2132, -1.7097>>
			ENDIF
			
					
		BREAK
		
		CASE   TUGBOAT
			IF counterjetski = 0
				RETURN  <<-2767.1545, 2923.9209, -1.1199>>

			ELIF counterjetski = 1
				RETURN <<-2766.0686, 2908.8931, -1.0505>>

			ELIF counterjetski = 2
				RETURN  <<-2770.9153, 2928.0857, -1.2780>>

			ELIF counterjetski = 3
				RETURN  <<-2768.4568, 2889.6602, -1.1289>>
			ELIF counterjetski = 4
				RETURN  <<-2765.1545, 2900.9209, -1.1199>>		
			ELIF counterjetski = 5
				RETURN  <<-2771.4568, 2905.6602, -1.1289>>
			ENDIF
	
		BREAK
	ENDSWITCH	
	SCRIPT_ASSERT("GETJETSKISPAWN_COORDS is broken. Tell Kevin Wong")
	RETURN <<-1012.9749, 6242.6372, -1.1634>>

ENDFUNC


FUNC VECTOR GETJETSKIRESPAWN_COORDS()
	INT counterjetski
	IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
		counterjetski = 5
	ELSE
		counterjetski = GB_GET_INDEX_OF_GOON(GET_GANG_BOSS_PLAYER_INDEX(), player_ID())
	ENDIF
	
	PRINTLN("GETJETSKIRESPAWN_COORDS  counterjetski = ", counterjetski)
	PRINTLN("serverBD.MapArea =  ", ENUM_TO_INT(serverBD.MapArea ))
	
	IF  serverBD.MapArea = CONTAINERSHIP
		IF counterjetski = 0   RETURN <<3212.0474, -85.1364, 0.4581>>
		ELIF counterjetski = 1 RETURN <<3070.1868, -106.6181, 0.2643>>
		ELIF counterjetski = 2 RETURN <<3377.301, -202.4127, 0.8126>>
		ELIF counterjetski = 3 RETURN <<3427.9248, -289.1371, 0.645>>
		ELIF counterjetski = 4 RETURN <<3422.6885, -378.5902, 0.4568>>
		ELIF counterjetski = 5 RETURN <<3375.189, -494.8062, 1.955>>

		ENDIF
	ELIF  serverBD.MapArea = PADDLESTEAMER
	
		IF counterjetski = 0   RETURN <<2708.2107, -1648.3726, 1.0228>>
		ELIF counterjetski = 1 RETURN <<2765.0498, -1664.6429, 1.0459>>
		ELIF counterjetski = 2 RETURN <<2892.4736, -1253.6687, 0.2681>>
		ELIF counterjetski = 3 RETURN <<2897.8845, -1202.1033, 0.0385>>
		ELIF counterjetski = 4 RETURN <<2839.2759, -1212.1224, 0.4156>>
		ELIF counterjetski = 5 RETURN <<2836.4502, -1160.7767, -0.1465>>
		ENDIF
	
	ELIF serverBD.MapArea = PLANENORTH
		IF counterjetski = 0   RETURN <<-933.9257, 6845.1162, 0.1161>>
		ELIF counterjetski = 1 RETURN <<-768.8779, 6788.8643, -0.3356>>
		ELIF counterjetski = 2 RETURN <<-675.7148, 6650.4595, 0.5063>>
		ELIF counterjetski = 3 RETURN <<-681.525, 6483.855, 0.6498>>
		ELIF counterjetski = 4 RETURN <<-767.5818, 6365.2832, 1.3525>>
		ELIF counterjetski = 5 RETURN <<-920.9078, 6312.791, 0.6602>>
		ENDIF
		

	ELIF serverBD.MapArea = CARGOSHIP
	
		IF counterjetski = 0   RETURN<<62.5283, -2871.5671, 0.7724>>
		ELIF counterjetski = 1 RETURN<<45.5803, -2978.4114, 0.3402>>
		ELIF counterjetski = 2 RETURN<<-23.1547, -3063.6672, 1.2579>>
		ELIF counterjetski = 3 RETURN<<-42.5379, -3150.6934, 0.7744>>
		ELIF counterjetski = 4 RETURN<<-133.2555, -3123.6541, -0.1452>>
		ELIF counterjetski = 5 RETURN<<-194.499, -3181.501, -0.7797>>
		ENDIF                  	
	
	ELIF  serverBD.MapArea = SUBMARINE
			
	
		IF counterjetski = 0   RETURN <<2408.9429, 6697.8032, 0.3778>>
		ELIF counterjetski = 1 RETURN <<2439.8328, 6792.1855, 0.3985>>
		ELIF counterjetski = 2 RETURN <<2506.5205, 6871.9541, -0.2933>>
		ELIF counterjetski = 3 RETURN <<2605.1448, 6906.0605, 0.591>>
		ELIF counterjetski = 4 RETURN <<2711.3855, 6911.0889, 0.9865>>
		ELIF counterjetski = 5 RETURN <<2808.1914, 6859.2183, 0.8395>>
		ENDIF                         

	ELIF  serverBD.MapArea = MILITARYDEBRIS	
		IF counterjetski = 0   RETURN <<4002.2913, 3768.3149, -0.549>>
		ELIF counterjetski = 1 RETURN <<4116.6587, 3805.0034, 1.9367>>
		ELIF counterjetski = 2 RETURN <<4249.7813, 3770.6279, 0.3236>>
		ELIF counterjetski = 3 RETURN <<4342.792, 3686.6599, 1.9332>>
		ELIF counterjetski = 4 RETURN <<4387.1035, 3569.5911, 1.2003>>
		ELIF counterjetski = 5 RETURN <<4377.9658, 3444.1392, 1.5163>>
		ENDIF                        
	
	ELIF  serverBD.MapArea = PLANESOUTH
		IF counterjetski = 0   RETURN <<1915.5179, -2722.9236, -0.2301>>
		ELIF counterjetski = 1 RETURN <<2010.0168, -2824.0085, -0.2663>>
		ELIF counterjetski = 2 RETURN <<2059.2192, -2931.1787, 0.0218>>
		ELIF counterjetski = 3 RETURN <<2050.2517, -3056.3027, 0.9162>>
		ELIF counterjetski = 4 RETURN <<1975.9358, -3166.9763, -0.2203>>
		ELIF counterjetski = 5 RETURN <<1874.6621, -3225.3862, 0.776>>
		ENDIF                   
		
	ELIF  serverBD.MapArea = MILITARYPLANE
		IF counterjetski = 0   RETURN <<-2928.9956, 3668.9824, 0.31>>

		ELIF counterjetski = 1 RETURN <<-2968.4126, 3826.9253, -0.3712>>

		ELIF counterjetski = 2 RETURN <<-3080.5779, 3933.0251, -0.4947>>

		ELIF counterjetski = 3 RETURN <<-3250.6787, 3976.4409, -0.091>>

		ELIF counterjetski = 4 RETURN  <<-3422.4055, 3924.8713, -0.6138>>

		ELIF counterjetski = 5 RETURN <<-3537.3127, 3805.9626, 0.4037>>

		ENDIF                   
		
	ELIF  serverBD.MapArea = BATHYSCAPE
		IF counterjetski = 0   RETURN <<-2684.2954, -257.0117, 0.1348>>

		ELIF counterjetski = 1 RETURN  <<-2564.0173, -397.6332, 0.6662>>

		ELIF counterjetski = 2 RETURN <<-2536.9185, -583.0862, -0.2603>>

		ELIF counterjetski = 3 RETURN <<-2617.3855, -729.0892, 0.833>>

		ELIF counterjetski = 4 RETURN <<-2782.6843, -822.0383, 0.2424>>

		ELIF counterjetski = 5 RETURN <<-2979.0959, -787.7824, 0.2871>>

		ENDIF                   
		
	ELIF  serverBD.MapArea = TUGBOAT
		IF counterjetski = 0   RETURN <<-2945.949, 2839.3555, -0.2352>>

		ELIF counterjetski = 1 RETURN <<-3036.1179, 2777.1877, 0.0065>>

		ELIF counterjetski = 2 RETURN <<-3233.4851, 2748.6677, 0.228>>

		ELIF counterjetski = 3 RETURN <<-3339.8081, 2790.8003, 0.1298>>

		ELIF counterjetski = 4 RETURN <<-3418.4336, 2862.3452, 1.1379>>

		ELIF counterjetski = 5 RETURN <<-3468.6912, 2955.863, 0.4168>>

		ENDIF                   
		
		
	ENDIF
	
	SCRIPT_ASSERT("GETJETSKIRESPAWN_COORDS is broken. Tell Kevin Wong")
	RETURN <<-1012.9749, 6242.6372, -1.1634>>

ENDFUNC



/*
FUNC BOOL CREATE_JETSKIS()
	
	IF serverBD.bcreate_bjetskis
		RETURN FALSE
	ENDIF
	
	
	REQUEST_MODEL(SEASHARK)
	IF HAS_MODEL_LOADED(SEASHARK)
		

		//IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), serverBD.vareablipcoordinates, FALSE) < 600.0                                   
		
			serverBD.bcreate_bjetskis = TRUE
			CREATE_NET_VEHICLE(salvagelocalplayersjetski, SEASHARK, GETJETSKISPAWN_COORDS(),GET_HEADING_BETWEEN_TWO_VECTORS_2D(GETJETSKISPAWN_COORDS(),serverBD.vareablipcoordinates ), NETWORK_IS_HOST_OF_THIS_SCRIPT(), true )
			SET_BOAT_ANCHOR(NET_TO_VEH(salvagelocalplayersjetski),TRUE)
			
			blipjetski = ADD_BLIP_FOR_ENTITY (NET_TO_VEH(salvagelocalplayersjetski)) //( vcreation_coords)
			SET_BLIP_SCALE(blipjetski,1.0  )
			SET_BLIP_SPRITE(blipjetski, RADAR_TRACE_SEASHARK) // RADAR_TRACE_SEASHARK
			RETURN TRUE
		//ENDIF
		
		
	ELSE
		REQUEST_MODEL(SEASHARK)
	
	ENDIF
	RETURN FALSE
ENDFUNC
*/
FUNC BOOL CREATE_JETSKIS_SERVER()
	
	IF serverBD.bcreate_bjetskis
		RETURN FALSE
	ENDIF
	
	INT i
	INT numberofjetskistospawn = GB_GET_NUM_GOONS_IN_LOCAL_GANG()+1
	
	PRINTLN("CREATE_JETSKIS_SERVER serverBD.vareablipcoordinates = ", serverBD.vareablipcoordinates)
	REQUEST_MODEL(SEASHARK)
	CLEAR_AREA(GETJETSKISPAWN_COORDS(i), 30.0, TRUE)
	IF HAS_MODEL_LOADED(SEASHARK)
		REPEAT numberofjetskistospawn i
			//CLEAR_AREA(GETJETSKISPAWN_COORDS(i), 7.0, FALSE)
			CREATE_NET_VEHICLE(serverBD.salvagelocalplayersjetski[i], SEASHARK, GETJETSKISPAWN_COORDS(i),GET_HEADING_BETWEEN_TWO_VECTORS_2D(GETJETSKISPAWN_COORDS(i), GET_AREA_BLIP_COORDINATES(serverBD.MapArea) ), NETWORK_IS_HOST_OF_THIS_SCRIPT(), true )
			IF CAN_ANCHOR_BOAT_HERE(NET_TO_VEH(serverBD.salvagelocalplayersjetski[i]))
				SET_BOAT_ANCHOR(NET_TO_VEH(serverBD.salvagelocalplayersjetski[i]),TRUE)
			ENDIF
			PRINTLN("CREATE_JETSKIS_SERVER created serverBD.salvagelocalplayersjetski", i)
		ENDREPEAT
		serverBD.bcreate_bjetskis = TRUE
		RETURN TRUE
	ELSE
		REQUEST_MODEL(SEASHARK)
		serverBD.bcreate_bjetskis = FALSE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC CREATE_BLIPS_FOR_JETSKIS()

		//blips will need to be created locally?
	INT i
	REPEAT MAXNUMJETSKIS i
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.salvagelocalplayersjetski[i])

			IF NOT DOES_BLIP_EXIST(blipjetski[i])
				blipjetski[i] = ADD_BLIP_FOR_COORD (GETJETSKISPAWN_COORDS(i)) //( vcreation_coords)
				SET_BLIP_SCALE(blipjetski[i] ,1.0  )
				SET_BLIP_SPRITE(blipjetski[i], RADAR_TRACE_SEASHARK) // RADAR_TRACE_SEASHARK
				PRINTLN("[SALVAGE] CREATE_BLIPS_FOR_JETSKIS FOR, ", i)
				IF i = 0
					SET_BLIP_ROUTE(blipjetski[i], TRUE) 
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
ENDPROC

PROC DELETE_ALL_JETSKI_BLIPS()
	INT i
	REPEAT MAXNUMJETSKIS i
	IF DOES_BLIP_EXIST(blipjetski[i])
		REMOVE_BLIP(blipjetski[i])
		PRINTLN("[SALVAGE] DELETE_ALL_JETSKI_BLIPS, ", i)
	ENDIF
	ENDREPEAT

ENDPROC


PROC DELETE_JETSKI_BLIP(INT i)
	IF DOES_BLIP_EXIST(blipjetski[i])
		REMOVE_BLIP(blipjetski[i])
		PRINTLN("[SALVAGE] DELETE_BLIP done")
		bhavejetskisbeendeleted = TRUE
	ENDIF
	

ENDPROC

PROC DELETE_PACKAGE()

/*	IF DOES_PARTICLE_FX_LOOPED_EXIST(flare_ptfx)
		STOP_PARTICLE_FX_LOOPED(flare_ptfx)
	endif 
	
	IF  DOES_PARTICLE_FX_LOOPED_EXIST(flare_Smoke_ptfx)
		STOP_PARTICLE_FX_LOOPED(flare_Smoke_ptfx)
	ENDIF
	*/
	IF NOT HAS_SOUND_FINISHED(iSoundfrompackage)
		STOP_SOUND(iSoundfrompackage)
	ENDIF
	
	//REMOVE_TRACKIFY_MULTIPLE_TARGET(iTargetLoop)
	IF serverBD.eServer_ending_stat = TIME_EXPIRES
	//	ADD_EXPLOSION(GET_CHECKPOINT_LOCATION(serverBD.ipackagecounter), EXP_TAG_ROCKET, 1.0, TRUE, FALSE, 0.0)
	
	ENDIF
//	IF NOT g_sMPTunables.bgb_finders_keepers_disable_trackify
//		REMOVE_TRACKIFY_MULTIPLE_TARGET(1)
//		ENABLE_MULTIPLAYER_TRACKIFY_APP(FALSE)
//	ENDIF
	
	//DELETE_CHECKPOINT(localCheckpointData[serverBD.ipackagecounter].checkpointObj)
	//RELEASE_SOUND_ID(iSoundfrompackage)
ENDPROC

FUNC BOOL IS_THIS_PLAYER_A_GANG_GOON(PLAYER_INDEX thisPlayer)
	RETURN GB_IS_PLAYER_MEMBER_OF_THIS_GANG(thisPlayer, GB_GET_LOCAL_PLAYER_GANG_BOSS(), FALSE)
ENDFUNC




PROC ADD_FINDERS_KEEPERS_BLIP()

	IF NOT DOES_BLIP_EXIST(PackageBlipAreaLongRange)
		PackageBlipAreaLongRange = ADD_BLIP_FOR_COORD(serverBD.vareablipcoordinates)
		
		SET_BLIP_AS_SHORT_RANGE(PackageBlipAreaLongRange, FALSE)
		//SET_BLIP_DISPLAY(PackageBlipAreaLongRange, DISPLAY_RADAR_ONLY) 
		//GET_OFFSET_FROM_COORD_IN_WORLD_COORDS(serverBD.vPackageLocation, 0.0,<<GET_RANDOM_FLOAT_IN_RANGE(-50,50), GET_RANDOM_FLOAT_IN_RANGE(-50,50), 0.0>>)
		IF bcreatejetskiblips = FALSE
			SET_BLIP_ROUTE(PackageBlipAreaLongRange, TRUE) 
		ENDIF
		
		SET_BLIP_SPRITE(PackageBlipAreaLongRange, RADAR_TRACE_FINDERS_KEEPERS) 
		SET_BLIP_NAME_FROM_TEXT_FILE(PackageBlipAreaLongRange, "GB_SAL_GO1b")
		SET_BLIP_FLASHES(PackageBlipAreaLongRange, TRUE)
		SET_BLIP_FLASH_TIMER(PackageBlipAreaLongRange, 10000)

		SET_BLIP_COLOUR_FROM_HUD_COLOUR(PackageBlipAreaLongRange, HUD_COLOUR_YELLOW)
		SET_BLIP_PRIORITY(PackageBlipAreaLongRange, BLIP_PRIORITY_HIGHES_SPECIAL_MED)
		SET_BLIP_SCALE(PackageBlipAreaLongRange,g_sMPTunables.fgangboss_Job_blip_scale  )
		MPGlobalsAmbience.vQuickGPS = GETJETSKISPAWN_COORDS(5)
		
		MPGlobalsAmbience.bIsGPSDropOff = TRUE
		//SET_BLIP_NAME_FROM_TEXT_FILE(PackageBlipAreaLongRange, "CPC_BLIP")
		IF DOES_BLIP_EXIST(PackageBlipArea)
		
			SET_BLIP_COORDS(PackageBlipArea,serverBD.vareablipcoordinates )
		ENDIF
		#IF IS_DEBUG_BUILD
		VECTOR tempvector = GET_CHECKPOINT_LOCATION(serverBD.ipackagecounter)
		PRINTLN("     ---------->     [salvage]  - ADD_FINDERS_KEEPERS_BLIP - serverBD.vPackageLocation = ", tempvector)
		PRINTLN("     ---------->     [salvage]  - ADD_FINDERS_KEEPERS_BLIP - serverBD.vareablipcoordinates ", serverBD.vareablipcoordinates)
		#ENDIF
	ENDIF

ENDPROC

/// PURPOSE: Adds a blip at the packages's location
PROC ADD_PACKAGE_AREA_BLIP()
	
//	playerBD[PARTICIPANT_ID_TO_INT()].this_player_has_picked_up_package  = FALSE
	//Delete_MP_Objective_Text()
	//bRUMBLE_HELP1 = 0
	
	bhas_the_package_been_created = false 	
	
	IF NOT DOES_BLIP_EXIST(PackageBlipArea)
		bcreatepackageareablip[serverBD.ipackagecounter] = TRUE
		PackageBlipArea = ADD_BLIP_FOR_RADIUS(serverBD.vareablipcoordinates, g_sMPTunables.fexec_salvage_area_radius +20.0     )
		SET_BLIP_COLOUR_FROM_HUD_COLOUR(PackageBlipArea, HUD_COLOUR_YELLOW)
		//GET_OFFSET_FROM_COORD_IN_WORLD_COORDS(serverBD.vPackageLocation, 0.0,<<GET_RANDOM_FLOAT_IN_RANGE(-50,50), GET_RANDOM_FLOAT_IN_RANGE(-50,50), 0.0>>)
		SET_BLIP_ALPHA(PackageBlipArea, g_sMPTunables.iblip_area_alpha )
		#IF IS_DEBUG_BUILD
		VECTOR tempvector = GET_CHECKPOINT_LOCATION(serverBD.ipackagecounter)
		PRINTLN("     ---------->     [salvage]  - ADD_PACKAGE_AREA_BLIP - serverBD.vPackageLocation = ", tempvector)
		PRINTLN("     ---------->     [salvage]  - ADD_PACKAGE_AREA_BLIP - serverBD.vareablipcoordinates = ", serverBD.vareablipcoordinates)
		#ENDIF
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			//serverBD.has_a_checkpoint_been_passed = FALSE
		ENDIF
	ENDIF
	
	ADD_FINDERS_KEEPERS_BLIP()
	
	IF serverBD.ipackagecounter != 0
		IF serverBD.ipackagecounter != NUM_CHECKPOINTS
			//PRINT_TICKER("GB_FDKP_CREPAC")
		ELSE
			//PRINT_TICKER("GB_FDKP_CREPAC2")
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Removes the package area blip
PROC REMOVE_PACKAGE_AREA_BLIP()
	IF DOES_BLIP_EXIST(PackageBlipArea)
		REMOVE_BLIP(PackageBlipArea)
		//VECTOR tempvector = GET_CHECKPOINT_LOCATION(serverBD.ipackagecounter)
		//PRINTLN("     ---------->     [salvage]  - REMOVE_PACKAGE_AREA_BLIP - serverBD.vPackageLocation = ", tempvector)
		
	ENDIF
ENDPROC

PROC REMOVE_PACKAGE_AREA_BLIP_LONG_RANG_BLIP()
	IF DOES_BLIP_EXIST(PackageBlipAreaLongRange)
	    MPGlobalsAmbience.vQuickGPS =  <<0,0,0>> 
		MPGlobalsAmbience.bIsGPSDropOff = FALSE
		REMOVE_BLIP(PackageBlipAreaLongRange)
		//VECTOR tempvector = GET_CHECKPOINT_LOCATION(serverBD.ipackagecounter)
		//PRINTLN("     ---------->     [salvage]  - REMOVE_PACKAGE_AREA_BLIP_LONG_RANG_BLIP - serverBD.vPackageLocation = ", tempvector)
	ENDIF
ENDPROC


PROC CLEANUP_FLARE()

	IF DOES_ENTITY_EXIST( objFlare ) 
		DETACH_ENTITY( objFlare )
		DELETE_OBJECT( objFlare )
		SET_OBJECT_AS_NO_LONGER_NEEDED( objFlare )
		CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - CLEANUP_FLARE")
	ENDIF
	
	IF DOES_PARTICLE_FX_LOOPED_EXIST( players_underwater_flare )
		STOP_PARTICLE_FX_LOOPED( players_underwater_flare )
		REMOVE_NAMED_PTFX_ASSET( strHeistFX )
		
		CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - CLEANUP_FLARE FX")
	ENDIF
	
//	has_flare_fx_been_created = FALSE
	IF NOT HAS_SOUND_FINISHED(iSoundfromflare)
		STOP_SOUND(iSoundfromflare)
	ENDIF
ENDPROC

PROC REMOVE_REBREATHER_BLIP()
	int I
	REPEAT MAX_NUM_REBREATHERS i
		IF DOES_BLIP_EXIST(bliprebreather[i])
			REMOVE_BLIP( bliprebreather[i] )
		ENDIF
	ENDREPEAT
ENDPROC

PROC REMOVE_REBREATHER()
	//CLEAR_PED_STORED_HAT_PROP(PLAYER_PED_ID())
	PRINTLN("[SALVAGE] REMOVE_REBREATHER")
	

	IF NOT bplayerworemaskatstart
	or inum_of_rebreathers_at_start < 1
		IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
			REMOVE_CURRENT_HEAD_HEIST_GEAR(PLAYER_PED_ID())
			REMOVE_MP_HEIST_GEAR(PLAYER_PED_ID(), HEIST_GEAR_NONE)
			//SET_MP_HEIST_GEAR(PLAYER_PED_ID(), HEIST_GEAR_NONE)
					
			SET_PED_DIES_IN_WATER(PLAYER_PED_ID(), TRUE)
			SET_PED_DIES_IN_SINKING_VEHICLE(PLAYER_PED_ID(), TRUE) // url:bugstar:2345317
			SET_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_DO_GEAR_BERD_CHECK)
			PRINTLN("[SALVAGE] REMOVE_REBREATHER 1")
		ENDIF
	ENDIF
	int I
	REPEAT MAX_NUM_REBREATHERS i
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT() 
			
			IF DOES_ENTITY_EXIST(serverBD.objrebreather[i])
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.network_index_rebreather[i])
					IF  NETWORK_DOES_NETWORK_ID_EXIST(serverBD.network_index_rebreather[i])
						IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.network_index_rebreather[i])
							DELETE_OBJECT(serverBD.objrebreather[i])
							DELETE_NET_ID (serverBD.network_index_rebreather[i])
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	
	
	SET_MP_INT_CHARACTER_STAT(MP_STAT_BREATHING_APPAR_BOUGHT, inum_of_rebreathers_at_start)
	SET_PLAYER_UNDERWATER_BREATH_PERCENT_REMAINING(PLAYER_ID(), 100.0)
	SET_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_DO_GEAR_BERD_CHECK)
ENDPROC



PROC REMOVE_CHECKPOINTS()
	INT i
	REPEAT serverBD.inumberofcheckpoints i
		
			DELETE_CHECKPOINT(localCheckpointData[i].checkpointObj)
			
			IF DOES_BLIP_EXIST(localCheckpointData[i].cpBlip)
				REMOVE_BLIP( localCheckpointData[i].cpBlip )
			ENDIF
			
	ENDREPEAT
ENDPROC

PROC CLEANUP_BLIPS_AND_ASSETS()
		DELETE_PACKAGE()
		DELETE_ALL_JETSKI_BLIPS()
		REMOVE_PACKAGE_AREA_BLIP()
		REMOVE_PACKAGE_AREA_BLIP_LONG_RANG_BLIP()
		REMOVE_CHECKPOINTS()
		CLEANUP_FLARE()
		IF AUDIO_IS_SCRIPTED_MUSIC_PLAYING()
		TRIGGER_MUSIC_EVENT("SALVAGE_STOP_MUSIC")
		ENDIF
		SET_AUDIO_FLAG("DisableFlightMusic", FALSE) 
		SET_AUDIO_FLAG("WantedMusicDisabled", FALSE) 
		
		IF cleanupcountdownmusic = FALSE
		IF (GET_CHALLENGE_EXPIRE_TIME()-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.Starttimer) < 31000 AND GET_CHALLENGE_EXPIRE_TIME()-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.Starttimer) > 6000)
		//	PLAY_SOUND_FRONTEND(-1, "5s", "MP_MISSION_COUNTDOWN_SOUNDSET", FALSE)
			cleanupcountdownmusic = TRUE
			CANCEL_MUSIC_EVENT("APT_COUNTDOWN_30S")
			TRIGGER_MUSIC_EVENT("APT_COUNTDOWN_30S_KILL")
			PRINTLN("CLEANUP_BLIPS_AND_ASSETS player 5s countdown")
		ENDIF
		ENDIF
		//REMOVE_REBREATHER()
		REMOVE_REBREATHER_BLIP()
		Delete_MP_Objective_Text()
ENDPROC

PROC REMOVE_JETSKIS_SERVER()
	INT i
	REPEAT MAXNUMJETSKIS i
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.salvagelocalplayersjetski[i])
			CLEANUP_NET_ID(serverBD.salvagelocalplayersjetski[i])
		ENDIF
	ENDREPEAT
ENDPROC



FUNC INT GET_AREA_BLIP_ALPHA(PLAYER_INDEX piGangBoss)

	IF piGangBoss != INVALID_PLAYER_INDEX()
		IF NOT IS_VECTOR_ZERO(serverBD.vareablipcoordinates)
			FLOAT fDistance = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(GET_PLAYER_PED(piGangBoss), serverBD.vareablipcoordinates, FALSE)
			IF (fDistance < ((ciHTB_TRIGGER_RADIUS - 2) + GB_GET_BOSS_PROXIMITY_BONUS_RADIUS()))
				RETURN ciHTB_BLIP_ALPHA_INSIDE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN g_sMPTunables.iblip_area_alpha

ENDFUNC


/// PURPOSE:
///    Maintain the pad rumble intensity based on distance.
///    Further away = lower intensity.
PROC MAINTAIN_RUMBLE()

	//IF NOT GB_IS_PLAYER_MEMBER_OF_THIS_GANG(PLAYER_ID(), GET_GANG_BOSS_PLAYER_INDEX(), FALSE)
	//	EXIT
	//ENDIF
	
	VECTOR vPlayerPos = GET_ENTITY_COORDS(GET_PLAYER_PED(PLAYER_ID()), FALSE)
	FLOAT fDistance = VDIST(GET_CHECKPOINT_LOCATION(serverBD.ipackagecounter), vPlayerPos)
	//FLOAT fDelta, fIntensity
	
	
	
		IF (ROUND(fDistance) < g_sMPTunables.igb_finders_keepers_rumble_radius)
			
			IF bRUMBLE_HELP1 = 2
				IF GET_PROFILE_SETTING (PROFILE_CONTROLLER_VIBRATION) != 0   //Vibration set to off in frontend 
					IF NOT IS_HELP_MESSAGE_ON_SCREEN()
						 bRUMBLE_HELP1 = 3
						// PRINT_HELP("GB_FDKP_RUMH1")
					ENDIF
				ELSE
					bRUMBLE_HELP1 = 3
				ENDIF
			ENDIF
			
			 
			IF fDistance < cfHTB_MIN_RUMBLE_DISTANCE
				//fIntensity = ciHTB_MAX_SHAKE_VALUE
				irumbleduration = ciHTB_RUMBLE_DURATION 
			ELSE
				//fDelta = 1.0 - (CLAMP(fDistance, cfHTB_MIN_RUMBLE_DISTANCE, TO_FLOAT(g_sMPTunables.igb_finders_keepers_rumble_radius)) / TO_FLOAT(g_sMPTunables.igb_finders_keepers_rumble_radius))
				//fIntensity = LERP_FLOAT(ciHTB_MIN_SHAKE_VALUE, ciHTB_MAX_SHAKE_VALUE, fDelta)
				
				irumbleduration = ROUND(fDistance)*ciHTB_RUMBLE_DURATION_MULTIPLIER
				//PRINTLN("[salvage]  -  - MAINTAIN_RUMBLE - PlayerDistance: ", fDistance, ", irumbleduration ", irumbleduration)
			ENDIF
			
			
			IF HAS_NET_TIMER_EXPIRED(rumbletimer,irumbleduration)	
				RESET_NET_TIMER(rumbletimer)
				//SET_CONTROL_SHAKE(PLAYER_CONTROL, ciHTB_RUMBLE_DURATION, ROUND(fIntensity))
			ENDIF
			
			#IF IS_DEBUG_BUILD
			IF (GET_FRAME_COUNT() % 60) = 0
				//PRINTLN("[salvage]  -  - MAINTAIN_RUMBLE - PlayerDistance: ", fDistance, ", fIntensity: ", ROUND(fIntensity))
			ENDIF
			#ENDIF
		ENDIF
	
	
ENDPROC

FUNC BOOL CAN_ATTACH_FLARE_TO_PLAYER()

	PED_INDEX LocalPlayerPed = PLAYER_PED_ID()
	IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(), serverBD.vareablipcoordinates, FALSE) < (g_sMPTunables.fexec_salvage_area_radius   + 300.0  )                                
		IF NOT IS_ENTITY_DEAD(LocalPlayerPed)
			IF IS_ENTITY_IN_WATER( LocalPlayerPed ) 
			OR IS_PED_SWIMMING( LocalPlayerPed ) 
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE

ENDFUNC

FUNC BOOL IS_PLAYER_IN_BOAT_OR_IN_WATER()
	PED_INDEX LocalPlayerPed = PLAYER_PED_ID()
	IF NOT IS_ENTITY_DEAD(LocalPlayerPed)
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	        VEHICLE_INDEX temp_vehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
	        IF IS_VEHICLE_MODEL(temp_vehicle, SEASHARK)
				RETURN TRUE
			ENDIF
		ENDIF

		IF IS_PED_IN_ANY_BOAT(PLAYER_PED_ID())
			RETURN TRUE
		ENDIF
		
		IF CAN_ATTACH_FLARE_TO_PLAYER()
			RETURN TRUE
		ENDIF
		
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    rumbles harder when player gets close to package
PROC MAINTAIN_PACKAGE_CLIENT_BLIPS_AND_RUMBLE()


	IF bRUMBLE_HELP1 = 0
	AND HAS_NET_TIMER_EXPIRED(helptimer,10000)
		IF NOT  GB_SHOULD_RUN_SPEC_CAM()
			IF NOT IS_HELP_MESSAGE_ON_SCREEN()
			AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
			AND  IS_PLAYER_IN_BOAT_OR_IN_WATER()
			AND CAN_ATTACH_FLARE_TO_PLAYER()
				RESET_NET_TIMER(helptimer)
				 bRUMBLE_HELP1 = 1
				 PRINT_HELP("GB_SAL_HELP1b")
			ENDIF
		
		
			IF NOT Is_This_The_Current_Objective_Text("GB_SAL_HELP1")
				Delete_MP_Objective_Text()
				Print_Objective_Text("GB_SAL_HELP1")
			ENDIF
		ENDIF
	ENDIF
	
	

	IF HAS_SOUND_FINISHED(iSoundfrompackage)
		IF HAS_NET_TIMER_EXPIRED(serverBD.soundpackagetonetimer,irumbleduration)
			FLOAT fDistance = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), GET_CHECKPOINT_LOCATION(serverBD.ipackagecounter), FALSE)
			IF g_sMPTunables.igb_finders_keepers_sfx_radius > ROUND(fDistance    )                                 
				RESET_NET_TIMER(serverBD.soundpackagetonetimer)
				//PLAY_SOUND_FROM_COORD(iSoundfrompackage, "Case_Beep", GET_CHECKPOINT_LOCATION(serverBD.ipackagecounter), "GTAO_Magnate_Finders_Keepers_Soundset", FALSE, 0, TRUE) 
			ENDIF
		ENDIF
	ENDIF

	IF bdisplay_blip_near_end_of_expire_time = FALSE
		//IF HAS_NET_TIMER_EXPIRED(serverBD.Starttimer, timertoshowpackageblip)
		IF bAddBlipforpackage = TRUE
			//REMOVE_PACKAGE_AREA_BLIP()
			//REMOVE_PACKAGE_AREA_BLIP_LONG_RANG_BLIP()
			bdisplay_blip_near_end_of_expire_time = TRUE
		//	PRINT_HELP_NO_SOUND("GB_FDKP_HELP2")
			//GB_SET_GANG_BOSS_HELP_BACKGROUND()
			PRINTLN("     ---------->     [salvage]  - MAINTAIN_PACKAGE_CLIENT_BLIPS_AND_RUMBLE Net timer expired")
		ENDIF
	ENDIF

ENDPROC

PROC GIVE_INITIAL_PLAYER_REBREATHER()
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
	
		IF NOT initialrebreathercheck
			IF IS_PED_WEARING_HELMET(PLAYER_PED_ID())
			OR IS_CURRENT_HEAD_PROP_A_HELMET(PLAYER_PED_ID())
				
				CLEAR_PED_PROP(PLAYER_PED_ID(), ANCHOR_HEAD)
			//	SET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_PROPS,OUTFIT_DEFAULT, FALSE)
				PRINTLN("MAGNATE_GANG_BOSS [SALVAGE] GIVE_PLAYER_REBREATHER REMOVE HELMET ")
				REMOVE_PED_HELMET(PLAYER_PED_ID(), TRUE)
			ENDIF		
			
			IF IS_MP_HEIST_GEAR_EQUIPPED(PLAYER_PED_ID(), HEIST_GEAR_REBREATHER)
				bplayerworemaskatstart= TRUE
				PRINTLN("MAGNATE_GANG_BOSS [SALVAGE] GIVE_INITIAL_PLAYER_REBREATHER player HAS WORN A REBREATHER AT START")
			ENDIF
			initialrebreathercheck = TRUE
		ENDIF	
		
		INT iRebreatherCount = GET_MP_INT_CHARACTER_STAT(MP_STAT_BREATHING_APPAR_BOUGHT)
		PRINTLN("MAGNATE_GANG_BOSS [SALVAGE] GIVE_INITIAL_PLAYER_REBREATHER - REBREATHER count = ",iRebreatherCount) 
		IF iRebreatherCount < g_sMPTunables.iexec_salvage_initial_rebreathers_given         
		PRINTLN("MAGNATE_GANG_BOSS [SALVAGE] GIVE_INITIAL_PLAYER_REBREATHER - CURRENT REBREATHERS = ", GET_MP_INT_CHARACTER_STAT(MP_STAT_BREATHING_APPAR_BOUGHT), "   - ADDING ", (4-iRebreatherCount), " MORE FOR JOB TO MAKE TOTAL 4") 
		INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_BREATHING_APPAR_BOUGHT, g_sMPTunables.iexec_salvage_initial_rebreathers_given-iRebreatherCount)
		ENDIF
		CLEAR_PED_STORED_HAT_PROP(PLAYER_PED_ID())
		SET_MP_HEIST_GEAR(PLAYER_PED_ID(), HEIST_GEAR_REBREATHER)
		SET_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_DO_GEAR_BERD_CHECK)
		g_give_player_a_rebreather = FALSE
		playerBD[PARTICIPANT_ID_TO_INT()].this_player_in_salvage_area = TRUE
		SET_PLAYER_UNDERWATER_BREATH_PERCENT_REMAINING(PLAYER_ID(), 100.0)
		bGIVE_PLAYER_REBREATHER = TRUE
			
		SET_PED_DIES_IN_WATER(PLAYER_PED_ID(), FALSE)
		SET_PED_DIES_IN_SINKING_VEHICLE(PLAYER_PED_ID(), FALSE) // url:bugstar:2345317
	ENDIF
ENDPROC


PROC SEND_REBREATHER_COLLECTION_TICKER_EVENT(INT breathernumber)


		SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerData
		TickerData.TickerEvent = TICKER_EVENT_SALVAGE_COLLECTED_REBREATHER
		TickerData.playerID = PLAYER_ID()
		Tickerdata.dataInt = breathernumber
		BROADCAST_TICKER_EVENT(TickerData,ALL_PLAYERS_ON_SCRIPT())
		
ENDPROC


PROC SEND_REBREATHER_TICKER_EVENT()

	IF NOT bEquippedRebreather
		SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerData
		TickerData.TickerEvent = TICKER_EVENT_SALVAGE_EQUIPPED_REBREATHER
		TickerData.playerID = PLAYER_ID()
		BROADCAST_TICKER_EVENT(TickerData,ALL_PLAYERS_ON_SCRIPT())
		bEquippedRebreather = TRUE
		
	ENDIF

ENDPROC

// ---- Event Handling -----



/// PURPOSE:
///    Sort the players when a new score has been detected
PROC SORT_SALVAGE_PLAYER_SCORES(PLAYER_INDEX playerID, INT iPlayerScore)
	SORT_AMBIENT_ACTIVITY_LEADERBOARD(serverBD.sSortedPackagesCollected, playerID, iPlayerScore, AAL_SORT_DESCENDING)

	serverBD.iServerSyncId++
ENDPROC
PROC PROCESS_PLAYER_JOIN(INT iCount)
	
	STRUCT_PLAYER_SCRIPT_EVENTS data 
	
	GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, data, SIZE_OF(data))

	// Check player is valid
	IF data.PlayerIndex != INVALID_PLAYER_INDEX()

		// If we haven't yet had knowledge of this player then loop over threads ensuring they have joined this script
		IF NOT IS_BIT_SET(serverBD.iPlayerBitSet, NATIVE_TO_INT(data.PlayerIndex))
			INT iThread
			REPEAT data.NumThreads iThread

				IF data.Threads[iThread] = GET_ID_OF_THIS_THREAD()
					
					SORT_SALVAGE_PLAYER_SCORES(data.PlayerIndex,0)
					
					SET_BIT(serverBD.iPlayerBitSet, NATIVE_TO_INT(data.PlayerIndex))
					PRINTLN("[SALVAGE] PROCESS_PLAYER_JOINED")
				ENDIF       
			ENDREPEAT
		ENDIF
	ENDIF

ENDPROC

PROC PROCESS_EVENTS()
	INT iCount
	EVENT_NAMES ThisScriptEvent
	
	REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iCount
	
		ThisScriptEvent = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iCount)
		

		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			IF ThisScriptEvent = EVENT_NETWORK_PLAYER_JOIN_SCRIPT
				PROCESS_PLAYER_JOIN(iCount)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC
/// PURPOSE:
///    
PROC MAINTAIN_FINDERS_KEEPERS_PLAYER_LEFT()
	IF MAINTAIN_PLAYER_LEAVING_AMBIENT_ACTIVITY_LEADERBOARD(serverBD.sSortedPackagesCollected, serverStaggeredPlayerLoop, AAL_SORT_DESCENDING)
		serverBD.iServerSyncId++
	ENDIF
ENDPROC

FUNC MAP_AREA_ZONE GET_PLAYERS_CURRENT_AREA()

	VECTOR	VCONTAINERSHIP	= GET_AREA_BLIP_COORDINATES(CONTAINERSHIP) // <<3172.2461, -337.4840, -14.5003>>  // <<-2268.8484, 293.1660, 173.6019>>
	VECTOR	VPLANENORTH		= GET_AREA_BLIP_COORDINATES(PLANENORTH	) // 	<<-978.4571, 6603.1729, -12.8434>>// <<473.4940, -1271.3734, 28.6789>>
	VECTOR	VPADDLESTEAMER	= GET_AREA_BLIP_COORDINATES(PADDLESTEAMER) //<<2671.5374, -1398.6479, -8.4314>>//  <<-1633.5690, -291.0810, 50.6773>>
	VECTOR	VCARGOSHIP		= GET_AREA_BLIP_COORDINATES(CARGOSHIP) //<<-203.7591, -2863.4998, -0.9841>>
	VECTOR	VPLANESOUTH		= GET_AREA_BLIP_COORDINATES(PLANESOUTH	) // <<1794.1816, -2968.3179, -3.0297>>
	VECTOR	VMILITARYDEBRIS	= GET_AREA_BLIP_COORDINATES(MILITARYDEBRIS) // <<4128.1602, 3537.0808, -26.9639>>
	VECTOR	VSUBMARINE		= GET_AREA_BLIP_COORDINATES(SUBMARINE) // <<2651.7754, 6647.8843, -3.8573>>

	VECTOR	VMILITARYPLANE	= GET_AREA_BLIP_COORDINATES(MILITARYPLANE) // <<2651.7754, 6647.8843, -3.8573>>
	VECTOR	VBATHYSCAPE		= GET_AREA_BLIP_COORDINATES(BATHYSCAPE	) // <<2651.7754, 6647.8843, -3.8573>>
	VECTOR	VTUGBOAT		= GET_AREA_BLIP_COORDINATES(TUGBOAT) // <<2651.7754, 6647.8843, -3.8573>>


	FLOAT distancefromVCONTAINERSHIP	 = VDIST(GET_ENTITY_COORDS(GET_PLAYER_PED( GB_GET_LOCAL_PLAYER_GANG_BOSS())), VCONTAINERSHIP)				
	FLOAT distancefromVPLANENORTH	     = VDIST(GET_ENTITY_COORDS(GET_PLAYER_PED( GB_GET_LOCAL_PLAYER_GANG_BOSS())), VPLANENORTH		)
	FLOAT distancefromVPADDLESTEAMER     = VDIST(GET_ENTITY_COORDS(GET_PLAYER_PED( GB_GET_LOCAL_PLAYER_GANG_BOSS())), VPADDLESTEAMER   )
	FLOAT distancefromVCARGOSHIP     	 = VDIST(GET_ENTITY_COORDS(GET_PLAYER_PED( GB_GET_LOCAL_PLAYER_GANG_BOSS())), VCARGOSHIP   )
	FLOAT distancefromVPLANESOUTH    	 = VDIST(GET_ENTITY_COORDS(GET_PLAYER_PED( GB_GET_LOCAL_PLAYER_GANG_BOSS())), VPLANESOUTH   )
	FLOAT distancefromVMILITARYDEBRIS    = VDIST(GET_ENTITY_COORDS(GET_PLAYER_PED( GB_GET_LOCAL_PLAYER_GANG_BOSS())), VMILITARYDEBRIS   )
	FLOAT distancefromVSUBMARINE    	 = VDIST(GET_ENTITY_COORDS(GET_PLAYER_PED( GB_GET_LOCAL_PLAYER_GANG_BOSS())), VSUBMARINE   )
	
	FLOAT distancefromVMILITARYPLANE     = VDIST(GET_ENTITY_COORDS(GET_PLAYER_PED( GB_GET_LOCAL_PLAYER_GANG_BOSS())), VMILITARYPLANE   )
	FLOAT distancefromVBATHYSCAPE    	 = VDIST(GET_ENTITY_COORDS(GET_PLAYER_PED( GB_GET_LOCAL_PLAYER_GANG_BOSS())), VBATHYSCAPE   )
	FLOAT distancefromVTUGBOAT    	     = VDIST(GET_ENTITY_COORDS(GET_PLAYER_PED( GB_GET_LOCAL_PLAYER_GANG_BOSS())), VTUGBOAT   )
	
	
	
	
	PRINTLN("[salvage]  GET_PLAYERS_CURRENT_AREA   distancefromVPLANENORTH		= ", 	distancefromVPLANENORTH		)
	PRINTLN("[salvage]  GET_PLAYERS_CURRENT_AREA   distancefromVPADDLESTEAMER  =  ",     distancefromVPADDLESTEAMER    )
	PRINTLN("[salvage]  GET_PLAYERS_CURRENT_AREA   VCONTAINERSHIP	  =  ",     VCONTAINERSHIP	    )

	MAP_AREA_ZONE closestmapzone = GENERAL
	
	FLOAT ftempclosestdistance = 100000.0
	
	IF distancefromVCONTAINERSHIP < ftempclosestdistance
		closestmapzone = CONTAINERSHIP
		ftempclosestdistance =  distancefromVCONTAINERSHIP
	ENDIF
	
	IF distancefromVPLANENORTH < ftempclosestdistance
		closestmapzone = PLANENORTH
		ftempclosestdistance =  distancefromVPLANENORTH
	ENDIF
	
	IF distancefromVPADDLESTEAMER < ftempclosestdistance
		closestmapzone = PADDLESTEAMER
		ftempclosestdistance =  distancefromVPADDLESTEAMER
	ENDIF
	
	IF distancefromVPLANESOUTH < ftempclosestdistance
		closestmapzone = PLANESOUTH
		ftempclosestdistance =  distancefromVPLANESOUTH
	ENDIF
	
	IF distancefromVCARGOSHIP< ftempclosestdistance
		closestmapzone = CARGOSHIP
		ftempclosestdistance =  distancefromVCARGOSHIP
	ENDIF
	IF distancefromVMILITARYDEBRIS < ftempclosestdistance
		closestmapzone = MILITARYDEBRIS 
		ftempclosestdistance =  distancefromVMILITARYDEBRIS
	ENDIF
	
	IF distancefromVSUBMARINE < ftempclosestdistance
		closestmapzone = SUBMARINE 
		ftempclosestdistance =  distancefromVSUBMARINE 
	ENDIF
	
	IF distancefromVMILITARYPLANE < ftempclosestdistance
		closestmapzone = MILITARYPLANE 
		ftempclosestdistance =  distancefromVMILITARYPLANE 
	ENDIF
	IF distancefromVBATHYSCAPE < ftempclosestdistance
		closestmapzone = BATHYSCAPE  
		ftempclosestdistance =  distancefromVBATHYSCAPE 
	ENDIF
	IF distancefromVTUGBOAT  < ftempclosestdistance
		closestmapzone = TUGBOAT  
		ftempclosestdistance =  distancefromVTUGBOAT  
	ENDIF
	

	
	PRINTLN("[salvage]  GET_PLAYERS_CURRENT_AREA closestmapzone = ", ENUM_TO_INT(closestmapzone))
	PRINTLN("[salvage]  GET_PLAYERS_CURRENT_AREA ftempclosestdistance = ", ftempclosestdistance)
	RETURN closestmapzone


ENDFUNC


FUNC VECTOR GET_REBREATHER_COORDINATES( MAP_AREA_ZONE players_area, INT icounter)


	IF  players_area = CONTAINERSHIP
		SWITCH icounter
			CASE 0 RETURN <<3142.7810, -258.6545, -21.1781>>	PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT 3142.7810, -258.6545, -21.1781")BREAK
			CASE 1 RETURN <<3149.4570, -291.8208, -25.1835>>    PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT 3149.4570, -291.8208, -25.1835")BREAK
			CASE 2 RETURN <<3157.3423, -308.1521, -27.5322>>    PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT 3157.3423, -308.1521, -27.5322")BREAK
			CASE 3 RETURN <<3155.6841, -346.0982, -25.8994>>    PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT 3155.6841, -346.0982, -25.8994")BREAK
			CASE 4 RETURN <<3185.8042, -343.2384, -30.1358>>    PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT 3185.8042, -343.2384, -30.1358")BREAK
			CASE 5 RETURN <<3177.1279, -370.8136, -29.4753>>    PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT 3177.1279, -370.8136, -29.4753")BREAK
			CASE 6 RETURN <<3193.6179, -368.2251, -29.2594>>    PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT 3193.6179, -368.2251, -29.2594")BREAK
			CASE 7 RETURN <<3207.8567, -384.6090, -15.8373>>    PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT 3207.8567, -384.6090, -15.8373")BREAK
			CASE 8 RETURN <<3163.4536, -378.8613, -18.8252>>    PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT 3163.4536, -378.8613, -18.8252")BREAK
			CASE 9 RETURN <<3188.5115, -305.0131, -10.0456>>    PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT 3188.5115, -305.0131, -10.0456")BREAK
		ENDSWITCH
	ENDIF
	
	IF  players_area = PLANENORTH
		SWITCH icounter
			CASE 0 RETURN<<-989.5922, 6565.9165, -24.2550>>     PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT-989.5922, 6565.9165, -24.2550")BREAK
			CASE 1 RETURN<<-1026.5356, 6518.3999, -35.3361>>    PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT-1026.5356, 6518.3999, -35.3361")BREAK
			CASE 2 RETURN<<-916.1893, 6637.1045, -30.7942>>     PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT-916.1893, 6637.1045, -30.7942")BREAK
			CASE 3 RETURN<<-929.2811, 6591.7754, -29.5562>>     PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT-929.2811, 6591.7754, -29.5562")BREAK
			CASE 4 RETURN<<-874.1296, 6581.4766, -29.3949>>     PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT-874.1296, 6581.4766, -29.3949")BREAK
			CASE 5 RETURN<<-811.0801, 6664.5801, -19.0324>>     PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT-811.0801, 6664.5801, -19.0324")BREAK
			CASE 6 RETURN<<-942.7502, 6689.8491, -30.6195>>     PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT-942.7502, 6689.8491, -30.6195")BREAK
			CASE 7 RETURN<<-984.6858, 6665.8081, -32.0440>>     PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT-984.6858, 6665.8081, -32.0440")BREAK
			CASE 8 RETURN<<-970.4923, 6492.4146, -30.8570>>     PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT-970.4923, 6492.4146, -30.8570")BREAK
			CASE 9 RETURN<<-878.9694, 6622.4199, -30.8634>>     PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT-878.9694, 6622.4199, -30.8634")BREAK
		ENDSWITCH
	ENDIF
		
	IF  players_area = PADDLESTEAMER
		SWITCH icounter
			CASE 0 RETURN <<2685.3232, -1398.1770, -13.6244>>    PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT-989.5922, 6565.9165, -24.2550")BREAK
			CASE 1 RETURN <<2665.1643, -1414.4856, -20.5692>>    PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT-1026.5356, 6518.3999, -35.3361")BREAK
			CASE 2 RETURN <<2644.1982, -1423.7665, -19.0826>>    PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT-916.1893, 6637.1045, -30.7942")BREAK
			CASE 3 RETURN <<2680.7532, -1369.1603, -19.6391>>    PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT-929.2811, 6591.7754, -29.5562")BREAK
			CASE 4 RETURN <<2649.2952, -1365.4832, -16.2271>>    PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT-874.1296, 6581.4766, -29.3949")BREAK
			CASE 5 RETURN <<2608.8882, -1407.2898, -14.6000>>    PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT-811.0801, 6664.5801, -19.0324")BREAK
			CASE 6 RETURN <<2656.1558, -1494.7499, -17.9674>>    PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT-942.7502, 6689.8491, -30.6195")BREAK
			CASE 7 RETURN <<2697.7468, -1325.8494, -19.9296>>    PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT-984.6858, 6665.8081, -32.0440")BREAK
			CASE 8 RETURN <<2662.6885, -1433.6698, -20.8508>>    PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT-970.4923, 6492.4146, -30.8570")BREAK
			CASE 9 RETURN <<2649.8943, -1394.2932, -16.7490>>    PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT-878.9694, 6622.4199, -30.8634")BREAK
		ENDSWITCH
	ENDIF
		
		IF  players_area = CARGOSHIP
		SWITCH icounter
			CASE 0 RETURN <<-232.464, -2870.585, -18.0988>>    PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	<<-232.464, -2870.585, -18.0988>>")BREAK
			CASE 1 RETURN <<-111.4055, -2872.1436, -6.5123>>    PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	<<-111.4055, -2872.1436, -6.5123>>1")BREAK
			CASE 2 RETURN <<-298.935, -2884.572, -13.7479>>    PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	<<-298.935, -2884.572, -13.7479>>")BREAK
			CASE 3 RETURN <<-295.4654, -2944.3706, -14.8823>>    PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	<<-295.4654, -2944.3706, -14.8823>>")BREAK
			CASE 4 RETURN <<-216.2081, -2977.9795, -24.2963>>    PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	<<-216.2081, -2977.9795, -24.2963>>")BREAK
			CASE 5 RETURN <<-198.3882, -2936.8015, -13.1733>>    PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	<<-198.3882, -2936.8015, -13.1733>>")BREAK
			CASE 6 RETURN <<-111.7231, -2923.2625, -11.8486>>    PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	<<-111.7231, -2923.2625, -11.8486>>")BREAK
			CASE 7 RETURN <<-133.2846, -2790.6033, -7.1538>>    PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	<<-133.2846, -2790.6033, -7.1538>>")BREAK
			CASE 8 RETURN <<-133.282, -2790.6001, -6.8968>>    PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	<<-133.282, -2790.6001, -6.8968>>")BREAK
			CASE 9 RETURN <<-271.0499, -2817.2776, -7.4216>>    PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	<<-271.0499, -2817.2776, -7.4216>>")BREAK
		ENDSWITCH
	ENDIF
		
		
		
		
		
				
	IF  players_area = PLANESOUTH
		SWITCH icounter
			CASE 0 RETURN <<1831.7064, -2938.6311, -45.2356>>  PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	<<1831.7064, -2938.6311, -45.2356>>>>")BREAK
			CASE 1 RETURN <<1762.5571, -2991.8362, -50.5554>>   PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	<<1762.5571, -2991.8362, -50.5554>>3>>1")BREAK
			CASE 2 RETURN <<1817.9899, -2919.8904, -31.9195>>  PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	<<1817.9899, -2919.8904, -31.9195>>>>")BREAK
			CASE 3 RETURN <<1869.6853, -2947.8806, -46.0897>>    PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	<<1869.6853, -2947.8806, -46.0897>>23>>")BREAK
			CASE 4 RETURN <<1871.8484, -3036.0205, -47.687>>    PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	<<1871.8484, -3036.0205, -47.687>>63>>")BREAK
			CASE 5 RETURN <<1828.0731, -3031.95, -34.9909>>    PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	<<1828.0731, -3031.95, -34.9909>>33>>")BREAK
			CASE 6 RETURN <<1878.0989, -2930.4917, -37.7912>>    PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	<<1878.0989, -2930.4917, -37.7912>>86>>")BREAK
			CASE 7 RETURN <<1750.2693, -2928.7454, -34.4448>>   PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	<<1750.2693, -2928.7454, -34.4448>>8>>")BREAK
			CASE 8 RETURN <<1721.2423, -2980.9277, -29.2093>>  PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	<<1721.2423, -2980.9277, -29.2093>>>>")BREAK
			CASE 9 RETURN <<1721.2422, -2980.9211, -28.5943>>   PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	<<1721.2422, -2980.9211, -28.5943>>6>>")BREAK
		ENDSWITCH
	ENDIF
		
		
	IF  players_area = MILITARYDEBRIS
		SWITCH icounter
			CASE 0 RETURN <<4156.2344, 3499.9768, -28.9651>>  	PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	  <<4156.2344, 3499.9768, -28.9651>>     ")BREAK
			CASE 1 RETURN <<4123.4834, 3524.5154, -27.3879>>   PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	  <<4123.4834, 3524.5154, -27.3879>>     	")BREAK
			CASE 2 RETURN <<4184.126, 3567.26, -9.0319>>  		PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	  <<4184.126, 3567.26, -9.0319>>     ")BREAK
			CASE 3 RETURN <<4184.1255, 3567.2593, -8.7436>>    PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	  <<4184.1255, 3567.2593, -8.7436>>     	")BREAK
			CASE 4 RETURN <<4069.2585, 3597.9187, -20.1678>>   PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	  <<4069.2585, 3597.9187, -20.1678>>     ")BREAK
			CASE 5 RETURN <<4112.9106, 3667.8672, -12.7752>>  	PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	  <<4112.9106, 3667.8672, -12.7752>>     ")BREAK
			CASE 6 RETURN <<4112.9121, 3667.873, -12.5514>>    PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	  <<4112.9121, 3667.873, -12.5514>>     	")BREAK
			CASE 7 RETURN <<4211.2314, 3645.2463, -36.9892>>   PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	  <<4211.2314, 3645.2463, -36.9892>>     ")BREAK
			CASE 8 RETURN <<4204.519, 3615.5486, -22.8357>>  	PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	  <<4204.519, 3615.5486, -22.8357>>     ")BREAK
			CASE 9 RETURN <<4248.5615, 3568.8093, -28.0085>>   PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	  <<4248.5615, 3568.8093, -28.0085>>     ")BREAK
		ENDSWITCH
	ENDIF	
	
	IF  players_area = SUBMARINE
		SWITCH icounter
			CASE 0 RETURN<<2683.8635, 6651.7612, -17.1646>> 	PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	  <<2683.8635, 6651.7612, -17.1646>>    ")BREAK
			CASE 1 RETURN<<2659.4888, 6648.311, -25.0424>>      PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	  <<2659.4888, 6648.311, -25.0424>>    	")BREAK
			CASE 2 RETURN<<2642.9099, 6673.7441, -23.2345>>	    PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	  <<2642.9099, 6673.7441, -23.2345>>	 ")BREAK
			CASE 3 RETURN<<2759.3396, 6634.2397, -24.695>>      PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	  <<2759.3396, 6634.2397, -24.695>>   	")BREAK
			CASE 4 RETURN<<2702.9128, 6619.7104, -23.8372>>      PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	  <<2702.9128, 6619.7104, -23.8372>>    ")BREAK
			CASE 5 RETURN<<2699.209, 6683.521, -36.9647>> 	    PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	  <<2699.209, 6683.521, -36.9647>>    ")BREAK
			CASE 6 RETURN<<2718.2793, 6661.374, -28.1706>>      PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	  <<2718.2793, 6661.374, -28.1706>>   	")BREAK
			CASE 7 RETURN<<2630.0259, 6630.8667, -13.7466>>      PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	  <<2630.0259, 6630.8667, -13.7466>>    ")BREAK
			CASE 8 RETURN<<2613.6721, 6676.5103, -19.7247>>	    PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	  <<2613.6721, 6676.5103, -19.7247>>   ")BREAK
			CASE 9 RETURN<<2637.5901, 6610.6943, -8.8308>>      PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	  <<2637.5901, 6610.6943, -8.8308>>    ")BREAK
		ENDSWITCH
	ENDIF
		
	IF  players_area = MILITARYPLANE
		SWITCH icounter
			CASE 0 RETURN  <<-3241.9656, 3688.9651, -20.1426>>		PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	<<-3241.9656, 3688.9651, -20.1426>>    ")BREAK
			CASE 1 RETURN  <<-3208.5549, 3640.823, -35.7548>>   	PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	<<-3208.5549, 3640.823, -35.7548>>   	")BREAK
			CASE 2 RETURN 	<<-3235.6765, 3642.6904, -32.7612>>   	PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	<<-3235.6765, 3642.6904, -32.7612>>	 ")BREAK
			CASE 3 RETURN  <<-3282.1055, 3641.5625, -26.2563>>   	PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	<<-3282.1055, 3641.5625, -26.2563>>  	")BREAK
			CASE 4 RETURN  <<-3218.2556, 3687.9048, -25.9951>>    	PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	<<-3218.2556, 3687.9048, -25.9951>>    ")BREAK
			CASE 5 RETURN  <<-3160.1523, 3700.6992, -22.8258>>  	PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	<<-3160.1523, 3700.6992, -22.8258>>  ")BREAK
			CASE 6 RETURN  <<-3151.1982, 3647.7224, -31.4883>>   	PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	<<-3151.1982, 3647.7224, -31.4883>>  	")BREAK
			CASE 7 RETURN  <<-3262.1897, 3674.48, -25.8873>>    	PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	<<-3262.1897, 3674.48, -25.8873>>    ")BREAK
			CASE 8 RETURN 	<<-3259.4561, 3590.2712, -31.6526>>     PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	<<-3259.4561, 3590.2712, -31.6526>>   ")BREAK
			CASE 9 RETURN  <<-3223.8589, 3715.489, -33.1337>>   	PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	<<-3223.8589, 3715.489, -33.1337>>   ")BREAK
		ENDSWITCH
	ENDIF
		
		
	IF  players_area = BATHYSCAPE
		SWITCH icounter
			CASE 0 RETURN      <<-2839.0601, -445.753, -16.6761>>	PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	      <<-2839.0601, -445.753, -16.6761>>   ")BREAK
			CASE 1 RETURN      <<-2852.8157, -422.0252, -9.3182>>    PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	      <<-2852.8157, -422.0252, -9.3182>>   ")BREAK
			CASE 2 RETURN      <<-2848.1091, -493.7422, -16.5211>>    PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	  <<-2848.1091, -493.7422, -16.5211>>  	 ")BREAK
			CASE 3 RETURN      <<-2835.3201, -520.9986, -34.6716>>    PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	  <<-2835.3201, -520.9986, -34.6716>>   	")BREAK
			CASE 4 RETURN      <<-2864.3059, -578.0115, -20.2448>>     PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT     <<-2864.3059, -578.0115, -20.2448>>  ")BREAK
			CASE 5 RETURN      <<-2808.7815, -577.7209, -20.8295>>    PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	  <<-2808.7815, -577.7209, -20.8295>>   ")BREAK
			CASE 6 RETURN      <<-2774.604, -440.0441, -34.5225>>    PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	      <<-2774.604, -440.0441, -34.5225>>  ")BREAK
			CASE 7 RETURN      <<-2798.3301, -388.744, -26.0379>>     PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	  <<-2798.3301, -388.744, -26.0379>>     ")BREAK
			CASE 8 RETURN      <<-2901.9021, -395.7604, -39.0237>>    PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	  <<-2901.9021, -395.7604, -39.0237>>    ")BREAK
			CASE 9 RETURN      <<-2887.6013, -495.8942, -38.2618>>    PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	  <<-2887.6013, -495.8942, -38.2618>>    ")BREAK
		ENDSWITCH
	ENDIF
		
		
	IF  players_area = TUGBOAT
		SWITCH icounter
			CASE 0 RETURN      <<-3178.99, 3039.0574, -34.0949>> 	PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	      <<-3178.99, 3039.0574, -34.0949>>    ")BREAK
			CASE 1 RETURN      <<-3151.3179, 3041.0935, -18.6523>>     PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	  <<-3151.3179, 3041.0935, -18.6523>>  	")BREAK
			CASE 2 RETURN      <<-3174.0542, 2991.4753, -37.0318>>	    PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	  <<-3174.0542, 2991.4753, -37.0318>>	 ")BREAK
			CASE 3 RETURN      <<-3231.0859, 3028.2664, -31.3029>>     PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	  <<-3231.0859, 3028.2664, -31.3029>> 	")BREAK
			CASE 4 RETURN      <<-3258.6016, 3079.2427, -33.5343>>      PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	  <<-3258.6016, 3079.2427, -33.5343>>   ")BREAK
			CASE 5 RETURN      <<-3216.9326, 3078.686, -33.9647>>    PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	      <<-3216.9326, 3078.686, -33.9647>>  ")BREAK
			CASE 6 RETURN      <<-3237.8838, 2994.729, -28.6229>>     PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	  <<-3237.8838, 2994.729, -28.6229>>  	")BREAK
			CASE 7 RETURN      <<-3199.2488, 3081.5796, -25.5331>>      PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	  <<-3199.2488, 3081.5796, -25.5331>>   ")BREAK
			CASE 8 RETURN      <<-3197.7998, 2975.1814, -18.7584>>	    PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	  <<-3197.7998, 2975.1814, -18.7584>>  ")BREAK
			CASE 9 RETURN      <<-3121.2246, 3057.6792, -15.9893>>     PRINTLN("GET_REBREATHER_COORDINATES  REBREATHER CREATED AT	  <<-3121.2246, 3057.6792, -15.9893>>  ")BREAK
		ENDSWITCH
	ENDIF
		

		
	PRINTLN("[SALVAGE] CREATE_INITIAL_REBREATHER_PICKUPS LOCATION  = CONTAINERSHIP")
	SCRIPT_ASSERT("INVALID OUTCOME IN [salvage] GET_REBREATHER_COORDINATES")
	RETURN <<3188.5115, -305.0131, -10.0456>>    
ENDFUNC


//PURPOSE: Get Random Location for checkpoints
FUNC BOOL ADD_BLIP_FOR_REBREATHER_PICKUPS()
	INT i
	
	REPEAT MAX_NUM_REBREATHERS i

		//IF DOES_ENTITY_EXIST(serverBD.objrebreather[i])
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.network_index_rebreather[i])
		AND NOT DOES_BLIP_EXIST( bliprebreather[i])
		//AND NOT HAS_PICKUP_BEEN_COLLECTED(serverBD.objrebreather[i])
			bliprebreather[i] = ADD_BLIP_FOR_ENTITY (NET_TO_ENT(serverBD.network_index_rebreather[i]))
			g_salvage_refresh_rebreather_blips = FALSE
			bcreate_rebreather_blips = TRUE
			//SET_BLIP_SPRITE(bliprebreather[i], RADAR_TRACE_MASK)
			SET_BLIP_SCALE(bliprebreather[i],1.0  )
			SET_BLIP_COLOUR_FROM_HUD_COLOUR(bliprebreather[i],HUD_COLOUR_GREEN)
			SET_BLIP_NAME_FROM_TEXT_FILE(bliprebreather[i],"BLIP_REBREATHE")
			SET_BLIP_PRIORITY(bliprebreather[i], BLIP_PRIORITY_HIGHEST_SPECIAL_HIGH)
//				SHOW_HEIGHT_ON_BLIP(currentCheckpoint.cpBlip,TRUE)
			PRINTLN("[SALVAGE] ADD_BLIP_FOR_REBREATHER_PICKUPS ",i )
			IF i = MAX_NUM_REBREATHERS-1

				PRINTLN("[SALVAGE] ADD_BLIP_FOR_REBREATHER_PICKUPS  done")
				RETURN	TRUE
			ENDIF
			//g_salvage_player_has_collected_rebreather = FALSE
		ELSE
			//PRINTLN("[SALVAGE] ADD_BLIP_FOR_REBREATHER_PICKUPS REBREATHER DOES NOT EXIST COUNTER ", i)
		ENDIF
	ENDREPEAT		                   
		
	
	
	
	//PRINTLN("[SALVAGE] ADD_BLIP_FOR_REBREATHER_PICKUPS NOT done")
	RETURN	FALSE
ENDFUNC



//PURPOSE: Get Random Location for checkpoints
FUNC BOOL CREATE_INITIAL_REBREATHER_PICKUPS( MAP_AREA_ZONE players_area)
	INT i
	
	
	

	PRINTLN("[SALVAGE] CREATE_INITIAL_REBREATHER_PICKUPS	GB_GET_NUM_GOONS_IN_LOCAL_GANG() =  ", 	GB_GET_NUM_GOONS_IN_LOCAL_GANG())
	IF GB_GET_NUM_GOONS_IN_LOCAL_GANG() = 1
		serverBD.inumberofrebreathers = g_sMPTunables.iexec_salvage_number_of_active_rebreathers_2p
		PRINTLN("[SALVAGE] CREATE_INITIAL_REBREATHER_PICKUPS g_sMPTunables.iexec_salvage_number_of_active_rebreathers_2p ")
	ELIF GB_GET_NUM_GOONS_IN_LOCAL_GANG() = 2
		serverBD.inumberofrebreathers = g_sMPTunables.iexec_salvage_number_of_active_rebreathers_3p
		PRINTLN("[SALVAGE] CREATE_INITIAL_REBREATHER_PICKUPS g_sMPTunables.iexec_salvage_number_of_active_rebreathers_3p ")
	ELIF GB_GET_NUM_GOONS_IN_LOCAL_GANG() = 3
		serverBD.inumberofrebreathers = g_sMPTunables.iexec_salvage_number_of_active_rebreathers_4p
		PRINTLN("[SALVAGE] CREATE_INITIAL_REBREATHER_PICKUPS g_sMPTunables.iexec_salvage_number_of_active_rebreathers_4p ")

	ELSE
		serverBD.inumberofrebreathers = g_sMPTunables.iexec_salvage_number_of_active_rebreathers_4p
		PRINTLN("[SALVAGE] CREATE_INITIAL_REBREATHER_PICKUPS g_sMPTunables.iexec_salvage_number_of_active_rebreathers_4p GB_GET_NUM_GOONS_IN_LOCAL_GANG is returning invalid number SOMETHING HAS GONE FUNNY")

	
	ENDIF
	
	
	IF serverBD.inumberofrebreathers >= MAX_NUM_REBREATHERS
		serverBD.inumberofrebreathers = 9
	ENDIF
	PRINTLN("[SALVAGE] serverBD.inumberofrebreathers ",serverBD.inumberofrebreathers)
	IF HAS_MODEL_LOADED(hei_prop_carrier_crate_01a)

		PRINTLN("[SALVAGE] CREATE_INITIAL_REBREATHER_PICKUPS MAP_AREA_ZONE = ",ENUM_TO_INT(players_area))
		VECTOR vrebreathercoords[10] 
		REPEAT serverBD.inumberofrebreathers i
			vrebreathercoords[i] = GET_REBREATHER_COORDINATES( players_area, i)
			IF CREATE_REBREATHER_CRATE(serverBD.objrebreather[i],serverBD.network_index_rebreather[i], vrebreathercoords[i], TRUE)
				GB_ENABLE_PICKUP_FOR_GANG_MEMBERS(serverBD.objrebreather[i])
				PRINTLN("[SALVAGE] CREATE_INITIAL_REBREATHER_PICKUPS ",i ,vrebreathercoords[i])
				serverBD.bcreate_rebreathers = TRUE
				
			ENDIF
		ENDREPEAT		                   
		RETURN	TRUE	
	ENDIF
	
	PRINTLN("[SALVAGE] CREATE_INITIAL_REBREATHER_PICKUPS FALSE")
	RETURN	FALSE
ENDFUNC

PROC CHECK_IF_THERE_IS_ALREADY_REBREATHER_AT_POSITION(MAP_AREA_ZONE players_area, INT &z )

		VECTOR vrebreathercoords
		vrebreathercoords = GET_REBREATHER_COORDINATES( players_area, z)
		IF GET_CLOSEST_OBJECT_OF_TYPE(vrebreathercoords, 5.0,hei_prop_carrier_crate_01a, FALSE, FALSE,FALSE) != NULL
		OR NETWORK_DOES_NETWORK_ID_EXIST(serverBD.network_index_rebreather[z])
			PRINTLN("[SALVAGE] CHECK_IF_THERE_IS_ALREADY_REBREATHER_AT_POSITION Already a package here.  ",z)	
			z++
		ENDIF
		IF z >= MAX_NUM_REBREATHERS
			z = 0
		ENDIF

ENDPROC

FUNC BOOL CREATE_REPLACEMENT_REBREATHER_PICKUP( MAP_AREA_ZONE players_area, int lastrebreatherindex= 0)
	INT i
	int z
	IF CAN_REGISTER_MISSION_OBJECTS(GB_GET_BOSS_MISSION_NUM_OBJ_REQUIRED(FMMC_TYPE_GB_SALVAGE))
		IF HAS_MODEL_LOADED(hei_prop_carrier_crate_01a)
		
			//REPEAT MAX_NUM_REBREATHERS i
				
		
					PRINTLN("[SALVAGE] CREATE_REPLACEMENT_REBREATHER_PICKUP lastrebreatherindex = ",lastrebreatherindex)
					PRINTLN("[SALVAGE] CREATE_REPLACEMENT_REBREATHER_PICKUP i = ",i)	
					z = lastrebreatherindex + 1
					IF z >= MAX_NUM_REBREATHERS
						z = 0
					ENDIF
					
					INT b
					INT irebreathercounter
					REPEAT MAX_NUM_REBREATHERS b
						IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.network_index_rebreather[b])
							irebreathercounter++
							PRINTLN("[SALVAGE] CREATE_REPLACEMENT_REBREATHER_PICKUP irebreathercounter = ",irebreathercounter)	
						ENDIF
					ENDREPEAT
					
					IF irebreathercounter >=serverBD.inumberofrebreathers 
						PRINTLN("[SALVAGE] CREATE_REPLACEMENT_REBREATHER_PICKUP TOO MANY REBREATHER irebreathercounter = ",irebreathercounter)	
						PRINTLN("[SALVAGE] CREATE_REPLACEMENT_REBREATHER_PICKUP TOO MANY REBREATHER serverBD.inumberofrebreathers  = ",serverBD.inumberofrebreathers )
						g_salvage_refresh_rebreather_blips = TRUE 
						// do not create any more rebreathers
						RETURN	TRUE
					ELSE
						PRINTLN("[SALVAGE] CREATE_REPLACEMENT_REBREATHER_PICKUP2 z = ",z)	
							
						CHECK_IF_THERE_IS_ALREADY_REBREATHER_AT_POSITION(players_area, z)
						CHECK_IF_THERE_IS_ALREADY_REBREATHER_AT_POSITION(players_area, z)
						CHECK_IF_THERE_IS_ALREADY_REBREATHER_AT_POSITION(players_area, z)
						CHECK_IF_THERE_IS_ALREADY_REBREATHER_AT_POSITION(players_area, z)
						CHECK_IF_THERE_IS_ALREADY_REBREATHER_AT_POSITION(players_area, z)
						CHECK_IF_THERE_IS_ALREADY_REBREATHER_AT_POSITION(players_area, z)
						CHECK_IF_THERE_IS_ALREADY_REBREATHER_AT_POSITION(players_area, z)
						
						VECTOR vrebreathercoords
						vrebreathercoords = GET_REBREATHER_COORDINATES( players_area, z)
						IF CREATE_REBREATHER_CRATE(serverBD.objrebreather[z],serverBD.network_index_rebreather[z], vrebreathercoords, TRUE)
							GB_ENABLE_PICKUP_FOR_GANG_MEMBERS(serverBD.objrebreather[z])
							PRINTLN("[SALVAGE] CREATE_REPLACEMENT_REBREATHER_PICKUP MAP_AREA_ZONE = ",ENUM_TO_INT(players_area))		
							PRINTLN("[SALVAGE] CREATE_REPLACEMENT_REBREATHER_PICKUP z = ",z)	
							PRINTLN("[SALVAGE] CREATE_REPLACEMENT_REBREATHER_PICKUP vrebreathercoords = ",vrebreathercoords)	
							g_salvage_refresh_rebreather_blips = TRUE 
							RETURN	TRUE
						ENDIF
	               ENDIF
					
			//ENDREPEAT
		ELSE
			REQUEST_LOAD_MODEL(hei_prop_carrier_crate_01a)
		ENDIF
	ELSE
		PRINTLN("[SALVAGE] CAN_REGISTER_MISSION_OBJECTS 1 = FALSE  ")
	
	ENDIF
	
	
	PRINTLN("[SALVAGE] CREATE_REPLACEMENT_REBREATHER_PICKUP FALSE")
	RETURN	FALSE
ENDFUNC



//Helper function to get the servers game/mission state
FUNC INT GET_SERVER_MISSION_STATE()
	RETURN serverBD.iServerGameState
ENDFUNC



/// PURPOSE:
///    Checks if a specific player is near the package
/// PARAMS:
///    thisPlayer - The player to check
/// RETURNS:
///    True if so
FUNC BOOL IS_THIS_PLAYER_NEAR_CHECKPOINT_AREA(PLAYER_INDEX thisPlayer)
	PED_INDEX playerPed = GET_PLAYER_PED(thisPlayer)
	IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(playerPed, serverBD.vareablipcoordinates, FALSE) < (g_sMPTunables.fexec_salvage_area_radius   + 20.0  )                                  

		IF initialisemusic = 0
			GB_SET_PLAYER_AS_QUALIFYING_PARTICIPANT()
			TRIGGER_MUSIC_EVENT("SALVAGE_START_MUSIC")

			initialisemusic = 1
		ENDIF
		REMOVE_PACKAGE_AREA_BLIP_LONG_RANG_BLIP()
		RETURN TRUE
	ELSE
		IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(playerPed, serverBD.vareablipcoordinates, FALSE) > g_sMPTunables.fexec_salvage_area_radius     + 50.0                                    
			IF initialisemusic = 1
				IF AUDIO_IS_SCRIPTED_MUSIC_PLAYING()
					TRIGGER_MUSIC_EVENT("SALVAGE_STOP_MUSIC")
					initialisemusic = 0
				ENDIF
			ENDIF
		ENDIF
		
		IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
			ADD_FINDERS_KEEPERS_BLIP()
		ENDIF
	ENDIF
	

	
	RETURN FALSE
ENDFUNC

FUNC BOOL CHECK_TO_DELETE_JETSKI_BLIP(int i)
	
		IF DOES_BLIP_EXIST(blipjetski[i])
			IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.salvagelocalplayersjetski[i])
			
				/*IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), GETJETSKISPAWN_COORDS(), << 15.0,15.0,15.0>>)
					PRINTLN("[SALVAGE] CHECK_TO_DELETE_JETSKI_BLIP() TRUE IS_ENTITY_AT_COORD(), blipjetski, << 5.0,5.0,5.0>>) ")
					RETURN TRUE
				ENDIF
				
				IF IS_PED_IN_ANY_BOAT(PLAYER_PED_ID())
					PRINTLN("[SALVAGE] CHECK_TO_DELETE_JETSKI_BLIP() TRUEIS_PED_IN_ANY_BOAT(PLAYER_PED_ID()) ")
					RETURN TRUE
				ENDIF*/
				
				IF IS_PED_IN_THIS_VEHICLE(player_ped_id(), NET_TO_VEH(serverBD.salvagelocalplayersjetski[i]))
					PRINTLN("[SALVAGE] CHECK_TO_DELETE_JETSKI_BLIP() IS_PED_IN_THIS_VEHICLE TRUE ")
					bmarkalljetskistobedeleted = TRUE
					RETURN TRUE
				ENDIF 
				
			    IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					VEHICLE_INDEX players_car = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					  IF IS_VEHICLE_MODEL(players_car, SEASHARK)
					  	PRINTLN("[SALVAGE] CHECK_TO_DELETE_JETSKI_BLIP() Player is in seashark")
					 	 bmarkalljetskistobedeleted = TRUE
						 RETURN TRUE
					  ENDIF
				
				ENDIF
				
				
				
				IF NOT IS_VEHICLE_SEAT_FREE(NET_TO_VEH(serverBD.salvagelocalplayersjetski[i]), VS_DRIVER)
					PRINTLN("[SALVAGE] CHECK_TO_DELETE_JETSKI_BLIP() TRUEIS_PED_IN_ANY_BOAT(PLAYER_PED_ID()) ")
					RETURN TRUE
				ENDIF
				/*IF IS_THIS_PLAYER_NEAR_CHECKPOINT_AREA(PLAYER_ID())
					PRINTLN("[SALVAGE] CHECK_TO_DELETE_JETSKI_BLIP IS_THIS_PLAYER_NEAR_CHECKPOINT_AREA() TRUE")
					RETURN TRUE
				ENDIF*/
				IF NOT IS_VEHICLE_DRIVEABLE(NET_TO_VEH(serverBD.salvagelocalplayersjetski[i]))
					PRINTLN("[SALVAGE] CHECK_TO_DELETE_JETSKI_BLIP() TRUE IS_VEHICLE_DRIVEABLE")
					RETURN TRUE
				ENDIF
			ELSE
				PRINTLN("[SALVAGE] CHECK_TO_DELETE_JETSKI_BLIP jetski does not exist TRUE")
				RETURN TRUE
			
			ENDIF
		ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT UPDATE_TOTAL_PACKAGES_COLLECTED()

	INT totalcounter = 0
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		totalcounter += serverBD.iclientscore[i]
		PRINTLN("[SALVAGE] UPDATE_TOTAL_PACKAGES_COLLECTED() PLAYER",i, " has", serverBD.iclientscore[i], "packages" )
	ENDREPEAT
	PRINTLN("[SALVAGE] UPDATE_TOTAL_PACKAGES_COLLECTED() = ",totalcounter )
	RETURN totalcounter
ENDFUNC


FUNC BOOL HAS_ANY_PLAYER_REACHED_SALVAGE_AREA()
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
	
		IF playerBD[i].this_player_in_salvage_area  = TRUE
			PRINTLN("HAS_ANY_PLAYER_REACHED_SALVAGE_AREA")
			RETURN TRUE
		ENDIF
		
	ENDREPEAT
	RETURN FALSE
ENDFUNC

PROC RESET_PLAYER_BD_PICKUP_FLAGS()

	PRINTLN("[salvage] RESET_PLAYER_BD_PICKUP_FLAGS() called")
	
	IF (HAS_NET_TIMER_EXPIRED(saftycheckpointtimer,SAFETYTIMER) AND playerBD[PARTICIPANT_ID_TO_INT()].this_player_has_picked_up_package )
	//	SORT_SALVAGE_PLAYER_SCORES(PLAYER_ID(),playerBD[PARTICIPANT_ID_TO_INT()].iclientscore)
		gsalvagegiveplayerapoint = FALSE

	ENDIF
	
	
	playerBD[PARTICIPANT_ID_TO_INT()].this_player_has_picked_up_package = FALSE
	playerBD[PARTICIPANT_ID_TO_INT()].itempcheckpoingnumber = 0
	
	
	

	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		serverBD.has_a_checkpoint_been_passed = FALSE
		PRINTLN("[salvage] gRESET_PLAYER_BD_PICKUP_FLAGS() reset")
		//serverBD.ipackagecounter = UPDATE_TOTAL_PACKAGES_COLLECTED()
		
	ENDIF

ENDPROC



FUNC BOOL HAS_CHECKPOINT_BEEN_COLLECTED_SERVER()
	INT i

	PLAYER_INDEX player_who_collected_package
	REPEAT NUM_NETWORK_PLAYERS i
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
			PLAYER_INDEX Player = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
			
			//IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(Player, GB_GET_LOCAL_PLAYER_GANG_BOSS(), true)
		
				
				IF playerBD[i].this_player_has_picked_up_package  = TRUE
				AND serverBD.has_a_checkpoint_been_passed= FALSE
					
					
					IF NOT IS_BIT_SET(serverBD.serverCheckpointData[GET_LONG_BITSET_INDEX(playerBD[i].itempcheckpoingnumber)].iCheckpointCollectedBS,GET_LONG_BITSET_BIT(playerBD[i].itempcheckpoingnumber))
				
						serverBD.has_a_checkpoint_been_passed = TRUE
						player_who_collected_package  = Player
						
						//IF MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI
							TickerEventData.TickerEvent = TICKER_EVENT_SALVAGE_CHECKPOINT_COLLECTED
							TickerEventData.playerID = player_who_collected_package

							IF  (serverBD.iclientscore[i]) < g_sMPTunables.iexec_salvage_cp_increment_cap            
								TickerEventData.dataInt = g_sMPTunables.iexec_salvage_cp_increment + ( (serverBD.iclientscore[i]) * g_sMPTunables.iexec_salvage_cp_increment)
							ELSE
								TickerEventData.dataInt = g_sMPTunables.iexec_salvage_cp_increment_cap   * g_sMPTunables.iexec_salvage_cp_increment   
							ENDIF
							
							IF TickerEventData.playerID = PLAYER_ID()
								IF display_pickupmoney_help = FALSE	
									display_pickupmoney_help = TRUE
									TickerEventData.dataInt2 = 1
								ELSE
									TickerEventData.dataInt2 = 0
								ENDIF
							ENDIF
							PRINTLN("[salvage] CASH REWARD = HAS_CHECKPOINT_BEEN_COLLECTED_SERVER TickerEventData.dataInt = ", TickerEventData.dataInt)
							BROADCAST_TICKER_EVENT(TickerEventData, ALL_PLAYERS_ON_SCRIPT())
							NET_PRINT_TIME() NET_PRINT("     ---------->     SALVAGE  - TICKER_EVENT_SALVAGE_CHECKPOINT_COLLECTED     <----------     ") NET_NL()
					//	ENDIF
						serverBD.iclientscore[i]++	
						PRINTLN("     ---------->     [SALVAGE] - HAS_CHECKPOINT_BEEN_COLLECTED_SERVER i  = ", i  )
						PRINTLN("     ---------->     [SALVAGE] - HAS_CHECKPOINT_BEEN_COLLECTED_SERVER playerBD[i].itempcheckpoingnumber  = ", playerBD[i].itempcheckpoingnumber  )
						serverBD.serverCheckpointData[playerBD[i].itempcheckpoingnumber].iCollectedBy = i
						//MPGlobalsAmbience.CPCData.player_who_package_been_pickedup[iPlayer] = -1
						SET_BIT(serverBD.serverCheckpointData[GET_LONG_BITSET_INDEX(playerBD[i].itempcheckpoingnumber )].iCheckpointCollectedBS,GET_LONG_BITSET_BIT(playerBD[i].itempcheckpoingnumber ))
						DELETE_PACKAGE()
						//DELETE_CHECKPOINT(localCheckpointData[playerBD[i].itempcheckpoingnumber ].checkpointObj)
					//	REMOVE_BLIP(localCheckpointData[playerBD[i].itempcheckpoingnumber].cpBlip)
					//	localCheckpointData[playerBD[i].itempcheckpoingnumber].has_checkpoint_been_collected = TRUE
					//	playerBD[i].itempcheckpoingnumber = 0
						//RESET_PLAYER_BD_PICKUP_FLAGS()
						PRINTLN("     ---------->     [SALVAGE] - HAS_CHECKPOINT_BEEN_COLLECTED_SERVER serverBD.serverCheckpointData[playerBD[i].itempcheckpoingnumber].iCollectedBy  = ", serverBD.serverCheckpointData[playerBD[i].itempcheckpoingnumber].iCollectedBy  )
						PRINTLN("     ---------->     [SALVAGE] - HAS_CHECKPOINT_BEEN_COLLECTED_SERVER true serverBD.ipackagecounter = ", serverBD.ipackagecounter )
						serverBD.ipackagecounter = UPDATE_TOTAL_PACKAGES_COLLECTED()
						SORT_SALVAGE_PLAYER_SCORES(Player,serverBD.iclientscore[i])
						
						
						
					
						PRINTLN("     ---------->     [SALVAGE] - HAS_CHECKPOINT_BEEN_COLLECTED_SERVER true PLAYER ID = ", i )
					
			
					
					//RESET_PLAYER_BD_PICKUP_FLAGS()
					//serverBD.has_this_package_been_pickedup[playerBD[i].itempcheckpoingnumber] = TRUE
				
			
					//serverBD.has_a_checkpoint_been_passed = FALSE
					RETURN TRUE
				ENDIF
			ENDIF

		
		ENDIF
	ENDREPEAT
	
	// Host calculates the number of checkpoints remainig
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		serverBD.inumberofcheckpointsRemaining = serverBD.inumberofcheckpoints - serverBD.ipackagecounter
	ENDIF

	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns the cash value for the current number of checkpoints collected
///    1 = $100, 2 = $200, 3 = $300 etc (max $1000)
/// RETURNS:
///    
FUNC INT GET_CHECKPOINT_VALUE()
	INT toReturn = serverBD.iclientscore[PARTICIPANT_ID_TO_INT()] * 100
	
	IF toReturn > 1000
		toReturn = 1000
	ENDIF
	
	RETURN toReturn
ENDFUNC


FUNC BOOL HAS_CHECKPOINT_BEEN_COLLECTED_CLIENT()
	//IF DOES_PICKUP_EXIST(the_package)
		AMBIENT_JOB_DATA amData

		if playerBD[PARTICIPANT_ID_TO_INT()].this_player_has_picked_up_package  = FALSE
			BOOL IS_PLAYER_A_PASSENGER = FALSE
			
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
          		VEHICLE_INDEX temp_vehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
            	IF GET_PED_IN_VEHICLE_SEAT(temp_vehicle, VS_DRIVER) != PLAYER_PED_ID()
					IS_PLAYER_A_PASSENGER = TRUE
					PRINTLN("[SALVAGE] HAS_CHECKPOINT_BEEN_COLLECTED_CLIENT IS_PLAYER_A_PASSENGER = TRUE")
				ENDIF
			ENDIF
			

			
			
			INT i
			
			
			REPEAT serverBD.inumberofcheckpoints i
				//IF serverBD.has_this_package_been_pickedup[i] = FALSE
					//IF NOT localCheckpointData[i].has_checkpoint_been_collected
					IF NOT IS_BIT_SET(serverBD.serverCheckpointData[GET_LONG_BITSET_INDEX(i)].iCheckpointCollectedBS,GET_LONG_BITSET_BIT(i))
					AND NOT IS_BIT_SET(iLocalCheckpointCollected[GET_LONG_BITSET_INDEX(i)], GET_LONG_BITSET_BIT(i))
						IF ((GET_DISTANCE_BETWEEN_COORDS(GET_CHECKPOINT_LOCATION(i), GET_ENTITY_COORDS(GET_PLAYER_PED(PLAYER_ID()), FALSE)) < 3.0) AND NOT IS_PLAYER_A_PASSENGER)

								PRINTLN("     ---------->     [SALVAGE] - HAS_CHECKPOINT_BEEN_COLLECTED_CLIENT true, Local Player PARTICIPANT_ID_TO_INT = ", PARTICIPANT_ID_TO_INT())
								PRINTLN("     ---------->     [SALVAGE] - HAS_CHECKPOINT_BEEN_COLLECTED_CLIENT true,CHECK POINT NUMBER i = ",i)
							//	playerBD[PARTICIPANT_ID_TO_INT()].iclientscore++
								playerBD[PARTICIPANT_ID_TO_INT()].this_player_has_picked_up_package = TRUE
								playerBD[PARTICIPANT_ID_TO_INT()].itempcheckpoingnumber = i
								GB_SET_PLAYER_AS_QUALIFYING_PARTICIPANT()
								SET_BIT(iLocalCheckpointCollected[GET_LONG_BITSET_INDEX(i)], GET_LONG_BITSET_BIT(i))
								// Add cash to extra pool for number collected								
								DELETE_CHECKPOINT(localCheckpointData[i].checkpointObj)
								IF DOES_BLIP_EXIST(localCheckpointData[i].cpBlip)
									REMOVE_BLIP(localCheckpointData[i].cpBlip)
								ENDIF
							//	localCheckpointData[i].has_checkpoint_been_collected = TRUE
								IF display_pickupmoney_help = FALSE
									PRINT_HELP_WITH_NUMBER("GB_SAL_CASHPICK",g_sMPTunables.iexec_salvage_cp_increment                                )
									display_pickupmoney_help = TRUE
								ENDIF
								RESET_NET_TIMER(saftycheckpointtimer)	
								
								IF NOT IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()
								AND HAS_NET_TIMER_EXPIRED(helptimer,500)
								//	PLAY_SOUND_FROM_COORD(-1, "Boss_Message_Orange", GET_CHECKPOINT_LOCATION(i),"GTAO_Boss_Goons_FM_Soundset", FALSE)
									RESET_NET_TIMER(rebreathersoundtimer)
								ENDIF
								INT amountcash		
								IF  (serverBD.iclientscore[PARTICIPANT_ID_TO_INT()]) < g_sMPTunables.iexec_salvage_cp_increment_cap            
									amountcash = g_sMPTunables.iexec_salvage_cp_increment + ( (serverBD.iclientscore[PARTICIPANT_ID_TO_INT()]) * g_sMPTunables.iexec_salvage_cp_increment)
								ELSE
									amountcash = g_sMPTunables.iexec_salvage_cp_increment_cap   * g_sMPTunables.iexec_salvage_cp_increment   
								ENDIF							
								IF USE_SERVER_TRANSACTIONS()
									PRINTLN("[MAGNATE_GANG_BOSS] [GB$] TICKER_EVENT_SALVAGE_CHECKPOINT_COLLECTED - TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_SALVAGE_CHECKPOINT_COLLECTION, ", 100, ", iScriptTransactionIndex, FALSE, TRUE)")
									INT iScriptTransactionIndex = 0
									TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_SALVAGE_CHECKPOINT_COLLECTION, amountcash, iScriptTransactionIndex, FALSE, TRUE) //Add new service
								ELSE
									PRINTLN("[MAGNATE_GANG_BOSS] [GB$] TICKER_EVENT_SALVAGE_CHECKPOINT_COLLECTED - NETWORK_EARN_FROM_AMBIENT_JOB(", 100, ",\"AM_CP_COLLECTION\",amData)")
									NETWORK_EARN_FROM_AMBIENT_JOB(amountcash,"AM_CP_COLLECTION",amData)
								ENDIF
								
								RETURN TRUE
						ENDIF
					ENDIF
				//ENDIF
			ENDREPEAT
		ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_REBREATHER_BEEN_COLLECTED_HOST()

	IF g_salvage_rebreather_has_been_collected = TRUE
	AND g_salvage_rebreather_has_been_collected_number != -1

			PRINTLN("[SALVAGE] HAS_REBREATHER_BEEN_COLLECTED_HOST Creating a replacement rebreather number ",g_salvage_rebreather_has_been_collected_number)

					//CREATE_REPLACEMENT_REBREATHER_PICKUP( serverBD.MapArea, g_salvage_rebreather_has_been_collected_number) 
			g_salvage_rebreather_has_been_collected = FALSE	
			temprebreathernumber[g_salvage_rebreather_has_been_collected_number] = g_salvage_rebreather_has_been_collected_number
			g_salvage_rebreather_has_been_collected_number = -1
			RESET_NET_TIMER(rebreathertimer)
			
			RETURN TRUE
		
		//ENDIF
		

	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_REBREATHER_BEEN_COLLECTED_CLIENT()
	INT i
	VECTOR tempvector
	REPEAT MAX_NUM_REBREATHERS i
		//CREATE_REBREATHER_CRATE(serverBD.objrebreather[i], GET_REBREATHER_COORDINATES( serverBD.MapArea, i))
	//	IF g_salvage_refresh_rebreather_blips = FALSE
			IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.network_index_rebreather[i])
				IF g_salvage_player_has_collected_rebreather = FALSE
					IF ((GET_DISTANCE_BETWEEN_COORDS(GET_REBREATHER_COORDINATES( serverBD.MapArea, i), GET_ENTITY_COORDS(GET_PLAYER_PED(PLAYER_ID()), FALSE)) < 1.0) )
					AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())	
						SEND_REBREATHER_COLLECTION_TICKER_EVENT(i)
						PRINTLN("[SALVAGE] HAS_REBREATHER_BEEN_COLLECTED_CLIENT 1 counter ", i)
						IF DOES_BLIP_EXIST( bliprebreather[i])
							REMOVE_BLIP( bliprebreather[i])
						ENDIF	
						
					//	IF HAS_SOUND_FINISHED(-1)
						IF NOT IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()
							PLAY_SOUND_FROM_COORD(-1, "Boss_Message_Orange", GET_CHECKPOINT_LOCATION(i),"GTAO_Boss_Goons_FM_Soundset", FALSE)
							//RESET_NET_TIMER(rebreathersoundtimer)
						ENDIF
						//ENDIF						
						//INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_BREATHING_APPAR_BOUGHT, 1)
						//SET_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_DO_GEAR_BERD_CHECK)
						//SET_PLAYER_UNDERWATER_BREATH_PERCENT_REMAINING(PLAYER_ID(), 100.0)
						//IF NOT IS_MP_HEIST_GEAR_EQUIPPED(PLAYER_PED_ID(), HEIST_GEAR_REBREATHER)
						//	SET_MP_HEIST_GEAR(PLAYER_PED_ID(), HEIST_GEAR_REBREATHER)
						//	PRINTLN("[SALVAGE] PROCESS_PICKUP HAS_REBREATHER_BEEN_COLLECTED_CLIENT GIVING REBREATHER ")
						//ENDIF
						//DELETE_NET_ID(dserverBD.network_index_rebreather[i])
						RESET_NET_TIMER(rebreathertimer)
						g_salvage_player_has_collected_rebreather = FALSE
						//Delete_MP_Objective_Text()
						inumberofrebreatherscollected++
						PRINTLN(" [SALVAGE] HAS_REBREATHER_BEEN_COLLECTED_CLIENT inumberofrebreatherscollected = ",inumberofrebreatherscollected )
						RETURN TRUE	
					ENDIF
				ENDIF
				tempvector = GET_REBREATHER_COORDINATES( serverBD.MapArea, i)
				DRAW_CHECKPOINT_LIGHTS(tempvector)
			ELSE
				IF g_salvage_player_has_collected_rebreather = TRUE
				AND ((GET_DISTANCE_BETWEEN_COORDS(GET_REBREATHER_COORDINATES( serverBD.MapArea, i), GET_ENTITY_COORDS(GET_PLAYER_PED(PLAYER_ID()), FALSE)) < 2.0) )
					SEND_REBREATHER_COLLECTION_TICKER_EVENT(i)
					//PRINTLN("[SALVAGE] HAS_REBREATHER_BEEN_COLLECTED_CLIENT 2 counter", i)
					
					IF DOES_BLIP_EXIST( bliprebreather[i])
						REMOVE_BLIP( bliprebreather[i])
					ENDIF
					/*IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
						IF DOES_ENTITY_EXIST(serverBD.objrebreather[i])
						DELETE_OBJECT(serverBD.objrebreather[i])
						ENDIF
						
					ENDIF*/
					RESET_NET_TIMER(rebreathertimer)
					g_salvage_player_has_collected_rebreather = FALSE
					//Delete_MP_Objective_Text()
					PRINTLN(" SALVAGE] HAS_REBREATHER_BEEN_COLLECTED_CLIENT inumberofrebreatherscollected = ",inumberofrebreatherscollected )
					inumberofrebreatherscollected++
					RETURN TRUE	
				ENDIF
			ENDIF
		//ENDIF
	ENDREPEAT	
	

	RETURN FALSE
ENDFUNC



FUNC INT NUM_CHECKPOINTS_REMAINING()
	
	RETURN serverBD.inumberofcheckpointsRemaining
ENDFUNC

PROC DRAW_BOTTOM_RIGHT_HUD()
	
	IF NOT MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI
		EXIT
	ENDIF
	
	IF GB_SHOULD_RUN_SPEC_CAM()
		EXIT
	ENDIF
	
	HUD_COLOURS timerColour = HUD_COLOUR_WHITE
	
	INT iTimeRemaining 
	BOOL bbForceRebuildNames = TRUE
	iTimeRemaining = GET_CHALLENGE_EXPIRE_TIME()-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.Starttimer)
	iTimeRemaining = IMAX(iTimeRemaining, 0)
	
	
	IF iTimeRemaining < 30000
		timerColour = HUD_COLOUR_RED
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF IS_DEBUG_KEY_PRESSED(KEY_F11, KEYBOARD_MODIFIER_NONE, "SALVAGE Output")
			PRINTLN("[SALVAGE] DRAW_BOTTOM_RIGHT_HUD  serverBD.sSortedPackagesCollected[0].playerID  = ", GET_PLAYER_NAME(serverBD.sSortedPackagesCollected[0].playerID))
			PRINTLN("[SALVAGE] DRAW_BOTTOM_RIGHT_HUD serverBD.sSortedPackagesCollected[1].playerID = ", GET_PLAYER_NAME(serverBD.sSortedPackagesCollected[1].playerID))
			PRINTLN("[SALVAGE] DRAW_BOTTOM_RIGHT_HUD serverBD.sSortedPackagesCollected[2].playerID = ", GET_PLAYER_NAME(serverBD.sSortedPackagesCollected[2].playerID))
			PRINTLN("[SALVAGE] DRAW_BOTTOM_RIGHT_HUD serverBD.sSortedPackagesCollected[3].playerID = ", GET_PLAYER_NAME(serverBD.sSortedPackagesCollected[3].playerID))


		ENDIF
	#ENDIF
	PLAYER_INDEX player1 
	PLAYER_INDEX player2
	PLAYER_INDEX player3
	PLAYER_INDEX player4
	INT score1
	INT score2
	INT score3
	INT score4
	IF serverBD.sSortedPackagesCollected[0].playerID != INVALID_PLAYER_INDEX()
		IF IS_NET_PLAYER_OK(serverBD.sSortedPackagesCollected[0].playerID, FALSE)
			player1 = serverBD.sSortedPackagesCollected[0].playerID
			score1 = serverBD.sSortedPackagesCollected[0].iScore
		ENDIF
	ENDIF
	
	IF serverBD.sSortedPackagesCollected[1].playerID != INVALID_PLAYER_INDEX()
		IF IS_NET_PLAYER_OK(serverBD.sSortedPackagesCollected[1].playerID, FALSE)
			player2 = serverBD.sSortedPackagesCollected[1].playerID
			score2 = serverBD.sSortedPackagesCollected[1].iScore
		ENDIF	
	ENDIF
	IF serverBD.sSortedPackagesCollected[2].playerID != INVALID_PLAYER_INDEX()
		IF IS_NET_PLAYER_OK(serverBD.sSortedPackagesCollected[2].playerID, FALSE)
			player3 = serverBD.sSortedPackagesCollected[2].playerID
			score3 = serverBD.sSortedPackagesCollected[2].iScore
		ENDIF
	ENDIF
	
	IF serverBD.sSortedPackagesCollected[3].playerID  != INVALID_PLAYER_INDEX()
		IF IS_NET_PLAYER_OK(serverBD.sSortedPackagesCollected[3].playerID, FALSE)
			player4 = serverBD.sSortedPackagesCollected[3].playerID
			score4 = serverBD.sSortedPackagesCollected[3].iScore
		ENDIF	
	ENDIF
	
	INT players_score
	IF NOT GB_SHOULD_RUN_SPEC_CAM()
		players_score = serverBD.iclientscore[PARTICIPANT_ID_TO_INT()]
	ENDIF
	
	
	
	BOTTOM_RIGHT_UI_1STINT_2NDINT_3RDINT_4THINT_LOCALINT_TIMER(player1,
																player2, 
																player3, 
																player4, 
																score1, 
																score2,
																score3,
																score4,
																players_score, 
																iTimeRemaining,			
																bbForceRebuildNames, 
																timerColour,
																"GB_CHAL_END",
																 FMEVENT_SCORETITLE_YOUR_SCORE)
	// CHECKPOINTS															 
	DRAW_GENERIC_SCORE(NUM_CHECKPOINTS_REMAINING(), "CHECKPOINT", DEFAULT, DEFAULT, HUDORDER_SECONDBOTTOM)


ENDPROC

FUNC STRING GET_MISSION_STATE_STRING(INT iState)

	SWITCH(iState)
		CASE GAME_STATE_INIT 				RETURN "GAME_STATE_INIT"
		CASE GAME_STATE_RUNNING 			RETURN "GAME_STATE_RUNNING"
		CASE GAME_STATE_TERMINATE_DELAY 	RETURN "GAME_STATE_TERMINATE_DELAY"
		CASE GAME_STATE_END 				RETURN "GAME_STATE_END"
	ENDSWITCH

	RETURN "UNKNOWN STATE!"

ENDFUNC

//Helper function to set a clients game/mission state
PROC SET_CLIENT_MISSION_STATE(INT iPart,INT iState)
	playerBD[iPart].iClientGameState = iState
	CPRINTLN(DEBUG_NET_AMBIENT, DEBUG_NET_AMBIENT, "=== MAGNATE_GANG_BOSS SALVAGE SET_CLIENT_MISSION_STATE = ",GET_MISSION_STATE_STRING(iState))
	
ENDPROC

//Helper function to set the servers game/mission state
PROC SET_SERVER_MISSION_STATE(INT iState)
	serverBD.iServerGameState = iState
	CPRINTLN(DEBUG_NET_AMBIENT, DEBUG_NET_AMBIENT, "=== MAGNATE_GANG_BOSS SALVAGE SET_SERVER_MISSION_STATE = ",GET_MISSION_STATE_STRING(iState))
ENDPROC
// Widgets & Debug.
#IF IS_DEBUG_BUILD
	BOOL bHostEndMissionNow
	BOOL bWarpToPackage
	INT soundtype = 2
	BOOL bkillgame
	BOOL bkillsound
	
#ENDIF


#IF IS_DEBUG_BUILD
PROC CREATE_WIDGETS()
	INT iPlayer
	TEXT_LABEL_15 tl63

	START_WIDGET_GROUP("SALVAGE")  
		ADD_WIDGET_BOOL("Warp to Package", bWarpToPackage)

		ADD_WIDGET_BOOL("End SALVAGE", bkillgame)	

		ADD_WIDGET_BOOL("serverbd.has_a_checkpoint_been_passed", serverbd.has_a_checkpoint_been_passed)	
		
		 ADD_WIDGET_INT_SLIDER(" g_sMPTunables.iexec_salvage_time_limit",  g_sMPTunables.iexec_salvage_time_limit ,0, 1000000,1)
		// Gameplay debug, sever only.
		START_WIDGET_GROUP("Server Only Gameplay") 
			ADD_WIDGET_BOOL("End mission", bHostEndMissionNow)			
		STOP_WIDGET_GROUP()		
		// Rumble Vibrations
		START_WIDGET_GROUP("RUMBLE Freq") 
			//ADD_WIDGET_INT_SLIDER("ROUND(g_sMPTunables.igb_finders_keepers_rumble_radius)", g_sMPTunables.igb_finders_keepers_rumble_radius,0.0, 200.0,1.0)
			ADD_WIDGET_INT_SLIDER("ciHTB_RUMBLE_DURATION", ciHTB_RUMBLE_DURATION,0, 1000,1)
			ADD_WIDGET_INT_SLIDER("ciHTB_RUMBLE_DURATION_MULTIPLIER", ciHTB_RUMBLE_DURATION_MULTIPLIER,0, 10000,1)
			
			ADD_WIDGET_VECTOR_SLIDER("MPGlobalsAmbience.vQuickGPS", MPGlobalsAmbience.vQuickGPS, -3000.0, 30000.0, 0.1)
		STOP_WIDGET_GROUP()	
		// Rumble Vibrations
		START_WIDGET_GROUP("PACKAGE SOUNDS") 
			//ADD_WIDGET_INT_SLIDER("Sound Freq", soundexpiretimer,0, 10000,5)
			ADD_WIDGET_INT_SLIDER("Sound String", soundtype,0, 4,1)
			ADD_WIDGET_BOOL("Kill Sound", bkillsound)	
		
		STOP_WIDGET_GROUP()	
		
		
		// Data about the server
		START_WIDGET_GROUP("Server BD") 
			ADD_WIDGET_BOOL("bcreate_rebreathers",serverBD.bcreate_rebreathers)	
			ADD_WIDGET_INT_READ_ONLY("serverBD.inumberofrebreathers",serverBD.inumberofrebreathers)
			ADD_WIDGET_INT_SLIDER("ipackagecounter", serverBD.ipackagecounter,-1, HIGHEST_INT,1)
			ADD_WIDGET_INT_SLIDER("S. Game state", serverBD.iServerGameState,-1, HIGHEST_INT,1)
			ADD_WIDGET_INT_SLIDER("ADD_WIDGET_INT (number of rebreathers)  ",serverBD.inumberofrebreathers,-1, HIGHEST_INT,1)
			ADD_WIDGET_INT_READ_ONLY("inumberofcheckpointsRemaining",serverBD.inumberofcheckpointsRemaining)
			
			ADD_WIDGET_BOOL("bcreate_bjetskis",serverBD.bcreate_bjetskis)	
		STOP_WIDGET_GROUP()	
		
		START_WIDGET_GROUP("FLARE ATTACHMENT TWEAK ")
			ADD_WIDGET_BOOL("update flare flare_attach ",flare_attach)	
			ADD_WIDGET_FLOAT_SLIDER("flare_offset_x", flare_offset_x,-360.0, 360.0,0.01)
			ADD_WIDGET_FLOAT_SLIDER("flare_offset_y", flare_offset_y,-360.0, 360.0,0.01)
			ADD_WIDGET_FLOAT_SLIDER("flare_offset_z", flare_offset_z,-360.0, 360.0,0.01)
		
			ADD_WIDGET_FLOAT_SLIDER("flare_ROT_x", flare_ROT_x,-360.0, 360.0,0.01)
			ADD_WIDGET_FLOAT_SLIDER("flare_ROT_y", flare_ROT_y,-360.0, 360.0,0.01)
			ADD_WIDGET_FLOAT_SLIDER("flare_ROT_z", flare_ROT_z,-360.0, 360.0,0.01)
			
		
		STOP_WIDGET_GROUP()	
		
		START_WIDGET_GROUP("temprebreathernumber")  
			ADD_WIDGET_INT_READ_ONLY("temprebreathernumber[0]",temprebreathernumber[0])
			ADD_WIDGET_INT_READ_ONLY("temprebreathernumber[1]",temprebreathernumber[1])
			ADD_WIDGET_INT_READ_ONLY("temprebreathernumber[2]",temprebreathernumber[2])
			ADD_WIDGET_INT_READ_ONLY("temprebreathernumber[3]",temprebreathernumber[3])
			ADD_WIDGET_INT_READ_ONLY("temprebreathernumber[4]",temprebreathernumber[4])
			ADD_WIDGET_INT_READ_ONLY("temprebreathernumber[5]",temprebreathernumber[5])
			ADD_WIDGET_INT_READ_ONLY("temprebreathernumber[6]",temprebreathernumber[6])
			ADD_WIDGET_INT_READ_ONLY("temprebreathernumber[7]",temprebreathernumber[7])
			ADD_WIDGET_INT_READ_ONLY("temprebreathernumber[8]",temprebreathernumber[8])
			ADD_WIDGET_INT_READ_ONLY("temprebreathernumber[9]",temprebreathernumber[9])
		STOP_WIDGET_GROUP()	
		// Data about the clients. * = You.
		START_WIDGET_GROUP("Client BD")  				
			REPEAT NUM_NETWORK_PLAYERS iPlayer
				tl63 = "Player "
				tl63 += iPlayer
				IF iPlayer = PARTICIPANT_ID_TO_INT()
					tl63 += "*"
				ENDIF
				START_WIDGET_GROUP(tl63)
					ADD_WIDGET_INT_SLIDER("Game state", playerBD[iPlayer].iClientGameState,-1, HIGHEST_INT,1)
					ADD_WIDGET_INT_SLIDER("PLAYERS SCORE",serverBD.iclientscore[PARTICIPANT_ID_TO_INT()],0, 10,1)
					ADD_WIDGET_BOOL("IS PLAYER IN SALVAGE AREA playerBD[i].this_player_in_salvage_area", playerBD[iPlayer].this_player_in_salvage_area)
					ADD_WIDGET_BOOL("this_player_has_picked_up_package ", playerBD[iPlayer].this_player_has_picked_up_package)
					
					
				STOP_WIDGET_GROUP()
			ENDREPEAT				
		STOP_WIDGET_GROUP()	
		
	STOP_WIDGET_GROUP()
ENDPROC		

PROC UPDATE_WIDGETS()
	IF bWarpToPackage = TRUE
	OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
		IF NET_WARP_TO_COORD(GET_CHECKPOINT_LOCATION(serverBD.ipackagecounter), 0, TRUE, FALSE)
			NET_PRINT_TIME() NET_PRINT("     ---------->     SALVAGE -  UPDATE_WIDGETS - bWarpToPackage DONE    <----------     ") NET_NL()
			bWarpToPackage = FALSE
		ELSE
			NET_PRINT_TIME() NET_PRINT("     ---------->     SALVAGE -  UPDATE_WIDGETS - bWarpToPackage IN PROGRESS    <----------     ") NET_NL()
		ENDIF
		
	ENDIF		
	
	
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
	OR GB_SHOULD_KILL_ACTIVE_BOSS_MISSION()
		SET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT(),GAME_STATE_END)
		TickerEventData.TickerEvent = TICKER_EVENT_SALVAGE_EXPIRY_TIME
		TickerEventData.playerID = serverBD.sSortedPackagesCollected[0].playerID // finders_keepers_the_winner
		
		IF TickerEventData.playerID != NULL
		AND IS_NET_PLAYER_OK(TickerEventData.playerID)
			PRINTLN(" WIDGET SALVAGE TICKER_EVENT_FINDERSKEEPERS_EXPIRY_TIME TickerEventData.playerID =", GET_PLAYER_NAME(TickerEventData.playerID))
		ENDIF
		
		IF IS_GAME_A_DRAW()
			TickerEventData.playerID2 =serverBD.sSortedPackagesCollected[1].playerID //finders_keepers_the_winner2
			TickerEventData.dataInt = 1 // game is a draw
			IF TickerEventData.playerID2 != NULL
			AND IS_NET_PLAYER_OK(TickerEventData.playerID2)
				PRINTLN("WIDGETSALVAGE TICKER_EVENT_FINDERSKEEPERS_EXPIRY_TIME TickerEventData.playerID2 =", GET_PLAYER_NAME(TickerEventData.playerID2))
			ENDIF
		ELSE
			TickerEventData.playerID2 = NULL
		ENDIF
		
		//If nobody scored
		IF serverBD.sSortedPackagesCollected[0].iScore < 1
		AND serverBD.sSortedPackagesCollected[1].iScore < 1
			TickerEventData.dataInt = 3 // nobody scored
			PRINTLN("WIDGETSALVAGE TICKER_EVENT_FINDERSKEEPERS_EXPIRY_TIME Nobody scored")
		ENDIF
		#IF IS_DEBUG_BUILD
			TickerEventData.dataInt2 = 666
		#ENDIF
		bkillgame = TRUE
		BROADCAST_TICKER_EVENT(TickerEventData, ALL_PLAYERS_ON_SCRIPT())
		NET_PRINT_TIME() NET_PRINT("    WIDGET  ---------->     SALVAGE -  MISSION END - TICKER_EVENT_FINDERSKEEPERS_EXPIRY_TIME     <----------     ") NET_NL()
	ENDIF
ENDPROC

#ENDIF


FUNC BOOL INIT_CLIENT()
	REQUEST_LOAD_MODEL(SEASHARK)
	REQUEST_LOAD_MODEL(hei_prop_carrier_crate_01a)
	
	INT i
	REPEAT MAX_NUM_REBREATHERS i
		temprebreathernumber[i] = -1
	ENDREPEAT
	
	IF MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI	
		SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_START_OF_JOB, "GB_BIGM_SAL_T", "GB_SAL_HELP5")		
		GB_SET_GANG_BOSS_HELP_BACKGROUND()
	ENDIF	
	PRINT_HELP("GB_SAL_SETUP")

	GB_SET_COMMON_TELEMETRY_DATA_ON_START()
	//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_GB_SALVAGE
	PRINTLN("MAGNATE_GANG_BOSS [SALVAGE] - INIT_CLIENT ")
	//request_script_audio_bank("EPSILONISM_04_SOUNDSET")
	//REQUEST_PTFX_ASSET()
	//REQUEST_NAMED_PTFX_ASSET("scr_indep_fireworks")

	gsalvagegiveplayerapoint = FALSE
	gRESET_PLAYER_BD_PICKUP_FLAGS = FALSE
	g_salvage_refresh_rebreather_blips = FALSE  //refresh blips for everyone 
	g_salvage_rebreather_has_been_collected = FALSE

	//GIVE_PLAYER_REBREATHER()

	GB_COMMON_BOSS_MISSION_SETUP(FMMC_TYPE_GB_SALVAGE)
	SET_AUDIO_FLAG("DisableFlightMusic", TRUE) 
	SET_AUDIO_FLAG("WantedMusicDisabled", TRUE) 
	inum_of_rebreathers_at_start = GET_MP_INT_CHARACTER_STAT(MP_STAT_BREATHING_APPAR_BOUGHT)
	RETURN TRUE
ENDFUNC

FUNC MAP_AREA_ZONE SHIFT_AREA_ALONG_ONE(MAP_AREA_ZONE players_area )
	SWITCH players_area
		
		CASE CONTAINERSHIP RETURN PLANENORTH BREAK
		CASE PLANENORTH	RETURN PADDLESTEAMER BREAK
		CASE PADDLESTEAMER	RETURN CARGOSHIP BREAK
		CASE CARGOSHIP	RETURN PLANESOUTH BREAK
		CASE PLANESOUTH	RETURN MILITARYDEBRIS BREAK
		CASE MILITARYDEBRIS	RETURN SUBMARINE BREAK
		CASE SUBMARINE	RETURN MILITARYPLANE BREAK
		CASE MILITARYPLANE	RETURN BATHYSCAPE BREAK
		CASE BATHYSCAPE	RETURN TUGBOAT BREAK
		CASE TUGBOAT	RETURN CONTAINERSHIP BREAK

		
	ENDSWITCH
		
	RETURN GENERAL 	

ENDFUNC

FUNC BOOL INIT_SERVER()
	serverBD.piGangBoss = GB_GET_LOCAL_PLAYER_MISSION_HOST()
	RESET_NET_TIMER(serverBD.Starttimer)
	PRINTLN("[SALVAGE] MAGNATE_GANG_BOSS [GB SALVAGE] - INIT_SERVER")
	
	PLAYSTATS_CREATE_MATCH_HISTORY_ID_2(serverBD.iMatchId1, serverBD.iMatchId2)
	MAP_AREA_ZONE players_area
	//2710605 Add support for variations to M menu for all Executive missions - Please call this in your mission scripts

	INT i = 0
	IF GB_GET_PLAYER_GANG_BOSS_MISSION_VARIATION() >-1
		PRINTLN("[SALVAGE] GET_PLAYERS_CURRENT_AREA  = ",GB_GET_PLAYER_GANG_BOSS_MISSION_VARIATION())
		IF GB_GET_PLAYER_GANG_BOSS_MISSION_VARIATION() = 0 
			players_area  = CONTAINERSHIP
		ELIF  GB_GET_PLAYER_GANG_BOSS_MISSION_VARIATION() =1
			players_area  = PLANENORTH
		ELIF  GB_GET_PLAYER_GANG_BOSS_MISSION_VARIATION() =2
			players_area  = PADDLESTEAMER
		ELIF  GB_GET_PLAYER_GANG_BOSS_MISSION_VARIATION() =3
			players_area  = CARGOSHIP
		ELIF  GB_GET_PLAYER_GANG_BOSS_MISSION_VARIATION() =4
			players_area  = PLANESOUTH
		ELIF  GB_GET_PLAYER_GANG_BOSS_MISSION_VARIATION() =5
			players_area  = MILITARYDEBRIS
		ELIF  GB_GET_PLAYER_GANG_BOSS_MISSION_VARIATION() =6
			players_area  = SUBMARINE
		ELIF  GB_GET_PLAYER_GANG_BOSS_MISSION_VARIATION() =7
			players_area  = MILITARYPLANE
		ELIF  GB_GET_PLAYER_GANG_BOSS_MISSION_VARIATION() =8
			players_area  = BATHYSCAPE
		ELIF  GB_GET_PLAYER_GANG_BOSS_MISSION_VARIATION() =9
			players_area  = TUGBOAT
		ELSE
			
			
			players_area = GET_PLAYERS_CURRENT_AREA()
			WHILE (NOT IS_SALVAGE_VARIATION_DIFFERENT_FROM_LAST_USED(player_ID(), ENUM_TO_INT(players_area) )
			OR IS_SALVAGE_VARIATION_SAME_FROM_ANOTHER_ACTIVE_INSTANCE( ENUM_TO_INT(players_area) ))	
			AND i < 11
				PRINTLN("[SALVAGE] IS_SALVAGE_VARIATION_DIFFERENT_FROM_LAST_USED = Same as last. 1 ", ENUM_TO_INT(players_area))
				players_area = SHIFT_AREA_ALONG_ONE(players_area)
				PRINTLN("[SALVAGE] IS_SALVAGE_VARIATION_DIFFERENT_FROM_LAST_USED New one 1 =  ", ENUM_TO_INT(players_area))
				i++

			ENDWHILE
			
			IF i >=11
				players_area = GET_PLAYERS_CURRENT_AREA()
				PRINTLN("[SALVAGE]  Exit loop with no results. just use general players_area =   ", ENUM_TO_INT(players_area))
			ENDIF
				
		ENDIF
	ELSE
	
		players_area = GET_PLAYERS_CURRENT_AREA()
		WHILE (NOT IS_SALVAGE_VARIATION_DIFFERENT_FROM_LAST_USED(player_ID(), ENUM_TO_INT(players_area) )
		OR IS_SALVAGE_VARIATION_SAME_FROM_ANOTHER_ACTIVE_INSTANCE( ENUM_TO_INT(players_area) ))	
		AND i < 11
			PRINTLN("[SALVAGE] IS_SALVAGE_VARIATION_DIFFERENT_FROM_LAST_USED = Same as last. ", ENUM_TO_INT(players_area))
			players_area = SHIFT_AREA_ALONG_ONE(players_area)
			PRINTLN("[SALVAGE] IS_SALVAGE_VARIATION_DIFFERENT_FROM_LAST_USED New one =  ", ENUM_TO_INT(players_area))
			i++

		ENDWHILE
		
		IF i >=11
			players_area = GET_PLAYERS_CURRENT_AREA()
			PRINTLN("[SALVAGE]  Exit loop with no results. just use general players_area =   ", ENUM_TO_INT(players_area))
		ENDIF

		
	ENDIF
	
	

	IF players_area = GENERAL
		players_area = INT_TO_ENUM(MAP_AREA_ZONE,GET_RANDOM_INT_IN_RANGE(0,10))
		PRINTLN("[SALVAGE] GET_PLAYERS_CURRENT_AREA = General. Need to assign a random area. AREA = ", ENUM_TO_INT(players_area))
	ENDIF
	

	
	serverBD.vareablipcoordinates =  GET_AREA_BLIP_COORDINATES(players_area)
	INITIALISE_AMBIENT_ACTIVITY_LEADERBOARD(serverBD.sSortedPackagesCollected, AAL_SORT_DESCENDING)
	
	SET_LAST_SALVAGE_VARIATION(ENUM_TO_INT(players_area))
	
	
	
	
	GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.icurrentsalvagevariationbeingplayed = ENUM_TO_INT(players_area)
	PRINTLN("[SALVAGE] GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.icurrentsalvagevariationbeingplayed = ", ENUM_TO_INT(players_area))
	
	SORT_SALVAGE_PLAYER_SCORES(player_id(),0)
	GB_COMMON_BOSS_MISSION_SETUP(FMMC_TYPE_GB_SALVAGE)
	i = 0

	

	REPEAT NUM_CHECKPOINTS i

		IF i > 0
			PRINTLN("[SALVAGE] GET_DISTANCE_BETWEEN_COORDS(serverBD.vPackageLocation[i-1], GET_CHECKPOINT_LOCATION(i)) = ", GET_DISTANCE_BETWEEN_COORDS(GET_CHECKPOINT_LOCATION(i-1), GET_CHECKPOINT_LOCATION(i) ))
		ENDIF
		// make sure next package is 50 meters away
		/*
		IF i > 0
			WHILE GET_DISTANCE_BETWEEN_COORDS(serverBD.vPackageLocation[i-1], GET_CHECKPOINT_LOCATION(i)) < g_sMPTunables.fgbfinderskeepers_package_distance_creation
				PRINTLN("SALVAGE location is 50 meters too close to last package ", i)
				
				GET_CHECKPOINT_LOCATION(i)= GET_CHECKPOINT_LOCATION(players_area)
				PRINTLN("SALVAGE GET_DISTANCE_BETWEEN_COORDS(serverBD.vPackageLocation[i-1], GET_CHECKPOINT_LOCATION(i)) = ", GET_DISTANCE_BETWEEN_COORDS(serverBD.vPackageLocation[i-1], GET_CHECKPOINT_LOCATION(i)) )
				//PRINTLN("SALVAGE NEW GET_DISTANCE_BETWEEN_COORDS(serverBD.vPackageLocation[i-1], GET_CHECKPOINT_LOCATION(i)) = ", GET_DISTANCE_BETWEEN_COORDS(serverBD.vPackageLocation[i-1], GET_CHECKPOINT_LOCATION(i)))
			ENDWHILE
		ENDIF
		*/
	//	PRINTLN("SALVAGE LOCATIONS FOR PACKAGES serverBD.vPackageLocation[", i,"] = ",GET_CHECKPOINT_LOCATION(i) )
	ENDREPEAT
	serverBD.MapArea = players_area
	
	

	REQUEST_MODEL(SEASHARK)

	PRINTLN(" [salvage] INIT_SERVER() serverBD.inumberofrebreathers =  ", serverBD.inumberofrebreathers)
	RETURN TRUE

ENDFUNC

FUNC BOOL PROCESS_PRE_GAME(MP_MISSION_DATA missionScriptArgs)
	gfinderskeeperslocalplayerhascollectedpickup = FALSE
	CPRINTLN(DEBUG_NET_AMBIENT, DEBUG_NET_AMBIENT, "[SALVAGE] === MAGNATE_GANG_BOSS SALVAGE PROCESS_PRE_GAME")
	
	// This marks the script as a net script, and handles any instancing setup. 
	SETUP_MP_MISSION_FOR_NETWORK(GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(missionScriptArgs.mdID.idMission), missionScriptArgs)
	
	GB_COMMON_BOSS_MISSION_PREGAME() 
	
	//Reserve Entities
	RESERVE_NETWORK_MISSION_OBJECTS(GB_GET_BOSS_MISSION_NUM_OBJ_REQUIRED(FMMC_TYPE_GB_SALVAGE))
	RESERVE_NETWORK_MISSION_VEHICLES(GB_GET_BOSS_MISSION_NUM_VEH_REQUIRED(FMMC_TYPE_GB_SALVAGE))
	
	// This makes sure the net script is active, waits until it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD,SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD,SIZE_OF(playerBD))
	
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	initial_player_boss = GB_GET_THIS_PLAYER_GANG_BOSS(PLAYER_ID())

	#IF IS_DEBUG_BUILD
	CREATE_WIDGETS()
	#ENDIF
	
	RETURN TRUE
	
ENDFUNC







PROC MAINTAIN_PACKAGE_CLIENT()

	INT i
	
	IF serverBD.iServerGameState!= GAME_STATE_END
		IF serverBD.brandomcoordinatesgenerated = TRUE
		AND bcreatepackageareablip[serverBD.ipackagecounter] = FALSE
			// delete old ones first
			
			DELETE_PACKAGE()
			REMOVE_PACKAGE_AREA_BLIP()
			REMOVE_PACKAGE_AREA_BLIP_LONG_RANG_BLIP()
			IF serverBD.iServerGameState = GAME_STATE_RUNNING
				ADD_PACKAGE_AREA_BLIP()
			ENDIF
			
			IF NOT Is_There_Any_Current_Objective_Text_From_This_Script()
				//Print_Objective_Text("GB_SAL_HELP2") // enter the salvage area
			ENDIF
			
		ENDIF
		IF NOT GB_SHOULD_RUN_SPEC_CAM()
			IF serverBD.ipackagecounter != NUM_CHECKPOINTS-1
					IF IS_THIS_PLAYER_NEAR_CHECKPOINT_AREA(PLAYER_ID())
						IF NOT Is_This_The_Current_Objective_Text("GB_SAL_HELP1")
							Print_Objective_Text("GB_SAL_HELP1")
						ENDIF
					ELSE
						IF NOT Is_This_The_Current_Objective_Text("GB_SAL_HELP2")
							Print_Objective_Text("GB_SAL_HELP2") // enter the salvage area
						ENDIF
					ENDIF
					
			ELSE
				IF  NOT Is_There_Any_Current_Objective_Text_From_This_Script()
					Print_Objective_Text("GB_SAL_HELP4")   // find the final package
				ENDIF
			
			ENDIF
		ENDIF
		
		IF (bhas_the_package_been_created = FALSE AND IS_THIS_PLAYER_NEAR_CHECKPOINT_AREA(PLAYER_ID()))
		OR 	bhas_the_package_been_created = TRUE
			IF g_give_player_a_rebreather = TRUE
			AND  IS_PLAYER_IN_BOAT_OR_IN_WATER()
				GIVE_INITIAL_PLAYER_REBREATHER()		
				PRINTLN("GIVE_INITIAL_PLAYER_REBREATHER() 2")
			ENDIF
		ENDIF
		IF bmarkalljetskistobedeleted
			DELETE_ALL_JETSKI_BLIPS()
			bmarkalljetskistobedeleted = FALSE
		ENDIF
		
		//If player is close to area, create package
		IF bhas_the_package_been_created
		OR GB_SHOULD_RUN_SPEC_CAM()
			REPEAT serverBD.inumberofcheckpoints i
				DRAW_CHECKPOINTS(localCheckpointData[i], GET_CHECKPOINT_LOCATION(i),i)
			ENDREPEAT	                   
		ENDIF	
				
		IF NOT bhas_the_package_been_created
			IF IS_THIS_PLAYER_NEAR_CHECKPOINT_AREA(PLAYER_ID())
			
			//	REMOVE_PACKAGE_AREA_BLIP()
				
				//GB_SET_GANG_BOSS_HELP_BACKGROUND()
				CREATE_SALVAGE_CHECKPOINT()
				REMOVE_PACKAGE_AREA_BLIP_LONG_RANG_BLIP()

				
				bmarkalljetskistobedeleted = TRUE
				

				IF NETWORK_DOES_NETWORK_ID_EXIST(salvagelocalplayersjetski)
					CLEANUP_NET_ID(salvagelocalplayersjetski)
				ENDIF
	
				USE_CUSTOM_SPAWN_POINTS(TRUE)
				ADD_CUSTOM_SPAWN_POINT(GETJETSKIRESPAWN_COORDS(), GET_HEADING_BETWEEN_TWO_VECTORS_2D(GETJETSKIRESPAWN_COORDS(),GET_AREA_BLIP_COORDINATES(serverBD.MapArea)))
				SET_PLAYER_RESPAWN_IN_VEHICLE(TRUE, SEASHARK, FALSE)
				SET_PLAYER_WILL_SPAWN_FACING_COORDS( serverBD.vareablipcoordinates, TRUE, FALSE)
				
				bhas_the_package_been_created = TRUE
			ENDIF
			
			IF NOT bhas_the_package_been_created
				IF bdisplayseasharkhelp = FALSE
				AND NOT IS_HELP_MESSAGE_ON_SCREEN()
				AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					IF bhavejetskisbeendeleted = FALSE
					AND bcreatejetskiblips = TRUE
					AND bmarkalljetskistobedeleted = FALSE
						IF HAS_NET_TIMER_EXPIRED(helptimer,5000)
							IF serverBD.bcreate_bjetskis
								bdisplayseasharkhelp =TRUE
								PRINT_HELP("GB_SAL_HELP6")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			
		ELSE	
			
		
			MAINTAIN_PACKAGE_CLIENT_BLIPS_AND_RUMBLE()	
			SEND_REBREATHER_TICKER_EVENT()
		ENDIF
		

		IF bGIVE_PLAYER_REBREATHER = FALSE
			IF IS_THIS_PLAYER_NEAR_CHECKPOINT_AREA(PLAYER_ID())
			AND  IS_PLAYER_IN_BOAT_OR_IN_WATER()
				
				GIVE_INITIAL_PLAYER_REBREATHER()
				PRINTLN("GIVE_PLAYER_REBREATHER() 1")
				IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_KNIFE)
					SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_KNIFE, TRUE)
				ENDIF
				
			ENDIF
		ENDIF

		
		IF bcreatejetskiblips = TRUE
		AND  serverBD.bcreate_bjetskis = TRUE
			REPEAT MAXNUMJETSKIS i
				IF CHECK_TO_DELETE_JETSKI_BLIP(i)
					DELETE_JETSKI_BLIP(i)
					
				ENDIF 
			ENDREPEAT	   
		ENDIF

		
		IF IS_THIS_PLAYER_NEAR_CHECKPOINT_AREA(PLAYER_ID())
		AND IS_PLAYER_IN_BOAT_OR_IN_WATER()
			IF serverBD.bcreate_rebreathers
				IF NOT bcreate_rebreather_blips
				OR g_salvage_refresh_rebreather_blips
					ADD_BLIP_FOR_REBREATHER_PICKUPS()
				ENDIF
			ENDIF
		ENDIF
		
		/*IF NOT bcreate_bjetskis
			IF CAN_REGISTER_MISSION_VEHICLES(GB_GET_BOSS_MISSION_NUM_VEH_REQUIRED(FMMC_TYPE_GB_SALVAGE))
				CREATE_JETSKIS()
			ELSE
				PRINTLN("[SALVAGE] CAN_REGISTER_MISSION_VEHICLES 1 = FALSE  ")
			ENDIF
		ENDIF*/
		IF bcreatejetskiblips = FALSE
			IF serverBD.bcreate_bjetskis = TRUE
				CREATE_BLIPS_FOR_JETSKIS()
				bcreatejetskiblips = TRUE
			ENDIF
		ENDIF
		
		
		IF IS_THIS_PLAYER_NEAR_CHECKPOINT_AREA(PLAYER_ID())	
			IF NOT IS_VECTOR_ZERO(serverBD.vareablipcoordinates )
				GB_MAINTAIN_BOSS_WORK_DISTANT_CHECKS(FMMC_TYPE_GB_SALVAGE, serverBD.vareablipcoordinates, bSet, 500, 750)
			ENDIF	
		ENDIF
		
		IF DOES_BLIP_EXIST(PackageBlipArea)
			IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(PLAYER_ID(), GB_GET_THIS_PLAYER_GANG_BOSS(PLAYER_ID()), TRUE)
			OR GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), TRUE)
				PLAYER_INDEX piTempBoss
				IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), TRUE)
					piTempBoss = GB_GET_THIS_PLAYER_GANG_BOSS(PLAYER_ID())
				ELSE
					piTempBoss = serverBD.piGangBoss
				ENDIF
				
				IF (GET_AREA_BLIP_ALPHA(piTempBoss) != iBlipAlphaCached)
					PRINTLN("[SALVAGE] - MAINTAIN_PACKAGE_CLIENT - eGBHTB_INIT - Updating blip alpha from: ",iBlipAlphaCached,", to: ",GET_AREA_BLIP_ALPHA(serverBD.piGangBoss))
					iBlipAlphaCached = GET_AREA_BLIP_ALPHA(piTempBoss)
					SET_BLIP_ALPHA(PackageBlipArea, iBlipAlphaCached)
				ENDIF
			ENDIF

		ENDIF
		IF serverbd.has_a_checkpoint_been_passed = FALSE
			IF	HAS_CHECKPOINT_BEEN_COLLECTED_CLIENT()
				
			ENDIF
		ENDIF
		
		IF  HAS_REBREATHER_BEEN_COLLECTED_CLIENT()
			
		ENDIF
	ELSE
		PRINTLN("[SALVAGE] SERVER HAS ENDED GAME")
		playerBD[PARTICIPANT_ID_TO_INT()].iClientGameState = GAME_STATE_END
	ENDIF
ENDPROC
  
  
PROC MAINTAIN_FINAL_SHARD()
	IF serverBD.sSortedPackagesCollected[0].playerID = PLAYER_ID()
	AND serverBD.sSortedPackagesCollected[0].iScore > serverBD.sSortedPackagesCollected[1].iScore
//		IF MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI
//			SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS, "GB_WORK_OVER", "GB_WORK_OVER")
//		ENDIF
		PRINTLN("[SALVAGE] MAINTAIN_FINAL_SHARD PLAYER WON")
		GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_SALVAGE, TRUE, sRewardsData)
		CLEANUP_BLIPS_AND_ASSETS()
	ELSE
//		IF MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI
//			SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS, "GB_WORK_OVER", "GB_WORK_OVER")
//		ENDIF
		PRINTLN("[SALVAGE] MAINTAIN_FINAL_SHARD PLAYER lost or drew")
		GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_SALVAGE, FALSE, sRewardsData )
		CLEANUP_BLIPS_AND_ASSETS()
	ENDIF
	bHasdisplayedfinalshard = TRUE
ENDPROC

PROC HANDLE_FINDERS_KEEPERS_COUNTDOWN_AUDIO(INT iRemainingTime)
	IF iRemainingTime < GET_FRAME_TIME()
		MPGlobalsAmbience.iBS_AmbientEventAudioTriggers = 0
		CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - HANDLE_FINDERS_KEEPERS_COUNTDOWN_AUDIO has finished")
	ELIF (iRemainingTime < 36000 AND iRemainingTime > 31000)
		IF initialisemusic != 4
			initialisemusic = 4
			TRIGGER_MUSIC_EVENT("APT_PRE_COUNTDOWN_STOP")
			
		ENDIF
	
	ELIF (iRemainingTime < 31000 AND iRemainingTime > 6000)
		IF initialisemusic != 3
			initialisemusic = 3
			TRIGGER_MUSIC_EVENT("APT_COUNTDOWN_30S")
		ENDIF
	ELIF iRemainingTime < 6000
		IF NOT IS_BIT_SET(MPGlobalsAmbience.iBS_AmbientEventAudioTriggers, ciAE_AUDIO_PLAY_COUNTDOWN)
			PLAY_SOUND_FRONTEND(-1, "5s", "MP_MISSION_COUNTDOWN_SOUNDSET", FALSE)
			MPGlobalsAmbience.iBS_AmbientEventAudioTriggers = 0
			SET_BIT(MPGlobalsAmbience.iBS_AmbientEventAudioTriggers, ciAE_AUDIO_PLAY_COUNTDOWN)
			CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - HANDLE_FINDERS_KEEPERS_COUNTDOWN_AUDIO ciAE_AUDIO_PLAY_COUNTDOWN")
		ENDIF
	ENDIF
ENDPROC



PROC MANAGE_UNDERWATER_FLARE()
	IF CAN_REGISTER_MISSION_OBJECTS(GB_GET_BOSS_MISSION_NUM_OBJ_REQUIRED(FMMC_TYPE_GB_SALVAGE))
		IF CAN_ATTACH_FLARE_TO_PLAYER()
			REQUEST_NAMED_PTFX_ASSET( strHeistFX )
		
			IF HAS_NAMED_PTFX_ASSET_LOADED( strHeistFX )
				// Create a flare object on the body
				IF NOT DOES_ENTITY_EXIST( objFlare )
					objFlare = create_object( PROP_FLARE_01B, get_offset_from_entity_in_world_coords( PLAYER_PED_ID(), <<0.0, 0.0, 1.0>> ), true, NETWORK_IS_HOST_OF_THIS_SCRIPT() )
					attach_entity_to_entity( objFlare, PLAYER_PED_ID(), get_ped_bone_index( PLAYER_PED_ID(), BONETAG_SPINE3 ), <<flare_offset_x, flare_offset_y, flare_offset_z>>, <<flare_ROT_x, flare_ROT_y, flare_ROT_z>>)
					
					PLAY_SOUND_FROM_ENTITY(iSoundfromflare,"Flare",objFlare,"DLC_Exec_Salvage_Sounds") //Sound
					//SET_BIT( iLocalBoolCheck10, LBOOL10_HUMANE_FLARE_CREATED )
					//CLEAR_BIT( iLocalBoolCheck12,LBOOL12_OBJ_RES_UNDERWATER_FLARE)
					CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - MANAGE_UNDERWATER_FLARE - Created flare.")
				ELSE
				
						IF DOES_ENTITY_HAVE_DRAWABLE(objFlare)
					// Create PTFX for that
							IF NOT DOES_PARTICLE_FX_LOOPED_EXIST( players_underwater_flare )
							//AND has_flare_fx_been_created = FALSE
							//	has_flare_fx_been_created = TRUE
								USE_PARTICLE_FX_ASSET( strHeistFX )
								players_underwater_flare =  START_NETWORKED_PARTICLE_FX_LOOPED_ON_entity( "scr_heist_biolab_flare_underwater", objFlare, <<0.11, 0.00, 0.0>>, <<0.0, 0.0, 0.0>> ) 
								CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - MANAGE_UNDERWATER_FLARE - Created PTFX.")
							ENDIF
						ENDIF
					
				ENDIF
				
				IF flare_attach = TRUE
					flare_attach = FALSE
					CLEANUP_FLARE()
				ENDIF
			ENDIF
		ELSE
			CLEANUP_FLARE()
		ENDIF
	ELSE
		PRINTLN("[SALVAGE] CAN_REGISTER_MISSION_OBJECTS 2 = FALSE  ")
	ENDIF
ENDPROC

PROC CLIENT_PROCESSING()
	
	HANDLE_FINDERS_KEEPERS_COUNTDOWN_AUDIO(GET_CHALLENGE_EXPIRE_TIME()-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.Starttimer))

	//MAINTAIN_FINDERS_KEEPERS_SORTED_PARTICIPANTS()
	IF GET_SERVER_MISSION_STATE() != GAME_STATE_END
		DRAW_BOTTOM_RIGHT_HUD()
	ENDIF
	DRAW_CHALLENGE_DPAD_LBD(g_GBLeaderboardStruct.challengeLbdStruct, g_GBLeaderboardStruct.siDpadMovie, SUB_BOSS, g_GBLeaderboardStruct.dpadVariables, 
							g_GBLeaderboardStruct.fmDpadStruct)
							
	MANAGE_UNDERWATER_FLARE()							
	
	#IF IS_DEBUG_BUILD
		UPDATE_WIDGETS()
	#ENDIF
	
	IF gRESET_PLAYER_BD_PICKUP_FLAGS = TRUE
	OR (HAS_NET_TIMER_EXPIRED(saftycheckpointtimer,SAFETYTIMER) AND playerBD[PARTICIPANT_ID_TO_INT()].this_player_has_picked_up_package )
		RESET_PLAYER_BD_PICKUP_FLAGS()
		gRESET_PLAYER_BD_PICKUP_FLAGS = FALSE
	ENDIF
		
	IF gsalvagegiveplayerapoint = TRUE		
		gsalvagegiveplayerapoint = FALSE	
		//serverBD.iclientscore[PARTICIPANT_ID_TO_INT()]++
	ENDIF
	SWITCH serverBD.eGEN_Stage
		CASE GEN_START
			MAINTAIN_PACKAGE_CLIENT()
			//IF 
			IF  serverBD.ipackagecounter >= NUM_CHECKPOINTS 
			AND bHasdisplayedfinalshard = FALSE
				
				MAINTAIN_FINAL_SHARD()
			ENDIF
		BREAK 
		
		
		CASE GEN_IN_PACKAGE_AREA
			
		
		BREAK
		
		CASE GEN_SEARCH_FOR_PACKAGE_IN_AREA
		
		BREAK

	ENDSWITCH
	
	

ENDPROC

PROC SERVER_PROCESSING()

	MAINTAIN_FINDERS_KEEPERS_PLAYER_LEFT()
	
	SWITCH serverBD.eGEN_Stage
	
		CASE GEN_START
//			MAINTAIN_FINDERS_KEEPERS_PLAYER_LEFT()
		BREAK 
		
		CASE GEN_IN_PACKAGE_AREA
			
		BREAK
		
		CASE GEN_SEARCH_FOR_PACKAGE_IN_AREA
		
		BREAK
	ENDSWITCH

ENDPROC







FUNC BOOL MISSION_END_CHECK_SERVER()	
	IF serverBD.ipackagecounter >= NUM_CHECKPOINTS
	OR serverBD.inumberofcheckpointsRemaining = 0
		IF bHasdisplayedfinalshard = FALSE
			IF MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI
				TickerEventData.TickerEvent = TICKER_EVENT_SALVAGE_COLLECTED_ALL_CHECKPOINT
				
				TickerEventData.playerID = serverBD.sSortedPackagesCollected[0].playerID// finders_keepers_the_winner
				IF TickerEventData.playerID != NULL
				AND IS_NET_PLAYER_OK(TickerEventData.playerID)
					PRINTLN("TICKER_EVENT_SALVAGE_COLLECTED_ALL_CHECKPOINT TickerEventData.playerID =", GET_PLAYER_NAME(TickerEventData.playerID))
				ENDIF
				
				serverBD.eServer_ending_stat = ALL_PACKAGES_COLLECTED
				IF IS_GAME_A_DRAW()
					TickerEventData.playerID2 = serverBD.sSortedPackagesCollected[1].playerID//finders_keepers_the_winner2
					IF TickerEventData.playerID2 != NULL
					AND IS_NET_PLAYER_OK(TickerEventData.playerID2)
						PRINTLN("TICKER_EVENT_SALVAGE_COLLECTED_ALL_CHECKPOINT TickerEventData.playerID2 =", GET_PLAYER_NAME(TickerEventData.playerID2))
					ENDIF
					TickerEventData.dataInt = 1 // game is a draw
				ELSE
				
					TickerEventData.playerID2 = NULL
				ENDIF
					
				BROADCAST_TICKER_EVENT(TickerEventData, ALL_PLAYERS_ON_SCRIPT())
				NET_PRINT_TIME() NET_PRINT("     ---------->     SALVAGE -  MISSION END - TICKER_EVENT_SALVAGE_COLLECTED_ALL_CHECKPOINT     <----------     ") NET_NL()
			ENDIF
			PRINTLN("MAGNATE_GANG_BOSS [GB SALVAGE] - serverBD.ipackagecounter >= NUM_CHECKPOINTS - 1")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF HAS_NET_TIMER_EXPIRED(serverBD.Starttimer, GET_CHALLENGE_EXPIRE_TIME())
		IF MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI
			serverBD.eServer_ending_stat = TIME_EXPIRES
			TickerEventData.TickerEvent = TICKER_EVENT_SALVAGE_EXPIRY_TIME
			TickerEventData.playerID = serverBD.sSortedPackagesCollected[0].playerID// finders_keepers_the_winner
			TickerEventData.dataInt2 = serverBD.sSortedPackagesCollected[0].iScore
			IF serverBD.sSortedPackagesCollected[0].playerID != NULL
				IF TickerEventData.playerID != NULL
				AND IS_NET_PLAYER_OK(TickerEventData.playerID)
					PRINTLN("SALVAGE TICKER_EVENT_SALVAGE_EXPIRY_TIME TickerEventData.playerID =", GET_PLAYER_NAME(TickerEventData.playerID))
				ENDIF
				IF IS_GAME_A_DRAW()
					IF TickerEventData.playerID2 != NULL
					AND IS_NET_PLAYER_OK(TickerEventData.playerID2, FALSE)
						TickerEventData.playerID2 = serverBD.sSortedPackagesCollected[1].playerID//finders_keepers_the_winner2
						PRINTLN("SALVAGE TICKER_EVENT_SALVAGE_EXPIRY_TIME TickerEventData.playerID2 =", GET_PLAYER_NAME(TickerEventData.playerID2))
						TickerEventData.dataInt = 1 // game is a draw
					ENDIF
				ELSE
					TickerEventData.playerID2 = NULL
				ENDIF
				IF serverBD.sSortedPackagesCollected[0].iScore < 1
				AND serverBD.sSortedPackagesCollected[1].iScore < 1
					PRINTLN("SALVAGE TICKER_EVENT_SALVAGE_EXPIRY_TIME Nobody scored")
					TickerEventData.dataInt = 3 // nobody scored
				ENDIF
			ENDIF
			BROADCAST_TICKER_EVENT(TickerEventData, ALL_PLAYERS_ON_SCRIPT())
			NET_PRINT_TIME() NET_PRINT("     ---------->     SALVAGE -  MISSION END - TICKER_EVENT_SALVAGE_EXPIRY_TIME     <----------     ") NET_NL()
		ENDIF
		PRINTLN("SALVAGE MAGNATE_GANG_BOSS [GB SALVAGE] - MISSION_END_CHECK_SERVER HAS_NET_TIMER_EXPIRED(serverBD.Starttimer, GET_CHALLENGE_EXPIRE_TIME()) ")
		RETURN TRUE
	ENDIF
	
	#IF IS_DEBUG_BUILD
	
		IF bkillgame = TRUE
		OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
		OR GB_SHOULD_KILL_ACTIVE_BOSS_MISSION() 
		
			PRINTLN("SALVAGE bkillgame = TRUE")
			IF MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI
				serverBD.eServer_ending_stat = TIME_EXPIRES
				TickerEventData.TickerEvent = TICKER_EVENT_SALVAGE_EXPIRY_TIME
				TickerEventData.playerID = serverBD.sSortedPackagesCollected[0].playerID // finders_keepers_the_winner
				
				IF TickerEventData.playerID != NULL
				AND IS_NET_PLAYER_OK(TickerEventData.playerID)
					PRINTLN("SALVAGE TICKER_EVENT_SALVAGE_EXPIRY_TIME TickerEventData.playerID =", GET_PLAYER_NAME(TickerEventData.playerID))
				ENDIF
				
				//PRINTLN("MAGNATE_GANG_BOSS [GB SALVAGE] - finders_keepers_the_winner ")
				
				
				
				IF IS_GAME_A_DRAW()
					TickerEventData.playerID2 =serverBD.sSortedPackagesCollected[1].playerID //finders_keepers_the_winner2
					TickerEventData.dataInt = 1 // game is a draw
					IF TickerEventData.playerID2 != NULL
					AND IS_NET_PLAYER_OK(TickerEventData.playerID2)
						PRINTLN("SALVAGE TICKER_EVENT_SALVAGE_EXPIRY_TIME TickerEventData.playerID2 =", GET_PLAYER_NAME(TickerEventData.playerID2))
					ENDIF
				ELSE

					TickerEventData.playerID2 = NULL
				ENDIF
				
				//If nobody scored
				IF serverBD.sSortedPackagesCollected[0].iScore < 1
				AND serverBD.sSortedPackagesCollected[1].iScore < 1
					TickerEventData.dataInt = 3 // nobody scored
				ENDIF
				
				BROADCAST_TICKER_EVENT(TickerEventData, ALL_PLAYERS_ON_SCRIPT())
				NET_PRINT_TIME() NET_PRINT("     ---------->     SALVAGE -  MISSION END - TICKER_EVENT_SALVAGE_EXPIRY_TIME     <----------     ") NET_NL()
			ENDIF
			
			PRINTLN("SALVAGE MAGNATE_GANG_BOSS [GB SALVAGE] - MISSION_END_CHECK_SERVER IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S) ")
			RETURN TRUE
		ENDIF
	#ENDIF

	RETURN FALSE
ENDFUNC

//Server only function that checks to see if the game mode should now end. For this test script, it if time has passed.
FUNC BOOL HAVE_MISSION_END_CONDITIONS_BEEN_MET()
	
	#IF IS_DEBUG_BUILD
		IF bHostEndMissionNow
			RETURN TRUE
		ENDIF
	#ENDIF	
	
	IF MISSION_END_CHECK_SERVER()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

// -----------------------------------------------------------------------------------------------------------
//		Mission Cleanup
// -----------------------------------------------------------------------------------------------------------


PROC SCRIPT_CLEANUP()
	IF NETWORK_IS_GAME_IN_PROGRESS()
		CPRINTLN(DEBUG_NET_AMBIENT, DEBUG_NET_AMBIENT, "=== MAGNATE_GANG_BOSS SALVAGE SCRIPT_CLEANUP")
		GB_TIDYUP_SPECTATOR_CAM()

//		//RELEASE_CONTEXT_INTENTION(sSpecVars.iGbSpecContext)
//		IF NOT g_sMPTunables.bgb_finders_keepers_disable_trackify
//			REMOVE_TRACKIFY_MULTIPLE_TARGET(1)
//			SET_NUMBER_OF_MULTIPLE_TRACKIFY_TARGETS(0)
//			
//			ENABLE_MULTIPLAYER_TRACKIFY_APP(FALSE)
//		ENDIF
		CLEANUP_BLIPS_AND_ASSETS()
		IF GET_SERVER_MISSION_STATE() = GAME_STATE_END
		ENDIF
		
		
		gfinderskeeperslocalplayerhascollectedpickup = FALSE	
		g_salvage_refresh_rebreather_blips = FALSE  //refresh blips for everyone 
		g_salvage_rebreather_has_been_collected = FALSE
		g_sGb_Telemetry_data.sdata.m_match1 = serverBD.iMatchId1
		g_sGb_Telemetry_data.sdata.m_match2 = serverBD.iMatchId2
		PROCESS_CURRENT_BOSS_CHALLENGE_PLAYSTATS(g_sGb_Telemetry_data.sdata, serverBD.iclientscore[PARTICIPANT_ID_TO_INT()], inumberofrebreatherscollected)
//		IF IS_CELLPHONE_TRACKIFY_IN_USE()
//		AND  NOT g_sMPTunables.bgb_finders_keepers_disable_trackify
//			HANG_UP_AND_PUT_AWAY_PHONE()
//		ENDIF
		//IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		//IF NETWORK_DOES_NETWORK_ID_EXIST(salvagelocalplayersjetski)
		//	CLEANUP_NET_ID(salvagelocalplayersjetski)
		//ENDIF
		PRINTLN(" [SALVAGE] SCRIPT_CLEANUP() inumberofrebreatherscollected = ",inumberofrebreatherscollected )
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			REMOVE_JETSKIS_SERVER()
		ENDIF
		///ENDIF
		g_give_player_a_rebreather = FALSE
		USE_CUSTOM_SPAWN_POINTS(FALSE)
		SET_PLAYER_RESPAWN_IN_VEHICLE(FALSE)
		REMOVE_REBREATHER()
		gsalvagegiveplayerapoint = FALSE
		gRESET_PLAYER_BD_PICKUP_FLAGS = FALSE
		GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.icurrentsalvagevariationbeingplayed = -1
		PRINTLN("[SALVAGE] SCRIPT_CLEANUP() reset GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.icurrentsalvagevariationbeingplayed = ",GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.icurrentsalvagevariationbeingplayed)
	
	//REMOVE_PTFX_ASSET()
		//TRIGGER_MUSIC_EVENT("SALVAGE_STOP_MUSIC")
		SET_AUDIO_FLAG("DisableFlightMusic", FALSE) 
		SET_AUDIO_FLAG("WantedMusicDisabled", FALSE) 
		GB_COMMON_BOSS_MISSION_CLEANUP() 
	ENDIF
	
	TERMINATE_THIS_THREAD()
	
ENDPROC


//----------------------
//	MAIN SCRIPT
//----------------------

SCRIPT(MP_MISSION_DATA missionScriptArgs)
	CPRINTLN(DEBUG_NET_AMBIENT, DEBUG_NET_AMBIENT, "=== MAGNATE_GANG_BOSS SALVAGE START")
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF NOT PROCESS_PRE_GAME(missionScriptArgs)
			SCRIPT_CLEANUP()
		ENDIF	
	ENDIF
	
	
	
	INT i
	
	
	WHILE (TRUE)
	
		MP_LOOP_WAIT_ZERO()
		
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			// Boss quites
			PRINTLN("[SALVAGE] GB_GET_NUM_GOONS_IN_PLAYER_GANG(PLAYER_ID()) = 0")
			IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
			OR GET_GANG_BOSS_PLAYER_INDEX() = PLAYER_ID()
			OR initial_player_boss = PLAYER_ID()
				GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LEFT)
			ELSE
				GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_BOSS_LEFT)
			ENDIF
			CPRINTLN(DEBUG_NET_AMBIENT, DEBUG_NET_AMBIENT, "=== MAGNATE_GANG_BOSS SALVAGE SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE")
			SCRIPT_CLEANUP()
		ENDIF
		
		IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
			IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
			AND GB_GET_NUM_GOONS_IN_PLAYER_GANG(PLAYER_ID()) = 0
			
			#IF IS_DEBUG_BUILD
			AND NOT GET_COMMANDLINE_PARAM_EXISTS("sc_NoGoonsRequiredForBossWork")
			#ENDIF
			
				PRINTLN("[SALVAGE] GB_GET_NUM_GOONS_IN_PLAYER_GANG(PLAYER_ID()) = 0")
				SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_CHAL_OVER", "PIM_MAGM101")
				IF bHasdisplayedfinalshard = FALSE
					GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_SALVAGE, FALSE, sRewardsData )
				ENDIF
				GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LOW_NUMBERS)
				SCRIPT_CLEANUP()
			ELIF GB_GET_THIS_PLAYER_GANG_BOSS(PLAYER_ID()) = INVALID_PLAYER_INDEX()
			OR NOT NETWORK_IS_PLAYER_ACTIVE(GB_GET_THIS_PLAYER_GANG_BOSS(PLAYER_ID()))
				PRINTLN("[SALVAGE] NETWORK_IS_PLAYER_ACTIVE(GB_GET_THIS_PLAYER_GANG_BOSS(PLAYER_ID()))")
				
				IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
				OR GET_GANG_BOSS_PLAYER_INDEX() = PLAYER_ID()
				OR initial_player_boss = PLAYER_ID()
					GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LEFT)
				//	SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_CHAL_OVER", "GB_GOONLEFTb", GB_GET_PLAYERS_GANG_FOR_LEAVE_EVENT())
				ELSE
					IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID())
						GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_BOSS_LEFT)
						IF DOES_PLAYER_OWN_OFFICE(GB_GET_LOCAL_PLAYER_GANG_BOSS())
							SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_CHAL_OVER", "GBTER_BIGBSLFTC")
						ELSE
							SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_CHAL_OVER", "GBTER_BIG_BSLFT")
						ENDIF
					ELSE
						GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LEFT)
						SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_CHAL_OVER", "GB_GOONLEFTb")
					ENDIF
				ENDIF
				
				IF bHasdisplayedfinalshard = FALSE
					GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_SALVAGE, FALSE, sRewardsData )
				ENDIF
				SCRIPT_CLEANUP()
			ENDIF
		ENDIF
		
		IF GB_SHOULD_QUIT_ACTIVE_BOSS_MISSION()
			GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_FORCED)
			PRINTLN("[SALVAGE] - GB_SHOULD_QUIT_ACTIVE_BOSS_MISSION = TRUE")
			SCRIPT_CLEANUP()
		ENDIF
		IF GB_SHOULD_KILL_ACTIVE_BOSS_MISSION()
			GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_FORCED)
			PRINTLN("[SALVAGE] GB_SHOULD_KILL_ACTIVE_BOSS_MISSION")
			SCRIPT_CLEANUP()
		ENDIF
		
		
		GB_MAINTAIN_SPECTATE(sSpecVars)
	
		SWITCH(GET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT()))
		
			CASE GAME_STATE_INIT
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					IF GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()) != HASH("YachtRm_Bridge")
						GB_SET_ITS_SAFE_FOR_SPEC_CAM()	
						IF GB_LAUNCH_CHALLENGE_WITH_SPEC_CAM_IS_ACTIVE(sSpecVars)
							IF INIT_CLIENT()
								SET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT(),GAME_STATE_RUNNING)
							ENDIF
						ENDIF
					ELSE
						// Tell players in yacht bridge that a VIP Challange is starting
						PRINT_HELP_FOREVER_NO_SOUND("GBM_IN_APRT")
						GB_SET_GANG_BOSS_HELP_BACKGROUND(FALSE)
					ENDIF
				ENDIF
			BREAK
			
			CASE GAME_STATE_RUNNING
				MAINTAIN_SORTED_PARTICIPANTS()
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					CLIENT_PROCESSING()
					
					
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					IF bHasdisplayedfinalshard = FALSE
						
						MAINTAIN_FINAL_SHARD()
						CLEANUP_BLIPS_AND_ASSETS()
					ENDIF
					// Display bottom 
					//DRAW_BOTTOM_RIGHT_HUD()
					DRAW_CHALLENGE_DPAD_LBD(g_GBLeaderboardStruct.challengeLbdStruct, g_GBLeaderboardStruct.siDpadMovie, SUB_BOSS, g_GBLeaderboardStruct.dpadVariables, 
					g_GBLeaderboardStruct.fmDpadStruct)
					IF GB_MAINTAIN_BOSS_END_UI(BOSS_END_UI) 
						SET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT(),GAME_STATE_END)
					ENDIF
				ENDIF
			BREAK
			
			CASE GAME_STATE_END
				IF GB_SHOULD_RUN_SPEC_CAM()
					MAINTAIN_SORTED_PARTICIPANTS()
					GB_SET_SHOULD_CLOSE_SPECTATE()
				ELSE
				
					SCRIPT_CLEANUP()
				ENDIF
				
			BREAK
			
		ENDSWITCH
	
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			PROCESS_EVENTS()
			SWITCH(GET_SERVER_MISSION_STATE())
			
				CASE GAME_STATE_INIT
					IF INIT_SERVER()
						
						SET_SERVER_MISSION_STATE(GAME_STATE_RUNNING)
						
					ENDIF
					
				BREAK
				
				CASE GAME_STATE_RUNNING
					
					IF serverbd.has_a_checkpoint_been_passed = FALSE
						IF  HAS_CHECKPOINT_BEEN_COLLECTED_SERVER()

						ENDIF
					ENDIF
					
					IF NOT serverBD.bcreate_bjetskis
							IF CAN_REGISTER_MISSION_VEHICLES(GB_GET_BOSS_MISSION_NUM_VEH_REQUIRED(FMMC_TYPE_GB_SALVAGE))
								CREATE_JETSKIS_server()
							ELSE
								PRINTLN("[SALVAGE] CAN_REGISTER_MISSION_VEHICLES 1 = FALSE  ")
							ENDIF
					ENDIF
					
					
					//IF  bhas_the_package_been_created
					IF NOT serverBD.bcreate_rebreathers
						IF HAS_ANY_PLAYER_REACHED_SALVAGE_AREA()
							CREATE_INITIAL_REBREATHER_PICKUPS(serverBD.MapArea)
						ENDIF
					ENDIF
					
					IF g_salvage_rebreather_has_been_collected = TRUE
						IF HAS_REBREATHER_BEEN_COLLECTED_HOST()
						
						ENDIF
					
						
					ENDIF
					IF HAS_NET_TIMER_EXPIRED(rebreathertimer, g_sMPTunables.iexec_salvage_rebreather_delay)
						
						REPEAT MAX_NUM_REBREATHERS i
							IF temprebreathernumber[i] != -1
								CREATE_REPLACEMENT_REBREATHER_PICKUP( serverBD.MapArea, temprebreathernumber[i]) 
								temprebreathernumber[i] = -1
								RESET_NET_TIMER(rebreathertimer)
							ENDIF
						ENDREPEAT
					ENDIF
					
					IF HAVE_MISSION_END_CONDITIONS_BEEN_MET()
						SET_SERVER_MISSION_STATE(GAME_STATE_END)
					ELSE
						SERVER_PROCESSING()
						
					ENDIF
					PROCESS_EVENTS()
					
					//Bug 2555965
					IF GB_SHOULD_KILL_ACTIVE_BOSS_MISSION()
						SET_SERVER_MISSION_STATE(GAME_STATE_END)
					ENDIF
				BREAK
				
				CASE GAME_STATE_END
					
				BREAK
				
			ENDSWITCH
		
		ENDIF
		
	ENDWHILE
	
	
// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT

