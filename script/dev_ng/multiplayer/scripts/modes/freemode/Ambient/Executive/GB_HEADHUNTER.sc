//////////////////////////////////////////////////////////////////////////////////////////
// Name:        GB_HEADHUNTER.sc														//
// Description: VIP & Gang have to kill AI targets										//
// Written by:  David Watson															//
// Date: 27/01/2016																		//
//////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "net_mission.sch"

// Game Headers
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_path.sch"
USING "commands_event.sch"

// Network Headers
USING "net_include.sch"
USING "net_events.sch"
USING "net_spawn.sch"

//
USING "net_scoring_common.sch"
USING "net_ambience.sch"

//USING "help_at_location.sch"
USING "commands_object.sch"

USING "net_xp_animation.sch"

USING "net_wait_zero.sch"



USING "net_gang_boss.sch"
USING "DM_Leaderboard.sch"
USING "am_common_ui.sch"
#IF IS_DEBUG_BUILD
USING "net_debug_log.sch"
#ENDIF

// Game States. 
CONST_INT GAME_STATE_INI 				0
CONST_INT GAME_STATE_RUNNING			1
CONST_INT GAME_STATE_LEAVE				2
CONST_INT GAME_STATE_TERMINATE_DELAY	3
CONST_INT GAME_STATE_END				4

ENUM HEADHUNTER_STAGE_ENUM
	eAD_TARGETS,
	eAD_ATTACK,
	eAD_OFF_MISSION,
	eAD_CLEANUP
ENDENUM





CONST_INT MAX_TARGETS					4
CONST_INT MAX_BODYGUARDS_PER_TARGET		3
CONST_INT MAX_AMB_VEH_PER_TARGET		3

CONST_INT MAX_BODYGUARD_BLIPS			30

CONST_INT TIME_MISSION_DURATION 60000 * 15

CONST_INT TIME_EXPOLDE_VEH		30000

CONST_INT TIME_MISSION_TIMEOUT	600000



CONST_INT TIME_FORCE_RIVALS_JOIN	600000

CONST_INT MAX_SCENARIO_BLOCKING_AREAS	5

CONST_INT HEADHUNTER_AREA_CITY			0
CONST_INT HEADHUNTER_AREA_COUNTRY		1

//Server Bitset Data
CONST_INT biS_SHouldDeletePackage		0
CONST_INT biS_AllTargetsKilled			1
CONST_INT biS_AllPlayersFinished		2
CONST_INT biS_DurationExpired			3
CONST_INT biS_InitSpawnCoords			4
CONST_INT biS_BossLaunchedQuit			5
CONST_INT biS_ForceRivalsOnMission		6



CONST_INT 	TARGET_TYPE_DRIVER_WITH_BODYGUARDS		0
CONST_INT 	TARGET_TYPE_SOLO_ON_FOOT				1
CONST_INT 	TARGET_TYPE_ON_FOOT_WITH_BODYGUARDS		2

CONST_INT PED_AI_STATE_CREATED			0
CONST_INT PED_AI_STATE_TOUR_IN_CAR		1
CONST_INT PED_AI_STATE_FLEE_IN_CAR		2
CONST_INT PED_AI_STATE_LEAVE_CAR		3
CONST_INT PED_AI_STATE_FLEE_ON_FOOT		4
CONST_INT PED_AI_STATE_TOUR_ON_FOOT		5
CONST_INT PED_AI_STATE_DEFEND_TARGET	6
CONST_INT PED_AI_STATE_DEFEND_IN_CAR	7
CONST_INT PED_AI_STATE_SCENARIO			8
CONST_INT PED_AI_STATE_TAKE_COVER		9
CONST_INT PED_AI_STATE_DO_NOTHING		98
CONST_INT PED_AI_STATE_FINISHED			99

STRUCT structTarget
	INT iTargetType
	NETWORK_INDEX niPed
	NETWORK_INDEX niVeh
	
	NETWORK_INDEX niBodyguardPed[MAX_BODYGUARDS_PER_TARGET]
	
	NETWORK_INDEX niAmbientVeh[MAX_AMB_VEH_PER_TARGET]
	MODEL_NAMES mVeh
	MODEL_NAMES mPed
	
	VECTOR vSpawnCoord
	FLOAT fSpawnHead
	
	INT iTargetPedState
	INT iBodyguardPedState[MAX_BODYGUARDS_PER_TARGET]
	
	INT iSpawnData
	
	INT iClearAreaBitset
	
	BOOL bShouldSeekCover
	BOOL bShouldEnterCombat
ENDSTRUCT

CONST_INT MAX_HEADHUNTER_SPAWN_COORDS		10

STRUCT headhunterSpawnCoords
	VECTOR vSpawnCoords
	FLOAT fSpawnHeading
	
	VECTOR vBodyguardSpawnCoords[MAX_BODYGUARDS_PER_TARGET]
	FLOAT fBodyguardSpawnHead[MAX_BODYGUARDS_PER_TARGET]
	VECTOR vAmbientVehSpawnCoords[MAX_AMB_VEH_PER_TARGET]
	FLOAT fAmbientVehSpawnHead[MAX_AMB_VEH_PER_TARGET]
	MODEL_NAMES modelAmbVeh[MAX_AMB_VEH_PER_TARGET]
ENDSTRUCT

// The server broadcast data.
// Everyone can read this data, only the server can update it
STRUCT ServerBroadcastData
	INT iServerGameState
		
	INT iServerBitSet
	
	structTarget Targets[MAX_TARGETS]
	INT iTargetCreatedBitSet
	INT iTargetDriveFastBitSet
	INT iTargetVehDamagedBitSet
	INT iTargetAimedAtBitSet
	INT iTargetNearbyShotsBitset
	INT iTargetVehStuckBitSet
	
	INT iTargetKilledBitset
	INT iLockDoorsBitset
	INT iNumTargetsKilled = 0
	
	INT iUnlockedTargetDoorsBitset
	HEADHUNTER_STAGE_ENUM eStage = eAD_TARGETS
	
	
	
	INT iBossPartWhoLaunched = -1
	INT iBossPlayerWhoLaunched = -1
	INT iPlayerKilledBoss = -1
	
	INT iMapArea = -1
	INT iNumberOfSpawnCoords
	headhunterSpawnCoords pedSpawnCoords[MAX_HEADHUNTER_SPAWN_COORDS]
	headhunterSpawnCoords vehSpawnCoords[MAX_HEADHUNTER_SPAWN_COORDS]
	
	SCRIPT_TIMER MissionTerminateDelayTimer
	
	SCRIPT_TIMER timeDuration
	SCRIPT_TIMER timeForceRivalsJoin
	SCRIPT_TIMER timeResetAlpha
	
	SCRIPT_TIMER timeFailsafe[MAX_TARGETS]
	INT iMinPlayers = -1
	
	INT iMatchId1
	INT iMatchId2
	
	SCENARIO_BLOCKING_INDEX scenBlockingArea[MAX_SCENARIO_BLOCKING_AREAS]
	INT iScenBlockBitset
	
	#IF IS_DEBUG_BUILD
	BOOL bAllowWith2Players = FALSE
	BOOL bSomeoneSPassed
	INT iDebugVariation = -1
	#ENDIF
ENDSTRUCT
ServerBroadcastData serverBD



CONST_INT biP_MissionStartExpired	0
CONST_INT biP_JoinedAsRival			1
CONST_INT biP_GotThePackage			2
CONST_INT biP_JustCompHack			3
CONST_INT biP_Finished				4
CONST_INT biP_KilledBoss			5
CONST_INT biP_HudOkForSCTV			6


// The client broadcast data.
// Everyone can read this data, only the local player only the server can update it
STRUCT PlayerBroadcastData
	INT iGameState
	
	INT iPlayerBitSet
	
	INT iPlayerAimAtTargetBitSet
	INT iPlayerTargetVehStuckBitSet
	INT iPlayerShotsNearTargetBitSet
	INT iPlayerTargetVehDamagedBitSet
	INT iPlayerKilledTargetBitset
	INT iNonPartKilledTargetBitset
	
	INT iCustomBlipBitset
	
	HEADHUNTER_STAGE_ENUM eStage = eAD_TARGETS
	
	INT iMyBossPart = -1
	
	INT iMyTotalKills
	INT iOnFootTargetPedShouldSeekCover
	
	INT iOnFootTargetPedShouldEnterCombat
	
	#IF IS_DEBUG_BUILD
	INT iPDebugBitset
	
	#ENDIF
ENDSTRUCT
PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]

///// MISSION VARIABLES /////
INT iBoolsBitSet
CONST_INT biHelp1Done			0
CONST_INT biL_DoneIntroShard	1
CONST_INT biL_DoneIntroHelp		2
CONST_INT biL_DoneCoordHelp		3
CONST_INT biL_PlayerDied		4
CONST_INT biL_BlippedRival		5
CONST_INT biL_DoneEndTelemetry	6
CONST_INT biL_ResetPlayerBlips	7
CONST_INT biL_DonePackageHelp	8
CONST_INT biL_WaitForShard		9
CONST_INT biL_ResetBlipForShard	10
CONST_INT biL_SetBlipHeight		11
CONST_INT biL_RivalsJoinedHelp	12
CONST_INT biL_PreventCollection	13
CONST_INT biL_ResetForSctv		14
CONST_INT biL_SCTV_Clear_God_Text	15
CONST_INT biL_SetupSpawnCoord	16
CONST_INT biL_SetMaxWantedLevel	17
CONST_INT biL_SetFriendlyFire	18


INT iMusicBitset
CONST_INT biMusic_Init					0
CONST_INT biMusic_TriggerStart			1
CONST_INT biMusic_Reset					2
CONST_INT biMusic_2Packages				3
CONST_INT biMusic_1Package				4
CONST_INT biMusic_30sStarted			5
CONST_INT biMusic_StopAllCPack			6
CONST_INT biMusic_Attack				7
CONST_INT biMusic_ShouldStop30s			8
CONST_INT biMusic_Started30sCountdown	9
CONST_INT biMusic_StoppedCountdownMusic	10
CONST_INT biMusic_PrepareKill30s		11
CONST_INT biMusic_RestoreRadio			12
CONST_INT biMusic_DoneFadeInRadio		13
CONST_INT biMusic_DoneStart30s			14
CONST_INT biMusic_DonePre30sStop		15
CONST_INT biMusic_DoneKill30s			16


INT iStaggeredParticipant

INT iClientStaggeredPlayer

INT iLocalNumberOfTargetsKilled

//--Relationship groups

REL_GROUP_HASH relHeadHunterPlayer
//REL_GROUP_HASH relHeadHunterAi 
REL_GROUP_HASH relMyFmGroup




//INT iMyCurrentWantedLevel

INT iMyLastClosestTarget = -1


GB_STRUCT_BOSS_END_UI gbBossEndUi

BLIP_INDEX blipTarget[MAX_TARGETS]

AI_BLIP_STRUCT bodyguardBlip[MAX_BODYGUARD_BLIPS]

NETWORK_INDEX niLocalBodyguardList[MAX_BODYGUARD_BLIPS]

HUD_COLOURS hcLaunchingGang 

BOOL bDoneDistanceCheck

INT iMyMaxWantedLevel
SCRIPT_TIMER timeNoWanted
// Widgets & Debug.
#IF IS_DEBUG_BUILD
	BOOL bHostEndMissionNow

	CONST_INT biPDebug_PressedS		0
	CONST_INT biPDebug_PressedF		1
	CONST_INT biPDebug_PressedJ		2
#ENDIF

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  MAIN PROCS                            ////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//--Tunables
FUNC INT GET_GB_HEADHUNTER_DURATION()
//	RETURN 45000
	RETURN  g_sMPTunables.iexec_headhunter_time_limit
//	RETURN g_sMPTunables.iGB_HEADHUNTER_time_limit //TIME_MISSION_DURATION
ENDFUNC

FUNC INT GET_GB_HEADHUNTER_TOTAL_NUM_TARGETS()
	RETURN MAX_TARGETS
//	RETURN g_sMPTunables.iGB_HEADHUNTER_number_of_packages     // MAX_PACKAGES
ENDFUNC
//
//FUNC FLOAT GET_MIN_DIST_BETWEEN_PACKAGES()
//	RETURN TO_FLOAT(g_sMPTunables.iGB_HEADHUNTER_minimum_distance_to_next_package)
//ENDFUNC

FUNC INT GET_ON_FOOT_BODYGUARD_START_HEALTH()
	RETURN g_sMPTunables.iexec_headhunter_security_health
ENDFUNC

FUNC INT GET_ON_FOOT_BODYGUARD_START_ARMOUR()
	RETURN 200
ENDFUNC

FUNC INT GET_ON_FOOT_BODYGUARD_ACCURACY()
	RETURN ROUND(g_sMPTunables.fexec_headhunter_security_accuracy)
ENDFUNC

FUNC INT GET_ON_FOOT_TARGET_START_HEALTH()
	RETURN g_sMPTunables.iexec_headhunter_target_health
ENDFUNC

FUNC INT GET_ON_FOOT_TARGET_START_ARMOUR
	RETURN 100
ENDFUNC 

FUNC INT GET_ON_FOOT_TARGET_ACCURACY()
	RETURN ROUND(g_sMPTunables.fexec_headhunter_target_accuracy)
ENDFUNC
FUNC INT GET_GB_HEADHUNTER_FORCE_RIVALS_JOIN_TIME()
	RETURN TIME_FORCE_RIVALS_JOIN
ENDFUNC

FUNC MODEL_NAMES GET_BODYGUARD_MODEL()
	RETURN S_M_M_ChemSec_01
ENDFUNC


FUNC INT GET_MY_SPEC_TARGET_AS_PARTICIPANT_INT()
	INT iPart = -1
	PLAYER_INDEX playerSPec = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget
	PARTICIPANT_INDEX partSpec
	
	IF NETWORK_IS_PLAYER_ACTIVE(playerSPec)
		IF NETWORK_IS_PLAYER_A_PARTICIPANT(playerSPec)
			partSpec = NETWORK_GET_PARTICIPANT_INDEX(playerSPec)
			iPart = NATIVE_TO_INT(partSpec)
		ENDIF
	ENDIF
	
	RETURN iPart
ENDFUNC

FUNC BOOL SHOULD_LOCAL_PLAYER_SEE_GB_HEADHUNTER_HUD()
	IF NOT IS_PLAYER_SCTV(PLAYER_ID())
		IF NOT MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI 
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [SHOULD_LOCAL_PLAYER_SEE_GB_HEADHUNTER_HUD] FALSE - GB_IS_IT_SAFE_TO_DISPLAY_BOSS_MISSION_UI")
			IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_HudOkForSCTV)
				CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_HudOkForSCTV)
			ENDIF
			RETURN FALSE
		ENDIF
		
		IF GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE()
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - SHOULD_LOCAL_PLAYER_SEE_GB_HEADHUNTER_HUD - FALSE AS GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE")
			IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_HudOkForSCTV)
				CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_HudOkForSCTV)
			ENDIF
			RETURN FALSE
		ENDIF
		
		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_HudOkForSCTV)
			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_HudOkForSCTV)
		ENDIF
	ELSE
		//-- SCTV player
		INT iPart = GET_MY_SPEC_TARGET_AS_PARTICIPANT_INT()
		IF iPart = -1
			RETURN FALSE
		ENDIF
		
		IF NOT IS_BIT_SET(playerBD[iPart].iPlayerBitSet, biP_HudOkForSCTV)
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC


FUNC BOOL DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
	INT iPart = -1
	
	IF NOT IS_PLAYER_SCTV(PLAYER_ID())
		iPart = PARTICIPANT_ID_TO_INT()
	ELSE
		iPart = GET_MY_SPEC_TARGET_AS_PARTICIPANT_INT()
	ENDIF
	
	IF iPart != -1
		RETURN (IS_BIT_SET(playerBD[iPart].iPlayerBitSet, biP_JoinedAsRival))
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

FUNC INT GET_NUMBER_OF_TARGETS_REMAINING()
	RETURN (MAX_TARGETS - serverBD.iNumTargetsKilled)
ENDFUNC

FUNC FLOAT GET_VDIST2_TO_NEAREST_TARGET(INT &iClosest, BOOL bConsideOnFootOnly = FALSE)
	FLOAT fClosest2 = 9999999999.0
	FLOAT fCurrent2 = 0.0
//	INT iClosest = -1
	INT i
	
	VECTOR vPlayerLoc = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
	VECTOR vLoc
	
	REPEAT MAX_TARGETS i
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.Targets[i].niPed)
			IF serverBD.Targets[i].iTargetType = TARGET_TYPE_ON_FOOT_WITH_BODYGUARDS
			OR NOT bConsideOnFootOnly
				IF NOT IS_NET_PED_INJURED(serverBD.Targets[i].niPed)
					vLoc = GET_ENTITY_COORDS(NET_TO_PED(serverBD.Targets[i].niPed))
					fCurrent2 = VDIST2(vPlayerLoc, vLoc)
					IF fCurrent2 < fClosest2
						fClosest2 = fCurrent2
						iClosest = i
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN fClosest2
ENDFUNC


FUNC PLAYER_INDEX GET_CLOSEST_ATTACK_PLAYER_TO_TARGET(INT iTarget, FLOAT &fVdist2ToPlayer)
	PLAYER_INDEX playerClosest = INVALID_PLAYER_INDEX()
	FLOAT fClosest2 = 99999999
	FLOAT fCurDist2
	VECTOR vTargetLoc = GET_ENTITY_COORDS(NET_TO_PED(serverBD.Targets[itarget].niPed))
	INT iPart
	
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iPart
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPart))
			PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart))
			IF NOT IS_PLAYER_SCTV(PlayerId)
				PED_INDEX pedPlayer = GET_PLAYER_PED(PlayerId)
				IF iPart = serverBD.iBossPartWhoLaunched
				OR playerBD[iPart].iMyBossPart = serverBD.iBossPartWhoLaunched
					IF NOT IS_PED_INJURED(pedPlayer)
						fCurDist2 = VDIST2(GET_ENTITY_COORDS(pedPlayer), vTargetLoc)
						IF fCurDist2 < fClosest2
							fClosest2 = fCurDist2
							playerClosest = PlayerId
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF playerClosest != INVALID_PLAYER_INDEX()
		fVdist2ToPlayer = fClosest2
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [GET_CLOSEST_ATTACK_PLAYER_TO_TARGET] CLOSEST PLAYER = ", GET_PLAYER_NAME(playerClosest))
	ENDIF
	
	RETURN playerClosest
ENDFUNC
PROC SET_LAUNCHING_GANG_HUD_COLOUR()
	IF serverBD.iBossPartWhoLaunched > -1
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [SET_LAUNCHING_GANG_HUD_COLOUR] Set...")
		hcLaunchingGang = GB_GET_PLAYER_GANG_HUD_COLOUR(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(serverBD.iBossPartWhoLaunched)))
	ENDIF
ENDPROC

/// PURPOSE:
///    Store my boss's participant ID in playerBD, so I don't have to calculate it every time
FUNC INT GET_MY_BOSS_PARTICPANT_ID()
	INT iPart = -1
	PLAYER_INDEX playerToCheck = INVALID_PLAYER_INDEX()
	
	IF NOT IS_PLAYER_SCTV(PLAYER_ID())
		iPart = PARTICIPANT_ID_TO_INT()
		playerToCheck = PLAYER_ID()
	ELSE
		iPart = GET_MY_SPEC_TARGET_AS_PARTICIPANT_INT()
		playerToCheck = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget
	ENDIF
	
	IF iPart = -1
		RETURN -1
	ENDIF
	
	IF playerBD[iPart].iMyBossPart != -1
		//-- Already stored
		RETURN playerBD[iPart].iMyBossPart
	ENDIF
	
	PLAYER_INDEX playerBoss
	
	IF GB_IS_PLAYER_BOSS_OF_A_GANG(playerToCheck)
		//-- I'm a a Boss, store my participant ID
		playerBD[iPart].iMyBossPart = iPart
		IF iPart = serverBD.iBossPartWhoLaunched	
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER [GET_MY_BOSS_PARTICPANT_ID] SETTING MYSELF CRITICAL AS I'M THE BOSS WHO LAUNCHED")
			GB_SET_LOCAL_PLAYER_CRITICAL_TO_JOB(TRUE)
		ENDIF
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER [GET_MY_BOSS_PARTICPANT_ID] SETTING iMyBossPart TO ", iPart, " (MYSELF) AS I'M A BOSS")
	ELSE
		//-- I'm not a boss
		IF GB_IS_PLAYER_MEMBER_OF_A_GANG(playerToCheck)
			playerBoss = GB_GET_THIS_PLAYER_GANG_BOSS(playerToCheck)
			IF playerBoss != INVALID_PLAYER_INDEX()
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER [GET_MY_BOSS_PARTICPANT_ID] THINK MY BOSS IS ", GET_PLAYER_NAME(playerBoss))
				IF NETWORK_IS_PLAYER_A_PARTICIPANT(playerBoss)
					playerBD[iPart].iMyBossPart = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(playerBoss))
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER [GET_MY_BOSS_PARTICPANT_ID] SETTING iMyBossPart TO ", playerBD[iPart].iMyBossPart, " WHO IS PLAYER ",GET_PLAYER_NAME(playerBoss))
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN playerBD[iPart].iMyBossPart
ENDFUNC

/// PURPOSE:
///    Determine if it was my boss that launched the assault Boss Work
FUNC BOOL DID_MY_BOSS_LAUNCH_GB_HEADHUNTER()
	INT iMyBossPart = GET_MY_BOSS_PARTICPANT_ID()
	IF iMyBossPart > -1
		IF serverBD.iBossPartWhoLaunched = iMyBossPart
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_HEADHUNTER_DAMAGE_EVENT(INT iCount)

	STRUCT_ENTITY_DAMAGE_EVENT sei 

	PED_INDEX pedKiller
	PED_INDEX pedTemp
	PLAYER_INDEX playerVictim
	PLAYER_INDEX playerKiller
	PLAYER_INDEX playerBoss
	
	SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerData
	
	#IF IS_DEBUG_BUILD
	INT iVictimPart
	#ENDIF


	INT iTarget
	// Grab the event data.
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sei, SIZE_OF(sei))
		REPEAT MAX_TARGETS iTarget
			IF NOT IS_BIT_SET(serverBD.iTargetKilledBitset, iTarget)
				IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerKilledTargetBitset, iTarget)
					IF DOES_ENTITY_EXIST(sei.VictimIndex)				
						IF IS_ENTITY_A_VEHICLE(sei.VictimIndex)		
							IF NOT IS_BIT_SET(serverBD.iTargetVehDamagedBitSet, iTarget)
								IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerTargetVehDamagedBitSet, iTarget)
									IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.Targets[iTarget].niVeh)
										IF DOES_ENTITY_EXIST(NET_TO_VEH(serverbd.Targets[iTarget].niVeh))						
											IF GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(sei.VictimIndex) = NET_TO_VEH(serverbd.Targets[iTarget].niVeh)
												IF DOES_ENTITY_EXIST(sei.DamagerIndex)
													IF IS_ENTITY_A_PED(sei.DamagerIndex)							
														IF GET_PED_INDEX_FROM_ENTITY_INDEX(sei.DamagerIndex) = PLAYER_PED_ID()
															IF GET_MY_BOSS_PARTICPANT_ID() = serverBD.iBossPartWhoLaunched
																SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerTargetVehDamagedBitSet, iTarget)
																CPRINTLN(DEBUG_NET_MAGNATE,	"     ---------->     HEADHUNTER - [PROCESS_HEADHUNTER_DAMAGE_EVENT] I DAMAGED THIS TARGET VEHICLE = ", iTarget)
															ENDIF
														ENDIF
													ENDIF
												ENDIF
												
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF IS_ENTITY_A_PED(sei.VictimIndex)
								pedTemp = GET_PED_INDEX_FROM_ENTITY_INDEX(sei.VictimIndex)
								IF pedTemp = NET_TO_PED(serverBD.Targets[iTarget].niPed)
									IF DOES_ENTITY_EXIST(sei.DamagerIndex)
										IF IS_ENTITY_A_PED(sei.DamagerIndex)	
											pedKiller = GET_PED_INDEX_FROM_ENTITY_INDEX(sei.DamagerIndex)				
											IF IS_PED_A_PLAYER(pedKiller)
												playerKiller = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedKiller)
												CPRINTLN(DEBUG_NET_MAGNATE,	"     ---------->     HEADHUNTER - [PROCESS_HEADHUNTER_DAMAGE_EVENT] THIS PLAYER ", GET_PLAYER_NAME(playerKiller), " HAS DAMAGED TARGET PED ", iTarget)
												IF NETWORK_IS_PLAYER_A_PARTICIPANT(playerKiller)
													CPRINTLN(DEBUG_NET_MAGNATE,	"     ---------->     HEADHUNTER - [PROCESS_HEADHUNTER_DAMAGE_EVENT] THIS PLAYER ", GET_PLAYER_NAME(playerKiller), " HAS DAMAGED TARGET PED ", iTarget, " AND IS A PART")
													IF sei.VictimDestroyed
														CPRINTLN(DEBUG_NET_MAGNATE,	"     ---------->     HEADHUNTER - [PROCESS_HEADHUNTER_DAMAGE_EVENT] THIS PLAYER ", GET_PLAYER_NAME(playerKiller), " HAS KILLED TARGET PED ", iTarget)
														IF playerKiller = PLAYER_ID()
															playerBD[PARTICIPANT_ID_TO_INT()].iMyTotalKills++
															
															CPRINTLN(DEBUG_NET_MAGNATE,	"     ---------->     HEADHUNTER - [PROCESS_HEADHUNTER_DAMAGE_EVENT] I HAVE KILLED TARGET PED ", iTarget, " MY kills = ", playerBD[PARTICIPANT_ID_TO_INT()].iMyTotalKills)
															SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerKilledTargetBitset, iTarget)
															
															
															TickerData.TickerEvent = TICKER_EVENT_MTA_KILLED
															TickerData.playerID = PLAYER_ID()
															BROADCAST_TICKER_EVENT(TickerData,ALL_PLAYERS_ON_SCRIPT(FALSE))
															
															PRINT_TICKER("HUNT_TCKP") // You assassinated a target.
															
														ENDIF
													ENDIF
												ELSE
													//-- Damaged/killed by non-part player
													IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iNonPartKilledTargetBitset, iTarget)
														IF sei.VictimDestroyed
															playerKiller = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedKiller)
															CPRINTLN(DEBUG_NET_MAGNATE,	"     ---------->     HEADHUNTER - [PROCESS_HEADHUNTER_DAMAGE_EVENT] THINK THIS TARGET KILLED BY NON NON PART ", iTarget, " WILL SET iNonPartKilledTargetBitset, KILLED BY PLAYER ", GET_PLAYER_NAME(playerKiller))
															SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iNonPartKilledTargetBitset, iTarget)
														ENDIF
													ENDIF
												ENDIF
											ELSE
												//-- Damaged / killed by non-player
												IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iNonPartKilledTargetBitset, iTarget)
													IF sei.VictimDestroyed
														CPRINTLN(DEBUG_NET_MAGNATE,	"     ---------->     HEADHUNTER - [PROCESS_HEADHUNTER_DAMAGE_EVENT] THINK THIS TARGET KILLED BY NON PLAYER ", iTarget, " WILL SET iNonPartKilledTargetBitset")
														SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iNonPartKilledTargetBitset, iTarget)
													ENDIF
												ENDIF
											ENDIF
										ELSE
											//-- Damager not a ped
											CPRINTLN(DEBUG_NET_MAGNATE,	"     ---------->     HEADHUNTER - [PROCESS_HEADHUNTER_DAMAGE_EVENT] THINK THIS TARGET ", iTarget, " HAS BEEN DAMAGED BY SOMETHING OTHER THAN A PED!")
											IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iNonPartKilledTargetBitset, iTarget)
												IF sei.VictimDestroyed
													CPRINTLN(DEBUG_NET_MAGNATE,	"     ---------->     HEADHUNTER - [PROCESS_HEADHUNTER_DAMAGE_EVENT] THINK THIS TARGET KILLED BY A NON-PED!", iTarget, " WILL SET iNonPartKilledTargetBitset")
													SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iNonPartKilledTargetBitset, iTarget)
												ENDIF
											ENDIF
										ENDIF
									ELSE
										//-- Damager doesn't exist (probably -1)
										CPRINTLN(DEBUG_NET_MAGNATE,	"     ---------->     HEADHUNTER - [PROCESS_HEADHUNTER_DAMAGE_EVENT] THINK THIS TARGET ", iTarget, " HAS BEEN DAMAGED BUT DAMAGER DOESN'T EXIST!")
										IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iNonPartKilledTargetBitset, iTarget)
											IF sei.VictimDestroyed
												CPRINTLN(DEBUG_NET_MAGNATE,	"     ---------->     HEADHUNTER - [PROCESS_HEADHUNTER_DAMAGE_EVENT] THINK THIS TARGET KILLED BUT DAMAGER DOESN'T EXIST!", iTarget, " WILL SET iNonPartKilledTargetBitset")
												SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iNonPartKilledTargetBitset, iTarget)
											ENDIF
										ENDIF
									ENDIF					
								ENDIF
							
								
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	//-- Set rivals as permanant participants if they damage someone in the key organization
		IF DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
			IF NOT GB_IS_GLOBAL_CLIENT_BIT0_SET(PLAYER_ID(), eGB_GLOBAL_CLIENT_BITSET_0_PERMANENT_PARTICIPANT)
				IF DOES_ENTITY_EXIST(sei.VictimIndex)	
					IF IS_ENTITY_A_PED(sei.VictimIndex)
						pedTemp = GET_PED_INDEX_FROM_ENTITY_INDEX(sei.VictimIndex)
						IF IS_PED_A_PLAYER(pedTemp)
							playerVictim = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedTemp)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [PROCESS_HEADHUNTER_DAMAGE_EVENT] THINK THIS PLAYER HAS BEEN DAMAGED BY SOMEONE ", GET_PLAYER_NAME(playerVictim))
							IF DOES_ENTITY_EXIST(sei.DamagerIndex)
								IF IS_ENTITY_A_PED(sei.DamagerIndex)	
									pedKiller = GET_PED_INDEX_FROM_ENTITY_INDEX(sei.DamagerIndex)				
									IF IS_PED_A_PLAYER(pedKiller)
										CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [PROCESS_HEADHUNTER_DAMAGE_EVENT] THINK THIS PLAYER HAS BEEN DAMAGED BY A PLAYER ", GET_PLAYER_NAME(playerVictim))
										playerKiller = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedKiller)
										CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [PROCESS_HEADHUNTER_DAMAGE_EVENT] THINK THIS PLAYER ", GET_PLAYER_NAME(playerVictim), " HAS BEEN DAMAGED BY THIS PLAYER ", GET_PLAYER_NAME(playerKiller))
										IF playerKiller = PLAYER_ID()
											CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [PROCESS_HEADHUNTER_DAMAGE_EVENT] THINK THIS PLAYER ", GET_PLAYER_NAME(playerVictim), " HAS BEEN DAMAGED BY ME")
											
											IF NETWORK_IS_PLAYER_A_PARTICIPANT(playerVictim)
												#IF IS_DEBUG_BUILD
												iVictimPart = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(playerVictim))
												CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [PROCESS_HEADHUNTER_DAMAGE_EVENT] THINK THIS PLAYER ", GET_PLAYER_NAME(playerVictim), " IS A PARTICIPANT iVictimPart = ", iVictimPart)
												#ENDIF

												//-- Have I damaged someone in launching gang?
												IF GB_IS_PLAYER_MEMBER_OF_A_GANG(playerVictim)
													CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [PROCESS_HEADHUNTER_DAMAGE_EVENT] VICTIM ", GET_PLAYER_NAME(playerVictim), " IS A MEMBER OF A GANG")
													IF serverBD.iBossPartWhoLaunched > -1
														playerBoss = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(serverBD.iBossPartWhoLaunched))
														CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [PROCESS_HEADHUNTER_DAMAGE_EVENT] CHECKING IF VICTIM ", GET_PLAYER_NAME(playerVictim), " IS IN LAUNCHING BOSS'S GANG", GET_PLAYER_NAME(playerBoss))
														IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(playerVictim, playerBoss)
															IF NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE()
																CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [PROCESS_HEADHUNTER_DAMAGE_EVENT] SET AS PERMENANT AS I HAVE DAMAGED PLAYER ",GET_PLAYER_NAME(playerVictim), " WHO IS A MEMBER OF THIS PLAYER'S GANG", GET_PLAYER_NAME(playerBoss))
																GB_SET_PLAYER_AS_PERMANENT_ON_BOSS_MISSION()
																GB_SET_PLAYER_AS_QUALIFYING_PARTICIPANT()
															ENDIF
														ENDIF
													ENDIF
												ENDIF
												
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF

					ENDIF
				ENDIF
			ENDIF
		ENDIF
ENDPROC

PROC PROCESS_EVENTS()
	
	INT iCount
	EVENT_NAMES ThisScriptEvent
	
	//process the events
	
	REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iCount
		
		ThisScriptEvent = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iCount)
		
		SWITCH ThisScriptEvent
			
			CASE EVENT_NETWORK_DAMAGE_ENTITY
				

				PROCESS_HEADHUNTER_DAMAGE_EVENT(iCount)

				
				
			BREAK
			
		ENDSWITCH
		
	ENDREPEAT
	
	
ENDPROC



PROC STOP_30S_COUNTDOWN_AUDIO()
//	EXIT
	
	
	//-- From spreadsheet
	//--  //depot/gta5/docs/Audio/Interactive_Music/DLC/Apartments_Magnate.xlsx
	IF IS_BIT_SET(iMusicBitset, biMusic_ShouldStop30s)
		IF IS_BIT_SET(iMusicBitset, biMusic_Started30sCountdown)
			IF NOT IS_BIT_SET(iMusicBitset, biMusic_StoppedCountdownMusic)
			
				//-- Restore radio control 
				IF IS_BIT_SET(iMusicBitset, biMusic_PrepareKill30s)	
					IF NOT IS_BIT_SET(iMusicBitset, biMusic_RestoreRadio)
						SET_USER_RADIO_CONTROL_ENABLED(TRUE)
						SET_AUDIO_FLAG("AllowScoreAndRadio", FALSE)
						SET_AUDIO_FLAG("WantedMusicDisabled", FALSE)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [STOP_30S_COUNTDOWN_AUDIO] SSET_USER_RADIO_CONTROL_ENABLED(TRUE)")
					ENDIF
				ENDIF
				
				IF NOT IS_BIT_SET(iMusicBitset, biMusic_DoneKill30s)
					IF TRIGGER_MUSIC_EVENT("APT_COUNTDOWN_30S_KILL")
						SET_BIT(iMusicBitset, biMusic_DoneKill30s)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [STOP_30S_COUNTDOWN_AUDIO] SET biMusic_DoneKill30s")
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(iMusicBitset, biMusic_DoneKill30s)
					IF NOT IS_BIT_SET(iMusicBitset, biMusic_DoneFadeInRadio)
						IF TRIGGER_MUSIC_EVENT("APT_FADE_IN_RADIO")
							SET_BIT(iMusicBitset, biMusic_DoneFadeInRadio)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [STOP_30S_COUNTDOWN_AUDIO] SET biMusic_DoneFadeInRadio")
						ENDIF
					ENDIF
					
					IF IS_BIT_SET(iMusicBitset, biMusic_DoneFadeInRadio)
						SET_BIT(iMusicBitset, biMusic_StoppedCountdownMusic)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [STOP_30S_COUNTDOWN_AUDIO] SET biMusic_StoppedCountdownMusic")
					ENDIF
				ENDIF
				
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_GB_HEADHUNTER_MUSIC()
	//EXIT
	
	/*
	STOP_30S_COUNTDOWN_AUDIO()
	
	IF IS_BIT_SET(iMusicBitset, biMusic_Started30sCountdown)
		//-- Don't do anything here if we're doing the 30s countdown audio
		EXIT
	ENDIF
	IF DID_MY_BOSS_LAUNCH_GB_HEADHUNTER()
		IF NOT IS_BIT_SET(iMusicBitset, biMusic_Init)
			SET_AUDIO_FLAG("DisableFlightMusic", TRUE) 
			SET_AUDIO_FLAG("WantedMusicDisabled", TRUE)
			
			SET_BIT(iMusicBitset, biMusic_Init)
			
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_GB_HEADHUNTER_MUSIC] SET biMusic_Init")
		ENDIF
		
		//-- Event start
		IF NOT IS_BIT_SET(iMusicBitset, biMusic_TriggerStart)
			IF TRIGGER_MUSIC_EVENT("BG_HEADHUNTER_START")
				SET_BIT(iMusicBitset, biMusic_TriggerStart)
			
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_GB_HEADHUNTER_MUSIC] SET biMusic_TriggerStart")
			ELSE
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_GB_HEADHUNTER_MUSIC] TRYING TO TRIGGER BG_HEADHUNTER_START")
			ENDIF
		ENDIF
		
		//-- 2 packages remaining
		IF IS_BIT_SET(iMusicBitset, biMusic_TriggerStart)
			IF NOT IS_BIT_SET(iMusicBitset, biMusic_2Packages)
				IF serverBD.iPackagesCollected = 1
					IF TRIGGER_MUSIC_EVENT("BG_HEADHUNTER_MID")
						SET_BIT(iMusicBitset, biMusic_2Packages)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_GB_HEADHUNTER_MUSIC] SET biMusic_2Packages")
					ELSE
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_GB_HEADHUNTER_MUSIC] TRYING TO TRIGGER BG_HEADHUNTER_MID")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//-- 1 package remaining
		IF IS_BIT_SET(iMusicBitset, biMusic_2Packages)
			IF NOT IS_BIT_SET(iMusicBitset, biMusic_1Package)
				IF serverBD.iPackagesCollected = 2
					IF TRIGGER_MUSIC_EVENT("BG_HEADHUNTER_FINAL")
						SET_BIT(iMusicBitset, biMusic_1Package)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_GB_HEADHUNTER_MUSIC] SET biMusic_1Package")
					ELSE
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_GB_HEADHUNTER_MUSIC] TRYING TO TRIGGER BG_HEADHUNTER_FINAL")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//-- Stop when all collected / Boss killed
		IF IS_BIT_SET(serverBD.iServerBitSet, biS_AllTargetsKilled)
		OR IS_BIT_SET(serverBD.iServerBitSet, biS_BossKilled)
			IF IS_BIT_SET(iMusicBitset, biMusic_1Package)
				IF NOT IS_BIT_SET(iMusicBitset, biMusic_StopAllCPack)
					IF TRIGGER_MUSIC_EVENT("BG_HEADHUNTER_STOP")
						SET_BIT(iMusicBitset, biMusic_StopAllCPack)
						SET_BIT(iMusicBitset, biMusic_Reset)
						SET_AUDIO_FLAG("DisableFlightMusic", FALSE) 
						SET_AUDIO_FLAG("WantedMusicDisabled", FALSE)
						
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_GB_HEADHUNTER_MUSIC] SET biMusic_1Package")
					ELSE
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_GB_HEADHUNTER_MUSIC] TRYING TO TRIGGER BG_HEADHUNTER_STOP")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//-- Rival players
	IF DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
		IF GB_IS_GLOBAL_CLIENT_BIT0_SET(PLAYER_ID(), eGB_GLOBAL_CLIENT_BITSET_0_PERMANENT_PARTICIPANT)
			IF NOT IS_BIT_SET(iMusicBitset, biMusic_Init)
				SET_AUDIO_FLAG("DisableFlightMusic", TRUE) 
				SET_AUDIO_FLAG("WantedMusicDisabled", TRUE)
				
				SET_BIT(iMusicBitset, biMusic_Init)
				
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_GB_HEADHUNTER_MUSIC] SET biMusic_Init")
			ENDIF
			
			IF NOT IS_BIT_SET(iMusicBitset, biMusic_Attack)
				IF TRIGGER_MUSIC_EVENT("BG_HEADHUNTER_START_ATTACK")
					SET_BIT(iMusicBitset, biMusic_Attack)
				
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_GB_HEADHUNTER_MUSIC] SET biMusic_Attack")
				ELSE
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_GB_HEADHUNTER_MUSIC] TRYING TO TRIGGER BG_HEADHUNTER_START_ATTACK")
				ENDIF
			ENDIF
			
			//-- Stop when all collected / Boss killed
			IF IS_BIT_SET(serverBD.iServerBitSet, biS_AllTargetsKilled)
			OR IS_BIT_SET(serverBD.iServerBitSet, biS_BossKilled)
				IF IS_BIT_SET(iMusicBitset, biMusic_Attack)
					IF NOT IS_BIT_SET(iMusicBitset, biMusic_StopAllCPack)
						IF TRIGGER_MUSIC_EVENT("BG_HEADHUNTER_STOP")
							SET_BIT(iMusicBitset, biMusic_StopAllCPack)
							SET_BIT(iMusicBitset, biMusic_Reset)
							SET_AUDIO_FLAG("DisableFlightMusic", FALSE) 
							SET_AUDIO_FLAG("WantedMusicDisabled", FALSE)
							
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_GB_HEADHUNTER_MUSIC] SET biMusic_1Package")
						ELSE
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_GB_HEADHUNTER_MUSIC] TRYING TO TRIGGER BG_HEADHUNTER_STOP")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	*/
ENDPROC



PROC MAINTAIN_30S_COUNTDOWN_MUSIC(INT iTime)
	//EXIT
	
	
	//-- From spreadsheet
	//--  //depot/gta5/docs/Audio/Interactive_Music/DLC/Apartments_Magnate.xlsx
	
	IF IS_BIT_SET(iMusicBitset, biMusic_ShouldStop30s)
		STOP_30S_COUNTDOWN_AUDIO()
	//	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_30S_COUNTDOWN_MUSIC] EXIT AS biMusic_ShouldStop30s")
		EXIT 
	ENDIF
	
	IF NOT IS_BIT_SET(iMusicBitset, biMusic_DonePre30sStop)
		IF iTime <= 35000
			IF TRIGGER_MUSIC_EVENT("APT_PRE_COUNTDOWN_STOP")
				SET_BIT(iMusicBitset, biMusic_DonePre30sStop)
				SET_BIT(iMusicBitset, biMusic_Started30sCountdown)
				SET_AUDIO_FLAG("AllowScoreAndRadio", TRUE)
				SET_USER_RADIO_CONTROL_ENABLED(FALSE) 
				SET_AUDIO_FLAG("WantedMusicDisabled", TRUE)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_30S_COUNTDOWN_MUSIC] SET biMusic_DonePre30sStop")
			ELSE
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_30S_COUNTDOWN_MUSIC] TRYING TO TRIGGER APT_PRE_COUNTDOWN_STOP")
			ENDIF
		ENDIF
	ENDIF
	
	
	IF IS_BIT_SET(iMusicBitset, biMusic_Started30sCountdown)
		IF iTime <= 30000
			IF NOT IS_BIT_SET(iMusicBitset, biMusic_PrepareKill30s)
				IF PREPARE_MUSIC_EVENT("APT_COUNTDOWN_30S_KILL")
//					SET_AUDIO_FLAG("AllowScoreAndRadio", TRUE)
//					SET_USER_RADIO_CONTROL_ENABLED(FALSE) 
//					SET_AUDIO_FLAG("WantedMusicDisabled", TRUE)
					SET_BIT(iMusicBitset, biMusic_PrepareKill30s)
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_30S_COUNTDOWN_MUSIC] SET biMusic_PrepareKill30s")
				ELSE
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_30S_COUNTDOWN_MUSIC] TRYING TO PREPARE APT_COUNTDOWN_30S_KILL")
				ENDIF
			
			
			ELSE IS_BIT_SET(iMusicBitset, biMusic_PrepareKill30s)
				IF NOT IS_BIT_SET(iMusicBitset, biMusic_DoneStart30s)
					IF TRIGGER_MUSIC_EVENT("APT_COUNTDOWN_30S")
						SET_BIT(iMusicBitset, biMusic_DoneStart30s)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_30S_COUNTDOWN_MUSIC] SET biMusic_DoneStart30s")
					ELSE
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_30S_COUNTDOWN_MUSIC] TRYING TO TRIGGER APT_COUNTDOWN_30S")
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(iMusicBitset, biMusic_DoneStart30s)
				IF iTime <= 27000
					IF NOT IS_BIT_SET(iMusicBitset, biMusic_RestoreRadio)
						SET_USER_RADIO_CONTROL_ENABLED(TRUE)
						SET_AUDIO_FLAG("AllowScoreAndRadio", FALSE)
						SET_BIT(iMusicBitset, biMusic_RestoreRadio) 
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_30S_COUNTDOWN_MUSIC] SET biMusic_RestoreRadio")
					ENDIF
					
					IF iTime <= 500 
						IF TRIGGER_MUSIC_EVENT("APT_FADE_IN_RADIO")
							CANCEL_MUSIC_EVENT("APT_COUNTDOWN_30S_KILL")
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_30S_COUNTDOWN_MUSIC] TRIGGERED APT_FADE_IN_RADIO")
							CLEAR_BIT(iMusicBitset, biMusic_Started30sCountdown)
						ELSE
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_30S_COUNTDOWN_MUSIC] TRYING TO TRIGGER APT_FADE_IN_RADIO")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC



//Helper function to get the servers game/mission state
FUNC INT GET_SERVER_MISSION_STATE()
	RETURN serverBD.iServerGameState
ENDFUNC


///	PURPOSE: Need specific relationship groups for players taking part. Don't want AI to attack non-mission players
PROC SETUP_UW_REL_GROUPS()
	PRINTLN("     ---------->     KILL LIST - SETUP_UW_REL_GROUPS")
	
	ADD_RELATIONSHIP_GROUP("relHeadHunterPlayer", relHeadHunterPlayer)
//	ADD_RELATIONSHIP_GROUP("relHeadHunterAi", relHeadHunterAi)
	
	RELATIONSHIP_TYPE relWithCops = GET_RELATIONSHIP_BETWEEN_GROUPS(GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID()), RELGROUPHASH_COP)
	
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		
		
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, rgFM_Team[i], relHeadHunterPlayer)	
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relHeadHunterPlayer, rgFM_Team[i])
		
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, rgFM_Team[i], relHeadHunterAi)	
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relHeadHunterAi, rgFM_Team[i])
		
		SET_RELATIONSHIP_BETWEEN_GROUPS(relWithCops, relHeadHunterPlayer, RELGROUPHASH_COP)
		SET_RELATIONSHIP_BETWEEN_GROUPS(relWithCops, RELGROUPHASH_COP, relHeadHunterPlayer)
		
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relHeadHunterPlayer, rgFM_AiAmbientGangMerc[5])
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, rgFM_AiAmbientGangMerc[5], relHeadHunterPlayer)
		
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relHeadHunterPlayer, rgFM_AiLike)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, rgFM_AiLike, relHeadHunterPlayer)
		
	ENDREPEAT
	
	//Hate UW players
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relHeadHunterPlayer, relHeadHunterAi)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relHeadHunterAi, relHeadHunterPlayer)
	
	// Like Cops
//	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, rgFM_Team[i], rgFM_AiLikePlyrLikeCops)
//	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, rgFM_AiLikePlyrLikeCops, rgFM_Team[i])
//	//Dislike Player Like Cops
//	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_DISLIKE, rgFM_Team[i], rgFM_AiDislikePlyrLikeCops)
//	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_DISLIKE, rgFM_AiDislikePlyrLikeCops, rgFM_Team[i])
//	//Hate Player Like Cops
//	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, rgFM_Team[i], rgFM_AiHatePlyrLikeCops)
//	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, rgFM_AiHatePlyrLikeCops, rgFM_Team[i])
	
	//SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, rgFM_Team[i], rgFM_AiHatedByCopsAndMercs)
	

	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_AMBIENT_GANG_CULT, relHeadHunterAi)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE , RELGROUPHASH_AMBIENT_GANG_CULT, relHeadHunterPlayer)

	
	
	//-- Cops
	SET_AMBIENT_COP_REL_TO_THIS_RELGROUP(ACQUAINTANCE_TYPE_PED_LIKE, relHeadHunterAi)
	
	//**AMBIENT GANGS
	SET_AMBIENT_GANG_LIKES_THIS_RELGROUP(relHeadHunterAi)
	SET_AMBIENT_GANG_LIKES_THIS_RELGROUP(relHeadHunterPlayer)
	
	
ENDPROC




PROC REMOVE_UW_REL_GROUPS()
//	REMOVE_RELATIONSHIP_GROUP(relHeadHunterAi)
	REMOVE_RELATIONSHIP_GROUP(relHeadHunterPlayer)
ENDPROC

PROC RESET_ALL_PLAYER_BLIPS()
	INT i
	IF NOT IS_BIT_SET(iBoolsBitSet, biL_ResetPlayerBlips)
		
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER [RESET_ALL_PLAYER_BLIPS] SET biL_ResetPlayerBlips")
		SET_BIT(iBoolsBitSet, biL_ResetPlayerBlips)
		
		REPEAT NUM_NETWORK_PLAYERS i
			PLAYER_INDEX PlayerId = INT_TO_PLAYERINDEX(i)
			IF NETWORK_IS_PLAYER_ACTIVE(PlayerId)
				IF PlayerId != PLAYER_ID()
					IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iCustomBlipBitset, i)
						SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(PlayerId, RADAR_TRACE_TEMP_4, FALSE)
						
						SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(PlayerId, GET_BLIP_COLOUR_FROM_HUD_COLOUR(hcLaunchingGang) , FALSE)
						
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

PROC CLEANUP_HEADHUNTER_SPAWNING()
	IF IS_BIT_SET(iBoolsBitSet, biL_SetupSpawnCoord)
		CLEAR_PLAYER_NEXT_RESPAWN_LOCATION()
		CLEAR_SPECIFIC_SPAWN_LOCATION_FROM_THIS_SCRIPT()
		
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER [CLEANUP_HEADHUNTER_SPAWNING] CALLED")
		
		CLEAR_BIT(iBoolsBitSet, biL_SetupSpawnCoord)
	ENDIF
ENDPROC

PROC CLEANUP_ASSETS()
	INT i
	
	REPEAT MAX_TARGETS i
		IF DOES_BLIP_EXIST(blipTarget[i])
			REMOVE_BLIP(blipTarget[i])
		ENDIF
	ENDREPEAT
	
	IF IS_BIT_SET(iBoolsBitSet, biL_SetFriendlyFire)
		SET_PLAYER_DEFAULT_FRIENDLY_FIRE_OPTION()
		CLEAR_BIT(iBoolsBitSet, biL_SetFriendlyFire)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [CLEANUP_ASSETS] SET_PLAYER_DEFAULT_FRIENDLY_FIRE_OPTION")
	ENDIF
	CLEANUP_HEADHUNTER_SPAWNING()
	
	RESET_ALL_PLAYER_BLIPS()
ENDPROC



//Cleanup Mission Data
PROC SCRIPT_CLEANUP()
	
	#IF IS_DEBUG_BUILD
	IF NOT IS_PLAYER_SCTV(PLAYER_ID())
		IF serverBD.bSomeoneSPassed
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		ENDIF
	ENDIF
	#ENDIF
	
	IF NOT IS_PLAYER_SCTV(PLAYER_ID())
		IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneEndTelemetry)
			IF NETWORK_IS_GAME_IN_PROGRESS()
				IF DID_MY_BOSS_LAUNCH_GB_HEADHUNTER()
				OR DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
					GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LEFT)
					SET_BIT(iBoolsBitSet, biL_DoneEndTelemetry)
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER -  CLEANUP MISSION - TELEMETRY SETTING GB_TELEMETRY_END_LEFT AS NOTHING SET YET")
				ELSE
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER -  CLEANUP - DIDN'T SEND TELEMETRY AS DID_MY_BOSS_LAUNCH_GB_ASSAULT / DID_LOCAL_PLAYER_JOIN_AS_RIVAL")
				ENDIF
			ENDIF
		ENDIF
	
		INT iOptional0 = serverBD.iNumTargetsKilled
		INT iOptional1 = serverBD.iMapArea
		INT iOptional2 = -1
		IF NETWORK_IS_GAME_IN_PROGRESS()
			iOptional2 = playerBD[PARTICIPANT_ID_TO_INT()].iMyTotalKills
		ENDIF
		
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER -  CLEANUP - PROCESS_CURRENT_BOSS_WORK_PLAYSTATS Called with...")
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER -  CLEANUP - iOptional0 = ", iOptional0)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER -  CLEANUP - iOptional1 = ", iOptional1)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER -  CLEANUP - iOptional2 = ", iOptional2)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER -  CLEANUP - ")
		PROCESS_CURRENT_BOSS_WORK_PLAYSTATS(iOptional0, iOptional1, iOptional2)
	


		IF NETWORK_IS_GAME_IN_PROGRESS()
			IF GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID()) = relHeadHunterPlayer
				SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(), relMyFmGroup)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER -  CLEANUP MISSION - RESTORED MY FM REL GROUP     <----------     ") NET_NL()
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iBoolsBitSet, biL_SetFriendlyFire)
		SET_PLAYER_DEFAULT_FRIENDLY_FIRE_OPTION()
		CLEAR_BIT(iBoolsBitSet, biL_SetFriendlyFire)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - CLEANUP MISSION SET_PLAYER_DEFAULT_FRIENDLY_FIRE_OPTION")
	ENDIF
	
	REMOVE_UW_REL_GROUPS()
	
	IF serverBD.iBossPlayerWhoLaunched != -1
		IF IS_BIT_SET(iBoolsBitSet, biL_BlippedRival)
			IF NETWORK_IS_PLAYER_ACTIVE(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched))
//				
//				SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched), RADAR_TRACE_TEMP_4, FALSE)
//				SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched), BLIP_COLOUR_RED, FALSE)
//				FORCE_BLIP_PLAYER(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched), FALSE, FALSE)
//				SET_PLAYER_BLIP_AS_LONG_RANGE(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched), FALSE) 
//				SET_FIXED_BLIP_SCALE_FOR_PLAYER(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched), g_sMPTunables.fgangboss_Job_blip_scale, FALSE)
//				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER -  CLEANUP MISSION - biL_BlippedRival WAS SET, RESET BLIP")
			ENDIF
		ENDIF
	ENDIF
	
	IF GB_IS_GLOBAL_NON_BD_BIT0_SET(eGB_GLOBAL_NON_BD_BITSET_0_BLOCK_DPAD_DOWN_LB)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER -  CLEANUP MISSION - CLEAR eGB_GLOBAL_NON_BD_BITSET_0_BLOCK_DPAD_DOWN_LB")
		GB_CLEAR_GLOBAL_NON_BD_BIT0(eGB_GLOBAL_NON_BD_BITSET_0_BLOCK_DPAD_DOWN_LB)
	ENDIF
	
	//-- Reset music
//	IF IS_BIT_SET(iMusicBitset, biMusic_Init)
//		IF NOT IS_BIT_SET(iMusicBitset, biMusic_Reset)
//			SET_AUDIO_FLAG("DisableFlightMusic", FALSE) 
//			SET_AUDIO_FLAG("WantedMusicDisabled", FALSE)
//			
//			TRIGGER_MUSIC_EVENT("BG_HEADHUNTER_STOP")
//			
//			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER -  CLEANUP MISSION - SET biMusic_Reset")
//		ENDIF
//	ENDIF


	
	//-- Cleanup scenario blocking areas
	INT iScen
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			REPEAT MAX_SCENARIO_BLOCKING_AREAS iScen
				IF IS_BIT_SET(serverBD.iScenBlockBitset, iScen)
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER -  CLEANUP MISSION - REMOVING SCENARIO BLOCKING AREA ", iScen)
					REMOVE_SCENARIO_BLOCKING_AREA(serverBD.scenBlockingArea[iScen], TRUE)
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iBoolsBitSet, biL_SetMaxWantedLevel)
		SET_MAX_WANTED_LEVEL(iMyMaxWantedLevel)
		CLEAR_BIT(iBoolsBitSet, biL_SetMaxWantedLevel)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER -  CLEANUP MISSION restored my max wanted level = ", iMyMaxWantedLevel) 
	ENDIF
	CLEANUP_HEADHUNTER_SPAWNING()
	
	SET_BIT(iMusicBitset, biMusic_ShouldStop30s)
	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER -  CLEANUP MISSION - SET biMusic_ShouldStop30s")
	STOP_30S_COUNTDOWN_AUDIO()
	SET_LOCAL_PLAYER_CAN_COLLECT_PORTABLE_PICKUPS(TRUE)
	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER -  CLEANUP MISSION - SET_LOCAL_PLAYER_CAN_COLLECT_PORTABLE_PICKUPS(TRUE)")
	GB_SET_LOCAL_PLAYER_CRITICAL_TO_JOB(FALSE)
	
	GB_COMMON_BOSS_MISSION_CLEANUP()
	
	SET_MAX_WANTED_LEVEL(5)
	
	RESET_ALL_PLAYER_BLIPS()
	
	
	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER -  CLEANUP MISSION      <----------     ") NET_NL()
	#IF IS_DEBUG_BUILD NET_LOG("SCRIPT_CLEANUP")	#ENDIF
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC





//PURPOSE: Returns the Cash reward for completing the objective
FUNC INT GET_CASH_REWARD()
	RETURN g_sMPTunables.iUrbanWarfareCashReward
ENDFUNC

//Do necessary pre game start ini.
PROC PROCESS_PRE_GAME( MP_MISSION_DATA missionScriptArgs )
	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER -  																						 ")
	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - ******************************************************************************************")
	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - *																						*")
	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - *						MISSION START													*")
	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - *																						*")
	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - ******************************************************************************************")
	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER -  																						 ")
	
	// This marks the script as a net script, and handles any instancing setup. 
	SETUP_MP_MISSION_FOR_NETWORK(GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(missionScriptArgs.mdID.idMission) ,  missionScriptArgs)	//NUM_NETWORK_PLAYERS, missionScriptArgs)	//
	//NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE, PARTICIPANT_ID_TO_INT())  
	
	//Reserve Entities
	RESERVE_NETWORK_MISSION_PEDS(MAX_TARGETS + (MAX_BODYGUARDS_PER_TARGET * MAX_TARGETS))
	RESERVE_NETWORK_MISSION_VEHICLES(2 + (MAX_AMB_VEH_PER_TARGET * 2)) // 2 in-vehicle targets, plus at most MAX_AMB_VEH_PER_TARGET vehicles for both on-foot targets
	
	//RESERVE_NETWORK_MISSION_OBJECTS(MAX_PACKAGES)
	
	// This makes sure the net script is active, waits until it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
	
	// KGM: Wait for the first network broadcast before moving on
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		#IF IS_DEBUG_BUILD NET_LOG("HEADHUNTER -  FAILED TO RECEIVE INITIAL NETWORK BROADCAST")	#ENDIF
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER -  MISSION END - FAILED TO RECEIVE INITIAL NETWORK BROADCAST - SCRIPT CLEANUP D     <----------     ") NET_NL()
		SCRIPT_CLEANUP()
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		// This script will not be paused if another script calls PAUSE_GAME
		SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			#IF IS_DEBUG_BUILD
			serverBD.iDebugVariation = GB_GET_PLAYER_GANG_BOSS_MISSION_VARIATION()
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - serverBD.iDebugVariation = ", serverBD.iDebugVariation)
			#ENDIF
		ENDIF
		
	//	SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_HEADHUNTER, TRUE)
		
		SETUP_UW_REL_GROUPS()
		
		GB_COMMON_BOSS_MISSION_PREGAME()		
     

		playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_INI
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - TUNABLES...")
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - TUNABLES... g_sMPTunables.iexec_headhunter_time_limit                						= ", g_sMPTunables.iexec_headhunter_time_limit                )
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - TUNABLES... g_sMPTunables.iexec_headhunter_1st_target_wanted         						= ", g_sMPTunables.iexec_headhunter_1st_target_wanted         )
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - TUNABLES... g_sMPTunables.iexec_headhunter_2nd_target_wanted         						= ", g_sMPTunables.iexec_headhunter_2nd_target_wanted         )
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - TUNABLES... g_sMPTunables.iexec_headhunter_3rd_target_wanted         						= ", g_sMPTunables.iexec_headhunter_3rd_target_wanted         )
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - TUNABLES... g_sMPTunables.bexec_headhunter_wanted_persists_death     						= ", g_sMPTunables.bexec_headhunter_wanted_persists_death     )
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - TUNABLES... g_sMPTunables.iexec_headhunter_target_health             						= ", g_sMPTunables.iexec_headhunter_target_health             )
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - TUNABLES... g_sMPTunables.fexec_headhunter_target_accuracy           						= ", g_sMPTunables.fexec_headhunter_target_accuracy           )
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - TUNABLES... g_sMPTunables.iexec_headhunter_security_health           						= ", g_sMPTunables.iexec_headhunter_security_health           )
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - TUNABLES... g_sMPTunables.fexec_headhunter_security_accuracy         						= ", g_sMPTunables.fexec_headhunter_security_accuracy         )
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - TUNABLES... g_sMPTunables.iexec_headhunter_req_players               						= ", g_sMPTunables.iexec_headhunter_req_players               )
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - TUNABLES... g_sMPTunables.iexec_headhunter_cooldown                  						= ", g_sMPTunables.iexec_headhunter_cooldown                  )
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - TUNABLES... g_sMPTunables.iexec_headhunter_cash_per_target           						= ", g_sMPTunables.iexec_headhunter_cash_per_target           )
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - TUNABLES... g_sMPTunables.iexec_headhunter_rp_per_target             						= ", g_sMPTunables.iexec_headhunter_rp_per_target             )
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - TUNABLES... g_sMPTunables.iEXEC1_HEADHUNTER_CASH_REWARD_TARGETS_DEFENDED             		= ", g_sMPTunables.iEXEC1_HEADHUNTER_CASH_REWARD_TARGETS_DEFENDED )            
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - TUNABLES... g_sMPTunables.iEXEC1_HEADHUNTER_RP_REWARD_TARGETS_DEFENDED            			= ", g_sMPTunables.iEXEC1_HEADHUNTER_RP_REWARD_TARGETS_DEFENDED)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - TUNABLES... g_sMPTunables.iexec_headhunter_minimum_participation_cash						= ", g_sMPTunables.iexec_headhunter_minimum_participation_cash)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - TUNABLES... g_sMPTunables.iexec_headhunter_minimum_participation_rp  						= ", g_sMPTunables.iexec_headhunter_minimum_participation_rp  )

//		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - TUNABLES... g_sMPTunables.iGB_HEADHUNTER_time_limit						= ", g_sMPTunables.iGB_HEADHUNTER_time_limit)
//		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - TUNABLES... g_sMPTunables.iGB_HEADHUNTER_number_of_packages				= ", g_sMPTunables.iGB_HEADHUNTER_number_of_packages)
//		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - TUNABLES... g_sMPTunables.iGB_HEADHUNTER_minimum_distance_to_next_package	= ", g_sMPTunables.iGB_HEADHUNTER_minimum_distance_to_next_package)
//		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - TUNABLES... g_sMPTunables.bGB_HEADHUNTER_disable_quadrant_northeast		= ", g_sMPTunables.bGB_HEADHUNTER_disable_quadrant_northeast)
//		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - TUNABLES... g_sMPTunables.bGB_HEADHUNTER_disable_quadrant_northwest		= ", g_sMPTunables.bGB_HEADHUNTER_disable_quadrant_northwest)
//		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - TUNABLES... g_sMPTunables.bGB_HEADHUNTER_disable_quadrant_southeast		= ", g_sMPTunables.bGB_HEADHUNTER_disable_quadrant_southeast)
//		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - TUNABLES... g_sMPTunables.bGB_HEADHUNTER_disable_quadrant_southwest		= ", g_sMPTunables.bGB_HEADHUNTER_disable_quadrant_southwest)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER")
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER -  PRE_GAME DONE     <----------     ") NET_NL()
	ELSE
		#IF IS_DEBUG_BUILD NET_LOG("HEADHUNTER -  NETWORK_IS_GAME_IN_PROGRESS = FALSE")	#ENDIF
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER -  MISSION END - NETWORK_IS_GAME_IN_PROGRESS = FALSE - SCRIPT CLEANUP Q     <----------     ") NET_NL()
		SCRIPT_CLEANUP()
	ENDIF
ENDPROC


PROC MOVE_TO_EXPLODE_VEHICLE_STAGE()

ENDPROC				

FUNC BOOL ARE_ALL_PEDS_CLEANED_UP()
	
	
	RETURN TRUE
ENDFUNC

FUNC BOOL MISSION_END_CHECK()

	

	
	RETURN FALSE
ENDFUNC

//Server only function that checks to see if the game mode should now end. For this test script, it if time has passed.
FUNC BOOL HAVE_MISSION_END_CONDITIONS_BEEN_MET()
	
	#IF IS_DEBUG_BUILD
		IF bHostEndMissionNow
			RETURN TRUE
		ENDIF
		
		IF ServerBD.bSomeoneSPassed
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [HAVE_MISSION_END_CONDITIONS_BEEN_MET] TRUE AS bSomeoneSPassed")
			RETURN TRUE
		ENDIF
		
	#ENDIF	
	
	IF MISSION_END_CHECK()
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_AllPlayersFinished)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC




//Helper function to get a clients game/mission state
FUNC INT GET_CLIENT_MISSION_STATE(INT iPlayer)
	RETURN playerBD[iPlayer].iGameState
ENDFUNC

#IF IS_DEBUG_BUILD
PROC CREATE_WIDGETS()
	INT iPlayer
	TEXT_LABEL_15 tl63

	START_WIDGET_GROUP("GB_HEADHUNTER")  
		
		
		
		// Gameplay debug, sever only.
		START_WIDGET_GROUP("Server Only Gameplay") 

			ADD_WIDGET_BOOL("End mission", bHostEndMissionNow)			
		STOP_WIDGET_GROUP()		
		
		// Data about the server
		START_WIDGET_GROUP("Server BD") 
			ADD_WIDGET_INT_SLIDER("S. Game state", serverBD.iServerGameState,-1, HIGHEST_INT,1)

		STOP_WIDGET_GROUP()	

		// Data about the clients. * = You.
		START_WIDGET_GROUP("Client BD")  				
			REPEAT NUM_NETWORK_PLAYERS iPlayer
				tl63 = "Player "
				tl63 += iPlayer
				IF iPlayer = PARTICIPANT_ID_TO_INT()
					tl63 += "*"
				ENDIF
				START_WIDGET_GROUP(tl63)
					ADD_WIDGET_INT_SLIDER("Game state", playerBD[iPlayer].iGameState,-1, HIGHEST_INT,1)
				STOP_WIDGET_GROUP()
			ENDREPEAT				
		STOP_WIDGET_GROUP()	
		
	STOP_WIDGET_GROUP()
ENDPROC		

PROC UPDATE_WIDGETS()
	
	
	
ENDPROC

VECTOR vWarpCoordsForJskip
FUNC BOOL HANDLE_J_SKIP()
	INT iClosest = -1
	GET_VDIST2_TO_NEAREST_TARGET(iClosest)

	IF iClosest >= 0
		IF ARE_VECTORS_EQUAL(vWarpCoordsForJskip, << 0.0, 0.0, 0.0>>)
			vWarpCoordsForJskip = GET_ENTITY_COORDS(NET_TO_PED(serverBD.Targets[iClosest].niPed), FALSE)
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [HANDLE_J_SKIP] WARPING TO ", vWarpCoordsForJskip, " FOR TARGET ", iClosest)
			
			IF NOT ARE_VECTORS_EQUAL(vWarpCoordsForJskip, << 0.0, 0.0, 0.0>>)
				J_SKIP_MP(vWarpCoordsForJskip, << 20.0, 20.0, 20.0>>)
			ENDIF
		ENDIF
		
		IF NOT ARE_VECTORS_EQUAL(vWarpCoordsForJskip, << 0.0, 0.0, 0.0>>)
			
			MAINTAIN_J_SKIP_WARP()
			IF NOT g_bDoJSkip
				vWarpCoordsForJskip = << 0.0, 0.0, 0.0 >>
				RETURN TRUE
			ELSE
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [HANDLE_J_SKIP] WAITING FOR g_bDoJSkip")
			ENDIF
		
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_DEBUG_KEYS()
	SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerData
	
	IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPDebugBitset, biPDebug_PressedS)
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S) 
			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPDebugBitset, biPDebug_PressedS)
			TickerData.TickerEvent = TICKER_EVENT_PLAYER_CHEATED_SPASS
			TickerData.playerID = PLAYER_ID()
			BROADCAST_TICKER_EVENT(TickerData, ALL_PLAYERS_ON_SCRIPT(TRUE))
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_DEBUG_KEYS] I S-PASSED ")
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPDebugBitset, biPDebug_PressedF)
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F) 
			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPDebugBitset, biPDebug_PressedF)
			TickerData.TickerEvent = TICKER_EVENT_PLAYER_CHEATED_FFAIL
			TickerData.playerID = PLAYER_ID()
			BROADCAST_TICKER_EVENT(TickerData, ALL_PLAYERS_ON_SCRIPT(TRUE))
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_DEBUG_KEYS] I F-FAILED ")
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPDebugBitset, biPDebug_PressedJ)
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J) 
			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPDebugBitset, biPDebug_PressedJ)
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_DEBUG_KEYS] I J-SKIPPED ")
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPDebugBitset, biPDebug_PressedJ)
		IF HANDLE_J_SKIP()
			CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPDebugBitset, biPDebug_PressedJ)
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_DEBUG_KEYS] I J-SKIP FINISHED ")
		ENDIF
	ENDIF
	
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_DOWN)
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.Targets[2].niVeh)
			IF IS_NET_VEHICLE_DRIVEABLE(serverBD.Targets[2].niVeh)
			
				SET_ENTITY_ROTATION(NET_TO_VEH(serverBD.Targets[2].niVeh), << 0.0, 180.0, 0.0 >>)
			ENDIF
		ENDIF
		
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.Targets[3].niVeh)
			IF IS_NET_VEHICLE_DRIVEABLE(serverBD.Targets[3].niVeh)
			
				SET_ENTITY_ROTATION(NET_TO_VEH(serverBD.Targets[3].niVeh), << 0.0, 180.0, 0.0 >>)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

#ENDIF

FUNC STRING GET_VEHICLE_TYPE_STRING()
	RETURN "" //GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(serverBD.VehModel)
ENDFUNC

FUNC STRING GET_VEH_BLIP_STRING()
//	IF serverBD.VehModel = RHINO
//		RETURN "ABLIP_TANK"			//"~BLIP_TEMP_6~"
//	ELIF IS_THIS_MODEL_A_PLANE(serverBD.VehModel)
//		RETURN "ABLIP_PLANE"		//"~BLIP_TEMP_6~"
//	ENDIF
//	
//	RETURN "ABLIP_HELI"				//"~BLIP_TEMP_6~"*/
	RETURN ""
ENDFUNC

//PURPOSE: Adds a blip to the Veh
PROC ADD_VEH_BLIP()

ENDPROC

PROC REMOVE_VEHICLE_BLIP()

ENDPROC



//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  ENTITY CREATION                  /////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////
///    			VEHICLE   				///
///////////////////////////////////////////
//PURPOSE: Create the Veh
FUNC BOOL CREATE_VEH()

	
	RETURN TRUE
ENDFUNC

//PURPOSE: Creates the Veh and Driver that will deliver the Vehicle
FUNC BOOL CREATE_MAIN_VEHICLE()

	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Determine if the participant iPart thinks any of the targets is stuck / damaged / being aimed at etc.
PROC SERVER_PART_PROCESS_TARGETS(INT iPart)
	
	#IF IS_DEBUG_BUILD
	PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart))
	#ENDIF
	INT iTarget
	
	REPEAT MAX_TARGETS iTarget
		
		IF NOT IS_BIT_SET(ServerBD.iTargetVehDamagedBitSet, iTarget)
			IF IS_BIT_SET(playerBD[iPart].iPlayerTargetVehDamagedBitSet, iTarget)
				SET_BIT(ServerBD.iTargetVehDamagedBitSet, iTarget)
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] Server thinks this player ", GET_PLAYER_NAME(PlayerId), " damaged this target ", iTarget)
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(ServerBD.iTargetAimedAtBitSet, iTarget)
			IF IS_BIT_SET(playerBD[iPart].iPlayerAimAtTargetBitSet, iTarget)
				SET_BIT(ServerBD.iTargetAimedAtBitSet, iTarget)
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] Server thinks this player ", GET_PLAYER_NAME(PlayerId), " is aiming at target ", iTarget)
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(ServerBD.iTargetVehStuckBitSet, iTarget)
			IF IS_BIT_SET(playerBD[iPart].iPlayerTargetVehStuckBitSet, iTarget)
				SET_BIT(ServerBD.iTargetVehStuckBitSet, iTarget)
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] Server thinks this player ", GET_PLAYER_NAME(PlayerId), " thinks this target is stuck target ", iTarget)
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(ServerBD.iTargetNearbyShotsBitset, iTarget)
			IF IS_BIT_SET(playerBD[iPart].iPlayerShotsNearTargetBitSet, iTarget)
				SET_BIT(ServerBD.iTargetNearbyShotsBitset, iTarget)
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] Server thinks this player ", GET_PLAYER_NAME(PlayerId), " detected shots near target ", iTarget)
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(serverBD.iTargetKilledBitset, iTarget)
			IF IS_BIT_SET(playerBD[iPart].iPlayerKilledTargetBitset, iTarget)
				SET_BIT(serverBD.iTargetKilledBitset, iTarget)
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] Server thinks this player ", GET_PLAYER_NAME(PlayerId), " killed target ", iTarget)
			ELIF IS_BIT_SET(playerBD[iPart].iNonPartKilledTargetBitset, iTarget) 
				SET_BIT(serverBD.iTargetKilledBitset, iTarget)
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] Server thinks this player ", GET_PLAYER_NAME(PlayerId), " thinks this target ", iTarget, " killed by non-player / non-part")
			ENDIF
		ENDIF
		
		IF serverBD.Targets[iTarget].iTargetType = TARGET_TYPE_ON_FOOT_WITH_BODYGUARDS
			IF NOT serverBD.Targets[iTarget].bShouldSeekCover
				IF IS_BIT_SET(playerBD[iPart].iOnFootTargetPedShouldSeekCover, iTarget)
					serverBD.Targets[iTarget].bShouldSeekCover = TRUE
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] Server thinks this player ", GET_PLAYER_NAME(PlayerId), " THINKS THIS TARGET ", iTarget, " SHOULD SEEK COVER")
				ENDIF
			ENDIF
			
			IF NOT serverBD.Targets[iTarget].bShouldEnterCombat
				IF IS_BIT_SET(playerBD[iPart].iOnFootTargetPedShouldEnterCombat, iTarget)
					serverBD.Targets[iTarget].bShouldEnterCombat = TRUE
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] Server thinks this player ", GET_PLAYER_NAME(PlayerId), " THINKS THIS TARGET ", iTarget, " SHOULD ENTER COMBAT")
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

FUNC BOOL HAS_IN_VEHICLE_TARGET_AND_BODYGUARDS_BEEN_KILLED(INT iTarget)
	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.Targets[iTarget].niPed)
		RETURN FALSE
	ENDIF
	
	IF NOT IS_NET_PED_INJURED(serverBD.Targets[iTarget].niPed)
		RETURN FALSE
	ENDIF
	
	INT i
	REPEAT MAX_BODYGUARDS_PER_TARGET i
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.Targets[iTarget].niBodyguardPed[i])
			IF NOT IS_NET_PED_INJURED(serverBD.Targets[iTarget].niBodyguardPed[i])
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

PROC MAINTAIN_LOCK_TARGET_DOORS_FOR_PART(INT iPart)
	INT i
	IF NOT IS_BIT_SET(serverBD.iLockDoorsBitset, iPart)
		REPEAT MAX_TARGETS i
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.Targets[i].niVeh)
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.Targets[i].niPed)
					IF NOT IS_NET_PED_INJURED(serverbd.Targets[i].niPed)
						IF IS_NET_VEHICLE_DRIVEABLE(serverBD.Targets[i].niVeh)
							SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(NET_TO_VEH(serverBD.Targets[i].niVeh), 
									NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart)), TRUE)
						
							SET_BIT(serverBD.iLockDoorsBitset, iPart)
							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] [MAINTAIN_LOCK_TARGET_DOORS_FOR_PART] LOCKED VEHICLE ", i, " DOORS FOR PLAYER ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart))))
						ENDIF
					ENDIF
				ENDIF
			ELSE
				SET_BIT(serverBD.iLockDoorsBitset, iPart)
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

PROC UNLOCK_TARGET_VEHICLE_DOORS_FOR_ALL_PARTICIPANTS(INT iTarget)
	INT i
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
			PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
			IF NOT IS_PLAYER_SCTV(PlayerId)
				SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(NET_TO_VEH(serverBD.Targets[iTarget].niVeh), 
									PlayerId, FALSE)
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - UNLOCK_TARGET_VEHICLE_DOORS_FOR_ALL_PARTICIPANTS UNLOCKED VEHICLE ", iTarget, " FOR PLAYER ", GET_PLAYER_NAME(PlayerId))
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC MAINTAIN_UNLOCK_TARGET_VEHICLE_DOORS()
	INT i
	REPEAT MAX_TARGETS i
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.Targets[i].niVeh)
			IF NOT IS_BIT_SET(serverBD.iUnlockedTargetDoorsBitset, i)
				IF HAS_IN_VEHICLE_TARGET_AND_BODYGUARDS_BEEN_KILLED(i)
					IF IS_NET_VEHICLE_DRIVEABLE(serverBD.Targets[i].niVeh)
						UNLOCK_TARGET_VEHICLE_DOORS_FOR_ALL_PARTICIPANTS(i)
						SET_BIT(serverBD.iUnlockedTargetDoorsBitset, i)
						
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [MAINTAIN_UNLOCK_TARGET_VEHICLE_DOORS SET BIT ", i)
					ENDIF
						
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT

ENDPROC

PROC MAINTAIN_TARGETS_KILLED_FAILSAFE()

	
	INT i
	REPEAT MAX_TARGETS i
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.Targets[i].niPed)
			IF NOT IS_BIT_SET(serverBD.iTargetKilledBitset, i)
				IF NOT HAS_NET_TIMER_STARTED(serverBD.timeFailsafe[i])
					IF IS_NET_PED_INJURED(serverBD.Targets[i].niPed)
						START_NET_TIMER(serverBD.timeFailsafe[i])
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_TARGETS_KILLED_FAILSAFE] started fail-safe timer for target ", i )
					ENDIF
				ELSE
					IF HAS_NET_TIMER_EXPIRED(serverBD.timeFailsafe[i], 10000)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_TARGETS_KILLED_FAILSAFE] Failsafe timer expired! Ped ", i)
						SET_BIT(serverBD.iTargetKilledBitset, i)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

//PURPOSE: Runs through the max num participents and does relevant checks
PROC MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER()
	INT iNumTargetsKilled = 0
	INT iTarget = 0
	BOOL bSomeoneNotFinished = FALSE
	//For now we only need to do this check once the Veh is destroyed.
	IF serverBD.iServerGameState != GAME_STATE_END
		REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iStaggeredParticipant
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iStaggeredParticipant))
				PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iStaggeredParticipant))
				IF NOT IS_PLAYER_SCTV(PlayerId)
					//PED_INDEX PlayerPedId = GET_PLAYER_PED(PlayerId)
					
					//** CHECKS THAT DON'T NEED THE PLAYER TO BE ALIVE **//
					
					
					//** CHECKS THAT REQUIRE THE PLAYER TO BE ALIVE **//
					IF IS_NET_PLAYER_OK(PlayerId)
						

					ENDIF
					
					MAINTAIN_LOCK_TARGET_DOORS_FOR_PART(iStaggeredParticipant)
					
					SERVER_PART_PROCESS_TARGETS(iStaggeredParticipant)
					
					//-- Everyone finished?
					IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_AllPlayersFinished)
						IF NOT bSomeoneNotFinished
							IF IS_BIT_SET(serverBD.iServerBitSet, biS_AllTargetsKilled)
							OR IS_BIT_SET(serverBD.iServerBitSet, biS_DurationExpired)
							OR IS_BIT_SET(serverBD.iServerBitSet, biS_BossLaunchedQuit)
								IF NOT IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_Finished)
									bSomeoneNotFinished = TRUE
									CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] SERVER THINKS THIS PLAYER NOT FINISHED ", GET_PLAYER_NAME(PlayerId))
								ENDIF
							ELSE
								bSomeoneNotFinished = TRUE
							ENDIF
						ENDIF
					ENDIF
					
					#IF IS_DEBUG_BUILD
					//-- Debug s-skip
					IF NOT ServerBD.bSomeoneSPassed
						IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPDebugBitset, biPDebug_PressedS)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] SERVER THINKS THIS PLAYER S-PASSED ", GET_PLAYER_NAME(PlayerId))
							ServerBD.bSomeoneSPassed = TRUE
						ENDIF
					ENDIF
					
					IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_DurationExpired)
					AND NOT IS_BIT_SET(serverBD.iServerBitSet, biS_AllTargetsKilled)
						IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPDebugBitset, biPDebug_PressedF)
							IF playerBD[iStaggeredParticipant].iMyBossPart = serverBD.iBossPartWhoLaunched
								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] SERVER THINKS THIS PLAYER F-FAILED ", GET_PLAYER_NAME(PlayerId), " will set biS_DurationExpired")
								SET_BIT(serverBD.iServerBitSet, biS_DurationExpired)
							ELSE
								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] SERVER THINKS THIS PLAYER F-FAILED ", GET_PLAYER_NAME(PlayerId), " will set biS_AllTargetsKilled")
								SET_BIT(serverBD.iServerBitSet, biS_AllTargetsKilled)
							ENDIF
						ENDIF
					ENDIF
					#ENDIF
				ENDIF
			ELSE
				//-- Part not active
				
				//-- CHeck for Boss who launched the job quiting the session
				IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_BossLaunchedQuit)
					IF serverBD.iBossPartWhoLaunched > -1
						IF iStaggeredParticipant = serverBD.iBossPartWhoLaunched
							SET_BIT(serverBD.iServerBitSet, biS_BossLaunchedQuit)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] SERVER THINKS BOSS WHO LAUNCHED WORK QUIT SESSION erverBD.iBossPartWhoLaunched = ", serverBD.iBossPartWhoLaunched)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		MAINTAIN_TARGETS_KILLED_FAILSAFE()
		
		REPEAT MAX_TARGETS iTarget
			IF IS_BIT_SET(serverBD.iTargetKilledBitset, iTarget)
				iNumTargetsKilled++
			ENDIF
		ENDREPEAT
		
		//-- Number of targets killed
		IF serverBD.iNumTargetsKilled != iNumTargetsKilled
			serverBD.iNumTargetsKilled = iNumTargetsKilled
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] serverBD.iNumTargetsKilled UPDATED TO ", serverBD.iNumTargetsKilled)
		ENDIF
		
		
		//-- All targets killed?
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_AllTargetsKilled)
			IF iNumTargetsKilled >= MAX_TARGETS
				SET_BIT(serverBD.iServerBitSet, biS_AllTargetsKilled)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] SET biS_AllTargetsKilled AS iNumTargetsKilled = ", iNumTargetsKilled)
			ENDIF
		ENDIF
	
		
		//--All finished?
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_AllPlayersFinished)
			IF NOT bSomeoneNotFinished
				SET_BIT(serverBD.iServerBitSet, biS_AllPlayersFinished)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] SET biS_AllPlayersFinished")
			ENDIF
		ENDIF
		
	ENDIF
ENDPROC


PROC DISABLE_PLAYER_MISSION_VEHICLE_CONTROLS_THIS_FRAME(BOOL bDisableExit = TRUE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BRAKE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HANDBRAKE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_LR)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_UD)
	IF bDisableExit
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
	ENDIF
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HORN)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_NEXT_RADIO)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PREV_RADIO)
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_ROLL_LR)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_PITCH_UD)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_YAW_LEFT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_YAW_LEFT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_THROTTLE_UP)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_THROTTLE_DOWN)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_UNDERCARRIAGE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_NEXT_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_PREV_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_TARGET_LEFT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_TARGET_RIGHT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_VERTICAL_FLIGHT_MODE)
ENDPROC

PROC STOP_PLAYER_LEAVING_VEHICLE_THIS_FRAME()
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
	ENDIF
ENDPROC



FUNC BOOL HAS_ANY_MISSION_END_CONDITION_BEEN_MET()
	RETURN ( IS_BIT_SET(serverBD.iServerBitSet, biS_AllTargetsKilled)
	OR IS_BIT_SET(serverBD.iServerBitSet, biS_DurationExpired)
	OR IS_BIT_SET(serverBD.iServerBitSet, biS_BossLaunchedQuit))	
ENDFUNC

FUNC INT GET_CASH_REWARD_FOR_NUM_TARGETS_KILLED()
	RETURN serverBD.iNumTargetsKilled * g_sMPTunables.iexec_headhunter_cash_per_target   // TODO. Add cash tunable here
ENDFUNC

FUNC INT GET_RP_REWARD_FOR_NUM_TARGETS_KILLED()
	RETURN serverBD.iNumTargetsKilled * g_sMPTunables.iexec_headhunter_rp_per_target   // TODO. Add  RP tunable here
ENDFUNC

FUNC INT GET_CASH_REWARD_FOR_NUM_TARGETS_DEFENDED()
	RETURN (MAX_TARGETS - serverBD.iNumTargetsKilled) * g_sMPTunables.iEXEC1_HEADHUNTER_CASH_REWARD_TARGETS_DEFENDED // TODO. Add cash tunable here
ENDFUNC

FUNC INT GET_RP_REWARD_FOR_NUM_TARGETS_DEFENDED()
	RETURN (MAX_TARGETS - serverBD.iNumTargetsKilled) * g_sMPTunables.iEXEC1_HEADHUNTER_RP_REWARD_TARGETS_DEFENDED // TODO. Add RP tunable here
ENDFUNC

PROC UPDATE_GLOBAL_RELATIONSHIP_GROUP_FOR_MISSION_END()
	PLAYER_INDEX playerBoss
	IF serverBD.iBossPlayerWhoLaunched != -1
		playerBoss = INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER [UPDATE_GLOBAL_RELATIONSHIP_GROUP_FOR_MISSION_END] - Think this player launched ", GET_PLAYER_NAME(playerBoss))
	ENDIF
	INT i
	
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
			PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
			IF NOT IS_PLAYER_SCTV(PlayerId)
				IF playerBoss != INVALID_PLAYER_INDEX()
					IF PlayerId = playerBoss
					OR GB_IS_PLAYER_MEMBER_OF_THIS_GANG(PlayerId, playerBoss)
						SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, rgFM_Team[i], relHeadHunterAi)	
						SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relHeadHunterAi, rgFM_Team[i])
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER Setting relHeadHunterAi tp hate player ", GET_PLAYER_NAME(playerBoss))
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
					
					
	
ENDPROC

//PURPOSE: Checks to see if this is the end and the player should be rewarded.
PROC CHECK_FOR_REWARD()
//	BOOL bEventCompleted
	GANG_BOSS_MANAGE_REWARDS_DATA gbRewards
	
	INT iPart = PARTICIPANT_ID_TO_INT()
	PLAYER_INDEX player= PLAYER_ID()
	
	IF IS_PLAYER_SCTV(PLAYER_ID())
		iPart = GET_MY_SPEC_TARGET_AS_PARTICIPANT_INT()
		player = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget
	ENDIF
	
	
	STRING sOrganization
	HUD_COLOURS hclPlayer
	
	IF NOT IS_BIT_SET(playerBD[iPart].iPlayerBitSet, biP_Finished)
		IF NOT IS_BIT_SET(iBoolsBitSet, biL_WaitForShard)
			IF IS_BIT_SET(serverBD.iServerBitSet, biS_AllTargetsKilled)
				//-- All targets killed
				IF iPart = serverBD.iBossPartWhoLaunched
				OR GET_MY_BOSS_PARTICPANT_ID() = serverBD.iBossPartWhoLaunched
					//-- I'm the boss who launched event
					
					IF GB_GET_PLAYER_UI_LEVEL(player) >= GB_UI_LEVEL_MINIMAL
						sOrganization =GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(player)
						hclPlayer = GB_GET_PLAYER_GANG_HUD_COLOUR(player)
						SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS, "GB_WINNER", "BIGM_HUNTD", sOrganization, hclPlayer) 
					ENDIF
					
					IF player = PLAYER_ID()
						IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneEndTelemetry)
							
							GB_SET_COMMON_TELEMETRY_DATA_ON_END(TRUE, GB_TELEMETRY_END_WON)
							
							SET_BIT(iBoolsBitSet, biL_DoneEndTelemetry)
						ENDIF
						
						gbRewards.iExtraCash = GET_CASH_REWARD_FOR_NUM_TARGETS_KILLED()
						gbRewards.iExtraRp = GET_RP_REWARD_FOR_NUM_TARGETS_KILLED()
						GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_HEADHUNTER, TRUE, gbRewards)
						
						
						
						CLEAR_PLAYER_WANTED_LEVEL(player)
						iMyMaxWantedLevel = GET_MAX_WANTED_LEVEL() 
						SET_MAX_WANTED_LEVEL(0)
						SET_BIT(iBoolsBitSet, biL_SetMaxWantedLevel)
						START_NET_TIMER(timeNoWanted)
					ENDIF
					
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [CHECK_FOR_REWARD] MY GANG LAUNCHED AND ALL TARGETS WERE KILLED iMyMaxWantedLevel = ", iMyMaxWantedLevel)
				ELIF DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
					//-- I was on a rival gang
					IF GB_IS_PLAYER_MEMBER_OF_A_GANG(player)
						IF GB_GET_PLAYER_UI_LEVEL(player) >= GB_UI_LEVEL_MINIMAL
							sOrganization = GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched))
							hclPlayer = GB_GET_PLAYER_GANG_HUD_COLOUR(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched))
							SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "BIGM_HUNTD", sOrganization, hclPlayer) 
						ENDIF
						
						IF player = PLAYER_ID()
							IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneEndTelemetry)
								GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LOST)
								SET_BIT(iBoolsBitSet, biL_DoneEndTelemetry)
							ENDIF
							
							IF GB_IS_PLAYER_A_QUALIFYING_PARTICIPANT(player)
								gbRewards.iExtraCash = GET_CASH_REWARD_FOR_NUM_TARGETS_DEFENDED()
								gbRewards.iExtraRp = GET_RP_REWARD_FOR_NUM_TARGETS_DEFENDED()
							ENDIF
							GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_HEADHUNTER, FALSE, gbRewards)
						ENDIF
						
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [CHECK_FOR_REWARD] I WAS A RIVAL AND ALL TARGETS WERE KILLED")
					ELSE
						IF GB_GET_PLAYER_UI_LEVEL(player) >= GB_UI_LEVEL_MINIMAL
							sOrganization = GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched))
							hclPlayer = GB_GET_PLAYER_GANG_HUD_COLOUR(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched))
							SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "BIGM_HUNTD", sOrganization, hclPlayer) 
					
						ENDIF
						
						IF player = PLAYER_ID()
							IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneEndTelemetry)
								GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LOST)
								SET_BIT(iBoolsBitSet, biL_DoneEndTelemetry)
							ENDIF
							
							IF GB_IS_PLAYER_A_QUALIFYING_PARTICIPANT(player)
								gbRewards.iExtraCash = GET_CASH_REWARD_FOR_NUM_TARGETS_DEFENDED()
								gbRewards.iExtraRp = GET_RP_REWARD_FOR_NUM_TARGETS_DEFENDED()
							ENDIF
							GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_HEADHUNTER, FALSE, gbRewards)
						ENDIF
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [CHECK_FOR_REWARD] I'M NOT IN A GANG AND ALL TARGETS WERE KILLED")
					ENDIF
					
				ENDIF
				
				
				SET_BIT(iBoolsBitSet, biL_WaitForShard)	
			//	bEventCompleted = TRUE
			ELIF IS_BIT_SET(serverBD.iServerBitSet, biS_DurationExpired)
				//-- Mission duration expired
				INT numberStillAlive = MAX_TARGETS - serverBD.iNumTargetsKilled
				
				IF iPart = serverBD.iBossPartWhoLaunched
				OR GET_MY_BOSS_PARTICPANT_ID() = serverBD.iBossPartWhoLaunched
					IF GB_GET_PLAYER_UI_LEVEL(player) >= GB_UI_LEVEL_MINIMAL
						sOrganization = GB_GET_ORGANIZATION_NAME_AS_A_STRING()
						hclPlayer = GB_GET_PLAYER_GANG_HUD_COLOUR(player)
						// ~a~ ~s~failed to kill all the targets.
						SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "BIGM_HUNFD", sOrganization, hclPlayer) 
					ENDIF
				
				
					IF player = PLAYER_ID()
						IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneEndTelemetry)
							GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LOST)
							SET_BIT(iBoolsBitSet, biL_DoneEndTelemetry)
						ENDIF
						
						gbRewards.iExtraCash = GET_CASH_REWARD_FOR_NUM_TARGETS_KILLED()
						gbRewards.iExtraRp = GET_RP_REWARD_FOR_NUM_TARGETS_KILLED()
						GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_HEADHUNTER, FALSE, gbRewards)
					ENDIF
					
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [CHECK_FOR_REWARD] MY GANG LAUNCHED AND TIME EXPIRED. THIS MANY TARGETS STILL ALIVE numberStillAlive = ", numberStillAlive)		
				ELSE
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [CHECK_FOR_REWARD] I'M A RIVAL AND TIME EXPIRED. THIS MANY TARGETS STILL ALIVE numberStillAlive = ", numberStillAlive)
					
					IF GB_GET_PLAYER_UI_LEVEL(player) >= GB_UI_LEVEL_MINIMAL
						sOrganization = GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched))
						hclPlayer = GB_GET_PLAYER_GANG_HUD_COLOUR(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched))
						IF numberStillAlive = 1
							SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS, "GB_WINNER", "BIGM_HUN1S", sOrganization, hclPlayer, DEFAULT, numberStillAlive) 
						ELIF numberStillAlive > 1 
							SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS, "GB_WINNER", "BIGM_HUNRS", sOrganization, hclPlayer, DEFAULT, numberStillAlive) 
						ELSE
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [CHECK_FOR_REWARD] I'M A RIVAL AND TIME EXPIRED BUT numberStillAlive = ", numberStillAlive)
						ENDIF
					ENDIF
					
					IF player = PLAYER_ID()
						IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneEndTelemetry)
							GB_SET_COMMON_TELEMETRY_DATA_ON_END(TRUE, GB_TELEMETRY_END_WON)
							SET_BIT(iBoolsBitSet, biL_DoneEndTelemetry)
						ENDIF
						
						IF GB_IS_PLAYER_A_QUALIFYING_PARTICIPANT(player)
							gbRewards.iExtraCash = GET_CASH_REWARD_FOR_NUM_TARGETS_DEFENDED()
							gbRewards.iExtraRp = GET_RP_REWARD_FOR_NUM_TARGETS_DEFENDED()
						ENDIF
						GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_HEADHUNTER, TRUE, gbRewards)
						
						CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
						
						
					ENDIF
					
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [CHECK_FOR_REWARD] I'M A RIVAL AND TIME EXPIRED. THIS MANY TARGETS STILL ALIVE numberStillAlive = ", numberStillAlive)
				ENDIF
				SET_BIT(iBoolsBitSet, biL_WaitForShard)

			ELIF IS_BIT_SET(serverBD.iServerBitSet, biS_BossLaunchedQuit)
				//-- Boss who launched quit
				
				IF SHOULD_LOCAL_PLAYER_SEE_GB_HEADHUNTER_HUD()
					IF DID_MY_BOSS_LAUNCH_GB_HEADHUNTER()
						IF GB_GET_PLAYER_UI_LEVEL(player) >= GB_UI_LEVEL_MINIMAL
						//	SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "BIGM_SGHTBQ") // Your Boss quit the session
						ENDIF
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [CHECK_FOR_REWARD] biS_BossLaunchedQuit AND MY BOSS QUIT")
					ELSE
						IF GB_GET_PLAYER_UI_LEVEL(player) >= GB_UI_LEVEL_MINIMAL
							SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_OVER, "GB_WORK_OVER", "BIGM_HUNTRBQ") // The rival Boss quit the session
						ENDIF
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [CHECK_FOR_REWARD] biS_BossLaunchedQuit AND RIVAL BOSS")
					ENDIF
				ELSE
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [CHECK_FOR_REWARD] biS_BossLaunchedQuit BUT NOT SHOULD_LOCAL_PLAYER_SEE_GB_ASSAULT_HUD")
				ENDIF
				
				IF player = PLAYER_ID()
					IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneEndTelemetry)
						GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_BOSS_LEFT)
						SET_BIT(iBoolsBitSet, biL_DoneEndTelemetry)
					ENDIF
									
					GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_HEADHUNTER, FALSE, gbRewards)
				ENDIF
				
				SET_BIT(iBoolsBitSet, biL_WaitForShard)
			//	bEventCompleted = TRUE
			
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(iBoolsBitSet, biL_WaitForShard)
			Clear_Any_Objective_Text_From_This_Script()
			
			IF NOT IS_BIT_SET(iMusicBitset, biMusic_ShouldStop30s)
				CLEANUP_ASSETS()
				
				UPDATE_GLOBAL_RELATIONSHIP_GROUP_FOR_MISSION_END()
				
				SET_BIT(iMusicBitset, biMusic_ShouldStop30s)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [CHECK_FOR_REWARD] SET biL_ShouldStop30s ")
			ENDIF
			
			IF NOT IS_BIT_SET(iBoolsBitSet, biL_ResetBlipForShard)
				
			ENDIF
	
			IF GB_MAINTAIN_BOSS_END_UI(gbBossEndUi, FALSE)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [CHECK_FOR_REWARD] SET biP_Finished ")
				IF iPart = PARTICIPANT_ID_TO_INT()
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_Finished)
				ENDIF
			
			ELSE
				#IF IS_DEBUG_BUILD
				IF GET_FRAME_COUNT() % 100 = 0
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [CHECK_FOR_REWARD] WAITING FOR GB_MAINTAIN_BOSS_END_UI ")
				ENDIF
				#ENDIF
				
				IF IS_BIT_SET(iBoolsBitSet, biL_SetMaxWantedLevel)
					IF HAS_NET_TIMER_EXPIRED(timeNoWanted, 10000)
						SET_MAX_WANTED_LEVEL(iMyMaxWantedLevel)
						CLEAR_BIT(iBoolsBitSet, biL_SetMaxWantedLevel)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [CHECK_FOR_REWARD] RESTORED MY MAX WANTED LEVEL = ", iMyMaxWantedLevel)
					ENDIF
				ENDIF
				
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Controls the ped blips displaying for when they shoot
PROC CONTROL_PED_LOOP()
	
ENDPROC





/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  MISSION PROCS                  //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

/// PURPOSE:
///    Start a countdown when at least one player is in the vehicle. Mission will automatically start when timer ends
PROC MAINTAIN_START_MISSION_TIMER_SERVER()
//	BOOL bTimerShouldStart
//	
//	IF GET_ENTITY_MODEL(NET_TO_VEH(serverbd.niveh)) = VALKYRIE
//		bTimerShouldStart = IS_BIT_SET(serverbd.iServerBitSet, biS_AtLeastTwoPlayersInVeh)
//	ELSE
//		bTimerShouldStart = IS_BIT_SET(serverbd.iServerBitSet, biS_AtLeastOnePlayerInVeh)
//	ENDIF
//	
//	IF bTimerShouldStart //IS_BIT_SET(serverbd.iServerBitSet, biS_AtLeastOnePlayerInVeh)
//		IF NOT HAS_NET_TIMER_STARTED(serverbd.timeMissionStart)
//			START_NET_TIMER(serverbd.timeMissionStart)
//			NET_PRINT_TIME() NET_PRINT("     ---------->     HEADHUNTER -  START timeMissionStart    <----------     ") NET_NL()
//		ENDIF
//	ELSE
//		IF HAS_NET_TIMER_STARTED(serverbd.timeMissionStart)
//			RESET_NET_TIMER(serverbd.timeMissionStart)
//			NET_PRINT_TIME() NET_PRINT("     ---------->     HEADHUNTER -  RESET timeMissionStart    <----------     ") NET_NL()
//		ENDIF
//	ENDIF
ENDPROC


PROC MAINTAIN_START_MISSION_TIMER_CLIENT()
	
//	BOOL bTimerShouldStart
//	
//	IF GET_ENTITY_MODEL(NET_TO_VEH(serverbd.niveh)) = VALKYRIE
//		bTimerShouldStart = IS_BIT_SET(serverbd.iServerBitSet, biS_AtLeastTwoPlayersInVeh)
//	ELSE
//		bTimerShouldStart = IS_BIT_SET(serverbd.iServerBitSet, biS_AtLeastOnePlayerInVeh)
//	ENDIF
//	
//	IF bTimerShouldStart //IS_BIT_SET(serverbd.iServerBitSet, biS_AtLeastOnePlayerInVeh)
//		IF (TIME_MISSION_START-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.timeMissionStart)) >= 0
//			SET_MIDDLE_TITLE_POSITION_HUD_THIS_FRAME()
//			DRAW_GENERIC_TIMER((TIME_MISSION_START-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.timeMissionStart)), "UW_WAIT", 0, TIMER_STYLE_USEMILLISECONDS, DEFAULT, DEFAULT, HUDORDER_DONTCARE, DEFAULT, HUD_COLOUR_WHITE)
//		ELSE
//			SET_MIDDLE_TITLE_POSITION_HUD_THIS_FRAME()
//			DRAW_GENERIC_TIMER(0, "UW_WAIT", 0, TIMER_STYLE_USEMILLISECONDS, DEFAULT, DEFAULT, HUDORDER_DONTCARE, DEFAULT, HUD_COLOUR_WHITE)
//			
//			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
//				SET_BIT(serverbd.iServerBitSet, biS_MissionStartExpired)
//				NET_PRINT_TIME() NET_PRINT("     ---------->     HEADHUNTER -  MISSION START TIME EXPIRED    <----------     ") NET_NL()
//			ENDIF
//		ENDIF
//	ENDIF		
ENDPROC

PROC MAINTAIN_UW_MUSIC()
	
//	IF NOT IS_BIT_SET(iBoolsBitSet, biDoneMusic)
//		IF serverBD.eStage = eAD_ATTACK
//			IF TRIGGER_MUSIC_EVENT("MP_MC_ACTION")
//				SET_BIT(iBoolsBitSet, biDoneMusic)
//				NET_PRINT_TIME() NET_PRINT("     ---------->     HEADHUNTER -  MAINTAIN_UW_MUSIC - MP_MC_ACTION    <----------     ") NET_NL()
//			ENDIF
//		ENDIF
//	ENDIF
//	
//	IF NOT IS_BIT_SET(iBoolsBitSet, biResetMusic)
//		IF IS_BIT_SET(iBoolsBitSet, biDoneMusic)
//			IF serverBD.eStage > eAD_ATTACK
//				IF IS_PED_INJURED(PLAYER_PED_ID())
//					IF TRIGGER_MUSIC_EVENT("MP_MC_FAIL")
//						SET_BIT(iBoolsBitSet, biResetMusic)						
//						NET_PRINT_TIME() NET_PRINT("     ---------->     HEADHUNTER -  MAINTAIN_UW_MUSIC - MP_MC_FAIL    <----------     ") NET_NL()
//					ENDIF
//				ELSE
//					IF TRIGGER_MUSIC_EVENT("MP_MC_STOP")
//						SET_BIT(iBoolsBitSet, biResetMusic)						
//						NET_PRINT_TIME() NET_PRINT("     ---------->     HEADHUNTER -  MAINTAIN_UW_MUSIC - MP_MC_STOP    <----------     ") NET_NL()
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
ENDPROC



/// PURPOSE:
///    Handle the mission duration (server side)
PROC MAINTAIN_MISSION_DURATION_SERVER()
	IF NOT HAS_NET_TIMER_STARTED(serverBD.timeDuration)
		START_NET_TIMER(serverBD.timeDuration)
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] [MAINTAIN_MISSION_DURATION_SERVER] STARTED timeDuration")
	ELSE
		IF HAS_NET_TIMER_EXPIRED(serverBD.timeDuration, GET_GB_HEADHUNTER_DURATION())
			SET_BIT(serverBD.iServerBitSet, biS_DurationExpired)
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER] [MAINTAIN_MISSION_DURATION_SERVER] SET biS_DurationExpired")
		ENDIF
	ENDIF
				
ENDPROC



PROC DRAW_KILLS_BAR()
	
//	DRAW_GENERIC_BIG_DOUBLE_NUMBER((serverBD.iKillGoal-serverBD.iTotalKills), serverBD.iKillGoal, "GHO_KILLB")
ENDPROC

PROC MAINTAIN_BOTTOM_RIGHT_HUD()
	IF NOT DID_MY_BOSS_LAUNCH_GB_HEADHUNTER()
	AND NOT DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
		EXIT
	ENDIF
	
	
	
	IF NOT SHOULD_LOCAL_PLAYER_SEE_GB_HEADHUNTER_HUD()
		EXIT
	ENDIF
	
	PLAYER_INDEX player = PLAYER_ID()
	
	IF IS_PLAYER_SCTV(PLAYER_ID())
		player = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget
	ENDIF
	
	
	GB_UI_LEVEL myUiLevel = GB_GET_PLAYER_UI_LEVEL(player)
	
	IF myUiLevel < GB_UI_LEVEL_FULL	
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER [MAINTAIN_BOTTOM_RIGHT_HUD] EXIT AS myUiLevel = ", ENUM_TO_INT(myUiLevel))
		EXIT
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(serverBD.timeDuration)
		INT iTimeLeft = GET_GB_HEADHUNTER_DURATION()-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.timeDuration)
		INT iTargetsRemaining = GET_GB_HEADHUNTER_TOTAL_NUM_TARGETS() - serverBD.iNumTargetsKilled
		
		MAINTAIN_30S_COUNTDOWN_MUSIC(iTimeLeft)
		
		IF iTimeLeft > 30000
			BOTTOM_RIGHT_UI_SCORE_TIMER(iTargetsRemaining, "HUNT_HUD", iTimeLeft, DEFAULT, "GB_WORK_END")
		ELSE
			IF iTimeLeft > 0	
				BOTTOM_RIGHT_UI_SCORE_TIMER(iTargetsRemaining, "HUNT_HUD", iTimeLeft, HUD_COLOUR_RED, "GB_WORK_END")
			ELSE
				BOTTOM_RIGHT_UI_SCORE_TIMER(iTargetsRemaining, "HUNT_HUD", 0, HUD_COLOUR_RED, "GB_WORK_END")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC ADD_BLIP_FOR_PACKAGE()
ENDPROC

PROC MAINTAIN_DISTANCE_CHECKS_FOR_VARIATION()
	IF DID_MY_BOSS_LAUNCH_GB_HEADHUNTER()
		//-- Don't want to do distance checks for launching gang, mainly so this script can control the wanted rating.
		EXIT
	ENDIF
	
	IF NOT DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
		EXIT
	ENDIF
	
	VECTOR vAction
	//PLAYER_INDEX playerBoss
	INT iCLosest = -1
	
	FLOAT fDist = GET_VDIST2_TO_NEAREST_TARGET(iCLosest)
	
	IF fDist <= 1600 // 40m
		IF NOT GB_IS_PLAYER_PERMANENT_PARTICIPANT(PLAYER_ID())
			IF NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE()
				GB_SET_PLAYER_AS_PERMANENT_ON_BOSS_MISSION()
				NETWORK_SET_FRIENDLY_FIRE_OPTION(FALSE)
				SET_BIT(iBoolsBitSet, biL_SetFriendlyFire)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_DISTANCE_CHECKS_FOR_VARIATION] NETWORK_SET_FRIENDLY_FIRE_OPTION")
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_DISTANCE_CHECKS_FOR_VARIATION] SET GB_SET_PLAYER_AS_PERMANENT_ON_BOSS_MISSION AS fDist2 = ", fDist)  
			ENDIF
		ENDIF
	ENDIF
	
	IF ServerBD.iBossPlayerWhoLaunched > -1
		IF iClosest > -1
			
			vAction = GET_ENTITY_COORDS(NET_TO_PED(serverBD.Targets[iClosest].niPed), FALSE)
		ENDIF
	ENDIF

	
	
	IF NOT ARE_VECTORS_EQUAL(vAction, <<0.0, 0.0, 0.0>>)
		GB_MAINTAIN_BOSS_WORK_DISTANT_CHECKS(FMMC_TYPE_GB_HEADHUNTER, vAction, bDoneDistanceCheck)
	ENDIF
ENDPROC

PROC MAINTAIN_FORCE_RIVALS_ON_MISSION_SERVER()

	IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_ForceRivalsOnMission)
		IF NOT HAS_NET_TIMER_STARTED(serverBD.timeForceRivalsJoin)
			START_NET_TIMER(serverBD.timeForceRivalsJoin)
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_FORCE_RIVALS_ON_MISSION_SERVER] STARTED timeForceRivalsJoin") 
		ELSE
			IF HAS_NET_TIMER_EXPIRED(serverBD.timeForceRivalsJoin, 500)
				SET_BIT(serverBD.iServerBitSet, biS_ForceRivalsOnMission)			
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_FORCE_RIVALS_ON_MISSION_SERVER] SET biS_ForceRivalsOnMission")
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC MAINTAIN_PLAYER_WANTED_LEVEL()
	
	IF HAS_ANY_MISSION_END_CONDITION_BEEN_MET()
		EXIT
	ENDIF
	
	IF NOT DID_MY_BOSS_LAUNCH_GB_HEADHUNTER()
		EXIT
	ENDIF
	
	IF NOT g_sMPTunables.bexec_headhunter_wanted_persists_death
		EXIT
	ENDIF
	
	IF serverBD.iNumTargetsKilled = 0
		EXIT
	ENDIF
	
//	INT iTempWanted
//	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//		IF NOT IS_BIT_SET(iBoolsBitSet, biL_PlayerDied)
//			iTempWanted = GET_PLAYER_WANTED_LEVEL(PLAYER_ID())
//			IF iMyCurrentWantedLevel != iTempWanted
//				iMyCurrentWantedLevel = iTempWanted
//				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_PLAYER_WANTED_LEVEL] iMyCurrentWantedLevel UPDATED TO ", iMyCurrentWantedLevel)
//			ENDIF
//					
//		ELSE
//			IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
//				IF iMyCurrentWantedLevel > 0			
//					SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), iMyCurrentWantedLevel)
//					SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
//				ENDIF
//				
//				CLEAR_BIT(iBoolsBitSet, biL_PlayerDied)
//				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_PLAYER_WANTED_LEVEL] CLEAR biL_PlayerDied iMyCurrentWantedLevel = ", iMyCurrentWantedLevel)
//			ENDIF
//		ENDIF
//	ELSE
//		IF NOT IS_BIT_SET(iBoolsBitSet, biL_PlayerDied)
//			SET_BIT(iBoolsBitSet, biL_PlayerDied)
//			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_PLAYER_WANTED_LEVEL] SET biL_PlayerDied, iMyCurrentWantedLevel = ", iMyCurrentWantedLevel) 
//		ENDIF
//	ENDIF
	
ENDPROC
PROC MAINTAIN_PLAYER_JOINING_AS_RIVAL()
	IF GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE()
		IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_JoinedAsRival)
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER [MAINTAIN_PLAYER_JOINING_AS_RIVAL] Block_All_MissionsAtCoords_Missions(FALSE) AS GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE")
			Block_All_MissionsAtCoords_Missions(FALSE)
			CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_JoinedAsRival)
		ENDIF
		EXIT
	ENDIF
	
	IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) = GB_UI_LEVEL_NONE
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_JoinedAsRival)
	
		IF NOT DID_MY_BOSS_LAUNCH_GB_HEADHUNTER()
			IF NOT IS_MP_PASSIVE_MODE_ENABLED()
				IF IS_BIT_SET(serverBD.iServerBitSet, biS_ForceRivalsOnMission)
					#IF IS_DEBUG_BUILD
					IF IS_BIT_SET(serverBD.iServerBitSet, biS_ForceRivalsOnMission)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER [MAINTAIN_PLAYER_JOINING_AS_RIVAL] biS_ForceRivalsOnMission IS SET")
					ENDIF
					#ENDIF
					Block_All_MissionsAtCoords_Missions(TRUE)
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER [MAINTAIN_PLAYER_JOINING_AS_RIVAL] Block_All_MissionsAtCoords_Missions(TRUE)")
					
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_JoinedAsRival)

					
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER [MAINTAIN_PLAYER_JOINING_AS_RIVAL] SET biP_JoinedAsRival - NOT SETTING AS PERMANENT")
				ENDIF	
			ENDIF
		ENDIF
		
	ENDIF
	
ENDPROC

PROC MAINTAIN_PLAYER_AS_QUALIFYING_PARTICIPANT()
	INT iClosest
	FLOAT fClosest2 = GET_VDIST2_TO_NEAREST_TARGET(iClosest)
	IF fClosest2 < 10000.0 // 100 m
		IF NOT GB_IS_PLAYER_A_QUALIFYING_PARTICIPANT(PLAYER_ID())
			GB_SET_PLAYER_AS_QUALIFYING_PARTICIPANT()
		
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->    ---------->     HEADHUNTER [MAINTAIN_PLAYER_AS_QUALIFYING_PARTICIPANT] I'VE BEEN SET AS QUALIFYING PART AS fClosest2 = ", fClosest2)
		ENDIF	
	ENDIF
	
//	IF NOT DID_MY_BOSS_LAUNCH_GB_HEADHUNTER()
//		IF fClosest2 <= (g_sMPTunables.iFREEMODE_EVENT_UI_ENTRY_DISTANCE * g_sMPTunables.iFREEMODE_EVENT_UI_ENTRY_DISTANCE)
//			IF NOT GB_IS_GLOBAL_CLIENT_BIT0_SET(PLAYER_ID(), eGB_GLOBAL_CLIENT_BITSET_0_TEMPORARY_PARTICIPANT)
//				GB_SET_GLOBAL_CLIENT_BIT0(eGB_GLOBAL_CLIENT_BITSET_0_TEMPORARY_PARTICIPANT)
//				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->    ---------->     HEADHUNTER [MAINTAIN_PLAYER_AS_QUALIFYING_PARTICIPANT] SET eGB_GLOBAL_CLIENT_BITSET_0_TEMPORARY_PARTICIPANT")
//			ENDIF
//		ELSE
//			IF fClosest2 >= (g_sMPTunables.iFREEMODE_EVENT_UI_EXIT_DISTANCE * g_sMPTunables.iFREEMODE_EVENT_UI_EXIT_DISTANCE)
//				IF GB_IS_GLOBAL_CLIENT_BIT0_SET(PLAYER_ID(), eGB_GLOBAL_CLIENT_BITSET_0_TEMPORARY_PARTICIPANT)
//					GB_CLEAR_GLOBAL_CLIENT_BIT0(eGB_GLOBAL_CLIENT_BITSET_0_TEMPORARY_PARTICIPANT)
//					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->    ---------->     HEADHUNTER [MAINTAIN_PLAYER_AS_QUALIFYING_PARTICIPANT] CLEAR eGB_GLOBAL_CLIENT_BITSET_0_TEMPORARY_PARTICIPANT")
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
ENDPROC



PROC MAINTAIN_HEADHUNTER_INTRO_SHARD()

	IF NOT DID_MY_BOSS_LAUNCH_GB_HEADHUNTER()
	AND NOT DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
		EXIT
	ENDIF
	
	IF NOT SHOULD_LOCAL_PLAYER_SEE_GB_HEADHUNTER_HUD()
		EXIT
	ENDIF
	
	GB_UI_LEVEL myUiLevel
	PLAYER_INDEX player = INVALID_PLAYER_INDEX()
	INT iPart = -1
	
	IF NOT IS_PLAYER_SCTV(PLAYER_ID())
		player = PLAYER_ID()
		iPart = PARTICIPANT_ID_TO_INT()
	ELSE
		iPart = GET_MY_SPEC_TARGET_AS_PARTICIPANT_INT()
		player = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget
	ENDIF
	
	IF iPart = -1
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneIntroShard)
		myUiLevel = GB_GET_PLAYER_UI_LEVEL(player)
		IF myUiLevel >= GB_UI_LEVEL_MINIMAL
			IF iPart = serverBD.iBossPartWhoLaunched
			OR GET_MY_BOSS_PARTICPANT_ID() = serverBD.iBossPartWhoLaunched
				IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_MP_RAY_LAVOY,"HUNT_TXT1",TXTMSG_LOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED) 
					//-- I'm the boss who launched event
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_START_OF_JOB, "BIGM_HUNTN", "BIGM_HUNTBD") // Collect the packages hidden around the map

					IF iPart = PARTICIPANT_ID_TO_INT()
						GB_SET_LOCAL_PLAYER_CRITICAL_TO_JOB(TRUE)
						GB_SET_COMMON_TELEMETRY_DATA_ON_START()
					ENDIF
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER [MAINTAIN_HEADHUNTER_INTRO_SHARD] I'M ON GANG WHO LAUNCHED WORK")
				ENDIF
			ELSE
				IF iPart = PARTICIPANT_ID_TO_INT()
					GB_SET_COMMON_TELEMETRY_DATA_ON_START()
				ENDIF
				SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_START_OF_JOB, "BIGM_HUNTN", "BIGM_HUNTPR") 
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER [MAINTAIN_HEADHUNTER_INTRO_SHARD] I'M A RIVAL")
			ENDIF
		ELSE
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER [MAINTAIN_HEADHUNTER_INTRO_SHARD] NOT DOING SHARD BECAUSE myUiLevel = ", ENUM_TO_INT(myUiLevel))
		ENDIF

		SET_BIT(iBoolsBitSet, biL_DoneIntroShard)
	ENDIF
ENDPROC

PROC MAINTAIN_HEADHUNTER_OBJECTIVE_TEXT()
/*

	HUD_COLOURS hcTempGang
	INT iBlipColour
SHOULD_LOCAL_PLAYER_SEE_GB_HEADHUNTER_HUD
IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) < GB_UI_LEVEL_FULL

iTempGangID = GET_GANG_ID_FOR_PLAYER(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched))
			IF iTempGangID > -1
				hcTempGang = GET_HUD_COLOUR_FOR_GANG_ID(iTempGangID)
				Print_Objective_Text_With_Player_Name_And_String_Coloured("GB_SGHT_PROT", INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched), "GB_SGHT_BOSS", DEFAULT, hcTempGang)
			//	Print_Objective_Text_With_Coloured_Player_Name("GB_SGHT_PROT", INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched), hcTempGang) 
			ENDIF
*/
	
//	INT iTempGangID
	HUD_COLOURS hcTempGang
	TEXT_LABEL_63 sOrganization
	
	PLAYER_INDEX player = PLAYER_ID()
	INT iPart = PARTICIPANT_ID_TO_INT()
	
	IF IS_PLAYER_SCTV(PLAYER_ID())
		iPart = GET_MY_SPEC_TARGET_AS_PARTICIPANT_INT()
		player = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget
	ENDIF
	
	GB_UI_LEVEL myUiLevel = GB_GET_PLAYER_UI_LEVEL(player)
	
	IF NOT IS_PLAYER_SCTV(PLAYER_ID())
		IF NOT SHOULD_LOCAL_PLAYER_SEE_GB_HEADHUNTER_HUD()
			IF Is_This_The_Current_Objective_Text("HUNT_OBJ")
				Clear_Any_Objective_Text_From_This_Script()
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_HEADHUNTER_OBJECTIVE_TEXT] CLEAR HUNT_OBJ AS SHOULD_LOCAL_PLAYER_SEE_GB_HEADHUNTER_HUD")
			ENDIF
			
			IF Is_This_The_Current_Objective_Text("HUNT_OBJ1L")
				Clear_Any_Objective_Text_From_This_Script()
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_HEADHUNTER_OBJECTIVE_TEXT] CLEAR HUNT_OBJ1L AS SHOULD_LOCAL_PLAYER_SEE_GB_HEADHUNTER_HUD")
			ENDIF
			
			IF Is_This_The_Current_Objective_Text("HUNT_TOBJ")
				Clear_Any_Objective_Text_From_This_Script()
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_HEADHUNTER_OBJECTIVE_TEXT] CLEAR HUNT_TOBJ AS SHOULD_LOCAL_PLAYER_SEE_GB_HEADHUNTER_HUD")
			ENDIF
			
			IF Is_This_The_Current_Objective_Text("HUNT_DOBJ")
				Clear_Any_Objective_Text_From_This_Script()
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_HEADHUNTER_OBJECTIVE_TEXT] CLEAR HUNT_DOBJ AS SHOULD_LOCAL_PLAYER_SEE_GB_HEADHUNTER_HUD")
			ENDIF
			
			IF Is_This_The_Current_Objective_Text("HUNT_DOBJ1L")
				Clear_Any_Objective_Text_From_This_Script()
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_HEADHUNTER_OBJECTIVE_TEXT] CLEAR HUNT_DOBJ1L AS SHOULD_LOCAL_PLAYER_SEE_GB_HEADHUNTER_HUD")
			ENDIF
			
			EXIT
		ENDIF
	ELSE
		//-- SCTV player
		IF NOT IS_BIT_SET(playerBD[iPart].iPlayerBitSet, biP_HudOkForSCTV)
			Clear_Any_Objective_Text_From_This_Script()
			EXIT
		ENDIF
	ENDIF
	
	IF myUiLevel < GB_UI_LEVEL_FULL
		IF Is_This_The_Current_Objective_Text("HUNT_OBJ")
			Clear_Any_Objective_Text_From_This_Script()
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_HEADHUNTER_OBJECTIVE_TEXT] CLEAR HUNT_OBJ AS myUiLevel = ", ENUM_TO_INT(myUiLevel))
		ENDIF
		
		IF Is_This_The_Current_Objective_Text("HUNT_OBJ1L")
			Clear_Any_Objective_Text_From_This_Script()
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_HEADHUNTER_OBJECTIVE_TEXT] CLEAR HUNT_OBJ1L AS myUiLevel = ", ENUM_TO_INT(myUiLevel))
		ENDIF
		
		IF Is_This_The_Current_Objective_Text("HUNT_TOBJ")
			Clear_Any_Objective_Text_From_This_Script()
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_HEADHUNTER_OBJECTIVE_TEXT] CLEAR HUNT_TOBJ AS myUiLevel = ", ENUM_TO_INT(myUiLevel))
		ENDIF
		
		IF Is_This_The_Current_Objective_Text("HUNT_DOBJ")
			Clear_Any_Objective_Text_From_This_Script()
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_HEADHUNTER_OBJECTIVE_TEXT] CLEAR HUNT_DOBJ AS myUiLevel = ", ENUM_TO_INT(myUiLevel))
		ENDIF
		
		IF Is_This_The_Current_Objective_Text("HUNT_DOBJ1L")
			Clear_Any_Objective_Text_From_This_Script()
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_HEADHUNTER_OBJECTIVE_TEXT] CLEAR HUNT_DOBJ1L AS myUiLevel = ", ENUM_TO_INT(myUiLevel))
		ENDIF
		
		EXIT
	ENDIF

	
	BOOL bOneTargetLeft = (GET_NUMBER_OF_TARGETS_REMAINING() = 1)
	
	IF iPart = serverBD.iBossPartWhoLaunched
		//-- I'm the boss who launched event
		IF NOT bOneTargetLeft
			IF NOT Is_This_The_Current_Objective_Text("HUNT_OBJ")
				Print_Objective_Text("HUNT_OBJ") // Kill the ~r~targets.
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_HEADHUNTER_OBJECTIVE_TEXT] DONE HUNT_OBJ - I LAUNCHED")
			ENDIF
		ELSE
			IF NOT Is_This_The_Current_Objective_Text("HUNT_OBJ1L")
				Print_Objective_Text("HUNT_OBJ1L") // Kill the ~r~targets.
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_HEADHUNTER_OBJECTIVE_TEXT] DONE HUNT_OBJ1L - I LAUNCHED")
			ENDIF
		ENDIF
	ELIF GET_MY_BOSS_PARTICPANT_ID() = serverBD.iBossPartWhoLaunched
		//-- My boss launched the work
//		IF NOT Is_This_The_Current_Objective_Text("HUNT_TOBJ")
//			iTempGangID = GET_GANG_ID_FOR_PLAYER(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched))
//			IF iTempGangID > -1
//				hcTempGang = GET_HUD_COLOUR_FOR_GANG_ID(iTempGangID)
//				Print_Objective_Text_With_Player_Name_And_String_Coloured("HUNT_TOBJ", INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched), "HUNT_TARG", hcTempGang, HUD_COLOUR_RED)
//				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_HEADHUNTER_OBJECTIVE_TEXT] DONE HUNT_TOBJ - MY BOSS LAUNCHED")
//			ENDIF
//		ENDIF
		IF NOT bOneTargetLeft
			IF NOT Is_This_The_Current_Objective_Text("HUNT_OBJ")
				Print_Objective_Text("HUNT_OBJ") // Kill the ~r~targets.
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_HEADHUNTER_OBJECTIVE_TEXT] DONE HUNT_OBJ - MY BOSS LAUNCHED")
			ENDIF
		ELSE
			IF NOT Is_This_The_Current_Objective_Text("HUNT_OBJ1L")
				Print_Objective_Text("HUNT_OBJ1L") // Kill the ~r~targets.
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_HEADHUNTER_OBJECTIVE_TEXT] DONE HUNT_OBJ1L - MY BOSS LAUNCHED")
			ENDIF
		ENDIF
		
	ELIF DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
		//-- Not part of the launching gang
	//	TEXT_LABEL_63 tlName
		IF serverBD.iBossPlayerWhoLaunched > -1
			IF NOT bOneTargetLeft
				IF NOT Is_This_The_Current_Objective_Text("HUNT_DOBJ")
					sOrganization = GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched))
					hcTempGang = GB_GET_PLAYER_GANG_HUD_COLOUR(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched))
					Print_Objective_Text_With_Coloured_String("HUNT_DOBJ", sOrganization, hcTempGang)
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_HEADHUNTER_OBJECTIVE_TEXT] DONE HUNT_DOBJ - I'm a rival")
				ENDIF
			ELSE 
				IF NOT Is_This_The_Current_Objective_Text("HUNT_DOBJ1L")
					sOrganization =GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched) #IF IS_DEBUG_BUILD , TRUE #ENDIF)
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_HEADHUNTER_OBJECTIVE_TEXT] DOING HUNT_DOBJ1L sOrganization = ", sOrganization)
					hcTempGang = GB_GET_PLAYER_GANG_HUD_COLOUR(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched))
					Print_Objective_Text_With_Coloured_String("HUNT_DOBJ1L", sOrganization, hcTempGang)
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_HEADHUNTER_OBJECTIVE_TEXT] DONE HUNT_DOBJ1L - I'm a rival, sOrganization = ", sOrganization)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC


PROC MAINTAIN_HEADHUNTER_HELP_TEXT()
	IF NOT SHOULD_LOCAL_PLAYER_SEE_GB_HEADHUNTER_HUD()
		IF IS_THIS_HELP_MESSAGE_WITH_STRING_BEING_DISPLAYED("HUNT_HELPA", "HUNT_TARBLP")
			CLEAR_HELP()
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_HEADHUNTER_HELP_TEXT] CLEAR HUNT_HELPA AS SHOULD_LOCAL_PLAYER_SEE_GB_SIGHTSEER_HUD")
		ENDIF
		
		EXIT
	ENDIF
	
	GB_UI_LEVEL myUiLevel = GB_GET_PLAYER_UI_LEVEL(PLAYER_ID())
	IF myUiLevel < GB_UI_LEVEL_MINIMAL
		IF IS_THIS_HELP_MESSAGE_WITH_STRING_BEING_DISPLAYED("HUNT_HELPA", "HUNT_TARBLP")
			CLEAR_HELP()
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_HEADHUNTER_HELP_TEXT] CLEAR HUNT_HELPA AS myUiLevel = ", ENUM_TO_INT(myUiLevel))
		ENDIF
		
		EXIT
	ENDIF
	
	//STRING sOrganization
	IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneIntroHelp)
		IF NOT IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_GB_START_OF_JOB)	
			IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE)
				IF PARTICIPANT_ID_TO_INT() = serverBD.iBossPartWhoLaunched
				//	PRINT_HELP_NO_SOUND("HUNT_HELPA") // Kill the targets before the time expires.
					PRINT_HELP_WITH_COLOURED_STRING_NO_SOUND("HUNT_HELPA", "HUNT_TARBLP", HUD_COLOUR_RED)
					//PRINT_HELP_WITH_STRING_NO_SOUND()			
					GB_SET_GANG_BOSS_HELP_BACKGROUND()
					SET_BIT(iBoolsBitSet, biL_DoneIntroHelp)
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_HEADHUNTER_HELP_TEXT] DONE INTRO HELP - I LAUNCHED")
				ELIF GET_MY_BOSS_PARTICPANT_ID() = serverBD.iBossPartWhoLaunched
				//	PRINT_HELP_NO_SOUND("HUNT_HELPA") // Kill the targets before the time expires.
					PRINT_HELP_WITH_COLOURED_STRING_NO_SOUND("HUNT_HELPAG", "HUNT_TARBLP", HUD_COLOUR_RED)
					GB_SET_GANG_BOSS_HELP_BACKGROUND()
					SET_BIT(iBoolsBitSet, biL_DoneIntroHelp)
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_HEADHUNTER_HELP_TEXT] DONE INTRO HELP - MY BOSS LAUNCHED")
				ELIF DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
				//	PRINT_HELP_NO_SOUND("HUNT_HELPD") // Protect the targets until the time expires..
				//	PRINT_HELP_WITH_STRING_NO_SOUND("HUNT_HELPD", "HUNT_TARBLPD")
					
		//			sOrganization = GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(INT_TO_PLAYERINDEX(serverBD.iBossPlayerWhoLaunched))
		//			PRINT_HELP_WITH_LITERAL_STRING_AND_COLOURED_STRING_NO_SOUND("HUNT_HELPD",sOrganization,  hcLaunchingGang,  "HUNT_TARBLPD", HUD_COLOUR_BLUE)
					PRINT_HELP_WITH_COLOURED_STRING_NO_SOUND("HUNT_HELPD", "HUNT_TARRVL", HUD_COLOUR_BLUE)
					GB_SET_GANG_BOSS_HELP_BACKGROUND()
					SET_BIT(iBoolsBitSet, biL_DoneIntroHelp)
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_HEADHUNTER_HELP_TEXT] DONE INTRO HELP - I'm a rival")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iBoolsBitSet, biL_DoneIntroHelp)
		IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneCoordHelp)
			IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE)
				IF NOT IS_THIS_HELP_MESSAGE_WITH_STRING_BEING_DISPLAYED("HUNT_HELPA", "HUNT_TARBLP")
					IF PARTICIPANT_ID_TO_INT() = serverBD.iBossPartWhoLaunched
					OR GET_MY_BOSS_PARTICPANT_ID() = serverBD.iBossPartWhoLaunched
						PRINT_HELP_NO_SOUND("HUNT_HELPB")
						GB_SET_GANG_BOSS_HELP_BACKGROUND()
						SET_BIT(iBoolsBitSet, biL_DoneCoordHelp)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_HEADHUNTER_HELP_TEXT] DONE COORDINATE ATTACK HELP")
					ENDIF
				ENDIF
			ENDIF
		ENDIF		
	ENDIF
	
ENDPROC



/// PURPOSE:
///    Draws a marker above the package when it has been dropped
PROC MAINTAIN_PACKAGE_MARKER()

ENDPROC



PROC MAINTAIN_PLAYER_COLLECT_PACKAGES()
	
ENDPROC

PROC MAINTAIN_TICKERS()

ENDPROC

PROC MAINTAIN_HEADHUNTER_LOCAL_TARGETS_KILLED()
	IF iLocalNumberOfTargetsKilled != serverBD.iNumTargetsKilled
		IF DID_MY_BOSS_LAUNCH_GB_HEADHUNTER()
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
					IF NOT NETWORK_IS_IN_MP_CUTSCENE()
						INT iCurrentWantedLevel = GET_PLAYER_WANTED_LEVEL(PLAYER_ID())
						
						SWITCH serverBD.iNumTargetsKilled
							CASE 1
								IF iCurrentWantedLevel < g_sMPTunables.iexec_headhunter_1st_target_wanted
									SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), g_sMPTunables.iexec_headhunter_1st_target_wanted)
									SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
									CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_HEADHUNTER_LOCAL_TARGETS_KILLED] serverBD.iNumTargetsKilled = ", serverBD.iNumTargetsKilled , " iCurrentWantedLevel = ", iCurrentWantedLevel, " SET TO ", g_sMPTunables.iexec_headhunter_1st_target_wanted )
								ENDIF
							BREAK
							
							CASE 2
								IF iCurrentWantedLevel < g_sMPTunables.iexec_headhunter_2nd_target_wanted
									SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), g_sMPTunables.iexec_headhunter_2nd_target_wanted)
									SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
									CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_HEADHUNTER_LOCAL_TARGETS_KILLED] serverBD.iNumTargetsKilled = ", serverBD.iNumTargetsKilled , " iCurrentWantedLevel = ", iCurrentWantedLevel, " SET TO ", g_sMPTunables.iexec_headhunter_2nd_target_wanted )
								ENDIF
							BREAK
							
							CASE 3
								IF iCurrentWantedLevel < g_sMPTunables.iexec_headhunter_3rd_target_wanted
									SET_MAX_WANTED_LEVEL(g_sMPTunables.iexec_headhunter_3rd_target_wanted)
									SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), g_sMPTunables.iexec_headhunter_3rd_target_wanted)
									SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
									CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_HEADHUNTER_LOCAL_TARGETS_KILLED] serverBD.iNumTargetsKilled = ", serverBD.iNumTargetsKilled , " iCurrentWantedLevel = ", iCurrentWantedLevel, " SET TO ", g_sMPTunables.iexec_headhunter_3rd_target_wanted)
								ENDIF
							BREAK
						ENDSWITCH
						
						iLocalNumberOfTargetsKilled = serverBD.iNumTargetsKilled
				
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_HEADHUNTER_LOCAL_TARGETS_KILLED] iLocalNumberOfTargetsKilled UPDATED TO ", iLocalNumberOfTargetsKilled)
					ENDIF
				ENDIF
		
			ENDIF
		ELSE
			iLocalNumberOfTargetsKilled = serverBD.iNumTargetsKilled
		ENDIF
		
	ENDIF
ENDPROC

PROC MAINTAIN_MAX_PLAYER_CHECKS_CLIENT()
	PLAYER_INDEX playerBoss
	//INT iPart
//	PARTICIPANT_INDEX part
	PLAYER_INDEX PlayerId
	
	IF serverBD.iServerGameState != GAME_STATE_END
		PlayerId = INT_TO_PLAYERINDEX(iClientStaggeredPlayer)
		IF NETWORK_IS_PLAYER_ACTIVE(PlayerId)
			IF NETWORK_IS_PLAYER_A_PARTICIPANT(PlayerId)
			//	part = NETWORK_GET_PARTICIPANT_INDEX(PlayerId)
			//	iPart = NATIVE_TO_INT(part)
			
				IF NOT HAS_ANY_MISSION_END_CONDITION_BEEN_MET()
					IF NOT DID_MY_BOSS_LAUNCH_GB_HEADHUNTER()
						IF DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
							IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PlayerId)
								playerBoss = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(serverBD.iBossPartWhoLaunched))
								IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(PlayerId, playerBoss)
									IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iCustomBlipBitset, iClientStaggeredPlayer)
										SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(PlayerId, RADAR_TRACE_TEMP_4, TRUE)
										SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(PlayerId, GET_BLIP_COLOUR_FROM_HUD_COLOUR(hcLaunchingGang), TRUE)
										
										SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iCustomBlipBitset, iClientStaggeredPlayer)
										CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_MAX_PLAYER_CHECKS_CLIENT] I HAVE SET CUSTOM BLIP FOR PLAYER ", GET_PLAYER_NAME(PlayerId))
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iCustomBlipBitset, iClientStaggeredPlayer)
					SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(PlayerId, RADAR_TRACE_TEMP_4, FALSE)
					SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(PlayerId, GET_BLIP_COLOUR_FROM_HUD_COLOUR(hcLaunchingGang), FALSE)
					CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iCustomBlipBitset, iClientStaggeredPlayer)
					
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_MAX_PLAYER_CHECKS_CLIENT] CLEAR iClientStaggeredPlayer as PLAYER NO LONGER A PART ", GET_PLAYER_NAME(PlayerId))
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	iClientStaggeredPlayer++ 
	
	IF iClientStaggeredPlayer >= NUM_NETWORK_PLAYERS
		iClientStaggeredPlayer = 0
	ENDIF
	
	
ENDPROC

/// PURPOSE:
///    Have defending players respawn close to target, if they were nearby when killed
PROC MAINTAIN_HEADHUNTER_SPAWNING()
	IF DID_MY_BOSS_LAUNCH_GB_HEADHUNTER()
	 //-- Only for defenders
		EXIT
	ENDIF
	
	IF HAS_ANY_MISSION_END_CONDITION_BEEN_MET()
		EXIT
	ENDIF
	
	INT iCLosest
	VECTOR vEntityCoords
	FLOAT fDist2 = GET_VDIST2_TO_NEAREST_TARGET(iCLosest, TRUE)
	IF fDist2 < 62500 // 250m
		IF iCLosest > -1
			IF iMyLastClosestTarget != iCLosest
				vEntityCoords = GET_ENTITY_COORDS(NET_TO_PED(serverBD.Targets[iCLosest].niPed), FALSE)
				SET_PLAYER_NEXT_RESPAWN_LOCATION(SPAWN_LOCATION_NEAR_SPECIFIC_COORDS)	
					
				SETUP_SPECIFIC_SPAWN_LOCATION(vEntityCoords, // VECTOR vCoords, 
												0.0, 											 //	FLOAT fHeading, 
												250.0, 											 //	FLOAT fSearchRadius=100.0, 
												TRUE, 											 //	BOOL bDoVisibleChecks=TRUE, 
												FMEVENT_MINIMUM_SPAWN_DISTANCE_FROM_ACTION, 	 //	FLOAT fMinDistFromCoords=0.0, 
												FALSE, 											 //	BOOL bConsiderInteriors = FALSE, 
												TRUE, 											 //	BOOL bDoNearARoadChecks=TRUE, 
												65, 											 //	FLOAT MinDistFromOtherPlayers=65.0, 
												TRUE, 											 //	BOOL bNearCentrePoint=TRUE, 
												FALSE)											//	BOOL bIgnoreGlobalExclusionZones=FALSE)
				
				iMyLastClosestTarget = iCLosest
				
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [MAINTAIN_HEADHUNTER_SPAWNING] iMyLastClosestTarget = ", iMyLastClosestTarget, " WILL TRY TO RESPAWN NEAR ", vEntityCoords)
				
				SET_BIT(iBoolsBitSet, biL_SetupSpawnCoord)
			ENDIF
		ENDIF
	ELSE
		IF fDist2 > 250000 // 500m
			CLEANUP_HEADHUNTER_SPAWNING()
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_HEADHUNTER_TARGETS_CLIENT()
	MAINTAIN_PLAYER_JOINING_AS_RIVAL()
	
	MAINTAIN_PLAYER_AS_QUALIFYING_PARTICIPANT()
	
	MAINTAIN_HEADHUNTER_INTRO_SHARD()
	
	MAINTAIN_HEADHUNTER_OBJECTIVE_TEXT()
	
	MAINTAIN_HEADHUNTER_HELP_TEXT()
	
	MAINTAIN_HEADHUNTER_LOCAL_TARGETS_KILLED()
	
	MAINTAIN_HEADHUNTER_SPAWNING()
	
	MAINTAIN_PLAYER_WANTED_LEVEL()
	
	MAINTAIN_BOTTOM_RIGHT_HUD()
	
	MAINTAIN_TICKERS()
ENDPROC

//PURPOSE: Process the HEADHUNTER stages for the Client
PROC PROCESS_HEADHUNTER_CLIENT()
	IF IS_PLAYER_SCTV(PLAYER_ID())
		EXIT
	ENDIF
	
	SWITCH playerBD[PARTICIPANT_ID_TO_INT()].eStage		
		CASE eAD_TARGETS
			MAINTAIN_HEADHUNTER_TARGETS_CLIENT()
			
			CHECK_FOR_REWARD()
			IF serverBD.eStage = eAD_OFF_MISSION
				playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_OFF_MISSION
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER -  PLAYER STAGE eAD_TARGETS -> eAD_OFF_MISSION AS SERVER SAYS SO   <----------     ") NET_NL()
			ELIF serverBD.eStage = eAD_CLEANUP
				playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_CLEANUP
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER -  PLAYER STAGE eAD_TARGETS -> eAD_CLEANUP AS SERVER SAYS SO   <----------     ") NET_NL()
			ENDIF
		BREAK
		
		
		CASE eAD_OFF_MISSION
			CHECK_FOR_REWARD()
			
			IF HAS_ANY_MISSION_END_CONDITION_BEEN_MET()
				STOP_30S_COUNTDOWN_AUDIO()
			ENDIF
			IF serverBD.eStage = eAD_CLEANUP
				playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_CLEANUP
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER -  PLAYER STAGE eAD_OFF_MISSION -> eAD_CLEANUP AS SERVER SAYS SO   <----------     ") NET_NL()
			ENDIF
			

		BREAK
		
		CASE eAD_CLEANUP
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER -  SCRIPT CLEANUP B     <----------     ") NET_NL()
			SCRIPT_CLEANUP()
		BREAK
		
	ENDSWITCH
ENDPROC

PROC PROCESS_HEADHUNTER_SCTV_CLIENT()
	IF NOT IS_PLAYER_SCTV(PLAYER_ID())
		IF IS_BIT_SET(iBoolsBitSet, biL_ResetForSctv)
			CLEAR_BIT(iBoolsBitSet, biL_ResetForSctv)
		ENDIF
		
		EXIT
	ENDIF
	
	PLAYER_INDEX playerSPec = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget
	PARTICIPANT_INDEX partSpec
	INT iPartSpec
	HEADHUNTER_STAGE_ENUM partSpecStage
	#IF IS_DEBUG_BUILD
		BOOL bPlayerValid
	#ENDIF
	
	//Kill the pit set if they were in the script then became SCTV
	IF NOT IS_BIT_SET(iBoolsBitSet, biL_ResetForSctv)
		IF playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet != 0
			playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet = 0
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     HEADHUNTER -  [PROCESS_HEADHUNTER_SCTV_CLIENT]  playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet != 0 - Clearing")
		ENDIF
		SET_BIT(iBoolsBitSet, biL_ResetForSctv)
	ENDIF
	
	//Clear the objective text
	IF NOT IS_BIT_SET(iBoolsBitSet, biL_SCTV_Clear_God_Text)
		SET_BIT(iBoolsBitSet, biL_SCTV_Clear_God_Text)	
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"    ---------->     HEADHUNTER -  [PROCESS_HEADHUNTER_SCTV_CLIENT]  Clear_Any_Objective_Text_From_This_Script")
		Clear_Any_Objective_Text_From_This_Script()
	ENDIF

	
	IF NETWORK_IS_PLAYER_ACTIVE(playerSPec)
		IF NETWORK_IS_PLAYER_A_PARTICIPANT(playerSPec)
			partSpec = NETWORK_GET_PARTICIPANT_INDEX(playerSPec)
			iPartSpec = NATIVE_TO_INT(partSpec)
			partSpecStage = playerBD[iPartSpec].eStage
			
			#IF IS_DEBUG_BUILD
				bPlayerValid = TRUE
				IF bPlayerValid ENDIF
			#ENDIF
			
			
			
			SWITCH partSpecStage
				CASE eAD_TARGETS
					MAINTAIN_HEADHUNTER_INTRO_SHARD()
					
					MAINTAIN_HEADHUNTER_OBJECTIVE_TEXT()
					
					MAINTAIN_BOTTOM_RIGHT_HUD()
				BREAK
				
				CASE eAD_OFF_MISSION
					CHECK_FOR_REWARD()
				BREAK
				
				CASE eAD_CLEANUP
					SCRIPT_CLEANUP()
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_BODYGUARD_BLIPS()
	INT i
	REPEAT MAX_BODYGUARD_BLIPS i
		UPDATE_ENEMY_NET_PED_BLIP(niLocalBodyguardList[i], bodyguardBlip[i])
	ENDREPEAT
ENDPROC


PROC PROCESS_TARGET_PED_BRAIN()
//	IF HAS_ANY_MISSION_END_CONDITION_BEEN_MET()
//		EXIT
//	ENDIF
	
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		INT i
		INT j
		REPEAT MAX_TARGETS i
			SWITCH serverBD.Targets[i].iTargetPedState
				CASE PED_AI_STATE_CREATED
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.Targets[i].niPed)
					 	
						IF serverbd.Targets[i].iTargetType = TARGET_TYPE_DRIVER_WITH_BODYGUARDS
							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.Targets[i].niVeh)
								serverBD.Targets[i].iTargetPedState = PED_AI_STATE_TOUR_IN_CAR //  
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [PROCESS_TARGET_PED_BRAIN] TARGET_PED_CREATED -> TARGET_PED_TOUR_IN_CAR PED = ", i)
							ENDIF
						ELIF serverbd.Targets[i].iTargetType = TARGET_TYPE_SOLO_ON_FOOT
							serverBD.Targets[i].iTargetPedState = PED_AI_STATE_TOUR_ON_FOOT
							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [PROCESS_TARGET_PED_BRAIN] TARGET_PED_CREATED -> PED_AI_STATE_TOUR_ON_FOOT PED = ", i)
						ELSE
							serverBD.Targets[i].iTargetPedState = PED_AI_STATE_SCENARIO
							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [PROCESS_TARGET_PED_BRAIN] TARGET_PED_CREATED -> PED_AI_STATE_SCENARIO PED = ", i)
						ENDIF
					ENDIF
				BREAK
				
				CASE PED_AI_STATE_TOUR_IN_CAR
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.Targets[i].niPed)
						IF NOT IS_NET_PED_INJURED(serverbd.Targets[i].niPed)
							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.Targets[i].niVeh)
								IF IS_NET_VEHICLE_DRIVEABLE(serverbd.Targets[i].niVeh)
									IF IS_PED_SITTING_IN_VEHICLE(NET_TO_PED(serverbd.Targets[i].niPed), NET_TO_VEH(serverbd.Targets[i].niVeh))
										IF IS_BIT_SET(serverBD.iTargetVehDamagedBitSet, i)

											SET_BIT(serverBD.iTargetDriveFastBitSet, i)
											serverBD.Targets[i].iTargetPedState = PED_AI_STATE_FLEE_IN_CAR
											CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [PROCESS_TARGET_PED_BRAIN] TARGET_PED_TOUR_IN_CAR -> TARGET_PED_FLEE_IN_CAR as biS_SomeoneDamagedVeh PED = ", i)

										ELIF IS_BIT_SET(serverBD.iTargetAimedAtBitSet, i)
											SET_BIT(serverBD.iTargetDriveFastBitSet, i)
											serverBD.Targets[i].iTargetPedState = PED_AI_STATE_FLEE_IN_CAR
											CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [PROCESS_TARGET_PED_BRAIN] TARGET_PED_TOUR_IN_CAR -> TARGET_PED_FLEE_IN_CAR as biS_SomeoneAimingAtTarget PED = ", i)
 										ELIF IS_BIT_SET(serverBD.iTargetNearbyShotsBitset, i)
											SET_BIT(serverBD.iTargetDriveFastBitSet, i)
											serverBD.Targets[i].iTargetPedState = PED_AI_STATE_FLEE_IN_CAR
											CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [PROCESS_TARGET_PED_BRAIN] TARGET_PED_TOUR_IN_CAR -> PED_AI_STATE_FLEE_IN_CAR as bullets near target PED = ", i)
										
										ELIF IS_BIT_SET(serverBD.iTargetVehStuckBitSet, i)
											serverBD.Targets[i].iTargetPedState = PED_AI_STATE_LEAVE_CAR
											CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [PROCESS_TARGET_PED_BRAIN] TARGET_PED_TOUR_IN_CAR -> TARGET_PED_LEAVE_CAR as car stuck PED = ", i)
										
										ENDIF
									ELSE	
										serverBD.Targets[i].iTargetPedState = PED_AI_STATE_FLEE_ON_FOOT
										CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [PROCESS_TARGET_PED_BRAIN]  TARGET_PED_TOUR_IN_CAR -> TARGET_PED_FLEE_ON_FOOT 1 PED = ", i)
									ENDIF
								ELSE	
									serverBD.Targets[i].iTargetPedState = PED_AI_STATE_LEAVE_CAR
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [PROCESS_TARGET_PED_BRAIN]  TARGET_PED_TOUR_IN_CAR -> TARGET_PED_LEAVE_CAR 1 PED = ", i)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
							
				BREAK
				
				CASE PED_AI_STATE_FLEE_IN_CAR
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.Targets[i].niPed)
						IF NOT IS_NET_PED_INJURED(serverbd.Targets[i].niPed)
							IF NOT IS_PED_SITTING_IN_ANY_VEHICLE(NET_TO_PED(serverbd.Targets[i].niPed))
								serverBD.Targets[i].iTargetPedState = PED_AI_STATE_FLEE_ON_FOOT
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [PROCESS_TARGET_PED_BRAIN]  TARGET_PED_FLEE_IN_CAR -> TARGET_PED_FLEE_ON_FOOT 1 PED = ", i)
							ELSE	
								IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.Targets[i].niVeh)
									IF NOT IS_NET_VEHICLE_DRIVEABLE(serverbd.Targets[i].niVeh)
										serverBD.Targets[i].iTargetPedState = PED_AI_STATE_LEAVE_CAR
										CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [PROCESS_TARGET_PED_BRAIN]  TARGET_PED_FLEE_IN_CAR -> TARGET_PED_LEAVE_CAR 1 PED = ", i)
									ELIF IS_BIT_SET(serverBD.iTargetVehStuckBitSet, i) 
										serverBD.Targets[i].iTargetPedState = PED_AI_STATE_LEAVE_CAR
										CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [PROCESS_TARGET_PED_BRAIN]  TARGET_PED_FLEE_IN_CAR -> TARGET_PED_LEAVE_CAR as car stuck PED = ", i)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE PED_AI_STATE_LEAVE_CAR
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.Targets[i].niPed)
						IF NOT IS_NET_PED_INJURED(serverbd.Targets[i].niPed)
							IF NOT IS_PED_IN_ANY_VEHICLE(NET_TO_PED(serverbd.Targets[i].niPed))
								serverBD.Targets[i].iTargetPedState = PED_AI_STATE_FLEE_ON_FOOT
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [PROCESS_TARGET_PED_BRAIN]  TTARGET_PED_LEAVE_CAR -> TARGET_PED_FLEE_ON_FOOT 1 PED = ", i)
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE PED_AI_STATE_TOUR_ON_FOOT
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.Targets[i].niPed)
						IF NOT IS_NET_PED_INJURED(serverbd.Targets[i].niPed)
							IF IS_BIT_SET(serverBD.iTargetAimedAtBitSet, i)
								serverBD.Targets[i].iTargetPedState = PED_AI_STATE_FLEE_ON_FOOT
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [PROCESS_TARGET_PED_BRAIN] PED_AI_STATE_TOUR_ON_FOOT -> PED_AI_STATE_FLEE_ON_FOOT as biS_SomeoneAimingAtTarget PED = ", i)
							ELIF IS_BIT_SET(serverBD.iTargetNearbyShotsBitset, i)		
								serverBD.Targets[i].iTargetPedState = PED_AI_STATE_FLEE_ON_FOOT
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [PROCESS_TARGET_PED_BRAIN] PED_AI_STATE_TOUR_ON_FOOT -> PED_AI_STATE_FLEE_ON_FOOT as biS_SomeoneAimingAtTarget PED = ", i)
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				
				CASE PED_AI_STATE_SCENARIO
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.Targets[i].niPed)
						IF serverBD.Targets[i].bShouldSeekCover
							serverBD.Targets[i].iTargetPedState = PED_AI_STATE_TAKE_COVER //PED_AI_STATE_DEFEND_TARGET
							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [PROCESS_TARGET_PED_BRAIN] TARGET ", i, " PED_AI_STATE_SCENARIO -> PED_AI_STATE_TAKE_COVER")
						ENDIF
					ENDIF
				BREAK
				
				CASE PED_AI_STATE_TAKE_COVER
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.Targets[i].niPed)
						IF serverBD.Targets[i].bShouldEnterCombat
							serverBD.Targets[i].iTargetPedState = PED_AI_STATE_DEFEND_TARGET //PED_AI_STATE_DEFEND_TARGET
							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [PROCESS_TARGET_PED_BRAIN] TARGET ", i, " PED_AI_STATE_TAKE_COVER -> PED_AI_STATE_DEFEND_TARGET")
						ENDIF
					ENDIF
				BREAK
				
				CASE PED_AI_STATE_FLEE_ON_FOOT
				BREAK
			ENDSWITCH
			
			j = 0
			
			REPEAT MAX_BODYGUARDS_PER_TARGET j			
				SWITCH serverBD.Targets[i].iBodyguardPedState[j]
					CASE PED_AI_STATE_CREATED
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.Targets[i].niBodyguardPed[j])
							IF serverbd.Targets[i].iTargetType = TARGET_TYPE_DRIVER_WITH_BODYGUARDS
								serverBD.Targets[i].iBodyguardPedState[j] = PED_AI_STATE_TOUR_IN_CAR
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [PROCESS_TARGET_PED_BRAIN] TARGET ", i, " BODYGUARD ", j, " PED_AI_STATE_CREATED -> PED_AI_STATE_TOUR_IN_CAR")
							ELIF serverbd.Targets[i].iTargetType = TARGET_TYPE_ON_FOOT_WITH_BODYGUARDS
								serverBD.Targets[i].iBodyguardPedState[j] = PED_AI_STATE_SCENARIO
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [PROCESS_TARGET_PED_BRAIN] TARGET ", i, " BODYGUARD ", j, " PED_AI_STATE_CREATED -> PED_AI_STATE_SCENARIO")
							ENDIF
						ENDIF
					BREAK
					
					CASE PED_AI_STATE_TOUR_IN_CAR
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.Targets[i].niBodyguardPed[j])
							IF serverBD.Targets[i].iTargetPedState = PED_AI_STATE_FLEE_IN_CAR
							
								serverBD.Targets[i].iBodyguardPedState[j] = PED_AI_STATE_DEFEND_IN_CAR
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [PROCESS_TARGET_PED_BRAIN] TARGET ", i, " BODYGUARD ", j, " PED_AI_STATE_TOUR_IN_CAR -> PED_AI_STATE_DEFEND_IN_CAR")
							ELIF serverBD.Targets[i].iTargetPedState = PED_AI_STATE_LEAVE_CAR
							OR serverBD.Targets[i].iTargetPedState = PED_AI_STATE_FLEE_ON_FOOT
								serverBD.Targets[i].iBodyguardPedState[j] = PED_AI_STATE_LEAVE_CAR
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [PROCESS_TARGET_PED_BRAIN] TARGET ", i, " BODYGUARD ", j, " PED_AI_STATE_TOUR_IN_CAR -> PED_AI_STATE_LEAVE_CAR")
							ENDIF
						ENDIF
					BREAK
					
					CASE PED_AI_STATE_SCENARIO
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.Targets[i].niBodyguardPed[j])
							IF serverBD.Targets[i].bShouldSeekCover
								serverBD.Targets[i].iBodyguardPedState[j] = PED_AI_STATE_DEFEND_TARGET
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [PROCESS_TARGET_PED_BRAIN] TARGET ", i, " BODYGUARD ", j, " PED_AI_STATE_SCENARIO -> PED_AI_STATE_DEFEND_TARGET")
							
							ENDIF
						ENDIF
					BREAK
					
					CASE PED_AI_STATE_DEFEND_IN_CAR
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.Targets[i].niBodyguardPed[j])
							IF serverBD.Targets[i].iTargetPedState = PED_AI_STATE_LEAVE_CAR
							OR serverBD.Targets[i].iTargetPedState = PED_AI_STATE_FLEE_ON_FOOT
								serverBD.Targets[i].iBodyguardPedState[j] = PED_AI_STATE_LEAVE_CAR
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [PROCESS_TARGET_PED_BRAIN] TARGET ", i, " BODYGUARD ", j, " PED_AI_STATE_DEFEND_IN_CAR -> PED_AI_STATE_LEAVE_CAR")
							
							ENDIF
						ENDIF
					BREAK
					
					CASE PED_AI_STATE_LEAVE_CAR	
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.Targets[i].niBodyguardPed[j])
							IF NOT IS_NET_PED_INJURED(serverBD.Targets[i].niBodyguardPed[j])
								IF NOT IS_PED_IN_ANY_VEHICLE(NET_TO_PED(serverBD.Targets[i].niBodyguardPed[j]))
									serverBD.Targets[i].iBodyguardPedState[j] = PED_AI_STATE_DEFEND_TARGET
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [PROCESS_TARGET_PED_BRAIN] TARGET ", i, " BODYGUARD ", j, " PED_AI_STATE_LEAVE_CAR -> PED_AI_STATE_DEFEND_TARGET")
								ENDIF
							ENDIF
						ENDIF
					BREAK
					
					CASE PED_AI_STATE_DEFEND_TARGET
					BREAK
				ENDSWITCH	
			ENDREPEAT
		ENDREPEAT
	ENDIF
ENDPROC


//PURPOSE:checks to see if the vehicle is stuck or undrivable
FUNC BOOL MTA_VEHICLE_STUCK_CHECKS(INT iTarget)
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.Targets[iTarget].niVeh)
		IF IS_NET_VEHICLE_DRIVEABLE(serverbd.Targets[iTarget].niVeh)
			IF IS_VEHICLE_STUCK_TIMER_UP(NET_TO_VEH(serverbd.Targets[iTarget].niVeh), VEH_STUCK_ON_ROOF, ROOF_TIME)
			OR IS_VEHICLE_STUCK_TIMER_UP(NET_TO_VEH(serverbd.Targets[iTarget].niVeh), VEH_STUCK_ON_SIDE, SIDE_TIME)
			//	PRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     MTA - [PROCESS_TARGET_PED_BODY] [MTA_VEHICLE_STUCK_CHECKS] I think this vehicle is stuck ", iTarget)
				
				RETURN TRUE
				
			ENDIF 
		ELSE
			RETURN TRUE
		ENDIF 
	ENDIF 
	
	RETURN FALSE
ENDFUNC

FUNC PLAYER_INDEX GET_CLOSEST_ATTACKING_PLAYER_TO_TARGET(INT iTarget)
	PLAYER_INDEX playerClosest = INVALID_PLAYER_INDEX()
	FLOAT fClosestDist = 9999999999.0
	FLOAT fDist = 0.0
	INT i
	
	VECTOR vTargetCoors = GET_ENTITY_COORDS(NET_TO_PED(serverBD.Targets[iTarget].niPed), FALSE)
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
			PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
			PED_INDEX PlayerPedId = GET_PLAYER_PED(PlayerId)
			IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PlayerID)
				IF NOT IS_BIT_SET(playerBD[i].iPlayerBitSet, biP_JoinedAsRival)
					IF GB_GET_THIS_PLAYER_GANG_BOSS(PlayerID) = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(serverBD.iBossPartWhoLaunched))
						IF NOT IS_PED_INJURED(PlayerPedid)
							fDist = VDIST2(GET_ENTITY_COORDS(PlayerPedID), vTargetCoors)	
							IF fDist < fClosestDist
								fClosestDist = fDist
								playerClosest = playerId
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
		IF playerClosest != INVALID_PLAYER_INDEX()
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER GET_CLOSEST_ATTACKING_PLAYER_TO_TARGET playerClosest = ", GET_PLAYER_NAME(playerClosest), " Target = ", iTarget)
		ELSE
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER GET_CLOSEST_ATTACKING_PLAYER_TO_TARGET playerClosest = INVALID_PLAYER_INDEX Target = ", iTarget)
		ENDIF
	#ENDIF
	
	RETURN playerClosest
ENDFUNC
FUNC PED_INDEX GET_PED_TO_FLEE(INT iTargetFlee)
	PED_INDEX pedToFlee
	PLAYER_INDEX playerToFlee = GET_CLOSEST_ATTACKING_PLAYER_TO_TARGET(iTargetFlee)
	IF playerToFlee != INVALID_PLAYER_INDEX()
		pedToFlee = GET_PLAYER_PED(playerToFlee)
	ENDIF
	
	RETURN pedToFlee
ENDFUNC

PROC MAINTAIN_MARKER_FOR_TARGET(INT iTarget)
	IF NOT SHOULD_LOCAL_PLAYER_SEE_GB_HEADHUNTER_HUD()
		EXIT
	ENDIF
	
	IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) < GB_UI_LEVEL_BACKGROUND
		EXIT
	ENDIF
	
	IF NOT DID_MY_BOSS_LAUNCH_GB_HEADHUNTER()
	AND NOT DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
		EXIT
	ENDIF
	
	IF HAS_ANY_MISSION_END_CONDITION_BEEN_MET()
		EXIT
	ENDIF
	
	
	INT r, g, b, a
	VECTOR vCoord
	
	IF DID_MY_BOSS_LAUNCH_GB_HEADHUNTER()
		GET_HUD_COLOUR(HUD_COLOUR_RED, r, g, b, a)
	ELSE
		GET_HUD_COLOUR(HUD_COLOUR_BLUE, r, g, b, a)
	ENDIF
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.Targets[iTarget].niPed)
		IF NOT IS_NET_PED_INJURED(serverBD.Targets[iTarget].niPed)
			IF NOT IS_PED_SITTING_IN_ANY_VEHICLE(NET_TO_PED(serverBD.Targets[iTarget].niPed))
				vCoord = GET_ENTITY_COORDS(NET_TO_PED(serverBD.Targets[iTarget].niPed))
				DRAW_MARKER(MARKER_ARROW, vCoord+<<0,0,2>>, <<0,0,0>>, <<180,0,0>>, <<0.5,0.5,0.5>>, r, g, b, 100, TRUE, TRUE)
			ELSE
				VEHICLE_INDEX veh = GET_VEHICLE_PED_IS_IN(NET_TO_PED(serverBD.Targets[iTarget].niPed))
				FM_EVENTDRAW_MARKER_ABOVE_VEHICLE(veh, r, g, b)
			ENDIF
		ENDIF
	ENDIF
		
ENDPROC

/// PURPOSE:
///    Figure out if the target should stop playing their idle scenario and seek cover
/// PARAMS:
///    iTarget - 
PROC MAINTAIN_ON_FOOT_TARGET_SHOULD_SEEK_COVER(INT iTarget)
	IF serverBD.Targets[iTarget].iTargetType != TARGET_TYPE_ON_FOOT_WITH_BODYGUARDS
		EXIT
	ENDIF
	
	IF serverBD.Targets[iTarget].bShouldSeekCover
		EXIT
	ENDIF
	
	IF GET_MY_BOSS_PARTICPANT_ID() != serverBD.iBossPartWhoLaunched
		EXIT
	ENDIF
	
	IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iOnFootTargetPedShouldSeekCover, iTarget)
		EXIT
	ENDIF
	
	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.Targets[iTarget].niPed)
		EXIT
	ENDIF
	
	IF IS_NET_PED_INJURED(serverBD.Targets[iTarget].niPed)
		SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iOnFootTargetPedShouldSeekCover, iTarget)
		EXIT
	ENDIF
	
	PED_INDEX pedTarget = NET_TO_PED(serverBD.Targets[iTarget].niPed)
	
	VECTOR vPlayerLoc = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
	
	VECTOR vTargetLoc = GET_ENTITY_COORDS(pedTarget, FALSE)
	
	//-- React if close to target
	IF VDIST2(vPlayerLoc, vTargetLoc) < 62500 // 250m
		SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iOnFootTargetPedShouldSeekCover, iTarget)
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [MAINTAIN_ON_FOOT_TARGET_SHOULD_SEEK_COVER] SET TARGET ", iTarget, " SHOULD REACT AS PLAYER CLOSER THAN 250M")
		EXIT
	ENDIF
	
	IF IS_PED_INJURED(PLAYER_PED_ID())
		EXIT
	ENDIF
	
	//-- React if close to bodyguards
	PED_INDEX pedBodyguard
	INT i
	
	REPEAT MAX_BODYGUARDS_PER_TARGET i
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.Targets[iTarget].niBodyguardPed[i])
			IF IS_NET_PED_INJURED(serverBD.Targets[iTarget].niBodyguardPed[i])
				SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iOnFootTargetPedShouldSeekCover, iTarget)
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [MAINTAIN_ON_FOOT_TARGET_SHOULD_SEEK_COVER] SET TARGET ", iTarget, " SHOULD REACT AS BODYGUARD ", i, " IS INJURED")
			ELSE
				pedBodyguard = NET_TO_PED(serverBD.Targets[iTarget].niBodyguardPed[i])
				
				IF GET_MY_BOSS_PARTICPANT_ID() = serverBD.iBossPartWhoLaunched
					//-- Player close to bodyguard
					IF VDIST2(vPlayerLoc, vTargetLoc) < 40000 // 200m
						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iOnFootTargetPedShouldSeekCover, iTarget)
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [MAINTAIN_ON_FOOT_TARGET_SHOULD_SEEK_COVER] SET TARGET ", iTarget, " SHOULD REACT AS BODYGUARD ", i, " IS WITHIN 200M OF PLAYER")
						EXIT
					ENDIF
					
					//--Player aiming at bodyguard
					IF IS_PLAYER_AIMING_AT_PED(pedBodyguard)
						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iOnFootTargetPedShouldSeekCover, iTarget)
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [MAINTAIN_ON_FOOT_TARGET_SHOULD_SEEK_COVER] SET TARGET ", iTarget, " SHOULD REACT AS BODYGUARD ", i, " IS BEING AIMED AT BY PLAYER")
						EXIT
					ENDIF
					
					
				ENDIF
					
				
			ENDIF
		ENDIF
	ENDREPEAT
	
	
	
	
ENDPROC

/// PURPOSE:
///    Figure out if the target should start attacking the players, rather than just seeking cover
/// PARAMS:
///    iTarget - 
PROC MAINTAIN_ON_FOOT_TARGET_SHOULD_ENTER_COMBAT(INT iTarget)
	
	IF serverBD.Targets[iTarget].iTargetType != TARGET_TYPE_ON_FOOT_WITH_BODYGUARDS
		EXIT
	ENDIF
	
	IF serverBD.Targets[iTarget].bShouldEnterCombat
		EXIT
	ENDIF
	
	IF GET_MY_BOSS_PARTICPANT_ID() != serverBD.iBossPartWhoLaunched
		EXIT
	ENDIF
	
	IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iOnFootTargetPedShouldEnterCombat, iTarget)
		EXIT
	ENDIF
	
	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.Targets[iTarget].niPed)
		EXIT
	ENDIF
	
	IF IS_NET_PED_INJURED(serverBD.Targets[iTarget].niPed)
		SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iOnFootTargetPedShouldSeekCover, iTarget)
		EXIT
	ENDIF
	
	PED_INDEX pedTarget = NET_TO_PED(serverBD.Targets[iTarget].niPed)
	
	VECTOR vPlayerLoc = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
	
	VECTOR vTargetLoc = GET_ENTITY_COORDS(pedTarget, FALSE)
	
	//-- React if close to target
	IF VDIST2(vPlayerLoc, vTargetLoc) < 5625 // 75m
		SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iOnFootTargetPedShouldEnterCombat, iTarget)
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [MAINTAIN_ON_FOOT_TARGET_SHOULD_ENTER_COMBAT] SET TARGET ", iTarget, " SHOULD ENTER COMBAT AS PLAYER CLOSER THAN 75")
		EXIT
	ENDIF
	
	IF IS_PED_INJURED(PLAYER_PED_ID())
		EXIT
	ENDIF
	
	//-- Enter combat if all bodyguards dead

	INT i
	BOOL bBodyguardStillAlive = FALSE
	REPEAT MAX_BODYGUARDS_PER_TARGET i
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.Targets[iTarget].niBodyguardPed[i])
			IF NOT IS_NET_PED_INJURED(serverBD.Targets[iTarget].niBodyguardPed[i])
				bBodyguardStillAlive = TRUE
			ENDIF
		ENDIF
	ENDREPEAT
				
	IF NOT bBodyguardStillAlive
		SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iOnFootTargetPedShouldEnterCombat, iTarget)
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [MAINTAIN_ON_FOOT_TARGET_SHOULD_ENTER_COMBAT] SET TARGET ", iTarget, " SHOULD ENTER COMBAT AS ALL BODYGUARDS DEAD")
	ENDIF
	
	
	
ENDPROC

INT iActiveDamageTrackingBitset
INT iStaggeredDamageTrackCount
/// PURPOSE: 
///    Activate damage tracking on on-foot peds . Every player needs to turn it on
PROC MAINTAIN_ACTIVATE_DAMAGE_TRACKING_CLIENT()
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.Targets[iStaggeredDamageTrackCount].niPed)
		IF NOT IS_NET_PED_INJURED(serverBD.Targets[iStaggeredDamageTrackCount].niPed)
			IF NOT IS_BIT_SET(iActiveDamageTrackingBitset, iStaggeredDamageTrackCount)
				ACTIVATE_DAMAGE_TRACKER_ON_NETWORK_ID(serverBD.Targets[iStaggeredDamageTrackCount].niPed, TRUE)
				SET_BIT(iActiveDamageTrackingBitset, iStaggeredDamageTrackCount)
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [MAINTAIN_ACTIVATE_DAMAGE_TRACKING_CLIENT] Turned on damage tracking for target ", iStaggeredDamageTrackCount)
			ENDIF
		ENDIF
	ENDIF
	
	iStaggeredDamageTrackCount++
	
	IF iStaggeredDamageTrackCount = MAX_TARGETS
		iStaggeredDamageTrackCount = 0
	ENDIF
ENDPROC


PROC MAINTAIN_TARGET_FELL_THROUGH_WORLD_CHECK(INT iTarget)
	
	FLOAT fSpeed
	FLOAT fGroundZ
	VECTOR vLoc, vLocHigh
	IF serverbd.Targets[iTarget].iTargetType = TARGET_TYPE_DRIVER_WITH_BODYGUARDS
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.Targets[iTarget].niPed)
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverbd.Targets[iTarget].niPed)
				IF NOT IS_NET_PED_INJURED(serverbd.Targets[iTarget].niPed)
					IF NOT IS_PED_IN_ANY_VEHICLE(NET_TO_PED(serverbd.Targets[iTarget].niPed))
						fSpeed = GET_ENTITY_SPEED(NET_TO_PED(serverbd.Targets[iTarget].niPed))
						IF fSpeed < 0.1
							
							vLoc = GET_ENTITY_COORDS(NET_TO_PED(serverbd.Targets[iTarget].niPed))
							vLocHigh = vLoc
							vLocHigh.z += 500.0
							IF GET_GROUND_Z_FOR_3D_COORD(vLocHigh, fGroundZ)
								IF ABSF(vLoc.z - fGroundZ) > 30.0
									
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [MAINTAIN_TARGET_FELL_THROUGH_WORLD_CHECK] Killing target ", iTarget, " as below map. Details...")
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [MAINTAIN_TARGET_FELL_THROUGH_WORLD_CHECK] 				Speed = " , fSpeed)
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [MAINTAIN_TARGET_FELL_THROUGH_WORLD_CHECK] 				Location = " , vLoc) 
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [MAINTAIN_TARGET_FELL_THROUGH_WORLD_CHECK] 				Ground Z = " , fGroundZ)
									SET_ENTITY_HEALTH(NET_TO_PED(serverbd.Targets[iTarget].niPed) , 0)
								ENDIF
							ENDIF
						ENDIF
								
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
					
ENDPROC


PROC PROCESS_TARGET_PED_BODY()
//	IF HAS_ANY_MISSION_END_CONDITION_BEEN_MET()
//		EXIT
//	ENDIF
	INT i
	INT j
	PED_INDEX pedToFlee
	SCRIPTTASKSTATUS status
	PLAYER_INDEX playerClosestAttacker = INVALID_PLAYER_INDEX()
	REPEAT MAX_TARGETS i
		//-- Aiming at target?
		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerAimAtTargetBitSet, i)
			IF NOT IS_BIT_SET(serverBD.iTargetAimedAtBitSet, i)
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.Targets[i].niPed)
					IF NOT IS_NET_PED_INJURED(serverbd.Targets[i].niPed)
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							IF GET_MY_BOSS_PARTICPANT_ID() = serverBD.iBossPartWhoLaunched
								IF IS_PLAYER_AIMING_AT_PED(NET_TO_PED(serverbd.Targets[i].niPed)) 
									SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerAimAtTargetBitSet, i)
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [PROCESS_TARGET_PED_BODY] SET iPlayerAimAtTargetBitSet AS I AM AIMING AT PED = ", i)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//-- Target veh stuck?
		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerTargetVehStuckBitSet, i)
			IF NOT IS_BIT_SET(serverBD.iTargetVehStuckBitSet, i)
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.Targets[i].niVeh)
					IF MTA_VEHICLE_STUCK_CHECKS(i)
						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerTargetVehStuckBitSet, i)
						PRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [PROCESS_TARGET_PED_BODY] I think this vehicle is stuck ", i)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//-- Bullets near target?
		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerShotsNearTargetBitSet, i)
			IF NOT IS_BIT_SET(serverBD.iTargetNearbyShotsBitset, i)
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.Targets[i].niPed)
					IF NOT IS_NET_PED_INJURED(serverbd.Targets[i].niPed)
						IF IS_BULLET_IN_AREA(GET_ENTITY_COORDS(NET_TO_PED(serverbd.Targets[i].niPed)), 10.0)
							SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerShotsNearTargetBitSet, i)
							PRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [PROCESS_TARGET_PED_BODY] I think shots are near this target ", i)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		MAINTAIN_TARGET_FELL_THROUGH_WORLD_CHECK(i)
		
		IF SHOULD_LOCAL_PLAYER_SEE_GB_HEADHUNTER_HUD()
			IF NOT DOES_BLIP_EXIST(blipTarget[i])
				IF NOT HAS_ANY_MISSION_END_CONDITION_BEEN_MET()
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.Targets[i].niPed)
						IF NOT IS_NET_PED_INJURED(serverbd.Targets[i].niPed)
							blipTarget[i] = ADD_BLIP_FOR_ENTITY(NET_TO_PED(serverbd.Targets[i].niPed))
							IF GET_MY_BOSS_PARTICPANT_ID() = serverBD.iBossPartWhoLaunched
								SET_BLIP_PRIORITY(blipTarget[i], BLIP_PRIORITY_HIGHEST_SPECIAL_HIGH)
								SET_BLIP_SPRITE(blipTarget[i], RADAR_TRACE_TEMP_4) //
								
								SET_BLIP_COLOUR_FROM_HUD_COLOUR(blipTarget[i],HUD_COLOUR_RED)
							ELSE
								SET_BLIP_SPRITE(blipTarget[i], RADAR_TRACE_VIP)
								SET_BLIP_COLOUR_FROM_HUD_COLOUR(blipTarget[i],HUD_COLOUR_BLUE)	
							//	SET_BLIP_AS_FRIENDLY(blipTarget[i], TRUE)
							ENDIF
							
							SET_BLIP_FLASHES(blipTarget[i], TRUE)
							SET_BLIP_FLASH_TIMER(blipTarget[i], 7000)
							SET_BLIP_SCALE(blipTarget[i], 1.1)
							SET_BLIP_NAME_FROM_TEXT_FILE(blipTarget[i], "HUNT_BLIP")
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.Targets[i].niPed)
					IF IS_NET_PED_INJURED(serverbd.Targets[i].niPed)
						REMOVE_BLIP(blipTarget[i])
					ELIF HAS_ANY_MISSION_END_CONDITION_BEEN_MET()
						REMOVE_BLIP(blipTarget[i])
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF DOES_BLIP_EXIST(blipTarget[i])
				REMOVE_BLIP(blipTarget[i])
			ENDIF
		ENDIF
		
		//-- Check if on foot target and bodyguard should stop playing their scenario and seek cover
		MAINTAIN_ON_FOOT_TARGET_SHOULD_SEEK_COVER(i)
		
		//-- Check if on foot target should start attacking the players, rather than seek cover
		MAINTAIN_ON_FOOT_TARGET_SHOULD_ENTER_COMBAT(i)
		
		//-- Marker
		MAINTAIN_MARKER_FOR_TARGET(i)
		
		SWITCH serverBD.Targets[i].iTargetPedState
			CASE PED_AI_STATE_TOUR_IN_CAR
				IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.Targets[i].niVeh)
						IF IS_NET_VEHICLE_DRIVEABLE(serverbd.Targets[i].niVeh)
							IF NOT IS_NET_PED_INJURED(serverbd.Targets[i].niPed)
								IF IS_PED_SITTING_IN_VEHICLE(NET_TO_PED(serverbd.Targets[i].niPed), NET_TO_VEH(serverbd.Targets[i].niVeh))
									IF (GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverbd.Targets[i].niPed), SCRIPT_TASK_VEHICLE_DRIVE_WANDER) != PERFORMING_TASK
									AND GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverbd.Targets[i].niPed), SCRIPT_TASK_VEHICLE_DRIVE_WANDER) != WAITING_TO_START_TASK)
										TASK_VEHICLE_DRIVE_WANDER(NET_TO_PED(serverbd.Targets[i].niPed), NET_TO_VEH(serverbd.Targets[i].niVeh), 20.0, DRIVINGMODE_STOPFORCARS)
										
										CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [PROCESS_TARGET_PED_BODY] I've given ped TASK_VEHICLE_DRIVE_WANDER PED = ", i)
									ENDIF
									
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE PED_AI_STATE_FLEE_IN_CAR
				IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.Targets[i].niVeh)
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.Targets[i].niPed)
							IF IS_NET_VEHICLE_DRIVEABLE(serverbd.Targets[i].niVeh)
								IF NOT IS_NET_PED_INJURED(serverbd.Targets[i].niPed)
									IF IS_PED_SITTING_IN_VEHICLE(NET_TO_PED(serverbd.Targets[i].niPed), NET_TO_VEH(serverbd.Targets[i].niVeh))
										IF (GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverbd.Targets[i].niPed), SCRIPT_TASK_VEHICLE_DRIVE_WANDER) != PERFORMING_TASK
										AND GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverbd.Targets[i].niPed), SCRIPT_TASK_VEHICLE_DRIVE_WANDER) != WAITING_TO_START_TASK)
										OR IS_BIT_SET(serverBD.iTargetDriveFastBitSet, i)
											TASK_VEHICLE_DRIVE_WANDER(NET_TO_PED(serverbd.Targets[i].niPed), NET_TO_VEH(serverbd.Targets[i].niVeh), 30.0, DRIVINGMODE_AVOIDCARS)
											IF IS_BIT_SET(serverBD.iTargetDriveFastBitSet, i)
												CLEAR_BIT(serverBD.iTargetDriveFastBitSet, i)
											ENDIF

											
											CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [PROCESS_TARGET_PED_BODY] I've given ped TASK_VEHICLE_DRIVE_WANDER FAST PED = ", i)
										ENDIF
										
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE PED_AI_STATE_LEAVE_CAR
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.Targets[i].niPed)
					IF NOT IS_NET_PED_INJURED(serverbd.Targets[i].niPed)
						IF GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverbd.Targets[i].niPed), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != PERFORMING_TASK
						AND GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverbd.Targets[i].niPed), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != WAITING_TO_START_TASK
							IF MAINTAIN_OWNERSHIP_OF_NETWORK_ID(serverbd.Targets[i].niPed)
								IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverbd.Targets[i].niPed)
									TASK_LEAVE_ANY_VEHICLE(NET_TO_PED(serverbd.Targets[i].niPed))
									
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [PROCESS_TARGET_PED_BODY] I've given ped TASK_LEAVE_ANY_VEHICLE PED = ", i)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE PED_AI_STATE_TOUR_ON_FOOT
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.Targets[i].niPed)
					IF NOT IS_NET_PED_INJURED(serverbd.Targets[i].niPed)
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							IF GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverbd.Targets[i].niPed), SCRIPT_TASK_WANDER_STANDARD) != PERFORMING_TASK
							AND GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverbd.Targets[i].niPed), SCRIPT_TASK_WANDER_STANDARD) != WAITING_TO_START_TASK
												
								IF MAINTAIN_OWNERSHIP_OF_NETWORK_ID(serverbd.Targets[i].niPed)
									IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverbd.Targets[i].niPed)
										TASK_WANDER_STANDARD(NET_TO_PED(serverbd.Targets[i].niPed))
										CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [PROCESS_TARGET_PED_BODY] I've given ped TASK_WANDER_STANDARD PED = ", i)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
				
			CASE PED_AI_STATE_FLEE_ON_FOOT
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.Targets[i].niPed)
					IF NOT IS_NET_PED_INJURED(serverbd.Targets[i].niPed)
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							IF GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverbd.Targets[i].niPed), SCRIPT_TASK_SMART_FLEE_PED) != PERFORMING_TASK
							AND GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverbd.Targets[i].niPed), SCRIPT_TASK_SMART_FLEE_PED) != WAITING_TO_START_TASK
												
								IF MAINTAIN_OWNERSHIP_OF_NETWORK_ID(serverbd.Targets[i].niPed)
									IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverbd.Targets[i].niPed)
										pedToFlee = GET_PED_TO_FLEE(i)
										IF NOT IS_PED_INJURED(pedToFlee)
											TASK_SMART_FLEE_PED(NET_TO_PED(serverbd.Targets[i].niPed), pedToFlee, 500, -1, FALSE, TRUE)							
											CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [PROCESS_TARGET_PED_BODY] I've given ped TASK_SMART_FLEE_PED PED = ", i)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE PED_AI_STATE_DEFEND_TARGET
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.Targets[i].niPed)
					IF NOT IS_NET_PED_INJURED(serverBD.Targets[i].niPed)
						IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.Targets[i].niPed) // 2748344
							SET_PED_RESET_FLAG(NET_TO_PED(serverBD.Targets[i].niPed), PRF_PreventAllMeleeTakedowns,TRUE)
						ENDIF
						
						status = GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.Targets[i].niPed), SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED)
						IF status != PERFORMING_TASK
						AND status != WAITING_TO_START_TASK
							IF MAINTAIN_OWNERSHIP_OF_NETWORK_ID(serverBD.Targets[i].niPed)
								IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverbd.Targets[i].niPed)
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NET_TO_PED(serverBD.Targets[i].niPed), FALSE)
									TASK_COMBAT_HATED_TARGETS_AROUND_PED(NET_TO_PED(serverBD.Targets[i].niPed), 299 )
									
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [PROCESS_TARGET_PED_BODY] I HAVE GIVEN TASK_COMBAT_HATED_TARGETS_AROUND_PED TARGET ", i)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
			BREAK
			
			CASE PED_AI_STATE_TAKE_COVER
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.Targets[i].niPed)
					IF NOT IS_NET_PED_INJURED(serverBD.Targets[i].niPed)
						IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.Targets[i].niPed) // 2748344
							SET_PED_RESET_FLAG(NET_TO_PED(serverBD.Targets[i].niPed), PRF_PreventAllMeleeTakedowns,TRUE)
						ENDIF
						
						status = GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.Targets[i].niPed), SCRIPT_TASK_SEEK_COVER_FROM_PED) //SCRIPT_TASK_STAY_IN_COVER
						IF status != PERFORMING_TASK
						AND status != WAITING_TO_START_TASK
							IF MAINTAIN_OWNERSHIP_OF_NETWORK_ID(serverBD.Targets[i].niPed)
								IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverbd.Targets[i].niPed)
							//		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NET_TO_PED(serverBD.Targets[i].niPed), FALSE)
									SET_PED_TO_LOAD_COVER(NET_TO_PED(serverBD.Targets[i].niPed), TRUE)
								//	TASK_STAY_IN_COVER(NET_TO_PED(serverBD.Targets[i].niPed))
									FLOAT fClosest2
									fClosest2 = 0.0
									playerClosestAttacker = GET_CLOSEST_ATTACK_PLAYER_TO_TARGET(i, fClosest2)
									IF playerClosestAttacker != INVALID_PLAYER_INDEX()
										TASK_SEEK_COVER_FROM_PED(NET_TO_PED(serverbd.Targets[i].niPed), GET_PLAYER_PED(playerClosestAttacker), -1)
										CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [PROCESS_TARGET_PED_BODY] I HAVE GIVEN TASK_SEEK_COVER_FROM_PED TARGET ", i, " TAKING COVER FROM ", GET_PLAYER_NAME(playerClosestAttacker))
									ELSE
										CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [PROCESS_TARGET_PED_BODY] FAILED TO FIND CLOSEST ATTACKER FOR TASK_SEEK_COVER_FROM_PED!")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE PED_AI_STATE_SCENARIO
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.Targets[i].niPed)
					IF NOT IS_NET_PED_INJURED(serverBD.Targets[i].niPed)
						status = GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.Targets[i].niPed), SCRIPT_TASK_START_SCENARIO_IN_PLACE)
						IF status != PERFORMING_TASK
						AND status != WAITING_TO_START_TASK
							IF MAINTAIN_OWNERSHIP_OF_NETWORK_ID(serverBD.Targets[i].niPed)
								IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverbd.Targets[i].niPed)
									
									TASK_START_SCENARIO_IN_PLACE(NET_TO_PED(serverBD.Targets[i].niPed),"WORLD_HUMAN_SMOKING")	
									CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [PROCESS_TARGET_PED_BODY] I HAVE GIVEN SCRIPT_TASK_START_SCENARIO_IN_PLACE TARGET ", i)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
			BREAK
			
		ENDSWITCH
		
		j = 0
		REPEAT MAX_BODYGUARDS_PER_TARGET j			
			SWITCH serverBD.Targets[i].iBodyguardPedState[j]
				CASE PED_AI_STATE_CREATED
				BREAK
				
				CASE PED_AI_STATE_TOUR_IN_CAR
				BREAK
				
				CASE PED_AI_STATE_SCENARIO	
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.Targets[i].niBodyguardPed[j])
						IF NOT IS_NET_PED_INJURED(serverBD.Targets[i].niBodyguardPed[j])
							status = GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.Targets[i].niBodyguardPed[j]), SCRIPT_TASK_START_SCENARIO_IN_PLACE)
							IF status != PERFORMING_TASK
							AND status != WAITING_TO_START_TASK
								IF MAINTAIN_OWNERSHIP_OF_NETWORK_ID(serverBD.Targets[i].niBodyguardPed[j])
									IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverbd.Targets[i].niBodyguardPed[j])
										TASK_START_SCENARIO_IN_PLACE(NET_TO_PED(serverBD.Targets[i].niBodyguardPed[j]),"WORLD_HUMAN_GUARD_STAND")	
										CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [PROCESS_TARGET_PED_BODY] I HAVE GIVEN TARGET ", i, " BODYGUARD ", j, " TASK_START_SCENARIO_IN_PLACE")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
				BREAK
				
				CASE PED_AI_STATE_DEFEND_IN_CAR
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.Targets[i].niBodyguardPed[j])
						IF NOT IS_NET_PED_INJURED(serverBD.Targets[i].niBodyguardPed[j])
							status = GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.Targets[i].niBodyguardPed[j]), SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED)
							IF status != PERFORMING_TASK
							AND status != WAITING_TO_START_TASK
								IF MAINTAIN_OWNERSHIP_OF_NETWORK_ID(serverBD.Targets[i].niBodyguardPed[j])
									IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverbd.Targets[i].niBodyguardPed[j])
										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NET_TO_PED(serverBD.Targets[i].niBodyguardPed[j]), FALSE)
										TASK_COMBAT_HATED_TARGETS_AROUND_PED(NET_TO_PED(serverBD.Targets[i].niBodyguardPed[j]), 299 )
										
										CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [PROCESS_TARGET_PED_BODY] - PED_AI_STATE_DEFEND_IN_CAR I HAVE GIVEN TARGET ", i, " BODYGUARD ", j, " TASK_COMBAT_HATED_TARGETS_AROUND_PED")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
				BREAK
				
				CASE PED_AI_STATE_DEFEND_TARGET
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.Targets[i].niBodyguardPed[j])
						IF NOT IS_NET_PED_INJURED(serverBD.Targets[i].niBodyguardPed[j])
							status = GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.Targets[i].niBodyguardPed[j]), SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED)
							IF status != PERFORMING_TASK
							AND status != WAITING_TO_START_TASK
								IF MAINTAIN_OWNERSHIP_OF_NETWORK_ID(serverBD.Targets[i].niBodyguardPed[j])
									IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverbd.Targets[i].niBodyguardPed[j])
										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NET_TO_PED(serverBD.Targets[i].niBodyguardPed[j]), FALSE)
										TASK_COMBAT_HATED_TARGETS_AROUND_PED(NET_TO_PED(serverBD.Targets[i].niBodyguardPed[j]), 299 )
										
										CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [PROCESS_TARGET_PED_BODY] - PED_AI_STATE_DEFEND_TARGET I HAVE GIVEN TARGET ", i, " BODYGUARD ", j, " TASK_COMBAT_HATED_TARGETS_AROUND_PED")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
				BREAK
				
				CASE PED_AI_STATE_LEAVE_CAR
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.Targets[i].niBodyguardPed[j])
						IF NOT IS_NET_PED_INJURED(serverBD.Targets[i].niBodyguardPed[j])
							IF IS_PED_SITTING_IN_ANY_VEHICLE(NET_TO_PED(serverBD.Targets[i].niBodyguardPed[j]))
								status = GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.Targets[i].niBodyguardPed[j]), SCRIPT_TASK_LEAVE_ANY_VEHICLE)
								IF status != PERFORMING_TASK
								AND status != WAITING_TO_START_TASK
									IF MAINTAIN_OWNERSHIP_OF_NETWORK_ID(serverBD.Targets[i].niBodyguardPed[j])
										IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverbd.Targets[i].niBodyguardPed[j])
											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NET_TO_PED(serverBD.Targets[i].niBodyguardPed[j]), TRUE)
											TASK_LEAVE_ANY_VEHICLE(NET_TO_PED(serverBD.Targets[i].niBodyguardPed[j]), 299 )
											
											SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(serverBD.Targets[i].niBodyguardPed[j]), CA_LEAVE_VEHICLES, TRUE)
											
											CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [PROCESS_TARGET_PED_BODY] I HAVE GIVEN TARGET ", i, " BODYGUARD ", j, " TASK_LEAVE_ANY_VEHICLE")
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		ENDREPEAT
	ENDREPEAT
	
	IF DID_MY_BOSS_LAUNCH_GB_HEADHUNTER()
		MAINTAIN_BODYGUARD_BLIPS()
	ENDIF
ENDPROC

PROC INIT_TARGETS_SERVER()
	
	
	serverbd.Targets[0].iTargetType = TARGET_TYPE_ON_FOOT_WITH_BODYGUARDS // //TARGET_TYPE_SOLO_ON_FOOT //TARGET_TYPE_DRIVER_WITH_BODYGUARDS
	serverbd.Targets[0].mVeh = BALLER3
	serverbd.Targets[0].mPed = A_M_Y_BUSINESS_03
	
	serverbd.Targets[1].iTargetType = TARGET_TYPE_ON_FOOT_WITH_BODYGUARDS //TARGET_TYPE_DRIVER_WITH_BODYGUARDS
	serverbd.Targets[1].mVeh = DUBSTA
	serverbd.Targets[1].mPed = A_M_Y_BUSINESS_03
	
	serverbd.Targets[2].iTargetType = TARGET_TYPE_DRIVER_WITH_BODYGUARDS // //TARGET_TYPE_DRIVER_WITH_BODYGUARDS
	serverbd.Targets[2].mVeh = cog552
	serverbd.Targets[2].mPed = A_M_Y_BUSINESS_03
	
	serverbd.Targets[3].iTargetType = TARGET_TYPE_DRIVER_WITH_BODYGUARDS
	serverbd.Targets[3].mVeh = BALLER5
	serverbd.Targets[3].mPed = A_M_Y_BUSINESS_03
	
//	serverbd.Targets[4].iTargetType = TARGET_TYPE_ON_FOOT_WITH_BODYGUARDS
//	serverbd.Targets[4].mVeh = BALLER3
//	serverbd.Targets[4].mPed = A_M_Y_BUSINESS_03
//	
//	serverbd.Targets[5].iTargetType = TARGET_TYPE_ON_FOOT_WITH_BODYGUARDS
//	serverbd.Targets[5].mVeh = DUBSTA
//	serverbd.Targets[5].mPed = A_M_Y_BUSINESS_03
//	
//	serverbd.Targets[6].iTargetType = TARGET_TYPE_ON_FOOT_WITH_BODYGUARDS
//	serverbd.Targets[6].mVeh = COG55
//	serverbd.Targets[6].mPed = A_M_Y_BUSINESS_03
//	
//	serverbd.Targets[7].iTargetType = TARGET_TYPE_ON_FOOT_WITH_BODYGUARDS
//	serverbd.Targets[7].mVeh = ROCOTO
//	serverbd.Targets[7].mPed = A_M_Y_BUSINESS_03
//	
//	serverbd.Targets[8].iTargetType = TARGET_TYPE_ON_FOOT_WITH_BODYGUARDS
//	serverbd.Targets[8].mVeh = BALLER3
//	serverbd.Targets[8].mPed = A_M_Y_BUSINESS_03
	
//	serverbd.Targets[9].iTargetType = TARGET_TYPE_ON_FOOT_WITH_BODYGUARDS
//	serverbd.Targets[9].mVeh = DUBSTA
//	serverbd.Targets[9].mPed = A_M_Y_BUSINESS_03
//	
//	
	
		
ENDPROC

FUNC INT GET_MAP_AREA()
	IF serverBD.iMapArea != -1
		RETURN serverBD.iMapArea
	ENDIF
	
	
//	serverBD.iMapArea = HEADHUNTER_AREA_COUNTRY //HEADHUNTER_AREA_CITY // // // // 
	
	#IF IS_DEBUG_BUILD
		IF serverBD.iDebugVariation > -1
			IF serverBD.iDebugVariation <= 4
				serverBD.iMapArea = HEADHUNTER_AREA_CITY
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER [INIT_MAP_AREA] serverBD.iMapArea = HEADHUNTER_AREA_CITY AS serverBD.iDebugVariation = ", serverBD.iDebugVariation)
			ELSE
				serverBD.iMapArea = HEADHUNTER_AREA_COUNTRY
				CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER [INIT_MAP_AREA] serverBD.iMapArea = HEADHUNTER_AREA_COUNTRY AS serverBD.iDebugVariation = ", serverBD.iDebugVariation)
			
			ENDIF
		ENDIF
	#ENDIF
	
	IF serverBD.iMapArea = -1
		//serverBD.iMapArea = GET_RANDOM_INT_IN_RANGE(0, 2)
		IF GET_HASH_OF_MAP_AREA_AT_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)) = MAP_AREA_CITY
			serverBD.iMapArea = HEADHUNTER_AREA_CITY
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER [INIT_MAP_AREA] serverBD.iMapArea = HEADHUNTER_AREA_CITY AS GET_HASH_OF_MAP_AREA_AT_COORDS = MAP_AREA_CITY")
		ELSE
			serverBD.iMapArea = HEADHUNTER_AREA_COUNTRY
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER [INIT_MAP_AREA] serverBD.iMapArea = HEADHUNTER_AREA_COUNTRY AS GET_HASH_OF_MAP_AREA_AT_COORDS != MAP_AREA_CITY")
		
		ENDIF
	ENDIF
		
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER [INIT_MAP_AREA] - serverBD.iMapArea = ", serverBD.iMapArea)
	
	RETURN serverBD.iMapArea
ENDFUNC

PROC INIT_TARGETS_CLIENT()
	INT i
	INT j
	BOOL bOnLaunchingGang = ((PARTICIPANT_ID_TO_INT() = serverBD.iBossPartWhoLaunched) OR (GET_MY_BOSS_PARTICPANT_ID() = serverBD.iBossPartWhoLaunched))
	
	REPEAT MAX_TARGETS i
		IF NOT bOnLaunchingGang
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbD.Targets[i].niPed)
			//	SET_ENTITY_CAN_BE_DAMAGED(NET_TO_PED(serverbD.Targets[i].niPed), FALSE)
				SET_PED_CAN_BE_TARGETTED_BY_PLAYER(NET_TO_PED(serverbD.Targets[i].niPed), PLAYER_ID(), FALSE)
			//	CPRINTLN(DEBUG_NET_MAGNATE,	"     ---------->     HEADHUNTER - [INIT_TARGETS_CLIENT] I CAN'T DAMAGE THIS TARGET ", i)
			ENDIF
			
			
			
			j = 0
			REPEAT MAX_BODYGUARDS_PER_TARGET j
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.Targets[i].niBodyguardPed[j])
					SET_PED_CAN_BE_TARGETTED_BY_PLAYER(NET_TO_PED(serverbD.Targets[i].niBodyguardPed[j]), PLAYER_ID(), FALSE)
				ENDIF
					
			ENDREPEAT
		ENDIF
		
		
	ENDREPEAT
	
ENDPROC

FUNC BOOL DOES_TARGET_EXIST(INT iTarget)
	SWITCH serverbd.Targets[iTarget].iTargetType
		CASE TARGET_TYPE_DRIVER_WITH_BODYGUARDS
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.Targets[iTarget].niPed)
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.Targets[iTarget].niVeh)
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		
		CASE TARGET_TYPE_SOLO_ON_FOOT
		CASE TARGET_TYPE_ON_FOOT_WITH_BODYGUARDS
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.Targets[iTarget].niPed)
				
					RETURN TRUE
				
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC ADD_HEADHUNTER_SCENARIO_BLOCKING_AREA(VECTOR vLoc)
	INT iBitToUse = -1
	INT i
	REPEAT MAX_SCENARIO_BLOCKING_AREAS i
		IF iBitToUse = -1
			IF NOT IS_BIT_SET(serverBD.iScenBlockBitset, i)
				iBitToUse = i
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF iBitToUse < 0
		EXIT
	ENDIF
	
	IF ARE_VECTORS_EQUAL(vLoc, <<-1596.3986, 3058.8145, 31.5661>>) 
		serverBD.scenBlockingArea[iBitToUse] = ADD_SCENARIO_BLOCKING_AREA(<<-1631.3120, 3027.8889, 30.8859>>,  <<-1560.5502, 3137.9331, 33.9384>>, TRUE)
		SET_BIT(serverBD.iScenBlockBitset, iBitToUse)
		
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [ADD_HEADHUNTER_SCENARIO_BLOCKING_AREA] ADD SCNEARIO BLOCKING AREA vLoc = ", vLoc, " iBitToUse = ", iBitToUse) 
	ENDIF
	
	IF ARE_VECTORS_EQUAL(vLoc,<<2328.0659, 2576.5583, 45.6677>>) 
		serverBD.scenBlockingArea[iBitToUse] = ADD_SCENARIO_BLOCKING_AREA(<<2266.3396, 2486.0745, 54.9760>>, <<2413.6621, 2679.9817, 43.5461>>, TRUE)
		SET_BIT(serverBD.iScenBlockBitset, iBitToUse)
		
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [ADD_HEADHUNTER_SCENARIO_BLOCKING_AREA] ADD SCNEARIO BLOCKING AREA vLoc = ", vLoc, " iBitToUse = ", iBitToUse) 
	ENDIF
	
	IF ARE_VECTORS_EQUAL(vLoc,<<-46.8187, 1946.6593, 189.5608>>) 
		serverBD.scenBlockingArea[iBitToUse] = ADD_SCENARIO_BLOCKING_AREA(<<-65.1142, 1905.5771, 194.9851>>, <<-20.5564, 2013.4683, 171.5573>>, TRUE)
		SET_BIT(serverBD.iScenBlockBitset, iBitToUse)
		
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [ADD_HEADHUNTER_SCENARIO_BLOCKING_AREA] ADD SCNEARIO BLOCKING AREA vLoc = ", vLoc, " iBitToUse = ", iBitToUse) 
	ENDIF
ENDPROC

PROC INIT_SPAWN_COORDS()
	GET_MAP_AREA()
	
	IF serverBD.iNumberOfSpawnCoords = 0
		IF serverBD.iMapArea = HEADHUNTER_AREA_CITY
			serverBD.iNumberOfSpawnCoords = 10
		ELSE
			serverBD.iNumberOfSpawnCoords = 9
		ENDIF
		
		CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - INIT_SPAWN_COORDS MAP AREA = ", serverBD.iMapArea, " NUMBER OF SPAWN LOC = ", serverBD.iNumberOfSpawnCoords)
	ENDIF

	IF IS_BIT_SET(serverBD.iServerBitSet, biS_InitSpawnCoords)
		EXIT
	ENDIF
	
	SWITCH serverBD.iMapArea
		CASE HEADHUNTER_AREA_CITY
			//-- Target on Foot

			serverBD.pedSpawnCoords[0].vSpawnCoords = <<-106.8009, 331.5984, 111.7262>>  		serverBD.pedSpawnCoords[0].fSpawnHeading = 45.1956
					serverBD.pedSpawnCoords[0].vBodyguardSpawnCoords[0] =  <<-106.2522, 353.0331, 111.8849>>
					serverBD.pedSpawnCoords[0].vBodyguardSpawnCoords[1] =  <<-104.2692, 356.5287, 111.8858>>
					serverBD.pedSpawnCoords[0].vBodyguardSpawnCoords[2] =  <<-108.1528, 347.4515, 111.8858>>
					
					
			serverBD.pedSpawnCoords[1].vSpawnCoords = <<-1193.4076, -218.8712, 36.9448>> 		serverBD.pedSpawnCoords[1].fSpawnHeading = 136.8281	
					serverBD.pedSpawnCoords[1].vBodyguardSpawnCoords[0] =  <<-1174.7235, -238.3963, 36.9385>>
					serverBD.pedSpawnCoords[1].vBodyguardSpawnCoords[1] =  <<-1201.0111, -231.2006, 36.9480>>
					serverBD.pedSpawnCoords[1].vBodyguardSpawnCoords[2] =  <<-1162.5201, -229.3399, 36.9564>>
					
			serverBD.pedSpawnCoords[2].vSpawnCoords = <<-1646.5825, -1102.8011, 12.0181>>  		serverBD.pedSpawnCoords[2].fSpawnHeading = 312.8899
					serverBD.pedSpawnCoords[2].vBodyguardSpawnCoords[0] =  <<-1643.2920, -1098.1542, 12.0207>>
					serverBD.pedSpawnCoords[2].vBodyguardSpawnCoords[1] =  <<-1638.8253, -1093.1046, 12.0269>>
				
			serverBD.pedSpawnCoords[3].vSpawnCoords = <<1171.3065, -398.4046, 70.5896>> 	serverBD.pedSpawnCoords[3].fSpawnHeading = 252.8763
					serverBD.pedSpawnCoords[3].vBodyguardSpawnCoords[0] =  <<1180.0662, -405.9521, 66.7792>>
					serverBD.pedSpawnCoords[3].vBodyguardSpawnCoords[1] =  <<1176.9991, -397.1275, 66.9280>>
					
			serverBD.pedSpawnCoords[4].vSpawnCoords = <<929.1267, -1255.1079, 24.4835>>    	serverBD.pedSpawnCoords[4].fSpawnHeading = 27.8866
					serverBD.pedSpawnCoords[4].vBodyguardSpawnCoords[0] =  <<943.2654, -1244.3000, 24.6881>>
					serverBD.pedSpawnCoords[4].vBodyguardSpawnCoords[1] =  <<939.8859, -1229.9584, 24.6520>>
					serverBD.pedSpawnCoords[4].vBodyguardSpawnCoords[2] =  <<920.2808, -1259.0972, 24.5269>>
					
			serverBD.pedSpawnCoords[5].vSpawnCoords = <<883.0366, -2166.1875, 31.2735>>   	serverBD.pedSpawnCoords[5].fSpawnHeading = 186.7157
					serverBD.pedSpawnCoords[5].vBodyguardSpawnCoords[0] =  <<871.2222, -2167.2205, 31.2735>>
					serverBD.pedSpawnCoords[5].vBodyguardSpawnCoords[1] =  <<872.3619, -2199.4636, 29.5194>>
					serverBD.pedSpawnCoords[5].vBodyguardSpawnCoords[2] =  <<888.3759, -2169.1643, 35.2714>>
			
			serverBD.pedSpawnCoords[6].vSpawnCoords = <<-110.3279, -2705.5991, 5.0099>>   	serverBD.pedSpawnCoords[6].fSpawnHeading =  0.3174
					serverBD.pedSpawnCoords[6].vBodyguardSpawnCoords[0] =  <<-133.4379, -2700.1094, 5.0103>>
					serverBD.pedSpawnCoords[6].vBodyguardSpawnCoords[1] =  <<-127.7720, -2681.3037, 5.0274>>
					serverBD.pedSpawnCoords[6].vBodyguardSpawnCoords[2] =  <<-112.7492, -2673.0620, 5.0060>> 	serverBD.pedSpawnCoords[6].fBodyguardSpawnHead[2] = 180.0
			
			//-- Probably needs some ambient vehicles in car park.
			serverBD.pedSpawnCoords[7].vSpawnCoords = <<-520.2162, 163.9754, 70.0812>>   	serverBD.pedSpawnCoords[7].fSpawnHeading =  358.2249
					serverBD.pedSpawnCoords[7].vBodyguardSpawnCoords[0] =  <<-508.6846, 166.9421, 69.9316>>
					serverBD.pedSpawnCoords[7].vBodyguardSpawnCoords[1] =  <<-495.0937, 169.2970, 69.9316>>

						serverBD.pedSpawnCoords[7].vAmbientVehSpawnCoords[0] = <<-505.5487, 166.7718, 69.9316>>   
						serverBD.pedSpawnCoords[7].fAmbientVehSpawnHead[0] = 85.9643
						serverBD.pedSpawnCoords[7].modelAmbVeh[0] = BALLER5
						
						serverBD.pedSpawnCoords[7].vAmbientVehSpawnCoords[1] = <<-498.6042, 169.8883, 69.9316>>  
						serverBD.pedSpawnCoords[7].fAmbientVehSpawnHead[1] = 263.6038
						serverBD.pedSpawnCoords[7].modelAmbVeh[1] = cog552
					
//			serverBD.pedSpawnCoords[8].vSpawnCoords = <<-564.0579, -1799.8756, 21.6583>>    serverBD.pedSpawnCoords[8].fSpawnHeading =  3.8990
//					serverBD.pedSpawnCoords[8].vBodyguardSpawnCoords[0] =  <<-550.5852, -1795.8179, 21.4177>>
//					serverBD.pedSpawnCoords[8].vBodyguardSpawnCoords[1] =  <<-549.1705, -1788.6077, 21.1052>>
//					serverBD.pedSpawnCoords[8].vBodyguardSpawnCoords[2] =  <<-556.3987, -1779.6371, 21.0624>>	


			serverBD.pedSpawnCoords[8].vSpawnCoords = <<-591.7915, -1765.7930, 22.1854>>     	serverBD.pedSpawnCoords[9].fSpawnHeading =  241.2612
					serverBD.pedSpawnCoords[8].vBodyguardSpawnCoords[0] =  <<-593.1422, -1779.9946, 21.8499>>
					serverBD.pedSpawnCoords[8].vBodyguardSpawnCoords[1] = <<-584.6111, -1774.9810, 21.6200>>// <<-583.1315, -1777.1934, 21.6175>>
					serverBD.pedSpawnCoords[8].vBodyguardSpawnCoords[2] =  <<-580.9441, -1769.8176, 22.1854>>
					
					serverBD.pedSpawnCoords[8].vAmbientVehSpawnCoords[0] = <<-582.6792, -1777.6953, 21.6132>>   
					serverBD.pedSpawnCoords[8].fAmbientVehSpawnHead[0] = 145.1394
					serverBD.pedSpawnCoords[8].modelAmbVeh[0] = BALLER5
						
			serverBD.pedSpawnCoords[9].vSpawnCoords = <<306.2488, -1000.8085, 28.3108>>     	serverBD.pedSpawnCoords[9].fSpawnHeading =  63.2117
					serverBD.pedSpawnCoords[9].vBodyguardSpawnCoords[0] =  <<314.5830, -998.1405, 28.1613>>
					serverBD.pedSpawnCoords[9].vBodyguardSpawnCoords[1] =  <<300.5795, -997.6307, 28.1986>>
					
	
					
						serverBD.pedSpawnCoords[9].vAmbientVehSpawnCoords[0] = <<312.5270, -1000.5713, 28.2617>>  
						serverBD.pedSpawnCoords[9].fAmbientVehSpawnHead[0] = 148.5301
						serverBD.pedSpawnCoords[9].modelAmbVeh[0] = BALLER5
						
						serverBD.pedSpawnCoords[9].vAmbientVehSpawnCoords[1] = <<303.7290, -1000.5684, 28.3096>> 
						serverBD.pedSpawnCoords[9].fAmbientVehSpawnHead[1] = 44.2995
						serverBD.pedSpawnCoords[9].modelAmbVeh[1] = cog552
					
					
			//-- Target in vehicle
		//	serverBD.vehSpawnCoords[0].vSpawnCoords = <<530.7976, -169.5322, 53.9185>>		serverBD.vehSpawnCoords[0].fSpawnHeading = 180.8315
			serverBD.vehSpawnCoords[0].vSpawnCoords = <<533.1884, -136.2944, 58.6519>>		serverBD.vehSpawnCoords[0].fSpawnHeading = 179.583
			serverBD.vehSpawnCoords[1].vSpawnCoords = <<774.9319, -1329.6525, 25.2430>>		serverBD.vehSpawnCoords[1].fSpawnHeading = 268.6526
			serverBD.vehSpawnCoords[2].vSpawnCoords = <<999.0349, -3054.0791, 4.9011>>		serverBD.vehSpawnCoords[2].fSpawnHeading = 90.3809
			
		//	serverBD.vehSpawnCoords[3].vSpawnCoords = <<233.9330, -1790.9407, 27.4313>>		serverBD.vehSpawnCoords[3].fSpawnHeading = 141.8552
			serverBD.vehSpawnCoords[3].vSpawnCoords = <<329.7707, -1750.9174, 28.2917>>		serverBD.vehSpawnCoords[3].fSpawnHeading = 229.4149
			
		//	serverBD.vehSpawnCoords[4].vSpawnCoords =<<-1089.7437, -2038.1128, 12.1742>> 	serverBD.vehSpawnCoords[4].fSpawnHeading = 309.9676
			serverBD.vehSpawnCoords[4].vSpawnCoords =<<-1000.1754, -2098.1331, 11.3367>> 	serverBD.vehSpawnCoords[4].fSpawnHeading = 141.0009
			
			serverBD.vehSpawnCoords[5].vSpawnCoords = <<-712.6369, -880.1467, 22.5928>>		serverBD.vehSpawnCoords[5].fSpawnHeading = 359.1567
			serverBD.vehSpawnCoords[6].vSpawnCoords = <<-1482.4604, -498.4642, 31.8069>>	serverBD.vehSpawnCoords[6].fSpawnHeading = 212.8829
			serverBD.vehSpawnCoords[7].vSpawnCoords = <<-1357.3246, 579.7441, 130.4830>>	serverBD.vehSpawnCoords[7].fSpawnHeading = 257.1156
			serverBD.vehSpawnCoords[8].vSpawnCoords = <<-555.6091, 55.0564, 48.3253>>		serverBD.vehSpawnCoords[8].fSpawnHeading = 174.0491
			serverBD.vehSpawnCoords[9].vSpawnCoords = <<-84.1599, -1024.1138, 27.2205>>		serverBD.vehSpawnCoords[9].fSpawnHeading = 245.895
		BREAK
		
		CASE HEADHUNTER_AREA_COUNTRY
			//-- Target on Foot
			serverBD.pedSpawnCoords[0].vSpawnCoords = <<1447.2708, 3750.1653, 30.9342>>     serverBD.pedSpawnCoords[0].fSpawnHeading =  225.1522
					serverBD.pedSpawnCoords[0].vBodyguardSpawnCoords[0] =  <<1440.1421, 3753.7502, 30.9407>>
					serverBD.pedSpawnCoords[0].vBodyguardSpawnCoords[1] =  <<1455.6880, 3760.3882, 31.0543>>
			
			//-- Block scenarios: <<-65.1142, 1905.5771, 194.9851>>, <<-20.5564, 2013.4683, 171.5573>>
			serverBD.pedSpawnCoords[1].vSpawnCoords = <<-46.8187, 1946.6593, 189.5608>>            serverBD.pedSpawnCoords[9].fSpawnHeading =  128.6356
					serverBD.pedSpawnCoords[1].vBodyguardSpawnCoords[0] =  <<-52.1263, 1953.1298, 189.1861>>
					serverBD.pedSpawnCoords[1].vBodyguardSpawnCoords[1] =  <<-63.1502, 1951.9463, 189.1861>>
					serverBD.pedSpawnCoords[1].vBodyguardSpawnCoords[2] =  <<-59.1516, 1964.0745, 189.1861>>
					
			serverBD.pedSpawnCoords[2].vSpawnCoords = <<2002.5273, 4978.5156, 40.5969>>       serverBD.pedSpawnCoords[2].fSpawnHeading =  214.3712
					serverBD.pedSpawnCoords[2].vBodyguardSpawnCoords[0] =  <<2013.2814, 4976.8848, 40.4305>>
					serverBD.pedSpawnCoords[2].vBodyguardSpawnCoords[1] =  <<2025.9471, 4978.3833, 40.1376>>
					serverBD.pedSpawnCoords[2].vBodyguardSpawnCoords[2] =  <<2000.8303, 4990.3599, 40.4477>>
			
			serverBD.pedSpawnCoords[3].vSpawnCoords = <<2939.4177, 4623.8330, 47.7256>>        serverBD.pedSpawnCoords[3].fSpawnHeading =  151.8443
					serverBD.pedSpawnCoords[3].vBodyguardSpawnCoords[0] =  <<2930.6150, 4620.4956, 47.7246>>
					serverBD.pedSpawnCoords[3].vBodyguardSpawnCoords[1] =  <<2946.9114, 4629.4790, 47.7251>>
					serverBD.pedSpawnCoords[3].vBodyguardSpawnCoords[2] =  <<2936.9558, 4609.8003, 47.7277>>	
			
			serverBD.pedSpawnCoords[4].vSpawnCoords = <<519.5331, 3105.4644, 39.5241>>        serverBD.pedSpawnCoords[4].fSpawnHeading =  186.7534
					serverBD.pedSpawnCoords[4].vBodyguardSpawnCoords[0] =  <<518.6117, 3090.1946, 39.4652>>
					serverBD.pedSpawnCoords[4].vBodyguardSpawnCoords[1] =  <<532.1984, 3083.5598, 39.4652>>

			
			//-- Block scenarios <<-1631.3120, 3027.8889, 30.8859>>,  <<-1560.5502, 3137.9331, 33.9384>>
			serverBD.pedSpawnCoords[5].vSpawnCoords = <<-1597.6035, 3083.6731, 31.5661>>         serverBD.pedSpawnCoords[5].fSpawnHeading =  101.7629
					serverBD.pedSpawnCoords[5].vBodyguardSpawnCoords[0] =  <<-1601.2593, 3078.7847, 31.5661>>
					serverBD.pedSpawnCoords[5].vBodyguardSpawnCoords[1] =  <<-1609.1093, 3080.7649, 31.5661>>
					serverBD.pedSpawnCoords[5].vBodyguardSpawnCoords[2] =  <<-1598.4003, 3070.0908, 32.6629>>
			
			//-- Beach cabin, needs vehicles:
			serverBD.pedSpawnCoords[6].vSpawnCoords = <<1470.0551, 6550.6660, 13.9091>>          serverBD.pedSpawnCoords[6].fSpawnHeading =  352.4692
					serverBD.pedSpawnCoords[6].vBodyguardSpawnCoords[0] =  <<1459.4520, 6546.8188, 13.6304>>
					serverBD.pedSpawnCoords[6].vBodyguardSpawnCoords[1] =  <<1460.6414, 6562.2017, 12.7644>>
					
						serverBD.pedSpawnCoords[6].vAmbientVehSpawnCoords[0] = <<1459.1785, 6544.8789, 13.7130>>   
						serverBD.pedSpawnCoords[6].fAmbientVehSpawnHead[0] = 88.1481
						serverBD.pedSpawnCoords[6].modelAmbVeh[0] = BALLER5
						
						serverBD.pedSpawnCoords[6].vAmbientVehSpawnCoords[1] = <<1460.2123, 6560.1787, 12.9444>>  
						serverBD.pedSpawnCoords[6].fAmbientVehSpawnHead[1] = 97.3821
						serverBD.pedSpawnCoords[6].modelAmbVeh[1] = cog552
			
			//-- Block scenarios: <<2266.3396, 2486.0745, 54.9760>>, <<2413.6621, 2679.9817, 43.5461>>
			serverBD.pedSpawnCoords[7].vSpawnCoords = <<2328.0659, 2576.5583, 45.6677>>          serverBD.pedSpawnCoords[7].fSpawnHeading =  335.1092
					serverBD.pedSpawnCoords[7].vBodyguardSpawnCoords[0] =  <<2339.7451, 2569.6160, 46.7255>>
					serverBD.pedSpawnCoords[7].vBodyguardSpawnCoords[1] =  <<2356.3853, 2593.8589, 46.1125>>
					serverBD.pedSpawnCoords[7].vBodyguardSpawnCoords[2] =  <<2338.9346, 2549.4526, 45.6677>>
			
			serverBD.pedSpawnCoords[8].vSpawnCoords = <<-1600.2952, 5204.9282, 3.3151>>           serverBD.pedSpawnCoords[8].fSpawnHeading =  296.2108
					serverBD.pedSpawnCoords[8].vBodyguardSpawnCoords[0] =  <<-1587.4084, 5193.2100, 3.3151>> 
					serverBD.pedSpawnCoords[8].vBodyguardSpawnCoords[1] =  <<-1576.5579, 5176.3213, 18.7159>>
					serverBD.pedSpawnCoords[8].vBodyguardSpawnCoords[2] =  <<-1595.5533, 5206.5933, 3.3151>> 
					
						serverBD.pedSpawnCoords[8].vAmbientVehSpawnCoords[0] = <<-1575.9238, 5170.9951, 18.5541>>    
						serverBD.pedSpawnCoords[8].fAmbientVehSpawnHead[0] = 312.3839
						serverBD.pedSpawnCoords[8].modelAmbVeh[0] = BALLER5
			
			
					
						//-- Maybe needs some amb veh
//			serverBD.pedSpawnCoords[9].vSpawnCoords = <<-155.1522, 6433.9736, 30.9159>>      serverBD.pedSpawnCoords[1].fSpawnHeading =  242.7229
//					serverBD.pedSpawnCoords[9].vBodyguardSpawnCoords[0] =  <<-147.8665, 6435.0933, 30.9159>>
//					serverBD.pedSpawnCoords[9].vBodyguardSpawnCoords[1] =  <<-154.8386, 6427.6230, 30.9159>>	
					
			//-- Target in vehicle
			serverBD.vehSpawnCoords[0].vSpawnCoords = <<1514.5256, 1730.1399, 108.9447>>	serverBD.vehSpawnCoords[0].fSpawnHeading = 87.1735
			serverBD.vehSpawnCoords[1].vSpawnCoords = <<2028.3313, 3444.5444, 42.9909>>		serverBD.vehSpawnCoords[1].fSpawnHeading = 251.7144
			serverBD.vehSpawnCoords[2].vSpawnCoords = <<392.7668, 3588.5850, 32.2922>>		serverBD.vehSpawnCoords[2].fSpawnHeading = 351.6842
		//	serverBD.vehSpawnCoords[3].vSpawnCoords = <<204.0912, 2455.2039, 55.5372>>		serverBD.vehSpawnCoords[3].fSpawnHeading = 266.2455
			serverBD.vehSpawnCoords[3].vSpawnCoords = <<200.9893, 2351.4001, 72.5299>>		serverBD.vehSpawnCoords[3].fSpawnHeading = 297.6164
			
			serverBD.vehSpawnCoords[4].vSpawnCoords = <<-2337.7917, 4041.6460, 29.4140>>	serverBD.vehSpawnCoords[4].fSpawnHeading = 346.6894
			serverBD.vehSpawnCoords[5].vSpawnCoords = <<-742.3113, 5537.6670, 32.4857>>		serverBD.vehSpawnCoords[5].fSpawnHeading = 30.2635
			serverBD.vehSpawnCoords[6].vSpawnCoords = <<1586.2771, 6445.4385, 24.1337>>		serverBD.vehSpawnCoords[6].fSpawnHeading = 155.2497
			serverBD.vehSpawnCoords[7].vSpawnCoords = <<2574.8450, 5086.1475, 43.6593>>		serverBD.vehSpawnCoords[7].fSpawnHeading = 11.7033
			serverBD.vehSpawnCoords[8].vSpawnCoords = <<362.3407, 4431.7549, 61.9071>>		serverBD.vehSpawnCoords[8].fSpawnHeading = 290.5464
			serverBD.vehSpawnCoords[9].vSpawnCoords = <<-1906.6606, 1773.9332, 170.3475>> 	serverBD.vehSpawnCoords[9].fSpawnHeading = 113.5510
		BREAK
	ENDSWITCH
	
	
	
	SET_BIT(serverBD.iServerBitSet, biS_InitSpawnCoords)
ENDPROC

FUNC FLOAT GET_TARGET_DEFENSIVE_AREA_SIZE(INT iTarget)
	IF ARE_VECTORS_EQUAL(serverbd.Targets[iTarget].vSpawnCoord, <<-1597.6035, 3083.6731, 31.5661>>)  // Country loc [5]
		RETURN 10.0
	ENDIF
	
	RETURN 3.0
ENDFUNC

#IF IS_DEBUG_BUILD
PROC SET_SPAWN_COORDS_FOR_DEBUG_VARIATION()
	INT iDebugVariation = serverBD.iDebugVariation

	
	IF iDebugVariation = -1
		EXIT
	ENDIF
	
	
	SWITCH iDebugVariation
	/// CITY
		CASE 0
			// TARGET_TYPE_ON_FOOT_WITH_BODYGUARDS
			serverbd.Targets[0].vSpawnCoord = serverBD.pedSpawnCoords[0].vSpawnCoords
			serverbd.Targets[0].fSpawnHead 	= serverBD.pedSpawnCoords[0].fSpawnHeading
			serverBD.Targets[0].iSpawnData 	= 0
			
			// TARGET_TYPE_ON_FOOT_WITH_BODYGUARDS
			serverbd.Targets[1].vSpawnCoord = serverBD.pedSpawnCoords[1].vSpawnCoords
			serverbd.Targets[1].fSpawnHead 	= serverBD.pedSpawnCoords[1].fSpawnHeading
			serverBD.Targets[1].iSpawnData 	= 1
			
			// TARGET_TYPE_DRIVER_WITH_BODYGUARDS
			serverbd.Targets[2].vSpawnCoord = serverBD.vehSpawnCoords[0].vSpawnCoords
			serverbd.Targets[2].fSpawnHead 	= serverBD.vehSpawnCoords[0].fSpawnHeading
			serverBD.Targets[2].iSpawnData 	= 0
			
			// TARGET_TYPE_DRIVER_WITH_BODYGUARDS
			serverbd.Targets[3].vSpawnCoord =  serverBD.vehSpawnCoords[1].vSpawnCoords
			serverbd.Targets[3].fSpawnHead 	=  serverBD.vehSpawnCoords[1].fSpawnHeading
			serverBD.Targets[3].iSpawnData 	=  1
			
		BREAK
		
		CASE 1
			// TARGET_TYPE_ON_FOOT_WITH_BODYGUARDS
			serverbd.Targets[0].vSpawnCoord = serverBD.pedSpawnCoords[2].vSpawnCoords
			serverbd.Targets[0].fSpawnHead 	= serverBD.pedSpawnCoords[2].fSpawnHeading
			serverBD.Targets[0].iSpawnData 	= 2
			
			// TARGET_TYPE_ON_FOOT_WITH_BODYGUARDS
			serverbd.Targets[1].vSpawnCoord = serverBD.pedSpawnCoords[3].vSpawnCoords
			serverbd.Targets[1].fSpawnHead 	= serverBD.pedSpawnCoords[3].fSpawnHeading
			serverBD.Targets[1].iSpawnData 	= 3
			
			// TARGET_TYPE_DRIVER_WITH_BODYGUARDS
			serverbd.Targets[2].vSpawnCoord = serverBD.vehSpawnCoords[2].vSpawnCoords
			serverbd.Targets[2].fSpawnHead 	= serverBD.vehSpawnCoords[2].fSpawnHeading
			serverBD.Targets[2].iSpawnData 	= 2
			
			// TARGET_TYPE_DRIVER_WITH_BODYGUARDS
			serverbd.Targets[3].vSpawnCoord =  serverBD.vehSpawnCoords[3].vSpawnCoords
			serverbd.Targets[3].fSpawnHead 	=  serverBD.vehSpawnCoords[3].fSpawnHeading
			serverBD.Targets[3].iSpawnData 	=  3
		BREAK
		
		CASE 2
			// TARGET_TYPE_ON_FOOT_WITH_BODYGUARDS
			serverbd.Targets[0].vSpawnCoord = serverBD.pedSpawnCoords[4].vSpawnCoords
			serverbd.Targets[0].fSpawnHead 	= serverBD.pedSpawnCoords[4].fSpawnHeading
			serverBD.Targets[0].iSpawnData 	= 4
			
			// TARGET_TYPE_ON_FOOT_WITH_BODYGUARDS
			serverbd.Targets[1].vSpawnCoord = serverBD.pedSpawnCoords[5].vSpawnCoords
			serverbd.Targets[1].fSpawnHead 	= serverBD.pedSpawnCoords[5].fSpawnHeading
			serverBD.Targets[1].iSpawnData 	= 5
			
			// TARGET_TYPE_DRIVER_WITH_BODYGUARDS
			serverbd.Targets[2].vSpawnCoord = serverBD.vehSpawnCoords[4].vSpawnCoords
			serverbd.Targets[2].fSpawnHead 	= serverBD.vehSpawnCoords[4].fSpawnHeading
			serverBD.Targets[2].iSpawnData 	= 4
			
			// TARGET_TYPE_DRIVER_WITH_BODYGUARDS
			serverbd.Targets[3].vSpawnCoord =  serverBD.vehSpawnCoords[5].vSpawnCoords
			serverbd.Targets[3].fSpawnHead 	=  serverBD.vehSpawnCoords[5].fSpawnHeading
			serverBD.Targets[3].iSpawnData 	=  5
		BREAK
		
		CASE 3
			// TARGET_TYPE_ON_FOOT_WITH_BODYGUARDS
			serverbd.Targets[0].vSpawnCoord = serverBD.pedSpawnCoords[6].vSpawnCoords
			serverbd.Targets[0].fSpawnHead 	= serverBD.pedSpawnCoords[6].fSpawnHeading
			serverBD.Targets[0].iSpawnData 	= 6
			
			// TARGET_TYPE_ON_FOOT_WITH_BODYGUARDS
			serverbd.Targets[1].vSpawnCoord = serverBD.pedSpawnCoords[7].vSpawnCoords
			serverbd.Targets[1].fSpawnHead 	= serverBD.pedSpawnCoords[7].fSpawnHeading
			serverBD.Targets[1].iSpawnData 	= 7
			
			// TARGET_TYPE_DRIVER_WITH_BODYGUARDS
			serverbd.Targets[2].vSpawnCoord = serverBD.vehSpawnCoords[6].vSpawnCoords
			serverbd.Targets[2].fSpawnHead 	= serverBD.vehSpawnCoords[6].fSpawnHeading
			serverBD.Targets[2].iSpawnData 	= 6
			
			// TARGET_TYPE_DRIVER_WITH_BODYGUARDS
			serverbd.Targets[3].vSpawnCoord =  serverBD.vehSpawnCoords[7].vSpawnCoords
			serverbd.Targets[3].fSpawnHead 	=  serverBD.vehSpawnCoords[7].fSpawnHeading
			serverBD.Targets[3].iSpawnData 	=  7
		BREAK
		
		CASE 4
			// TARGET_TYPE_ON_FOOT_WITH_BODYGUARDS
			serverbd.Targets[0].vSpawnCoord = serverBD.pedSpawnCoords[8].vSpawnCoords
			serverbd.Targets[0].fSpawnHead 	= serverBD.pedSpawnCoords[8].fSpawnHeading
			serverBD.Targets[0].iSpawnData 	= 8
			
			// TARGET_TYPE_ON_FOOT_WITH_BODYGUARDS
			serverbd.Targets[1].vSpawnCoord = serverBD.pedSpawnCoords[9].vSpawnCoords
			serverbd.Targets[1].fSpawnHead 	= serverBD.pedSpawnCoords[9].fSpawnHeading
			serverBD.Targets[1].iSpawnData 	= 9
			
			// TARGET_TYPE_DRIVER_WITH_BODYGUARDS
			serverbd.Targets[2].vSpawnCoord = serverBD.vehSpawnCoords[8].vSpawnCoords
			serverbd.Targets[2].fSpawnHead 	= serverBD.vehSpawnCoords[8].fSpawnHeading
			serverBD.Targets[2].iSpawnData 	= 8
			
			// TARGET_TYPE_DRIVER_WITH_BODYGUARDS
			serverbd.Targets[3].vSpawnCoord =  serverBD.vehSpawnCoords[9].vSpawnCoords
			serverbd.Targets[3].fSpawnHead 	=  serverBD.vehSpawnCoords[9].fSpawnHeading
			serverBD.Targets[3].iSpawnData 	=  9
		BREAK
		
	/// COUNTRY
		CASE 5
			// TARGET_TYPE_ON_FOOT_WITH_BODYGUARDS
			serverbd.Targets[0].vSpawnCoord = serverBD.pedSpawnCoords[0].vSpawnCoords
			serverbd.Targets[0].fSpawnHead 	= serverBD.pedSpawnCoords[0].fSpawnHeading
			serverBD.Targets[0].iSpawnData 	= 0
			
			// TARGET_TYPE_ON_FOOT_WITH_BODYGUARDS
			serverbd.Targets[1].vSpawnCoord = serverBD.pedSpawnCoords[1].vSpawnCoords
			serverbd.Targets[1].fSpawnHead 	= serverBD.pedSpawnCoords[1].fSpawnHeading
			serverBD.Targets[1].iSpawnData 	= 1
			
			// TARGET_TYPE_DRIVER_WITH_BODYGUARDS
			serverbd.Targets[2].vSpawnCoord = serverBD.vehSpawnCoords[0].vSpawnCoords
			serverbd.Targets[2].fSpawnHead 	= serverBD.vehSpawnCoords[0].fSpawnHeading
			serverBD.Targets[2].iSpawnData 	= 0
			
			// TARGET_TYPE_DRIVER_WITH_BODYGUARDS
			serverbd.Targets[3].vSpawnCoord =  serverBD.vehSpawnCoords[1].vSpawnCoords
			serverbd.Targets[3].fSpawnHead 	=  serverBD.vehSpawnCoords[1].fSpawnHeading
			serverBD.Targets[3].iSpawnData 	=  1
			
		BREAK
		
		CASE 6
			// TARGET_TYPE_ON_FOOT_WITH_BODYGUARDS
			serverbd.Targets[0].vSpawnCoord = serverBD.pedSpawnCoords[2].vSpawnCoords
			serverbd.Targets[0].fSpawnHead 	= serverBD.pedSpawnCoords[2].fSpawnHeading
			serverBD.Targets[0].iSpawnData 	= 2
			
			// TARGET_TYPE_ON_FOOT_WITH_BODYGUARDS
			serverbd.Targets[1].vSpawnCoord = serverBD.pedSpawnCoords[3].vSpawnCoords
			serverbd.Targets[1].fSpawnHead 	= serverBD.pedSpawnCoords[3].fSpawnHeading
			serverBD.Targets[1].iSpawnData 	= 3
			
			// TARGET_TYPE_DRIVER_WITH_BODYGUARDS
			serverbd.Targets[2].vSpawnCoord = serverBD.vehSpawnCoords[2].vSpawnCoords
			serverbd.Targets[2].fSpawnHead 	= serverBD.vehSpawnCoords[2].fSpawnHeading
			serverBD.Targets[2].iSpawnData 	= 2
			
			// TARGET_TYPE_DRIVER_WITH_BODYGUARDS
			serverbd.Targets[3].vSpawnCoord =  serverBD.vehSpawnCoords[3].vSpawnCoords
			serverbd.Targets[3].fSpawnHead 	=  serverBD.vehSpawnCoords[3].fSpawnHeading
			serverBD.Targets[3].iSpawnData 	=  3
		BREAK
		
		CASE 7
			// TARGET_TYPE_ON_FOOT_WITH_BODYGUARDS
			serverbd.Targets[0].vSpawnCoord = serverBD.pedSpawnCoords[4].vSpawnCoords
			serverbd.Targets[0].fSpawnHead 	= serverBD.pedSpawnCoords[4].fSpawnHeading
			serverBD.Targets[0].iSpawnData 	= 4
			
			// TARGET_TYPE_ON_FOOT_WITH_BODYGUARDS
			serverbd.Targets[1].vSpawnCoord = serverBD.pedSpawnCoords[5].vSpawnCoords
			serverbd.Targets[1].fSpawnHead 	= serverBD.pedSpawnCoords[5].fSpawnHeading
			serverBD.Targets[1].iSpawnData 	= 5
			
			// TARGET_TYPE_DRIVER_WITH_BODYGUARDS
			serverbd.Targets[2].vSpawnCoord = serverBD.vehSpawnCoords[4].vSpawnCoords
			serverbd.Targets[2].fSpawnHead 	= serverBD.vehSpawnCoords[4].fSpawnHeading
			serverBD.Targets[2].iSpawnData 	= 4
			
			// TARGET_TYPE_DRIVER_WITH_BODYGUARDS
			serverbd.Targets[3].vSpawnCoord =  serverBD.vehSpawnCoords[5].vSpawnCoords
			serverbd.Targets[3].fSpawnHead 	=  serverBD.vehSpawnCoords[5].fSpawnHeading
			serverBD.Targets[3].iSpawnData 	=  5
		BREAK
		
		CASE 8
			// TARGET_TYPE_ON_FOOT_WITH_BODYGUARDS
			serverbd.Targets[0].vSpawnCoord = serverBD.pedSpawnCoords[6].vSpawnCoords
			serverbd.Targets[0].fSpawnHead 	= serverBD.pedSpawnCoords[6].fSpawnHeading
			serverBD.Targets[0].iSpawnData 	= 6
			
			// TARGET_TYPE_ON_FOOT_WITH_BODYGUARDS
			serverbd.Targets[1].vSpawnCoord = serverBD.pedSpawnCoords[7].vSpawnCoords
			serverbd.Targets[1].fSpawnHead 	= serverBD.pedSpawnCoords[7].fSpawnHeading
			serverBD.Targets[1].iSpawnData 	= 7
			
			// TARGET_TYPE_DRIVER_WITH_BODYGUARDS
			serverbd.Targets[2].vSpawnCoord = serverBD.vehSpawnCoords[6].vSpawnCoords
			serverbd.Targets[2].fSpawnHead 	= serverBD.vehSpawnCoords[6].fSpawnHeading
			serverBD.Targets[2].iSpawnData 	= 6
			
			// TARGET_TYPE_DRIVER_WITH_BODYGUARDS
			serverbd.Targets[3].vSpawnCoord =  serverBD.vehSpawnCoords[7].vSpawnCoords
			serverbd.Targets[3].fSpawnHead 	=  serverBD.vehSpawnCoords[7].fSpawnHeading
			serverBD.Targets[3].iSpawnData 	=  7
		BREAK
		
		CASE 9
			// TARGET_TYPE_ON_FOOT_WITH_BODYGUARDS
			serverbd.Targets[0].vSpawnCoord = serverBD.pedSpawnCoords[8].vSpawnCoords
			serverbd.Targets[0].fSpawnHead 	= serverBD.pedSpawnCoords[8].fSpawnHeading
			serverBD.Targets[0].iSpawnData 	= 8
			
			// TARGET_TYPE_ON_FOOT_WITH_BODYGUARDS // No ped[9] in countryside
			serverbd.Targets[1].vSpawnCoord = serverBD.pedSpawnCoords[7].vSpawnCoords
			serverbd.Targets[1].fSpawnHead 	= serverBD.pedSpawnCoords[7].fSpawnHeading
			serverBD.Targets[1].iSpawnData 	= 7
			
			// TARGET_TYPE_DRIVER_WITH_BODYGUARDS
			serverbd.Targets[2].vSpawnCoord = serverBD.vehSpawnCoords[8].vSpawnCoords
			serverbd.Targets[2].fSpawnHead 	= serverBD.vehSpawnCoords[8].fSpawnHeading
			serverBD.Targets[2].iSpawnData 	= 8
			
			// TARGET_TYPE_DRIVER_WITH_BODYGUARDS
			serverbd.Targets[3].vSpawnCoord =  serverBD.vehSpawnCoords[9].vSpawnCoords
			serverbd.Targets[3].fSpawnHead 	=  serverBD.vehSpawnCoords[9].fSpawnHeading
			serverBD.Targets[3].iSpawnData 	=  9
		BREAK
	ENDSWITCH
	
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [SET_SPAWN_COORDS_FOR_DEBUG_VARIATION] iDebugVariation = ", iDebugVariation, " serverbd.Targets[0].vSpawnCoord = ", serverbd.Targets[0].vSpawnCoord)
	
	serverBD.iDebugVariation = -1
ENDPROC

#ENDIF

FUNC BOOL GET_SPAWN_COORD_FOR_TARGET(INT iTarget, VECTOR &vSpawn, FLOAT &fSpawn, INT &iSpawnDataToUse)
	INT iRand
	
	CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [GET_SPAWN_COORD_FOR_TARGET] FINDING SPAWN COORDS FOR TARGET ", iTarget, " ...")
	
	
	
	SWITCH serverBD.Targets[iTarget].iTargetType
		CASE TARGET_TYPE_SOLO_ON_FOOT
			iRand = GET_RANDOM_INT_IN_RANGE(0, serverBD.iNumberOfSpawnCoords)
			IF NOT ARE_VECTORS_EQUAL(serverBD.pedSpawnCoords[iRand].vSpawnCoords, << 0.0, 0.0, 0.0 >>)
				IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(	serverBD.pedSpawnCoords[iRand].vSpawnCoords)
					vSpawn = serverBD.pedSpawnCoords[iRand].vSpawnCoords
					fSpawn = serverBD.pedSpawnCoords[iRand].fSpawnHeading
					serverBD.pedSpawnCoords[iRand].vSpawnCoords = << 0.0, 0.0, 0.0>>
					RETURN TRUE
				ELSE
					PRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [GET_SPAWN_COORD_FOR_TARGET] REJECTING ON-FOOT SPAWN POINT ", iRand, " AT ", serverBD.pedSpawnCoords[iRand].vSpawnCoords, " AS NOT IS_POINT_OK_FOR_NET_ENTITY_CREATION")
				ENDIF
			ENDIF
		BREAK
		
		
		CASE TARGET_TYPE_ON_FOOT_WITH_BODYGUARDS
			iRand = GET_RANDOM_INT_IN_RANGE(0, serverBD.iNumberOfSpawnCoords)
			
//			#IF IS_DEBUG_BUILD
//				IF MAX_HEADHUNTER_SPAWN_COORDS = MAX_TARGETS
//					iRand = iTarget
//				ENDIF
//			#ENDIF
			
			IF NOT ARE_VECTORS_EQUAL(serverBD.pedSpawnCoords[iRand].vSpawnCoords, << 0.0, 0.0, 0.0 >>)
				IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(	serverBD.pedSpawnCoords[iRand].vSpawnCoords)
					vSpawn = serverBD.pedSpawnCoords[iRand].vSpawnCoords
					fSpawn = serverBD.pedSpawnCoords[iRand].fSpawnHeading
					iSpawnDataToUse = iRand
					serverBD.pedSpawnCoords[iRand].vSpawnCoords = << 0.0, 0.0, 0.0>>
					RETURN TRUE
				ELSE
					PRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [GET_SPAWN_COORD_FOR_TARGET] REJECTING ON-FOOT SPAWN POINT ", iRand, " AT ", serverBD.pedSpawnCoords[iRand].vSpawnCoords, " AS NOT IS_POINT_OK_FOR_NET_ENTITY_CREATION")
				ENDIF
			ENDIF
		BREAK
		
		CASE TARGET_TYPE_DRIVER_WITH_BODYGUARDS
			iRand = GET_RANDOM_INT_IN_RANGE(0, MAX_HEADHUNTER_SPAWN_COORDS)
			IF NOT ARE_VECTORS_EQUAL(serverBD.vehSpawnCoords[iRand].vSpawnCoords, << 0.0, 0.0, 0.0 >>)
				IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(	serverBD.vehSpawnCoords[iRand].vSpawnCoords)
					vSpawn = serverBD.vehSpawnCoords[iRand].vSpawnCoords
					fSpawn = serverBD.vehSpawnCoords[iRand].fSpawnHeading
					serverBD.vehSpawnCoords[iRand].vSpawnCoords = << 0.0, 0.0, 0.0>>
					RETURN TRUE
				ELSE
					PRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [GET_SPAWN_COORD_FOR_TARGET] REJECTING VEH SPAWN POINT ", iRand, " AT ", serverBD.vehSpawnCoords[iRand].vSpawnCoords, " AS NOT IS_POINT_OK_FOR_NET_ENTITY_CREATION")
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
		

	

	
	RETURN FALSE
ENDFUNC

FUNC BOOL CREATE_TARGET(INT iTarget)
	MODEL_NAMES mVehToUse
	MODEL_NAMES mPedTToUSe
	VECTOR vSpawn
	FLOAT fSpawn
	INT iSpawnData
	
	INT iHealth	
	INT iArmour
								
	#IF IS_DEBUG_BUILD
	SET_SPAWN_COORDS_FOR_DEBUG_VARIATION()
	#ENDIF
	

	
	SWITCH serverbd.Targets[iTarget].iTargetType
		CASE TARGET_TYPE_DRIVER_WITH_BODYGUARDS
			
			
			IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.Targets[iTarget].niVeh)
				mVehToUse = serverbd.Targets[iTarget].mVeh
				IF CAN_REGISTER_MISSION_VEHICLES(1)
					IF REQUEST_LOAD_MODEL(mVehToUse)
						IF ARE_VECTORS_EQUAL(serverbd.Targets[iTarget].vSpawnCoord, << 0.0, 0.0, 0.0 >>)
							IF GET_SPAWN_COORD_FOR_TARGET(iTarget, vSpawn, fSpawn, iSpawnData)
								serverbd.Targets[iTarget].vSpawnCoord = vSpawn
								serverbd.Targets[iTarget].fSpawnHead = fSpawn
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - CREATE_TARGET GOT SPAWN COORDS iTargetNum = ", iTarget, " vSpawn = ", vSpawn, " fSpawn = ",fSpawn)
							ENDIF
						ENDIF
						
						IF NOT ARE_VECTORS_EQUAL(serverbd.Targets[iTarget].vSpawnCoord, << 0.0, 0.0, 0.0 >>)
							
							IF CREATE_NET_VEHICLE(serverbd.Targets[iTarget].niVeh, 
												mVehToUse, 
												serverbd.Targets[iTarget].vSpawnCoord,
												serverbd.Targets[iTarget].fSpawnHead)
								
								SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(NET_TO_VEH(serverbd.Targets[iTarget].niVeh), TRUE)
								SET_VEHICLE_DOORS_LOCKED_FOR_NON_SCRIPT_PLAYERS(NET_TO_VEH(serverbd.Targets[iTarget].niVeh), TRUE)
								SET_VEHICLE_ENGINE_ON(NET_TO_VEH(serverbd.Targets[iTarget].niVeh), TRUE, TRUE)
								SET_ENTITY_ONLY_DAMAGED_BY_RELATIONSHIP_GROUP(NET_TO_PED(serverbd.Targets[iTarget].niVeh), TRUE, relHeadHunterPlayer)
								SET_VEHICLE_TYRES_CAN_BURST(NET_TO_VEH(serverbd.Targets[iTarget].niVeh), FALSE)
								SET_VEHICLE_AUTOMATICALLY_ATTACHES(NET_TO_VEH(serverbd.Targets[iTarget].niVeh), FALSE)
							//	SET_VEHICLE_CAN_BE_TARGETTED(NET_TO_VEH(serverbD.Targets[iTarget].niVeh), FALSE)
								SET_ENTITY_LOAD_COLLISION_FLAG(NET_TO_VEH(serverbd.Targets[iTarget].niVeh), TRUE)
							//	SET_VEHICLE_ALLOW_HOMING_MISSLE_LOCKON_SYNCED(NET_TO_VEH(serverbd.Targets[iTarget].niVeh), FALSE)
								SET_VEHICLE_STRONG(NET_TO_VEH(serverbD.Targets[iTarget].niVeh), TRUE)
								SET_DISABLE_VEHICLE_PETROL_TANK_DAMAGE(NET_TO_VEH(serverbD.Targets[iTarget].niVeh), TRUE)
								
								SET_ALLOW_RAMMING_SOOP_OR_RAMP(NET_TO_VEH(serverbD.Targets[iTarget].niVeh), FALSE)
//								FLOAT fHealth
//								fHealth = 10000.0
//								
//								SET_ENTITY_MAX_HEALTH(NET_TO_VEH(serverbD.Targets[iTarget].niVeh),ROUND(fHealth) )
//								SET_ENTITY_HEALTH(NET_TO_VEH(serverbD.Targets[iTarget].niVeh),ROUND(fHealth))
//								
//								SET_VEHICLE_PETROL_TANK_HEALTH(NET_TO_VEH(serverbD.Targets[iTarget].niVeh),fHealth)
//								SET_VEHICLE_BODY_HEALTH(NET_TO_VEH(serverbD.Targets[iTarget].niVeh),fHealth)
//								SET_VEHICLE_ENGINE_HEALTH(NET_TO_VEH(serverbD.Targets[iTarget].niVeh),fHealth)
//						
								SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE(NET_TO_VEH(serverbD.Targets[iTarget].niVeh), FALSE)
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - CREATE_TARGET CREATED VEHICLE FOR TARGET iTargetNum = ", iTarget)
							ELSE
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - CREATE_TARGET FAILING - CREATE_NET_VEHICLE -  iTargetNum = ", iTarget)
							ENDIF
						ELSE
							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - CREATE_TARGET FAILING - ARE_VECTORS_EQUAL -  iTargetNum = ", iTarget)
							
						ENDIF
					ENDIF
				ELSE
					CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - CREATE_TARGET FAILING - CAN_REGISTER_MISSION_VEHICLES -  iTargetNum = ", iTarget)
				ENDIF
			ENDIF
			
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.Targets[iTarget].niVeh)
				IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.Targets[iTarget].niPed)
					IF CAN_REGISTER_MISSION_PEDS(1)
						mPedTToUSe = serverbd.Targets[iTarget].mPed
						IF REQUEST_LOAD_MODEL(mPedTToUSe)
							IF CREATE_NET_PED_IN_VEHICLE(serverbd.Targets[iTarget].niPed, 
														serverbd.Targets[iTarget].niVeh, 
														PEDTYPE_CRIMINAL, 
														mPedTToUSe, VS_DRIVER)
							
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - CREATE_TARGET CREATED PED FOR TARGET iTargetNum = ", iTarget)
								
								SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(NET_TO_PED(serverbd.Targets[iTarget].niPed), TRUE)
								
								SET_ENTITY_IS_TARGET_PRIORITY(NET_TO_PED(serverbd.Targets[iTarget].niPed), TRUE)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NET_TO_PED(serverbd.Targets[iTarget].niPed), TRUE)
								SET_PED_RELATIONSHIP_GROUP_HASH(NET_TO_PED(serverbd.Targets[iTarget].niPed),relHeadHunterAi)
								
								SET_ENTITY_ONLY_DAMAGED_BY_RELATIONSHIP_GROUP(NET_TO_PED(serverbd.Targets[iTarget].niPed), TRUE, relHeadHunterPlayer)
								
								SET_PED_DIES_WHEN_INJURED(NET_TO_PED(serverbd.Targets[iTarget].niPed),TRUE)
								
								SET_BIT(serverBD.iTargetCreatedBitSet, iTarget)
								
								#IF IS_DEBUG_BUILD
									TEXT_LABEL_15 tl15Ped
									tl15Ped = "Ped "
									tl15Ped += iTarget
									SET_PED_NAME_DEBUG(NET_TO_PED(serverbd.Targets[iTarget].niPed), tl15Ped)
								#ENDIF
								
							ENDIF
						ENDIF
					ELSE
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - CREATE_TARGET FAILING - CAN_REGISTER_MISSION_PEDS -  iTargetNum = ", iTarget)
					ENDIF
				ENDIF
			ENDIF
					
		BREAK
		
		CASE TARGET_TYPE_SOLO_ON_FOOT
			
			IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.Targets[iTarget].niPed)
				IF CAN_REGISTER_MISSION_PEDS(1)
					mPedTToUSe = serverbd.Targets[iTarget].mPed
					IF REQUEST_LOAD_MODEL(mPedTToUSe)
						IF ARE_VECTORS_EQUAL(serverbd.Targets[iTarget].vSpawnCoord, << 0.0, 0.0, 0.0 >>)
							IF GET_SPAWN_COORD_FOR_TARGET(iTarget, vSpawn, fSpawn, iSpawnData)
								serverbd.Targets[iTarget].vSpawnCoord = vSpawn
								serverbd.Targets[iTarget].fSpawnHead = fSpawn
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - CREATE_TARGET TARGET_TYPE_SOLO_ON_FOOT GOT SPAWN COORDS iTargetNum = ", iTarget, " vSpawn = ", vSpawn, " fSpawn = ",fSpawn)
							ENDIF
						ENDIF
						
						IF NOT ARE_VECTORS_EQUAL(serverbd.Targets[iTarget].vSpawnCoord, << 0.0, 0.0, 0.0 >>)
							IF CREATE_NET_PED(serverbd.Targets[iTarget].niPed, PEDTYPE_CRIMINAL, mPedTToUSe, serverbd.Targets[iTarget].vSpawnCoord, serverbd.Targets[iTarget].fSpawnHead)
								
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - CREATE_TARGET TARGET_TYPE_SOLO_ON_FOOT CREATED PED FOR TARGET iTargetNum = ", iTarget)
								
								SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(NET_TO_PED(serverbd.Targets[iTarget].niPed), TRUE)
								
								SET_ENTITY_IS_TARGET_PRIORITY(NET_TO_PED(serverbd.Targets[iTarget].niPed), TRUE)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NET_TO_PED(serverbd.Targets[iTarget].niPed), TRUE)
								SET_PED_RELATIONSHIP_GROUP_HASH(NET_TO_PED(serverbd.Targets[iTarget].niPed),relHeadHunterAi)
								SET_ENTITY_ONLY_DAMAGED_BY_RELATIONSHIP_GROUP(NET_TO_PED(serverbd.Targets[iTarget].niPed), TRUE, relHeadHunterPlayer)
								SET_PED_DIES_WHEN_INJURED(NET_TO_PED(serverbd.Targets[iTarget].niPed),TRUE)
								
								SET_BIT(serverBD.iTargetCreatedBitSet, iTarget)
								
								#IF IS_DEBUG_BUILD
									TEXT_LABEL_15 tl15Ped
									tl15Ped = "Ped "
									tl15Ped += iTarget
									SET_PED_NAME_DEBUG(NET_TO_PED(serverbd.Targets[iTarget].niPed), tl15Ped)
								#ENDIF
								
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE TARGET_TYPE_ON_FOOT_WITH_BODYGUARDS
			IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.Targets[iTarget].niPed)
				IF CAN_REGISTER_MISSION_PEDS(1)
					mPedTToUSe = serverbd.Targets[iTarget].mPed
					IF REQUEST_LOAD_MODEL(mPedTToUSe)
						CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - CREATE_TARGET TARGET_TYPE_ON_FOOT_WITH_BODYGUARDS CHECKING serverbd.Targets[", itarget, "].vSpawnCoord = ", serverbd.Targets[iTarget].vSpawnCoord)
						IF ARE_VECTORS_EQUAL(serverbd.Targets[iTarget].vSpawnCoord, << 0.0, 0.0, 0.0 >>)
							IF GET_SPAWN_COORD_FOR_TARGET(iTarget, vSpawn, fSpawn, iSpawnData)
								serverbd.Targets[iTarget].vSpawnCoord = vSpawn
								serverbd.Targets[iTarget].fSpawnHead = fSpawn
								serverBD.Targets[iTarget].iSpawnData = iSpawnData
								ADD_HEADHUNTER_SCENARIO_BLOCKING_AREA(vSpawn)
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - CREATE_TARGET TARGET_TYPE_ON_FOOT_WITH_BODYGUARDS GOT SPAWN COORDS iTargetNum = ", iTarget, " iSpawnData = ", iSpawnData,  " vSpawn = ", vSpawn, " fSpawn = ",fSpawn)
							ENDIF
						ENDIF
						
						IF NOT ARE_VECTORS_EQUAL(serverbd.Targets[iTarget].vSpawnCoord, << 0.0, 0.0, 0.0 >>)
							IF CREATE_NET_PED(serverbd.Targets[iTarget].niPed, PEDTYPE_CRIMINAL, mPedTToUSe, serverbd.Targets[iTarget].vSpawnCoord, serverbd.Targets[iTarget].fSpawnHead)
								
								
								
								SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(NET_TO_PED(serverbd.Targets[iTarget].niPed), TRUE)
								
								SET_ENTITY_IS_TARGET_PRIORITY(NET_TO_PED(serverbd.Targets[iTarget].niPed), TRUE)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NET_TO_PED(serverbd.Targets[iTarget].niPed), TRUE)
								SET_PED_RELATIONSHIP_GROUP_HASH(NET_TO_PED(serverbd.Targets[iTarget].niPed),relHeadHunterAi)
								SET_ENTITY_ONLY_DAMAGED_BY_RELATIONSHIP_GROUP(NET_TO_PED(serverbd.Targets[iTarget].niPed), TRUE, relHeadHunterPlayer)
								SET_PED_DIES_WHEN_INJURED(NET_TO_PED(serverbd.Targets[iTarget].niPed),TRUE)
								
								SET_PED_COMBAT_MOVEMENT(NET_TO_PED(serverbd.Targets[iTarget].niPed), CM_DEFENSIVE)
								SET_PED_CONFIG_FLAG(NET_TO_PED(serverbd.Targets[iTarget].niPed), PCF_DontInfluenceWantedLevel, TRUE)
								SET_PED_HIGHLY_PERCEPTIVE(NET_TO_PED(serverbd.Targets[iTarget].niPed), TRUE)
							//	SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(serverbd.Targets[iTarget].niPed), CA_SWITCH_TO_ADVANCE_IF_CANT_FIND_COVER, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(serverbd.Targets[iTarget].niPed), CA_JUST_SEEK_COVER, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(serverbd.Targets[iTarget].niPed), CA_SWITCH_TO_DEFENSIVE_IF_IN_COVER, TRUE)
								SET_PED_TARGET_LOSS_RESPONSE(NET_TO_PED(serverbd.Targets[iTarget].niPed), TLR_NEVER_LOSE_TARGET)
								
								GIVE_WEAPON_TO_PED(NET_TO_PED(serverbd.Targets[iTarget].niPed), WEAPONTYPE_PISTOL, 99999999)
								FLOAT fDefensive
								fDefensive = GET_TARGET_DEFENSIVE_AREA_SIZE(iTarget)
								SET_PED_SPHERE_DEFENSIVE_AREA( NET_TO_PED(serverBD.Targets[iTarget].niPed), serverbd.Targets[iTarget].vSpawnCoord, fDefensive)
								
								
								SET_PED_CONFIG_FLAG(NET_TO_PED(serverBD.Targets[iTarget].niPed), PCF_KeepRelationshipGroupAfterCleanUp, TRUE)
								SET_PED_CONFIG_FLAG(NET_TO_PED(serverBD.Targets[iTarget].niPed), PCF_DisableGoToWritheWhenInjured, TRUE)
								
								iHealth = GET_ON_FOOT_TARGET_START_HEALTH()
								SET_ENTITY_MAX_HEALTH(NET_TO_PED(serverBD.Targets[iTarget].niPed), iHealth)
								SET_ENTITY_HEALTH(NET_TO_PED(serverBD.Targets[iTarget].niPed), iHealth)
								
								iArmour = GET_ON_FOOT_TARGET_START_ARMOUR()
								SET_PED_ARMOUR(NET_TO_PED(serverBD.Targets[iTarget].niPed), iArmour)
								
								SET_PED_ACCURACY(NET_TO_PED(serverBD.Targets[iTarget].niPed), GET_ON_FOOT_TARGET_ACCURACY())
								SET_ENTITY_LOAD_COLLISION_FLAG(NET_TO_PED(serverBD.Targets[iTarget].niPed), TRUE)
								
								CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - CREATE_TARGET TARGET_TYPE_ON_FOOT_WITH_BODYGUARDS CREATED PED FOR TARGET iTargetNum = ", iTarget, " fDefensive = ", fDefensive, " iHealth = ", iHealth, " iArmour = ",iArmour, " accuracy = ", GET_ON_FOOT_TARGET_ACCURACY())
								SET_BIT(serverBD.iTargetCreatedBitSet, iTarget)
								
								#IF IS_DEBUG_BUILD
									TEXT_LABEL_15 tl15Ped
									tl15Ped = "Ped "
									tl15Ped += iTarget
									SET_PED_NAME_DEBUG(NET_TO_PED(serverbd.Targets[iTarget].niPed), tl15Ped)
								#ENDIF
								
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CREATE_ALL_TARGETS()
	INT i
	REPEAT MAX_TARGETS i
		CREATE_TARGET(i)
	ENDREPEAT
	
	
	i = 0
	REPEAT MAX_TARGETS i
		IF NOT DOES_TARGET_EXIST(i)
			CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - CREATE_ALL_TARGETS THIS TARGET DOESN'T EXIST ", i)
			RETURN FALSE
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC


FUNC BOOL CREATE_BODYGUARDS_FOR_TARGET(INT iTarget)
	MODEL_NAMES mBodyGuard
	INT i
	INT iSpawnIndex
	INT iHealth
	INT iArmour
	VECTOR vSpawn
	FLOAT fSpawn
	
	
	
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_15 tl15Name
	#ENDIF
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.Targets[iTarget].niPed)
	
		SWITCH serverBD.Targets[iTarget].iTargetType
			CASE TARGET_TYPE_DRIVER_WITH_BODYGUARDS
				IF TAKE_CONTROL_OF_NET_ID(serverBD.Targets[iTarget].niVeh)
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.Targets[iTarget].niVeh)
						mBodyGuard = GET_BODYGUARD_MODEL()
						REPEAT MAX_BODYGUARDS_PER_TARGET i
							IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.Targets[iTarget].niBodyguardPed[i])
								IF CAN_REGISTER_MISSION_PEDS(1)
									IF REQUEST_LOAD_MODEL(mBodyGuard)
										IF CREATE_NET_PED_IN_VEHICLE(serverbd.Targets[iTarget].niBodyguardPed[i], 
																	serverbd.Targets[iTarget].niVeh, 
																	PEDTYPE_CRIMINAL, 
																	mBodyGuard, INT_TO_ENUM(VEHICLE_SEAT, i))
											
											SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(NET_TO_PED(serverbd.Targets[iTarget].niBodyguardPed[i]), TRUE)
											
											SET_ENTITY_IS_TARGET_PRIORITY(NET_TO_PED(serverbd.Targets[iTarget].niBodyguardPed[i]), TRUE)
											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NET_TO_PED(serverbd.Targets[iTarget].niBodyguardPed[i]), TRUE)
											SET_PED_RELATIONSHIP_GROUP_HASH(NET_TO_PED(serverbd.Targets[iTarget].niBodyguardPed[i]),relHeadHunterAi)
											
											SET_ENTITY_ONLY_DAMAGED_BY_RELATIONSHIP_GROUP(NET_TO_PED(serverbd.Targets[iTarget].niBodyguardPed[i]), TRUE, relHeadHunterPlayer)
											
											SET_PED_DIES_WHEN_INJURED(NET_TO_PED(serverbd.Targets[iTarget].niBodyguardPed[i]),TRUE)
											GIVE_WEAPON_TO_PED(NET_TO_PED(serverbd.Targets[iTarget].niBodyguardPed[i]), WEAPONTYPE_MICROSMG, 99999999)
											
											SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(serverbd.Targets[iTarget].niBodyguardPed[i]), CA_LEAVE_VEHICLES, FALSE)
											SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(serverbd.Targets[iTarget].niBodyguardPed[i]), CA_ALWAYS_FIGHT, TRUE)
											SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(serverbd.Targets[iTarget].niBodyguardPed[i]), CA_DO_DRIVEBYS, TRUE)
											
											#IF IS_DEBUG_BUILD
												CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [CREATE_BODYGUARDS_FOR_TARGET] CREATED IN-VEHICLE BODYGUARD ", i, " FOR TARGET PED ", iTarget)
												tl15Name  = "T "
												tl15Name += iTarget
												tl15Name += " B "
												tl15Name += "i"
												SET_PED_NAME_DEBUG(NET_TO_PED(serverbd.Targets[iTarget].niBodyguardPed[i]), tl15Name)
											#ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDREPEAT
					ENDIF
				ENDIF
			BREAK
			
			CASE TARGET_TYPE_ON_FOOT_WITH_BODYGUARDS
				mBodyGuard = GET_BODYGUARD_MODEL()
				REPEAT MAX_BODYGUARDS_PER_TARGET i
					IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.Targets[iTarget].niBodyguardPed[i])
						IF CAN_REGISTER_MISSION_PEDS(1)
							IF REQUEST_LOAD_MODEL(mBodyGuard)
								iSpawnIndex = serverBD.Targets[iTarget].iSpawnData
								vSpawn = serverBD.pedSpawnCoords[iSpawnIndex].vBodyguardSpawnCoords[i]
								fSpawn = serverBD.pedSpawnCoords[iSpawnIndex].fBodyguardSpawnHead[i]
								IF NOT ARE_VECTORS_EQUAL(vSpawn, << 0.0, 0.0, 0.0>> )
									IF CREATE_NET_PED(serverbd.Targets[iTarget].niBodyguardPed[i], PEDTYPE_CRIMINAL, mBodyGuard, vSpawn, fSpawn)
										SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(NET_TO_PED(serverbd.Targets[iTarget].niBodyguardPed[i]), TRUE)
											
										SET_ENTITY_IS_TARGET_PRIORITY(NET_TO_PED(serverbd.Targets[iTarget].niBodyguardPed[i]), TRUE)
										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NET_TO_PED(serverbd.Targets[iTarget].niBodyguardPed[i]), TRUE)
										SET_PED_RELATIONSHIP_GROUP_HASH(NET_TO_PED(serverbd.Targets[iTarget].niBodyguardPed[i]),relHeadHunterAi)
										
										SET_ENTITY_ONLY_DAMAGED_BY_RELATIONSHIP_GROUP(NET_TO_PED(serverbd.Targets[iTarget].niBodyguardPed[i]), TRUE, relHeadHunterPlayer)
										
									//	SET_PED_DIES_WHEN_INJURED(NET_TO_PED(serverbd.Targets[iTarget].niBodyguardPed[i]),TRUE)
										GIVE_WEAPON_TO_PED(NET_TO_PED(serverbd.Targets[iTarget].niBodyguardPed[i]), WEAPONTYPE_CARBINERIFLE, 99999999)
										
										SET_PED_COMBAT_MOVEMENT(NET_TO_PED(serverbd.Targets[iTarget].niBodyguardPed[i]), CM_DEFENSIVE)
										SET_PED_CONFIG_FLAG(NET_TO_PED(serverbd.Targets[iTarget].niBodyguardPed[i]), PCF_DontInfluenceWantedLevel, TRUE)
										SET_PED_HIGHLY_PERCEPTIVE(NET_TO_PED(serverbd.Targets[iTarget].niBodyguardPed[i]), TRUE)
										SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(serverbd.Targets[iTarget].niBodyguardPed[i]), CA_SWITCH_TO_ADVANCE_IF_CANT_FIND_COVER, TRUE)
										SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(serverbd.Targets[iTarget].niBodyguardPed[i]), CA_SWITCH_TO_DEFENSIVE_IF_IN_COVER, TRUE)
										SET_PED_TARGET_LOSS_RESPONSE(NET_TO_PED(serverbd.Targets[iTarget].niBodyguardPed[i]), TLR_NEVER_LOSE_TARGET)
										
										SET_PED_SPHERE_DEFENSIVE_AREA( NET_TO_PED(serverBD.Targets[iTarget].niBodyguardPed[i]), vSpawn, 7.0)
										
										SET_PED_CONFIG_FLAG(NET_TO_PED(serverBD.Targets[iTarget].niBodyguardPed[i]), PCF_KeepRelationshipGroupAfterCleanUp, TRUE)
										
										iHealth = GET_ON_FOOT_BODYGUARD_START_HEALTH()
										SET_ENTITY_MAX_HEALTH(NET_TO_PED(serverBD.Targets[iTarget].niBodyguardPed[i]), iHealth)
										SET_ENTITY_HEALTH(NET_TO_PED(serverBD.Targets[iTarget].niBodyguardPed[i]), iHealth)
										
										iArmour = GET_ON_FOOT_BODYGUARD_START_ARMOUR()
										SET_PED_ARMOUR(NET_TO_PED(serverBD.Targets[iTarget].niBodyguardPed[i]), iArmour)
										
										SET_PED_ACCURACY(NET_TO_PED(serverBD.Targets[iTarget].niBodyguardPed[i]), GET_ON_FOOT_BODYGUARD_ACCURACY())
										
										SET_PED_SUFFERS_CRITICAL_HITS(NET_TO_PED(serverBD.Targets[iTarget].niBodyguardPed[i]), FALSE)
										
										SET_PED_CONFIG_FLAG(NET_TO_PED(serverBD.Targets[iTarget].niBodyguardPed[i]), PCF_DisableGoToWritheWhenInjured, TRUE)
										#IF IS_DEBUG_BUILD
											CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [CREATE_BODYGUARDS_FOR_TARGET] CREATED ON-FOOT BODYGUARD ", i, " FOR TARGET PED ", iTarget, " AT COORDS ", vSpawn, " Health = ", iHealth, " iArmour = ", iArmour, " accuracy = ", GET_ON_FOOT_BODYGUARD_ACCURACY() )
											tl15Name  = "T "
											tl15Name += iTarget
											tl15Name += " B "
											tl15Name += i
											SET_PED_NAME_DEBUG(NET_TO_PED(serverbd.Targets[iTarget].niBodyguardPed[i]), tl15Name)
										#ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
										
			BREAK
		ENDSWITCH
	ENDIF
	RETURN FALSE
ENDFUNC
FUNC BOOL SHOULD_TARGET_HAVE_BODYGUARDS(INT iTarget)
	RETURN(serverBD.Targets[iTarget].iTargetType = TARGET_TYPE_DRIVER_WITH_BODYGUARDS
		OR serverBD.Targets[iTarget].iTargetType = TARGET_TYPE_ON_FOOT_WITH_BODYGUARDS)
ENDFUNC

FUNC BOOL DO_TARGET_BODYGUARDS_EXIST(INT iTarget)
	INT i
	
	INT iTargetType =serverBD.Targets[iTarget].iTargetType
	INT iSpawnIndex = serverBD.Targets[iTarget].iSpawnData
	
	VECTOR vBodyguardSpawnCoords
	
	REPEAT MAX_BODYGUARDS_PER_TARGET i
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.Targets[iTarget].niBodyguardPed[i])
			IF iTargetType = TARGET_TYPE_DRIVER_WITH_BODYGUARDS
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER [CREATE_ALL_HEADHUNTER_ENTITIES] [CREATE_ALL_BODYGUARDS] TARGET ", iTarget, " WAITING FOR IN-VEH BODYGUARD ", i)
				RETURN FALSE
			ELIF iTargetType = TARGET_TYPE_ON_FOOT_WITH_BODYGUARDS
				vBodyguardSpawnCoords = serverBD.pedSpawnCoords[iSpawnIndex].vBodyguardSpawnCoords[i]
				IF NOT ARE_VECTORS_EQUAL(vBodyguardSpawnCoords, << 0.0, 0.0, 0.0 >>)
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER [CREATE_ALL_HEADHUNTER_ENTITIES] [CREATE_ALL_BODYGUARDS] TARGET ", iTarget, " WAITING FOR ON-FOOT BODYGUARD ", i)
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC
FUNC BOOL CREATE_ALL_BODYGUARDS()
	INT i
	REPEAT MAX_TARGETS i
		IF SHOULD_TARGET_HAVE_BODYGUARDS(i)
			CREATE_BODYGUARDS_FOR_TARGET(i)
		ENDIF
	ENDREPEAT
	
	i = 0
	REPEAT MAX_TARGETS i
		IF SHOULD_TARGET_HAVE_BODYGUARDS(i)
			IF NOT DO_TARGET_BODYGUARDS_EXIST(i)
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT

	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_TARGET_HAVE_AMBIENT_VEHICLES(INT iTarget)
	IF serverBD.Targets[iTarget].iTargetType != TARGET_TYPE_ON_FOOT_WITH_BODYGUARDS
		RETURN FALSE
	ENDIF
	
	INT iSpawnIndex = serverBD.Targets[iTarget].iSpawnData
	
	RETURN (NOT(ARE_VECTORS_EQUAL(serverBD.pedSpawnCoords[iSpawnIndex].vAmbientVehSpawnCoords[0], << 0.0, 0.0, 0.0 >>)))
ENDFUNC

FUNC BOOL DO_TARGET_AMBIENT_VEHICLES_EXIST(INT iTarget)
	INT iSpawnIndex = serverBD.Targets[iTarget].iSpawnData
	INT i
	
	VECTOR vAmbientVehSpawn
	REPEAT MAX_AMB_VEH_PER_TARGET i
		vAmbientVehSpawn = serverBD.pedSpawnCoords[iSpawnIndex].vAmbientVehSpawnCoords[i]
		IF NOT ARE_VECTORS_EQUAL(vAmbientVehSpawn, << 0.0, 0.0, 0.0 >>)
			IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.Targets[iTarget].niAmbientVeh[i])
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER [CREATE_ALL_HEADHUNTER_ENTITIES] WAITING FOR AMBIENT VEH ", i, " FOR TARGET ", iTarget, " AT COORDS ", vAmbientVehSpawn) 
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

FUNC BOOL CREATE_AMBIENT_VEHICLES_FOR_TARGET(INT iTarget)
	INT iSpawnIndex = serverBD.Targets[iTarget].iSpawnData
	INT i
	
	MODEL_NAMES mVeh
	VECTOR vSpawn
	FLOAT fSpawn
	REPEAT MAX_AMB_VEH_PER_TARGET i
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.Targets[iTarget].niAmbientVeh[i])
			vSpawn = serverBD.pedSpawnCoords[iSpawnIndex].vAmbientVehSpawnCoords[i]
			fSpawn = serverBD.pedSpawnCoords[iSpawnIndex].fAmbientVehSpawnHead[i]
			IF NOT ARE_VECTORS_EQUAL(vSpawn, << 0.0, 0.0, 0.0 >>)
				mVeh = serverBD.pedSpawnCoords[iSpawnIndex].modelAmbVeh[i]
				IF CAN_REGISTER_MISSION_VEHICLES(1)
					IF REQUEST_LOAD_MODEL(mVeh)
						IF NOT IS_BIT_SET(serverbd.Targets[iTarget].iClearAreaBitset, i)
							CLEAR_AREA(vSpawn, 10.0, TRUE, DEFAULT, DEFAULT, TRUE)
							SET_BIT(serverbd.Targets[iTarget].iClearAreaBitset, i)
						ENDIF
						IF CREATE_NET_VEHICLE(serverbd.Targets[iTarget].niAmbientVeh[i], 
													mVeh, 
													vSpawn,
													fSpawn)
							
							SET_VEHICLE_DOORS_LOCKED_FOR_NON_SCRIPT_PLAYERS(NET_TO_VEH(serverbd.Targets[iTarget].niAmbientVeh[i]), TRUE)
							SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(NET_TO_VEH(serverbd.Targets[iTarget].niAmbientVeh[i]), TRUE)
							SET_ENTITY_ONLY_DAMAGED_BY_RELATIONSHIP_GROUP(NET_TO_PED(serverbd.Targets[iTarget].niAmbientVeh[i]), TRUE, relHeadHunterPlayer)
							SET_VEHICLE_AUTOMATICALLY_ATTACHES(NET_TO_VEH(serverbd.Targets[iTarget].niAmbientVeh[i]), FALSE)
							
							SET_ALLOW_RAMMING_SOOP_OR_RAMP(NET_TO_VEH(serverbd.Targets[iTarget].niAmbientVeh[i]), FALSE)
							CPRINTLN(DEBUG_NET_AMBIENT_LR,	"     ---------->     HEADHUNTER - [CREATE_AMBIENT_VEHICLES_FOR_TARGET] CREATED AMBIENT VEH ", i, " FOR TARGET ", iTarget, " AT COORD ", vSpawn)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CREATE_ALL_AMBIENT_VEHICLES()
	INT i
	REPEAT MAX_TARGETS i
		IF SHOULD_TARGET_HAVE_AMBIENT_VEHICLES(i)
			CREATE_AMBIENT_VEHICLES_FOR_TARGET(i)
		ENDIF
	ENDREPEAT
	
	i = 0
	REPEAT MAX_TARGETS i
		IF SHOULD_TARGET_HAVE_AMBIENT_VEHICLES(i)
			IF NOT DO_TARGET_AMBIENT_VEHICLES_EXIST(i)
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC


FUNC BOOL CREATE_ALL_HEADHUNTER_ENTITIES()
	IF NOT CREATE_ALL_TARGETS()
	//	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER [CREATE_ALL_HEADHUNTER_ENTITIES] WAITING FOR CREATE_ALL_TARGETS")
		RETURN FALSE
	ENDIF
	
	IF NOT CREATE_ALL_BODYGUARDS()
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER [CREATE_ALL_HEADHUNTER_ENTITIES] WAITING FOR CREATE_ALL_BODYGUARDS")
		RETURN FALSE
	ENDIF
	
	IF NOT CREATE_ALL_AMBIENT_VEHICLES()
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER [CREATE_ALL_HEADHUNTER_ENTITIES] WAITING FOR CREATE_ALL_AMBIENT_VEHICLES")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL DO_ALL_NETWORK_IDS_EXIST()
	INT i
	
	REPEAT MAX_TARGETS i
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.Targets[i].niPed)
			RETURN FALSE
		ENDIF
	ENDREPEAT
	
	i = 0
	REPEAT MAX_TARGETS i
		IF SHOULD_TARGET_HAVE_BODYGUARDS(i)
			IF NOT DO_TARGET_BODYGUARDS_EXIST(i)
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	
	i = 0
	REPEAT MAX_TARGETS i
		IF SHOULD_TARGET_HAVE_AMBIENT_VEHICLES(i)
			IF NOT DO_TARGET_AMBIENT_VEHICLES_EXIST(i)
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///     Create a local copy of all the bodyguard network indexes. Can then loop through this list when updating the bodyguard AI_BLIP_STRUCT
PROC CREATE_LOCAL_BODYGUARD_LIST()
	INT i, j, k
	
	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [CREATE_LOCAL_BODYGUARD_LIST] Called...")
	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [CREATE_LOCAL_BODYGUARD_LIST]")
	REPEAT MAX_TARGETS j
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.Targets[j].niPed)
			IF serverBD.Targets[j].iTargetType = TARGET_TYPE_ON_FOOT_WITH_BODYGUARDS
				IF SHOULD_TARGET_HAVE_BODYGUARDS(j)
					k = 0
					REPEAT MAX_BODYGUARDS_PER_TARGET k
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.Targets[j].niBodyguardPed[k])
							niLocalBodyguardList[i] = serverBD.Targets[j].niBodyguardPed[k]
							i++
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - [CREATE_LOCAL_BODYGUARD_LIST] ADDING BODYGUARD ", k, " FOR TARGET ", j, " TOTAL BODYGUARDS ADDED = ", i) 
						ENDIF
					ENDREPEAT
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT			
ENDPROC

//PURPOSE: Process the stages for the Server
PROC PROCESS_HEADHUNTER_SERVER()

	
	SWITCH serverBD.eStage
		CASE eAD_TARGETS
			
			
			MAINTAIN_MISSION_DURATION_SERVER()
			
			MAINTAIN_FORCE_RIVALS_ON_MISSION_SERVER()
			
			IF IS_BIT_SET(serverBD.iServerBitSet, biS_AllTargetsKilled)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER [PROCESS_HEADHUNTER_SERVER] serverBD.eStage eAD_TARGETS -> eAD_OFF_MISSION AS biS_AllTargetsKilled")
				serverBD.eStage = eAD_OFF_MISSION 
			ELIF IS_BIT_SET(serverBD.iServerBitSet, biS_DurationExpired)
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER [PROCESS_HEADHUNTER_SERVER] serverBD.eStage eAD_TARGETS -> eAD_OFF_MISSION AS biS_DurationExpired")
				serverBD.eStage = eAD_OFF_MISSION 

			ENDIF
		BREAK

		
		CASE eAD_OFF_MISSION
		BREAK

		CASE eAD_CLEANUP
		BREAK
	ENDSWITCH
ENDPROC

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                      Main Loop                      //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
SCRIPT(MP_MISSION_DATA missionScriptArgs)
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
	AND IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE)
		#IF IS_DEBUG_BUILD	
			CPRINTLN(DEBUG_NET_MAGNATE, "MP: Starting mission ") NET_NL()
		#ENDIF
		
		// Vehry out all the initial game starting duties. 
		PROCESS_PRE_GAME(missionScriptArgs)	
		
		// Setup widgets. Make sure this is done after PROCESS_PRE_GAME()
		#IF IS_DEBUG_BUILD
			CREATE_WIDGETS()
		#ENDIF
	ELSE
		#IF IS_DEBUG_BUILD NET_LOG("HEADHUNTER -  NETWORK_IS_GAME_IN_PROGRESS = FALSE")	#ENDIF
		CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER -  MISSION END - NETWORK_IS_GAME_IN_PROGRESS = FALSE - SCRIPT CLEANUP P     <----------     ") NET_NL()
		SCRIPT_CLEANUP()
	ENDIF
	
	// Main loop
	WHILE TRUE

		// One wait to rule them all. This can be the ONLY wait from here on in.....
		MP_LOOP_WAIT_ZERO()
				
		// If we have a match end event, bail
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			#IF IS_DEBUG_BUILD NET_LOG("SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE")	#ENDIF
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER -  MISSION END - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE - SCRIPT CLEANUP B     <----------     ") NET_NL()
			SCRIPT_CLEANUP()
//		ELIF GB_SHOULD_KILL_ACTIVE_BOSS_MISSION() 
//			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER -  MISSION END - GB_SHOULD_KILL_ACTIVE_BOSS_MISSION - SCRIPT CLEANUP <----------     ") NET_NL()
//			SCRIPT_CLEANUP()
		ENDIF
		
		//Check if player is in a tutorial session and end
//		IF NETWORK_IS_IN_TUTORIAL_SESSION()
//			#IF IS_DEBUG_BUILD NET_LOG("PLAYER IN TUT SESSION")	#ENDIF
//			NET_PRINT_TIME() NET_PRINT("     ---------->     HEADHUNTER -  MISSION END - PLAYER IN TUTORIAL SESSION - SCRIPT CLEANUP C     <----------     ") NET_NL()
//			SCRIPT_CLEANUP()
//		ENDIF
		
		// Deal with the debug
		#IF IS_DEBUG_BUILD		
			UPDATE_WIDGETS()
			
			MAINTAIN_DEBUG_KEYS()
		#ENDIF		
	
		// If we need to leave the mission, make sure we just straight to the end stage and terminate the script - DON'T NEED FOR AMBIENT MISSION
		/*IF SHOULD_PLAYER_LEAVE_MP_MISSION() 
			#IF IS_DEBUG_BUILD
			NET_PRINT_TIME() NET_PRINT( GET_MP_MISSION_NAME(thisMission), " Leave mission = TRUE \n")
			#ENDIF
			playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_LEAVE
			#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_LEAVE")	#ENDIF
		ENDIF*/
		
		g_sGb_Telemetry_data.sdata.m_match1 = serverBD.iMatchId1
		g_sGb_Telemetry_data.sdata.m_match2 = serverBD.iMatchId2
		
		// -----------------------------------
		// Process your game logic.....
		SWITCH GET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT())
			
			// Wait untill the server gives the all go before moving on
			CASE GAME_STATE_INI
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					IF DO_ALL_NETWORK_IDS_EXIST()
						GET_MY_BOSS_PARTICPANT_ID()
						
						
						IF DID_MY_BOSS_LAUNCH_GB_HEADHUNTER()
							GB_COMMON_BOSS_MISSION_SETUP(FMMC_TYPE_GB_HEADHUNTER)
							SET_MAX_WANTED_LEVEL(3)
							SET_MENTAL_STATE_MULTIPLIER(0.0)
							relMyFmGroup = GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID())
							SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(), relHeadHunterPlayer)
						
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - SET_MAX_WANTED_LEVEL(3)")
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - relMyFmGroup = ", ENUM_TO_INT(relMyFmGroup))
							
						ELSE
							GB_COMMON_BOSS_MISSION_SETUP(FMMC_TYPE_GB_HEADHUNTER, FALSE)
							
							
						ENDIF
						
						SET_LAUNCHING_GANG_HUD_COLOUR()
						
						INIT_TARGETS_CLIENT()
						
						CREATE_LOCAL_BODYGUARD_LIST()
						
						playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_RUNNING
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER -  playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_RUNNING    <----------     ") NET_NL()
						#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_RUNNING")	#ENDIF
					ENDIF
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER -  playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY 1    <----------     ") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_TERMINATE_DELAY")	#ENDIF
				ENDIF
			BREAK
			
			// Main gameplay state.
			CASE GAME_STATE_RUNNING
				IF GB_SHOULD_QUIT_ACTIVE_BOSS_MISSION()
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_LEAVE
				ENDIF
			
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					MAINTAIN_MAX_PLAYER_CHECKS_CLIENT()
					
					PROCESS_TARGET_PED_BODY()
					
					PROCESS_HEADHUNTER_CLIENT()
					
					PROCESS_HEADHUNTER_SCTV_CLIENT()
					
					PROCESS_EVENTS()
					
					IF DID_MY_BOSS_LAUNCH_GB_HEADHUNTER()
					OR DID_LOCAL_PLAYER_JOIN_AS_RIVAL()
						DRAW_CHALLENGE_DPAD_LBD(g_GBLeaderboardStruct.challengeLbdStruct, g_GBLeaderboardStruct.siDpadMovie, SUB_BOSS, g_GBLeaderboardStruct.dpadVariables, 
												g_GBLeaderboardStruct.fmDpadStruct)
					ENDIF
					
					MAINTAIN_DISTANCE_CHECKS_FOR_VARIATION()
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY
					UPDATE_GLOBAL_RELATIONSHIP_GROUP_FOR_MISSION_END()
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER -  playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY 2   <----------     ") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_TERMINATE_DELAY")	#ENDIF
				ENDIF
				
				MAINTAIN_GB_HEADHUNTER_MUSIC()
			BREAK
			
			CASE GAME_STATE_TERMINATE_DELAY 
				SET_MISSION_FINISHED(serverBD.MissionTerminateDelayTimer)
		      	IF IS_MISSION_READY_TO_CLEANUP(serverBD.MissionTerminateDelayTimer)
		            playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER -  playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END 3    <----------     ") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_END 3")	#ENDIF
		      	ENDIF
			BREAK
			
			// The game stage the local player is placed when we want him to leave a mission
			CASE GAME_STATE_LEAVE
				//LEAVE_MY_MP_MISSION(thisMission)
				playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER -  playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END 2    <----------     ") NET_NL()
				#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_END 2")	#ENDIF
			FALLTHRU
			
			//Cleans up then terminates the mission
			CASE GAME_STATE_END
				CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER -  SCRIPT CLEANUP A     <----------     ") NET_NL()
				SCRIPT_CLEANUP()
			BREAK

		ENDSWITCH
		
		// -----------------------------------
		// Process server game logic		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			
			SWITCH GET_SERVER_MISSION_STATE()
				
				// For CnC group missions, we may want to wait for all players to have launched the script
				CASE GAME_STATE_INI
					
					INIT_SPAWN_COORDS()
					
					INIT_TARGETS_SERVER()
					
					IF CREATE_ALL_HEADHUNTER_ENTITIES()
						PLAYSTATS_CREATE_MATCH_HISTORY_ID_2(serverBD.iMatchId1, serverBD.iMatchId2)
						serverBD.iServerGameState = GAME_STATE_RUNNING
						serverBD.iBossPartWhoLaunched = PARTICIPANT_ID_TO_INT()
						serverBD.iBossPlayerWhoLaunched = NATIVE_TO_INT(PLAYER_ID())
						GB_COMMON_BOSS_MISSION_SETUP(FMMC_TYPE_GB_HEADHUNTER)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER -  serverBD.iServerGameState = GAME_STATE_RUNNING    <---------- ")
						#IF IS_DEBUG_BUILD NET_LOG_Host("iServerGameState = GAME_STATE_RUNNING")	#ENDIF
					ELSE
					//	CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER - WAITING FOR CREATE_ALL_HEADHUNTER_ENTITIES")
					ENDIF
				BREAK
				
				// Look for game end conditions
				CASE GAME_STATE_RUNNING
					
					MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER()
					
					MAINTAIN_UNLOCK_TARGET_VEHICLE_DOORS()
					
					PROCESS_TARGET_PED_BRAIN()
					
					PROCESS_HEADHUNTER_SERVER()

							
					#IF IS_DEBUG_BUILD
						IF bHostEndMissionNow
						OR MPGlobalsAmbience.bKillActiveEvent
							MPGlobalsAmbience.bKillActiveEvent = FALSE
							serverBD.iServerGameState = GAME_STATE_END
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER -  serverBD.iServerGameState = GAME_STATE_END 1    <----------     ") NET_NL()
							#IF IS_DEBUG_BUILD NET_LOG_Host("iServerGameState = GAME_STATE_END 1")	#ENDIF
						ENDIF
					#ENDIF	
					
					IF HAVE_MISSION_END_CONDITIONS_BEEN_MET()
						serverBD.iServerGameState = GAME_STATE_END
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER -  serverBD.iServerGameState = GAME_STATE_END 4    <----------     ") NET_NL()
						#IF IS_DEBUG_BUILD NET_LOG_Host("iServerGameState = GAME_STATE_END 4")	#ENDIF
					ELIF GB_SHOULD_KILL_ACTIVE_BOSS_MISSION() 
						serverBD.iServerGameState = GAME_STATE_END
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     HEADHUNTER -  serverBD.iServerGameState = GAME_STATE_END 4  AS GB_SHOULD_KILL_ACTIVE_BOSS_MISSION  <----------     ") NET_NL()
					ENDIF
					
				BREAK
				
				CASE GAME_STATE_END
				BREAK
				
			ENDSWITCH
			
		ENDIF
		
	ENDWHILE
	
	
ENDSCRIPT
