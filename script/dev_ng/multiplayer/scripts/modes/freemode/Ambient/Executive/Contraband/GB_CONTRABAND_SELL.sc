
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Written by:  Christopher Speirs & Steve Tiley																		//
// Date: 		02/02/2016					 																			//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Crate functions SERVER_MAINTAIN_DROP_OFFS / MAINTAIN_CRATE_DROP_SERVER / BROADCAST_CRATE_EVENT /SERVER_INCREMENT_DROP_COUNT


USING "globals.sch"
USING "net_mission.sch"
USING "net_common_functions.sch"
USING "net_realty_warehouse.sch"




USING "GB_CONTRABAND_SELL_HEADER_PRIVATE.sch"

FUNC BOOL SERVER_SET_SHIPMENT_TYPE()
	IF serverBD.eShipmentType = eSELL_SHIPMENT_TYPE_INVALID
		INT iSize
		eSELL_SHIPMENT_TYPE eType = GET_SELL_SHIPMENT_TYPE(GET_SELL_VAR(), iSize)
		SET_SHIPMENT_TYPE(eType)
		SET_SHIPMENT_SIZE(iSize)
		RETURN TRUE
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SETUP_DAMAGE_TRACKING(BOOL bON)
	IF bLAND_DAMAGE()
		INT i
		REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[i])
				IF bON
					IF NOT IS_DAMAGE_TRACKER_ACTIVE_ON_NETWORK_ID(serverBD.niVeh[i])
						PRINTLN("[EXEC1] [CTRB_SELL] SETUP_DAMAGE_TRACKING TRUE for i = ", i)
						ACTIVATE_DAMAGE_TRACKER_ON_NETWORK_ID(serverBD.niVeh[i], TRUE)
					ENDIF
				ELSE
					IF IS_DAMAGE_TRACKER_ACTIVE_ON_NETWORK_ID(serverBD.niVeh[i])
						PRINTLN("[EXEC1] [CTRB_SELL] SETUP_DAMAGE_TRACKING FALSE for i = ", i)
						ACTIVATE_DAMAGE_TRACKER_ON_NETWORK_ID(serverBD.niVeh[i], FALSE)
					ENDIF
				ENDIF
				
			ENDIF				
		ENDREPEAT
	ENDIF
ENDPROC

FUNC BOOL INIT_CLIENT()
	
	PRINTLN("[EXEC1] [CTRB_SELL] INIT_CLIENT - Local player is not inside warehouse, proceeding with client initialisation")
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_FlowPlaySellMissionPlaylist")
		MPGlobalsAmbience.sMagnateGangBossData.iSellMissionPlaylistCount++
	ENDIF
	#ENDIF
	
	SET_SUPPRESS_GANG_CHASE_MODELS_FOR_VARIATION( TRUE)
		
	IF bAIR_LOW()
		sLocaldata.iAltitudeWarningSound = GET_SOUND_ID()
		PRINTLN("[EXEC1] [CTRB_SELL] [INIT_CLIENT] Got iAltitudeWarningSound sound id")
	ENDIF
	IF NOT GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID())
		IF serverBD.iCratesToSell != -1
			g_iContraSellMissionCratesToSell = serverBD.iCratesToSell
			g_iContraSellMissionBuyerID = serverBD.iBuyerID
			PRINTLN("[EXEC1] [CTRB_SELL] [INIT_CLIENT] I'm not boss, set g_iContraSellMissionCratesToSell = ", g_iContraSellMissionCratesToSell, " g_iContraSellMissionBuyerID = ", g_iContraSellMissionBuyerID)
		ENDIF
	ENDIF
	
	playerBD[PARTICIPANT_ID_TO_INT()].damagerPlayer = INVALID_PLAYER_INDEX()
	
	RETURN TRUE
ENDFUNC

//╔═════════════════════════════════════════════════════════════════════════════╗
//║				BIGMESSAGE														║

FUNC BOOL IS_LOCAL_BIGMESSAGE_BIT0_SET(INT iBitset)
	RETURN IS_BIT_SET(sLocaldata.iBigMessageBitSet, iBitset)
ENDFUNC

PROC SET_LOCAL_BIGMESSAGE_BIT0(INT iBitset #IF IS_DEBUG_BUILD , BOOL bBlockLogPrints = FALSE #ENDIF)

	#IF IS_DEBUG_BUILD
	IF NOT bBlockLogPrints
		IF NOT IS_BIT_SET(iLocalBitset0, iBitset)
			PRINTLN("[EXEC1] [CTRB_SELL] SET_LOCAL_BIGMESSAGE_BIT0 - ", iBitset)
		ENDIF
	ENDIF
	#ENDIF
	
	SET_BIT(sLocaldata.iBigMessageBitSet, iBitset)
	
ENDPROC

PROC CLEAR_LOCAL_BIGMESSAGE_BIT0(INT iBitset #IF IS_DEBUG_BUILD , BOOL bBlockLogPrints = FALSE #ENDIF)

	#IF IS_DEBUG_BUILD
	IF NOT bBlockLogPrints
		IF IS_BIT_SET(iLocalBitset0, iBitset)
			PRINTLN("[EXEC1] [CTRB_SELL] CLEAR_LOCAL_BIGMESSAGE_BIT0 - ", iBitset)
		ENDIF
	ENDIF
	#ENDIF
	
	CLEAR_BIT(sLocaldata.iBigMessageBitSet, iBitset)
	
ENDPROC

#IF IS_DEBUG_BUILD
/// PURPOSE:
///    Prints information about the big message that has just been requested to draw
/// PARAMS:
///    iBigMessage - the index of the message that was drawn
///    title - the title of the message
///    strapline - the subtitle of the message
PROC PRINT_BIG_MESSAGE_DEBUG_INFORMATION(INT iBigMessage, STRING title, STRING strapline)
	PRINTLN("[EXEC1] [CTRB_SELL]  - ************ NEW SHARD HAS BEEN SETUP *************")
	PRINTLN("[EXEC1] [CTRB_SELL]  - PRINT_BIG_MESSAGE_DEBUG_INFORMATION - Big Message:	", iBigMessage)
	PRINTLN("[EXEC1] [CTRB_SELL]  - PRINT_BIG_MESSAGE_DEBUG_INFORMATION - Title: 			", GET_FILENAME_FOR_AUDIO_CONVERSATION(title))
	PRINTLN("[EXEC1] [CTRB_SELL]  - PRINT_BIG_MESSAGE_DEBUG_INFORMATION - Strapline:		", GET_FILENAME_FOR_AUDIO_CONVERSATION(strapline))
	PRINTLN("[EXEC1] [CTRB_SELL]  - ***************************************************")
ENDPROC
#ENDIF

PROC DO_BIG_MESSAGE( BIG_MESSAGE_ENUM thisMessage, eEND_REASON endReason = eENDREASON_NO_REASON_YET)

	STRING sMessageTitle, sStrapline
	BIG_MESSAGE_TYPE bmType
//	BOOL bPlayerMessage
	
	IF IS_SAFE_FOR_BIG_MESSAGE()
	OR ( thisMessage = eBIGM_GANG_FAIL )// AND GET_END_REASON() <> eENDREASON_NO_BOSS_LEFT ) // Show shard after death shard in these instances (except for VIP left)
		IF NOT IS_LOCAL_BIGMESSAGE_BIT0_SET(ENUM_TO_INT(thisMessage))
			SWITCH thisMessage
			
				// Gang Messages
			
				CASE eBIGM_GANG_START
					sMessageTitle = "GB_SELL_BM_01"
					IF bAIR_VARIATION()
						IF DOES_SHIPMENT_TYPE_HAVE_MULTIPLE_VEHICLES()
							sStrapline = "GB_SELL_BM_06b"
						ELSE
							sStrapline = "GB_SELL_BM_06"
						ENDIF
					ELIF bSEA_VARIATION()
						sStrapline = "GB_SELL_BM_07"
					ELSE
						IF DOES_SHIPMENT_TYPE_HAVE_MULTIPLE_VEHICLES()
							sStrapline = "GB_SELL_BM_02b"
						ELSE
							sStrapline = "GB_SELL_BM_02"
						ENDIF
						
					ENDIF
					bmType = BIG_MESSAGE_GB_START_OF_JOB
					
					SETUP_NEW_BIG_MESSAGE(bmType, sMessageTitle, sStrapline)
					SET_LOCAL_BIGMESSAGE_BIT0(ENUM_TO_INT(thisMessage))
				BREAK
				
				CASE eBIGM_GANG_SUCCESS
					sMessageTitle = "EXEC_SOLD"
					sStrapline = "GB_SELL_BM_03"
					bmType = BIG_MESSAGE_GB_END_OF_JOB_SUCCESS
					
					SETUP_NEW_BIG_MESSAGE(bmType, sMessageTitle, sStrapline)
					SET_LOCAL_BIGMESSAGE_BIT0(ENUM_TO_INT(thisMessage))
				BREAK
				
				CASE eBIGM_GANG_FAIL
				
					sMessageTitle = "BIGM_CLOST"
					
					bmType = BIG_MESSAGE_GB_END_OF_JOB_FAIL
				
					IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
						IF GET_DOES_BUYER_ONLY_WANT_SPECIAL_ITEMS()
							IF iNumCratesForShard > 1
								sStrapline = "GB_SELL_BM_10p"
							ELSE
								sStrapline = "GB_SELL_BM_10"
							ENDIF
							
							IF iNumCratesReturned > 0
								PRINT_TICKER_WITH_TWO_INTS("GB_SELL_TICK_1", iNumCratesReturned, GET_CONTRABAND_UNITS_TO_SELL_FROM_BUYER())
							ELSE
								PRINT_TICKER("GB_SELL_TICK_3")
							ENDIF
						ELSE
							IF endReason = eENDREASON_TRUCK_DESTROYED
								IF iNumCratesForShard > 1
									sStrapline = "GB_SELL_BM_08p"
								ELSE
									sStrapline = "GB_SELL_BM_08"
								ENDIF
							ELIF endReason = eENDREASON_TIME_UP
							OR endReason = eENDREASON_VEHICLE_STUCK
								IF iNumCratesForShard > 1
									sStrapline = "GB_SELL_BM_09p"
								ELSE
									sStrapline = "GB_SELL_BM_09"
								ENDIF
							ELSE
								sStrapline = "GB_SELL_BM_04"
							ENDIF
							
							IF iNumCratesReturned > 0
								PRINT_TICKER_WITH_TWO_INTS("GB_SELL_TICK_1", iNumCratesReturned, GET_CONTRABAND_UNITS_TO_SELL_FROM_BUYER())
							ELSE
								PRINT_TICKER("GB_SELL_TICK_2")
							ENDIF
						ENDIF
						
						IF iNumCratesForShard > -1
							SETUP_NEW_BIG_MESSAGE_WITH_INT_AND_STRING(bmType, iNumCratesForShard, sStrapline, sMessageTitle)
						ENDIF
					ELSE
						sStrapline = "GB_SELL_BM_04"
						
						SETUP_NEW_BIG_MESSAGE(bmType, sMessageTitle, sStrapline)
					ENDIF
					
					SET_LOCAL_BIGMESSAGE_BIT0(ENUM_TO_INT(thisMessage))
					
				BREAK
				
			ENDSWITCH

			
			#IF IS_DEBUG_BUILD	
			PRINT_BIG_MESSAGE_DEBUG_INFORMATION(ENUM_TO_INT(thisMessage), sMessageTitle, sStrapline) 
			#ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_RESTRICT_MAX_WANTED_LEVEL()
	INT iBuyerToCheck = GET_BUYER()
	IF GET_CONTRABAND_UNITS_TO_SELL_FROM_BUYER() = 1
		iBuyerToCheck = 3
	ENDIF
	
	IF iBuyerToCheck = 2		
		RETURN g_sMPTunables.iEXEC_SELL_MEDIUM_WANTED_RATING_CAP < 5  
	ELIF iBuyerToCheck = 3
		RETURN g_sMPTunables.iEXEC_SELL_EASY_WANTED_RATING_CAP < 5  
	ENDIF
	
	
	RETURN g_sMPTunables.iEXEC_SELL_HARD_WANTED_CAP < 5
ENDFUNC

FUNC INT GET_MAX_WANTED_LEVEL_FOR_SELL_CONTRABAND()
	INT iBuyerToCheck = GET_BUYER()
	IF GET_CONTRABAND_UNITS_TO_SELL_FROM_BUYER() = 1
		iBuyerToCheck = 3
	ENDIF
	
	IF SHOULD_RESTRICT_MAX_WANTED_LEVEL()
	//	RETURN 2
		IF iBuyerToCheck = 2		// Medium
			RETURN g_sMPTunables.iEXEC_SELL_MEDIUM_WANTED_RATING_CAP
		ELIF iBuyerToCheck = 3	// Easy
			RETURN g_sMPTunables.iEXEC_SELL_EASY_WANTED_RATING_CAP
		ENDIF
	ENDIF
	
	RETURN g_sMPTunables.iEXEC_SELL_HARD_WANTED_CAP 
ENDFUNC

PROC MAINTAIN_RESTRICT_MAX_WANTED_LEVEL()
	IF bAIR_RESTRICTED()
	OR bAIR_LOW()
	OR bSTING()
		IF SHOULD_RESTRICT_MAX_WANTED_LEVEL()
			IF GET_MAX_WANTED_LEVEL() > GET_MAX_WANTED_LEVEL_FOR_SELL_CONTRABAND()
				SET_MAX_WANTED_LEVEL(GET_MAX_WANTED_LEVEL_FOR_SELL_CONTRABAND())
				PRINTLN("[EXEC1] [CTRB_SELL] [MAINTAIN_RESTRICT_MAX_WANTED_LEVEL] SET_MAX_WANTED_LEVEL ")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL DO_EASY_HELP_TEXT()
	RETURN (GET_BUYER() = 2) OR (GET_BUYER() = 3) OR (GET_CONTRABAND_UNITS_TO_SELL_FROM_BUYER() = 1) 
ENDFUNC

ENUM eCTRA_REMOVAL
	eREMOVE_INITIAL,
	eREMOVE_LOSS,
	eREMOVE_WIN,
	eREMOVE_EMPTY
ENDENUM
#IF IS_DEBUG_BUILD
FUNC STRING GET_eCTRA_REMOVAL_DEBUG_PRINT(eCTRA_REMOVAL eRemove)
	SWITCH eRemove
		CASE eREMOVE_INITIAL	RETURN "eREMOVE_INITIAL"
		CASE eREMOVE_LOSS		RETURN "eREMOVE_LOSS"
		CASE eREMOVE_WIN		RETURN "eREMOVE_WIN"
		CASE eREMOVE_EMPTY		RETURN "eREMOVE_EMPTY"
	ENDSWITCH
	RETURN ""
ENDFUNC
	#ENDIF


PROC HANDLE_CONTRABAND_REMOVAL(eCTRA_REMOVAL eRemove)
	// Dont do any of this if we've removed cash at the end
	IF NOT IS_SERVER_BIT1_SET(eSERVERBITSET1_CONTRABAND_REMOVED_AT_END)
		IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
			INT iContrabandToRemove
			INT iEarnings = 0
//			BOOL bSpecialItemsOnly = FALSE
			REMOVE_CONTRABAND_REASON_ENUM eReason
			
			// [NOTE FOR BOBBY] - we now only sell whatever Mark/Martin tell us to sell, not all of the contraband in the warehouse
			
			INT iTotalContraband = GET_CONTRABAND_UNITS_TO_SELL_FROM_BUYER() 
			
			FLOAT fNumVehicles = TO_FLOAT( GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() )
			
			FLOAT fDroppedOffAmount, fDropTarget, fTotalContraband
			
			PRINTLN("[CTRB_SELL] HANDLE_CONTRABAND_REMOVAL, eRemove = ", GET_eCTRA_REMOVAL_DEBUG_PRINT(eRemove), " - iTotalContraband = ", iTotalContraband, " - serverBD.iStartingWarehouse = ", serverBD.iStartingWarehouse)
			
			SWITCH eRemove
				CASE eREMOVE_INITIAL
					IF iTotalContraband > 1
						iContrabandToRemove = GET_INITIAL_CONTRABAND_QUANTITY_TO_REMOVE_FROM_SHIPMENT_TYPE()
						iInitialContrabandRemoved = iContrabandToRemove
						eReason = REMOVE_CONTRA_MISSION_STARTED
						PRINTLN("[CTRB_SELL] HANDLE_CONTRABAND_REMOVAL, eREMOVE_INITIAL = ", iContrabandToRemove)
					ELSE
						PRINTLN("[CTRB_SELL] HANDLE_CONTRABAND_REMOVAL, eREMOVE_INITIAL - iTotalContraband = ", iTotalContraband, "; not removing any")
					ENDIF
				BREAK
				CASE eREMOVE_LOSS	 
				
					IF GET_END_REASON() = eENDREASON_TRUCK_DESTROYED
						
						fTotalContraband 	= TO_FLOAT(iTotalContraband)
						IF serverBD.iFailVehicle = 0
							fDroppedOffAmount 	= TO_FLOAT(serverBD.iVeh0DropCount)
						ElIF serverBD.iFailVehicle = 1
							fDroppedOffAmount 	= TO_FLOAT(serverBD.iVeh1DropCount)
						ELIF serverBD.iFailVehicle = 2
							fDroppedOffAmount 	= TO_FLOAT(serverBD.iVeh2DropCount)
						ELSE
							fDroppedOffAmount 	= TO_FLOAT(serverBD.iDroppedOffCount)
						ENDIF
						fDropTarget			= TO_FLOAT(iDROP_TARGET())
						
						PRINTLN("[CTRB_SELL] HANDLE_CONTRABAND_REMOVAL, fTotalContraband = ", fTotalContraband, " fDroppedOffAmount = ", fDroppedOffAmount, " fDropTarget = ", fDropTarget)
			
						IF bMULTIPLE()
						OR bSTING()
						OR ( bAIR_VARIATION() AND NOT bAIR_CLEAR_AREA() )
							iContrabandToRemove =  FLOOR( ( fTotalContraband / fNumVehicles ) * ( 1 - ( fDroppedOffAmount / fDropTarget ) ) )
							PRINTLN("[CTRB_SELL] HANDLE_CONTRABAND_REMOVAL - iContrabandToRemove =  FLOOR( ( fTotalContraband / fNumVehicles ) * ( 1 - ( fDroppedOffAmount / fDropTarget ) ) ) = iContrabandToRemove = ", iContrabandToRemove)
							PRINTLN("[CTRB_SELL] HANDLE_CONTRABAND_REMOVAL - ", iContrabandToRemove, " =  FLOOR( ( ", fTotalContraband, " / ", fNumVehicles, " ) * ( 1 - ( ", fDroppedOffAmount, " / ", fDropTarget, " ) ) ) = ", iContrabandToRemove)
						ELSE
							iContrabandToRemove =  FLOOR(fTotalContraband / fNumVehicles )
							PRINTLN("[CTRB_SELL] HANDLE_CONTRABAND_REMOVAL - iContrabandToRemove =  FLOOR(fTotalContraband / fNumVehicles ) = iContrabandToRemove = ", iContrabandToRemove)
							PRINTLN("[CTRB_SELL] HANDLE_CONTRABAND_REMOVAL - ", iContrabandToRemove, " =  FLOOR(", fTotalContraband, " / ", fNumVehicles, " ) = ",iContrabandToRemove)
						ENDIF
						
						PRINTLN("[CTRB_SELL] HANDLE_CONTRABAND_REMOVAL, eENDREASON_TRUCK_DESTROYED, fNumVehicles = ", fNumVehicles, " iContrabandToRemove = ", iContrabandToRemove)
					
						PRINTLN("[CTRB_SELL] HANDLE_CONTRABAND_REMOVAL, FLOOR(TO_FLOAT(iContrabandToRemove) * g_sMPTunables.fexec_stock_remove_at_fail_sell) = ", FLOOR(TO_FLOAT(iContrabandToRemove) * g_sMPTunables.fexec_stock_remove_at_fail_sell))
						PRINTLN("[CTRB_SELL] HANDLE_CONTRABAND_REMOVAL, FLOOR(TO_FLOAT(", iContrabandToRemove, ") * ", g_sMPTunables.fexec_stock_remove_at_fail_sell, ") = ", FLOOR(TO_FLOAT(iContrabandToRemove) * g_sMPTunables.fexec_stock_remove_at_fail_sell))
						iContrabandToRemove =  FLOOR(TO_FLOAT(iContrabandToRemove) * g_sMPTunables.fexec_stock_remove_at_fail_sell)
		
						//Now remove the amount we've already removed, if there's any to remove
						iContrabandToRemove -= iInitialContrabandRemoved
						PRINTLN("[CTRB_SELL] HANDLE_CONTRABAND_REMOVAL, , iContrabandToRemove,  -= , iInitialContrabandRemoved,  = , iContrabandToRemove")
						PRINTLN("[CTRB_SELL] HANDLE_CONTRABAND_REMOVAL, ", iContrabandToRemove + iInitialContrabandRemoved, " -= ", iInitialContrabandRemoved, " = ", iContrabandToRemove)
						
						eReason = REMOVE_CONTRA_MISSION_FAILED
						
						IF iContrabandToRemove < g_sMPTunables.iEXEC_CONTRABAND_MINIMUM_LOST
							iContrabandToRemove = g_sMPTunables.iEXEC_CONTRABAND_MINIMUM_LOST
						ENDIF
						
						iNumCratesForShard = iContrabandToRemove + iInitialContrabandRemoved
						iNumCratesReturned = iTotalContraband - iNumCratesForShard
						
						IF iContrabandToRemove > 0
							SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_AM_SELL_SPECIAL_CARGO)
						ENDIF
						
						PRINTLN("[CTRB_SELL] HANDLE_CONTRABAND_REMOVAL, iNumCratesForShard = ", iNumCratesForShard)
						PRINTLN("[CTRB_SELL] HANDLE_CONTRABAND_REMOVAL, iNumCratesReturned = ", iNumCratesReturned)
						
						PRINTLN("[CTRB_SELL] HANDLE_CONTRABAND_REMOVAL, eREMOVE_LOSS = ", iContrabandToRemove, " iEXEC_CONTRABAND_MINIMUM_LOS = ", g_sMPTunables.iEXEC_CONTRABAND_MINIMUM_LOST)
					#IF IS_DEBUG_BUILD
					ELSE
						PRINTLN("[CTRB_SELL] HANDLE_CONTRABAND_REMOVAL, GET_END_REASON() != eENDREASON_TRUCK_DESTROYED")
						#ENDIF
					ENDIF
				BREAK
				CASE eREMOVE_WIN	 
					iContrabandToRemove = (iTotalContraband - iInitialContrabandRemoved)
					iEarnings = MPGlobalsAmbience.sMagnateGangBossData.iContrabandSellEarnings
					eReason = REMOVE_CONTRA_MISSION_PASSED
					SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_AM_SELL_SPECIAL_CARGO)
					PRINTLN("[CTRB_SELL] HANDLE_CONTRABAND_REMOVAL, eREMOVE_WIN = (iTotalContraband - iInitialContrabandRemoved) = ", iTotalContraband," - ", iInitialContrabandRemoved, " =  ", iContrabandToRemove)
				BREAK
			ENDSWITCH
			
			//Cap it
			IF iContrabandToRemove > GET_CONTRABAND_UNITS_TOTAL_FOR_WAREHOUSE(serverbd.iStartingWarehouse)
				PRINTLN("[CTRB_SELL] HANDLE_CONTRABAND_REMOVAL, iContrabandToRemove < 1 - iContrabandToRemove = ", GET_CONTRABAND_UNITS_TOTAL_FOR_WAREHOUSE(serverbd.iStartingWarehouse))
				iContrabandToRemove = GET_CONTRABAND_UNITS_TOTAL_FOR_WAREHOUSE(serverbd.iStartingWarehouse)
			ENDIF
			
			//If it will go nexitave then set it to the total, i.e. clear it all. 
			IF iTotalContraband - iContrabandToRemove <= 0
				iContrabandToRemove = iTotalContraband
				PRINTLN("[CTRB_SELL] HANDLE_CONTRABAND_REMOVAL, iContrabandToRemove = iTotalContraband = ", iContrabandToRemove)
			ENDIF
		
			IF iContrabandToRemove > 0				
				REMOVE_CONTRABAND_FROM_WAREHOUSE(serverBD.iStartingWarehouse, iContrabandToRemove, iEarnings, eReason, eResult, GET_DOES_BUYER_ONLY_WANT_SPECIAL_ITEMS())
				PRINTLN("[CTRB_SELL] HANDLE_CONTRABAND_REMOVAL, REMOVE_CONTRABAND_FROM_WAREHOUSE(", serverBD.iStartingWarehouse, ", ", iContrabandToRemove, ") - iContrabandToRemove = ", iContrabandToRemove)
				PRINTLN("[CTRB_SELL] HANDLE_CONTRABAND_REMOVAL, GET_DOES_BUYER_ONLY_WANT_SPECIAL_ITEMS() = ", GET_DOES_BUYER_ONLY_WANT_SPECIAL_ITEMS())
			ENDIF
		ENDIF
	ENDIF
ENDPROC

BOOL bIWon = FALSE

PROC HANDLE_REWARDS()
	
	IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_CALCULATED_REWARDS)
		IF IS_NET_PLAYER_OK(PLAYER_ID())
			IF GET_END_REASON() != eENDREASON_NO_REASON_YET
			
				PLAYER_INDEX playerId = PLAYER_ID() // Set the winning player here before passing into wanted cleanup function. Using PLAYER_ID() as placeholder right now.
				
				IF GET_END_REASON() = eENDREASON_WIN_CONDITION_TRIGGERED
					bIWon = TRUE
//					IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
					
						// [NOTE FOR BOBBY] - we're no longer selling the whole lot so we need to let rewards function know how many we're selling;
						//						below this, we add the one we took initially so that the player is rewarded for all and not just whats left
					
						sGangBossManageRewardsData.iContrabandUnitsSold = GET_CONTRABAND_UNITS_TO_SELL_FROM_BUYER()
					
//						IF NOT IS_INITIAL_CONTRABAND_REMOVAL_DISABLED()
//						AND NOT GET_DOES_BUYER_ONLY_WANT_SPECIAL_ITEMS() // [NOTE FOR BOBBY] - dont give the initial taken if the buyer only wants special
//							sGangBossManageRewardsData.iAdditionalContrabandUnitsForRewards = iInitialContrabandRemoved
//							PRINTLN("[EXEC1] [CTRB_SELL] HANDLE_REWARDS - I won - setting sGangBossManageRewardsData.iAdditionalContrabandUnitsForRewards to ", iInitialContrabandRemoved)
//						ENDIF
					
						IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
						AND g_MP_STAT_LIFETIME_SELL_MISSIONS_COMPLETED[GET_ACTIVE_CHARACTER_SLOT()] = 0
							GB_SET_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_NOTIFICATION_PASSED_FIRST_SELL)
							CPRINTLN(DEBUG_NET_MAGNATE, "[RE] [GB_MAINTAIN_FAIL_CALLS] GB_SET_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_NOTIFICATION_PASSED_FIRST_SELL)") 
						ENDIF
						
						IF bLAND_DAMAGE()
							sGangBossManageRewardsData.iExtraCash = serverBD.iBonus
							sGangBossManageRewardsData.bDelayExtraCashReward = TRUE
							PRINTLN("[EXEC1] [CTRB_SELL] HANDLE_REWARDS - iExtraCash = ",sGangBossManageRewardsData.iExtraCash)
							PRINTLN("[EXEC1] [CTRB_SELL] HANDLE_REWARDS - bDelayExtraCashReward = ", GET_STRING_FROM_BOOL(sGangBossManageRewardsData.bDelayExtraCashReward))
						ENDIF
//					ENDIF
				ENDIF
				
				// [NOTE FOR BOBBY] - If only buying special items, specify here and pass to rewards function so that
				//						we only give cash for special items				
				sGangBossManageRewardsData.bSpecialItemsOnly = GET_DOES_BUYER_ONLY_WANT_SPECIAL_ITEMS()
				sGangBossManageRewardsData.iBuyerID = serverBD.iBuyerID
				GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_CONTRABAND_SELL, bIWon, sGangBossManageRewardsData)
				
				IF bIWon
					// Do any extra processing here for if the locla player won, before passing into GANG_BOSS_MANAGE_REWARDS
					// sGangBossManageRewardsData etc.
					GANG_BOSS_MANAGE_WANTED_CLEAN_UP(TRUE, playerId)
					//[GB_CONT_TICK_S] ~a~ ~s~sold a contraband shipment.
					SCRIPT_EVENT_DATA_TICKER_MESSAGE sData
					sData.TickerEvent = TICKER_EVENT_CONTRABAND_SELL_SOLD
					sData.playerID = playerId
					IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
						BROADCAST_TICKER_EVENT(sData, ALL_PLAYERS(FALSE))
					ENDIF
				ELSE
					GB_START_CONTRABAND_SELL_DELAY_TIMER()
					GANG_BOSS_MANAGE_WANTED_CLEAN_UP(TRUE, INVALID_PLAYER_INDEX())
				ENDIF
					
				EXECUTE_COMMON_END_TELEMTRY(bIWon, FALSE)
				
				SET_LOCAL_BIT0(eLOCALBITSET0_CALCULATED_REWARDS)
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_SERVER_BIT1_SET(eSERVERBITSET1_CONTRABAND_REMOVED_AT_END)
			IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
				IF NOT IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), eCLIENTBITSET0_CONTRABAND_ADDED_AT_END)
					IF bIWon
						HANDLE_CONTRABAND_REMOVAL(eREMOVE_WIN)
					ELIF GET_END_REASON() = eENDREASON_TRUCK_DESTROYED
						HANDLE_CONTRABAND_REMOVAL(eREMOVE_LOSS)
					//We failed and not because a truck was destroyed
					ELSE
						PRINTLN("[CTRB_SELL] HANDLE_CONTRABAND_REMOVAL GET_END_REASON() != eENDREASON_TRUCK_DESTROYED")
						MPGlobalsAmbience.sMagnateGangBossData.iContrabandSellEarnings = 0
						SET_CLIENT_BIT0(eCLIENTBITSET0_CONTRABAND_ADDED_AT_END)
					ENDIF
					
					IF eResult = CONTRABAND_TRANSACTION_STATE_SUCCESS
						PRINTLN("[EXEC1] [CTRB_SELL] - HANDLE_REWARDS - Transaction success!")
						MPGlobalsAmbience.sMagnateGangBossData.iContrabandSellEarnings = 0
						SET_CLIENT_BIT0(eCLIENTBITSET0_CONTRABAND_ADDED_AT_END)
					ENDIF
					
					IF eResult = CONTRABAND_TRANSACTION_STATE_FAILED
						PRINTLN("[EXEC1] [CTRB_SELL] - HANDLE_REWARDS - Transaction failed!")
						MPGlobalsAmbience.sMagnateGangBossData.iContrabandSellEarnings = 0
						SET_CLIENT_BIT0(eCLIENTBITSET0_CONTRABAND_ADDED_AT_END)
					ENDIF
					
					IF IS_SERVER_BIT1_SET(eSERVERBITSET1_LAUNCHED_WITH_NO_CONTRABAND)
						PRINTLN("[EXEC1] [CTRB_SELL] - HANDLE_REWARDS - Launched with no contraband!")
						MPGlobalsAmbience.sMagnateGangBossData.iContrabandSellEarnings = 0
						SET_CLIENT_BIT0(eCLIENTBITSET0_CONTRABAND_ADDED_AT_END)
					ENDIF
				ENDIF
			ELSE
				IF GB_GET_LOCAL_PLAYER_GANG_BOSS() = INVALID_PLAYER_INDEX()
					IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
						PRINTLN("[EXEC1] [CTRB_SELL] HANDLE_REWARDS - Boss has quit the session, setting eSERVERBITSET1_CONTRABAND_REMOVED_AT_END as the boss is unable to")
						SET_SERVER_BIT1(eSERVERBITSET1_CONTRABAND_REMOVED_AT_END)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI
				
			IF NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE()
			AND NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_PASSIVE()
			AND NOT GB_SHOULD_HIDE_GANG_BOSS_EVENT(FMMC_TYPE_GB_CONTRABAND_SELL)
				IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_DONE_END_SHARD)
					IF IS_SAFE_FOR_BIG_MESSAGE()
					
						SWITCH GET_END_REASON()
							CASE eENDREASON_NO_BOSS_LEFT
								PRINTLN("[EXEC1] [CTRB_SELL] HANDLE_REWARDS - no boss left on script.")
								CLEAR_HELP()
//										DO_BIG_MESSAGE( eBIGM_GANG_FAIL)
								// Display end shard here.
							BREAK
							CASE eENDREASON_ALL_GOONS_LEFT
								PRINTLN("[EXEC1] [CTRB_SELL] HANDLE_REWARDS - no goons left on script")
								CLEAR_HELP()
								DO_BIG_MESSAGE( eBIGM_GANG_FAIL, GET_END_REASON())
								// Display end shard here.
							BREAK
							CASE eENDREASON_TIME_UP
								PRINTLN("[EXEC1] [CTRB_SELL] HANDLE_REWARDS - mode time expired")
								CLEAR_HELP()
								DO_BIG_MESSAGE( eBIGM_GANG_FAIL, GET_END_REASON())
								// Display end shard here
							BREAK
							CASE eENDREASON_TRUCK_DESTROYED
							CASE eENDREASON_VEHICLE_STUCK
								PRINTLN("[EXEC1] [CTRB_SELL] HANDLE_REWARDS - eENDREASON_TRUCK_DESTROYED ")
								CLEAR_HELP()
								DO_BIG_MESSAGE( eBIGM_GANG_FAIL, GET_END_REASON())
								// Display end shard here
							BREAK
							CASE eENDREASON_WIN_CONDITION_TRIGGERED
								PRINTLN("[EXEC1] [CTRB_SELL] HANDLE_REWARDS - somebody won")
								CLEAR_HELP()
								DO_BIG_MESSAGE( eBIGM_GANG_SUCCESS)
								// Display end shard here
								// IF local player won set bIWon = TRUE here.
							BREAK
						ENDSWITCH
					
						SET_LOCAL_BIT0(eLOCALBITSET0_DONE_END_SHARD)
					ENDIF
				ENDIF
			ELSE
				PRINTLN("[EXEC1] [CTRB_SELL] HANDLE_REWARDS - GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE ")
			ENDIF
		ELSE
			PRINTLN("[EXEC1] [CTRB_SELL] HANDLE_REWARDS - GB_IS_IT_SAFE_TO_DISPLAY_BOSS_MISSION_UI ")
		ENDIF
		
	ENDIF
	
	IF (GB_MAINTAIN_BOSS_END_UI(sEndUI)
	AND IS_SERVER_BIT1_SET(eSERVERBITSET1_CONTRABAND_REMOVED_AT_END))
	
	#IF IS_DEBUG_BUILD
	OR DEBUG_SKIPPED_MISSION()
	#ENDIF
	
		SET_CLIENT_BIT0(eCLIENTBITSET0_COMPLETED_REWARDS)
	ENDIF
	
ENDPROC

PROC SET_COOLDOWN_TIMER(INT iWarehouse)

	IF CHECK_WAREHOUSE_ID(iWarehouse)
		IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_SET_COOLDOWN)
			IF GET_END_REASON() = eENDREASON_WIN_CONDITION_TRIGGERED	
				IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
					IF DOES_LOCAL_PLAYER_OWN_WAREHOUSE(iWarehouse)
						INT iWarehouseIndex = GET_SAVE_SLOT_FOR_WAREHOUSE(iWarehouse) 
						MPGlobalsAmbience.sMagnateGangBossData.iTimeOfLastSuccessfulSellMission[iWarehouseIndex] = GET_CLOUD_TIME_AS_INT()
						SET_MP_INT_CHARACTER_STAT(GET_STAT_FOR_SELL_COOLDOWN(iWarehouseIndex),MPGlobalsAmbience.sMagnateGangBossData.iTimeOfLastSuccessfulSellMission[iWarehouseIndex])
						PRINTLN("[EXEC1] [CTRB_SELL] [GB_LAUNCH] SET_COOLDOWN_TIMER - Set to ",MPGlobalsAmbience.sMagnateGangBossData.iTimeOfLastSuccessfulSellMission[iWarehouseIndex]," iWarehouse = ",iWarehouse, " index = ",iWarehouseIndex)
						SET_LOCAL_BIT0(eLOCALBITSET0_SET_COOLDOWN)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

FUNC VECTOR vLOCATE_DIMENSION(BOOL bPackage = FALSE, BOOL bLAND = FALSE)

	IF bAIR_VARIATION()
		IF bAIR_CLEAR_AREA()
			IF bLAND
				RETURN <<(cfSELL_CRATE_LOC_SIZE_Z*4), (cfSELL_CRATE_LOC_SIZE_Z*4), cfSELL_CRATE_LOC_SIZE_Z*1.5 >>
			ELSE
				RETURN <<(cfSELL_CRATE_LOC_SIZE_Z*1.5), (cfSELL_CRATE_LOC_SIZE_Z*1.5), cfSELL_CRATE_LOC_SIZE_Z*1.5 >>
			ENDIF
		ELSE
			RETURN <<(cfSELL_CRATE_LOC_SIZE_Z/2), (cfSELL_CRATE_LOC_SIZE_Z/2), cfSELL_CRATE_LOC_SIZE_Z*1.5 >>
		ENDIF
	ELIF bMULTIPLE() // See url:bugstar:2812650 - Mission completed although we were not in the visible destination corona
	OR bSTING()
		IF bPackage
			RETURN <<cfSELL_STING_MULTIPLE_SIZE*1.5, cfSELL_STING_MULTIPLE_SIZE*1.5, cfSELL_LOCATE_SIZE>>
		ELSE
			RETURN <<cfSELL_STING_MULTIPLE_SIZE*1.2, cfSELL_STING_MULTIPLE_SIZE*1.2, cfSELL_LOCATE_SIZE>>
			//RETURN <<cfSELL_LOCATE_SIZE*1.2, cfSELL_LOCATE_SIZE*1.2, cfSELL_LOCATE_SIZE>>
		ENDIF
	ENDIF
	
	IF bSEA_VARIATION()
		RETURN <<(cfSELL_LOCATE_SIZE*3), (cfSELL_LOCATE_SIZE*3), cfSELL_LOCATE_SIZE*1.5 >>
	ENDIF
	
	RETURN <<cfSELL_LOCATE_SIZE, cfSELL_LOCATE_SIZE, cfSELL_LOCATE_SIZE>>
ENDFUNC

FUNC FLOAT fGET_ALLOWABLE_HEIGHT()
	INT iBuyerToCheck = GET_BUYER()
	IF GET_CONTRABAND_UNITS_TO_SELL_FROM_BUYER() = 1
		iBuyerToCheck = 3
	ENDIF
	
	SWITCH GET_SHIPMENT_TYPE()
		CASE eSELL_SHIPMENT_TYPE_AIR_ONE_CUBAN800
		CASE eSELL_SHIPMENT_TYPE_AIR_TWO_CUBAN800
		CASE eSELL_SHIPMENT_TYPE_AIR_THREE_CUBAN800
			
			IF iBuyerToCheck = 2 // Medium
			
				RETURN TO_FLOAT(g_sMPTunables.iEXEC_SELL_AIRFLYLOW_MEDIUM_RESTRICTION_HEIGHT) // 60
			
			ELIF iBuyerToCheck = 3 // Easy
			
				RETURN TO_FLOAT(g_sMPTunables.iEXEC_SELL_AIRFLYLOW_EASY_RESTRICTION_HEIGHT) // 70
			
			ELSE // default/Hard
		
				RETURN g_sMPTunables.fexec_sell_airflylow_restriction_height // 50
			ENDIF
		BREAK
			
		CASE eSELL_SHIPMENT_TYPE_AIR_ONE_TITAN
		
			IF iBuyerToCheck = 2 // Medium
			
				RETURN TO_FLOAT(g_sMPTunables.iEXEC_SELL_AIRFLYLOW_MEDIUM_RESTRICTION_HEIGHT_TITAN) // 80
			
			ELIF iBuyerToCheck = 3 // Easy
			
				RETURN TO_FLOAT(g_sMPTunables.iEXEC_SELL_AIRFLYLOW_EASY_RESTRICTION_HEIGHT_TITAN) // 90
			
			ELSE // default/Hard
		
				RETURN TO_FLOAT(g_sMPTunables.iEXEC_SELL_AIRFLYLOW_RESTRICTION_HEIGHT_TITAN) // 70
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN g_sMPTunables.fexec_sell_airflylow_restriction_height // 50
ENDFUNC

#IF IS_DEBUG_BUILD

//╔═════════════════════════════════════════════════════════════════════════════╗
//║				SELL LOGIC														║

FUNC STRING DBG_GET_MISSION_STAGE_NAME(eCLIENT_MISSION_STAGE sStage)
	SWITCH sStage
		CASE eSELL_STAGE_INIT 
			RETURN "eSELL_STAGE_INIT"
		BREAK
		CASE eSELL_STAGE_VEH 
			RETURN "eSELL_STAGE_VEH"
		BREAK
		CASE eSELL_STAGE_SELLING
			RETURN "eSELL_STAGE_SELLING"
		BREAK
		CASE eSELL_STAGE_CLEANUP
			RETURN "eSELL_STAGE_CLEANUP"
		BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

#ENDIF // #IF IS_DEBUG_BUILD

FUNC eCLIENT_MISSION_STAGE GET_LOCAL_CLIENT_MISSION_STAGE()
	RETURN playerBD[PARTICIPANT_ID_TO_INT()].eStage		
ENDFUNC

PROC SET_LOCAL_CLIENT_MISSION_STAGE(eCLIENT_MISSION_STAGE eStage)
	PRINTNL()
	PRINTLN("[EXEC1] [CTRB_SELL] Current_stage ", DBG_GET_MISSION_STAGE_NAME(playerBD[PARTICIPANT_ID_TO_INT()].eStage))
	
	playerBD[PARTICIPANT_ID_TO_INT()].eStage = eStage
	
	PRINTNL()
	PRINTLN("[EXEC1] [CTRB_SELL] SET_LOCAL_CLIENT_MISSION_STAGE >>> ", DBG_GET_MISSION_STAGE_NAME(eStage))
	PRINTNL()
ENDPROC

//╔═════════════════════════════════════════════════════════════════════════════╗
//║				CONTRABAND / VEHICLE											║

FUNC MODEL_NAMES GET_SELL_VEH_MODEL()

	IF bAIR_VARIATION()
	
		#IF IS_DEBUG_BUILD
		IF g_bFakeAirDrop
			RETURN BUZZARD
		ENDIF
		#ENDIF
	
		SWITCH GET_SHIPMENT_TYPE()
			CASE eSELL_SHIPMENT_TYPE_AIR_ONE_CUBAN800
			CASE eSELL_SHIPMENT_TYPE_AIR_TWO_CUBAN800
			CASE eSELL_SHIPMENT_TYPE_AIR_THREE_CUBAN800
				RETURN CUBAN800
				
			CASE eSELL_SHIPMENT_TYPE_AIR_ONE_TITAN
				RETURN TITAN
		ENDSWITCH
	ENDIF
	
	IF bSEA_VARIATION()
		RETURN TUG
	ENDIF
	
	RETURN BRICKADE
ENDFUNC

FUNC ENTITY_INDEX VEH_ENT(INT iVeh)
	ENTITY_INDEX eiVeh 
	IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niVeh[iVeh])
		eiVeh = NET_TO_ENT(serverBD.niVeh[iVeh])
	ENDIF
	
	RETURN eiVeh
ENDFUNC

FUNC VEHICLE_INDEX VEH_ID(INT iVeh)
	VEHICLE_INDEX vehId
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niVeh[iVeh])
		vehId = NET_TO_VEH(serverBD.niVeh[iVeh])
	ENDIF
	
	RETURN vehId
ENDFUNC

FUNC BOOL IS_SELL_VEH_OK(INT iVeh)
	IF DOES_ENTITY_EXIST(VEH_ENT(iVeh))
	AND NOT IS_ENTITY_DEAD(VEH_ENT(iVeh))
		IF IS_VEHICLE_DRIVEABLE(VEH_ID(iVeh))
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//FUNC VECTOR vGET_VEH_START( INT iStartPos)
//	
//	IF bAIR_VARIATION()
//		SWITCH serverBD.eShipmentType
//			CASE eSELL_SHIPMENT_TYPE_AIR_ONE_CUBAN800
//			CASE eSELL_SHIPMENT_TYPE_AIR_TWO_CUBAN800
//				SWITCH iStartPos
//					CASE 0	RETURN <<-968.1948, -2889.6753, 12.9599>>
//					CASE 1	RETURN <<-960.1045, -3067.6252, 12.9444>>
//					CASE 2	RETURN <<-1067.5291, -3154.7375, 13.0472>>
//					CASE 3	RETURN <<-930.1005, -3121.0369, 12.9444>>
//					CASE 4	RETURN <<-1086.1022, -2977.5989, 12.9449>>
//					CASE 5	RETURN <<-1708.2968, -2959.5718, 12.9448>>
//					CASE 6	RETURN <<-1227.5731, -2346.8250, 12.9451>>
//					CASE 7	RETURN <<-1101.4456, -2400.5569, 12.9451>>
//					CASE 8	RETURN <<-1025.1583, -2429.9004, 12.9445>>
//					CASE 9	RETURN <<-979.2667, -2373.1401, 12.9445>>
//					CASE 10	RETURN <<-1070.7360, -2478.9067, 12.9446>>
//					CASE 11	RETURN <<-1297.4449, -2437.8901, 12.9451>>
//					CASE 12	RETURN <<-1291.2418, -2303.7087, 13.0883>>
//					CASE 13	RETURN <<-1097.9617, -3434.0132, 12.9451>>
//					CASE 14	RETURN <<-1114.1782, -2966.9878, 12.9448>>
//					CASE 15	RETURN <<-1730.1891, -2946.0002, 12.9443>>
//					CASE 16	RETURN <<-1677.4768, -3094.1240, 12.9447>>
//					CASE 17	RETURN <<-1412.5236, -3248.3083, 12.9449>>
//					CASE 18	RETURN <<-1514.5017, -3199.3088, 12.9449>>
//					CASE 19	RETURN <<-1602.3079, -3134.2810, 12.9449>>
//				ENDSWITCH
//			BREAK
//			
//			CASE eSELL_SHIPMENT_TYPE_AIR_ONE_TITAN
//			
//			BREAK
//		ENDSWITCH
//		
//	ENDIF
//	
//	RETURN <<-1193.2894, -2245.7986, 12.9446>>
//ENDFUNC
//
//FUNC FLOAT fGET_VEH_HEAD( INT iStartPos)
//	
//	IF bAIR_VARIATION()
//		IF bAIR_VARIATION()
//		SWITCH serverBD.eShipmentType
//			CASE eSELL_SHIPMENT_TYPE_AIR_ONE_CUBAN800
//			CASE eSELL_SHIPMENT_TYPE_AIR_TWO_CUBAN800
//				SWITCH iStartPos
//					CASE 0	RETURN 103.3978
//					CASE 1	RETURN 125.9975
//					CASE 2	RETURN 91.5969
//					CASE 3	RETURN 132.3964
//					CASE 4	RETURN 191.7962
//					CASE 5	RETURN 15.1962
//					CASE 6	RETURN 290.1954
//					CASE 7	RETURN 100.9950
//					CASE 8	RETURN 48.3939
//					CASE 9	RETURN 104.9941
//					CASE 10	RETURN 12.7939
//					CASE 11	RETURN 109.3935
//					CASE 12	RETURN 63.1932
//					CASE 13	RETURN 296.9930
//					CASE 14	RETURN 130.9911
//					CASE 15	RETURN 306.3914
//					CASE 16	RETURN 9.3913
//					CASE 17	RETURN 307.7906
//					CASE 18	RETURN 305.5890
//					CASE 19	RETURN 330.3874
//				ENDSWITCH
//			BREAK
//			
//			CASE eSELL_SHIPMENT_TYPE_AIR_ONE_TITAN
//			
//			BREAK
//		ENDSWITCH
//	ENDIF
//	
//	RETURN 114.2482
//ENDFUNC

FUNC VECTOR GET_VECTOR_INFRONT_OF_COORDS_WITH_HEADING(VECTOR vStart, FLOAT fHeading, FLOAT fDistance = 2.0)

    VECTOR vEnd 
    
    //end vector - pointing north 
    vEnd    = <<0.0, 1.0, 0.0>> 
    
    //point it in the same direction as the entity
    ROTATE_VECTOR_ABOUT_Z(vEnd, fHeading ) 

    //Make the normilised roted vector 300 times larger 
	
    vEnd.x *= fDistance 
    vEnd.y *= fDistance 
    vEnd.z *= fDistance 
    
    //add it on to the start vector to get the end vector coordinates 
    vEnd += vStart 
    
    RETURN vEnd 

ENDFUNC

PROC INIT_IDEAL_SPAWN_COORDS()
	INT i
	REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
		IF bLAND_VARIATION()
			serverBD.vSellVehSpawnCoords[i] =  GET_LAND_SELL_VEHICLE_CREATION_POS(i)
			serverBD.fSellVehSpawnHeading[i] = GET_LAND_SELL_VEHICLE_CREATION_HEADING(i)
		ELIF bAIR_VARIATION()
			serverBD.vSellVehSpawnCoords[i] =  GET_AIR_SELL_VEHICLE_CREATION_POS(i)
			serverBD.fSellVehSpawnHeading[i] = GET_AIR_SELL_VEHICLE_CREATION_HEADING(i)
		ELSE
			serverBD.vSellVehSpawnCoords[i] =  GET_SEA_SELL_VEHICLE_CREATION_POS()
			serverBD.fSellVehSpawnHeading[i] = GET_SEA_SELL_VEHICLE_CREATION_HEADING()
		ENDIF
		
		PRINTLN("[EXEC1] [CTRB_SELL] CREATE_SELL_VEH preferred serverBD.vSellVehSpawnCoords[",i, "] = ", serverBD.vSellVehSpawnCoords[i]) 
		PRINTLN("[EXEC1] [CTRB_SELL] CREATE_SELL_VEH preferred serverBD.fSellVehSpawnHeading[",i, "] = ", serverBD.fSellVehSpawnHeading[i])
		
		IF NOT IS_POINT_OK_FOR_NET_ENTITY_CREATION(serverBD.vSellVehSpawnCoords[i],4,1,1,1,FALSE,FALSE,FALSE)
			PRINTLN("[EXEC1] [CTRB_SELL] CREATE_SELL_VEH VEH [", i, "] point not ok will try clear area")
			CLEAR_AREA(serverBD.vSellVehSpawnCoords[i], 4.0, TRUE, DEFAULT, DEFAULT, TRUE)
		ENDIF
	ENDREPEAT
ENDPROC



FUNC BOOL GET_IDEAL_SPAWN_POS_FOR_SELL_VEHICLE(INT iVeh, VECTOR &vIdeal, FLOAT &fIdeal)
	
	

	IF bLAND_VARIATION()
		//-- Before creating Land sell vehicles, try to figure out if any of the 3 groups of possible spawn locations for the warehouse are actually going to be valid
		//-- Do this by doing one iteration of HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS and checking the result
		//-- If any of the spawn locations looks like it'll be valid, go with that one. 
		//-- Otherwise move onto the next group of spawn coords and try those instead
		//-- If none of them look like they'll work, fallback to the first one and let the closest car node routines in HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS
		//-- figure out a location
	
		IF iVeh = 0																		
		OR (iVeh > 0 AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[iVeh - 1])) //-- 1 vehicle at a time
		
			IF NOT IS_VECTOR_ZERO(vIdeal)
				RETURN TRUE
			ENDIF
		
			VEHICLE_SPAWN_LOCATION_PARAMS Params
			Params.fMinDistFromCoords = 0.0 
			Params.bConsiderHighways= FALSE
			Params.fMaxDistance= 200.0
			Params.bAvoidSpawningInExclusionZones= TRUE
			Params.bConsiderOnlyActiveNodes= FALSE
			Params.bCheckEntityArea = TRUE
			Params.bUseExactCoordsIfPossible = TRUE
			VECTOR vSpawnHeading
			VECTOR vLocToTry = GET_LAND_SELL_VEHICLE_CREATION_POS(iVeh, serverBD.iFindIdealSpawnCoordProg)
			FLOAT fHeading = GET_LAND_SELL_VEHICLE_CREATION_HEADING(iVeh, serverBD.iFindIdealSpawnCoordProg)
			
			vSpawnHeading = GET_VECTOR_INFRONT_OF_COORDS_WITH_HEADING(vLocToTry, fHeading, 20.0)
			IF HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(vLocToTry, 
															vSpawnHeading, 
															GET_SELL_VEH_MODEL(), 
															FALSE, 
															vLocToTry, 
															fHeading, 
															Params)
				vIdeal = vLocToTry
				fIdeal = fHeading
				
				serverBD.vSellVehSpawnCoords[iVeh] = vIdeal
				serverBD.fSellVehSpawnHeading[iVeh] = fIdeal
				
				PRINTLN("[EXEC1] [CTRB_SELL] [CREATE_SELL_VEH] [GET_IDEAL_SPAWN_POS_FOR_SELL_VEHICLE] Got ideal coords for veh ", iVeh, " - using spawn group ", serverBD.iFindIdealSpawnCoordProg, " Ideal pos = ", vIdeal, " Heading = ", fIdeal, " Time - ",GET_CLOUD_TIME_AS_INT())  
				RETURN TRUE
			ELSE
				BOOL bResetSpawning
				
				//-- Reset the spawning data if there's been more than 1 attempt
				IF g_SpawnData.iSpawnVehicleServerRequestCount > 1
					IF g_SpawnData.iSpawnVehicleState > SPAWN_VEHICLE_SEND_REQUEST
						bResetSpawning = TRUE
						PRINTLN("[EXEC1] [CTRB_SELL] [CREATE_SELL_VEH] [GET_IDEAL_SPAWN_POS_FOR_SELL_VEHICLE] .......... Resetting spawn as g_SpawnData.iSpawnVehicleServerRequestCount = ", g_SpawnData.iSpawnVehicleServerRequestCount)
					ENDIF
				ENDIF
				
				//-- Or if the IS_POINT_OK_FOR_NET_ENTITY_CREATION in the initial stage of HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS has failed, and it's using
				//-- the nearest car node instead.
				IF NOT IS_VECTOR_ZERO(g_SpawnData.vRequestedSpawnVehicleCoords)
					IF g_SpawnData.iSpawnVehicleState > SPAWN_VEHICLE_SEND_REQUEST
						IF NOT ARE_VECTORS_EQUAL(g_SpawnData.vRequestedSpawnVehicleCoords, vLocToTry)
							bResetSpawning = TRUE
							PRINTLN("[EXEC1] [CTRB_SELL] [CREATE_SELL_VEH] [GET_IDEAL_SPAWN_POS_FOR_SELL_VEHICLE] .......... Resetting spawn as ARE_VECTORS_EQUAL")
						ENDIF
					ENDIF
				ENDIF
				
				IF bResetSpawning
					g_SpawnData.iSpawnVehicleState = SPAWN_VEHICLE_INIT
					Cleanup_Entity_Area_For_Spawning_Vehicle()
					CLEAR_CUSTOM_VEHICLE_NODES()
					
					serverBD.iFindIdealSpawnCoordProg++
					
					IF serverBD.iFindIdealSpawnCoordProg > 2
						//-- There's 3 groups of spawn coords per warehouse. If we've tried them all and they're all blocked, fall back to the first set and go with that
						vIdeal = GET_LAND_SELL_VEHICLE_CREATION_POS(iVeh, 0)
						fIdeal = GET_LAND_SELL_VEHICLE_CREATION_HEADING(iVeh, 0)
						
						serverBD.vSellVehSpawnCoords[iVeh] = vIdeal
						serverBD.fSellVehSpawnHeading[iVeh] = fIdeal
						
						PRINTLN("[EXEC1] [CTRB_SELL] [CREATE_SELL_VEH] [GET_IDEAL_SPAWN_POS_FOR_SELL_VEHICLE] Got ideal coords for veh ", iVeh, " but failed to find a suitable spawn group, using 0", " Ideal pos = ", vIdeal, " Heading = ", fIdeal, " Time - ", GET_CLOUD_TIME_AS_INT())
						RETURN TRUE
					ELSE
						PRINTLN("[EXEC1] [CTRB_SELL] [CREATE_SELL_VEH] [GET_IDEAL_SPAWN_POS_FOR_SELL_VEHICLE] .......... Failed to find ideal spawn pos for veh ", iVeh, ", will try spawn group ", serverBD.iFindIdealSpawnCoordProg) 
					ENDIF
				ENDIF
				
		
			ENDIF
		ENDIF
	ELIF bAIR_VARIATION()
		vIdeal = GET_AIR_SELL_VEHICLE_CREATION_POS(iVeh)
		fIdeal = GET_AIR_SELL_VEHICLE_CREATION_HEADING(iVeh)
		RETURN TRUE
	ELSE
		vIdeal = GET_SEA_SELL_VEHICLE_CREATION_POS()
		fIdeal = GET_SEA_SELL_VEHICLE_CREATION_HEADING()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CREATE_HELI_FOR_TITAN()
	VECTOR vSpawnCoord
	FLOAT fSpawnHeading
	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niBuzzardForTitan)
		IF REQUEST_LOAD_MODEL(BUZZARD)
			vSpawnCoord = GET_COORDS_FOR_BUZZARD_NEAR_TITAN()
			fSpawnHeading = GET_HEADING_FOR_BUZZARD_NEAR_TITAN()
			IF CREATE_NET_VEHICLE(serverBD.niBuzzardForTitan, BUZZARD, vSpawnCoord, fSpawnHeading)
				NETWORK_FADE_IN_ENTITY(NET_TO_VEH(serverBD.niBuzzardForTitan), TRUE)
				
				PRINTLN("[EXEC1] [CTRB_SELL] [CREATE_HELI_FOR_TITAN], Buzzard created; vPos: ", vSpawnCoord, "; fHeadingVeh: ", fSpawnHeading, " Time - ", GET_CLOUD_TIME_AS_INT())
			
				
				//Add decorators
				IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
					INT iDecoratorValue
					IF DECOR_EXIST_ON(NET_TO_VEH(serverBD.niBuzzardForTitan), "MPBitset")
						iDecoratorValue = DECOR_GET_INT(NET_TO_VEH(serverBD.niBuzzardForTitan), "MPBitset")
					ENDIF
					SET_BIT(iDecoratorValue, MP_DECORATOR_BS_NON_PASSIVE_MODE_VEHICLE)
					SET_BIT(iDecoratorValue, MP_DECORATOR_BS_NON_MODDABLE_VEHICLE)
					DECOR_SET_INT(NET_TO_VEH(serverBD.niBuzzardForTitan), "MPBitset", iDecoratorValue)
				ENDIF
				
				SET_VEHICLE_AUTOMATICALLY_ATTACHES(NET_TO_VEH(serverBD.niBuzzardForTitan), FALSE)						
				SET_VEHICLE_DOORS_LOCKED_FOR_NON_SCRIPT_PLAYERS(NET_TO_VEH(serverBD.niBuzzardForTitan), TRUE)
				SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(NET_TO_VEH(serverBD.niBuzzardForTitan), TRUE)
				SET_VEHICLE_CAN_BE_USED_BY_FLEEING_PEDS(NET_TO_VEH(serverBD.niBuzzardForTitan), FALSE)
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CREATE_SELL_VEH(INT iVeh)
	
//	VECTOR vIdealPosVeh
//	FLOAT fIdealHeadingVeh
	

	
	
	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[iVeh])

		IF GET_IDEAL_SPAWN_POS_FOR_SELL_VEHICLE(iVeh, serverBD.vIdealPosVeh, serverBD.fIdealHeadingVeh)
			PRINTLN("[EXEC1] [CTRB_SELL] [CREATE_SELL_VEH] Got ideal spawn coords for veh ", iVeh, " Ideal pos = ", serverBD.vIdealPosVeh, " Heading = ", serverBD.fIdealHeadingVeh)
			
			IF bSEA_VARIATION()
				serverBD.vSellVehSpawnCoords[iVeh] = serverBD.vIdealPosVeh
				serverBD.fSellVehSpawnHeading[iVeh] = serverBD.fIdealHeadingVeh
				SET_BIT(serverBD.iSellVehicleSpawnBitset, iVeh)
				PRINTLN("[EXEC1] [CTRB_SELL] [CREATE_SELL_VEH] - Sea Variation - using set coords")
			ELSE
				IF NOT ARE_VECTORS_EQUAL(serverBD.vSellVehSpawnCoords[iVeh], << 0.0, 0.0, 0.0>>)
					IF iVeh = 0																		//-- Only find spawn coords if this is the first vehicle
					OR (iVeh > 0 AND IS_BIT_SET(serverBD.iSellVehicleSpawnBitset, iVeh - 1)) 		//-- or if all previous vehicles have spawn coords
						IF NOT IS_BIT_SET(serverBD.iSellVehicleSpawnBitset, iVeh)
							IF REQUEST_LOAD_MODEL(GET_SELL_VEH_MODEL())
								VEHICLE_SPAWN_LOCATION_PARAMS Params
								Params.fMinDistFromCoords = 0.0 
								Params.bConsiderHighways= FALSE
								Params.fMaxDistance= 200.0

								Params.bConsiderOnlyActiveNodes= FALSE
								IF NOT bAIR_VARIATION()
									Params.bAvoidSpawningInExclusionZones= TRUE
								ELSE	
									Params.bAvoidSpawningInExclusionZones= FALSE
								ENDIF
								Params.bCheckEntityArea = TRUE
								Params.bUseExactCoordsIfPossible = TRUE
								VECTOR vSpawnHeading
								vSpawnHeading = GET_VECTOR_INFRONT_OF_COORDS_WITH_HEADING(serverBD.vIdealPosVeh, serverBD.fIdealHeadingVeh, 20.0)
								

						//		PRINTLN("[EXEC1] [CTRB_SELL] [spawning] Called with vIdealPosVeh = ",vIdealPosVeh, " serverBD.vSellVehSpawnCoords[", iVeh, "] = ", serverBD.vSellVehSpawnCoords[iVeh])
								IF HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(serverBD.vIdealPosVeh, 
																				vSpawnHeading, 
																				GET_SELL_VEH_MODEL(), 
																				FALSE, 
																				serverBD.vSellVehSpawnCoords[iVeh], 
																				serverBD.fSellVehSpawnHeading[iVeh], 
																				Params)
																				

									
									SET_BIT(serverBD.iSellVehicleSpawnBitset, iVeh)
									PRINTLN("[EXEC1] [CTRB_SELL] [CREATE_SELL_VEH], vehicle ", iVeh, " using coord ", serverBD.vSellVehSpawnCoords[iVeh], " and heading ", serverBD.fSellVehSpawnHeading[iVeh])
									PRINTLN("[EXEC1] [CTRB_SELL] [CREATE_SELL_VEH], vehicle ", iVeh, " vIdealPosVeh = ", serverBD.vIdealPosVeh, " fIdealHeadingVeh = ", serverBD.fIdealHeadingVeh)
									
								ELSE
								//	PRINTLN("[EXEC1] [CTRB_SELL] CREATE_SELL_VEH, waiting for HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS, vehicle ", iVeh)
	//								
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		

		IF REQUEST_LOAD_MODEL(GET_SELL_VEH_MODEL())
			IF CAN_REGISTER_MISSION_VEHICLES(GB_GET_BOSS_MISSION_NUM_VEH_REQUIRED(FMMC_TYPE_GB_CONTRABAND_SELL))
				IF IS_BIT_SET(serverBD.iSellVehicleSpawnBitset, iVeh)
					IF CREATE_NET_VEHICLE(serverBD.niVeh[iVeh], GET_SELL_VEH_MODEL(), serverBD.vSellVehSpawnCoords[iVeh], serverBD.fSellVehSpawnHeading[iVeh])
						NETWORK_FADE_IN_ENTITY(NET_TO_VEH(serverBD.niVeh[iVeh]), TRUE)
						
						PRINTLN("[EXEC1] [CTRB_SELL] [CREATE_SELL_VEH], vehicle ", iVeh, " created; vPos: ", serverBD.vSellVehSpawnCoords[iVeh], "; fHeadingVeh: ", serverBD.fSellVehSpawnHeading[iVeh], " Time - ", GET_CLOUD_TIME_AS_INT())
					
						CLEAR_AREA_OF_VEHICLES(serverBD.vSellVehSpawnCoords[iVeh], 20.00)
						
						//Add decorators
						IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
							INT iDecoratorValue
							IF DECOR_EXIST_ON(VEH_ID(iVeh), "MPBitset")
								iDecoratorValue = DECOR_GET_INT(VEH_ID(iVeh), "MPBitset")
							ENDIF
							SET_BIT(iDecoratorValue, MP_DECORATOR_BS_NON_PASSIVE_MODE_VEHICLE)
							SET_BIT(iDecoratorValue, MP_DECORATOR_BS_NON_MODDABLE_VEHICLE)
							DECOR_SET_INT(VEH_ID(iVeh), "MPBitset", iDecoratorValue)
						ENDIF
						
						// Delay setting this vehicle as containing contraband if the buyer is buying 20% or 50%
						IF NOT SHOULD_DELAY_CONTRABAND_BLIP_FOR_BUYER()
							GB_SET_VEHICLE_AS_CONTRABAND(VEH_ID(iVeh),iVeh)
						ENDIF
						
						IF bSEA_VARIATION()
							PRINTLN("[EXEC1] [CTRB_SELL] - CREATE_SELL_VEH - Vehicle is a Tug")
							SET_BOAT_SINKS_WHEN_WRECKED(VEH_ID(iVeh), TRUE)
							SET_BOAT_ANCHOR(VEH_ID(iVeh), TRUE)
							SET_ENTITY_MAX_HEALTH(VEH_ID(iVeh), g_sMPTunables.iexec_sell_tug_health)
							SET_ENTITY_HEALTH(VEH_ID(iVeh), g_sMPTunables.iexec_sell_tug_health)
							SET_VEHICLE_CAN_LEAK_PETROL(VEH_ID(iVeh), FALSE)
							SET_VEHICLE_CAN_LEAK_OIL(VEH_ID(iVeh), FALSE)
							SET_DISABLE_VEHICLE_PETROL_TANK_FIRES(VEH_ID(iVeh), TRUE)
						
							IF HAS_PLAYER_UNLOCKED_INCREASED_SPEED_FOR_VARIATION()
								MODIFY_VEHICLE_TOP_SPEED(VEH_ID(iVeh), GET_VEHICLE_SPEED_MODIFIER_FOR_VARIATION())
								PRINTLN("[EXEC1] [CTRB_SELL] [VEHICLE UPGRADE], CREATE_SELL_VEH - Increased Tug speed has been purchased, modifying top speed by ", GET_VEHICLE_SPEED_MODIFIER_FOR_VARIATION(), "%")
							ELSE
								// Give Tug boat increased speed by default
								IF bSEA_ATTACK()
									MODIFY_VEHICLE_TOP_SPEED(VEH_ID(iVeh), g_sMPTunables.fexec1_sell_seattacked_tug_speed)
									PRINTLN("[EXEC1] [CTRB_SELL] [VEHICLE UPGRADE], CREATE_SELL_VEH - Increased Tug speed has not been purchased, modifying top speed by 25%")
								ELIF bSEA_DEFEND()
									MODIFY_VEHICLE_TOP_SPEED(VEH_ID(iVeh), g_sMPTunables.fexec_sell_seadefend_tug_speed)
									PRINTLN("[EXEC1] [CTRB_SELL] [VEHICLE UPGRADE], CREATE_SELL_VEH - Increased Tug speed has not been purchased, modifying top speed by 25%")
								ENDIF
							ENDIF
							
						ELIF bAIR_VARIATION()
							IF GET_SELL_VEH_MODEL() = CUBAN800
								PRINTLN("[EXEC1] [CTRB_SELL] - CREATE_SELL_VEH - Vehicle is a Cuban800")
								SET_VEHICLE_COLOURS(VEH_ID(iVeh), 132, 64)
								SET_VEHICLE_EXTRA_COLOURS(VEH_ID(iVeh), 111, 156)
							ELSE
								PRINTLN("[EXEC1] [CTRB_SELL] - CREATE_SELL_VEH - Vehicle is a Titan")
								SET_VEHICLE_COLOURS(VEH_ID(iVeh), 7, 7)
								SET_VEHICLE_EXTRA_COLOURS(VEH_ID(iVeh), 7, 7)
							ENDIF
							SET_ENTITY_MAX_HEALTH(VEH_ID(iVeh), g_sMPTunables.iexec_sell_plane_health)
							SET_ENTITY_HEALTH(VEH_ID(iVeh), g_sMPTunables.iexec_sell_plane_health)
							
							PRINTLN("[EXEC1] [CTRB_SELL] [VEHICLE UPGRADE], CREATE_SELL_VEH - MAKING SURE PLANE IS RESISTANT ENOUGH")
							SET_VEHICLE_CAN_BREAK(VEH_ID(iVeh), FALSE)
													
							BOOL bHasUpgrade = HAS_PLAYER_UNLOCKED_INCREASED_RADAR_JAMMER_DURATION_FOR_VARIATION()
							serverBD.iRadarJamDuration = GET_RADAR_JAM_DURATION(bHasUpgrade)
							serverBD.iRadarJamRechargeDelay = GET_RADAR_JAM_RECHARGE_DELAY(bHasUpgrade)
							serverBD.iRadarJamRechargeTime = GET_RADAR_JAM_RECHARGE_TIME(bHasUpgrade)
							PRINTLN("[EXEC1] [CTRB_SELL] [VEHICLE UPGRADE], CREATE_SELL_VEH - Has upgraded Radar jammer been unlocked: ", GET_STRING_FROM_BOOL(bHasUpgrade))
							PRINTLN("[EXEC1] [CTRB_SELL] [VEHICLE UPGRADE], CREATE_SELL_VEH - serverBD.iRadarJamDuration: ", serverBD.iRadarJamDuration)
							PRINTLN("[EXEC1] [CTRB_SELL] [VEHICLE UPGRADE], CREATE_SELL_VEH - serverBD.iRadarJamRechargeDelay: ", serverBD.iRadarJamRechargeDelay)
							PRINTLN("[EXEC1] [CTRB_SELL] [VEHICLE UPGRADE], CREATE_SELL_VEH - serverBD.iRadarJamRechargeTime: ", serverBD.iRadarJamRechargeTime)
						ELSE 
							PRINTLN("[EXEC1] [CTRB_SELL] - CREATE_SELL_VEH - Vehicle is a Brickade")
							SET_VEHICLE_PETROL_TANK_HEALTH(VEH_ID(iVeh), TO_FLOAT(g_sMPTunables.iexec_sell_brickade_health))
							SET_VEHICLE_BODY_HEALTH(VEH_ID(iVeh), TO_FLOAT(g_sMPTunables.iexec_sell_brickade_health))
							SET_VEHICLE_ENGINE_HEALTH(VEH_ID(iVeh), TO_FLOAT(g_sMPTunables.iexec_sell_brickade_health))
							SET_VEHICLE_COLOURS(VEH_ID(iVeh), 17, 12)
							SET_VEHICLE_EXTRA_COLOURS(VEH_ID(iVeh), 1, 156)	
							
							IF HAS_PLAYER_UNLOCKED_BULLETPROOF_TYRES_FOR_VARIATION()
								PRINTLN("[EXEC1] [CTRB_SELL] - CREATE_SELL_VEH - Player has unlocked bulletproof tyres")
								SET_VEHICLE_TYRES_CAN_BURST(VEH_ID(iVeh), FALSE)
							ENDIF
						ENDIF
						
						SET_VEHICLE_AUTOMATICALLY_ATTACHES(VEH_ID(iVeh), FALSE)						
						SET_VEHICLE_DOORS_LOCKED_FOR_NON_SCRIPT_PLAYERS(VEH_ID(iVeh), TRUE)
						SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(VEH_ID(iVeh), TRUE)
						SET_VEHICLE_CAN_BE_USED_BY_FLEEING_PEDS(VEH_ID(iVeh), FALSE)						
						SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE(VEH_ID(iVeh), FALSE)
						IF !bAIR_VARIATION()
							SET_VEHICLE_STRONG(VEH_ID(iVeh), TRUE)
						ENDIF
						SET_VEHICLE_USES_MP_PLAYER_DAMAGE_MULTIPLIER(VEH_ID(iVeh), FALSE)
						
						IF HAS_PLAYER_UNLOCKED_INCREASED_ARMOR_FOR_VARIATION()
							PRINTLN("[EXEC1] [CTRB_SELL] [VEHICLE UPGRADE], CREATE_SELL_VEH - Increased armour unlocked, setting damage scale to 0.08 (6 rocket hits)")
							SET_VEHICLE_DAMAGE_SCALE(VEH_ID(iVeh), 0.08)
						ELSE
							PRINTLN("[EXEC1] [CTRB_SELL] [VEHICLE UPGRADE], CREATE_SELL_VEH - Increased armour NOT unlocked, setting damage scale to 0.3 (3 rocket hits)")
							SET_VEHICLE_DAMAGE_SCALE(VEH_ID(iVeh), 0.2)
						ENDIF
						
						SET_ENTITY_INVINCIBLE(NET_TO_VEH(serverBD.niVeh[iVeh]), TRUE)
						
						CALL_SET_ALLOW_RAMMING_SOOP_OR_RAMP(VEH_ID(iVeh))
						
						serverBD.vIdealPosVeh = << 0.0, 0.0, 0.0 >>
						serverBD.iFindIdealSpawnCoordProg = 0
						#IF IS_DEBUG_BUILD
						TEXT_LABEL_15 tl15Name = "Veh "
						tl15Name += iVeh
						SET_VEHICLE_NAME_DEBUG(VEH_ID(iVeh), tl15Name)
						#ENDIF
						RETURN TRUE
						//SET_ENTITY_INVINCIBLE(VEH_ID(), TRUE)
					ENDIF
				ENDIF
			ENDIF

		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL bUSE_PLURAL()
	
	IF iDROP_TARGET() = 1
	OR bAIR_CLEAR_AREA()
	OR bNORMAL()
	OR bLAND_DEFEND()
		RETURN FALSE
	ENDIF
	
	RETURN ( iDROPPED_OFF_COUNT() < (iDROP_TARGET() - 1) )
ENDFUNC

FUNC STRING GET_OPENING_SELL_MISSION_CALL()
	BOOL bFemale = !IS_OFFICE_PA_MALE(TRUE)
	INT iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
	
	IF bLAND_VARIATION()
		IF bSTING()
		OR bMULTIPLE()
			IF bFemale
				SWITCH iRand
					CASE 0 RETURN "EXCAL_TRU2F"
					CASE 1 RETURN "EXCAL_TRU2F2"
					DEFAULT RETURN "EXCAL_TRU2F3"
				ENDSWITCH
			ELSE
				SWITCH iRand
					CASE 0 RETURN "EXCAL_TRU2M"
					CASE 1 RETURN "EXCAL_TRU2M2"
					DEFAULT RETURN "EXCAL_TRU2M3"
				ENDSWITCH
			ENDIF
		ELSE
			//Sell Contraband - Truck multiple drops
			IF DOES_SHIPMENT_TYPE_HAVE_MULTIPLE_VEHICLES()
				IF bFemale
					SWITCH iRand
						CASE 0 RETURN "EXCAL_SGENF"
						CASE 1 RETURN "EXCAL_SGENF2"
						DEFAULT RETURN "EXCAL_SGENF3"
					ENDSWITCH
				ELSE
					SWITCH iRand
						CASE 0 RETURN "EXCAL_SGENM"
						CASE 1 RETURN "EXCAL_SGENM2"
						DEFAULT RETURN "EXCAL_SGENM3"
					ENDSWITCH
				ENDIF

			//Sell Contraband - Truck normal
			ELSE
				IF bFemale
					SWITCH iRand
						CASE 0 RETURN "EXCAL_TRU1F"
						CASE 1 RETURN "EXCAL_TRU1F2"
						DEFAULT RETURN "EXCAL_TRU1F3"
					ENDSWITCH
				ELSE
					SWITCH iRand
						CASE 0 RETURN "EXCAL_TRU1M"
						CASE 1 RETURN "EXCAL_TRU1M2"
						DEFAULT RETURN "EXCAL_TRU1M3"
					ENDSWITCH
				ENDIF
			ENDIF
		ENDIF
	
	ELIF bAIR_VARIATION() 
		IF bAIR_CLEAR_AREA()
			IF GET_SHIPMENT_TYPE() = eSELL_SHIPMENT_TYPE_AIR_THREE_CUBAN800
				IF bFemale
					SWITCH iRand
						CASE 0 RETURN "EXCAL_VAGUF"
						CASE 1 RETURN "EXCAL_VAGUF2"
						DEFAULT RETURN "EXCAL_VAGUF3"
					ENDSWITCH
				ELSE
					SWITCH iRand
						CASE 0 RETURN "EXCAL_VAGUM"
						CASE 1 RETURN "EXCAL_VAGUM2"
						DEFAULT RETURN "EXCAL_VAGUM3"
					ENDSWITCH
				ENDIF
			ELIF GET_SHIPMENT_TYPE() =  eSELL_SHIPMENT_TYPE_AIR_TWO_CUBAN800
				IF bFemale
					SWITCH iRand
						CASE 0 RETURN "EXCAL_PLAN2F"
						CASE 1 RETURN "EXCAL_PLA2F2"
						DEFAULT RETURN "EXCAL_PLA2F3"
					ENDSWITCH
				ELSE
					SWITCH iRand
						CASE 0 RETURN "EXCAL_PLAN2M"
						CASE 1 RETURN "EXCAL_PLA2M2"
						DEFAULT RETURN "EXCAL_PLA2M3"
					ENDSWITCH
				ENDIF
			ELSE
				IF bFemale
					SWITCH iRand
						CASE 0 RETURN "EXCAL_PLAN1F"
						CASE 1 RETURN "EXCAL_PLA1F2"
						DEFAULT RETURN "EXCAL_PLA1F3"
					ENDSWITCH
				ELSE
					SWITCH iRand
						CASE 0 RETURN "EXCAL_PLAN1M"
						CASE 1 RETURN "EXCAL_PLA1M2"
						DEFAULT RETURN "EXCAL_PLA1M3"
					ENDSWITCH
				ENDIF
			ENDIF
		ELSE
			IF bFemale
				SWITCH iRand
					CASE 0 RETURN "EXCAL_PLAN3F"
					CASE 1 RETURN "EXCAL_PLA3F2"
					DEFAULT RETURN "EXCAL_PLA3F3"
				ENDSWITCH
			ELSE
				SWITCH iRand
					CASE 0 RETURN "EXCAL_PLAN3M"
					CASE 1 RETURN "EXCAL_PLA3M2"
					DEFAULT RETURN "EXCAL_PLA3M3"
				ENDSWITCH
			ENDIF
			
		ENDIF
	ELSE
		//Sell Contraband - Boat normal. 
		IF bFemale
			SWITCH iRand
				CASE 0 RETURN "EXCAL_BOATF"
				CASE 1 RETURN "EXCAL_BOATF2"
				DEFAULT RETURN "EXCAL_BOATF3"
			ENDSWITCH
		ELSE
			SWITCH iRand
				CASE 0 RETURN "EXCAL_BOATM"
				CASE 1 RETURN "EXCAL_BOATM2"
				DEFAULT RETURN "EXCAL_BOATM3"
			ENDSWITCH
		ENDIF
	ENDIF
	
	RETURN ""
ENDFUNC

structPedsForConversation sSpeech
PROC DO_BOSS_INTRO_PHONECALL()

	
	IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
		IF IS_PLAYER_IN_OFFICE_PROPERTY(PLAYER_ID())
		OR NOT IS_PLAYER_PLAYING(PLAYER_ID())
		OR IS_PLAYER_IN_CUTSCENE(PLAYER_ID())
		OR IS_LOCAL_PLAYER_DOING_HELI_DOCK_CUTSCENE()
			EXIT
		ENDIF
		
		IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_PHONE_CALL_SET_UP)
			IF IS_OFFICE_PA_MALE(TRUE)
				ADD_PED_FOR_DIALOGUE(sSpeech, 3, NULL, "EXECPA_MALE")
			ELSE
				ADD_PED_FOR_DIALOGUE(sSpeech, 2, NULL, "EXECPA_FEMALE")
			ENDIF
			SET_LOCAL_BIT0(eLOCALBITSET0_PHONE_CALL_SET_UP)
		ELIF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_PHONE_TRIGGERED)
			IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE,DEFAULT, FALSE)
				INT phonecallModifiers
				SET_BIT(phonecallModifiers, MP_COMMS_MODIFIER_FORCE_PLAYER_TO_ANSWER)
				IF Request_MP_Comms_Message(sSpeech, GET_OFFICE_PA_CHAR(TRUE), "EXCALAU", GET_OPENING_SELL_MISSION_CALL(), phonecallModifiers) 
					SET_LOCAL_BIT0(eLOCALBITSET0_PHONE_TRIGGERED)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL CREATE_SELL_VEHS()
	IF IS_SERVER_BIT0_SET(eSERVERBITSET0_VEH_MADE)
		RETURN TRUE
	ENDIF

	INT i
	REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
		IF CREATE_SELL_VEH(i)
			// Do anything else to the vehicle here
		ENDIF
	ENDREPEAT
	
	REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
		IF NOT DOES_ENTITY_EXIST(VEH_ID(i))
			RETURN FALSE
		ENDIF
	ENDREPEAT
	
	IF bAIR_VARIATION()
		IF GET_SHIPMENT_TYPE() = eSELL_SHIPMENT_TYPE_AIR_ONE_TITAN
			CREATE_HELI_FOR_TITAN()
		
			IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niBuzzardForTitan)
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	SET_SERVER_BIT0(eSERVERBITSET0_VEH_MADE)
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Deal with (Land) Sell vehicles which have become stuck not long after creation, likely because the spawning failed to find a sensible location 
PROC MAINTAIN_SELL_VEHICLES_STUCK_AFTER_CREATION()
	IF NOT bLAND_VARIATION()
		EXIT
	ENDIF
	
	INT i
	VEHICLE_SPAWN_LOCATION_PARAMS Params
	REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
		IF IS_BIT_SET(serverBD.iVehStuckSinceCreatedBitset, i)
			IF IS_VEHICLE_DRIVEABLE(VEH_ID(i))
				IF serverBD.iSellVehToWarp = -1
				OR serverBD.iSellVehToWarp = i
					IF serverBD.iSellVehToWarp = -1
						serverBD.iSellVehToWarp = i
						serverBD.vWarpStuckLoc = GET_ENTITY_COORDS(VEH_ID(i))
						PRINTLN("[EXEC1] [CTRB_SELL] [MAINTAIN_SELL_VEHICLES_STUCK_AFTER_CREATION] iSellVehToWarp = ", serverBD.iSellVehToWarp, " Current coords = ", serverBD.vWarpStuckLoc)
					ENDIF
					
					Params.fMinDistFromCoords = 10.0
					Params.bConsiderHighways= FALSE
					Params.fMaxDistance= 100.0
					Params.bConsiderOnlyActiveNodes= FALSE
					Params.bAvoidSpawningInExclusionZones= TRUE
					Params.bCheckEntityArea = TRUE //  TRUE
					Params.bUseExactCoordsIfPossible = FALSE
					VECTOR vNewCoord = <<0.0, 0.0, 0.0>>
					FLOAT fNewHeading = 0.0
					MODEL_NAMES model = GET_ENTITY_MODEL(VEH_ID(i))
					IF HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(serverBD.vWarpStuckLoc, 
																		<< 0.0, 0.0, 0.0 >>, 
																		model, 
																		FALSE, 
																		vNewCoord, 
																		fNewHeading, 
																		Params)
						SET_ENTITY_COORDS(VEH_ID(i), vNewCoord)
						SET_ENTITY_HEADING(VEH_ID(i), fNewHeading)
						SET_VEHICLE_ON_GROUND_PROPERLY(VEH_ID(i))
						serverBD.iSellVehToWarp = -1
						CLEAR_BIT(serverBD.iVehStuckSinceCreatedBitset, i)
						PRINTLN("[EXEC1] [CTRB_SELL] [MAINTAIN_SELL_VEHICLES_STUCK_AFTER_CREATION] Sell veh ", i, " warped to ", vNewCoord, " with heading ", fNewHeading)
					ENDIF
				ENDIF
			ELSE
				PRINTLN("[EXEC1] [CTRB_SELL] [MAINTAIN_SELL_VEHICLES_STUCK_AFTER_CREATION] Want to warp veh ", i, " but not driveable!")
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC LOCK_SELL_VEH(INT iVeh, BOOL bLock)
	IF NETWORK_HAS_CONTROL_OF_ENTITY(VEH_ENT(iVeh))
		IF bLock
			IF IS_SELL_VEH_OK(iVeh)
				IF GET_VEHICLE_DOOR_LOCK_STATUS(VEH_ID(iVeh)) <> VEHICLELOCK_LOCKED
					PRINTLN("[CTRB_SELL] 2806496 LOCK_SELL_VEH, VEHICLELOCK_LOCKED")
					SET_VEHICLE_DOORS_LOCKED(VEH_ID(iVeh), VEHICLELOCK_LOCKED)
				ENDIF
			ENDIF
		ELSE
			IF IS_SELL_VEH_OK(iVeh)
				IF GET_VEHICLE_DOOR_LOCK_STATUS(VEH_ID(iVeh)) <> VEHICLELOCK_UNLOCKED
					PRINTLN("[CTRB_SELL] 2806496 LOCK_SELL_VEH, VEHICLELOCK_UNLOCKED")
					SET_VEHICLE_DOORS_LOCKED(VEH_ID(iVeh), VEHICLELOCK_UNLOCKED)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC FIX_SELL_VEH(INT iVeh)
	IF NETWORK_HAS_CONTROL_OF_ENTITY(VEH_ENT(iVeh))
		PRINTLN("[CTRB_SELL] FIX_SELL_VEH")
		SET_VEHICLE_ENGINE_HEALTH(VEH_ID(iVeh), 1000)
	ENDIF
ENDPROC

FUNC BOOL HAS_THIS_SELL_VEH_DELIVERED_CONTRABAND(INT iVeh)
	SWITCH iVeh
		CASE 0	RETURN IS_SERVER_BIT0_SET(eSERVERBITSET0_VEH_0_DELIVERED)
		CASE 1	RETURN IS_SERVER_BIT0_SET(eSERVERBITSET0_VEH_1_DELIVERED)
		CASE 2	RETURN IS_SERVER_BIT0_SET(eSERVERBITSET0_VEH_2_DELIVERED)
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

// Local check
FUNC BOOL IS_PLAYER_IN_SELL_VEH(INT iVeh, BOOL bCheckFOrDelivered = TRUE)
	IF NOT IS_SELL_VEH_OK(iVeh)
		RETURN FALSE
	ENDIF
	
	IF bCheckFOrDelivered
		IF HAS_THIS_SELL_VEH_DELIVERED_CONTRABAND(iVeh)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh[iVeh]))
		RETURN FALSE
	ENDIF
		
	RETURN TRUE
ENDFUNC

// local check
FUNC BOOL IS_PLAYER_IN_ANY_SELL_VEH(BOOL bCheckDelivered = TRUE)
	INT i
	REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
		IF IS_PLAYER_IN_SELL_VEH(i, bCheckDelivered)
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

// Local/Remote Check
FUNC BOOL IS_THIS_PLAYER_IN_SELL_VEH(PLAYER_INDEX playerToCheck, INT iVeh, BOOL bCheckForDelivered = TRUE)
	IF NOT IS_SELL_VEH_OK(iVeh)
		RETURN FALSE
	ENDIF
	
	IF bCheckFOrDelivered
		IF HAS_THIS_SELL_VEH_DELIVERED_CONTRABAND(iVeh)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF NOT IS_PED_IN_VEHICLE(GET_PLAYER_PED(playerToCheck), NET_TO_VEH(serverBD.niVeh[iVeh]))
		RETURN FALSE
	ENDIF
		
	RETURN TRUE
ENDFUNC

// Local/Remote check
FUNC BOOL IS_THIS_PLAYER_IN_ANY_SELL_VEH(PLAYER_INDEX playerToCheck, BOOL bCheckDelivered = TRUE)
	INT i
	REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
		IF IS_THIS_PLAYER_IN_SELL_VEH(playerToCheck, i, bCheckDelivered)
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_MY_SELL_VEH_AS_INT()
	INT i
	REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
		IF IS_PLAYER_IN_SELL_VEH(i)
			RETURN i
		ENDIF
	ENDREPEAT
	
	RETURN -1
ENDFUNC

FUNC BOOL IS_SELL_VEH_OCCUPIED_BY_TEAMMATE(INT iVeh, PED_INDEX pedPlayer)
	IF IS_SELL_VEH_OK(iVeh)
		IF pedPlayer = GET_PED_IN_VEHICLE_SEAT(VEH_ID(iVeh), VS_DRIVER)
		
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_DRIVER_OF_SELL_VEH(INT iVeh, BOOL bCheckFOrDelivered = TRUE)
	IF IS_PLAYER_IN_SELL_VEH(iVeh, bCheckFOrDelivered)
		IF IS_NET_PLAYER_OK(PLAYER_ID(), FALSE)
			IF PLAYER_PED_ID() = GET_PED_IN_VEHICLE_SEAT(VEH_ID(iVeh), VS_DRIVER)
		
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE	
ENDFUNC

FUNC BOOL IS_DRIVER_OF_ANY_SELL_VEH()
	INT i
	REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
		IF IS_DRIVER_OF_SELL_VEH(i)
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

FUNC PLAYER_INDEX GET_DRIVER_OF_SELL_VEH(INT iVeh)

	INT iParticipant
	PARTICIPANT_INDEX participantID
	IF IS_SELL_VEH_OCCUPIED(iVeh)
		REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_NATIVE(PARTICIPANT_INDEX, iParticipant))
				participantID = INT_TO_NATIVE(PARTICIPANT_INDEX, iParticipant)
				PLAYER_INDEX playerID = NETWORK_GET_PLAYER_INDEX(participantID)
				PED_INDEX pedPlayer
				IF IS_NET_PLAYER_OK(playerID, FALSE)
					pedPlayer = GET_PLAYER_PED(playerID)
					IF NOT IS_PED_INJURED(pedPlayer)
						IF IS_PED_IN_ANY_VEHICLE(pedPlayer)
							IF GET_VEHICLE_PED_IS_IN(pedPlayer) = NET_TO_VEH(serverBD.niVeh[iVeh])
								RETURN(playerID)
								//PRINTLN("[EXEC1] [CTRB_SELL] 
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	RETURN INVALID_PLAYER_INDEX()
ENDFUNC

FUNC BOOL IS_PASSENGER(INT iVeh)
	VEHICLE_SEAT seatToCheck
	INT i
	
	IF IS_PLAYER_IN_SELL_VEH(iVeh)
		IF IS_NET_PLAYER_OK(PLAYER_ID(), FALSE)
			REPEAT COUNT_OF(VEHICLE_SEAT) i
				seatToCheck = INT_TO_ENUM(VEHICLE_SEAT, i)
				IF PLAYER_PED_ID() = GET_PED_IN_VEHICLE_SEAT(VEH_ID(iVeh), seatToCheck)
			
					RETURN TRUE
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
	
	RETURN FALSE	
ENDFUNC

FUNC BOOL IS_PASSENGER_OF_ANY_SELL_VEHICLE()
	INT i
	REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
		IF IS_PLAYER_IN_SELL_VEH(i)
			IF IS_PASSENGER(i)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_SELL_VEH_HAVE_PASSENGER(INT iVeh)
	VEHICLE_SEAT seatToCheck
	INT i
	PED_INDEX ped
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[iVeh])
		IF NOT IS_ENTITY_DEAD(VEH_ID(iVeh))
			
			REPEAT COUNT_OF(VEHICLE_SEAT) i
				seatToCheck = INT_TO_ENUM(VEHICLE_SEAT, i)
			 	ped = GET_PED_IN_VEHICLE_SEAT(VEH_ID(iVeh), seatToCheck)
				IF DOES_ENTITY_EXIST(ped)
					RETURN TRUE
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL AM_I_IN_THIS_SELL_VEH(INT iVeh)
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[iVeh])
		IF IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh[iVeh]))
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL AM_I_IN_ANY_SELL_VEH()
	INT iVeh
	REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() iVeh
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[iVeh])
			IF IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh[iVeh]))
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC
FUNC INT GET_SELL_VEH_ARRAY_POS_I_AM_IN()
	INT iVeh
	REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() iVeh
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[iVeh])
			IF IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh[iVeh]))
				RETURN iVeh
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN -1
ENDFUNC
//
//FUNC INT GET_SELL_VEH_I_AM_IN_int()
//	INT iVeh
//	REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() iVeh
//		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[iVeh])
//			IF IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh[iVeh]))
//				RETURN iVeh
//			ENDIF
//		ENDIF
//	ENDREPEAT
//	RETURN 0
//ENDFUNC


FUNC BOOL IS_ANY_SELL_VEH_OCCUPIED()
	INT i
	REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
		IF IS_SELL_VEH_OCCUPIED(i)
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

FUNC BOOL ARE_ALL_SELL_VEHS_OCCUPIED()
	INT i
	REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
		IF NOT HAS_THIS_SELL_VEH_DELIVERED_CONTRABAND(i)
			IF NOT IS_SELL_VEH_OCCUPIED(i)
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT

	RETURN TRUE
ENDFUNC

FUNC VECTOR VEH_POS(INT iVeh, BOOL bCloseBy = FALSE)

	VECTOR vReturn 
	
	IF IS_SELL_VEH_OK(iVeh)
		vReturn = GET_ENTITY_COORDS(VEH_ENT(iVeh))
	ENDIF
	
	IF bCloseBy
		vReturn = vReturn + <<5.0, 5.0, 0.0>>
	ENDIF
	
	RETURN vReturn
ENDFUNC

FUNC FLOAT fVEH_HEADING(INT iVeh)

	FLOAT fReturn
	
	IF IS_SELL_VEH_OK(iVeh)
		fReturn = GET_ENTITY_HEADING(VEH_ENT(iVeh))
	ENDIF
	
	RETURN fReturn
ENDFUNC

FUNC BOOL IS_ANY_SELL_VEH_NEAR_DROP_OFF()
	BOOL bToReturn = FALSE
	
	INT i
	REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
		IF GET_DISTANCE_BETWEEN_COORDS(VEH_POS(i), GET_SELL_DROP_COORDS(GET_SELL_VAR(), serverBD.iClosestDropOff[i], serverBD.iRoute, i)) < 50
			bToReturn = TRUE
		ENDIF
	ENDREPEAT
	
	RETURN bToReturn
ENDFUNC

FUNC VEHICLE_INDEX GET_CLOSEST_SELL_VEH_TO_PED(INT iPed, INT &iVeh)
	INT i
	FLOAT fClosestveh = 9999.99
	FLOAT fTempDist
	VEHICLE_INDEX vehToReturn = INT_TO_NATIVE(VEHICLE_INDEX, -1)

	IF DOES_SHIPMENT_TYPE_HAVE_MULTIPLE_VEHICLES()
		REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
			IF DOES_ENTITY_EXIST(VEH_ID(i))
				fTempDist = GET_DISTANCE_BETWEEN_ENTITIES(NET_TO_PED(serverBD.sPed[iPed].netIndex), VEH_ID(i))
				IF fTempDist < fClosestveh
					fClosestveh = fTempDist
					vehToReturn = VEH_ID(i)
					iVeh = i
				ENDIF
			ENDIF
		ENDREPEAT
	ELSE
		vehToReturn = VEH_ID(0)
		iVeh = 0
	ENDIF
	
	RETURN vehToReturn	
ENDFUNC

FUNC INT GET_CLOSEST_SELL_VEH_TO_LOCAL_PLAYER(BOOL bIncludeOcupied = TRUE)
	INT i
	FLOAT fClosestveh = 9999.99
	FLOAT fTempDist
	INT iVehToReturn = -1

	IF DOES_SHIPMENT_TYPE_HAVE_MULTIPLE_VEHICLES()
		REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
			IF DOES_ENTITY_EXIST(VEH_ID(i))
			AND NOT HAS_THIS_SELL_VEH_DELIVERED_CONTRABAND(i)
				fTempDist = GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), VEH_ID(i))
				IF fTempDist < fClosestveh
					IF bIncludeOcupied
					OR NOT IS_SELL_VEH_OCCUPIED(i)
					OR IS_DRIVER_OF_SELL_VEH(i)
						fClosestveh = fTempDist
						iVehToReturn = i
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ELSE
		iVehToReturn = 0
	ENDIF
	
	RETURN iVehToReturn	
ENDFUNC

// Get the plane height above ground
FUNC FLOAT GET_HEIGHT_ABOVE_GROUND(VEHICLE_INDEX planeVehicle, FLOAT &fGroundHeight)

	FLOAT fHeightAboveGround
	VECTOR vPlayerCoords = GET_ENTITY_COORDS(planeVehicle)
	GET_GROUND_Z_FOR_3D_COORD(vPlayerCoords, fGroundHeight)
	IF fGroundHeight < 0
		fGroundHeight = 0
	ENDIF
	
//	IF IS_ENTITY_IN_AREA(planeVehicle, <<-181.68785, 3545.77539, 62.87735>>, <<2463.76489, 4640.36670, 31.42104>>, FALSE, FALSE)
//		IF fGroundHeight < 30
//			fGroundHeight = 30
//		ENDIF
//	ENDIF
	
	fHeightAboveGround = ( vPlayerCoords.z - fGroundHeight )	
	IF fHeightAboveGround < 0
		fHeightAboveGround *= -1
	ENDIF	
	
	RETURN fHeightAboveGround
ENDFUNC

FUNC BOOL bFLYING_TOO_HIGH()
//	RETURN ( serverBD.fDistAboveGround > fGET_ALLOWABLE_HEIGHT() )
	RETURN (sLocaldata.fMyDistAboveGround > fGET_ALLOWABLE_HEIGHT())
ENDFUNC

PROC DO_RADAR_ALTIMETER()
	INT iVeh
	REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() iVeh
		IF IS_PLAYER_IN_SELL_VEH(iVeh)
			PRINTLN("DO_RADAR_ALTIMETER, sLocaldata.fMyDistGroundHeight = ", sLocaldata.fMyDistGroundHeight, " fGET_ALLOWABLE_HEIGHT() = ", fGET_ALLOWABLE_HEIGHT())
			SET_FAKE_MINIMAP_MAX_ALTIMETER_HEIGHT(sLocaldata.fMyDistGroundHeight + fGET_ALLOWABLE_HEIGHT())	
		ELSE
			IF NOT IS_PLAYER_IN_ANY_SELL_VEH()
				PRINTLN("DO_RADAR_ALTIMETER, 0 ")
				SET_FAKE_MINIMAP_MAX_ALTIMETER_HEIGHT(0)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC


/// PURPOSE:
///    Deal with the player flying the plane too high during the "Air Low" variation
PROC MAINTAIN_PLANE_HEIGHT_CLIENT()
	FLOAT fDist
	FLOAT fGroundHeight
	BOOL bPlayTooHighSound = FALSE
	INT i
	
	IF bAIR_LOW()
		REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
			IF DOES_ENTITY_EXIST(VEH_ID(i))
			AND NOT IS_ENTITY_DEAD(VEH_ID(i))
				IF IS_PLAYER_IN_SELL_VEH(i)
					DO_RADAR_ALTIMETER()
					
					fDist = GET_HEIGHT_ABOVE_GROUND(VEH_ID(i), fGroundHeight)
					
					IF sLocaldata.fMyDistGroundHeight != fGroundHeight 
						sLocaldata.fMyDistGroundHeight = fGroundHeight
					ENDIF
					
					IF sLocaldata.fMyDistAboveGround != fDist
						sLocaldata.fMyDistAboveGround = fDist
					ENDIF
					
					IF sLocaldata.fMyDistAboveGround > fGET_ALLOWABLE_HEIGHT()
						//-- Too high
						
						IF bPlayTooHighSound = FALSE
							bPlayTooHighSound = TRUE
						ENDIF
						
						IF IS_DRIVER_OF_SELL_VEH(i)
							IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
								IF IS_LOCAL_BIT0_SET(eLOCALBITSET0_GIVEN_WANTED_FOR_TOO_HIGH)
									CLEAR_LOCAL_BIT0(eLOCALBITSET0_GIVEN_WANTED_FOR_TOO_HIGH)
									
									IF HAS_NET_TIMER_STARTED(sLocaldata.timeTooHigh)
										RESET_NET_TIMER(sLocaldata.timeTooHigh)
										PRINTLN("[EXEC1] - MAINTAIN_WANTED_RATINGS RESET timeTooHigh as wanted level is 0!")
									ENDIF
								ENDIF
								
								
								
								IF NOT IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), eCLIENTBITSET0_PLANE_TOO_HIGH)
									PRINTLN("[EXEC1] [CTRB_SELL] [MAINTAIN_PLANE_HEIGHT_CLIENT] I'm too high - fMyDistAboveGround = ", sLocaldata.fMyDistAboveGround)
									SET_CLIENT_BIT0(eCLIENTBITSET0_PLANE_TOO_HIGH)
								ENDIF
							ENDIF
						ELSE
							IF IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), eCLIENTBITSET0_PLANE_TOO_HIGH)
								PRINTLN("[EXEC1] [CTRB_SELL] [MAINTAIN_PLANE_HEIGHT_CLIENT] Clear eCLIENTBITSET0_PLANE_TOO_HIGH as not driver ")
								CLEAR_CLIENT_BIT0(eCLIENTBITSET0_PLANE_TOO_HIGH)
							ENDIF
							

						ENDIF
						
					ELSE
						//-- Below max height
						
						IF IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), eCLIENTBITSET0_PLANE_TOO_HIGH)
							PRINTLN("[EXEC1] [CTRB_SELL] [MAINTAIN_PLANE_HEIGHT_CLIENT] Clear eCLIENTBITSET0_PLANE_TOO_HIGH as fMyDistAboveGround = ", sLocaldata.fMyDistAboveGround)
							CLEAR_CLIENT_BIT0(eCLIENTBITSET0_PLANE_TOO_HIGH)
						ENDIF
						
						IF bPlayTooHighSound = TRUE
							bPlayTooHighSound = FALSE
						ENDIF
						
						IF IS_DRIVER_OF_SELL_VEH(i)
							IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_FLY_LOW_HELP)
		 
								IF IS_BIT_SET(MPGlobalsAmbience.iFmGbHelpBitSet, BI_FM_GANG_BOSS_HELP_BLIP_WARN)
									IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE)
									AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SCONT_GOHP")
										PRINT_HELP_NO_SOUND("SCONTRA_HLP13") //Fly as low as possible to avoid detection by the Cops.
										GB_SET_GANG_BOSS_HELP_BACKGROUND()
										SET_LOCAL_BIT0(eLOCALBITSET0_FLY_LOW_HELP)
										PRINTLN("[EXEC1] [CTRB_SELL] [MAINTAIN_PLANE_HEIGHT_CLIENT] Done fly-lo help")
									ENDIF
								ENDIF
								
							ENDIF
						ENDIF
					ENDIF
					
//				ELSE
//					//-- Not in sell vehicle
//					IF IS_LOCAL_BIT0_SET(eLOCALBITSET0_PLAY_ALTITUDE_WARNING_SOUND)
//						STOP_SOUND(sLocaldata.iAltitudeWarningSound)
//						CLEAR_LOCAL_BIT0(eLOCALBITSET0_PLAY_ALTITUDE_WARNING_SOUND)
//						PRINTLN("[EXEC1] [CTRB_SELL] [MAINTAIN_PLANE_HEIGHT_CLIENT] Stop warning as not in vehicle ")
//					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF bPlayTooHighSound
			IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_PLAY_ALTITUDE_WARNING_SOUND)
				//-- Play auido. Only Start playing if we haven't just stopped it playing as otherwise it sounds weird.
				IF NOT HAS_NET_TIMER_STARTED(sLocaldata.timeHeightWarningSTop)
				OR (HAS_NET_TIMER_STARTED(sLocaldata.timeHeightWarningSTop) AND HAS_NET_TIMER_EXPIRED(sLocaldata.timeHeightWarningSTop, 1000))
				//	PLAY_SOUND_FRONTEND(sLocaldata.iAltitudeWarningSound, "Altitude_Warning", "EXILE_1")
					PLAY_SOUND_FRONTEND(sLocaldata.iAltitudeWarningSound, "Altitude_Warning_Loop", "DLC_Exec_Fly_Low_Sounds")
					SET_LOCAL_BIT0(eLOCALBITSET0_PLAY_ALTITUDE_WARNING_SOUND)
					PRINTLN("[EXEC1] [CTRB_SELL] [MAINTAIN_PLANE_HEIGHT_CLIENT] Playing altitude warning audio")
				ENDIF
			ENDIF
		ELSE
			IF IS_LOCAL_BIT0_SET(eLOCALBITSET0_PLAY_ALTITUDE_WARNING_SOUND)
				RESET_NET_TIMER(sLocaldata.timeHeightWarningSTop)
				START_NET_TIMER(sLocaldata.timeHeightWarningSTop)
				STOP_SOUND(sLocaldata.iAltitudeWarningSound)
				CLEAR_LOCAL_BIT0(eLOCALBITSET0_PLAY_ALTITUDE_WARNING_SOUND)
				PRINTLN("[EXEC1] [CTRB_SELL] [MAINTAIN_PLANE_HEIGHT_CLIENT] Stop warning sound as below height ")
			ENDIF
		ENDIF
		
		IF IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), eCLIENTBITSET0_PLANE_TOO_HIGH)
			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SCONTRA_HLP11")
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					PRINT_HELP("SCONTRA_HLP11")
					RESET_NET_TIMER(sLocaldata.timeTooHighHelp)
				ENDIF
			ENDIF
				
			IF NOT HAS_NET_TIMER_STARTED(sLocaldata.timeTooHigh)
				START_NET_TIMER(sLocaldata.timeTooHigh)
				PRINTLN("[EXEC1] [CTRB_SELL] [MAINTAIN_PLANE_HEIGHT_CLIENT] Started timeTooHigh")
			ENDIF
		ELSE
			IF HAS_NET_TIMER_STARTED(sLocaldata.timeTooHigh)
				RESET_NET_TIMER(sLocaldata.timeTooHigh)
				PRINTLN("[EXEC1] [CTRB_SELL] [MAINTAIN_PLANE_HEIGHT_CLIENT] Reset timeTooHigh as eCLIENTBITSET0_PLANE_TOO_HIGH is cleared")
			ENDIF
			
			//-- CLear the warning help, but only if it's been displayed for a minumum amount of time
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SCONTRA_HLP11")
				IF HAS_NET_TIMER_EXPIRED(sLocaldata.timeTooHighHelp, 3000)
					CLEAR_HELP()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SET_CONTRABAND_PLAYER()
	IF GET_PLAYER_RIVAL_ENTITY_TYPE(PLAYER_ID()) != eRIVALENTITYTYPE_CONTRABAND
		IF IS_DRIVER_OF_ANY_SELL_VEH()
			PRINTLN("[EXEC1] [CTRB_SELL] SET_CONTRABAND_PLAYER, SET ")
			SET_PLAYER_RIVAL_ENTITY_TYPE(eRIVALENTITYTYPE_CONTRABAND)
		ENDIF
	ELSE
		IF NOT IS_DRIVER_OF_ANY_SELL_VEH()
			PRINTLN("[EXEC1] [CTRB_SELL] SET_CONTRABAND_PLAYER, CLEAR ")
			SET_PLAYER_RIVAL_ENTITY_TYPE(eRIVALENTITYTYPE_INVALID)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL CAN_COLLECT_MORE_CONTRA()
//	IF DOES_LOCAL_PLAYER_OWN_A_WAREHOUSE()
		IF( GET_COUNT_OF_WAREHOUSES_OWNED() > 1 )
			IF ( GET_PLAYER_CONTRABAND_TOTAL() > 1 )
		
				RETURN TRUE
			ENDIF
		ENDIF
//	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL ENTITY_AT_DROP_OFF_COORDS( VECTOR vDrop, ENTITY_INDEX eiEntity, BOOL bPackage = FALSE)
	IF IS_ENTITY_AT_COORD(eiEntity, vDrop, vLOCATE_DIMENSION(bPackage))
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC HANDLE_VEHICLE_STOPPING()
	INT iVeh

	IF NOT bAIR_DROP()
	AND NOT bAIR_LOW()
	AND NOT bAIR_CLEAR_AREA()
	AND NOT bAIR_RESTRICTED()
	AND NOT bAIR_ATTACK()
	AND NOT bMULTIPLE()
	AND NOT bSTING()
		IF IS_DRIVER_OF_ANY_SELL_VEH()
			iVeh = GET_SELL_VEH_I_AM_IN_INT()
//			IF IS_BIT_SET(serverBD.iActiveDropOffBitset, serverBD.iClosestDropOff)
				IF HAS_THIS_SELL_VEH_DELIVERED_CONTRABAND(iVeh)
					BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(VEH_ID(GET_SELL_VEH_I_AM_IN_INT()))
				ENDIF
//			ENDIF
		ENDIF
	ENDIF
ENDPROC



FUNC eCLIENT_BITSET_0 GET_AIR_CLEAR_LAND_BIT_TO_SET(INT iVeh)
	SWITCH iVeh
		CASE 0	RETURN eCLIENTBITSET0_PLANE_0_LANDED
		CASE 1	RETURN eCLIENTBITSET0_PLANE_1_LANDED
		CASE 2	RETURN eCLIENTBITSET0_PLANE_2_LANDED
	ENDSWITCH
	RETURN eCLIENTBITSET0_PLANE_0_LANDED
ENDFUNC

PROC EJECT_PLAYER_FROM_VEHICLE(INT iVeh, BOOL bIgnoreAirCheck = FALSE)
	PRINTLN("[EXEC1] [CTRB_SELL] EJECT_PLAYER_FROM_VEHICLE Called")
	IF IS_PLAYER_IN_SELL_VEH(iVeh, FALSE)
	AND ( NOT bAIR_VARIATION() OR bIgnoreAirCheck )
	AND NOT bSEA_VARIATION()
		PRINTLN("[EXEC1] [CTRB_SELL] EJECT_PLAYER_FROM_VEHICLE exiting veh ", iVeh)
		TASK_LEAVE_VEHICLE(PLAYER_PED_ID(), VEH_ID(iVeh))
	ENDIF
ENDPROC
PROC HANDLE_LANDING_PLANE_FLAG()
	IF bAIR_CLEAR_AREA()
		PRINTLN("[EXEC1] [CTRB_SELL] HANDLE_LANDING_PLANE_FLAG ")
		INT iVeh
		eCLIENT_BITSET_0 eBit
		REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() iVeh
			eBit = GET_AIR_CLEAR_LAND_BIT_TO_SET(iVeh)
			IF NOT IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), eBit)
				//PRINTLN("[EXEC1] [CTRB_SELL] HANDLE_LANDING_PLANE_FLAG - IS_CLIENT_BIT0_SET ", iVeh)
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[iVeh])
					//PRINTLN("[EXEC1] [CTRB_SELL] HANDLE_LANDING_PLANE_FLAG - NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID ", iVeh)
					IF IS_DRIVER_OF_SELL_VEH(iVeh)
						//PRINTLN("[EXEC1] [CTRB_SELL] HANDLE_LANDING_PLANE_FLAG - IS_DRIVER_OF_SELL_VEH ", iVeh)
						//IF IS_BIT_SET(serverBD.iActiveDropOffBitset[iVeh], serverBD.iClosestDropOff[iVeh])
							IF NOT IS_ENTITY_IN_AIR(VEH_ENT(iVeh))
							AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(VEH_ENT(iVeh), FALSE), GET_SELL_DROP_COORDS(GET_SELL_VAR(), serverBD.iClosestDropOff[iVeh], serverBD.iRoute, iVeh)) < (cfSELL_CRATE_LOC_SIZE_Z/2)
								PRINTLN("[EXEC1] [CTRB_SELL] HANDLE_LANDING_PLANE_FLAG - SET_CLIENT_BIT0 - ", iVeh)
								SET_CLIENT_BIT0(eBit)
							ENDIF
						//ENDIF
					ENDIF
				ENDIF
			ELSE	
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[iVeh])
					//PRINTLN("[EXEC1] [CTRB_SELL] HANDLE_LANDING_PLANE_FLAG - ELSE NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID ", iVeh)
					IF IS_DRIVER_OF_SELL_VEH(iVeh, FALSE)
						//PRINTLN("[EXEC1] [CTRB_SELL] HANDLE_LANDING_PLANE_FLAG - IS_DRIVER_OF_SELL_VEH ", iVeh)
						IF GET_ENTITY_SPEED(VEH_ENT(iVeh)) < 2.0
							//IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(VEH_ID(iVeh))
								PRINTLN("[EXEC1] [CTRB_SELL] HANDLE_LANDING_PLANE_FLAG - TASK_LEAVE_VEHICLE  ", iVeh)
								TASK_LEAVE_VEHICLE(PLAYER_PED_ID(), VEH_ID(iVeh))
								LOCK_SELL_VEH(iVeh, TRUE)
							//ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC


FUNC PLAYER_INDEX PLAYER_REPAIRING()
	PLAYER_INDEX player = INVALID_PLAYER_INDEX()
	
	IF serverBD.iPlayerRepair <> -1
		player = INT_TO_PLAYERINDEX(serverBD.iPlayerRepair)
	ENDIF
	
	RETURN player
ENDFUNC

FUNC BOOL IS_TEAMMATE_REPAIRING()
	IF PLAYER_REPAIRING() <> INVALID_PLAYER_INDEX()
	AND PLAYER_REPAIRING() <> PLAYER_ID()
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_RUN_STUCK_CHECKS(INT iVeh)

	// If the vehicle has moved away from its spawn point
	IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(VEH_ID(iVeh), serverBD.vSellVehSpawnCoords[iVeh]) > 10
		RETURN TRUE
	ENDIF
	
	IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), VEH_ID(iVeh)) < 100
		RETURN TRUE
	ENDIF
	
	SWITCH iVeh
		CASE 0  
			IF IS_SERVER_BIT0_SET(eSERVERBITSET0_VEHICLE_0_ENTERED_ONCE)	
				RETURN TRUE	
			ENDIF 
		BREAK
		CASE 1  
			IF IS_SERVER_BIT0_SET(eSERVERBITSET0_VEHICLE_1_ENTERED_ONCE)	
				RETURN TRUE	
			ENDIF 
		BREAK
		CASE 2  
			IF IS_SERVER_BIT0_SET(eSERVERBITSET0_VEHICLE_2_ENTERED_ONCE)	
				RETURN TRUE	
			ENDIF 
		BREAK
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

PROC MAINTAIN_VEHICLE_STUCK_CHECKS()
	INT iVeh
	REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() iVeh
		IF IS_SELL_VEH_OK(iVeh)
			IF NOT IS_SERVER_BIT0_SET(eSERVERBITSET0_VEHICLE_STUCK)
				IF bSEA_VARIATION()
					IF SHOULD_RUN_STUCK_CHECKS(iVeh)
						IF NOT IS_ENTITY_IN_WATER(VEH_ID(iVeh))
							IF NOT HAS_NET_TIMER_STARTED(serverBD.stVehicleStuckTimer[iVeh])
								PRINTLN("[EXEC1] [CTRB_SELL] [STUCK], MAINTAIN_VEHICLE_STUCK_CHECKS - Sea Variation vehicle out of water timer started")
								START_NET_TIMER(serverBD.stVehicleStuckTimer[iVeh])
							ELSE
								IF HAS_NET_TIMER_EXPIRED(serverBD.stVehicleStuckTimer[iVeh], GET_MODE_TUNEABLE_OUT_OF_WATER_TIME())
									PRINTLN("[EXEC1] [CTRB_SELL] [STUCK], MAINTAIN_VEHICLE_STUCK_CHECKS - Sea Variation vehicle out of water timer expired")
									SET_SERVER_BIT0(eSERVERBITSET0_VEHICLE_STUCK)
								ENDIF
							ENDIF
						ELSE
							IF HAS_NET_TIMER_STARTED(serverBD.stVehicleStuckTimer[iVeh])
								#IF IS_DEBUG_BUILD
								FLOAT fTimeDiff = TO_FLOAT((GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.stVehicleStuckTimer[iVeh]) / 1000))
								PRINTLN("[EXEC1] [CTRB_SELL] [STUCK], MAINTAIN_VEHICLE_STUCK_CHECKS - Sea Variation vehicle out of water timer reset, elapsed time: ", fTimeDiff, " seconds")
								#ENDIF
								RESET_NET_TIMER(serverBD.stVehicleStuckTimer[iVeh])
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

FUNC BOOL CREATE_DROP_OFF_VEH()
	
	VECTOR vPosStart, vPosVeh
	FLOAT fHeadingVeh
	INT iLanes
	
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
	AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
//		vPosStart = GET_ENTITY_COORDS(PLAYER_PED_ID())
		vPosStart = GET_WAREHOUSE_COORDS(serverBD.iStartingWarehouse)
	ENDIF
	
	IF NOT DOES_MODE_NEED_DROP_OFF_VEH()
		SET_SERVER_BIT0(eSERVERBITSET0_DROP_OFF_VEH_MADE)
		RETURN TRUE
	ENDIF
	
	IF NOT IS_SERVER_BIT0_SET(eSERVERBITSET0_DROP_OFF_VEH_MADE)
		IF DOES_MODE_NEED_DROP_OFF_VEH()
		OR GET_SAFE_VEHICLE_NODE(vPosStart, vPosVeh, fHeadingVeh, iLanes)
			INT i
			REPEAT GET_NUM_DROP_OFF_VEH_FOR_VARIATION() i
				IF REQUEST_LOAD_MODEL(GET_DROP_OFF_VEH_MODEL(i))
				AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niDropOffVeh[i])
					IF DOES_MODE_NEED_DROP_OFF_VEH()
						vPosVeh = vGET_DROP_OFF_VEH_START(i)
						fHeadingVeh = fGET_DROP_OFF_VEH_HEAD(i)
					ENDIF
					IF CAN_REGISTER_MISSION_VEHICLES((GB_GET_BOSS_MISSION_NUM_VEH_REQUIRED(FMMC_TYPE_GB_CONTRABAND_SELL) + 1))
						IF CREATE_NET_VEHICLE(serverBD.niDropOffVeh[i], GET_DROP_OFF_VEH_MODEL(i), vPosVeh, fHeadingVeh)
						
							PRINTLN("[EXEC1] [CTRB_SELL] CREATE_DROP_OFF_VEH, DONE ", vPosVeh)
						
							
							//Add decorators
							IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
								INT iDecoratorValue
								IF DECOR_EXIST_ON(NET_TO_ENT(serverBD.niDropOffVeh[i]), "MPBitset")
									iDecoratorValue = DECOR_GET_INT(NET_TO_ENT(serverBD.niDropOffVeh[i]), "MPBitset")
								ENDIF
								SET_BIT(iDecoratorValue, MP_DECORATOR_BS_NON_PASSIVE_MODE_VEHICLE)
								SET_BIT(iDecoratorValue, MP_DECORATOR_BS_NON_MODDABLE_VEHICLE)
								DECOR_SET_INT(NET_TO_ENT(serverBD.niDropOffVeh[i]), "MPBitset", iDecoratorValue)
							ENDIF
							
							IF bSEA_VARIATION()
								SET_BOAT_ANCHOR(NET_TO_VEH(serverBD.niDropOffVeh[i]), TRUE)
							ENDIF
							
							SET_VEHICLE_ALLOW_HOMING_MISSLE_LOCKON_SYNCED(NET_TO_VEH(serverBD.niDropOffVeh[i]), FALSE)
							
							SET_VEHICLE_AUTOMATICALLY_ATTACHES(NET_TO_ENT(serverBD.niDropOffVeh[i]), FALSE)
							SET_ENTITY_INVINCIBLE(NET_TO_ENT(serverBD.niDropOffVeh[i]), TRUE)
							SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(NET_TO_VEH(serverBD.niDropOffVeh[i]), FALSE)
							SET_ENTITY_CAN_BE_DAMAGED(NET_TO_ENT(serverBD.niDropOffVeh[i]), FALSE)
							
							SET_VEHICLE_DOORS_LOCKED_FOR_NON_SCRIPT_PLAYERS(NET_TO_VEH(serverBD.niDropOffVeh[i]), TRUE)
							SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(NET_TO_VEH(serverBD.niDropOffVeh[i]), TRUE)
							SET_VEHICLE_CAN_BE_USED_BY_FLEEING_PEDS(NET_TO_VEH(serverBD.niDropOffVeh[i]), FALSE)
							
							CALL_SET_ALLOW_RAMMING_SOOP_OR_RAMP(NET_TO_VEH(serverBD.niDropOffVeh[i]))
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
			REPEAT GET_NUM_DROP_OFF_VEH_FOR_VARIATION() i
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niDropOffVeh[i])
					SET_SERVER_BIT0(eSERVERBITSET0_DROP_OFF_VEH_MADE)
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
	
	RETURN IS_SERVER_BIT0_SET(eSERVERBITSET0_DROP_OFF_VEH_MADE)
ENDFUNC

FUNC BOOL CREATE_DROP_OFF_PED( INT iPed)

	VECTOR vPos
	FLOAT fHeading

	IF DOES_MODE_NEED_DROP_OFF_PED()
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niDropOffPed[iPed])
			IF REQUEST_LOAD_MODEL(GET_DROP_OFF_PED_MODEL( iPed))
			
				vPos = vGET_DROP_OFF_PED_START( iPed)
				fHeading = fGET_DROP_OFF_PED_HEAD( iPed)
		
				IF CAN_REGISTER_MISSION_PEDS(GB_GET_BOSS_MISSION_NUM_PEDS_REQUIRED(FMMC_TYPE_GB_CONTRABAND_SELL) + 1)
					IF SHOULD_SPAWN_DROP_OFF_PED_IN_VEHICLE()
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niDropOffVeh[0])
							IF CREATE_NET_PED_IN_VEHICLE(serverBD.niDropOffPed[iPed], serverBD.niDropOffVeh[0], PEDTYPE_MISSION, GET_DROP_OFF_PED_MODEL( iPed), GET_DROP_OFF_PED_VEHICLE_SEAT(iPed))
								PRINTLN("[EXEC1] [CTRB_SELL] CREATE_DROP_OFF_PED, Created ped ", iPed, " in vehicle")
								SET_ENTITY_INVINCIBLE(NET_TO_PED(serverBD.niDropOffPed[iPed]), TRUE)
								SET_ENTITY_CAN_BE_DAMAGED(NET_TO_PED(serverBD.niDropOffPed[iPed]), FALSE)
								SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(serverBD.niDropOffPed[iPed]), CA_LEAVE_VEHICLES, FALSE)
								SET_PED_CONFIG_FLAG(NET_TO_PED(serverBD.niDropOffPed[iPed]), PCF_DisablePanicInVehicle, TRUE)
								SET_PED_CONFIG_FLAG(NET_TO_PED(serverBD.niDropOffPed[iPed]), PCF_CanBeAgitated, FALSE)
								SET_PED_CAN_BE_DRAGGED_OUT(NET_TO_PED(serverBD.niDropOffPed[iPed]), FALSE)
								SET_PED_CONFIG_FLAG(NET_TO_PED(serverBD.niDropOffPed[iPed]), PCF_PlayersDontDragMeOutOfCar, TRUE)
								SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(NET_TO_PED(serverBD.niDropOffPed[iPed]), KNOCKOFFVEHICLE_NEVER)
								SET_PED_FLEE_ATTRIBUTES(NET_TO_PED(serverBD.niDropOffPed[iPed]), FA_NEVER_FLEE, TRUE)
								SET_PED_RELATIONSHIP_GROUP_HASH(NET_TO_PED(serverBD.niDropOffPed[iPed]), rgFM_AiLike)
								SET_PED_KEEP_TASK(NET_TO_PED(serverBD.niDropOffPed[iPed]), TRUE)
								STOP_PED_SPEAKING(NET_TO_PED(serverBD.niDropOffPed[iPed]), TRUE)
								TASK_STAND_STILL(NET_TO_PED(serverBD.niDropOffPed[iPed]), -1)
								IF bSEA_ATTACK()
									SET_TREAT_AS_AMBIENT_PED_FOR_DRIVER_LOCKON(NET_TO_PED(serverBD.niDropOffPed[iPed]), TRUE)
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF CREATE_NET_PED(serverBD.niDropOffPed[iPed], PEDTYPE_MISSION, GET_DROP_OFF_PED_MODEL( iPed), vPos, fHeading)
							PRINTLN("[EXEC1] [CTRB_SELL] CREATE_DROP_OFF_PED, DONE ", vPos)
							SET_ENTITY_INVINCIBLE(NET_TO_PED(serverBD.niDropOffPed[iPed]), TRUE)
							SET_ENTITY_CAN_BE_DAMAGED(NET_TO_PED(serverBD.niDropOffPed[iPed]), FALSE)
							SET_PED_KEEP_TASK(NET_TO_PED(serverBD.niDropOffPed[iPed]), TRUE)
							STOP_PED_SPEAKING(NET_TO_PED(serverBD.niDropOffPed[iPed]), TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CREATE_DROP_OFF_PEDS()
	INT i
	
	REPEAT GET_NUM_DROP_OFF_PEDS_FOR_VARIATION() i
		IF CREATE_DROP_OFF_PED( i)
			PRINTLN("[EXEC1] [CTRB_SELL] [DROPOFF_PED], CREATE_DROP_OFF_PEDS - Drop off ped ", i, " created.")
		ENDIF
	ENDREPEAT
	
	REPEAT GET_NUM_DROP_OFF_PEDS_FOR_VARIATION() i
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niDropOffPed[i])
			RETURN FALSE
		ENDIF
	ENDREPEAT 
	
	RETURN TRUE
ENDFUNC

//FUNC VECTOR DROP_OFF_VEH_POS(BOOL bCloseBy = FALSE)
//
//	VECTOR vReturn 
//	
//	IF IS_DROP_OFF_VEH_OK()
//		vReturn = GET_ENTITY_COORDS(DROP_OFF_VEH_ENT())
//	ENDIF
//	
//	IF bCloseBy
//		vReturn = vReturn + <<3.0, 3.0, 0.0>>
//	ENDIF
//	
//	RETURN vReturn
//ENDFUNC

//FUNC FLOAT DROP_OFF_VEH_HEADING()
//
//	FLOAT fReturn
//	
//	IF IS_DROP_OFF_VEH_OK()
//		fReturn = GET_ENTITY_HEADING(DROP_OFF_VEH_ENT())
//	ENDIF
//	
//	RETURN fReturn
//ENDFUNC

//╔═════════════════════════════════════════════════════════════════════════════╗
//║				AI PEDS															║

// Ped deletion

FUNC BOOL DELETE_PEDS()
	INT i
	REPEAT GET_NUM_PEDS_FOR_VARIATION() i
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPed[i].netIndex)
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sPed[i].netIndex)
			OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.sPed[i].netIndex) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
			OR (IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.sPed[i].netIndex) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
				IF TAKE_CONTROL_OF_NET_ID(serverBD.sPed[i].netIndex)
					IF NOT bLAND_DEFEND()
					AND NOT bLAND_ATTACK()
						NETWORK_FADE_OUT_ENTITY(NET_TO_ENT(serverBD.sPed[i].netIndex), TRUE, TRUE)
						DELETE_NET_ID(serverBD.sPed[i].netIndex)
					ELSE
						CLEANUP_NET_ID(serverBD.sPed[i].netIndex) // For land defend, don't want to delete the peds as they might be right in front of the player
					ENDIF
					SET_PED_STATE(i, ePEDSTATE_CREATE)
					PRINTLN("[EXEC1] [CTRB_SELL] [PED], DELETE_PEDS - Ped ", i, " deleted")
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	i = 0
	REPEAT GET_NUM_PEDS_FOR_VARIATION() i
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPed[i].netIndex)
			PRINTLN("[EXEC1] [CTRB_SELL] [PED], DELETE_PEDS - Waiting for ped ", i, " to be deleted")
			RETURN FALSE
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

// ------------------------------------------------------------------>|
// 		Ped Creation / Behaviour

FUNC BOOL DOES_AI_PED_EXIST(INT iPed)
	RETURN NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPed[iPed].netIndex)
ENDFUNC

FUNC PED_INDEX GET_AI_PED_PED_INDEX(INT iPed)
	IF DOES_AI_PED_EXIST(iPed)
		RETURN NET_TO_PED(serverBD.sPed[iPed].netIndex)
	ENDIF
	
	RETURN NULL
ENDFUNC

FUNC ENTITY_INDEX GET_AI_PED_ENTITY_INDEX(INT iPed)
	IF DOES_AI_PED_EXIST(iPed)
		RETURN NET_TO_ENT(serverBD.sPed[iPed].netIndex)
	ENDIF
	RETURN NULL
ENDFUNC

/// PURPOSE:
///    Set the health of the specified ped to 0 if the server says so, and I have control
/// PARAMS:
///    iPed - 
PROC HANDLE_PED_DISTANCE_FROM_SELL_VEH_CLIENT(INT iPed)
	IF IS_BIT_SET(serverBD.iAiPedShouldDieBitset, iPed)
		IF DOES_AI_PED_EXIST(iPed)
			IF NOT IS_PED_INJURED(GET_AI_PED_PED_INDEX(iPed))
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netIndex)
        		OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.sPed[iPed].netIndex) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
					IF TAKE_CONTROL_OF_NET_ID(serverBD.sPed[iPed].netIndex)
						PRINTLN("[EXEC1] [CTRB_SELL] [PED] I'VE SET TO HEALTH OF PED ", iPed, " TO 0")
						SET_ENTITY_HEALTH(GET_AI_PED_ENTITY_INDEX(iPed), 0)
					ENDIF
				ENDIF
			ENDIF
		ENDIF	
	ENDIF
ENDPROC

/// PURPOSE:
///    Clients process the bodies of the AI peds
PROC MAINTAIN_PED_BODIES()
	
	PED_INDEX pedId
	VECTOR vFleeCoords
	INT iClosestVeh
	VEHICLE_INDEX pedVeh
	IF DOES_VARIATION_HAVE_AI_PEDS()
		INT iPed
		REPEAT GET_NUM_PEDS_FOR_VARIATION() iPed
			SWITCH GET_PED_STATE(iPed)
				CASE ePEDSTATE_INACTIVE
				BREAK
				
				// In this state whilst being created
				CASE ePEDSTATE_CREATE
					
				BREAK
				
				// Come in here to stand idly
				CASE ePEDSTATE_GUARD_CONTRABAND
					IF DOES_AI_PED_EXIST(iPed)
					
					ENDIF
				BREAK
				
				// Chase any vehicles/people with contraband
				CASE ePEDSTATE_CHASE_CONTRABAND
					IF DOES_AI_PED_EXIST(iPed)
						pedID = GET_AI_PED_PED_INDEX(iPed)
						
						IF bSEA_ATTACK()
						OR bLAND_ATTACK()
					//	OR bAIR_ATTACK()
							HANDLE_PED_DISTANCE_FROM_SELL_VEH_CLIENT(iPed)
						ENDIF
						
						IF NOT IS_ENTITY_DEAD(pedID)
							IF GET_SCRIPT_TASK_STATUS(pedID, SCRIPT_TASK_VEHICLE_MISSION) != PERFORMING_TASK
							AND GET_SCRIPT_TASK_STATUS(pedID, SCRIPT_TASK_VEHICLE_MISSION) != WAITING_TO_START_TASK
								IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netIndex)
			            		OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.sPed[iPed].netIndex) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
									IF TAKE_CONTROL_OF_NET_ID(serverBD.sPed[iPed].netIndex)
										pedVeh = NET_TO_VEH(GET_NET_VEHICLE_PED_SHOULD_BE_IN( iPed))
										
										IF DOES_ENTITY_EXIST(pedVeh)
										AND DOES_ENTITY_EXIST(VEH_ID(serverBD.iRandVehToAttack))
											IF IS_ENTITY_ALIVE(pedVeh)
											AND IS_ENTITY_ALIVE(VEH_ID(serverBD.iRandVehToAttack))
												SET_ATTRIBUTES_FOR_CHASE( pedId, GET_PED_ACCURACY_FOR_VARIATION())
												IF bAIR_ATTACK()
													TASK_HELI_MISSION(	pedId, 
																	pedVeh, 
																	VEH_ID(serverBD.iRandVehToAttack), 
																	NULL, 
																	GET_ENTITY_COORDS(VEH_ID(serverBD.iRandVehToAttack)), 
																	MISSION_ATTACK, 
																	100, 10, -1, 100, 60, -1, 
																	HF_MaintainHeightAboveTerrain | HF_StartEngineImmediately)
													PRINTLN("[EXEC1] [CTRB_SELL] [PED], MAINTAIN_PED_BODIES - Ped ", iPed, " tasked with TASK_HELI_MISSION.")
												ELSE
													TASK_VEHICLE_MISSION(pedId, pedVeh, VEH_ID(serverBD.iRandVehToAttack), MISSION_FOLLOW, 50.0, DRIVINGMODE_AVOIDCARS, -1, -1)
													PRINTLN("[EXEC1] [CTRB_SELL] [PED], MAINTAIN_PED_BODIES - Ped ", iPed, " tasked with TASK_VEHICLE_MISSION.")
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF bSEA_ATTACK()
								OR bLAND_ATTACK()
									IF NOT IS_PED_DOING_DRIVEBY(pedId)
										IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netIndex)
			            				OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.sPed[iPed].netIndex) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
											IF TAKE_CONTROL_OF_NET_ID(serverBD.sPed[iPed].netIndex)
												pedVeh = NET_TO_VEH(GET_NET_VEHICLE_PED_SHOULD_BE_IN( iPed))
											
												IF DOES_ENTITY_EXIST(pedVeh)
												AND DOES_ENTITY_EXIST(VEH_ID(serverBD.iRandVehToAttack))
													IF IS_ENTITY_ALIVE(pedVeh)
													AND IS_ENTITY_ALIVE(VEH_ID(serverBD.iRandVehToAttack))
														TASK_DRIVE_BY(pedId, NULL, VEH_ID(serverBD.iRandVehToAttack), <<0.0,0.0,0.0>>, -1, 30, TRUE)
														PRINTLN("[EXEC1] [CTRB_SELL] [PED], MAINTAIN_PED_BODIES - Ped ", iPed, " tasked with TASK_DRIVE_BY.")
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE ePEDSTATE_CHASE_CONTRABAND_AND_ATTACK
					// Handle the ped task
					IF DOES_AI_PED_EXIST(iPed)
						pedID = GET_AI_PED_PED_INDEX(iPed) 
				
						IF NOT IS_ENTITY_DEAD(pedID)
							IF GET_SCRIPT_TASK_STATUS(pedID, SCRIPT_TASK_VEHICLE_MISSION) != PERFORMING_TASK
							AND GET_SCRIPT_TASK_STATUS(pedID, SCRIPT_TASK_VEHICLE_MISSION) != WAITING_TO_START_TASK
								IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netIndex)
			            		OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.sPed[iPed].netIndex) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
									IF TAKE_CONTROL_OF_NET_ID(serverBD.sPed[iPed].netIndex)
										pedVeh = NET_TO_VEH(GET_NET_VEHICLE_PED_SHOULD_BE_IN(iPed))
										
										IF DOES_ENTITY_EXIST(pedVeh)
										AND DOES_ENTITY_EXIST(VEH_ID(serverBD.iRandVehToAttack))
											IF IS_ENTITY_ALIVE(pedVeh)
											AND IS_ENTITY_ALIVE(VEH_ID(serverBD.iRandVehToAttack))
											
												IF bSEA_ATTACK()
												AND IS_SERVER_BIT0_SET(eSERVERBITSET0_SEA_ATTACK_HELI_VARIANT)
													DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_VEHICLE_SPACE_ROCKET, pedVeh, pedId)
													DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_VEHICLE_ROCKET, pedVeh, pedId)
													SET_CURRENT_PED_VEHICLE_WEAPON(pedId, WEAPONTYPE_VEHICLE_PLAYER_BUZZARD)
												ENDIF
											
												SET_ATTRIBUTES_FOR_CHASE( pedId, GET_PED_ACCURACY_FOR_VARIATION())
												TASK_HELI_MISSION(	pedId, 
																	pedVeh, 
																	VEH_ID(serverBD.iRandVehToAttack), 
																	NULL, 
																	GET_ENTITY_COORDS(VEH_ID(serverBD.iRandVehToAttack)), 
																	MISSION_ATTACK, 
																	80, 30, -1, 60, 50, -1, 
																	HF_MaintainHeightAboveTerrain | HF_StartEngineImmediately)
												PRINTLN("[EXEC1] [CTRB_SELL] [PED], MAINTAIN_PED_BODIES - Ped ", iPed, " tasked with TASK_HELI_MISSION.")
												
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
//							IF NOT bAIR_ATTACK()
//								INT iVeh
//								iVeh = GET_NET_VEHICLE_PED_SHOULD_BE_IN_AS_INT(iPed)
//								IF iVeh <> -1
//									IF HAS_NET_TIMER_EXPIRED(serverBD.stVehSpawnDelayTimer[iVeh], ciSAFE_TO_ATTACK_DELAY)
//										IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netIndex)
//							            OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.sPed[iPed].netIndex) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
//											IF TAKE_CONTROL_OF_NET_ID(serverBD.sPed[iPed].netIndex)
//												pedVeh = NET_TO_VEH(GET_NET_VEHICLE_PED_SHOULD_BE_IN(iPed))
//												
//												IF DOES_ENTITY_EXIST(pedVeh)
//													IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(GET_NET_VEHICLE_PED_SHOULD_BE_IN(iPed))
//						            				OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(GET_NET_VEHICLE_PED_SHOULD_BE_IN(iPed)) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
//														IF TAKE_CONTROL_OF_NET_ID(serverBD.sPed[iPed].netIndex)
//															IF IS_ENTITY_ALIVE(pedVeh)
//																DISABLE_VEHICLE_WEAPON(FALSE, WEAPONTYPE_VEHICLE_SPACE_ROCKET, pedVeh, NULL)
//																DISABLE_VEHICLE_WEAPON(FALSE, WEAPONTYPE_VEHICLE_ROCKET, pedVeh, NULL)
//															ENDIF
//														ENDIF
//													ENDIF
//												ENDIF
//											ENDIF	
//										ENDIF
//									ENDIF
//								ENDIF
//							ENDIF
						ENDIF
					ENDIF
				
					// Handle firing the rocket
//					INT iVeh
//					iVeh = GET_NET_VEHICLE_PED_SHOULD_BE_IN_AS_INT(iPed)
//					IF iVeh <> -1
//						IF HAS_NET_TIMER_EXPIRED(serverBD.stVehSpawnDelayTimer[iVeh], ciSAFE_TO_ATTACK_DELAY)
//							IF HAS_NET_TIMER_EXPIRED(sLocalData.stHeliFireTimer, sLocalData.iRocketDelay)
//							OR NOT HAS_NET_TIMER_STARTED(sLocalData.stHeliFireTimer)
//							#IF IS_DEBUG_BUILD
//							OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_LEFT)
//							#ENDIF
//								IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GET_NET_VEHICLE_PED_SHOULD_BE_IN(iPed))
//								AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[serverBD.iRandVehToAttack])
//									IF IS_NET_VEHICLE_DRIVEABLE(GET_NET_VEHICLE_PED_SHOULD_BE_IN(iPed))
//										IF NOT IS_NET_PED_INJURED(serverBD.sPed[iPed].netIndex)
//											IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netIndex)
//												pedId = GET_AI_PED_PED_INDEX(iPed)
//												pedVeh = NET_TO_VEH(GET_NET_VEHICLE_PED_SHOULD_BE_IN( iPed))
//												contraVeh = VEH_ID(serverBD.iRandVehToAttack)
//												
//												IF GET_DISTANCE_BETWEEN_ENTITIES(pedVeh, contraVeh) < 100
//													IF IS_PED_SITTING_IN_VEHICLE(pedId, pedVeh)
//														IF DOES_ENTITY_EXIST(pedVeh)
//														AND NOT IS_ENTITY_DEAD(contraVeh)
//															
//															IF SET_CURRENT_PED_VEHICLE_WEAPON(pedId, WEAPONTYPE_VEHICLE_SPACE_ROCKET)
//																VECTOR vHeliOffset
//																vHeliOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(pedVeh, GET_ENTITY_COORDS(contraVeh))
//																
//																IF vHeliOffset.y >= 10
//																	INT iRocketRNG
//																	iRocketRNG = GET_RANDOM_INT_IN_RANGE(0, 100)
//																	
//																	IF iRocketRNG < GET_PED_ACCURACY_FOR_VARIATION()
//																		SET_VEHICLE_SHOOT_AT_TARGET(pedId, contraVeh, <<0,0,0>>)
//																		PRINTLN("[EXEC1] [CTRB_SELL] - ePEDSTATE_CHASE_CONTRABAND_AND_ATTACK - Firing on target at driver.")
//				                                                    ELSE
//																		VECTOR vOffset
//																		vOffset = <<0.0, 0.0, 0.0>>
//																		vOffset.x = GET_RANDOM_FLOAT_IN_RANGE(5, 10)
//					                                                    vOffset.y = GET_RANDOM_FLOAT_IN_RANGE(5, 10)
//					                                                    vOffset.z = GET_RANDOM_FLOAT_IN_RANGE(5, 10)
//																		
//					                                                    IF GET_RANDOM_INT_IN_RANGE(0,2) = 0
//																			vOffset.x *= -1
//					                                                    ENDIF
//					                                                    IF GET_RANDOM_INT_IN_RANGE(0,2) = 0
//					                                                        vOffset.y *= -1
//					                                                    ENDIF
//																	
//				                                                        SET_VEHICLE_SHOOT_AT_TARGET(pedId, NULL, GET_ENTITY_COORDS(contraVeh) + vOffset)
//																		PRINTLN("[EXEC1] [CTRB_SELL] - ePEDSTATE_CHASE_CONTRABAND_AND_ATTACK - Firing miss at driver.")
//				                                                    ENDIF
//				                                                ENDIF
//																
//																RESET_NET_TIMER(sLocalData.stHeliFireTimer)
//																START_NET_TIMER(sLocalData.stHeliFireTimer)
//																
//				                                                sLocalData.iRocketDelay = GET_RANDOM_INT_IN_RANGE(ciROCKET_DELAY, 10000)
//															ENDIF
//				                                        ENDIF
//													ENDIF
//												ENDIF
//											ENDIF
//										ENDIF
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
					
				BREAK
				
				// Attack enemies nearby
				CASE ePEDSTATE_ATTACK_HATED_TARGETS
					IF DOES_AI_PED_EXIST(iPed)
						pedId = GET_AI_PED_PED_INDEX(iPed)
						
						IF bSEA_ATTACK()
						OR bLAND_ATTACK()
						//OR bAIR_ATTACK()
							HANDLE_PED_DISTANCE_FROM_SELL_VEH_CLIENT(iPed)
						ENDIF
						
						IF NOT IS_ENTITY_DEAD(pedId)
							IF bSEA_DEFEND()
								IF GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_COMBAT_HATED_TARGETS_IN_AREA) != PERFORMING_TASK
								AND GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_COMBAT_HATED_TARGETS_IN_AREA) != WAITING_TO_START_TASK
									IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netIndex)
				            		OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.sPed[iPed].netIndex) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
										IF TAKE_CONTROL_OF_NET_ID(serverBD.sPed[iPed].netIndex)
											SET_ATTRIBUTES_FOR_ATTACK( pedId, iPed, TRUE, FALSE, GET_PED_ACCURACY_FOR_VARIATION())
				                        	TASK_COMBAT_HATED_TARGETS_IN_AREA(pedId, GET_ENTITY_COORDS(pedId), 299.9)
											PRINTLN("[EXEC1] [CTRB_SELL] [PED], MAINTAIN_PED_BODIES - Ped ", iPed, " tasked with TASK_COMBAT_HATED_TARGETS_IN_AREA.")
										ENDIF
									ENDIF
								ENDIF
							ELIF bLAND_ATTACK()
							OR bLAND_DEFEND()
							OR bAIR_ATTACK()
								IF GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_COMBAT_HATED_TARGETS_IN_AREA) != PERFORMING_TASK
								AND GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_COMBAT_HATED_TARGETS_IN_AREA) != WAITING_TO_START_TASK
									IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netIndex)
				            		OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.sPed[iPed].netIndex) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
										IF TAKE_CONTROL_OF_NET_ID(serverBD.sPed[iPed].netIndex)
											SET_ATTRIBUTES_FOR_ATTACK( pedId, iPed, FALSE, TRUE, GET_PED_ACCURACY_FOR_VARIATION())
				                        	TASK_COMBAT_HATED_TARGETS_IN_AREA(pedId, GET_ENTITY_COORDS(pedId), 299.9)
											PRINTLN("[EXEC1] [CTRB_SELL] [PED], MAINTAIN_PED_BODIES - Ped ", iPed, " tasked with TASK_COMBAT_HATED_TARGETS_IN_AREA.")
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED) != PERFORMING_TASK
								AND GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED) != WAITING_TO_START_TASK
									IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netIndex)
				            		OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.sPed[iPed].netIndex) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
										IF TAKE_CONTROL_OF_NET_ID(serverBD.sPed[iPed].netIndex)
											SET_ATTRIBUTES_FOR_ATTACK( pedId, iPed, TRUE, TRUE, GET_PED_ACCURACY_FOR_VARIATION())
				                        	TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedID, 299.9)
											PRINTLN("[EXEC1] [CTRB_SELL] [PED], MAINTAIN_PED_BODIES - Ped ", iPed, " tasked with TASK_COMBAT_HATED_TARGETS_AROUND_PED.")
										ENDIF
									ENDIF
								ENDIF
							ENDIF
	                    ENDIF
					ENDIF
				BREAK
				
				CASE ePEDSTATE_EXIT_VEH
					IF DOES_AI_PED_EXIST(iPed)
						pedId = GET_AI_PED_PED_INDEX(iPed)
						
						IF NOT IS_ENTITY_DEAD(pedId)
							IF (GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_LEAVE_ANY_VEHICLE) != PERFORMING_TASK
							AND GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_LEAVE_ANY_VEHICLE) != WAITING_TO_START_TASK)
								IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netIndex)
			            		OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.sPed[iPed].netIndex) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
									IF TAKE_CONTROL_OF_NET_ID(serverBD.sPed[iPed].netIndex)
										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedId, TRUE)
										TASK_LEAVE_ANY_VEHICLE(pedId)
										PRINTLN("[EXEC1] [CTRB_SELL] [PED], MAINTAIN_PED_BODIES - Ped ", iPed, " tasked with TASK_LEAVE_ANY_VEHICLE.")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
						
				BREAK
				// Peds run away
				CASE ePEDSTATE_FLEE_ON_FOOT
					IF DOES_AI_PED_EXIST(iPed)
						pedId = GET_AI_PED_PED_INDEX(iPed)
						
						IF NOT IS_ENTITY_DEAD(pedId)
							IF (GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_SMART_FLEE_POINT) != PERFORMING_TASK
							AND GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_SMART_FLEE_POINT) != WAITING_TO_START_TASK)
								IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netIndex)
			            		OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.sPed[iPed].netIndex) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
									IF TAKE_CONTROL_OF_NET_ID(serverBD.sPed[iPed].netIndex)
										TASK_SMART_FLEE_COORD(pedId, GET_ENTITY_COORDS(pedId), 10000.0, 999999)
										SET_PED_FLEE_ATTRIBUTES(pedId, FA_UPDATE_TO_NEAREST_HATED_PED, TRUE)
										SET_PED_FLEE_ATTRIBUTES(pedId, FA_USE_VEHICLE, FALSE)
										PRINTLN("[EXEC1] [CTRB_SELL] [PED], MAINTAIN_PED_BODIES - Ped ", iPed, " tasked with TASK_SMART_FLEE_COORD.")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE ePEDSTATE_FLEE_IN_VEH
					IF DOES_AI_PED_EXIST(iPed)
						pedId = GET_AI_PED_PED_INDEX(iPed)
						
						IF NOT IS_ENTITY_DEAD(pedId)
							IF (GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_SMART_FLEE_POINT) != PERFORMING_TASK
							AND GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_SMART_FLEE_POINT) != WAITING_TO_START_TASK)
								IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netIndex)
			            		OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.sPed[iPed].netIndex) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
									IF TAKE_CONTROL_OF_NET_ID(serverBD.sPed[iPed].netIndex)
										TASK_SMART_FLEE_COORD(pedId, GET_ENTITY_COORDS(pedId), 10000.0, 999999)
										SET_PED_FLEE_ATTRIBUTES(pedId, FA_UPDATE_TO_NEAREST_HATED_PED, TRUE)
										SET_PED_FLEE_ATTRIBUTES(pedId, FA_USE_VEHICLE, FALSE)
										PRINTLN("[EXEC1] [CTRB_SELL] [PED], MAINTAIN_PED_BODIES - Ped ", iPed, " tasked with TASK_SMART_FLEE_COORD.")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				// Flee drop-off
				CASE ePEDSTATE_FLEE_DROP_OFF
					IF DOES_AI_PED_EXIST(iPed)
						pedId = NET_TO_PED(serverBD.sPed[iPed].netIndex)
						
						IF NOT IS_ENTITY_DEAD(pedId)
							IF (GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_SMART_FLEE_POINT) != PERFORMING_TASK
							AND GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_SMART_FLEE_POINT) != WAITING_TO_START_TASK)
								IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netIndex)
			            		OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.sPed[iPed].netIndex) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
									IF TAKE_CONTROL_OF_NET_ID(serverBD.sPed[iPed].netIndex)
										GET_CLOSEST_SELL_VEH_TO_PED(iPed, iClosestVeh)
										IF iClosestVeh <> -1
											vFleeCoords = GET_SELL_DROP_COORDS(GET_SELL_VAR(), serverBD.iClosestDropOff[iClosestVeh], serverBD.iRoute, iClosestVeh)
											IF NOT ARE_VECTORS_EQUAL(vFleeCoords, << 0.0, 0.0, 0.0 >>)
												SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedId, TRUE)
												SET_PED_COMBAT_ATTRIBUTES(pedId, CA_ALWAYS_FIGHT, FALSE)
												SET_PED_COMBAT_ATTRIBUTES(pedId, CA_ALWAYS_FLEE, TRUE)
												SET_PED_FLEE_ATTRIBUTES(pedId, FA_DISABLE_COWER,TRUE)
												SET_PED_FLEE_ATTRIBUTES(pedId,  FA_COWER_INSTEAD_OF_FLEE,FALSE)
												TASK_SMART_FLEE_COORD(pedId, vFleeCoords, 10000.0, 999999)
												PRINTLN("[EXEC1] [CTRB_SELL] [PED], MAINTAIN_PED_BODIES - Ped ", iPed, " tasked with TASK_SMART_FLEE_COORD (ePEDSTATE_FLEE_DROP_OFF) from coord ", vFleeCoords)
											ELSE
												PRINTLN("[EXEC1] [CTRB_SELL] [PED], MAINTAIN_PED_BODIES - Ped ", iPed, " can't flee because flee coords are zero! serverBD.iClosestDropOff = ", serverBD.iClosestDropOff[iClosestVeh])
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							HANDLE_PED_DISTANCE_FROM_SELL_VEH_CLIENT(iPed)
						ENDIF
					ENDIF
				BREAK
				
				// Peds drive to a specified location
				CASE ePEDSTATE_DRIVE_TO_POINT
					IF DOES_AI_PED_EXIST(iPed)
						
					ENDIF
				BREAK
				
				CASE ePEDSTATE_RELEASE_TO_AMBIENT_POPULATION
					IF DOES_AI_PED_EXIST(iPed)
						pedId = NET_TO_PED(serverBD.sPed[iPed].netIndex)
							
						IF NOT IS_ENTITY_DEAD(pedId)
							IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netIndex)
		            		OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.sPed[iPed].netIndex) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
								IF TAKE_CONTROL_OF_NET_ID(serverBD.sPed[iPed].netIndex)
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedId, FALSE)
									SET_PED_KEEP_TASK(pedId, TRUE)
									SET_PED_DIES_WHEN_INJURED(pedId, TRUE)
									CLEANUP_NET_ID(serverBD.sPed[iPed].netIndex)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				// Peds parachute when falling
				CASE ePEDSTATE_PARACHUTE
					IF DOES_AI_PED_EXIST(iPed)
					
					ENDIF
				BREAK
				
				// Move here when dead
				CASE ePEDSTATE_DEAD
				BREAK
			
				// Reset the peds here	
				CASE ePEDSTATE_RESET
				BREAK
			ENDSWITCH
		ENDREPEAT
	ENDIF
ENDPROC

FUNC FLOAT GET_MAXIMUM_DISTANCE_FOR_ENTITY_FORCE_CLEANUP()
	IF bAIR_ATTACK()
		RETURN 2000.0
	ENDIF
	RETURN 400.0
ENDFUNC

FUNC FLOAT GET_MINIMUM_DISTANCE_FOR_TIMED_ENTITY_CLEANUP()
	IF bAIR_ATTACK()
		RETURN 1000.0
	ENDIF
	RETURN 200.0
ENDFUNC

PROC HANDLE_PED_DISTANCE_FROM_SELL_VEH_SERVER(INT iPed)
	FLOAT fMaxCleanupDist = GET_MAXIMUM_DISTANCE_FOR_ENTITY_FORCE_CLEANUP()
	FLOAT fTimedCleanupDist = GET_MINIMUM_DISTANCE_FOR_TIMED_ENTITY_CLEANUP()
	INT iVeh
	
	VEHICLE_INDEX veh = GET_CLOSEST_SELL_VEH_TO_PED(iPed, iVeh)
	
	IF DOES_ENTITY_EXIST(veh)
		IF NOT IS_BIT_SET(serverBD.iAiPedShouldDieBitset, iPed)
			IF GET_DISTANCE_BETWEEN_ENTITIES(GET_AI_PED_ENTITY_INDEX(iPed), veh) > fMaxCleanupDist
				PRINTLN("[EXEC1] [CTRB_SELL] [PED], HANDLE_PED_DISTANCE_FROM_SELL_VEH_SERVER - Ped ", iPed, " is > fMaxCleanupDist from closest sell veh, killing ped immediately, fMaxCleanupDist = ", fMaxCleanupDist)
				//-- Dave W - I think you need to have control to set a ped's health. Set a bit instead and get whoever has control to set the health
				//-- (See HANDLE_PED_DISTANCE_FROM_SELL_VEH_CLIENT)
				
			//	SET_ENTITY_HEALTH(GET_AI_PED_ENTITY_INDEX(iPed), 0)
				SET_BIT(serverBD.iAiPedShouldDieBitset, iPed)
			ELSE
				IF GET_DISTANCE_BETWEEN_ENTITIES(GET_AI_PED_ENTITY_INDEX(iPed), veh) > fTimedCleanupDist
					IF NOT HAS_NET_TIMER_STARTED(serverBD.sPed[iPed].stOutOfRangeTimer)
						PRINTLN("[EXEC1] [CTRB_SELL] [PED], HANDLE_PED_DISTANCE_FROM_SELL_VEH_SERVER - Ped ", iPed, " is > fTimedCleanupDist from sell veh, starting ped out of range timer, fTimedCleanupDist = ", fTimedCleanupDist)
						START_NET_TIMER(serverBD.sPed[iPed].stOutOfRangeTimer)
					ELSE
						IF HAS_NET_TIMER_EXPIRED(serverBD.sPed[iPed].stOutOfRangeTimer, GET_MODE_TUNEABLE_PED_OUT_OF_RANGE_TIME())
							PRINTLN("[EXEC1] [CTRB_SELL] [PED], HANDLE_PED_DISTANCE_FROM_SELL_VEH_SERVER - Ped ", iPed, " out of range timer expired, killing ped immediately")
						//	SET_ENTITY_HEALTH(GET_AI_PED_ENTITY_INDEX(iPed), 0)
							SET_BIT(serverBD.iAiPedShouldDieBitset, iPed)
						ENDIF
					ENDIF
				ELSE
					IF HAS_NET_TIMER_STARTED(serverBD.sPed[iPed].stOutOfRangeTimer)
						PRINTLN("[EXEC1] [CTRB_SELL] [PED], HANDLE_PED_DISTANCE_FROM_SELL_VEH_SERVER - Ped ", iPed, " is within fTimedCleanupDist from sell veh again, resetting ped out of range timer, fTimedCleanupDist = ", fTimedCleanupDist)
						RESET_NET_TIMER(serverBD.sPed[iPed].stOutOfRangeTimer)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//-- Flee dropoff
		VECTOR vDropOffCoord
		IF bLAND_ATTACK()
			IF NOT IS_BIT_SET(serverBD.iAiPedShouldDieBitset, iPed) 
				IF NOT IS_BIT_SET(serverBD.iAiPedShouldFleeBitset, iPed)
					IF serverBD.iClosestDropOff[iVeh] >= 0
						vDropOffCoord =  GET_SELL_DROP_COORDS(GET_SELL_VAR(), serverBD.iClosestDropOff[iVeh], serverBD.iRoute, iVeh)
						FLOAT fDist = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(GET_AI_PED_ENTITY_INDEX(iPed), FALSE), vDropOffCoord)
						IF fDist < 100
							SET_BIT(serverBD.iAiPedShouldFleeBitset, iPed)
							PRINTLN("[EXEC1] [CTRB_SELL] [PED], HANDLE_PED_DISTANCE_FROM_SELL_VEH_SERVER - Ped ", iPed, " set to flee as they are this distance from drop off ", fDist, ", drop-off coord ", vDropOffCoord, " , closest drop-off = ", serverBD.iClosestDropOff[iVeh]) 
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELIF bAIR_ATTACK()
			IF NOT IS_BIT_SET(serverBD.iAiPedShouldDieBitset, iPed) 
				IF NOT IS_BIT_SET(serverBD.iAiPedShouldFleeBitset, iPed)
					IF iDROPPED_OFF_COUNT() = (iDROP_TARGET() - 1)
						IF serverBD.iClosestDropOff[iVeh] >= 0
							vDropOffCoord =  GET_SELL_DROP_COORDS(GET_SELL_VAR(), serverBD.iClosestDropOff[iVeh], serverBD.iRoute, iVeh)
							FLOAT fDist = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(GET_AI_PED_ENTITY_INDEX(iPed), FALSE), vDropOffCoord)
							IF fDist < 100
								SET_BIT(serverBD.iAiPedShouldFleeBitset, iPed)
								PRINTLN("[EXEC1] [CTRB_SELL] [PED], HANDLE_PED_DISTANCE_FROM_SELL_VEH_SERVER - Ped ", iPed, " set to flee as they are this distance from drop off ", fDist, ", drop-off coord ", vDropOffCoord, " , closest drop-off = ", serverBD.iClosestDropOff[iVeh]) 
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF		
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_TARGET_KILLED_TOTAL(INT iPed)
	IF NOT bLAND_DEFEND()
		EXIT
	ENDIF
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPed[iPed].netIndex)
		IF IS_NET_PED_INJURED(serverBD.sPed[iPed].netIndex)
			IF NOT IS_BIT_SET(serverBD.iGangChasePedKilledBitset, iPed)
				SET_BIT(serverBD.iGangChasePedKilledBitset, iPed)
				serverBD.iTotalKills++
				PRINTLN("[EXEC1] [CTRB_SELL] [PED] [MAINTAIN_TARGET_KILLED_TOTAL] PED ", iPed, " is injured. serverBD.iTotalKills = ", serverBD.iTotalKills)
			ENDIF
		ELSE
			IF IS_BIT_SET(serverBD.iGangChasePedKilledBitset, iPed)
				CLEAR_BIT(serverBD.iGangChasePedKilledBitset, iPed)
				PRINTLN("[EXEC1] [CTRB_SELL] [PED] [MAINTAIN_TARGET_KILLED_TOTAL] PED ", iPed, " not injured. serverBD.iTotalKills = ", serverBD.iTotalKills)
			ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(serverBD.iGangChasePedKilledBitset, iPed)
			CLEAR_BIT(serverBD.iGangChasePedKilledBitset, iPed)
			PRINTLN("[EXEC1] [CTRB_SELL] [PED] [MAINTAIN_TARGET_KILLED_TOTAL] NI for PED ", iPed, " doesn't exist. serverBD.iTotalKills = ", serverBD.iTotalKills)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Server processes the brains of the AI peds
PROC MAINTAIN_PED_BRAINS()
	INT iVehForPed 
	
	IF DOES_VARIATION_HAVE_AI_PEDS()
		INT iNearestCOntraVeh
		INT iPed
		REPEAT GET_NUM_PEDS_FOR_VARIATION() iPed
			
			MAINTAIN_TARGET_KILLED_TOTAL(iPed)
			
			SWITCH GET_PED_STATE(iPed)
				CASE ePEDSTATE_INACTIVE
				BREAK
				
				// In this state whilst being created
				CASE ePEDSTATE_CREATE
					IF DOES_AI_PED_EXIST(iPed)
						SET_PED_STATE(iPed, ePEDSTATE_GUARD_CONTRABAND)
					ENDIF
				BREAK
				
				// Come in here to stand idly and wait for spooking or other instruction
				CASE ePEDSTATE_GUARD_CONTRABAND
					IF DOES_AI_PED_EXIST(iPed)
						SWITCH GET_SELL_VAR()
							CASE eVAR_SEA_DEFEND
							CASE eVAR_AIR_CLEAR_AREA
								IF NOT IS_ENTITY_DEAD(GET_AI_PED_ENTITY_INDEX(iPed))
									IF IS_SERVER_BIT0_SET(eSERVERBITSET0_SEA_DEFEND_PEDS_SPOOKED)
										PRINTLN("[EXEC1] [CTRB_SELL] [PED], MAINTAIN_PED_BRAINS, eVAR_SEA_DEFEND - Ped ", iPed, " should moving to new state as they have been spooked")
										SET_PED_STATE(iPed, GET_PED_AI_STATE_FOR_VARIATION( iPed))
									ENDIF
								ELSE
									IF NOT IS_SERVER_BIT0_SET(eSERVERBITSET0_SEA_DEFEND_PEDS_SPOOKED)
										PRINTLN("[EXEC1] [CTRB_SELL] [PED], MAINTAIN_PED_BRAINS, eVAR_SEA_DEFEND - Ped ", iPed, " killed before spooking, setting peds as spooked")
										SET_SERVER_BIT0(eSERVERBITSET0_SEA_DEFEND_PEDS_SPOOKED)
									ENDIF
								
									SET_PED_STATE(iPed, ePEDSTATE_DEAD)
								ENDIF
							BREAK
							
							CASE eVAR_SEA_ATTACK
								IF NOT IS_ENTITY_DEAD(GET_AI_PED_PED_INDEX(iPed))
									PRINTLN("[EXEC1] [CTRB_SELL] [PED], MAINTAIN_PED_BRAINS, eVAR_SEA_ATTACK - Ped ", iPed, " should moving to new state as they have been created")
									SET_PED_STATE(iPed, GET_PED_AI_STATE_FOR_VARIATION( iPed))
								ELSE
									SET_PED_STATE(iPed, ePEDSTATE_DEAD)
								ENDIF
							BREAK
							
							CASE eVAR_LAND_ATTACK
							CASE eVAR_LAND_DEFEND
								IF NOT IS_ENTITY_DEAD(GET_AI_PED_PED_INDEX(iPed))
									PRINTLN("[EXEC1] [CTRB_SELL] [PED], MAINTAIN_PED_BRAINS, eVAR_LAND_ATTACK / eVAR_LAND_DEFEND - Ped ", iPed, " should moving to new state as they have been created")
									SET_PED_STATE(iPed, GET_PED_AI_STATE_FOR_VARIATION( iPed))
								ELSE
									SET_PED_STATE(iPed, ePEDSTATE_DEAD)
								ENDIF
							BREAK
							
							CASE eVAR_AIR_ATTACK
								IF NOT IS_ENTITY_DEAD(GET_AI_PED_PED_INDEX(iPed))
									IF IS_BIT_SET(serverBD.iAirAttackHeliSpooked, GET_NET_VEHICLE_PED_SHOULD_BE_IN_AS_INT( iPed))
										PRINTLN("[EXEC1] [CTRB_SELL] [PED], MAINTAIN_PED_BRAINS, eVAR_AIR_ATTACK - Ped ", iPed, " should moving to new state as they have been created")
										SET_PED_STATE(iPed, GET_PED_AI_STATE_FOR_VARIATION( iPed))
									ENDIF
								ELSE
									SET_PED_STATE(iPed, ePEDSTATE_DEAD)
								ENDIF
							BREAK
						ENDSWITCH
					ENDIF
				BREAK
				
				// Chase any vehicles/people with contraband
				CASE ePEDSTATE_CHASE_CONTRABAND
					IF DOES_AI_PED_EXIST(iPed)
						IF IS_ENTITY_DEAD(GET_AI_PED_ENTITY_INDEX(iPed))
							PRINTLN("[EXEC1] [CTRB_SELL] [PED], MAINTAIN_PED_BRAINS - Ped ", iPed, " has died, moving to dead ped state")
							SET_PED_STATE(iPed, ePEDSTATE_DEAD)
						ELSE
							IF bSEA_ATTACK()
							OR bLAND_ATTACK()
							//OR bAIR_ATTACK()
								HANDLE_PED_DISTANCE_FROM_SELL_VEH_SERVER(iPed)
							ENDIF
							
							IF IS_BIT_SET(serverBD.iAiPedShouldFleeBitset, iPed)
								SET_PED_STATE(iPed, ePEDSTATE_FLEE_DROP_OFF)
							ENDIF
							
							IF bAIR_ATTACK()
								IF iDROPPED_OFF_COUNT() = iDROP_TARGET()
								OR GET_MODE_STATE() > eMODESTATE_RUN
									SET_PED_STATE(iPed, ePEDSTATE_FLEE_ON_FOOT)
								ENDIF
							ENDIF
//							IF GET_MODE_STATE() > eMODESTATE_RUN
//								PRINTLN("[EXEC1] [CTRB_SELL] [PED], MAINTAIN_PED_BRAINS - Ped ", iPed, " moving to release to ambient population state")
//								SET_PED_STATE(iPed, ePEDSTATE_RELEASE_TO_AMBIENT_POPULATION)
//							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE ePEDSTATE_CHASE_CONTRABAND_AND_ATTACK
					IF DOES_AI_PED_EXIST(iPed)
						IF IS_ENTITY_DEAD(GET_AI_PED_ENTITY_INDEX(iPed))
							PRINTLN("[EXEC1] [CTRB_SELL] [PED], MAINTAIN_PED_BRAINS - Ped ", iPed, " has died, moving to dead ped state")
							SET_PED_STATE(iPed, ePEDSTATE_DEAD)
						ELSE
							IF bSEA_ATTACK()
							OR bLAND_ATTACK()
							//OR bAIR_ATTACK()
								HANDLE_PED_DISTANCE_FROM_SELL_VEH_SERVER(iPed)
							ENDIF
							
							IF bAIR_ATTACK()
								IF HAS_ALL_CONTRABAND_BEEN_DROPPED_OFF()
									PRINTLN("[EXEC1] [CTRB_SELL] [PED], MAINTAIN_PED_BRAINS - Ped ", iPed, " All contraband delivered, moving to flee")
									SET_PED_STATE(iPed, ePEDSTATE_FLEE_IN_VEH)
								ENDIF
							ENDIF
							
							IF IS_BIT_SET(serverBD.iAiPedShouldFleeBitset, iPed)
								SET_PED_STATE(iPed, ePEDSTATE_FLEE_DROP_OFF)
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				// Attack enemies nearby
				CASE ePEDSTATE_ATTACK_HATED_TARGETS
					IF DOES_AI_PED_EXIST(iPed)
						IF IS_ENTITY_DEAD(GET_AI_PED_ENTITY_INDEX(iPed))
							PRINTLN("[EXEC1] [CTRB_SELL] [PED], MAINTAIN_PED_BRAINS - Ped ", iPed, " has died, moving to dead ped state")
							SET_PED_STATE(iPed, ePEDSTATE_DEAD)
						ELSE
							IF bSEA_ATTACK()
							OR bLAND_ATTACK()
							OR bAIR_ATTACK()
								HANDLE_PED_DISTANCE_FROM_SELL_VEH_SERVER(iPed)
							ENDIF
							
							IF bAIR_ATTACK()
								IF iDROPPED_OFF_COUNT() = iDROP_TARGET()
								OR GET_MODE_STATE() > eMODESTATE_RUN
									SET_PED_STATE(iPed, ePEDSTATE_FLEE_IN_VEH)
								ELSE
									//-- If the pilot is attacking any hated target and a contraband vehicle comes close, fight the contraband vehicle instead
									IF IS_AI_PED_DRIVER_OF_NON_CONTRABAND_VEHICLE(iPed)
										IF IS_ANY_CONTRABAND_VEHICLE_NEAR_AI_PED(iPed, iNearestCOntraVeh)
											PRINTLN("[EXEC1] [CTRB_SELL] [PED], MAINTAIN_PED_BRAINS - Ped ", iPed, " IS_ANY_CONTRABAND_VEHICLE_NEAR_AI_PED near veh ", iNearestCOntraVeh)
											serverBD.iRandVehToAttack = iNearestCOntraVeh
											SET_PED_STATE(iPed, ePEDSTATE_CHASE_CONTRABAND_AND_ATTACK)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							IF bLAND_DEFEND()
								iVehForPed = GET_NET_VEHICLE_PED_SHOULD_BE_IN_AS_INT(iPed)
								IF iVehForPed > -1
									IF IS_BIT_SET(serverBD.iGangChaseStuckBitset, iVehForPed)	
										IF IS_PED_IN_ANY_VEHICLE(GET_AI_PED_PED_INDEX(iPed))
											PRINTLN("[EXEC1] [CTRB_SELL] [PED], MAINTAIN_PED_BRAINS - Ped ", iPed, " gang chase veh stuck bit is set, veh iVehForPed")
											SET_PED_STATE(iPed, ePEDSTATE_EXIT_VEH)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							
//							IF GET_MODE_STATE() > eMODESTATE_RUN
//								PRINTLN("[EXEC1] [CTRB_SELL] [PED], MAINTAIN_PED_BRAINS - Ped ", iPed, " moving to release to ambient population state")
//								SET_PED_STATE(iPed, ePEDSTATE_RELEASE_TO_AMBIENT_POPULATION)
//							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				// Peds run away
				CASE ePEDSTATE_FLEE_ON_FOOT
				BREAK
				
				CASE ePEDSTATE_FLEE_IN_VEH
				BREAK
				
				CASE ePEDSTATE_EXIT_VEH
				
					IF bLAND_DEFEND()
						IF DOES_AI_PED_EXIST(iPed)
							IF NOT IS_ENTITY_DEAD(GET_AI_PED_ENTITY_INDEX(iPed))
								IF NOT IS_PED_IN_ANY_VEHICLE(GET_AI_PED_PED_INDEX(iPed))
									SET_PED_STATE(iPed, ePEDSTATE_ATTACK_HATED_TARGETS)
									PRINTLN("[EXEC1] [CTRB_SELL] [PED], MAINTAIN_PED_BRAINS - Ped ", iPed, " CASE ePEDSTATE_EXIT_VEH, no longer in a veh")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
				BREAK
				// Avoid getting too close to drop-off
				CASE ePEDSTATE_FLEE_DROP_OFF
					IF DOES_AI_PED_EXIST(iPed)
						IF IS_ENTITY_DEAD(GET_AI_PED_ENTITY_INDEX(iPed))
							PRINTLN("[EXEC1] [CTRB_SELL] [PED], MAINTAIN_PED_BRAINS - Ped ", iPed, " has died, moving to dead ped state")
							SET_PED_STATE(iPed, ePEDSTATE_DEAD)
						ELSE
							IF GET_MODE_STATE() > eMODESTATE_RUN
								PRINTLN("[EXEC1] [CTRB_SELL] [PED], MAINTAIN_PED_BRAINS - Ped ", iPed, " moving to release to ambient population state")
								SET_PED_STATE(iPed, ePEDSTATE_RELEASE_TO_AMBIENT_POPULATION)
							ENDIF
							
							HANDLE_PED_DISTANCE_FROM_SELL_VEH_SERVER(iPed)
						ENDIF
					ENDIF
				BREAK
				
				// Peds drive to a specified location
				CASE ePEDSTATE_DRIVE_TO_POINT
				BREAK
				
				// Peds parachute when falling
				CASE ePEDSTATE_PARACHUTE
				BREAK
				
				// Move here when dead
				CASE ePEDSTATE_DEAD
				BREAK
			
				// Reset the peds here	
				CASE ePEDSTATE_RESET
				BREAK
			ENDSWITCH
		ENDREPEAT
	ENDIF
ENDPROC

FUNC PED_INDEX GET_NEAREST_PED_TO_PLAYER(BOOL bIncludeDeadPeds = FALSE)
	INT i
	FLOAT fClosestPed = 9999.99
	FLOAT fTempDist
	PED_INDEX pedToReturn = INT_TO_NATIVE(PED_INDEX, -1)

	IF DOES_VARIATION_HAVE_AI_PEDS()
		REPEAT GET_NUM_PEDS_FOR_VARIATION() i
			IF NOT IS_PED_INJURED(GET_AI_PED_PED_INDEX(i))
			OR bIncludeDeadPeds
				fTempDist = GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), GET_AI_PED_PED_INDEX(i))
				IF fTempDist < fClosestPed
					fClosestPed = fTempDist
					pedToReturn = GET_AI_PED_PED_INDEX(i)
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	RETURN pedToReturn
ENDFUNC


/// PURPOSE:
///    Add all conditions for spooking/alerting AI peds here
PROC MAINTAIN_CLIENT_SPOOK_CHECKS()
	IF DOES_VARIATION_HAVE_AI_PEDS()
	AND NOT IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), eCLIENTBITSET0_SEA_DEFEND_SPOOKED_PEDS)
		INT i
		FLOAT fDistance 
		FLOAT fDistance0 = 30
		FLOAT fDistance1 = 50
		FLOAT fDistance2 = 100
		PED_INDEX closestPed
		SWITCH GET_SELL_VAR()
			CASE eVAR_SEA_DEFEND
			CASE eVAR_AIR_CLEAR_AREA
				REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[i])
						IF IS_SELL_VEH_OK(i)
							IF GET_SELL_VAR() = eVAR_AIR_CLEAR_AREA
								fDistance  = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_SELL_DROP_COORDS(GET_SELL_VAR(), 0, serverBD.iRoute, i))
								fDistance0 = 120
								fDistance1 = 150
								fDistance2 = 200
							ELSE
								IF DOES_VARIATION_HAVE_AI_PEDS()
								AND bSEA_DEFEND()
									closestPed = GET_NEAREST_PED_TO_PLAYER()
									IF NATIVE_TO_INT(closestPed) != -1
										fDistance  = GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), closestPed)
									ELSE
										fDistance  = GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), VEH_ID(i))
									ENDIF
								ELSE
									fDistance  = GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), VEH_ID(i))
								ENDIF
							ENDIF
							
							IF fDistance < fDistance0
							OR HAS_AIR_CLEAR_AREA_PLANE_LANDED(i)
								PRINTLN("[EXEC1] [CTRB_SELL] [PLAYER], MAINTAIN_CLIENT_SPOOK_CHECKS - Local player is within 30m of sell veh, spooking peds")
								SET_CLIENT_BIT0(eCLIENTBITSET0_SEA_DEFEND_SPOOKED_PEDS)
							ELIF fDistance < fDistance1
								IF IS_PED_SHOOTING(PLAYER_PED_ID())
									PRINTLN("[EXEC1] [CTRB_SELL] [PLAYER], MAINTAIN_CLIENT_SPOOK_CHECKS - Local player is within 50m of sell veh and is shooting, spooking peds")
									SET_CLIENT_BIT0(eCLIENTBITSET0_SEA_DEFEND_SPOOKED_PEDS)
								ENDIF	
							ELIF fDistance < fDistance2
								IF bSEA_DEFEND()
									IF NOT IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), eCLIENTBITSET0_SEA_DEFEND_WITHIN_RANGE)
										PRINTLN("[EXEC1] [CTRB_SELL], MAINTAIN_CLIENT_SPOOK_CHECKS - Player is within 100m of attackers")
										SET_CLIENT_BIT0(eCLIENTBITSET0_SEA_DEFEND_WITHIN_RANGE)
									ENDIF
								ENDIF
								
								IF IS_PED_SHOOTING(PLAYER_PED_ID())
								AND NOT IS_PED_CURRENT_WEAPON_SILENCED(PLAYER_PED_ID())
									PRINTLN("[EXEC1] [CTRB_SELL] [PLAYER], MAINTAIN_CLIENT_SPOOK_CHECKS - Local player is within 100m of sell veh and is shooting a non-silenced weapon, spooking peds")
									SET_CLIENT_BIT0(eCLIENTBITSET0_SEA_DEFEND_SPOOKED_PEDS)
								ENDIF	
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
			BREAK
			
			CASE eVAR_AIR_ATTACK
				INT iVeh
				FLOAT fDist
				REPEAT ciMAX_CONTRABAND_PED_VEHICLES iVeh
					IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iSpookedAirAttackHeliBitset, iVeh)
					AND NOT IS_BIT_SET(serverBD.iAirAttackHeliSpooked, iVeh)	
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPedVeh[iVeh].netIndex)
							IF IS_NET_VEHICLE_DRIVEABLE(serverBD.sPedVeh[iVeh].netIndex)
								fDist = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_ENTITY_COORDS(NET_TO_VEH(serverBD.sPedVeh[iVeh].netIndex)))
								
								IF (IS_PLAYER_IN_ANY_SELL_VEH() AND fDist < 600.0)
								OR (fDist < 200.0)
									SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iSpookedAirAttackHeliBitset, iVeh)
									PRINTLN("[EXEC1] [CTRB_SELL] [PLAYER], MAINTAIN_CLIENT_SPOOK_CHECKS I have spooked AI veh ", iVeh, " fDist = ", fDist)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
			BREAK
		ENDSWITCH
	ENDIF
	
	
ENDPROC

// Ped Creation

FUNC BOOL CREATE_AI_PED( INT iPed, BOOL bInitialSpawn = FALSE)
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_15 tl15Name
	#ENDIF
	IF REQUEST_LOAD_MODEL(GET_PED_MODEL_FOR_VARIATION( iPed))	
		IF NOT DOES_AI_PED_EXIST(iPed)
			IF SHOULD_AI_PED_SPAWN_IN_VEHICLE( iPed)
				// Create peds in vehicle logic here
				NETWORK_INDEX vehToEnter
				vehToEnter = GET_NET_VEHICLE_PED_SHOULD_BE_IN( iPed)
				
				VEHICLE_SEAT thisSeat
				thisSeat = GET_VEHICLE_SEAT_PED_SHOULD_BE_IN( iPed)
				
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(vehToEnter)
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(vehToEnter)
        			OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(vehToEnter) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
						IF TAKE_CONTROL_OF_NET_ID(vehToEnter)
							IF CREATE_NET_PED_IN_VEHICLE(serverBD.sPed[iPed].netIndex, vehToEnter, PEDTYPE_MISSION, GET_PED_MODEL_FOR_VARIATION( iPed), thisSeat)
								PRINTLN("[EXEC1] [CTRB_SELL] [PED] CREATE_AI_PED, Ped ", iPed, " created in vehicle ", NATIVE_TO_INT(vehToEnter), " in seat ", GET_VEHICLE_SEAT_NAME(thisSeat))
								CLEAR_BIT(serverBD.iAiPedShouldDieBitset, iPed)
								serverBD.sPed[iPed].vSpawnCoord = <<0.0, 0.0, 0.0>>
								serverBD.sPed[iPed].fSpawnHeading = 0.0
								RESET_NET_TIMER(serverBD.sPed[iPed].stOutOfRangeTimer)
								
								#IF IS_DEBUG_BUILD
								tl15Name = "PED " 
								tl15Name += iPed
								SET_PED_NAME_DEBUG(NET_TO_PED(serverBD.sPed[iPed].netIndex), tl15Name)
								#ENDIF
								
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF bInitialSpawn
					IF CREATE_NET_PED(serverBD.sPed[iPed].netIndex, PEDTYPE_MISSION, GET_PED_MODEL_FOR_VARIATION( iPed), GET_AI_PED_INITIAL_SPAWN_COORD( iPed), GET_AI_PED_INITIAL_SPAWN_HEADING( iPed))
						CLEAR_BIT(serverBD.iAiPedShouldDieBitset, iPed)
						serverBD.sPed[iPed].vSpawnCoord = <<0.0, 0.0, 0.0>>
						serverBD.sPed[iPed].fSpawnHeading = 0.0
						RESET_NET_TIMER(serverBD.sPed[iPed].stOutOfRangeTimer)
						SET_ATTRIBUTES_FOR_GUARD( GET_AI_PED_PED_INDEX(iPed))
						
						#IF IS_DEBUG_BUILD
						tl15Name = "PED " 
						tl15Name += iPed
						SET_PED_NAME_DEBUG(NET_TO_PED(serverBD.sPed[iPed].netIndex), tl15Name)
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			SET_MODEL_AS_NO_LONGER_NEEDED(GET_PED_MODEL_FOR_VARIATION( iPed))
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CREATE_START_PEDS()
	// If no peds in this variation, return true
	IF NOT DOES_VARIATION_HAVE_AI_PEDS()
		RETURN TRUE
	ENDIF
	
	IF NOT DOES_VARIATION_SPAWN_PEDS_AT_START()
		RETURN TRUE
	ENDIF
	
	INT i
	REPEAT GET_NUM_PEDS_FOR_VARIATION() i
		IF NOT CREATE_AI_PED( i, TRUE)
			PRINTLN("[EXEC1] [CTRB_SELL] [PED], CREATE_START_PEDS - waiting for ped ", i, " to be created")
			RETURN FALSE
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

//╔═════════════════════════════════════════════════════════════════════════════╗
//║				AI PED VEHICLES													║

FUNC BOOL DELETE_VEHICLES()
	INT i
	REPEAT GET_NUM_ATTACK_VEHICLES_FOR_VARIATION() i
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPedVeh[i].netIndex)
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sPedVeh[i].netIndex)
			OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.sPedVeh[i].netIndex) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
			OR (IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.sPedVeh[i].netIndex) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
				IF TAKE_CONTROL_OF_NET_ID(serverBD.sPedVeh[i].netIndex)
					IF NOT bLAND_DEFEND()
					AND NOT bLAND_ATTACK()
						NETWORK_FADE_OUT_ENTITY(NET_TO_ENT(serverBD.sPedVeh[i].netIndex), TRUE, TRUE)
					ENDIF
					CLEANUP_NET_ID(serverBD.sPedVeh[i].netIndex) // For land defend, don't want to delete the vehicles as they might be right in front of the player
					RESET_NET_TIMER(serverBD.stVehSpawnDelayTimer[i])
					SET_VEHICLE_STATE(i, eVEHICLESTATE_CREATE)
					PRINTLN("[EXEC1] [CTRB_SELL] [PED_VEHICLE], DELETE_AMBUSH_VEHICLES - Vehicle ", i, " deleted")
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	i = 0
	REPEAT GET_NUM_ATTACK_VEHICLES_FOR_VARIATION() i
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPedVeh[i].netIndex)
			PRINTLN("[EXEC1] [CTRB_SELL] [PED_VEHICLE], DELETE_AMBUSH_VEHICLES - Waiting for vehicle ", i, " to be deleted")
			RETURN FALSE
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC


/// PURPOSE:
///    Direction from the drop off that the Land Defend ai should come from 
FUNC FLOAT GET_CONRABAND_DROPOFF_AI_SPAWN_DIRECTION_AS_HEADING(INT iDrop)
	
	FLOAT fHeading
	SWITCH iDrop
		CASE ciD_LSIA				fHeading = 317.5908 	BREAK
		CASE ciD_Vespucci			fHeading = 65.8782 		BREAK
		CASE ciD_Rockford_Plaza		fHeading = 79.6437 		BREAK
		CASE ciD_Hawick				fHeading = 339.1620 	BREAK
		CASE ciD_Greenwich			fHeading = 300.5366 	BREAK
		CASE ciD_Rockford_Hills		fHeading = 26.3732 		BREAK
		CASE ciD_Pillbox_Hill		fHeading = 12.9312  	BREAK
		CASE ciD_Davis				fHeading = 306.5281 	BREAK
		CASE ciD_Terminal			fHeading = 70.6314 		BREAK
		CASE ciD_Chumash			fHeading = 199.4232 	BREAK
		CASE ciD_Banham_Canyon		fHeading = 232.5371 	BREAK
		CASE ciD_Pacific_Bluffs		fHeading = 217.9965 	BREAK
		CASE ciD_Murrieta_Heights	fHeading = 97.8555 		BREAK
		CASE ciD_Cypress_Flats		fHeading = 254.7614 	BREAK
		CASE ciD_Downtown_Vinewood	fHeading = 149.1127 	BREAK
		CASE ciD_Rehab				fHeading = 299.9396 	BREAK
		CASE ciGove					fHeading = 143.5423 	BREAK
		CASE ciLaFuente				fHeading = 153.6289		BREAK
		CASE ciVinewood				fHeading = 327.4365 	BREAK
		CASE ciElysian				fHeading = 0.0 			BREAK	
	ENDSWITCH
	
	PRINTLN("[EXEC1] [CTRB_SELL], [CREATE_NON_CONTRABAND_VEHICLE] [GET_CONRABAND_DROPOFF_AI_SPAWN_DIRECTION_AS_HEADING] Called with iDrop = ", iDrop, " Returning ",fHeading) 
	RETURN fHeading
ENDFUNC

/// PURPOSE:
///    Converts the heading we want the AI to come from in Land Defend to a direction vector that can be passed to FIND_SPAWN_POINT_IN_DIRECTION
FUNC VECTOR GET_CONRABAND_DROPOFF_AI_SPAWN_DIRECTION_AS_VECTOR(INT iDrop)
	
	
	FLOAT fHeading = GET_CONRABAND_DROPOFF_AI_SPAWN_DIRECTION_AS_HEADING(iDrop)
	VECTOR vDir = CONVERT_ROTATION_TO_DIRECTION_VECTOR(<< 0.0, 0.0, fHeading >>)
	
	PRINTLN("[EXEC1] [CTRB_SELL], [CREATE_NON_CONTRABAND_VEHICLE] [GET_CONRABAND_DROPOFF_AI_SPAWN_DIRECTION_AS_VECTOR] Called with iDrop = ", iDrop,  "Returning ",vDir) 
	RETURN vDir
ENDFUNC

/// PURPOSE:
///   Figure out which drop-off the delivered sell vehicle is closest to (for Land Defend)    
FUNC INT GET_DROP_OFF_FOR_LAND_DEFEND_AI_SPAWN(INT iSellVeh)
	PRINTLN("[EXEC1] [CTRB_SELL], [CREATE_NON_CONTRABAND_VEHICLE] [GET_DROP_OFF_FOR_LAND_DEFEND_AI_SPAWN] Called with sell veh ", iSellVeh)
	INT i
	INT iClosest = -1
	FLOAT fClosest2 = 9999999999.0
	FLOAT fDist2
	VECTOR vCoords
	VECTOR vDropOffCoords
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[iSellVeh])
		vCoords = GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niVeh[iSellVeh]), FALSE)
		
		REPEAT ciMAX_SELL_DROP_OFFS i
			vDropOffCoords = GET_SELL_DROP_COORDS(GET_SELL_VAR(), i, serverBD.iRoute, iSellVeh)
			IF NOT IS_VECTOR_ZERO(vDropOffCoords)
				fDist2 = VDIST2(vCoords, vDropOffCoords)
				IF fDist2 < fClosest2
					fClosest2 = fDist2
					iClosest = i
				ENDIF
			ENDIF
		ENDREPEAT
	ELSE
		PRINTLN("[EXEC1] [CTRB_SELL], [CREATE_NON_CONTRABAND_VEHICLE] [GET_DROP_OFF_FOR_LAND_DEFEND_AI_SPAWN] Sell veh doesn't exist ")		
	ENDIF
	

	
	PRINTLN("[EXEC1] [CTRB_SELL], [CREATE_NON_CONTRABAND_VEHICLE] [GET_DROP_OFF_FOR_LAND_DEFEND_AI_SPAWN] Returning ", iClosest, " Sell veh coords = ", vCoords)
	
	RETURN iClosest
		
ENDFUNC

/// PURPOSE:
///    How far away from the specifed drop off the Land Defend AI should ideally spawn
FUNC FLOAT GET_IDEAL_SPAWN_DISTANCE_FOR_LAND_DEFEND_AI(INT iDrop)
	FLOAT fDist = 225.0
	SWITCH iDrop
		CASE 16 	fDist = 150.0	BREAK
		CASE 19 	fDist = 150.0	BREAK
		
	ENDSWITCH
	
	RETURN fDist
ENDFUNC



FUNC BOOL CREATE_NON_CONTRABAND_VEHICLE( INT iVehicle, MODEL_NAMES model)
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_15 tl15Name
	#ENDIF
	SWITCH GET_SELL_VAR()
	
		// Vehicle spawning logic for Sea attack Variation
		CASE eVAR_SEA_ATTACK
			IF serverBD.sSpawnStruct.iCurrentPedSearchingForSpawnCoords = (-1)
				IF serverBD.sSpawnStruct.iCurrentNonContrabandVehicleSearchingForSpawnCoords = -1
				OR serverBD.sSpawnStruct.iCurrentNonContrabandVehicleSearchingForSpawnCoords = iVehicle
					
					serverBD.sSpawnStruct.iCurrentNonContrabandVehicleSearchingForSpawnCoords = iVehicle
					
					IF IS_VECTOR_ZERO(serverBD.sSpawnStruct.vSpawnNonContrabandVehicleCoords)	
						
						IF IS_VECTOR_ZERO(serverBD.sSpawnStruct.vSpawnDirection)
							serverBD.sSpawnStruct.vSpawnDirection = GET_ENTITY_FORWARD_VECTOR(VEH_ID(0))
							PRINTLN("[EXEC1] [CTRB_SELL], CREATE_NON_CONTRABAND_VEHICLE - serverBD.vSpawnDirection = GET_CONTRABAND_FORWARD_VECTOR(0) = ", serverBD.sSpawnStruct.vSpawnDirection)
						ELSE
							IF IS_SERVER_BIT0_SET(eSERVERBITSET0_SEA_ATTACK_HELI_VARIANT)
								IF sLocalData.iNumFailedAttempts > 5
									IF sLocalData.bDoSightCheck = TRUE
										sLocalData.bDoSightCheck = FALSE
									ENDIF
								ENDIF
								
								GET_HELI_CREATION_POINT(VEH_ID(0), serverBD.sSpawnStruct.vSpawnNonContrabandVehicleCoords, serverBD.sSpawnStruct.fSpawnNonContrabandVehicleHeading)
								
								IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(serverBD.sSpawnStruct.vSpawnNonContrabandVehicleCoords, 3, DEFAULT, DEFAULT, DEFAULT, sLocalData.bDoSightCheck, sLocalData.bDoSightCheck)
									serverBD.sSpawnStruct.vSpawnNonContrabandVehicleCoords.z = GET_HELI_CREATION_HEIGHT()
									PRINTLN("[EXEC1] [CTRB_SELL], CREATE_NON_CONTRABAND_VEHICLE, Heli Variation - serverBD.vSpawnNonContrabandVehicleCoords = ", serverBD.sSpawnStruct.vSpawnNonContrabandVehicleCoords)
									PRINTLN("[EXEC1] [CTRB_SELL], CREATE_NON_CONTRABAND_VEHICLE, Heli Variation - serverBD.fSpawnNonContrabandVehicleHeading = ", serverBD.sSpawnStruct.fSpawnNonContrabandVehicleHeading)
								ELSE
									PRINTLN("[EXEC1] [CTRB_SELL], CREATE_NON_CONTRABAND_VEHICLE, Heli Variation - Point not ok for net entity creation")
									serverBD.sSpawnStruct.vSpawnNonContrabandVehicleCoords = << 0.0, 0.0, 0.0 >>
									IF sLocalData.iNumFailedAttempts < 5
										sLocalData.iNumFailedAttempts++
									ELSE
										sLocalData.iNumFailedAttempts2++
									ENDIF
								ENDIF
							ELSE
								IF sLocalData.iNumFailedAttempts >= 5
									IF sLocalData.bDoSightCheck = TRUE
										sLocalData.bDoSightCheck = FALSE
									ENDIF
								
									IF sLocalData.iNumFailedAttempts2 >= 5
										IF NOT IS_SERVER_BIT0_SET(eSERVERBITSET0_SEA_ATTACK_HELI_VARIANT)
											PRINTLN("[EXEC1] [CTRB_SELL], CREATE_NON_CONTRABAND_VEHICLE, Jetski Variation - iNumFailedAttempts2 > 5, switching to helicopter creation")
											SET_SERVER_BIT0(eSERVERBITSET0_SEA_ATTACK_HELI_VARIANT)
											serverBD.sSpawnStruct.vSpawnNonContrabandVehicleCoords = << 0.0, 0.0, 0.0 >>
											sLocalData.iNumFailedAttempts = 0
											sLocalData.iNumFailedAttempts2 = 0
										ENDIF
									ELSE
										PRINTLN("[EXEC1] [CTRB_SELL], CREATE_NON_CONTRABAND_VEHICLE, Jetski Variation - iNumFailedAttempts > 5, searching around vehicle and not 100m ahead")
										GET_BOAT_CREATION_POINT(VEH_ID(0), serverBD.sSpawnStruct.vSpawnNonContrabandVehicleCoords, serverBD.sSpawnStruct.fSpawnNonContrabandVehicleHeading, TRUE)
									ENDIF
								ELSE
									GET_BOAT_CREATION_POINT(VEH_ID(0), serverBD.sSpawnStruct.vSpawnNonContrabandVehicleCoords, serverBD.sSpawnStruct.fSpawnNonContrabandVehicleHeading)
								ENDIF
								
								IF TEST_PROBE_AGAINST_WATER(serverBD.sSpawnStruct.vSpawnNonContrabandVehicleCoords+<<0, 0, 2>>, serverBD.sSpawnStruct.vSpawnNonContrabandVehicleCoords-<<0, 0, ciBOAT_CREATION_HEIGHT>>, serverBD.sSpawnStruct.vSpawnNonContrabandVehicleCoords)
									IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(serverBD.sSpawnStruct.vSpawnNonContrabandVehicleCoords, 3, DEFAULT, DEFAULT, DEFAULT, sLocalData.bDoSightCheck, sLocalData.bDoSightCheck)
										IF IS_LOCATION_ABOVE_GROUND(serverBD.sSpawnStruct.vSpawnNonContrabandVehicleCoords)
											PRINTLN("[EXEC1] [CTRB_SELL], CREATE_NON_CONTRABAND_VEHICLE, Jetski Variation - serverBD.vSpawnNonContrabandVehicleCoords = ", serverBD.sSpawnStruct.vSpawnNonContrabandVehicleCoords)
											PRINTLN("[EXEC1] [CTRB_SELL], CREATE_NON_CONTRABAND_VEHICLE, Jetski Variation - serverBD.fSpawnNonContrabandVehicleHeading = ", serverBD.sSpawnStruct.fSpawnNonContrabandVehicleHeading)
										ELSE
											PRINTLN("[EXEC1] [CTRB_SELL], CREATE_NON_CONTRABAND_VEHICLE, Jetski Variation - Point is below ground")
											serverBD.sSpawnStruct.vSpawnNonContrabandVehicleCoords = << 0.0, 0.0, 0.0 >>
											
											IF sLocalData.iNumFailedAttempts < 5
												sLocalData.iNumFailedAttempts++
											ELSE
												sLocalData.iNumFailedAttempts2++
											ENDIF
										ENDIF
									ELSE
										PRINTLN("[EXEC1] [CTRB_SELL], CREATE_NON_CONTRABAND_VEHICLE, Jetski Variation - Point not ok for net entity creation")
										serverBD.sSpawnStruct.vSpawnNonContrabandVehicleCoords = << 0.0, 0.0, 0.0 >>
										
										IF sLocalData.iNumFailedAttempts < 5
											sLocalData.iNumFailedAttempts++
										ELSE
											sLocalData.iNumFailedAttempts2++
										ENDIF
									ENDIF
								ELSE
									PRINTLN("[EXEC1] [CTRB_SELL], CREATE_NON_CONTRABAND_VEHICLE, Jetski Variation - test probe against water failed")
									serverBD.sSpawnStruct.vSpawnNonContrabandVehicleCoords = << 0.0, 0.0, 0.0 >>
								
									IF sLocalData.iNumFailedAttempts < 5
										sLocalData.iNumFailedAttempts++
									ELSE
										sLocalData.iNumFailedAttempts2++
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF CREATE_NET_VEHICLE(serverBD.sPedVeh[iVehicle].netIndex, model, serverBD.sSpawnStruct.vSpawnNonContrabandVehicleCoords, serverBD.sSpawnStruct.fSpawnNonContrabandVehicleHeading, TRUE, TRUE, TRUE, FALSE, TRUE)
							SET_AMBUSH_VEHICLE_SPAWN_ATTRIBUTES(iVehicle)
							SET_MODEL_AS_NO_LONGER_NEEDED(model)
							serverBD.sSpawnStruct.iCurrentNonContrabandVehicleSearchingForSpawnCoords = (-1)
							serverBD.sSpawnStruct.vSpawnNonContrabandVehicleCoords = << 0.0, 0.0, 0.0 >>
							serverBD.sSpawnStruct.fSpawnNonContrabandVehicleHeading = 0.0
							serverBD.sSpawnStruct.iAmbushGetNonContrabandCoordsAttempts = 0
							serverBD.sSpawnStruct.iGetSpawnNonContrabandVehicleCoordsStage = 0
							sLocalData.iNumFailedAttempts = 0
							sLocalData.bDoSightCheck = TRUE
							START_NET_TIMER(serverBD.stVehSpawnDelayTimer[iVehicle])
							
							IF IS_SERVER_BIT0_SET(eSERVERBITSET0_SEA_ATTACK_HELI_VARIANT)
								serverBD.fNumWavesSpawned += 1.0
								PRINTLN("[EXEC1] [CTRB_SELL] [PED_VEHICLES], [HELI SPAWN] serverBD.fNumWavesSpawned = ", serverBD.fNumWavesSpawned)
							ELSE
								serverBD.fNumWavesSpawned += 0.5
								PRINTLN("[EXEC1] [CTRB_SELL] [PED_VEHICLES], [JETSKI SPAWN] serverBD.fNumWavesSpawned = ", serverBD.fNumWavesSpawned)
							ENDIF
							
							#IF IS_DEBUG_BUILD
							tl15Name = "VEH " 
							tl15Name += iVehicle
							SET_VEHICLE_NAME_DEBUG(NET_TO_VEH(serverBD.sPedVeh[iVehicle].netIndex), tl15Name)
							#ENDIF
							
							PRINTLN("[EXEC1] [CTRB_SELL], CREATE_NON_CONTRABAND_VEHICLE - created non contraband vehicle ", iVehicle)
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		// ADD SPAWNING LOGIC FOR OTHER VARIATIONS AS NEEDED HERE
		
		CASE eVAR_LAND_ATTACK
			IF serverBd.sSpawnStruct.iCurrentPedSearchingForSpawnCoords = (-1)
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[serverBD.iRandVehToAttack])
					IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niVeh[serverBD.iRandVehToAttack])
						IF serverBD.sSpawnStruct.iCurrentNonContrabandVehicleSearchingForSpawnCoords = -1
						OR serverBD.sSpawnStruct.iCurrentNonContrabandVehicleSearchingForSpawnCoords = iVehicle
							
							serverBd.sSpawnStruct.iCurrentNonContrabandVehicleSearchingForSpawnCoords = iVehicle
							
							IF IS_VECTOR_ZERO(serverBd.sSpawnStruct.vSpawnNonContrabandVehicleCoords)	
								
								IF IS_VECTOR_ZERO(serverBD.sSpawnStruct.vSpawnDirection)
									serverBD.sSpawnStruct.vSpawnDirection = GET_ENTITY_FORWARD_VECTOR(NET_TO_VEH(serverBD.niVeh[serverBD.iRandVehToAttack]))
									PRINTLN("[EXEC1] - CREATE_NON_CONTRABAND_VEHICLE - serverBD.vSpawnDirection = GET_ENTITY_FORWARD_VECTOR = ", serverBD.sSpawnStruct.vSpawnDirection, " serverBD.iRandVehToAttack = ", serverBD.iRandVehToAttack)
								ELSE
									FLOAT fSpawnDistance
									
									IF (serverBD.sSpawnStruct.vSpawnDirection.z >= 0.0 AND serverBD.sSpawnStruct.vSpawnDirection.z < 90.0)
									OR (serverBD.sSpawnStruct.vSpawnDirection.z >= 270.0 AND serverBD.sSpawnStruct.vSpawnDirection.z < 360.0)
										fSpawnDistance = 220
									ELSE
										fSpawnDistance = 120
									ENDIF
									BOOL bSpawn
									FLOAT fSpanwDistFromContra
									VECTOR vOffsetFromContra
									
									bSpawn = TRUE 
									IF FIND_SPAWN_POINT_IN_DIRECTION(GET_ENTITY_COORDS(NET_TO_ENT(serverBD.niVeh[serverBD.iRandVehToAttack])), serverBD.sSpawnStruct.vSpawnDirection, fSpawnDistance, serverBd.sSpawnStruct.vSpawnNonContrabandVehicleCoords)
										fSpanwDistFromContra = GET_DISTANCE_BETWEEN_COORDS(serverBd.sSpawnStruct.vSpawnNonContrabandVehicleCoords, GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niVeh[serverBD.iRandVehToAttack]), FALSE))
										vOffsetFromContra = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(NET_TO_VEH(serverBD.niVeh[serverBD.iRandVehToAttack]), serverBd.sSpawnStruct.vSpawnNonContrabandVehicleCoords)
										
										//-- Too far from the player?
										IF fSpanwDistFromContra > 290.0
											bSpawn = FALSE
											PRINTLN("[EXEC1] [CTRB_SELL] - CREATE_NON_CONTRABAND_VEHICLE - rejecting  spawn coords too far from player ", serverBd.sSpawnStruct.vSpawnNonContrabandVehicleCoords, " dist = ", fSpanwDistFromContra)
										ENDIF
										
										//-- Location ok?
										IF NOT IS_POINT_OK_FOR_NET_ENTITY_CREATION(serverBd.sSpawnStruct.vSpawnNonContrabandVehicleCoords, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 190)
											bSpawn = FALSE
											PRINTLN("[EXEC1] [CTRB_SELL] - CREATE_NON_CONTRABAND_VEHICLE - serverBd.sSpawnStruct.vSpawnNonContrabandVehicleCoords not okay for net entity creation, setting back to zero so we search again")
										ENDIF
										
										IF ABSF(vOffsetFromContra.x) > 100.0
											bSpawn = FALSE
											PRINTLN("[EXEC1] [CTRB_SELL] - CREATE_NON_CONTRABAND_VEHICLE REJECTING COORDS ", serverBd.sSpawnStruct.vSpawnNonContrabandVehicleCoords ," As X offset from contra vehicle = ", vOffsetFromContra.x)
										ENDIF
										
										IF NOT bSpawn
											serverBd.sSpawnStruct.vSpawnNonContrabandVehicleCoords = << 0.0, 0.0, 0.0 >>
										ELSE
											serverBd.sSpawnStruct.fSpawnNonContrabandVehicleHeading = GET_HEADING_BETWEEN_VECTORS_2D(serverBd.sSpawnStruct.vSpawnNonContrabandVehicleCoords, GET_ENTITY_COORDS(NET_TO_ENT(serverBD.niVeh[serverBD.iRandVehToAttack])))
											PRINTLN("[EXEC1] [CTRB_SELL] - CREATE_NON_CONTRABAND_VEHICLE - serverBd.vSpawnNonContrabandVehicleCoords is okay for net entity creation. Dist = ",fSpanwDistFromContra, " Tried to spawn at least this far ", fSpawnDistance, " serverBD.iRandVehToAttack = ", serverBD.iRandVehToAttack)
											PRINTLN("[EXEC1] [CTRB_SELL] - CREATE_NON_CONTRABAND_VEHICLE - serverBd.fSpawnNonContrabandVehicleHeading = GET_HEADING_BETWEEN_VECTORS_2D = ", serverBd.sSpawnStruct.fSpawnNonContrabandVehicleHeading)
										
										ENDIF
										
										
									ELSE
										PRINTLN("[EXEC1] - CREATE_NON_CONTRABAND_VEHICLE - FIND_SPAWN_POINT_IN_DIRECTION = FALSE. Adding rotation to coords with ADD_ROTATION_FOR_SPAWN_CHECK")
										ADD_ROTATION_FOR_SPAWN_CHECK(serverBD.sSpawnStruct.vSpawnDirection)
									ENDIF
								ENDIF
							ENDIF
							
							IF NOT IS_VECTOR_ZERO(serverBd.sSpawnStruct.vSpawnNonContrabandVehicleCoords)	
								IF CREATE_NET_VEHICLE(serverBD.sPedVeh[iVehicle].netIndex, model, serverBd.sSpawnStruct.vSpawnNonContrabandVehicleCoords, serverBd.sSpawnStruct.fSpawnNonContrabandVehicleHeading, TRUE, TRUE, TRUE, FALSE, TRUE)
									SET_AMBUSH_VEHICLE_SPAWN_ATTRIBUTES(iVehicle)
									SET_MODEL_AS_NO_LONGER_NEEDED(model)
								//	SET_VEHICLE_STATE(iVehicle, GET_AFTER_CREATION_VEHICLE_STATE(iVehicle))
									serverBd.sSpawnStruct.iCurrentNonContrabandVehicleSearchingForSpawnCoords = (-1)
									serverBd.sSpawnStruct.vSpawnNonContrabandVehicleCoords = << 0.0, 0.0, 0.0 >>
									serverBd.sSpawnStruct.fSpawnNonContrabandVehicleHeading = 0.0
									ServerBd.sSpawnStruct.iAmbushGetNonContrabandCoordsAttempts = 0
									serverBd.sSpawnStruct.iGetSpawnNonContrabandVehicleCoordsStage = 0
									serverBD.fNumWavesSpawned += 1.0
									
									#IF IS_DEBUG_BUILD
									serverBD.iGangChaseVehCreated++
									PRINTLN("[EXEC1] - CREATE_NON_CONTRABAND_VEHICLE - created non contraband vehicle ", iVehicle, " serverBD.iGangChaseVehCreated = ", serverBD.iGangChaseVehCreated, " serverBD.fNumWavesSpawned = ", serverBD.fNumWavesSpawned, " serverBD.iRandVehToAttack = ", serverBD.iRandVehToAttack)
									#ENDIF
									
									#IF IS_DEBUG_BUILD
									tl15Name = "VEH " 
									tl15Name += iVehicle
									SET_VEHICLE_NAME_DEBUG(NET_TO_VEH(serverBD.sPedVeh[iVehicle].netIndex), tl15Name)
									#ENDIF
									RETURN TRUE
								ENDIF
							ENDIF
							
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		
		BREAK
		
		CASE eVAR_AIR_ATTACK
			IF serverBD.sSpawnStruct.iCurrentPedSearchingForSpawnCoords = (-1)
				IF serverBD.sSpawnStruct.iCurrentNonContrabandVehicleSearchingForSpawnCoords = -1
				OR serverBD.sSpawnStruct.iCurrentNonContrabandVehicleSearchingForSpawnCoords = iVehicle
					
					serverBD.sSpawnStruct.iCurrentNonContrabandVehicleSearchingForSpawnCoords = iVehicle
					
					IF IS_VECTOR_ZERO(serverBD.sSpawnStruct.vSpawnNonContrabandVehicleCoords)	
						IF IS_VECTOR_ZERO(serverBD.sSpawnStruct.vSpawnNearCoords)		
					//		serverBD.sSpawnStruct.vSpawnNearCoords =GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niVeh[serverBD.iRandVehToAttack]), FALSE)
							serverBD.sSpawnStruct.vSpawnNearCoords = GET_SELL_DROP_COORDS(eVAR_AIR_ATTACK, serverBD.iDropOffAiVehShouldUse[iVehicle], serverBD.iRoute, 0)
							serverBD.sSpawnStruct.vSpawnNearCoords.z += 50.0
						ENDIF
						
						IF NOT IS_VECTOR_ZERO(serverBD.sSpawnStruct.vSpawnNearCoords)		
							VEHICLE_SPAWN_LOCATION_PARAMS Params
							Params.fMinDistFromCoords = 0.0 
							Params.bConsiderHighways= TRUE
							Params.fMaxDistance= 50.0
							Params.bConsiderOnlyActiveNodes= FALSE
							Params.bAvoidSpawningInExclusionZones= TRUE
							Params.bCheckEntityArea = TRUE
							Params.bUseExactCoordsIfPossible = TRUE
							VECTOR vSpawnHeading
							VECTOR vTemp
							vTemp = serverBD.sSpawnStruct.vSpawnNearCoords
							vSpawnHeading = serverBD.sSpawnStruct.vSpawnNearCoords
							PRINTLN("[EXEC1] [CTRB_SELL], CREATE_NON_CONTRABAND_VEHICLE, Heli Variation Looking for spawn coords near ", serverBD.sSpawnStruct.vSpawnNearCoords)
							
							IF HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(serverBD.sSpawnStruct.vSpawnNearCoords, 
																vSpawnHeading, 
																model, 
																FALSE, 
																vTemp, 
																serverBD.sSpawnStruct.fSpawnNonContrabandVehicleHeading, 
																Params)
								
								serverBD.sSpawnStruct.vSpawnNonContrabandVehicleCoords = vTemp
							//	serverBD.sSpawnStruct.vSpawnNonContrabandVehicleCoords.z += 100.0
								
							//	IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(serverBd.sSpawnStruct.vSpawnNonContrabandVehicleCoords, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 190)
									PRINTLN("[EXEC1] [CTRB_SELL], CREATE_NON_CONTRABAND_VEHICLE, Heli Variation - serverBD.vSpawnNonContrabandVehicleCoords = ", serverBD.sSpawnStruct.vSpawnNonContrabandVehicleCoords)
									PRINTLN("[EXEC1] [CTRB_SELL], CREATE_NON_CONTRABAND_VEHICLE, Heli Variation - serverBD.fSpawnNonContrabandVehicleHeading = ", serverBD.sSpawnStruct.fSpawnNonContrabandVehicleHeading)
							//	ELSE
								//	PRINTLN("[EXEC1] [CTRB_SELL], CREATE_NON_CONTRABAND_VEHICLE, Heli Variation - Point not ok for net entity creation")
								//	serverBD.sSpawnStruct.vSpawnNonContrabandVehicleCoords = << 0.0, 0.0, 0.0 >>
								//	serverBD.sSpawnStruct.vSpawnNearCoords = << 0.0, 0.0, 0.0 >> 
							//	ENDIF
							ENDIF
						ENDIF
								
					ELSE
						IF CREATE_NET_VEHICLE(serverBD.sPedVeh[iVehicle].netIndex, model, serverBD.sSpawnStruct.vSpawnNonContrabandVehicleCoords, serverBD.sSpawnStruct.fSpawnNonContrabandVehicleHeading, TRUE, TRUE, TRUE, TRUE, FALSE)
							SET_AMBUSH_VEHICLE_SPAWN_ATTRIBUTES(iVehicle)
							SET_MODEL_AS_NO_LONGER_NEEDED(model)
							serverBD.sSpawnStruct.iCurrentNonContrabandVehicleSearchingForSpawnCoords = (-1)
							serverBD.sSpawnStruct.vSpawnNearCoords = << 0.0, 0.0, 0.0 >>
							serverBD.sSpawnStruct.vSpawnNonContrabandVehicleCoords = << 0.0, 0.0, 0.0 >>
							serverBD.sSpawnStruct.fSpawnNonContrabandVehicleHeading = 0.0
							serverBD.sSpawnStruct.iAmbushGetNonContrabandCoordsAttempts = 0
							serverBD.sSpawnStruct.iGetSpawnNonContrabandVehicleCoordsStage = 0
							sLocalData.iNumFailedAttempts = 0
							MODIFY_VEHICLE_TOP_SPEED(NET_TO_VEH(serverBD.sPedVeh[iVehicle].netIndex), 30)
							serverBD.fNumWavesSpawned += 1.0
							PRINTLN("[EXEC1] [CTRB_SELL] [PED_VEHICLES], CREATE_NON_CONTRABAND_VEHICLE eVAR_AIR_ATTACK serverBD.fNumWavesSpawned = ", serverBD.fNumWavesSpawned)
							
							
							#IF IS_DEBUG_BUILD
							tl15Name = "VEH " 
							tl15Name += iVehicle
							SET_VEHICLE_NAME_DEBUG(NET_TO_VEH(serverBD.sPedVeh[iVehicle].netIndex), tl15Name)
							#ENDIF
							
							PRINTLN("[EXEC1] [CTRB_SELL], CREATE_NON_CONTRABAND_VEHICLE - created non contraband vehicle ", iVehicle)
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE eVAR_LAND_DEFEND

			IF serverBd.sSpawnStruct.iCurrentPedSearchingForSpawnCoords = (-1)
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[serverBD.iRandVehToAttack])
					IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niVeh[serverBD.iRandVehToAttack])
						IF serverBD.sSpawnStruct.iCurrentNonContrabandVehicleSearchingForSpawnCoords = -1
						OR serverBD.sSpawnStruct.iCurrentNonContrabandVehicleSearchingForSpawnCoords = iVehicle
							
							serverBd.sSpawnStruct.iCurrentNonContrabandVehicleSearchingForSpawnCoords = iVehicle
							IF sLocalData.iNumFailedAttempts < 50
								IF IS_VECTOR_ZERO(serverBd.sSpawnStruct.vSpawnNonContrabandVehicleCoords)	
									PRINTLN("[EXEC1] - [CREATE_NON_CONTRABAND_VEHICLE] ")
									PRINTLN("[EXEC1] - [CREATE_NON_CONTRABAND_VEHICLE] ")
									PRINTLN("[EXEC1] - [CREATE_NON_CONTRABAND_VEHICLE] ")
									PRINTLN("[EXEC1] - [CREATE_NON_CONTRABAND_VEHICLE] ")
									PRINTLN("[EXEC1] - [CREATE_NON_CONTRABAND_VEHICLE] ********************** FINDING NEW COORD **********************")
									PRINTLN("[EXEC1] - [CREATE_NON_CONTRABAND_VEHICLE] ")
										
									FLOAT fSpawnDistance
									INT iDropOffToSpawnNear 
									FLOAT fSpawnHeading
									
									iDropOffToSpawnNear = GET_DROP_OFF_FOR_LAND_DEFEND_AI_SPAWN(serverBD.iRandVehToAttack) 
									fSpawnDistance = GET_IDEAL_SPAWN_DISTANCE_FOR_LAND_DEFEND_AI(iDropOffToSpawnNear)
									
									IF IS_VECTOR_ZERO(serverBD.sSpawnStruct.vSpawnDirection)
										
										IF iDropOffToSpawnNear >= 0
											serverBD.sSpawnStruct.vSpawnDirection = GET_CONRABAND_DROPOFF_AI_SPAWN_DIRECTION_AS_VECTOR(iDropOffToSpawnNear)
											fSpawnHeading = GET_CONRABAND_DROPOFF_AI_SPAWN_DIRECTION_AS_HEADING(iDropOffToSpawnNear)
											serverBD.sSpawnStruct.vSpawnDirAsHeading = << 0.0, 0.0, fSpawnHeading>>
											PRINTLN("[EXEC1] - [CREATE_NON_CONTRABAND_VEHICLE] - vSpawnDirAsHeading = ", serverBD.sSpawnStruct.vSpawnDirAsHeading , " fSpawnDistance = ", fSpawnDistance, "serverBD.vSpawnDirection = ", serverBD.sSpawnStruct.vSpawnDirection, " serverBD.iRandVehToAttack = ", serverBD.iRandVehToAttack, " serverBD.iClosestDropOff[serverBD.iRandVehToAttack] = ", serverBD.iClosestDropOff[serverBD.iRandVehToAttack])
										ELSE
											PRINTLN("[EXEC1] - [CREATE_NON_CONTRABAND_VEHICLE] LAND_DEFEND GET_DROP_OFF_FOR_LAND_DEFEND_AI_SPAWN returning -1!")
										ENDIF
									ENDIF
									
									IF NOT IS_VECTOR_ZERO(serverBD.sSpawnStruct.vSpawnDirection)
										
										BOOL bSpawn
										FLOAT fSpanwDistFromContra
										
										bSpawn = TRUE 
										IF FIND_SPAWN_POINT_IN_DIRECTION(GET_ENTITY_COORDS(NET_TO_ENT(serverBD.niVeh[serverBD.iRandVehToAttack])), serverBD.sSpawnStruct.vSpawnDirection, fSpawnDistance, serverBd.sSpawnStruct.vSpawnNonContrabandVehicleCoords)
											fSpanwDistFromContra = GET_DISTANCE_BETWEEN_COORDS(serverBd.sSpawnStruct.vSpawnNonContrabandVehicleCoords, GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niVeh[serverBD.iRandVehToAttack]), FALSE))

											
											//-- Too far from the player?
											IF fSpanwDistFromContra > 300.0 //fSpanwDistFromContra * 1.5
												bSpawn = FALSE
												PRINTLN("[EXEC1] [CTRB_SELL] - [CREATE_NON_CONTRABAND_VEHICLE] - rejecting  spawn coords too far from player ", serverBd.sSpawnStruct.vSpawnNonContrabandVehicleCoords, " dist = ", fSpanwDistFromContra)
											ENDIF
											
											//-- Location ok?
											IF NOT IS_POINT_OK_FOR_NET_ENTITY_CREATION(serverBd.sSpawnStruct.vSpawnNonContrabandVehicleCoords, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT)
												bSpawn = FALSE
												PRINTLN("[EXEC1] [CTRB_SELL] - [CREATE_NON_CONTRABAND_VEHICLE] - serverBd.sSpawnStruct.vSpawnNonContrabandVehicleCoords not okay for net entity creation, setting back to zero so we search again")
											ENDIF
											
											
											
											IF NOT bSpawn
												serverBd.sSpawnStruct.vSpawnNonContrabandVehicleCoords = << 0.0, 0.0, 0.0 >>
												serverBD.sSpawnStruct.vSpawnDirection = << 0.0, 0.0, 0.0 >>
												sLocalData.iNumFailedAttempts++
												PRINTLN("[EXEC1] [CTRB_SELL] - [CREATE_NON_CONTRABAND_VEHICLE] bSpawn = FALSE, iNumFailedAttempts = ", sLocalData.iNumFailedAttempts)
											ELSE
												serverBd.sSpawnStruct.fSpawnNonContrabandVehicleHeading = GET_HEADING_BETWEEN_VECTORS_2D(serverBd.sSpawnStruct.vSpawnNonContrabandVehicleCoords, GET_ENTITY_COORDS(NET_TO_ENT(serverBD.niVeh[serverBD.iRandVehToAttack])))
												PRINTLN("[EXEC1] [CTRB_SELL] - [CREATE_NON_CONTRABAND_VEHICLE] - serverBd.vSpawnNonContrabandVehicleCoords is okay for net entity creation. Coords = ", serverBd.sSpawnStruct.vSpawnNonContrabandVehicleCoords, " Dist = ",fSpanwDistFromContra, " Tried to spawn at least this far ", fSpawnDistance, " serverBD.iRandVehToAttack = ", serverBD.iRandVehToAttack)
												PRINTLN("[EXEC1] [CTRB_SELL] - [CREATE_NON_CONTRABAND_VEHICLE] - serverBd.fSpawnNonContrabandVehicleHeading = GET_HEADING_BETWEEN_VECTORS_2D = ", serverBd.sSpawnStruct.fSpawnNonContrabandVehicleHeading)
											
											ENDIF
											
											
										ELSE
											sLocalData.iNumFailedAttempts++
											ADD_ROTATION_FOR_SPAWN_CHECK(serverBD.sSpawnStruct.vSpawnDirAsHeading)
											serverBD.sSpawnStruct.vSpawnDirection = CONVERT_ROTATION_TO_DIRECTION_VECTOR(serverBD.sSpawnStruct.vSpawnDirAsHeading)
											PRINTLN("[EXEC1] - [CREATE_NON_CONTRABAND_VEHICLE] - FIND_SPAWN_POINT_IN_DIRECTION = FALSE. Adding rotation to coords with ADD_ROTATION_FOR_SPAWN_CHECK iNumFailedAttempts = ", sLocalData.iNumFailedAttempts, " vSpawnDirAsHeading = ",serverBD.sSpawnStruct.vSpawnDirAsHeading, " vSpawnDirection = ", serverBD.sSpawnStruct.vSpawnDirection, " fSpawnDistance = ", fSpawnDistance)
										ENDIF
									ENDIF
								ENDIF
							ELSE
								PRINTLN("[EXEC1] [CTRB_SELL], CREATE_NON_CONTRABAND_VEHICLE, eVAR_LAND_DEFEND Failed to find spawn coord too many times, using HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS")
								IF IS_VECTOR_ZERO(serverBD.sSpawnStruct.vSpawnNearCoords)		
									serverBD.sSpawnStruct.vSpawnNearCoords =GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niVeh[serverBD.iRandVehToAttack]), FALSE)
									
									
								ENDIF
								
								IF NOT IS_VECTOR_ZERO(serverBD.sSpawnStruct.vSpawnNearCoords)		
									VEHICLE_SPAWN_LOCATION_PARAMS Params
									
									
									IF ARE_VECTORS_ALMOST_EQUAL(serverBD.sSpawnStruct.vSpawnNearCoords, <<-1190.6583, -2196.3591, 12.1951>>, 50.0)
										PRINTLN("[EXEC1] [CTRB_SELL], CREATE_NON_CONTRABAND_VEHICLE, eVAR_LAND_DEFEND Using alt coords as near airport")
										serverBD.sSpawnStruct.vSpawnNearCoords = <<-962.3659, -2148.9243, 7.8790>> 
										
										Params.fMinDistFromCoords = 0.0
										Params.bConsiderHighways= FALSE
										Params.fMaxDistance= 75.0
										Params.bConsiderOnlyActiveNodes= TRUE
										Params.bAvoidSpawningInExclusionZones= TRUE
										Params.bCheckEntityArea = FALSE //  TRUE
										Params.bUseExactCoordsIfPossible = FALSE
									ELSE
										Params.fMinDistFromCoords = 100.0 
										Params.bConsiderHighways= FALSE
										Params.fMaxDistance= 250.0
										Params.bConsiderOnlyActiveNodes= FALSE
										Params.bAvoidSpawningInExclusionZones= TRUE
										Params.bCheckEntityArea = FALSE //  TRUE
										Params.bUseExactCoordsIfPossible = FALSE
									ENDIF
									VECTOR vSpawnHeading 
									vSpawnHeading = serverBD.sSpawnStruct.vSpawnNearCoords
									PRINTLN("[EXEC1] [CTRB_SELL], CREATE_NON_CONTRABAND_VEHICLE, eVAR_LAND_DEFEND Looking for spawn coords near ", serverBD.sSpawnStruct.vSpawnNearCoords, " serverBD.iRandVehToAttack = ",serverBD.iRandVehToAttack )
									
									IF HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(serverBD.sSpawnStruct.vSpawnNearCoords, 
																		vSpawnHeading, 
																		model, 
																		FALSE, 
																		serverBD.sSpawnStruct.vSpawnNonContrabandVehicleCoords, 
																		serverBD.sSpawnStruct.fSpawnNonContrabandVehicleHeading, 
																		Params)
										
										
									//	IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(serverBd.sSpawnStruct.vSpawnNonContrabandVehicleCoords, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 190)
											PRINTLN("[EXEC1] [CTRB_SELL], CREATE_NON_CONTRABAND_VEHICLE, eVAR_LAND_DEFEND - serverBD.vSpawnNonContrabandVehicleCoords = ", serverBD.sSpawnStruct.vSpawnNonContrabandVehicleCoords)
											PRINTLN("[EXEC1] [CTRB_SELL], CREATE_NON_CONTRABAND_VEHICLE, eVAR_LAND_DEFEND - serverBD.fSpawnNonContrabandVehicleHeading = ", serverBD.sSpawnStruct.fSpawnNonContrabandVehicleHeading)
									//	ELSE
									//		PRINTLN("[EXEC1] [CTRB_SELL], CREATE_NON_CONTRABAND_VEHICLE, eVAR_LAND_DEFEND - Point not ok for net entity creation")
									//		serverBD.sSpawnStruct.vSpawnNonContrabandVehicleCoords = << 0.0, 0.0, 0.0 >>
									//		serverBD.sSpawnStruct.vSpawnNearCoords = << 0.0, 0.0, 0.0 >> 
									//	ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							IF NOT IS_VECTOR_ZERO(serverBd.sSpawnStruct.vSpawnNonContrabandVehicleCoords)	
								IF CREATE_NET_VEHICLE(serverBD.sPedVeh[iVehicle].netIndex, model, serverBd.sSpawnStruct.vSpawnNonContrabandVehicleCoords, serverBd.sSpawnStruct.fSpawnNonContrabandVehicleHeading, TRUE, TRUE, TRUE, FALSE, TRUE)
									SET_AMBUSH_VEHICLE_SPAWN_ATTRIBUTES(iVehicle)
									SET_MODEL_AS_NO_LONGER_NEEDED(model)
								//	SET_VEHICLE_STATE(iVehicle, GET_AFTER_CREATION_VEHICLE_STATE(iVehicle))
									serverBd.sSpawnStruct.iCurrentNonContrabandVehicleSearchingForSpawnCoords = (-1)
									serverBd.sSpawnStruct.vSpawnNonContrabandVehicleCoords = << 0.0, 0.0, 0.0 >>
									serverBD.sSpawnStruct.vSpawnDirection = << 0.0, 0.0, 0.0 >>
									serverBd.sSpawnStruct.fSpawnNonContrabandVehicleHeading = 0.0
									ServerBd.sSpawnStruct.iAmbushGetNonContrabandCoordsAttempts = 0
									serverBd.sSpawnStruct.iGetSpawnNonContrabandVehicleCoordsStage = 0
									serverBD.fNumWavesSpawned += 1.0
									sLocalData.iNumFailedAttempts = 0
									CLEAR_BIT(serverBD.iGangChaseStuckBitset, iVehicle)
									#IF IS_DEBUG_BUILD
									serverBD.iGangChaseVehCreated++
									PRINTLN("[EXEC1] - [CREATE_NON_CONTRABAND_VEHICLE] ")
									PRINTLN("[EXEC1] - [CREATE_NON_CONTRABAND_VEHICLE] - created non contraband vehicle ", iVehicle, " serverBD.iGangChaseVehCreated = ", serverBD.iGangChaseVehCreated, " serverBD.fNumWavesSpawned = ", serverBD.fNumWavesSpawned, " serverBD.iRandVehToAttack = ", serverBD.iRandVehToAttack)
									PRINTLN("[EXEC1] - [CREATE_NON_CONTRABAND_VEHICLE] ********************** NON-CONTRABAND VEHICLE CREATED **********************")
									PRINTLN("[EXEC1] - [CREATE_NON_CONTRABAND_VEHICLE] ")
									#ENDIF
									
									#IF IS_DEBUG_BUILD
									tl15Name = "VEH " 
									tl15Name += iVehicle
									SET_VEHICLE_NAME_DEBUG(NET_TO_VEH(serverBD.sPedVeh[iVehicle].netIndex), tl15Name)
									#ENDIF
									RETURN TRUE
								ENDIF
							ENDIF
							
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC



PROC MAINTAIN_NON_CONTRABAND_VEHICLES()

	IF DOES_VARIATION_HAVE_AI_VEHICLES()
		INT iVehicle
		INT iPed
		
		REPEAT GET_NUM_ATTACK_VEHICLES_FOR_VARIATION() iVehicle
			SWITCH GET_VEHICLE_STATE(iVehicle)
			
				CASE eVEHICLESTATE_INACTIVE
				
				BREAK
				
				CASE eVEHICLESTATE_CREATE
					IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPedVeh[iVehicle].netIndex)
						IF IS_SAFE_TO_CREATE_NON_CONTRABAND_VEHICLES()
							IF HAS_NET_TIMER_STARTED(serverBD.stPedVehRespawnDelay)
								RESET_NET_TIMER(serverBD.stPedVehRespawnDelay)
							ENDIF
							IF REQUEST_LOAD_MODEL(GET_VEHICLE_MODEL_FOR_VARIATION())
								IF CREATE_NON_CONTRABAND_VEHICLE( iVehicle, GET_VEHICLE_MODEL_FOR_VARIATION())
									PRINTLN("[EXEC1] [CTRB_SELL] [PED_VEHICLE], MAINTAIN_NON_CONTRABAND_VEHICLES - Vehicle ", iVehicle, " created, moving to populate")
									SET_VEHICLE_STATE(iVehicle, eVEHICLESTATE_POPULATE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE eVEHICLESTATE_POPULATE
					IF DOES_VARIATION_HAVE_AI_PEDS()
						REPEAT GET_NUM_PEDS_FOR_VARIATION() iPed
							IF CREATE_AI_PED( iPed)
								PRINTLN("[EXEC1] [CTRB_SELL] [PED_VEHICLES], MAINTAIN_NON_CONTRABAND_VEHICLES, eVEHICLESTATE_POPULATE - Ped ", iPed, " created.")
							ENDIF
						ENDREPEAT
					ENDIF
					
					IF IS_NON_CONTRABAND_VEHICLE_OCCUPIED(iVehicle)
						SET_VEHICLE_STATE(iVehicle, eVEHICLESTATE_DRIVEABLE)
					ELSE
						PRINTLN("[EXEC1] [CTRB_SELL] [PED_VEHICLES], MAINTAIN_NON_CONTRABAND_VEHICLES, VEH NOT OCCUPIED! Veh ", iVehicle)
					ENDIF
				BREAK
				
				CASE eVEHICLESTATE_DRIVEABLE
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPedVeh[iVehicle].netIndex)
						IF NOT IS_VEHICLE_DRIVEABLE(NET_TO_VEH(serverBD.sPedVeh[iVehicle].netIndex))
						OR IS_ENTITY_DEAD(NET_TO_VEH(serverBD.sPedVeh[iVehicle].netIndex))
							PRINTLN("[EXEC1] [CTRB_SELL] [PED_VEHICLES], MAINTAIN_NON_CONTRABAND_VEHICLES, eVEHICLESTATE_DRIVEABLE - Veh ", iVehicle, " no longer driveable.")
							SET_VEHICLE_STATE(iVehicle, eVEHICLESTATE_NOT_DRIVEABLE)
						ENDIF
					ENDIF
				BREAK
				
				CASE eVEHICLESTATE_NOT_DRIVEABLE

				BREAK
				
				CASE eVEHICLESTATE_RESET
					
				BREAK
				
			ENDSWITCH
		
		ENDREPEAT
	ENDIF
ENDPROC

/// PURPOSE:
///    For AI helicopters, need to clean them up as soon as they're not drivable, as otherwise they'll fall through the map and assert
/// RETURNS:
///    
FUNC BOOL SHOULD_CLEANUP_CONTRABAND_VEHICLES_IMMEDIATELY()
	IF bAIR_ATTACK()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_NON_CONTRABAND_PED_AND_VEHICLE_RESPAWNING()
	IF DOES_VARIATION_HAVE_AI_PEDS()
	AND DOES_VARIATION_HAVE_AI_VEHICLES()
		IF NOT HAS_VEHICLE_RESPAWNING_TIMER_STARTED()
			IF SHOULD_RESPAWN_VEHICLES()

				PRINTLN("[EXEC1] [CTRB_SELL] [PED_VEHICLES], MAINTAIN_NON_CONTRABAND_VEHICLES, eVEHICLESTATE_NOT_DRIVEABLE - Vehicles should respawn, starting timer ")
				START_NET_TIMER(serverBD.stPedVehRespawnTimer)
				
			ENDIF
		ELSE
			IF HAS_VEHICLE_RESPAWNING_TIMER_EXPIRED()
				IF NOT HAS_NET_TIMER_STARTED(serverBD.stPedVehRespawnDelay)
					IF DELETE_VEHICLES()
					AND DELETE_PEDS()
						PRINTLN("[EXEC1] [CTRB_SELL] [PED_VEHICLES], MAINTAIN_NON_CONTRABAND_VEHICLES RESPAWN TIMER EXPIRED AND ENTITIES DELETED")
						RESET_NET_TIMER(serverBD.stPedVehRespawnTimer)
						START_NET_TIMER(serverBD.stPedVehRespawnDelay)
						RANDOMISE_VEHICLES()
						IF DOES_SHIPMENT_TYPE_HAVE_MULTIPLE_VEHICLES()
							IF NOT bLAND_DEFEND()
							AND NOT bLAND_ATTACK()
								serverBD.iRandVehToAttack = GET_RANDOM_INT_IN_RANGE(0, GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE())
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				//-- If AI helicopters are wrecked then they need to be cleaned up pretty quickly, or
				//-- they'll fall through the map and assert.
				IF SHOULD_CLEANUP_CONTRABAND_VEHICLES_IMMEDIATELY()
					DELETE_VEHICLES()
					DELETE_PEDS()
					RANDOMISE_VEHICLES()
					IF DOES_SHIPMENT_TYPE_HAVE_MULTIPLE_VEHICLES()
						IF NOT bLAND_DEFEND()
						AND NOT bLAND_ATTACK()
							serverBD.iRandVehToAttack = GET_RANDOM_INT_IN_RANGE(0, GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE())
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_CLIENT_VEHICLE_SPAWN_CHECKS()
	IF DOES_VARIATION_HAVE_AI_VEHICLES()
		SWITCH GET_SELL_VAR()
			CASE eVAR_SEA_ATTACK
				IF HAS_ANY_SELL_VEHICLE_BEEN_ENTERED()
					IF NOT IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), eCLIENTBITSET0_SEA_ATTACK_SPAWN_DISTANCE_REACHED)
						IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niVeh[0])
							IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(VEH_ID(0), GET_SEA_SELL_VEHICLE_CREATION_POS(), FALSE) >= GET_MODE_TUNEABLE_DISTANCE_FROM_START_FOR_PED_SPAWNING()
								PRINTLN("[EXEC1] [CTRB_SELL] [PLAYER], MAINTAIN_CLIENT_VEHICLE_SPAWN_CHECKS - eVAR_SEA_ATTACK - Vehicle is now > 100m from starting point, setting that server should spawn attackers")
								SET_CLIENT_BIT0(eCLIENTBITSET0_SEA_ATTACK_SPAWN_DISTANCE_REACHED)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE eVAR_LAND_ATTACK
		//	CASE eVAR_AIR_ATTACK
				INT i
				REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
					IF NOT IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), eCLIENTBITSET0_LAND_ATTACK_SPAWN_DISTANCE_REACHED)
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[i])
							IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niVeh[i])
								IF NOT IS_VEHICLE_NEAR_CONTRABAND_WAREHOUSE(NET_TO_VEH(serverBD.niVeh[i]), serverBD.iStartingWarehouse, 350)
									PRINTLN("[EXEC1] [CTRB_SELL] [PLAYER], MAINTAIN_CLIENT_VEHICLE_SPAWN_CHECKS - eVAR_LAND_ATTACK - Vehicle is now > 200m from starting point, setting that server should spawn attackers")
									SET_CLIENT_BIT0(eCLIENTBITSET0_LAND_ATTACK_SPAWN_DISTANCE_REACHED)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

PROC MAINTAIN_BLOCKING_ZONES()

	VECTOR vDropOff = GET_WAREHOUSE_COORDS(serverBD.iStartingWarehouse)
	IF NOT IS_VECTOR_ZERO(vDropOff)
		ADD_TRAFFIC_REDUCTION_ZONE(serverBD.iTrafficReductionIndex,vDropOff)
	ENDIF

ENDPROC


//╔═════════════════════════════════════════════════════════════════════════════╗
//║				CRATES															║

// serverBD.sContraband[serverBD.iNumCrates].iCrateBS
CONST_INT CRATE_BS_ANIM_STARTED 0
CONST_INT CRATE_BS_ANIM_PLAYING 1
CONST_INT CRATE_BS_ANIM_DONE	2

FUNC MODEL_NAMES modelCRATE()

	IF bLAND_VARIATION()
	
		RETURN EX_PROP_ADV_CASE_SM
		
	ELIF bAIR_VARIATION()

		RETURN EX_PROP_ADV_CASE_SM_FLASH
		
	ENDIF
	
	RETURN PROP_DRUG_PACKAGE
ENDFUNC

FUNC MODEL_NAMES modelCHUTE()
	RETURN P_CARGO_CHUTE_S
ENDFUNC

FUNC STRING sANIM_DICT_CHUTE()
	RETURN "P_cargo_chute_S"
ENDFUNC

//PURPOSE: Loads the Crate assets
FUNC BOOL LOAD_CRATE_ASSETS()

	REQUEST_MODEL(modelCRATE())
	REQUEST_MODEL(modelCHUTE())
	REQUEST_ANIM_DICT(sANIM_DICT_CHUTE())

	IF HAS_MODEL_LOADED(modelCRATE())
	AND HAS_MODEL_LOADED(modelCHUTE())
	AND HAS_ANIM_DICT_LOADED(sANIM_DICT_CHUTE())
	    RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

//PURPOSE: Creates and drops a crate cache from the plane
FUNC BOOL CREATE_DROPPED_CRATE(VECTOR vCrate, FLOAT fCrate, INT iVeh)
	
	VECTOR vCrateDropOffset// = <<0, 0, 100>>
	VECTOR vChuteCoords
	INT index = serverBD.iNumCrates[iVeh]
    //Create the Crate 
	
    IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.sContraband[index].netId[iVeh])     
		IF CAN_REGISTER_MISSION_OBJECTS(1)
	        serverBD.sContraband[index].netId[iVeh] = OBJ_TO_NET(CREATE_PORTABLE_PICKUP(PICKUP_PORTABLE_CRATE_UNFIXED_INCAR, ( vCrate + vCrateDropOffset ), FALSE, modelCRATE()))
	        SET_ENTITY_HEADING(NET_TO_OBJ(serverBD.sContraband[index].netId[iVeh]), fCrate)
	        SET_NETWORK_ID_EXISTS_ON_ALL_MACHINES(serverBD.sContraband[index].netId[iVeh], TRUE)
	                    
	        SET_ENTITY_INVINCIBLE(NET_TO_OBJ(serverBD.sContraband[index].netId[iVeh]), TRUE)
	        SET_OBJECT_FORCE_VEHICLES_TO_AVOID(NET_TO_OBJ(serverBD.sContraband[index].netId[iVeh]), TRUE)
	        
	        SET_ACTIVATE_OBJECT_PHYSICS_AS_SOON_AS_IT_IS_UNFROZEN(NET_TO_OBJ(serverBD.sContraband[index].netId[iVeh]), TRUE)
	        SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(NET_TO_OBJ(serverBD.sContraband[index].netId[iVeh]), FALSE)
	        ACTIVATE_PHYSICS(NET_TO_OBJ(serverBD.sContraband[index].netId[iVeh]))
	        SET_ENTITY_VELOCITY(NET_TO_OBJ(serverBD.sContraband[index].netId[iVeh]), <<0, 0, -0.2>>)
	        
	        PREVENT_COLLECTION_OF_PORTABLE_PICKUP(NET_TO_OBJ(serverBD.sContraband[index].netId[iVeh]), TRUE)
	        
	        SET_ENTITY_LOD_DIST(NET_TO_OBJ(serverBD.sContraband[index].netId[iVeh]), 1200)  
			
			IF HAS_NET_TIMER_STARTED(serverBD.sContraband[index].timeCleanup[iVeh])
				RESET_NET_TIMER(serverBD.sContraband[index].timeCleanup[iVeh])
			ENDIF
			
			PRINTLN("[EXEC1] [CTRB_SELL] [CRT] - CREATE_DROPPED_CRATE, CRATE_MADE vCrate = ", vCrate, " index = ", index, " iVeh = ", iVeh)
		ELSE
			PRINTLN("[EXEC1] [CTRB_SELL] [CRT] - CAN_REGISTER_MISSION_PICKUPS Failing! vCrate = ", vCrate, " index = ", index, " iVeh = ", iVeh)
		ENDIF
    ENDIF
      
    //Create Chute
	IF bAIR_VARIATION()
		IF CAN_REGISTER_MISSION_OBJECTS(1)
		    IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.sContraband[index].netId[iVeh])
			    IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.sContraband[index].niChute[iVeh])
					vChuteCoords = GET_ENTITY_COORDS(NET_TO_OBJ(serverBD.sContraband[index].netId[iVeh]))
		            IF CREATE_NET_OBJ(serverBD.sContraband[index].niChute[iVeh], modelCHUTE(), ( vChuteCoords + <<0, 0, 1>>) )  
		                //SET_NETWORK_ID_EXISTS_ON_ALL_MACHINES(serverBD.sContraband[index].niChute[iVeh], TRUE)
		          
		                PRINTLN("[EXEC1] [CTRB_SELL] [CRT]  - CREATE_DROPPED_CRATE, CREATED CHUTE FOR CRATE vChuteCoords = ", vChuteCoords)
		          
		                ATTACH_ENTITY_TO_ENTITY(NET_TO_OBJ(serverBD.sContraband[index].niChute[iVeh]), NET_TO_OBJ(serverBD.sContraband[index].netId[iVeh]), 0, <<0, 0, 0>>, <<0, 0, 0>>)
		                SET_ACTIVATE_OBJECT_PHYSICS_AS_SOON_AS_IT_IS_UNFROZEN(NET_TO_OBJ(serverBD.sContraband[index].niChute[iVeh]), TRUE)
		                SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(NET_TO_OBJ(serverBD.sContraband[index].niChute[iVeh]), FALSE)
		                PLAY_ENTITY_ANIM(NET_TO_OBJ(serverBD.sContraband[index].niChute[iVeh]), "P_cargo_chute_S_deploy", "P_cargo_chute_S", INSTANT_BLEND_IN, FALSE, FALSE)
		                FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(NET_TO_OBJ(serverBD.sContraband[index].niChute[iVeh]))
		                
		                SET_ENTITY_LOD_DIST(NET_TO_OBJ(serverBD.sContraband[index].niChute[iVeh]), 1200)
		    
		                RESET_NET_TIMER(serverBD.sContraband[index].InAirCheckDelayTimer[iVeh])
		            ENDIF
			    ENDIF
		    ENDIF
	      
			IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.sContraband[index].netId[iVeh])
			AND NETWORK_DOES_NETWORK_ID_EXIST(serverBD.sContraband[index].niChute[iVeh])
			    RETURN TRUE
			ENDIF
		ELSE
			PRINTLN("[EXEC1] [CTRB_SELL] [CRT] - CAN_REGISTER_MISSION_OBJECTS Failing! vCrate = ", vCrate, " index = ", index, " iVeh = ", iVeh)
		ENDIF
	ELSE
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.sContraband[index].netId[iVeh])
		    RETURN TRUE
		ENDIF
	ENDIF
	  
	RETURN FALSE
ENDFUNC

FUNC BOOL ROTATION_IS_TOO_FAR(OBJECT_INDEX oiCrate)
	FLOAT fRot = GET_ENTITY_PITCH(oiCrate)
	
	IF fRot > 10
	OR fRot < -10
		PRINTLN("[EXEC1] [CTRB_SELL] [CRT]  - ROTATION_IS_TOO_FAR - PITCH")
		RETURN TRUE
	ENDIF
	
	fRot = GET_ENTITY_ROLL(oiCrate)
	IF fRot > 10
	OR fRot < -10
		PRINTLN("[EXEC1] [CTRB_SELL] [CRT]  - ROTATION_IS_TOO_FAR - ROLL")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Plays an anim when the package lands
FUNC BOOL SHOULD_LAND_ANIM_START(INT i, INT iVeh, OBJECT_INDEX oiCrate)

	IF NOT DOES_ENTITY_EXIST(oiCrate)
		RETURN FALSE
	ENDIF

	IF HAS_NET_TIMER_EXPIRED(serverBD.sContraband[i].InAirCheckDelayTimer[iVeh], 3000)
		IF NOT IS_ENTITY_IN_AIR(oiCrate)
//			PREVENT_COLLECTION_OF_PORTABLE_PICKUP(oiCrate, FALSE)
			PRINTLN("[EXEC1] [CTRB_SELL] [CRT]  - SHOULD_LAND_ANIM_START - TRUE - NOT IS_ENTITY_IN_AIR")
			RETURN TRUE
		ENDIF
	ENDIF

	IF IS_ENTITY_IN_WATER(oiCrate)
//		PREVENT_COLLECTION_OF_PORTABLE_PICKUP(oiCrate, FALSE)
		PRINTLN("[EXEC1] [CTRB_SELL] [CRT]  - SHOULD_LAND_ANIM_START - TRUE - IS_ENTITY_IN_WATER")
		RETURN TRUE
	ENDIF

	IF ROTATION_IS_TOO_FAR(oiCrate)
//		PREVENT_COLLECTION_OF_PORTABLE_PICKUP(oiCrate, FALSE)
		PRINTLN("[EXEC1] [CTRB_SELL] [CRT]  - SHOULD_LAND_ANIM_START - TRUE - ROTATION_IS_TOO_FAR")
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

PROC CLEANUP_CRATES()
	INT i
	INT iVeh
	ENTITY_INDEX eToClean
	REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() iVeh
		REPEAT ciMAX_CRATES i
			IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.sContraband[i].niChute[iVeh])
			AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sContraband[i].niChute[iVeh])
//				DELETE_NET_ID(serverBD.sContraband[i].niChute[iVeh])
				eToClean = NET_TO_ENT(serverBD.sContraband[i].niChute[iVeh])
				SET_ENTITY_AS_NO_LONGER_NEEDED(eToClean)
			ENDIF
			IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.sContraband[i].netId[iVeh])
			AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sContraband[i].netId[iVeh])
//				DELETE_NET_ID(serverBD.sContraband[i].netId[iVeh])
				eToClean = NET_TO_ENT(serverBD.sContraband[i].netId[iVeh])
				SET_ENTITY_AS_NO_LONGER_NEEDED(eToClean)
			ENDIF
		ENDREPEAT
	ENDREPEAT
ENDPROC

////PURPOSE: Plays an anim when the package lands - ALSO DOES DELETION AND TIMEOUT CHECKS
PROC CONTROL_CRATE_LANDING()
	 
	VECTOR vCratePos
	FLOAT vCrateGroundZ
//	ENTITY_INDEX eiCrate
	OBJECT_INDEX objCrate
	OBJECT_INDEX objChute
	
	INT i
	INT iVeh
	REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() iVeh
		REPEAT ciMAX_CRATES i
	  
		 	//CRATE CHECKS
		  	IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.sContraband[i].netId[iVeh])
		 	AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sContraband[i].netId[iVeh])
			
	//			eiCrate = NET_TO_ENT(serverBD.sContraband[i].netId[iVeh])
			
	//			PRINTLN("CONTROL_CRATE_LANDING a i = ", i)
				
				IF TAKE_CONTROL_OF_NET_ID(serverBD.sContraband[i].netId[iVeh])			
					APPLY_FORCE_TO_ENTITY_CENTER_OF_MASS(NET_TO_ENT(serverBD.sContraband[i].netId[iVeh]), APPLY_TYPE_FORCE, <<0,0,10>>, 0, FALSE, TRUE)
				ENDIF
							
		    	IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sContraband[i].netId[iVeh])
		        OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.sContraband[i].netId[iVeh]) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
				
					//CHUTE CHECKS
					IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.sContraband[i].niChute[iVeh])
					AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sContraband[i].niChute[iVeh])  
					
						objCrate = NET_TO_OBJ(serverBD.sContraband[i].netId[iVeh])
						objChute = NET_TO_OBJ(serverBD.sContraband[i].niChute[iVeh])
					
	//					PRINTLN("CONTROL_CRATE_LANDING b i = ", i)
		                    
						IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sContraband[i].niChute[iVeh])
						OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.sContraband[i].niChute[iVeh]) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
						
	//						PRINTLN("CONTROL_CRATE_LANDING c i = ", i)
		                          
							IF TAKE_CONTROL_OF_NET_ID(serverBD.sContraband[i].netId[iVeh])
							AND TAKE_CONTROL_OF_NET_ID(serverBD.sContraband[i].niChute[iVeh])
							
	//							PRINTLN("CONTROL_CRATE_LANDING d i = ", i)
		                                
								//Delete Chute when Crumple is done
								IF NOT IS_BIT_SET(serverBD.sContraband[i].iCrateBS[iVeh], CRATE_BS_ANIM_DONE)
								AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iCrateBS[iVeh], CRATE_BS_ANIM_DONE)
									IF IS_BIT_SET(serverBD.sContraband[i].iCrateBS[iVeh], CRATE_BS_ANIM_PLAYING)
									OR IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iCrateBS[iVeh], CRATE_BS_ANIM_PLAYING)
										IF DOES_ENTITY_EXIST(objChute)
											IF NOT IS_ENTITY_PLAYING_ANIM(objChute, "P_cargo_chute_S", "P_cargo_chute_S_crumple")
												SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iCrateBS[iVeh], CRATE_BS_ANIM_DONE)
												//SET_BIT(serverBD.sContraband[i].iCrateBS[iVeh], CRATE_BS_ANIM_DONE)
												DELETE_NET_ID(serverBD.sContraband[i].niChute[iVeh])
												DELETE_NET_ID(serverBD.sContraband[i].netId[iVeh])
//												PREVENT_COLLECTION_OF_PORTABLE_PICKUP(objCrate, FALSE)
												PRINTLN("[EXEC1] [CTRB_SELL] [CRT]  - CONTROL_CRATE_LANDING - PREVENT_COLLECTION_OF_PORTABLE_PICKUP - FALSE - B - ", i)
												PRINTLN("[EXEC1] [CTRB_SELL] [CRT]  - CONTROL_CRATE_LANDING - LAND ANIM DONE - CHUTE DELETED - ", i) 
												
												CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iCrateBS[iVeh], CRATE_BS_ANIM_STARTED )
												CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iCrateBS[iVeh], CRATE_BS_ANIM_PLAYING )
												CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iCrateBS[iVeh], CRATE_BS_ANIM_DONE	)
											ENDIF
										ENDIF
									ENDIF
								ELSE
									PRINTLN("CONTROL_CRATE_LANDING a i = ", i)
								ENDIF
								
								//Check Crumple is playing
								IF NOT IS_BIT_SET(serverBD.sContraband[i].iCrateBS[iVeh], CRATE_BS_ANIM_PLAYING)
								AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iCrateBS[iVeh], CRATE_BS_ANIM_PLAYING)
									IF IS_BIT_SET(serverBD.sContraband[i].iCrateBS[iVeh], CRATE_BS_ANIM_STARTED)
									OR IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iCrateBS[iVeh], CRATE_BS_ANIM_STARTED)
										IF DOES_ENTITY_EXIST(objChute)
											IF IS_ENTITY_PLAYING_ANIM(objChute, "P_cargo_chute_S", "P_cargo_chute_S_crumple")
												SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iCrateBS[iVeh], CRATE_BS_ANIM_PLAYING)
												//SET_BIT(serverBD.sContraband[i].iCrateBS[iVeh], CRATE_BS_ANIM_STARTED)
												PRINTLN("[EXEC1] [CTRB_SELL] [CRT]  - CONTROL_CRATE_LANDING - LAND ANIM P_cargo_chute_S_crumple -  PLAYING - ", i)
											ENDIF
										ENDIF
									ENDIF
								ELSE
									PRINTLN("CONTROL_CRATE_LANDING b i = ", i)
								ENDIF
								
										
		                        //Start Crumple
		                        IF NOT IS_BIT_SET(serverBD.sContraband[i].iCrateBS[iVeh], CRATE_BS_ANIM_STARTED)
	//	                        AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iCrateBS[iVeh], CRATE_BS_ANIM_STARTED)
									IF SHOULD_LAND_ANIM_START(i, iVeh, objCrate)
										PLAY_ENTITY_ANIM(objChute, "P_cargo_chute_S_crumple", "P_cargo_chute_S", INSTANT_BLEND_IN, FALSE, FALSE) //TRUE)
//										SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iCrateBS, CRATE_BS_ANIM_STARTED)
										SET_BIT(serverBD.sContraband[i].iCrateBS[iVeh], CRATE_BS_ANIM_STARTED)
										PRINTLN("[EXEC1] [CTRB_SELL] [CRT]  - CONTROL_CRATE_LANDING - LAND ANIM STARTED  P_cargo_chute_S_crumple  STARTED - ", i)
										PRINTLN("[EXEC1] [CTRB_SELL] [CRT]  - CONTROL_CRATE_LANDING - NETID = ", NATIVE_TO_INT(serverBD.sContraband[i].niChute[iVeh]))
										PLAY_SOUND_FROM_ENTITY(sLocaldata.iChuteSoundId, "Parachute_Land", objChute, "DLC_Exec_Air_Drop_Sounds")
								
									

									ELSE
										SET_DAMPING(objCrate, PHYSICS_DAMPING_LINEAR_V2, 0.0245)
									ENDIF
								ELSE
									PRINTLN("CONTROL_CRATE_LANDING c i = ", i)
								ENDIF
								
		                    ENDIF
						ENDIF
					ENDIF
		            
					  
					//Check Crate is above ground
					IF DOES_ENTITY_EXIST(objCrate)
						vCratePos = GET_ENTITY_COORDS(objCrate)
						IF GET_GROUND_Z_FOR_3D_COORD(vCratePos, vCrateGroundZ) 
							IF vCratePos.z < vCrateGroundZ
							AND NOT IS_ENTITY_IN_WATER(objCrate)
								IF TAKE_CONTROL_OF_NET_ID(serverBD.sContraband[i].netId[iVeh])
									SET_ENTITY_COORDS(objCrate, <<vCratePos.x, vCratePos.y, vCrateGroundZ>>)
									PRINTLN("[EXEC1] [CTRB_SELL] [CRT]  - CONTROL_CRATE_LANDING - NORMAL CRATE BELOW GROUND - WARP BACK UP ") 
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF bLAND_VARIATION()
						IF NOT HAS_NET_TIMER_STARTED(serverBD.sContraband[i].timeCleanup[iVeh])
							START_NET_TIMER(serverBD.sContraband[i].timeCleanup[iVeh])
							PRINTLN("[EXEC1] [CTRB_SELL] [CRT] Started crate cleanup timer Contra ", i, " Veh ", iVeh)
						ELSE
							IF HAS_NET_TIMER_EXPIRED(serverBD.sContraband[i].timeCleanup[iVeh], 30000)
								PRINTLN("[EXEC1] [CTRB_SELL] [CRT] Cleanup timer expired Contra ", i, " Veh ", iVeh)
								CLEANUP_NET_ID(serverBD.sContraband[i].netId[iVeh])
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDREPEAT
ENDPROC

PROC CLEANUP_CRATE_DROP_ALARM()
	IF sLocaldata.iDropAlarm != -1
		PRINTLN("[EXEC1]  [SELL AUDIO] - [DROP] CLEANUP_CRATE_DROP_ALARM - STOP_SOUND(", sLocaldata.iDropAlarm, ")")
		STOP_SOUND(sLocaldata.iDropAlarm)
		RELEASE_SOUND_ID(sLocaldata.iDropAlarm)
		sLocaldata.iDropAlarm = -1
		PRINTLN("[EXEC1]  [SELL AUDIO] - [DROP] CLEANUP_CRATE_DROP_ALARM - sLocaldata.iDropAlarm = -1")
	ENDIF
ENDPROC

PROC PLAY_AIR_DROP_CRATE_SOUND()
	PRINTLN("[EXEC1] [SELL AUDIO] - [DROP] PLAY_AIR_DROP_CRATE_SOUND, PLAY_SOUND_FRONTEND(-1,\"Drop_Package\",\"DLC_Exec_Air_Drop_Sounds\",FALSE)")
	PLAY_SOUND_FRONTEND(-1, "Drop_Package", "DLC_Exec_Air_Drop_Sounds", FALSE)
ENDPROC

PROC PLAY_AIR_DROP_ALARM_SOUND()
	IF sLocaldata.iDropAlarm = -1
	AND bAIR_VARIATION()
		sLocaldata.iDropAlarm = GET_SOUND_ID()
		PLAY_SOUND_FRONTEND(sLocaldata.iDropAlarm,"Drop_Zone_Alarm","DLC_Exec_Air_Drop_Sounds",FALSE)
		PRINTLN("[EXEC1]  [SELL AUDIO] - [DROP]  PLAY_AIR_DROP_ALARM_SOUND, PLAY_SOUND_FRONTEND(", sLocaldata.iDropAlarm, ",\"Drop_Zone_Alarm\",\DLC_Exec_Air_Drop_Sounds\",FALSE)")
	ENDIF
ENDPROC

PROC MAINTAIN_CRATE_DROP_CLIENT()
	IF bOK_TO_DO_CRATES()
		IF NOT HAS_ALL_CONTRABAND_BEEN_DROPPED_OFF()
			IF LOAD_CRATE_ASSETS()
				INT iVeh
				REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() iVeh	
				
					IF IS_PASSENGER(iVeh)
					AND serverBD.iNumCrates[iVeh] < ciMAX_CRATES
					
						IF IS_SELL_VEH_OK(iVeh)
						AND IS_ENTITY_AT_COORD(VEH_ENT(iVeh), GET_SELL_DROP_COORDS(GET_SELL_VAR(), serverBD.iClosestDropOff[iVeh], serverBD.iRoute, iVeh), vLOCATE_DIMENSION())
						AND ( NOT bAIR_VARIATION() OR IS_ENTITY_IN_AIR(VEH_ENT(iVeh))#IF IS_DEBUG_BUILD OR GET_COMMANDLINE_PARAM_EXISTS("sc_GBAlwaysAllowDrop") #ENDIF )
							 PLAY_AIR_DROP_ALARM_SOUND()
						ELSE
							CLEANUP_CRATE_DROP_ALARM()
						ENDIF
					ENDIF
				
					IF IS_DRIVER_OF_SELL_VEH(iVeh)
					AND serverBD.iNumCrates[iVeh] < ciMAX_CRATES
						BOOL bCanDrop, bInArea, bWanted
						//If we are in the area then sound the alarm
						
						IF IS_SELL_VEH_OK(iVeh)
						AND IS_ENTITY_AT_COORD(VEH_ENT(iVeh), GET_SELL_DROP_COORDS(GET_SELL_VAR(), serverBD.iClosestDropOff[iVeh], serverBD.iRoute, iVeh), vLOCATE_DIMENSION())
						AND ( NOT bAIR_VARIATION() OR IS_ENTITY_IN_AIR(VEH_ENT(iVeh))#IF IS_DEBUG_BUILD OR GET_COMMANDLINE_PARAM_EXISTS("sc_GBAlwaysAllowDrop") #ENDIF )
						
							PRINTLN("[EXEC1] MAINTAIN_CRATE_DROP_CLIENT - Hit 7 - iVeh = ", iVeh)
							bInArea = TRUE
							PLAY_AIR_DROP_ALARM_SOUND()
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT)
							IF bAIR_VARIATION()
							AND NOT IS_LOCAL_BIT1_SET(eLOCALBITSET1_BOMB_DOORS)
								OPEN_BOMB_BAY_DOORS(VEH_ID(iVeh))
								SET_LOCAL_BIT1(eLOCALBITSET1_BOMB_DOORS)
							ENDIF
						//Clean up the alarm
						ElSE
							CLEANUP_CRATE_DROP_ALARM()
							IF bAIR_VARIATION()
							AND IS_LOCAL_BIT1_SET(eLOCALBITSET1_BOMB_DOORS)
								CLOSE_BOMB_BAY_DOORS(VEH_ID(iVeh))
								CLEAR_LOCAL_BIT1(eLOCALBITSET1_BOMB_DOORS)
							ENDIF
						ENDIF
						
						bWanted = ((GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) != 0) 
									OR IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), eCLIENTBITSET0_WANTED))
						
						//If it's air restricted or STING
						IF(bAIR_RESTRICTED() OR bSTING())
						//If we've give the player the scripted wanted
						AND sLocaldata.iWantedStored != -1
						AND NOT Is_This_The_Current_Objective_Text("BYCB_CLY_LSC0")
							//PRINTLN("[EXEC1] MAINTAIN_CRATE_DROP_CLIENT - bWanted = FALSE")
							bWanted = FALSE
						ENDIF
						IF Is_This_The_Current_Objective_Text("BYCB_CLY_LSC0")
							bWanted = TRUE
						ENDIF
						PRINTLN("[EXEC1] MAINTAIN_CRATE_DROP_CLIENT - bInArea = ", bInArea)
						PRINTLN("[EXEC1] MAINTAIN_CRATE_DROP_CLIENT - bWanted = ", bWanted)
						PRINTLN("[EXEC1] MAINTAIN_CRATE_DROP_CLIENT - IS_BIT_SET(sLocaldata.iDroppedBitSet[iVeh], serverBD.iClosestDropOff[", iVeh, "]) = ", IS_BIT_SET(sLocaldata.iDroppedBitSet[iVeh], serverBD.iClosestDropOff[iVeh]))
						
						//If we're in the area then print the help
						IF bInArea
						AND NOT bWanted
						AND NOT IS_BIT_SET(sLocaldata.iDroppedBitSet[iVeh], serverBD.iClosestDropOff[iVeh])
							// "Press ~INPUT_FRONTEND_RIGHT~ to drop Contraband."
							bCanDrop = TRUE
							PRINT_HELP("SCONTRA_CDROP")
							PRINTLN("[EXEC1] MAINTAIN_CRATE_DROP_CLIENT - SCONTRA_CDROP")
						ELIF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SCONTRA_CDROP")
							PRINTLN("[EXEC1] MAINTAIN_CRATE_DROP_CLIENT - CLEAR_HELP SCONTRA_CDROP")
							CLEAR_HELP(TRUE)
						ENDIF
						
						SWITCH sLocaldata.iCrateProgress
							CASE 0
									
								IF bCanDrop
								AND IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_FRONTEND_RIGHT)
								AND NOT IS_BIT_SET(sLocaldata.iDroppedBitSet[iVeh], serverBD.iClosestDropOff[iVeh])
								AND NOT bWanted
									BROADCAST_CRATE_EVENT(iVeh, serverBD.iClosestDropOff[iVeh], serverBD.iLaunchPosix)
									PRINTLN("[EXEC1] [CTRB_SELL] [CRT]  - MAINTAIN_CRATE_DROP_CLIENT CASE, COMPLETE = ", serverBD.iClosestDropOff[iVeh], " - iVeh = ", iVeh) 
									SET_BIT(sLocaldata.iDroppedBitSet[iVeh], serverBD.iClosestDropOff[iVeh])
									sLocaldata.iCrateProgress++
								ENDIF
							BREAK
							CASE 1
								sLocaldata.iCrateProgress = 0
							BREAK
						ENDSWITCH
					ELSE
						IF NOT IS_PASSENGER(iVeh)
							CLEANUP_CRATE_DROP_ALARM()
						ENDIF
					ENDIF
				ENDREPEAT
			ENDIF
		ELSE
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SCONTRA_CDROP")
				CLEAR_HELP(TRUE)
			ENDIF
			CLEANUP_CRATE_DROP_ALARM()
		ENDIF
	ENDIF
ENDPROC

FUNC STRING GET_AIR_RESTRICTED_DIALOUGE()
	SWITCH serverBD.iRoute
		CASE 0	RETURN "ARMY_ANN1"
		CASE 1	RETURN "ARMY_ANN2"
		CASE 2	RETURN "ARMY_ANN3"
		CASE 3	RETURN "ARMY_ANN4"
		CASE 4	RETURN "ARMY_ANN1"
	ENDSWITCH 
	RETURN "ARMY_ANN1"
ENDFUNC

PROC MAINTAIN_DIALOGUE()
	IF bAIR_RESTRICTED()
		//If it's the last contraband
		IF iDROPPED_OFF_COUNT() = (iDROP_TARGET() - 1)
			//Set up the ped
			IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_DIALOUGE_SET_UP)
				//But only if in the area
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				AND IS_ENTITY_AT_COORD(PLAYER_PED_ID(), GET_SELL_DROP_COORDS(GET_SELL_VAR(), serverBD.iClosestDropOff[0], serverBD.iRoute, 0), vLOCATE_DIMENSION()+<<10.0, 10.0, 10.0>>)
					ADD_PED_FOR_DIALOGUE(sLocaldata.sLocalPedStruct, 3, NULL, "ARMT_ANNOUNCER")
					PRINTLN("[EXEC1] [CTRB_SELL] [CRT]  - MAINTAIN_DIALOGUE - ADD_PED_FOR_DIALOGUE(sLocaldata.sLocalPedStruct, 3, NULL, \"ARMT_ANNOUNCER\")") 
					SET_LOCAL_BIT0(eLOCALBITSET0_DIALOUGE_SET_UP)
				ENDIF
			ELIF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_DIALOUGE_TRIGGERED_01)
//				IF CREATE_CONVERSATION(sLocaldata.sLocalPedStruct, "ARMYAUD", GET_AIR_RESTRICTED_DIALOUGE(), CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES, DO_NOT_ADD_TO_BRIEF_SCREEN)
				IF CREATE_CONVERSATION_USING_V_CONTENT_IN_DLC(sLocaldata.sLocalPedStruct, "ARMYAUD", GET_AIR_RESTRICTED_DIALOUGE(), CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES, DO_NOT_ADD_TO_BRIEF_SCREEN) 
					PRINTLN("[EXEC1] [CTRB_SELL] [CRT]  - MAINTAIN_DIALOGUE - CREATE_CONVERSATION(sLocaldata.sLocalPedStruct, \"ARMYAUD\", ", GET_AIR_RESTRICTED_DIALOUGE(), ", CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES, DO_NOT_ADD_TO_BRIEF_SCREEN)") 
					SET_LOCAL_BIT0(eLOCALBITSET0_DIALOUGE_TRIGGERED_01)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_LAND_PLANE_CLIENT()
	IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_PRINT_LAND_HELP)
		IF bOK_TO_DO_CRATES()
			INT i
			REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
				IF IS_DRIVER_OF_SELL_VEH(i)
				AND serverBD.iNumCrates[i] < ciMAX_CRATES
					IF IS_SELL_VEH_OK(i)
					AND IS_ENTITY_AT_COORD(VEH_ENT(i), GET_SELL_DROP_COORDS(GET_SELL_VAR(), serverBD.iClosestDropOff[i], serverBD.iRoute, i), vLOCATE_DIMENSION(TRUE))
						// "Press ~INPUT_FRONTEND_RIGHT~ to drop Contraband."
						PRINT_HELP("SCONTRA_CLAND")
						SET_LOCAL_BIT0(eLOCALBITSET0_PRINT_LAND_HELP)
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
ENDPROC

FUNC VECTOR GET_SET_VEHICLE_CREATE_OFFSET()
	IF NOT bAIR_VARIATION()
		RETURN <<0.0, -6.0, -0.32>> 
	ENDIF
	
	IF GET_SHIPMENT_TYPE() = eSELL_SHIPMENT_TYPE_AIR_ONE_TITAN
		RETURN <<0.0, 0.0, -5.0>> 
	ENDIF
	
	RETURN <<0.0, 0.0, -2.0>> 
ENDFUNC
PROC MAINTAIN_CRATE_DROP_SERVER()	
	INT i
	INT iBitToCheck
	eSERVER_BITSET_0 eServerBitToCheck
	IF bOK_TO_DO_CRATES()
		IF LOAD_CRATE_ASSETS()
			REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
			
				iBitToCheck = ENUM_TO_INT(eSERVERBITSET0_CRATE_DROP1) + i
				eServerBitToCheck = INT_TO_ENUM(eSERVER_BITSET_0, iBitToCheck)
				
				IF IS_SERVER_BIT0_SET(eServerBitToCheck)
					IF serverBD.iNumCrates[i] < ciMAX_CRATES
						SWITCH serverBD.iCrateProgress[i]
							CASE 0
								PRINTLN("[EXEC1] [CTRB_SELL] [CRT]  - MAINTAIN_CRATE_DROP_SERVER CASE 0 veh ", i) 
								serverBD.iCrateProgress[i]++
							BREAK
							CASE 1
							//	IF serverBD.iVehForCrateDrop <> -1
								IF IS_BIT_SET(serverBD.iVehForCrateDropBitSet, i)
									IF IS_SELL_VEH_OK(i)
									AND CREATE_DROPPED_CRATE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(VEH_ENT(i), GET_SET_VEHICLE_CREATE_OFFSET()), fVEH_HEADING(i), i )
										serverBD.iNumCrates[i]++
										PRINTLN("[EXEC1] [CTRB_SELL] [CRT]  - MAINTAIN_CRATE_DROP_SERVER, serverBD.iNumCrates[",i ,"] = ", serverBD.iNumCrates[i]) 
										CLEAR_SERVER_BIT0(eServerBitToCheck)
										serverBD.iCrateProgress[i] = 0
										CLEAR_BIT(serverBD.iVehForCrateDropBitSet, i)
									ENDIF
								ELSE
									CLEAR_SERVER_BIT0(eServerBitToCheck)
									
									PRINTLN("[EXEC1] [CTRB_SELL] [CRT] CLEARing server bit as bit ", i, " not set" )
								ENDIF
							BREAK
						ENDSWITCH
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
	
	CONTROL_CRATE_LANDING()
ENDPROC

//╔═════════════════════════════════════════════════════════════════════════════╗
//║				FX																║

//Loads all particle effects required
FUNC BOOL LOAD_INTERACT_PARTICLE_FX()
	REQUEST_NAMED_PTFX_ASSET("scr_lowrider")

	IF HAS_NAMED_PTFX_ASSET_LOADED("scr_lowrider")
		//NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - LOAD_INTERACT_PARTICLE_FX - DONE ") NET_NL()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC FLOAT fPARTICLE_SCALE()

	#IF IS_DEBUG_BUILD
	IF tfScale <> cfPARTICLE_SCALE
		
		RETURN tfScale
	ENDIF
	#ENDIF
	
	RETURN cfPARTICLE_SCALE
ENDFUNC

PROC ADD_PARTICLE_FX(INT iCrate, VECTOR vPos)
	IF LOAD_INTERACT_PARTICLE_FX()
		IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(sLocaldata.ptfxFlare[iCrate])		//scr_drug_traffic_flare_L
			BOOL bLocalOnly = TRUE
			USE_PARTICLE_FX_ASSET("scr_lowrider")
			sLocaldata.ptfxFlare[iCrate] = START_PARTICLE_FX_LOOPED_AT_COORD("scr_lowrider_flare" , vPos, <<0, 0, -0.2>>, fPARTICLE_SCALE(), DEFAULT, DEFAULT, DEFAULT, bLocalOnly)//, <<0, 0, 0>>)
			
//			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_SpecialCrateDrop)
				SET_PARTICLE_FX_LOOPED_COLOUR(sLocaldata.ptfxFlare[iCrate], 0.8, 0.18, 0.19, bLocalOnly)
//			ELSE
//				SET_PARTICLE_FX_LOOPED_COLOUR(sLocaldata.ptfxFlare[iCrate], 1.0, 0.84, 0.0)
//			ENDIF
			PRINTLN("     ---------->    CRATE DROP - ADD_CRATE_FLARES ", iCrate, " at coords ",vPos) 
		ENDIF
	ENDIF			
ENDPROC

//PURPOSE: Removes a flare
PROC CLEANUP_CRATE_FLARE(INT iCrate)
	IF sLocaldata.ptfxFlare[iCrate] <> NULL
		IF DOES_PARTICLE_FX_LOOPED_EXIST(sLocaldata.ptfxFlare[iCrate])
			STOP_PARTICLE_FX_LOOPED(sLocaldata.ptfxFlare[iCrate])
			NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - CLEANUP_CRATE_FLARE ") NET_PRINT_INT(iCrate) NET_NL()
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Loops through and cleans up all flares
PROC CLEANUP_ALL_CRATE_FLARES()
	INT i
	REPEAT ciMAX_SELL_DROP_OFFS i
		CLEANUP_CRATE_FLARE(i)
	ENDREPEAT
ENDPROC

PROC CLEANUP_PLANE_FX()
	IF DOES_PARTICLE_FX_LOOPED_EXIST(sLocaldata.ptfxPlane)
		REMOVE_PARTICLE_FX(sLocaldata.ptfxPlane)
	ENDIF
//	REMOVE_PTFX_ASSET()
ENDPROC

//╔═════════════════════════════════════════════════════════════════════════════╗
//║				SERVER															║

FUNC INT STARTING_WAREHOUSE()
	RETURN serverBD.iStartingWarehouse
ENDFUNC

#IF IS_DEBUG_BUILD
CONST_INT debug_NUM_VARIATIONS_IN_PLAYLIST 3

FUNC eSELL_VARIATION GET_NEXT_VARIATION_IN_PLAYLIST()
	SWITCH MPGlobalsAmbience.sMagnateGangBossData.iSellMissionPlaylistCount
		CASE 0	RETURN	eVAR_AIR_DROP
	//	CASE 1	RETURN 	eVAR_LAND_BREAKDOWN
	//	CASE 2	RETURN 	eVAR_AIR_FAILURE
	ENDSWITCH
	
	RETURN eVAR_AIR_DROP
ENDFUNC
#ENDIF

FUNC INT GET_TYPE_NUMBER(eSELL_VARIATION eSellVar)

	IF 		IS_THIS_A_LAND_VARIATION(eSellVar)	RETURN 0
	ELIF	IS_THIS_AN_AIR_VARIATION(eSellVar)	RETURN 1
	ELIF	IS_THIS_A_SEA_VARIATION(eSellVar)	RETURN 2
	ENDIF
	
	RETURN -1

ENDFUNC

FUNC BOOL IS_SELL_TYPE_DIFFERENT_FROM_PREVIOUS(eSELL_VARIATION eSellVar)
	
	RETURN MPGlobalsAmbience.sMagnateGangBossData.iTypeOfLastSellMission != GET_TYPE_NUMBER(eSellVar)

ENDFUNC

PROC SET_PREVIOUS_SELL_TYPE(eSELL_VARIATION eSellVar)

	MPGlobalsAmbience.sMagnateGangBossData.iTypeOfLastSellMission = GET_TYPE_NUMBER(eSellVar)
	PRINTLN("[EXEC1] [CTRB_SELL] SET_PREVIOUS_SELL_TYPE = ",MPGlobalsAmbience.sMagnateGangBossData.iTypeOfLastSellMission)

ENDPROC

FUNC INT GET_HISTORY_LIST_SIZE()
	RETURN g_sMPTunables.iEXEC_SELL_MISSIONS_HISTORY
ENDFUNC

FUNC BOOL IS_VARIATION_IN_HISTORY_LIST(eSELL_VARIATION eSellVar)
	INT i
	REPEAT GET_HISTORY_LIST_SIZE() i
		IF MPGlobalsAmbience.sMagnateGangBossData.eSellMissionHistoryList[i] = eSellVar
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

PROC ADD_VARIATION_TO_HISTORY_LIST(eSELL_VARIATION eSellVar)
	// If there's no variation in slot 0, add it and exit
	IF MPGlobalsAmbience.sMagnateGangBossData.eSellMissionHistoryList[0] = eVAR_EMPTY
		MPGlobalsAmbience.sMagnateGangBossData.eSellMissionHistoryList[0] = eSellVar
		EXIT
	ELSE
		// There's already a variation in slot 0 for the new variation, bump all other variations down the list, and add the variation to slot 0
		MPGlobalsAmbience.sMagnateGangBossData.eSellMissionHistoryList[7] = MPGlobalsAmbience.sMagnateGangBossData.eSellMissionHistoryList[6]
		MPGlobalsAmbience.sMagnateGangBossData.eSellMissionHistoryList[6] = MPGlobalsAmbience.sMagnateGangBossData.eSellMissionHistoryList[5]
		MPGlobalsAmbience.sMagnateGangBossData.eSellMissionHistoryList[5] = MPGlobalsAmbience.sMagnateGangBossData.eSellMissionHistoryList[4]
		MPGlobalsAmbience.sMagnateGangBossData.eSellMissionHistoryList[4] = MPGlobalsAmbience.sMagnateGangBossData.eSellMissionHistoryList[3]
		MPGlobalsAmbience.sMagnateGangBossData.eSellMissionHistoryList[3] = MPGlobalsAmbience.sMagnateGangBossData.eSellMissionHistoryList[2]
		MPGlobalsAmbience.sMagnateGangBossData.eSellMissionHistoryList[2] = MPGlobalsAmbience.sMagnateGangBossData.eSellMissionHistoryList[1]
		MPGlobalsAmbience.sMagnateGangBossData.eSellMissionHistoryList[1] = MPGlobalsAmbience.sMagnateGangBossData.eSellMissionHistoryList[0]
		MPGlobalsAmbience.sMagnateGangBossData.eSellMissionHistoryList[0] = eSellVar
	ENDIF
ENDPROC

FUNC BOOL IS_VARIATION_BLOCKED(eSELL_VARIATION eSellVar)
	SWITCH eSellVar
		CASE eVAR_DEFAULT
			IF g_sMPTunables.bexec_disable_sell_default
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE eVAR_MULTIPLE
			IF g_sMPTunables.bexec_disable_sell_multiple
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE eVAR_STING
			IF g_sMPTunables.bexec_disable_sell_sting
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE eVAR_LAND_DAMAGE
			IF g_sMPTunables.bexec_disable_sell_nodamage
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE eVAR_LAND_TRACKIFY
			IF g_sMPTunables.bexec_disable_sell_trackify
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE eVAR_LAND_ATTACK
			IF g_sMPTunables.bexec_disable_sell_attacked
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE eVAR_LAND_DEFEND
			IF g_sMPTunables.bexec_disable_sell_defend
				RETURN TRUE
			ENDIF
		BREAK
				
		CASE eVAR_AIR_LOW
			IF g_sMPTunables.bexec_disable_sell_airflylow
				RETURN TRUE
			ENDIF
		BREAK
				
		CASE eVAR_AIR_DROP
			IF g_sMPTunables.bexec_disable_sell_airdrop
				RETURN TRUE
			ENDIF
		BREAK
				
		CASE eVAR_AIR_CLEAR_AREA
			IF g_sMPTunables.bexec_disable_sell_aircleararea
				RETURN TRUE
			ENDIF
		BREAK
				
		CASE eVAR_AIR_RESTRICTED
			IF g_sMPTunables.bexec_disable_sell_airrestricted
				RETURN TRUE
			ENDIF
		BREAK
				
		CASE eVAR_AIR_ATTACK
			IF g_sMPTunables.bexec_disable_sell_airattacked
				RETURN TRUE
			ENDIF
		BREAK
				
		CASE eVAR_SEA_ATTACK
			IF g_sMPTunables.bexec_disable_sell_seaattacked
				RETURN TRUE
			ENDIF
		BREAK
				
		CASE eVAR_SEA_DEFEND
			IF g_sMPTunables.bexec_disable_sell_seadefend
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_OTHER_BOSS_ON_VARIATION(eSELL_VARIATION eSellVar)
	INT i
	INT iVar
	PLAYER_INDEX playerID
	
	REPEAT NUM_NETWORK_PLAYERS i
		IF NETWORK_IS_PLAYER_ACTIVE(INT_TO_PLAYERINDEX(i))
			playerID = INT_TO_PLAYERINDEX(i)
			
			IF GB_IS_PLAYER_BOSS_OF_A_GANG(playerID)
				IF GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(playerID) = FMMC_TYPE_GB_CONTRABAND_SELL
					iVar = GB_GET_CONTRABAND_VARIATION_PLAYER_IS_ON(playerID)
					
					IF ENUM_TO_INT(eSellVar) = iVar
						PRINTLN("[EXEC1] [CTRB_SELL] IS_OTHER_BOSS_ON_VARIATION - Yes - Boss ", GET_PLAYER_NAME(playerID), " is already on ", sGET_VARIATION(eSellVar))
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC





FUNC BOOL IS_VARIATION_VALID_FOR_CONTRA_AMOUNT(eSELL_VARIATION eSellVar)
	#IF IS_DEBUG_BUILD
	PRINTLN("[EXEC1] [CTRB_SELL] [IS_VARIATION_VALID_FOR_CONTRA_AMOUNT] Called with eSellVar = ", sGET_VARIATION(eSellVar), " g_iContraSellMissionCratesToSell = ", g_iContraSellMissionCratesToSell, " g_iContraSellMissionBuyerID = ", g_iContraSellMissionBuyerID) 
	#ENDIF
	

	SWITCH eSellVar
		CASE eVAR_MULTIPLE
		CASE eVAR_STING
		CASE eVAR_AIR_RESTRICTED
			RETURN g_iContraSellMissionCratesToSell >= 5
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC eSELL_VARIATION GET_RANDOM_VARIATION_FOR_SPECIAL_ITEMS()
	INT iRand = GET_RANDOM_INT_IN_RANGE(0, 8)
	
	SWITCH iRand
		CASE 0	RETURN eVAR_DEFAULT
		CASE 1	RETURN eVAR_LAND_ATTACK
		CASE 2	RETURN eVAR_LAND_DEFEND
		CASE 3	RETURN eVAR_LAND_DAMAGE
		CASE 4	RETURN eVAR_LAND_TRACKIFY
		CASE 5	RETURN eVAR_AIR_CLEAR_AREA
		CASE 6	RETURN eVAR_SEA_ATTACK
		CASE 7	RETURN eVAR_SEA_DEFEND
	ENDSWITCH
	
	RETURN eVAR_DEFAULT
ENDFUNC

FUNC BOOL SERVER_PICK_VARIATION()

	IF GET_SELL_VAR() = eVAR_EMPTY
	
		#IF IS_DEBUG_BUILD
		
		// Debug menu
		serverBD.iDebugVariation2 = GB_GET_PLAYER_GANG_BOSS_MISSION_VARIATION2()
		PRINTLN("[EXEC1] [CTRB_SELL] SERVER_DEBUG_INIT [DEBUG_OVERRIDE], iDebugVariation2 =  ", serverBD.iDebugVariation2)
	
		// Debug override
		IF serverBD.iDebugVariation2 <> -1
		
			serverBD.eSellVar = INT_TO_ENUM(eSELL_VARIATION, serverBD.iDebugVariation2)
			SERVER_SET_SHIPMENT_TYPE()
			IF bMULTIPLE()
			OR bSTING()
			OR bAIR_VARIATION()
			OR bSEA_VARIATION()
				MPGlobalsAmbience.sMagnateGangBossData.iSellMissionVariationCounter[serverBD.iDebugVariation2]++
				IF MPGlobalsAmbience.sMagnateGangBossData.iSellMissionVariationCounter[serverBD.iDebugVariation2] >= 5
					MPGlobalsAmbience.sMagnateGangBossData.iSellMissionVariationCounter[serverBD.iDebugVariation2] = 0
				ENDIF
				IF NOT bSEA_VARIATION()
					serverBD.iRoute = MPGlobalsAmbience.sMagnateGangBossData.iSellMissionVariationCounter[serverBD.iDebugVariation2]
					SET_SERVER_BIT0(eSERVERBITSET0_ROUTE_SET)
					PRINTLN("[EXEC1] [CTRB_SELL] - SERVER_PICK_VARIATION - serverBD.iRoute being set to ", serverBD.iRoute)
				ENDIF
			ELSE
				MPGlobalsAmbience.sMagnateGangBossData.iSellMissionVariationCounter[serverBD.iDebugVariation2]++
				IF MPGlobalsAmbience.sMagnateGangBossData.iSellMissionVariationCounter[serverBD.iDebugVariation2] >= 20
					MPGlobalsAmbience.sMagnateGangBossData.iSellMissionVariationCounter[serverBD.iDebugVariation2] = 0
				ENDIF
				serverBD.iFoundDropCount = 0
				INT iCount, iNumVeh
				iCount = MPGlobalsAmbience.sMagnateGangBossData.iSellMissionVariationCounter[serverBD.iDebugVariation2]
				serverBD.iFoundDropCount++
				REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() iNumVeh
					SET_BIT(serverBD.iActiveDropOffBitset[iNumVeh], iCount)	
					SET_BIT(serverBD.dropOffLocationBitSet, iCount)	
					PRINTLN("[EXEC1] [CTRB_SELL] DBG SET_BIT(serverBD.iActiveDropOffBitset, ", iCount, ")")
				ENDREPEAT
			ENDIF
			
			
			PRINTLN("[EXEC1] [CTRB_SELL] SERVER_DEBUG_INIT [DEBUG_OVERRIDE], iSellMissionVariationCounter[",serverBD.iDebugVariation2,"] is now ",MPGlobalsAmbience.sMagnateGangBossData.iSellMissionVariationCounter[serverBD.iDebugVariation2])
		ELSE
		
		#ENDIF
	
			eSELL_VARIATION eSellVar
	
			IF GET_DOES_BUYER_ONLY_WANT_SPECIAL_ITEMS()
				eSellVar = GET_RANDOM_VARIATION_FOR_SPECIAL_ITEMS()
			ELSE
				eSellVar = INT_TO_ENUM(eSELL_VARIATION, GET_RANDOM_INT_IN_RANGE(0, ENUM_TO_INT(eVAR_EMPTY)))
			ENDIF
			
			IF NOT IS_VARIATION_IN_HISTORY_LIST(eSellVar)
			AND NOT IS_VARIATION_BLOCKED(eSellVar)
			AND NOT IS_OTHER_BOSS_ON_VARIATION(eSellVar)
			AND IS_VARIATION_VALID_FOR_CONTRA_AMOUNT(eSellVar)
			AND IS_SELL_TYPE_DIFFERENT_FROM_PREVIOUS(eSellVar)
				serverBD.eSellVar = eSellVar
				ADD_VARIATION_TO_HISTORY_LIST(eSellVar)
				SET_PREVIOUS_SELL_TYPE(eSellVar)
			ENDIF
		
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF // #IF IS_DEBUG_BUILD
		
		#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_FlowPlaySellMissionPlaylist")
			IF MPGlobalsAmbience.sMagnateGangBossData.iSellMissionPlaylistCount = -1
			OR MPGlobalsAmbience.sMagnateGangBossData.iSellMissionPlaylistCount = debug_NUM_VARIATIONS_IN_PLAYLIST
				MPGlobalsAmbience.sMagnateGangBossData.iSellMissionPlaylistCount = 0
			ENDIF
			
			serverBD.eSellVar = GET_NEXT_VARIATION_IN_PLAYLIST()
			
			PRINTLN("[EXEC1] [CTRB_SELL] SERVER_PICK_VARIATION - On Flow Play Playlist, setting to variation ", MPGlobalsAmbience.sMagnateGangBossData.iSellMissionPlaylistCount)
			PRINTLN("[EXEC1] [CTRB_SELL] SERVER_PICK_VARIATION - On Flow Play Playlist, variation name: ", sGET_VARIATION())
			
		ENDIF
		#ENDIF
		
		PRINTLN("[EXEC1] [CTRB_SELL] SERVER_PICK_VARIATION = ", sGET_VARIATION())
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ROUTE_BLOCKED(INT iPos)
	SWITCH GET_SELL_VAR()
		CASE eVAR_AIR_ATTACK
			SWITCH iPos
				CASE 0	RETURN g_sMPTunables.bexec_disable_sell_airattacked_1
				CASE 1	RETURN g_sMPTunables.bexec_disable_sell_airattacked_2
				CASE 2	RETURN g_sMPTunables.bexec_disable_sell_airattacked_3
				CASE 3	RETURN g_sMPTunables.bexec_disable_sell_airattacked_4
				CASE 4	RETURN g_sMPTunables.bexec_disable_sell_airattacked_5
			ENDSWITCH
		BREAK
		
		CASE eVAR_AIR_CLEAR_AREA
			SWITCH iPos
				CASE 0	RETURN g_sMPTunables.bexec_disable_sell_aircleararea_1
				CASE 1	RETURN g_sMPTunables.bexec_disable_sell_aircleararea_2
				CASE 2	RETURN g_sMPTunables.bexec_disable_sell_aircleararea_3
				CASE 3	RETURN g_sMPTunables.bexec_disable_sell_aircleararea_4
				CASE 4	RETURN g_sMPTunables.bexec_disable_sell_aircleararea_5
			ENDSWITCH
		BREAK
		
		CASE eVAR_AIR_DROP
			SWITCH iPos
				CASE 0	RETURN g_sMPTunables.bexec_disable_sell_airdrop_1
				CASE 1	RETURN g_sMPTunables.bexec_disable_sell_airdrop_2
				CASE 2	RETURN g_sMPTunables.bexec_disable_sell_airdrop_3
				CASE 3	RETURN g_sMPTunables.bexec_disable_sell_airdrop_4
				CASE 4	RETURN g_sMPTunables.bexec_disable_sell_airdrop_5
			ENDSWITCH
		BREAK
		
		CASE eVAR_AIR_RESTRICTED
			SWITCH iPos
				CASE 0	RETURN g_sMPTunables.bexec_disable_sell_airrestricted_1
				CASE 1	RETURN g_sMPTunables.bexec_disable_sell_airrestricted_2
				CASE 2	RETURN g_sMPTunables.bexec_disable_sell_airrestricted_3
				CASE 3	RETURN g_sMPTunables.bexec_disable_sell_airrestricted_4
				CASE 4	RETURN g_sMPTunables.bexec_disable_sell_airrestricted_5
			ENDSWITCH
		BREAK
	
		CASE eVAR_MULTIPLE
			SWITCH iPos
				CASE 0	RETURN g_sMPTunables.bexec_disable_sell_multiple_1
				CASE 1	RETURN g_sMPTunables.bexec_disable_sell_multiple_2
				CASE 2	RETURN g_sMPTunables.bexec_disable_sell_multiple_3
				CASE 3	RETURN g_sMPTunables.bexec_disable_sell_multiple_4
				CASE 4	RETURN g_sMPTunables.bexec_disable_sell_multiple_5
			ENDSWITCH
		BREAK
		
		CASE eVAR_STING
			SWITCH iPos
				CASE 0	RETURN g_sMPTunables.bexec_disable_sell_sting_1
				CASE 1	RETURN g_sMPTunables.bexec_disable_sell_sting_2
				CASE 2	RETURN g_sMPTunables.bexec_disable_sell_sting_3
				CASE 3	RETURN g_sMPTunables.bexec_disable_sell_sting_4
				CASE 4	RETURN g_sMPTunables.bexec_disable_sell_sting_5
			ENDSWITCH
		BREAK
				
		CASE eVAR_SEA_ATTACK
			SWITCH iPos
				CASE 0	RETURN g_sMPTunables.bexec_disable_sell_seaattacked_1
				CASE 1	RETURN g_sMPTunables.bexec_disable_sell_seaattacked_2
				CASE 2	RETURN g_sMPTunables.bexec_disable_sell_seaattacked_3
				CASE 3	RETURN g_sMPTunables.bexec_disable_sell_seaattacked_4
				CASE 4	RETURN g_sMPTunables.bexec_disable_sell_seaattacked_5
			ENDSWITCH
		BREAK
				
		CASE eVAR_SEA_DEFEND
			SWITCH iPos
				CASE 0	RETURN g_sMPTunables.bexec_disable_sell_seadefend_1
				CASE 1	RETURN g_sMPTunables.bexec_disable_sell_seadefend_2
				CASE 2	RETURN g_sMPTunables.bexec_disable_sell_seadefend_3
				CASE 3	RETURN g_sMPTunables.bexec_disable_sell_seadefend_4
				CASE 4	RETURN g_sMPTunables.bexec_disable_sell_seadefend_5
			ENDSWITCH
		BREAK		
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

// Select the drop-off route
FUNC BOOL SERVER_SET_ROUTE()
	
	INT iRoute
	
	IF NOT IS_SERVER_BIT0_SET(eSERVERBITSET0_ROUTE_SET)
	
		IF bMULTIPLE()
		OR bSTING()
		OR bAIR_VARIATION()
			
			iRoute = GET_RANDOM_INT_IN_RANGE(0, 5)
			
			IF IS_ROUTE_BLOCKED(iRoute)
				RETURN FALSE
			ENDIF
							
			serverBD.iRoute = iRoute
			
			#IF IS_DEBUG_BUILD
				IF GB_GET_PLAYER_GANG_BOSS_MISSION_VARIATION() <> -1
					serverBD.iRoute = GB_GET_PLAYER_GANG_BOSS_MISSION_VARIATION()	
					PRINTLN("[EXEC1] [CTRB_SELL] SERVER_SET_ROUTE - Debug - serverBD.iRoute = ", serverBD.iRoute)
				ENDIF
			#ENDIF
			
			PRINTLN("[EXEC1] [CTRB_SELL] SERVER_SET_ROUTE, >>> ", serverBD.iRoute)
			
			SET_SERVER_BIT0(eSERVERBITSET0_ROUTE_SET)
			
		// Sea variations have 1 route per start point
		ELIF bSEA_VARIATION()
		
			iRoute = GET_RANDOM_INT_IN_RANGE(0, 5)
		
			IF IS_ROUTE_BLOCKED(iRoute)
			#IF IS_DEBUG_BUILD
			AND GB_GET_PLAYER_GANG_BOSS_MISSION_VARIATION() = -1
			#ENDIF
				RETURN FALSE
			ENDIF
		
			#IF IS_DEBUG_BUILD
				IF GB_GET_PLAYER_GANG_BOSS_MISSION_VARIATION() <> -1
					iRoute = GB_GET_PLAYER_GANG_BOSS_MISSION_VARIATION()	
					PRINTLN("[EXEC1] [CTRB_SELL] SERVER_SET_ROUTE - Debug - iRoute = ", iRoute)
				ENDIF
			#ENDIF
		
			serverBD.iRoute = iRoute
			serverBD.iStartingPos[0] = iRoute
			PRINTLN("[EXEC1] [CTRB_SELL] SERVER_SET_ROUTE (Sea variation), >>> ", serverBD.iRoute)
			
			SET_SERVER_BIT0(eSERVERBITSET0_ROUTE_SET)
		ELSE
			// Other variations dont require a route
			SET_SERVER_BIT0(eSERVERBITSET0_ROUTE_SET)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN IS_SERVER_BIT0_SET(eSERVERBITSET0_ROUTE_SET)
ENDFUNC

// Choose vehicle start pos.
FUNC BOOL SERVER_SET_START_POS()	

	INT iStartPos0, iStartPos1, iStartPos2
	
	IF NOT IS_SERVER_BIT0_SET(eSERVERBITSET0_START_POS_SET)
		INT iBossId = GB_GET_GANG_ID_FROM_BOSS(PLAYER_ID())
		
		#IF IS_DEBUG_BUILD
			IF iBossId > 9
				NET_SCRIPT_ASSERT("SERVER_SET_START_POS iBossId > 9! Will use 0")
				PRINTLN("[EXEC1] [CTRB_SELL] SERVER_SET_START_POS iBossId > 9! Will use 0")
				iBossId = 0
			ELIF iBossId < 0
				NET_SCRIPT_ASSERT("SERVER_SET_START_POS iBossId < 0! Will use 0")
				PRINTLN("[EXEC1] [CTRB_SELL] SERVER_SET_START_POS iBossId < 0! Will use 0")
				iBossId = 0
			ENDIF
		#ENDIF
				
		PRINTLN("[EXEC1] [CTRB_SELL] SERVER_SET_START_POS iBossId = ", iBossId)
		
		IF bAIR_VARIATION()
			SWITCH GET_SHIPMENT_TYPE()
				CASE eSELL_SHIPMENT_TYPE_AIR_ONE_CUBAN800
					iStartPos0 = iBossId * 3 //GET_RANDOM_INT_IN_RANGE(0, 20)
				BREAK
				
				CASE eSELL_SHIPMENT_TYPE_AIR_TWO_CUBAN800
					iStartPos0 = iBossId * 3 //GET_RANDOM_INT_IN_RANGE(0, 20)
					iStartPos1 = iStartPos0 + 1 //GET_RANDOM_INT_IN_RANGE(0, 20)
					
					IF iStartPos0 = iStartPos1
						RETURN FALSE
					ENDIF
				BREAK
				
				CASE eSELL_SHIPMENT_TYPE_AIR_THREE_CUBAN800
					iStartPos0 = iBossId * 3 //GET_RANDOM_INT_IN_RANGE(0, 20)
					iStartPos1 = iStartPos0 + 1 //GET_RANDOM_INT_IN_RANGE(0, 20)
					iStartPos2 = iStartPos1 + 1 //GET_RANDOM_INT_IN_RANGE(0, 20)
					
					IF iStartPos0 = iStartPos1
					OR iStartPos0 = iStartPos2
					OR iStartPos1 = iStartPos2
						RETURN FALSE
					ENDIF
				BREAK
				
				CASE eSELL_SHIPMENT_TYPE_AIR_ONE_TITAN
					iStartPos0 = GET_RANDOM_INT_IN_RANGE(0, 12)
				BREAK
			ENDSWITCH
		ELSE
			iStartPos0 = GET_RANDOM_INT_IN_RANGE(0, 5)
						
			#IF IS_DEBUG_BUILD
				IF GB_GET_PLAYER_GANG_BOSS_MISSION_VARIATION() <> -1
					iStartPos0 = GB_GET_PLAYER_GANG_BOSS_MISSION_VARIATION()	
					PRINTLN("[EXEC1] [CTRB_SELL] SERVER_SET_START_POS - Debug - iStartPos0 = ", iStartPos0)
				ENDIF
			#ENDIF
		ENDIF

		serverBD.iStartingPos[0] = iStartPos0
		serverBD.iStartingPos[1] = iStartPos1
		serverBD.iStartingPos[2] = iStartPos2

		PRINTLN("[EXEC1] [CTRB_SELL] SERVER_SET_START_POS, >>> ", serverBD.iStartingPos[0])
		PRINTLN("[EXEC1] [CTRB_SELL] SERVER_SET_START_POS, >>> ", serverBD.iStartingPos[1])
		PRINTLN("[EXEC1] [CTRB_SELL] SERVER_SET_START_POS, >>> ", serverBD.iStartingPos[2])
		
		SET_SERVER_BIT0(eSERVERBITSET0_START_POS_SET)
	ENDIF
	
	RETURN IS_SERVER_BIT0_SET(eSERVERBITSET0_START_POS_SET)
ENDFUNC

// Choose which drop-off will contain the wanted rating for the Sting variant.
PROC SERVER_SET_STING()
	
	IF serverBD.iSting = -1
	AND bSTING()
		INT iRand, iSting
		iRand = GET_RANDOM_INT_IN_RANGE(0, 10000)
		
		IF IS_INT_IN_RANGE(iRand, 7500, 10000)
			iSting = 1
		ELIF IS_INT_IN_RANGE(iRand, 5000, 7500)
			iSting = 2
		ELIF IS_INT_IN_RANGE(iRand, 2500, 5000)
			iSting = 3
		ELSE
			iSting = 4
		ENDIF
		
		serverBD.iSting = iSting
		PRINTLN("[EXEC1] [CTRB_SELL] SERVER_SET_ROUTE, >>> ", serverBD.iSting)
	ENDIF
ENDPROC

PROC SERVER_SET_SEA_VARIATION_SAFE_WARP_LOCATION()
	IF bSEA_ATTACK()
		SWITCH serverBD.iStartingPos[0]
			CASE 0	serverBD.vSeaVariationEndWarpLocation =  <<-1419.8027, -1540.2639, 2.0909>>		BREAK
			CASE 1	serverBD.vSeaVariationEndWarpLocation =  <<-141.5667, -2711.0811, 6.0002>>		BREAK
			CASE 2	serverBD.vSeaVariationEndWarpLocation =  <<215.8467, -3336.9417, 5.7982>>		BREAK
			CASE 3	serverBD.vSeaVariationEndWarpLocation =  <<1294.1938, -3217.0327, 5.9059>>		BREAK
			CASE 4	serverBD.vSeaVariationEndWarpLocation =  <<982.3854, -3343.4568, 6.0958>>		BREAK
		ENDSWITCH
	ENDIF
	
	IF bSEA_DEFEND()
		SWITCH serverBD.iStartingPos[0]
			CASE 0	serverBD.vSeaVariationEndWarpLocation =  <<133.834, -3336.7249, 6.0218>>		BREAK
			CASE 1	serverBD.vSeaVariationEndWarpLocation =  <<1293.4642, -3126.0784, 5.906>>	BREAK
			CASE 2	serverBD.vSeaVariationEndWarpLocation =  <<-761.9681, -2840.4038, 13.9415>>		BREAK
			CASE 3	serverBD.vSeaVariationEndWarpLocation =  <<-1331.2817, -1693.3656, 2.1151>>		BREAK
			CASE 4	serverBD.vSeaVariationEndWarpLocation =  <<-587.2385, -2362.5408, 13.8282>>		BREAK
		ENDSWITCH
	ENDIF
ENDPROC

FUNC BOOL bSHOULD_CHECK_MIN_DISTANCE()

	IF bNORMAL()
	OR bLAND_DAMAGE()
	OR bLAND_ATTACK()
	OR bLAND_DEFEND()
	OR bLAND_TRACKIFY()
	
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

// 2757409 ensure drop-off is far enough away from start pos
FUNC BOOL bPASSED_DIST_CHECK( INT iCount)
	FLOAT fCurrentDist
	VECTOR vWarehouseCoords
	IF bSHOULD_CHECK_MIN_DISTANCE()
		
		vWarehouseCoords = GET_WAREHOUSE_COORDS(serverBD.iStartingWarehouse)
		
		INT iVeh
		REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() iVeh
			
			fCurrentDist = GET_DISTANCE_BETWEEN_COORDS(GET_SELL_DROP_COORDS(GET_SELL_VAR(), iCount, serverBD.iRoute, iVeh), vWarehouseCoords)
			
			PRINTLN("bPASSED_DIST_CHECK, iCount = ", iCount, " fCurrentDist = ", fCurrentDist, " iVeh = ", iVeh)
			
			// As long as one of the drop offs is > 1000m away, use these/this route
			IF fCurrentDist >= g_sMPTunables.iEXEC_SELL_MIN_SPAWN_DISTANCE_FROM_OFFICE
				RETURN TRUE
			ENDIF
		ENDREPEAT
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_GLOBAL_SELL_COORD_BLOCKED(INT iCoord)
	SWITCH iCoord
		CASE 0	RETURN g_sMPTunables.bexec_disable_sell_coord_global0
		CASE 1	RETURN g_sMPTunables.bexec_disable_sell_coord_global1
		CASE 2	RETURN g_sMPTunables.bexec_disable_sell_coord_global2
		CASE 3	RETURN g_sMPTunables.bexec_disable_sell_coord_global3
		CASE 4	RETURN g_sMPTunables.bexec_disable_sell_coord_global4
		CASE 5	RETURN g_sMPTunables.bexec_disable_sell_coord_global5
		CASE 6	RETURN g_sMPTunables.bexec_disable_sell_coord_global6
		CASE 7	RETURN g_sMPTunables.bexec_disable_sell_coord_global7
		CASE 8	RETURN g_sMPTunables.bexec_disable_sell_coord_global8
		CASE 9	RETURN g_sMPTunables.bexec_disable_sell_coord_global9
		CASE 10	RETURN g_sMPTunables.bexec_disable_sell_coord_global10
		CASE 11	RETURN g_sMPTunables.bexec_disable_sell_coord_global11
		CASE 12	RETURN g_sMPTunables.bexec_disable_sell_coord_global12
		CASE 13	RETURN g_sMPTunables.bexec_disable_sell_coord_global13
		CASE 14	RETURN g_sMPTunables.bexec_disable_sell_coord_global14
		CASE 15	RETURN g_sMPTunables.bexec_disable_sell_coord_global15
		CASE 16	RETURN g_sMPTunables.bexec_disable_sell_coord_global16
		CASE 17	RETURN g_sMPTunables.bexec_disable_sell_coord_global17
		CASE 18	RETURN g_sMPTunables.bexec_disable_sell_coord_global18
		CASE 19	RETURN g_sMPTunables.bexec_disable_sell_coord_global19
	ENDSWITCH

	RETURN FALSE
ENDFUNC

PROC SERVER_CLEAR_LAST_DROP_OFFS()
	INT iLoop
	REPEAT ciMAX_SELL_VEHS iLoop
		serverBD.iLastDropOff[iLoop] = -1
	ENDREPEAT
ENDPROC

FUNC BOOL SERVER_CHOOSE_DROP_OFFS()
	#IF IS_DEBUG_BUILD
	IF serverBD.iDebugVariation2 <> -1
		IF NOT bAIR_VARIATION()
		AND NOT bMULTIPLE()
		AND NOT bSTING()
		AND NOT bSEA_VARIATION()
		
			PRINTLN("SERVER_CHOOSE_DROP_OFFS, DEBUG RETURN TRUE")
			RETURN TRUE
		ENDIF
	ENDIF
	#ENDIF

	INT iRand, iCount
	BOOL bSkipCountCheck
	
	// For these modes we only want the first 5 as they are set coords, see GET_SELL_DROP_COORDS
	IF NOT bAIR_VARIATION()
	AND NOT bMULTIPLE()
	AND NOT bSTING()
	AND NOT bSEA_VARIATION()
		// Or else pick random
		iRand = GET_RANDOM_INT_IN_RANGE(0, ciMAX_SELL_DROP_OFFS)
		bSkipCountCheck = TRUE
		IF IS_GLOBAL_SELL_COORD_BLOCKED(iRand)
			RETURN FALSE
		ENDIF
		iCount = ( iRand + serverBD.iFoundDropCount )
		
		PRINTLN("[EXEC1] [CTRB_SELL] A SERVER_CHOOSE_DROP_OFFS, iCount = ", iCount, " serverBD.iFoundDropCount = ", serverBD.iFoundDropCount)
	ELIF(bAIR_DROP()
	OR bAIR_LOW())
	AND iDROP_TARGET() < ciMAX_AIR_DROP_OFFS
		iRand = GET_RANDOM_INT_IN_RANGE(0, ciMAX_AIR_DROP_OFFS)
		bSkipCountCheck = TRUE
		PRINTLN("[EXEC1] [CTRB_SELL] B SERVER_CHOOSE_DROP_OFFS, NEW_AIR_MODE_LOGIC ") // url:bugstar:2831395 - Sell Missions - Air Drop variations - when selecting less than 10 can we make sure the blips are more spread out
		iCount = iRand 
	ELSE
		iCount = ( iRand + serverBD.iFoundDropCount )
		
		PRINTLN("[EXEC1] [CTRB_SELL] C SERVER_CHOOSE_DROP_OFFS, iCount = ", iCount, " serverBD.iFoundDropCount = ", serverBD.iFoundDropCount, " iDROP_TARGET() = ", iDROP_TARGET())
	ENDIF
	
	IF iCount >= ciMAX_SELL_DROP_OFFS
		iCount = 0
	ENDIF
	
	IF bSEA_VARIATION()
		iCount = 0
	ENDIF
	IF bAIR_RESTRICTED()
		IF serverBD.eShipmentType =  eSELL_SHIPMENT_TYPE_AIR_TWO_CUBAN800
			IF serverBD.iLastDropOff[1] = -1
				serverBD.iLastDropOff[1] = (iDROP_TARGET()/2)-1
				PRINTLN("[EXEC1] [CTRB_SELL] bAIR_RESTRICTED serverBD.iLastDropOff[1] = ", serverBD.iLastDropOff[1])
			ENDIF
			IF serverBD.iLastDropOff[0] = -1
				serverBD.iLastDropOff[0] = (iDROP_TARGET()/2)-1
				PRINTLN("[EXEC1] [CTRB_SELL] bAIR_RESTRICTED serverBD.iLastDropOff[0] = ", serverBD.iLastDropOff[0])
			ENDIF
		ELSE
			IF serverBD.iLastDropOff[0] = -1
				serverBD.iLastDropOff[0] = iDROP_TARGET()-1
				PRINTLN("[EXEC1] [CTRB_SELL] bAIR_RESTRICTED serverBD.iLastDropOff[0] = ", serverBD.iLastDropOff[0])
			ENDIF
		ENDIF
	ENDIF
	
	INT iNumVeh
	IF bPASSED_DIST_CHECK(iCount)		
		IF serverBD.iFoundDropCount <  iDROP_TARGET()
			IF NOT IS_BIT_SET(serverBD.iActiveDropOffBitset[0], iCount)
			
				serverBD.iFoundDropCount++
				
				IF bSkipCountCheck
				OR iCount < iMAX_DROP_OFFS()
					iNumVeh = 0
					REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() iNumVeh
						SET_BIT(serverBD.iActiveDropOffBitset[iNumVeh], iCount)	
						SET_BIT(serverBD.dropOffLocationBitSet, iCount)	
						PRINTLN("[EXEC1] [CTRB_SELL] SET_BIT(serverBD.iActiveDropOffBitset, ", iCount, " iNumVeh = ", iNumVeh, ")")
					ENDREPEAT
				ENDIF
				PRINTLN("[EXEC1] [CTRB_SELL] SERVER_CHOOSE_DROP_OFFS, >>> ", iCount)
				IF NOT bMULTIPLE()
				AND NOT bAIR_VARIATION()
				AND NOT bSTING()
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	RETURN (serverBD.iFoundDropCount = iDROP_TARGET())
ENDFUNC

PROC SERVER_INCREMENT_DROP_COUNT(INT iClosest, INT iVeh = 0)
	IF DOES_SHIPMENT_TYPE_HAVE_MULTIPLE_VEHICLES()
		IF iVeh = 0
			serverBD.iVeh0DropCount++
			PRINTLN("[EXEC1] [CTRB_SELL] SERVER_MAINTAIN_DROP_OFFS - serverBD.iVeh0DropCount = ", serverBD.iVeh0DropCount)
		ELIF iVeh = 1
			serverBD.iVeh1DropCount++
			PRINTLN("[EXEC1] [CTRB_SELL] SERVER_MAINTAIN_DROP_OFFS - serverBD.iVeh1DropCount = ", serverBD.iVeh1DropCount)
		ELSE
			serverBD.iVeh2DropCount++
			PRINTLN("[EXEC1] [CTRB_SELL] SERVER_MAINTAIN_DROP_OFFS - serverBD.iVeh2DropCount = ", serverBD.iVeh2DropCount)
		ENDIF
	ENDIF
	serverBD.iDroppedOffCount++
	PRINTLN("[EXEC1] [CTRB_SELL] SERVER_MAINTAIN_DROP_OFFS - SERVER_INCREMENT_DROP_COUNT, COMPLETE iClosest = ", iClosest, " total now = ", serverBD.iDroppedOffCount, " iVeh = ", iVeh)
	IF bAIR_VARIATION()
		INT i
		REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
			PRINTLN("[EXEC1] [CTRB_SELL] SERVER_MAINTAIN_DROP_OFFS - CLEAR_BIT for i = ", i, " iClosest = ", iClosest)
			CLEAR_BIT(serverBD.iActiveDropOffBitset[i], iClosest)
		ENDREPEAT
	ELSE
		CLEAR_BIT(serverBD.iActiveDropOffBitset[iVeh], iClosest)
	ENDIF
ENDPROC

PROC SET_SELL_VEH_HAS_DELIVERED_CONTRABAND(INT iVeh)
	SWITCH iVeh
		CASE 0
			IF NOT IS_SERVER_BIT0_SET(eSERVERBITSET0_VEH_0_DELIVERED)
				PRINTLN("[EXEC1] [CTRB_SELL], SET_SELL_VEH_HAS_DELIVERED_CONTRABAND - veh ", iVeh, " has been delivered")
				SET_SERVER_BIT0(eSERVERBITSET0_VEH_0_DELIVERED)
			ENDIF
		BREAK
		
		CASE 1
			IF NOT IS_SERVER_BIT0_SET(eSERVERBITSET0_VEH_1_DELIVERED)
				PRINTLN("[EXEC1] [CTRB_SELL], SET_SELL_VEH_HAS_DELIVERED_CONTRABAND - veh ", iVeh, " has been delivered")
				SET_SERVER_BIT0(eSERVERBITSET0_VEH_1_DELIVERED)
			ENDIF
		BREAK
		
		CASE 2
			IF NOT IS_SERVER_BIT0_SET(eSERVERBITSET0_VEH_2_DELIVERED)
				PRINTLN("[EXEC1] [CTRB_SELL], SET_SELL_VEH_HAS_DELIVERED_CONTRABAND - veh ", iVeh, " has been delivered")
				SET_SERVER_BIT0(eSERVERBITSET0_VEH_2_DELIVERED)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL IS_THIS_A_HIDDEN_DROP_OFF(INT i, INT iVehicle = -1)
	IF iVehicle = -1
		INT iLoop
		REPEAT ciMAX_SELL_VEHS iLoop
			IF i = serverBD.iLastDropOff[iLoop]
				RETURN TRUE
			ENDIF
		ENDREPEAT
	ELSE
		IF i = serverBD.iLastDropOff[iVehicle]
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE	
ENDFUNC

PROC SERVER_MAINTAIN_DROP_OFFS( VEHICLE_INDEX dropOffVeh, INT iDropOffVeh, INT& iClosest)

	INT i
	INT iVehDropOffCount
	VECTOR vDrop
	
	FLOAT fCurrentDist
	FLOAT fShortDist = 9999999.99

	ENTITY_INDEX eiEntity 
	
	#IF IS_DEBUG_BUILD
	BOOL bCrateExistsForDebug
	#ENDIF
	
	IF NOT HAS_ALL_CONTRABAND_BEEN_DROPPED_OFF()	
	
		// Checking for crates in drop-offs
		IF bOK_TO_DO_CRATES()
			iVehDropOffCount = GET_SELL_VEH_DROP_OFF_COUNT(iDropOffVeh)
			IF iVehDropOffCount < ciMAX_CRATES
				IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.sContraband[iVehDropOffCount].netId[iDropOffVeh]) 
					eiEntity = NET_TO_ENT(serverBD.sContraband[iVehDropOffCount].netId[iDropOffVeh])
					PRINTLN("[EXEC1] [CTRB_SELL] SERVER_MAINTAIN_DROP_OFFS - eiEntity = NET_TO_ENT(serverBD.sContraband[", iVehDropOffCount, "].netId[", iDropOffVeh, "])")
					
					#IF IS_DEBUG_BUILD
					bCrateExistsForDebug = TRUE
					#ENDIF
				ENDIF
			ENDIF
		ELSE
			// Checking for vehicles in drop-offs
			eiEntity = dropOffVeh
		ENDIF
		
		IF bAIR_RESTRICTED()
			IF serverBD.iLastDropOff[iDropOffVeh] != -1
				PRINTLN("[EXEC1] [CTRB_SELL] SERVER_MAINTAIN_DROPS - iDropOffVeh = ", iDropOffVeh, " - iVehDropOffCount = ", iVehDropOffCount)
				IF iVehDropOffCount >= ciDROP_OFF_UNRESTRICTED
					serverBD.iLastDropOff[iDropOffVeh] = -1
					PRINTLN("[EXEC1] [CTRB_SELL] SERVER_MAINTAIN_DROPS bAIR_RESTRICTED iDROPPED_OFF_COUNT() = (iDROP_TARGET() - 1)")
					PRINTLN("[EXEC1] [CTRB_SELL] SERVER_MAINTAIN_DROPS bAIR_RESTRICTED serverBD.iLastDropOff[", iDropOffVeh, "] = ", serverBD.iLastDropOff[iDropOffVeh])
				ENDIF
			ENDIF
		ENDIF
		
		// Work out closest drop-off
		REPEAT ciMAX_SELL_DROP_OFFS i
			IF IS_BIT_SET(serverBD.iActiveDropOffBitset[iDropOffVeh], i)
			AND NOT IS_THIS_A_HIDDEN_DROP_OFF(i, iDropOffVeh)
				vDrop = GET_SELL_DROP_COORDS(GET_SELL_VAR(), i, serverBD.iRoute, iDropOffVeh)
				PRINTLN("[1546486] vDrop = ", vDrop)
				fCurrentDist = GET_DISTANCE_BETWEEN_COORDS(vDrop, GET_ENTITY_COORDS(dropOffVeh, FALSE))
				IF fCurrentDist < fShortDist
					fShortDist = fCurrentDist
					iClosest = i
					PRINTLN("[1546486] closest = ", i, " fCurrentDist = ", fCurrentDist, " fShortDist = ", fShortDist)
				ENDIF
			ENDIF
		ENDREPEAT
		
		// And count drop offs
		IF DOES_ENTITY_EXIST(eiEntity)
		AND NOT IS_ENTITY_DEAD(eiEntity)
			#IF IS_DEBUG_BUILD
				IF bCrateExistsForDebug 
					PRINTLN("[bCrateExistsForDebug] Crate exists checking coords....")
					VECTOR vCrateCoords = GET_ENTITY_COORDS(eiEntity, FALSE)
					VECTOR vDropOFfCoords = GET_SELL_DROP_COORDS(GET_SELL_VAR(), iClosest, serverBD.iRoute, iDropOffVeh)
					FLOAT fDist = GET_DISTANCE_BETWEEN_COORDS(vCrateCoords, vDropOffCoords)
					BOOL bAtCoords = ENTITY_AT_DROP_OFF_COORDS(GET_SELL_DROP_COORDS(GET_SELL_VAR(), iClosest, serverBD.iRoute, iDropOffVeh), eiEntity, TRUE)
					PRINTLN("[bCrateExistsForDebug] bAtCoords = ", bAtCoords)
					PRINTLN("[bCrateExistsForDebug] vCrateCoords = ", vCrateCoords)
					PRINTLN("[bCrateExistsForDebug] vDropOFfCoords = ", vDropOFfCoords)
					PRINTLN("[bCrateExistsForDebug] Distance between = ", fDist)
					PRINTLN("[bCrateExistsForDebug] iClosest = ", iClosest)
					PRINTLN("[bCrateExistsForDebug] ")
				ENDIF
			#ENDIF
			IF bAIR_CLEAR_AREA()
				IF HAS_AIR_CLEAR_AREA_PLANE_LANDED(iDropOffVeh)
					SET_SELL_VEH_HAS_DELIVERED_CONTRABAND(iDropOffVeh)
				ENDIF
			ELIF ENTITY_AT_DROP_OFF_COORDS(GET_SELL_DROP_COORDS(GET_SELL_VAR(), iClosest, serverBD.iRoute, iDropOffVeh), eiEntity, TRUE)
				SERVER_INCREMENT_DROP_COUNT(iClosest, iDropOffVeh)
				IF bOK_TO_DO_CRATES()
				AND NOT bAIR_VARIATION()
					PRINTLN("[EXEC1]  [SELL AUDIO] SERVER_MAINTAIN_DROP_OFFS, DLC_Exec_Land_Multiple_Sounds ")
					PLAY_SOUND_FROM_COORD(-1, "Drop_Package", VEH_POS(iDropOffVeh), "DLC_Exec_Land_Multiple_Sounds", TRUE, 0)
				ENDIF
				// Check for all deliveries made for vehicle
				IF DOES_SHIPMENT_TYPE_HAVE_MULTIPLE_VEHICLES()
					IF GET_SELL_VEH_DROP_OFF_COUNT(iDropOffVeh) = GET_SELL_VEH_TARGET_DROP_COUNT()
						SET_SELL_VEH_HAS_DELIVERED_CONTRABAND(iDropOffVeh)
					ENDIF
				ELSE
					IF HAS_ALL_CONTRABAND_BEEN_DROPPED_OFF()
						SET_SELL_VEH_HAS_DELIVERED_CONTRABAND(iDropOffVeh)
					ENDIF
				ENDIF
			ENDIF
		#IF IS_DEBUG_BUILD
		ELSE
			IF bCrateExistsForDebug 
				PRINTLN("[bCrateExistsForDebug] Crate exists but dead!")
			ENDIF
		#ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CLIENT_MANAGE_SMOKE_FROM_DROP_OFFS()
	
	INT iDropOffVeh, i 
	
	IF IS_PLAYER_IN_ANY_SELL_VEH(FALSE)
		iDropOffVeh = GET_SELL_VEH_I_AM_IN_INT()
	ELSE
		iDropOffVeh = GET_CLOSEST_SELL_VEH_TO_LOCAL_PLAYER(TRUE)
	ENDIF
		
	IF iDropOffVeh <> -1
	
		INT iClosest
		iClosest = serverBD.iClosestDropOff[iDropOffVeh]

		IF iClosest <> -1
			IF bAIR_VARIATION()
	//		AND IS_ENTITY_ALIVE(dropOffVeh)
			AND IS_BIT_SET(serverBD.iActiveDropOffBitset[iDropOffVeh], iClosest)
				ADD_PARTICLE_FX(iClosest, GET_SELL_DROP_COORDS(GET_SELL_VAR(), iClosest, serverBD.iRoute, iDropOffVeh))
			ELSE
				BOOL bShouldCleanupPtfx = TRUE
				i = 0
				
				//-- Don't clean up the flare if there's multiple sell vehicles, and another vehicle still has to complete this drop off
				IF NOT IS_BIT_SET(serverBD.iActiveDropOffBitset[iDropOffVeh], iClosest)
					REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
						IF IS_BIT_SET(serverBD.iActiveDropOffBitset[i], iClosest)
							bShouldCleanupPtfx = FALSE
						ENDIF
					ENDREPEAT
				ELSE
					bShouldCleanupPtfx = FALSE
				ENDIF
				
				IF bShouldCleanupPtfx
					CLEANUP_CRATE_FLARE(iClosest)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SERVER_SET_START_WAREHOUSE()
	IF serverBD.iStartingWarehouse = -1
		IF GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.contrabandMissionData.iMissionWarehouse = -1
			serverBD.iStartingWarehouse = sLocaldata.contraData.iMissionWarehouse
			PRINTLN("[EXEC1] [CTRB_SELL] SERVER_SET_START_WAREHOUSE, DEBUG  ")
		ELSE
			PRINTLN("[EXEC1] [CTRB_SELL] SERVER_SET_START_WAREHOUSE, LAPTOP  ")
			serverBD.iStartingWarehouse = GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.contrabandMissionData.iMissionWarehouse
		ENDIF
		
		// [NOTE FOR BOBBY] - Set starting contraband to amount passed from Mark / Martin, and now the total amount it was previously
		
		serverBD.iStartingContraband = GET_CONTRABAND_UNITS_TO_SELL_FROM_BUYER()
		serverBD.iWarehouseTotalContraband = GET_CONTRABAND_UNITS_TOTAL_FOR_WAREHOUSE(serverBD.iStartingWarehouse)
		
		IF serverBD.iStartingContraband <= 0
			PRINTLN("[EXEC1] [CTRB_SELL] SERVER_SET_START_WAREHOUSE, iStartingContraband = 0, adding 1 contraband to warehouse so there's something to sell")
			serverBD.iStartingContraband = 1
			SET_SERVER_BIT1(eSERVERBITSET1_LAUNCHED_WITH_NO_CONTRABAND)
		ENDIF
		PRINTLN("[EXEC1] [CTRB_SELL] SERVER_SET_START_WAREHOUSE, iStartingContraband =  ", serverBD.iStartingContraband)
		PRINTLN("[EXEC1] [CTRB_SELL] SERVER_SET_START_WAREHOUSE, iStartingWarehouse =  ", serverBD.iStartingWarehouse)
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SERVER_MAINTAIN_DROPS()
	
	IF DOES_SHIPMENT_TYPE_HAVE_MULTIPLE_VEHICLES()
		INT iClosest
		INT i
		REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
			IF DOES_ENTITY_EXIST(VEH_ID(i))
			AND NOT HAS_THIS_SELL_VEH_DELIVERED_CONTRABAND(i)
				SERVER_MAINTAIN_DROP_OFFS( VEH_ID(i), i, iClosest)
				IF serverBD.iClosestDropOff[i] <> iClosest
					serverBD.iClosestDropOff[i] = iClosest
					PRINTLN("SERVER_MAINTAIN_DROPS, serverBD.iClosestDropOff [",i,"] = ", serverBD.iClosestDropOff[i])
				ENDIF
				iClosest = 0
			ENDIF
		ENDREPEAT
	ELSE
		INT iClosest
		IF DOES_ENTITY_EXIST(VEH_ID(0))
			SERVER_MAINTAIN_DROP_OFFS( VEH_ID(0), 0, iClosest)
			
			PRINTLN("SERVER_MAINTAIN_DROPS, iClosest = ", iClosest)
			IF serverBD.iClosestDropOff[0] <> iClosest
				serverBD.iClosestDropOff[0] = iClosest
				PRINTLN("SERVER_MAINTAIN_DROPS, serverBD.iClosestDropOff[0] = ", serverBD.iClosestDropOff[0])
			ENDIF
		ENDIF
	ENDIF
	
		
ENDPROC

// Radar Jam HUD

PROC CLEANUP_RADAR_JAM_SOUNDS()
	IF IS_BIT_SET(sLocaldata.iRadarJamSoundBitSet, ciRADAR_JAM_SOUND_TRIGGER_ACTIVE)
		IF iRadarJamSoundID_Active != -1
			IF NOT HAS_SOUND_FINISHED(iRadarJamSoundID_Active)
				STOP_SOUND(iRadarJamSoundID_Active)
				RELEASE_SOUND_ID(iRadarJamSoundID_Active)
			ENDIF
		ENDIF
		CLEAR_BIT(sLocaldata.iRadarJamSoundBitSet, ciRADAR_JAM_SOUND_TRIGGER_ACTIVE)
	ENDIF
	
	IF IS_BIT_SET(sLocaldata.iRadarJamSoundBitSet, ciRADAR_JAM_SOUND_TRIGGER_ACTIVE_EMPTY)
		IF iRadarJamSoundID_ActiveEmpty != -1
			RELEASE_SOUND_ID(iRadarJamSoundID_ActiveEmpty)
		ENDIF
		CLEAR_BIT(sLocaldata.iRadarJamSoundBitSet, ciRADAR_JAM_SOUND_TRIGGER_ACTIVE_EMPTY)
	ENDIF
	
	IF IS_BIT_SET(sLocaldata.iRadarJamSoundBitSet, ciRADAR_JAM_SOUND_TRIGGER_CHARGING)
		IF iRadarJamSoundID_Charging != -1
			IF NOT HAS_SOUND_FINISHED(iRadarJamSoundID_Charging)
				STOP_SOUND(iRadarJamSoundID_Charging)
				RELEASE_SOUND_ID(iRadarJamSoundID_Charging)
			ENDIF
		ENDIF
		CLEAR_BIT(sLocaldata.iRadarJamSoundBitSet, ciRADAR_JAM_SOUND_TRIGGER_CHARGING)
	ENDIF
	
	IF IS_BIT_SET(sLocaldata.iRadarJamSoundBitSet, ciRADAR_JAM_ACTIVATED)
		CLEAR_BIT(sLocaldata.iRadarJamSoundBitSet, ciRADAR_JAM_ACTIVATED)
	ENDIF
ENDPROC

PROC PLAY_RADAR_JAM_ACTIVE_SOUND()
	IF NOT IS_BIT_SET(sLocaldata.iRadarJamSoundBitSet, ciRADAR_JAM_ACTIVATED)
		IF iRadarJamSoundID_Charge_Full != -1
			RELEASE_SOUND_ID(iRadarJamSoundID_Charge_Full)
		ENDIF
		SET_BIT(sLocaldata.iRadarJamSoundBitSet, ciRADAR_JAM_ACTIVATED)
	ENDIF
	
	// Trigger looping sound
	IF NOT IS_BIT_SET(sLocaldata.iRadarJamSoundBitSet, ciRADAR_JAM_SOUND_TRIGGER_ACTIVE)
		iRadarJamSoundID_Active = GET_SOUND_ID()
		PLAY_SOUND_FRONTEND(iRadarJamSoundID_Active, "Active", "DLC_Exec_Jammer_Sounds", FALSE)
		SET_BIT(sLocaldata.iRadarJamSoundBitSet, ciRADAR_JAM_SOUND_TRIGGER_ACTIVE)
		PRINTLN("[EXEC1] [CTRB_SELL] [RADAR JAM], MAINTAIN_RADAR_JAM_HUD - Triggering active sound")
	ENDIF
ENDPROC

PROC PLAY_RADAR_JAM_ACTIVE_EMPTY_SOUND()
	// Kill looping sound
	IF IS_BIT_SET(sLocaldata.iRadarJamSoundBitSet, ciRADAR_JAM_SOUND_TRIGGER_ACTIVE)
		STOP_SOUND(iRadarJamSoundID_Active)
		RELEASE_SOUND_ID(iRadarJamSoundID_Active)
		CLEAR_BIT(sLocaldata.iRadarJamSoundBitSet, ciRADAR_JAM_SOUND_TRIGGER_ACTIVE)
		PRINTLN("[EXEC1] [CTRB_SELL] [RADAR JAM], MAINTAIN_RADAR_JAM_HUD - Killing active sound")
	ENDIF
	
	// Trigger empty sound
	IF NOT IS_BIT_SET(sLocaldata.iRadarJamSoundBitSet, ciRADAR_JAM_SOUND_TRIGGER_ACTIVE_EMPTY)
		iRadarJamSoundID_ActiveEmpty = GET_SOUND_ID()
		PLAY_SOUND_FRONTEND(iRadarJamSoundID_ActiveEmpty, "Active_Empty", "DLC_Exec_Jammer_Sounds", FALSE)
		SET_BIT(sLocaldata.iRadarJamSoundBitSet, ciRADAR_JAM_SOUND_TRIGGER_ACTIVE_EMPTY)
		PRINTLN("[EXEC1] [CTRB_SELL] [RADAR JAM], MAINTAIN_RADAR_JAM_HUD - Triggering active empty sound")
	ENDIF
ENDPROC

PROC PLAY_RADAR_JAM_CHARGING_SOUND()
	// Trigger charging sound
	IF NOT IS_BIT_SET(sLocaldata.iRadarJamSoundBitSet, ciRADAR_JAM_SOUND_TRIGGER_CHARGING)
		iRadarJamSoundID_Charging = GET_SOUND_ID()
		PLAY_SOUND_FRONTEND(iRadarJamSoundID_Charging, "Charging", "DLC_Exec_Jammer_Sounds", FALSE)
		SET_BIT(sLocaldata.iRadarJamSoundBitSet, ciRADAR_JAM_SOUND_TRIGGER_CHARGING)
		PRINTLN("[EXEC1] [CTRB_SELL] [RADAR JAM], MAINTAIN_RADAR_JAM_HUD - Triggering charging sound")
	ENDIF
ENDPROC

PROC PLAY_RADAR_JAM_CHARGE_FULL_SOUND()
	// Check if its been used, and clear up all sound IDs and bits for next use
	IF IS_BIT_SET(sLocaldata.iRadarJamSoundBitSet, ciRADAR_JAM_ACTIVATED)
		
		CLEANUP_RADAR_JAM_SOUNDS()
		
		// trigger fully charged sound
		iRadarJamSoundID_Charge_Full = GET_SOUND_ID()
		PLAY_SOUND_FRONTEND(iRadarJamSoundID_Charge_Full, "Charge_Full", "DLC_Exec_Jammer_Sounds", FALSE)
		
		PRINTLN("[EXEC1] [CTRB_SELL] [RADAR JAM], MAINTAIN_RADAR_JAM_HUD - Triggering fully charged sound, resetting sound data")
	ENDIF
ENDPROC

PROC MAINTAIN_RADAR_JAM_HUD()
	INT iBarVal
	HUD_COLOURS colour = HUD_COLOUR_WHITE
	IF HAS_NET_TIMER_STARTED(sLocaldata.stRadarJamTimer)
		
		IF HAS_NET_TIMER_EXPIRED(sLocaldata.stRadarJamTimer, serverBD.iRadarJamDuration)
		
			IF HAS_NET_TIMER_EXPIRED(sLocaldata.stRadarJamTimer, (serverBD.iRadarJamDuration + serverBD.iRadarJamRechargeDelay))
			
				IF NOT HAS_NET_TIMER_EXPIRED(sLocaldata.stRadarJamTimer, (serverBD.iRadarJamDuration + serverBD.iRadarJamRechargeDelay + serverBD.iRadarJamRechargeTime))
				
					// Play the sound when starting to charge
					PLAY_RADAR_JAM_CHARGING_SOUND()
				
					iBarVal = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sLocaldata.stRadarJamTimer) - (serverBD.iRadarJamDuration + serverBD.iRadarJamRechargeDelay)
					
					IF iBarVal < (serverBD.iRadarJamRechargeTime * 0.90)
						colour = HUD_COLOUR_RED
					ENDIF
					
					DRAW_GENERIC_METER(iBarVal, serverBD.iRadarJamRechargeTime, "SCONTRA_RJAM", colour, DEFAULT, HUDORDER_TOP)
				ENDIF
			ELSE
			
				// Play the sound when empty
				PLAY_RADAR_JAM_ACTIVE_EMPTY_SOUND()
			
				colour = HUD_COLOUR_RED
				DRAW_GENERIC_METER(0, serverBD.iRadarJamDuration, "SCONTRA_RJAM", colour, DEFAULT, HUDORDER_TOP)
			ENDIF
		ELSE
			// Play the sound when activated
			PLAY_RADAR_JAM_ACTIVE_SOUND()
		
			iBarVal = (serverBD.iRadarJamDuration - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sLocaldata.stRadarJamTimer))
			
			IF iBarVal < (serverBD.iRadarJamDuration / 4)
				colour = HUD_COLOUR_RED
			ENDIF
			
			DRAW_GENERIC_METER(iBarVal, serverBD.iRadarJamDuration, "SCONTRA_RJAM", colour, DEFAULT, HUDORDER_TOP)
		ENDIF
	ELSE
		
		// Play the sound when fully charged (only done if activated)
		PLAY_RADAR_JAM_CHARGE_FULL_SOUND()
	
		DRAW_GENERIC_METER(serverBD.iRadarJamDuration, serverBD.iRadarJamDuration, "SCONTRA_RJAM", colour, DEFAULT, HUDORDER_TOP)
	ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_IN_AIRPORT()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	
		VECTOR vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
		
		IF IS_COORD_IN_SPECIFIED_AREA(vPlayerCoords, AC_AIRPORT_AIRSIDE, 1000)
		OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-1256.4573,-2150.7021,12.9248>>, <<100.00, 100.00, 100.00>>)
		
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//╔═════════════════════════════════════════════════════════════════════════════╗
//║				BLIPS															║

FUNC INT GET_CONTRABAND_BLIP_COLOUR(INT i)

	UNUSED_PARAMETER(i)

//	IF IS_SELL_VEH_OCCUPIED(i)
//		RETURN GET_BLIP_COLOUR_FROM_HUD_COLOUR(GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID()))
//	ENDIF

	RETURN BLIP_COLOUR_BLUE
ENDFUNC

PROC COLOUR_CONTRA_BLIP()
	IF DOES_BLIP_EXIST(sLocaldata.blipVeh)
		SET_BLIP_COLOUR(sLocaldata.blipVeh, GET_CONTRABAND_BLIP_COLOUR())
	ENDIF
ENDPROC

PROC REMOVE_CONTRABAND_BLIP(INT i)
	IF DOES_BLIP_EXIST(sLocaldata.blipVeh[i])
		REMOVE_BLIP(sLocaldata.blipVeh[i])
		PRINTLN("2738261, REMOVE_CONTRABAND_BLIP, blip ", i)
	ENDIF
ENDPROC

PROC REMOVE_CONTRABAND_BLIPS()
	INT i
	REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
		REMOVE_CONTRABAND_BLIP(i)
	ENDREPEAT
ENDPROC

FUNC BOOL PLAYER_WANTED()
	RETURN IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), eCLIENTBITSET0_WANTED)
ENDFUNC

FUNC BOOL SHOULD_HIDE_DROP_OFF_BLIPS()
	IF NOT HAS_ALL_CONTRABAND_BEEN_DROPPED_OFF()
		IF Is_This_The_Current_Objective_Text("BYCB_CLY_LSC0")
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL OK_TO_DISPLAY_CONTRABAND_VEH_BLIP(INT iVeh)
	IF AM_I_IN_THIS_SELL_VEH(iVeh)
		RETURN FALSE
	ENDIF
	
	IF HAS_ALL_CONTRABAND_BEEN_DROPPED_OFF()
		RETURN FALSE
	ENDIF
	
	IF HAS_THIS_SELL_VEH_DELIVERED_CONTRABAND(iVeh)
		RETURN FALSE
	ENDIF
	
	IF SHOULD_HIDE_DROP_OFF_BLIPS()
		RETURN FALSE
	ENDIF
	
	IF IS_SELL_VEH_OCCUPIED(iVeh)
		IF NOT IS_SHIPMENT_BLIP_FLASH_ACTIVE()
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC SET_PLAYER_BLIP_ALPHA_FOR_CONTRABAND_DRIVER(INT iVeh, INT iAlpha)
	PLAYER_INDEX playerDriver
	INT iPlayer
	
	playerDriver = GET_DRIVER_OF_SELL_VEH(iVeh)
	iPlayer = NATIVE_TO_INT(playerDriver)
	
	IF playerDriver != INVALID_PLAYER_INDEX()
	AND iPlayer != -1
	AND DOES_BLIP_EXIST(g_PlayerBlipsData.playerBlips[iPlayer])
		PRINTLN("[MAINTAIN_CONTRABAND_BLIPS] Forcing player blip alpha for player ", GET_PLAYER_NAME(playerDriver), " Player int = ", iPlayer) 
		SET_BLIP_ALPHA(g_PlayerBlipsData.playerBlips[iPlayer], iAlpha)
	ENDIF
ENDPROC

PROC MAINTAIN_CONTRABAND_BLIPS()
	INT i
	
	REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
		IF IS_SELL_VEH_OK(i)
			IF OK_TO_DISPLAY_CONTRABAND_VEH_BLIP(i)
				IF NOT DOES_BLIP_EXIST(sLocaldata.blipVeh[i])
					sLocaldata.blipVeh[i] = ADD_BLIP_FOR_ENTITY(VEH_ENT(i))
					SET_BLIP_SPRITE(sLocaldata.blipVeh[i], GET_CONTRABAND_BLIP_SPRITE())
					SET_BLIP_COLOUR(sLocaldata.blipVeh[i], GET_CONTRABAND_BLIP_COLOUR(i))
					SET_BLIP_PRIORITY(sLocaldata.blipVeh[i], BLIP_PRIORITY_HIGHEST_SPECIAL_HIGH)
					SET_BLIP_NAME_FROM_TEXT_FILE(sLocaldata.blipVeh[i], "BYCB_NM")
//					SET_BLIP_ROUTE(sLocaldata.blipVeh[i], TRUE)
					PRINTLN("2738261, DRAW_CONTRABAND_BLIP")

				ENDIF
				
				IF GET_BLIP_COLOUR(sLocaldata.blipVeh[i]) != GET_CONTRABAND_BLIP_COLOUR(i)
					SET_BLIP_COLOUR(sLocaldata.blipVeh[i], GET_CONTRABAND_BLIP_COLOUR(i))
				ENDIF
				
				IF NOT sLocaldata.blipVehOkLastFrame[i]
					SET_PLAYER_BLIP_ALPHA_FOR_CONTRABAND_DRIVER(i, 0)
				ENDIF
				
				sLocaldata.blipVehOkLastFrame[i] = TRUE
			ELSE
				REMOVE_CONTRABAND_BLIP(i)
				
				//-- Now that the contraband flashes on/off, need to force the contraband vehicle driver's blip on (actually set its alpha)
				//-- because otherwise there will be a brief period when there's no player blip or contraband blip due to the fact that
				//-- the remote player blip update is a staggered update (see bug 2827903)
				
				IF sLocaldata.blipVehOkLastFrame[i]
					IF NOT IS_SHIPMENT_BLIP_FLASH_ACTIVE()
						IF NOT AM_I_IN_THIS_SELL_VEH(i)
							IF IS_SELL_VEH_OCCUPIED(i)
								SET_PLAYER_BLIP_ALPHA_FOR_CONTRABAND_DRIVER(i, 255)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				sLocaldata.blipVehOkLastFrame[i] = FALSE
			ENDIF
		ELSE
			REMOVE_CONTRABAND_BLIP(i)
		ENDIF
	ENDREPEAT
	
	
	//-- Only do GPS to closest Contra vehicle (and if I'm not in a contra vehicle).
	IF NOT AM_I_IN_ANY_SELL_VEH()
	AND NOT bAIR_VARIATION()
		INT iClosest = GET_CLOSEST_SELL_VEH_TO_LOCAL_PLAYER(FALSE)
		IF iClosest > -1
			IF NOT ARE_ALL_SELL_VEHS_OCCUPIED()
				IF sLocaldata.iCLosestCOntraVehForGPS != iClosest
					IF sLocaldata.iCLosestCOntraVehForGPS >= 0
						IF DOES_BLIP_EXIST(sLocaldata.blipVeh[sLocaldata.iCLosestCOntraVehForGPS])
							IF DOES_BLIP_HAVE_GPS_ROUTE(sLocaldata.blipVeh[sLocaldata.iCLosestCOntraVehForGPS])
								PRINTLN("[MAINTAIN_CONTRABAND_BLIPS] REMOVED GPS FOR CONTRA VEH ", sLocaldata.iCLosestCOntraVehForGPS, " as no longer closest"  )
								SET_BLIP_ROUTE(sLocaldata.blipVeh[sLocaldata.iCLosestCOntraVehForGPS], FALSE)
							ENDIF
						ENDIF
					ENDIF
					IF DOES_BLIP_EXIST(sLocaldata.blipVeh[iClosest])
						IF NOT DOES_BLIP_HAVE_GPS_ROUTE(sLocaldata.blipVeh[iClosest])
							PRINTLN("[MAINTAIN_CONTRABAND_BLIPS] ADDED GPS FOR CONTRA VEH ", iClosest)
							SET_BLIP_ROUTE(sLocaldata.blipVeh[iClosest], TRUE)
						ENDIF
					ENDIF
					
					sLocaldata.iCLosestCOntraVehForGPS = iClosest
					PRINTLN("[MAINTAIN_CONTRABAND_BLIPS] sLocaldata.iCLosestCOntraVehForGPS = ", sLocaldata.iCLosestCOntraVehForGPS) 
				ENDIF
			ELSE
				//-- Only draw a GPS to unoccupied contraband vehicles
				IF sLocaldata.iCLosestCOntraVehForGPS > -1
					IF DOES_BLIP_EXIST(sLocaldata.blipVeh[sLocaldata.iCLosestCOntraVehForGPS])
						IF DOES_BLIP_HAVE_GPS_ROUTE(sLocaldata.blipVeh[sLocaldata.iCLosestCOntraVehForGPS])
							PRINTLN("[MAINTAIN_CONTRABAND_BLIPS] REMOVED GPS FOR CONTRA VEH ", sLocaldata.iCLosestCOntraVehForGPS, " as I'm not in a contra vehicle, but all are occupied")
							SET_BLIP_ROUTE(sLocaldata.blipVeh[sLocaldata.iCLosestCOntraVehForGPS], FALSE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF sLocaldata.iCLosestCOntraVehForGPS > -1
				IF DOES_BLIP_EXIST(sLocaldata.blipVeh[sLocaldata.iCLosestCOntraVehForGPS])
					IF DOES_BLIP_HAVE_GPS_ROUTE(sLocaldata.blipVeh[sLocaldata.iCLosestCOntraVehForGPS])
						PRINTLN("[MAINTAIN_CONTRABAND_BLIPS] REMOVED GPS FOR CONTRA VEH ", sLocaldata.iCLosestCOntraVehForGPS, " as iClosest <= -1")
						SET_BLIP_ROUTE(sLocaldata.blipVeh[sLocaldata.iCLosestCOntraVehForGPS], FALSE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF sLocaldata.iCLosestCOntraVehForGPS > -1
			IF DOES_BLIP_EXIST(sLocaldata.blipVeh[sLocaldata.iCLosestCOntraVehForGPS])
				IF DOES_BLIP_HAVE_GPS_ROUTE(sLocaldata.blipVeh[sLocaldata.iCLosestCOntraVehForGPS])
					PRINTLN("[MAINTAIN_CONTRABAND_BLIPS] REMOVED GPS FOR CONTRA VEH ", sLocaldata.iCLosestCOntraVehForGPS, " as I'm in a contra vehicle")
					SET_BLIP_ROUTE(sLocaldata.blipVeh[sLocaldata.iCLosestCOntraVehForGPS], FALSE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

FUNC BLIP_SPRITE GET_BLIP_SPRITE_FOR_VEHICLE(VEHICLE_INDEX veh)
	MODEL_NAMES model = GET_ENTITY_MODEL(veh)

	IF IS_THIS_MODEL_A_BIKE(model)
		RETURN RADAR_TRACE_GANG_BIKERS
	ENDIF
	
	IF IS_THIS_MODEL_A_HELI(model)
		IF IS_THIS_VEH_A_SELL_VEH(veh)
			RETURN RADAR_TRACE_HELICOPTER
		ELSE
			RETURN RADAR_TRACE_ENEMY_HELI_SPIN
		ENDIF
	ENDIF
	
	IF IS_THIS_MODEL_A_PLANE(model)
		RETURN RADAR_TRACE_PLAYER_PLANE
	ENDIF
	
//	IF IS_THIS_MODEL_A_BOAT(model)
//		IF model = SEASHARK
//		OR model = SEASHARK2
//		OR model = SEASHARK3
//			RETURN RADAR_TRACE_SEASHARK
//		ELSE
//			RETURN RADAR_TRACE_PLAYER_BOAT
//		ENDIF
//	ENDIF
	
	RETURN RADAR_TRACE_AI
ENDFUNC

FUNC BOOL SHOULD_VEHICLE_BLIP_HAVE_FIXED_ROTATION(VEHICLE_INDEX veh)
	IF IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(veh))
		RETURN TRUE
	ENDIF
	
	IF IS_THIS_MODEL_A_BOAT(GET_ENTITY_MODEL(veh))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_PED_BE_BLIPPED(INT iPed)
	IF IS_PED_INJURED(GET_AI_PED_PED_INDEX(iPed))
		RETURN FALSE
	ENDIF
	
	IF bSEA_DEFEND()
		IF IS_SERVER_BIT0_SET(eSERVERBITSET0_VEHICLE_0_ENTERED_ONCE)
			IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), GET_SEA_SELL_VEHICLE_CREATION_POS(), FALSE) > 100
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC MAINTAIN_PED_BLIPS()
	IF DOES_VARIATION_HAVE_AI_PEDS()
		IF HAVE_PEDS_BEEN_SPOOKED()
		OR NOT DOES_VARIATION_REQUIRE_PED_SPOOKING()
			INT i
			REPEAT GET_NUM_PEDS_FOR_VARIATION() i
				IF SHOULD_PED_BE_BLIPPED_AS_ENEMY( i)
					IF DOES_AI_PED_EXIST(i)
						IF SHOULD_PED_BE_BLIPPED(i)
							IF IS_PED_IN_ANY_VEHICLE(GET_AI_PED_PED_INDEX(i))
								VEHICLE_INDEX veh = GET_VEHICLE_PED_IS_USING(GET_AI_PED_PED_INDEX(i))
								IF NOT IS_VEHICLE_A_NON_CONTRABAND_VEHICLE(veh)
									IF NOT DOES_BLIP_EXIST(sLocaldata.blipPed[i])
									OR (DOES_BLIP_EXIST(sLocaldata.blipPed[i]) AND (GET_BLIP_SPRITE(sLocaldata.blipPed[i]) != GET_BLIP_SPRITE_FOR_VEHICLE(veh)))
										sLocaldata.blipPed[i] = ADD_BLIP_FOR_ENTITY(GET_AI_PED_PED_INDEX(i))
										SET_BLIP_SPRITE(sLocaldata.blipPed[i], GET_BLIP_SPRITE_FOR_VEHICLE(veh))
										SET_BLIP_COLOUR_FROM_HUD_COLOUR(sLocaldata.blipPed[i], HUD_COLOUR_RED)
										PRINTLN("[EXEC1] [CTRB_SELL] [MAINTAIN_PED_BLIPS] - Added Blip for ped ", i)
									ENDIF
								ELSE
									// Remove blips if the ped is in a contraband vehicle
									IF DOES_BLIP_EXIST(sLocaldata.blipPed[i])
										REMOVE_BLIP(sLocaldata.blipPed[i])
										PRINTLN("[EXEC1] [CTRB_SELL] [MAINTAIN_PED_BLIPS] - Removed Blip for ped ", i, " - In a contraband vehicle")
									ENDIF
								ENDIF
							ELSE
								IF NOT DOES_BLIP_EXIST(sLocaldata.blipPed[i])
								OR (DOES_BLIP_EXIST(sLocaldata.blipPed[i]) AND (GET_BLIP_SPRITE(sLocaldata.blipPed[i]) != RADAR_TRACE_AI) AND NOT bLAND_DEFEND())
									IF NOT DOES_BLIP_EXIST(sLocaldata.blipPed[i])
										sLocaldata.blipPed[i] = ADD_BLIP_FOR_ENTITY(GET_AI_PED_PED_INDEX(i))
										PRINTLN("[EXEC1] [CTRB_SELL] [MAINTAIN_PED_BLIPS] - Added Blip for ped in contra vehicle, ped ", i)
									ENDIF
									IF bLAND_DEFEND()
										SET_BLIP_SPRITE(sLocaldata.blipPed[i], RADAR_TRACE_TEMP_4)
										SET_BLIP_NAME_FROM_TEXT_FILE(sLocaldata.blipPed[i], "DCONTRA_BLP")
										PRINTLN("[EXEC1] [CTRB_SELL] [MAINTAIN_PED_BLIPS], DCONTRA_BLP")
									ELSE
										SET_BLIP_SPRITE(sLocaldata.blipPed[i], RADAR_TRACE_AI)
									ENDIF
									SET_BLIP_COLOUR_FROM_HUD_COLOUR(sLocaldata.blipPed[i], HUD_COLOUR_RED)
									SET_BLIP_SCALE(sLocaldata.blipPed[i], BLIP_SIZE_NETWORK_PED)
								ENDIF
							ENDIF
						ELSE
							IF DOES_BLIP_EXIST(sLocaldata.blipPed[i])
								PRINTLN("[EXEC1] [CTRB_SELL] [MAINTAIN_PED_BLIPS] - Removed Blip for ped ", i, " - Injured")
								REMOVE_BLIP(sLocaldata.blipPed[i])
							ENDIF
						ENDIF
					ELSE
						IF DOES_BLIP_EXIST(sLocaldata.blipPed[i])
							PRINTLN("[EXEC1] [CTRB_SELL] [MAINTAIN_PED_BLIPS] - Removed Blip for ped ", i, " - Ped ni doesn't exist")
							REMOVE_BLIP(sLocaldata.blipPed[i])
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF	
ENDPROC

PROC REMOVE_PED_BLIPS()
	INT i
	REPEAT ciMAX_CONTRABAND_PEDS i
		IF DOES_BLIP_EXIST(sLocaldata.blipPed[i])
			REMOVE_BLIP(sLocaldata.blipPed[i])
		ENDIF
	ENDREPEAT
ENDPROC

FUNC BOOL OK_TO_BLIP_NON_CONTRABAND_VEHICLE(INT iVeh)
	IF bAIR_ATTACK()
		RETURN IS_BIT_SET(serverBD.iAirAttackHeliSpooked, iVeh)
	ENDIF
	
	
	RETURN TRUE
ENDFUNC

PROC MAINTAIN_NON_CONTRABAND_VEHICLE_BLIPS()
	IF DOES_VARIATION_HAVE_AI_VEHICLES()
		INT i
		REPEAT ciMAX_CONTRABAND_PED_VEHICLES i
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPedVeh[i].netIndex)
				VEHICLE_INDEX veh = NET_TO_VEH(serverBD.sPedVeh[i].netIndex)
				IF NOT IS_ENTITY_DEAD(veh)
					IF IS_NON_CONTRABAND_VEHICLE_OCCUPIED(i)
						IF NOT DOES_BLIP_EXIST(sLocaldata.blipPedVeh[i])
							IF OK_TO_BLIP_NON_CONTRABAND_VEHICLE(i)
								sLocaldata.blipPedVeh[i] = ADD_BLIP_FOR_ENTITY(veh)
								IF bLAND_DEFEND()
									SET_BLIP_SPRITE(sLocaldata.blipPedVeh[i], RADAR_TRACE_TEMP_4)
									SET_BLIP_NAME_FROM_TEXT_FILE(sLocaldata.blipPedVeh[i], "DCONTRA_BLP")
									PRINTLN("[EXEC1] [CTRB_SELL] [MAINTAIN_NON_CONTRABAND_VEHICLE_BLIPS], DCONTRA_BLP")
								ELSE
									SET_BLIP_SPRITE(sLocaldata.blipPedVeh[i], GET_BLIP_SPRITE_FOR_VEHICLE(veh))
								ENDIF
								SET_BLIP_COLOUR_FROM_HUD_COLOUR(sLocaldata.blipPedVeh[i], HUD_COLOUR_RED)
								PRINTLN("[EXEC1] [CTRB_SELL] [MAINTAIN_NON_CONTRABAND_VEHICLE_BLIPS] - Add blip for veh ", i)
		//						IF SHOULD_VEHICLE_BLIP_HAVE_FIXED_ROTATION(veh)
		//							SET_BLIP_ROTATION(sLocaldata.blipPedVeh[i], ROUND(GET_ENTITY_HEADING(veh)))
		//						ENDIF
		

		
							ENDIF
						ENDIF
					ELSE
						IF DOES_BLIP_EXIST(sLocaldata.blipPedVeh[i])
							REMOVE_BLIP(sLocaldata.blipPedVeh[i])
							PRINTLN("[EXEC1] [CTRB_SELL] [MAINTAIN_NON_CONTRABAND_VEHICLE_BLIPS] - Removed Blip for veh ", i, " - nobody in veh")
						ENDIF
					ENDIF
				ELSE
					IF DOES_BLIP_EXIST(sLocaldata.blipPedVeh[i])
						REMOVE_BLIP(sLocaldata.blipPedVeh[i])
						PRINTLN("[EXEC1] [CTRB_SELL] [MAINTAIN_NON_CONTRABAND_VEHICLE_BLIPS] - Removed Blip for veh ", i, " - veh is dead")
					ENDIF
				ENDIF
			ELSE
				IF DOES_BLIP_EXIST(sLocaldata.blipPedVeh[i])
					REMOVE_BLIP(sLocaldata.blipPedVeh[i])
					PRINTLN("[EXEC1] [CTRB_SELL] [MAINTAIN_NON_CONTRABAND_VEHICLE_BLIPS] - Removed Blip for veh ", i, " - veh ni doesn't exist")
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

PROC REMOVE_VEHICLE_BLIPS()
	INT i
	REPEAT ciMAX_CONTRABAND_PED_VEHICLES i
		IF DOES_BLIP_EXIST(sLocaldata.blipPedVeh[i])
			REMOVE_BLIP(sLocaldata.blipPedVeh[i])
		ENDIF
	ENDREPEAT
ENDPROC

FUNC INT GET_SELL_VEHICLE_THAT_HAS_NOT_COMPLETED_DROP(INT iDrop)
	INT i
	REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
		IF IS_BIT_SET(serverBD.iActiveDropOffBitset[i], iDrop)
			RETURN i
		ENDIF
	ENDREPEAT
	RETURN -1
ENDFUNC

FUNC BOOL SHOULD_DROP_OFF_BE_BLIPPED_FOR_LOCAL_PLAYER(INT iDropOff, INT iVeh)

	INT i
	
	IF iVeh >= 0 
		RETURN IS_BIT_SET(serverBD.iActiveDropOffBitset[iVeh], iDropOff)
	ELSE
		REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
			IF IS_BIT_SET(serverBD.iActiveDropOffBitset[i], iDropOff)
				RETURN TRUE
			ENDIF
		ENDREPEAT
	ENDIF
	
	RETURN FALSE
	
ENDFUNC


FUNC FLOAT GET_AREA_BLIP_RADIUS()
	IF bAIR_VARIATION()
		RETURN 75.0
	ENDIF

	RETURN 25.0
ENDFUNC

FUNC BOOL SHOULD_DO_KILL_ENEMIES_HELP()
	IF bAIR_CLEAR_AREA()
	AND NOT ARE_ALL_ATTACKING_PEDS_DEAD()
	AND (HAVE_ALL_AIR_CLEAR_AREA_PLANES_LANDED()
	OR (NOT IS_DRIVER_OF_ANY_SELL_VEH() 
	AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_SELL_DROP_COORDS(GET_SELL_VAR(), serverBD.iClosestDropOff[0], serverBD.iRoute, 0)) < cfSELL_CRATE_LOC_SIZE_Z))
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC REMOVE_AIRPORT_BLIP()
	IF DOES_BLIP_EXIST(sLocaldata.blipLSIA)
		REMOVE_BLIP(sLocaldata.blipLSIA)
	ENDIF
ENDPROC

FUNC VECTOR vAIRPORT_COORDS()
	RETURN <<-1156.0415, -2740.3728, 12.9525>>
ENDFUNC

PROC TOGGLE_LSIA_AIRPORT_BLIP(BOOL bOn)
	IF bOn
	
//		INT iDropOffBlipR, iDropOffBlipG, iDropOffBlipB, iDropOffBlipA
//		GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iDropOffBlipR, iDropOffBlipG, iDropOffBlipB, iDropOffBlipA)
	
		IF DOES_BLIP_EXIST(sLocaldata.blipLSIA)
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF NOT DOES_BLIP_HAVE_GPS_ROUTE(sLocaldata.blipLSIA)
					SET_BLIP_ROUTE(sLocaldata.blipLSIA, TRUE)
				ENDIF
			ELSE
				IF DOES_BLIP_HAVE_GPS_ROUTE(sLocaldata.blipLSIA)
					SET_BLIP_ROUTE(sLocaldata.blipLSIA, FALSE)
				ENDIF
			ENDIF
//			DRAW_MARKER(MARKER_CYLINDER, vAIRPORT_COORDS(), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, <<ciDELIVERY_DISTANCE, ciDELIVERY_DISTANCE, 0.5>>, 
//						iDropOffBlipR, iDropOffBlipG, iDropOffBlipB, iDropOffBlipA)
		ELSE
			sLocaldata.blipLSIA = ADD_BLIP_FOR_COORD(vAIRPORT_COORDS())
		ENDIF	
	ELSE
		REMOVE_AIRPORT_BLIP()
	ENDIF
ENDPROC

PROC MAINTAIN_AIRPORT_BLIP()
	IF bAIR_VARIATION()
		IF IS_PLAYER_IN_AIRPORT()
		OR IS_PLAYER_IN_ANY_SELL_VEH()
		OR ARE_ALL_SELL_VEHS_OCCUPIED()
			TOGGLE_LSIA_AIRPORT_BLIP(FALSE)
		ELSE
			TOGGLE_LSIA_AIRPORT_BLIP(TRUE)
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_DROP_BLIPS(INT iVeh = -1)

	IF bLAND_TRACKIFY()
	AND NOT IS_SERVER_BIT1_SET(eSERVERBITSET1_LAND_TRACKIFY_SHOULD_BLIP_DROP_OFFS)
		// Dont do drop off blips for trackify variation
		EXIT
	ENDIF
	
	INT i
	VECTOR vCoords
	
	BOOL bKillEnemies = SHOULD_DO_KILL_ENEMIES_HELP()
	
	REPEAT ciMAX_SELL_DROP_OFFS i
	//	IF NOT IS_BIT_SET(serverBD.iActiveDropOffBitset, i)
		IF NOT SHOULD_DROP_OFF_BE_BLIPPED_FOR_LOCAL_PLAYER(i, iVeh)
		OR SHOULD_HIDE_DROP_OFF_BLIPS()
		OR IS_THIS_A_HIDDEN_DROP_OFF(i, iVeh)
		OR bKillEnemies
			IF DOES_BLIP_EXIST(sLocaldata.blipDrop[i])
				PRINTLN("[EXEC1] [CTRB_SELL] [BLIP] MAINTAIN_DROP_BLIPS, REMOVE_DROP_BLIP ", i)
				REMOVE_BLIP(sLocaldata.blipDrop[i])
				
//				STOP_LAND_VEHICLES_DEAD()
			ENDIF
			IF DOES_BLIP_EXIST(sLocaldata.blipDropArea[i])
				PRINTLN("[EXEC1] [CTRB_SELL] [BLIP] MAINTAIN_DROP_BLIPS, REMOVE_DROP_BLIP AREA", i)
				REMOVE_BLIP(sLocaldata.blipDropArea[i])
			ENDIF
		ELSE
			
			INT iVehicleIn = -1
		//	IF DOES_SHIPMENT_TYPE_HAVE_MULTIPLE_VEHICLES()
				IF IS_PLAYER_IN_ANY_SELL_VEH()
					iVehicleIn = GET_SELL_VEH_I_AM_IN_INT()
				ELSE
					//-- If I'm not in a contra veh, but they're all occupied, draw a gps route to the closest drop-off to the closest contra veh (2814859)
					IF ARE_ALL_SELL_VEHS_OCCUPIED()
						iVehicleIn = GET_CLOSEST_SELL_VEH_TO_LOCAL_PLAYER()
					ENDIF
				ENDIF
		//	ENDIF
			IF IS_PLAYER_IN_ANY_SELL_VEH()
				INT iClosest
				IF iVehicleIn <> -1
					iClosest = serverBD.iClosestDropOff[iVehicleIn]
				ENDIF
				IF iClosest <> -1
					IF DOES_BLIP_EXIST(sLocaldata.blipDrop[iClosest])
							
						// Ugly fix for url:bugstar:2814136
						vCoords = GET_SELL_DROP_COORDS(GET_SELL_VAR(), iClosest, serverBD.iRoute, iVehicleIn)
						IF NOT ARE_VECTORS_EQUAL(vCoords, <<-1143.7770, -2223.1814, 12.1958>>)
							IF NOT DOES_BLIP_HAVE_GPS_ROUTE(sLocaldata.blipDrop[iClosest])
								SET_BLIP_ROUTE(sLocaldata.blipDrop[iClosest], TRUE)
								PRINTLN("[EXEC1] [CTRB_SELL] [BLIP] MAINTAIN_DROP_BLIPS, SET_BLIP_ROUTE ", iClosest)
							ENDIF	
						ELSE
							IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vCoords, FALSE) >= 100
								IF NOT DOES_BLIP_HAVE_GPS_ROUTE(sLocaldata.blipDrop[iClosest])
									SET_BLIP_ROUTE(sLocaldata.blipDrop[iClosest], TRUE)
									PRINTLN("[EXEC1] [CTRB_SELL] [BLIP] MAINTAIN_DROP_BLIPS, SET_BLIP_ROUTE ", iClosest)
								ENDIF	
							ELSE
								IF DOES_BLIP_HAVE_GPS_ROUTE(sLocaldata.blipDrop[iClosest])
									SET_BLIP_ROUTE(sLocaldata.blipDrop[iClosest], FALSE)
									PRINTLN("[EXEC1] [CTRB_SELL] [BLIP] MAINTAIN_DROP_BLIPS, SET_BLIP_ROUTE ", iClosest)
								ENDIF
							ENDIF
						ENDIF
						
					ENDIF
				ENDIF		
			ENDIF
			
			IF NOT DOES_BLIP_EXIST(sLocaldata.blipDrop[i])
				IF iVeh = -1
					iVeh = GET_SELL_VEHICLE_THAT_HAS_NOT_COMPLETED_DROP(i)
				ENDIF
			
				vCoords = GET_SELL_DROP_COORDS(GET_SELL_VAR(), i, serverBD.iRoute, iVeh)
				PRINTLN("[EXEC1] [CTRB_SELL] [BLIP] MAINTAIN_DROP_BLIPS, ADD_DROP_BLIP ", i, " vCoords = ",vCoords)
				sLocaldata.blipDrop[i] = ADD_BLIP_FOR_COORD(vCoords)
				IF bAIR_VARIATION()
				OR bSTING()
				OR bMULTIPLE()
					sLocaldata.blipDropArea[i] = ADD_BLIP_FOR_RADIUS(vCoords, GET_AREA_BLIP_RADIUS())
					SET_BLIP_COLOUR(sLocaldata.blipDropArea[i], BLIP_COLOUR_YELLOW)
					SET_BLIP_ALPHA(sLocaldata.blipDropArea[i], 100)
					SHOW_HEIGHT_ON_BLIP(sLocaldata.blipDropArea[i], FALSE)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC REMOVE_DROP_BLIPS()
	INT i
	REPEAT ciMAX_SELL_DROP_OFFS i
		IF DOES_BLIP_EXIST(sLocaldata.blipDrop[i])
			PRINTLN("[EXEC1] [CTRB_SELL] [BLIP] REMOVE_DROP_BLIP  ")
			REMOVE_BLIP(sLocaldata.blipDrop[i])
		ENDIF
		IF DOES_BLIP_EXIST(sLocaldata.blipDropArea[i])
			PRINTLN("[EXEC1] [CTRB_SELL] [BLIP] REMOVE_DROP_BLIP  ")
			REMOVE_BLIP(sLocaldata.blipDropArea[i])
		ENDIF
	ENDREPEAT
ENDPROC

FUNC VECTOR vMARKER_DIMENSIONS()

	IF bAIR_VARIATION()
		RETURN <<cfSELL_CRATE_LOC_SIZE_Z, cfSELL_CRATE_LOC_SIZE_Z, cfSELL_CRATE_LOC_SIZE_Z>>
	ENDIF
	
	IF bSEA_VARIATION()
		RETURN <<ciSEA_DELIVERY_DISTANCE*2, ciSEA_DELIVERY_DISTANCE*2, 2.0>>
	ENDIF
	
	RETURN <<ciDELIVERY_DISTANCE, ciDELIVERY_DISTANCE, 0.5>>
ENDFUNC

FUNC INT iMARKER_ALPHA( INT iDefault)
	#IF IS_DEBUG_BUILD
	IF tiAlphaMarker <> ciMARKER_ALPHA
		
		RETURN tiAlphaMarker
	ENDIF
	#ENDIF
	
	IF bAIR_VARIATION()
	
		RETURN ciMARKER_ALPHA
	ENDIF
	
	RETURN iDefault
ENDFUNC

PROC DRAW_DROP_OFF_MARKER()
	INT iDropOffBlipR, iDropOffBlipG, iDropOffBlipB, iDropOffBlipA//, i
	GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iDropOffBlipR, iDropOffBlipG, iDropOffBlipB, iDropOffBlipA)
	
	IF NOT bSTING()
	AND NOT bMULTIPLE()
		
		IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(),SCRIPT_TASK_LEAVE_VEHICLE) <>  WAITING_TO_START_TASK
			AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(),SCRIPT_TASK_LEAVE_VEHICLE) <>  PERFORMING_TASK
				IF IS_PLAYER_IN_ANY_SELL_VEH()
					INT iMyVeh = GET_MY_SELL_VEH_AS_INT()
					IF iMyVeh >= 0
						DRAW_MARKER(MARKER_CYLINDER, GET_SELL_DROP_COORDS(GET_SELL_VAR(), serverBD.iClosestDropOff[iMyVeh], serverBD.iRoute, GET_SELL_VEH_I_AM_IN_INT()), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 
									vMARKER_DIMENSIONS(), iDropOffBlipR, iDropOffBlipG, iDropOffBlipB, iMARKER_ALPHA( iDropOffBlipA))
					ENDIF
//				ELSE
//					REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
//						DRAW_MARKER(MARKER_CYLINDER, GET_SELL_DROP_COORDS(GET_SELL_VAR(), serverBD.iClosestDropOff[i], serverBD.iRoute, i), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 
//								vMARKER_DIMENSIONS(), iDropOffBlipR, iDropOffBlipG, iDropOffBlipB, iMARKER_ALPHA( iDropOffBlipA))
//								
//					ENDREPEAT
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC REMOVE_ALL_BLIPS()
 	REMOVE_CONTRABAND_BLIPS()
	REMOVE_DROP_BLIPS()
	REMOVE_PED_BLIPS()
	REMOVE_VEHICLE_BLIPS()
	TOGGLE_LSIA_AIRPORT_BLIP(FALSE)
ENDPROC

CONST_INT ciWARNING_TIMER 	600000 // 10MIN
CONST_INT ciEND_TIMER_15	900000 // 15MIN
CONST_INT ciEND_TIMER_20	1800000 // 20MIN
//CONST_INT ciEND_TIMED_VAR	420000 // 7MIN

FUNC INT iTIME_LIMIT()//()

//	IF bTIMED()
//	
//		RETURN serverBD.iTimedTimeLimit
//	ENDIF

	RETURN GET_MODE_TUNEABLE_TIME_LIMIT() //ciEND_TIMER_15
ENDFUNC

FUNC INT GET_TIME_REMAINING()//()
	
	INT iTimeReturn
	
	iTimeReturn = ( iTIME_LIMIT() - ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), serverBD.modeTimer.Timer)) )

	IF iTimeReturn < 0
		iTimeReturn = 0
	ELIF iTimeReturn > iTIME_LIMIT()
		iTimeReturn = iTIME_LIMIT() 
	ENDIF
	
	RETURN iTimeReturn
ENDFUNC

//FUNC INT GET_UNLOAD_TIME_REMAINING()
//	
//	INT iTimeReturn
//	
//	iTimeReturn = ( ciDROP_TIME - ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), serverBD.stDroppedOff.Timer)) )
//
//	IF iTimeReturn < 0
//		iTimeReturn = 0
//	ELIF iTimeReturn > ciDROP_TIME
//		iTimeReturn = ciDROP_TIME 
//	ENDIF
//	
//	RETURN iTimeReturn
//ENDFUNC

FUNC INT GET_REPAIR_TIME_REMAINING()
	
	INT iTimeReturn
	
	iTimeReturn = ( ciREPAIR_TIME - ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), serverBD.stRepair.Timer))  )

	IF iTimeReturn < 0
		iTimeReturn = 0
	ELIF iTimeReturn > ciREPAIR_TIME
		iTimeReturn = ciREPAIR_TIME 
	ENDIF
	
	RETURN iTimeReturn
ENDFUNC

FUNC INT iDROPPED_OFF_COUNT_WITH_DELAY()
	
	INT iDropOffCount = iDROPPED_OFF_COUNT()
	//If it's all been dropped then use default behavlor
	IF iDropOffCount = iDROP_TARGET()
		IF NOT bAIR_RESTRICTED()
			RETURN iDropOffCount
		ELIF IS_LOCAL_BIT0_SET(eLOCALBITSET0_WANTED_DELAY_CLEARED)
			RETURN iDropOffCount
		ENDIF
	ENDIF
	
	//Start a timer when it's been dropped, incrament the count once the timer has expired
	INT iLoop
	INT iCount
	IF iDropOffCount > 0
		REPEAT iDropOffCount iLoop
			IF NOT HAS_NET_TIMER_STARTED(sLocaldata.stDropTimer[iLoop])
				START_NET_TIMER(sLocaldata.stDropTimer[iLoop])
			ELIF HAS_NET_TIMER_EXPIRED(sLocaldata.stDropTimer[iLoop], g_sMPTunables.iDROPPED_OFF_COUNT_WITH_DELAY)
				iCount++
			ENDIF
		ENDREPEAT
	ENDIF
	
	RETURN iCount

ENDFUNC

FUNC INT iAMOUNT_DROPPED_OFF_FOR_HUD()
	SWITCH GET_SELL_VAR()
		CASE eVAR_DEFAULT 	
		CASE eVAR_LAND_DAMAGE
		CASE eVAR_LAND_TRACKIFY
		CASE eVAR_LAND_ATTACK
		CASE eVAR_LAND_DEFEND
		CASE eVAR_AIR_CLEAR_AREA
		
			RETURN serverBD.iVehicleCountDeliveredAllContraband
			
		CASE eVAR_AIR_RESTRICTED
		CASE eVAR_AIR_DROP
		
			RETURN iDROPPED_OFF_COUNT_WITH_DELAY()
			
	ENDSWITCH

	RETURN iDROPPED_OFF_COUNT()
ENDFUNC

FUNC INT iAMOUNT_TO_DROP_OFF_FOR_HUD()
	SWITCH GET_SELL_VAR()
		CASE eVAR_DEFAULT 	
		CASE eVAR_LAND_DAMAGE
		CASE eVAR_LAND_TRACKIFY
		CASE eVAR_LAND_ATTACK
		CASE eVAR_LAND_DEFEND
		CASE eVAR_AIR_CLEAR_AREA
		
			RETURN GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE()
			
	ENDSWITCH

	RETURN iDROP_TARGET()
ENDFUNC

FUNC BOOL SHOULD_SHOW_CONTRABAND_UI()

	IF (bMULTIPLE())
	OR (bAIR_LOW())
	OR (bSTING())
	OR (bAIR_ATTACK())
	OR (bAIR_DROP())
	OR (bAIR_RESTRICTED())
	OR bLAND_DAMAGE()
	OR ( bNORMAL() AND DOES_SHIPMENT_TYPE_HAVE_MULTIPLE_VEHICLES() )
	OR ( bLAND_ATTACK() AND DOES_SHIPMENT_TYPE_HAVE_MULTIPLE_VEHICLES() )
	OR ( bLAND_DEFEND() AND DOES_SHIPMENT_TYPE_HAVE_MULTIPLE_VEHICLES() )
	OR ( bLAND_TRACKIFY() AND DOES_SHIPMENT_TYPE_HAVE_MULTIPLE_VEHICLES() )
	OR ( bAIR_CLEAR_AREA() AND DOES_SHIPMENT_TYPE_HAVE_MULTIPLE_VEHICLES() )
	
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

PROC MAINTAIN_GLOBAL_SIGNAL_TIMER()
	IF SHOULD_DELAY_CONTRABAND_BLIP_FOR_BUYER()
		IF HAS_NET_TIMER_STARTED(serverBD.stBlipDelayTimer)
			IF NOT HAS_NET_TIMER_EXPIRED(serverBD.stBlipDelayTimer, GET_CONTRABAND_BLIP_DELAY_TIME_FOR_BUYER())
				INT iEventTime
				HUD_COLOURS eColour
		
				iEventTime = GET_CONTRABAND_BLIP_DELAY_TIME_FOR_BUYER() - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.stBlipDelayTimer)
				
				IF iEventTime > CONTRABAND_NEARLY_GLOBAL_TIME
					eColour = HUD_COLOUR_WHITE
				ELSE
					eColour = HUD_COLOUR_RED
				ENDIF
			
				IF iEventTime > 0
					DRAW_GENERIC_TIMER(iEventTime, "BYCB_GLBPNG", 0, TIMER_STYLE_DONTUSEMILLISECONDS, -1, PODIUMPOS_NONE, HUDORDER_DONTCARE, FALSE, eColour, HUDFLASHING_NONE, 0, FALSE, eColour)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_DO_RADAR_JAM_HUD_AND_SOUNDS(INT iVeh)

	IF (IS_DRIVER_OF_SELL_VEH(iVeh)
	AND DOES_SELL_VEH_HAVE_PASSENGER(iVeh))
		RETURN TRUE
	ENDIF
	
	IF (IS_PASSENGER(iVeh)
	AND GET_DRIVER_OF_SELL_VEH(iVeh) != INVALID_PLAYER_INDEX())
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

PROC DRAW_HUD_BOTTOM_RIGHT()

	HUD_COLOURS hcColour = HUD_COLOUR_WHITE
	
	sLocalData.iTimeLeft = GET_TIME_REMAINING()	
	
	IF (IS_OK_TO_DRAW_BOTTOM_RIGHT_HUD(TRUE)
	OR bLAND_TRACKIFY())
	AND sLocalData.iTimeLeft < iTIME_LIMIT()
	
		// BONUS $10,000
		IF bLAND_DAMAGE()
		AND HAS_ANY_SELL_VEHICLE_BEEN_ENTERED()
		
			IF serverBD.iBonus > 99999

				SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()

			ELIF serverBD.iBonus > 999

				SET_MIDDLE_TITLE_POSITION_HUD_THIS_FRAME()
			ENDIF
		
			DRAW_GENERIC_SCORE(serverBD.iBonus, "SCONTRA_HUD_DMG", DEFAULT, HUD_COLOUR_GOLD, HUDORDER_THIRDBOTTOM, FALSE, "HUD_CASH")
		ENDIF
	
		// Repair meter
		IF HAS_NET_TIMER_STARTED(serverBD.stRepair)

			START_NET_TIMER(sLocaldata.stExtraRepairHudTime)

			DRAW_GENERIC_METER(( ciREPAIR_TIME - GET_REPAIR_TIME_REMAINING() ), ciREPAIR_TIME, "SCONTRA_REPAIR", HUD_COLOUR_RED, DEFAULT, HUDORDER_THIRDBOTTOM) 
			
		// url:bugstar:2768230 - Sell - Land Breakdown - The Repairing bar should stay on screen for a couple of extra seconds once it has reached full. 
		ELIF HAS_NET_TIMER_STARTED(sLocaldata.stExtraRepairHudTime) 
			IF HAS_NET_TIMER_EXPIRED(sLocaldata.stExtraRepairHudTime, ( ciREPAIR_TIME + 2000 ))
				RESET_NET_TIMER(sLocaldata.stExtraRepairHudTime)
			ELSE
				DRAW_GENERIC_METER(ciREPAIR_TIME, ciREPAIR_TIME, "SCONTRA_REPAIR", HUD_COLOUR_RED, DEFAULT, HUDORDER_THIRDBOTTOM) 
			ENDIF
		ENDIF

		IF sLocalData.iTimeLeft <= 30000
			hcColour = HUD_COLOUR_RED
		ENDIF
		
		IF bAIR_VARIATION()
			BOOL bPlayRadarJamSounds = FALSE
			INT i
			REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
				IF IS_SELL_VEH_OK(i)
					IF SHOULD_DO_RADAR_JAM_HUD_AND_SOUNDS(i)
						bPlayRadarJamSounds = TRUE
					ENDIF
				ENDIF
			ENDREPEAT
			
			IF bPlayRadarJamSounds
				MAINTAIN_RADAR_JAM_HUD()
			ELSE
				CLEANUP_RADAR_JAM_SOUNDS()
			ENDIF
		ENDIF
		
		// Dropped off meter
//		IF HAS_NET_TIMER_STARTED(serverBD.stDroppedOff)
//			DRAW_GENERIC_METER(GET_UNLOAD_TIME_REMAINING(), ciDROP_TIME, "SCONTRA_ST_DRP", HUD_COLOUR_WHITE, DEFAULT, HUDORDER_THIRDBOTTOM) 
//		ENDIF
		
		MAINTAIN_GLOBAL_SIGNAL_TIMER()
		
		// CONTRABAND SOLD 0/5 
		IF SHOULD_SHOW_CONTRABAND_UI()
			DRAW_GENERIC_BIG_DOUBLE_NUMBER(iAMOUNT_DROPPED_OFF_FOR_HUD(), iAMOUNT_TO_DROP_OFF_FOR_HUD(), "SCONTRA_HUD_SC", DEFAULT, HUD_COLOUR_WHITE, HUDORDER_SECONDBOTTOM)
		ENDIF
		
		IF NOT bLAND_DEFEND()
			IF bAIR_CLEAR_AREA()
			AND SHOULD_DO_KILL_ENEMIES_HELP()
				INT iNumber = GET_NUMBER_OF_ATTACKING_PEDS_THAT_ARE_DEAD()
				BOTTOM_RIGHT_UI_VEHTEAM_ENEMIES_TIMER(iNumber, iNumber, GET_NUM_PEDS_FOR_VARIATION(), sLocalData.iTimeLeft, hcColour, DEFAULT, "SCONTRA_TIMER")
			ELSE
				IF bLAND_TRACKIFY()
				AND IS_PHONE_ONSCREEN()
					SET_PHONE_UNDER_HUD_THIS_FRAME()
				ENDIF
			
				// Mission time
				DRAW_GENERIC_TIMER(sLocalData.iTimeLeft, "SCONTRA_TIMER", DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDORDER_BOTTOM, DEFAULT, hcColour)
			ENDIF
		ELSE
			IF serverBD.iDroppedOffCount >= iDROP_TARGET()
				INT iKillTarget = GET_TOTAL_KILLS_TARGET()
				//-- Kills / total kills + mission time, taken from Kill List
				BOTTOM_RIGHT_UI_VEHTEAM_ENEMIES_TIMER(serverBD.iTotalKills, serverBD.iTotalKills, iKillTarget, sLocalData.iTimeLeft, hcColour, DEFAULT, "SCONTRA_TIMER")
			ELSE
				// Mission time
				DRAW_GENERIC_TIMER(sLocalData.iTimeLeft, "SCONTRA_TIMER", DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDORDER_BOTTOM, DEFAULT, hcColour)
			ENDIF
		ENDIF

	ENDIF
ENDPROC

PROC MANAGE_TEXT_MESSAGES()
	// Boss openinbg text message 
//	IF GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID())	
//		IF NOT IS_BIT_SET(sLocaldata.iTextMessageBitSet, ciTEXT_BIT_BOSS_OPEN_WAREHOUSE)
//			IF bAIR_CLEAR_AREA()
//				IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(GET_OFFICE_PA_CHAR(TRUE), "SCONTRA_TXT_6", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED) 
//					SET_BIT(sLocaldata.iTextMessageBitSet, ciTEXT_BIT_BOSS_OPEN_WAREHOUSE)
//				ENDIF
//			ELIF bMULTIPLE()
//			OR bAIR_VARIATION()
//			OR bSTING()
//				// "The delivery truck is ready for moving, boss. Get it safely to the drop location to make the sale. Just be careful, there are multiple drop-offs to make."
//				IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(GET_OFFICE_PA_CHAR(TRUE), "SCONTRA_TXT_7", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED) 
//					SET_BIT(sLocaldata.iTextMessageBitSet, ciTEXT_BIT_BOSS_OPEN_WAREHOUSE)
//				ENDIF
//			ELSE
//				IF GET_COUNT_OF_WAREHOUSES_OWNED() > 1
//					// "The delivery truck is ready for moving, boss. Get it safely to the drop location to make the sale. You can also collect additional shipments for your other warehouses, but be careful as others may try and take your shipment out. Hire more Bodyguards if you need to."
//					IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(GET_OFFICE_PA_CHAR(TRUE), "SCONTRA_TXT_1", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED) 
//						SET_BIT(sLocaldata.iTextMessageBitSet, ciTEXT_BIT_BOSS_OPEN_WAREHOUSE)
//					ENDIF
//				ELSE
//					// "The delivery truck is ready for moving, boss. Get it safely to the drop location to make the sale. Just be careful out there as others may try and take your shipment out. Hire more Bodyguards if you need to."
//					IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(GET_OFFICE_PA_CHAR(TRUE), "SCONTRA_TXT_1", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED) 
//						SET_BIT(sLocaldata.iTextMessageBitSet, ciTEXT_BIT_BOSS_OPEN_WAREHOUSE)
//					ENDIF
//				ENDIF
//			ENDIF
//			SET_BIT(sLocaldata.iTextMessageBitSet, ciTEXT_BIT_BOSS_OPEN_WAREHOUSE)
//		ENDIF
//	ELSE

	IF NOT IS_BIT_SET(sLocaldata.iTextMessageBitSet, ciTEXT_BIT_UNKNOWN)
		IF bSEA_VARIATION()
		OR bAIR_VARIATION()
			IF NOT GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
			OR IS_LOCAL_BIT0_SET(eLOCALBITSET0_PHONE_TRIGGERED)
				IF NOT IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()
				AND NOT IS_PHONE_ONSCREEN()
					// "Hello, as per the PA's request the goods have been moved to the specified delivery vehicle."
					IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_MP_RAY_LAVOY, "SCONTRA_TXT_21", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED) 
						SET_BIT(sLocaldata.iTextMessageBitSet, ciTEXT_BIT_UNKNOWN)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			SET_BIT(sLocaldata.iTextMessageBitSet, ciTEXT_BIT_UNKNOWN)
		ENDIF
	ELSE
		IF NOT GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID())	
			// Goon opening text message
			IF NOT IS_BIT_SET(sLocaldata.iTextMessageBitSet, ciTEXT_BIT_GOON_OPEN)
				IF bAIR_VARIATION()
					//Suit up: we’re making a drop and you’re the backup. Keep the boss and the merchandise safe.
					IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(GET_OFFICE_PA_CHAR(TRUE), "SCONTRA_TXT_20", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED) 
						SET_BIT(sLocaldata.iTextMessageBitSet, ciTEXT_BIT_GOON_OPEN)
					ENDIF
				ELIF DOES_SHIPMENT_TYPE_HAVE_MULTIPLE_VEHICLES()
					// "Help my boss, your VIP, to deliver the vehicles and make the sale. Protect them at all costs. It’s your wages on the line if the vehicle goes BOOM!"
					IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(GET_OFFICE_PA_CHAR(TRUE), "SCONTRA_TXT_4P", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED) 
						SET_BIT(sLocaldata.iTextMessageBitSet, ciTEXT_BIT_GOON_OPEN)
					ENDIF
				ELSE
					// "Help my boss, your VIP, to deliver the vehicle and make the sale. Protect them at all costs. It’s your wages on the line if the vehicle goes BOOM!"
					IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(GET_OFFICE_PA_CHAR(TRUE), "SCONTRA_TXT_4", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED) 
						SET_BIT(sLocaldata.iTextMessageBitSet, ciTEXT_BIT_GOON_OPEN)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(sLocaldata.iTextMessageBitSet, ciTEXT_BIT_TWIST_TEXT_SENT)
			SWITCH GET_SELL_VAR()
				CASE eVAR_SEA_ATTACK
					IF serverBD.fNumWavesSpawned > 0
						// Watch out you have enemies on route to your location – hold them off until you make the sale.
						IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(GET_OFFICE_PA_CHAR(TRUE), "SCONTRA_TXT_19", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED) 
							SET_BIT(sLocaldata.iTextMessageBitSet, ciTEXT_BIT_TWIST_TEXT_SENT)
						ENDIF
					ENDIF
				BREAK
				CASE eVAR_SEA_DEFEND
					IF IS_SERVER_BIT0_SET(eSERVERBITSET0_SEA_DEFEND_PLAYER_IN_RANGE)
						// Sneaky assholes – I see there are some gang members hanging around the boat. Take them out or try and escape in the boat – your call.
						IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(GET_OFFICE_PA_CHAR(TRUE), "SCONTRA_TXT_18", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED) 
							SET_BIT(sLocaldata.iTextMessageBitSet, ciTEXT_BIT_TWIST_TEXT_SENT)
						ENDIF
					ENDIF
				BREAK
				CASE eVAR_AIR_CLEAR_AREA
					IF HAS_ANY_AIR_CLEAR_AREA_PLANE_LANDED()
						IF ARE_ALL_ATTACKING_PEDS_DEAD()
							SET_BIT(sLocaldata.iTextMessageBitSet, ciTEXT_BIT_TWIST_TEXT_SENT)
						// "Shit, change of plan. We've set up an emergency landing for you. Get the plane safely to the drop location to make the sale."
						ELIF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(GET_OFFICE_PA_CHAR(TRUE), "SCONTRA_TXT_13", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED) 
							SET_BIT(sLocaldata.iTextMessageBitSet, ciTEXT_BIT_TWIST_TEXT_SENT)
						ENDIF
					ENDIF
				BREAK
				CASE eVAR_AIR_LOW
					IF IS_DRIVER_OF_ANY_SELL_VEH()
						// "We don't want the cops on our tail. Flow low for this one! Keep an eye on your radar."
						IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(GET_OFFICE_PA_CHAR(TRUE), "SCONTRA_TXT_11", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED) 
							SET_BIT(sLocaldata.iTextMessageBitSet, ciTEXT_BIT_TWIST_TEXT_SENT)
						ENDIF
					ENDIF
				BREAK
				CASE eVAR_LAND_DAMAGE
					IF HAS_ANY_SELL_VEHICLE_BEEN_ENTERED()
						// "The buyer has put up a bonus on this one. Deliver the Contraband in mint condition to receive the most cash. Drive carefully!"
						IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(GET_OFFICE_PA_CHAR(TRUE), "SCONTRA_TXT_10", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED) 
							SET_BIT(sLocaldata.iTextMessageBitSet, ciTEXT_BIT_TWIST_TEXT_SENT)
						ENDIF
					ENDIF
				BREAK
				CASE eVAR_LAND_TRACKIFY
					IF HAS_ANY_SELL_VEHICLE_BEEN_ENTERED()
						IF AM_I_IN_KEY_ORGANISATION()
							// "The buyer is being extra cautious with this one. They haven't given us their exact location, but pinged a Trackify signal for you to follow. Happy hunting!"
							IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(GET_OFFICE_PA_CHAR(TRUE), "SCONTRA_TXT_12", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED) 
								SET_BIT(sLocaldata.iTextMessageBitSet, ciTEXT_BIT_TWIST_TEXT_SENT)
							ENDIF
						ENDIF
					ENDIF
				BREAK			
				CASE eVAR_LAND_DEFEND
					IF serverBD.iDroppedOffCount >= iDROP_TARGET()
						// Uh oh, something isn't right here. I'm detecting enemies closing in around you. Defend the area and keep the contraband safe to complete the sale.
						IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(GET_OFFICE_PA_CHAR(TRUE), "SCONTRA_TXT_14", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED) 
							SET_BIT(sLocaldata.iTextMessageBitSet, ciTEXT_BIT_TWIST_TEXT_SENT)
						ENDIF
					ENDIF
				BREAK
				CASE eVAR_AIR_ATTACK
				//	IF IS_PLAYER_IN_ANY_SELL_VEH()
						INT iVeh
						REPEAT ciMAX_CONTRABAND_PED_VEHICLES iVeh
							IF IS_BIT_SET(serverBD.iAirAttackHeliSpooked, iVeh)	
								IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPedVeh[iVeh].netIndex)
									IF IS_NET_VEHICLE_DRIVEABLE(serverBD.sPedVeh[iVeh].netIndex)
									//	IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_ENTITY_COORDS(NET_TO_VEH(serverBD.sPedVeh[iVeh].netIndex))) < 300.0
											// Bogey on your six! Watch out for enemy missiles and flex your flying skills to complete the sale in one piece. Good luck!
											IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(GET_OFFICE_PA_CHAR(TRUE), "SCONTRA_TXT_15", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED) 
												SET_BIT(sLocaldata.iTextMessageBitSet, ciTEXT_BIT_TWIST_TEXT_SENT)
											ENDIF
									//	ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDREPEAT
				//	ENDIF
				BREAK
				
				CASE eVAR_AIR_RESTRICTED
					IF serverBD.iLastDropOff[0] = -1
					AND HAS_ANY_SELL_VEHICLE_BEEN_ENTERED()
						// Okay so the last drop-off is over a restricted area. Quickly get in there, make the drop, avoid getting shot down and then lose the cops. Simple!
						IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(GET_OFFICE_PA_CHAR(TRUE), "SCONTRA_TXT_17", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED) 
							SET_BIT(sLocaldata.iTextMessageBitSet, ciTEXT_BIT_TWIST_TEXT_SENT)
						ENDIF
					ENDIF
				BREAK
				CASE eVAR_STING
					IF sLocaldata.iWantedStored != -1
						IF NOT IS_BIT_SET(GlobalplayerBD_FM[NETWORK_PLAYER_ID_TO_INT()].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bNoCopsActive)
							// Cops had eyes on that drop-off, Boss. Shake them before completing the delivery.
							IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(GET_OFFICE_PA_CHAR(TRUE), "SCONTRA_TXT_8", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED) 
								SET_BIT(sLocaldata.iTextMessageBitSet, ciTEXT_BIT_TWIST_TEXT_SENT)
							ENDIF
						ELSE
							PRINTLN("[EXEC1] [CTRB_SELL] Not doing text message as cops yurn blind eye is active")
							SET_BIT(sLocaldata.iTextMessageBitSet, ciTEXT_BIT_TWIST_TEXT_SENT)
						ENDIF
					ENDIF
				BREAK
				CASE eVAR_LAND_ATTACK
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPedVeh[0].netIndex)
					OR NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPedVeh[1].netIndex)
						// Urgh, these guys. Take them out Boss – you're not going to be able to lose them easily.
						IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(GET_OFFICE_PA_CHAR(TRUE), "SCONTRA_TXT_16", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED) 
							SET_BIT(sLocaldata.iTextMessageBitSet, ciTEXT_BIT_TWIST_TEXT_SENT)
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
ENDPROC

FUNC STRING sDROP_TEXT( BOOL bDriver)

	STRING sDropText

	IF bDriver
		IF bAIR_LOW()
			IF bUSE_PLURAL()
				// "Fly low and deliver Contraband to the ~y~drop-offs."
				sDropText = "SCONTRA_FLY_DP"
			ELSE
				// "Fly low and deliver Contraband to the ~y~drop-off."
				sDropText = "SCONTRA_FLY_D"
			ENDIF
		ELIF bAIR_VARIATION()
		OR bSEA_VARIATION()
			// "Deliver the Contraband to the ~y~restricted area."
			IF bAIR_RESTRICTED()
			AND AM_I_IN_ANY_SELL_VEH()
			AND serverBD.iLastDropOff[GET_SELL_VEH_ARRAY_POS_I_AM_IN()] = -1
				sDropText = "SCONTRA_DROPR"
			ELIF bUSE_PLURAL()
				// "Deliver the Contraband to the ~y~drop-offs."
				sDropText = "SCONTRA_DROP_1P"
			ELSE
				// "Deliver the Contraband to the ~y~drop-off."
				sDropText = "SCONTRA_DROP_1"
			ENDIF
		ELIF bLAND_TRACKIFY()
		AND NOT IS_SERVER_BIT1_SET(eSERVERBITSET1_LAND_TRACKIFY_SHOULD_BLIP_DROP_OFFS)
			// "Use Trackify to search for the drop-off."
			sDropText = "SCONTRA_DROP_T"
		ELSE
			IF bUSE_PLURAL()
				// "Deliver the truck to the ~y~drop-off."
				sDropText = "SCONTRA_DROPP"
			ELSE
				// "Deliver the truck to the ~y~drop-off."
				sDropText = "SCONTRA_DROP"
			ENDIF
		ENDIF
	ELSE
		IF bLAND_TRACKIFY()
		AND NOT IS_SERVER_BIT1_SET(eSERVERBITSET1_LAND_TRACKIFY_SHOULD_BLIP_DROP_OFFS)
			sDropText = "SCONTRA_HLP_T"
		ELSE
			IF bUSE_PLURAL()
				sDropText = "SCONTRA_HLP_DP"
			ELSE
				sDropText = "SCONTRA_HLP_D"
			ENDIF
		ENDIF
	
//		IF bAIR_VARIATION()
//		OR bSEA_VARIATION()
//			// "Help deliver the ~b~plane~s~ to the ~y~drop-off."
//			sDropText = "SCONTRA_DROP_A"
//		ELSE
//			IF bUSE_PLURAL()
//				// "Help deliver the ~b~truck~s~ to the ~y~drop-offs."
//				sDropText = "SCONTRA_DROP_HP"
//			ELSE
//				// "Help deliver the ~b~truck~s~ to the ~y~drop-off."
//				sDropText = "SCONTRA_DROP_H"
//			ENDIF
//		ENDIF
	ENDIF
	
	RETURN sDropText
ENDFUNC

FUNC STRING sVEH_TEXT()

	IF bAIR_VARIATION()
		IF bAIR_RESTRICTED()
		AND ARE_ALL_SELL_VEHS_OCCUPIED()
			// Wait for your organisation to lose the cops
			RETURN "SCONTRA_VEH_LC"
		ELSE
			IF DOES_SHIPMENT_TYPE_HAVE_MULTIPLE_VEHICLES()
				IF IS_PLAYER_IN_AIRPORT()
				OR HAS_ANY_SELL_VEHICLE_BEEN_ENTERED()
					// "Enter a ~HUD_COLOUR_BLUE~plane."
					RETURN "SCONTRA_VEH_1b"
				ELSE					
					// "Go to ~y~LSIA ~s~and enter a ~b~plane."
					RETURN "SCONTRA_DROP_LP"
				ENDIF
			ELSE
				IF IS_PLAYER_IN_AIRPORT()
				OR HAS_ANY_SELL_VEHICLE_BEEN_ENTERED()
					// "Enter the ~HUD_COLOUR_BLUE~plane."
					RETURN "SCONTRA_VEH_1"
				ELSE					
					// "Go to ~y~LSIA ~s~and enter the ~b~plane."
					RETURN "SCONTRA_DROP_LS"
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bSEA_VARIATION()
		RETURN "SCONTRA_VEH_2"
	ENDIF
	
	IF bVEH_BROKEN_DOWN()
		
		IF IS_TEAMMATE_REPAIRING()
			// "Protect <C>~a~</C> ~s~whilst the ~b~vehicle~s~ is repaired."
			RETURN "SCONTRA_RP_PR"
		ELSE
		
			// "Repair the ~HUD_COLOUR_BLUE~truck."
			RETURN "SCONTRA_VEH_R"
		ENDIF
	ENDIF
	
	IF DOES_SHIPMENT_TYPE_HAVE_MULTIPLE_VEHICLES()
		// "Enter a ~HUD_COLOUR_BLUE~truck."
		RETURN "SCONTRA_VEHb"
	ENDIF
	
	// "Enter the ~HUD_COLOUR_BLUE~truck."
	RETURN "SCONTRA_VEH"
ENDFUNC

PROC HANDLE_SELL_BLIPS(BOOL bMissionOver = FALSE)
	INT iVeh

	IF NOT bMissionOver
		MAINTAIN_PED_BLIPS()
		MAINTAIN_NON_CONTRABAND_VEHICLE_BLIPS()
		MAINTAIN_CONTRABAND_BLIPS()
		MAINTAIN_AIRPORT_BLIP()
		
		// If local player is in a sell vehicle, show the drop off blips for that vehicle,
		// otherwise, wait for all vehs to be occupied and blip one of the drop offs
		IF IS_PLAYER_IN_ANY_SELL_VEH(FALSE)
			REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() iVeh
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[iVeh])
					IF AM_I_IN_THIS_SELL_VEH(iVeh)
						MAINTAIN_DROP_BLIPS(iVeh)
					ENDIF
				ENDIF
			ENDREPEAT
		ELSE
			IF ARE_ALL_SELL_VEHS_OCCUPIED()
				MAINTAIN_DROP_BLIPS()
			ENDIF
		ENDIF
			
		//Flash the contraband blip
		IF bLAND_TRACKIFY()
			IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_FLASH_BLIP_DONE)
			AND IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SCONTRA_HLP2")
				INT i
				REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
					IF DOES_BLIP_EXIST(sLocaldata.blipVeh[i])
						PRINTLN("[EXEC1] [CTRB_SELL] [BRK] - SET_BLIP_FLASHES(sLocaldata.blipVeh, TRUE)")
						SET_BLIP_FLASHES(sLocaldata.blipVeh[i], TRUE)
						SET_BLIP_FLASH_INTERVAL(sLocaldata.blipVeh[i], BLIP_FLASHING_TIME)
						SET_BLIP_FLASH_TIMER(sLocaldata.blipVeh[i], FLASH_CONTRABAND_BLIP_DEFAULT_TIME)
					ENDIF
				ENDREPEAT
				SET_LOCAL_BIT0(eLOCALBITSET0_FLASH_BLIP_DONE)
			ENDIF
		ENDIF
	ELSE
		REMOVE_ALL_BLIPS()
	ENDIF
ENDPROC

FUNC BOOL DOES_ORGANIZATION_HAVE_WANTED_RATING()
	INT iParticipant
	PLAYER_INDEX playerID
	PARTICIPANT_INDEX participantID
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_NATIVE(PARTICIPANT_INDEX, iParticipant))
			participantID = INT_TO_NATIVE(PARTICIPANT_INDEX, iParticipant)
			playerID = NETWORK_GET_PLAYER_INDEX(participantID)
			IF IS_NET_PLAYER_OK(playerID, FALSE)
				IF GET_PLAYER_WANTED_LEVEL(playerID) != 0
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

PROC HANDLE_SELL_TEXT( BOOL bMissionOver = FALSE)
	BOOL bMissionEnding = FALSE
	
	IF NOT bLAND_DEFEND()
		IF HAS_ALL_CONTRABAND_BEEN_DROPPED_OFF()
		OR bMissionOver 
			bMissionEnding = TRUE
		ENDIF
	ELSE
		IF bMissionOver
			bMissionEnding = TRUE
		ELSE
			IF HAS_ALL_CONTRABAND_BEEN_DROPPED_OFF()
				IF serverBD.fNumWavesSpawned >= GET_MAX_AMBUSH_WAVES_FOR_VARIATION()
					IF ARE_ALL_ATTACKING_PEDS_DEAD()
						bMissionEnding = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
//	IF HAS_ALL_CONTRABAND_BEEN_DROPPED_OFF()
//	OR bMissionOver 
	IF bMissionEnding
		IF bAIR_RESTRICTED()
			 IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) != 0
				// "Lose the Cops."
				IF NOT Is_This_The_Current_Objective_Text("BYCB_CLY_LSC0")
					Print_Objective_Text("BYCB_CLY_LSC0")
				ENDIF
			ELIF DOES_ORGANIZATION_HAVE_WANTED_RATING()
				// Wait for your organisation to lose the cops
				IF NOT Is_This_The_Current_Objective_Text("SCONTRA_VEH_LC")
					Print_Objective_Text("SCONTRA_VEH_LC")
				ENDIF
			ELSE
				REMOVE_ALL_BLIPS()
				Clear_Any_Objective_Text_From_This_Script()
			ENDIF
		ELSE
			REMOVE_ALL_BLIPS()
			Clear_Any_Objective_Text_From_This_Script() // Clear god text, mode is over.
		ENDIF
	ELSE
		MANAGE_TEXT_MESSAGES()
	
		// Contraband help trigger ("Deliver the contraband ~HUD_COLOUR_BLUE~~BLIP_CONTRABAND~~HUD_COLOUR_WHITE~ to the drop-off to receive payment.")
		IF IS_SERVER_BIT0_SET(eSERVERBITSET0_VEHICLE_0_ENTERED_ONCE)
			IF GB_IS_LOCAL_HELP_BIT_SET(eGB_LOCAL_HELP_BITSET_NOTIFICATION_HIDE_CONTRABAND_PLAYER)
				GB_CLEAR_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_NOTIFICATION_HIDE_CONTRABAND_PLAYER)
			ENDIF
			IF GB_IS_LOCAL_HELP_BIT_SET(eGB_LOCAL_HELP_BITSET_NOTIFICATION_HIDE_CONTRABAND_PLAYER_P)
				GB_CLEAR_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_NOTIFICATION_HIDE_CONTRABAND_PLAYER_P)
			ENDIF
		ELSE
			IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_INITIAL_HELP_SENT)
				IF NOT IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()
					IF bUSE_PLURAL()
						GB_SET_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_NOTIFICATION_HIDE_CONTRABAND_PLAYER_P) 
					ELSE 
						GB_SET_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_NOTIFICATION_HIDE_CONTRABAND_PLAYER) 
					ENDIF
					IF DO_EASY_HELP_TEXT()
					AND GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID())
						GB_SET_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_EASY_SELL_MISSION)
					ENDIF
					SET_LOCAL_BIT0(eLOCALBITSET0_INITIAL_HELP_SENT)
				ENDIF
			ENDIF
		ENDIF
		
		//If the plane has landed then remove the blips and print the new god text
		IF SHOULD_DO_KILL_ENEMIES_HELP()
			IF NOT Is_This_The_Current_Objective_Text("SCONTRA_AIRCAE")
				Print_Objective_Text("SCONTRA_AIRCAE")
			ENDIF
		ELIF PLAYER_WANTED()			
			// "Lose the Cops."
			IF NOT Is_This_The_Current_Objective_Text("BYCB_CLY_LSC0")
				Print_Objective_Text("BYCB_CLY_LSC0")
			ENDIF
		ELIF IS_ANY_SELL_VEH_OCCUPIED()
			IF NOT g_bSellWarningHelpPrinted
			AND NOT GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
			AND NOT GB_IS_LOCAL_HELP_BIT_SET(eGB_LOCAL_HELP_BITSET_NOTIFICATION_WARN_ABOUT_GRIEF) 
				g_bSellWarningHelpPrinted = TRUE
				GB_SET_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_NOTIFICATION_WARN_ABOUT_GRIEF) 
				PRINTLN("[EXEC1] [CTRB_SELL] g_bSellWarningHelpPrinted = TRUE")
			ENDIF
		
			//Don't draw the markers if it's air drop
			IF NOT bAIR_VARIATION()
				DRAW_DROP_OFF_MARKER()
			ENDIF
			
			IF IS_DRIVER_OF_ANY_SELL_VEH()
				IF bAIR_DROP()
				OR bAIR_RESTRICTED()
				OR bAIR_ATTACK()
					IF IS_ENTITY_IN_AIR(GET_SELL_VEH_I_AM_IN())
						// "Press ~INPUT_FRONTEND_RIGHT~ to drop Contraband when you are flying above a drop-off."
						GB_SET_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_NOTIFICATION_AIR_DROP) 
					ENDIF
				ENDIF
				
				IF bAIR_RESTRICTED()
				AND serverBD.eShipmentType =  eSELL_SHIPMENT_TYPE_AIR_TWO_CUBAN800
				AND AM_I_IN_ANY_SELL_VEH()
				AND GET_SELL_VEH_DROP_OFF_COUNT(GET_SELL_VEH_I_AM_IN_INT()) = ciDROP_OFF_RESTRICTED
				AND GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
					//Help deliver the ~b~Contraband~s~ 
					IF NOT Is_This_The_Current_Objective_Text("SCONTRA_DROP_B")
						Print_Objective_Text("SCONTRA_DROP_B")
					ENDIF
				ELIF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) != 0
				AND bAIR_RESTRICTED()
					// "Lose the Cops."
					IF NOT Is_This_The_Current_Objective_Text("BYCB_CLY_LSC0")
						Print_Objective_Text("BYCB_CLY_LSC0")
					ENDIF
				ELSE
					IF NOT Is_This_The_Current_Objective_Text(sDROP_TEXT( IS_DRIVER_OF_ANY_SELL_VEH()))
						Print_Objective_Text(sDROP_TEXT( IS_DRIVER_OF_ANY_SELL_VEH()))
					ENDIF
				ENDIF
			ELSE
				IF NOT bAIR_RESTRICTED()
				OR GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
					IF ARE_ALL_SELL_VEHS_OCCUPIED()
						// "Help <C>~a~</C> ~s~deliver the ~a~~s~ to the ~y~drop-off."
						IF NOT Is_This_The_Current_Objective_Text(sDROP_TEXT( IS_DRIVER_OF_ANY_SELL_VEH()))					
							//Print_Objective_Text_With_String(sDROP_TEXT( IS_DRIVER_OF_ANY_SELL_VEH()), "SCONTRA_CTRA")
							Print_Objective_Text_With_Coloured_Text_Label(sDROP_TEXT( IS_DRIVER_OF_ANY_SELL_VEH()), "SCONTRA_CTRAS", HUD_COLOUR_BLUE)// GET_PLAYER_HUD_COLOUR(GB_GET_LOCAL_PLAYER_GANG_BOSS()))
						ENDIF
					ELSE
						PRINTLN("Print_Objective_Text, sVEH_TEXT aaa ")
						// "Enter the ~HUD_COLOUR_BLUE~truck."
						IF NOT Is_This_The_Current_Objective_Text(sVEH_TEXT())
							Print_Objective_Text(sVEH_TEXT())
						ENDIF
					ENDIF
				ELSE
					IF bAIR_RESTRICTED()
						IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) != 0
							IF NOT Is_This_The_Current_Objective_Text("BYCB_CLY_LSC0")
								Print_Objective_Text("BYCB_CLY_LSC0")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF
		ELSE
			IF NOT bLAND_DEFEND()
				IF IS_TEAMMATE_REPAIRING()
					// "Protect <C>~a~</C> ~s~whilst the ~b~vehicle~s~ is repaired."
					IF NOT Is_This_The_Current_Objective_Text(sVEH_TEXT())
						Print_Objective_Text(sVEH_TEXT())
					ENDIF
				ELSE
					PRINTLN("Print_Objective_Text, sVEH_TEXT bbb ")
					// "Enter the ~HUD_COLOUR_BLUE~truck."
					IF NOT Is_This_The_Current_Objective_Text(sVEH_TEXT())
						Print_Objective_Text(sVEH_TEXT())
					ENDIF
				ENDIF
			ELSE
				IF serverBD.iDroppedOffCount >= iDROP_TARGET()
					IF NOT Is_This_The_Current_Objective_Text("SCONTRA_DEF")
						Print_Objective_Text("SCONTRA_DEF") // Defend the ~HUD_COLOUR_BLUE~Contraband.
					ENDIF
				ELSE
					// "Enter the ~HUD_COLOUR_BLUE~truck."
					IF NOT Is_This_The_Current_Objective_Text(sVEH_TEXT())
						Print_Objective_Text(sVEH_TEXT())
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//╔═════════════════════════════════════════════════════════════════════════════╗
//║				WANTED															║

CONST_INT ciTIME_ALLOWED_FLY_LOW 	10000

PROC SET_WANTED_TO(INT iThisMuch)
	IF IS_NET_PLAYER_OK(PLAYER_ID(), FALSE)
		IF SHOULD_RESTRICT_MAX_WANTED_LEVEL()
			INT iMaxSellWanted = GET_MAX_WANTED_LEVEL_FOR_SELL_CONTRABAND()
			IF iThisMuch > iMaxSellWanted
				PRINTLN("[EXEC1] [CTRB_SELL] [SET_WANTED_TO] Want to set wanted level to ", iThisMuch, " but max wanted restricted to ", iMaxSellWanted)
				iThisMuch = iMaxSellWanted
			ENDIF
		ENDIF
		
		SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), iThisMuch)
		SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())	
	ENDIF
ENDPROC

PROC MAINTAIN_WANTED_RATINGS()

	INT iCurrentWanted
	iCurrentWanted = GET_PLAYER_WANTED_LEVEL(PLAYER_ID())
	
	// Set the flag for changing to lose wanted objective
	IF bSTING()
	OR bAIR_LOW()
		IF iCurrentWanted = 0
			IF IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), eCLIENTBITSET0_WANTED)
				CLEAR_CLIENT_BIT0(eCLIENTBITSET0_WANTED)
			ENDIF
		ELSE
			IF NOT IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), eCLIENTBITSET0_WANTED)
				SET_CLIENT_BIT0(eCLIENTBITSET0_WANTED)
			ENDIF
		ENDIF
	ENDIF
	IF bAIR_RESTRICTED()
		IF sLocaldata.iWantedStored = -1
		AND AM_I_IN_ANY_SELL_VEH()
		AND GET_SELL_VEH_DROP_OFF_COUNT(GET_SELL_VEH_I_AM_IN_INT()) = ciDROP_OFF_RESTRICTED
			IF iCurrentWanted < g_sMPTunables.iexec_sell_airrestricted_wanted_cap      		
				SET_WANTED_TO(g_sMPTunables.iexec_sell_airrestricted_wanted_cap)
			ENDIF
			PRINTLN("[EXEC1] [CTRB_SELL] [WTD] MAINTAIN_WANTED_RATINGS, iWantedCap = ", g_sMPTunables.iexec_sell_airrestricted_wanted_cap, " iCurrentWanted = ", iCurrentWanted)
			sLocaldata.iWantedStored = g_sMPTunables.iexec_sell_airrestricted_wanted_cap
		ENDIF
	ELIF bSTING()
		// Give a wanted rating at the sting drop-off
		IF sLocaldata.iWantedStored = -1
			IF serverBD.iSting = iDROPPED_OFF_COUNT()
				IF iCurrentWanted < g_sMPTunables.iexec_sell_sting_wanted_cap		
					SET_WANTED_TO(g_sMPTunables.iexec_sell_sting_wanted_cap)
				ENDIF
				PRINTLN("[EXEC1] [CTRB_SELL] [WTD] MAINTAIN_WANTED_RATINGS, iWantedCap = ", g_sMPTunables.iexec_sell_sting_wanted_cap, " iCurrentWanted = ", iCurrentWanted)
				sLocaldata.iWantedStored = g_sMPTunables.iexec_sell_sting_wanted_cap
				CLEAR_LOCAL_BIT0(eLOCALBITSET0_STOP_MAINTAINING_WANTED)
			ENDIF
		ELIF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_STOP_MAINTAINING_WANTED)
			IF IS_NET_PLAYER_OK(PLAYER_ID())
			AND GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iRespawnState = RESPAWN_STATE_PLAYING 
				//If it's set that we need to re-add the wanted after death then do so
//				IF IS_LOCAL_BIT0_SET(eLOCALBITSET0_ADD_WANTED_ON_DEATH)
//					IF NOT IS_SCREEN_FADED_IN()
//					AND IS_PLAYER_PLAYING(PLAYER_ID())
//						IF sLocaldata.iWantedCache != 0
//							IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < sLocaldata.iWantedCache
//								SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), sLocaldata.iWantedCache)
//								SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
//								PRINTLN("[EXEC1] - MAINTAIN_WANTED_RATINGS -given myself a ", sLocaldata.iWantedCache, " star wanted level because - eLOCALBITSET0_ADD_WANTED_ON_DEATH")
//							ENDIF
//						ENDIF
//						IF sLocaldata.iWantedCache = GET_PLAYER_WANTED_LEVEL(PLAYER_ID())
//							PRINTLN("[EXEC1] - MAINTAIN_WANTED_RATINGS -sLocaldata.iWantedCache = GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) - eLOCALBITSET0_ADD_WANTED_ON_DEATH")
//							CLEAR_LOCAL_BIT0(eLOCALBITSET0_ADD_WANTED_ON_DEATH)
//							CLEAR_LOCAL_BIT0(eLOCALBITSET0_WANTED_LEVEL_HAS_BEEN_APPLIED)
//						ENDIF
//					ENDIF
//				ELSE
					#IF IS_DEBUG_BUILD
					IF sLocaldata.iWantedCache != GET_PLAYER_WANTED_LEVEL(PLAYER_ID())
						PRINTLN("[EXEC1] - MAINTAIN_WANTED_RATINGS - sLocaldata.iWantedCache =  ", sLocaldata.iWantedCache, " - GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = ", GET_PLAYER_WANTED_LEVEL(PLAYER_ID()), " setting sLocaldata.iWantedCache = ", GET_PLAYER_WANTED_LEVEL(PLAYER_ID()))
					ENDIF
					#ENDIF
					sLocaldata.iWantedCache = GET_PLAYER_WANTED_LEVEL(PLAYER_ID())
					//If the wanted level gets to zero then set that we no longer need to maintain the wanted rating
					IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_WANTED_LEVEL_HAS_BEEN_APPLIED)
						IF sLocaldata.iWantedCache > 0
							SET_LOCAL_BIT0(eLOCALBITSET0_WANTED_LEVEL_HAS_BEEN_APPLIED)
							PRINTLN("[EXEC1] - MAINTAIN_WANTED_RATINGS - SET_LOCAL_BIT0(eLOCALBITSET0_WANTED_LEVEL_HAS_BEEN_APPLIED)")
						ENDIF
					ELSE
						IF sLocaldata.iWantedCache = 0
							SET_LOCAL_BIT0(eLOCALBITSET0_STOP_MAINTAINING_WANTED)
							PRINTLN("[EXEC1] - MAINTAIN_WANTED_RATINGS - sLocaldata.iWantedCache =  ", 0, " - SET_LOCAL_BIT0(eLOCALBITSET0_STOP_MAINTAINING_WANTED)")
						ENDIF
					ENDIF
//				ENDIF
//			ELSE
//				//Set it needs re-applied
//				IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_ADD_WANTED_ON_DEATH)
//					SET_LOCAL_BIT0(eLOCALBITSET0_ADD_WANTED_ON_DEATH)
//					PRINTLN("[EXEC1] - MAINTAIN_WANTED_RATINGS - SET_LOCAL_BIT0(eLOCALBITSET0_ADD_WANTED_ON_DEATH)")
//				ENDIF
			ENDIF
		ENDIF
	ELIF bAIR_LOW()
	AND HAS_NET_TIMER_STARTED(sLocaldata.timeTooHigh)
	AND HAS_NET_TIMER_EXPIRED(sLocaldata.timeTooHigh, ciTIME_ALLOWED_FLY_LOW)
		// Give wanted rating if flying too high for too long
		IF iCurrentWanted < g_sMPTunables.iexec_sell_airflylow_wanted_cap
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NETWORK_PLAYER_ID_TO_INT()].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bNoCopsActive)
				IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_GIVEN_WANTED_FOR_TOO_HIGH)
					SET_WANTED_TO(g_sMPTunables.iexec_sell_airflylow_wanted_cap)
					SET_LOCAL_BIT0(eLOCALBITSET0_GIVEN_WANTED_FOR_TOO_HIGH)
					PRINTLN("[EXEC1] - MAINTAIN_WANTED_RATINGS GIVEN WANTED LEVEL = ", g_sMPTunables.iexec_sell_airflylow_wanted_cap)
				ENDIF
			ENDIF
		ENDIF
	ELSE	
		// url:bugstar:2768502 - Sell Missions: Can you please suppress the Wanted level across all Sell Missions (expect for when we script it e.g. Air Restricted etc.)
		// unless you kill a cop...
		IF NOT IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), eCLIENTBITSET0_KILLED_COP)
			IF iCurrentWanted <> 0
				// url:bugstar:2768502 - Sell Missions: Can you please suppress the Wanted level across all Sell Missions (expect for when we script it e.g. Air Restricted etc.)
				IF NOT IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), eCLIENTBITSET0_WANTED)
					SET_WANTED_TO(0)
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF
ENDPROC

// Player pos when repairing with the blowtorch
FUNC VECTOR vREPAIR_COORDS()

	VECTOR vReturn
	vReturn = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(VEH_POS(), fVEH_HEADING(), <<0.0, 5.1, 0.0>>) // tfRepairY to debug Y
	
	IF GET_GROUND_Z_FOR_3D_COORD(vReturn, vReturn.z)
		RETURN vReturn
	ENDIF
	
	RETURN vReturn
ENDFUNC

PROC DO_REPAIR_ANIMS()
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
	AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())	
		IF (GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_START_SCENARIO_IN_PLACE) != PERFORMING_TASK
		AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_START_SCENARIO_IN_PLACE) != WAITING_TO_START_TASK)
			SET_ENTITY_COORDS(PLAYER_PED_ID(), vREPAIR_COORDS())
			SET_ENTITY_HEADING(PLAYER_PED_ID(), ( fVEH_HEADING() -180.00 ))
			TASK_START_SCENARIO_IN_PLACE(PLAYER_PED_ID(), "WORLD_HUMAN_WELDING", DEFAULT, FALSE)
//			TASK_START_SCENARIO_AT_POSITION(PLAYER_PED_ID(), "WORLD_HUMAN_WELDING", vREPAIR_COORDS(), ( fVEH_HEADING() -180.00 ))
			PRINTLN("[EXEC1] [CTRB_SELL] [BRK] - Tasked for scenario WORLD_HUMAN_WELDING")
		ENDIF
	ENDIF
ENDPROC

PROC STOP_REPAIR_ANIMS()
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
	AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		IF (GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_START_SCENARIO_IN_PLACE) = PERFORMING_TASK
		OR GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_START_SCENARIO_IN_PLACE) = WAITING_TO_START_TASK)
			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			PRINTLN("[EXEC1] [CTRB_SELL] [BRK] - STOP_REPAIR_ANIMS")
		ENDIF
	ENDIF
ENDPROC

CONST_INT ciREPAIR_START 	0
CONST_INT ciREPAIR_POSITION	1
CONST_INT ciREPAIRING		2
CONST_INT ciREPAIR_END		3

FUNC INT GET_REPAIR_STAGE()
	RETURN sLocaldata.iRepairProgress
ENDFUNC

PROC GOTO_REPAIR_STAGE(INT iStage)
	sLocaldata.iRepairProgress = iStage
	
	#IF IS_DEBUG_BUILD
	SWITCH iStage
		CASE ciREPAIR_START 	
			PRINTLN("GOTO_REPAIR_STAGE >> ciREPAIR_START ")
		BREAK
		CASE ciREPAIR_POSITION	
			PRINTLN("GOTO_REPAIR_STAGE >> ciREPAIR_POSITION ")
		BREAK
		CASE ciREPAIRING	
			PRINTLN("GOTO_REPAIR_STAGE >> ciREPAIRING ")
		BREAK
		CASE ciREPAIR_END	
			PRINTLN("GOTO_REPAIR_STAGE >> ciREPAIR_END ")
		BREAK
	ENDSWITCH
	#ENDIF
ENDPROC

PROC CLEANUP_REPAIR_FX()
	IF DOES_PARTICLE_FX_LOOPED_EXIST(sLocaldata.ptfxRepair)
		PRINTLN("[EXEC1] [CTRB_SELL] DO_REPAIR_EFFECTS, CLEANUP_REPAIR_FX ")
		STOP_PARTICLE_FX_LOOPED(sLocaldata.ptfxRepair)
	ENDIF
ENDPROC

// Smoke comes out the truck
PROC DO_REPAIR_EFFECTS(BOOL bDo)	
	IF bDo
		REQUEST_NAMED_PTFX_ASSET("scr_sell")
	
		IF HAS_NAMED_PTFX_ASSET_LOADED("scr_sell")
			IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(sLocaldata.ptfxRepair) 
				USE_PARTICLE_FX_ASSET("scr_sell")
				PRINTLN("[EXEC1] [CTRB_SELL] DO_REPAIR_EFFECTS, START_NETWORKED_PARTICLE_FX_LOOPED_ON_ENTITY ")
				
				sLocaldata.ptfxRepair = START_NETWORKED_PARTICLE_FX_LOOPED_ON_ENTITY_BONE("scr_vehicle_damage_smoke", VEH_ID(), <<0, -1.0, -0.4>>, <<0,0,0>>, 
																											GET_ENTITY_BONE_INDEX_BY_NAME(VEH_ID(), "overheat"), 3.0)
				
				// The effect is looping and Y-forward, has a 'damage' evolution you can set that goes from 0(white smoke) to 1(black smoke)
				SET_PARTICLE_FX_LOOPED_EVOLUTION(sLocaldata.ptfxRepair, "damage", 0.9) 
			ENDIF
		ENDIF
	ELSE
		CLEANUP_REPAIR_FX()
	ENDIF
ENDPROC

FUNC BOOL IS_VARIATION_SAFE_FOR_RADAR_JAM()
	IF bAIR_VARIATION()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CLEANUP_CONTEXT()
	IF iRadarJamContextIntention != (-1)
		PRINTLN("[EXEC1] [CTRB_SELL] [CONTEXT], CLEANUP_CONTEXT - Radar jam context intention released")
		RELEASE_CONTEXT_INTENTION(iRadarJamContextIntention)
	ENDIF
ENDPROC

PROC MAINTAIN_RADAR_JAM()
	IF IS_VARIATION_SAFE_FOR_RADAR_JAM()
		INT iVeh 
		
	
		IF IS_PLAYER_IN_ANY_SELL_VEH()
			IF IS_PASSENGER_OF_ANY_SELL_VEHICLE()
				iVeh = GET_SELL_VEH_I_AM_IN_INT()
				
				SWITCH iRadarJamStep
					CASE 0
						IF bDonePassengerRadarJamHelp = FALSE
							IF IS_BIT_SET(MPGlobalsAmbience.iFmGbHelpBitSet, BI_FM_GANG_BOSS_HELP_BLIP_WARN_SELL)
								IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
									PRINT_HELP("SCONTRA_HLP10")
									bDonePassengerRadarJamHelp = TRUE
								ENDIF
							ENDIF
						ELSE
							IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SCONTRA_HLP10")
								iRadarJamContextIntention = NEW_CONTEXT_INTENTION
								// Register context intention
								REGISTER_CONTEXT_INTENTION(iRadarJamContextIntention, CP_MEDIUM_PRIORITY, "SCONTRA_CTXT")
								PRINTLN("[EXEC1] [CTRB_SELL] [ST EXEC] [RJAM] MAINTAIN_RADAR_JAM, Step ", iRadarJamStep, " - Context intention registered, ", iRadarJamContextIntention)
								iRadarJamStep++
							ENDIF
						ENDIF
					BREAK
					CASE 1
						IF iRadarJamContextIntention != (-1)
							PRINTLN("radar jam spam - waiting for context to be triggered")
							IF HAS_CONTEXT_BUTTON_TRIGGERED(iRadarJamContextIntention)
								PRINTLN("[EXEC1] [CTRB_SELL] [ST EXEC] [RJAM] MAINTAIN_RADAR_JAM, Step ", iRadarJamStep, " - Context has triggered")
								CLEANUP_CONTEXT()
								iRadarJamStep++
							ENDIF
						ELSE
							PRINTLN("radar jam spam - iRadarJamContextIntention = (-1)")
						ENDIF
					BREAK
					CASE 2
						PRINTLN("radar jam spam - disabling lock on")
						SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerEventData
						TickerEventData.TickerEvent = TICKER_EVENT_RADAR_JAM_ACTIVATED
						
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[iVeh])
							IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niVeh[iVeh])
								PRINTLN("[EXEC1] [CTRB_SELL] [ST EXEC] [RJAM] MAINTAIN_RADAR_JAM, Step ", iRadarJamStep, " - Vehicle no longer allowed homing missile lockon, have control")
								SET_VEHICLE_ALLOW_HOMING_MISSLE_LOCKON_SYNCED(NET_TO_VEH(serverBD.niVeh[iVeh]), FALSE)
								BROADCAST_START_RADAR_JAM(ALL_PLAYERS_IN_VEHICLE(VEH_ID(iVeh)))
								BROADCAST_TICKER_EVENT(TickerEventData, ALL_PLAYERS_IN_VEHICLE(VEH_ID(iVeh), TRUE))
								iRadarJamStep++
							ELSE
								PRINTLN("[EXEC1] [CTRB_SELL] [ST EXEC] [RJAM] MAINTAIN_RADAR_JAM, Step ", iRadarJamStep, " - Broadcast to set vehicle no longer allowed homing missile lockon, dont have control")
								BROADCAST_SET_VEHICLE_AS_TARGETABLE_BY_HOMING_MISSILE(ALL_PLAYERS_ON_SCRIPT(TRUE), FALSE)
								BROADCAST_START_RADAR_JAM(ALL_PLAYERS_IN_VEHICLE(VEH_ID(iVeh)))
								BROADCAST_TICKER_EVENT(TickerEventData, ALL_PLAYERS_IN_VEHICLE(VEH_ID(iVeh), TRUE))
								iRadarJamStep++
							ENDIF
						ENDIF
					BREAK
					CASE 3
						PRINTLN("radar jam spam - waiting for 10s to bass and re-enable lock on")
						IF HAS_NET_TIMER_STARTED(sLocaldata.stRadarJamTimer)
							IF HAS_NET_TIMER_EXPIRED(sLocaldata.stRadarJamTimer, serverBD.iRadarJamDuration)
								IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niVeh[iVeh])
									PRINTLN("[EXEC1] [CTRB_SELL] [ST EXEC] [RJAM] MAINTAIN_RADAR_JAM, Step ", iRadarJamStep, " - Radar Jam ended, Vehicle allowed homing missile lockon, have control")
									SET_VEHICLE_ALLOW_HOMING_MISSLE_LOCKON_SYNCED(NET_TO_VEH(serverBD.niVeh[iVeh]), TRUE)
									iRadarJamStep++
								ELSE
									BROADCAST_SET_VEHICLE_AS_TARGETABLE_BY_HOMING_MISSILE(ALL_PLAYERS_ON_SCRIPT(TRUE), TRUE)
									PRINTLN("[EXEC1] [CTRB_SELL] [ST EXEC] [RJAM] MAINTAIN_RADAR_JAM, Step ", iRadarJamStep, " - Radar Jam ended, Broadcast to set vehicle allowed homing missile lockon, dont have control")
									iRadarJamStep++
								ENDIF
							ENDIF
						ENDIF
					BREAK
					CASE 4
						PRINTLN("radar jam spam - waiting for total time to pass")
						IF HAS_NET_TIMER_STARTED(sLocaldata.stRadarJamTimer)
							IF HAS_NET_TIMER_EXPIRED(sLocaldata.stRadarJamTimer, (serverBD.iRadarJamDuration + serverBD.iRadarJamRechargeDelay + serverBD.iRadarJamRechargeTime))
								BROADCAST_STOP_RADAR_JAM(ALL_PLAYERS_IN_VEHICLE(VEH_ID(iVeh)))
								PRINTLN("[EXEC1] [CTRB_SELL] [ST EXEC] [RJAM] MAINTAIN_RADAR_JAM, Step ", iRadarJamStep, " - Radar Jam timer has expired, requesting server timer reset")
								iRadarJamStep++
							ENDIF
						ENDIF
					BREAK
					CASE 5
						PRINTLN("radar jam spam - waiting for timer to be reset")
						IF NOT HAS_NET_TIMER_STARTED(sLocaldata.stRadarJamTimer)
							PRINTLN("[EXEC1] [CTRB_SELL] [ST EXEC] [RJAM] MAINTAIN_RADAR_JAM, Step ", iRadarJamStep, " - Radar Jam timer has been reset, safe to start again")
							iRadarJamStep = 0
						ENDIF
					BREAK
				ENDSWITCH
			ELSE
				CLEANUP_CONTEXT()
				
				IF bDonePilotRadarJamHelp = FALSE
					IF IS_BIT_SET(MPGlobalsAmbience.iFmGbHelpBitSet, BI_FM_GANG_BOSS_HELP_BLIP_WARN)
					AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
						PRINT_HELP("SCONTRA_HLP9")
						bDonePilotRadarJamHelp = TRUE
					ENDIF
				ENDIF
			ENDIF
		ELSE
			CLEANUP_CONTEXT()
		ENDIF
	ELSE
		CLEANUP_CONTEXT()
	ENDIF
ENDPROC

FUNC eLOCAL_BITSET_1 GET_CLEAR_INVINCIBILITY_LOCAL_BIT(INT iVeh)
	SWITCH iVeh
		CASE 0 RETURN eLOCALBITSET1_VEH_0_INVINCIBILITY_CLEARED
		CASE 1 RETURN eLOCALBITSET1_VEH_1_INVINCIBILITY_CLEARED
		CASE 2 RETURN eLOCALBITSET1_VEH_2_INVINCIBILITY_CLEARED
	ENDSWITCH
	
	RETURN eLOCALBITSET1_VEH_0_INVINCIBILITY_CLEARED
ENDFUNC

PROC CLIENT_SELL_VEH_LOOP()
	INT i

	REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[i])
			IF IS_SELL_VEH_OK(i)
				SET_VEHICLE_ALLOW_HOMING_MISSLE_LOCKON(NET_TO_VEH(serverBD.niVeh[i]), FALSE)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC MAINTAIN_VEHICLE_ACCESS()
	INT i

	REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
		// Sell Vehicle
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[i])
			IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG(TRUE)
			AND (GB_GET_LOCAL_PLAYER_GANG_BOSS() = serverBD.piLaunchBoss
			OR PLAYER_ID() = serverBD.piLaunchBoss)
				IF IS_SELL_VEH_OK(i)
					IF GET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(VEH_ID(i), PLAYER_ID())
						PRINTLN("2806496, MAINTAIN_VEHICLE_ACCESS, GET_VEHICLE_DOORS_LOCKED_FOR_PLAYER, UNLOCK ")
						SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(VEH_ID(i), PLAYER_ID(), FALSE)
					ENDIF
				ENDIF
			ELSE
				IF IS_SELL_VEH_OK(i)
					IF NOT GET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(VEH_ID(i), PLAYER_ID())
						PRINTLN("2806496, MAINTAIN_VEHICLE_ACCESS, GET_VEHICLE_DOORS_LOCKED_FOR_PLAYER, LOCK 1 ")
						SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(VEH_ID(i), PLAYER_ID(), TRUE)
					ENDIF
				ENDIF
			ENDIF
			
			// Check invincibility
			IF HAS_ANY_SELL_VEHICLE_BEEN_ENTERED()
				IF NOT IS_LOCAL_BIT1_SET(GET_CLEAR_INVINCIBILITY_LOCAL_BIT(i))
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niVeh[i])
					OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.niVeh[i]) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
						IF TAKE_CONTROL_OF_NET_ID(serverBD.niVeh[i])
							SET_ENTITY_INVINCIBLE(NET_TO_VEH(serverBD.niVeh[i]), FALSE)
							SET_LOCAL_BIT1(GET_CLEAR_INVINCIBILITY_LOCAL_BIT(i))
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Drop off vehicle
	i = 0
	REPEAT GET_NUM_DROP_OFF_VEH_FOR_VARIATION() i
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niDropOffVeh[i])
			IF IS_ENTITY_ALIVE(NET_TO_VEH(serverBD.niDropOffVeh[i]))
				IF NOT GET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(NET_TO_VEH(serverBD.niDropOffVeh[i]), PLAYER_ID())
					PRINTLN("2806496, MAINTAIN_VEHICLE_ACCESS, GET_VEHICLE_DOORS_LOCKED_FOR_PLAYER, LOCK 2 ")
					SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(NET_TO_VEH(serverBD.niDropOffVeh[i]), PLAYER_ID(), TRUE)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	i = 0
	IF bLAND_DEFEND()
		REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[i])
				IF IS_ENTITY_ALIVE(NET_TO_VEH(serverBD.niVeh[i]))
					IF HAS_THIS_SELL_VEH_DELIVERED_CONTRABAND(i)
						IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niVeh[i])
		        		OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.niVeh[i]) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
							IF TAKE_CONTROL_OF_NET_ID(serverBD.niVeh[i])
								INT iBitToCheck = ENUM_TO_INT(eLOCALBITSET1_LDEFEND_SET_INVINCIBLE0) + i
								IF NOT IS_LOCAL_BIT1_SET(INT_TO_ENUM(eLOCAL_BITSET_1, iBitToCheck))
									SET_LOCAL_BIT1(INT_TO_ENUM(eLOCAL_BITSET_1, iBitToCheck))
									SET_ENTITY_INVINCIBLE(NET_TO_VEH(serverBD.niVeh[i]), TRUE)
									PRINTLN("[EXEC1] [CTRB_SELL] SET DROP-OFF VEH INVINCIBLE ", i)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	IF DOES_VARIATION_HAVE_AI_VEHICLES()
		i=0
		REPEAT GET_NUM_ATTACK_VEHICLES_FOR_VARIATION() i
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPedVeh[i].netIndex)
				IF IS_ENTITY_ALIVE(NET_TO_ENT(serverBD.sPedVeh[i].netIndex))
					IF NOT GET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(NET_TO_VEH(serverBD.sPedVeh[i].netIndex), PLAYER_ID())
						PRINTLN("2806496, MAINTAIN_VEHICLE_ACCESS, GET_VEHICLE_DOORS_LOCKED_FOR_PLAYER, LOCK 3 ")
						SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(NET_TO_VEH(serverBD.sPedVeh[i].netIndex), PLAYER_ID(), TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	
	ENDIF
ENDPROC

PROC MAINTAIN_TRACKIFY_LOGIC()
	IF bLAND_TRACKIFY()
		IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_ENABLED_TRACKIFY_APP)
			IF HAS_ANY_SELL_VEHICLE_BEEN_ENTERED()
				ENABLE_MULTIPLAYER_TRACKIFY_APP(TRUE)
				SET_NUMBER_OF_MULTIPLE_TRACKIFY_TARGETS(1)
				VECTOR vDropOffCoords = GET_SELL_DROP_COORDS(GET_SELL_VAR(), serverBD.iClosestDropOff[0], serverBD.iRoute, 0)
				SET_TRACKIFY_MULTIPLE_TARGET_VECTOR(0, vDropOffCoords)
				
				PRINTLN("[EXEC1] [CTRB_SELL] [TRACKIFY], MAINTAIN_TRACKIFY_LOGIC - TRACKIFY HAS BEEN ENABLED")
				SET_LOCAL_BIT0(eLOCALBITSET0_ENABLED_TRACKIFY_APP)
			ENDIF
		ELSE
			IF IS_BIT_SET(MPGlobalsAmbience.iFmGbHelpBitSet, BI_FM_GANG_BOSS_HELP_BLIP_WARN)
				IF NOT IS_BIT_SET(sLocaldata.iHelpBitSet, ciHELP_BIT_HOW_TO_USE_TRACKIFY)
					IF IS_SAFE_FOR_HELP_TEXT()
					AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SCONT_GOHP")
						// "Trackify allows you to locate a target in the world.  Use the information it provides to track down the buyer. 
						// It can be launched from the bottom right of the phone app menu. "
						PRINT_HELP("SCONTRA_HLP12")
						SET_BIT(sLocaldata.iHelpBitSet, ciHELP_BIT_HOW_TO_USE_TRACKIFY)
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), eCLIENTBITSET0_LAND_TRACKIFY_SELL_VEH_NEAR_DROP_OFF)
				IF IS_ANY_SELL_VEH_OCCUPIED()
					IF IS_ANY_SELL_VEH_NEAR_DROP_OFF()
						PRINTLN("[EXEC1] [CTRB_SELL] [TRACKIFY], MAINTAIN_TRACKIFY_LOGIC - One of the sell vehicles is near the drop off, setting eCLIENTBITSET0_LAND_TRACKIFY_SELL_VEH_NEAR_DROP_OFF")
						SET_CLIENT_BIT0(eCLIENTBITSET0_LAND_TRACKIFY_SELL_VEH_NEAR_DROP_OFF)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CLOSE_TRACKIFY()
	IF bLAND_TRACKIFY()
		IF IS_LOCAL_BIT0_SET(eLOCALBITSET0_ENABLED_TRACKIFY_APP)
			IF IS_PHONE_ONSCREEN()
				HANG_UP_AND_PUT_AWAY_PHONE()
			ELSE
				PRINTLN("[EXEC1] [CTRB_SELL] [TRACKIFY], CLOSE_TRACKIFY - TRACKIFY HAS BEEN DISABLED")
				ENABLE_MULTIPLAYER_TRACKIFY_APP(FALSE)
				CLEAR_LOCAL_BIT0(eLOCALBITSET0_ENABLED_TRACKIFY_APP)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_MUSIC_AND_SOUNDS()
	IF NOT IS_BIT_SET(sLocaldata.iSoundBitSet, ciSOUND_BIT_TRIGGERED_5S)
		// Arbitrary 30s so that it doesnt randomly trigger as soon as it launches
		IF HAS_NET_TIMER_STARTED(serverBD.modeTimer)
			IF HAS_NET_TIMER_EXPIRED(serverBD.modeTimer, 30000)
				IF sLocalData.iTimeLeft > 0
				AND sLocalData.iTimeLeft <= 5000
					PLAY_SOUND_FRONTEND(-1, "5s", "MP_MISSION_COUNTDOWN_SOUNDSET", FALSE)
					SET_BIT(sLocaldata.iSoundBitSet, ciSOUND_BIT_TRIGGERED_5S)
					PRINTLN("[EXEC1] [CTRB_SELL] [SOUND], MAINTAIN_MUSIC_AND_SOUNDS - 5s countdown SFX triggered.")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_ADDITIONAL_HELP()
	IF NOT HAS_ANY_SELL_VEHICLE_BEEN_ENTERED()
		IF bSEA_VARIATION()
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[0])
				VECTOR coordsLeft = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(VEH_ID(0)), GET_ENTITY_HEADING(VEH_ID(0)), <<-5.200, -2.270, -0.660>>)
				VECTOR coordsRight = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(VEH_ID(0)), GET_ENTITY_HEADING(VEH_ID(0)), <<5.200, -2.270, -0.660>>)
				
				IF IS_ENTITY_IN_WATER(PLAYER_PED_ID())
				AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), coordsLeft) < 3.0
					OR GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), coordsRight) < 3.0
						IF NOT IS_BIT_SET(sLocaldata.iHelpBitSet, ciHELP_CLIMB_ABOARD)
							PRINT_HELP("SCONTRA_HLP14")
							SET_BIT(sLocaldata.iHelpBitSet, ciHELP_CLIMB_ABOARD)
						ENDIF
					ELSE
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SCONTRA_HLP14")
							CLEAR_HELP()
						ENDIF
						IF IS_BIT_SET(sLocaldata.iHelpBitSet, ciHELP_CLIMB_ABOARD)
							CLEAR_BIT(sLocaldata.iHelpBitSet, ciHELP_CLIMB_ABOARD)
						ENDIF
					ENDIF
				ELSE
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SCONTRA_HLP14")
						CLEAR_HELP()
					ENDIF
					IF IS_BIT_SET(sLocaldata.iHelpBitSet, ciHELP_CLIMB_ABOARD)
						CLEAR_BIT(sLocaldata.iHelpBitSet, ciHELP_CLIMB_ABOARD)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELIF NOT IS_BIT_SET(sLocaldata.iHelpBitSet, ciHELP_REMOVAL)
		SET_BIT(sLocaldata.iHelpBitSet, ciHELP_REMOVAL)
		GB_SET_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_DO_COMPLETE_SHIPMENT_HELP)
	ELSE
		IF bLAND_DEFEND()
			IF NOT IS_BIT_SET(sLocaldata.iHelpBitSet, ciHELP_LAND_DEFEND_ENEMY)
				IF serverBD.fNumWavesSpawned > 0
					IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE)
						PRINT_HELP("SCONTRA_HLPLD") // Take out the attacking enemies ~HUD_COLOUR_RED~~BLIP_TEMP_4~~s~ to protect the goods and complete the sale.
						SET_BIT(sLocaldata.iHelpBitSet, ciHELP_LAND_DEFEND_ENEMY)
						PRINTLN("[EXEC1] [CTRB_SELL] SET ciHELP_LAND_DEFEND_ENEMY")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_VARIATION_USE_CUSTOM_SPAWNS()
	IF bSEA_ATTACK()
	OR bSEA_DEFEND()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CLEANUP_CUSTOM_SPAWNS(BOOL bIgnoreBitSet = FALSE)
	IF IS_LOCAL_BIT0_SET(eLOCALBITSET0_CUSTOM_SPAWN_POINTS_ACTIVATED)
	OR bIgnoreBitSet
		IF NOT IS_PLAYER_RESPAWNING(PLAYER_ID())
			CLEAR_LOCAL_BIT0(eLOCALBITSET0_CUSTOM_SPAWN_POINTS_ACTIVATED)
			CLEAR_LOCAL_BIT0(eLOCALBITSET0_CUSTOM_SPAWN_POINTS_ADDED)
			CLEAR_CUSTOM_SPAWN_POINTS()
			SET_PLAYER_RESPAWN_IN_VEHICLE(FALSE)
			USE_CUSTOM_SPAWN_POINTS(FALSE)
			PRINTLN("[EXEC1] [CTRB_SELL], CLEANUP_CUSTOM_SPAWNS - called")
			PRINTLN("[EXEC1] [CTRB_SELL], CLEANUP_CUSTOM_SPAWNS - bIgnoreBitSet = ", GET_STRING_FROM_BOOL(bIgnoreBitSet))
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_SPAWN_LOCATIONS()
	IF SHOULD_VARIATION_USE_CUSTOM_SPAWNS()
		IF IS_SERVER_BIT0_SET(eSERVERBITSET0_VEHICLE_0_ENTERED_ONCE)
			// Wait for the sell vehicle to be closer to a spawn point than where it started from
			IF sLocaldata.iSpawnArea != -1
				// Only use these spawns if I'm within range of the sell vehicle when I die
				IF AM_I_WITHIN_RANGE_OF_SELL_VEH_FOR_CUSTOM_SPAWN(0)
					PRINTLN("[sell mission - sea variation - spawn spam] - Tug is currently closer spawn point ", sLocaldata.iSpawnArea, "; player is within range for spawn so should respawn on jetski")
					IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_CUSTOM_SPAWN_POINTS_ACTIVATED)
						USE_CUSTOM_SPAWN_POINTS(TRUE)
						PRINTLN("[EXEC1] [CTRB_SELL], MAINTAIN_SPAWN_LOCATIONS - [1] Player is within range of vehicle for spawn, setting USE_CUSTOM_SPAWN_POINTS")
						sLocaldata.bSpawnLocationChanged = TRUE
						SET_LOCAL_BIT0(eLOCALBITSET0_CUSTOM_SPAWN_POINTS_ACTIVATED)
					ELSE
						IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_CUSTOM_SPAWN_POINTS_ADDED)
						OR sLocaldata.bSpawnLocationChanged
							USE_CUSTOM_SPAWN_POINTS(TRUE)
							INT i
							REPEAT ciMAX_RESPAWN_POINTS_PER_AREA i
								ADD_CUSTOM_SPAWN_POINT(GET_CUSTOM_SPAWN_POSITION(i), 0.0)
							ENDREPEAT
							
							SET_PLAYER_RESPAWN_IN_VEHICLE(TRUE, SEASHARK, FALSE)
							sLocaldata.bSpawnLocationChanged = FALSE
							sLocaldata.iSpawnArea = GET_CLOSEST_SPAWN_AREA_TO_SELL_VEH()
							SET_LOCAL_BIT0(eLOCALBITSET0_CUSTOM_SPAWN_POINTS_ADDED)
							PRINTLN("[EXEC1] [CTRB_SELL], MAINTAIN_SPAWN_LOCATIONS - [1] Player is within range of vehicle for spawn, Custom spawn points have been added with ADD_CUSTOM_SPAWN_POINT")
						ELSE
							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[0])
								IF IS_ENTITY_ALIVE(NET_TO_VEH(serverBD.niVeh[0]))
									SET_PLAYER_WILL_SPAWN_FACING_COORDS(GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niVeh[0])), TRUE, FALSE)
								ENDIF
							ENDIF
						ENDIF
						
						IF sLocaldata.iSpawnArea != GET_CLOSEST_SPAWN_AREA_TO_SELL_VEH()
							sLocaldata.iSpawnArea = GET_CLOSEST_SPAWN_AREA_TO_SELL_VEH()
							CLEANUP_CUSTOM_SPAWNS()
							sLocaldata.bSpawnLocationChanged = TRUE
							PRINTLN("[EXEC1] [CTRB_SELL], MAINTAIN_SPAWN_LOCATIONS - [1] new spawn location: ", sLocaldata.iSpawnArea)
							PRINTLN("[EXEC1] [CTRB_SELL], MAINTAIN_SPAWN_LOCATIONS - [1] sLocaldata.bSpawnLocationChanged: ", GET_STRING_FROM_BOOL(sLocaldata.bSpawnLocationChanged))
						ENDIF
					ENDIF
				ELSE
					CLEANUP_CUSTOM_SPAWNS()
				ENDIF
			ELSE
				PRINTLN("[sell mission - sea variation - spawn spam] - Tug is currently closer to the start position than a spawn point")
			
				IF sLocaldata.iSpawnArea != GET_CLOSEST_SPAWN_AREA_TO_SELL_VEH()
					sLocaldata.iSpawnArea = GET_CLOSEST_SPAWN_AREA_TO_SELL_VEH()
					PRINTLN("[EXEC1] [CTRB_SELL], MAINTAIN_SPAWN_LOCATIONS - [2] new spawn location: ", sLocaldata.iSpawnArea)
				ENDIF
				
				CLEANUP_CUSTOM_SPAWNS()
			ENDIF
		ELSE
			// Only do this for the route where the boat spawns under the bridge in sea defend
			IF bSEA_DEFEND()
			AND serverBD.iRoute = 1
				IF AM_I_WITHIN_RANGE_OF_SELL_VEH_FOR_CUSTOM_SPAWN(0, TRUE)
					IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_CUSTOM_SPAWN_POINTS_ACTIVATED)
						USE_CUSTOM_SPAWN_POINTS(TRUE)
						PRINTLN("[EXEC1] [CTRB_SELL], MAINTAIN_SPAWN_LOCATIONS - [2] Player is within range of vehicle for spawn, setting USE_CUSTOM_SPAWN_POINTS")
						SET_LOCAL_BIT0(eLOCALBITSET0_CUSTOM_SPAWN_POINTS_ACTIVATED)
					ELSE
						IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_CUSTOM_SPAWN_POINTS_ADDED)
							ADD_CUSTOM_SPAWN_POINT(<<882.9380, -2548.9731, 27.4119>>, 175.2256)
							ADD_CUSTOM_SPAWN_POINT(<<890.0646, -2549.7996, 27.4180>>, 174.6591)
							ADD_CUSTOM_SPAWN_POINT(<<886.0687, -2549.9731, 27.4020>>, 175.2238)
							ADD_CUSTOM_SPAWN_POINT(<<878.1968, -2549.3467, 27.4200>>, 174.6580)
							SET_LOCAL_BIT0(eLOCALBITSET0_CUSTOM_SPAWN_POINTS_ADDED)
							PRINTLN("[EXEC1] [CTRB_SELL], MAINTAIN_SPAWN_LOCATIONS - [2] Player is within range of vehicle for spawn, Custom spawn points have been added with ADD_CUSTOM_SPAWN_POINT")
						ENDIF
					ENDIF
				ELSE
					CLEANUP_CUSTOM_SPAWNS()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

CONST_INT ciSTUCK_TIME_LIMIT 20000
CONST_INT ciSTUCK_TIME_LIMIT_SHORT 5000
CONST_INT iSTUCK_AFTER_CREATION_MAX_TIME 20000

PROC VEHICLE_STUCK_CHECKS()
	
	INT iStuckTimeToCheck = ciSTUCK_TIME_LIMIT

	
	IF bLAND_VARIATION()	
	OR bAIR_VARIATION()
	OR bSEA_VARIATION()
		INT i
		INT iClientBitToCheck
		eCLIENT_BITSET_0 clientBitToCheck
		BOOL bStuck
		REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
			iClientBitToCheck = ENUM_TO_INT(eCLIENTBITSET0_VEH0_STUCK) + i
			clientBitToCheck = INT_TO_ENUM(eCLIENT_BITSET_0, iClientBitToCheck)
			
			iStuckTimeToCheck = ciSTUCK_TIME_LIMIT
			
			IF bLAND_VARIATION()
				SWITCH i
					CASE 0
						IF NOT IS_SERVER_BIT0_SET(eSERVERBITSET0_VEHICLE_0_ENTERED_ONCE)	
							iStuckTimeToCheck = ciSTUCK_TIME_LIMIT_SHORT
						ENDIF
					BREAK
					
					CASE 1
						IF NOT IS_SERVER_BIT0_SET(eSERVERBITSET0_VEHICLE_1_ENTERED_ONCE)	
							iStuckTimeToCheck = ciSTUCK_TIME_LIMIT_SHORT
						ENDIF
					BREAK
					
					CASE 2
						IF NOT IS_SERVER_BIT0_SET(eSERVERBITSET0_VEHICLE_2_ENTERED_ONCE)	
							iStuckTimeToCheck = ciSTUCK_TIME_LIMIT_SHORT
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
			
			IF IS_SELL_VEH_OK(i)
				IF NOT IS_CLIENT_BIT0_SET(PARTICIPANT_ID(),clientBitToCheck )
					IF NOT IS_BIT_SET(serverBD.iVehStuckSinceCreatedBitset, i)
						IF NOT HAS_THIS_SELL_VEH_DELIVERED_CONTRABAND(i)
							IF NETWORK_HAS_CONTROL_OF_ENTITY(VEH_ID(i))
								IF IS_VEHICLE_STUCK_TIMER_UP(VEH_ID(i), VEH_STUCK_ON_ROOF, iStuckTimeToCheck)
									PRINTLN("[EXEC1] [CTRB_SELL] [VEHICLE_STUCK_CHECKS] Think Sell Veh, ", i, " is stuck! - VEH_STUCK_ON_ROOF")
									bStuck = TRUE
								ELIF IS_VEHICLE_STUCK_TIMER_UP(VEH_ID(i), VEH_STUCK_ON_SIDE, iStuckTimeToCheck)
									PRINTLN("[EXEC1] [CTRB_SELL] [VEHICLE_STUCK_CHECKS] Think Sell Veh, ", i, " is stuck! - VEH_STUCK_ON_SIDE")
									bStuck = TRUE
								ELIF IS_VEHICLE_STUCK_TIMER_UP(VEH_ID(i), VEH_STUCK_HUNG_UP, iStuckTimeToCheck)
									PRINTLN("[EXEC1] [CTRB_SELL] [VEHICLE_STUCK_CHECKS] Think Sell Veh, ", i, " is stuck! - VEH_STUCK_HUNG_UP")
									bStuck = TRUE
								ELIF IS_VEHICLE_STUCK_TIMER_UP(VEH_ID(i), VEH_STUCK_JAMMED, iStuckTimeToCheck)
									PRINTLN("[EXEC1] [CTRB_SELL] [VEHICLE_STUCK_CHECKS] Think Sell Veh, ", i, " is stuck! - VEH_STUCK_JAMMED")
									bStuck = TRUE
									
								ELIF iStuckTimeToCheck = ciSTUCK_TIME_LIMIT_SHORT
									//-- Additional check for the vehicle not being on all wheels shortly after spawning.
									IF NOT HAS_NET_TIMER_STARTED(serverBD.stVehicleStuckTimer[i])
										IF NOT IS_VEHICLE_ON_ALL_WHEELS(VEH_ID(i))
											START_NET_TIMER(serverBD.stVehicleStuckTimer[i])
											PRINTLN("[EXEC1] [CTRB_SELL] [VEHICLE_STUCK_CHECKS] [CREATE_SELL_VEH] Think Sell Veh, ", i, " is not on all wheels - start timer")
										ENDIF
									ELSE
										IF NOT IS_VEHICLE_ON_ALL_WHEELS(VEH_ID(i))
											IF HAS_NET_TIMER_EXPIRED(serverBD.stVehicleStuckTimer[i], iStuckTimeToCheck)
												PRINTLN("[EXEC1] [CTRB_SELL] [VEHICLE_STUCK_CHECKS] [CREATE_SELL_VEH] Think Sell Veh, ", i, " is stuck! - IS_VEHICLE_ON_ALL_WHEELS")
												bStuck = TRUE
											ENDIF
										ELSE
											PRINTLN("[EXEC1] [CTRB_SELL] [VEHICLE_STUCK_CHECKS] [CREATE_SELL_VEH] Think Sell Veh, ", i, " is on all wheels - reset timer")
											RESET_NET_TIMER(serverBD.stVehicleStuckTimer[i])
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF bStuck
						IF bAIR_VARIATION()
							PRINTLN("[EXEC1] [CTRB_SELL] [VEHICLE_STUCK_CHECKS], NETWORK_EXPLODE_VEHICLE ", i)
							NETWORK_EXPLODE_VEHICLE(VEH_ID(i))
						ELSE
							SET_CLIENT_BIT0(clientBitToCheck)
						ENDIF
					ENDIF
				ELSE
					IF IS_BIT_SET(serverBD.iVehStuckSinceCreatedBitset, i)
						CLEAR_CLIENT_BIT0(clientBitToCheck)
						PRINTLN("[EXEC1] [CTRB_SELL] [VEHICLE_STUCK_CHECKS] Clearing stuck check for Veh ", i, " as server is warping veh")
					ENDIF
				ENDIF
				
				
			ENDIF
		ENDREPEAT
	ENDIF
	
	
	INT iVehicle
	INT iNetId
	IF bLAND_DEFEND()
		REPEAT GET_NUM_ATTACK_VEHICLES_FOR_VARIATION() iVehicle

			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPedVeh[iVehicle].netIndex)
				iNetId = NETWORK_ENTITY_GET_OBJECT_ID(NET_TO_VEH(serverBD.sPedVeh[iVehicle].netIndex))
				IF sLocaldata.gangChaseNetId[iVehicle] != iNetId
					IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iGangChaseStuckBitset, iVehicle)
						CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iGangChaseStuckBitset, iVehicle)
						PRINTLN("[EXEC1] [CTRB_SELL] [VEHICLE_STUCK_CHECKS] Clear iGangChaseStuckBitset for veh ", iVehicle, " as net id updated")
					ENDIF
					
					sLocaldata.gangChaseNetId[iVehicle] = iNetId
				ENDIF
				
				IF NOT IS_BIT_SET(serverBD.iGangChaseStuckBitset, iVehicle)
					IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iGangChaseStuckBitset, iVehicle)			
					
						IF IS_NET_VEHICLE_DRIVEABLE(serverBD.sPedVeh[iVehicle].netIndex)
							IF IS_VEHICLE_STUCK_TIMER_UP(NET_TO_VEH(serverBD.sPedVeh[iVehicle].netIndex), VEH_STUCK_ON_ROOF, ciSTUCK_TIME_LIMIT)
								SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iGangChaseStuckBitset, iVehicle)
								PRINTLN("[EXEC1] [CTRB_SELL] [VEHICLE_STUCK_CHECKS] Think gang chase veh ", iVehicle, " is stuck - VEH_STUCK_ON_ROOF")
							ELIF IS_VEHICLE_STUCK_TIMER_UP(NET_TO_VEH(serverBD.sPedVeh[iVehicle].netIndex), VEH_STUCK_ON_SIDE, ciSTUCK_TIME_LIMIT)
								SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iGangChaseStuckBitset, iVehicle)
								PRINTLN("[EXEC1] [CTRB_SELL] [VEHICLE_STUCK_CHECKS] Think gang chase veh ", iVehicle, " is stuck - VEH_STUCK_ON_SIDE")
							ELIF IS_VEHICLE_STUCK_TIMER_UP(NET_TO_VEH(serverBD.sPedVeh[iVehicle].netIndex), VEH_STUCK_HUNG_UP, ciSTUCK_TIME_LIMIT)
								SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iGangChaseStuckBitset, iVehicle)
								PRINTLN("[EXEC1] [CTRB_SELL] [VEHICLE_STUCK_CHECKS] Think gang chase veh ", iVehicle, " is stuck - VEH_STUCK_HUNG_UP")
							ELIF IS_VEHICLE_STUCK_TIMER_UP(NET_TO_VEH(serverBD.sPedVeh[iVehicle].netIndex), VEH_STUCK_JAMMED, ciSTUCK_TIME_LIMIT)
								SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iGangChaseStuckBitset, iVehicle)
								PRINTLN("[EXEC1] [CTRB_SELL] [VEHICLE_STUCK_CHECKS] Think gang chase veh ", iVehicle, " is stuck - VEH_STUCK_JAMMED")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

FUNC BOOL ARE_ALL_ENEMIES_DEAD()
	INT iPed
	REPEAT GET_NUM_PEDS_FOR_VARIATION() iPed
		IF DOES_AI_PED_EXIST(iPed)
			IF NOT IS_ENTITY_DEAD(GET_AI_PED_PED_INDEX(iPed))
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN TRUE
ENDFUNC

PROC CLEAR_ANY_MISSION_HELP()

	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SCONTRA_CDROP")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SCONTRA_RP_H1")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SCONTRA_RP_H2")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SCONTRA_HLP11")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SCONTRA_HLP10")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SCONTRA_HLP9")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SCONTRA_HLP12")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SCONTRA_HLP8")
		CLEAR_HELP(TRUE)
	ENDIF	
ENDPROC

FUNC BOOL DOES_SELL_MODE_HAVE_SCORE()
	//IF GET_SELL_VAR() = eVAR_AIR_LOW
	IF GET_SELL_VAR() = eVAR_AIR_DROP
	OR GET_SELL_VAR() = eVAR_AIR_RESTRICTED
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

//Set any audio flags on mode start
PROC SET_AUDIO_FLAGS_ON_MUSIC_START()
	IF NOT IS_LOCAL_AUDIO_BIT0_SET(eSELL_AUDIO_TRIGGER_SET_UP_DONE)
		IF DOES_SELL_MODE_HAVE_SCORE()
			PRINTLN("[EXEC1] [SELL AUDIO] - [CTRB_SELL] SET_AUDIO_FLAGS_ON_MUSIC_START - SET_AUDIO_FLAG(\"DisableFlightMusic\", TRUE) ")
			PRINTLN("[EXEC1] [SELL AUDIO] - [CTRB_SELL] SET_AUDIO_FLAGS_ON_MUSIC_START - SET_AUDIO_FLAG(\"WantedMusicDisabled\", TRUE) ")
			SET_AUDIO_FLAG("DisableFlightMusic", TRUE) 
			SET_AUDIO_FLAG("WantedMusicDisabled", TRUE)
		ENDIF
		SET_LOCAL_AUDIO_BIT0(eSELL_AUDIO_TRIGGER_SET_UP_DONE)
	ENDIF
ENDPROC

//Set any audio triggers that are needed. 
PROC SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGERS eTrigger)
	
	//If we've not already set the audio flag then trigger what we need
	IF NOT IS_LOCAL_AUDIO_BIT0_SET(eTrigger)
		PRINTLN("[EXEC1] [SELL AUDIO] - [CTRB_SELL] SET_SELL_AUDIO_MUSIC_TRIGGERS - TRIGGER_MUSIC_EVENT(", GET_AUDIO_TRIGGER_DEBUG_STRING(eTrigger), ")")
		TRIGGER_MUSIC_EVENT(GET_AUDIO_TRIGGER_STRING(eTrigger))
		SET_LOCAL_AUDIO_BIT0(eTrigger)
	ENDIF
ENDPROC

FUNC eSELL_AUDIO_TRIGGERS GET_PASS_OR_FAIL_AUDIO_TRIGGER_FOR_VAR(BOOL bPassed)
	IF bPassed
		SWITCH GET_SELL_VAR()
			CASE eVAR_AIR_CLEAR_AREA		RETURN eSELL_AUDIO_TRIGGER_AIR_CLEAR_STOP
			CASE eVAR_SEA_ATTACK			RETURN eSELL_AUDIO_TRIGGER_SEA_ATTACK_STOP
			CASE eVAR_SEA_DEFEND			RETURN eSELL_AUDIO_TRIGGER_SEA_DEFEND_STOP
			CASE eVAR_STING					RETURN eSELL_AUDIO_TRIGGER_MP_MUSIC_STOP
			CASE eVAR_LAND_DAMAGE			RETURN eSELL_AUDIO_TRIGGER_MP_MUSIC_STOP
			CASE eVAR_LAND_TRACKIFY			RETURN eSELL_AUDIO_TRIGGER_MP_MUSIC_STOP 
			CASE eVAR_DEFAULT				RETURN eSELL_AUDIO_TRIGGER_MP_MUSIC_STOP
			CASE eVAR_AIR_ATTACK			RETURN eSELL_AUDIO_TRIGGER_AIRATTACK_STOP
			CASE eVAR_LAND_ATTACK			RETURN eSELL_AUDIO_TRIGGER_LAND_ATTACK_STOP
			CASE eVAR_LAND_DEFEND			RETURN eSELL_AUDIO_TRIGGER_LAND_DEFEND_STOP
			CASE eVAR_MULTIPLE				RETURN eSELL_AUDIO_TRIGGER_MP_MUSIC_STOP
			CASE eVAR_AIR_LOW				RETURN eSELL_AUDIO_TRIGGER_MP_MUSIC_STOP
			
		ENDSWITCH
	ELSE
		SWITCH GET_SELL_VAR()
			CASE eVAR_AIR_CLEAR_AREA		RETURN eSELL_AUDIO_TRIGGER_AIR_CLEAR_FAIL
			CASE eVAR_SEA_ATTACK			RETURN eSELL_AUDIO_TRIGGER_SEA_ATTACK_FAIL
			CASE eVAR_SEA_DEFEND			RETURN eSELL_AUDIO_TRIGGER_SEA_DEFEND_FAIL
			CASE eVAR_STING					RETURN eSELL_AUDIO_TRIGGER_STING_FAIL
			CASE eVAR_LAND_DAMAGE			RETURN eSELL_AUDIO_TRIGGER_MP_MUSIC_FAIL
			CASE eVAR_LAND_TRACKIFY			RETURN eSELL_AUDIO_TRIGGER_MP_MUSIC_FAIL
			CASE eVAR_DEFAULT				RETURN eSELL_AUDIO_TRIGGER_MP_MUSIC_FAIL
			CASE eVAR_AIR_ATTACK			RETURN eSELL_AUDIO_TRIGGER_AIRATTACK_FAIL
			CASE eVAR_LAND_ATTACK			RETURN eSELL_AUDIO_TRIGGER_MP_MUSIC_FAIL
			CASE eVAR_LAND_DEFEND			RETURN eSELL_AUDIO_TRIGGER_MP_MUSIC_FAIL
			CASE eVAR_MULTIPLE				RETURN eSELL_AUDIO_TRIGGER_MP_MUSIC_FAIL
			CASE eVAR_AIR_LOW				RETURN eSELL_AUDIO_TRIGGER_MP_MUSIC_STOP
		ENDSWITCH
	ENDIF
	RETURN eSELL_AUDIO_TRIGGER_AIR_CLEAR_STOP
ENDFUNC


//Clean up any audio on mode end
PROC CLEAR_AUDIO_FLAGS_ON_MODE_END()
	IF NOT IS_LOCAL_AUDIO_BIT0_SET(eSELL_AUDIO_TRIGGER_CLEARED_ON_MODE_END)
		IF DOES_SELL_MODE_HAVE_SCORE()
			PRINTLN("[EXEC1] [SELL AUDIO] - [CTRB_SELL] CLEAR_AUDIO_FLAGS_ON_MODE_END")
			IF IS_LOCAL_AUDIO_BIT0_SET(eSELL_AUDIO_TRIGGER_SET_UP_DONE)
				IF GET_END_REASON() = eENDREASON_WIN_CONDITION_TRIGGERED
					SET_SELL_AUDIO_MUSIC_TRIGGERS(GET_PASS_OR_FAIL_AUDIO_TRIGGER_FOR_VAR(TRUE))
				ELSE
					SET_SELL_AUDIO_MUSIC_TRIGGERS(GET_PASS_OR_FAIL_AUDIO_TRIGGER_FOR_VAR(FALSE))
				ENDIF
				PRINTLN("[EXEC1] [SELL AUDIO] - [CTRB_SELL]CLEAR_AUDIO_FLAGS_ON_MODE_END - SET_AUDIO_FLAG(\"DisableFlightMusic\", FALSE) eVAR_AIR_CLEAR_AREA")
				PRINTLN("[EXEC1] [SELL AUDIO] - [CTRB_SELL]CLEAR_AUDIO_FLAGS_ON_MODE_END - SET_AUDIO_FLAG(\"WantedMusicDisabled\", FALSE) eVAR_AIR_CLEAR_AREA")
				SET_AUDIO_FLAG("DisableFlightMusic", FALSE) 
				SET_AUDIO_FLAG("WantedMusicDisabled", FALSE)
			ENDIF
		ENDIF
		SET_LOCAL_AUDIO_BIT0(eSELL_AUDIO_TRIGGER_CLEARED_ON_MODE_END)
	ENDIF
ENDPROC

PROC TRIGGER_AUDIO_MUSIC_ON_WIN()
	IF DOES_SELL_MODE_HAVE_SCORE()
		PRINTLN("[EXEC1] [SELL AUDIO] - [CTRB_SELL] TRIGGER_AUDIO_MUSIC_ON_WIN")
		SWITCH GET_SELL_VAR()
			CASE eVAR_AIR_CLEAR_AREA		SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGER_AIR_CLEAR_STOP)	BREAK
			CASE eVAR_SEA_ATTACK			SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGER_SEA_ATTACK_STOP)	BREAK
			CASE eVAR_SEA_DEFEND			SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGER_SEA_DEFEND_STOP)	BREAK
			CASE eVAR_STING					SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGER_MP_MUSIC_STOP)	BREAK
			CASE eVAR_LAND_DAMAGE			SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGER_MP_MUSIC_STOP)	BREAK
			CASE eVAR_LAND_TRACKIFY			SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGER_MP_MUSIC_STOP)	BREAK
			CASE eVAR_DEFAULT				SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGER_MP_MUSIC_STOP)	BREAK
			CASE eVAR_AIR_ATTACK			SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGER_AIRATTACK_STOP)	BREAK
			CASE eVAR_LAND_ATTACK			SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGER_LAND_ATTACK_STOP)	BREAK
			CASE eVAR_LAND_DEFEND			SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGER_LAND_DEFEND_STOP)	BREAK
			CASE eVAR_MULTIPLE				SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGER_MP_MUSIC_STOP)	BREAK
			CASE eVAR_AIR_LOW				SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGER_MP_MUSIC_STOP)	BREAK
			DEFAULT
				SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGER_MP_MUSIC_STOP)
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

FUNC BOOL ARE_ALL_AIR_ATTACK_HELIS_DEAD()
	INT iVeh
	IF serverBD.fNumWavesSpawned >= GET_MAX_AMBUSH_WAVES_FOR_VARIATION()
		REPEAT GET_NUM_ATTACK_VEHICLES_FOR_VARIATION() iVeh
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPedVeh[iVeh].netIndex)
				RETURN FALSE
			ENDIF
		ENDREPEAT
	ELSE
		RETURN FALSE
	ENDIF
	RETURN TRUE	
ENDFUNC
FUNC BOOL ARE_ALL_ATTACK_PEDS_DEAD()
	INT iPed
	IF serverBD.fNumWavesSpawned >= GET_MAX_AMBUSH_WAVES_FOR_VARIATION()
		REPEAT GET_NUM_PEDS_FOR_VARIATION() iPed
			IF DOES_AI_PED_EXIST(iPed)
			AND NOT IS_PED_INJURED(GET_AI_PED_PED_INDEX(iPed))
				RETURN FALSE
			ENDIF
		ENDREPEAT
	ELSE
		RETURN FALSE
	ENDIF
	RETURN TRUE	
ENDFUNC

//this is called every frame
PROC MAINTAIN_AUDIO_MUSIC_TRIGGERS()
	SWITCH GET_SELL_VAR()
		CASE eVAR_AIR_LOW
			//Trigger as soon as the player enters the aeroplane
			IF IS_DRIVER_OF_ANY_SELL_VEH()
				SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGER_FLY_LOW_START)
			ENDIF
			
			//Trigger if the players become wanted, trigger this
			IF IS_LOCAL_BIT0_SET(eLOCALBITSET0_GIVEN_WANTED_FOR_TOO_HIGH)
				SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGER_FLY_LOW_WANTED)
				IF IS_LOCAL_AUDIO_BIT0_SET(eSELL_AUDIO_TRIGGER_FLY_LOW_FLYING)
					CLEAR_LOCAL_AUDIO_BIT0(eSELL_AUDIO_TRIGGER_FLY_LOW_FLYING)
				ENDIF
				SET_LOCAL_BIT0(eLOCALBITSET0_HAD_AIR_LOW_WANTED)
				
			//Trigger once the wanted level is lost and player goes back to delivering
			ELIF IS_LOCAL_BIT0_SET(eLOCALBITSET0_HAD_AIR_LOW_WANTED)
				SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGER_FLY_LOW_FLYING)
				IF IS_LOCAL_AUDIO_BIT0_SET(eSELL_AUDIO_TRIGGER_FLY_LOW_WANTED)
					CLEAR_LOCAL_AUDIO_BIT0(eSELL_AUDIO_TRIGGER_FLY_LOW_WANTED)
				ENDIF
			ENDIF		
		BREAK
		
		CASE eVAR_AIR_CLEAR_AREA
			//If the plane has landed and we're in the fight area trigger - AUDIO_TRIGGER_AIR_CLEAR_SHOOTOUT
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_SELL_DROP_COORDS(GET_SELL_VAR(), serverBD.iClosestDropOff[0], serverBD.iRoute, 0)) < cfSELL_CRATE_LOC_SIZE_Z
			AND NOT ARE_ALL_ATTACK_PEDS_DEAD()
				SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGER_AIR_CLEAR_SHOOTOUT)
				SET_LOCAL_BIT1(eLOCALBITSET1_BEEN_IN_AIR_CLEAR_AREA)
				IF IS_LOCAL_AUDIO_BIT0_SET(eSELL_AUDIO_TRIGGER_AIR_CLEAR_LEFT_AREA)
					CLEAR_LOCAL_AUDIO_BIT0(eSELL_AUDIO_TRIGGER_AIR_CLEAR_LEFT_AREA)
					PRINTLN("[EXEC1] [SELL AUDIO] - [CTRB_SELL] CLEAR_LOCAL_AUDIO_BIT0(eSELL_AUDIO_TRIGGER_AIR_CLEAR_LEFT_AREA) - GET_DISTANCE_BETWEEN_COORDS")
				ENDIF
			ELIF IS_LOCAL_BIT1_SET(eLOCALBITSET1_BEEN_IN_AIR_CLEAR_AREA)
			OR ARE_ALL_ATTACK_PEDS_DEAD()
				SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGER_AIR_CLEAR_LEFT_AREA)
				IF IS_LOCAL_AUDIO_BIT0_SET(eSELL_AUDIO_TRIGGER_AIR_CLEAR_SHOOTOUT)
					CLEAR_LOCAL_AUDIO_BIT0(eSELL_AUDIO_TRIGGER_AIR_CLEAR_SHOOTOUT)
					PRINTLN("[EXEC1] [SELL AUDIO] - [CTRB_SELL] CLEAR_LOCAL_AUDIO_BIT0(eSELL_AUDIO_TRIGGER_AIR_CLEAR_SHOOTOUT) - GET_DISTANCE_BETWEEN_COORDS NOT")
				ENDIF
			ENDIF
			
			INT iVeh
			iVeh = GET_CLOSEST_SELL_VEH_TO_LOCAL_PLAYER()
			IF iVeh <> -1
				IF IS_DRIVER_OF_SELL_VEH(iVeh)
					//If the plane has been told to land the trigger - AUDIO_TRIGGER_AIR_CLEAR_SHOOTOUT
					IF IS_LOCAL_BIT0_SET(eLOCALBITSET0_PRINT_LAND_HELP)
						SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGER_AIR_CLEAR_LAND)
					//If we are in the plane then trigger - AUDIO_TRIGGER_AIR_CLEAR_START
					ELSE
						SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGER_AIR_CLEAR_START)
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE eVAR_MULTIPLE
			IF IS_DRIVER_OF_ANY_SELL_VEH()
				SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGER_DELIVERING_START)
			ENDIF
		BREAK
			
		CASE eVAR_LAND_DEFEND
			//Trigger when player enters truck.
			IF IS_DRIVER_OF_ANY_SELL_VEH()
				SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGER_LAND_DEFEND_START)
			ENDIF
			//Trigger when player is told to ‘take out the enemies’
			IF serverBD.fNumWavesSpawned > 0
				SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGER_LAND_DEFEND_ENEMIES)
			ENDIF
		BREAK
		
		CASE eVAR_LAND_ATTACK
			//Trigger when player enters truck containing contraband.
			IF IS_DRIVER_OF_ANY_SELL_VEH()
				SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGER_LAND_ATTACK_START)
			ENDIF
			//Trigger when enemies appear.
			IF IS_SERVER_BIT0_SET(eSERVERBITSET0_CAN_LAND_GANG_CHASE_SPAWN)
				SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGER_LAND_ATTACK_HOSTILE)
			ENDIF
			//Trigger when enemies are killed/lost and no longer in pursuit.
			IF ARE_ALL_ATTACK_PEDS_DEAD()
				SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGER_LAND_ATTACK_LOST)
			ENDIF
		BREAK
		
		CASE eVAR_AIR_ATTACK
			//Trigger when the player gets in the plane
			IF IS_DRIVER_OF_ANY_SELL_VEH()
				SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGER_AIRATTACK_START)
			ENDIF
			//Trigger when enemy helicopter appears
			IF serverBD.iAirAttackHeliSpooked != 0
				SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGER_AIRATTACK_ENEMIES)
			ENDIF
			//Trigger once all enemy helicopters have been dealt with (lost or destroyed) and player is in the clear for the remainder of the mission
			IF NOT IS_LOCAL_AUDIO_BIT0_SET(eSELL_AUDIO_TRIGGER_AIRATTACK_DELIVERING)
				IF ARE_ALL_AIR_ATTACK_HELIS_DEAD()
					SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGER_AIRATTACK_DELIVERING)
				ENDIF 	
			ENDIF 	
		BREAK
		
		CASE eVAR_SEA_ATTACK
			IF IS_DRIVER_OF_ANY_SELL_VEH()
				SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGER_SEA_ATTACK_START)
			ENDIF
			IF HAS_FIRST_WAVE_SPAWNED()
				SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGER_SEA_ATTACK_FIGHT)
			ENDIF
		BREAK
		
		CASE eVAR_SEA_DEFEND
			IF HAVE_PEDS_BEEN_SPOOKED()
				IF HAVE_ALL_PEDS_BEEN_KILLED()
				OR (AM_I_IN_ANY_SELL_VEH()
				AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), GET_SEA_SELL_VEHICLE_CREATION_POS()) > 200)
					SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGER_SEA_DEFEND_DELIVER)
				ELSE
					IF DOES_ENTITY_EXIST(GET_NEAREST_PED_TO_PLAYER(TRUE))
						IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), GET_NEAREST_PED_TO_PLAYER(TRUE)) < 150
							SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGER_SEA_DEFEND_START)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE eVAR_STING
			//If we've give the player the scripted wanted
			IF sLocaldata.iWantedStored != -1
				//Trigger as soon as player loses the cops
				IF IS_LOCAL_BIT0_SET(eLOCALBITSET0_STOP_MAINTAINING_WANTED)
					SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGER_DELIVERING_SMA)
				//Trigger with the scripted wanted rating
				ELSE
					SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGER_STING_WANTED_SMA)
				ENDIF
			//Trigger when player gets in truck and mission begins
			ELIF IS_DRIVER_OF_ANY_SELL_VEH()
				SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGER_DELIVERING_START)
			ENDIF
		BREAK
		CASE eVAR_LAND_DAMAGE
		CASE eVAR_LAND_TRACKIFY
		CASE eVAR_DEFAULT
			//Trigger when player gets in truck and mission begins
			IF IS_DRIVER_OF_ANY_SELL_VEH()
				SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGER_DELIVERING_START)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC CLIENT_PROCESS_MISSION_LOGIC()
	
	INT iVeh

	MAINTAIN_WANTED_RATINGS()
	HANDLE_VEHICLE_STOPPING()
	HANDLE_LANDING_PLANE_FLAG()
	//CLIENT_HANDLE_BREAKDOWNS()
	MAINTAIN_RADAR_JAM()
	//CLIENT_HANDLE_FLY_LOW()
	MAINTAIN_PLANE_HEIGHT_CLIENT()
	VEHICLE_STUCK_CHECKS()
	MAINTAIN_AUDIO_MUSIC_TRIGGERS()
	MAINTAIN_DIALOGUE()
	DO_BOSS_INTRO_PHONECALL()
	SET_CONTRABAND_PLAYER()
	CLIENT_MANAGE_SMOKE_FROM_DROP_OFFS()	
	
	SWITCH GET_LOCAL_CLIENT_MISSION_STAGE()
		
		CASE eSELL_STAGE_INIT
			DO_BIG_MESSAGE( eBIGM_GANG_START)
			SET_LOCAL_CLIENT_MISSION_STAGE(eSELL_STAGE_VEH)
		BREAK
	
		CASE eSELL_STAGE_VEH
			IF IS_ANY_SELL_VEH_OCCUPIED()
			OR HAS_ALL_CONTRABAND_BEEN_DROPPED_OFF()
				SET_LOCAL_CLIENT_MISSION_STAGE(eSELL_STAGE_SELLING)
			ENDIF
		BREAK
		CASE eSELL_STAGE_SELLING
			IF HAVE_ALL_AIR_CLEAR_AREA_PLANES_LANDED()
				IF ARE_ALL_ENEMIES_DEAD()
					INT i
					REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
						EJECT_PLAYER_FROM_VEHICLE(i)
						LOCK_SELL_VEH(i, TRUE)
					ENDREPEAT
					SET_LOCAL_CLIENT_MISSION_STAGE(eSELL_STAGE_CLEANUP)
					SET_SELL_AUDIO_MUSIC_TRIGGERS(eSELL_AUDIO_TRIGGER_AIR_CLEAR_STOP)
				ENDIF
			ELIF IS_ANY_SELL_VEH_OCCUPIED()
			OR HAS_ALL_CONTRABAND_BEEN_DROPPED_OFF()			
				IF HAS_ALL_CONTRABAND_BEEN_DROPPED_OFF()
					IF bAIR_RESTRICTED()
					AND NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_WANTED_DELAY_CLEARED)
						IF NOT HAS_NET_TIMER_STARTED(sLocaldata.stWantedDelay)
							START_NET_TIMER(sLocaldata.stWantedDelay)
							PRINTLN("[EXEC1] [CTRB_SELL] [CRT]  - START_NET_TIMER(sLocaldata.stWantedDelay)") 
							CLEAR_ANY_MISSION_HELP()	
						ELIF HAS_NET_TIMER_EXPIRED(sLocaldata.stWantedDelay, 1000)
						AND GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
							SET_LOCAL_BIT0(eLOCALBITSET0_WANTED_DELAY_CLEARED)
							PRINTLN("[EXEC1] [CTRB_SELL] [CRT]  - HAS_NET_TIMER_EXPIRED(sLocaldata.stWantedDelay) - SET_LOCAL_BIT0(eLOCALBITSET0_WANTED_DELAY_CLEARED)") 
						ENDIF
					ELSE
						IF NOT bLAND_DEFEND()
							IF NOT bAIR_VARIATION()
							AND NOT bSEA_VARIATION()
								INT i
								REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
									EJECT_PLAYER_FROM_VEHICLE(i)
									LOCK_SELL_VEH(i, TRUE)
								ENDREPEAT
							ENDIF
							
							TRIGGER_AUDIO_MUSIC_ON_WIN()
							SET_LOCAL_CLIENT_MISSION_STAGE(eSELL_STAGE_CLEANUP)
						ELSE
							//-- Land Defend variation, enemies don't arrive until drop-off has been made
							IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_EXIT_CONTRA_VEH_FOR_DEFEND)
								INT i
								REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
									EJECT_PLAYER_FROM_VEHICLE(i)
									LOCK_SELL_VEH(i, TRUE)
								ENDREPEAT
								SET_LOCAL_BIT0(eLOCALBITSET0_EXIT_CONTRA_VEH_FOR_DEFEND)
							ENDIF
							
							IF serverBD.fNumWavesSpawned >= GET_MAX_AMBUSH_WAVES_FOR_VARIATION()
								IF ARE_ALL_ATTACKING_PEDS_DEAD()
									TRIGGER_AUDIO_MUSIC_ON_WIN()
									SET_LOCAL_CLIENT_MISSION_STAGE(eSELL_STAGE_CLEANUP)
								ENDIF
							ENDIF
								
						ENDIF
					ENDIF
				ELSE
					IF NOT bAIR_VARIATION()
					AND NOT bSEA_VARIATION()
						IF IS_PLAYER_IN_ANY_SELL_VEH(FALSE)
							iVeh = GET_SELL_VEH_I_AM_IN_INT()
							IF HAS_THIS_SELL_VEH_DELIVERED_CONTRABAND(iVeh)
								EJECT_PLAYER_FROM_VEHICLE(iVeh)
								LOCK_SELL_VEH(iVeh, TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				SET_LOCAL_CLIENT_MISSION_STAGE(eSELL_STAGE_VEH)
			ENDIF
		BREAK
		CASE eSELL_STAGE_CLEANUP
			GB_CLEAR_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_NOTIFICATION_BLIP_WARN)
		BREAK
	ENDSWITCH
ENDPROC

//╚═════════════════════════════════════════════════════════════════════════════╝

#IF IS_DEBUG_BUILD

PROC MAINTAIN_J_SKIPS()
	
	IF NOT IS_CLIENT_DEBUG_BIT0_SET(PARTICIPANT_ID(), eCLIENTDEBUGBITSET0_I_HAVE_S_PASSED)
		IF IS_DEBUG_KEY_JUST_RELEASED(KEY_S, KEYBOARD_MODIFIER_NONE, "S Pass")
			PRINTLN("[EXEC1] [CTRB_SELL] KEY_S, KEYBOARD_MODIFIER_NONE just released")
			SCRIPT_EVENT_DATA_TICKER_MESSAGE debugTicker
			debugTicker.TickerEvent = TICKER_EVENT_PLAYER_CHEATED_SPASS
			BROADCAST_TICKER_EVENT(debugTicker, ALL_PLAYERS_ON_SCRIPT())
			SET_CLIENT_DEBUG_BIT0(eCLIENTDEBUGBITSET0_I_HAVE_S_PASSED)
		ENDIF
	ENDIF
	
	IF NOT IS_CLIENT_DEBUG_BIT0_SET(PARTICIPANT_ID(), eCLIENTDEBUGBITSET0_I_HAVE_F_FAILED)
		IF IS_DEBUG_KEY_JUST_RELEASED(KEY_F, KEYBOARD_MODIFIER_NONE, "F Fail")
			PRINTLN("[EXEC1] [CTRB_SELL] KEY_F, KEYBOARD_MODIFIER_NONE just released")
			SCRIPT_EVENT_DATA_TICKER_MESSAGE debugTicker
			debugTicker.TickerEvent = TICKER_EVENT_PLAYER_CHEATED_FFAIL
			BROADCAST_TICKER_EVENT(debugTicker, ALL_PLAYERS_ON_SCRIPT())
			SET_CLIENT_DEBUG_BIT0(eCLIENTDEBUGBITSET0_I_HAVE_F_FAILED)
		ENDIF
	ENDIF
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF serverBD.iSPassPart > (-1)
			PRINTLN("[EXEC1] [CTRB_SELL] [SERVER] serverBD.iSPassPart > (-1)")
			IF GET_MODE_STATE() < eMODESTATE_REWARDS
				SET_END_REASON(eENDREASON_WIN_CONDITION_TRIGGERED)
				SET_MODE_STATE(eMODESTATE_REWARDS)
			ENDIF
		ELIF serverBD.iFFailPart > (-1)
			PRINTLN("[EXEC1] [CTRB_SELL] [SERVER] serverBD.iFFailPart > (-1)")
			IF GET_MODE_STATE() < eMODESTATE_REWARDS
				SET_END_REASON(eENDREASON_TIME_UP)
				SET_MODE_STATE(eMODESTATE_REWARDS)
			ENDIF
		ENDIF
	ENDIF
	
	// J skip to vehicle
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_J, KEYBOARD_MODIFIER_NONE, "")
		INT iVeh = GET_CLOSEST_SELL_VEH_TO_LOCAL_PLAYER(FALSE)
		
		IF iVeh != -1
			IF bAIR_CLEAR_AREA()
			AND NOT ARE_ALL_ATTACKING_PEDS_DEAD()
			AND	GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_SELL_DROP_COORDS(GET_SELL_VAR(), serverBD.iClosestDropOff[0], serverBD.iRoute, 0)) < (cfSELL_CRATE_LOC_SIZE_Z/2)
							
				INT iPed
				PED_INDEX pedId
				REPEAT GET_NUM_PEDS_FOR_VARIATION() iPed
					IF serverBD.sPed[iPed].eState != ePEDSTATE_DEAD
						IF DOES_AI_PED_EXIST(iPed)
							pedId = NET_TO_PED(serverBD.sPed[iPed].netIndex)
							IF NOT IS_ENTITY_DEAD(pedId)
								IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netIndex)
			            		OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.sPed[iPed].netIndex) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
									IF TAKE_CONTROL_OF_NET_ID(serverBD.sPed[iPed].netIndex)
										SET_ENTITY_HEALTH(pedId, 0)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
	
			ELIF IS_DRIVER_OF_SELL_VEH(iVeh)
	//			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
	//			//AND bLAND_BREAKDOWN() OR bAIR_FAILURE()
	//			AND NOT bVEH_BROKEN_DOWN()
	//			AND serverBD.iBreakdownCount < ciMAX_BREAKDOWNS
	//				SERVER_SET_BREAKDOWN(#IF IS_DEBUG_BUILD FALSE #ENDIF)
	//			ELSE
					SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), GET_SELL_DROP_COORDS(GET_SELL_VAR(), serverBD.iClosestDropOff[iVeh], serverBD.iRoute, iVeh))
					SET_ENTITY_HEADING(VEH_ENT(iVeh), 355.6729)
	//			ENDIF
			ELSE
				IF bAIR_VARIATION()
				AND NOT IS_PLAYER_IN_AIRPORT()
					SET_ENTITY_COORDS(PLAYER_PED_ID(), vAIRPORT_COORDS())
				ELSE
					IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), VEH_ENT(iVeh), <<7.0, 7.0, 7.0>>)
						IF IS_VEHICLE_SEAT_FREE(VEH_ID(iVeh), VS_DRIVER)
							TASK_ENTER_VEHICLE(PLAYER_PED_ID(), VEH_ID(iVeh), -1, VS_DRIVER, PEDMOVEBLENDRATIO_RUN, ECF_WARP_PED)
						ELSE
							IF IS_VEHICLE_SEAT_FREE(VEH_ID(iVeh), VS_FRONT_RIGHT)
								TASK_ENTER_VEHICLE(PLAYER_PED_ID(), VEH_ID(iVeh), -1, VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_RUN, ECF_WARP_PED)
							ELSE
								SET_ENTITY_COORDS(PLAYER_PED_ID(), VEH_POS(iVeh, TRUE))
							ENDIF
						ENDIF
					ELSE
						SET_ENTITY_COORDS(PLAYER_PED_ID(), VEH_POS(iVeh, TRUE))
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	
//	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_DOWN)
//		INT iVeh = 0
//		REPEAT ciMAX_CONTRABAND_PED_VEHICLES iVeh
//			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPedVeh[iVeh].netIndex)
//				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sPedVeh[iVeh].netIndex)
//					IF IS_NET_VEHICLE_DRIVEABLE(serverBD.sPedVeh[iVeh].netIndex)
//				//		NETWORK_EXPLODE_HELI(NET_TO_VEH(serverBD.sPedVeh[0].netIndex))
//						SET_ENTITY_ROTATION(NET_TO_VEH(serverBD.sPedVeh[iVeh].netIndex), << 0.0, 180.0, 0.0>> )
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDREPEAT
//	ENDIF
ENDPROC
#ENDIF

// Fail if the plane has landed outside the drop-off after having engine failure
//PROC CLIENT_TRACK_GROUNDED_PLANE()
//	IF bROUTE_DIVERTED()
//		IF iDROPPED_OFF_COUNT() = 0
//			IF IS_DRIVER_OF_SELL_VEH()
//				IF NOT IS_ENTITY_IN_AIR(VEH_ENT())
//					IF NOT IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), eCLIENTBITSET0_VEH_GROUNDED)
//						PRINTLN("[EXEC1] [CTRB_SELL] CLIENT_TRACK_GROUNDED_PLANE, eCLIENTBITSET0_VEH_GROUNDED, SET_CLIENT_BIT0  ")
//						SET_CLIENT_BIT0(eCLIENTBITSET0_VEH_GROUNDED)
//					ENDIF
//				ELSE
//					IF IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), eCLIENTBITSET0_VEH_GROUNDED)
//						PRINTLN("[EXEC1] [CTRB_SELL] CLIENT_TRACK_GROUNDED_PLANE, eCLIENTBITSET0_VEH_GROUNDED, CLEAR_CLIENT_BIT0  ")
//						CLEAR_CLIENT_BIT0(eCLIENTBITSET0_VEH_GROUNDED)
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
//ENDPROC

FUNC BOOL IS_PLAYER_WITHIN_RANGE_OF_DROP_OFF_FOR_WARP()
	
	// only done for sea variation so ok to pass 0 as only one vehicle
	VECTOR vDropOff = GET_SELL_DROP_COORDS(GET_SELL_VAR(), serverBD.iClosestDropOff[0], serverBD.iRoute, 0)
	
	IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vDropOff, FALSE) <= 100
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_PLAYER_WARP()

	IF IS_LOCAL_BIT0_SET(eLOCALBITSET0_END_WARP_DONE)
		PRINTLN("contra sell warp spam - player should NOT warp - IS_LOCAL_BIT0_SET(eLOCALBITSET0_END_WARP_DONE)")
		RETURN FALSE
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(sLocalData.stWarpAllowanceTimer)
		IF HAS_NET_TIMER_EXPIRED(sLocalData.stWarpAllowanceTimer, 2000)
			PRINTLN("contra sell warp spam - player should NOT warp - warp allowance timer expired")
			RETURN FALSE
		ENDIF
	ENDIF
	

	IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
		PRINTLN("contra sell warp spam - player should NOT warp - player is not okay")
		RETURN FALSE
	ENDIF

	IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		RETURN TRUE
	ENDIF
	
	INT i
	REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
		IF DOES_ENTITY_EXIST(VEH_ID(i))
			IF IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(), VEH_ID(i), TRUE)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	PRINTLN("contra sell warp spam - player should NOT warp - player is in a vehicle")
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_PLAYER_WARPING()
	IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	IF IS_LOCAL_BIT0_SET(eLOCALBITSET0_END_WARP_DONE)
		RETURN FALSE
	ENDIF

	IF sLocaldata.iWarpStage > 0 AND sLocaldata.iWarpStage < 5
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CLEANUP_CONTRABAND_DECORATOR()
	INT i
	REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[i])
			
			IF DOES_ENTITY_EXIST(VEH_ENT(i))
				IF NETWORK_HAS_CONTROL_OF_ENTITY(VEH_ENT(i))
					IF NOT IS_ENTITY_DEAD(VEH_ENT(i))
						GB_CLEAR_VEHICLE_AS_CONTRABAND(VEH_ID(i))
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT

ENDPROC

FUNC BOOL IS_SAFE_TO_SHOW_REWARDS()
	IF bSEA_VARIATION()
		IF SHOULD_PLAYER_WARP()
		OR IS_PLAYER_WARPING()
			IF IS_PLAYER_WITHIN_RANGE_OF_DROP_OFF_FOR_WARP()
			OR IS_PLAYER_WARPING()
				SWITCH sLocaldata.iWarpStage
					CASE 0
						IF NOT IS_SCREEN_FADED_OUT()
							CLEANUP_CUSTOM_SPAWNS(TRUE)
							SETUP_SPECIFIC_SPAWN_LOCATION(serverBD.vSeaVariationEndWarpLocation, 0.0, 100.0, TRUE, 0, FALSE, FALSE, 65, TRUE, TRUE)
							DO_SCREEN_FADE_OUT(500)
							PRINTLN("[EXEC1] [CTRB_SELL] [WARP] [Step ", sLocaldata.iWarpStage, "] - IS_SAFE_TO_SHOW_REWARDS - Fading screen out")
							sLocaldata.iWarpStage++
						ENDIF
					BREAK
					
					CASE 1
						IF IS_SCREEN_FADED_OUT()
							PRINTLN("[EXEC1] [CTRB_SELL] [WARP] [Step ", sLocaldata.iWarpStage, "] - IS_SAFE_TO_SHOW_REWARDS - Screen is fully faded")
							sLocaldata.iWarpStage++
						ENDIF
					BREAK
					
					CASE 2
						IF WARP_TO_SPAWN_LOCATION(SPAWN_LOCATION_NEAR_SPECIFIC_COORDS, FALSE)
							PRINTLN("[EXEC1] [CTRB_SELL] [WARP] [Step ", sLocaldata.iWarpStage, "] - IS_SAFE_TO_SHOW_REWARDS - Player has been warped to shore")
							sLocaldata.iWarpStage++
						ENDIF
					BREAK
					
					CASE 3
						IF NOT IS_SCREEN_FADED_IN()
							DO_SCREEN_FADE_IN(500)
							PRINTLN("[EXEC1] [CTRB_SELL] [WARP] [Step ", sLocaldata.iWarpStage, "] - IS_SAFE_TO_SHOW_REWARDS - Screen is still faded out, fading in")
							sLocaldata.iWarpStage++
						ELSE
							PRINTLN("[EXEC1] [CTRB_SELL] [WARP] [Step ", sLocaldata.iWarpStage, "] - IS_SAFE_TO_SHOW_REWARDS - Screen is faded in, moving to next step")
							sLocaldata.iWarpStage++
						ENDIF
					BREAK
					
					CASE 4
						IF IS_SCREEN_FADED_IN()
							PRINTLN("[EXEC1] [CTRB_SELL] [WARP] [Step ", sLocaldata.iWarpStage, "] - IS_SAFE_TO_SHOW_REWARDS - Screen is faded in, returning true")
							CLEAR_SPECIFIC_SPAWN_LOCATION()
							SET_LOCAL_BIT0(eLOCALBITSET0_END_WARP_DONE)
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH
			ELSE
				RETURN TRUE		
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF

	RETURN FALSE		// CHANGE TO FALSE
ENDFUNC

// [NOTE FOR BOBBY] - Wait until the timer has expired before we set the vehicles as containing contraband.
// 						This timer is started once one of the vehicles have been entered

PROC SEND_CONTRABAND_VISIBLE_TICKER()
	SCRIPT_EVENT_DATA_TICKER_MESSAGE sData
	
	sData.TickerEvent = TICKER_EVENT_CONTRABAND_VISIBLE
	sData.playerID = PLAYER_ID()
	sData.playerID2 = GB_GET_LOCAL_PLAYER_GANG_BOSS()
	
	BROADCAST_TICKER_EVENT(sData, ALL_PLAYERS_ON_SCRIPT(TRUE))
ENDPROC

PROC MAINTAIN_CONTRABAND_BLIP_DELAY()
	IF SHOULD_DELAY_CONTRABAND_BLIP_FOR_BUYER()
		IF NOT IS_SERVER_BIT1_SET(eSERVERBITSET1_VEHICLES_MARKED_AS_CONTRABAND)
			IF HAS_NET_TIMER_STARTED(serverBD.stBlipDelayTimer)
			AND HAS_NET_TIMER_EXPIRED(serverBD.stBlipDelayTimer, GET_CONTRABAND_BLIP_DELAY_TIME_FOR_BUYER())
				INT i
				REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
					GB_SET_VEHICLE_AS_CONTRABAND(VEH_ID(i), i)
				ENDREPEAT
				
				SEND_CONTRABAND_VISIBLE_TICKER()
				
				SET_SERVER_BIT1(eSERVERBITSET1_VEHICLES_MARKED_AS_CONTRABAND)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_GLOBAL_REVEAL_SOUNDS()
	IF HAS_NET_TIMER_STARTED(serverBD.stBlipDelayTimer)
		IF NOT IS_LOCAL_BIT1_SET(eLOCALBITSET1_PLAYED_FIVE_SECOND_GLOBAL_TIMER)
			IF HAS_NET_TIMER_EXPIRED(serverBD.stBlipDelayTimer, (GET_CONTRABAND_BLIP_DELAY_TIME_FOR_BUYER()-5000 ))
				PRINTLN("[EXEC1] - contraband sell timer has five seconds left. BLEEP BLEEP BLEEP")
				PLAY_SOUND_FRONTEND(-1, "5s", "MP_MISSION_COUNTDOWN_SOUNDSET", FALSE)
				SET_LOCAL_BIT1(eLOCALBITSET1_PLAYED_FIVE_SECOND_GLOBAL_TIMER)
			ENDIF
		ENDIF
		
		IF NOT IS_LOCAL_BIT1_SET(eLOCALBITSET1_PLAYED_GLOBAL_BLIP)
			IF HAS_NET_TIMER_EXPIRED(serverBD.stBlipDelayTimer, GET_CONTRABAND_BLIP_DELAY_TIME_FOR_BUYER())
				PRINTLN("[EXEC1] - contraband sell timer is done. BEEEEEP")
				PLAY_SOUND_FRONTEND(-1, "Crates_Blipped", "GTAO_Magnate_Boss_Modes_Soundset", FALSE)
				SET_LOCAL_BIT1(eLOCALBITSET1_PLAYED_GLOBAL_BLIP)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

// [NOTE FOR BOBBY] - Need to hold off storing contraband vehicle coords for Martin until we say they should be blipped
//						Dont bother delaying if we dont need to

FUNC BOOL IS_SAFE_TO_MAINTAIN_ENTRY_TIMERS()
	IF SHOULD_DELAY_CONTRABAND_BLIP_FOR_BUYER()
		RETURN IS_SERVER_BIT1_SET(eSERVERBITSET1_VEHICLES_MARKED_AS_CONTRABAND)
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC MAINTAIN_VEHICLE_ENTRY_TIMERS()
	BOOL bDelivered
	IF IS_SAFE_TO_MAINTAIN_ENTRY_TIMERS()
		IF IS_SERVER_BIT0_SET(eSERVERBITSET0_VEHICLE_0_ENTERED_ONCE)
			bDelivered = HAS_THIS_SELL_VEH_DELIVERED_CONTRABAND(0)
			STORE_CONTRABAND_VEHICLE_COORDS(VEH_ID(0),vehicleCoordTimer[0],0, bDelivered)
		ENDIF
		
		IF IS_SERVER_BIT0_SET(eSERVERBITSET0_VEHICLE_1_ENTERED_ONCE)
			bDelivered = HAS_THIS_SELL_VEH_DELIVERED_CONTRABAND(1)
			STORE_CONTRABAND_VEHICLE_COORDS(VEH_ID(1),vehicleCoordTimer[1],1, bDelivered)
		ENDIF
		
		IF IS_SERVER_BIT0_SET(eSERVERBITSET0_VEHICLE_2_ENTERED_ONCE)
			bDelivered = HAS_THIS_SELL_VEH_DELIVERED_CONTRABAND(2)
			STORE_CONTRABAND_VEHICLE_COORDS(VEH_ID(2),vehicleCoordTimer[2],2, bDelivered)
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_GHOST_ORG_HELP()
	IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_DONE_ORG_HELP)
		GB_SET_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_NOTIFICATION_BLIP_WARN) 
		IF HAS_ANY_SELL_VEHICLE_BEEN_ENTERED()
		AND IS_SAFE_TO_MAINTAIN_ENTRY_TIMERS()
	 		IF GB_TRIGGER_GHOST_ORG_HELP_TEXT_FOR_CONTRABAND_MISSIONS()
				PRINTLN("[EXEC1] GB_TRIGGER_GHOST_ORG_HELP_TEXT_FOR_CONTRABAND_MISSIONS - DONE")
				SET_LOCAL_BIT0(eLOCALBITSET0_DONE_ORG_HELP)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC HANDLE_END_OF_MODE_CHECKS()
	IF bSEA_VARIATION()
		IF IS_LOCAL_BIT0_SET(eLOCALBITSET0_END_WARP_DONE)
			IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
			AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
				SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
			ENDIF
		ENDIF
			
		IF SHOULD_PLAYER_WARP()
		OR IS_PLAYER_WARPING()
			IF NOT HAS_NET_TIMER_STARTED(sLocaldata.stEndOfMissionInvincibility)
				IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_END_WARP_DONE)
					IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
					AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
						SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
					ENDIF
				ENDIF
			ELSE
				IF HAS_NET_TIMER_EXPIRED(sLocaldata.stEndOfMissionInvincibility, 5000)
				OR IS_LOCAL_BIT0_SET(eLOCALBITSET0_END_WARP_DONE)
					IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
					AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
						SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF HAS_NET_TIMER_EXPIRED(sLocaldata.stEndOfMissionInvincibility, 5000)
			OR IS_LOCAL_BIT0_SET(eLOCALBITSET0_END_WARP_DONE)
				IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
				AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
					SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC HELP_TEXT_CLEANUP()
	IF GB_IS_LOCAL_HELP_BIT_SET(eGB_LOCAL_HELP_BITSET_NOTIFICATION_AIR_DROP)
		GB_CLEAR_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_NOTIFICATION_AIR_DROP) 
	ENDIF
	IF GB_IS_LOCAL_HELP_BIT_SET(eGB_LOCAL_HELP_BITSET_NOTIFICATION_HIDE_CONTRABAND_PLAYER)
		GB_CLEAR_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_NOTIFICATION_HIDE_CONTRABAND_PLAYER)
	ENDIF
	IF GB_IS_LOCAL_HELP_BIT_SET(eGB_LOCAL_HELP_BITSET_NOTIFICATION_HIDE_CONTRABAND_PLAYER_P)
		GB_CLEAR_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_NOTIFICATION_HIDE_CONTRABAND_PLAYER_P)
	ENDIF
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SCONTRA_HLP8")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SCONTRA_HLP14")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SCONTRA_HLP6")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SCONTRA_HLP6P")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SCONTRA_HLP7")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SCONTRA_HLP7P")
		CLEAR_HELP(TRUE)
	ENDIF
ENDPROC

PROC CLIENT_PROCESSING()
	
	#IF IS_DEBUG_BUILD
	MAINTAIN_J_SKIPS()
	#ENDIF
	// Blip the vehicle for off mission players
	MAINTAIN_VEHICLE_ENTRY_TIMERS()	
	MAINTAIN_PED_BODIES()
	MAINTAIN_GHOST_ORG_HELP()
	MAINTAIN_VEHICLE_ACCESS()
	CLIENT_SELL_VEH_LOOP()
	
	MAINTAIN_GLOBAL_REVEAL_SOUNDS()
	
	SWITCH GET_MODE_STATE()
	
		CASE eMODESTATE_INIT
			// ********************************************************************
			// Server says when omode has intialised and moves us onto run state.
			// ********************************************************************
		BREAK
		
		CASE eMODESTATE_RUN
			
			// ***********************
			// Do common start calls
			// ***********************
			IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_CALLED_COMMON_SETUP)
				GB_COMMON_BOSS_MISSION_SETUP(FMMC_TYPE_GB_CONTRABAND_SELL,DEFAULT,ENUM_TO_INT(serverBD.eSellVar))
				GB_SET_PLAYER_AS_QUALIFYING_PARTICIPANT()
				SETUP_DAMAGE_TRACKING(TRUE)
				
				IF bAIR_VARIATION()
					UNLOCK_AIRPORT_GATES()
				ENDIF
				// Unlock gates url:bugstar:2801049 - Sell Missions: Land Defend: Metal mesh gate remained closed at the the destination drop off point. 
				IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<210.93,-2022.58,17.65>>, 6.0, prop_fnclink_03gate1)
					PRINTLN("SET_STATE_OF_CLOSEST_DOOR_OF_TYPE, OPEN ")
					SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(prop_fnclink_03gate1, <<210.93,-2022.58,17.65>>, TRUE, 1.0)
				ENDIF
				SET_LOCAL_BIT0(eLOCALBITSET0_CALLED_COMMON_SETUP)
			ELSE
				IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_CALLED_SAFE_FOR_SPEC_CAM_SETUP)
					IF HAS_NET_TIMER_EXPIRED(serverBD.modeTimer, TRACK_GOON_DELAY)
						GB_SET_ITS_SAFE_FOR_SPEC_CAM()
						SET_LOCAL_BIT0(eLOCALBITSET0_CALLED_SAFE_FOR_SPEC_CAM_SETUP)
					ENDIF
				ENDIF
			ENDIF
			
			// *******************************************
			// Main mode logic here, after setup is done
			// *******************************************
			
			// Warp out of warehouse at the start if we're inside
			// Process logic once we're outside
			IF IS_PLAYER_IN_WAREHOUSE(PLAYER_ID())
			AND NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_WARPED_OUT_OF_WAREHOUSE_AT_START)
				g_SimpleInteriorData.bTriggerExit = TRUE
				PRINTLN("[EXEC1] [CTRB_SELL] INIT_CLIENT - Local player is inside warehouse, triggering walkout, setting eLOCALBITSET0_WARPED_OUT_OF_WAREHOUSE_AT_START")
				SET_LOCAL_BIT0(eLOCALBITSET0_WARPED_OUT_OF_WAREHOUSE_AT_START)
			ELSE
				IF g_SimpleInteriorData.bTriggerExit = TRUE
				OR NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_WARPED_OUT_OF_WAREHOUSE_AT_START)
				 	PRINTLN("[EXEC1] [CTRB_SELL], Player was already outside of warehouse, setting eLOCALBITSET0_WARPED_OUT_OF_WAREHOUSE_AT_START")
					SET_LOCAL_BIT0(eLOCALBITSET0_WARPED_OUT_OF_WAREHOUSE_AT_START)
					g_SimpleInteriorData.bTriggerExit = FALSE
				ENDIF
			
				IF NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_HIDE()
				AND NOT GB_IS_LOCAL_PLAYER_RESTRICTED_FOR_PASSIVE()
				// AND NOT GB_SHOULD_HIDE_GANG_BOSS_EVENT(// Add your mode here)
					IF GET_END_REASON() = eENDREASON_NO_REASON_YET
						IF MPGlobals.sFreemodeCache.bIsSafeToDisplayBossMissionUI
						OR HAS_ALL_CONTRABAND_BEEN_DROPPED_OFF()
							IF NOT IS_LOCAL_BIT0_SET(eLOCALBITSET0_CALLED_START_COMMON_TELEMETRY)
								GB_SET_COMMON_TELEMETRY_DATA_ON_START()
								SET_LOCAL_BIT0(eLOCALBITSET0_CALLED_START_COMMON_TELEMETRY)
								PRINTLN("[EXEC1] - [TELEMETRY] - [CTRB_SELL] called GB_SET_COMMON_TELEMETRY_DATA_ON_START()")
							ENDIF
							
							// ********************************************
							// Run main mode logic here.
							// Remember to set the player as 
							// GB_SET_PLAYER_AS_QUALIFYING_PARTICIPANT() 
							// when appropriate.
							// ********************************************
							
							DRAW_CHALLENGE_DPAD_LBD(g_GBLeaderboardStruct.challengeLbdStruct, g_GBLeaderboardStruct.siDpadMovie, SUB_BOSS, g_GBLeaderboardStruct.dpadVariables, 
														g_GBLeaderboardStruct.fmDpadStruct)
							CLIENT_PROCESS_MISSION_LOGIC()
							IF NOT g_SimpleInteriorData.bWalkingInOrOut //IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR() // url:bugstar:2825081
								HANDLE_SELL_TEXT()
								HANDLE_SELL_BLIPS()
								DRAW_HUD_BOTTOM_RIGHT()
							ENDIF
							HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
							HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_DISTRICT_NAME)
							HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_STREET_NAME)
							
							MAINTAIN_TRACKIFY_LOGIC()
							MAINTAIN_CLIENT_SPOOK_CHECKS()
							MAINTAIN_CLIENT_VEHICLE_SPAWN_CHECKS()
							MAINTAIN_MUSIC_AND_SOUNDS()
							MAINTAIN_ADDITIONAL_HELP()
							MAINTAIN_SPAWN_LOCATIONS()
							MAINTAIN_RESTRICT_MAX_WANTED_LEVEL()
							
							IF bAIR_CLEAR_AREA()
								MAINTAIN_LAND_PLANE_CLIENT()
							ELIF bAIR_LOW()
							OR bAIR_DROP()
							OR bAIR_RESTRICTED()
							OR bAIR_ATTACK()
							OR bSTING()
							OR bMULTIPLE()
								MAINTAIN_CRATE_DROP_CLIENT()
							ENDIF
							
							IF iFocusParticipant > (-1)
								IF iFocusParticipant = PARTICIPANT_ID_TO_INT()
									// I am playing, not spectating.
								ELSE
									// I am spectating not playing.
								ENDIF
							ENDIF
							
						ENDIF
						
					ENDIF
					
				ELSE
					GB_CLEAR_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_NOTIFICATION_BLIP_WARN)
					Clear_Any_Objective_Text_From_This_Script() // Clear objective text if player is retricted for hide or passive.
				ENDIF
			ENDIF
		BREAK
		
		CASE eMODESTATE_REWARDS
		
			IF bSEA_VARIATION()
				IF NOT HAS_NET_TIMER_STARTED(sLocalData.stWarpAllowanceTimer)
					START_NET_TIMER(sLocalData.stWarpAllowanceTimer)
				ENDIF	
			ENDIF
		
			// url:bugstar:2813512 - Sell Missions - Takes quite a long time to clean up the sell mission after a fail - call was delayed for ages and couldn't get inside my warehouse
			IF GET_END_REASON() <> eENDREASON_WIN_CONDITION_TRIGGERED
				IF NOT IS_BIT_SET(g_GB_WORK_VARS.iEventBitSet, ciGB_BOSS_SELL_MISSION_FAILED)
					PRINTLN("[EXEC1] [CTRB_SELL] CLIENT_PROCESSING, ciGB_BOSS_SELL_MISSION_FAILED")
					SET_BIT(g_GB_WORK_VARS.iEventBitSet, ciGB_BOSS_SELL_MISSION_FAILED)
				ENDIF
			ENDIF
			
			CLEANUP_RADAR_JAM_SOUNDS()
			
			CLEANUP_CRATE_DROP_ALARM()
		
			// Clear any help at this stage becuase the mission has almost ended
			HELP_TEXT_CLEANUP()
			
			// to protect players from being killed whilst trying to warp
			HANDLE_END_OF_MODE_CHECKS()
			
			IF IS_SAFE_TO_SHOW_REWARDS()
				CLEANUP_CUSTOM_SPAWNS()
				CLEANUP_CONTEXT()
				CLOSE_TRACKIFY()
				HANDLE_SELL_TEXT(TRUE)
				HANDLE_SELL_BLIPS(TRUE)
				HANDLE_REWARDS() // Handle end of mode rewards.
				SET_COOLDOWN_TIMER(serverBD.iStartingWarehouse) //Set the cooldown timer if successful
				CLEAR_AUDIO_FLAGS_ON_MODE_END()
				
				// Server will move us onto end state once all end of mode logic is complete.
			ENDIF
			
			CLEANUP_CONTRABAND_DECORATOR()
		BREAK
		
		CASE eMODESTATE_END
		
			PRINTLN("[EXEC1] [CTRB_SELL] CLIENT_PROCESSING, eMODESTATE_END  ")
			
		BREAK
		
	ENDSWITCH

ENDPROC

// For timed variant (removed)
PROC SERVER_GRAB_TIME_LIMIT()

	INT i
	VECTOR vDrop
	
	FLOAT fCurrentDist
	FLOAT fShortDist = 9999999.99
	
	IF serverBD.fDistDrop = -1.0
	
		// Work out closest drop-off
		REPEAT ciMAX_SELL_DROP_OFFS i
			
			IF IS_BIT_SET(serverBD.iActiveDropOffBitset, i)
			AND NOT IS_THIS_A_HIDDEN_DROP_OFF(i)
				vDrop = GET_SELL_DROP_COORDS(GET_SELL_VAR(), i, serverBD.iRoute)
				
				fCurrentDist = GET_DISTANCE_BETWEEN_COORDS(vDrop, VEH_POS())
				
				IF fCurrentDist < fShortDist
					fShortDist = fCurrentDist
				ENDIF
			ENDIF
		ENDREPEAT
		
		serverBD.fDistDrop = fShortDist
		PRINTLN("[EXEC1] [CTRB_SELL] SERVER_GRAB_TIME_LIMIT, fDistDrop = ", serverBD.fDistDrop)
		
		INT iKms
		iKms = ROUND(serverBD.fDistDrop / 1000)
		
		PRINTLN("[EXEC1] [CTRB_SELL] SERVER_GRAB_TIME_LIMIT, iKms = ", iKms)
		
		// 2 minutes plus a minute per km to the drop-off
		serverBD.iTimedTimeLimit = ( 120000 + ( 60000 * iKms ) )
		PRINTLN("[EXEC1] [CTRB_SELL] SERVER_GRAB_TIME_LIMIT, = ", serverBD.iTimedTimeLimit)
	ENDIF
ENDPROC

PROC SERVER_VEHICLE_LOOP()
	INT i
	INT iDeliveredCount
	REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i 
		IF HAS_THIS_SELL_VEH_DELIVERED_CONTRABAND(i)
			iDeliveredCount++
		ENDIF
	ENDREPEAT
	
	IF serverBD.iVehicleCountDeliveredAllContraband <> iDeliveredCount
		serverBD.iVehicleCountDeliveredAllContraband = iDeliveredCount
	ENDIF
ENDPROC

PROC SERVER_MANAGE_BOSS_LEAVING()
	IF GB_GET_LOCAL_PLAYER_GANG_BOSS() = INVALID_PLAYER_INDEX()
	AND GET_END_REASON() = eENDREASON_NO_REASON_YET
		PRINTLN("[EXEC1] [CTRB_SELL] SERVER_PROCESSING, SERVER_MANAGE_BOSS_LEAVING, eENDREASON_NO_BOSS_LEFT  ")
		SET_END_REASON(eENDREASON_NO_BOSS_LEFT)
	ENDIF
ENDPROC

PROC SERVER_PROCESSING()

	MAINTAIN_CRATE_DROP_SERVER()
	SERVER_MAINTAIN_DROPS()
	MAINTAIN_VEHICLE_STUCK_CHECKS()
	MAINTAIN_PED_BRAINS()
	MAINTAIN_NON_CONTRABAND_VEHICLES()
	MAINTAIN_NON_CONTRABAND_PED_AND_VEHICLE_RESPAWNING()
	MAINTAIN_BLOCKING_ZONES()
	MAINTAIN_CONTRABAND_BLIP_DELAY()
	SERVER_VEHICLE_LOOP()
	SERVER_MANAGE_BOSS_LEAVING()
	
	SWITCH GET_MODE_STATE()
	
		CASE eMODESTATE_INIT
			
			// ********************************************
			// Initialise server data here.
			// Will remain in here until server moves on.
			// ********************************************
			
			IF CREATE_SELL_VEHS()
				IF CREATE_DROP_OFF_VEH()
				AND CREATE_DROP_OFF_PEDS()
				AND CREATE_START_PEDS()
				
	//				SERVER_GRAB_TIME_LIMIT()
	
					serverBD.iLaunchPosix = GET_CLOUD_TIME_AS_INT()
					PRINTLN("[EXEC1] [CTRB_SELL] SERVER_PROCESSING, serverBD.iLaunchPosix = ", serverBD.iLaunchPosix)
					
					// Move onto next stage.
					SET_MODE_STATE(eMODESTATE_RUN)
					
					// Get MatchIds for Telemetry
					PLAYSTATS_CREATE_MATCH_HISTORY_ID_2(serverBD.iMatchId1, serverBD.iMatchId2)
					PRINTLN("[EXEC1] [CTRB_SELL] SERVER_PROCESSING, eMODESTATE_INIT  ")
				ENDIF
			ENDIF

		BREAK
		
		CASE eMODESTATE_RUN

//			PRINTLN("[EXEC1] [CTRB_SELL] SERVER_PROCESSING, eMODESTATE_RUN  ")
			
			// Stay in run state until we have an end reason.
			IF GET_END_REASON() = eENDREASON_NO_REASON_YET
				
				// Widget for forcing time up at end of mode.
				#IF IS_DEBUG_BUILD
				IF serverBD.bFakeEndOfModeTimer
					PRINTLN("[EXEC1] serverBD.bFakeEndOfModeTimer = TRUE.")
					SET_END_REASON(eENDREASON_TIME_UP)
				ENDIF
				#ENDIF
				
				// If the mode timer expires, set end reason to time up.
				IF NOT HAS_NET_TIMER_STARTED(serverBD.modeTimer)
					START_NET_TIMER(serverBD.modeTimer)
				ELSE
					
					IF HAS_NET_TIMER_EXPIRED(serverBD.modeTimer, iTIME_LIMIT())
						PRINTLN("[EXEC1] [CTRB_SELL] SERVER_PROCESSING, eENDREASON_TIME_UP  ", iTIME_LIMIT())
						
						SET_END_REASON(eENDREASON_TIME_UP)
						
					// Send warning text
					ELIF HAS_NET_TIMER_EXPIRED(serverBD.modeTimer, ciWARNING_TIMER)
						IF NOT IS_SERVER_BIT0_SET(eSERVERBITSET0_TIME_TEXT)
							PRINTLN("[EXEC1] [CTRB_SELL] SERVER_PROCESSING, eSERVERBITSET0_TIME_TEXT  ")
							SET_SERVER_BIT0(eSERVERBITSET0_TIME_TEXT)
						ENDIF
					ENDIF
				ENDIF
				
				// Veh destroyed
				INT i
				REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i 
					IF IS_SERVER_BIT0_SET(eSERVERBITSET0_VEH_MADE)
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[i])
							IF NOT IS_SELL_VEH_OK(i)
							AND NOT HAS_THIS_SELL_VEH_DELIVERED_CONTRABAND(i)
								PRINTLN("[EXEC1] [CTRB_SELL] SERVER_PROCESSING, eENDREASON_TRUCK_DESTROYED - Truck ", i)
								IF GET_END_REASON() != eENDREASON_TRUCK_DESTROYED
									SET_END_REASON(eENDREASON_TRUCK_DESTROYED)
									IF serverBD.iFailVehicle =  -1 
										serverBD.iFailVehicle =  i 
										PRINTLN("[EXEC1] [CTRB_SELL] SERVER_PROCESSING, serverBD.iFailVehicle = ", i)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
				
				IF IS_SERVER_BIT0_SET(eSERVERBITSET0_PLAYERS_FINISHED)
				AND GET_END_REASON() = eENDREASON_NO_REASON_YET
					PRINTLN("[EXEC1] [CTRB_SELL] SERVER_PROCESSING, eENDREASON_WIN_CONDITION_TRIGGERED  ")
					SET_END_REASON(eENDREASON_WIN_CONDITION_TRIGGERED)
				ENDIF
				
				IF IS_SERVER_BIT0_SET(eSERVERBITSET0_VEHICLE_STUCK)
				AND GET_END_REASON() = eENDREASON_NO_REASON_YET
					PRINTLN("[EXEC1] [CTRB_SELL] SERVER_PROCESSING, eENDREASON_VEHICLE_STUCK  ")
					SET_END_REASON(eENDREASON_VEHICLE_STUCK)
				ENDIF


			ELSE
				PRINTLN("[EXEC1] [CTRB_SELL] [SERVER] SERVER_PROCESSING, eENDREASON_TIME_UP  ")
				// Move onto rewards once we have an end reason (therefore mode has ended). 
				SET_MODE_STATE(eMODESTATE_REWARDS)
			ENDIF
			
			MAINTAIN_SELL_VEHICLES_STUCK_AFTER_CREATION()
		BREAK
		
		CASE eMODESTATE_REWARDS
		
			//PRINTLN("[EXEC1] [CTRB_SELL] SERVER_PROCESSING, eMODESTATE_REWARDS  ")
			
			// Once all clients have handled the end of the mode, allow the server to move to the end. 
			IF IS_SERVER_BIT0_SET(eSERVERBITSET0_ALL_PARTICIPANTS_COMPLETED_REWARDS)
				PRINTLN("[EXEC1] [CTRB_SELL] - [REWARDS] - all participants completed rewards.")
				SET_MODE_STATE(eMODESTATE_END)
			ENDIF
			
		BREAK
		
		CASE eMODESTATE_END
		
			PRINTLN("[EXEC1] [CTRB_SELL] SERVER_PROCESSING, eMODESTATE_END  ")
			// Do anything required at very end of mode once rewards and gameplay are opver, then end.
			SET_SERVER_GAME_STATE(GAME_STATE_END)
		BREAK
		
	ENDSWITCH

ENDPROC


// -----------------------------------------------------------------------------------------------------------
//		Mission Cleanup
// -----------------------------------------------------------------------------------------------------------

PROC VEHICLE_CLEANUP()

	SET_MODEL_AS_NO_LONGER_NEEDED(GET_SELL_VEH_MODEL())
	
	VEHICLE_INDEX vehId
	INT i
	REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[i])
		
			vehId = NET_TO_VEH(serverBD.niVeh[i])
		
			IF DOES_ENTITY_EXIST(VEH_ENT(i))
				IF NETWORK_HAS_CONTROL_OF_ENTITY(VEH_ENT(i))
					IF NOT IS_ENTITY_DEAD(VEH_ENT(i))
						IF NOT bAIR_VARIATION()
							BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(VEH_ID(i))
						ENDIF
						GB_CLEAR_VEHICLE_AS_CONTRABAND(VEH_ID(i))
					ENDIF
					SET_VEHICLE_AS_NO_LONGER_NEEDED(vehId)
					PRINTLN("[EXEC1] [CTRB_SELL] SCRIPT_CLEANUP, SET_VEHICLE_AS_NO_LONGER_NEEDED ")
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC DROP_VEHICLE_CLEANUP()

	
	VEHICLE_INDEX vehId
	INT i
	REPEAT GET_NUM_DROP_OFF_VEH_FOR_VARIATION() i
		SET_MODEL_AS_NO_LONGER_NEEDED(GET_DROP_OFF_VEH_MODEL(i))
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niDropOffVeh[i])
		
			vehId = NET_TO_VEH(serverBD.niDropOffVeh[i])
		
			IF DOES_ENTITY_EXIST(vehId)
			AND NETWORK_HAS_CONTROL_OF_ENTITY(vehId)
				IF IS_VEHICLE_DRIVEABLE(vehId)
					SET_ENTITY_INVINCIBLE(vehId, FALSE)
					SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(vehId, FALSE)
					SET_ENTITY_CAN_BE_DAMAGED(vehId, TRUE)
				ENDIF
				SET_VEHICLE_AS_NO_LONGER_NEEDED(vehId)
				PRINTLN("[EXEC1] [CTRB_SELL] SCRIPT_CLEANUP, DROP_VEHICLE_CLEANUP ")
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC DROP_PED_CLEANUP()
	INT i
	
	REPEAT GET_NUM_DROP_OFF_PEDS_FOR_VARIATION() i

		SET_MODEL_AS_NO_LONGER_NEEDED(GET_DROP_OFF_PED_MODEL( i))
		ENTITY_INDEX ent
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niDropOffPed[i])	
			ent = NET_TO_ENT(serverBD.niDropOffPed[i])
			IF DOES_ENTITY_EXIST(ent)
				IF NETWORK_HAS_CONTROL_OF_ENTITY(ent)
					IF IS_ENTITY_ALIVE(NET_TO_PED(serverBD.niDropOffPed[i]))
						SET_ENTITY_INVINCIBLE(NET_TO_PED(serverBD.niDropOffPed[i]), FALSE)
						SET_ENTITY_CAN_BE_DAMAGED(NET_TO_PED(serverBD.niDropOffPed[i]), TRUE)
					ENDIF
					PRINTLN("[EXEC1] [CTRB_SELL] SCRIPT_CLEANUP, DROP_PED_CLEANUP - ", i)
					SET_ENTITY_AS_NO_LONGER_NEEDED(ent)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC AI_PED_CLEANUP()
	INT i
	
	REPEAT GET_NUM_PEDS_FOR_VARIATION() i
		SET_MODEL_AS_NO_LONGER_NEEDED(GET_PED_MODEL_FOR_VARIATION( i))
		ENTITY_INDEX ent
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPed[i].netIndex)	
			ent = NET_TO_ENT(serverBD.sPed[i].netIndex)
			IF DOES_ENTITY_EXIST(ent)
				IF NETWORK_HAS_CONTROL_OF_ENTITY(ent)
					IF NOT IS_NET_PED_INJURED(serverBD.sPed[i].netIndex)
						SET_ENTITY_INVINCIBLE(NET_TO_PED(serverBD.sPed[i].netIndex), FALSE)
						SET_ENTITY_CAN_BE_DAMAGED(NET_TO_PED(serverBD.sPed[i].netIndex), TRUE)
					ENDIF
					SET_ENTITY_AS_NO_LONGER_NEEDED(ent)
					PRINTLN("[EXEC1] [CTRB_SELL] SCRIPT_CLEANUP, DROP_PED_CLEANUP - ", i)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC NON_CONTRABAND_VEHICLE_CLEANUP()
	INT i
	
	REPEAT GET_NUM_ATTACK_VEHICLES_FOR_VARIATION() i
		SET_MODEL_AS_NO_LONGER_NEEDED(GET_VEHICLE_MODEL_FOR_VARIATION())
		ENTITY_INDEX ent
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPedVeh[i].netIndex)	
			ent = NET_TO_ENT(serverBD.sPedVeh[i].netIndex)
			IF DOES_ENTITY_EXIST(ent)
				IF NETWORK_HAS_CONTROL_OF_ENTITY(ent)
					IF IS_NET_VEHICLE_DRIVEABLE(serverBD.sPedVeh[i].netIndex)
						SET_ENTITY_INVINCIBLE(ent, FALSE)
						SET_ENTITY_CAN_BE_DAMAGED(ent, TRUE)
					ENDIF
					SET_ENTITY_AS_NO_LONGER_NEEDED(ent)
					PRINTLN("[EXEC1] [CTRB_SELL] SCRIPT_CLEANUP, DROP_PED_CLEANUP - ", i)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC CLEANUP_TRACKIFY()
	IF bLAND_TRACKIFY()
		IF IS_LOCAL_BIT0_SET(eLOCALBITSET0_ENABLED_TRACKIFY_APP)
			ENABLE_MULTIPLAYER_TRACKIFY_APP(FALSE)
			CLEAR_LOCAL_BIT0(eLOCALBITSET0_ENABLED_TRACKIFY_APP)
		ENDIF
	ENDIF
ENDPROC

PROC REMOVE_ALL_SELL_PA_TEXT_MESSAGES()
	DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER("SCONTRA_TXT_6")
	DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER("SCONTRA_TXT_7")
	//DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER("SCONTRA_TXT_2")
	DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER("SCONTRA_TXT_1")
	DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER("SCONTRA_TXT_4")
	DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER("SCONTRA_TXT_8")
	DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER("SCONTRA_TXT_10")
	DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER("SCONTRA_TXT_11")
	DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER("SCONTRA_TXT_12")
	DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER("SCONTRA_TXT_13")
	DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER("SCONTRA_TXT_14")
	DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER("SCONTRA_TXT_15")
	DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER("SCONTRA_TXT_16")
	DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER("SCONTRA_TXT_17")
	DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER("SCONTRA_TXT_18")
	DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER("SCONTRA_TXT_19")
ENDPROC

BOOL bGangBossOnStart

FUNC INT GET_COUNT_OF_DESTROYED_CONTRABAND()
	IF GET_END_REASON() = eENDREASON_TRUCK_DESTROYED
		IF serverBD.iFailVehicle = 0
			RETURN (iDROP_TARGET() - serverBD.iVeh0DropCount)
		ElIF serverBD.iFailVehicle = 1
			RETURN (iDROP_TARGET() - serverBD.iVeh1DropCount)
		ELIF serverBD.iFailVehicle = 2
			RETURN (iDROP_TARGET() - serverBD.iVeh2DropCount)
		ENDIF
		
		RETURN (iDROP_TARGET() - serverBD.iDroppedOffCount)
	ENDIF
	RETURN 0
ENDFUNC

FUNC INT GET_COUNT_OF_DELIVERED_CONTRABAND()
	IF DOES_SHIPMENT_TYPE_HAVE_MULTIPLE_VEHICLES()
		RETURN (serverBD.iVeh0DropCount + serverBD.iVeh1DropCount + serverBD.iVeh2DropCount)
	ENDIF
	
	RETURN serverBD.iDroppedOffCount 
ENDFUNC


PROC SCRIPT_CLEANUP(BOOL bSetDoDissconnectHelp = FALSE)

	PRINTLN("[EXEC1] [CTRB_SELL] SCRIPT_CLEANUP")
	SET_FAKE_MINIMAP_MAX_ALTIMETER_HEIGHT(0)
	SET_PLAYER_RIVAL_ENTITY_TYPE(eRIVALENTITYTYPE_INVALID)

	IF iRadarJamContextIntention != (-1)
		RELEASE_CONTEXT_INTENTION(iRadarJamContextIntention)
	ENDIF
	
	IF bSetDoDissconnectHelp
	AND bGangBossOnStart
		SET_BIT(MPGlobalsAmbience.iFmGbHelpBitSet2, BI_FM_GANG_BOSS_HELP_2_DO_DISSCONNECT_HELP)
		PRINTLN("[EXEC1] [CTRB_SELL] [INIT_CLIENT] SET_BIT(MPGlobalsAmbience.iFmGbHelpBitSet2, BI_FM_GANG_BOSS_HELP_2_DO_DISSCONNECT_HELP)")
	ENDIF
	
	SETUP_DAMAGE_TRACKING(FALSE)
	GB_CLEAR_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_NOTIFICATION_AIR_DROP) 
	GB_CLEAR_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_NOTIFICATION_HIDE_CONTRABAND_PLAYER)
	GB_CLEAR_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_NOTIFICATION_HIDE_CONTRABAND_PLAYER_P)
	CLEAR_ANY_MISSION_HELP()
	REMOVE_ALL_BLIPS()
	CLEANUP_PLANE_FX()
	CLEANUP_REPAIR_FX()
	CLEANUP_ALL_CRATE_FLARES()
	CLEANUP_CRATES()
	CLEANUP_CRATE_DROP_ALARM()
	CLEANUP_RADAR_JAM_SOUNDS()
	REMOVE_ALL_SELL_PA_TEXT_MESSAGES()
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF SHOULD_RESTRICT_MAX_WANTED_LEVEL()
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bNoCopsActive)
				SET_MAX_WANTED_LEVEL(5)
				PRINTLN("[EXEC1] [CTRB_SELL] [SCRIPT_CLEANUP] SET_MAX_WANTED_LEVEL(5)")
			ENDIF
		ENDIF
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
	AND GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
	AND GET_END_REASON() != eENDREASON_WIN_CONDITION_TRIGGERED
		GB_SET_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_NOTIFICATION_FAIL_CALL_TYPE_SELL)		
		PRINTLN("[EXEC1] [CTRB_SELL] [GB_MAINTAIN_FAIL_CALLS] GB_CALL_PLAYER_FOR_FAIL_CONVERSATION - eGB_LOCAL_HELP_BITSET_NOTIFICATION_FAIL_CALL_TYPE_SELL")
	ENDIF
	
	//Kick players out the vehicle or damage it if they are in it
	IF GET_END_REASON()!= eENDREASON_NO_REASON_YET
		INT i
		REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[i])
			AND NOT IS_ENTITY_DEAD(VEH_ID(i))
				IF bAIR_VARIATION()
					IF NETWORK_HAS_CONTROL_OF_ENTITY(VEH_ENT(i))
						PRINTLN("[EXEC1] [CTRB_SELL] [SCRIPT_CLEANUP] bAIR_VARIATION, i = ", i)
						IF IS_ENTITY_IN_AIR(VEH_ENT(i))
							PRINTLN("[EXEC1] [CTRB_SELL] [SCRIPT_CLEANUP] bAIR_VARIATION, IS_ENTITY_IN_AIR ")
							IF GET_END_REASON() <> eENDREASON_WIN_CONDITION_TRIGGERED
								SET_VEHICLE_ENGINE_ON(VEH_ID(i), FALSE, TRUE)							
							ENDIF
//							SET_VEHICLE_ENGINE_HEALTH(VEH_ID(i), 0)
						ELSE
							PRINTLN("[EXEC1] [CTRB_SELL] [SCRIPT_CLEANUP] bAIR_VARIATION, IS_ENTITY_IN_AIR(false) ")
							EJECT_PLAYER_FROM_VEHICLE(i, TRUE)
							LOCK_SELL_VEH(i, TRUE)
						ENDIF
					ENDIF
				ELIF GET_END_REASON() != eENDREASON_WIN_CONDITION_TRIGGERED	
				OR ( bLAND_VARIATION() AND GET_END_REASON() = eENDREASON_WIN_CONDITION_TRIGGERED )	
					PRINTLN("[EXEC1] [CTRB_SELL] [SCRIPT_CLEANUP] backup ")
					IF bSEA_VARIATION()
						//Steve add fix for - url:bugstar:2769866 - Sell Contraband - Sting: Players are able to keep the Brickade after the mission ends
						IF NETWORK_HAS_CONTROL_OF_ENTITY(VEH_ENT(i))
							SET_BOAT_SINKS_WHEN_WRECKED(VEH_ID(i), TRUE)
							SET_VEHICLE_ENGINE_ON(VEH_ID(i), FALSE, TRUE)
							SET_VEHICLE_ENGINE_HEALTH(VEH_ID(i), 0)
							SET_BOAT_WRECKED(VEH_ID(i))
						ENDIF
					ELSE
						EJECT_PLAYER_FROM_VEHICLE(i)
						LOCK_SELL_VEH(i, TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF

	VEHICLE_CLEANUP()
	DROP_VEHICLE_CLEANUP()
	DROP_PED_CLEANUP()
	AI_PED_CLEANUP()
	NON_CONTRABAND_VEHICLE_CLEANUP()
	REMOVE_TRAFFIC_REDUCTION_ZONE(serverBD.iTrafficReductionIndex)
	CLEANUP_TRACKIFY()
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
	ENDIF
	
	SET_SUPPRESS_GANG_CHASE_MODELS_FOR_VARIATION( FALSE)
	//
	
	IF bAIR_LOW()
		IF IS_LOCAL_BIT0_SET(eLOCALBITSET0_PLAY_ALTITUDE_WARNING_SOUND)
			STOP_SOUND(sLocaldata.iAltitudeWarningSound)
			CLEAR_LOCAL_BIT0(eLOCALBITSET0_PLAY_ALTITUDE_WARNING_SOUND)
			PRINTLN("[EXEC1] [CTRB_SELL] [SCRIPT_CLEANUP] Stop warning sound")
		ENDIF
		
		RELEASE_SOUND_ID(sLocaldata.iAltitudeWarningSound)
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		g_sGb_Telemetry_data.sdata.m_match1 = serverBD.iMatchId1
		g_sGb_Telemetry_data.sdata.m_match2 = serverBD.iMatchId2
		PROCESS_CURRENT_BOSS_CONTRABAND_PLAYSTATS(	ENUM_TO_INT(GET_SELL_VAR()), 
													serverBD.iRoute, 
													serverBD.iShipmentSize, 
													serverBD.iStartingWarehouse, 
													serverBD.iWarehouseTotalContraband, 
													serverBD.dropOffLocationBitSet,
													DEFAULT,
													GET_COUNT_OF_DESTROYED_CONTRABAND(),
													GET_COUNT_OF_DELIVERED_CONTRABAND())
		PRINTLN("[EXEC1] - [TELEMETRY] - [CTRB_SELL] called PROCESS_CURRENT_BOSS_CONTRABAND_PLAYSTATS")
	ENDIF
	
	GB_COMMON_BOSS_MISSION_CLEANUP()	
	
	Clear_Any_Objective_Text_From_This_Script()
	
	//GB_TIDYUP_SPECTATOR_CAM()
	
	TERMINATE_THIS_THREAD()
ENDPROC

FUNC BOOL PROCESS_PRE_GAME(MP_MISSION_DATA missionScriptArgs)
	
	// This marks the script as a net script, and handles any instancing setup. 
	SETUP_MP_MISSION_FOR_NETWORK(GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(missionScriptArgs.mdID.idMission), missionScriptArgs)
	INT iLoop
	SERVER_CLEAR_LAST_DROP_OFFS()
	REPEAT ciMAX_CONTRABAND_PED_VEHICLES iLoop
		serverBD.iDropOffAiVehShouldUse[iLoop] = -1
	ENDREPEAT
	
	//Reserve Entities
	RESERVE_NETWORK_MISSION_OBJECTS(GB_GET_BOSS_MISSION_NUM_OBJ_REQUIRED(FMMC_TYPE_GB_CONTRABAND_SELL))
	RESERVE_NETWORK_MISSION_PEDS(GB_GET_BOSS_MISSION_NUM_PEDS_REQUIRED(FMMC_TYPE_GB_CONTRABAND_SELL))
	RESERVE_NETWORK_MISSION_VEHICLES(GB_GET_BOSS_MISSION_NUM_VEH_REQUIRED(FMMC_TYPE_GB_CONTRABAND_SELL))
	// This makes sure the net script is active, waits until it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD,SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD,SIZE_OF(playerBD))
	
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
//	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
//		g_iContraSellMissionCratesToSell = 5
//		g_iContraSellMissionBuyerID = 2
//	ENDIF
	
	PRINTLN("[EXEC1] [CTRB_SELL] PROCESS_PRE_GAME g_iContraSellMissionBuyerID = ", g_iContraSellMissionBuyerID, " g_iContraSellMissionCratesToSell = ", g_iContraSellMissionCratesToSell)
	
	
	sLocaldata.contraData = GB_GET_PLAYER_CONTRABAND_MISSION_DATA()
	
	GB_COMMON_BOSS_MISSION_PREGAME()

	RETURN TRUE
	
ENDFUNC


PROC RESTART_DAMAGE_TIMER()
	IF NOT HAS_NET_TIMER_STARTED(sLocaldata.localDamageTimer)
		START_NET_TIMER(sLocaldata.localDamageTimer)
		PRINTLN("[EXEC1] [CTRB_SELL] [CTRB_SELL DAMAGE] [DMG]  - Starting damage timer.")
	ELSE
		REINIT_NET_TIMER(sLocaldata.localDamageTimer)
		START_NET_TIMER(sLocaldata.localDamageTimer)
		PRINTLN("[EXEC1] [CTRB_SELL] [CTRB_SELL DAMAGE] [DMG]  - Resetting damage timer.")
	ENDIF
ENDPROC

CONST_INT ciVEH_DMG_MIN		1

// Cap url:bugstar:2794712
FUNC INT iDAMAGE_AMOUNT()

	INT iDamage
	
	iDamage = GET_RANDOM_INT_IN_RANGE(g_sMPTunables.iexec_sell_nodamage_min_value_reduction, ( g_sMPTunables.iexec_sell_nodamage_max_value_reduction  + 1 ) )
	
//	IF iDamage > g_sMPTunables.iexec_sell_nodamage_value_reduction
//		iDamage = g_sMPTunables.iexec_sell_nodamage_value_reduction
//	ENDIF

	RETURN iDamage
ENDFUNC

PROC PROCESS_CRASH_DAMAGE()
	// Hit by a vehicle
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF bLAND_DAMAGE()
			INT iDMG = iDAMAGE_AMOUNT()
			PRINTLN("[EXEC1] [CTRB_SELL] [CTRB_SELL DAMAGE] [DMG]  - PROCESS_CRASH_DAMAGE - ***************************************************************")
			PRINTLN("[EXEC1] [CTRB_SELL] [CTRB_SELL DAMAGE] [DMG]  - PROCESS_CRASH_DAMAGE - Player hit by car or crashed, cash before $", serverBD.iBonus)
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			AND (IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
			OR IS_PED_IN_ANY_PLANE(PLAYER_PED_ID()))
				PRINTLN("[EXEC1] [CTRB_SELL] [CTRB_SELL DAMAGE] [DMG]  - PROCESS_CRASH_DAMAGE - Player hit by car or crashed [air], dropping $", iDMG)
				serverBD.iBonus -= iDMG
			ELSE
				PRINTLN("[EXEC1] [CTRB_SELL] [CTRB_SELL DAMAGE] [DMG]  - PROCESS_CRASH_DAMAGE - Player hit by car or crashed, dropping $", iDMG)
				serverBD.iBonus -= iDMG
			ENDIF
			RESTART_DAMAGE_TIMER()
			IF serverBD.iBonus < ciVEH_DMG_MIN
				PRINTLN("[EXEC1] [CTRB_SELL] [CTRB_SELL DAMAGE] [DMG]  - PROCESS_CRASH_DAMAGE - player cash has dropped below $", ciVEH_DMG_MIN, ", keeping at $", ciVEH_DMG_MIN)
				serverBD.iBonus = ciVEH_DMG_MIN
			ENDIF
			PRINTLN("[EXEC1] [CTRB_SELL] [CTRB_SELL DAMAGE] [DMG]  - PROCESS_CRASH_DAMAGE - Player hit by car or crashed, remaining cash is now $", serverBD.iBonus)
			PRINTLN("[EXEC1] [CTRB_SELL] [CTRB_SELL DAMAGE] [DMG]  - PROCESS_CRASH_DAMAGE - ***************************************************************")
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_VEHICLE_DAMAGE()
	// Hit by a vehicle
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF bLAND_DAMAGE()
			INT iDMG = iDAMAGE_AMOUNT()
			PRINTLN("[EXEC1] [CTRB_SELL] [CTRB_SELL DAMAGE] [DMG]  - PROCESS_VEHICLE_DAMAGE - ***************************************************************")
			PRINTLN("[EXEC1] [CTRB_SELL] [CTRB_SELL DAMAGE] [DMG]  - PROCESS_VEHICLE_DAMAGE - Player's car has been hit, cash before $", serverBD.iBonus)
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			AND( IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
			OR IS_PED_IN_ANY_PLANE(PLAYER_PED_ID()))
				PRINTLN("[EXEC1] [CTRB_SELL] [CTRB_SELL DAMAGE] [DMG]  - PROCESS_VEHICLE_DAMAGE - Player's air vehicle has been hit, dropping $", iDMG)
				serverBD.iBonus -= iDMG
			ELSE
				PRINTLN("[EXEC1] [CTRB_SELL] [CTRB_SELL DAMAGE] [DMG]  - PROCESS_VEHICLE_DAMAGE - Player's car has been hit, dropping $", iDMG)
				serverBD.iBonus -= iDMG 
			ENDIF
			RESTART_DAMAGE_TIMER()
			IF serverBD.iBonus < ciVEH_DMG_MIN
				PRINTLN("[EXEC1] [CTRB_SELL] [CTRB_SELL DAMAGE] [DMG]  - PROCESS_VEHICLE_DAMAGE - player cash has dropped below $", ciVEH_DMG_MIN, ", keeping at $", ciVEH_DMG_MIN)
				serverBD.iBonus = ciVEH_DMG_MIN
			ENDIF
			PRINTLN("[EXEC1] [CTRB_SELL] [CTRB_SELL DAMAGE] [DMG]  - PROCESS_VEHICLE_DAMAGE - Player's car has been hit, remaining cash is now $", serverBD.iBonus)
			PRINTLN("[EXEC1] [CTRB_SELL] [CTRB_SELL DAMAGE] [DMG]  - PROCESS_VEHICLE_DAMAGE - ***************************************************************")
		ENDIF
	ENDIF
ENDPROC

PROC SEND_CONTRABAND_DESTROYED_TICKER(PLAYER_INDEX destroyer)
	
	SCRIPT_EVENT_DATA_TICKER_MESSAGE sData
	
	sData.TickerEvent = TICKER_EVENT_CONTRABAND_DESTROYED
	sData.playerID = destroyer
		
	INT iPlayer, iPlayerFlags
	
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		IF NETWORK_IS_PLAYER_ACTIVE(INT_TO_PLAYERINDEX(iPlayer))
			IF IS_NET_PLAYER_OK(destroyer, FALSE)
			
				IF NETWORK_IS_PLAYER_A_PARTICIPANT(destroyer)
					SET_BIT(iPlayerFlags, iPlayer)
				ELSE
					IF NOT GB_IS_PLAYER_ON_CONTRABAND_MISSION(destroyer)
						SET_BIT(iPlayerFlags, iPlayer)
					ENDIF 
				ENDIF
			
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF iPlayerFlags != 0
		BROADCAST_TICKER_EVENT(sData, iPlayerFlags)
	ENDIF
	
ENDPROC

PROC PROCESS_DAMAGE(INT iCount)
	
	PED_INDEX VictimPedID	
	PED_INDEX DamagerPedID	
	PLAYER_INDEX playerDamager
//	PLAYER_INDEX VictimPlayerID
	VEHICLE_INDEX VictimVehicleID
	VEHICLE_INDEX DamagerVehicleID
	INT i

	STRUCT_ENTITY_DAMAGE_EVENT sEntityID
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sEntityID, SIZE_OF(sEntityID))
		PRINTLN("2830923, PROCESS_DAMAGE 0 ")
		// IF the victim exists
		IF DOES_ENTITY_EXIST(sEntityID.VictimIndex)
			PRINTLN("2830923, PROCESS_DAMAGE 1 ")

			// IF the victim is a vehicle
			IF IS_ENTITY_A_VEHICLE(sEntityID.VictimIndex)
			
				PRINTLN("2830923, PROCESS_DAMAGE 2 ")
			
				VictimVehicleID = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(sEntityID.VictimIndex)
				
				REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
					IF VictimVehicleID = VEH_ID(i)
					
						PRINTLN("2830923, PROCESS_DAMAGE 3 ")
		
						IF DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
							PRINTLN("2830923, PROCESS_DAMAGE 4 ")
							IF IS_ENTITY_A_PED(sEntityID.DamagerIndex)
								PRINTLN("2830923, PROCESS_DAMAGE 5 ")
								DamagerPedID = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex)
								IF IS_PED_A_PLAYER(DamagerPedID)
									playerDamager = NETWORK_GET_PLAYER_INDEX_FROM_PED(DamagerPedID)
									PRINTLN("2830923, PROCESS_DAMAGE 6A ")
									
									IF NOT IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), eCLIENTBITSET0_SERVER_SHOULD_SEND_DESTROYED_EVENT)
										IF NOT IS_SELL_VEH_OK(i)
											IF GET_END_REASON() = eENDREASON_NO_REASON_YET
												IF NOT HAS_THIS_SELL_VEH_DELIVERED_CONTRABAND(i)
													playerBD[PARTICIPANT_ID_TO_INT()].damagerPlayer = playerDamager
													SET_CLIENT_BIT0(eCLIENTBITSET0_SERVER_SHOULD_SEND_DESTROYED_EVENT)
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
								IF DamagerPedID = PLAYER_PED_ID()
									PROCESS_CRASH_DAMAGE()
								ELSE
									PROCESS_VEHICLE_DAMAGE()
								ENDIF
							ELSE
								PRINTLN("2830923, PROCESS_DAMAGE 6B ")
								PROCESS_CRASH_DAMAGE()
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
			ELIF IS_ENTITY_A_PED(sEntityID.VictimIndex)
				PRINTLN("2830923, PROCESS_DAMAGE 777 ")
				VictimPedID = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.VictimIndex)
				IF NOT IS_CLIENT_BIT0_SET(PARTICIPANT_ID(), eCLIENTBITSET0_KILLED_COP)
					IF IS_MODEL_AMBIENT_COP(GET_ENTITY_MODEL(VictimPedID))
						IF DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
							IF IS_ENTITY_A_PED(sEntityID.DamagerIndex)
								DamagerPedID = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex)
								IF DamagerPedID = PLAYER_PED_ID()
									PRINTLN("[EXEC1] [CTRB_SELL] [WTD] PROCESS_DAMAGE, eCLIENTBITSET0_KILLED_COP")
									SET_CLIENT_BIT0(eCLIENTBITSET0_KILLED_COP)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
			IF IS_ENTITY_A_VEHICLE(sEntityID.DamagerIndex)
				DamagerVehicleID = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex)
				REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
					IF DamagerVehicleID = VEH_ID(i)
						
						PRINTLN("[land damage] [damager] - Weapon used: ", sEntityID.WeaponUsed)
					
						//Collision Damage
						IF GET_WEAPON_DAMAGE_TYPE(INT_TO_ENUM(WEAPON_TYPE, sEntityID.WeaponUsed)) = DAMAGE_TYPE_COLLISION
						OR sEntityID.IsResponsibleForCollision
							PRINTLN("[EXEC1] [CTRB_SELL] [WTD] PROCESS_DAMAGE - Vehicle ", i, "; DAMAGE_TYPE_COLLISION")
							PROCESS_CRASH_DAMAGE()
							EXIT
						ENDIF
						
						IF INT_TO_ENUM(WEAPON_TYPE, sEntityID.WeaponUsed) = WEAPONTYPE_RAMMEDBYVEHICLE
							PRINTLN("[EXEC1] [CTRB_SELL] [WTD] PROCESS_DAMAGE - Vehicle ", i, ", WEAPONTYPE_RAMMEDBYVEHICLE")
							PROCESS_CRASH_DAMAGE()
							EXIT
						ENDIF
					ENDIF
				ENDREPEAT
						
			ELIF IS_ENTITY_A_PED(sEntityID.DamagerIndex)
				IF IS_PED_A_PLAYER(GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex))
					playerDamager = NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex))
					
					// Is the player on the script (i.e. in the organisation)
					IF NETWORK_IS_PLAYER_A_PARTICIPANT(playerDamager)
						IF IS_THIS_PLAYER_IN_ANY_SELL_VEH(playerDamager)
							
							//Collision Damage
							IF GET_WEAPON_DAMAGE_TYPE(INT_TO_ENUM(WEAPON_TYPE, sEntityID.WeaponUsed)) = DAMAGE_TYPE_COLLISION
							OR sEntityID.IsResponsibleForCollision
								PRINTLN("[EXEC1] [CTRB_SELL] [WTD] PROCESS_DAMAGE - ", GET_PLAYER_NAME(playerDamager), "; DAMAGE_TYPE_COLLISION")
								PROCESS_CRASH_DAMAGE()
								EXIT
							ENDIF
							
							// Rammed damage
							IF INT_TO_ENUM(WEAPON_TYPE, sEntityID.WeaponUsed) = WEAPONTYPE_RAMMEDBYVEHICLE
								PRINTLN("[EXEC1] [CTRB_SELL] [WTD] PROCESS_DAMAGE - ", GET_PLAYER_NAME(playerDamager), ", WEAPONTYPE_RAMMEDBYVEHICLE")
								PROCESS_CRASH_DAMAGE()
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_EVENTS()

    INT iCount
    EVENT_NAMES ThisScriptEvent
//	STRUCT_EVENT_COMMON_DETAILS Details
	SCRIPT_EVENT_DATA_EXEC_DROP sEvent
	STRUCT_SCRIPT_EVENT_SET_VEHICLE_AS_TARGETABLE_BY_HOMING_MISSILE eMissileEvent
	SCRIPT_EVENT_DATA_CONTRABAND_DESTROYED sDestroyedEvent
	//SCRIPT_EVENT_DATA_EXEC_DROP_REPAIR DetailsSub
   
    REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iCount

        ThisScriptEvent = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iCount)
       	
        SWITCH ThisScriptEvent
            
			CASE EVENT_NETWORK_SCRIPT_EVENT
				
				IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sEvent, SIZE_OF(sEvent))
					SWITCH sEvent.Details.Type
						CASE SCRIPT_EVENT_CONTRABAND_SELL_START_RADAR_JAM
							IF NOT HAS_NET_TIMER_STARTED(sLocaldata.stRadarJamTimer)
								PRINTLN("[EXEC1] [CTRB_SELL] [ST EXEC] [RJAM] [SERVER] PROCESS_EVENTS, Radar Jam Timer started")
								START_NET_TIMER(sLocaldata.stRadarJamTimer)
							ENDIF
						BREAK
						
						CASE SCRIPT_EVENT_CONTRABAND_SELL_STOP_RADAR_JAM
							IF HAS_NET_TIMER_STARTED(sLocaldata.stRadarJamTimer)
								PRINTLN("[EXEC1] [CTRB_SELL] [ST EXEC] [RJAM] [SERVER] PROCESS_EVENTS, Radar Jam Timer reset")
								RESET_NET_TIMER(sLocaldata.stRadarJamTimer)
							ENDIF
						BREAK
						
						CASE SCRIPT_EVENT_EXEC_DROP
							IF sEvent.iPosix = serverBD.iLaunchPosix
								IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
									IF NOT bAIR_VARIATION()
									OR NOT IS_BIT_SET(sLocaldata.iEventBitSet, sEvent.iDropOff)
										PRINTLN("[EXEC1] [CTRB_SELL] [CRT] PROCESS_EVENTS, NEW_SET_SERVER_BIT0(eSERVERBITSET0_CRATE_DROP) sEvent.iVeh = ", sEvent.iVeh, " sEvent.iDropOff = ", sEvent.iDropOff) 
										INT iBitToCheck
										eSERVER_BITSET_0 eServerBitToCheck
										iBitToCheck = ENUM_TO_INT(eSERVERBITSET0_CRATE_DROP1) + sEvent.iVeh
										eServerBitToCheck = INT_TO_ENUM(eSERVER_BITSET_0, iBitToCheck)
										SET_SERVER_BIT0(eServerBitToCheck)
										SET_BIT(serverBD.iVehForCrateDropBitSet, sEvent.iVeh)
										SET_BIT(sLocaldata.iEventBitSet, sEvent.iDropOff)
									ENDIF
								ENDIF		
								
								IF bAIR_VARIATION()
									IF IS_NET_PLAYER_OK(sEvent.Details.FromPlayerIndex)
									AND IS_NET_PLAYER_OK(PLAYER_ID())
										IF sEvent.Details.FromPlayerIndex = PLAYER_ID()
										OR GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(GET_PLAYER_PED(sEvent.Details.FromPlayerIndex))) < 5.00
											PLAY_AIR_DROP_CRATE_SOUND()
										ENDIF
									ENDIF
								ENDIF
							ELSE
								PRINTLN("[EXEC1] [CTRB_SELL] SCRIPT_EVENT_EXEC_DROP - Posix doesn't match. sEvent.iPosix = ", sEvent.iPosix, " - serverBD.iLaunchPosix = ", serverBD.iLaunchPosix)
							ENDIF
						BREAK
					ENDSWITCH
				ENDIF
				
				IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, eMissileEvent, SIZE_OF(eMissileEvent))
					SWITCH eMissileEvent.Details.Type
						CASE SCRIPT_EVENT_GB_SET_VEHICLE_AS_TARGETABLE_BY_HOMING_MISSILE
							IF IS_PLAYER_IN_ANY_SELL_VEH()
								INT iVeh 
								iVeh = GET_SELL_VEH_I_AM_IN_INT()
								IF iVeh <> -1
									IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[iVeh])
										IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niVeh[iVeh])
											SET_VEHICLE_ALLOW_HOMING_MISSLE_LOCKON_SYNCED(VEH_ID(iVeh), eMissileEvent.bTargetable)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						BREAK
					ENDSWITCH
				ENDIF
				
				IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sDestroyedEvent, SIZE_OF(sDestroyedEvent))
					IF sDestroyedEvent.Details.Type = SCRIPT_EVENT_CONTRABAND_DESTROYED
						IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(PLAYER_ID(), sDestroyedEvent.contrabandOwner)
							IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
								IF NOT IS_SERVER_BIT1_SET(eSERVERBITSET1_DESTROYED_TICKER_SENT)
									SEND_CONTRABAND_DESTROYED_TICKER(sDestroyedEvent.destroyer)
									SET_SERVER_BIT1(eSERVERBITSET1_DESTROYED_TICKER_SENT)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE EVENT_NETWORK_DAMAGE_ENTITY
				
				PROCESS_DAMAGE(iCount)	
			BREAK
			
        ENDSWITCH
            
    ENDREPEAT
ENDPROC

// [NOTE FOR BOBBY] - Start the timer for delaying blipping once players have entered vehicle
//						Look for calls to this if blip doesnt appear, likely the culprit

PROC START_BLIP_DELAY_TIMER()
	IF SHOULD_DELAY_CONTRABAND_BLIP_FOR_BUYER()
		IF NOT HAS_NET_TIMER_STARTED(serverBD.stBlipDelayTimer)
			PRINTLN("[EXEC1] [CTRB_SELL] - START_BLIP_DELAY_TIMER - started")
			START_NET_TIMER(serverBD.stBlipDelayTimer)
		ENDIF
	ENDIF
ENDPROC


PROC SERVER_PROCESS_PARTICIPANT_LOOP()
	
	INT iParticipant
	INT iPlayer
	INT iVeh
	PLAYER_INDEX playerID
	PLAYER_INDEX playerDriver[ciMAX_SELL_VEHS]
	PARTICIPANT_INDEX participantID
	PED_INDEX pedID
	BOOL bAllCompletedRewards = TRUE
	BOOL bVehOccupied[ciMAX_SELL_VEHS]
	BOOL bPlayersFinished = TRUE
	BOOL bVehStuck = FALSE
	
	
	//BOOL bVehGrounded = FALSE

	// Refill data.
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_NATIVE(PARTICIPANT_INDEX, iParticipant))
			
			participantID = INT_TO_NATIVE(PARTICIPANT_INDEX, iParticipant)
			playerID = NETWORK_GET_PLAYER_INDEX(participantID)
			pedID = GET_PLAYER_PED(playerID)
			iPlayer = NATIVE_TO_INT(playerID)
			
			SET_BIT(iParticipantActiveBitset, iParticipant)
			
			IF IS_NET_PLAYER_OK(playerID, FALSE)
			
				IF playerBD[iParticipant].eStage <> eSELL_STAGE_CLEANUP
					bPlayersFinished = FALSE
				ENDIF
				
				SET_BIT(iPlayerOkBitset, iPlayer)
				
				IF IS_ENTITY_DEAD(pedID)
					SET_BIT(iPlayerDeadBitset, iPlayer)
				ENDIF
				
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[0])
					IF IS_SELL_VEH_OCCUPIED_BY_TEAMMATE(0, GET_PLAYER_PED(playerID))
						playerDriver[0] = playerID
						bVehOccupied[0] = TRUE
					ENDIF
				ENDIF
				
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[0])
					IF IS_SELL_VEH_OCCUPIED_BY_TEAMMATE(1, GET_PLAYER_PED(playerID))
						playerDriver[1] = playerID
						bVehOccupied[1] = TRUE
					ENDIF
				ENDIF
				
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[0])
					IF IS_SELL_VEH_OCCUPIED_BY_TEAMMATE(2, GET_PLAYER_PED(playerID))
						playerDriver[2] = playerID
						bVehOccupied[2] = TRUE
					ENDIF
				ENDIF
				
//				IF IS_CLIENT_BIT0_SET(participantID, eCLIENTBITSET0_VEH_GROUNDED)
//					bVehGrounded = TRUE
//				ENDIF
				
				
				IF NOT bVehStuck
					//IF IS_CLIENT_BIT0_SET(participantID, eCLIENTBITSET0_VEH_STUCK)
					IF IS_CLIENT_BIT0_SET(participantID, eCLIENTBITSET0_VEH0_STUCK)
						IF NOT IS_BIT_SET(serverBD.iVehStuckSinceCreatedBitset, 0)
							IF IS_SERVER_BIT0_SET(eSERVERBITSET0_VEHICLE_0_ENTERED_ONCE) 
							OR NOT bLAND_VARIATION()
								bVehStuck = TRUE
								PRINTLN("[EXEC1] [CTRB_SELL] participant ", iParticipant, " has set eCLIENTBITSET0_VEH0_STUCK. PlayerId = ", iPlayer, " player ", GET_PLAYER_NAME(playerID))
							ELSE
								SET_BIT(serverBD.iVehStuckSinceCreatedBitset, 0)
								PRINTLN("[EXEC1] [CTRB_SELL] participant ", iParticipant, " has set eCLIENTBITSET0_VEH0_STUCK but timeAllSellVehCreated not expired. PlayerId = ", iPlayer, " player ", GET_PLAYER_NAME(playerID))
							ENDIF
						ENDIF
					ELIF IS_CLIENT_BIT0_SET(participantID, eCLIENTBITSET0_VEH1_STUCK)
						IF NOT IS_BIT_SET(serverBD.iVehStuckSinceCreatedBitset, 1)
							IF IS_SERVER_BIT0_SET(eSERVERBITSET0_VEHICLE_1_ENTERED_ONCE) 
							OR NOT bLAND_VARIATION()
								bVehStuck = TRUE
								PRINTLN("[EXEC1] [CTRB_SELL] participant ", iParticipant, " has set eCLIENTBITSET0_VEH1_STUCK. PlayerId = ", iPlayer, " player ", GET_PLAYER_NAME(playerID))
							ELSE
								SET_BIT(serverBD.iVehStuckSinceCreatedBitset, 1)
								PRINTLN("[EXEC1] [CTRB_SELL] participant ", iParticipant, " has set eCLIENTBITSET0_VEH0_STUCK but timeAllSellVehCreated not expired. PlayerId = ", iPlayer, " player ", GET_PLAYER_NAME(playerID))
							ENDIF
						ENDIF
					ELIF IS_CLIENT_BIT0_SET(participantID, eCLIENTBITSET0_VEH2_STUCK)
						IF NOT IS_BIT_SET(serverBD.iVehStuckSinceCreatedBitset, 2)
							IF IS_SERVER_BIT0_SET(eSERVERBITSET0_VEHICLE_2_ENTERED_ONCE) 
							OR NOT bLAND_VARIATION()
								bVehStuck = TRUE
								PRINTLN("[EXEC1] [CTRB_SELL] participant ", iParticipant, " has set eCLIENTBITSET0_VEH2_STUCK. PlayerId = ", iPlayer, " player ", GET_PLAYER_NAME(playerID))
							ELSE
								SET_BIT(serverBD.iVehStuckSinceCreatedBitset, 2)
								PRINTLN("[EXEC1] [CTRB_SELL] participant ", iParticipant, " has set eCLIENTBITSET0_VEH0_STUCK but timeAllSellVehCreated not expired. PlayerId = ", iPlayer, " player ", GET_PLAYER_NAME(playerID))
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT IS_CLIENT_BIT0_SET(participantID, eCLIENTBITSET0_COMPLETED_REWARDS)
					bAllCompletedRewards = FALSE
				ENDIF
					
				IF NOT IS_SERVER_BIT0_SET(eSERVERBITSET0_SEA_DEFEND_PEDS_SPOOKED)
					IF IS_CLIENT_BIT0_SET(participantID, eCLIENTBITSET0_SEA_DEFEND_SPOOKED_PEDS)
						PRINTLN("[EXEC1] [CTRB_SELL] participant ", iParticipant, " has spooked sea defend peds. PlayerId = ", iPlayer)
						SET_SERVER_BIT0(eSERVERBITSET0_SEA_DEFEND_PEDS_SPOOKED)
					ENDIF
				ENDIF
				
				IF NOT IS_SERVER_BIT1_SET(eSERVERBITSET1_DESTROYED_EVENT_SENT)
					IF IS_CLIENT_BIT0_SET(participantID, eCLIENTBITSET0_SERVER_SHOULD_SEND_DESTROYED_EVENT)
					AND playerBD[iParticipant].damagerPlayer != INVALID_PLAYER_INDEX()
						SEND_CONTRABAND_DESTROYED_EVENT(playerBD[iParticipant].damagerPlayer)
						PRINTLN("[EXEC1] [CTRB_SELL] participant ", iParticipant, " Telling server to send destroyed event. PlayerId = ", iPlayer)
						SET_SERVER_BIT1(eSERVERBITSET1_DESTROYED_EVENT_SENT)
					ENDIF
				ENDIF
				
				IF NOT HAS_AIR_CLEAR_AREA_PLANE_LANDED(0)
					IF IS_CLIENT_BIT0_SET(participantID, eCLIENTBITSET0_PLANE_0_LANDED)
						PRINTLN("[EXEC1] [CTRB_SELL] participant ", iParticipant, " has landed the plane. PlayerId = ", iPlayer)
						SET_SERVER_BIT0(eSERVERBITSET0_PLANE_0_LANDED)
						SET_SELL_VEH_HAS_DELIVERED_CONTRABAND(0)
					ENDIF
				ENDIF
				
				IF NOT HAS_AIR_CLEAR_AREA_PLANE_LANDED(1)
					IF IS_CLIENT_BIT0_SET(participantID, eCLIENTBITSET0_PLANE_1_LANDED)
						PRINTLN("[EXEC1] [CTRB_SELL] participant ", iParticipant, " has landed the plane. PlayerId = ", iPlayer)
						SET_SERVER_BIT0(eSERVERBITSET0_PLANE_1_LANDED)
						SET_SELL_VEH_HAS_DELIVERED_CONTRABAND(1)
					ENDIF
				ENDIF
				
				IF NOT HAS_AIR_CLEAR_AREA_PLANE_LANDED(2)
					IF IS_CLIENT_BIT0_SET(participantID, eCLIENTBITSET0_PLANE_2_LANDED)
						PRINTLN("[EXEC1] [CTRB_SELL] participant ", iParticipant, " has landed the plane. PlayerId = ", iPlayer)
						SET_SERVER_BIT0(eSERVERBITSET0_PLANE_2_LANDED)
						SET_SELL_VEH_HAS_DELIVERED_CONTRABAND(2)
					ENDIF
				ENDIF
				
				IF NOT IS_SERVER_BIT0_SET(eSERVERBITSET0_SEA_ATTACK_START_VEH_SPAWN)
					IF IS_CLIENT_BIT0_SET(participantID, eCLIENTBITSET0_SEA_ATTACK_SPAWN_DISTANCE_REACHED)
						PRINTLN("[EXEC1] [CTRB_SELL] participant ", iParticipant, " reporting that sell veh is > 100m from start. PlayerId = ", iPlayer)
						SET_SERVER_BIT0(eSERVERBITSET0_SEA_ATTACK_START_VEH_SPAWN)
					ENDIF					
				ENDIF
				
				IF NOT IS_SERVER_BIT0_SET(eSERVERBITSET0_CAN_LAND_GANG_CHASE_SPAWN)
					IF IS_CLIENT_BIT0_SET(participantID, eCLIENTBITSET0_LAND_ATTACK_SPAWN_DISTANCE_REACHED)
						INT iContraVehToAttack
						iContraVehToAttack = GET_CONTRABAND_VEHICLE_TO_ATTACK()
						IF iContraVehToAttack > -1
							serverBD.iRandVehToAttack = iContraVehToAttack
							PRINTLN("[EXEC1] [CTRB_SELL] participant ", iParticipant, " reporting that sell veh is > 100m from start. PlayerId = ", iPlayer, " set serverBD.iRandVehToAttack = ", serverBD.iRandVehToAttack)
							SET_SERVER_BIT0(eSERVERBITSET0_CAN_LAND_GANG_CHASE_SPAWN)
						ELSE
							PRINTLN("[EXEC1] [CTRB_SELL] participant ", iParticipant, " reporting that sell veh is > 100m from start. PlayerId = ", iPlayer, " but can't find contraband vehicle to attack!")						
						ENDIF
					ENDIF					
				ENDIF
				
				IF NOT IS_SERVER_BIT0_SET(eSERVERBITSET0_SEA_DEFEND_PLAYER_IN_RANGE)
					IF IS_CLIENT_BIT0_SET(participantID, eCLIENTBITSET0_SEA_DEFEND_WITHIN_RANGE)
						PRINTLN("[EXEC1] [CTRB_SELL] participant ", iParticipant, " is <= 100m from peds/veh. PlayerId = ", iPlayer)
						SET_SERVER_BIT0(eSERVERBITSET0_SEA_DEFEND_PLAYER_IN_RANGE)
					ENDIF
				ENDIF
				
				IF NOT IS_SERVER_BIT1_SET(eSERVERBITSET1_CONTRABAND_REMOVED_AT_END)
					IF IS_CLIENT_BIT0_SET(participantID, eCLIENTBITSET0_CONTRABAND_ADDED_AT_END)
						PRINTLN("[EXEC1] [CTRB_SELL] boss participant ", iParticipant, " has transfered contraband to warehouse. PlayerId = ", iPlayer)
						SET_SERVER_BIT1(eSERVERBITSET1_CONTRABAND_REMOVED_AT_END)
					ENDIF
				ENDIF
				
				IF NOT IS_SERVER_BIT1_SET(eSERVERBITSET1_LAND_TRACKIFY_SHOULD_BLIP_DROP_OFFS)
					IF IS_CLIENT_BIT0_SET(participantID, eCLIENTBITSET0_LAND_TRACKIFY_SELL_VEH_NEAR_DROP_OFF)
						PRINTLN("[EXEC1] [CTRB_SELL] participant ", iParticipant, " reporting that land trackify sell veh is near drop off. PlayerId = ", iPlayer)
						SET_SERVER_BIT1(eSERVERBITSET1_LAND_TRACKIFY_SHOULD_BLIP_DROP_OFFS)
					ENDIF
				ENDIF
				
				IF bAIR_ATTACK()
					iVeh = 0
					REPEAT ciMAX_CONTRABAND_PED_VEHICLES iVeh
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPedVeh[iVeh].netIndex)
							IF NOT IS_BIT_SET(serverBD.iAirAttackHeliSpooked, iVeh)	
								IF IS_BIT_SET(playerBD[iParticipant].iSpookedAirAttackHeliBitset, iVeh)
									SET_BIT(serverBD.iAirAttackHeliSpooked, iVeh)	
									PRINTLN("[EXEC1] [CTRB_SELL] Set iAirAttackHeliSpooked for AI veh ", iVeh, " as this player has spooked them ", GET_PLAYER_NAME(playerID))
								ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
				ENDIF
				
				IF bLAND_DEFEND()
					iVeh = 0
					REPEAT GET_NUM_ATTACK_VEHICLES_FOR_VARIATION() iVeh
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPedVeh[iVeh].netIndex)
							IF NOT IS_BIT_SET(serverBD.iGangChaseStuckBitset, iVeh)
								IF GET_VEHICLE_STATE(iVeh) = eVEHICLESTATE_DRIVEABLE
									IF IS_BIT_SET(playerBD[iParticipant].iGangChaseStuckBitset, iVeh)
										SET_BIT(serverBD.iGangChaseStuckBitset, iVeh)
										PRINTLN("[EXEC1] [CTRB_SELL] Set gang chase stuck veh ", iVeh, " as this player thinks they're stuck ", GET_PLAYER_NAME(playerID))
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
				ENDIF
				
				#IF IS_DEBUG_BUILD
				IF serverBD.iSPassPart = (-1)
				AND serverBD.iFFailPart = (-1)
					IF IS_CLIENT_DEBUG_BIT0_SET(participantID, eCLIENTDEBUGBITSET0_I_HAVE_S_PASSED)
						serverBD.iSPassPart = iParticipant
						PRINTLN("[EXEC1] [CTRB_SELL] participant ", iParticipant, " has S passed. Setting serverBD.iSPassPart = ", iParticipant)
					ELIF IS_CLIENT_DEBUG_BIT0_SET(participantID, eCLIENTDEBUGBITSET0_I_HAVE_F_FAILED)
						serverBD.iFFailPart = iParticipant
						PRINTLN("[EXEC1] [CTRB_SELL] participant ", iParticipant, " has F failed. Setting serverBD.iFFailPart = ", iParticipant)
					ENDIF
				ENDIF
				#ENDIF
				
			ENDIF
			
		ENDIF
		
	ENDREPEAT
	
	// Store driver 1
	IF playerDriver[0] <> serverBD.playerDriver[0]
		serverBD.playerDriver[0] = playerDriver[0]
	ENDIF
	
	// Store driver 2
	IF playerDriver[1] <> serverBD.playerDriver[1]
		serverBD.playerDriver[1] = playerDriver[1]
	ENDIF
	
	// Store driver 3
	IF playerDriver[2] <> serverBD.playerDriver[2]
		serverBD.playerDriver[2] = playerDriver[2]
	ENDIF
	
	IF bVehOccupied[0] 
		IF NOT IS_SERVER_BIT0_SET(eSERVERBITSET0_VEHICLE_0_ENTERED_ONCE)
			START_BLIP_DELAY_TIMER()
			SET_SERVER_BIT0(eSERVERBITSET0_VEHICLE_0_ENTERED_ONCE)
			PRINTLN("[EXEC1] [CTRB_SELL] [BITS] SET_BIT, eSERVERBITSET0_VEHICLE_0_ENTERED_ONCE ")
		ENDIF
		IF NOT IS_SERVER_BIT0_SET(eSERVERBITSET0_SELL_VEH_0_OCCUPIED)
			SET_SERVER_BIT0(eSERVERBITSET0_SELL_VEH_0_OCCUPIED)
			PRINTLN("[EXEC1] [CTRB_SELL] [BITS] SET_BIT, eSERVERBITSET0_SELL_VEH_0_OCCUPIED ")
		ENDIF
	ELSE
		IF IS_SERVER_BIT0_SET(eSERVERBITSET0_SELL_VEH_0_OCCUPIED)
			CLEAR_SERVER_BIT0(eSERVERBITSET0_SELL_VEH_0_OCCUPIED)
			PRINTLN("[EXEC1] [CTRB_SELL] [BITS] CLEAR_BIT, eSERVERBITSET0_SELL_VEH_0_OCCUPIED ")
		ENDIF
	ENDIF
	
	IF bVehOccupied[1] 
		IF NOT IS_SERVER_BIT0_SET(eSERVERBITSET0_VEHICLE_1_ENTERED_ONCE)
			START_BLIP_DELAY_TIMER()
			SET_SERVER_BIT0(eSERVERBITSET0_VEHICLE_1_ENTERED_ONCE)
			PRINTLN("[EXEC1] [CTRB_SELL] [BITS] SET_BIT, eSERVERBITSET0_VEHICLE_1_ENTERED_ONCE ")
		ENDIF
		IF NOT IS_SERVER_BIT0_SET(eSERVERBITSET0_SELL_VEH_1_OCCUPIED)
			SET_SERVER_BIT0(eSERVERBITSET0_SELL_VEH_1_OCCUPIED)
			PRINTLN("[EXEC1] [CTRB_SELL] [BITS] SET_BIT, eSERVERBITSET0_SELL_VEH_1_OCCUPIED ")
		ENDIF
	ELSE
		IF IS_SERVER_BIT0_SET(eSERVERBITSET0_SELL_VEH_1_OCCUPIED)
			CLEAR_SERVER_BIT0(eSERVERBITSET0_SELL_VEH_1_OCCUPIED)
			PRINTLN("[EXEC1] [CTRB_SELL] [BITS] CLEAR_BIT, eSERVERBITSET0_SELL_VEH_1_OCCUPIED ")
		ENDIF
	ENDIF
	
	IF bVehOccupied[2] 
		IF NOT IS_SERVER_BIT0_SET(eSERVERBITSET0_VEHICLE_2_ENTERED_ONCE)
			START_BLIP_DELAY_TIMER()
			SET_SERVER_BIT0(eSERVERBITSET0_VEHICLE_2_ENTERED_ONCE)
			PRINTLN("[EXEC1] [CTRB_SELL] [BITS] SET_BIT, eSERVERBITSET0_VEHICLE_2_ENTERED_ONCE ")
		ENDIF
		IF NOT IS_SERVER_BIT0_SET(eSERVERBITSET0_SELL_VEH_2_OCCUPIED)
			SET_SERVER_BIT0(eSERVERBITSET0_SELL_VEH_2_OCCUPIED)
			PRINTLN("[EXEC1] [CTRB_SELL] [BITS] SET_BIT, eSERVERBITSET0_SELL_VEH_2_OCCUPIED ")
		ENDIF
	ELSE
		IF IS_SERVER_BIT0_SET(eSERVERBITSET0_SELL_VEH_2_OCCUPIED)
			CLEAR_SERVER_BIT0(eSERVERBITSET0_SELL_VEH_2_OCCUPIED)
			PRINTLN("[EXEC1] [CTRB_SELL] [BITS] CLEAR_BIT, eSERVERBITSET0_SELL_VEH_2_OCCUPIED ")
		ENDIF
	ENDIF
	
	IF bVehStuck
		SET_SERVER_BIT0(eSERVERBITSET0_VEHICLE_STUCK)
	ENDIF
	
	IF bPlayersFinished
		IF NOT IS_SERVER_BIT0_SET(eSERVERBITSET0_PLAYERS_FINISHED)
			PRINTLN("[EXEC1] [CTRB_SELL] [BITS] SET_BIT, eSERVERBITSET0_PLAYERS_FINISHED ")
			SET_SERVER_BIT0(eSERVERBITSET0_PLAYERS_FINISHED)
		ENDIF
	ENDIF
	
	IF bAllCompletedRewards
		IF NOT IS_SERVER_BIT0_SET(eSERVERBITSET0_ALL_PARTICIPANTS_COMPLETED_REWARDS)
			SET_SERVER_BIT0(eSERVERBITSET0_ALL_PARTICIPANTS_COMPLETED_REWARDS)
		ENDIF
	ELSE
		IF IS_SERVER_BIT0_SET(eSERVERBITSET0_ALL_PARTICIPANTS_COMPLETED_REWARDS)
			CLEAR_SERVER_BIT0(eSERVERBITSET0_ALL_PARTICIPANTS_COMPLETED_REWARDS)
		ENDIF
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD

TEXT_WIDGET_ID twIdServerGameState
TEXT_WIDGET_ID twIdClientGameState[GB_MAX_GANG_SIZE_INCLUDING_BOSS_EXEC]
TEXT_WIDGET_ID twIdClientName[GB_MAX_GANG_SIZE_INCLUDING_BOSS_EXEC]
TEXT_WIDGET_ID twIdModeState
BOOL bTerminateScriptNow
FLOAT xOffset
FLOAT yOffset
FLOAT zOffset
BOOL bShowOffsets = FALSE
BOOL bDrawSpawnAreaMarkers = FALSE

PROC UPDATE_SERVER_GAME_STATE_WIDGET()
	SET_CONTENTS_OF_TEXT_WIDGET(twIdServerGameState, GET_GAME_STATE_NAME(GET_SERVER_GAME_STATE()))
	SET_CONTENTS_OF_TEXT_WIDGET(twIdModeState, GET_MODE_STATE_NAME(GET_MODE_STATE()))
ENDPROC

PROC UPDATE_CLIENT_GAME_STATE_WIDGET(INT iParticipant)
	
	STRING strName
	
	SET_CONTENTS_OF_TEXT_WIDGET(twIdClientGameState[iParticipant], "NOT ACTIVE")
	SET_CONTENTS_OF_TEXT_WIDGET(twIdClientName[iParticipant], "NOT ACTIVE")
	
	IF IS_BIT_SET(iParticipantActiveBitset, iParticipant)
		IF IS_BIT_SET(iPlayerOkBitset, iParticipant)
			SET_CONTENTS_OF_TEXT_WIDGET(twIdClientGameState[iParticipant], GET_GAME_STATE_NAME(GET_CLIENT_GAME_STATE(iParticipant)))
			strName = GET_PLAYER_NAME(INT_TO_NATIVE(PLAYER_INDEX, iParticipant))
			SET_CONTENTS_OF_TEXT_WIDGET(twIdClientName[iParticipant], strName)
		ENDIF
	ENDIF
	
ENDPROC

PROC CREATE_WIDGETS()
	
	INT iParticipant
	TEXT_LABEL_63 tl63Temp
	
	START_WIDGET_GROUP("GB_SELL ")
		ADD_WIDGET_INT_READ_ONLY("serverBD.iVehicleCountDeliveredAllContraband", serverBD.iVehicleCountDeliveredAllContraband)
		ADD_WIDGET_BOOL("Locally terminate Script Now", bTerminateScriptNow)
		ADD_WIDGET_BOOL("Fake End Of Mode Timer", serverBD.bFakeEndOfModeTimer)
		ADD_WIDGET_INT_READ_ONLY("serverBD.iStartingContraband", serverBD.iStartingContraband)
		ADD_WIDGET_INT_READ_ONLY("serverBD.iPlayerRepair", serverBD.iPlayerRepair)
		ADD_WIDGET_INT_READ_ONLY("iWantedStored", sLocaldata.iWantedStored)
		ADD_WIDGET_INT_READ_ONLY("iMissionWarehouse", serverBD.iStartingWarehouse)
		ADD_WIDGET_INT_READ_ONLY("iRoute", serverBD.iRoute)
		ADD_WIDGET_INT_READ_ONLY("serverBD.iSting", serverBD.iSting)
		ADD_WIDGET_INT_READ_ONLY("serverBD.iDroppedOffCount", serverBD.iDroppedOffCount)
		ADD_WIDGET_INT_READ_ONLY("serverBD.iClosestDropOff", serverBD.iClosestDropOff[0])
		ADD_WIDGET_INT_READ_ONLY("iNumCrates", serverBD.iNumCrates[0])
		ADD_WIDGET_INT_READ_ONLY("serverBD.iBreakdownCount", serverBD.iBreakdownCount)
		ADD_WIDGET_INT_READ_ONLY("sLocaldata.iRepairProgress", sLocaldata.iRepairProgress)
		ADD_WIDGET_FLOAT_READ_ONLY("serverBD.fStoredBreakdownDistance", serverBD.fStoredBreakdownDistance)
		ADD_WIDGET_FLOAT_READ_ONLY("serverBD.fDistDrop", serverBD.fDistDrop)
		ADD_WIDGET_INT_SLIDER("tiAlphaMarker", tiAlphaMarker, 0, 255, 1)
		ADD_WIDGET_FLOAT_SLIDER("tfScale", tfScale, -100, 255, 1)
		ADD_WIDGET_FLOAT_READ_ONLY("serverBD.fDistAboveGround", serverBD.fDistAboveGround)
		ADD_WIDGET_FLOAT_READ_ONLY("serverBD.fDistGroundHeight", serverBD.fDistGroundHeight)
//		ADD_WIDGET_FLOAT_SLIDER("Speed modifier", fNewSpeedModifier, 0, 10000, 25)
		
		START_WIDGET_GROUP("Tug Offset Debugging (draws marker)")
			ADD_WIDGET_BOOL("Show offet markers", bShowOffsets)
			ADD_WIDGET_FLOAT_SLIDER("Offset x", xOffset, LOWEST_INT, HIGHEST_INT, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("Offset y", yOffset, LOWEST_INT, HIGHEST_INT, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("Offset z", zOffset, LOWEST_INT, HIGHEST_INT, 0.01)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Sea Variation Spawn Markers")
			ADD_WIDGET_BOOL("Draw Markers at central spawn points", bDrawSpawnAreaMarkers)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("REPAIR")
			ADD_WIDGET_FLOAT_SLIDER("tfRepairY", tfRepairY, -100, 255, 1)
			ADD_WIDGET_FLOAT_SLIDER("tfMarkerY", tfMarkerY, -100, 255, 1)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Game State")
			START_WIDGET_GROUP("Server")
				twIdServerGameState = ADD_TEXT_WIDGET("Game State")
				twIdModeState = ADD_TEXT_WIDGET("Mode State")
				UPDATE_SERVER_GAME_STATE_WIDGET()
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Client")
				REPEAT GB_GET_MAX_GANG_SIZE_INCLUDING_BOSS() iParticipant
					tl63Temp = ""
					tl63Temp +="Part "
					tl63Temp += iParticipant
					tl63Temp += " Name"
					twIdClientName[iParticipant] = ADD_TEXT_WIDGET(tl63Temp)
					tl63Temp = ""
					tl63Temp += "Part "
					tl63Temp += iParticipant
					tl63Temp += " Game State"
					twIdClientGameState[iParticipant] = ADD_TEXT_WIDGET(tl63Temp)
					tl63Temp = ""
					tl63Temp +="Part "
					tl63Temp += iParticipant
					tl63Temp += " Player ID"
					ADD_WIDGET_INT_READ_ONLY(tl63Temp, iParticipant)
					tl63Temp = ""
					tl63Temp +="Part "
					tl63Temp += iParticipant
					tl63Temp += " Player stored Part ID"
					ADD_WIDGET_INT_READ_ONLY(tl63Temp, iParticipant)
					UPDATE_CLIENT_GAME_STATE_WIDGET(iParticipant)
				ENDREPEAT
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
	
	PRINTLN("[EXEC1] [CTRB_SELL] - created widgets.")
	
ENDPROC		

PROC UPDATE_WIDGETS()
	
	INT iPartcount
	INT r,g,b,a
	
	UPDATE_SERVER_GAME_STATE_WIDGET()
	
	REPEAT GB_GET_MAX_GANG_SIZE_INCLUDING_BOSS() iPartcount
		UPDATE_CLIENT_GAME_STATE_WIDGET(iPartcount)
	ENDREPEAT
	
	IF NOT IS_LOCAL_DEBUG_BIT0_SET(eLOCALDEBUGBITSET0_TERMINATE_SCRIPT_NOW)
		IF bTerminateScriptNow
			PRINTLN("[EXEC1] [CTRB_SELL] widget bTerminateScriptNow = TRUE.")
			SET_LOCAL_DEBUG_BIT0(eLOCALDEBUGBITSET0_TERMINATE_SCRIPT_NOW)
		ENDIF
	ENDIF
	
	INT i
//	REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
//		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[i])
//			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niVeh[i])
//			AND IS_ENTITY_ALIVE(VEH_ID(i))
//				IF fSpeedModifier != fNewSpeedModifier
//					MODIFY_VEHICLE_TOP_SPEED(VEH_ID(i), fNewSpeedModifier)
//					fSpeedModifier = fNewSpeedModifier
//				ENDIF
//			ELSE
//				IF fNewSpeedModifier != fSpeedModifier
//					fNewSpeedModifier = fSpeedModifier
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDREPEAT
	
	IF bDrawSpawnAreaMarkers
		GET_HUD_COLOUR(HUD_COLOUR_YELLOW, r, g, b, a)
		DRAW_MARKER(MARKER_CYLINDER, GET_SPAWN_AREA_CENTRAL_COORDS(0), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, <<100, 100, 50>>, r, g, b, 255)
		DRAW_MARKER(MARKER_CYLINDER, GET_SPAWN_AREA_CENTRAL_COORDS(1), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, <<100, 100, 50>>, r, g, b, 255)
		DRAW_MARKER(MARKER_CYLINDER, GET_SPAWN_AREA_CENTRAL_COORDS(2), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, <<100, 100, 50>>, r, g, b, 255)
	ENDIF
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF bShowOffsets
			i = 0
			VECTOR coords
			REPEAT GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE() i
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[i])
					coords = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(VEH_ID(i)), GET_ENTITY_HEADING(VEH_ID(i)), <<xOffset, yOffset, zOffset>>)
					GET_HUD_COLOUR(HUD_COLOUR_YELLOW, r, g, b, a)
					DRAW_MARKER(MARKER_CYLINDER, coords, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, <<0.5, 0.5, 0.2>>, r, g, b)
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
	
ENDPROC

#ENDIF

PROC UPDATE_FOCUS_PARTICIPANT()

	// Save out the current focus participant. Stays at -1 if not the local player or not spectating a script participant.
	iFocusParticipant = (-1)
	
	IF IS_BIT_SET(iPlayerOkBitset, NATIVE_TO_INT(PLAYER_ID()))
		IF IS_BIT_SET(iParticipantActiveBitset, PARTICIPANT_ID_TO_INT())
			IF NOT IS_PLAYER_SPECTATING(PLAYER_ID())
				iFocusParticipant = PARTICIPANT_ID_TO_INT()
			ELSE
				IF IS_A_SPECTATOR_CAM_RUNNING()
				AND NOT IS_SPECTATOR_HUD_HIDDEN()
					PED_INDEX specTargetPed = GET_SPECTATOR_SELECTED_PED()
					IF IS_PED_A_PLAYER(specTargetPed)
						PLAYER_INDEX specPlayerTemp = NETWORK_GET_PLAYER_INDEX_FROM_PED(specTargetPed)
						IF NETWORK_IS_PLAYER_A_PARTICIPANT(specPlayerTemp)
							PARTICIPANT_INDEX specTargetParticipant = NETWORK_GET_PARTICIPANT_INDEX(specPlayerTemp)
							iFocusParticipant = NATIVE_TO_INT(specTargetParticipant)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC INIT_BUYER_ID()
	
	IF serverBD.iBuyerID = -1
		serverBD.iBuyerID = g_iContraSellMissionBuyerID
		PRINTLN("[EXEC1] [CTRB_SELL], [INIT_SERVER] [INIT_BUYER_ID] serverBD.iBuyerID = ", serverBD.iBuyerID)
	ENDIF
ENDPROC

PROC INIT_NUMBER_OF_CRATES_TO_SELL()
	IF serverBD.iCratesToSell = -1
		serverBD.iCratesToSell = g_iContraSellMissionCratesToSell
		PRINTLN("[EXEC1] [CTRB_SELL], [INIT_SERVER] [INIT_NUMBER_OF_CRATES_TO_SELL] serverBD.iCratesToSell = ", serverBD.iCratesToSell)
	ENDIF
ENDPROC

FUNC BOOL INIT_SERVER()
	INIT_BUYER_ID()
	
	INIT_NUMBER_OF_CRATES_TO_SELL()
	
	IF SERVER_PICK_VARIATION()
	AND SERVER_SET_START_WAREHOUSE()
	AND SERVER_SET_SHIPMENT_TYPE()
	AND SERVER_SET_START_POS()
	AND SERVER_SET_ROUTE()
		SERVER_SET_STING()
		SERVER_SET_SEA_VARIATION_SAFE_WARP_LOCATION()
		IF SERVER_CHOOSE_DROP_OFFS()
			IF CHOOSE_DROP_OFF_AI_VEHS_SHOULD_DEFEND()
				INIT_GANG_CHASE_MODELS_FOR_VARIATION()
				
				IF DOES_SHIPMENT_TYPE_HAVE_MULTIPLE_VEHICLES()
					serverBD.iRandVehToAttack = GET_RANDOM_INT_IN_RANGE(0, GET_NUM_SELL_VEHS_FOR_SHIPMENT_TYPE())
					PRINTLN("[EXEC1] [CTRB_SELL], INIT_SERVER - Rand veh to attack is: ", serverBD.iRandVehToAttack)
				ENDIF
				
				PRINTLN("[EXEC1] [CTRB_SELL] INIT_SERVER DONE ")
				
				serverBD.piLaunchBoss = PLAYER_ID()
				
				IF bLAND_DAMAGE()
					serverBD.iBonus = g_sMPTunables.iexec_sell_nodamage_init_bonus
				ENDIF
				
				INIT_IDEAL_SPAWN_COORDS()
				RETURN TRUE
			ELSE
				PRINTLN("[EXEC1] [CTRB_SELL] INIT_SERVER WAITING FOR CHOOSE_DROP_OFF_AI_VEHS_SHOULD_DEFEND")
			ENDIF
		ELSE
			PRINTLN("[EXEC1] [CTRB_SELL] INIT_SERVER WAITING FOR SERVER_CHOOSE_DROP_OFFS")
		ENDIF
	ELSE
		PRINTLN("[EXEC1] [CTRB_SELL] INIT_SERVER WAITING FOR SERVER_PICK_VARIATION ETC")
	ENDIF
	
	RETURN FALSE
ENDFUNC

//----------------------
//	MAIN SCRIPT
//----------------------

 // FEATURE_EXECUTIVE

SCRIPT(MP_MISSION_DATA missionScriptArgs)


	PRINTLN("[EXEC1] [CTRB_SELL] START")
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE)
			
			IF NOT PROCESS_PRE_GAME(missionScriptArgs)
				PRINTLN("[EXEC1] [CTRB_SELL] TEST_BROKE1 ")
				EXECUTE_COMMON_END_TELEMTRY(FALSE, TRUE)
				SCRIPT_CLEANUP()
			ENDIF
			// Setup widgets. Make sure this is done after PROCESS_PRE_GAME()
			#IF IS_DEBUG_BUILD
				CREATE_WIDGETS()
			#ENDIF
		ELSE
			PRINTLN("[EXEC1] [CTRB_SELL] TEST_BROKE2 ")
			PRINTLN("[EXEC1] [CTRB_SELL] calling SCRIPT_CLEANUP() - IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE) = FALSE")
			SCRIPT_CLEANUP()
			
		ENDIF
	ELSE
		PRINTLN("[EXEC1] [CTRB_SELL] TEST_BROKE3 ")
		PRINTLN("[EXEC1] [CTRB_SELL] calling SCRIPT_CLEANUP() - NETWORK_IS_GAME_IN_PROGRESS() = FALSE")
		EXECUTE_COMMON_END_TELEMTRY(FALSE, TRUE)
		SCRIPT_CLEANUP()
		
	ENDIF
	
	bGangBossOnStart = GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
	
	WHILE (TRUE)
	
		MP_LOOP_WAIT_ZERO()
		
		#IF IS_DEBUG_BUILD
		IF IS_LOCAL_DEBUG_BIT0_SET(eLOCALDEBUGBITSET0_TERMINATE_SCRIPT_NOW)
			PRINTLN("[EXEC1] [CTRB_SELL] TEST_BROKE4 ")
			PRINTLN("[EXEC1] [CTRB_SELL]  eLOCALDEBUGBITSET0_TERMINATE_SCRIPT_NOW is set")
			EXECUTE_COMMON_END_TELEMTRY(FALSE, TRUE)
			SCRIPT_CLEANUP()
		ENDIF
		#ENDIF

		IF GB_SHOULD_QUIT_ACTIVE_BOSS_MISSION()
			PRINTLN("[EXEC1] [CTRB_SELL] TEST_BROKE6 ")
			PRINTLN("[EXEC1] [CTRB_SELL]  - GB_SHOULD_QUIT_ACTIVE_BOSS_MISSION = TRUE")
			EXECUTE_COMMON_END_TELEMTRY(FALSE, TRUE)
			SCRIPT_CLEANUP()
		ENDIF
		
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			PRINTLN("[EXEC1] [CTRB_SELL] TEST_BROKE7 ")
			PRINTLN("[EXEC1] [CTRB_SELL] SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE")
			EXECUTE_COMMON_END_TELEMTRY(FALSE, TRUE)
			SCRIPT_CLEANUP(TRUE)
		ENDIF
		
		UPDATE_FOCUS_PARTICIPANT()
		PROCESS_EVENTS()
		
		//GB_MAINTAIN_SPECTATE(sSpecVars)

		// Deal with the debug
		#IF IS_DEBUG_BUILD		
			UPDATE_WIDGETS()
		#ENDIF
		
		SWITCH(GET_CLIENT_GAME_STATE(PARTICIPANT_ID_TO_INT()))
		
			CASE GAME_STATE_INIT
				IF GET_SERVER_GAME_STATE() = GAME_STATE_RUNNING

					IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
					AND NOT IS_INITIAL_CONTRABAND_REMOVAL_DISABLED()
					AND NOT IS_SERVER_BIT1_SET(eSERVERBITSET1_LAUNCHED_WITH_NO_CONTRABAND)
					AND NOT GET_DOES_BUYER_ONLY_WANT_SPECIAL_ITEMS()
					AND GET_CONTRABAND_UNITS_TO_SELL_FROM_BUYER() > 1 // Are we selling enough contraband to give away at start
						HANDLE_CONTRABAND_REMOVAL(eREMOVE_INITIAL)
						
						IF eResult = CONTRABAND_TRANSACTION_STATE_SUCCESS
						OR eResult = CONTRABAND_TRANSACTION_STATE_FAILED

							IF INIT_CLIENT()

								SET_CLIENT_GAME_STATE(PARTICIPANT_ID_TO_INT(),GAME_STATE_RUNNING)
								//Call the set up if it's not already been done
								SET_AUDIO_FLAGS_ON_MUSIC_START()
							ENDIF
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
						IF NOT GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
							PRINTLN("[EXEC1] [CTRB_SELL] - No initial contra taken - Local player is a goon")
						ENDIF
						
						IF IS_INITIAL_CONTRABAND_REMOVAL_DISABLED()
							PRINTLN("[EXEC1] [CTRB_SELL] - No initial contra taken - Initial contraband removal is disabled via tunable")
						ENDIF
						
						IF IS_SERVER_BIT1_SET(eSERVERBITSET1_LAUNCHED_WITH_NO_CONTRABAND)
							PRINTLN("[EXEC1] [CTRB_SELL] - No initial contra taken - Player has launched with no contraband")
						ENDIF
						
						IF NOT GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
							PRINTLN("[EXEC1] [CTRB_SELL] - No initial contra taken - Player is not boss of a GANG")
						ENDIF
						
						IF GET_DOES_BUYER_ONLY_WANT_SPECIAL_ITEMS()
							PRINTLN("[EXEC1] [CTRB_SELL] - No initial contra taken - Buyer only wants special items")
						ENDIF
						
						IF GET_CONTRABAND_UNITS_TO_SELL_FROM_BUYER() <= 1
							PRINTLN("[EXEC1] [CTRB_SELL] - No initial contra taken - GET_CONTRABAND_UNITS_TO_SELL_FROM_BUYER = ", GET_CONTRABAND_UNITS_TO_SELL_FROM_BUYER())
						ENDIF
						#ENDIF
					
						IF INIT_CLIENT()
							SET_CLIENT_GAME_STATE(PARTICIPANT_ID_TO_INT(),GAME_STATE_RUNNING)
							//Call the set up if it's not already been done
							SET_AUDIO_FLAGS_ON_MUSIC_START()
						ENDIF
					ENDIF
				ELIF GET_SERVER_GAME_STATE() = GAME_STATE_END
					SET_CLIENT_GAME_STATE(PARTICIPANT_ID_TO_INT(), GAME_STATE_END)
				ENDIF
			BREAK
			
			CASE GAME_STATE_RUNNING
				IF GET_SERVER_GAME_STATE() = GAME_STATE_RUNNING
				
					CLIENT_PROCESSING()
					
				ELIF GET_SERVER_GAME_STATE() = GAME_STATE_END
					SET_CLIENT_GAME_STATE(PARTICIPANT_ID_TO_INT(), GAME_STATE_END)
				ENDIF
			BREAK
			
			CASE GAME_STATE_END
				PRINTLN("[EXEC1] [CTRB_SELL] TEST_BROKE8 ")
				SCRIPT_CLEANUP()
			BREAK
			
		ENDSWITCH
	
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		
			IF GB_SHOULD_KILL_ACTIVE_BOSS_MISSION()
				PRINTLN("[EXEC1] [CTRB_SELL] TEST_BROKE5 ")
				PRINTLN("[EXEC1] [CTRB_SELL] - GB_SHOULD_KILL_ACTIVE_BOSS_MISSION = TRUE")
				SET_SERVER_GAME_STATE(GAME_STATE_END)
			ENDIF
		
			SWITCH(GET_SERVER_GAME_STATE())
			
				CASE GAME_STATE_INIT
					IF INIT_SERVER()
						SET_SERVER_GAME_STATE(GAME_STATE_RUNNING)
					ENDIF
				BREAK
				
				CASE GAME_STATE_RUNNING
					SERVER_PROCESSING()
					SERVER_PROCESS_PARTICIPANT_LOOP()
				BREAK
				
				CASE GAME_STATE_END
				BREAK
				
			ENDSWITCH
		
		ENDIF
		
	ENDWHILE

	
	
// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT


