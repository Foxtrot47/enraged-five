USING "globals.sch"
USING "rage_builtins.sch"
USING "Security_Contract/fm_content_security_contract_core.sch"

//////////////////////////
///    CONST 
//////////////////////////

CONST_INT SCRV_MISSION_ENTITY_ID							0

CONST_INT SCRV_PORTAL_ID_WALK_ENTRY							0
CONST_INT SCRV_PORTAL_ID_WALK_EXIT							1
CONST_INT SCRV_PORTAL_ID_VEHICLE_EXIT						2
CONST_INT SCRV_PORTAL_ID_VEHICLE_ENTRY						3
CONST_INT SCRV_PORTAL_ID_WALK_ENTRY_ALT						4
CONST_INT SCRV_PORTAL_ID_VEHICLE_ENTRY_ALT					5

CONST_FLOAT SCRV_LOCATE_MISSION_ENTITY_DISTANCE				5.0

CONST_INT SCRV_TICKER_DOOR_UNLOCKED							0
CONST_INT SCRV_TICKER_VEHICLE_COLLECTED						1

CONST_INT SCRV_HELP_TEXT_KEYPAD								0
CONST_INT SCRV_HELP_LOCATE_VEHICLE							1
CONST_INT SCRV_HELP_HUMANE_MID_POINT						2
CONST_INT SCRV_HELP_BOXVILLE_ENTER							3

CONST_INT SCRV_INTERACT_KEYPAD								0

CONST_INT SCRV_DOOR_OPEN_TIME								3000
CONST_INT SCRV_ALARM_TIMER									5000
CONST_INT SCRV_WAIT_TO_GO_KEYPAD_TIMER						10000

CONST_INT SCRV_ATTACHED_VEH_4								0
CONST_INT SCRV_ATTACHED_MISS_ENT							1

CONST_INT SCRV_CUTSCENE_ENTER								0

CONST_FLOAT SCRV_CUTSCENE_ENTER_END_PHASE					0.6

CONST_FLOAT SCRV_PED_BLIP_MAX_DISTANCE						20.0

CONST_INT SCRV_HUMANE_MID_POINT_TRIGGER						2

CONST_INT SCRV_MONKEY_AGITATED_TIMER						10000
CONST_INT SCRV_AMBUSH_DELAY_TIMER							5000

CONST_INT SCRV_INTERIOR_ROOM_KEY_IMPORT_EXPORT_WAREHOUSE		HASH("IntWareMed_Room")
CONST_INT SCRV_INTERIOR_ROOM_KEY_HANGAR							HASH("GtaMloRoom001")

/// DIALOGUES
CONST_INT SCRV_DIALOGUE_ID_OPENING							0

/// TEXT MESSAGES
CONST_INT SCRV_TEXT_MESSAGE_IMANI_TEXT						0
CONST_INT SCRV_TEXT_MESSAGE_HUMANE_MID_POINT				1

CONST_INT SCRV_MISSION_INTERIOR_ID							0
//////////////////////////
///    ENUMS 
//////////////////////////

ENUM SCRV_MODE_STATE
	eSCRVSTATE_GO_TO = 0,
	eSCRVSTATE_LOCATE_VEHICLE,
	eSCRVSTATE_UNLOCK_DOOR,
	eSCRVSTATE_COLLECT_MISSION_ENTITY,
	eSCRVSTATE_DELIVER_MISSION_ENTITY,
	eSCRVSTATE_END
ENDENUM

ENUM SCRV_CLIENT_MODE_STATE
	eSCRVSTATECLIENT_GO_TO = 0,
	eSCRVSTATECLIENT_LOCATE_VEHICLE,
	eSCRVSTATECLIENT_UNLOCK_DOOR,
	eSCRVSTATECLIENT_COLLECT_MISSION_ENTITY,
	eSCRVSTATECLIENT_DELIVER_MISSION_ENTITY,
	eSCRVSTATECLIENT_HELP_DELIVER_MISSION_ENTITY,
	eSCRVSTATECLIENT_RECOVER_MISSION_ENTITY,
	eSCRVSTATECLIENT_END 
ENDENUM

ENUM SCRV_TAGS
	SCRV_TAGS_MISSION_VEHICLE = 0,
	SCRV_TAGS_KEYPAD = 1,
	SCRV_TAGS_VEHICLE_1 = 2,
	SCRV_TAGS_AMBUSH_1 = 3,
	SCRV_TAGS_AMBUSH_2 = 4,
	SCRV_TAGS_VEHICLE_2 = 5,
	SCRV_TAGS_VEHICLE_3 = 6,
	SCRV_TAGS_VEHICLE_4 = 7,
	SCRV_TAGS_TITAN = 8,
	SCRV_TAGS_VEHICLE_5 = 9,
	SCRV_TAGS_SPAWN = 10,
	SCRV_TAGS_SPAWN_2 = 11,
	SCRV_TAGS_SPAWN_3 = 12,
	SCRV_TAGS_SPAWN_4 = 13,
	SCRV_TAGS_SPAWN_5 = 14,
	SCRV_TAGS_SPAWN_6 = 15,
	SCRV_TAGS_BANSHEE = 16,
	SCRV_TAGS_BANSHEE_2 = 17,
	SCRV_TAGS_SULTAN = 18,
	SCRV_TAGS_SULTAN_2 = 19
ENDENUM

ENUM SCRV_GUARD_TASK
	eSCRVGUARD_IDLE = 0,
	eSCRVGUARD_SCENARIO = 1,
	eSCRVGUARD_PATROL = 2,
	eSCRVGUARD_ATTACK = 3
ENDENUM

ENUM SCRV_LAB_TASK
	eSCRVLAB_IDLE = 0,
	eSCRVLAB_SCENARIO = 1,
	eSCRVLAB_COWER = 2
ENDENUM

ENUM SCRV_PED_BEHAVIOUR
	SCRV_PED_BEHAVIOUR_DEFAULT,
	SCRV_PED_BEHAVIOUR_COWER
ENDENUM

ENUM SCRV_INTERIOR
	eSCRVINTERIOR_SECURITY_ROOM
ENDENUM

ENUM SCRV_MUSIC
	eSCRV_MUSIC_INIT = -1,
	eSCRV_MUSIC_START,
	eSCRV_MUSIC_ENTER_LOCATION,
	eSCRV_MUSIC_GUNFIGHT,
	eSCRV_MUSIC_UNLOCK_DOOR,
	eSCRV_MUSIC_DELIVER
ENDENUM


//////////////////////////
///    HELPERS 
//////////////////////////

FUNC INT SCRV_GET_INTERACT_PROP_KEYPAD
	SWITCH GET_SUBVARIATION()
		CASE FSS_RCV_LOCATION_1			RETURN 18
		CASE FSS_RCV_LOCATION_2			RETURN 20
		CASE FSS_RCV_LOCATION_3			RETURN 27
	ENDSWITCH
	RETURN -1
ENDFUNC

FUNC STRING SCRV_ITEM_LABEL_FOR_MISSION
	SWITCH GET_SUBVARIATION()
		CASE FSS_RCV_LOCATION_1		RETURN "FXR_ITM_VAN"
		CASE FSS_RCV_LOCATION_2	
			SWITCH serverBD.iRandomModeInt1
				CASE 0					RETURN "FXR_ITM_COMET"
				CASE 1					RETURN "FXR_ITM_IGNUS"
				CASE 2					RETURN "FXR_ITM_JESTER"
			ENDSWITCH
		BREAK
		CASE FSS_RCV_LOCATION_3	
			SWITCH serverBD.iRandomModeInt1
				CASE 0					RETURN "FXR_ITM_REEVER"
				CASE 1					RETURN "FXR_ITM_SHINOBI"
				CASE 2					RETURN "FXR_ITM_AVARUS"
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN "FXR_ITM_VEH"
ENDFUNC

FUNC FMC_MISSION_INTERIOR SCRV_GET_MISSION_INTERIOR()
	SWITCH GET_SUBVARIATION()
		CASE FSS_RCV_LOCATION_1		RETURN eFMCINTERIOR_HUMANE_LABS_INSTANCED
		CASE FSS_RCV_LOCATION_2		RETURN eFMCINTERIOR_IMPORT_EXPORT_WAREHOUSE
		CASE FSS_RCV_LOCATION_3		RETURN eFMCINTERIOR_HANGAR
	ENDSWITCH
	RETURN eFMCINTERIOR_INVALID
ENDFUNC

FUNC INT SCRV_GET_MISSION_INTERIOR_HASH()
	SWITCH GET_SUBVARIATION()
		CASE FSS_RCV_LOCATION_2		RETURN SCRV_INTERIOR_ROOM_KEY_IMPORT_EXPORT_WAREHOUSE
		CASE FSS_RCV_LOCATION_3		RETURN SCRV_INTERIOR_ROOM_KEY_HANGAR
	ENDSWITCH
	RETURN SCRV_INTERIOR_ROOM_KEY_HANGAR
ENDFUNC

FUNC BOOL SHOULD_MAINTAIN_PROP_FORCE_ROOM()
	SWITCH GET_SUBVARIATION()
		CASE FSS_RCV_LOCATION_2
		CASE FSS_RCV_LOCATION_3
			RETURN TRUE
	ENDSWITCH
	RETURN FALSE
ENDFUNC
FUNC INT SCRV_GET_PED_SPAWN_ID(SCRV_TAGS eTag)
	SWITCH eTag
		CASE SCRV_TAGS_SPAWN
			SWITCH GET_SUBVARIATION()
				CASE FSS_RCV_LOCATION_1			RETURN 27
				CASE FSS_RCV_LOCATION_2			RETURN 14
				CASE FSS_RCV_LOCATION_3			RETURN 20
			ENDSWITCH
		BREAK
		CASE SCRV_TAGS_SPAWN_2
			SWITCH GET_SUBVARIATION()
				CASE FSS_RCV_LOCATION_1			RETURN 28
				CASE FSS_RCV_LOCATION_2			RETURN 15
				CASE FSS_RCV_LOCATION_3			RETURN 21
			ENDSWITCH
		BREAK
		CASE SCRV_TAGS_SPAWN_3
			SWITCH GET_SUBVARIATION()
				CASE FSS_RCV_LOCATION_1			RETURN 29
				CASE FSS_RCV_LOCATION_2			RETURN 16
				CASE FSS_RCV_LOCATION_3			RETURN 22
			ENDSWITCH
		BREAK
		CASE SCRV_TAGS_SPAWN_4
			SWITCH GET_SUBVARIATION()
				CASE FSS_RCV_LOCATION_1			RETURN -1
				CASE FSS_RCV_LOCATION_2			RETURN 17
				CASE FSS_RCV_LOCATION_3			RETURN 23
			ENDSWITCH
		BREAK
		CASE SCRV_TAGS_SPAWN_5
			SWITCH GET_SUBVARIATION()
				CASE FSS_RCV_LOCATION_1			RETURN -1
				CASE FSS_RCV_LOCATION_2			RETURN 18
				CASE FSS_RCV_LOCATION_3			RETURN 24
			ENDSWITCH
		BREAK
		CASE SCRV_TAGS_SPAWN_6
			SWITCH GET_SUBVARIATION()
				CASE FSS_RCV_LOCATION_1			RETURN -1
				CASE FSS_RCV_LOCATION_2			RETURN 19
				CASE FSS_RCV_LOCATION_3			RETURN 25
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN -1
ENDFUNC

//////////////////////////
///    OBJECTIVE TEXT  
//////////////////////////

PROC SCRV_OBJECTIVE_TEXT_ENTER
	SWITCH GET_SUBVARIATION()
		CASE FSS_RCV_LOCATION_1
			Print_Objective_Text_With_Coloured_Text_Label("FXR_ENTER_THE", "FXR_LOC_RLAB", HUD_COLOUR_YELLOW)
		BREAK
		CASE FSS_RCV_LOCATION_2
			Print_Objective_Text_With_Coloured_Text_Label("FXR_ENTER_THE", "FXR_LOC_GAR", HUD_COLOUR_YELLOW)
		BREAK
		CASE FSS_RCV_LOCATION_3
			Print_Objective_Text_With_Coloured_Text_Label("FXR_ENTER_THE", "FXR_LOC_HAN", HUD_COLOUR_YELLOW)
		BREAK
	ENDSWITCH
ENDPROC

PROC SCRV_OBJECTIVE_TEXT_EXIT
	SWITCH GET_SUBVARIATION()
		CASE FSS_RCV_LOCATION_1
			Print_Objective_Text("FXR_EXIT_LAB")
		BREAK
		CASE FSS_RCV_LOCATION_2
			Print_Objective_Text("FXR_EXIT_GAR")
		BREAK
		CASE FSS_RCV_LOCATION_3
			Print_Objective_Text("FXR_EXIT_HAN")
		BREAK
	ENDSWITCH
ENDPROC

PROC SCRV_OBJECTIVE_TEXT_GO_TO
	SWITCH GET_SUBVARIATION()
		CASE FSS_RCV_LOCATION_1
			Print_Objective_Text_With_Coloured_Text_Label("FXR_GOTO", "FXR_LOC_HLR", HUD_COLOUR_YELLOW)
		BREAK
		CASE FSS_RCV_LOCATION_2
			Print_Objective_Text_With_Coloured_Text_Label("FXR_GOTO", "FXR_LOC_RAN", HUD_COLOUR_YELLOW)
		BREAK
		CASE FSS_RCV_LOCATION_3
			Print_Objective_Text_With_Coloured_Text_Label("FXR_GOTO", "FXR_LOC_LSIA", HUD_COLOUR_YELLOW)
		BREAK
	ENDSWITCH
ENDPROC

PROC SCRV_OBJECTIVE_TEXT_LOCATE_VEHICLE
	IF NOT IS_LOCAL_PLAYER_IN_MISSION_INTERIOR(SCRV_GET_MISSION_INTERIOR())
		SCRV_OBJECTIVE_TEXT_ENTER()
	ELSE
		SWITCH GET_SUBVARIATION()
			CASE FSS_RCV_LOCATION_1
				Print_Objective_Text("FXR_OT_LOC_VEH1")
			BREAK
			CASE FSS_RCV_LOCATION_2
				Print_Objective_Text("FXR_OT_LOC_VEH2")
			BREAK
			CASE FSS_RCV_LOCATION_3
				Print_Objective_Text("FXR_OT_LOC_VEH3")
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

PROC SCRV_OBJECTIVE_TEXT_UNLOCK_DOOR
	IF NOT IS_LOCAL_PLAYER_IN_MISSION_INTERIOR(SCRV_GET_MISSION_INTERIOR())
		SCRV_OBJECTIVE_TEXT_ENTER()
	ELSE
		IF NOT IS_MISSION_SERVER_BIT_SET(eMISSIONSERVERBITSET_IMANI_TEXT_RECEIVED)
			IF NOT HAS_MISSION_ENTITY_BEEN_COLLECTED_FOR_FIRST_TIME(SCRV_MISSION_ENTITY_ID)
				Print_Objective_Text_With_Coloured_Text_Label("FXR_OT_RCVR", SCRV_ITEM_LABEL_FOR_MISSION(), HUD_COLOUR_BLUE)
			ELSE
				IF HAS_NET_TIMER_EXPIRED(serverBD.sMissionData.stGenericTimer, SCRV_WAIT_TO_GO_KEYPAD_TIMER)
					RESET_NET_TIMER(serverBD.sMissionData.stGenericTimer)
					SET_MISSION_CLIENT_BIT(eMISSIONCLIENTBITSET_IMANI_TEXT_RECEIVED)
				ELSE
					CLEAR_OBJECTIVE()
				ENDIF
			ENDIF
		ELSE
			SWITCH GET_SUBVARIATION()
				CASE FSS_RCV_LOCATION_1
					Print_Objective_Text("FXR_OT_RV_ULCK1")
				BREAK
				CASE FSS_RCV_LOCATION_2
					Print_Objective_Text("FXR_OT_RV_ULCK2")
				BREAK
				CASE FSS_RCV_LOCATION_3
					Print_Objective_Text("FXR_OT_RV_ULCK3")
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
ENDPROC

PROC SCRV_STEAL_OBJECTIVE_TEXT
	IF NOT IS_LOCAL_PLAYER_IN_MISSION_INTERIOR(SCRV_GET_MISSION_INTERIOR())
	AND IS_MISSION_ENTITY_IN_MISSION_INTERIOR(SCRV_MISSION_ENTITY_ID)
		SCRV_OBJECTIVE_TEXT_ENTER()
	ELIF IS_LOCAL_PLAYER_IN_MISSION_INTERIOR(SCRV_GET_MISSION_INTERIOR())
	AND NOT IS_MISSION_ENTITY_IN_MISSION_INTERIOR(SCRV_MISSION_ENTITY_ID)
		SCRV_OBJECTIVE_TEXT_EXIT()
	ELSE
		IF IS_ANY_MISSION_ENTITY_HELD_BY_RIVAL_TEAM()
			Print_Objective_Text_With_Coloured_Text_Label("FXR_OT_RCVR", SCRV_ITEM_LABEL_FOR_MISSION(), HUD_COLOUR_RED)
		ELSE
			Print_Objective_Text_With_Coloured_Text_Label("FXR_OT_RCVR", SCRV_ITEM_LABEL_FOR_MISSION(), HUD_COLOUR_BLUE)
		ENDIF
	ENDIF
ENDPROC

PROC SCRV_DELIVER_OBJECTIVE_TEXT
	IF IS_LOCAL_PLAYER_IN_MISSION_INTERIOR(SCRV_GET_MISSION_INTERIOR())
		SCRV_OBJECTIVE_TEXT_EXIT()
	ELSE
		Print_Objective_Text_With_String("FXR_OT_DEL", SCRV_ITEM_LABEL_FOR_MISSION(), DEFAULT, HUD_COLOUR_WHITE)
	ENDIF
ENDPROC

PROC SCRV_HELP_DELIVER_OBJECTIVE_TEXT
	IF NOT IS_LOCAL_PLAYER_IN_MISSION_INTERIOR(SCRV_GET_MISSION_INTERIOR())
	AND IS_MISSION_ENTITY_IN_MISSION_INTERIOR(SCRV_MISSION_ENTITY_ID)
		SCRV_OBJECTIVE_TEXT_ENTER()
	ELIF IS_LOCAL_PLAYER_IN_MISSION_INTERIOR(SCRV_GET_MISSION_INTERIOR())
	AND NOT IS_MISSION_ENTITY_IN_MISSION_INTERIOR(SCRV_MISSION_ENTITY_ID)
		SCRV_OBJECTIVE_TEXT_EXIT()
	ELSE
		Print_Objective_Text_With_String("FXR_OT_HDEL", SCRV_ITEM_LABEL_FOR_MISSION(), DEFAULT, HUD_COLOUR_WHITE)
	ENDIF
ENDPROC

PROC SCRV_RECOVER_OBJECTIVE_TEXT
	IF NOT IS_LOCAL_PLAYER_IN_MISSION_INTERIOR(SCRV_GET_MISSION_INTERIOR())
	AND IS_MISSION_ENTITY_IN_MISSION_INTERIOR(SCRV_MISSION_ENTITY_ID)
		SCRV_OBJECTIVE_TEXT_ENTER()
	ELSE
		Print_Objective_Text_With_String("FXR_OT_RCVR", SCRV_ITEM_LABEL_FOR_MISSION(), DEFAULT, HUD_COLOUR_WHITE)
	ENDIF
ENDPROC

//////////////////////////
///    MODE STATES  
//////////////////////////

FUNC BOOL SCRV_PROGRESS_STATE_INTERACT()
	RETURN IS_ARRAYED_BIT_SET(serverBD.iInteractLocationDoneBitset, SCRV_INTERACT_KEYPAD)
ENDFUNC

FUNC BOOL SCRV_PRGORESS_STATE_GO_TO()
	RETURN IS_LOCAL_PLAYER_WITHIN_AIRPORT_GATES()
ENDFUNC

PROC SCRV_SETUP_STATES
	ADD_MODE_STATE(eSCRVSTATE_GO_TO, eMODESTATE_GO_TO_POINT)
		ADD_MODE_STATE_TRANSITION_DEFAULT(eSCRVSTATE_GO_TO, eSCRVSTATE_LOCATE_VEHICLE)
	IF GET_SUBVARIATION() = FSS_RCV_LOCATION_3
		ADD_MODE_STATE_TRANSITION_CUSTOM(eSCRVSTATE_GO_TO, eSCRVSTATE_LOCATE_VEHICLE,&SCRV_PRGORESS_STATE_GO_TO)
	ENDIF

	ADD_MODE_STATE(eSCRVSTATE_LOCATE_VEHICLE, eMODESTATE_SEARCH_AREA)
		ADD_MODE_STATE_TRANSITION_DEFAULT(eSCRVSTATE_LOCATE_VEHICLE, eSCRVSTATE_UNLOCK_DOOR)

	ADD_MODE_STATE(eSCRVSTATE_UNLOCK_DOOR, eMODESTATE_INTERACT)
		ADD_MODE_STATE_TRANSITION_CUSTOM(eSCRVSTATE_UNLOCK_DOOR, eSCRVSTATE_COLLECT_MISSION_ENTITY, &SCRV_PROGRESS_STATE_INTERACT)
	
	ADD_MODE_STATE(eSCRVSTATE_COLLECT_MISSION_ENTITY, eMODESTATE_COLLECT_MISSION_ENTITY)
		ADD_MODE_STATE_TRANSITION_DEFAULT(eSCRVSTATE_COLLECT_MISSION_ENTITY, eSCRVSTATE_DELIVER_MISSION_ENTITY)
		
	ADD_MODE_STATE(eSCRVSTATE_DELIVER_MISSION_ENTITY, eMODESTATE_DELIVER_MISSION_ENTITY)
		ADD_MODE_STATE_TRANSITION_DEFAULT(eSCRVSTATE_DELIVER_MISSION_ENTITY, eSCRVSTATE_END)

	ADD_MODE_STATE(eSCRVSTATE_END, eMODESTATE_END)
ENDPROC

PROC SCRV_SETUP_CLIENT_STATES
	ADD_CLIENT_MODE_STATE(eSCRVSTATECLIENT_GO_TO, eMODESTATE_GO_TO_POINT, &SCRV_OBJECTIVE_TEXT_GO_TO)
		ADD_CLIENT_MODE_STATE_TRANSITION_LINKED(eSCRVSTATECLIENT_GO_TO, eSCRVSTATECLIENT_LOCATE_VEHICLE, eSCRVSTATE_LOCATE_VEHICLE)
	
	ADD_CLIENT_MODE_STATE(eSCRVSTATECLIENT_LOCATE_VEHICLE, eMODESTATE_SEARCH_AREA, &SCRV_OBJECTIVE_TEXT_LOCATE_VEHICLE)
		ADD_CLIENT_MODE_STATE_TRANSITION_LINKED(eSCRVSTATECLIENT_LOCATE_VEHICLE, eSCRVSTATECLIENT_UNLOCK_DOOR, eSCRVSTATE_UNLOCK_DOOR)
	
	ADD_CLIENT_MODE_STATE(eSCRVSTATECLIENT_UNLOCK_DOOR, eMODESTATE_INTERACT, &SCRV_OBJECTIVE_TEXT_UNLOCK_DOOR)
		ADD_CLIENT_MODE_STATE_TRANSITION_LINKED(eSCRVSTATECLIENT_UNLOCK_DOOR, eSCRVSTATECLIENT_COLLECT_MISSION_ENTITY, eSCRVSTATE_COLLECT_MISSION_ENTITY)
		
	ADD_CLIENT_MODE_STATES_DELIVERY(eSCRVSTATECLIENT_COLLECT_MISSION_ENTITY, eSCRVSTATECLIENT_DELIVER_MISSION_ENTITY, eSCRVSTATECLIENT_HELP_DELIVER_MISSION_ENTITY, eSCRVSTATECLIENT_RECOVER_MISSION_ENTITY, eSCRVSTATECLIENT_END,
		&SCRV_STEAL_OBJECTIVE_TEXT, &SCRV_DELIVER_OBJECTIVE_TEXT, &SCRV_HELP_DELIVER_OBJECTIVE_TEXT, &SCRV_RECOVER_OBJECTIVE_TEXT)

	ADD_CLIENT_MODE_STATE(eSCRVSTATECLIENT_END, eMODESTATE_END, &EMPTY)
ENDPROC


//////////////////////////
///    FUNCTION POINTERS
//////////////////////////

/////////////////
///    PED
/////////////////

FUNC BOOL SCRV_PED_ACTIVATE(INT iPed)
	IF SCRV_GET_PED_SPAWN_ID(SCRV_TAGS_SPAWN) = iPed
	OR SCRV_GET_PED_SPAWN_ID(SCRV_TAGS_SPAWN_2) = iPed
	OR SCRV_GET_PED_SPAWN_ID(SCRV_TAGS_SPAWN_3) = iPed
	OR SCRV_GET_PED_SPAWN_ID(SCRV_TAGS_SPAWN_4) = iPed
	OR SCRV_GET_PED_SPAWN_ID(SCRV_TAGS_SPAWN_5) = iPed
	OR SCRV_GET_PED_SPAWN_ID(SCRV_TAGS_SPAWN_6) = iPed
		RETURN IS_MISSION_SERVER_BIT_SET(eMISSIONSERVERBITSET_IMANI_TEXT_RECEIVED) 
			AND HAS_NET_TIMER_EXPIRED(serverBD.sMissionData.stSecondAmbush,SCRV_AMBUSH_DELAY_TIMER + (iPed - SCRV_GET_PED_SPAWN_ID(SCRV_TAGS_SPAWN))*500)
			AND ARE_ANY_PARTICIPANTS_IN_MISSION_INTERIOR()
	ENDIF
	RETURN TRUE
ENDFUNC

FUNC BOOL SCRV_PED_BLIP_ENABLE(INT iPed)
	IF NOT IS_ENTITY_ALIVE(NET_TO_ENT(serverBD.sPed[iPed].netId))
		RETURN FALSE
	ENDIF

	IF IS_LOCAL_PLAYER_IN_MISSION_INTERIOR(SCRV_GET_MISSION_INTERIOR())
		IF data.Ped.Peds[iPed].iInterior = 0
			IF GET_DISTANCE_BETWEEN_COORDS(LOCAL_PLAYER_COORD, GET_ENTITY_COORDS(NET_TO_ENT(serverBD.sPed[iPed].netId))) < SCRV_PED_BLIP_MAX_DISTANCE
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC FLOAT SCRV_PED_BLIP_SCALE(INT iPed)
	IF IS_LOCAL_PLAYER_IN_MISSION_INTERIOR(SCRV_GET_MISSION_INTERIOR())
		IF data.Ped.Peds[iPed].iInterior = 0
			RETURN 0.75
		ENDIF
	ENDIF
	RETURN 1.0
ENDFUNC

FUNC BOOL SCRV_PED_BLIP_SHOW_HEIGHT(INT iPed)
	IF IS_LOCAL_PLAYER_IN_MISSION_INTERIOR(SCRV_GET_MISSION_INTERIOR())
		IF data.Ped.Peds[iPed].iInterior = 0
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL SCRV_PED_TO_PATROL(INT iPed, PED_INDEX pedId)
	UNUSED_PARAMETER(iPed)
	UNUSED_PARAMETER(pedId)
	RETURN DOES_PED_HAVE_PATROL(iPed)
ENDFUNC

FUNC BOOL SCRV_PED_TO_SCENARIO(INT iPed, PED_INDEX pedId)
	UNUSED_PARAMETER(iPed)
	UNUSED_PARAMETER(pedId)
	RETURN data.Ped.Peds[iPed].iScenarioIndex != (-1)
ENDFUNC

SCRIPT_TIMER stPedAttackDelay
FUNC BOOL SCRV_PED_TO_ATTACK(INT iPed, PED_INDEX pedId)
	UNUSED_PARAMETER(iPed)
	UNUSED_PARAMETER(pedId)
	
	RETURN HAS_ANY_PED_GROUP_BEEN_TRIGGERED() AND HAS_NET_TIMER_EXPIRED(stPedAttackDelay, data.Ped.Peds[iPed].iMaxReactionDelay)
ENDFUNC

FUNC BOOL SCRV_PED_TO_COWER(INT iPed, PED_INDEX pedId)
	UNUSED_PARAMETER(iPed)
	UNUSED_PARAMETER(pedId)
	RETURN HAS_ANY_PED_GROUP_BEEN_TRIGGERED()
ENDFUNC

PROC SCRV_SETUP_PED_TASKS(INT iBehaviour)
	SWITCH INT_TO_ENUM(SCRV_PED_BEHAVIOUR, iBehaviour)
		CASE SCRV_PED_BEHAVIOUR_DEFAULT
			ADD_PED_TASK(iBehaviour, eSCRVGUARD_IDLE, ePEDTASK_IDLE)
				ADD_PED_TASK_TRANSITION(iBehaviour, eSCRVGUARD_IDLE, eSCRVGUARD_PATROL, &SCRV_PED_TO_PATROL)
				ADD_PED_TASK_TRANSITION(iBehaviour, eSCRVGUARD_IDLE, eSCRVGUARD_SCENARIO, &SCRV_PED_TO_SCENARIO)
				ADD_PED_TASK_TRANSITION(iBehaviour, eSCRVGUARD_IDLE, eSCRVGUARD_ATTACK, &SCRV_PED_TO_ATTACK)
			
			ADD_PED_TASK(iBehaviour, eSCRVGUARD_SCENARIO, ePEDTASK_DO_SCENARIOS)
				ADD_PED_TASK_TRANSITION(iBehaviour, eSCRVGUARD_SCENARIO, eSCRVGUARD_ATTACK, &SCRV_PED_TO_ATTACK)
				
			ADD_PED_TASK(iBehaviour, eSCRVGUARD_PATROL, ePEDTASK_PATROL)
				ADD_PED_TASK_TRANSITION(iBehaviour, eSCRVGUARD_PATROL, eSCRVGUARD_ATTACK, &SCRV_PED_TO_ATTACK)
			
			ADD_PED_TASK(iBehaviour, eSCRVGUARD_ATTACK, ePEDTASK_ATTACK_HATED_TARGETS)
		BREAK
		CASE SCRV_PED_BEHAVIOUR_COWER
			ADD_PED_TASK(iBehaviour, eSCRVLAB_IDLE, ePEDTASK_IDLE)
				ADD_PED_TASK_TRANSITION(iBehaviour, eSCRVLAB_IDLE, eSCRVLAB_COWER, &SCRV_PED_TO_COWER)
				ADD_PED_TASK_TRANSITION(iBehaviour, eSCRVLAB_IDLE, eSCRVLAB_SCENARIO, &SCRV_PED_TO_SCENARIO)

			ADD_PED_TASK(iBehaviour, eSCRVLAB_SCENARIO, ePEDTASK_DO_SCENARIOS)
				ADD_PED_TASK_TRANSITION(iBehaviour, eSCRVLAB_SCENARIO, eSCRVLAB_COWER, &SCRV_PED_TO_COWER)

			ADD_PED_TASK(iBehaviour, eSCRVLAB_COWER, ePEDTASK_COWER)
		BREAK
	ENDSWITCH
ENDPROC

FUNC INT SCRV_GET_PED_BEHAVIOUR(INT iPed)
	IF GET_SUBVARIATION() = FSS_RCV_LOCATION_1
		IF iPed = 24
		OR iPed = 25
		OR iPed = 26
			RETURN ENUM_TO_INT(SCRV_PED_BEHAVIOUR_COWER)
		ENDIF
	ENDIF	

	RETURN ENUM_TO_INT(SCRV_PED_BEHAVIOUR_DEFAULT)

ENDFUNC

PROC SCRV_PROP_CLIENT(INT iProp, ENTITY_INDEX propId, BOOL bDead)
	UNUSED_PARAMETER(bDead)
	
	IF GET_SUBVARIATION() = FSS_RCV_LOCATION_2
		// Fix for interior vehicles not appearing lit properly
		IF data.Prop.Props[iProp].iInterior != -1
			IF NOT (IS_VALID_INTERIOR(sInteriorlLocal.sInterior[SCRV_MISSION_INTERIOR_ID].index)
					AND IS_INTERIOR_READY(sInteriorlLocal.sInterior[SCRV_MISSION_INTERIOR_ID].index))
				SET_ENTITY_LOCALLY_INVISIBLE(propId)
				PRINTLN("SCRV_PROP_CLIENT - SET_ENTITY_LOCALLY_INVISIBLE iProp: ", iProp)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SCRV_PED_ATTRIBUTES(INT iPed, PED_INDEX pedID, BOOL bAmbush)
	UNUSED_PARAMETER(bAmbush)
	IF data.Ped.Peds[iPed].model = INT_TO_ENUM(MODEL_NAMES, HASH("g_f_importexport_01"))
		SWITCH iPed%5
			CASE 0
				SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_HEAD, 		0, 0)
				SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_HAIR, 		0, 0)
				SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_TORSO,	 	0, 0)
				SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_LEG, 		0, 0)
				SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_SPECIAL, 	0, 0)
			BREAK
			CASE 1
				SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_HEAD, 		1, 0)
				SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_HAIR, 		1, 0)
				SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_TORSO,	 	1, 1)
				SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_LEG, 		1, 0)
				SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_SPECIAL, 	1, 0)
			BREAK
			CASE 2
				SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_HEAD, 		2, 0)
				SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_HAIR, 		2, 0)
				SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_TORSO,	 	2, 0)
				SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_LEG, 		2, 0)
				SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_SPECIAL, 	2, 0)
			BREAK
			CASE 3
				SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_HEAD, 		3, 0)
				SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_HAIR, 		3, 0)
				SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_TORSO,	 	3, 3)
				SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_LEG, 		3, 3)
				SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_SPECIAL, 	3, 0)
			BREAK
			CASE 4
				SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_HEAD, 		4, 0)
				SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_HAIR, 		4, 0)
				SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_TORSO,	 	4, 1)
				SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_LEG, 		4, 2)
				SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_SPECIAL, 	4, 0)
			BREAK
		ENDSWITCH
	ELIF data.Ped.Peds[iPed].model = INT_TO_ENUM(MODEL_NAMES, HASH("g_m_importexport_01"))
		SWITCH iPed%5
			CASE 0
				SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_HEAD, 		0, 0)
				SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_HAIR, 		0, 0)
				SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_TORSO,	 	0, 0)
				SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_LEG, 		0, 2)
				SET_PED_PROP_INDEX(			pedID, ANCHOR_HEAD, 		0, 0)
			BREAK
			CASE 1
				SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_HEAD, 		1, 0)
				SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_HAIR, 		1, 0)
				SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_TORSO,	 	1, 0)
				SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_LEG, 		1, 0)
				SET_PED_PROP_INDEX(			pedID, ANCHOR_EYES, 		0, 0)
			BREAK
			CASE 2
				SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_HEAD, 		2, 0)
				SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_HAIR, 		2, 0)
				SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_TORSO,	 	2, 0)
				SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_LEG, 		2, 0)
//				SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_SPECIAL, 	2, 0)
			BREAK
			CASE 3
				SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_HEAD, 		3, 0)
				SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_HAIR, 		3, 0)
				SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_TORSO,	 	3, 0)
				SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_LEG, 		3, 0)
//				SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_SPECIAL, 	3, 0)
			BREAK
			CASE 4
				SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_HEAD, 		4, 0)
				SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_HAIR, 		4, 0)
				SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_TORSO,	 	4, 0)
				SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_LEG, 		4, 0)
				SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_SPECIAL, 	1, 0)
			BREAK
		ENDSWITCH
	ENDIF
	
	IF IS_PED_DATA_BIT_SET(iPed, ePEDDATABITSET_I_AM_A_CIVILIAN)
		SET_PED_TREATED_AS_FRIENDLY(pedID, TRUE)
		SET_PED_CAN_BE_TARGETTED(pedID, FALSE)
	ENDIF
ENDPROC

/////////////////
///    VEHICLE
/////////////////

FUNC BOOL SCRV_VEHICLE_MODS(INT iVehicle, VEHICLE_SETUP_STRUCT_MP& sData)
	SWITCH GET_SUBVARIATION()
		CASE FSS_RCV_LOCATION_1
			IF iVehicle = GET_TAG_ID(SCRV_TAGS_MISSION_VEHICLE)
				sData.VehicleSetup.iModIndex[MOD_ENGINE] = 4
				sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 3
				RETURN TRUE
			ENDIF
		BREAK
		CASE FSS_RCV_LOCATION_2
			IF iVehicle = GET_TAG_ID(SCRV_TAGS_MISSION_VEHICLE)
				IF data.Vehicle.Vehicles[iVehicle].model = COMET7
					PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - SCRV_VEHICLE_MODS - sData.VehicleSetup.eModel = COMET7")
					sData.VehicleSetup.eModel = COMET7
					sData.VehicleSetup.tlPlateText = "OLG28311"
					sData.VehicleSetup.iColour1 = 28
					sData.VehicleSetup.iColour2 = 28
					sData.VehicleSetup.iColourExtra1 = 28
					sData.VehicleSetup.iColourExtra2 = 0
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWindowTintColour = 3
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					sData.VehicleSetup.iModIndex[MOD_SPOILER] = 2
					sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 2
					sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
					sData.VehicleSetup.iModIndex[MOD_SKIRT] = 8
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 4
					sData.VehicleSetup.iModIndex[MOD_BONNET] = 6
					sData.VehicleSetup.iModIndex[MOD_ENGINE] = 2
					sData.VehicleSetup.iModIndex[MOD_BRAKES] = 2
					sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 2
					sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 2
					sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
					sData.VehicleSetup.iModIndex[MOD_PLTVANITY] = 1
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 1
					RETURN TRUE
				ELIF data.Vehicle.Vehicles[iVehicle].model = IGNUS
					PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - SCRV_VEHICLE_MODS - sData.VehicleSetup.eModel = IGNUS")
					sData.VehicleSetup.eModel = IGNUS
					sData.VehicleSetup.tlPlateText = "44EVH397"
					sData.VehicleSetup.iColour1 = 88
					sData.VehicleSetup.iColour2 = 88
					sData.VehicleSetup.iColourExtra1 = 0
					sData.VehicleSetup.iColourExtra2 = 158
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWindowTintColour = 3
					sData.VehicleSetup.iWheelType = 7
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					sData.VehicleSetup.iModIndex[MOD_SPOILER] = 14
					sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 9
					sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 5
					sData.VehicleSetup.iModIndex[MOD_SKIRT] = 2
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 4
					sData.VehicleSetup.iModIndex[MOD_BONNET] = 10
					sData.VehicleSetup.iModIndex[MOD_ROOF] = 2
					sData.VehicleSetup.iModIndex[MOD_ENGINE] = 2
					sData.VehicleSetup.iModIndex[MOD_BRAKES] = 2
					sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 2
					sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 1
					sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 2
					RETURN TRUE
				ELIF data.Vehicle.Vehicles[iVehicle].model = JESTER4
					PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - SCRV_VEHICLE_MODS - sData.VehicleSetup.eModel = JESTER4")
					sData.VehicleSetup.eModel = JESTER4
					sData.VehicleSetup.tlPlateText = "01LHV430"
					sData.VehicleSetup.iColour1 = 70
					sData.VehicleSetup.iColour2 = 36
					sData.VehicleSetup.iColourExtra1 = 0
					sData.VehicleSetup.iColourExtra2 = 156
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWindowTintColour = 3
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_BULLETPROOF_TYRES)
					sData.VehicleSetup.iModIndex[MOD_SPOILER] = 8
					sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 6
					sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 8
					sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 4
					sData.VehicleSetup.iModIndex[MOD_BONNET] = 6
					sData.VehicleSetup.iModIndex[MOD_WING_L] = 2
					sData.VehicleSetup.iModIndex[MOD_ROOF] = 2
					sData.VehicleSetup.iModIndex[MOD_ENGINE] = 2
					sData.VehicleSetup.iModIndex[MOD_BRAKES] = 2
					sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 2
					sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 2
					sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
					sData.VehicleSetup.iModIndex[MOD_WHEELS] = 44
					sData.VehicleSetup.iModIndex[MOD_PLTVANITY] = 2
					sData.VehicleSetup.iModIndex[MOD_INTERIOR2] = 1
					sData.VehicleSetup.iModIndex[MOD_INTERIOR3] = 2
					sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 2
					sData.VehicleSetup.iModIndex[MOD_INTERIOR5] = 3
					sData.VehicleSetup.iModIndex[MOD_SEATS] = 5
					sData.VehicleSetup.iModIndex[MOD_STEERING] = 3
					sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 4
					sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 5
					sData.VehicleSetup.iModIndex[MOD_ENGINEBAY3] = 1
					sData.VehicleSetup.iModIndex[MOD_CHASSIS2] = 3
					sData.VehicleSetup.iModIndex[MOD_CHASSIS4] = 1
					sData.VehicleSetup.iModIndex[MOD_CHASSIS5] = 4
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 8
					RETURN TRUE
				ENDIF
			ELIF iVehicle = GET_TAG_ID(SCRV_TAGS_VEHICLE_1)
			  OR iVehicle = GET_TAG_ID(SCRV_TAGS_AMBUSH_2)
				sData.VehicleSetup.eModel = CHINO2
				sData.VehicleSetup.iColour1 = 146
				sData.VehicleSetup.iColour2 = 25
				sData.VehicleSetup.iColourExtra1 = 145
				sData.VehicleSetup.iColourExtra2 = 156
				sData.iColour5 = 1
				sData.iColour6 = 132
				sData.iLivery2 = 0
				sData.VehicleSetup.iWindowTintColour = 3
				sData.VehicleSetup.iWheelType = 2
				sData.VehicleSetup.iTyreR = 255
				sData.VehicleSetup.iTyreG = 255
				sData.VehicleSetup.iTyreB = 255
				sData.VehicleSetup.iNeonR = 255
				sData.VehicleSetup.iNeonB = 255
				SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_1)
				sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
				sData.VehicleSetup.iModIndex[MOD_ENGINE] = 2
				sData.VehicleSetup.iModIndex[MOD_BRAKES] = 2
				sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 2
				sData.VehicleSetup.iModIndex[MOD_WHEELS] = 16
				sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 4
				sData.VehicleSetup.iModIndex[MOD_TRUNK] = 6
				sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 2
				sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 2
				sData.VehicleSetup.iModIndex[MOD_CHASSIS5] = 2
				sData.VehicleSetup.iModIndex[MOD_LIVERY] = 9
				RETURN TRUE
			ELIF iVehicle = GET_TAG_ID(SCRV_TAGS_AMBUSH_1)
			  OR iVehicle = GET_TAG_ID(SCRV_TAGS_VEHICLE_2)
				sData.VehicleSetup.eModel = BUCCANEER2
				sData.VehicleSetup.iColour1 = 146
				sData.VehicleSetup.iColour2 = 111
				sData.VehicleSetup.iColourExtra1 = 145
				sData.VehicleSetup.iColourExtra2 = 90
				sData.iColour5 = 1
				sData.iColour6 = 132
				sData.iLivery2 = 0
				sData.VehicleSetup.iWheelType = 8
				sData.VehicleSetup.iTyreR = 255
				sData.VehicleSetup.iTyreG = 255
				sData.VehicleSetup.iTyreB = 255
				sData.VehicleSetup.iNeonR = 255
				sData.VehicleSetup.iNeonB = 255
				SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_3)
				sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
				sData.VehicleSetup.iModIndex[MOD_BONNET] = 2
				sData.VehicleSetup.iModIndex[MOD_ENGINE] = 2
				sData.VehicleSetup.iModIndex[MOD_BRAKES] = 2
				sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 2
				sData.VehicleSetup.iModIndex[MOD_WHEELS] = 3
				sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 4
				sData.VehicleSetup.iModIndex[MOD_PLAQUE] = 6
				sData.VehicleSetup.iModIndex[MOD_TRUNK] = 2
				sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 4
				sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 1
				sData.VehicleSetup.iModIndex[MOD_CHASSIS5] = 1
				sData.VehicleSetup.iModIndex[MOD_LIVERY] = 3
				RETURN TRUE
			ELIF iVehicle = GET_TAG_ID(SCRV_TAGS_VEHICLE_3)
				sData.VehicleSetup.eModel = PEYOTE3
				sData.VehicleSetup.iPlateIndex = 3
				sData.VehicleSetup.iColour1 = 145
				sData.VehicleSetup.iColour2 = 0
				sData.VehicleSetup.iColourExtra1 = 74
				sData.VehicleSetup.iColourExtra2 = 141
				sData.iColour5 = 1
				sData.iColour6 = 132
				sData.iLivery2 = 0
				sData.VehicleSetup.iWheelType = 8
				sData.VehicleSetup.iTyreR = 255
				sData.VehicleSetup.iTyreG = 255
				sData.VehicleSetup.iTyreB = 255
				sData.VehicleSetup.iNeonR = 255
				sData.VehicleSetup.iNeonB = 255
				sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
				sData.VehicleSetup.iModIndex[MOD_GRILL] = 3
				sData.VehicleSetup.iModIndex[MOD_BONNET] = 1
				sData.VehicleSetup.iModIndex[MOD_ENGINE] = 2
				sData.VehicleSetup.iModIndex[MOD_BRAKES] = 2
				sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 2
				sData.VehicleSetup.iModIndex[MOD_WHEELS] = 36
				sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 4
				sData.VehicleSetup.iModIndex[MOD_STEERING] = 16
				sData.VehicleSetup.iModIndex[MOD_TRUNK] = 3
				sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 5
				sData.VehicleSetup.iModIndex[MOD_CHASSIS3] = 1
				sData.VehicleSetup.iModIndex[MOD_CHASSIS5] = 2
				sData.VehicleSetup.iModIndex[MOD_LIVERY] = 2
				RETURN TRUE
			ELIF iVehicle = GET_TAG_ID(SCRV_TAGS_VEHICLE_4)
				sData.VehicleSetup.eModel = BALLER
				sData.VehicleSetup.iColour1 = 145
				sData.VehicleSetup.iColour2 = 0
				sData.VehicleSetup.iColourExtra1 = 74
				sData.VehicleSetup.iColourExtra2 = 156
				sData.iColour5 = 1
				sData.iColour6 = 132
				sData.iLivery2 = 0
				sData.VehicleSetup.iWheelType = 3
				sData.VehicleSetup.iTyreR = 255
				sData.VehicleSetup.iTyreG = 255
				sData.VehicleSetup.iTyreB = 255
				sData.VehicleSetup.iNeonR = 255
				sData.VehicleSetup.iNeonB = 255
				SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_9)
				sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
				sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
				sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
				sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
				sData.VehicleSetup.iModIndex[MOD_ENGINE] = 2
				sData.VehicleSetup.iModIndex[MOD_BRAKES] = 1
				sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 1
				sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 3
				sData.VehicleSetup.iModIndex[MOD_WHEELS] = 34
				RETURN TRUE
			ELIF iVehicle = GET_TAG_ID(SCRV_TAGS_VEHICLE_5)
				sData.VehicleSetup.eModel = EMPEROR
				sData.VehicleSetup.iColour1 = 0
				sData.VehicleSetup.iColour2 = 0
				sData.VehicleSetup.iColourExtra1 = 145
				sData.VehicleSetup.iColourExtra2 = 156
				sData.iColour5 = 1
				sData.iColour6 = 132
				sData.iLivery2 = 0
				sData.VehicleSetup.iWindowTintColour = 3
				sData.VehicleSetup.iWheelType = 2
				sData.VehicleSetup.iTyreR = 255
				sData.VehicleSetup.iTyreG = 255
				sData.VehicleSetup.iTyreB = 255
				sData.VehicleSetup.iNeonR = 255
				sData.VehicleSetup.iNeonB = 255
				SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_10)
				sData.VehicleSetup.iModIndex[MOD_ENGINE] = 3
				sData.VehicleSetup.iModIndex[MOD_BRAKES] = 2
				sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 2
				sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
				sData.VehicleSetup.iModIndex[MOD_WHEELS] = 18
				sData.VehicleSetup.iModVariation[0] = 1
				RETURN TRUE
			ENDIF
		BREAK
		CASE FSS_RCV_LOCATION_3
			IF iVehicle = GET_TAG_ID(SCRV_TAGS_VEHICLE_1)
				sData.VehicleSetup.eModel =  BANSHEE2
				sData.VehicleSetup.tlPlateText = "68KWK927"
				sData.VehicleSetup.iColour1 = 64
				sData.VehicleSetup.iColour2 = 112
				sData.VehicleSetup.iColourExtra1 = 70
				sData.VehicleSetup.iColourExtra2 = 156
				sData.iColour5 = 1
				sData.iColour6 = 132
				sData.iLivery2 = 0
				sData.VehicleSetup.iTyreR = 255
				sData.VehicleSetup.iTyreG = 255
				sData.VehicleSetup.iTyreB = 255
				sData.VehicleSetup.iNeonR = 2
				sData.VehicleSetup.iNeonG = 21
				sData.VehicleSetup.iNeonB = 255
				SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
				SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
				SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
				SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
				SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_1)
				sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
				sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 4
				sData.VehicleSetup.iModIndex[MOD_SKIRT] = 4
				sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 4
				sData.VehicleSetup.iModIndex[MOD_GRILL] = 2
				sData.VehicleSetup.iModIndex[MOD_BONNET] = 8
				sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
				sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 3
				sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
				sData.VehicleSetup.iModIndex[MOD_WHEELS] = 36
				sData.VehicleSetup.iModIndex[MOD_INTERIOR3] = 2
				sData.VehicleSetup.iModIndex[MOD_INTERIOR5] = 3
				sData.VehicleSetup.iModIndex[MOD_SEATS] = 12
				sData.VehicleSetup.iModIndex[MOD_STEERING] = 6
				sData.VehicleSetup.iModIndex[MOD_LIVERY] = 2
				RETURN TRUE
			ELIF iVehicle = GET_TAG_ID(SCRV_TAGS_AMBUSH_1)
				sData.VehicleSetup.eModel = SULTANRS
				sData.VehicleSetup.tlPlateText = "15HBW365"
				sData.VehicleSetup.iColour1 = 92
				sData.VehicleSetup.iColour2 = 92
				sData.VehicleSetup.iColourExtra1 = 92
				sData.VehicleSetup.iColourExtra2 = 156
				sData.iColour5 = 1
				sData.iColour6 = 132
				sData.iLivery2 = 0
				sData.VehicleSetup.iWindowTintColour = 2
				sData.VehicleSetup.iTyreR = 255
				sData.VehicleSetup.iTyreG = 255
				sData.VehicleSetup.iTyreB = 255
				sData.VehicleSetup.iNeonR = 255
				sData.VehicleSetup.iNeonB = 255
				sData.VehicleSetup.iModIndex[MOD_SPOILER] = 11
				sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 9
				sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 6
				sData.VehicleSetup.iModIndex[MOD_SKIRT] = 2
				sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 4
				sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
				sData.VehicleSetup.iModIndex[MOD_BONNET] = 1
				sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
				sData.VehicleSetup.iModIndex[MOD_ENGINE] = 3
				sData.VehicleSetup.iModIndex[MOD_BRAKES] = 2
				sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 2
				sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 2
				sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
				sData.VehicleSetup.iModIndex[MOD_WHEELS] = 49
				sData.VehicleSetup.iModIndex[MOD_INTERIOR3] = 3
				sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 5
				sData.VehicleSetup.iModIndex[MOD_INTERIOR5] = 4
				sData.VehicleSetup.iModIndex[MOD_SEATS] = 5
				sData.VehicleSetup.iModIndex[MOD_STEERING] = 6
				sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 1
				sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 4
				sData.VehicleSetup.iModIndex[MOD_ENGINEBAY3] = 6
				sData.VehicleSetup.iModIndex[MOD_CHASSIS4] = 1
				sData.VehicleSetup.iModIndex[MOD_LIVERY] = 4
				RETURN TRUE
			ELIF iVehicle = GET_TAG_ID(SCRV_TAGS_AMBUSH_2)
				sData.VehicleSetup.eModel = BANSHEE2
				sData.VehicleSetup.tlPlateText = "06NZQ185"
				sData.VehicleSetup.iColour1 = 38
				sData.VehicleSetup.iColour2 = 38
				sData.VehicleSetup.iColourExtra1 = 0
				sData.VehicleSetup.iColourExtra2 = 156
				sData.iColour5 = 1
				sData.iColour6 = 132
				sData.iLivery2 = 0
				sData.VehicleSetup.iWindowTintColour = 3
				sData.VehicleSetup.iTyreR = 255
				sData.VehicleSetup.iTyreG = 255
				sData.VehicleSetup.iTyreB = 255
				SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_1)
				sData.VehicleSetup.iModIndex[MOD_SPOILER] = 2
				sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 3
				sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
				sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
				sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
				sData.VehicleSetup.iModIndex[MOD_BONNET] = 4
				sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
				sData.VehicleSetup.iModIndex[MOD_ENGINE] = 3
				sData.VehicleSetup.iModIndex[MOD_BRAKES] = 2
				sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 2
				sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 2
				sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
				sData.VehicleSetup.iModIndex[MOD_WHEELS] = 26
				sData.VehicleSetup.iModIndex[MOD_INTERIOR3] = 1
				sData.VehicleSetup.iModIndex[MOD_INTERIOR5] = 3
				sData.VehicleSetup.iModIndex[MOD_SEATS] = 5
				sData.VehicleSetup.iModIndex[MOD_STEERING] = 13
				sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 2
				sData.VehicleSetup.iModIndex[MOD_ENGINEBAY3] = 6
				sData.VehicleSetup.iModIndex[MOD_LIVERY] = 6
				RETURN TRUE
			ELIF iVehicle = GET_TAG_ID(SCRV_TAGS_VEHICLE_2)
				sData.VehicleSetup.eModel = SULTANRS
				sData.VehicleSetup.tlPlateText = "86CVG072"
				sData.VehicleSetup.iColour1 = 28
				sData.VehicleSetup.iColour2 = 28
				sData.VehicleSetup.iColourExtra1 = 28
				sData.VehicleSetup.iColourExtra2 = 0
				sData.iColour5 = 1
				sData.iColour6 = 132
				sData.iLivery2 = 0
				sData.VehicleSetup.iWindowTintColour = 2
				sData.VehicleSetup.iTyreR = 255
				sData.VehicleSetup.iTyreG = 255
				sData.VehicleSetup.iTyreB = 255
				sData.VehicleSetup.iNeonR = 255
				sData.VehicleSetup.iNeonB = 255
				sData.VehicleSetup.iModIndex[MOD_SPOILER] = 3
				sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 11
				sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 2
				sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
				sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
				sData.VehicleSetup.iModIndex[MOD_GRILL] = 4
				sData.VehicleSetup.iModIndex[MOD_BONNET] = 8
				sData.VehicleSetup.iModIndex[MOD_ENGINE] = 3
				sData.VehicleSetup.iModIndex[MOD_BRAKES] = 2
				sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 2
				sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 2
				sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
				sData.VehicleSetup.iModIndex[MOD_WHEELS] = 2
				sData.VehicleSetup.iModIndex[MOD_INTERIOR3] = 2
				sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 1
				sData.VehicleSetup.iModIndex[MOD_INTERIOR5] = 6
				sData.VehicleSetup.iModIndex[MOD_SEATS] = 3
				sData.VehicleSetup.iModIndex[MOD_STEERING] = 12
				sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 3
				sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 3
				sData.VehicleSetup.iModIndex[MOD_ENGINEBAY3] = 1
				sData.VehicleSetup.iModIndex[MOD_CHASSIS4] = 1
				sData.VehicleSetup.iModIndex[MOD_LIVERY] = 5
				RETURN TRUE
			ELIF iVehicle = GET_TAG_ID(SCRV_TAGS_VEHICLE_3)
				sData.VehicleSetup.eModel = FACTION2
				sData.VehicleSetup.iColour1 = 0
				sData.VehicleSetup.iColour2 = 0
				sData.VehicleSetup.iColourExtra1 = 0
				sData.VehicleSetup.iColourExtra2 = 89
				sData.iColour5 = 1
				sData.iColour6 = 132
				sData.iLivery2 = 0
				sData.VehicleSetup.iWindowTintColour = 3
				sData.VehicleSetup.iWheelType = 2
				sData.VehicleSetup.iTyreR = 255
				sData.VehicleSetup.iTyreG = 255
				sData.VehicleSetup.iTyreB = 255
				sData.VehicleSetup.iNeonR = 255
				sData.VehicleSetup.iNeonB = 255
				SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_1)
				sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
				sData.VehicleSetup.iModIndex[MOD_BONNET] = 1
				sData.VehicleSetup.iModIndex[MOD_ENGINE] = 2
				sData.VehicleSetup.iModIndex[MOD_BRAKES] = 2
				sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 2
				sData.VehicleSetup.iModIndex[MOD_WHEELS] = 2
				sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 4
				sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 6
				sData.VehicleSetup.iModIndex[MOD_STEERING] = 1
				sData.VehicleSetup.iModIndex[MOD_ICE] = 1
				sData.VehicleSetup.iModIndex[MOD_TRUNK] = 1
				sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 4
				sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 1
				sData.VehicleSetup.iModIndex[MOD_LIVERY] = 2
				RETURN TRUE
			ELIF iVehicle = GET_TAG_ID(SCRV_TAGS_TITAN)
				sData.VehicleSetup.eModel = TITAN // TITAN
				sData.VehicleSetup.tlPlateText = "05YJI028"
				sData.VehicleSetup.iPlateIndex = 4
				sData.VehicleSetup.iColour1 = 121
				sData.VehicleSetup.iColour2 = 111
				sData.VehicleSetup.iColourExtra1 = 122
				sData.VehicleSetup.iColourExtra2 = 156
				sData.iColour5 = 1
				sData.iColour6 = 132
				sData.iLivery2 = 0
				sData.VehicleSetup.iTyreR = 255
				sData.VehicleSetup.iTyreG = 255
				sData.VehicleSetup.iTyreB = 255
				sData.VehicleSetup.iNeonR = 255
				sData.VehicleSetup.iNeonB = 255
				RETURN TRUE
			ELIF iVehicle = GET_TAG_ID(SCRV_TAGS_VEHICLE_5)
				sData.VehicleSetup.eModel = SABREGT2
				sData.VehicleSetup.iColour1 = 88
				sData.VehicleSetup.iColour2 = 88
				sData.VehicleSetup.iColourExtra1 = 88
				sData.VehicleSetup.iColourExtra2 = 88
				sData.iColour5 = 1
				sData.iColour6 = 132
				sData.iLivery2 = 0
				sData.VehicleSetup.iWindowTintColour = 2
				sData.VehicleSetup.iWheelType = 1
				sData.VehicleSetup.iTyreR = 255
				sData.VehicleSetup.iTyreG = 255
				sData.VehicleSetup.iTyreB = 255
				sData.VehicleSetup.iNeonR = 255
				sData.VehicleSetup.iNeonB = 255
				sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
				sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 3
				sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
				sData.VehicleSetup.iModIndex[MOD_BONNET] = 2
				sData.VehicleSetup.iModIndex[MOD_ENGINE] = 2
				sData.VehicleSetup.iModIndex[MOD_BRAKES] = 2
				sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 2
				sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 1
				sData.VehicleSetup.iModIndex[MOD_TRUNK] = 1
				sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 4
				sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 3
				sData.VehicleSetup.iModIndex[MOD_LIVERY] = 3
				RETURN TRUE
			ELIF iVehicle = GET_TAG_ID(SCRV_TAGS_BANSHEE)
			OR iVehicle = GET_TAG_ID(SCRV_TAGS_BANSHEE_2)
				sData.VehicleSetup.eModel = BANSHEE2
				sData.VehicleSetup.tlPlateText = "65GRZ071"
				sData.VehicleSetup.iColour1 = 27
				sData.VehicleSetup.iColour2 = 27
				sData.VehicleSetup.iColourExtra1 = 27
				sData.VehicleSetup.iColourExtra2 = 2
				sData.iColour5 = 1
				sData.iColour6 = 132
				sData.iLivery2 = 0
				sData.VehicleSetup.iWindowTintColour = 3
				sData.VehicleSetup.iTyreR = 255
				sData.VehicleSetup.iTyreG = 255
				sData.VehicleSetup.iTyreB = 255
				SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_1)
				sData.VehicleSetup.iModIndex[MOD_SPOILER] = 3
				sData.VehicleSetup.iModIndex[MOD_SKIRT] = 2
				sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 4
				sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
				sData.VehicleSetup.iModIndex[MOD_BONNET] = 3
				sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
				sData.VehicleSetup.iModIndex[MOD_ENGINE] = 3
				sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 2
				sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 2
				sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
				sData.VehicleSetup.iModIndex[MOD_WHEELS] = 7
				sData.VehicleSetup.iModIndex[MOD_INTERIOR3] = 1
				sData.VehicleSetup.iModIndex[MOD_INTERIOR5] = 3
				sData.VehicleSetup.iModIndex[MOD_SEATS] = 5
				sData.VehicleSetup.iModIndex[MOD_STEERING] = 13
				sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 4
				sData.VehicleSetup.iModIndex[MOD_ENGINEBAY3] = 1
				sData.VehicleSetup.iModIndex[MOD_LIVERY] = 3
				RETURN TRUE
			ELIF iVehicle = GET_TAG_ID(SCRV_TAGS_SULTAN)
			OR iVehicle = GET_TAG_ID(SCRV_TAGS_SULTAN_2)
				sData.VehicleSetup.eModel = sultanrs // SULTANRS
				sData.VehicleSetup.tlPlateText = "84TFX949"
				sData.VehicleSetup.iPlateIndex = 2
				sData.VehicleSetup.iColour1 = 138
				sData.VehicleSetup.iColour2 = 138
				sData.VehicleSetup.iColourExtra1 = 88
				sData.VehicleSetup.iColourExtra2 = 88
				sData.iColour5 = 1
				sData.iColour6 = 132
				sData.iLivery2 = 0
				sData.VehicleSetup.iWindowTintColour = 3
				sData.VehicleSetup.iWheelType = 7
				sData.VehicleSetup.iTyreR = 255
				sData.VehicleSetup.iTyreG = 255
				sData.VehicleSetup.iTyreB = 255
				sData.VehicleSetup.iNeonR = 255
				sData.VehicleSetup.iNeonB = 255
				sData.VehicleSetup.iModIndex[MOD_SPOILER] = 10
				sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 12
				sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 5
				sData.VehicleSetup.iModIndex[MOD_SKIRT] = 3
				sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 5
				sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 4
				sData.VehicleSetup.iModIndex[MOD_GRILL] = 4
				sData.VehicleSetup.iModIndex[MOD_BONNET] = 10
				sData.VehicleSetup.iModIndex[MOD_WING_L] = 2
				sData.VehicleSetup.iModIndex[MOD_ROOF] = 5
				sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
				sData.VehicleSetup.iModIndex[MOD_WHEELS] = 17
				sData.VehicleSetup.iModIndex[MOD_INTERIOR1] = 1
				sData.VehicleSetup.iModIndex[MOD_INTERIOR3] = 4
				sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 5
				sData.VehicleSetup.iModIndex[MOD_INTERIOR5] = 7
				sData.VehicleSetup.iModIndex[MOD_SEATS] = 6
				sData.VehicleSetup.iModIndex[MOD_STEERING] = 16
				sData.VehicleSetup.iModIndex[MOD_LIVERY] = 3
				RETURN TRUE
			ELIF iVehicle = GET_TAG_ID(SCRV_TAGS_MISSION_VEHICLE)
				IF data.Vehicle.Vehicles[iVehicle].model = REEVER
					sData.VehicleSetup.eModel = REEVER
					sData.VehicleSetup.tlPlateText = "77FQX299"
					sData.VehicleSetup.iColour1 = 138
					sData.VehicleSetup.iColour2 = 0
					sData.VehicleSetup.iColourExtra1 = 138
					sData.VehicleSetup.iColourExtra2 = 156
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWheelType = 6
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 2
					sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 3
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 4
					sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 5
					sData.VehicleSetup.iModIndex[MOD_ENGINE] = 2
					sData.VehicleSetup.iModIndex[MOD_BRAKES] = 2
					sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 2
					sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
					sData.VehicleSetup.iModIndex[MOD_WHEELS] = 50
					sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 50
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 1
					RETURN TRUE
				ELIF data.Vehicle.Vehicles[iVehicle].model = AVARUS
					sData.VehicleSetup.eModel = AVARUS
					sData.VehicleSetup.tlPlateText = "83RHD141"
					sData.VehicleSetup.iColour1 = 29
					sData.VehicleSetup.iColour2 = 142
					sData.VehicleSetup.iColourExtra1 = 28
					sData.VehicleSetup.iColourExtra2 = 156
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWheelType = 6
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					sData.VehicleSetup.iModIndex[MOD_SPOILER] = 8
					sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
					sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
					sData.VehicleSetup.iModIndex[MOD_SKIRT] = 5
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
					sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 3
					sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
					sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
					sData.VehicleSetup.iModIndex[MOD_ROOF] = 5
					sData.VehicleSetup.iModIndex[MOD_ENGINE] = 4
					sData.VehicleSetup.iModIndex[MOD_BRAKES] = 2
					sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 3
					sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
					sData.VehicleSetup.iModIndex[MOD_WHEELS] = 61
					sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 69
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 10
					RETURN TRUE
				ELIF data.Vehicle.Vehicles[iVehicle].model = SHINOBI
					sData.VehicleSetup.eModel = SHINOBI
					sData.VehicleSetup.tlPlateText = "49TGN381"
					sData.VehicleSetup.iPlateIndex = 3
					sData.VehicleSetup.iColour1 = 111
					sData.VehicleSetup.iColour2 = 28
					sData.VehicleSetup.iColourExtra1 = 0
					sData.VehicleSetup.iColourExtra2 = 0
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWheelType = 6
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 4
					sData.VehicleSetup.iModIndex[MOD_BRAKES] = 4
					sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
					sData.VehicleSetup.iModIndex[MOD_WHEELS] = 42
					sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 42
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 7
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC SCRV_VEHICLE_ATTRIBUTES(INT iVehicle, VEHICLE_INDEX vehId)
	UNUSED_PARAMETER(iVehicle)
	SWITCH GET_ENTITY_MODEL(vehId)
		CASE ASTRON		 
		CASE BALLER7	 
		CASE BUFFALO4
		CASE COMET7	
		CASE DEITY	
		CASE GRANGER2
		CASE IGNUS
		CASE JUBILEE	
		CASE PATRIOT3
		CASE YOUGA4	
		CASE ZENO
		CASE MULE5
		CASE CINQUEMILA
		CASE CHAMPION
		CASE REEVER
		CASE SHINOBI	 
		CASE IWAGEN		
			SET_VEHICLE_BITSET_DECORATOR(vehID,MP_DECORATOR_BS_IS_EARLY_MISSION_VEHICLE)
		BREAK
	ENDSWITCH
ENDPROC

PROC SCRV_VEHICLES_MAINTAIN_CLIENT(INT iVehicle, VEHICLE_INDEX vehId, BOOL bDriveable, BOOL bInVehicle, INT iMissionEntityForCarrier)
	UNUSED_PARAMETER(bDriveable)
	UNUSED_PARAMETER(bInVehicle)
	UNUSED_PARAMETER(iMissionEntityForCarrier)
	NETWORK_INDEX niVeh = serverBD.sVehicle[iVehicle].netId
	IF data.Vehicle.Vehicles[iVehicle].iInterior = 0
		IF IS_LOCAL_PLAYER_IN_MISSION_INTERIOR(SCRV_GET_MISSION_INTERIOR())
			IF IS_VEHICLE_BIT_SET(iVehicle,eVEHICLEBITSET_FROZEN)
				IF MAINTAIN_CONTROL_OF_NETWORK_ID(niVeh)
					FREEZE_ENTITY_POSITION(vehId,FALSE)
					CLEAR_VEHICLE_CLIENT_BIT(iVehicle,eVEHICLECLIENTBITSET_FROZEN)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SCRV_VEHICLES_MAINTAIN_DOORS(INT iVehicle, VEHICLE_INDEX vehID)
	SWITCH GET_SUBVARIATION()
		CASE FSS_RCV_LOCATION_1
			IF GET_TAG_ID(SCRV_TAGS_MISSION_VEHICLE) = iVehicle
				IF NOT IS_MISSION_CLIENT_BIT_SET(LOCAL_PARTICIPANT_INDEX,eMISSIONCLIENTBITSET_BACK_DOOR_OPENED)
					IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
						SET_VEHICLE_DOOR_OPEN(vehID,SC_DOOR_REAR_RIGHT,TRUE,TRUE)
						SET_VEHICLE_DOOR_OPEN(vehID,SC_DOOR_REAR_LEFT,TRUE,TRUE)
						SET_MISSION_CLIENT_BIT(eMISSIONCLIENTBITSET_BACK_DOOR_OPENED)
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE FSS_RCV_LOCATION_2
			IF GET_TAG_ID(SCRV_TAGS_MISSION_VEHICLE) = iVehicle
				IF data.Vehicle.Vehicles[iVehicle].model = COMET7
					IF NOT IS_MISSION_CLIENT_BIT_SET(LOCAL_PARTICIPANT_INDEX,eMISSIONCLIENTBITSET_BACK_DOOR_OPENED)
						IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
							LOWER_CONVERTIBLE_ROOF(vehID,TRUE)
							SET_MISSION_CLIENT_BIT(eMISSIONCLIENTBITSET_BACK_DOOR_OPENED)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE FSS_RCV_LOCATION_3
			IF GET_TAG_ID(SCRV_TAGS_TITAN) = iVehicle
				IF NOT IS_VEHICLE_DOOR_FULLY_OPEN(vehID, SC_DOOR_BOOT)
					IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
						SET_VEHICLE_DOOR_OPEN(vehID,SC_DOOR_BOOT)
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC SCRV_ENTITY_ON_DAMAGED(INT iEntity, STRUCT_ENTITY_DAMAGE_EVENT sEvent, PLAYER_INDEX damager)
	UNUSED_PARAMETER(sEvent)
	UNUSED_PARAMETER(damager)
	IF iEntity = GET_TAG_ID(SCRV_TAGS_MISSION_VEHICLE)
		RESET_NET_TIMER(serverBD.sMissionData.stMonkeyTimer)
		START_NET_TIMER(serverBD.sMissionData.stMonkeyTimer)
	ENDIF
ENDPROC

FUNC VECTOR SCRV_GET_VEHICLE_ALT_POSITION(INT iVehicle)
	IF iVehicle = GET_TAG_ID(SCRV_TAGS_MISSION_VEHICLE)
		RETURN <<-1259.5094, -3019.3269, -49.4903>>
	ELIF iVehicle = GET_TAG_ID(SCRV_TAGS_VEHICLE_4)
		RETURN <<-1262.4624, -3018.8726, -49.4903>>
	ENDIF
	RETURN data.Vehicle.Vehicles[iVehicle].vCoords
ENDFUNC

FUNC FLOAT SCRV_GET_VEHICLE_ALT_HEADING(INT iVehicle)
	IF iVehicle = GET_TAG_ID(SCRV_TAGS_MISSION_VEHICLE)
		RETURN 248.3860
	ELIF iVehicle = GET_TAG_ID(SCRV_TAGS_VEHICLE_4)
		RETURN 247.9225
	ENDIF
	RETURN data.Vehicle.Vehicles[iVehicle].fHeading
ENDFUNC

PROC SCRV_RESET_VEHICLE_TO_ALT_POSITION(INT iVehicle, VEHICLE_INDEX vehId)
	VECTOR vCoord = GET_ENTITY_COORDS(vehId)
	// Angled area that covers the back of the Titan
	VECTOR v1 = <<-1266.9629, -3021.0977, -50.0>>
	VECTOR v2 = <<-1266.9049, -3013.0728, -45.0>>
//	DRAW_ANGLED_AREA_FROM_FACES(v1,v2,3.0,255,0,0,128)
	IF IS_ENTITY_IN_ANGLED_AREA(NET_TO_ENT(serverBD.sVehicle[iVehicle].netId),v1,v2,3.0,TRUE)
	AND vCoord.z < -48.0					// if the bike is on the hangar floor height (below the Titan floor height)
		IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
			SET_ENTITY_COORDS(vehId,SCRV_GET_VEHICLE_ALT_POSITION(iVehicle))
			SET_VEHICLE_ON_GROUND_PROPERLY(vehId)
			SET_ENTITY_HEADING(vehId,SCRV_GET_VEHICLE_ALT_HEADING(iVehicle))
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - SCRV_RESET_VEHICLE_TO_ALT_POSITION - Bike reset to titan")
		ENDIF
	ENDIF
ENDPROC

PROC SCRV_VEHICLE_MAINTAIN_CLIENT(INT iVehicle, VEHICLE_INDEX vehId, BOOL bDriveable, BOOL bInVehicle, INT iMissionEntityForCarrier)
	UNUSED_PARAMETER(bInVehicle)
	UNUSED_PARAMETER(bDriveable)
	UNUSED_PARAMETER(iMissionEntityForCarrier)
	IF GET_SUBVARIATION() != FSS_RCV_LOCATION_3
		EXIT
	ENDIF
	IF iVehicle = GET_TAG_ID(SCRV_TAGS_MISSION_VEHICLE)
	OR iVehicle = GET_TAG_ID(SCRV_TAGS_VEHICLE_4)
		SCRV_RESET_VEHICLE_TO_ALT_POSITION(iVehicle,vehId)
	ENDIF
ENDPROC

/////////////////
///    MISSION ENTITY
/////////////////

PROC SCRV_MISSION_ENTITY_ON_COLLECT(INT iMissionEntity)
	UNUSED_PARAMETER(iMissionEntity)
	IF NOT IS_MISSION_SERVER_BIT_SET(eMISSIONSERVERBITSET_MISSION_ENTITY_COLLECTED_TICKER)
		MISSION_TICKER_DATA sTickerData
		sTickerData.iInt1 = SCRV_TICKER_VEHICLE_COLLECTED
		SEND_FM_CONTENT_TICKER_EVENT_WITH_DATA(eFMCTICKER_CUSTOM, sTickerData) 
		SET_MISSION_CLIENT_BIT(eMISSIONCLIENTBITSET_MISSION_ENTITY_COLLECTED_TICKER)
	ENDIF
ENDPROC

FUNC BOOL SCRV_MISSION_ENTITY_BLIP_ENABLE(INT iMissionEntity)
	UNUSED_PARAMETER(iMissionEntity)
	
	SWITCH GET_CLIENT_MODE_STATE() 
		CASE eMODESTATE_COLLECT_MISSION_ENTITY
		CASE eMODESTATE_DELIVER_MISSION_ENTITY
		CASE eMODESTATE_HELP_DELIVER_MISSION_ENTITY
		CASE eMODESTATE_RECOVER_MISSION_ENTITY
			IF IS_MISSION_ENTITY_IN_MISSION_INTERIOR(SCRV_MISSION_ENTITY_ID)
				RETURN IS_LOCAL_PLAYER_IN_MISSION_INTERIOR(SCRV_GET_MISSION_INTERIOR())
			ENDIF
			RETURN TRUE
	ENDSWITCH
	IF  GET_CLIENT_MODE_STATE_ID() = ENUM_TO_INT(eSCRVSTATE_UNLOCK_DOOR)
	AND NOT IS_MISSION_CLIENT_BIT_SET(LOCAL_PARTICIPANT_INDEX, eMISSIONCLIENTBITSET_IMANI_TEXT_RECEIVED)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BLIP_SPRITE SCRV_MISSION_ENTITY_BLIP_SPRITE(INT iMissionEntity)
	UNUSED_PARAMETER(iMissionEntity)
	SWITCH GET_SUBVARIATION()
		CASE FSS_RCV_LOCATION_1		
		CASE FSS_RCV_LOCATION_2			RETURN RADAR_TRACE_GANG_VEHICLE
		CASE FSS_RCV_LOCATION_3			RETURN RADAR_TRACE_GANG_BIKE
	ENDSWITCH
	RETURN RADAR_TRACE_INVALID
ENDFUNC

FUNC STRING SCRV_MISSION_ENTITY_BLIP_NAME(INT iMissionEntity)
	UNUSED_PARAMETER(iMissionEntity)
	RETURN "FXR_BLIP_VEH"
ENDFUNC

FUNC BOOL SCRV_MISSION_ENTITY_BLIP_DRAW_MARKER(INT iEntity, ENTITY_INDEX entityId)
	UNUSED_PARAMETER(iEntity)
	UNUSED_PARAMETER(entityId)
	RETURN FALSE
ENDFUNC

FUNC BOOL SCRV_MISSION_ENTITY_VISIBLE_RIVAL(INT iMissionEntity)
	UNUSED_PARAMETER(iMissionEntity)
	RETURN GET_MODE_STATE_ID() >= ENUM_TO_INT(eSCRVSTATE_COLLECT_MISSION_ENTITY) AND NOT IS_MISSION_ENTITY_IN_MISSION_INTERIOR(0)
ENDFUNC

/////////////////
///    PORTAL
/////////////////

FUNC BOOL SCRV_SHOULD_PORTAL_TRIGGER(INT iPortal, VECTOR vCoord)
	
	SWITCH iPortal
		CASE SCRV_PORTAL_ID_WALK_ENTRY
			IF IS_PED_IN_ANY_VEHICLE(LOCAL_PED_INDEX)
				RETURN FALSE
			ENDIF
		BREAK
	ENDSWITCH	
	
	IF NOT CALL logic.Portal.Blip.Enable(iPortal)
		RETURN FALSE
	ENDIF
	IF IS_PED_IN_ANY_VEHICLE(LOCAL_PED_INDEX,TRUE)
		RETURN IS_ENTITY_AT_COORD(LOCAL_PED_INDEX, vCoord, <<2.0, 2.0, 2.0>>)
	ELSE
		RETURN IS_ENTITY_AT_COORD(LOCAL_PED_INDEX, vCoord, <<1.0, 1.0, 2.0>>)
	ENDIF
ENDFUNC

FUNC BOOL SCRV_SHOULD_PORTAL_DRAW_CORONA(INT iPortal)
	RETURN CALL logic.Portal.Blip.Enable(iPortal)
ENDFUNC

FUNC BOOL SCRV_SHOULD_ENABLE_PORTAL_BLIP(INT iPortal)
	SWITCH iPortal
		CASE SCRV_PORTAL_ID_WALK_ENTRY	
			RETURN 	NOT IS_DIALOGUE_PLAYING(SCRV_DIALOGUE_ID_OPENING)
					AND GET_MODE_STATE_ID() > ENUM_TO_INT(eSCRVSTATE_GO_TO) 
					AND IS_MISSION_ENTITY_IN_MISSION_INTERIOR(SCRV_MISSION_ENTITY_ID) 
					AND NOT IS_LOCAL_PLAYER_IN_MISSION_INTERIOR(SCRV_GET_MISSION_INTERIOR())
		CASE SCRV_PORTAL_ID_WALK_EXIT	
			RETURN GET_MODE_STATE_ID() > ENUM_TO_INT(eSCRVSTATE_COLLECT_MISSION_ENTITY) 
					AND IS_LOCAL_PLAYER_IN_MISSION_INTERIOR(SCRV_GET_MISSION_INTERIOR()) 
					AND NOT IS_PLAYER_IN_VEHICLE_WITH_ANY_MISSION_ENTITY(LOCAL_PLAYER_INDEX)
					AND NOT IS_MISSION_ENTITY_IN_MISSION_INTERIOR(SCRV_MISSION_ENTITY_ID)
		CASE SCRV_PORTAL_ID_VEHICLE_EXIT
			RETURN GET_MODE_STATE_ID() > ENUM_TO_INT(eSCRVSTATE_COLLECT_MISSION_ENTITY) 
					AND IS_LOCAL_PLAYER_IN_MISSION_INTERIOR(SCRV_GET_MISSION_INTERIOR()) 
					AND IS_PLAYER_IN_VEHICLE_WITH_ANY_MISSION_ENTITY(LOCAL_PLAYER_INDEX)
					AND IS_MISSION_ENTITY_IN_MISSION_INTERIOR(SCRV_MISSION_ENTITY_ID)
		ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL SCRV_SHOULD_DRAW_GPS_PORTAL_BLIP(INT iPortal)
	unused_parameter(iPortal)
	RETURN FALSE
ENDFUNC

FUNC BOOL SCRV_PORTAL_KEEP_VEHICLE(INT iPortal)
	SWITCH iPortal
		CASE SCRV_PORTAL_ID_VEHICLE_EXIT	RETURN TRUE
	ENDSWITCH
	RETURN FALSE
ENDFUNC

//////////////////////////
///   INTERACT
//////////////////////////

PROC SCRV_MAINTAIN_INTERACT(INT iLocation)
	SWITCH iLocation
		CASE SCRV_INTERACT_KEYPAD
			IF NOT IS_MISSION_LOCAL_BIT_SET(eMISSIONLOCALBITSET_BUTTON_SOUND_TRIGGERED)
				IF (IS_SYNCHRONIZED_SCENE_RUNNING(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sInteract.SyncScene.iNetID))
				AND GET_SYNCHRONIZED_SCENE_PHASE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sInteract.SyncScene.iNetID)) > 0.4)
					PLAY_SOUND_FROM_COORD(-1, "Press", data.Interact.Locations[SCRV_INTERACT_KEYPAD].vCoords, "DLC_SECURITY_BUTTON_PRESS_SOUNDS")
					SET_MISSION_LOCAL_BIT(eMISSIONLOCALBITSET_BUTTON_SOUND_TRIGGERED)
					PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - SCRV_MAINTAIN_INTERACT - Done button press sound")
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL SCRV_INTERACT_ENABLE_LOCATION(INT iLocation)
	unused_parameter(iLocation)
	RETURN TRUE
ENDFUNC

FUNC BOOL SCRV_INTERACT_BLIP_ENABLE(INT iLocation)
	unused_parameter(iLocation)
	RETURN FALSE
ENDFUNC

FUNC STRING SCRV_GET_INTERACT_CONTEXT_INTENTION_HELP_TEXT(INT iLocation)
	SWITCH iLocation
		CASE SCRV_INTERACT_KEYPAD
			RETURN "FXR_INT_RV_UD"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

PROC SCRV_INTERACT_ON_COMPLETE(INT iLocation)
	SWITCH iLocation
		CASE SCRV_INTERACT_KEYPAD
			MISSION_TICKER_DATA sTickerData
			sTickerData.iInt1 = SCRV_TICKER_DOOR_UNLOCKED
			SEND_FM_CONTENT_TICKER_EVENT_WITH_DATA(eFMCTICKER_CUSTOM, sTickerData) 
			PLAY_SOUND_FRONTEND(-1, "Collect_Pass", "PrSCRV_Elevator_Pass_Sounds")
		BREAK			
	ENDSWITCH
ENDPROC

FUNC STRING SCRV_GET_INTERACT_ANIM_DICT(INT iLocation)
	SWITCH iLocation
		CASE SCRV_INTERACT_KEYPAD
			IF IS_PLAYER_FEMALE()
				RETURN "ANIM_HEIST@HS3F@IG6_PUSH_BUTTON@FEMALE@"
			ELSE
				RETURN "ANIM_HEIST@HS3F@IG6_PUSH_BUTTON@MALE@"
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC STRING SCRV_GET_INTERACT_ANIM(INT iLocation)
	SWITCH iLocation
		CASE SCRV_INTERACT_KEYPAD
			SWITCH sInteract.Anim.iStage
				CASE 0
					RETURN "push_button"
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC VECTOR SCRV_GET_INTERACT_ANIM_COORDS(INT iInteract)

	VECTOR vLocation
	OBJECT_INDEX objectId
	
	SWITCH iInteract
		CASE SCRV_INTERACT_KEYPAD	
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sProp[SCRV_GET_INTERACT_PROP_KEYPAD()].netID) 
				
					objectId = NET_TO_OBJ(serverBD.sProp[SCRV_GET_INTERACT_PROP_KEYPAD()].netID) 
					
					IF DOES_ENTITY_EXIST(objectId)					
						vLocation = GET_ENTITY_COORDS(objectId)
					ENDIF
				ENDIF
		BREAK
	ENDSWITCH
	RETURN vLocation
ENDFUNC

FUNC FLOAT SCRV_GET_INTERACT_ANIM_HEADING(INT iLocation)
	SWITCH iLocation
		CASE SCRV_INTERACT_KEYPAD		
			RETURN data.Prop.Props[SCRV_GET_INTERACT_PROP_KEYPAD()].fHeading
		BREAK
	ENDSWITCH
	
	RETURN 0.0
ENDFUNC

FUNC VECTOR SCRV_GET_INTERACT_FACING_COORDS(INT iLocation)
	SWITCH iLocation
		CASE SCRV_INTERACT_KEYPAD
			RETURN data.Prop.Props[SCRV_GET_INTERACT_PROP_KEYPAD()].vCoords
	ENDSWITCH
	
	RETURN CALL logic.Interact.Coords(iLocation)
ENDFUNC

FUNC VECTOR SCRV_GET_INTERACT_FORWARD_DIRECTION(INT iLocation)
	SWITCH iLocation
		CASE SCRV_INTERACT_KEYPAD
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sProp[SCRV_GET_INTERACT_PROP_KEYPAD()].netID)
				RETURN -GET_ENTITY_FORWARD_VECTOR(NET_TO_ENT(serverBD.sProp[SCRV_GET_INTERACT_PROP_KEYPAD()].netID))
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

/////////////////
///    TEXT MESSAGE
/////////////////

FUNC STRING SCRV_GET_TEXT_LABEL(INT iText)
	SWITCH iText
		CASE SCRV_TEXT_MESSAGE_IMANI_TEXT
			SWITCH GET_SUBVARIATION()
				CASE FSS_RCV_LOCATION_1					RETURN "FXR_TXT_KP1"
				CASE FSS_RCV_LOCATION_2					RETURN "FXR_TXT_KP2"
				CASE FSS_RCV_LOCATION_3					RETURN "FXR_TXT_KP3"
			ENDSWITCH
		BREAK
		CASE SCRV_TEXT_MESSAGE_HUMANE_MID_POINT										RETURN "FXR_TXT_HMP"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC BOOL SCRV_SEND_TEXT(INT iText)
	SWITCH iText
		CASE SCRV_TEXT_MESSAGE_IMANI_TEXT				RETURN IS_MISSION_CLIENT_BIT_SET(LOCAL_PARTICIPANT_INDEX, eMISSIONCLIENTBITSET_IMANI_TEXT_RECEIVED)
		CASE SCRV_TEXT_MESSAGE_HUMANE_MID_POINT			RETURN GET_SUBVARIATION() = FSS_RCV_LOCATION_1 AND IS_PLAYER_IN_TRIGGER_AREA(LOCAL_PARTICIPANT_INDEX,SCRV_HUMANE_MID_POINT_TRIGGER)
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC enumCharacterList SCRV_GET_TEXT_CHAR(INT iText)
	SWITCH iText
		CASE SCRV_TEXT_MESSAGE_IMANI_TEXT			
		CASE SCRV_TEXT_MESSAGE_HUMANE_MID_POINT			RETURN CHAR_FIXER_IMANI
	ENDSWITCH
	RETURN CHAR_FIXER_IMANI
ENDFUNC

FUNC enumTxtMsgMissionCritical SCRV_GET_TEXT_CRIT_STATUS(INT iText)
	SWITCH iText
		CASE SCRV_TEXT_MESSAGE_IMANI_TEXT			RETURN TXTMSG_CRITICAL
	ENDSWITCH
	RETURN TXTMSG_NOT_CRITICAL
ENDFUNC

FUNC enumTxtMsgLockedStatus SCRV_GET_TEXT_LOCK_STATUS(INT iText)
	SWITCH iText
		CASE SCRV_TEXT_MESSAGE_IMANI_TEXT		RETURN TXTMSG_LOCKED	
	ENDSWITCH
	RETURN TXTMSG_UNLOCKED
ENDFUNC

/////////////////
///    DIALOGUES
/////////////////

FUNC STRING SCRV_GET_DIALOGUE_ROOT(INT iDialogue)
	SWITCH GET_SUBVARIATION()
		CASE FSS_RCV_LOCATION_1
			SWITCH iDialogue
				CASE SCRV_DIALOGUE_ID_OPENING						RETURN "FXFR_SRV_1A"
			ENDSWITCH
		BREAK
		CASE FSS_RCV_LOCATION_2
			SWITCH iDialogue
				CASE SCRV_DIALOGUE_ID_OPENING						RETURN "FXFR_SRV_2A"
			ENDSWITCH
		BREAK
		CASE FSS_RCV_LOCATION_3
			SWITCH iDialogue
				CASE SCRV_DIALOGUE_ID_OPENING						RETURN "FXFR_SRV_3A"
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC STRING SCRV_GET_DIALOGUE_SUBTITLE_GROUP(INT iDialogue)
	UNUSED_PARAMETER(iDialogue)

	RETURN "FXFRAUD"
ENDFUNC

FUNC STRING SCRV_GET_DIALOGUE_CHARACTER(INT iDialogue, INT iSpeaker)
	UNUSED_PARAMETER(iDialogue)
	UNUSED_PARAMETER(iSpeaker)
	
	RETURN "FIX_FRANKLIN"
ENDFUNC

FUNC BOOL SCRV_SHOULD_TRIGGER_DIALOGUE(INT iDialogue)
	SWITCH iDialogue
		CASE  SCRV_DIALOGUE_ID_OPENING					RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SCRV_USE_HEADSET_AUDIO(INT iDialogue)
	SWITCH iDialogue
		CASE  SCRV_DIALOGUE_ID_OPENING					RETURN FALSE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC enumCharacterList SCRV_GET_CELLPHONE_CHARACTER(INT iDialogue)
	UNUSED_PARAMETER(iDialogue)
	
	RETURN CHAR_FIXER_FRANKLIN
ENDFUNC

///////////////////////
///    INTERIOR
///////////////////////

FUNC BOOL SCRV_SHOULD_KEEP_VEHICLE_WHEN_WARPED_OUT_MISSION_INTERIOR()
	RETURN FALSE
ENDFUNC

FUNC VECTOR SCRV_INTERIOR_DISPLACED_COORD(INT iInterior)
	UNUSED_PARAMETER(iInterior)
	
	RETURN data.Portal[SCRV_PORTAL_ID_WALK_ENTRY].vCoord
ENDFUNC

///////////////////////
///    CUTSCENE
///////////////////////

FUNC BOOL SCRV_SHOULD_ENABLE_CUTSCENE()
	RETURN GET_END_REASON() = eENDREASON_NO_REASON_YET
ENDFUNC

FUNC BOOL SCRV_SHOULD_CUTSCENE_START(INT iScene)
	IF IS_LOCAL_PLAYER_USING_PORTAL()
	AND DOES_PORTAL_HAVE_CUTSCENE(GET_PORTAL_BEING_USED())
	AND GET_PORTAL_CUTSCENE(GET_PORTAL_BEING_USED()) = iScene
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC PLAYER_INDEX SCRV_GET_CUTSCENE_PLAYER_ID(INT iScene, INT iPlayerClone)
	UNUSED_PARAMETER(iScene)
	UNUSED_PARAMETER(iPlayerClone)
	
	RETURN LOCAL_PLAYER_INDEX
ENDFUNC

FUNC VECTOR SCRV_GET_CUTSCENE_COORDS(INT iScene)
	SWITCH iScene
		CASE SCRV_CUTSCENE_ENTER
			SWITCH GET_SUBVARIATION()
				CASE FSS_RCV_LOCATION_1
					RETURN << 3526.48, 3737.55, 36.231 >>
				CASE FSS_RCV_LOCATION_2
					RETURN << 113.561, -1589.21, 29.092 >>
				CASE FSS_RCV_LOCATION_3
					RETURN << -1245.34, -3471.93, 13.445 >>
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC FLOAT SCRV_GET_CUTSCENE_HEADING(INT iScene)
	SWITCH iScene		
		CASE SCRV_CUTSCENE_ENTER
			SWITCH GET_SUBVARIATION()
				CASE FSS_RCV_LOCATION_1
					RETURN 0.0000
				CASE FSS_RCV_LOCATION_2
					RETURN 58.0094
				CASE FSS_RCV_LOCATION_3
					IF serverBD.sMissionData.bBossOwnsHangar
						RETURN 236.8418
					ELSE
						RETURN 337.1128
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN 0.0
ENDFUNC

FUNC BOOL SCRV_SHOULD_CUTSCENE_FADE_OUT(INT iScene)
	FLOAT fDurationMS
	FLOAT fFadeOutStartTimeMS
	
	SWITCH iScene
		CASE SCRV_CUTSCENE_ENTER
			fDurationMS = GET_ANIM_DURATION(data.Cutscene.Scenes[iScene].sAnimDict, data.Cutscene.Scenes[iScene].sCameraAnim) * 1000
			fFadeOutStartTimeMS = (fDurationMS * SCRV_CUTSCENE_ENTER_END_PHASE) - data.Cutscene.Scenes[iScene].iFadeOutTime
		BREAK
	ENDSWITCH
	
	RETURN GET_SYNCHRONIZED_SCENE_PHASE(sCutscene.sScenes[iScene].iID) > fFadeOutStartTimeMS / fDurationMS
ENDFUNC

FUNC BOOL SCRV_SHOULD_CUTSCENE_FINISH(INT iScene)
	SWITCH iScene
		CASE SCRV_CUTSCENE_ENTER
			RETURN GET_SYNCHRONIZED_SCENE_PHASE(sCutscene.sScenes[iScene].iID) > SCRV_CUTSCENE_ENTER_END_PHASE
	ENDSWITCH
	
	RETURN GET_SYNCHRONIZED_SCENE_PHASE(sCutscene.sScenes[iScene].iID) > 0.99
ENDFUNC

PROC SCRV_CUTSCENE_MAINTAIN_RUN(INT iScene)
	SWITCH iScene
		CASE SCRV_CUTSCENE_ENTER
			SWITCH GET_SUBVARIATION()
				CASE FSS_RCV_LOCATION_1
				CASE FSS_RCV_LOCATION_3
					IF NOT IS_MISSION_LOCAL_BIT_SET(eMISSIONLOCALBITSET_DOOR_SOUND_TRIGGERED)
					AND IS_SYNCHRONIZED_SCENE_RUNNING(sCutscene.sScenes[iScene].iID)				
					AND GET_SYNCHRONIZED_SCENE_PHASE(sCutscene.sScenes[iScene].iID) >= 0.29
						PLAY_SOUND_FRONTEND(-1, "Metal_Door_Push", "GTAO_Script_Doors_Sounds")	
						SET_MISSION_LOCAL_BIT(eMISSIONLOCALBITSET_DOOR_SOUND_TRIGGERED)
					ENDIF
				BREAK
				CASE FSS_RCV_LOCATION_2
					IF NOT IS_MISSION_LOCAL_BIT_SET(eMISSIONLOCALBITSET_DOOR_SOUND_TRIGGERED)
					AND IS_SYNCHRONIZED_SCENE_RUNNING(sCutscene.sScenes[iScene].iID)				
					AND GET_SYNCHRONIZED_SCENE_PHASE(sCutscene.sScenes[iScene].iID) >= 0.11
						PLAY_SOUND_FRONTEND(-1, "Barge_Door_Metal", "DLC_Security_Door_Barge_Sounds")	
						SET_MISSION_LOCAL_BIT(eMISSIONLOCALBITSET_DOOR_SOUND_TRIGGERED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

FUNC VECTOR SCRV_CUTSCENE_LOAD_SCENE_COORDS(INT iScene)
	RETURN SCRV_GET_CUTSCENE_COORDS(iScene)
ENDFUNC

//////////////////////////
///    SEARCH AREA
//////////////////////////

FUNC BOOL SCRV_SEARCH_AREA_BLIP_ENABLE(INT iSearchArea)
	UNUSED_PARAMETER(iSearchArea)
	RETURN FALSE
ENDFUNC

FUNC BOOL SCRV_IS_MISSION_ENTITY_INSIDE_TITAN()
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[GET_TAG_ID(SCRV_TAGS_MISSION_VEHICLE)].netId)
	AND IS_ENTITY_ALIVE(NET_TO_ENT(serverBD.sVehicle[GET_TAG_ID(SCRV_TAGS_MISSION_VEHICLE)].netId))
		VECTOR vAltPos = data.Vehicle.Vehicles[GET_TAG_ID(SCRV_TAGS_MISSION_VEHICLE)].vCoords
		VECTOR vMEPos = GET_ENTITY_COORDS(NET_TO_ENT(serverBD.sVehicle[GET_TAG_ID(SCRV_TAGS_MISSION_VEHICLE)].netId))
		RETURN VDIST(vAltPos,vMEPos) < 2.0
	ENDIF
	RETURN TRUE
ENDFUNC

FUNC BOOL SCRV_CAN_SEE_VEHICLE
	IF GET_SUBVARIATION() = FSS_RCV_LOCATION_3
		IF SCRV_IS_MISSION_ENTITY_INSIDE_TITAN()
		AND NOT IS_PLAYER_IN_TRIGGER_AREA(LOCAL_PARTICIPANT_INDEX, 4)
			RETURN FALSE
		ENDIF
	ENDIF
	RETURN IS_ENTITY_ON_SCREEN(NET_TO_ENT(serverBD.sVehicle[GET_TAG_ID(SCRV_TAGS_MISSION_VEHICLE)].netId))
ENDFUNC

FUNC BOOL SCRV_SEARCH_AREA_HAS_TRIGGERED(INT iSearchArea)
	IF sSearchArea.fTriggerDistanceSquared < data.SearchArea.Areas[iSearchArea].fTriggerRadius * data.SearchArea.Areas[iSearchArea].fTriggerRadius
		IF SCRV_CAN_SEE_VEHICLE()
			RETURN TRUE
		ENDIF
	ENDIF	
	RETURN FALSE
ENDFUNC

FUNC VECTOR SCRV_SEARCH_AREA_TRIGGER_COORDS(INT iSearchArea)
	IF GET_SUBVARIATION() = FSS_RCV_LOCATION_3
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[GET_TAG_ID(SCRV_TAGS_MISSION_VEHICLE)].netId)
		AND NOT SCRV_IS_MISSION_ENTITY_INSIDE_TITAN()
			RETURN GET_ENTITY_COORDS(NET_TO_ENT(serverBD.sVehicle[GET_TAG_ID(SCRV_TAGS_MISSION_VEHICLE)].netId), FALSE)
		ENDIF
	ENDIF
	
	RETURN data.SearchArea.Areas[iSearchArea].vCoords
ENDFUNC


//////////////////////////
///    AMBUSH
//////////////////////////

FUNC BOOL SCRV_SHOULD_ACTIVATE_AMBUSH()
	IF IS_MISSION_SERVER_BIT_SET(eMISSIONSERVERBITSET_AMBUSH_STARTED)
		RETURN TRUE
	ENDIF	
	RETURN FALSE
ENDFUNC

FUNC BOOL SCRV_AMBUSH_IS_TARGET_VALID(INT iEntity, ENTITY_INDEX targetId)
	UNUSED_PARAMETER(iEntity)
	UNUSED_PARAMETER(targetId)
	IF NOT IS_MISSION_SERVER_BIT_SET(eMISSIONSERVERBITSET_AMBUSH_STARTED)
		RETURN FALSE
	ENDIF	
	RETURN TRUE
ENDFUNC

//////////////////////////
///    TICKER
//////////////////////////

FUNC STRING SCRV_TICKER_CUSTOM_LOCAL(MISSION_TICKER_DATA &sTickerData)
	IF sTickerData.iInt1 != -1
		SWITCH sTickerData.iInt1
			CASE SCRV_TICKER_DOOR_UNLOCKED
				SWITCH GET_SUBVARIATION()
					CASE FSS_RCV_LOCATION_1									RETURN "FXR_TIC_DU1L"
					CASE FSS_RCV_LOCATION_2									RETURN "FXR_TIC_DU2L"
					CASE FSS_RCV_LOCATION_3									RETURN "FXR_TIC_DU3L"
				ENDSWITCH
			BREAK
			CASE SCRV_TICKER_VEHICLE_COLLECTED
				SWITCH GET_SUBVARIATION()
					CASE FSS_RCV_LOCATION_1									RETURN "FXR_TIC_CVA1L"
					CASE FSS_RCV_LOCATION_2
						SWITCH serverBD.iRandomModeInt1
							CASE 0					RETURN "FXR_TIC_CV2L"
							CASE 1					RETURN "FXR_TIC_CV2L2"
							CASE 2					RETURN "FXR_TIC_CV2L3"
						ENDSWITCH
					BREAK
					CASE FSS_RCV_LOCATION_3	
						SWITCH serverBD.iRandomModeInt1
							CASE 0					RETURN "FXR_TIC_CV3L"
							CASE 1					RETURN "FXR_TIC_CV3L2"
							CASE 2					RETURN "FXR_TIC_CV3L3"
						ENDSWITCH
					BREAK
				ENDSWITCH
			BREAK
		ENDSWITCH
	ENDIF
	
	
	RETURN ""
ENDFUNC

FUNC STRING SCRV_TICKER_CUSTOM_REMOTE(MISSION_TICKER_DATA &sTickerData)
	IF sTickerData.iInt1 != -1
		SWITCH sTickerData.iInt1
			CASE SCRV_TICKER_DOOR_UNLOCKED
				SWITCH GET_SUBVARIATION()
					CASE FSS_RCV_LOCATION_1									RETURN "FXR_TIC_DU1R"
					CASE FSS_RCV_LOCATION_2									RETURN "FXR_TIC_DU2R"
					CASE FSS_RCV_LOCATION_3									RETURN "FXR_TIC_DU3R"
				ENDSWITCH
			BREAK
			CASE SCRV_TICKER_VEHICLE_COLLECTED
				SWITCH GET_SUBVARIATION()
					CASE FSS_RCV_LOCATION_1									RETURN "FXR_TIC_CVA1R"
					CASE FSS_RCV_LOCATION_2
						SWITCH serverBD.iRandomModeInt1
							CASE 0					RETURN "FXR_TIC_CV2R"
							CASE 1					RETURN "FXR_TIC_CV2R2"
							CASE 2					RETURN "FXR_TIC_CV2R3"
						ENDSWITCH
					BREAK
					CASE FSS_RCV_LOCATION_3	
						SWITCH serverBD.iRandomModeInt1
							CASE 0					RETURN "FXR_TIC_CV3R"
							CASE 1					RETURN "FXR_TIC_CV3R2"
							CASE 2					RETURN "FXR_TIC_CV3R3"
						ENDSWITCH
					BREAK
				ENDSWITCH
			BREAK
		ENDSWITCH
	ENDIF
	
	RETURN ""
ENDFUNC

//////////////////////////
///    HELP TEXT
//////////////////////////

FUNC BOOL SCRV_SHOULD_TRIGGER_HELP_TEXT(INT iHelp)
	SWITCH iHelp
		CASE SCRV_HELP_TEXT_KEYPAD 			RETURN GET_CLIENT_MODE_STATE_ID() = ENUM_TO_INT(eSCRVSTATECLIENT_UNLOCK_DOOR) AND IS_MISSION_CLIENT_BIT_SET(LOCAL_PARTICIPANT_INDEX, eMISSIONCLIENTBITSET_IMANI_TEXT_RECEIVED)
		CASE SCRV_HELP_LOCATE_VEHICLE		RETURN GET_CLIENT_MODE_STATE_ID() = ENUM_TO_INT(eSCRVSTATECLIENT_LOCATE_VEHICLE) AND IS_LOCAL_PLAYER_IN_MISSION_INTERIOR(SCRV_GET_MISSION_INTERIOR())
		CASE SCRV_HELP_HUMANE_MID_POINT		RETURN GET_SUBVARIATION() = FSS_RCV_LOCATION_1 AND IS_PLAYER_IN_TRIGGER_AREA(LOCAL_PARTICIPANT_INDEX, SCRV_HUMANE_MID_POINT_TRIGGER)
		CASE SCRV_HELP_BOXVILLE_ENTER		RETURN GET_SUBVARIATION() = FSS_RCV_LOCATION_1 AND GET_CLIENT_MODE_STATE_ID() = ENUM_TO_INT(eSCRVSTATECLIENT_UNLOCK_DOOR)
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC STRING SCRV_GET_HELP_TEXT_LABEL(INT iHelp)
	SWITCH iHelp
		CASE SCRV_HELP_TEXT_KEYPAD
			SWITCH GET_SUBVARIATION()
				CASE FSS_RCV_LOCATION_1				RETURN "FXR_HT_RV_KP1"
				CASE FSS_RCV_LOCATION_2				RETURN "FXR_HT_RV_KP2"
				CASE FSS_RCV_LOCATION_3				RETURN "FXR_HT_RV_KP3"
			ENDSWITCH
		BREAK
		CASE SCRV_HELP_LOCATE_VEHICLE
			SWITCH GET_SUBVARIATION()
				CASE FSS_RCV_LOCATION_1				RETURN "FXR_HT_RV_LV1"
				CASE FSS_RCV_LOCATION_2
					SWITCH serverBD.iRandomModeInt1
						CASE 0					RETURN "FXR_HT_RV_LV21"
						CASE 1					RETURN "FXR_HT_RV_LV22"
						CASE 2					RETURN "FXR_HT_RV_LV23"
					ENDSWITCH
				BREAK
				CASE FSS_RCV_LOCATION_3	
					SWITCH serverBD.iRandomModeInt1
						CASE 0					RETURN "FXR_HT_RV_LV31"
						CASE 1					RETURN "FXR_HT_RV_LV32"
						CASE 2					RETURN "FXR_HT_RV_LV33"
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE SCRV_HELP_HUMANE_MID_POINT		RETURN "FXR_HT_HUM_MID"
		CASE SCRV_HELP_BOXVILLE_ENTER		RETURN "FXR_HT_BOXV_ENT"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

//////////////////////////
///    SPAWNING
//////////////////////////

FUNC BOOL SCRV_USE_CUSTOM_SPAWNS
	RETURN IS_LOCAL_PLAYER_IN_MISSION_INTERIOR(SCRV_GET_MISSION_INTERIOR())
ENDFUNC

//////////////////////////
///   AUDIO
//////////////////////////

FUNC INT SCRV_GET_NEXT_MUSIC_EVENT(INT iCurrentEvent)
	SWITCH INT_TO_ENUM(SCRV_MUSIC, iCurrentEvent)
	
		CASE eSCRV_MUSIC_INIT
			RETURN ENUM_TO_INT(eSCRV_MUSIC_START)
	
		CASE eSCRV_MUSIC_START
			IF GET_MODE_STATE_ID() = ENUM_TO_INT(eSCRVSTATE_LOCATE_VEHICLE)
				RETURN ENUM_TO_INT(eSCRV_MUSIC_ENTER_LOCATION)
			ENDIF
		BREAK
		
		CASE eSCRV_MUSIC_ENTER_LOCATION
			IF HAS_PED_BEEN_TRIGGERED(0)
				RETURN ENUM_TO_INT(eSCRV_MUSIC_GUNFIGHT)
			ENDIF
		BREAK
		
		CASE eSCRV_MUSIC_GUNFIGHT
			IF GET_MODE_STATE_ID() = ENUM_TO_INT(eSCRVSTATE_UNLOCK_DOOR)
				RETURN ENUM_TO_INT(eSCRV_MUSIC_UNLOCK_DOOR)
			ENDIF
		BREAK
				
		CASE eSCRV_MUSIC_UNLOCK_DOOR
			IF GET_MODE_STATE_ID() = ENUM_TO_INT(eSCRVSTATE_DELIVER_MISSION_ENTITY) AND NOT IS_LOCAL_PLAYER_IN_MISSION_INTERIOR(SCRV_GET_MISSION_INTERIOR())
				RETURN ENUM_TO_INT(eSCRV_MUSIC_DELIVER)
			ENDIF
		BREAK
		CASE eSCRV_MUSIC_DELIVER

		BREAK
	ENDSWITCH
	
	RETURN -1
ENDFUNC

FUNC STRING SCRV_GET_MUSIC_EVENT_STRING(INT iEvent)
	SWITCH INT_TO_ENUM(SCRV_MUSIC, iEvent)
		CASE eSCRV_MUSIC_START
			RETURN "FIXER_DELIVERING_START"
		
		CASE eSCRV_MUSIC_ENTER_LOCATION
			RETURN "FIXER_SUSPENSE"
		
		CASE eSCRV_MUSIC_GUNFIGHT
			RETURN "FIXER_GUNFIGHT"

		CASE eSCRV_MUSIC_UNLOCK_DOOR
			RETURN "FIXER_MED_INTENSITY"
		
		CASE eSCRV_MUSIC_DELIVER
			RETURN "FIXER_DELIVERING"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

//////////////////////////
///    PLACEMENT DATA 
//////////////////////////

PROC SCRV_SET_PLACEMENT_DATA

	//INTERACT
	data.Interact.iNumLocations = 1
	
	data.Interact.Locations[SCRV_INTERACT_KEYPAD].eType = eINTERACTIONTYPE_SYNC_SCENE
	data.Interact.Locations[SCRV_INTERACT_KEYPAD].vCoords = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(data.Prop.Props[SCRV_GET_INTERACT_PROP_KEYPAD()].vCoords, data.Prop.Props[SCRV_GET_INTERACT_PROP_KEYPAD()].fHeading, <<0.0, -0.5, -1.5>>)
	data.Interact.Locations[SCRV_INTERACT_KEYPAD].Blip.eSprite = RADAR_TRACE_KEYPAD
	data.Interact.Locations[SCRV_INTERACT_KEYPAD].Blip.eHudColour = HUD_COLOUR_GREEN
	data.Interact.Locations[SCRV_INTERACT_KEYPAD].Anim.iNumStages = 1
	data.Interact.Locations[SCRV_INTERACT_KEYPAD].fMaxDistance = 1
	data.Interact.Locations[SCRV_INTERACT_KEYPAD].fFacingAngle = 60
	SET_INTERACT_LOCATION_DATA_BIT(SCRV_INTERACT_KEYPAD, eINTERACTLOCATIONDATABITSET_CHECK_FACING)
	SET_INTERACT_LOCATION_DATA_BIT(SCRV_INTERACT_KEYPAD, eINTERACTLOCATIONDATABITSET_GO_STRAIGHT_TO_COORD)
	SET_INTERACT_LOCATION_DATA_BIT(SCRV_INTERACT_KEYPAD, eINTERACTLOCATIONDATABITSET_FLASH_BLIP_ON_CREATION)

	SWITCH GET_SUBVARIATION()
		CASE FSS_RCV_LOCATION_1
			//Props attached
			data.Prop.AttachedProp[0].iPropIndex = 19
			data.Prop.AttachedProp[0].eParentType = ET_VEHICLE
			data.Prop.AttachedProp[0].iParentIndex = GET_TAG_ID(SCRV_TAGS_MISSION_VEHICLE)
			data.Prop.AttachedProp[0].vOffset = <<0.0000,-2.0000,-0.3000>>
			data.Prop.Props[19].vRotation = <<0.0000,0.0000,0.0000>>
			IF serverBD.iRandomModeInt0 = 1
				data.Vehicle.Vehicles[GET_TAG_ID(SCRV_TAGS_MISSION_VEHICLE)].vCoords = <<3618.4172, 3737.4263, 27.6901>>
				data.Vehicle.Vehicles[GET_TAG_ID(SCRV_TAGS_MISSION_VEHICLE)].fHeading = 7.3297
				data.SearchArea.Areas[0].vCoords = <<3619.2573, 3733.5020, 27.6901>>
			ENDIF
		BREAK
		CASE FSS_RCV_LOCATION_2
			MH_CLIENT_REQUEST_MODEL_HIDE(eMH_MODEL_HIDE_BITSET_prop_compressor_01_966x_n2987y)	
			SWITCH serverBD.iRandomModeInt1
				CASE 0
					PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - SCRV_SET_PLACEMENT_DATA - data.Vehicle.Vehicles[GET_TAG_ID(SCRV_TAGS_MISSION_VEHICLE)].model = COMET7")
					data.Vehicle.Vehicles[GET_TAG_ID(SCRV_TAGS_MISSION_VEHICLE)].model = COMET7
				BREAK
				CASE 1
					PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - SCRV_SET_PLACEMENT_DATA - data.Vehicle.Vehicles[GET_TAG_ID(SCRV_TAGS_MISSION_VEHICLE)].model = IGNUS")
					data.Vehicle.Vehicles[GET_TAG_ID(SCRV_TAGS_MISSION_VEHICLE)].model = IGNUS
				BREAK
				CASE 2
					PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - SCRV_SET_PLACEMENT_DATA - data.Vehicle.Vehicles[GET_TAG_ID(SCRV_TAGS_MISSION_VEHICLE)].model = JESTER4")
					data.Vehicle.Vehicles[GET_TAG_ID(SCRV_TAGS_MISSION_VEHICLE)].model = JESTER4
				BREAK
			ENDSWITCH
			
			data.Vehicle.Vehicles[GET_TAG_ID(SCRV_TAGS_VEHICLE_1)].model = CHINO2
			data.Vehicle.Vehicles[GET_TAG_ID(SCRV_TAGS_VEHICLE_2)].model = BUCCANEER2
			data.Vehicle.Vehicles[GET_TAG_ID(SCRV_TAGS_VEHICLE_3)].model = PEYOTE3
			data.Vehicle.Vehicles[GET_TAG_ID(SCRV_TAGS_VEHICLE_4)].model = BALLER
			data.Vehicle.Vehicles[GET_TAG_ID(SCRV_TAGS_VEHICLE_5)].model = EMPEROR
			data.Vehicle.Vehicles[GET_TAG_ID(SCRV_TAGS_AMBUSH_1)].model = BUCCANEER2
			data.Vehicle.Vehicles[GET_TAG_ID(SCRV_TAGS_AMBUSH_2)].model = CHINO2
			
			IF serverBD.iRandomModeInt0 = 1
				data.Vehicle.Vehicles[GET_TAG_ID(SCRV_TAGS_MISSION_VEHICLE)].vCoords = <<977.8514, -3015.1487, -40.6470>>
				data.Vehicle.Vehicles[GET_TAG_ID(SCRV_TAGS_MISSION_VEHICLE)].fHeading = 299.2231
				data.SearchArea.Areas[0].vCoords = <<977.8514, -3015.1487, -40.6470>>
			ENDIF
		BREAK
		CASE FSS_RCV_LOCATION_3
			serverBD.sMissionData.bBossOwnsHangar = GET_PLAYERS_OWNED_HANGAR(GB_GET_LOCAL_PLAYER_GANG_BOSS()) = LSIA_HANGAR_1
			IF serverBD.sMissionData.bBossOwnsHangar
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - SCRV_SET_PLACEMENT_DATA - Alternative Hangar")
				data.Portal[SCRV_PORTAL_ID_WALK_ENTRY] = data.Portal[SCRV_PORTAL_ID_WALK_ENTRY_ALT]
				data.Portal[SCRV_PORTAL_ID_VEHICLE_ENTRY] = data.Portal[SCRV_PORTAL_ID_VEHICLE_ENTRY_ALT]
				
				data.CustomSpawns.Spawns[0].vCoords = <<-1452.0104, -3289.2773, 12.9449>>
				data.CustomSpawns.Spawns[0].fHeading = 236.2184
				data.CustomSpawns.Spawns[1].vCoords = <<-1453.1665, -3291.4072, 12.9449>>
				data.CustomSpawns.Spawns[1].fHeading = 236.2184
				data.CustomSpawns.Spawns[2].vCoords = <<-1454.4659, -3293.2021, 12.9449>>
				data.CustomSpawns.Spawns[2].fHeading = 236.2184
				data.CustomSpawns.Spawns[3].vCoords = <<-1451.1150, -3287.3899, 12.9449>>
				data.CustomSpawns.Spawns[3].fHeading = 236.2184
				data.CustomSpawns.Spawns[4].vCoords = <<-1454.5502, -3288.4626, 12.9449>>
				data.CustomSpawns.Spawns[4].fHeading = 236.2184
				data.CustomSpawns.Spawns[5].vCoords = <<-1453.7357, -3286.8145, 12.9449>>
				data.CustomSpawns.Spawns[5].fHeading = 236.2184
				data.CustomSpawns.Spawns[6].vCoords = <<-1456.1749, -3289.2715, 12.9449>>
				data.CustomSpawns.Spawns[6].fHeading = 236.2184
				data.CustomSpawns.Spawns[7].vCoords = <<-1457.2249, -3291.1318, 12.9449>>
				data.CustomSpawns.Spawns[7].fHeading = 236.2184

			ENDIF

			SWITCH serverBD.iRandomModeInt1
				CASE 0
					PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - SCRV_SET_PLACEMENT_DATA - data.Vehicle.Vehicles[GET_TAG_ID(SCRV_TAGS_MISSION_VEHICLE)].model = REEVER")
					data.Vehicle.Vehicles[GET_TAG_ID(SCRV_TAGS_MISSION_VEHICLE)].model = INT_TO_ENUM(MODEL_NAMES, HASH("reever"))
				BREAK
				CASE 1
					PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - SCRV_SET_PLACEMENT_DATA - data.Vehicle.Vehicles[GET_TAG_ID(SCRV_TAGS_MISSION_VEHICLE)].model = SHINOBI")
					data.Vehicle.Vehicles[GET_TAG_ID(SCRV_TAGS_MISSION_VEHICLE)].model = INT_TO_ENUM(MODEL_NAMES, HASH("shinobi"))
				BREAK
				CASE 2
					PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - SCRV_SET_PLACEMENT_DATA - data.Vehicle.Vehicles[GET_TAG_ID(SCRV_TAGS_MISSION_VEHICLE)].model = AVARUS")
					data.Vehicle.Vehicles[GET_TAG_ID(SCRV_TAGS_MISSION_VEHICLE)].model = AVARUS
				BREAK
			ENDSWITCH

			SET_VEHICLE_BIT(GET_TAG_ID(SCRV_TAGS_TITAN),eVEHICLEBITSET_FROZEN)
			
			data.Vehicle.Vehicles[GET_TAG_ID(SCRV_TAGS_VEHICLE_3)].model = FACTION2
			data.Vehicle.Vehicles[GET_TAG_ID(SCRV_TAGS_VEHICLE_5)].model = SABREGT2
			
			data.Vehicle.Vehicles[GET_TAG_ID(SCRV_TAGS_VEHICLE_4)].vCoords = data.Vehicle.Vehicles[GET_TAG_ID(SCRV_TAGS_TITAN)].vCoords + <<0.0,-6.0,1.5>>
			data.Vehicle.Vehicles[GET_TAG_ID(SCRV_TAGS_VEHICLE_4)].vCoords.z = -47.8
			
			data.Vehicle.Vehicles[GET_TAG_ID(SCRV_TAGS_MISSION_VEHICLE)].vCoords = data.Vehicle.Vehicles[GET_TAG_ID(SCRV_TAGS_TITAN)].vCoords + <<0.0,-2.5,1.5>>
			data.Vehicle.Vehicles[GET_TAG_ID(SCRV_TAGS_MISSION_VEHICLE)].vCoords.z = -47.8
			
			IF serverBD.iRandomModeInt0 = 1
				data.Vehicle.Vehicles[GET_TAG_ID(SCRV_TAGS_MISSION_VEHICLE)].fHeading -= 180.0
			ENDIF
		BREAK
	ENDSWITCH
	// Potal
	CLEAR_PORTAL_DATA_BIT(SCRV_PORTAL_ID_WALK_ENTRY, ePORTALDATABITSET_INSIDE_INTERIOR)
	data.Portal[SCRV_PORTAL_ID_WALK_ENTRY].iCutscene = SCRV_CUTSCENE_ENTER
	// Cutscenes
	data.Cutscene.iNumScenes = 1
	SWITCH GET_SUBVARIATION()
		CASE FSS_RCV_LOCATION_1
		CASE FSS_RCV_LOCATION_3
			data.Cutscene.Scenes[SCRV_CUTSCENE_ENTER].iNumPlayerClones = 1
			data.Cutscene.Scenes[SCRV_CUTSCENE_ENTER].sAnimDict = "anim@door_trans@hinge_l@"
			data.Cutscene.Scenes[SCRV_CUTSCENE_ENTER].sCameraAnim = "walk_cam_left"
			data.Cutscene.Scenes[SCRV_CUTSCENE_ENTER].PlayerClones[0].sAnim = "walk_player1"
		BREAK
		CASE FSS_RCV_LOCATION_2
			data.Cutscene.Scenes[SCRV_CUTSCENE_ENTER].iNumPlayerClones = 1
			data.Cutscene.Scenes[SCRV_CUTSCENE_ENTER].sAnimDict = "anim@door_trans@hinge_l@"
			data.Cutscene.Scenes[SCRV_CUTSCENE_ENTER].sCameraAnim = "charge_cam_left"
			data.Cutscene.Scenes[SCRV_CUTSCENE_ENTER].PlayerClones[0].sAnim = "charge_player1"
		BREAK
	ENDSWITCH
	SET_CUTSCENE_DATA_BIT(SCRV_CUTSCENE_ENTER, eCUTSCENEDATABITSET_FADE_OUT)
	SET_CUTSCENE_DATA_BIT(SCRV_CUTSCENE_ENTER, eCUTSCENEDATABITSET_LOCAL_PLAYER_REMOTELY_VISIBLE)

	INT iPed
	REPEAT data.Ped.iCount iPed
		data.Ped.Peds[iPed].iMaxReactionDelay = GET_RANDOM_INT_IN_RANGE(0, 2000)
		
		SET_PED_DATA_BIT(iPed, ePEDDATABITSET_FADE_IN)
	ENDREPEAT

ENDPROC

//////////////////////////
///   LOGIC
//////////////////////////

PROC SCRV_MAINTAIN_PLAYER_ENTERED_GARAGE
	IF IS_LOCAL_PLAYER_IN_MISSION_INTERIOR(SCRV_GET_MISSION_INTERIOR())
		IF NOT IS_MISSION_CLIENT_BIT_SET(LOCAL_PARTICIPANT_INDEX, eMISSIONCLIENTBITSET_HAS_ENTERED_INTERIOR)
			SET_MISSION_CLIENT_BIT(eMISSIONCLIENTBITSET_HAS_ENTERED_INTERIOR)
		ENDIF
				
		IF NOT IS_AUDIO_SCENE_ACTIVE("Contract_Recovery_Vehicle_Collision_Mute_Scene")
			START_AUDIO_SCENE("Contract_Recovery_Vehicle_Collision_Mute_Scene")
			PRINTLN("[",scriptName,"] - [SCRV_MAINTAIN_PLAYER_ENTERED_GARAGE] - Started collision mute audio scene")
		ENDIF
	ELSE
		IF IS_AUDIO_SCENE_ACTIVE("Contract_Recovery_Vehicle_Collision_Mute_Scene")
			STOP_AUDIO_SCENE("Contract_Recovery_Vehicle_Collision_Mute_Scene")
			PRINTLN("[",scriptName,"] - [SCRV_MAINTAIN_PLAYER_ENTERED_GARAGE] - Stopped collision mute audio scene")
		ENDIF
	ENDIF

ENDPROC

FUNC VECTOR SCRV_GET_LOCK_DOOR_COORDS(INT iDoor)
	SWITCH GET_SUBVARIATION()
		CASE FSS_RCV_LOCATION_1
			SWITCH iDoor
				CASE 0						RETURN <<3620.8428, 3751.5271, 27.6901>>
				CASE 1						RETURN <<3627.7131, 3746.7163, 27.6901>>
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN <<0,0,0>>
ENDFUNC

FUNC FLOAT SCRV_GET_LOCK_DOOR_OPEN_RATIO(INT iDoor)
	INT iTimeDiff
	SWITCH GET_SUBVARIATION()
		CASE FSS_RCV_LOCATION_1
			SWITCH iDoor
				CASE 0
					IF HAS_NET_TIMER_STARTED(serverBD.sMissionData.tTimeDoorOpened)
						iTimeDiff = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.sMissionData.tTimeDoorOpened)
						RETURN CLAMP(iTimeDiff/TO_FLOAT(SCRV_DOOR_OPEN_TIME), 0.0,1.0)
					ENDIF
					RETURN 0.0
				CASE 1				RETURN 0.0
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN 0.0
ENDFUNC

PROC SCRV_MAINTAIN_DOOR_OPENING_SOUND(VECTOR vDoorPosition, FLOAT fDoorOpenRatio)
	SWITCH GET_SUBVARIATION()
		CASE FSS_RCV_LOCATION_1
			IF fDoorOpenRatio = 1.0
				IF IS_MISSION_LOCAL_BIT_SET(eMISSIONLOCALBITSET_GARAGE_DOOR_LOOP_PLAYING)
					STOP_SOUND(sLocalVariables.iSoundId[2])
					sLocalVariables.iSoundId[2] = -1
					CLEAR_MISSION_LOCAL_BIT(eMISSIONLOCALBITSET_GARAGE_DOOR_LOOP_PLAYING)
				ENDIF
			ELIF fDoorOpenRatio > 0.0
				IF NOT IS_MISSION_LOCAL_BIT_SET(eMISSIONLOCALBITSET_GARAGE_DOOR_LOOP_PLAYING)
					IF PLAY_SOUND_FROM_COORD_WITH_NEW_SOUND_ID(sLocalVariables.iSoundId[2], "Garage_Door_Open_Loop", vDoorPosition, "GTAO_Script_Doors_Sounds")
						SET_MISSION_LOCAL_BIT(eMISSIONLOCALBITSET_GARAGE_DOOR_LOOP_PLAYING)
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

FUNC MODEL_NAMES SCRV_GET_LOCK_DOOR_MODEL(INT iDoor)
	UNUSED_PARAMETER(iDoor)
	SWITCH GET_SUBVARIATION()
		CASE FSS_RCV_LOCATION_1				RETURN V_ILEV_BL_SHUTTER2
	ENDSWITCH
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

FUNC INT SCRV_GET_LOCK_DOOR_HASH(INT iDoor)
	UNUSED_PARAMETER(iDoor)
	SWITCH GET_SUBVARIATION()
		CASE FSS_RCV_LOCATION_1				RETURN HASH("V_ILEV_BL_SHUTTER2")
	ENDSWITCH
	RETURN 0
ENDFUNC

FUNC BOOL SCRV_SHOULD_MAINTAIN_DOORS()
	SWITCH GET_SUBVARIATION()
		CASE FSS_RCV_LOCATION_1				RETURN TRUE
	ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC SCRV_MAINTAIN_DOORS()
	INT iDoorHash, iDoor
	SWITCH GET_SUBVARIATION()
		CASE FSS_RCV_LOCATION_1
			REPEAT 2 iDoor
				IF DOOR_SYSTEM_FIND_EXISTING_DOOR(SCRV_GET_LOCK_DOOR_COORDS(iDoor), SCRV_GET_LOCK_DOOR_MODEL(iDoor), iDoorHash)
					DOOR_SYSTEM_SET_OPEN_RATIO(iDoorHash, SCRV_GET_LOCK_DOOR_OPEN_RATIO(iDoor), FALSE, FALSE)
					DOOR_SYSTEM_SET_DOOR_STATE(iDoorHash, DOORSTATE_LOCKED, FALSE, TRUE)
					SCRV_MAINTAIN_DOOR_OPENING_SOUND(SCRV_GET_LOCK_DOOR_COORDS(iDoor), SCRV_GET_LOCK_DOOR_OPEN_RATIO(iDoor))
				ELSE
					iDoorHash = SCRV_GET_LOCK_DOOR_HASH(iDoor)
					ADD_DOOR_TO_SYSTEM(iDoorHash, SCRV_GET_LOCK_DOOR_MODEL(iDoor), SCRV_GET_LOCK_DOOR_COORDS(iDoor), DEFAULT, FALSE, FALSE)
					PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - SCRV_MAINTAIN_DOORS - Added door")
				ENDIF
			ENDREPEAT
			IF IS_LOCAL_PLAYER_IN_MISSION_INTERIOR(SCRV_GET_MISSION_INTERIOR())
				SET_MISSION_DOOR_STATE(v_ilev_bl_door_r, <<3598.511,3688.977,28.972>>, "SCRV_MAINTAIN_DOORS_v_ilev_bl_door_r", DOORSTATE_LOCKED)
				SET_MISSION_DOOR_STATE(v_ilev_bl_door_l, <<3596.381,3690.469,28.972>>, "SCRV_MAINTAIN_DOORS_v_ilev_bl_door_l", DOORSTATE_LOCKED)
			ENDIF
		BREAK
		CASE FSS_RCV_LOCATION_2
			IF IS_LOCAL_PLAYER_IN_MISSION_INTERIOR(SCRV_GET_MISSION_INTERIOR())
				SET_MISSION_DOOR_STATE(v_ilev_roc_door3, << 967.029,-3006.720,-39.503>>, "SCRV_MAINTAIN_DOORS_v_ilev_roc_door3", DOORSTATE_LOCKED)
				SET_MISSION_DOOR_STATE(v_ilev_vag_door, <<1005.168,-2998.267,-39.496>>, "SCRV_MAINTAIN_DOORS_v_ilev_vag_door", DOORSTATE_LOCKED)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC SCRV_MAINTAIN_INTERACT_KEYPAD
	IF NOT HAS_NET_TIMER_STARTED(serverBD.sMissionData.tTimeDoorOpened)
		IF IS_ARRAYED_BIT_SET(serverBD.iInteractLocationDoneBitset, SCRV_INTERACT_KEYPAD)
			START_NET_TIMER(serverBD.sMissionData.tTimeDoorOpened)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SCRV_SHOULD_ENTER_COMBAT_MODE
	IF NOT IS_MISSION_CLIENT_BIT_SET(LOCAL_PARTICIPANT_INDEX, eMISSIONCLIENTBITSET_COMBAT_POSE_SET)
		RETURN IS_LOCAL_PLAYER_IN_MISSION_INTERIOR(SCRV_GET_MISSION_INTERIOR())
	ENDIF
	RETURN FALSE
ENDFUNC

PROC SCRV_PROCESS_COMBAT_MODE
	IF SCRV_SHOULD_ENTER_COMBAT_MODE()
		SET_MISSION_CLIENT_BIT(eMISSIONCLIENTBITSET_COMBAT_POSE_SET)
		SET_PED_STEALTH_MOVEMENT(LOCAL_PED_INDEX,TRUE)
		IF IS_WEAPON_VALID(GET_PLAYER_CURRENT_HELD_WEAPON())
		AND HAS_PED_GOT_WEAPON(LOCAL_PED_INDEX, GET_PLAYER_CURRENT_HELD_WEAPON())
			SET_CURRENT_PED_WEAPON(LOCAL_PED_INDEX, GET_PLAYER_CURRENT_HELD_WEAPON(), TRUE)
		ELIF HAS_PED_GOT_WEAPON(LOCAL_PED_INDEX, WEAPONTYPE_PISTOL)
			SET_CURRENT_PED_WEAPON(LOCAL_PED_INDEX, WEAPONTYPE_PISTOL, TRUE)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_PLAY_ALARM()

	SWITCH GET_SUBVARIATION()
		CASE FSS_RCV_LOCATION_1			RETURN IS_MISSION_SERVER_BIT_SET(eMISSIONSERVERBITSET_PEDS_ALERTED) AND HAS_NET_TIMER_EXPIRED_READ_ONLY(serverBD.sMissionData.stAlarmTimer, SCRV_ALARM_TIMER)
	ENDSWITCH

	RETURN FALSE

ENDFUNC

FUNC STRING GET_ALARM_SOUND()

	RETURN "FIB_05_BIOTECH_LAB_ALARMS"

ENDFUNC

PROC SCRV_MAINTAIN_ALARMS()
	IF GET_SUBVARIATION() != FSS_RCV_LOCATION_1
		EXIT
	ENDIF
	IF NOT IS_MISSION_SERVER_BIT_SET(eMISSIONSERVERBITSET_PEDS_ALERTED)
		INT iPed
		REPEAT data.Ped.iCount iPed
			IF data.Ped.Peds[iPed].iInterior = 0
				IF HAS_PED_BEEN_TRIGGERED(iPed)
					START_NET_TIMER(serverBD.sMissionData.stAlarmTimer)
					SET_MISSION_CLIENT_BIT(eMISSIONCLIENTBITSET_PEDS_ALERTED)
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF

	IF SHOULD_PLAY_ALARM()
		IF PREPARE_ALARM(GET_ALARM_SOUND())
			IF NOT IS_ALARM_PLAYING(GET_ALARM_SOUND())
				START_ALARM(GET_ALARM_SOUND(),FALSE)
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - SCRV_MAINTAIN_ALARMS - Started alarm ",GET_ALARM_SOUND())
			ENDIF
		ENDIF
	ELSE
		IF IS_ALARM_PLAYING(GET_ALARM_SOUND())
			STOP_ALARM(GET_ALARM_SOUND(),TRUE)
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - SCRV_MAINTAIN_ALARMS - Stopped alarm ",GET_ALARM_SOUND())
		ENDIF
	ENDIF

ENDPROC

PROC SCRV_MAINTAIN_VEHICLE_RADIO
	IF GET_SUBVARIATION() != FSS_RCV_LOCATION_3
		EXIT
	ENDIF
	IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.sVehicle[GET_TAG_ID(SCRV_TAGS_VEHICLE_2)].netId)
		VEHICLE_INDEX vehId = NET_TO_VEH(serverBD.sVehicle[GET_TAG_ID(SCRV_TAGS_VEHICLE_2)].netId)
		IF IS_ENTITY_ALIVE(vehId)
			IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[GET_TAG_ID(SCRV_TAGS_VEHICLE_2)].netId)
				SET_VEHICLE_RADIO_ENABLED(vehId, TRUE)
				IF IS_VEHICLE_RADIO_ON(vehId)
					SET_VEHICLE_RADIO_LOUD(vehId, TRUE)
					SET_VEH_FORCED_RADIO_THIS_FRAME(vehId)
					SET_VEH_RADIO_STATION(vehId, "RADIO_02_POP")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SCRV_MAINTAIN_MARKER
	IF IS_MISSION_SERVER_BIT_SET(eMISSIONSERVERBITSET_BUTTON_FOUND)
		IF GET_MODE_STATE_ID() = ENUM_TO_INT(eSCRVSTATE_UNLOCK_DOOR)
			INT iR = 0, iG = 0, iB = 0, iA = 0
			GET_HUD_COLOUR(HUD_COLOUR_WHITE, iR, iG, iB, iA)
			SWITCH GET_SUBVARIATION()
				CASE FSS_RCV_LOCATION_1
					FM_EVENTDRAW_MARKER_ABOVE_ENTITY(NET_TO_ENT(serverBD.sProp[SCRV_GET_INTERACT_PROP_KEYPAD()].netID), iR, iG, iB, 0.25,DEFAULT,DEFAULT,FALSE,-56.2)
				BREAK
				CASE FSS_RCV_LOCATION_2
					FM_EVENTDRAW_MARKER_ABOVE_ENTITY(NET_TO_ENT(serverBD.sProp[SCRV_GET_INTERACT_PROP_KEYPAD()].netID), iR, iG, iB, 0.25,DEFAULT,DEFAULT,FALSE,-0.197000)
				BREAK
				CASE FSS_RCV_LOCATION_3
					FM_EVENTDRAW_MARKER_ABOVE_ENTITY(NET_TO_ENT(serverBD.sProp[SCRV_GET_INTERACT_PROP_KEYPAD()].netID), iR, iG, iB, 0.25,DEFAULT,DEFAULT,FALSE,90.007004)
				BREAK
			ENDSWITCH
		ENDIF
	ELSE
		IF GET_MODE_STATE_ID() = ENUM_TO_INT(eSCRVSTATE_UNLOCK_DOOR)
			IF VDIST(data.Prop.Props[SCRV_GET_INTERACT_PROP_KEYPAD()].vCoords, LOCAL_PLAYER_COORD) < 5.0
				SET_MISSION_CLIENT_BIT(eMISSIONCLIENTBITSET_BUTTON_FOUND)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SCRV_MAINTAIN_MONKEY_SOUND
	IF GET_SUBVARIATION() = FSS_RCV_LOCATION_1

		IF REQUEST_SCRIPT_AUDIO_BANK("DLC_SECURITY/DLC_Sec_Contract_Monkey")
		
			IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.sVehicle[GET_TAG_ID(SCRV_TAGS_MISSION_VEHICLE)].netId)
				ENTITY_INDEX vehicle = NET_TO_ENT(serverBD.sVehicle[GET_TAG_ID(SCRV_TAGS_MISSION_VEHICLE)].netId)
				IF DOES_ENTITY_EXIST(vehicle) 
				AND IS_ENTITY_ALIVE(vehicle)
					IF HAS_NET_TIMER_STARTED(serverBD.sMissionData.stMonkeyTimer)
						IF NOT HAS_NET_TIMER_EXPIRED(serverBD.sMissionData.stMonkeyTimer, SCRV_MONKEY_AGITATED_TIMER)
							IF PLAY_SOUND_FROM_ENTITY_WITH_NEW_SOUND_ID(sLocalVariables.iSoundId[1], "Agitated_Loop", vehicle, "DLC_SECURITY_MONKEY_SOUNDS")
								PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - SCRV_MAINTAIN_MONKEY_SOUND - Agitated - Played sound FX ",sLocalVariables.iSoundId[1])
								STOP_SOUND(sLocalVariables.iSoundId[0])
								sLocalVariables.iSoundId[0] = -1
							ENDIF
						ELSE
							RESET_NET_TIMER(serverBD.sMissionData.stMonkeyTimer)
						ENDIF
					ELSE
						IF PLAY_SOUND_FROM_ENTITY_WITH_NEW_SOUND_ID(sLocalVariables.iSoundId[0], "Idle_Loop", vehicle, "DLC_SECURITY_MONKEY_SOUNDS")
							PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - SCRV_MAINTAIN_MONKEY_SOUND - Idle - Played sound FX ",sLocalVariables.iSoundId[0])
								STOP_SOUND(sLocalVariables.iSoundId[1])
								sLocalVariables.iSoundId[1] = -1
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SCRV_MAINTAIN_HANGAR_ELEMENTS
	IF GET_SUBVARIATION() != FSS_RCV_LOCATION_3
		EXIT
	ENDIF
	REMOVE_HANGAR_ENTITY_SETS_BY_TYPE(LSIA_HANGAR_1, HANGAR_SET_TYPE_PERSONAL_QUARTERS)
	REMOVE_HANGAR_ENTITY_SETS_BY_TYPE(LSIA_HANGAR_1, HANGAR_SET_TYPE_FLOOR_DECAL)
	
	IF NOT IS_MISSION_LOCAL_BIT_SET(eMISSIONLOCALBITSET_CREATED_INTERIOR_NAVMESH_BLOCKS)
	AND IS_LOCAL_PLAYER_IN_MISSION_INTERIOR(SCRV_GET_MISSION_INTERIOR())
		sLocalVariables.iNavMeshBlockingObjectIDs[0] = ADD_NAVMESH_BLOCKING_OBJECT (<<-1291.6591, -3012.5652, -49.4900>>, <<1.2,1.2,3.0>>, 0.0)
		sLocalVariables.iNavMeshBlockingObjectIDs[1] = ADD_NAVMESH_BLOCKING_OBJECT (<<-1291.5853, -3023.0466, -49.4901>>, <<1.2,1.2,3.0>>, 0.0)
		SET_MISSION_LOCAL_BIT(eMISSIONLOCALBITSET_CREATED_INTERIOR_NAVMESH_BLOCKS)
	ENDIF
ENDPROC

/// PURPOSE:
///    Check if the player is in the hangar interior, if so, force radar zoom to zoomed in, interior only
PROC SCRV_MAINTAIN_HANGAR_RADAR_ZOOM
	IF GET_SUBVARIATION() != FSS_RCV_LOCATION_3
		EXIT
	ENDIF
	
	IF IS_LOCAL_PLAYER_IN_MISSION_INTERIOR(SCRV_GET_MISSION_INTERIOR())
		SET_RADAR_AS_INTERIOR_THIS_FRAME()
		SET_RADAR_ZOOM(100)
		HIDE_MINIMAP_EXTERIOR_MAP_THIS_FRAME()
	ENDIF
ENDPROC

FUNC BOOL SCRV_IS_INTERIOR_VALID_AND_READY(SCRV_INTERIOR eInterior)
	IF IS_VALID_INTERIOR(sInteriorlLocal.sInterior[eInterior].index)
	AND IS_INTERIOR_READY(sInteriorlLocal.sInterior[eInterior].index)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SCRV_FORCE_ROOM_FOR_ENTITY(ENTITY_INDEX entityID, SCRV_INTERIOR eInterior, INT iRoomKey)
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] SCRV_FORCE_ROOM_FOR_ENTITY - entityID = ", NATIVE_TO_INT(entityID), ", eInterior = ", ENUM_TO_INT(eInterior), ", iRoomKey = ", iRoomKey)
	DEBUG_PRINTCALLSTACK()
	
	FORCE_ROOM_FOR_ENTITY(entityID, sInteriorlLocal.sInterior[eInterior].index, iRoomKey)
ENDPROC

PROC SCRV_MAINTAIN_PROP_FORCE_ROOM()
	IF SHOULD_MAINTAIN_PROP_FORCE_ROOM()
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sProp[0].netID)
			ENTITY_INDEX propID = NET_TO_ENT(serverBD.sProp[0].netID)
			IF DOES_ENTITY_EXIST(propID)
			AND IS_LOCAL_PLAYER_IN_MISSION_INTERIOR(SCRV_GET_MISSION_INTERIOR())
				IF SCRV_IS_INTERIOR_VALID_AND_READY(eSCRVINTERIOR_SECURITY_ROOM)
				AND NOT IS_ENTITY_DEAD(propID)
					SCRV_FORCE_ROOM_FOR_ENTITY(propID, eSCRVINTERIOR_SECURITY_ROOM, SCRV_GET_MISSION_INTERIOR_HASH())
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SCRV_MAINTAIN_CLIENT
	SCRV_MAINTAIN_PLAYER_ENTERED_GARAGE()
	SCRV_MAINTAIN_DOORS()
	SCRV_PROCESS_COMBAT_MODE()
	SCRV_MAINTAIN_ALARMS()
	SCRV_MAINTAIN_VEHICLE_RADIO()
	SCRV_MAINTAIN_MARKER()
	SCRV_MAINTAIN_MONKEY_SOUND()
	SCRV_MAINTAIN_HANGAR_ELEMENTS()
	SCRV_MAINTAIN_HANGAR_RADAR_ZOOM()
	SCRV_MAINTAIN_PROP_FORCE_ROOM()
ENDPROC

PROC SCRV_MAINTAIN_AMBUSH
	IF GET_MODE_STATE_ID() >= ENUM_TO_INT(eSCRVSTATE_COLLECT_MISSION_ENTITY) AND NOT IS_MISSION_ENTITY_IN_MISSION_INTERIOR(SCRV_MISSION_ENTITY_ID)
		SET_MISSION_SERVER_BIT(eMISSIONSERVERBITSET_AMBUSH_STARTED)
	ENDIF
ENDPROC

PROC SCRV_MAINTAIN_SERVER
	SCRV_MAINTAIN_INTERACT_KEYPAD()
	SCRV_MAINTAIN_AMBUSH()
ENDPROC

FUNC BOOL SCRV_INIT_CLIENT
	sLocalVariables.iSoundId[0] = -1
	sLocalVariables.iSoundId[1] = -1
	sLocalVariables.iSoundId[2] = -1
	RETURN TRUE
ENDFUNC

PROC SCRV_CLEANUP_PROCESSING()
	
	// Temp fix for url:bugstar:7417507 - Fixer - Security Contract - Vehicle Recovery - LSIA - If the player drives the objective bike into the Agency Garage as the mission timer reaches zero, they get a completion cutscene with Franklin, despite the mission being failed.
	IF NOT IS_GENERIC_BIT_SET(eGENERICBITSET_I_WON) 
		CLEAR_BIT(g_SimpleInteriorData.iEleventhBS, BS11_SIMPLE_INTERIOR_FIXER_HQ_PLAY_HANDOVER_SCENE)
	ENDIF

	MH_CLIENT_CANCEL_MODEL_HIDE(eMH_MODEL_HIDE_BITSET_prop_compressor_01_966x_n2987y)
	IF GET_SUBVARIATION() = FSS_RCV_LOCATION_1
		IF IS_ALARM_PLAYING(GET_ALARM_SOUND())
			STOP_ALARM(GET_ALARM_SOUND(),TRUE)
		ENDIF
	ENDIF
	IF IS_MISSION_LOCAL_BIT_SET(eMISSIONLOCALBITSET_GARAGE_DOOR_LOOP_PLAYING)
		STOP_SOUND(sLocalVariables.iSoundId[2])
		CLEAR_MISSION_LOCAL_BIT(eMISSIONLOCALBITSET_GARAGE_DOOR_LOOP_PLAYING)
	ENDIF
	IF 	sLocalVariables.iSoundId[0] != -1
		RELEASE_SOUND_ID(sLocalVariables.iSoundId[0])
		sLocalVariables.iSoundId[0] = -1
	ENDIF
	IF 	sLocalVariables.iSoundId[1] != -1
		RELEASE_SOUND_ID(sLocalVariables.iSoundId[1])
		sLocalVariables.iSoundId[1] = -1
	ENDIF
	IF 	sLocalVariables.iSoundId[2] != -1
		RELEASE_SOUND_ID(sLocalVariables.iSoundId[2])
		sLocalVariables.iSoundId[2] = -1
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("Contract_Recovery_Vehicle_Collision_Mute_Scene")
		STOP_AUDIO_SCENE("Contract_Recovery_Vehicle_Collision_Mute_Scene")
	ENDIF
	
	IF IS_MISSION_LOCAL_BIT_SET(eMISSIONLOCALBITSET_CREATED_INTERIOR_NAVMESH_BLOCKS)
		REMOVE_NAVMESH_BLOCKING_OBJECT(sLocalVariables.iNavMeshBlockingObjectIDs[0])
		REMOVE_NAVMESH_BLOCKING_OBJECT(sLocalVariables.iNavMeshBlockingObjectIDs[1])
		CLEAR_MISSION_LOCAL_BIT(eMISSIONLOCALBITSET_CREATED_INTERIOR_NAVMESH_BLOCKS)
	ENDIF
ENDPROC

FUNC BOOL SCRV_OVERRIDE_J_SKIP
	SWITCH INT_TO_ENUM(SCRV_CLIENT_MODE_STATE, GET_CLIENT_MODE_STATE_ID())
		CASE eSCRVSTATECLIENT_LOCATE_VEHICLE
			IF NOT IS_LOCAL_PLAYER_IN_MISSION_INTERIOR(SCRV_GET_MISSION_INTERIOR())
				NET_WARP_TO_COORD(data.Portal[SCRV_PORTAL_ID_WALK_ENTRY].vCoord, 0,FALSE)
				RETURN TRUE
			ENDIF
			NET_WARP_TO_COORD(data.Vehicle.Vehicles[GET_TAG_ID(SCRV_TAGS_MISSION_VEHICLE)].vCoords, 0,FALSE)
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL SCRV_SHOULD_RUN_INTERIOR_EXTERIOR_BLIP_CHECKS(INT iPed)
	UNUSED_PARAMETER(iPed)
	IF GET_SUBVARIATION() = FSS_RCV_LOCATION_1
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

PROC SCRV_SETUP_LOGIC()
	logic.Data.Setup = 								&SCRV_SET_PLACEMENT_DATA
	
	logic.StateMachine.SetupStates = 				&SCRV_SETUP_STATES
	logic.StateMachine.SetupClientStates = 			&SCRV_SETUP_CLIENT_STATES

	logic.Ped.Activate =							&SCRV_PED_ACTIVATE
	logic.Ped.Behaviour.Setup = 					&SCRV_SETUP_PED_TASKS
	logic.Ped.Behaviour.Get = 						&SCRV_GET_PED_BEHAVIOUR
	logic.Ped.Attributes =							&SCRV_PED_ATTRIBUTES
	
	logic.Prop.Client =								&SCRV_PROP_CLIENT

	logic.Vehicle.Mods = 							&SCRV_VEHICLE_MODS
	logic.Vehicle.Attributes = 						&SCRV_VEHICLE_ATTRIBUTES
	logic.Vehicle.Client = 							&SCRV_VEHICLES_MAINTAIN_CLIENT
	logic.Vehicle.Doors =							&SCRV_VEHICLES_MAINTAIN_DOORS
	logic.Vehicle.OnDamaged =						&SCRV_ENTITY_ON_DAMAGED
	logic.Vehicle.Client =							&SCRV_VEHICLE_MAINTAIN_CLIENT
	
	logic.MissionEntity.OnCollect = 				&SCRV_MISSION_ENTITY_ON_COLLECT
	logic.MissionEntity.Blip.Enable =				&SCRV_MISSION_ENTITY_BLIP_ENABLE
	logic.MissionEntity.Blip.Sprite =				&SCRV_MISSION_ENTITY_BLIP_SPRITE
	logic.MissionEntity.Blip.Name =					&SCRV_MISSION_ENTITY_BLIP_NAME
	logic.MissionEntity.Blip.DrawMarker =			&SCRV_MISSION_ENTITY_BLIP_DRAW_MARKER
	logic.MissionEntity.Rival.Visible =				&SCRV_MISSION_ENTITY_VISIBLE_RIVAL
	
	logic.Portal.ShouldTrigger = 					&SCRV_SHOULD_PORTAL_TRIGGER
	logic.Portal.DrawCorona = 						&SCRV_SHOULD_PORTAL_DRAW_CORONA
	logic.Portal.Blip.Enable = 						&SCRV_SHOULD_ENABLE_PORTAL_BLIP
	logic.Portal.Blip.DrawGPS =						&SCRV_SHOULD_DRAW_GPS_PORTAL_BLIP
	logic.Portal.KeepVehicle = 						&SCRV_PORTAL_KEEP_VEHICLE

	logic.Interact.EnableLocation =					&SCRV_INTERACT_ENABLE_LOCATION
	logic.Interact.Blip.Enable = 					&SCRV_INTERACT_BLIP_ENABLE
	logic.Interact.HelpText.ContextIntention = 		&SCRV_GET_INTERACT_CONTEXT_INTENTION_HELP_TEXT
	logic.Interact.OnComplete = 					&SCRV_INTERACT_ON_COMPLETE
	logic.Interact.Anim.AnimDict =					&SCRV_GET_INTERACT_ANIM_DICT
	logic.Interact.Anim.Anim = 						&SCRV_GET_INTERACT_ANIM
	logic.Interact.Anim.Heading = 					&SCRV_GET_INTERACT_ANIM_HEADING
	logic.Interact.FacingCoords = 					&SCRV_GET_INTERACT_FACING_COORDS
	logic.Interact.ForwardDirection = 				&SCRV_GET_INTERACT_FORWARD_DIRECTION
	logic.Interact.Anim.Coords = 					&SCRV_GET_INTERACT_ANIM_COORDS
	logic.Interact.MaintainPlayingInteract =		&SCRV_MAINTAIN_INTERACT

	logic.TextMessages.ShouldSend =					&SCRV_SEND_TEXT
	logic.TextMessages.Label = 						&SCRV_GET_TEXT_LABEL
	logic.TextMessages.Character = 					&SCRV_GET_TEXT_CHAR
	logic.TextMessages.CriticalStatus = 			&SCRV_GET_TEXT_CRIT_STATUS
	logic.TextMessages.LockedStatus = 				&SCRV_GET_TEXT_LOCK_STATUS

	logic.Dialogue.Root = 							&SCRV_GET_DIALOGUE_ROOT
	logic.Dialogue.SubtitleGroup = 					&SCRV_GET_DIALOGUE_SUBTITLE_GROUP
	logic.Dialogue.Character = 						&SCRV_GET_DIALOGUE_CHARACTER
	logic.Dialogue.ShouldTrigger = 					&SCRV_SHOULD_TRIGGER_DIALOGUE
	logic.Dialogue.UseHeadsetAudio =				&SCRV_USE_HEADSET_AUDIO
	logic.Dialogue.CellphoneCharacter =				&SCRV_GET_CELLPHONE_CHARACTER
	
	//Vehicle Clean up 
	logic.Interior.ShouldKeepVehicleOnWarpOut = 	&SCRV_SHOULD_KEEP_VEHICLE_WHEN_WARPED_OUT_MISSION_INTERIOR
	logic.Interior.DisplacedCoords = 				&SCRV_INTERIOR_DISPLACED_COORD
	logic.Interior.ShouldRunInteriorExteriorBlipChecks = &SCRV_SHOULD_RUN_INTERIOR_EXTERIOR_BLIP_CHECKS
	
	// Cutscene (entry / exit animations)
	logic.Cutscene.Enable = 						&SCRV_SHOULD_ENABLE_CUTSCENE
	logic.Cutscene.ShouldStart = 					&SCRV_SHOULD_CUTSCENE_START
	logic.Cutscene.PlayerID = 						&SCRV_GET_CUTSCENE_PLAYER_ID
	logic.Cutscene.Coords = 						&SCRV_GET_CUTSCENE_COORDS
	logic.Cutscene.Heading = 						&SCRV_GET_CUTSCENE_HEADING
	logic.Cutscene.ShouldFadeOut = 					&SCRV_SHOULD_CUTSCENE_FADE_OUT
	logic.Cutscene.ShouldFinish = 					&SCRV_SHOULD_CUTSCENE_FINISH
	logic.Cutscene.LoadSceneCoords = 				&SCRV_CUTSCENE_LOAD_SCENE_COORDS
	logic.Cutscene.MaintainRun = 					&SCRV_CUTSCENE_MAINTAIN_RUN
	
	logic.SearchArea.Blip.Enable =					&SCRV_SEARCH_AREA_BLIP_ENABLE
	logic.SearchArea.HasTriggered = 				&SCRV_SEARCH_AREA_HAS_TRIGGERED
	logic.SearchArea.TriggerCoords =				&SCRV_SEARCH_AREA_TRIGGER_COORDS

	logic.Ambush.ShouldActivate = 					&SCRV_SHOULD_ACTIVATE_AMBUSH
	logic.Ambush.IsTargetValid = 					&SCRV_AMBUSH_IS_TARGET_VALID
	
	logic.Hud.Tickers.CustomStringLocal = 			&SCRV_TICKER_CUSTOM_LOCAL
	logic.Hud.Tickers.CustomStringRemote = 			&SCRV_TICKER_CUSTOM_REMOTE

	logic.HelpText.Trigger = 						&SCRV_SHOULD_TRIGGER_HELP_TEXT
	logic.HelpText.Text = 							&SCRV_GET_HELP_TEXT_LABEL
	
	logic.Player.Spawning.Custom.Enable =			&SCRV_USE_CUSTOM_SPAWNS

	logic.Audio.MusicTrigger.Event = 				&SCRV_GET_NEXT_MUSIC_EVENT
	logic.Audio.MusicTrigger.EventString = 			&SCRV_GET_MUSIC_EVENT_STRING
	logic.Audio.MusicTrigger.StopString = 			&GET_FIXER_SECURITY_CONTRACT_MUSIC_STOP_EVENT_STRING
	logic.Audio.MusicTrigger.FailString = 			&GET_FIXER_SECURITY_CONTRACT_MUSIC_FAIL_EVENT_STRING

	logic.Maintain.Client =							&SCRV_MAINTAIN_CLIENT
	logic.Maintain.Server =							&SCRV_MAINTAIN_SERVER
	logic.Init.Client =								&SCRV_INIT_CLIENT
	
	logic.Cleanup.ScriptEnd =						&SCRV_CLEANUP_PROCESSING

	#IF IS_DEBUG_BUILD	
	logic.Debug.OverrideJSkip = 					&SCRV_OVERRIDE_J_SKIP
	#ENDIF
ENDPROC
