USING "fixer_hq_interior_seating_activities_public.sch"
USING "fixer_hq_interior_seating_helper.sch"
USING "net_simple_interior.sch"
USING "net_peds_fixer_hq.sch"

#IF FEATURE_FIXER

SEATS_LOCAL_DATA sSeatData

FUNC BOOL SHOULD_ENABLE_FRANKLIN_OFFICE_SOFA()
	BOOL bEnableFranklinSeat = FALSE		
	
	IF NOT IS_BIT_SET(g_SimpleInteriorData.iEleventhBS, BS11_SIMPLE_INTERIOR_FIXER_HQ_FRANKLIN_PRESENT) // Franklin is not present, enable seat
		bEnableFranklinSeat = TRUE
	ELSE
		IF g_iFixerFranklinActivity != ENUM_TO_INT(PED_ACT_FIXER_FRANKLIN_1) // Franklin is here but not on couch, enable seat
	   		bEnableFranklinSeat = TRUE
		ENDIF
	ENDIF
	
	RETURN bEnableFranklinSeat				
ENDFUNC

PROC STORE_SEAT_DATA()
	IF IS_PLAYER_FEMALE()
		SETUP_FIXER_HQ_SEAT(FIXER_HQ_INTERIOR_SEAT_0, sSeatData, 0, SEAT_TYPE_CLUBHOUSE_ANIM)
	ELSE
		SETUP_FIXER_HQ_SEAT(FIXER_HQ_INTERIOR_SEAT_0, sSeatData, 0, SEAT_TYPE_OFFICE_ANIM)
	ENDIF
	SETUP_FIXER_HQ_SEAT(FIXER_HQ_INTERIOR_SEAT_1, sSeatData, 1, SEAT_TYPE_CLUBHOUSE_ANIM)
	
	SETUP_FIXER_HQ_SEAT(FIXER_HQ_INTERIOR_SEAT_2, sSeatData, 2, SEAT_TYPE_OFFICE_ANIM)
	SETUP_FIXER_HQ_SEAT(FIXER_HQ_INTERIOR_SEAT_3, sSeatData, 3, SEAT_TYPE_CLUBHOUSE_ANIM)
	SETUP_FIXER_HQ_SEAT(FIXER_HQ_INTERIOR_SEAT_4, sSeatData, 4, SEAT_TYPE_OFFICE_ANIM)
	SETUP_FIXER_HQ_SEAT(FIXER_HQ_INTERIOR_SEAT_5, sSeatData, 5, SEAT_TYPE_OFFICE_ANIM)
	
	SETUP_FIXER_HQ_SEAT(FIXER_HQ_INTERIOR_SEAT_6, sSeatData, 6, SEAT_TYPE_OFFICE_ANIM)
			
	IF SHOULD_ENABLE_FRANKLIN_OFFICE_SOFA() // Block middle and right seats on sofa when Franklin is sitting here so player doesn't clip into him
		SETUP_FIXER_HQ_SEAT(FIXER_HQ_INTERIOR_SEAT_7, sSeatData, 7, SEAT_TYPE_OFFICE_ANIM)
		SETUP_FIXER_HQ_SEAT(FIXER_HQ_INTERIOR_SEAT_8, sSeatData, 8, SEAT_TYPE_CLUBHOUSE_ANIM)
	ENDIF
	SETUP_FIXER_HQ_SEAT(FIXER_HQ_INTERIOR_SEAT_9, sSeatData, 9, SEAT_TYPE_OFFICE_ANIM)
	
	SETUP_FIXER_HQ_SEAT(FIXER_HQ_INTERIOR_SEAT_10, sSeatData, 10, SEAT_TYPE_CLUBHOUSE_ANIM)
	SETUP_FIXER_HQ_SEAT(FIXER_HQ_INTERIOR_SEAT_11, sSeatData, 11, SEAT_TYPE_CLUBHOUSE_ANIM)
	SETUP_FIXER_HQ_SEAT(FIXER_HQ_INTERIOR_SEAT_12, sSeatData, 12, SEAT_TYPE_OFFICE_ANIM)
	SETUP_FIXER_HQ_SEAT(FIXER_HQ_INTERIOR_SEAT_13, sSeatData, 13, SEAT_TYPE_OFFICE_ANIM)
	
	SETUP_FIXER_HQ_SEAT(FIXER_HQ_INTERIOR_SEAT_14, sSeatData, 14, SEAT_TYPE_CLUBHOUSE_ANIM)
	
	// Only set this seat up if owner hasn't setup the propety, otherwise a smoking activity is placed here
	IF (GET_PLAYER_NUM_FIXER_SECURITY_CONTRACTS_COMPLETED(GET_OWNER_OF_SIMPLE_INTERIOR_LOCAL_PLAYER_IS_IN()) < 1)
	AND NOT HAS_PLAYER_COMPLETED_FIXER_INTRO_MISSION(GET_OWNER_OF_SIMPLE_INTERIOR_LOCAL_PLAYER_IS_IN())
		SETUP_FIXER_HQ_SEAT(FIXER_HQ_INTERIOR_SEAT_15, sSeatData, 15, SEAT_TYPE_CLUBHOUSE_ANIM)
	ENDIF
		
	SETUP_FIXER_HQ_SEAT(FIXER_HQ_INTERIOR_SEAT_16, sSeatData, 16, SEAT_TYPE_OFFICE_ANIM)
	SETUP_FIXER_HQ_SEAT(FIXER_HQ_INTERIOR_SEAT_17, sSeatData, 17, SEAT_TYPE_OFFICE_ANIM)
	
	SETUP_FIXER_HQ_SEAT(FIXER_HQ_INTERIOR_SEAT_18, sSeatData, 18, SEAT_TYPE_RAILING_ANIM)
	SETUP_FIXER_HQ_SEAT(FIXER_HQ_INTERIOR_SEAT_19, sSeatData, 19, SEAT_TYPE_RAILING_ANIM)
	SETUP_FIXER_HQ_SEAT(FIXER_HQ_INTERIOR_SEAT_20, sSeatData, 20, SEAT_TYPE_RAILING_ANIM)
	SETUP_FIXER_HQ_SEAT(FIXER_HQ_INTERIOR_SEAT_21, sSeatData, 21, SEAT_TYPE_RAILING_ANIM)
	SETUP_FIXER_HQ_SEAT(FIXER_HQ_INTERIOR_SEAT_22, sSeatData, 22, SEAT_TYPE_RAILING_ANIM)
	SETUP_FIXER_HQ_SEAT(FIXER_HQ_INTERIOR_SEAT_23, sSeatData, 23, SEAT_TYPE_RAILING_ANIM)
	
	SETUP_FIXER_HQ_SEAT(FIXER_HQ_INTERIOR_SEAT_24, sSeatData, 24, SEAT_TYPE_OFFICE_ANIM)
ENDPROC

FUNC BOOL SCRIPT_INIT()

	IF NETWORK_IS_SCRIPT_ACTIVE("fixer_hq_seating", NATIVE_TO_INT(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner), TRUE)
		PRINTLN("[FIXER_HQ_SEATS] fixer_hq_seating script with this ID already running")	
		RETURN FALSE
	ENDIF
	IF GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner = INVALID_PLAYER_INDEX()
		PRINTLN("[FIXER_HQ_SEATS] SCRIPT_INIT - Fixer HQ - propertyOwner = INVALID_PLAYER_INDEX()")
		RETURN FALSE
	ENDIF	
	
	NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE, NATIVE_TO_INT(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner))
	
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	
	LOG_MAX_NUM_SEATS()
	
	STORE_SEAT_DATA()
	
	PRINTLN("[FIXER_HQ_SEATS] SCRIPT_INIT - Fixer HQ - TRUE")
	
	RETURN TRUE
ENDFUNC

PROC SCRIPT_CLEANUP()
	PRINTLN("[FIXER_HQ_SEATS] - CLEANING UP SEATS")
	
	g_bInNightclubSeatLocate = FALSE
	
	CLEANUP_SEATS_ACTIVITY(sSeatData)	
	
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

FUNC BOOL SHOULD_TERMINATE_SCRIPT()
	IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		PRINTLN("[FIXER_HQ_SEATS] SHOULD_TERMINATE_SCRIPT - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE")
		RETURN TRUE
	ENDIF	

	IF IS_SCREEN_FADED_OUT()
	AND NOT IS_LOCAL_PLAYER_VIEWING_CCTV()
		PRINTLN("[FIXER_HQ_SEATS] SHOULD_TERMINATE_SCRIPT - IS_SCREEN_FADED_OUT")
		
		RETURN TRUE
	ENDIF
	
	IF NOT IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())
		PRINTLN("[FIXER_HQ_SEATS] SHOULD_TERMINATE_SCRIPT - player outside bounds")
		
		RETURN TRUE
	ENDIF
	
	IF (IS_PLAYER_WALKING_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID()) AND IS_SCREEN_FADED_OUT())
		PRINTLN("[FIXER_HQ_SEATS] SHOULD_TERMINATE_SCRIPT - IS_PLAYER_WALKING_OUT_OF_SIMPLE_INTERIOR is TRUE")
		
		RETURN TRUE
	ENDIF
	
	IF NOT IS_SKYSWOOP_AT_GROUND()
		PRINTLN("[FIXER_HQ_SEATS] SHOULD_TERMINATE_SCRIPT - IS_SKYSWOOP_AT_GROUND is FALSE")
		
		RETURN TRUE
	ENDIF
	
	IF g_sShopSettings.playerRoom != 767622941
		PRINTLN("[FIXER_HQ_SEATS] SHOULD_TERMINATE_SCRIPT - Not in room")
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

#ENDIF

SCRIPT
	#IF FEATURE_FIXER
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF NOT SCRIPT_INIT()
			SCRIPT_CLEANUP()
		ENDIF
	ELSE
		SCRIPT_CLEANUP()
	ENDIF
	
		
	WHILE TRUE
		MP_LOOP_WAIT_ZERO()
		IF SHOULD_TERMINATE_SCRIPT()
			SCRIPT_CLEANUP()
		ENDIF
		
		MAINTAIN_SEATS_ACTIVITY(sSeatData)		
		
	ENDWHILE

	
	
	#ENDIF
ENDSCRIPT