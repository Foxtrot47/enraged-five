USING "globals.sch"
USING "net_include.sch"
USING "net_simple_interior.sch"

#IF FEATURE_MUSIC_STUDIO

CONST_INT MAX_SMOKING_ACTIVITIES		2
CONST_INT MAX_HIDDEN_SCENE_OBJECTS 		2

STRUCT ServerBroadcastData
	NETWORK_INDEX objLighter[MAX_SMOKING_ACTIVITIES]
	NETWORK_INDEX objSmoking[MAX_SMOKING_ACTIVITIES]
	NETWORK_INDEX objSmokingSpecial[MAX_SMOKING_ACTIVITIES] // Only for Special Strain in Music Studio Smoking Room
	NETWORK_INDEX objAshtray[MAX_SMOKING_ACTIVITIES]
ENDSTRUCT
ServerBroadcastData serverBD

CONST_INT SMOKING_ACTIVITY_BS_SPARKS				0
CONST_INT SMOKING_ACTIVITY_BS_EXHALE_MOUTH			1
CONST_INT SMOKING_ACTIVITY_BS_HELMET				2
CONST_INT SMOKING_ACTIVITY_BS_HOOD_UP				3
CONST_INT SMOKING_ACTIVITY_BS_OXY_TUBE				4
CONST_INT SMOKING_ACTIVITY_BS_CLEARED_FACE_PROPS	5
CONST_INT SMOKING_ACTIVITY_BS_PED_IS_WALKING		6
CONST_INT SMOKING_ACTIVITY_BS_TICKER_STARTING_SS	7
CONST_INT SMOKING_ACTIVITY_BS_INHALE_MOUTH			8

ENUM SMOKING_ACTIVITY_STATE
	SMOKING_ACTIVITY_STATE_INIT,
	SMOKING_ACTIVITY_STATE_PROMPT,
	SMOKING_ACTIVITY_STATE_WALK,
	SMOKING_ACTIVITY_STATE_WALKING_TO_POSITION,
	SMOKING_ACTIVITY_STATE_WAIT_TO_SIT,
	
	SMOKING_ACTIVITY_STATE_IDLE,
	SMOKING_ACTIVITY_STATE_TRANSFER_BASE_POSE,
	SMOKING_ACTIVITY_STATE_WEED_IDLE,
	
	SMOKING_ACTIVITY_STATE_SMOKING,
	SMOKING_ACTIVITY_STATE_EXIT_SMOKING,
	
	SMOKING_ACTIVITY_STATE_LAMAR_STRAIN,
	SMOKING_ACTIVITY_STATE_LAMAR_STRAIN_BASE,
	
	SMOKING_ACTIVITY_STATE_WEED_EXITING,
	SMOKING_ACTIVITY_STATE_EXITING
ENDENUM

ENUM SMOKING_ACTIVITY_CLIP
	SMOKING_ACTIVITY_INVALID,

	// Enter/sitdown
	SMOKING_ACTIVITY_ENTER,
	
	// Exit/standup
	SMOKING_ACTIVITY_EXIT,
						
	// Initial base pose
	SMOKING_ACTIVITY_BASE,
	SMOKING_ACTIVITY_BASE_ASHTRAY,
	SMOKING_ACTIVITY_BASE_JOINT,
	SMOKING_ACTIVITY_BASE_JOINT_SPECIAL,
	SMOKING_ACTIVITY_BASE_LIGHTER,
		
	// Initial idle pose variations
	SMOKING_ACTIVITY_IDLE_01,
	SMOKING_ACTIVITY_IDLE_01_ASHTRAY,
	SMOKING_ACTIVITY_IDLE_01_JOINT,
	SMOKING_ACTIVITY_IDLE_01_JOINT_SPECIAL,
	SMOKING_ACTIVITY_IDLE_01_LIGHTER,
	
	SMOKING_ACTIVITY_IDLE_02,
	SMOKING_ACTIVITY_IDLE_02_ASHTRAY,
	SMOKING_ACTIVITY_IDLE_02_JOINT,
	SMOKING_ACTIVITY_IDLE_02_JOINT_SPECIAL,
	SMOKING_ACTIVITY_IDLE_02_LIGHTER,
	
	SMOKING_ACTIVITY_IDLE_03,
	SMOKING_ACTIVITY_IDLE_03_ASHTRAY,
	SMOKING_ACTIVITY_IDLE_03_JOINT,
	SMOKING_ACTIVITY_IDLE_03_JOINT_SPECIAL,
	SMOKING_ACTIVITY_IDLE_03_LIGHTER,
			
	// Enter for smoking normal weed
	SMOKING_ACTIVITY_NORMAL_ENTER,
	SMOKING_ACTIVITY_NORMAL_ENTER_ASHTRAY,
	SMOKING_ACTIVITY_NORMAL_ENTER_LIGHTER,
	SMOKING_ACTIVITY_NORMAL_ENTER_JOINT,
	SMOKING_ACTIVITY_NORMAL_ENTER_JOINT_SPECIAL,
	
	// Enter for smoking lamar strain
	SMOKING_ACTIVITY_LAMAR_STRAIN_ENTER,
	SMOKING_ACTIVITY_LAMAR_STRAIN_ENTER_ASHTRAY,
	SMOKING_ACTIVITY_LAMAR_STRAIN_ENTER_JOINT,
	SMOKING_ACTIVITY_LAMAR_STRAIN_ENTER_JOINT_SPECIAL,
	SMOKING_ACTIVITY_LAMAR_STRAIN_ENTER_LIGHTER,
						
	// Weed Base pose after using normal weed initially
	SMOKING_ACTIVITY_WEED_BASE,
	SMOKING_ACTIVITY_WEED_BASE_ASHTRAY,
	SMOKING_ACTIVITY_WEED_BASE_JOINT,
	SMOKING_ACTIVITY_WEED_BASE_JOINT_SPECIAL,
	SMOKING_ACTIVITY_WEED_BASE_LIGHTER,
		
	// Idle pose variations after using normal weed
	SMOKING_ACTIVITY_WEED_IDLE_01,
	SMOKING_ACTIVITY_WEED_IDLE_01_ASHTRAY,
	SMOKING_ACTIVITY_WEED_IDLE_01_JOINT,
	SMOKING_ACTIVITY_WEED_IDLE_01_JOINT_SPECIAL,
	SMOKING_ACTIVITY_WEED_IDLE_01_LIGHTER,
	
	SMOKING_ACTIVITY_WEED_IDLE_02,
	SMOKING_ACTIVITY_WEED_IDLE_02_ASHTRAY,
	SMOKING_ACTIVITY_WEED_IDLE_02_JOINT,
	SMOKING_ACTIVITY_WEED_IDLE_02_JOINT_SPECIAL,
	SMOKING_ACTIVITY_WEED_IDLE_02_LIGHTER,
	
	SMOKING_ACTIVITY_WEED_IDLE_03,
	SMOKING_ACTIVITY_WEED_IDLE_03_ASHTRAY,
	SMOKING_ACTIVITY_WEED_IDLE_03_JOINT,
	SMOKING_ACTIVITY_WEED_IDLE_03_JOINT_SPECIAL,
	SMOKING_ACTIVITY_WEED_IDLE_03_LIGHTER,
							
	// Exit from Weed Base
	SMOKING_ACTIVITY_WEED_EXIT,
	SMOKING_ACTIVITY_WEED_EXIT_ASHTRAY,
	SMOKING_ACTIVITY_WEED_EXIT_JOINT,
	SMOKING_ACTIVITY_WEED_EXIT_JOINT_SPECIAL,
	SMOKING_ACTIVITY_WEED_EXIT_LIGHTER,
			
	// Smoke normal weed once player is in Weed Base
	SMOKING_ACTIVITY_WEED_SMOKE_01,
	SMOKING_ACTIVITY_WEED_SMOKE_ASHTRAY_01,
	SMOKING_ACTIVITY_WEED_SMOKE_JOINT_01,
	SMOKING_ACTIVITY_WEED_SMOKE_JOINT_SPECIAL_01,
	SMOKING_ACTIVITY_WEED_SMOKE_LIGHTER_01,
	
	SMOKING_ACTIVITY_WEED_SMOKE_02,
	SMOKING_ACTIVITY_WEED_SMOKE_ASHTRAY_02,
	SMOKING_ACTIVITY_WEED_SMOKE_JOINT_02,
	SMOKING_ACTIVITY_WEED_SMOKE_JOINT_SPECIAL_02,
	SMOKING_ACTIVITY_WEED_SMOKE_LIGHTER_02,
							
	// Transfer from Weed Base to Lamar Strain
	SMOKING_ACTIVITY_WEED_TO_LAMAR,
	SMOKING_ACTIVITY_WEED_TO_LAMAR_ASHTRAY,
	SMOKING_ACTIVITY_WEED_TO_LAMAR_JOINT,
	SMOKING_ACTIVITY_WEED_TO_LAMAR_JOINT_SPECIAL,
	SMOKING_ACTIVITY_WEED_TO_LAMAR_LIGHTER,
				
	// Transfer from Weed Base to Base
	SMOKING_ACTIVITY_WEED_TO_BASE,
	SMOKING_ACTIVITY_WEED_TO_BASE_ASHTRAY,
	SMOKING_ACTIVITY_WEED_TO_BASE_JOINT,
	SMOKING_ACTIVITY_WEED_TO_BASE_JOINT_SPECIAL,
	SMOKING_ACTIVITY_WEED_TO_BASE_LIGHTER,
		
	// Transfer from Pose 1 Weed Base to Pose 2 Base
	SMOKING_ACTIVITY_WEED_TO_BASE_P2,
	SMOKING_ACTIVITY_WEED_TO_BASE_ASHTRAY_P2,
	SMOKING_ACTIVITY_WEED_TO_BASE_JOINT_P2,
	SMOKING_ACTIVITY_WEED_TO_BASE_JOINT_SPECIAL_P2,
	SMOKING_ACTIVITY_WEED_TO_BASE_LIGHTER_P2,
	
	// Transfer from Pose 2 Weed Base to Pose 1 Base
	SMOKING_ACTIVITY_WEED_TO_BASE_P1,
	SMOKING_ACTIVITY_WEED_TO_BASE_ASHTRAY_P1,
	SMOKING_ACTIVITY_WEED_TO_BASE_JOINT_P1,
	SMOKING_ACTIVITY_WEED_TO_BASE_JOINT_SPECIAL_P1,
	SMOKING_ACTIVITY_WEED_TO_BASE_LIGHTER_P1,
			
	// Relaxed pose after Enter Lamar Strain
	SMOKING_ACTIVITY_LAMAR_STRAIN,
	SMOKING_ACTIVITY_LAMAR_STRAIN_ASHTRAY,
	SMOKING_ACTIVITY_LAMAR_STRAIN_JOINT,
	SMOKING_ACTIVITY_LAMAR_STRAIN_JOINT_SPECIAL,
	SMOKING_ACTIVITY_LAMAR_STRAIN_LIGHTER,
		
	// Base relaxed pose after Lamar Strain
	SMOKING_ACTIVITY_LAMAR_STRAIN_BASE,
	SMOKING_ACTIVITY_LAMAR_STRAIN_BASE_ASHTRAY,
	SMOKING_ACTIVITY_LAMAR_STRAIN_BASE_JOINT,
	SMOKING_ACTIVITY_LAMAR_STRAIN_BASE_JOINT_SPECIAL,
	SMOKING_ACTIVITY_LAMAR_STRAIN_BASE_LIGHTER,
		
	// Transfer between base poses
	SMOKING_ACTIVITY_TRANS_P1_TO_P2,
	SMOKING_ACTIVITY_TRANS_P1_TO_P2_ASHTRAY,
	SMOKING_ACTIVITY_TRANS_P1_TO_P2_JOINT,
	SMOKING_ACTIVITY_TRANS_P1_TO_P2_JOINT_SPECIAL,
	SMOKING_ACTIVITY_TRANS_P1_TO_P2_LIGHTER,
	
	SMOKING_ACTIVITY_TRANS_P2_TO_P1,
	SMOKING_ACTIVITY_TRANS_P2_TO_P1_ASHTRAY,
	SMOKING_ACTIVITY_TRANS_P2_TO_P1_JOINT,
	SMOKING_ACTIVITY_TRANS_P2_TO_P1_JOINT_SPECIAL,
	SMOKING_ACTIVITY_TRANS_P2_TO_P1_LIGHTER			
ENDENUM

ENUM SMOKING_POSE
	SMOKING_POSE_1,
	SMOKING_POSE_2
ENDENUM

STRUCT SMOKING_ACTIVITY
	VECTOR vLocateA
	VECTOR vLocateB
	VECTOR vScenePos
	VECTOR vSceneRot
	FLOAT fHeading
	BOOL bMakePlayerHigh
ENDSTRUCT

STRUCT SMOKING_ACTIVITY_DATA
	FLOAT fLocateWidth = 1.0
	
	INT iBS
	INT iSyncSceneID = -1
	INT iSmokingActivityID
	INT iSmokingContextIntention = NEW_CONTEXT_INTENTION
	
	PED_COMP_NAME_ENUM eStoredMask
	
	PTFX_ID ptfxJoint
	PTFX_ID ptfxJointLS
	PTFX_ID ptfxLighter
	
	SMOKING_ACTIVITY_STATE eCurrentState = SMOKING_ACTIVITY_STATE_INIT

	SMOKING_ACTIVITY sSmokingActivities[MAX_SMOKING_ACTIVITIES]
	
	INT iActiveSmokingActivities
	
	SMOKING_POSE eCurrentPose = SMOKING_POSE_1		
	
	OBJECT_INDEX objCollisions[MAX_HIDDEN_SCENE_OBJECTS]
ENDSTRUCT
SMOKING_ACTIVITY_DATA sSmokingActivityData

FUNC STRING GET_SMOKING_POSE_AS_STRING(SMOKING_POSE ePose)
	SWITCH ePose
		CASE SMOKING_POSE_1				RETURN "SMOKING_POSE_1"
		CASE SMOKING_POSE_2				RETURN "SMOKING_POSE_2"
	ENDSWITCH
	
	RETURN "Invalid pose."
ENDFUNC

PROC SET_SMOKING_POSE(SMOKING_POSE ePose)
	PRINTLN("[MUSIC_STUDIO_SMOKING] - SET_SMOKING_POSE: from ", GET_SMOKING_POSE_AS_STRING(sSmokingActivityData.eCurrentPose), " to ", GET_SMOKING_POSE_AS_STRING(ePose))
	sSmokingActivityData.eCurrentPose = ePose
ENDPROC

FUNC STRING GET_SMOKING_ACTIVITY_STATE_AS_STRING(SMOKING_ACTIVITY_STATE eState)
	SWITCH eState
		CASE SMOKING_ACTIVITY_STATE_INIT					RETURN "SMOKING_ACTIVITY_STATE_INIT"
		CASE SMOKING_ACTIVITY_STATE_PROMPT					RETURN "SMOKING_ACTIVITY_STATE_PROMPT"
		CASE SMOKING_ACTIVITY_STATE_WALK					RETURN "SMOKING_ACTIVITY_STATE_WALK"
		CASE SMOKING_ACTIVITY_STATE_WALKING_TO_POSITION		RETURN "SMOKING_ACTIVITY_STATE_WALKING_TO_POSITION"
		CASE SMOKING_ACTIVITY_STATE_WAIT_TO_SIT				RETURN "SMOKING_ACTIVITY_STATE_WAIT_TO_SIT"
		CASE SMOKING_ACTIVITY_STATE_IDLE					RETURN "SMOKING_ACTIVITY_STATE_IDLE"
		CASE SMOKING_ACTIVITY_STATE_TRANSFER_BASE_POSE		RETURN "SMOKING_ACTIVITY_STATE_TRANSFER_BASE_POSE"
		CASE SMOKING_ACTIVITY_STATE_WEED_IDLE				RETURN "SMOKING_ACTIVITY_STATE_WEED_IDLE"
		CASE SMOKING_ACTIVITY_STATE_SMOKING					RETURN "SMOKING_ACTIVITY_STATE_SMOKING"
		CASE SMOKING_ACTIVITY_STATE_EXIT_SMOKING			RETURN "SMOKING_ACTIVITY_STATE_EXIT_SMOKING"
		CASE SMOKING_ACTIVITY_STATE_LAMAR_STRAIN			RETURN "SMOKING_ACTIVITY_STATE_LAMAR_STRAIN"
		CASE SMOKING_ACTIVITY_STATE_LAMAR_STRAIN_BASE		RETURN "SMOKING_ACTIVITY_STATE_LAMAR_STRAIN_BASE"
		CASE SMOKING_ACTIVITY_STATE_EXITING					RETURN "SMOKING_ACTIVITY_STATE_EXITING"
	ENDSWITCH
	
	RETURN "Invalid state."
ENDFUNC

PROC SET_SMOKING_ACTIVITY_STATE(SMOKING_ACTIVITY_STATE eNewState)
	sSmokingActivityData.eCurrentState = eNewState
	
	PRINTLN("[MUSIC_STUDIO_SMOKING] - SET_SMOKING_ACTIVITY_STATE: ", GET_SMOKING_ACTIVITY_STATE_AS_STRING(eNewState))
ENDPROC

FUNC BOOL IS_ACTIVITY_LEFT_SIDE_SOFA()
	RETURN sSmokingActivityData.iSmokingActivityID = 0
ENDFUNC

FUNC STRING BUILD_CLIP_NAME(STRING clip, BOOL bInsertPose = TRUE, BOOL bInsertTrans = FALSE)
	TEXT_LABEL_63 clipName

	IF IS_PED_WEARING_HIGH_HEELS(PLAYER_PED_ID())
		clipName += "female"
	ELSE
		clipName += "male"
	ENDIF
	
	IF IS_ACTIVITY_LEFT_SIDE_SOFA()
		clipName += "_pos_b_"
	ELSE
		clipName += "_pos_c_"
	ENDIF
	
	IF bInsertTrans = TRUE
		clipName += "trans_"
	ENDIF
	
	IF bInsertPose = TRUE
		IF sSmokingActivityData.eCurrentPose = SMOKING_POSE_1
			clipName += "p1_"
		ELSE
			clipName += "p2_"
		ENDIF
	ENDIF
	
	clipName += clip
	
	RETURN GET_STRING_FROM_TL(clipName)
ENDFUNC

FUNC STRING GET_SMOKING_ACTIVITY_ANIM_CLIP(SMOKING_ACTIVITY_CLIP eClip)	
	SWITCH eClip
		CASE SMOKING_ACTIVITY_ENTER			
			RETURN BUILD_CLIP_NAME("enter")//"male_pos_c_p1_enter"				
		BREAK
								
		CASE SMOKING_ACTIVITY_EXIT
			RETURN BUILD_CLIP_NAME("exit")//"male_pos_c_p1_exit"
		BREAK
										
		CASE SMOKING_ACTIVITY_BASE
			RETURN BUILD_CLIP_NAME("base")//"male_pos_c_p1_base"
		BREAK
		
		CASE SMOKING_ACTIVITY_BASE_ASHTRAY
			RETURN BUILD_CLIP_NAME("base_ashtray")//"male_pos_c_p1_base_ashtray"
		BREAK
		
		CASE SMOKING_ACTIVITY_BASE_JOINT
			RETURN BUILD_CLIP_NAME("base_normal_weed")//"male_pos_c_p1_base_normal_weed"
		BREAK
				
		CASE SMOKING_ACTIVITY_BASE_JOINT_SPECIAL
			RETURN BUILD_CLIP_NAME("base_lamar_strain")//"male_pos_c_p1_base_lamar_strain"
		BREAK
		
		CASE SMOKING_ACTIVITY_BASE_LIGHTER
			RETURN BUILD_CLIP_NAME("base_lighter")//"male_pos_c_p1_base_lighter"
		BREAK
												
		CASE SMOKING_ACTIVITY_IDLE_01
			RETURN BUILD_CLIP_NAME("idle_01")//"male_pos_a_p1_idle_01"
		BREAK
										
		CASE SMOKING_ACTIVITY_IDLE_01_ASHTRAY
			RETURN BUILD_CLIP_NAME("idle_01_ashtray")//"male_pos_c_p1_idle_01_ashtray"
		BREAK
		
		CASE SMOKING_ACTIVITY_IDLE_01_JOINT
			RETURN BUILD_CLIP_NAME("idle_01_normal_weed")//"male_pos_c_p1_idle_01_normal_weed"
		BREAK
		
		CASE SMOKING_ACTIVITY_IDLE_01_JOINT_SPECIAL
			RETURN BUILD_CLIP_NAME("idle_01_lamar_strain")//"male_pos_c_p1_idle_01_lamar_strain"
		BREAK
		
		CASE SMOKING_ACTIVITY_IDLE_01_LIGHTER
			RETURN BUILD_CLIP_NAME("idle_01_lighter")//"male_pos_c_p1_idle_01_lighter"
		BREAK
						
		CASE SMOKING_ACTIVITY_IDLE_02
			RETURN BUILD_CLIP_NAME("idle_02")//"male_pos_a_p1_idle_02"
		BREAK
										
		CASE SMOKING_ACTIVITY_IDLE_02_ASHTRAY
			RETURN BUILD_CLIP_NAME("idle_02_ashtray")//"male_pos_c_p1_idle_02_ashtray"
		BREAK
		
		CASE SMOKING_ACTIVITY_IDLE_02_JOINT
			RETURN BUILD_CLIP_NAME("idle_02_normal_weed")//"male_pos_c_p1_idle_02_normal_weed"
		BREAK
		
		CASE SMOKING_ACTIVITY_IDLE_02_JOINT_SPECIAL
			RETURN BUILD_CLIP_NAME("idle_02_lamar_strain")//"male_pos_c_p1_idle_02_lamar_strain"
		BREAK
		
		CASE SMOKING_ACTIVITY_IDLE_02_LIGHTER
			RETURN BUILD_CLIP_NAME("idle_02_lighter")//"male_pos_c_p1_idle_02_lighter"
		BREAK
						
		CASE SMOKING_ACTIVITY_IDLE_03
			RETURN BUILD_CLIP_NAME("idle_03")//"male_pos_a_p1_idle_03"
		BREAK
										
		CASE SMOKING_ACTIVITY_IDLE_03_ASHTRAY
			RETURN BUILD_CLIP_NAME("idle_03_ashtray")//"male_pos_c_p1_idle_03_ashtray"
		BREAK
		
		CASE SMOKING_ACTIVITY_IDLE_03_JOINT
			RETURN BUILD_CLIP_NAME("idle_03_normal_weed")//"male_pos_c_p1_idle_03_normal_weed"
		BREAK
		
		CASE SMOKING_ACTIVITY_IDLE_03_JOINT_SPECIAL
			RETURN BUILD_CLIP_NAME("idle_03_lamar_strain")//"male_pos_c_p1_idle_03_lamar_strain"
		BREAK
		
		CASE SMOKING_ACTIVITY_IDLE_03_LIGHTER
			RETURN BUILD_CLIP_NAME("idle_03_lighter")//"male_pos_c_p1_idle_03_lighter"
		BREAK
																
		CASE SMOKING_ACTIVITY_NORMAL_ENTER
			RETURN BUILD_CLIP_NAME("enter_normal_weed")//"male_pos_c_p1_enter_normal_weed"
		BREAK
							
		CASE SMOKING_ACTIVITY_NORMAL_ENTER_ASHTRAY
			RETURN BUILD_CLIP_NAME("enter_normal_weed_ashtray")//"male_pos_c_p1_enter_normal_ashtray"
		BREAK
		
		CASE SMOKING_ACTIVITY_NORMAL_ENTER_JOINT
			RETURN BUILD_CLIP_NAME("enter_normal_weed_normal_weed")//"male_pos_c_p1_enter_normal_weed_normal_weed"
		BREAK
		
		CASE SMOKING_ACTIVITY_NORMAL_ENTER_JOINT_SPECIAL
			RETURN BUILD_CLIP_NAME("enter_normal_weed_lamar_strain")//"male_pos_c_p1_enter_normal_weed_lamar_strain"
		BREAK
		
		CASE SMOKING_ACTIVITY_NORMAL_ENTER_LIGHTER
			RETURN BUILD_CLIP_NAME("enter_normal_weed_lighter")//"male_pos_c_p1_enter_normal_lighter"
		BREAK				
		
		CASE SMOKING_ACTIVITY_LAMAR_STRAIN_ENTER
			RETURN BUILD_CLIP_NAME("enter_lamar_strain")//"male_pos_c_p1_enter_lamar_strain"
		BREAK
		
		CASE SMOKING_ACTIVITY_LAMAR_STRAIN_ENTER_ASHTRAY
			RETURN BUILD_CLIP_NAME("enter_lamar_strain_ashtray")//"male_pos_c_p1_enter_lamar_strain_ashtray"
		BREAK
		
		CASE SMOKING_ACTIVITY_LAMAR_STRAIN_ENTER_JOINT
			RETURN BUILD_CLIP_NAME("enter_lamar_strain_normal_weed")//"male_pos_c_p1_enter_lamar_strain_normal_weed"
		BREAK
		
		CASE SMOKING_ACTIVITY_LAMAR_STRAIN_ENTER_JOINT_SPECIAL
			RETURN BUILD_CLIP_NAME("enter_lamar_strain_lamar_strain")//"male_pos_c_p1_enter_lamar_strain_lamar_strain"
		BREAK
		
		CASE SMOKING_ACTIVITY_LAMAR_STRAIN_ENTER_LIGHTER
			RETURN BUILD_CLIP_NAME("enter_lamar_strain_lighter")//"male_pos_c_p1_enter_lamar_strain_lighter"
		BREAK																
		
		CASE SMOKING_ACTIVITY_WEED_BASE
			RETURN BUILD_CLIP_NAME("weed_base")//"male_pos_c_p1_weed_base"
		BREAK
		
		CASE SMOKING_ACTIVITY_WEED_BASE_ASHTRAY
			RETURN BUILD_CLIP_NAME("weed_base_ashtray")//"male_pos_c_p1_weed_base_ashtray"
		BREAK
		
		CASE SMOKING_ACTIVITY_WEED_BASE_JOINT
			RETURN BUILD_CLIP_NAME("weed_base_normal_weed")//"male_pos_c_p1_weed_base_normal_weed"
		BREAK
		
		CASE SMOKING_ACTIVITY_WEED_BASE_JOINT_SPECIAL
			RETURN BUILD_CLIP_NAME("weed_base_lamar_strain")//"male_pos_c_p1_weed_base_lamar_strain"
		BREAK
		
		CASE SMOKING_ACTIVITY_WEED_BASE_LIGHTER
			RETURN BUILD_CLIP_NAME("weed_base_lighter")//"male_pos_c_p1_weed_base_lighter"
		BREAK
				
		CASE SMOKING_ACTIVITY_WEED_IDLE_01
			RETURN BUILD_CLIP_NAME("weed_idle_01")//"male_pos_c_p1_weed_idle_01"
		BREAK
		
		CASE SMOKING_ACTIVITY_WEED_IDLE_01_ASHTRAY
			RETURN BUILD_CLIP_NAME("weed_idle_01_ashtray")//"male_pos_c_p1_weed_idle_01_ashtray"
		BREAK
		
		CASE SMOKING_ACTIVITY_WEED_IDLE_01_JOINT
			RETURN BUILD_CLIP_NAME("weed_idle_01_normal_weed")//"male_pos_c_p1_weed_idle_01_normal_weed"
		BREAK
		
		CASE SMOKING_ACTIVITY_WEED_IDLE_01_JOINT_SPECIAL
			RETURN BUILD_CLIP_NAME("weed_idle_01_lamar_strain")//"male_pos_c_p1_weed_idle_01_lamar_strain"
		BREAK
		
		CASE SMOKING_ACTIVITY_WEED_IDLE_01_LIGHTER
			RETURN BUILD_CLIP_NAME("weed_idle_01_lighter")//"male_pos_c_p1_weed_idle_01_lighter"
		BREAK
				
		CASE SMOKING_ACTIVITY_WEED_IDLE_02
			RETURN BUILD_CLIP_NAME("weed_idle_02")//"male_pos_c_p1_weed_idle_02"
		BREAK
		
		CASE SMOKING_ACTIVITY_WEED_IDLE_02_ASHTRAY
			RETURN BUILD_CLIP_NAME("weed_idle_02_ashtray")//"male_pos_c_p1_weed_idle_02_ashtray"
		BREAK
		
		CASE SMOKING_ACTIVITY_WEED_IDLE_02_JOINT
			RETURN BUILD_CLIP_NAME("weed_idle_02_normal_weed")//"male_pos_c_p1_weed_idle_02_normal_weed"
		BREAK
		
		CASE SMOKING_ACTIVITY_WEED_IDLE_02_JOINT_SPECIAL
			RETURN BUILD_CLIP_NAME("weed_idle_02_lamar_strain")//"male_pos_c_p1_weed_idle_02_lamar_strain"
		BREAK
		
		CASE SMOKING_ACTIVITY_WEED_IDLE_02_LIGHTER
			RETURN BUILD_CLIP_NAME("weed_idle_02_lighter")//"male_pos_c_p1_weed_idle_02_lighter"
		BREAK
				
		CASE SMOKING_ACTIVITY_WEED_IDLE_03
			RETURN BUILD_CLIP_NAME("weed_idle_03")//"male_pos_c_p1_weed_idle_03"
		BREAK
		
		CASE SMOKING_ACTIVITY_WEED_IDLE_03_ASHTRAY
			RETURN BUILD_CLIP_NAME("weed_idle_03_ashtray")//"male_pos_c_p1_weed_idle_03_ashtray"
		BREAK
		
		CASE SMOKING_ACTIVITY_WEED_IDLE_03_JOINT
			RETURN BUILD_CLIP_NAME("weed_idle_03_normal_weed")//"male_pos_c_p1_weed_idle_03_normal_weed"
		BREAK
		
		CASE SMOKING_ACTIVITY_WEED_IDLE_03_JOINT_SPECIAL
			RETURN BUILD_CLIP_NAME("weed_idle_03_lamar_strain")//"male_pos_c_p1_weed_idle_03_lamar_strain"
		BREAK
		
		CASE SMOKING_ACTIVITY_WEED_IDLE_03_LIGHTER
			RETURN BUILD_CLIP_NAME("weed_idle_03_lighter")//"male_pos_c_p1_weed_idle_03_lighter"
		BREAK
												
		CASE SMOKING_ACTIVITY_WEED_EXIT
			RETURN BUILD_CLIP_NAME("weed_exit")//"male_pos_c_p1_weed_exit"
		BREAK
													
		CASE SMOKING_ACTIVITY_WEED_EXIT_ASHTRAY
			RETURN BUILD_CLIP_NAME("weed_exit_ashtray")//"male_pos_c_p1_weed_exit_ashtray"
		BREAK
		
		CASE SMOKING_ACTIVITY_WEED_EXIT_JOINT
			RETURN BUILD_CLIP_NAME("weed_exit_normal_weed")//"male_pos_c_p1_weed_exit_normal_weed"
		BREAK
		
		CASE SMOKING_ACTIVITY_WEED_EXIT_JOINT_SPECIAL
			RETURN BUILD_CLIP_NAME("weed_exit_lamar_strain")//"male_pos_c_p1_weed_exit_lamar_strain"
		BREAK
		
		CASE SMOKING_ACTIVITY_WEED_EXIT_LIGHTER
			RETURN BUILD_CLIP_NAME("weed_exit_lighter")//"male_pos_c_p1_weed_exit_lighter"
		BREAK
				
		CASE SMOKING_ACTIVITY_WEED_SMOKE_01
			RETURN BUILD_CLIP_NAME("weed_smoke_01")//"male_pos_c_p1_weed_smoke_01"
		BREAK

		CASE SMOKING_ACTIVITY_WEED_SMOKE_ASHTRAY_01
			RETURN BUILD_CLIP_NAME("weed_smoke_01_ashtray")//"male_pos_c_p1_weed_smoke_01_ashtray"
		BREAK
		
		CASE SMOKING_ACTIVITY_WEED_SMOKE_JOINT_01
			RETURN BUILD_CLIP_NAME("weed_smoke_01_normal_weed")//"male_pos_c_p1_weed_smoke_01_normal_weed"
		BREAK
		
		CASE SMOKING_ACTIVITY_WEED_SMOKE_JOINT_SPECIAL_01
			RETURN BUILD_CLIP_NAME("weed_smoke_01_lamar_strain")//"male_pos_c_p1_weed_smoke_01_lamar_strain"
		BREAK
		
		CASE SMOKING_ACTIVITY_WEED_SMOKE_LIGHTER_01
			RETURN BUILD_CLIP_NAME("weed_smoke_01_lighter")//"male_pos_c_p1_weed_smoke_01_lighter"
		BREAK
		
		CASE SMOKING_ACTIVITY_WEED_SMOKE_02
			RETURN BUILD_CLIP_NAME("weed_smoke_02")//"male_pos_c_p1_weed_smoke_02"
		BREAK

		CASE SMOKING_ACTIVITY_WEED_SMOKE_ASHTRAY_02
			RETURN BUILD_CLIP_NAME("weed_smoke_02_ashtray")//"male_pos_c_p1_weed_smoke_02_ashtray"
		BREAK
		
		CASE SMOKING_ACTIVITY_WEED_SMOKE_JOINT_02
			RETURN BUILD_CLIP_NAME("weed_smoke_02_normal_weed")//"male_pos_c_p1_weed_smoke_02_normal_weed"
		BREAK
		
		CASE SMOKING_ACTIVITY_WEED_SMOKE_JOINT_SPECIAL_02
			RETURN BUILD_CLIP_NAME("weed_smoke_02_lamar_strain")//"male_pos_c_p1_weed_smoke_02_lamar_strain"
		BREAK
		
		CASE SMOKING_ACTIVITY_WEED_SMOKE_LIGHTER_02
			RETURN BUILD_CLIP_NAME("weed_smoke_02_lighter")//"male_pos_c_p1_weed_smoke_02_lighter"
		BREAK
								
		CASE SMOKING_ACTIVITY_WEED_TO_LAMAR
			RETURN BUILD_CLIP_NAME("weed_to_lamar", TRUE, TRUE)//"male_pos_c_trans_p1_weed_to_lamar"
		BREAK
		
		CASE SMOKING_ACTIVITY_WEED_TO_LAMAR_ASHTRAY
			RETURN BUILD_CLIP_NAME("weed_to_lamar_ashtray", TRUE, TRUE)//"male_pos_c_trans_p1_weed_to_lamar_ashtray"
		BREAK
		
		CASE SMOKING_ACTIVITY_WEED_TO_LAMAR_JOINT
			RETURN BUILD_CLIP_NAME("weed_to_lamar_normal_weed", TRUE, TRUE)//"male_pos_c_trans_p1_weed_to_lamar_normal_weed"
		BREAK
		
		CASE SMOKING_ACTIVITY_WEED_TO_LAMAR_JOINT_SPECIAL
			RETURN BUILD_CLIP_NAME("weed_to_lamar_lamar_strain", TRUE, TRUE)//"male_pos_c_trans_p1_weed_to_lamar_lamar_strain"
		BREAK
		
		CASE SMOKING_ACTIVITY_WEED_TO_LAMAR_LIGHTER
			RETURN BUILD_CLIP_NAME("weed_to_lamar_lighter", TRUE, TRUE)//"male_pos_c_trans_p1_weed_to_lamar_lighter"
		BREAK
												
		CASE SMOKING_ACTIVITY_WEED_TO_BASE
			RETURN BUILD_CLIP_NAME("p1_weed_to_p1", FALSE, TRUE)//"male_pos_c_trans_p1_weed_to_p1"
		BREAK
		
		CASE SMOKING_ACTIVITY_WEED_TO_BASE_ASHTRAY
			RETURN BUILD_CLIP_NAME("p1_weed_to_p1_ashtray", FALSE, TRUE)//"male_pos_c_trans_p1_weed_to_p1_ashtray"
		BREAK
		
		CASE SMOKING_ACTIVITY_WEED_TO_BASE_JOINT
			RETURN BUILD_CLIP_NAME("p1_weed_to_p1_normal_weed", FALSE, TRUE)//"male_pos_c_trans_p1_weed_to_p1_normal_weed"
		BREAK
		
		CASE SMOKING_ACTIVITY_WEED_TO_BASE_JOINT_SPECIAL
			RETURN BUILD_CLIP_NAME("p1_weed_to_p1_lamar_strain", FALSE, TRUE)//"male_pos_c_trans_p1_weed_to_p1_lamar_strain"
		BREAK
		
		CASE SMOKING_ACTIVITY_WEED_TO_BASE_LIGHTER
			RETURN BUILD_CLIP_NAME("p1_weed_to_p1_lighter", FALSE, TRUE)//"male_pos_c_trans_p1_weed_to_p1_lighter"
		BREAK
												
		CASE SMOKING_ACTIVITY_WEED_TO_BASE_P2
			RETURN BUILD_CLIP_NAME("p1_weed_to_p2", FALSE, TRUE)//"male_pos_c_trans_p1_weed_to_p2"
		BREAK
		
		CASE SMOKING_ACTIVITY_WEED_TO_BASE_ASHTRAY_P2
			RETURN BUILD_CLIP_NAME("p1_weed_to_p2_ashtray", FALSE, TRUE)//"male_pos_c_trans_p1_weed_to_p2_ashtray"
		BREAK
		
		CASE SMOKING_ACTIVITY_WEED_TO_BASE_JOINT_P2
			RETURN BUILD_CLIP_NAME("p1_weed_to_p2_normal_weed", FALSE, TRUE)//"male_pos_c_trans_p1_weed_to_p2_normal_weed"
		BREAK
		
		CASE SMOKING_ACTIVITY_WEED_TO_BASE_JOINT_SPECIAL_P2
			RETURN BUILD_CLIP_NAME("p1_weed_to_p2_lamar_strain", FALSE, TRUE)//"male_pos_c_trans_p1_weed_to_p2_lamar_strain"
		BREAK
		
		CASE SMOKING_ACTIVITY_WEED_TO_BASE_LIGHTER_P2
			RETURN BUILD_CLIP_NAME("p1_weed_to_p2_lighter", FALSE, TRUE)//"male_pos_c_trans_p1_weed_to_p2_lighter"
		BREAK
		
		CASE SMOKING_ACTIVITY_WEED_TO_BASE_P1
			RETURN BUILD_CLIP_NAME("p2_weed_to_p1", FALSE, TRUE)//"male_pos_c_trans_p2_weed_to_p1"
		BREAK
		
		CASE SMOKING_ACTIVITY_WEED_TO_BASE_ASHTRAY_P1
			RETURN BUILD_CLIP_NAME("p2_weed_to_p1_ashtray", FALSE, TRUE)//"male_pos_c_trans_p2_weed_to_p1_ashtray"
		BREAK
		
		CASE SMOKING_ACTIVITY_WEED_TO_BASE_JOINT_P1
			RETURN BUILD_CLIP_NAME("p2_weed_to_p1_normal_weed", FALSE, TRUE)//"male_pos_c_trans_p2_weed_to_p1_normal_weed"
		BREAK
		
		CASE SMOKING_ACTIVITY_WEED_TO_BASE_JOINT_SPECIAL_P1
			RETURN BUILD_CLIP_NAME("p2_weed_to_p1_lamar_strain", FALSE, TRUE)//"male_pos_c_trans_p2_weed_to_p1_lamar_strain"
		BREAK
		
		CASE SMOKING_ACTIVITY_WEED_TO_BASE_LIGHTER_P1
			RETURN BUILD_CLIP_NAME("p2_weed_to_p1_lighter", FALSE, TRUE)//"male_pos_c_trans_p2_weed_to_p1_lighter"
		BREAK
																						
		CASE SMOKING_ACTIVITY_LAMAR_STRAIN
			RETURN BUILD_CLIP_NAME("lamar_strain", FALSE)//"male_pos_c_lamar_strain"
		BREAK
		
		CASE SMOKING_ACTIVITY_LAMAR_STRAIN_ASHTRAY
			RETURN BUILD_CLIP_NAME("lamar_strain_ashtray", FALSE)//"male_pos_c_lamar_strain_ashtray"
		BREAK
		
		CASE SMOKING_ACTIVITY_LAMAR_STRAIN_JOINT
			RETURN BUILD_CLIP_NAME("lamar_strain_normal_weed", FALSE)//"male_pos_c_lamar_strain_normal_weed"
		BREAK
		
		CASE SMOKING_ACTIVITY_LAMAR_STRAIN_JOINT_SPECIAL
			RETURN BUILD_CLIP_NAME("lamar_strain_lamar_strain", FALSE)//"male_pos_c_lamar_strain_lamar_strain"
		BREAK
		
		CASE SMOKING_ACTIVITY_LAMAR_STRAIN_LIGHTER
			RETURN BUILD_CLIP_NAME("lamar_strain_lighter", FALSE)//"male_pos_c_lamar_strain_lighter"
		BREAK
				
		CASE SMOKING_ACTIVITY_LAMAR_STRAIN_BASE
			RETURN BUILD_CLIP_NAME("lamar_strain_base", FALSE)//"male_pos_c_lamar_strain_base"
		BREAK
		
		CASE SMOKING_ACTIVITY_LAMAR_STRAIN_BASE_ASHTRAY
			RETURN BUILD_CLIP_NAME("lamar_strain_base_ashtray", FALSE)//"male_pos_c_lamar_strain_base_ashtray"
		BREAK
		
		CASE SMOKING_ACTIVITY_LAMAR_STRAIN_BASE_JOINT
			RETURN BUILD_CLIP_NAME("lamar_strain_base_normal_weed", FALSE)//"male_pos_c_lamar_strain_base_normal_weed"
		BREAK
		
		CASE SMOKING_ACTIVITY_LAMAR_STRAIN_BASE_JOINT_SPECIAL
			RETURN BUILD_CLIP_NAME("lamar_strain_base_lamar_strain", FALSE)//"male_pos_c_lamar_strain_base_lamar_strain"
		BREAK
		
		CASE SMOKING_ACTIVITY_LAMAR_STRAIN_BASE_LIGHTER
			RETURN BUILD_CLIP_NAME("lamar_strain_base_lighter", FALSE)//"male_pos_c_lamar_strain_base_lighter"
		BREAK

		CASE SMOKING_ACTIVITY_TRANS_P1_TO_P2
			RETURN BUILD_CLIP_NAME("p1_to_p2", FALSE, TRUE)//"male_pos_c_trans_p1_to_p2"
		BREAK
		
		CASE SMOKING_ACTIVITY_TRANS_P1_TO_P2_ASHTRAY
			RETURN BUILD_CLIP_NAME("p1_to_p2_ashtray", FALSE, TRUE)//"male_pos_c_trans_p1_to_p2_ashtray"
		BREAK
		
		CASE SMOKING_ACTIVITY_TRANS_P1_TO_P2_JOINT
			RETURN BUILD_CLIP_NAME("p1_to_p2_normal_weed", FALSE, TRUE)//"male_pos_c_trans_p1_to_p2_normal_weed"
		BREAK
		
		CASE SMOKING_ACTIVITY_TRANS_P1_TO_P2_JOINT_SPECIAL
			RETURN BUILD_CLIP_NAME("p1_to_p2_lamar_strain", FALSE, TRUE)//"male_pos_c_trans_p1_to_p2_lamar_strain"
		BREAK
		
		CASE SMOKING_ACTIVITY_TRANS_P1_TO_P2_LIGHTER
			RETURN BUILD_CLIP_NAME("p1_to_p2_lighter", FALSE, TRUE)//"male_pos_c_trans_p1_to_p2_lighter"
		BREAK

		CASE SMOKING_ACTIVITY_TRANS_P2_TO_P1
			RETURN BUILD_CLIP_NAME("p2_to_p1", FALSE, TRUE)//"male_pos_c_trans_p2_to_p1"
		BREAK
		
		CASE SMOKING_ACTIVITY_TRANS_P2_TO_P1_ASHTRAY
			RETURN BUILD_CLIP_NAME("p2_to_p1_ashtray", FALSE, TRUE)//"male_pos_c_trans_p2_to_p1_ashtray"
		BREAK
		
		CASE SMOKING_ACTIVITY_TRANS_P2_TO_P1_JOINT
			RETURN BUILD_CLIP_NAME("p2_to_p1_normal_weed", FALSE, TRUE)//"male_pos_c_trans_p2_to_p1_normal_weed"
		BREAK
		
		CASE SMOKING_ACTIVITY_TRANS_P2_TO_P1_JOINT_SPECIAL
			RETURN BUILD_CLIP_NAME("p2_to_p1_lamar_strain", FALSE, TRUE)//"male_pos_c_trans_p2_to_p1_lamar_strain"
		BREAK
		
		CASE SMOKING_ACTIVITY_TRANS_P2_TO_P1_LIGHTER
			RETURN BUILD_CLIP_NAME("p2_to_p1_lighter", FALSE, TRUE)//"male_pos_c_trans_p2_to_p1_lighter"
		BREAK								
	ENDSWITCH
		
	RETURN "INVALID CLIP NAME"
ENDFUNC

FUNC STRING GET_SMOKING_ACTIVITY_ANIM_DICT()		
	IF IS_PED_WEARING_HIGH_HEELS(PLAYER_PED_ID())
		RETURN "anim@scripted@player@fix_astu_ig8_weed_smoke_v1@heeled@"
	ENDIF
	
	RETURN "anim@scripted@player@fix_astu_ig8_weed_smoke_v1@male@"
ENDFUNC

FUNC VECTOR GET_PROP_POSITION_ASHTRAY(INT iActivity)
	IF iActivity = 0
		RETURN <<-1006.5438, -79.0538, -99.4851>>
	ELSE
		RETURN <<-1006.5181, -78.1404, -99.4849>>
	ENDIF
ENDFUNC

FUNC VECTOR GET_PROP_ROTATION_ASHTRAY(INT iActivity)
	IF iActivity = 0
		RETURN <<0.0, 0.0, -90.10>>
	ELSE
		RETURN <<0.0, 0.0, 0.0>>
	ENDIF
ENDFUNC

FUNC VECTOR GET_PROP_POSITION_LIGHTER(INT iActivity)
	IF iActivity = 0
		RETURN <<-1006.5784, -79.1784, -99.4974>>
	ELSE
		RETURN <<-1006.5220, -78.2894, -99.4971>>
	ENDIF	
ENDFUNC

FUNC VECTOR GET_PROP_ROTATION_LIGHTER(INT iActivity)
	IF iActivity = 0
		RETURN <<-90.0, -90.0, 180.0>>
	ELSE
		RETURN <<-89.2648, 38.4126, -71.9980>>
	ENDIF	
ENDFUNC

FUNC VECTOR GET_PROP_POSITION_JOINT(INT iActivity)
	IF iActivity = 0
		RETURN <<-1006.6101, -79.1065, -99.4540>>
	ELSE
		RETURN <<-1006.5750, -78.2018, -99.4540>>
	ENDIF	
ENDFUNC

FUNC VECTOR GET_PROP_ROTATION_JOINT(INT iActivity)
	IF iActivity = 0
		RETURN <<7.3229, -159.3244, 18.6625>>
	ELSE
		RETURN <<-1.7006, -19.7833, -135.4718>>
	ENDIF	
ENDFUNC

FUNC VECTOR GET_PROP_POSITION_JOINT_SPECIAL(INT iActivity)
	IF iActivity = 0
		RETURN <<-1006.6101, -79.0028, -99.4540>>
	ELSE
		RETURN <<-1006.5695, -78.0693, -99.4524>>
	ENDIF	
ENDFUNC

FUNC VECTOR GET_PROP_ROTATION_JOINT_SPECIAL(INT iActivity)
	IF iActivity = 0
		RETURN <<-7.3229, -159.3244, -18.6624>>
	ELSE
		RETURN <<17.7090, -158.0644, -50.9087>>
	ENDIF	
ENDFUNC

FUNC VECTOR GET_SMOKING_SCENE_POSITION(INT iIndex)	
	IF iIndex >= 0 AND iIndex <= sSmokingActivityData.iActiveSmokingActivities	
		RETURN sSmokingActivityData.sSmokingActivities[iIndex].vScenePos
	ENDIF
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC VECTOR GET_SMOKING_SCENE_ROTATION(INT iIndex)
	IF iIndex >= 0 AND iIndex <= sSmokingActivityData.iActiveSmokingActivities	
		RETURN sSmokingActivityData.sSmokingActivities[iIndex].vSceneRot
	ENDIF
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC FLOAT GET_SMOKING_SCENE_HEADING(INT iIndex)
	IF iIndex >= 0 AND iIndex <= sSmokingActivityData.iActiveSmokingActivities	
		RETURN sSmokingActivityData.sSmokingActivities[iIndex].fHeading
	ENDIF
	
	RETURN 0.0
ENDFUNC

FUNC BOOL SHOULD_MAKE_PLAYER_HIGH(INT iIndex)
	RETURN sSmokingActivityData.sSmokingActivities[iIndex].bMakePlayerHigh
ENDFUNC

FUNC MODEL_NAMES GET_SMOKING_PROP_NAME()
	RETURN INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("P_CS_Joint_01"))
ENDFUNC

FUNC MODEL_NAMES GET_LIGHTER_PROP_NAME()
	RETURN INT_TO_ENUM( MODEL_NAMES, HASH("ex_prop_exec_lighter_01"))
ENDFUNC

FUNC MODEL_NAMES GET_ASHTRAY_PROP_NAME()
	RETURN INT_TO_ENUM( MODEL_NAMES, HASH("ex_prop_exec_ashtray_01"))
ENDFUNC

PROC CLEAR_PED_FACE_PROPS()
	sSmokingActivityData.eStoredMask = GET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_BERD)
	
	IF IS_ITEM_A_HELMET(GET_ENTITY_MODEL(PLAYER_PED_ID()), COMP_TYPE_PROPS, GET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_PROPS, ENUM_TO_INT(ANCHOR_HEAD)))
		SET_BIT(sSmokingActivityData.iBS, SMOKING_ACTIVITY_BS_HELMET)
	ENDIF
	
	IF IS_PED_WEARING_PILOT_SUIT(PLAYER_PED_ID(), PED_COMP_TEETH)
		SET_BIT(sSmokingActivityData.iBS, SMOKING_ACTIVITY_BS_OXY_TUBE)
	ENDIF
	
	SET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_BERD, BERD_FMM_0_0, FALSE)
	
	REMOVE_MP_HEIST_GEAR(PLAYER_PED_ID(), HEIST_GEAR_NIGHTVISION)
	
	IF IS_PED_WEARING_HAZ_HOOD_UP(PLAYER_PED_ID())
		SWAP_CHEM_SUIT_HOOD_FOR_ALTERNATE(PLAYER_PED_ID())
		
		SET_BIT(sSmokingActivityData.iBS, SMOKING_ACTIVITY_BS_HOOD_UP)
	ENDIF
	
	IF IS_BIT_SET(sSmokingActivityData.iBS, SMOKING_ACTIVITY_BS_HELMET)
		CLEAR_PED_PROP(PLAYER_PED_ID(), ANCHOR_HEAD)
	ENDIF
	
	IF IS_BIT_SET(sSmokingActivityData.iBS, SMOKING_ACTIVITY_BS_OXY_TUBE)
		SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_TEETH, 0, 0)
	ENDIF
	
	SET_BIT(sSmokingActivityData.iBS, SMOKING_ACTIVITY_BS_CLEARED_FACE_PROPS)
ENDPROC

PROC RESET_PED_FACE_PROPS()
	IF IS_BIT_SET(sSmokingActivityData.iBS, SMOKING_ACTIVITY_BS_HOOD_UP)
		SWAP_CHEM_SUIT_HOOD_FOR_ALTERNATE(PLAYER_PED_ID())
		
		CLEAR_BIT(sSmokingActivityData.iBS, SMOKING_ACTIVITY_BS_HOOD_UP)
	ENDIF
	
	IF IS_BIT_SET(sSmokingActivityData.iBS, SMOKING_ACTIVITY_BS_CLEARED_FACE_PROPS)
		SET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_BERD, sSmokingActivityData.eStoredMask, FALSE)
		
		CLEAR_BIT(sSmokingActivityData.iBS, SMOKING_ACTIVITY_BS_CLEARED_FACE_PROPS)
	ENDIF
ENDPROC

PROC CLEANUP_SMOKING_ACTIVITY(BOOL bDeleteEntity = FALSE, BOOL bForceDelete = FALSE)		
	IF IS_PLAYER_IN_MUSIC_STUDIO(PLAYER_ID())
	AND IS_LOCAL_PLAYER_IN_MUSIC_STUDIO_SMOKING_ROOM()
		CLEAR_BIT(g_SimpleInteriorData.iEleventhBS, BS11_SIMPLE_INTERIOR_IN_MUSIC_STUDIO_SMOKING_AREA)
	ENDIF
		
	IF sSmokingActivityData.eCurrentState > SMOKING_ACTIVITY_STATE_WALK
		NETWORK_STOP_SYNCHRONISED_SCENE(sSmokingActivityData.iSyncSceneID)
		
		IF DOES_PARTICLE_FX_LOOPED_EXIST(sSmokingActivityData.ptfxJoint)
			STOP_PARTICLE_FX_LOOPED(sSmokingActivityData.ptfxJoint)
		ENDIF
		
		IF DOES_PARTICLE_FX_LOOPED_EXIST(sSmokingActivityData.ptfxJointLS)
			STOP_PARTICLE_FX_LOOPED(sSmokingActivityData.ptfxJointLS)
		ENDIF
		
		IF DOES_PARTICLE_FX_LOOPED_EXIST(sSmokingActivityData.ptfxLighter)
			STOP_PARTICLE_FX_LOOPED(sSmokingActivityData.ptfxLighter)
		ENDIF
		
		IF HAS_NAMED_PTFX_ASSET_LOADED("scr_sec")
			REMOVE_NAMED_PTFX_ASSET("scr_sec")
		ENDIF
		
		CLEAR_BIT(sSmokingActivityData.iBS, SMOKING_ACTIVITY_BS_SPARKS)
		CLEAR_BIT(sSmokingActivityData.iBS, SMOKING_ACTIVITY_BS_EXHALE_MOUTH)
		CLEAR_BIT(sSmokingActivityData.iBS, SMOKING_ACTIVITY_BS_INHALE_MOUTH)
		
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.objLighter[sSmokingActivityData.iSmokingActivityID])
		AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.objLighter[sSmokingActivityData.iSmokingActivityID])
			VECTOR vLighterPosition, vLighterRotation
			
			vLighterPosition = GET_PROP_POSITION_LIGHTER(sSmokingActivityData.iSmokingActivityID)
			vLighterRotation = GET_PROP_ROTATION_LIGHTER(sSmokingActivityData.iSmokingActivityID)
			
			SET_ENTITY_COORDS_NO_OFFSET(NET_TO_OBJ(serverBD.objLighter[sSmokingActivityData.iSmokingActivityID]), vLighterPosition)
			SET_ENTITY_ROTATION(NET_TO_OBJ(serverBD.objLighter[sSmokingActivityData.iSmokingActivityID]), vLighterRotation)
			FREEZE_ENTITY_POSITION(NET_TO_OBJ(serverBD.objLighter[sSmokingActivityData.iSmokingActivityID]), TRUE)
		ENDIF
		
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.objSmoking[sSmokingActivityData.iSmokingActivityID])
		AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.objSmoking[sSmokingActivityData.iSmokingActivityID])
			VECTOR vSmokingPosition, vSmokingRotation

			vSmokingPosition = GET_PROP_POSITION_JOINT(sSmokingActivityData.iSmokingActivityID)
			vSmokingRotation = GET_PROP_ROTATION_JOINT(sSmokingActivityData.iSmokingActivityID)
			
			SET_ENTITY_COORDS_NO_OFFSET(NET_TO_OBJ(serverBD.objSmoking[sSmokingActivityData.iSmokingActivityID]), vSmokingPosition)
			SET_ENTITY_ROTATION(NET_TO_OBJ(serverBD.objSmoking[sSmokingActivityData.iSmokingActivityID]), vSmokingRotation)
			FREEZE_ENTITY_POSITION(NET_TO_OBJ(serverBD.objSmoking[sSmokingActivityData.iSmokingActivityID]), TRUE)
		ENDIF
		
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.objSmokingSpecial[sSmokingActivityData.iSmokingActivityID])
		AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.objSmokingSpecial[sSmokingActivityData.iSmokingActivityID])
			VECTOR vSmokingPosition, vSmokingRotation
			
			vSmokingPosition = GET_PROP_POSITION_JOINT_SPECIAL(sSmokingActivityData.iSmokingActivityID)
			vSmokingRotation = GET_PROP_ROTATION_JOINT_SPECIAL(sSmokingActivityData.iSmokingActivityID)
			
			SET_ENTITY_COORDS_NO_OFFSET(NET_TO_OBJ(serverBD.objSmokingSpecial[sSmokingActivityData.iSmokingActivityID]), vSmokingPosition)
			SET_ENTITY_ROTATION(NET_TO_OBJ(serverBD.objSmokingSpecial[sSmokingActivityData.iSmokingActivityID]), vSmokingRotation)
			FREEZE_ENTITY_POSITION(NET_TO_OBJ(serverBD.objSmokingSpecial[sSmokingActivityData.iSmokingActivityID]), TRUE)
		ENDIF
		
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.objAshtray[sSmokingActivityData.iSmokingActivityID])
		AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.objAshtray[sSmokingActivityData.iSmokingActivityID])
			VECTOR vAshtrayPosition, vAshtrayRotation

			vAshtrayPosition = GET_PROP_POSITION_ASHTRAY(sSmokingActivityData.iSmokingActivityID)
			vAshtrayRotation = GET_PROP_ROTATION_ASHTRAY(sSmokingActivityData.iSmokingActivityID)
									
			SET_ENTITY_COORDS_NO_OFFSET(NET_TO_OBJ(serverBD.objAshtray[sSmokingActivityData.iSmokingActivityID]), vAshtrayPosition)
			SET_ENTITY_ROTATION(NET_TO_OBJ(serverBD.objAshtray[sSmokingActivityData.iSmokingActivityID]), vAshtrayRotation)
			FREEZE_ENTITY_POSITION(NET_TO_OBJ(serverBD.objAshtray[sSmokingActivityData.iSmokingActivityID]), TRUE)
		ENDIF
	ENDIF
	
	IF sSmokingActivityData.iSmokingContextIntention != NEW_CONTEXT_INTENTION
		RELEASE_CONTEXT_INTENTION(sSmokingActivityData.iSmokingContextIntention)
		sSmokingActivityData.iSmokingContextIntention = NEW_CONTEXT_INTENTION
	ENDIF
	
	IF bDeleteEntity
	AND sSmokingActivityData.eCurrentState > SMOKING_ACTIVITY_STATE_WALK
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.objLighter[sSmokingActivityData.iSmokingActivityID])
		AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.objLighter[sSmokingActivityData.iSmokingActivityID])
			DELETE_NET_ID(serverBD.objLighter[sSmokingActivityData.iSmokingActivityID])
		ENDIF
		
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.objSmoking[sSmokingActivityData.iSmokingActivityID])
		AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.objSmoking[sSmokingActivityData.iSmokingActivityID])
			DELETE_NET_ID(serverBD.objSmoking[sSmokingActivityData.iSmokingActivityID])
		ENDIF
		
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.objSmokingSpecial[sSmokingActivityData.iSmokingActivityID])
		AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.objSmokingSpecial[sSmokingActivityData.iSmokingActivityID])
			DELETE_NET_ID(serverBD.objSmokingSpecial[sSmokingActivityData.iSmokingActivityID])
		ENDIF
		
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.objAshtray[sSmokingActivityData.iSmokingActivityID])
		AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.objAshtray[sSmokingActivityData.iSmokingActivityID])
			DELETE_NET_ID(serverBD.objAshtray[sSmokingActivityData.iSmokingActivityID])
		ENDIF
	ENDIF
	
	IF bForceDelete
		INT i
		REPEAT MAX_SMOKING_ACTIVITIES i
			IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.objLighter[i])
			AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.objLighter[i])
				DELETE_NET_ID(serverBD.objLighter[i])
			ENDIF
						
			IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.objSmoking[i])
			AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.objSmoking[i])
				DELETE_NET_ID(serverBD.objSmoking[i])
			ENDIF
			
			IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.objSmokingSpecial[i])
			AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.objSmokingSpecial[i])
				DELETE_NET_ID(serverBD.objSmokingSpecial[i])
			ENDIF
								
			IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.objAshtray[i])
			AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.objAshtray[i])
				DELETE_NET_ID(serverBD.objAshtray[i])
			ENDIF		
		ENDREPEAT
	ENDIF
	
	sSmokingActivityData.iBS = 0
	
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("SAFEHOUSE_FRANKLIN_SOFA")
	
	REMOVE_ANIM_DICT(GET_SMOKING_ACTIVITY_ANIM_DICT())
	
	IF IS_BIT_SET(g_SimpleInteriorData.iEleventhBS, BS11_SIMPLE_INTERIOR_MUSIC_STUDIO_BAIL_SOMKIING_AREA_SEAT)
		CLEAR_BIT(g_SimpleInteriorData.iEleventhBS, BS11_SIMPLE_INTERIOR_MUSIC_STUDIO_BAIL_SOMKIING_AREA_SEAT)
		PRINTLN("CLEANUP_SMOKING_ACTIVITY BS11_SIMPLE_INTERIOR_MUSIC_STUDIO_BAIL_SOMKIING_AREA_SEAT")
	ENDIF
	
	IF sSmokingActivityData.eCurrentState > SMOKING_ACTIVITY_STATE_PROMPT
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_UseKinematicModeWhenStationary, FALSE)
			
			SET_PED_CAN_HEAD_IK(PLAYER_PED_ID(), TRUE)
		ENDIF
		
		CLEAR_BIT(g_SimpleInteriorData.iSeventhBS, BS7_SIMPLE_INTERIOR_USING_SEATED_ACTIVITY)
	ENDIF
	
	RESET_PED_FACE_PROPS()
	
	SET_PLAYER_IS_SMOKING_FIXER_SPECIAL_STRAIN(FALSE)
	SET_PLAYER_IN_FIXER_SPECIAL_STRAIN_CHAIR(FALSE)
	
	SET_SMOKING_ACTIVITY_STATE(SMOKING_ACTIVITY_STATE_INIT)
ENDPROC

PROC SCRIPT_CLEANUP()
	PRINTLN("[MUSIC_STUDIO_SMOKING] - SCRIPT_CLEANUP")
	
	CLEANUP_SMOKING_ACTIVITY(TRUE)
	
	INT iSofaPosition
	REPEAT MAX_HIDDEN_SCENE_OBJECTS iSofaPosition
		IF DOES_ENTITY_EXIST(sSmokingActivityData.objCollisions[iSofaPosition])
			DELETE_OBJECT(sSmokingActivityData.objCollisions[iSofaPosition])			
		ENDIF														
	ENDREPEAT	
	
	IF IS_BIT_SET(g_SimpleInteriorData.iEleventhBS, BS11_SIMPLE_INTERIOR_MUSIC_STUDIO_BAIL_SOMKIING_AREA_SEAT)
		CLEAR_BIT(g_SimpleInteriorData.iEleventhBS, BS11_SIMPLE_INTERIOR_MUSIC_STUDIO_BAIL_SOMKIING_AREA_SEAT)
		PRINTLN("CLEANUP_SMOKING_ACTIVITY BS11_SIMPLE_INTERIOR_MUSIC_STUDIO_BAIL_SOMKIING_AREA_SEAT")
	ENDIF

	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

FUNC BOOL IS_PLAYER_IN_SMOKING_ROOM()
	IF IS_PLAYER_IN_MUSIC_STUDIO(PLAYER_ID())
	AND IS_LOCAL_PLAYER_IN_MUSIC_STUDIO_SMOKING_ROOM()		
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

PROC ADD_NEW_SMOKING_ACTIVITY(VECTOR vLocateA, VECTOR vLocateB, VECTOR vScenePos, VECTOR vSceneRot, FLOAT fHeading, BOOL bMakePlayerHigh = FALSE)
	IF sSmokingActivityData.iActiveSmokingActivities < MAX_SMOKING_ACTIVITIES

		sSmokingActivityData.sSmokingActivities[sSmokingActivityData.iActiveSmokingActivities].vLocateA = vLocateA
		sSmokingActivityData.sSmokingActivities[sSmokingActivityData.iActiveSmokingActivities].vLocateB = vLocateB
		sSmokingActivityData.sSmokingActivities[sSmokingActivityData.iActiveSmokingActivities].vScenePos = vScenePos
		sSmokingActivityData.sSmokingActivities[sSmokingActivityData.iActiveSmokingActivities].vSceneRot = vSceneRot
		sSmokingActivityData.sSmokingActivities[sSmokingActivityData.iActiveSmokingActivities].fHeading = fHeading
		sSmokingActivityData.sSmokingActivities[sSmokingActivityData.iActiveSmokingActivities].bMakePlayerHigh = bMakePlayerHigh

		sSmokingActivityData.iActiveSmokingActivities++
	
	ELSE
		PRINTLN("[MUSIC_STUDIO_SMOKING] - ERROR - ADD_NEW_SMOKING_ACTIVITY - Trying to create more activities than MAX_SMOKING_ACTIVITIES")
	ENDIF
ENDPROC

PROC INITIALISE_SMOKING_ACTIVITY_DATA()	
	IF IS_PLAYER_IN_MUSIC_STUDIO(PLAYER_ID())
	AND IS_LOCAL_PLAYER_IN_MUSIC_STUDIO_SMOKING_ROOM()
		sSmokingActivityData.fLocateWidth = 0.8
	
		// Sofa left side
		ADD_NEW_SMOKING_ACTIVITY(<<-1007.980652,-79.057701,-98.003067>>, <<-1006.661316,-79.060822,-99.967491>>, << -1010.000, -70.000, -100.000 >>, <<0.0, 0.0, 0.0>>, 92.45, TRUE)	
		
		// Sofa right side
		ADD_NEW_SMOKING_ACTIVITY(<<-1007.932251,-78.031807,-98.003067>>, <<-1006.629395,-78.026947,-99.967491>> , << -1010.000, -70.000, -100.000 >>, <<0.0, 0.0, 0.0>>, 95.93, TRUE)				
	ENDIF
ENDPROC

PROC PROCESS_PRE_GAME()
	PRINTLN("[MUSIC_STUDIO_SMOKING] - PROCESS_PRE_GAME - Initialising")
	
	IF IS_PLAYER_IN_MUSIC_STUDIO(PLAYER_ID())
	AND IS_LOCAL_PLAYER_IN_MUSIC_STUDIO_SMOKING_ROOM()	
		NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE, GET_SIMPLE_INTERIOR_INT_SCRIPT_INSTANCE())	
	ENDIF
	
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	
	RESERVE_LOCAL_NETWORK_MISSION_OBJECTS(MAX_SMOKING_ACTIVITIES * 4) // (4 props required for each activity (lighter, normal weed, special weed, ashtray)
	
	IF NOT Wait_For_First_Network_Broadcast()
		PRINTLN("[MUSIC_STUDIO_SMOKING] - PROCESS_PRE_GAME - Failed to receive initial network broadcast.")
		
		SCRIPT_CLEANUP()
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		PRINTLN("[MUSIC_STUDIO_SMOKING] - PROCESS_PRE_GAME - Initialised.")
	ELSE
		PRINTLN("[MUSIC_STUDIO_SMOKING] - PROCESS_PRE_GAME - NETWORK_IS_GAME_IN_PROGRESS = FALSE")
		
		SCRIPT_CLEANUP()
	ENDIF
			
	INITIALISE_SMOKING_ACTIVITY_DATA()
ENDPROC

FUNC BOOL IS_PLAYER_IN_SMOKING_AREA(PLAYER_INDEX pPlayer)
	IF IS_NET_PLAYER_OK(pPlayer)
		PED_INDEX pPed = GET_PLAYER_PED(pPlayer)
		
		IF DOES_ENTITY_EXIST(pPed)	
			INT i
			REPEAT sSmokingActivityData.iActiveSmokingActivities i
				IF IS_ENTITY_IN_ANGLED_AREA(pPed, sSmokingActivityData.sSmokingActivities[i].vLocateA, sSmokingActivityData.sSmokingActivities[i].vLocateB, sSmokingActivityData.fLocateWidth)
					sSmokingActivityData.iSmokingActivityID = i
					RETURN TRUE
				ENDIF
			ENDREPEAT
		ENDIF		
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_LOCAL_PLAYER_USE_THIS_SEAT()
	PLAYER_INDEX playerToCheck
	
	INT iSeatID = sSmokingActivityData.iSmokingActivityID
		
	INT iPlayerIndex, iNumPart
	iNumPart = NETWORK_GET_NUM_PARTICIPANTS()
	REPEAT iNumPart iPlayerIndex
		IF iPlayerIndex != -1
		AND	NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPlayerIndex))
			playerToCheck = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPlayerIndex))
										
			IF IS_NET_PLAYER_OK(playerToCheck)
			AND playerToCheck != PLAYER_ID()
			AND NATIVE_TO_INT(playerToCheck) >= 0
				PED_INDEX pPed = GET_PLAYER_PED(playerToCheck)
				
				IF IS_ACTIVITY_LEFT_SIDE_SOFA()
				AND IS_PLAYER_SPAWNING_INTO_MUSIC_STUDIO_SMOKING_CHAIR(playerToCheck)
					RETURN FALSE
				ENDIF
				
				IF DOES_ENTITY_EXIST(pPed)
				AND NOT IS_ENTITY_DEAD(pPed)
					IF IS_ENTITY_IN_ANGLED_AREA(pPed, sSmokingActivityData.sSmokingActivities[iSeatID].vLocateA, sSmokingActivityData.sSmokingActivities[iSeatID].vLocateB, sSmokingActivityData.fLocateWidth + 0.3) // Increasing locate width when checking for other players in locate as extra measure to stop players sitting in same seat
						RETURN FALSE
					ENDIF
				ENDIF	
			ENDIF
		ENDIF
	ENDREPEAT

	RETURN TRUE
ENDFUNC

FUNC BOOL IS_PLAYER_FACING_SEAT()
	RETURN IS_HEADING_ACCEPTABLE(GET_ENTITY_HEADING(PLAYER_PED_ID()), GET_SMOKING_SCENE_HEADING(sSmokingActivityData.iSmokingActivityID), 60.0)	
ENDFUNC

PROC PLAY_SCENE(SMOKING_ACTIVITY_CLIP ePedClip, SMOKING_ACTIVITY_CLIP eAshtrayClip = SMOKING_ACTIVITY_INVALID, SMOKING_ACTIVITY_CLIP eJointClip = SMOKING_ACTIVITY_INVALID, SMOKING_ACTIVITY_CLIP eJointSpecialClip = SMOKING_ACTIVITY_INVALID, SMOKING_ACTIVITY_CLIP eLighterClip = SMOKING_ACTIVITY_INVALID)
	sSmokingActivityData.iSyncSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(GET_SMOKING_SCENE_POSITION(sSmokingActivityData.iSmokingActivityID), GET_SMOKING_SCENE_ROTATION(sSmokingActivityData.iSmokingActivityID), DEFAULT, TRUE, FALSE)
					
	NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), sSmokingActivityData.iSyncSceneID, GET_SMOKING_ACTIVITY_ANIM_DICT(), GET_SMOKING_ACTIVITY_ANIM_CLIP(ePedClip), REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT, REALLY_SLOW_BLEND_IN)	
		
	IF eAshtrayClip != SMOKING_ACTIVITY_INVALID
		NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_OBJ(serverBD.objAshtray[sSmokingActivityData.iSmokingActivityID]), sSmokingActivityData.iSyncSceneID, GET_SMOKING_ACTIVITY_ANIM_DICT(), GET_SMOKING_ACTIVITY_ANIM_CLIP(eAshtrayClip), INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE)
	ENDIF
	IF eJointClip != SMOKING_ACTIVITY_INVALID
		NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_OBJ(serverBD.objSmoking[sSmokingActivityData.iSmokingActivityID]), sSmokingActivityData.iSyncSceneID, GET_SMOKING_ACTIVITY_ANIM_DICT(), GET_SMOKING_ACTIVITY_ANIM_CLIP(eJointClip), INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE)
	ENDIF
	IF eJointSpecialClip != SMOKING_ACTIVITY_INVALID
		NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_OBJ(serverBD.objSmokingSpecial[sSmokingActivityData.iSmokingActivityID]), sSmokingActivityData.iSyncSceneID, GET_SMOKING_ACTIVITY_ANIM_DICT(), GET_SMOKING_ACTIVITY_ANIM_CLIP(eJointSpecialClip), INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE)
	ENDIF
	IF eLighterClip != SMOKING_ACTIVITY_INVALID
		NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_OBJ(serverBD.objLighter[sSmokingActivityData.iSmokingActivityID]), sSmokingActivityData.iSyncSceneID, GET_SMOKING_ACTIVITY_ANIM_DICT(), GET_SMOKING_ACTIVITY_ANIM_CLIP(eLighterClip), INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE)
	ENDIF	
				
	NETWORK_START_SYNCHRONISED_SCENE(sSmokingActivityData.iSyncSceneID)
ENDPROC

PROC PLAY_IDLE_SCENE()
	SMOKING_ACTIVITY_CLIP eIdle
	SMOKING_ACTIVITY_CLIP eIdleAshtray
	SMOKING_ACTIVITY_CLIP eIdleJoint
	SMOKING_ACTIVITY_CLIP eIdleJointSpecial
	SMOKING_ACTIVITY_CLIP eIdleLighter
	
	INT iIdle = GET_RANDOM_INT_IN_RANGE(0, 3)
	
	IF iIdle = 0
		eIdle = SMOKING_ACTIVITY_IDLE_01
		eIdleAshtray = SMOKING_ACTIVITY_IDLE_01_ASHTRAY
		eIdleJoint = SMOKING_ACTIVITY_IDLE_01_JOINT
		eIdleJointSpecial = SMOKING_ACTIVITY_IDLE_01_JOINT_SPECIAL
		eIdleLighter = SMOKING_ACTIVITY_IDLE_01_LIGHTER
	ELIF iIdle = 1
		eIdle = SMOKING_ACTIVITY_IDLE_02
		eIdleAshtray = SMOKING_ACTIVITY_IDLE_02_ASHTRAY
		eIdleJoint = SMOKING_ACTIVITY_IDLE_02_JOINT
		eIdleJointSpecial = SMOKING_ACTIVITY_IDLE_02_JOINT_SPECIAL
		eIdleLighter = SMOKING_ACTIVITY_IDLE_02_LIGHTER
	ELSE
		eIdle = SMOKING_ACTIVITY_IDLE_03
		eIdleAshtray = SMOKING_ACTIVITY_IDLE_03_ASHTRAY
		eIdleJoint = SMOKING_ACTIVITY_IDLE_03_JOINT
		eIdleJointSpecial = SMOKING_ACTIVITY_IDLE_03_JOINT_SPECIAL
		eIdleLighter = SMOKING_ACTIVITY_IDLE_03_LIGHTER
	ENDIF

	PLAY_SCENE(eIdle, eIdleAshtray, eIdleJoint, eIdleJointSpecial, eIdleLighter)
ENDPROC

PROC PLAY_WEED_IDLE_SCENE()
	SMOKING_ACTIVITY_CLIP eIdle
	SMOKING_ACTIVITY_CLIP eIdleAshtray
	SMOKING_ACTIVITY_CLIP eIdleJoint
	SMOKING_ACTIVITY_CLIP eIdleJointSpecial
	SMOKING_ACTIVITY_CLIP eIdleLighter
	
	INT iIdle = GET_RANDOM_INT_IN_RANGE(0, 3)
	
	IF iIdle = 0
		eIdle = SMOKING_ACTIVITY_WEED_IDLE_01
		eIdleAshtray = SMOKING_ACTIVITY_WEED_IDLE_01_ASHTRAY
		eIdleJoint = SMOKING_ACTIVITY_WEED_IDLE_01_JOINT
		eIdleJointSpecial = SMOKING_ACTIVITY_WEED_IDLE_01_JOINT_SPECIAL
		eIdleLighter = SMOKING_ACTIVITY_WEED_IDLE_01_LIGHTER
	ELIF iIdle = 1
		eIdle = SMOKING_ACTIVITY_WEED_IDLE_02
		eIdleAshtray = SMOKING_ACTIVITY_WEED_IDLE_02_ASHTRAY
		eIdleJoint = SMOKING_ACTIVITY_WEED_IDLE_02_JOINT
		eIdleJointSpecial = SMOKING_ACTIVITY_WEED_IDLE_02_JOINT_SPECIAL
		eIdleLighter = SMOKING_ACTIVITY_WEED_IDLE_02_LIGHTER
	ELSE
		eIdle = SMOKING_ACTIVITY_WEED_IDLE_03
		eIdleAshtray = SMOKING_ACTIVITY_WEED_IDLE_03_ASHTRAY
		eIdleJoint = SMOKING_ACTIVITY_WEED_IDLE_03_JOINT
		eIdleJointSpecial = SMOKING_ACTIVITY_WEED_IDLE_03_JOINT_SPECIAL
		eIdleLighter = SMOKING_ACTIVITY_WEED_IDLE_03_LIGHTER
	ENDIF

	PLAY_SCENE(eIdle, eIdleAshtray, eIdleJoint, eIdleJointSpecial, eIdleLighter)
ENDPROC

PROC PLAY_BASE_POSE_SWAP_SCENE()
	SMOKING_ACTIVITY_CLIP eClip
	SMOKING_ACTIVITY_CLIP eClipAshtray
	SMOKING_ACTIVITY_CLIP eClipJoint
	SMOKING_ACTIVITY_CLIP eClipJointSpecial
	SMOKING_ACTIVITY_CLIP eClipLighter
	
	IF sSmokingActivityData.eCurrentPose = SMOKING_POSE_1
		SET_SMOKING_POSE(SMOKING_POSE_2)
		
		eClip = SMOKING_ACTIVITY_TRANS_P1_TO_P2
		eClipAshtray = SMOKING_ACTIVITY_TRANS_P1_TO_P2_ASHTRAY
		eClipJoint = SMOKING_ACTIVITY_TRANS_P1_TO_P2_JOINT
		eClipJointSpecial = SMOKING_ACTIVITY_TRANS_P1_TO_P2_JOINT_SPECIAL
		eClipLighter = SMOKING_ACTIVITY_TRANS_P1_TO_P2_LIGHTER
	ELSE
		SET_SMOKING_POSE(SMOKING_POSE_1)
		
		eClip = SMOKING_ACTIVITY_TRANS_P2_TO_P1
		eClipAshtray = SMOKING_ACTIVITY_TRANS_P2_TO_P1_ASHTRAY
		eClipJoint = SMOKING_ACTIVITY_TRANS_P2_TO_P1_JOINT
		eClipJointSpecial = SMOKING_ACTIVITY_TRANS_P2_TO_P1_JOINT_SPECIAL
		eClipLighter = SMOKING_ACTIVITY_TRANS_P2_TO_P1_LIGHTER
	ENDIF
	
	PLAY_SCENE(eClip, eClipAshtray, eClipJoint, eClipJointSpecial, eClipLighter)
ENDPROC

PROC PLAY_WEED_BASE_TO_BASE_POSE_SWAP_SCENE()
	SMOKING_ACTIVITY_CLIP eClip
	SMOKING_ACTIVITY_CLIP eClipAshtray
	SMOKING_ACTIVITY_CLIP eClipJoint
	SMOKING_ACTIVITY_CLIP eClipJointSpecial
	SMOKING_ACTIVITY_CLIP eClipLighter
	
	IF sSmokingActivityData.eCurrentPose = SMOKING_POSE_1
		SET_SMOKING_POSE(SMOKING_POSE_2)
		
		eClip = SMOKING_ACTIVITY_WEED_TO_BASE_P2
		eClipAshtray = SMOKING_ACTIVITY_WEED_TO_BASE_ASHTRAY_P2
		eClipJoint = SMOKING_ACTIVITY_WEED_TO_BASE_JOINT_P2
		eClipJointSpecial = SMOKING_ACTIVITY_WEED_TO_BASE_JOINT_SPECIAL_P2
		eClipLighter = SMOKING_ACTIVITY_WEED_TO_BASE_LIGHTER_P2
	ELSE
		SET_SMOKING_POSE(SMOKING_POSE_1)
		
		eClip = SMOKING_ACTIVITY_WEED_TO_BASE_P1
		eClipAshtray = SMOKING_ACTIVITY_WEED_TO_BASE_ASHTRAY_P1
		eClipJoint = SMOKING_ACTIVITY_WEED_TO_BASE_JOINT_P1
		eClipJointSpecial = SMOKING_ACTIVITY_WEED_TO_BASE_JOINT_SPECIAL_P1
		eClipLighter = SMOKING_ACTIVITY_WEED_TO_BASE_LIGHTER_P1
	ENDIF
	
	PLAY_SCENE(eClip, eClipAshtray, eClipJoint, eClipJointSpecial, eClipLighter)
ENDPROC

FUNC BOOL REQUEST_CONTROL_OF_PROPS()
	
	// --- Normal cigarette ---
	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.objSmoking[sSmokingActivityData.iSmokingActivityID])
		RETURN FALSE
	ENDIF
	
	IF NOT TAKE_CONTROL_OF_NET_ID(serverBD.objSmoking[sSmokingActivityData.iSmokingActivityID])
		RETURN FALSE
	ENDIF
	
	// --- 420 ---
	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.objSmokingSpecial[sSmokingActivityData.iSmokingActivityID])
		RETURN FALSE
	ENDIF
	
	IF NOT TAKE_CONTROL_OF_NET_ID(serverBD.objSmokingSpecial[sSmokingActivityData.iSmokingActivityID])
		RETURN FALSE
	ENDIF
	
	// --- Ashtray ---
	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.objAshtray[sSmokingActivityData.iSmokingActivityID])
		RETURN FALSE
	ENDIF
	
	IF NOT TAKE_CONTROL_OF_NET_ID(serverBD.objAshtray[sSmokingActivityData.iSmokingActivityID])
		RETURN FALSE
	ENDIF
	
	// --- Lighter ---
	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.objLighter[sSmokingActivityData.iSmokingActivityID])
		RETURN FALSE
	ENDIF
	
	IF NOT TAKE_CONTROL_OF_NET_ID(serverBD.objLighter[sSmokingActivityData.iSmokingActivityID])
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC SHOW_SMOKING_PROMPT()
	TEXT_LABEL_15 textLabel = "MS_SMOKE"	
	
	IF NOT HAS_PLAYER_UNLOCKED_FIXER_SHORT_TRIPS(PLAYER_ID())
		textLabel += "_L"		
	ENDIF
	
	IF HAS_PLAYER_COMPLETED_ALL_FIXER_SHORT_TRIPS(PLAYER_ID())
		SWITCH GET_PLAYER_CURRENT_FIXER_SHORT_TRIP(PLAYER_ID())
			CASE FST_SHORT_TRIP_1 	textLabel += "_R_1"		BREAK
			CASE FST_SHORT_TRIP_2 	textLabel += "_R_2"		BREAK
			CASE FST_SHORT_TRIP_3 	textLabel += "_R_3"		BREAK
		ENDSWITCH			
	ENDIF
	
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		textLabel += "_PC"
	ENDIF
	
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		REGISTER_CONTEXT_INTENTION(sSmokingActivityData.iSmokingContextIntention, CP_HIGH_PRIORITY, textLabel)
	ELSE
		REGISTER_CONTEXT_INTENTION(sSmokingActivityData.iSmokingContextIntention, CP_HIGH_PRIORITY, textLabel)
	ENDIF
ENDPROC

PROC USE_SPECIAL_WEED()
	// Player needs to have unlocked the Short Trips missions and completed the first one as part of the story before they can launch any from the smoking room
	IF HAS_PLAYER_UNLOCKED_FIXER_SHORT_TRIPS(PLAYER_ID())
	AND HAS_LOCAL_PLAYER_COMPLETED_FIXER_SHORT_TRIP(FST_SHORT_TRIP_1)	
		CLEAR_PED_FACE_PROPS()
		
		g_drunkCameraTimeCycleModifier = 0.9 // end Stoned effect early so we can see the FixerShortTrip effect
	
		// If we are in Idle (player is not holding a joint) then play "enter lamar strain" (player picks up special joint)
		IF sSmokingActivityData.eCurrentState = SMOKING_ACTIVITY_STATE_IDLE
			PLAY_SCENE(SMOKING_ACTIVITY_LAMAR_STRAIN_ENTER, SMOKING_ACTIVITY_LAMAR_STRAIN_ENTER_ASHTRAY, SMOKING_ACTIVITY_LAMAR_STRAIN_ENTER_JOINT, SMOKING_ACTIVITY_LAMAR_STRAIN_ENTER_JOINT_SPECIAL, SMOKING_ACTIVITY_LAMAR_STRAIN_ENTER_LIGHTER)
		ELSE  // Otherwise we are in Weed Idle (player is holding joint) then play "weed to lamar" (player places normal joint back and picks up special joint)
			PLAY_SCENE(SMOKING_ACTIVITY_WEED_TO_LAMAR, SMOKING_ACTIVITY_WEED_TO_LAMAR_ASHTRAY, SMOKING_ACTIVITY_WEED_TO_LAMAR_JOINT, SMOKING_ACTIVITY_WEED_TO_LAMAR_JOINT_SPECIAL, SMOKING_ACTIVITY_WEED_TO_LAMAR_LIGHTER)					
		ENDIF						
									
		RELEASE_CONTEXT_INTENTION(sSmokingActivityData.iSmokingContextIntention)
		sSmokingActivityData.iSmokingContextIntention = NEW_CONTEXT_INTENTION
		
		SET_PLAYER_IS_SMOKING_FIXER_SPECIAL_STRAIN(TRUE)
		
		SET_SMOKING_ACTIVITY_STATE(SMOKING_ACTIVITY_STATE_LAMAR_STRAIN)	
	ELSE
		PLAY_SOUND_FRONTEND(-1, "ERROR", "HUD_FRONTEND_DEFAULT_SOUNDSET")
		PRINTLN("[MUSIC_STUDIO_SMOKING] - USE_SPECIAL_WEED - Player has not unlocked Short Trips yet so cannot use Special strain.")
	ENDIF
ENDPROC

// Play smoking scene from Weed Base (where ped is actively holding the joint)
// Pose 1 has variation of smoking (male_pos_b_p1_weed_smoke_01/02) while Pose 2 only has 1 version
PROC USE_WEED_FROM_WEED_BASE()
	INT iVariation = 0

	IF sSmokingActivityData.eCurrentPose = SMOKING_POSE_1
		iVariation = GET_RANDOM_INT_IN_RANGE(0, 2)
	ENDIF	

	IF iVariation = 0 // pick a variation of this clip
		PLAY_SCENE(SMOKING_ACTIVITY_WEED_SMOKE_01, SMOKING_ACTIVITY_WEED_SMOKE_ASHTRAY_01, SMOKING_ACTIVITY_WEED_SMOKE_JOINT_01, SMOKING_ACTIVITY_WEED_SMOKE_JOINT_SPECIAL_01, SMOKING_ACTIVITY_WEED_SMOKE_LIGHTER_01)
	ELSE
		PLAY_SCENE(SMOKING_ACTIVITY_WEED_SMOKE_02, SMOKING_ACTIVITY_WEED_SMOKE_ASHTRAY_02, SMOKING_ACTIVITY_WEED_SMOKE_JOINT_02, SMOKING_ACTIVITY_WEED_SMOKE_JOINT_SPECIAL_02, SMOKING_ACTIVITY_WEED_SMOKE_LIGHTER_02)
	ENDIF
ENDPROC

PROC MAINTAIN_SMOKING_PTFX()
	IF NOT DOES_ENTITY_EXIST(PLAYER_PED_ID())
	OR IS_PED_INJURED(PLAYER_PED_ID())
		EXIT
	ENDIF
	
	REQUEST_NAMED_PTFX_ASSET("scr_sec")
	
	IF HAS_NAMED_PTFX_ASSET_LOADED("scr_sec")
		IF HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), HASH("vfx_sec_weed_lighter_start"))
			IF NOT IS_BIT_SET(sSmokingActivityData.iBS, SMOKING_ACTIVITY_BS_SPARKS)
				USE_PARTICLE_FX_ASSET("scr_sec")
				
				START_NETWORKED_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_sec_weed_lighter_sparks", NET_TO_OBJ(serverBD.objLighter[sSmokingActivityData.iSmokingActivityID]), <<0.0, 0.0, 0.05>>, <<0.0, 0.0, 0.0>>)
				
				SET_BIT(sSmokingActivityData.iBS, SMOKING_ACTIVITY_BS_SPARKS)
			ENDIF
			
			IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(sSmokingActivityData.ptfxLighter)
				USE_PARTICLE_FX_ASSET("scr_sec")
				
				sSmokingActivityData.ptfxLighter = START_NETWORKED_PARTICLE_FX_LOOPED_ON_ENTITY("scr_sec_weed_lighter_flame", NET_TO_OBJ(serverBD.objLighter[sSmokingActivityData.iSmokingActivityID]), <<0.0, 0.0, 0.05>>, <<0.0, 0.0, 0.0>>)
			ENDIF
		ENDIF
		
		IF HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), HASH("vfx_sec_weed_lighter_stop"))
			IF DOES_PARTICLE_FX_LOOPED_EXIST(sSmokingActivityData.ptfxLighter)
				STOP_PARTICLE_FX_LOOPED(sSmokingActivityData.ptfxLighter)
			ENDIF
		ENDIF
		
		IF HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), HASH("vfx_sec_weed_smoke_start"))
		AND NOT DOES_PARTICLE_FX_LOOPED_EXIST(sSmokingActivityData.ptfxJoint)
			USE_PARTICLE_FX_ASSET("scr_sec")
			
			sSmokingActivityData.ptfxJoint = START_NETWORKED_PARTICLE_FX_LOOPED_ON_ENTITY("scr_sec_weed_smoke", NET_TO_OBJ(serverBD.objSmoking[sSmokingActivityData.iSmokingActivityID]), <<-0.07, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
		ENDIF
		
		IF HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), HASH("vfx_sec_weed_smoke_stop"))
			IF DOES_PARTICLE_FX_LOOPED_EXIST(sSmokingActivityData.ptfxJoint)
				STOP_PARTICLE_FX_LOOPED(sSmokingActivityData.ptfxJoint)
			ENDIF
		ENDIF
		
		IF HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), HASH("vfx_sec_weed_smoke_ls_start"))
		AND NOT DOES_PARTICLE_FX_LOOPED_EXIST(sSmokingActivityData.ptfxJointLS)
			USE_PARTICLE_FX_ASSET("scr_sec")
			
			sSmokingActivityData.ptfxJointLS = START_NETWORKED_PARTICLE_FX_LOOPED_ON_ENTITY("scr_sec_weed_smoke", NET_TO_OBJ(serverBD.objSmokingSpecial[sSmokingActivityData.iSmokingActivityID]), <<-0.07, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
		ENDIF
		
		IF HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), HASH("vfx_sec_weed_smoke_ls_stop"))
			IF DOES_PARTICLE_FX_LOOPED_EXIST(sSmokingActivityData.ptfxJointLS)
				STOP_PARTICLE_FX_LOOPED(sSmokingActivityData.ptfxJointLS)
			ENDIF
		ENDIF
		
		IF HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), HASH("vfx_sec_weed_smoke_exhale_start"))
			IF NOT IS_BIT_SET(sSmokingActivityData.iBS, SMOKING_ACTIVITY_BS_EXHALE_MOUTH)
				USE_PARTICLE_FX_ASSET("scr_sec")
				
				IF IS_LOCAL_PLAYER_SMOKING_FIXER_SPECIAL_STRAIN()
					Player_Takes_Weed_Hit(PLAYER_PED_ID(), NULL, 1, SMOKING_EFFECT_SHORT_TRIP)
				ELSE
					SET_DRUNK_WEED_EFFECT_STRENGTH(0.5)
					Player_Takes_Weed_Hit(PLAYER_PED_ID())
				ENDIF

				START_NETWORKED_PARTICLE_FX_NON_LOOPED_ON_PED_BONE("scr_sec_weed_smoke_exhale", PLAYER_PED_ID(), <<-0.025, 0.13, 0.0>>, <<0.0, 0.0, 0.0>>, BONETAG_HEAD)
				
				SET_BIT(sSmokingActivityData.iBS, SMOKING_ACTIVITY_BS_EXHALE_MOUTH)
			ENDIF
		ENDIF
		
		IF HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), HASH("vfx_sec_weed_smoke_inhale"))
			IF NOT IS_BIT_SET(sSmokingActivityData.iBS, SMOKING_ACTIVITY_BS_INHALE_MOUTH)
				SET_DRUNK_WEED_EFFECT_STRENGTH(0.5)
				Player_Takes_Weed_Hit(PLAYER_PED_ID())		
				
				SET_BIT(sSmokingActivityData.iBS, SMOKING_ACTIVITY_BS_INHALE_MOUTH)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_SAFE_TO_DISPLAY_PROMPT()
	IF IS_BIT_SET(g_SimpleInteriorData.iEleventhBS, BS11_SIMPLE_INTERIOR_MUSIC_STUDIO_BLOCK_SMOKING_CHAIR)
		RETURN FALSE
	ENDIF	
	
	IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	// We are spawing into seat by pass all other checks
	IF IS_PLAYER_SPAWNING_INTO_MUSIC_STUDIO_SMOKING_CHAIR(PLAYER_ID())
		RETURN TRUE
	ENDIF	
	
	IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
		RETURN FALSE
	ENDIF	
	
	IF IS_ANY_INTERACTION_ANIM_PLAYING()
		RETURN FALSE
	ENDIF
	
	IF IS_PAUSE_MENU_ACTIVE_EX()
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_simpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_INSTANT_PASS_OUT)
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	IF NOT IS_PLAYER_IN_SMOKING_AREA(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	IF NOT CAN_LOCAL_PLAYER_USE_THIS_SEAT()
		RETURN FALSE
	ENDIF
	
	IF NOT IS_PLAYER_FACING_SEAT()
		RETURN FALSE
	ENDIF
	
	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.objLighter[sSmokingActivityData.iSmokingActivityID])
		RETURN FALSE
	ENDIF
	
	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.objSmoking[sSmokingActivityData.iSmokingActivityID])	
		RETURN FALSE
	ENDIF
	
	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.objSmokingSpecial[sSmokingActivityData.iSmokingActivityID])
		RETURN FALSE
	ENDIF
	
	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.objAshtray[sSmokingActivityData.iSmokingActivityID])
		RETURN FALSE
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		RETURN FALSE
	ENDIF

	RETURN TRUE
ENDFUNC

PROC MAINTAIN_SMOKING_ACTIVITY()
	INT iSmokingLocalScene
	
	IF (g_SimpleInteriorData.bForceDrinkActivitiesCleanup
	OR (g_SpawnData.bPassedOutDrunk AND IS_SCREEN_FADED_OUT()))
	OR IS_TRANSITION_ACTIVE()
	OR NOT IS_SKYSWOOP_AT_GROUND()
	AND (sSmokingActivityData.eCurrentState > SMOKING_ACTIVITY_STATE_WALK)
		CLEANUP_SMOKING_ACTIVITY(TRUE)
		
		g_SimpleInteriorData.bForceDrinkActivitiesCleanup = FALSE
		
	ENDIF
	
	IF sSmokingActivityData.eCurrentState > SMOKING_ACTIVITY_STATE_PROMPT
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PED_CAN_HEAD_IK(PLAYER_PED_ID(), FALSE)
		
		SET_THIRD_PERSON_CAM_RELATIVE_PITCH_LIMITS_THIS_UPDATE(-70.0027, -5.1277)
		
		HIDE_HUD_AND_RADAR_THIS_FRAME()
	ENDIF
	
	IF sSmokingActivityData.eCurrentState > SMOKING_ACTIVITY_STATE_WALK
		MAINTAIN_SMOKING_PTFX()
		REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
		DISABLE_SELECTOR_THIS_FRAME()
	ENDIF
	
	SWITCH sSmokingActivityData.eCurrentState
		CASE SMOKING_ACTIVITY_STATE_INIT
			SET_PLAYER_IS_SMOKING_FIXER_SPECIAL_STRAIN(FALSE)
			SET_PLAYER_IN_FIXER_SPECIAL_STRAIN_CHAIR(FALSE)
		
			// Chose an initial pose randomly
			IF GET_RANDOM_INT_IN_RANGE(0, 2) = 0
				sSmokingActivityData.eCurrentPose = SMOKING_POSE_1
			ELSE
				sSmokingActivityData.eCurrentPose = SMOKING_POSE_2
			ENDIF
		
			SET_SMOKING_ACTIVITY_STATE(SMOKING_ACTIVITY_STATE_PROMPT)
		BREAK
		
		CASE SMOKING_ACTIVITY_STATE_PROMPT
			IF IS_SAFE_TO_DISPLAY_PROMPT()
				IF IS_PLAYER_IN_MUSIC_STUDIO(PLAYER_ID())
				AND IS_LOCAL_PLAYER_IN_MUSIC_STUDIO_SMOKING_ROOM()				
					SET_BIT(g_SimpleInteriorData.iEleventhBS, BS11_SIMPLE_INTERIOR_IN_MUSIC_STUDIO_SMOKING_AREA)
				ENDIF
				
				IF sSmokingActivityData.iSmokingContextIntention = NEW_CONTEXT_INTENTION
				AND NOT IS_PLAYER_SPAWNING_INTO_MUSIC_STUDIO_SMOKING_CHAIR(PLAYER_ID())
					REGISTER_CONTEXT_INTENTION(sSmokingActivityData.iSmokingContextIntention, CP_HIGH_PRIORITY, "BLUNT_SIT_PRMPT")
				ELSE
					IF HAS_CONTEXT_BUTTON_TRIGGERED(sSmokingActivityData.iSmokingContextIntention)
					OR IS_PLAYER_SPAWNING_INTO_MUSIC_STUDIO_SMOKING_CHAIR(PLAYER_ID())
						RELEASE_CONTEXT_INTENTION(sSmokingActivityData.iSmokingContextIntention)
						sSmokingActivityData.iSmokingContextIntention = NEW_CONTEXT_INTENTION
						
						SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_UseKinematicModeWhenStationary, TRUE)
						
						SET_BIT(g_SimpleInteriorData.iSeventhBS, BS7_SIMPLE_INTERIOR_USING_SEATED_ACTIVITY)
						
						IF IS_PLAYER_SPAWNING_INTO_MUSIC_STUDIO_SMOKING_CHAIR(PLAYER_ID())
							REQUEST_NAMED_PTFX_ASSET("scr_sec")
							REQUEST_ANIM_DICT(GET_SMOKING_ACTIVITY_ANIM_DICT())
							
							IF REQUEST_SCRIPT_AUDIO_BANK("SAFEHOUSE_FRANKLIN_SOFA")
							AND HAS_NAMED_PTFX_ASSET_LOADED("scr_sec")
							AND HAS_ANIM_DICT_LOADED(GET_SMOKING_ACTIVITY_ANIM_DICT())
								SET_SMOKING_ACTIVITY_STATE(SMOKING_ACTIVITY_STATE_WAIT_TO_SIT)
							ENDIF
						ELSE
							SET_SMOKING_ACTIVITY_STATE(SMOKING_ACTIVITY_STATE_WALK)
						ENDIF
					ENDIF
				ENDIF
			ELSE									
				IF IS_PLAYER_IN_MUSIC_STUDIO(PLAYER_ID())
				AND IS_LOCAL_PLAYER_IN_MUSIC_STUDIO_SMOKING_ROOM()
					CLEAR_BIT(g_SimpleInteriorData.iEleventhBS, BS11_SIMPLE_INTERIOR_IN_MUSIC_STUDIO_SMOKING_AREA)
				ENDIF
								
				IF sSmokingActivityData.iSmokingContextIntention != NEW_CONTEXT_INTENTION
					RELEASE_CONTEXT_INTENTION(sSmokingActivityData.iSmokingContextIntention)
					sSmokingActivityData.iSmokingContextIntention = NEW_CONTEXT_INTENTION
				ENDIF
			ENDIF
		BREAK
		
		CASE SMOKING_ACTIVITY_STATE_WALK
			REQUEST_NAMED_PTFX_ASSET("scr_sec")
			REQUEST_ANIM_DICT(GET_SMOKING_ACTIVITY_ANIM_DICT())

			IF REQUEST_SCRIPT_AUDIO_BANK("SAFEHOUSE_FRANKLIN_SOFA")
			AND HAS_NAMED_PTFX_ASSET_LOADED("scr_sec")
			AND HAS_ANIM_DICT_LOADED(GET_SMOKING_ACTIVITY_ANIM_DICT())
				
				IF IS_ACTIVITY_LEFT_SIDE_SOFA()
					TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), <<-1006.7897, -79.0990, -99.0000>>, PEDMOVEBLENDRATIO_WALK, DEFAULT, 92.45, 0.1)
				ELSE
					TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), <<-1006.8047, -77.9789, -99.0000>>, PEDMOVEBLENDRATIO_WALK, DEFAULT, 95.93, 0.1)
				ENDIF
				
				CLEAR_BIT(sSmokingActivityData.iBS, SMOKING_ACTIVITY_BS_PED_IS_WALKING)
																					 
				SET_SMOKING_ACTIVITY_STATE(SMOKING_ACTIVITY_STATE_WALKING_TO_POSITION)
			ENDIF				
			
		BREAK
		
		CASE SMOKING_ACTIVITY_STATE_WALKING_TO_POSITION
			IF IS_PED_WALKING(PLAYER_PED_ID())
				SET_BIT(sSmokingActivityData.iBS, SMOKING_ACTIVITY_BS_PED_IS_WALKING)
			ENDIF
		
			IF REQUEST_CONTROL_OF_PROPS()
			AND (GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) <> PERFORMING_TASK OR (NOT IS_PED_WALKING(PLAYER_PED_ID()) AND IS_BIT_SET(sSmokingActivityData.iBS, SMOKING_ACTIVITY_BS_PED_IS_WALKING)))
			AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) <> WAITING_TO_START_TASK
				PLAY_SCENE(SMOKING_ACTIVITY_ENTER)
	
				SET_SMOKING_ACTIVITY_STATE(SMOKING_ACTIVITY_STATE_WAIT_TO_SIT)
			ENDIF
		BREAK
		
		CASE SMOKING_ACTIVITY_STATE_WAIT_TO_SIT
			REQUEST_CONTROL_OF_PROPS()
			
			IF REQUEST_CONTROL_OF_PROPS()
				iSmokingLocalScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sSmokingActivityData.iSyncSceneID)
				
				IF iSmokingLocalScene = -1
				OR GET_SYNCHRONIZED_SCENE_PHASE(iSmokingLocalScene) >= 0.99
				OR IS_PLAYER_SPAWNING_INTO_MUSIC_STUDIO_SMOKING_CHAIR(PLAYER_ID())
					PLAY_SCENE(SMOKING_ACTIVITY_BASE, SMOKING_ACTIVITY_BASE_ASHTRAY, SMOKING_ACTIVITY_BASE_JOINT, SMOKING_ACTIVITY_BASE_JOINT_SPECIAL, SMOKING_ACTIVITY_BASE_LIGHTER)
										
					SET_PLAYER_IN_FIXER_SPECIAL_STRAIN_CHAIR(TRUE)
					
					SET_PLAYER_SPAWNING_INTO_MUSIC_STUDIO_SMOKING_CHAIR(FALSE)
					
					SET_SMOKING_ACTIVITY_STATE(SMOKING_ACTIVITY_STATE_IDLE)
				ENDIF
			ENDIF
		BREAK
		
		CASE SMOKING_ACTIVITY_STATE_IDLE
			REQUEST_CONTROL_OF_PROPS()
			
			IF REQUEST_CONTROL_OF_PROPS()
				// Choose transitions while we are idling
				iSmokingLocalScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sSmokingActivityData.iSyncSceneID)
				IF iSmokingLocalScene = -1
				OR GET_SYNCHRONIZED_SCENE_PHASE(iSmokingLocalScene) >= 0.99		
					CLEAR_BIT(sSmokingActivityData.iBS, SMOKING_ACTIVITY_BS_SPARKS)
					CLEAR_BIT(sSmokingActivityData.iBS, SMOKING_ACTIVITY_BS_EXHALE_MOUTH)
					CLEAR_BIT(sSmokingActivityData.iBS, SMOKING_ACTIVITY_BS_INHALE_MOUTH)
					
				
					FLOAT iRandom
					iRandom = GET_RANDOM_FLOAT_IN_RANGE(0.0, 1.0)
					
					IF iRandom <= 0.35 // 35% to pick idle variation 01/02/03
						PLAY_IDLE_SCENE()
						
					ELIF iRandom >= 0.85 // 15% to go to go to base with alternate pose
						PLAY_BASE_POSE_SWAP_SCENE()
						SET_SMOKING_ACTIVITY_STATE(SMOKING_ACTIVITY_STATE_TRANSFER_BASE_POSE)
					ELSE
						PLAY_SCENE(SMOKING_ACTIVITY_BASE, SMOKING_ACTIVITY_BASE_ASHTRAY, SMOKING_ACTIVITY_BASE_JOINT, SMOKING_ACTIVITY_BASE_JOINT_SPECIAL, SMOKING_ACTIVITY_BASE_LIGHTER)
					ENDIF													
				ENDIF
																
				IF IS_NET_PLAYER_OK(PLAYER_ID())
				AND NOT IS_ANY_INTERACTION_ANIM_PLAYING()
				AND NOT IS_PAUSE_MENU_ACTIVE_EX()
				AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
				AND NOT IS_INTERACTION_MENU_OPEN()
					IF HAVE_CONTROLS_CHANGED(FRONTEND_CONTROL)
						IF sSmokingActivityData.iSmokingContextIntention != NEW_CONTEXT_INTENTION
							RELEASE_CONTEXT_INTENTION(sSmokingActivityData.iSmokingContextIntention)
							sSmokingActivityData.iSmokingContextIntention = NEW_CONTEXT_INTENTION
						ENDIF
					ENDIF
					
					IF sSmokingActivityData.iSmokingContextIntention = NEW_CONTEXT_INTENTION																				
						SHOW_SMOKING_PROMPT()
													
					ELSE
						CONTROL_ACTION playerControlAction
						
						IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
							playerControlAction = INPUT_SCRIPT_RRIGHT
						ELSE
							playerControlAction = INPUT_FRONTEND_RIGHT
						ENDIF
						
						IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, playerControlAction) // EXIT	
						OR IS_BIT_SET(g_SimpleInteriorData.iEleventhBS, BS11_SIMPLE_INTERIOR_MUSIC_STUDIO_BAIL_SOMKIING_AREA_SEAT)
							PLAY_SCENE(SMOKING_ACTIVITY_EXIT)
							
							RELEASE_CONTEXT_INTENTION(sSmokingActivityData.iSmokingContextIntention)
							sSmokingActivityData.iSmokingContextIntention = NEW_CONTEXT_INTENTION
							
							SET_SMOKING_ACTIVITY_STATE(SMOKING_ACTIVITY_STATE_EXITING)
						ELIF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X) // NORMAL WEED
							CLEAR_PED_FACE_PROPS()
							
							PLAY_SCENE(SMOKING_ACTIVITY_NORMAL_ENTER, SMOKING_ACTIVITY_NORMAL_ENTER_ASHTRAY, SMOKING_ACTIVITY_NORMAL_ENTER_JOINT, SMOKING_ACTIVITY_NORMAL_ENTER_JOINT_SPECIAL, SMOKING_ACTIVITY_NORMAL_ENTER_LIGHTER)
																					
							RELEASE_CONTEXT_INTENTION(sSmokingActivityData.iSmokingContextIntention)
							sSmokingActivityData.iSmokingContextIntention = NEW_CONTEXT_INTENTION
							
							SET_SMOKING_ACTIVITY_STATE(SMOKING_ACTIVITY_STATE_SMOKING)		
						ELIF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT) // SPECIAL WEED
							USE_SPECIAL_WEED()																																		
						ENDIF
					ENDIF
				ELSE
					IF sSmokingActivityData.iSmokingContextIntention != NEW_CONTEXT_INTENTION
						RELEASE_CONTEXT_INTENTION(sSmokingActivityData.iSmokingContextIntention)
						sSmokingActivityData.iSmokingContextIntention = NEW_CONTEXT_INTENTION
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE SMOKING_ACTIVITY_STATE_TRANSFER_BASE_POSE
			REQUEST_CONTROL_OF_PROPS()
			
			IF REQUEST_CONTROL_OF_PROPS()		
				iSmokingLocalScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sSmokingActivityData.iSyncSceneID)
			
				IF iSmokingLocalScene = -1
				OR GET_SYNCHRONIZED_SCENE_PHASE(iSmokingLocalScene) >= 0.99												
					PLAY_SCENE(SMOKING_ACTIVITY_BASE, SMOKING_ACTIVITY_BASE_ASHTRAY, SMOKING_ACTIVITY_BASE_JOINT, SMOKING_ACTIVITY_BASE_JOINT_SPECIAL, SMOKING_ACTIVITY_BASE_LIGHTER)	
					
					SET_SMOKING_ACTIVITY_STATE(SMOKING_ACTIVITY_STATE_IDLE)
				ENDIF
			ENDIF
		BREAK
		
		CASE SMOKING_ACTIVITY_STATE_WEED_IDLE
			REQUEST_CONTROL_OF_PROPS()
			
			IF REQUEST_CONTROL_OF_PROPS()
				// Choose transitions while we are idling
				iSmokingLocalScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sSmokingActivityData.iSyncSceneID)
				IF iSmokingLocalScene = -1
				OR GET_SYNCHRONIZED_SCENE_PHASE(iSmokingLocalScene) >= 0.99
					CLEAR_BIT(sSmokingActivityData.iBS, SMOKING_ACTIVITY_BS_SPARKS)
					CLEAR_BIT(sSmokingActivityData.iBS, SMOKING_ACTIVITY_BS_EXHALE_MOUTH)
					CLEAR_BIT(sSmokingActivityData.iBS, SMOKING_ACTIVITY_BS_INHALE_MOUTH)
					
					FLOAT fRandom
					fRandom = GET_RANDOM_FLOAT_IN_RANGE(0.0, 1.0)
					
					IF fRandom <= 0.35 // 35% to pick idle variation 01/02/03
						PLAY_WEED_IDLE_SCENE()	
						
					ELIF fRandom > 0.35 AND fRandom <= 0.5 // 15% to go to base with alternate pose
						PLAY_WEED_BASE_TO_BASE_POSE_SWAP_SCENE()
						
						SET_SMOKING_ACTIVITY_STATE(SMOKING_ACTIVITY_STATE_TRANSFER_BASE_POSE)
					ELIF fRandom >= 0.85 // 15% to go to base
						PLAY_SCENE(SMOKING_ACTIVITY_WEED_TO_BASE, SMOKING_ACTIVITY_WEED_TO_BASE_ASHTRAY, SMOKING_ACTIVITY_WEED_TO_BASE_JOINT, SMOKING_ACTIVITY_WEED_TO_BASE_JOINT_SPECIAL, SMOKING_ACTIVITY_WEED_TO_BASE_LIGHTER)
												
						SET_SMOKING_ACTIVITY_STATE(SMOKING_ACTIVITY_STATE_IDLE)
					ELSE	
						PLAY_SCENE(SMOKING_ACTIVITY_WEED_BASE, SMOKING_ACTIVITY_WEED_BASE_ASHTRAY, SMOKING_ACTIVITY_WEED_BASE_JOINT, SMOKING_ACTIVITY_WEED_BASE_JOINT_SPECIAL, SMOKING_ACTIVITY_WEED_BASE_LIGHTER)						
					ENDIF													
				ENDIF
																
				IF IS_NET_PLAYER_OK(PLAYER_ID())
				AND NOT IS_ANY_INTERACTION_ANIM_PLAYING()
				AND NOT IS_PAUSE_MENU_ACTIVE_EX()
				AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
				AND NOT IS_INTERACTION_MENU_OPEN()
					IF HAVE_CONTROLS_CHANGED(FRONTEND_CONTROL)
						IF sSmokingActivityData.iSmokingContextIntention != NEW_CONTEXT_INTENTION
							RELEASE_CONTEXT_INTENTION(sSmokingActivityData.iSmokingContextIntention)
							sSmokingActivityData.iSmokingContextIntention = NEW_CONTEXT_INTENTION
						ENDIF
					ENDIF
					
					IF sSmokingActivityData.iSmokingContextIntention = NEW_CONTEXT_INTENTION							
						SHOW_SMOKING_PROMPT()
													
					ELSE
						CONTROL_ACTION playerControlAction
						
						IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
							playerControlAction = INPUT_SCRIPT_RRIGHT
						ELSE
							playerControlAction = INPUT_FRONTEND_RIGHT
						ENDIF
						
						IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, playerControlAction) // EXIT									
							PLAY_SCENE(SMOKING_ACTIVITY_WEED_EXIT, SMOKING_ACTIVITY_WEED_EXIT_ASHTRAY, SMOKING_ACTIVITY_WEED_EXIT_JOINT, SMOKING_ACTIVITY_WEED_EXIT_JOINT_SPECIAL, SMOKING_ACTIVITY_WEED_EXIT_LIGHTER)
							
							RELEASE_CONTEXT_INTENTION(sSmokingActivityData.iSmokingContextIntention)
							sSmokingActivityData.iSmokingContextIntention = NEW_CONTEXT_INTENTION
							
							SET_SMOKING_ACTIVITY_STATE(SMOKING_ACTIVITY_STATE_WEED_EXITING)
						ELIF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X) // NORMAL WEED
							CLEAR_PED_FACE_PROPS()
							
							USE_WEED_FROM_WEED_BASE()
																																										
							RELEASE_CONTEXT_INTENTION(sSmokingActivityData.iSmokingContextIntention)
							sSmokingActivityData.iSmokingContextIntention = NEW_CONTEXT_INTENTION
							
							SET_SMOKING_ACTIVITY_STATE(SMOKING_ACTIVITY_STATE_SMOKING)		
						ELIF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT) // SPECIAL WEED
							USE_SPECIAL_WEED()																																		
						ENDIF
					ENDIF
				ELSE
					IF sSmokingActivityData.iSmokingContextIntention != NEW_CONTEXT_INTENTION
						RELEASE_CONTEXT_INTENTION(sSmokingActivityData.iSmokingContextIntention)
						sSmokingActivityData.iSmokingContextIntention = NEW_CONTEXT_INTENTION
					ENDIF
				ENDIF
			ENDIF
		BREAK

		CASE SMOKING_ACTIVITY_STATE_SMOKING
			REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
			
			REQUEST_CONTROL_OF_PROPS()
			
			IF REQUEST_CONTROL_OF_PROPS()
				iSmokingLocalScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sSmokingActivityData.iSyncSceneID)
				
				IF iSmokingLocalScene = -1
				OR GET_SYNCHRONIZED_SCENE_PHASE(iSmokingLocalScene) >= 0.99
					PLAY_SCENE(SMOKING_ACTIVITY_WEED_BASE, SMOKING_ACTIVITY_WEED_BASE_ASHTRAY, SMOKING_ACTIVITY_WEED_BASE_JOINT, SMOKING_ACTIVITY_WEED_BASE_JOINT_SPECIAL, SMOKING_ACTIVITY_WEED_BASE_LIGHTER)
					
					CLEAR_BIT(sSmokingActivityData.iBS, SMOKING_ACTIVITY_BS_SPARKS)
					CLEAR_BIT(sSmokingActivityData.iBS, SMOKING_ACTIVITY_BS_EXHALE_MOUTH)
					CLEAR_BIT(sSmokingActivityData.iBS, SMOKING_ACTIVITY_BS_INHALE_MOUTH)
					
					SET_SMOKING_ACTIVITY_STATE(SMOKING_ACTIVITY_STATE_EXIT_SMOKING)
				ENDIF
			ENDIF
		BREAK
		
		CASE SMOKING_ACTIVITY_STATE_EXIT_SMOKING
			REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
			
			REQUEST_CONTROL_OF_PROPS()
			
			IF REQUEST_CONTROL_OF_PROPS()
				iSmokingLocalScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sSmokingActivityData.iSyncSceneID)
				
				IF iSmokingLocalScene = -1
				OR GET_SYNCHRONIZED_SCENE_PHASE(iSmokingLocalScene) >= 0.99	
					PLAY_SCENE(SMOKING_ACTIVITY_WEED_BASE, SMOKING_ACTIVITY_WEED_BASE_ASHTRAY, SMOKING_ACTIVITY_WEED_BASE_JOINT, SMOKING_ACTIVITY_WEED_BASE_JOINT_SPECIAL, SMOKING_ACTIVITY_WEED_BASE_LIGHTER)
					
					CLEAR_BIT(sSmokingActivityData.iBS, SMOKING_ACTIVITY_BS_SPARKS)
					CLEAR_BIT(sSmokingActivityData.iBS, SMOKING_ACTIVITY_BS_EXHALE_MOUTH)
					CLEAR_BIT(sSmokingActivityData.iBS, SMOKING_ACTIVITY_BS_INHALE_MOUTH)
					
					RESET_PED_FACE_PROPS()
					
					sSmokingActivityData.iBS = 0
					
					SET_SMOKING_ACTIVITY_STATE(SMOKING_ACTIVITY_STATE_WEED_IDLE)
				ENDIF
			ENDIF
		BREAK
			
		CASE SMOKING_ACTIVITY_STATE_LAMAR_STRAIN
			REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
		
			REQUEST_CONTROL_OF_PROPS()
			
			IF REQUEST_CONTROL_OF_PROPS()
				iSmokingLocalScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sSmokingActivityData.iSyncSceneID)
			
				IF iSmokingLocalScene = -1
				OR GET_SYNCHRONIZED_SCENE_PHASE(iSmokingLocalScene) >= 0.99												
					PLAY_SCENE(SMOKING_ACTIVITY_LAMAR_STRAIN, SMOKING_ACTIVITY_LAMAR_STRAIN_ASHTRAY, SMOKING_ACTIVITY_LAMAR_STRAIN_JOINT, SMOKING_ACTIVITY_LAMAR_STRAIN_JOINT_SPECIAL, SMOKING_ACTIVITY_LAMAR_STRAIN_LIGHTER)
					
					CLEAR_BIT(sSmokingActivityData.iBS, SMOKING_ACTIVITY_BS_SPARKS)
					CLEAR_BIT(sSmokingActivityData.iBS, SMOKING_ACTIVITY_BS_EXHALE_MOUTH)
					CLEAR_BIT(sSmokingActivityData.iBS, SMOKING_ACTIVITY_BS_INHALE_MOUTH)
					
					SET_SMOKING_ACTIVITY_STATE(SMOKING_ACTIVITY_STATE_LAMAR_STRAIN_BASE)
				ENDIF
			ENDIF
		BREAK
		
		CASE SMOKING_ACTIVITY_STATE_LAMAR_STRAIN_BASE
			REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
		
			REQUEST_CONTROL_OF_PROPS()
			
			IF REQUEST_CONTROL_OF_PROPS()
				iSmokingLocalScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sSmokingActivityData.iSyncSceneID)
			
				IF iSmokingLocalScene = -1
				OR GET_SYNCHRONIZED_SCENE_PHASE(iSmokingLocalScene) >= 0.99												
					PLAY_SCENE(SMOKING_ACTIVITY_LAMAR_STRAIN_BASE, SMOKING_ACTIVITY_LAMAR_STRAIN_BASE_ASHTRAY, SMOKING_ACTIVITY_LAMAR_STRAIN_BASE_JOINT, SMOKING_ACTIVITY_LAMAR_STRAIN_BASE_JOINT_SPECIAL, SMOKING_ACTIVITY_LAMAR_STRAIN_BASE_LIGHTER)															
					
					CLEAR_BIT(sSmokingActivityData.iBS, SMOKING_ACTIVITY_BS_SPARKS)
					CLEAR_BIT(sSmokingActivityData.iBS, SMOKING_ACTIVITY_BS_EXHALE_MOUTH)
					CLEAR_BIT(sSmokingActivityData.iBS, SMOKING_ACTIVITY_BS_INHALE_MOUTH)
				ENDIF
			ENDIF
		BREAK
		
		CASE SMOKING_ACTIVITY_STATE_EXITING
			iSmokingLocalScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sSmokingActivityData.iSyncSceneID)
			
			IF iSmokingLocalScene = -1
			OR GET_SYNCHRONIZED_SCENE_PHASE(iSmokingLocalScene) >= 0.99
				CLEANUP_SMOKING_ACTIVITY()
			ENDIF
		BREAK
		
		CASE SMOKING_ACTIVITY_STATE_WEED_EXITING
			iSmokingLocalScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sSmokingActivityData.iSyncSceneID)
			
			IF iSmokingLocalScene = -1
			OR GET_SYNCHRONIZED_SCENE_PHASE(iSmokingLocalScene) >= 0.99
				CLEANUP_SMOKING_ACTIVITY()
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC CREATE_SMOKING_ACTIVITY_PROPS()
	INT i
	REPEAT sSmokingActivityData.iActiveSmokingActivities i
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.objLighter[i])
			MODEL_NAMES eLighterModel = GET_LIGHTER_PROP_NAME()
		
			REQUEST_MODEL(eLighterModel)
		
			IF NOT HAS_MODEL_LOADED(eLighterModel)
				EXIT
			ENDIF
			
			REQUEST_ANIM_DICT(GET_SMOKING_ACTIVITY_ANIM_DICT())
			
			IF NOT HAS_ANIM_DICT_LOADED(GET_SMOKING_ACTIVITY_ANIM_DICT())
				EXIT
			ENDIF
			
			OBJECT_INDEX objTempLighter
			
			IF CAN_REGISTER_MISSION_OBJECTS(1)
				VECTOR vLighterPosition, vLighterRotation
				
				vLighterPosition = GET_PROP_POSITION_LIGHTER(i)
				vLighterRotation = GET_PROP_ROTATION_LIGHTER(i)			
			
				objTempLighter = CREATE_OBJECT(eLighterModel, vLighterPosition)
				
				SET_MODEL_AS_NO_LONGER_NEEDED(eLighterModel)
				SET_ENTITY_CAN_BE_DAMAGED(objTempLighter, FALSE)
				NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(objTempLighter, TRUE)
				SET_ENTITY_CAN_ONLY_BE_DAMAGED_BY_SCRIPT_PARTICIPANTS(objTempLighter, TRUE)
				SET_ENTITY_COORDS_NO_OFFSET(objTempLighter, vLighterPosition)
				SET_ENTITY_ROTATION(objTempLighter, vLighterRotation)
				FREEZE_ENTITY_POSITION(objTempLighter, TRUE)
				SET_ENTITY_COLLISION(objTempLighter, FALSE)
				
				serverBD.objLighter[i] = OBJ_TO_NET(objTempLighter)
				
				REMOVE_ANIM_DICT(GET_SMOKING_ACTIVITY_ANIM_DICT())
			ENDIF				
		ENDIF
						
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.objSmoking[i])			
			MODEL_NAMES eSmokingModel = GET_SMOKING_PROP_NAME()

			REQUEST_MODEL(eSmokingModel)
			
			IF NOT HAS_MODEL_LOADED(eSmokingModel)
				EXIT
			ENDIF
			
			REQUEST_ANIM_DICT(GET_SMOKING_ACTIVITY_ANIM_DICT())
			
			IF NOT HAS_ANIM_DICT_LOADED(GET_SMOKING_ACTIVITY_ANIM_DICT())
				EXIT
			ENDIF
			
			OBJECT_INDEX objTempCig
			
			IF CAN_REGISTER_MISSION_OBJECTS(1)
				VECTOR vSmokingPosition, vSmokingRotation
				
				vSmokingPosition = GET_PROP_POSITION_JOINT(i)
				vSmokingRotation = GET_PROP_ROTATION_JOINT(i)
												
				objTempCig = CREATE_OBJECT(eSmokingModel, vSmokingPosition)
				
				SET_MODEL_AS_NO_LONGER_NEEDED(eSmokingModel)
				SET_ENTITY_CAN_BE_DAMAGED(objTempCig, FALSE)
				NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(objTempCig, TRUE)
				SET_ENTITY_CAN_ONLY_BE_DAMAGED_BY_SCRIPT_PARTICIPANTS(objTempCig, TRUE)
				SET_ENTITY_COORDS_NO_OFFSET(objTempCig, vSmokingPosition)
				SET_ENTITY_ROTATION(objTempCig, vSmokingRotation)
				FREEZE_ENTITY_POSITION(objTempCig, TRUE)
				SET_ENTITY_COLLISION(objTempCig, FALSE)
					
				serverBD.objSmoking[i] = OBJ_TO_NET(objTempCig)
				
				REMOVE_ANIM_DICT(GET_SMOKING_ACTIVITY_ANIM_DICT())
			ENDIF
		ENDIF
		
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.objSmokingSpecial[i])			
			MODEL_NAMES eSmokingModel = GET_SMOKING_PROP_NAME()

			REQUEST_MODEL(eSmokingModel)
			
			IF NOT HAS_MODEL_LOADED(eSmokingModel)
				EXIT
			ENDIF
			
			REQUEST_ANIM_DICT(GET_SMOKING_ACTIVITY_ANIM_DICT())
			
			IF NOT HAS_ANIM_DICT_LOADED(GET_SMOKING_ACTIVITY_ANIM_DICT())
				EXIT
			ENDIF
			
			OBJECT_INDEX objTempCigSpecial
			
			IF CAN_REGISTER_MISSION_OBJECTS(1)
				VECTOR vSmokingPosition, vSmokingRotation
				
				vSmokingPosition = GET_PROP_POSITION_JOINT_SPECIAL(i)
				vSmokingRotation = GET_PROP_ROTATION_JOINT_SPECIAL(i)
					
				objTempCigSpecial = CREATE_OBJECT(eSmokingModel, vSmokingPosition)
				
				SET_MODEL_AS_NO_LONGER_NEEDED(eSmokingModel)
				SET_ENTITY_CAN_BE_DAMAGED(objTempCigSpecial, FALSE)
				NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(objTempCigSpecial, TRUE)
				SET_ENTITY_CAN_ONLY_BE_DAMAGED_BY_SCRIPT_PARTICIPANTS(objTempCigSpecial, TRUE)
				SET_ENTITY_COORDS_NO_OFFSET(objTempCigSpecial, vSmokingPosition)
				SET_ENTITY_ROTATION(objTempCigSpecial, vSmokingRotation)
				FREEZE_ENTITY_POSITION(objTempCigSpecial, TRUE)
				SET_ENTITY_COLLISION(objTempCigSpecial, FALSE)
				
				serverBD.objSmokingSpecial[i] = OBJ_TO_NET(objTempCigSpecial)
				
				REMOVE_ANIM_DICT(GET_SMOKING_ACTIVITY_ANIM_DICT())
			ENDIF
		ENDIF
						
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.objAshtray[i])
			MODEL_NAMES eAshtrayModel = GET_ASHTRAY_PROP_NAME()
		
			REQUEST_MODEL(eAshtrayModel)
			
			IF NOT HAS_MODEL_LOADED(eAshtrayModel)
				EXIT
			ENDIF
			
			REQUEST_ANIM_DICT(GET_SMOKING_ACTIVITY_ANIM_DICT())
			
			IF NOT HAS_ANIM_DICT_LOADED(GET_SMOKING_ACTIVITY_ANIM_DICT())
				EXIT
			ENDIF
			
			OBJECT_INDEX objTempAshtray
			
			IF CAN_REGISTER_MISSION_OBJECTS(1)
				VECTOR vAshtrayPosition, vAshtrayRotation
								
				vAshtrayPosition = GET_PROP_POSITION_ASHTRAY(i)
				vAshtrayRotation = GET_PROP_ROTATION_ASHTRAY(i)
										
				objTempAshtray = CREATE_OBJECT(eAshtrayModel, vAshtrayPosition)
				
				SET_MODEL_AS_NO_LONGER_NEEDED(eAshtrayModel)
				SET_ENTITY_CAN_BE_DAMAGED(objTempAshtray, FALSE)
				NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(objTempAshtray, TRUE)
				SET_ENTITY_CAN_ONLY_BE_DAMAGED_BY_SCRIPT_PARTICIPANTS(objTempAshtray, TRUE)
				SET_ENTITY_COORDS_NO_OFFSET(objTempAshtray, vAshtrayPosition)
				SET_ENTITY_ROTATION(objTempAshtray, vAshtrayRotation)
				FREEZE_ENTITY_POSITION(objTempAshtray, TRUE)
				
				serverBD.objAshtray[i] = OBJ_TO_NET(objTempAshtray)
				
				REMOVE_ANIM_DICT(GET_SMOKING_ACTIVITY_ANIM_DICT())
			ENDIF
		ENDIF				
	ENDREPEAT
ENDPROC

PROC MAINTAIN_HELP_TEXT()

	IF NOT IS_LOCAL_PLAYER_IN_MUSIC_STUDIO_SMOKING_ROOM()		
		EXIT
	ENDIF
	
	IF NOT HAS_PLAYER_UNLOCKED_FIXER_SHORT_TRIPS(PLAYER_ID())
		EXIT
	ENDIF
	
		//WE don't need to continue looping through players when the help text is already beling displayed
	IF IS_BIT_SET(g_SimpleInteriorData.iEleventhBS, BS11_SIMPLE_INTERIOR_SPECIAL_STRAIN_HELP_TEXT_1)
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SS_HELP_JOIN")
			EXIT
		ELSE
			PRINTLN("[FIX_FOR_7564967] SS_HELP_JOIN is no longer being displayed. Resuming checks")
			CLEAR_BIT(g_SimpleInteriorData.iEleventhBS, BS11_SIMPLE_INTERIOR_SPECIAL_STRAIN_HELP_TEXT_1)
		ENDIF
	ENDIF	
	
	IF IS_BIT_SET(g_SimpleInteriorData.iEleventhBS, BS11_SIMPLE_INTERIOR_SPECIAL_STRAIN_HELP_TEXT_2)
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SS_HELP_SIT")				
			EXIT
		ELSE	
			PRINTLN("[FIX_FOR_7564967] SS_HELP_SIT is no longer being displayed. Resuming checks")
			CLEAR_BIT(g_SimpleInteriorData.iEleventhBS, BS11_SIMPLE_INTERIOR_SPECIAL_STRAIN_HELP_TEXT_2)
			SET_BIT(sSmokingActivityData.iBS, SMOKING_ACTIVITY_BS_TICKER_STARTING_SS)
		ENDIF
	ENDIF
	
	IF (IS_BIT_SET(g_SimpleInteriorData.iEleventhBS, BS11_SIMPLE_INTERIOR_SPECIAL_STRAIN_HELP_TEXT_1)
	AND IS_BIT_SET(g_SimpleInteriorData.iEleventhBS, BS11_SIMPLE_INTERIOR_SPECIAL_STRAIN_HELP_TEXT_2)
	AND IS_BIT_SET(sSmokingActivityData.iBS, SMOKING_ACTIVITY_BS_TICKER_STARTING_SS))
	OR (IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FXR_COUCH_HELP"))
		EXIT
	ENDIF

	IF IS_PLAYER_IN_FIXER_SPECIAL_STRAIN_CHAIR(PLAYER_ID())
	AND HAS_PLAYER_UNLOCKED_FIXER_SHORT_TRIPS(PLAYER_ID())
	AND HAS_PLAYER_COMPLETED_FIXER_SHORT_TRIP(PLAYER_ID(), FST_SHORT_TRIP_1)
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MUSIC_STUDIO_DISPLAYED_SMOKING_COUCH_HELP)
			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FXR_COUCH_HELP")
				RELEASE_CONTEXT_INTENTION(sSmokingActivityData.iSmokingContextIntention)
				PRINT_HELP("FXR_COUCH_HELP", DEFAULT_HELP_TEXT_TIME)	// Sitting on the couch ~BLIP_PICKUP_WEED~ allows you to smoke weed or take a hit of Lamar's Special Strain to enjoy a Short Trip with another player.
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MUSIC_STUDIO_DISPLAYED_SMOKING_COUCH_HELP, TRUE)	
				EXIT
			ENDIF
		ENDIF
	ENDIF
	
	
	INT iPlayer
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		PLAYER_INDEX playerToCheck = INT_TO_PLAYERINDEX(iPlayer)
		
		IF playerToCheck != PLAYER_ID()
		AND	NETWORK_IS_PLAYER_ACTIVE(playerToCheck)
		AND IS_NET_PLAYER_OK(playerToCheck, FALSE, FALSE)	
			IF ARE_PLAYERS_IN_SAME_SIMPLE_INTERIOR(PLAYER_ID(), playerToCheck, TRUE)	
			
				IF IS_PLAYER_IN_FIXER_SPECIAL_STRAIN_CHAIR(playerToCheck)
				AND IS_PLAYER_SMOKING_FIXER_SPECIAL_STRAIN(playerToCheck)		
				
					IF IS_LOCAL_PLAYER_IN_FIXER_SPECIAL_STRAIN_CHAIR()							
						PRINTLN("[MUSIC_STUDIO][SHORT_TRIPS] A remote player is launching short trips, player is seated, displaying SS_HELP_JOIN (",GET_PLAYER_NAME(playerToCheck),")")
						IF NOT IS_BIT_SET(g_SimpleInteriorData.iEleventhBS, BS11_SIMPLE_INTERIOR_SPECIAL_STRAIN_HELP_TEXT_1)
						AND NOT IS_THIS_HELP_MESSAGE_WITH_PLAYER_NAME_BEING_DISPLAYED("SS_HELP_JOIN", GET_PLAYER_NAME(playerToCheck))	// ~a~ has smoked Lamar's Special Strain to start a Short Trip. Remain seated on the couch to join them.											
							RELEASE_CONTEXT_INTENTION(sSmokingActivityData.iSmokingContextIntention)
							PRINT_HELP_WITH_PLAYER_NAME("SS_HELP_JOIN", GET_PLAYER_NAME(playerToCheck), HUD_COLOUR_WHITE, DEFAULT_HELP_TEXT_TIME)	
							SET_BIT(g_SimpleInteriorData.iEleventhBS, BS11_SIMPLE_INTERIOR_SPECIAL_STRAIN_HELP_TEXT_1)
						ENDIF								
					ELSE		
						PRINTLN("[MUSIC_STUDIO][SHORT_TRIPS] A remote player is launching short trips, player is not seated, displaying SS_HELP_SIT, and ticker (",GET_PLAYER_NAME(playerToCheck),")")
						IF NOT IS_BIT_SET(g_SimpleInteriorData.iEleventhBS, BS11_SIMPLE_INTERIOR_SPECIAL_STRAIN_HELP_TEXT_2)
						AND NOT IS_THIS_HELP_MESSAGE_WITH_PLAYER_NAME_BEING_DISPLAYED("	", GET_PLAYER_NAME(playerToCheck))	// ~a~ has smoked Lamar's Special Strain to start a Short Trip. Sit down on the couch ~BLIP_PICKUP_WEED~ to join them.		
							PRINT_HELP_WITH_PLAYER_NAME("SS_HELP_SIT", GET_PLAYER_NAME(playerToCheck), HUD_COLOUR_WHITE, DEFAULT_HELP_TEXT_TIME)	
							SET_BIT(g_SimpleInteriorData.iEleventhBS, BS11_SIMPLE_INTERIOR_SPECIAL_STRAIN_HELP_TEXT_2)
						ENDIF
						
						IF NOT IS_BIT_SET(sSmokingActivityData.iBS, SMOKING_ACTIVITY_BS_TICKER_STARTING_SS)
							PRINT_TICKER_WITH_PLAYER_NAME("SS_HELP_START", playerToCheck, FALSE, TRUE)		// ~a~ is starting a Short Trip.
							SET_BIT(sSmokingActivityData.iBS, SMOKING_ACTIVITY_BS_TICKER_STARTING_SS)
						ENDIF	
					ENDIF	
					
					BREAKLOOP //IF ONE OF THE PLAYERS IS LAUNCHING A SHORT TRIP AND WE DISPLAYED HELP TEXT, WE DON'T NEED TO CONTINUE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT			
			
ENDPROC

FUNC VECTOR GET_COLLIDER_POSITION(INT iPosition)
	SWITCH iPosition
		CASE 0
			RETURN << -1006.982, -79.003, -100.022 >>
		BREAK
		CASE 1
			RETURN << -1006.982, -77.953, -100.022 >>
		BREAK
	ENDSWITCH
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC BOOL IS_PED_ON_SOFA(PED_INDEX piPed, INT iPosition)
	SWITCH iPosition
		CASE 0
			RETURN IS_ENTITY_IN_ANGLED_AREA(piPed, <<-1007.372925,-78.964638,-100.003067>>, <<-1008.065918,-79.018974,-98.003067>>, 0.800000)
		BREAK
		CASE 1
			RETURN IS_ENTITY_IN_ANGLED_AREA(piPed, <<-1008.202393,-78.158058,-100.003067>>, <<-1007.362671,-78.125282,-98.003067>>, 0.800000)
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Loop through each sofa position, then each participant and check if any are sitting on the sofa. If true then create an invisible collider, and delete if the position is unoccupied
PROC MAINTAIN_INVISIBLE_COLLIDERS()
	MODEL_NAMES modelName = INT_TO_ENUM(MODEL_NAMES, (GET_HASH_KEY("prop_bin_01a")))
	BOOL bDeleteCollider = TRUE

	INT iSofaPosition
	REPEAT MAX_HIDDEN_SCENE_OBJECTS iSofaPosition

		PLAYER_INDEX playerToCheck
		INT iPlayerIndex
		REPEAT NETWORK_GET_NUM_PARTICIPANTS() iPlayerIndex
			IF iPlayerIndex != -1
			AND	NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPlayerIndex))
				playerToCheck = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPlayerIndex))
											
				IF IS_NET_PLAYER_OK(playerToCheck)
				AND playerToCheck != PLAYER_ID()

					PED_INDEX piPed = GET_PLAYER_PED(playerToCheck)
																		
					IF IS_PED_ON_SOFA(piPed, iSofaPosition)
						bDeleteCollider = FALSE
						
						IF NOT DOES_ENTITY_EXIST(sSmokingActivityData.objCollisions[iSofaPosition])
							IF REQUEST_LOAD_MODEL(modelName)
								sSmokingActivityData.objCollisions[iSofaPosition] = CREATE_OBJECT(modelName, GET_COLLIDER_POSITION(iSofaPosition), FALSE, FALSE)
								
								FREEZE_ENTITY_POSITION(sSmokingActivityData.objCollisions[iSofaPosition], TRUE)
								SET_ENTITY_CAN_BE_DAMAGED(sSmokingActivityData.objCollisions[iSofaPosition], FALSE)
								SET_ENTITY_VISIBLE(sSmokingActivityData.objCollisions[iSofaPosition], FALSE)
								SET_ENTITY_PROOFS(sSmokingActivityData.objCollisions[iSofaPosition], TRUE, TRUE, TRUE, FALSE, TRUE, TRUE, TRUE, TRUE)
								SET_MODEL_AS_NO_LONGER_NEEDED(modelName)
								PRINTLN("[MUSIC_STUDIO_SMOKING][MAINTAIN_INVISIBLE_COLLIDERS] creating collision for position: ", iSofaPosition)								
							ELSE
								PRINTLN("[MUSIC_STUDIO_SMOKING][MAINTAIN_INVISIBLE_COLLIDERS] model not loaded yet")
							ENDIF
						ENDIF												
					ENDIF	
					
				ENDIF
			ENDIF
					
		ENDREPEAT
				
		IF DOES_ENTITY_EXIST(sSmokingActivityData.objCollisions[iSofaPosition])
		AND bDeleteCollider
			DELETE_OBJECT(sSmokingActivityData.objCollisions[iSofaPosition])			
		ENDIF
														
	ENDREPEAT						
ENDPROC

SCRIPT
	IF NETWORK_IS_GAME_IN_PROGRESS()
		PROCESS_PRE_GAME()
	ELSE
		SCRIPT_CLEANUP()
	ENDIF
	
	WHILE TRUE
		WAIT(0)
		
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		OR NOT IS_PLAYER_IN_SMOKING_ROOM()
			SCRIPT_CLEANUP()
		ENDIF
		
		MAINTAIN_SMOKING_ACTIVITY()
		MAINTAIN_INVISIBLE_COLLIDERS()
		MAINTAIN_HELP_TEXT()
		
		IF IS_SCREEN_FADED_OUT()
			SCRIPT_CLEANUP()
		ENDIF
		
		IF NETWORK_IS_GAME_IN_PROGRESS()
		AND NETWORK_IS_HOST_OF_THIS_SCRIPT()
			CREATE_SMOKING_ACTIVITY_PROPS()
		ENDIF
						
		IF NOT IS_PLAYER_IN_MUSIC_STUDIO(PLAYER_ID())
		AND NOT IS_LOCAL_PLAYER_IN_MUSIC_STUDIO_SMOKING_ROOM()
			SCRIPT_CLEANUP()
		ENDIF

	ENDWHILE
ENDSCRIPT

#ENDIF
