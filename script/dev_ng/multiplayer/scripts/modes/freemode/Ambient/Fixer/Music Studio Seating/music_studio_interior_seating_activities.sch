USING "context_control_public.sch"
USING "website_public.sch"
USING "freemode_header.sch"
USING "script_maths.sch"

CONST_INT MAX_NUM_SEATS_FOR_SITTING_ACTIVITY 31

CONST_INT LOCATE_CHECKS_PER_TICK 5

//Time before a new animation variation is chosen, this should be infrequent.
CONST_INT TIME_BEFORE_ANIM_VAR_CHANGE 90000

//There is a brief period of time while the player isn't allowed to sit down
//that is triggered every time the seat is blocked.
CONST_INT SEAT_BLOCK_TIMER			  		150

CONST_INT CAPSULE_SHRINK_LINGER_NUM_FRAMES	5

TWEAK_FLOAT SITTING_CAPSULE_SIZE 			0.13

ENUM SEAT_TYPE
	SEAT_INVALID
	,SEAT_TYPE_OFFICE_ANIM
	,SEAT_TYPE_CLUBHOUSE_ANIM
	,SEAT_TYPE_BASE_ANIM
	,SEAT_TYPE_JACUZZI_ANIM
ENDENUM

ENUM SEAT_STATE
	SS_INIT
	,SS_LOCATE_CHECK
	,SS_PROMPT
	,SS_REQUEST_ANIMATION
	,SS_MOVE_TO_ANIM_START
	,SS_ANIM_START_ENTER
	,SS_ANIM_SITTING
	,SS_ANIM_INTERUPT
	,SS_CLEANUP
ENDENUM

STRUCT LOCATE_STRUCT
	FLOAT fWidth = 0.75
	
	VECTOR v0
	VECTOR v1
ENDSTRUCT

STRUCT ANIM_STRUCT
	VECTOR vPos
	VECTOR vRot
ENDSTRUCT

//Randomly selected when player sits down. Specific selection for various chairs.
ENUM SEAT_ANIM_0
	ANIM_AMB_CLUBHOUSE_SEATING
	,ANIM_AMB_OFFICE_SEATING
	,ANIM_AMB_FACILITY_BRIEFING_ROOM_SEATING
	,ANIM_AMB_JACUZZI
ENDENUM

//Randomly selected when player sits down, changes very infreuqently.
ENUM SEAT_ANIM_1
	/* Offfice, Clubhouse, and briefing room variations */
	VAR_A
	,VAR_B
	,VAR_C
	,VAR_D
	,VAR_E
ENDENUM

//Clip is Enter, then ping pongs between Base <-> Idle/Transition then Exit
ENUM SEAT_ANIM_2_CLIP
	/* Clips */
	IDLE_A
	,IDLE_B
	,IDLE_C
	,ENTER
	,LEAVE
	,BASE
	/* This is not a clip. Use as an offset for the transitions below. */
	,TRANSITIONS
	,A_TO_B
	,B_TO_C
	,C_TO_D
	,D_TO_E
	/* This is not a clip. Use as an offset for the transitions below. */
	,TRANSITION_LOOPS
	,B_TO_A	//Unused, but here to maintain consistency in offsets.. Please do not remove.
	,C_TO_A
	,D_TO_A
	,E_TO_A
ENDENUM

STRUCT ANIM_STATE_STRUCT
	INT iVarChangeTime
	
	SEAT_ANIM_0 eDictBase
	SEAT_ANIM_1 eDictVar
	
	SEAT_ANIM_2_CLIP eClip
	SEAT_ANIM_2_CLIP ePrevClip
ENDSTRUCT

STRUCT SEAT_DATA_STRUCT
	ANIM_STRUCT animation
	
	LOCATE_STRUCT entryLocate
	
	SEAT_TYPE eType
ENDSTRUCT

TYPEDEF FUNC BOOL ADDITIONAL_CHECK_BEFORE_PROMPT(SEAT_DATA_STRUCT& in_data, INT iSeatIndex)

TYPEDEF FUNC BOOL USE_ALTERNATIVE_HELP_TEXT(SEAT_DATA_STRUCT& in_data, INT iSeatIndex, TEXT_LABEL_15& out_txtHelp)

STRUCT SEATS_PRIVATE_DATA
	ADDITIONAL_CHECK_BEFORE_PROMPT cCanPlayerUseSeat
	
	ANIM_STATE_STRUCT animationState
	
	INT iContext = NEW_CONTEXT_INTENTION
	INT iBSGeneral = 0
	INT iNetSceneID = -1
	INT iInSeatLocate = 0
	INT iPlayerToCheck = 0
	INT iSeatBlockedTimer = -1
	INT iFrameLastCapsuleChange = -1
	
	SEAT_STATE eState = SS_INIT
	
	USE_ALTERNATIVE_HELP_TEXT cAltHelpExit
	USE_ALTERNATIVE_HELP_TEXT cAltHelpEnter
ENDSTRUCT

#IF IS_DEBUG_BUILD
STRUCT SEATS_DEBUG_DATA
	BOOL bForceAnimVarNow
	
	INT iNextForcedAnimVar
ENDSTRUCT
#ENDIF

STRUCT SEATS_PUBLIC_DATA
	SEAT_DATA_STRUCT seats[MAX_NUM_SEATS_FOR_SITTING_ACTIVITY]
ENDSTRUCT

STRUCT SEATS_LOCAL_DATA
	SEATS_PRIVATE_DATA private
	
	SEATS_PUBLIC_DATA public
	
	#IF IS_DEBUG_BUILD
	SEATS_DEBUG_DATA debug
	#ENDIF
ENDSTRUCT

ENUM PRIVATE_SEAT_GENERAL_BS
	PSGB_BUSY_ON_PHONE
	,PSGB_BUSY_DEAD
	,PSGB_BUSY_RUNNING
	,PSGB_BUSY_IN_VEHICLE
	,PSGB_BUSY_BROWSER_OPEN
	,PSGB_ADDITIONAL_CHECK_BEFORE_PROMPT
	,PSGB_ALT_HELP_ENTER
	,PSGB_ALT_HELP_EXIT
	,PSGB_WAITING_FOR_NEXT_DICTIONARY
	,PSGB_BLOCK_EXIT_THIS_FRAME
	,PSGB_GET_OUT_OF_SEAT
	,PSGB_ENTRY_COMPLETE
	,PSGB_BLOCK_MANUAL_VAR_CHANGE
	,PSGB_BLOCK_FIRST_PERSON
ENDENUM

/*PSGB_BUSY_ON_PHONE || PSGB_BUSY_DEAD || PSGB_BUSY_RUNNING || PSGB_BUSY_IN_VEHICLE || PSGB_BUSY_BROWSER_OPEN*/
CONST_INT BUSY_MASK  1 + 2 + 4 + 8 + 16

PROC LOG_MAX_NUM_SEATS()
	PRINTLN("[MUSIC_STUDIO_INT_SEATS] : MAX_NUM_SEATS_FOR_SITTING_ACTIVITY = ", MAX_NUM_SEATS_FOR_SITTING_ACTIVITY)
ENDPROC

PROC SET_BIT_VALUE_ENUM(INT& iBS, ENUM_TO_INT eBit, BOOL bValue)
	INT iBit = eBit
	
	IF bValue
		SET_BIT(iBS, iBit)
	ELSE
		CLEAR_BIT(iBS, iBit)
	ENDIF
ENDPROC

FUNC BOOL IS_ENTITY_IN_SEAT_ENTRY_LOCATE(ENTITY_INDEX entity, SEAT_DATA_STRUCT& seat)
	RETURN IS_ENTITY_IN_ANGLED_AREA(entity, seat.entryLocate.v0, seat.entryLocate.v1, seat.entryLocate.fWidth)
ENDFUNC

FUNC BOOL IS_ENTITY_FACING_SEAT(ENTITY_INDEX entity, VECTOR vRotation, SEAT_TYPE eSeatType)
	IF DOES_ENTITY_EXIST(entity)
		IF eSeatType = SEAT_TYPE_BASE_ANIM
		OR eSeatType = SEAT_TYPE_JACUZZI_ANIM
			IF IS_HEADING_ACCEPTABLE_CORRECTED(GET_ENTITY_HEADING(entity), vRotation.z - 180.0, 45.0)
				RETURN TRUE
			ENDIF
	
		ELIF IS_HEADING_ACCEPTABLE_CORRECTED(GET_ENTITY_HEADING(entity), vRotation.z, 45.0)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL STAGGERED_UPDATE_LOCATE_CHECK(SEATS_LOCAL_DATA& data)
	INT i
	REPEAT LOCATE_CHECKS_PER_TICK i
		IF IS_ENTITY_IN_SEAT_ENTRY_LOCATE(PLAYER_PED_ID(), data.public.seats[data.private.iInSeatLocate])
		AND IS_ENTITY_FACING_SEAT(PLAYER_PED_ID(), data.public.seats[data.private.iInSeatLocate].animation.vRot, data.public.seats[data.private.iInSeatLocate].eType)
			RETURN TRUE
		ELSE
			data.private.iInSeatLocate = (data.private.iInSeatLocate + 1) % MAX_NUM_SEATS_FOR_SITTING_ACTIVITY
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

FUNC SEAT_ANIM_0 RANDOM_ANIM_S0(SEAT_TYPE eType)
	SWITCH eType
		CASE SEAT_TYPE_OFFICE_ANIM		RETURN ANIM_AMB_OFFICE_SEATING
		CASE SEAT_TYPE_CLUBHOUSE_ANIM	RETURN ANIM_AMB_CLUBHOUSE_SEATING
		CASE SEAT_TYPE_BASE_ANIM		RETURN ANIM_AMB_FACILITY_BRIEFING_ROOM_SEATING
		CASE SEAT_TYPE_JACUZZI_ANIM		RETURN ANIM_AMB_JACUZZI
	ENDSWITCH
	
	RETURN ANIM_AMB_CLUBHOUSE_SEATING
ENDFUNC

FUNC INT GET_ANIM_S1_VARIATIONS(SEAT_ANIM_0 eBase)
	SWITCH eBase
		CASE ANIM_AMB_CLUBHOUSE_SEATING 				RETURN 3
		CASE ANIM_AMB_OFFICE_SEATING 					RETURN PICK_INT(IS_PLAYER_FEMALE(), 4, 5)
		CASE ANIM_AMB_FACILITY_BRIEFING_ROOM_SEATING 	RETURN 5
		CASE ANIM_AMB_JACUZZI							RETURN 3
	ENDSWITCH
	
	RETURN 0
ENDFUNC

FUNC VECTOR GET_ANIM_ROT(SEATS_LOCAL_DATA& data)
	VECTOR vRot = data.public.seats[data.private.iInSeatLocate].animation.vRot
	
	IF IS_PLAYER_FEMALE()
	AND data.private.animationState.eDictBase = ANIM_AMB_CLUBHOUSE_SEATING
		vRot.z -= 4
	ENDIF
	
	RETURN vRot
ENDFUNC

FUNC VECTOR GET_ANIM_POS(SEATS_LOCAL_DATA& data)
	RETURN data.public.seats[data.private.iInSeatLocate].animation.vPos
ENDFUNC

FUNC SEAT_ANIM_1 RANDOM_ANIM_S1(SEATS_LOCAL_DATA& data)
	INT iRand = GET_RANDOM_INT_IN_RANGE(0, GET_ANIM_S1_VARIATIONS(data.private.animationState.eDictBase))
	
	IF data.private.animationState.eDictBase = ANIM_AMB_OFFICE_SEATING
		IF IS_PLAYER_FEMALE()
			IF INT_TO_ENUM(SEAT_ANIM_1, iRand) = VAR_A
			OR INT_TO_ENUM(SEAT_ANIM_1, iRand) = VAR_E
				iRand = ENUM_TO_INT(VAR_B)
			ENDIF
		ELSE
			IF INT_TO_ENUM(SEAT_ANIM_1, iRand) = VAR_B
			OR INT_TO_ENUM(SEAT_ANIM_1, iRand) = VAR_C
				iRand = ENUM_TO_INT(VAR_D)
			ENDIF
		ENDIF
	ENDIF
	
	IF data.private.animationState.eDictBase = ANIM_AMB_JACUZZI
		IF data.private.iInSeatLocate >= 118
		AND data.private.iInSeatLocate <= 122
			iRand = 2
		ENDIF
	ENDIF
	
	IF data.private.animationState.eDictBase = ANIM_AMB_FACILITY_BRIEFING_ROOM_SEATING
		iRand = GET_RANDOM_INT_IN_RANGE(0, 4)
		
		IF iRand >= 1
			iRand++
		ENDIF
	ENDIF
	
	RETURN INT_TO_ENUM(SEAT_ANIM_1, iRand)
ENDFUNC

PROC SET_START_ANIMATION(SEATS_LOCAL_DATA& data)
	data.private.animationState.eDictBase = RANDOM_ANIM_S0(data.public.seats[data.private.iInSeatLocate].eType)
	data.private.animationState.eDictVar = RANDOM_ANIM_S1(data)
	data.private.animationState.eClip = ENTER
	data.private.animationState.ePrevClip = ENTER
	
	PRINTLN("[MUSIC_STUDIO_INT_SEATS][ANIM] Start base : ", ENUM_TO_INT(data.private.animationState.eDictBase))
	PRINTLN("[MUSIC_STUDIO_INT_SEATS][ANIM] Start var : ", ENUM_TO_INT(data.private.animationState.eDictVar))
	PRINTLN("[MUSIC_STUDIO_INT_SEATS][ANIM] Start clip : ", ENUM_TO_INT(data.private.animationState.eClip))
ENDPROC

PROC CONSTRUCT_ANIM_CLIP(SEATS_LOCAL_DATA& data, TEXT_LABEL_15& sClip)
	IF data.private.animationState.eDictBase = ANIM_AMB_OFFICE_SEATING
		IF IS_PLAYER_FEMALE()
			SWITCH data.private.animationState.eClip
				CASE IDLE_A		sClip = "IDLE_A"		BREAK
				CASE IDLE_B		sClip = "IDLE_B"		BREAK
				CASE IDLE_C		sClip = "IDLE_C"		BREAK
				CASE ENTER		sClip = "ENTER"	 		BREAK
				CASE BASE		sClip = "BASE"			BREAK
				CASE LEAVE		sClip = "EXIT"			BREAK
				CASE A_TO_B		sClip = "D_TO_B"		BREAK
				CASE B_TO_C		sClip = "B_TO_C"		BREAK
				CASE C_TO_D		sClip = "C_TO_D"		BREAK
				CASE D_TO_E		sClip = "D_TO_B"		BREAK
				CASE B_TO_A		sClip = "B_TO_A"		BREAK
				CASE C_TO_A		sClip = "C_TO_A"		BREAK
				CASE D_TO_A		sClip = "D_TO_B"		BREAK
				CASE E_TO_A		sClip = "E_TO_A"		BREAK
				DEFAULT 		sClip = "invalid_clip"	BREAK
			ENDSWITCH
		ELSE
			SWITCH data.private.animationState.eClip
				CASE IDLE_A		sClip = "IDLE_A"		BREAK
				CASE IDLE_B		sClip = "IDLE_B"		BREAK
				CASE IDLE_C		sClip = "IDLE_C"		BREAK
				CASE ENTER		sClip = "ENTER"	 		BREAK
				CASE BASE		sClip = "BASE"			BREAK
				CASE LEAVE		sClip = "EXIT"			BREAK
				CASE A_TO_B		sClip = "A_TO_B"		BREAK
				CASE B_TO_C		sClip = "B_TO_C"		BREAK
				CASE C_TO_D		sClip = "A_TO_D"		BREAK
				CASE D_TO_E		sClip = "D_TO_E"		BREAK
				CASE B_TO_A		sClip = "B_TO_E"		BREAK
				CASE C_TO_A		sClip = "C_TO_A"		BREAK
				CASE D_TO_A		sClip = "D_TO_A"		BREAK
				CASE E_TO_A		sClip = "E_TO_A"		BREAK
				DEFAULT 		sClip = "invalid_clip"	BREAK
			ENDSWITCH
		ENDIF
	ELSE
		SWITCH data.private.animationState.eClip
			CASE IDLE_A		sClip = "IDLE_A"		BREAK
			CASE IDLE_B		sClip = "IDLE_B"		BREAK
			CASE IDLE_C		sClip = "IDLE_C"		BREAK
			CASE ENTER		sClip = "ENTER"	 		BREAK
			CASE BASE		sClip = "BASE"			BREAK
			CASE LEAVE		sClip = "EXIT"			BREAK
			CASE A_TO_B		sClip = "A_TO_B"		BREAK
			CASE B_TO_C		sClip = "B_TO_C"		BREAK
			CASE C_TO_D		sClip = "C_TO_D"		BREAK
			CASE D_TO_E		sClip = "D_TO_E"		BREAK
			CASE B_TO_A		sClip = "B_TO_E"		BREAK
			CASE C_TO_A		sClip = "C_TO_A"		BREAK
			CASE D_TO_A		sClip = "D_TO_A"		BREAK
			CASE E_TO_A		sClip = "E_TO_A"		BREAK
			DEFAULT 		sClip = "invalid_clip"	BREAK
		ENDSWITCH
	ENDIF
	
	IF data.private.animationState.eDictBase = ANIM_AMB_JACUZZI
		IF data.private.animationState.eClip = BASE
			sClip = "BASE_A"
		ELIF data.private.animationState.eClip = IDLE_A
			sClip = "IDLE_B"
		ENDIF
	ENDIF
	
	PRINTLN("[MUSIC_STUDIO_INT_SEATS] Constructed seat anim clip (", ENUM_TO_INT(data.private.animationState.eClip),"): ", sClip)
ENDPROC

PROC CONSTRUCT_ANIM_DICT(SEATS_LOCAL_DATA& data, TEXT_LABEL_63& sDict)
	SWITCH data.private.animationState.eDictBase
		CASE ANIM_AMB_CLUBHOUSE_SEATING 				sDict = "ANIM@AMB@CLUBHOUSE@SEATING"				BREAK
		CASE ANIM_AMB_OFFICE_SEATING 					sDict = "ANIM@AMB@OFFICE@SEATING"					BREAK
		CASE ANIM_AMB_FACILITY_BRIEFING_ROOM_SEATING 	sDict = "ANIM@AMB@FACILITY@BRIEFING_ROOM@SEATING" 	BREAK
		CASE ANIM_AMB_JACUZZI							sDict = "ANIM@AMB@YACHT@JACUZZI@SEATED"				BREAK
	ENDSWITCH
	
	IF IS_PLAYER_FEMALE()
		sDict += "@FEMALE"
	ELSE
		sDict += "@MALE"
	ENDIF
	
	SWITCH data.private.animationState.eDictBase
		CASE ANIM_AMB_OFFICE_SEATING
			IF IS_PLAYER_FEMALE()
				SWITCH data.private.animationState.eDictVar
					CASE VAR_A 		sDict += "@VAR_B@BASE@"	BREAK
					CASE VAR_B		sDict += "@VAR_B@BASE@"	BREAK
					CASE VAR_C		sDict += "@VAR_C@BASE@"	BREAK
					CASE VAR_D		sDict += "@VAR_D@BASE@"	BREAK
					CASE VAR_E		sDict += "@VAR_B@BASE@"	BREAK
				ENDSWITCH
			ELSE
				SWITCH data.private.animationState.eDictVar
					CASE VAR_A 		sDict += "@VAR_A@BASE@"	BREAK
					CASE VAR_B		sDict += "@VAR_B@BASE@"	BREAK
					CASE VAR_C		sDict += "@VAR_C@BASE@"	BREAK
					CASE VAR_D		sDict += "@VAR_D@BASE@"	BREAK
					CASE VAR_E		sDict += "@VAR_E@BASE@"	BREAK
				ENDSWITCH
			ENDIF
		BREAK
		
		CASE ANIM_AMB_CLUBHOUSE_SEATING
			SWITCH data.private.animationState.eDictVar
				CASE VAR_A 		sDict += "@VAR_A@BASE@"	BREAK
				CASE VAR_B		sDict += "@VAR_B@BASE@"	BREAK
				CASE VAR_C		sDict += "@VAR_C@BASE@"	BREAK
				CASE VAR_D		sDict += "@VAR_D@BASE@"	BREAK
				CASE VAR_E		sDict += "@VAR_E@BASE@"	BREAK
			ENDSWITCH
		BREAK
		
		CASE ANIM_AMB_FACILITY_BRIEFING_ROOM_SEATING
			SWITCH data.private.animationState.eDictVar
				CASE VAR_A 		sDict += "@VAR_A@"	BREAK
				CASE VAR_B		sDict += "@VAR_B@"	BREAK
				CASE VAR_C		sDict += "@VAR_C@"	BREAK
				CASE VAR_D		sDict += "@VAR_D@"	BREAK
				CASE VAR_E		sDict += "@VAR_E@"	BREAK
			ENDSWITCH
		BREAK
		
		CASE ANIM_AMB_JACUZZI
			SWITCH data.private.animationState.eDictVar
				CASE VAR_A		sDict += "@VARIATION_01@"	BREAK
				CASE VAR_B		sDict += "@VARIATION_02@"	BREAK
				CASE VAR_C		sDict += "@VARIATION_03@"	BREAK
				CASE VAR_D		sDict += "@VARIATION_04@"	BREAK
				CASE VAR_E		sDict += "@VARIATION_05@"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	PRINTLN("[MUSIC_STUDIO_INT_SEATS] Constructed seat anim dict (", ENUM_TO_INT(data.private.animationState.eDictBase)," + ",ENUM_TO_INT(data.private.animationState.eDictVar) ,"): ", sDict)
ENDPROC

#IF IS_DEBUG_BUILD
FUNC STRING GET_SEAT_STATE_STRING(SEAT_STATE state)
	SWITCH state
		CASE SS_INIT 				RETURN "SS_INIT"
		CASE SS_LOCATE_CHECK 		RETURN "SS_LOCATE_CHECK"
		CASE SS_PROMPT 				RETURN "SS_PROMPT"
		CASE SS_ANIM_INTERUPT 		RETURN "SS_ANIM_INTERUPT"
		CASE SS_ANIM_SITTING 		RETURN "SS_ANIM_SITTING"
		CASE SS_ANIM_START_ENTER 	RETURN "SS_ANIM_START_ENTER"
		CASE SS_CLEANUP				RETURN "SS_CLEANUP"
		CASE SS_MOVE_TO_ANIM_START	RETURN "SS_MOVE_TO_ANIM_START"
		CASE SS_REQUEST_ANIMATION	RETURN "SS_REQUEST_ANIMATION"
	ENDSWITCH
	
	RETURN "INVALID_STATE"
ENDFUNC

PROC PRINT_BUSY_REASON(SEATS_LOCAL_DATA& data)
	IF IS_BIT_SET_ENUM(data.private.iBSGeneral, PSGB_BUSY_BROWSER_OPEN)
		PRINTLN("[MUSIC_STUDIO_INT_SEATS] PRINT_BUSY_REASON : Player has browswer open.")
	ENDIF
	
	IF IS_BIT_SET_ENUM(data.private.iBSGeneral, PSGB_BUSY_DEAD)
		PRINTLN("[MUSIC_STUDIO_INT_SEATS] PRINT_BUSY_REASON : Player is dead.")
	ENDIF
	
	IF IS_BIT_SET_ENUM(data.private.iBSGeneral, PSGB_BUSY_IN_VEHICLE)
		PRINTLN("[MUSIC_STUDIO_INT_SEATS] PRINT_BUSY_REASON : Player is in a vehicle.")
	ENDIF
	
	IF IS_BIT_SET_ENUM(data.private.iBSGeneral, PSGB_BUSY_ON_PHONE)
		PRINTLN("[MUSIC_STUDIO_INT_SEATS] PRINT_BUSY_REASON : Player has phone open.")
	ENDIF
	
	IF IS_BIT_SET_ENUM(data.private.iBSGeneral, PSGB_BUSY_RUNNING)
		PRINTLN("[MUSIC_STUDIO_INT_SEATS] PRINT_BUSY_REASON : Player is running.")
	ENDIF
ENDPROC
#ENDIF

FUNC FLOAT GET_PED_CAPSULE_SIZE()
	RETURN SITTING_CAPSULE_SIZE
ENDFUNC

FUNC BOOL IS_PED_BLOCKING_ACTIVITY_ANGLED_AREA(SEATS_LOCAL_DATA& data)
	PED_INDEX pPeds[10]
	
	GET_PED_NEARBY_PEDS(PLAYER_PED_ID(), pPeds)
	
	INT i
	REPEAT 10 i
		IF DOES_ENTITY_EXIST(pPeds[i])
		AND NOT IS_PED_INJURED(pPeds[i])
		AND IS_ENTITY_VISIBLE(pPeds[i])
		AND IS_ENTITY_IN_SEAT_ENTRY_LOCATE(pPeds[i], data.public.seats[data.private.iInSeatLocate])
			PRINTLN("[MUSIC_STUDIO_INT_SEATS] IS_PED_BLOCKING_ACTIVITY_ANGLED_AREA - seat ", data.private.iInSeatLocate, " is blocked.")
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	PRINTLN("[MUSIC_STUDIO_INT_SEATS] IS_PED_BLOCKING_ACTIVITY_ANGLED_AREA - seat ", data.private.iInSeatLocate, " is not blocked.")
	
	RETURN FALSE
ENDFUNC

PROC RUN_IGNORE_COLLISION_FOR_SIMPLE_POP_LOGIC(SEATS_LOCAL_DATA& data)
	IF data.private.eState = SS_ANIM_SITTING
		// Do nothing
	ELIF data.private.eState > SS_MOVE_TO_ANIM_START
		SET_GAMEPLAY_CAM_IGNORE_ENTITY_COLLISION_THIS_UPDATE(PLAYER_PED_ID())
		SET_PED_CAPSULE(PLAYER_PED_ID(), GET_PED_CAPSULE_SIZE())
		data.private.iFrameLastCapsuleChange = GET_FRAME_COUNT()
	//Capsule shrink linger so that it doesn't pop the player out during
	//transition from cleanup->locate check and when the player becomes
	//busy during the locate check state.
	ELIF GET_FRAME_COUNT() - data.private.iFrameLastCapsuleChange < CAPSULE_SHRINK_LINGER_NUM_FRAMES
		SET_PED_CAPSULE(PLAYER_PED_ID(), GET_PED_CAPSULE_SIZE())
	ENDIF
ENDPROC

PROC SET_SEAT_STATE(SEATS_LOCAL_DATA& data, SEAT_STATE state)
	data.private.eState = state
	PRINTLN("[MUSIC_STUDIO_INT_SEATS] SET_SEAT_STATE : Setting state to ", GET_SEAT_STATE_STRING(state))
ENDPROC

PROC UPDATE_ENTITY_CHECKS(SEATS_LOCAL_DATA& data)
	SET_BIT_VALUE_ENUM(data.private.iBSGeneral, PSGB_BUSY_BROWSER_OPEN, IS_BROWSER_OPEN())
	SET_BIT_VALUE_ENUM(data.private.iBSGeneral, PSGB_BUSY_DEAD, IS_ENTITY_DEAD(PLAYER_PED_ID()))
	SET_BIT_VALUE_ENUM(data.private.iBSGeneral, PSGB_BUSY_IN_VEHICLE, IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE))
	SET_BIT_VALUE_ENUM(data.private.iBSGeneral, PSGB_BUSY_ON_PHONE, IS_PHONE_ONSCREEN())
	SET_BIT_VALUE_ENUM(data.private.iBSGeneral, PSGB_BUSY_RUNNING, IS_PED_RUNNING(PLAYER_PED_ID()))
ENDPROC

FUNC BOOL ARE_THERE_TOO_MANY_PLAYERS_CLOSE_TO_ACTIVITY_ANGLED_AREA(SEATS_LOCAL_DATA& data)
	INT iParticipant
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
			PLAYER_INDEX playerID = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
			
			IF playerID <> PLAYER_ID()
			AND IS_NET_PLAYER_OK(playerID)
				IF IS_ENTITY_IN_SEAT_ENTRY_LOCATE(GET_PLAYER_PED(playerID), data.public.seats[data.private.iInSeatLocate])
				OR GET_PLAYER_USING_OFFICE_SEATID(playerID) = data.private.iInSeatLocate
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_BUSY(SEATS_LOCAL_DATA& data)
	RETURN (data.private.iBSGeneral & BUSY_MASK) > 0
ENDFUNC

FUNC INT GET_NEXT_VAR_FOR_SEAT(SEATS_LOCAL_DATA& data)
	INT iNewVar = (ENUM_TO_INT(data.private.animationState.eDictVar) + 1) % GET_ANIM_S1_VARIATIONS(data.private.animationState.eDictBase)
	
	IF data.private.animationState.eDictBase = ANIM_AMB_FACILITY_BRIEFING_ROOM_SEATING
	OR data.private.animationState.eDictBase = ANIM_AMB_JACUZZI
		RETURN ENUM_TO_INT(data.private.animationState.eDictVar)
	ENDIF
	
	IF IS_PLAYER_FEMALE()
		IF data.private.animationState.eDictBase = ANIM_AMB_OFFICE_SEATING
			IF INT_TO_ENUM(SEAT_ANIM_1, iNewVar) = VAR_A
			OR INT_TO_ENUM(SEAT_ANIM_1, iNewVar) = VAR_E
				iNewVar = ENUM_TO_INT(VAR_B)
			ENDIF
		ENDIF
	ELSE
		IF data.private.animationState.eDictBase = ANIM_AMB_OFFICE_SEATING
			IF INT_TO_ENUM(SEAT_ANIM_1, iNewVar) = VAR_B
			OR INT_TO_ENUM(SEAT_ANIM_1, iNewVar) = VAR_C
				iNewVar = ENUM_TO_INT(VAR_D)
			ENDIF
		ENDIF
	ENDIF
	
	RETURN iNewVar
ENDFUNC

PROC GENERATE_AND_STORE_NEXT_ANIM(SEATS_LOCAL_DATA& data, BOOL bForceVarChange = FALSE)
	INT iNewClip
	INT iNewVar
	
	#IF IS_DEBUG_BUILD
	IF data.debug.bForceAnimVarNow
		iNewClip = ENUM_TO_INT(BASE)
		iNewVar = data.debug.iNextForcedAnimVar
		
		PRINTLN("[MUSIC_STUDIO_INT_SEATS][ANIM] New var : ", iNewVar)
		PRINTLN("[MUSIC_STUDIO_INT_SEATS][ANIM] New clip : ", iNewClip)
		
		data.private.animationState.ePrevClip = data.private.animationState.eClip
		data.private.animationState.eClip = INT_TO_ENUM(SEAT_ANIM_2_CLIP, iNewClip)
		data.private.animationState.eDictVar = INT_TO_ENUM(SEAT_ANIM_1, iNewVar)
		EXIT
	ENDIF
	#ENDIF
	
	IF bForceVarChange
		iNewVar = GET_NEXT_VAR_FOR_SEAT(data)
		
		//Only set a transition if we actually changed var (Some seats cannot handle more than one var)
		IF iNewVar <> ENUM_TO_INT(data.private.animationState.eDictVar)
			IF iNewVar = 0
				iNewClip = ENUM_TO_INT(TRANSITION_LOOPS) + ENUM_TO_INT(data.private.animationState.eDictVar)
			ELSE
				iNewClip = ENUM_TO_INT(TRANSITIONS) + iNewVar
			ENDIF
		ELSE
			iNewClip = ENUM_TO_INT(data.private.animationState.eClip)
		ENDIF
		
		//Set it back because the transition forward is tied to the PREVIOUS DictVar.
		//Next time this function is called, this will change.
		iNewVar =  ENUM_TO_INT(data.private.animationState.eDictVar)
	ELSE
		//Should also switch between variants after a while...
		IF data.private.animationState.eClip = BASE
			iNewVar = ENUM_TO_INT(data.private.animationState.eDictVar)
			
			// idle_a, idle_b, idle_c = 3 variations
			INT iNumIdleVars = 3 
			
			iNewClip = GET_RANDOM_INT_IN_RANGE(0, iNumIdleVars)
			
			IF iNewClip = ENUM_TO_INT(data.private.animationState.ePrevClip)
				iNewClip = (iNewClip + 1) % iNumIdleVars
			ENDIF
		ELSE
			//If we just just used a transition anim to get to this Var then we now need to update the Var from previous
			IF data.private.animationState.eClip > TRANSITIONS
				iNewVar = GET_NEXT_VAR_FOR_SEAT(data)
				
				//Start the timer because we've completed the transition
				data.private.animationState.iVarChangeTime = GET_GAME_TIMER()
			ELSE
				//Otherwise keep it as is
				iNewVar = ENUM_TO_INT(data.private.animationState.eDictVar)
			ENDIF
			
			//Should change to a new variation of the base anim?
			IF GET_GAME_TIMER() - data.private.animationState.iVarChangeTime >= TIME_BEFORE_ANIM_VAR_CHANGE
			AND data.private.animationState.eDictBase != ANIM_AMB_JACUZZI
				iNewVar = GET_NEXT_VAR_FOR_SEAT(data)
				
				//Only set a transition if we actually changed var (Some seats cannot handle more than one var)
				IF iNewVar <> ENUM_TO_INT(data.private.animationState.eDictVar)
					IF iNewVar = 0
						iNewClip = ENUM_TO_INT(TRANSITION_LOOPS) + ENUM_TO_INT(data.private.animationState.eDictVar)
					ELSE
						iNewClip = ENUM_TO_INT(TRANSITIONS) + iNewVar
					ENDIF
				ENDIF
				
				//Set it back because the transition forward is tied to the PREVIOUS DictVar.
				//Next time this function is called, this will change.
				iNewVar =  ENUM_TO_INT(data.private.animationState.eDictVar)
			ELSE
				iNewClip = ENUM_TO_INT(BASE)
			ENDIF
		ENDIF
	ENDIF
	
	PRINTLN("[MUSIC_STUDIO_INT_SEATS][ANIM] New var : ", iNewVar)
	PRINTLN("[MUSIC_STUDIO_INT_SEATS][ANIM] New clip : ", iNewClip)
	
	data.private.animationState.ePrevClip = data.private.animationState.eClip
	data.private.animationState.eClip = INT_TO_ENUM(SEAT_ANIM_2_CLIP, iNewClip)
	data.private.animationState.eDictVar = INT_TO_ENUM(SEAT_ANIM_1, iNewVar)
ENDPROC	

PROC MAINTAIN_SUPRESS_PHONE_AND_ACTIONS(SEATS_LOCAL_DATA& data)
	WEAPON_TYPE wtWeapon
	
	SET_THIRD_PERSON_CAM_RELATIVE_PITCH_LIMITS_THIS_UPDATE(-70.0027, -10.1277)
	
	SWITCH data.private.eState
		CASE SS_MOVE_TO_ANIM_START
		CASE SS_CLEANUP
		CASE SS_ANIM_SITTING
			DISABLE_CELLPHONE_THIS_FRAME_ONLY(TRUE)
			INVALIDATE_IDLE_CAM()
			HUD_FORCE_WEAPON_WHEEL(FALSE)
			DISPLAY_AMMO_THIS_FRAME(FALSE)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_WHEEL)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
			HUD_SUPPRESS_WEAPON_WHEEL_RESULTS_THIS_FRAME()
			DISABLE_CELLPHONE_CAMERA_APP_THIS_FRAME_ONLY()
			
			IF GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),wtWeapon)
			AND wtWeapon != WEAPONTYPE_UNARMED
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
			ENDIF
			
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
			SET_CLEAR_ON_CALL_HUD_THIS_FRAME()
			DISABLE_SELECTOR_THIS_FRAME()
		BREAK
	ENDSWITCH
ENDPROC

PROC LEAVE_SEAT(SEATS_LOCAL_DATA& data)
	data.private.iNetSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(GET_ANIM_POS(data), GET_ANIM_ROT(data), DEFAULT, TRUE, FALSE, DEFAULT, DEFAULT, 1.12)
	
	TEXT_LABEL_63 sDict
	
	CONSTRUCT_ANIM_DICT(data, sDict)
	
	NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), data.private.iNetSceneID, sDict, "exit", SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_ON_ABORT_STOP_SCENE)
	
	NETWORK_START_SYNCHRONISED_SCENE(data.private.iNetSceneID)
	
	RELEASE_CONTEXT_INTENTION(data.private.iContext)
	
	PRINTLN("[MUSIC_STUDIO_INT_SEATS] LEAVE_SEAT : Leaving seat")	
	
	SET_SEAT_STATE(data, SS_CLEANUP)
ENDPROC

PROC MAINTAIN_EXIT_INTERRUPT(SEATS_LOCAL_DATA& data)
	IF HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), GET_HASH_KEY("EXIT_INTERRUPT"))
	AND data.private.eState = SS_CLEANUP
		PRINTLN("[MUSIC_STUDIO_INT_SEATS] MAINTAIN_EXIT_INTERRUPT : HAS_ANIM_EVENT_FIRED(EXIT_INTERRUPT) : TRUE")
		
		VECTOR vLeftInput = <<GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X), GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y), 0>>
		VECTOR vRightInput = <<GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_X), GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_Y), 0>>
		
		IF VMAG(vLeftInput) >= 0.24
		OR VMAG(vRightInput) >= 0.24
			PRINTLN("[MUSIC_STUDIO_INT_SEATS] Input recieved, interupting seat anim.")
			
			SET_SEAT_STATE(data, SS_ANIM_INTERUPT)
		ENDIF
	ENDIF
ENDPROC

PROC CLEAR_SEATS_HELP_TEXT(SEATS_LOCAL_DATA& data)
	IF data.private.iContext != NEW_CONTEXT_INTENTION
		RELEASE_CONTEXT_INTENTION(data.private.iContext)
		data.private.iContext = NEW_CONTEXT_INTENTION
		CLEAR_HELP()
	ENDIF
ENDPROC

FUNC BOOL IS_EXIT_BLOCKED_THIS_FRAME(SEATS_LOCAL_DATA& data)
	RETURN IS_BIT_SET_ENUM(data.private.iBSGeneral, PSGB_BLOCK_EXIT_THIS_FRAME)
ENDFUNC

PROC FORCE_VARIATION_CHANGE(SEATS_LOCAL_DATA& data)
	//Make sure we actually have something to transition to
	//AND we are not already transitioning...
	IF (GET_NEXT_VAR_FOR_SEAT(data) <> ENUM_TO_INT(data.private.animationState.eDictVar)
	AND data.private.animationState.eClip <  TRANSITIONS
	AND data.private.animationState.eClip <> ENTER
	AND data.private.animationState.eClip <> LEAVE)
	#IF IS_DEBUG_BUILD
	OR data.debug.bForceAnimVarNow
	#ENDIF
		TEXT_LABEL_63 txtAnimDict 
		TEXT_LABEL_15 txtAnimClip
		
		GENERATE_AND_STORE_NEXT_ANIM(data, TRUE)
		
		CONSTRUCT_ANIM_DICT(data, txtAnimDict)
		CONSTRUCT_ANIM_CLIP(data, txtAnimClip)
		
		data.private.iNetSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(GET_ANIM_POS(data), GET_ANIM_ROT(data), DEFAULT, TRUE, FALSE, DEFAULT, DEFAULT, 1.12)
		
		NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), data.private.iNetSceneID, txtAnimDict, txtAnimClip, SLOW_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_ON_ABORT_STOP_SCENE)
		
		NETWORK_START_SYNCHRONISED_SCENE(data.private.iNetSceneID)
		
		//Need to make sure we don't head straight to SET_SEAT_STATE(data, SS_CLEANUP) with any delays before next anim
		SET_BIT_ENUM(data.private.iBSGeneral, PSGB_WAITING_FOR_NEXT_DICTIONARY)
		
		FORCE_PED_AI_AND_ANIMATION_UPDATE(player_ped_id())
		
		PRINTLN("[MUSIC_STUDIO_INT_SEATS]FORCE_VARIATION_CHANGE - Complete.")
	ELSE
		#IF IS_DEBUG_BUILD
		IF GET_NEXT_VAR_FOR_SEAT(data) = ENUM_TO_INT(data.private.animationState.eDictVar)
			PRINTLN("[MUSIC_STUDIO_INT_SEATS] - FORCE_VARIATION_CHANGE - There are no variations for this seat.")
		ELIF data.private.animationState.eClip >  TRANSITIONS
			PRINTLN("[MUSIC_STUDIO_INT_SEATS] - FORCE_VARIATION_CHANGE - Already in a transition.")
		ELIF data.private.animationState.eClip = ENTER
		OR data.private.animationState.eClip = LEAVE
			PRINTLN("[MUSIC_STUDIO_INT_SEATS] - FORCE_VARIATION_CHANGE - Entering/Exiting.")
		ENDIF
		#ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_SITTING_INTERUPT(SEATS_LOCAL_DATA& data)
	#IF IS_DEBUG_BUILD
	IF data.debug.bForceAnimVarNow
		FORCE_VARIATION_CHANGE(data)
		data.debug.bForceAnimVarNow = FALSE
		
		EXIT
	ENDIF
	#ENDIF
	
	IF data.private.eState = SS_ANIM_SITTING
		VECTOR vLeftInput = <<GET_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X), GET_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y), 0>>
		FLOAT fLeftInputMag = VMAG(vLeftInput)
		
		IF data.private.eState = SS_ANIM_SITTING
		AND NOT HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), GET_HASH_KEY("BLOCK_INTERRUPT"))
		AND NOT IS_BIT_SET_ENUM(data.private.iBSGeneral, PSGB_BLOCK_MANUAL_VAR_CHANGE)
			IF fLeftInputMag >= 0.35
			AND NOT g_bUsingCCTV
			AND NOT g_bRequestingLiveStream
				PRINTLN("[MUSIC_STUDIO_INT_SEATS] Input recieved, forcing next variation.")
				
				FORCE_VARIATION_CHANGE(data)
			ENDIF
		ENDIF
		
		IF HAVE_CONTROLS_CHANGED(FRONTEND_CONTROL)
			CLEAR_SEATS_HELP_TEXT(data)
		ENDIF
		
		IF HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), GET_HASH_KEY("BLOCK_INTERRUPT"))
			PRINTLN("[MUSIC_STUDIO_INT_SEATS] MAINTAIN_SITTING_INTERUPT : HAS_ANIM_EVENT_FIRED(BLOCK_INTERRUPT) : TRUE")
			
			CLEAR_SEATS_HELP_TEXT(data)
		ELIF g_bUsingCCTV
		OR g_bRequestingLiveStream
			CLEAR_SEATS_HELP_TEXT(data)
		ELIF IS_EXIT_BLOCKED_THIS_FRAME(data)
			PRINTLN("[MUSIC_STUDIO_INT_SEATS] MAINTAIN_SITTING_INTERUPT : IS_EXIT_BLOCKED_THIS_FRAME() : TRUE")
			
			CLEAR_SEATS_HELP_TEXT(data)
		ELSE
			ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(FRONTEND_CONTROL)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_ALTERNATE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK1)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK2)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)
			
			IF NOT IS_PAUSE_MENU_ACTIVE()
			AND NOT IS_BIT_SET_ENUM(data.private.iBSGeneral, PSGB_BUSY_ON_PHONE)
			AND NOT IS_BIT_SET_ENUM(data.private.iBSGeneral, PSGB_BUSY_BROWSER_OPEN)
			AND NOT IS_INTERACTION_MENU_OPEN()
			AND NOT IS_SELECTOR_UI_BUTTON_PRESSED()
			AND data.private.animationState.eClip <> ENTER
				IF data.private.iContext = NEW_CONTEXT_INTENTION
					IF data.private.animationState.eDictBase = ANIM_AMB_OFFICE_SEATING
					OR data.private.animationState.eDictBase = ANIM_AMB_CLUBHOUSE_SEATING
						IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
							REGISTER_CONTEXT_INTENTION(data.private.iContext, CP_HIGH_PRIORITY, "MPOF_AC_PC_EXIT")
						ELSE
							REGISTER_CONTEXT_INTENTION(data.private.iContext, CP_HIGH_PRIORITY, "MPOF_AC_EXIT")
						ENDIF
					ELSE
						IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
							REGISTER_CONTEXT_INTENTION(data.private.iContext, CP_HIGH_PRIORITY, "MPOFSEAT_PCEXIT")
						ELSE
							REGISTER_CONTEXT_INTENTION(data.private.iContext, CP_HIGH_PRIORITY, "MPOFSEAT_EXIT")
						ENDIF
					ENDIF
				ENDIF
				
				IF data.private.eState = SS_ANIM_SITTING
					CONTROL_ACTION playerControlAction
					
					IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
						playerControlAction = INPUT_SCRIPT_RRIGHT
					ELSE
						playerControlAction = INPUT_FRONTEND_RIGHT
					ENDIF
					
					IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, playerControlAction)
						PRINTLN("[MUSIC_STUDIO_INT_SEATS] Player wants to leave seat")
						
						LEAVE_SEAT(data)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	CLEAR_BIT_ENUM(data.private.iBSGeneral, PSGB_BLOCK_EXIT_THIS_FRAME)
ENDPROC

PROC CLEANUP_SEATS_ACTIVITY(SEATS_LOCAL_DATA& data)
	RELEASE_CONTEXT_INTENTION(data.private.iContext)
	
	CLEAR_SEATS_HELP_TEXT(data)
	
	IF data.private.eState > SS_PROMPT
		SET_PLAYER_USING_OFFICE_SEATID(-1)
		PRINTLN("[MUSIC_STUDIO_INT_SEATS] CLEANUP_SEATS_ACTIVITY : seat in use, clearing player seat ID.")
		
		INT localScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(data.private.iNetSceneID)
		
		IF IS_SYNCHRONIZED_SCENE_RUNNING(localScene)
			NETWORK_STOP_SYNCHRONISED_SCENE(data.private.iNetSceneID)
		ENDIF
		
		IF NOT IS_TRANSITION_ACTIVE()
		AND NOT IS_NEW_LOAD_SCENE_ACTIVE()
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		ENDIF
	ENDIF
	
	CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSFive, BS_SIMPLE_INTERIOR_GLOBAL_BS_FIVE_USING_JACUZZI)
	
	PRINTLN("[MUSIC_STUDIO_INT_SEATS] CLEANUP_SEATS_ACTIVITY : Complete.")
ENDPROC

PROC MAINTAIN_SEATS_ACTIVITY_CLEANUP(SEATS_LOCAL_DATA& data)
	IF IS_PED_INJURED(PLAYER_PED_ID())
	OR IS_PED_RAGDOLL(PLAYER_PED_ID())
	OR IS_PED_IN_COMBAT(PLAYER_PED_ID())
	OR IS_PED_IN_MELEE_COMBAT(PLAYER_PED_ID())
		RELEASE_CONTEXT_INTENTION(data.private.iContext)
		
		CLEAR_SEATS_HELP_TEXT(data)
		
		IF data.private.eState > SS_PROMPT
			SET_PLAYER_USING_OFFICE_SEATID(-1)
			PRINTLN("[MUSIC_STUDIO_INT_SEATS] CLEANUP_SEATS_ACTIVITY : seat in use, clearing player seat ID.")
			
			INT localScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(data.private.iNetSceneID)
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(localScene)
				NETWORK_STOP_SYNCHRONISED_SCENE(data.private.iNetSceneID)
			ENDIF
		ENDIF
		
		SET_SEAT_STATE(data, SS_INIT)
		
		PRINTLN("[MUSIC_STUDIO_INT_SEATS] CLEANUP_SEATS_ACTIVITY : Complete.")
	ENDIF
ENDPROC

FUNC BOOL DO_ADDITIONAL_PROMPT_CHECK(SEATS_LOCAL_DATA& data)
	RETURN NOT IS_BIT_SET_ENUM(data.private.iBSGeneral, PSGB_ADDITIONAL_CHECK_BEFORE_PROMPT)
	OR CALL data.private.cCanPlayerUseSeat(data.public.seats[data.private.iInSeatLocate], data.private.iInSeatLocate)
ENDFUNC

FUNC BOOL IS_A_DISABLED_JACUZZI_SEAT(SEAT_TYPE eSeatType)
	
	IF eSeatType != SEAT_TYPE_JACUZZI_ANIM
		RETURN FALSE
	ENDIF
	
	RETURN g_sMPtunables.bVC_PENTHOUSE_DISABLE_HOTTUB	
ENDFUNC

FUNC BOOL IS_SEAT_DCTL(INT iSeatID)
	SWITCH iSeatID
		CASE 2
		CASE 3
		CASE 6
		CASE 7
			IF g_sShopSettings.playerRoom = 486476725
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_DJ_POOH_ON_COUCH()
	IF g_iPedLayout = 0
	AND IS_DRE_IN_MUSIC_STUDIO()
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

PROC MAINTAIN_SEATS_ACTIVITY(SEATS_LOCAL_DATA& data)
	UPDATE_ENTITY_CHECKS(data)
	RUN_IGNORE_COLLISION_FOR_SIMPLE_POP_LOGIC(data)
	
	TEXT_LABEL_63 sAnimDict
	TEXT_LABEL_15 sAnimClip
	
	SWITCH data.private.eState
		CASE SS_INIT
			CLEAR_BIT_ENUM(data.private.iBSGeneraL, PSGB_ENTRY_COMPLETE)
			
			SET_SEAT_STATE(data,SS_LOCATE_CHECK)
		BREAK
		
		CASE SS_LOCATE_CHECK
			g_bInNightclubSeatLocate = FALSE
			
			IF STAGGERED_UPDATE_LOCATE_CHECK(data)
				data.private.iSeatBlockedTimer = GET_GAME_TIMER()
				SET_SEAT_STATE(data, SS_PROMPT)
			ENDIF
		BREAK
		
		CASE SS_PROMPT
			IF IS_ENTITY_IN_SEAT_ENTRY_LOCATE(PLAYER_PED_ID(), data.public.seats[data.private.iInSeatLocate])
			AND IS_ENTITY_FACING_SEAT(PLAYER_PED_ID(), data.public.seats[data.private.iInSeatLocate].animation.vRot, data.public.seats[data.private.iInSeatLocate].eType)
			AND NOT (IS_DJ_POOH_ON_COUCH() AND data.private.iInSeatLocate = 8) // Right-most seat on couch in main recording room
				IF IS_PLAYER_BUSY(data)
				OR ARE_THERE_TOO_MANY_PLAYERS_CLOSE_TO_ACTIVITY_ANGLED_AREA(data)
				OR IS_PED_BLOCKING_ACTIVITY_ANGLED_AREA(data)
				OR NOT DO_ADDITIONAL_PROMPT_CHECK(data)
				OR IS_PLAYER_USING_BALLISTIC_EQUIPMENT(PLAYER_ID())
				OR IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
				OR IS_PED_IN_COMBAT(PLAYER_PED_ID())
				OR IS_PED_IN_MELEE_COMBAT(PLAYER_PED_ID())
				OR IS_PED_PERFORMING_MELEE_ACTION(PLAYER_PED_ID())
				OR IS_PED_RAGDOLL(PLAYER_PED_ID())
				OR IS_PED_INJURED(PLAYER_PED_ID())
				OR IS_BIT_SET(g_simpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_INSTANT_PASS_OUT)
				OR IS_A_DISABLED_JACUZZI_SEAT(data.public.seats[data.private.iInSeatLocate].eType)
					SET_BIT(g_SimpleInteriorData.iForthBs, BS4_SIMPLE_INTERIOR_BLOCK_PLAYER_DANCING_PROMPT_THIS_FRAME)
					
					IF ARE_THERE_TOO_MANY_PLAYERS_CLOSE_TO_ACTIVITY_ANGLED_AREA(data)
						REGISTER_CONTEXT_INTENTION(data.private.iContext, CP_HIGH_PRIORITY, "POD_TOO_MANY")
					ELSE
						RELEASE_CONTEXT_INTENTION(data.private.iContext)
						
						CLEAR_SEATS_HELP_TEXT(data)
						
						SET_SEAT_STATE(data, SS_LOCATE_CHECK)
					ENDIF
				ELSE
					g_bInNightclubSeatLocate = TRUE
					
					SET_BIT(g_SimpleInteriorData.iForthBs, BS4_SIMPLE_INTERIOR_BLOCK_PLAYER_DANCING_PROMPT_THIS_FRAME)
					
					IF data.private.iContext = NEW_CONTEXT_INTENTION
						IF GET_GAME_TIMER() - data.private.iSeatBlockedTimer > SEAT_BLOCK_TIMER
							IF IS_SEAT_DCTL(data.private.iInSeatLocate)
							AND NOT g_sMPTunables.bVC_PENTHOUSE_DISABLE_DCTL
								REGISTER_CONTEXT_INTENTION(data.private.iContext, CP_HIGH_PRIORITY, "MPTV_WALKOFF")
							ELSE
								REGISTER_CONTEXT_INTENTION(data.private.iContext, CP_HIGH_PRIORITY, "MPJAC_SIT")
							ENDIF
						ENDIF
					ELIF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_CONTEXT)
						RELEASE_CONTEXT_INTENTION(data.private.iContext)
						
						SET_START_ANIMATION(data)
						
						SET_SEAT_STATE(data, SS_REQUEST_ANIMATION)
						
						IF data.public.seats[data.private.iInSeatLocate].eType = SEAT_TYPE_JACUZZI_ANIM
							SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSFive, BS_SIMPLE_INTERIOR_GLOBAL_BS_FIVE_USING_JACUZZI)
							SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_AM_RELAX_IN_HOT_TUB)
							PRINTLN("[MUSIC_STUDIO_INT_SEATS] Enter hot tub daily objective complete")
						ENDIF
					ENDIF
				ENDIF
			ELSE
				CLEAR_SEATS_HELP_TEXT(data)
				SET_SEAT_STATE(data, SS_LOCATE_CHECK)
			ENDIF
		BREAK
		
		CASE SS_REQUEST_ANIMATION
			CONSTRUCT_ANIM_DICT(data, sAnimDict)
			
			REQUEST_ANIM_DICT(sAnimDict)
			
			IF HAS_ANIM_DICT_LOADED(sAnimDict)
				SET_SEAT_STATE(data, SS_MOVE_TO_ANIM_START)
			ENDIF
		BREAK
		
		CASE SS_MOVE_TO_ANIM_START
			CONSTRUCT_ANIM_DICT(data, sAnimDict)
			CONSTRUCT_ANIM_CLIP(data, sAnimClip)
			
			IF data.private.animationState.eDictBase = ANIM_AMB_JACUZZI
				IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
					IF NOT IS_BIT_SET_ENUM(data.private.iBSGeneral, PSGB_BLOCK_FIRST_PERSON)
						SET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT, CAM_VIEW_MODE_THIRD_PERSON_MEDIUM)
						SET_BIT_ENUM(data.private.iBSGeneral, PSGB_BLOCK_FIRST_PERSON)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "JAC_RUN_ENTER_ANIM: setting camera view mode to third person")
					ENDIF
				ENDIF
			ENDIF
			
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			
			VECTOR vTriggerPos
			vTriggerPos = GET_ANIM_INITIAL_OFFSET_POSITION(sAnimDict, sAnimClip, GET_ANIM_POS(data), GET_ANIM_ROT(data))
			
			VECTOR vTempRotation
			vTempRotation = GET_ANIM_INITIAL_OFFSET_ROTATION(sAnimDict, sAnimClip, GET_ANIM_POS(data), GET_ANIM_ROT(data))
			
			FLOAT fTriggerHead
			fTriggerHead = vTempRotation.Z
			
			FLOAT fTargetRadius
			fTargetRadius = 0.05
			
			IF data.private.animationState.eDictBase = ANIM_AMB_OFFICE_SEATING
				fTargetRadius = 0.15
			ENDIF
			
			TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), vTriggerPos, PEDMOVEBLENDRATIO_WALK, 500, fTriggerHead, fTargetRadius)
			
			SET_PLAYER_USING_OFFICE_SEATID(data.private.iInSeatLocate)
			
			PRINTLN("[MUSIC_STUDIO_INT_SEATS] WALKING TO COORDS")
			
			SET_SEAT_STATE(data, SS_ANIM_START_ENTER)
		BREAK
		
		CASE SS_ANIM_START_ENTER
			PRINTLN("[MUSIC_STUDIO_INT_SEATS] WALKING TO COORDS")
			
			MAINTAIN_SUPRESS_PHONE_AND_ACTIONS(data)
			
			SCRIPTTASKSTATUS eTaskStatus
			
			CONSTRUCT_ANIM_DICT(data, sAnimDict)
			CONSTRUCT_ANIM_CLIP(data, sAnimClip)
			
			eTaskStatus = GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD)
			
			VECTOR vTempRotation2
			vTempRotation2 = GET_ANIM_INITIAL_OFFSET_ROTATION(sAnimDict, sAnimClip, GET_ANIM_POS(data), GET_ANIM_ROT(data))
			
			Float fTriggerHead2
			fTriggerHead2 = vTempRotation2.Z
			
			IF (eTaskStatus <> PERFORMING_TASK
			AND eTaskStatus <> WAITING_TO_START_TASK)
			OR IS_HEADING_ACCEPTABLE_CORRECTED(GET_ENTITY_HEADING(PLAYER_PED_ID()), fTriggerHead2, 5.0)
				IF data.private.animationState.eDictBase = ANIM_AMB_JACUZZI
					IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
						IF NOT IS_BIT_SET_ENUM(data.private.iBSGeneral, PSGB_BLOCK_FIRST_PERSON)
							SET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT, CAM_VIEW_MODE_THIRD_PERSON_MEDIUM)
							SET_BIT_ENUM(data.private.iBSGeneral, PSGB_BLOCK_FIRST_PERSON)
							CDEBUG1LN(DEBUG_SAFEHOUSE, "JAC_RUN_ENTER_ANIM: setting camera view mode to third person")
						ENDIF
					ENDIF
				ENDIF
				
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
				
				data.private.iNetSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(GET_ANIM_POS(data), GET_ANIM_ROT(data), DEFAULT, TRUE, FALSE, DEFAULT, DEFAULT, 1.12)
				
				NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), data.private.iNetSceneID, sAnimDict, sAnimClip, REALLY_SLOW_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_ON_ABORT_STOP_SCENE)
				
				NETWORK_START_SYNCHRONISED_SCENE(data.private.iNetSceneID)
				
				//Need to set reset the animtion variation timer.
				data.private.animationState.iVarChangeTime = GET_GAME_TIMER()
				
				//Need to make sure we don't head straight to SET_SEAT_STATE(data, SS_CLEANUP) with any delays before next anim
				SET_BIT_ENUM(data.private.iBSGeneral, PSGB_WAITING_FOR_NEXT_DICTIONARY)
				
				SET_SEAT_STATE(data, SS_ANIM_SITTING)
			ENDIF
		BREAK
		
		CASE SS_ANIM_SITTING
			IF IS_BIT_SET_ENUM(data.private.iBSGeneral, PSGB_GET_OUT_OF_SEAT)
			OR IS_LOCAL_PLAYER_BEING_KICKED_OUT_OF_SIMPLE_INTERIOR_RESTRICTED_AREA()
				PRINTLN("[MUSIC_STUDIO_INT_SEATS] IS_BIT_SET_ENUM(data.private.iBSGeneral, PSGB_GET_OUT_OF_SEAT) : External request for player to leave seat.")
				
				CLEAR_BIT_ENUM(data.private.iBSGeneral, PSGB_GET_OUT_OF_SEAT)
				
				LEAVE_SEAT(data)
			ENDIF
			
			IF (IS_DJ_POOH_ON_COUCH() AND data.private.iInSeatLocate = 8) // Right-most seat on couch in main recording room
				LEAVE_SEAT(data)
			ENDIF
			
			MAINTAIN_SUPRESS_PHONE_AND_ACTIONS(data)
			
			MAINTAIN_SITTING_INTERUPT(data)
			
			INT localIdleScene
			localIdleScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(data.private.iNetSceneID)
			
			IF IS_BIT_SET_ENUM(data.private.iBSGeneral, PSGB_BLOCK_FIRST_PERSON)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			ENDIF
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(localIdleScene)
				IF GET_SYNCHRONIZED_SCENE_PHASE(localIdleScene) >= 1 // Has the previous finished yet?
					IF data.private.animationState.eDictBase = ANIM_AMB_JACUZZI
						IF IS_BIT_SET_ENUM(data.private.iBSGeneral, PSGB_BLOCK_FIRST_PERSON)
							IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_THIRD_PERSON_MEDIUM
								SET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT, CAM_VIEW_MODE_THIRD_PERSON_MEDIUM)
								SET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT, CAM_VIEW_MODE_FIRST_PERSON)
								CDEBUG1LN(DEBUG_SAFEHOUSE, "JAC_RUN_ENTER_ANIM: setting camera view mode to first person")
							ENDIF
							
							CLEAR_BIT_ENUM(data.private.iBSGeneral, PSGB_BLOCK_FIRST_PERSON)
						ENDIF
					ENDIF
					
					GENERATE_AND_STORE_NEXT_ANIM(data)
					
					CONSTRUCT_ANIM_DICT(data, sAnimDict)
					CONSTRUCT_ANIM_CLIP(data, sAnimClip)
					
					data.private.iNetSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(GET_ANIM_POS(data), GET_ANIM_ROT(data), DEFAULT, TRUE, FALSE, DEFAULT, DEFAULT, 1.12)
					
					NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), data.private.iNetSceneID, sAnimDict, sAnimClip, SLOW_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_ON_ABORT_STOP_SCENE)
					
					NETWORK_START_SYNCHRONISED_SCENE(data.private.iNetSceneID)
					
					//Need to make sure we don't head straight to SET_SEAT_STATE(data, SS_CLEANUP) with any delays before next anim
					SET_BIT_ENUM(data.private.iBSGeneral, PSGB_WAITING_FOR_NEXT_DICTIONARY)
					SET_BIT_ENUM(data.private.iBSGeneraL, PSGB_ENTRY_COMPLETE)
				ELSE
					CLEAR_BIT_ENUM(data.private.iBSGeneral, PSGB_WAITING_FOR_NEXT_DICTIONARY)
				ENDIF
			ELSE
				//@@Maybe there should be a timeout timer on this.
				IF NOT IS_BIT_SET_ENUM(data.private.iBSGeneral, PSGB_WAITING_FOR_NEXT_DICTIONARY)
					PRINTLN("[MUSIC_STUDIO_INT_SEATS] MAINTAIN_SEATS_ACTIVITY : Not waiting for dictionary and no scene running.")
					SET_SEAT_STATE(data, SS_CLEANUP)
				ENDIF
			ENDIF
		BREAK
		
		CASE SS_ANIM_INTERUPT
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[MUSIC_STUDIO_INT_SEATS] MAINTAIN_SEATS_ACTIVITY : NETWORK_STOP_SYNCHRONISED_SCENE")
			
			NETWORK_STOP_SYNCHRONISED_SCENE(data.private.iNetSceneID)
			
			SET_SEAT_STATE(data, SS_CLEANUP)
		BREAK
		
		CASE SS_CLEANUP
			INT localScene
			localScene	= NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(data.private.iNetSceneID)
			
			SCRIPTTASKSTATUS eExitSceneStatus
			eExitSceneStatus = GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_SYNCHRONIZED_SCENE)
			
			IF eExitSceneStatus = WAITING_TO_START_TASK
			OR eExitSceneStatus = PERFORMING_TASK
				IF IS_SYNCHRONIZED_SCENE_RUNNING(localScene)
					IF GET_SYNCHRONIZED_SCENE_PHASE(localScene) >= 0.7
					OR HAS_ANIM_EVENT_FIRED (PLAYER_PED_ID(), GET_HASH_KEY("BREAKOUT_FINISH"))
					OR HAS_ANIM_EVENT_FIRED (PLAYER_PED_ID(), GET_HASH_KEY("BREAK_OUT"))
						#IF IS_DEBUG_BUILD
						IF HAS_ANIM_EVENT_FIRED (PLAYER_PED_ID(), GET_HASH_KEY("BREAKOUT_FINISH"))
							PRINTLN("[MUSIC_STUDIO_INT_SEATS] MAINTAIN_SEATS_ACTIVITY: Finishing anim early as BREAKOUT_FINISH anim event fired")
						ENDIF
						
						IF HAS_ANIM_EVENT_FIRED (PLAYER_PED_ID(), GET_HASH_KEY("BREAK_OUT"))
							PRINTLN("[MUSIC_STUDIO_INT_SEATS] MAINTAIN_SEATS_ACTIVITY: Finishing anim early as BREAK_OUT anim event fired")
						ENDIF
						#ENDIF
						
						PRINTLN("[MUSIC_STUDIO_INT_SEATS] MAINTAIN_SEATS_ACTIVITY: NETWORK_STOP_SYNCHRONISED_SCENE")
						
						NETWORK_STOP_SYNCHRONISED_SCENE(data.private.iNetSceneID)
						
						SET_PLAYER_USING_OFFICE_SEATID(-1)
						
						CLEAR_SEATS_HELP_TEXT(data)
						
						SET_SEAT_STATE(data, SS_INIT)
					ENDIF
				ENDIF
			ELSE
				PRINTLN("[MUSIC_STUDIO_INT_SEATS] MAINTAIN_SEATS_ACTIVITY: Sync scene task status: ", ENUM_TO_INT(eExitSceneStatus))
				PRINTLN("[MUSIC_STUDIO_INT_SEATS] MAINTAIN_SEATS_ACTIVITY: Scene isn't running, cleanup anyway")
				SET_PLAYER_USING_OFFICE_SEATID(-1)
				CLEAR_SEATS_HELP_TEXT(data)
				SET_SEAT_STATE(data, SS_INIT)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC
