
USING "globals.sch"
USING "rage_builtins.sch"
USING "fm_content_includes.sch"

//----------------------
//	CONSTANTS
//----------------------
CONST_INT MAX_NUM_PEDS						30
CONST_INT MAX_NUM_VEHICLES					30
CONST_INT MAX_NUM_PROPS						30

CONST_INT MAX_NUM_STATES					11
CONST_INT MAX_NUM_CLIENT_STATES				12
CONST_INT MAX_NUM_TRANSITIONS				7

CONST_INT MAX_NUM_MISSION_INTERIORS			1
CONST_INT MAX_NUM_PATROLS					8
CONST_INT MAX_NUM_PATROL_NODES				4
CONST_INT MAX_NUM_TRIGGER_AREAS				10
CONST_INT MAX_NUM_PED_GROUP_TRIGGERS		10
CONST_INT MAX_NUM_PED_TRIGGERS				10
CONST_INT MAX_NUM_PED_GROUPS				2
CONST_INT MAX_NUM_GOTO_LOCATIONS			1
CONST_INT MAX_NUM_INTERACT_LOCATIONS		3
CONST_INT MAX_NUM_TARGETS					15
CONST_INT MAX_NUM_PICKUPS					1
CONST_INT MAX_NUM_ATTACH_PROPS				3
CONST_INT MAX_NUM_TAGS						20
CONST_INT MAX_NUM_MISSION_PORTALS			6
CONST_INT MAX_NUM_PORTAL_WARP_COORDS		8
CONST_INT MAX_NUM_LEAVE_AREAS				1
CONST_INT MAX_NUM_DIALOGUE					1
CONST_INT MAX_NUM_AMBUSH_VEHICLES			6
CONST_INT MAX_NUM_PED_BEHAVIOURS			6
CONST_INT MAX_NUM_CUSTOM_SPAWNS				12
CONST_INT MAX_NUM_HELP_TEXTS				5
CONST_INT MAX_NUM_PED_TASKS					7
CONST_INT MAX_NUM_SEARCH_AREAS				4
CONST_INT MAX_NUM_SCENARIOS					26
CONST_INT MAX_NUM_TEXT_MESSAGES				2
CONST_INT MAX_NUM_CUTSCENES					2
CONST_INT MAX_NUM_CUTSCENE_PLAYERS			1
CONST_INT MAX_NUM_CUTSCENE_CAMERAS			1
CONST_INT MAX_NUM_COLLISION_TESTS			4
CONST_INT MAX_NUM_BOTTOM_RIGHT_HUD			1
CONST_INT MAX_NUM_FLARES					1
CONST_INT MAX_NUM_MUSIC_TRIGGERS			4
CONST_INT MAX_NUM_POP_BLOCKERS				5
CONST_INT MAX_NUM_FOCUS_CAMS				1

CONST_INT MAX_NUM_NAVMESH_BLOCKING_OBJECTS	2	// Not hooked up to anything other than local array currently

CONST_INT ENABLE_TAIL_ENTITY				1	

CONST_INT ENABLE_LOSE_THE_COPS				1

CONST_INT FIXER_OPENING_DIALOGUE_DELAY		6000

CONST_INT MAX_NUM_DECALS					10

CONST_INT SECURITY_KILL_TARGET_RP			250

CONST_INT MAX_CASH_DRIER_SOUNDS				2

CONST_INT MAX_NUM_MOLOTOV_TARGETS			4

//----------------------
//	SERVER VARIABLES
//----------------------

STRUCT MISSION_SPECIFIC_SERVER_DATA

	FIXER_SECURITY_VARIATION eMissionVariation = FSV_INVALID
	FIXER_SECURITY_SUBVARIATION eMissionSubvariation = FSS_INVALID
	INT iRandom = -1
	INT iRandom2 = -1
	INT iPlayerToFollow = -1
	INT iMolotovTarget[MAX_NUM_MOLOTOV_TARGETS]
	INT iTrafficReductionZone = -1
	INT iAutoDestroyPropTime
	INT iWinnerCash
	SCRIPT_TIMER tTimeDoorOpened
	SCRIPT_TIMER stGenericTimer
	SCRIPT_TIMER stSpawnAmbushTimer
	SCRIPT_TIMER stMonkeyTimer
	SCRIPT_TIMER stAlarmTimer
	SCRIPT_TIMER stTargetSelectionTimeout[MAX_NUM_MOLOTOV_TARGETS]
	SCRIPT_TIMER stAutoDestroyPropTimer
	SCRIPT_TIMER stHoldEnd
	SCRIPT_TIMER stSecondAmbush
	
	BOOL bBossOwnsHangar

ENDSTRUCT

//----------------------
//	LOCAL VARIABLES
//----------------------

STRUCT DECALS_STRUCT
	INT iDecalBitSet
	INT iDecalCreationStagger
	DECAL_ID decalId[MAX_NUM_DECALS]
ENDSTRUCT

STRUCT MISSION_SPECIFIC_LOCAL_VARIABLES
	
	SCRIPT_TIMER stMissionTimer
	INT iPedHealthBitset
	INT iPedRelBitset
	INT iTickerBitset
	INT iFlashHudBitset
	BLIP_INDEX GenericBlip
	BLIP_INDEX InteriorBlip
	INTERIOR_INSTANCE_INDEX InteriorIndex
	
	SCRIPT_TIMER stDialogueStart
	SCRIPT_TIMER stGeneralTimer
	
	DECALS_STRUCT sDecals
	
	INT iSoundId[3]
	
	INT iCashDrierSoundId[MAX_CASH_DRIER_SOUNDS]
	INT iNumCashDrierSounds
	
	INT iGenericSet = -1
	
	INT iNavMeshBlockingObjectIDs[MAX_NUM_NAVMESH_BLOCKING_OBJECTS]
	
	INT iAiDialogueDelay
	SCRIPT_TIMER stAiDialogueTriggered
	
ENDSTRUCT

MISSION_SPECIFIC_LOCAL_VARIABLES sLocalVariables

//----------------------
//	MISSION VARIABLES
//----------------------

STRUCT MISSION_SPECIFIC_CLIENT_DATA
	INT bWarpedOutOfGarageState = -1
ENDSTRUCT

//----------------------
//	BITSET WRAPPERS
//----------------------

ENUM eMISSION_SERVER_BITSET
	eMISSIONSERVERBITSET_MISSION_ENTITY_LOCATED,
	eMISSIONSERVERBITSET_SAFE_BOX_RANDOM_POSITION_SET,
	eMISSIONSERVERBITSET_SAFE_CODES_RANDOM_POSITION_SET,
	eMISSIONSERVERBITSET_MISSION_ENTITY_REPOSITIONED,
	eMISSIONSERVERBITSET_SAFE_CODES_CREATED,
	eMISSIONSERVERBITSET_TARGETS_CREATED,
	eMISSIONSERVERBITSET_SAFE_LOCATED,
	eMISSIONSERVERBITSET_CODES_LOCATED,
	eMISSIONSERVERBITSET_PEDS_ALERTED,
	eMISSIONSERVERBITSET_BUTTON_FOUND,
	eMISSIONSERVERBITSET_IMANI_TEXT_RECEIVED,
	eMISSIONSERVERBITSET_MISSION_ENTITY_COLLECTED_TICKER,
	eMISSIONSERVERBITSET_CLIENT_COLLECTED_AT_LEAST_ONCE,
	eMISSIONSERVERBITSET_CLIENT_LOCATED,
	eMISSIONSERVERBITSET_TARGET_LOCATED_0,
	eMISSIONSERVERBITSET_TARGET_LOCATED_1,
	eMISSIONSERVERBITSET_TARGET_LOCATED_2,
	eMISSIONSERVERBITSET_TARGET_LOCATED_3,
	eMISSIONSERVERBITSET_TARGET_KILLED_0,
	eMISSIONSERVERBITSET_TARGET_KILLED_1,
	eMISSIONSERVERBITSET_TARGET_KILLED_2,
	eMISSIONSERVERBITSET_TARGET_KILLED_3,
	eMISSIONSERVERBITSET_SHOT_FLARE,
	eMISSIONSERVERBITSET_TRIGGER_AMBUSH,
	eMISSIONSERVERBITSET_ANIM_PACKAGE_COLLECTED,
	eMISSIONSERVERBITSET_FAILED_STEALTH,
	eMISSIONSERVERBITSET_SPAWNED_PARTICLE_EFFECT,
	eMISSIONSERVERBITSET_PLAYER_ENTERED_INTERIOR,
	eMISSIONSERVERBITSET_CUTSCENE_SET,
	eMISSIONSERVERBITSET_EXPLOSION,
	eMISSIONSERVERBITSET_HANGAR_BIKE_1_SET,
	eMISSIONSERVERBITSET_HANGAR_BIKE_2_SET,
	eMISSIONSERVERBITSET_INTERIOR_IS_READY,
	eMISSIONSERVERBITSET_AMBUSH_STARTED,

	eMISSIONSERVERBITSET_END
ENDENUM

ENUM eMISSION_CLIENT_BITSET
	eMISSIONCLIENTBITSET_MISSION_ENTITY_LOCATED,
	eMISSIONCLIENTBITSET_HAS_ENTERED_INTERIOR,
	eMISSIONCLIENTBITSET_SAFE_BOX_RANDOM_POSITION_SET,
	eMISSIONCLIENTBITSET_SAFE_CODES_RANDOM_POSITION_SET,
	eMISSIONCLIENTBITSET_MISSION_ENTITY_REPOSITIONED,
	eMISSIONCLIENTBITSET_SAFE_CODES_CREATED,
	eMISSIONCLIENTBITSET_COMBAT_POSE_SET,
	eMISSIONCLIENTBITSET_TARGETS_CREATED,
	eMISSIONCLIENTBITSET_IMANI_TEXT_RECEIVED,
	eMISSIONCLIENTBITSET_SAFE_LOCATED,
	eMISSIONCLIENTBITSET_CODES_LOCATED,
	eMISSIONCLIENTBITSET_MISSION_ENTITY_COLLECTED,
	eMISSIONCLIENTBITSET_PLAYER_ENTERED_INTERIOR,
	eMISSIONCLIENTBITSET_PEDS_ALERTED,
	eMISSIONCLIENTBITSET_KLAXON_PLAYING,
	eMISSIONCLIENTBITSET_BACK_DOOR_OPENED,
	eMISSIONCLIENTBITSET_BUTTON_FOUND,
	eMISSIONCLIENTBITSET_MISSION_ENTITY_COLLECTED_TICKER,
	eMISSIONCLIENTBITSET_CLIENT_COLLECTED_AT_LEAST_ONCE,
	eMISSIONCLIENTBITSET_CLIENT_LOCATED,
	eMISSIONCLIENTBITSET_TARGET_LOCATED_0,
	eMISSIONCLIENTBITSET_TARGET_LOCATED_1,
	eMISSIONCLIENTBITSET_TARGET_LOCATED_2,
	eMISSIONCLIENTBITSET_TARGET_LOCATED_3,
	eMISSIONCLIENTBITSET_TARGET_KILLED_0,
	eMISSIONCLIENTBITSET_TARGET_KILLED_1,
	eMISSIONCLIENTBITSET_TARGET_KILLED_2,
	eMISSIONCLIENTBITSET_TARGET_KILLED_3,
	eMISSIONCLIENTBITSET_SHOT_FLARE,
	eMISSIONCLIENTBITSET_TRIGGER_AMBUSH,
	eMISSIONCLIENTBITSET_ANIM_PACKAGE_COLLECTED,
	eMISSIONCLIENTBITSET_FAILED_STEALTH,
	eMISSIONCLIENTBITSET_SAFE_DECAL_STATE_SET,
	eMISSIONCLIENTBITSET_SPAWNED_PARTICLE_EFFECT,
	eMISSIONCLIENTBITSET_CUTSCENE_SET,
	eMISSIONCLIENTBITSET_HANGAR_BIKE_1_SET,
	eMISSIONCLIENTBITSET_HANGAR_BIKE_2_SET,
	eMISSIONCLIENTBITSET_INTERIOR_IS_READY,
	
	eMISSIONCLIENTBITSET_END
ENDENUM

ENUM eMISSION_LOCAL_BITSET	
	eMISSIONLOCALBITSET_USE_DIALOGUE_START_DELAY,
	eMISSIONLOCALBITSET_DONE_FORCED_FOCUS,
	eMISSIONLOCALBITSET_GARAGE_DOOR_LOOP_PLAYING,
	eMISSIONLOCALBITSET_DOOR_SOUND_TRIGGERED,
	eMISSIONLOCALBITSET_SAFEHANDLE_SOUND_TRIGGERED,
	eMISSIONLOCALBITSET_SAFEUNLOCKED_SOUND_TRIGGERED,
	eMISSIONLOCALBITSET_SAFEOPEN_SOUND_TRIGGERED,
	eMISSIONLOCALBITSET_CREATED_INTERIOR_NAVMESH_BLOCKS,
	eMISSIONLOCALBITSET_PICKUP_SOUND_TRIGGERED,
	eMISSIONLOCALBITSET_BUTTON_SOUND_TRIGGERED,
	eMISSIONLOCALBITSET_DONE_COLLECT_VIP_BLIP_FLASH,
	eMISSIONLOCALBITSET_DONE_DIALOGUE_PLAYER_NEAR,
	eMISSIONLOCALBITSET_DONE_DIALOGUE_SHOOTOUT_END,
	eMISSIONLOCALBITSET_DONE_DIALOGUE_AMBUSH,
	eMISSIONLOCALBITSET_DONE_DIALOGUE_DROP_OFF,
	
	eMISSIONLOCALBITSET_END
ENDENUM

#IF IS_DEBUG_BUILD
FUNC STRING GET_MISSION_SERVER_BIT_NAME(eMISSION_SERVER_BITSET eBit)
	SWITCH eBit
		CASE eMISSIONSERVERBITSET_MISSION_ENTITY_LOCATED				RETURN "eMISSIONSERVERBITSET_MISSION_ENTITY_LOCATED"
		CASE eMISSIONSERVERBITSET_SAFE_BOX_RANDOM_POSITION_SET			RETURN "eMISSIONSERVERBITSET_SAFE_BOX_RANDOM_POSITION_SET"
		CASE eMISSIONSERVERBITSET_SAFE_CODES_RANDOM_POSITION_SET		RETURN "eMISSIONSERVERBITSET_SAFE_CODES_RANDOM_POSITION_SET"
		CASE eMISSIONSERVERBITSET_MISSION_ENTITY_REPOSITIONED			RETURN "eMISSIONSERVERBITSET_MISSION_ENTITY_REPOSITIONED"
		CASE eMISSIONSERVERBITSET_SAFE_CODES_CREATED					RETURN "eMISSIONSERVERBITSET_SAFE_CODES_CREATED"
		CASE eMISSIONSERVERBITSET_TARGETS_CREATED						RETURN "eMISSIONSERVERBITSET_TARGETS_CREATED"
		CASE eMISSIONSERVERBITSET_SAFE_LOCATED							RETURN "eMISSIONSERVERBITSET_SAFE_LOCATED"
		CASE eMISSIONSERVERBITSET_CODES_LOCATED							RETURN "eMISSIONSERVERBITSET_CODES_LOCATED"
		CASE eMISSIONSERVERBITSET_PEDS_ALERTED							RETURN "eMISSIONSERVERBITSET_PEDS_ALERTED"
		CASE eMISSIONSERVERBITSET_BUTTON_FOUND							RETURN "eMISSIONSERVERBITSET_BUTTON_FOUND"
		CASE eMISSIONSERVERBITSET_IMANI_TEXT_RECEIVED					RETURN "eMISSIONSERVERBITSET_IMANI_TEXT_RECEIVED"
		CASE eMISSIONSERVERBITSET_MISSION_ENTITY_COLLECTED_TICKER		RETURN "eMISSIONSERVERBITSET_MISSION_ENTITY_COLLECTED_TICKER"
		CASE eMISSIONSERVERBITSET_CLIENT_COLLECTED_AT_LEAST_ONCE		RETURN "eMISSIONSERVERBITSET_CLIENT_COLLECTED_AT_LEAST_ONCE"
		CASE eMISSIONSERVERBITSET_CLIENT_LOCATED						RETURN "eMISSIONSERVERBITSET_CLIENT_LOCATED"
		CASE eMISSIONSERVERBITSET_SHOT_FLARE							RETURN "eMISSIONSERVERBITSET_SHOT_FLARE"
		CASE eMISSIONSERVERBITSET_TRIGGER_AMBUSH						RETURN "eMISSIONSERVERBITSET_TRIGGER_AMBUSH"
		CASE eMISSIONSERVERBITSET_TARGET_KILLED_0						RETURN "eMISSIONSERVERBITSET_TARGET_KILLED_0"
		CASE eMISSIONSERVERBITSET_TARGET_KILLED_1						RETURN "eMISSIONSERVERBITSET_TARGET_KILLED_1"
		CASE eMISSIONSERVERBITSET_TARGET_KILLED_2						RETURN "eMISSIONSERVERBITSET_TARGET_KILLED_2"
		CASE eMISSIONSERVERBITSET_TARGET_KILLED_3						RETURN "eMISSIONSERVERBITSET_TARGET_KILLED_3"
		CASE eMISSIONSERVERBITSET_ANIM_PACKAGE_COLLECTED				RETURN "eMISSIONSERVERBITSET_ANIM_PACKAGE_COLLECTED"
		CASE eMISSIONSERVERBITSET_FAILED_STEALTH						RETURN "eMISSIONSERVERBITSET_FAILED_STEALTH"
		CASE eMISSIONSERVERBITSET_SPAWNED_PARTICLE_EFFECT				RETURN "eMISSIONSERVERBITSET_SPAWNED_PARTICLE_EFFECT"
		CASE eMISSIONSERVERBITSET_PLAYER_ENTERED_INTERIOR				RETURN "eMISSIONSERVERBITSET_PLAYER_ENTERED_INTERIOR"
		CASE eMISSIONSERVERBITSET_CUTSCENE_SET							RETURN "eMISSIONSERVERBITSET_CUTSCENE_SET"
		CASE eMISSIONSERVERBITSET_EXPLOSION								RETURN "eMISSIONSERVERBITSET_EXPLOSION"
		CASE eMISSIONSERVERBITSET_HANGAR_BIKE_1_SET						RETURN "eMISSIONSERVERBITSET_HANGAR_BIKE_1_SET"
		CASE eMISSIONSERVERBITSET_HANGAR_BIKE_2_SET						RETURN "eMISSIONSERVERBITSET_HANGAR_BIKE_2_SET"
		CASE eMISSIONSERVERBITSET_INTERIOR_IS_READY						RETURN "eMISSIONSERVERBITSET_INTERIOR_IS_READY"
		
		CASE eMISSIONSERVERBITSET_END									RETURN "eMISSIONSERVERBITSET_END"
	ENDSWITCH
	
	RETURN "***INVALID***"
ENDFUNC

FUNC STRING GET_MISSION_CLIENT_BIT_NAME(eMISSION_CLIENT_BITSET eBit)
	SWITCH eBit
		CASE eMISSIONCLIENTBITSET_MISSION_ENTITY_LOCATED				RETURN "eMISSIONCLIENTBITSET_MISSION_ENTITY_LOCATED"
		CASE eMISSIONCLIENTBITSET_HAS_ENTERED_INTERIOR					RETURN "eMISSIONCLIENTBITSET_HAS_ENTERED_INTERIOR"
		CASE eMISSIONCLIENTBITSET_SAFE_BOX_RANDOM_POSITION_SET			RETURN "eMISSIONCLIENTBITSET_SAFE_BOX_RANDOM_POSITION_SET"
		CASE eMISSIONCLIENTBITSET_SAFE_CODES_RANDOM_POSITION_SET		RETURN "eMISSIONCLIENTBITSET_SAFE_CODES_RANDOM_POSITION_SET"
		CASE eMISSIONCLIENTBITSET_MISSION_ENTITY_REPOSITIONED			RETURN "eMISSIONCLIENTBITSET_MISSION_ENTITY_REPOSITIONED"
		CASE eMISSIONCLIENTBITSET_SAFE_CODES_CREATED					RETURN "eMISSIONCLIENTBITSET_SAFE_CODES_CREATED"
		CASE eMISSIONCLIENTBITSET_COMBAT_POSE_SET						RETURN "eMISSIONCLIENTBITSET_COMBAT_POSE_SET"
		CASE eMISSIONCLIENTBITSET_TARGETS_CREATED						RETURN "eMISSIONCLIENTBITSET_TARGETS_CREATED"
		CASE eMISSIONCLIENTBITSET_IMANI_TEXT_RECEIVED					RETURN "eMISSIONCLIENTBITSET_IMANI_TEXT_RECEIVED"
		CASE eMISSIONCLIENTBITSET_SAFE_LOCATED							RETURN "eMISSIONCLIENTBITSET_SAFE_LOCATED"
		CASE eMISSIONCLIENTBITSET_CODES_LOCATED							RETURN "eMISSIONCLIENTBITSET_CODES_LOCATED"
		CASE eMISSIONCLIENTBITSET_MISSION_ENTITY_COLLECTED				RETURN "eMISSIONCLIENTBITSET_MISSION_ENTITY_COLLECTED"
		CASE eMISSIONCLIENTBITSET_PLAYER_ENTERED_INTERIOR				RETURN "eMISSIONCLIENTBITSET_PLAYER_ENTERED_INTERIOR"
		CASE eMISSIONCLIENTBITSET_PEDS_ALERTED							RETURN "eMISSIONCLIENTBITSET_PEDS_ALERTED"
		CASE eMISSIONCLIENTBITSET_KLAXON_PLAYING						RETURN "eMISSIONCLIENTBITSET_KLAXON_PLAYING"
		CASE eMISSIONCLIENTBITSET_BACK_DOOR_OPENED						RETURN "eMISSIONCLIENTBITSET_BACK_DOOR_OPENED"
		CASE eMISSIONCLIENTBITSET_BUTTON_FOUND							RETURN "eMISSIONCLIENTBITSET_BUTTON_FOUND"
		CASE eMISSIONCLIENTBITSET_MISSION_ENTITY_COLLECTED_TICKER		RETURN "eMISSIONCLIENTBITSET_MISSION_ENTITY_COLLECTED_TICKER"
		CASE eMISSIONCLIENTBITSET_CLIENT_COLLECTED_AT_LEAST_ONCE		RETURN "eMISSIONCLIENTBITSET_CLIENT_COLLECTED_AT_LEAST_ONCE"
		CASE eMISSIONCLIENTBITSET_CLIENT_LOCATED						RETURN "eMISSIONCLIENTBITSET_CLIENT_LOCATED"
		CASE eMISSIONCLIENTBITSET_SHOT_FLARE							RETURN "eMISSIONCLIENTBITSET_SHOT_FLARE"
		CASE eMISSIONCLIENTBITSET_TRIGGER_AMBUSH						RETURN "eMISSIONCLIENTBITSET_TRIGGER_AMBUSH"
		CASE eMISSIONCLIENTBITSET_TARGET_KILLED_0						RETURN "eMISSIONCLIENTBITSET_TARGET_KILLED_0"
		CASE eMISSIONCLIENTBITSET_TARGET_KILLED_1						RETURN "eMISSIONCLIENTBITSET_TARGET_KILLED_1"
		CASE eMISSIONCLIENTBITSET_TARGET_KILLED_2						RETURN "eMISSIONCLIENTBITSET_TARGET_KILLED_2"
		CASE eMISSIONCLIENTBITSET_TARGET_KILLED_3						RETURN "eMISSIONCLIENTBITSET_TARGET_KILLED_3"
		CASE eMISSIONCLIENTBITSET_ANIM_PACKAGE_COLLECTED				RETURN "eMISSIONCLIENTBITSET_ANIM_PACKAGE_COLLECTED"
		CASE eMISSIONCLIENTBITSET_FAILED_STEALTH						RETURN "eMISSIONCLIENTBITSET_FAILED_STEALTH"
		CASE eMISSIONCLIENTBITSET_SPAWNED_PARTICLE_EFFECT				RETURN "eMISSIONCLIENTBITSET_SPAWNED_PARTICLE_EFFECT"
		CASE eMISSIONCLIENTBITSET_CUTSCENE_SET							RETURN "eMISSIONCLIENTBITSET_CUTSCENE_SET"
		CASE eMISSIONCLIENTBITSET_HANGAR_BIKE_1_SET						RETURN "eMISSIONCLIENTBITSET_HANGAR_BIKE_1_SET"
		CASE eMISSIONCLIENTBITSET_HANGAR_BIKE_2_SET						RETURN "eMISSIONCLIENTBITSET_HANGAR_BIKE_2_SET"
		CASE eMISSIONCLIENTBITSET_INTERIOR_IS_READY						RETURN "eMISSIONCLIENTBITSET_INTERIOR_IS_READY"
		
		CASE eMISSIONCLIENTBITSET_END									RETURN "eMISSIONCLIENTBITSET_END"
	ENDSWITCH
	
	RETURN "***INVALID***"
ENDFUNC

FUNC STRING GET_MISSION_LOCAL_BIT_NAME(eMISSION_LOCAL_BITSET eBit)
	SWITCH eBit
		CASE eMISSIONLOCALBITSET_USE_DIALOGUE_START_DELAY				RETURN "eMISSIONLOCALBITSET_USE_DIALOGUE_START_DELAY"
		CASE eMISSIONLOCALBITSET_DONE_FORCED_FOCUS						RETURN "eMISSIONLOCALBITSET_DONE_FORCED_FOCUS"
		CASE eMISSIONLOCALBITSET_GARAGE_DOOR_LOOP_PLAYING				RETURN "eMISSIONLOCALBITSET_GARAGE_DOOR_LOOP_PLAYING"
		CASE eMISSIONLOCALBITSET_DOOR_SOUND_TRIGGERED					RETURN "eMISSIONLOCALBITSET_DOOR_SOUND_TRIGGERED"
		CASE eMISSIONLOCALBITSET_SAFEHANDLE_SOUND_TRIGGERED				RETURN "eMISSIONLOCALBITSET_SAFEHANDLE_SOUND_TRIGGERED"
		CASE eMISSIONLOCALBITSET_SAFEUNLOCKED_SOUND_TRIGGERED			RETURN "eMISSIONLOCALBITSET_SAFEUNLOCKED_SOUND_TRIGGERED"
		CASE eMISSIONLOCALBITSET_SAFEOPEN_SOUND_TRIGGERED				RETURN "eMISSIONLOCALBITSET_SAFEOPEN_SOUND_TRIGGERED"
		CASE eMISSIONLOCALBITSET_PICKUP_SOUND_TRIGGERED					RETURN "eMISSIONLOCALBITSET_PICKUP_SOUND_TRIGGERED"
		CASE eMISSIONLOCALBITSET_DONE_DIALOGUE_PLAYER_NEAR				RETURN "eMISSIONLOCALBITSET_DONE_DIALOGUE_PLAYER_NEAR"
		CASE eMISSIONLOCALBITSET_DONE_DIALOGUE_SHOOTOUT_END				RETURN "eMISSIONLOCALBITSET_DONE_DIALOGUE_SHOOTOUT_END"
		CASE eMISSIONLOCALBITSET_DONE_DIALOGUE_AMBUSH					RETURN "eMISSIONLOCALBITSET_DONE_DIALOGUE_AMBUSH"
		CASE eMISSIONLOCALBITSET_DONE_DIALOGUE_DROP_OFF					RETURN "eMISSIONLOCALBITSET_DONE_DIALOGUE_DROP_OFF"
	
		CASE eMISSIONLOCALBITSET_END									RETURN "eMISSIONLOCALBITSET_END"
	ENDSWITCH
	
	RETURN "***INVALID***"
ENDFUNC

#ENDIF

FUNC eMISSION_SERVER_BITSET GET_MISSION_SERVER_BIT_FOR_CLIENT_BIT(eMISSION_CLIENT_BITSET eClientBit)
	SWITCH eClientBit
		CASE eMISSIONCLIENTBITSET_MISSION_ENTITY_LOCATED					RETURN eMISSIONSERVERBITSET_MISSION_ENTITY_LOCATED
		CASE eMISSIONCLIENTBITSET_SAFE_BOX_RANDOM_POSITION_SET				RETURN eMISSIONSERVERBITSET_SAFE_BOX_RANDOM_POSITION_SET
		CASE eMISSIONCLIENTBITSET_SAFE_CODES_RANDOM_POSITION_SET			RETURN eMISSIONSERVERBITSET_SAFE_CODES_RANDOM_POSITION_SET
		CASE eMISSIONCLIENTBITSET_MISSION_ENTITY_REPOSITIONED				RETURN eMISSIONSERVERBITSET_MISSION_ENTITY_REPOSITIONED
		CASE eMISSIONCLIENTBITSET_SAFE_CODES_CREATED						RETURN eMISSIONSERVERBITSET_SAFE_CODES_CREATED
		CASE eMISSIONCLIENTBITSET_TARGETS_CREATED							RETURN eMISSIONSERVERBITSET_TARGETS_CREATED
		CASE eMISSIONCLIENTBITSET_SAFE_LOCATED								RETURN eMISSIONSERVERBITSET_SAFE_LOCATED
		CASE eMISSIONCLIENTBITSET_CODES_LOCATED								RETURN eMISSIONSERVERBITSET_CODES_LOCATED
		CASE eMISSIONCLIENTBITSET_PEDS_ALERTED								RETURN eMISSIONSERVERBITSET_PEDS_ALERTED
		CASE eMISSIONCLIENTBITSET_BUTTON_FOUND								RETURN eMISSIONSERVERBITSET_BUTTON_FOUND
		CASE eMISSIONCLIENTBITSET_IMANI_TEXT_RECEIVED						RETURN eMISSIONSERVERBITSET_IMANI_TEXT_RECEIVED
		CASE eMISSIONCLIENTBITSET_MISSION_ENTITY_COLLECTED_TICKER			RETURN eMISSIONSERVERBITSET_MISSION_ENTITY_COLLECTED_TICKER
		CASE eMISSIONCLIENTBITSET_CLIENT_COLLECTED_AT_LEAST_ONCE			RETURN eMISSIONSERVERBITSET_CLIENT_COLLECTED_AT_LEAST_ONCE
		CASE eMISSIONCLIENTBITSET_CLIENT_LOCATED							RETURN eMISSIONSERVERBITSET_CLIENT_LOCATED
		CASE eMISSIONCLIENTBITSET_TARGET_LOCATED_0							RETURN eMISSIONSERVERBITSET_TARGET_LOCATED_0
		CASE eMISSIONCLIENTBITSET_TARGET_LOCATED_1							RETURN eMISSIONSERVERBITSET_TARGET_LOCATED_1
		CASE eMISSIONCLIENTBITSET_TARGET_LOCATED_2							RETURN eMISSIONSERVERBITSET_TARGET_LOCATED_2
		CASE eMISSIONCLIENTBITSET_TARGET_LOCATED_3							RETURN eMISSIONSERVERBITSET_TARGET_LOCATED_3
		CASE eMISSIONCLIENTBITSET_TARGET_KILLED_0							RETURN eMISSIONSERVERBITSET_TARGET_KILLED_0
		CASE eMISSIONCLIENTBITSET_TARGET_KILLED_1							RETURN eMISSIONSERVERBITSET_TARGET_KILLED_1
		CASE eMISSIONCLIENTBITSET_TARGET_KILLED_2							RETURN eMISSIONSERVERBITSET_TARGET_KILLED_2
		CASE eMISSIONCLIENTBITSET_TARGET_KILLED_3							RETURN eMISSIONSERVERBITSET_TARGET_KILLED_3
		CASE eMISSIONCLIENTBITSET_SHOT_FLARE								RETURN eMISSIONSERVERBITSET_SHOT_FLARE
		CASE eMISSIONCLIENTBITSET_TRIGGER_AMBUSH							RETURN eMISSIONSERVERBITSET_TRIGGER_AMBUSH
		CASE eMISSIONCLIENTBITSET_ANIM_PACKAGE_COLLECTED					RETURN eMISSIONSERVERBITSET_ANIM_PACKAGE_COLLECTED
		CASE eMISSIONCLIENTBITSET_FAILED_STEALTH							RETURN eMISSIONSERVERBITSET_FAILED_STEALTH
		CASE eMISSIONCLIENTBITSET_SPAWNED_PARTICLE_EFFECT					RETURN eMISSIONSERVERBITSET_SPAWNED_PARTICLE_EFFECT
		CASE eMISSIONCLIENTBITSET_PLAYER_ENTERED_INTERIOR					RETURN eMISSIONSERVERBITSET_PLAYER_ENTERED_INTERIOR
		CASE eMISSIONCLIENTBITSET_CUTSCENE_SET								RETURN eMISSIONSERVERBITSET_CUTSCENE_SET
		CASE eMISSIONCLIENTBITSET_HANGAR_BIKE_1_SET							RETURN eMISSIONSERVERBITSET_HANGAR_BIKE_1_SET
		CASE eMISSIONCLIENTBITSET_HANGAR_BIKE_2_SET							RETURN eMISSIONSERVERBITSET_HANGAR_BIKE_2_SET
		CASE eMISSIONCLIENTBITSET_INTERIOR_IS_READY							RETURN eMISSIONSERVERBITSET_INTERIOR_IS_READY
	ENDSWITCH
	
	RETURN eMISSIONSERVERBITSET_END
ENDFUNC

FUNC INT GET_MISSION_RIVAL_TIMER_DELAY_DEFAULT()
	RETURN 10000
ENDFUNC
