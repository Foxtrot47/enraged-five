USING "commands_debug.sch"
USING "script_maths.sch"
USING "rage_builtins.sch"
USING "cutscene_help.sch"
USING "net_simple_interior.sch"

#IF FEATURE_FIXER

CONST_INT MAX_NUM_SEATS_FOR_SITTING_ACTIVITY 31

CONST_INT LOCATE_CHECKS_PER_TICK 5

//Time before a new animation variation is chosen, this should be infrequent.
CONST_INT TIME_BEFORE_ANIM_VAR_CHANGE 90000

//There is a brief period of time while the player isn't allowed to sit down
//that is triggered every time the seat is blocked.
CONST_INT SEAT_BLOCK_TIMER			  		150

CONST_INT CAPSULE_SHRINK_LINGER_NUM_FRAMES	5

TWEAK_FLOAT SITTING_CAPSULE_SIZE 			0.13

ENUM SEAT_TYPE
	SEAT_INVALID
	,SEAT_TYPE_OFFICE_ANIM
	,SEAT_TYPE_CLUBHOUSE_ANIM
	,SEAT_TYPE_BASE_ANIM
	,SEAT_TYPE_JACUZZI_ANIM
	,SEAT_TYPE_RAILING_ANIM
ENDENUM

ENUM SEAT_STATE
	SS_INIT
	,SS_LOCATE_CHECK
	,SS_PROMPT
	,SS_REQUEST_ANIMATION
	,SS_MOVE_TO_ANIM_START
	,SS_ANIM_START_ENTER
	,SS_ANIM_SITTING
	,SS_ANIM_INTERUPT
	,SS_CLEANUP
ENDENUM

STRUCT LOCATE_STRUCT
	FLOAT fWidth = 0.75
	
	VECTOR v0
	VECTOR v1
ENDSTRUCT

STRUCT ANIM_STRUCT
	VECTOR vPos
	VECTOR vRot
ENDSTRUCT

//Randomly selected when player sits down. Specific selection for various chairs.
ENUM SEAT_ANIM_0
	ANIM_AMB_CLUBHOUSE_SEATING
	,ANIM_AMB_OFFICE_SEATING
	,ANIM_AMB_FACILITY_BRIEFING_ROOM_SEATING
	,ANIM_AMB_JACUZZI
	,ANIM_AMB_RAILING_LEAN
ENDENUM

//Randomly selected when player sits down, changes very infreuqently.
ENUM SEAT_ANIM_1
	/* Offfice, Clubhouse, and briefing room variations */
	VAR_A
	,VAR_B
	,VAR_C
	,VAR_D
	,VAR_E
ENDENUM

//Clip is Enter, then ping pongs between Base <-> Idle/Transition then Exit
ENUM SEAT_ANIM_2_CLIP
	/* Clips */
	IDLE_A
	,IDLE_B
	,IDLE_C
	,ENTER
	,LEAVE
	,BASE
	/* This is not a clip. Use as an offset for the transitions below. */
	,TRANSITIONS
	,A_TO_B
	,B_TO_C
	,C_TO_D
	,D_TO_E
	/* This is not a clip. Use as an offset for the transitions below. */
	,TRANSITION_LOOPS
	,B_TO_A	//Unused, but here to maintain consistency in offsets.. Please do not remove.
	,C_TO_A
	,D_TO_A
	,E_TO_A
ENDENUM

STRUCT ANIM_STATE_STRUCT
	INT iVarChangeTime
	
	SEAT_ANIM_0 eDictBase
	SEAT_ANIM_1 eDictVar
	
	SEAT_ANIM_2_CLIP eClip
	SEAT_ANIM_2_CLIP ePrevClip
ENDSTRUCT

STRUCT SEAT_DATA_STRUCT
	ANIM_STRUCT animation
	
	LOCATE_STRUCT entryLocate
	
	SEAT_TYPE eType
ENDSTRUCT

TYPEDEF FUNC BOOL ADDITIONAL_CHECK_BEFORE_PROMPT(SEAT_DATA_STRUCT& in_data, INT iSeatIndex)

TYPEDEF FUNC BOOL USE_ALTERNATIVE_HELP_TEXT(SEAT_DATA_STRUCT& in_data, INT iSeatIndex, TEXT_LABEL_15& out_txtHelp)

STRUCT SEATS_PRIVATE_DATA
	ADDITIONAL_CHECK_BEFORE_PROMPT cCanPlayerUseSeat
	
	ANIM_STATE_STRUCT animationState
	
	INT iContext = NEW_CONTEXT_INTENTION
	INT iBSGeneral = 0
	INT iNetSceneID = -1
	INT iInSeatLocate = 0
	INT iPlayerToCheck = 0
	INT iSeatBlockedTimer = -1
	INT iFrameLastCapsuleChange = -1
	
	SEAT_STATE eState = SS_INIT
	
	USE_ALTERNATIVE_HELP_TEXT cAltHelpExit
	USE_ALTERNATIVE_HELP_TEXT cAltHelpEnter
ENDSTRUCT

#IF IS_DEBUG_BUILD
STRUCT SEATS_DEBUG_DATA
	BOOL bForceAnimVarNow
	
	INT iNextForcedAnimVar
ENDSTRUCT
#ENDIF

STRUCT SEATS_PUBLIC_DATA
	SEAT_DATA_STRUCT seats[MAX_NUM_SEATS_FOR_SITTING_ACTIVITY]
ENDSTRUCT

STRUCT SEATS_LOCAL_DATA
	SEATS_PRIVATE_DATA private
	
	SEATS_PUBLIC_DATA public
	
	#IF IS_DEBUG_BUILD
	SEATS_DEBUG_DATA debug
	#ENDIF
ENDSTRUCT

ENUM FIXER_HQ_INTERIOR_SEATS
	FIXER_HQ_INTERIOR_SEAT_0, 	// Ground Floor - Area 1 (1 individual seats) seat 1
	FIXER_HQ_INTERIOR_SEAT_1, 	// Ground Floor - Area 1 (1 individual seats) seat 2
	
	FIXER_HQ_INTERIOR_SEAT_2, 	// Ground Floor - Area 2 (1 sofa + 1 individual seat) individual seat
	FIXER_HQ_INTERIOR_SEAT_3, 	// Ground Floor - Area 2 (1 sofa + 1 individual seat) sofa left side
	FIXER_HQ_INTERIOR_SEAT_4, 	// Ground Floor - Area 2 (1 sofa + 1 individual seat) sofa middle
	FIXER_HQ_INTERIOR_SEAT_5, 	// Ground Floor - Area 2 (1 sofa + 1 individual seat) sofa right side
	
	FIXER_HQ_INTERIOR_SEAT_6, 	// Office Floor - Franklin's Office (1 sofa + 1 individual seat) sofa left side
	FIXER_HQ_INTERIOR_SEAT_7, 	// Office Floor - Franklin's Office (1 sofa + 1 individual seat) sofa middle
	FIXER_HQ_INTERIOR_SEAT_8, 	// Office Floor - Franklin's Office (1 sofa + 1 individual seat) sofa right side
	FIXER_HQ_INTERIOR_SEAT_9, 	// Office Floor - Franklin's Office (1 sofa + 1 individual seat) individual seat
	
	FIXER_HQ_INTERIOR_SEAT_10, 	// Office Floor - Open area between offices (1 sofa + 1 individual seat) sofa left side
	FIXER_HQ_INTERIOR_SEAT_11, 	// Office Floor - Open area between offices (1 sofa + 1 individual seat) sofa middle
	FIXER_HQ_INTERIOR_SEAT_12, 	// Office Floor - Open area between offices (1 sofa + 1 individual seat) sofa right side
	FIXER_HQ_INTERIOR_SEAT_13, 	// Office Floor - Open area between offices (1 sofa + 1 individual seat) individual seat
	
	FIXER_HQ_INTERIOR_SEAT_14, 	// Office Floor - Player's Office (1 sofa + 1 individual seat) sofa left side
	FIXER_HQ_INTERIOR_SEAT_15, 	// Office Floor - Player's Office (1 sofa + 1 individual seat) sofa middl
	FIXER_HQ_INTERIOR_SEAT_16, 	// Office Floor - Player's Office (1 sofa + 1 individual seat) sofa right side
	FIXER_HQ_INTERIOR_SEAT_17, 	// Office Floor - Player's Office (1 sofa + 1 individual seat) individual seat	
	
	FIXER_HQ_INTERIOR_SEAT_18,	// Office Floor - Railing lean (multiple spots), closest to staircase
	FIXER_HQ_INTERIOR_SEAT_19,	// Office Floor - Railing lean (multiple spots), in front of Franklin's office
	FIXER_HQ_INTERIOR_SEAT_20,	// Office Floor - Railing lean (multiple spots), next to Franklin's office, in front of the adjacent hallway
	FIXER_HQ_INTERIOR_SEAT_21,	// Office Floor - Railing lean (multiple spots), on angled railing, next to seats
	FIXER_HQ_INTERIOR_SEAT_22,	// Office Floor - Railing lean (multiple spots), on angled railing, left of telescope
	FIXER_HQ_INTERIOR_SEAT_23,	// Office Floor - Railing lean (multiple spots), in corner, in front of side door to player's office
	
	FIXER_HQ_INTERIOR_SEAT_24,	// Office Floor - Franklin's Office office chair at desk (not Franklin's)
	
	FIXER_HQ_INTERIOR_SEAT_COUNT
ENDENUM

ENUM FIXER_HQ_INTERIOR_OP_FLOOR_SEATS
	FIXER_HQ_INTERIOR_OP_FLOOR_SEAT_0, 	// Operations Floor - Back corner L-shape sofa - position 1
	FIXER_HQ_INTERIOR_OP_FLOOR_SEAT_1, 	// Operations Floor - Back corner L-shape sofa - position 2
	FIXER_HQ_INTERIOR_OP_FLOOR_SEAT_2, 	// Operations Floor - Back corner L-shape sofa - position 3		
	FIXER_HQ_INTERIOR_SEATS_OP_FLOOR_COUNT
ENDENUM

ENUM FIXER_HQ_INTERIOR_PQ_SEATS
	FIXER_HQ_INTERIOR_PQ_SEAT_0, 	// Personal Quarters - L-shape sofa 1 - position 1
	FIXER_HQ_INTERIOR_PQ_SEAT_1, 	// Personal Quarters - L-shape sofa 1 - position 
	FIXER_HQ_INTERIOR_PQ_SEAT_2, 	// Personal Quarters - L-shape sofa 1 - position 3	
	
	FIXER_HQ_INTERIOR_PQ_SEAT_3, 	// Personal Quarters - L-shape sofa 2 (against back wall) - position 1
	FIXER_HQ_INTERIOR_PQ_SEAT_4, 	// Personal Quarters - L-shape sofa 2 (against back wall) - position 3
	FIXER_HQ_INTERIOR_SEATS_PQ_COUNT
ENDENUM

TWEAK_FLOAT cfFIXER_HQ_INTERIOR_SEAT_LOCATE_WIDTH		1.25
TWEAK_FLOAT cfFIXER_HQ_INTERIOR_RAILING_LOCATE_WIDTH	1.8

PROC GET_FIXER_HQ_INTERIOR_SEAT_WORLD_TRANSFORM(FIXER_HQ_INTERIOR_SEATS eSeat, VECTOR& vPos, FLOAT& fZRot)
	IF IS_PLAYER_FEMALE()
		SWITCH eSeat					
			CASE FIXER_HQ_INTERIOR_SEAT_0		vPos = << 6.75663, -6.16382, 0 >>		fZRot = 325.000000		BREAK	// office - Original world coords in Hawick location vPos = << 376.711, -64.968, 102.363 >>	fZRot = -145.0
			CASE FIXER_HQ_INTERIOR_SEAT_1		vPos = << 6.76825, -8.49972, 0.0 >>		fZRot = 216.919998		BREAK	// clubhouse - Original world coords in Hawick location	<< 374.512, -64.180, 102.363 >>	fZRot = 106.920

			CASE FIXER_HQ_INTERIOR_SEAT_2		vPos = << 2.18689, 2.62849, 0 >>		fZRot = 41.0 			BREAK	// office - Original world coords in Hawick location	<< 386.536, -63.681, 102.363 >>	fZRot = -69.0 
			CASE FIXER_HQ_INTERIOR_SEAT_3		vPos = << 2.99902, -0.187584, 0 >>		fZRot = 134.0 			BREAK	// clubhouse - Original world coords in Hawick location	<< 383.612, -63.481, 102.363 >>	fZRot = 24.1 
			CASE FIXER_HQ_INTERIOR_SEAT_4		vPos = << 2.54896, 0.35308, 0 >>		fZRot = 134.0			BREAK	// office - Original world coords in Hawick location	<< 384.274, -63.243, 102.363 >>	fZRot = 23.9
			CASE FIXER_HQ_INTERIOR_SEAT_5		vPos = << 2.00586, 0.859912, 0 >>		fZRot = 134.0			BREAK	// office - Original world coords in Hawick location	<< 384.936, -62.906, 102.363 >>	fZRot = 24.1 

			CASE FIXER_HQ_INTERIOR_SEAT_6		vPos = << 2.29356, 10.3028, 3.799 >>	fZRot = 266.239990 		BREAK	// office - Original world coords in Hawick location	<<393.711,-66.406, 106.162>>	fZRot = 156.240
			CASE FIXER_HQ_INTERIOR_SEAT_7		vPos = << 2.31938, 9.64707, 3.799 >>	fZRot = 267.239990 		BREAK	// clubhouse - Original world coords in Hawick location	<<393.086,-66.206, 106.162>>	fZRot = 157.240
			CASE FIXER_HQ_INTERIOR_SEAT_8		vPos = << 2.32215, 8.9436, 3.799 >>		fZRot = 265.520020 		BREAK	// clubhouse - Original world coords in Hawick location	<<392.424,-65.968, 106.162>>	fZRot = 155.520 
			CASE FIXER_HQ_INTERIOR_SEAT_9		vPos = << 1.06633, 7.51704, 3.799 >>	fZRot = 210.000000 		BREAK	// office - Original world coords in Hawick location	<< 391.513, -64.300, 106.162 >>	fZRot = 100.0 

			CASE FIXER_HQ_INTERIOR_SEAT_10		vPos = << 2.63443, 0.211807, 3.799 >>	fZRot = 138.000000 		BREAK	// clubhouse - Original world coords in Hawick location	<< 384.112, -63.275, 106.162 >>	fZRot = 28.0 
			CASE FIXER_HQ_INTERIOR_SEAT_11		vPos = << 1.58678, 1.22775, 3.799 >>	fZRot = 134.000000 		BREAK	// office - Original world coords in Hawick location	<< 385.425, -62.638, 106.162 >>	fZRot = 24.0 
			CASE FIXER_HQ_INTERIOR_SEAT_12		vPos = << 2.09373, 0.747148, 3.799 >>	fZRot = 135.000000 		BREAK	// clubhouse - Original world coords in Hawick location	<< 384.800, -62.950, 106.162 >>	fZRot = 25.0 
			CASE FIXER_HQ_INTERIOR_SEAT_13		vPos = << 4.57492, 0.546689, 3.799 >>	fZRot = 210.000000 		BREAK	// office - Original world coords in Hawick location	<< 383.763, -65.213, 106.162 >>	fZRot = 100.0 

			CASE FIXER_HQ_INTERIOR_SEAT_14		vPos = << 3.54098, -4.38113, 3.799 >>	fZRot = 0.8 			BREAK	// clubhouse - Original world coords in Hawick location	<<379.486,-62.556, 106.162>>	fZRot = -109.080
			CASE FIXER_HQ_INTERIOR_SEAT_15		vPos = << 4.21208, -4.36248, 3.799 >>	fZRot = 0.919998 		BREAK	// clubhouse - Original world coords in Hawick location	<<379.274,-63.193, 106.162>>	fZRot = -109.080
			CASE FIXER_HQ_INTERIOR_SEAT_16		vPos = << 4.91477, -4.31954, 3.799 >>	fZRot = 0.8				BREAK	// office - Original world coords in Hawick location	<<379.074,-63.868, 106.162>>	fZRot = -109.080
			CASE FIXER_HQ_INTERIOR_SEAT_17		vPos = << 6.35365, -5.65603, 3.799 >>	fZRot = 300.000000 		BREAK	// office - Original world coords in Hawick location	<< 377.326, -64.763, 106.162 >>	fZRot = -170.0
			
			// It seems like the anims for heeled and non-heeled are the same, but just in case for future changes, set up separately...
			CASE FIXER_HQ_INTERIOR_SEAT_18		
				IF IS_PED_WEARING_HIGH_HEELS(PLAYER_PED_ID())
					vPos = << 6.641, 11.704, 3.800 >>		fZRot = 180.0000			// railing - Original world coords in Vespucci location << -997.270, -747.900, 64.694 >>	fZRot = -180.0
				ELSE
					vPos = << 6.641, 11.704, 3.800 >>		fZRot = 180.0000 			// railing - Original world coords in Vespucci location << -997.270, -747.900, 64.694 >>	fZRot = -180.0
				ENDIF				
			BREAK
				
			CASE FIXER_HQ_INTERIOR_SEAT_19
				IF IS_PED_WEARING_HIGH_HEELS(PLAYER_PED_ID())
					vPos = << 4.732, 8.865, 3.800 >>		fZRot = 270.0000			// railing - Original world coords in Vespucci location << -999.1794, -750.7386, 64.694 >>	fZRot = -90.0
				ELSE
					vPos = << 4.732, 8.865, 3.800 >>		fZRot = 270.0000 			// railing - Original world coords in Vespucci location << -999.1794, -750.7386, 64.694 >>	fZRot = -90.0
				ENDIF
			BREAK
				
			CASE FIXER_HQ_INTERIOR_SEAT_20
				IF IS_PED_WEARING_HIGH_HEELS(PLAYER_PED_ID())
					vPos = << 4.728, 4.368, 3.805 >>		fZRot = 270.0000			// railing - Original world coords in Vespucci location << -999.1794, -750.7386, 64.694 >>	fZRot = -90.0
				ELSE
					vPos = << 4.728, 4.368, 3.805 >>		fZRot = 270.0000 			// railing - Original world coords in Vespucci location << -999.1794, -750.7386, 64.694 >>	fZRot = -90.0
				ENDIF
			BREAK
				
			CASE FIXER_HQ_INTERIOR_SEAT_21
				IF IS_PED_WEARING_HIGH_HEELS(PLAYER_PED_ID())
					vPos = << 6.126, 1.851, 3.805 >>		fZRot = 315.0000			// railing - Original world coords in Vespucci location << -997.785, -757.7528, 64.699 >>	fZRot = -45.0
				ELSE
					vPos = << 6.126, 1.851, 3.805 >>		fZRot = 315.0000 			// railing - Original world coords in Vespucci location << -997.785, -757.7528, 64.699 >>	fZRot = -45.0
				ENDIF
				BREAK
				
			CASE FIXER_HQ_INTERIOR_SEAT_22
				IF IS_PED_WEARING_HIGH_HEELS(PLAYER_PED_ID())
					vPos = << 7.941, 0.043, 3.804 >>		fZRot = 315.0000			// railing - Original world coords in Vespucci location << -995.9703, -759.561, 64.698 >>	fZRot = -45.0
				ELSE
					vPos = << 7.941, 0.043, 3.804 >>		fZRot = 315.0000 			// railing - Original world coords in Vespucci location << -995.9703, -759.561, 64.698 >>	fZRot = -45.0
				ENDIF
			BREAK
				
			CASE FIXER_HQ_INTERIOR_SEAT_23
				IF IS_PED_WEARING_HIGH_HEELS(PLAYER_PED_ID())
					vPos = << 9.425, -7.846, 3.805 >>		fZRot = 270.0000			// railing - Original world coords in Vespucci location << -994.486, -767.45, 64.699 >>	fZRot = -90.0
				ELSE
					vPos = << 9.425, -7.846, 3.805 >>		fZRot = 270.0000 			// railing - Original world coords in Vespucci location << -994.486, -767.45, 64.699 >>	fZRot = -90.0
				ENDIF
			BREAK
				
			CASE FIXER_HQ_INTERIOR_SEAT_24		vPos = << -0.46545, 10.7022, 3.799 >>	fZRot = 319.160004 			BREAK	// office - Original world coords in Hawick location << 395.030, -63.950, 106.162 >> fZRot = -150.840
		ENDSWITCH
	ELSE
		SWITCH eSeat					
			CASE FIXER_HQ_INTERIOR_SEAT_0		vPos = << 6.82983, -6.11058, 0 >>		fZRot = 325.000000		BREAK	// office - Original world coords in Hawick location vPos = << 376.736, -65.055, 102.363 >>	fZRot = -145.0
			CASE FIXER_HQ_INTERIOR_SEAT_1		vPos = << 6.76825, -8.49972, 0.0 >>		fZRot = 216.919998		BREAK	// clubhouse - Original world coords in Hawick location	<< 374.512, -64.180, 102.363 >>	fZRot = 106.920

			CASE FIXER_HQ_INTERIOR_SEAT_2		vPos = << 2.18689, 2.62849, 0 >>		fZRot = 41.0 			BREAK	// office - Original world coords in Hawick location	<< 386.536, -63.681, 102.363 >>	fZRot = -69.0 
			CASE FIXER_HQ_INTERIOR_SEAT_3		vPos = << 2.99902, -0.187584, 0 >>		fZRot = 134.0 			BREAK	// clubhouse - Original world coords in Hawick location	<< 383.612, -63.481, 102.363 >>	fZRot = 24.1 
			CASE FIXER_HQ_INTERIOR_SEAT_4		vPos = << 2.54896, 0.35308, 0 >>		fZRot = 134.0			BREAK	// office - Original world coords in Hawick location	<< 384.274, -63.243, 102.363 >>	fZRot = 23.9
			CASE FIXER_HQ_INTERIOR_SEAT_5		vPos = << 2.00586, 0.859912, 0 >>		fZRot = 134.0			BREAK	// office - Original world coords in Hawick location	<< 384.936, -62.906, 102.363 >>	fZRot = 24.1 

			CASE FIXER_HQ_INTERIOR_SEAT_6		vPos = << 2.29356, 10.3028, 3.799 >>	fZRot = 266.239990 		BREAK	// office - Original world coords in Hawick location	<<393.711,-66.406, 106.162>>	fZRot = 156.240
			CASE FIXER_HQ_INTERIOR_SEAT_7		vPos = << 2.31938, 9.64707, 3.799 >>	fZRot = 267.239990 		BREAK	// clubhouse - Original world coords in Hawick location	<<393.086,-66.206, 106.162>>	fZRot = 157.240
			CASE FIXER_HQ_INTERIOR_SEAT_8		vPos = << 2.32215, 8.9436, 3.799 >>		fZRot = 265.520020 		BREAK	// clubhouse - Original world coords in Hawick location	<<392.424,-65.968, 106.162>>	fZRot = 155.520 
			CASE FIXER_HQ_INTERIOR_SEAT_9		vPos = << 1.06633, 7.51704, 3.799 >>	fZRot = 210.000000 		BREAK	// office - Original world coords in Hawick location	<< 391.513, -64.300, 106.162 >>	fZRot = 100.0 

			CASE FIXER_HQ_INTERIOR_SEAT_10		vPos = << 2.63443, 0.211807, 3.799 >>	fZRot = 138.000000 		BREAK	// clubhouse - Original world coords in Hawick location	<< 384.112, -63.275, 106.162 >>	fZRot = 28.0 
			CASE FIXER_HQ_INTERIOR_SEAT_11		vPos = << 1.58678, 1.22775, 3.799 >>	fZRot = 134.000000 		BREAK	// office - Original world coords in Hawick location	<< 385.425, -62.638, 106.162 >>	fZRot = 24.0 
			CASE FIXER_HQ_INTERIOR_SEAT_12		vPos = << 2.09373, 0.747148, 3.799 >>	fZRot = 135.000000 		BREAK	// clubhouse - Original world coords in Hawick location	<< 384.800, -62.950, 106.162 >>	fZRot = 25.0 
			CASE FIXER_HQ_INTERIOR_SEAT_13		vPos = << 4.57492, 0.546689, 3.799 >>	fZRot = 210.000000 		BREAK	// office - Original world coords in Hawick location	<< 383.763, -65.213, 106.162 >>	fZRot = 100.0 

			CASE FIXER_HQ_INTERIOR_SEAT_14		vPos = << 3.43708, -4.35296, 3.799 >>	fZRot = 0.8 			BREAK	// clubhouse - Original world coords in Hawick location	<< 379.548, -62.468, 106.162 >>	fZRot = -109.080
			CASE FIXER_HQ_INTERIOR_SEAT_15		vPos = << 4.21208, -4.36248, 3.799 >>	fZRot = 0.919998 		BREAK	// clubhouse - Original world coords in Hawick location	<<379.274,-63.193, 106.162>>	fZRot = -109.080
			CASE FIXER_HQ_INTERIOR_SEAT_16		vPos = << 4.91477, -4.31954, 3.799 >>	fZRot = 0.8				BREAK	// office - Original world coords in Hawick location	<<379.074,-63.868, 106.162>>	fZRot = -109.080
			CASE FIXER_HQ_INTERIOR_SEAT_17		vPos = << 6.35365, -5.65603, 3.799 >>	fZRot = 300.000000 		BREAK	// office - Original world coords in Hawick location	<< 377.326, -64.763, 106.162 >>	fZRot = -170.0
		
			CASE FIXER_HQ_INTERIOR_SEAT_18		vPos = << 6.641, 11.542, 3.800 >>		fZRot = 180.0000 		BREAK	// railing - Original world coords in Vespucci location <<-997.270, -748.062, 64.694>>	fZRot = -180.000
			CASE FIXER_HQ_INTERIOR_SEAT_19		vPos = << 4.861, 8.865, 3.800 >>		fZRot = 270.0000 		BREAK	// railing - Original world coords in Vespucci location <<-999.05, -750.7386, 64.694>>	fZRot = -90.0
			CASE FIXER_HQ_INTERIOR_SEAT_20		vPos = << 4.861, 4.368, 3.800 >>		fZRot = 270.960 		BREAK	// railing - Original world coords in Vespucci location << -999.05, -755.2363, 64.694>>	fZRot = -90.0
			CASE FIXER_HQ_INTERIOR_SEAT_21		vPos = << 6.226, 1.951, 3.799 >>		fZRot = 315.000 		BREAK	// railing - Original world coords in Vespucci location <<-997.685, -757.6528, 64.69304>>fZRot = -45.0
			CASE FIXER_HQ_INTERIOR_SEAT_22		vPos = << 8.041, 0.143, 3.800 >>		fZRot = 315.000 		BREAK	// railing - Original world coords in Vespucci location <<-995.8703, -759.461, 64.694>>	fZRot = 45.0
			CASE FIXER_HQ_INTERIOR_SEAT_23		vPos = << 9.579, -7.846, 3.799 >>		fZRot = 270.00 			BREAK	// railing - Original world coords in Vespucci location <<-994.332, -767.45, 64.69304>>	fZRot = -90.0
			
			CASE FIXER_HQ_INTERIOR_SEAT_24		vPos = << -0.509708, 10.7127, 3.799 >>	fZRot = 318.0 			BREAK	// office - Original world coords in Hawick location << 395.055, -63.912, 106.162 >> fZRot = -150.840
		ENDSWITCH
	ENDIF	
	
	fZRot = TRANSFORM_SIMPLE_INTERIOR_HEADING_TO_WORLD_HEADING_GIVEN_ID(fZRot, GET_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID()))	
	vPos = TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS_GIVEN_ID(vPos, GET_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID()))
ENDPROC

PROC GET_FIXER_HQ_INTERIOR_OP_FLOOR_SEAT_WORLD_TRANSFORM(FIXER_HQ_INTERIOR_OP_FLOOR_SEATS eSeat, VECTOR& vPos, FLOAT& fZRot)
	IF IS_PLAYER_FEMALE()
		SWITCH eSeat					
			CASE FIXER_HQ_INTERIOR_OP_FLOOR_SEAT_0		vPos = << -7.66923, 7.25021, 8.6 >>		fZRot = 180.500000 		BREAK	// clubhouse - Original world coords in Hawick location	 <<394.250,-56.000, 110.963>>	fZRot = 70.5
			CASE FIXER_HQ_INTERIOR_OP_FLOOR_SEAT_1		vPos = << -8.41115, 7.20686, 8.6 >>		fZRot = 180.500000		BREAK	// office - Original world coords in Hawick location	 <<394.463,-55.288, 110.963>>	fZRot = 70.5
			CASE FIXER_HQ_INTERIOR_OP_FLOOR_SEAT_2		vPos = << -9.24418, 7.30273, 8.6 >>		fZRot = 180.500000		BREAK	// clubhouse - Original world coords in Hawick location	 <<394.838,-54.538, 110.963>>	fZRot = 70.5 

		ENDSWITCH
	ELSE
		SWITCH eSeat					
			CASE FIXER_HQ_INTERIOR_OP_FLOOR_SEAT_0		vPos = << -7.66923, 7.25021, 8.6 >>		fZRot = 180.500000 		BREAK	// clubhouse - Original world coords in Hawick location	 <<394.250,-56.000, 110.963>>	fZRot = 70.5
			CASE FIXER_HQ_INTERIOR_OP_FLOOR_SEAT_1		vPos = << -8.41115, 7.20686, 8.6 >>		fZRot = 180.500000		BREAK	// office - Original world coords in Hawick location	 <<394.463,-55.288, 110.963>>	fZRot = 70.5
			CASE FIXER_HQ_INTERIOR_OP_FLOOR_SEAT_2		vPos = << -9.24418, 7.30273, 8.6 >>		fZRot = 180.500000		BREAK	// clubhouse - Original world coords in Hawick location	 <<394.838,-54.538, 110.963>>	fZRot = 70.5 
		ENDSWITCH
	ENDIF
	
	fZRot = TRANSFORM_SIMPLE_INTERIOR_HEADING_TO_WORLD_HEADING_GIVEN_ID(fZRot, GET_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID()))	
	vPos = TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS_GIVEN_ID(vPos, GET_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID()))
ENDPROC

PROC GET_FIXER_HQ_INTERIOR_PQ_SEAT_WORLD_TRANSFORM(FIXER_HQ_INTERIOR_PQ_SEATS eSeat, VECTOR& vPos, FLOAT& fZRot)
	IF IS_PLAYER_FEMALE()
		SWITCH eSeat					
			CASE FIXER_HQ_INTERIOR_PQ_SEAT_0		vPos = << 5.24747, -5.83303, 8.6 >>		fZRot = 93.199997 		BREAK	// clubhouse - Original world coords in Hawick location	<< 377.538, -63.663, 110.963 >>	fZRot = -16.800 
			CASE FIXER_HQ_INTERIOR_PQ_SEAT_1		vPos = << 6.29509, -4.69403, 8.6 >>		fZRot = 0.000000		BREAK	// office - Original world coords in Hawick location	<< 378.250, -65.037, 110.963 >>	fZRot = -110.0
			CASE FIXER_HQ_INTERIOR_PQ_SEAT_2		vPos = << 7.32827, -4.71705, 8.6 >>		fZRot = 1.000000 		BREAK	// office - Original world coords in Hawick location	<< 377.875, -66.000, 110.963 >>	fZRot = -109.0
																		
			CASE FIXER_HQ_INTERIOR_PQ_SEAT_3		vPos = << 9.164, -8.66425, 8.6 >>		fZRot = 265.000000		BREAK	// office - Original world coords in Hawick location	<< 373.538, -66.375, 110.963 >>	fZRot = 155.0 
			CASE FIXER_HQ_INTERIOR_PQ_SEAT_4		vPos = << 6.94726, -9.73712, 8.6 >>		fZRot = 180.440002	 	BREAK	// office - Original world coords in Hawick location	<< 373.288, -63.925, 110.963 >>	fZRot = 80.440
		ENDSWITCH
	ELSE
		SWITCH eSeat					
			CASE FIXER_HQ_INTERIOR_PQ_SEAT_0		vPos = << 5.24747, -5.83303, 8.6 >>		fZRot = 93.199997 		BREAK	// clubhouse - Original world coords in Hawick location	<< 377.538, -63.663, 110.963 >>	fZRot = -16.800 
			CASE FIXER_HQ_INTERIOR_PQ_SEAT_1		vPos = << 6.29509, -4.69403, 8.6 >>		fZRot = 0.000000		BREAK	// office - Original world coords in Hawick location	<< 378.250, -65.037, 110.963 >>	fZRot = -110.0
			CASE FIXER_HQ_INTERIOR_PQ_SEAT_2		vPos = << 7.32827, -4.71705, 8.6 >>		fZRot = 1.000000 		BREAK	// office - Original world coords in Hawick location	<< 377.875, -66.000, 110.963 >>	fZRot = -109.0
																		
			CASE FIXER_HQ_INTERIOR_PQ_SEAT_3		vPos = << 9.164, -8.66425, 8.6 >>		fZRot = 265.000000		BREAK	// office - Original world coords in Hawick location	<< 373.538, -66.375, 110.963 >>	fZRot = 155.0 
			CASE FIXER_HQ_INTERIOR_PQ_SEAT_4		vPos = << 6.94726, -9.73712, 8.6 >>		fZRot = 180.440002	 	BREAK	// office - Original world coords in Hawick location	<< 373.288, -63.925, 110.963 >>	fZRot = 80.440
		ENDSWITCH
	ENDIF
	
	fZRot = TRANSFORM_SIMPLE_INTERIOR_HEADING_TO_WORLD_HEADING_GIVEN_ID(fZRot, GET_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID()))	
	vPos = TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS_GIVEN_ID(vPos, GET_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID()))
ENDPROC

FUNC VECTOR GET_FIXER_HQ_INTERIOR_SEAT_ENTRY_LOCATE_OFFSET_0()
	RETURN <<-0.5, -0.5, 0.0>>
ENDFUNC

FUNC VECTOR GET_FIXER_HQ_INTERIOR_SEAT_ENTRY_LOCATE_OFFSET_1()
	RETURN <<0.5, -0.5, 2.0>>
ENDFUNC

FUNC VECTOR GET_FIXER_HQ_INTERIOR_SEAT_ENTRY_LOCATE_OFFSET_0_ALT()
	RETURN <<-1.0, -0.5, 0.0>>
ENDFUNC

FUNC VECTOR GET_FIXER_HQ_INTERIOR_SEAT_ENTRY_LOCATE_OFFSET_1_ALT()
	RETURN <<1.0, -0.5, 2.0>>
ENDFUNC

FUNC FLOAT GET_FIXER_HQ_INTERIOR_SEAT_ROTATION_OFFSET(SEAT_TYPE eType, FLOAT fRotation)
	SWITCH eType
		CASE SEAT_TYPE_OFFICE_ANIM
		CASE SEAT_TYPE_CLUBHOUSE_ANIM
		CASE SEAT_TYPE_RAILING_ANIM
			RETURN fRotation
		CASE SEAT_TYPE_JACUZZI_ANIM
		CASE SEAT_TYPE_BASE_ANIM
			RETURN fRotation - 180.0
	ENDSWITCH
	
	RETURN fRotation
ENDFUNC

FUNC BOOL IS_SEAT_RAILING_LEAN(SEATS_LOCAL_DATA& data, INT iSeatIndex)
	IF data.public.seats[iSeatIndex].eType = SEAT_TYPE_RAILING_ANIM
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

PROC SETUP_FIXER_HQ_SEAT(FIXER_HQ_INTERIOR_SEATS eSeat, SEATS_LOCAL_DATA& data, INT iSeatIndex, SEAT_TYPE eType)
	VECTOR vSeatPos
	FLOAT fSeatZRot
	
	GET_FIXER_HQ_INTERIOR_SEAT_WORLD_TRANSFORM(eSeat, vSeatPos, fSeatZRot)
	
	data.public.seats[iSeatIndex].eType = eType
	data.public.seats[iSeatIndex].animation.vPos = vSeatPos
	data.public.seats[iSeatIndex].animation.vRot = <<0.0, 0.0, fSeatZRot>>
	IF eSeat = FIXER_HQ_INTERIOR_SEAT_1
		data.public.seats[iSeatIndex].entryLocate.v0 = ROTATE_VECTOR_ABOUT_Z(GET_FIXER_HQ_INTERIOR_SEAT_ENTRY_LOCATE_OFFSET_0_ALT(), GET_FIXER_HQ_INTERIOR_SEAT_ROTATION_OFFSET(eType, fSeatZRot)) + vSeatPos
		data.public.seats[iSeatIndex].entryLocate.v1 = ROTATE_VECTOR_ABOUT_Z(GET_FIXER_HQ_INTERIOR_SEAT_ENTRY_LOCATE_OFFSET_1_ALT(), GET_FIXER_HQ_INTERIOR_SEAT_ROTATION_OFFSET(eType, fSeatZRot)) + vSeatPos
	ELIF IS_SEAT_RAILING_LEAN(data, iSeatIndex)
		data.public.seats[iSeatIndex].entryLocate.v0 = ROTATE_VECTOR_ABOUT_Z(GET_FIXER_HQ_INTERIOR_SEAT_ENTRY_LOCATE_OFFSET_0()+<<0.0, 0.0, -0.5>>, GET_FIXER_HQ_INTERIOR_SEAT_ROTATION_OFFSET(eType, fSeatZRot)) + vSeatPos
		data.public.seats[iSeatIndex].entryLocate.v1 = ROTATE_VECTOR_ABOUT_Z(GET_FIXER_HQ_INTERIOR_SEAT_ENTRY_LOCATE_OFFSET_1(), GET_FIXER_HQ_INTERIOR_SEAT_ROTATION_OFFSET(eType, fSeatZRot)) + vSeatPos
	ELSE
		data.public.seats[iSeatIndex].entryLocate.v0 = ROTATE_VECTOR_ABOUT_Z(GET_FIXER_HQ_INTERIOR_SEAT_ENTRY_LOCATE_OFFSET_0(), GET_FIXER_HQ_INTERIOR_SEAT_ROTATION_OFFSET(eType, fSeatZRot)) + vSeatPos
		data.public.seats[iSeatIndex].entryLocate.v1 = ROTATE_VECTOR_ABOUT_Z(GET_FIXER_HQ_INTERIOR_SEAT_ENTRY_LOCATE_OFFSET_1(), GET_FIXER_HQ_INTERIOR_SEAT_ROTATION_OFFSET(eType, fSeatZRot)) + vSeatPos
	ENDIF
	
	IF IS_SEAT_RAILING_LEAN(data, iSeatIndex)
		data.public.seats[iSeatIndex].entryLocate.fWidth = cfFIXER_HQ_INTERIOR_RAILING_LOCATE_WIDTH
	ELSE
		data.public.seats[iSeatIndex].entryLocate.fWidth = cfFIXER_HQ_INTERIOR_SEAT_LOCATE_WIDTH
	ENDIF
	
ENDPROC

PROC SETUP_FIXER_HQ_PQ_SEAT(FIXER_HQ_INTERIOR_PQ_SEATS eSeat, SEATS_LOCAL_DATA& data, INT iSeatIndex, SEAT_TYPE eType)
	VECTOR vSeatPos
	FLOAT fSeatZRot
	
	GET_FIXER_HQ_INTERIOR_PQ_SEAT_WORLD_TRANSFORM(eSeat, vSeatPos, fSeatZRot)
	
	data.public.seats[iSeatIndex].eType = eType
	data.public.seats[iSeatIndex].animation.vPos = vSeatPos
	data.public.seats[iSeatIndex].animation.vRot = <<0.0, 0.0, fSeatZRot>>
	IF eSeat = FIXER_HQ_INTERIOR_PQ_SEAT_1
		data.public.seats[iSeatIndex].entryLocate.v0 = ROTATE_VECTOR_ABOUT_Z(GET_FIXER_HQ_INTERIOR_SEAT_ENTRY_LOCATE_OFFSET_0_ALT(), GET_FIXER_HQ_INTERIOR_SEAT_ROTATION_OFFSET(eType, fSeatZRot)) + vSeatPos
		data.public.seats[iSeatIndex].entryLocate.v1 = ROTATE_VECTOR_ABOUT_Z(GET_FIXER_HQ_INTERIOR_SEAT_ENTRY_LOCATE_OFFSET_1_ALT(), GET_FIXER_HQ_INTERIOR_SEAT_ROTATION_OFFSET(eType, fSeatZRot)) + vSeatPos
	ELSE
		data.public.seats[iSeatIndex].entryLocate.v0 = ROTATE_VECTOR_ABOUT_Z(GET_FIXER_HQ_INTERIOR_SEAT_ENTRY_LOCATE_OFFSET_0(), GET_FIXER_HQ_INTERIOR_SEAT_ROTATION_OFFSET(eType, fSeatZRot)) + vSeatPos
		data.public.seats[iSeatIndex].entryLocate.v1 = ROTATE_VECTOR_ABOUT_Z(GET_FIXER_HQ_INTERIOR_SEAT_ENTRY_LOCATE_OFFSET_1(), GET_FIXER_HQ_INTERIOR_SEAT_ROTATION_OFFSET(eType, fSeatZRot)) + vSeatPos
	ENDIF

	data.public.seats[iSeatIndex].entryLocate.fWidth = cfFIXER_HQ_INTERIOR_SEAT_LOCATE_WIDTH
ENDPROC

PROC SETUP_FIXER_HQ_OP_FLOOR_SEAT(FIXER_HQ_INTERIOR_OP_FLOOR_SEATS eSeat, SEATS_LOCAL_DATA& data, INT iSeatIndex, SEAT_TYPE eType)
	VECTOR vSeatPos
	FLOAT fSeatZRot
	
	GET_FIXER_HQ_INTERIOR_OP_FLOOR_SEAT_WORLD_TRANSFORM(eSeat, vSeatPos, fSeatZRot)
	
	data.public.seats[iSeatIndex].eType = eType
	data.public.seats[iSeatIndex].animation.vPos = vSeatPos
	data.public.seats[iSeatIndex].animation.vRot = <<0.0, 0.0, fSeatZRot>>
	data.public.seats[iSeatIndex].entryLocate.v0 = ROTATE_VECTOR_ABOUT_Z(GET_FIXER_HQ_INTERIOR_SEAT_ENTRY_LOCATE_OFFSET_0(), GET_FIXER_HQ_INTERIOR_SEAT_ROTATION_OFFSET(eType, fSeatZRot)) + vSeatPos
	data.public.seats[iSeatIndex].entryLocate.v1 = ROTATE_VECTOR_ABOUT_Z(GET_FIXER_HQ_INTERIOR_SEAT_ENTRY_LOCATE_OFFSET_1(), GET_FIXER_HQ_INTERIOR_SEAT_ROTATION_OFFSET(eType, fSeatZRot)) + vSeatPos
	

	data.public.seats[iSeatIndex].entryLocate.fWidth = cfFIXER_HQ_INTERIOR_SEAT_LOCATE_WIDTH
ENDPROC

#ENDIF
