USING "globals.sch"
USING "rage_builtins.sch"
USING "Security_Contract/fm_content_security_contract_core.sch"

//////////////////////////
///    CONSTS 
//////////////////////////

CONST_INT SC_RESCUE_PED_BEHAVIOUR_VIP				0
CONST_INT SC_RESCUE_PED_BEHAVIOUR_DEFENDER			1
CONST_INT SC_RESCUE_PED_BEHAVIOUR_ATTACKER			2
CONST_INT SC_RESCUE_PED_BEHAVIOUR_CORPSE			3

CONST_INT SC_RESCUE_INITIAL_GOTO					0

CONST_INT SC_RESCUE_SEARCH_AREA						0

CONST_INT SC_RESCUE_TAKE_OUT_TARGETS				0

CONST_INT SC_RESCUE_DIALOGUE_OPENING				0

CONST_INT SC_RESCUE_HELP_OPENING					0
CONST_INT SC_RESCUE_HELP_SECURE_AREA				1
CONST_INT SC_RESCUE_HELP_LOOK_LISTEN				2
CONST_INT SC_RESCUE_HELP_SEARCH_AREA				3

CONST_FLOAT SC_RESCUE_VIP_NEAR_RANGE				15.0

CONST_FLOAT SC_RESCUE_MAVERICK_DIRT_LEVEL			15.0

CONST_INT SC_RESCUE_BOTTOM_RIGHT_HUD_CLIENT_HEALTH	0

CONST_INT SC_RESCUE_CRASH_SITE_FLARE				0

CONST_INT SC_RESCUE_MISSION_ENTITY					0

CONST_INT SC_RESCUE_FOCUS_CAM_FLARE					0

CONST_FLOAT SC_RESCUE_FLARE_SHOT_RANGE				300.0

CONST_FLOAT SC_RESCUE_FLARE_SHOT_TARGET_HEIGHT		100.0

CONST_FLOAT SC_RESCUE_AI_DIALOGUE_NEAR_DROP_OFF_DISTANCE	25.0
CONST_FLOAT SC_RESCUE_AI_DIALOGUE_NEAR_AMBUSH_DISTANCE		120.0
CONST_FLOAT SC_RESCUE_AI_DIALOGUE_NEAR_PLAYER_DISTANCE		15.0
CONST_INT SC_RESCUE_AI_DLG_DELAY_MIN				15000
CONST_INT SC_RESCUE_AI_DLG_DELAY_MAX				30000

//////////////////////////
///    ENUMS 
//////////////////////////

ENUM SC_RESCUE_AI_DIALOGUE
	eSC_RESCUE_AI_DIALOGUE_FIRST_NEAR_PLAYER = 0,
	eSC_RESCUE_AI_DIALOGUE_SHOOTOUT,
	eSC_RESCUE_AI_DIALOGUE_SHOOTOUT_END,
	eSC_RESCUE_AI_DIALOGUE_AMBUSH_CAR,
	eSC_RESCUE_AI_DIALOGUE_AMBUSH_HELI,
	eSC_RESCUE_AI_DIALOGUE_AMBUSH_GENERAL,
	eSC_RESCUE_AI_DIALOGUE_DROP_OFF
ENDENUM

ENUM SC_RESCUE_MODE_STATE
	eSC_RESCUE_STATE_GO_TO = 0,
	eSC_RESCUE_STATE_SEARCH_AREA,
	eSC_RESCUE_STATE_TAKE_OUT,
	eSC_RESCUE_STATE_COLLECT_VIP,
	eSC_RESCUE_STATE_DELIVER_VIP,
	eSC_RESCUE_STATE_END
ENDENUM

ENUM SC_RESCUE_CLIENT_MODE_STATE
	eSC_RESCUE_CLIENT_STATE_GO_TO = 0,
	eSC_RESCUE_CLIENT_STATE_SEARCH_AREA,
	eSC_RESCUE_CLIENT_STATE_TAKE_OUT,
	eSC_RESCUE_CLIENT_STATE_COLLECT_VIP,
	eSC_RESCUE_CLIENT_STATE_DELIVER_VIP,
	eSC_RESCUE_CLIENT_STATE_HELP_DELIVER_VIP,
	eSC_RESCUE_CLIENT_STATE_RECOVER_CLIENT,
	eSC_RESCUE_CLIENT_STATE_END 
ENDENUM

ENUM SC_RESCUE_PED_TASKS_VIP
	eSC_RESCUE_PED_TASK_VIP_IDLE = 0,
	eSC_RESCUE_PED_TASK_VIP_PLAY_ANIM = 1,
	eSC_RESCUE_PED_TASK_VIP_FOLLOW = 2
ENDENUM

ENUM SC_RESCUE_PED_TASKS_DEFENDER
	eSC_RESCUE_PED_TASK_DEFENDER_IDLE = 0,
	eSC_RESCUE_PED_TASK_DEFENDER_ATTACK = 1,
	eSC_RESCUE_PED_TASK_DEFENDER_RETURN_TO_CRASH = 2,
	eSC_RESCUE_PED_TASK_DEFENDER_SCENARIO = 3
ENDENUM

ENUM SC_RESCUE_PED_TASKS_ATTACKER
	eSC_RESCUE_PED_TASK_ATTACKER_IDLE = 0,
	eSC_RESCUE_PED_TASK_ATTACKER_SCENARIO = 1,
	eSC_RESCUE_PED_TASK_ATTACKER_ATTACK = 2
ENDENUM

ENUM SC_RESCUE_PED_TASKS_CORPSE
	eSC_RESCUE_PED_TASKS_CORPSE_IDLE = 0
ENDENUM

ENUM SC_RESCUE_TAGS
	eSC_RESCUE_TAG_CRASH = 0,
	eSC_RESCUE_TAG_VIP = 1,
	eSC_RESCUE_TAG_BODYGUARD = 2,
	eSC_RESCUE_TAG_BODYGUARD_2 = 3,
	eSC_RESCUE_TAG_FUGITIVE = 4,
	eSC_RESCUE_TAG_GRANGER = 5
ENDENUM

ENUM SC_RESCUE_MUSIC
	eSC_RESCUE_MUSIC_INIT = -1,
	eSC_RESCUE_MUSIC_START,
	eSC_RESCUE_MUSIC_SEARCH,
	eSC_RESCUE_MUSIC_DELIVER
ENDENUM

ENUM SC_RESCUE_TICKERS
	eSC_RESCUE_TICKER_LOCATED,
	eSC_RESCUE_TICKER_COLLECTED,
	eSC_RESCUE_TICKER_SECURED
ENDENUM

//////////////////////////
///    HELPERS 
//////////////////////////

FUNC BOOL SC_RESCUE_ARE_ALL_TARGETS_DEAD()
	RETURN (sTakeOutTargetLocal.iTargetsRemaining = 0) AND sTakeOutTargetLocal.iTargetsRegistered > 0
ENDFUNC

FUNC VECTOR SC_RESCUE_DELIVERY_COORD()
	RETURN <<217.7, -3.5, 73.4>>
ENDFUNC

FUNC PED_INDEX SC_RESCUE_GET_VIP_PED_INDEX()
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPed[GET_TAG_ID(eSC_RESCUE_TAG_VIP)].netId)
		RETURN NET_TO_PED(serverBD.sPed[GET_TAG_ID(eSC_RESCUE_TAG_VIP)].netId)
	ENDIF
	RETURN NULL
ENDFUNC

FUNC BOOL SC_RESCUE_IS_VIP_PED_GROUPED()
	IF DOES_ENTITY_EXIST(SC_RESCUE_GET_VIP_PED_INDEX())
		IF IS_PED_IN_GROUP(SC_RESCUE_GET_VIP_PED_INDEX())
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC VECTOR GET_FLARE_FIRE_POINT()
	SWITCH GET_SUBVARIATION()
		CASE FSS_RES_LOCATION_1		RETURN <<-1624.9458, 4599.0703, 40.9954>>
		CASE FSS_RES_LOCATION_2		RETURN <<1958.8148, 3469.4011, 40.9351>>
		CASE FSS_RES_LOCATION_3		RETURN <<1041.6304, -3279.0518, 4.8871>>
		CASE FSS_RES_LOCATION_4		RETURN <<1630.3577, -85.0664, 165.7594>>
	ENDSWITCH
	RETURN ZERO_VECTOR()
ENDFUNC

FUNC VECTOR GET_BODYGUARD_PED_RETURN_TO_CRASH_COORD(INT iPed)
	SWITCH GET_SUBVARIATION()
		CASE FSS_RES_LOCATION_1
			IF iPed = GET_TAG_ID(eSC_RESCUE_TAG_BODYGUARD)
				RETURN <<-1634.6935, 4600.9102, 42.5990>>
			ELIF iPed = GET_TAG_ID(eSC_RESCUE_TAG_BODYGUARD_2)
				RETURN <<-1637.9867, 4597.9048, 41.9927>>
			ENDIF
		BREAK
		CASE FSS_RES_LOCATION_2
			IF iPed = GET_TAG_ID(eSC_RESCUE_TAG_BODYGUARD)
				RETURN <<1950.2278, 3493.0703, 39.9210>>
			ELIF iPed = GET_TAG_ID(eSC_RESCUE_TAG_BODYGUARD_2)
				RETURN <<1950.9233, 3489.2444, 39.9856>>
			ENDIF
		BREAK
		CASE FSS_RES_LOCATION_3
			IF iPed = GET_TAG_ID(eSC_RESCUE_TAG_BODYGUARD)
				RETURN <<1045.6416, -3277.8999, 4.8985>>
			ELIF iPed = GET_TAG_ID(eSC_RESCUE_TAG_BODYGUARD_2)
				RETURN <<1049.3093, -3276.3970, 4.8991>>
			ENDIF
		BREAK
		CASE FSS_RES_LOCATION_4
			IF iPed = GET_TAG_ID(eSC_RESCUE_TAG_BODYGUARD)
				RETURN <<1634.2395, -80.2310, 167.5102>>
			ELIF iPed = GET_TAG_ID(eSC_RESCUE_TAG_BODYGUARD_2)
				RETURN <<1629.2360, -77.6587, 166.7695>>
			ENDIF
		BREAK
	ENDSWITCH
	RETURN ZERO_VECTOR()
ENDFUNC

FUNC FLOAT GET_BODYGUARD_PED_RETURN_TO_CRASH_HEADING(INT iPed)
	SWITCH GET_SUBVARIATION()
		CASE FSS_RES_LOCATION_1
			IF iPed = GET_TAG_ID(eSC_RESCUE_TAG_BODYGUARD)
				RETURN 353.3175
			ELIF iPed = GET_TAG_ID(eSC_RESCUE_TAG_BODYGUARD_2)
				RETURN 126.0421
			ENDIF
		BREAK
		CASE FSS_RES_LOCATION_2
			IF iPed = GET_TAG_ID(eSC_RESCUE_TAG_BODYGUARD)
				RETURN 63.2657
			ELIF iPed = GET_TAG_ID(eSC_RESCUE_TAG_BODYGUARD_2)
				RETURN 158.8659
			ENDIF
		BREAK
		CASE FSS_RES_LOCATION_3
			IF iPed = GET_TAG_ID(eSC_RESCUE_TAG_BODYGUARD)
				RETURN 139.9757
			ELIF iPed = GET_TAG_ID(eSC_RESCUE_TAG_BODYGUARD_2)
				RETURN 272.6851
			ENDIF
		BREAK
		CASE FSS_RES_LOCATION_4
			IF iPed = GET_TAG_ID(eSC_RESCUE_TAG_BODYGUARD)
				RETURN 194.8905
			ELIF iPed = GET_TAG_ID(eSC_RESCUE_TAG_BODYGUARD_2)
				RETURN 81.5449
			ENDIF
		BREAK
	ENDSWITCH
	RETURN 0.0
ENDFUNC

//////////////////////////
///    OBJECTIVE TEXT  
//////////////////////////

PROC SC_RESCUE_OBJECTIVE_TEXT_GO_TO()
	BOOL bThe
	STRING strSub
	SWITCH GET_SUBVARIATION()
		CASE FSS_RES_LOCATION_1
			strSub = "FXR_LOC_RE_RC"
		BREAK
		CASE FSS_RES_LOCATION_2
			strSub = "FXR_LOC_RE_GSD"
			bThe = TRUE
		BREAK
		CASE FSS_RES_LOCATION_3
			strSub = "FXR_LOC_RE_T"
		BREAK
		CASE FSS_RES_LOCATION_4
			strSub = "FXR_LOC_RE_LAD"
			bThe = TRUE
		BREAK
		CASE FSS_RES_LOCATION_5
			strSub = "FXR_LOC_RE_LP"
		BREAK
	ENDSWITCH

	Print_Objective_Text_With_Coloured_Text_Label(PICK_STRING(bThe, "FXR_GOTO_THE", "FXR_GOTO"), strSub, HUD_COLOUR_YELLOW)
ENDPROC

PROC SC_RESCUE_OBJECTIVE_TEXT_SEARCH_AREA()
	SWITCH GET_SUBVARIATION()
		CASE FSS_RES_LOCATION_4
			Print_Objective_Text("FXR_OT_RE_SVIPB")
		BREAK
		DEFAULT
			Print_Objective_Text("FXR_OT_RE_SVIP")
		BREAK
	ENDSWITCH
ENDPROC

PROC SC_RESCUE_OBJECTIVE_TEXT_RESCUE()
	STRING strSub = "FXR_ENT_RE_PRO"
	IF GET_SUBVARIATION() = FSS_RES_LOCATION_1
		strSub = "FXR_ENT_RE_ALT"
	ENDIF
	Print_Objective_Text_With_Coloured_Text_Label("FXR_OT_RE_RESC", strSub, HUD_COLOUR_RED)
ENDPROC

PROC SC_OBJECTIVE_TEXT_COLLECT_VIP()
	Print_Objective_Text(PICK_STRING(GET_SUBVARIATION() = FSS_RES_LOCATION_4, "FXR_OT_RE_COLB", "FXR_OT_RE_COL"))
ENDPROC

PROC SC_OBJECTIVE_TEXT_DELIVER_VIP()
	SWITCH GET_SUBVARIATION()
		CASE FSS_RES_LOCATION_4
			IF DOES_ENTITY_EXIST(SC_RESCUE_GET_VIP_PED_INDEX())
				IF ARE_PEDS_IN_THE_SAME_VEHICLE(LOCAL_PED_INDEX, SC_RESCUE_GET_VIP_PED_INDEX())
					Print_Objective_Text("FXR_OT_RE_DEL2B")
				ELSE
					Print_Objective_Text("FXR_OT_RE_DELB")
				ENDIF
			ENDIF
		BREAK
		DEFAULT
			IF DOES_ENTITY_EXIST(SC_RESCUE_GET_VIP_PED_INDEX())
				IF ARE_PEDS_IN_THE_SAME_VEHICLE(LOCAL_PED_INDEX, SC_RESCUE_GET_VIP_PED_INDEX())
					Print_Objective_Text("FXR_OT_RE_DEL2")
				ELSE
					Print_Objective_Text("FXR_OT_RE_DEL")
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH	
ENDPROC

PROC SC_OBJECTIVE_TEXT_HELP_DELIVER_VIP()
	SWITCH GET_SUBVARIATION()
		CASE FSS_RES_LOCATION_4
			IF DOES_ENTITY_EXIST(SC_RESCUE_GET_VIP_PED_INDEX())
				IF ARE_PEDS_IN_THE_SAME_VEHICLE(LOCAL_PED_INDEX, SC_RESCUE_GET_VIP_PED_INDEX())
					Print_Objective_Text("FXR_OT_RE_HDELC")
				ELSE
					Print_Objective_Text("FXR_OT_RE_HDELB")
				ENDIF
			ENDIF
		BREAK
		DEFAULT
			IF DOES_ENTITY_EXIST(SC_RESCUE_GET_VIP_PED_INDEX())
				IF ARE_PEDS_IN_THE_SAME_VEHICLE(LOCAL_PED_INDEX, SC_RESCUE_GET_VIP_PED_INDEX())
					Print_Objective_Text("FXR_OT_RE_HDEL2")
				ELSE
					Print_Objective_Text("FXR_OT_RE_HDEL")
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH	
	
	
ENDPROC

//////////////////////////
///    MODE STATES  
//////////////////////////

PROC SC_RESCUE_SETUP_STATES()
	ADD_MODE_STATE(eSC_RESCUE_STATE_GO_TO, eMODESTATE_GO_TO_POINT)
		ADD_MODE_STATE_TRANSITION_DEFAULT(eSC_RESCUE_STATE_GO_TO, eSC_RESCUE_STATE_SEARCH_AREA)

	ADD_MODE_STATE(eSC_RESCUE_STATE_SEARCH_AREA, eMODESTATE_SEARCH_AREA)
		ADD_MODE_STATE_TRANSITION_DEFAULT(eSC_RESCUE_STATE_SEARCH_AREA, eSC_RESCUE_STATE_TAKE_OUT)

	ADD_MODE_STATE(eSC_RESCUE_STATE_TAKE_OUT, eMODESTATE_TAKE_OUT_TARGETS)
		ADD_MODE_STATE_TRANSITION_DEFAULT(eSC_RESCUE_STATE_TAKE_OUT, eSC_RESCUE_STATE_COLLECT_VIP)
		
	ADD_MODE_STATE(eSC_RESCUE_STATE_COLLECT_VIP, eMODESTATE_COLLECT_MISSION_ENTITY)
		ADD_MODE_STATE_TRANSITION_DEFAULT(eSC_RESCUE_STATE_COLLECT_VIP, eSC_RESCUE_STATE_DELIVER_VIP)
			
	ADD_MODE_STATE(eSC_RESCUE_STATE_DELIVER_VIP, eMODESTATE_DELIVER_MISSION_ENTITY)
		ADD_MODE_STATE_TRANSITION_DEFAULT(eSC_RESCUE_STATE_DELIVER_VIP, eSC_RESCUE_STATE_END)
	
	ADD_MODE_STATE(eSC_RESCUE_STATE_END, eMODESTATE_END)
ENDPROC

PROC SC_RESCUE_SETUP_CLIENT_STATES()
	ADD_CLIENT_MODE_STATE(eSC_RESCUE_CLIENT_STATE_GO_TO, eMODESTATE_GO_TO_POINT, &SC_RESCUE_OBJECTIVE_TEXT_GO_TO)
		ADD_CLIENT_MODE_STATE_TRANSITION_LINKED(eSC_RESCUE_CLIENT_STATE_GO_TO, eSC_RESCUE_CLIENT_STATE_SEARCH_AREA, eSC_RESCUE_STATE_SEARCH_AREA)
	
	ADD_CLIENT_MODE_STATE(eSC_RESCUE_CLIENT_STATE_SEARCH_AREA, eMODESTATE_SEARCH_AREA, &SC_RESCUE_OBJECTIVE_TEXT_SEARCH_AREA)
		ADD_CLIENT_MODE_STATE_TRANSITION_LINKED(eSC_RESCUE_CLIENT_STATE_SEARCH_AREA, eSC_RESCUE_CLIENT_STATE_TAKE_OUT, eSC_RESCUE_STATE_TAKE_OUT)
	
	ADD_CLIENT_MODE_STATE(eSC_RESCUE_CLIENT_STATE_TAKE_OUT, eMODESTATE_TAKE_OUT_TARGETS, &SC_RESCUE_OBJECTIVE_TEXT_RESCUE)
		ADD_CLIENT_MODE_STATE_TRANSITION_LINKED(eSC_RESCUE_CLIENT_STATE_TAKE_OUT, eSC_RESCUE_CLIENT_STATE_COLLECT_VIP, eSC_RESCUE_STATE_COLLECT_VIP)
			
	ADD_CLIENT_MODE_STATES_DELIVERY(eSC_RESCUE_CLIENT_STATE_COLLECT_VIP, eSC_RESCUE_CLIENT_STATE_DELIVER_VIP, eSC_RESCUE_CLIENT_STATE_HELP_DELIVER_VIP, eSC_RESCUE_CLIENT_STATE_RECOVER_CLIENT, eSC_RESCUE_CLIENT_STATE_END,
									&SC_OBJECTIVE_TEXT_COLLECT_VIP, &SC_OBJECTIVE_TEXT_DELIVER_VIP, &SC_OBJECTIVE_TEXT_HELP_DELIVER_VIP, &EMPTY)
		
	ADD_CLIENT_MODE_STATE(eSC_RESCUE_CLIENT_STATE_END, eMODESTATE_END, &EMPTY)
ENDPROC

//////////////////////////
///    PLACEMENT DATA 
//////////////////////////

PROC SC_RESCUE_SET_PLACEMENT_DATA()
	
	data.MissionEntity.iCount = 1
	data.MissionEntity.MissionEntities[SC_RESCUE_MISSION_ENTITY].iCarrierVehicle = GET_TAG_ID(eSC_RESCUE_TAG_VIP)
	data.MissionEntity.MissionEntities[SC_RESCUE_MISSION_ENTITY].eCarrierType = ET_PED
	data.MissionEntity.MissionEntities[SC_RESCUE_MISSION_ENTITY].model = PROP_DRUG_PACKAGE
	data.MissionEntity.MissionEntities[SC_RESCUE_MISSION_ENTITY].vCoords = (data.Ped.Peds[GET_TAG_ID(eSC_RESCUE_TAG_VIP)].vCoords + <<0.0, 0.0, 0.5>>)
	SET_DATA_BIT(eDATABITSET_DISABLE_DETACH_CARRIER_ON_MISSION_ENTITY_DELIVERY)
	
	data.SearchArea.iNumAreas = 1
	data.Ambush.iSpawnDelay = 5000
	
	SET_PED_BIT(GET_TAG_ID(eSC_RESCUE_TAG_VIP), ePEDBITSET_ENABLE_PLAYER_NEAR_PED)
	SET_PED_DATA_BIT(GET_TAG_ID(eSC_RESCUE_TAG_VIP), ePEDDATABITSET_AMBUSH_TARGET)
	
	INT iPed, iCount
	REPEAT data.Ped.iCount iPed
		IF iPed != GET_TAG_ID(eSC_RESCUE_TAG_VIP)
		AND iPed != GET_TAG_ID(eSC_RESCUE_TAG_BODYGUARD)
		AND iPed != GET_TAG_ID(eSC_RESCUE_TAG_BODYGUARD_2)
		AND NOT IS_PED_BIT_SET(iPed, ePEDBITSET_AMBUSH_PED)
			SET_PED_BIT(iPed, ePEDBITSET_TARGET_PED)
			iCount++
		ENDIF
	ENDREPEAT
	data.TakeOutTarget[SC_RESCUE_TAKE_OUT_TARGETS].iCount = iCount
	
	data.BottomRightHUD[SC_RESCUE_BOTTOM_RIGHT_HUD_CLIENT_HEALTH].eType = eBOTTOMRIGHTHUD_CUSTOM
	
	data.Flares.iCount = 1
	data.Flares.Flare[SC_RESCUE_CRASH_SITE_FLARE].eColour = HUD_COLOUR_RED
	
	IF GET_SUBVARIATION() != FSS_RES_LOCATION_5
		data.FocusCam.Cam[SC_RESCUE_FOCUS_CAM_FLARE].vCoords = GET_FLARE_FIRE_POINT()+<<0.0, 0.0, SC_RESCUE_FLARE_SHOT_TARGET_HEIGHT/2>>
		data.FocusCam.Cam[SC_RESCUE_FOCUS_CAM_FLARE].fRange = data.GoToPoint.Locations[SC_RESCUE_INITIAL_GOTO].fRadius
	ENDIF
	
	SWITCH GET_SUBVARIATION()
		CASE FSS_RES_LOCATION_1
			SET_PROP_DATA_BIT(GET_TAG_ID(eSC_RESCUE_TAG_CRASH), ePROPDATABITSET_CHECK_PLAYER_NEAR)
			data.Flares.Flare[SC_RESCUE_CRASH_SITE_FLARE].vCoords = <<-1641.3589, 4597.9155, 42.6554>>
		
			SWITCH serverBD.iRandomModeInt0
				CASE 0
					data.Ped.Peds[0].model               = A_M_Y_Runner_02
				BREAK
				CASE 1
					data.Ped.Peds[0].model               = A_M_Y_Runner_02
					data.Ped.Peds[0].vCoords             = <<-1601.0533, 4470.8931, 13.0982>>
					data.Ped.Peds[0].fHeading            = 344.3999
					data.Ped.Peds[0].weapon              = WEAPONTYPE_UNARMED

					data.Ped.Peds[1].model               = S_M_M_HighSec_01
					data.Ped.Peds[1].vCoords             = <<-1597.5796, 4475.4829, 14.3376>>
					data.Ped.Peds[1].fHeading            = 344.3999
					data.Ped.Peds[1].weapon              = WEAPONTYPE_PISTOL

					data.Ped.Peds[2].model               = S_M_M_HighSec_01
					data.Ped.Peds[2].vCoords             = <<-1602.0859, 4474.4844, 14.0506>>
					data.Ped.Peds[2].fHeading            = 344.3999
					data.Ped.Peds[2].weapon              = WEAPONTYPE_PISTOL

					data.Ped.Peds[3].model               = A_M_O_ACULT_02
					data.Ped.Peds[3].vCoords             = <<-1589.2826, 4488.8462, 17.9533>>
					data.Ped.Peds[3].fHeading            = 130.5995
					data.Ped.Peds[3].weapon              = WEAPONTYPE_PISTOL

					data.Ped.Peds[4].model               = A_M_O_ACULT_02
					data.Ped.Peds[4].vCoords             = <<-1597.7976, 4488.4810, 17.8443>>
					data.Ped.Peds[4].fHeading            = 166.7993
					data.Ped.Peds[4].weapon              = WEAPONTYPE_PISTOL

					data.Ped.Peds[5].model               = A_M_O_ACULT_02
					data.Ped.Peds[5].vCoords             = <<-1596.6277, 4497.1294, 19.1485>>
					data.Ped.Peds[5].fHeading            = 166.7993
					data.Ped.Peds[5].weapon              = WEAPONTYPE_ASSAULTRIFLE

					data.Ped.Peds[6].model               = A_M_O_ACULT_02
					data.Ped.Peds[6].vCoords             = <<-1583.9814, 4491.8916, 19.8301>>
					data.Ped.Peds[6].fHeading            = 153.5992
					data.Ped.Peds[6].weapon              = WEAPONTYPE_ASSAULTRIFLE

					data.Ped.Peds[7].model               = A_M_O_ACULT_02
					data.Ped.Peds[7].vCoords             = <<-1587.6300, 4501.5664, 19.7831>>
					data.Ped.Peds[7].fHeading            = 153.5992
					data.Ped.Peds[7].weapon              = WEAPONTYPE_SAWNOFFSHOTGUN

					data.Ped.Peds[8].model               = A_M_O_ACULT_02
					data.Ped.Peds[8].vCoords             = <<-1592.5417, 4501.4780, 19.4526>>
					data.Ped.Peds[8].fHeading            = 171.7990
					data.Ped.Peds[8].weapon              = WEAPONTYPE_SAWNOFFSHOTGUN

					data.Ped.Peds[9].model               = A_M_O_ACULT_02
					data.Ped.Peds[9].vCoords             = <<-1603.7710, 4485.4556, 16.4426>>
					data.Ped.Peds[9].fHeading            = 187.3989
					data.Ped.Peds[9].weapon              = WEAPONTYPE_PISTOL

					data.Ped.Peds[10].model               = A_M_O_ACULT_02
					data.Ped.Peds[10].vCoords             = <<-1580.3348, 4491.4185, 20.8642>>
					data.Ped.Peds[10].fHeading            = 149.3988
					data.Ped.Peds[10].weapon              = WEAPONTYPE_PISTOL
				BREAK
				CASE 2
					data.Ped.Peds[0].model               = A_M_Y_Runner_02
					data.Ped.Peds[0].vCoords             = <<-1606.8047, 4711.8252, 41.5107>>
					data.Ped.Peds[0].fHeading            = 131.3983
					data.Ped.Peds[0].weapon              = WEAPONTYPE_UNARMED

					data.Ped.Peds[1].model               = S_M_M_HighSec_01
					data.Ped.Peds[1].vCoords             = <<-1603.4817, 4711.2891, 42.4222>>
					data.Ped.Peds[1].fHeading            = 235.7981
					data.Ped.Peds[1].weapon              = WEAPONTYPE_PISTOL

					data.Ped.Peds[2].model               = S_M_M_HighSec_01
					data.Ped.Peds[2].vCoords             = <<-1607.4171, 4705.3037, 41.7542>>
					data.Ped.Peds[2].fHeading            = 235.7981
					data.Ped.Peds[2].weapon              = WEAPONTYPE_PISTOL

					data.Ped.Peds[3].model               = A_M_O_ACULT_02
					data.Ped.Peds[3].vCoords             = <<-1592.9297, 4694.3545, 42.0709>>
					data.Ped.Peds[3].fHeading            = 72.1977
					data.Ped.Peds[3].weapon              = WEAPONTYPE_PISTOL

					data.Ped.Peds[4].model               = A_M_O_ACULT_02
					data.Ped.Peds[4].vCoords             = <<-1602.5725, 4680.4502, 40.4064>>
					data.Ped.Peds[4].fHeading            = 21.5976
					data.Ped.Peds[4].weapon              = WEAPONTYPE_PISTOL

					data.Ped.Peds[5].model               = A_M_O_ACULT_02
					data.Ped.Peds[5].vCoords             = <<-1595.0332, 4687.3643, 41.2702>>
					data.Ped.Peds[5].fHeading            = 34.7976
					data.Ped.Peds[5].weapon              = WEAPONTYPE_ASSAULTRIFLE

					data.Ped.Peds[6].model               = A_M_O_ACULT_02
					data.Ped.Peds[6].vCoords             = <<-1590.9962, 4715.2681, 46.6754>>
					data.Ped.Peds[6].fHeading            = 72.7976
					data.Ped.Peds[6].weapon              = WEAPONTYPE_ASSAULTRIFLE

					data.Ped.Peds[7].model               = A_M_O_ACULT_02
					data.Ped.Peds[7].vCoords             = <<-1584.0587, 4704.2607, 45.7243>>
					data.Ped.Peds[7].fHeading            = 72.7976
					data.Ped.Peds[7].weapon              = WEAPONTYPE_SAWNOFFSHOTGUN

					data.Ped.Peds[8].model               = A_M_O_ACULT_02
					data.Ped.Peds[8].vCoords             = <<-1583.4153, 4695.9448, 46.0876>>
					data.Ped.Peds[8].fHeading            = 72.7976
					data.Ped.Peds[8].weapon              = WEAPONTYPE_SAWNOFFSHOTGUN

					data.Ped.Peds[9].model               = A_M_O_ACULT_02
					data.Ped.Peds[9].vCoords             = <<-1582.7635, 4688.5127, 44.9787>>
					data.Ped.Peds[9].fHeading            = 41.3975
					data.Ped.Peds[9].weapon              = WEAPONTYPE_PISTOL

					data.Ped.Peds[10].model               = A_M_O_ACULT_02
					data.Ped.Peds[10].vCoords             = <<-1574.6913, 4691.6587, 47.2168>>
					data.Ped.Peds[10].fHeading            = 50.3974
					data.Ped.Peds[10].weapon              = WEAPONTYPE_PISTOL
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FSS_RES_LOCATION_2
			SET_PROP_DATA_BIT(GET_TAG_ID(eSC_RESCUE_TAG_CRASH), ePROPDATABITSET_CHECK_PLAYER_NEAR)
			data.Flares.Flare[SC_RESCUE_CRASH_SITE_FLARE].vCoords = <<1950.9940, 3491.3257, 40.1096>>
		
			SWITCH serverBD.iRandomModeInt0
				CASE 0
					data.Ped.Peds[10].model               = g_m_y_mexgang_01
				BREAK
				CASE 1
					data.Ped.Peds[0].model               = mp_g_m_pros_01
					data.Ped.Peds[0].vCoords             = <<1860.7231, 3342.0095, 42.7159>>
					data.Ped.Peds[0].fHeading            = 79.7982
					data.Ped.Peds[0].weapon              = WEAPONTYPE_PISTOL

					data.Ped.Peds[1].model               = mp_g_m_pros_01
					data.Ped.Peds[1].vCoords             = <<1848.2917, 3366.6775, 41.7046>>
					data.Ped.Peds[1].fHeading            = 143.3979
					data.Ped.Peds[1].weapon              = WEAPONTYPE_PISTOL

					data.Ped.Peds[2].model               = mp_g_m_pros_01
					data.Ped.Peds[2].vCoords             = <<1863.2286, 3351.3367, 42.1884>>
					data.Ped.Peds[2].fHeading            = 137.7979
					data.Ped.Peds[2].weapon              = WEAPONTYPE_MICROSMG

					data.Ped.Peds[3].model               = mp_g_m_pros_01
					data.Ped.Peds[3].vCoords             = <<1871.9783, 3333.2437, 42.9505>>
					data.Ped.Peds[3].fHeading            = 110.5977
					data.Ped.Peds[3].weapon              = WEAPONTYPE_MICROSMG

					data.Ped.Peds[4].model               = mp_g_m_pros_01
					data.Ped.Peds[4].vCoords             = <<1862.6844, 3335.6602, 42.9652>>
					data.Ped.Peds[4].fHeading            = 110.5977
					data.Ped.Peds[4].weapon              = WEAPONTYPE_PUMPSHOTGUN

					data.Ped.Peds[5].model               = mp_g_m_pros_01
					data.Ped.Peds[5].vCoords             = <<1849.0784, 3362.4971, 42.3023>>
					data.Ped.Peds[5].fHeading            = 137.7976
					data.Ped.Peds[5].weapon              = WEAPONTYPE_PUMPSHOTGUN

					data.Ped.Peds[6].model               = mp_g_m_pros_01
					data.Ped.Peds[6].vCoords             = <<1871.7495, 3343.1123, 42.3829>>
					data.Ped.Peds[6].fHeading            = 113.7975
					data.Ped.Peds[6].weapon              = WEAPONTYPE_SMG

					data.Ped.Peds[7].model               = mp_g_m_pros_01
					data.Ped.Peds[7].vCoords             = <<1860.6787, 3358.4121, 41.5614>>
					data.Ped.Peds[7].fHeading            = 135.7973
					data.Ped.Peds[7].weapon              = WEAPONTYPE_SMG
					
					data.Ped.Peds[8].model               = S_M_M_HighSec_01
					data.Ped.Peds[8].vCoords             = <<1841.3239, 3340.9846, 42.4530>>
					data.Ped.Peds[8].fHeading            = 322.9990
					data.Ped.Peds[8].weapon              = WEAPONTYPE_PISTOL

					data.Ped.Peds[9].model               = S_M_M_HighSec_01
					data.Ped.Peds[9].vCoords             = <<1849.1794, 3333.0940, 42.9054>>
					data.Ped.Peds[9].fHeading            = 322.9990
					data.Ped.Peds[9].weapon              = WEAPONTYPE_PISTOL
					
					data.Ped.Peds[10].model               = g_m_y_mexgang_01
					data.Ped.Peds[10].vCoords             = <<1844.6672, 3336.7354, 42.6273>>
					data.Ped.Peds[10].fHeading            = 135.9995
					data.Ped.Peds[10].weapon              = WEAPONTYPE_UNARMED
				BREAK
				CASE 2
					data.Ped.Peds[0].model               = mp_g_m_pros_01
					data.Ped.Peds[0].vCoords             = <<2046.3744, 3421.2693, 43.2323>>
					data.Ped.Peds[0].fHeading            = 202.1962
					data.Ped.Peds[0].weapon              = WEAPONTYPE_PISTOL

					data.Ped.Peds[1].model               = mp_g_m_pros_01
					data.Ped.Peds[1].vCoords             = <<2043.3046, 3442.6355, 42.8267>>
					data.Ped.Peds[1].fHeading            = 202.1962
					data.Ped.Peds[1].weapon              = WEAPONTYPE_PISTOL

					data.Ped.Peds[2].model               = mp_g_m_pros_01
					data.Ped.Peds[2].vCoords             = <<2048.9465, 3424.5627, 43.1821>>
					data.Ped.Peds[2].fHeading            = 193.1962
					data.Ped.Peds[2].weapon              = WEAPONTYPE_MICROSMG

					data.Ped.Peds[3].model               = mp_g_m_pros_01
					data.Ped.Peds[3].vCoords             = <<2040.9141, 3435.7258, 42.9670>>
					data.Ped.Peds[3].fHeading            = 209.3961
					data.Ped.Peds[3].weapon              = WEAPONTYPE_MICROSMG

					data.Ped.Peds[4].model               = mp_g_m_pros_01
					data.Ped.Peds[4].vCoords             = <<2040.0647, 3426.0093, 43.1660>>
					data.Ped.Peds[4].fHeading            = 209.3961
					data.Ped.Peds[4].weapon              = WEAPONTYPE_PUMPSHOTGUN

					data.Ped.Peds[5].model               = mp_g_m_pros_01
					data.Ped.Peds[5].vCoords             = <<2048.9658, 3438.6631, 42.8737>>
					data.Ped.Peds[5].fHeading            = 191.1961
					data.Ped.Peds[5].weapon              = WEAPONTYPE_PUMPSHOTGUN

					data.Ped.Peds[6].model               = mp_g_m_pros_01
					data.Ped.Peds[6].vCoords             = <<2048.4443, 3434.2366, 42.9658>>
					data.Ped.Peds[6].fHeading            = 185.5962
					data.Ped.Peds[6].weapon              = WEAPONTYPE_SMG

					data.Ped.Peds[7].model               = mp_g_m_pros_01
					data.Ped.Peds[7].vCoords             = <<2041.6447, 3429.5671, 43.0911>>
					data.Ped.Peds[7].fHeading            = 194.5961
					data.Ped.Peds[7].weapon              = WEAPONTYPE_SMG
					
					data.Ped.Peds[8].model               = S_M_M_HighSec_01
					data.Ped.Peds[8].vCoords             = <<2049.2432, 3407.8660, 43.3641>>
					data.Ped.Peds[8].fHeading            = 366.5966
					data.Ped.Peds[8].weapon              = WEAPONTYPE_PISTOL

					data.Ped.Peds[9].model               = S_M_M_HighSec_01
					data.Ped.Peds[9].vCoords             = <<2053.1926, 3410.1438, 43.3623>>
					data.Ped.Peds[9].fHeading            = 12.1965
					data.Ped.Peds[9].weapon              = WEAPONTYPE_PISTOL
					
					data.Ped.Peds[10].model               = g_m_y_mexgang_01
					data.Ped.Peds[10].vCoords             = <<2053.7625, 3401.4270, 43.6037>>
					data.Ped.Peds[10].fHeading            = 247.5969
					data.Ped.Peds[10].weapon              = WEAPONTYPE_UNARMED
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FSS_RES_LOCATION_3
			SET_VEHICLE_DATA_BIT(GET_TAG_ID(eSC_RESCUE_TAG_CRASH), eVEHICLEDATABITSET_CHECK_NEAR_VEHICLE)
			data.Flares.Flare[SC_RESCUE_CRASH_SITE_FLARE].vCoords = <<1049.2322, -3279.9915, 4.8968>>
		
			SWITCH serverBD.iRandomModeInt0
				CASE 0
					data.Ped.Peds[10].model               = G_M_Y_KORLIEUT_01
				BREAK
				CASE 1
					data.Ped.Peds[0].model               = mp_g_m_pros_01
					data.Ped.Peds[0].vCoords             = <<857.3395, -3301.9509, 4.9011>>
					data.Ped.Peds[0].fHeading            = 92.9989
					data.Ped.Peds[0].weapon              = WEAPONTYPE_PISTOL

					data.Ped.Peds[1].model               = mp_g_m_pros_01
					data.Ped.Peds[1].vCoords             = <<860.1885, -3272.0229, 4.9000>>
					data.Ped.Peds[1].fHeading            = 113.9988
					data.Ped.Peds[1].weapon              = WEAPONTYPE_PISTOL

					data.Ped.Peds[2].model               = mp_g_m_pros_01
					data.Ped.Peds[2].vCoords             = <<858.5533, -3292.4521, 4.8970>>
					data.Ped.Peds[2].fHeading            = 98.3988
					data.Ped.Peds[2].weapon              = WEAPONTYPE_MICROSMG

					data.Ped.Peds[3].model               = mp_g_m_pros_01
					data.Ped.Peds[3].vCoords             = <<864.8616, -3313.2966, 4.9011>>
					data.Ped.Peds[3].fHeading            = 91.1987
					data.Ped.Peds[3].weapon              = WEAPONTYPE_MICROSMG

					data.Ped.Peds[4].model               = mp_g_m_pros_01
					data.Ped.Peds[4].vCoords             = <<870.9391, -3300.4421, 4.8949>>
					data.Ped.Peds[4].fHeading            = 91.1987
					data.Ped.Peds[4].weapon              = WEAPONTYPE_PUMPSHOTGUN

					data.Ped.Peds[5].model               = mp_g_m_pros_01
					data.Ped.Peds[5].vCoords             = <<861.8206, -3283.6204, 4.8960>>
					data.Ped.Peds[5].fHeading            = 102.1986
					data.Ped.Peds[5].weapon              = WEAPONTYPE_PUMPSHOTGUN

					data.Ped.Peds[6].model               = mp_g_m_pros_01
					data.Ped.Peds[6].vCoords             = <<868.0834, -3293.5803, 4.8952>>
					data.Ped.Peds[6].fHeading            = 93.1986
					data.Ped.Peds[6].weapon              = WEAPONTYPE_SMG

					data.Ped.Peds[7].model               = mp_g_m_pros_01
					data.Ped.Peds[7].vCoords             = <<853.0034, -3311.5498, 4.9011>>
					data.Ped.Peds[7].fHeading            = 42.5985
					data.Ped.Peds[7].weapon              = WEAPONTYPE_SMG
					
					data.Ped.Peds[8].model               = S_M_M_HighSec_01
					data.Ped.Peds[8].vCoords             = <<847.0811, -3285.3306, 4.9011>>
					data.Ped.Peds[8].fHeading            = 292.3993
					data.Ped.Peds[8].weapon              = WEAPONTYPE_PISTOL

					data.Ped.Peds[9].model               = S_M_M_HighSec_01
					data.Ped.Peds[9].vCoords             = <<847.4521, -3305.1196, 4.9011>>
					data.Ped.Peds[9].fHeading            = 265.1992
					data.Ped.Peds[9].weapon              = WEAPONTYPE_PISTOL
					
					data.Ped.Peds[10].model               = G_M_Y_KORLIEUT_01
					data.Ped.Peds[10].vCoords             = <<849.6426, -3293.6960, 4.9007>>
					data.Ped.Peds[10].fHeading            = 58.3995
					data.Ped.Peds[10].weapon              = WEAPONTYPE_UNARMED
				BREAK
				CASE 2
					data.Ped.Peds[0].model               = mp_g_m_pros_01
					data.Ped.Peds[0].vCoords             = <<1117.8475, -3238.9185, 4.8948>>
					data.Ped.Peds[0].fHeading            = 113.7972
					data.Ped.Peds[0].weapon              = WEAPONTYPE_PUMPSHOTGUN

					data.Ped.Peds[1].model               = mp_g_m_pros_01
					data.Ped.Peds[1].vCoords             = <<1096.0253, -3238.9639, 4.8948>>
					data.Ped.Peds[1].fHeading            = 283.7972
					data.Ped.Peds[1].weapon              = WEAPONTYPE_SMG

					data.Ped.Peds[2].model               = mp_g_m_pros_01
					data.Ped.Peds[2].vCoords             = <<1098.5724, -3226.6965, 4.8977>>
					data.Ped.Peds[2].fHeading            = 227.7972
					data.Ped.Peds[2].weapon              = WEAPONTYPE_SMG

					data.Ped.Peds[3].model               = mp_g_m_pros_01
					data.Ped.Peds[3].vCoords             = <<1117.1085, -3230.7388, 4.8963>>
					data.Ped.Peds[3].fHeading            = 153.7972
					data.Ped.Peds[3].weapon              = WEAPONTYPE_PISTOL

					data.Ped.Peds[4].model               = mp_g_m_pros_01
					data.Ped.Peds[4].vCoords             = <<1096.4512, -3238.6138, 4.8948>>
					data.Ped.Peds[4].fHeading            = 273.7972
					data.Ped.Peds[4].weapon              = WEAPONTYPE_PISTOL

					data.Ped.Peds[5].model               = mp_g_m_pros_01
					data.Ped.Peds[5].vCoords             = <<1100.2318, -3226.1526, 4.8978>>
					data.Ped.Peds[5].fHeading            = 201.7972
					data.Ped.Peds[5].weapon              = WEAPONTYPE_MICROSMG

					data.Ped.Peds[6].model               = mp_g_m_pros_01
					data.Ped.Peds[6].vCoords             = <<1083.7115, -3236.6309, 4.8948>>
					data.Ped.Peds[6].fHeading            = 273.7972
					data.Ped.Peds[6].weapon              = WEAPONTYPE_MICROSMG

					data.Ped.Peds[7].model               = mp_g_m_pros_01
					data.Ped.Peds[7].vCoords             = <<1089.4183, -3243.9517, 4.8950>>
					data.Ped.Peds[7].fHeading            = 237.4792
					data.Ped.Peds[7].weapon              = WEAPONTYPE_PUMPSHOTGUN

					data.Ped.Peds[8].model               = S_M_M_HighSec_01
					data.Ped.Peds[8].vCoords             = <<1104.6215, -3242.0701, 4.8950>>
					data.Ped.Peds[8].fHeading            = 141.8695
					data.Ped.Peds[8].weapon              = WEAPONTYPE_PISTOL

					data.Ped.Peds[9].model               = S_M_M_HighSec_01
					data.Ped.Peds[9].vCoords             = <<1107.2043, -3238.1130, 4.8948>>
					data.Ped.Peds[9].fHeading            = 281.0505
					data.Ped.Peds[9].weapon              = WEAPONTYPE_PISTOL

					data.Ped.Peds[10].model               = G_M_Y_KORLIEUT_01
					data.Ped.Peds[10].vCoords             = <<1104.2776, -3239.3354, 4.8949>>
					data.Ped.Peds[10].fHeading            = 280.5545
					data.Ped.Peds[10].weapon              = WEAPONTYPE_UNARMED
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FSS_RES_LOCATION_4
			SET_VEHICLE_DATA_BIT(GET_TAG_ID(eSC_RESCUE_TAG_CRASH), eVEHICLEDATABITSET_CHECK_NEAR_VEHICLE)
			data.Flares.Flare[SC_RESCUE_CRASH_SITE_FLARE].vCoords = <<1634.4581, -81.0344, 167.4318>>
			
			SWITCH serverBD.iRandomModeInt0
				CASE 0
					data.Ped.Peds[10].model               = A_F_Y_BUSINESS_02
				BREAK
				CASE 1
					data.Ped.Peds[0].model               = mp_g_m_pros_01
					data.Ped.Peds[0].vCoords             = <<1665.2185, -6.1699, 165.1181>>
					data.Ped.Peds[0].fHeading            = 42.9995
					data.Ped.Peds[0].weapon              = WEAPONTYPE_PISTOL

					data.Ped.Peds[1].model               = mp_g_m_pros_01
					data.Ped.Peds[1].vCoords             = <<1663.7725, 7.4628, 165.0677>>
					data.Ped.Peds[1].fHeading            = 101.9993
					data.Ped.Peds[1].weapon              = WEAPONTYPE_PISTOL

					data.Ped.Peds[2].model               = mp_g_m_pros_01
					data.Ped.Peds[2].vCoords             = <<1660.5421, 17.5311, 166.3600>>
					data.Ped.Peds[2].fHeading            = 188.9991
					data.Ped.Peds[2].weapon              = WEAPONTYPE_MICROSMG

					data.Ped.Peds[3].model               = mp_g_m_pros_01
					data.Ped.Peds[3].vCoords             = <<1666.6334, -9.5094, 165.5581>>
					data.Ped.Peds[3].fHeading            = 35.7988
					data.Ped.Peds[3].weapon              = WEAPONTYPE_MICROSMG

					data.Ped.Peds[4].model               = mp_g_m_pros_01
					data.Ped.Peds[4].vCoords             = <<1663.8064, -9.6994, 166.1989>>
					data.Ped.Peds[4].fHeading            = 20.1988
					data.Ped.Peds[4].weapon              = WEAPONTYPE_PUMPSHOTGUN

					data.Ped.Peds[5].model               = mp_g_m_pros_01
					data.Ped.Peds[5].vCoords             = <<1661.0179, 8.3662, 165.0677>>
					data.Ped.Peds[5].fHeading            = 208.5987
					data.Ped.Peds[5].weapon              = WEAPONTYPE_PUMPSHOTGUN

					data.Ped.Peds[6].model               = mp_g_m_pros_01
					data.Ped.Peds[6].vCoords             = <<1659.4297, 12.3468, 165.0677>>
					data.Ped.Peds[6].fHeading            = 192.3987
					data.Ped.Peds[6].weapon              = WEAPONTYPE_SMG

					data.Ped.Peds[7].model               = mp_g_m_pros_01
					data.Ped.Peds[7].vCoords             = <<1659.8579, -13.1652, 168.9971>>
					data.Ped.Peds[7].fHeading            = 302.9983
					data.Ped.Peds[7].weapon              = WEAPONTYPE_SMG
					
					data.Ped.Peds[8].model               = S_M_M_HighSec_01
					data.Ped.Peds[8].vCoords             = <<1662.8458, 3.2510, 165.1181>>
					data.Ped.Peds[8].fHeading            = 31.3999
					data.Ped.Peds[8].weapon              = WEAPONTYPE_PISTOL

					data.Ped.Peds[9].model               = S_M_M_HighSec_01
					data.Ped.Peds[9].vCoords             = <<1663.8672, -0.7522, 165.1181>>
					data.Ped.Peds[9].fHeading            = 133.9997
					data.Ped.Peds[9].weapon              = WEAPONTYPE_PISTOL
					
					data.Ped.Peds[10].model               = A_F_Y_BUSINESS_02
					data.Ped.Peds[10].vCoords             = <<1666.1508, 0.4412, 165.1181>>
					data.Ped.Peds[10].fHeading            = 40.4000
					data.Ped.Peds[10].weapon              = WEAPONTYPE_UNARMED
				BREAK
				CASE 2
					data.Ped.Peds[0].model               = mp_g_m_pros_01
					data.Ped.Peds[0].vCoords             = <<1598.8861, -23.8839, 120.8486>>
					data.Ped.Peds[0].fHeading            = 111.9968
					data.Ped.Peds[0].weapon              = WEAPONTYPE_PISTOL

					data.Ped.Peds[1].model               = mp_g_m_pros_01
					data.Ped.Peds[1].vCoords             = <<1634.0753, -35.2953, 127.3759>>
					data.Ped.Peds[1].fHeading            = 69.7966
					data.Ped.Peds[1].weapon              = WEAPONTYPE_PISTOL

					data.Ped.Peds[2].model               = mp_g_m_pros_01
					data.Ped.Peds[2].vCoords             = <<1604.5369, -21.0358, 121.8280>>
					data.Ped.Peds[2].fHeading            = 111.7965
					data.Ped.Peds[2].weapon              = WEAPONTYPE_MICROSMG

					data.Ped.Peds[3].model               = mp_g_m_pros_01
					data.Ped.Peds[3].vCoords             = <<1620.4486, -33.8043, 124.9892>>
					data.Ped.Peds[3].fHeading            = 74.5963
					data.Ped.Peds[3].weapon              = WEAPONTYPE_MICROSMG

					data.Ped.Peds[4].model               = mp_g_m_pros_01
					data.Ped.Peds[4].vCoords             = <<1607.0189, -32.3641, 122.2955>>
					data.Ped.Peds[4].fHeading            = 81.7963
					data.Ped.Peds[4].weapon              = WEAPONTYPE_PUMPSHOTGUN

					data.Ped.Peds[5].model               = mp_g_m_pros_01
					data.Ped.Peds[5].vCoords             = <<1621.5499, -21.3865, 124.3833>>
					data.Ped.Peds[5].fHeading            = 81.7963
					data.Ped.Peds[5].weapon              = WEAPONTYPE_PUMPSHOTGUN

					data.Ped.Peds[6].model               = mp_g_m_pros_01
					data.Ped.Peds[6].vCoords             = <<1617.0498, -22.8383, 123.2838>>
					data.Ped.Peds[6].fHeading            = 81.7963
					data.Ped.Peds[6].weapon              = WEAPONTYPE_SMG

					data.Ped.Peds[7].model               = mp_g_m_pros_01
					data.Ped.Peds[7].vCoords             = <<1630.8086, -26.0749, 126.0390>>
					data.Ped.Peds[7].fHeading            = 81.7963
					data.Ped.Peds[7].weapon              = WEAPONTYPE_SMG
					
					data.Ped.Peds[8].model               = S_M_M_HighSec_01
					data.Ped.Peds[8].vCoords             = <<1585.1624, -27.1533, 121.0297>>
					data.Ped.Peds[8].fHeading            = 293.9977
					data.Ped.Peds[8].weapon              = WEAPONTYPE_PISTOL

					data.Ped.Peds[9].model               = S_M_M_HighSec_01
					data.Ped.Peds[9].vCoords             = <<1579.2615, -36.7080, 119.9902>>
					data.Ped.Peds[9].fHeading            = 293.9977
					data.Ped.Peds[9].weapon              = WEAPONTYPE_PISTOL
					
					data.Ped.Peds[10].model               = A_F_Y_BUSINESS_02
					data.Ped.Peds[10].vCoords             = <<1574.9158, -37.6376, 120.3019>>
					data.Ped.Peds[10].fHeading            = 358.1981
					data.Ped.Peds[10].weapon              = WEAPONTYPE_UNARMED
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FSS_RES_LOCATION_5
			SET_PED_DATA_BIT(8, ePEDDATABITSET_I_AM_A_CORPSE)
			SET_PED_DATA_BIT(9, ePEDDATABITSET_I_AM_A_CORPSE)
			SET_PROP_DATA_BIT(GET_TAG_ID(eSC_RESCUE_TAG_CRASH), ePROPDATABITSET_CHECK_PLAYER_NEAR)
			data.Flares.Flare[SC_RESCUE_CRASH_SITE_FLARE].vCoords = <<-525.7749, -1719.7250, 18.1930>>
		
			SWITCH serverBD.iRandomModeInt0
				CASE 0
					data.Ped.Peds[10].model               = A_M_Y_BUSINESS_03
				BREAK
				CASE 1
					data.Ped.Peds[0].model               = mp_g_m_pros_01
					data.Ped.Peds[0].vCoords             = <<-525.5981, -1632.4200, 16.7978>>
					data.Ped.Peds[0].fHeading            = 343.0024
					data.Ped.Peds[0].weapon              = WEAPONTYPE_PISTOL

					data.Ped.Peds[1].model               = mp_g_m_pros_01
					data.Ped.Peds[1].vCoords             = <<-532.7646, -1619.2811, 16.7979>>
					data.Ped.Peds[1].fHeading            = 286.9992
					data.Ped.Peds[1].weapon              = WEAPONTYPE_PISTOL

					data.Ped.Peds[2].model               = mp_g_m_pros_01
					data.Ped.Peds[2].vCoords             = <<-531.4089, -1618.9053, 16.7979>>
					data.Ped.Peds[2].fHeading            = 103.3989
					data.Ped.Peds[2].weapon              = WEAPONTYPE_MICROSMG

					data.Ped.Peds[3].model               = mp_g_m_pros_01
					data.Ped.Peds[3].vCoords             = <<-538.4414, -1623.9060, 16.7979>>
					data.Ped.Peds[3].fHeading            = 322.7984
					data.Ped.Peds[3].weapon              = WEAPONTYPE_MICROSMG

					data.Ped.Peds[4].model               = mp_g_m_pros_01
					data.Ped.Peds[4].vCoords             = <<-536.9992, -1628.2222, 16.7979>>
					data.Ped.Peds[4].fHeading            = 57.2106
					data.Ped.Peds[4].weapon              = WEAPONTYPE_MICROSMG

					data.Ped.Peds[5].model               = mp_g_m_pros_01
					data.Ped.Peds[5].vCoords             = <<-513.5928, -1632.6344, 16.7978>>
					data.Ped.Peds[5].fHeading            = 102.1980
					data.Ped.Peds[5].weapon              = WEAPONTYPE_PUMPSHOTGUN

					data.Ped.Peds[6].model               = mp_g_m_pros_01
					data.Ped.Peds[6].vCoords             = <<-514.9395, -1633.0656, 16.7978>>
					data.Ped.Peds[6].fHeading            = 282.1976
					data.Ped.Peds[6].weapon              = WEAPONTYPE_SMG

					data.Ped.Peds[7].model               = mp_g_m_pros_01
					data.Ped.Peds[7].vCoords             = <<-541.8005, -1636.7872, 18.1100>>
					data.Ped.Peds[7].fHeading            = 65.3972
					data.Ped.Peds[7].weapon              = WEAPONTYPE_SMG
					
					// 8 & 9 stay the same
					
					data.Ped.Peds[10].model               = A_M_Y_BUSINESS_03
					data.Ped.Peds[10].vCoords             = <<-535.2132, -1629.5781, 17.0352>>
					data.Ped.Peds[10].fHeading            = 278.7995
					data.Ped.Peds[10].weapon              = WEAPONTYPE_UNARMED

				BREAK
				CASE 2
					data.Ped.Peds[0].model               = mp_g_m_pros_01
					data.Ped.Peds[0].vCoords             = <<-463.7527, -1669.6333, 18.0464>>
					data.Ped.Peds[0].fHeading            = 70.2395
					data.Ped.Peds[0].weapon              = WEAPONTYPE_PUMPSHOTGUN

					data.Ped.Peds[1].model               = mp_g_m_pros_01
					data.Ped.Peds[1].vCoords             = <<-460.4100, -1663.3002, 18.0402>>
					data.Ped.Peds[1].fHeading            = 69.7963
					data.Ped.Peds[1].weapon              = WEAPONTYPE_PUMPSHOTGUN

					data.Ped.Peds[2].model               = mp_g_m_pros_01
					data.Ped.Peds[2].vCoords             = <<-455.5369, -1667.4031, 18.0291>>
					data.Ped.Peds[2].fHeading            = 352.8144
					data.Ped.Peds[2].weapon              = WEAPONTYPE_PUMPSHOTGUN

					data.Ped.Peds[3].model               = mp_g_m_pros_01
					data.Ped.Peds[3].vCoords             = <<-453.4417, -1670.6595, 18.0291>>
					data.Ped.Peds[3].fHeading            = 357.9956
					data.Ped.Peds[3].weapon              = WEAPONTYPE_MICROSMG

					data.Ped.Peds[4].model               = mp_g_m_pros_01
					data.Ped.Peds[4].vCoords             = <<-449.6013, -1666.4374, 18.0291>>
					data.Ped.Peds[4].fHeading            = 204.9953
					data.Ped.Peds[4].weapon              = WEAPONTYPE_PISTOL

					data.Ped.Peds[5].model               = mp_g_m_pros_01
					data.Ped.Peds[5].vCoords             = <<-448.6855, -1667.5909, 18.0291>>
					data.Ped.Peds[5].fHeading            = 35.3951
					data.Ped.Peds[5].weapon              = WEAPONTYPE_SMG

					data.Ped.Peds[6].model               = mp_g_m_pros_01
					data.Ped.Peds[6].vCoords             = <<-472.7645, -1670.5203, 17.7210>>
					data.Ped.Peds[6].fHeading            = 296.1947
					data.Ped.Peds[6].weapon              = WEAPONTYPE_PISTOL

					data.Ped.Peds[7].model               = mp_g_m_pros_01
					data.Ped.Peds[7].vCoords             = <<-471.7408, -1669.9133, 17.8698>>
					data.Ped.Peds[7].fHeading            = 119.5938
					data.Ped.Peds[7].weapon              = WEAPONTYPE_SMG
					
					// 8 & 9 stay the same
					
					data.Ped.Peds[10].model               = A_M_Y_BUSINESS_03
					data.Ped.Peds[10].vCoords             = <<-466.3639, -1656.6155, 18.2831>>
					data.Ped.Peds[10].fHeading            = 329.3970
					data.Ped.Peds[10].weapon              = WEAPONTYPE_UNARMED
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
ENDPROC

//////////////////////////
///   INITIALISATION
//////////////////////////

FUNC BOOL SC_RESCUE_SERVER_INIT()
	RETURN TRUE
ENDFUNC

FUNC BOOL SC_RESCUE_CLIENT_INIT()	
	RETURN TRUE
ENDFUNC

//////////////////////////
///   PED CONFIG
//////////////////////////

PROC SC_RESCUE_SET_PED_ATTRIBUTES(INT iPed, PED_INDEX pedID, BOOL bAmbush)

	IF bAmbush
	OR IS_PED_DATA_BIT_SET(iPed, ePEDDATABITSET_I_AM_A_CORPSE)
		EXIT
	ENDIF

	SWITCH GET_PED_BEHAVIOUR(iPed)
		CASE SC_RESCUE_PED_BEHAVIOUR_VIP
			IF GET_SUBVARIATION() != FSS_RES_LOCATION_5
				SET_PED_RELATIONSHIP_GROUP_HASH(pedID, rgFM_AiLike)
			ENDIF
			SET_PED_CAN_BE_TARGETTED(pedID, FALSE)
		BREAK
		CASE SC_RESCUE_PED_BEHAVIOUR_DEFENDER
			SET_PED_RELATIONSHIP_GROUP_HASH(pedID, rgFM_AiLike_HateAiHate)
			SET_PED_CAN_BE_TARGETTED(pedID, FALSE)
		BREAK
		CASE SC_RESCUE_PED_BEHAVIOUR_ATTACKER
			SET_PED_RELATIONSHIP_GROUP_HASH(pedID, rgFM_AiHate)
		BREAK
	ENDSWITCH

	SET_PED_ACCURACY(pedID, 1)
	SET_PED_COMBAT_ATTRIBUTES(pedID, CA_USE_COVER, TRUE)
	SET_PED_COMBAT_ATTRIBUTES(pedID, CA_CAN_FIGHT_ARMED_PEDS_WHEN_NOT_ARMED, TRUE)
	SET_PED_SUFFERS_CRITICAL_HITS(pedId, FALSE)
	SET_COMBAT_FLOAT(pedID,CCF_FIGHT_PROFICIENCY, 0.1)
	SET_ENTITY_MAX_HEALTH(pedID, 2000)
	SET_ENTITY_HEALTH(pedID, 2000)
	SET_PED_DIES_WHEN_INJURED(pedID, FALSE)
	SET_PED_CONFIG_FLAG(pedId, PCF_DisableGoToWritheWhenInjured, TRUE)
	SET_PED_CONFIG_FLAG(pedId, PCF_DontActivateRagdollFromBulletImpact, TRUE)

	IF iPed = GET_TAG_ID(eSC_RESCUE_TAG_VIP)
		SET_PED_DIES_IN_WATER(pedID, FALSE)
		SET_PED_CONFIG_FLAG(pedID, PCF_DisableJumpingFromVehiclesAfterLeader, TRUE) 
		APPLY_PED_BLOOD_DAMAGE_BY_ZONE(pedId, PDZ_TORSO, 0.664, 0.317, BDT_SHOTGUN_SMALL)
		APPLY_PED_BLOOD_DAMAGE_BY_ZONE(pedId, PDZ_TORSO, 0.786, 0.469, BDT_SHOTGUN_LARGE)
		APPLY_PED_BLOOD_DAMAGE_BY_ZONE(pedId, PDZ_LEFT_ARM, 0.000, 0.148, BDT_SHOTGUN_LARGE)
		APPLY_PED_BLOOD_DAMAGE_BY_ZONE(pedId, PDZ_RIGHT_LEG, 0.210, 0.664, BDT_SHOTGUN_SMALL)
		
		SWITCH GET_SUBVARIATION()
			CASE FSS_RES_LOCATION_1
				SET_PED_COMPONENT_VARIATION(pedId, INT_TO_ENUM(PED_COMPONENT,0), 1, 2, 0) //(head)
				SET_PED_COMPONENT_VARIATION(pedId, INT_TO_ENUM(PED_COMPONENT,2), 0, 1, 0) //(hair)
				SET_PED_COMPONENT_VARIATION(pedId, INT_TO_ENUM(PED_COMPONENT,3), 0, 1, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(pedId, INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
				SET_PED_COMPONENT_VARIATION(pedId, INT_TO_ENUM(PED_COMPONENT,9), 0, 0, 0) //(task)

			BREAK
			CASE FSS_RES_LOCATION_2
				SET_PED_COMPONENT_VARIATION(pedId, INT_TO_ENUM(PED_COMPONENT,0), 1, 0, 0) //(head)
				SET_PED_COMPONENT_VARIATION(pedId, INT_TO_ENUM(PED_COMPONENT,2), 1, 0, 0) //(hair)
				SET_PED_COMPONENT_VARIATION(pedId, INT_TO_ENUM(PED_COMPONENT,3), 1, 0, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(pedId, INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
				SET_PED_COMPONENT_VARIATION(pedId, INT_TO_ENUM(PED_COMPONENT,6), 0, 2, 0) //(feet)
				SET_PED_COMPONENT_VARIATION(pedId, INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
				SET_PED_PROP_INDEX(pedId, INT_TO_ENUM(PED_PROP_POSITION,0), 0, 0)
			BREAK
			CASE FSS_RES_LOCATION_3
				SET_PED_COMPONENT_VARIATION(pedId, INT_TO_ENUM(PED_COMPONENT,0), 0, 1, 0) //(head)
				SET_PED_COMPONENT_VARIATION(pedId, INT_TO_ENUM(PED_COMPONENT,2), 0, 1, 0) //(hair)
				SET_PED_COMPONENT_VARIATION(pedId, INT_TO_ENUM(PED_COMPONENT,3), 2, 1, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(pedId, INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
				SET_PED_PROP_INDEX(pedId, INT_TO_ENUM(PED_PROP_POSITION,1), 0, 0)
			BREAK
			CASE FSS_RES_LOCATION_4
				SET_PED_COMPONENT_VARIATION(pedId, INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
				SET_PED_COMPONENT_VARIATION(pedId, INT_TO_ENUM(PED_COMPONENT,2), 0, 1, 0) //(hair)
				SET_PED_COMPONENT_VARIATION(pedId, INT_TO_ENUM(PED_COMPONENT,3), 1, 2, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(pedId, INT_TO_ENUM(PED_COMPONENT,4), 1, 2, 0) //(lowr)
				SET_PED_PROP_INDEX(pedId, INT_TO_ENUM(PED_PROP_POSITION,1), 0, 0)
			BREAK
			CASE FSS_RES_LOCATION_5
				SET_PED_COMPONENT_VARIATION(pedId, INT_TO_ENUM(PED_COMPONENT,0), 0, 2, 0) //(head)
				SET_PED_COMPONENT_VARIATION(pedId, INT_TO_ENUM(PED_COMPONENT,3), 1, 2, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(pedId, INT_TO_ENUM(PED_COMPONENT,4), 1, 0, 0) //(lowr)
			BREAK
		ENDSWITCH
	ENDIF

ENDPROC

FUNC BOOL SC_RESCUE_SHOULD_PED_TRIGGER(INT iPed)
	UNUSED_PARAMETER(iPed)
	IF iPed = GET_TAG_ID(eSC_RESCUE_TAG_VIP)
		RETURN IS_PED_BIT_SET(iPed, ePEDBITSET_PLAYER_HAS_BEEN_NEAR)
	ENDIF
	RETURN FALSE
ENDFUNC

PROC SC_RESCUE_SETUP_PED_TRIGGERS(INT iPed)
	ADD_PED_TRIGGER(iPed, &PED_TRIGGER_HAS_PED_SEEN_ENEMY_PLAYER)
	ADD_PED_TRIGGER(iPed, &PED_TRIGGER_HAS_PED_BEEN_TARGETTED)
	ADD_PED_TRIGGER(iPed, &PED_TRIGGER_HAS_PED_TRIGGER_AREA_BEEN_ACTIVATED)
	ADD_PED_TRIGGER(iPed, &PED_TRIGGER_HAS_PED_BEEN_DAMAGED)
	ADD_PED_TRIGGER(iPed, &SC_RESCUE_SHOULD_PED_TRIGGER)
ENDPROC

FUNC BOOL SC_RESCUE_SHOULD_PED_PLAY_ANIM(INT iPed, PED_INDEX pedID)
	UNUSED_PARAMETER(iPed)
	UNUSED_PARAMETER(pedID)
	RETURN TRUE
ENDFUNC

FUNC BOOL SC_RESCUE_SHOULD_PED_FOLLOW(INT iPed, PED_INDEX pedID)
	UNUSED_PARAMETER(pedID)
	IF iPed = GET_TAG_ID(eSC_RESCUE_TAG_VIP)
	AND SC_RESCUE_ARE_ALL_TARGETS_DEAD()
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL SC_RESCUE_SHOULD_PED_DO_SCENARIOS(INT iPed, PED_INDEX pedID)
	IF iPed = GET_TAG_ID(eSC_RESCUE_TAG_BODYGUARD)
	OR iPed = GET_TAG_ID(eSC_RESCUE_TAG_BODYGUARD_2)
		IF GET_PED_TASK_ID(iPed) = ENUM_TO_INT(eSC_RESCUE_PED_TASK_DEFENDER_RETURN_TO_CRASH)
			IF IS_ENTITY_AT_COORD(pedID, GET_BODYGUARD_PED_RETURN_TO_CRASH_COORD(iPed), <<1.5, 1.5, 2.0>>)
			AND IS_HEADING_ACCEPTABLE(GET_ENTITY_HEADING(pedID), GET_BODYGUARD_PED_RETURN_TO_CRASH_HEADING(iPed), 15.0)
				RETURN TRUE
			ENDIF
		ENDIF
		RETURN FALSE
	ENDIF
	
	SWITCH GET_SUBVARIATION()
		CASE FSS_RES_LOCATION_5
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SC_RESCUE_SHOULD_PED_ATTACK(INT iPed, PED_INDEX pedID)
	UNUSED_PARAMETER(pedID)
	
	SWITCH GET_SUBVARIATION()
		CASE FSS_RES_LOCATION_5
			IF IS_PED_BIT_SET(iPed, ePEDBITSET_TARGET_PED)
				RETURN HAS_PED_BEEN_TRIGGERED(iPed)
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SC_RESCUE_SHOULD_PED_RETURN_TO_CRASH(INT iPed, PED_INDEX pedID)
	UNUSED_PARAMETER(pedID)
	IF GET_TAG_ID(eSC_RESCUE_TAG_BODYGUARD) = iPed
	OR GET_TAG_ID(eSC_RESCUE_TAG_BODYGUARD_2) = iPed
		IF SC_RESCUE_IS_VIP_PED_GROUPED()
			RETURN TRUE
		ENDIF
	ENDIF
    RETURN FALSE
ENDFUNC

PROC SC_RESCUE_SETUP_PED_TASKS(INT iBehaviour)

	SWITCH iBehaviour
		CASE SC_RESCUE_PED_BEHAVIOUR_VIP						
			ADD_PED_TASK(iBehaviour, eSC_RESCUE_PED_TASK_VIP_IDLE, ePEDTASK_IDLE)
				ADD_PED_TASK_TRANSITION(iBehaviour, eSC_RESCUE_PED_TASK_VIP_IDLE, eSC_RESCUE_PED_TASK_VIP_PLAY_ANIM, &SC_RESCUE_SHOULD_PED_PLAY_ANIM)
				
			ADD_PED_TASK(iBehaviour, eSC_RESCUE_PED_TASK_VIP_PLAY_ANIM, ePEDTASK_PLAY_ANIM)
				ADD_PED_TASK_TRANSITION(iBehaviour, eSC_RESCUE_PED_TASK_VIP_PLAY_ANIM, eSC_RESCUE_PED_TASK_VIP_FOLLOW, &SC_RESCUE_SHOULD_PED_FOLLOW)
			
			ADD_PED_TASK(iBehaviour, eSC_RESCUE_PED_TASK_VIP_FOLLOW, ePEDTASK_FOLLOW_PLAYER)
				
		BREAK
		CASE SC_RESCUE_PED_BEHAVIOUR_DEFENDER		
			ADD_PED_TASK(iBehaviour, eSC_RESCUE_PED_TASK_DEFENDER_IDLE, ePEDTASK_IDLE)
				ADD_PED_TASK_TRANSITION(iBehaviour, eSC_RESCUE_PED_TASK_DEFENDER_IDLE, eSC_RESCUE_PED_TASK_DEFENDER_ATTACK, &SC_RESCUE_SHOULD_PED_ATTACK)
			
			ADD_PED_TASK(iBehaviour, eSC_RESCUE_PED_TASK_DEFENDER_ATTACK, ePEDTASK_ATTACK_HATED_TARGETS)
				ADD_PED_TASK_TRANSITION(iBehaviour, eSC_RESCUE_PED_TASK_DEFENDER_ATTACK, eSC_RESCUE_PED_TASK_DEFENDER_RETURN_TO_CRASH, &SC_RESCUE_SHOULD_PED_RETURN_TO_CRASH)
				
			ADD_PED_TASK(iBehaviour, eSC_RESCUE_PED_TASK_DEFENDER_RETURN_TO_CRASH, ePEDTASK_WALK_TO_POINT)
				ADD_PED_TASK_TRANSITION(iBehaviour, eSC_RESCUE_PED_TASK_DEFENDER_RETURN_TO_CRASH, eSC_RESCUE_PED_TASK_DEFENDER_SCENARIO, &SC_RESCUE_SHOULD_PED_DO_SCENARIOS)
			
			ADD_PED_TASK(iBehaviour, eSC_RESCUE_PED_TASK_DEFENDER_SCENARIO, ePEDTASK_DO_SCENARIOS)
			
		BREAK
		CASE SC_RESCUE_PED_BEHAVIOUR_ATTACKER
			ADD_PED_TASK(iBehaviour, eSC_RESCUE_PED_TASK_ATTACKER_IDLE, ePEDTASK_IDLE)
				ADD_PED_TASK_TRANSITION(iBehaviour, eSC_RESCUE_PED_TASK_ATTACKER_IDLE, eSC_RESCUE_PED_TASK_ATTACKER_SCENARIO, &SC_RESCUE_SHOULD_PED_DO_SCENARIOS)
				ADD_PED_TASK_TRANSITION(iBehaviour, eSC_RESCUE_PED_TASK_ATTACKER_IDLE, eSC_RESCUE_PED_TASK_ATTACKER_ATTACK, &SC_RESCUE_SHOULD_PED_ATTACK)
						
			ADD_PED_TASK(iBehaviour, eSC_RESCUE_PED_TASK_ATTACKER_SCENARIO, ePEDTASK_DO_SCENARIOS)
				ADD_PED_TASK_TRANSITION(iBehaviour, eSC_RESCUE_PED_TASK_ATTACKER_SCENARIO, eSC_RESCUE_PED_TASK_ATTACKER_ATTACK, &SC_RESCUE_SHOULD_PED_ATTACK)
			
			ADD_PED_TASK(iBehaviour, eSC_RESCUE_PED_TASK_ATTACKER_ATTACK, ePEDTASK_ATTACK_HATED_TARGETS)
			
		BREAK
		CASE SC_RESCUE_PED_BEHAVIOUR_CORPSE
			ADD_PED_TASK(iBehaviour, eSC_RESCUE_PED_TASKS_CORPSE_IDLE, ePEDTASK_IDLE)
		BREAK
	ENDSWITCH
	
ENDPROC

FUNC INT SC_RESCUE_GET_PED_BEHAVIOUR(INT iPed)

	SWITCH GET_SUBVARIATION()
		CASE FSS_RES_LOCATION_5
			IF iPed = GET_TAG_ID(eSC_RESCUE_TAG_BODYGUARD)
			OR iPed = GET_TAG_ID(eSC_RESCUE_TAG_BODYGUARD_2)
				RETURN SC_RESCUE_PED_BEHAVIOUR_CORPSE
			ENDIF
		BREAK
	ENDSWITCH
	
	IF iPed = GET_TAG_ID(eSC_RESCUE_TAG_VIP)
		RETURN SC_RESCUE_PED_BEHAVIOUR_VIP
	ENDIF
	
	IF iPed = GET_TAG_ID(eSC_RESCUE_TAG_BODYGUARD)
	OR iPed = GET_TAG_ID(eSC_RESCUE_TAG_BODYGUARD_2)
		RETURN SC_RESCUE_PED_BEHAVIOUR_DEFENDER
	ENDIF
	
	RETURN SC_RESCUE_PED_BEHAVIOUR_ATTACKER
ENDFUNC

FUNC FLOAT SC_RESCUE_GET_NEAR_PED_RANGE(INT iPed)
	IF iPed = GET_TAG_ID(eSC_RESCUE_TAG_VIP)
		RETURN SC_RESCUE_VIP_NEAR_RANGE
	ENDIF
	RETURN -1.0
ENDFUNC

FUNC BOOL SC_RESCUE_GET_NEAR_PED_SHOULD_RUN_PERSISTENT_CHECK(INT iPed)
	IF iPed = GET_TAG_ID(eSC_RESCUE_TAG_VIP)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC SC_RESCUE_NEAR_PED_ON_CHANGE(INT iPed, BOOL bNear)
	IF iPed = GET_TAG_ID(eSC_RESCUE_TAG_VIP)
	AND bNear
		IF NOT IS_MISSION_SERVER_BIT_SET(eMISSIONSERVERBITSET_CLIENT_LOCATED)
			MISSION_TICKER_DATA sTickerData
			sTickerData.iInt1 = ENUM_TO_INT(eSC_RESCUE_TICKER_LOCATED)
			SEND_FM_CONTENT_TICKER_EVENT_WITH_DATA(eFMCTICKER_CUSTOM, sTickerData)
			SET_MISSION_CLIENT_BIT(eMISSIONCLIENTBITSET_CLIENT_LOCATED)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SC_RESCUE_IS_FOLLOW_PLAYER_ENABLED(INT iPed, PED_INDEX pedID)
	IF iPed = GET_TAG_ID(eSC_RESCUE_TAG_VIP)
	
		IF DOES_ENTITY_EXIST(LOCAL_VEHICLE_INDEX)
			IF IS_VEHICLE_FULL(LOCAL_VEHICLE_INDEX)
			AND NOT IS_PED_IN_VEHICLE(pedID, LOCAL_VEHICLE_INDEX)
				RETURN FALSE
			ENDIF
		ENDIF
	
		RETURN (GET_MODE_STATE() >= eMODESTATE_COLLECT_MISSION_ENTITY)
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC FLOAT SC_RESCUE_GET_FOLLOW_PLAYER_MAX_MOVE_BLEND_RATIO(INT iPed)
	IF iPed = GET_TAG_ID(eSC_RESCUE_TAG_VIP)
		RETURN 1.5
	ENDIF
	RETURN PEDMOVEBLENDRATIO_RUN
ENDFUNC

FUNC eFOLLOW_PED_IDLE_TYPE SC_RESCUE_GET_FOLLOW_PLAYER_IDLE_BEHAVIOUR(INT iPed)
	IF iPed = GET_TAG_ID(eSC_RESCUE_TAG_VIP)
		RETURN eFOLLOWPEDIDLETYPE_ANIM
	ENDIF
	RETURN eFOLLOWPEDIDLETYPE_NONE
ENDFUNC

FUNC FLOAT SC_RESCUE_GET_FOLLOW_PLAYER_JOIN_RANGE(INT iPed)
	IF iPed = GET_TAG_ID(eSC_RESCUE_TAG_VIP)
		RETURN SC_RESCUE_VIP_NEAR_RANGE
	ENDIF
	RETURN -1.0
ENDFUNC

PROC SC_RESCUE_FOLLOW_PLAYER_ON_JOIN_GROUP(INT iPed)
	IF iPed = GET_TAG_ID(eSC_RESCUE_TAG_VIP) 
		MISSION_TICKER_DATA sTickerData
		
		IF NOT IS_MISSION_SERVER_BIT_SET(eMISSIONSERVERBITSET_CLIENT_COLLECTED_AT_LEAST_ONCE)
			sTickerData.iInt1 = ENUM_TO_INT(eSC_RESCUE_TICKER_COLLECTED)
			SET_MISSION_CLIENT_BIT(eMISSIONCLIENTBITSET_CLIENT_COLLECTED_AT_LEAST_ONCE)
		ELSE
			sTickerData.iInt1 = ENUM_TO_INT(eSC_RESCUE_TICKER_SECURED)
		ENDIF
		
		SEND_FM_CONTENT_TICKER_EVENT_WITH_DATA(eFMCTICKER_CUSTOM, sTickerData)
	ENDIF
ENDPROC

FUNC INT SC_RESCUE_GET_PED_HEALTH_ON_TRIGGER(INT iPed)
	INT iHealth = 200
	IF iPed = GET_TAG_ID(eSC_RESCUE_TAG_BODYGUARD)
	OR iPed = GET_TAG_ID(eSC_RESCUE_TAG_BODYGUARD_2)
		iHealth = 600
	ELIF iPed = GET_TAG_ID(eSC_RESCUE_TAG_VIP)
		iHealth = 2000
	ENDIF
	RETURN iHealth
ENDFUNC

FUNC BOOL SC_RESCUE_GET_SHOULD_CHANGE_PED_STATS_ON_TRIGGER(INT iPed)
	IF iPed = GET_TAG_ID(eSC_RESCUE_TAG_VIP)
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

FUNC STRING SC_RESCUE_GET_AI_DIALOGUE_STRING_FIRST_NEAR_PLAYER(PED_INDEX ped)
	
	STRING sContext = NULL
	INT iRandom
	
	SWITCH GET_ENTITY_MODEL(ped)
		CASE A_M_Y_RUNNER_02
			iRandom = GET_RANDOM_INT_IN_RANGE(0, 2)
			IF iRandom = 0
				sContext = "SEE_WEIRDO"
			ELSE
				sContext = "GUN_BEG"
			ENDIF
		BREAK
		CASE G_M_Y_MEXGANG_01
			sContext = "DYING_MOAN"
		BREAK
		CASE G_M_Y_KORLIEUT_01
			sContext = "GENERIC_FRIGHTENED_HIGH"
		BREAK
		CASE A_F_Y_BUSINESS_02
			sContext = "GENERIC_FRIGHTENED_HIGH"
		BREAK
		CASE A_M_Y_BUSINESS_03
			sContext = "GENERIC_FRIGHTENED_HIGH"
		BREAK
	ENDSWITCH
	
	RETURN sContext
ENDFUNC

FUNC STRING SC_RESCUE_GET_AI_DIALOGUE_STRING_SHOOTOUT(PED_INDEX ped)
	
	STRING sContext = NULL
	INT iRandom
	
	SWITCH GET_ENTITY_MODEL(ped)
		CASE A_M_Y_RUNNER_02
			iRandom = GET_RANDOM_INT_IN_RANGE(0, 2)
			IF iRandom = 0
				sContext = "GENERIC_FRIGHTENED_HIGH"
			ELSE
				sContext = "GENERIC_FRIGHTENED_MED"
			ENDIF
		BREAK
		CASE G_M_Y_MEXGANG_01
			iRandom = GET_RANDOM_INT_IN_RANGE(0, 3)
			IF iRandom = 0
				sContext = "GENERIC_FRIGHTENED_HIGH"
			ELIF iRandom = 1
				sContext = "GENERIC_SHOCKED_HIGH"
			ELSE
				sContext = "FALL_BACK"
			ENDIF
		BREAK
		CASE G_M_Y_KORLIEUT_01
			sContext = "PINNED_DOWN"
		BREAK
		CASE A_F_Y_BUSINESS_02
			sContext = "GENERIC_FRIGHTENED_MED"
		BREAK
		CASE A_M_Y_BUSINESS_03
			sContext = "GENERIC_FRIGHTENED_MED"
		BREAK
	ENDSWITCH
	
	RETURN sContext
ENDFUNC

FUNC STRING SC_RESCUE_GET_AI_DIALOGUE_STRING_SHOOTOUT_END(PED_INDEX ped)
	
	STRING sContext = NULL
	INT iRandom
	
	SWITCH GET_ENTITY_MODEL(ped)
		CASE A_M_Y_RUNNER_02
			iRandom = GET_RANDOM_INT_IN_RANGE(0, 2)
			IF iRandom = 0
				sContext = "JACK_VEHICLE_BACK"
			ELSE
				sContext = "BLOCKED_GENERIC"
			ENDIF
		BREAK
		CASE G_M_Y_MEXGANG_01
			iRandom = GET_RANDOM_INT_IN_RANGE(0, 2)
			IF iRandom = 0
				sContext = "GENERIC_THANKS"
			ELSE
				sContext = "GENERIC_CHEER"
			ENDIF
		BREAK
		CASE G_M_Y_KORLIEUT_01
			iRandom = GET_RANDOM_INT_IN_RANGE(0, 2)
			IF iRandom = 0
				sContext = "GENERIC_THANKS"
			ELSE
				sContext = "FALL_BACK"
			ENDIF
		BREAK
		CASE A_F_Y_BUSINESS_02
			sContext = "DRUGS_DEAL_INTERRUPTED_GENERIC"
		BREAK
		CASE A_M_Y_BUSINESS_03
			sContext = "GENERIC_THANKS"
		BREAK
	ENDSWITCH
	
	RETURN sContext
ENDFUNC

FUNC STRING SC_RESCUE_GET_AI_DIALOGUE_STRING_AMBUSH_CAR(PED_INDEX ped)
	
	STRING sContext = NULL
	
	SWITCH GET_ENTITY_MODEL(ped)
		CASE A_M_Y_RUNNER_02
			sContext = "OVER_THERE"
		BREAK
		CASE G_M_Y_MEXGANG_01
			sContext = "SUSPECT_SPOTTED"
		BREAK
		CASE G_M_Y_KORLIEUT_01
			sContext = "SUSPECT_SPOTTED"
		BREAK
		CASE A_F_Y_BUSINESS_02
			sContext = "OVER_THERE"
		BREAK
		CASE A_M_Y_BUSINESS_03
			sContext = "OVER_THERE"
		BREAK
	ENDSWITCH
	
	RETURN sContext
ENDFUNC

FUNC STRING SC_RESCUE_GET_AI_DIALOGUE_STRING_AMBUSH_HELI(PED_INDEX ped)
	
	STRING sContext = NULL
	
	SWITCH GET_ENTITY_MODEL(ped)
		CASE A_M_Y_RUNNER_02
			sContext = "UP_THERE"
		BREAK
		CASE G_M_Y_MEXGANG_01
			sContext = "SUSPECT_SPOTTED"
		BREAK
		CASE G_M_Y_KORLIEUT_01
			sContext = "SUSPECT_SPOTTED"
		BREAK
		CASE A_F_Y_BUSINESS_02
			sContext = "UP_THERE"
		BREAK
		CASE A_M_Y_BUSINESS_03
			sContext = "UP_THERE"
		BREAK
	ENDSWITCH
	
	RETURN sContext
ENDFUNC

FUNC STRING SC_RESCUE_GET_AI_DIALOGUE_STRING_AMBUSH_GENERAL(PED_INDEX ped)
	
	STRING sContext = NULL
	INT iRandom
	
	SWITCH GET_ENTITY_MODEL(ped)
		CASE A_M_Y_RUNNER_02
			iRandom = GET_RANDOM_INT_IN_RANGE(0, 3)
			IF iRandom = 0
				sContext = "DODGE"
			ELIF iRandom = 1
				sContext = "GENERIC_CURSE_MED"
			ELSE
				sContext = "GENERIC_SHOCKED_MED"
			ENDIF
		BREAK
		CASE G_M_Y_MEXGANG_01
			iRandom = GET_RANDOM_INT_IN_RANGE(0, 3)
			IF iRandom = 0
				sContext = "GENERIC_FRIGHTENED_MED"
			ELIF iRandom = 1
				sContext = "GENERIC_CURSE_MED"
			ELSE
				sContext = "GENERIC_SHOCKED_MED"
			ENDIF
		BREAK
		CASE G_M_Y_KORLIEUT_01
			iRandom = GET_RANDOM_INT_IN_RANGE(0, 2)
			IF iRandom = 0
				sContext = "COVER_ME"
			ELSE
				sContext = "GENERIC_CURSE_MED"
			ENDIF
		BREAK
		CASE A_F_Y_BUSINESS_02
			iRandom = GET_RANDOM_INT_IN_RANGE(0, 3)
			IF iRandom = 0
				sContext = "PROVOKE_GENERIC"
			ELIF iRandom = 1
				sContext = "SEE_WEIRDO"
			ELSE
				sContext = "GENERIC_CURSE_MED"
			ENDIF
		BREAK
		CASE A_M_Y_BUSINESS_03
			iRandom = GET_RANDOM_INT_IN_RANGE(0, 4)
			IF iRandom = 0
				sContext = "GENERIC_SHOCKED_HIGH"
			ELIF iRandom = 1
				sContext = "SEE_WEIRDO"
			ELIF iRandom = 2
				sContext = "GENERIC_CURSE_MED"
			ELSE
				sContext = "GUN_BEG"
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN sContext
ENDFUNC

FUNC STRING SC_RESCUE_GET_AI_DIALOGUE_STRING_DROP_OFF(PED_INDEX ped)
	
	STRING sContext = NULL
	
	SWITCH GET_ENTITY_MODEL(ped)
		CASE A_M_Y_RUNNER_02
			sContext = "GENERIC_THANKS"
		BREAK
		CASE G_M_Y_MEXGANG_01
			sContext = "GENERIC_BYE"
		BREAK
		CASE G_M_Y_KORLIEUT_01
			sContext = "GENERIC_BYE"
		BREAK
		CASE A_F_Y_BUSINESS_02
			sContext = "GENERIC_THANKS"
		BREAK
		CASE A_M_Y_BUSINESS_03
			sContext = "GENERIC_BYE"
		BREAK
	ENDSWITCH
	
	RETURN sContext
ENDFUNC

PROC SC_RESCUE_TRIGGER_AI_DIALOGUE(PED_INDEX ped, SC_RESCUE_AI_DIALOGUE eDialogue)
	
	STRING sContext = NULL
	
	SWITCH eDialogue
		CASE eSC_RESCUE_AI_DIALOGUE_FIRST_NEAR_PLAYER
			sContext = SC_RESCUE_GET_AI_DIALOGUE_STRING_FIRST_NEAR_PLAYER(ped)
		BREAK
		CASE eSC_RESCUE_AI_DIALOGUE_SHOOTOUT
			sContext = SC_RESCUE_GET_AI_DIALOGUE_STRING_SHOOTOUT(ped)
		BREAK
		CASE eSC_RESCUE_AI_DIALOGUE_SHOOTOUT_END
			sContext = SC_RESCUE_GET_AI_DIALOGUE_STRING_SHOOTOUT_END(ped)
		BREAK
		CASE eSC_RESCUE_AI_DIALOGUE_AMBUSH_CAR
			sContext = SC_RESCUE_GET_AI_DIALOGUE_STRING_AMBUSH_CAR(ped)
		BREAK
		CASE eSC_RESCUE_AI_DIALOGUE_AMBUSH_HELI
			sContext = SC_RESCUE_GET_AI_DIALOGUE_STRING_AMBUSH_HELI(ped)
		BREAK
		CASE eSC_RESCUE_AI_DIALOGUE_AMBUSH_GENERAL
			sContext = SC_RESCUE_GET_AI_DIALOGUE_STRING_AMBUSH_GENERAL(ped)
		BREAK
		CASE eSC_RESCUE_AI_DIALOGUE_DROP_OFF
			sContext = SC_RESCUE_GET_AI_DIALOGUE_STRING_DROP_OFF(ped)
		BREAK
	ENDSWITCH
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sContext)	
		PLAY_PED_AMBIENT_SPEECH_NATIVE(ped, sContext, "SPEECH_PARAMS_FORCE", FALSE)
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - SC_RESCUE_TRIGGER_AI_DIALOGUE - PLAYING CONTEXT ",sContext, "")
	#IF IS_DEBUG_BUILD 
	ELSE
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - SC_RESCUE_TRIGGER_AI_DIALOGUE - CONTEXT STRING IS NULL OR EMPTY!")
	#ENDIF
	ENDIF
	
	sLocalVariables.iAiDialogueDelay = GET_RANDOM_INT_IN_RANGE(SC_RESCUE_AI_DLG_DELAY_MIN, SC_RESCUE_AI_DLG_DELAY_MAX)
	
	RESET_NET_TIMER(sLocalVariables.stAiDialogueTriggered)
	START_NET_TIMER(sLocalVariables.stAiDialogueTriggered)
ENDPROC

PROC MAINTAIN_PED_RELATIONSHIP_GROUP(INT iPed, PED_INDEX pedID)
	IF sLocalVariables.iPedHealthBitset != 0
		IF iPed = GET_TAG_ID(eSC_RESCUE_TAG_VIP)
			IF GET_PED_RELATIONSHIP_GROUP_HASH(pedID) != rgFM_AiLike_HateAiHate
				SET_PED_RELATIONSHIP_GROUP_HASH(pedID, rgFM_AiLike_HateAiHate)
			ENDIF
		ENDIF
	ENDIF
	
	IF iPed = GET_TAG_ID(eSC_RESCUE_TAG_VIP)
	OR iPed = GET_TAG_ID(eSC_RESCUE_TAG_BODYGUARD)
	OR iPed = GET_TAG_ID(eSC_RESCUE_TAG_BODYGUARD_2)
		IF NOT IS_BIT_SET(sLocalVariables.iPedRelBitset, iPed)
			SET_PED_TREATED_AS_FRIENDLY(pedID, TRUE, TRUE)
			SET_BIT(sLocalVariables.iPedRelBitset, iPed)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL REQUEST_AND_LOAD_PED_CLIPSET(STRING stClipSet)

	REQUEST_CLIP_SET(stClipSet)
	
	IF NOT HAS_CLIP_SET_LOADED(stClipSet)		
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - REQUEST_AND_LOAD_PED_CLIPSET - HAS_CLIP_SET_LOADED(",stClipSet, ") = FALSE")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

PROC MAINTAIN_PED_LOCO(INT iPed, PED_INDEX pedID, BOOL bDead)

	IF bDead
		EXIT
	ENDIF
	
	IF iPed = GET_TAG_ID(eSC_RESCUE_TAG_VIP)
	
		IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netId)
		
			IF SC_RESCUE_ARE_ALL_TARGETS_DEAD()
		
				STRING stDrunkClipSet = "move_m@injured"
				IF GET_SUBVARIATION() = FSS_RES_LOCATION_4
					stDrunkClipSet = "move_f@injured"
				ENDIF
				IF REQUEST_AND_LOAD_PED_CLIPSET(stDrunkClipSet)
					SET_PED_MOVEMENT_CLIPSET(pedId, stDrunkClipSet)
				ENDIF
				
			ENDIF
			
		ENDIF
	ENDIF

ENDPROC

FUNC BOOL SHOULD_MAINTAIN_PED_HEALTH(INT iPed)
	IF IS_PED_DATA_BIT_SET(iPed, ePEDDATABITSET_I_AM_A_CORPSE)
	OR IS_PED_BIT_SET(iPed, ePEDBITSET_AMBUSH_PED)
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

PROC MAINTAIN_PED_HEALTH(INT iPed, PED_INDEX pedID, BOOL bDead)
	IF SHOULD_MAINTAIN_PED_HEALTH(iPed)
		MAINTAIN_PED_HEALTH_REGEN(iPed, pedID, bDead, 2000, SC_RESCUE_GET_PED_HEALTH_ON_TRIGGER(iPed), SC_RESCUE_GET_SHOULD_CHANGE_PED_STATS_ON_TRIGGER(iPed))
	ELIF IS_PED_DATA_BIT_SET(iPed, ePEDDATABITSET_I_AM_A_CORPSE)
		IF !bDead
		AND GET_ENTITY_HEALTH(pedID) > 0
			SET_ENTITY_HEALTH(pedID, 0)
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_PED_BLIP_FLASH(INT iPed)
	IF iPed = GET_TAG_ID(eSC_RESCUE_TAG_VIP)
		IF GET_MODE_STATE_ID() = ENUM_TO_INT(eSC_RESCUE_STATE_COLLECT_VIP)
		AND NOT IS_MISSION_LOCAL_BIT_SET(eMISSIONLOCALBITSET_DONE_COLLECT_VIP_BLIP_FLASH)
			FLASH_MISSION_BLIP(sPedLocal.sPed[iPed].sBlip.BlipID)
			SET_MISSION_LOCAL_BIT(eMISSIONLOCALBITSET_DONE_COLLECT_VIP_BLIP_FLASH)
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_PED_AI_DIALOGUE(INT iPed, PED_INDEX pedID, BOOL bDead)
	
	IF iPed != GET_TAG_ID(eSC_RESCUE_TAG_VIP)
	OR bDead	
		EXIT
	ENDIF

	IF GET_MODE_STATE_ID() = ENUM_TO_INT(eSC_RESCUE_STATE_TAKE_OUT)
		IF NOT IS_MISSION_LOCAL_BIT_SET(eMISSIONLOCALBITSET_DONE_DIALOGUE_PLAYER_NEAR)
			IF sMissionEntityLocal.sMissionEntity[0].fDistanceFromLocal < SC_RESCUE_AI_DIALOGUE_NEAR_PLAYER_DISTANCE		
				SC_RESCUE_TRIGGER_AI_DIALOGUE(pedID, eSC_RESCUE_AI_DIALOGUE_FIRST_NEAR_PLAYER)
				SET_MISSION_LOCAL_BIT(eMISSIONLOCALBITSET_DONE_DIALOGUE_PLAYER_NEAR)
			ENDIF
		ELSE
			IF HAS_NET_TIMER_EXPIRED(sLocalVariables.stAiDialogueTriggered, sLocalVariables.iAiDialogueDelay)
				SC_RESCUE_TRIGGER_AI_DIALOGUE(pedID, eSC_RESCUE_AI_DIALOGUE_SHOOTOUT)
			ENDIF
		ENDIF
	ELIF (GET_MODE_STATE_ID() = ENUM_TO_INT(eSC_RESCUE_STATE_COLLECT_VIP) OR GET_MODE_STATE_ID() = ENUM_TO_INT(eSC_RESCUE_STATE_DELIVER_VIP))
		IF sMissionEntityLocal.sMissionEntity[0].fDistanceFromDropoff < SC_RESCUE_AI_DIALOGUE_NEAR_DROP_OFF_DISTANCE
			IF NOT IS_MISSION_LOCAL_BIT_SET(eMISSIONLOCALBITSET_DONE_DIALOGUE_DROP_OFF)
				SC_RESCUE_TRIGGER_AI_DIALOGUE(pedID, eSC_RESCUE_AI_DIALOGUE_DROP_OFF)
				SET_MISSION_LOCAL_BIT(eMISSIONLOCALBITSET_DONE_DIALOGUE_DROP_OFF)
			ENDIF
		ELSE
			IF NOT IS_MISSION_LOCAL_BIT_SET(eMISSIONLOCALBITSET_DONE_DIALOGUE_SHOOTOUT_END)
				IF sMissionEntityLocal.sMissionEntity[0].fDistanceFromLocal < SC_RESCUE_AI_DIALOGUE_NEAR_PLAYER_DISTANCE	
					SC_RESCUE_TRIGGER_AI_DIALOGUE(pedID, eSC_RESCUE_AI_DIALOGUE_SHOOTOUT_END)
					SET_MISSION_LOCAL_BIT(eMISSIONLOCALBITSET_DONE_DIALOGUE_SHOOTOUT_END)
				ENDIF
			ELSE
				IF NOT IS_AMBUSH_WAVE_DEAD()
					BOOL bAmbushNearby
					INT iAmbush
					REPEAT MAX_NUM_AMBUSH_VEHICLES iAmbush
						IF serverBD.sAmbush.sVehicle[iAmbush].fDistanceFromTarget < SC_RESCUE_AI_DIALOGUE_NEAR_AMBUSH_DISTANCE
							bAmbushNearby = TRUE
						ENDIF
					ENDREPEAT
					IF bAmbushNearby
						IF NOT IS_MISSION_LOCAL_BIT_SET(eMISSIONLOCALBITSET_DONE_DIALOGUE_AMBUSH)
							IF HAS_NET_TIMER_EXPIRED(sLocalVariables.stAiDialogueTriggered, 5000)
								IF IS_THIS_MODEL_A_HELI(CALL logic.Ambush.OverrideVehicleModel())
									SC_RESCUE_TRIGGER_AI_DIALOGUE(pedID, eSC_RESCUE_AI_DIALOGUE_AMBUSH_HELI)
								ELSE
									SC_RESCUE_TRIGGER_AI_DIALOGUE(pedID, eSC_RESCUE_AI_DIALOGUE_AMBUSH_CAR)
								ENDIF
								SET_MISSION_LOCAL_BIT(eMISSIONLOCALBITSET_DONE_DIALOGUE_AMBUSH)
							ENDIF
						ELSE	
							IF HAS_NET_TIMER_EXPIRED(sLocalVariables.stAiDialogueTriggered, sLocalVariables.iAiDialogueDelay)
								SC_RESCUE_TRIGGER_AI_DIALOGUE(pedID, eSC_RESCUE_AI_DIALOGUE_AMBUSH_GENERAL)
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF IS_MISSION_LOCAL_BIT_SET(eMISSIONLOCALBITSET_DONE_DIALOGUE_AMBUSH)
						CLEAR_MISSION_LOCAL_BIT(eMISSIONLOCALBITSET_DONE_DIALOGUE_AMBUSH)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC SC_RESCUE_PED_CLIENT_MAINTAIN(INT iPed, PED_INDEX pedID, BOOL bDead)
	MAINTAIN_PED_HEALTH(iPed, pedID, bDead)
	MAINTAIN_PED_RELATIONSHIP_GROUP(iPed, pedID)
	MAINTAIN_PED_LOCO(iPed, pedID, bDead)
	MAINTAIN_PED_BLIP_FLASH(iPed)
	MAINTAIN_PED_AI_DIALOGUE(iPed, pedID, bDead)
ENDPROC

PROC SC_RESCUE_PED_SERVER_MAINTAIN(INT iPed, PED_INDEX pedID, BOOL bDead)
	UNUSED_PARAMETER(pedID)
	IF iPed = GET_TAG_ID(eSC_RESCUE_TAG_VIP)
	AND bDead
		IF GET_END_REASON() = eENDREASON_NO_REASON_YET
			SET_END_REASON(eENDREASON_PEDS_KILLED)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SC_RESCUE_PED_BLIP_ENABLE(INT iEntity)
	IF iEntity = GET_TAG_ID(eSC_RESCUE_TAG_VIP)
		IF ARE_PEDS_IN_THE_SAME_VEHICLE(LOCAL_PED_INDEX, SC_RESCUE_GET_VIP_PED_INDEX())
		OR NOT IS_SEARCH_AREA_DONE(SC_RESCUE_SEARCH_AREA)
			RETURN FALSE
		ENDIF
		RETURN TRUE
	ELIF iEntity = GET_TAG_ID(eSC_RESCUE_TAG_BODYGUARD)
	OR iEntity = GET_TAG_ID(eSC_RESCUE_TAG_BODYGUARD_2)
		IF GET_PED_BEHAVIOUR(iEntity) = SC_RESCUE_PED_BEHAVIOUR_CORPSE
			RETURN FALSE
		ENDIF
		RETURN GET_CLIENT_MODE_STATE() = eMODESTATE_TAKE_OUT_TARGETS
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BLIP_SPRITE SC_RESCUE_PED_BLIP_SPRITE(INT iEntity)
	IF iEntity = GET_TAG_ID(eSC_RESCUE_TAG_VIP)
		IF IS_PED_IN_GROUP(SC_RESCUE_GET_VIP_PED_INDEX())
			RETURN RADAR_TRACE_OBJECTIVE_BLUE
		ENDIF
		RETURN RADAR_TRACE_FRIEND
	ELIF iEntity = GET_TAG_ID(eSC_RESCUE_TAG_BODYGUARD)
	OR iEntity = GET_TAG_ID(eSC_RESCUE_TAG_BODYGUARD_2)
		RETURN RADAR_TRACE_OBJECTIVE_BLUE
	ENDIF
	RETURN RADAR_TRACE_INVALID
ENDFUNC

FUNC HUD_COLOURS SC_RESCUE_PED_BLIP_COLOUR(INT iEntity)
	IF iEntity = GET_TAG_ID(eSC_RESCUE_TAG_VIP)
		RETURN HUD_COLOUR_BLUE
	ELIF iEntity = GET_TAG_ID(eSC_RESCUE_TAG_BODYGUARD)
	OR iEntity = GET_TAG_ID(eSC_RESCUE_TAG_BODYGUARD_2)
		RETURN HUD_COLOUR_BLUE
	ENDIF
	RETURN HUD_COLOUR_PURE_WHITE
ENDFUNC

FUNC STRING SC_RESCUE_PED_BLIP_NAME(INT iEntity)
	IF iEntity = GET_TAG_ID(eSC_RESCUE_TAG_VIP)
		SWITCH GET_SUBVARIATION()
			CASE FSS_RES_LOCATION_4
				RETURN "FXR_BLP_RE_VIPB"
			BREAK
			DEFAULT
				RETURN "FXR_BLP_RE_VIP"
			BREAK
		ENDSWITCH
	ELIF iEntity = GET_TAG_ID(eSC_RESCUE_TAG_BODYGUARD)
	OR iEntity = GET_TAG_ID(eSC_RESCUE_TAG_BODYGUARD_2)
		RETURN "FXR_BLP_RE_BDG"
	ENDIF
	RETURN ""
ENDFUNC

FUNC FLOAT SC_RESCUE_PED_BLIP_SCALE(INT iEntity)
	IF iEntity = GET_TAG_ID(eSC_RESCUE_TAG_BODYGUARD)
	OR iEntity = GET_TAG_ID(eSC_RESCUE_TAG_BODYGUARD_2)
		RETURN 0.6
	ELIF iEntity = GET_TAG_ID(eSC_RESCUE_TAG_VIP) 
	AND SC_RESCUE_PED_BLIP_SPRITE(iEntity) = RADAR_TRACE_OBJECTIVE_BLUE
		RETURN 0.6
	ENDIF
	RETURN 1.0
ENDFUNC

FUNC BLIP_PRIORITY SC_RESCUE_PED_BLIP_PRIORITY(INT iEntity)
	IF iEntity = GET_TAG_ID(eSC_RESCUE_TAG_VIP)
		RETURN BLIP_PRIORITY_HIGHEST_SPECIAL_HIGH
	ENDIF
	RETURN BLIP_PRIORITY_HIGHES_SPECIAL_MED
ENDFUNC

FUNC BOOL SC_RESCUE_PED_BLIP_SHOULD_DRAW_MARKER(INT iPed, PED_INDEX pedID)
	IF iPed = GET_TAG_ID(eSC_RESCUE_TAG_VIP)
		IF IS_PED_LOCAL_BIT_SET(iPed, ePEDLOCALBITSET_IN_ANY_VEHICLE)
		OR IS_PED_IN_GROUP(pedID)
			RETURN FALSE
		ENDIF
		RETURN SC_RESCUE_PED_BLIP_ENABLE(iPed)
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC FLOAT SC_RESCUE_PED_BLIP_GET_MARKER_SCALE(INT iEntity)
	IF iEntity = GET_TAG_ID(eSC_RESCUE_TAG_VIP)
		RETURN 0.25
	ENDIF
	RETURN 0.5
ENDFUNC

FUNC BOOL SC_RESCUE_PED_BLIP_FLASH_ON_CREATION(INT iEntity)
	IF iEntity = GET_TAG_ID(eSC_RESCUE_TAG_VIP)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SC_RESCUE_DISABLE_GENERIC_ENEMY_PED_BLIP(INT iPed, BOOL bDead)
	UNUSED_PARAMETER(bDead)
	IF iPed = GET_TAG_ID(eSC_RESCUE_TAG_VIP)
	OR iPed = GET_TAG_ID(eSC_RESCUE_TAG_BODYGUARD)
	OR iPed = GET_TAG_ID(eSC_RESCUE_TAG_BODYGUARD_2)
		RETURN TRUE
	ENDIF
	RETURN bDead
ENDFUNC

FUNC STRING SC_RESCUE_PED_GET_SCENARIO(INT iPed)
	SWITCH GET_SUBVARIATION()
		CASE FSS_RES_LOCATION_5
			SWITCH serverBD.iRandomModeInt0
				CASE 1
					SWITCH iPed
						CASE 0		RETURN "WORLD_HUMAN_SMOKING"
						CASE 1		RETURN "WORLD_HUMAN_HANG_OUT_STREET"
						CASE 2		RETURN "WORLD_HUMAN_HANG_OUT_STREET"
						CASE 3		RETURN "WORLD_HUMAN_SMOKING"
						CASE 4		RETURN "WORLD_HUMAN_STAND_MOBILE"
						CASE 5		RETURN "WORLD_HUMAN_HANG_OUT_STREET"
						CASE 6		RETURN "WORLD_HUMAN_STAND_IMPATIENT"
						CASE 7		RETURN "WORLD_HUMAN_GUARD_STAND"
						CASE 10		RETURN "WORLD_HUMAN_BUM_SLUMPED"
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH iPed
						CASE 0		RETURN "WORLD_HUMAN_SMOKING"
						CASE 1		RETURN "WORLD_HUMAN_SMOKING"
						CASE 2		RETURN "WORLD_HUMAN_STAND_MOBILE"
						CASE 3		RETURN "WORLD_HUMAN_GUARD_STAND"
						CASE 4		RETURN "WORLD_HUMAN_HANG_OUT_STREET"
						CASE 5		RETURN "WORLD_HUMAN_HANG_OUT_STREET"
						CASE 6		RETURN "WORLD_HUMAN_HANG_OUT_STREET"
						CASE 7		RETURN "WORLD_HUMAN_STAND_IMPATIENT"
						CASE 10		RETURN "WORLD_HUMAN_BUM_SLUMPED"
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	IF iPed = GET_TAG_ID(eSC_RESCUE_TAG_BODYGUARD)
	OR iPed = GET_TAG_ID(eSC_RESCUE_TAG_BODYGUARD_2)
		RETURN "WORLD_HUMAN_GUARD_STAND"
	ENDIF
	
	#IF MAX_NUM_SCENARIOS
	IF data.ped.Peds[iPed].iScenarioIndex != -1
		RETURN CONVERT_TEXT_LABEL_TO_STRING(data.Scenario[data.ped.Peds[iPed].iScenarioIndex].sName)
	ENDIF
	#ENDIF
	
	RETURN ""
ENDFUNC

FUNC STRING SC_RESCUE_PED_GET_PLAY_ANIM_DIC(INT iPed)
	IF iPed = GET_TAG_ID(eSC_RESCUE_TAG_VIP)
		RETURN "amb@code_human_cower@male@idle_a"
	ENDIF
	
	RETURN ""
ENDFUNC

FUNC STRING SC_RESCUE_PED_GET_PLAY_ANIM_ANIM(INT iPed)
	IF iPed = GET_TAG_ID(eSC_RESCUE_TAG_VIP)
		RETURN "idle_c"
	ENDIF
	
	RETURN ""
ENDFUNC

FUNC FLOAT SC_RESCUE_PED_GET_ATTACK_HATED_RANGE(INT iPed)
	UNUSED_PARAMETER(iPed)
	RETURN 80.0
ENDFUNC

FUNC BOOL SC_RESCUE_PED_TASK_WALK_TO_POINT_USE_NAV_MESH(INT iPed)
	UNUSED_PARAMETER(iPed)
	RETURN TRUE
ENDFUNC

FUNC VECTOR SC_RESCUE_PED_TASK_WALK_TO_POINT_COORDS(INT iPed)
	RETURN GET_BODYGUARD_PED_RETURN_TO_CRASH_COORD(iPed)
ENDFUNC

FUNC FLOAT SC_RESCUE_PED_TASK_WALK_TO_POINT_MOVE_BLEND_RATIO(INT iPed)
	UNUSED_PARAMETER(iPed)
	RETURN PEDMOVEBLENDRATIO_RUN
ENDFUNC

FUNC INT SC_RESCUE_PED_TASK_WALK_TO_POINT_TIME_BEFORE_WARP()
	RETURN -1
ENDFUNC

FUNC FLOAT SC_RESCUE_PED_TASK_WALK_TO_POINT_FINAL_HEALING(INT iPed)
	RETURN GET_BODYGUARD_PED_RETURN_TO_CRASH_HEADING(iPed)
ENDFUNC


//////////////////////////
///   SEARCH AREA
//////////////////////////

FUNC BOOL SC_RESCUE_ENABLE_SEARCH_AREAS()
	RETURN (GET_MODE_STATE() = eMODESTATE_SEARCH_AREA)
ENDFUNC

FUNC BOOL SC_RESCUE_SEARCH_AREA_ENABLED(INT iSearchArea)
	SWITCH iSearchArea
		CASE SC_RESCUE_SEARCH_AREA
			RETURN TRUE
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC VECTOR SC_RESCUE_GET_SEARCH_AREA_FORCED_BLIP_COORDS(INT iSearchArea)
	SWITCH GET_SUBVARIATION()
		CASE FSS_RES_LOCATION_1
			SWITCH iSearchArea
				CASE SC_RESCUE_SEARCH_AREA			RETURN <<-1633.9862, 4603.6875, 43.5257>>
			ENDSWITCH
		BREAK
		CASE FSS_RES_LOCATION_2
			SWITCH iSearchArea
				CASE SC_RESCUE_SEARCH_AREA			RETURN <<1950.9772, 3487.9885, 39.9121>>
			ENDSWITCH
		BREAK
		CASE FSS_RES_LOCATION_3
			SWITCH iSearchArea
				CASE SC_RESCUE_SEARCH_AREA			RETURN <<1048.8494, -3278.6626, 4.8976>>
			ENDSWITCH
		BREAK
		CASE FSS_RES_LOCATION_4
			SWITCH iSearchArea
				CASE SC_RESCUE_SEARCH_AREA			RETURN <<1631.0350, -83.9214, 165.7715>>
			ENDSWITCH
		BREAK
		CASE FSS_RES_LOCATION_5
			SWITCH iSearchArea
				CASE SC_RESCUE_SEARCH_AREA			RETURN <<-524.0905, -1720.4906, 18.2087>>
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN ZERO_VECTOR()
ENDFUNC

FUNC BOOL SC_RESCUE_HAS_SEARCH_AREA_TRIGGERED(INT iSearchArea)
	SWITCH iSearchArea
		CASE SC_RESCUE_SEARCH_AREA
			IF IS_PED_BIT_SET(GET_TAG_ID(eSC_RESCUE_TAG_VIP), ePEDBITSET_PLAYER_HAS_BEEN_NEAR)
				RETURN TRUE
			ENDIF
			IF sLocalVariables.iPedHealthBitset != 0
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

//////////////////////////
///   MAINTAIN
//////////////////////////

PROC SC_RESCUE_SERVER_MAINTAIN()

ENDPROC

FUNC VECTOR GET_BLOOD_DECAL_LOCATION(INT iDecal)
	SWITCH GET_SUBVARIATION()
		CASE FSS_RES_LOCATION_1
			SWITCH serverBD.iRandomModeInt0
				CASE 0
					SWITCH iDecal
						CASE 0						RETURN <<-1616.5692, 4599.3301, 39.4420>>
						CASE 1						RETURN <<-1585.5695, 4607.3892, 29.7542>>
						CASE 2						RETURN <<-1558.9965, 4611.4180, 20.5266>>
						CASE 3						RETURN <<-1535.9507, 4601.9180, 23.4631>>
						CASE 4						RETURN <<-1515.5317, 4606.7646, 34.0848>>
						CASE 5						RETURN <<-1485.3993, 4616.3906, 46.0705>>
						CASE 6						RETURN <<-1479.7871, 4619.8618, 48.8608>>
						CASE 7						RETURN <<-1498.0862, 4610.0903, 41.9923>>
						CASE 8						RETURN <<-1595.4872, 4606.1284, 35.4733>>
						CASE 9						RETURN <<-1630.8337, 4602.0391, 43.0795>>
					ENDSWITCH
				BREAK
				CASE 1
					SWITCH iDecal
						CASE 0						RETURN <<-1639.0260, 4592.9546, 42.6846>>
						CASE 1						RETURN <<-1626.6233, 4564.2378, 42.3613>>
						CASE 2						RETURN <<-1612.3317, 4546.5415, 39.1147>>
						CASE 3						RETURN <<-1607.3014, 4518.2813, 13.0632>>
						CASE 4						RETURN <<-1594.6504, 4503.3691, 19.1978>>
						CASE 5						RETURN <<-1597.2067, 4481.7510, 15.9932>>
						CASE 6						RETURN <<-1608.6019, 4540.9512, 38.2118>>
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH iDecal
						CASE 0						RETURN <<-1622.2794, 4618.4463, 43.6855>>
						CASE 1						RETURN <<-1608.4047, 4637.8125, 46.9848>>
						CASE 2						RETURN <<-1598.4485, 4675.1606, 41.2810>>
						CASE 3						RETURN <<-1602.8699, 4696.3843, 39.9774>>
						CASE 4						RETURN <<-1631.9587, 4689.0054, 37.6492>>
						CASE 5						RETURN <<-1626.9802, 4659.2466, 37.9172>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE FSS_RES_LOCATION_2
			SWITCH serverBD.iRandomModeInt0
				CASE 0
					SWITCH iDecal
						CASE 0						RETURN <<1953.8534, 3462.6121, 40.5341>>
						CASE 1						RETURN <<1981.1675, 3451.5315, 41.6499>>
						CASE 2						RETURN <<2009.2721, 3471.0491, 42.2563>>
						CASE 3						RETURN <<2031.8801, 3502.3254, 40.3563>>
						CASE 4						RETURN <<2058.9250, 3529.0640, 41.3089>>
						CASE 5						RETURN <<2081.6477, 3559.6997, 40.8595>>
					ENDSWITCH
				BREAK
				CASE 1
					SWITCH iDecal
						CASE 0						RETURN <<1951.5027, 3475.3938, 40.3757>>
						CASE 1						RETURN <<1952.5409, 3438.0686, 40.8332>>
						CASE 2						RETURN <<1934.5475, 3406.0520, 41.2673>>
						CASE 3						RETURN <<1917.2399, 3384.4150, 41.9180>>
						CASE 4						RETURN <<1880.8313, 3363.7302, 41.3038>>
						CASE 5						RETURN <<1859.3472, 3343.3254, 42.7127>>
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH iDecal
						CASE 0						RETURN <<1950.9917, 3475.7751, 40.3453>>
						CASE 1						RETURN <<1963.7313, 3450.6738, 40.9106>>
						CASE 2						RETURN <<1986.9916, 3435.1379, 41.0560>>
						CASE 3						RETURN <<2014.1086, 3429.2876, 42.3633>>
						CASE 4						RETURN <<2033.9469, 3422.8313, 43.3581>>
						CASE 5						RETURN <<2045.9325, 3409.3958, 43.4136>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE FSS_RES_LOCATION_3
			SWITCH serverBD.iRandomModeInt0
				CASE 0
					SWITCH iDecal
						CASE 0						RETURN <<1048.8494, -3278.6626, 4.8976>>
						CASE 1						RETURN <<1054.6857, -3293.0242, 4.8995>>
						CASE 2						RETURN <<1066.3003, -3308.3713, 4.9011>>
						CASE 3						RETURN <<1035.6189, -3311.2483, 4.9016>>
						CASE 4						RETURN <<1010.7515, -3313.2583, 8.2794>>
						CASE 5						RETURN <<1027.5836, -3313.2920, 10.9500>>
						CASE 6						RETURN <<1008.9663, -3318.2007, 13.6205>>
					ENDSWITCH
				BREAK
				CASE 1
					SWITCH iDecal
						CASE 0						RETURN <<1033.2338, -3278.0239, 4.8920>>
						CASE 1						RETURN <<1002.3944, -3277.7456, 4.9001>>
						CASE 2						RETURN <<972.6006, -3275.0540, 4.8996>>
						CASE 3						RETURN <<949.9730, -3281.5259, 4.8964>>
						CASE 4						RETURN <<919.4836, -3285.5457, 4.9000>>
						CASE 5						RETURN <<886.7822, -3276.3032, 4.8998>>
						CASE 6						RETURN <<862.1555, -3274.5630, 4.8988>>
						CASE 7						RETURN <<845.8107, -3288.6794, 4.9011>>
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH iDecal
						CASE 0						RETURN <<1053.3444, -3276.5569, 4.8991>>
						CASE 1						RETURN <<1074.9456, -3276.3301, 4.8991>>
						CASE 2						RETURN <<1100.7820, -3276.9634, 4.8987>>
						CASE 3						RETURN <<1126.9572, -3273.3457, 4.8999>>
						CASE 4						RETURN <<1128.7512, -3253.7095, 4.9006>>
						CASE 5						RETURN <<1121.9896, -3244.3843, 4.8948>>
						CASE 6						RETURN <<1108.3905, -3239.6616, 4.8949>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE FSS_RES_LOCATION_4
			SWITCH serverBD.iRandomModeInt0
				CASE 0
					SWITCH iDecal
						CASE 0						RETURN <<1652.3556, -85.1933, 169.7255>>
						CASE 1						RETURN <<1671.4172, -46.9941, 172.7748>>
						CASE 2						RETURN <<1665.7982, -14.1139, 172.7744>>
						CASE 3						RETURN <<1644.8181, 19.5455, 172.7744>>
						CASE 4						RETURN <<1658.7593, 48.1677, 171.3304>>
						CASE 5						RETURN <<1679.8529, 45.7351, 162.0396>>
					ENDSWITCH
				BREAK
				CASE 1
					SWITCH iDecal
						CASE 0						RETURN <<1640.9586, -87.1012, 167.3490>>
						CASE 1						RETURN <<1666.4633, -73.5352, 172.1409>>
						CASE 2						RETURN <<1670.6129, -49.8295, 172.7748>>
						CASE 3						RETURN <<1669.6287, -31.7968, 172.7748>>
						CASE 4						RETURN <<1664.8839, -21.3005, 172.7748>>
						CASE 5						RETURN <<1659.8145, -14.8884, 168.9971>>
						CASE 6						RETURN <<1665.7778, -8.9649, 165.5581>>
						CASE 7						RETURN <<1663.1255, -0.4421, 165.1181>>
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH iDecal
						CASE 0						RETURN <<1625.1682, -74.5698, 165.7917>>
						CASE 1						RETURN <<1630.3413, -67.2338, 159.3705>>
						CASE 2						RETURN <<1630.4575, -61.1328, 155.4128>>
						CASE 3						RETURN <<1626.6375, -57.5107, 147.9962>>
						CASE 4						RETURN <<1621.7072, -46.5601, 134.1182>>
						CASE 5						RETURN <<1614.1357, -41.7075, 133.8481>>
						CASE 6						RETURN <<1604.7755, -37.0791, 129.5614>>
						CASE 7						RETURN <<1587.9365, -33.4461, 120.8084>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE FSS_RES_LOCATION_5
			SWITCH serverBD.iRandomModeInt0
				CASE 0
					SWITCH iDecal
						CASE 0						RETURN <<-524.0905, -1720.4906, 18.2087>>
						CASE 1						RETURN <<-528.9202, -1716.5966, 18.2378>>
						CASE 2						RETURN <<-527.0386, -1696.7373, 18.1321>>
						CASE 3						RETURN <<-541.0480, -1674.0812, 18.2084>>
						CASE 4						RETURN <<-558.5519, -1663.8409, 18.2370>>
					ENDSWITCH
				BREAK
				CASE 1
					SWITCH iDecal
						CASE 0						RETURN <<-521.0469, -1715.2233, 18.3204>>
						CASE 1						RETURN <<-519.5396, -1701.2400, 18.2261>>
						CASE 2						RETURN <<-534.8347, -1680.4755, 18.1320>>
						CASE 3						RETURN <<-551.1736, -1657.3059, 18.2643>>
						CASE 4						RETURN <<-547.3461, -1637.4962, 18.0136>>
						CASE 5						RETURN <<-533.5805, -1624.7036, 16.7979>>
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH iDecal
						CASE 0						RETURN <<-511.0534, -1724.4755, 18.2996>>
						CASE 1						RETURN <<-489.5634, -1737.0814, 17.6810>>
						CASE 2						RETURN <<-468.9920, -1728.9727, 17.6560>>
						CASE 3						RETURN <<-463.0661, -1709.4110, 17.6443>>
						CASE 4						RETURN <<-464.6268, -1692.7426, 17.9179>>
						CASE 5						RETURN <<-467.4155, -1675.0481, 18.0532>>
						CASE 6						RETURN <<-462.2230, -1666.6300, 18.0440>>
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

PROC REMOVE_BLOOD_DECALS()
	INT iDecal
	REPEAT MAX_NUM_DECALS iDecal
		IF IS_BIT_SET(sLocalVariables.sDecals.iDecalBitSet, iDecal)
			REMOVE_DECAL(sLocalVariables.sDecals.decalId[iDecal])
		ENDIF
	ENDREPEAT
ENDPROC

PROC MAINTAIN_BLOOD_DECALS()
	VECTOR vLocation = GET_BLOOD_DECAL_LOCATION(sLocalVariables.sDecals.iDecalCreationStagger)
	IF NOT IS_VECTOR_ZERO(vLocation)
		IF NOT IS_BIT_SET(sLocalVariables.sDecals.iDecalBitSet, sLocalVariables.sDecals.iDecalCreationStagger)
			IF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vLocation, 30.0)
				sLocalVariables.sDecals.decalId[sLocalVariables.sDecals.iDecalCreationStagger] = ADD_DECAL(DECAL_RSID_BLOOD_SPLATTER, vLocation, <<0.0, 0.0, -1.0>>, normalise_vector(<<0.0, 1.0, 0.0>>), 1.0, 1.0, 0.196, 0, 0, 1.0, -1)		
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_BLOOD_DECALS - Created decal ", sLocalVariables.sDecals.iDecalCreationStagger)
				SET_BIT(sLocalVariables.sDecals.iDecalBitSet, sLocalVariables.sDecals.iDecalCreationStagger)
			ENDIF
		ENDIF
		sLocalVariables.sDecals.iDecalCreationStagger++
	ELSE
		sLocalVariables.sDecals.iDecalCreationStagger = 0
	ENDIF
ENDPROC

PROC MAINTAIN_FLARE_SHOT()
	IF NOT IS_MISSION_SERVER_BIT_SET(eMISSIONSERVERBITSET_SHOT_FLARE)
	AND NOT IS_MISSION_CLIENT_BIT_SET(LOCAL_PARTICIPANT_INDEX, eMISSIONCLIENTBITSET_SHOT_FLARE)
		IF IS_ENTITY_IN_RANGE_COORDS(LOCAL_PED_INDEX, GET_FLARE_FIRE_POINT(), SC_RESCUE_FLARE_SHOT_RANGE)
			VECTOR vFirePoint = GET_FLARE_FIRE_POINT()
			VECTOR vTargetPoint = vFirePoint+<<GET_RANDOM_FLOAT_IN_RANGE(-20.0, 20.0), GET_RANDOM_FLOAT_IN_RANGE(-20.0, 20.0), SC_RESCUE_FLARE_SHOT_TARGET_HEIGHT>>
			
			IF NOT IS_VECTOR_ZERO(vFirePoint)
			AND NOT IS_VECTOR_ZERO(vTargetPoint)
				SHOOT_SINGLE_BULLET_BETWEEN_COORDS(vFirePoint, vTargetPoint, 0, TRUE, WEAPONTYPE_DLC_FLAREGUN)
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_FLARE_SHOT - Fired flare from ", vFirePoint, " to ", vTargetPoint)
			ELSE
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_FLARE_SHOT - vfirePoint is at origin, variation doesnt nee flare shot.")
			ENDIF

			SET_MISSION_CLIENT_BIT(eMISSIONCLIENTBITSET_SHOT_FLARE)
		ENDIF
	ENDIF
ENDPROC

PROC SC_RESCUE_CLIENT_MAINTAIN()
	MAINTAIN_BLOOD_DECALS()
	MAINTAIN_FLARE_SHOT()
ENDPROC

//////////////////////////
///   DIALOGUE
//////////////////////////

FUNC STRING SC_RESCUE_GET_DIALOGUE_ROOT(INT iDialogue)
	SWITCH iDialogue
		CASE SC_RESCUE_DIALOGUE_OPENING
			SWITCH GET_SUBVARIATION()
				CASE FSS_RES_LOCATION_1		RETURN "FXFR_SRE_1A"
				CASE FSS_RES_LOCATION_2		RETURN "FXFR_SRE_2A"
				CASE FSS_RES_LOCATION_3		RETURN "FXFR_SRE_3A"
				CASE FSS_RES_LOCATION_4		RETURN "FXFR_SRE_4A"
				CASE FSS_RES_LOCATION_5		RETURN "FXFR_SRE_5A"
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC STRING SC_RESCUE_GET_DIALOGUE_SUBTITLE_GROUP(INT iDialogue)
	SWITCH iDialogue
		CASE SC_RESCUE_DIALOGUE_OPENING		RETURN "FXFRAUD"
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC STRING SC_RESCUE_GET_DIALOGUE_CHARACTER(INT iDialogue, INT iSpeaker)
	UNUSED_PARAMETER(iSpeaker)
	SWITCH iDialogue
		CASE SC_RESCUE_DIALOGUE_OPENING		RETURN "FIX_FRANKLIN"
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC BOOL SC_RESCUE_SHOULD_TRIGGER_DIALOGUE(INT iDialogue)
	SWITCH iDialogue
		CASE SC_RESCUE_DIALOGUE_OPENING		RETURN TRUE
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC INT SC_RESCUE_DIALOGUE_DELAY(INT iDialogue)
	SWITCH iDialogue
		CASE SC_RESCUE_DIALOGUE_OPENING		RETURN 2000
	ENDSWITCH
	RETURN 0
ENDFUNC

FUNC BOOL SC_RESCUE_DIALOGUE_USE_HEADSET_AUDIO(INT iDialogue)
	SWITCH iDialogue
		CASE SC_RESCUE_DIALOGUE_OPENING		RETURN FALSE
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC enumCharacterList SC_RESCUE_DIALOGUE_PHONE_CHARACTER(INT iDialogue)
	SWITCH iDialogue
		CASE SC_RESCUE_DIALOGUE_OPENING		RETURN CHAR_FIXER_FRANKLIN
	ENDSWITCH
	RETURN CHAR_FIXER_FRANKLIN
ENDFUNC

FUNC INT SC_RESCUE_DIALOGUE_SPEAKER_ID(INT iDialogue, INT iSpeaker)
	UNUSED_PARAMETER(iSpeaker)
	SWITCH iDialogue
		CASE SC_RESCUE_DIALOGUE_OPENING		RETURN 2
	ENDSWITCH
	RETURN -1
ENDFUNC

//////////////////////////
///   AMBUSH
//////////////////////////

FUNC BOOL SC_RESCUE_SHOULD_AMBUSH_ACTIVATE()
	IF DOES_ENTITY_EXIST(SC_RESCUE_GET_VIP_PED_INDEX())
		RETURN IS_PED_IN_GROUP(SC_RESCUE_GET_VIP_PED_INDEX())
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC MODEL_NAMES SC_RESCUE_GET_AMBUSH_VEHICLE_MODEL_OVERRIDE()
	PED_INDEX pedID = SC_RESCUE_GET_VIP_PED_INDEX()
	IF DOES_ENTITY_EXIST(pedID)
		IF IS_PED_IN_ANY_PLANE(pedID)
		OR IS_PED_IN_ANY_HELI(pedID)
			IF GET_SUBVARIATION() = FSS_RES_LOCATION_1
				RETURN MAVERICK
			ELSE
				RETURN BUZZARD2
			ENDIF
		ENDIF
	ENDIF
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

FUNC eENTITY_TYPE SC_RESCUE_GET_AMBUSH_TARGET_ENTITY_TYPE()
	RETURN eENTITYTYPE_PED
ENDFUNC

//////////////////////////
///   VEHICLES
//////////////////////////

PROC SC_RESCUE_VEHICLE_SPAWN_ATTRIBUTES(INT iVehicle, VEHICLE_INDEX vehID)
	
	BOOL bTurnOnRadio = FALSE
	IF iVehicle = GET_TAG_ID(eSC_RESCUE_TAG_FUGITIVE)
	OR iVehicle = GET_TAG_ID(eSC_RESCUE_TAG_GRANGER)
		bTurnOnRadio = TRUE
		SET_VEHICLE_DOOR_OPEN(vehID, SC_DOOR_FRONT_LEFT, DEFAULT, TRUE)
		SET_VEHICLE_DOOR_OPEN(vehID, SC_DOOR_FRONT_RIGHT, DEFAULT, TRUE)
		IF GET_SUBVARIATION() != FSS_RES_LOCATION_1
			SET_VEHICLE_DOOR_OPEN(vehID, SC_DOOR_REAR_LEFT, DEFAULT, TRUE)
			SET_VEHICLE_DOOR_OPEN(vehID, SC_DOOR_REAR_RIGHT, DEFAULT, TRUE)
		ENDIF
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] SC_RESCUE_VEHICLE_SPAWN_ATTRIBUTES - Opened doors of vehicle ", iVehicle)
		
	ELIF iVehicle = GET_TAG_ID(eSC_RESCUE_TAG_CRASH)
		IF GET_SUBVARIATION() = FSS_RES_LOCATION_3
		OR GET_SUBVARIATION() = FSS_RES_LOCATION_4
			bTurnOnRadio = TRUE
			SET_VEHICLE_DOOR_OPEN(vehID, SC_DOOR_FRONT_LEFT)
			SET_VEHICLE_DOOR_OPEN(vehID, SC_DOOR_FRONT_RIGHT)
			SET_VEHICLE_DOOR_OPEN(vehID, SC_DOOR_REAR_LEFT)
			SET_VEHICLE_DOOR_OPEN(vehID, SC_DOOR_BONNET, TRUE)
			
			SET_VEHICLE_DIRT_LEVEL(vehID, GET_RANDOM_FLOAT_IN_RANGE(10.0, 14.0))
			
			VECTOR vDamageCoord = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(vehID), GET_ENTITY_HEADING(vehID), <<0.2, 0.4, 0.0>>)
			SET_VEHICLE_DAMAGE(vehID, vDamageCoord, GET_RANDOM_FLOAT_IN_RANGE(50, 75), 500, FALSE)	
			
			SET_VEHICLE_ENGINE_HEALTH(vehID, GET_VEHICLE_ENGINE_HEALTH(vehID)/4.0)
		ENDIF
	ENDIF
	
	IF IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_AMBUSH_VEHICLE)
	AND GET_ENTITY_MODEL(vehID) = MAVERICK
		SET_VEHICLE_DIRT_LEVEL(vehID, SC_RESCUE_MAVERICK_DIRT_LEVEL)
	ENDIF
	
	IF bTurnOnRadio
		SET_VEHICLE_ENGINE_ON(vehId, TRUE, TRUE)
		SET_VEHICLE_RADIO_ENABLED(vehId, TRUE)
		SET_VEHICLE_RADIO_LOUD(vehId, TRUE)
		SET_VEH_FORCED_RADIO_THIS_FRAME(vehId)
		IF GET_SUBVARIATION() = FSS_RES_LOCATION_1
			SET_VEH_RADIO_STATION(vehId, "RADIO_06_COUNTRY")
		ELSE
			SET_VEH_RADIO_STATION(vehId, "RADIO_01_CLASS_ROCK") 
		ENDIF
	ENDIF
ENDPROC

PROC SC_RESCUE_MAINTAIN_VEHICLE_CLIENT(INT iVehicle, VEHICLE_INDEX vehId, BOOL bDriveable, BOOL bInVehicle, INT iMissionEntityForCarrier)
	UNUSED_PARAMETER(bInVehicle)
	UNUSED_PARAMETER(iMissionEntityForCarrier)
	IF iVehicle = GET_TAG_ID(eSC_RESCUE_TAG_FUGITIVE)
	OR iVehicle = GET_TAG_ID(eSC_RESCUE_TAG_GRANGER)
	OR (iVehicle = GET_TAG_ID(eSC_RESCUE_TAG_CRASH) AND ((GET_SUBVARIATION() = FSS_RES_LOCATION_3) OR (GET_SUBVARIATION() = FSS_RES_LOCATION_4)))
		IF bDriveable
			IF NETWORK_HAS_CONTROL_OF_ENTITY(vehId)
				SET_VEH_FORCED_RADIO_THIS_FRAME(vehId)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC FLOAT SC_RESCUE_VEHICLE_NEAR_RANGE(INT iVehicle)
	UNUSED_PARAMETER(iVehicle)
	RETURN 10.0
ENDFUNC

FUNC BOOL SC_RESCUE_VEHICLE_MODS(INT iVehicle, VEHICLE_SETUP_STRUCT_MP &sData)

	IF IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_AMBUSH_VEHICLE)
	AND SC_RESCUE_GET_AMBUSH_VEHICLE_MODEL_OVERRIDE() = MAVERICK
		sData.VehicleSetup.eModel = MAVERICK // MAVERICK
		sData.VehicleSetup.iPlateIndex = 4
		sData.VehicleSetup.iColour1 = 10
		sData.VehicleSetup.iColour2 = 0
		sData.VehicleSetup.iColourExtra1 = 5
		sData.VehicleSetup.iColourExtra2 = 156
		sData.iColour5 = 1
		sData.iColour6 = 132
		sData.iLivery2 = 0
		sData.VehicleSetup.iTyreR = 255
		sData.VehicleSetup.iTyreG = 255
		sData.VehicleSetup.iTyreB = 255
		sData.VehicleSetup.iNeonR = 255
		sData.VehicleSetup.iNeonB = 255
		SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_0)
		SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_1)
		SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_6)
		SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_9)
		SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_10)
		SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_BULLETPROOF_TYRES)
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

//////////////////////////
///   HELP
//////////////////////////

FUNC BOOL SC_RESCUE_HELP_TRIGGER(INT iHelp)
	SWITCH iHelp
		CASE SC_RESCUE_HELP_OPENING			RETURN IS_IT_SAFE_FOR_OPENING_HELP_TEXT()
		CASE SC_RESCUE_HELP_SECURE_AREA		RETURN GET_CLIENT_MODE_STATE() = eMODESTATE_TAKE_OUT_TARGETS
		CASE SC_RESCUE_HELP_LOOK_LISTEN
			IF GET_MODE_STATE() = eMODESTATE_SEARCH_AREA
				IF GET_SUBVARIATION() = FSS_RES_LOCATION_1
				OR GET_SUBVARIATION() = FSS_RES_LOCATION_2
				OR GET_SUBVARIATION() = FSS_RES_LOCATION_5
					RETURN IS_PROP_CLIENT_BIT_SET(GET_TAG_ID(eSC_RESCUE_TAG_CRASH), LOCAL_PARTICIPANT_INDEX, ePROPCLIENTBITSET_PLAYER_HAS_BEEN_NEAR)
				ELSE
					RETURN IS_VEHICLE_CLIENT_BIT_SET(GET_TAG_ID(eSC_RESCUE_TAG_CRASH), LOCAL_PARTICIPANT_INDEX, eVEHICLECLIENTBITSET_PLAYER_IS_NEAR_ME)
				ENDIF
			ENDIF
		BREAK
		CASE SC_RESCUE_HELP_SEARCH_AREA		RETURN (GET_CLIENT_MODE_STATE() = eMODESTATE_SEARCH_AREA) AND (sSearchArea.sAreas[SC_RESCUE_SEARCH_AREA].fCentreDistanceSquared < (data.SearchArea.Areas[SC_RESCUE_SEARCH_AREA].fEnterRadius * data.SearchArea.Areas[SC_RESCUE_SEARCH_AREA].fEnterRadius))
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC STRING SC_RESCUE_HELP_TEXT(INT iHelp)
	SWITCH iHelp
		// If subvariation is FSS_RES_LOCATION_4, use the alternate female gendered string for non-English languages
		CASE SC_RESCUE_HELP_OPENING			RETURN PICK_STRING(GET_SUBVARIATION() = FSS_RES_LOCATION_4, "FIX_HLP_RES_0B", "FIX_HLP_RES_0")
		CASE SC_RESCUE_HELP_SECURE_AREA		RETURN PICK_STRING(GET_SUBVARIATION() = FSS_RES_LOCATION_4, "FIX_HLP_RES_1B", "FIX_HLP_RES_1")
		CASE SC_RESCUE_HELP_LOOK_LISTEN		
			SWITCH GET_SUBVARIATION()
				CASE FSS_RES_LOCATION_5
					RETURN "FIX_HLP_RES_3"
				BREAK
				DEFAULT
					PICK_STRING(GET_SUBVARIATION() = FSS_RES_LOCATION_4, "FIX_HLP_RES_2B", "FIX_HLP_RES_2")
				BREAK
			ENDSWITCH
		BREAK
		CASE SC_RESCUE_HELP_SEARCH_AREA		RETURN PICK_STRING(GET_SUBVARIATION() = FSS_RES_LOCATION_4, "FIX_HLP_RES_4B", "FIX_HLP_RES_4")
	ENDSWITCH
	RETURN ""
ENDFUNC

//////////////////////////
///   TAKE OUT TARGET
//////////////////////////

FUNC BOOL SC_RESCUE_ENABLE_TAKE_OUT_TARGETS_HUD(INT iHUD)
	UNUSED_PARAMETER(iHUD)
	RETURN FALSE
ENDFUNC

FUNC BOOL SC_RESCUE_ENABLE_TAKE_OUT_TARGETS_PED_MARKER(INT iPed)
	UNUSED_PARAMETER(iPed)
	RETURN FALSE
ENDFUNC

//////////////////////////
///   PROPS
//////////////////////////

FUNC FLOAT SC_RESCUE_PROP_NEAR_RANGE(INT iProp)
	UNUSED_PARAMETER(iProp)
	RETURN 10.0
ENDFUNC

//////////////////////////
///   BOTTOM RIGHT HUD
//////////////////////////

FUNC BOOL SC_RESCUE_BOTTOM_RIGHT_HUD_ENABLED(INT iHUD)
	SWITCH iHUD
		CASE SC_RESCUE_BOTTOM_RIGHT_HUD_CLIENT_HEALTH
			IF GET_MODE_STATE() >= eMODESTATE_COLLECT_MISSION_ENTITY
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC SC_RESCUE_BOTTOM_RIGHT_HUD_DRAW(INT iHUD)
	SWITCH iHUD
		CASE SC_RESCUE_BOTTOM_RIGHT_HUD_CLIENT_HEALTH
			IF DOES_ENTITY_EXIST(SC_RESCUE_GET_VIP_PED_INDEX())
				INT iVal, iMaxVal
				HUD_COLOURS eColour
				iVal = GET_ENTITY_HEALTH(SC_RESCUE_GET_VIP_PED_INDEX())
				iMaxVal = GET_ENTITY_MAX_HEALTH(SC_RESCUE_GET_VIP_PED_INDEX())
				IF iVal <= iMaxVal/4
					eColour = HUD_COLOUR_RED
				ENDIF
				
				DRAW_GENERIC_METER(iVal, iMaxVal, PICK_STRING(GET_SUBVARIATION() = FSS_RES_LOCATION_4, "FXR_HUD_RE_HEB", "FXR_HUD_RE_HEA"), eColour, DEFAULT, HUDORDER_THIRDBOTTOM)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

//////////////////////////
///   AUDIO
//////////////////////////

FUNC INT SC_RESCUE_GET_NEXT_MUSIC_EVENT(INT iCurrentEvent)
	SWITCH INT_TO_ENUM(SC_RESCUE_MUSIC, iCurrentEvent)
	
		CASE eSC_RESCUE_MUSIC_INIT
			RETURN ENUM_TO_INT(eSC_RESCUE_MUSIC_START)
	
		CASE eSC_RESCUE_MUSIC_START
			IF GET_CLIENT_MODE_STATE() = eMODESTATE_SEARCH_AREA
				RETURN ENUM_TO_INT(eSC_RESCUE_MUSIC_SEARCH)
			ENDIF
		BREAK
		
		CASE eSC_RESCUE_MUSIC_SEARCH
			IF GET_CLIENT_MODE_STATE() >= eMODESTATE_COLLECT_MISSION_ENTITY
				RETURN ENUM_TO_INT(eSC_RESCUE_MUSIC_DELIVER)
			ENDIF
		BREAK
				
		CASE eSC_RESCUE_MUSIC_DELIVER

		BREAK
	ENDSWITCH
	
	RETURN -1
ENDFUNC

FUNC STRING SC_RESCUE_GET_MUSIC_EVENT_STRING(INT iEvent)
	SWITCH INT_TO_ENUM(SC_RESCUE_MUSIC, iEvent)
		CASE eSC_RESCUE_MUSIC_START
			RETURN "FIXER_DELIVERING_START"
		
		CASE eSC_RESCUE_MUSIC_SEARCH
			RETURN "FIXER_MUSIC_STOP"
		
		CASE eSC_RESCUE_MUSIC_DELIVER
			RETURN "FIXER_MED_INTENSITY_START"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

//////////////////////////
///   FLARE
//////////////////////////

FUNC BOOL SC_RESCUE_GET_FLARE_ENABLED(INT iFlare)
	SWITCH iFlare
		CASE SC_RESCUE_CRASH_SITE_FLARE		RETURN TRUE
	ENDSWITCH
	RETURN FALSE
ENDFUNC

//////////////////////////
///   TICKERS
//////////////////////////

FUNC STRING SC_RESCUE_GET_TICKER_STRING_LOCAL(MISSION_TICKER_DATA &sTickerData)
	SWITCH INT_TO_ENUM(SC_RESCUE_TICKERS, sTickerData.iInt1)
		CASE eSC_RESCUE_TICKER_LOCATED
			RETURN PICK_STRING(GET_SUBVARIATION() = FSS_RES_LOCATION_4, "FXR_TCK_RE_LOCLB", "FXR_TCK_RE_LOCL")
		
		CASE eSC_RESCUE_TICKER_COLLECTED
			RETURN PICK_STRING(GET_SUBVARIATION() = FSS_RES_LOCATION_4, "FXR_TCK_RE_COLLB", "FXR_TCK_RE_COLL")
		
		CASE eSC_RESCUE_TICKER_SECURED
			RETURN PICK_STRING(GET_SUBVARIATION() = FSS_RES_LOCATION_4, "FXR_TCK_RE_SECLB", "FXR_TCK_RE_SECL")
		
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC STRING SC_RESCUE_GET_TICKER_STRING_REMOTE(MISSION_TICKER_DATA &sTickerData)
	SWITCH INT_TO_ENUM(SC_RESCUE_TICKERS, sTickerData.iInt1)
		CASE eSC_RESCUE_TICKER_LOCATED
			RETURN PICK_STRING(GET_SUBVARIATION() = FSS_RES_LOCATION_4, "FXR_TCK_RE_LOCRB", "FXR_TCK_RE_LOCR")
		
		CASE eSC_RESCUE_TICKER_COLLECTED
			RETURN PICK_STRING(GET_SUBVARIATION() = FSS_RES_LOCATION_4, "FXR_TCK_RE_COLRB", "FXR_TCK_RE_COLR")
		
		CASE eSC_RESCUE_TICKER_SECURED
			RETURN PICK_STRING(GET_SUBVARIATION() = FSS_RES_LOCATION_4, "FXR_TCK_RE_SECRB", "FXR_TCK_RE_SECR")
		
	ENDSWITCH
	
	RETURN ""
ENDFUNC

//////////////////////////
///   MISSION ENTITY
//////////////////////////

FUNC BOOL SC_RESCUE_SHOULD_BLIP_MISSION_ENTITY(INT iEntity)
	UNUSED_PARAMETER(iEntity)
	RETURN FALSE
ENDFUNC

//////////////////////////
///   FOCUS CAM
//////////////////////////

FUNC BOOL SC_RESCUE_FOCUS_CAM_ENABLE(INT iCam)
	SWITCH iCam
		CASE SC_RESCUE_FOCUS_CAM_FLARE
			IF NOT IS_MISSION_LOCAL_BIT_SET(eMISSIONLOCALBITSET_DONE_FORCED_FOCUS)
				IF IS_MISSION_CLIENT_BIT_SET(LOCAL_PARTICIPANT_INDEX, eMISSIONCLIENTBITSET_SHOT_FLARE)
					IF NOT HAS_NET_TIMER_EXPIRED(sLocalVariables.stGeneralTimer, 3000)
						PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] SC_RESCUE_FOCUS_CAM_ENABLE - Focus cam running this frame")
						RETURN TRUE
					ENDIF
					RESET_NET_TIMER(sLocalVariables.stGeneralTimer)
					PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] SC_RESCUE_FOCUS_CAM_ENABLE - Done with focus cam")
					SET_MISSION_LOCAL_BIT(eMISSIONLOCALBITSET_DONE_FORCED_FOCUS)
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL SC_RESCUE_FOCUS_CAM_FORCE_ACTIVE(INT iCam)
	SWITCH iCam
		CASE SC_RESCUE_FOCUS_CAM_FLARE		RETURN SC_RESCUE_FOCUS_CAM_ENABLE(iCam)
	ENDSWITCH
	RETURN FALSE
ENDFUNC

//////////////////////////
///   CLEANUP
//////////////////////////

PROC SC_RESCUE_SCRIPT_CLEANUP()
	REMOVE_BLOOD_DECALS()
ENDPROC


//////////////////////////
///   LOGIC
//////////////////////////

PROC SC_RESCUE_SETUP_LOGIC()
	logic.Data.Setup = &SC_RESCUE_SET_PLACEMENT_DATA
	
	logic.StateMachine.SetupStates = &SC_RESCUE_SETUP_STATES
	logic.StateMachine.SetupClientStates = &SC_RESCUE_SETUP_CLIENT_STATES
	
	logic.Init.Server = &SC_RESCUE_SERVER_INIT
	logic.Init.Client = &SC_RESCUE_CLIENT_INIT
	
	logic.Ped.Attributes = &SC_RESCUE_SET_PED_ATTRIBUTES
	logic.Ped.Behaviour.Setup = &SC_RESCUE_SETUP_PED_TASKS
	logic.Ped.Behaviour.Get = &SC_RESCUE_GET_PED_BEHAVIOUR
	logic.Ped.Triggers.Setup = &SC_RESCUE_SETUP_PED_TRIGGERS
	logic.Ped.Triggers.PlayerNearPed.Range = &SC_RESCUE_GET_NEAR_PED_RANGE
	logic.Ped.Triggers.PlayerNearPed.PersistentCheck = &SC_RESCUE_GET_NEAR_PED_SHOULD_RUN_PERSISTENT_CHECK
	logic.Ped.Triggers.PlayerNearPed.OnChanged = &SC_RESCUE_NEAR_PED_ON_CHANGE
	logic.Ped.Task.FollowPlayer.Enable = &SC_RESCUE_IS_FOLLOW_PLAYER_ENABLED
	logic.Ped.Task.FollowPlayer.MaxMoveBlendRatio = &SC_RESCUE_GET_FOLLOW_PLAYER_MAX_MOVE_BLEND_RATIO
	logic.Ped.Task.FollowPlayer.IdleBehaviour = &SC_RESCUE_GET_FOLLOW_PLAYER_IDLE_BEHAVIOUR
	logic.Ped.Task.FollowPlayer.JoinRange = &SC_RESCUE_GET_FOLLOW_PLAYER_JOIN_RANGE
	logic.Ped.Task.FollowPlayer.OnJoinGroup = &SC_RESCUE_FOLLOW_PLAYER_ON_JOIN_GROUP
	logic.Ped.Client = &SC_RESCUE_PED_CLIENT_MAINTAIN
	logic.Ped.Server = &SC_RESCUE_PED_SERVER_MAINTAIN
	logic.Ped.Blip.Enable = &SC_RESCUE_PED_BLIP_ENABLE
	logic.Ped.Blip.Sprite = &SC_RESCUE_PED_BLIP_SPRITE
	logic.Ped.Blip.Colour = &SC_RESCUE_PED_BLIP_COLOUR
	logic.Ped.Blip.Name = &SC_RESCUE_PED_BLIP_NAME
	logic.Ped.Blip.Scale = &SC_RESCUE_PED_BLIP_SCALE
	logic.Ped.Blip.Priority = &SC_RESCUE_PED_BLIP_PRIORITY
	logic.Ped.Blip.DrawMarker = &SC_RESCUE_PED_BLIP_SHOULD_DRAW_MARKER
	logic.Ped.Blip.MarkerScale = &SC_RESCUE_PED_BLIP_GET_MARKER_SCALE
	logic.Ped.Blip.FlashOnCreation = &SC_RESCUE_PED_BLIP_FLASH_ON_CREATION
	logic.Ped.DisableGenericEnemyPedBlip = &SC_RESCUE_DISABLE_GENERIC_ENEMY_PED_BLIP
	logic.Ped.Task.DoScenario.ScenarioName = &SC_RESCUE_PED_GET_SCENARIO
	logic.Ped.Task.PlayAnim.AnimDict = &SC_RESCUE_PED_GET_PLAY_ANIM_DIC
	logic.Ped.Task.PlayAnim.Anim = &SC_RESCUE_PED_GET_PLAY_ANIM_ANIM
	logic.Ped.Task.AttackHatedTargets.Range = &SC_RESCUE_PED_GET_ATTACK_HATED_RANGE
	logic.Ped.Task.WalkToPoint.UseNavMesh = &SC_RESCUE_PED_TASK_WALK_TO_POINT_USE_NAV_MESH
	logic.Ped.Task.WalkToPoint.Coords = &SC_RESCUE_PED_TASK_WALK_TO_POINT_COORDS
	logic.Ped.Task.WalkToPoint.MoveBlendRatio = &SC_RESCUE_PED_TASK_WALK_TO_POINT_MOVE_BLEND_RATIO
	logic.Ped.Task.WalkToPoint.TimeBeforeWarp = &SC_RESCUE_PED_TASK_WALK_TO_POINT_TIME_BEFORE_WARP
	logic.Ped.Task.WalkToPoint.FinalHeading = &SC_RESCUE_PED_TASK_WALK_TO_POINT_FINAL_HEALING
	
	logic.SearchArea.Enable = &SC_RESCUE_ENABLE_SEARCH_AREAS
	logic.SearchArea.EnableArea = &SC_RESCUE_SEARCH_AREA_ENABLED
	logic.SearchArea.ForceBlipCoords = &SC_RESCUE_GET_SEARCH_AREA_FORCED_BLIP_COORDS
	logic.SearchArea.HasTriggered = &SC_RESCUE_HAS_SEARCH_AREA_TRIGGERED
	
	logic.Maintain.Server = &SC_RESCUE_SERVER_MAINTAIN
	logic.Maintain.Client = &SC_RESCUE_CLIENT_MAINTAIN
	
	logic.Dialogue.Root = &SC_RESCUE_GET_DIALOGUE_ROOT
	logic.Dialogue.SubtitleGroup = &SC_RESCUE_GET_DIALOGUE_SUBTITLE_GROUP
	logic.Dialogue.Character = &SC_RESCUE_GET_DIALOGUE_CHARACTER
	logic.Dialogue.ShouldTrigger = &SC_RESCUE_SHOULD_TRIGGER_DIALOGUE
	logic.Dialogue.Delay = &SC_RESCUE_DIALOGUE_DELAY
	logic.Dialogue.UseHeadsetAudio = &SC_RESCUE_DIALOGUE_USE_HEADSET_AUDIO
	logic.Dialogue.CellphoneCharacter = &SC_RESCUE_DIALOGUE_PHONE_CHARACTER
	logic.Dialogue.SpeakerID = &SC_RESCUE_DIALOGUE_SPEAKER_ID
	
	logic.Ambush.ShouldActivate = &SC_RESCUE_SHOULD_AMBUSH_ACTIVATE
	logic.Ambush.OverrideVehicleModel = &SC_RESCUE_GET_AMBUSH_VEHICLE_MODEL_OVERRIDE
	logic.Ambush.TargetEntityType = &SC_RESCUE_GET_AMBUSH_TARGET_ENTITY_TYPE
	
	logic.Vehicle.Attributes = &SC_RESCUE_VEHICLE_SPAWN_ATTRIBUTES
	logic.Vehicle.Client = &SC_RESCUE_MAINTAIN_VEHICLE_CLIENT
	logic.Vehicle.NearRange = &SC_RESCUE_VEHICLE_NEAR_RANGE
	logic.Vehicle.Mods = &SC_RESCUE_VEHICLE_MODS
	
	logic.HelpText.Trigger = &SC_RESCUE_HELP_TRIGGER
	logic.HelpText.Text = &SC_RESCUE_HELP_TEXT
	
	logic.TakeOutTarget.BottomRightHUD.Enable = &SC_RESCUE_ENABLE_TAKE_OUT_TARGETS_HUD
	logic.TakeOutTarget.DrawPedArrow = &SC_RESCUE_ENABLE_TAKE_OUT_TARGETS_PED_MARKER
	
	logic.Prop.NearRange = &SC_RESCUE_PROP_NEAR_RANGE
	
	logic.BottomRightHUD.Enable = &SC_RESCUE_BOTTOM_RIGHT_HUD_ENABLED
	logic.BottomRightHUD.DrawCustom = &SC_RESCUE_BOTTOM_RIGHT_HUD_DRAW
	
	logic.Audio.MusicTrigger.Event = &SC_RESCUE_GET_NEXT_MUSIC_EVENT
	logic.Audio.MusicTrigger.EventString = &SC_RESCUE_GET_MUSIC_EVENT_STRING
	logic.Audio.MusicTrigger.StopString = &GET_FIXER_SECURITY_CONTRACT_MUSIC_STOP_EVENT_STRING
	logic.Audio.MusicTrigger.FailString = &GET_FIXER_SECURITY_CONTRACT_MUSIC_FAIL_EVENT_STRING
	
	logic.Flare.Enabled = &SC_RESCUE_GET_FLARE_ENABLED
	
	logic.Hud.Tickers.CustomStringLocal = &SC_RESCUE_GET_TICKER_STRING_LOCAL
	logic.Hud.Tickers.CustomStringRemote = &SC_RESCUE_GET_TICKER_STRING_REMOTE
	
	logic.MissionEntity.Blip.Enable = &SC_RESCUE_SHOULD_BLIP_MISSION_ENTITY
	
	logic.FocusCam.Enable = &SC_RESCUE_FOCUS_CAM_ENABLE
	logic.FocusCam.ForceActive = &SC_RESCUE_FOCUS_CAM_FORCE_ACTIVE
	
	logic.Cleanup.ScriptEnd = &SC_RESCUE_SCRIPT_CLEANUP
	
ENDPROC
