
USING "globals.sch"
USING "rage_builtins.sch"

CONST_INT ENABLE_CONTENT	FEATURE_FIXER

#IF ENABLE_CONTENT
USING "Variations/fm_content_payphone_hit_approach.sch"
USING "Variations/fm_content_payphone_hit_attack.sch"
USING "Variations/fm_content_payphone_hit_construction.sch"
USING "Variations/fm_content_payphone_hit_golf.sch"
USING "Variations/fm_content_payphone_hit_joyrider.sch"
USING "Variations/fm_content_payphone_hit_list.sch"
USING "Variations/fm_content_payphone_hit_motel.sch"
USING "Variations/fm_content_payphone_hit_taxi.sch"

/// PURPOSE: Use to set up the logic pointers for your mission
///    
PROC SETUP_MISSION_LOGIC()
	SWITCH GET_VARIATION()
		CASE FPV_TAXI 				PAYTAX_SETUP_LOGIC()		BREAK
		CASE FPV_GOLF				GOLF_SETUP_LOGIC()			BREAK
		CASE FPV_MOTEL				PAYMOT_SETUP_LOGIC()		BREAK
		CASE FPV_HIT_LIST			HITLIST_SETUP_LOGIC()		BREAK
		CASE FPV_COORD_ATTACK		PAYATTACK_SETUP_LOGIC()		BREAK
		CASE FPV_CONSTRUCTION		CONSTRUCTION_SETUP_LOGIC()	BREAK
		CASE FPV_JOYRIDER			PAYJOY_SETUP_LOGIC()		BREAK
		CASE FPV_APPROACH			APPVEHICLES_SETUP_LOGIC()	BREAK
		#IF IS_DEBUG_BUILD
		DEFAULT
			SCRIPT_ASSERT("Please populate SETUP_MISSION_LOGIC in fm_content_payphone_hit_variations.sch")
		BREAK
		#ENDIF
	ENDSWITCH
ENDPROC
#ENDIF
