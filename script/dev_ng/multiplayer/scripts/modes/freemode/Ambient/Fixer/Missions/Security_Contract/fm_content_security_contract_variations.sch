
USING "globals.sch"
USING "rage_builtins.sch"

CONST_INT ENABLE_CONTENT	FEATURE_FIXER

#IF ENABLE_CONTENT
USING "Variations/fm_content_security_contract_assault.sch"
USING "Variations/fm_content_security_contract_protect.sch"
USING "Variations/fm_content_security_contract_recover_package.sch"
USING "Variations/fm_content_security_contract_recover_vehicle.sch"
USING "Variations/fm_content_security_contract_rescue.sch"
USING "Variations/fm_content_security_contract_tail.sch"

/// PURPOSE: Use to set up the logic pointers for your mission
///    
PROC SETUP_MISSION_LOGIC()
	SWITCH GET_VARIATION()
		CASE FSV_RECOVERY_VEHICLE 			SCRV_SETUP_LOGIC()		BREAK
		CASE FSV_RECOVERY_PACKAGE			SCRP_SETUP_LOGIC()		BREAK
		CASE FSV_RECOVERY_ASSAULT			SCAS_SETUP_LOGIC()		BREAK
		CASE FSV_RECOVERY_RESCUE			SC_RESCUE_SETUP_LOGIC()		BREAK
		CASE FSV_RECOVERY_PROTECT			SC_PROTECT_SETUP_LOGIC()	BREAK
		CASE FSV_RECOVERY_TAIL				SC_TAIL_SETUP_LOGIC()		BREAK
	ENDSWITCH
ENDPROC
#ENDIF
