
USING "globals.sch"
USING "rage_builtins.sch"

CONST_INT ENABLE_CONTENT	FEATURE_FIXER

#IF ENABLE_CONTENT
USING "variations/fm_content_vip_contract_1_ballas.sch"
USING "variations/fm_content_vip_contract_1_families.sch"
USING "variations/fm_content_vip_contract_1_limo.sch"
USING "variations/fm_content_vip_contract_1_nightclub.sch"
USING "variations/fm_content_vip_contract_1_setup.sch"
USING "variations/fm_content_vip_contract_1_way_in.sch"
USING "variations/fm_content_vip_contract_1_yacht.sch"

/// PURPOSE: Use to set up the logic pointers for your mission
///    
PROC SETUP_MISSION_LOGIC()
	SWITCH GET_VARIATION()
		CASE FVV_SETUP
			FIB_SETUP_LOGIC()
		BREAK
		CASE FVV_LIMO
			LIMO_SETUP_LOGIC()
		BREAK
		CASE FVV_NIGHTCLUB
			NIGHTCLUB_SETUP_LOGIC()
		BREAK
		CASE FVV_WAY_IN
			WAYIN_SETUP_LOGIC()
		BREAK
		CASE FVV_YACHT
			YACHT_SETUP_LOGIC()
		BREAK
		CASE FVV_BALLAS
			BALLAS_SETUP_LOGIC()
		BREAK
		CASE FVV_FAMILIES
			FAM_SETUP_LOGIC()
		BREAK
		
		#IF IS_DEBUG_BUILD
		DEFAULT
			SCRIPT_ASSERT("Please populate SETUP_MISSION_LOGIC in fm_content_vip_contract_1_variations.sch.sch")
		#ENDIF
	ENDSWITCH
ENDPROC
#ENDIF
