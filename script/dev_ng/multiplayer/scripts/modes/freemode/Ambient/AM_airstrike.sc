
// ----------------------------------------------------- \\
// ----------------------------------------------------- \\
//                                                       \\
// SCRIPT:                                               \\
//                                                       \\
// AM_airstrike.sc                                 		 \\
//                                                       \\
// ----------------------------------------------------- \\
//                                                       \\
// DESCRIPTION:                                          \\
//                                                       \\
// runs airstrike logic.							     \\
//                                                       \\
// ----------------------------------------------------- \\
//                                                       \\
// AUTHOR:                                               \\
//                                                       \\
// William.Kennedy@RockstarNorth.com                     \\
//                                                    	 \\
// ----------------------------------------------------- \\
// ----------------------------------------------------- \\

 

USING "globals.sch"

// Game Headers
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_camera.sch"
USING "commands_hud.sch"
USING "commands_weapon.sch"
USING "commands_clock.sch"
USING "commands_fire.sch"
USING "lineactivation.sch"
USING "fm_relationships.sch"
USING "net_mission.sch"
USING "commands_water.sch"

// Network Headers
USING "net_include.sch"
USING "freemode_header.sch"

USING "net_wait_zero.sch"

#IF IS_DEBUG_BUILD 
USING "net_debug.sch"
USING "profiler.sch"
USING "net_debug_log.sch"
#ENDIF



// ***********
// VARIABLES.
// ***********

//MP_MISSION_DATA thisScript
//MP_MISSION thisMission = eAM_CR_SecurityVan

// Game states.
CONST_INT GAME_STATE_INI						0
CONST_INT GAME_STATE_INI_SPAWN					1
CONST_INT GAME_STATE_RUNNING					2
CONST_INT GAME_STATE_END						3

CONST_INT JET_PILOT								0
CONST_FLOAT PLANE_SPEED							60.0
CONST_INT NUM_EXPLOSIONS						10 // Rhythm = 1, 3, 2, 4
CONST_FLOAT PLANE_DESTINATION_HEIGHT 			50.0
CONST_FLOAT LEAVE_RANGE							250.0
CONST_FLOAT PLANE_CREATION_HEIGHT 				200.0
CONST_FLOAT PLANE_CREATION_RADIUS 				600.0
CONST_INT FLYOVER_TARGET_DISTANCE				15

CONST_INT FLARE_INSIDE_INTERIOR_STATE_NONE 		0
CONST_INT FLARE_INSIDE_INTERIOR_STATE_INSIDE 	1
CONST_INT FLARE_INSIDE_INTERIOR_STATE_OUTSIDE	2

BLIP_INDEX biJetBlip
SEQUENCE_INDEX seqPlane, seqPlane2

BOOL bCreatedSequences, bCreatedSequences2
BOOL bOnMissionOnLaunch
BOOL bPlayedJetAudio
BOOL bDisplayedAirStrikeHelp
BOOL bGivenBombCoordsSequence
BOOL bGivenFlare
BOOL bKeepMissilesGoing = FALSE
BOOL bStartcounter
BOOL bCountToZero

VECTOR vPlayercoordsWhenCalledForStrike
VECTOR vBombsStartLocation

FLOAT fJetDistance

//VECTOR vAirbaseCoords = <<-2495.9741, 3141.0999, 31.7983>>

SCRIPT_TIMER stFlyThroughLocateFailsafeTimer
SCRIPT_TIMER stEndScriptTimer
SCRIPT_TIMER stCreatePlaneDelayScriptTimer

ENUM eLOGICSTATE
	eLOGICSTATE_DO_AIRSTRIKE = 0,
	eLOGICSTATE_FINISHED
ENDENUM

ENUM eJET_STATE
    eJETSTATE_NOT_EXIST = 0,
    eJETSTATE_SPAWNING,
    eJETSTATE_FLYING,
    eJETSTATE_DEAD
ENDENUM

STRUCT STRUCT_AIRSTRIKE_JET_PED
    NETWORK_INDEX netId
    MODEL_NAMES eModel = S_M_M_PILOT_02
ENDSTRUCT

STRUCT STRUCT_AIRSTRIKE_JET_DATA
    NETWORK_INDEX netId
    MODEL_NAMES eModel = LAZER
    eJET_STATE eState
    STRUCT_AIRSTRIKE_JET_PED sPilot
    VECTOR vSpawnCoords
    FLOAT fHeading
    BOOL bActive
    INT iCircCount
    INT iActivatingStage
	SCRIPT_TIMER stLeaveTimer
	VECTOR vRandomEnd
ENDSTRUCT

structPedsForConversation sSpeech

BOOL bAddedPilotForDialogue
BOOL bPlayedArrivedDialogue
BOOL bPlayedNoFlareDialogue

INT iRocketsFiredBitset

// ****************
// BROADCAST DATA.
// ****************

// The server broadcast data.
// Everyone can read this data, only the server can update it. 
STRUCT ServerBroadcastData

	INT iServerGameState
	eLOGICSTATE eState
	STRUCT_AIRSTRIKE_JET_DATA sJet
	VECTOR vExplosionCoords[NUM_EXPLOSIONS]
	INT iExplosionCounter
	SCRIPT_TIMER stExplosionTimer
	BOOL bDoExplosions
	BOOL bFlareNotThrownInTime
	INT iFlareThrownInsideInterior
	BOOL bShownFlareInsideInteriorHelp
	BOOL bMissionForcedExplosions
	
ENDSTRUCT
ServerBroadcastData serverBD

// The client broadcast data.
// Everyone can read this data, only the local player only the server can update it. 
STRUCT PlayerBroadcastData

	INT iGameState
	eLOGICSTATE eState
	VECTOR vAirStrikeTargetCoords
	
ENDSTRUCT
PlayerBroadcastData playerBD[2]

// ****************
// Widgets & Debug.
// ****************
#IF IS_DEBUG_BUILD

	BOOL bHostEndMissionNow
	
	FUNC STRING GET_JET_STATE_NAME(eJET_STATE eNewState)
	
		SWITCH eNewState
			CASE eJETSTATE_NOT_EXIST 	RETURN "NOT_EXIST"
		    CASE eJETSTATE_SPAWNING 	RETURN "SPAWNING"
		    CASE eJETSTATE_FLYING 		RETURN "FLYING"
		    CASE eJETSTATE_DEAD 		RETURN "DEAD"
		ENDSWITCH
		
		RETURN "NOT_IN_SWITCH"
		
	ENDFUNC
	
	FUNC STRING GET_LOGIC_STATE_NAME(eLOGICSTATE eNewState)
	
		SWITCH eNewState
			CASE eLOGICSTATE_DO_AIRSTRIKE 		RETURN "DO_AIRSTRIKE"
			CASE eLOGICSTATE_FINISHED 			RETURN "FINISHED"
		ENDSWITCH
		
		RETURN "NOT_IN_SWITCH"
		
	ENDFUNC
	
#ENDIF

PROC GOTO_SERVER_LOGIC_STATE(eLOGICSTATE eNewState)
	
	#IF IS_DEBUG_BUILD
	IF serverBD.eState != eNewState
		NET_PRINT("[WJK] - Airstrike - setting server logic state to ")NET_PRINT(GET_LOGIC_STATE_NAME(eNewState))NET_NL()
	ENDIF
	#ENDIF
	
	serverBD.eState = eNewState
	
ENDPROC

PROC GOTO_CLIENT_LOGIC_STATE(eLOGICSTATE eNewState)
	
	#IF IS_DEBUG_BUILD
	
	IF playerBd[PARTICIPANT_ID_TO_INT()].eState != eNewState
		NET_PRINT("[WJK] - Airstrike - setting client logic state to ")NET_PRINT(GET_LOGIC_STATE_NAME(eNewState))NET_NL()
	ENDIF
	#ENDIF
	
	playerBd[PARTICIPANT_ID_TO_INT()].eState = eNewState
	
ENDPROC

PROC GOTO_JET_STATE(eJET_STATE eNewState)
	
	#IF IS_DEBUG_BUILD
	IF serverBD.sJet.eState != eNewState
		NET_PRINT("[WJK] - Airstrike - setting jet state to ")NET_PRINT(GET_JET_STATE_NAME(eNewState))NET_NL()
	ENDIF
	#ENDIF
		
	IF (eNewState = eJETSTATE_DEAD)
		MPGlobalsAmbience.bAirStrikeVehicleAlive = FALSE
	ENDIF

	serverBD.sJet.eState = eNewState
	
ENDPROC


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  MAIN PROCS                            ////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

PROC MAINTAIN_START_OF_FRAME_FLAGS()
	
	
	
ENDPROC

FUNC BOOL CONTROL_CHECKS(NETWORK_INDEX netId)
	
	IF IS_NETWORK_ID_OWNED_BY_PARTICIPANT(netId)
		RETURN TRUE
	ENDIF
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(netId)
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC REMOVE_JET_BLIP()
    
    IF DOES_BLIP_EXIST(biJetBlip)
        REMOVE_BLIP(biJetBlip)
    ENDIF
    
ENDPROC

/// PURPOSE:
///    Adds driver for dialogue once he exists.
PROC ADD_PILOT_FOR_DIALOGUE()
	IF NOT bAddedPilotForDialogue
	AND IS_CONVERSATION_STATUS_FREE()
		IF NOT IS_NET_PED_INJURED(serverBD.sJet.sPilot.netId)
			STRING sVoiceID = "FM_Pilot_Air"
			
			#IF FEATURE_HEIST_ISLAND
			IF IS_LOCAL_PLAYER_ON_HEIST_ISLAND()
				sVoiceID = "HS4_AIRSTRIKE1"
			ENDIF
			#ENDIF
			
			ADD_PED_FOR_DIALOGUE(sSpeech, 3, NET_TO_PED(serverBD.sJet.sPilot.netId), sVoiceID, TRUE)
			bAddedPilotForDialogue = TRUE
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Creates the required sequences for the script
PROC CREATE_SEQUENCES()

	IF NOT bCreatedSequences
		IF IS_NET_VEHICLE_DRIVEABLE(serverBD.sJet.netId)
			
			NET_PRINT("[WJK] - Airstrike - CREATE_SEQUENCES - creating sequence for plane to fly to coords: ")NET_PRINT_VECTOR(vPlayercoordsWhenCalledForStrike+<<0, 0, (PLANE_DESTINATION_HEIGHT+5.0)>>)NET_NL()
			
			OPEN_SEQUENCE_TASK(seqPlane)
				TASK_PLANE_MISSION(NULL, NET_TO_VEH(serverBD.sJet.netId), NULL, NULL, vPlayercoordsWhenCalledForStrike+<<0, 0, (PLANE_DESTINATION_HEIGHT+5.0)>>, MISSION_GOTO, PLANE_SPEED, FLYOVER_TARGET_DISTANCE, -1, CEIL((PLANE_DESTINATION_HEIGHT+5.0)), 20)
				TASK_PLANE_MISSION(NULL, NET_TO_VEH(serverBD.sJet.netId), NULL, NULL, serverBD.sJet.vRandomEnd, MISSION_GOTO, PLANE_SPEED, (FLYOVER_TARGET_DISTANCE+10), -1, CEIL((PLANE_DESTINATION_HEIGHT+5.0)), 20)
//				TASK_VEHICLE_DRIVE_TO_COORD(NULL, NET_TO_VEH(serverBD.sJet.netId), playerBD[PARTICIPANT_ID_TO_INT()].vAirStrikeTargetCoords+<<0, 0, PLANE_DESTINATION_HEIGHT>>, PLANE_SPEED, DRIVINGSTYLE_NORMAL, serverBD.sJet.eModel, DRIVINGMODE_PLOUGHTHROUGH, 15, -1)
//				TASK_VEHICLE_DRIVE_TO_COORD(NULL, NET_TO_VEH(serverBD.sJet.netId), serverBD.sJet.vRandomEnd, PLANE_SPEED, DRIVINGSTYLE_NORMAL, serverBD.sJet.eModel, DRIVINGMODE_PLOUGHTHROUGH, -1, -1)
			CLOSE_SEQUENCE_TASK(seqPlane)
			
			bCreatedSequences = TRUE
			NET_PRINT("[WJK] - Airstrike - CREATE_SEQUENCES - created sequences.")NET_NL()
			
		ENDIF
		
	ENDIF
ENDPROC

PROC CREATE_SEQUENCES_2()

	IF NOT bCreatedSequences2
		IF IS_NET_VEHICLE_DRIVEABLE(serverBD.sJet.netId)
		
			OPEN_SEQUENCE_TASK(seqPlane2)
				TASK_PLANE_MISSION(NULL, NET_TO_VEH(serverBD.sJet.netId), NULL, NULL, playerBD[PARTICIPANT_ID_TO_INT()].vAirStrikeTargetCoords+<<0, 0, (PLANE_DESTINATION_HEIGHT+5.0)>>, MISSION_GOTO, PLANE_SPEED, FLYOVER_TARGET_DISTANCE, -1, CEIL((PLANE_DESTINATION_HEIGHT+5.0)), 20)
				TASK_PLANE_MISSION(NULL, NET_TO_VEH(serverBD.sJet.netId), NULL, NULL, serverBD.sJet.vRandomEnd, MISSION_GOTO, PLANE_SPEED, (FLYOVER_TARGET_DISTANCE+10), -1, CEIL((PLANE_DESTINATION_HEIGHT+5.0)), 20)
//				TASK_VEHICLE_DRIVE_TO_COORD(NULL, NET_TO_VEH(serverBD.sJet.netId), playerBD[PARTICIPANT_ID_TO_INT()].vAirStrikeTargetCoords+<<0, 0, PLANE_DESTINATION_HEIGHT>>, PLANE_SPEED, DRIVINGSTYLE_NORMAL, serverBD.sJet.eModel, DRIVINGMODE_PLOUGHTHROUGH, 15, -1)
//				TASK_VEHICLE_DRIVE_TO_COORD(NULL, NET_TO_VEH(serverBD.sJet.netId), serverBD.sJet.vRandomEnd, PLANE_SPEED, DRIVINGSTYLE_NORMAL, serverBD.sJet.eModel, DRIVINGMODE_PLOUGHTHROUGH, -1, -1)
			CLOSE_SEQUENCE_TASK(seqPlane2)
			
			bCreatedSequences2 = TRUE
			NET_PRINT("[WJK] - Airstrike - CREATE_SEQUENCES - created sequences2.")NET_NL()
			
		ENDIF
		
	ENDIF
ENDPROC
//PURPOSE: Clears the sequences created for this script
PROC CLEAR_SEQUENCES()

	IF bCreatedSequences
		CLEAR_SEQUENCE_TASK(seqPlane)
		NET_PRINT("[WJK] - Airstrike - CLEAR_SEQUENCES - cleared sequences.")NET_NL()
	ENDIF
	
	IF bCreatedSequences2
		CLEAR_SEQUENCE_TASK(seqPlane2)
		NET_PRINT("[WJK] - Airstrike - CLEAR_SEQUENCES - cleared sequences2.")NET_NL()
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Gets a random vector within a donut centred at vPos with a radius of fRadius and a height of fHeight.
/// PARAMS:
///    vPos - The sphere disc
///    fInnerRadius - Radius of the hole in the middle
///    fOuterRadius - Radius of the donut
///    fHeight - Height of the disc (goes both above and below vPos)
/// RETURNS:
///    A random position vector within fRadius of vPos.
FUNC VECTOR GET_RANDOM_POINT_IN_DONUT(VECTOR vPos, FLOAT fInnerRadius, FLOAT fOuterRadius, FLOAT fHeight)
 	
	// Pick a random unit direction vector.
	VECTOR vDir = << GET_RANDOM_FLOAT_IN_RANGE(-1,1) , GET_RANDOM_FLOAT_IN_RANGE(-1,1), 0 >>
	FLOAT fHalfHeight = fHeight / 2.0
	
	// Project this vector into the disc some random distance.
	vDir = GET_VECTOR_OF_LENGTH(vDir, GET_RANDOM_FLOAT_IN_RANGE(fInnerRadius, fOuterRadius))
	
	// Grab a random height and add it on.
	vDir.z = GET_RANDOM_FLOAT_IN_RANGE(-fHalfHeight, fHalfHeight)
	
	RETURN vPos + vDir
	
ENDFUNC

//PURPOSE: Calculates a position to Create the plane at
PROC GET_PLANE_CREATION_POINT(VECTOR &vPos, FLOAT &fHeading)
	vPos = GET_RANDOM_POINT_IN_DONUT(vPlayercoordsWhenCalledForStrike+<<0, 0, PLANE_CREATION_HEIGHT>>, PLANE_CREATION_RADIUS, PLANE_CREATION_RADIUS, PLANE_CREATION_HEIGHT/2)
	// fHeading = GET_HEADING_FROM_VECTOR_2D(vPlayercoordsWhenCalledForStrike.x-vPos.x, vPlayercoordsWhenCalledForStrike.y-vPos.y) //GET_HEADING_BETWEEN_VECTORS_2D(vPos, playerBD[PARTICIPANT_ID_TO_INT()].vAirStrikeTargetCoords)
	
//	For Airstrike add this to GET_PLANE_CREATION_POINT
	FLOAT fApproxHeight = GET_APPROX_HEIGHT_FOR_AREA(vPos.x-PLANE_CREATION_RADIUS, vPos.y-PLANE_CREATION_RADIUS, vPos.x+PLANE_CREATION_RADIUS, vPos.y+PLANE_CREATION_RADIUS)
	IF vPos.z < fApproxHeight
	      vPos.z = fApproxHeight
	ENDIF 
	
	fHeading = GET_HEADING_BETWEEN_VECTORS_2D(vPos, vPlayercoordsWhenCalledForStrike)
	NET_PRINT("[WJK] - Airstrike - GET_PLANE_CREATION_POINT - creation point = ")NET_PRINT_VECTOR(vPos)NET_NL()
ENDPROC

FUNC BOOL CREATE_PLANE()
	
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.sJet.netId)
		IF REQUEST_LOAD_MODEL(serverBD.sJet.eModel)
			IF LOAD_STREAM("AIRSTRIKE_FLYOVER")
				
				VECTOR vPos
				FLOAT fHeading
				GET_PLANE_CREATION_POINT(vPos, fHeading)
				
				IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(vPos, 6, 1, 1, 5)
				
//					vPos.z += GET_APPROX_HEIGHT_FOR_AREA(vPos.x-PLANE_CREATION_RADIUS, vPos.y-PLANE_CREATION_RADIUS, vPos.x+PLANE_CREATION_RADIUS, vPos.y+PLANE_CREATION_RADIUS)
					
					IF CREATE_NET_VEHICLE(serverBD.sJet.netId, serverBD.sJet.eModel, vPos, fHeading, TRUE, TRUE, TRUE)	//FALSE, FALSE, FALSE)	
						
						serverBD.sJet.vRandomEnd = (vPos*<<-1,-1,0.0>>)
						NET_PRINT_TIME() NET_PRINT("[WJK] - Airstrike - serverBD.sJet.vRandomEnd = ")NET_PRINT_VECTOR(serverBD.sJet.vRandomEnd)NET_NL()
						
						SET_VEHICLE_DOORS_LOCKED(NET_TO_VEH(serverBD.sJet.netId), VEHICLELOCK_LOCKED)
						
						FREEZE_ENTITY_POSITION(NET_TO_VEH(serverBD.sJet.netId), FALSE)
						SET_ENTITY_DYNAMIC(NET_TO_VEH(serverBD.sJet.netId), TRUE)
						ACTIVATE_PHYSICS(NET_TO_VEH(serverBD.sJet.netId))
						SET_VEHICLE_FORWARD_SPEED(NET_TO_VEH(serverBD.sJet.netId), PLANE_SPEED)
						SET_VEHICLE_ENGINE_ON(NET_TO_VEH(serverBD.sJet.netId), TRUE, TRUE)
						SET_VEHICLE_EXTENDED_REMOVAL_RANGE(NET_TO_VEH(serverBD.sJet.netId), 1000)
						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(NET_TO_VEH(serverBD.sJet.netId), FALSE)
																
						CONTROL_LANDING_GEAR(NET_TO_VEH(serverBD.sJet.netId), LGC_RETRACT_INSTANT)
						OPEN_BOMB_BAY_DOORS(NET_TO_VEH(serverBD.sJet.netId))
						
						NET_PRINT("[WJK] - Airstrike - CREATE_PLANE - created plane at vector ")NET_PRINT_VECTOR(vPos)NET_NL()
						
						SET_MODEL_AS_NO_LONGER_NEEDED(serverBD.sJet.eModel)
						RETURN TRUE
						
					ENDIF
				ELSE
					NET_PRINT("[WJK] - Airstrike - CREATE_PLANE - creation point is not ok for net entity creation.")NET_NL()
				ENDIF
				
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

//PURPOSE: Create the Pilot
FUNC BOOL CREATE_PILOT()

	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.sJet.sPilot.netId)
	AND REQUEST_LOAD_MODEL(serverBD.sJet.sPilot.eModel)
	AND NETWORK_DOES_NETWORK_ID_EXIST(serverBD.sJet.netId)
	
		IF IS_NET_VEHICLE_DRIVEABLE(serverBD.sJet.netId)
			IF CREATE_NET_PED_IN_VEHICLE(serverBD.sJet.sPilot.netId, serverBD.sJet.netId, PEDTYPE_CRIMINAL, serverBD.sJet.sPilot.eModel, VS_DRIVER)
				
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NET_TO_PED(serverBD.sJet.sPilot.netId), TRUE)
				SET_PED_RELATIONSHIP_GROUP_HASH(NET_TO_PED(serverBD.sJet.sPilot.netId), rgFM_AiLike)
				SET_PED_RANDOM_COMPONENT_VARIATION(NET_TO_PED(serverBD.sJet.sPilot.netId))
				
				SET_PED_KEEP_TASK(NET_TO_PED(serverBD.sJet.sPilot.netId), TRUE)
				
				CREATE_SEQUENCES()
				TASK_PERFORM_SEQUENCE(NET_TO_PED(serverBD.sJet.sPilot.netId), seqPlane)
				SET_TASK_VEHICLE_GOTO_PLANE_MIN_HEIGHT_ABOVE_TERRAIN(NET_TO_VEH(serverBD.sJet.netId), ROUND(PLANE_DESTINATION_HEIGHT))
				
				NET_PRINT("[WJK] - Airstrike - CREATE_PILOT - created pilot.") NET_NL()
				
			ENDIF
		ENDIF
	ENDIF
	
	//Check we have created the ped
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.sJet.sPilot.netId)
		NET_PRINT("[WJK] - Airstrike - CREATE_PILOT - pilot does not exist yet.") NET_NL()
		RETURN FALSE
	ENDIF
	
	SET_MODEL_AS_NO_LONGER_NEEDED(serverBD.sJet.sPilot.eModel)
	RETURN TRUE
	
ENDFUNC

FUNC BOOL CREATE_PLANE_AND_PILOT()
	
	IF NOT HAS_NET_TIMER_STARTED(stCreatePlaneDelayScriptTimer)
		
		START_NET_TIMER(stCreatePlaneDelayScriptTimer)
		
	ELSE
		
		IF REQUEST_LOAD_MODEL(serverBD.sJet.eModel)
		AND REQUEST_LOAD_MODEL(serverBD.sJet.sPilot.eModel)
			
			IF HAS_NET_TIMER_EXPIRED(stCreatePlaneDelayScriptTimer, 3000)
				
				IF CREATE_PLANE()
				AND CREATE_PILOT()
				
					RETURN TRUE
					
				ENDIF
			
			ENDIF
			
		ENDIF
		
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC PROCESS_JET_BRAIN()
    
    SWITCH serverBD.sJet.eState
        
        CASE eJETSTATE_NOT_EXIST
			
	   		GOTO_JET_STATE(eJETSTATE_SPAWNING)
			
        BREAK
        
        CASE eJETSTATE_SPAWNING
            
			IF CREATE_PLANE_AND_PILOT()
                GOTO_JET_STATE(eJETSTATE_FLYING)
            ENDIF
			
        BREAK
        
        CASE eJETSTATE_FLYING
              
            IF NOT IS_NET_VEHICLE_DRIVEABLE(serverBD.sJet.netId)
				IF IS_NET_PED_INJURED(serverBD.sJet.sPilot.netId)
	                GOTO_JET_STATE(eJETSTATE_DEAD)
				ENDIF
            ELIF IS_NET_PED_INJURED(serverBD.sJet.sPilot.netId)
	        	GOTO_JET_STATE(eJETSTATE_DEAD)
            ENDIF
                
        BREAK
		
        CASE eJETSTATE_DEAD
            
			
			
        BREAK
        
    ENDSWITCH
    
ENDPROC

PROC KILL_JET_CREW()
	
	PED_INDEX pedId
	
	IF NOT IS_NET_PED_INJURED(serverBD.sJet.sPilot.netId)
		IF CONTROL_CHECKS(serverBD.sJet.sPilot.netId)
            IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sJet.sPilot.netId)
                pedId = NET_TO_PED(serverBD.sJet.sPilot.netId)
                SET_ENTITY_HEALTH(pedId, 0)
            ENDIF
		ENDIF
    ENDIF
    
ENDPROC

PROC DO_AIRSTRIKE_EXPLOSIONS()
	PED_INDEX pedIndex = PLAYER_PED_ID()
	
	VECTOR vStartCoord, vEndCoord
	
	INT iDamage = 200
	
	IF IS_BIT_SET(g_iArcadeFortuneBS, ARCADE_FORTUNE_GLOBAL_BS_AIRSTRIKE)
		pedIndex = NULL
		iDamage = 300
	ENDIF
	
	IF IS_VECTOR_ZERO(vBombsStartLocation)
		IF IS_NET_VEHICLE_DRIVEABLE(serverBD.sJet.netId)
			vBombsStartLocation = GET_ENTITY_COORDS(NET_TO_VEH(serverBD.sJet.netId))
			vBombsStartLocation.z -= 5.0
		ENDIF
	ENDIF
		
	SWITCH serverBD.iExplosionCounter
		CASE 0
			IF NOT HAS_NET_TIMER_STARTED(serverBD.stExplosionTimer)
				IF NOT IS_BIT_SET(iRocketsFiredBitset, serverBD.iExplosionCounter)
					vStartCoord = playerBD[PARTICIPANT_ID_TO_INT()].vAirStrikeTargetCoords
					vStartCoord.z = vBombsStartLocation.z
					vEndCoord = playerBD[PARTICIPANT_ID_TO_INT()].vAirStrikeTargetCoords
					vEndCoord = vEndCoord+<<0.1, 0.1, 0.0>>
					GET_GROUND_Z_FOR_3D_COORD(vEndCoord+<< 0.0, 0.0, 10.0 >>, vEndCoord.z)
					vEndCoord.z -= 25.0
//						SHOOT_SINGLE_BULLET_BETWEEN_COORDS(vStartCoord, vEndCoord, iDamage, TRUE, WEAPONTYPE_AIRSTRIKE_ROCKET, NET_TO_PED(serverBD.sJet.sPilot.netId), TRUE, FALSE)
					SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY_NEW(vStartCoord, vEndCoord, iDamage, TRUE, WEAPONTYPE_AIRSTRIKE_ROCKET, PLAYER_PED_ID(), TRUE, FALSE, -1, pedIndex, TRUE, TRUE, NULL, FALSE)
					NET_PRINT("[WJK] - Airstrike - DO_AIRSTRIKE_EXPLOSIONS - shot rocket 0. vStartCoord = ")NET_PRINT_VECTOR(vStartCoord)NET_PRINT(", vEndCoord = ")NET_PRINT_VECTOR(vEndCoord)NET_NL()
					NET_PRINT("[WJK] - Airstrike - DO_AIRSTRIKE_EXPLOSIONS - shot rocket 0. vAirStrikeTargetCoords = ")NET_PRINT_VECTOR(playerBD[PARTICIPANT_ID_TO_INT()].vAirStrikeTargetCoords)NET_NL()
					NET_PRINT("[WJK] - Airstrike - DO_AIRSTRIKE_EXPLOSIONS - shot rocket 0. player coords = ")NET_PRINT_VECTOR(GET_ENTITY_COORDS(PLAYER_PED_ID()))NET_NL()
					START_NET_TIMER(serverBD.stExplosionTimer)
					bKeepMissilesGoing = TRUE
					SET_BIT(iRocketsFiredBitset, serverBD.iExplosionCounter)
					
					IF IS_CONVERSATION_STATUS_FREE()
						#IF FEATURE_HEIST_ISLAND
						IF IS_LOCAL_PLAYER_ON_HEIST_ISLAND()
							CREATE_CONVERSATION(sSpeech, "HS4FAUD", "HS4F_SAAIP1", CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT)
						ENDIF
						#ENDIF
					ENDIF
				ENDIF
			ELSE
				IF HAS_NET_TIMER_EXPIRED(serverBD.stExplosionTimer, 750)
					RESET_NET_TIMER(serverBD.stExplosionTimer)
					serverBD.iExplosionCounter++
				ENDIF
			ENDIF
		BREAK
		CASE 1
			IF NOT HAS_NET_TIMER_STARTED(serverBD.stExplosionTimer)
				IF NOT IS_BIT_SET(iRocketsFiredBitset, serverBD.iExplosionCounter)
					vStartCoord = (playerBD[PARTICIPANT_ID_TO_INT()].vAirStrikeTargetCoords+<<5,5,0>>)
					vStartCoord.z = vBombsStartLocation.z
					vEndCoord = playerBD[PARTICIPANT_ID_TO_INT()].vAirStrikeTargetCoords+<<5,5,0>>
					vEndCoord = vEndCoord+<<0.1, 0.1, 0.0>>
					GET_GROUND_Z_FOR_3D_COORD(vEndCoord+<< 0.0, 0.0, 10.0 >>, vEndCoord.z)
					vEndCoord.z -= 25.0
//						SHOOT_SINGLE_BULLET_BETWEEN_COORDS(vStartCoord, vEndCoord, iDamage, TRUE, WEAPONTYPE_AIRSTRIKE_ROCKET, NET_TO_PED(serverBD.sJet.sPilot.netId), TRUE, FALSE)
					SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY_NEW(vStartCoord, vEndCoord, iDamage, TRUE, WEAPONTYPE_AIRSTRIKE_ROCKET, NET_TO_PED(serverBD.sJet.sPilot.netId), TRUE, FALSE, -1, pedIndex, TRUE, FALSE, NULL, FALSE)
					NET_PRINT("[WJK] - Airstrike - DO_AIRSTRIKE_EXPLOSIONS - shot rocket 1. vStartCoord = ")NET_PRINT_VECTOR(vStartCoord)NET_PRINT(", vEndCoord = ")NET_PRINT_VECTOR(vEndCoord)NET_NL()
					NET_PRINT("[WJK] - Airstrike - DO_AIRSTRIKE_EXPLOSIONS - shot rocket 1. vAirStrikeTargetCoords = ")NET_PRINT_VECTOR(playerBD[PARTICIPANT_ID_TO_INT()].vAirStrikeTargetCoords)NET_NL()
					NET_PRINT("[WJK] - Airstrike - DO_AIRSTRIKE_EXPLOSIONS - shot rocket 1. player coords = ")NET_PRINT_VECTOR(GET_ENTITY_COORDS(PLAYER_PED_ID()))NET_NL()
					START_NET_TIMER(serverBD.stExplosionTimer)
					SET_BIT(iRocketsFiredBitset, serverBD.iExplosionCounter)
				ENDIF
			ELSE
				IF HAS_NET_TIMER_EXPIRED(serverBD.stExplosionTimer, 500)
					RESET_NET_TIMER(serverBD.stExplosionTimer)
					serverBD.iExplosionCounter++
				ENDIF
			ENDIF
		BREAK
		CASE 2
			IF NOT HAS_NET_TIMER_STARTED(serverBD.stExplosionTimer)
				IF NOT IS_BIT_SET(iRocketsFiredBitset, serverBD.iExplosionCounter)
					vStartCoord = (playerBD[PARTICIPANT_ID_TO_INT()].vAirStrikeTargetCoords+<<-5,-5,0>>)
					vStartCoord.z = vBombsStartLocation.z
					vEndCoord = playerBD[PARTICIPANT_ID_TO_INT()].vAirStrikeTargetCoords+<<-5,-5,0>>
					vEndCoord = vEndCoord+<<0.1, 0.1, 0.0>>
					GET_GROUND_Z_FOR_3D_COORD(vEndCoord+<< 0.0, 0.0, 10.0 >>, vEndCoord.z)
					vEndCoord.z -= 25.0
//						SHOOT_SINGLE_BULLET_BETWEEN_COORDS(vStartCoord, vEndCoord, iDamage, TRUE, WEAPONTYPE_AIRSTRIKE_ROCKET, NET_TO_PED(serverBD.sJet.sPilot.netId), TRUE, FALSE)
					SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY_NEW(vStartCoord, vEndCoord, iDamage, TRUE, WEAPONTYPE_AIRSTRIKE_ROCKET, NET_TO_PED(serverBD.sJet.sPilot.netId), TRUE, FALSE, -1, pedIndex, TRUE, FALSE, NULL, FALSE)
					NET_PRINT("[WJK] - Airstrike - DO_AIRSTRIKE_EXPLOSIONS - shot rocket 2. vStartCoord = ")NET_PRINT_VECTOR(vStartCoord)NET_PRINT(", vEndCoord = ")NET_PRINT_VECTOR(vEndCoord)NET_NL()
					NET_PRINT("[WJK] - Airstrike - DO_AIRSTRIKE_EXPLOSIONS - shot rocket 2. vAirStrikeTargetCoords = ")NET_PRINT_VECTOR(playerBD[PARTICIPANT_ID_TO_INT()].vAirStrikeTargetCoords)NET_NL()
					NET_PRINT("[WJK] - Airstrike - DO_AIRSTRIKE_EXPLOSIONS - shot rocket 2. player coords = ")NET_PRINT_VECTOR(GET_ENTITY_COORDS(PLAYER_PED_ID()))NET_NL()
					START_NET_TIMER(serverBD.stExplosionTimer)
					SET_BIT(iRocketsFiredBitset, serverBD.iExplosionCounter)
				ENDIF
			ELSE
				IF HAS_NET_TIMER_EXPIRED(serverBD.stExplosionTimer, 500)
					RESET_NET_TIMER(serverBD.stExplosionTimer)
					serverBD.iExplosionCounter++
				ENDIF
			ENDIF
		BREAK
		CASE 3
			IF NOT HAS_NET_TIMER_STARTED(serverBD.stExplosionTimer)
				IF NOT IS_BIT_SET(iRocketsFiredBitset, serverBD.iExplosionCounter)
					vStartCoord = (playerBD[PARTICIPANT_ID_TO_INT()].vAirStrikeTargetCoords+<<-5,5,0>>)
					vStartCoord.z = vBombsStartLocation.z
					vEndCoord = playerBD[PARTICIPANT_ID_TO_INT()].vAirStrikeTargetCoords+<<-5,5,0>>
					vEndCoord = vEndCoord+<<0.1, 0.1, 0.0>>
					GET_GROUND_Z_FOR_3D_COORD(vEndCoord+<< 0.0, 0.0, 10.0 >>, vEndCoord.z)
					vEndCoord.z -= 25.0
//						SHOOT_SINGLE_BULLET_BETWEEN_COORDS(vStartCoord, vEndCoord, iDamage, TRUE, WEAPONTYPE_AIRSTRIKE_ROCKET, NET_TO_PED(serverBD.sJet.sPilot.netId), TRUE, FALSE)
					SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY_NEW(vStartCoord, vEndCoord, iDamage, TRUE, WEAPONTYPE_AIRSTRIKE_ROCKET, NET_TO_PED(serverBD.sJet.sPilot.netId), TRUE, FALSE, -1, pedIndex, TRUE, FALSE, NULL, FALSE)
					NET_PRINT("[WJK] - Airstrike - DO_AIRSTRIKE_EXPLOSIONS - shot rocket 3. vStartCoord = ")NET_PRINT_VECTOR(vStartCoord)NET_PRINT(", vEndCoord = ")NET_PRINT_VECTOR(vEndCoord)NET_NL()
					NET_PRINT("[WJK] - Airstrike - DO_AIRSTRIKE_EXPLOSIONS - shot rocket 3. vAirStrikeTargetCoords = ")NET_PRINT_VECTOR(playerBD[PARTICIPANT_ID_TO_INT()].vAirStrikeTargetCoords)NET_NL()
					NET_PRINT("[WJK] - Airstrike - DO_AIRSTRIKE_EXPLOSIONS - shot rocket 3. player coords = ")NET_PRINT_VECTOR(GET_ENTITY_COORDS(PLAYER_PED_ID()))NET_NL()
					START_NET_TIMER(serverBD.stExplosionTimer)
					SET_BIT(iRocketsFiredBitset, serverBD.iExplosionCounter)
				ENDIF
			ELSE
				IF HAS_NET_TIMER_EXPIRED(serverBD.stExplosionTimer, 750)
					RESET_NET_TIMER(serverBD.stExplosionTimer)
					serverBD.iExplosionCounter++
				ENDIF
			ENDIF
		BREAK
		CASE 4
			IF NOT HAS_NET_TIMER_STARTED(serverBD.stExplosionTimer)
				IF NOT IS_BIT_SET(iRocketsFiredBitset, serverBD.iExplosionCounter)
					vStartCoord = (playerBD[PARTICIPANT_ID_TO_INT()].vAirStrikeTargetCoords+<<5,-5,0>>)
					vStartCoord.z = vBombsStartLocation.z
					vEndCoord = playerBD[PARTICIPANT_ID_TO_INT()].vAirStrikeTargetCoords+<<5,-5,0>>
					vEndCoord = vEndCoord+<<0.1, 0.1, 0.0>>
					GET_GROUND_Z_FOR_3D_COORD(vEndCoord+<< 0.0, 0.0, 10.0 >>, vEndCoord.z)
					vEndCoord.z -= 25.0
//						SHOOT_SINGLE_BULLET_BETWEEN_COORDS(vStartCoord, vEndCoord, iDamage, TRUE, WEAPONTYPE_AIRSTRIKE_ROCKET, NET_TO_PED(serverBD.sJet.sPilot.netId), TRUE, FALSE)
					SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY_NEW(vStartCoord, vEndCoord, iDamage, TRUE, WEAPONTYPE_AIRSTRIKE_ROCKET, NET_TO_PED(serverBD.sJet.sPilot.netId), TRUE, FALSE, -1, pedIndex, TRUE, FALSE, NULL, FALSE)
					NET_PRINT("[WJK] - Airstrike - DO_AIRSTRIKE_EXPLOSIONS - shot rocket 4. vStartCoord = ")NET_PRINT_VECTOR(vStartCoord)NET_PRINT(", vEndCoord = ")NET_PRINT_VECTOR(vEndCoord)NET_NL()
					NET_PRINT("[WJK] - Airstrike - DO_AIRSTRIKE_EXPLOSIONS - shot rocket 4. vAirStrikeTargetCoords = ")NET_PRINT_VECTOR(playerBD[PARTICIPANT_ID_TO_INT()].vAirStrikeTargetCoords)NET_NL()
					NET_PRINT("[WJK] - Airstrike - DO_AIRSTRIKE_EXPLOSIONS - shot rocket 4. player coords = ")NET_PRINT_VECTOR(GET_ENTITY_COORDS(PLAYER_PED_ID()))NET_NL()
					START_NET_TIMER(serverBD.stExplosionTimer)
					SET_BIT(iRocketsFiredBitset, serverBD.iExplosionCounter)
				ENDIF
			ELSE
				IF HAS_NET_TIMER_EXPIRED(serverBD.stExplosionTimer,500)
					RESET_NET_TIMER(serverBD.stExplosionTimer)
					serverBD.iExplosionCounter++
				ENDIF
			ENDIF
		BREAK
		CASE 5
			IF NOT HAS_NET_TIMER_STARTED(serverBD.stExplosionTimer)
				IF NOT IS_BIT_SET(iRocketsFiredBitset, serverBD.iExplosionCounter)
					vStartCoord = playerBD[PARTICIPANT_ID_TO_INT()].vAirStrikeTargetCoords+<<10,5,0>>
					vStartCoord.z = vBombsStartLocation.z
					vEndCoord = playerBD[PARTICIPANT_ID_TO_INT()].vAirStrikeTargetCoords+<<10,5,0>>
					vEndCoord = vEndCoord+<<0.1, 0.1, 0.0>>
					GET_GROUND_Z_FOR_3D_COORD(vEndCoord+<< 0.0, 0.0, 10.0 >>, vEndCoord.z)
					vEndCoord.z -= 25.0
//						SHOOT_SINGLE_BULLET_BETWEEN_COORDS(vStartCoord, vEndCoord, iDamage, TRUE, WEAPONTYPE_AIRSTRIKE_ROCKET, NET_TO_PED(serverBD.sJet.sPilot.netId), TRUE, FALSE)
					SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY_NEW(vStartCoord, vEndCoord, iDamage, TRUE, WEAPONTYPE_AIRSTRIKE_ROCKET, NET_TO_PED(serverBD.sJet.sPilot.netId), TRUE, FALSE, -1, pedIndex, TRUE, FALSE, NULL, FALSE)
					NET_PRINT("[WJK] - Airstrike - DO_AIRSTRIKE_EXPLOSIONS - shot rocket 5. vStartCoord = ")NET_PRINT_VECTOR(vStartCoord)NET_PRINT(", vEndCoord = ")NET_PRINT_VECTOR(vEndCoord)NET_NL()
					NET_PRINT("[WJK] - Airstrike - DO_AIRSTRIKE_EXPLOSIONS - shot rocket 5. vAirStrikeTargetCoords = ")NET_PRINT_VECTOR(playerBD[PARTICIPANT_ID_TO_INT()].vAirStrikeTargetCoords)NET_NL()
					NET_PRINT("[WJK] - Airstrike - DO_AIRSTRIKE_EXPLOSIONS - shot rocket 5. player coords = ")NET_PRINT_VECTOR(GET_ENTITY_COORDS(PLAYER_PED_ID()))NET_NL()
					START_NET_TIMER(serverBD.stExplosionTimer)
					SET_BIT(iRocketsFiredBitset, serverBD.iExplosionCounter)
				ENDIF
			ELSE
				IF HAS_NET_TIMER_EXPIRED(serverBD.stExplosionTimer, 750)
					RESET_NET_TIMER(serverBD.stExplosionTimer)
					serverBD.iExplosionCounter++
				ENDIF
			ENDIF
		BREAK
		CASE 6
			IF NOT HAS_NET_TIMER_STARTED(serverBD.stExplosionTimer)
				IF NOT IS_BIT_SET(iRocketsFiredBitset, serverBD.iExplosionCounter)
					vStartCoord = (playerBD[PARTICIPANT_ID_TO_INT()].vAirStrikeTargetCoords+<<10,10,0>>)
					vStartCoord.z = vBombsStartLocation.z
					vEndCoord = playerBD[PARTICIPANT_ID_TO_INT()].vAirStrikeTargetCoords+<<10,10,0>>
					vEndCoord = vEndCoord+<<0.1, 0.1, 0.0>>
					GET_GROUND_Z_FOR_3D_COORD(vEndCoord+<< 0.0, 0.0, 10.0 >>, vEndCoord.z)
					vEndCoord.z -= 25.0
//						SHOOT_SINGLE_BULLET_BETWEEN_COORDS(vStartCoord, vEndCoord, iDamage, TRUE, WEAPONTYPE_AIRSTRIKE_ROCKET, NET_TO_PED(serverBD.sJet.sPilot.netId), TRUE, FALSE)
					SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY_NEW(vStartCoord, vEndCoord, iDamage, TRUE, WEAPONTYPE_AIRSTRIKE_ROCKET, NET_TO_PED(serverBD.sJet.sPilot.netId), TRUE, FALSE, -1, pedIndex, TRUE, FALSE, NULL, FALSE)
					NET_PRINT("[WJK] - Airstrike - DO_AIRSTRIKE_EXPLOSIONS - shot rocket 6. vStartCoord = ")NET_PRINT_VECTOR(vStartCoord)NET_PRINT(", vEndCoord = ")NET_PRINT_VECTOR(vEndCoord)NET_NL()
					NET_PRINT("[WJK] - Airstrike - DO_AIRSTRIKE_EXPLOSIONS - shot rocket 6. vAirStrikeTargetCoords = ")NET_PRINT_VECTOR(playerBD[PARTICIPANT_ID_TO_INT()].vAirStrikeTargetCoords)NET_NL()
					NET_PRINT("[WJK] - Airstrike - DO_AIRSTRIKE_EXPLOSIONS - shot rocket 6. player coords = ")NET_PRINT_VECTOR(GET_ENTITY_COORDS(PLAYER_PED_ID()))NET_NL()
					START_NET_TIMER(serverBD.stExplosionTimer)
					SET_BIT(iRocketsFiredBitset, serverBD.iExplosionCounter)
				ENDIF
			ELSE
				IF HAS_NET_TIMER_EXPIRED(serverBD.stExplosionTimer, 500)
					RESET_NET_TIMER(serverBD.stExplosionTimer)
					serverBD.iExplosionCounter++
				ENDIF
			ENDIF
		BREAK
		CASE 7
			IF NOT HAS_NET_TIMER_STARTED(serverBD.stExplosionTimer)
				IF NOT IS_BIT_SET(iRocketsFiredBitset, serverBD.iExplosionCounter)
					vStartCoord = (playerBD[PARTICIPANT_ID_TO_INT()].vAirStrikeTargetCoords+<<-10,-10,0>>)
					vStartCoord.z = vBombsStartLocation.z
					vEndCoord = playerBD[PARTICIPANT_ID_TO_INT()].vAirStrikeTargetCoords+<<-10,-10,0>>
					vEndCoord = vEndCoord+<<0.1, 0.1, 0.0>>
					GET_GROUND_Z_FOR_3D_COORD(vEndCoord+<< 0.0, 0.0, 10.0 >>, vEndCoord.z)
					vEndCoord.z -= 25.0
//						SHOOT_SINGLE_BULLET_BETWEEN_COORDS(vStartCoord, vEndCoord, iDamage, TRUE, WEAPONTYPE_AIRSTRIKE_ROCKET, NET_TO_PED(serverBD.sJet.sPilot.netId), TRUE, FALSE)
					SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY_NEW(vStartCoord, vEndCoord, iDamage, TRUE, WEAPONTYPE_AIRSTRIKE_ROCKET, NET_TO_PED(serverBD.sJet.sPilot.netId), TRUE, FALSE, -1, pedIndex, TRUE, FALSE, NULL, FALSE)
					NET_PRINT("[WJK] - Airstrike - DO_AIRSTRIKE_EXPLOSIONS - shot rocket 7. vStartCoord = ")NET_PRINT_VECTOR(vStartCoord)NET_PRINT(", vEndCoord = ")NET_PRINT_VECTOR(vEndCoord)NET_NL()
					NET_PRINT("[WJK] - Airstrike - DO_AIRSTRIKE_EXPLOSIONS - shot rocket 7. vAirStrikeTargetCoords = ")NET_PRINT_VECTOR(playerBD[PARTICIPANT_ID_TO_INT()].vAirStrikeTargetCoords)NET_NL()
					NET_PRINT("[WJK] - Airstrike - DO_AIRSTRIKE_EXPLOSIONS - shot rocket 7. player coords = ")NET_PRINT_VECTOR(GET_ENTITY_COORDS(PLAYER_PED_ID()))NET_NL()
					START_NET_TIMER(serverBD.stExplosionTimer)
					SET_BIT(iRocketsFiredBitset, serverBD.iExplosionCounter)
				ENDIF
			ELSE
				IF HAS_NET_TIMER_EXPIRED(serverBD.stExplosionTimer, 500)
					RESET_NET_TIMER(serverBD.stExplosionTimer)
					serverBD.iExplosionCounter++
				ENDIF
			ENDIF
		BREAK
		CASE 8
			IF NOT HAS_NET_TIMER_STARTED(serverBD.stExplosionTimer)
				IF NOT IS_BIT_SET(iRocketsFiredBitset, serverBD.iExplosionCounter)
					vStartCoord = (playerBD[PARTICIPANT_ID_TO_INT()].vAirStrikeTargetCoords+<<-10,10,0>>)
					vStartCoord.z = vBombsStartLocation.z
					vEndCoord = playerBD[PARTICIPANT_ID_TO_INT()].vAirStrikeTargetCoords+<<-10,10,0>>
					vEndCoord = vEndCoord+<<0.1, 0.1, 0.0>>
					GET_GROUND_Z_FOR_3D_COORD(vEndCoord+<< 0.0, 0.0, 10.0 >>, vEndCoord.z)
					vEndCoord.z -= 25.0
//						SHOOT_SINGLE_BULLET_BETWEEN_COORDS(vStartCoord, vEndCoord, iDamage, TRUE, WEAPONTYPE_AIRSTRIKE_ROCKET, NET_TO_PED(serverBD.sJet.sPilot.netId), TRUE, FALSE)
					SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY_NEW(vStartCoord, vEndCoord, iDamage, TRUE, WEAPONTYPE_AIRSTRIKE_ROCKET, NET_TO_PED(serverBD.sJet.sPilot.netId), TRUE, FALSE, -1, pedIndex, TRUE, FALSE, NULL, FALSE)
					NET_PRINT("[WJK] - Airstrike - DO_AIRSTRIKE_EXPLOSIONS - shot rocket 8. vStartCoord = ")NET_PRINT_VECTOR(vStartCoord)NET_PRINT(", vEndCoord = ")NET_PRINT_VECTOR(vEndCoord)NET_NL()
					NET_PRINT("[WJK] - Airstrike - DO_AIRSTRIKE_EXPLOSIONS - shot rocket 8. vAirStrikeTargetCoords = ")NET_PRINT_VECTOR(playerBD[PARTICIPANT_ID_TO_INT()].vAirStrikeTargetCoords)NET_NL()
					NET_PRINT("[WJK] - Airstrike - DO_AIRSTRIKE_EXPLOSIONS - shot rocket 8. player coords = ")NET_PRINT_VECTOR(GET_ENTITY_COORDS(PLAYER_PED_ID()))NET_NL()
					START_NET_TIMER(serverBD.stExplosionTimer)
					SET_BIT(iRocketsFiredBitset, serverBD.iExplosionCounter)
				ENDIF
			ELSE
				IF HAS_NET_TIMER_EXPIRED(serverBD.stExplosionTimer, 500)
					RESET_NET_TIMER(serverBD.stExplosionTimer)
					serverBD.iExplosionCounter++
				ENDIF
			ENDIF
		BREAK
		CASE 9
			IF NOT HAS_NET_TIMER_STARTED(serverBD.stExplosionTimer)
				IF NOT IS_BIT_SET(iRocketsFiredBitset, serverBD.iExplosionCounter)
					vStartCoord = (playerBD[PARTICIPANT_ID_TO_INT()].vAirStrikeTargetCoords+<<10,-10,0>>)
					vStartCoord.z = vBombsStartLocation.z
					vEndCoord = playerBD[PARTICIPANT_ID_TO_INT()].vAirStrikeTargetCoords+<<10,-10,0>>
					vEndCoord = vEndCoord+<<0.1, 0.1, 0.0>>
					GET_GROUND_Z_FOR_3D_COORD(vEndCoord+<< 0.0, 0.0, 10.0 >>, vEndCoord.z)
					vEndCoord.z -= 25.0
//						SHOOT_SINGLE_BULLET_BETWEEN_COORDS(vStartCoord, vEndCoord, iDamage, TRUE, WEAPONTYPE_AIRSTRIKE_ROCKET, NET_TO_PED(serverBD.sJet.sPilot.netId), TRUE, FALSE)
					SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY_NEW(vStartCoord, vEndCoord, iDamage, TRUE, WEAPONTYPE_AIRSTRIKE_ROCKET, NET_TO_PED(serverBD.sJet.sPilot.netId), TRUE, FALSE, -1, pedIndex, TRUE, FALSE, NULL, FALSE)
					NET_PRINT("[WJK] - Airstrike - DO_AIRSTRIKE_EXPLOSIONS - shot rocket 9. vStartCoord = ")NET_PRINT_VECTOR(vStartCoord)NET_PRINT(", vEndCoord = ")NET_PRINT_VECTOR(vEndCoord)NET_NL()
					NET_PRINT("[WJK] - Airstrike - DO_AIRSTRIKE_EXPLOSIONS - shot rocket 9. vAirStrikeTargetCoords = ")NET_PRINT_VECTOR(playerBD[PARTICIPANT_ID_TO_INT()].vAirStrikeTargetCoords)NET_NL()
					NET_PRINT("[WJK] - Airstrike - DO_AIRSTRIKE_EXPLOSIONS - shot rocket 9. player coords = ")NET_PRINT_VECTOR(GET_ENTITY_COORDS(PLAYER_PED_ID()))NET_NL()
					serverBD.iExplosionCounter++ // Done!
					bKeepMissilesGoing = FALSE
					SET_BIT(iRocketsFiredBitset, serverBD.iExplosionCounter)
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC

FUNC INT IS_FLARE_OUTSIDE()
	
	IF serverBD.iFlareThrownInsideInterior = FLARE_INSIDE_INTERIOR_STATE_INSIDE
		NET_PRINT("[WJK] - Airstrike - IS_FLARE_OUTSIDE - iFlareThrownInsideInterior = 1, flare is not outside.") NET_NL()
		RETURN serverBD.iFlareThrownInsideInterior
	ELIF serverBD.iFlareThrownInsideInterior = FLARE_INSIDE_INTERIOR_STATE_OUTSIDE
		NET_PRINT("[WJK] - Airstrike - IS_FLARE_OUTSIDE - iFlareThrownInsideInterior = 1, flare is outside.") NET_NL()
		RETURN serverBD.iFlareThrownInsideInterior
	ENDIF
	
	IF NOT IS_COLLISION_MARKED_OUTSIDE((playerBD[PARTICIPANT_ID_TO_INT()].vAirStrikeTargetCoords+<<0.0, 0.0, 0.5>>))
		NET_PRINT("[WJK] - Airstrike - IS_FLARE_OUTSIDE - IS_COLLISION_MARKED_OUTSIDE = FALSE, flare is not outside.") NET_NL()
		serverBD.iFlareThrownInsideInterior = FLARE_INSIDE_INTERIOR_STATE_INSIDE
		RETURN serverBD.iFlareThrownInsideInterior
	ENDIF
	
	NET_PRINT("[WJK] - Airstrike - IS_FLARE_OUTSIDE -  flare is outside.") NET_NL()
	serverBD.iFlareThrownInsideInterior = FLARE_INSIDE_INTERIOR_STATE_OUTSIDE
	
	RETURN serverBD.iFlareThrownInsideInterior
	
ENDFUNC

PROC PROCESS_DISTANCE_COUNTER()
	
	VEHICLE_INDEX viJet
	VECTOR vTargetCoords
	VECTOR vJetCoords
	INT iJetDistance
	
	IF IS_NET_VEHICLE_DRIVEABLE(serverBD.sJet.netId)
    	viJet = NET_TO_VEH(serverBD.sJet.netId)
		vJetCoords = GET_ENTITY_COORDS(viJet)
	ENDIF
	
	IF NOT bCountToZero
		IF NOT IS_VECTOR_ZERO(playerBD[PARTICIPANT_ID_TO_INT()].vAirStrikeTargetCoords)
			vTargetCoords = playerBD[PARTICIPANT_ID_TO_INT()].vAirStrikeTargetCoords
		ELSE
			IF IS_NET_PLAYER_OK(PLAYER_ID())
				vTargetCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
			ENDIF
		ENDIF
	ENDIF
	
	IF bStartcounter
	AND NOT IS_BIT_SET(g_iArcadeFortuneBS, ARCADE_FORTUNE_GLOBAL_BS_AIRSTRIKE)
		IF NOT bCountToZero
			IF IS_NET_VEHICLE_DRIVEABLE(serverBD.sJet.netId)
				IF NOT IS_ENTITY_AT_COORD(viJet, vPlayercoordsWhenCalledForStrike, << FLYOVER_TARGET_DISTANCE+60, FLYOVER_TARGET_DISTANCE+60, 1000 >>)
					fJetDistance = GET_DISTANCE_BETWEEN_COORDS(vJetCoords, vTargetCoords, FALSE)
				ELSE
					bCountToZero = TRUE
				ENDIF
			ELSE
				bCountToZero = TRUE
			ENDIF
		ELSE
			fJetDistance -= 3.75
		ENDIF
		
		iJetDistance = FLOOR(fJetDistance)
		
		HUD_COLOURS eTextColour
		
		IF fJetDistance <= 200.0
			eTextColour = HUD_COLOUR_RED
		ELSE
			eTextColour = HUD_COLOUR_WHITE
		ENDIF
		
		IF (fJetDistance >= 0)
			DRAW_GENERIC_SCORE(iJetDistance, "AIRSTRIKE_2", -1, eTextColour, HUDORDER_DONTCARE, FALSE, "AIRSTRIKE_6", FALSE, 0, HUDFLASHING_NONE, 0, eTextColour)
		ENDIF
		
	ENDIF
	
ENDPROC

FUNC STRING GET_JET_BLIP_NAME()

#IF FEATURE_HEIST_ISLAND
	IF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
		RETURN "PA_AIRSTRIKE_BLIP"	
	ENDIF
#ENDIF
	
	RETURN "AIRSTRIKE_5"
ENDFUNC

PROC PROCESS_JET_BODY()
    
//    PED_INDEX pedId
    VEHICLE_INDEX niHJet
//	VEHICLE_MISSION eVehicleMission
	VECTOR vAngleCoord1, vAngleCoord2
	VECTOR vAudioCoords
	
    SWITCH serverBD.sJet.eState
            
            CASE eJETSTATE_NOT_EXIST
                    
                REMOVE_JET_BLIP()
                    
            BREAK
            
            CASE eJETSTATE_SPAWNING
                
                REMOVE_JET_BLIP()
                    
            BREAK
            
            CASE eJETSTATE_FLYING
                
				// If jet is stil flying.
                IF IS_NET_VEHICLE_DRIVEABLE(serverBD.sJet.netId)
                    
					bStartcounter = TRUE
					
					niHJet = NET_TO_VEH(serverBD.sJet.netId)
					
					IF NOT DOES_BLIP_EXIST(biJetBlip)
                        biJetBlip = ADD_BLIP_FOR_ENTITY(niHJet)
						SET_BLIP_SPRITE(biJetBlip, RADAR_TRACE_POLICE_PLANE_MOVE)
						SET_BLIP_NAME_FROM_TEXT_FILE(biJetBlip, GET_JET_BLIP_NAME())
						SHOW_HEIGHT_ON_BLIP(biJetBlip, FALSE)
					ELSE
						SET_BLIP_ROTATION(biJetBlip, CEIL(GET_ENTITY_HEADING(niHJet)))
                    ENDIF
					
					// Handle big jet audio.
					IF NOT bPlayedJetAudio
						IF NOT IS_VECTOR_ZERO(playerBD[PARTICIPANT_ID_TO_INT()].vAirStrikeTargetCoords)
							vAudioCoords = playerBD[PARTICIPANT_ID_TO_INT()].vAirStrikeTargetCoords
						ELSE
							vAudioCoords = vPlayercoordsWhenCalledForStrike
						ENDIF
						IF GET_DISTANCE_BETWEEN_COORDS(vAudioCoords, GET_ENTITY_COORDS(niHJet)) <= (PLANE_SPEED*5)
							PLAY_STREAM_FROM_VEHICLE(NET_TO_VEH(serverBD.sJet.netId))
							bPlayedJetAudio = TRUE
							NET_PRINT("[WJK] - Airstrike - jet is <= 5 seconds away, playing cool jet audio.")NET_NL()
						ENDIF
					ENDIF
					
					// Pilot dialogue.

					IF NOT bPlayedArrivedDialogue
					AND NOT IS_BIT_SET(g_iArcadeFortuneBS, ARCADE_FORTUNE_GLOBAL_BS_AIRSTRIKE)
						IF NOT serverBD.bFlareNotThrownInTime
							IF NOT IS_NET_PED_INJURED(serverBD.sJet.sPilot.netId)
								IF NOT IS_VECTOR_ZERO(playerBD[PARTICIPANT_ID_TO_INT()].vAirStrikeTargetCoords)
									IF GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()), GET_ENTITY_COORDS(NET_TO_VEH(serverBD.sJet.sPilot.netId)), FALSE) <= 250.0
										IF GET_DISTANCE_BETWEEN_COORDS(vAudioCoords, GET_ENTITY_COORDS(niHJet)) >= (PLANE_SPEED*2.5)
											IF IS_CONVERSATION_STATUS_FREE()
												#IF FEATURE_HEIST_ISLAND
												IF IS_LOCAL_PLAYER_ON_HEIST_ISLAND()
													CREATE_CONVERSATION(sSpeech, "HS4FAUD", "HS4F_SAACI1", CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT)
												ELSE
												#ENDIF
													CREATE_CONVERSATION_USING_V_CONTENT_IN_DLC(sSpeech, "CT_AUD", "MPCT_ASarr", CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT)
												#IF FEATURE_HEIST_ISLAND
												ENDIF
												#ENDIF
											ENDIF
											
											bPlayedArrivedDialogue = TRUE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
	
					IF serverBD.iExplosionCounter < NUM_EXPLOSIONS
						IF NOT serverBD.bDoExplosions
							IF NOT IS_VECTOR_ZERO(playerBD[PARTICIPANT_ID_TO_INT()].vAirStrikeTargetCoords)
								NET_PRINT("[WJK] - Airstrike - vAirStrikeTargetCoords not zero.")NET_NL()
								IF IS_FLARE_OUTSIDE() = FLARE_INSIDE_INTERIOR_STATE_OUTSIDE
									IF NOT HAS_NET_TIMER_STARTED(stFlyThroughLocateFailsafeTimer) // In case for some reason the jet doesn't fly through the locate.
										START_NET_TIMER(stFlyThroughLocateFailsafeTimer)
									ELSE
										IF HAS_NET_TIMER_EXPIRED(stFlyThroughLocateFailsafeTimer, 7500)
											serverBD.bDoExplosions = TRUE
										ENDIF
									ENDIF
									vAngleCoord1 = (playerBD[PARTICIPANT_ID_TO_INT()].vAirStrikeTargetCoords - << FLYOVER_TARGET_DISTANCE+10, FLYOVER_TARGET_DISTANCE+10, 1000 >>)
									vAngleCoord2 = (playerBD[PARTICIPANT_ID_TO_INT()].vAirStrikeTargetCoords + << FLYOVER_TARGET_DISTANCE+10, FLYOVER_TARGET_DISTANCE+10, 1000 >>)
									vAngleCoord1.z = -200
									vAngleCoord2.z = 1500 //1000 bug 1626849
									IF IS_ENTITY_AT_COORD(niHJet, playerBD[PARTICIPANT_ID_TO_INT()].vAirStrikeTargetCoords, << FLYOVER_TARGET_DISTANCE+60, FLYOVER_TARGET_DISTANCE+60, 1000 >>)
									AND IS_PROJECTILE_TYPE_IN_AREA(vAngleCoord1, vAngleCoord2, WEAPONTYPE_FLARE)
										serverBD.bDoExplosions = TRUE
									ENDIF
								ELIF IS_FLARE_OUTSIDE() = FLARE_INSIDE_INTERIOR_STATE_INSIDE
									serverBD.bDoExplosions = TRUE
									serverBD.iExplosionCounter = 100
								ENDIF
								
							ELSE
								NET_PRINT("[WJK] - Airstrike - vAirStrikeTargetCoords is zero.")NET_NL()
								IF IS_ENTITY_AT_COORD(niHJet, vPlayercoordsWhenCalledForStrike, << FLYOVER_TARGET_DISTANCE+60, FLYOVER_TARGET_DISTANCE+60, 1000 >>)
									serverBD.bDoExplosions = TRUE
									serverBD.iExplosionCounter = 100
									serverBD.bFlareNotThrownInTime = TRUE
									NET_PRINT("[WJK] - Airstrike - Flare not thrown in time.")NET_NL()
									
									IF NOT bPlayedNoFlareDialogue
									AND NOT IS_BIT_SET(g_iArcadeFortuneBS, ARCADE_FORTUNE_GLOBAL_BS_AIRSTRIKE)
										IF IS_CONVERSATION_STATUS_FREE()
											#IF FEATURE_HEIST_ISLAND
											IF NOT IS_LOCAL_PLAYER_ON_HEIST_ISLAND()
											#ENDIF
												CREATE_CONVERSATION_USING_V_CONTENT_IN_DLC(sSpeech, "CT_AUD", "MPCT_ASflr", CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT)
											#IF FEATURE_HEIST_ISLAND
											ENDIF
											#ENDIF
										ENDIF
										
										bPlayedNoFlareDialogue = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
                ENDIF
                
				// If jet or pilot are dead, kill crew.
				IF NOT IS_NET_VEHICLE_DRIVEABLE(serverBD.sJet.netId)
				OR IS_NET_PED_INJURED(serverBD.sJet.sPilot.netId)
					KILL_JET_CREW()
				ENDIF
				
            BREAK
            
			CASE eJETSTATE_DEAD
                    
                REMOVE_JET_BLIP()
                    
            BREAK
            
    ENDSWITCH
    
ENDPROC

/// PURPOSE:
///    Server only function that checks to see if the game mode should now end. For this test script, it if time has passed.
/// RETURNS:
///    True if the the mode has been active for ROUND_DURATION 
FUNC BOOL HAVE_MISSION_END_CONDITIONS_BEEN_MET()
	
	#IF IS_DEBUG_BUILD
		IF bHostEndMissionNow
			RETURN TRUE
		ENDIF
	#ENDIF	
	
	RETURN FALSE
	
ENDFUNC
 
/// PURPOSE:
///    Helper function to get a clients game/mission state
/// PARAMS:
///    iPlayer - The player's game state you want.
/// RETURNS:
///    The game state.
FUNC INT GET_CLIENT_MISSION_STATE(INT iPlayer)
	RETURN playerBD[iPlayer].iGameState
ENDFUNC


/// PURPOSE:
///    Helper function to get the servers game/mission state
/// RETURNS:
///    The server game state.
FUNC INT GET_SERVER_MISSION_STATE()
	RETURN serverBD.iServerGameState
ENDFUNC

/// PURPOSE:
///    Gets if pedId is piloting sa helicopter.
/// PARAMS:
///    pedId - ped to check if piloting a helicopter.
/// RETURNS:
///    BOOL, TRUE if piloting a helicopter, FALSE if not.
FUNC BOOL IS_PED_PILOTING_ANY_HELI(PED_INDEX pedId)
	
	VEHICLE_INDEX vehId
	
	IF IS_PED_IN_ANY_HELI(pedId)
		vehId = GET_VEHICLE_PED_IS_IN(pedId)
		IF IS_VEHICLE_DRIVEABLE(vehId)
			IF GET_PED_IN_VEHICLE_SEAT(vehId) = pedId
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC PROCESS_SERVER_eLOGICSTATE_DO_AIRSTRIKE()

	VECTOR vProjectileCoords
	
	#IF IS_DEBUG_BUILD
	VECTOR vPlayerCoords
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
		ENDIF
	ENDIF
	#ENDIF
	
	ADD_PILOT_FOR_DIALOGUE()
			
	// Put flare in hand of player. Once we pickup the coords of the thrown flare, move on.
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			IF NOT bGivenFlare
			AND NOT IS_BIT_SET(g_iArcadeFortuneBS, ARCADE_FORTUNE_GLOBAL_BS_AIRSTRIKE)
				IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("cellphone_flashhand")) <= 0 
					BOOL bPutIntoHand = (NOT IS_PLAYER_IN_ANY_SHOP())
					GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_FLARE, 1, bPutIntoHand, bPutIntoHand)
					SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_FLARE, bPutIntoHand)
					bGivenFlare = TRUE
				ENDIF
			ELSE	
				IF IS_BIT_SET(g_iArcadeFortuneBS, ARCADE_FORTUNE_GLOBAL_BS_AIRSTRIKE)
				OR GET_COORDS_OF_PROJECTILE_TYPE_WITHIN_DISTANCE(PLAYER_PED_ID(), WEAPONTYPE_FLARE, 100.0, playerBD[PARTICIPANT_ID_TO_INT()].vAirStrikeTargetCoords, TRUE)
					IF IS_BIT_SET(g_iArcadeFortuneBS, ARCADE_FORTUNE_GLOBAL_BS_AIRSTRIKE)
						playerBD[PARTICIPANT_ID_TO_INT()].vAirStrikeTargetCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
					ELSE
						IF ARE_VECTORS_EQUAL(playerBD[PARTICIPANT_ID_TO_INT()].vAirStrikeTargetCoords, << 0.0, 0.0, 0.0 >>)
							playerBD[PARTICIPANT_ID_TO_INT()].vAirStrikeTargetCoords = vProjectileCoords
							PRINTLN("[WJK] - Airstrike - GET_COORDS_OF_PROJECTILE_TYPE_WITHIN_DISTANCE has returned TRUE with a target value of ", playerBD[PARTICIPANT_ID_TO_INT()].vAirStrikeTargetCoords)
							PRINTLN("[WJK] - Airstrike - player coords = ", vPlayerCoords)
						ENDIF
					ENDIF
					
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AIRSTRIKE_1")
						CLEAR_HELP()
					ENDIF
					
					IF NOT bGivenBombCoordsSequence
						IF NOT IS_NET_PED_INJURED(serverBD.sJet.sPilot.netId)
							IF IS_NET_VEHICLE_DRIVEABLE(serverBD.sJet.netId)
								CREATE_SEQUENCES_2()
								CLEAR_PED_TASKS(NET_TO_PED(serverBD.sJet.sPilot.netId))
								TASK_PERFORM_SEQUENCE(NET_TO_PED(serverBD.sJet.sPilot.netId), seqPlane2)
								SET_TASK_VEHICLE_GOTO_PLANE_MIN_HEIGHT_ABOVE_TERRAIN(NET_TO_VEH(serverBD.sJet.netId), ROUND(PLANE_DESTINATION_HEIGHT))
								bGivenBombCoordsSequence = TRUE
							ENDIF
						ENDIF
					ENDIF
				ELSE
					// Tell player how to select an airstrike target.
					IF NOT bDisplayedAirStrikeHelp
					AND NOT IS_BIT_SET(g_iArcadeFortuneBS, ARCADE_FORTUNE_GLOBAL_BS_AIRSTRIKE)
						IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
							IF IS_PED_PILOTING_ANY_HELI(PLAYER_PED_ID())
								PRINT_HELP("AIRSTRIKE_4")
							ELSE
								PRINT_HELP("AIRSTRIKE_1")
							ENDIF
							bDisplayedAirStrikeHelp = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	// Jet AI.
	PROCESS_JET_BRAIN()
	PROCESS_JET_BODY()
	
ENDPROC

PROC PROCESS_SERVER_eLOGICSTATE_FINISHED()
	
ENDPROC

/// PURPOSE:
///    Processes security van script server logic.
PROC PROCESS_SERVER()
	
	SWITCH serverBD.eState
		CASE eLOGICSTATE_DO_AIRSTRIKE
			PROCESS_SERVER_eLOGICSTATE_DO_AIRSTRIKE()
		BREAK
		CASE eLOGICSTATE_FINISHED
			PROCESS_SERVER_eLOGICSTATE_FINISHED()
		BREAK
	ENDSWITCH
	
	IF serverBD.sJet.eState = eJETSTATE_DEAD AND bKeepMissilesGoing = FALSE
		NET_PRINT("[WJK] - Airstrike - jet is dead, initiating script termination.")NET_NL()
		serverBD.iServerGameState = GAME_STATE_END
	ENDIF
	
	PROCESS_DISTANCE_COUNTER()
		
	IF serverBD.bDoExplosions AND serverBD.iExplosionCounter < NUM_EXPLOSIONS
	
		DO_AIRSTRIKE_EXPLOSIONS()
	
	ENDIF	
	
	IF serverBD.iExplosionCounter >= NUM_EXPLOSIONS
		IF NOT HAS_NET_TIMER_STARTED(stEndScriptTimer)
			START_NET_TIMER(stEndScriptTimer)
		ELSE
			IF HAS_NET_TIMER_EXPIRED(stEndScriptTimer, 10000)
				NET_PRINT("[WJK] - Airstrike - airstrike complete, initiating script termination.")NET_NL()
				serverBD.iServerGameState = GAME_STATE_END
			ENDIF
		ENDIF
	ENDIF
	
	//Set to end state if player enters passive mode whilst airstrike is active.
	IF IS_MP_PASSIVE_MODE_ENABLED()
		serverBD.iServerGameState = GAME_STATE_END
	ENDIF
	
	IF NOT IS_BIT_SET(g_iArcadeFortuneBS, ARCADE_FORTUNE_GLOBAL_BS_AIRSTRIKE)
		IF serverBD.iFlareThrownInsideInterior = FLARE_INSIDE_INTERIOR_STATE_INSIDE
			IF NOT serverBD.bShownFlareInsideInteriorHelp
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AIRSTRIKE_1")
				OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AIRSTRIKE_2")
				OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AIRSTRIKE_4")
					PRINT_HELP("AIRSTRIKE_3")
					serverBD.bShownFlareInsideInteriorHelp = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bOnMissionOnLaunch
		IF NOT IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
			NET_PRINT("[WJK] - Airstrike - was on mission when jet was called, now not on mission, initiating script termination.")NET_NL()
			serverBD.iServerGameState = GAME_STATE_END
		ENDIF
	ELSE
		IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
		OR IS_FM_MISSION_LAUNCH_IN_PROGRESS()
			NET_PRINT("[WJK] - Airstrike - was not on mission when jet was called, now on mission, initiating script termination.")NET_NL()
			serverBD.iServerGameState = GAME_STATE_END
		ENDIF
	ENDIF
	
	IF IS_CUTSCENE_PLAYING()
		NET_PRINT("[WJK] - Airstrike - IS_CUTSCENE_PLAYING() = TRUE, initiating script termination.")NET_NL()
		serverBD.iServerGameState = GAME_STATE_END
	ENDIF
	
	IF collectables_missiondata_MAIN.bturnintoanimal
	OR IS_PLAYER_AN_ANIMAL(PLAYER_ID())
		NET_PRINT("[WJK] - Airstrike - IS_PLAYER_AN_ANIMAL() = TRUE, initiating script termination.")NET_NL()
		serverBD.iServerGameState = GAME_STATE_END
	ENDIF
	
ENDPROC



/// PURPOSE:
///    Widgets and debug.
#IF IS_DEBUG_BUILD
	
	BOOL bKillJet
	
	/// PURPOSE:
	///    Creates widgets.
	PROC CREATE_WIDGETS()
		
		START_WIDGET_GROUP("Airstrike")
			ADD_WIDGET_BOOL("Kill Jet", bKillJet)
			ADD_WIDGET_VECTOR_SLIDER("Target Coords", playerBD[PARTICIPANT_ID_TO_INT()].vAirStrikeTargetCoords, -10000.0, 10000.0, 0.1)
		STOP_WIDGET_GROUP()
		
	ENDPROC		
	
	/// PURPOSE:
	///    Updates widgets.
	PROC UPDATE_WIDGETS()		
		
		IF bKillJet
			IF IS_NET_VEHICLE_DRIVEABLE(serverBD.sJet.netId)
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sJet.netId)
					SET_VEHICLE_ENGINE_HEALTH(NET_TO_VEH(serverBD.sJet.netId), -1.0)
				ELSE
					NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.sJet.netId)
				ENDIF
			ELSE
				bKillJet = FALSE
			ENDIF
		ENDIF
		
	ENDPROC

#ENDIF


/// PURPOSE:
///    Script cleanup.
PROC SCRIPT_CLEANUP()

	//Task the plane to flee the player if alive to stop it returning 
	IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.sJet.sPilot.netId)
	AND NETWORK_DOES_NETWORK_ID_EXIST(serverBD.sJet.netId)
		IF IS_NET_VEHICLE_DRIVEABLE(serverBD.sJet.netId)
		AND NOT IS_NET_PED_INJURED(serverBD.sJet.sPilot.netId)
			TASK_PLANE_MISSION(NET_TO_PED(serverBD.sJet.sPilot.netId), NET_TO_VEH(serverBD.sJet.netId), NULL, PLAYER_PED_ID(), <<1.0,1.0,1.0>>, MISSION_FLEE, PLANE_SPEED, 1000, -1, 400, 400)
		ENDIF
	ENDIF
	
//	STOP_STREAM()
	BROADCAST_TELL_SERVER_CONTACT_REQUEST_COMPLETE(ALL_PLAYERS(),REQUEST_AIRSTRIKE,INVALID_PLAYER_INDEX())
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()  // call this to terminate your script.
	
ENDPROC	


/// PURPOSE:
///    Do necessary pre game start ini.
PROC PROCESS_PRE_GAME(MP_MISSION_DATA missionScriptArgs)
	
	// This marks the script as a net script, and handles any instancing setup. 
	SETUP_MP_MISSION_FOR_NETWORK(GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(missionScriptArgs.mdID.idMission) ,  missionScriptArgs )
	
//	thisScript = missionScriptArgs
	
	RESERVE_NETWORK_MISSION_PEDS(1)
	RESERVE_NETWORK_MISSION_VEHICLES(1)
	
	// This makes sure the net script is active, waits until it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
	
	// This script will not be paused if another script calls PAUSE_GAME
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	
	// KGM: Wait for the first network broadcast before moving on - call this as the last instruction in pre-game
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		NET_PRINT("[WJK] - Airstrike - Failed to receive initial network broadcast data. Cleaning up.")NET_NL()	
		SCRIPT_CLEANUP()
	ENDIF
	
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		vPlayercoordsWhenCalledForStrike = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
		NET_PRINT("[WJK] - Airstrike - CREATE_SEQUENCES - saved vPlayercoordsWhenCalledForStrike as ")NET_PRINT_VECTOR(vPlayercoordsWhenCalledForStrike)NET_NL()	
	ENDIF
	
	IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
		bOnMissionOnLaunch = TRUE
	ENDIF
	
	RESET_REMINDER_TIMER_FOR_ACTIVITY(FMMC_TYPE_AIRSTRIKE)
	
	playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_INI
	
ENDPROC

/// PURPOSE:
///    Processes client side logic.
PROC PROCESS_CLIENT()
	MPGlobalsAmbience.bAirstrikeExplosionsInProgress = serverBD.bDoExplosions
ENDPROC


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                      Main Loop                             ///////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///    

SCRIPT(MP_MISSION_DATA missionScriptArgs)

	#IF IS_DEBUG_BUILD	
		NET_PRINT_TIME() NET_PRINT_THREAD_ID() NET_PRINT("CnC: Starting AM_airstrike \n") NET_NL()
	#ENDIF
	
	// Carry out all the initial game starting duties. 
	IF NETWORK_IS_GAME_IN_PROGRESS()
		
		PROCESS_PRE_GAME(missionScriptArgs)	

		// Setup widgets. Make sure this is done after PROCESS_PRE_GAME()
		#IF IS_DEBUG_BUILD
			CREATE_WIDGETS()
		#ENDIF
		
	ENDIF
	
	// Main loop.
	WHILE TRUE

		// One wait to rule them all. This can be the ONLY wait from here on in.
		MP_LOOP_WAIT_ZERO()
		
		// Initiate script profiler.
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		SCRIPT_PROFILER_START_OF_FRAME()
		#ENDIF
		#ENDIF
		
//		// Get rid of any cop players who shouldn't be running this script.
//		IF IS_PLAYER_COP(PLAYER_ID())
//			NET_PRINT("SECURITY VAN: I am a sneaky sneaky cop that managed to run the security van script, time to go now.") NET_NL()
//			SCRIPT_CLEANUP()
//		ENDIF
		
		// If we have a match end event, bail.
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
     		SCRIPT_CLEANUP()
		ENDIF	
		
		IF NETWORK_IS_GAME_IN_PROGRESS()
			
			// Flags for one call per frame of expensive commands.
			MAINTAIN_START_OF_FRAME_FLAGS()
			
			// Deal with the debug.
			#IF IS_DEBUG_BUILD
				UPDATE_WIDGETS()
			#ENDIF		
		
			// -----------------------------------
			// Process your game logic.....
			SWITCH GET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT())
							
				// Wait until the server gives the all go before moving on.
				CASE GAME_STATE_INI		
					IF GET_SERVER_MISSION_STATE() > GAME_STATE_INI	
						playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_RUNNING					
					ENDIF				
				BREAK
				
				// Main gameplay state.
				CASE GAME_STATE_RUNNING			
					
					PROCESS_CLIENT()
					
					// Look for the server say the mission has ended.
					IF GET_SERVER_MISSION_STATE() = GAME_STATE_END
						playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END			
					ENDIF	
					
				BREAK	
	
				CASE GAME_STATE_END				
					SCRIPT_CLEANUP()
				BREAK
				
			ENDSWITCH
			
			// -----------------------------------
			// Process server game logic		
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				
				SWITCH GET_SERVER_MISSION_STATE()
					
					CASE GAME_STATE_INI	
						serverBD.iServerGameState = GAME_STATE_RUNNING
					BREAK
					
					// Look for game end conditions.
					CASE GAME_STATE_RUNNING		
						
						// Process security van script server logic.
						PROCESS_SERVER()
						
						#IF IS_DEBUG_BUILD
							IF bHostEndMissionNow		
								serverBD.iServerGameState = GAME_STATE_END
							ENDIF
						#ENDIF	
						
					BREAK
					
					CASE GAME_STATE_END			
										
						// We'd want to upload scores here, show results, do whatever.
						
					BREAK	
					
				ENDSWITCH

			ENDIF
		
		ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
			SCRIPT_PROFILER_END_OF_FRAME()
		#ENDIF
		#ENDIF
		
		
	ENDWHILE
	
// End of Mission
ENDSCRIPT



// Use these if we split client and server logic in the future.


//PROC PROCESS_CLIENT_eLOGICSTATE_PICK_TARGET()
//	
//ENDPROC
//
//PROC PROCESS_CLIENT_eLOGICSTATE_DO_AIRSTRIKE()
//	
//ENDPROC
//
//PROC PROCESS_CLIENT_eLOGICSTATE_FINISHED()
//	
//ENDPROC

//	SWITCH serverBD.eState
//		CASE eLOGICSTATE_PICK_TARGET
//			PROCESS_CLIENT_eLOGICSTATE_PICK_TARGET()
//		BREAK
//		CASE eLOGICSTATE_DO_AIRSTRIKE
//			PROCESS_CLIENT_eLOGICSTATE_DO_AIRSTRIKE()
//		BREAK
//		CASE eLOGICSTATE_FINISHED
//			PROCESS_CLIENT_eLOGICSTATE_FINISHED()
//		BREAK
//	ENDSWITCH
