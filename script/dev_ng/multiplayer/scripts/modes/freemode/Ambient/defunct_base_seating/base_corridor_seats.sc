///    Author 	: 	Orlando C-H
///    Team 	: 	Online Tech
///    Info		: 	Each seating area needs it's own script so that the 
///    				MAX_NUM_SEATS_FOR_SITTING_ACTIVITY can be redefined to minimise
///    				wasted space.

CONST_INT MAX_NUM_SEATS_FOR_SITTING_ACTIVITY 		4
USING "defunct_base_sitting_activities_public.sch"
USING "defunct_base_seat_areas.sch"
USING "defunct_base_bench_helper.sch"

SEATS_LOCAL_DATA m_seatData

PROC STORE_SEAT_DATA()
	INT iSeatIndices[2]
	iSeatIndices[0] = 0
	iSeatIndices[1] = 1
	SETUP_NAMED_FACILITY_BENCH(FACILITY_BENCH_LOBBY_0, m_seatData, iSeatIndices)
	iSeatIndices[0] = 2
	iSeatIndices[1] = 3
	SETUP_NAMED_FACILITY_BENCH(FACILITY_BENCH_LOBBY_1, m_seatData, iSeatIndices)
ENDPROC


FUNC BOOL SCRIPT_INIT()
	IF g_ownerOfBasePropertyIAmIn <> INVALID_PLAYER_INDEX()
		IF NETWORK_IS_SCRIPT_ACTIVE("base_corridor_seats", NATIVE_TO_INT(g_ownerOfBasePropertyIAmIn), TRUE)
			PRINTLN("[SEATS] base_corridor_seats : SCRIPT_INIT : script with this ID already running) :  terminating.")
			RETURN FALSE
		ENDIF
		NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE, NATIVE_TO_INT(g_ownerOfBasePropertyIAmIn))
		HANDLE_NET_SCRIPT_INITIALISATION()
		SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
		
		STORE_SEAT_DATA()
		
		#IF IS_DEBUG_BUILD
		START_WIDGET_GROUP("Seats")
			ADD_WIDGET_INT_SLIDER("FORCED_ANIM_VARIATION", m_seatData.debug.iNextForcedAnimVar, 0, 20, 1)
			ADD_WIDGET_BOOL("FORCE_NOW", m_seatData.debug.bForceAnimVarNow)
		STOP_WIDGET_GROUP()
		#ENDIF //IS_DEBUG_BUILD
		
		RETURN TRUE
	ELSE
		PRINTLN("[SEATS] base_corridor_seats : SCRIPT_INIT : g_ownerOfBasePropertyIAmIn = INVALID_PLAYER_INDEX() :  terminating.")
		RETURN FALSE
	ENDIF
ENDFUNC

FUNC BOOL SHOULD_TERMINATE_SCRIPT()
	IF NOT IS_ENTITY_IN_DEFUNCT_BASE_OPS_LOWER(PLAYER_PED_ID())
		PRINTLN("[SEATS] SHOULD_TERMINATE_SCRIPT : player outside bounds : true" )
		RETURN TRUE
	ENDIF
	IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		PRINTLN("[SEATS] SHOULD_TERMINATE_SCRIPT : SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE : true" )
		RETURN TRUE
	ENDIF	
	IF g_ownerOfBasePropertyIAmIn = INVALID_PLAYER_INDEX()
		PRINTLN("[SEATS] SHOULD_TERMINATE_SCRIPT : g_ownerOfBasePropertyIAmIn = INVALID_PLAYER_INDEX() : true" )
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC SCRIPT_CLEANUP()
	PRINTLN("[SEATS] CLEANING UP LIVING QUATERS SEATS")
	CLEANUP_SEATS_ACTIVITY(m_seatData)
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

SCRIPT
	IF SCRIPT_INIT()
		WHILE NOT SHOULD_TERMINATE_SCRIPT()					
			MAINTAIN_SEATS_ACTIVITY(m_seatData)
			WAIT(0)
		ENDWHILE
	ENDIF
	SCRIPT_CLEANUP()
ENDSCRIPT
