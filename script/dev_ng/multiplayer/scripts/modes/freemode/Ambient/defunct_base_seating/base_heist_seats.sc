///    Author 	: 	Orlando C-H
///    Team 	: 	Online Tech
///    Info		: 	Each seating area has its own script so that the 
///    				MAX_NUM_SEATS_FOR_SITTING_ACTIVITY can be redefined to minimise
///    				wasted space.


CONST_INT MAX_NUM_SEATS_FOR_SITTING_ACTIVITY 		30 /* Exact number of seats to keep stack size low	*/
CONST_INT LOCATE_CHECKS_PER_TICK 					5  /* Keep low for more effective staggering 	*/	
USING "defunct_base_sitting_activities_public.sch"
USING "defunct_base_seat_areas.sch"
USING "net_include.sch"
using "script_maths.sch"

SEATS_LOCAL_DATA 	m_seatData
INT 				m_iBSPrevFrame				//Keep track of various activities happening in previous frame

CONST_INT			ciPLANNING_BOARD_PREV_FRAME	0 
CONST_INT 			ciPI_MENU_PREV_FRAME		1
CONST_INT 			ciPAUSE_MENU_PREV_FRAME		2
CONST_INT 			ciPHONE_PREV_FRAME			3
CONST_INT			ciBROWSER_PREV_FRAME		4
TWEAK_FLOAT cfANIM_ADJUSTMENT_X 0.0
TWEAK_FLOAT cfANIM_ADJUSTMENT_Y	0.01
TWEAK_FLOAT cfANIM_ADJUSTMENT_Z 0.015
TWEAK_FLOAT cfBENCH_ANIM_ROT_OFFSET 180.0
TWEAK_FLOAT cfLOCATE_FORWARD_OFFSET -0.7
CONST_FLOAT LOCATE_WIDTH 1.0
TWEAK_FLOAT FACING_THRESHOLD		0.36
#IF IS_DEBUG_BUILD
STRUCT DEBUG_DATA
	BOOL bFemale 	= FALSE
	BOOL bSpawnPeds = FALSE
	BOOL bClearPeds	= FALSE
	BOOL bToggleSex = FALSE
	PED_INDEX peds[MAX_NUM_SEATS_FOR_SITTING_ACTIVITY]
	INT iAnimClip	= 0
	BOOL bForceClip	= FALSE
	BOOL bDrawAnimPos = FALSE
	INT iMinPedIdx 	= 0
	INT iMaxPedIdx 	= 15
	BOOL bUpdateData = FALSE
ENDSTRUCT
DEBUG_DATA m_debugData
#ENDIF

//Checks whether the player is using the planning board and if they are facing away from the board
//if they are using the front row seats.
FUNC BOOL CAN_PLAYER_USE_SEAT(SEAT_DATA_STRUCT& seatData, INT iSeatIndex)
	//If not front row, let them face any direction.
	IF iSeatIndex < 22
		RETURN TRUE	
	ENDIF
	
	//Need to grab ped rotation later
	IF NOT DOES_ENTITY_EXIST(PLAYER_PED_ID())
	OR IS_ENTITY_DEAD(PLAYER_PED_ID())
		RETURN FALSE
	ENDIF
	
	//Don't show prompt if player is interacting with planning board (or doing so in prev frame)
	IF IS_PLAYER_INTERACTING_WITH_GANG_OPS_PLANNING_SCREEN(PLAYER_ID()) 
	OR IS_BIT_SET(m_iBSPrevFrame, ciPLANNING_BOARD_PREV_FRAME)
		RETURN FALSE
	ENDIF
	
	//Heading of seat based on entry locate coords
	VECTOR v0 = seatData.entryLocate.v0
	VECTOR v1 = seatData.entryLocate.v1
	v0.z = 0
	v1.z = 0
	VECTOR vLocateFacing = NORMALISE_VECTOR(v0 - v1)
		
	//Get player facing	
	VECTOR vForward = GET_ENTITY_FORWARD_VECTOR(PLAYER_PED_ID())
	FLOAT fDot = DOT_PRODUCT(vLocateFacing, vForward)
	BOOL bAllowPrompt = fDot > FACING_THRESHOLD
	
	RETURN bAllowPrompt
ENDFUNC


FUNC VECTOR GET_LOCATE_OFFSET_FROM_ANIM_V0()
	RETURN <<0.0,0.1,1.0>>
ENDFUNC

FUNC VECTOR GET_LOCATE_OFFSET_FROM_ANIM_V1()
	RETURN <<0.0, cfLOCATE_FORWARD_OFFSET, -1.0>>
ENDFUNC

PROC SETUP_HEIST_SEAT(INT iIndex, VECTOR vAnimPos, FLOAT fAnimRotZ, VECTOR vLocateOffsetV0, VECTOR vLocateOffsetV1)
	m_seatData.public.seats[iIndex].eType = HEIST_BENCH 
	m_seatData.public.seats[iIndex].entryLocate.v0 = vAnimPos + ROTATE_VECTOR_ABOUT_Z(vLocateOffsetV0, fAnimRotZ - cfBENCH_ANIM_ROT_OFFSET)
	m_seatData.public.seats[iIndex].entryLocate.v1 = vAnimPos + ROTATE_VECTOR_ABOUT_Z(vLocateOffsetV1, fAnimRotZ- cfBENCH_ANIM_ROT_OFFSET)
	m_seatData.public.seats[iIndex].entryLocate.fWidth = LOCATE_WIDTH
	m_seatData.public.seats[iIndex].animation.vPos = vAnimPos
	m_seatData.public.seats[iIndex].animation.vRot = <<0, 0, fAnimRotZ>>
ENDPROC

FUNC VECTOR APPLY_ANIM_ADJUSTMENTS(VECTOR vOriginalPos, FLOAT fAnimRotZ)
	RETURN vOriginalPos + ROTATE_VECTOR_ABOUT_Z(<<cfANIM_ADJUSTMENT_X, cfANIM_ADJUSTMENT_Y, cfANIM_ADJUSTMENT_Z>>, fAnimRotZ)
ENDFUNC

PROC STORE_SEAT_DATA()
	VECTOR vLocateOffsetV0 = GET_LOCATE_OFFSET_FROM_ANIM_V0()
	VECTOR vLocateOffsetV1 = GET_LOCATE_OFFSET_FROM_ANIM_V1()
	//Back left (looking towards board) left to right
	SETUP_HEIST_SEAT(0, 	APPLY_ANIM_ADJUSTMENTS(<<343.930, 4875.389, -60.440>>, -90.0), -90.000, 	vLocateOffsetV0, vLocateOffsetV1) 
	SETUP_HEIST_SEAT(1,		APPLY_ANIM_ADJUSTMENTS(<<344.080, 4874.239, -60.440>>, -86.0), -86.000, 	vLocateOffsetV0, vLocateOffsetV1)
	SETUP_HEIST_SEAT(2,		APPLY_ANIM_ADJUSTMENTS(<<344.330, 4873.189, -60.440>>, -78.0), -78.000,		vLocateOffsetV0, vLocateOffsetV1)
	SETUP_HEIST_SEAT(3, 	APPLY_ANIM_ADJUSTMENTS(<<344.700, 4872.239, -60.440>>, -72.0), -72.000, 	vLocateOffsetV0, vLocateOffsetV1)
	SETUP_HEIST_SEAT(4, 	APPLY_ANIM_ADJUSTMENTS(<<345.060, 4871.480, -60.440>>, -67.0), -67.000,		vLocateOffsetV0, vLocateOffsetV1)
	SETUP_HEIST_SEAT(5, 	APPLY_ANIM_ADJUSTMENTS(<<345.860, 4870.289, -60.440>>, -60.0), -60.000,		vLocateOffsetV0, vLocateOffsetV1)
	//Back right (looking towards board) left to right
	SETUP_HEIST_SEAT(6, 	APPLY_ANIM_ADJUSTMENTS(<<350.410, 4867.050, -60.440>>, -26.0), 	-26.000, 	vLocateOffsetV0, vLocateOffsetV1)
	SETUP_HEIST_SEAT(7, 	APPLY_ANIM_ADJUSTMENTS(<<351.510, 4866.750, -60.440>>, -17.0), 	-17.000, 	vLocateOffsetV0, vLocateOffsetV1)
	SETUP_HEIST_SEAT(8, 	APPLY_ANIM_ADJUSTMENTS(<<352.660, 4866.600, -60.440>>, -10.0), 	-10.000, 	vLocateOffsetV0, vLocateOffsetV1)
	SETUP_HEIST_SEAT(9, 	APPLY_ANIM_ADJUSTMENTS(<<353.600, 4866.580, -60.440>>, -4.0), 	-4.000, 	vLocateOffsetV0, vLocateOffsetV1)
	SETUP_HEIST_SEAT(10,	APPLY_ANIM_ADJUSTMENTS(<<354.700, 4866.680, -60.440>>, 4.0), 	4.000, 		vLocateOffsetV0, vLocateOffsetV1)
	SETUP_HEIST_SEAT(11, 	APPLY_ANIM_ADJUSTMENTS(<<355.850, 4866.910, -60.440>>, 10.0), 	10.000, 	vLocateOffsetV0, vLocateOffsetV1)
	//Mid left
	SETUP_HEIST_SEAT(12, 	APPLY_ANIM_ADJUSTMENTS(<<346.020, 4876.871, -60.838>>, -102.0),	-102.000,	vLocateOffsetV0, vLocateOffsetV1)
	SETUP_HEIST_SEAT(13, 	APPLY_ANIM_ADJUSTMENTS(<<345.970, 4875.721, -60.838>>, -93.0),	-93.000, 	vLocateOffsetV0, vLocateOffsetV1)
	SETUP_HEIST_SEAT(14, 	APPLY_ANIM_ADJUSTMENTS(<<346.190, 4874.210, -60.838>>, -81.0), 	-81.000, 	vLocateOffsetV0, vLocateOffsetV1)
	SETUP_HEIST_SEAT(15, 	APPLY_ANIM_ADJUSTMENTS(<<346.620, 4873.010, -60.838>>, -73.0),	-73.000, 	vLocateOffsetV0, vLocateOffsetV1)
	SETUP_HEIST_SEAT(16, 	APPLY_ANIM_ADJUSTMENTS(<<347.270, 4871.860, -60.838>>, -62.000),-62.000, 	vLocateOffsetV0, vLocateOffsetV1)
	//Mid right
	SETUP_HEIST_SEAT(17, 	APPLY_ANIM_ADJUSTMENTS(<<351.570, 4868.860, -60.838>>, -20.0), 	-20.000, 	vLocateOffsetV0, vLocateOffsetV1)
	SETUP_HEIST_SEAT(18, 	APPLY_ANIM_ADJUSTMENTS(<<352.890, 4868.650, -60.838>>, -9.0), 	-9.000, 	vLocateOffsetV0, vLocateOffsetV1)
	SETUP_HEIST_SEAT(19, 	APPLY_ANIM_ADJUSTMENTS(<<354.170, 4868.680, -60.838>>, 0.0), 	0.000, 		vLocateOffsetV0, vLocateOffsetV1)
	SETUP_HEIST_SEAT(20, 	APPLY_ANIM_ADJUSTMENTS(<<355.120, 4868.850, -60.838>>, 10.0), 	8.000, 		vLocateOffsetV0, vLocateOffsetV1)
	SETUP_HEIST_SEAT(21, 	APPLY_ANIM_ADJUSTMENTS(<<356.580, 4869.380, -60.838>>, 10.0), 	20.000, 	vLocateOffsetV0, vLocateOffsetV1)
	//Front left
	SETUP_HEIST_SEAT(22, 	APPLY_ANIM_ADJUSTMENTS(<<348.200, 4876.560, -61.240>>, -101.0),	-101.000, 	vLocateOffsetV0, vLocateOffsetV1)
	SETUP_HEIST_SEAT(23, 	APPLY_ANIM_ADJUSTMENTS(<<348.200, 4875.310, -61.240>>, -88.0),	-88.000, 	vLocateOffsetV0, vLocateOffsetV1)
	SETUP_HEIST_SEAT(24,	APPLY_ANIM_ADJUSTMENTS(<<348.470, 4874.280, -61.240>>, -76.0),	-76.000, 	vLocateOffsetV0, vLocateOffsetV1)
	SETUP_HEIST_SEAT(25,	APPLY_ANIM_ADJUSTMENTS(<<349.170, 4872.970, -61.240>>, -60.0), 	-60.000, 	vLocateOffsetV0, vLocateOffsetV1)
	//Front right
	SETUP_HEIST_SEAT(26, 	APPLY_ANIM_ADJUSTMENTS(<<352.120, 4870.980, -61.240>>, -20.0), 	-20.000, 	vLocateOffsetV0, vLocateOffsetV1)
	SETUP_HEIST_SEAT(27,	APPLY_ANIM_ADJUSTMENTS(<<353.420, 4870.840, -61.240>>, -6.0), 	-6.000, 	vLocateOffsetV0, vLocateOffsetV1)
	SETUP_HEIST_SEAT(28, 	APPLY_ANIM_ADJUSTMENTS(<<354.670, 4870.995, -61.240>>, 8.0), 	8.000, 		vLocateOffsetV0, vLocateOffsetV1)
	SETUP_HEIST_SEAT(29,	APPLY_ANIM_ADJUSTMENTS(<<356.020, 4871.545, -61.240>>, 25.000), 25.000, 	vLocateOffsetV0, vLocateOffsetV1)
	
	SET_ADDITIONAL_CHECK_BEFORE_PROMPT(m_seatData, &CAN_PLAYER_USE_SEAT)
ENDPROC

PROC SET_BIT_VALUE(INT& iBS, INT iBit, BOOL bValue)
	IF bValue
		SET_BIT(iBS, iBit)
	ELSE
		CLEAR_BIT(iBS, iBit)
	ENDIF
ENDPROC

PROC MAINTAIN_SCRIPT_UPDATE()
	MAINTAIN_SEATS_ACTIVITY(m_seatData)
	//If I own the base then I should be able to interact with 
	//the heist planning board, change how to get up, and hide
	//the default leave_seat help text.
	IF g_ownerOfBasePropertyIAmIn = PLAYER_ID()
	AND NOT IS_BIT_SET(GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iGeneralBitSet, GANG_OPS_BD_GENERAL_BITSET_BOARD_DISABLED_IN_PI_MENU)
		IF IS_PLAYER_IN_ANY_SEAT(m_seatData, TRUE, TRUE)
			SEATS_BLOCK_DEFAULT_EXIT_THIS_FRAME(m_seatData)
		ENDIF
		IF IS_PLAYER_IN_ANY_SEAT(m_seatData, FALSE, FALSE)
			SET_BIT(g_iFacilityHeistPlanningSeats, ciFACILITY_HEIST_PLANNING_SEATS_READY_FOR_BOARD)
			IF m_iBSPrevFrame = 0
			AND NOT IS_BIT_SET(GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iGeneralBitSet, GANG_OPS_BD_GENERAL_BITSET_INTERACTING_WITH_BOARD) 
			AND NOT IS_PI_MENU_OPEN()
			AND NOT IS_PAUSE_MENU_ACTIVE()
			AND NOT IS_PHONE_ONSCREEN()
			AND NOT IS_BROWSER_OPEN()
			AND IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RRIGHT)
				SEATS_LEAVE_SEAT(m_seatData)
			ENDIF
		ELSE 
			CLEAR_BIT(g_iFacilityHeistPlanningSeats, ciFACILITY_HEIST_PLANNING_SEATS_READY_FOR_BOARD)
		ENDIF
	ELSE
		CLEAR_BIT(g_iFacilityHeistPlanningSeats, ciFACILITY_HEIST_PLANNING_SEATS_READY_FOR_BOARD)
	ENDIF
	
	m_iBSPrevFrame = 0
	IF IS_BIT_SET(GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iGeneralBitSet, GANG_OPS_BD_GENERAL_BITSET_INTERACTING_WITH_BOARD)
		SET_BIT(m_iBSPrevFrame, ciPLANNING_BOARD_PREV_FRAME)
	ENDIF
	IF IS_PI_MENU_OPEN()
		SET_BIT(m_iBSPrevFrame, ciPI_MENU_PREV_FRAME)
	ENDIF
	IF IS_PAUSE_MENU_ACTIVE()
		SET_BIT(m_iBSPrevFrame, ciPAUSE_MENU_PREV_FRAME)
	ENDIF
	IF IS_BROWSER_OPEN()
		SET_BIT(m_iBSPrevFrame, ciBROWSER_PREV_FRAME)
	ENDIF
	IF IS_PHONE_ONSCREEN()
		SET_BIT(m_iBSPrevFrame, ciPHONE_PREV_FRAME)
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
PROC INIT_WIDGETS()
	START_WIDGET_GROUP("Heist room")
		ADD_WIDGET_BOOL("Spawn peds", m_debugData.bSpawnPeds)
		ADD_WIDGET_BOOL("Clear peds", m_debugData.bClearPeds)
		ADD_WIDGET_BOOL("Toggle ped sex", m_debugData.bToggleSex)
		ADD_WIDGET_BOOL("Debug draw", m_debugData.bDrawAnimPos)
		ADD_WIDGET_INT_SLIDER("Ped anim clip", m_debugData.iAnimClip, 0, 4, 1)
		ADD_WIDGET_BOOL("Force ped clip", m_debugData.bForceClip)
		ADD_WIDGET_INT_SLIDER("Ped idx min", m_debugData.iMinPedIdx,0, 30, 1)
		ADD_WIDGET_INT_SLIDER("Ped idx max", m_debugData.iMaxPedIdx,0, 30, 1)
		ADD_WIDGET_FLOAT_SLIDER("cfANIM_ADJUSTMENT_X", cfANIM_ADJUSTMENT_X, -1, 1, 0.01)
		ADD_WIDGET_FLOAT_SLIDER("cfANIM_ADJUSTMENT_Y", cfANIM_ADJUSTMENT_Y, -1, 1, 0.01)
		ADD_WIDGET_FLOAT_SLIDER("cfANIM_ADJUSTMENT_Z", cfANIM_ADJUSTMENT_Z, -1, 1, 0.01)
		ADD_WIDGET_BOOL("Update seat data", m_debugData.bUpdateData)
		ADD_WIDGET_INT_SLIDER("FORCED_ANIM_VARIATION", m_seatData.debug.iNextForcedAnimVar, 0, 20, 1)
		ADD_WIDGET_BOOL("FORCE_NOW", m_seatData.debug.bForceAnimVarNow)	
		ADD_WIDGET_FLOAT_SLIDER("cfMOVE_TO_SEAT_TARGET_RADIUS", cfMOVE_TO_SEAT_TARGET_RADIUS, 0, 1.5, 0.05)
		ADD_WIDGET_FLOAT_SLIDER("HEIST_BENCH_CAPSULE_SIZE", HEIST_BENCH_CAPSULE_SIZE, 0, 0.5, 0.005)
		ADD_WIDGET_FLOAT_SLIDER("cfLOCATE_FORWARD_OFFSET", cfLOCATE_FORWARD_OFFSET, -3, 0, 0.05)
	STOP_WIDGET_GROUP()
ENDPROC

PROC GET_ANIM_DATA_FOR_DEBUG(TEXT_LABEL_63& out_txtDict)
	out_txtDict = "ANIM@AMB@FACILITY@BRIEFING_ROOM@SEATING"
	IF m_debugData.bFemale
		out_txtDict += "@FEMALE"	
	ELSE
		out_txtDict += "@MALE"	
	ENDIF
	SWITCH m_debugData.iAnimClip
		CASE 0 out_txtDict += "@VAR_A@"	BREAK
		CASE 1 out_txtDict += "@VAR_B@"	BREAK
		CASE 2 out_txtDict += "@VAR_C@"	BREAK
		CASE 3 out_txtDict += "@VAR_D@"	BREAK
		CASE 4 out_txtDict += "@VAR_E@"	BREAK
	ENDSWITCH
ENDPROC

PROC MAINTAIN_WIDGETS()
	INT iPed
	IF g_ownerOfBasePropertyIAmIn <> INVALID_PLAYER_INDEX()
	AND NETWORK_GET_THIS_SCRIPT_IS_NETWORK_SCRIPT()
	AND NETWORK_IS_HOST_OF_THIS_SCRIPT()
		SEAT_DATA_STRUCT seat
		TEXT_LABEL_63 txtDict
		GET_ANIM_DATA_FOR_DEBUG(txtDict)
		
		IF m_debugData.bToggleSex
			m_debugData.bToggleSex = FALSE
			m_debugData.bSpawnPeds = TRUE
			m_debugData.bFemale = !m_debugData.bFemale
		ENDIF
		
		IF m_debugData.bSpawnPeds
			m_debugData.bClearPeds = TRUE
		ENDIF
		
		IF m_debugData.bClearPeds
			m_debugData.bClearPeds = FALSE
			REPEAT MAX_NUM_SEATS_FOR_SITTING_ACTIVITY iPed
				IF DOES_ENTITY_EXIST(m_debugData.peds[iPed])
					DELETE_PED(m_debugData.peds[iPed])
				ENDIF
			ENDREPEAT
		ENDIF
		
		IF m_debugData.bSpawnPeds
			REQUEST_MODEL(MP_M_FREEMODE_01)
			REQUEST_MODEL(MP_F_FREEMODE_01)
			REQUEST_ANIM_DICT(txtDict)
			IF HAS_MODEL_LOADED(MP_M_FREEMODE_01)
			AND HAS_MODEL_LOADED(MP_F_FREEMODE_01)
			AND HAS_ANIM_DICT_LOADED(txtDict)
				m_debugData.bSpawnPeds = FALSE
				m_debugData.bForceClip = TRUE
				VECTOR vPos
				VECTOR vRot
				INT idx
				REPEAT m_debugData.iMaxPedIdx iPed
					idx = iPed + m_debugData.iMinPedIdx
					seat = m_seatData.public.seats[iPed]
					vPos =  GET_ANIM_INITIAL_OFFSET_POSITION(txtDict, "BASE", seat.animation.vPos, seat.animation.vRot) 
					vRot = GET_ANIM_INITIAL_OFFSET_ROTATION(txtDict, "BASE", seat.animation.vPos, seat.animation.vRot)
					IF m_debugData.bFemale
						m_debugData.peds[idx]= CREATE_PED(PEDTYPE_CIVMALE, MP_M_FREEMODE_01, vPos, vRot.z, TRUE, FALSE)
					ELSE
						m_debugData.peds[idx]= CREATE_PED(PEDTYPE_CIVFEMALE, MP_F_FREEMODE_01, vPos, vRot.z, TRUE, FALSE)
					ENDIF
				ENDREPEAT
			ENDIF
		ELSE
			SET_MODEL_AS_NO_LONGER_NEEDED(MP_M_FREEMODE_01)
			SET_MODEL_AS_NO_LONGER_NEEDED(MP_F_FREEMODE_01)
		ENDIF
		
		IF m_debugData.bForceClip
			m_debugData.bForceClip = FALSE
			INT iNetSceneID
			INT idx
			REPEAT m_debugData.iMaxPedIdx iPed
				idx = iPed + m_debugData.iMinPedIdx
				IF DOES_ENTITY_EXIST(m_debugData.peds[idx])
					seat = m_seatData.public.seats[idx]
					CLEAR_PED_TASKS_IMMEDIATELY(m_debugData.peds[idx])	
					iNetSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(seat.animation.vPos, seat.animation.vRot, DEFAULT, TRUE, FALSE)
					NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(m_debugData.peds[idx], iNetSceneID, txtDict, "BASE" , INSTANT_BLEND_IN, INSTANT_BLEND_OUT, 
														SYNCED_SCENE_DONT_INTERRUPT|SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_ON_ABORT_STOP_SCENE, RBF_PLAYER_IMPACT, DEFAULT)					
					NETWORK_START_SYNCHRONISED_SCENE(iNetSceneID)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(m_debugData.peds[idx])
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
	
	IF m_debugData.bUpdateData
		STORE_SEAT_DATA()
	ENDIF
	
	IF m_debugData.bDrawAnimPos
		REPEAT MAX_NUM_SEATS_FOR_SITTING_ACTIVITY iPed
			DRAW_DEBUG_SPHERE(m_seatData.public.seats[iPed].animation.vPos, 0.05)
		ENDREPEAT
	ENDIF
ENDPROC
#ENDIF

FUNC BOOL SCRIPT_INIT()
	IF g_ownerOfBasePropertyIAmIn <> INVALID_PLAYER_INDEX()
		IF NETWORK_IS_SCRIPT_ACTIVE("base_heist_seats", NATIVE_TO_INT(g_ownerOfBasePropertyIAmIn), TRUE)
			PRINTLN("[SEATS] base_heist_seats : SCRIPT_INIT : script with this ID already running) :  terminating.")
			RETURN FALSE
		ENDIF
		NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE, NATIVE_TO_INT(g_ownerOfBasePropertyIAmIn))
		HANDLE_NET_SCRIPT_INITIALISATION()
		SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
		
		#IF IS_DEBUG_BUILD
		INIT_WIDGETS()
		#ENDIF //IS_DEBUG_BUILD
		
		STORE_SEAT_DATA()
		
		PRINTLN("[SEATS] base_heist_seats : SCRIPT_INIT : Complete. size_of(m_seatData) = ", SIZE_OF(m_seatData))
		RETURN TRUE
	ELSE
		PRINTLN("[SEATS] base_heist_seats : SCRIPT_INIT : g_ownerOfBasePropertyIAmIn = INVALID_PLAYER_INDEX() :  terminating.")
		RETURN FALSE
	ENDIF
ENDFUNC

FUNC BOOL SHOULD_TERMINATE_SCRIPT()
	IF NOT IS_ENTITY_IN_DEFUNCT_BASE_HEIST_ROOM(PLAYER_PED_ID())
		PRINTLN("[SEATS] SHOULD_TERMINATE_SCRIPT : player outside bounds : true" )
		RETURN TRUE
	ENDIF
	IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		PRINTLN("[SEATS] SHOULD_TERMINATE_SCRIPT : SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE : true" )
		RETURN TRUE
	ENDIF	
	IF g_ownerOfBasePropertyIAmIn = INVALID_PLAYER_INDEX()
		PRINTLN("[SEATS] SHOULD_TERMINATE_SCRIPT : g_ownerOfBasePropertyIAmIn = INVALID_PLAYER_INDEX() : true" )
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC SCRIPT_CLEANUP()
	PRINTLN("[SEATS] CLEANING UP HEIST PLANNING ROOM SEATS")
	CLEAR_BIT(g_iFacilityHeistPlanningSeats, ciFACILITY_HEIST_PLANNING_SEATS_READY_FOR_BOARD)
	CLEANUP_SEATS_ACTIVITY(m_seatData)
	#IF IS_DEBUG_BUILD
		m_debugData.bClearPeds = TRUE
		MAINTAIN_WIDGETS()
	#ENDIF
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

SCRIPT
	IF SCRIPT_INIT()
		WHILE NOT SHOULD_TERMINATE_SCRIPT()	
			#IF IS_DEBUG_BUILD
			MAINTAIN_WIDGETS()
			#ENDIF //IS_DEBUG_BUILD
			MAINTAIN_SCRIPT_UPDATE()
			WAIT(0)
		ENDWHILE
	ENDIF
	SCRIPT_CLEANUP()
ENDSCRIPT
