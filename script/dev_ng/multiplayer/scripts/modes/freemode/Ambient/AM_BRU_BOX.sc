//////////////////////////////////////////////////////////////////////////////////////////
// Name:        AM_BRU_BOX.sc															//
// Description: Player calls for a special box from Brucie.								//
// Written by:  Ryan Baker																//
// Date: 23/01/2013																		//
//////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"

// Game Headers
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_path.sch"
//USING "commands_interiors.sch"
USING "commands_event.sch"

// Network Headers
USING "net_include.sch"
USING "net_events.sch"
USING "net_spawn.sch"

USING "net_mission.sch"
//
USING "net_scoring_common.sch"
USING "net_ambience.sch"

//USING "help_at_location.sch"
USING "commands_object.sch"
USING "net_xp_animation.sch"
USING "net_bru_box.sch"
USING "net_power_ups.sch"

USING "net_wait_zero.sch"

#IF IS_DEBUG_BUILD
USING "net_debug_log.sch"
#ENDIF

// Game States. 
CONST_INT GAME_STATE_INI 				0
CONST_INT GAME_STATE_RUNNING			1
CONST_INT GAME_STATE_LEAVE				2
CONST_INT GAME_STATE_TERMINATE_DELAY	3
CONST_INT GAME_STATE_END				4

TWEAK_INT RAGE_TIME						60000
CONST_INT LEAVE_RANGE					500
CONST_FLOAT INITIAL_PULSE				1000.0
CONST_FLOAT FAST_PULSE					250.0

ENUM BRU_BOX_STAGE_ENUM
	eBB_PICKUP_BOX,
	eBB_RAGE_MODE,
	eBB_CLEANUP
ENDENUM

//Server Bitset Data
CONST_INT biS_AnyCrewArrested		0
CONST_INT biS_AllCrewArrested		1
CONST_INT biS_AllCrewDead			2
CONST_INT biS_AllCrewLeftStartArea	3
CONST_INT biS_AnyCrewHasFinished 	4
CONST_INT biS_AllCrewHaveFinished 	5
CONST_INT biS_AllCrewHaveLeft 		6
CONST_INT biS_RageHasFinished		7
CONST_INT biS_UseGlobalCreation		8

// The server broadcast data.
// Everyone can read this data, only the server can update it
STRUCT ServerBroadcastData
	INT iServerGameState
		
	INT iServerBitSet
	
	NETWORK_INDEX niBox
		
	VECTOR vDropLocation
	
	INT iParticpantPickedUpBox = -1
	
	INT iInstance
	
	BRU_BOX_STAGE_ENUM eBruBoxStage = eBB_PICKUP_BOX
	
	SCRIPT_TIMER MissionTerminateDelayTimer
ENDSTRUCT
ServerBroadcastData serverBD


CONST_INT biP_PickedUpBox			0
CONST_INT biP_FinishedRage			1

// The client broadcast data.
// Everyone can read this data, only the local player only the server can update it
STRUCT PlayerBroadcastData
	INT iGameState
	
	INT iPlayerBitSet
	
	BRU_BOX_STAGE_ENUM eBruBoxStage = eBB_PICKUP_BOX
	
	#IF IS_DEBUG_BUILD
		INT iDebugPassFail = 0
	#ENDIF
ENDSTRUCT
PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]

///// MISSION VARIABLES /////
INT iBoolsBitSet
CONST_INT biInitialSetup			0
CONST_INT biHelpDisplayed			1
CONST_INT biBoxPickedUp				2
CONST_INT biSequencesCreated		3
CONST_INT biRageModeStarted			4
CONST_INT biRageScreenActive		5
CONST_INT biPulseOn					6
CONST_INT biRageScreenInitial		7

INT iDrop
//NETWORK_INDEX niBox

BLIP_INDEX BoxBlip

SCRIPT_TIMER iRageTimer

//SCRIPT_TIMER iPulseTimer
//FLOAT fPulseDelay = INITIAL_PULSE

//PTFX_ID ptfxBeacon
//INT iBleepSoundID = -1
//INT iPickupSoundID = -1

BOOL bInTutorialSession

// Widgets & Debug.
#IF IS_DEBUG_BUILD
	BOOL bHostEndMissionNow
	BOOL bWarpToBox
#ENDIF

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  MAIN PROCS                            ////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Do necessary pre game start ini.
// Returns FALSE if the script fails to receive an initial network broadcast.
FUNC BOOL PROCESS_PRE_GAME( MP_MISSION_DATA missionScriptArgs )
	
	// This marks the script as a net script, and handles any instancing setup. 
	SETUP_MP_MISSION_FOR_NETWORK(GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(missionScriptArgs.mdID.idMission) ,  missionScriptArgs)	//NUM_NETWORK_PLAYERS, missionScriptArgs)	//
	//NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE, PARTICIPANT_ID_TO_INT())  
	
	//Reserve Entities
	//RESERVE_NETWORK_MISSION_PEDS(1)
	//RESERVE_NETWORK_MISSION_VEHICLES(1)
	RESERVE_NETWORK_MISSION_OBJECTS(1)

	// This makes sure the net script is active, waits untill it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
	
	// KGM: Wait for the first network broadcast before moving on
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		RETURN FALSE
	ENDIF
			
	// This script will not be paused if another script calls PAUSE_GAME
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		serverBD.iInstance = missionScriptArgs.iInstanceID
		PRINTLN("     ---------->     BRU BOX - serverBD.iInstance = ", serverBD.iInstance)
		g_i_BrucieInstanceDm = serverBD.iInstance
		serverBD.vDropLocation = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
		NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - serverBD.vDropLocation = ") NET_PRINT_VECTOR(serverBD.vDropLocation) NET_NL()
		IF NOT IS_VECTOR_ZERO(MPGlobalsAmbience.BruBoxDataDM.vBruBoxPosition)
			SET_BIT(serverBD.iServerBitSet, biS_UseGlobalCreation)
			NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - USING GLOBAL PICKUP CREATION POINT - biS_UseGlobalCreation SET ") NET_NL()
		ENDIF
	ENDIF
	
	bInTutorialSession = NETWORK_IS_IN_TUTORIAL_SESSION()
	
	playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_INI
	NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - PRE_GAME DONE - aa     <----------     ") NET_NL()
	
	RETURN TRUE
ENDFUNC

//Player leaves area
FUNC BOOL MISSION_END_CHECK()
		
	/*IF serverBD.eBruBoxStage = eBB_RAGE_MODE
		//Player Died in Rage
		IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
			NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - MISSION END - PLAYER DIED     <----------     ") NET_NL()
			RETURN TRUE
		ENDIF
		
		//Player entered a Corona
		IF GET_CORONA_STATUS() != CORONA_STATUS_IDLE
			NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - MISSION END - PLAYER IN CORONA     <----------     ") NET_NL()
			RETURN TRUE
		ENDIF
	EL*/
	
	//Bail if DM version is running and been cleared
	IF serverBD.iInstance >= NUM_NETWORK_PLAYERS
	AND GET_FM_MISSION_TYPE_PLAYER_IS_ON(PLAYER_ID()) = FMMC_TYPE_DEATHMATCH	
		IF IS_VECTOR_ZERO(MPGlobalsAmbience.BruBoxDataDM.vBruBoxPosition)
			NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - MISSION END - DEATHMATCH CLEARED THE BRU BOX     <----------     ") NET_NL()
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_PLAYER_AN_ANIMAL(PLAYER_ID())
		NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - MISSION END - IS_PLAYER_AN_ANIMAL    <----------     ") NET_NL()
		RETURN TRUE
	ENDIF
	
	IF serverBD.eBruBoxStage = eBB_CLEANUP
		NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - MISSION END - SERVER IN CLEANUP     <----------     ") NET_NL()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//Server only function that checks to see if the game mode should now end. For this test script, it if time has passed.
FUNC BOOL HAVE_MISSION_END_CONDITIONS_BEEN_MET()
	
	#IF IS_DEBUG_BUILD
		IF bHostEndMissionNow
			RETURN TRUE
		ENDIF
	#ENDIF	
	
	IF MISSION_END_CHECK()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


//Helper function to get a clients game/mission state
FUNC INT GET_CLIENT_MISSION_STATE(INT iPlayer)
	RETURN playerBD[iPlayer].iGameState
ENDFUNC

//Helper function to get the servers game/mission state
FUNC INT GET_SERVER_MISSION_STATE()
	RETURN serverBD.iServerGameState
ENDFUNC

#IF IS_DEBUG_BUILD
PROC CREATE_WIDGETS()
	INT iPlayer
	TEXT_LABEL_15 tl63

	START_WIDGET_GROUP("BRU BOX")  
		
		ADD_WIDGET_BOOL("Warp to Box", bWarpToBox)
		//ADD_WIDGET_FLOAT_READ_ONLY("Pulse Delay", fPulseDelay)
				
		// Gameplay debug, sever only.
		START_WIDGET_GROUP("Server Only Gameplay") 
			ADD_WIDGET_BOOL("End mission", bHostEndMissionNow)			
		STOP_WIDGET_GROUP()		
		
		// Data about the server
		START_WIDGET_GROUP("Server BD") 
				ADD_WIDGET_INT_SLIDER("S. Game state", serverBD.iServerGameState,-1, HIGHEST_INT,1)
		STOP_WIDGET_GROUP()	

		// Data about the clients. * = You.
		START_WIDGET_GROUP("Client BD")  				
			REPEAT 2 iPlayer
				tl63 = "Player "
				tl63 += iPlayer
				IF iPlayer = PARTICIPANT_ID_TO_INT()
					tl63 += "*"
				ENDIF
				START_WIDGET_GROUP(tl63)
					ADD_WIDGET_INT_SLIDER("Game state", playerBD[iPlayer].iGameState,-1, HIGHEST_INT,1)
				STOP_WIDGET_GROUP()
			ENDREPEAT				
		STOP_WIDGET_GROUP()	
		
	STOP_WIDGET_GROUP()
ENDPROC		

PROC UPDATE_WIDGETS()
	
	//Warp player to the ammo crate
	IF bWarpToBox = TRUE
		IF playerBD[PARTICIPANT_ID_TO_INT()].eBruBoxStage = eBB_RAGE_MODE
			IF NET_WARP_TO_COORD(serverBD.vDropLocation+<<0, 0, 1>>, 0, TRUE, FALSE)
				NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - UPDATE_WIDGETS - bWarpToBox DONE    <----------     ") NET_NL()
				bWarpToBox = FALSE
			ELSE
				NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - UPDATE_WIDGETS - bWarpToBox IN PROGRESS    <----------     ") NET_NL()
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

#ENDIF

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  ENTITY CREATION                  /////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////
///    		CRATE AND CHUTE    			///
///////////////////////////////////////////

/*FUNC BOOL LOAD_ANIMS()
	REQUEST_ANIM_DICT("P_cargo_chute_S")
		
	IF HAS_ANIM_DICT_LOADED("P_cargo_chute_S")
		NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - LOAD_ANIMS - DONE ") NET_NL()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC*/

//PURPOSE: Loads the Crate assets
FUNC BOOL LOAD_BOX_ASSETS()
	REQUEST_MODEL(PROP_DRUG_PACKAGE_02)
	
	IF HAS_MODEL_LOADED(PROP_DRUG_PACKAGE_02)
	//AND LOAD_ANIMS()
		//NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - LOAD_BOX_ASSETS - DONE ") NET_NL()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//Creates the Box
FUNC BOOL CREATE_BOX()

	PRINTLN("2588666 CREATE_BOX ")
	
	//Create the Box
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niBox)
		IF LOAD_BOX_ASSETS()
			IF CAN_REGISTER_MISSION_OBJECTS(1)						
				
				VECTOR vPos
				FLOAT fHeading
				
				BOOL bUseAngledArea
				FLOAT fDistance = 250
				VECTOR vLocation1 = serverBD.vDropLocation
				VECTOR vLocation2
				
				//Get Deathmatch Coord
				IF IS_BIT_SET(serverBD.iServerBitSet, biS_UseGlobalCreation)
					vPos = MPGlobalsAmbience.BruBoxDataDM.vBruBoxPosition
					fHeading = MPGlobalsAmbience.BruBoxDataDM.fBruBoxHeading
				ENDIF
				
				//LTS
				IF GET_FM_MISSION_TYPE_PLAYER_IS_ON(PLAYER_ID()) = FMMC_TYPE_MISSION_LTS
					INT iTeam = GET_PLAYER_TEAM(PLAYER_ID())
					IF g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBoundsStruct[0].fWidth > 0
						vLocation1 = g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBoundsStruct[0].vPos1 
						vLocation2 = g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBoundsStruct[0].vPos2
						fDistance = g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBoundsStruct[0].fWidth
						bUseAngledArea = TRUE
						PRINTLN("     ---------->     BRU BOX - USE LTS ANGLED AREA")
						PRINTLN("     ---------->     BRU BOX - vLocation1 = ", vLocation1)
						PRINTLN("     ---------->     BRU BOX - vLocation2 = ", vLocation2)
						PRINTLN("     ---------->     BRU BOX - fDistance = ", fDistance)
					ELIF g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBoundsStruct[0].fRadius > 0
						vLocation1 = g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBoundsStruct[0].vPos
						fDistance = g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBoundsStruct[0].fRadius
						PRINTLN("     ---------->     BRU BOX - USE LTS SPHERE")
						PRINTLN("     ---------->     BRU BOX - vLocation1 = ", vLocation1)
						PRINTLN("     ---------->     BRU BOX - fDistance = ", fDistance)
					ENDIF
				ENDIF
				
				SPAWN_SEARCH_PARAMS SpawnSearchParams
				SpawnSearchParams.bPreferPointsCloserToRoads=TRUE
				SpawnSearchParams.fMinDistFromPlayer=30.0
				SpawnSearchParams.bEdgesOnly=TRUE
				
				IF bUseAngledArea = FALSE				
					IF IS_BIT_SET(serverBD.iServerBitSet, biS_UseGlobalCreation) 
					OR GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY(vLocation1, fDistance, vPos, fHeading, SpawnSearchParams)
					
						NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - serverBD.iInstance = ") NET_PRINT_INT(serverBD.iInstance) NET_NL()
						IF serverBD.iInstance >= NUM_NETWORK_PLAYERS
						OR GET_FM_MISSION_TYPE_PLAYER_IS_ON(PLAYER_ID()) = FMMC_TYPE_DEATHMATCH
							serverBD.niBox = OBJ_TO_NET(CREATE_PORTABLE_PICKUP(PICKUP_PORTABLE_PACKAGE, vPos, TRUE, PROP_DRUG_PACKAGE_02))
							NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - CREATE NETWORKED PICKUP - FOR DM") NET_NL()
						ELSE
							serverBD.niBox = OBJ_TO_NET(CREATE_NON_NETWORKED_PORTABLE_PICKUP(PICKUP_PORTABLE_PACKAGE, vPos, TRUE, PROP_DRUG_PACKAGE_02))
							NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - CREATE NON-NETWORK PICKUP - FOR FREEMODE") NET_NL()
						ENDIF
						
						SET_ENTITY_HEADING(NET_TO_OBJ(serverBD.niBox), fHeading)
						NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - SPHERE CHECK - CREATED BOX ")  NET_PRINT(" AT POS ") NET_PRINT_VECTOR(vPos) NET_NL()
					ENDIF
				ELSE
					IF IS_BIT_SET(serverBD.iServerBitSet, biS_UseGlobalCreation) 
					OR GET_SAFE_COORDS_IN_ANGLED_AREA_FOR_CREATING_ENTITY(vLocation1, vLocation2, fDistance, vPos, fHeading, SpawnSearchParams)
					
						NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - serverBD.iInstance = ") NET_PRINT_INT(serverBD.iInstance) NET_NL()
						IF serverBD.iInstance >= NUM_NETWORK_PLAYERS
						OR GET_FM_MISSION_TYPE_PLAYER_IS_ON(PLAYER_ID()) = FMMC_TYPE_DEATHMATCH
							serverBD.niBox = OBJ_TO_NET(CREATE_PORTABLE_PICKUP(PICKUP_PORTABLE_PACKAGE, vPos, TRUE, PROP_DRUG_PACKAGE_02))
							NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - CREATE NETWORKED PICKUP - FOR DM") NET_NL()
						ELSE
							serverBD.niBox = OBJ_TO_NET(CREATE_NON_NETWORKED_PORTABLE_PICKUP(PICKUP_PORTABLE_PACKAGE, vPos, TRUE, PROP_DRUG_PACKAGE_02))
							NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - CREATE NON-NETWORK PICKUP - FOR FREEMODE") NET_NL()
						ENDIF
												
						SET_ENTITY_HEADING(NET_TO_OBJ(serverBD.niBox), fHeading)
						NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - ANGLED AREA CHECK - CREATED BOX ")  NET_PRINT(" AT POS ") NET_PRINT_VECTOR(vPos) NET_NL()
					ENDIF
				ENDIF
			ELSE
				PRINTLN("2588666 REGISTER ")
			ENDIF
		ELSE
			PRINTLN("2588666 LOAD_BOX_ASSETS ")
		ENDIF
	ELSE
		PRINTLN("2588666 BOX EXISTS ")
	ENDIF
		
	IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niBox)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


//PURPOSE: Adds a blip to the box
PROC ADD_BOX_BLIP()
	IF NOT DOES_BLIP_EXIST(BoxBlip)
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niBox)
			BoxBlip = ADD_BLIP_FOR_ENTITY(NET_TO_OBJ(serverBD.niBox))
			
			IF IS_KING_OF_THE_HILL()
				SET_BLIP_SCALE(BoxBlip, cfKOTH_PICKUP_BLIP_SCALE)
			ELSE
				SET_BLIP_SCALE(BoxBlip, BLIP_SIZE_NETWORK_PICKUP_LARGE)
			ENDIF
			
			SET_BLIP_SPRITE(BoxBlip, RADAR_TRACE_TESTOSTERONE)
			SET_BLIP_COLOUR(BoxBlip, BLIP_COLOUR_GREEN)
			//SHOW_HEIGHT_ON_BLIP(BoxBlip, TRUE) - REMOVED FOR BUG 1613418
			//SET_BLIP_ALPHA(BoxBlip, 120)
			SET_BLIP_NAME_FROM_TEXT_FILE(BoxBlip, "ABB_BLIPN")
			NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - ADD_BOX_BLIP ") NET_NL()
		ENDIF
	//ELSE
	//	NET_SHOW_HEIGHT_ON_BLIP(BoxBlip)
	ENDIF
ENDPROC

PROC REMOVE_BOX_BLIP()
	IF DOES_BLIP_EXIST(BoxBlip)
		REMOVE_BLIP(BoxBlip)
		NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - REMOVE_BOX_BLIP ") NET_NL()
	ENDIF
ENDPROC	

//PURPOSE: Controls the display of help text
PROC CONTROL_HELP_TEXT()
	IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
		INT iStat = GET_MP_INT_CHARACTER_STAT(MP_STAT_TUTORIAL_BITSET)
		IF NOT IS_BIT_SET(iStat, TUTBS_BRU_BOX_HELP_1)
// Speirs removed see 1466647 and "DM_BRU_HLP"
//			PRINT_HELP("ABB_HELP1")	//~s~When collected the Brucie Box ~BLIP_TESTOSTERONE~ will give you a temporary boost to damage and toughness.
			SET_BIT(iStat, TUTBS_BRU_BOX_HELP_1)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_TUTORIAL_BITSET, iStat)
			NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - TUTBS_BRU_BOX_HELP_1 ") NET_NL()
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Returns TRUE if the player has picked up the Crate
FUNC BOOL HAS_PICKED_UP_BOX()
	IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niBox)
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niBox)
			IF IS_ENTITY_ATTACHED_TO_ENTITY(NET_TO_OBJ(serverBD.niBox), PLAYER_PED_ID())
			AND TAKE_CONTROL_OF_NET_ID(serverBD.niBox)
				//PLAY_SOUND_FROM_ENTITY(iPickupSoundID, "Crate_Collect", NET_TO_OBJ(serverBD.niBox), "MP_CRATE_DROP_SOUNDS")
							
				DELETE_NET_ID(serverBD.niBox)
				NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - DELETE BOX - A    <----------     ") NET_NL()
				
				IF DOES_BLIP_EXIST(BoxBlip)
					REMOVE_BLIP(BoxBlip)
				ENDIF
				
				// 2527917
				IF NOT GB_IS_THIS_BOSS_DROP_IN_PROGRESS(eGB_BOSS_DROPS_BULLSHARK)
					PRINT_TICKER("ABB_BOXCT")	//~s~Brucie Box Collected
				ENDIF
				
				//Send Ticker Out
				SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerEventData
				TickerEventData.TickerEvent = TICKER_EVENT_BRU_BOX_COLLECTED
				TickerEventData.playerID = PLAYER_ID()
				TickerEventData.dataInt = iDrop
				BROADCAST_TICKER_EVENT(TickerEventData, ALL_PLAYERS_ON_SCRIPT(TRUE))
				
				SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_PickedUpBox)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC




///////////////////////////////////////////
///    			RAGE MODE    			///
///////////////////////////////////////////

//PURPOSE: Cleans up the Rage Mode
PROC CLEAR_RAGE_MODE()
	IF IS_BIT_SET(iBoolsBitSet, biRageModeStarted)
		//Do Normal Damage
		PRINTLN("CS_DMG], CLEAR_RAGE_MODE")
		SET_PLAYER_WEAPON_DAMAGE_MODIFIER(PLAYER_ID(), FREEMODE_PLAYER_WEAPON_DAMAGE)
		#IF IS_DEBUG_BUILD
			SET_PLAYER_WEAPON_DAMAGE_MODIFIER(PLAYER_ID(), lw_fPlayerWeaponDamageModifier)
		#ENDIF
		SET_PLAYER_MELEE_WEAPON_DAMAGE_MODIFIER(PLAYER_ID(), FREEMODE_PLAYER_MELEE_WEAPON_DAMAGE)
		
		//Take Normal Damage
		SET_PLAYER_WEAPON_DEFENSE_MODIFIER(PLAYER_ID(), FREEMODE_PLAYER_WEAPON_DEFENSE)
		SET_PLAYER_MELEE_WEAPON_DEFENSE_MODIFIER(PLAYER_ID(), FREEMODE_PLAYER_MELEE_WEAPON_DEFENSE)
		
		//Normal Screen
		//CLEAR_TIMECYCLE_MODIFIER()
//		IF GET_TIMECYCLE_MODIFIER_INDEX() != -1
//			SET_TRANSITION_OUT_OF_TIMECYCLE_MODIFIER(2.0)
			STOP_FX_EFFECT(FX_REASON_RAGE) 
			CLEAR_BIT(iBoolsBitSet, biRageScreenActive)
			CLEAR_BIT(iBoolsBitSet, biRageScreenInitial)
			NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - CLEAR TIMCECYCLE - B") NET_NL()
//		ENDIF
		
		CLEAR_BIT(iBoolsBitSet, biRageModeStarted)
		NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - CLEAR_RAGE_MODE - DONE") NET_NL()
	ENDIF
ENDPROC

//PURPOSE: Controls whether the Rage filter should be displayed or cleaned up mid Rage Mode
PROC CONTROL_RAGE_SCREEN()
	IF IS_CELLPHONE_CAMERA_IN_USE()
		IF IS_BIT_SET(iBoolsBitSet, biRageScreenActive)
//			IF GET_TIMECYCLE_MODIFIER_INDEX() != -1
//				SET_TRANSITION_OUT_OF_TIMECYCLE_MODIFIER(2.0)
				STOP_FX_EFFECT(FX_REASON_RAGE) 
				CLEAR_BIT(iBoolsBitSet, biRageScreenActive)
				CLEAR_BIT(iBoolsBitSet, biRageScreenInitial)
				NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - CLEAR TIMCECYCLE - A") NET_NL()
//			ENDIF
			//CLEAR_BIT(iBoolsBitSet, biRageScreenActive)
			//NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - RAGE MODE - SET TIMECYCLE MODIFIER AGAIN") NET_NL()
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(iBoolsBitSet, biRageScreenInitial)
			//SET_CURRENT_PLAYER_TCMODIFIER("PULSE")
			//START_PLAYER_TCMODIFIER_TRANSITION()
			//SET_PLAYER_TCMODIFIER_TRANSITION()
			
//			IF GET_TIMECYCLE_MODIFIER_INDEX() = -1
//				SET_TRANSITION_TIMECYCLE_MODIFIER("MP_Bull_tos", 1.0)	//REDMIST	//"PULSE", fPulseDelay)
				TRIGGER_FX_EFFECT(FX_REASON_RAGE) 
				SET_BIT(iBoolsBitSet, biRageScreenInitial)
				NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - SET TIMCECYCLE - A") NET_NL()
//			ENDIF
			//SET_CONTROL_SHAKE(PLAYER_CONTROL, ROUND(fPulseDelay/2), 250)
			
			//SET_BIT(iBoolsBitSet, biRageScreenActive)
			//SET_BIT(iBoolsBitSet, biPulseOn)
			//NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - RAGE MODE - SET TIMECYCLE MODIFIER AGAIN") NET_NL()
		
		//Control in and out of Pulse
		ELSE
			IF NOT IS_BIT_SET(iBoolsBitSet, biRageScreenActive)	
				TRIGGER_FX_EFFECT(FX_REASON_RAGE) 
				SET_BIT(iBoolsBitSet, biRageScreenActive)
				NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - SET biRageScreenActive - B") NET_NL()
			ENDIF
			
//			IF GET_TIMECYCLE_MODIFIER_INDEX() = -1
//				IF IS_BIT_SET(iBoolsBitSet, biRageScreenActive)	
//					//SET_TRANSITION_TIMECYCLE_MODIFIER("MP_Bull_tost", 1.0)
//					TRIGGER_FX_EFFECT(FX_REASON_RAGE) 
//					NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - SET TIMCECYCLE - B") NET_NL()
//					CLEAR_BIT(iBoolsBitSet, biRageScreenActive)	
//					NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - CLEAR biRageScreenActive - B") NET_NL()
//				ENDIF
//			ELSE
//				IF NOT IS_BIT_SET(iBoolsBitSet, biRageScreenActive)	
//					SET_BIT(iBoolsBitSet, biRageScreenActive)
//					NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - SET biRageScreenActive - B") NET_NL()
//				ENDIF
//			ENDIF
			
			//IF GET_TIMECYCLE_MODIFIER_INDEX
			//	
			//ENDIF
			
			/*IF HAS_NET_TIMER_EXPIRED(iPulseTimer, ROUND(fPulseDelay))
				//PULSE ON
				IF NOT IS_BIT_SET(iBoolsBitSet, biPulseOn)
					SET_TRANSITION_TIMECYCLE_MODIFIER("PULSE", (fPulseDelay/1000))
					RESET_NET_TIMER(iPulseTimer)
					IF fPulseDelay = INITIAL_PULSE
						fPulseDelay = FAST_PULSE
					ENDIF
					SET_BIT(iBoolsBitSet, biPulseOn)
					
					INT iShakeAmount = (500 - ROUND(fPulseDelay))	//250 down to 100
					IF iShakeAmount < 100
						iShakeAmount = 100
					ENDIF
					SET_CONTROL_SHAKE(PLAYER_CONTROL, ROUND(fPulseDelay/2), iShakeAmount)
					
				//PULSE OFF
				ELSE
					IF fPulseDelay = INITIAL_PULSE
						fPulseDelay = FAST_PULSE
					ELSE
						fPulseDelay += 2.1
					ENDIF
					IF fPulseDelay > 375	//500
						fPulseDelay = 375	//500
					ENDIF
					
					SET_TRANSITION_OUT_OF_TIMECYCLE_MODIFIER(fPulseDelay/1000)
					CLEAR_BIT(iBoolsBitSet, biPulseOn)
					RESET_NET_TIMER(iPulseTimer)
				ENDIF
			ENDIF*/
			
		ENDIF
		
	ENDIF
		
	//IF GET_TIMECYCLE_REGION_OVERRIDE() = -1 WAITING FOR NEW CODE COMMAND
	//	SET_TRANSITION_TIMECYCLE_MODIFIER("PULSE", 1.0)
	//	NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - RAGE MODE - SET TIMECYCLE MODIFIER AGAIN") NET_NL()
	//ENDIF
ENDPROC

//PURPOSE: Processes the Rage Mode and returns TRUE when it has finished
FUNC BOOL HAS_RAGE_MODE_FINISHED()

	//RAGE MODE SETUP
	IF NOT IS_BIT_SET(iBoolsBitSet, biRageModeStarted)
		
		//Do Double Damage
		PRINTLN("CS_DMG], HAS_RAGE_MODE_FINISHED")
		SET_PLAYER_WEAPON_DAMAGE_MODIFIER(PLAYER_ID(), FREEMODE_PLAYER_WEAPON_DAMAGE*2)
		#IF IS_DEBUG_BUILD
			SET_PLAYER_WEAPON_DAMAGE_MODIFIER(PLAYER_ID(), lw_fPlayerWeaponDamageModifier*2)
		#ENDIF
		SET_PLAYER_MELEE_WEAPON_DAMAGE_MODIFIER(PLAYER_ID(), FREEMODE_PLAYER_MELEE_WEAPON_DAMAGE*2)
		
		//Take Half Damage
		IF NATIVE_TO_INT(PLAYER_ID()) != g_i_PowerPlayer
			SET_PLAYER_WEAPON_DEFENSE_MODIFIER(PLAYER_ID(), FREEMODE_PLAYER_WEAPON_DEFENSE/2)
			SET_PLAYER_MELEE_WEAPON_DEFENSE_MODIFIER(PLAYER_ID(), FREEMODE_PLAYER_MELEE_WEAPON_DEFENSE/2)
			NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - TAKE HALF DAMAGE") NET_NL()
		ELSE
			SET_PLAYER_WEAPON_DEFENSE_MODIFIER(PLAYER_ID(), FREEMODE_PLAYER_WEAPON_DEFENSE)
			SET_PLAYER_MELEE_WEAPON_DEFENSE_MODIFIER(PLAYER_ID(), FREEMODE_PLAYER_MELEE_WEAPON_DEFENSE)
			NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - TAKE NORMAL DAMAGE - PLAYER IS POWER PLAYER") NET_NL()
		ENDIF
		
		//Rage Screen
//		IF GET_TIMECYCLE_MODIFIER_INDEX() = -1
			//SET_TRANSITION_TIMECYCLE_MODIFIER("MP_Bull_tost", 1.0)	//"PULSE", fPulseDelay)
			TRIGGER_FX_EFFECT(FX_REASON_RAGE) 
			NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - SET TIMCECYCLE - C") NET_NL()
			CLEAR_BIT(iBoolsBitSet, biRageScreenActive)	
			NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - CLEAR biRageScreenActive - C") NET_NL()
//		ENDIF
		SET_BIT(iBoolsBitSet, biRageScreenActive)
		SET_BIT(iBoolsBitSet, biPulseOn)
		
		RESET_NET_TIMER(iRageTimer)
		SET_BIT(iBoolsBitSet, biRageModeStarted)
		NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - RAGE MODE - STARTED") NET_NL()
		
	//RAGE MODE ACTIVE	
	ELSE
		
		IF (RAGE_TIME-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(iRageTimer)) >= 0
			DRAW_GENERIC_TIMER((RAGE_TIME-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(iRageTimer)), "ABB_TIMER", 0, TIMER_STYLE_DONTUSEMILLISECONDS, DEFAULT, DEFAULT, HUDORDER_TOP)
		ELSE
			DRAW_GENERIC_TIMER(0, "ABB_TIMER", 0, TIMER_STYLE_DONTUSEMILLISECONDS, DEFAULT, DEFAULT, HUDORDER_TOP)
		ENDIF
		
		CONTROL_RAGE_SCREEN()
		
		IF HAS_NET_TIMER_EXPIRED(iRageTimer, RAGE_TIME)
		OR NOT IS_NET_PLAYER_OK(PLAYER_ID())
		OR GET_CORONA_STATUS() != CORONA_STATUS_IDLE
		OR IS_PLAYER_AN_ANIMAL(PLAYER_ID())
			CLEAR_RAGE_MODE()
			
			NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - RAGE MODE - FINISHED") NET_NL()
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  MISSION PROCS                  //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Cleanup Mission Data
PROC SCRIPT_CLEANUP()
	
	g_i_BrucieInstanceDm = -1
	
	IF DOES_BLIP_EXIST(BoxBlip)
		REMOVE_BLIP(BoxBlip)
	ENDIF
	
	CLEAR_RAGE_MODE()
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			//Delete Crate and Chute
			IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niBox)
			AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niBox)
				DELETE_NET_ID(serverBD.niBox)
				NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - DELETE CRATE - B    <----------     ") NET_NL()
			ENDIF
			
			serverBD.iServerGameState = GAME_STATE_END
			NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - serverBD.iServerGameState = GAME_STATE_END 999    <----------     ") NET_NL()
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_UseGlobalCreation)
		CLEAR_DM_BRU_BOX_DATA()
	ENDIF
	
	NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - CLEANUP MISSION      <----------     ") NET_NL()
	#IF IS_DEBUG_BUILD NET_LOG("SCRIPT_CLEANUP")	#ENDIF
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

//PURPOSE: Process the BRU BOX stages for the Client
PROC PROCESS_BRU_BOX_CLIENT()
	
	//playerBD[PARTICIPANT_ID_TO_INT()].eBruBoxStage = serverBD.eBruBoxStage
	
	SWITCH playerBD[PARTICIPANT_ID_TO_INT()].eBruBoxStage		
		CASE eBB_PICKUP_BOX
			CONTROL_HELP_TEXT() 
			ADD_BOX_BLIP()
			
			IF HAS_PICKED_UP_BOX()
				PU_GIVE_RAGE()
				REMOVE_BOX_BLIP()
				SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_AM_USE_BULL_SHARK)
				playerBD[PARTICIPANT_ID_TO_INT()].eBruBoxStage = eBB_RAGE_MODE
				NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - PLAYER STAGE = eBB_RAGE_MODE - PICKED UP THE BOX    <----------     ") NET_NL()
			ELIF serverBD.eBruBoxStage >= eBB_RAGE_MODE
				REMOVE_BOX_BLIP()
				playerBD[PARTICIPANT_ID_TO_INT()].eBruBoxStage = eBB_RAGE_MODE
				NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - PLAYER STAGE = eBB_RAGE_MODE - SERVER MOVED ON    <----------     ") NET_NL()
			ENDIF
		BREAK
		
		CASE eBB_RAGE_MODE
			/*IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_PickedUpBox)
			AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_FinishedRage)
				IF HAS_RAGE_MODE_FINISHED()
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_FinishedRage)
					NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - biP_FinishedRage SET    <----------     ") NET_NL()
					//playerBD[PARTICIPANT_ID_TO_INT()].eBruBoxStage = eBB_CLEANUP
					//NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - PLAYER STAGE = eBB_CLEANUP    <----------     ") NET_NL()
				ENDIF
			ENDIF*/
			
			
			
			IF serverBD.eBruBoxStage >= eBB_CLEANUP
				playerBD[PARTICIPANT_ID_TO_INT()].eBruBoxStage = eBB_CLEANUP
				NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - PLAYER STAGE = eBB_CLEANUP - SERVER MOVED ON    <----------     ") NET_NL()
			ENDIF
		BREAK
				
		CASE eBB_CLEANUP
			//NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - SCRIPT CLEANUP B     <----------     ") NET_NL()
			//SCRIPT_CLEANUP()
		BREAK
	ENDSWITCH
ENDPROC

//PURPOSE: Process the BRU BOX stages for the Server
PROC PROCESS_BRU_BOX_SERVER()
	SWITCH serverBD.eBruBoxStage		
		CASE eBB_PICKUP_BOX
			IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niBox)
			OR serverBD.iParticpantPickedUpBox != -1
				serverBD.eBruBoxStage = eBB_RAGE_MODE
				NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - SERVER STAGE = eBB_RAGE_MODE    <----------     ") NET_NL()
			ENDIF
		BREAK
		
		CASE eBB_RAGE_MODE
			//IF IS_BIT_SET(serverBD.iServerBitSet, biS_RageHasFinished)
				serverBD.eBruBoxStage = eBB_CLEANUP
				NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - SERVER STAGE = eBB_CLEANUP    <----------     ") NET_NL()
			//ENDIF
		BREAK
				
		CASE eBB_CLEANUP
			
		BREAK
	ENDSWITCH
ENDPROC


//PURPOSE: Runs through the max num participents and does relevant checks
PROC MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER()
	INT iParticipant
	
	//Set Bits that can then be cleared in the repeat if necessary
	
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
			//PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
			//PED_INDEX PlayerPedId = GET_PLAYER_PED(PlayerId)
			
			//** CHECKS THAT DON'T NEED THE PLAYER TO BE ALIVE **//
			//Check if any player has picked up the Box
			IF serverBD.iParticpantPickedUpBox = -1
				IF IS_BIT_SET(playerBD[iParticipant].iPlayerBitSet, biP_PickedUpBox)
					serverBD.iParticpantPickedUpBox = iParticipant
					NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER - BOX PICKED UP BY PLAYER ") NET_PRINT_INT(serverBD.iParticpantPickedUpBox) NET_NL()
				ENDIF
			ENDIF
			
			//Check if any player has finished the Boat Skip
			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_RageHasFinished)
				IF IS_BIT_SET(playerBD[iParticipant].iPlayerBitSet, biP_FinishedRage)
					SET_BIT(serverBD.iServerBitSet, biS_RageHasFinished)
					NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER - RAGE HAS FINISHED - biS_RageHasFinished SET") NET_NL()
				ENDIF
			ENDIF
			
			//** CHECKS THAT REQUIRE THE PLAYER TO BE ALIVE **//
			//IF IS_NET_PLAYER_OK(PlayerId)
			//	
			//ELSE
			//	//CLEAR_BIT(serverBD.iServerBitSet, biS_AllCrewArrested)
			//ENDIF
			
		ENDIF
	ENDREPEAT
ENDPROC


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                      Main Loop                      //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
SCRIPT(MP_MISSION_DATA missionScriptArgs)
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		#IF IS_DEBUG_BUILD	
			NET_PRINT_TIME() NET_PRINT("MP: Starting mission ") NET_NL()
		#ENDIF
		
		// Carry out all the initial game starting duties. 
		IF NOT PROCESS_PRE_GAME(missionScriptArgs)
			#IF IS_DEBUG_BUILD NET_LOG("FAILED TO RECEIVE INITIAL NETWORK BROADCAST")	#ENDIF
			NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - MISSION END - FAILED TO RECEIVE INITIAL NETWORK BROADCAST - SCRIPT CLEANUP E     <----------     ") NET_NL()
			SCRIPT_CLEANUP()
		ENDIF
		
		//Bail if this is a DM variation and we aren't on a DM
		IF missionScriptArgs.iInstanceID >= NUM_NETWORK_PLAYERS
			IF GET_FM_MISSION_TYPE_PLAYER_IS_ON(PLAYER_ID()) != FMMC_TYPE_DEATHMATCH
			
			AND NOT IS_BIT_SET(MPGlobalsAmbience.BruBoxDataDM.iBitSet, ciGB_BRU_BOX_ON)
			 //#IF FEATURE_GANG_BOSS
			
				NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - MISSION END - NOT IN DM - SCRIPT CLEANUP G     <----------     ") NET_NL()
				SCRIPT_CLEANUP()
			ENDIF
		ENDIF
		
		// Setup widgets. Make sure this is done after PROCESS_PRE_GAME()
		#IF IS_DEBUG_BUILD
			CREATE_WIDGETS()
		#ENDIF
	ENDIF
	
	iDrop = missionScriptArgs.mdGenericInt
	PRINTLN("     ---------->     BRU BOX 2588666, MAINTAIN_BRU_BOX_DEATHMATCH, iDrop   = ", iDrop)
	
	// Main loop
	WHILE TRUE

		// One wait to rule them all. This can be the ONLY wait from here on in.....
		MP_LOOP_WAIT_ZERO()
						
		// If we have a match end event, bail
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		//OR NOT IS_NET_PLAYER_OK(PLAYER_ID(), TRUE, TRUE)	//FALSE, TRUE)
			#IF IS_DEBUG_BUILD NET_LOG("SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE")	#ENDIF
			NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - MISSION END - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE - SCRIPT CLEANUP B     <----------     ") NET_NL()
			SCRIPT_CLEANUP()
		ENDIF
		
		//Check if BRU BOX shouldn't start
		/*IF NOT CAN_DO_BRU_BOX(FALSE)
			#IF IS_DEBUG_BUILD NET_LOG("CAN_DO_BRU_BOX")	#ENDIF
			NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - MISSION END - CAN_DO_BRU_BOX = FALSE - SCRIPT CLEANUP D     <----------     ") NET_NL()
			SCRIPT_CLEANUP()
		ENDIF*/
		
		//Bail if this is a DM variation and we aren't on a DM
		IF missionScriptArgs.iInstanceID >= NUM_NETWORK_PLAYERS
			IF GET_FM_MISSION_TYPE_PLAYER_IS_ON(PLAYER_ID()) != FMMC_TYPE_DEATHMATCH
			
			AND g_sBossDropOverall.iActiveCountBullshark = 0
			 //#IF FEATURE_GANG_BOSS
			
				NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - MISSION END - NOT IN DM - SCRIPT CLEANUP H     <----------     ") NET_NL()
				SCRIPT_CLEANUP()
			ENDIF
		ENDIF
		
		//Check if player has swapped from/to tutorial session and end
		IF NETWORK_IS_IN_TUTORIAL_SESSION() != bInTutorialSession
			#IF IS_DEBUG_BUILD NET_LOG("PLAYER IN TUT SESSION")	#ENDIF
			NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - MISSION END - PLAYER IN TUTORIAL SESSION - SCRIPT CLEANUP C     <----------     ") NET_NL()
			SCRIPT_CLEANUP()
		ENDIF
		
		NETWORK_PREVENT_SCRIPT_HOST_MIGRATION()	//ONLY LAUNCHING PLAYER SHOULD BE HOST. IF THEY LEAVE SCRIPT SHOULD END
		
		// Deal with the debug
		#IF IS_DEBUG_BUILD		
			UPDATE_WIDGETS()
		#ENDIF		
	
		// If we need to leave the mission, make sure we just straight to the end stage and terminate the script - DON'T NEED FOR AMBIENT MISSION
		/*IF SHOULD_PLAYER_LEAVE_MP_MISSION() 
			#IF IS_DEBUG_BUILD
			NET_PRINT_TIME() NET_PRINT( GET_MP_MISSION_NAME(thisMission), " Leave mission = TRUE \n")
			#ENDIF
			playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_LEAVE
			#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_LEAVE")	#ENDIF
		ENDIF*/
		
		// -----------------------------------
		// Process your game logic.....
		SWITCH GET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT())
			
			// Wait untill the server gives the all go before moving on
			CASE GAME_STATE_INI
				IF LOAD_BOX_ASSETS()
					IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
						playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_RUNNING
						NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_RUNNING    <----------     ") NET_NL()
						#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_RUNNING")	#ENDIF
					ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
						playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY
						NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY 1    <----------     ") NET_NL()
						#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_TERMINATE_DELAY")	#ENDIF
					ENDIF
				ENDIF
			BREAK
			
			// Main gameplay state.
			CASE GAME_STATE_RUNNING
				
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					PROCESS_BRU_BOX_CLIENT()
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY
					NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY 2   <----------     ") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_TERMINATE_DELAY")	#ENDIF
				ENDIF
				
				// Make the player end the mission if they leave the Area
				IF playerBD[PARTICIPANT_ID_TO_INT()].eBruBoxStage = eBB_PICKUP_BOX
					IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), serverBD.vDropLocation, <<LEAVE_RANGE, LEAVE_RANGE, LEAVE_RANGE>>)
					AND GET_FM_MISSION_TYPE_PLAYER_IS_ON(PLAYER_ID()) != FMMC_TYPE_DEATHMATCH
					
					AND NOT IS_BIT_SET(MPGlobalsAmbience.BruBoxDataDM.iBitSet, ciGB_BRU_BOX_ON)
					 //#IF FEATURE_GANG_BOSS
					
						playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
						NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END 6 - MISSION END - PLAYER NOT IN AREA ") NET_PRINT_VECTOR(serverBD.vDropLocation) NET_NL()
						#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_END 6")	#ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE GAME_STATE_TERMINATE_DELAY 
				SET_MISSION_FINISHED(serverBD.MissionTerminateDelayTimer)
		      	IF IS_MISSION_READY_TO_CLEANUP(serverBD.MissionTerminateDelayTimer)
		            playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
					NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END 3    <----------     ") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_END 3")	#ENDIF
		      	ENDIF
			BREAK
			
			// The game stage the local player is placed when we want him to leave a mission
			CASE GAME_STATE_LEAVE
				//LEAVE_MY_MP_MISSION(thisMission)
				playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
				NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END 2    <----------     ") NET_NL()
				#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_END 2")	#ENDIF
			FALLTHRU
			
			//Cleans up then terminates the mission
			CASE GAME_STATE_END
				NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - SCRIPT CLEANUP A     <----------     ") NET_NL()
				SCRIPT_CLEANUP()
			BREAK

		ENDSWITCH
		
		// -----------------------------------
		// Process server game logic		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			
			SWITCH GET_SERVER_MISSION_STATE()
				
				// For CnC group missions, we may want to wait for all players to have launched the script
				CASE GAME_STATE_INI
					IF CREATE_BOX()
						serverBD.iServerGameState = GAME_STATE_RUNNING
						NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - serverBD.iServerGameState = GAME_STATE_RUNNING    <----------     ") NET_NL()
						#IF IS_DEBUG_BUILD NET_LOG_Host("iServerGameState = GAME_STATE_RUNNING")	#ENDIF
					ENDIF
				BREAK
				
				// Look for game end conditions
				CASE GAME_STATE_RUNNING
					
					MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER()
					PROCESS_BRU_BOX_SERVER()
					
					#IF IS_DEBUG_BUILD
						IF bHostEndMissionNow
							serverBD.iServerGameState = GAME_STATE_END
							NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - serverBD.iServerGameState = GAME_STATE_END 1    <----------     ") NET_NL()
							#IF IS_DEBUG_BUILD NET_LOG_Host("iServerGameState = GAME_STATE_END 1")	#ENDIF
						ENDIF
					#ENDIF	
					
					IF HAVE_MISSION_END_CONDITIONS_BEEN_MET()
						serverBD.iServerGameState = GAME_STATE_END
						NET_PRINT_TIME() NET_PRINT("     ---------->     BRU BOX - serverBD.iServerGameState = GAME_STATE_END 4    <----------     ") NET_NL()
						#IF IS_DEBUG_BUILD NET_LOG_Host("iServerGameState = GAME_STATE_END 4")	#ENDIF
					ENDIF
					
				BREAK
				
				CASE GAME_STATE_END
				BREAK
				
			ENDSWITCH
			
		ENDIF
		
	ENDWHILE
	
ENDSCRIPT
