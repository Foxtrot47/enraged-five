// V - garage_doors.sch
//Author- Conor McGuire
// 7/6/2012
USING "globals.sch"

// Game Headers
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_brains.sch"

// Network Headers
USING "net_include.sch"
USING "net_events.sch"
USING "net_mission.sch"
USING "net_scoring_common.sch"

USING "selector_public.sch"
USING "net_garages.sch"
USING "net_doors.sch"

USING "net_wait_zero.sch"
#IF FEATURE_HEIST_ISLAND
USING "warptransition\heist_island_warp_data.sch"
#ENDIF

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////// VARIABLES /////////////////////////////////////////////////////////////////////////
// A var to easily refer to this mission.
MP_MISSION thisMission = eAM_DOORS

// Main States 
CONST_INT SERVER_STAGE_INIT 				0
CONST_INT SERVER_STAGE_CREATE				1
CONST_INT SERVER_STAGE_RUNNING				2
CONST_INT SERVER_STAGE_LEAVE				3
CONST_INT SERVER_STAGE_END					4

CONST_INT CLIENT_STAGE_INIT					0
CONST_INT CLIENT_STAGE_RUNNING				1
CONST_INT CLIENT_STAGE_END					2

CONST_INT NUMBER_OF_NON_GARAGE_DOORS		7
ENUM NON_GARAGE_DOORS
	NGD_PRISON_GATE_INNER_RIGHT = 0,
	NGD_PRISON_GATE_INNER,
	NGD_PRISON_GATE_FRONT,
	NGD_BIOLAB_L,
	NGD_BIOLAB_R,
	NGD_POLICE_ROOF,
	NGD_POLICE_GROUND
ENDENUM

SCRIPT_TIMER netStoredTime

BOOL bNoDoorsRegistered
BOOL bDoorsAdded
BOOL bDoorsExist

BOOL bDoorsOpenForRaces

INT iGameMode

INT iSlowLoopDoorIndex

CONST_INT NUM_ONE_WAY_ENTRANCES 6

MP_DOOR_ONE_WAY_DETAILS OneWayDetails[NUM_ONE_WAY_ENTRANCES]

INT iSlowPlayerLoop
INT iServerSlowOneWayDoorLoop

INT iClientSlowOneWayDoorLoop

CONST_INT SERVER_DOOR_UNLOCKED	0
CONST_INT SERVER_DOOR_LOCKED	1

//INT iBaseBarrierSounds[4]


// The server broadcast data.
// Everyone can read this data, only the server can update it. 
STRUCT ServerBroadcastData
	INT iServerStage
	INT iServerBS
	
	
	INT iOneWayDoorOpenRequests[NUM_ONE_WAY_ENTRANCES]
	INT iOneWayDoorForceOpenRequests[NUM_ONE_WAY_ENTRANCES]
	INT iDoorOpenStateBS
	NEW_GARAGE_DOOR_DATA_SERVER NewGarageDoors[TOTAL_NUM_MP_GARAGES]
	
ENDSTRUCT
ServerBroadcastData serverBD

INT iDoorSetBS[NUM_ONE_WAY_ENTRANCES]
SCRIPT_TIMER serverDoorStateTimeout[NUM_ONE_WAY_ENTRANCES]

// The client broadcast data.
// Everyone can read this data, only the local player only the server can update it. 
STRUCT PlayerBroadcastData
	INT iStage
	INT iClientBS
	INT iOneWayDoorOpenRequests
	INT iOneWayForcedOpenRequests
ENDSTRUCT
PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]

/// PURPOSE:
///    Do necessary pre game start ini.
/// RETURNS:
///    FALSE if the script fails to receive its initial network broadcast.
FUNC BOOL PROCESS_PRE_GAME( MP_MISSION_DATA missionScriptArgs )
	
	// This marks the script as a net script, and handles any instancing setup. 
	SETUP_MP_MISSION_FOR_NETWORK( GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(missionScriptArgs.mdID.idMission) ,  missionScriptArgs )
	
	// This makes sure the net script is active, waits untull it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
		
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
	
	// KGM: Wait for the first network broadcast before moving on - call this as the last instruction in pre-game
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		RETURN FALSE
	ENDIF
	
	// This script will not be paused if another script calls PAUSE_GAME
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	
	iGameMode = GAME_MODE_FREEMODE

	RETURN TRUE
ENDFUNC

PROC SET_SERVER_STAGE(INT iStage)
	#IF IS_DEBUG_BUILD
	SWITCH iStage
		CASE SERVER_STAGE_INIT
			NET_PRINT("AM_DOORS: SET_SERVER_STAGE = SERVER_STAGE_INIT") NET_NL()
		BREAK
		CASE SERVER_STAGE_RUNNING
			NET_PRINT("AM_DOORS: SET_SERVER_STAGE = SERVER_STAGE_RUNNING") NET_NL()
		BREAK
		CASE SERVER_STAGE_LEAVE
			NET_PRINT("AM_DOORS: SET_SERVER_STAGE = SERVER_STAGE_LEAVE") NET_NL()
		BREAK
		CASE SERVER_STAGE_END
			NET_PRINT("AM_DOORS: SET_SERVER_STAGE = SERVER_STAGE_END") NET_NL()
		BREAK
	ENDSWITCH
	#ENDIF
	serverBD.iServerStage = iStage
ENDPROC

FUNC INT GET_SERVER_STAGE()
	RETURN serverBD.iServerStage
ENDFUNC

PROC SET_CLIENT_STAGE(INT iStage)
	#IF IS_DEBUG_BUILD
	SWITCH iStage
		CASE CLIENT_STAGE_INIT
			NET_PRINT("AM_DOORS: SET_CLIENT_MISSION_STAGE = CLIENT_STAGE_INIT ") NET_NL()
		BREAK
		CASE CLIENT_STAGE_RUNNING
			NET_PRINT("AM_DOORS: SET_CLIENT_MISSION_STAGE = CLIENT_STAGE_RUNNING ") NET_NL()
		BREAK
		CASE CLIENT_STAGE_END
			NET_PRINT("AM_DOORS: SET_CLIENT_MISSION_STAGE = CLIENT_STAGE_END") NET_NL()
		BREAK
	ENDSWITCH
	#ENDIF
	playerBD[PARTICIPANT_ID_TO_INT()].iStage = iStage
ENDPROC

FUNC INT GET_CLIENT_STAGE(INT iParticipant)
	RETURN playerBD[iParticipant].iStage
ENDFUNC

FUNC BOOL IS_PLAYER_DOING_ANY_RACE()
	IF IS_THIS_A_RACE()
		RETURN TRUE
	ENDIF
	IF GET_CURRENT_GAMEMODE() = GAMEMODE_CREATOR
		IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_RACE  
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_RACE_DOORS_STATE()
	IF IS_PLAYER_DOING_ANY_RACE()
		IF NOT bDoorsOpenForRaces
			OPEN_ALL_BARRIERS_FOR_RACE(TRUE)
			bDoorsOpenForRaces = TRUE
		ENDIF
	ELSE
		IF bDoorsOpenForRaces = TRUE
			bDoorsOpenForRaces = FALSE
			CLOSE_ALL_BARRIERS_FOR_RACE()
		ENDIF
	ENDIF
ENDPROC

PROC INIT_ONE_WAY_DOOR_DETAILS()
	INT i
	REPEAT NUM_ONE_WAY_ENTRANCES i
		GET_MP_DOOR_ONE_WAY_DETAILS(i,OneWayDetails[i])
	ENDREPEAT
ENDPROC

PROC SET_MP_DOOR_STATE(INT iDoor,INT iDoorHash, DOOR_STATE_ENUM doorState)
	INT iDoorBSVar = SERVER_DOOR_LOCKED
	IF doorState = DOORSTATE_UNLOCKED
		iDoorBSVar = SERVER_DOOR_UNLOCKED
	ENDIF
//	IF iDoor = AIRPORT_LEFT_SIDE_GATES //2160728
//		EXIT
//	ENDIF
	IF iDoorSetBS[iDoor] = 0
		IF DOOR_SYSTEM_GET_DOOR_PENDING_STATE(iDoorHash) = DOORSTATE_INVALID
			IF DOOR_SYSTEM_GET_DOOR_STATE(iDoorHash) != doorState
				IF NETWORK_HAS_CONTROL_OF_DOOR(iDoorHash)
					SET_BIT(iDoorSetBS[iDoor], iDoorBSVar)
					DOOR_SYSTEM_SET_DOOR_STATE(iDoorHash,doorState,TRUE,TRUE)
					#IF IS_DEBUG_BUILD
					IF doorState = DOORSTATE_UNLOCKED
						PRINTLN("SET_MP_DOOR_STATE: setting one way door #",iDoor," to be unlocked (ACTUAL STATE)")
					ELSE
						PRINTLN("SET_MP_DOOR_STATE: setting one way door #",iDoor," to be locked (ACTUAL STATE)")
					ENDIF
					#ENDIF
					REINIT_NET_TIMER(serverDoorStateTimeout[iDoor],TRUE)
				ELSE
					NETWORK_REQUEST_CONTROL_OF_DOOR(iDoorHash)
				ENDIF
			ENDIF
		ELSE
			IF DOOR_SYSTEM_GET_DOOR_PENDING_STATE(iDoorHash) != doorState
				IF NETWORK_HAS_CONTROL_OF_DOOR(iDoorHash)
					SET_BIT(iDoorSetBS[iDoor], iDoorBSVar)
					DOOR_SYSTEM_SET_DOOR_STATE(iDoorHash,doorState,TRUE,TRUE)
					#IF IS_DEBUG_BUILD
					IF doorState = DOORSTATE_UNLOCKED
						PRINTLN("SET_MP_DOOR_STATE: setting one way door #",iDoor," to be unlocked (PENDING STATE)")
					ELSE
						PRINTLN("SET_MP_DOOR_STATE: setting one way door #",iDoor," to be locked (PENDING STATE)")
					ENDIF
					#ENDIF
					REINIT_NET_TIMER(serverDoorStateTimeout[iDoor],TRUE)
				ELSE
					NETWORK_REQUEST_CONTROL_OF_DOOR(iDoorHash)
				ENDIF
			ENDIF
		ENDIF	
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(serverDoorStateTimeout[iDoor])
		IF HAS_NET_TIMER_EXPIRED(serverDoorStateTimeout[iDoor],1000,TRUE)
			RESET_NET_TIMER(serverDoorStateTimeout[iDoor])
			iDoorSetBS[iDoor] = 0
			PRINTLN("CLIENT_MAINTAIN_ONE_WAY_DOORS: Clearing door state timer for door  #", iDoor)
		ENDIF
	ENDIF
ENDPROC

PROC CLIENT_MAINTAIN_ONE_WAY_DOORS()
	FLOAT fDistance
	VECTOR vPlayerCoords = GET_PLAYER_COORDS(PLAYER_ID())
	fDistance = VDIST(vPlayerCoords,OneWayDetails[iClientSlowOneWayDoorLoop].vArea1)
	
	IF IS_BIT_SET(g_MissionNetDoorOverride.iOneWayDoorOpenRequests,iClientSlowOneWayDoorLoop)
	OR IS_BIT_SET(g_MissionNetDoorOverride.iOneWayDoorClosedRequests,iClientSlowOneWayDoorLoop) 
	OR IS_BIT_SET(g_MissionNetDoorOverride.iOneWayForcedOpenRequests,iClientSlowOneWayDoorLoop)
		IF IS_BIT_SET(g_MissionNetDoorOverride.iOneWayDoorOpenRequests,iClientSlowOneWayDoorLoop)
			#IF IS_DEBUG_BUILD
			IF NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iOneWayDoorOpenRequests,iClientSlowOneWayDoorLoop)
				PRINTLN("CLIENT_MAINTAIN_ONE_WAY_DOORS: (MC OVERRIDE) Player requesting open one way door #", iClientSlowOneWayDoorLoop)
			ENDIF
			#ENDIF
			SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iOneWayDoorOpenRequests,iClientSlowOneWayDoorLoop)
		ENDIF
		IF IS_BIT_SET(g_MissionNetDoorOverride.iOneWayDoorClosedRequests,iClientSlowOneWayDoorLoop)
			#IF IS_DEBUG_BUILD
			IF IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iOneWayDoorOpenRequests,iClientSlowOneWayDoorLoop)
				PRINTLN("CLIENT_MAINTAIN_ONE_WAY_DOORS: (MC OVERRIDE) Player Clearing request for open one way door #", iClientSlowOneWayDoorLoop)
			ENDIF
			#ENDIF
			CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iOneWayDoorOpenRequests,iClientSlowOneWayDoorLoop)
		ENDIF
		IF IS_BIT_SET(g_MissionNetDoorOverride.iOneWayForcedOpenRequests,iClientSlowOneWayDoorLoop)
			#IF IS_DEBUG_BUILD
			IF NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iOneWayForcedOpenRequests,iClientSlowOneWayDoorLoop)
				PRINTLN("CLIENT_MAINTAIN_ONE_WAY_DOORS: (MC OVERRIDE) Player requesting FORCE open one way door #", iClientSlowOneWayDoorLoop)
			ENDIF
			#ENDIF
			SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iOneWayForcedOpenRequests,iClientSlowOneWayDoorLoop)
		ENDIF
	ELSE
		IF fDistance <= 20
			IF IS_POINT_IN_ANGLED_AREA(vPlayerCoords,OneWayDetails[iClientSlowOneWayDoorLoop].vArea1,OneWayDetails[iClientSlowOneWayDoorLoop].vArea2,OneWayDetails[iClientSlowOneWayDoorLoop].fWidth)
				IF NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iOneWayDoorOpenRequests,iClientSlowOneWayDoorLoop)
					SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iOneWayDoorOpenRequests,iClientSlowOneWayDoorLoop)
					PRINTLN("CLIENT_MAINTAIN_ONE_WAY_DOORS: Player requesting open one way door #", iClientSlowOneWayDoorLoop)
				ENDIF
			ENDIF
			
			IF iClientSlowOneWayDoorLoop = AIRPORT_LEFT_SIDE_GATES	//Open gates from outside if got pegasus inside
			OR iClientSlowOneWayDoorLoop = AIRPORT_RIGHT_SIDE_GATES
			OR iClientSlowOneWayDoorLoop = AIRPORT_FAR_RIGHT_SIDE_GATES
				IF g_bHasPegasusVehicleInAirport
				
				#IF FEATURE_HEIST_ISLAND
				OR NOT HEIST_ISLAND_DATA__IS_LSIA_TO_BEACH_PARTY_TRIGGER_BLOCKED()
				#ENDIF
					IF IS_POINT_IN_ANGLED_AREA(vPlayerCoords,OneWayDetails[iClientSlowOneWayDoorLoop].vInArea1,OneWayDetails[iClientSlowOneWayDoorLoop].vInArea2,OneWayDetails[iClientSlowOneWayDoorLoop].fInWidth)
						IF NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iOneWayDoorOpenRequests,iClientSlowOneWayDoorLoop)
							SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iOneWayDoorOpenRequests,iClientSlowOneWayDoorLoop)
							PRINTLN("CLIENT_MAINTAIN_ONE_WAY_DOORS: Player requesting open one way door from outside #", iClientSlowOneWayDoorLoop)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF iClientSlowOneWayDoorLoop = CAR_IMPOUND_GATE_1	//Open gates from outside if player has paid impound fine
			OR iClientSlowOneWayDoorLoop = CAR_IMPOUND_GATE_2
			OR iClientSlowOneWayDoorLoop = CAR_IMPOUND_GATE_3	
				IF CURRENT_SAVED_VEHICLE_SLOT() >= 0
					IF IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_PAID_IMPOUND_FINE)
						IF NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iOneWayDoorOpenRequests,iClientSlowOneWayDoorLoop)
							SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iOneWayDoorOpenRequests,iClientSlowOneWayDoorLoop)
							PRINTLN("CLIENT_MAINTAIN_ONE_WAY_DOORS: Player requesting open impound one way door from outside #", iClientSlowOneWayDoorLoop)
						ENDIF	
					ELSE
						IF IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iOneWayDoorOpenRequests,iClientSlowOneWayDoorLoop)	
						AND NOT IS_POINT_IN_ANGLED_AREA(vPlayerCoords,OneWayDetails[iClientSlowOneWayDoorLoop].vArea1,OneWayDetails[iClientSlowOneWayDoorLoop].vArea2,OneWayDetails[iClientSlowOneWayDoorLoop].fWidth)
							CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iOneWayDoorOpenRequests,iClientSlowOneWayDoorLoop)
							PRINTLN("CLIENT_MAINTAIN_ONE_WAY_DOORS: clearing bit. Player has paid fine and not in inside area #", iClientSlowOneWayDoorLoop)	
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		ELSE
			IF IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iOneWayDoorOpenRequests,iClientSlowOneWayDoorLoop)
				PRINTLN("CLIENT_MAINTAIN_ONE_WAY_DOORS: Player Clearing request for open one way door #", iClientSlowOneWayDoorLoop)
				CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iOneWayDoorOpenRequests,iClientSlowOneWayDoorLoop)
			ENDIF
		ENDIF
	ENDIF
	iClientSlowOneWayDoorLoop++
	IF iClientSlowOneWayDoorLoop >= NUM_ONE_WAY_ENTRANCES
		iClientSlowOneWayDoorLoop = 0
	ENDIF
ENDPROC

PROC SERVER_MAINTAIN_ONE_WAY_DOORS()
	iServerSlowOneWayDoorLoop = 0
	REPEAT NUM_ONE_WAY_ENTRANCES iServerSlowOneWayDoorLoop
		IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(iSlowPlayerLoop),FALSE)
			IF IS_BIT_SET(playerBD[iSlowPlayerLoop].iOneWayDoorOpenRequests,iServerSlowOneWayDoorLoop)
				#IF IS_DEBUG_BUILD
				IF NOT IS_BIT_SET(serverBD.iOneWayDoorOpenRequests[iServerSlowOneWayDoorLoop],iSlowPlayerLoop)
					PRINTLN("SERVER_MAINTAIN_ONE_WAY_DOORS: Server setting door #",iSlowPlayerLoop , " for player #",iServerSlowOneWayDoorLoop)
				ENDIF
				#ENDIF
				SET_BIT(serverBD.iOneWayDoorOpenRequests[iServerSlowOneWayDoorLoop],iSlowPlayerLoop)
			ELSE
				#IF IS_DEBUG_BUILD
				IF IS_BIT_SET(serverBD.iOneWayDoorOpenRequests[iServerSlowOneWayDoorLoop],iSlowPlayerLoop)
					PRINTLN("SERVER_MAINTAIN_ONE_WAY_DOORS: Server clearing door #",iSlowPlayerLoop , " for player #",iServerSlowOneWayDoorLoop)
				ENDIF
				#ENDIF
				CLEAR_BIT(serverBD.iOneWayDoorOpenRequests[iServerSlowOneWayDoorLoop],iSlowPlayerLoop)
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			IF IS_BIT_SET(serverBD.iOneWayDoorOpenRequests[iServerSlowOneWayDoorLoop],iSlowPlayerLoop)
				PRINTLN("SERVER_MAINTAIN_ONE_WAY_DOORS: Server clearing door #",iSlowPlayerLoop , " for player #",iServerSlowOneWayDoorLoop)
			ENDIF
			#ENDIF
			CLEAR_BIT(serverBD.iOneWayDoorForceOpenRequests[iServerSlowOneWayDoorLoop],iSlowPlayerLoop)
			#IF IS_DEBUG_BUILD
			IF IS_BIT_SET(serverBD.iOneWayDoorForceOpenRequests[iServerSlowOneWayDoorLoop],iSlowPlayerLoop)
				PRINTLN("SERVER_MAINTAIN_ONE_WAY_DOORS: Server clearing FORCE open door #",iSlowPlayerLoop , " for player #",iServerSlowOneWayDoorLoop)
			ENDIF
			#ENDIF
			CLEAR_BIT(serverBD.iOneWayDoorForceOpenRequests[iServerSlowOneWayDoorLoop],iSlowPlayerLoop)
		ENDIF
		IF serverBD.iOneWayDoorForceOpenRequests[iServerSlowOneWayDoorLoop] != 0
			IF OneWayDetails[iServerSlowOneWayDoorLoop].iDoorHash[0] != 0
				SET_MP_DOOR_STATE(iServerSlowOneWayDoorLoop,OneWayDetails[iServerSlowOneWayDoorLoop].iDoorHash[0],DOORSTATE_FORCE_OPEN_THIS_FRAME)
			ENDIF
			IF OneWayDetails[iServerSlowOneWayDoorLoop].iDoorHash[1] != 0
				SET_MP_DOOR_STATE(iServerSlowOneWayDoorLoop,OneWayDetails[iServerSlowOneWayDoorLoop].iDoorHash[1],DOORSTATE_FORCE_OPEN_THIS_FRAME)
			ENDIF
		ELIF serverBD.iOneWayDoorOpenRequests[iServerSlowOneWayDoorLoop] != 0
			IF OneWayDetails[iServerSlowOneWayDoorLoop].iDoorHash[0] != 0
				SET_MP_DOOR_STATE(iServerSlowOneWayDoorLoop,OneWayDetails[iServerSlowOneWayDoorLoop].iDoorHash[0],DOORSTATE_UNLOCKED)
			ENDIF
			IF OneWayDetails[iServerSlowOneWayDoorLoop].iDoorHash[1] != 0
				SET_MP_DOOR_STATE(iServerSlowOneWayDoorLoop,OneWayDetails[iServerSlowOneWayDoorLoop].iDoorHash[1],DOORSTATE_UNLOCKED)
			ENDIF
		ELSE
			IF OneWayDetails[iServerSlowOneWayDoorLoop].iDoorHash[0] != 0
				SET_MP_DOOR_STATE(iServerSlowOneWayDoorLoop,OneWayDetails[iServerSlowOneWayDoorLoop].iDoorHash[0],DOORSTATE_LOCKED)
			ENDIF
			IF OneWayDetails[iServerSlowOneWayDoorLoop].iDoorHash[1] != 0
				SET_MP_DOOR_STATE(iServerSlowOneWayDoorLoop,OneWayDetails[iServerSlowOneWayDoorLoop].iDoorHash[1],DOORSTATE_LOCKED)
			ENDIF
		ENDIF
	ENDREPEAT
	
	iSlowPlayerLoop++
	IF iSlowPlayerLoop >= NUM_NETWORK_PLAYERS
		iSlowPlayerLoop = 0
		//iServerSlowOneWayDoorLoop++
		//IF iServerSlowOneWayDoorLoop >= NUM_ONE_WAY_ENTRANCES
		//	iServerSlowOneWayDoorLoop = 0
		//ENDIF
	ENDIF
	
ENDPROC

PROC CLEANUP_SCRIPT()
	REMOVE_ALL_PERMANENT_DOORS(iGameMode)
	MPGlobalsAmbience.bInitAMDoorsLaunch = FALSE
	MPGlobalsAmbience.g_bDoorsInited = FALSE
	NET_PRINT("AM_DOORS: CLEANUP SCRIPT") NET_NL()	
	
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

#IF IS_DEBUG_BUILD
PROC CREATE_WIDGETS()
	START_WIDGET_GROUP("AM_DOORS")
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		CREATE_SCRIPT_PROFILER_WIDGET()
		#ENDIF
		#ENDIF
	STOP_WIDGET_GROUP()
ENDPROC

#ENDIF

FUNC BOOL IS_PLAYER_ALLOWED_IN_AB(PLAYER_INDEX thePlayer)
	IF DOES_PLAYER_OWN_A_HANGER(thePlayer)
		HANGAR_ID eHangarID = GET_PLAYERS_OWNED_HANGAR(thePlayer)
		
		IF eHangarID = ZANCUDO_HANGAR_A2
		OR eHangarID = ZANCUDO_HANGAR_3497
		OR eHangarID = ZANCUDO_HANGAR_3499
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF GB_IS_PLAYER_MEMBER_OF_A_GANG(thePlayer)
		PLAYER_INDEX pBoss = GB_GET_THIS_PLAYER_GANG_BOSS(thePlayer)
		
		IF pBoss != INVALID_PLAYER_INDEX()
			IF DOES_PLAYER_OWN_A_HANGER(pBoss)
				HANGAR_ID eHangarID = GET_PLAYERS_OWNED_HANGAR(pBoss)
				
				IF eHangarID = ZANCUDO_HANGAR_A2
				OR eHangarID = ZANCUDO_HANGAR_3497
				OR eHangarID = ZANCUDO_HANGAR_3499
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


CONST_INT ARMY_BASE_BARRIER_EAST_IN		0
CONST_INT ARMY_BASE_BARRIER_EAST_OUT	1
CONST_INT ARMY_BASE_BARRIER_WEST_IN		2
CONST_INT ARMY_BASE_BARRIER_WEST_OUT	3

STRUCT ARMY_BASE_BARRIERS_DETAILS
	INT iDoorHash
	VECTOR vPosition
	VECTOR vBounds[2]
	FLOAT fWidth
	FLOAT openRate
ENDSTRUCT

FLOAT armyBaseBarriersOpenRatio[4]

PROC GET_BASE_BARRIERS_DETAILS(INT iBaseDoor,ARMY_BASE_BARRIERS_DETAILS &details)
	SWITCH iBaseDoor
		CASE ARMY_BASE_BARRIER_EAST_IN
			details.iDoorHash =	MP_DOOR_MBASE_IN_BARRIER
			details.vPosition = <<-1588.27, 2794.21, 16.85>>
			details.vBounds[0] = <<-1585.348633,2796.764648,15.333453>>
			details.vBounds[1] = <<-1573.873657,2783.339111,20.003242>>
			details.fWidth = 6.75
			details.openRate = 0.55
		BREAK
		CASE ARMY_BASE_BARRIER_EAST_OUT
			details.iDoorHash =	MP_DOOR_MBASE_OUT_BARRIER
			details.vPosition = <<-1589.58, 2793.67, 16.86>>
			details.vBounds[0] = <<-1592.551758,2791.039795,15.326007>>
			details.vBounds[1] = <<-1604.540039,2805.557617,20.174604>>		
			details.fWidth = 6.75
			details.openRate = 0.55
		BREAK
		CASE ARMY_BASE_BARRIER_WEST_IN
			details.iDoorHash =	MP_DOOR_MBASE_W_IN_BARRIER
			details.vPosition = <<-2307.3269, 3390.4834, 29.9780>>
			details.vBounds[0] = <<-2309.541992,3387.239746,29.722107>>
			details.vBounds[1] = <<-2322.485352,3397.324219,35.677418>> 
			details.fWidth = 7
			details.openRate = 0.55
		BREAK
		CASE ARMY_BASE_BARRIER_WEST_OUT
			details.iDoorHash =	MP_DOOR_MBASE_W_OUT_BARRIER
			details.vPosition = <<-2299.7419, 3385.0378,30.0525>>
			details.vBounds[0] = <<-2297.572021,3388.048340,29.794430>>
			details.vBounds[1] = <<-2284.746094,3377.589844,36.370010>> 
			details.fWidth = 6.75
			details.openRate = 0.55
		BREAK
	ENDSWITCH
ENDPROC


PROC MAINTAIN_ARMY_BASE_BARRIERS()
	INT iDoor, iPlayer
	BOOL bOpenDoor
	ARMY_BASE_BARRIERS_DETAILS details
	PLAYER_INDEX thePlayer
	REPEAT ARMY_BASE_BARRIER_WEST_OUT+1 iDoor
		GET_BASE_BARRIERS_DETAILS(iDoor,details)
		IF GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()),details.vPosition) < 150
			PRINTLN("MAINTAIN_ARMY_BASE_BARRIERS: player near door # ",iDoor)
			bOpenDoor = FALSE
			REPEAT NUM_NETWORK_PLAYERS iPlayer
				thePlayer = INT_TO_PLAYERINDEX(iPlayer)
				IF IS_NET_PLAYER_OK(thePlayer)
					IF IS_PLAYER_ALLOWED_IN_AB(thePlayer)
					AND IS_ENTITY_IN_ANGLED_AREA(GET_PLAYER_PED(thePlayer),details.vBounds[0],details.vBounds[1],details.fWidth)
						PRINTLN("MAINTAIN_ARMY_BASE_BARRIERS: player ",GET_PLAYER_NAME(thePlayer)," has hangar in AB should open door # ",iDoor)
						bOpenDoor = TRUE
						iPlayer = NUM_NETWORK_PLAYERS
					ENDIF
				ENDIF
			ENDREPEAT
			IF bOpenDoor
				IF armyBaseBarriersOpenRatio[iDoor] != 1.0
					armyBaseBarriersOpenRatio[iDoor] = armyBaseBarriersOpenRatio[iDoor]+@ details.openRate
					IF armyBaseBarriersOpenRatio[iDoor] > 1.0
						armyBaseBarriersOpenRatio[iDoor] = 1.0
					ENDIF
					
//					IF iBaseBarrierSounds[iSlowLoopDoorIndex] = -1
//						iBaseBarrierSounds[iSlowLoopDoorIndex]= GET_SOUND_ID()
//					ENDIF
					SET_SCRIPT_UPDATE_DOOR_AUDIO(details.iDoorHash,TRUE)
					DOOR_SYSTEM_SET_OPEN_RATIO(details.iDoorHash, armyBaseBarriersOpenRatio[iDoor], FALSE, FALSE)
					DOOR_SYSTEM_SET_DOOR_STATE(details.iDoorHash, DOORSTATE_UNLOCKED, FALSE, TRUE)
					PRINTLN("MAINTAIN_ARMY_BASE_BARRIERS: opening door # ",iDoor)
				ENDIF
//				IF DOOR_SYSTEM_GET_DOOR_PENDING_STATE(details.iDoorHash) = DOORSTATE_INVALID
//					IF DOOR_SYSTEM_GET_DOOR_STATE(details.iDoorHash) != DOORSTATE_UNLOCKED
//						DOOR_SYSTEM_SET_DOOR_STATE(details.iDoorHash,doorState,FALSE
//					ELSE
//						
//					ENDIF
//				ELSE
//					IF DOOR_SYSTEM_GET_DOOR_PENDING_STATE(details.iDoorHash) != DOORSTATE_UNLOCKED
//						IF NETWORK_HAS_CONTROL_OF_DOOR(details.iDoorHash)
//							SET_BIT(iDoorSetBS[iDoor], iDoorBSVar)
//							DOOR_SYSTEM_SET_DOOR_STATE(iDoorHash,DOORSTATE_UNLOCKED,TRUE,TRUE)
//						ENDIF
//					ENDIF
//				ENDIF
			ELSE
				IF armyBaseBarriersOpenRatio[iDoor] != 0.0
					armyBaseBarriersOpenRatio[iDoor] = armyBaseBarriersOpenRatio[iDoor]-@ details.openRate
					IF armyBaseBarriersOpenRatio[iDoor] < 0.0
						armyBaseBarriersOpenRatio[iDoor] = 0.0
					ENDIF
					SET_SCRIPT_UPDATE_DOOR_AUDIO(details.iDoorHash,TRUE)
					DOOR_SYSTEM_SET_OPEN_RATIO(details.iDoorHash, armyBaseBarriersOpenRatio[iDoor], FALSE, FALSE)
					DOOR_SYSTEM_SET_DOOR_STATE(details.iDoorHash, DOORSTATE_LOCKED, FALSE, TRUE)
					PRINTLN("MAINTAIN_ARMY_BASE_BARRIERS: closing door # ",iDoor)
				ENDIF
			ENDIF
		ELSE
			//just close door?
		ENDIF
	ENDREPEAT
//			IF DOOR_SYSTEM_GET_DOOR_PENDING_STATE(iDoorHash) = DOORSTATE_INVALID
//				IF DOOR_SYSTEM_GET_DOOR_STATE(iDoorHash) != doorState
//					IF NETWORK_HAS_CONTROL_OF_DOOR(iDoorHash)
//						SET_BIT(iDoorSetBS[iDoor], iDoorBSVar)
//						DOOR_SYSTEM_SET_DOOR_STATE(iDoorHash,doorState,TRUE,TRUE)
//						#IF IS_DEBUG_BUILD
//						IF doorState = DOORSTATE_UNLOCKED
//							PRINTLN("SET_MP_DOOR_STATE: setting one way door #",iDoor," to be unlocked (ACTUAL STATE)")
//						ELSE
//							PRINTLN("SET_MP_DOOR_STATE: setting one way door #",iDoor," to be locked (ACTUAL STATE)")
//						ENDIF
//						#ENDIF
//						REINIT_NET_TIMER(serverDoorStateTimeout[iDoor],TRUE)
//					ELSE
//						NETWORK_REQUEST_CONTROL_OF_DOOR(iDoorHash)
//					ENDIF
//				ENDIF
//			ELSE
//				IF DOOR_SYSTEM_GET_DOOR_PENDING_STATE(iDoorHash) != doorState
//					IF NETWORK_HAS_CONTROL_OF_DOOR(iDoorHash)
//						SET_BIT(iDoorSetBS[iDoor], iDoorBSVar)
//						DOOR_SYSTEM_SET_DOOR_STATE(iDoorHash,doorState,TRUE,TRUE)
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDREPEAT
//	iDoorHash = MP_DOOR_MBASE_IN_BARRIER
//	IF GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID(),)
//	IF DOOR_SYSTEM_GET_DOOR_PENDING_STATE(iDoorHash) = DOORSTATE_INVALID
//			IF DOOR_SYSTEM_GET_DOOR_STATE(iDoorHash) != doorState
//				IF NETWORK_HAS_CONTROL_OF_DOOR(iDoorHash)
//					SET_BIT(iDoorSetBS[iDoor], iDoorBSVar)
//					DOOR_SYSTEM_SET_DOOR_STATE(iDoorHash,doorState,TRUE,TRUE)
//					#IF IS_DEBUG_BUILD
//					IF doorState = DOORSTATE_UNLOCKED
//						PRINTLN("SET_MP_DOOR_STATE: setting one way door #",iDoor," to be unlocked (ACTUAL STATE)")
//					ELSE
//						PRINTLN("SET_MP_DOOR_STATE: setting one way door #",iDoor," to be locked (ACTUAL STATE)")
//					ENDIF
//					#ENDIF
//					REINIT_NET_TIMER(serverDoorStateTimeout[iDoor],TRUE)
//				ELSE
//					NETWORK_REQUEST_CONTROL_OF_DOOR(iDoorHash)
//				ENDIF
//			ENDIF
//		ELSE
//			IF DOOR_SYSTEM_GET_DOOR_PENDING_STATE(iDoorHash) != doorState
//				IF NETWORK_HAS_CONTROL_OF_DOOR(iDoorHash)
//					SET_BIT(iDoorSetBS[iDoor], iDoorBSVar)
//					DOOR_SYSTEM_SET_DOOR_STATE(iDoorHash,doorState,TRUE,TRUE)
//	SET_MP_DOOR_STATE(iServerSlowOneWayDoorLoop,DOORSTATE_LOCKED)
//	MP_DOOR_MBASE_IN_BARRIER
	
ENDPROC

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                      Main Loop                             ////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
SCRIPT(MP_MISSION_DATA missionScriptArgs) 
	
	// Setup widgets						 
	#IF IS_DEBUG_BUILD
		NET_PRINT_TIME() NET_PRINT_STRINGS("Starting mission ", GET_MP_MISSION_NAME(thisMission)) NET_NL()
	#ENDIF
	
	IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		PRINTLN("AM_DOORS:  cleaning up as SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE() BEFORE PROCESS PREGAME COMPLETE!")
		CLEANUP_SCRIPT()
		
	ENDIF
	
	// Carry out all the initial game starting duties. 
	IF NETWORK_IS_GAME_IN_PROGRESS() 
	
		IF NOT PROCESS_PRE_GAME(missionScriptArgs)
			NET_PRINT("AM_DOORS: Ending script as PROCESS_PRE_GAME failed to reveive an initial network broadcast.") NET_NL()
			CLEANUP_SCRIPT()
		ENDIF
		
		// Setup widgets. Make sure this is done after PROCESS_PRE_GAME()
		#IF IS_DEBUG_BUILD
			CREATE_WIDGETS()
		#ENDIF
	ENDIF
	//for release
	IF thisMission = eAM_DOORS
	
	ENDIF
	iSlowLoopDoorIndex = 0
//	REPEAT 4 iSlowLoopDoorIndex
//		iBaseBarrierSounds[iSlowLoopDoorIndex] = -1
//	ENDREPEAT
//	iSlowLoopDoorIndex = 0
	
	//INT iParticipant
	INIT_ONE_WAY_DOOR_DETAILS()
	
	// Main loop.
	WHILE TRUE

		// One wait to rule them all. This can be the ONLY wait from here on in.....
		MP_LOOP_WAIT_ZERO()
		
		
//		// if in running state
//		#IF IS_DEBUG_BUILD
//		#IF SCRIPT_PROFILER_ACTIVE 
//			IF IS_NET_PLAYER_OK(PLAYER_ID(), TRUE, TRUE)
//				SET_SCRIPT_PROFILER_ACTIVE(TRUE)
//				//SET_SCRIPT_PROFILER_RENDER_BARS(TRUE)
//			ENDIF
//		#ENDIF
//		#ENDIF
		
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		SCRIPT_PROFILER_START_OF_FRAME()
		#ENDIF
		#ENDIF
		// If we have a match end event, bail.
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			CLEANUP_SCRIPT()
		ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("CLEANUP_SCRIPT")
		#ENDIF
		#ENDIF
		
		IF NETWORK_IS_IN_TUTORIAL_SESSION()
			IF NETWORK_GET_PLAYER_TUTORIAL_SESSION_INSTANCE(PLAYER_ID()) = 32
				IF missionScriptArgs.iInstanceId != AM_DOORS_RESERVED_INSTANCES + NATIVE_TO_INT(PLAYER_ID())
					NET_PRINT("AM_DOORS: Ending script as in tutorial missionScriptArgs.iInstanceId != AM_DOORS_RESERVED_INSTANCES + NATIVE_TO_INT(PLAYER_ID())") NET_NL()
					SET_CLIENT_STAGE(CLIENT_STAGE_END)
				ENDIF
			ELSE
				IF missionScriptArgs.iInstanceId != NETWORK_GET_PLAYER_TUTORIAL_SESSION_INSTANCE(PLAYER_ID())
					NET_PRINT("AM_DOORS: Ending script as in tutorial  missionScriptArgs.iInstanceId != NETWORK_GET_PLAYER_TUTORIAL_SESSION_INSTANCE(PLAYER_ID())") NET_NL()
					SET_CLIENT_STAGE(CLIENT_STAGE_END)
				ENDIF
			ENDIF
		ELSE
			IF missionScriptArgs.iInstanceId != -1
				NET_PRINT("AM_DOORS: Ending mission as not in tutorial and iLaunchedInstance != -1") NET_NL()
				SET_CLIENT_STAGE(CLIENT_STAGE_END)
			ENDIF
		ENDIF
		

		REINIT_NET_TIMER(netStoredTime)
		
		#IF IS_DEBUG_BUILD
			IF NOT HAS_NET_TIMER_STARTED(GarageOutputStopSpamTimer)
				g_DB_AllowGarageOutput = FALSE
				START_NET_TIMER(GarageOutputStopSpamTimer)
			ELIF GET_ABS_NET_TIMER_DIFFERENCE(netStoredTime,GarageOutputStopSpamTimer) >= 2000
				g_DB_AllowGarageOutput = TRUE
				RESET_NET_TIMER(GarageOutputStopSpamTimer)
			ENDIF
		#ENDIF

		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("reinit timers")
		#ENDIF
		#ENDIF

		#IF IS_DEBUG_BUILD 
		#IF SCRIPT_PROFILER_ACTIVE 
			OPEN_SCRIPT_PROFILE_MARKER_GROUP("GET_CLIENT_STAGE ")
		#ENDIF
		#ENDIF

		SWITCH GET_CLIENT_STAGE(PARTICIPANT_ID_TO_INT())
			CASE CLIENT_STAGE_INIT
				
				IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_RACE  
					iGameMode = FAKE_GAME_MODE_RACES
				ELSE
					iGameMode = FAKE_GAME_MODE_ALL
				ENDIF
				IF NOT g_bDoorSystemInitialised
				AND GET_CURRENT_GAMEMODE() != GAMEMODE_SP
				AND GET_CURRENT_GAMEMODE() != GAMEMODE_EMPTY
					IF NOT bNoDoorsRegistered
						IF CHECK_ALL_PREVIOUS_NET_DOORS_REMOVED(iSlowLoopDoorIndex, iGameMode)
							NET_PRINT("AM_DOORS: bNoDoorsRegistered = TRUE") NET_NL()
							bNoDoorsRegistered = TRUE
							iSlowLoopDoorIndex = 0
						ENDIF
					ELSE
						IF NOT bDoorsExist
							IF NOT bDoorsAdded
							
								IF INIT_NET_DOORS(iSlowLoopDoorIndex,iGameMode)
									iSlowLoopDoorIndex = 0
									bDoorsAdded = TRUE
								ELSE
									PRINTLN("AM_DOORS: Registered doors Waiting for INIT_NET_DOORS")
								ENDIF
							ELSE
								IF HAVE_ALL_NET_DOORS_BEEN_REGISTERED(iSlowLoopDoorIndex, iGameMode)
									iSlowLoopDoorIndex = 0
									bDoorsExist = TRUE	
								ELSE	
									PRINTLN("AM_DOORS: Registered doors Waiting for HAVE_ALL_NET_DOORS_BEEN_REGISTERED")
								ENDIF
							ENDIF
						ELSE
							IF GET_SERVER_STAGE() >= SERVER_STAGE_RUNNING
								SET_CLIENT_STAGE(CLIENT_STAGE_RUNNING) 
							ELSE
								PRINTLN("AM_DOORS: Registered doors Waiting for GET_SERVER_STAGE() >= SERVER_STAGE_RUNNING")
							ENDIF
						ENDIF
					ENDIF
				ELSE
					PRINTLN("AM_DOORS: waiting for NOT g_bDoorSystemInitialised. Current Game Mode: ",GET_CURRENT_GAMEMODE() )
				ENDIF
				
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE 
					ADD_SCRIPT_PROFILE_MARKER("CLIENT_STAGE_INIT")
				#ENDIF
				#ENDIF
				
			BREAK
			CASE CLIENT_STAGE_RUNNING
				
				MAINTAIN_RACE_DOORS_STATE()
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_RACE_DOORS_STATE")
				#ENDIF
				#ENDIF
				
				MPGlobalsAmbience.g_bDoorsInited = TRUE
				
				MAINTAIN_ARMY_BASE_BARRIERS()
				
				CLIENT_MAINTAIN_ONE_WAY_DOORS()
//				MANAGE_CLIENT_NON_GARAGE_DOORS()
//				#IF IS_DEBUG_BUILD
//				#IF SCRIPT_PROFILER_ACTIVE
//				ADD_SCRIPT_PROFILE_MARKER("MANAGE_CLIENT_NON_GARAGE_DOORS()")
//				#ENDIF
//				#ENDIF
				
			BREAK

			CASE CLIENT_STAGE_END
				CLEANUP_SCRIPT()
				
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("CLIENT_STAGE_END")
				#ENDIF
				#ENDIF
				
			BREAK
		
		ENDSWITCH
		
		#IF IS_DEBUG_BUILD 
		#IF SCRIPT_PROFILER_ACTIVE 
			CLOSE_SCRIPT_PROFILE_MARKER_GROUP()
		#ENDIF
		#ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("client processing")
		#ENDIF
		#ENDIF
		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()

			SWITCH GET_SERVER_STAGE()
				
				// For CnC group missions, we may want to wait for all players to have launched the script.
				CASE SERVER_STAGE_INIT	
					SET_SERVER_STAGE(SERVER_STAGE_RUNNING)
				BREAK
				
				CASE SERVER_STAGE_RUNNING
					IF GET_CLIENT_STAGE(PARTICIPANT_ID_TO_INT()) = CLIENT_STAGE_RUNNING
						SERVER_MAINTAIN_ONE_WAY_DOORS()
					ENDIF
				BREAK
				
				CASE SERVER_STAGE_LEAVE
				
				BREAK
				
				CASE SERVER_STAGE_END
				
				BREAK
			ENDSWITCH
		ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("host processing")
		#ENDIF
		#ENDIF
		
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		SCRIPT_PROFILER_END_OF_FRAME()
		#ENDIF
		#ENDIF
	ENDWHILE
	// End of Mission
ENDSCRIPT
