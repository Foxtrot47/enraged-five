USING "globals.sch"
USING "net_include.sch"
USING "net_simple_interior.sch"
USING "net_MP_TV.sch"

INT iNumberOfSeats = 7

STRUCT ServerBroadcastData
	NETWORK_INDEX objSeats[8]
	MP_TV_SERVER_DATA_STRUCT MPTVServer
ENDSTRUCT
ServerBroadcastData serverBD

STRUCT PlayerBroadcastData
	INT iBoardroomSeatID = -1
	MP_TV_CLIENT_DATA_STRUCT MPTVClient
ENDSTRUCT
PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]

MP_TV_LOCAL_DATA_STRUCT sMPTVLocal

ENUM BOARDROOM_SEATING_VAR
	BOARDROOM_SEATING_VAR_A,
	BOARDROOM_SEATING_VAR_B,
	BOARDROOM_SEATING_VAR_C
ENDENUM

ENUM BOARDROOM_SEATING_DIRECTION
	BOARDROOM_SEATING_DIRECTION_CENTER,
	BOARDROOM_SEATING_DIRECTION_LEFT,
	BOARDROOM_SEATING_DIRECTION_RIGHT
ENDENUM

ENUM BOARDROOM_SEATING_STATE
	BOARDROOM_SEATING_STATE_INIT,
	BOARDROOM_SEATING_STATE_PROMPT,
	BOARDROOM_SEATING_STATE_WALK,
	BOARDROOM_SEATING_STATE_WALKING_TO_POSITION,
	BOARDROOM_SEATING_STATE_WAIT_TO_SIT,
	BOARDROOM_SEATING_STATE_IDLE,
	BOARDROOM_SEATING_STATE_TURNING,
	BOARDROOM_SEATING_STATE_EXITING
ENDENUM

CONST_INT BOARDROOM_SEATING_BS_TURNED_LEFT			0
CONST_INT BOARDROOM_SEATING_BS_TURNED_RIGHT			1
CONST_INT BOARDROOM_SEATING_BS_PLAYING_IDLE			2
CONST_INT BOARDROOM_SEATING_BS_USING_TV_CONTROLS	3

STRUCT BOARDROOM_SEATING_DATA
	BOARDROOM_SEATING_STATE eCurrentState = BOARDROOM_SEATING_STATE_INIT
	
	BOARDROOM_SEATING_VAR eBoardroomSeatingVariation = BOARDROOM_SEATING_VAR_A
	
	FLOAT fIdealHeading
	
	INT iBoardroomSeatBS
	INT iBoardroomSeatID = -1
	INT iBoardroomSeatSyncSceneID
	INT iBoardroomSeatContextIntention = NEW_CONTEXT_INTENTION
ENDSTRUCT
BOARDROOM_SEATING_DATA sBoardroomSeatingData

PROC SET_BOARDROOM_SEATING_STATE(BOARDROOM_SEATING_STATE eNewState)
	sBoardroomSeatingData.eCurrentState = eNewState
	
	PRINTLN("[AM_MP_BOARDROOM_SEATING] - SET_BOARDROOM_SEATING_STATE: ", ENUM_TO_INT(eNewState))
ENDPROC

FUNC STRING GET_BOARDROOM_SEATING_ANIM_DICT(INT iIndex, BOARDROOM_SEATING_VAR eVariation = BOARDROOM_SEATING_VAR_A, BOARDROOM_SEATING_DIRECTION eDirection = BOARDROOM_SEATING_DIRECTION_CENTER)
	IF IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
	OR IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
	OR IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())
		IF IS_PLAYER_FEMALE()
			SWITCH eVariation
				CASE BOARDROOM_SEATING_VAR_A
					SWITCH eDirection
						CASE BOARDROOM_SEATING_DIRECTION_CENTER	RETURN "anim@amb@clubhouse@boardroom@crew@female@var_a@base@"
						CASE BOARDROOM_SEATING_DIRECTION_LEFT	RETURN "anim@amb@clubhouse@boardroom@crew@female@var_a@base_l@"
						CASE BOARDROOM_SEATING_DIRECTION_RIGHT	RETURN "anim@amb@clubhouse@boardroom@crew@female@var_a@base_r@"
					ENDSWITCH
				BREAK
				
				CASE BOARDROOM_SEATING_VAR_B
					SWITCH eDirection
						CASE BOARDROOM_SEATING_DIRECTION_CENTER	RETURN "anim@amb@clubhouse@boardroom@crew@female@var_b@base@"
						CASE BOARDROOM_SEATING_DIRECTION_LEFT	RETURN "anim@amb@clubhouse@boardroom@crew@female@var_b@base_l@"
						CASE BOARDROOM_SEATING_DIRECTION_RIGHT	RETURN "anim@amb@clubhouse@boardroom@crew@female@var_b@base_r@"
					ENDSWITCH
				BREAK
				
				CASE BOARDROOM_SEATING_VAR_C
					SWITCH eDirection
						CASE BOARDROOM_SEATING_DIRECTION_CENTER	RETURN "anim@amb@clubhouse@boardroom@crew@female@var_c@base@"
						CASE BOARDROOM_SEATING_DIRECTION_LEFT	RETURN "anim@amb@clubhouse@boardroom@crew@female@var_c@base_l@"
						CASE BOARDROOM_SEATING_DIRECTION_RIGHT	RETURN "anim@amb@clubhouse@boardroom@crew@female@var_c@base_r@"
					ENDSWITCH
				BREAK
			ENDSWITCH
		ELSE
			SWITCH eVariation
				CASE BOARDROOM_SEATING_VAR_A
					SWITCH eDirection
						CASE BOARDROOM_SEATING_DIRECTION_CENTER	RETURN "anim@amb@clubhouse@boardroom@crew@male@var_a@base@"
						CASE BOARDROOM_SEATING_DIRECTION_LEFT	RETURN "anim@amb@clubhouse@boardroom@crew@male@var_a@base_l@"
						CASE BOARDROOM_SEATING_DIRECTION_RIGHT	RETURN "anim@amb@clubhouse@boardroom@crew@male@var_a@base_r@"
					ENDSWITCH
				BREAK
				
				CASE BOARDROOM_SEATING_VAR_B
					SWITCH eDirection
						CASE BOARDROOM_SEATING_DIRECTION_CENTER	RETURN "anim@amb@clubhouse@boardroom@crew@male@var_b@base@"
						CASE BOARDROOM_SEATING_DIRECTION_LEFT	RETURN "anim@amb@clubhouse@boardroom@crew@male@var_b@base_l@"
						CASE BOARDROOM_SEATING_DIRECTION_RIGHT	RETURN "anim@amb@clubhouse@boardroom@crew@male@var_b@base_r@"
					ENDSWITCH
				BREAK
				
				CASE BOARDROOM_SEATING_VAR_C
					SWITCH eDirection
						CASE BOARDROOM_SEATING_DIRECTION_CENTER	RETURN "anim@amb@clubhouse@boardroom@crew@male@var_c@base@"
						CASE BOARDROOM_SEATING_DIRECTION_LEFT	RETURN "anim@amb@clubhouse@boardroom@crew@male@var_c@base_l@"
						CASE BOARDROOM_SEATING_DIRECTION_RIGHT	RETURN "anim@amb@clubhouse@boardroom@crew@male@var_c@base_r@"
					ENDSWITCH
				BREAK
			ENDSWITCH
		ENDIF
	ELSE
		IF IS_PLAYER_FEMALE()
			SWITCH iIndex
				CASE 0
				CASE 1
				CASE 2
				CASE 3
					IF NOT IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())
						RETURN "anim@amb@office@boardroom@boss@female@"
					ENDIF
					FALLTHRU
				CASE 4
				CASE 5
				CASE 6
				CASE 7
					SWITCH eVariation
						CASE BOARDROOM_SEATING_VAR_A
							SWITCH eDirection
								CASE BOARDROOM_SEATING_DIRECTION_CENTER	RETURN "anim@amb@office@boardroom@crew@female@var_a@base@"
								CASE BOARDROOM_SEATING_DIRECTION_LEFT	RETURN "anim@amb@office@boardroom@crew@female@var_a@base_l@"
								CASE BOARDROOM_SEATING_DIRECTION_RIGHT	RETURN "anim@amb@office@boardroom@crew@female@var_a@base_r@"
							ENDSWITCH
						BREAK
						
						CASE BOARDROOM_SEATING_VAR_B
							SWITCH eDirection
								CASE BOARDROOM_SEATING_DIRECTION_CENTER	RETURN "anim@amb@office@boardroom@crew@female@var_b@base@"
								CASE BOARDROOM_SEATING_DIRECTION_LEFT	RETURN "anim@amb@office@boardroom@crew@female@var_b@base_l@"
								CASE BOARDROOM_SEATING_DIRECTION_RIGHT	RETURN "anim@amb@office@boardroom@crew@female@var_b@base_r@"
							ENDSWITCH
						BREAK
						
						CASE BOARDROOM_SEATING_VAR_C
							SWITCH eDirection
								CASE BOARDROOM_SEATING_DIRECTION_CENTER	RETURN "anim@amb@office@boardroom@crew@female@var_c@base@"
								CASE BOARDROOM_SEATING_DIRECTION_LEFT	RETURN "anim@amb@office@boardroom@crew@female@var_c@base_l@"
								CASE BOARDROOM_SEATING_DIRECTION_RIGHT	RETURN "anim@amb@office@boardroom@crew@female@var_c@base_r@"
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK				
			ENDSWITCH
		ELSE
			SWITCH iIndex
				CASE 0
				CASE 1
				CASE 2
				CASE 3
					IF NOT IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())
						RETURN "anim@amb@office@boardroom@boss@male@"
					ENDIF
					FALLTHRU
				CASE 4
				CASE 5
				CASE 6
				CASE 7
					SWITCH eVariation
						CASE BOARDROOM_SEATING_VAR_A
							SWITCH eDirection
								CASE BOARDROOM_SEATING_DIRECTION_CENTER	RETURN "anim@amb@office@boardroom@crew@male@var_a@base@"
								CASE BOARDROOM_SEATING_DIRECTION_LEFT	RETURN "anim@amb@office@boardroom@crew@male@var_a@base_l@"
								CASE BOARDROOM_SEATING_DIRECTION_RIGHT	RETURN "anim@amb@office@boardroom@crew@male@var_a@base_r@"
							ENDSWITCH
						BREAK
						
						CASE BOARDROOM_SEATING_VAR_B
							SWITCH eDirection
								CASE BOARDROOM_SEATING_DIRECTION_CENTER	RETURN "anim@amb@office@boardroom@crew@male@var_b@base@"
								CASE BOARDROOM_SEATING_DIRECTION_LEFT	RETURN "anim@amb@office@boardroom@crew@male@var_b@base_l@"
								CASE BOARDROOM_SEATING_DIRECTION_RIGHT	RETURN "anim@amb@office@boardroom@crew@male@var_b@base_r@"
							ENDSWITCH
						BREAK
						
						CASE BOARDROOM_SEATING_VAR_C
							SWITCH eDirection
								CASE BOARDROOM_SEATING_DIRECTION_CENTER	RETURN "anim@amb@office@boardroom@crew@male@var_c@base@"
								CASE BOARDROOM_SEATING_DIRECTION_LEFT	RETURN "anim@amb@office@boardroom@crew@male@var_c@base_l@"
								CASE BOARDROOM_SEATING_DIRECTION_RIGHT	RETURN "anim@amb@office@boardroom@crew@male@var_c@base_r@"
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK				
			ENDSWITCH
		ENDIF
	ENDIF
	
	RETURN ""
ENDFUNC

FUNC VECTOR GET_BOARDROOM_SEATING_SCENE_POSITION(INT iIndex, BOOL bEnter = FALSE)
	VECTOR vScenePosition
	
	IF IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
		SWITCH iIndex
			CASE 0	vScenePosition = <<2703.905, -370.476, -55.1649>>	BREAK
			CASE 1	vScenePosition = <<2705.002, -370.476, -55.1649>>	BREAK
			CASE 2	vScenePosition = <<2706.050, -369.538, -55.1649>>	BREAK
			CASE 3	vScenePosition = <<2705.002, -368.526, -55.1649>>	BREAK
			CASE 4	vScenePosition = <<2703.905, -368.526, -55.1649>>	BREAK
		ENDSWITCH
	ELIF IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
		SWITCH iIndex
			CASE 0	vScenePosition = <<1562.6500, 390.5500, -53.6800>>	BREAK
			CASE 1	vScenePosition = <<1562.6500, 389.4000, -53.6800>>	BREAK
			CASE 2	vScenePosition = <<1562.6500, 387.8866, -53.6800>>	BREAK
			CASE 3	vScenePosition = <<1564.1000, 389.9674, -53.6800>>	BREAK
			CASE 4	vScenePosition = <<1564.1000, 387.3691, -53.6800>>	BREAK
		ENDSWITCH
	ELIF IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())
		SWITCH iIndex
			CASE 0	vScenePosition = <<-1349.4037, 142.0680, -95.5057>>		BREAK
			CASE 1	vScenePosition = <<-1350.6766, 142.6623, -95.5057>>		BREAK
			CASE 2	vScenePosition = <<-1351.8383, 142.3551, -95.5057>>		BREAK
			CASE 3	vScenePosition = <<-1353.0413, 142.0094, -95.5057>>		BREAK
			CASE 4	vScenePosition = <<-1350.2150, 140.8773, -95.5057>>		BREAK
			CASE 5	vScenePosition = <<-1351.3978, 140.5723, -95.5057>>		BREAK
			CASE 6	vScenePosition = <<-1352.6111, 140.2685, -95.5057>>		BREAK
			CASE 7	vScenePosition = <<-1353.8767, 140.8985, -95.5057>>		BREAK
		ENDSWITCH
	#IF FEATURE_FIXER
	ELIF IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())				
		SWITCH iIndex
			CASE 0  vScenePosition = << -6.78359, -1.02747, 4.397 >>	BREAK	// Original world coords in Hawick location <<386.1686, -54.0011, 106.7600>>
			CASE 1  vScenePosition = << -7.87022, -0.253249, 4.397 >>	BREAK	// Original world coords in Hawick location <<387.2678, -53.2448, 106.7600>>
			CASE 2  vScenePosition = << -7.88755, 1.29342, 4.397 >>		BREAK	// Original world coords in Hawick location <<388.7271, -53.7575, 106.7600>>
			CASE 3  vScenePosition = << -7.91141, 2.79893, 4.397 >>		BREAK	// Original world coords in Hawick location	<<390.15, -54.25, 106.7600>>				
			CASE 4  vScenePosition = << -6.79927, 3.54576, 4.397 >>		BREAK	// Original world coords in Hawick location <<390.4714, -55.5505, 106.7600>>
			CASE 5  vScenePosition = << -5.66434, 2.7327, 4.397 >>		BREAK	// Original world coords in Hawick location <<389.3192, -56.3389, 106.7600>>
			CASE 6  vScenePosition = << -5.62514, 1.24466, 4.397 >>		BREAK	// Original world coords in Hawick location <<387.9075, -55.8668, 106.7600>>
			CASE 7  vScenePosition = << -5.62118, -0.163204, 4.397 >>	BREAK	// Original world coords in Hawick location <<386.5832, -55.3890, 106.7600>>
		ENDSWITCH	
		
		vScenePosition = TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS_GIVEN_ID(vScenePosition, GET_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID()))		
	#ENDIF
	ELSE
		SWITCH iIndex
			CASE 0	vScenePosition = <<970.3333, 55.5550, 115.7754>>	BREAK
			CASE 1	vScenePosition = <<969.3386, 56.1765, 115.7754>>	BREAK
			CASE 2	vScenePosition = <<968.3069, 56.8211, 115.7754>>	BREAK
			CASE 3	vScenePosition = <<967.8962, 58.3022, 115.7754>>	BREAK
			CASE 4	vScenePosition = <<969.4728, 58.6255, 115.7754>>	BREAK
			CASE 5	vScenePosition = <<970.4819, 57.9950, 115.7754>>	BREAK
			CASE 6	vScenePosition = <<971.4991, 57.3593, 115.7754>>	BREAK
		ENDSWITCH
		
		IF NOT bEnter
			SWITCH iIndex
				CASE 0
				CASE 1
				CASE 2
					vScenePosition.x -= 0.0837
					vScenePosition.y -= 0.1338
				BREAK
				
				CASE 3
					vScenePosition.x -= 0.0630
					vScenePosition.y += 0.0407
				BREAK
				
				CASE 4
				CASE 5
				CASE 6
					vScenePosition.x += 0.0837
					vScenePosition.y += 0.1338
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
	
	RETURN vScenePosition
ENDFUNC

FUNC VECTOR GET_BOARDROOM_SEATING_SCENE_ROTATION(INT iIndex)
	
	IF IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
		SWITCH iIndex
			CASE 0	RETURN <<0.0, 0.0, 180.0>>
			CASE 1	RETURN <<0.0, 0.0, 180.0>>
			CASE 2	RETURN <<0.0, 0.0, -90.0>>
			CASE 3	RETURN <<0.0, 0.0, 0.0>>
			CASE 4	RETURN <<0.0, 0.0, 0.0>>
		ENDSWITCH
	ELIF IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
		SWITCH iIndex
			CASE 0	RETURN <<-0.0000, -0.0000, 90.0000>>
			CASE 1	RETURN <<-0.0000, -0.0000, 90.0000>>
			CASE 2	RETURN <<-0.0000, -0.0000, 90.0000>>
			CASE 3	RETURN <<-0.0000, -0.0000, -90.0000>>
			CASE 4	RETURN <<-0.0000, -0.0000, -90.0000>>
		ENDSWITCH
	ELIF IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())
		SWITCH iIndex
			CASE 0	RETURN <<-0.0000, -0.0000, -75.0000>>
			CASE 1	RETURN <<0.0000, 0.0000, 15.0000>>
			CASE 2	RETURN <<0.0000, 0.0000, 15.0000>>
			CASE 3	RETURN <<0.0000, 0.0000, 15.0000>>
			CASE 4	RETURN <<0.0000, -0.0000, -165.0000>>
			CASE 5	RETURN <<0.0000, -0.0000, -165.0000>>
			CASE 6	RETURN <<0.0000, -0.0000, -165.0000>>
			CASE 7	RETURN <<0.0000, -0.0000, 105.0000>>
		ENDSWITCH
	#IF FEATURE_FIXER
	ELIF IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())		
		FLOAT fFixerRotation
	
		SWITCH iIndex		
			CASE 0	fFixerRotation = 180.0	BREAK	// Original world coords at Hawick location <<-0.0000, -0.0000, 70.0>>
			CASE 1	fFixerRotation = 90.0	BREAK	// Original world coords at Hawick location <<-0.0000, -0.0000, -20.0>>
			CASE 2	fFixerRotation = 90.0	BREAK	// Original world coords at Hawick location <<-0.0000, -0.0000, -20.0>>
			CASE 3	fFixerRotation = 90.0	BREAK	// Original world coords at Hawick location <<-0.0000, -0.0000, -20.0>>
			CASE 4	fFixerRotation = 0.0	BREAK	// Original world coords at Hawick location <<-0.0000, -0.0000, -110.0>>
			CASE 5	fFixerRotation = 270.0	BREAK	// Original world coords at Hawick location <<-0.0000, -0.0000, 160.0>>	
			CASE 6	fFixerRotation = 270.0	BREAK	// Original world coords at Hawick location <<-0.0000, -0.0000, 160.0>>	
			CASE 7	fFixerRotation = 270.0	BREAK	// Original world coords at Hawick location <<-0.0000, -0.0000, 160.0>>															
		ENDSWITCH
		
		fFixerRotation = TRANSFORM_SIMPLE_INTERIOR_HEADING_TO_WORLD_HEADING_GIVEN_ID(fFixerRotation, GET_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID()))
		RETURN <<0.0, 0.0, fFixerRotation>>		
	#ENDIF
	ELSE
		SWITCH iIndex
			CASE 0	RETURN <<0.0, 0.0, 148.0>>
			CASE 1	RETURN <<0.0, 0.0, 148.0>>
			CASE 2	RETURN <<0.0, 0.0, 148.0>>
			CASE 3	RETURN <<0.0, 0.0, 58.0>>
			CASE 4	RETURN <<0.0, 0.0, -32.0>>
			CASE 5	RETURN <<0.0, 0.0, -32.0>>
			CASE 6	RETURN <<0.0, 0.0, -32.0>>
		ENDSWITCH
	ENDIF
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

PROC CLEANUP_BOARDROOM_SEATING(BOOL bDeleteEntity = FALSE)
	IF sBoardroomSeatingData.eCurrentState > BOARDROOM_SEATING_STATE_WALK
		NETWORK_STOP_SYNCHRONISED_SCENE(sBoardroomSeatingData.iBoardroomSeatSyncSceneID)
		
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
		ENDIF
		
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.objSeats[sBoardroomSeatingData.iBoardroomSeatID])
		AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.objSeats[sBoardroomSeatingData.iBoardroomSeatID])
			VECTOR vBoardroomSeatPosition, vBoardroomSeatRotation
			vBoardroomSeatPosition = GET_ANIM_INITIAL_OFFSET_POSITION(GET_BOARDROOM_SEATING_ANIM_DICT(sBoardroomSeatingData.iBoardroomSeatID, sBoardroomSeatingData.eBoardroomSeatingVariation), "enter_chair", GET_BOARDROOM_SEATING_SCENE_POSITION(sBoardroomSeatingData.iBoardroomSeatID, TRUE), GET_BOARDROOM_SEATING_SCENE_ROTATION(sBoardroomSeatingData.iBoardroomSeatID))
			vBoardroomSeatRotation = GET_ANIM_INITIAL_OFFSET_ROTATION(GET_BOARDROOM_SEATING_ANIM_DICT(sBoardroomSeatingData.iBoardroomSeatID, sBoardroomSeatingData.eBoardroomSeatingVariation), "enter_chair", GET_BOARDROOM_SEATING_SCENE_POSITION(sBoardroomSeatingData.iBoardroomSeatID, TRUE), GET_BOARDROOM_SEATING_SCENE_ROTATION(sBoardroomSeatingData.iBoardroomSeatID))
			
			SET_ENTITY_COORDS_NO_OFFSET(NET_TO_OBJ(serverBD.objSeats[sBoardroomSeatingData.iBoardroomSeatID]), vBoardroomSeatPosition)
			SET_ENTITY_ROTATION(NET_TO_OBJ(serverBD.objSeats[sBoardroomSeatingData.iBoardroomSeatID]), vBoardroomSeatRotation)
			FREEZE_ENTITY_POSITION(NET_TO_OBJ(serverBD.objSeats[sBoardroomSeatingData.iBoardroomSeatID]), TRUE)
		ENDIF
	ENDIF
	
	IF sBoardroomSeatingData.iBoardroomSeatContextIntention != NEW_CONTEXT_INTENTION
		RELEASE_CONTEXT_INTENTION(sBoardroomSeatingData.iBoardroomSeatContextIntention)
		sBoardroomSeatingData.iBoardroomSeatContextIntention = NEW_CONTEXT_INTENTION
	ENDIF
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPJAC_NO_SIT")
		CLEAR_HELP()
	ENDIF
	
	IF bDeleteEntity
	AND sBoardroomSeatingData.eCurrentState > BOARDROOM_SEATING_STATE_WALK
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.objSeats[sBoardroomSeatingData.iBoardroomSeatID])
		AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.objSeats[sBoardroomSeatingData.iBoardroomSeatID])
			DELETE_NET_ID(serverBD.objSeats[sBoardroomSeatingData.iBoardroomSeatID])
		ENDIF
	ENDIF
	
	sBoardroomSeatingData.iBoardroomSeatBS = 0
	
	IF IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
	OR IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
	OR IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())	
		REMOVE_ANIM_DICT("anim@amb@clubhouse@boardroom@crew@female@var_a@base@")
		REMOVE_ANIM_DICT("anim@amb@clubhouse@boardroom@crew@female@var_a@base_l@")
		REMOVE_ANIM_DICT("anim@amb@clubhouse@boardroom@crew@female@var_a@base_r@")
		
		REMOVE_ANIM_DICT("anim@amb@clubhouse@boardroom@crew@female@var_b@base@")
		REMOVE_ANIM_DICT("anim@amb@clubhouse@boardroom@crew@female@var_b@base_l@")
		REMOVE_ANIM_DICT("anim@amb@clubhouse@boardroom@crew@female@var_b@base_r@")
		
		REMOVE_ANIM_DICT("anim@amb@clubhouse@boardroom@crew@female@var_c@base@")
		REMOVE_ANIM_DICT("anim@amb@clubhouse@boardroom@crew@female@var_c@base_l@")
		REMOVE_ANIM_DICT("anim@amb@clubhouse@boardroom@crew@female@var_c@base_r@")
		
		REMOVE_ANIM_DICT("anim@amb@clubhouse@boardroom@crew@male@var_a@base@")
		REMOVE_ANIM_DICT("anim@amb@clubhouse@boardroom@crew@male@var_a@base_l@")
		REMOVE_ANIM_DICT("anim@amb@clubhouse@boardroom@crew@male@var_a@base_r@")
		
		REMOVE_ANIM_DICT("anim@amb@clubhouse@boardroom@crew@male@var_b@base@")
		REMOVE_ANIM_DICT("anim@amb@clubhouse@boardroom@crew@male@var_b@base_l@")
		REMOVE_ANIM_DICT("anim@amb@clubhouse@boardroom@crew@male@var_b@base_r@")
		
		REMOVE_ANIM_DICT("anim@amb@clubhouse@boardroom@crew@male@var_c@base@")
		REMOVE_ANIM_DICT("anim@amb@clubhouse@boardroom@crew@male@var_c@base_l@")
		REMOVE_ANIM_DICT("anim@amb@clubhouse@boardroom@crew@male@var_c@base_r@")
	ELSE
		REMOVE_ANIM_DICT("anim@amb@office@boardroom@crew@female@var_a@base@")
		REMOVE_ANIM_DICT("anim@amb@office@boardroom@crew@female@var_a@base_l@")
		REMOVE_ANIM_DICT("anim@amb@office@boardroom@crew@female@var_a@base_r@")
		
		REMOVE_ANIM_DICT("anim@amb@office@boardroom@crew@female@var_b@base@")
		REMOVE_ANIM_DICT("anim@amb@office@boardroom@crew@female@var_b@base_l@")
		REMOVE_ANIM_DICT("anim@amb@office@boardroom@crew@female@var_b@base_r@")
		
		REMOVE_ANIM_DICT("anim@amb@office@boardroom@crew@female@var_c@base@")
		REMOVE_ANIM_DICT("anim@amb@office@boardroom@crew@female@var_c@base_l@")
		REMOVE_ANIM_DICT("anim@amb@office@boardroom@crew@female@var_c@base_r@")
		
		REMOVE_ANIM_DICT("anim@amb@office@boardroom@crew@male@var_a@base@")
		REMOVE_ANIM_DICT("anim@amb@office@boardroom@crew@male@var_a@base_l@")
		REMOVE_ANIM_DICT("anim@amb@office@boardroom@crew@male@var_a@base_r@")
		
		REMOVE_ANIM_DICT("anim@amb@office@boardroom@crew@male@var_b@base@")
		REMOVE_ANIM_DICT("anim@amb@office@boardroom@crew@male@var_b@base_l@")
		REMOVE_ANIM_DICT("anim@amb@office@boardroom@crew@male@var_b@base_r@")
		
		REMOVE_ANIM_DICT("anim@amb@office@boardroom@crew@male@var_c@base@")
		REMOVE_ANIM_DICT("anim@amb@office@boardroom@crew@male@var_c@base_l@")
		REMOVE_ANIM_DICT("anim@amb@office@boardroom@crew@male@var_c@base_r@")
		
		REMOVE_ANIM_DICT("anim@amb@office@boardroom@boss@female@")
		
		REMOVE_ANIM_DICT("anim@amb@office@boardroom@boss@male@")
	ENDIF
	
	IF sBoardroomSeatingData.eCurrentState > BOARDROOM_SEATING_STATE_PROMPT
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_UseKinematicModeWhenStationary, FALSE)
		ENDIF
		
		CLEAR_BIT(g_SimpleInteriorData.iSeventhBS, BS7_SIMPLE_INTERIOR_USING_SEATED_ACTIVITY)
	ENDIF
	
	sBoardroomSeatingData.iBoardroomSeatID = -1
	playerBD[NATIVE_TO_INT(PLAYER_ID())].iBoardroomSeatID = -1
	
	SET_BOARDROOM_SEATING_STATE(BOARDROOM_SEATING_STATE_PROMPT)
ENDPROC

FUNC BOOL CAN_USE_TV_FROM_BOARDROOM_TABLE()
	IF IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SCRIPT_CLEANUP()
	PRINTLN("[AM_MP_BOARDROOM_SEATING] - SCRIPT_CLEANUP")
	
	CLEANUP_BOARDROOM_SEATING()
	
	IF CAN_USE_TV_FROM_BOARDROOM_TABLE()
		CLEANUP_MP_TV(serverBD.MPTVServer, playerBD[NATIVE_TO_INT(PLAYER_ID())].MPTVClient, sMPTVLocal, InteriorPropStruct)
	ENDIF
	
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC


FUNC BOOL SHOULD_TERMINATE_SCRIPT()
	IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		PRINTLN("[AM_MP_BOARDROOM_SEATING] SHOULD_TERMINATE_SCRIPT - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE")
		
		RETURN TRUE
	ENDIF

	IF IS_SCREEN_FADED_OUT()
	AND NOT IS_LOCAL_PLAYER_VIEWING_CCTV()
	AND NOT IS_BIT_SET(g_SimpleInteriorData.iSeventhBS, BS7_SIMPLE_INTERIOR_RENOVATION_TRIGGERED)
	AND (playerBD[NATIVE_TO_INT(PLAYER_ID())].MPTVClient.eStage != MP_TV_CLIENT_STAGE_ACTIVATED
	OR IS_BIT_SET(g_SimpleInteriorData.iSeventhBS, BS7_SIMPLE_INTERIOR_RENOVATION_TRIGGERED))
		PRINTLN("[AM_MP_BOARDROOM_SEATING] SHOULD_TERMINATE_SCRIPT - IS_SCREEN_FADED_OUT")
		
		RETURN TRUE
	ENDIF
		
	IF (IS_PLAYER_WALKING_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID()) AND IS_SCREEN_FADED_OUT())
		PRINTLN("[AM_MP_BOARDROOM_SEATING] SHOULD_TERMINATE_SCRIPT - IS_PLAYER_WALKING_OUT_OF_SIMPLE_INTERIOR is TRUE")
		
		RETURN TRUE
	ENDIF
	
	
	
	IF NOT IS_SKYSWOOP_AT_GROUND()
		PRINTLN("[AM_MP_BOARDROOM_SEATING] SHOULD_TERMINATE_SCRIPT - IS_SKYSWOOP_AT_GROUND is FALSE")
		
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())
		IF g_sShopSettings.playerRoom != 767622941 // Terminate if player leaves the area	
			IF NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].MPTVClient.iBitset, MPTV_CLIENT_BS_OCCUPYING_SEAT)
				PRINTLN("[AM_MP_BOARDROOM_SEATING] SHOULD_TERMINATE_SCRIPT - Not in room")			
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF		
	
	IF (NOT IS_PLAYER_IN_CASINO_APARTMENT(PLAYER_ID())
	OR GET_INTERIOR_FLOOR_INDEX() != ciCASINO_APT_FLOOR_APARTMENT)
	AND NOT IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
	AND NOT IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
	AND NOT IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())
	#IF FEATURE_FIXER
	AND NOT IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())
	#ENDIF		
		RETURN TRUE
	ENDIF
				
	RETURN FALSE
ENDFUNC

PROC PROCESS_PRE_GAME()
	PRINTLN("[AM_MP_BOARDROOM_SEATING] - PROCESS_PRE_GAME - Initialising")
	
	NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE, GET_SIMPLE_INTERIOR_INT_SCRIPT_INSTANCE())
	
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
	
	IF IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
	OR IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
		iNumberOfSeats = 5
	ELIF IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())
		iNumberOfSeats = 8
	#IF FEATURE_FIXER
	ELIF IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())	
		iNumberOfSeats = 8
		sMPTVLocal.bInFixerHQ = TRUE		
	#ENDIF
	ELSE
		iNumberOfSeats = 7
	ENDIF	
	
	RESERVE_LOCAL_NETWORK_MISSION_OBJECTS(iNumberOfSeats)
	
	InteriorPropStruct.iIndex = -1 
	InteriorPropStruct.iGarageSize = 0
	InteriorPropStruct.iType = -1
	InteriorPropStruct.iBuildingID = -1	
		
	IF NOT Wait_For_First_Network_Broadcast()
		PRINTLN("[AM_MP_BOARDROOM_SEATING] - PROCESS_PRE_GAME - Failed to receive initial network broadcast.")
		
		SCRIPT_CLEANUP()
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		PRINTLN("[AM_MP_BOARDROOM_SEATING] - PROCESS_PRE_GAME - Initialised.")
	ELSE
		PRINTLN("[AM_MP_BOARDROOM_SEATING] - PROCESS_PRE_GAME - NETWORK_IS_GAME_IN_PROGRESS = FALSE")
		
		SCRIPT_CLEANUP()
	ENDIF
ENDPROC

FUNC VECTOR GET_BOARDROOM_SEAT_LOCATE(INT iIndex, BOOL bSecondVector = FALSE)
	IF IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
		IF NOT bSecondVector
			SWITCH iIndex
				CASE 0	RETURN <<2703.513916,-370.775360,-55.780895>>
				CASE 1	RETURN <<2704.613770,-370.786621,-55.780895>>
				CASE 2	RETURN <<2706.250000,-369.945526,-55.780895>>
				CASE 3	RETURN <<2705.380371,-368.309174,-55.780895>>
				CASE 4	RETURN <<2704.241943,-368.294586,-55.780895>>
			ENDSWITCH
		ELSE
			SWITCH iIndex
				CASE 0	RETURN <<2704.321533,-370.770935,-53.780895>>
				CASE 1	RETURN <<2705.474854,-370.768585,-53.780895>>
				CASE 2	RETURN <<2706.241455,-369.124390,-53.780895>>
				CASE 3	RETURN <<2704.588135,-368.311707,-53.780895>>
				CASE 4	RETURN <<2703.460938,-368.313324,-53.780895>>
			ENDSWITCH
		ENDIF
	ELIF IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
		IF NOT bSecondVector
			SWITCH iIndex
				CASE 0	RETURN <<1563.4500, 390.6300, -54.2800>>
				CASE 1	RETURN <<1563.4500, 389.3500, -54.2800>>
				CASE 2	RETURN <<1563.4500, 387.8000, -54.2800>>
				CASE 3	RETURN <<1563.8000, 390.0000, -54.2800>>
				CASE 4	RETURN <<1563.8000, 387.4000, -54.2800>>
			ENDSWITCH
		ELSE
			SWITCH iIndex
				CASE 0	RETURN <<1562.1000, 390.6300, -52.2800>>
				CASE 1	RETURN <<1562.1000, 389.2000, -52.2800>>
				CASE 2	RETURN <<1562.1000, 387.9500, -52.2800>>
				CASE 3	RETURN <<1565.3500, 390.0000, -52.2800>>
				CASE 4	RETURN <<1565.3500, 387.4000, -52.2800>>
			ENDSWITCH
		ENDIF
	ELIF IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())
		IF NOT bSecondVector
			SWITCH iIndex
				CASE 0	RETURN <<-1349.4041, 142.0680, -96.1500>>	// Chair 1 Min Locate
				CASE 1	RETURN <<-1350.6770, 142.6620, -96.1500>>
				CASE 2	RETURN <<-1351.8380, 142.3550, -96.1500>>
				CASE 3	RETURN <<-1353.0410, 142.0090, -96.1500>>
				CASE 4	RETURN <<-1350.2150, 140.8770, -96.1500>>
				CASE 5	RETURN <<-1351.3979, 140.5720, -96.1500>>
				CASE 6	RETURN <<-1352.6110, 140.2690, -96.1500>>
				CASE 7	RETURN <<-1353.8770, 140.8990, -96.1500>>
			ENDSWITCH
		ELSE
			SWITCH iIndex
				CASE 0	RETURN <<-1348.3088, 142.3617, -94.2000>>	// Chair 1 Max Locate
				CASE 1	RETURN <<-1350.9720, 143.7660, -94.2000>>
				CASE 2	RETURN <<-1352.1320, 143.4520, -94.2000>>
				CASE 3	RETURN <<-1353.3380, 143.1150, -94.2000>>
				CASE 4	RETURN <<-1349.9310, 139.8150, -94.2000>>
				CASE 5	RETURN <<-1351.1080, 139.4900, -94.2000>>
				CASE 6	RETURN <<-1352.3180, 139.1750, -94.2000>>
				CASE 7 	RETURN <<-1354.9800, 140.6030, -94.2000>>
			ENDSWITCH
		ENDIF
		#IF FEATURE_FIXER
	ELIF IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())
		VECTOR vFixerPosition
								
		IF NOT bSecondVector
			SWITCH iIndex
				CASE 0	vFixerPosition = << -7.824, -1.388, 3.800  >>		BREAK	// Chair 1 Min Locate	Original world coords in Hawick location <<386.185870,-52.900055,106.163300>>									
				CASE 1	vFixerPosition = << -8.32683, 0.518825, 3.8003 >>	BREAK	// Original world coords in Hawick location <<388.149475,-53.079788,106.163300>>										
				CASE 2	vFixerPosition = << -8.34352, 2.10567, 3.8003 >>	BREAK	// Original world coords in Hawick location <<389.646332,-53.606842,106.163300>>														
				CASE 3	vFixerPosition = << -8.31509, 3.82615, 3.8003 >>	BREAK	// Original world coords in Hawick location <<391.253326,-54.221989,106.163300>>		
				CASE 4	vFixerPosition = << -5.8089, 4.0711, 3.8003 >>		BREAK	// Original world coords in Hawick location <<390.626343,-56.660816,106.163300>>				
				CASE 5	vFixerPosition = << -5.0809, 2.13945, 3.8003 >>		BREAK	// Original world coords in Hawick location <<388.562195,-56.684254,106.163300>>										
				CASE 6	vFixerPosition = << -5.12508, 0.507945, 3.8003 >>	BREAK	// Original world coords in Hawick location <<387.044189,-56.084724,106.163300>>
				CASE 7	vFixerPosition = << -5.21689, -1.15697, 3.8003 >>	BREAK	// Original world coords in Hawick location <<385.511078,-55.429024,106.163300>>
			ENDSWITCH
		ELSE
			SWITCH iIndex
				CASE 0	vFixerPosition = << -5.672, -1.325, 5.800 >> 		BREAK	// Chair 1 Max Locate	// Original world coords in Hawick location <<385.509203,-54.944006,108.163300>>																
				CASE 1	vFixerPosition = << -8.25242, -1.24265, 5.8003 >> 	BREAK	// Original world coords in Hawick location <<386.468781,-52.547256,108.163300>>															
				CASE 2	vFixerPosition = << -8.31406, 0.553645, 5.8003 >>   BREAK	// Original world coords in Hawick location <<388.177826,-53.103703,108.163300>>														
				CASE 3	vFixerPosition = << -8.40686, 2.18833, 5.8003 >> 	BREAK	// Original world coords in Hawick location <<389.745667,-53.575584,108.163300>>																		
				CASE 4	vFixerPosition = << -7.66931, 4.10364, 5.8003 >> 	BREAK	// Original world coords in Hawick location <<391.293213,-54.923733,108.163300>>										
				CASE 5	vFixerPosition = << -5.12323, 3.70668, 5.8003 >>  	BREAK	// Original world coords in Hawick location <<390.049377,-57.180500,108.163300>> 																		
				CASE 6	vFixerPosition = << -5.12872, 2.04745, 5.8003 >> 	BREAK	// Original world coords in Hawick location <<388.492096,-56.607853,108.163300>> 
				CASE 7 	vFixerPosition = << -5.18656, 0.461797, 5.8003 >> 	BREAK	// Original world coords in Hawick location <<387.021851,-56.011173,108.163300>>
			ENDSWITCH
		ENDIF
		
		RETURN TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS_GIVEN_ID(vFixerPosition, GET_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID()))		
	#ENDIF
	ELSE
		IF NOT bSecondVector
			SWITCH iIndex
				CASE 0	RETURN <<969.805603,55.404041,115.174210>>
				CASE 1	RETURN <<968.829224,56.270962,115.174210>>
				CASE 2	RETURN <<967.824707,56.879871,115.174210>>
				CASE 3	RETURN <<967.955688,58.791992,115.174210>>
				CASE 4	RETURN <<969.938660,58.579243,115.174202>>
				CASE 5	RETURN <<970.953674,57.932941,115.174202>>
				CASE 6	RETURN <<971.970459,57.337189,115.174202>>
			ENDSWITCH
		ELSE
			SWITCH iIndex
				CASE 0	RETURN <<970.588745,54.958439,117.174202>>
				CASE 1	RETURN <<969.606567,55.763111,117.174210>>
				CASE 2	RETURN <<968.515320,56.429581,117.174210>>
				CASE 3	RETURN <<967.479797,58.034542,117.174210>>
				CASE 4	RETURN <<969.187561,59.031597,117.174202>>
				CASE 5	RETURN <<970.227051,58.390736,117.174202>>
				CASE 6	RETURN <<971.268127,57.798199,117.174202>>
			ENDSWITCH
		ENDIF
	ENDIF
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC FLOAT GET_BOARDROOM_SEAT_ENTER_HEADING(INT iIndex)
	IF IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
		SWITCH iIndex
			CASE 0	RETURN 0.0
			CASE 1	RETURN 0.0
			CASE 2	RETURN 90.0
			CASE 3	RETURN 180.0
			CASE 4	RETURN 180.0
		ENDSWITCH
	ELIF IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
		SWITCH iIndex
			CASE 0	RETURN 306.5716
			CASE 1	RETURN 272.9339
			CASE 2	RETURN 293.9756
			CASE 3	RETURN 135.1971
			CASE 4	RETURN 86.5222
		ENDSWITCH
	ELIF IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())
		SWITCH iIndex
			CASE 0	RETURN 107.0537
			CASE 1	RETURN 182.2757
			CASE 2	RETURN 182.2757
			CASE 3	RETURN 182.2757
			CASE 4	RETURN 14.0876
			CASE 5	RETURN 14.0876
			CASE 6	RETURN 14.0876
			CASE 7	RETURN 286.4302
		ENDSWITCH
	#IF FEATURE_FIXER
	ELIF IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())
		FLOAT fFixerHeading
	
		SWITCH iIndex
			CASE 0	fFixerHeading = 357.195190	BREAK	// Original world heading at Hawick location 247.1952
			CASE 1	fFixerHeading = 277.760803	BREAK	// Original world heading at Hawick location 167.7608			
			CASE 2	fFixerHeading = 277.760803	BREAK 	// Original world heading at Hawick location 167.7608
			CASE 3	fFixerHeading = 277.760803	BREAK	// Original world heading at Hawick location 167.7608		
			CASE 4	fFixerHeading = 181.412811		BREAK	// Original world heading at Hawick location 71.4128
			CASE 5	fFixerHeading = 81.970886	BREAK	// Original world heading at Hawick location 31.9709
			CASE 6	fFixerHeading = 81.970886	BREAK	// Original world heading at Hawick location 31.9709
			CASE 7	fFixerHeading = 81.970886	BREAK	// Original world heading at Hawick location 31.9709
		ENDSWITCH
		
		fFixerHeading = TRANSFORM_SIMPLE_INTERIOR_HEADING_TO_WORLD_HEADING_GIVEN_ID(fFixerHeading, GET_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID()))	
		
		RETURN fFixerHeading
	#ENDIF
	ELSE
		SWITCH iIndex
			CASE 0	RETURN 330.0
			CASE 1	RETURN 330.0
			CASE 2	RETURN 330.0
			CASE 3	RETURN 235.0
			CASE 4	RETURN 145.0
			CASE 5	RETURN 145.0
			CASE 6	RETURN 145.0
		ENDSWITCH
	ENDIF
	
	RETURN 0.0
ENDFUNC

FUNC FLOAT GET_BOARDROOM_SEAT_HEADING_ACCEPTABLE_LEEWAY()
	IF IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())
	#IF FEATURE_FIXER
	OR IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())
	#ENDIF
		RETURN 50.0
	ENDIF
	
	RETURN 45.0
ENDFUNC

FUNC FLOAT GET_BOARDROOM_SEAT_SYNCHRONIZED_SCENE_RATE()
	IF IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())
		RETURN 1.15
	ENDIF
	
	RETURN 1.0
ENDFUNC

FUNC FLOAT GET_BOARDROOM_SEAT_WALK_TARGET_RADIUS()
	IF IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())
		RETURN 0.15
	ENDIF
	
	RETURN 0.05
ENDFUNC

FUNC FLOAT GET_BOARDROOM_SEAT_CLEANUP_PHASE()
	IF IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())
		RETURN 0.80
	ENDIF
	
	RETURN 0.99
ENDFUNC

FUNC BOOL IS_PLAYER_IN_BOARDROOM_SEAT_AREA(PLAYER_INDEX pPlayer, INT iIndex = -1, BOOL bSetIndex = FALSE)
	IF IS_NET_PLAYER_OK(pPlayer)
		PED_INDEX pPed = GET_PLAYER_PED(pPlayer)
		
		IF DOES_ENTITY_EXIST(pPed)
			INT i
			REPEAT iNumberOfSeats i
				IF ((iIndex = -1) OR (iIndex = i))
					IF IS_ENTITY_IN_ANGLED_AREA(pPed, GET_BOARDROOM_SEAT_LOCATE(i), GET_BOARDROOM_SEAT_LOCATE(i, TRUE), 1.25)
						IF bSetIndex
							sBoardroomSeatingData.iBoardroomSeatID = i
							playerBD[NATIVE_TO_INT(PLAYER_ID())].iBoardroomSeatID = i
							sBoardroomSeatingData.fIdealHeading = GET_BOARDROOM_SEAT_ENTER_HEADING(i)
						ENDIF
						
						RETURN TRUE
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_ALONE_IN_BOARDROOM_SEAT_AREA()
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		PLAYER_INDEX playerID = INT_TO_PLAYERINDEX(i)
		
		IF IS_NET_PLAYER_OK(playerID)
		AND NOT NETWORK_IS_PLAYER_CONCEALED(playerID)
			IF playerID <> PLAYER_ID()
				IF IS_PLAYER_IN_BOARDROOM_SEAT_AREA(playerID, sBoardroomSeatingData.iBoardroomSeatID)
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

PROC MAINTAIN_INTERUPT_LOGIC()
	IF sBoardroomSeatingData.eCurrentState > BOARDROOM_SEATING_STATE_WALKING_TO_POSITION
		INVALIDATE_IDLE_CAM()
		HUD_FORCE_WEAPON_WHEEL(FALSE)
		DISPLAY_AMMO_THIS_FRAME(FALSE)			
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_WHEEL)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
		HUD_SUPPRESS_WEAPON_WHEEL_RESULTS_THIS_FRAME()
		SET_CLEAR_ON_CALL_HUD_THIS_FRAME()
		DISABLE_CELLPHONE_THIS_FRAME_ONLY()
		DISABLE_SELECTOR_THIS_FRAME()
	ENDIF
ENDPROC

/// PURPOSE:
///    Manages some camera collision issues with the chair while playing some of the animations
/// PARAMS:
///    fCapsuleSize - The capsule size the local player peds capsule should be set to
///    bRemoveCollision - Should collision between the player ped and seat object be removed this frame?
PROC MANAGE_CAMERA_COLLISION(FLOAT fCapsuleSize = 0.25, BOOL bRemoveCollision = TRUE)
	IF DOES_ENTITY_EXIST(NET_TO_OBJ(serverBD.objSeats[sBoardroomSeatingData.iBoardroomSeatID]))

		SET_PED_CAPSULE(PLAYER_PED_ID(), fCapsuleSize)				
		
		SET_GAMEPLAY_CAM_IGNORE_ENTITY_COLLISION_THIS_UPDATE(NET_TO_OBJ(serverBD.objSeats[sBoardroomSeatingData.iBoardroomSeatID]))
		
		IF bRemoveCollision
			SET_ENTITY_NO_COLLISION_ENTITY(PLAYER_PED_ID(), NET_TO_OBJ(serverBD.objSeats[sBoardroomSeatingData.iBoardroomSeatID]), TRUE) // replacing collision between player and office chair.
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_BOARDROOM_SEATING()
	MAINTAIN_INTERUPT_LOGIC()
	
	BOARDROOM_SEATING_DIRECTION eDirection = BOARDROOM_SEATING_DIRECTION_CENTER
	
	INT iBoardroomSeatingLocalSceneID
	
	STRING sPlayerClip
	STRING sChairClip
	
	IF IS_TRANSITION_ACTIVE()
	OR NOT IS_SKYSWOOP_AT_GROUND()
	AND (sBoardroomSeatingData.eCurrentState > BOARDROOM_SEATING_STATE_WALK)
		CLEANUP_BOARDROOM_SEATING(TRUE)
	ENDIF
	
	#IF FEATURE_HEIST_ISLAND
	IF IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
	AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
		IF sBoardroomSeatingData.eCurrentState >= BOARDROOM_SEATING_STATE_PROMPT
		AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<1563.3784, 388.8997, -54.2859>>) <= 3.0
			SET_PED_CAPSULE(PLAYER_PED_ID(), 0.17)
		ENDIF
	ENDIF
	#ENDIF
	
	SWITCH sBoardroomSeatingData.eCurrentState
		CASE BOARDROOM_SEATING_STATE_INIT
			SET_BOARDROOM_SEATING_STATE(BOARDROOM_SEATING_STATE_PROMPT)
		BREAK
		
		CASE BOARDROOM_SEATING_STATE_PROMPT
			IF IS_NET_PLAYER_OK(PLAYER_ID())
			AND NOT g_sMPTunables.bVC_PENTHOUSE_DISABLE_DINING
			AND IS_PLAYER_CONTROL_ON(PLAYER_ID())
			AND NOT IS_ANY_INTERACTION_ANIM_PLAYING()
			AND NOT IS_PAUSE_MENU_ACTIVE_EX()
			AND NOT IS_BIT_SET(g_simpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_INSTANT_PASS_OUT)
			AND NOT IS_PED_RAGDOLL(PLAYER_PED_ID())
			AND Get_Peds_Drunk_Alcohol_Hit_Count(PLAYER_PED_ID()) <= 9
			AND NOT g_SpawnData.bPassedOutDrunk
			AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
			AND IS_PLAYER_IN_BOARDROOM_SEAT_AREA(PLAYER_ID(), DEFAULT, TRUE)
			AND IS_PLAYER_ALONE_IN_BOARDROOM_SEAT_AREA()
			AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			AND IS_HEADING_ACCEPTABLE_CORRECTED(GET_ENTITY_HEADING(PLAYER_PED_ID()), sBoardroomSeatingData.fIdealHeading, GET_BOARDROOM_SEAT_HEADING_ACCEPTABLE_LEEWAY())
				IF sBoardroomSeatingData.iBoardroomSeatContextIntention = NEW_CONTEXT_INTENTION
					IF sBoardroomSeatingData.iBoardroomSeatID = 3
					AND GET_OWNER_OF_SIMPLE_INTERIOR_LOCAL_PLAYER_IS_IN() != PLAYER_ID()
					AND NOT IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
					AND NOT IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
					AND NOT IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())
					#IF FEATURE_FIXER
					AND NOT IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())
					#ENDIF
						PRINT_HELP_FOREVER("MPJAC_NO_SIT")
					ELSE
						REGISTER_CONTEXT_INTENTION(sBoardroomSeatingData.iBoardroomSeatContextIntention, CP_HIGH_PRIORITY, "MPJAC_SIT")
					ENDIF
				ELSE
					IF HAS_CONTEXT_BUTTON_TRIGGERED(sBoardroomSeatingData.iBoardroomSeatContextIntention)
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPJAC_NO_SIT")
							CLEAR_HELP()
						ENDIF
						
						RELEASE_CONTEXT_INTENTION(sBoardroomSeatingData.iBoardroomSeatContextIntention)
						sBoardroomSeatingData.iBoardroomSeatContextIntention = NEW_CONTEXT_INTENTION
						
						SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_UseKinematicModeWhenStationary, TRUE)
						
						SET_BIT(g_SimpleInteriorData.iSeventhBS, BS7_SIMPLE_INTERIOR_USING_SEATED_ACTIVITY)
						
						IF IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
							SWITCH sBoardroomSeatingData.iBoardroomSeatID
								CASE 0	sBoardroomSeatingData.eBoardroomSeatingVariation = BOARDROOM_SEATING_VAR_A	BREAK
								CASE 1	sBoardroomSeatingData.eBoardroomSeatingVariation = BOARDROOM_SEATING_VAR_B	BREAK
								CASE 2	sBoardroomSeatingData.eBoardroomSeatingVariation = BOARDROOM_SEATING_VAR_C	BREAK
								CASE 3	sBoardroomSeatingData.eBoardroomSeatingVariation = BOARDROOM_SEATING_VAR_A	BREAK
								CASE 4	sBoardroomSeatingData.eBoardroomSeatingVariation = BOARDROOM_SEATING_VAR_B	BREAK
							ENDSWITCH
						ELIF IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
							SWITCH sBoardroomSeatingData.iBoardroomSeatID
								CASE 0	sBoardroomSeatingData.eBoardroomSeatingVariation = BOARDROOM_SEATING_VAR_A	BREAK
								CASE 1	sBoardroomSeatingData.eBoardroomSeatingVariation = BOARDROOM_SEATING_VAR_B	BREAK
								CASE 2	sBoardroomSeatingData.eBoardroomSeatingVariation = BOARDROOM_SEATING_VAR_C	BREAK
								CASE 3	sBoardroomSeatingData.eBoardroomSeatingVariation = BOARDROOM_SEATING_VAR_A	BREAK
								CASE 4	sBoardroomSeatingData.eBoardroomSeatingVariation = BOARDROOM_SEATING_VAR_B	BREAK
								CASE 5	sBoardroomSeatingData.eBoardroomSeatingVariation = BOARDROOM_SEATING_VAR_C	BREAK
								CASE 6	sBoardroomSeatingData.eBoardroomSeatingVariation = BOARDROOM_SEATING_VAR_A	BREAK
								CASE 7	sBoardroomSeatingData.eBoardroomSeatingVariation = BOARDROOM_SEATING_VAR_B	BREAK
							ENDSWITCH
						ELIF IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())
							SWITCH sBoardroomSeatingData.iBoardroomSeatID
								CASE 0	sBoardroomSeatingData.eBoardroomSeatingVariation = BOARDROOM_SEATING_VAR_A	BREAK
								CASE 1	sBoardroomSeatingData.eBoardroomSeatingVariation = BOARDROOM_SEATING_VAR_B	BREAK
								CASE 2	sBoardroomSeatingData.eBoardroomSeatingVariation = BOARDROOM_SEATING_VAR_C	BREAK
								CASE 3	sBoardroomSeatingData.eBoardroomSeatingVariation = BOARDROOM_SEATING_VAR_A	BREAK
								CASE 4	sBoardroomSeatingData.eBoardroomSeatingVariation = BOARDROOM_SEATING_VAR_B	BREAK
								CASE 5	sBoardroomSeatingData.eBoardroomSeatingVariation = BOARDROOM_SEATING_VAR_C	BREAK
								CASE 6	sBoardroomSeatingData.eBoardroomSeatingVariation = BOARDROOM_SEATING_VAR_A	BREAK
								CASE 7	sBoardroomSeatingData.eBoardroomSeatingVariation = BOARDROOM_SEATING_VAR_B	BREAK
							ENDSWITCH
						#IF FEATURE_FIXER
						ELIF IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())
							SWITCH sBoardroomSeatingData.iBoardroomSeatID
								CASE 0	sBoardroomSeatingData.eBoardroomSeatingVariation = BOARDROOM_SEATING_VAR_A	BREAK
								CASE 1	sBoardroomSeatingData.eBoardroomSeatingVariation = BOARDROOM_SEATING_VAR_B	BREAK
								CASE 2	sBoardroomSeatingData.eBoardroomSeatingVariation = BOARDROOM_SEATING_VAR_C	BREAK
								CASE 3	sBoardroomSeatingData.eBoardroomSeatingVariation = BOARDROOM_SEATING_VAR_A	BREAK
								CASE 4	sBoardroomSeatingData.eBoardroomSeatingVariation = BOARDROOM_SEATING_VAR_A	BREAK
								CASE 5	sBoardroomSeatingData.eBoardroomSeatingVariation = BOARDROOM_SEATING_VAR_C	BREAK
								CASE 6	sBoardroomSeatingData.eBoardroomSeatingVariation = BOARDROOM_SEATING_VAR_B	BREAK
								CASE 7	sBoardroomSeatingData.eBoardroomSeatingVariation = BOARDROOM_SEATING_VAR_B	BREAK
							ENDSWITCH
						#ENDIF
						ELSE
							SWITCH sBoardroomSeatingData.iBoardroomSeatID
								CASE 0	sBoardroomSeatingData.eBoardroomSeatingVariation = BOARDROOM_SEATING_VAR_A	BREAK
								CASE 1	sBoardroomSeatingData.eBoardroomSeatingVariation = BOARDROOM_SEATING_VAR_B	BREAK
								CASE 2	sBoardroomSeatingData.eBoardroomSeatingVariation = BOARDROOM_SEATING_VAR_C	BREAK
								CASE 3	sBoardroomSeatingData.eBoardroomSeatingVariation = BOARDROOM_SEATING_VAR_A	BREAK
								CASE 4	sBoardroomSeatingData.eBoardroomSeatingVariation = BOARDROOM_SEATING_VAR_A	BREAK
								CASE 5	sBoardroomSeatingData.eBoardroomSeatingVariation = BOARDROOM_SEATING_VAR_C	BREAK
								CASE 6	sBoardroomSeatingData.eBoardroomSeatingVariation = BOARDROOM_SEATING_VAR_B	BREAK
							ENDSWITCH
						ENDIF
						
						SET_BOARDROOM_SEATING_STATE(BOARDROOM_SEATING_STATE_WALK)
					ENDIF
				ENDIF
			ELSE
				IF sBoardroomSeatingData.iBoardroomSeatContextIntention != NEW_CONTEXT_INTENTION
					RELEASE_CONTEXT_INTENTION(sBoardroomSeatingData.iBoardroomSeatContextIntention)
					sBoardroomSeatingData.iBoardroomSeatContextIntention = NEW_CONTEXT_INTENTION
				ENDIF
				
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPJAC_NO_SIT")
					CLEAR_HELP()
				ENDIF
			ENDIF
		BREAK
		
		CASE BOARDROOM_SEATING_STATE_WALK
			BOOL bAnimsLoaded
			
			IF sBoardroomSeatingData.iBoardroomSeatID != 3
			OR IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
			OR IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
			OR IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())
			OR IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())
				REQUEST_ANIM_DICT(GET_BOARDROOM_SEATING_ANIM_DICT(sBoardroomSeatingData.iBoardroomSeatID, sBoardroomSeatingData.eBoardroomSeatingVariation, BOARDROOM_SEATING_DIRECTION_CENTER))
				REQUEST_ANIM_DICT(GET_BOARDROOM_SEATING_ANIM_DICT(sBoardroomSeatingData.iBoardroomSeatID, sBoardroomSeatingData.eBoardroomSeatingVariation, BOARDROOM_SEATING_DIRECTION_LEFT))
				REQUEST_ANIM_DICT(GET_BOARDROOM_SEATING_ANIM_DICT(sBoardroomSeatingData.iBoardroomSeatID, sBoardroomSeatingData.eBoardroomSeatingVariation, BOARDROOM_SEATING_DIRECTION_RIGHT))
				
				IF HAS_ANIM_DICT_LOADED(GET_BOARDROOM_SEATING_ANIM_DICT(sBoardroomSeatingData.iBoardroomSeatID, sBoardroomSeatingData.eBoardroomSeatingVariation, BOARDROOM_SEATING_DIRECTION_CENTER))
				AND HAS_ANIM_DICT_LOADED(GET_BOARDROOM_SEATING_ANIM_DICT(sBoardroomSeatingData.iBoardroomSeatID, sBoardroomSeatingData.eBoardroomSeatingVariation, BOARDROOM_SEATING_DIRECTION_LEFT))
				AND HAS_ANIM_DICT_LOADED(GET_BOARDROOM_SEATING_ANIM_DICT(sBoardroomSeatingData.iBoardroomSeatID, sBoardroomSeatingData.eBoardroomSeatingVariation, BOARDROOM_SEATING_DIRECTION_RIGHT))
					bAnimsLoaded = TRUE
				ENDIF
			ELSE
				REQUEST_ANIM_DICT(GET_BOARDROOM_SEATING_ANIM_DICT(sBoardroomSeatingData.iBoardroomSeatID))
				
				IF HAS_ANIM_DICT_LOADED(GET_BOARDROOM_SEATING_ANIM_DICT(sBoardroomSeatingData.iBoardroomSeatID))
					bAnimsLoaded = TRUE
				ENDIF
			ENDIF
			
			IF bAnimsLoaded
			AND NOT IS_PED_INJURED(PLAYER_PED_ID())
				VECTOR vPlayerPosition, vPlayerRotation
				vPlayerPosition = GET_ANIM_INITIAL_OFFSET_POSITION(GET_BOARDROOM_SEATING_ANIM_DICT(sBoardroomSeatingData.iBoardroomSeatID, sBoardroomSeatingData.eBoardroomSeatingVariation), "enter", GET_BOARDROOM_SEATING_SCENE_POSITION(sBoardroomSeatingData.iBoardroomSeatID, TRUE), GET_BOARDROOM_SEATING_SCENE_ROTATION(sBoardroomSeatingData.iBoardroomSeatID))
				vPlayerRotation = GET_ANIM_INITIAL_OFFSET_ROTATION(GET_BOARDROOM_SEATING_ANIM_DICT(sBoardroomSeatingData.iBoardroomSeatID, sBoardroomSeatingData.eBoardroomSeatingVariation), "enter", GET_BOARDROOM_SEATING_SCENE_POSITION(sBoardroomSeatingData.iBoardroomSeatID, TRUE), GET_BOARDROOM_SEATING_SCENE_ROTATION(sBoardroomSeatingData.iBoardroomSeatID))
				
				TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), vPlayerPosition, PEDMOVEBLENDRATIO_WALK, DEFAULT, vPlayerRotation.z, GET_BOARDROOM_SEAT_WALK_TARGET_RADIUS())
				
				SET_BOARDROOM_SEATING_STATE(BOARDROOM_SEATING_STATE_WALKING_TO_POSITION)
			ENDIF
		BREAK
		
		CASE BOARDROOM_SEATING_STATE_WALKING_TO_POSITION
			IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.objSeats[sBoardroomSeatingData.iBoardroomSeatID])
				NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.objSeats[sBoardroomSeatingData.iBoardroomSeatID])
			ENDIF
			
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.objSeats[sBoardroomSeatingData.iBoardroomSeatID])
				AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) <> PERFORMING_TASK
				AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) <> WAITING_TO_START_TASK
					IF sBoardroomSeatingData.iBoardroomSeatID = 3
					AND NOT IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
					AND NOT IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
					AND NOT IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())
					AND NOT IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())
						sPlayerClip = "enter_b"
						sChairClip = "enter_b_chair"
					ELSE
						sPlayerClip = "enter"
						sChairClip = "enter_chair"
					ENDIF
					
					sBoardroomSeatingData.iBoardroomSeatSyncSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(GET_ENTITY_COORDS(NET_TO_OBJ(serverBD.objSeats[sBoardroomSeatingData.iBoardroomSeatID])), GET_ENTITY_ROTATION(NET_TO_OBJ(serverBD.objSeats[sBoardroomSeatingData.iBoardroomSeatID])), DEFAULT, TRUE)
					
					NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), sBoardroomSeatingData.iBoardroomSeatSyncSceneID, GET_BOARDROOM_SEATING_ANIM_DICT(sBoardroomSeatingData.iBoardroomSeatID, sBoardroomSeatingData.eBoardroomSeatingVariation), sPlayerClip, REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT)
					NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_OBJ(serverBD.objSeats[sBoardroomSeatingData.iBoardroomSeatID]), sBoardroomSeatingData.iBoardroomSeatSyncSceneID, GET_BOARDROOM_SEATING_ANIM_DICT(sBoardroomSeatingData.iBoardroomSeatID, sBoardroomSeatingData.eBoardroomSeatingVariation), sChairClip, REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS)
					
					NETWORK_START_SYNCHRONISED_SCENE(sBoardroomSeatingData.iBoardroomSeatSyncSceneID)
						
					SET_BOARDROOM_SEATING_STATE(BOARDROOM_SEATING_STATE_WAIT_TO_SIT)
				ENDIF
			ENDIF
		BREAK
		
		CASE BOARDROOM_SEATING_STATE_WAIT_TO_SIT
			IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.objSeats[sBoardroomSeatingData.iBoardroomSeatID])
				NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.objSeats[sBoardroomSeatingData.iBoardroomSeatID])
			ENDIF
			
			// B*7378867 - do collision fixes for the Fixer HQ boardroom
			#IF FEATURE_FIXER
			IF IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())
				MANAGE_CAMERA_COLLISION()
			ENDIF
			#ENDIF
			
			iBoardroomSeatingLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sBoardroomSeatingData.iBoardroomSeatSyncSceneID)
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iBoardroomSeatingLocalSceneID)
				IF GET_SYNCHRONIZED_SCENE_RATE(iBoardroomSeatingLocalSceneID) != GET_BOARDROOM_SEAT_SYNCHRONIZED_SCENE_RATE()
					SET_SYNCHRONIZED_SCENE_RATE(iBoardroomSeatingLocalSceneID, GET_BOARDROOM_SEAT_SYNCHRONIZED_SCENE_RATE())
				ENDIF
			ENDIF
			
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.objSeats[sBoardroomSeatingData.iBoardroomSeatID])
				IF iBoardroomSeatingLocalSceneID = -1
				OR GET_SYNCHRONIZED_SCENE_PHASE(iBoardroomSeatingLocalSceneID) >= 0.99
					sBoardroomSeatingData.iBoardroomSeatSyncSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(GET_ENTITY_COORDS(NET_TO_OBJ(serverBD.objSeats[sBoardroomSeatingData.iBoardroomSeatID])), GET_ENTITY_ROTATION(NET_TO_OBJ(serverBD.objSeats[sBoardroomSeatingData.iBoardroomSeatID])), DEFAULT, TRUE)
					
					NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), sBoardroomSeatingData.iBoardroomSeatSyncSceneID, GET_BOARDROOM_SEATING_ANIM_DICT(sBoardroomSeatingData.iBoardroomSeatID, sBoardroomSeatingData.eBoardroomSeatingVariation), "base", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT)
					NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_OBJ(serverBD.objSeats[sBoardroomSeatingData.iBoardroomSeatID]), sBoardroomSeatingData.iBoardroomSeatSyncSceneID, GET_BOARDROOM_SEATING_ANIM_DICT(sBoardroomSeatingData.iBoardroomSeatID, sBoardroomSeatingData.eBoardroomSeatingVariation), "base_chair", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS)
					
					NETWORK_START_SYNCHRONISED_SCENE(sBoardroomSeatingData.iBoardroomSeatSyncSceneID)
					
					SET_BOARDROOM_SEATING_STATE(BOARDROOM_SEATING_STATE_IDLE)
				ENDIF
			ENDIF
		BREAK
		
		CASE BOARDROOM_SEATING_STATE_IDLE
			
			IF IS_BIT_SET(sBoardroomSeatingData.iBoardroomSeatBS, BOARDROOM_SEATING_BS_USING_TV_CONTROLS)
				EXIT
			ENDIF
		
			IF NOT IS_BIT_SET(sBoardroomSeatingData.iBoardroomSeatBS, BOARDROOM_SEATING_BS_PLAYING_IDLE)
			AND NOT IS_BIT_SET(sBoardroomSeatingData.iBoardroomSeatBS, BOARDROOM_SEATING_BS_TURNED_LEFT)
			AND NOT IS_BIT_SET(sBoardroomSeatingData.iBoardroomSeatBS, BOARDROOM_SEATING_BS_TURNED_RIGHT)
			AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.objSeats[3])
				PLAYER_INDEX pOwner
				pOwner = GET_OWNER_OF_SIMPLE_INTERIOR_LOCAL_PLAYER_IS_IN()
				
				IF pOwner != INVALID_PLAYER_INDEX()
				AND IS_NET_PLAYER_OK(pOwner)
					IF playerBD[NATIVE_TO_INT(pOwner)].iBoardroomSeatID = 3
					AND NOT IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
					AND NOT IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
					AND NOT IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())
					AND NOT IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())
						IF NOT IS_PED_HEADTRACKING_ENTITY(PLAYER_PED_ID(), NET_TO_OBJ(serverBD.objSeats[3]))
							TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), NET_TO_OBJ(serverBD.objSeats[3]), -1, SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_HIGH)
						ENDIF
					ELSE
						IF IS_PED_HEADTRACKING_ENTITY(PLAYER_PED_ID(), NET_TO_OBJ(serverBD.objSeats[3]))
							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
								TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF IS_PED_HEADTRACKING_ENTITY(PLAYER_PED_ID(), NET_TO_OBJ(serverBD.objSeats[3]))
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.objSeats[sBoardroomSeatingData.iBoardroomSeatID])
				NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.objSeats[sBoardroomSeatingData.iBoardroomSeatID])
			ENDIF
			
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.objSeats[sBoardroomSeatingData.iBoardroomSeatID])
				iBoardroomSeatingLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sBoardroomSeatingData.iBoardroomSeatSyncSceneID)
				
				IF iBoardroomSeatingLocalSceneID = -1
				OR GET_SYNCHRONIZED_SCENE_PHASE(iBoardroomSeatingLocalSceneID) >= 0.99
					IF NOT IS_BIT_SET(sBoardroomSeatingData.iBoardroomSeatBS, BOARDROOM_SEATING_BS_PLAYING_IDLE)
						INT iRandClip
						iRandClip = GET_RANDOM_INT_IN_RANGE(0, 3)
						
						IF NOT IS_BIT_SET(sBoardroomSeatingData.iBoardroomSeatBS, BOARDROOM_SEATING_BS_TURNED_LEFT)
						AND NOT IS_BIT_SET(sBoardroomSeatingData.iBoardroomSeatBS, BOARDROOM_SEATING_BS_TURNED_RIGHT)
						AND NOT IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
						AND NOT IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
						AND NOT IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())
							SWITCH iRandClip
								CASE 0
									sPlayerClip = "idle_a"
									sChairClip = "idle_a_chair"
								BREAK
								
								CASE 1
									sPlayerClip = "idle_d"
									sChairClip = "idle_d_chair"
								BREAK
								
								CASE 2
									sPlayerClip = "idle_e"
									sChairClip = "idle_e_chair"
								BREAK
							ENDSWITCH
						ELSE
							SWITCH iRandClip
								CASE 0
									sPlayerClip = "idle_a"
									sChairClip = "idle_a_chair"
								BREAK
								
								CASE 1
									sPlayerClip = "idle_b"
									sChairClip = "idle_b_chair"
								BREAK
								
								CASE 2
									sPlayerClip = "idle_c"
									sChairClip = "idle_c_chair"
								BREAK
							ENDSWITCH
						ENDIF
						
						sBoardroomSeatingData.iBoardroomSeatSyncSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(GET_ENTITY_COORDS(NET_TO_OBJ(serverBD.objSeats[sBoardroomSeatingData.iBoardroomSeatID])), GET_ENTITY_ROTATION(NET_TO_OBJ(serverBD.objSeats[sBoardroomSeatingData.iBoardroomSeatID])), DEFAULT, TRUE)
						
						IF IS_BIT_SET(sBoardroomSeatingData.iBoardroomSeatBS, BOARDROOM_SEATING_BS_TURNED_LEFT)
							NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), sBoardroomSeatingData.iBoardroomSeatSyncSceneID, GET_BOARDROOM_SEATING_ANIM_DICT(sBoardroomSeatingData.iBoardroomSeatID, sBoardroomSeatingData.eBoardroomSeatingVariation, BOARDROOM_SEATING_DIRECTION_LEFT), sPlayerClip, REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT)
							NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_OBJ(serverBD.objSeats[sBoardroomSeatingData.iBoardroomSeatID]), sBoardroomSeatingData.iBoardroomSeatSyncSceneID, GET_BOARDROOM_SEATING_ANIM_DICT(sBoardroomSeatingData.iBoardroomSeatID, sBoardroomSeatingData.eBoardroomSeatingVariation, BOARDROOM_SEATING_DIRECTION_LEFT), sChairClip, REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS)
						ELIF IS_BIT_SET(sBoardroomSeatingData.iBoardroomSeatBS, BOARDROOM_SEATING_BS_TURNED_RIGHT)
							NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), sBoardroomSeatingData.iBoardroomSeatSyncSceneID, GET_BOARDROOM_SEATING_ANIM_DICT(sBoardroomSeatingData.iBoardroomSeatID, sBoardroomSeatingData.eBoardroomSeatingVariation, BOARDROOM_SEATING_DIRECTION_RIGHT), sPlayerClip, REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT)
							NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_OBJ(serverBD.objSeats[sBoardroomSeatingData.iBoardroomSeatID]), sBoardroomSeatingData.iBoardroomSeatSyncSceneID, GET_BOARDROOM_SEATING_ANIM_DICT(sBoardroomSeatingData.iBoardroomSeatID, sBoardroomSeatingData.eBoardroomSeatingVariation, BOARDROOM_SEATING_DIRECTION_RIGHT), sChairClip, REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS)
						ELSE
							NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), sBoardroomSeatingData.iBoardroomSeatSyncSceneID, GET_BOARDROOM_SEATING_ANIM_DICT(sBoardroomSeatingData.iBoardroomSeatID, sBoardroomSeatingData.eBoardroomSeatingVariation), sPlayerClip, REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT)
							NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_OBJ(serverBD.objSeats[sBoardroomSeatingData.iBoardroomSeatID]), sBoardroomSeatingData.iBoardroomSeatSyncSceneID, GET_BOARDROOM_SEATING_ANIM_DICT(sBoardroomSeatingData.iBoardroomSeatID, sBoardroomSeatingData.eBoardroomSeatingVariation), sChairClip, REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS)
						ENDIF
						
						NETWORK_START_SYNCHRONISED_SCENE(sBoardroomSeatingData.iBoardroomSeatSyncSceneID)
						
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
						ENDIF
						
						SET_BIT(sBoardroomSeatingData.iBoardroomSeatBS, BOARDROOM_SEATING_BS_PLAYING_IDLE)
					ELSE
						sBoardroomSeatingData.iBoardroomSeatSyncSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(GET_ENTITY_COORDS(NET_TO_OBJ(serverBD.objSeats[sBoardroomSeatingData.iBoardroomSeatID])), GET_ENTITY_ROTATION(NET_TO_OBJ(serverBD.objSeats[sBoardroomSeatingData.iBoardroomSeatID])), DEFAULT, TRUE)
						
						IF IS_BIT_SET(sBoardroomSeatingData.iBoardroomSeatBS, BOARDROOM_SEATING_BS_TURNED_LEFT)
							NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), sBoardroomSeatingData.iBoardroomSeatSyncSceneID, GET_BOARDROOM_SEATING_ANIM_DICT(sBoardroomSeatingData.iBoardroomSeatID, sBoardroomSeatingData.eBoardroomSeatingVariation, BOARDROOM_SEATING_DIRECTION_LEFT), "base", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT)
							NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_OBJ(serverBD.objSeats[sBoardroomSeatingData.iBoardroomSeatID]), sBoardroomSeatingData.iBoardroomSeatSyncSceneID, GET_BOARDROOM_SEATING_ANIM_DICT(sBoardroomSeatingData.iBoardroomSeatID, sBoardroomSeatingData.eBoardroomSeatingVariation, BOARDROOM_SEATING_DIRECTION_LEFT), "base_chair", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS)
						ELIF IS_BIT_SET(sBoardroomSeatingData.iBoardroomSeatBS, BOARDROOM_SEATING_BS_TURNED_RIGHT)
							NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), sBoardroomSeatingData.iBoardroomSeatSyncSceneID, GET_BOARDROOM_SEATING_ANIM_DICT(sBoardroomSeatingData.iBoardroomSeatID, sBoardroomSeatingData.eBoardroomSeatingVariation, BOARDROOM_SEATING_DIRECTION_RIGHT), "base", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT)
							NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_OBJ(serverBD.objSeats[sBoardroomSeatingData.iBoardroomSeatID]), sBoardroomSeatingData.iBoardroomSeatSyncSceneID, GET_BOARDROOM_SEATING_ANIM_DICT(sBoardroomSeatingData.iBoardroomSeatID, sBoardroomSeatingData.eBoardroomSeatingVariation, BOARDROOM_SEATING_DIRECTION_RIGHT), "base_chair", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS)
						ELSE
							NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), sBoardroomSeatingData.iBoardroomSeatSyncSceneID, GET_BOARDROOM_SEATING_ANIM_DICT(sBoardroomSeatingData.iBoardroomSeatID, sBoardroomSeatingData.eBoardroomSeatingVariation), "base", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT)
							NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_OBJ(serverBD.objSeats[sBoardroomSeatingData.iBoardroomSeatID]), sBoardroomSeatingData.iBoardroomSeatSyncSceneID, GET_BOARDROOM_SEATING_ANIM_DICT(sBoardroomSeatingData.iBoardroomSeatID, sBoardroomSeatingData.eBoardroomSeatingVariation), "base_chair", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS)
						ENDIF
						
						NETWORK_START_SYNCHRONISED_SCENE(sBoardroomSeatingData.iBoardroomSeatSyncSceneID)
						
						CLEAR_BIT(sBoardroomSeatingData.iBoardroomSeatBS, BOARDROOM_SEATING_BS_PLAYING_IDLE)
					ENDIF
				ENDIF
				
				IF IS_NET_PLAYER_OK(PLAYER_ID())
				AND NOT IS_ANY_INTERACTION_ANIM_PLAYING()
				AND NOT IS_PAUSE_MENU_ACTIVE_EX()
				AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
				AND NOT IS_INTERACTION_MENU_OPEN()
					IF HAVE_CONTROLS_CHANGED(FRONTEND_CONTROL)
					OR IS_BIT_SET(sBoardroomSeatingData.iBoardroomSeatBS, BOARDROOM_SEATING_BS_PLAYING_IDLE)
						IF sBoardroomSeatingData.iBoardroomSeatContextIntention != NEW_CONTEXT_INTENTION
							RELEASE_CONTEXT_INTENTION(sBoardroomSeatingData.iBoardroomSeatContextIntention)
							sBoardroomSeatingData.iBoardroomSeatContextIntention = NEW_CONTEXT_INTENTION
						ENDIF
					ENDIF
					
					IF sBoardroomSeatingData.iBoardroomSeatContextIntention = NEW_CONTEXT_INTENTION
						IF NOT IS_BIT_SET(sBoardroomSeatingData.iBoardroomSeatBS, BOARDROOM_SEATING_BS_PLAYING_IDLE)
							IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)											
								IF sBoardroomSeatingData.iBoardroomSeatID = 3
								AND NOT IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
								AND NOT IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
								AND NOT IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())
								AND NOT IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())
									REGISTER_CONTEXT_INTENTION(sBoardroomSeatingData.iBoardroomSeatContextIntention, CP_HIGH_PRIORITY, "MPOFSEAT_PCEXIT") // stand up
								ELSE							
									IF CAN_USE_TV_FROM_BOARDROOM_TABLE()																						
										REGISTER_CONTEXT_INTENTION(sBoardroomSeatingData.iBoardroomSeatContextIntention, CP_HIGH_PRIORITY, "FHQ_BR_TV_PC") // stand up + direction + TV
									ELSE
										REGISTER_CONTEXT_INTENTION(sBoardroomSeatingData.iBoardroomSeatContextIntention, CP_HIGH_PRIORITY, "MPOFSEAT_EX_PCC") // stand up + direction
									ENDIF
								ENDIF																
							ELSE																												
								IF sBoardroomSeatingData.iBoardroomSeatID = 3
								AND NOT IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
								AND NOT IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
								AND NOT IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())
								AND NOT IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())
									REGISTER_CONTEXT_INTENTION(sBoardroomSeatingData.iBoardroomSeatContextIntention, CP_HIGH_PRIORITY, "MPOFSEAT_EXIT") // stand up
								ELSE							
									IF CAN_USE_TV_FROM_BOARDROOM_TABLE()																						
										REGISTER_CONTEXT_INTENTION(sBoardroomSeatingData.iBoardroomSeatContextIntention, CP_HIGH_PRIORITY, "FHQ_BR_TV") // stand up + direction + TV
									ELSE
										REGISTER_CONTEXT_INTENTION(sBoardroomSeatingData.iBoardroomSeatContextIntention, CP_HIGH_PRIORITY, "MPOFSEAT_EXIT_C") // stand up + direction
									ENDIF
								ENDIF																							
							ENDIF
						ENDIF
					ELSE
						CONTROL_ACTION exitControlAction
						IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
							exitControlAction = INPUT_SCRIPT_RRIGHT
						ELSE
							exitControlAction = INPUT_FRONTEND_RIGHT
						ENDIF
						
						IF CAN_USE_TV_FROM_BOARDROOM_TABLE()
							CONTROL_ACTION tvControlAction
							IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
								tvControlAction = INPUT_SCRIPT_RLEFT
							ELSE
								tvControlAction = INPUT_FRONTEND_LEFT
							ENDIF
							
							IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, tvControlAction)
								SET_BIT(sBoardroomSeatingData.iBoardroomSeatBS, BOARDROOM_SEATING_BS_USING_TV_CONTROLS)
								
								CLIENT_SETUP_TV_ACTIVATING(sMPTVLocal, sMPTVLocal.eCurrentChannel)
								
								SET_MP_TV_CLIENT_STAGE(playerBD[NATIVE_TO_INT(PLAYER_ID())].MPTVClient, MP_TV_CLIENT_STAGE_ACTIVATED)
								
								RELEASE_CONTEXT_INTENTION(sBoardroomSeatingData.iBoardroomSeatContextIntention)
								sBoardroomSeatingData.iBoardroomSeatContextIntention = NEW_CONTEXT_INTENTION
								
								EXIT
							ENDIF
						ENDIF
						
						INT iLeftX
						iLeftX = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_MOVE_LR)
						
						IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
							exitControlAction = INPUT_SCRIPT_RRIGHT
						ELSE
							exitControlAction = INPUT_FRONTEND_RIGHT
						ENDIF
						
						IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, exitControlAction)
							IF sBoardroomSeatingData.iBoardroomSeatID = 3
							AND NOT IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
							AND NOT IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
							AND NOT IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())
							AND NOT IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())
								sPlayerClip = "exit_b"
								sChairClip = "exit_b_chair"
							ELSE
								IF IS_BIT_SET(sBoardroomSeatingData.iBoardroomSeatBS, BOARDROOM_SEATING_BS_TURNED_LEFT)
									sPlayerClip = "exit_seat"
									sChairClip = "exit_seat_chair"
									eDirection = BOARDROOM_SEATING_DIRECTION_LEFT
								ELIF IS_BIT_SET(sBoardroomSeatingData.iBoardroomSeatBS, BOARDROOM_SEATING_BS_TURNED_RIGHT)
									sPlayerClip = "exit_seat"
									sChairClip = "exit_seat_chair"
									eDirection = BOARDROOM_SEATING_DIRECTION_RIGHT
								ELSE
									sPlayerClip = "exit"
									sChairClip = "exit_chair"
									eDirection = BOARDROOM_SEATING_DIRECTION_CENTER
								ENDIF
							ENDIF
							
							sBoardroomSeatingData.iBoardroomSeatSyncSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(GET_ENTITY_COORDS(NET_TO_OBJ(serverBD.objSeats[sBoardroomSeatingData.iBoardroomSeatID])), GET_ENTITY_ROTATION(NET_TO_OBJ(serverBD.objSeats[sBoardroomSeatingData.iBoardroomSeatID])))
							
							NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), sBoardroomSeatingData.iBoardroomSeatSyncSceneID, GET_BOARDROOM_SEATING_ANIM_DICT(sBoardroomSeatingData.iBoardroomSeatID, sBoardroomSeatingData.eBoardroomSeatingVariation, eDirection), sPlayerClip, REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT)
							NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_OBJ(serverBD.objSeats[sBoardroomSeatingData.iBoardroomSeatID]), sBoardroomSeatingData.iBoardroomSeatSyncSceneID, GET_BOARDROOM_SEATING_ANIM_DICT(sBoardroomSeatingData.iBoardroomSeatID, sBoardroomSeatingData.eBoardroomSeatingVariation, eDirection), sChairClip, REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS)
							
							NETWORK_START_SYNCHRONISED_SCENE(sBoardroomSeatingData.iBoardroomSeatSyncSceneID)
							
							RELEASE_CONTEXT_INTENTION(sBoardroomSeatingData.iBoardroomSeatContextIntention)
							sBoardroomSeatingData.iBoardroomSeatContextIntention = NEW_CONTEXT_INTENTION
							
							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
								TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
							ENDIF
							
							IF CAN_USE_TV_FROM_BOARDROOM_TABLE()
								SET_MP_TV_CLIENT_STAGE(playerBD[NATIVE_TO_INT(PLAYER_ID())].MPTVClient, MP_TV_CLIENT_STAGE_WALKING)
								
								CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].MPTVClient.iBitset, MPTV_CLIENT_BS_OCCUPYING_SEAT)
								
								playerBD[NATIVE_TO_INT(PLAYER_ID())].MPTVClient.iSeat = -1
								
								SET_MP_TV_CLIENT_STAGE(playerBD[NATIVE_TO_INT(PLAYER_ID())].MPTVClient, MP_TV_CLIENT_STAGE_WALKING)
								
								RELEASE_CONTEXT_INTENTION(sMPTVLocal.iContextButtonIntention)
							ENDIF
							
							SET_BOARDROOM_SEATING_STATE(BOARDROOM_SEATING_STATE_EXITING)
						ELIF sBoardroomSeatingData.iBoardroomSeatID != 3
						OR IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
						OR IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
						OR IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())
						OR IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())
							IF iLeftX < 100
							AND NOT IS_BIT_SET(sBoardroomSeatingData.iBoardroomSeatBS, BOARDROOM_SEATING_BS_TURNED_LEFT)
								IF IS_BIT_SET(sBoardroomSeatingData.iBoardroomSeatBS, BOARDROOM_SEATING_BS_TURNED_RIGHT)
									sBoardroomSeatingData.iBoardroomSeatSyncSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(GET_ENTITY_COORDS(NET_TO_OBJ(serverBD.objSeats[sBoardroomSeatingData.iBoardroomSeatID])), GET_ENTITY_ROTATION(NET_TO_OBJ(serverBD.objSeats[sBoardroomSeatingData.iBoardroomSeatID])), DEFAULT, TRUE)
									
									NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), sBoardroomSeatingData.iBoardroomSeatSyncSceneID, GET_BOARDROOM_SEATING_ANIM_DICT(sBoardroomSeatingData.iBoardroomSeatID, sBoardroomSeatingData.eBoardroomSeatingVariation, BOARDROOM_SEATING_DIRECTION_RIGHT), "exit", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT)
									NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_OBJ(serverBD.objSeats[sBoardroomSeatingData.iBoardroomSeatID]), sBoardroomSeatingData.iBoardroomSeatSyncSceneID, GET_BOARDROOM_SEATING_ANIM_DICT(sBoardroomSeatingData.iBoardroomSeatID, sBoardroomSeatingData.eBoardroomSeatingVariation, BOARDROOM_SEATING_DIRECTION_RIGHT), "exit_chair", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS)
									
									NETWORK_START_SYNCHRONISED_SCENE(sBoardroomSeatingData.iBoardroomSeatSyncSceneID)
									
									CLEAR_BIT(sBoardroomSeatingData.iBoardroomSeatBS, BOARDROOM_SEATING_BS_TURNED_LEFT)
									CLEAR_BIT(sBoardroomSeatingData.iBoardroomSeatBS, BOARDROOM_SEATING_BS_TURNED_RIGHT)
								ELSE
									sBoardroomSeatingData.iBoardroomSeatSyncSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(GET_ENTITY_COORDS(NET_TO_OBJ(serverBD.objSeats[sBoardroomSeatingData.iBoardroomSeatID])), GET_ENTITY_ROTATION(NET_TO_OBJ(serverBD.objSeats[sBoardroomSeatingData.iBoardroomSeatID])), DEFAULT, TRUE)
									
									NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), sBoardroomSeatingData.iBoardroomSeatSyncSceneID, GET_BOARDROOM_SEATING_ANIM_DICT(sBoardroomSeatingData.iBoardroomSeatID, sBoardroomSeatingData.eBoardroomSeatingVariation, BOARDROOM_SEATING_DIRECTION_LEFT), "enter", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT)
									NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_OBJ(serverBD.objSeats[sBoardroomSeatingData.iBoardroomSeatID]), sBoardroomSeatingData.iBoardroomSeatSyncSceneID, GET_BOARDROOM_SEATING_ANIM_DICT(sBoardroomSeatingData.iBoardroomSeatID, sBoardroomSeatingData.eBoardroomSeatingVariation, BOARDROOM_SEATING_DIRECTION_LEFT), "enter_chair", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS)
									
									NETWORK_START_SYNCHRONISED_SCENE(sBoardroomSeatingData.iBoardroomSeatSyncSceneID)
									
									SET_BIT(sBoardroomSeatingData.iBoardroomSeatBS, BOARDROOM_SEATING_BS_TURNED_LEFT)
									CLEAR_BIT(sBoardroomSeatingData.iBoardroomSeatBS, BOARDROOM_SEATING_BS_TURNED_RIGHT)
								ENDIF
								
								IF NOT IS_PED_INJURED(PLAYER_PED_ID())
									TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
								ENDIF
								
								RELEASE_CONTEXT_INTENTION(sBoardroomSeatingData.iBoardroomSeatContextIntention)
								sBoardroomSeatingData.iBoardroomSeatContextIntention = NEW_CONTEXT_INTENTION
								
								SET_BOARDROOM_SEATING_STATE(BOARDROOM_SEATING_STATE_TURNING)
							ELIF iLeftX > 150
							AND NOT IS_BIT_SET(sBoardroomSeatingData.iBoardroomSeatBS, BOARDROOM_SEATING_BS_TURNED_RIGHT)
								IF IS_BIT_SET(sBoardroomSeatingData.iBoardroomSeatBS, BOARDROOM_SEATING_BS_TURNED_LEFT)
									sBoardroomSeatingData.iBoardroomSeatSyncSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(GET_ENTITY_COORDS(NET_TO_OBJ(serverBD.objSeats[sBoardroomSeatingData.iBoardroomSeatID])), GET_ENTITY_ROTATION(NET_TO_OBJ(serverBD.objSeats[sBoardroomSeatingData.iBoardroomSeatID])), DEFAULT, TRUE)
									
									NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), sBoardroomSeatingData.iBoardroomSeatSyncSceneID, GET_BOARDROOM_SEATING_ANIM_DICT(sBoardroomSeatingData.iBoardroomSeatID, sBoardroomSeatingData.eBoardroomSeatingVariation, BOARDROOM_SEATING_DIRECTION_LEFT), "exit", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT)
									NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_OBJ(serverBD.objSeats[sBoardroomSeatingData.iBoardroomSeatID]), sBoardroomSeatingData.iBoardroomSeatSyncSceneID, GET_BOARDROOM_SEATING_ANIM_DICT(sBoardroomSeatingData.iBoardroomSeatID, sBoardroomSeatingData.eBoardroomSeatingVariation, BOARDROOM_SEATING_DIRECTION_LEFT), "exit_chair", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS)
									
									NETWORK_START_SYNCHRONISED_SCENE(sBoardroomSeatingData.iBoardroomSeatSyncSceneID)
									
									CLEAR_BIT(sBoardroomSeatingData.iBoardroomSeatBS, BOARDROOM_SEATING_BS_TURNED_LEFT)
									CLEAR_BIT(sBoardroomSeatingData.iBoardroomSeatBS, BOARDROOM_SEATING_BS_TURNED_RIGHT)
								ELSE
									sBoardroomSeatingData.iBoardroomSeatSyncSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(GET_ENTITY_COORDS(NET_TO_OBJ(serverBD.objSeats[sBoardroomSeatingData.iBoardroomSeatID])), GET_ENTITY_ROTATION(NET_TO_OBJ(serverBD.objSeats[sBoardroomSeatingData.iBoardroomSeatID])), DEFAULT, TRUE)
									
									NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), sBoardroomSeatingData.iBoardroomSeatSyncSceneID, GET_BOARDROOM_SEATING_ANIM_DICT(sBoardroomSeatingData.iBoardroomSeatID, sBoardroomSeatingData.eBoardroomSeatingVariation, BOARDROOM_SEATING_DIRECTION_RIGHT), "enter", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT)
									NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_OBJ(serverBD.objSeats[sBoardroomSeatingData.iBoardroomSeatID]), sBoardroomSeatingData.iBoardroomSeatSyncSceneID, GET_BOARDROOM_SEATING_ANIM_DICT(sBoardroomSeatingData.iBoardroomSeatID, sBoardroomSeatingData.eBoardroomSeatingVariation, BOARDROOM_SEATING_DIRECTION_RIGHT), "enter_chair", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS)
									
									NETWORK_START_SYNCHRONISED_SCENE(sBoardroomSeatingData.iBoardroomSeatSyncSceneID)
									
									CLEAR_BIT(sBoardroomSeatingData.iBoardroomSeatBS, BOARDROOM_SEATING_BS_TURNED_LEFT)
									SET_BIT(sBoardroomSeatingData.iBoardroomSeatBS, BOARDROOM_SEATING_BS_TURNED_RIGHT)
								ENDIF
								
								IF NOT IS_PED_INJURED(PLAYER_PED_ID())
									TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
								ENDIF
								
								RELEASE_CONTEXT_INTENTION(sBoardroomSeatingData.iBoardroomSeatContextIntention)
								sBoardroomSeatingData.iBoardroomSeatContextIntention = NEW_CONTEXT_INTENTION
								
								SET_BOARDROOM_SEATING_STATE(BOARDROOM_SEATING_STATE_TURNING)
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF sBoardroomSeatingData.iBoardroomSeatContextIntention != NEW_CONTEXT_INTENTION
						RELEASE_CONTEXT_INTENTION(sBoardroomSeatingData.iBoardroomSeatContextIntention)
						sBoardroomSeatingData.iBoardroomSeatContextIntention = NEW_CONTEXT_INTENTION
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE BOARDROOM_SEATING_STATE_TURNING
			IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.objSeats[sBoardroomSeatingData.iBoardroomSeatID])
				NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.objSeats[sBoardroomSeatingData.iBoardroomSeatID])
			ENDIF
			
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.objSeats[sBoardroomSeatingData.iBoardroomSeatID])
				iBoardroomSeatingLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sBoardroomSeatingData.iBoardroomSeatSyncSceneID)
				
				IF iBoardroomSeatingLocalSceneID = -1
				OR GET_SYNCHRONIZED_SCENE_PHASE(iBoardroomSeatingLocalSceneID) >= 0.99
					sBoardroomSeatingData.iBoardroomSeatSyncSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(GET_ENTITY_COORDS(NET_TO_OBJ(serverBD.objSeats[sBoardroomSeatingData.iBoardroomSeatID])), GET_ENTITY_ROTATION(NET_TO_OBJ(serverBD.objSeats[sBoardroomSeatingData.iBoardroomSeatID])), DEFAULT, TRUE)
					
					IF IS_BIT_SET(sBoardroomSeatingData.iBoardroomSeatBS, BOARDROOM_SEATING_BS_TURNED_LEFT)
						NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), sBoardroomSeatingData.iBoardroomSeatSyncSceneID, GET_BOARDROOM_SEATING_ANIM_DICT(sBoardroomSeatingData.iBoardroomSeatID, sBoardroomSeatingData.eBoardroomSeatingVariation, BOARDROOM_SEATING_DIRECTION_LEFT), "base", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT)
						NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_OBJ(serverBD.objSeats[sBoardroomSeatingData.iBoardroomSeatID]), sBoardroomSeatingData.iBoardroomSeatSyncSceneID, GET_BOARDROOM_SEATING_ANIM_DICT(sBoardroomSeatingData.iBoardroomSeatID, sBoardroomSeatingData.eBoardroomSeatingVariation, BOARDROOM_SEATING_DIRECTION_LEFT), "base_chair", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS)
					ELIF IS_BIT_SET(sBoardroomSeatingData.iBoardroomSeatBS, BOARDROOM_SEATING_BS_TURNED_RIGHT)
						NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), sBoardroomSeatingData.iBoardroomSeatSyncSceneID, GET_BOARDROOM_SEATING_ANIM_DICT(sBoardroomSeatingData.iBoardroomSeatID, sBoardroomSeatingData.eBoardroomSeatingVariation, BOARDROOM_SEATING_DIRECTION_RIGHT), "base", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT)
						NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_OBJ(serverBD.objSeats[sBoardroomSeatingData.iBoardroomSeatID]), sBoardroomSeatingData.iBoardroomSeatSyncSceneID, GET_BOARDROOM_SEATING_ANIM_DICT(sBoardroomSeatingData.iBoardroomSeatID, sBoardroomSeatingData.eBoardroomSeatingVariation, BOARDROOM_SEATING_DIRECTION_RIGHT), "base_chair", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS)
					ELSE
						NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), sBoardroomSeatingData.iBoardroomSeatSyncSceneID, GET_BOARDROOM_SEATING_ANIM_DICT(sBoardroomSeatingData.iBoardroomSeatID, sBoardroomSeatingData.eBoardroomSeatingVariation), "base", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT)
						NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_OBJ(serverBD.objSeats[sBoardroomSeatingData.iBoardroomSeatID]), sBoardroomSeatingData.iBoardroomSeatSyncSceneID, GET_BOARDROOM_SEATING_ANIM_DICT(sBoardroomSeatingData.iBoardroomSeatID, sBoardroomSeatingData.eBoardroomSeatingVariation), "base_chair", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS)
					ENDIF
					
					NETWORK_START_SYNCHRONISED_SCENE(sBoardroomSeatingData.iBoardroomSeatSyncSceneID)
					
					SET_BOARDROOM_SEATING_STATE(BOARDROOM_SEATING_STATE_IDLE)
				ENDIF
			ENDIF
		BREAK
		
		CASE BOARDROOM_SEATING_STATE_EXITING
			iBoardroomSeatingLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sBoardroomSeatingData.iBoardroomSeatSyncSceneID)
			
			IF iBoardroomSeatingLocalSceneID = -1
			OR GET_SYNCHRONIZED_SCENE_PHASE(iBoardroomSeatingLocalSceneID) >= GET_BOARDROOM_SEAT_CLEANUP_PHASE()
				CLEANUP_BOARDROOM_SEATING()
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

FUNC MODEL_NAMES GET_CHAIR_MODEL()
	IF IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
	OR IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
		RETURN INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_clubhouse_chair_01")) // Rough looking basic chairs
	ELIF IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())
		RETURN INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("tr_Prop_Tr_Chair_01a")) // Fancier looking basic chairs
	ENDIF
	
	RETURN INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("vw_prop_vw_offchair_01"))	// Big & fancy Office Chairs
ENDFUNC

PROC CREATE_BOARDROOM_SEATING_PROPS()
	MODEL_NAMES eChairModel = GET_CHAIR_MODEL()
	
	INT i
	REPEAT iNumberOfSeats i
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.objSeats[i])
			REQUEST_MODEL(eChairModel)
			
			IF NOT HAS_MODEL_LOADED(eChairModel)
				EXIT
			ENDIF
			
			REQUEST_ANIM_DICT(GET_BOARDROOM_SEATING_ANIM_DICT(i))
			
			IF NOT HAS_ANIM_DICT_LOADED(GET_BOARDROOM_SEATING_ANIM_DICT(i))
				EXIT
			ENDIF
			
			OBJECT_INDEX objTempSeat
			
			IF CAN_REGISTER_MISSION_OBJECTS(1)
				VECTOR vSeatPosition, vSeatRotation
				vSeatPosition = GET_ANIM_INITIAL_OFFSET_POSITION(GET_BOARDROOM_SEATING_ANIM_DICT(i), "enter_chair", GET_BOARDROOM_SEATING_SCENE_POSITION(i, TRUE), GET_BOARDROOM_SEATING_SCENE_ROTATION(i))
				vSeatRotation = GET_ANIM_INITIAL_OFFSET_ROTATION(GET_BOARDROOM_SEATING_ANIM_DICT(i), "enter_chair", GET_BOARDROOM_SEATING_SCENE_POSITION(i, TRUE), GET_BOARDROOM_SEATING_SCENE_ROTATION(i))
				
				objTempSeat = CREATE_OBJECT(eChairModel, vSeatPosition)
				
				SET_MODEL_AS_NO_LONGER_NEEDED(eChairModel)
				SET_ENTITY_CAN_BE_DAMAGED(objTempSeat, FALSE)
				NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(objTempSeat, TRUE)
				SET_ENTITY_CAN_ONLY_BE_DAMAGED_BY_SCRIPT_PARTICIPANTS(objTempSeat, TRUE)
				SET_ENTITY_COORDS_NO_OFFSET(objTempSeat, vSeatPosition)
				SET_ENTITY_ROTATION(objTempSeat, vSeatRotation)
				FREEZE_ENTITY_POSITION(objTempSeat, TRUE)
				
				serverBD.objSeats[i] = OBJ_TO_NET(objTempSeat)
				
				REMOVE_ANIM_DICT(GET_BOARDROOM_SEATING_ANIM_DICT(i))
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC MAINTAIN_FACILITY_MP_TV_CLIENT()
	IF playerBD[NATIVE_TO_INT(PLAYER_ID())].MPTVClient.eStage <> MP_TV_CLIENT_STAGE_INIT
	AND sBoardroomSeatingData.eCurrentState >= BOARDROOM_SEATING_STATE_IDLE
		IF NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].MPTVClient.iBitset, MPTV_CLIENT_BS_OCCUPYING_SEAT)
		AND sBoardroomSeatingData.eCurrentState != BOARDROOM_SEATING_STATE_EXITING
			SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].MPTVClient.iBitset, MPTV_CLIENT_BS_OCCUPYING_SEAT)
			
			CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].MPTVClient.iBitset, MPTV_CLIENT_BS_LEAVING_SEAT)
			
			playerBD[NATIVE_TO_INT(PLAYER_ID())].MPTVClient.iSeat = sBoardroomSeatingData.iBoardroomSeatID
			
			SET_MP_TV_CLIENT_STAGE(playerBD[NATIVE_TO_INT(PLAYER_ID())].MPTVClient, MP_TV_CLIENT_STAGE_SITTING)
		ENDIF
		
		IF playerBD[NATIVE_TO_INT(PLAYER_ID())].MPTVClient.eStage != MP_TV_CLIENT_STAGE_ACTIVATED
		AND IS_BIT_SET(sBoardroomSeatingData.iBoardroomSeatBS, BOARDROOM_SEATING_BS_USING_TV_CONTROLS)
			CLEAR_BIT(sBoardroomSeatingData.iBoardroomSeatBS, BOARDROOM_SEATING_BS_USING_TV_CONTROLS)
		ENDIF
	ENDIF
	
	CLIENT_MAINTAIN_MP_TV(serverBD.MPTVServer, playerBD[NATIVE_TO_INT(PLAYER_ID())].MPTVClient, sMPTVLocal, InteriorPropStruct)
ENDPROC

SCRIPT
	IF NETWORK_IS_GAME_IN_PROGRESS()
		PROCESS_PRE_GAME()
	ELSE
		SCRIPT_CLEANUP()
	ENDIF
	
	WHILE TRUE 
		MP_LOOP_WAIT_ZERO()
		
		IF SHOULD_TERMINATE_SCRIPT()
			SCRIPT_CLEANUP()
		ENDIF
		
		MAINTAIN_BOARDROOM_SEATING()
		
		IF CAN_USE_TV_FROM_BOARDROOM_TABLE()
			MAINTAIN_FACILITY_MP_TV_CLIENT() 
		ENDIF
		
		IF NETWORK_IS_GAME_IN_PROGRESS()
		AND NETWORK_IS_HOST_OF_THIS_SCRIPT()
			CREATE_BOARDROOM_SEATING_PROPS()
			
			IF CAN_USE_TV_FROM_BOARDROOM_TABLE()
				SERVER_MAINTAIN_MP_TV(serverBD.MPTVServer, playerBD[sMPTVLocal.iServerPlayerStagger].MPTVClient, sMPTVLocal, InteriorPropStruct)
			ENDIF
		ENDIF
		
	ENDWHILE
	
ENDSCRIPT
