//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        AM_MP_VEH_WEAPON.sc																					//
// Description:  This script handles vehicle weapons such as armory hacker truck property homing missiles.			//
//																													//
//																													//
// Written by:  Ata																									//
// Date:  		17/04/2018																							//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_event.sch"
USING "net_include.sch"
USING "net_events.sch"
USING "net_spawn.sch"
USING "net_ambience.sch"
USING "net_wait_zero.sch"
USING "freemode_events_header.sch"

#IF IS_DEBUG_BUILD
USING "net_debug_log.sch"
#ENDIF



USING "net_vehicle_weapon.sch"


//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════════════════╡ CONSTS ╞═════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

// Shape Test Stages
CONST_INT VEHICLE_WEAPON_SHAPETEST_INIT							0
CONST_INT VEHICLE_WEAPON_SHAPETEST_PROCESS						1
CONST_INT VEHICLE_WEAPON_SHAPETEST_READY						2

// Weapon target one Stage
CONST_INT VEHICLE_WEAPON_TARGET_CHECK_INIT						0
CONST_INT VEHICLE_WEAPON_TARGET_CHECK_NOT_HIT_ANYTHING			1
CONST_INT VEHICLE_WEAPON_TARGET_CHECK_HIT_SOMETHING				2

// iLocalBS
CONST_INT VEHICLE_WEAPON_LOCAL_HIT_NEW_TARGET					0
CONST_INT VEHICLE_WEAPON_LOCAL_TOOK_A_SHOOT						1
CONST_INT VEHICLE_WEAPON_LOCAL_UPDATE_FIRST_CAM					2
CONST_INT VEHICLE_WEAPON_LOCAL_SHOT_ALL_TARGETS					3
CONST_INT VEHICLE_WEAPON_LOCAL_BS_START_INSTRUCTIONAL_PROMPT	4
CONST_INT VEHICLE_WEAPON_LOCAL_BS_START_FIRING_HOMMING_MISSILE	5
CONST_INT VEHICLE_WEAPON_LOCAL_BS_SHOULD_SHOW_PC_PROMPTS		6
CONST_INT VEHICLE_WEAPON_LOCAL_BS_STARTED_FROM_INTERIOR			7


//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ DATA STRUCTURES ╞═════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

STRUCT ServerBroadcastData
	INT iServerBS
ENDSTRUCT

STRUCT PlayerBroadcastData
	INT iLocalBS
ENDSTRUCT

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡    VARIABLES    ╞═════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]
ServerBroadcastData serverBD

VEHICLE_WEAPON_STRUCT sVehWeaponStruct

#IF IS_DEBUG_BUILD
FLOAT fPropCamOffsetX, fPropCamOffsetY, fPropCamOffsetZ
#ENDIF

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════════╡    CLEANUP   ╞═════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛
PROC CLEANUP_VEHCILE_WEAPON_CAMERA()
	IF DOES_CAM_EXIST(sVehWeaponStruct.VehWeaponCamera)
		DESTROY_CAM(sVehWeaponStruct.VehWeaponCamera)
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		CLEAR_TIMECYCLE_MODIFIER()
	ENDIF
ENDPROC

PROC STOP_CAM_PAN_LOOP_SOUND()
	IF sVehWeaponStruct.iPanLoopSoundID = -1
		STOP_SOUND(sVehWeaponStruct.iPanLoopSoundID)
		RELEASE_SOUND_ID(sVehWeaponStruct.iPanLoopSoundID)
		sVehWeaponStruct.iPanLoopSoundID = -1
		PRINTLN("[AM_MP_VEH_WEAPON] HANDLE_AVENGER_SOUNDS UPDATED STOP")
	ENDIF
ENDPROC 

#IF IS_DEBUG_BUILD
PROC CREATE_DEBUG_WIDGET()
	START_WIDGET_GROUP("Vehicle Weapon")
		
		START_WIDGET_GROUP("Camera offset")
			ADD_WIDGET_FLOAT_SLIDER("Camera X offset Limit", fPropCamOffsetX, -100, 100, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Camera Y offset Limit", fPropCamOffsetY, -100, 100, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Camera Z offset Limit", fPropCamOffsetZ, -100, 100, 0.1)
		STOP_WIDGET_GROUP()

		START_WIDGET_GROUP("Start Vehicle Weapon")
			ADD_WIDGET_BOOL("Start vehicle weapon", g_bStartVehWeaponScript)
			ADD_WIDGET_BOOL("Clean up vehicle weapon", g_bCleanupehWeaponScript)
		STOP_WIDGET_GROUP()

	STOP_WIDGET_GROUP()
ENDPROC

FUNC STRING GET_VEH_WEAPON_STATE_NAME(VEH_WEAPON_STATE eState)
	
	SWITCH eState
		CASE VEH_WEAPON_STATE_IDLE 			RETURN "VEH_WEAPON_STATE_IDLE" 
		CASE VEH_WEAPON_STATE_INIT			RETURN "VEH_WEAPON_STATE_INIT" 
		CASE VEH_WEAPON_STATE_UPDATE		RETURN "VEH_WEAPON_STATE_UPDATE" 					
		CASE VEH_WEAPON_STATE_CLEANUP		RETURN "VEH_WEAPON_STATE_CLEANUP" 
	ENDSWITCH
	
	RETURN "INVALID STATE"
	
ENDFUNC
#ENDIF

PROC SET_VEH_WEAPON_STATE(VEH_WEAPON_STATE eState)
	IF sVehWeaponStruct.eVehWeaponState  != eState
		sVehWeaponStruct.eVehWeaponState  = eState
		#IF IS_DEBUG_BUILD
		PRINTLN("[AM_MP_VEH_WEAPON] - SET_VEHICLE_WEAPON_STATE: ", GET_VEH_WEAPON_STATE_NAME(eState))
		#ENDIF
	ENDIF
ENDPROC

PROC SET_SCRIPT_STARTED_FROM_INTERIOR(BOOL bStartFromInterior)
	IF bStartFromInterior
		IF NOT IS_BIT_SET(sVehWeaponStruct.iLocalBS, VEHICLE_WEAPON_LOCAL_BS_STARTED_FROM_INTERIOR)
			SET_BIT(sVehWeaponStruct.iLocalBS, VEHICLE_WEAPON_LOCAL_BS_STARTED_FROM_INTERIOR)
			PRINTLN("[AM_MP_VEH_WEAPON] - SET_SCRIPT_STARTED_FROM_INTERIOR TRUE")
		ENDIF	
	ELSE
		IF IS_BIT_SET(sVehWeaponStruct.iLocalBS, VEHICLE_WEAPON_LOCAL_BS_STARTED_FROM_INTERIOR)
			CLEAR_BIT(sVehWeaponStruct.iLocalBS, VEHICLE_WEAPON_LOCAL_BS_STARTED_FROM_INTERIOR)
			PRINTLN("[AM_MP_VEH_WEAPON] - SET_SCRIPT_STARTED_FROM_INTERIOR FALSE")
		ENDIF	
	ENDIF
ENDPROC	

FUNC BOOL IS_SCRIPT_STARTED_FROM_INTERIOR()
	RETURN IS_BIT_SET(sVehWeaponStruct.iLocalBS, VEHICLE_WEAPON_LOCAL_BS_STARTED_FROM_INTERIOR)
ENDFUNC 

FUNC STRING GET_VEHICLE_WEAPON_SCALEFORM_NAME()
	IF IS_SCRIPT_STARTED_FROM_INTERIOR()
		RETURN "DRONE_CAM"
	ELSE
		RETURN "heli_cam"
	ENDIF
	
	RETURN ""
ENDFUNC 

PROC INITIALISE(BOOL bCreateWidget = TRUE)
	
	sVehWeaponStruct.iMissileFireMeter 	= 100
	sVehWeaponStruct.fBaseCamFov		= 50
	sVehWeaponStruct.vehWeaponSF 		= REQUEST_SCALEFORM_MOVIE(GET_VEHICLE_WEAPON_SCALEFORM_NAME())
	
	REQUEST_AMBIENT_AUDIO_BANK("SCRIPT\\POLICE_CHOPPER_CAM")
	REQUEST_STREAMED_TEXTURE_DICT("helicopterhud")	
	REQUEST_STREAMED_TEXTURE_DICT("droneHUD")

	sVehWeaponStruct.iZoomSound = GET_SOUND_ID()
	sVehWeaponStruct.iPanSound = GET_SOUND_ID()
	
	SET_VEH_WEAPON_STATE(VEH_WEAPON_STATE_IDLE)	
	
	INT i 
	FOR i = 0 TO MAX_NUM_TARGET_VEH - 1 
		sVehWeaponStruct.iAcquiringSoundID[i] = -1
		sVehWeaponStruct.iAcquiredTargetSoundID[i] = -1
	ENDFOR
	
	IF bCreateWidget
		#IF IS_DEBUG_BUILD
		CREATE_DEBUG_WIDGET()
		#ENDIF
	ENDIF	
ENDPROC

#IF FEATURE_GEN9_EXCLUSIVE
PROC SET_AUDIO_RAIN_HAPTIC_FLAG(BOOL bSet)
	IF bSet
		SET_AUDIO_FLAG("DisableRainHaptics", TRUE)
	ELSE
		SET_AUDIO_FLAG("DisableRainHaptics", FALSE)
	ENDIF
ENDPROC
#ENDIF

PROC STOP_VEHICLE_WEAPON_SOUNDS()

	IF sVehWeaponStruct.iPanSound != -1
		STOP_SOUND(sVehWeaponStruct.iPanSound)
		RELEASE_SOUND_ID(sVehWeaponStruct.iPanSound)
		sVehWeaponStruct.iPanSound = -1
	ENDIF
	
	IF sVehWeaponStruct.iZoomSound != -1
		STOP_SOUND(sVehWeaponStruct.iZoomSound)
		RELEASE_SOUND_ID(sVehWeaponStruct.iZoomSound)
		sVehWeaponStruct.iZoomSound = -1
	ENDIF
	
	IF sVehWeaponStruct.iBackgroundSoundID != -1
		STOP_SOUND(sVehWeaponStruct.iBackgroundSoundID)
		RELEASE_SOUND_ID(sVehWeaponStruct.iBackgroundSoundID)
		sVehWeaponStruct.iBackgroundSoundID = -1
	ENDIF
	
	STOP_AUDIO_SCENE("DLC_GR_MOC_Turret_View_Scene")
	RELEASE_AMBIENT_AUDIO_BANK()	
ENDPROC

PROC STOP_TARGET_ACQUIRING_SOUND(INT iSoundIndex, BOOL bClearFlag = TRUE)
	IF IS_BIT_SET(sVehWeaponStruct.iTargetSoundBS, iSoundIndex)
		IF !HAS_SOUND_FINISHED(sVehWeaponStruct.iAcquiringSoundID[iSoundIndex])
			STOP_SOUND(sVehWeaponStruct.iAcquiringSoundID[iSoundIndex])
			RELEASE_SOUND_ID(sVehWeaponStruct.iAcquiringSoundID[iSoundIndex])
			PRINTLN("STOP_TARGET_ACQUIRING_SOUND STOP SOUND iSoundIndex: ", iSoundIndex)
		ENDIF
		sVehWeaponStruct.iAcquiringSoundID[iSoundIndex] = -1
		IF bClearFlag
			CLEAR_BIT(sVehWeaponStruct.iTargetSoundBS, iSoundIndex)
		ENDIF
		RESET_NET_TIMER(sVehWeaponStruct.sAcquiringTargetSound[iSoundIndex])
	ENDIF
ENDPROC 

PROC STOP_TARGET_ACQUIRIED_SOUND(INT iSoundIndex, BOOL bClearFlag = TRUE)
	IF IS_BIT_SET(sVehWeaponStruct.iTargetSoundBS, iSoundIndex + MAX_NUM_TARGET_VEH)
		IF !HAS_SOUND_FINISHED(sVehWeaponStruct.iAcquiredTargetSoundID[iSoundIndex])
			STOP_SOUND(sVehWeaponStruct.iAcquiredTargetSoundID[iSoundIndex])
			RELEASE_SOUND_ID(sVehWeaponStruct.iAcquiredTargetSoundID[iSoundIndex])
			PRINTLN("STOP_TARGET_ACQUIRIED_SOUND STOP SOUND iSoundIndex: ", iSoundIndex)
		ENDIF
		sVehWeaponStruct.iAcquiredTargetSoundID[iSoundIndex] = -1
		IF bClearFlag
			CLEAR_BIT(sVehWeaponStruct.iTargetSoundBS, iSoundIndex + MAX_NUM_TARGET_VEH)
		ENDIF
		RESET_NET_TIMER(sVehWeaponStruct.sAcquiredTargetSound[iSoundIndex])
	ENDIF
ENDPROC 

PROC STOP_ALL_TARGET_SOUNDS()
	INT iNumTarget
	FOR iNumTarget = 0 TO MAX_NUM_TARGET_VEH - 1
		STOP_TARGET_ACQUIRING_SOUND(iNumTarget)
		STOP_TARGET_ACQUIRIED_SOUND(iNumTarget)
	ENDFOR
ENDPROC

PROC SCRIPT_CLEANUP(BOOL bReset = FALSE)
	
	PRINTLN("[AM_MP_VEH_WEAPON] - SCRIPT_CLEANUP - bReset: ", bReset)
	
	#IF IS_DEBUG_BUILD
		g_bStartVehWeaponScript 	= FALSE
		g_bCleanupehWeaponScript 	= FALSE
	#ENDIF
	
	IF !IS_SCRIPT_STARTED_FROM_INTERIOR()
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HUNTGUN_1b")
			CLEAR_HELP()
		ENDIF
		
		CLEAR_TIMECYCLE_MODIFIER()
	ENDIF	
	
	IF IS_ENTITY_ALIVE(GET_VEHICLE_WEAPON_WEAPONISED_VEHICLE())
		CLEAR_FOCUS()
	ENDIF	
	
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("helicopterhud")
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("droneHUD")
	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sVehWeaponStruct.vehWeaponSF)
	
	CLEANUP_VEHCILE_WEAPON_CAMERA()
	STOP_CAM_PAN_LOOP_SOUND()
	
	IF IS_RADAR_MAP_DISABLED()
		DISABLE_RADAR_MAP(FALSE)
	ENDIF
	
	UNLOCK_MINIMAP_ANGLE() 
	UNLOCK_MINIMAP_POSITION()
	
	IF IS_SKYSWOOP_AT_GROUND()
	AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
		IF NOT IS_PLAYER_TELEPORT_ACTIVE()
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
		ELSE
			PRINTLN("[AM_MP_VEH_WEAPON] - SCRIPT_CLEANUP - player teleport is active is player busted ?")
		ENDIF
	ENDIF
	
	RESET_NET_TIMER(sVehWeaponStruct.sStartVehWeaponCamera)
	RESET_NET_TIMER(sVehWeaponStruct.sHomingMissileTimer)
	RESET_NET_TIMER(sVehWeaponStruct.newTargetRayCastTimer)
	//RESET_NET_TIMER(sVehWeaponStruct.sVariableNetworkTimer)
	RESET_NET_TIMER(sVehWeaponStruct.sHomingMissileCoolDownTimer)
	
	SET_LOCAL_PLAYER_USING_VEHICLE_WEAPON(FALSE)
	SET_PLAYER_USING_VEHICLE_WEAPON(FALSE)
	
	sVehWeaponStruct.iNumShotHoming = -1
	sVehWeaponStruct.iHomingStagger = 0
	sVehWeaponStruct.iLocalBS 		= 0
	sVehWeaponStruct.iNewTargetShapeHitSomthing = VEHICLE_WEAPON_TARGET_CHECK_INIT
	sVehWeaponStruct.iEndTargetCoordShapeHitSomthing = VEHICLE_WEAPON_TARGET_CHECK_INIT
	
	INT i 
	FOR i = 0 TO MAX_NUM_TARGET_VEH - 1 
		sVehWeaponStruct.iTargetShapeHitSomthing[i] = VEHICLE_WEAPON_TARGET_CHECK_INIT
		RESET_NET_TIMER(sVehWeaponStruct.sTargetValidateTime[i])
	ENDFOR
	
	STOP_ALL_TARGET_SOUNDS()
	
	IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bTurretInUse, 0)
		CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bTurretInUse, 0)
	ENDIF	
	
	sVehWeaponStruct.iTargetSoundBS = 0
	
	#IF FEATURE_GEN9_EXCLUSIVE
	SET_AUDIO_RAIN_HAPTIC_FLAG(FALSE)
	#ENDIF
	
	STOP_VEHICLE_WEAPON_SOUNDS()
	
	CLEANUP_MENU_ASSETS()	
	
	IF DOES_BLIP_EXIST(sVehWeaponStruct.ArrowStrokeBlipBg)
		REMOVE_BLIP(sVehWeaponStruct.ArrowStrokeBlipBg)
	ENDIF	
	
	IF DOES_BLIP_EXIST(sVehWeaponStruct.ArrowStrokeBlipID)
		REMOVE_BLIP(sVehWeaponStruct.ArrowStrokeBlipID)
	ENDIF	
	
	IF !bReset
		SET_KILL_VEHICLE_WEAPON_SCRIPT(FALSE)
		TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
	ELSE
		INITIALISE(FALSE)
	ENDIF
	
ENDPROC

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡    PROCEDURES   ╞═════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

FUNC BOOL IS_VEH_WEAPON_STATE(VEH_WEAPON_STATE eState)
	RETURN sVehWeaponStruct.eVehWeaponState = eState
ENDFUNC

#IF IS_DEBUG_BUILD
PROC UPDATE_DEBUG_WIDGETS()
	IF g_bCleanupehWeaponScript
		SCRIPT_CLEANUP()
	ENDIF
	
	IF fPropCamOffsetX != 0.0
	OR fPropCamOffsetY != 0.0
	OR fPropCamOffsetZ != 0.0
		IF DOES_ENTITY_EXIST(GET_VEHICLE_WEAPON_WEAPONISED_VEHICLE())
			ATTACH_CAM_TO_ENTITY(sVehWeaponStruct.VehWeaponCamera, GET_VEHICLE_WEAPON_WEAPONISED_VEHICLE(), <<fPropCamOffsetX,fPropCamOffsetY,fPropCamOffsetZ>>)
		ENDIF	
	ENDIF
	
ENDPROC
#ENDIF

PROC SCRIPT_INITIALISE(MP_MISSION_DATA missionScriptArgs)
	#IF IS_DEBUG_BUILD
	PRINTLN("[AM_MP_VEH_WEAPON] - SCRIPT_INITIALISE - Launching instance: ", missionScriptArgs.iInstanceId)
	#ENDIF
	
	NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE, missionScriptArgs.iInstanceId)
	
	// This makes sure the net script is active, waits untill it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
	
	// KGM: Wait for the first network broadcast before moving on
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		PRINTLN("[AM_MP_VEH_WEAPON] - FAILED TO RECEIVE INITIAL NETWORK BROADCAST - SCRIPT CLEANUP")
		SCRIPT_CLEANUP()
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		// This script will not be paused if another script calls PAUSE_GAME
		PRINTLN("[AM_MP_VEH_WEAPON] - INITIALISED")
	ELSE
		PRINTLN("[AM_MP_VEH_WEAPON] - NETWORK_IS_GAME_IN_PROGRESS = FALSE - SCRIPT CLEANUP")
		SCRIPT_CLEANUP()
	ENDIF
	
	IF missionScriptArgs.iBitSet = 1
		SET_SCRIPT_STARTED_FROM_INTERIOR(TRUE)
	ELSE
		SET_SCRIPT_STARTED_FROM_INTERIOR(FALSE)
	ENDIF
	
	INITIALISE()
ENDPROC

FUNC BOOL SHOULD_PAUSE_VEH_WEAPON()
	IF IS_PAUSE_MENU_ACTIVE()
	OR IS_COMMERCE_STORE_OPEN()
		PRINTLN("[AM_MP_VEH_WEAPON] - SHOULD_PAUSE_VEHICLE_WEAPON - pause or store is open")
		RETURN TRUE
	ENDIF
	
	IF IS_SELECTOR_ONSCREEN()
		PRINTLN("[AM_MP_VEH_WEAPON] - SHOULD_PAUSE_VEHICLE_WEAPON - SHOULD_PAUSE_VEH_WEAPON")
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_SPECTATING(PLAYER_ID())
		PRINTLN("[AM_MP_VEH_WEAPON] - SHOULD_PAUSE_VEHICLE_WEAPON - IS_PLAYER_SPECTATING")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_PLAYING_VEHICLE_WEAPON_ANIMATION()
	IF g_iShouldLaunchTruckTurret != -1
	OR g_iInteriorTurretSeat != -1

		IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@FACILITY@LAUNCH_CONTROLS@", "COMPUTER_idle")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@FACILITY@LAUNCH_CONTROLS@", "COMPUTER_idle_control")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@FACILITY@LAUNCH_CONTROLS@", "COMPUTER_enter_control")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@FACILITY@LAUNCH_CONTROLS@", "COMPUTER_enter")
			CDEBUG1LN(DEBUG_NET_VEH_WEAPON, "[AM_MP_VEH_WEAPON] IS_PLAYER_PLAYING_VEHICLE_WEAPON_ANIMATION: returning TRUE, player is playing computer anim")
			RETURN TRUE
		ELSE
			CDEBUG1LN(DEBUG_NET_VEH_WEAPON, "[AM_MP_VEH_WEAPON] IS_PLAYER_PLAYING_VEHICLE_WEAPON_ANIMATION: returning FALSE, player is NOT playing computer anim")
		ENDIF
	
		IF SHOULD_BAIL_FROM_TURRET()
			CDEBUG1LN(DEBUG_NET_VEH_WEAPON, "[AM_MP_VEH_WEAPON] IS_PLAYER_PLAYING_VEHICLE_WEAPON_ANIMATION: returning false, SHOULD_BAIL_FROM_TURRET ")
			RETURN FALSE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_BLOCK_VEHICLE_WEAPON()
	IF IS_PLAYER_IN_HACKER_TRUCK(PLAYER_ID())
		IF g_sMPTunables.bBB_TERRORBYTE_DISABLE_MISSILE_LAUNCHER 
			PRINTLN("[AM_MP_VEH_WEAPON] SHOULD_BLOCK_VEHICLE_WEAPON: bBB_TERRORBYTE_DISABLE_DISABLE_VEHICLE_WORKSHOP TRUE ")
			RETURN TRUE
		ENDIF	
	ENDIF
	
	IF IS_CUSTOM_MENU_ON_SCREEN()
		PRINTLN("[AM_MP_VEH_WEAPON] SHOULD_BLOCK_VEHICLE_WEAPON - IS_CUSTOM_MENU_ON_SCREEN is true - TRUE")
		RETURN TRUE
	ENDIF
	
	IF IS_INTERACTION_MENU_OPEN()
		PRINTLN("[AM_MP_VEH_WEAPON] SHOULD_BLOCK_VEHICLE_WEAPON - IS_INTERACTION_MENU_OPEN is true - TRUE")
		RETURN TRUE
	ENDIF
	
	IF IS_PAUSE_MENU_ACTIVE()
		PRINTLN("[AM_MP_VEH_WEAPON] SHOULD_BLOCK_VEHICLE_WEAPON - IS_PAUSE_MENU_ACTIVE is true - TRUE")
		RETURN TRUE
	ENDIF
	
	IF IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
		PRINTLN("[AM_MP_VEH_WEAPON] SHOULD_BLOCK_VEHICLE_WEAPON - IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR is true - TRUE")
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_IN_HACKER_TRUCK(PLAYER_ID())
		IF IS_OWNERS_HACKER_TRUCK_INSIDE_BUSINESS_HUB(g_OwnerOfHackerTruckPropertyIAmIn)
			PRINTLN("[AM_MP_VEH_WEAPON] SHOULD_BLOCK_VEHICLE_WEAPON - IS_OWNERS_HACKER_TRUCK_INSIDE_BUSINESS_HUB is true - TRUE")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_PLAYER_IN_PASSIVE_MODE(PLAYER_ID())
		PRINTLN("[AM_MP_VEH_WEAPON] SHOULD_BLOCK_VEHICLE_WEAPON - drone is in cool down - TRUE")
		RETURN TRUE
	ENDIF
	
	IF IS_PHONE_ONSCREEN()
		PRINTLN("[AM_MP_VEH_WEAPON] SHOULD_BLOCK_VEHICLE_WEAPON - IS_PHONE_ONSCREEN - TRUE")
		RETURN TRUE
	ENDIF	
	
	IF IS_CELLPHONE_CAMERA_IN_USE()
		PRINTLN("[AM_MP_VEH_WEAPON] SHOULD_BLOCK_VEHICLE_WEAPON - IS_CELLPHONE_CAMERA_IN_USE - TRUE")
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_CHANGING_CLOTHES()
		PRINTLN("[AM_MP_VEH_WEAPON] SHOULD_BLOCK_VEHICLE_WEAPON - player is changing clothes - TRUE")
		RETURN TRUE
	ENDIF
	
	IF !CAN_CREATE_VEHICLE_WEAPON_CAMERA()
		PRINTLN("[AM_MP_VEH_WEAPON] SHOULD_BLOCK_VEHICLE_WEAPON - CAN_CREATE_VEHICLE_WEAPON_CAMERA false - TRUE")
		RETURN TRUE
	ENDIF
	
	IF IS_VEHICLE_WEAPON_OWNER_TAKING_VEHICLE_IN_OR_OUT_OF_PROPERTY()
		PRINTLN("[AM_MP_VEH_WEAPON] SHOULD_BLOCK_VEHICLE_WEAPON - IS_VEHICLE_WEAPON_OWNER_TAKING_VEHICLE_IN_OR_OUT_OF_PROPERTY true - TRUE")
		RETURN TRUE
	ENDIF
	
	IF g_bBustedWarpInProgress
		PRINTLN("[AM_MP_VEH_WEAPON] SHOULD_BLOCK_VEHICLE_WEAPON - g_bBustedWarpInProgress true - TRUE")
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_IN_HACKER_TRUCK(PLAYER_ID())
		IF IS_ENTITY_ALIVE(GET_HACKER_TRUCK_INDEX_PED_IS_IN_INTERIOR())
			IF IS_ENTITY_IN_ANGLED_AREA(GET_HACKER_TRUCK_INDEX_PED_IS_IN_INTERIOR(), <<-136.161423,4617.175781,124.513405>>, <<-490.305756,4924.749023,159.067673>>, 19.500000)
				PRINTLN("[AM_MP_VEH_WEAPON] - SHOULD_BLOCK_VEHICLE_WEAPON - truck is in tunnel")
				RETURN TRUE
			ENDIF
			IF IS_ENTITY_UPSIDEDOWN(GET_HACKER_TRUCK_INDEX_PED_IS_IN_INTERIOR())
				PRINTLN("[AM_MP_VEH_WEAPON] - SHOULD_BLOCK_VEHICLE_WEAPON - truck is in UPSIDEDOWN")
				RETURN TRUE
			ENDIF	
		ELSE
			PRINTLN("[AM_MP_VEH_WEAPON] - SHOULD_BLOCK_VEHICLE_WEAPON - truck doesn't exist")
			RETURN TRUE
		ENDIF
	ENDIF	
	
	IF IS_VEHICLE_WEAPON_WEAPONISED_VEHICLE_DRIVER_IN_PASSIVE_MODE()
		PRINTLN("[AM_MP_VEH_WEAPON] - SHOULD_BLOCK_VEHICLE_WEAPON - driver in passive mode")
		RETURN TRUE
	ENDIF
	
	IF NOT IS_SCRIPT_STARTED_FROM_INTERIOR()
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())	
			VEHICLE_INDEX playerVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			IF IS_ENTITY_ALIVE(playerVeh)
				VECTOR vCamCoords = GET_ENTITY_COORDS(playerVeh)
				FLOAT fCamZ
				IF GET_GROUND_Z_FOR_3D_COORD(vCamCoords, fCamZ)
					IF VDIST(vCamCoords , <<vCamCoords.x,vCamCoords.y,fCamZ>>) < 3.0
						PRINTLN("[AM_MP_VEH_WEAPON] - SHOULD_BLOCK_VEHICLE_WEAPON - vehicle close to ground - TRUE")
						RETURN TRUE
					ENDIF
				ENDIF	
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_TURRET_PED_LAUNCH_TURRET_HUD()
	IF g_iShouldLaunchTruckTurret != -1
	OR g_iInteriorTurretSeat != -1
		IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@FACILITY@LAUNCH_CONTROLS@", "enter")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@FACILITY@LAUNCH_CONTROLS@", "enter_left")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@FACILITY@LAUNCH_CONTROLS@", "exit")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@FACILITY@LAUNCH_CONTROLS@", "exit_left")
		OR IsPedPerformingTask(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD)
			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "CAN_TURRET_PED_LAUNCH_TURRET_HUD: returning false, player is playing enter anim")
			RETURN FALSE
		ENDIF
		
		IF NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@FACILITY@LAUNCH_CONTROLS@", "base")
		AND NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@FACILITY@LAUNCH_CONTROLS@", "computer_enter")
		AND NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@FACILITY@LAUNCH_CONTROLS@", "computer_exit")
		AND NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@FACILITY@LAUNCH_CONTROLS@", "COMPUTER_idle")
		AND NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@FACILITY@LAUNCH_CONTROLS@", "COMPUTER_idle_control")
		AND NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@FACILITY@LAUNCH_CONTROLS@", "COMPUTER_enter_control")
			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "CAN_TURRET_PED_LAUNCH_TURRET_HUD: returning false, player is not in the seat")
			RETURN FALSE
		ENDIF
		
		IF SHOULD_BAIL_FROM_TURRET()
			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "CAN_TURRET_PED_LAUNCH_TURRET_HUD: returning false, SHOULD_BAIL_FROM_TURRET ")
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_PLAYER_IN_TURRENT_SEAT_IN_HACKER_TRUCK()
	IF IS_PLAYER_IN_HACKER_TRUCK(PLAYER_ID())
	AND g_iShouldLaunchTruckTurret = 2
	AND CAN_TURRET_PED_LAUNCH_TURRET_HUD()
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC


PROC MAINTAIN_VEHICLE_WEAPON_TRIGGER()
	
	#IF IS_DEBUG_BUILD
	IF g_bStartVehWeaponScript
		SET_PLAYER_INITALISING_VEHICLE_WEAPON(TRUE)
		SET_VEH_WEAPON_STATE(VEH_WEAPON_STATE_INIT)
	ENDIF
	#ENDIF
	
	IF NOT SHOULD_PAUSE_VEH_WEAPON()
		IF IS_NET_PLAYER_OK(PLAYER_ID())
			IF !SHOULD_BLOCK_VEHICLE_WEAPON()
				IF IS_SCRIPT_STARTED_FROM_INTERIOR()
					IF IS_PLAYER_IN_TURRENT_SEAT_IN_HACKER_TRUCK()			
					AND NOT IS_PLAYER_PLAYING_VEHICLE_WEAPON_ANIMATION()
					AND IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MP_HTRUCK_T_2")
						IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_CELLPHONE_SELECT)
						OR IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_CELLPHONE_SELECT)
							SET_PLAYER_INITALISING_VEHICLE_WEAPON(TRUE)
							SET_VEH_WEAPON_STATE(VEH_WEAPON_STATE_INIT)
							CLEAR_HELP()
						ENDIF
					ENDIF	
				ELSE
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
						AND NOT IS_HELP_MESSAGE_ON_SCREEN()
						AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HUNTGUN_1b")
							PRINT_HELP("HUNTGUN_1b")
						ELSE
							IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HUNTGUN_1b")
								CLEAR_HELP()
							ELSE
								IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_CONTEXT)
								OR IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_CONTEXT)
									SET_PLAYER_INITALISING_VEHICLE_WEAPON(TRUE)
									SET_VEH_WEAPON_STATE(VEH_WEAPON_STATE_INIT)
									CLEAR_HELP()
								ENDIF
							ENDIF	
						ENDIF
					ELSE
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HUNTGUN_1b")
							CLEAR_HELP()
						ENDIF	
					ENDIF 
				ENDIF
			ELSE
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HUNTGUN_1b")
					CLEAR_HELP()
				ENDIF	
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_VEHICLE_WEAPON_ENVIROMENT_CONTROL()
	
	IF IS_VEH_WEAPON_STATE(VEH_WEAPON_STATE_INIT)
	OR IS_VEH_WEAPON_STATE(VEH_WEAPON_STATE_UPDATE)
		IF IS_ENTITY_ALIVE(GET_VEHICLE_WEAPON_WEAPONISED_VEHICLE())
			VECTOR vDroneCoord = GET_ENTITY_COORDS(GET_VEHICLE_WEAPON_WEAPONISED_VEHICLE())
			SET_POP_CONTROL_SPHERE_THIS_FRAME(vDroneCoord, 100, 200)
			ACTIVATE_INTERIOR_GROUPS_USING_CAMERA()
			IF GET_FRAME_COUNT() % 120 = 0
				SET_SCENARIO_PEDS_SPAWN_IN_SPHERE_AREA(vDroneCoord, 300, 30)
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_VEH_WEAPON_STATE(VEH_WEAPON_STATE_UPDATE)
		// Need to do this mess because replay is unavailable when controls are off
		IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		ENDIF
		
		DISABLE_ALL_CONTROLS_FOR_PLAYER_THIS_FRAME()
		
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CHARACTER_WHEEL)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_CHARACTER_MICHAEL)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_CHARACTER_FRANKLIN)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_CHARACTER_TREVOR)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_CHARACTER_MULTIPLAYER)
		
		ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X) 
		ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y) 
		ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_X)
		ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_Y)
		
		ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_LB) 
		ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_RB) 
		ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_LT)
		ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_RT)
		ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_LS)
		ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_RS)
		
		ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_VEH_CIN_CAM)
		
		ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_CONTEXT)
		ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
		
		ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
		ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
		
		IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
			ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_CANCEL)
			ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_ACCEPT) 
		ENDIF				
		
		IF IS_PAUSE_MENU_ACTIVE()
			ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_FRONTEND_ACCEPT)
			ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_FRONTEND_CANCEL) 
			ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_FRONTEND_UP)
			ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_FRONTEND_DOWN)
			ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_FRONTEND_LEFT)
			ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_X)
			ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_Y)
			ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT_AXIS_Y)
			ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT_AXIS_X)
		ENDIF	
	ENDIF
	
ENDPROC

PROC PLAY_CAM_PAN_LOOP_SOUND()
	IF sVehWeaponStruct.iPanLoopSoundID = -1
		sVehWeaponStruct.iPanLoopSoundID = GET_SOUND_ID()
		PLAY_SOUND_FRONTEND(sVehWeaponStruct.iPanLoopSoundID , "Pan", "DLC_GR_MOC_Turret_Sounds", TRUE)
		CDEBUG1LN(DEBUG_NET_VEH_WEAPON, "[AM_MP_VEH_WEAPON] - PLAY_CAM_PAN_LOOP_SOUND")
	ENDIF	
ENDPROC

FUNC FLOAT RESTRICT_HEADING(float fHeading, float min=-180.0, float max=180.0)
	while fHeading < min
		fHeading += 360.0
	ENDWHILE
	while fHeading > max
		fHeading -= 360.0
	ENDWHILE
	return fHeading
ENDFUNC

PROC UPDATE_VEHICLE_WEAPON_CAMERA()
	IF DOES_CAM_EXIST(sVehWeaponStruct.VehWeaponCamera)
	AND (IS_CAM_RENDERING(sVehWeaponStruct.VehWeaponCamera) OR !IS_BIT_SET(sVehWeaponStruct.iLocalBS, VEHICLE_WEAPON_LOCAL_UPDATE_FIRST_CAM))
	AND IS_ENTITY_ALIVE(GET_VEHICLE_WEAPON_WEAPONISED_VEHICLE())
	AND GET_VEHICLE_WEAPON_WEAPONISED_VEHICLE() != INT_TO_NATIVE(VEHICLE_INDEX, -1)
		FLOAT fCameraZoomSpeed		= 0.25
		FLOAT fCameraInterpSpeed	= 0.0
		GET_MAX_CAM_INTERP_SPEED(fCameraInterpSpeed)
		
		IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
			fCameraZoomSpeed	*= 6
		ENDIF	
			
		INT iZoom, iStickYaw, iStickPitch
		FLOAT fStickPitch, fStickYaw
		
		FLOAT fMaxCamPitch, fMinCamPitch ,fMaxFOV , fMinFOV, fMaxHeading, fMinHeading, fMaxCameraYaw
		FLOAT fInputScale = 128
		
		VECTOR vCamCoord
		
		GET_MIN_MAX_CAM_PITCH(fMinCamPitch, fMaxCamPitch)
		GET_MIN_MAX_CAM_FOV(fMinFOV, fMaxFOV)
		GET_MIN_MAX_CAM_HEADING(fMinHeading, fMaxHeading)
		GET_MAX_CAM_YAW(fMaxCameraYaw)
		
		ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(FRONTEND_CONTROL)
		SET_INPUT_EXCLUSIVE(PLAYER_CONTROL, INPUT_SNIPER_ZOOM) // Needed for PC, as sniper zoom is the same as weapon cycle.
		
		//Get analogue stick positions.
		IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
			fInputScale = 15
			// Mouse uses float for better precision.
			fStickPitch = GET_DISABLED_CONTROL_UNBOUND_NORMAL(PLAYER_CONTROL, INPUT_SCALED_LOOK_UD) * -fInputScale
			fStickYaw = GET_DISABLED_CONTROL_UNBOUND_NORMAL(PLAYER_CONTROL, INPUT_SCALED_LOOK_LR) * fInputScale
		ELSE
			iStickPitch = FLOOR(GET_DISABLED_CONTROL_UNBOUND_NORMAL(PLAYER_CONTROL, INPUT_SCALED_LOOK_UD) * -fInputScale)
			iStickYaw = FLOOR(GET_DISABLED_CONTROL_UNBOUND_NORMAL(PLAYER_CONTROL, INPUT_SCALED_LOOK_LR) * fInputScale)
		ENDIF									 
		iZoom = FLOOR(GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_SNIPER_ZOOM) * fInputScale)									 
		
		// No need to dead-zone on mouse
		IF NOT IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)		
			INT iDeadzoneMagnitude
			iDeadzoneMagnitude = ROUND(iDeadzoneMagnitude * fInputScale)
			//Deadzone zoom
			IF ABSI(iZoom) < iDeadzoneMagnitude
				iZoom = 0
			ENDIF
			//Deadzone camera movement
			VECTOR vecInput = <<iStickPitch, iStickYaw, 0>> 
			IF VMAG(vecInput) < iDeadzoneMagnitude
				iStickPitch = 0
				iStickYaw = 0
			ENDIF
		ENDIF
		
		FLOAT fYawMultiplier = ((vCamCoord.x / fMaxCamPitch) * 0.5)

		IF fYawMultiplier > 0
			fYawMultiplier += 1.0
		ELSE
			fYawMultiplier = 1.0
		ENDIF
		
		// Using TIMESTEP() with mouse breaks mouse movement.
		IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
			// Mouse uses float for better precision.
			sVehWeaponStruct.iStickPosition[VEH_WEAPON_RIGHT_X] = (fStickPitch * sVehWeaponStruct.fBaseCamFov) * fCameraInterpSpeed
			sVehWeaponStruct.iStickPosition[VEH_WEAPON_RIGHT_Y] = ((fStickYaw * sVehWeaponStruct.fBaseCamFov) * fCameraInterpSpeed) * fYawMultiplier
		ELSE
			sVehWeaponStruct.iStickPosition[VEH_WEAPON_RIGHT_X] = ((TO_FLOAT(iStickPitch) * sVehWeaponStruct.fBaseCamFov) * fCameraInterpSpeed) * TIMESTEP()
			sVehWeaponStruct.iStickPosition[VEH_WEAPON_RIGHT_Y] = (((TO_FLOAT(iStickYaw) * sVehWeaponStruct.fBaseCamFov) * fCameraInterpSpeed) * fYawMultiplier) * TIMESTEP()
		ENDIF
		
		sVehWeaponStruct.iStickPosition[VEH_WEAPON_RIGHT_Y] = RESTRICT_HEADING(sVehWeaponStruct.iStickPosition[VEH_WEAPON_RIGHT_Y], fMinHeading, fMaxHeading)
		
		IF iStickPitch != 0
		AND iStickYaw != 0
			vCamCoord = GET_CAM_ROT(sVehWeaponStruct.VehWeaponCamera)
		ENDIF
		
		IF NOT ARE_VECTORS_EQUAL(GET_CAM_COORD(sVehWeaponStruct.VehWeaponCamera),<<0,0,0>>)

			IF !IS_ENTITY_DEAD(GET_VEHICLE_WEAPON_WEAPONISED_VEHICLE())
				FLOAT fVehHeading = GET_ENTITY_HEADING(GET_VEHICLE_WEAPON_WEAPONISED_VEHICLE())

				FLOAT fCamVehDiff = fVehHeading - vCamCoord.z
				
				INT safeAbort
				WHILE ABSF(fCamVehDiff) >= 180
				AND safeAbort < 30
	
					IF fCamVehDiff < 0 
						fCamVehDiff += 360
					ELSE
						fCamVehDiff -= 360 
					ENDIF

					safeAbort++
					IF safeAbort = 30
						SCRIPT_ASSERT("[AM_MP_VEH_WEAPON] Stuck in while loop in AM_MP_VEHICLE_WEAPON.sch")
					ENDIF
					
				ENDWHILE
				
				IF fCamVehDiff >= fMaxHeading									
					IF fCamVehDiff > 0
						vCamCoord.z = fVehHeading - fMaxHeading
					ELSE
						vCamCoord.z = fVehHeading + fMaxHeading
					ENDIF
				ENDIF 
				
				IF fCamVehDiff < fMinHeading									
					IF fCamVehDiff > 0
						vCamCoord.z = fVehHeading - ABSF(fMinHeading)
					ELSE
						vCamCoord.z = fVehHeading + ABSF(fMinHeading)
					ENDIF
				ENDIF

			ENDIF
			
			// When we use the attach method we should track the difference each frame so we can try and correct the heading/pitch of the turret.
			FLOAT fVehicleDiffX = 0.0
			FLOAT fVehicleDiffY = 0.0

			IF ARE_VECTORS_EQUAL( sVehWeaponStruct.vAttachedEntityRotation, <<0,0,0>>)

				sVehWeaponStruct.vAttachedEntityRotation = GET_ENTITY_ROTATION(GET_VEHICLE_WEAPON_WEAPONISED_VEHICLE())
			ELSE
				VECTOR vCurrentRot = GET_ENTITY_ROTATION(GET_VEHICLE_WEAPON_WEAPONISED_VEHICLE())

				FLOAT fPitchScale

				fPitchScale = ((ABSF(sVehWeaponStruct.vAttachedCamRotationOffset.z)-90.0) / -90.0)
				
				fVehicleDiffX += ((vCurrentRot.x - sVehWeaponStruct.vAttachedEntityRotation.x)*fPitchScale)
				fVehicleDiffY += (vCurrentRot.z - sVehWeaponStruct.vAttachedEntityRotation.z)

				// Keep the heading within the -180 to 180 range.
				IF fVehicleDiffY > 180
					fVehicleDiffY -= 360
				ENDIF
				IF fVehicleDiffY < -180
					fVehicleDiffY += 360
				ENDIF
				
				// Limit the pitch update to avoid gimbal lock.
				IF vCurrentRot.x < -80
				OR vCurrentRot.x > 80
					fVehicleDiffX = 0.0
					fVehicleDiffY = 0.0
				ENDIF
				
				IF IS_ENTITY_UPSIDEDOWN(GET_VEHICLE_WEAPON_WEAPONISED_VEHICLE())
					fVehicleDiffX = 0.0
					fVehicleDiffY = 0.0
				ENDIF
				
				sVehWeaponStruct.vAttachedEntityRotation = vCurrentRot
			ENDIF
			
			IF sVehWeaponStruct.iStickPosition[VEH_WEAPON_RIGHT_X] != 0 OR sVehWeaponStruct.iStickPosition[VEH_WEAPON_RIGHT_Y] != 0
			OR fVehicleDiffX != 0.0 OR fVehicleDiffY != 0.0
			OR !IS_BIT_SET(sVehWeaponStruct.iLocalBS, VEHICLE_WEAPON_LOCAL_UPDATE_FIRST_CAM)

				FLOAT fCamRotSpeed 
				
				fCamRotSpeed = (((sVehWeaponStruct.fBaseCamFov-(fMinFOV-1)) / (fMaxFOV-(fMinFOV-1)))*6)

				// Lower limit
				IF fCamRotSpeed < 3.0
					fCamRotSpeed = 3.0
				ENDIF
					
				sVehWeaponStruct.iStickPosition[VEH_WEAPON_RIGHT_X] = sVehWeaponStruct.iStickPosition[VEH_WEAPON_RIGHT_X] * fCamRotSpeed
				sVehWeaponStruct.iStickPosition[VEH_WEAPON_RIGHT_Y] = sVehWeaponStruct.iStickPosition[VEH_WEAPON_RIGHT_Y] * fCamRotSpeed

				// Camera attaches track yaw and pitch offsets separately.
				sVehWeaponStruct.vAttachedCamRotationOffset.x += sVehWeaponStruct.iStickPosition[VEH_WEAPON_RIGHT_X]
				sVehWeaponStruct.vAttachedCamRotationOffset.z -= sVehWeaponStruct.iStickPosition[VEH_WEAPON_RIGHT_Y]
				
				sVehWeaponStruct.vAttachedCamRotationOffset.x -= fVehicleDiffX
				sVehWeaponStruct.vAttachedCamRotationOffset.z -= fVehicleDiffY
				
				IF sVehWeaponStruct.vAttachedCamRotationOffset.x < fMinCamPitch
					sVehWeaponStruct.vAttachedCamRotationOffset.x = fMinCamPitch
				ENDIF
				IF sVehWeaponStruct.vAttachedCamRotationOffset.x > fMaxCamPitch
					sVehWeaponStruct.vAttachedCamRotationOffset.x = fMaxCamPitch
				ENDIF
				
				IF fMaxCameraYaw >= 360.0
					WHILE sVehWeaponStruct.vAttachedCamRotationOffset.z > 180.0
						sVehWeaponStruct.vAttachedCamRotationOffset.z -= 360.0
					ENDWHILE
					WHILE sVehWeaponStruct.vAttachedCamRotationOffset.z < -180.0
						sVehWeaponStruct.vAttachedCamRotationOffset.z += 360.0
					ENDWHILE
				ELSE
					IF sVehWeaponStruct.vAttachedCamRotationOffset.z < (-fMaxCameraYaw / 2.0)
						sVehWeaponStruct.vAttachedCamRotationOffset.z = (-fMaxCameraYaw / 2.0)
					ENDIF
					IF sVehWeaponStruct.vAttachedCamRotationOffset.z > (fMaxCameraYaw / 2.0)
						sVehWeaponStruct.vAttachedCamRotationOffset.z = (fMaxCameraYaw / 2.0)
					ENDIF
				ENDIF

			ENDIF
			
			IF NOT IS_BIT_SET(sVehWeaponStruct.iLocalBS, VEHICLE_WEAPON_LOCAL_UPDATE_FIRST_CAM)
				IF IS_SCRIPT_STARTED_FROM_INTERIOR()
					sVehWeaponStruct.vAttachedCamRotationOffset = <<fMinCamPitch, 0, 0>>
				ELSE
					sVehWeaponStruct.vAttachedCamRotationOffset = <<fMaxCamPitch, 0, 0>>
				ENDIF	
				SET_BIT(sVehWeaponStruct.iLocalBS, VEHICLE_WEAPON_LOCAL_UPDATE_FIRST_CAM)
			ENDIF	
			
			IF NOT ARE_VECTORS_ALMOST_EQUAL(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].vTurretRayTrace[0], <<sVehWeaponStruct.vAttachedCamRotationOffset.x, 0.0, sVehWeaponStruct.vAttachedCamRotationOffset.z>>) 
				GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].vTurretRayTrace[0] =  <<sVehWeaponStruct.vAttachedCamRotationOffset.x, 0.0, sVehWeaponStruct.vAttachedCamRotationOffset.z>> 
			ENDIF
			
			IF IS_SCRIPT_STARTED_FROM_INTERIOR()
				ATTACH_CAM_TO_VEHICLE_BONE(sVehWeaponStruct.VehWeaponCamera, GET_VEHICLE_WEAPON_WEAPONISED_VEHICLE(), GET_ENTITY_BONE_INDEX_BY_NAME(GET_VEHICLE_WEAPON_WEAPONISED_VEHICLE(), "weapon_1d"), TRUE , << sVehWeaponStruct.vAttachedCamRotationOffset.x, 0.0, sVehWeaponStruct.vAttachedCamRotationOffset.z>>, GET_TURRET_CAM_OFFSET(), TRUE)
			ELSE	
				ATTACH_CAM_TO_VEHICLE_BONE(sVehWeaponStruct.VehWeaponCamera, GET_VEHICLE_WEAPON_WEAPONISED_VEHICLE(), 0, TRUE , << sVehWeaponStruct.vAttachedCamRotationOffset.x, 0.0, sVehWeaponStruct.vAttachedCamRotationOffset.z>>, GET_TURRET_CAM_OFFSET(), TRUE)
			ENDIF	
		ENDIF
		
		sVehWeaponStruct.fBaseCamFov += (TO_FLOAT(iZoom) / 5) * TIMESTEP() * fCameraZoomSpeed
		
		IF sVehWeaponStruct.fBaseCamFov > fMaxFOV
			sVehWeaponStruct.fBaseCamFov = fMaxFOV
		ENDIF
		IF sVehWeaponStruct.fBaseCamFov < fMinFOV
			sVehWeaponStruct.fBaseCamFov = fMinFOV			
		ENDIF
		
		SET_GAMEPLAY_CAM_MOTION_BLUR_SCALING_THIS_UPDATE(1.0)

		SET_CAM_FOV(sVehWeaponStruct.VehWeaponCamera, sVehWeaponStruct.fBaseCamFov)
		
		FLOAT fBlurOverride = (sVehWeaponStruct.fBaseCamFov - 5.0) /42.0
		SET_DISTANCE_BLUR_STRENGTH_OVERRIDE(fBlurOverride)
		
		// Audio 
		IF REQUEST_AMBIENT_AUDIO_BANK("SCRIPT\\POLICE_CHOPPER_CAM")
			IF ABSI(iZoom) > 0
			AND sVehWeaponStruct.fBaseCamFov <= fMaxFOV
			AND sVehWeaponStruct.fBaseCamFov >= fMinFOV
				INT iSoundID = sVehWeaponStruct.iZoomSound
				IF iSoundID != -1
					IF HAS_SOUND_FINISHED(iSoundID)	
						PLAY_SOUND_FRONTEND(iSoundID,"COP_HELI_CAM_ZOOM")
					ENDIF
					
					SET_VARIABLE_ON_SOUND(iSoundID,"Ctrl", TO_FLOAT(iZoom))
					IF iZoom < 0 
						SET_VARIABLE_ON_SOUND(iSoundID,"Dir",-1)
					ELSE
						SET_VARIABLE_ON_SOUND(iSoundID,"Dir", 1)
					ENDIF
				ENDIF
			ELSE
				IF sVehWeaponStruct.iZoomSound != -1
					IF NOT HAS_SOUND_FINISHED(sVehWeaponStruct.iZoomSound)	
						STOP_SOUND(sVehWeaponStruct.iZoomSound)
					ENDIF
				ENDIF
			ENDIF
			
			VECTOR vRStick = -<<GET_DISABLED_CONTROL_UNBOUND_NORMAL(PLAYER_CONTROL, INPUT_SCALED_LOOK_UD), 0, GET_DISABLED_CONTROL_UNBOUND_NORMAL(PLAYER_CONTROL, INPUT_SCALED_LOOK_LR)>>
			
			IF VMAG(vRStick) > 0
				INT iSoundID = sVehWeaponStruct.iPanSound
				IF iSoundID != -1
					IF HAS_SOUND_FINISHED(iSoundID) 
						PLAY_SOUND_FRONTEND(iSoundID, "COP_HELI_CAM_TURN")
					ENDIF
					SET_VARIABLE_ON_SOUND(iSoundID, "Ctrl", VMAG(vRStick))
				ENDIF
			ELSE
				INT iSoundID = sVehWeaponStruct.iPanSound
				IF iSoundID != -1
					IF NOT HAS_SOUND_FINISHED(iSoundID) 
						STOP_SOUND(iSoundID)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF
ENDPROC

PROC MAINTAIN_WEAPON_PROP_DIRECTION()

	IF IS_ENTITY_ALIVE(GET_VEHICLE_WEAPON_WEAPONISED_VEHICLE())
	AND DOES_CAM_EXIST(sVehWeaponStruct.VehWeaponCamera)
	AND IS_CAM_RENDERING(sVehWeaponStruct.VehWeaponCamera)
		INT boneIndex = GET_ENTITY_BONE_INDEX_BY_NAME(GET_VEHICLE_WEAPON_WEAPONISED_VEHICLE(), GET_VEHICLE_WEAPON_BONE_NAME())
		IF boneIndex <> -1
			IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bTurretInUse, 0)
				SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bTurretInUse, 0)
			ENDIF	
		ELSE
			CDEBUG1LN(DEBUG_NET_VEH_WEAPON, "[AM_MP_VEH_WEAPON] - MAINTAIN_WEAPON_PROP_DIRECTION - boneIndex = -1")
		ENDIF
	ELSE
		IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bTurretInUse, 0)
			CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bTurretInUse, 0)
		ENDIF
	ENDIF	
ENDPROC

FUNC BOOL IS_ANY_TARGET_ACQUIRE_SOUND_PLAYING()
	INT i 
	FOR i = 0 TO MAX_NUM_TARGET_VEH - 1 
		IF !HAS_SOUND_FINISHED(sVehWeaponStruct.iAcquiringSoundID[i])
		OR !HAS_SOUND_FINISHED(sVehWeaponStruct.iAcquiredTargetSoundID[i])
			RETURN TRUE
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC

PROC DRAW_BOX_AROUND_TARGET_VEHICLE(ENTITY_INDEX entIndex, BOOL bHasLetter = FALSE, INT iTargetIndex = 0)
	IF HAS_STREAMED_TEXTURE_DICT_LOADED("helicopterhud")
		
		FLOAT  fBoxDrawSize, x, y
			
		INT r, g, b, a
			
		SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD) 
		
		IF sVehWeaponStruct.iNumTargets > -1
			IF IS_ENTITY_ALIVE(entIndex)
				GET_SCREEN_COORD_FROM_WORLD_COORD(GET_ENTITY_COORDS(entIndex), x, y)
				
			
				fBoxDrawSize = 0.03
					
				IF x > 0.10 AND x < 0.90
				AND y > 0.10 AND y < 0.90
					
					IF IS_ENTITY_A_PED(entIndex)
						PED_INDEX targetPedIndex = GET_PED_INDEX_FROM_ENTITY_INDEX(entIndex)
						IF IS_PED_A_PLAYER(targetPedIndex)
							PLAYER_INDEX targetPlayerIndex = NETWORK_GET_PLAYER_INDEX_FROM_PED(targetPedIndex)
							IF IS_NET_PLAYER_OK(targetPlayerIndex)
								IF IS_PLAYER_IN_VEH_SEAT(targetPlayerIndex, VS_DRIVER)
									SET_VEHICLE_HOMING_LOCKEDONTO_STATE(GET_VEHICLE_PED_IS_IN(targetPedIndex), HLOS_ACQUIRED)
									PRINTLN("[AM_MP_VEH_WEAPON] DRAW_BOX_AROUND_TARGET_VEHICLE SET_VEHICLE_HOMING_LOCKEDONTO_STATE player: ", GET_PLAYER_NAME(targetPlayerIndex))
								ENDIF
							ENDIF	
						ENDIF
					ELIF IS_ENTITY_A_VEHICLE(entIndex)
						PED_INDEX targetPedIndex = GET_PED_IN_VEHICLE_SEAT(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(entIndex))
						IF DOES_ENTITY_EXIST(targetPedIndex)
							IF IS_PED_A_PLAYER(targetPedIndex)
								PLAYER_INDEX targetPlayerIndex = NETWORK_GET_PLAYER_INDEX_FROM_PED(targetPedIndex)
								IF IS_NET_PLAYER_OK(targetPlayerIndex)
									SET_VEHICLE_HOMING_LOCKEDONTO_STATE(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(entIndex), HLOS_ACQUIRED)
									PRINTLN("[AM_MP_VEH_WEAPON] DRAW_BOX_AROUND_TARGET_VEHICLE SET_VEHICLE_HOMING_LOCKEDONTO_STATE on vehicle player is in: ", GET_PLAYER_NAME(targetPlayerIndex))
								ENDIF	
							ENDIF	
						ENDIF	
					ENDIF
					
					IF NOT IS_BIT_SET(sVehWeaponStruct.iTargetSoundBS, iTargetIndex)
						IF HAS_SOUND_FINISHED(sVehWeaponStruct.iAcquiringSoundID[iTargetIndex])
						AND sVehWeaponStruct.iAcquiringSoundID[iTargetIndex] = -1
						AND NOT IS_ANY_TARGET_ACQUIRE_SOUND_PLAYING()
							sVehWeaponStruct.iAcquiringSoundID[iTargetIndex] = GET_SOUND_ID()
							PLAY_SOUND_FRONTEND(sVehWeaponStruct.iAcquiringSoundID[iTargetIndex], "VULKAN_LOCK_ON_AMBER")
							START_NET_TIMER(sVehWeaponStruct.sAcquiringTargetSound[iTargetIndex])
							SET_BIT(sVehWeaponStruct.iTargetSoundBS, iTargetIndex)
						ENDIF
					ELSE
						IF NOT IS_BIT_SET(sVehWeaponStruct.iTargetSoundBS, iTargetIndex + MAX_NUM_TARGET_VEH)
							IF HAS_NET_TIMER_STARTED(sVehWeaponStruct.sAcquiringTargetSound[iTargetIndex])
								IF HAS_NET_TIMER_EXPIRED(sVehWeaponStruct.sAcquiringTargetSound[iTargetIndex], 1000)
									STOP_TARGET_ACQUIRING_SOUND(iTargetIndex, FALSE)
								
									IF HAS_SOUND_FINISHED(sVehWeaponStruct.iAcquiredTargetSoundID[iTargetIndex])
									AND sVehWeaponStruct.iAcquiredTargetSoundID[iTargetIndex] = -1
										sVehWeaponStruct.iAcquiredTargetSoundID[iTargetIndex] = GET_SOUND_ID()
										PLAY_SOUND_FRONTEND(sVehWeaponStruct.iAcquiredTargetSoundID[iTargetIndex], "VULKAN_LOCK_ON_RED")
										START_NET_TIMER(sVehWeaponStruct.sAcquiredTargetSound[iTargetIndex])
										SET_BIT(sVehWeaponStruct.iTargetSoundBS, iTargetIndex + MAX_NUM_TARGET_VEH)
										PRINTLN("[AM_MP_VEH_WEAPON] DRAW_BOX_AROUND_TARGET_VEHICLE PLAY_SOUND_FRONTEND TARGET_ACQUIRED")
									ENDIF
								ENDIF	
							ENDIF
						ELSE
							IF HAS_NET_TIMER_STARTED(sVehWeaponStruct.sAcquiredTargetSound[iTargetIndex])
								IF HAS_NET_TIMER_EXPIRED(sVehWeaponStruct.sAcquiredTargetSound[iTargetIndex], 700)
									STOP_TARGET_ACQUIRIED_SOUND(iTargetIndex, FALSE)
								ENDIF
							ENDIF	
						ENDIF
					ENDIF	

					SET_DRAW_ORIGIN(GET_ENTITY_COORDS(entIndex), FALSE)
					
					IF !IS_BIT_SET(sVehWeaponStruct.iTargetSoundBS, iTargetIndex + MAX_NUM_TARGET_VEH)
						GET_HUD_COLOUR(HUD_COLOUR_ORANGE, r, g, b, a)
					ELSE
						GET_HUD_COLOUR(HUD_COLOUR_RED, r, g, b, a)
					ENDIF
					
					IF NOT bHasLetter
						DRAW_SPRITE("helicopterhud", "hud_corner", -fBoxDrawSize * 0.6, -fBoxDrawSize, fBoxDrawSize * 1.5, fBoxDrawSize * 1.5, 0, r, g, b, a, TRUE)
					ENDIF
					DRAW_SPRITE("helicopterhud", "hud_corner", fBoxDrawSize * 0.6, -fBoxDrawSize, fBoxDrawSize * 1.5, fBoxDrawSize * 1.5, 90, r, g, b, a, TRUE)
					DRAW_SPRITE("helicopterhud", "hud_corner", -fBoxDrawSize * 0.6, fBoxDrawSize, fBoxDrawSize * 1.5, fBoxDrawSize * 1.5, 270, r, g, b, a, TRUE)
					DRAW_SPRITE("helicopterhud", "hud_corner", fBoxDrawSize * 0.6, fBoxDrawSize, fBoxDrawSize * 1.5, fBoxDrawSize * 1.5, 180, r, g, b, a, TRUE)
					
					CLEAR_DRAW_ORIGIN()
					
					
				ELSE
					STOP_TARGET_ACQUIRING_SOUND(iTargetIndex)
					STOP_TARGET_ACQUIRIED_SOUND(iTargetIndex)
				ENDIF		
			ELSE
				STOP_TARGET_ACQUIRING_SOUND(iTargetIndex)
				STOP_TARGET_ACQUIRIED_SOUND(iTargetIndex)
			ENDIF	
		ELSE
			STOP_TARGET_ACQUIRING_SOUND(iTargetIndex)
			STOP_TARGET_ACQUIRIED_SOUND(iTargetIndex)
		ENDIF
	ENDIF	
ENDPROC

PROC DRAW_TARGET_AREA()
	IF HAS_STREAMED_TEXTURE_DICT_LOADED("droneHUD")

		FLOAT  fBoxDrawSize
		
		INT r, g, b, a
		
		SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD) 

		fBoxDrawSize = 0.045
		
		GET_HUD_COLOUR(HUD_COLOUR_WHITE, r, g, b, a)

		DRAW_SPRITE("droneHUD", "corner", 0.2, 0.2, fBoxDrawSize * 1.5, fBoxDrawSize * 1.5, 0, r, g, b, a, TRUE)
		DRAW_SPRITE("droneHUD", "corner", 0.8, 0.2, fBoxDrawSize * 1.5, fBoxDrawSize * 1.5, 90, r, g, b, a, TRUE)
		DRAW_SPRITE("droneHUD", "corner", 0.2, 0.8, fBoxDrawSize * 1.5, fBoxDrawSize * 1.5, 270, r, g, b, a, TRUE)
		DRAW_SPRITE("droneHUD", "corner", 0.8, 0.8, fBoxDrawSize * 1.5, fBoxDrawSize * 1.5, 180, r, g, b, a, TRUE)
				
	ENDIF	
ENDPROC

FUNC BOOL IS_CAM_FACING_TARGET_VEHICLE(ENTITY_INDEX entIndex)
	//FLOAT fDot
	
	IF DOES_ENTITY_EXIST(entIndex)
	AND NOT IS_ENTITY_DEAD(entIndex)
		
		FLOAT x, y
		
		GET_SCREEN_COORD_FROM_WORLD_COORD(GET_ENTITY_COORDS(entIndex), x, y)
		
		// Check if entity is on screen 
		IF x < 0.10 OR x > 0.90
		OR y < 0.10 OR y > 0.90
			RETURN FALSE
		ENDIF
	ENDIF	
	
//	RETURN fDot >= 0
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_THIS_NEW_TARGET(ENTITY_INDEX entIndex)
	INT iNum
	FOR iNum = 0 TO MAX_NUM_NEW_TARGET - 1
		IF DOES_ENTITY_EXIST(entIndex)
		AND !IS_ENTITY_DEAD(entIndex)
			IF entIndex = sVehWeaponStruct.newTargetVeh[iNum]
			 	RETURN FALSE
			ENDIF
		ENDIF
	ENDFOR
	RETURN TRUE
ENDFUNC

FUNC VEHICLE_INDEX GET_CLOSEST_VEHICLE_TO_WEAPON(ENTITY_INDEX veh1, ENTITY_INDEX veh2)
	ENTITY_INDEX closesEnt
	
	IF DOES_CAM_EXIST(sVehWeaponStruct.VehWeaponCamera)
	AND IS_CAM_RENDERING(sVehWeaponStruct.VehWeaponCamera)
		IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(veh1, GET_CAM_COORD(sVehWeaponStruct.VehWeaponCamera)) < GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(veh2, GET_CAM_COORD(sVehWeaponStruct.VehWeaponCamera))
			closesEnt = veh1
		ELSE
			closesEnt = veh2
		ENDIF
	ENDIF
	
	RETURN closesEnt
ENDFUNC

FUNC BOOL ASSIGN_NEW_TARGET(ENTITY_INDEX entIndex)
	INT iNumTarget
	FOR iNumTarget = 0 TO MAX_NUM_TARGET_VEH - 1
		IF !DOES_ENTITY_EXIST(sVehWeaponStruct.targetVeh[iNumTarget])
			sVehWeaponStruct.targetVeh[iNumTarget] = entIndex
			
			CDEBUG1LN(DEBUG_NET_VEH_WEAPON, "[AM_MP_VEH_WEAPON] - ASSIGN_NEW_TARGET current target sum: ", sVehWeaponStruct.iNumTargets , " new target sum: ", sVehWeaponStruct.iNumTargets + 1 , " array index: ", iNumTarget)
			
			sVehWeaponStruct.iNumTargets++
			RETURN TRUE
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC

FUNC FLOAT GET_VECTOR_MAGNITUDE(VECTOR vVector)
	RETURN SQRT((vVector.x * vVector.x) + (vVector.y * vVector.y) + (vVector.z * vVector.z))
ENDFUNC

FUNC INT GET_FURTHEST_SELECTED_TARGET_TO_CAM()
	INT vFurthestTargetIndex = -1
	FLOAT fFurthestDistanceSqr = 0
	
	IF DOES_CAM_EXIST(sVehWeaponStruct.VehWeaponCamera)
	AND IS_CAM_RENDERING(sVehWeaponStruct.VehWeaponCamera)
		
		VECTOR vCurrentCoord = GET_CAM_COORD(sVehWeaponStruct.VehWeaponCamera)
		INT iNumTarget
		FOR iNumTarget = 0 TO MAX_NUM_TARGET_VEH - 1
			IF DOES_ENTITY_EXIST(sVehWeaponStruct.targetVeh[iNumTarget])
			AND NOT IS_ENTITY_DEAD(sVehWeaponStruct.targetVeh[iNumTarget])
				VECTOR vDirectionToTarget = GET_ENTITY_COORDS(sVehWeaponStruct.targetVeh[iNumTarget]) - vCurrentCoord
	            FLOAT dSqrToTarget = GET_VECTOR_MAGNITUDE(vDirectionToTarget)
				IF (dSqrToTarget > fFurthestDistanceSqr)
	                fFurthestDistanceSqr = dSqrToTarget
	                vFurthestTargetIndex = iNumTarget
				ENDIF	
			ENDIF	
		ENDFOR	
	ENDIF

	sVehWeaponStruct.iFurthestTargetIndexToRemove = vFurthestTargetIndex
	RETURN vFurthestTargetIndex
ENDFUNC

PROC REPLACE_NEW_TARGET_WITH_FURTHEST_ONE(ENTITY_INDEX newEntity)
	IF GET_FURTHEST_SELECTED_TARGET_TO_CAM() != -1
	AND GET_FURTHEST_SELECTED_TARGET_TO_CAM() < MAX_NUM_TARGET_VEH
		IF DOES_ENTITY_EXIST(sVehWeaponStruct.targetVeh[GET_FURTHEST_SELECTED_TARGET_TO_CAM()])
		AND DOES_ENTITY_EXIST(newEntity)	
			sVehWeaponStruct.targetVeh[GET_FURTHEST_SELECTED_TARGET_TO_CAM()] = newEntity
		ENDIF	
	ENDIF
ENDPROC

FUNC BOOL IS_NEW_TARGET_CLOSER_THAN_OLD_FURTHEST_TARGET(ENTITY_INDEX newEntity)
	
	IF GET_FURTHEST_SELECTED_TARGET_TO_CAM() != -1
		IF IS_ENTITY_ALIVE(sVehWeaponStruct.targetVeh[GET_FURTHEST_SELECTED_TARGET_TO_CAM()])
		AND IS_ENTITY_ALIVE(newEntity)
		AND IS_ENTITY_ALIVE(GET_PED_NEAR_VEHICLE())
		
			IF GET_DISTANCE_BETWEEN_ENTITIES(GET_PED_NEAR_VEHICLE(), sVehWeaponStruct.targetVeh[GET_FURTHEST_SELECTED_TARGET_TO_CAM()]) > GET_DISTANCE_BETWEEN_ENTITIES(GET_PED_NEAR_VEHICLE(), newEntity)
			AND GET_DISTANCE_BETWEEN_ENTITIES(sVehWeaponStruct.targetVeh[GET_FURTHEST_SELECTED_TARGET_TO_CAM()], newEntity) > 5	// offset to avoid flicker 
				RETURN TRUE 
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC DRAW_TARGET_SQUARE_ON_VEHICLES()
	INT iNumTarget
	FOR iNumTarget = 0 TO MAX_NUM_TARGET_VEH - 1
		DRAW_BOX_AROUND_TARGET_VEHICLE(sVehWeaponStruct.targetVeh[iNumTarget], FALSE, iNumTarget)
	ENDFOR
ENDPROC

FUNC BOOL IS_THIS_CURRENT_TARGET(ENTITY_INDEX entToCheck)
	INT iNumTarget
	FOR iNumTarget = 0 TO MAX_NUM_TARGET_VEH - 1
		IF DOES_ENTITY_EXIST(sVehWeaponStruct.targetVeh[iNumTarget])
		AND !IS_ENTITY_DEAD(sVehWeaponStruct.targetVeh[iNumTarget])
		AND DOES_ENTITY_EXIST(entToCheck)
		AND !IS_ENTITY_DEAD(entToCheck)
			IF entToCheck = sVehWeaponStruct.targetVeh[iNumTarget]
				RETURN TRUE
			ENDIF
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC 

PROC PROCESS_NEW_TARGET_IN_SIGHT_SHAPETEST(ENTITY_INDEX newTarget)
	IF DOES_CAM_EXIST(sVehWeaponStruct.VehWeaponCamera)
	AND IS_CAM_RENDERING(sVehWeaponStruct.VehWeaponCamera)
	AND IS_ENTITY_ALIVE(newTarget)
	AND IS_ENTITY_ALIVE(GET_VEHICLE_WEAPON_WEAPONISED_VEHICLE())
	
		VECTOR vStartPoint = GET_CAM_COORD(sVehWeaponStruct.VehWeaponCamera)
		VECTOR vEndPoint = GET_ENTITY_COORDS(newTarget)

		SWITCH sVehWeaponStruct.inewTargetDetectShapeTest
		     CASE VEHICLE_WEAPON_SHAPETEST_INIT

				sVehWeaponStruct.newTargetDetectShapeTest = START_SHAPE_TEST_LOS_PROBE(vStartPoint, vEndPoint, SCRIPT_INCLUDE_ALL)

				IF NATIVE_TO_INT(sVehWeaponStruct.newTargetDetectShapeTest) != 0
					sVehWeaponStruct.inewTargetDetectShapeTest 		= VEHICLE_WEAPON_SHAPETEST_PROCESS
				ENDIF	

		     BREAK
		     CASE VEHICLE_WEAPON_SHAPETEST_PROCESS
		     
		        INT iHitSomething
		        VECTOR vNormalAtPosHit, vTargetHitPos
		        ENTITY_INDEX entityHit
				 
				sVehWeaponStruct.eNewTargetShapeTestResult = GET_SHAPE_TEST_RESULT(sVehWeaponStruct.newTargetDetectShapeTest, iHitSomething, vTargetHitPos, vNormalAtPosHit, entityHit)

				// result is ready
		         IF sVehWeaponStruct.eNewTargetShapeTestResult = SHAPETEST_STATUS_RESULTS_READY

					IF iHitSomething = 0
						
						sVehWeaponStruct.iNewTargetShapeHitSomthing = VEHICLE_WEAPON_TARGET_CHECK_NOT_HIT_ANYTHING

						vTargetHitPos = <<0,0,0>> 
						
						IF IS_BIT_SET(sVehWeaponStruct.iLocalBS, VEHICLE_WEAPON_LOCAL_HIT_NEW_TARGET)
							CLEAR_BIT(sVehWeaponStruct.iLocalBS, VEHICLE_WEAPON_LOCAL_HIT_NEW_TARGET)
						ENDIF
						
		             ELSE
						
		                 sVehWeaponStruct.iNewTargetShapeHitSomthing = VEHICLE_WEAPON_TARGET_CHECK_HIT_SOMETHING

						IF DOES_ENTITY_EXIST(entityHit)
							IF IS_ENTITY_A_VEHICLE(entityHit)
							OR (IS_ENTITY_A_PED(entityHit)
							AND IS_PED_A_PLAYER(GET_PED_INDEX_FROM_ENTITY_INDEX(entityHit)))
								IF entityHit = GET_ENTITY_FROM_PED_OR_VEHICLE(newTarget)
									IF !IS_ENTITY_DEAD(newTarget)
										IF !IS_BIT_SET(sVehWeaponStruct.iLocalBS, VEHICLE_WEAPON_LOCAL_HIT_NEW_TARGET)
											SET_BIT(sVehWeaponStruct.iLocalBS, VEHICLE_WEAPON_LOCAL_HIT_NEW_TARGET)
											CDEBUG1LN(DEBUG_NET_VEH_WEAPON,"[AM_MP_VEH_WEAPON] - PROCESS_NEW_TARGET_IN_SIGHT_SHAPETEST  VEHICLE_WEAPON_LOCAL_HIT_NEW_TARGET sVehWeaponStruct.iRayCastStagger: ", sVehWeaponStruct.iRayCastStagger, " TRUE")
										ENDIF
									ENDIF	
								ENDIF
							ELSE
								IF IS_BIT_SET(sVehWeaponStruct.iLocalBS, VEHICLE_WEAPON_LOCAL_HIT_NEW_TARGET)
									CLEAR_BIT(sVehWeaponStruct.iLocalBS, VEHICLE_WEAPON_LOCAL_HIT_NEW_TARGET)
								ENDIF
							ENDIF
						ENDIF
						
						sVehWeaponStruct.newTargetDetectShapeTest = INT_TO_NATIVE(SHAPETEST_INDEX, 0)
						sVehWeaponStruct.inewTargetDetectShapeTest 	= VEHICLE_WEAPON_SHAPETEST_INIT  
						
		             ENDIF
					 
				ELIF sVehWeaponStruct.eNewTargetShapeTestResult	= SHAPETEST_STATUS_NONEXISTENT
					sVehWeaponStruct.inewTargetDetectShapeTest 	= VEHICLE_WEAPON_SHAPETEST_INIT    
		         ENDIF

		     BREAK
		 ENDSWITCH	
    ELSE
		IF IS_BIT_SET(sVehWeaponStruct.iLocalBS, VEHICLE_WEAPON_LOCAL_HIT_NEW_TARGET)
			CLEAR_BIT(sVehWeaponStruct.iLocalBS, VEHICLE_WEAPON_LOCAL_HIT_NEW_TARGET)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_ENTITY_ALLOWED_IN_SEARCH(ENTITY_INDEX entIndex)
	IF DOES_ENTITY_EXIST(entIndex)
	AND NOT IS_ENTITY_DEAD(entIndex)
		IF IS_ENTITY_A_VEHICLE(entIndex)
			IF IS_ANY_PLAYER_IN_VEHICLE(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(entIndex), FALSE, FALSE)
				RETURN TRUE
			ENDIF
		
			IF IS_MODEL_POLICE_VEHICLE(GET_ENTITY_MODEL(entIndex))
				RETURN TRUE
			ENDIF
		ELIF IS_ENTITY_A_PED(entIndex)
			PED_INDEX playerPed = GET_PED_INDEX_FROM_ENTITY_INDEX(entIndex)
			IF IS_PED_A_PLAYER(playerPed)
				IF NOT GB_ARE_PLAYERS_IN_SAME_GANG(NETWORK_GET_PLAYER_INDEX_FROM_PED(playerPed), PLAYER_ID())
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF	
	ENDIF
	RETURN FALSE
ENDFUNC 

PROC PROCESS_SEARCH_FOR_NEW_TARGET()
	IF DOES_CAM_EXIST(sVehWeaponStruct.VehWeaponCamera)
	AND IS_CAM_RENDERING(sVehWeaponStruct.VehWeaponCamera)
		IF IS_NET_PLAYER_OK(PLAYER_ID())	
			IF IS_ENTITY_ALIVE(GET_PED_NEAR_VEHICLE())
				INT iLoop, iNumTarget
				VEHICLE_INDEX nearbyVehs[100]
				PLAYER_INDEX playerID
				PED_INDEX playerPed
	
				// Vehicle search
				INT iNearbyVehs = GET_ALL_VEHICLES(nearbyVehs)
				IF iNearbyVehs > 0
					REPEAT iNearbyVehs iLoop
						IF !DOES_ENTITY_EXIST(nearbyVehs[iLoop])
							RELOOP
						ENDIF
						IF !IS_ENTITY_A_VEHICLE(nearbyVehs[iLoop])
						OR nearbyVehs[iLoop] = GET_VEHICLE_WEAPON_WEAPONISED_VEHICLE()
						OR NOT IS_ENTITY_ALLOWED_IN_SEARCH(nearbyVehs[iLoop])
							RELOOP
						ELSE	
							IF IS_THIS_NEW_TARGET(nearbyVehs[iLoop])
							AND NOT IS_THIS_CURRENT_TARGET(nearbyVehs[iLoop])
								// New vehicle found
								IF IS_CAM_FACING_TARGET_VEHICLE(nearbyVehs[iLoop])
								AND GET_DISTANCE_BETWEEN_ENTITIES(nearbyVehs[iLoop], GET_PED_NEAR_VEHICLE()) < 500
									IF iNumTarget > -1 
									AND iNumTarget < MAX_NUM_NEW_TARGET - 1
										sVehWeaponStruct.newTargetVeh[iNumTarget] = nearbyVehs[iLoop]
										CDEBUG1LN(DEBUG_NET_VEH_WEAPON, "[AM_MP_VEH_WEAPON] - PROCESS_VEHICLE_WEAPONS found possible target validate this iNumTarget", iNumTarget,  "iLoop: ", iLoop , " is: ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(sVehWeaponStruct.newTargetVeh[iNumTarget])))
										iNumTarget++
									ELSE
										CDEBUG1LN(DEBUG_NET_VEH_WEAPON, "[AM_MP_VEH_WEAPON] - PROCESS_VEHICLE_WEAPONS reached iNumTarget : ",iNumTarget)
										iNumTarget = 0
									ENDIF	
								ELSE
									RELOOP
								ENDIF
							ELSE
								CDEBUG1LN(DEBUG_NET_VEH_WEAPON, "[AM_MP_VEH_WEAPON] - PROCESS_VEHICLE_WEAPONS already exist iLoop: ", iLoop)
							ENDIF	
						ENDIF
					ENDREPEAT
				ENDIF	
				
//				// Player search
				REPEAT NUM_NETWORK_PLAYERS iLoop
					IF NETWORK_IS_PLAYER_ACTIVE(INT_TO_PLAYERINDEX(iLoop))
						playerID = INT_TO_PLAYERINDEX(iLoop)
						playerPed = GET_PLAYER_PED(playerID)
						IF playerID = PLAYER_ID()
						OR NOT IS_ENTITY_ALLOWED_IN_SEARCH(playerPed)
						OR IS_ENTITY_DEAD(playerPed)
							RELOOP
						ENDIF
						IF IS_THIS_NEW_TARGET(playerPed)
						AND NOT IS_THIS_CURRENT_TARGET(playerPed)
							IF GET_DISTANCE_BETWEEN_ENTITIES(playerPed, GET_PED_NEAR_VEHICLE()) < 500
								IF iNumTarget > -1 
								AND iNumTarget < MAX_NUM_NEW_TARGET - 1
									sVehWeaponStruct.newTargetVeh[iNumTarget] = playerPed
									CDEBUG1LN(DEBUG_NET_VEH_WEAPON, "[AM_MP_VEH_WEAPON] - PROCESS_VEHICLE_WEAPONS found possible player target validate this iNumTarget", iNumTarget,  "iLoop: ", iLoop , " is: ", GET_PLAYER_NAME(playerID))
									iNumTarget++
								ELSE
									CDEBUG1LN(DEBUG_NET_VEH_WEAPON, "[AM_MP_VEH_WEAPON] - PROCESS_VEHICLE_WEAPONS reached iNumTarget : ",iNumTarget)
									iNumTarget = 0
								ENDIF
							ENDIF
						ENDIF	
					ENDIF
				ENDREPEAT	
				sVehWeaponStruct.eVehWeaponTargetState = VEH_WEAPON_TARGET_STATE_VALIDATE_TARGET
			ENDIF
		ENDIF
	ENDIF	
ENDPROC

PROC PROCESS_NET_TARGET_VALIDATION()

	IF sVehWeaponStruct.iRayCastStagger < 0 OR sVehWeaponStruct.iRayCastStagger >= MAX_NUM_NEW_TARGET 
		sVehWeaponStruct.iRayCastStagger  = 0
	ELSE	
		IF DOES_ENTITY_EXIST(sVehWeaponStruct.newTargetVeh[sVehWeaponStruct.iRayCastStagger])
		AND !IS_ENTITY_DEAD(sVehWeaponStruct.newTargetVeh[sVehWeaponStruct.iRayCastStagger])
			PROCESS_NEW_TARGET_IN_SIGHT_SHAPETEST(sVehWeaponStruct.newTargetVeh[sVehWeaponStruct.iRayCastStagger])
			IF HAS_NET_TIMER_EXPIRED(sVehWeaponStruct.newTargetRayCastTimer, 1000)
				IF sVehWeaponStruct.eNewTargetShapeTestResult = SHAPETEST_STATUS_RESULTS_READY
				
					IF IS_BIT_SET(sVehWeaponStruct.iLocalBS, VEHICLE_WEAPON_LOCAL_HIT_NEW_TARGET)
						IF sVehWeaponStruct.iNumTargets < MAX_NUM_TARGET_VEH - 1
							IF ASSIGN_NEW_TARGET(sVehWeaponStruct.newTargetVeh[sVehWeaponStruct.iRayCastStagger])
								sVehWeaponStruct.newTargetVeh[sVehWeaponStruct.iRayCastStagger] = INT_TO_NATIVE(VEHICLE_INDEX, -1)
								CLEAR_BIT(sVehWeaponStruct.iLocalBS, VEHICLE_WEAPON_LOCAL_HIT_NEW_TARGET)
								RESET_NET_TIMER(sVehWeaponStruct.newTargetRayCastTimer)
								sVehWeaponStruct.iRayCastStagger ++
								CDEBUG1LN(DEBUG_NET_VEH_WEAPON, "[AM_MP_VEH_WEAPON] - PROCESS_NET_TARGET_VALIDATION this is valid target sVehWeaponStruct.iRayCastStagger: ", sVehWeaponStruct.iRayCastStagger)
							ENDIF	
						ELSE
							IF IS_NEW_TARGET_CLOSER_THAN_OLD_FURTHEST_TARGET(sVehWeaponStruct.newTargetVeh[sVehWeaponStruct.iRayCastStagger])
								REPLACE_NEW_TARGET_WITH_FURTHEST_ONE(sVehWeaponStruct.newTargetVeh[sVehWeaponStruct.iRayCastStagger])
								CDEBUG1LN(DEBUG_NET_VEH_WEAPON, "[AM_MP_VEH_WEAPON] - PROCESS_NET_TARGET_VALIDATION REPLACE_NEW_TARGET_WITH_FURTHEST_ONE")
							ELSE
								//CDEBUG1LN(DEBUG_NET_VEH_WEAPON, "[AM_MP_VEH_WEAPON] - PROCESS_NET_TARGET_VALIDATION CAN'T REPLACE_NEW_TARGET_WITH_FURTHEST_ONE")
							ENDIF
							
							sVehWeaponStruct.newTargetVeh[sVehWeaponStruct.iRayCastStagger] = INT_TO_NATIVE(VEHICLE_INDEX, -1)
							CLEAR_BIT(sVehWeaponStruct.iLocalBS, VEHICLE_WEAPON_LOCAL_HIT_NEW_TARGET)
							sVehWeaponStruct.inewTargetDetectShapeTest 	= VEHICLE_WEAPON_SHAPETEST_INIT 
							RESET_NET_TIMER(sVehWeaponStruct.newTargetRayCastTimer)
							sVehWeaponStruct.iRayCastStagger ++
							CDEBUG1LN(DEBUG_NET_VEH_WEAPON, "[AM_MP_VEH_WEAPON] - PROCESS_NET_TARGET_VALIDATION CAN'T sVehWeaponStruct.iNumTargets: ", sVehWeaponStruct.iNumTargets, " sVehWeaponStruct.iRayCastStagger: ", sVehWeaponStruct.iRayCastStagger)
						ENDIF
					ELSE
						sVehWeaponStruct.inewTargetDetectShapeTest 	= VEHICLE_WEAPON_SHAPETEST_INIT 
						sVehWeaponStruct.newTargetVeh[sVehWeaponStruct.iRayCastStagger] = INT_TO_NATIVE(VEHICLE_INDEX, -1)
						CLEAR_BIT(sVehWeaponStruct.iLocalBS, VEHICLE_WEAPON_LOCAL_HIT_NEW_TARGET)
						REINIT_NET_TIMER(sVehWeaponStruct.newTargetRayCastTimer)
						sVehWeaponStruct.iRayCastStagger ++
						CDEBUG1LN(DEBUG_NET_VEH_WEAPON, "[AM_MP_VEH_WEAPON] - PROCESS_NET_TARGET_VALIDATION can't ray cast validate new target reset search sVehWeaponStruct.iRayCastStagger: ", sVehWeaponStruct.iRayCastStagger)
					ENDIF
				ENDIF
			ENDIF	
		ELSE
			sVehWeaponStruct.newTargetVeh[sVehWeaponStruct.iRayCastStagger] = INT_TO_NATIVE(VEHICLE_INDEX, -1)
			sVehWeaponStruct.inewTargetDetectShapeTest 	= VEHICLE_WEAPON_SHAPETEST_INIT 
			RESET_NET_TIMER(sVehWeaponStruct.newTargetRayCastTimer)
			sVehWeaponStruct.iRayCastStagger ++
		ENDIF
	ENDIF	
	
	IF sVehWeaponStruct.iRayCastStagger = MAX_NUM_NEW_TARGET
		sVehWeaponStruct.eVehWeaponTargetState = VEH_WEAPON_TARGET_STATE_SEARCHING
		sVehWeaponStruct.inewTargetDetectShapeTest 	= VEHICLE_WEAPON_SHAPETEST_INIT 
		RESET_NET_TIMER(sVehWeaponStruct.newTargetRayCastTimer)
		sVehWeaponStruct.iRayCastStagger = 0
		CDEBUG1LN(DEBUG_NET_VEH_WEAPON, "[AM_MP_VEH_WEAPON] - PROCESS_NET_TARGET_VALIDATION sVehWeaponStruct.iRayCastStagger = MAX_NUM_NEW_TARGET sVehWeaponStruct.eVehWeaponTargetState = VEH_WEAPON_TARGET_STATE_SEARCHING")
	ENDIF
	
ENDPROC

PROC VALIDATE_CURRENT_TARGETS()
	INT iNumTarget 
	INT iNumValidTarget = -1
	FOR iNumTarget = 0 TO MAX_NUM_TARGET_VEH - 1
		IF DOES_ENTITY_EXIST(sVehWeaponStruct.targetVeh[iNumTarget])
			IF DOES_ENTITY_EXIST(sVehWeaponStruct.targetVeh[iNumTarget])
				IF !IS_BIT_SET(sVehWeaponStruct.iTargetBS, iNumTarget)
					IF HAS_NET_TIMER_STARTED(sVehWeaponStruct.sTargetValidateTime[iNumTarget])
						IF HAS_NET_TIMER_EXPIRED(sVehWeaponStruct.sTargetValidateTime[iNumTarget], 1000)
							sVehWeaponStruct.targetVeh[iNumTarget] = INT_TO_NATIVE(VEHICLE_INDEX, -1)
							IF sVehWeaponStruct.iNumTargets > -1
								
								sVehWeaponStruct.iTargetShapeHitSomthing[iNumTarget] = VEHICLE_WEAPON_TARGET_CHECK_INIT
								RESET_NET_TIMER(sVehWeaponStruct.sTargetValidateTime[iNumTarget])
								sVehWeaponStruct.iNumTargets--
								CDEBUG1LN(DEBUG_NET_VEH_WEAPON, "[AM_MP_VEH_WEAPON] - VALIDATE_CURRENT_TARGETS removing index: ", iNumTarget, " ray cast not hitting")
							ENDIF	
						ENDIF
					ELSE
						START_NET_TIMER(sVehWeaponStruct.sTargetValidateTime[iNumTarget])
					ENDIF
				ELSE
					RESET_NET_TIMER(sVehWeaponStruct.sTargetValidateTime[iNumTarget])
				ENDIF
				
				IF  !IS_CAM_FACING_TARGET_VEHICLE(sVehWeaponStruct.targetVeh[iNumTarget])
				//OR (DOES_ENTITY_EXIST(sVehWeaponStruct.targetVeh[iNumTarget]) AND DOES_ENTITY_EXIST(GET_PED_NEAR_VEHICLE()) AND GET_DISTANCE_BETWEEN_ENTITIES(sVehWeaponStruct.targetVeh[iNumTarget], GET_PED_NEAR_VEHICLE()) > 500)
				OR !DOES_ENTITY_EXIST(sVehWeaponStruct.targetVeh[iNumTarget])
				OR !IS_ENTITY_ALLOWED_IN_SEARCH(sVehWeaponStruct.targetVeh[iNumTarget])
					IF !DOES_ENTITY_EXIST(sVehWeaponStruct.targetVeh[iNumTarget])
						CDEBUG1LN(DEBUG_NET_VEH_WEAPON, "[AM_MP_VEH_WEAPON] - VALIDATE_CURRENT_TARGETS removing index: ", iNumTarget, " does not exist")
					ENDIF	
					IF !IS_CAM_FACING_TARGET_VEHICLE(sVehWeaponStruct.targetVeh[iNumTarget])
						CDEBUG1LN(DEBUG_NET_VEH_WEAPON, "[AM_MP_VEH_WEAPON] - VALIDATE_CURRENT_TARGETS removing index : ", iNumTarget, "  not facing current target ")
					ENDIF
					
					sVehWeaponStruct.targetVeh[iNumTarget] = INT_TO_NATIVE(VEHICLE_INDEX, -1)
					IF sVehWeaponStruct.iNumTargets > -1
						sVehWeaponStruct.iTargetShapeHitSomthing[iNumTarget] = VEHICLE_WEAPON_TARGET_CHECK_INIT
						
						RESET_NET_TIMER(sVehWeaponStruct.sTargetValidateTime[iNumTarget])
						sVehWeaponStruct.iNumTargets--
					ENDIF	
				ENDIF
			ENDIF	
		ENDIF
		
		IF DOES_ENTITY_EXIST(sVehWeaponStruct.targetVeh[iNumTarget])
			iNumValidTarget++
		ENDIF
	ENDFOR
	
	IF sVehWeaponStruct.iNumTargets != iNumValidTarget
		CDEBUG1LN(DEBUG_NET_VEH_WEAPON, "[AM_MP_VEH_WEAPON] - VALIDATE_CURRENT_TARGETS out of sync sVehWeaponStruct.iNumTargets: ", sVehWeaponStruct.iNumTargets, " iNumValidTarget: ", iNumValidTarget)
		sVehWeaponStruct.iNumTargets  = iNumValidTarget
	ENDIF
ENDPROC	

PROC MAINTAIN_VEHICLE_WEAPON_SHOOTING()
	
	IF !IS_SCRIPT_STARTED_FROM_INTERIOR()
		EXIT
	ENDIF	
	
	CONTROL_ACTION fireControl = INPUT_FRONTEND_RT
	IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
		fireControl = INPUT_CURSOR_ACCEPT
	ENDIF
	
	IF (IS_CONTROL_PRESSED(FRONTEND_CONTROL, fireControl)
	OR IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, fireControl))
	OR IS_BIT_SET(sVehWeaponStruct.iLocalBS, VEHICLE_WEAPON_LOCAL_BS_START_FIRING_HOMMING_MISSILE)
		INT iDamage = 250
		VEHICLE_INDEX entToIgnore = GET_ENTITY_TO_IGNORE_FOR_BULLET()
		IF sVehWeaponStruct.iNumTargets > -1
			IF NOT IS_BIT_SET(sVehWeaponStruct.iLocalBS, VEHICLE_WEAPON_LOCAL_SHOT_ALL_TARGETS)
				IF !HAS_NET_TIMER_STARTED(sVehWeaponStruct.sHomingMissileTimer)
					START_NET_TIMER(sVehWeaponStruct.sHomingMissileTimer)
					sVehWeaponStruct.iHomingStagger = 0
					SET_BIT(sVehWeaponStruct.iLocalBS, VEHICLE_WEAPON_LOCAL_BS_START_FIRING_HOMMING_MISSILE)
				ELSE
					IF HAS_NET_TIMER_EXPIRED(sVehWeaponStruct.sHomingMissileTimer, 300)
					OR sVehWeaponStruct.iHomingStagger = 0
						IF sVehWeaponStruct.iHomingStagger < 0
						OR sVehWeaponStruct.iHomingStagger >= MAX_NUM_TARGET_VEH
							sVehWeaponStruct.iHomingStagger = 0
						ENDIF	

						IF DOES_ENTITY_EXIST(sVehWeaponStruct.targetVeh[sVehWeaponStruct.iHomingStagger])
						AND NOT IS_ENTITY_DEAD(sVehWeaponStruct.targetVeh[sVehWeaponStruct.iHomingStagger])
							INT boneIndex = GET_ENTITY_BONE_INDEX_BY_NAME(GET_VEHICLE_WEAPON_WEAPONISED_VEHICLE(), GET_VEHICLE_WEAPON_BONE_NAME())
							IF boneIndex <> -1
								VECTOR vStartCoord 		= GET_WORLD_POSITION_OF_ENTITY_BONE(GET_VEHICLE_WEAPON_WEAPONISED_VEHICLE(), boneIndex) + GET_VEHICLE_WEAPON_BONE_OFFSET(sVehWeaponStruct.iHomingStagger)							
								VECTOR vRot 			= GET_FINAL_RENDERED_CAM_ROT()
								VECTOR vDir 			= <<-SIN(vRot.z) * COS(vRot.x), COS(vRot.z) * COS(vRot.x), SIN(vRot.x)>>
								VECTOR vOffset 			= <<1.0, 1.0, 1.0>>
								VECTOR vEndPoint 		= vStartCoord + (vDir*vOffset)
								SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY_NEW(vStartCoord, 
																						vEndPoint, iDamage, TRUE, GET_VEHICLE_WEPAON_TYPE(), 
																						PLAYER_PED_ID(), DEFAULT, DEFAULT, DEFAULT, entToIgnore, DEFAULT, DEFAULT, 
																						sVehWeaponStruct.targetVeh[sVehWeaponStruct.iHomingStagger], DEFAULT, DEFAULT, TRUE)	
								
								PLAY_SOUND_FROM_COORD(-1, "Fire", vStartCoord, "DLC_BTL_Terrobyte_Turret_Sounds", TRUE, 120, TRUE)
								
								sVehWeaponStruct.iNumShotHoming++
								REINIT_NET_TIMER(sVehWeaponStruct.sHomingMissileTimer)
							ELSE
								CDEBUG1LN(DEBUG_NET_VEH_WEAPON, "[AM_MP_VEH_WEAPON] - MAINTAIN_VEHICLE_WEAPON_SHOOTING - boneIndex = -1")
							ENDIF																						
						ENDIF															
						
						IF sVehWeaponStruct.iNumShotHoming = sVehWeaponStruct.iNumTargets
							sVehWeaponStruct.iHomingStagger = 0
							sVehWeaponStruct.iMissileFireMeter = 0
							sVehWeaponStruct.iNumShotHoming = -1
							SET_BIT(sVehWeaponStruct.iLocalBS, VEHICLE_WEAPON_LOCAL_SHOT_ALL_TARGETS)
							CLEAR_BIT(sVehWeaponStruct.iLocalBS, VEHICLE_WEAPON_LOCAL_BS_START_FIRING_HOMMING_MISSILE)
							START_NET_TIMER(sVehWeaponStruct.sHomingMissileCoolDownTimer)
							RESET_NET_TIMER(sVehWeaponStruct.sHomingMissileTimer)
							CDEBUG1LN(DEBUG_NET_VEH_WEAPON, "[AM_MP_VEH_WEAPON] - MAINTAIN_VEHICLE_WEAPON_SHOOTING - VEHICLE_WEAPON_LOCAL_TOOK_A_SHOOT Homing")
						ENDIF	
						
						sVehWeaponStruct.iHomingStagger ++
						CDEBUG1LN(DEBUG_NET_VEH_WEAPON, "[AM_MP_VEH_WEAPON] - MAINTAIN_VEHICLE_WEAPON_SHOOTING - sVehWeaponStruct.iHomingStagger: ", sVehWeaponStruct.iHomingStagger, " sVehWeaponStruct.iNumTargets:  ", sVehWeaponStruct.iNumTargets)	
					ELSE
						CDEBUG1LN(DEBUG_NET_VEH_WEAPON, "[AM_MP_VEH_WEAPON] - MAINTAIN_VEHICLE_WEAPON_SHOOTING - WAITING ON TIMER sVehWeaponStruct.iHomingStagger: ", sVehWeaponStruct.iHomingStagger, " sVehWeaponStruct.iNumTargets:  ", sVehWeaponStruct.iNumTargets)	
					ENDIF	
				ENDIF
			ELSE	
				CDEBUG1LN(DEBUG_NET_VEH_WEAPON, "[AM_MP_VEH_WEAPON] - MAINTAIN_VEHICLE_WEAPON_SHOOTING - VEHICLE_WEAPON_LOCAL_SHOT_ALL_TARGETS TRUE")
				
			ENDIF	
		ELSE
			IF NOT IS_BIT_SET(sVehWeaponStruct.iLocalBS, VEHICLE_WEAPON_LOCAL_TOOK_A_SHOOT)
				IF sVehWeaponStruct.iHomingStagger < 0
				OR sVehWeaponStruct.iHomingStagger >= MAX_NUM_TARGET_VEH
					sVehWeaponStruct.iHomingStagger = 0
				ENDIF	
				
				INT boneIndex = GET_ENTITY_BONE_INDEX_BY_NAME(GET_VEHICLE_WEAPON_WEAPONISED_VEHICLE(), GET_VEHICLE_WEAPON_BONE_NAME())
				IF boneIndex <> -1
					VECTOR vStartPoint 		= GET_WORLD_POSITION_OF_ENTITY_BONE(GET_VEHICLE_WEAPON_WEAPONISED_VEHICLE(), boneIndex) + GET_VEHICLE_WEAPON_BONE_OFFSET(sVehWeaponStruct.iHomingStagger)
					VECTOR vRocketOffset 	= GET_FINAL_RENDERED_CAM_COORD()
					VECTOR vRot 			= GET_FINAL_RENDERED_CAM_ROT()
					VECTOR vDir 			= <<-SIN(vRot.z) * COS(vRot.x), COS(vRot.z) * COS(vRot.x), SIN(vRot.x)>>
					VECTOR vOffset 			= <<10.0, 10.0, 10.0>>
					
					IF !IS_VECTOR_ZERO(sVehWeaponStruct.vEndTargetCoordHitPos)
						vOffset.x = GET_DISTANCE_BETWEEN_COORDS( sVehWeaponStruct.vEndTargetCoordHitPos, vRocketOffset, FALSE)
						vOffset.y = GET_DISTANCE_BETWEEN_COORDS( sVehWeaponStruct.vEndTargetCoordHitPos, vRocketOffset, FALSE)
						vOffset.z = GET_DISTANCE_BETWEEN_COORDS( sVehWeaponStruct.vEndTargetCoordHitPos, vRocketOffset, FALSE)
					ENDIF
					
					VECTOR vEndPoint 		= vRocketOffset + (vDir*vOffset)
					
					SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY_NEW(vStartPoint, 
																			vEndPoint, iDamage, TRUE, GET_VEHICLE_WEPAON_TYPE(1), 
																			PLAYER_PED_ID(), DEFAULT, DEFAULT, DEFAULT, entToIgnore, DEFAULT, DEFAULT, 
																			DEFAULT, DEFAULT, DEFAULT, TRUE)
					
					PLAY_SOUND_FROM_COORD(-1, "Fire", vStartPoint, "DLC_BTL_Terrobyte_Turret_Sounds", TRUE, 120, TRUE)
					SET_BIT(sVehWeaponStruct.iLocalBS, VEHICLE_WEAPON_LOCAL_TOOK_A_SHOOT)
					sVehWeaponStruct.iMissileFireMeter = 0
					START_NET_TIMER(sVehWeaponStruct.sMissileCoolDownTimer)
					sVehWeaponStruct.iHomingStagger ++
					CDEBUG1LN(DEBUG_NET_VEH_WEAPON, "[AM_MP_VEH_WEAPON] - MAINTAIN_VEHICLE_WEAPON_SHOOTING - VEHICLE_WEAPON_LOCAL_TOOK_A_SHOOT")
				ELSE
					CDEBUG1LN(DEBUG_NET_VEH_WEAPON, "[AM_MP_VEH_WEAPON] - MAINTAIN_VEHICLE_WEAPON_SHOOTING - boneIndex = -1")
				ENDIF
			ENDIF
		ENDIF	
	ENDIF	
ENDPROC

PROC PROCESS_TARGET_IN_END_COORD_SHAPETEST()
	
	INT iHitSomething
	VECTOR vNormalAtPosHit
	ENTITY_INDEX entityHit
	SHAPETEST_STATUS shapetestStatus 

	IF DOES_CAM_EXIST(sVehWeaponStruct.VehWeaponCamera)
	AND IS_CAM_RENDERING(sVehWeaponStruct.VehWeaponCamera)
	AND IS_ENTITY_ALIVE(GET_VEHICLE_WEAPON_WEAPONISED_VEHICLE())
	
		VECTOR vRocketOffset 	= GET_FINAL_RENDERED_CAM_COORD()
		VECTOR vRot 			= GET_FINAL_RENDERED_CAM_ROT()
		VECTOR vDir 			= <<-SIN(vRot.z) * COS(vRot.x), COS(vRot.z) * COS(vRot.x), SIN(vRot.x)>>
		VECTOR vOffset 			= <<300.0, 300.0, 300.0>>
		VECTOR vEndPoint 		= vRocketOffset + (vDir*vOffset)
		VECTOR vStartPoint 		= vRocketOffset + (vDir*<<1,1,1>>)
		SWITCH sVehWeaponStruct.iEndTargetCoordShapeTest
		     CASE VEHICLE_WEAPON_SHAPETEST_INIT

				sVehWeaponStruct.targetEndCoordShapeTest = START_SHAPE_TEST_LOS_PROBE(vStartPoint, vEndPoint, SCRIPT_INCLUDE_ALL, GET_VEHICLE_WEAPON_WEAPONISED_VEHICLE())
				
				IF NATIVE_TO_INT(sVehWeaponStruct.targetEndCoordShapeTest) != 0
					sVehWeaponStruct.iEndTargetCoordShapeTest = VEHICLE_WEAPON_SHAPETEST_PROCESS
				ENDIF	

		     BREAK
		     CASE VEHICLE_WEAPON_SHAPETEST_PROCESS
		     
				shapetestStatus = GET_SHAPE_TEST_RESULT(sVehWeaponStruct.targetEndCoordShapeTest, iHitSomething, sVehWeaponStruct.vEndTargetCoordHitPos, vNormalAtPosHit, entityHit)

				// result is ready
		         IF shapetestStatus = SHAPETEST_STATUS_RESULTS_READY
					
					IF iHitSomething = 0
						
						sVehWeaponStruct.iEndTargetCoordShapeHitSomthing = VEHICLE_WEAPON_TARGET_CHECK_NOT_HIT_ANYTHING
						//PRINTLN("[AM_MP_VEH_WEAPON] - PROCESS_TARGET_IN_END_COORD_SHAPETEST VEHICLE_WEAPON_TARGET_CHECK_NOT_HIT_ANYTHING ")
						sVehWeaponStruct.vEndTargetCoordHitPos = <<300.0, 300.0, 300.0>>

		             ELSE
		                sVehWeaponStruct.iEndTargetCoordShapeHitSomthing = VEHICLE_WEAPON_TARGET_CHECK_HIT_SOMETHING
						//PRINTLN("[AM_MP_VEH_WEAPON] - PROCESS_TARGET_IN_END_COORD_SHAPETEST VEHICLE_WEAPON_TARGET_CHECK_HIT_SOMETHING sVehWeaponStruct.vEndTargetCoordHitPos: ", sVehWeaponStruct.vEndTargetCoordHitPos)
						sVehWeaponStruct.iEndTargetCoordShapeTest 	= VEHICLE_WEAPON_SHAPETEST_INIT  
		             ENDIF
					 
				ELIF shapetestStatus = SHAPETEST_STATUS_NONEXISTENT
					sVehWeaponStruct.iEndTargetCoordShapeTest = VEHICLE_WEAPON_SHAPETEST_INIT    
		        ENDIF

		     BREAK
		 ENDSWITCH	
	ENDIF

ENDPROC

PROC PROCESS_VEHICLE_WEAPONS()
	
	IF !IS_SCRIPT_STARTED_FROM_INTERIOR()
		EXIT
	ENDIF
	
	// Homing missile timer
	IF IS_BIT_SET(sVehWeaponStruct.iLocalBS, VEHICLE_WEAPON_LOCAL_SHOT_ALL_TARGETS)
		IF HAS_NET_TIMER_STARTED(sVehWeaponStruct.sHomingMissileCoolDownTimer)
			IF !HAS_NET_TIMER_EXPIRED(sVehWeaponStruct.sHomingMissileCoolDownTimer, 5000)
				INT iMissileFireTimer = (100 * ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), sVehWeaponStruct.sHomingMissileCoolDownTimer.Timer)))
				sVehWeaponStruct.iMissileFireMeter =  iMissileFireTimer / 5000
				STOP_ALL_TARGET_SOUNDS()
				EXIT
			ELSE
				RESET_NET_TIMER(sVehWeaponStruct.sHomingMissileCoolDownTimer)
				CLEAR_BIT(sVehWeaponStruct.iLocalBS, VEHICLE_WEAPON_LOCAL_SHOT_ALL_TARGETS)
				sVehWeaponStruct.iNumShotHoming = -1
				sVehWeaponStruct.iMissileFireMeter = 100
			ENDIF
		ENDIF
	ENDIF
	
	// Missile timer
	IF IS_BIT_SET(sVehWeaponStruct.iLocalBS, VEHICLE_WEAPON_LOCAL_TOOK_A_SHOOT)
		IF HAS_NET_TIMER_STARTED(sVehWeaponStruct.sMissileCoolDownTimer)
			IF !HAS_NET_TIMER_EXPIRED(sVehWeaponStruct.sMissileCoolDownTimer, 1500)
				INT iMissileFireTimer = (100 * ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), sVehWeaponStruct.sMissileCoolDownTimer.Timer)))
				sVehWeaponStruct.iMissileFireMeter =  iMissileFireTimer / 1500
				STOP_ALL_TARGET_SOUNDS()
				EXIT
			ELSE
				RESET_NET_TIMER(sVehWeaponStruct.sMissileCoolDownTimer)
				CLEAR_BIT(sVehWeaponStruct.iLocalBS, VEHICLE_WEAPON_LOCAL_TOOK_A_SHOOT)
				sVehWeaponStruct.iMissileFireMeter = 100
			ENDIF
		ENDIF
	ENDIF
	
	// Homing missile target finder 
	IF NOT IS_BIT_SET(sVehWeaponStruct.iLocalBS, VEHICLE_WEAPON_LOCAL_TOOK_A_SHOOT)
	AND NOT IS_BIT_SET(sVehWeaponStruct.iLocalBS, VEHICLE_WEAPON_LOCAL_BS_START_FIRING_HOMMING_MISSILE)
		SWITCH sVehWeaponStruct.eVehWeaponTargetState
			CASE VEH_WEAPON_TARGET_STATE_SEARCHING 
				PROCESS_SEARCH_FOR_NEW_TARGET()
			BREAK
			
			CASE VEH_WEAPON_TARGET_STATE_VALIDATE_TARGET
				PROCESS_NET_TARGET_VALIDATION()
			BREAK
		ENDSWITCH
		
		VALIDATE_CURRENT_TARGETS()
	ENDIF	

	DRAW_TARGET_SQUARE_ON_VEHICLES()
	PROCESS_TARGET_IN_END_COORD_SHAPETEST()
ENDPROC

PROC PROCESS_TARGET_IN_SIGHT_SHAPETEST()
	
	IF !IS_SCRIPT_STARTED_FROM_INTERIOR()
		EXIT
	ENDIF
	
	INT iHitSomething, iTargetIndex
	VECTOR vNormalAtPosHit, vTargetHitPos
	ENTITY_INDEX entityHit
	SHAPETEST_STATUS shapetestStatus 
	
	FOR iTargetIndex = 0 TO MAX_NUM_TARGET_VEH - 1
		IF DOES_CAM_EXIST(sVehWeaponStruct.VehWeaponCamera)
		AND IS_CAM_RENDERING(sVehWeaponStruct.VehWeaponCamera)
		AND IS_ENTITY_ALIVE(sVehWeaponStruct.targetVeh[iTargetIndex])
		AND IS_ENTITY_ALIVE(GET_VEHICLE_WEAPON_WEAPONISED_VEHICLE())
		
			VECTOR vStartPoint = GET_CAM_COORD(sVehWeaponStruct.VehWeaponCamera)
			VECTOR vEndPoint = GET_ENTITY_COORDS(sVehWeaponStruct.targetVeh[iTargetIndex])

			SWITCH sVehWeaponStruct.itargetDetectShapeTest[iTargetIndex]
			     CASE VEHICLE_WEAPON_SHAPETEST_INIT

					sVehWeaponStruct.targetDetectShapeTest[iTargetIndex] = START_SHAPE_TEST_LOS_PROBE(vStartPoint, vEndPoint, SCRIPT_INCLUDE_ALL)
					
					IF NATIVE_TO_INT(sVehWeaponStruct.targetDetectShapeTest[iTargetIndex]) != 0
						sVehWeaponStruct.itargetDetectShapeTest[iTargetIndex] 		= VEHICLE_WEAPON_SHAPETEST_PROCESS
					ENDIF	

			     BREAK
			     CASE VEHICLE_WEAPON_SHAPETEST_PROCESS
			     
					shapetestStatus = GET_SHAPE_TEST_RESULT(sVehWeaponStruct.targetDetectShapeTest[iTargetIndex], iHitSomething, vTargetHitPos, vNormalAtPosHit, entityHit)

					// result is ready
			         IF shapetestStatus = SHAPETEST_STATUS_RESULTS_READY
						
						IF iHitSomething = 0
							
							sVehWeaponStruct.iTargetShapeHitSomthing[iTargetIndex] = VEHICLE_WEAPON_TARGET_CHECK_NOT_HIT_ANYTHING
							PRINTLN("[AM_MP_VEH_WEAPON] - PROCESS_TARGET_IN_SIGHT_SHAPETEST VEHICLE_WEAPON_TARGET_CHECK_NOT_HIT_ANYTHING ")
							vTargetHitPos = <<0,0,0>> 
							
							IF IS_BIT_SET(sVehWeaponStruct.iTargetBS, iTargetIndex)
								CLEAR_BIT(sVehWeaponStruct.iTargetBS, iTargetIndex)
								PRINTLN("[AM_MP_VEH_WEAPON] - PROCESS_TARGET_IN_SIGHT_SHAPETEST  iTargetBS: ", iTargetIndex, " FALSE ")
							ENDIF
							
			             ELSE
							
			                sVehWeaponStruct.iTargetShapeHitSomthing[iTargetIndex] = VEHICLE_WEAPON_TARGET_CHECK_HIT_SOMETHING
							
							IF IS_BIT_SET(sVehWeaponStruct.iTargetBS, iTargetIndex)
								CLEAR_BIT(sVehWeaponStruct.iTargetBS, iTargetIndex)
							ENDIF
							
							IF DOES_ENTITY_EXIST(entityHit)
								IF sVehWeaponStruct.targetVeh[iTargetIndex] = entityHit
									IF !IS_BIT_SET(sVehWeaponStruct.iTargetBS, iTargetIndex)
										SET_BIT(sVehWeaponStruct.iTargetBS, iTargetIndex)
										PRINTLN("[AM_MP_VEH_WEAPON] - PROCESS_TARGET_IN_SIGHT_SHAPETEST  sVehWeaponStruct.iTargetBS: ", iTargetIndex, " TRUE ")
									ENDIF
								ENDIF
							ENDIF
							
							sVehWeaponStruct.targetDetectShapeTest[iTargetIndex] 	= INT_TO_NATIVE(SHAPETEST_INDEX, 0)
							sVehWeaponStruct.itargetDetectShapeTest[iTargetIndex] 	= VEHICLE_WEAPON_SHAPETEST_INIT  
							
			             ENDIF
						 
					ELIF shapetestStatus	= SHAPETEST_STATUS_NONEXISTENT
						sVehWeaponStruct.itargetDetectShapeTest[iTargetIndex] 		= VEHICLE_WEAPON_SHAPETEST_INIT    
			         ENDIF

			     BREAK
			 ENDSWITCH	
	    ELSE
			IF IS_BIT_SET(sVehWeaponStruct.iTargetBS, iTargetIndex)
				CLEAR_BIT(sVehWeaponStruct.iTargetBS, iTargetIndex)
			ENDIF
		ENDIF
	ENDFOR	
ENDPROC

PROC SCALEFORM_SET_ELEMENT_VISIBLE(STRING sMethodName, BOOL bVisible)
	BEGIN_SCALEFORM_MOVIE_METHOD(sVehWeaponStruct.vehWeaponSF, sMethodName)
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bVisible)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC SCALEFORM_SET_HEADING(INT iHeadingAngle)
	BEGIN_SCALEFORM_MOVIE_METHOD(sVehWeaponStruct.vehWeaponSF, "SET_HEADING")
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iHeadingAngle)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC SCALEFORM_SET_ZOOM(INT iZoom)
	BEGIN_SCALEFORM_MOVIE_METHOD(sVehWeaponStruct.vehWeaponSF, "SET_ZOOM")
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iZoom)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC MANAGE_DRONE_SCALEFORM_ZOOM_LEVEL()
	IF sVehWeaponStruct.fBaseCamFov <= 60
	AND sVehWeaponStruct.fBaseCamFov > 55
		SCALEFORM_SET_ZOOM(0)
	ELIF sVehWeaponStruct.fBaseCamFov <= 55
	AND sVehWeaponStruct.fBaseCamFov > 50	
		SCALEFORM_SET_ZOOM(1)
	ELIF sVehWeaponStruct.fBaseCamFov <= 50
	AND sVehWeaponStruct.fBaseCamFov > 45	
		SCALEFORM_SET_ZOOM(2)
	ELIF sVehWeaponStruct.fBaseCamFov <= 45
	AND sVehWeaponStruct.fBaseCamFov > 40
		SCALEFORM_SET_ZOOM(3)
	ELIF sVehWeaponStruct.fBaseCamFov <= 40
	AND sVehWeaponStruct.fBaseCamFov > 30
		SCALEFORM_SET_ZOOM(4)	
	ENDIF
ENDPROC 

/// PURPOSE:
///    Set reticle target colour
/// PARAMS:
///    bOnTarget - True for red , False for white
PROC SCALEFORM_SET_RETICLE_ON_TARGET(BOOL bOnTarget)
	BEGIN_SCALEFORM_MOVIE_METHOD(sVehWeaponStruct.vehWeaponSF, "SET_RETICLE_ON_TARGET")
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bOnTarget)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

/// PURPOSE:
///    Set Reticle state
/// PARAMS:
///    INT iState 0: Normal (diagonal lines are hidden, centre circle doesn't flash) 1: Fire (diagonal lines are shown briefly, outer curves animate) 2: Charging (centre circle flashes)
PROC SCALEFORM_SET_RETICLE_STATE(INT iState)
	BEGIN_SCALEFORM_MOVIE_METHOD(sVehWeaponStruct.vehWeaponSF, "SET_RETICLE_STATE")
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iState)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC SCALEFORM_SET_MISSILE_METER_PERCENTAGE(INT iPecentage)
	BEGIN_SCALEFORM_MOVIE_METHOD(sVehWeaponStruct.vehWeaponSF, "SET_MISSILE_PERCENTAGE")
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPecentage)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC DRAW_VEHICLE_WEAPON_SCALEFORM()
	
	IF IS_SCRIPT_STARTED_FROM_INTERIOR()
	
		IF NOT HAS_SCALEFORM_MOVIE_LOADED(sVehWeaponStruct.vehWeaponSF)
			sVehWeaponStruct.vehWeaponSF = REQUEST_SCALEFORM_MOVIE(GET_VEHICLE_WEAPON_SCALEFORM_NAME())
			CDEBUG1LN(DEBUG_NET_VEH_WEAPON, "[AM_MP_VEH_WEAPON] - DRAW_VEHICLE_WEAPON_SCALEFORM scaleform not loaded")
			EXIT
		ENDIF
		
		SCALEFORM_SET_ELEMENT_VISIBLE("SET_SHOCK_METER_IS_VISIBLE", FALSE)
		
		SCALEFORM_SET_ELEMENT_VISIBLE("SET_DETONATE_METER_IS_VISIBLE", FALSE)
		
		IF sVehWeaponStruct.iNumTargets > -1
		OR IS_BIT_SET(sVehWeaponStruct.iLocalBS, VEHICLE_WEAPON_LOCAL_SHOT_ALL_TARGETS)
			SCALEFORM_SET_ELEMENT_VISIBLE("SET_RETICLE_IS_VISIBLE", FALSE)
		ELSE
			SCALEFORM_SET_ELEMENT_VISIBLE("SET_RETICLE_IS_VISIBLE", TRUE)
		ENDIF
		
		SCALEFORM_SET_ELEMENT_VISIBLE("SET_HEADING_METER_IS_VISIBLE", TRUE)
		
		SCALEFORM_SET_ELEMENT_VISIBLE("SET_ZOOM_METER_IS_VISIBLE", TRUE)
		
		SCALEFORM_SET_ELEMENT_VISIBLE("SET_BOTTOM_LEFT_CORNER_IS_VISIBLE", FALSE)
		
		SCALEFORM_SET_ELEMENT_VISIBLE("SET_MISSILE_METER_IS_VISIBLE", TRUE) 
		
		SCALEFORM_SET_ELEMENT_VISIBLE("SET_INFO_LIST_IS_VISIBLE", FALSE)
		
		SCALEFORM_SET_ELEMENT_VISIBLE("SET_SHOCK_METER_IS_VISIBLE", FALSE)
		
		SCALEFORM_SET_ELEMENT_VISIBLE("SET_DETONATE_METER_IS_VISIBLE", FALSE)
		
		SCALEFORM_SET_ELEMENT_VISIBLE("SET_BOOST_METER_IS_VISIBLE", FALSE)

		SCALEFORM_SET_ELEMENT_VISIBLE("SET_SOUND_WAVE_IS_VISIBLE", FALSE)
		
		SCALEFORM_SET_ELEMENT_VISIBLE("SET_WARNING_IS_VISIBLE", FALSE)
		
		SCALEFORM_SET_RETICLE_ON_TARGET(FALSE)
		
		SCALEFORM_SET_RETICLE_STATE(0)
		
		// Drone zoom level
		MANAGE_DRONE_SCALEFORM_ZOOM_LEVEL()
		
		// Drone heading 
		IF DOES_CAM_EXIST(sVehWeaponStruct.VehWeaponCamera)
			VECTOR vCamRot = GET_CAM_ROT(sVehWeaponStruct.VehWeaponCamera)
			SCALEFORM_SET_HEADING(ROUND(-vCamRot.z))
		ENDIF
		
		SCALEFORM_SET_MISSILE_METER_PERCENTAGE(sVehWeaponStruct.iMissileFireMeter)
	ELSE
		IF DOES_CAM_EXIST(sVehWeaponStruct.VehWeaponCamera)
			VECTOR vCamRot = GET_CAM_ROT(sVehWeaponStruct.VehWeaponCamera)
			
			FLOAT fDrawAng = vCamRot.z

			FLOAT fArrowPos, fHeliHeading, fBarMinH, fBarMaxH, fBarCamH
			VECTOR vPos
			
			IF DOES_ENTITY_EXIST(GET_VEHICLE_WEAPON_WEAPONISED_VEHICLE())
				IF NOT IS_ENTITY_DEAD(GET_VEHICLE_WEAPON_WEAPONISED_VEHICLE())
					vPos = GET_ENTITY_COORDS(GET_VEHICLE_WEAPON_WEAPONISED_VEHICLE()) 
					fHeliHeading = GET_ENTITY_HEADING(GET_VEHICLE_WEAPON_WEAPONISED_VEHICLE()) 
				ENDIF
			ENDIF
			
			//Make camera more than 0
			WHILE fDrawAng < 0.0 		fDrawAng += 360.0 		ENDWHILE
			WHILE fDrawAng > 360.0 		fDrawAng -= 360.0 		ENDWHILE
			
			//Make Heli heading more than 0
			WHILE fHeliHeading < 0.0 	fHeliHeading += 360.0	ENDWHILE
			WHILE fHeliHeading > 360.0 	fHeliHeading -= 360.0 	ENDWHILE
			
			//The heading is = to the cam heading
			fBarCamH = fDrawAng
			
			FLOAT fMaxCameraYaw
			GET_MAX_CAM_YAW(fMaxCameraYaw)
			
			//Get the min heading 
			fBarMinH = fHeliHeading - fMaxCameraYaw
			//get the max heading
			fBarMaxH = fHeliHeading + fMaxCameraYaw
			
			//If its a radian out then move it
			IF fBarCamH < fBarMinH
			AND (fBarCamH + 360.0) <= fBarMaxH
				fBarCamH += 360.0
			ENDIF
			IF fBarCamH > fBarMaxH 
			AND (fBarCamH - 360.0) >= fBarMinH
				fBarCamH -= 360.0
			ENDIF
			
			//Get the current heading as a ratio between these and make it a %
			fArrowPos = (fBarCamH - fBarMinH)/(fBarMaxH - fBarMinH)
			
			//Cap it
			IF fArrowPos = 2.0 
				fArrowPos = 0.0
				//CDEBUG1LN(DEBUG_NET_GUN_TURRET, "fArrowPos          = 0.0 x")
			ELIF fArrowPos = -1.0 
				fArrowPos = 1.0
				//CDEBUG1LN(DEBUG_NET_GUN_TURRET, "fArrowPos          = 1.0 x")
			ELIF fArrowPos < 0.0 
			AND fArrowPos >= -0.5 
				//CDEBUG1LN(DEBUG_NET_GUN_TURRET, "fArrowPos          = 0.0")
				fArrowPos = 0.0
			ELIF fArrowPos < 0.0 
			AND fArrowPos > -1.0 
				//CDEBUG1LN(DEBUG_NET_GUN_TURRET, "fArrowPos          = 1.0 y")
				fArrowPos = 1.0
			ELIF fArrowPos > 1.5
			AND fArrowPos < 2.0 
				fArrowPos = 0.0
				//CDEBUG1LN(DEBUG_NET_GUN_TURRET, "fArrowPos          = 0.0 y")
			ELIF fArrowPos > 2.0
			OR fArrowPos < -1.0
				fArrowPos = 0.5
				//CDEBUG1LN(DEBUG_NET_GUN_TURRET, "fArrowPos          = 0.5")
			ENDIF
			
			
			BEGIN_SCALEFORM_MOVIE_METHOD(sVehWeaponStruct.vehWeaponSF, "SET_ALT_FOV_HEADING")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(vPos.z)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fArrowPos)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fDrawAng)
			END_SCALEFORM_MOVIE_METHOD()
			
			BEGIN_SCALEFORM_MOVIE_METHOD(sVehWeaponStruct.vehWeaponSF, "SET_CAM_LOGO")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
			END_SCALEFORM_MOVIE_METHOD()
		ENDIF	
	ENDIF
	
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
	DRAW_SCALEFORM_MOVIE_FULLSCREEN(sVehWeaponStruct.vehWeaponSF, 255, 255, 255, 0)
	
ENDPROC

PROC DRAW_VEHICLE_WEAPON_HELP_KEY()
	IF NOT IS_BIT_SET(sVehWeaponStruct.iLocalBS, VEHICLE_WEAPON_LOCAL_BS_START_INSTRUCTIONAL_PROMPT)
		IF LOAD_MENU_ASSETS()
			
			REMOVE_MENU_HELP_KEYS()
			
			ADD_MENU_HELP_KEY_GROUP(INPUTGROUP_SCRIPT_RSTICK_ALL, "BLIP_184")
			
			IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
				ADD_MENU_HELP_KEY_GROUP(INPUTGROUP_CURSOR_SCROLL, "CELL_284")
			ELSE	
				ADD_MENU_HELP_KEY_GROUP(INPUTGROUP_SCRIPT_LSTICK_ALL, "CELL_284")
			ENDIF	
			
			CONTROL_ACTION fireControl = INPUT_FRONTEND_RT
			IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
				fireControl = INPUT_CURSOR_ACCEPT
			ENDIF
			
			ADD_MENU_HELP_KEY_CLICKABLE(fireControl, "BLIP_39")
			ADD_MENU_HELP_KEY_CLICKABLE(INPUT_VEH_CIN_CAM, "MOVE_DRONE_RE")
			
			SET_BIT(sVehWeaponStruct.iLocalBS, VEHICLE_WEAPON_LOCAL_BS_START_INSTRUCTIONAL_PROMPT)
		ENDIF
	ELSE	
		DRAW_MENU_HELP_SCALEFORM(0) 
	ENDIF
	
	IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
		IF NOT IS_BIT_SET(sVehWeaponStruct.iLocalBS, VEHICLE_WEAPON_LOCAL_BS_SHOULD_SHOW_PC_PROMPTS)
			SET_BIT(sVehWeaponStruct.iLocalBS, VEHICLE_WEAPON_LOCAL_BS_SHOULD_SHOW_PC_PROMPTS)
			CLEAR_BIT(sVehWeaponStruct.iLocalBS, VEHICLE_WEAPON_LOCAL_BS_START_INSTRUCTIONAL_PROMPT)
		ENDIF	
	ELSE
		IF IS_BIT_SET(sVehWeaponStruct.iLocalBS, VEHICLE_WEAPON_LOCAL_BS_SHOULD_SHOW_PC_PROMPTS)
			CLEAR_BIT(sVehWeaponStruct.iLocalBS, VEHICLE_WEAPON_LOCAL_BS_SHOULD_SHOW_PC_PROMPTS)
			CLEAR_BIT(sVehWeaponStruct.iLocalBS, VEHICLE_WEAPON_LOCAL_BS_START_INSTRUCTIONAL_PROMPT)
		ENDIF
	ENDIF
	
ENDPROC

PROC DRAW_VEHICLE_WEAPON_HUD()

	DISABLE_CELLPHONE_CAMERA_APP_THIS_FRAME_ONLY()
	DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	
	DRAW_VEHICLE_WEAPON_SCALEFORM()
	DRAW_VEHICLE_WEAPON_HELP_KEY()
	SET_INSTRUCTIONAL_BUTTONS_UNDER_HUD_THIS_FRAME()
	SET_LOADING_ICON_SUBTITLES_OFFSET_SHIFT_THIS_FRAME(2) 
	
ENDPROC

FUNC VECTOR EULER_TO_DIRECTION_VECTOR(VECTOR vEuler)
	VECTOR vResult
	vResult.x = COS(vEuler.x)
	vResult.y = COS(vEuler.z)
	vResult.z = SIN(vEuler.x)
	vResult.y *= vResult.x
	vResult.x *= -SIN(vEuler.z)
	RETURN vResult
ENDFUNC

PROC MAINTAIN_VEHICLE_WEAPON_BLIP()
	
	IF !IS_RADAR_MAP_DISABLED()
		DISABLE_RADAR_MAP(TRUE)
	ENDIF
	
	SET_BIGMAP_ACTIVE(FALSE)
	
	IF IS_SCRIPT_STARTED_FROM_INTERIOR()
		HIDE_PLAYER_ARROW_THIS_FRAME()	
	ELSE
		IF DOES_ENTITY_EXIST(sVehWeaponStruct.VehWeaponVehicleIndex)
			SET_ENTITY_LOCALLY_INVISIBLE(sVehWeaponStruct.VehWeaponVehicleIndex)
		ENDIF	
	ENDIF	
	
	VECTOR vPos = GET_FINAL_RENDERED_CAM_COORD()
	IF IS_PAUSE_MENU_ACTIVE()
	OR IS_PAUSE_MENU_ACTIVE_EX()
	OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
	OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
	OR IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
		SET_FAKE_PAUSEMAP_PLAYER_POSITION_THIS_FRAME(vPos.x,vPos.y)
		SET_FAKE_GPS_PLAYER_POSITION_THIS_FRAME(vPos.x, vPos.y)
		IF IS_PAUSE_MENU_ACTIVE()
		OR IS_PAUSE_MENU_ACTIVE_EX()
			IF DOES_BLIP_EXIST(GET_MAIN_PLAYER_BLIP_ID())
				SET_BLIP_ALPHA(GET_MAIN_PLAYER_BLIP_ID(), 255)
			ENDIF
		ENDIF
	ELSE
		IF IS_SCRIPT_STARTED_FROM_INTERIOR()
			IF DOES_BLIP_EXIST(GET_MAIN_PLAYER_BLIP_ID())
				SET_BLIP_ALPHA(GET_MAIN_PLAYER_BLIP_ID(), 0)
			ENDIF
		ENDIF
	ENDIF
	
	LOCK_MINIMAP_POSITION(vPos.x,vPos.y)
	SET_RADAR_AS_EXTERIOR_THIS_FRAME()
	HIDE_MINIMAP_INTERIOR_MAP_THIS_FRAME()
	IF DOES_CAM_EXIST(sVehWeaponStruct.VehWeaponCamera)
		SET_CAM_CONTROLS_MINI_MAP_HEADING(sVehWeaponStruct.VehWeaponCamera, TRUE)
	ENDIF
	SET_RADAR_ZOOM(0)
	
	IF IS_SCRIPT_STARTED_FROM_INTERIOR()
		VECTOR vRot			
		vRot = GET_FINAL_RENDERED_CAM_ROT(EULER_XYZ)
		vRot = EULER_TO_DIRECTION_VECTOR(vRot)
		
		IF NOT DOES_BLIP_EXIST(sVehWeaponStruct.ArrowStrokeBlipID)
			sVehWeaponStruct.ArrowStrokeBlipBg = CREATE_BLIP_FOR_COORD(vPos)
			SET_BLIP_SPRITE(sVehWeaponStruct.ArrowStrokeBlipBg, RADAR_TRACE_CENTRE_STROKE)
			SHOW_HEIGHT_ON_BLIP(sVehWeaponStruct.ArrowStrokeBlipBg, FALSE)	
			SET_BLIP_DISPLAY(sVehWeaponStruct.ArrowStrokeBlipBg, DISPLAY_BOTH)
			SET_BLIP_COLOUR(sVehWeaponStruct.ArrowStrokeBlipBg,GET_BLIP_COLOUR_FROM_HUD_COLOUR(HUD_COLOUR_BLACK))	
			SET_BLIP_SHOW_CONE(sVehWeaponStruct.ArrowStrokeBlipBg, TRUE)
			SET_BLIP_SCALE(sVehWeaponStruct.ArrowStrokeBlipBg, 0.54)	
			SET_BLIP_PRIORITY(sVehWeaponStruct.ArrowStrokeBlipBg, INT_TO_ENUM(BLIP_PRIORITY, (ENUM_TO_INT(BLIPPRIORITY_OVER_CENTRE_BLIP)+1)))
			sVehWeaponStruct.ArrowStrokeBlipID = CREATE_BLIP_FOR_COORD(vPos)
			SET_BLIP_SPRITE(sVehWeaponStruct.ArrowStrokeBlipID, RADAR_TRACE_CENTRE_STROKE)
			SHOW_HEIGHT_ON_BLIP(sVehWeaponStruct.ArrowStrokeBlipID, FALSE)	
			SET_BLIP_DISPLAY(sVehWeaponStruct.ArrowStrokeBlipID, DISPLAY_BOTH)
			SET_BLIP_COLOUR(sVehWeaponStruct.ArrowStrokeBlipID,GET_BLIP_COLOUR_FROM_HUD_COLOUR(HUD_COLOUR_GREEN))
			SET_BLIP_SHOW_CONE(sVehWeaponStruct.ArrowStrokeBlipID, TRUE)
			SET_BLIP_SCALE(sVehWeaponStruct.ArrowStrokeBlipID, 0.44)	
			SET_BLIP_PRIORITY(sVehWeaponStruct.ArrowStrokeBlipID, INT_TO_ENUM(BLIP_PRIORITY, (ENUM_TO_INT(BLIPPRIORITY_OVER_CENTRE_BLIP)+1)))
			SET_BLIP_HIDDEN_ON_LEGEND(sVehWeaponStruct.ArrowStrokeBlipID, TRUE)
			SET_BLIP_ROTATION(sVehWeaponStruct.ArrowStrokeBlipID, ROUND(GET_HEADING_FROM_VECTOR_2D (vRot.x, vRot.y)))
			
			SET_BLIP_HIDDEN_ON_LEGEND(sVehWeaponStruct.ArrowStrokeBlipBg, TRUE)
			SET_BLIP_ROTATION(sVehWeaponStruct.ArrowStrokeBlipBg, ROUND(GET_HEADING_FROM_VECTOR_2D (vRot.x, vRot.y)))
		ELSE
			SET_BLIP_COORDS(sVehWeaponStruct.ArrowStrokeBlipID, vPos)
			SET_BLIP_ROTATION(sVehWeaponStruct.ArrowStrokeBlipID, ROUND(GET_HEADING_FROM_VECTOR_2D (vRot.x, vRot.y)))
			SET_BLIP_DISPLAY(sVehWeaponStruct.ArrowStrokeBlipID, DISPLAY_RADAR_ONLY)
			SET_BLIP_COORDS(sVehWeaponStruct.ArrowStrokeBlipBg, vPos)
			SET_BLIP_ROTATION(sVehWeaponStruct.ArrowStrokeBlipBg, ROUND(GET_HEADING_FROM_VECTOR_2D (vRot.x, vRot.y)))
			SET_BLIP_DISPLAY(sVehWeaponStruct.ArrowStrokeBlipBg, DISPLAY_RADAR_ONLY)
		ENDIF
	ENDIF	

ENDPROC

PROC MAINTAIN_INTERIOR_VEHICLE_WEAPON_CAMERA_CREATION()
	IF CAN_CREATE_VEHICLE_WEAPON_CAMERA()
		IF IS_PLAYER_PLAYING_VEHICLE_WEAPON_ANIMATION()
			IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_FREEZE_POSITION)
			ENDIF
			IF NOT HAS_NET_TIMER_STARTED(sVehWeaponStruct.sStartVehWeaponCamera)
				IF IS_ENTITY_ALIVE(GET_VEHICLE_WEAPON_WEAPONISED_VEHICLE())
					IF NEW_LOAD_SCENE_START_SPHERE(GET_ENTITY_COORDS(GET_VEHICLE_WEAPON_WEAPONISED_VEHICLE()), 100, NEWLOADSCENE_FLAG_REQUIRE_COLLISION)
						START_NET_TIMER(sVehWeaponStruct.sStartVehWeaponCamera)
						PRINTLN("[AM_MP_VEH_WEAPON] - MAINTAIN_INTERIOR_VEHICLE_WEAPON_CAMERA_CREATION DRONE_LOCAL_BS_START_DRONE_LOAD_SCENE TRUE")	
					ENDIF
				ELSE
					PRINTLN("[AM_MP_VEH_WEAPON] - MAINTAIN_INTERIOR_VEHICLE_WEAPON_CAMERA_CREATION vehicle is not alive")	
				ENDIF		
			ELIF HAS_NET_TIMER_EXPIRED(sVehWeaponStruct.sStartVehWeaponCamera, 2000)
				SET_FOCUS_ENTITY(GET_VEHICLE_WEAPON_WEAPONISED_VEHICLE())
			ENDIF	
		ELSE
			CDEBUG1LN(DEBUG_NET_VEH_WEAPON, "[AM_MP_VEH_WEAPON] - MAINTAIN_INTERIOR_VEHICLE_WEAPON_CAMERA_CREATION : waiting for animation")
		ENDIF
		
		IF HAS_NET_TIMER_STARTED(sVehWeaponStruct.sStartVehWeaponCamera)
			IF HAS_NET_TIMER_EXPIRED(sVehWeaponStruct.sStartVehWeaponCamera, 5000)
				IF IS_NEW_LOAD_SCENE_LOADED()
					IF NOT DOES_CAM_EXIST(sVehWeaponStruct.VehWeaponCamera)

						sVehWeaponStruct.VehWeaponCamera = CREATE_CAMERA(DEFAULT, TRUE)
						
						SET_CAM_FOV(sVehWeaponStruct.VehWeaponCamera, sVehWeaponStruct.fBaseCamFov)
						
						VECTOR vVehicleRot = GET_ENTITY_ROTATION(GET_VEHICLE_WEAPON_WEAPONISED_VEHICLE())
						SET_CAM_ROT(sVehWeaponStruct.VehWeaponCamera ,<<0,0, vVehicleRot.z>>)
						
						ATTACH_CAM_TO_ENTITY(sVehWeaponStruct.VehWeaponCamera, GET_VEHICLE_WEAPON_WEAPONISED_VEHICLE(), GET_VEHICLE_WEAPON_CAMERA_OFFSET())

						SET_LOCAL_PLAYER_USING_VEHICLE_WEAPON(TRUE)
						SET_PLAYER_USING_VEHICLE_WEAPON(TRUE)
						
						UPDATE_VEHICLE_WEAPON_CAMERA()
						
						CLEAR_TIMECYCLE_MODIFIER()
						
						START_AUDIO_SCENE("DLC_GR_MOC_Turret_View_Scene")
					ELSE
						IF	IS_BIT_SET(sVehWeaponStruct.iLocalBS, VEHICLE_WEAPON_LOCAL_UPDATE_FIRST_CAM)
							MAINTAIN_VEHICLE_WEAPON_BLIP()
							
							DRAW_VEHICLE_WEAPON_HUD()
							
							NEW_LOAD_SCENE_STOP()
							
							RENDER_SCRIPT_CAMS(TRUE, FALSE, 0 , TRUE, TRUE)
							
							IF sVehWeaponStruct.iBackgroundSoundID = -1
								sVehWeaponStruct.iBackgroundSoundID = GET_SOUND_ID()
								PLAY_SOUND_FRONTEND(sVehWeaponStruct.iBackgroundSoundID , "Turret_Camera_Hum_Loop", "DLC_BTL_Terrobyte_Turret_Sounds")
								CDEBUG1LN(DEBUG_NET_VEH_WEAPON, "[AM_MP_VEH_WEAPON] - Turret_Camera_Hum_Loop")
							ENDIF
							
							#IF FEATURE_GEN9_EXCLUSIVE
							SET_AUDIO_RAIN_HAPTIC_FLAG(TRUE)
							#ENDIF
							
							SET_VEH_WEAPON_STATE(VEH_WEAPON_STATE_UPDATE)
						ENDIF
					ENDIF
				ELSE
					PRINTLN("[AM_MP_VEH_WEAPON] - MAINTAIN_INTERIOR_VEHICLE_WEAPON_CAMERA_CREATION - waiting on load scene")
				ENDIF
			ENDIF	
		ENDIF	
	ELSE	
		SET_VEH_WEAPON_STATE(VEH_WEAPON_STATE_CLEANUP)
		PRINTLN("[AM_MP_VEH_WEAPON] - MAINTAIN_INTERIOR_VEHICLE_WEAPON_CAMERA_CREATION - CAN_CREATE_VEHICLE_WEAPON_CAMERA is false")
	ENDIF
ENDPROC

PROC MAINTAIN_VEHICLE_WEAPON_CAMERA_CREATION()
	IF CAN_CREATE_VEHICLE_WEAPON_CAMERA()
		
		IF NOT DOES_CAM_EXIST(sVehWeaponStruct.VehWeaponCamera)

			sVehWeaponStruct.VehWeaponCamera = CREATE_CAMERA(DEFAULT, TRUE)
			
			SET_CAM_FOV(sVehWeaponStruct.VehWeaponCamera, sVehWeaponStruct.fBaseCamFov)
			
			VECTOR vVehicleRot = GET_ENTITY_ROTATION(GET_VEHICLE_WEAPON_WEAPONISED_VEHICLE())
			SET_CAM_ROT(sVehWeaponStruct.VehWeaponCamera ,<<0,0, vVehicleRot.z>>)
			
			ATTACH_CAM_TO_ENTITY(sVehWeaponStruct.VehWeaponCamera, GET_VEHICLE_WEAPON_WEAPONISED_VEHICLE(), GET_VEHICLE_WEAPON_CAMERA_OFFSET())

			SET_LOCAL_PLAYER_USING_VEHICLE_WEAPON(TRUE)
			SET_PLAYER_USING_VEHICLE_WEAPON(TRUE)
			
			UPDATE_VEHICLE_WEAPON_CAMERA()
			
			CLEAR_TIMECYCLE_MODIFIER()
			
			//START_AUDIO_SCENE("DLC_GR_MOC_Turret_View_Scene")
		ELSE
			IF	IS_BIT_SET(sVehWeaponStruct.iLocalBS, VEHICLE_WEAPON_LOCAL_UPDATE_FIRST_CAM)
				MAINTAIN_VEHICLE_WEAPON_BLIP()
				
				DRAW_VEHICLE_WEAPON_HUD()
				
				RENDER_SCRIPT_CAMS(TRUE, FALSE, 0 , TRUE, TRUE)
				
				SET_TIMECYCLE_MODIFIER("eyeinthesky")
				
				IF sVehWeaponStruct.iBackgroundSoundID = -1
					sVehWeaponStruct.iBackgroundSoundID = GET_SOUND_ID()
					PLAY_SOUND_FRONTEND(sVehWeaponStruct.iBackgroundSoundID , "Turret_Camera_Hum_Loop", "DLC_BTL_Terrobyte_Turret_Sounds")
					CDEBUG1LN(DEBUG_NET_VEH_WEAPON, "[AM_MP_VEH_WEAPON] - Turret_Camera_Hum_Loop")
				ENDIF
				
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					sVehWeaponStruct.VehWeaponVehicleIndex = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					VECTOR vVehicleCoords = GET_ENTITY_COORDS(sVehWeaponStruct.VehWeaponVehicleIndex)
					CLEAR_PED_TASKS(PLAYER_PED_ID())
					TASK_HELI_MISSION(PLAYER_PED_ID(), sVehWeaponStruct.VehWeaponVehicleIndex, NULL, NULL, vVehicleCoords, MISSION_GOTO, 0, -1, -1, -1, -1)
					SET_AIRCRAFT_PILOT_SKILL_NOISE_SCALAR(sVehWeaponStruct.VehWeaponVehicleIndex, 0.0)
				ENDIF
				
				PRINT_HELP("HUNTGUN_2b")
				
				#IF FEATURE_GEN9_EXCLUSIVE
				SET_AUDIO_RAIN_HAPTIC_FLAG(TRUE)
				#ENDIF
				
				SET_VEH_WEAPON_STATE(VEH_WEAPON_STATE_UPDATE)
			ENDIF
		ENDIF
				
	ELSE	
		SET_VEH_WEAPON_STATE(VEH_WEAPON_STATE_CLEANUP)
		PRINTLN("[AM_MP_VEH_WEAPON] - MAINTAIN_VEHICLE_WEAPON_CAMERA_CREATION - CAN_CREATE_VEHICLE_WEAPON_CAMERA is false")
	ENDIF
ENDPROC

FUNC BOOL SHOULD_KILL_THIS_SCRIPT()
	IF NOT IS_SKYSWOOP_AT_GROUND()
		PRINTLN("[AM_MP_VEH_WEAPON] - SHOULD_KILL_THIS_SCRIPT - IS_SKYSWOOP_AT_GROUND is false - TRUE")
		RETURN TRUE
	ENDIF
	
	IF SHOULD_KILL_VEHICLE_WEAPON_SCRIPT()
		PRINTLN("[AM_MP_VEH_WEAPON] - SHOULD_KILL_THIS_SCRIPT - SHOULD_KILL_VEHICLE_WEAPON_SCRIPT is true - TRUE")
		RETURN TRUE
	ENDIF
	
	IF !IS_SCRIPT_STARTED_FROM_INTERIOR()
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF !SHOULD_START_VEHICLE_WEAPON_SCRIPT_FOR_THIS_WEAPON(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
				PRINTLN("[AM_MP_VEH_WEAPON] - SHOULD_KILL_THIS_SCRIPT - not started from interior SHOULD_START_VEHICLE_WEAPON_SCRIPT_FOR_THIS_WEAPON is false - TRUE")
				RETURN TRUE
			ENDIF
		ELSE
			PRINTLN("[AM_MP_VEH_WEAPON] - SHOULD_KILL_THIS_SCRIPT - not started from interior and not in vehicle - TRUE")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC	

FUNC BOOL SHOULD_RESET_THIS_SCRIPT() 
	IF IS_LOCAL_PLAYER_USING_VEHICLE_WEAPON()
	AND NOT SHOULD_PAUSE_VEH_WEAPON()
		IF IS_SELECTOR_UI_BUTTON_PRESSED(FALSE)
		OR IS_SELECTOR_ONSCREEN()
		OR IS_REPLAY_RECORDING_FEED_BUTTONS_ONSCREEN()
			// Skip
		ELSE
			IF IS_SCRIPT_STARTED_FROM_INTERIOR()
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_VEH_CIN_CAM)
				OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_VEH_CIN_CAM)
					PRINTLN("[AM_MP_VEH_WEAPON] - SHOULD_RESET_THIS_SCRIPT - player pressed circle - TRUE")
					RETURN TRUE
				ENDIF
			ELSE
				IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_CONTEXT)
				OR IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_CONTEXT)
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), sVehWeaponStruct.VehWeaponVehicleIndex)
						SET_AIRCRAFT_PILOT_SKILL_NOISE_SCALAR(sVehWeaponStruct.VehWeaponVehicleIndex, 1.0)
						CLEAR_PED_TASKS(PLAYER_PED_ID())
					ENDIF	
					PRINTLN("[AM_MP_VEH_WEAPON] - SHOULD_RESET_THIS_SCRIPT - player pressed circle - TRUE")
					RETURN TRUE
				ENDIF
				
				IF IS_ENTITY_ALIVE(GET_VEHICLE_WEAPON_WEAPONISED_VEHICLE())
					IF DOES_CAM_EXIST(sVehWeaponStruct.VehWeaponCamera)
						VECTOR vCamCoords = GET_CAM_COORD(sVehWeaponStruct.VehWeaponCamera)
						FLOAT fCamZ
						IF GET_GROUND_Z_FOR_3D_COORD(vCamCoords, fCamZ)
							IF VDIST(vCamCoords , <<vCamCoords.x,vCamCoords.y,fCamZ>>) < 2.0
								PRINTLN("[AM_MP_VEH_WEAPON] - SHOULD_RESET_THIS_SCRIPT - vehicle close to ground - TRUE")
								RETURN TRUE
							ENDIF
						ENDIF	
					ENDIF
					
					IF IS_ENTITY_IN_WATER(GET_VEHICLE_WEAPON_WEAPONISED_VEHICLE())
						PRINTLN("[AM_MP_VEH_WEAPON] - SHOULD_RESET_THIS_SCRIPT - vehicle in water - TRUE")
						RETURN TRUE
					ENDIF	
					
					IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), sVehWeaponStruct.VehWeaponVehicleIndex)
						SET_AIRCRAFT_PILOT_SKILL_NOISE_SCALAR(sVehWeaponStruct.VehWeaponVehicleIndex, 1.0)
						CLEAR_PED_TASKS(PLAYER_PED_ID())
					ENDIF	
				ELSE
					PRINTLN("[AM_MP_VEH_WEAPON] - SHOULD_RESET_THIS_SCRIPT - vehicle not alive - TRUE")
					RETURN TRUE
				ENDIF
			ENDIF
			
			IF IS_VEHICLE_WEAPON_WEAPONISED_VEHICLE_DRIVER_IN_PASSIVE_MODE()
				PRINTLN("[AM_MP_VEH_WEAPON] - SHOULD_RESET_THIS_SCRIPT - driver in passive mode - TRUE")
				RETURN TRUE
			ENDIF
			
		ENDIF	
	ENDIF
	
	IF IS_PLAYER_WALKING_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
		PRINTLN("[AM_MP_VEH_WEAPON] - SHOULD_RESET_THIS_SCRIPT - IS_PLAYER_WALKING_OUT_OF_SIMPLE_INTERIOR is true - TRUE")
		RETURN TRUE
	ENDIF
	
	IF IS_VEHICLE_WEAPON_OWNER_TAKING_VEHICLE_IN_OR_OUT_OF_PROPERTY()
		PRINTLN("[AM_MP_VEH_WEAPON] - SHOULD_RESET_THIS_SCRIPT - IS_VEHICLE_WEAPON_OWNER_TAKING_VEHICLE_IN_OR_OUT_OF_PROPERTY is true - TRUE")
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_IN_HACKER_TRUCK(PLAYER_ID())
		IF IS_ENTITY_ALIVE(GET_HACKER_TRUCK_INDEX_PED_IS_IN_INTERIOR())
			IF GET_FRAME_COUNT() % 30 = 0 
				IF IS_ENTITY_IN_ANGLED_AREA( GET_HACKER_TRUCK_INDEX_PED_IS_IN_INTERIOR(), <<-136.161423,4617.175781,124.513405>>, <<-490.305756,4924.749023,159.067673>>, 19.500000)
					PRINTLN("[AM_MP_VEH_WEAPON] - SHOULD_RESET_THIS_SCRIPT - truck is in tunnel")
					RETURN TRUE
				ENDIF
			ENDIF	
			IF IS_ENTITY_UPSIDEDOWN(GET_HACKER_TRUCK_INDEX_PED_IS_IN_INTERIOR())
				PRINTLN("[AM_MP_VEH_WEAPON] - SHOULD_RESET_THIS_SCRIPT - truck is in UPSIDEDOWN")
				RETURN TRUE
			ENDIF	
		ELSE
			PRINTLN("[AM_MP_VEH_WEAPON] - SHOULD_RESET_THIS_SCRIPT - truck doesn't exist")
			RETURN TRUE
		ENDIF
	ENDIF	
		
	RETURN FALSE
ENDFUNC

PROC RUN_MAIN_CLIENT_LOGIC()

	MAINTAIN_VEHICLE_WEAPON_ENVIROMENT_CONTROL()
	
	IF IS_VEH_WEAPON_STATE(VEH_WEAPON_STATE_IDLE)
		MAINTAIN_VEHICLE_WEAPON_TRIGGER()
	ELIF IS_VEH_WEAPON_STATE(VEH_WEAPON_STATE_INIT)
		IF IS_SCRIPT_STARTED_FROM_INTERIOR()
			MAINTAIN_INTERIOR_VEHICLE_WEAPON_CAMERA_CREATION()
		ELSE
			MAINTAIN_VEHICLE_WEAPON_CAMERA_CREATION()
		ENDIF
	ELIF IS_VEH_WEAPON_STATE(VEH_WEAPON_STATE_UPDATE)
		IF SHOULD_RESET_THIS_SCRIPT()
			SET_VEH_WEAPON_STATE(VEH_WEAPON_STATE_CLEANUP)
		ENDIF
		
		MAINTAIN_VEHICLE_WEAPON_BLIP()
		
		IF SHOULD_PAUSE_VEH_WEAPON()
			EXIT
		ENDIF
		
		UPDATE_VEHICLE_WEAPON_CAMERA()
		MAINTAIN_WEAPON_PROP_DIRECTION()
		
		PROCESS_VEHICLE_WEAPONS()
		MAINTAIN_VEHICLE_WEAPON_SHOOTING()
		PROCESS_TARGET_IN_SIGHT_SHAPETEST() 	

		DRAW_VEHICLE_WEAPON_HUD()
	
	ELIF IS_VEH_WEAPON_STATE(VEH_WEAPON_STATE_CLEANUP)
		SCRIPT_CLEANUP(TRUE)
	ENDIF
	
ENDPROC

PROC RUN_MAIN_SERVER_LOGIC()
	
ENDPROC

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡    MAIN LOOP    ╞═════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

SCRIPT  (MP_MISSION_DATA missionScriptArgs) 
	
	IF NETWORK_IS_GAME_IN_PROGRESS() 
		SCRIPT_INITIALISE(missionScriptArgs)	
	ELSE
		SCRIPT_CLEANUP()
	ENDIF
	
	// Main loop
	WHILE TRUE

		MP_LOOP_WAIT_ZERO()
			
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			PRINTLN("[AM_MP_VEH_WEAPON] - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE = TRUE")
			SCRIPT_CLEANUP()
		ENDIF
		
		IF SHOULD_KILL_THIS_SCRIPT()
			PRINTLN("[AM_MP_VEH_WEAPON] - SHOULD_KILL_THIS_SCRIPT = TRUE")
			SCRIPT_CLEANUP()
		ENDIF
		
		RUN_MAIN_CLIENT_LOGIC()

		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			RUN_MAIN_SERVER_LOGIC()
		ENDIF
		
		#IF IS_DEBUG_BUILD
		UPDATE_DEBUG_WIDGETS()
		#ENDIF
		
	ENDWHILE
ENDSCRIPT

