//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        AM_MP_MUSIC_STUDIO.sc																		//
// Description: Script for managing the interior of the solomons office. access, spawning etc. 				//
//				is managed by AM_MP_SMPL_INTERIOR_* script while this script is launched by simple interior	//
//				script to handle anything specific to the office.											//
// Written by:  Online Technical Team: Mark Richardson,														//
// Date:  		11/02/2020																					//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_path.sch"
USING "commands_event.sch"
USING "net_include.sch"
USING "net_events.sch"
USING "net_spawn.sch"
USING "net_ambience.sch"
USING "net_wait_zero.sch"
USING "net_collectables_pickups.sch"
USING "net_activity_creator_activities.sch"
USING "net_MP_CCTV.sch"
USING "net_arcade_cabinet.sch"
USING "drunk_public.sch"

#IF FEATURE_MUSIC_STUDIO
USING "net_simple_interior.sch"
USING "net_simple_interior_music_studio.sch"
USING "net_peds_script_management.sch"
USING "net_music_studio_engineer_screens.sch"
USING "net_dr_dre_appearance_controller.sch"

USING "net_dj.sch"
USING "net_club_music.sch"

#IF IS_DEBUG_BUILD
USING "net_debug_log.sch"
#ENDIF

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ VARIABLES ╞═════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

//////////////////////////////////////////////////////////////
// LOCAL SCRIPT BIT SETS

//m_InteriorData.ibs
CONST_INT BS_MUSIC_STUDIO_INTERIOR_REFRESH_ON_INIT					0
CONST_INT BS_MUSIC_STUDIO_LOAD_SCENE_STARTED						1
CONST_INT BS_MUSIC_STUDIO_CALLED_CLEAR_HELP							2
CONST_INT BS_MUSIC_STUDIO_SEATING_LAUNCHED							4
CONST_INT BS_MUSIC_STUDIO_DOORS_ADDED_TO_SYSTEM						5
CONST_INT BS_MUSIC_STUDIO_DOORS_LOCKED_FOR_DRUMS_RECORDING			6
CONST_INT BS_MUSIC_STUDIO_GIVEN_VISIT_STUDIO_AWARD					7
CONST_INT BS_MUSIC_STUDIO_ENTERED_INTERIOR_FROM_SMOKING_ROOM		8
CONST_INT BS_MUSIC_STUDIO_TOGGLED_SMOKING_ROOM_DOOR_COLLISION		9
CONST_INT BS_MUSIC_STUDIO_SET_MODEL_HIDES_FOR_DRE_PERFORMANCE		10
CONST_INT BS_MUSIC_STUDIO_SET_MODEL_HIDES_FOR_DRUMS					11
CONST_INT BS_MUSIC_STUDIO_SET_MODEL_OVERLAY_FOR_RECORDING_LIGHT		12
CONST_INT BS_MUSIC_STUDIO_RESET_TIMER_AWARD_PRODUCER				13
CONST_INT BS_MUSIC_STUDIO_ENTOURAGE_GIRL_COLLIDER_CREATED			14
CONST_INT BS_MUSIC_STUDIO_DRE_CHAIR_SPAWNED							15
CONST_INT BS_MUSIC_STUDIO_SET_INVISIBLE_FOR_MOCAP					16
CONST_INT BS_MUSIC_STUDIO_WATCHED_MOCAP								17
CONST_INT BS_MUSIC_STUDIO_WATCHING_MOCAP							18
CONST_INT BS_MUSIC_STUDIO_SMOKING_ACTIVITY_STARTED					19
CONST_INT BS_MUSIC_STUDIO_DISPLAYED_MIXER_DESK_HELP_TEXT			20
CONST_INT BS_MUSIC_STUDIO_MIXING_ROOM_DOORS_CLOSED_FOR_CUTSCENE		21
CONST_INT BS_MUSIC_STUDIO_SET_AUDIO_SCENE_FOR_DRE_PERFORMANCE		22
CONST_INT BS_MUSIC_STUDIO_CREATED_RESTRICTED_AREA_BLOCKERS			23
CONST_INT BS_MUSIC_STUDIO_WARPING_OUT_OF_RESTRICTED_AREA			24
CONST_INT BS_MUSIC_STUDIO_WARPING_OUT_OF_RESTRICTED_AREA_DRUMS		25
CONST_INT BS_MUSIC_STUDIO_WARPING_OUT_OF_RESTRICTED_AREA_VOCALS		26
CONST_INT BS_MUSIC_STUDIO_WARPING_OUT_OF_RESTRICTED_AREA_LOBBY		27
CONST_INT BS_MUSIC_STUDIO_WARPING_OUT_OF_RESTRICTED_AREA_HALLWAY_LEFT		28
CONST_INT BS_MUSIC_STUDIO_WARPING_OUT_OF_RESTRICTED_AREA_HALLWAY_RIGHT		29

CONST_INT ciMUSIC_STUDIO_WORK_NUM_RECORDING_LIGHTS					3
//////////////////////////////////////////////////////////////
// BROADCAST DATA BIT SETS

// m_PlayerBD.iBS - Use with SET_LOCAL_PLAYER_BROADCAST_BIT
CONST_INT BS_MUSIC_STUDIO_PLAYER_BD_READY_TO_WARP_OUT					0

/// End bit set const INTs
//////////////////////////////////////////////////////////////

// iServerBS
//CONST_INT SERVER_BS_							0

CONST_INT MUSIC_STUDIO_ROOM_KEY 										-601456399
CONST_INT MUSIC_STUDIO_ENTITY_SET_BITSET_ARRAY_SIZE						1
CONST_INT MAX_HIDDEN_SCENE_OBJECTS 										1
CONST_INT MAX_INTRO_CUTSCENE_FALLBACK_TIME								60000 // 1 minute
CONST_INT MUSIC_STUDIO_MOCAP_CUTSCENE_EARLY_FADE_TIME					500

ENUM MUSIC_STUDIO_SERVER_STATE
	MUSIC_STUDIO_SERVER_STATE_LOADING,
	MUSIC_STUDIO_SERVER_STATE_IDLE
ENDENUM

ENUM MUSIC_STUDIO_CLIENT_STATE
	MUSIC_STUDIO_STATE_LOAD_INTERIOR,
	MUSIC_STUDIO_STATE_MOCAP,
	MUSIC_STUDIO_STATE_CREATE_ENTITIES,
	MUSIC_STUDIO_STATE_IDLE
ENDENUM

ENUM MUSIC_STUDIO_ENTITY_SET_ID
	MUSIC_STUDIO_SET_INVALID = -1,
	MUSIC_STUDIO_SET_DEFAULT,
	MUSIC_STUDIO_SET_STU_EXT_P1,
	MUSIC_STUDIO_SET_STU_EXT_P3A1,
	MUSIC_STUDIO_SET_TRIP1_INT_P2, // 3
	
	MUSIC_STUDIO_SET_COUNT // Increase MUSIC_STUDIO_ENTITY_SET_BITSET_ARRAY_SIZE if more than 31 enum values.
ENDENUM

STRUCT RESTRICTED_AREA_DETAILS
	VECTOR vAngledAreaA
	VECTOR vAngledAreaB
	FLOAT fAngledAreaWidth
	VECTOR vGotoCoords
	STRING sKickHelp
	BOOL bKickOut = FALSE
	INT iAdditionalHelpInt = -1
ENDSTRUCT

ENUM RESTRICTED_AREAS
	RA_DOOR_1,
	RA_DOOR_2,
	RA_DOOR_3,
	RESTRICTED_AREA_COUNT
ENDENUM

ENUM MUSIC_STUDIO_SPAWNED_OBJECTS
	MUSIC_STUDIO_INVISIBLE_PED_COLLISION_LAMAR,
	MUSIC_STUDIO_INVISIBLE_PED_COLLISION_FRANKLIN,
	MUSIC_STUDIO_INVISIBLE_PED_COLLISION_ENTOURAGE,
	MUSIC_STUDIO_INVISIBLE_PED_COLLISION_DR_DRE1,
	MUSIC_STUDIO_INVISIBLE_PED_COLLISION_DR_DRE2,
	MUSIC_STUDIO_INVISIBLE_PED_COLLISION_DR_DRE_LEFT,
	MUSIC_STUDIO_INVISIBLE_PED_COLLISION_DR_DRE_RIGHT,
	MUSIC_STUDIO_INVISIBLE_PED_COLLISION_VOCALS1,
	MUSIC_STUDIO_INVISIBLE_PED_COLLISION_PATROL_1,
	MUSIC_STUDIO_INVISIBLE_PED_COLLISION_PATROL_2,
	MUSIC_STUDIO_INVISIBLE_PED_COLLISION_PATROL_3,
	MUSIC_STUDIO_INVISIBLE_PED_COLLISION_COUNT 
ENDENUM

ENUM MUSIC_STUDIO_MOCAP_CUTSCENE
	MUSIC_STUDIO_MOCAP_CUTSCENE_WEED_ROOM_INTRO,
	MUSIC_STUDIO_MOCAP_CUTSCENE_WEED_ROOM_ENTER,
	MUSIC_STUDIO_MOCAP_CUTSCENE_WEED_ROOM_EXIT
ENDENUM

ENUM MUSIC_STUDIO_MOCAP_CUTSCENE_STATE
	MUSIC_STUDIO_MOCAP_CUTSCENE_STATE_INITIALISE,
	MUSIC_STUDIO_MOCAP_CUTSCENE_STATE_ASSET_REQUEST,
	MUSIC_STUDIO_MOCAP_CUTSCENE_STATE_START_CUTSCENE,
	MUSIC_STUDIO_MOCAP_CUTSCENE_STATE_IDLING,
	MUSIC_STUDIO_MOCAP_CUTSCENE_STATE_PLAY_INTERIOR,
	MUSIC_STUDIO_MOCAP_CUTSCENE_STATE_CLEANUP,
	MUSIC_STUDIO_MOCAP_CUTSCENE_STATE_WAIT_FOR_INTERIOR_READY
ENDENUM

STRUCT ServerBroadcastData
	MUSIC_STUDIO_SERVER_STATE eState = MUSIC_STUDIO_SERVER_STATE_LOADING	
	INT iServerBS
	SCRIPT_TIMER tWorkStateTimer
	
	SERVER_CREATOR_ACTIVITY_PEDS activityPeds[iMaxCreatorActivities]
	SERVER_CREATOR_ACTIVITY_PROPS activityProps[iMaxCreatorActivities]
		
	DJ_SERVER_DATA DJServerData
ENDSTRUCT

STRUCT PlayerBroadcastData
	INT iBS
	
	MP_CCTV_CLIENT_DATA_STRUCT MPCCTVClient
ENDSTRUCT

STRUCT MUSIC_STUDIO_DATA
	//Bit sets
	INT iBS
	INT iEntitySetBS[MUSIC_STUDIO_ENTITY_SET_BITSET_ARRAY_SIZE]
	
	//Script state
	INT iScriptInstance
	MUSIC_STUDIO_CLIENT_STATE eState
	
	//Interior data
	SIMPLE_INTERIORS eSimpleInteriorID = SIMPLE_INTERIOR_MUSIC_STUDIO
	SIMPLE_INTERIOR_DETAILS	SimpleInteriorDetails
	INTERIOR_INSTANCE_INDEX iInteriorID
	VECTOR vInteriorPosition
	FLOAT fInteriorHeading
	TEXT_LABEL_63 sInteriorType
	BOOL bScriptRelaunched
		
	//Loading data
	SCRIPT_TIMER tPinInMemTimer
	INT iPinInMemTimeToExpire = 10000
	
	// Ped data
	BOOL bPedScriptLaunched = FALSE
	SCRIPT_TIMER stPedLaunchingBailTimer
	
	MP_CCTV_LOCAL_DATA_STRUCT MPCCTVLocal
	
	DJ_LOCAL_DATA DJLocalData
	
	BLIP_INDEX blipSmokingRoom
	BLIP_INDEX blipSmokingRoomCouch
	
	BLIP_INDEX blipClothing
	
	OBJECT_INDEX objSmokingRoomDoor
	
	OBJECT_INDEX objRecordingLights[ciMUSIC_STUDIO_WORK_NUM_RECORDING_LIGHTS]
	
	//Adding chair prop when dre's absent
	OBJECT_INDEX objDreChair
	// Awards
	SCRIPT_TIMER timerAwardProducer
	SCRIPT_TIMER tempTimerAwardProducer
	INT iSavedTimeAwardProducer
	INT iCurrentTimeAwardProducer
	
	//Collision for peds
	OBJECT_INDEX objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_COUNT]
		
	//screens
	SCREEN_DATA sEngineerScreenData
	SCREEN_DATA sFranklinScreenData
	//SCREEN_DATA sPoohScreenData
	SCREEN_DATA sStaffScreenData
	
	//FOR KICKING PLAYERS OUT OF THE AREA
	INT iRestrictedAreaLoop
	RESTRICTED_AREAS eAreaLastKickedOutOf = RESTRICTED_AREA_COUNT
	
	// Mocap Data
	MUSIC_STUDIO_MOCAP_CUTSCENE eMocapScene = MUSIC_STUDIO_MOCAP_CUTSCENE_WEED_ROOM_INTRO
	MUSIC_STUDIO_MOCAP_CUTSCENE_STATE eMusicStudioSceneState
	OBJECT_INDEX sceneHiddenProps[MAX_HIDDEN_SCENE_OBJECTS]
	PED_INDEX scenePed
	BOOL bIdleTimeout
	SCRIPT_TIMER tIdleFallback
	SCRIPT_TIMER timeFailSafe
	SCRIPT_TIMER tRefreshTimer
	
	SCRIPT_TIMER restrictedHelpTimer
ENDSTRUCT
#IF IS_DEBUG_BUILD
STRUCT MUSIC_STUDIO_DEBUG_DATA
	BOOL bBypassCCTVCheck
	DJ_DEBUG_DATA DJDebugData
	
	// Awards
	BOOL bDrawAwardValuesProducer
	BOOL bResetAwardProducer
	BOOL bAddMinutesToAwardProducer
	INT iAddMinutesToAwardProducer
	INT iTimeHours 
	INT iTimeMinutes
	INT iTimeSeconds
	INT iNextPosix = -2
	BOOL bBlockDreToday = FALSE
	TEXT_WIDGET_ID sNextPerformance
ENDSTRUCT
MUSIC_STUDIO_DEBUG_DATA debugData
#ENDIF

MUSIC_STUDIO_DATA m_InteriorData
ServerBroadcastData m_ServerBD
PlayerBroadcastData m_PlayerBD[NUM_NETWORK_PLAYERS]

ACTIVITY_INTERIOR_STRUCT interiorStruct
CHECK_CREATOR_ACTIVITIES_STRUCT activityCheck[iMaxCreatorActivities]
ACTIVITY_CONTROLLER_STRUCT activityControllerStruct
PED_VAR_ID_STRUCT pedLocalVariationsStruct[iMaxCreatorActivities]
BUNKER_FACTORY_ACTIVITY_STRUCT bunkerFactoryActivityStruct

ACM_LAUNCHER_STRUCT eArcadeCabinetManager 

FUNC BOOL STORE_MUSIC_STUDIO_PROPERTY_INTERIOR_ID()
	TEXT_LABEL_63 tl63InteriorType
	VECTOR vInteriorPosition
	FLOAT fInteriorHeading
	
	GET_SIMPLE_INTERIOR_INTERIOR_TYPE_AND_POSITION(m_InteriorData.eSimpleInteriorID, tl63InteriorType, vInteriorPosition, fInteriorHeading)
	m_InteriorData.iInteriorID = GET_INTERIOR_AT_COORDS_WITH_TYPE(vInteriorPosition, tl63InteriorType)
	
	RETURN IS_VALID_INTERIOR(m_InteriorData.iInteriorID)
ENDFUNC

// ***************************************************************************************
// 					ENTITY SETS
// ***************************************************************************************

#IF IS_DEBUG_BUILD

/// PURPOSE:
///    Gets the name of the enum in MUSIC_STUDIO_ENTITY_SET_ID, for log prints.
/// PARAMS:
///    eMusicStudioSetID - Music Studio entity set id
DEBUGONLY FUNC STRING DEBUG_GET_MUSIC_STUDIO_ENTITY_SET_ENUM_STRING(MUSIC_STUDIO_ENTITY_SET_ID eMusicStudioSetID)
	SWITCH eMusicStudioSetID
		CASE MUSIC_STUDIO_SET_INVALID					RETURN "MUSIC_STUDIO_SET_INVALID"
		CASE MUSIC_STUDIO_SET_STU_EXT_P1				RETURN "MUSIC_STUDIO_SET_STU_EXT_P1"
		CASE MUSIC_STUDIO_SET_STU_EXT_P3A1				RETURN "MUSIC_STUDIO_SET_STU_EXT_P3A1"
		CASE MUSIC_STUDIO_SET_TRIP1_INT_P2				RETURN "MUSIC_STUDIO_SET_TRIP1_INT_P2"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

#ENDIF

/// PURPOSE:
///    Queries if the MUSIC_STUDIO entity set bit has been set. Determines if the entity set is active.
/// PARAMS:
///    eMusicStudioSetID - Entity set to query.
///    iEntitySetBS - The entity set bitset.
FUNC BOOL IS_MUSIC_STUDIO_ENTITY_SET_BIT_ACTIVE(MUSIC_STUDIO_ENTITY_SET_ID eMusicStudioSetID, INT &iEntitySetBS[MUSIC_STUDIO_ENTITY_SET_BITSET_ARRAY_SIZE])
	INT iArrayIndex = (ENUM_TO_INT(eMusicStudioSetID)/32)
	INT iBit		= (ENUM_TO_INT(eMusicStudioSetID)%32)
	RETURN IS_BIT_SET(iEntitySetBS[iArrayIndex], iBit)
ENDFUNC

/// PURPOSE:
///    Toggles the entity set active bit set. States whether an entity set is active.
/// PARAMS:
///    eMusicStudioSetID - Entity set to toggle.
///    iEntitySetBS - The entity set bitset.
///    bSet - Entity set active state.
PROC TOGGLE_MUSIC_STUDIO_ENTITY_SET_ACTIVE_BIT(MUSIC_STUDIO_ENTITY_SET_ID eMusicStudioSetID, INT &iEntitySetBS[MUSIC_STUDIO_ENTITY_SET_BITSET_ARRAY_SIZE], BOOL bSet)
	INT iArrayIndex = (ENUM_TO_INT(eMusicStudioSetID)/32)
	INT iBit		= (ENUM_TO_INT(eMusicStudioSetID)%32)
	
	IF bSet
		SET_BIT(iEntitySetBS[iArrayIndex], iBit)
		PRINTLN("TOGGLE_MUSIC_STUDIO_ENTITY_SET_ACTIVE_BIT - Setting Entity Set: ", DEBUG_GET_MUSIC_STUDIO_ENTITY_SET_ENUM_STRING(eMusicStudioSetID), " Bit: ", iBit)
	ELSE
		CLEAR_BIT(iEntitySetBS[iArrayIndex], iBit)
		PRINTLN("TOGGLE_MUSIC_STUDIO_ENTITY_SET_ACTIVE_BIT - Clearing Entity Set: ", DEBUG_GET_MUSIC_STUDIO_ENTITY_SET_ENUM_STRING(eMusicStudioSetID), " Bit: ", iBit)
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks to see if the music studio upgrade enum id is a valid entity set.
/// PARAMS:
///    eMusicStudioSetID - music studio entity set enum
FUNC BOOL IS_MUSIC_STUDIO_ENTITY_SET_VALID(MUSIC_STUDIO_ENTITY_SET_ID eMusicStudioSetID)
	RETURN (eMusicStudioSetID != MUSIC_STUDIO_SET_INVALID  AND eMusicStudioSetID != MUSIC_STUDIO_SET_COUNT)
ENDFUNC

/// PURPOSE:
///    Gets the name of the entity set provided by the artists. 
/// PARAMS:
///    eMusicStudioSetID - music studio entity set enum
FUNC STRING GET_MUSIC_STUDIO_ENTITY_SET(MUSIC_STUDIO_ENTITY_SET_ID eMusicStudioSetID = MUSIC_STUDIO_SET_INVALID)
	IF NOT IS_MUSIC_STUDIO_ENTITY_SET_VALID(eMusicStudioSetID)
		ASSERTLN("[GET_MUSIC_STUDIO_ENTITY_SET] Entity set isn't valid. Entity Set: ", DEBUG_GET_MUSIC_STUDIO_ENTITY_SET_ENUM_STRING(eMusicStudioSetID))
		RETURN ""
	ENDIF
	
	SWITCH eMusicStudioSetID
		CASE MUSIC_STUDIO_SET_DEFAULT				RETURN "entity_set_default"
		CASE MUSIC_STUDIO_SET_STU_EXT_P1			RETURN "Entity_Set_FIX_STU_EXT_P1"
		CASE MUSIC_STUDIO_SET_STU_EXT_P3A1			RETURN "Entity_Set_FIX_STU_EXT_P3A1"
		CASE MUSIC_STUDIO_SET_TRIP1_INT_P2			RETURN "Entity_Set_FIX_TRIP1_INT_P2"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

/// PURPOSE:
///    Wrapper for removing the entity set, then adding it if the associated bit is set.
/// PARAMS:
///    eMusicStudioSetID - The music stduio entity set id 
PROC REFRESH_MUSIC_STUDIO_PROPERTY_ENTITY_SET(MUSIC_STUDIO_ENTITY_SET_ID eMusicStudioSetID)
	IF IS_MUSIC_STUDIO_ENTITY_SET_VALID(eMusicStudioSetID)
		STRING strEntitySet = GET_MUSIC_STUDIO_ENTITY_SET(eMusicStudioSetID)
		REMOVE_SIMPLE_INTERIOR_ENTITY_SET(m_InteriorData.eSimpleInteriorID, strEntitySet, m_InteriorData.iInteriorID)
		IF IS_PROPERTY_ENTITY_SET_ACTIVE_BIT_SET(ENUM_TO_INT(eMusicStudioSetID), m_InteriorData.iEntitySetBS)
			ADD_SIMPLE_INTERIOR_ENTITY_SET(m_InteriorData.eSimpleInteriorID, strEntitySet, DEFAULT, m_InteriorData.iInteriorID)
		ENDIF
	ELSE
		ASSERTLN("[REFRESH_MUSIC_STUDIO_PROPERTY_ENTITY_SET] Invalid entity set! eMusicStudioSetID: ", DEBUG_GET_MUSIC_STUDIO_ENTITY_SET_ENUM_STRING(eMusicStudioSetID))
	ENDIF
ENDPROC

/// PURPOSE:
///    Activates the MUSIC_STUDIO entity sets.
///    This function doesn't contain any logic. Purely activates/removes the entity sets based on their bit state.
///    Default entity sets are toggled within: SET_MUSIC_STUDIO_ENTITY_SET_DATA()
PROC SET_MUSIC_STUDIO_ENTITY_SETS()
	IF IS_VALID_INTERIOR(m_InteriorData.iInteriorID)
		INT iEntitySetEnum
		REPEAT MUSIC_STUDIO_SET_COUNT iEntitySetEnum
			MUSIC_STUDIO_ENTITY_SET_ID eMusicStudioSetID = INT_TO_ENUM(MUSIC_STUDIO_ENTITY_SET_ID, iEntitySetEnum)
			IF IS_MUSIC_STUDIO_ENTITY_SET_VALID(eMusicStudioSetID)
				PRINTLN("[SET_MUSIC_STUDIO_ENTITY_SETS] Refreshing entity set: ", DEBUG_GET_MUSIC_STUDIO_ENTITY_SET_ENUM_STRING(eMusicStudioSetID))
				REFRESH_MUSIC_STUDIO_PROPERTY_ENTITY_SET(eMusicStudioSetID)
			ENDIF
		ENDREPEAT
	ELSE
		PRINTLN("[SET_MUSIC_STUDIO_ENTITY_SETS] Unable to set music studio entity sets as interior is invalid")
	ENDIF
ENDPROC

/// PURPOSE:
///    Activates the MUSIC_STUDIO entity sets.
///    This function sets the entity sets bit sets based on their conditions.
///    Call this in INIT before SET_MUSIC_STUDIO_PROPERTY_ENTITY_SETS
PROC SET_MUSIC_STUDIO_ENTITY_SET_DATA()
	PRINTLN("[AM_MP_MUSIC_STUDIO] SET_MUSIC_STUDIO_ENTITY_SET_DATA - Setting entity set data:")
	
	// ----- RANDOM PROPS (url:bugstar:7310124) -----
	TOGGLE_MUSIC_STUDIO_ENTITY_SET_ACTIVE_BIT(MUSIC_STUDIO_SET_STU_EXT_P1, m_InteriorData.iEntitySetBS, FALSE)
	TOGGLE_MUSIC_STUDIO_ENTITY_SET_ACTIVE_BIT(MUSIC_STUDIO_SET_STU_EXT_P3A1, m_InteriorData.iEntitySetBS, TRUE)
	TOGGLE_MUSIC_STUDIO_ENTITY_SET_ACTIVE_BIT(MUSIC_STUDIO_SET_TRIP1_INT_P2, m_InteriorData.iEntitySetBS, FALSE)
	TOGGLE_MUSIC_STUDIO_ENTITY_SET_ACTIVE_BIT(MUSIC_STUDIO_SET_DEFAULT, m_InteriorData.iEntitySetBS, TRUE)
ENDPROC

/// PURPOSE:
///    Called for 1 frame at the start of the script. Inits the music studio entity sets.
PROC INITIALISE_MUSIC_STUDIO_ENTITY_SETS()
	SET_MUSIC_STUDIO_ENTITY_SET_DATA()
	SET_MUSIC_STUDIO_ENTITY_SETS()	
ENDPROC

// ***************************************************************************************
// 					END OF ENTITY SETS
// ***************************************************************************************

#IF IS_DEBUG_BUILD
/// PURPOSE:
///    Creates the widgets used for the mp award producer
PROC DEBUG_CREATE_WIDGETS_MP_AWARD_PRODUCER()
	START_WIDGET_GROUP("Awards - Producer")
		ADD_WIDGET_BOOL("Draw Award Values", debugData.bDrawAwardValuesProducer)
		ADD_WIDGET_BOOL("Reset Saved Time", debugData.bResetAwardProducer)
		ADD_WIDGET_BOOL("Add Minutes", debugData.bAddMinutesToAwardProducer)
		ADD_WIDGET_INT_SLIDER("Minutes to Add", debugData.iAddMinutesToAwardProducer, 0, 30, 1)
	STOP_WIDGET_GROUP()
ENDPROC

/// PURPOSE:
///    Wraopper for resetting the award stat
FUNC BOOL DEBUG_RESET_MP_AWARD_PRODUCER()
	IF (debugData.bResetAwardProducer)
		debugData.bResetAwardProducer = FALSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Wrapper for adding minutes to the award.
FUNC BOOL DEBUG_ADD_MINUTES_TO_MP_AWARD_PRODUCER()
	IF (debugData.bAddMinutesToAwardProducer)
		debugData.bAddMinutesToAwardProducer = FALSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Draws useful widgets to track how many minutes have passed. 
PROC DEBUG_DRAW_AWARD_VALUES_FOR_MP_AWARD_PRODUCER(INT iSavedTime, INT iCurrentTime)
	IF (debugData.bDrawAwardValuesProducer)
	OR GET_COMMANDLINE_PARAM_EXISTS("sc_DrawValuesForMPAwardProducer")
		TEXT_LABEL_63 tlText
		VECTOR vPos = <<0.5, 0.5, 0.0>>
		VECTOR vOffset = << 0.0, 0.02, 0.0>>
		
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
		
		tlText = "===== MP_AWARD_PRODUCER ====="
		DRAW_DEBUG_TEXT_2D(Get_String_From_TextLabel(tlText), vPos, 0, 255, 0)
		vPos += vOffset
		
		tlText = "iSavedTime (Minutes): "
		tlText += GET_STRING_FROM_INT(iSavedTime)
		DRAW_DEBUG_TEXT_2D(Get_String_From_TextLabel(tlText), vPos, 0, 255, 0)
		vPos += vOffset
		
		tlText = "iCurrentTime (Seconds): "
		tlText += GET_STRING_FROM_INT(iCurrentTime)
		DRAW_DEBUG_TEXT_2D(Get_String_From_TextLabel(tlText), vPos, 0, 255, 0)
		vPos += vOffset
	ENDIF
ENDPROC
#ENDIF // IS_DEBUG_BUILD

/// PURPOSE:
///    Determines if the award should increment. 
FUNC BOOL SHOULD_TRACK_MP_AWARD_PRODUCER()
	IF IS_PED_IN_MIXING_ROOM_AREA(PLAYER_PED_ID())
	AND IS_DRE_IN_MUSIC_STUDIO()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Main updated for award producer. 
PROC UPDATE_MUSIC_STUDIO_MP_AWARD_PRODUCER()
	IF SHOULD_TRACK_MP_AWARD_PRODUCER()
		IF NOT HAS_NET_TIMER_STARTED(m_InteriorData.timerAwardProducer)
			START_NET_TIMER(m_InteriorData.timerAwardProducer)
		ELSE
			IF NOT IS_BIT_SET(m_InteriorData.iBS, BS_MUSIC_STUDIO_RESET_TIMER_AWARD_PRODUCER)
				SET_BIT(m_InteriorData.iBS, BS_MUSIC_STUDIO_RESET_TIMER_AWARD_PRODUCER)
			ENDIF
			
			m_InteriorData.iSavedTimeAwardProducer = GET_MP_INT_CHARACTER_AWARD(MP_AWARD_PRODUCER)
			m_InteriorData.iCurrentTimeAwardProducer = (GET_ABS_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(m_InteriorData.timerAwardProducer) / 1000)
			
			#IF IS_DEBUG_BUILD
			IF DEBUG_RESET_MP_AWARD_PRODUCER()
				PRINTLN("UPDATE_MUSIC_STUDIO_MP_AWARD_PRODUCER - Debug used! Reset Award")
				SET_MP_INT_CHARACTER_AWARD(MP_AWARD_PRODUCER, 0)
			ENDIF
			
			IF DEBUG_ADD_MINUTES_TO_MP_AWARD_PRODUCER()
				m_InteriorData.iSavedTimeAwardProducer += debugData.iAddMinutesToAwardProducer
				SET_MP_INT_CHARACTER_AWARD(MP_AWARD_PRODUCER, m_InteriorData.iSavedTimeAwardProducer)
				RESET_NET_TIMER(m_InteriorData.timerAwardProducer)
				PRINTLN("UPDATE_MUSIC_STUDIO_MP_AWARD_PRODUCER - Debug used! Added ", debugData.iAddMinutesToAwardProducer, " minutes to award.")
			ENDIF
			#ENDIF
			
			IF ((m_InteriorData.iCurrentTimeAwardProducer % 60) = 0)
			AND (m_InteriorData.iCurrentTimeAwardProducer != 0)
				m_InteriorData.iSavedTimeAwardProducer += 1	// Add 1 minute
				RESET_NET_TIMER(m_InteriorData.timerAwardProducer)
				SET_MP_INT_CHARACTER_AWARD(MP_AWARD_PRODUCER, m_InteriorData.iSavedTimeAwardProducer)
				PRINTLN("UPDATE_MUSIC_STUDIO_MP_AWARD_PRODUCER - Added 1 minute.")
			ENDIF
		ENDIF
	ELIF HAS_NET_TIMER_STARTED(m_InteriorData.timerAwardProducer)
		IF IS_BIT_SET(m_InteriorData.iBS, BS_MUSIC_STUDIO_RESET_TIMER_AWARD_PRODUCER)
			NET_TIMER_PAUSE_RESET(m_InteriorData.tempTimerAwardProducer)
			CLEAR_BIT(m_InteriorData.iBS, BS_MUSIC_STUDIO_RESET_TIMER_AWARD_PRODUCER)
		ENDIF
		
		NET_TIMER_PAUSE_THIS_FRAME(m_InteriorData.timerAwardProducer, m_InteriorData.tempTimerAwardProducer)
	ENDIF
	
	#IF IS_DEBUG_BUILD
	DEBUG_DRAW_AWARD_VALUES_FOR_MP_AWARD_PRODUCER(m_InteriorData.iSavedTimeAwardProducer, m_InteriorData.iCurrentTimeAwardProducer)
	#ENDIF
ENDPROC

/// PURPOSE:
///    Main update for all awards
PROC UPDATE_MUSIC_STUDIO_AWARDS()
	UPDATE_MUSIC_STUDIO_MP_AWARD_PRODUCER()
ENDPROC

#ENDIF // FEATURE_MUSIC_STUDIO


//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ FUCTIONS ╞══════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

#IF IS_DEBUG_BUILD
FUNC STRING DEBUG_GET_MUSIC_STUDIO_STATE_NAME(MUSIC_STUDIO_CLIENT_STATE eState)
	SWITCH eState 
		CASE MUSIC_STUDIO_STATE_LOAD_INTERIOR			RETURN "MUSIC_STUDIO_STATE_LOAD_INTERIOR"
		CASE MUSIC_STUDIO_STATE_CREATE_ENTITIES			RETURN "MUSIC_STUDIO_STATE_CREATE_ENTITIES"
		CASE MUSIC_STUDIO_STATE_IDLE					RETURN "MUSIC_STUDIO_STATE_IDLE"
		CASE MUSIC_STUDIO_STATE_MOCAP					RETURN "MUSIC_STUDIO_STATE_MOCAP"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC STRING DEBUG_GET_MUSIC_STUDIO_SERVER_STATE_NAME(MUSIC_STUDIO_SERVER_STATE eState)
	SWITCH eState
		CASE MUSIC_STUDIO_SERVER_STATE_LOADING	RETURN "MUSIC_STUDIO_SERVER_STATE_LOADING"
		CASE MUSIC_STUDIO_SERVER_STATE_IDLE		RETURN "MUSIC_STUDIO_SERVER_STATE_IDLE"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC STRING DEBUG_GET_PLAYER_BROADCAST_BIT_NAME(INT iBit)
	SWITCH iBit
		CASE 0				RETURN "UNUSED"
	ENDSWITCH
	
	RETURN "NULL"
ENDFUNC

FUNC STRING DEBUG_GET_MUSIC_STUDIO_WORK_STATE_STRING(INT iState)
	SWITCH iState
		CASE 0			RETURN "MSW_STATE_IDLE" 
		CASE 1			RETURN "MSW_STATE_DRE_ARRIVED"
		CASE 2			RETURN "MSW_STATE_MUSICIAN_ARRIVED"
		CASE 3			RETURN "MSW_STATE_MUSICIAN_LEFT"
		CASE 4			RETURN "MSW_STATE_DRE_LEFT"
	ENDSWITCH
	RETURN "MUSIC_STUDIO_WORK_STATE_INVALID"
ENDFUNC

PROC CREATE_DEBUG_WIDGETS()
	START_WIDGET_GROUP("AM_MP_MUSIC_STUDIO")
		ADD_WIDGET_INT_SLIDER("Drunk Spawn Location", g_iMusicStudioDrunkSpawnLocation, -1, 11, 1)	
		
	//	START_NEW_WIDGET_COMBO()
	//	INT i
	//	REPEAT MUSIC_STUDIO_WORK_STATE_COUNT i		
	//		ADD_TO_WIDGET_COMBO(DEBUG_GET_MUSIC_STUDIO_WORK_STATE_STRING(i))
	//	ENDREPEAT
	//	STOP_WIDGET_COMBO("Current Music Studio Work State", debugData.iState)
		
		
		ADD_WIDGET_BOOL("FORCE NEXT DRE STATE: ", g_sMusicStudioDreWorkData.bForceNextState)
		
		ADD_WIDGET_STRING("TIME UNTIL NEXT DRE STAGE")
		ADD_WIDGET_INT_READ_ONLY("HOURS: ", debugData.iTimeHours)
		ADD_WIDGET_INT_READ_ONLY("MINUTES: ", debugData.iTimeMinutes)
		ADD_WIDGET_INT_READ_ONLY("SECONDS: ", debugData.iTimeSeconds)
		debugData.sNextPerformance = ADD_TEXT_WIDGET("NEXT DRE: ")
		SET_CONTENTS_OF_TEXT_WIDGET(debugData.sNextPerformance, "DRE BLOCKED TODAY")
		
		ADD_WIDGET_BOOL("BYPASS CCTV CHECK", debugData.bBypassCCTVCheck)
		
		CREATE_CLUB_MUSIC_WIDGETS(debugData.DJDebugData)
		DEBUG_CREATE_WIDGETS_MP_AWARD_PRODUCER()
	STOP_WIDGET_GROUP()
	
ENDPROC

FUNC INT DEBUG_GET_TIME_LEFT_PER_STAGE()
	SWITCH g_clubMusicData.eActiveDJ
		CASE CLUB_DJ_DR_DRE_ABSENT					RETURN (g_sMusicStudioDreWorkData.iNextDrePosix - GET_CLOUD_TIME_AS_INT())
		CASE CLUB_DJ_DR_DRE_MIX						RETURN (g_sMPTunables.iFIXER_STUDIO_TRACK_TIME_DR_DRE_MIX - g_clubMusicData.iCurrentTrackTimeMS)/1000
		CASE CLUB_DJ_DR_DRE_MIX_VOCALS				RETURN (g_sMPTunables.iFIXER_STUDIO_TRACK_TIME_DR_DRE_MIX_VOCALS - g_clubMusicData.iCurrentTrackTimeMS)/1000
		CASE CLUB_DJ_DR_DRE_MUSICIAN_DRUMS			RETURN (g_sMPTunables.iFIXER_STUDIO_TRACK_TIME_DR_DRE_MUSICIAN_DRUMS - g_clubMusicData.iCurrentTrackTimeMS)/1000
		CASE CLUB_DJ_DR_DRE_MUSICIAN_VOCALS			RETURN (g_sMPTunables.iFIXER_STUDIO_TRACK_TIME_DR_DRE_MUSICIAN_VOCALS - g_clubMusicData.iCurrentTrackTimeMS)/1000 
		CASE CLUB_DJ_DR_DRE_MIX_2					RETURN (g_sMPTunables.iFIXER_STUDIO_TRACK_TIME_DR_DRE_MIX_2 - g_clubMusicData.iCurrentTrackTimeMS)/1000 		
	ENDSWITCH
	RETURN -1
ENDFUNC

PROC DEBUG_UPDATE_DR_DRE_TIMERS()
	IF debugData.bBlockDreToday
		EXIT
	ENDIF

	IF SHOULD_DR_DRE_APPEARANCE_BE_BLOCKED_TODAY()
	AND g_clubMusicData.eActiveDJ = CLUB_DJ_DR_DRE_ABSENT
		debugData.bBlockDreToday = TRUE
		EXIT
	ENDIF
	
	INT iTimeLeft = DEBUG_GET_TIME_LEFT_PER_STAGE()
	debugData.iTimeHours = iTimeLeft/3600
	debugData.iTimeMinutes = iTimeLeft/60 - debugData.iTimeHours*60
	debugData.iTimeSeconds = iTimeLeft%60
		
		
	IF g_sMusicStudioDreWorkData.iNextDrePosix != -1
	AND debugData.iNextPosix!= g_sMusicStudioDreWorkData.iNextDrePosix 
		TEXT_LABEL_63 sLabel	
		debugData.iNextPosix = g_sMusicStudioDreWorkData.iNextDrePosix 
		UGC_DATE sData
		CONVERT_POSIX_TIME(g_sMusicStudioDreWorkData.iNextDrePosix, sData)		
		sLabel+=sData.nDay
		sLabel+="/"
		sLabel+=sData.nMonth
		sLabel+="/"
		sLabel+=sData.nYear
		sLabel+="   "
		sLabel+=sData.nHour
		sLabel+=":"
		sLabel+=sData.nMinute
		sLabel+=":"
		sLabel+=sData.nSecond
		SET_CONTENTS_OF_TEXT_WIDGET(debugData.sNextPerformance, sLabel)
	ENDIF
ENDPROC

PROC UPDATE_DEBUG_WIDGETS()
	debugData.DJDebugData.bDisplayDebug = g_sMusicStudioDreWorkData.bDisplayDebugData
	UPDATE_CLUB_MUSIC_WIDGETS(CLUB_LOCATION_MUSIC_STUDIO, debugData.DJDebugData, m_ServerBD.DJServerData, m_InteriorData.DJLocalData)	
	DEBUG_UPDATE_DR_DRE_TIMERS()
ENDPROC
#ENDIF

FUNC BOOL IS_MUSIC_STUDIO_STATE(MUSIC_STUDIO_CLIENT_STATE eState)
	RETURN m_InteriorData.eState = eState
ENDFUNC

FUNC BOOL IS_MUSIC_STUDIO_SERVER_STATE(MUSIC_STUDIO_SERVER_STATE eState)
	RETURN m_ServerBD.eState = eState
ENDFUNC

PROC SET_MUSIC_STUDIO_STATE(MUSIC_STUDIO_CLIENT_STATE eState)
	CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_MUSIC_STUDIO - SET_MUSIC_STUDIO_STATE - New state: ", DEBUG_GET_MUSIC_STUDIO_STATE_NAME(eState))
	m_InteriorData.eState = eState
ENDPROC

PROC SET_MUSIC_STUDIO_SERVER_STATE(MUSIC_STUDIO_SERVER_STATE eState)
	CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_MUSIC_STUDIO - SET_MUSIC_STUDIO_SERVER_STATE - New state: ", DEBUG_GET_MUSIC_STUDIO_SERVER_STATE_NAME(eState))
	m_ServerBD.eState = eState
ENDPROC

PROC SET_LOCAL_PLAYER_BROADCAST_BIT(INT iBit, BOOL bSet)
	IF bSet
		IF NOT IS_BIT_SET(m_PlayerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, iBit)
			CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_MUSIC_STUDIO - SET_LOCAL_PLAYER_BROADCAST_BIT - Setting bit ", DEBUG_GET_PLAYER_BROADCAST_BIT_NAME(iBit))
			SET_BIT(m_PlayerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, iBit)
		ENDIF
	ELSE
		IF IS_BIT_SET(m_PlayerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, iBit)	
			CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_MUSIC_STUDIO - SET_LOCAL_PLAYER_BROADCAST_BIT - Clearing bit ", DEBUG_GET_PLAYER_BROADCAST_BIT_NAME(iBit))
			CLEAR_BIT(m_PlayerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, iBit)
		ENDIF
	ENDIF
ENDPROC

PROC CLEANUP_CCTV(BOOL bReturnControl)
	CLEANUP_MP_CCTV_CLIENT(m_PlayerBD[NATIVE_TO_INT(PLAYER_ID())].MPCCTVClient, m_InteriorData.MPCCTVLocal, bReturnControl)
ENDPROC

PROC CLEANUP_MUSIC_STUDIO_INTERIOR_BLIPS()
	SAFE_REMOVE_BLIP(m_InteriorData.blipSmokingRoom)
	SAFE_REMOVE_BLIP(m_InteriorData.blipSmokingRoomCouch)
	SAFE_REMOVE_BLIP(m_InteriorData.blipClothing)
ENDPROC

PROC CLEANUP_INVISIBLE_OBJECTS()
	INT i
	REPEAT MUSIC_STUDIO_INVISIBLE_PED_COLLISION_COUNT i
		IF DOES_ENTITY_EXIST(m_InteriorData.objCollisions[i])
			SAFE_DELETE_OBJECT(m_InteriorData.objCollisions[i])
		ENDIF
	ENDREPEAT
	
	IF IS_BIT_SET(m_InteriorData.iBS, BS_MUSIC_STUDIO_ENTOURAGE_GIRL_COLLIDER_CREATED)
		CLEAR_BIT(m_InteriorData.iBS, BS_MUSIC_STUDIO_ENTOURAGE_GIRL_COLLIDER_CREATED)
	ENDIF
	
ENDPROC

PROC CLEANUP_DRE_OBJECTS()
	
	IF DOES_ENTITY_EXIST(m_InteriorData.objDreChair)
		SAFE_DELETE_OBJECT(m_InteriorData.objDreChair)
	ENDIF
	
	
	IF IS_BIT_SET(m_InteriorData.iBS, BS_MUSIC_STUDIO_DRE_CHAIR_SPAWNED)
		CLEAR_BIT(m_InteriorData.iBS, BS_MUSIC_STUDIO_DRE_CHAIR_SPAWNED)
	ENDIF
ENDPROC

PROC CLEAN_UP_RECORDING_LIGHTS()
	INT i
	REPEAT ciMUSIC_STUDIO_WORK_NUM_RECORDING_LIGHTS i
		IF DOES_ENTITY_EXIST(m_InteriorData.objRecordingLights[i])
			SAFE_DELETE_OBJECT(m_InteriorData.objRecordingLights[i])
		ENDIF
	ENDREPEAT
ENDPROC

// Need to make a global since script restarts between transitions.
PROC SET_LOCAL_PLAYER_VIEWED_WEED_ROOM_ENTER_CUTSCENE_THIS_SESSION(BOOL bSet)
	IF bSet
		PRINTLN("SET_LOCAL_PLAYER_VIEWED_WEED_ROOM_ENTER_CUTSCENE_THIS_SESSION - TRUE")
		SET_BIT(g_SimpleInteriorData.iEleventhBS, BS11_SIMPLE_INTERIOR_WALKED_INTO_WEED_ROOM_THIS_SESSION)
	ELSE
		PRINTLN("SET_LOCAL_PLAYER_VIEWED_WEED_ROOM_ENTER_CUTSCENE_THIS_SESSION - FALSE")
		CLEAR_BIT(g_SimpleInteriorData.iEleventhBS, BS11_SIMPLE_INTERIOR_WALKED_INTO_WEED_ROOM_THIS_SESSION)
	ENDIF
ENDPROC

FUNC BOOL HAS_LOCAL_PLAYER_VIEWED_WEED_ROOM_ENTER_CUTSCENE_THIS_SESSION()
	RETURN IS_BIT_SET(g_SimpleInteriorData.iEleventhBS, BS11_SIMPLE_INTERIOR_WALKED_INTO_WEED_ROOM_THIS_SESSION)
ENDFUNC

PROC SET_LOCAL_PLAYER_VIEWED_WEED_ROOM_EXIT_CUTSCENE_THIS_SESSION(BOOL bSet)
	IF bSet
		PRINTLN("SET_LOCAL_PLAYER_VIEWED_WEED_ROOM_EXIT_CUTSCENE_THIS_SESSION - TRUE")
		SET_BIT(g_SimpleInteriorData.iEleventhBS, BS11_SIMPLE_INTERIOR_WALKED_OUT_OF_WEED_ROOM_THIS_SESSION)
	ELSE
		PRINTLN("SET_LOCAL_PLAYER_VIEWED_WEED_ROOM_EXIT_CUTSCENE_THIS_SESSION - FALSE")
		CLEAR_BIT(g_SimpleInteriorData.iEleventhBS, BS11_SIMPLE_INTERIOR_WALKED_OUT_OF_WEED_ROOM_THIS_SESSION)
	ENDIF
ENDPROC

FUNC BOOL HAS_LOCAL_PLAYER_VIEWED_WEED_ROOM_EXIT_CUTSCENE_THIS_SESSION()
	RETURN IS_BIT_SET(g_SimpleInteriorData.iEleventhBS, BS11_SIMPLE_INTERIOR_WALKED_OUT_OF_WEED_ROOM_THIS_SESSION)
ENDFUNC

PROC SET_SMOKING_ROOM_EMITTER()	
	IF IS_LOCAL_PLAYER_IN_MUSIC_STUDIO_SMOKING_ROOM()
		SET_STATIC_EMITTER_ENABLED("SE_sf_dlc_studio_sec_writers_01", TRUE)
		SET_EMITTER_RADIO_STATION("SE_sf_dlc_studio_sec_writers_01", "HIDDEN_RADIO_SEC_STUDIO_LOUNGE")
	ENDIF
ENDPROC

PROC CLEANUP_SMOKING_ROOM_EMITTER()
	SET_STATIC_EMITTER_ENABLED("SE_sf_dlc_studio_sec_writers_01", FALSE)
ENDPROC

PROC SCRIPT_CLEANUP()
	CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_MUSIC_STUDIO - SCRIPT_CLEANUP")
	
	SEND_SOCIAL_HUB_EXIT_TELEMETRY_DATA()
	RESET_NET_TIMER(g_SimpleInteriorData.tIntAccessFallback)
		
	// CCTV
	BOOL bReturnControl = TRUE
	IF IS_SKYSWOOP_MOVING()
	OR IS_SKYSWOOP_IN_SKY()
	OR IS_TRANSITION_ACTIVE()
	OR IS_TRANSITION_RUNNING()
	OR IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
		bReturnControl = FALSE
	ENDIF
	CLEANUP_CCTV(bReturnControl)
	
	IF IS_AUDIO_SCENE_ACTIVE("DLC_Fixer_Studio_Interior_Scene")
		STOP_AUDIO_SCENE("DLC_Fixer_Studio_Interior_Scene")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("DLC_Security_Music_Studio_Producer_IG_Playing_Scene")
		STOP_AUDIO_SCENE("DLC_Security_Music_Studio_Producer_IG_Playing_Scene")
	ENDIF
		
	SET_RADAR_ZOOM_PRECISE(0)
	
	SIMPLE_INTERIOR_CLEAR_ENTERING_WITH_VEHICLE()
	SIMPLE_INTERIOR_CLEAR_ENTERING_WITH_VEH_AS_PASSENGER()
	
	g_SimpleInteriorData.bForceDrinkActivitiesCleanup = FALSE
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_NotAllowedToJackAnyPlayers, FALSE)
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableStartEngine, FALSE)
		SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
		SET_ENTITY_CAN_BE_DAMAGED(PLAYER_PED_ID(), TRUE)
		SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(PLAYER_PED_ID(), TRUE)
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID() ,PCF_DontActivateRagdollFromVehicleImpact,FALSE)
	ENDIF	
	g_bIsPlayerWatchingMusicStudioCutscene = FALSE
	IF NOT IS_LOCAL_PLAYER_IN_MUSIC_STUDIO_SMOKING_ROOM()
		CLEANUP_CLUB_MUSIC(CLUB_LOCATION_MUSIC_STUDIO, m_InteriorData.DJLocalData, m_ServerBD.DJServerData)
	ENDIF
	CLEANUP_MUSIC_STUDIO_INTERIOR_BLIPS()
	CLEANUP_DRE_OBJECTS()
	CLEANUP_INVISIBLE_OBJECTS()
	CLEANUP_MUSIC_STUDIO_SCREEN(m_InteriorData.sEngineerScreenData)
	CLEANUP_MUSIC_STUDIO_SCREEN(m_InteriorData.sFranklinScreenData)
	// CLEANUP_MUSIC_STUDIO_SCREEN(m_InteriorData.sPoohScreenData)
	CLEANUP_MUSIC_STUDIO_SCREEN(m_InteriorData.sStaffScreenData)
	SET_KILL_ARCADE_CABINET_MANAGER_SCRIPT(TRUE)
	CLEAN_UP_RECORDING_LIGHTS()
	CLEANUP_SMOKING_ROOM_EMITTER()
	
	SET_PLAYER_SPAWNING_INTO_MUSIC_STUDIO_SMOKING_CHAIR(FALSE)
	
	IF IS_BIT_SET(m_InteriorData.iBS, BS_MUSIC_STUDIO_WARPING_OUT_OF_RESTRICTED_AREA)
	OR IS_BIT_SET(m_InteriorData.iBS, BS_MUSIC_STUDIO_WARPING_OUT_OF_RESTRICTED_AREA_DRUMS)
		CLEAR_CUSTOM_SPAWN_POINTS()
		USE_CUSTOM_SPAWN_POINTS(FALSE)	
	ENDIF
	
	//url:bugstar:7367421
	RELEASE_SCRIPT_AUDIO_BANK()
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

PROC SEND_MUSIC_STUDIO_SOCIAL_HUB_ENTRY_TELEMETRY()
	
	GTAO_SOCIAL_HUB eHub = GTAO_SH_MUSIC_STUDIO
	BOOl bPulledInByPlayers = IS_BIT_SET(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_WARPING_VIA_ENTER_WITH_NEARBY_PLAYER)
	GTAO_SOCIAL_HUB_ACCESS_POINT eEntryPoint
		
	IF IS_LOCAL_PLAYER_IN_MUSIC_STUDIO_SMOKING_ROOM()
		//Entering the smoking room from the music studio
		eHub 		= GTAO_SH_MUSIC_STUDIO_SMOKING_ROOM
		eEntryPoint = GTAO_SH_AP_SMOKING_ROOM_DOOR
	ELIF IS_BIT_SET(g_SimpleInteriorData.iEleventhBS, BS11_SIMPLE_INTERIOR_MUSIC_STUDIO_LEAVING_SMOKING_ROOM)
		//Entering the music studio from the smoking room
		eEntryPoint = GTAO_SH_AP_SMOKING_ROOM_DOOR
	ELSE
		IF IS_BIT_SET(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_SPAWNING_IN_SIMPLE_INTERIOR)
			eEntryPoint = GTAO_SH_AP_SPAWNED_IN
		ELSE
			// --- Entry Point ---
			SWITCH GET_SIMPLE_INTERIOR_ENTRY_POINT_LOCAL_PLAYER_USED()
				CASE ciMUSIC_STUDIO_ENTRANCE_FOOT
					eEntryPoint = GTAO_SH_AP_FRONT_DOOR
				BREAK
				CASE ciMUSIC_STUDIO_ENTRANCE_FOOT_BACK
					eEntryPoint = GTAO_SH_AP_BACK_DOOR
				BREAK
				CASE ciMUSIC_STUDIO_ENTRANCE_FOOT_ROOF
					eEntryPoint = GTAO_SH_AP_ROOF
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
	
	SEND_SOCIAL_HUB_ENTRY_TELEMETRY(eHub, eEntryPoint, 0, bPulledInByPlayers, g_SimpleInteriorData.bAcceptedInviteIntoSimpleInterior,	0, FALSE, GTAO_SH_EV_NONE)
ENDPROC

PROC INITIALISE_ARCADE_CABINET_STRUCT()			
	eArcadeCabinetManager.eArcadeCab[ACM_SLOT_1] = ARCADE_CABINET_SUM_QUB3D
	PRINTLN("[AM_MP_MUSIC_STUDIO] INITIALISE_ARCADE_CABINET_STRUCT - Setting ACM_SLOT_1 to: ", GET_ARCADE_CABINET_NAME(ARCADE_CABINET_SUM_QUB3D))	

	eArcadeCabinetManager.iInstanceId = m_InteriorData.iScriptInstance
	eArcadeCabinetManager.iRoomKey = ENUM_TO_INT(MUSIC_STUDIO_ROOM_KEY) 
	eArcadeCabinetManager.eArcadeCabinetPropertyType = ACP_MUSIC_STUDIO		
ENDPROC

PROC REFRESH_MOCAP_CUTSCENE_BIT_SETS()
	IF IS_LOCAL_PLAYER_IN_MUSIC_STUDIO_SMOKING_ROOM()
		PRINTLN("REFRESH_MOCAP_CUTSCENE_BIT_SETS - EXIT - In smoking room.")
		EXIT
	ENDIF
	
	IF IS_BIT_SET(g_SimpleInteriorData.iEleventhBS, BS11_SIMPLE_INTERIOR_MUSIC_STUDIO_LEAVING_SMOKING_ROOM)
		PRINTLN("REFRESH_MOCAP_CUTSCENE_BIT_SETS - EXIT - Leaving smoking room.")
		EXIT
	ENDIF
	
	INT iEntranceUsed = GET_SIMPLE_INTERIOR_ENTRY_POINT_LOCAL_PLAYER_USED()
	
	IF iEntranceUsed != ciMUSIC_STUDIO_ENTRANCE_FOOT
	AND iEntranceUsed != ciMUSIC_STUDIO_ENTRANCE_FOOT_BACK
	AND iEntranceUsed != ciMUSIC_STUDIO_ENTRANCE_FOOT_ROOF
		PRINTLN("REFRESH_MOCAP_CUTSCENE_BIT_SETS - EXIT - Invalid entrance to music studio: ", iEntranceUsed)
		EXIT
	ENDIF
	
	PRINTLN("REFRESH_MOCAP_CUTSCENE_BIT_SETS - TRUE.")
	SET_LOCAL_PLAYER_VIEWED_WEED_ROOM_ENTER_CUTSCENE_THIS_SESSION(FALSE)
	SET_LOCAL_PLAYER_VIEWED_WEED_ROOM_EXIT_CUTSCENE_THIS_SESSION(FALSE)
ENDPROC

PROC INITIALISE_SCREEN_DATA()
	//screens
	m_InteriorData.sEngineerScreenData.eScreen = MUSIC_STUDIO_SCREEN_ENGINEER
	m_InteriorData.sFranklinScreenData.eScreen = MUSIC_STUDIO_SCREEN_PHONE_FRANKLIN
	//m_InteriorData.sPoohScreenData.eScreen = MUSIC_STUDIO_SCREEN_PHONE_POOH
	m_InteriorData.sStaffScreenData.eScreen = MUSIC_STUDIO_SCREEN_PHONE_STAFF
	
	//g_bPoohPhoneLinked = FALSE
ENDPROC

PROC INITIALISE()
	IF m_InteriorData.eSimpleInteriorID != SIMPLE_INTERIOR_INVALID
		g_bPlayerLeavingCurrentInteriorInVeh = FALSE
		
		GET_SIMPLE_INTERIOR_DETAILS(m_InteriorData.eSimpleInteriorID, m_InteriorData.SimpleInteriorDetails, FALSE)
		
		IF g_SimpleInteriorData.iExitMenuOption > -1
			g_SimpleInteriorData.iExitMenuOption = -1
		ENDIF
		
		REFRESH_MOCAP_CUTSCENE_BIT_SETS()
		
		CLEAR_BIT(g_SimpleInteriorData.iEleventhBS, BS11_SIMPLE_INTERIOR_MUSIC_STUDIO_ENTER_SMOKING_ROOM)
		
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_NotAllowedToJackAnyPlayers, TRUE)
			SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
			SET_ENTITY_CAN_BE_DAMAGED(PLAYER_PED_ID(), FALSE)
			SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(PLAYER_PED_ID(), FALSE)
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DontActivateRagdollFromVehicleImpact,TRUE)
		ENDIF
		
		IF NOT IS_AUDIO_SCENE_ACTIVE("DLC_Fixer_Studio_Interior_Scene")
			START_AUDIO_SCENE("DLC_Fixer_Studio_Interior_Scene")
		ENDIF
		
		//url:bugstar:7367421
		REQUEST_SCRIPT_AUDIO_BANK("DLC_SECURITY/DLC_Sec_Agency")
		REQUEST_SCRIPT_AUDIO_BANK("DLC_SECURITY/DLC_Sec_Studio")
		
		#IF IS_DEBUG_BUILD
		CREATE_DEBUG_WIDGETS()
		CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_MUSIC_STUDIO - INITIALISE - Done.")
		#ENDIF
		
		IF STORE_MUSIC_STUDIO_PROPERTY_INTERIOR_ID()
			INITIALISE_MUSIC_STUDIO_ENTITY_SETS()
		ELSE
			SCRIPT_ASSERT("AM_MP_MUSIC_STUDIO - INITIALISE - Failed to get interior ID! Cannot init Entity Sets.")
		ENDIF
		
		SEND_MUSIC_STUDIO_SOCIAL_HUB_ENTRY_TELEMETRY()
		INITIALISE_SCREEN_DATA()
		INITIALISE_ARCADE_CABINET_STRUCT()
		SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_AM_VISIT_MUSIC_STUDIO)				
		SET_SMOKING_ROOM_EMITTER()
		INITIALISE_SOCIAL_HUB_EXIT_TELEMETRY_DATA(GTAO_SH_MUSIC_STUDIO)
	ELSE
		CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_MUSIC_STUDIO - Unable to initialise, invalid simple interior index.")
	ENDIF
ENDPROC

PROC SCRIPT_INITIALISE(SIMPLE_INTERIOR_CHILD_SCRIPT_DATA &scriptData)

	CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_MUSIC_STUDIO - SCRIPT_INITIALISE - Launching instance ", m_InteriorData.iScriptInstance)
			
	m_InteriorData.eSimpleInteriorID 	= scriptData.eSimpleInteriorID
	m_InteriorData.iScriptInstance 		= scriptData.iScriptInstance
	
	g_iClothesShopInstance = m_InteriorData.iScriptInstance
	
	NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE, m_InteriorData.iScriptInstance)
	
	// Ensures net script is active, waits until it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(m_ServerBD, SIZE_OF(m_ServerBD))
	
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(m_PlayerBD, SIZE_OF(m_PlayerBD))
	
	IF NOT Wait_For_First_Network_Broadcast() 
		CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_MUSIC_STUDIO - SCRIPT_INITIALISE - Failed to receive initial network broadcast.")
		SCRIPT_CLEANUP()
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_MUSIC_STUDIO - SCRIPT_INITIALISE - Initialised.")
	ELSE
		CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_MUSIC_STUDIO - SCRIPT_INITIALISE - NETWORK_IS_GAME_IN_PROGRESS = FALSE")
		SCRIPT_CLEANUP()
	ENDIF
	
	g_bLaunchedMissionFrmSMPLIntLaptop = FALSE
	INITIALISE()
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════════╡ MOCAP ╞═══════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

#IF IS_DEBUG_BUILD
/// PURPOSE:
///    Debug - Gets each cutscene state as a string for prints. 
FUNC STRING GET_MUSIC_STUDIO_MOCAP_CUTSCENE_STATE_NAME(MUSIC_STUDIO_MOCAP_CUTSCENE_STATE eCutsceneState)
	SWITCH eCutsceneState
		CASE MUSIC_STUDIO_MOCAP_CUTSCENE_STATE_INITIALISE				RETURN "MUSIC_STUDIO_MOCAP_CUTSCENE_STATE_INITIALISE"
		CASE MUSIC_STUDIO_MOCAP_CUTSCENE_STATE_ASSET_REQUEST			RETURN "MUSIC_STUDIO_MOCAP_CUTSCENE_STATE_ASSET_REQUEST"
		CASE MUSIC_STUDIO_MOCAP_CUTSCENE_STATE_START_CUTSCENE			RETURN "MUSIC_STUDIO_MOCAP_CUTSCENE_STATE_START_CUTSCENE"
		CASE MUSIC_STUDIO_MOCAP_CUTSCENE_STATE_IDLING					RETURN "MUSIC_STUDIO_MOCAP_CUTSCENE_STATE_IDLING"
		CASE MUSIC_STUDIO_MOCAP_CUTSCENE_STATE_PLAY_INTERIOR			RETURN "MUSIC_STUDIO_MOCAP_CUTSCENE_STATE_PLAY_INTERIOR"
		CASE MUSIC_STUDIO_MOCAP_CUTSCENE_STATE_CLEANUP					RETURN "MUSIC_STUDIO_MOCAP_CUTSCENE_STATE_CLEANUP"
		CASE MUSIC_STUDIO_MOCAP_CUTSCENE_STATE_WAIT_FOR_INTERIOR_READY	RETURN "MUSIC_STUDIO_MOCAP_CUTSCENE_STATE_WAIT_FOR_INTERIOR_READY"
	ENDSWITCH
	
	RETURN ""
ENDFUNC
#ENDIF

/// PURPOSE:
///    Wrapper for setting the mocap state for debugging. 
PROC SET_MUSIC_STUDIO_MOCAP_CUTSCENE_STATE(MUSIC_STUDIO_MOCAP_CUTSCENE_STATE eCutsceneState)
	CDEBUG1LN(DEBUG_PROPERTY, "SET_MUSIC_STUDIO_MOCAP_CUTSCENE_STATE - New state: ", GET_MUSIC_STUDIO_MOCAP_CUTSCENE_STATE_NAME(eCutsceneState))
	m_InteriorData.eMusicStudioSceneState = eCutsceneState
ENDPROC

FUNC BOOL DO_MUSIC_STUDIO_WEED_ROOM_MOCAP_CHECKS()
	IF IS_PLAYER_IN_CORONA()
	OR NETWORK_IS_ACTIVITY_SESSION()
		RETURN FALSE
	ENDIF
	
	IF NOT IS_LOCAL_PLAYER_IN_MUSIC_STUDIO_SMOKING_ROOM()
		RETURN FALSE
	ENDIF
	
	IF HAS_PLAYER_ACCEPTED_SIMPLE_INTERIOR_INVITE()
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_SimpleInteriorData.iEleventhBS, BS11_SIMPLE_INTERIOR_BLOCK_MUSIC_STUDIO_WEED_ROOM_MOCAP)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Determines if the player should watch the cutscene FIX_WRM_INT
FUNC BOOL SHOULD_WATCH_MOCAP_SCENE_WEED_ROOM_INTRO()
	IF NOT DO_MUSIC_STUDIO_WEED_ROOM_MOCAP_CHECKS()
		RETURN FALSE
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_ForceWeedRoomCutscenes")
		RETURN TRUE
	ENDIF
	#ENDIF
	
	IF HAS_LOCAL_PLAYER_VIEWED_MUSIC_STUDIO_WEED_ROOM_INTRO_CUTSCENE()
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL CAN_LOCAL_PLAYER_USE_THIS_SEAT()
	PLAYER_INDEX playerToCheck
		
	INT iPlayerIndex, iNumPart
	iNumPart = NETWORK_GET_NUM_PARTICIPANTS()
	REPEAT iNumPart iPlayerIndex
		IF iPlayerIndex != -1
		AND	NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPlayerIndex))
			playerToCheck = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPlayerIndex))
										
			IF IS_NET_PLAYER_OK(playerToCheck)
			AND playerToCheck != PLAYER_ID()
			AND NATIVE_TO_INT(playerToCheck) >= 0
				
				PED_INDEX pPed = GET_PLAYER_PED(playerToCheck)
	
				IF IS_PLAYER_SPAWNING_INTO_MUSIC_STUDIO_SMOKING_CHAIR(playerToCheck)
					RETURN FALSE
				ENDIF
				
				IF DOES_ENTITY_EXIST(pPed)
				AND NOT IS_ENTITY_DEAD(pPed)
					IF IS_ENTITY_IN_ANGLED_AREA(pPed, <<-1007.309326,-79.379921,-100.003067>>, <<-1007.308044,-78.605667,-97.944633>>, 1.5)
					OR IS_ENTITY_IN_ANGLED_AREA(pPed, <<-1007.299744,-78.566147,-100.003067>>, <<-1007.306458,-77.756546,-98.003067>>, 1.5)
						RETURN FALSE
					ENDIF
				ENDIF	
			ENDIF
		ENDIF
	ENDREPEAT

	RETURN TRUE
ENDFUNC


/// PURPOSE:
///    Wrapper for starting the given cutscene enum
PROC MUSIC_STUDIO_START_MOCAP(MUSIC_STUDIO_MOCAP_CUTSCENE eScene)
	m_InteriorData.eMocapScene = eScene
	SET_MUSIC_STUDIO_STATE(MUSIC_STUDIO_STATE_MOCAP)
	SET_MUSIC_STUDIO_MOCAP_CUTSCENE_STATE(MUSIC_STUDIO_MOCAP_CUTSCENE_STATE_INITIALISE)
	PRINTLN("MUSIC_STUDIO_START_MOCAP - Starting scene: ", eScene)
	IF eScene = MUSIC_STUDIO_MOCAP_CUTSCENE_WEED_ROOM_INTRO
	OR eScene = MUSIC_STUDIO_MOCAP_CUTSCENE_WEED_ROOM_ENTER
		IF NOT IS_BIT_SET(g_SimpleInteriorData.iEleventhBS, BS11_SIMPLE_INTERIOR_MUSIC_STUDIO_BLOCK_SPAWNING_IN_SMOKING_AREA_SEAT_AFTER_MODCAP)
			IF CAN_LOCAL_PLAYER_USE_THIS_SEAT()
				SET_PLAYER_SPAWNING_INTO_MUSIC_STUDIO_SMOKING_CHAIR(TRUE)
			ELSE
				PRINTLN("MUSIC_STUDIO_START_MOCAP - seat is ocupied")
			ENDIF
		ELSE
			PRINTLN("MUSIC_STUDIO_START_MOCAP - spawning in seat is blocked")
		ENDIF	
	ENDIF
ENDPROC

FUNC BOOL SHOULD_WATCH_MOCAP_SCENE_WEED_ROOM_ENTER()
	IF NOT DO_MUSIC_STUDIO_WEED_ROOM_MOCAP_CHECKS()
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_LOCAL_PLAYER_VIEWED_MUSIC_STUDIO_WEED_ROOM_INTRO_CUTSCENE()
		RETURN FALSE
	ENDIF
	
	IF HAS_LOCAL_PLAYER_VIEWED_WEED_ROOM_ENTER_CUTSCENE_THIS_SESSION()
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Sets the stat and variables for each cutscene once it's viewed. 
PROC SET_MUSIC_STUDIO_MOCAP_SCENE_AS_VIEWED(MUSIC_STUDIO_MOCAP_CUTSCENE eScene)
	IF eScene = MUSIC_STUDIO_MOCAP_CUTSCENE_WEED_ROOM_INTRO	
		SET_PLAYER_BD_HAS_VIEWED_MUSIC_STUDIO_WEED_ROOM_INTRO_MOCAP(TRUE)
		SET_LOCAL_PLAYER_HAS_VIEWED_MUSIC_STUDIO_WEED_ROOM_INTRO_CUTSCENE(TRUE)
	ELIF eScene = MUSIC_STUDIO_MOCAP_CUTSCENE_WEED_ROOM_ENTER
		SET_LOCAL_PLAYER_VIEWED_WEED_ROOM_ENTER_CUTSCENE_THIS_SESSION(TRUE)
	ELIF eScene = MUSIC_STUDIO_MOCAP_CUTSCENE_WEED_ROOM_EXIT
		SET_LOCAL_PLAYER_VIEWED_WEED_ROOM_EXIT_CUTSCENE_THIS_SESSION(TRUE)
	ENDIF
ENDPROC

/// PURPOSE:
///    Unhide any props that were previously hidden. 
PROC UNHIDE_PROPS_AFTER_MUSIC_STUDIO_MOCAP()
	INT i
	
	REPEAT MAX_HIDDEN_SCENE_OBJECTS i 
		IF DOES_ENTITY_EXIST(m_InteriorData.sceneHiddenProps[i])
			SET_ENTITY_VISIBLE(m_InteriorData.sceneHiddenProps[i], TRUE)
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Grabs the model of the object that needs to be hidden during the cutscnee. 
FUNC MODEL_NAMES GET_PROP_NAME_FOR_MOCAP_HIDE(MUSIC_STUDIO_MOCAP_CUTSCENE eScene, INT iIndex)
	SWITCH eScene
		CASE MUSIC_STUDIO_MOCAP_CUTSCENE_WEED_ROOM_INTRO
		CASE MUSIC_STUDIO_MOCAP_CUTSCENE_WEED_ROOM_ENTER
		CASE MUSIC_STUDIO_MOCAP_CUTSCENE_WEED_ROOM_EXIT
			SWITCH iIndex
				CASE 0 RETURN DUMMY_MODEL_FOR_SCRIPT // Compile!
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

/// PURPOSE:
///    Grabs the prop coords to hide during the cutscene. 
FUNC VECTOR GET_PROP_COORDS_FOR_MOCAP_HIDE(MUSIC_STUDIO_MOCAP_CUTSCENE eScene, INT iIndex)
	
	VECTOR vReturn
	
	SWITCH eScene
		CASE MUSIC_STUDIO_MOCAP_CUTSCENE_WEED_ROOM_INTRO
		CASE MUSIC_STUDIO_MOCAP_CUTSCENE_WEED_ROOM_ENTER
		CASE MUSIC_STUDIO_MOCAP_CUTSCENE_WEED_ROOM_EXIT
			SWITCH iIndex
				CASE 0 vReturn = <<0, 0, 0>>	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	IF NOT IS_VECTOR_ZERO(vReturn)
		vReturn = TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS_GIVEN_ID(vReturn, m_InteriorData.eSimpleInteriorID)
	ENDIF
	
	RETURN vReturn
ENDFUNC

/// PURPOSE:
///    Locally store the objects which need hidden during cutscene.
PROC CACHE_OBJECTS_FOR_HIDE_IN_MOCAP(MUSIC_STUDIO_MOCAP_CUTSCENE eScene)
	INT i
	MODEL_NAMES model
	VECTOR vCoords
	
	REPEAT MAX_HIDDEN_SCENE_OBJECTS i
		model 	= GET_PROP_NAME_FOR_MOCAP_HIDE(eScene, i)
		vCoords = GET_PROP_COORDS_FOR_MOCAP_HIDE(eScene, i)
		
		IF model != DUMMY_MODEL_FOR_SCRIPT
			m_InteriorData.sceneHiddenProps[i] = GET_CLOSEST_OBJECT_OF_TYPE(vCoords, 1.0, model, FALSE, FALSE, FALSE)
		ENDIF
	ENDREPEAT

	// Store any script created props here.
ENDPROC

/// PURPOSE:
///    Uses CACHE_OBJECTS_FOR_HIDE_IN_MOCAP to hide the objects whilst the cutscene is playing.  
PROC HIDE_PROPS_DURING_MUSIC_STUDIO_MOCAP(MUSIC_STUDIO_MOCAP_CUTSCENE eScene)
	INT i
	
	CACHE_OBJECTS_FOR_HIDE_IN_MOCAP(eScene)
	
	REPEAT MAX_HIDDEN_SCENE_OBJECTS i 
		IF DOES_ENTITY_EXIST(m_InteriorData.sceneHiddenProps[i])
			SET_ENTITY_VISIBLE(m_InteriorData.sceneHiddenProps[i], FALSE)
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Grabs any additional textures needed for the cutscene. 
FUNC STRING MUSIC_STUDIO_MOCAP_GET_ADDITIONAL_TXDS_REQUIRED(MUSIC_STUDIO_MOCAP_CUTSCENE eScene, INT iTXDIndex)
	SWITCH eScene
		CASE MUSIC_STUDIO_MOCAP_CUTSCENE_WEED_ROOM_INTRO
		CASE MUSIC_STUDIO_MOCAP_CUTSCENE_WEED_ROOM_ENTER
		CASE MUSIC_STUDIO_MOCAP_CUTSCENE_WEED_ROOM_EXIT
			SWITCH iTXDIndex
				CASE 0	RETURN "" // Compile!
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

/// PURPOSE:
///    If MUSIC_STUDIO_MOCAP_GET_ADDITIONAL_TXDS_REQUIRED isn't empty, request the texture.
FUNC BOOL MUSIC_STUDIO_MOCAP_LOAD_TEXTURE_DICTIONARIES_FOR_RENDER_TARGETS(MUSIC_STUDIO_MOCAP_CUTSCENE eScene)
	INT i
	STRING sTXD
	
	WHILE TRUE
		sTXD = MUSIC_STUDIO_MOCAP_GET_ADDITIONAL_TXDS_REQUIRED(eScene, i)
		
		IF IS_STRING_NULL_OR_EMPTY(sTXD)
			BREAKLOOP
		ELSE
			REQUEST_STREAMED_TEXTURE_DICT(sTXD)
			
			IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(sTXD)
				RETURN FALSE
			ENDIF
			
			i++
		ENDIF
	ENDWHILE
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    If MUSIC_STUDIO_MOCAP_GET_ADDITIONAL_TXDS_REQUIRED isn't empty, unloads the textures.
PROC MUSIC_STUDIO_MOCAP_UNLOAD_TEXTURE_DICTIONARIES_FOR_RENDER_TARGETS(MUSIC_STUDIO_MOCAP_CUTSCENE eScene)
	INT i
	STRING sTXD
	
	WHILE TRUE
		sTXD = MUSIC_STUDIO_MOCAP_GET_ADDITIONAL_TXDS_REQUIRED(eScene, i)
		
		IF IS_STRING_NULL_OR_EMPTY(sTXD)
			EXIT
		ELSE
			SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(sTXD)
			i++
		ENDIF
	ENDWHILE
ENDPROC

/// PURPOSE:
///    If cutscene contains any anim events, check them here.
PROC MUSIC_STUDIO_MOCAP_LISTEN_FOR_ANIM_EVENTS(MUSIC_STUDIO_MOCAP_CUTSCENE eScene)
	IF IS_ENTITY_ALIVE(m_InteriorData.scenePed)
		SWITCH eScene
			CASE MUSIC_STUDIO_MOCAP_CUTSCENE_WEED_ROOM_INTRO
			CASE MUSIC_STUDIO_MOCAP_CUTSCENE_WEED_ROOM_ENTER
			CASE MUSIC_STUDIO_MOCAP_CUTSCENE_WEED_ROOM_EXIT
				// Add anim events here.
			BREAK
		ENDSWITCh
	ENDIF
ENDPROC

/// PURPOSE:
///    Cleans up everything to do with the cutscene. 
PROC CLEANUP_MUSIC_STUDIO_CUTSCENE(BOOL bClearActiveCutsceneFlag = FALSE)
	IF IS_CUTSCENE_PLAYING()
		STOP_CUTSCENE_IMMEDIATELY()
		REMOVE_CUTSCENE()
	ENDIF
	
	BOOL bClearHelp = FALSE
	
	IF IS_ANY_FLOATING_HELP_BEING_DISPLAYED()
		bClearHelp = TRUE
	ENDIF
	
	CLEANUP_MP_CUTSCENE(bClearHelp, TRUE)
	
	NETWORK_SET_VOICE_ACTIVE(TRUE)
	
	IF DOES_ENTITY_EXIST(m_InteriorData.scenePed)
		DELETE_PED(m_InteriorData.scenePed)
	ENDIF
	
	IF IS_BIT_SET(m_InteriorData.iBS, BS_MUSIC_STUDIO_SET_INVISIBLE_FOR_MOCAP)
		SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
		CLEAR_BIT(m_InteriorData.iBS, BS_MUSIC_STUDIO_SET_INVISIBLE_FOR_MOCAP)
	ENDIF
	
	IF bClearActiveCutsceneFlag
		SET_LOCAL_PLAYER_WATCHING_MUSIC_STUDIO_WEED_ROOM_INTRO_CUTSCENE(FALSE)
	ENDIF
		
	SET_BIT(m_InteriorData.iBS, BS_MUSIC_STUDIO_WATCHED_MOCAP)
	
	m_InteriorData.bIdleTimeout = FALSE
	UNHIDE_PROPS_AFTER_MUSIC_STUDIO_MOCAP()
	MUSIC_STUDIO_MOCAP_UNLOAD_TEXTURE_DICTIONARIES_FOR_RENDER_TARGETS(m_InteriorData.eMocapScene)
ENDPROC

/// PURPOSE:
///    Finish the cutscene. 
FUNC BOOL FINISH_MUSIC_STUDIO_MOCAP_SCENE(MUSIC_STUDIO_MOCAP_CUTSCENE eScene)
	IF eScene = MUSIC_STUDIO_MOCAP_CUTSCENE_WEED_ROOM_INTRO
		SET_LOCAL_PLAYER_WATCHING_MUSIC_STUDIO_WEED_ROOM_INTRO_CUTSCENE(FALSE)
	ENDIF
	
	IF eScene != MUSIC_STUDIO_MOCAP_CUTSCENE_WEED_ROOM_EXIT
		IF NOT IS_SIMPLE_INTERIOR_CHILD_SCRIPT_READY()
			SET_SIMPLE_INTERIOR_CHILD_SCRIPT_READY(TRUE)
			SET_SIMPLE_INTERIOR_CHILD_SCRIPT_IS_IN_CONTROL(FALSE, FALSE)
			g_SimpleInteriorData.bDontSimulateInputGaitOnEntrance = TRUE
		ENDIF
	ENDIF
	SET_BIT(m_InteriorData.iBS, BS_MUSIC_STUDIO_WATCHED_MOCAP)
	
	RETURN TRUE
ENDFUNC

FUNC CUTSCENE_SECTION GET_WEED_ROOM_ENTER_CUTSCENE_SECTION()
	IF NOT IS_BIT_SET(g_SimpleInteriorData.iEleventhBS, BS11_SIMPLE_INTERIOR_WALKED_INTO_WEED_ROOM_WITH_LAMAR_AND_FRANKLIN_THIS_SESSION)
		SET_BIT(g_SimpleInteriorData.iEleventhBS, BS11_SIMPLE_INTERIOR_WALKED_INTO_WEED_ROOM_WITH_LAMAR_AND_FRANKLIN_THIS_SESSION)
		RETURN CS_SECTION_4 // Walking in with Lamar & Franklin boi
	ENDIF
	
	INT iSectionIndex = GET_RANDOM_INT_IN_RANGE(0, 3)
	SWITCH iSectionIndex
		CASE 0	RETURN CS_SECTION_1 // Waves and sits down.
		CASE 1	RETURN CS_SECTION_2 // Shrugs and sits down.
		CASE 2	RETURN CS_SECTION_3 // Fist bumps and sits down.
	ENDSWITCH
	
	RETURN CS_SECTION_1
ENDFUNC

FUNC CUTSCENE_SECTION GET_WEED_ROOM_EXIT_CUTSCENE_SECTION()
	INT iWeedCount = Get_Peds_Drunk_Weed_Hit_Count(PLAYER_PED_ID())
	IF iWeedCount > 0
		PRINTLN("GET_WEED_ROOM_EXIT_CUTSCENE_SECTION - Stoned. Weed count: ", iWeedCount)
		RETURN CS_SECTION_5 // Is high and struggles to open door.
	ENDIF
	
	INT iSectionIndex = GET_RANDOM_INT_IN_RANGE(0, 2)
	SWITCH iSectionIndex
		CASE 0	RETURN CS_SECTION_1 | CS_SECTION_2 	// Leaves couch, streches, and leaves.
		CASE 1	RETURN CS_SECTION_3 | CS_SECTION_4 	// Streches and leaves. 
	ENDSWITCH
	
	RETURN CS_SECTION_1 | CS_SECTION_2
ENDFUNC

/// PURPOSE:
///    Requests the cutscene based on the given enum.
PROC REQUEST_CINEMATIC_CUTSCENE(MUSIC_STUDIO_MOCAP_CUTSCENE eScene)
	IF eScene = MUSIC_STUDIO_MOCAP_CUTSCENE_WEED_ROOM_INTRO
		REQUEST_CUTSCENE("FIX_WRM_INT", CUTSCENE_PLAYBACK_FORCE_LOAD_AUDIO_EVENT)
	ELIF eScene = MUSIC_STUDIO_MOCAP_CUTSCENE_WEED_ROOM_ENTER
		REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("FIX_WEED_INT", GET_WEED_ROOM_ENTER_CUTSCENE_SECTION(), CUTSCENE_PLAYBACK_FORCE_LOAD_AUDIO_EVENT)
	ELIF eScene = MUSIC_STUDIO_MOCAP_CUTSCENE_WEED_ROOM_EXIT
		REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("FIX_WEED_EXT", GET_WEED_ROOM_EXIT_CUTSCENE_SECTION(), CUTSCENE_PLAYBACK_FORCE_LOAD_AUDIO_EVENT)
	ENDIF
ENDPROC

/// PURPOSE:
///    Main update for handling the cutscenes
PROC MAINTAIN_MUSIC_STUDIO_MOCAP_CUTSCENE_STATE(MUSIC_STUDIO_MOCAP_CUTSCENE eScene)
	
	IF ((IS_SKYSWOOP_MOVING() OR IS_SKYSWOOP_IN_SKY()) AND NOT IS_SKYSWOOP_AT_GROUND())
	OR IS_ANY_TRANSITION_SESSION_INVITE_BEING_ACTIONED()
	#IF IS_DEBUG_BUILD
	OR IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED()
	#ENDIF
		IF NOT g_SpawnData.bSpawningInSimpleInterior
		#IF IS_DEBUG_BUILD
		AND NOT GET_COMMANDLINE_PARAM_EXISTS("sc_SkipFixerHQCutsceneFailsafe")
		#ENDIF
			CLEANUP_MUSIC_STUDIO_CUTSCENE(TRUE)
			
			#IF IS_DEBUG_BUILD
			IF NOT IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED()
			#ENDIF
				IF eScene = MUSIC_STUDIO_MOCAP_CUTSCENE_WEED_ROOM_EXIT
					TRIGGER_SWITCH_BETWEEN_SIMPLE_INTERIOR_INTERIORS(m_InteriorData.eSimpleInteriorID)
				ELSE
					SET_MUSIC_STUDIO_STATE(MUSIC_STUDIO_STATE_LOAD_INTERIOR)
				ENDIF
			#IF IS_DEBUG_BUILD
			ENDIF
			#ENDIF
			
			CDEBUG1LN(DEBUG_CUTSCENE, "MAINTAIN_MUSIC_STUDIO_MOCAP_CUTSCENE_STATE - Child script is ready, mocap has not played yet, cleaning up mocap.")
			
			EXIT
		ELSE
			CDEBUG1LN(DEBUG_CUTSCENE, "MAINTAIN_MUSIC_STUDIO_MOCAP_CUTSCENE_STATE - Child script is ready, mocap has not played yet, Preventing mocap cleanup for bSpawningInSimpleInterior.")
		ENDIF
	ENDIF
	
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
	AND m_InteriorData.eMusicStudioSceneState != MUSIC_STUDIO_MOCAP_CUTSCENE_STATE_CLEANUP
	AND m_InteriorData.eMusicStudioSceneState != MUSIC_STUDIO_MOCAP_CUTSCENE_STATE_WAIT_FOR_INTERIOR_READY
	AND eScene != MUSIC_STUDIO_MOCAP_CUTSCENE_WEED_ROOM_EXIT
		SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
		SET_BIT(m_InteriorData.iBS, BS_MUSIC_STUDIO_SET_INVISIBLE_FOR_MOCAP)
	ENDIF
	
	VECTOR vInteriorPos
	PLAYER_INDEX pedForIntro = PLAYER_ID()
	
	SWITCH m_InteriorData.eMusicStudioSceneState
		CASE MUSIC_STUDIO_MOCAP_CUTSCENE_STATE_INITIALISE
			vInteriorPos = GET_MUSIC_STUDIO_INTERIOR_POSITION()
			
			IF MUSIC_STUDIO_MOCAP_LOAD_TEXTURE_DICTIONARIES_FOR_RENDER_TARGETS(eScene)
			AND SMPL_INTERIOR_CREATE_PLAYER_PED_COPY_FOR_INTRO_CUT(pedForIntro, TRUE, m_InteriorData.scenePed, vInteriorPos, 180)
				CDEBUG1LN(DEBUG_CUTSCENE, "MAINTAIN_MUSIC_STUDIO_MOCAP_CUTSCENE_STATE - FIXER_HQ_MOCAP_CUTSCENE_STATE_INITIALISE")
				
				HIDE_PROPS_DURING_MUSIC_STUDIO_MOCAP(eScene)
				REQUEST_CINEMATIC_CUTSCENE(eScene)
				SET_MUSIC_STUDIO_MOCAP_CUTSCENE_STATE(MUSIC_STUDIO_MOCAP_CUTSCENE_STATE_ASSET_REQUEST)
			ENDIF
		BREAK
		
		CASE MUSIC_STUDIO_MOCAP_CUTSCENE_STATE_ASSET_REQUEST
			IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
				
				SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_1", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)				
				SET_MUSIC_STUDIO_MOCAP_CUTSCENE_STATE(MUSIC_STUDIO_MOCAP_CUTSCENE_STATE_START_CUTSCENE)
			ENDIF
		BREAK
		
		CASE MUSIC_STUDIO_MOCAP_CUTSCENE_STATE_START_CUTSCENE
			CDEBUG1LN(DEBUG_CUTSCENE, "MAINTAIN_MUSIC_STUDIO_MOCAP_CUTSCENE_STATE - Loading scene")
			
			IF HAS_CUTSCENE_LOADED()
				IF IS_ENTITY_ALIVE(m_InteriorData.scenePed)
					CDEBUG1LN(DEBUG_CUTSCENE, "MAINTAIN_MUSIC_STUDIO_MOCAP_CUTSCENE_STATE - Registering scene ped MP_1 for cut")
					REGISTER_ENTITY_FOR_CUTSCENE(m_InteriorData.scenePed, "MP_1", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
					SET_ENTITY_VISIBLE(m_InteriorData.scenePed, TRUE)
				ELSE
					//Remove any peds we don't need
					REGISTER_ENTITY_FOR_CUTSCENE(NULL, "MP_1", CU_DONT_ANIMATE_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
				ENDIF
				
				IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
				AND eScene = MUSIC_STUDIO_MOCAP_CUTSCENE_WEED_ROOM_EXIT
					SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
					SET_BIT(m_InteriorData.iBS, BS_MUSIC_STUDIO_SET_INVISIBLE_FOR_MOCAP)
				ENDIF
				
				CDEBUG1LN(DEBUG_CUTSCENE, "MAINTAIN_MUSIC_STUDIO_MOCAP_CUTSCENE_STATE - Start scene")
				START_CUTSCENE()
				START_MP_CUTSCENE()
				g_bIsPlayerWatchingMusicStudioCutscene = TRUE
				MAINTAIN_REMOTE_PLAYER_VISIBILITY_STATE(FALSE)
				SET_BIT(m_InteriorData.iBS, BS_MUSIC_STUDIO_WATCHING_MOCAP)
				
				IF m_InteriorData.eMocapScene = MUSIC_STUDIO_MOCAP_CUTSCENE_WEED_ROOM_INTRO
					SET_LOCAL_PLAYER_WATCHING_MUSIC_STUDIO_WEED_ROOM_INTRO_CUTSCENE(TRUE)
				ENDIF
				
				SET_MUSIC_STUDIO_MOCAP_CUTSCENE_STATE(MUSIC_STUDIO_MOCAP_CUTSCENE_STATE_IDLING)
			ENDIF
		BREAK
		
		CASE MUSIC_STUDIO_MOCAP_CUTSCENE_STATE_IDLING
			IF IS_CUTSCENE_PLAYING()
				MAINTAIN_REMOTE_PLAYER_VISIBILITY_STATE(FALSE)
				
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(500)
					
					PRINTLN("MAINTAIN_MUSIC_STUDIO_MOCAP_CUTSCENE_STATE - Interior scene is playing, fading screen in.")
				ENDIF
				
				IF eScene != MUSIC_STUDIO_MOCAP_CUTSCENE_WEED_ROOM_EXIT
					IF NOT IS_SIMPLE_INTERIOR_CHILD_SCRIPT_READY()
						SET_SIMPLE_INTERIOR_CHILD_SCRIPT_READY(TRUE)
						SET_SIMPLE_INTERIOR_CHILD_SCRIPT_IS_IN_CONTROL(FALSE, FALSE)
						g_SimpleInteriorData.bDontSimulateInputGaitOnEntrance = TRUE
					ENDIF
				ENDIF
				
				IF HAS_NET_TIMER_STARTED(m_InteriorData.tIdleFallback)
					RESET_NET_TIMER(m_InteriorData.tIdleFallback)
					CDEBUG1LN(DEBUG_CUTSCENE, "MAINTAIN_MUSIC_STUDIO_MOCAP_CUTSCENE_STATE - Resetting idle fallback timer.")
				ENDIF
				
				RESET_NET_TIMER(m_InteriorData.timeFailSafe)
				
				SET_MUSIC_STUDIO_MOCAP_CUTSCENE_STATE(MUSIC_STUDIO_MOCAP_CUTSCENE_STATE_PLAY_INTERIOR)
			ELSE
				CDEBUG1LN(DEBUG_CUTSCENE, "MAINTAIN_MUSIC_STUDIO_MOCAP_CUTSCENE_STATE - Waiting for cutscene playback.")
				
				IF NOT HAS_NET_TIMER_STARTED(m_InteriorData.tIdleFallback)
					START_NET_TIMER(m_InteriorData.tIdleFallback)
					CDEBUG1LN(DEBUG_CUTSCENE, "MAINTAIN_MUSIC_STUDIO_MOCAP_CUTSCENE_STATE - Starting idle fallback timer.")
				ENDIF
				
				IF HAS_NET_TIMER_EXPIRED(m_InteriorData.tIdleFallback, 7500)
					CDEBUG1LN(DEBUG_CUTSCENE, "MAINTAIN_MUSIC_STUDIO_MOCAP_CUTSCENE_STATE - Idle fallback timer has expired, bailing on cutscene.")
					SET_MUSIC_STUDIO_MOCAP_CUTSCENE_STATE(MUSIC_STUDIO_MOCAP_CUTSCENE_STATE_CLEANUP)
				ENDIF
			ENDIF
		BREAK
		
		CASE MUSIC_STUDIO_MOCAP_CUTSCENE_STATE_PLAY_INTERIOR
						
			DISABLE_CELLPHONE_THIS_FRAME_ONLY(TRUE)
			MAINTAIN_REMOTE_PLAYER_VISIBILITY_STATE(FALSE)
			MUSIC_STUDIO_MOCAP_LISTEN_FOR_ANIM_EVENTS(eScene)
			
			IF NOT HAS_NET_TIMER_STARTED(m_InteriorData.timeFailSafe)
				START_NET_TIMER(m_InteriorData.timeFailSafe)
			ENDIF
			
			IF HAS_NET_TIMER_EXPIRED(m_InteriorData.timeFailSafe, MAX_INTRO_CUTSCENE_FALLBACK_TIME)
				IF NOT IS_SCREEN_FADED_OUT() AND NOT IS_SCREEN_FADING_OUT()
					DO_SCREEN_FADE_OUT(0)
					SET_MUSIC_STUDIO_MOCAP_SCENE_AS_VIEWED(eScene)
				ENDIF
				
				IF IS_SCREEN_FADED_OUT()
					RESET_NET_TIMER(m_InteriorData.timeFailSafe)
					SET_MUSIC_STUDIO_MOCAP_CUTSCENE_STATE(MUSIC_STUDIO_MOCAP_CUTSCENE_STATE_CLEANUP)
				ENDIF
			ENDIF
			
			IF IS_CUTSCENE_PLAYING()
				IF eScene = MUSIC_STUDIO_MOCAP_CUTSCENE_WEED_ROOM_EXIT
					IF IS_SCREEN_FADED_IN()
					AND ((GET_CUTSCENE_END_TIME() - GET_CUTSCENE_TIME()) <= MUSIC_STUDIO_MOCAP_CUTSCENE_EARLY_FADE_TIME)
						DO_SCREEN_FADE_OUT(MUSIC_STUDIO_MOCAP_CUTSCENE_EARLY_FADE_TIME)
					ENDIF
				ENDIF
			ELSE
				SET_MUSIC_STUDIO_MOCAP_SCENE_AS_VIEWED(eScene)
				CLEANUP_MP_CUTSCENE(FALSE, FALSE)
				SET_MUSIC_STUDIO_MOCAP_CUTSCENE_STATE(MUSIC_STUDIO_MOCAP_CUTSCENE_STATE_CLEANUP)
				RESET_NET_TIMER(m_InteriorData.timeFailSafe)
				
				IF DOES_ENTITY_EXIST(m_InteriorData.scenePed)
					DELETE_PED(m_InteriorData.scenePed)
				ENDIF
				
				IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
					IF eScene = MUSIC_STUDIO_MOCAP_CUTSCENE_WEED_ROOM_INTRO
					OR eScene = MUSIC_STUDIO_MOCAP_CUTSCENE_WEED_ROOM_ENTER
						// Spawn player somewhere in weed room. 
						REINIT_NET_TIMER(m_InteriorData.timeFailSafe)
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE MUSIC_STUDIO_MOCAP_CUTSCENE_STATE_CLEANUP
			g_bIsPlayerWatchingMusicStudioCutscene = FALSE
			IF NOT HAS_NET_TIMER_STARTED(m_InteriorData.tIdleFallback)
				START_NET_TIMER(m_InteriorData.tIdleFallback)
			ENDIF
			
			// If we idle in the cleanup for more than 5 seconds then bail and force cleanup.
			IF HAS_NET_TIMER_EXPIRED(m_InteriorData.tIdleFallback, 5000)
				m_InteriorData.bIdleTimeout = TRUE				
				CDEBUG1LN(DEBUG_CUTSCENE, "MAINTAIN_MUSIC_STUDIO_MOCAP_CUTSCENE_STATE - Idle fallback timer has expired, forcing cleanup.")
			ENDIF
			
			IF NOT HAS_NET_TIMER_STARTED(m_InteriorData.timeFailSafe)
			OR (HAS_NET_TIMER_EXPIRED(m_InteriorData.timeFailSafe, 1000) AND IS_INTERIOR_READY(m_InteriorData.iInteriorID))
			OR m_InteriorData.bIdleTimeout
			OR eScene = MUSIC_STUDIO_MOCAP_CUTSCENE_WEED_ROOM_EXIT
				
				IF eScene = MUSIC_STUDIO_MOCAP_CUTSCENE_WEED_ROOM_INTRO
				OR eScene = MUSIC_STUDIO_MOCAP_CUTSCENE_WEED_ROOM_ENTER
					IF IS_SCREEN_FADED_OUT()
						DO_SCREEN_FADE_IN(0)
					ENDIF
				ENDIF
				
				IF IS_SCREEN_FADED_IN()
				OR m_InteriorData.bIdleTimeout
				OR eScene = MUSIC_STUDIO_MOCAP_CUTSCENE_WEED_ROOM_EXIT
					CLEANUP_MUSIC_STUDIO_CUTSCENE()
					SIMPLE_INTERIOR_INT_SCRIPT_MOCAP_REQUEST_EXT_ESTABLISING_SHOT_CLEANUP()
					
					// Update all viewers stats to say they have watched the cutscene.
					IF NOT m_InteriorData.bIdleTimeout
						SET_MUSIC_STUDIO_MOCAP_SCENE_AS_VIEWED(eScene)
					ENDIF
					
					IF eScene = MUSIC_STUDIO_MOCAP_CUTSCENE_WEED_ROOM_EXIT
						TRIGGER_SWITCH_BETWEEN_SIMPLE_INTERIOR_INTERIORS(m_InteriorData.eSimpleInteriorID)
					ELSE
						IF HAS_NET_TIMER_EXPIRED(m_InteriorData.tRefreshTimer, 2000)
							IF FINISH_MUSIC_STUDIO_MOCAP_SCENE(eScene)		
								SET_SIMPLE_INTERIOR_INT_SCRIPT_SHOULD_NOT_FADE_IN_ON_ENTRY(TRUE)
								SET_MUSIC_STUDIO_MOCAP_CUTSCENE_STATE(MUSIC_STUDIO_MOCAP_CUTSCENE_STATE_WAIT_FOR_INTERIOR_READY)
								CDEBUG1LN(DEBUG_CUTSCENE, "MAINTAIN_MUSIC_STUDIO_MOCAP_CUTSCENE_STATE - ******* FINISHED ******")
							ENDIF
						ELSE
							CDEBUG1LN(DEBUG_CUTSCENE, "MAINTAIN_MUSIC_STUDIO_MOCAP_CUTSCENE_STATE - m_InteriorData.tRefreshTimer NOT expired.")
						ENDIF
					ENDIF
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_CUTSCENE, "MAINTAIN_MUSIC_STUDIO_MOCAP_CUTSCENE_STATE - WAITING FOR REFRESH")
			ENDIF
		BREAK
		
		CASE MUSIC_STUDIO_MOCAP_CUTSCENE_STATE_WAIT_FOR_INTERIOR_READY
			IF g_iSimpleInteriorState = SIMPLE_INT_STATE_IDLE
			OR SIMPLE_INTERIOR_IS_LOCAL_PLAYER_ENTERING_WITH_VEHICLE()
				
				SET_MUSIC_STUDIO_STATE(MUSIC_STUDIO_STATE_IDLE)				
				CLEAR_BIT(m_InteriorData.iBS, BS_MUSIC_STUDIO_WATCHING_MOCAP)
				
				CDEBUG1LN(DEBUG_CUTSCENE, "MAINTAIN_MUSIC_STUDIO_MOCAP_CUTSCENE_STATE - Moving to auto shop state idle.")
			ELSE
				CDEBUG1LN(DEBUG_CUTSCENE, "MAINTAIN_MUSIC_STUDIO_MOCAP_CUTSCENE_STATE - MUSIC_STUDIO_MOCAP_CUTSCENE_STATE_WAIT_FOR_INTERIOR_READY - Waiting on g_iSimpleInteriorState")
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC UPDATE_CHECK_FOR_EXIT_MOCAP()
	IF IS_BIT_SET(g_SimpleInteriorData.iSeventhBS, BS7_SIMPLE_INTERIOR_LOCAL_PLAYER_PLAYING_INT_EXIT_MOCAP)
		MUSIC_STUDIO_START_MOCAP(MUSIC_STUDIO_MOCAP_CUTSCENE_WEED_ROOM_EXIT)
	ENDIF
ENDPROC

PROC LOAD_INTERIOR()
	
	IF NOT IS_BIT_SET(m_InteriorData.iBS, BS_MUSIC_STUDIO_INTERIOR_REFRESH_ON_INIT)
	
		IF NOT m_InteriorData.bScriptRelaunched 
			
			GET_SIMPLE_INTERIOR_INTERIOR_TYPE_AND_POSITION(m_InteriorData.eSimpleInteriorID, m_InteriorData.sInteriorType, m_InteriorData.vInteriorPosition, m_InteriorData.fInteriorHeading, TRUE)
			m_InteriorData.iInteriorID = GET_INTERIOR_AT_COORDS_WITH_TYPE(m_InteriorData.vInteriorPosition, m_InteriorData.sInteriorType)
			
			IF IS_VALID_INTERIOR(m_InteriorData.iInteriorID) AND IS_INTERIOR_READY(m_InteriorData.iInteriorID)
				
				BOOL bReady = TRUE
				
				IF bReady 
					IF IS_PLAYER_IN_MUSIC_STUDIO(PLAYER_ID())
					
						INT iRoomKey = GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID())
					
						IF iRoomKey = ENUM_TO_INT(MUSIC_STUDIO_ROOM_KEY)
							FORCE_ROOM_FOR_ENTITY(PLAYER_PED_ID(), m_InteriorData.iInteriorID, iRoomKey)
							CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_MUSIC_STUDIO - LOAD_INTERIOR - Forcing room for player, we are inside the interior.")
						ELSE
							CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_MUSIC_STUDIO - LOAD_INTERIOR - Player is in the MUSIC_STUDIO but unable to determine if we should call FORCE_ROOM_FOR_ENTITY")
						ENDIF
					ELSE
						CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_MUSIC_STUDIO - LOAD_INTERIOR - Preventing room force, not inside interior yet.")
					ENDIF
					
					REPIN_AND_REFRESH_SIMPLE_INTERIOR()
					
					SET_SIMPLE_INTERIOR_CHILD_SCRIPT_ALLOWS_FOR_WARP(TRUE)
					SET_BIT(m_InteriorData.iBS, BS_MUSIC_STUDIO_INTERIOR_REFRESH_ON_INIT)
					CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_MUSIC_STUDIO - LOAD_INTERIOR - Setting BS_MUSIC_STUDIO_INTERIOR_REFRESH_ON_INIT")
					
					RESET_NET_TIMER(m_InteriorData.tPinInMemTimer)
				ENDIF
			ELSE
				IF NOT HAS_NET_TIMER_STARTED(m_InteriorData.tPinInMemTimer)
					CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_MUSIC_STUDIO - LOAD_INTERIOR - Pinning interior: ", NATIVE_TO_INT(m_InteriorData.iInteriorID), " in memory.")
					PIN_INTERIOR_IN_MEMORY_VALIDATE(m_InteriorData.iInteriorID)
					START_NET_TIMER(m_InteriorData.tPinInMemTimer)
				ENDIF
				
				IF HAS_NET_TIMER_EXPIRED(m_InteriorData.tPinInMemTimer, m_InteriorData.iPinInMemTimeToExpire)
					RESET_NET_TIMER(m_InteriorData.tPinInMemTimer)
				ENDIF
				
				#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_MUSIC_STUDIO - LOAD_INTERIOR - Waiting for a valid and ready interior with ID: ", NATIVE_TO_INT(m_InteriorData.iInteriorID), " valid? ",
					IS_VALID_INTERIOR(m_InteriorData.iInteriorID), " ready? ", IS_INTERIOR_READY(m_InteriorData.iInteriorID), " disabled? ", IS_INTERIOR_DISABLED(m_InteriorData.iInteriorID))
				#ENDIF
			ENDIF
		ELSE
			SET_SIMPLE_INTERIOR_CHILD_SCRIPT_ALLOWS_FOR_WARP(TRUE)
			SET_BIT(m_InteriorData.iBS, BS_MUSIC_STUDIO_INTERIOR_REFRESH_ON_INIT)
			CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_MUSIC_STUDIO - LOAD_INTERIOR - Setting BS_MUSIC_STUDIO_INTERIOR_REFRESH_ON_INIT")
		ENDIF
	ENDIF
	
	IF CAN_SIMPLE_INTERIOR_CHILD_SCRIPT_TAKE_CONTROL()
	AND IS_BIT_SET(m_InteriorData.iBS, BS_MUSIC_STUDIO_INTERIOR_REFRESH_ON_INIT)
		
		BOOL bReady = TRUE
	
		IF NOT IS_BIT_SET(m_InteriorData.iBS, BS_MUSIC_STUDIO_LOAD_SCENE_STARTED)
			
			IF IS_PLAYER_SWITCH_IN_PROGRESS()
				PRINTLN("AM_MP_MUSIC_STUDIO - LOAD_INTERIOR - Can't start load scene player switch is in progress")
				EXIT
			ENDIF
			
			VECTOR vLoadCoords
			
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
				vLoadCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
			ELSE
				PRINTLN("AM_MP_MUSIC_STUDIO - LOAD_INTERIOR - Can't start load scene player is dead")
				EXIT
			ENDIF
			
			NEW_LOAD_SCENE_START_SPHERE(vLoadCoords, 25.0)
			
			SET_BIT(m_InteriorData.iBS, BS_MUSIC_STUDIO_LOAD_SCENE_STARTED)
			
			PRINTLN("AM_MP_MUSIC_STUDIO - LOAD_INTERIOR - Setting BS_MUSIC_STUDIO_LOAD_SCENE_STARTED")			
			bReady = FALSE
		ELSE
			IF IS_NEW_LOAD_SCENE_ACTIVE()
				
				IF NOT IS_NEW_LOAD_SCENE_LOADED()
					#IF IS_DEBUG_BUILD
					PRINTLN("AM_MP_MUSIC_STUDIO - LOAD_INTERIOR - IS_NEW_LOAD_SCENE_LOADED = FALSE")
					#ENDIF
					bReady = FALSE
				ELSE
					GET_SIMPLE_INTERIOR_INTERIOR_TYPE_AND_POSITION(m_InteriorData.eSimpleInteriorID, m_InteriorData.sInteriorType, m_InteriorData.vInteriorPosition, m_InteriorData.fInteriorHeading)
					m_InteriorData.iInteriorID = GET_INTERIOR_AT_COORDS_WITH_TYPE(m_InteriorData.vInteriorPosition, m_InteriorData.sInteriorType)
					
					IF IS_VALID_INTERIOR(m_InteriorData.iInteriorID) 
					AND IS_INTERIOR_READY(m_InteriorData.iInteriorID)
						#IF IS_DEBUG_BUILD
						PRINTLN("AM_MP_MUSIC_STUDIO - LOAD_INTERIOR - Player is inside interior bounds, load scene is ready.")
						#ENDIF
						NEW_LOAD_SCENE_STOP()
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT HAVE_ALL_PEDS_BEEN_CREATED()
			PRINTLN("AM_MP_MUSIC_STUDIO- LOAD_INTERIOR - Waiting for the peds to be created.")
			bReady = FALSE
			
			IF HAS_NET_TIMER_EXPIRED(m_InteriorData.stPedLaunchingBailTimer, g_iPedScriptLaunchingBailTimerMS)
				PRINTLN("AM_MP_MUSIC_STUDIO - LOAD_INTERIOR - Ped script has taken longer than: ", g_iPedScriptLaunchingBailTimerMS, "ms so setting g_bInitPedsCreated to TRUE.")
				g_bInitPedsCreated = TRUE
			ENDIF
		ENDIF
		
		IF bReady
			SET_MUSIC_STUDIO_STATE(MUSIC_STUDIO_STATE_CREATE_ENTITIES)			
			
			//IF IS_LOCAL_PLAYER_IN_MUSIC_STUDIO_SMOKING_ROOM()
			//OR IS_BIT_SET(g_SimpleInteriorData.iEleventhBS, BS11_SIMPLE_INTERIOR_MUSIC_STUDIO_LEAVING_SMOKING_ROOM)
				g_SimpleInteriorData.bDontSimulateInputGaitOnEntrance = TRUE
			//ENDIF
			
			IF IS_BIT_SET(g_SimpleInteriorData.iEleventhBS, BS11_SIMPLE_INTERIOR_MUSIC_STUDIO_LEAVING_SMOKING_ROOM)
				SET_BIT(m_InteriorData.iBS, BS_MUSIC_STUDIO_ENTERED_INTERIOR_FROM_SMOKING_ROOM)
			ENDIF
			
			CLEAR_BIT(g_SimpleInteriorData.iEleventhBS, BS11_SIMPLE_INTERIOR_MUSIC_STUDIO_LEAVING_SMOKING_ROOM)
		ENDIF
	ELSE
		CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_MUSIC_STUDIO - LOAD_INTERIOR - CAN_SIMPLE_INTERIOR_CHILD_SCRIPT_TAKE_CONTROL = ", CAN_SIMPLE_INTERIOR_CHILD_SCRIPT_TAKE_CONTROL(), ", BS_MUSIC_STUDIO_INTERIOR_REFRESH_ON_INIT = ", IS_BIT_SET(m_InteriorData.iBS, BS_MUSIC_STUDIO_INTERIOR_REFRESH_ON_INIT))
	ENDIF
ENDPROC

PROC MAINTAIN_PLAYER_FLAGS()
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
	AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
	AND NOT g_bEnableJumpingOffHalfTrackInProperty
		SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisablePlayerJumping, TRUE)
		SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisablePlayerVaulting, TRUE)
		SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisablePlayerAutoVaulting, TRUE)
	ENDIF
ENDPROC

PROC MAINTAIN_MUSIC_STUDIO_ACTIVITIES()
	interiorStruct.vInteriorPos = GET_MUSIC_STUDIO_BLIP_COORDS(m_InteriorData.eSimpleInteriorID)
	
	IF IS_PLAYER_IN_MUSIC_STUDIO(PLAYER_ID())	
		IF NOT IS_SCREEN_FADED_OUT()
			IF NOT HAS_DR_DRE_STAGE_NEARLY_EXPIRED()
				INIT_ACTIVITY_LAUNCH_SCRIPT_CHECK_WITH_AI_PEDS_MUSIC_STUDIO(activityControllerStruct, activityCheck, m_ServerBD.activityProps, m_ServerBD.activityPeds, pedLocalVariationsStruct, interiorStruct, bunkerFactoryActivityStruct)
				MAINTAIN_APARTMENT_ACTIVITIES_CREATOR(activityControllerStruct, activityCheck, m_ServerBD.activityProps, m_ServerBD.activityPeds)
			ELSE
				activityControllerStruct.bInitCheckStruct = FALSE
				activityControllerStruct.bAllPropsCreated = FALSE
				activityControllerStruct.iActivityCountForInterior = 0
				RESET_CREATOR_ACTIVITIES_ONLY(activityControllerStruct, activityCheck)
				RESET_MUSIC_STUDIO_ACTIVITIES(activityControllerStruct, activityCheck, FALSE)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CONTEXT)
				DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_CONTEXT)
				PRINTLN("AM_MP_MUSIC_STUDIO - MAINTAIN_MUSIC_STUDIO_ACTIVITIES - BLOCKING ACTIVITIES - DRE IS ABOUT TO CHANGE STATE")
			ENDIF
		ELSE
			DESTROY_ALL_NETWORK_ACTIVITY_PROPS(m_ServerBD.activityProps)
			DESTROY_ALL_NETWORK_ACTIVITY_PEDS(m_ServerBD.activityPeds)
			DESTROY_ALL_LOCAL_ACTIVITY_PROPS(activityCheck)
			DESTROY_ALL_LOCAL_ACTIVITY_PEDS(activityCheck)
			activityControllerStruct.bInitCheckStruct = FALSE
			activityControllerStruct.bAllPropsCreated = FALSE
			activityControllerStruct.iActivityCountForInterior = 0
			RESET_CREATOR_ACTIVITIES_ONLY(activityControllerStruct, activityCheck)
			RESET_MUSIC_STUDIO_ACTIVITIES(activityControllerStruct, activityCheck)
			CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "MAINTAIN_MUSIC_STUDIO_ACTIVITIES: interior is updating, not launching activities scripts")
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_MUSIC_STUDIO_CCTV()
	IF IS_SKYSWOOP_MOVING()
	OR IS_SKYSWOOP_IN_SKY()
	OR IS_TRANSITION_ACTIVE()
	OR IS_TRANSITION_RUNNING()
	OR (g_clubMusicData.eActiveDJ != CLUB_DJ_NULL AND g_clubMusicData.eNextDJ != CLUB_DJ_NULL ) //DJ active
		IF m_PlayerBD[NATIVE_TO_INT(PLAYER_ID())].MPCCTVClient.eStage = MP_CCTV_CLIENT_STAGE_ACTIVATED
			IF (g_clubMusicData.eActiveDJ != CLUB_DJ_NULL AND g_clubMusicData.eNextDJ != CLUB_DJ_NULL )
				CLEANUP_CCTV(TRUE)
			ELSE
				CLEANUP_CCTV(FALSE)
			ENDIF
		ENDIF
	ELSE
		IF HAS_LOCAL_PLAYER_COMPLETED_FIXER_STORY_AS_LEADER()
		#IF IS_DEBUG_BUILD
		OR debugData.bBypassCCTVCheck = TRUE
		#ENDIF
			CLIENT_MAINTAIN_MP_CCTV(m_PlayerBD[NATIVE_TO_INT(PLAYER_ID())].MPCCTVClient, m_InteriorData.MPCCTVLocal, m_InteriorData.eSimpleInteriorID)
			
			IF g_SimpleInteriorData.bForceCCTVCleanup
				CLEANUP_CCTV(FALSE)
				
				g_SimpleInteriorData.bForceCCTVCleanup = FALSE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_SEATING_ACTIVITY_LAUNCHING()
	IF NOT IS_BIT_SET(m_InteriorData.iBS, BS_MUSIC_STUDIO_SEATING_LAUNCHED)
	AND IS_PLAYER_IN_MUSIC_STUDIO(PLAYER_ID())
	AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY("music_studio_seating")) = 0
	AND NOT NETWORK_IS_SCRIPT_ACTIVE("music_studio_seating", DEFAULT, TRUE)
	AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
	AND NOT _SIMPLE_INTERIOR_IS_PLAYER_SWITCHING_BETWEEN_INTERIORS()
	AND NOT HAS_PLAYER_ACCEPTED_SIMPLE_INTERIOR_INVITE()
	AND NOT IS_PLAYER_AUTOWARP_BETWEEN_TWO_INTERIORS()
		REQUEST_SCRIPT("music_studio_seating")
		
		IF HAS_SCRIPT_LOADED("music_studio_seating")
			START_NEW_SCRIPT("music_studio_seating", MULTIPLAYER_MISSION_STACK_SIZE)
			
			SET_SCRIPT_AS_NO_LONGER_NEEDED("music_studio_seating")
			
			SET_BIT(m_InteriorData.iBS, BS_MUSIC_STUDIO_SEATING_LAUNCHED)
		ENDIF
	ELIF IS_BIT_SET(m_InteriorData.iBS, BS_MUSIC_STUDIO_SEATING_LAUNCHED)
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY("music_studio_seating")) = 0
		AND NOT NETWORK_IS_SCRIPT_ACTIVE("music_studio_seating", DEFAULT, TRUE)
			CLEAR_BIT(m_InteriorData.iBS, BS_MUSIC_STUDIO_SEATING_LAUNCHED)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Making ped capsule size smaller as there are tight gaps between coffee tables and sofas in the property
PROC MAINTAIN_PED_CAPSULE()
	IF IS_PLAYER_IN_MUSIC_STUDIO(PLAYER_ID())
	AND NOT NETWORK_IS_ACTIVITY_SESSION()
	AND NOT IS_SCREEN_FADED_OUT()
	AND NOT IS_TRANSITION_ACTIVE()
	AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
		IF IS_LOCAL_PLAYER_IN_MUSIC_STUDIO_SMOKING_ROOM()	
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-1006.899353,-78.533569,-99.003067>>, <<0.500000,1.600000,1.000000>>)
			OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-1003.075562,-77.770721,-99.003067>>, <<0.500000,0.900000,1.000000>>)		
				SET_PED_CAPSULE(PLAYER_PED_ID(), 0.2)
			ENDIF
		ELSE // Long sofa in the main recording room
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1003.307617,-66.393356,-100.003075>>, <<-1001.402344,-63.135056,-98.003075>>, 0.800000)
				SET_PED_CAPSULE(PLAYER_PED_ID(), 0.2)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_SMOKING_ACTIVITY_LAUNCHING()
	IF NOT IS_BIT_SET(m_InteriorData.iBS, BS_MUSIC_STUDIO_SMOKING_ACTIVITY_STARTED)
	AND NOT NETWORK_IS_ACTIVITY_SESSION()
	AND NOT IS_SCREEN_FADED_OUT()
	AND NOT IS_TRANSITION_ACTIVE()
	AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
		IF IS_LOCAL_PLAYER_IN_MUSIC_STUDIO_SMOKING_ROOM()		
		AND NOT NETWORK_IS_SCRIPT_ACTIVE("music_studio_smoking", GET_SIMPLE_INTERIOR_INT_SCRIPT_INSTANCE(), TRUE)
		AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY("music_studio_smoking")) <= 0
			REQUEST_SCRIPT("music_studio_smoking")
			
			IF HAS_SCRIPT_LOADED("music_studio_smoking")
				START_NEW_SCRIPT("music_studio_smoking", DEFAULT_STACK_SIZE)
				
				SET_SCRIPT_AS_NO_LONGER_NEEDED("music_studio_smoking")
				
				SET_BIT(m_InteriorData.iBS, BS_MUSIC_STUDIO_SMOKING_ACTIVITY_STARTED)
			ENDIF
		ENDIF		
	ELSE
		IF NOT NETWORK_IS_SCRIPT_ACTIVE("music_studio_smoking", GET_SIMPLE_INTERIOR_INT_SCRIPT_INSTANCE(), TRUE)
		AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY("music_studio_smoking")) <= 0
			CLEAR_BIT(m_InteriorData.iBS, BS_MUSIC_STUDIO_SMOKING_ACTIVITY_STARTED)
		ENDIF
	ENDIF		
ENDPROC

PROC MAINTAIN_MUSIC_STUDIO_SHOP_SCRIPT_INSTANCES()
	IF g_iClothesShopInstance = -1
		g_iClothesShopInstance = m_InteriorData.iScriptInstance
		PRINTLN("MAINTAIN_MUSIC_STUDIO_SHOP_SCRIPT_INSTANCES - Updating g_iClothesShopInstance from: -1 to: ", g_iClothesShopInstance)
	ENDIF
ENDPROC

PROC CREATE_MUSIC_STUDIO_ENTITIES()
	BOOL bReady = TRUE
	
	IF bReady
		IF IS_FIXER_EVENT_REWARD_ACTIVE(FIXER_EVENT_REWARD_VISIT_MUSIC_HUB)
		AND NOT DOES_LOCAL_PLAYER_OWN_FIXER_EVENT_REWARD(FIXER_EVENT_REWARD_VISIT_MUSIC_HUB)
			GIVE_LOCAL_PLAYER_FIXER_EVENT_REWARD(FIXER_EVENT_REWARD_VISIT_MUSIC_HUB)
		ENDIF
		
		IF SHOULD_WATCH_MOCAP_SCENE_WEED_ROOM_INTRO()
			MUSIC_STUDIO_START_MOCAP(MUSIC_STUDIO_MOCAP_CUTSCENE_WEED_ROOM_INTRO)
		ELIF SHOULD_WATCH_MOCAP_SCENE_WEED_ROOM_ENTER()
			MUSIC_STUDIO_START_MOCAP(MUSIC_STUDIO_MOCAP_CUTSCENE_WEED_ROOM_ENTER)
		ELSE
			SET_MUSIC_STUDIO_STATE(MUSIC_STUDIO_STATE_IDLE)	
			SET_SIMPLE_INTERIOR_CHILD_SCRIPT_READY(TRUE)
			SET_SIMPLE_INTERIOR_CHILD_SCRIPT_ALLOWS_FOR_WARP(TRUE)
			
			IF IS_BIT_SET(g_SimpleInteriorData.iEleventhBS, BS11_SIMPLE_INTERIOR_BLOCK_MUSIC_STUDIO_WEED_ROOM_MOCAP)
				CLEAR_BIT(g_SimpleInteriorData.iEleventhBS, BS11_SIMPLE_INTERIOR_BLOCK_MUSIC_STUDIO_WEED_ROOM_MOCAP)
			ENDIF
		ENDIF
	ENDIF
ENDPROC


PROC GET_MUSIC_STUDIO_DOORS_DATA(INT i, DOOR_DATA_STRUCT &sData)
	SWITCH i
		CASE 0
			sData.model 	= INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_sf_door_rec_01a"))
			sData.coords 	= << -1010.7510, -85.6181, -99.2534 >>
			sData.doorHash  = GET_UNIQUE_DOOR_HASH(sData.model, sData.coords)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "MUSIC_STUDIO_00"
			#ENDIF
		BREAK
		CASE 1
			sData.model 	= INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_sf_door_rec_01a"))
			sData.coords 	= << -1012.8726, -64.5344, -98.8532 >>
			sData.doorHash  = GET_UNIQUE_DOOR_HASH(sData.model, sData.coords)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "MUSIC_STUDIO_02"
			#ENDIF
		BREAK
		CASE 2
			sData.model 	= INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_sf_door_rec_01a"))
			sData.coords 	= << -1012.8726, -49.1813, -98.8532 >>
			sData.doorHash  = GET_UNIQUE_DOOR_HASH(sData.model, sData.coords)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "MUSIC_STUDIO_03"
			#ENDIF
		BREAK
		CASE 3
			sData.model 	= INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_sf_door_rec_01a"))
			sData.coords 	= << -1008.4345, -49.1813, -98.8532 >>
			sData.doorHash  = GET_UNIQUE_DOOR_HASH(sData.model, sData.coords)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "MUSIC_STUDIO_03"
			#ENDIF
		BREAK
		
		CASE 4
			sData.model 	= INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_sf_door_rec_01a"))
			sData.coords 	= << -1002.9728, -54.7916, -98.8501 >>
			sData.doorHash  = GET_UNIQUE_DOOR_HASH(sData.model, sData.coords)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "MUSIC_STUDIO_04"
			#ENDIF
		BREAK
		//BEHIND DRUMS
		CASE 5
			sData.model 	= INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_sf_door_rec_01a"))
			sData.coords 	= << -989.6590, -57.2871, -98.8534 >>
			sData.doorHash  = GET_UNIQUE_DOOR_HASH(sData.model, sData.coords)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "MUSIC_STUDIO_05"
			#ENDIF
		BREAK
		//GUITAR WALL BACK
		CASE 6
			sData.model 	= INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_sf_door_rec_01a"))
			sData.coords 	= << -986.5245, -81.8240, -98.8534 >>
			sData.doorHash  = GET_UNIQUE_DOOR_HASH(sData.model, sData.coords)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "MUSIC_STUDIO_05"
			#ENDIF
		BREAK
		//WEED ROOM
		CASE 7
			sData.model 	= INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_sf_door_stud_01a"))
			sData.coords 	= << -1005.6094, -74.8248, -98.8501 >>
			sData.doorHash  = GET_UNIQUE_DOOR_HASH(sData.model, sData.coords)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "MUSIC_STUDIO_06"
			#ENDIF
		BREAK
		//MIXING STUDIO 1 LEFT
		CASE 8
			sData.model 	= INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_sf_door_com_l_06a"))
			sData.coords 	= << -1003.1869, -71.0579, -98.8532 >>
			sData.doorHash  = GET_UNIQUE_DOOR_HASH(sData.model, sData.coords)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "MUSIC_STUDIO_07"
			#ENDIF
		BREAK
		//MIXING STUDIO 1 RIGHT
		CASE 9
			sData.model 	= INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_sf_door_com_r_06a"))
			sData.coords 	= << -1005.6022, -70.1124, -98.8532 >>
			sData.doorHash  = GET_UNIQUE_DOOR_HASH(sData.model, sData.coords)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "MUSIC_STUDIO_08"
			#ENDIF
		BREAK
		//MIXING STUDIO 2 LEFT
		CASE 10
			sData.model 	= INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_sf_door_com_l_06a"))
			sData.coords 	= << -1003.1602, -59.0497, -98.8532 >>
			sData.doorHash  = GET_UNIQUE_DOOR_HASH(sData.model, sData.coords)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "MUSIC_STUDIO_09"
			#ENDIF
		BREAK
		//MIXING STUDIO 2 RIGHT
		CASE 11
			sData.model 	= INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_sf_door_com_r_06a"))
			sData.coords 	= << -1000.6660, -58.3377, -98.8532 >>
			sData.doorHash  = GET_UNIQUE_DOOR_HASH(sData.model, sData.coords)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "MUSIC_STUDIO_10"
			#ENDIF
		BREAK
		CASE 12 // DRUMS RECORDING BOOTH
			sData.model 	= INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_sf_door_rec_01a"))
			sData.coords 	= << -1001.8008, -73.0367, -98.8532 >>
			sData.doorHash  = GET_UNIQUE_DOOR_HASH(sData.model, sData.coords)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "MUSIC_STUDIO_11"
			#ENDIF
		BREAK
		CASE 13 // DRUMS RECORDING BOOTH	2
			sData.model 	= INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_sf_door_rec_01a"))
			sData.coords 	= << -995.8492, -79.6876, -98.8532 >>
			sData.doorHash  = GET_UNIQUE_DOOR_HASH(sData.model, sData.coords)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "MUSIC_STUDIO_12"
			#ENDIF
		BREAK
		CASE 14 // to allow audio bleed to the other room
			sData.model 	= INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_sf_door_rec_01a"))
			sData.coords 	= << -997.4695, -56.0643, -98.8532 >>
			sData.doorHash  = GET_UNIQUE_DOOR_HASH(sData.model, sData.coords)
			#IF IS_DEBUG_BUILD
				sData.dbg_name = "MUSIC_STUDIO_12"
			#ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC ADD_MUSIC_STUDIO_DOOR_TO_SYSTEM_AND_LOCK(INT iDoorIndex, BOOL bLocked, BOOL bLockOpen = TRUE, BOOL bDoorsOnLeft = FALSE)
	DOOR_DATA_STRUCT sData
	GET_MUSIC_STUDIO_DOORS_DATA(iDoorIndex, sData)
	
	ADD_DOOR_TO_SYSTEM_AND_LOCK(sData.doorHash, sData.coords, sData.model, bLocked, bLockOpen, bDoorsOnLeft)
ENDPROC

PROC MAINTAIN_MUSIC_STUDIO_DOOR_LOCKS() //TO DO , CREATE DESCRIPTIVE ENUMS
	IF IS_BIT_SET(m_InteriorData.iBS, BS_MUSIC_STUDIO_INTERIOR_REFRESH_ON_INIT)
	AND NOT IS_BIT_SET(m_InteriorData.iBS, BS_MUSIC_STUDIO_DOORS_ADDED_TO_SYSTEM)
		
		//ADD_MUSIC_STUDIO_DOOR_TO_SYSTEM_AND_LOCK(0, TRUE, FALSE)	//unlocked for patroling peds
		ADD_MUSIC_STUDIO_DOOR_TO_SYSTEM_AND_LOCK(1, TRUE, FALSE)
		//ADD_MUSIC_STUDIO_DOOR_TO_SYSTEM_AND_LOCK(2, TRUE, FALSE)	//unlocked for patroling peds
		//ADD_MUSIC_STUDIO_DOOR_TO_SYSTEM_AND_LOCK(3, TRUE, FALSE)  //unlocked for patroling peds
		ADD_MUSIC_STUDIO_DOOR_TO_SYSTEM_AND_LOCK(4, TRUE, FALSE)
		ADD_MUSIC_STUDIO_DOOR_TO_SYSTEM_AND_LOCK(5, TRUE, FALSE)
		ADD_MUSIC_STUDIO_DOOR_TO_SYSTEM_AND_LOCK(6, TRUE, FALSE)
		ADD_MUSIC_STUDIO_DOOR_TO_SYSTEM_AND_LOCK(7, TRUE, FALSE)
		//MIXING ROOM DOORS (ALL OPEN WIDE)
		ADD_MUSIC_STUDIO_DOOR_TO_SYSTEM_AND_LOCK(8, TRUE, TRUE)
		ADD_MUSIC_STUDIO_DOOR_TO_SYSTEM_AND_LOCK(9, TRUE, TRUE, TRUE)
		ADD_MUSIC_STUDIO_DOOR_TO_SYSTEM_AND_LOCK(10, TRUE, TRUE)
		ADD_MUSIC_STUDIO_DOOR_TO_SYSTEM_AND_LOCK(11, TRUE, TRUE, TRUE)
		
		ADD_MUSIC_STUDIO_DOOR_TO_SYSTEM_AND_LOCK(14, TRUE, TRUE, TRUE)
		
		PRINTLN("AM_MP_MUSIC_STUDIO - MAINTAIN_MUSIC_STUDIO_DOOR_LOCKS - Setup and locked doors")
		SET_BIT(m_InteriorData.iBS, BS_MUSIC_STUDIO_DOORS_ADDED_TO_SYSTEM)	
		EXIT
	ENDIF
	
	
	
	IF NOT IS_BIT_SET(m_InteriorData.iBS, BS_MUSIC_STUDIO_DOORS_LOCKED_FOR_DRUMS_RECORDING)
		IF g_clubMusicData.eActiveDJ = CLUB_DJ_DR_DRE_MUSICIAN_DRUMS
		AND g_clubMusicData.iCurrentTrackTimeMS <= 145000
			ADD_MUSIC_STUDIO_DOOR_TO_SYSTEM_AND_LOCK(12, TRUE, FALSE)
			ADD_MUSIC_STUDIO_DOOR_TO_SYSTEM_AND_LOCK(13, TRUE, FALSE)
			SET_BIT(m_InteriorData.iBS, BS_MUSIC_STUDIO_DOORS_LOCKED_FOR_DRUMS_RECORDING)
		ENDIF	
	ENDIF
	
	IF IS_BIT_SET(m_InteriorData.iBS, BS_MUSIC_STUDIO_DOORS_LOCKED_FOR_DRUMS_RECORDING)
		IF g_clubMusicData.eActiveDJ != CLUB_DJ_DR_DRE_MUSICIAN_DRUMS
		OR (g_clubMusicData.eActiveDJ = CLUB_DJ_DR_DRE_MUSICIAN_DRUMS AND g_clubMusicData.iCurrentTrackTimeMS > 145000)
			ADD_MUSIC_STUDIO_DOOR_TO_SYSTEM_AND_LOCK(12, FALSE, FALSE)
			ADD_MUSIC_STUDIO_DOOR_TO_SYSTEM_AND_LOCK(13, FALSE, FALSE)
			CLEAR_BIT(m_InteriorData.iBS, BS_MUSIC_STUDIO_DOORS_LOCKED_FOR_DRUMS_RECORDING)
		ENDIF		
	ENDIF
ENDPROC

PROC MAINTAIN_MUSIC_STUDIO_INTERIOR_BLIPS()
	IF NOT IS_PLAYER_IN_ZOOMED_OUT_RADAR_OR_MAP(PLAYER_ID())
		IF NOT IS_LOCAL_PLAYER_IN_MUSIC_STUDIO_SMOKING_ROOM()
			IF DOES_PLAYER_HAVE_ACCESS_TO_MUSIC_STUDIO_SMOKING_ROOM()
				ADD_BASIC_BLIP(m_InteriorData.blipSmokingRoom, <<-1005.0000, -74.8248, -98.8501>>, RADAR_TRACE_PICKUP_WEED, "MUS_STIO_SMR_BL", DEFAULT, TRUE)
				
				IF DOES_BLIP_EXIST(m_InteriorData.blipSmokingRoom)
					SET_BLIP_COLOUR_FROM_HUD_COLOUR(m_InteriorData.blipSmokingRoom, HUD_COLOUR_GREENLIGHT)
				ENDIF
			ELSE
				SAFE_REMOVE_BLIP(m_InteriorData.blipSmokingRoom)
			ENDIF
			
			SAFE_REMOVE_BLIP(m_InteriorData.blipSmokingRoomCouch)
			
			ADD_BASIC_BLIP(m_InteriorData.blipClothing, <<-1014.16, -89.53, -100.40>>, GET_SHOP_BLIP_SPRITE(CLOTHES_SHOP_MUSIC_STUDIO), GET_SHOP_NAME(CLOTHES_SHOP_MUSIC_STUDIO), DEFAULT, TRUE)
		ELSE
			IF NOT IS_PLAYER_IN_FIXER_SPECIAL_STRAIN_CHAIR(PLAYER_ID())
				ADD_BASIC_BLIP(m_InteriorData.blipSmokingRoomCouch, <<-1007.7170, -78.5658, -99.9729>>, RADAR_TRACE_PICKUP_WEED, "MUS_STIO_SMK_BL", DEFAULT, TRUE)
				
				IF DOES_BLIP_EXIST(m_InteriorData.blipSmokingRoomCouch)
					SET_BLIP_COLOUR_FROM_HUD_COLOUR(m_InteriorData.blipSmokingRoomCouch, HUD_COLOUR_GREENLIGHT)
				ENDIF
			ELSE
				SAFE_REMOVE_BLIP(m_InteriorData.blipSmokingRoomCouch)
			ENDIF
			
			SAFE_REMOVE_BLIP(m_InteriorData.blipSmokingRoom)
			SAFE_REMOVE_BLIP(m_InteriorData.blipClothing)
		ENDIF
	ELSE
		CLEANUP_MUSIC_STUDIO_INTERIOR_BLIPS()
	ENDIF
ENDPROC

FUNC BOOL SHOULD_HIDE_MIXER_PROPS()
	IF NOT IS_BIT_SET(m_InteriorData.iBS, BS_MUSIC_STUDIO_SET_MODEL_HIDES_FOR_DRE_PERFORMANCE)
	
		IF g_clubMusicData.eActiveDJ != CLUB_DJ_DR_DRE_ABSENT
		AND NOT g_clubMusicData.bHasDJSwitchOverSceneStarted
			PRINTLN("[AM_MP_MUSIC_STUDIO][SHOULD_HIDE_MIXER_PROPS] HIDING MIXER PROPS")
			RETURN TRUE
		ENDIF
	ENDIF

	
	RETURN FALSE	
ENDFUNC

FUNC BOOL SHOULD_SHOW_MIXER_PROPS()
	IF IS_BIT_SET(m_InteriorData.iBS, BS_MUSIC_STUDIO_SET_MODEL_HIDES_FOR_DRE_PERFORMANCE)
	
		IF g_clubMusicData.eNextDJ != CLUB_DJ_NULL
		AND g_clubMusicData.eActiveDJ != g_clubMusicData.eNextDJ
			PRINTLN("[AM_MP_MUSIC_STUDIO][SHOULD_HIDE_MIXER_PROPS] SHOWING MIXER PROPS g_clubMusicData.eActiveDJ != g_clubMusicData.eNextDJ")
			RETURN TRUE
		ENDIF
	
		IF g_clubMusicData.bHasDJSwitchOverSceneStarted
		OR IS_CUTSCENE_PLAYING()
			PRINTLN("[AM_MP_MUSIC_STUDIO][SHOULD_HIDE_MIXER_PROPS] SHOWING MIXER PROPS  g_clubMusicData.bHasDJSwitchOverSceneStarted OR IS_CUTSCENE_PLAYING()")
			RETURN TRUE
		ENDIF
		
		IF g_clubMusicData.eActiveDJ = CLUB_DJ_DR_DRE_ABSENT
		OR g_clubMusicData.eNextDJ = CLUB_DJ_DR_DRE_ABSENT
			PRINTLN("[AM_MP_MUSIC_STUDIO][SHOULD_HIDE_MIXER_PROPS] SHOWING MIXER PROPS  g_clubMusicData.eActiveDJ = CLUB_DJ_DR_DRE_ABSENT OR g_clubMusicData.eNextDJ = CLUB_DJ_DR_DRE_ABSENT")
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE	
ENDFUNC

FUNC BOOL SHOULD_HIDE_DRUM_PROPS()
	IF NOT IS_BIT_SET(m_InteriorData.iBS, BS_MUSIC_STUDIO_SET_MODEL_HIDES_FOR_DRUMS)
		
			
		IF g_clubMusicData.eActiveDJ = CLUB_DJ_DR_DRE_MUSICIAN_DRUMS 
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE	
ENDFUNC

FUNC BOOL SHOULD_SHOW_DRUM_PROPS()
	IF IS_BIT_SET(m_InteriorData.iBS, BS_MUSIC_STUDIO_SET_MODEL_HIDES_FOR_DRUMS)
	
		IF g_clubMusicData.bHasDJSwitchOverSceneStarted
		OR IS_CUTSCENE_PLAYING()
			RETURN TRUE
		ENDIF
		
		IF g_clubMusicData.eActiveDJ != CLUB_DJ_DR_DRE_MUSICIAN_DRUMS  
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE	
ENDFUNC

FUNC BOOL SHOULD_TURN_RECORDING_LIGHT_ON()
	IF NOT IS_BIT_SET(m_InteriorData.iBS, BS_MUSIC_STUDIO_SET_MODEL_OVERLAY_FOR_RECORDING_LIGHT)
		IF g_clubMusicData.eActiveDJ = CLUB_DJ_DR_DRE_MUSICIAN_DRUMS
		OR g_clubMusicData.eNextDJ = CLUB_DJ_DR_DRE_MUSICIAN_DRUMS
		OR g_clubMusicData.eActiveDJ = CLUB_DJ_DR_DRE_MUSICIAN_VOCALS
		OR g_clubMusicData.eNextDJ = CLUB_DJ_DR_DRE_MUSICIAN_VOCALS
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE	
ENDFUNC

FUNC BOOL SHOULD_TURN_RECORDING_LIGHT_OFF()
	IF IS_BIT_SET(m_InteriorData.iBS, BS_MUSIC_STUDIO_SET_MODEL_OVERLAY_FOR_RECORDING_LIGHT)
		IF g_clubMusicData.eActiveDJ = CLUB_DJ_DR_DRE_MIX_2
		OR g_clubMusicData.eNextDJ = CLUB_DJ_DR_DRE_MIX_2 
			RETURN TRUE
		ENDIF
		SET_BIT(m_InteriorData.iBS, BS_MUSIC_STUDIO_SET_MODEL_OVERLAY_FOR_RECORDING_LIGHT)
	ENDIF
	
	RETURN FALSE	
ENDFUNC

PROC GET_MUSIC_STUDIO_RECORDING_LIGHT_POSITION_AND_HEADING(INT i, VECTOR &vPosition, FLOAT &fHeading)
	SWITCH i
		CASE 0
			vPosition = <<-1004.5027, -70.8679, -97.5609>>
			fHeading = -21.38
		BREAK
		CASE 1
			vPosition = <<-1008.4358, -72.8284, -97.4533>>
			fHeading =  -90
		BREAK
		CASE 2
			vPosition = <<-1008.4358, -57.5145, -97.4533>>
			fHeading = -90
		BREAK
	ENDSWITCH
ENDPROC



PROC MAINTAIN_MUSIC_STUDIO_MODEL_HIDES_FOR_DRE_PERFORMANCE()
	
	IF SHOULD_HIDE_MIXER_PROPS()	
	//small mixer panels	
		//bottom left 
		CREATE_MODEL_HIDE_EXCLUDING_SCRIPT_OBJECTS(<<-995.6716, -68.6484, -99.1514>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_sf_s_mixer_02b")), TRUE)
		//bottom middle 
		CREATE_MODEL_HIDE_EXCLUDING_SCRIPT_OBJECTS(<<-995.6716, -68.6484, -99.1514>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_sf_s_mixer_01b")), TRUE)
		//bottom right 
		CREATE_MODEL_HIDE_EXCLUDING_SCRIPT_OBJECTS(<<-995.6716, -68.6484, -99.1514>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_sf_s_mixer_02b")), TRUE)
	//BIG mixer panels		
		//top left
		CREATE_MODEL_HIDE_EXCLUDING_SCRIPT_OBJECTS(<<-995.6716, -68.6484, -99.1514>>, 0.50, INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_sf_s_mixer_02a")), TRUE)
		//top middle
		CREATE_MODEL_HIDE_EXCLUDING_SCRIPT_OBJECTS(<<-995.6716, -68.6484, -99.1514>>, 0.5, INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_sf_s_mixer_01a")), TRUE)
		//top right
		//CREATE_MODEL_HIDE_EXCLUDING_SCRIPT_OBJECTS(<<-995.6716, -68.6484, -99.1514>>, 0.5, INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_sf_s_mixer_02a")), TRUE)
		SET_BIT(m_InteriorData.iBS, BS_MUSIC_STUDIO_SET_MODEL_HIDES_FOR_DRE_PERFORMANCE)
	ENDIF
	
	IF SHOULD_SHOW_MIXER_PROPS()
	//small mixer panels	
		//bottom left 
		REMOVE_MODEL_HIDE(<<-995.6716, -68.6484, -99.1514>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_sf_s_mixer_02b")))
		//bottom middle 
		REMOVE_MODEL_HIDE(<<-995.6716, -68.6484, -99.1514>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_sf_s_mixer_01b")))
		//bottom right 
		REMOVE_MODEL_HIDE(<<-995.6716, -68.6484, -99.1514>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_sf_s_mixer_02b")))
	//BIG mixer panels		
		//top left
		REMOVE_MODEL_HIDE(<<-995.6716, -68.6484, -99.1514>>, 0.5, INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_sf_s_mixer_02a")))
		//top middle
		REMOVE_MODEL_HIDE(<<-995.6716, -68.6484, -99.1514>>, 0.5, INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_sf_s_mixer_01a")))
		//top right
		//REMOVE_MODEL_HIDE(<<-995.6716, -68.6484, -99.1514>>, 0.5, INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_sf_s_mixer_02a")))
		
		CLEAR_BIT(m_InteriorData.iBS, BS_MUSIC_STUDIO_SET_MODEL_HIDES_FOR_DRE_PERFORMANCE)		
	ENDIF
	
	
	IF SHOULD_HIDE_DRUM_PROPS()
		//drum set
		CREATE_MODEL_HIDE_EXCLUDING_SCRIPT_OBJECTS(<<-999.3611, -74.1947, -100.0032>>, 0.5, INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_sf_drum_kit_01a")), TRUE)
		//stool 
		CREATE_MODEL_HIDE_EXCLUDING_SCRIPT_OBJECTS(<<-999.5626, -75.4731, -99.9861>>, 0.5, INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_sf_chair_stool_08a")), TRUE)
		SET_BIT(m_InteriorData.iBS, BS_MUSIC_STUDIO_SET_MODEL_HIDES_FOR_DRUMS)
	ENDIF
	
	IF SHOULD_SHOW_DRUM_PROPS()
		//top middle
		REMOVE_MODEL_HIDE(<<-999.3611, -74.1947, -100.0032>>, 0.5, INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_sf_drum_kit_01a")))
		//top right
		REMOVE_MODEL_HIDE(<<-999.5626, -75.4731, -99.9861>>, 0.5, INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_sf_chair_stool_08a")))
		CLEAR_BIT(m_InteriorData.iBS, BS_MUSIC_STUDIO_SET_MODEL_HIDES_FOR_DRUMS)		
	ENDIF
	
	IF SHOULD_TURN_RECORDING_LIGHT_ON()
		INT i
		VECTOR vPosition
		FLOAT fHead
		REPEAT ciMUSIC_STUDIO_WORK_NUM_RECORDING_LIGHTS i
			IF NOT DOES_ENTITY_EXIST(m_InteriorData.objRecordingLights[i])			
				GET_MUSIC_STUDIO_RECORDING_LIGHT_POSITION_AND_HEADING(i, vPosition, fHead)
				m_InteriorData.objRecordingLights[i] = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_sf_lightbox_rec_on_01a")),vPosition, FALSE, TRUE)
				FREEZE_ENTITY_POSITION(m_InteriorData.objRecordingLights[i], TRUE)
				SET_ENTITY_HEADING(m_InteriorData.objRecordingLights[i], fHead)
				SET_ENTITY_COMPLETELY_DISABLE_COLLISION(m_InteriorData.objRecordingLights[i], FALSE)
				SET_ENTITY_DYNAMIC(m_InteriorData.objRecordingLights[i], FALSE)
				
				
			ENDIF
		ENDREPEAT
		
		IF DOES_ENTITY_EXIST(m_InteriorData.objRecordingLights[0])
		AND DOES_ENTITY_EXIST(m_InteriorData.objRecordingLights[1])
		AND DOES_ENTITY_EXIST(m_InteriorData.objRecordingLights[2])
			SET_BIT(m_InteriorData.iBS, BS_MUSIC_STUDIO_SET_MODEL_OVERLAY_FOR_RECORDING_LIGHT)
		ENDIF
 	ENDIF
	
	IF SHOULD_TURN_RECORDING_LIGHT_OFF()
	
		CLEAN_UP_RECORDING_LIGHTS()	
		IF NOT DOES_ENTITY_EXIST(m_InteriorData.objRecordingLights[0])
		AND NOT DOES_ENTITY_EXIST(m_InteriorData.objRecordingLights[1])
		AND NOT DOES_ENTITY_EXIST(m_InteriorData.objRecordingLights[2])
			CLEAR_BIT(m_InteriorData.iBS, BS_MUSIC_STUDIO_SET_MODEL_OVERLAY_FOR_RECORDING_LIGHT)
		ENDIF	
 	ENDIF
ENDPROC

FUNC BOOL GET_MUSIC_STUDIO_EXIT_RECORDING_AREA_INTERIOR_SPAWN_POINT(INT iSpawnPoint, VECTOR &vSpawnPoint, FLOAT &fSpawnHeading, BOOL bDrums)
	IF bDrums
		SWITCH iSpawnPoint
			CASE 0	vSpawnPoint = <<-1002.6129, -73.6897, -100.0031>> fSpawnHeading = 71.8320	RETURN TRUE BREAK
			CASE 1	vSpawnPoint = <<-1003.4423, -73.8817, -100.0031>> fSpawnHeading = 70.1629	RETURN TRUE BREAK
			CASE 2	vSpawnPoint = <<-1003.5100, -72.8807, -100.0031>> fSpawnHeading = 46.0847	RETURN TRUE BREAK
			CASE 3	vSpawnPoint = <<-1004.7711, -73.7588, -100.0031>> fSpawnHeading = 4.8284	RETURN TRUE BREAK
			CASE 4	vSpawnPoint = <<-1005.5067, -72.5047, -100.0031>> fSpawnHeading = 324.5737	RETURN TRUE BREAK
		ENDSWITCH
	ELSE
		SWITCH iSpawnPoint
			CASE 0	vSpawnPoint = <<-1000.9576, -65.3346, -100.0031>> fSpawnHeading = 239.7200	RETURN TRUE BREAK
			CASE 1	vSpawnPoint = <<-1001.6131, -66.4573, -100.0031>> fSpawnHeading = 239.7200	RETURN TRUE BREAK
			CASE 2	vSpawnPoint = <<-1000.3021, -64.2120, -100.0031>> fSpawnHeading = 239.7200	RETURN TRUE BREAK
			CASE 3	vSpawnPoint = <<-1002.2686, -67.5799, -100.0031>> fSpawnHeading = 239.7200	RETURN TRUE BREAK
			CASE 4	vSpawnPoint = <<-999.6465, -63.0893, -100.0031>> fSpawnHeading = 239.7200	RETURN TRUE BREAK
			CASE 5	vSpawnPoint = <<-1002.9241, -68.7026, -100.0031>> fSpawnHeading = 239.7200	RETURN TRUE BREAK
			CASE 6	vSpawnPoint = <<-999.0195, -61.5433, -100.0031>> fSpawnHeading = 186.0632	RETURN TRUE BREAK
			CASE 7	vSpawnPoint = <<-1004.1292, -70.0368, -100.0031>> fSpawnHeading = 295.3446	RETURN TRUE BREAK
			CASE 8	vSpawnPoint = <<-999.8350, -65.9901, -100.0031>> fSpawnHeading = 239.7200	RETURN TRUE BREAK
			CASE 9	vSpawnPoint = <<-1000.4905, -67.1128, -100.0031>> fSpawnHeading = 239.7200	RETURN TRUE BREAK
			CASE 10	vSpawnPoint = <<-999.1794, -64.8675, -100.0031>> fSpawnHeading = 239.7200	RETURN TRUE BREAK
			CASE 11	vSpawnPoint = <<-1001.1460, -68.2354, -100.0031>> fSpawnHeading = 239.7200	RETURN TRUE BREAK
			CASE 12	vSpawnPoint = <<-998.5239, -63.7448, -100.0031>> fSpawnHeading = 239.7200	RETURN TRUE BREAK
			CASE 13	vSpawnPoint = <<-1001.8015, -69.3580, -100.0031>> fSpawnHeading = 239.7200	RETURN TRUE BREAK
			CASE 14	vSpawnPoint = <<-996.8605, -63.6880, -100.0031>> fSpawnHeading = 182.9253	RETURN TRUE BREAK
			CASE 15	vSpawnPoint = <<-1000.2376, -70.5309, -100.0031>> fSpawnHeading = 316.9180	RETURN TRUE BREAK
		ENDSWITCH
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL GET_MUSIC_STUDIO_EXIT_RESTRICTED_AREA_INTERIOR_SPAWN_POINT(INT iSpawnPoint, VECTOR &vSpawnPoint, FLOAT &fSpawnHeading)
	
	SWITCH iSpawnPoint
		CASE 0	vSpawnPoint = <<-1010.0571, -84.2280, -100.4031>> fSpawnHeading = 0.0		RETURN TRUE BREAK
		CASE 1	vSpawnPoint = <<-1012.1531, -49.7938, -100.4031>> fSpawnHeading = 270.5516	RETURN TRUE BREAK
		CASE 2	vSpawnPoint = <<-1009.1279, -49.7938, -100.4031>> fSpawnHeading = 90.9647	RETURN TRUE BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC SET_UP_CUSTOM_SPAWN_POINTS_FOR_PERFORMANCE(BOOL bDrums)
	IF NOT g_SpawnData.bUseCustomSpawnPoints
		
		VECTOR vSpawnPoint
		FLOAT fSpawnHeading, fWeight
		
		fWeight = 1.0		
		USE_CUSTOM_SPAWN_POINTS(TRUE, FALSE, TRUE, 0.01, 2, DEFAULT, DEFAULT, 0.01, DEFAULT, DEFAULT, TRUE, TRUE)
		
		SIMPLE_INTERIOR_DETAILS details
		GET_SIMPLE_INTERIOR_DETAILS(m_InteriorData.eSimpleInteriorID, details, TRUE)
				
		INT i
		WHILE GET_MUSIC_STUDIO_EXIT_RECORDING_AREA_INTERIOR_SPAWN_POINT(i, vSpawnPoint, fSpawnHeading, bDrums)			
			ADD_CUSTOM_SPAWN_POINT(vSpawnPoint, fSpawnHeading, fWeight)
			fWeight -= 0.025 
					
			i += 1
		ENDWHILE
		
	ENDIF
ENDPROC



PROC MAINTAIN_WARP_PLAYER_OUT_OF_RECORDING_BOOTH_DURING_DRUMS_PERFORMANCE()
	
	IF (g_clubMusicData.eActiveDJ = CLUB_DJ_DR_DRE_MUSICIAN_DRUMS
	AND g_clubMusicData.iCurrentTrackTimeMS <= 145000)
		IF NOT IS_BIT_SET(m_InteriorData.iBS, BS_MUSIC_STUDIO_WARPING_OUT_OF_RESTRICTED_AREA_DRUMS) //issue is they may move before warp is fully completed so need to keep call until spawn returns true
			IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<-998.723633,-79.388138,-101.002998>>, <<-999.595947,-73.867249,-97.721222>>, 5.000000)
			OR IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<-995.897156,-80.331123,-101.003006>>, <<-1000.539673,-80.278801,-97.582184>>, 5.000000)
			OR IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<-997.932983,-74.757713,-101.002998>>, <<-1001.240479,-73.197372,-97.753059>>, 3.000000)
			OR IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<-1001.172119,-72.218246,-101.003067>>, <<-1001.240479,-73.197372,-97.753059>>, 1.000000)
				SET_UP_CUSTOM_SPAWN_POINTS_FOR_PERFORMANCE(TRUE)
				SET_BIT(m_InteriorData.iBS, BS_MUSIC_STUDIO_WARPING_OUT_OF_RESTRICTED_AREA_DRUMS)
				PRINTLN("MAINTAIN_WARP_PLAYER_OUT_OF_RECORDING_BOOTH_DURING_DRUMS_PERFORMANCE: player in restricted area: Drums")
			ENDIF
		ELSE
			IF WARP_TO_SPAWN_LOCATION(SPAWN_LOCATION_CUSTOM_SPAWN_POINTS, FALSE, FALSE, FALSE, FALSE, DEFAULT, DEFAULT, FALSE)
				CLEAR_CUSTOM_SPAWN_POINTS()
				USE_CUSTOM_SPAWN_POINTS(FALSE)		
				CLEAR_BIT(m_InteriorData.iBS, BS_MUSIC_STUDIO_WARPING_OUT_OF_RESTRICTED_AREA_DRUMS)
				PRINTLN("MAINTAIN_WARP_PLAYER_OUT_OF_RECORDING_BOOTH_DURING_DRUMS_PERFORMANCE: finished warp out of drums area")
			ENDIF
			IF IS_BROWSER_OPEN()
				CLOSE_WEB_BROWSER()
			ENDIF
		ENDIF
	ENDIF
	
	IF g_clubMusicData.eActiveDJ != CLUB_DJ_DR_DRE_ABSENT
		IF NOT IS_BIT_SET(m_InteriorData.iBS, BS_MUSIC_STUDIO_WARPING_OUT_OF_RESTRICTED_AREA)
			IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<-997.267, -68.195, -100.0031>>, <<-998.210, -69.661, -98.0031>>, 3.300000)
			OR IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<-993.616638,-63.904812,-99.942978>>, <<-995.602966,-66.418938,-98.212379>>, 3.000000)
			OR IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<-992.131653,-66.016037,-99.930664>>, <<-994.957581,-67.216232,-98.129997>>, 2.000000)
			OR IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<-992.683960,-66.714386,-100.003098>>, <<-995.789917,-72.039368,-99.003090>>, 2.750000)
			OR IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<-996.198364,-72.835411,-99.954529>>, <<-996.466492,-69.854698,-98.149307>>, 2.750000)
			OR IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<-998.548340,-72.564537,-99.890884>>, <<-997.526550,-69.940369,-98.188576>>, 2.750000)
			OR IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<-995.422791,-67.121529,-100.253098>>, <<-997.652466,-68.562035,-97.753098>>, 2.250000)
				SET_UP_CUSTOM_SPAWN_POINTS_FOR_PERFORMANCE(FALSE)
				SET_BIT(m_InteriorData.iBS, BS_MUSIC_STUDIO_WARPING_OUT_OF_RESTRICTED_AREA)
				PRINTLN("MAINTAIN_WARP_PLAYER_OUT_OF_RECORDING_BOOTH_DURING_DRUMS_PERFORMANCE: player in restricted area")
			ENDIF
		ELSE
			IF WARP_TO_SPAWN_LOCATION(SPAWN_LOCATION_CUSTOM_SPAWN_POINTS, FALSE, FALSE, FALSE, FALSE, DEFAULT, DEFAULT, FALSE)
				CLEAR_CUSTOM_SPAWN_POINTS()
				USE_CUSTOM_SPAWN_POINTS(FALSE)	
				CLEAR_BIT(m_InteriorData.iBS, BS_MUSIC_STUDIO_WARPING_OUT_OF_RESTRICTED_AREA)
				PRINTLN("MAINTAIN_WARP_PLAYER_OUT_OF_RECORDING_BOOTH_DURING_DRUMS_PERFORMANCE: finished warp out of restricted area")
			ENDIF
			IF IS_BROWSER_OPEN()
				CLOSE_WEB_BROWSER()
			ENDIF
		ENDIF
	ENDIF
	
	IF g_clubMusicData.eActiveDJ = CLUB_DJ_DR_DRE_MUSICIAN_VOCALS
		IF NOT IS_BIT_SET(m_InteriorData.iBS, BS_MUSIC_STUDIO_WARPING_OUT_OF_RESTRICTED_AREA_VOCALS)
			IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<-997.499390,-69.300415,-100.253098>>, <<-1000.204285,-67.739120,-98.003075>>, 2)	
				SET_UP_CUSTOM_SPAWN_POINTS_FOR_PERFORMANCE(FALSE)
				SET_BIT(m_InteriorData.iBS, BS_MUSIC_STUDIO_WARPING_OUT_OF_RESTRICTED_AREA_VOCALS)
				PRINTLN("MAINTAIN_WARP_PLAYER_OUT_OF_RECORDING_BOOTH_DURING_DRUMS_PERFORMANCE: player in restricted area: vocals")
			ENDIF
		ELSE
			IF WARP_TO_SPAWN_LOCATION(SPAWN_LOCATION_CUSTOM_SPAWN_POINTS, FALSE, FALSE, FALSE, FALSE, DEFAULT, DEFAULT, FALSE)
				CLEAR_CUSTOM_SPAWN_POINTS()
				USE_CUSTOM_SPAWN_POINTS(FALSE)	
				CLEAR_BIT(m_InteriorData.iBS, BS_MUSIC_STUDIO_WARPING_OUT_OF_RESTRICTED_AREA_VOCALS)
				PRINTLN("MAINTAIN_WARP_PLAYER_OUT_OF_RECORDING_BOOTH_DURING_DRUMS_PERFORMANCE: finished warp out of restricted vocals area")
			ENDIF
			IF IS_BROWSER_OPEN()
				CLOSE_WEB_BROWSER()
			ENDIF
		ENDIF
		
	ENDIF
	
	IF NOT IS_BIT_SET(m_InteriorData.iBS, BS_MUSIC_STUDIO_WARPING_OUT_OF_RESTRICTED_AREA_LOBBY)
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1009.888733,-85.443008,-100.403122>>, <<-1009.788330,-94.426392,-97.564346>>, 3.500000)		
			USE_CUSTOM_SPAWN_POINTS(TRUE, FALSE, TRUE, 0.01, 2, DEFAULT, DEFAULT, 0.01, DEFAULT, DEFAULT, TRUE, TRUE)
			VECTOR vSpawnPoint
			FLOAT fSpawnHeading
			GET_MUSIC_STUDIO_EXIT_RESTRICTED_AREA_INTERIOR_SPAWN_POINT(0, vSpawnPoint, fSpawnHeading)
			ADD_CUSTOM_SPAWN_POINT(vSpawnPoint, fSpawnHeading, 1.0)
			SET_BIT(m_InteriorData.iBS, BS_MUSIC_STUDIO_WARPING_OUT_OF_RESTRICTED_AREA_LOBBY)
		ENDIF
	ELSE
		IF WARP_TO_SPAWN_LOCATION(SPAWN_LOCATION_CUSTOM_SPAWN_POINTS, FALSE, FALSE, FALSE, FALSE, DEFAULT, DEFAULT, FALSE)
			CLEAR_CUSTOM_SPAWN_POINTS()
			USE_CUSTOM_SPAWN_POINTS(FALSE)	
			
			CLEAR_BIT(m_InteriorData.iBS, BS_MUSIC_STUDIO_WARPING_OUT_OF_RESTRICTED_AREA_LOBBY)
		ENDIF
		IF IS_BROWSER_OPEN()
			CLOSE_WEB_BROWSER()
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(m_InteriorData.iBS, BS_MUSIC_STUDIO_WARPING_OUT_OF_RESTRICTED_AREA_HALLWAY_LEFT)
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1013.187805,-49.826374,-100.003075>>, <<-1023.534668,-49.965073,-97.503075>>, 3.500000)
			USE_CUSTOM_SPAWN_POINTS(TRUE, FALSE, TRUE, 0.01, 2, DEFAULT, DEFAULT, 0.01, DEFAULT, DEFAULT, TRUE, TRUE)
			VECTOR vSpawnPoint
			FLOAT fSpawnHeading
			GET_MUSIC_STUDIO_EXIT_RESTRICTED_AREA_INTERIOR_SPAWN_POINT(0, vSpawnPoint, fSpawnHeading)
			ADD_CUSTOM_SPAWN_POINT(vSpawnPoint, fSpawnHeading, 1.0)
			SET_BIT(m_InteriorData.iBS, BS_MUSIC_STUDIO_WARPING_OUT_OF_RESTRICTED_AREA_HALLWAY_LEFT)
		ENDIF
	ELSE
		IF WARP_TO_SPAWN_LOCATION(SPAWN_LOCATION_CUSTOM_SPAWN_POINTS, FALSE, FALSE, FALSE, FALSE, DEFAULT, DEFAULT, FALSE)
			CLEAR_CUSTOM_SPAWN_POINTS()
			USE_CUSTOM_SPAWN_POINTS(FALSE)	
			
			CLEAR_BIT(m_InteriorData.iBS, BS_MUSIC_STUDIO_WARPING_OUT_OF_RESTRICTED_AREA_HALLWAY_LEFT)
		ENDIF
		IF IS_BROWSER_OPEN()
			CLOSE_WEB_BROWSER()
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(m_InteriorData.iBS, BS_MUSIC_STUDIO_WARPING_OUT_OF_RESTRICTED_AREA_HALLWAY_RIGHT)
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1008.102173,-49.809204,-100.003075>>, <<-1002.000977,-49.796852,-97.464470>>, 3.500000)
			USE_CUSTOM_SPAWN_POINTS(TRUE, FALSE, TRUE, 0.01, 2, DEFAULT, DEFAULT, 0.01, DEFAULT, DEFAULT, TRUE, TRUE)
			VECTOR vSpawnPoint
			FLOAT fSpawnHeading
			GET_MUSIC_STUDIO_EXIT_RESTRICTED_AREA_INTERIOR_SPAWN_POINT(0, vSpawnPoint, fSpawnHeading)
			ADD_CUSTOM_SPAWN_POINT(vSpawnPoint, fSpawnHeading, 1.0)
			SET_BIT(m_InteriorData.iBS, BS_MUSIC_STUDIO_WARPING_OUT_OF_RESTRICTED_AREA_HALLWAY_RIGHT)
		ENDIF
	ELSE
		IF WARP_TO_SPAWN_LOCATION(SPAWN_LOCATION_CUSTOM_SPAWN_POINTS, FALSE, FALSE, FALSE, FALSE, DEFAULT, DEFAULT, FALSE)
			CLEAR_CUSTOM_SPAWN_POINTS()
			USE_CUSTOM_SPAWN_POINTS(FALSE)				
			CLEAR_BIT(m_InteriorData.iBS, BS_MUSIC_STUDIO_WARPING_OUT_OF_RESTRICTED_AREA_HALLWAY_RIGHT)

		ENDIF
		IF IS_BROWSER_OPEN()
			CLOSE_WEB_BROWSER()
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Temporarily removes collision between the player and the smoking room door upon entry to the studio from the smoking room
PROC MAINTAIN_FIX_FOR_DOOR_ON_ENTRY()
	IF IS_BIT_SET(m_InteriorData.iBS, BS_MUSIC_STUDIO_ENTERED_INTERIOR_FROM_SMOKING_ROOM)
		IF DOES_ENTITY_EXIST(m_InteriorData.objSmokingRoomDoor)
		AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
			IF NOT IS_BIT_SET(m_InteriorData.iBS, BS_MUSIC_STUDIO_TOGGLED_SMOKING_ROOM_DOOR_COLLISION)
				PRINTLN("AM_MP_MUSIC_STUDIO - MAINTAIN_FIX_FOR_DOOR_ON_ENTRY turning off collision between player and smoking room door")
				SET_ENTITY_NO_COLLISION_ENTITY(PLAYER_PED_ID(), m_InteriorData.objSmokingRoomDoor, TRUE)
				SET_BIT(m_InteriorData.iBS, BS_MUSIC_STUDIO_TOGGLED_SMOKING_ROOM_DOOR_COLLISION)
			ENDIF
		ELSE
			m_InteriorData.objSmokingRoomDoor = GET_CLOSEST_OBJECT_OF_TYPE(<<-1005.6094, -74.8248, -98.8501>>, 1.0, INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_sf_door_stud_01a")), FALSE, FALSE, FALSE)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_REMOVE_ENTOURAGE_COLLIDER()
	IF g_clubMusicData.eActiveDJ = CLUB_DJ_DR_DRE_ABSENT
	AND g_iPedLayout != 2
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC VECTOR GET_COLLIDER_POSITION(MUSIC_STUDIO_SPAWNED_OBJECTS eObject)
	SWITCH eObject
		CASE MUSIC_STUDIO_INVISIBLE_PED_COLLISION_ENTOURAGE
			IF g_clubMusicData.eActiveDJ != CLUB_DJ_DR_DRE_ABSENT
				SWITCH g_iPedLayout
					CASE 0
						RETURN <<-1001.342, -62.677, -100.0031>>
					BREAK
					CASE 1
						RETURN <<-1002.8923, -67.2454, -100.0031>>
					BREAK
					CASE 2
						RETURN <<-1002.8923, -67.2454, -100.0031>>
					BREAK
				ENDSWITCH	
			ELSE
				SWITCH g_iPedLayout
					CASE 2
						RETURN <<-1002.8923, -67.2454, -100.0031>>
					BREAK
				ENDSWITCH	
			ENDIF
		BREAK
		CASE MUSIC_STUDIO_INVISIBLE_PED_COLLISION_FRANKLIN
			RETURN <<-1003.7432, -80.0108, -99.9675>>
		BREAK
		CASE MUSIC_STUDIO_INVISIBLE_PED_COLLISION_LAMAR
			RETURN <<-1003.0919, -78.5783, -100.0031>>
		BREAK
		CASE MUSIC_STUDIO_INVISIBLE_PED_COLLISION_DR_DRE1
			RETURN <<-999.079, -68.402, -99.582>>
		BREAK	
		CASE MUSIC_STUDIO_INVISIBLE_PED_COLLISION_DR_DRE2
			RETURN <<-996.554, -67.130, -99.474>>
		BREAK	
		CASE MUSIC_STUDIO_INVISIBLE_PED_COLLISION_DR_DRE_LEFT
			RETURN <<-995.0781, -64.4144, -100.0032>>
		BREAK
		CASE MUSIC_STUDIO_INVISIBLE_PED_COLLISION_DR_DRE_RIGHT
			RETURN <<-999.3182, -71.1841, -100.0032>>
		BREAK
		CASE MUSIC_STUDIO_INVISIBLE_PED_COLLISION_PATROL_1
			RETURN <<-1009.210, -85.847, -99.573>>
		BREAK	
		CASE MUSIC_STUDIO_INVISIBLE_PED_COLLISION_PATROL_2
			RETURN <<-1013.332, -50.725, -99.103>>
		BREAK
		CASE MUSIC_STUDIO_INVISIBLE_PED_COLLISION_PATROL_3
			RETURN  <<-1007.928, -50.725,-99.103>> 
		BREAK	
		CASE MUSIC_STUDIO_INVISIBLE_PED_COLLISION_VOCALS1
			RETURN <<-998.8884, -68.4930, -99.0031>> 
		BREAK
	ENDSWITCH
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

PROC MAINTAIN_DRE_CHAIR_PROP()

	IF NOT IS_BIT_SET(m_InteriorData.iBS, BS_MUSIC_STUDIO_DRE_CHAIR_SPAWNED)
	AND g_clubMusicData.eActiveDJ = CLUB_DJ_DR_DRE_ABSENT
		IF NOT DOES_ENTITY_EXIST(m_InteriorData.objDreChair)
			IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, (GET_HASH_KEY("hei_Prop_Heist_Off_Chair"))))
				m_InteriorData.objDreChair = CREATE_OBJECT(INT_TO_ENUM(MODEL_NAMES, (GET_HASH_KEY("hei_Prop_Heist_Off_Chair"))),<<-996.4230, -68.3292, -99.9830>> , FALSE, FALSE)			
				FREEZE_ENTITY_POSITION(m_InteriorData.objDreChair, TRUE)
				SET_ENTITY_CAN_BE_DAMAGED(m_InteriorData.objDreChair, FALSE)
				SET_ENTITY_PROOFS(m_InteriorData.objDreChair, TRUE, TRUE, TRUE, FALSE, TRUE, TRUE, TRUE, TRUE)
				SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, (GET_HASH_KEY("hei_Prop_Heist_Off_Chair"))))
				PRINTLN("[AM_MP_MUSIC_STUDIO][MAINTAIN_DRE_CHAIR_PROP] creating hei_Prop_Heist_Off_Chair")		
				SET_BIT(m_InteriorData.iBS, BS_MUSIC_STUDIO_DRE_CHAIR_SPAWNED)
			ELSE
				PRINTLN("[AM_MP_MUSIC_STUDIO][MAINTAIN_DRE_CHAIR_PROP] hei_Prop_Heist_Off_Chair model not loaded yet")
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(m_InteriorData.iBS, BS_MUSIC_STUDIO_DRE_CHAIR_SPAWNED)
	AND (g_clubMusicData.eActiveDJ != CLUB_DJ_DR_DRE_ABSENT
	OR g_clubMusicData.bHasDJSwitchOverSceneStarted)
		IF DOES_ENTITY_EXIST(m_InteriorData.objDreChair)
			SAFE_DELETE_OBJECT(m_InteriorData.objDreChair)
			CLEAR_BIT(m_InteriorData.iBS, BS_MUSIC_STUDIO_DRE_CHAIR_SPAWNED)
		ENDIF
	ENDIF
	
ENDPROC

PROC MAINTAIN_SPAWNING_OBJECTS()
	#IF IS_DEBUG_BUILD
	VECTOR vCreateLoc
	#ENDIF
	IF NOT IS_BIT_SET(m_InteriorData.iBS, BS_MUSIC_STUDIO_ENTOURAGE_GIRL_COLLIDER_CREATED)
		IF NOT SHOULD_REMOVE_ENTOURAGE_COLLIDER()
		AND NOT DOES_ENTITY_EXIST(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_ENTOURAGE])
			IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, (GET_HASH_KEY("prop_bin_01a"))))
				m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_ENTOURAGE] = CREATE_OBJECT(INT_TO_ENUM(MODEL_NAMES, (GET_HASH_KEY("prop_bin_01a"))),GET_COLLIDER_POSITION(MUSIC_STUDIO_INVISIBLE_PED_COLLISION_ENTOURAGE) , FALSE, FALSE)
				
				FREEZE_ENTITY_POSITION(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_ENTOURAGE], TRUE)
				//SET_ENTITY_COLLISION(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_ENTOURAGE], TRUE)
				SET_ENTITY_CAN_BE_DAMAGED(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_ENTOURAGE], FALSE)
				SET_ENTITY_VISIBLE(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_ENTOURAGE], FALSE)
				SET_ENTITY_PROOFS(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_ENTOURAGE], TRUE, TRUE, TRUE, FALSE, TRUE, TRUE, TRUE, TRUE)
				SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, (GET_HASH_KEY("prop_bin_01a"))))
				#IF IS_DEBUG_BUILD
				vCreateLoc = GET_COLLIDER_POSITION(MUSIC_STUDIO_INVISIBLE_PED_COLLISION_ENTOURAGE)
				PRINTLN("[AM_MP_MUSIC_STUDIO][MAINTAIN_INVISIBLE_COLLIDERS] creating collision MUSIC_STUDIO_INVISIBLE_PED_COLLISION_ENTOURAGE at: ",vCreateLoc )		
				#ENDIF
				SET_BIT(m_InteriorData.iBS, BS_MUSIC_STUDIO_ENTOURAGE_GIRL_COLLIDER_CREATED)
			ELSE
				PRINTLN("[AM_MP_MUSIC_STUDIO][MAINTAIN_INVISIBLE_COLLIDERS] MUSIC_STUDIO_INVISIBLE_PED_COLLISION_ENTOURAGE model not loaded yet")
			ENDIF
		ENDIF
	ELSE	
		IF SHOULD_REMOVE_ENTOURAGE_COLLIDER()
		AND DOES_ENTITY_EXIST(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_ENTOURAGE])
			CLEAR_BIT(m_InteriorData.iBS, BS_MUSIC_STUDIO_ENTOURAGE_GIRL_COLLIDER_CREATED)
			SAFE_DELETE_OBJECT(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_ENTOURAGE])
		ENDIF
	ENDIF
	
	
	IF IS_LOCAL_PLAYER_IN_MUSIC_STUDIO_SMOKING_ROOM()
		IF NOT DOES_ENTITY_EXIST(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_FRANKLIN])
			IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, (GET_HASH_KEY("prop_bin_01a"))))
				m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_FRANKLIN] = CREATE_OBJECT(INT_TO_ENUM(MODEL_NAMES, (GET_HASH_KEY("prop_bin_01a"))),GET_COLLIDER_POSITION(MUSIC_STUDIO_INVISIBLE_PED_COLLISION_FRANKLIN) , FALSE, FALSE)
				
				FREEZE_ENTITY_POSITION(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_FRANKLIN], TRUE)
				//SET_ENTITY_COLLISION(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_FRANKLIN], TRUE)
				SET_ENTITY_CAN_BE_DAMAGED(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_FRANKLIN], FALSE)
				SET_ENTITY_VISIBLE(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_FRANKLIN], FALSE)
				SET_ENTITY_PROOFS(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_FRANKLIN], TRUE, TRUE, TRUE, FALSE, TRUE, TRUE, TRUE, TRUE)
				SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, (GET_HASH_KEY("prop_bin_01a"))))
				#IF IS_DEBUG_BUILD
				vCreateLoc = GET_COLLIDER_POSITION(MUSIC_STUDIO_INVISIBLE_PED_COLLISION_FRANKLIN)
				PRINTLN("[AM_MP_MUSIC_STUDIO][MAINTAIN_INVISIBLE_COLLIDERS] creating collision MUSIC_STUDIO_INVISIBLE_PED_COLLISION_FRANKLIN at: ",vCreateLoc )		
				#ENDIF
			ELSE
				PRINTLN("[AM_MP_MUSIC_STUDIO][MAINTAIN_INVISIBLE_COLLIDERS] MUSIC_STUDIO_INVISIBLE_PED_COLLISION_FRANKLIN model not loaded yet")
			ENDIF
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_LAMAR])
			IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, (GET_HASH_KEY("prop_bin_01a"))))
				m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_LAMAR] = CREATE_OBJECT(INT_TO_ENUM(MODEL_NAMES, (GET_HASH_KEY("prop_bin_01a"))),GET_COLLIDER_POSITION(MUSIC_STUDIO_INVISIBLE_PED_COLLISION_LAMAR) , FALSE, FALSE)
				
				FREEZE_ENTITY_POSITION(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_LAMAR], TRUE)
				//SET_ENTITY_COLLISION(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_LAMAR], TRUE)
				SET_ENTITY_CAN_BE_DAMAGED(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_LAMAR], FALSE)
				SET_ENTITY_VISIBLE(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_LAMAR], FALSE)
				SET_ENTITY_PROOFS(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_LAMAR], TRUE, TRUE, TRUE, FALSE, TRUE, TRUE, TRUE, TRUE)
				SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, (GET_HASH_KEY("prop_bin_01a"))))
				#IF IS_DEBUG_BUILD
				vCreateLoc = GET_COLLIDER_POSITION(MUSIC_STUDIO_INVISIBLE_PED_COLLISION_LAMAR)
				PRINTLN("[AM_MP_MUSIC_STUDIO][MAINTAIN_INVISIBLE_COLLIDERS] creating collision MUSIC_STUDIO_INVISIBLE_PED_COLLISION_LAMAR  at: ",vCreateLoc )		
				#ENDIF
			ELSE
				PRINTLN("[AM_MP_MUSIC_STUDIO][MAINTAIN_INVISIBLE_COLLIDERS] MUSIC_STUDIO_INVISIBLE_PED_COLLISION_FRANKLIN model not loaded yet")
			ENDIF
		ENDIF
	ENDIF
	
	
	
	
	
	IF g_clubMusicData.eActiveDJ != CLUB_DJ_DR_DRE_ABSENT
		IF NOT DOES_ENTITY_EXIST(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_DR_DRE1]) 
			IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, (GET_HASH_KEY("prop_table_02"))))
				m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_DR_DRE1] = CREATE_OBJECT(INT_TO_ENUM(MODEL_NAMES, (GET_HASH_KEY("prop_table_02"))),GET_COLLIDER_POSITION(MUSIC_STUDIO_INVISIBLE_PED_COLLISION_DR_DRE1) , FALSE, FALSE)
			
				FREEZE_ENTITY_POSITION(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_DR_DRE1], TRUE)
				SET_ENTITY_ROTATION(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_DR_DRE1], <<0.0, 0.0, 50.700>>)
				SET_ENTITY_CAN_BE_DAMAGED(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_DR_DRE1], FALSE)
				SET_ENTITY_VISIBLE(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_DR_DRE1], FALSE)
				SET_ENTITY_PROOFS(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_DR_DRE1], TRUE, TRUE, TRUE, FALSE, TRUE, TRUE, TRUE, TRUE)
				SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, (GET_HASH_KEY("prop_table_02")))) //set an no longer needed after second one
				#IF IS_DEBUG_BUILD
				vCreateLoc = GET_COLLIDER_POSITION(MUSIC_STUDIO_INVISIBLE_PED_COLLISION_DR_DRE1)
				PRINTLN("[AM_MP_MUSIC_STUDIO][MAINTAIN_INVISIBLE_COLLIDERS] creating collision MUSIC_STUDIO_INVISIBLE_PED_COLLISION_DR_DRE1  at: ",vCreateLoc )		
				#ENDIF
			ELSE
				PRINTLN("[AM_MP_MUSIC_STUDIO][MAINTAIN_INVISIBLE_COLLIDERS] prop_table_02 model not loaded yet")
			ENDIF
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_DR_DRE2]) 
			IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, (GET_HASH_KEY("PROP_BOX_WOOD07A"))))
				m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_DR_DRE2] = CREATE_OBJECT(INT_TO_ENUM(MODEL_NAMES, (GET_HASH_KEY("PROP_BOX_WOOD07A"))),GET_COLLIDER_POSITION(MUSIC_STUDIO_INVISIBLE_PED_COLLISION_DR_DRE2) , FALSE, FALSE)
			
				FREEZE_ENTITY_POSITION(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_DR_DRE2], TRUE)
				SET_ENTITY_ROTATION(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_DR_DRE2], <<0.0, 0.00, 14.930>>)
				SET_ENTITY_CAN_BE_DAMAGED(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_DR_DRE2], FALSE)
				SET_ENTITY_VISIBLE(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_DR_DRE2], FALSE)
				SET_ENTITY_PROOFS(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_DR_DRE2], TRUE, TRUE, TRUE, FALSE, TRUE, TRUE, TRUE, TRUE)
				SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, (GET_HASH_KEY("PROP_BOX_WOOD07A"))))
				#IF IS_DEBUG_BUILD
				vCreateLoc = GET_COLLIDER_POSITION(MUSIC_STUDIO_INVISIBLE_PED_COLLISION_DR_DRE2)
				PRINTLN("[AM_MP_MUSIC_STUDIO][MAINTAIN_INVISIBLE_COLLIDERS] creating collision MUSIC_STUDIO_INVISIBLE_PED_COLLISION_DR_DRE2  at: ",vCreateLoc )		
				#ENDIF
			ELSE
				PRINTLN("[AM_MP_MUSIC_STUDIO][MAINTAIN_INVISIBLE_COLLIDERS] PROP_BOX_WOOD07A model not loaded yet")
			ENDIF
		ENDIF
		
		
		IF NOT DOES_ENTITY_EXIST(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_DR_DRE_LEFT]) 
			IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, (GET_HASH_KEY("SF_Prop_SF_Blocker_Studio_01a"))))
				m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_DR_DRE_LEFT] = CREATE_OBJECT(INT_TO_ENUM(MODEL_NAMES, (GET_HASH_KEY("SF_Prop_SF_Blocker_Studio_01a"))),GET_COLLIDER_POSITION(MUSIC_STUDIO_INVISIBLE_PED_COLLISION_DR_DRE_LEFT) , FALSE, FALSE)
			
				FREEZE_ENTITY_POSITION(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_DR_DRE_LEFT], TRUE)
				//SET_ENTITY_ROTATION(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_DR_DRE_LEFT], <<0.0, 0.000, 0.00>>)
				SET_ENTITY_CAN_BE_DAMAGED(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_DR_DRE_LEFT], FALSE)
				SET_ENTITY_PROOFS(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_DR_DRE_LEFT], TRUE, TRUE, TRUE, FALSE, TRUE, TRUE, TRUE, TRUE)
				SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, (GET_HASH_KEY("SF_Prop_SF_Blocker_Studio_01a"))))
				#IF IS_DEBUG_BUILD
				vCreateLoc = GET_COLLIDER_POSITION(MUSIC_STUDIO_INVISIBLE_PED_COLLISION_DR_DRE_LEFT)
				PRINTLN("[AM_MP_MUSIC_STUDIO][MAINTAIN_INVISIBLE_COLLIDERS] creating collision MUSIC_STUDIO_INVISIBLE_PED_COLLISION_DR_DRE_LEFT at: ",vCreateLoc )		
				#ENDIF
			ENDIF
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_DR_DRE_RIGHT]) 
			IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, (GET_HASH_KEY("SF_Prop_SF_Blocker_Studio_02a"))))
				m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_DR_DRE_RIGHT] = CREATE_OBJECT(INT_TO_ENUM(MODEL_NAMES, (GET_HASH_KEY("SF_Prop_SF_Blocker_Studio_02a"))),GET_COLLIDER_POSITION(MUSIC_STUDIO_INVISIBLE_PED_COLLISION_DR_DRE_RIGHT) , FALSE, FALSE)
			
				FREEZE_ENTITY_POSITION(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_DR_DRE_RIGHT], TRUE)
				//SET_ENTITY_ROTATION(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_DR_DRE_RIGHT], <<0.0, 90.000, 30.330>>)
				SET_ENTITY_CAN_BE_DAMAGED(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_DR_DRE_RIGHT], FALSE)
				SET_ENTITY_PROOFS(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_DR_DRE_RIGHT], TRUE, TRUE, TRUE, FALSE, TRUE, TRUE, TRUE, TRUE)
				SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, (GET_HASH_KEY("SF_Prop_SF_Blocker_Studio_02a"))))
				#IF IS_DEBUG_BUILD
				vCreateLoc = GET_COLLIDER_POSITION(MUSIC_STUDIO_INVISIBLE_PED_COLLISION_DR_DRE_RIGHT)
				PRINTLN("[AM_MP_MUSIC_STUDIO][MAINTAIN_INVISIBLE_COLLIDERS] creating collision MUSIC_STUDIO_INVISIBLE_PED_COLLISION_DR_DRE_RIGHT  at: ",vCreateLoc )		
				#ENDIF
			ENDIF
		ENDIF
		

	ELSE
		IF DOES_ENTITY_EXIST(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_DR_DRE1]) 
			SAFE_DELETE_OBJECT(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_DR_DRE1])
		ENDIF
		
		IF DOES_ENTITY_EXIST(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_DR_DRE2]) 
			SAFE_DELETE_OBJECT(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_DR_DRE2])
		ENDIF
		
		IF DOES_ENTITY_EXIST(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_DR_DRE_LEFT]) 
			SAFE_DELETE_OBJECT(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_DR_DRE_LEFT])
		ENDIF
		
		IF DOES_ENTITY_EXIST(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_DR_DRE_RIGHT]) 
			SAFE_DELETE_OBJECT(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_DR_DRE_RIGHT])
		ENDIF
	ENDIF
	
	IF g_clubMusicData.eActiveDJ = CLUB_DJ_DR_DRE_MUSICIAN_VOCALS	
		IF NOT DOES_ENTITY_EXIST(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_VOCALS1]) 
			IF REQUEST_LOAD_MODEL(prop_dummy_car)
				m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_VOCALS1] = CREATE_OBJECT(prop_dummy_car,GET_COLLIDER_POSITION(MUSIC_STUDIO_INVISIBLE_PED_COLLISION_VOCALS1) , FALSE, FALSE)
			
				FREEZE_ENTITY_POSITION(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_VOCALS1], TRUE)
				SET_ENTITY_ROTATION(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_VOCALS1], <<0.0000, -0.0000, 59.6000>>)
				SET_ENTITY_CAN_BE_DAMAGED(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_VOCALS1], FALSE)
				SET_ENTITY_VISIBLE(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_VOCALS1], FALSE)
				SET_ENTITY_PROOFS(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_VOCALS1], TRUE, TRUE, TRUE, FALSE, TRUE, TRUE, TRUE, TRUE)
				//SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, (GET_HASH_KEY("prop_bin_01a")))) //set an no longer needed after second one
				#IF IS_DEBUG_BUILD
				vCreateLoc = GET_COLLIDER_POSITION(MUSIC_STUDIO_INVISIBLE_PED_COLLISION_VOCALS1)
				PRINTLN("[AM_MP_MUSIC_STUDIO][MAINTAIN_INVISIBLE_COLLIDERS] creating collision MUSIC_STUDIO_INVISIBLE_PED_COLLISION_VOCALS1  at: ",vCreateLoc )		
				#ENDIF
			ELSE
				PRINTLN("[AM_MP_MUSIC_STUDIO][MAINTAIN_INVISIBLE_COLLIDERS] xm_prop_base_fence_02 model not loaded yet")
			ENDIF
		ENDIF
	ELSE
		IF DOES_ENTITY_EXIST(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_VOCALS1]) 
			SAFE_DELETE_OBJECT(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_VOCALS1])
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(m_InteriorData.iBS, BS_MUSIC_STUDIO_CREATED_RESTRICTED_AREA_BLOCKERS)
	
		IF NOT DOES_ENTITY_EXIST(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_PATROL_1]) 
			IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, (GET_HASH_KEY("prop_bin_01a"))))
				m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_PATROL_1] = CREATE_OBJECT(INT_TO_ENUM(MODEL_NAMES, (GET_HASH_KEY("prop_bin_01a"))),GET_COLLIDER_POSITION(MUSIC_STUDIO_INVISIBLE_PED_COLLISION_PATROL_1) , FALSE, FALSE)		
				FREEZE_ENTITY_POSITION(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_PATROL_1], TRUE)
				SET_ENTITY_ROTATION(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_PATROL_1], <<0.0, 90.000, -90.000>>)
				SET_ENTITY_CAN_BE_DAMAGED(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_PATROL_1], FALSE)
				SET_ENTITY_VISIBLE(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_PATROL_1], FALSE)
				SET_ENTITY_PROOFS(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_PATROL_1], TRUE, TRUE, TRUE, FALSE, TRUE, TRUE, TRUE, TRUE)
				SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, (GET_HASH_KEY("prop_bin_01a"))))
			ENDIF
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_PATROL_2]) 
			IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, (GET_HASH_KEY("prop_bin_01a"))))
				m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_PATROL_2] = CREATE_OBJECT(INT_TO_ENUM(MODEL_NAMES, (GET_HASH_KEY("prop_bin_01a"))),GET_COLLIDER_POSITION(MUSIC_STUDIO_INVISIBLE_PED_COLLISION_PATROL_2) , FALSE, FALSE)		
				FREEZE_ENTITY_POSITION(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_PATROL_2], TRUE)
				SET_ENTITY_ROTATION(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_PATROL_2], <<0.0, 90.000, 180.000>>)
				SET_ENTITY_CAN_BE_DAMAGED(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_PATROL_2], FALSE)
				SET_ENTITY_VISIBLE(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_PATROL_2], FALSE)
				SET_ENTITY_PROOFS(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_PATROL_2], TRUE, TRUE, TRUE, FALSE, TRUE, TRUE, TRUE, TRUE)
				SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, (GET_HASH_KEY("prop_bin_01a"))))
			ENDIF
		ENDIF
				
		IF NOT DOES_ENTITY_EXIST(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_PATROL_3]) 
			IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, (GET_HASH_KEY("prop_bin_01a"))))
				m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_PATROL_3] = CREATE_OBJECT(INT_TO_ENUM(MODEL_NAMES, (GET_HASH_KEY("prop_bin_01a"))),GET_COLLIDER_POSITION(MUSIC_STUDIO_INVISIBLE_PED_COLLISION_PATROL_3) , FALSE, FALSE)		
				FREEZE_ENTITY_POSITION(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_PATROL_3], TRUE)
				SET_ENTITY_ROTATION(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_PATROL_3], <<0.0, 90.000, 0.00>>)
				SET_ENTITY_CAN_BE_DAMAGED(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_PATROL_3], FALSE)
				SET_ENTITY_VISIBLE(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_PATROL_3], FALSE)
				SET_ENTITY_PROOFS(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_PATROL_3], TRUE, TRUE, TRUE, FALSE, TRUE, TRUE, TRUE, TRUE)
				SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, (GET_HASH_KEY("prop_bin_01a"))))
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_PATROL_1])
		AND DOES_ENTITY_EXIST(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_PATROL_2])
		AND DOES_ENTITY_EXIST(m_InteriorData.objCollisions[MUSIC_STUDIO_INVISIBLE_PED_COLLISION_PATROL_3])
			SET_BIT(m_InteriorData.iBS, BS_MUSIC_STUDIO_CREATED_RESTRICTED_AREA_BLOCKERS)
		ENDIF
		
	ENDIF
ENDPROC

FUNC RESTRICTED_AREA_DETAILS GET_CASINO_CLUB_RESTRICTED_AREA_DETAILS(RESTRICTED_AREAS eArea)
	
	RESTRICTED_AREA_DETAILS sDetails
	
	SWITCH eArea
		CASE RA_DOOR_1
			sDetails.vAngledAreaA		= <<-1006.237,-48.271,-100.480>>
			sDetails.vAngledAreaB		= <<-1006.237,-51.075, -97.460>>
			sDetails.fAngledAreaWidth	= 6.0
			sDetails.sKickHelp			= "MUS_STIO_DOOR_R"					
			sDetails.vGotoCoords		= <<-1010.6329, -52.1342, -100.4031>>
			sDetails.bKickOut			= TRUE
		BREAK
		CASE RA_DOOR_2
			sDetails.vAngledAreaA		= <<-1015.181,-48.271,-100.00>>
			sDetails.vAngledAreaB		= <<-1015.181,-51.075,-98.113>>
			sDetails.fAngledAreaWidth	= 6.0	
			sDetails.sKickHelp			= "MUS_STIO_DOOR_R"	
			sDetails.vGotoCoords		= <<-1010.6329, -52.1342, -100.4031>>
			sDetails.bKickOut			= TRUE			
		BREAK
		CASE RA_DOOR_3
			sDetails.vAngledAreaA		= <<-1011.523,-86.634, -100.074>>
			sDetails.vAngledAreaB		= <<-1008.131,-86.634, -98.093>>
			sDetails.fAngledAreaWidth	= 3.75		
			sDetails.sKickHelp			= "MUS_STIO_DOOR_R"					
			sDetails.vGotoCoords		= <<-1011.6202, -83.7252, -100.4031>>
			sDetails.bKickOut			= TRUE			
		BREAK
	ENDSWITCH
	
	RETURN sDetails
ENDFUNC

FUNC BOOL IS_PLAYER_IN_RESTRICTED_AREA(RESTRICTED_AREA_DETAILS &sDetails)
	
	IF NOT IS_PLAYER_IN_MUSIC_STUDIO(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	RETURN IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), sDetails.vAngledAreaA, sDetails.vAngledAreaB, sDetails.fAngledAreaWidth)
ENDFUNC

FUNC BOOL IS_SAFE_TO_KICK_PLAYER_FROM_RESTRICTED_AREA()
	IF g_bDrinkingActivityRunning
	OR MPGlobalsInteractions.QuickplayInteraction.bPlayInteractionAnim
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC KICK_PLAYER_FROM_RESTICTED_AREA(RESTRICTED_AREA_DETAILS& details)
	IF IS_SAFE_TO_KICK_PLAYER_FROM_RESTRICTED_AREA()
		CLEAR_PED_TASKS(PLAYER_PED_ID())
		TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(), details.vGotoCoords, PEDMOVE_WALK)
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
	ENDIF
	SET_LOCAL_PLAYER_BEING_KICKED_OUT_OF_RESTRICTED_AREA(TRUE)
ENDPROC

PROC PRINT_RESTRICTED_AREA_HELP(RESTRICTED_AREA_DETAILS &sDetails)
	IF NOT IS_STRING_NULL_OR_EMPTY(sDetails.sKickHelp)
		IF sDetails.bKickOut
			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sDetails.sKickHelp)
				PRINT_HELP(sDetails.sKickHelp)														
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_MUSIC_STUDIO_AREA_ACCESS_RESTRICTIONS()
	IF NOT NETWORK_IS_ACTIVITY_SESSION()
	AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
		
		REPEAT RESTRICTED_AREA_COUNT m_InteriorData.iRestrictedAreaLoop
			RESTRICTED_AREAS eArea			 = INT_TO_ENUM(RESTRICTED_AREAS, m_InteriorData.iRestrictedAreaLoop)
			RESTRICTED_AREA_DETAILS sDetails = GET_CASINO_CLUB_RESTRICTED_AREA_DETAILS(eArea)
			
			IF m_InteriorData.eAreaLastKickedOutOf != RESTRICTED_AREA_COUNT
				eArea 		= m_InteriorData.eAreaLastKickedOutOf
				sDetails 	= GET_CASINO_CLUB_RESTRICTED_AREA_DETAILS(eArea)
				PRINTLN("AM_MP_MUSIC_STUDIO - MAINTAIN_MUSIC_STUDIO_AREA_ACCESS_RESTRICTIONS - Being kicked out of area: ", eArea)
			ENDIF
			
			IF IS_PLAYER_IN_RESTRICTED_AREA(sDetails)
			AND sDetails.bKickOut
			
				IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) != WAITING_TO_START_TASK
				AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) != PERFORMING_TASK	
					m_InteriorData.eAreaLastKickedOutOf = eArea
					KICK_PLAYER_FROM_RESTICTED_AREA(sDetails)			
				ENDIF	
				
				
				PRINT_RESTRICTED_AREA_HELP(sDetails)
				
			ELIF m_InteriorData.eAreaLastKickedOutOf != RESTRICTED_AREA_COUNT
				PRINTLN("AM_MP_MUSIC_STUDIO - MAINTAIN_MUSIC_STUDIO_AREA_ACCESS_RESTRICTIONS - No longer being kicked out of area")
				m_InteriorData.eAreaLastKickedOutOf  = RESTRICTED_AREA_COUNT
				SET_LOCAL_PLAYER_BEING_KICKED_OUT_OF_RESTRICTED_AREA(FALSE)
				
				IF IS_SKYSWOOP_AT_GROUND()
				AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
					CLEAR_PED_TASKS(PLAYER_PED_ID())
					NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				ENDIF
			ENDIF
		ENDREPEAT
			//m_InteriorData.iRestrictedAreaLoop = (m_InteriorData.iRestrictedAreaLoop + 1) % ENUM_TO_INT(RESTRICTED_AREA_COUNT)
	ELSE
		m_InteriorData.eAreaLastKickedOutOf  = RESTRICTED_AREA_COUNT
		SET_LOCAL_PLAYER_BEING_KICKED_OUT_OF_RESTRICTED_AREA(FALSE)
	ENDIF
ENDPROC

/// PURPOSE:
///    Draws a green marker outside the smoking room to indicate you can go in it.
PROC MAINTAIN_SMOKING_ROOM_MARKER()
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
	AND NOT IS_LOCAL_PLAYER_IN_MUSIC_STUDIO_SMOKING_ROOM()
	AND DOES_PLAYER_HAVE_ACCESS_TO_MUSIC_STUDIO_SMOKING_ROOM()
		INT iR, iG, iB, iA
		VECTOR vPos = <<-1004.9439, -74.4456, -100.0000>>
			
		GET_HUD_COLOUR(HUD_COLOUR_GREENDARK, iR, iG, iB, iA)
		DRAW_MARKER(MARKER_CYLINDER, vPos, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, <<0.60, 0.60, 0.60>>, iR, iG, iB, 255)
	ENDIF
ENDPROC

PROC MAINTAIN_AUDIO_SCENE_FOR_DRE_PERFORMANCE()
	//ACTIVATE DRE AUDIO SCENE
	IF NOT IS_BIT_SET(m_InteriorData.iBS, BS_MUSIC_STUDIO_SET_AUDIO_SCENE_FOR_DRE_PERFORMANCE)
	AND IS_DRE_IN_MUSIC_STUDIO()
		IF IS_AUDIO_SCENE_ACTIVE("DLC_Fixer_Studio_Interior_Scene")
			STOP_AUDIO_SCENE("DLC_Fixer_Studio_Interior_Scene")
		ENDIF
		
		IF NOT IS_AUDIO_SCENE_ACTIVE("DLC_Security_Music_Studio_Producer_IG_Playing_Scene")
			START_AUDIO_SCENE("DLC_Security_Music_Studio_Producer_IG_Playing_Scene")
		ENDIF
		SET_BIT(m_InteriorData.iBS, BS_MUSIC_STUDIO_SET_AUDIO_SCENE_FOR_DRE_PERFORMANCE)
	ENDIF
	
	//REVERT TO MUSIC STUDIO AUDIO SCENE
	IF IS_BIT_SET(m_InteriorData.iBS, BS_MUSIC_STUDIO_SET_AUDIO_SCENE_FOR_DRE_PERFORMANCE)
	AND NOT IS_DRE_IN_MUSIC_STUDIO()
		IF IS_AUDIO_SCENE_ACTIVE("DLC_Security_Music_Studio_Producer_IG_Playing_Scene")
			STOP_AUDIO_SCENE("DLC_Security_Music_Studio_Producer_IG_Playing_Scene")
		ENDIF
		
		IF NOT IS_AUDIO_SCENE_ACTIVE("DLC_Fixer_Studio_Interior_Scene")
			START_AUDIO_SCENE("DLC_Fixer_Studio_Interior_Scene")
		ENDIF
		CLEAR_BIT(m_InteriorData.iBS, BS_MUSIC_STUDIO_SET_AUDIO_SCENE_FOR_DRE_PERFORMANCE)
	ENDIF	
ENDPROC

PROC MAINTAIN_DISABLE_RECORDING_DURING_PERFORMANCE()
	IF g_clubMusicData.eActiveDJ != CLUB_DJ_DR_DRE_ABSENT
		REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
	ENDIF
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════╡ MAIN LOGIC PROC ╞══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC RUN_MAIN_CLIENT_LOGIC()
	// NOTE: Only update procedures should be called from 
	// within RUN_MAIN_CLIENT_LOGIC. Custom logic should be 
	// placed inside of its own procedure and referenced here.
	
	MAINTAIN_PLAYER_FLAGS()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_PLAYER_FLAGS")
	#ENDIF	
	#ENDIF
		
	IF NOT IS_LOCAL_PLAYER_IN_MUSIC_STUDIO_SMOKING_ROOM()
		MAINTAIN_CLUB_MUSIC(CLUB_LOCATION_MUSIC_STUDIO, m_ServerBD.DJServerData, m_InteriorData.DJLocalData)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_CLUB_MUSIC")
		#ENDIF
		#ENDIF	
	ENDIF
	
	MAINTAIN_PED_SCRIPT_LAUNCHING(PED_LOCATION_MUSIC_STUDIO, m_InteriorData.bPedScriptLaunched, m_InteriorData.iScriptInstance)
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_PED_SCRIPT_LAUNCHING")
	#ENDIF
	#ENDIF
	
	MAINTAIN_MUSIC_STUDIO_CCTV()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_MUSIC_STUDIO_CCTV")
	#ENDIF
	#ENDIF
	
	MAINTAIN_SEATING_ACTIVITY_LAUNCHING()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_SEATING_ACTIVITY_LAUNCHING")
	#ENDIF
	#ENDIF
	
	MAINTAIN_MUSIC_STUDIO_DOOR_LOCKS()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_MUSIC_STUDIO_DOOR_LOCKS")
	#ENDIF	
	#ENDIF

	MAINTAIN_SMOKING_ACTIVITY_LAUNCHING()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_SMOKING_ACTIVITY_LAUNCHING")
	#ENDIF
	#ENDIF
	
	MAINTAIN_PED_CAPSULE()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_PED_CAPSULE")
	#ENDIF
	#ENDIF
	
	IF NOT IS_LOCAL_PLAYER_IN_MUSIC_STUDIO_SMOKING_ROOM()
		START_ARCADE_CABINET_MANAGER_SCRIPT(eArcadeCabinetManager)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("START_ARCADE_CABINET_MANAGER_SCRIPT")
		#ENDIF
		#ENDIF
	ENDIF
	
	IF IS_MUSIC_STUDIO_STATE(MUSIC_STUDIO_STATE_LOAD_INTERIOR)
		
		LOAD_INTERIOR()
	
	ELIF IS_MUSIC_STUDIO_STATE(MUSIC_STUDIO_STATE_CREATE_ENTITIES)
	
		CREATE_MUSIC_STUDIO_ENTITIES()
	
	ELIF IS_MUSIC_STUDIO_STATE(MUSIC_STUDIO_STATE_IDLE)
		
		IF NETWORK_IS_ACTIVITY_SESSION()
			
			//Only update logic for an activity session.
		
		ELSE
			
			IF NOT IS_BIT_SET(m_InteriorData.iBS, BS_MUSIC_STUDIO_CALLED_CLEAR_HELP)
				
				IF NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
				AND IS_PLAYER_IN_MUSIC_STUDIO(PLAYER_ID())
				//AND NOT SHOULD_KEEP_EXTERIOR_CAM_FOR_THIS_SIMPLE_INTERIOR(m_InteriorData.eSimpleInteriorID)
					IF IS_SCREEN_FADED_IN()
						CLEAR_HELP()
						SET_BIT(m_InteriorData.iBS, BS_MUSIC_STUDIO_CALLED_CLEAR_HELP)
						PRINTLN("AM_MP_MUSIC_STUDIO - Calling clear help")
					ENDIF
				ENDIF
			ENDIF	
			
			MAINTAIN_SPAWNING_OBJECTS()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_SPAWNING_OBJECTS")
			#ENDIF
			#ENDIF
			
			MAINTAIN_DISABLE_RECORDING_DURING_PERFORMANCE()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_DISABLE_RECORDING_DURING_PERFORMANCE")
			#ENDIF
			#ENDIF
			
			IF NOT IS_LOCAL_PLAYER_IN_MUSIC_STUDIO_SMOKING_ROOM()
				MAINTAIN_AUDIO_SCENE_FOR_DRE_PERFORMANCE()
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_AUDIO_SCENE_FOR_DRE_PERFORMANCE")
				#ENDIF
				#ENDIF	
				
				MAINTAIN_MUSIC_STUDIO_AREA_ACCESS_RESTRICTIONS()
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_MUSIC_STUDIO_AREA_ACCESS_RESTRICTIONS")
				#ENDIF
				#ENDIF	
				
				//ENGINEER'S SCREEN
				MAINTAIN_MUSIC_STUDIO_SCREEN(m_InteriorData.sEngineerScreenData)
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_MUSIC_STUDIO_SCREEN")
				#ENDIF
				#ENDIF		
					
			
				MAINTAIN_MUSIC_STUDIO_SCREEN(m_InteriorData.sStaffScreenData)
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_MUSIC_STUDIO_SCREEN")
				#ENDIF
				#ENDIF	
				
				
				MAINTAIN_DRE_CHAIR_PROP()
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_DRE_CHAIR_PROP")
				#ENDIF
				#ENDIF		
			
					
				MAINTAIN_MUSIC_STUDIO_MODEL_HIDES_FOR_DRE_PERFORMANCE()
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_MUSIC_STUDIO_MODEL_HIDES_FOR_DRE_PERFORMANCE")
				#ENDIF
				#ENDIF	
				
				MAINTAIN_WARP_PLAYER_OUT_OF_RECORDING_BOOTH_DURING_DRUMS_PERFORMANCE()
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_MUSIC_STUDIO_MODEL_HIDES_FOR_DRE_PERFORMANCE")
				#ENDIF
				#ENDIF	
				
				MAINTAIN_MUSIC_STUDIO_SHOP_SCRIPT_INSTANCES()
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_MUSIC_STUDIO_SHOP_SCRIPT_INSTANCES")
				#ENDIF	
				#ENDIF
			ELSE
				//FRANKLIN'S PHONE SCREEN
				MAINTAIN_MUSIC_STUDIO_SCREEN(m_InteriorData.sFranklinScreenData)
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_MUSIC_STUDIO_SCREEN")
				#ENDIF
				#ENDIF	
			ENDIF
			
			MAINTAIN_MUSIC_STUDIO_ACTIVITIES()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_MUSIC_STUDIO_ACTIVITIES")
			#ENDIF	
			#ENDIF
			
			
			MAINTAIN_MUSIC_STUDIO_INTERIOR_BLIPS()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_MUSIC_STUDIO_INTERIOR_BLIPS")
			#ENDIF	
			#ENDIF
			
			MAINTAIN_FIX_FOR_DOOR_ON_ENTRY()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_FIX_FOR_DOOR_ON_ENTRY")
			#ENDIF
			#ENDIF
			
			UPDATE_MUSIC_STUDIO_AWARDS()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("UPDATE_MUSIC_STUDIO_AWARDS")
			#ENDIF
			#ENDIF
			
			UPDATE_CHECK_FOR_EXIT_MOCAP()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("UPDATE_CHECK_FOR_EXIT_MOCAP")
			#ENDIF
			#ENDIF			
			
			MAINTAIN_SMOKING_ROOM_MARKER()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_SMOKING_ROOM_MARKER")
			#ENDIF
			#ENDIF
			
			IF NOT IS_BIT_SET(m_InteriorData.iBS, BS_MUSIC_STUDIO_GIVEN_VISIT_STUDIO_AWARD)
			AND IS_SCREEN_FADED_IN()
				IF NOT GET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_MUSIC_STUDIO)
					SET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_MUSIC_STUDIO, TRUE)
				ENDIF
				
				SET_BIT(m_InteriorData.iBS, BS_MUSIC_STUDIO_GIVEN_VISIT_STUDIO_AWARD)
			ENDIF
					
		ENDIF
	ELIF IS_MUSIC_STUDIO_STATE(MUSIC_STUDIO_STATE_MOCAP)
		MAINTAIN_MUSIC_STUDIO_MOCAP_CUTSCENE_STATE(m_InteriorData.eMocapScene)
	ENDIF
ENDPROC

PROC RUN_MAIN_SERVER_LOGIC()
	
	IF IS_MUSIC_STUDIO_SERVER_STATE(MUSIC_STUDIO_SERVER_STATE_LOADING)
		SET_MUSIC_STUDIO_SERVER_STATE(MUSIC_STUDIO_SERVER_STATE_IDLE)
	ELIF IS_MUSIC_STUDIO_SERVER_STATE(MUSIC_STUDIO_SERVER_STATE_IDLE)	
		
	ENDIF
ENDPROC

SCRIPT #IF FEATURE_MUSIC_STUDIO (SIMPLE_INTERIOR_CHILD_SCRIPT_DATA scriptData) #ENDIF
	
	#IF FEATURE_MUSIC_STUDIO
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		SCRIPT_INITIALISE(scriptData)
	ELSE
		SCRIPT_CLEANUP()
	ENDIF
	
	WHILE TRUE
		MP_LOOP_WAIT_ZERO()
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		SCRIPT_PROFILER_START_OF_FRAME() 
		#ENDIF
		#ENDIF
		
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_MUSIC_STUDIO - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE = TRUE")
			#ENDIF
			SCRIPT_CLEANUP()
		ENDIF
		
#IF IS_DEBUG_BUILD
		IF IS_KEYBOARD_KEY_PRESSED(KEY_F)
			SCRIPT_CLEANUP()
		ENDIF
#ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE")
		#ENDIF	
		#ENDIF
		
		IF SHOULD_THIS_SIMPLE_INTERIOR_CHILD_THREAD_TERMINATE(scriptData.iScriptInstance, scriptData.eSimpleInteriorID)
			#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_MUSIC_STUDIO - SHOULD_THIS_SIMPLE_INTERIOR_CHILD_THREAD_TERMINATE = TRUE")
			#ENDIF
			SCRIPT_CLEANUP()
		ENDIF
		
#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("SHOULD_THIS_SIMPLE_INTERIOR_CHILD_THREAD_TERMINATE")
	#ENDIF	
#ENDIF
		
		IF NETWORK_GET_HOST_OF_THIS_SCRIPT() != INT_TO_NATIVE(PARTICIPANT_INDEX , -1)
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iSimpleInteriorHost = NATIVE_TO_INT(NETWORK_GET_PLAYER_INDEX(NETWORK_GET_HOST_OF_THIS_SCRIPT()))
		ENDIF
		
		IF IS_MUSIC_STUDIO_SERVER_STATE(MUSIC_STUDIO_SERVER_STATE_IDLE)
			RUN_MAIN_CLIENT_LOGIC()
		ENDIF
		
#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("RUN_MAIN_CLIENT_LOGIC")
		#ENDIF	
#ENDIF
		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			RUN_MAIN_SERVER_LOGIC()
		ENDIF			
		
#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("RUN_MAIN_SERVER_LOGIC")
		#ENDIF	
#ENDIF
		
#IF IS_DEBUG_BUILD
		UPDATE_DEBUG_WIDGETS()
		
		#IF SCRIPT_PROFILER_ACTIVE
		SCRIPT_PROFILER_END_OF_FRAME() 
		#ENDIF
		
#ENDIF
			
				
	ENDWHILE
	#ENDIF	// FEATURE_MUSIC_STUDIO
	
ENDSCRIPT
