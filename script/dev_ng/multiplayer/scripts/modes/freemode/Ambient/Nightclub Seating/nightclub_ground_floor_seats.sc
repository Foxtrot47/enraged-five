///    Author 	: 	Alexander Murphy
///    Team 	: 	Online Tech
///    Info		: 	Each seating area has its own script so that the
///    				MAX_NUM_SEATS_FOR_SITTING_ACTIVITY can be redefined to minimise
///    				wasted space.

USING "globals.sch"

CONST_INT MAX_NUM_SEATS_FOR_SITTING_ACTIVITY 10

USING "nightclub_sitting_activities_public.sch"
USING "nightclub_seat_areas.sch"
USING "nightclub_sitting_helper.sch"

SEATS_LOCAL_DATA m_seatData

PROC STORE_SEAT_DATA()
	SETUP_NAMED_NIGHTCLUB_GROUND_FLOOR_SEAT(NIGHTCLUB_GROUND_FLOOR_SEAT_0, m_seatData, 0, SEAT_TYPE_CLUBHOUSE_ANIM)
	SETUP_NAMED_NIGHTCLUB_GROUND_FLOOR_SEAT(NIGHTCLUB_GROUND_FLOOR_SEAT_1, m_seatData, 1, SEAT_TYPE_OFFICE_ANIM)
	SETUP_NAMED_NIGHTCLUB_GROUND_FLOOR_SEAT(NIGHTCLUB_GROUND_FLOOR_SEAT_2, m_seatData, 2, SEAT_TYPE_CLUBHOUSE_ANIM)
	SETUP_NAMED_NIGHTCLUB_GROUND_FLOOR_SEAT(NIGHTCLUB_GROUND_FLOOR_SEAT_3, m_seatData, 3, SEAT_TYPE_OFFICE_ANIM)
//	SETUP_NAMED_NIGHTCLUB_GROUND_FLOOR_SEAT(NIGHTCLUB_GROUND_FLOOR_SEAT_4, m_seatData, 4, SEAT_TYPE_CLUBHOUSE_ANIM)
	SETUP_NAMED_NIGHTCLUB_GROUND_FLOOR_SEAT(NIGHTCLUB_GROUND_FLOOR_SEAT_5, m_seatData, 5, SEAT_TYPE_OFFICE_ANIM)
	SETUP_NAMED_NIGHTCLUB_GROUND_FLOOR_SEAT(NIGHTCLUB_GROUND_FLOOR_SEAT_6, m_seatData, 6, SEAT_TYPE_CLUBHOUSE_ANIM)
	SETUP_NAMED_NIGHTCLUB_GROUND_FLOOR_SEAT(NIGHTCLUB_GROUND_FLOOR_SEAT_7, m_seatData, 7, SEAT_TYPE_OFFICE_ANIM)
	SETUP_NAMED_NIGHTCLUB_GROUND_FLOOR_SEAT(NIGHTCLUB_GROUND_FLOOR_SEAT_8, m_seatData, 8, SEAT_TYPE_CLUBHOUSE_ANIM)
	SETUP_NAMED_NIGHTCLUB_GROUND_FLOOR_SEAT(NIGHTCLUB_GROUND_FLOOR_SEAT_9, m_seatData, 9, SEAT_TYPE_OFFICE_ANIM)
ENDPROC

FUNC BOOL SCRIPT_INIT()
	IF GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner <> INVALID_PLAYER_INDEX()
		IF NETWORK_IS_SCRIPT_ACTIVE("nightclub_ground_floor_seats", NATIVE_TO_INT(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner), TRUE)
			PRINTLN("[CLUB_SEATS] nightclub_ground_floor_seats script with this ID already running")
			
			RETURN FALSE
		ENDIF
		
		NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE, NATIVE_TO_INT(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner))
		
		HANDLE_NET_SCRIPT_INITIALISATION()
		
		SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
		
		LOG_MAX_NUM_SEATS()
		
		STORE_SEAT_DATA()
		
		PRINTLN("[CLUB_SEATS] SCRIPT_INIT - GROUND FLOOR - TRUE")
		
		RETURN TRUE
	ELSE
		PRINTLN("[CLUB_SEATS] SCRIPT_INIT - GROUND FLOOR - propertyOwner = INVALID_PLAYER_INDEX()")
		
		RETURN FALSE
	ENDIF
ENDFUNC

FUNC BOOL SHOULD_TERMINATE_SCRIPT()
	IF IS_SCREEN_FADED_OUT()
	AND NOT IS_LOCAL_PLAYER_NIGHTCLUB_RENOVATION_ACTIVE()
		PRINTLN("[CLUB_SEATS] SHOULD_TERMINATE_SCRIPT - GROUND FLOOR - screen is faded out")
		
		RETURN TRUE
	ENDIF
	
	IF NOT IS_LOCAL_PLAYER_IN_NIGHTCLUB_GROUND_FLOOR_SEAT_AREA()
		PRINTLN("[CLUB_SEATS] SHOULD_TERMINATE_SCRIPT - GROUND FLOOR - player outside bounds")
		
		RETURN TRUE
	ENDIF
	
	IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		PRINTLN("[CLUB_SEATS] SHOULD_TERMINATE_SCRIPT - GROUND FLOOR - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE")
		
		RETURN TRUE
	ENDIF
	
	IF GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner = INVALID_PLAYER_INDEX()
		PRINTLN("[CLUB_SEATS] SHOULD_TERMINATE_SCRIPT - GROUND FLOOR - propertyOwner = INVALID_PLAYER_INDEX()")
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SCRIPT_CLEANUP()
	PRINTLN("[CLUB_SEATS] - GROUND FLOOR - CLEANING UP SEATS")
	
	g_bInNightclubSeatLocate = FALSE
	
	CLEANUP_SEATS_ACTIVITY(m_seatData)
	
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

SCRIPT
	IF SCRIPT_INIT()
		WHILE NOT SHOULD_TERMINATE_SCRIPT()
			WAIT(0)
			
			MAINTAIN_SEATS_ACTIVITY(m_seatData)
		ENDWHILE
	ENDIF
	
	SCRIPT_CLEANUP()
ENDSCRIPT
