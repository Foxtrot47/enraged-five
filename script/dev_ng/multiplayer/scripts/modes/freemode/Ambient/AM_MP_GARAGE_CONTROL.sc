//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        AM_MP_GARAGE_CONTROL.sc																	//
// Description: Controls opening/closing of garage and warping player out									//
// Written by:  Conor McGuire																				//
// Date: 2013-03-05 (ISO 8601) 																				//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
USING "globals.sch"
USING "net_garages.sch"
USING "minigames_helpers.sch"

USING "net_wait_zero.sch"

// Main States 
CONST_INT SERVER_STAGE_INIT 				0
CONST_INT SERVER_STAGE_CREATE				1
CONST_INT SERVER_STAGE_RUNNING				2
CONST_INT SERVER_STAGE_LEAVE				3
CONST_INT SERVER_STAGE_END					4

CONST_INT CLIENT_STAGE_INIT					0
CONST_INT CLIENT_STAGE_RUNNING				1
CONST_INT CLIENT_STAGE_END					2

VECTOR vWorldPointLoc

GARAGE_DATA_STRUCT initGarageData

SCRIPT_TIMER netStoredTime

SCRIPT_TIMER NewSyncDelayTimerGarage

//SCRIPT_TIMER DoorClosedForWarpTimer

INT iGarageToCheckForCam

BOOL bDisabledStore

CONST_INT REQUEST_BS_OPEN 			0
CONST_INT REQUEST_BS_FORCE_OPEN		1

#IF IS_DEBUG_BUILD
	BOOL g_DB_RequestSimeonGarageOpen
	BOOL g_DB_ClearRequestSimeonGarageOpen
#ENDIF

// The server broadcast data.
// Everyone can read this data, only the server can update it. 
STRUCT ServerBroadcastData
	INT iServerStage
	INT iServerBS
	NEW_GARAGE_DOOR_DATA_SERVER garageDoorData[TOTAL_NUM_MP_GARAGES]
	
ENDSTRUCT
ServerBroadcastData serverBD

// The client broadcast data.
// Everyone can read this data, only the local player only the server can update it. 
STRUCT PlayerBroadcastData
	INT iStage
	INT iClientBS
	
	INT iRequestBS
	INT iForceRequestBS
ENDSTRUCT
PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                    PROCS                           ////////////////////////////////////////////////////////////////
PROC INIT_CURRENT_GARAGE_DETAILS()
	IF ARE_VECTORS_ALMOST_EQUAL(vWorldPointLoc,<< 1204.4286, -3110.8469, 4.3988 >>,0.5)
		initGarageData.iGarageIDHash = MP_GAR_SIMEON
	ENDIF
	
	GET_GARAGE_DETAILS(initGarageData,initGarageData.iGarageIDHash)
ENDPROC


// Do necessary pre game start ini.
// Returns FALSE if the script fails to receive an initial network broadcast.
FUNC BOOL PROCESS_PRE_GAME()
	NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE)
	// This makes sure the net script is active, waits untull it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
		
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
	
	// KGM: Wait for the first network broadcast before moving on - call this as the last instruction in pre-game
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		RETURN FALSE
	ENDIF
	
	// This script will not be paused if another script calls PAUSE_GAME
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	
	RETURN TRUE
ENDFUNC

PROC SET_SERVER_STAGE(INT iStage)
	#IF IS_DEBUG_BUILD
	SWITCH iStage
		CASE SERVER_STAGE_INIT
			NET_PRINT("AM_MP_GARAGE_CONTROL: SET_SERVER_STAGE = SERVER_STAGE_INIT") NET_NL()
		BREAK
		CASE SERVER_STAGE_RUNNING
			NET_PRINT("AM_MP_GARAGE_CONTROL: SET_SERVER_STAGE = SERVER_STAGE_RUNNING") NET_NL()
		BREAK
		CASE SERVER_STAGE_LEAVE
			NET_PRINT("AM_MP_GARAGE_CONTROL: SET_SERVER_STAGE = SERVER_STAGE_LEAVE") NET_NL()
		BREAK
		CASE SERVER_STAGE_END
			NET_PRINT("AM_MP_GARAGE_CONTROL: SET_SERVER_STAGE = SERVER_STAGE_END") NET_NL()
		BREAK
	ENDSWITCH
	#ENDIF
	serverBD.iServerStage = iStage
ENDPROC

FUNC INT GET_SERVER_STAGE()
	RETURN serverBD.iServerStage
ENDFUNC

PROC SET_CLIENT_STAGE(INT iStage)
	#IF IS_DEBUG_BUILD
	SWITCH iStage
		CASE CLIENT_STAGE_INIT
			NET_PRINT("AM_MP_GARAGE_CONTROL: SET_CLIENT_MISSION_STAGE = CLIENT_STAGE_INIT ") NET_NL()
		BREAK
		CASE CLIENT_STAGE_RUNNING
			NET_PRINT("AM_MP_GARAGE_CONTROL: SET_CLIENT_MISSION_STAGE = CLIENT_STAGE_RUNNING ") NET_NL()
		BREAK
		CASE CLIENT_STAGE_END
			NET_PRINT("AM_MP_GARAGE_CONTROL: SET_CLIENT_MISSION_STAGE = CLIENT_STAGE_END") NET_NL()
		BREAK
	ENDSWITCH
	#ENDIF
	playerBD[PARTICIPANT_ID_TO_INT()].iStage = iStage
ENDPROC

FUNC INT GET_CLIENT_STAGE(INT iParticipant)
	RETURN playerBD[iParticipant].iStage
ENDFUNC

PROC MAINTAIN_LEAVE_VEHICLE_FLAG()
	IF MPGlobalsAmbience.g_bAllowGetOutOfVehicle
		IF HAS_NET_TIMER_EXPIRED(MPGlobalsAmbience.g_AllowGetOutOfVehicleTimer,5000)
			MPGlobalsAmbience.g_bAllowGetOutOfVehicle = FALSE
		ENDIF
	ENDIF
ENDPROC

PROC CLEANUP_WARP_OUT_OF_GARAGE()
	IF MPGlobalsAmbience.g_iWarpOutOfGarageStage != 0
		IF IS_NET_PLAYER_OK(PLAYER_ID(),FALSE)
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		ENDIF
		
	ENDIF
	MPGlobalsAmbience.g_iWarpType = 0
	MPGlobalsAmbience.g_iWarpOutOfGarageStage = 0 
	MPGlobalsAmbience.g_bWarpOutOfGarageTookTooLongFading = FALSE
	MPGlobalsAmbience.g_bWarpOutOfGarageMadePlayerInvisible = FALSE
	MPGlobalsAmbience.g_bWarpOutOfGarageFadeStarted = FAlSE
	NET_PRINT("CLEANUP_WARP_OUT_OF_GARAGE()") NET_NL()
ENDPROC

/// PURPOSE:
///    Handles warping a player out a garage
FUNC BOOL NEW_CNC_SCRIPT_ONLY_HANDLE_WARPING_PLAYER_OUT_OF_GARAGE()
	IF MPGlobalsAmbience.g_iWarpOutOfGarageStage > 0
		IF MPGlobalsAmbience.g_bAllowGetOutOfVehicle
			TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
			DISABLE_LEAVE_VEHICLE_WHEN_IN_GARAGE()
		ENDIF
	ENDIF
	SWITCH MPGlobalsAmbience.g_iWarpOutOfGarageStage
		CASE 0
			IF IS_PLAYER_OK_TO_START_MP_CUTSCENE()
				IF NOT IS_BROWSER_OPEN()
					NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					NET_PRINT("MPGlobalsAmbience.g_iWarpOutOfGarageStage = 1") NET_NL()	
					MPGlobalsAmbience.g_bWarpOutOfGarageTookTooLongFading = FALSE
					MPGlobalsAmbience.g_bWarpOutOfGarageMadePlayerInvisible = FALSE
					MPGlobalsAmbience.g_iWarpOutOfGarageStage = 1
					REINIT_NET_TIMER(MPGlobalsAmbience.g_WarpOutOfGarageFailSafeTimer)
				ELSE
					NET_PRINT("CNC_SCRIPT_ONLY_HANDLE_WARPING_PLAYER_OUT_OF_GARAGE not warping - internet active") NET_NL()	
				ENDIF
			ENDIF
		BREAK
		
		CASE 1	
			
			IF DOOR_SYSTEM_GET_OPEN_RATIO(initGarageData.iDoor1ID) <= 0.1
				NET_PRINT("(Door Closed)MPGlobalsAmbience.g_iWarpOutOfGarageStage = 2") NET_NL()	
				MPGlobalsAmbience.g_iWarpOutOfGarageStage = 2
				REINIT_NET_TIMER(MPGlobalsAmbience.g_WarpOutOfGarageFailSafeTimer)
			ELSE
				#IF IS_DEBUG_BUILD
				PRINTLN("Garage door open ratio = ", DOOR_SYSTEM_GET_OPEN_RATIO(initGarageData.iDoor1ID))
				#ENDIF
				IF HAS_NET_TIMER_EXPIRED(MPGlobalsAmbience.g_WarpOutOfGarageFailSafeTimer,3000)
					NET_PRINT("(Hit timeout)MPGlobalsAmbience.g_iWarpOutOfGarageStage = 2") NET_NL()	
					MPGlobalsAmbience.g_iWarpOutOfGarageStage = 2
					REINIT_NET_TIMER(MPGlobalsAmbience.g_WarpOutOfGarageFailSafeTimer)
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			IF NOT MPGlobalsAmbience.g_bWarpOutOfGarageFadeStarted
				IF GET_ABS_NET_TIMER_DIFFERENCE(netStoredTime,MPGlobalsAmbience.g_WarpOutOfGarageFailSafeTimer) >= 500
					DO_SCREEN_FADE_OUT(1500)
					NET_PRINT("CNC_SCRIPT_ONLY_HANDLE_WARPING_PLAYER_OUT_OF_GARAGE fading screen") NET_NL()	
					MPGlobalsAmbience.g_bWarpOutOfGarageFadeStarted = TRUE
				ELSE
					NET_PRINT("GET_ABS_NET_TIMER_DIFFERENCE(netStoredTime,MPGlobalsAmbience.g_WarpOutOfGarageFailSafeTimer) < 500") NET_NL()		
				ENDIF
			ELSE
				IF IS_SCREEN_FADED_OUT()
					IF NOT MPGlobalsAmbience.g_bWarpOutOfGarageMadePlayerInvisible
						NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_SET_INVISIBLE )
						NET_PRINT("Setting player as invisible") NET_NL()
						MPGlobalsAmbience.g_bWarpOutOfGarageMadePlayerInvisible = TRUE
					ENDIF
				ENDIF
			ENDIF
			IF NOT MPGlobalsAmbience.g_bWarpOutOfGarageTookTooLongFading
				IF GET_ABS_NET_TIMER_DIFFERENCE(netStoredTime,MPGlobalsAmbience.g_WarpOutOfGarageFailSafeTimer) >= 3000
					IF NOT MPGlobalsAmbience.g_bWarpOutOfGarageTookTooLongFading
						MPGlobalsAmbience.g_bWarpOutOfGarageTookTooLongFading = TRUE
						NET_PRINT("CNC_SCRIPT_ONLY_HANDLE_WARPING_PLAYER_OUT_OF_GARAGE : took too long to fade continuing warp") NET_NL()	
						RESET_NET_TIMER(MPGlobalsAmbience.g_WarpOutOfGarageFailSafeTimer)
					ENDIF
				ELSE
					//NET_PRINT("GET_ABS_NET_TIMER_DIFFERENCE(netStoredTime,MPGlobalsAmbience.g_WarpOutOfGarageFailSafeTimer) < 3000") NET_NL()		
				ENDIF
			ELSE
				IF IS_SCREEN_FADED_OUT()
				OR MPGlobalsAmbience.g_bWarpOutOfGarageTookTooLongFading
					IF NOT MPGlobalsAmbience.g_bWarpOutOfGarageMadePlayerInvisible
						NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_SET_INVISIBLE )
						NET_PRINT("Setting player as invisible") NET_NL()
						MPGlobalsAmbience.g_bWarpOutOfGarageMadePlayerInvisible = TRUE
					ENDIF

					IF NOT WARP_TO_SPAWN_LOCATION(SPAWN_LOCATION_OUTSIDE_SIMEON_GARAGE, FALSE,FALSE)
						NET_PRINT("WARP_TO_SPAWN_LOCATION  SPAWN_LOCATION_OUTSIDE_SIMEON_GARAGE is returning false") NET_NL()
						RETURN FALSE
					ENDIF
					NET_PRINT("MPGlobalsAmbience.g_iWarpOutOfGarageStage = 3 (warped out of contact garage in freemode) ") NET_NL()
					MPGlobalsAmbience.g_iWarpOutOfGarageStage = 3 
				ENDIF
			ENDIF
		BREAK
		
		CASE 3
			IF MPGlobalsAmbience.g_bWarpOutOfGarageTookTooLongFading 
				IF DOES_CAM_EXIST(initGarageData.cameraData.theCam)
					RENDER_SCRIPT_CAMS(FALSE, FALSE,DEFAULT_INTERP_TO_FROM_GAME,TRUE,FALSE)
					DESTROY_CAM(initGarageData.cameraData.theCam)
					
					ENABLE_ALL_MP_HUD()
					DISABLE_SCRIPT_HUD(HUDPART_AWARDS, FALSE)
					SET_DPADDOWN_ACTIVE(TRUE)
					SET_WIDESCREEN_BORDERS(FALSE, -1)
					DISPLAY_RADAR(TRUE)
					DISPLAY_HUD(TRUE)
					NET_PRINT("NEW_CNC_SCRIPT_ONLY_HANDLE_WARPING_PLAYER_OUT_OF_GARAGE - cleaned up cam") NET_NL()
					
				ENDIF
	
				FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
				DO_SCREEN_FADE_IN(1000)
				
				NET_PRINT("MPGlobalsAmbience.g_iWarpOutOfGarageStage = 0 returning true - 1") NET_NL()
				CLEANUP_WARP_OUT_OF_GARAGE()
				RETURN TRUE
			ELSE
				IF GET_ABS_NET_TIMER_DIFFERENCE(netStoredTime,MPGlobalsAmbience.g_WarpOutOfGarageFailSafeTimer) >= 3000
					FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
					NET_PRINT("MPGlobalsAmbience.g_iWarpOutOfGarageStage = 0 returning true - 2") NET_NL()
					CLEANUP_WARP_OUT_OF_GARAGE()
					RETURN TRUE
				ELSE
					NET_PRINT("MPGlobalsAmbience.g_iWarpOutOfGarageStage = 0 returning true - 3") NET_NL()
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC


PROC NEW_MAINTAIN_WARP_OUT_OF_LOCKED_GARAGE()
	VECTOR vPlayerPos
	
	IF NOT MPGlobalsAmbience.g_bDisableWarpOutOfLockedGarage
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_DisableAutoWarpFromFreemodeGarage)
		IF IS_NET_PLAYER_OK(PLAYER_ID())
		AND NOT IS_PED_INJURED(PLAYER_PED_ID())
		AND NOT g_MultiplayerSettings.g_bSuicide
		AND IS_SKYSWOOP_AT_GROUND()
			vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
			IF MPGlobalsAmbience.NEW_g_iGarageWarpingOut = 0		
				IF GET_DISTANCE_BETWEEN_COORDS(vPlayerPos,initGarageData.vGarageLocation) <= 20
					IF IS_ENTITY_IN_GARAGE(PLAYER_PED_ID(),initGarageData.iGarageIDHash,TRUE,NewSyncDelayTimerGarage)
						IF NOT NEW_IS_GARAGE_SET_OPEN(initGarageData.iIndex)
						OR HAS_NET_TIMER_EXPIRED(MPGlobalsAmbience.g_WarpOutOfGarageFailSafeTimer,7000)
							SET_BIT(MPGlobalsAmbience.NEW_g_iGarageWarpingOut,0)
							PRINTLN("NEW_MAINTAIN_WARP_OUT_OF_LOCKED_GARAGE: player in locked garage at: ",vPlayerPos)
							EXIT
						ENDIF
					ELSE
						RESET_NET_TIMER(MPGlobalsAmbience.g_WarpOutOfGarageFailSafeTimer)
					ENDIF
					//ELSE
					//	EXIT
					//ENDIF
				ENDIF
			ELSE
				IF IS_BIT_SET(MPGlobalsAmbience.NEW_g_iGarageWarpingOut, 0)
					IF NEW_CNC_SCRIPT_ONLY_HANDLE_WARPING_PLAYER_OUT_OF_GARAGE()
						CLEAR_BIT(MPGlobalsAmbience.NEW_g_iGarageWarpingOut, 0)
						NET_PRINT("NEW_MAINTAIN_WARP_OUT_OF_LOCKED_GARAGE: Warped player out of locked garage") NET_NL()
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF MPGlobalsAmbience.g_iWarpOutOfGarageStage != 0 
				NET_PRINT("Clearing warp out of garage as player is not ok") NET_NL()
				CLEANUP_WARP_OUT_OF_GARAGE()
			ENDIF
		ENDIF
	ELSE
		IF MPGlobalsAmbience.g_iWarpOutOfGarageStage != 0 
			NET_PRINT("Clearing warp out of garage as warp is disabled") NET_NL()
			CLEANUP_WARP_OUT_OF_GARAGE()
		ENDIF
		#IF IS_DEBUG_BUILD
		IF HAS_NET_TIMER_STARTED(MPGlobalsAmbience.g_DebugWarpOutDisabledPrintTimer)
			IF GET_ABS_NET_TIMER_DIFFERENCE(netStoredTime,MPGlobalsAmbience.g_DebugWarpOutDisabledPrintTimer) >= 3000
				NET_PRINT("MPGlobalsAmbience.g_bDisableWarpOutOfLockedGarage = TRUE!") NET_NL()
				REINIT_NET_TIMER(MPGlobalsAmbience.g_DebugWarpOutDisabledPrintTimer)
			ENDIF
		ELSE
			NET_PRINT("MPGlobalsAmbience.g_bDisableWarpOutOfLockedGarage = TRUE!") NET_NL()
			REINIT_NET_TIMER(MPGlobalsAmbience.g_DebugWarpOutDisabledPrintTimer)
		ENDIF
		#ENDIF
	ENDIF
ENDPROC

FUNC BOOL WARP_OUT_OF_GARAGE_READY_FOR_CAMERA_CUT()
	IF MPGlobalsAmbience.g_iWarpOutOfGarageStage = 3
	OR MPGlobalsAmbience.g_iWarpOutOfGarageStage = 0
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC NEW_CLEANUP_GARAGE_CAM()
	initGarageData.cameraData.bStartedMoving = FALSE
	MPGlobalsAmbience.g_iGarageCamStage = GARAGE_CAM_STATE_WAIT
	UNLOCK_MINIMAP_ANGLE()
	IF bDisabledStore
		SET_STORE_ENABLED(TRUE)
		bDisabledStore = FALSE
		PRINTLN("AM_MP_GARAGE_CONTROL: enabled shop")
	ENDIF
	IF DOES_CAM_EXIST(initGarageData.cameraData.theCam)
		RENDER_SCRIPT_CAMS(FALSE, FALSE,DEFAULT_INTERP_TO_FROM_GAME,TRUE,FALSE)
		DESTROY_CAM(initGarageData.cameraData.theCam)
		
		ENABLE_ALL_MP_HUD()
		DISABLE_SCRIPT_HUD(HUDPART_AWARDS, FALSE)
		SET_DPADDOWN_ACTIVE(TRUE)
		SET_WIDESCREEN_BORDERS(FALSE, -1)
		DISPLAY_RADAR(TRUE)
		DISPLAY_HUD(TRUE)
	ENDIF
	NET_PRINT("NEW_CLEANUP_GARAGE_CAM()") NET_NL()
ENDPROC

FUNC BOOL IS_VEHICLE_SUITABLE(VEHICLE_INDEX vehID)
	IF NOT IS_THIS_MODEL_A_CAR(GET_ENTITY_MODEL(vehID))
	AND NOT IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(vehID))
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC


PROC NEW_MAINTAIN_GARAGE_CAMERA()
	SCRIPT_TIMER Timer
	VECTOR vPlayerPos
	BOOL bInVeh
	BOOL bDoorLocked

	IF IS_GARAGE_CAM_ACTIVE()
	//	PRINTLN("Open ratio: ", DOOR_SYSTEM_GET_OPEN_RATIO(initGarageData.iDoor1ID))
		SWITCH MPGlobalsAmbience.g_iGarageCamStage
			CASE GARAGE_CAM_STATE_WAIT
				initGarageData.cameraData.bDisableCameraPan = FALSE
				IF IS_NET_PLAYER_OK(PLAYER_ID())
				AND NOT IS_PED_INJURED(PLAYER_PED_ID())
					vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
					IF GET_DISTANCE_BETWEEN_COORDS(vPlayerPos,initGarageData.vGarageLocation) <= 20
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
								IF IS_VEHICLE_SUITABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
									bInVeh = TRUE
								ENDIF
							ENDIF
						ENDIF
						IF bInVeh
							IF NEW_IS_GARAGE_SET_OPEN(initGarageData.iIndex)
								IF IS_ENTITY_IN_GARAGE(PLAYER_PED_ID(),initGarageData.iGarageIDHash,FALSE,Timer,0,0)		
									IF DOOR_SYSTEM_GET_OPEN_RATIO(initGarageData.iDoor1ID) > 0.4
										NET_PRINT_STRINGS("NEW_MAINTAIN_GARAGE_CAMERA: Setting state GARAGE_CAM_STATE_INIT (player intesecting Garage), ", initGarageData.sGarageName ) NET_NL()
										MPGlobalsAmbience.g_iGarageCamStage = GARAGE_CAM_STATE_INIT
									ENDIF
								ENDIF					
							ELSE
								IF IS_ENTITY_IN_GARAGE(PLAYER_PED_ID(),initGarageData.iGarageIDHash,TRUE,Timer,0,0)			
									IF DOOR_SYSTEM_GET_OPEN_RATIO(initGarageData.iDoor1ID) > 0.4
										NET_PRINT_STRINGS("NEW_MAINTAIN_GARAGE_CAMERA: Setting state GARAGE_CAM_STATE_INIT (player intesecting Garage), ", initGarageData.sGarageName ) NET_NL()
										MPGlobalsAmbience.g_iGarageCamStage = GARAGE_CAM_STATE_INIT
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE GARAGE_CAM_STATE_INIT
				SET_STORE_ENABLED(FALSE)
				bDisabledStore = TRUE
				PRINTLN("NEW_MAINTAIN_GARAGE_CAMERA: Disabled shop")
				DISABLE_CELLPHONE_THIS_FRAME_ONLY()
				IF NOT DOES_CAM_EXIST(initGarageData.cameraData.theCam)
					initGarageData.cameraData.theCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE) 
					SET_CAM_ACTIVE(initGarageData.cameraData.theCam, TRUE)
					SET_CAM_PARAMS(initGarageData.cameraData.theCam, initGarageData.cameraData.vCam1Loc, initGarageData.cameraData.vCam1Rot,initGarageData.cameraData.fCam1FOV)
					SHAKE_CAM(initGarageData.cameraData.theCam,"Hand_shake", 0.3)
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
					DISABLE_ALL_MP_HUD()
					DISABLE_SCRIPT_HUD(HUDPART_AWARDS, TRUE)
					SET_DPADDOWN_ACTIVE(FALSE)
					SET_WIDESCREEN_BORDERS(TRUE, -1)
					DISPLAY_RADAR(FALSE)
					DISPLAY_HUD(FALSE)
					
				ENDIF
				IF IS_NET_PLAYER_OK(PLAYER_ID())
					LOCK_MINIMAP_ANGLE(ROUND(GET_ENTITY_HEADING(PLAYER_PED_ID())))
				ENDIF
				NET_PRINT("NEW_MAINTAIN_GARAGE_CAMERA: Setting state GARAGE_CAM_STATE_STATIC (from init)" ) NET_NL()
				MPGlobalsAmbience.g_iGarageCamStage = GARAGE_CAM_STATE_STATIC
				IF IS_NET_PLAYER_OK(PLAYER_ID())
				AND NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
							bInVeh = TRUE
						ENDIF
					ENDIF
				ENDIF

				IF (NOT IS_ENTITY_IN_GARAGE(PLAYER_PED_ID(),initGarageData.iGarageIDHash,FALSE,Timer,0,0)
				OR (NOT bInVeh AND NOT bDoorLocked))
				AND NOT IS_WARP_OUT_OF_GARAGE_IN_PROGRESS()
					NET_PRINT("NEW_MAINTAIN_GARAGE_CAMERA() player not in garage cleaning up cam") NET_NL()
					NEW_CLEANUP_GARAGE_CAM()
				ENDIF
			BREAK
			CASE GARAGE_CAM_STATE_STATIC
				IF IS_NET_PLAYER_OK(PLAYER_ID())
				AND NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
							bInVeh = TRUE
						ENDIF
					ENDIF
				ENDIF
				DISABLE_CELLPHONE_THIS_FRAME_ONLY()
				IF initGarageData.cameraData.bStartedMoving 
					SET_CAM_PARAMS(initGarageData.cameraData.theCam, GET_CAM_COORD(initGarageData.cameraData.theCam),GET_CAM_ROT(initGarageData.cameraData.theCam),GET_CAM_FOV(initGarageData.cameraData.theCam))
					initGarageData.cameraData.bStartedMoving = FALSE
				ENDIF
				
				IF NOT NEW_IS_GARAGE_SET_OPEN(initGarageData.iIndex)
					NET_PRINT("NEW_MAINTAIN_GARAGE_CAMERA: Setting state GARAGE_CAM_STATE_MOVING" ) NET_NL()
					MPGlobalsAmbience.g_iGarageCamStage = GARAGE_CAM_STATE_MOVING
				ENDIF
				IF (NOT IS_ENTITY_IN_GARAGE(PLAYER_PED_ID(),initGarageData.iGarageIDHash,FALSE,Timer,0,0)
				OR (NOT bInVeh AND NOT bDoorLocked))
				AND NOT IS_WARP_OUT_OF_GARAGE_IN_PROGRESS()
					NET_PRINT("NEW_MAINTAIN_GARAGE_CAMERA() player not in garage cleaning up cam") NET_NL()
					NEW_CLEANUP_GARAGE_CAM()
				ENDIF
			BREAK
			CASE GARAGE_CAM_STATE_MOVING
				IF IS_NET_PLAYER_OK(PLAYER_ID())
				AND NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
							bInVeh = TRUE
						ENDIF
					ENDIF
				ENDIF
				DISABLE_CELLPHONE_THIS_FRAME_ONLY()
				IF NOT initGarageData.cameraData.bStartedMoving
					SET_CAM_PARAMS(initGarageData.cameraData.theCam, initGarageData.cameraData.vCam2Loc, initGarageData.cameraData.vCam2Rot,initGarageData.cameraData.fCam2FOV, 16000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
					initGarageData.cameraData.bStartedMoving = TRUE
				ENDIF
				
				IF NEW_IS_GARAGE_SET_OPEN(initGarageData.iIndex)
				AND NOT IS_WARP_OUT_OF_GARAGE_IN_PROGRESS()
					NET_PRINT("NEW_MAINTAIN_GARAGE_CAMERA: Setting state GARAGE_CAM_STATE_STATIC (garage open)" ) NET_NL()
					MPGlobalsAmbience.g_iGarageCamStage = GARAGE_CAM_STATE_STATIC
				ENDIF
				IF (NOT IS_ENTITY_IN_GARAGE(PLAYER_PED_ID(),initGarageData.iGarageIDHash,FALSE,Timer,0,0)
				OR (NOT bInVeh AND NOT bDoorLocked))
				AND NOT IS_WARP_OUT_OF_GARAGE_IN_PROGRESS()
					NET_PRINT("NEW_MAINTAIN_GARAGE_CAMERA() player not in garage cleaning up cam") NET_NL()
					NEW_CLEANUP_GARAGE_CAM()
				ENDIF
				//NET_PRINT("MAINTAIN_GARAGE_CAMERA: Setting state GARAGE_CAM_STATE_END" ) NET_NL()
			BREAK
			CASE GARAGE_CAM_STATE_END
				NEW_CLEANUP_GARAGE_CAM()
			BREAK
		ENDSWITCH
	ELSE
		IF MPGlobalsAmbience.g_iGarageCamStage >  0
			NEW_CLEANUP_GARAGE_CAM()	
		ENDIF
	ENDIF
	iGarageToCheckForCam++
	IF iGarageToCheckForCam >= TOTAL_NUM_MP_GARAGES
		iGarageToCheckForCam = 0
	ENDIF
ENDPROC

PROC NEW_MAINTAIN_LOCAL_GLOBAL_STATES()
	INT i
	REPEAT TOTAL_NUM_MP_GARAGES i
		MPGlobalsAmbience.globalCopyOfGarageData[0] = serverBD.garageDoorData[i]
	ENDREPEAT
ENDPROC

PROC NEW_SERVER_CHECK_DOOR_EVENTS(INT iGarageLoopID)
	INT i
	//INT iPlayerID
	BOOL bForcedOpened, bOpened
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
		PARTICIPANT_INDEX participant = INT_TO_PARTICIPANTINDEX(i)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(participant)
			//PLAYER_INDEX thePlayer = NETWORK_GET_PLAYER_INDEX(participant)
			//iPlayerID = NATIVE_TO_INT(thePlayer)
			IF IS_BIT_SET(playerBD[i].iRequestBS,iGarageLoopID)
				SET_BIT(serverBD.garageDoorData[iGarageLoopID].iPlayerRequestedBS,i)
				//PRINTLN("Participant requesting door open!")
				bOpened = TRUE
			ELSE
				CLEAR_BIT(serverBD.garageDoorData[iGarageLoopID].iPlayerRequestedBS,i)
			ENDIF
			IF IS_BIT_SET(playerBD[i].iForceRequestBS,iGarageLoopID)
				SET_BIT(serverBD.garageDoorData[iGarageLoopID].iPlayerRequestedForcedBS,i)
				bForcedOpened = TRUE
			ELSE
				CLEAR_BIT(serverBD.garageDoorData[iGarageLoopID].iPlayerRequestedForcedBS,i)
			ENDIF
		ENDIF
		//PRINTLN("Checking participant # ", i, " Garage ID# ", iGarageLoopID)
		//PRINTLN("Their iRequestBS = ",playerBD[i].iRequestBS)
	ENDREPEAT
	IF NOT bOpened
		serverBD.garageDoorData[iGarageLoopID].iPlayerRequestedBS = 0
	ENDIF
	IF NOT bForcedOpened
		serverBD.garageDoorData[iGarageLoopID].iPlayerRequestedForcedBS = 0
	ENDIF
ENDPROC

PROC NEW_SERVER_PROCESS_GARAGE_DOORS()
	
	INT i
	IF IS_NET_PLAYER_OK(PLAYER_ID(),FALSE)
		i = 0
		
		NEW_SERVER_CHECK_DOOR_EVENTS(i)
		
		IF serverBD.garageDoorData[i].bStartClearArea
			IF NEW_IS_GARAGE_DOOR_PROPERLY_CLOSED(serverBD.garageDoorData[i].GarageDoorClosedFailsafe,initGarageData)
				RESET_NET_TIMER(serverBD.garageDoorData[i].GarageDoorClosedFailsafe)
				CLEAN_UP_GARAGE_INTERIOR(initGarageData.iGarageIDHash)
			ENDIF
		ELSE
			RESET_NET_TIMER(serverBD.garageDoorData[i].GarageDoorClosedFailsafe)
		ENDIF
		IF NOT NEW_IS_GARAGE_OCCUPIED(serverBD.garageDoorData[i],initGarageData)
			RESET_NET_TIMER(serverBD.garageDoorData[i].GarageOccupiedTimer)
			IF serverBD.garageDoorData[i].iPlayerRequestedForcedBS = 0
				IF serverBD.garageDoorData[i].iPlayerRequestedBS != 0
					IF serverBD.garageDoorData[i].bDoorLocked
						serverBD.garageDoorData[i].bDoorLocked = FALSE
						NET_PRINT_TIME() NET_NL()
						NET_PRINT_STRINGS("SERVER_PROCESS_GARAGE_DOORS_NEW() Host unlocking IN garage door : ", initGarageData.sGarageName) NET_NL()
					ENDIF
				ELSE
					IF NEW_IS_GARAGE_DOOR_CLEAR(initGarageData)
						IF NOT serverBD.garageDoorData[i].bDoorLocked
							serverBD.garageDoorData[i].bDoorLocked = TRUE
							NET_PRINT_TIME() NET_NL()
							NET_PRINT_STRINGS("SERVER_PROCESS_GARAGE_DOORS_NEW() Host locking IN garage door : ", initGarageData.sGarageName) NET_NL()
						ENDIF
					ELSE
						NET_PRINT_STRINGS("SERVER_PROCESS_GARAGE_DOORS_NEW() not locking IN door as door isn't clear to close: ",initGarageData.sGarageName) NET_NL()
					ENDIF
				ENDIF
			ELSE
				IF serverBD.garageDoorData[i].bDoorLocked
					serverBD.garageDoorData[i].bDoorLocked = FALSE
					NET_PRINT_TIME() NET_NL()
					NET_PRINT_STRINGS("SERVER_PROCESS_GARAGE_DOORS_NEW() Host unlocking IN garage door (forced open): ",initGarageData.sGarageName) NET_NL()
				ENDIF
			ENDIF
		ELSE
			IF NOT serverBD.garageDoorData[i].bDoorLocked
				IF NEW_IS_GARAGE_DOOR_CLEAR(initGarageData)
					serverBD.garageDoorData[i].bDoorLocked = TRUE
					NET_PRINT_TIME() NET_NL()
					NET_PRINT_STRINGS("SERVER_PROCESS_GARAGE_DOORS_NEW() Host locking IN garage door (occupied): ",initGarageData.sGarageName) NET_NL()
				ELSE
					NET_PRINT_STRINGS("SERVER_PROCESS_GARAGE_DOORS_NEW()  not locking door IN door isn't clear to close: ",initGarageData.sGarageName) NET_NL()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC NEW_SERVER_OPEN_CLOSE_GARAGE_DOORS()
	INT i = 0
	IF MPGlobalsAmbience.g_bDoorsInited
		IF IS_NET_PLAYER_OK(PLAYER_ID(),FALSE)
			IF serverBD.garageDoorData[i].bDoorLocked
				IF NOT serverBD.garageDoorData[i].bServerLockedDoor
					IF NETWORK_HAS_CONTROL_OF_DOOR(initGarageData.iDoor1ID)
					AND (initGarageData.iDoor2ID = 0 OR (initGarageData.iDoor2ID != 0 AND NETWORK_HAS_CONTROL_OF_DOOR(initGarageData.iDoor2ID)))
						DOOR_SYSTEM_SET_DOOR_STATE(initGarageData.iDoor1ID,DOORSTATE_LOCKED)
						NET_PRINT_STRINGS("AM_MP_GARAGE_CONTROL: Locking door 1 of garage: ",initGarageData.sGarageName) NET_NL()
						IF initGarageData.iDoor2ID != 0
							DOOR_SYSTEM_SET_DOOR_STATE(initGarageData.iDoor2ID,DOORSTATE_LOCKED)
							NET_PRINT_STRINGS("AM_MP_GARAGE_CONTROL: Locking door 2 of garage: ",initGarageData.sGarageName) NET_NL()
						ENDIF
						serverBD.garageDoorData[i].bServerLockedDoor = TRUE
					ELSE 
						NETWORK_REQUEST_CONTROL_OF_DOOR(initGarageData.iDoor1ID)
						IF initGarageData.iDoor2ID != 0
							NETWORK_REQUEST_CONTROL_OF_DOOR(initGarageData.iDoor2ID)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF serverBD.garageDoorData[i].bServerLockedDoor
					IF NETWORK_HAS_CONTROL_OF_DOOR(initGarageData.iDoor1ID)
					AND (initGarageData.iDoor2ID = 0 OR (initGarageData.iDoor2ID != 0 AND NETWORK_HAS_CONTROL_OF_DOOR(initGarageData.iDoor2ID)))
						DOOR_SYSTEM_SET_DOOR_STATE(initGarageData.iDoor1ID,DOORSTATE_UNLOCKED)
						NET_PRINT_STRINGS("AM_MP_GARAGE_CONTROL: UN-Locking door 1 of garage: ",initGarageData.sGarageName) NET_NL()
						IF initGarageData.iDoor2ID != 0 
							DOOR_SYSTEM_SET_DOOR_STATE(initGarageData.iDoor2ID,DOORSTATE_UNLOCKED)
							NET_PRINT_STRINGS("AM_MP_GARAGE_CONTROL: UN-Locking door 2 of garage: ",initGarageData.sGarageName) NET_NL()
						ENDIF
						serverBD.garageDoorData[i].bServerLockedDoor = FALSE
					ELSE 
						NETWORK_REQUEST_CONTROL_OF_DOOR(initGarageData.iDoor1ID)
						IF initGarageData.iDoor2ID != 0
							NETWORK_REQUEST_CONTROL_OF_DOOR(initGarageData.iDoor2ID)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_REQUEST_FLAGS()
	NEW_MAINTAIN_LOCAL_GLOBAL_STATES()
	IF playerBD[PARTICIPANT_ID_TO_INT()].iRequestBS != MPGlobalsAmbience.garageRequests.iOpenBS
		playerBD[PARTICIPANT_ID_TO_INT()].iRequestBS = MPGlobalsAmbience.garageRequests.iOpenBS
		
	ENDIF
	//PRINTLN("PARTICIPANT_ID_TO_INT() = ",PARTICIPANT_ID_TO_INT()) 
	//PRINTLN("playerBD[PARTICIPANT_ID_TO_INT()].iRequestBS now = ",playerBD[PARTICIPANT_ID_TO_INT()].iRequestBS )
	IF playerBD[PARTICIPANT_ID_TO_INT()].iForceRequestBS != MPGlobalsAmbience.garageRequests.iForceOpenBS
		playerBD[PARTICIPANT_ID_TO_INT()].iForceRequestBS = MPGlobalsAmbience.garageRequests.iForceOpenBS
		//PRINTLN("playerBD[PARTICIPANT_ID_TO_INT()].iForceRequestBS now = ",playerBD[PARTICIPANT_ID_TO_INT()].iForceRequestBS )
	ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_OUT_OF_RANGE()
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()),vWorldPointLoc,FALSE) >125
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC CLEANUP_SCRIPT()
	NEW_CLEANUP_GARAGE_CAM()
	CLEANUP_WARP_OUT_OF_GARAGE()
	//MPGlobalsAmbience.garageRequests.iOpenBS = 0
	//MPGlobalsAmbience.garageRequests.iForceOpenBS= 0
	NET_PRINT("AM_MP_GARAGE_CONTROL: cleaning up script") NET_NL()	
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

#IF IS_DEBUG_BUILD
PROC CREATE_WIDGETS()
	START_WIDGET_GROUP("AM_MP_GARAGE_CONTROL")
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		CREATE_SCRIPT_PROFILER_WIDGET()
		#ENDIF
		#ENDIF
		START_WIDGET_GROUP("Garage doors")
			ADD_WIDGET_BOOL("Warp out of locked garage disabled",MPGlobalsAmbience.g_bDisableWarpOutOfLockedGarage)
			ADD_WIDGET_BOOL("Allow get out of vehicle ",MPGlobalsAmbience.g_bAllowGetOutOfVehicle)
			START_WIDGET_GROUP("Garage Door States(SERVER)")
				ADD_WIDGET_BOOL("(READ ONLY)Simeon Locked",serverBD.garageDoorData[0].bDoorLocked)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Debug garage widget")
				START_WIDGET_GROUP("Simeon garage")
					ADD_WIDGET_BOOL("Request open: Simeon ",g_DB_RequestSimeonGarageOpen)
					ADD_WIDGET_BOOL("Clear Request open: Simeon contact",g_DB_ClearRequestSimeonGarageOpen)
					ADD_WIDGET_INT_READ_ONLY("Garage request flags: ", MPGlobalsAmbience.garageRequests.iOpenBS)
				STOP_WIDGET_GROUP()
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
ENDPROC

PROC MAINTAIN_DEBUG()
	IF g_DB_RequestSimeonGarageOpen
		REQUEST_GARAGE_OPEN(MP_GAR_SIMEON,TRUE)
		g_DB_RequestSimeonGarageOpen = FALSE
		NET_PRINT("WIDGET: requested simeon garage open") NET_NL()
	ENDIF
	IF g_DB_ClearRequestSimeonGarageOpen
		REQUEST_GARAGE_OPEN(MP_GAR_SIMEON,FALSE)
		g_DB_ClearRequestSimeonGarageOpen = FALSE
		NET_PRINT("WIDGET: clear requested simeon garage open") NET_NL()
	ENDIF
ENDPROC
#ENDIF

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                      Main Loop                             ////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
SCRIPT(coords_struct in_coords)

	vWorldPointLoc = in_coords.vec_coord[0]
	PRINTLN("AM_MP_GARAGE_CONTROL: started at ",vWorldPointLoc)
	INIT_CURRENT_GARAGE_DETAILS()
	
	IF NOT PROCESS_PRE_GAME()
		PRINTLN("AM_MP_GARAGE_CONTROL: Failed to receive initial network broadcast. Cleaning up.")
		CLEANUP_SCRIPT()
	ENDIF
	
	#IF IS_DEBUG_BUILD
	CREATE_WIDGETS()
	#ENDIF

	// Main loop.
	WHILE TRUE
		// One wait to rule them all. This can be the ONLY wait from here on in.....
		MP_LOOP_WAIT_ZERO()
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		SCRIPT_PROFILER_START_OF_FRAME()
		#ENDIF
		#ENDIF
		// If we have a match end event, bail.
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			CLEANUP_SCRIPT()
		ENDIF
		
		IF IS_PLAYER_OUT_OF_RANGE()
			CLEANUP_SCRIPT()
		ENDIF
		
		MAINTAIN_LEAVE_VEHICLE_FLAG()

		REINIT_NET_TIMER(netStoredTime)
		
		#IF IS_DEBUG_BUILD
			IF NOT HAS_NET_TIMER_STARTED(GarageOutputStopSpamTimer)
				g_DB_AllowGarageOutput = FALSE
				START_NET_TIMER(GarageOutputStopSpamTimer)
			ELIF GET_ABS_NET_TIMER_DIFFERENCE(netStoredTime,GarageOutputStopSpamTimer) >= 2000
				g_DB_AllowGarageOutput = TRUE
				RESET_NET_TIMER(GarageOutputStopSpamTimer)
			ENDIF
		#ENDIF

		SWITCH GET_CLIENT_STAGE(PARTICIPANT_ID_TO_INT())
			CASE CLIENT_STAGE_INIT
				IF GET_SERVER_STAGE() >= SERVER_STAGE_RUNNING
					SET_CLIENT_STAGE(CLIENT_STAGE_RUNNING) 
				ELSE
					NET_PRINT("AM_MP_GARAGE_CONTROL: Registered doors Waiting for GET_SERVER_STAGE() >= SERVER_STAGE_RUNNING") NET_NL()
				ENDIF
			BREAK
			CASE CLIENT_STAGE_RUNNING
				#IF IS_DEBUG_BUILD
				MAINTAIN_DEBUG()
				#IF SCRIPT_PROFILER_ACTIVE 
				ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_DEBUG()")
				#ENDIF
				#ENDIF
				
				MAINTAIN_REQUEST_FLAGS()
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_REQUEST_FLAGS()")
				#ENDIF
				#ENDIF
				
				NEW_MAINTAIN_GARAGE_CAMERA()
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("NEW_MAINTAIN_GARAGE_CAMERA()")
				#ENDIF
				#ENDIF
							
				NEW_MAINTAIN_WARP_OUT_OF_LOCKED_GARAGE()
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("NEW_MAINTAIN_WARP_OUT_OF_LOCKED_GARAGE()")
				#ENDIF
				#ENDIF

				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE
				SCRIPT_PROFILER_END_OF_FRAME()
				#ENDIF
				#ENDIF
			BREAK

			CASE CLIENT_STAGE_END
				CLEANUP_SCRIPT()
			BREAK
		
		ENDSWITCH
		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()

			SWITCH GET_SERVER_STAGE()
				
				// For CnC group missions, we may want to wait for all players to have launched the script.
				CASE SERVER_STAGE_INIT	
					SET_SERVER_STAGE(SERVER_STAGE_RUNNING)
				BREAK
				
				CASE SERVER_STAGE_RUNNING
					
					NEW_SERVER_PROCESS_GARAGE_DOORS()
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE
					ADD_SCRIPT_PROFILE_MARKER("NEW_SERVER_PROCESS_GARAGE_DOORS()")
					#ENDIF
					#ENDIF
				
					NEW_SERVER_OPEN_CLOSE_GARAGE_DOORS()
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE
					ADD_SCRIPT_PROFILE_MARKER("NEW_SERVER_OPEN_CLOSE_GARAGE_DOORS()")
					#ENDIF
					#ENDIF
				BREAK
				
				CASE SERVER_STAGE_LEAVE
				
				BREAK
				
				CASE SERVER_STAGE_END
				
				BREAK
			ENDSWITCH
		ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		SCRIPT_PROFILER_END_OF_FRAME()
		#ENDIF
		#ENDIF
	ENDWHILE
	//End of script
ENDSCRIPT
//EOF
