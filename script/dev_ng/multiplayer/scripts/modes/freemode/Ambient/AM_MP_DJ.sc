//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        AM_MP_DJ.sc																							//
//																													//
// Description: DJ Script creates DJ and runs animations.	 														//
//              Call START_DJ_SCRIPT to start a drone check out description in net_dj.sch							//
//																													//
// 																													//
//	   																												//
//																													//
// Written by:  Ata																									//
// Date:  		19/03/2019																							//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_event.sch"
USING "net_include.sch"
USING "net_events.sch"
USING "net_spawn.sch"
USING "net_ambience.sch"
USING "net_wait_zero.sch"
USING "freemode_events_header.sch"

#IF IS_DEBUG_BUILD
USING "net_debug_log.sch"
#ENDIF

#IF FEATURE_CASINO
USING "net_dj.sch"

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ DATA STRUCTURES ╞═════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

STRUCT ServerBroadcastData
	INT iServerBS
ENDSTRUCT

STRUCT PlayerBroadcastData
	INT iLocalBS
ENDSTRUCT

#IF IS_DEBUG_BUILD
STRUCT DjDebug
	FLOAT fDebugSyncSpeed
	BOOL bPauseSyncScene
ENDSTRUCT
#ENDIF

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡    CONSTS    ╞════════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

// iLocalBS 
CONST_INT LOCAL_BS_SYNC_SCENE_STARTED					0
CONST_INT LOCAL_BS_INITIAL_DJ_ANIM						1


//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡    VARIABLES    ╞═════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

DJ_STRUCT sDJ
PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]
ServerBroadcastData serverBD

#IF IS_DEBUG_BUILD
DjDebug sDebugDj
#ENDIF

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡    DEBUG    ╞═════════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

#IF IS_DEBUG_BUILD
PROC CREATE_DEBUG_WIDGET()
	START_WIDGET_GROUP("DJ")
		ADD_WIDGET_FLOAT_SLIDER("Synced Scene Speed", sDebugDj.fDebugSyncSpeed, -1, 20, 0.1)
		ADD_WIDGET_BOOL("Pause synced Scene", sDebugDj.bPauseSyncScene)
	STOP_WIDGET_GROUP()
ENDPROC

PROC UPDATE_DEBUG_WIDGETS()

ENDPROC

FUNC STRING GET_DJ_STATE_NAME(DJ_STATE eState)

	SWITCH eState
		CASE DJ_STATE_IDLE 			RETURN "DJ_STATE_IDLE" 
		CASE DJ_STATE_INIT 			RETURN "DJ_STATE_INIT" 
		CASE DJ_STATE_UPDATE		RETURN "DJ_STATE_UPDATE"
		CASE DJ_STATE_CLEANUP		RETURN "DJ_STATE_CLEANUP"
	ENDSWITCH
	
	RETURN "INVALID STATE"
	
ENDFUNC

FUNC STRING GET_DJ_NAME(DJ_TYPE eState)

	SWITCH eState
		CASE DJ_TYPE_INVALID 			RETURN "DJ_TYPE_INVALID" 
		CASE DJ_TYPE_SOLOMUN 			RETURN "DJ_TYPE_SOLOMUN" 
	ENDSWITCH
	
	RETURN "INVALID STATE"
	
ENDFUNC
#ENDIF

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡     CLEANUP     ╞══════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

PROC SCRIPT_CLEANUP()
	IF DOES_ENTITY_EXIST(sDJ.djPedIndex)
		DELETE_PED(sDJ.djPedIndex)
	ENDIF	
	
	SET_KILL_DJ_SCRIPT(FALSE)
	
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════╡     INITIALISE     ╞════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

FUNC BOOL IS_DJ_STATE(DJ_STATE eState)
	RETURN sDJ.eDjState = eState
ENDFUNC

PROC SET_DJ_STATE(DJ_STATE eState)
	IF sDJ.eDjState != eState
		sDJ.eDjState = eState
		PRINTLN("[AM_MP_DJ] - SET_DJ_STATE: ", GET_DJ_STATE_NAME(eState))
	ENDIF
ENDPROC

PROC SET_CURRENT_DJ(DJ_TYPE eCurrentDJ)
	sDj.eCurrentDj = eCurrentDJ
	CDEBUG1LN(DEBUG_NET_DJ, "[AM_MP_DJ] - SET_CURRENT_DJ : ", GET_DJ_NAME(eCurrentDJ))
ENDPROC

FUNC BOOL HAS_LOADED_ALL_DJ_ANIMS()
	INT iDJ = 0

	DJ_ANIM_DETAIL animDetail
	
		
	BOOL bReturn = TRUE
	REPEAT DJ_TYPE_COUNT iDJ
		IF iDJ != -1
			PRINTLN("[AM_MP_DJ] - HAS_LOADED_ALL_DJANIMS - Getting anims for ped: ", GET_DJ_NAME(INT_TO_ENUM(DJ_TYPE, iDJ)))
			
			GET_DJ_ANIM_DATA(INT_TO_ENUM(DJ_TYPE, iDJ), sDJ.iCurrentSequence, animDetail)
			
			IF NOT IS_STRING_NULL_OR_EMPTY(animDetail.sPedAnimDict[0])
				PRINTLN("[AM_MP_DJ] - HAS_LOADED_ALL_DJANIMS - anim dict: ", animDetail.sPedAnimDict[0])
				REQUEST_ANIM_DICT(animDetail.sPedAnimDict[0])
				IF NOT HAS_ANIM_DICT_LOADED(animDetail.sPedAnimDict[0])
					PRINTLN("[AM_MP_DJ] - HAS_LOADED_ALL_DJANIMS - waiting for dict to load: ", animDetail.sPedAnimDict[0])
					bReturn = FALSE
				ENDIF
			ENDIF
		ENDIF	
	ENDREPEAT
	
	
	RETURN bReturn
ENDFUNC

PROC INITIALISE()
	#IF IS_DEBUG_BUILD
	sDebugDj.fDebugSyncSpeed = -1
	CREATE_DEBUG_WIDGET()
	#ENDIF
	SET_CURRENT_DJ(DJ_TYPE_SOLOMUN)
	SET_DJ_STATE(DJ_STATE_INIT)
ENDPROC

PROC SCRIPT_INITIALISE(MP_MISSION_DATA missionScriptArgs)
	#IF IS_DEBUG_BUILD
	CDEBUG1LN(DEBUG_NET_DJ, "[AM_MP_DJ] - SCRIPT_INITIALISE - Launching instance: ", missionScriptArgs.iInstanceID)
	#ENDIF	
	
	NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE, missionScriptArgs.iInstanceID)
	
	// This makes sure the net script is active, waits untill it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))

	// KGM: Wait for the first network broadcast before moving on
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		PRINTLN("[AM_MP_DJ] - SCRIPT_INITIALISE FAILED TO RECEIVE INITIAL NETWORK BROADCAST - SCRIPT CLEANUP")
		SCRIPT_CLEANUP()
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		// This script will not be paused if another script calls PAUSE_GAME
		PRINTLN("[AM_MP_DJ] - INITIALISED")
	ELSE
		PRINTLN("[AM_MP_DJ] - INITIALISED NETWORK_IS_GAME_IN_PROGRESS = FALSE - SCRIPT CLEANUP")
		SCRIPT_CLEANUP()
	ENDIF
	
	INITIALISE()

ENDPROC

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡     METHODS     ╞══════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

PROC SET_DJ_SYNC_SCENE_STARTED(BOOL bStarted)
	IF bStarted
		IF NOT IS_BIT_SET(sDj.iLocalBS, LOCAL_BS_SYNC_SCENE_STARTED)
			SET_BIT(sDj.iLocalBS,LOCAL_BS_SYNC_SCENE_STARTED)
			PRINTLN("[AM_MP_DJ] - SET_DJ_SYNC_SCENE_STARTED TRUE")
		ENDIF	
	ELSE
		IF IS_BIT_SET(sDj.iLocalBS, LOCAL_BS_SYNC_SCENE_STARTED)
			CLEAR_BIT(sDj.iLocalBS,LOCAL_BS_SYNC_SCENE_STARTED)
			PRINTLN("[AM_MP_DJ] - SET_DJ_SYNC_SCENE_STARTED FALSE")
		ENDIF	
	ENDIF
ENDPROC

FUNC BOOL IS_DJ_SYNC_SCENE_STARTED()
	RETURN IS_BIT_SET(sDj.iLocalBS, LOCAL_BS_SYNC_SCENE_STARTED)
ENDFUNC

PROC SET_DJ_INITIAL_DJ_ANIM(BOOL bSet)
	IF bSet
		IF NOT IS_BIT_SET(sDj.iLocalBS, LOCAL_BS_INITIAL_DJ_ANIM)
			SET_BIT(sDj.iLocalBS,LOCAL_BS_INITIAL_DJ_ANIM)
			PRINTLN("[AM_MP_DJ] - SET_DJ_INITIAL_DJ_ANIM TRUE")
		ENDIF	
	ELSE
		IF IS_BIT_SET(sDj.iLocalBS, LOCAL_BS_INITIAL_DJ_ANIM)
			CLEAR_BIT(sDj.iLocalBS,LOCAL_BS_INITIAL_DJ_ANIM)
			PRINTLN("[AM_MP_DJ] - SET_DJ_INITIAL_DJ_ANIM FALSE")
		ENDIF	
	ENDIF
ENDPROC

FUNC BOOL IS_DJ_INITIAL_DJ_ANIM()
	RETURN IS_BIT_SET(sDj.iLocalBS, LOCAL_BS_INITIAL_DJ_ANIM)
ENDFUNC

FUNC BOOL SHOULD_KILL_THIS_SCRIPT()
	IF SHOULD_KILL_DJ_SCRIPT()
		PRINTLN("[AM_MP_DJ] - SHOULD_KILL_THIS_SCRIPT SHOULD_KILL_DJ_SCRIPT TRUE")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_DJ_IDLE()	

ENDPROC

FUNC BOOL CRAETE_DJ_PED()
	IF NOT DOES_ENTITY_EXIST(sDJ.djPedIndex)
		MODEL_NAMES pedModel = GET_DJ_PED_MODEL(sDJ.eCurrentDj)
		IF REQUEST_LOAD_MODEL(pedModel)
			
			GET_DJ_PED_COMPONENTS(sDJ.sDjPedData, sDJ.eCurrentDj)
			
			sDJ.djPedIndex = CREATE_PED(PEDTYPE_MISSION, pedModel, GET_DJ_COORDS(sDJ.eCurrentDj), GET_DJ_HEADING(sDJ.eCurrentDj), FALSE)
			APPLY_DJ_PED_STYLE_COMPONENTS(sDJ.djPedIndex, sDJ.sDjPedData)
			SET_CASINO_PED_PROPERTIES(sDJ.djPedIndex, sDJ.sDjPedData)
			SET_MODEL_AS_NO_LONGER_NEEDED(pedModel)
			CDEBUG1LN(DEBUG_NET_DJ, "[AM_MP_DJ] - DJ: ", GET_MODEL_NAME_FOR_DEBUG(pedModel), " created ")
			RETURN TRUE
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL CREATE_DJ_ANIM_PROPS()

	INT iPropIndex
	
	IF DOES_ENTITY_EXIST(sDJ.djPedIndex)
	AND NOT IS_ENTITY_DEAD(sDJ.djPedIndex)
		DJ_ANIM_DETAIL animDetail
		GET_DJ_ANIM_DATA(sDJ.eCurrentDj, sDJ.iCurrentSequence, animDetail)
		#IF IS_DEBUG_BUILD
		IF (ENUM_TO_INT(animDetail.objectModelName[iPropIndex]) != 0)
			CDEBUG1LN(DEBUG_NET_DJ, "[AM_MP_DJ] - CREATE_DJ_ANIM_PROPS: Attemping to create prop 1[", iPropIndex, "] = ", GET_MODEL_NAME_FOR_DEBUG(animDetail.objectModelName[iPropIndex]))
		ENDIF
		#ENDIF
		REPEAT MAX_ENTITIES_IN_SEQUENCE iPropIndex
			#IF IS_DEBUG_BUILD
			IF (ENUM_TO_INT(animDetail.objectModelName[iPropIndex]) != 0)
			CDEBUG1LN(DEBUG_NET_DJ, "[AM_MP_DJ] - CREATE_DJ_ANIM_PROPS: Attemping to create prop 2[", iPropIndex, "] = ", GET_MODEL_NAME_FOR_DEBUG(animDetail.objectModelName[iPropIndex]))
			ENDIF 
			#ENDIF
			IF IS_MODEL_IN_CDIMAGE(animDetail.objectModelName[iPropIndex])
				CDEBUG1LN(DEBUG_NET_DJ, "[AM_MP_DJ] - CREATE_DJ_ANIM_PROPS: Attemping to create prop [", iPropIndex, "] = ", GET_MODEL_NAME_FOR_DEBUG(animDetail.objectModelName[iPropIndex]), ", sAnimDict = ", animDetail.sEntityAnimDict[iPropIndex])
				IF NOT IS_STRING_NULL_OR_EMPTY(animDetail.sEntityAnimDict[iPropIndex])
					REQUEST_ANIM_DICT(animDetail.sEntityAnimDict[iPropIndex])
					CDEBUG1LN(DEBUG_NET_DJ, "[AM_MP_DJ] - CREATE_DJ_ANIM_PROPS: created prop [", iPropIndex, "] AnimDict Found: sAnimDict = ", animDetail.sEntityAnimDict[iPropIndex])
					IF HAS_ANIM_DICT_LOADED(animDetail.sEntityAnimDict[iPropIndex])
					AND REQUEST_LOAD_MODEL(animDetail.objectModelName[iPropIndex])

						IF NOT DOES_ENTITY_EXIST(sDJ.djPropIndex[iPropIndex])

							IF CAN_REGISTER_MISSION_ENTITIES(0,0,1,0)
								
								CDEBUG1LN(DEBUG_NET_DJ, "[AM_MP_DJ] - CREATE_DJ_ANIM_PROPS: created prop [", iPropIndex, "] = sAnimDict = ", animDetail.sEntityAnimDict[iPropIndex], ", sAnimClip: ", animDetail.sEntityAnimClip[iPropIndex], ", MODEL_NAME: ", GET_MODEL_NAME_FOR_DEBUG(animDetail.objectModelName[iPropIndex]))
								
								VECTOR tempPosition = GET_DJ_COORDS(sDJ.eCurrentDj)
								VECTOR tempRotation = <<0, 0, GET_DJ_HEADING(sDJ.eCurrentDj)>>
								
								GET_DJ_PROP_INTERACT_INITIAL_TRANSFORM(sDJ.eCurrentDj, tempPosition, tempRotation)
								
								IF VMAG(tempPosition) > 0.1	
									
									sDJ.djPropIndex[iPropIndex] = CREATE_OBJECT_NO_OFFSET(animDetail.objectModelName[iPropIndex], tempPosition, FALSE)
									SET_ENTITY_ROTATION(sDJ.djPropIndex[iPropIndex], tempRotation)
									
									SET_ENTITY_INVINCIBLE(sDJ.djPropIndex[iPropIndex], TRUE)
									SET_ENTITY_COLLISION(sDJ.djPropIndex[iPropIndex], FALSE)
									FREEZE_ENTITY_POSITION(sDJ.djPropIndex[iPropIndex], TRUE)
									
									CDEBUG1LN(DEBUG_NET_DJ, "[AM_MP_DJ] - CREATE_DJ_ANIM_PROPS: created prop [", iPropIndex, "] = ", GET_MODEL_NAME_FOR_DEBUG(animDetail.objectModelName[iPropIndex]), ", position = ", tempPosition)
								ELSE
									CDEBUG1LN(DEBUG_NET_DJ, "[AM_MP_DJ] - CREATE_DJ_ANIM_PROPS: prop creation position is 0")
									RETURN FALSE
								ENDIF
							ELSE
								CDEBUG1LN(DEBUG_NET_DJ, "[AM_MP_DJ] - CREATE_DJ_ANIM_PROPS: can't register entity")
								RETURN FALSE
							ENDIF
						
						ENDIF
					ELSE

						IF NOT REQUEST_LOAD_MODEL(animDetail.objectModelName[iPropIndex])
							CDEBUG1LN(DEBUG_NET_DJ, "[AM_MP_DJ] - CREATE_DJ_ANIM_PROPS: REQUEST_LOAD_MODEL NOT LOADED: ",  GET_MODEL_NAME_FOR_DEBUG(animDetail.objectModelName[iPropIndex]))
							
						ENDIF 
						CDEBUG1LN(DEBUG_NET_DJ, "[AM_MP_DJ] - CREATE_DJ_ANIM_PROPS: loading: ", animDetail.sEntityAnimDict[iPropIndex], " for entity [", iPropIndex)
						RETURN FALSE
					ENDIF
				ELSE

					CDEBUG1LN(DEBUG_NET_DJ, "[AM_MP_DJ] - CREATE_DJ_ANIM_PROPS: sAnimDict is empty, but model name does exist: FALSE")
				ENDIF
			ELSE
				IF DOES_ENTITY_EXIST(sDJ.djPropIndex[iPropIndex])
					DELETE_OBJECT(sDJ.djPropIndex[iPropIndex])
					CDEBUG1LN(DEBUG_NET_DJ, "CREATE_DJ_ANIM_PROPS: DELETE_ENTITY")
				ENDIF
				CDEBUG1LN(DEBUG_NET_DJ, "[AM_MP_DJ] - CREATE_DJ_ANIM_PROPS: IS_MODEL_IN_CDIMAGE: FALSE")
			ENDIF	
				

		ENDREPEAT
	ELSE
		CDEBUG1LN(DEBUG_NET_DJ, "[AM_MP_DJ] - CREATE_DJ_ANIM_PROPS: No ped associated with props, returning true")
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC MAINTAIN_DJ_INIT()	
	IF HAS_LOADED_ALL_DJ_ANIMS()
		IF CRAETE_DJ_PED()
			IF CREATE_DJ_ANIM_PROPS()
				SET_DJ_STATE(DJ_STATE_UPDATE)
			ENDIF	
		ENDIF
	ENDIF	
ENDPROC

PROC MAINTAIN_CURRENT_DJ_SEQUENCE_TO_MIX_TIME()
	#IF IS_DEBUG_BUILD
	IF sDebugDj.bPauseSyncScene
		EXIT
	ENDIF	
	#ENDIF
	
	IF sDj.iMixAnimCatchupTime > 0
		INT j = 0
		FLOAT fMixTime = sDj.iMixAnimCatchupTime / 1000.0
		TEXT_LABEL_63 tlClip
		tlClip = GET_DJ_ANIM_CLIP(sDj.eCurrentDj, sDj.iCurrentSequence)
		
		WHILE NOT IS_STRING_NULL_OR_EMPTY(tlClip)
		AND fMixTime > 0.0
		AND j < 50
			CDEBUG1LN(DEBUG_NET_DJ, "[AM_MP_DJ] - MAINTAIN_CURRENT_DJ_SEQUENCE_TO_MIX_TIME - Taking away ", tlClip, " from mix time ", fMixTime, " (sequence ", sDj.iCurrentSequence, ")")
			fMixTime -= GET_ANIM_DURATION(GET_DJ_ANIM_DICT(sDj.eCurrentDj), tlClip)
			sDj.iCurrentSequence++
			j++
			tlClip = GET_DJ_ANIM_CLIP(sDj.eCurrentDj, sDj.iCurrentSequence)
		ENDWHILE	
		
		sDj.iMixAnimCatchupTime = FLOOR(fMixTime * 1000.0)
		
		IF fMixTime <= 0
			ATTACH_DJ_HEADPHONES(sDj)
			sDj.iCurrentSequence--
			IF DOES_ENTITY_EXIST(sDJ.djPedIndex)
			AND NOT IS_ENTITY_DEAD(sDJ.djPedIndex)
				CLEAR_PED_TASKS(sDJ.djPedIndex)
			ENDIF
			SET_DJ_SYNC_SCENE_STARTED(FALSE)
			//sDj.bDoProcessEveryFrame[sDj.iDJPedID1] = TRUE
			
			sDj.iMixAnimTime = GET_CURRENT_MIX_TIME(sDj.iCurrentStationID) + sDj.iMixAnimCatchupTime
			SET_DJ_INITIAL_DJ_ANIM(TRUE)
		ENDIF			
		
	ENDIF
ENDPROC

PROC RUN_DJ_ANIM_SCENE_SEQUENCE()
	
	#IF IS_DEBUG_BUILD
	IF sDebugDj.bPauseSyncScene
		EXIT
	ENDIF	
	#ENDIF
	
	FLOAT fBlendIn = NORMAL_BLEND_IN
	FLOAT fBlendOut = NORMAL_BLEND_OUT
	
	DJ_ANIM_DETAIL animDetail
	GET_DJ_ANIM_DATA(sDJ.eCurrentDj, sDJ.iCurrentSequence, animDetail)
	
	#IF IS_DEBUG_BUILD
	INT iExpectedFramesUntilAnimEnds
	#ENDIF
	
	IF NOT IS_ENTITY_DEAD(sDJ.djPedIndex)
		
		// is the current anim about to finish? 
		IF IS_DJ_SYNC_SCENE_STARTED()
			
			#IF IS_DEBUG_BUILD
			iExpectedFramesUntilAnimEnds = EXPECTED_FRAMES_UNTIL_END_OF_ANIM(sDJ)
			#ENDIF
			
			BOOL bLastAnimBlend = FALSE  // last anim - ends when mix ends
			IF sDJ.iCurrentSequence = GET_DJ_ANIM_CLIP_COUNT(sDJ.eCurrentDj) - 1
				CPRINTLN(DEBUG_NET_DJ, "[AM_MP_DJ] - RUN_DJ_ANIM_SCENE_SEQUENCE: IS_DJ_ACTIVITY: bLastAnimBlend: GET_CURRENT_MIX_TIME = ", GET_CURRENT_MIX_TIME(sDJ.iCurrentStationID), ", GET_TOTAL_MIX_TIME = ", GET_TOTAL_MIX_TIME(sDJ.iCurrentStationID))
				IF GET_CURRENT_MIX_TIME(sDJ.iCurrentStationID) > GET_TOTAL_MIX_TIME(sDJ.iCurrentStationID)
					bLastAnimBlend = TRUE
					CPRINTLN(DEBUG_NET_DJ, "[AM_MP_DJ] - RUN_DJ_ANIM_SCENE_SEQUENCE: IS_DJ_ACTIVITY: bLastAnimBlend = TRUE")
				ENDIF
			ENDIF
			
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(sDJ.iSceneID)
			OR GET_SYNCHRONIZED_SCENE_PHASE(sDJ.iSceneID) >= 1
			OR bLastAnimBlend
				SET_DJ_SYNC_SCENE_STARTED(FALSE)					
				fBlendIn = INSTANT_BLEND_IN
				fBlendOut = INSTANT_BLEND_OUT
				
				IF bLastAnimBlend
					fBlendIn = 1.0
				ENDIF
				
				IF sDJ.iCurrentSequence = GET_DJ_ANIM_CLIP_COUNT(sDJ.eCurrentDj) - 1
					sDJ.iMixAnimTime = 0
				ENDIF
					
				sDJ.iCurrentSequence++
				IF sDJ.iCurrentSequence >= GET_DJ_ANIM_CLIP_COUNT(sDJ.eCurrentDj)
					sDJ.iCurrentSequence = 0
				ENDIF
				
				TEXT_LABEL_63 tl63HeadphoneClip = GET_DJ_HEADPHONES_ANIM_CLIP(sDJ.eCurrentDj, sDJ.iCurrentSequence)
				
				IF NOT IS_STRING_NULL_OR_EMPTY(tl63HeadphoneClip) 
					DETACH_DJ_HEADPHONES(sDJ)
				ENDIF
				#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_NET_DJ, "[AM_MP_DJ] - RUN_DJ_ANIM_SCENE_SEQUENCE: iExpectedFramesUntilAnimEnds: ", iExpectedFramesUntilAnimEnds)	
				#ENDIF
			ENDIF
	
		ENDIF

		IF NOT IS_DJ_SYNC_SCENE_STARTED()	
				
			IF sDJ.iCurrentSequence < 0
				sDJ.iCurrentSequence = 0
			ENDIF
			
			TEXT_LABEL_63 tl63HeadphoneClip = GET_DJ_HEADPHONES_ANIM_CLIP(sDJ.eCurrentDj, sDJ.iCurrentSequence)
			
			IF IS_STRING_NULL_OR_EMPTY(tl63HeadphoneClip) 
				ATTACH_DJ_HEADPHONES(sDJ)
			ELSE
				DETACH_DJ_HEADPHONES(sDJ)
			ENDIF
			CPRINTLN(DEBUG_NET_DJ, "[AM_MP_DJ] - RUN_DJ_ANIM_SCENE_SEQUENCE sDJ.iCurrentSequence = ", sDJ.iCurrentSequence)						

			CPRINTLN(DEBUG_NET_DJ, "[AM_MP_DJ] - RUN_DJ_ANIM_SCENE_SEQUENCE anim = ", animDetail.sPedAnimClip[0])
			
			IF NOT IS_STRING_NULL_OR_EMPTY(animDetail.sPedAnimDict[0])
				CDEBUG1LN(DEBUG_NET_DJ, "[AM_MP_DJ] - RUN_DJ_ANIM_SCENE_SEQUENCE: PLAYING_ANIM TASK_SYNCHRONIZED_SCENE:  Trying to start sync scene: bHasSyncSceneStarted: False: IS_STRING_NULL_OR_EMPTY: FALSE ")
				REQUEST_ANIM_DICT(animDetail.sPedAnimDict[0])
				IF HAS_ANIM_DICT_LOADED(animDetail.sPedAnimDict[0])
					CDEBUG1LN(DEBUG_NET_DJ, "[AM_MP_DJ] - RUN_DJ_ANIM_SCENE_SEQUENCE: PLAYING_ANIM TASK_SYNCHRONIZED_SCENE: iPedID:,  Trying to start sync scene: bHasSyncSceneStarted: False: IS_STRING_NULL_OR_EMPTY: FALSE: HAS_ANIM_DICT_LOADED: TRUE ")
					
					sDJ.iSceneID = CREATE_SYNCHRONIZED_SCENE(GET_DJ_ANIM_COORDS(sDJ.eCurrentDj), <<0, 0, GET_DJ_ANIM_HEADING(sDJ.eCurrentDj)>>, DEFAULT)
					
					FLOAT fPlayRate = 1.0
					
					INT i
					REPEAT MAX_PEDS_IN_SEQUENCE i 
						IF DOES_ENTITY_EXIST(sDJ.djPedIndex)
							IF NOT IS_ENTITY_DEAD(sDJ.djPedIndex)
							AND NOT IS_STRING_NULL_OR_EMPTY(animDetail.sPedAnimClip[i])

								TASK_SYNCHRONIZED_SCENE(sDJ.djPedIndex,sDJ.iSceneID, animDetail.sPedAnimDict[i], animDetail.sPedAnimClip[i], fBlendIn, fBlendOut, animDetail.flags[i].flags | SYNCED_SCENE_DONT_INTERRUPT, RBF_PLAYER_IMPACT, DEFAULT)
								CDEBUG1LN(DEBUG_NET_DJ, "[AM_MP_DJ] - RUN_DJ_ANIM_SCENE_SEQUENCE: PLAYING_ANIM TASK_SYNCHRONIZED_SCENE: ", i,", PedIndex: ", NATIVE_TO_INT(sDJ.djPedIndex), ", NetworkSceneID: ", sDJ.iSceneID, ", sAnimClip: ", animDetail.sPedAnimClip[i], ", sAnimDict: ", animDetail.sPedAnimDict[i])
								SET_RAGDOLL_BLOCKING_FLAGS(sDJ.djPedIndex, RBF_IMPACT_OBJECT) 
							
								
								CDEBUG1LN(DEBUG_NET_DJ, "[AM_MP_DJ] - RUN_DJ_ANIM_SCENE_SEQUENCE: CREATE_SYNCHRONIZED_SCENE: iMixAnimTime: ", sDJ.iMixAnimTime, " GET_CURRENT_MIX_TIME: ", GET_CURRENT_MIX_TIME(sDJ.iCurrentStationID), " - Difference of ", GET_CURRENT_MIX_TIME(sDJ.iCurrentStationID) - sDJ.iMixAnimTime)
								
								FLOAT fPredictedIdealNextDjSyncTime = (TO_FLOAT(GET_CURRENT_MIX_TIME(sDJ.iCurrentStationID)) / 1000.0) + GET_ANIM_DURATION(animDetail.sPedAnimDict[i], animDetail.sPedAnimClip[i])
								FLOAT fPredictedNextDjSyncAnimTime = fPredictedIdealNextDjSyncTime - (TO_FLOAT(sDJ.iMixAnimTime) / 1000.0)
								
								fPlayRate = fPredictedNextDjSyncAnimTime / GET_ANIM_DURATION(animDetail.sPedAnimDict[i], animDetail.sPedAnimClip[i])
							ELSE
								#IF IS_DEBUG_BUILD
								IF IS_ENTITY_DEAD(sDJ.djPedIndex)
									CDEBUG1LN(DEBUG_NET_DJ, "[AM_MP_DJ] - RUN_DJ_ANIM_SCENE_SEQUENCE: PedIndex ID: ", i," IS_ENTITY_DEAD = TRUE")
								ENDIF
								IF IS_STRING_NULL_OR_EMPTY(animDetail.sPedAnimClip[i])
									CDEBUG1LN(DEBUG_NET_DJ, "[AM_MP_DJ] - RUN_DJ_ANIM_SCENE_SEQUENCE: PedIndex ID: ", i," sAnimClip = NULL")
								ENDIF
								VECTOR vTemp 
								vTemp = GET_ENTITY_COORDS(sDJ.djPedIndex, FALSE)
								CDEBUG1LN(DEBUG_NET_DJ, "[AM_MP_DJ] - RUN_DJ_ANIM_SCENE_SEQUENCE: GET_ENTITY_COORDS: PedIndex ID: ", vTemp )
								
								#ENDIF
								
							ENDIF
						ELSE
							CDEBUG1LN(DEBUG_NET_DJ, "[AM_MP_DJ] - RUN_DJ_ANIM_SCENE_SEQUENCE: PedIndex ID: ", i," = NULL")
						ENDIF
					ENDREPEAT
					
					i = 0
					REPEAT MAX_ENTITIES_IN_SEQUENCE i 
						IF DOES_ENTITY_EXIST(sDJ.djPropIndex[i])
							IF NOT IS_STRING_NULL_OR_EMPTY(animDetail.sEntityAnimClip[i])
								CDEBUG1LN(DEBUG_NET_DJ, "[AM_MP_DJ] -  RUN_DJ_ANIM_SCENE_SEQUENCE i: ", i," ModelName: ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(sDJ.djPropIndex[i])), ", SceneID: ", sDJ.iSceneID, ", sAnimClip: ", animDetail.sEntityAnimClip[i], ", sAnimDict: ", animDetail.sEntityAnimDict[i])

								PLAY_SYNCHRONIZED_ENTITY_ANIM(sDJ.djPropIndex[i], sDJ.iSceneID, animDetail.sEntityAnimClip[i], animDetail.sEntityAnimDict[i], fBlendIn, fBlendOut, ENUM_TO_INT(animDetail.flags[i].flags))
								FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(sDJ.djPropIndex[i])
							ELSE
								CDEBUG1LN(DEBUG_NET_DJ, "[AM_MP_DJ] - RUN_DJ_ANIM_SCENE_SEQUENCE: sEntityAnimClip ", i," = NULL")
							ENDIF
						ENDIF
					ENDREPEAT
					
					SET_DJ_SYNC_SCENE_STARTED(TRUE)	
					
					FLOAT fDuration = GET_ANIM_DURATION(animDetail.sPedAnimDict[0], animDetail.sPedAnimClip[0])
					
					sDJ.iAnimDuration = FLOOR(fDuration * 1000.0)
					sDJ.iAnimStartTime = GET_GAME_TIMER()
					sDJ.iAnimOffset = 0			
					
					sDJ.iFacialAnimDuration = 0
					sDJ.iFacialAnimFrameDelay = 0
					
					IF fPlayRate > 1.2
						CDEBUG1LN(DEBUG_NET_DJ, "[AM_MP_DJ] - RUN_DJ_ANIM_SCENE_SEQUENCE: CREATE_SYNCHRONIZED_SCENE: fPlayRate is too large (", fPlayRate, "), capping at 1.2")
						fPlayRate = 1.2
					ELIF fPlayRate < 0.8
						CDEBUG1LN(DEBUG_NET_DJ, "[AM_MP_DJ] - RUN_DJ_ANIM_SCENE_SEQUENCE: CREATE_SYNCHRONIZED_SCENE: fPlayRate is too small (", fPlayRate, "), capping at 0.8")
						fPlayRate = 0.8
					ENDIF
					
					IF g_bNightClubRadioDelayNeeded
						fPlayRate = 0.0
						CDEBUG1LN(DEBUG_NET_DJ, "[AM_MP_DJ] - RUN_DJ_ANIM_SCENE_SEQUENCE: setting DJ anim rate to 0 to hold for music delay")
					ENDIF
					
					IF IS_DJ_INITIAL_DJ_ANIM()
						sDJ.iAnimOffset = GET_CURRENT_MIX_TIME(sDJ.iCurrentStationID) - sDJ.iMixAnimTime
						FLOAT fScenePhase = (TO_FLOAT(sDJ.iAnimOffset)/1000.0) / GET_ANIM_DURATION(animDetail.sPedAnimDict[0], animDetail.sPedAnimClip[0])
						fPlayRate = 1.0
						SET_SYNCHRONIZED_SCENE_PHASE(sDJ.iSceneID, fScenePhase)
						CDEBUG1LN(DEBUG_NET_DJ, "[AM_MP_DJ] - RUN_DJ_ANIM_SCENE_SEQUENCE: setting DJ anim start phase to ", fScenePhase," to match mix time")
					ENDIF
					
					#IF IS_DEBUG_BUILD 
						IF sDebugDj.fDebugSyncSpeed != -1.0
							fPlayRate = sDebugDj.fDebugSyncSpeed
						ENDIF
					#ENDIF

					SET_SYNCHRONIZED_SCENE_RATE(sDJ.iSceneID, fPlayRate)
					sDJ.fPhaseRate = fPlayRate
					CDEBUG1LN(DEBUG_NET_DJ, "[AM_MP_DJ] - RUN_DJ_ANIM_SCENE_SEQUENCE: setting anim rate to: ",sDJ.fPhaseRate)
						
					sDJ.iFacialAnimDuration = 0
					sDJ.iFacialAnimFrameDelay = 0
										
					CDEBUG1LN(DEBUG_NET_DJ, "[AM_MP_DJ] - RUN_DJ_ANIM_SCENE_SEQUENCE: CREATE_SYNCHRONIZED_SCENE: sDJ.iSceneID: ", sDJ.iSceneID)
				ELSE
					CDEBUG1LN(DEBUG_NET_DJ, "[AM_MP_DJ] - RUN_DJ_ANIM_SCENE_SEQUENCE: CREATE_SYNCHRONIZED_SCENE: not loaded yet")
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_NET_DJ, "[AM_MP_DJ] - RUN_DJ_ANIM_SCENE_SEQUENCE: CREATE_SYNCHRONIZED_SCENE: string is null or empty")
			ENDIF
				
		ELSE

			INT iLocalScene
			iLocalScene = sDJ.iSceneID
			
			#IF IS_DEBUG_BUILD
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sDJ.iSceneID)
				CDEBUG1LN(DEBUG_NET_DJ, "[AM_MP_DJ] - RUN_DJ_ANIM_SCENE_SEQUENCE: DJ_ACTIVITY: Current phase is ", GET_SYNCHRONIZED_SCENE_PHASE(iLocalScene))
			ENDIF
			#ENDIF
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(sDJ.iSceneID)
			OR GET_SYNCHRONIZED_SCENE_PHASE(iLocalScene) >= 1
				SET_DJ_SYNC_SCENE_STARTED(FALSE)
				sDJ.iCurrentSequence++
				
				IF sDJ.iCurrentSequence >= GET_DJ_ANIM_CLIP_COUNT(sDJ.eCurrentDj)
					sDJ.iCurrentSequence = 0
				ENDIF
				
				TEXT_LABEL_63 tl63HeadphoneClip = GET_DJ_HEADPHONES_ANIM_CLIP(sDJ.eCurrentDj, sDJ.iCurrentSequence)
				
				IF NOT IS_STRING_NULL_OR_EMPTY(tl63HeadphoneClip) 
					DETACH_DJ_HEADPHONES(sDJ)
				ENDIF
			ELSE
				//sync

				FLOAT fPredictedIdealNextDjSyncTime = (TO_FLOAT(GET_CURRENT_MIX_TIME(sDJ.iCurrentStationID)) / 1000.0) + (GET_ANIM_DURATION(animDetail.sPedAnimDict[0], animDetail.sPedAnimClip[0]) * (1.0 - GET_SYNCHRONIZED_SCENE_PHASE(iLocalScene)))
				FLOAT fPredictedNextDjSyncAnimTime = fPredictedIdealNextDjSyncTime - ((TO_FLOAT(sDJ.iMixAnimTime) / 1000.0) - (GET_ANIM_DURATION(animDetail.sPedAnimDict[0], animDetail.sPedAnimClip[0]) * (1.0 - GET_SYNCHRONIZED_SCENE_PHASE(iLocalScene))))
				
				CDEBUG1LN(DEBUG_NET_DJ, "RUN_DJ_ANIM_SCENE_SEQUENCE: DJ_ACTIVITY: iMixAnimTime (est): ", FLOOR(((TO_FLOAT(sDJ.iMixAnimTime) / 1000.0) - (GET_ANIM_DURATION(animDetail.sPedAnimDict[0]
					, animDetail.sPedAnimClip[0]) * (1.0 - GET_SYNCHRONIZED_SCENE_PHASE(iLocalScene)))) * 1000.0) 
					, " GET_CURRENT_MIX_TIME: ", GET_CURRENT_MIX_TIME(sDJ.iCurrentStationID), " - Difference of "
					, GET_CURRENT_MIX_TIME(sDJ.iCurrentStationID) - FLOOR(((TO_FLOAT(sDJ.iMixAnimTime) / 1000.0) - (GET_ANIM_DURATION(animDetail.sPedAnimDict[0], animDetail.sPedAnimClip[0]) * (1.0 - GET_SYNCHRONIZED_SCENE_PHASE(iLocalScene))))*1000.0)
					, ", fPredictedIdealNextDjSyncTime = ", fPredictedIdealNextDjSyncTime, ", fPredictedNextDjSyncAnimTime = ", fPredictedNextDjSyncAnimTime)
				
				FLOAT fPlayRate = fPredictedNextDjSyncAnimTime / (GET_ANIM_DURATION(animDetail.sPedAnimDict[0], animDetail.sPedAnimClip[0]) * (1.0 - GET_SYNCHRONIZED_SCENE_PHASE(iLocalScene)))
				
				IF fPlayRate > 1.2
					CDEBUG1LN(DEBUG_NET_DJ, "RUN_DJ_ANIM_SCENE_SEQUENCE: DJ_ACTIVITY: fPlayRate is too large (", fPlayRate, "), capping at 1.2")
					fPlayRate = 1.2
				ELIF fPlayRate < 0.8
					CDEBUG1LN(DEBUG_NET_DJ, "RUN_DJ_ANIM_SCENE_SEQUENCE: DJ_ACTIVITY: fPlayRate is too small (", fPlayRate, "), capping at 0.8")
					fPlayRate = 0.8
				ENDIF
				
				#IF IS_DEBUG_BUILD 
					IF sDebugDj.fDebugSyncSpeed != -1.0
						fPlayRate = sDebugDj.fDebugSyncSpeed
					ENDIF
				#ENDIF
				
				SET_SYNCHRONIZED_SCENE_RATE(sDJ.iSceneID, fPlayRate)
				sDJ.fPhaseRate = fPlayRate
				
//				CDEBUG1LN(DEBUG_NET_DJ, "RUN_DJ_ANIM_SCENE_SEQUENCE: setting anim rate to: ",sDJ.fPhaseRate)
			ENDIF
		ENDIF	

	ELSE
		CDEBUG1LN(DEBUG_NET_DJ, "[AM_MP_DJ] - RUN_DJ_ANIM_SCENE_SEQUENCE: PLAYING_ANIM TASK_SYNCHRONIZED_SCENE:  entity is dead or sDJ.eActiveActivity = Null")
	ENDIF
	
ENDPROC

PROC RUN_DJ_FACIAL_ANIM()
	IF DOES_ENTITY_EXIST(sDJ.djPedIndex)
		IF NOT IS_ENTITY_DEAD(sDJ.djPedIndex)
			IF FACIAL_ANIM_TIME_REMAINING(sDJ) < 100
				IF (sDJ.iFacialAnimFrameDelay <= 0)
					GIVE_DJ_FACIAL_ANIMS(sDJ)
				ELSE
					sDJ.iFacialAnimFrameDelay--
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_DJ_UPDATE()	
	RUN_DJ_ANIM_SCENE_SEQUENCE()
	RUN_DJ_FACIAL_ANIM()
ENDPROC

PROC MAINTAIN_DJ_CLEANUP()	

ENDPROC

PROC RUN_MAIN_CLIENT_LOGIC()
	IF IS_DJ_STATE(DJ_STATE_IDLE)
		MAINTAIN_DJ_IDLE()	
	ELIF IS_DJ_STATE(DJ_STATE_INIT)
		MAINTAIN_DJ_INIT()
	ELIF IS_DJ_STATE(DJ_STATE_UPDATE)
		MAINTAIN_DJ_UPDATE()
	ELIF IS_DJ_STATE(DJ_STATE_CLEANUP)
		MAINTAIN_DJ_CLEANUP()
	ENDIF
	
	MAINTAIN_CURRENT_DJ_SEQUENCE_TO_MIX_TIME()
ENDPROC

PROC RUN_MAIN_SERVER_LOGIC()

ENDPROC

#ENDIF

SCRIPT #IF FEATURE_CASINO (MP_MISSION_DATA scriptData) #ENDIF
	
	#IF FEATURE_CASINO
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		SCRIPT_INITIALISE(scriptData)		
	ELSE
		SCRIPT_CLEANUP()
	ENDIF
	
	WHILE TRUE
		MP_LOOP_WAIT_ZERO()
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		SCRIPT_PROFILER_START_OF_FRAME() 
		#ENDIF
		#ENDIF
		
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			PRINTLN("[AM_MP_DJ] - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE = TRUE")
			SCRIPT_CLEANUP()
		ENDIF
		
		IF SHOULD_KILL_THIS_SCRIPT()
			PRINTLN("[AM_MP_DJ] - SHOULD_KILL_THIS_SCRIPT = TRUE")
			SCRIPT_CLEANUP()
		ENDIF
		
		RUN_MAIN_CLIENT_LOGIC()

		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			RUN_MAIN_SERVER_LOGIC()
		ENDIF
		
		#IF IS_DEBUG_BUILD
		UPDATE_DEBUG_WIDGETS()
		
		#IF SCRIPT_PROFILER_ACTIVE
		SCRIPT_PROFILER_END_OF_FRAME() 
		#ENDIF
		#ENDIF
		
	ENDWHILE	
	
#ENDIF

ENDSCRIPT