/* ===================================================================================== *\
		MISSION NAME	:	GB_CASINO_HEIST_PLANNING.sc
		AUTHOR			:	Steve Tiley - 14/08/2019
		DESCRIPTION		:	Source script for the casino heist planning boards
\* ===================================================================================== */

USING "GB_CASINO_HEIST_PLANNING_CORE.sch"

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════════╡    SCRIPT    ╞═════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

SCRIPT

	#IF FEATURE_CASINO_HEIST

	IF NOT PROCESS_PRE_GAME(g_sCasinoHeistData)
		SCRIPT_CLEANUP()
	ENDIF
	
	#IF IS_DEBUG_BUILD
	CREATE_CASINO_HEIST_PLANNING_WIDGETS(g_sCasinoHeistData)
	#ENDIF	// IS_DEBUG_BUILD

	WHILE TRUE
	
		WAIT(0)
		
		#IF IS_DEBUG_BUILD
		MAINTAIN_CASINO_HEIST_PLANNING_WIDGETS(g_sCasinoHeistData)
		#ENDIF	// IS_DEBUG_BUILD
		
		PROCESS_CASINO_HEIST_PLANNING(g_sCasinoHeistData)
		 
	ENDWHILE
	
	#ENDIF	// FEATURE_CASINO_HEIST

ENDSCRIPT

