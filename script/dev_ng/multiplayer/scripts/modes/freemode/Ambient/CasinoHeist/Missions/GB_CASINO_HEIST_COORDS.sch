
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GB_CASINO_HEIST_COORDS.sch																							//
// Contains all mission variation data																					//
// Written by:  Martin McMillan & Ryan Elliott.																			//
// Date: 		23/07/2019																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#IF FEATURE_CASINO_HEIST

USING "globals.sch"
USING "net_prints.sch"
USING "net_include.sch"
USING "commands_misc.sch"
USING "net_casino_heist_peds_data.sch"

FUNC MODEL_NAMES DRONE_MODEL()
	RETURN INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Casino_Drone_02a"))
ENDFUNC

FUNC MODEL_NAMES DRONE_MODEL_BROKEN()
	RETURN INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Casino_Drone_broken01a"))
ENDFUNC

FUNC CSH_ITEM_TYPE GET_CSH_ITEM_TYPE_FROM_VARIATION(CSH_VARIATION eVariation)

	SWITCH eVariation
		CASE CHV_ARCADE_CABINETS			RETURN CIT_ARCADE_CABINETS
		CASE CHV_SCOPE_OUT_CASINO			RETURN CIT_ACCESS_POINTS
		CASE CHV_VAULT_CONTENTS				RETURN CIT_VAULT_CONTENTS
		CASE CHV_SECURITY_CAMERAS			RETURN CIT_CAMERA_LOCATIONS
		cASE CHV_GUARD_PATROL_ROUTES		RETURN CIT_PATROL_ROUTES
		CASE CHV_DISRUPT_SHIPMENTS			RETURN CIT_SHIPMENTS
		CASE CHV_PLASTIC_EXPLOSIVES			RETURN CIT_PLASTIC_EXPLOSIVES
		CASE CHV_STEALTH_OUTFITS			RETURN CIT_STEALTH_OUTFIT
		CASE CHV_DRONES						RETURN CIT_DRONE_PARTS
		CASE CHV_STEAL_EMP					RETURN CIT_EMP
		
		CASE CHV_ARMORED_EQUIPMENT_1
		CASE CHV_ARMORED_EQUIPMENT_2		
			RETURN CIT_ARMORED_OUTFIT
		
		CASE CHV_EXPLOSIVES_1
		CASE CHV_EXPLOSIVES_2				
			RETURN CIT_EXPLOSIVES
			
		CASE CHV_VAULT_KEY_CARDS_1
		CASE CHV_VAULT_KEY_CARDS_2			
			RETURN CIT_KEY_CARDS
		
		CASE CHV_ELECTRIC_DRILLS_1
		CASE CHV_ELECTRIC_DRILLS_2			
			RETURN CIT_ELECTRIC_DRILLS
	
		CASE CHV_VAULT_DRILL_1
		CASE CHV_VAULT_DRILL_2				
			RETURN CIT_VAULT_DRILLS
			
		CASE CHV_SEWER_TUNNEL_DRILL_1
		CASE CHV_SEWER_TUNNEL_DRILL_2		
			RETURN CIT_SEWER_TUNNEL_DRILL
			
		CASE CHV_VAULT_LASER_1
		CASE CHV_VAULT_LASER_2				
			RETURN CIT_LASER_EQUIPMENT
	
		CASE CHV_FIREFIGHTER_OUTFIT_1
		CASE CHV_FIREFIGHTER_OUTFIT_2		
			RETURN CIT_FIREFIGHTER_OUTFIT
			
		CASE CHV_MAINTENANCE_OUTFITS_1		RETURN CIT_MAINTENANCE_OUTFIT
		CASE CHV_MAINTENANCE_OUTFITS_2		RETURN CIT_CHEMICALS
		
		CASE CHV_BUGSTAR_OUTFITS_1			RETURN CIT_BUGSTAR_OUTFIT
		CASE CHV_BUGSTAR_OUTFITS_2			RETURN CIT_TRASHMASTER
			
		CASE CHV_GRUPPE_SECHS_VAN_1			RETURN CIT_GRUPPE_SECHS_VAN
		CASE CHV_GRUPPE_SECHS_VAN_2			RETURN CIT_GRUPPE_SECHS_OUTFIT
		
		CASE CHV_NOOSE_OUTFITS		
			RETURN CIT_NOOSE_OUTFIT
			
		CASE CHV_INSIDE_MAID
			RETURN CIT_ACCESS_CARD_LEVEL_1
		
		CASE CHV_INSIDE_VALET
		CASE CHV_INSIDE_CASHIER
			RETURN CIT_ACCESS_CARD_LEVEL_2
											
		CASE CHV_WEAPONS_SMUGGLERS
		CASE CHV_WEAPONS_STASH
		CASE CHV_UNMARKED_WEAPONS
		CASE CHV_RIOT_VAN
		CASE CHV_ZANCUDO_SHIPMENT
		CASE CHV_FLIGHT_SCHEDULE
			RETURN CIT_WEAPONS	
			
		CASE CHV_POLICE_AUCTION
		CASE CHV_CARGO_SHIP
		CASE CHV_DRAG_RACE
		CASE CHV_VALET_SERVICE
		CASE CHV_LOST_MC_BIKES
		CASE CHV_MERRYWEATHER_CONVOY
		CASE CHV_MERRYWEATHER_TEST_SITE
			RETURN CIT_VEHICLES
			
		CASE CHV_SERVER_FARM
		CASE CHV_FIB_RAID
			RETURN CIT_HACKING_DEVICE
			
		CASE CHV_CELEB_AFTER_PARTY
			RETURN CIT_CELEB_CAMERAS
		
		CASE CHV_CELEB_DISPOSE_OF_CAR
			RETURN CIT_CELEB_VEHICLE
	ENDSWITCH
	
	RETURN CIT_INVALID

ENDFUNC

////////////////////////////////////
/// MISSION ENTITY SPAWN COORDS ///
////////////////////////////////////

STRUCT CSH_MISSION_ENTITY_DATA

	MODEL_NAMES model
	VECTOR vCoords
	FLOAT fHeading
	VECTOR vRotation

ENDSTRUCT

PROC GET_UNMARKED_WEAPONS_MISSION_ENTITY_DATA(CSH_SUBVARIATION eSubvariation, INT iSpawn, MODEL_NAMES &model, VECTOR &vCoords, FLOAT &fHeading, VECTOR &vRotate)

	SWITCH eSubvariation
		CASE CHS_UW_ELYSIAN_ISLAND
			SWITCH iSpawn				
				CASE 0			
					model = 		prop_box_guncase_03a
					vCoords = 		<<480.8190, -2912.8940, 6.2920>>
					fHeading = 		91.794
					vRotate = 		<<-89.9560, -91.7940, 0.0000>>
				BREAK			
				CASE 1			
					model = 		prop_box_guncase_03a
					vCoords = 		<<427.3250, -2916.4141, 6.9420>>
					fHeading = 		353.596
					vRotate = 		<<-89.3900, 0.0000, -6.4040>>
				BREAK			
				CASE 2			
					model = 		prop_box_guncase_03a
					vCoords = 		<<478.7620, -2924.1589, 6.7560>>
					fHeading = 		0
					vRotate = 		<<-90.0000, 6.4040, 0.0000>>
				BREAK			
				CASE 3			
					model = 		prop_box_guncase_03a
					vCoords = 		<<517.3520, -2913.0659, 6.9410>>
					fHeading = 		89
					vRotate = 		<<-89.9800, -73.2880, 0.0000>>
				BREAK			
				CASE 4			
					model = 		prop_box_guncase_03a
					vCoords = 		<<534.9936, -2908.8936, 6.2940>>
					fHeading = 		360
					vRotate = 		<<15.3650, 90.0120, 0.0000>>
				BREAK			
				CASE 5			
					model = 		prop_box_guncase_03a
					vCoords = 		<<481.5440, -2904.9419, 6.2810>>
					fHeading = 		180.277
					vRotate = 		<<54.6070, 90.0690, -179.7230>>
				BREAK			
				CASE 6			
					model = 		prop_box_guncase_03a
					vCoords = 		<<547.7620, -2918.9209, 6.3520>>
					fHeading = 		311.42
					vRotate = 		<<40.4700, -77.4140, -48.5800>>
				BREAK			
				CASE 7			
					model = 		prop_box_guncase_03a
					vCoords = 		<<434.1670, -2904.6350, 9.3010>>
					fHeading = 		30.739
					vRotate = 		<<89.9720, 30.7390, 0.0000>>
				BREAK			
			ENDSWITCH				
		BREAK
		CASE CHS_UW_GRAPESEED
			SWITCH iSpawn				
				CASE 0			
					model = 		prop_box_guncase_03a
					vCoords = 		<<2293.2671, 4868.3560, 41.8810>>
					fHeading = 		332.596
					vRotate = 		<<-90.0000, 27.4040, 0.0000>>
				BREAK			
				CASE 1			
					model = 		prop_box_guncase_03a
					vCoords = 		<<2333.8899, 4857.3540, 41.6460>>
					fHeading = 		120.7
					vRotate = 		<<90.0000, 120.7000, 0.0000>>
				BREAK			
				CASE 2			
					model = 		prop_box_guncase_03a
					vCoords = 		<<2339.5061, 4893.0088, 41.3990>>
					fHeading = 		261.5
					vRotate = 		<<-18.0000, -90.0000, -98.5000>>
				BREAK			
				CASE 3			
					model = 		prop_box_guncase_03a
					vCoords = 		<<2315.4460, 4858.1948, 42.1430>>
					fHeading = 		133.8
					vRotate = 		<<-90.0000, -133.8000, 0.0000>>
				BREAK			
				CASE 4			
					model = 		prop_box_guncase_03a
					vCoords = 		<<2356.8601, 4876.5791, 42.0080>>
					fHeading = 		75.6
					vRotate = 		<<-90.0000, -75.6000, 0.0000>>
				BREAK			
				CASE 5			
					model = 		prop_box_guncase_03a
					vCoords = 		<<2325.9470, 4901.5708, 41.8340>>
					fHeading = 		329.4
					vRotate = 		<<90.0000, -30.6000, 0.0000>>
				BREAK			
				CASE 6			
					model = 		prop_box_guncase_03a
					vCoords = 		<<2291.6331, 4876.2358, 41.3830>>
					fHeading = 		300.7
					vRotate = 		<<-41.9000, 69.0000, 120.7000>>
				BREAK			
				CASE 7			
					model = 		prop_box_guncase_03a
					vCoords = 		<<2320.8069, 4839.9312, 41.3760>>
					fHeading = 		261.5
					vRotate = 		<<-42.1000, -90.0000, -98.5000>>
				BREAK			
			ENDSWITCH				
		BREAK
		CASE CHS_UW_STONER
			SWITCH iSpawn				
				CASE 0			
					model = 		prop_box_guncase_03a
					vCoords = 		<<287.62, 2823.27, 44.0250>>
					fHeading = 		280.3
					vRotate = 		<<90.0000, 280.3000, 0.0000>>
				BREAK			
				CASE 1			
					model = 		prop_box_guncase_03a
					vCoords = 		<<289.2560, 2854.8110, 43.8470>>
					fHeading = 		81.2
					vRotate = 		<<-90.0000, -81.2000, 0.0000>>
				BREAK			
				CASE 2			
					model = 		prop_box_guncase_03a
					vCoords = 		<<334.5600, 2862.4619, 43.6400>>
					fHeading = 		332.8
					vRotate = 		<<90.0000, -27.2000, 0.0000>>
				BREAK			
				CASE 3			
					model = 		prop_box_guncase_03a
					vCoords = 		<<296.3470, 2882.5081, 43.1730>>
					fHeading = 		13.6
					vRotate = 		<<18.5000, 89.7000, 13.6000>>
				BREAK			
				CASE 4			
					model = 		prop_box_guncase_03a
					vCoords = 		<<256.6110, 2874.0371, 43.2790>>
					fHeading = 		18
					vRotate = 		<<26.1000, -89.0000, 18.0000>>
				BREAK			
				CASE 5			
					model = 		prop_box_guncase_03a
					vCoords = 		<<315.3710, 2892.2429, 46.3060>>
					fHeading = 		291
					vRotate = 		<<90.0000, -69.0000, 0.0000>>
				BREAK			
				CASE 6			
					model = 		prop_box_guncase_03a
					vCoords = 		<<318.0300, 2886.0850, 46.0060>>
					fHeading = 		33.101
					vRotate = 		<<33.3500, -89.9200, 33.1010>>
				BREAK			
				CASE 7			
					model = 		prop_box_guncase_03a
					vCoords = 		<<321.4610, 2869.4089, 44.1100>>
					fHeading = 		332.301
					vRotate = 		<<90.0000, -27.6990, 0.0000>>
				BREAK			
			ENDSWITCH				
		BREAK
	ENDSWITCH

ENDPROC

PROC GET_EXPLOSIVES_1_MISSION_ENTITY_DATA(CSH_SUBVARIATION eSubvariation, INT iSpawn, MODEL_NAMES &model, VECTOR &vCoords, FLOAT &fHeading, VECTOR &vRotate)
	SWITCH eSubvariation
		CASE CHS_EXP1_VARIATION_1
			SWITCH iSpawn				
				CASE 0			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Box_Ammo01b"))
					vCoords = 		<<1747.7081,3874.1702,33.5753>>
					fHeading = 		331.398
					vRotate = 		<<0.0000, 0.0000, -28.6020>>
				BREAK			
				CASE 1			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Box_Ammo01b"))
					vCoords = 		<<1738.6536,3866.4636,33.6493>>
					fHeading = 		64.3966
					vRotate = 		<<0.0000, 0.0000, 64.3966>>
				BREAK			
				CASE 2			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Box_Ammo01b"))
					vCoords = 		<<1742.5686,3860.1018,33.5884>>
					fHeading = 		13.9966
					vRotate = 		<<0.0000, 0.0000, 13.9966>>
				BREAK			
				CASE 3			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Box_Ammo01b"))
					vCoords = 		<<1752.2300,3867.2380,33.5190>>
					fHeading = 		284.197
					vRotate = 		<<0.0000, 0.0000, -75.8030>>
				BREAK			
			ENDSWITCH				
		BREAK
		
		CASE CHS_EXP1_VARIATION_2
			SWITCH iSpawn				
				CASE 0			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Box_Ammo01b"))
					vCoords = 		<<-29.2197,3023.1792,39.8750>>
					fHeading = 		51.9998
					vRotate = 		<<0.0000, 0.0000, 51.9998>>
				BREAK			
				CASE 1			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Box_Ammo01b"))
					vCoords = 		<<-47.3488,3031.6733,39.6260>>
					fHeading = 		251.0001
					vRotate = 		<<2.0000, 2.0000, -108.9999>>
				BREAK			
				CASE 2			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Box_Ammo01b"))
					vCoords = 		<<-35.8691,3031.8647,40.0210>>
					fHeading = 		237.6001
					vRotate = 		<<0.0000, -0.0000, -122.3999>>
				BREAK			
				CASE 3			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Box_Ammo01b"))
					vCoords = 		<<-43.2261,3022.1003,39.6410>>
					fHeading = 		137.9987
					vRotate = 		<<0.0000, -1.1001, 137.9987>>
				BREAK			
			ENDSWITCH				
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_EXPLOSIVES_2_MISSION_ENTITY_DATA(CSH_SUBVARIATION eSubvariation, INT iSpawn, MODEL_NAMES &model, VECTOR &vCoords, FLOAT &fHeading, VECTOR &vRotate)
	SWITCH eSubvariation
		CASE CHS_EXP2_VARIATION_1
		CASE CHS_EXP2_VARIATION_2
			SWITCH iSpawn
				CASE 0
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Box_Ammo01b"))
					vCoords = 		<<1017.3050, -3106.1079, -38.8430>>
					fHeading = 		348.193
					vRotate = 		<<0.0000, 0.0000, -11.8070>>
				BREAK			
				CASE 1			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Box_Ammo01b"))
					vCoords = 		<<1018.0090, -3095.9209, -38.4900>>
					fHeading = 		251.393
					vRotate = 		<<0.0000, 0.0000, -108.6070>>
				BREAK			
				CASE 2			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Box_Ammo01b"))
					vCoords = 		<<1026.7052, -3096.3730, -37.791>>
					fHeading = 		235.1929
					vRotate = 		<<0.0000, -0.0000, -124.8070>>
				BREAK			
				CASE 3			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Box_Ammo01b"))
					vCoords = 		<<1026.1426, -3108.9038, -37.791>>
					fHeading = 		279.1929
					vRotate = 		<<0.0000, 0.0000, -80.8071>>
				BREAK			
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_SERVER_FARM_MISSION_ENTITY_DATA(CSH_SUBVARIATION eSubvariation, INT iSpawn, MODEL_NAMES &model, VECTOR &vCoords, FLOAT &fHeading, VECTOR &vRotate)
	SWITCH eSubvariation
		CASE CHS_SF_VARIATION_1
		CASE CHS_SF_VARIATION_2
		CASE CHS_SF_VARIATION_3
			SWITCH iSpawn				
				CASE 0			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_security_case_02a"))
					vCoords = 		<<2185.5295, 2911.9768, -84.9430>>
					fHeading = 		270.9997
					vRotate = 		<<90.0000, -89.0003, 0.0000>>
				BREAK			
				CASE 1			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_security_case_02a"))
					vCoords = 		<<2150.4197, 2911.7295, -84.6299>>
					fHeading = 		210.9999
					vRotate = 		<<0.0000, -0.0000, -149.0001>>
				BREAK			
				CASE 2			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_security_case_02a"))
					vCoords = 		<<1984.5964, 2911.9778, -85.3478>>
					fHeading = 		345.3999
					vRotate = 		<<0.0000, 0.0000, -14.6001>>
				BREAK			
				CASE 3			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_security_case_02a"))
					vCoords = 		<<2030.5782, 2938.0154, -85.3478>>
					fHeading = 		78.3999
					vRotate = 		<<0.0000, 0.0000, 78.3999>>
				BREAK			
				CASE 4			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_security_case_02a"))
					vCoords = 		<<2089.5078, 2896.1516, -85.3478>>
					fHeading = 		180.5995
					vRotate = 		<<0.0000, -0.0000, -179.4004>>
				BREAK			
				CASE 5			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_security_case_02a"))
					vCoords = 		<<2122.5537, 2936.0947, -84.6297>>
					fHeading = 		121.199
					vRotate = 		<<0.0000, -0.0000, 121.1990>>
				BREAK			
				CASE 6			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_security_case_02a"))
					vCoords = 		<<2349.4146, 2937.3745, -85.3478>>
					fHeading = 		292.9989
					vRotate = 		<<0.0000, 0.0000, -67.0011>>
				BREAK			
				CASE 7			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_security_case_02a"))
					vCoords = 		<<2310.1709, 2909.5703, -84.6287>>
					fHeading = 		305.9988
					vRotate = 		<<0.0000, 0.0000, -54.0012>>
				BREAK			
				CASE 8			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_security_case_02a"))
					vCoords = 		<<2238.6514, 2928.9297, -85.3478>>
					fHeading = 		49.5987
					vRotate = 		<<0.0000, 0.0000, 49.5987>>
				BREAK			
				CASE 9			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_security_case_02a"))
					vCoords = 		<<2278.9648, 2890.4932, -84.6325>>
					fHeading = 		43.599
					vRotate = 		<<0.0000, 0.0000, 43.5990>>
				BREAK			
			ENDSWITCH		
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_SERVER_FARM_PED_DATA(CSH_SUBVARIATION eSubvariation, INT iPed, INT iSpawn, VECTOR &vCoords, FLOAT &fHeading)
	SWITCH iPed				
		CASE 0			
			SWITCH eSubvariation
				CASE CHS_SF_VARIATION_1
					SWITCH iSpawn
						CASE 0
							vCoords = 		<<222.5657, -441.4509, 44.2469>>
							fHeading = 		161.3983
						BREAK
						
						CASE 1
							vCoords = 		<<216.3300, -446.5793, 43.3472>>
							fHeading = 		265.9967
						BREAK			
						
						CASE 2	
							vCoords = 		<<228.6967, -453.4553, 43.3472>>
							fHeading = 		173.7965
						BREAK			
					ENDSWITCH
				BREAK
				
				CASE CHS_SF_VARIATION_2			
					SWITCH iSpawn
						CASE 0
							vCoords = 		<<117.4721, -697.4313, 41.0289>>
							fHeading = 		355.1993
						BREAK
						
						CASE 1
							vCoords = 		<<118.8851, -680.8931, 41.0292>>
							fHeading = 		249.3983
						BREAK			
						
						CASE 2
							vCoords = 		<<133.2497, -683.8150, 41.0292>>
							fHeading = 		185.5983
						BREAK			
					ENDSWITCH
				BREAK			
				
				CASE CHS_SF_VARIATION_3			
					SWITCH iSpawn
						CASE 0
							vCoords = 		<<328.4309, -1584.5920, 31.7925>>
							fHeading = 		106.7999
						BREAK
						
						CASE 1
							vCoords = 		<<325.5886, -1591.0076, 31.5946>>
							fHeading = 		143.3998
						BREAK			
						
						CASE 2
							vCoords = 		<<308.2289, -1588.9790, 30.5322>>
							fHeading = 		305.3993
						BREAK			
					ENDSWITCH
				BREAK			
			ENDSWITCH
		BREAK			
	ENDSWITCH				
ENDPROC

PROC GET_ARMORED_EQUIPMENT_1_MISSION_ENTITY_DATA(CSH_SUBVARIATION eSubvariation, INT iSpawn, MODEL_NAMES &model, VECTOR &vCoords, FLOAT &fHeading, VECTOR &vRotate)
	SWITCH eSubvariation
		CASE CHS_AE1_VARIATION_1
		CASE CHS_AE1_VARIATION_2
			SWITCH iSpawn				
				CASE 0			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Adv_Case_Sm_Flash"))
					vCoords = 		<<934.0767, -3215.7764, -98.6817>>
					fHeading = 		178.5981
					vRotate = 		<<0.0000, -0.0000, 178.5980>>
				BREAK			
				CASE 1			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Adv_Case_Sm_Flash"))
					vCoords = 		<<894.4465, -3209.2930, -98.6157>>
					fHeading = 		190.9982
					vRotate = 		<<0.0000, -0.0000, -169.0018>>
				BREAK			
				CASE 2			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Adv_Case_Sm_Flash"))
					vCoords = 		<<946.1714, -3199.2393, -98.6858>>
					fHeading = 		182.198
					vRotate = 		<<0.0000, -0.0000, -177.8020>>
				BREAK			
				CASE 3			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Adv_Case_Sm_Flash"))
					vCoords = 		<<915.6430, -3219.4998, -98.6808>>
					fHeading = 		185.998
					vRotate = 		<<0.0000, -0.0000, -174.0019>>
				BREAK			
				CASE 4			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Adv_Case_Sm_Flash"))
					vCoords = 		<<899.2014, -3204.3066, -97.6074>>
					fHeading = 		204.1983
					vRotate = 		<<0.0000, -0.0000, -155.8017>>
				BREAK			
				CASE 5			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Adv_Case_Sm_Flash"))
					vCoords = 		<<908.4725, -3227.2925, -98.7118>>
					fHeading = 		214.3984
					vRotate = 		<<0.0000, -0.0000, -145.6015>>
				BREAK			
				CASE 6			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Adv_Case_Sm_Flash"))
					vCoords = 		<<891.0106, -3227.3552, -98.6485>>
					fHeading = 		291.7979
					vRotate = 		<<0.0000, 0.0000, -68.2021>>
				BREAK			
			ENDSWITCH				
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_FIB_RAID_MISSION_ENTITY_DATA(CSH_SUBVARIATION eSubvariation, INT iSpawn, MODEL_NAMES &model, VECTOR &vCoords, FLOAT &fHeading, VECTOR &vRotate)
	SWITCH eSubvariation
		CASE CHS_FIB_VARIATION_1
		CASE CHS_FIB_VARIATION_2
		CASE CHS_FIB_VARIATION_3
			SWITCH iSpawn				
				CASE 0			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_security_case_02a"))
					vCoords = 		<<125.0870, -731.8700, 242.0170>>
					fHeading = 		0
					vRotate = 		<<89.9800, 0.0000, 0.0000>>
				BREAK			
				CASE 1			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_security_case_02a"))
					vCoords = 		<<119.2130, -726.5220, 242.7180>>
					fHeading = 		42.2
					vRotate = 		<<90.0000, 42.2000, 0.0000>>
				BREAK			
				CASE 2			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_security_case_02a"))
					vCoords = 		<<112.9730, -733.4550, 242.4040>>
					fHeading = 		200.8
					vRotate = 		<<0.0000, 0.0000, -159.2000>>
				BREAK			
				CASE 3			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_security_case_02a"))
					vCoords = 		<<106.4050, -746.0030, 242.1070>>
					fHeading = 		107.8
					vRotate = 		<<89.9440, 107.8000, 0.0000>>
				BREAK			
				CASE 4			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_security_case_02a"))
					vCoords = 		<<107.2710, -753.0970, 242.0160>>
					fHeading = 		0
					vRotate = 		<<90.0000, 176.8830, 0.0000>>
				BREAK			
				CASE 5			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_security_case_02a"))
					vCoords = 		<<156.9870, -737.6050, 242.3210>>
					fHeading = 		108.2
					vRotate = 		<<0.0000, 0.0000, 108.2000>>
				BREAK			
				CASE 6			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_security_case_02a"))
					vCoords = 		<<153.3250, -744.4960, 242.0180>>
					fHeading = 		146.399
					vRotate = 		<<90.0000, 146.3990, 0.0000>>
				BREAK			
				CASE 7			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_security_case_02a"))
					vCoords = 		<<130.3230, -756.7250, 242.1410>>
					fHeading = 		302.2
					vRotate = 		<<90.0000, -57.8000, 0.0000>>
				BREAK			
				CASE 8			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_security_case_02a"))
					vCoords = 		<<151.0082, -750.5919, 241.6321>>
					fHeading = 		114.274
					vRotate = 		<<90.0000, 114.2740, 0.0000>>
				BREAK			
				CASE 9			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_security_case_02a"))
					vCoords = 		<<121.4861, -743.7041, 242.1051>>
					fHeading = 		0
					vRotate = 		<<89.9374, -140.0130, 0.0000>>
				BREAK			
			ENDSWITCH				
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_ELECTRIC_DRILLS_1_MISSION_ENTITY_DATA(CSH_SUBVARIATION eSubvariation, INT iSpawn, MODEL_NAMES &model, VECTOR &vCoords, FLOAT &fHeading, VECTOR &vRotate)
	model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_vault_drill_01a"))
	
	SWITCH eSubvariation
		CASE CHS_ED1_VARIATION_1
			SWITCH iSpawn				
				CASE 1			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_vault_drill_01a"))
					vCoords = 		<<-172.3737, -1070.9142, 41.1612>>
					fHeading = 		285.9998
					vRotate = 		<<-90.0000, 90.0000, -74.0003>>
				BREAK			
				CASE 2			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_vault_drill_01a"))
					vCoords = 		<<-161.4660, -1076.9758, 41.1442>>
					fHeading = 		3.5994
					vRotate = 		<<-90.0000, 90.0000, 3.5994>>
				BREAK			
				CASE 3			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_vault_drill_01a"))
					vCoords = 		<<-167.7487, -1084.0592, 41.1412>>
					fHeading = 		70.7994
					vRotate = 		<<-90.0000, 90.0000, 70.7994>>
				BREAK			
				CASE 4			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_vault_drill_01a"))
					vCoords = 		<<-157.8310, -1088.3921, 41.1410>>
					fHeading = 		35.1989
					vRotate = 		<<-90.0000, 90.0000, 35.1989>>
				BREAK			
				CASE 5			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_vault_drill_01a"))
					vCoords = 		<<-162.0758, -1066.5585, 41.1412>>
					fHeading = 		249.9989
					vRotate = 		<<-90.0000, 90.0000, -110.0011>>
				BREAK			
			ENDSWITCH
		BREAK
		CASE CHS_ED1_VARIATION_2
			SWITCH iSpawn
				CASE 1			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_vault_drill_01a"))
					vCoords = 		<<-1125.2562, -957.9663, 1.1521>>
					fHeading = 		325.6
					vRotate = 		<<-90.0000, 90.0000, -34.4000>>
				BREAK			
				CASE 2			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_vault_drill_01a"))
					vCoords = 		<<-1132.4006, -951.6277, 1.1520>>
					fHeading = 		33.7998
					vRotate = 		<<-90.0000, 90.0000, 33.7998>>
				BREAK			
				CASE 3			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_vault_drill_01a"))
					vCoords = 		<<-1125.1764, -969.5287, 1.1520>>
					fHeading = 		93.5998
					vRotate = 		<<-90.0000, 90.0000, 93.5998>>
				BREAK			
				CASE 4			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_vault_drill_01a"))
					vCoords = 		<<-1130.4806, -953.7077, 5.6341>>
					fHeading = 		209.9994
					vRotate = 		<<-90.0000, 90.0000, -150.0006>>
				BREAK			
				CASE 5			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_vault_drill_01a"))
					vCoords = 		<<-1123.7086, -971.9485, 5.6341>>
					fHeading = 		11.5993
					vRotate = 		<<-90.0000, 90.0000, 11.5993>>
				BREAK			
			ENDSWITCH
		BREAK
		CASE CHS_ED1_VARIATION_3
			SWITCH iSpawn
				CASE 1			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_vault_drill_01a"))
					vCoords = 		<<1294.6040, -741.8377, 64.5850>>
					fHeading = 		185.2002
					vRotate = 		<<-90.0000, 90.0000, -185.2002>>
				BREAK			
				CASE 2			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_vault_drill_01a"))
					vCoords = 		<<1292.0950, -752.6929, 64.5854>>
					fHeading = 		334.5996
					vRotate = 		<<-90.0000, 90.0000, -25.4004>>
				BREAK			
				CASE 3			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_vault_drill_01a"))
					vCoords = 		<<1322.7997, -755.9841, 65.4268>>
					fHeading = 		94.3994
					vRotate = 		<<-90.0000, 90.0000, 94.3994>>
				BREAK			
				CASE 4			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_vault_drill_01a"))
					vCoords = 		<<1315.2631, -762.5413, 65.4170>>
					fHeading = 		334.999
					vRotate = 		<<-90.0000, 90.0000, 334.999>>
				BREAK			
				CASE 5			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_vault_drill_01a"))
					vCoords = 		<<1297.9009, -751.7552, 64.5854>>
					fHeading = 		34.5993
					vRotate = 		<<-90.0000, 90.0000, 34.5993>>
				BREAK			
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

FUNC MODEL_NAMES ZANCUDO_SHIPMENT_ITEM_MODEL()
	RETURN INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Crate_Stack_01a"))
ENDFUNC

FUNC CSH_MISSION_ENTITY_DATA GET_CSH_MISSION_ENTITY_DATA(INT iMissionEntity, CSH_VARIATION eVariation, CSH_SUBVARIATION eSubvariation, INT iExtraParam, INT iExtraParam1)

	UNUSED_PARAMETER(iMissionEntity)
	UNUSED_PARAMETER(eSubvariation)
	
	CSH_MISSION_ENTITY_DATA data
	MODEL_NAMES model = DUMMY_MODEL_FOR_SCRIPT
	VECTOR vCoords = <<0.0, 0.0, 0.0>>
	FLOAT fHeading = 0.0
	VECTOR vRotate = <<0.0, 0.0, 0.0>>

	// If using a carrier vehicle, mission entity coords will be determined by the vehicle, otherwise add them here

	SWITCH eVariation
		CASE CHV_WEAPONS_STASH

			SWITCH iExtraParam
				CASE 0
					SWITCH iMissionEntity				
						CASE 0			
							model = 		prop_box_guncase_03a					
							vCoords = 		<<1113.5740, -3145.4751, -37.1840>>
							fHeading = 		279.396
							vRotate = 		<<90.0000, -16.4200, 0.0000>>
						BREAK						
						CASE 1			
							model = 		prop_box_guncase_03a					
							vCoords = 		<<1115.5995, -3163.8420, -36.8581>>
							fHeading = 		352.151
							vRotate = 		<<90.0000, -7.8490, 0.0000>>
						BREAK						
					ENDSWITCH				
				BREAK
				CASE 1
					SWITCH iMissionEntity							
						CASE 0			
							model = 		prop_box_guncase_03a					
							vCoords = 		<<1111.8740, -3142.9441, -37.4700>>
							fHeading = 		161.989
							vRotate = 		<<46.4810, -71.2320, -18.0110>>
						BREAK					
						CASE 1			
							model = 		prop_box_guncase_03a					
							vCoords = 		<<1117.7034, -3161.3135, -37.3284>>
							fHeading = 		90.269
							vRotate = 		<<-22.3602, 88.9390, 90.2690>>
						BREAK						
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH iMissionEntity				
						CASE 0			
							model = 		prop_box_guncase_03a					
							vCoords = 		<<1113.5740, -3145.4751, -37.1840>>
							fHeading = 		279.396
							vRotate = 		<<90.0000, -16.4200, 0.0000>>
						BREAK					
						CASE 1			
							model = 		prop_box_guncase_03a					
							vCoords = 		<<1122.1832, -3144.5803, -36.9850>>
							fHeading = 		81.988
							vRotate = 		<<90.0000, 81.9880, 0.0000>>
						BREAK			
					ENDSWITCH
				BREAK
				CASE 3
					SWITCH iMissionEntity							
						CASE 0			
							model = 		prop_box_guncase_03a					
							vCoords = 		<<1115.5995, -3163.8420, -36.8581>>
							fHeading = 		352.151
							vRotate = 		<<90.0000, -7.8490, 0.0000>>
						BREAK					
						CASE 1			
							model = 		prop_box_guncase_03a					
							vCoords = 		<<1122.1832, -3144.5803, -36.9850>>
							fHeading = 		81.988
							vRotate = 		<<90.0000, 81.9880, 0.0000>>
						BREAK			
					ENDSWITCH
				BREAK
				CASE 4
					SWITCH iMissionEntity						
						CASE 0			
							model = 		prop_box_guncase_03a					
							vCoords = 		<<1111.8740, -3142.9441, -37.4700>>
							fHeading = 		161.989
							vRotate = 		<<46.4810, -71.2320, -18.0110>>
						BREAK					
						CASE 1			
							model = 		prop_box_guncase_03a					
							vCoords = 		<<1117.7034, -3161.3135, -37.3284>>
							fHeading = 		90.269
							vRotate = 		<<-22.3602, 88.9390, 90.2690>>
						BREAK						
					ENDSWITCH
				BREAK
			ENDSWITCH

		BREAK
		CASE CHV_DRONES
			SWITCH eSubvariation
				CASE CHS_DRONES_VARIATION_1
					SWITCH iMissionEntity				
						CASE 0			
							model = 		DRONE_MODEL_BROKEN()
							vCoords = 		<<201.3850, -937.4080, 54.0090>>
							fHeading = 		0
//							vRotate = 		<<0.0000, 0.0000, 0.0000>>
						BREAK			
						CASE 1			
							model = 		DRONE_MODEL_BROKEN()
							vCoords = 		<<-1024.8800, -2710.2690, 31.6480>>
							fHeading = 		332.8
//							vRotate = 		<<0.0000, 0.0000, -27.2000>>
						BREAK			
						CASE 2			
							model = 		DRONE_MODEL_BROKEN()
							vCoords = 		<<-1604.1820, 167.6250, 76.6850>>
							fHeading = 		177.4
//							vRotate = 		<<0.0000, 0.0000, 177.4000>>
						BREAK			
						CASE 3			
							model = 		DRONE_MODEL_BROKEN()
							vCoords = 		<<-1794.4091, -1178.0050, 29.3880>>
							fHeading = 		137.6
//							vRotate = 		<<0.0000, 0.0000, 137.6000>>
						BREAK			
						CASE 4			
							model = 		DRONE_MODEL_BROKEN()
							vCoords = 		<<1145.9170, -643.9830, 73.2210>>
							fHeading = 		315.599
//							vRotate = 		<<0.0000, 0.0000, -44.4010>>
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_DRONES_VARIATION_2
					SWITCH iMissionEntity				
						CASE 0			
							model = 		DRONE_MODEL_BROKEN()
							vCoords = 		<<-1259.4860, -1480.4550, 21.3860>>
							fHeading = 		182.399
//							vRotate = 		<<0.0000, 0.0000, -177.6010>>
						BREAK			
						CASE 1			
							model = 		DRONE_MODEL_BROKEN()
							vCoords = 		<<-412.7140, 1170.1310, 342.4690>>
							fHeading = 		317.799
//							vRotate = 		<<0.0000, 0.0000, -42.2010>>
						BREAK			
						CASE 2		
							model = 		DRONE_MODEL_BROKEN()
							vCoords = 		<<-538.8170, -214.3400, 54.2180>>
							fHeading = 		28.799
//							vRotate = 		<<0.0000, 0.0000, 28.7990>>
						BREAK
						CASE 3			
							model = 		DRONE_MODEL_BROKEN()
							vCoords = 		<<-1293.9475, 83.6042, 67.8382>>
							fHeading = 		24.399
//							vRotate = 		<<0.0000, 0.0000, 24.3990>>
						BREAK					
						CASE 4			
							model = 		DRONE_MODEL_BROKEN()
							vCoords = 		<<103.1320, -1939.1030, 36.8620>>
							fHeading = 		225.999
//							vRotate = 		<<0.0000, 0.0000, -134.0010>>
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_DRONES_VARIATION_3
					SWITCH iMissionEntity				
						CASE 0			
							model = 		DRONE_MODEL_BROKEN()
							vCoords = 		<<-1228.3920, -776.0340, 34.4230>>
							fHeading = 		222.799
//							vRotate = 		<<0.0000, 0.0000, -137.2010>>
						BREAK			
						CASE 1			
							model = 		DRONE_MODEL_BROKEN()
							vCoords = 		<<-103.8640, 905.0740, 253.0510>>
							fHeading = 		213.799
//							vRotate = 		<<0.0000, 0.0000, -146.2010>>
						BREAK			
						CASE 2			
							model = 		DRONE_MODEL_BROKEN()
							vCoords = 		<<455.0340, -226.1110, 72.5590>>
							fHeading = 		251.799
//							vRotate = 		<<0.0000, 0.0000, -108.2010>>
						BREAK			
						CASE 3			
							model = 		DRONE_MODEL_BROKEN()
							vCoords = 		<<-263.7680, -1895.9709, 43.5770>>
							fHeading = 		170.599
//							vRotate = 		<<0.0000, 0.0000, 170.5990>>
						BREAK			
						CASE 4			
							model = 		DRONE_MODEL_BROKEN()	
							vCoords = 		<<1290.2841, -1728.5229, 69.8900>>
							fHeading = 		258.998
//							vRotate = 		<<0.0000, 0.0000, -101.0020>>
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
//		CASE CHV_INSIDE_MAID
//		
//			model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_swipe_card_01b"))
//		
//			SWITCH eSubvariation
//				CASE CHS_IMM_VARIATION_1
//					SWITCH iExtraParam				
//						CASE 0			
//							vCoords = 		<<258.3160, -996.7930, -99.5610>>
//							fHeading = 		295.599
//							vRotate = 		<<-89.9800, 35.3220, 0.0000>>
//						BREAK			
//						CASE 1			
//							vCoords = 		<<262.1110, -1000.3970, -99.2940>>
//							fHeading = 		196.8
//							vRotate = 		<<-89.9720, 163.2000, 0.0000>>
//						BREAK			
//						CASE 2			
//							vCoords = 		<<266.2230, -995.3060, -99.0400>>
//							fHeading = 		299.8
//							vRotate = 		<<-89.9600, 60.2000, 0.0000>>
//						BREAK			
//					ENDSWITCH			
//				BREAK
//				CASE CHS_IMM_VARIATION_2
//					SWITCH iExtraParam				
//						CASE 0			
//							vCoords = 		<<258.3160, -996.7930, -99.5610>>
//							fHeading = 		295.599
//							vRotate = 		<<-89.9800, 35.3220, 0.0000>>
//						BREAK			
//						CASE 1			
//							vCoords = 		<<262.1110, -1000.3970, -99.2940>>
//							fHeading = 		196.8
//							vRotate = 		<<-89.9720, 163.2000, 0.0000>>
//						BREAK			
//						CASE 2			
//							vCoords = 		<<266.2230, -995.3060, -99.0400>>
//							fHeading = 		299.8
//							vRotate = 		<<-89.9600, 60.2000, 0.0000>>
//						BREAK			
//					ENDSWITCH				
//				BREAK
//			ENDSWITCH
//		BREAK
		CASE CHV_MAINTENANCE_OUTFITS_1
			SWITCH eSubvariation
				CASE CHS_MO1_VARIATION_1
					SWITCH iMissionEntity				
						CASE 0			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_Ch_Bag_01a"))
							vCoords = 		<<-1562.7335, -3239.7422, 28.6410>>
							fHeading = 		72.5975
							vRotate = 		<<0.0000, 0.0000, 72.5975>>
						BREAK			
						CASE 1			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_Ch_Bag_01a"))
							vCoords = 		<<-1557.9861, -3232.4531, 25.3427>>
							fHeading = 		87.3978
							vRotate = 		<<0.0000, 0.0000, 87.3978>>
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_MO1_VARIATION_2
					SWITCH iMissionEntity				
						CASE 0			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_Ch_Bag_01a"))
							vCoords = 		<<-1165.5551,-233.6850,38.2670>>
							fHeading = 		71.198
							vRotate = 		<<0.0000, 0.0000, 71.1980>>
						BREAK			
						CASE 1			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_Ch_Bag_01a"))
							vCoords = 		<<-1162.8101,-230.8850,37.6900>>
							fHeading = 		33.998
							vRotate = 		<<0.0000, 0.0000, 33.9980>>
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_MO1_VARIATION_3
					SWITCH iMissionEntity				
						CASE 0			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_Ch_Bag_01a"))
							vCoords = 		<<-2185.0129, 4290.4580, 52.8160>>
							fHeading = 		308.799
							vRotate = 		<<0.0000, 0.0000, -51.2010>>
						BREAK			
						CASE 1			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_Ch_Bag_01a"))
							vCoords = 		<<-2175.3110,4290.9702,48.0550>>
							fHeading = 		281.599
							vRotate = 		<<0.0000, 0.0000, -78.4010>>
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_UNMARKED_WEAPONS
			SWITCH iMissionEntity
				CASE 0
					GET_UNMARKED_WEAPONS_MISSION_ENTITY_DATA(eSubvariation, iExtraParam, model, vCoords, fHeading, vRotate)
				BREAK
				CASE 1
					GET_UNMARKED_WEAPONS_MISSION_ENTITY_DATA(eSubvariation, iExtraParam1, model, vCoords, fHeading, vRotate)
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_EXPLOSIVES_1
			SWITCH iMissionEntity
				CASE 0
					GET_EXPLOSIVES_1_MISSION_ENTITY_DATA(eSubvariation, iExtraParam, model, vCoords, fHeading, vRotate)
				BREAK
				
				CASE 1
					GET_EXPLOSIVES_1_MISSION_ENTITY_DATA(eSubvariation, iExtraParam1, model, vCoords, fHeading, vRotate)
				BREAK
			ENDSWITCH
		BREAK		
		CASE CHV_FLIGHT_SCHEDULE
			model = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_case_sm_01x")) 
		BREAK
		CASE CHV_EXPLOSIVES_2
			SWITCH iMissionEntity
				CASE 0
					GET_EXPLOSIVES_2_MISSION_ENTITY_DATA(eSubvariation, iExtraParam, model, vCoords, fHeading, vRotate)
				BREAK
				
				CASE 1
					GET_EXPLOSIVES_2_MISSION_ENTITY_DATA(eSubvariation, iExtraParam1, model, vCoords, fHeading, vRotate)
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_VAULT_LASER_1
			SWITCH eSubvariation
				CASE CHS_VL1_GRAND_SENORA_DESERT
					SWITCH iMissionEntity				
						CASE 0			
							SWITCH iExtraParam
								CASE 0
									model = 		ch_prop_laserdrill_01a
									vCoords = 		<<1047.7788, 3191.7813, 38.8407>>
									fHeading = 		39.398
									vRotate = 		<<-2.0000, -0.0000, 39.3980>>
								BREAK
								CASE 1			
									model = 		ch_prop_laserdrill_01a
									vCoords = 		<<1019.2300, 3187.8379, 38.4260>>
									fHeading = 		149.198
									vRotate = 		<<0.0000, 0.0000, 149.1970>>
								BREAK	
							ENDSWITCH
						BREAK			
						CASE 1		
							SWITCH iExtraParam1
								CASE 0			
									model = 		ch_prop_LaserDrill_01a
									vCoords = 		<<1022.5890, 3240.4563, 37.8668>>
									fHeading = 		299.2918
									vRotate = 		<<0.0000, 0.0000, -60.7082>>
								BREAK			
								CASE 1			
									model = 		ch_prop_LaserDrill_01a
									vCoords = 		<<1014.9404, 3235.6550, 37.8723>>
									fHeading = 		127.0915
									vRotate = 		<<0.0000, -0.0000, 127.0915>>
								BREAK			
							ENDSWITCH
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_VL1_LA_MESA
					SWITCH iMissionEntity				
						CASE 0			
							SWITCH iExtraParam
								CASE 0			
									model = 		ch_prop_LaserDrill_01a
									vCoords = 		<<1254.3510, -2355.5623, 50.2240>>
									fHeading = 		253.7998
									vRotate = 		<<0.0000, 2.6620, -106.2002>>
								BREAK			
								CASE 1			
									model = 		ch_prop_LaserDrill_01a
									vCoords = 		<<1239.6251, -2371.5911, 49.4712>>
									fHeading = 		253.7998
									vRotate = 		<<0.0000, 2.0000, -106.2002>>
								BREAK			
							ENDSWITCH
						BREAK			
						CASE 1		
							SWITCH iExtraParam1
								CASE 0			
									model = 		ch_prop_LaserDrill_01a
									vCoords = 		<<1260.4644, -2327.2075, 50.4883>>
									fHeading = 		0
									vRotate = 		<<0.0000, 0.0000, 0.0000>>
								BREAK			
								CASE 1			
									model = 		ch_prop_LaserDrill_01a
									vCoords = 		<<1249.2369, -2396.0012, 47.7745>>
									fHeading = 		300
									vRotate = 		<<1.0000, 0.0000, -60.0000>>
								BREAK			
							ENDSWITCH
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_VAULT_LASER_2
			SWITCH eSubvariation
				CASE CHS_VL2_POWER_STATION
					SWITCH iMissionEntity				
						CASE 0			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_laserdrill_01a"))
							vCoords = 		<<1100.7090, -3199.3140, -39.0400>>
							fHeading = 		286.799
							vRotate = 		<<0.0000, 0.0000, -73.2010>>
						BREAK			
						CASE 1			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_laserdrill_01a"))
							vCoords = 		<<1095.1450, -3195.6741, -39.0390>>
							fHeading = 		125.199
							vRotate = 		<<0.0000, 0.0000, 125.1990>>
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_VL2_ZANCUDO
					SWITCH iMissionEntity				
						CASE 0			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_laserdrill_01a"))
							vCoords = 		<<1100.7090, -3199.3140, -39.0400>>
							fHeading = 		286.799
							vRotate = 		<<0.0000, 0.0000, -73.2010>>
						BREAK			
						CASE 1			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_laserdrill_01a"))
							vCoords = 		<<1095.1450, -3195.6741, -39.0390>>
							fHeading = 		125.199
							vRotate = 		<<0.0000, 0.0000, 125.1990>>
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_STEALTH_OUTFITS
			SWITCH eSubvariation
				CASE CHS_SO_VARIATION_1
					SWITCH iMissionEntity				
						CASE 0			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_duffbag_stealth_01a"))
							vCoords = 		<<3600.7290, 3665.9609, 33.8640>>
							fHeading = 		175.799
							vRotate = 		<<0.0000, 0.0000, 175.7990>>
						BREAK			
						CASE 1			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_duffbag_stealth_01a"))
							vCoords = 		<<3596.8701, 3658.1860, 34.6099>>
							fHeading = 		159.599
							vRotate = 		<<0.0000, 0.0000, 159.5990>>
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_SO_VARIATION_2
					SWITCH iMissionEntity				
						CASE 0			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_duffbag_stealth_01a"))
							vCoords = 		<<-1816.0530, 3095.6724, 32.7777>>
							fHeading = 		25.4
							vRotate = 		<<0.0000, 0.0000, 25.4000>>
						BREAK			
						CASE 1			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_duffbag_stealth_01a"))
							vCoords = 		<<-1796.9185, 3103.0933, 32.7779>>
							fHeading = 		194.1999
							vRotate = 		<<0.0000, -0.0000, -165.8001>>
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_PLASTIC_EXPLOSIVES
			SWITCH eSubvariation
				CASE CHS_PE_VARIATION_1
					SWITCH iMissionEntity				
						CASE 0			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Box_Ammo01a"))
							vCoords = 		<<4097.1362, 4731.7773, -92.7591>>
							fHeading = 		156.029
							vRotate = 		<<20.4840, -7.3208, 156.0290>>
						BREAK			
						CASE 1			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Box_Ammo01a"))
							vCoords = 		<<4117.0537, 4703.1616, -97.4039>>
							fHeading = 		313.843
							vRotate = 		<<-5.4060, 14.1710, -46.1570>>
						BREAK
					ENDSWITCH
				BREAK
				CASE CHS_PE_VARIATION_2
					SWITCH iMissionEntity				
						CASE 0			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Box_Ammo01a"))
							vCoords = 		<<-1863.5176, 5657.0244, -67.2069>>
							fHeading = 		206.654
							vRotate = 		<<0.0000, 23.0440, -153.3460>>
						BREAK			
						CASE 1			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Box_Ammo01a"))
							vCoords = 		<<-1821.9200, 5664.3081, -69.3060>>
							fHeading = 		136.054
							vRotate = 		<<-3.6980, 3.5660, 136.0540>>
						BREAK	
					ENDSWITCH
				BREAK
				CASE CHS_PE_VARIATION_3
					SWITCH iMissionEntity				
						CASE 0			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Box_Ammo01a"))
							vCoords = 		<<-91.1160, -2294.5518, -18.0786>>
							fHeading = 		58.319
							vRotate = 		<<0.0000, -17.9691, 58.3190>>
						BREAK			
						CASE 1			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Box_Ammo01a"))
							vCoords = 		<<-90.6527, -2323.3865, -21.0701>>
							fHeading = 		179.609
							vRotate = 		<<12.6590, 2.1650, 179.6090>>
						BREAK
					ENDSWITCH
				BREAK
				CASE CHS_PE_VARIATION_4
					SWITCH iMissionEntity				
						CASE 0			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Box_Ammo01a"))
							vCoords = 		<<354.6994, 3930.2710, -4.7065>>
							fHeading = 		111.597
							vRotate = 		<<4.0780, 0.9600, 111.5970>>
						BREAK			
						CASE 1			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Box_Ammo01a"))
							vCoords = 		<<341.0870, 3963.7661, -9.8240>>
							fHeading = 		202.597
							vRotate = 		<<-0.9000, 3.7000, -157.4030>>
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_VAULT_DRILL_2
			SWITCH eSubvariation
				CASE CHS_VD2_VARIATION_1
					SWITCH iMissionEntity				
						CASE 0			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_heist_drill_bag_01a"))
							vCoords = 		<<311.0890, -283.8570, 53.1730>>
							fHeading = 		38.598
							vRotate = 		<<0.0000, 0.0000, 38.5980>>
						BREAK			
						CASE 1			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_heist_drill_bag_01a"))
							vCoords = 		<<308.5820, -280.7390, 53.1640>>
							fHeading = 		60.598
							vRotate = 		<<0.0000, 0.0000, 60.5980>>
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_VD2_VARIATION_2
					SWITCH iMissionEntity				
						CASE 0			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_heist_drill_bag_01a"))
							vCoords = 		<<-2958.2991, 481.5510, 14.6820>>
							fHeading = 		48.198
							vRotate = 		<<0.0000, 0.0000, 48.1980>>
						BREAK			
						CASE 1			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_heist_drill_bag_01a"))
							vCoords = 		<<-2957.1260, 479.3210, 14.6870>>
							fHeading = 		68.798
							vRotate = 		<<0.0000, 0.0000, 68.7980>>
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_ELECTRIC_DRILLS_1
			SWITCH iMissionEntity				
				CASE 0			
					GET_ELECTRIC_DRILLS_1_MISSION_ENTITY_DATA(eSubvariation, iExtraParam, model, vCoords, fHeading, vRotate)
				BREAK			
				CASE 1			
					GET_ELECTRIC_DRILLS_1_MISSION_ENTITY_DATA(eSubvariation, iExtraParam1, model, vCoords, fHeading, vRotate)
				BREAK			
			ENDSWITCH
		BREAK
		CASE CHV_ELECTRIC_DRILLS_2
			model = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_vault_drill_01a"))
		BREAK
		CASE CHV_GRUPPE_SECHS_VAN_2
			model = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_duffbag_gruppe_01a"))
		BREAK
		CASE CHV_ARMORED_EQUIPMENT_1
			SWITCH iMissionEntity				
				CASE 0			
					GET_ARMORED_EQUIPMENT_1_MISSION_ENTITY_DATA(eSubvariation, iExtraParam, model, vCoords, fHeading, vRotate)
				BREAK			
				CASE 1			
					GET_ARMORED_EQUIPMENT_1_MISSION_ENTITY_DATA(eSubvariation, iExtraParam1, model, vCoords, fHeading, vRotate)
				BREAK			
			ENDSWITCH				
		BREAK
		CASE CHV_SERVER_FARM
			GET_SERVER_FARM_MISSION_ENTITY_DATA(eSubvariation, iExtraParam, model, vCoords, fHeading, vRotate)
		BREAK
		CASE CHV_VAULT_KEY_CARDS_2
			model = INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Vault_Key_Card_01a"))
			vRotate = 		<<-90.0, 0.0, 0.0>>
		BREAK
		CASE CHV_ARMORED_EQUIPMENT_2
			SWITCH iMissionEntity				
				CASE 0			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Adv_Case_Sm_Flash"))
					vCoords = 		<<3550.8044,3657.6511,28.5024>>
					fHeading = 		169.894
					vRotate = 		<<0.0000, -0.0000, 169.8940>>
				BREAK			
				CASE 1			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Adv_Case_Sm_Flash"))
					vCoords = 		<<3548.2783,3652.4775,28.5024>>
					fHeading = 		259.2938
					vRotate = 		<<0.0000, -0.0000, -100.7062>>
				BREAK			
			ENDSWITCH				
		BREAK
		CASE CHV_RIOT_VAN
		CASE CHV_WEAPONS_SMUGGLERS
		CASE CHV_ZANCUDO_SHIPMENT
			model = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_case_sm_01x"))
		BREAK
		CASE CHV_FIB_RAID
			GET_FIB_RAID_MISSION_ENTITY_DATA(eSubvariation, iExtraParam, model, vCoords, fHeading, vRotate)
		BREAK
//		CASE CHV_INSIDE_CASHIER
//			// Find item instead
//		BREAK
		CASE CHV_NOOSE_OUTFITS
			SWITCH eSubvariation
				CASE CHS_NOOSE_VARIATION_1
					SWITCH iMissionEntity				
						CASE 0			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_Ch_Bag_02a"))
							vCoords = 		<<458.9858, -993.8356, 30.1388>>
							fHeading = 		8.5956
							vRotate = 		<<0.0000, 0.0000, 8.5956>>
						BREAK			
						CASE 1			
							SWITCH iExtraParam
								CASE 0		
									model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_Ch_Bag_02a"))
									vCoords = 		<<459.9609, -979.5379, 30.2762>>
									fHeading = 		357.196
									vRotate = 		<<0.0000, 0.0000, -2.8040>>
						BREAK			
								CASE 1			
									model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_Ch_Bag_02a"))
									vCoords = 		<<438.0832, -995.6683, 30.5863>>
									fHeading = 		8.596
									vRotate = 		<<0.0000, 0.0000, 8.5960>>
								BREAK			
								CASE 2		
									model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_Ch_Bag_02a"))
									vCoords = 		<<452.7645, -977.0264, 30.1031>>
									fHeading = 		291.5984
									vRotate = 		<<0.0000, 0.0000, -68.4016>>
								BREAK			
							ENDSWITCH
						BREAK			
					ENDSWITCH	
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_VAULT_DRILL_1
			SWITCH eSubvariation
				CASE CHS_VD1_VARIATION_1
					SWITCH iMissionEntity				
						CASE 0			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_heist_drill_bag_01a"))
							vCoords = 		<<755.9210, -3192.3259, 5.8740>>
							fHeading = 		150.394
							vRotate = 		<<0.0000, 0.0000, 150.3940>>
						BREAK			
						CASE 1			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_heist_drill_bag_01a"))
							vCoords = 		<<754.6670, -3196.3669, 5.9220>>
							fHeading = 		310.594
							vRotate = 		<<0.0000, 0.0000, -49.4060>>
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_VD1_VARIATION_2
					SWITCH iMissionEntity				
						CASE 0			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_heist_drill_bag_01a"))
							vCoords = 		<<-1346.3550, -890.8580, 13.2720>>
							fHeading = 		64.997
							vRotate = 		<<0.0000, 0.0000, 64.9970>>
						BREAK			
						CASE 1			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_heist_drill_bag_01a"))
							vCoords = 		<<-1341.7070, -892.8380, 13.3140>>
							fHeading = 		102.997
							vRotate = 		<<0.0000, 0.0000, 102.9970>>
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		DEFAULT 
			model = PROP_DRUG_PACKAGE_02
		BREAK
	ENDSWITCH

	data.model = model
	data.vCoords = vCoords
	data.fHeading = fHeading
	data.vRotation = vRotate
	
	RETURN data

ENDFUNC

FUNC MODEL_NAMES GET_CSH_ENTITY_MODEL(CSH_VARIATION eVariation, CSH_SUBVARIATION eSubvariation)
	
	CSH_MISSION_ENTITY_DATA data = GET_CSH_MISSION_ENTITY_DATA(0, eVariation, eSubvariation, 0, 0)
	RETURN data.model
	
ENDFUNC

CONST_INT MAX_NUM_CSH_ENTITY_COMMON_SPAWN_COORDS		1
FUNC VECTOR GET_CSH_ENTITY_COMMON_SPAWN_COORDS(INT iSpawnCoord)
	
	SWITCH iSpawnCoord
		CASE 0			RETURN 0.0
	ENDSWITCH

	RETURN <<0.0,0.0,0.0>>

ENDFUNC

FUNC FLOAT GET_CSH_ENTITY_COMMON_SPAWN_HEADING(INT iSpawnCoord)

	SWITCH iSpawnCoord
		CASE 0			RETURN 0.0
	ENDSWITCH

	RETURN 0.0

ENDFUNC

/////////////////////////////////////////////////
/// 	VARIATION VEHICLE SPAWN COORDS 		  ///
/////////////////////////////////////////////////  

CONST_INT LMC_BENSON_ONE			6
CONST_INT LMC_BENSON_TWO			7
CONST_INT LMC_BENSON_THREE			8
CONST_INT LMC_BENSON_FOUR			9
CONST_INT LMC_BENSON_FIVE			10
CONST_INT LMC_BENSON_SIX			11

STRUCT CSH_VEHICLE_DATA

	MODEL_NAMES model
	VECTOR vCoords
	FLOAT fHeading

ENDSTRUCT

FUNC MODEL_NAMES GET_VEHICLE_MODEL_FROM_PLANNING_BOARD(CSH_VARIATION eVariation, CASINO_HEIST_DRIVERS eDriver, CASINO_HEIST_VEHICLE_SELECTION eChosenItem)

	MODEL_NAMES model = DUMMY_MODEL_FOR_SCRIPT
	
	UNUSED_PARAMETER(eVariation)
	
	SWITCH eDriver 
		CASE CASINO_HEIST_DRIVER__KARIM_DENZ		
			SWITCH eChosenItem
				CASE CASINO_HEIST_VEHICLE_SELECTION__DEFAULT 		model = ISSI3		BREAK
				CASE CASINO_HEIST_VEHICLE_SELECTION__ALTERNATE		model = ASBO		BREAK
				CASE CASINO_HEIST_VEHICLE_SELECTION__ALTERNATE_2	model = KANJO		BREAK
				CASE CASINO_HEIST_VEHICLE_SELECTION__ALTERNATE_3	model = SENTINEL3	BREAK
			ENDSWITCH
		BREAK
		CASE CASINO_HEIST_DRIVER__TALIANA_MARTINEZ	
			SWITCH eChosenItem
				CASE CASINO_HEIST_VEHICLE_SELECTION__DEFAULT 		model = RETINUE2	BREAK
				CASE CASINO_HEIST_VEHICLE_SELECTION__ALTERNATE		model = YOSEMITE2	BREAK
				CASE CASINO_HEIST_VEHICLE_SELECTION__ALTERNATE_2	model = SUGOI		BREAK
				CASE CASINO_HEIST_VEHICLE_SELECTION__ALTERNATE_3	model = JUGULAR		BREAK
			ENDSWITCH
		BREAK
		CASE CASINO_HEIST_DRIVER__EDDIE_TOH			
			SWITCH eChosenItem
				CASE CASINO_HEIST_VEHICLE_SELECTION__DEFAULT 		model = SULTAN2		BREAK
				CASE CASINO_HEIST_VEHICLE_SELECTION__ALTERNATE		model = GAUNTLET3	BREAK
				CASE CASINO_HEIST_VEHICLE_SELECTION__ALTERNATE_2	model = ELLIE		BREAK
				CASE CASINO_HEIST_VEHICLE_SELECTION__ALTERNATE_3	model = KOMODA		BREAK
			ENDSWITCH
		BREAK	
		CASE CASINO_HEIST_DRIVER__ZACH				
			SWITCH eChosenItem
				CASE CASINO_HEIST_VEHICLE_SELECTION__DEFAULT 		model = MANCHEZ		BREAK
				CASE CASINO_HEIST_VEHICLE_SELECTION__ALTERNATE		model = STRYDER		BREAK
				CASE CASINO_HEIST_VEHICLE_SELECTION__ALTERNATE_2	model = DEFILER		BREAK
				CASE CASINO_HEIST_VEHICLE_SELECTION__ALTERNATE_3	model = LECTRO		BREAK
			ENDSWITCH
		BREAK
		CASE CASINO_HEIST_DRIVER__WEAPONS_EXPERT	
			SWITCH eChosenItem
				CASE CASINO_HEIST_VEHICLE_SELECTION__DEFAULT 		model = ZHABA		BREAK
				CASE CASINO_HEIST_VEHICLE_SELECTION__ALTERNATE		model = VAGRANT		BREAK
				CASE CASINO_HEIST_VEHICLE_SELECTION__ALTERNATE_2	model = OUTLAW		BREAK
				CASE CASINO_HEIST_VEHICLE_SELECTION__ALTERNATE_3	model = EVERON		BREAK
			ENDSWITCH
		BREAK
		DEFAULT //Debug menu, for instance
//		CASE CASINO_HEIST_DRIVER__NONE 					
//			SWITCH eChosenItem
//				CASE CASINO_HEIST_VEHICLE_SELECTION__DEFAULT 		
//				CASE CASINO_HEIST_VEHICLE_SELECTION__ALTERNATE		
//				CASE CASINO_HEIST_VEHICLE_SELECTION__ALTERNATE_2	
//				CASE CASINO_HEIST_VEHICLE_SELECTION__ALTERNATE_3	
					model = SANDKING	
					SWITCH eVariation
						CASE CHV_LOST_MC_BIKES
							model = DAEMON
						BREAK
					ENDSWITCH
//				BREAK
//			ENDSWITCH
		BREAK
	ENDSWITCH												

	
	RETURN model
ENDFUNC

FUNC MODEL_NAMES SUPPORT_VEHICLE_MODEL()
	RETURN seashark
ENDFUNC

FUNC MODEL_NAMES GET_VEHICLE_FOR_GUNMAN(CASINO_HEIST_WEAPON_EXPERTS gunman)
	SWITCH gunman
		CASE CASINO_HEIST_WEAPON_EXPERT__KARL_ABOLAJI RETURN dubsta
		CASE CASINO_HEIST_WEAPON_EXPERT__GUSTAVO_MOTA RETURN vamos
		CASE CASINO_HEIST_WEAPON_EXPERT__CHARLIE RETURN rebel2
		CASE CASINO_HEIST_WEAPON_EXPERT__WEAPONS_EXPERT RETURN kamacho
		CASE CASINO_HEIST_WEAPON_EXPERT__PACKIE_MCREARY RETURN comet5
	ENDSWITCH
	
	SCRIPT_ASSERT("[GET_VEHICLE_FOR_GUNMAN] - Can't find vehivle for unknown gunman")
	RETURN dubsta
ENDFUNC

PROC GET_CELEB_DISPOSE_OF_CAR_VEHICLE_DATA(INT iSpawn, MODEL_NAMES &model, VECTOR &vCoords, FLOAT &fHeading)
	SWITCH iSpawn				
		CASE 0			
			model = 		Fugitive
			vCoords = 		<<908.4427, -13.6658, 77.7640>>
			fHeading = 		148.7994
		BREAK			
		CASE 1			
			model = 		Fugitive
			vCoords = 		<<940.5804, -45.8104, 77.7641>>
			fHeading = 		237.1115
		BREAK			
		CASE 2			
			model = 		Fugitive
			vCoords = 		<<908.4918, -72.4654, 77.7641>>
			fHeading = 		238.9397
		BREAK			
		CASE 3			
			model = 		Fugitive
			vCoords = 		<<851.7200, -40.9769, 77.7641>>
			fHeading = 		59.653
		BREAK			
		CASE 4			
			model = 		Fugitive
			vCoords = 		<<868.3211, -74.1356, 77.7641>>
			fHeading = 		147.6158
		BREAK			
	ENDSWITCH
ENDPROC

// url:bugstar:6163561 - Lost MC Bikes - Can we please add an additional 2 Benson trucks to this missions as there is meant to be a chance you find them empty as well
// Jumbled up the spreadsheet coords so that the empty vans are random, seemed the safest way to go about business
PROC GET_LOST_MC_BENSON_SPAWNS(INT iVehicle, CSH_SUBVARIATION eSubvariation, INT iExtraParam, MODEL_NAMES &model, VECTOR &vCoords, FLOAT &fHeading)

	model = benson
	
	SWITCH iExtraParam
		CASE 0
			SWITCH eSubvariation
				CASE CHS_LMC_VARIATION_1
					SWITCH iVehicle					
						CASE 6			
									vCoords = 		<<202.7318, 6384.4160, 30.4101>>	// SPOT 6
									fHeading = 		205.999
						BREAK		
						CASE 7		
									vCoords = 		<<207.6212, 6386.5918, 30.4065>>	// SPOT 5
									fHeading = 		205.999
						BREAK		
						CASE 8		
									vCoords = 		<<190.1830, 6417.1372, 30.1270>>	// SPOT 1
									fHeading = 		316.199
						BREAK		
						CASE 9		
									vCoords = 		<<183.5951, 6394.5498, 30.3814>>	// SPOT 3
									fHeading = 		116.5986
						BREAK		
						CASE 10		
									vCoords = 		<<197.7832, 6415.3315, 30.1706>>	// SPOT 4
									fHeading = 		307.1982
						BREAK		
						CASE 11		
									vCoords = 		<<188.7600, 6429.8540, 30.1650>>	// SPOT 2
									fHeading = 		240.599
						BREAK
					ENDSWITCH
				BREAK
				CASE CHS_LMC_VARIATION_2
					SWITCH iVehicle
						CASE 6			
									vCoords = 		<<1944.4064, 4621.2461, 39.4022>>   // SPOT 4
									fHeading = 		187.7986	
						BREAK		
						CASE 7		
									vCoords = 		<<1939.4290, 4638.5962, 39.5500>>   // SPOT 2
									fHeading = 		96.799								
						BREAK													
						CASE 8													
									vCoords = 		<<1963.0544, 4660.1655, 39.7662>>   // SPOT 5
									fHeading = 		63.1981								
						BREAK													
						CASE 9													
									vCoords = 		<<1944.9197, 4669.0605, 39.6679>>   // SPOT 6
									fHeading = 		61.7975
						BREAK		
						CASE 10		
									vCoords = 		<<1948.6460, 4633.9282, 39.6510>>	// SPOT 1
									fHeading = 		126.999							
						BREAK													
						CASE 11																			
									vCoords = 		<<1953.7971, 4651.0928, 39.7072>>   // SPOT 3
									fHeading = 		63.3988	
						BREAK
					ENDSWITCH
				BREAK
				CASE CHS_LMC_VARIATION_3
					SWITCH iVehicle
						CASE 6			
										vCoords = 		<<22.4061, 3685.7759, 38.7108>>		// SPOT 5
										fHeading = 		129.1989
						BREAK			
						CASE 7			
										vCoords = 		<<42.3850, 3727.1399, 38.6750>>		// SPOT 2
										fHeading = 		313.199
						BREAK			
						CASE 8			
										vCoords = 		<<33.1437, 3699.5095, 38.2418>>		// SPOT 4
										fHeading = 		20.1993
						BREAK			
						CASE 9			
										vCoords = 		<<54.0197, 3705.2820, 38.7550>>		// SPOT 6
										fHeading = 		241.3987
						BREAK			
						CASE 10			
										vCoords = 		<<32.5320, 3725.3721, 38.6810>>		// SPOT 1
										fHeading = 		333.8
						BREAK			
						CASE 11			
										vCoords = 		<<16.2135, 3706.3079, 38.6681>>		// SPOT 3
										fHeading = 		94.9999
						BREAK	
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE 1
			SWITCH eSubvariation
				CASE CHS_LMC_VARIATION_1
					SWITCH iVehicle						
						CASE 6			
									vCoords = 		<<183.5951, 6394.5498, 30.3814>>	// SPOT 3
									fHeading = 		116.5986
						BREAK		
						CASE 7		
									vCoords = 		<<207.6212, 6386.5918, 30.4065>>	// SPOT 5
									fHeading = 		205.999
						BREAK		
						CASE 8		
									vCoords = 		<<190.1830, 6417.1372, 30.1270>>	// SPOT 1
									fHeading = 		316.199
						BREAK		
						CASE 9		
									vCoords = 		<<197.7832, 6415.3315, 30.1706>>	// SPOT 4
									fHeading = 		307.1982
						BREAK		
						CASE 10		
									vCoords = 		<<188.7600, 6429.8540, 30.1650>>	// SPOT 2
									fHeading = 		240.599
						BREAK		
						CASE 11		
									vCoords = 		<<202.7318, 6384.4160, 30.4101>>	// SPOT 6
									fHeading = 		205.999
						BREAK
					ENDSWITCH
				BREAK
				CASE CHS_LMC_VARIATION_2				
					SWITCH iVehicle						
						CASE 6			
									vCoords = 		<<1939.4290, 4638.5962, 39.5500>>   // SPOT 2
									fHeading = 		96.799
						BREAK		
						CASE 7		
									vCoords = 		<<1944.4064, 4621.2461, 39.4022>>   // SPOT 4
									fHeading = 		187.7986
						BREAK		
						CASE 8		
									vCoords = 		<<1944.9197, 4669.0605, 39.6679>>   // SPOT 6
									fHeading = 		61.7975
						BREAK		
						CASE 9		
									vCoords = 		<<1953.7971, 4651.0928, 39.7072>>   // SPOT 3
									fHeading = 		63.3988	
						BREAK		
						CASE 10		
									vCoords = 		<<1963.0544, 4660.1655, 39.7662>>   // SPOT 5
									fHeading = 		63.1981
						BREAK		
						CASE 11		
									vCoords = 		<<1948.6460, 4633.9282, 39.6510>>	// SPOT 1
									fHeading = 		126.999	
						BREAK
					ENDSWITCH
				BREAK
				CASE CHS_LMC_VARIATION_3	
					SWITCH iVehicle
						CASE 6			
										vCoords = 		<<16.2135, 3706.3079, 38.6681>>		// SPOT 3
										fHeading = 		94.9999
						BREAK			
						CASE 7			
										vCoords = 		<<33.1437, 3699.5095, 38.2418>>		// SPOT 4
										fHeading = 		20.1993
						BREAK			
						CASE 8			
										vCoords = 		<<54.0197, 3705.2820, 38.7550>>		// SPOT 6
										fHeading = 		241.3987
						BREAK			
						CASE 9			
										vCoords = 		<<22.4061, 3685.7759, 38.7108>>		// SPOT 5
										fHeading = 		129.1989
						BREAK			
						CASE 10			
										vCoords = 		<<32.5320, 3725.3721, 38.6810>>		// SPOT 1
										fHeading = 		333.8
						BREAK			
						CASE 11			
										vCoords = 		<<42.3850, 3727.1399, 38.6750>>		// SPOT 2
										fHeading = 		313.199
						BREAK		
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE 2
			SWITCH eSubvariation
				CASE CHS_LMC_VARIATION_1
					SWITCH iVehicle		
						CASE 6			
									vCoords = 		<<188.7600, 6429.8540, 30.1650>>	// SPOT 2
									fHeading = 		240.599
						BREAK		
						CASE 7		
									vCoords = 		<<207.6212, 6386.5918, 30.4065>>	// SPOT 5
									fHeading = 		205.999
						BREAK		
						CASE 8		
									vCoords = 		<<197.7832, 6415.3315, 30.1706>>	// SPOT 4
									fHeading = 		307.1982
						BREAK		
						CASE 9		
									vCoords = 		<<190.1830, 6417.1372, 30.1270>>	// SPOT 1
									fHeading = 		316.199
						BREAK		
						CASE 10		
									vCoords = 		<<202.7318, 6384.4160, 30.4101>>	// SPOT 6
									fHeading = 		205.999
						BREAK		
						CASE 11		
									vCoords = 		<<183.5951, 6394.5498, 30.3814>>	// SPOT 3
									fHeading = 		116.5986
						BREAK
					ENDSWITCH
				BREAK
				CASE CHS_LMC_VARIATION_2
					SWITCH iVehicle							
						CASE 6			
									vCoords = 		<<1963.0544, 4660.1655, 39.7662>>   // SPOT 5
									fHeading = 		63.1981
						BREAK		
						CASE 7		vCoords = 		<<1953.7971, 4651.0928, 39.7072>>   // SPOT 3
									fHeading = 		63.3988	
									
						BREAK		
						CASE 8		vCoords = 		<<1944.4064, 4621.2461, 39.4022>>   // SPOT 4
									fHeading = 		187.7986
									
						BREAK		
						CASE 9		vCoords = 		<<1948.6460, 4633.9282, 39.6510>>	// SPOT 1
									fHeading = 		126.999	
									
						BREAK		
						CASE 10		vCoords = 		<<1939.4290, 4638.5962, 39.5500>>   // SPOT 2
									fHeading = 		96.799
									
						BREAK		
						CASE 11		vCoords = 		<<1944.9197, 4669.0605, 39.6679>>   // SPOT 6
									fHeading = 		61.7975
									
						BREAK
					ENDSWITCH
				BREAK
				CASE CHS_LMC_VARIATION_3	
					SWITCH iVehicle
						CASE 6			
										vCoords = 		<<42.3850, 3727.1399, 38.6750>>		// SPOT 2
										fHeading = 		313.199
						BREAK			
						CASE 7			
										vCoords = 		<<33.1437, 3699.5095, 38.2418>>		// SPOT 4
										fHeading = 		20.1993
						BREAK			
						CASE 8			
										vCoords = 		<<32.5320, 3725.3721, 38.6810>>		// SPOT 1
										fHeading = 		333.8
						BREAK			
						CASE 9			
										vCoords = 		<<22.4061, 3685.7759, 38.7108>>		// SPOT 5
										fHeading = 		129.1989
						BREAK			
						CASE 10			
										vCoords = 		<<16.2135, 3706.3079, 38.6681>>		// SPOT 3
										fHeading = 		94.9999
						BREAK			
						CASE 11			
										vCoords = 		<<54.0197, 3705.2820, 38.7550>>		// SPOT 6
										fHeading = 		241.3987
						BREAK	
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

FUNC CSH_VEHICLE_DATA GET_CSH_VEHICLE_DATA(INT iVehicle, CSH_VARIATION eVariation, CSH_SUBVARIATION eSubvariation, INT iExtraParam)

	MODEL_NAMES model = DUMMY_MODEL_FOR_SCRIPT
	VECTOR vCoords = <<0.0, 0.0, 0.0>>
	FLOAT fHeading = 0.0

	SWITCH eVariation
		CASE CHV_ARCADE_CABINETS	
			SWITCH eSubvariation
				CASE CHS_AC_ELYSIAN_ISLAND
					SWITCH iVehicle							
						CASE 0			
							model = 		MULE
							vCoords = 		<<-478.8630, -2823.4939, 5.0000>>
							fHeading = 		227
						BREAK					
						CASE 1			//Carrier Vehicle
							model = 		FLATBED
							vCoords = 		<<453.9250, -2098.2939, 20.9950>>
							fHeading = 		319.398
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_AC_DOWNTOWN_VINEWOOD
					SWITCH iVehicle					
						CASE 0			
							model = 		boxville2
							vCoords = 		<<71.4590, 122.8000, 78.1920>>
							fHeading = 		342
						BREAK				
						CASE 1			//Carrier Vehicle
							model = 		FLATBED
							vCoords = 		<<302.7810, 713.6240, 174.9410>>
							fHeading = 		358.399
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_AC_PALETO_BAY
					SWITCH iVehicle				
						CASE 0			
							model = 		MULE
							vCoords = 		<<-441.3070, 6141.2319, 30.4780>>
							fHeading = 		59.6
						BREAK							
						CASE 1			//Carrier Vehicle
							model = 		FLATBED
							vCoords = 		<<-1728.6300, 4791.4819, 57.3840>>
							fHeading = 		128.398
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_AC_LA_MESA
					SWITCH iVehicle				
						CASE 0			
							model = 		boxville2
							vCoords = 		<<914.6010, -1266.4210, 24.5710>>
							fHeading = 		205.4
						BREAK							
						CASE 1			//Carrier Vehicle
							model = 		FLATBED
							vCoords = 		<<1167.7070, -898.5610, 51.2520>>
							fHeading = 		355.798
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_AC_CHUMASH
					SWITCH iVehicle						
						CASE 0			
							model = 		boxville2
							vCoords = 		<<-3168.3879, 1131.1040, 20.0110>>
							fHeading = 		134.999
						BREAK					
						CASE 1			//Carrier Vehicle
							model = 		FLATBED
							vCoords = 		<<-2680.8459, -62.2460, 15.3700>>
							fHeading = 		218.8
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		
		CASE CHV_DRONES
			SWITCH eSubvariation
				CASE CHS_DRONES_VARIATION_2				
					SWITCH iVehicle				
						CASE 0			
							model = 		caddy
							vCoords = 		<<-1298.2233, 90.2426, 53.9308>>
							fHeading = 		301.9989
						BREAK			
					ENDSWITCH					
				BREAK
				CASE CHS_DRONES_VARIATION_3					
					SWITCH iVehicle				
						CASE 0			
							model = 		police3
							vCoords = 		<<1288.2472, -1732.9250, 51.9868>>
							fHeading = 		292.3962
						BREAK			
						CASE 1			
							model = 		Emperor2
							vCoords = 		<<1294.9220, -1730.1051, 52.6049>>
							fHeading = 		294.9962
						BREAK			
					ENDSWITCH					
				BREAK
			ENDSWITCH
		BREAK
		
		CASE CHV_MAINTENANCE_OUTFITS_2
			SWITCH eSubvariation
				CASE CHS_MO2_VARIATION_1					
					SWITCH iVehicle				
						CASE 0			//Carrier Vehicle
							model = 		BOXVILLE
							vCoords = 		<<-57.3828, -2659.0632, 5.0002>>
							fHeading = 		173.9999
						BREAK			
						CASE 1			
							model = 		BISON2
							vCoords = 		<<-49.5821, -2652.1636, 5.0007>>
							fHeading = 		1.1994
						BREAK			
					ENDSWITCH							
				BREAK
				CASE CHS_MO2_VARIATION_2				
					SWITCH iVehicle				
						CASE 0			//Carrier Vehicle
							model = 		BOXVILLE
							vCoords = 		<<1690.8077, -1732.0767, 111.4369>>
							fHeading = 		117.9998
						BREAK			
						CASE 1			
							model = 		BISON2
							vCoords = 		<<1698.2935, -1720.9550, 111.5499>>
							fHeading = 		75.9994
						BREAK			
					ENDSWITCH							
				BREAK
				CASE CHS_MO2_VARIATION_3					
					SWITCH iVehicle				
						CASE 0			//Carrier Vehicle
							model = 		BOXVILLE
							vCoords = 		<<870.4088, -2331.7417, 29.3458>>
							fHeading = 		337.8
						BREAK		
						CASE 1			
							model = 		BISON2
							vCoords = 		<<860.5229, -2333.3010, 29.3458>>
							fHeading = 		9.3999
						BREAK			
					ENDSWITCH								
				BREAK
			ENDSWITCH
		BREAK	
			
		CASE CHV_INSIDE_VALET	
			SWITCH eSubvariation
				CASE CHS_IMV_VARIATION_1
					SWITCH iVehicle				
						CASE 0			
							model = 		ROMERO
							vCoords = 		<<-1691.7910, -294.0686, 50.8123>>
							fHeading = 		53.5999
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_IMV_VARIATION_2
					SWITCH iVehicle				
						CASE 0			
							model = 		ROMERO
							vCoords = 		<<-1656.7778, -289.9378, 50.7751>>
							fHeading = 		233.3988
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_WEAPONS_STASH	
			SWITCH eSubvariation
				CASE CHS_WST_SAN_ANDREAS
					SWITCH iVehicle				
						CASE 0			
							model = 		BUCCANEER2
							vCoords = 		<<982.7650, -1828.5740, 30.2150>>
							fHeading = 		213.4
						BREAK			
						CASE 1			
							model = 		PCJ
							vCoords = 		<<974.0640, -1825.9041, 30.1400>>
							fHeading = 		213.4
						BREAK			
						CASE 2			
							model = 		PCJ
							vCoords = 		<<971.5710, -1826.2200, 30.1240>>
							fHeading = 		240.6
						BREAK			
						CASE 3			
							model = 		PEYOTE
							vCoords = 		<<1103.5370, -3156.9280, -38.5190>>
							fHeading = 		269.198
						BREAK			
						CASE 4			
							model = 		PCJ
							vCoords = 		<<1103.1930, -3146.1189, -38.5190>>
							fHeading = 		356.198
						BREAK			
						CASE 5			
							model = 		PCJ
							vCoords = 		<<1099.6920, -3145.8931, -38.5190>>
							fHeading = 		188.998
						BREAK	
						CASE 6			
							model = 		PEYOTE
							vCoords = 		<<1108.9480, -3159.2832, -38.5186>>
							fHeading = 		-0.0023
						BREAK	
					ENDSWITCH				
				BREAK
				CASE CHS_WST_PALETO_BAY
					SWITCH iVehicle				
						CASE 0			
							model = 		GBURRITO
							vCoords = 		<<-78.0200, 6497.2202, 30.4910>>
							fHeading = 		11
						BREAK			
						CASE 1			
							model = 		DAEMON
							vCoords = 		<<-84.9760, 6492.9790, 30.4910>>
							fHeading = 		49
						BREAK			
						CASE 2			
							model = 		DAEMON
							vCoords = 		<<-84.8510, 6494.7461, 30.4910>>
							fHeading = 		31.6
						BREAK			
						CASE 3			
							model = 		GBURRITO
							vCoords = 		<<1103.5370, -3156.9280, -38.5190>>
							fHeading = 		269.198
						BREAK			
						CASE 4			
							model = 		DAEMON
							vCoords = 		<<1103.1930, -3146.1189, -38.5190>>
							fHeading = 		356.198
						BREAK			
						CASE 5			
							model = 		DAEMON
							vCoords = 		<<1099.6920, -3145.8931, -38.5190>>
							fHeading = 		188.998
						BREAK			
						CASE 6			
							model = 		slamvan2
							vCoords = 		<<1109.0073, -3159.7764, -38.5186>>
							fHeading = 		1.7984
						BREAK
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_WEAPONS_SMUGGLERS
			SWITCH eSubvariation
				CASE CHS_WSM_VARIATION_1
					SWITCH iVehicle				
						CASE 0			
							model = 		SUPPORT_VEHICLE_MODEL()
							vCoords = 		<<2866.2810, -701.4430, 0.1580>>
							fHeading = 		322
						BREAK			
						CASE 1			
							model = 		SUPPORT_VEHICLE_MODEL()
							vCoords = 		<<2867.3401, -708.1230, 0.1560>>
							fHeading = 		306.4
						BREAK			
						CASE 2			
							model = 		SUPPORT_VEHICLE_MODEL()
							vCoords = 		<<2865.9670, -722.6540, 0.2490>>
							fHeading = 		279.2
						BREAK			
						CASE 3			
							model = 		SUPPORT_VEHICLE_MODEL()
							vCoords = 		<<2871.5891, -725.5990, 0.3110>>
							fHeading = 		321.2
						BREAK			
						CASE 4			
							model = 		FROGGER
							vCoords = 		<<2845.5847, -105.3938, 1.2762>>
							fHeading = 		321.1996
						BREAK			
						CASE 5			
							model = 		FROGGER
							vCoords = 		<<2846.0051, -67.3473, 1.8555>>
							fHeading = 		234.1993
						BREAK			
						CASE 6			
							model = 		DINGHY2
							vCoords = 		<<2869.4099, -75.4347, 0.5276>>
							fHeading = 		358.3991
						BREAK			
						CASE 7			
							model = 		DINGHY2
							vCoords = 		<<2859.9756, -96.0260, 0.1990>>
							fHeading = 		227.199
						BREAK			
						CASE 8			
							model = 		DINGHY2
							vCoords = 		<<3054.6277, -26.7192, 1.4568>>
							fHeading = 		76.399
						BREAK			
						CASE 9			
							model = 		tula
							vCoords = 		<<3064.8557, -0.9894, 1.3961>>
							fHeading = 		346.3988
						BREAK
						CASE 10			
							model = 		GET_VEHICLE_FOR_GUNMAN(GET_PLAYER_CASINO_HEIST_WEAPONS_EXPERT(GB_GET_LOCAL_PLAYER_GANG_BOSS()))
							vCoords = 		<<1725.8195, 3285.3372, 40.0845>>
							fHeading = 		336.3997
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_WSM_VARIATION_2
					SWITCH iVehicle				
						CASE 0			
							model = 		FROGGER
							vCoords = 		<<2548.8450, -1218.2660, 1.1520>>
							fHeading = 		108.4
						BREAK			
						CASE 1			
							model = 		FROGGER
							vCoords = 		<<2526.5149, -1223.9950, 1.2840>>
							fHeading = 		245.4
						BREAK			
						CASE 2			
							model = 		DINGHY2
							vCoords = 		<<2538.1919, -1241.3870, 0.4460>>
							fHeading = 		315.599
						BREAK			
						CASE 3			
							model = 		DINGHY2
							vCoords = 		<<2549.1040, -1238.6200, 0.1520>>
							fHeading = 		110.599
						BREAK			
						CASE 4			
							model = 		DINGHY2
							vCoords = 		<<2606.2544, -1327.8677, 0.3186>>
							fHeading = 		182.9987
						BREAK			
						CASE 5			//Carrier Vehicle
							model = 		tula
							vCoords = 		<<2616.3469, -1333.1564, 0.5065>>
							fHeading = 		302.7999
						BREAK			
						CASE 6			
							model = 		SUPPORT_VEHICLE_MODEL()
							vCoords = 		<<2867.5251, -793.1934, 0.1213>>
							fHeading = 		239.2
						BREAK			
						CASE 7			
							model = 		SUPPORT_VEHICLE_MODEL()
							vCoords = 		<<2868.1738, -785.1517, 0.3624>>
							fHeading = 		263.2
						BREAK			
						CASE 8			
							model = 		SUPPORT_VEHICLE_MODEL()
							vCoords = 		<<2867.3604, -766.3937, 0.4851>>
							fHeading = 		252.2
						BREAK			
						CASE 9			
							model = 		SUPPORT_VEHICLE_MODEL()
							vCoords = 		<<2868.7732, -771.5649, 0.5478>>
							fHeading = 		243.2
						BREAK
						CASE 10			
							model = 		GET_VEHICLE_FOR_GUNMAN(GET_PLAYER_CASINO_HEIST_WEAPONS_EXPERT(GB_GET_LOCAL_PLAYER_GANG_BOSS()))
							vCoords = 		<<1749.5123, 3293.2490, 40.1060>>
							fHeading = 		60.8
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_DRAG_RACE
			SWITCH eSubvariation
				CASE CHS_DR_VARIATION_1
					SWITCH iVehicle				
						CASE 0			
							model = 		GAUNTLET
							vCoords = 		<<1391.7679, 3000.4480, 39.5530>>
							fHeading = 		358.799
						BREAK			
						CASE 1			
							model = 		BANSHEE
							vCoords = 		<<1403.7860, 2989.0540, 39.5440>>
							fHeading = 		75.999
						BREAK		
						CASE 2			
							model = 		carbonrs
							vCoords = 		<<1396.2086, 3005.7358, 39.5528>>
							fHeading = 		339
						BREAK			
						CASE 3			
							model = 		carbonrs
							vCoords = 		<<1398.1525, 3006.1934, 39.5502>>
							fHeading = 		314.5999
						BREAK			
						CASE 4			
							model = 		carbonrs
							vCoords = 		<<1410.1040, 2995.2371, 39.5505>>
							fHeading = 		341.399
						BREAK			
						CASE 5			//Carrier Vehicle
							model = 		SANDKING
							vCoords = 		<<1403.2170, 3004.8320, 39.5460>>
							fHeading = 		314.6
						BREAK			
						CASE 6			//Carrier Vehicle
							model = 		SANDKING
							vCoords = 		<<1407.2080, 3000.4680, 39.5470>>
							fHeading = 		314.6
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_DR_VARIATION_2
					SWITCH iVehicle				
						CASE 0			
							model = 		GAUNTLET
							vCoords = 		<<-1482.3290, -2449.6790, 12.9450>>
							fHeading = 		179.199
						BREAK			
						CASE 1			
							model = 		BANSHEE
							vCoords = 		<<-1468.7450, -2454.2900, 12.9450>>
							fHeading = 		310.4
						BREAK	
						CASE 2			
							model = 		carbonrs
							vCoords = 		<<-1475.7712, -2446.2966, 12.9452>>
							fHeading = 		33
						BREAK			
						CASE 3			
							model = 		carbonrs
							vCoords = 		<<-1474.4600, -2444.3560, 12.9452>>
							fHeading = 		349
						BREAK			
						CASE 4			
							model = 		carbonrs
							vCoords = 		<<-1463.4865, -2447.1123, 12.9452>>
							fHeading = 		355.2
						BREAK			
						CASE 5			//Carrier Vehicle
							model = 		SANDKING
							vCoords = 		<<-1470.8040, -2437.8979, 12.9450>>
							fHeading = 		330.6
						BREAK			
						CASE 6			//Carrier Vehicle
							model = 		SANDKING
							vCoords = 		<<-1465.3521, -2440.8430, 12.9450>>
							fHeading = 		330.6
						BREAK			
					ENDSWITCH			
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_DISRUPT_SHIPMENTS
			SWITCH eSubvariation
				CASE CHS_DS_VARIATION_1
					SWITCH iVehicle				
						CASE 0			
							model = 		TROPIC2
							vCoords = 		<<-2912.0791, -628.1010, 0.0000>>
							fHeading = 		241.9996
						BREAK			
						CASE 1			
							model = 		TROPIC2
							vCoords = 		<<3615.0176, -41.4832, 0.1443>>
							fHeading = 		175
						BREAK			
						CASE 2			
							model = 		TROPIC2
							vCoords = 		<<-2300.8418, 5476.8770, -0.0000>>
							fHeading = 		166.6
						BREAK			
						CASE 3			
							model = 		SUPERVOLITO2
							vCoords = 		<<379.1205, 3432.3760, 69.2502>>
							fHeading = 		283.1995
						BREAK			
						CASE 4			
							model = 		SUPERVOLITO2
							vCoords = 		<<226.2046, -216.1721, 162.0059>>
							fHeading = 		70.7991
						BREAK			
						CASE 5			
							model = 		SUPERVOLITO2
							vCoords = 		<<-1744.1823, 4412.1470, 100.2588>>
							fHeading = 		71.9985
						BREAK			
						CASE 6			
							model = 		stockade
							vCoords = 		<<2715.6924, 3266.8555, 54.3467>>
							fHeading = 		151.5976
						BREAK			
						CASE 7			
							model = 		stockade
							vCoords = 		<<1338.8035, -2384.0471, 50.7886>>
							fHeading = 		350.7974
						BREAK			
						CASE 8			
							model = 		stockade
							vCoords = 		<<1623.4268, 6406.4785, 26.9501>>
							fHeading = 		67.3971
						BREAK			
						CASE 9			
							model = 		stockade
							vCoords = 		<<-1707.6619, 2435.5190, 29.6043>>
							fHeading = 		268.3969
						BREAK			
						CASE 10			
							model = 		SUPERVOLITO2
							vCoords = 		<<244.7002, -237.0265, 110.2503>>
							fHeading = 		70.799
						BREAK			
						CASE 11			
							model = 		SUPERVOLITO2
							vCoords = 		<<249.8918, -214.9318, 109.3465>>
							fHeading = 		70.799
						BREAK			
						CASE 12			
							model = 		SUPERVOLITO2
							vCoords = 		<<355.1865, 3436.4226, 68.1771>>
							fHeading = 		287.3988
						BREAK			
						CASE 13			
							model = 		SUPERVOLITO2
							vCoords = 		<<361.2102, 3417.2253, 70.8487>>
							fHeading = 		287.3988
						BREAK			
						CASE 14			
							model = 		SUPERVOLITO2
							vCoords = 		<<-1728.6554, 4396.8516, 98.0879>>
							fHeading = 		73.7985
						BREAK			
						CASE 15			
							model = 		SUPERVOLITO2
							vCoords = 		<<-1724.6342, 4418.0732, 97.9831>>
							fHeading = 		73.7985
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_DS_VARIATION_2
					SWITCH iVehicle				
						CASE 0			
							model = 		TROPIC2
							vCoords = 		<<1349.4352, -3474.4104, 0.3254>>
							fHeading = 		91
						BREAK			
						CASE 1			
							model = 		TROPIC2
							vCoords = 		<<13.2373, 7779.9971, 0.7582>>
							fHeading = 		93.2
						BREAK			
						CASE 2			
							model = 		TROPIC2
							vCoords = 		<<-3364.4580, 3308.0562, 0.2001>>
							fHeading = 		294.599
						BREAK			
						CASE 3			
							model = 		SUPERVOLITO2
							vCoords = 		<<-709.6321, 1049.3052, 320.6940>>
							fHeading = 		0.599
						BREAK			
						CASE 4			
							model = 		SUPERVOLITO2
							vCoords = 		<<831.2717, 2424.4089, 89.7616>>
							fHeading = 		298.5988
						BREAK			
						CASE 5			
							model = 		SUPERVOLITO2
							vCoords = 		<<3002.6860, 4384.4756, 95.5015>>
							fHeading = 		188.9987
						BREAK			
						CASE 6			
							model = 		stockade
							vCoords = 		<<-1244.3271, -610.4098, 28.1866>>
							fHeading = 		307.7985
						BREAK			
						CASE 7			
							model = 		stockade
							vCoords = 		<<-816.8278, 5263.1147, 89.3626>>
							fHeading = 		275.7981
						BREAK			
						CASE 8			
							model = 		stockade
							vCoords = 		<<2176.9346, -522.4238, 92.7239>>
							fHeading = 		133.5976
						BREAK			
						CASE 9			
							model = 		stockade
							vCoords = 		<<-749.4968, 2791.4668, 24.5165>>
							fHeading = 		120.3972
						BREAK			
						CASE 10			
							model = 		SUPERVOLITO2
							vCoords = 		<<-719.8558, 1026.2434, 322.7350>>
							fHeading = 		0.599
						BREAK			
						CASE 11			
							model = 		SUPERVOLITO2
							vCoords = 		<<-698.0895, 1026.2190, 321.8038>>
							fHeading = 		0.599
						BREAK			
						CASE 12			
							model = 		SUPERVOLITO2
							vCoords = 		<<812.0942, 2423.2615, 86.0032>>
							fHeading = 		300.599
						BREAK			
						CASE 13			
							model = 		SUPERVOLITO2
							vCoords = 		<<820.2598, 2404.4558, 88.4437>>
							fHeading = 		300.599
						BREAK			
						CASE 14			
							model = 		SUPERVOLITO2
							vCoords = 		<<3008.4382, 4409.4888, 96.4163>>
							fHeading = 		190.799
						BREAK			
						CASE 15			
							model = 		SUPERVOLITO2
							vCoords = 		<<2986.5920, 4407.8770, 97.7386>>
							fHeading = 		190.799
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_DS_VARIATION_3
					SWITCH iVehicle				
						CASE 0			
							model = 		TROPIC2
							vCoords = 		<<-955.2387, -3751.7058, 0.1673>>
							fHeading = 		33
						BREAK			
						CASE 1			
							model = 		TROPIC2
							vCoords = 		<<13.2373, 7779.9971, 0.7582>>
							fHeading = 		254.199
						BREAK			
						CASE 2			
							model = 		TROPIC2
							vCoords = 		<<-3598.0540, 933.7335, 0.1101>>
							fHeading = 		169.999
						BREAK			
						CASE 3			
							model = 		SUPERVOLITO2
							vCoords = 		<<3412.2192, 3676.1167, 86.7910>>
							fHeading = 		147.5989
						BREAK			
						CASE 4			
							model = 		SUPERVOLITO2
							vCoords = 		<<2026.3929, 5177.3877, 73.4000>>
							fHeading = 		147.5989
						BREAK			
						CASE 5			
							model = 		SUPERVOLITO2
							vCoords = 		<<915.8270, -3044.9685, 43.8000>>
							fHeading = 		52.7987
						BREAK			
						CASE 6			
							model = 		stockade
							vCoords = 		<<2476.0444, -701.0084, 60.2647>>
							fHeading = 		89.1982
						BREAK			
						CASE 7			
							model = 		stockade
							vCoords = 		<<251.0897, 810.3633, 195.3857>>
							fHeading = 		305.5974
						BREAK			
						CASE 8			
							model = 		stockade
							vCoords = 		<<-1787.0551, 4737.9990, 56.0435>>
							fHeading = 		133.9971
						BREAK			
						CASE 9			
							model = 		stockade
							vCoords = 		<<269.5162, 2687.2710, 43.2464>>
							fHeading = 		191.1971
						BREAK			
						CASE 10			
							model = 		SUPERVOLITO2
							vCoords = 		<<926.7957, -3066.6318, 44.5206>>
							fHeading = 		52.799
						BREAK			
						CASE 11			
							model = 		SUPERVOLITO2
							vCoords = 		<<946.3006, -3055.3318, 45.2692>>
							fHeading = 		52.799
						BREAK			
						CASE 12			
							model = 		SUPERVOLITO2
							vCoords = 		<<3431.8254, 3690.4431, 85.0151>>
							fHeading = 		146.5486
						BREAK			
						CASE 13			
							model = 		SUPERVOLITO2
							vCoords = 		<<3422.6826, 3713.4436, 87.3928>>
							fHeading = 		152.1485
						BREAK			
						CASE 14			
							model = 		SUPERVOLITO2
							vCoords = 		<<2045.9901, 5189.9595, 74.9697>>
							fHeading = 		148.1484
						BREAK			
						CASE 15			
							model = 		SUPERVOLITO2
							vCoords = 		<<2032.4774, 5205.2510, 71.4432>>
							fHeading = 		150.1483
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_LOST_MC_BIKES
			SWITCH eSubvariation
				CASE CHS_LMC_VARIATION_1
					SWITCH iVehicle				
						CASE 0			
							model = 		GBURRITO
							vCoords = 		<<163.7110, 6412.8379, 30.1680>>
							fHeading = 		308.8
						BREAK			
						CASE 1			
							model = 		GBURRITO
							vCoords = 		<<168.6140, 6408.6162, 30.1370>>
							fHeading = 		273.399
						BREAK			
						CASE 2			
							model = 		SANDKING
							vCoords = 		<<173.5130, 6432.4580, 30.2360>>
							fHeading = 		293.399
						BREAK			
						CASE 3			
							model = 		SANDKING
							vCoords = 		<<175.9910, 6431.0771, 30.2030>>
							fHeading = 		280.999
						BREAK	
						CASE 4	// I made these coords up because they don't exist on the spreadsheet		
							model = 		SANDKING
							vCoords = 		<<183.5951, 6394.5498, 30.3814>>
							fHeading = 		293.399
						BREAK			
						CASE 5			
							model = 		SANDKING
							vCoords = 		<<197.7832, 6415.3315, 30.1706>>
							fHeading = 		280.999
						BREAK
						CASE 6				
						CASE 7				
						CASE 8				
						CASE 9				
						CASE 10				
						CASE 11				
							GET_LOST_MC_BENSON_SPAWNS(iVehicle, eSubvariation, iExtraParam, model, vCoords, fHeading) 
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_LMC_VARIATION_2
					SWITCH iVehicle				
						CASE 0			
							model = 		GBURRITO
							vCoords = 		<<1972.1140, 4644.7710, 40.0080>>
							fHeading = 		166.2
						BREAK			
						CASE 1			
							model = 		GBURRITO
							vCoords = 		<<1965.5439, 4643.4902, 39.7940>>
							fHeading = 		136.599
						BREAK			
						CASE 2			
							model = 		SANDKING
							vCoords = 		<<1956.4680, 4643.4868, 39.7070>>
							fHeading = 		142.199
						BREAK			
						CASE 3			
							model = 		SANDKING
							vCoords = 		<<1954.0780, 4643.3608, 39.6590>>
							fHeading = 		93.799
						BREAK		
						CASE 4			
							model = 		SANDKING
							vCoords = 		<<1953.7971, 4651.0928, 39.7072>>
							fHeading = 		293.399
						BREAK			
						CASE 5			
							model = 		SANDKING
							vCoords = 		<<1944.4064, 4621.2461, 39.4022>>
							fHeading = 		280.999
						BREAK
						CASE 6				
						CASE 7				
						CASE 8				
						CASE 9				
						CASE 10				
						CASE 11				
							GET_LOST_MC_BENSON_SPAWNS(iVehicle, eSubvariation, iExtraParam, model, vCoords, fHeading) 
						BREAK				
					ENDSWITCH				
				BREAK
				CASE CHS_LMC_VARIATION_3
					SWITCH iVehicle				
						CASE 0			
							model = 		GBURRITO
							vCoords = 		<<28.1350, 3707.9570, 38.6830>>
							fHeading = 		356.2
						BREAK			
						CASE 1			
							model = 		GBURRITO
							vCoords = 		<<25.8790, 3701.2480, 38.7020>>
							fHeading = 		34.8
						BREAK			
						CASE 2			
							model = 		SANDKING
							vCoords = 		<<23.0400, 3711.6211, 38.6660>>
							fHeading = 		27.8
						BREAK			
						CASE 3			
							model = 		SANDKING
							vCoords = 		<<24.4530, 3714.1731, 38.6470>>
							fHeading = 		344.6
						BREAK	
						CASE 4			
							model = 		SANDKING
							vCoords = 		<<23.0400, 3711.6211, 40.6660>>
							fHeading = 		293.399
						BREAK			
						CASE 5			
							model = 		SANDKING
							vCoords = 		<<24.4530, 3714.1731, 40.6470>>
							fHeading = 		280.999
						BREAK
						CASE 6			
							model = 		benson
							vCoords = 		<<32.5320, 3725.3721, 38.6810>>
							fHeading = 		333.8
						BREAK			
						CASE 7			
							model = 		benson
							vCoords = 		<<42.3850, 3727.1399, 38.6750>>
							fHeading = 		313.199
						BREAK			
						CASE 8			
							model = 		benson
							vCoords = 		<<16.2135, 3706.3079, 38.6681>>
							fHeading = 		94.9999
						BREAK			
						CASE 9			
							model = 		benson
							vCoords = 		<<33.1437, 3699.5095, 38.2418>>
							fHeading = 		20.1993
						BREAK	
						CASE 10		
							model = 		benson
							vCoords = 		<<22.4061, 3685.7759, 38.7108>>
							fHeading = 		129.1989
						BREAK			
						CASE 11			
							model = 		benson
							vCoords = 		<<54.0197, 3705.2820, 38.7550>>
							fHeading = 		241.3987
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_CARGO_SHIP
			SWITCH eSubvariation
				CASE CHS_CS_VARIATION_1
					SWITCH iVehicle
						CASE 0			
							model = 		cargobob2
							vCoords = 		<<-105.7846, -2364.8708, 15.6131>>
							fHeading = 		179.5989
						BREAK			
						CASE 1				
							model = 		FUGITIVE	
							vCoords = 		<<-134.6370, -2388.7000, 5.0000>>	
							fHeading = 		37.399	
						BREAK				
						CASE 2				
							model = 		FUGITIVE	
							vCoords = 		<<-127.9160, -2387.1790, 5.0000>>	
							fHeading = 		66.799	
						BREAK				
						CASE 3			//Carrier Vehicle	
							model = 		SANDKING	//Vehicle model will be replaced with whatever player chooses
							vCoords = 		<<-139.4400, -2366.8831, 19.6260>>	//from the Planning Board
							fHeading = 		198.599	
						BREAK				
						CASE 4			//Carrier Vehicle	
							model = 		SANDKING	//Vehicle model will be replaced with whatever player chooses
							vCoords = 		<<-150.6700, -2390.0220, 5.0000>>	//from the Planning Board
							fHeading = 		241.398	
						BREAK	
						CASE 5			//Carrier Vehicle
							model = 		SANDKING
							vCoords = 		<<-141.8298, -2369.5471, 19.6259>>
							fHeading = 		199.398
						BREAK			
						CASE 6			//Carrier Vehicle
							model = 		SANDKING
							vCoords = 		<<-147.9068, -2387.0896, 5.0000>>
							fHeading = 		259.398
						BREAK			
					ENDSWITCH
				BREAK
				CASE CHS_CS_VARIATION_2
					SWITCH iVehicle
						CASE 0				
							model = 		GBURRITO	
							vCoords = 		<<-19.1930, 6205.8979, 30.0300>>	
							fHeading = 		191.2	
						BREAK				
						CASE 1				
							model = 		GBURRITO	
							vCoords = 		<<-24.2350, 6201.7808, 30.0420>>	
							fHeading = 		159.8	
						BREAK				
						CASE 2				
							model = 		cargobob2	//Use the longer hook variation
							vCoords = 		<<22.9680, 6215.5078, 30.5830>>	
							fHeading = 		51.4	
						BREAK				
						CASE 3			//Carrier Vehicle	
							model = 		SANDKING	//Vehicle model will be replaced with whatever player chooses
							vCoords = 		<<-4.9680, 6201.6172, 38.7550>>	//from the Planning Board
							fHeading = 		21.8	
						BREAK				
						CASE 4			//Carrier Vehicle	
							model = 		SANDKING	//Vehicle model will be replaced with whatever player chooses
							vCoords = 		<<-1.6600, 6214.5771, 30.4240>>	//from the Planning Board
							fHeading = 		103	
						BREAK		
						CASE 5			//Carrier Vehicle
							model = 		SANDKING
							vCoords = 		<<-11.1310, 6201.9673, 38.7552>>
							fHeading = 		320.6
						BREAK			
						CASE 6			//Carrier Vehicle
							model = 		SANDKING
							vCoords = 		<<1.0914, 6218.4185, 30.4580>>
							fHeading = 		95.5998
						BREAK			
					ENDSWITCH
				BREAK
				CASE CHS_CS_VARIATION_3
					SWITCH iVehicle				
						CASE 0			
							model = 		SCHAFTER2
							vCoords = 		<<589.3900, -1894.0100, 24.3010>>
							fHeading = 		282.599
						BREAK			
						CASE 1			
							model = 		SCHAFTER2
							vCoords = 		<<591.7350, -1888.4630, 24.2700>>
							fHeading = 		309.799
						BREAK			
						CASE 2			
							model = 		cargobob2
							vCoords = 		<<569.4227, -1942.5875, 23.7572>>
							fHeading = 		355.198
						BREAK			
						CASE 3			//Carrier Vehicle
							model = 		SANDKING
							vCoords = 		<<589.1750, -1880.0211, 24.1837>>
							fHeading = 		151.3989
						BREAK			
						CASE 4			//Carrier Vehicle
							model = 		SANDKING
							vCoords = 		<<596.7640, -1871.3510, 23.8270>>
							fHeading = 		141.199
						BREAK			
						CASE 5			//Carrier Vehicle
							model = 		SANDKING
							vCoords = 		<<578.8348, -1885.7469, 24.3593>>
							fHeading = 		219.399
						BREAK			
						CASE 6			//Carrier Vehicle
							model = 		SANDKING
							vCoords = 		<<599.8243, -1873.5422, 23.8131>>
							fHeading = 		120.399
						BREAK		
						CASE 7			
							model = 		SCHAFTER2
							vCoords = 		<<578.5626, -1897.4968, 23.9118>>
							fHeading = 		333.3988
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_ELECTRIC_DRILLS_2
			SWITCH eSubvariation
				CASE CHS_ED2_VARIATION_1
					SWITCH iVehicle				
						CASE 0			
							model = 		bulldozer
							vCoords = 		<<-923.3530, 384.3930, 78.1740>>
							fHeading = 		40.8
						BREAK			
						CASE 1			//Carrier Vehicle
							model = 		BURRITO
							vCoords = 		<<-916.9493, 398.2994, 78.3899>>
							fHeading = 		323.782
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_ED2_VARIATION_2
					SWITCH iVehicle				
						CASE 0			
							model = 		bulldozer
							vCoords = 		<<-366.8830, 6249.9902, 30.4870>>
							fHeading = 		117.2
						BREAK			
						CASE 1			//Carrier Vehicle
							model = 		BURRITO
							vCoords = 		<<-383.3271, 6269.2480, 29.6773>>
							fHeading = 		47.4604
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_ED2_VARIATION_3
					SWITCH iVehicle				
						CASE 0			
							model = 		bulldozer
							vCoords = 		<<1143.9850, 2092.3220, 54.8000>>
							fHeading = 		297.8
						BREAK			
						CASE 1			//Carrier Vehicle
							model = 		BURRITO
							vCoords = 		<<1153.1025, 2095.1167, 54.9150>>
							fHeading = 		236.6638
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_ZANCUDO_SHIPMENT	
			SWITCH eSubvariation
				CASE CHS_ZS_VARIATION_1
					SWITCH iVehicle				
						CASE 0			
							model = 		BARRACKS
							vCoords = 		<<-2442.6709, 2967.0491, 31.8100>>
							fHeading = 		329.599
						BREAK			
						CASE 1			//Carrier Vehicle
							model = 		BARRAGE
							vCoords = 		<<-2451.5845, 2956.4355, 31.8103>>
							fHeading = 		0
						BREAK	
						CASE 2			
							model = 		cargobob
							vCoords = 		<<-2381.9155, 2977.6069, 31.9376>>
							fHeading = 		10.4
						BREAK			
						CASE 3			
							model = 		BARRACKS
							vCoords = 		<<-2451.1982, 2979.7139, 31.8104>>
							fHeading = 		105.9988
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_ZS_VARIATION_2
					SWITCH iVehicle				
						CASE 0			
							model = 		BARRACKS
							vCoords = 		<<-2278.3279, 3170.9600, 31.8100>>
							fHeading = 		239.8
						BREAK			
						CASE 1			//Carrier Vehicle
							model = 		BARRAGE
							vCoords = 		<<-2288.4680, 3186.5281, 31.8100>>
							fHeading = 		212.599
						BREAK	
						CASE 2			
							model = 		cargobob
							vCoords = 		<<-2327.5361, 3188.1882, 31.8279>>
							fHeading = 		103.9983
						BREAK			
					ENDSWITCH			
				BREAK
				CASE CHS_ZS_VARIATION_3
					SWITCH iVehicle				
						CASE 0			
							model = 		BARRACKS
							vCoords = 		<<-2072.8030, 3264.0259, 31.8100>>
							fHeading = 		329.4
						BREAK			
						CASE 1			//Carrier Vehicle
							model = 		BARRAGE
							vCoords = 		<<-2073.8411, 3247.5190, 31.8100>>
							fHeading = 		49.2
						BREAK	
						CASE 2			
							model = 		cargobob
							vCoords = 		<<-2081.4209, 3181.0120, 31.8103>>
							fHeading = 		148.1995
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_BUGSTAR_OUTFITS_1
			SWITCH eSubvariation
				CASE CHS_BO1_VARIATION_1
					SWITCH iVehicle				
						CASE 0			
							model = 		BURRITO2
							vCoords = 		<<947.6550, -668.1820, 57.0160>>
							fHeading = 		299.2
						BREAK
						CASE 1			
							model = 		BISON3
							vCoords = 		<<958.3163, -662.5280, 57.0164>>
							fHeading = 		118.8
						BREAK
					ENDSWITCH
				BREAK
				CASE CHS_BO1_VARIATION_2
					SWITCH iVehicle				
						CASE 0			
							model = 		BURRITO2
							vCoords = 		<<-2987.3411, 727.4150, 27.4970>>
							fHeading = 		147.6
						BREAK
						CASE 1			
							model = 		BISON3
							vCoords = 		<<-3001.9141, 718.8981, 27.4328>>
							fHeading = 		292.5999
						BREAK
					ENDSWITCH
				BREAK
				CASE CHS_BO1_VARIATION_3
					SWITCH iVehicle				
						CASE 0			
							model = 		BURRITO2
							vCoords = 		<<-1498.3610, 420.1490, 110.1070>>
							fHeading = 		224.8
						BREAK
						CASE 1			
							model = 		BISON3
							vCoords = 		<<-1506.2412, 425.8701, 110.1050>>
							fHeading = 		203.7996
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_BUGSTAR_OUTFITS_2
			SWITCH eSubvariation
				CASE CHS_BO2_VARIATION_1
					SWITCH iVehicle				
						CASE 0			
							model = 		trash2
							vCoords = 		<<-468.6968, -1687.8225, 17.9442>>
							fHeading = 		208.4
						BREAK			
						CASE 1			
							model = 		Emperor2
							vCoords = 		<<-451.7901, -1695.0833, 17.9397>>
							fHeading = 		340.3996
						BREAK			
					ENDSWITCH
				BREAK
				CASE CHS_BO2_VARIATION_2
					SWITCH iVehicle				
						CASE 0			
							model = 		trash2
							vCoords = 		<<-505.2419, -1718.8328, 18.3173>>
							fHeading = 		190.1996
						BREAK			
						CASE 1			
							model = 		Emperor2
							vCoords = 		<<-510.3191, -1735.5620, 18.1176>>
							fHeading = 		144.3994
						BREAK			
					ENDSWITCH
				BREAK
				CASE CHS_BO2_VARIATION_3
					SWITCH iVehicle				
						CASE 0			
							model = 		trash2
							vCoords = 		<<-637.0629, -1725.8414, 23.2668>>
							fHeading = 		44.5995
						BREAK			
						CASE 1			
							model = 		Emperor2
							vCoords = 		<<-650.6069, -1716.9185, 23.6591>>
							fHeading = 		164.3987
						BREAK			
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_FIREFIGHTER_OUTFIT_1
			SWITCH eSubvariation
				CASE CHS_FF1_VARIATION_1
					SWITCH iVehicle
						CASE 0			//Carrier Vehicle
							model = 		firetruk
							vCoords = 		<<212.7410, -1649.2010, 28.8030>>
							fHeading = 		320.199
						BREAK
						CASE 1			//Carrier Vehicle
							model = 		firetruk
							vCoords = 		<<-1083.7874, -2374.3359, 12.9452>>
							fHeading = 		149.9998
						BREAK
						CASE 2			//Carrier Vehicle
							model = 		firetruk
							vCoords = 		<<1697.1077, 3588.0444, 34.6210>>
							fHeading = 		210.9982
						BREAK
					ENDSWITCH
				BREAK
				CASE CHS_FF1_VARIATION_2
					SWITCH iVehicle
						CASE 0			//Carrier Vehicle
							model = 		firetruk
							vCoords = 		<<1200.6815, -1467.7250, 33.8595>>
							fHeading = 		358.5985
						BREAK
						CASE 1			//Carrier Vehicle
							model = 		firetruk
							vCoords = 		<<-634.1979, -105.1648, 37.0504>>
							fHeading = 		82.7983
						BREAK
						CASE 2			//Carrier Vehicle
							model = 		firetruk
							vCoords = 		<<-2110.1167, 2832.4070, 31.8094>>
							fHeading = 		355.5978
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_MAINTENANCE_OUTFITS_1
			SWITCH eSubvariation
				CASE CHS_MO1_VARIATION_1
					SWITCH iVehicle				
						CASE 0			
							model = 		BURRITO
							vCoords = 		<<-1589.3188, -3238.1919, 12.9450>>
							fHeading = 		271
						BREAK			
					ENDSWITCH								
				BREAK
				CASE CHS_MO1_VARIATION_2
					SWITCH iVehicle				
						CASE 0			
							model = 		BURRITO
							vCoords = 		<<-1154.8149, -228.1860, 36.9020>>
							fHeading = 		135.4
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_MO1_VARIATION_3
					SWITCH iVehicle				
						CASE 0			
							model = 		BURRITO
							vCoords = 		<<-2170.2700, 4279.1670, 47.9820>>
							fHeading = 		161.4
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_GRUPPE_SECHS_VAN_1
			SWITCH eSubvariation
				CASE CHS_GS1_VARIATION_1
					SWITCH iVehicle				
						CASE 0			//Carrier Vehicle
							model = 		stockade
							vCoords = 		<<474.1363, -1278.5161, 28.5394>>
							fHeading = 		359.9995
						BREAK			
						CASE 1			
							model = 		boxville2
							vCoords = 		<<482.4669, -1282.1046, 28.5391>>
							fHeading = 		55.599
						BREAK			
					ENDSWITCH
				BREAK
				CASE CHS_GS1_VARIATION_2
					SWITCH iVehicle				
						CASE 0			//Carrier Vehicle
							model = 		stockade
							vCoords = 		<<237.8251, 2574.2712, 45.0134>>
							fHeading = 		16.7999
						BREAK			
						CASE 1			
							model = 		boxville2
							vCoords = 		<<256.7886, 2578.3052, 44.2169>>
							fHeading = 		97.7997
						BREAK			
					ENDSWITCH
				BREAK
				CASE CHS_GS1_VARIATION_3
					SWITCH iVehicle				
						CASE 0			//Carrier Vehicle
							model = 		stockade
							vCoords = 		<<139.2798, 6637.5718, 30.6034>>
							fHeading = 		134.6
						BREAK			
						CASE 1			
							model = 		boxville2
							vCoords = 		<<128.7480, 6627.0068, 30.7489>>
							fHeading = 		134.6
						BREAK			
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_UNMARKED_WEAPONS
			SWITCH eSubvariation
				CASE CHS_UW_ELYSIAN_ISLAND
					SWITCH iVehicle				
						CASE 0			
							model = 		POLICET
							vCoords = 		<<468.0900, -2933.7380, 5.0440>>
							fHeading = 		104.597
						BREAK			
						CASE 1			
							model = 		police3
							vCoords = 		<<478.7210, -2932.3799, 5.0440>>
							fHeading = 		88.997
						BREAK			
						CASE 2			
							model = 		police3
							vCoords = 		<<511.1530, -2933.4839, 5.0440>>
							fHeading = 		65.797
						BREAK
						CASE 3			
							model = 		kuruma
							vCoords = 		<<496.4508, -2934.7998, 5.0445>>
							fHeading = 		252.1989
						BREAK			
						CASE 4			
							model = 		MULE3
							vCoords = 		<<491.0022, -2938.0332, 5.0496>>
							fHeading = 		211.9992
						BREAK			
						CASE 5			
							model = 		police3
							vCoords = 		<<488.1122, -2943.0850, 5.0444>>
							fHeading = 		64.9969
						BREAK			
						CASE 6			
							model = 		POLICET
							vCoords = 		<<500.8084, -2941.9131, 5.0445>>
							fHeading = 		83.7968
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_UW_GRAPESEED
					SWITCH iVehicle				
						CASE 0			
							model = 		sheriff
							vCoords = 		<<2307.8870, 4890.3770, 40.8080>>
							fHeading = 		181.2
						BREAK			
						CASE 1			
							model = 		POLICET
							vCoords = 		<<2302.0190, 4883.3550, 40.8080>>
							fHeading = 		158
						BREAK			
						CASE 2			
							model = 		sheriff
							vCoords = 		<<2349.8911, 4878.7881, 40.8250>>
							fHeading = 		178.2
						BREAK	
						CASE 3			
							model = 		GBURRITO
							vCoords = 		<<2284.4646, 4888.2227, 40.0105>>
							fHeading = 		113.9998
						BREAK			
						CASE 4			
							model = 		MULE3
							vCoords = 		<<2290.4658, 4886.0269, 40.2579>>
							fHeading = 		132.7996
						BREAK			
						CASE 5			
							model = 		sheriff
							vCoords = 		<<2299.1497, 4900.9429, 40.1757>>
							fHeading = 		109.6
						BREAK			
						CASE 6			
							model = 		POLICET
							vCoords = 		<<2302.7292, 4907.5317, 40.4086>>
							fHeading = 		120.1997
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_UW_STONER
					SWITCH iVehicle				
						CASE 0			
							model = 		POLICET
							vCoords = 		<<315.1780, 2840.4160, 42.4360>>
							fHeading = 		298
						BREAK			
						CASE 1			
							model = 		sheriff
							vCoords = 		<<306.8200, 2833.3550, 42.4360>>
							fHeading = 		273.799
						BREAK			
						CASE 2			
							model = 		sheriff
							vCoords = 		<<292.2400, 2865.5310, 42.6420>>
							fHeading = 		319.999
						BREAK
						CASE 3			
							model = 		SADLER
							vCoords = 		<<311.5231, 2824.6975, 42.4362>>
							fHeading = 		257.9989
						BREAK			
						CASE 4			
							model = 		MULE3
							vCoords = 		<<298.9199, 2839.0657, 42.4679>>
							fHeading = 		309.1989
						BREAK			
						CASE 5			
							model = 		sheriff
							vCoords = 		<<281.0594, 2831.2471, 42.4538>>
							fHeading = 		235.3989
						BREAK			
						CASE 6			
							model = 		POLICET
							vCoords = 		<<280.7509, 2837.3589, 42.5992>>
							fHeading = 		218.7997
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_STEAL_EMP
			SWITCH eSubvariation
				CASE CHS_EMP_ULSA1
					SWITCH iVehicle				
						CASE 0			
							model = 		RUMPO
							vCoords = 		<<-1758.9020, 207.8090, 63.3860>>
							fHeading = 		115.8
						BREAK			
						CASE 1			
							model = 		schafter5
							vCoords = 		<<-1057.6362, -2329.4377, 12.9446>>
							fHeading = 		112.3999
						BREAK			
						CASE 2			
							model = 		schafter5
							vCoords = 		<<-1053.4658, -2324.7817, 12.9446>>
							fHeading = 		80.9997
						BREAK			
						CASE 3			
							model = 		cargobob2
							vCoords = 		<<-1059.1316, -2339.8354, 19.5294>>
							fHeading = 		61.8
						BREAK					
					ENDSWITCH				
				BREAK
				CASE CHS_EMP_ULSA2
					SWITCH iVehicle				
						CASE 0			
							model = 		RUMPO
							vCoords = 		<<-1700.0430, 155.3240, 63.3720>>
							fHeading = 		34.8
						BREAK					
						CASE 1			
							model = 		schafter5
							vCoords = 		<<-1205.0054, -2370.3601, 12.9452>>
							fHeading = 		94.3995
						BREAK			
						CASE 2			
							model = 		schafter5
							vCoords = 		<<-1201.7513, -2365.5232, 12.9452>>
							fHeading = 		124.3991
						BREAK			
						CASE 3			
							model = 		cargobob2
							vCoords = 		<<-1211.4325, -2355.3687, 12.9451>>
							fHeading = 		236.1998
						BREAK
					ENDSWITCH				
				BREAK
				CASE CHS_EMP_ULSA3
					SWITCH iVehicle				
						CASE 0			
							model = 		RUMPO
							vCoords = 		<<-1617.4170, 231.8070, 58.9810>>
							fHeading = 		61.399
						BREAK						
						CASE 1			
							model = 		schafter5
							vCoords = 		<<-1155.6505, -2434.3003, 12.9452>>
							fHeading = 		208.3998
						BREAK			
						CASE 2			
							model = 		schafter5
							vCoords = 		<<-1160.4210, -2431.1377, 12.9452>>
							fHeading = 		184.6001
						BREAK	
						CASE 3			
							model = 		cargobob2
							vCoords = 		<<-1146.6278, -2428.8887, 12.9452>>
							fHeading = 		238.7998
						BREAK
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_SEWER_TUNNEL_DRILL_1
			SWITCH eSubvariation
				CASE CHS_STD1_VARIATION_1
					SWITCH iVehicle				
						CASE 0			//Carrier Vehicle
							model = 		FLATBED
							vCoords = 		<<786.3642, 2287.9395, 47.7166>>
							fHeading = 		164.5994
						BREAK
					ENDSWITCH				
				BREAK
				CASE CHS_STD1_VARIATION_2
					SWITCH iVehicle				
						CASE 0			//Carrier Vehicle
							model = 		FLATBED
							vCoords = 		<<1216.5472, -3341.9543, 4.8014>>
							fHeading = 		88.5998
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_STD1_VARIATION_3
					SWITCH iVehicle				
						CASE 0			//Carrier Vehicle
							model = 		FLATBED
							vCoords = 		<<-2209.9294, 4252.1719, 46.3948>>
							fHeading = 		94.1999
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_SEWER_TUNNEL_DRILL_2
			SWITCH eSubvariation
				CASE CHS_STD2_VARIATION_1
					SWITCH iVehicle				
						CASE 0			
							model = 		MIXER
							vCoords = 		<<2012.3047, 4970.6333, 40.5390>>
							fHeading = 		301.9995
						BREAK			
						CASE 1			//Carrier Vehicle
							model = 		FLATBED
							vCoords = 		<<2009.7019, 4985.6045, 40.2094>>
							fHeading = 		235.2
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_STD2_VARIATION_2
					SWITCH iVehicle				
						CASE 0			
							model = 		MIXER
							vCoords = 		<<2641.4414, 2796.2722, 32.7538>>
							fHeading = 		26.3987
						BREAK			
						CASE 1			//Carrier Vehicle
							model = 		FLATBED
							vCoords = 		<<2647.5356, 2808.6338, 33.0320>>
							fHeading = 		160.9537
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_STD2_VARIATION_3
					SWITCH iVehicle				
						CASE 0			
							model = 		MIXER
							vCoords = 		<<1386.8303, -2069.4087, 50.9985>>
							fHeading = 		220.3971
						BREAK			
						CASE 1			//Carrier Vehicle
							model = 		FLATBED
							vCoords = 		<<1400.7566, -2066.6353, 50.9986>>
							fHeading = 		84.1999
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_RIOT_VAN
			SWITCH eSubvariation
				CASE CHS_RV_VARIATION_1
					SWITCH iVehicle				
						CASE 0			
							model = 		police3
							vCoords = 		<<1160.3199, -1649.1420, 35.9200>>
							fHeading = 		12.999
						BREAK			
						CASE 1			
							model = 		police3
							vCoords = 		<<1164.6810, -1658.0280, 35.7790>>
							fHeading = 		33.999
						BREAK			
						CASE 2			
							model = 		BMX
							vCoords = 		<<1165.6470, -1644.6980, 35.9200>>
							fHeading = 		68.743
						BREAK			
						CASE 3			
							model = 		BMX
							vCoords = 		<<1168.5070, -1645.8420, 35.9200>>
							fHeading = 		13.543
						BREAK			
						CASE 4			//Carrier Vehicle
							model = 		riot
							vCoords = 		<<1156.4220, -1655.9340, 35.5810>>
							fHeading = 		27.799
						BREAK			
						CASE 5			//Carrier Vehicle
							model = 		riot
							vCoords = 		<<-633.3650, -2166.9761, 50.4460>>
							fHeading = 		53.799
						BREAK			
						CASE 6			//Carrier Vehicle
							model = 		riot
							vCoords = 		<<-222.1570, -624.0490, 32.4760>>
							fHeading = 		69.2
						BREAK			
						CASE 7			//Carrier Vehicle
							model = 		riot
							vCoords = 		<<-1712.1600, 91.9432, 64.6783>>
							fHeading = 		292.9999
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_RV_VARIATION_2
					SWITCH iVehicle				
						CASE 0			
							model = 		sheriff
							vCoords = 		<<1780.9611, 3338.4709, 40.0000>>
							fHeading = 		195
						BREAK			
						CASE 1			
							model = 		sheriff
							vCoords = 		<<1783.8080, 3324.7839, 40.3350>>
							fHeading = 		143.4
						BREAK			
						CASE 2			
							model = 		sanchez
							vCoords = 		<<1776.2679, 3331.4050, 40.3190>>
							fHeading = 		161.399
						BREAK			
						CASE 3			
							model = 		sanchez
							vCoords = 		<<1774.6169, 3333.6421, 40.3400>>
							fHeading = 		188.599
						BREAK			
						CASE 4			//Carrier Vehicle
							model = 		riot
							vCoords = 		<<1773.2560, 3343.2151, 39.9990>>
							fHeading = 		110.599
						BREAK			
						CASE 5			//Carrier Vehicle
							model = 		riot
							vCoords = 		<<757.7840, 2263.0110, 48.2990>>
							fHeading = 		254.8
						BREAK			
						CASE 6			//Carrier Vehicle
							model = 		riot
							vCoords = 		<<-2139.9619, 2304.8899, 35.6880>>
							fHeading = 		268.8
						BREAK			
						CASE 7			//Carrier Vehicle
							model = 		riot
							vCoords = 		<<-217.7868, 3991.5598, 36.6021>>
							fHeading = 		5.6
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_RV_VARIATION_3
					SWITCH iVehicle				
						CASE 0			
							model = 		sheriff
							vCoords = 		<<1688.1910, 4923.2388, 41.0780>>
							fHeading = 		269.199
						BREAK			
						CASE 1			
							model = 		sheriff
							vCoords = 		<<1679.6720, 4920.5532, 41.0780>>
							fHeading = 		215.399
						BREAK			
						CASE 2			
							model = 		DAEMON
							vCoords = 		<<1689.0920, 4914.8760, 41.0780>>
							fHeading = 		276.198
						BREAK			
						CASE 3			
							model = 		DAEMON
							vCoords = 		<<1687.3879, 4913.7822, 41.0780>>
							fHeading = 		227.998
						BREAK			
						CASE 4			//Carrier Vehicle
							model = 		riot
							vCoords = 		<<1683.7531, 4927.5718, 41.0740>>
							fHeading = 		293.599
						BREAK			
						CASE 5			//Carrier Vehicle
							model = 		riot
							vCoords = 		<<-38.3530, 6304.2310, 30.3110>>
							fHeading = 		35.598
						BREAK			
						CASE 6			//Carrier Vehicle
							model = 		riot
							vCoords = 		<<693.8470, 3582.7219, 32.1080>>
							fHeading = 		278.798
						BREAK			
						CASE 7			//Carrier Vehicle
							model = 		riot
							vCoords = 		<<-2261.5088, 4349.9214, 42.3368>>
							fHeading = 		191.2
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_MERRYWEATHER_CONVOY
			SWITCH eSubvariation
				CASE CHS_MWC_LSIA
					SWITCH iVehicle				
						CASE 0		
							model = 		MESA3
							vCoords = 		<<-1348.2463, -2695.8208, 16.3>>
							fHeading = 		135.456
						BREAK			
						CASE 1			
							model = 		cargoplane
							vCoords = 		<<-1349.8176, -2699.1594, 12.9450>>
							fHeading = 		329.7989
						BREAK			
						CASE 2			
							model = 		MESA3
							vCoords = 		<<-1362.8429, -2736.9849, 12.9450>>
							fHeading = 		70.997
						BREAK			
						CASE 3			
							model = 		MESA3
							vCoords = 		<<-1373.4038, -2725.6101, 12.9449>>
							fHeading = 		212.5975
						BREAK			
						CASE 4			
							model = 		MESA3
							vCoords = 		<<-1355.4819, -2708.9871, 16.3>>
							fHeading = 		149.1462
						BREAK			
						CASE 5			
							model = 		INSURGENT
							vCoords = 		<<-1444.6876, 2735.0161, 9.8261>>
							fHeading = 		198.3501
						BREAK			
						CASE 6			
							model = 		buzzard
							vCoords = 		<<-1442.4031, 2729.4653, 34.9609>>
							fHeading = 		200.1501
						BREAK			
						CASE 7			//Carrier Vehicle
							model = 		SANDKING
							vCoords = 		<<-1440.1641, 2722.8174, 7.8147>>
							fHeading = 		201.9501
						BREAK			
						CASE 8			//Carrier Vehicle
							model = 		SANDKING
							vCoords = 		<<-1341.1293, -2684.3655, 16.3>>
							fHeading = 		150.2381
						BREAK						
					ENDSWITCH				
				BREAK
				CASE CHS_MWC_SANDY_SHORES
					SWITCH iVehicle				
						CASE 0			
							model = 		MESA3
							vCoords = 		<<1124.0876, 3026.0896, 42.8>>
							fHeading = 		271.5564
						BREAK			
						CASE 1			
							model = 		cargoplane
							vCoords = 		<<1127.2333, 3026.8833, 39.5341>>
							fHeading = 		104.7999
						BREAK			
						CASE 2			
							model = 		MESA3
							vCoords = 		<<1168.1874, 3030.8987, 39.4517>>
							fHeading = 		332.7991
						BREAK			
						CASE 3			
							model = 		MESA3
							vCoords = 		<<1161.7589, 3040.9224, 39.5164>>
							fHeading = 		79.399
						BREAK			
						CASE 4			
							model = 		MESA3
							vCoords = 		<<1137.2206, 3029.6792, 42.8>>
							fHeading = 		272.4868
						BREAK			
						CASE 5			
							model = 		INSURGENT
							vCoords = 		<<2576.4387, -674.7156, 52.4160>>
							fHeading = 		114.5998
						BREAK			
						CASE 6			
							model = 		buzzard
							vCoords = 		<<2569.4995, -677.5176, 81.3357>>
							fHeading = 		115.5998
						BREAK			
						CASE 7			//Carrier Vehicle
							model = 		SANDKING
							vCoords = 		<<2563.9807, -680.2990, 53.4104>>
							fHeading = 		114.3998
						BREAK			
						CASE 8			//Carrier Vehicle
							model = 		SANDKING
							vCoords = 		<<1112.0402, 3023.0361, 42.8>>
							fHeading = 		285.2199
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_EXPLOSIVES_1
			SWITCH eSubvariation
				CASE CHS_EXP1_VARIATION_1
					SWITCH iVehicle				
						CASE 0			
							model = 		JOURNEY
							vCoords = 		<<1738.1700, 3869.0320, 33.6880>>
							fHeading = 		157.197
						BREAK			
						CASE 1			
							model = 		ratloader
							vCoords = 		<<1751.7690, 3861.1860, 33.5320>>
							fHeading = 		81.396
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_EXP1_VARIATION_2
					SWITCH iVehicle				
						CASE 0			
							model = 		JOURNEY
							vCoords = 		<<-37.4890, 3018.7991, 39.6880>>
							fHeading = 		240.6
						BREAK			
						CASE 1			
							model = 		ratloader
							vCoords = 		<<-31.0300, 3029.4431, 40.0180>>
							fHeading = 		173.8
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_FLIGHT_SCHEDULE
			SWITCH eSubvariation
				CASE CHS_FS_ELYSIAN_ISLAND
					SWITCH iVehicle				
						CASE 0			
							model = 		MESA3
							vCoords = 		<<444.6100, -3076.9390, 5.0700>>
							fHeading = 		165.6
						BREAK			
						CASE 1			
							model = 		MESA3
							vCoords = 		<<448.8090, -3073.2839, 5.0700>>
							fHeading = 		185.4
						BREAK			
						CASE 2			
							model = 		buzzard
							vCoords = 		<<446.8930, -3060.9451, 5.0700>>
							fHeading = 		303.4
						BREAK			
						CASE 3			
							model = 		titan
							vCoords = 		<<-1696.6470, 5458.8882, 200.6300>>
							fHeading = 		189.397
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_FS_SANDY_SHORES
					SWITCH iVehicle				
						CASE 0			
							model = 		MESA3
							vCoords = 		<<1705.3260, 3278.6331, 40.1470>>
							fHeading = 		192.6
						BREAK			
						CASE 1			
							model = 		MESA3
							vCoords = 		<<1695.0990, 3275.1841, 40.0670>>
							fHeading = 		48.6
						BREAK			
						CASE 2			
							model = 		buzzard
							vCoords = 		<<1685.9110, 3269.1580, 39.8210>>
							fHeading = 		333.798
						BREAK			
						CASE 3			
							model = 		titan
							vCoords = 		<<-1893.9940, -2596.2109, 200.8790>>
							fHeading = 		333.798
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_FS_POWER_STATION
					SWITCH iVehicle				
						CASE 0			
							model = 		MESA3
							vCoords = 		<<2678.9619, 1439.9280, 23.5290>>
							fHeading = 		194.4
						BREAK			
						CASE 1			
							model = 		MESA3
							vCoords = 		<<2674.8589, 1436.5310, 23.5010>>
							fHeading = 		162.6
						BREAK			
						CASE 2			
							model = 		buzzard
							vCoords = 		<<2655.0391, 1439.7480, 23.5100>>
							fHeading = 		24.199
						BREAK			
						CASE 3			
							model = 		titan
							vCoords = 		<<3667.5884, 5086.4292, 209.3509>>
							fHeading = 		140.1996
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_MERRYWEATHER_TEST_SITE
			SWITCH eSubvariation
				CASE CHS_MWTS_VARIATION_1
					SWITCH iVehicle				
						CASE 0			
							model = 		INSURGENT
							vCoords = 		<<351.4543, 3419.3374, 35.3250>>
							fHeading = 		212.1987
						BREAK			
						CASE 1				//Carrier Vehicle
							model = 		SANDKING
							vCoords = 		<<325.6933, 3408.3457, 35.7190>>
							fHeading = 		320.7984
						BREAK			
						CASE 2				//Carrier Vehicle
							model = 		SANDKING
							vCoords = 		<<331.3647, 3423.6172, 35.4437>>
							fHeading = 		229.398
						BREAK		
						CASE 3			
							model = 		barrage
							vCoords = 		<<319.5577, 3397.1472, 35.5112>>
							fHeading = 		302.1999
						BREAK		
					ENDSWITCH				
				BREAK
				CASE CHS_MWTS_VARIATION_2
					SWITCH iVehicle				
						CASE 0			
							model = 		INSURGENT
							vCoords = 		<<551.4200, -2841.5969, 5.0440>>
							fHeading = 		261.799
						BREAK			
						CASE 1				//Carrier Vehicle
							model = 		SANDKING
							vCoords = 		<<539.3760, -2843.9590, 5.0450>>
							fHeading = 		32.998
						BREAK			
						CASE 2				//Carrier Vehicle
							model = 		SANDKING
							vCoords = 		<<534.7550, -2834.9971, 5.0450>>
							fHeading = 		74.798
						BREAK		
						CASE 3			
							model = 		barrage
							vCoords = 		<<515.6918, -2823.8599, 5.0454>>
							fHeading = 		26.6
						BREAK		
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	SWITCH eVariation
		CASE CHV_EXPLOSIVES_2
			SWITCH eSubvariation
				CASE CHS_EXP2_VARIATION_1
					SWITCH iVehicle				
						CASE 0			
							model = 		Rumpo3
							vCoords = 		<<841.2600, -1990.7330, 28.3010>>
							fHeading = 		214.6
						BREAK			
						CASE 1			
							model = 		Rumpo3
							vCoords = 		<<848.2160, -1989.1949, 28.3010>>
							fHeading = 		239
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_EXP2_VARIATION_2
					SWITCH iVehicle				
						CASE 0			
							model = 		Rumpo3
							vCoords = 		<<-1893.9830, -3037.5769, 12.9440>>
							fHeading = 		102.6
						BREAK			
						CASE 1			
							model = 		Rumpo3
							vCoords = 		<<-1889.8540, -3029.9109, 12.9440>>
							fHeading = 		133.8
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_VAULT_LASER_1
			SWITCH eSubvariation
				CASE CHS_VL1_GRAND_SENORA_DESERT
					SWITCH iVehicle				
						CASE 0			
							model = 		cargobob4
							vCoords = 		<<1045.3600, 3191.8611, 37.9970>>
							fHeading = 		249.2
						BREAK			
						CASE 1			
							model = 		cargobob4
							vCoords = 		<<1020.0930, 3190.1340, 37.575>>
							fHeading = 		168.6
						BREAK			
						CASE 2			
							model = 		MESA3
							vCoords = 		<<1037.6080, 3202.4851, 37.5240>>
							fHeading = 		145.999
						BREAK			
						CASE 3			
							model = 		MESA3
							vCoords = 		<<1030.7140, 3186.3640, 37.7670>>
							fHeading = 		350.799
						BREAK			
						CASE 4			
							model = 		BARRACKS
							vCoords = 		<<1009.2787, 3233.4517, 37.3050>>
							fHeading = 		101.3999
						BREAK			
						CASE 5			
							model = 		BARRACKS
							vCoords = 		<<1008.1710, 3238.9211, 37.3360>>
							fHeading = 		101.3999
						BREAK			
						CASE 6			
							model = 		halftrack
							vCoords = 		<<1034.8798, 3241.3296, 37.0104>>
							fHeading = 		297.1997
						BREAK			
						CASE 7			
							model = 		halftrack
							vCoords = 		<<1036.5935, 3174.0732, 38.1282>>
							fHeading = 		225.3993
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_VL1_LA_MESA
					SWITCH iVehicle				
						CASE 0			
							model = 		cargobob4
							vCoords = 		<<1238.2841, -2369.1060, 48.2720>>
							fHeading = 		196.2
						BREAK			
						CASE 1			
							model = 		cargobob4
							vCoords = 		<<1252.1890, -2357.5220, 48.8390>>
							fHeading = 		320.8
						BREAK			
						CASE 2			
							model = 		MESA3
							vCoords = 		<<1244.5790, -2350.2019, 48.9790>>
							fHeading = 		174
						BREAK			
						CASE 3			
							model = 		MESA3
							vCoords = 		<<1253.4730, -2370.4141, 48.2560>>
							fHeading = 		314
						BREAK			
						CASE 4			
							model = 		BARRACKS
							vCoords = 		<<1253.4706, -2401.0713, 47.1455>>
							fHeading = 		215
						BREAK			
						CASE 5			
							model = 		BARRACKS
							vCoords = 		<<1259.1577, -2396.9817, 47.6180>>
							fHeading = 		213.2002
						BREAK			
						CASE 6			
							model = 		halftrack
							vCoords = 		<<1267.2504, -2320.1150, 49.4728>>
							fHeading = 		342.4001
						BREAK			
						CASE 7			
							model = 		halftrack
							vCoords = 		<<1235.9036, -2395.1135, 46.6553>>
							fHeading = 		70.2001
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_VAULT_LASER_2
			SWITCH eSubvariation
				CASE CHS_VL2_POWER_STATION
					SWITCH iVehicle				
						CASE 0			
							model = 		dubsta3
							vCoords = 		<<2827.3821, 1463.5990, 23.5550>>
							fHeading = 		225.999
						BREAK			
						CASE 1			
							model = 		dubsta3
							vCoords = 		<<2833.5969, 1461.4600, 23.5550>>
							fHeading = 		209.199
						BREAK			
						CASE 2			
							model = 		mule4
							vCoords = 		<<2845.6079, 1469.9130, 23.5550>>
							fHeading = 		352.597
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_VL2_ZANCUDO
					SWITCH iVehicle				
						CASE 0			
							model = 		dubsta3
							vCoords = 		<<-2014.5330, 3388.3401, 30.2000>>
							fHeading = 		198.2
						BREAK			
						CASE 1			
							model = 		dubsta3
							vCoords = 		<<-2019.6470, 3391.7639, 30.1200>>
							fHeading = 		169.2
						BREAK			
						CASE 2			
							model = 		mule4
							vCoords = 		<<-2030.5420, 3383.8569, 30.1810>>
							fHeading = 		120.2
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_VALET_SERVICE
			SWITCH eSubvariation
				CASE CHS_VS_VARIATION_1
					SWITCH iVehicle				
						CASE 0			
							model = 		MINIVAN
							vCoords = 		<<-1615.9919, -502.4080, 34.9760>>
							fHeading = 		51.599
						BREAK			
						CASE 1			//Carrier Vehicle
							model = 		SANDKING
							vCoords = 		<<-1507.9880, -696.1310, 26.7670>>
							fHeading = 		50.598
						BREAK			
						CASE 2			//Carrier Vehicle
							model = 		SANDKING
							vCoords = 		<<-1334.2115, -519.4623, 31.3541>>
							fHeading = 		124.3998
						BREAK			
					ENDSWITCH								
				BREAK
				
				CASE CHS_VS_VARIATION_2
					SWITCH iVehicle				
						CASE 0			
							model = 		MINIVAN
							vCoords = 		<<-677.3030, -2236.6169, 4.8050>>
							fHeading = 		180
						BREAK			
						CASE 1			//Carrier Vehicle
							model = 		SANDKING
							vCoords = 		<<-567.1090, -2068.7261, 5.8470>>
							fHeading = 		223.2
						BREAK			
						CASE 2			//Carrier Vehicle
							model = 		SANDKING
							vCoords = 		<<-178.3065, -2182.2539, 9.2924>>
							fHeading = 		108.5998
						BREAK			
					ENDSWITCH				
				BREAK
				
				CASE CHS_VS_VARIATION_3
					SWITCH iVehicle				
						CASE 0			
							model = 		MINIVAN
							vCoords = 		<<-506.3440, -452.1300, 33.2010>>
							fHeading = 		260.6
						BREAK			
						CASE 1			//Carrier Vehicle
							model = 		SANDKING
							vCoords = 		<<-398.4860, -380.3060, 31.0900>>
							fHeading = 		83.2
						BREAK			
						CASE 2			//Carrier Vehicle
							model = 		SANDKING
							vCoords = 		<<-937.2791, -256.4674, 38.3117>>
							fHeading = 		241.7998
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_STEALTH_OUTFITS
			SWITCH eSubvariation
				CASE CHS_SO_VARIATION_1
					SWITCH iVehicle				
						CASE 0			
							model = 		barrage
							vCoords = 		<<3574.8699, 3665.4343, 32.8915>>
							fHeading = 		282.6
						BREAK			
						CASE 1			
							model = 		MESA3
							vCoords = 		<<3567.8569, 3657.7151, 32.9070>>
							fHeading = 		240.399
						BREAK			
						CASE 2			
							model = 		MESA3
							vCoords = 		<<3594.1931, 3662.1970, 32.8720>>
							fHeading = 		253.999
						BREAK			
						CASE 3			
							model = 		INSURGENT
							vCoords = 		<<3495.2185, 3678.8616, 32.8884>>
							fHeading = 		270.5964
						BREAK			
						CASE 4			
							model = 		INSURGENT
							vCoords = 		<<3476.0637, 3669.4417, 32.8832>>
							fHeading = 		173.1977
						BREAK
					ENDSWITCH				
				BREAK
				
				CASE CHS_SO_VARIATION_2
					SWITCH iVehicle				
						CASE 0			
							model = 		barrage
							vCoords = 		<<-1779.6820, 3087.7129, 31.8011>>
							fHeading = 		84.799
						BREAK			
						CASE 1			
							model = 		MESA3
							vCoords = 		<<-1772.3960, 3099.0371, 31.7990>>
							fHeading = 		127.599
						BREAK			
						CASE 2			
							model = 		MESA3
							vCoords = 		<<-1802.8831, 3096.2671, 31.8420>>
							fHeading = 		28.399
						BREAK			
						CASE 3			
							model = 		INSURGENT
							vCoords = 		<<-1747.9503, 3096.7129, 31.8302>>
							fHeading = 		278.7958
						BREAK			
						CASE 4			
							model = 		INSURGENT
							vCoords = 		<<-1751.1056, 3054.4902, 31.8289>>
							fHeading = 		199.796
						BREAK			
		
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	SWITCH eVariation
		CASE CHV_FIREFIGHTER_OUTFIT_2
			SWITCH eSubvariation
				CASE CHS_FF2_VARIATION_1
					SWITCH iVehicle				
						CASE 0			
							model = 		BODHI2
							vCoords = 		<<1430.0601, 2788.7490, 51.1660>>
							fHeading = 		94
						BREAK			
						CASE 1			
							model = 		sheriff
							vCoords = 		<<1427.0341, 2799.0339, 51.4180>>
							fHeading = 		127.199
						BREAK			
						CASE 2			//Carrier Vehicle
							model = 		firetruk
							vCoords = 		<<1435.2090, 2797.9419, 51.4760>>
							fHeading = 		105.599
						BREAK			
					ENDSWITCH
				BREAK
				CASE CHS_FF2_VARIATION_2
					SWITCH iVehicle				
						CASE 0			
							model = 		ZION
							vCoords = 		<<-781.1730, 579.5140, 125.4500>>
							fHeading = 		219.4
						BREAK			
						CASE 1			
							model = 		police3
							vCoords = 		<<-788.0350, 576.6770, 125.4230>>
							fHeading = 		241.6
						BREAK			
						CASE 2			
							model = 		firetruk
							vCoords = 		<<-790.5180, 585.6180, 125.9480>>
							fHeading = 		216.6
						BREAK			
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_PLASTIC_EXPLOSIVES
			SWITCH eSubvariation
				CASE CHS_PE_VARIATION_1
					SWITCH iVehicle				
						CASE 0			
							model = 		MESA3
							vCoords = 		<<3817.8130, 4463.2168, 2.8070>>
							fHeading = 		282
						BREAK			
						CASE 1			
							model = 		MESA3
							vCoords = 		<<3821.6399, 4458.9321, 2.5080>>
							fHeading = 		252.6
						BREAK			
						CASE 2			
							model = 		DINGHY
							vCoords = 		<<3870.4939, 4463.8691, -0.1870>>
							fHeading = 		-0.004
						BREAK			
						CASE 3			
							model = 		DINGHY2
							vCoords = 		<<4116.0591, 4700.1372, 1.7920>>
							fHeading = 		110.496
						BREAK			
						CASE 4			
							model = 		DINGHY2
							vCoords = 		<<4100.3848, 4725.7588, 1.5850>>
							fHeading = 		239.046
						BREAK			
						CASE 5			
							model = 		buzzard2
							vCoords = 		<<4107.7148, 4714.2568, 18.2030>>
							fHeading = 		136.246
						BREAK
						CASE 6			
							model = 		DINGHY2
							vCoords = 		<<3854.9719, 4454.2988, -0.1870>>
							fHeading = 		268.596
						BREAK			
						CASE 7			
							model = 		DINGHY2
							vCoords = 		<<3861.1509, 4469.5078, -0.0000>>
							fHeading = 		277.596
						BREAK			
						CASE 8			
							model = 		DINGHY2
							vCoords = 		<<4093.4761, 4705.4590, 0.2890>>
							fHeading = 		178.545
						BREAK
					ENDSWITCH
				BREAK
				CASE CHS_PE_VARIATION_2
					SWITCH iVehicle				
						CASE 0			
							model = 		MESA3
							vCoords = 		<<-1579.9871, 5165.2808, 18.5760>>
							fHeading = 		22
						BREAK			
						CASE 1			
							model = 		MESA3
							vCoords = 		<<-1575.0840, 5168.3848, 18.5670>>
							fHeading = 		347
						BREAK			
						CASE 2			
							model = 		DINGHY
							vCoords = 		<<-1602.4550, 5259.9268, -0.1870>>
							fHeading = 		23.998
						BREAK			
						CASE 3			
							model = 		DINGHY2
							vCoords = 		<<-1824.5341, 5657.1001, 0.8380>>
							fHeading = 		101.539
						BREAK			
						CASE 4			
							model = 		DINGHY2
							vCoords = 		<<-1839.9120, 5671.6489, 0.4420>>
							fHeading = 		251.339
						BREAK			
						CASE 5			
							model = 		buzzard2
							vCoords = 		<<-1833.3459, 5663.8828, 10.7860>>
							fHeading = 		203.939
						BREAK
						CASE 6			
							model = 		DINGHY2
							vCoords = 		<<-1597.8856, 5248.4307, -0.1876>>
							fHeading = 		12.998
						BREAK			
						CASE 7			
							model = 		DINGHY2
							vCoords = 		<<-1613.4238, 5266.9331, 0.0787>>
							fHeading = 		290.1978
						BREAK			
						CASE 8			
							model = 		DINGHY2
							vCoords = 		<<-1843.1183, 5654.9478, 0.5130>>
							fHeading = 		220.9977
						BREAK
					ENDSWITCH
				BREAK
				CASE CHS_PE_VARIATION_3
					SWITCH iVehicle				
						CASE 0			
							model = 		MESA3
							vCoords = 		<<114.3670, -2260.9480, 5.0990>>
							fHeading = 		79
						BREAK			
						CASE 1			
							model = 		MESA3
							vCoords = 		<<111.2550, -2252.5581, 5.0870>>
							fHeading = 		160.4
						BREAK			
						CASE 2			
							model = 		DINGHY
							vCoords = 		<<81.8730, -2262.3049, 0.6100>>
							fHeading = 		89.446
						BREAK			
						CASE 3			
							model = 		DINGHY2
							vCoords = 		<<-71.3200, -2312.2251, 0.1110>>
							fHeading = 		127.045
						BREAK			
						CASE 4			
							model = 		DINGHY2
							vCoords = 		<<-99.6360, -2306.8379, 0.2580>>
							fHeading = 		259.045
						BREAK			
						CASE 5			
							model = 		buzzard2
							vCoords = 		<<-84.1120, -2310.2490, 12.5230>>
							fHeading = 		193.445
						BREAK
						CASE 6			
							model = 		DINGHY2
							vCoords = 		<<82.2719, -2249.6938, 0.2861>>
							fHeading = 		89.446
						BREAK			
						CASE 7			
							model = 		DINGHY2
							vCoords = 		<<67.9466, -2257.0337, 0.5453>>
							fHeading = 		169.4457
						BREAK			
						CASE 8			
							model = 		DINGHY2
							vCoords = 		<<-83.7563, -2294.6399, 0.9535>>
							fHeading = 		194.0452
						BREAK
					ENDSWITCH
				BREAK
				CASE CHS_PE_VARIATION_4
					SWITCH iVehicle				
						CASE 0			
							model = 		MESA3
							vCoords = 		<<721.8730, 4172.2891, 39.7090>>
							fHeading = 		121.6
						BREAK			
						CASE 1			
							model = 		MESA3
							vCoords = 		<<728.3910, 4173.0078, 39.7090>>
							fHeading = 		145.6
						BREAK			
						CASE 2			
							model = 		DINGHY
							vCoords = 		<<707.7160, 4094.1799, 30.0830>>
							fHeading = 		181.198
						BREAK			
						CASE 3			
							model = 		DINGHY2
							vCoords = 		<<363.1700, 3953.7490, 30.0000>>
							fHeading = 		133.597
						BREAK			
						CASE 4			
							model = 		DINGHY2
							vCoords = 		<<336.8890, 3958.8689, 30.2320>>
							fHeading = 		241.396
						BREAK			
						CASE 5			
							model = 		buzzard2
							vCoords = 		<<353.7550, 3956.8621, 41.2430>>
							fHeading = 		241.396
						BREAK
						CASE 6			
							model = 		DINGHY2
							vCoords = 		<<707.4127, 4106.1753, 29.7490>>
							fHeading = 		181.198
						BREAK			
						CASE 7			
							model = 		DINGHY2
							vCoords = 		<<718.5231, 4089.1492, 30.0000>>
							fHeading = 		123.9979
						BREAK			
						CASE 8			
							model = 		DINGHY2
							vCoords = 		<<353.5311, 3970.9341, 29.6421>>
							fHeading = 		261.1975
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_VAULT_DRILL_2
			SWITCH eSubvariation
				CASE CHS_VD2_VARIATION_1
					SWITCH iVehicle				
						CASE 0			
							model = 		police3
							vCoords = 		<<322.0430, -273.7410, 52.9070>>
							fHeading = 		208.2
						BREAK			
						CASE 1			
							model = 		police3
							vCoords = 		<<310.6270, -268.8710, 52.9370>>
							fHeading = 		226.4
						BREAK			
						CASE 2			
							model = 		GRANGER
							vCoords = 		<<298.8780, -270.6050, 53.0170>>
							fHeading = 		341
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_VD2_VARIATION_2
					SWITCH iVehicle				
						CASE 0			
							model = 		police3
							vCoords = 		<<-2972.9629, 484.4100, 14.2690>>
							fHeading = 		320.4
						BREAK			
						CASE 1			
							model = 		police3
							vCoords = 		<<-2971.5061, 477.3970, 14.4280>>
							fHeading = 		339.4
						BREAK			
						CASE 2			
							model = 		GRANGER
							vCoords = 		<<-2965.4951, 491.1480, 14.2890>>
							fHeading = 		86.4
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_GUARD_PATROL_ROUTES
			SWITCH eSubvariation
				CASE CHS_GP_VARIATION_1
					SWITCH iVehicle				
						CASE 0			
							model = 		kamacho
							vCoords = 		<<-399.9440, 1196.8931, 324.6416>>
							fHeading = 		344.9999
						BREAK			
						CASE 1			
							model = 		kamacho
							vCoords = 		<<-390.4505, 1189.6005, 324.6416>>
							fHeading = 		278.1998
						BREAK			
						CASE 2			
							model = 		BALLER
							vCoords = 		<<-391.1296, 1231.5957, 324.6666>>
							fHeading = 		32.4
						BREAK			
						CASE 3			
							model = 		BALLER
							vCoords = 		<<-397.9318, 1235.4734, 324.6416>>
							fHeading = 		72.9999
						BREAK			
						CASE 4			
							model = 		JACKAL
							vCoords = 		<<-404.1270, 1217.8500, 324.6420>>
							fHeading = 		344
						BREAK			
						CASE 5			
							model = 		JACKAL
							vCoords = 		<<-388.0600, 1197.9890, 324.6420>>
							fHeading = 		273.799
						BREAK			
						CASE 6			
							model = 		ZION
							vCoords = 		<<-398.2380, 1203.2240, 324.6420>>
							fHeading = 		163.599
						BREAK			
						CASE 7			
							model = 		ZION
							vCoords = 		<<-414.6330, 1223.6429, 324.6420>>
							fHeading = 		53.599
						BREAK			
						CASE 8			
							model = 		ORACLE
							vCoords = 		<<-416.8280, 1215.0110, 324.6420>>
							fHeading = 		231.798
						BREAK			
						CASE 9			
							model = 		ORACLE
							vCoords = 		<<-389.3180, 1193.9690, 324.6420>>
							fHeading = 		97.198
						BREAK			
						CASE 10			
							model = 		FUGITIVE
							vCoords = 		<<-386.5010, 1206.5081, 324.6420>>
							fHeading = 		97.198
						BREAK			
						CASE 11			
							model = 		FUGITIVE
							vCoords = 		<<-409.0580, 1199.5341, 324.6420>>
							fHeading = 		163.798
						BREAK			
						CASE 12			
							model = 		REGINA
							vCoords = 		<<-419.4600, 1206.9250, 324.6420>>
							fHeading = 		51.598
						BREAK			
						CASE 13			
							model = 		REGINA
							vCoords = 		<<-384.8150, 1210.8400, 324.6420>>
							fHeading = 		98.198
						BREAK			
						CASE 14			
							model = 		SCHAFTER2
							vCoords = 		<<-396.4330, 1209.4620, 324.6420>>
							fHeading = 		343.797
						BREAK			
						CASE 15			
							model = 		SCHAFTER2
							vCoords = 		<<-412.3890, 1231.7810, 324.6420>>
							fHeading = 		50.797
						BREAK			
						CASE 16			
							model = 		DOMINATOR
							vCoords = 		<<-405.7760, 1211.6860, 324.6420>>
							fHeading = 		343.597
						BREAK			
						CASE 17			
							model = 		DOMINATOR
							vCoords = 		<<-410.8280, 1235.7660, 324.6420>>
							fHeading = 		231.797
						BREAK			
						CASE 18			
							model = 		FELON2
							vCoords = 		<<-382.2690, 1218.8260, 324.6420>>
							fHeading = 		275.197
						BREAK			
						CASE 19			//Carrier Vehicle
							model = 		FELON2
							vCoords = 		<<-413.6150, 1227.8149, 324.6420>>
							fHeading = 		50.796
						BREAK			
						CASE 20			//Carrier Vehicle
							model = 		FELON2
							vCoords = 		<<-382.9875, 1215.0859, 324.6416>>
							fHeading = 		275.5958
						BREAK						
					ENDSWITCH
				BREAK
				CASE CHS_GP_VARIATION_2
					SWITCH iVehicle				
						CASE 0			
							model = 		kamacho
							vCoords = 		<<-829.8136, -756.9855, 21.3628>>
							fHeading = 		270.5997
						BREAK			
						CASE 1			
							model = 		kamacho
							vCoords = 		<<-841.0653, -760.7867, 21.0302>>
							fHeading = 		89.9995
						BREAK			
						CASE 2			
							model = 		BALLER
							vCoords = 		<<-812.1882, -746.8392, 22.3396>>
							fHeading = 		235.9985
						BREAK			
						CASE 3			
							model = 		BALLER
							vCoords = 		<<-810.4868, -753.5585, 21.6625>>
							fHeading = 		270.1985
						BREAK			
						CASE 4			
							model = 		JACKAL
							vCoords = 		<<-822.7357, -760.6357, 21.0142>>
							fHeading = 		270.3984
						BREAK			
						CASE 5			
							model = 		JACKAL
							vCoords = 		<<-810.8791, -768.2141, 20.3120>>
							fHeading = 		89.3981
						BREAK			
						CASE 6			
							model = 		ZION
							vCoords = 		<<-841.1142, -768.2342, 20.3136>>
							fHeading = 		89.3981
						BREAK			
						CASE 7			
							model = 		ZION
							vCoords = 		<<-822.8734, -764.3426, 20.6513>>
							fHeading = 		89.3981
						BREAK			
						CASE 8			
							model = 		ORACLE
							vCoords = 		<<-810.3039, -760.6367, 20.9939>>
							fHeading = 		269.7978
						BREAK			
						CASE 9			
							model = 		ORACLE
							vCoords = 		<<-830.0663, -764.2704, 20.6594>>
							fHeading = 		90.7976
						BREAK			
						CASE 10			
							model = 		FUGITIVE
							vCoords = 		<<-840.7529, -771.9199, 20.0063>>
							fHeading = 		270.1974
						BREAK			
						CASE 11			
							model = 		FUGITIVE
							vCoords = 		<<-810.5962, -756.9427, 21.3362>>
							fHeading = 		89.5973
						BREAK			
						CASE 12			
							model = 		REGINA
							vCoords = 		<<-822.8769, -768.3004, 20.3062>>
							fHeading = 		89.9967
						BREAK			
						CASE 13			
							model = 		REGINA
							vCoords = 		<<-841.0781, -764.4578, 20.6410>>
							fHeading = 		269.5966
						BREAK			
						CASE 14			
							model = 		SCHAFTER2
							vCoords = 		<<-829.9478, -760.6412, 21.0310>>
							fHeading = 		269.5966
						BREAK			
						CASE 15			
							model = 		SCHAFTER2
							vCoords = 		<<-822.8523, -756.9753, 21.3550>>
							fHeading = 		90.5963
						BREAK			
						CASE 16			
							model = 		DOMINATOR
							vCoords = 		<<-835.9357, -746.1130, 22.4241>>
							fHeading = 		90.5963
						BREAK			
						CASE 17			
							model = 		DOMINATOR
							vCoords = 		<<-810.7714, -772.0289, 19.9949>>
							fHeading = 		269.3959
						BREAK			
						CASE 18			
							model = 		FELON2
							vCoords = 		<<-827.5715, -746.0598, 22.4289>>
							fHeading = 		269.3959
						BREAK			
						CASE 19			//Carrier Vehicle
							model = 		FELON2
							vCoords = 		<<-810.5333, -764.4266, 20.6400>>
							fHeading = 		269.3959
						BREAK			
						CASE 20			//Carrier Vehicle
							model = 		FELON2
							vCoords = 		<<-829.9433, -768.2315, 20.3126>>
							fHeading = 		269.3959
						BREAK			
					ENDSWITCH
				BREAK
				CASE CHS_GP_VARIATION_3
					SWITCH iVehicle				
						CASE 0			
							model = 		kamacho
							vCoords = 		<<-325.5283, -952.8741, 30.0806>>
							fHeading = 		68.9999
						BREAK			
						CASE 1			
							model = 		kamacho
							vCoords = 		<<-337.5358, -952.4479, 30.0806>>
							fHeading = 		250.1997
						BREAK			
						CASE 2			
							model = 		BALLER
							vCoords = 		<<-316.5004, -928.4941, 30.0806>>
							fHeading = 		69.5998
						BREAK			
						CASE 3			
							model = 		BALLER
							vCoords = 		<<-311.8108, -908.8322, 30.0777>>
							fHeading = 		348.7988
						BREAK			
						CASE 4			
							model = 		JACKAL
							vCoords = 		<<-337.1450, -903.5420, 30.0770>>
							fHeading = 		347.599
						BREAK			
						CASE 5			
							model = 		JACKAL
							vCoords = 		<<-336.4000, -948.8690, 30.0810>>
							fHeading = 		70.399
						BREAK			
						CASE 6			
							model = 		ZION
							vCoords = 		<<-345.2950, -932.2540, 30.0810>>
							fHeading = 		248.999
						BREAK			
						CASE 7			
							model = 		ZION
							vCoords = 		<<-319.0200, -907.3470, 30.0780>>
							fHeading = 		167.999
						BREAK			
						CASE 8			
							model = 		ORACLE
							vCoords = 		<<-333.7040, -941.9190, 30.0810>>
							fHeading = 		250.599
						BREAK			
						CASE 9			
							model = 		ORACLE
							vCoords = 		<<-322.7110, -906.4350, 30.0790>>
							fHeading = 		347.998
						BREAK			
						CASE 10			
							model = 		FUGITIVE
							vCoords = 		<<-332.7480, -938.0970, 30.0810>>
							fHeading = 		70.198
						BREAK			
						CASE 11			
							model = 		FUGITIVE
							vCoords = 		<<-341.3430, -921.7400, 30.0810>>
							fHeading = 		69.998
						BREAK			
						CASE 12			
							model = 		REGINA
							vCoords = 		<<-342.6840, -925.0900, 30.0810>>
							fHeading = 		249.598
						BREAK			
						CASE 13			
							model = 		REGINA
							vCoords = 		<<-329.8060, -904.9540, 30.0790>>
							fHeading = 		167.598
						BREAK			
						CASE 14			
							model = 		SCHAFTER2
							vCoords = 		<<-327.4030, -924.3540, 30.0810>>
							fHeading = 		70.197
						BREAK			
						CASE 15			
							model = 		SCHAFTER2
							vCoords = 		<<-333.5180, -904.1580, 30.0780>>
							fHeading = 		168.597
						BREAK			
						CASE 16			
							model = 		DOMINATOR
							vCoords = 		<<-330.0750, -931.3850, 30.0810>>
							fHeading = 		249.797
						BREAK			
						CASE 17			
							model = 		DOMINATOR
							vCoords = 		<<-320.0200, -938.8830, 30.0810>>
							fHeading = 		249.797
						BREAK			
						CASE 18			
							model = 		FELON2
							vCoords = 		<<-324.0890, -949.5180, 30.0810>>
							fHeading = 		249.797
						BREAK			
						CASE 19			//Carrier Vehicle
							model = 		FELON2
							vCoords = 		<<-328.5620, -928.0100, 30.0810>>
							fHeading = 		249.197
						BREAK			
						CASE 20			//Carrier Vehicle
							model = 		FELON2
							vCoords = 		<<-344.0960, -928.5662, 30.0806>>
							fHeading = 		69.1967
						BREAK						
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_POLICE_AUCTION
			SWITCH iVehicle				
				CASE 0			
					SWITCH eSubvariation
						CASE CHS_PA_VARIATION_1
							model = 		police3
							vCoords = 		<<827.8975, -1271.1481, 25.2645>>
							fHeading = 		0
						BREAK
						
						CASE CHS_PA_VARIATION_2
							model = 		police3
							vCoords = 		<<-559.9183, -147.0545, 37.1052>>
							fHeading = 		56.3995
						BREAK	
					ENDSWITCH
				BREAK			
				CASE 1			
					model = 		BALLER
					vCoords = 		<<996.4140, -3014.4580, -40.6470>>
					fHeading = 		85.8
				BREAK			
				CASE 2			
					model = 		BALLER
					vCoords = 		<<964.3920, -3025.1140, -40.6470>>
					fHeading = 		3.4
				BREAK			
				CASE 3			
					model = 		DILETTANTE
					vCoords = 		<<1000.4560, -3005.1321, -40.6470>>
					fHeading = 		281
				BREAK			
				CASE 4			
					model = 		DILETTANTE
					vCoords = 		<<970.0440, -3024.4851, -40.6470>>
					fHeading = 		182.749
				BREAK			
				CASE 5			
					model = 		FELON2
					vCoords = 		<<959.8400, -3025.3391, -40.6470>>
					fHeading = 		176.349
				BREAK			
				CASE 6			
					model = 		FELON2
					vCoords = 		<<1006.4200, -3007.1350, -40.6470>>
					fHeading = 		183.549
				BREAK			
				CASE 7			
					model = 		EMPEROR
					vCoords = 		<<1001.5520, -3009.5491, -40.6470>>
					fHeading = 		269.349
				BREAK			
				CASE 8			
					model = 		EMPEROR
					vCoords = 		<<959.0460, -3031.5549, -40.6470>>
					fHeading = 		269.349
				BREAK			
				CASE 9			
					model = 		DOMINATOR
					vCoords = 		<<963.7175, -3018.8521, -40.6470>>
					fHeading = 		270.748
				BREAK			
				CASE 10			
					model = 		DOMINATOR
					vCoords = 		<<996.2020, -3009.1030, -40.6470>>
					fHeading = 		2.149
				BREAK			
				CASE 11			
					model = 		BANSHEE
					vCoords = 		<<1000.4810, -2999.9199, -40.6470>>
					fHeading = 		358.749
				BREAK			
				CASE 12			
					model = 		BANSHEE
					vCoords = 		<<958.2500, -3019.1980, -40.6470>>
					fHeading = 		270.749
				BREAK			
				CASE 13			
					model = 		MANANA
					vCoords = 		<<995.3050, -3000.6890, -40.6470>>
					fHeading = 		270.749
				BREAK			
				CASE 14			
					model = 		MANANA
					vCoords = 		<<969.5920, -3018.4031, -40.6470>>
					fHeading = 		96.349
				BREAK			
				CASE 15			
					model = 		CHEETAH
					vCoords = 		<<995.3710, -3020.2900, -40.6470>>
					fHeading = 		176.948
				BREAK			
				CASE 16			
					model = 		CHEETAH
					vCoords = 		<<966.9480, -3014.3081, -40.6470>>
					fHeading = 		176.948
				BREAK			
				CASE 17			
					model = 		police3
					vCoords = 		<<977.5676, -2997.4746, -40.6470>>
					fHeading = 		269.3488
				BREAK			
				CASE 18			
					model = 		police3
					vCoords = 		<<977.4493, -3001.1082, -40.6470>>
					fHeading = 		270.1491
				BREAK			
				CASE 19			
					model = 		police3
					vCoords = 		<<977.5563, -3004.4756, -40.6470>>
					fHeading = 		270.1491
				BREAK			
				CASE 20			//Carrier Vehicle
					model = 		SANDKING
					vCoords = 		<<1001.3080, -3021.1909, -40.6470>>
					fHeading = 		0.6
				BREAK			
				CASE 21			//Carrier Vehicle
					model = 		SANDKING
					vCoords = 		<<976.1282, -3031.2388, -40.6470>>
					fHeading = 		88.8
				BREAK			
				CASE 22			//Carrier Vehicle
					model = 		SANDKING
					vCoords = 		<<1003.1503, -3012.7983, -40.6470>>
					fHeading = 		94.3999
				BREAK			
				CASE 23			//Carrier Vehicle
					model = 		SANDKING
					vCoords = 		<<966.7803, -3030.8093, -40.6470>>
					fHeading = 		269.1999
				BREAK			
			ENDSWITCH
		BREAK
		CASE CHV_GRUPPE_SECHS_VAN_2
			SWITCH eSubvariation
				CASE CHS_GS2_VARIATION_1
					SWITCH iVehicle				
						CASE 0			
							model = 		stockade
							vCoords = 		<<980.6110, -1231.2760, 24.3900>>
							fHeading = 		304.8
						BREAK			
						CASE 1			
							model = 		stockade
							vCoords = 		<<984.6850, -1237.4399, 24.3330>>
							fHeading = 		305.4
						BREAK			
						CASE 2			
							model = 		stockade
							vCoords = 		<<985.8380, -1241.2030, 24.3430>>
							fHeading = 		124.799
						BREAK			
						CASE 3			
							model = 		stockade
							vCoords = 		<<1011.4040, -1219.1570, 24.1030>>
							fHeading = 		89.399
						BREAK			
						CASE 4			
							model = 		stockade
							vCoords = 		<<1001.4560, -1239.9771, 24.3420>>
							fHeading = 		37.599
						BREAK			
						CASE 5			
							model = 		stockade
							vCoords = 		<<997.8630, -1243.9139, 24.3510>>
							fHeading = 		215.799
						BREAK			
						CASE 6			
							model = 		stockade
							vCoords = 		<<1012.1220, -1222.9020, 24.1120>>
							fHeading = 		268.799
						BREAK			
						CASE 7			
							model = 		kamacho
							vCoords = 		<<990.7690, -1220.5050, 24.3570>>
							fHeading = 		256.999
						BREAK			
						CASE 8			
							model = 		kamacho
							vCoords = 		<<991.4160, -1226.5500, 24.3320>>
							fHeading = 		226.399
						BREAK			
						CASE 9			
							model = 		dilettante2
							vCoords = 		<<973.3850, -1214.9139, 24.5230>>
							fHeading = 		33.398
						BREAK			
						CASE 10			//Carrier Vehicle
							model = 		stockade
							vCoords = 		<<1008.2280, -1235.8680, 24.2840>>
							fHeading = 		218.999
						BREAK			
					ENDSWITCH
				BREAK
				CASE CHS_GS2_VARIATION_2
					SWITCH iVehicle				
						CASE 0			
							model = 		stockade
							vCoords = 		<<-660.1780, -610.7040, 24.3080>>
							fHeading = 		90
						BREAK			
						CASE 1			
							model = 		stockade
							vCoords = 		<<-661.0400, -621.3160, 24.3080>>
							fHeading = 		90
						BREAK			
						CASE 2			
							model = 		stockade
							vCoords = 		<<-660.7030, -626.8810, 24.3080>>
							fHeading = 		268.8
						BREAK			
						CASE 3			
							model = 		stockade
							vCoords = 		<<-692.0260, -623.9580, 24.2990>>
							fHeading = 		268.8
						BREAK			
						CASE 4			
							model = 		stockade
							vCoords = 		<<-692.2990, -612.9320, 24.3030>>
							fHeading = 		88.6
						BREAK			
						CASE 5			
							model = 		stockade
							vCoords = 		<<-672.7050, -615.6650, 24.3080>>
							fHeading = 		266.799
						BREAK			
						CASE 6			
							model = 		dilettante2
							vCoords = 		<<-691.8340, -597.0570, 24.3080>>
							fHeading = 		209.999
						BREAK			
						CASE 7			
							model = 		kamacho
							vCoords = 		<<-677.1310, -595.4990, 24.3080>>
							fHeading = 		241.199
						BREAK			
						CASE 8			
							model = 		kamacho
							vCoords = 		<<-677.0110, -601.2210, 24.3080>>
							fHeading = 		205.399
						BREAK			
						CASE 9			
							model = 		stockade
							vCoords = 		<<-693.1780, -629.7750, 24.2970>>
							fHeading = 		87.799
						BREAK			
						CASE 10			//Carrier Vehicle
							model = 		stockade
							vCoords = 		<<-679.7800, -620.4380, 24.3050>>
							fHeading = 		87.799
						BREAK			
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_ARMORED_EQUIPMENT_1
			SWITCH iVehicle				
				CASE 0			
					model = 		insurgent2
					vCoords = 		<<883.2892, -3240.5623, -99.2768>>
					fHeading = 		359.4
				BREAK			
				CASE 1			
					model = 		insurgent2
					vCoords = 		<<875.1422, -3243.3428, -99.2821>>
					fHeading = 		88.3999
				BREAK			
				CASE 2			
					model = 		brickade
					vCoords = 		<<839.6880, -3237.9731, -99.6991>>
					fHeading = 		66.3998
				BREAK			
			ENDSWITCH				
		BREAK
		CASE CHV_VAULT_KEY_CARDS_2
			SWITCH eSubvariation
				CASE CHS_VKC2_VARIATION_1
					SWITCH iVehicle				
						CASE 0			//Carrier Vehicle
							model = 		PBUS
							vCoords = 		<<2384.6494, 1001.0072, 82.6178>>
							fHeading = 		226.1995
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_VKC2_VARIATION_2
					SWITCH iVehicle				
						CASE 0			//Carrier Vehicle
							model = 		PBUS
							vCoords = 		<<-1893.6838, 763.9855, 139.8719>>
							fHeading = 		131.3998
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_VKC2_VARIATION_3
					SWITCH iVehicle				
						CASE 0			//Carrier Vehicle
							model = 		PBUS
							vCoords = 		<<-101.1954, 2981.4954, 35.9916>>
							fHeading = 		104.7998
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_SECURITY_CAMERAS
			SWITCH eSubvariation
				CASE CHS_SC_LA_MESA
					SWITCH iVehicle				
						CASE 0	
							model = 		Banshee2
							vCoords = 		<<760.1740, -1641.0360, 29.0320>>
							fHeading = 		182
						BREAK			
						CASE 1	
							model = 		SultanRS
							vCoords = 		<<764.5800, -1636.5450, 29.4020>>
							fHeading = 		196.4
						BREAK	
						CASE 2	
							model = 		exemplar
							vCoords = 		<<-174.3757, -180.4767, 42.6197>>
							fHeading = 		344.2879
						BREAK	
						CASE 3			//Carrier Vehicle
							model = 		GAUNTLET4
							vCoords = 		<<-500.8136, -71.6735, 38.4220>>
							fHeading = 		154.9997
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_SC_BURTON
					SWITCH iVehicle				
						CASE 0			
							model = 		Banshee2
							vCoords = 		<<-135.5320, -1632.0930, 31.3090>>
							fHeading = 		160.799
						BREAK			
						CASE 1			
							model = 		SultanRS
							vCoords = 		<<-141.0640, -1636.7780, 31.4450>>
							fHeading = 		149.799
						BREAK	
						CASE 2	
							model = 		huntley
							vCoords = 		<<-190.6074, -175.4916, 42.6253>>
							fHeading = 		158.3435
						BREAK	
						CASE 3			//Carrier Vehicle
							model = 		GAUNTLET4
							vCoords = 		<<-363.0890, -184.1710, 36.2560>>
							fHeading = 		210
						BREAK			
					ENDSWITCH					
				BREAK
				CASE CHS_SC_VESPUCCI
					SWITCH iVehicle				
						CASE 0			
							model = 		Banshee2
							vCoords = 		<<-1293.8030, -1309.5880, 3.5580>>
							fHeading = 		135.8
						BREAK			
						CASE 1			
							model = 		SultanRS
							vCoords = 		<<-1299.1320, -1307.7220, 3.7040>>
							fHeading = 		108.8
						BREAK
						CASE 2	
							model = 		cheburek
							vCoords = 		<<-184.1676, -177.6213, 42.6247>>
							fHeading = 		336.5000
						BREAK
						CASE 3			//Carrier Vehicle
							model = 		GAUNTLET4
							vCoords = 		<<-223.8670, 22.6960, 54.7770>>
							fHeading = 		76.2
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_ARMORED_EQUIPMENT_2
			SWITCH iVehicle				
				CASE 0			
					model = 		buzzard
					vCoords = 		<<3558.3320, 3726.0920, 36.3527>>
					fHeading = 		55.5289
				BREAK			
				CASE 1			
					model = 		buzzard
					vCoords = 		<<3548.1755, 3721.1570, 36.3527>>
					fHeading = 		197.129
				BREAK			
				CASE 2			
					model = 		DINGHY2
					vCoords = 		<<3766.1675, 3812.9319, 0.6301>>
					fHeading = 		169.1998
				BREAK			
				CASE 3			
					model = 		MAVERICK
					vCoords = 		<<2627.0793, 3284.7703, 54.2883>>
					fHeading = 		212.5991
				BREAK			
			ENDSWITCH	
		BREAK			
		CASE CHV_ELECTRIC_DRILLS_1
			SWITCH eSubvariation
				CASE CHS_ED1_VARIATION_1
					SWITCH iVehicle				
						CASE 0			
							model = 		bulldozer
							vCoords = 		<<-183.2349, -1077.6217, 41.1393>>
							fHeading = 		313.8
						BREAK			
						CASE 1			
							model = 		MIXER
							vCoords = 		<<-148.2846, -1069.2335, 41.1392>>
							fHeading = 		253.7997
						BREAK			
						CASE 2			
							model = 		RUBBLE
							vCoords = 		<<-170.3488, -1105.8154, 41.1392>>
							fHeading = 		91.3989
						BREAK			
						CASE 3			
							model = 		dilettante2
							vCoords = 		<<-199.4907, -1097.9209, 20.6881>>
							fHeading = 		338.3991
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_ED1_VARIATION_2
					SWITCH iVehicle				
						CASE 0			
							model = 		bulldozer
							vCoords = 		<<-1125.2588, -964.0168, 1.1502>>
							fHeading = 		325
						BREAK			
						CASE 1			
							model = 		MIXER
							vCoords = 		<<-1111.5386, -949.4341, 1.3888>>
							fHeading = 		31.8
						BREAK			
						CASE 2			
							model = 		RUBBLE
							vCoords = 		<<-1138.3457, -943.4929, 1.5511>>
							fHeading = 		298.5996
						BREAK			
						CASE 3			
							model = 		dilettante2
							vCoords = 		<<-1103.6338, -962.0648, 1.2933>>
							fHeading = 		212.9993
						BREAK						
					ENDSWITCH				
				BREAK
				CASE CHS_ED1_VARIATION_3
					SWITCH iVehicle				
						CASE 0			
							model = 		bulldozer
							vCoords = 		<<1289.8352, -740.9192, 63.4723>>
							fHeading = 		336
						BREAK			
						CASE 1			
							model = 		MIXER
							vCoords = 		<<1318.7725, -742.2347, 64.7658>>
							fHeading = 		47.8
						BREAK			
						CASE 2			
							model = 		RUBBLE
							vCoords = 		<<1309.3973, -725.6512, 63.7990>>
							fHeading = 		247.3999
						BREAK			
						CASE 3			
							model = 		dilettante2
							vCoords = 		<<1357.4645, -741.1185, 66.0523>>
							fHeading = 		245.1968
						BREAK						
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_VAULT_CONTENTS
			SWITCH eSubvariation
				CASE CHS_VC_LEGION_SQUARE
					SWITCH iVehicle				
						CASE 0			
							model = 		kamacho
							vCoords = 		<<244.6385, -860.5272, 28.5069>>
							fHeading = 		249.6387
						BREAK			
					ENDSWITCH
				BREAK
				CASE CHS_VC_VESPUCCI_CANALS
					SWITCH iVehicle				
						CASE 0			
							model = 		kamacho
							vCoords = 		<<-813.4182, -1101.3035, 9.8678>>
							fHeading = 		299.4383
						BREAK			
					ENDSWITCH
				BREAK
				CASE CHS_VC_DEL_PERRO_PIER
					SWITCH iVehicle				
						CASE 0			
							model = 		kamacho
							vCoords = 		<<-1583.0400, -1028.1870, 12.0186>>
							fHeading = 		24.6381
						BREAK			
					ENDSWITCH
				BREAK
				CASE CHS_VC_ROCKFORD_HILLS
					SWITCH iVehicle				
						CASE 0			
							model = 		kamacho
							vCoords = 		<<-817.2997, -126.3718, 36.5257>>
							fHeading = 		245.0379
						BREAK			
					ENDSWITCH
				BREAK
				CASE CHS_VC_OBSERVATORY
					SWITCH iVehicle				
						CASE 0			
							model = 		kamacho
							vCoords = 		<<-397.2712, 1172.5983, 324.6416>>
							fHeading = 		285.4376
						BREAK			
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_FIB_RAID
			SWITCH eSubvariation
				CASE CHS_FIB_VARIATION_1
					SWITCH iVehicle				
						CASE 0			
							model = 		fbi
							vCoords = 		<<-12.5120, -1538.1250, 28.5150>>
							fHeading = 		60.8
						BREAK			
						CASE 1			
							model = 		police3
							vCoords = 		<<-6.7700, -1543.6490, 28.2990>>
							fHeading = 		74.4
						BREAK			
						CASE 2			
							model = 		ambulance
							vCoords = 		<<0.4490, -1540.6200, 28.3730>>
							fHeading = 		215
						BREAK			
					ENDSWITCH				
				BREAK
				
				CASE CHS_FIB_VARIATION_2
					SWITCH iVehicle				
						CASE 0			
							model = 		fbi
							vCoords = 		<<-982.3380, -1474.1639, 4.0150>>
							fHeading = 		149.6
						BREAK			
						CASE 1			
							model = 		police3
							vCoords = 		<<-985.0920, -1469.9919, 4.0020>>
							fHeading = 		179.4
						BREAK			
						CASE 2			
							model = 		ambulance
							vCoords = 		<<-989.8430, -1462.0190, 3.9810>>
							fHeading = 		-0.4
						BREAK			
					ENDSWITCH				
				BREAK
				
				CASE CHS_FIB_VARIATION_3
					SWITCH iVehicle				
						CASE 0			
							model = 		fbi
							vCoords = 		<<67.5720, 45.9830, 72.5150>>
							fHeading = 		236.4
						BREAK			
						CASE 1			
							model = 		police3
							vCoords = 		<<62.2780, 46.4490, 72.5150>>
							fHeading = 		212.6
						BREAK			
						CASE 2			
							model = 		ambulance
							vCoords = 		<<72.1320, 57.6860, 72.5150>>
							fHeading = 		287.199
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_CELEB_DISPOSE_OF_CAR
			SWITCH iVehicle
				CASE 0			
					model = 		ratloader
					vCoords = 		<<-1006.2629, 4952.3662, 196.0062>>
					fHeading = 		325.8
				BREAK			
				
				CASE 1			
					model = 		ratloader
					vCoords = 		<<-1096.2014, 4908.9346, 214.2919>>
					fHeading = 		253.3953
				BREAK			
				
				CASE 2			
					model = 		ratloader
					vCoords = 		<<-1125.3140, 4922.4937, 217.9474>>
					fHeading = 		332.9949
				BREAK			
				
				CASE 3			
					model = 		ratloader
					vCoords = 		<<-1126.1283, 4957.7363, 219.7543>>
					fHeading = 		187.1934
				BREAK			
				
				CASE 4		
					GET_CELEB_DISPOSE_OF_CAR_VEHICLE_DATA(iExtraParam, model, vCoords, fHeading)
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	SWITCH eVariation
		CASE CHV_CELEB_AFTER_PARTY
			SWITCH iVehicle				
				CASE 0			
					model = 		PCJ
					vCoords = 		<<888.5773, -71.4157, 77.7641>>
					fHeading = 		147.6550
				BREAK			
				CASE 1			
					model = 		PCJ
					vCoords = 		<<1033.4764, 190.1525, 79.8559>>
					fHeading = 		46.8
				BREAK			
				CASE 2			
					model = 		Asbo
					vCoords = 		<<889.2448, -64.6349, 77.7641>>
					fHeading = 		239.7843
				BREAK			
				CASE 3			
					model = 		Rumpo
					vCoords = 		<<1041.0256, 202.1381, 79.8559>>
					fHeading = 		148.9357
				BREAK			
				CASE 4			
					model = 		Frogger
					vCoords = 		<<-286.4657, -617.3065, 49.3379>>
					fHeading = 		181.5998
				BREAK			
				CASE 5			
					model = 		police3
					vCoords = 		<<-1123.2094, -862.8995, 12.5735>>
					fHeading = 		40.1268
				BREAK			
			ENDSWITCH									
		BREAK
		CASE CHV_VAULT_DRILL_1
			SWITCH eSubvariation
				CASE CHS_VD1_VARIATION_1
					SWITCH iVehicle					
						CASE 0			
							model = 		FUGITIVE
							vCoords = 		<<766.6306, -3202.6653, 4.9019>>
							fHeading = 		90.1999
						BREAK					
						CASE 1				
							model = 		police3	
							vCoords = 		<<771.2120, -3197.1980, 4.9010>>	
							fHeading = 		138.4	
						BREAK				
						CASE 2				
							model = 		police3	
							vCoords = 		<<771.0630, -3191.1951, 4.9010>>	
							fHeading = 		156.599	
						BREAK				
					ENDSWITCH					
				BREAK
				CASE CHS_VD1_VARIATION_2
					SWITCH iVehicle				
						CASE 0			
							model = 		FUGITIVE
							vCoords = 		<<-1353.6281, -891.2339, 12.8595>>
							fHeading = 		218.3999
						BREAK					
						CASE 1			
							model = 		police3
							vCoords = 		<<-1352.0790, -897.9860, 12.6170>>
							fHeading = 		60.2
						BREAK			
						CASE 2			
							model = 		police3
							vCoords = 		<<-1347.8340, -902.3330, 12.4760>>
							fHeading = 		80.4
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH

	CSH_VEHICLE_DATA data
	data.model = model
	data.vCoords = vCoords
	data.fHeading = fHeading
	
	RETURN data

ENDFUNC

FUNC VECTOR GET_CSH_VEHICLE_SPAWN_COORDS(CSH_VARIATION eVariation, CSH_SUBVARIATION eSubvariation, INT iVehicle, FREEMODE_DELIVERY_DROPOFFS eDropOff = FREEMODE_DELIVERY_DROPOFF_INVALID, INT iExtraParam = -1, INT iRespawnLocation = 0)

	UNUSED_PARAMETER(eDropOff)
	UNUSED_PARAMETER(iRespawnLocation)
	UNUSED_PARAMETER(iExtraParam)

	CSH_VEHICLE_DATA data = GET_CSH_VEHICLE_DATA(iVehicle, eVariation, eSubvariation, iExtraParam)
	RETURN data.vCoords
	
ENDFUNC

FUNC FLOAT GET_CSH_VEHICLE_SPAWN_HEADING(CSH_VARIATION eVariation, CSH_SUBVARIATION eSubvariation, INT iVehicle, FREEMODE_DELIVERY_DROPOFFS eDropOff = FREEMODE_DELIVERY_DROPOFF_INVALID, INT iExtraParam = -1, INT iRespawnLocation = 0)

	UNUSED_PARAMETER(eDropOff)
	UNUSED_PARAMETER(iRespawnLocation)
	UNUSED_PARAMETER(iExtraParam)

	CSH_VEHICLE_DATA data = GET_CSH_VEHICLE_DATA(iVehicle, eVariation, eSubvariation, iExtraParam)
	RETURN data.fHeading

ENDFUNC

FUNC VECTOR GET_CSH_ENTITY_SPAWN_COORDS(CSH_VARIATION eVariation, CSH_SUBVARIATION eSubvariation, INT iMissionEntity, INT iExtraParam = -1, INT iExtraParam1 = -1)

	UNUSED_PARAMETER(iExtraParam)
	UNUSED_PARAMETER(iExtraParam1)
	
	// Carrier vehicles just return the vehicle spawns
	INT iCarrierVehicle = GET_CSH_CARRIER_VEHICLE_INDEX(iMissionEntity, eSubvariation) 
	IF iCarrierVehicle != -1
		RETURN GET_CSH_VEHICLE_SPAWN_COORDS(eVariation, eSubvariation, iCarrierVehicle, DEFAULT, iExtraParam)
	ENDIF
	
	CSH_MISSION_ENTITY_DATA data = GET_CSH_MISSION_ENTITY_DATA(iMissionEntity, eVariation, eSubvariation, iExtraParam, iExtraParam1)
	RETURN data.vCoords
	
ENDFUNC

FUNC FLOAT GET_CSH_ENTITY_SPAWN_HEADING(CSH_VARIATION eVariation, CSH_SUBVARIATION eSubvariation, INT iMissionEntity, INT iExtraParam = -1, INT iExtraParam1 = -1)

	UNUSED_PARAMETER(iExtraParam)
	UNUSED_PARAMETER(iExtraParam1)
	
	// If entity is spawned in carrier vehicle, use coords of vehicle
	INT iCarrierVehicle = GET_CSH_CARRIER_VEHICLE_INDEX(iMissionEntity, eSubvariation) 
	IF iCarrierVehicle != -1
		RETURN GET_CSH_VEHICLE_SPAWN_HEADING(eVariation, eSubvariation, iCarrierVehicle, DEFAULT, iExtraParam)
	ENDIF
	
	CSH_MISSION_ENTITY_DATA data = GET_CSH_MISSION_ENTITY_DATA(iMissionEntity, eVariation, eSubvariation, iExtraParam, iExtraParam1)
	RETURN data.fHeading
	
ENDFUNC

FUNC VECTOR GET_CSH_ENTITY_PICKUP_ROTATION(INT iMissionEntity, CSH_VARIATION eVariation, CSH_SUBVARIATION eSubvariation, INT iExtraParam = -1, INT iExtraParam1 = -1)

	UNUSED_PARAMETER(iExtraParam)
	UNUSED_PARAMETER(iExtraParam1)

	CSH_MISSION_ENTITY_DATA data = GET_CSH_MISSION_ENTITY_DATA(iMissionEntity, eVariation, eSubvariation, iExtraParam, iExtraParam1)
	RETURN data.vRotation
	
ENDFUNC

////////////////////////////////////////
/// 		 VARIATION VEHICLE MODEL ///
////////////////////////////////////////

FUNC MODEL_NAMES GET_CSH_AMBUSH_VEHICLE_MODEL(CSH_VARIATION eVariation, CSH_SUBVARIATION eSubvariation, INT iAmbushArrayIndex, INT iExtraParam = -1)

	UNUSED_PARAMETER(iExtraParam)
	UNUSED_PARAMETER(eSubvariation)
	UNUSED_PARAMETER(iAmbushArrayIndex)
	
	SWITCH eVariation
		CASE CHV_ARCADE_CABINETS
			SWITCH eSubvariation
				CASE CHS_AC_ELYSIAN_ISLAND				RETURN FACTION
				CASE CHS_AC_DOWNTOWN_VINEWOOD			RETURN FUGITIVE
				CASE CHS_AC_PALETO_BAY					RETURN GBURRITO
				CASE CHS_AC_LA_MESA						RETURN FACTION
				CASE CHS_AC_CHUMASH						RETURN KURUMA
			ENDSWITCH
		BREAK
		CASE CHV_ZANCUDO_SHIPMENT						RETURN CRUSADER
		CASE CHV_DRAG_RACE								RETURN CARBONRS
		CASE CHV_WEAPONS_SMUGGLERS						RETURN FROGGER
		CASE CHV_LOST_MC_BIKES							RETURN GBURRITO
		CASE CHV_WEAPONS_STASH
			SWITCH eSubvariation
				CASE CHS_WST_SAN_ANDREAS		RETURN SLAMVAN
				CASE CHS_WST_PALETO_BAY			RETURN BUCCANEER2
			ENDSWITCH
		BREAK
		CASE CHV_CARGO_SHIP
			SWITCH eSubvariation
				CASE CHS_CS_VARIATION_1			RETURN DUBSTA 
				CASE CHS_CS_VARIATION_2			RETURN GBURRITO 
				CASE CHS_CS_VARIATION_3			RETURN SCHAFTER2
			ENDSWITCH
		BREAK
		CASE CHV_MERRYWEATHER_TEST_SITE			RETURN MESA3
		CASE CHV_STEALTH_OUTFITS				RETURN MESA3
		CASE CHV_PLASTIC_EXPLOSIVES				RETURN BUZZARD
		CASE CHV_ARMORED_EQUIPMENT_1			RETURN MESA3
		CASE CHV_EXPLOSIVES_2					RETURN SCHAFTER5
		CASE CHV_ARMORED_EQUIPMENT_2
			SWITCH iAmbushArrayIndex
				CASE 0	RETURN BUZZARD2
				CASE 1	RETURN MESA3
			ENDSWITCH
		BREAK
		CASE CHV_EXPLOSIVES_1					RETURN BODHI2
		CASE CHV_SECURITY_CAMERAS
			SWITCH iAmbushArrayIndex
				CASE 0	RETURN Banshee2
				CASE 1	RETURN SultanRS
			ENDSWITCH
		BREAK
		CASE CHV_VAULT_DRILL_2					RETURN FUGITIVE
		CASE CHV_VAULT_LASER_2					RETURN dubsta3
		CASE CHV_VAULT_DRILL_1					RETURN FUGITIVE
		CASE CHV_FLIGHT_SCHEDULE				RETURN BUZZARD
		CASE CHV_MERRYWEATHER_CONVOY			RETURN BUZZARD
	ENDSWITCH
	
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

CONST_INT MAX_NUM_CSH_COMMON_CARRIER_VEHICLE_MODELS		11
FUNC MODEL_NAMES GET_CSH_COMMON_CARRIER_VEHICLE_MODEL(INT iModel)

	SWITCH iModel
		CASE 0		RETURN BARRAGE
		CASE 1		RETURN HALFTRACK
		CASE 2		RETURN INSURGENT3
		CASE 3		RETURN LIMO2
		CASE 4		RETURN TAMPA3
		CASE 5		RETURN TECHNICAL3
		CASE 6		RETURN RUINER2
		CASE 7		RETURN BOXVILLE5
		CASE 8		RETURN DUNE3
		CASE 9		RETURN DUNE5
		CASE 10		RETURN CARACARA
	ENDSWITCH

	RETURN RUINER2

ENDFUNC

FUNC MODEL_NAMES GET_CSH_VEHICLE_MODEL(CSH_VARIATION eVariation, CSH_SUBVARIATION eSubvariation1, INT iVehicle, INT iExtraParam, PLAYER_INDEX playerID)

	UNUSED_PARAMETER(iExtraParam)
	
	CSH_VEHICLE_DATA data = GET_CSH_VEHICLE_DATA(iVehicle, eVariation, eSubvariation1, iExtraParam)
	MODEL_NAMES modelTemp
	
	IF GET_CSH_ITEM_TYPE_FROM_VARIATION(eVariation) = CIT_VEHICLES
		IF data.model = SANDKING
			CSH_MISSION_DATA missionData = GB_GET_REMOTE_PLAYER_CSH_MISSION_DATA(playerID)
			
			modelTemp = GET_VEHICLE_MODEL_FROM_PLANNING_BOARD(eVariation, INT_TO_ENUM(CASINO_HEIST_DRIVERS, missionData.iCrewMember), INT_TO_ENUM(CASINO_HEIST_VEHICLE_SELECTION, missionData.iChosenItem))

			IF modelTemp != DUMMY_MODEL_FOR_SCRIPT
				PRINTLN("GET_CSH_VEHICLE_MODEL, iCrewMember = ", missionData.iCrewMember, " model = ", GET_MODEL_NAME_FOR_DEBUG(modelTemp), " playerID = ", GET_PLAYER_NAME(playerID), " iChosenItem = ", missionData.iChosenItem)
				data.model = modelTemp
			ENDIF
		ENDIF
	ENDIF
	
	RETURN data.model
	
ENDFUNC

/////////////////////////////////
/// MISSION PED SPAWN COORDS ///
/////////////////////////////////

CONST_INT ciMAID_INSIDE 	0
CONST_INT ciMAID_OUTSIDE	1

STRUCT CSH_PED_DATA

	MODEL_NAMES model
	VECTOR vCoords
	FLOAT fHeading
	WEAPON_TYPE weapon
	STRING sScenario

ENDSTRUCT

FUNC CSH_PED_DATA GET_VAULT_CONTENTS_PED_DATA(INT iPed, CSH_SUBVARIATION eSubvariation, INT iExtraParam = -1)

	MODEL_NAMES model
	VECTOR vCoords
	FLOAT fHeading
	WEAPON_TYPE weapon
	STRING sScenario

	SWITCH iPed
		CASE 0
			SWITCH eSubvariation
				CASE CHS_VC_LEGION_SQUARE
					SWITCH iExtraParam				
						CASE 0			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_02"))
							vCoords = 		<<181.1719, -943.9254, 29.0919>>
							fHeading = 		322.3969
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 1			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_02"))
							vCoords = 		<<180.0119, -875.6740, 30.1166>>
							fHeading = 		262.9963
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 2			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_02"))
							vCoords = 		<<236.6564, -896.9291, 29.6920>>
							fHeading = 		136.3978
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 3			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_02"))
							vCoords = 		<<179.4794, -917.4413, 29.6918>>
							fHeading = 		315.3944
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 4			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_02"))
							vCoords = 		<<221.2252, -946.4835, 29.0919>>
							fHeading = 		343.5976
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
					ENDSWITCH
				BREAK
				CASE CHS_VC_VESPUCCI_CANALS
					SWITCH iExtraParam				
						CASE 0			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_02"))
							vCoords = 		<<-816.8832, -1107.7781, 10.1742>>
							fHeading = 		169.7975
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 1			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_02"))
							vCoords = 		<<-824.7117, -1059.9265, 10.8547>>
							fHeading = 		124.7943
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 2			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_02"))
							vCoords = 		<<-839.9531, -1130.0270, 6.0287>>
							fHeading = 		98.3956
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 3			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_02"))
							vCoords = 		<<-843.9219, -1098.0995, 8.1164>>
							fHeading = 		299.4457
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 4			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_02"))
							vCoords = 		<<-824.1573, -1083.4838, 10.1374>>
							fHeading = 		114.4442
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
					ENDSWITCH
				BREAK
				CASE CHS_VC_DEL_PERRO_PIER
					SWITCH iExtraParam				
						CASE 0			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_02"))
							vCoords = 		<<-1656.7551, -1110.5813, 12.0417>>
							fHeading = 		305.8456
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 1			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_02"))
							vCoords = 		<<-1674.5972, -1144.2139, 12.0174>>
							fHeading = 		197.4455
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 2			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_02"))
							vCoords = 		<<-1705.1829, -1147.2384, 12.0316>>
							fHeading = 		235.6447
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 3			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_02"))
							vCoords = 		<<-1696.5848, -1092.8013, 12.1526>>
							fHeading = 		181.8436
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 4			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_02"))
							vCoords = 		<<-1686.6598, -1122.8789, 12.1522>>
							fHeading = 		246.8429
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
					ENDSWITCH
				BREAK
				CASE CHS_VC_ROCKFORD_HILLS
					SWITCH iExtraParam				
						CASE 0			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_02"))
							vCoords = 		<<-838.0202, -125.3753, 36.5439>>
							fHeading = 		60.8407
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 1			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_02"))
							vCoords = 		<<-846.4454, -64.8119, 36.8384>>
							fHeading = 		134.2398
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 2			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_02"))
							vCoords = 		<<-804.5388, -146.0753, 36.9139>>
							fHeading = 		242.8396
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 3			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_02"))
							vCoords = 		<<-769.3478, -82.6857, 36.7029>>
							fHeading = 		342.639
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 4			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_02"))
							vCoords = 		<<-814.4337, -103.3956, 36.5915>>
							fHeading = 		251.0383
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
					ENDSWITCH
				BREAK
				CASE CHS_VC_OBSERVATORY
					SWITCH iExtraParam				
						CASE 0			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_02"))
							vCoords = 		<<-409.6533, 1073.1541, 326.6821>>
							fHeading = 		175.039
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 1			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_02"))
							vCoords = 		<<-423.1417, 1113.9352, 325.7680>>
							fHeading = 		180.2386
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 2			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_02"))
							vCoords = 		<<-405.0932, 1055.7506, 322.8424>>
							fHeading = 		165.0388
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 3			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_02"))
							vCoords = 		<<-430.4341, 1061.5698, 326.6907>>
							fHeading = 		112.4379
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 4			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_02"))
							vCoords = 		<<-385.5880, 1082.1653, 323.6203>>
							fHeading = 		164.237
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE 1
			SWITCH eSubvariation
				CASE CHS_VC_LEGION_SQUARE
					SWITCH iExtraParam				
						CASE 0			
							model = 		A_M_M_Farmer_01
							vCoords = 		<<181.7017, -942.7737, 29.0919>>
							fHeading = 		173.3973
							weapon = 		WEAPONTYPE_SAWNOFFSHOTGUN
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 1			
							model = 		A_M_M_Farmer_01
							vCoords = 		<<181.1374, -875.9651, 30.1167>>
							fHeading = 		78.1965
							weapon = 		WEAPONTYPE_SAWNOFFSHOTGUN
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 2			
							model = 		A_M_M_Farmer_01
							vCoords = 		<<236.0358, -897.9385, 29.6920>>
							fHeading = 		344.5967
							weapon = 		WEAPONTYPE_SAWNOFFSHOTGUN
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 3			
							model = 		A_M_M_Farmer_01
							vCoords = 		<<180.2584, -916.6758, 29.6918>>
							fHeading = 		139.7966
							weapon = 		WEAPONTYPE_SAWNOFFSHOTGUN
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 4			
							model = 		A_M_M_Farmer_01
							vCoords = 		<<221.5317, -945.3689, 29.0919>>
							fHeading = 		162.7965
							weapon = 		WEAPONTYPE_SAWNOFFSHOTGUN
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
					ENDSWITCH
				BREAK
				CASE CHS_VC_VESPUCCI_CANALS
					SWITCH iExtraParam				
						CASE 0			
							model = 		A_M_M_Farmer_01
							vCoords = 		<<-817.1810, -1108.9716, 10.1768>>
							fHeading = 		318.1971
							weapon = 		WEAPONTYPE_SAWNOFFSHOTGUN
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 1		
							model = 		A_M_M_Farmer_01
							vCoords = 		<<-825.8268, -1060.4736, 10.7469>>
							fHeading = 		292.7971
							weapon = 		WEAPONTYPE_SAWNOFFSHOTGUN
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 2	
							model = 		A_M_M_Farmer_01
							vCoords = 		<<-841.2771, -1130.4155, 6.0955>>
							fHeading = 		268.7969
							weapon = 		WEAPONTYPE_SAWNOFFSHOTGUN
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 3			
							model = 		A_M_M_Farmer_01
							vCoords = 		<<-843.0751, -1097.3662, 8.0877>>
							fHeading = 		146.7972
							weapon = 		WEAPONTYPE_SAWNOFFSHOTGUN
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 4			
							model = 		A_M_M_Farmer_01
							vCoords = 		<<-825.3754, -1083.9438, 10.1374>>
							fHeading = 		267.7971
							weapon = 		WEAPONTYPE_SAWNOFFSHOTGUN
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
					ENDSWITCH
				BREAK
				CASE CHS_VC_DEL_PERRO_PIER
					SWITCH iExtraParam				
						CASE 0		
							model = 		A_M_M_Farmer_01
							vCoords = 		<<-1655.7570, -1109.8446, 12.0386>>
							fHeading = 		130.9964
							weapon = 		WEAPONTYPE_SAWNOFFSHOTGUN
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK
						CASE 1				
							model = 		A_M_M_Farmer_01
							vCoords = 		<<-1674.3641, -1145.2273, 12.0173>>
							fHeading = 		8.1966
							weapon = 		WEAPONTYPE_SAWNOFFSHOTGUN
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK
						CASE 2					
							model = 		A_M_M_Farmer_01
							vCoords = 		<<-1704.2120, -1148.1951, 12.0300>>
							fHeading = 		30.7965
							weapon = 		WEAPONTYPE_SAWNOFFSHOTGUN
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK
						CASE 3					
							model = 		A_M_M_Farmer_01
							vCoords = 		<<-1696.4054, -1093.9073, 12.1526>>
							fHeading = 		11.1964
							weapon = 		WEAPONTYPE_SAWNOFFSHOTGUN
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK
						CASE 4					
							model = 		A_M_M_Farmer_01
							vCoords = 		<<-1685.3025, -1123.4005, 12.1521>>
							fHeading = 		75.7966
							weapon = 		WEAPONTYPE_SAWNOFFSHOTGUN
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK
					ENDSWITCH
				BREAK
				CASE CHS_VC_ROCKFORD_HILLS
					SWITCH iExtraParam				
						CASE 0			
							model = 		A_M_M_Farmer_01
							vCoords = 		<<-839.2482, -124.5717, 36.5654>>
							fHeading = 		243.442
							weapon = 		WEAPONTYPE_SAWNOFFSHOTGUN
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 1			
							model = 		A_M_M_Farmer_01
							vCoords = 		<<-847.4260, -65.6505, 36.8384>>
							fHeading = 		308.2423
							weapon = 		WEAPONTYPE_SAWNOFFSHOTGUN
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 2			
							model = 		A_M_M_Farmer_01
							vCoords = 		<<-803.5362, -146.5170, 36.8960>>
							fHeading = 		74.6424
							weapon = 		WEAPONTYPE_SAWNOFFSHOTGUN
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 3			
							model = 		A_M_M_Farmer_01
							vCoords = 		<<-768.8218, -81.8011, 36.7803>>
							fHeading = 		144.842
							weapon = 		WEAPONTYPE_SAWNOFFSHOTGUN
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 4			
							model = 		A_M_M_Farmer_01
							vCoords = 		<<-813.2503, -103.7418, 36.5992>>
							fHeading = 		86.8423
							weapon = 		WEAPONTYPE_SAWNOFFSHOTGUN
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
					ENDSWITCH
				BREAK
				CASE CHS_VC_OBSERVATORY
					SWITCH iExtraParam				
						CASE 0			
							model = 		A_M_M_Farmer_01
							vCoords = 		<<-409.5945, 1071.8882, 326.6821>>
							fHeading = 		347.6418
							weapon = 		WEAPONTYPE_SAWNOFFSHOTGUN
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 1			
							model = 		A_M_M_Farmer_01
							vCoords = 		<<-423.0339, 1112.7365, 325.7680>>
							fHeading = 		3.2416
							weapon = 		WEAPONTYPE_SAWNOFFSHOTGUN
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 2			
							model = 		A_M_M_Farmer_01
							vCoords = 		<<-405.1944, 1054.8146, 322.8425>>
							fHeading = 		0.2417
							weapon = 		WEAPONTYPE_SAWNOFFSHOTGUN
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 3			
							model = 		A_M_M_Farmer_01
							vCoords = 		<<-431.3750, 1060.8060, 326.6899>>
							fHeading = 		310.0415
							weapon = 		WEAPONTYPE_SAWNOFFSHOTGUN
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 4			
							model = 		A_M_M_Farmer_01
							vCoords = 		<<-385.8932, 1081.1248, 323.5682>>
							fHeading = 		338.4417
							weapon = 		WEAPONTYPE_SAWNOFFSHOTGUN
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	CSH_PED_DATA data
	data.model = model
	data.vCoords = vCoords
	data.fHeading = fHeading
	data.weapon = weapon
	data.sScenario = sScenario
	
	RETURN data
	
ENDFUNC

FUNC CSH_PED_DATA GET_CSH_PED_DATA(INT iPed, CSH_VARIATION eVariation, CSH_SUBVARIATION eSubvariation, INT iExtraParam = -1)

	MODEL_NAMES model = DUMMY_MODEL_FOR_SCRIPT
	VECTOR vCoords = <<0.0, 0.0, 0.0>>
	FLOAT fHeading = 0.0
	WEAPON_TYPE weapon = WEAPONTYPE_UNARMED
	STRING sScenario = ""

	SWITCH eVariation
		CASE CHV_ARCADE_CABINETS	
			SWITCH eSubvariation
				CASE CHS_AC_ELYSIAN_ISLAND
					SWITCH iPed				
						CASE 0			
							model = 		S_M_M_UPS_02
							vCoords = 		<<-482.1900, -2826.5649, 5.0000>>
							fHeading = 		91.599
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 1			
							model = 		S_M_M_UPS_02
							vCoords = 		<<-485.6550, -2820.0940, 5.0000>>
							fHeading = 		351.599
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 2			
							model = 		S_M_M_UPS_02
							vCoords = 		<<-491.0450, -2825.5720, 5.0000>>
							fHeading = 		40.799
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 3			
							model = 		S_M_M_UPS_02
							vCoords = 		<<-474.4390, -2830.2451, 6.3010>>
							fHeading = 		133.798
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 4			
							model = 		G_M_Y_BallaOrig_01
							vCoords = 		<<455.3290, -2097.3799, 24.2580>>
							fHeading = 		319.398
							weapon = 		WEAPONTYPE_SAWNOFFSHOTGUN
							sScenario = 		""
						BREAK			
						CASE 5			
							model = 		G_M_Y_BallaOrig_01
							vCoords = 		<<454.9040, -2097.0210, 24.2730>>
							fHeading = 		319.398
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK						
					ENDSWITCH				
				BREAK
				CASE CHS_AC_DOWNTOWN_VINEWOOD
					SWITCH iPed				
						CASE 0			
							model = 		S_M_M_Postal_02
							vCoords = 		<<67.0030, 122.5370, 78.1350>>
							fHeading = 		210.8
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 1			
							model = 		S_M_M_Postal_02
							vCoords = 		<<74.9880, 121.2930, 78.1980>>
							fHeading = 		106.799
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 2			
							model = 		S_M_M_Postal_02
							vCoords = 		<<58.9850, 123.6880, 78.2700>>
							fHeading = 		-5.401
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 3			
							model = 		S_M_M_Postal_02
							vCoords = 		<<70.1480, 117.1670, 78.1310>>
							fHeading = 		360.199
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 4			
							model = 		G_M_M_ChiGoon_02
							vCoords = 		<<302.9860, 714.1820, 176.9800>>
							fHeading = 		282.799
							weapon = 		WEAPONTYPE_SAWNOFFSHOTGUN
							sScenario = 		""
						BREAK			
						CASE 5			
							model = 		G_M_M_ChiGoon_02
							vCoords = 		<<303.3100, 714.3240, 177.4240>>
							fHeading = 		282.799
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK						
					ENDSWITCH				
				BREAK
				CASE CHS_AC_PALETO_BAY
					SWITCH iPed				
						CASE 0			
							model = 		S_M_M_UPS_02
							vCoords = 		<<-435.7350, 6135.8311, 30.4780>>
							fHeading = 		305.199
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 1			
							model = 		S_M_M_UPS_02
							vCoords = 		<<-438.0420, 6145.0142, 30.4780>>
							fHeading = 		104.399
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 2			
							model = 		S_M_M_UPS_02
							vCoords = 		<<-428.8890, 6128.9028, 30.4780>>
							fHeading = 		230.398
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 3			
							model = 		S_M_M_UPS_02
							vCoords = 		<<-421.5105, 6136.0049, 30.7770>>
							fHeading = 		137.398
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 4			
							model = 		G_M_Y_Lost_02
							vCoords = 		<<-1729.3770, 4790.7261, 59.2720>>
							fHeading = 		58.798
							weapon = 		WEAPONTYPE_SAWNOFFSHOTGUN
							sScenario = 		""
						BREAK			
						CASE 5			
							model = 		G_M_Y_Lost_02
							vCoords = 		<<-1729.3770, 4790.7261, 59.2720>>
							fHeading = 		58.798
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK						
					ENDSWITCH				
				BREAK
				CASE CHS_AC_LA_MESA
					SWITCH iPed				
						CASE 0			
							model = 		S_M_M_Postal_02
							vCoords = 		<<910.8070, -1266.8660, 24.5870>>
							fHeading = 		-95.601
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 1			
							model = 		S_M_M_Postal_02
							vCoords = 		<<908.6790, -1256.7970, 24.5860>>
							fHeading = 		131.199
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 2			
							model = 		S_M_M_Postal_02
							vCoords = 		<<913.1090, -1253.8650, 24.5530>>
							fHeading = 		206.799
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 3			
							model = 		S_M_M_Postal_02
							vCoords = 		<<915.1690, -1261.6990, 24.5650>>
							fHeading = 		107.398
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 4			
							model = 		G_M_Y_BallaOrig_01
							vCoords = 		<<1167.7300, -898.2100, 52.7820>>
							fHeading = 		355.798
							weapon = 		WEAPONTYPE_SAWNOFFSHOTGUN
							sScenario = 		""
						BREAK			
						CASE 5			
							model = 		G_M_Y_BallaOrig_01
							vCoords = 		<<1168.1010, -897.8920, 53.0470>>
							fHeading = 		355.798
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK				
					ENDSWITCH				
				BREAK
				CASE CHS_AC_CHUMASH
					SWITCH iPed				
						CASE 0			
							model = 		S_M_M_Postal_02
							vCoords = 		<<-3155.1699, 1135.1930, 19.8710>>
							fHeading = 		130.999
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 1			
							model = 		S_M_M_Postal_02
							vCoords = 		<<-3161.8250, 1128.0980, 19.9280>>
							fHeading = 		37.999
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 2			
							model = 		S_M_M_Postal_02
							vCoords = 		<<-3163.8999, 1134.6840, 19.1>>
							fHeading = 		110.999
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 3			
							model = 		S_M_M_Postal_02
							vCoords = 		<<-3164.3889, 1139.0959, 20.1490>>
							fHeading = 		254.798
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 4			
							model = 		G_M_Y_Korean_01
							vCoords = 		<<-2679.6699, -63.4800, 18.5840>>
							fHeading = 		218.8
							weapon = 		WEAPONTYPE_SAWNOFFSHOTGUN
							sScenario = 		""
						BREAK			
						CASE 5			
							model = 		G_M_Y_Korean_01
							vCoords = 		<<-2679.6699, -63.4800, 18.5840>>
							fHeading = 		218.8
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK					
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_INSIDE_MAID
			IF iExtraParam = ciMAID_INSIDE
				SWITCH eSubvariation
					CASE CHS_IMM_VARIATION_1
						SWITCH iPed				
							CASE 0			
								model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_f_y_casino_01"))
								vCoords = 		<<261.0670, -996.9260, -99.5660>>
								fHeading = 		96.2
								weapon = 		WEAPONTYPE_UNARMED
	//							sScenario = 		timetable@tracy@sleep@idle_a
							BREAK			
						ENDSWITCH				
					BREAK
					CASE CHS_IMM_VARIATION_2
						SWITCH iPed				
							CASE 0			
								model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_f_y_casino_01"))
								vCoords = 		<<261.0670, -996.9260, -99.5660>>
								fHeading = 		96.2
								weapon = 		WEAPONTYPE_UNARMED
	//							sScenario = 		timetable@tracy@sleep@idle_a
							BREAK			
						ENDSWITCH				
					BREAK
				ENDSWITCH
			ELSE
				SWITCH iPed				
					CASE 0			
						model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_f_y_casino_01"))
						vCoords = 		<<266.06, -1007.43, -102.01>>
						fHeading = 		8.6000
						weapon = 		WEAPONTYPE_UNARMED
						sScenario = 	"WORLD_HUMAN_WINDOW_SHOP_BROWSE"
					BREAK			
				ENDSWITCH				
			ENDIF
		BREAK		
		CASE CHV_WEAPONS_SMUGGLERS
			SWITCH eSubvariation
				CASE CHS_WSM_VARIATION_1
					SWITCH iPed				
						CASE 0			
							model = 		G_M_M_ArmGoon_01
							vCoords = 		<<2846.7427, -69.8540, 1.9755>>
							fHeading = 		155.7079
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK			
						CASE 1			
							model = 		G_M_M_ArmGoon_01
							vCoords = 		<<2850.9514, -78.9629, 1.4340>>
							fHeading = 		343.5076
							weapon = 		WEAPONTYPE_SMG
							sScenario = 		"WORLD_HUMAN_WINDOW_SHOP_BROWSE"
						BREAK			
						CASE 2			
							model = 		G_M_M_ArmGoon_01
							vCoords = 		<<2855.7202, -72.7075, 1.7405>>
							fHeading = 		275.1071
							weapon = 		WEAPONTYPE_SMG
							sScenario = 		"CODE_HUMAN_MEDIC_TEND_TO_DEAD"
						BREAK			
						CASE 3			
							model = 		G_M_M_ArmGoon_01
							vCoords = 		<<2854.1460, -91.0723, 1.5118>>
							fHeading = 		275.1071
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 4			
							model = 		G_M_M_ArmGoon_01
							vCoords = 		<<2852.9341, -104.2741, 0.7905>>
							fHeading = 		275.1071
							weapon = 		WEAPONTYPE_SMG
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 5			
							model = 		G_M_M_ArmGoon_01
							vCoords = 		<<2854.3025, -103.9993, 0.7196>>
							fHeading = 		95.7067
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 6			
							model = 		G_M_M_ArmGoon_01
							vCoords = 		<<2841.2195, -99.1580, 1.1699>>
							fHeading = 		149.7065
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 7			
							model = 		G_M_M_ArmGoon_01
							vCoords = 		<<2848.6528, -87.4811, 1.2238>>
							fHeading = 		145.9062
							weapon = 		WEAPONTYPE_SMG
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 8			
							model = 		G_M_M_ArmGoon_01
							vCoords = 		<<2848.0259, -88.4428, 1.4317>>
							fHeading = 		319.9057
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_SMOKING_POT"
						BREAK			
						CASE 9			
							model = 		G_M_M_ArmGoon_01
							vCoords = 		<<2855.6653, -77.2992, 1.4726>>
							fHeading = 		349.9053
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 10			
							model = 		G_M_M_ArmGoon_01
							vCoords = 		<<2845.4360, -102.6277, 1.1455>>
							fHeading = 		40.5052
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_GUARD_STAND"
						BREAK			
						CASE 11			
							model = 		G_M_M_ArmGoon_01
							vCoords = 		<<3053.0754, -3.9621, 1.5569>>
							fHeading = 		40.5052
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 12			
							model = 		G_M_M_ArmGoon_01
							vCoords = 		<<3053.3733, -3.8974, 1.3581>>
							fHeading = 		40.5052
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK
						CASE 13				
							model = 		GET_HIRED_CREW_PED_MODEL(CASINO_HEIST_DRIVER__NONE, CASINO_HEIST_HACKER__NONE, GET_PLAYER_CASINO_HEIST_WEAPONS_EXPERT(GB_GET_LOCAL_PLAYER_GANG_BOSS()))
							vCoords = 		<<1728.0190, 3285.3250, 40.0799>>
							fHeading = 		212.4	
							weapon = 		WEAPONTYPE_PISTOL	
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"	
						BREAK				
					ENDSWITCH				
				BREAK
				CASE CHS_WSM_VARIATION_2
					SWITCH iPed				
						CASE 0			
							model = 		G_M_M_ArmGoon_01
							vCoords = 		<<2533.0300, -1222.3850, 1.2140>>
							fHeading = 		-24.202
							weapon = 		WEAPONTYPE_SMG
							sScenario = 		"WORLD_HUMAN_WINDOW_SHOP_BROWSE"
						BREAK			
						CASE 1			
							model = 		G_M_M_ArmGoon_01
							vCoords = 		<<2541.6721, -1226.6021, 0.7800>>
							fHeading = 		192.398
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_MEDIC_TEND_TO_DEAD"
						BREAK			
						CASE 2			
							model = 		G_M_M_ArmGoon_01
							vCoords = 		<<2532.5239, -1229.5360, 0.9230>>
							fHeading = 		263.198
							weapon = 		WEAPONTYPE_SMG
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 3			
							model = 		G_M_M_ArmGoon_01
							vCoords = 		<<2533.6890, -1229.6050, 0.8690>>
							fHeading = 		85.397
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK			
						CASE 4			
							model = 		G_M_M_ArmGoon_01
							vCoords = 		<<2547.3950, -1220.6560, 0.8870>>
							fHeading = 		190.197
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_SMOKING_POT"
						BREAK			
						CASE 5			
							model = 		G_M_M_ArmGoon_01
							vCoords = 		<<2537.1370, -1217.2590, 1.3750>>
							fHeading = 		340.397
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 6			
							model = 		G_M_M_ArmGoon_01
							vCoords = 		<<2542.1851, -1220.5520, 0.9610>>
							fHeading = 		138.396
							weapon = 		WEAPONTYPE_SMG
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 7			
							model = 		G_M_M_ArmGoon_01
							vCoords = 		<<2541.4180, -1221.4880, 0.8760>>
							fHeading = 		318.796
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 8			
							model = 		G_M_M_ArmGoon_01
							vCoords = 		<<2538.8550, -1232.3000, 0.4650>>
							fHeading = 		181.396
							weapon = 		WEAPONTYPE_SMG
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 9			
							model = 		G_M_M_ArmGoon_01
							vCoords = 		<<2535.5281, -1240.9250, 0.2000>>
							fHeading = 		217.796
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		"WORLD_HUMAN_WINDOW_SHOP_BROWSE"
						BREAK			
						CASE 10			
							model = 		G_M_M_ArmGoon_01
							vCoords = 		<<2548.4509, -1238.3621, 2.0990>>
							fHeading = 		217.796
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK		
						CASE 11			
							model = 		G_M_M_ArmGoon_01
							vCoords = 		<<2605.9548, -1327.6934, 0.9948>>
							fHeading = 		181.396
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK			
						CASE 12			
							model = 		G_M_M_ArmGoon_01
							vCoords = 		<<2606.0168, -1327.7946, 0.8823>>
							fHeading = 		181.396
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK			
						CASE 13			
							model = 		GET_HIRED_CREW_PED_MODEL(CASINO_HEIST_DRIVER__NONE, CASINO_HEIST_HACKER__NONE, GET_PLAYER_CASINO_HEIST_WEAPONS_EXPERT(GB_GET_LOCAL_PLAYER_GANG_BOSS()))
							vCoords = 		<<1747.4822, 3292.4172, 40.1049>>
							fHeading = 		163.2
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_DRAG_RACE
			SWITCH eSubvariation
				CASE CHS_DR_VARIATION_1
					SWITCH iPed				
						CASE 0			
							model = 		G_M_M_ChiGoon_02
							vCoords = 		<<1402.9340, 3003.8989, 41.4190>>
							fHeading = 		128.599
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 1			
							model = 		G_M_M_ChiGoon_02
							vCoords = 		<<1406.9871, 2999.5769, 41.8530>>
							fHeading = 		128.599
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 2			
							model = 		S_M_Y_XMech_02
							vCoords = 		<<1405.4561, 2991.6030, 39.5450>>
							fHeading = 		75.999
							weapon = 		WEAPONTYPE_DLC_WRENCH
							sScenario = 		"random@street_race"
						BREAK			
						CASE 3			
							model = 		S_M_Y_XMech_02
							vCoords = 		<<1391.8320, 3003.4561, 39.5540>>
							fHeading = 		175.198
							weapon = 		WEAPONTYPE_DLC_WRENCH
							sScenario = 		"WORLD_HUMAN_WINDOW_SHOP_BROWSE"
						BREAK			
						CASE 4			
							model = 		S_M_Y_XMech_02
							vCoords = 		<<1401.3311, 3010.4609, 39.5440>>
							fHeading = 		259.997
							weapon = 		WEAPONTYPE_DLC_WRENCH
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 5			
							model = 		A_F_Y_EastSA_03
							vCoords = 		<<1404.4050, 2991.8879, 39.5470>>
							fHeading = 		274.997
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"random@street_race"
						BREAK			
						CASE 6			
							model = 		A_F_Y_EastSA_03
							vCoords = 		<<1398.8350, 3008.9951, 39.5480>>
							fHeading = 		262.797
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_CHEERING"
						BREAK			
						CASE 7			
							model = 		A_F_Y_EastSA_03
							vCoords = 		<<1400.0450, 3009.7351, 39.5460>>
							fHeading = 		274.796
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_CHEERING"
						BREAK			
						CASE 8			
							model = 		A_F_Y_EastSA_03
							vCoords = 		<<1414.1550, 3000.4771, 39.5390>>
							fHeading = 		0.596
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_CHEERING"
						BREAK			
						CASE 9			
							model = 		A_F_Y_EastSA_03
							vCoords = 		<<1413.0110, 2998.9609, 39.5400>>
							fHeading = 		356.396
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_CHEERING"
						BREAK			
						CASE 10			
							model = 		G_M_M_ChiGoon_02
							vCoords = 		<<1398.0856, 3003.9890, 39.5517>>
							fHeading = 		312.5996
							weapon = 		WEAPONTYPE_SMG
							sScenario = 		"random@street_race"
						BREAK			
						CASE 11			
							model = 		G_M_M_ChiGoon_02
							vCoords = 		<<1399.0149, 3004.8401, 39.5501>>
							fHeading = 		130.799
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 12			
							model = 		G_M_M_ChiGoon_02
							vCoords = 		<<1408.8391, 2995.1384, 39.5514>>
							fHeading = 		249.1987
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_MEDIC_TEND_TO_DEAD"
						BREAK			
						CASE 13			
							model = 		A_F_Y_EastSA_03
							vCoords = 		<<1411.3790, 3008.6123, 39.5333>>
							fHeading = 		140.3998
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"random@street_race"
						BREAK			
						CASE 14			
							model = 		G_M_M_ChiGoon_02
							vCoords = 		<<1400.6469, 2990.2061, 39.5500>>
							fHeading = 		253.399
							weapon = 		WEAPONTYPE_SMG
							sScenario = 		"random@street_race"
						BREAK			
						CASE 15			
							model = 		G_M_M_ChiGoon_02
							vCoords = 		<<1393.9258, 3000.2595, 39.5528>>
							fHeading = 		79.9986
							weapon = 		WEAPONTYPE_SMG
							sScenario = 		"random@street_race"
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_DR_VARIATION_2
					SWITCH iPed				
						CASE 0			
							model = 		G_M_M_ChiGoon_02
							vCoords = 		<<-1470.8040, -2437.8979, 12.9450>>
							fHeading = 		330.6
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 1			
							model = 		G_M_M_ChiGoon_02
							vCoords = 		<<-1465.3521, -2440.8430, 12.9450>>
							fHeading = 		330.6
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 2			
							model = 		S_M_Y_XMech_02
							vCoords = 		<<-1482.3051, -2452.6941, 12.9450>>
							fHeading = 		-0.601
							weapon = 		WEAPONTYPE_DLC_WRENCH
							sScenario = 		"WORLD_HUMAN_WINDOW_SHOP_BROWSE"
						BREAK			
						CASE 3			
							model = 		S_M_Y_XMech_02
							vCoords = 		<<-1469.1830, -2451.6069, 12.9450>>
							fHeading = 		132.999
							weapon = 		WEAPONTYPE_DLC_WRENCH
							sScenario = 		"random@street_race"
						BREAK			
						CASE 4			
							model = 		S_M_Y_XMech_02
							vCoords = 		<<-1476.2209, -2436.2439, 12.9450>>
							fHeading = 		302.198
							weapon = 		WEAPONTYPE_DLC_WRENCH
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 5			
							model = 		A_F_Y_EastSA_03
							vCoords = 		<<-1470.1260, -2452.4080, 12.9450>>
							fHeading = 		315.398
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"random@street_race"
						BREAK			
						CASE 6			
							model = 		A_F_Y_EastSA_03
							vCoords = 		<<-1475.1591, -2434.3530, 12.9450>>
							fHeading = 		297.198
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_CHEERING"
						BREAK			
						CASE 7			
							model = 		A_F_Y_EastSA_03
							vCoords = 		<<-1477.6350, -2438.2419, 12.9450>>
							fHeading = 		318.198
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_CHEERING"
						BREAK			
						CASE 8			
							model = 		A_F_Y_EastSA_03
							vCoords = 		<<-1459.9470, -2441.2361, 12.9450>>
							fHeading = 		10.598
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_CHEERING"
						BREAK			
						CASE 9			
							model = 		A_F_Y_EastSA_03
							vCoords = 		<<-1461.3890, -2443.4099, 12.9450>>
							fHeading = 		352.197
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_CHEERING"
						BREAK			
						CASE 10			
							model = 		G_M_M_ChiGoon_02
							vCoords = 		<<-1476.4547, -2447.5283, 12.9452>>
							fHeading = 		29.3999
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 11			
							model = 		G_M_M_ChiGoon_02
							vCoords = 		<<-1477.2284, -2446.3652, 12.9452>>
							fHeading = 		217.1996
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"random@street_race"
						BREAK			
						CASE 12			
							model = 		G_M_M_ChiGoon_02
							vCoords = 		<<-1464.8627, -2447.4399, 12.9452>>
							fHeading = 		262.3994
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_MEDIC_TEND_TO_DEAD"
						BREAK			
						CASE 13			
							model = 		A_F_Y_EastSA_03
							vCoords = 		<<-1464.2607, -2432.6570, 12.9451>>
							fHeading = 		158.9998
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"random@street_race"
						BREAK			
						CASE 14			
							model = 		G_M_M_ChiGoon_02
							vCoords = 		<<-1466.8121, -2455.0383, 12.9451>>
							fHeading = 		38.3987
							weapon = 		WEAPONTYPE_SMG
							sScenario = 		"random@street_race"
						BREAK			
						CASE 15			
							model = 		G_M_M_ChiGoon_02
							vCoords = 		<<-1480.1741, -2449.5212, 12.9452>>
							fHeading = 		82.1985
							weapon = 		WEAPONTYPE_SMG
							sScenario = 		"random@street_race"
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_DISRUPT_SHIPMENTS
			SWITCH eSubvariation
				CASE CHS_DS_VARIATION_1
					SWITCH iPed				
						CASE 0			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_02")) 
							vCoords = 		<<-3057.6399, -50.2315, 0.8214>>
							fHeading = 		294.9997
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 1			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_01"))
							vCoords = 		<<-3058.0020, -50.7832, 0.8266>>
							fHeading = 		294.9997
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK			
						CASE 2			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_02"))
							vCoords = 		<<2931.2932, -571.7087, 0.7988>>
							fHeading = 		294.9997
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 3			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_01"))
							vCoords = 		<<2932.2686, -572.4639, 1.8452>>
							fHeading = 		294.9997
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK			
						CASE 4			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_02"))
							vCoords = 		<<-964.4300, 5834.3037, 1.1306>>
							fHeading = 		294.9997
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 5			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_01"))
							vCoords = 		<<-964.8320, 5834.3066, 1.1302>>
							fHeading = 		294.9997
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK			
						CASE 6			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_02"))
							vCoords = 		<<379.1266, 3431.6599, 71.4047>>
							fHeading = 		294.9997
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 7			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_01"))
							vCoords = 		<<379.1266, 3431.6599, 71.4047>>
							fHeading = 		294.9997
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK			
						CASE 8			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_01"))
							vCoords = 		<<226.4798, -218.1770, 112.4103>>
							fHeading = 		294.9997
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 9			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_01"))
							vCoords = 		<<226.4798, -218.1770, 112.4103>>
							fHeading = 		294.9997
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK			
						CASE 10			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_01"))
							vCoords = 		<<-1745.6675, 4411.8374, 102.3208>>
							fHeading = 		294.9997
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 11			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_01"))
							vCoords = 		<<-1745.6675, 4411.8374, 102.3208>>
							fHeading = 		294.9997
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK			
						CASE 12			
							model = 		s_m_m_armoured_01
							vCoords = 		<<2715.6919, 3267.0884, 57.2643>>
							fHeading = 		294.9997
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 13			
							model = 		s_m_m_armoured_01
							vCoords = 		<<2715.6919, 3267.0884, 57.2643>>
							fHeading = 		294.9997
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK			
						CASE 14			
							model = 		s_m_m_armoured_01
							vCoords = 		<<1338.8030, -2383.7256, 53.5597>>
							fHeading = 		294.9997
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 15			
							model = 		s_m_m_armoured_01
							vCoords = 		<<1338.8030, -2383.7256, 53.5597>>
							fHeading = 		294.9997
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK			
						CASE 16			
							model = 		s_m_m_armoured_01
							vCoords = 		<<1623.4270, 6406.8120, 29.8595>>
							fHeading = 		294.9997
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 17			
							model = 		s_m_m_armoured_01
							vCoords = 		<<1623.4270, 6406.8120, 29.8595>>
							fHeading = 		294.9997
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK			
						CASE 18			
							model = 		s_m_m_armoured_01
							vCoords = 		<<-1707.6620, 2435.5054, 32.5201>>
							fHeading = 		294.9997
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 19			
							model = 		s_m_m_armoured_01
							vCoords = 		<<-1707.6620, 2435.5054, 32.5201>>
							fHeading = 		294.9997
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK		
						CASE 20			
							model = 		s_m_m_armoured_01
							vCoords = 		<<244.7002, -237.0265, 110.2503>>
							fHeading = 		294.9997
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK
						CASE 21			
							model = 		s_m_m_armoured_01
							vCoords = 		<<244.7002, -237.0265, 110.2503>>
							fHeading = 		294.9997
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK
						CASE 22			
							model = 		s_m_m_armoured_01
							vCoords = 		<<249.8918, -214.9318, 109.3465>>
							fHeading = 		294.9997
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK
						CASE 23			
							model = 		s_m_m_armoured_01
							vCoords = 		<<249.8918, -214.9318, 109.3465>>
							fHeading = 		294.9997
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK
						CASE 24		
							model = 		s_m_m_armoured_01
							vCoords = 		<<355.1865, 3436.4226, 68.1771>>
							fHeading = 		294.9997
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK
						CASE 25		
							model = 		s_m_m_armoured_01
							vCoords = 		<<355.1865, 3436.4226, 68.1771>>
							fHeading = 		294.9997
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK
						CASE 26			
							model = 		s_m_m_armoured_01
							vCoords = 		<<361.2102, 3417.2253, 70.8487>>
							fHeading = 		294.9997
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK
						CASE 27		
							model = 		s_m_m_armoured_01
							vCoords = 		<<361.2102, 3417.2253, 70.8487>>
							fHeading = 		294.9997
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK
						CASE 28		
							model = 		s_m_m_armoured_01
							vCoords = 		<<-1728.6554, 4396.8516, 98.0879>>
							fHeading = 		294.9997
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK
						CASE 29		
							model = 		s_m_m_armoured_01
							vCoords = 		<<-1728.6554, 4396.8516, 98.0879>>
							fHeading = 		294.9997
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK
						CASE 30			
							model = 		s_m_m_armoured_01
							vCoords = 		<<-1724.6342, 4418.0732, 97.9831>>
							fHeading = 		294.9997
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK
						CASE 31		
							model = 		s_m_m_armoured_01
							vCoords = 		<<-1724.6342, 4418.0732, 97.9831>>
							fHeading = 		294.9997
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK
					ENDSWITCH				
				BREAK
				CASE CHS_DS_VARIATION_2
					SWITCH iPed				
						CASE 0			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_02"))
							vCoords = 		<<493.9060, -3417.3989, 2.7313>>
							fHeading = 		0
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 1			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_01"))
							vCoords = 		<<493.9060, -3417.3989, 2.7313>>
							fHeading = 		0
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK			
						CASE 2			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_02"))
							vCoords = 		<<1523.2250, 6682.7876, 1.8514>>
							fHeading = 		0
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 3			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_01"))
							vCoords = 		<<1523.2250, 6682.7876, 1.8514>>
							fHeading = 		0
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK			
						CASE 4			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_02"))
							vCoords = 		<<-2986.1140, 3589.0154, 1.8623>>
							fHeading = 		0
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 5			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_01"))
							vCoords = 		<<-2986.1140, 3589.0154, 1.8623>>
							fHeading = 		0
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK			
						CASE 6			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_02"))
							vCoords = 		<<-709.4811, 1046.5803, 323.3814>>
							fHeading = 		0
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 7			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_01"))
							vCoords = 		<<-709.4811, 1046.5803, 323.3814>>
							fHeading = 		0
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK			
						CASE 8			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_01"))
							vCoords = 		<<831.2720, 2423.5835, 91.8115>>
							fHeading = 		0
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 9			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_01"))
							vCoords = 		<<831.2720, 2423.5835, 91.8115>>
							fHeading = 		0
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK			
						CASE 10			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_01"))
							vCoords = 		<<3002.9812, 4383.7563, 97.8685>>
							fHeading = 		0
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 11			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_01"))
							vCoords = 		<<3002.9812, 4383.7563, 97.8685>>
							fHeading = 		0
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK			
						CASE 12			
							model = 		s_m_m_armoured_01
							vCoords = 		<<-1244.3270, -610.3994, 31.1029>>
							fHeading = 		0
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 13			
							model = 		s_m_m_armoured_01
							vCoords = 		<<-1244.3270, -610.3994, 31.1029>>
							fHeading = 		0
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK			
						CASE 14			
							model = 		s_m_m_armoured_01
							vCoords = 		<<-817.2651, 5263.4077, 92.2641>>
							fHeading = 		0
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 15			
							model = 		s_m_m_armoured_01
							vCoords = 		<<-817.2651, 5263.4077, 92.2641>>
							fHeading = 		0
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK			
						CASE 16			
							model = 		s_m_m_armoured_01
							vCoords = 		<<2176.9351, -522.1904, 95.6297>>
							fHeading = 		0
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 17			
							model = 		s_m_m_armoured_01
							vCoords = 		<<2176.9351, -522.1904, 95.6297>>
							fHeading = 		0
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK			
						CASE 18			
							model = 		s_m_m_armoured_01
							vCoords = 		<<-749.4970, 2791.7373, 27.4358>>
							fHeading = 		0
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 19			
							model = 		s_m_m_armoured_01
							vCoords = 		<<-749.4970, 2791.7373, 27.4358>>
							fHeading = 		0
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK		
						CASE 20			
							model = 		s_m_m_armoured_01
							vCoords = 		<<-719.8558, 1026.2434, 322.7350>>
							fHeading = 		294.9997
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK
						CASE 21			
							model = 		s_m_m_armoured_01
							vCoords = 		<<-719.8558, 1026.2434, 322.7350>>
							fHeading = 		294.9997
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK
						CASE 22			
							model = 		s_m_m_armoured_01
							vCoords = 		<<-698.0895, 1026.2190, 321.8038>>
							fHeading = 		294.9997
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK
						CASE 23			
							model = 		s_m_m_armoured_01
							vCoords = 		<<-698.0895, 1026.2190, 321.8038>>
							fHeading = 		294.9997
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK
						CASE 24		
							model = 		s_m_m_armoured_01
							vCoords = 		<<812.0942, 2423.2615, 86.0032>>
							fHeading = 		294.9997
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK
						CASE 25		
							model = 		s_m_m_armoured_01
							vCoords = 		<<812.0942, 2423.2615, 86.0032>>
							fHeading = 		294.9997
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK
						CASE 26			
							model = 		s_m_m_armoured_01
							vCoords = 		<<820.2598, 2404.4558, 88.4437>>
							fHeading = 		294.9997
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK
						CASE 27		
							model = 		s_m_m_armoured_01
							vCoords = 		<<820.2598, 2404.4558, 88.4437>>
							fHeading = 		294.9997
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK
						CASE 28		
							model = 		s_m_m_armoured_01
							vCoords = 		<<3008.4382, 4409.4888, 96.4163>>
							fHeading = 		294.9997
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK
						CASE 29		
							model = 		s_m_m_armoured_01
							vCoords = 		<<3008.4382, 4409.4888, 96.4163>>
							fHeading = 		294.9997
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK
						CASE 30			
							model = 		s_m_m_armoured_01
							vCoords = 		<<2986.5920, 4407.8770, 97.7386>>
							fHeading = 		294.9997
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK
						CASE 31			
							model = 		s_m_m_armoured_01
							vCoords = 		<<2986.5920, 4407.8770, 97.7386>>
							fHeading = 		294.9997
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK
					ENDSWITCH				
				BREAK
				CASE CHS_DS_VARIATION_3
					SWITCH iPed				
						CASE 0			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_02"))
							vCoords = 		<<-2002.7330, -3176.0562, 1.8867>>
							fHeading = 		0
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 1			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_01"))
							vCoords = 		<<-2002.6154, -3176.1152, 1.8750>>
							fHeading = 		0
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK			
						CASE 2			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_02"))
							vCoords = 		<<679.3990, 6717.0391, 2.6539>>
							fHeading = 		0
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 3			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_01"))
							vCoords = 		<<678.4196, 6716.9155, 1.6337>>
							fHeading = 		0
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK			
						CASE 4			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_02"))
							vCoords = 		<<-3131.9700, 2009.2170, 0.8503>>
							fHeading = 		0
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 5			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_01"))
							vCoords = 		<<-3131.9700, 2009.2170, 0.8503>>
							fHeading = 		0
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK			
						CASE 6			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_01"))
							vCoords = 		<<3412.2363, 3675.3267, 89.0977>>
							fHeading = 		0
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 7			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_01"))
							vCoords = 		<<3412.2363, 3675.3267, 89.0977>>
							fHeading = 		0
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK			
						CASE 8			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_01"))
							vCoords = 		<<2026.3929, 5177.4214, 76.2079>>
							fHeading = 		0
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 9			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_01"))
							vCoords = 		<<2026.3929, 5177.4214, 76.2079>>
							fHeading = 		0
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK			
						CASE 10			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_01"))
							vCoords = 		<<915.8270, -3045.4397, 46.4916>>
							fHeading = 		0
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 11			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_01"))
							vCoords = 		<<915.8270, -3045.4397, 46.4916>>
							fHeading = 		0
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK			
						CASE 12			
							model = 		s_m_m_armoured_01
							vCoords = 		<<2477.5769, -701.5007, 63.1161>>
							fHeading = 		0
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 13			
							model = 		s_m_m_armoured_01
							vCoords = 		<<2477.5769, -701.5007, 63.1161>>
							fHeading = 		0
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK			
						CASE 14			
							model = 		s_m_m_armoured_01
							vCoords = 		<<251.0900, 810.6780, 198.3063>>
							fHeading = 		0
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 15			
							model = 		s_m_m_armoured_01
							vCoords = 		<<251.0900, 810.6780, 198.3063>>
							fHeading = 		0
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK			
						CASE 16			
							model = 		s_m_m_armoured_01
							vCoords = 		<<-1787.8114, 4736.3677, 58.3417>>
							fHeading = 		0
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 17			
							model = 		s_m_m_armoured_01
							vCoords = 		<<-1788.0553, 4736.7920, 58.7911>>
							fHeading = 		0
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK			
						CASE 18			
							model = 		s_m_m_armoured_01
							vCoords = 		<<269.5160, 2687.5730, 46.1571>>
							fHeading = 		0
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 19			
							model = 		s_m_m_armoured_01
							vCoords = 		<<269.5160, 2687.5730, 46.1571>>
							fHeading = 		0
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK	
						CASE 20			
							model = 		s_m_m_armoured_01
							vCoords = 		<<926.7957, -3066.6318, 44.5206>>
							fHeading = 		294.9997
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK
						CASE 21			
							model = 		s_m_m_armoured_01
							vCoords = 		<<926.7957, -3066.6318, 44.5206>>
							fHeading = 		294.9997
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK
						CASE 22			
							model = 		s_m_m_armoured_01
							vCoords = 		<<946.3006, -3055.3318, 45.2692>>
							fHeading = 		294.9997
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK
						CASE 23			
							model = 		s_m_m_armoured_01
							vCoords = 		<<946.3006, -3055.3318, 45.2692>>
							fHeading = 		294.9997
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK
						CASE 24		
							model = 		s_m_m_armoured_01
							vCoords = 		<<3431.8254, 3690.4431, 85.0151>>
							fHeading = 		294.9997
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK
						CASE 25		
							model = 		s_m_m_armoured_01
							vCoords = 		<<3431.8254, 3690.4431, 85.0151>>
							fHeading = 		294.9997
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK
						CASE 26			
							model = 		s_m_m_armoured_01
							vCoords = 		<<3422.6826, 3713.4436, 87.3928>>
							fHeading = 		294.9997
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK
						CASE 27		
							model = 		s_m_m_armoured_01
							vCoords = 		<<3422.6826, 3713.4436, 87.3928>>
							fHeading = 		294.9997
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK
						CASE 28		
							model = 		s_m_m_armoured_01
							vCoords = 		<<2045.9901, 5189.9595, 74.9697>>
							fHeading = 		294.9997
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK
						CASE 29		
							model = 		s_m_m_armoured_01
							vCoords = 		<<2045.9901, 5189.9595, 74.9697>>
							fHeading = 		294.9997
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK
						CASE 30			
							model = 		s_m_m_armoured_01
							vCoords = 		<<2032.4774, 5205.2510, 71.4432>>
							fHeading = 		294.9997
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK
						CASE 31			
							model = 		s_m_m_armoured_01
							vCoords = 		<<2032.4774, 5205.2510, 71.4432>>
							fHeading = 		294.9997
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_LOST_MC_BIKES
			SWITCH eSubvariation
				CASE CHS_LMC_VARIATION_1
					SWITCH iPed				
						CASE 0			
							model = 		G_M_Y_Lost_02
							vCoords = 		<<168.9920, 6411.6108, 30.1480>>
							fHeading = 		82.999
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 1			
							model = 		G_M_Y_Lost_02
							vCoords = 		<<176.5590, 6433.0630, 30.2130>>
							fHeading = 		44.598
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_MUSCLE_FLEX"
						BREAK			
						CASE 2			
							model = 		G_M_Y_Lost_02
							vCoords = 		<<178.3180, 6421.6040, 31.3620>>
							fHeading = 		151.598
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 3			
							model = 		G_M_Y_Lost_02
							vCoords = 		<<177.1860, 6421.2739, 31.3620>>
							fHeading = 		243.997
							weapon = 		WEAPONTYPE_SAWNOFFSHOTGUN
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 4			
							model = 		G_M_Y_Lost_02
							vCoords = 		<<177.7040, 6415.2100, 31.3620>>
							fHeading = 		28.397
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"WORLD_HUMAN_SMOKING_POT"
						BREAK			
						CASE 5			
							model = 		G_M_Y_Lost_02
							vCoords = 		<<190.0070, 6421.3281, 30.1310>>
							fHeading = 		121.397
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 6			
							model = 		G_M_Y_Lost_02
							vCoords = 		<<188.9680, 6420.5039, 30.1310>>
							fHeading = 		310.197
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 7			
							model = 		G_M_Y_Lost_02
							vCoords = 		<<182.0410, 6422.6919, 31.3620>>
							fHeading = 		259.597
							weapon = 		WEAPONTYPE_SAWNOFFSHOTGUN
							sScenario = 		"CODE_HUMAN_MEDIC_TEND_TO_DEAD"
						BREAK			
						CASE 8			
							model = 		G_F_Y_Lost_01
							vCoords = 		<<175.5430, 6433.9009, 30.2320>>
							fHeading = 		234.397
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_PROSTITUTE_HIGH_CLASS"
						BREAK			
						CASE 9			
							model = 		G_F_Y_Lost_01
							vCoords = 		<<167.6720, 6411.6890, 30.1520>>
							fHeading = 		275.197
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 10			
							model = 		G_F_Y_Lost_01
							vCoords = 		<<177.9930, 6420.3799, 31.3620>>
							fHeading = 		25.196
							weapon = 		WEAPONTYPE_SAWNOFFSHOTGUN
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 11			
							model = 		G_F_Y_Lost_01
							vCoords = 		<<192.7300, 6427.5620, 30.1480>>
							fHeading = 		63.196
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"WORLD_HUMAN_WELDING"
						BREAK			
						CASE 12			
							model = 		G_F_Y_Lost_01
							vCoords = 		<<171.9680, 6418.2461, 31.3620>>
							fHeading = 		68.796
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_SEAT_LEDGE"
						BREAK			
						CASE 13			
							model = 		G_F_Y_Lost_01
							vCoords = 		<<173.5180, 6430.3940, 30.2100>>
							fHeading = 		343.195
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_LMC_VARIATION_2
					SWITCH iPed				
						CASE 0			
							model = 		G_M_Y_Lost_02
							vCoords = 		<<1948.9180, 4640.9551, 39.6240>>
							fHeading = 		152.999
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 1			
							model = 		G_M_Y_Lost_02
							vCoords = 		<<1963.3669, 4645.5259, 39.7540>>
							fHeading = 		314.199
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 2			
							model = 		G_M_Y_Lost_02
							vCoords = 		<<1970.0250, 4649.6479, 39.9420>>
							fHeading = 		320.199
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 3			
							model = 		G_M_Y_Lost_02
							vCoords = 		<<1958.7590, 4628.3291, 40.0720>>
							fHeading = 		34.198
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"WORLD_HUMAN_SMOKING_POT"
						BREAK			
						CASE 4			
							model = 		G_M_Y_Lost_02
							vCoords = 		<<1961.4310, 4641.9019, 39.7150>>
							fHeading = 		272.398
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 5			
							model = 		G_M_Y_Lost_02
							vCoords = 		<<1947.7380, 4640.2251, 39.6580>>
							fHeading = 		247.999
							weapon = 		WEAPONTYPE_SAWNOFFSHOTGUN
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 6			
							model = 		G_M_Y_Lost_02
							vCoords = 		<<1944.7870, 4631.0571, 39.4570>>
							fHeading = 		301.999
							weapon = 		WEAPONTYPE_SAWNOFFSHOTGUN
							sScenario = 		"WORLD_HUMAN_WELDING"
						BREAK			
						CASE 7			
							model = 		G_F_Y_Lost_01
							vCoords = 		<<1964.1420, 4646.3301, 39.7860>>
							fHeading = 		141.999
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 8			
							model = 		G_F_Y_Lost_01
							vCoords = 		<<1948.8510, 4639.5542, 39.6000>>
							fHeading = 		38.598
							weapon = 		WEAPONTYPE_SAWNOFFSHOTGUN
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 9			
							model = 		G_M_Y_Lost_02
							vCoords = 		<<1961.2070, 4634.3052, 39.7520>>
							fHeading = 		301.398
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_PUSH_UPS"
						BREAK			
						CASE 10			
							model = 		G_F_Y_Lost_01
							vCoords = 		<<1962.3810, 4635.2520, 39.7670>>
							fHeading = 		129.198
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"WORLD_HUMAN_PROSTITUTE_HIGH_CLASS"
						BREAK			
						CASE 11			
							model = 		G_F_Y_Lost_01
							vCoords = 		<<1966.7240, 4634.5181, 40.1060>>
							fHeading = 		46.997
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK			
						CASE 12			
							model = 		G_F_Y_Lost_01
							vCoords = 		<<1935.2800, 4634.0659, 39.4830>>
							fHeading = 		287.597
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 13			
							model = 		G_M_Y_Lost_02
							vCoords = 		<<1954.4680, 4637.1958, 39.6380>>
							fHeading = 		110.796
							weapon = 		WEAPONTYPE_SAWNOFFSHOTGUN
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
					ENDSWITCH								
				BREAK			
				CASE CHS_LMC_VARIATION_3
					SWITCH iPed				
						CASE 0			
							model = 		G_M_Y_Lost_02
							vCoords = 		<<24.4070, 3706.4299, 38.6830>>
							fHeading = 		336.4
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 1			
							model = 		G_M_Y_Lost_02
							vCoords = 		<<35.6590, 3722.2029, 38.6460>>
							fHeading = 		210.399
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 2			
							model = 		G_M_Y_Lost_02
							vCoords = 		<<35.8330, 3720.7991, 38.5850>>
							fHeading = 		315.999
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 3			
							model = 		G_M_Y_Lost_02
							vCoords = 		<<34.5700, 3729.5830, 38.5700>>
							fHeading = 		149.998
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_WELDING"
						BREAK			
						CASE 4			
							model = 		G_M_Y_Lost_02
							vCoords = 		<<28.7500, 3729.4751, 38.5620>>
							fHeading = 		38.598
							weapon = 		WEAPONTYPE_SAWNOFFSHOTGUN
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 5			
							model = 		G_M_Y_Lost_02
							vCoords = 		<<16.2480, 3717.8201, 38.6350>>
							fHeading = 		246.597
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_PUSH_UPS"
						BREAK			
						CASE 6			
							model = 		G_M_Y_Lost_02
							vCoords = 		<<35.1560, 3705.5820, 38.6080>>
							fHeading = 		243.997
							weapon = 		WEAPONTYPE_SAWNOFFSHOTGUN
							sScenario = 		"CODE_HUMAN_MEDIC_TEND_TO_DEAD"
						BREAK			
						CASE 7			
							model = 		G_M_Y_Lost_02
							vCoords = 		<<25.4240, 3712.2610, 38.6750>>
							fHeading = 		126.197
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 8			
							model = 		G_F_Y_Lost_01
							vCoords = 		<<24.8200, 3707.4861, 38.6860>>
							fHeading = 		161.397
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 9			
							model = 		G_F_Y_Lost_01
							vCoords = 		<<17.7600, 3717.2129, 38.6210>>
							fHeading = 		75.397
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"WORLD_HUMAN_PROSTITUTE_HIGH_CLASS"
						BREAK			
						CASE 10			
							model = 		G_F_Y_Lost_01
							vCoords = 		<<27.9230, 3730.3491, 38.6190>>
							fHeading = 		225.396
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 11			
							model = 		G_F_Y_Lost_01
							vCoords = 		<<40.4670, 3715.4089, 38.6760>>
							fHeading = 		158.596
							weapon = 		WEAPONTYPE_SAWNOFFSHOTGUN
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK			
						CASE 12			
							model = 		G_F_Y_Lost_01
							vCoords = 		<<36.9000, 3721.6521, 38.5670>>
							fHeading = 		103.396
							weapon = 		WEAPONTYPE_SAWNOFFSHOTGUN
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 13			
							model = 		G_F_Y_Lost_01
							vCoords = 		<<40.4040, 3709.4409, 38.7310>>
							fHeading = 		54.196
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_GARDENER_PLANT"
						BREAK			
					ENDSWITCH				

				BREAK
			ENDSWITCH
		BREAK		
		CASE CHV_CARGO_SHIP
			SWITCH eSubvariation
				CASE CHS_CS_VARIATION_1
					SWITCH iPed				
						CASE 0			
							model = 		G_M_M_ChiGoon_02
							vCoords = 		<<-132.3260, -2386.4580, 5.0000>>
							fHeading = 		200.599
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 1			
							model = 		G_M_M_ChiGoon_02
							vCoords = 		<<-131.6940, -2387.7549, 5.0000>>
							fHeading = 		21.199
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 2			
							model = 		G_M_M_ChiGoon_02
							vCoords = 		<<-107.4014, -2370.3455, 15.6131>>
							fHeading = 		86.3989
							weapon = 		WEAPONTYPE_SMG
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 3			
							model = 		G_M_M_ChiGoon_02
							vCoords = 		<<-138.6630, -2370.8250, 19.6260>>
							fHeading = 		296.998
							weapon = 		WEAPONTYPE_SMG
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 4			
							model = 		G_M_M_ChiGoon_02
							vCoords = 		<<-137.6190, -2370.3140, 19.6260>>
							fHeading = 		125.798
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 5			
							model = 		G_M_M_ChiGoon_02
							vCoords = 		<<-130.3000, -2376.3669, 8.3190>>
							fHeading = 		179.798
							weapon = 		WEAPONTYPE_SMG
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 6			
							model = 		G_M_M_ChiGoon_02
							vCoords = 		<<-119.3650, -2361.0510, 12.7920>>
							fHeading = 		32.397
							weapon = 		WEAPONTYPE_SMG
							sScenario = 		"WORLD_HUMAN_BINOCULARS"
						BREAK			
						CASE 7			
							model = 		G_M_M_ChiGoon_02
							vCoords = 		<<-145.4970, -2353.4221, 10.9720>>
							fHeading = 		2.397
							weapon = 		WEAPONTYPE_SMG
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 8			
							model = 		G_M_M_ChiGoon_02
							vCoords = 		<<-155.2740, -2367.9021, 19.6360>>
							fHeading = 		359.197
							weapon = 		WEAPONTYPE_SMG
							sScenario = 		"WORLD_HUMAN_WELDING"
						BREAK			
						CASE 9			
							model = 		G_M_M_ChiGoon_02
							vCoords = 		<<-151.2450, -2373.5681, 19.6360>>
							fHeading = 		179.797
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_SEAT_LEDGE"
						BREAK			
						CASE 10			
							model = 		G_M_M_ChiGoon_02
							vCoords = 		<<-137.2190, -2352.7090, 8.3190>>
							fHeading = 		-0.803
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_BINOCULARS"
						BREAK			
						CASE 11			
							model = 		G_M_M_ChiGoon_02
							vCoords = 		<<-148.3020, -2366.6960, 8.3190>>
							fHeading = 		266.196
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_SMOKING_POT"
						BREAK			
						CASE 12			
							model = 		G_M_M_ChiGoon_02
							vCoords = 		<<-147.1460, -2392.0149, 5.0000>>
							fHeading = 		56.198
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_MEDIC_TIME_OF_DEATH"
						BREAK			
						CASE 13			
							model = 		G_M_M_ChiGoon_02
							vCoords = 		<<-143.6246, -2383.7732, 5.0000>>
							fHeading = 		269.9967
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 14			
							model = 		G_M_M_ChiGoon_02
							vCoords = 		<<-145.7322, -2378.0200, 8.3192>>
							fHeading = 		93.9962
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 15			
							model = 		G_M_M_ChiGoon_02
							vCoords = 		<<-127.7453, -2356.8025, 8.3192>>
							fHeading = 		0.9961
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK			
						CASE 16			
							model = 		G_M_M_ChiGoon_02
							vCoords = 		<<-126.3577, -2374.7910, 8.3192>>
							fHeading = 		189.3957
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 17			
							model = 		G_M_M_ChiGoon_02
							vCoords = 		<<-133.6211, -2356.7041, 19.6259>>
							fHeading = 		337.5956
							weapon = 		WEAPONTYPE_SMG
							sScenario = 		"WORLD_HUMAN_BINOCULARS"
						BREAK			
						CASE 18			
							model = 		G_M_M_ChiGoon_02
							vCoords = 		<<-141.0069, -2379.0493, 13.9841>>
							fHeading = 		182.5949
							weapon = 		WEAPONTYPE_SMG
							sScenario = 		"WORLD_HUMAN_SEAT_LEDGE"
						BREAK			
						CASE 19			
							model = 		G_M_M_ChiGoon_02
							vCoords = 		<<-150.7122, -2358.0720, 22.4565>>
							fHeading = 		4.5946
							weapon = 		WEAPONTYPE_SMG
							sScenario = 		"WORLD_HUMAN_SEAT_LEDGE"
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_CS_VARIATION_2
					SWITCH iPed				
						CASE 0			
							model = 		G_M_Y_Lost_01
							vCoords = 		<<17.3050, 6217.7861, 30.5290>>
							fHeading = 		131.199
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 1			
							model = 		G_M_Y_Lost_01
							vCoords = 		<<-1.1600, 6212.6890, 30.4380>>
							fHeading = 		23.399
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_MEDIC_TIME_OF_DEATH"
						BREAK			
						CASE 2			
							model = 		G_M_Y_Lost_01
							vCoords = 		<<-19.8210, 6197.8599, 30.2470>>
							fHeading = 		214.599
							weapon = 		WEAPONTYPE_PUMPSHOTGUN
							sScenario = 		"WORLD_HUMAN_SECURITY_SHINE_TORCH"
						BREAK			
						CASE 3			
							model = 		G_M_Y_Lost_01
							vCoords = 		<<-6.2160, 6207.5132, 38.7550>>
							fHeading = 		7.598
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"WORLD_HUMAN_BINOCULARS"
						BREAK			
						CASE 4			
							model = 		G_M_Y_Lost_01
							vCoords = 		<<-10.0150, 6200.0000, 38.7550>>
							fHeading = 		295.998
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 5			
							model = 		G_M_Y_Lost_01
							vCoords = 		<<-9.0410, 6200.6011, 38.7550>>
							fHeading = 		120.598
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"WORLD_HUMAN_SMOKING_POT"
						BREAK			
						CASE 6			
							model = 		G_M_Y_Lost_01
							vCoords = 		<<-11.7100, 6191.4082, 35.9550>>
							fHeading = 		134.398
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK			
						CASE 7			
							model = 		G_M_Y_Lost_01
							vCoords = 		<<-16.0830, 6207.3789, 30.0530>>
							fHeading = 		178.397
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 8			
							model = 		G_M_Y_Lost_01
							vCoords = 		<<-16.0650, 6206.2329, 30.0870>>
							fHeading = 		358.597
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 9			
							model = 		G_M_Y_Lost_01
							vCoords = 		<<-23.5400, 6212.0229, 30.3570>>
							fHeading = 		124.597
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 10			
							model = 		G_M_Y_Lost_01
							vCoords = 		<<-9.6290, 6206.5840, 30.3000>>
							fHeading = 		216.997
							weapon = 		WEAPONTYPE_PUMPSHOTGUN
							sScenario = 		"WORLD_HUMAN_WELDING"
						BREAK			
						CASE 11			
							model = 		G_M_Y_Lost_01
							vCoords = 		<<-5.6710, 6212.5420, 30.3690>>
							fHeading = 		302.997
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 12			
							model = 		G_M_Y_Lost_01
							vCoords = 		<<-2.6274, 6208.0942, 35.9544>>
							fHeading = 		319.398
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_SEAT_LEDGE"
						BREAK			
						CASE 13			
							model = 		G_M_Y_Lost_01
							vCoords = 		<<9.1309, 6216.1343, 30.4811>>
							fHeading = 		60.1967
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 14			
							model = 		G_M_Y_Lost_01
							vCoords = 		<<8.0420, 6216.7441, 30.4731>>
							fHeading = 		236.1965
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 15			
							model = 		G_M_Y_Lost_01
							vCoords = 		<<4.8739, 6211.9316, 33.2952>>
							fHeading = 		155.5964
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"WORLD_HUMAN_SEAT_LEDGE"
						BREAK			
						CASE 16			
							model = 		G_M_Y_Lost_01
							vCoords = 		<<-5.8237, 6196.1660, 38.7552>>
							fHeading = 		254.7962
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"WORLD_HUMAN_GUARD_STAND"
						BREAK			
						CASE 17			
							model = 		G_M_Y_Lost_01
							vCoords = 		<<-15.4875, 6200.7729, 38.7553>>
							fHeading = 		46.5959
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_SEAT_LEDGE"
						BREAK			
						CASE 18			
							model = 		G_M_Y_Lost_01
							vCoords = 		<<-23.1691, 6204.9204, 30.0255>>
							fHeading = 		336.3958
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK			
						CASE 19			
							model = 		G_M_Y_Lost_01
							vCoords = 		<<-28.7127, 6202.2598, 29.9266>>
							fHeading = 		29.9956
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_CS_VARIATION_3
					SWITCH iPed				
						CASE 0			
							model = 		G_M_M_ArmGoon_01
							vCoords = 		<<584.2333, -1884.0233, 24.3385>>
							fHeading = 		53.199
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_SECURITY_SHINE_TORCH"
						BREAK			
						CASE 1			
							model = 		G_M_M_ArmGoon_01
							vCoords = 		<<568.3235, -1936.5096, 23.7551>>
							fHeading = 		71.7989
							weapon = 		WEAPONTYPE_SMG
							sScenario = 		"WORLD_HUMAN_SMOKING_POT"
						BREAK			
						CASE 2			
							model = 		G_M_M_ArmGoon_01
							vCoords = 		<<594.4790, -1874.3240, 23.9340>>
							fHeading = 		316.598
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_MEDIC_TIME_OF_DEATH"
						BREAK			
						CASE 3			
							model = 		G_M_M_ArmGoon_01
							vCoords = 		<<587.8500, -1892.1360, 24.3820>>
							fHeading = 		282.198
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 4			
							model = 		G_M_M_ArmGoon_01
							vCoords = 		<<588.8920, -1891.8831, 24.4000>>
							fHeading = 		101.598
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 5			
							model = 		G_M_M_ArmGoon_01
							vCoords = 		<<585.6218, -1874.1791, 29.7761>>
							fHeading = 		177.597
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 6			
							model = 		G_M_M_ArmGoon_01
							vCoords = 		<<585.6262, -1875.3722, 29.7761>>
							fHeading = 		358.197
							weapon = 		WEAPONTYPE_SMG
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 7			
							model = 		G_M_M_ArmGoon_01
							vCoords = 		<<594.0630, -1866.3440, 29.5130>>
							fHeading = 		228.197
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_SEAT_LEDGE"
						BREAK			
						CASE 8			
							model = 		G_M_M_ArmGoon_01
							vCoords = 		<<594.1180, -1908.0490, 29.6930>>
							fHeading = 		197.597
							weapon = 		WEAPONTYPE_SMG
							sScenario = 		"WORLD_HUMAN_BINOCULARS"
						BREAK			
						CASE 9			
							model = 		G_M_M_ArmGoon_01
							vCoords = 		<<596.6590, -1880.2340, 24.0420>>
							fHeading = 		72.796
							weapon = 		WEAPONTYPE_SMG
							sScenario = 		"WORLD_HUMAN_GUARD_STAND"
						BREAK			
						CASE 10			
							model = 		G_M_M_ArmGoon_01
							vCoords = 		<<599.4960, -1891.3680, 24.1040>>
							fHeading = 		330.596
							weapon = 		WEAPONTYPE_SMG
							sScenario = 		"WORLD_HUMAN_SMOKING_POT"
						BREAK			
						CASE 11			
							model = 		G_M_M_ArmGoon_01
							vCoords = 		<<583.5470, -1892.7540, 24.1390>>
							fHeading = 		144.596
							weapon = 		WEAPONTYPE_SMG
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 12			
							model = 		G_M_M_ArmGoon_01
							vCoords = 		<<578.6170, -1874.3840, 29.8750>>
							fHeading = 		144.596
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_SEAT_LEDGE"
						BREAK			
						CASE 13			
							model = 		G_M_M_ArmGoon_01
							vCoords = 		<<569.7258, -1913.6362, 23.6644>>
							fHeading = 		106.5959
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_GUARD_STAND"
						BREAK			
						CASE 14			
							model = 		G_M_M_ArmGoon_01
							vCoords = 		<<563.6579, -1904.4518, 23.6649>>
							fHeading = 		134.5956
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_GUARD_STAND"
						BREAK			
						CASE 15			
							model = 		G_M_M_ArmGoon_01
							vCoords = 		<<577.1078, -1891.1526, 29.8247>>
							fHeading = 		221.3958
							weapon = 		WEAPONTYPE_SMG
							sScenario = 		"WORLD_HUMAN_SEAT_LEDGE"
						BREAK			
						CASE 16			
							model = 		G_M_M_ArmGoon_01
							vCoords = 		<<601.7185, -1861.5465, 23.7498>>
							fHeading = 		141.5955
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK			
						CASE 17			
							model = 		G_M_M_ArmGoon_01
							vCoords = 		<<581.2800, -1900.4176, 23.9532>>
							fHeading = 		183.5952
							weapon = 		WEAPONTYPE_SMG
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 18			
							model = 		G_M_M_ArmGoon_01
							vCoords = 		<<581.4259, -1901.6046, 23.9858>>
							fHeading = 		1.7949
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 19			
							model = 		G_M_M_ArmGoon_01
							vCoords = 		<<574.1752, -1896.3397, 23.8226>>
							fHeading = 		36.3949
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_SECURITY_SHINE_TORCH"
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_ELECTRIC_DRILLS_2
			SWITCH eSubvariation
				CASE CHS_ED2_VARIATION_1
					SWITCH iPed				
						CASE 0			
							model = 		S_M_Y_Construct_01
							vCoords = 		<<-916.9493, 398.2994, 78.3899>>
							fHeading = 		323.782
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 1			
							model = 		S_M_Y_Construct_01
							vCoords = 		<<-916.9493, 398.2994, 78.3899>>
							fHeading = 		323.782
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_ED2_VARIATION_2
					SWITCH iPed				
						CASE 0			
							model = 		S_M_Y_Construct_01
							vCoords = 		<<-383.3271, 6269.2480, 29.6773>>
							fHeading = 		47.4604
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 1			
							model = 		S_M_Y_Construct_01
							vCoords = 		<<-383.3271, 6269.2480, 29.6773>>
							fHeading = 		47.4604
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_ED2_VARIATION_3
					SWITCH iPed				
						CASE 0			
							model = 		S_M_Y_Construct_01
							vCoords = 		<<1153.1025, 2095.1167, 54.9150>>
							fHeading = 		236.6638
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 1			
							model = 		S_M_Y_Construct_01
							vCoords = 		<<1153.1025, 2095.1167, 54.9150>>
							fHeading = 		236.6638
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_ZANCUDO_SHIPMENT	
			SWITCH eSubvariation
				CASE CHS_ZS_VARIATION_1
					SWITCH iPed					
						CASE 0				
							model = 		S_M_Y_MARINE_03	
							vCoords = 		<<-2452.9089, 2953.3169, 31.8100>>	
							fHeading = 		125.799	
							weapon = 		WEAPONTYPE_PISTOL	
							sScenario = 		"CODE_HUMAN_MEDIC_TEND_TO_DEAD"	
						BREAK				
						CASE 1				
							model = 		S_M_Y_MARINE_03	
							vCoords = 		<<-2438.7610, 2974.1069, 31.8100>>	
							fHeading = 		239.199	
							weapon = 		WEAPONTYPE_PISTOL	
							sScenario = 		""	
						BREAK				
						CASE 2				
							model = 		S_M_Y_MARINE_03	
							vCoords = 		<<-2437.4690, 2976.4470, 31.8100>>	
							fHeading = 		239.199	
							weapon = 		WEAPONTYPE_PISTOL	
							sScenario = 		""	
						BREAK				
						CASE 3				
							model = 		S_M_Y_MARINE_03	
							vCoords = 		<<-2448.4480, 2972.4600, 31.8100>>	
							fHeading = 		197.399	
							weapon = 		WEAPONTYPE_PISTOL	
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"	
						BREAK				
						CASE 4				
							model = 		S_M_Y_MARINE_03	
							vCoords = 		<<-2448.0029, 2970.9580, 31.8100>>	
							fHeading = 		12.798	
							weapon = 		WEAPONTYPE_PISTOL	
							sScenario = 		"WORLD_HUMAN_COP_IDLES"	
						BREAK				
						CASE 5				
							model = 		S_M_Y_MARINE_03	
							vCoords = 		<<-2447.4890, 2962.0000, 31.8100>>	
							fHeading = 		204.798	
							weapon = 		WEAPONTYPE_CARBINERIFLE	
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"	
						BREAK				
						CASE 6				
							model = 		S_M_Y_MARINE_03	
							vCoords = 		<<-2441.1111, 2951.5229, 33.8540>>	
							fHeading = 		330.197	
							weapon = 		WEAPONTYPE_PISTOL	
							sScenario = 		"WORLD_HUMAN_GUARD_STAND"	
						BREAK				
						CASE 7				
							model = 		S_M_Y_MARINE_03	
							vCoords = 		<<-2444.2971, 2972.3230, 31.8100>>	
							fHeading = 		329.197	
							weapon = 		WEAPONTYPE_CARBINERIFLE	
							sScenario = 		"CODE_HUMAN_PATROL_2H"	
						BREAK				
						CASE 8				
							model = 		S_M_Y_MARINE_03	
							vCoords = 		<<-2458.0969, 2944.2490, 31.9600>>	
							fHeading = 		331.597	
							weapon = 		WEAPONTYPE_CARBINERIFLE	
							sScenario = 		"CODE_HUMAN_PATROL_2H"	
						BREAK				
						CASE 9				
							model = 		S_M_Y_MARINE_03	
							vCoords = 		<<-2457.8250, 2961.7209, 31.8100>>	
							fHeading = 		13.597	
							weapon = 		WEAPONTYPE_CARBINERIFLE	
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE_UPRIGHT"	
						BREAK		
						CASE 10			
							model = 		S_M_Y_MARINE_03
							vCoords = 		<<-2418.7217, 3007.3015, 31.8099>>
							fHeading = 		344.3986
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 11			
							model = 		S_M_Y_MARINE_03
							vCoords = 		<<-2423.2195, 3014.7114, 31.8099>>
							fHeading = 		245.1983
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_GUARD_STAND"
						BREAK			
					ENDSWITCH					
				BREAK
				CASE CHS_ZS_VARIATION_2
					SWITCH iPed				
						CASE 0			
							model = 		S_M_Y_MARINE_03
							vCoords = 		<<-2283.6411, 3175.0740, 31.8100>>
							fHeading = 		113.398
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 1			
							model = 		S_M_Y_MARINE_03
							vCoords = 		<<-2291.0969, 3189.8701, 31.8100>>
							fHeading = 		329.797
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_MEDIC_TEND_TO_DEAD"
						BREAK			
						CASE 2			
							model = 		S_M_Y_MARINE_03
							vCoords = 		<<-2266.0669, 3179.5349, 31.8100>>
							fHeading = 		56.597
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 3			
							model = 		S_M_Y_MARINE_03
							vCoords = 		<<-2264.8420, 3181.8391, 31.8100>>
							fHeading = 		56.597
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 4			
							model = 		S_M_Y_MARINE_03
							vCoords = 		<<-2274.1956, 3168.3999, 31.8100>>
							fHeading = 		58.397
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_WELDING"
						BREAK			
						CASE 5			
							model = 		S_M_Y_MARINE_03
							vCoords = 		<<-2278.1599, 3180.2173, 31.8100>>
							fHeading = 		147.9957
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_PUSH_UPS"
						BREAK			
						CASE 6			
							model = 		S_M_Y_MARINE_03
							vCoords = 		<<-2279.3757, 3181.1350, 31.8100>>
							fHeading = 		147.3956
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_PUSH_UPS"
						BREAK			
						CASE 7			
							model = 		S_M_Y_MARINE_03
							vCoords = 		<<-2293.0459, 3184.4121, 31.8100>>
							fHeading = 		214.396
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE_UPRIGHT"
						BREAK			
						CASE 8			
							model = 		S_M_Y_MARINE_03
							vCoords = 		<<-2292.1760, 3183.0190, 31.8100>>
							fHeading = 		33.196
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_COP_IDLES"
						BREAK			
						CASE 9			
							model = 		S_M_Y_MARINE_03
							vCoords = 		<<-2300.7170, 3183.0730, 31.8100>>
							fHeading = 		57.596
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_ZS_VARIATION_3
					SWITCH iPed				
						CASE 0			
							model = 		S_M_Y_MARINE_03
							vCoords = 		<<-2076.8970, 3256.6179, 31.8100>>
							fHeading = 		275.398
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 1			
							model = 		S_M_Y_MARINE_03
							vCoords = 		<<-2070.4690, 3245.7649, 31.8100>>
							fHeading = 		290.797
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_MEDIC_TEND_TO_DEAD"
						BREAK			
						CASE 2			
							model = 		S_M_Y_MARINE_03
							vCoords = 		<<-2084.7290, 3244.8440, 31.8100>>
							fHeading = 		147.997
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 3			
							model = 		S_M_Y_MARINE_03
							vCoords = 		<<-2087.1221, 3246.2090, 31.8100>>
							fHeading = 		147.997
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 4			
							model = 		S_M_Y_MARINE_03
							vCoords = 		<<-2078.3442, 3264.6956, 31.8104>>
							fHeading = 		60.9969
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_PUSH_UPS"
						BREAK			
						CASE 5			
							model = 		S_M_Y_MARINE_03
							vCoords = 		<<-2079.3242, 3263.2312, 31.8104>>
							fHeading = 		58.3957
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_PUSH_UPS"
						BREAK			
						CASE 6			
							model = 		S_M_Y_MARINE_03
							vCoords = 		<<-2060.3279, 3274.0229, 38.9110>>
							fHeading = 		327.396
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 7			
							model = 		S_M_Y_MARINE_03
							vCoords = 		<<-2082.7180, 3252.7019, 31.8100>>
							fHeading = 		147.195
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 8			
							model = 		S_M_Y_MARINE_03
							vCoords = 		<<-2079.1489, 3252.5730, 31.8100>>
							fHeading = 		198.194
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE_UPRIGHT"
						BREAK			
						CASE 9			
							model = 		S_M_Y_MARINE_03
							vCoords = 		<<-2078.6340, 3251.0840, 31.8100>>
							fHeading = 		15.794
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_BUGSTAR_OUTFITS_1
			SWITCH eSubvariation
				CASE CHS_BO1_VARIATION_1
					SWITCH iPed				
						CASE 0			
							model = 		S_M_Y_PestCont_01
							vCoords = 		<<944.6670, -669.9350, 57.0160>>
							fHeading = 		298.998
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 1			
							model = 		S_M_Y_PestCont_01
							vCoords = 		<<942.2180, -674.4780, 57.0160>>
							fHeading = 		352.599
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 2			
							model = 		S_M_Y_PestCont_01
							vCoords = 		<<963.6160, -675.8050, 57.0160>>
							fHeading = 		48.599
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_GARDENER_LEAF_BLOWER"
						BREAK			
						CASE 3			
							model = 		S_M_Y_PestCont_01
							vCoords = 		<<946.3260, -681.8640, 57.0160>>
							fHeading = 		316.599
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"CODE_HUMAN_MEDIC_TIME_OF_DEATH"
						BREAK			
						CASE 4			
							model = 		S_M_M_HighSec_01
							vCoords = 		<<960.5220, -669.5850, 57.4550>>
							fHeading = 		302.199
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_PATROL_1H"
						BREAK			
						CASE 5			
							model = 		S_M_M_HighSec_01
							vCoords = 		<<942.3380, -673.2760, 57.0160>>
							fHeading = 		171.398
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_STAND_IMPATIENT_UPRIGHT"
						BREAK			
						CASE 6			
							model = 		S_M_M_HighSec_01
							vCoords = 		<<948.9380, -684.4430, 57.0160>>
							fHeading = 		296.197
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_PATROL_1H"
						BREAK			
						CASE 7			
							model = 		S_M_M_HighSec_01
							vCoords = 		<<950.5070, -664.6510, 57.0160>>
							fHeading = 		209.996
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 8			
							model = 		S_M_M_HighSec_01
							vCoords = 		<<944.9480, -678.0390, 57.4550>>
							fHeading = 		128.996
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_GUARD_STAND"
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_BO1_VARIATION_2
					SWITCH iPed				
						CASE 0			
							model = 		S_M_Y_PestCont_01
							vCoords = 		<<-2982.6641, 722.8560, 27.4970>>
							fHeading = 		64.599
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 1			
							model = 		S_M_Y_PestCont_01
							vCoords = 		<<-3005.1809, 726.3640, 27.4970>>
							fHeading = 		306.198
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"CODE_HUMAN_MEDIC_TEND_TO_DEAD"
						BREAK			
						CASE 2			
							model = 		S_M_Y_PestCont_01
							vCoords = 		<<-2976.4670, 711.1790, 27.2470>>
							fHeading = 		133.398
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_GARDENER_LEAF_BLOWER"
						BREAK			
						CASE 3			
							model = 		S_M_Y_PestCont_01
							vCoords = 		<<-2985.4619, 730.3030, 27.4970>>
							fHeading = 		148.598
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 4			
							model = 		S_M_M_HighSec_01
							vCoords = 		<<-2983.9871, 723.5010, 27.4970>>
							fHeading = 		247.997
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_STAND_IMPATIENT_UPRIGHT"
						BREAK			
						CASE 5			
							model = 		S_M_M_HighSec_01
							vCoords = 		<<-2991.0100, 729.5180, 27.4970>>
							fHeading = 		300.197
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_GUARD_STAND"
						BREAK			
						CASE 6			
							model = 		S_M_M_HighSec_01
							vCoords = 		<<-2969.0710, 721.2810, 28.3230>>
							fHeading = 		116.996
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 7			
							model = 		S_M_M_HighSec_01
							vCoords = 		<<-2969.5710, 727.2470, 28.4440>>
							fHeading = 		85.196
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_PATROL_1H"
						BREAK			
						CASE 8			
							model = 		S_M_M_HighSec_01
							vCoords = 		<<-2991.0110, 706.5260, 27.4970>>
							fHeading = 		115.797
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_PATROL_1H"
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_BO1_VARIATION_3
					SWITCH iPed				
						CASE 0			
							model = 		S_M_Y_PestCont_01
							vCoords = 		<<-1500.7939, 422.5480, 110.1060>>
							fHeading = 		225.4
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 1			
							model = 		S_M_Y_PestCont_01
							vCoords = 		<<-1485.8300, 450.6400, 111.4700>>
							fHeading = 		289.199
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_GARDENER_LEAF_BLOWER"
						BREAK			
						CASE 2			
							model = 		S_M_Y_PestCont_01
							vCoords = 		<<-1495.2090, 438.6640, 111.4980>>
							fHeading = 		230.199
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"CODE_HUMAN_MEDIC_TEND_TO_DEAD"
						BREAK			
						CASE 3			
							model = 		S_M_Y_PestCont_01
							vCoords = 		<<-1492.0160, 415.6160, 110.1080>>
							fHeading = 		103.399
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 4			
							model = 		S_M_M_HighSec_01
							vCoords = 		<<-1493.3920, 415.3430, 110.1080>>
							fHeading = 		285.197
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK			
						CASE 5			
							model = 		S_M_M_HighSec_01
							vCoords = 		<<-1493.9850, 411.1490, 110.1080>>
							fHeading = 		234.198
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_PATROL_1H"
						BREAK			
						CASE 6			
							model = 		S_M_M_HighSec_01
							vCoords = 		<<-1506.7540, 435.2160, 110.1070>>
							fHeading = 		139.398
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_PATROL_1H"
						BREAK			
						CASE 7			
							model = 		S_M_M_HighSec_01
							vCoords = 		<<-1493.0990, 421.5290, 110.2440>>
							fHeading = 		143.398
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_GUARD_STAND"
						BREAK			
						CASE 8			
							model = 		S_M_M_HighSec_01
							vCoords = 		<<-1479.2750, 435.1350, 111.3980>>
							fHeading = 		228.199
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_GUARD_STAND"
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_FIREFIGHTER_OUTFIT_1
			SWITCH eSubvariation
				CASE CHS_FF1_VARIATION_1
					SWITCH iPed
						CASE 0			
							model = 		S_M_Y_Fireman_01
							vCoords = 		<<205.3305, -1652.2616, 28.8032>>
							fHeading = 		23.5946
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_COP_IDLES"
						BREAK			
						CASE 1			
							model = 		S_M_Y_Fireman_01
							vCoords = 		<<204.6392, -1651.2551, 28.8032>>
							fHeading = 		206.5944
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK
						CASE 2			
							model = 		S_M_Y_Fireman_01
							vCoords = 		<<-1080.6791, -2376.0156, 12.9452>>
							fHeading = 		293.1993
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK			
						CASE 3			
							model = 		S_M_Y_Fireman_01
							vCoords = 		<<-1079.6277, -2375.4260, 12.9452>>
							fHeading = 		117.3987
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK		
						CASE 4			
							model = 		S_M_Y_Fireman_01
							vCoords = 		<<1703.8722, 3587.0991, 34.5206>>
							fHeading = 		206.9971
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK			
						CASE 5			
							model = 		S_M_Y_Fireman_01
							vCoords = 		<<1695.0897, 3586.8389, 34.6209>>
							fHeading = 		293.9968
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
					ENDSWITCH
				BREAK
				
				CASE CHS_FF1_VARIATION_2
					SWITCH iPed
						CASE 0			
							model = 		S_M_Y_Fireman_01
							vCoords = 		<<1197.4889, -1462.8149, 33.8427>>
							fHeading = 		353.1953
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK			
						CASE 1			
							model = 		S_M_Y_Fireman_01
							vCoords = 		<<1206.5375, -1478.9703, 33.8595>>
							fHeading = 		275.5949
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK	
						CASE 2			
							model = 		S_M_Y_Fireman_01
							vCoords = 		<<-634.9459, -106.9902, 37.0336>>
							fHeading = 		317.5961
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 3			
							model = 		S_M_Y_Fireman_01
							vCoords = 		<<-633.1518, -101.0813, 37.0472>>
							fHeading = 		86.5957
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK	
						CASE 4			
							model = 		S_M_Y_Fireman_01
							vCoords = 		<<-2103.8438, 2832.7712, 31.8095>>
							fHeading = 		54.5965
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 5			
							model = 		S_M_Y_Fireman_01
							vCoords = 		<<-2104.9333, 2833.5608, 31.8094>>
							fHeading = 		230.7962
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_COP_IDLES"
						BREAK			
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_MAINTENANCE_OUTFITS_1
			SWITCH eSubvariation
				CASE CHS_MO1_VARIATION_1
					SWITCH iPed				
						CASE 0			
							model = 		S_M_M_DockWork_01
							vCoords = 		<<-1592.7952, -3238.3333, 12.9449>>
							fHeading = 		272.5986
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 1			
							model = 		S_M_M_DockWork_01
							vCoords = 		<<-1564.8055, -3229.7329, 25.3362>>
							fHeading = 		217.3975
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_COP_IDLES"
						BREAK			
						CASE 2			
							model = 		S_M_M_LatHandy_01
							vCoords = 		<<-1564.0400, -3230.8018, 25.3362>>
							fHeading = 		43.5973
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 3			
							model = 		S_M_M_LatHandy_01
							vCoords = 		<<-1558.5074, -3233.5315, 25.3362>>
							fHeading = 		43.5973
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"CODE_HUMAN_MEDIC_TEND_TO_DEAD"
						BREAK			
						CASE 4			
							model = 		S_M_M_LatHandy_01
							vCoords = 		<<-1562.5836, -3237.9731, 25.3362>>
							fHeading = 		168.5969
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_WINDOW_SHOP_BROWSE"
						BREAK			
						CASE 5			
							model = 		S_M_M_LatHandy_01
							vCoords = 		<<-1562.9108, -3238.6521, 28.6341>>
							fHeading = 		345.3965
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_WELDING"
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_MO1_VARIATION_2
					SWITCH iPed				
						CASE 0			
							model = 		S_M_M_DockWork_01
							vCoords = 		<<-1152.3931, -225.7751, 36.8990>>
							fHeading = 		133.199
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 1			
							model = 		S_M_M_LatHandy_01
							vCoords = 		<<-1159.7360, -219.7990, 36.9600>>
							fHeading = 		235.999
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 2			
							model = 		S_M_M_LatHandy_01
							vCoords = 		<<-1172.4850, -237.9640, 36.9350>>
							fHeading = 		182.999
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"CODE_HUMAN_MEDIC_TEND_TO_DEAD"
						BREAK			
						CASE 3			
							model = 		S_M_M_LatHandy_01
							vCoords = 		<<-1165.9950, -230.6110, 36.9510>>
							fHeading = 		297.199
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 4			
							model = 		S_M_M_DockWork_01
							vCoords = 		<<-1164.7865, -230.0372, 36.9530>>
							fHeading = 		117.398
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_COP_IDLES"
						BREAK			
						CASE 5			
							model = 		S_M_M_LatHandy_01
							vCoords = 		<<-1159.5393, -206.0792, 36.9600>>
							fHeading = 		17.7986
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_WELDING"
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_MO1_VARIATION_3
					SWITCH iPed				
						CASE 0			
							model = 		S_M_M_DockWork_01
							vCoords = 		<<-2169.1689, 4282.4331, 48.0030>>
							fHeading = 		158.4
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 1			
							model = 		S_M_M_DockWork_01
							vCoords = 		<<-2173.0730, 4288.3281, 48.0690>>
							fHeading = 		6.599
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_COP_IDLES"
						BREAK			
						CASE 2			
							model = 		S_M_M_LatHandy_01
							vCoords = 		<<-2173.2351, 4289.4951, 48.0520>>
							fHeading = 		188.199
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 3			
							model = 		S_M_M_LatHandy_01
							vCoords = 		<<-2185.6440, 4290.8750, 52.8150>>
							fHeading = 		146.199
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_WELDING"
						BREAK			
						CASE 4			
							model = 		S_M_M_LatHandy_01
							vCoords = 		<<-2175.4839, 4294.7920, 48.0600>>
							fHeading = 		236.199
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK			
						CASE 5			
							model = 		S_M_M_LatHandy_01
							vCoords = 		<<-2173.8535, 4282.1338, 48.1209>>
							fHeading = 		240.399
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_GRUPPE_SECHS_VAN_1
			SWITCH eSubvariation
				CASE CHS_GS1_VARIATION_1
					SWITCH iPed				
						CASE 0			
							model = 		S_M_Y_XMech_01
							vCoords = 		<<480.0412, -1280.4307, 28.5392>>
							fHeading = 		55.3967
							weapon = 		WEAPONTYPE_SAWNOFFSHOTGUN
							sScenario = 	""
						BREAK			
						CASE 1			
							model = 		S_M_Y_XMech_01
							vCoords = 		<<483.1170, -1278.3770, 28.5580>>
							fHeading = 		153.798
							weapon = 		WEAPONTYPE_SAWNOFFSHOTGUN
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 2			
							model = 		S_M_Y_XMech_02
							vCoords = 		<<477.1390, -1285.3450, 28.5390>>
							fHeading = 		178.797
							weapon = 		WEAPONTYPE_SAWNOFFSHOTGUN
							sScenario = 		"CODE_HUMAN_MEDIC_TEND_TO_DEAD"
						BREAK			
						CASE 3			
							model = 		S_M_Y_XMech_02
							vCoords = 		<<478.2189, -1276.8514, 28.5513>>
							fHeading = 		314.7967
							weapon = 		WEAPONTYPE_CROWBAR
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 4			
							model = 		S_M_Y_XMech_02
							vCoords = 		<<479.1009, -1275.9814, 28.5672>>
							fHeading = 		133.7969
							weapon = 		WEAPONTYPE_DLC_WRENCH
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 5			
							model = 		S_M_Y_XMech_02
							vCoords = 		<<486.2520, -1290.9530, 28.5470>>
							fHeading = 		265.396
							weapon = 		WEAPONTYPE_DLC_WRENCH
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
					ENDSWITCH
				BREAK
				CASE CHS_GS1_VARIATION_2
					SWITCH iPed				
						CASE 0			
							model = 		S_M_Y_XMech_01
							vCoords = 		<<253.8494, 2577.9055, 44.3306>>
							fHeading = 		93.3725
							weapon = 		WEAPONTYPE_SAWNOFFSHOTGUN
							sScenario = 	""
						BREAK			
						CASE 1			
							model = 		S_M_Y_XMech_01
							vCoords = 		<<255.0070, 2582.9199, 44.0840>>
							fHeading = 		307.573
							weapon = 		WEAPONTYPE_SAWNOFFSHOTGUN
							sScenario = 		"CODE_HUMAN_MEDIC_TEND_TO_DEAD"
						BREAK			
						CASE 2			
							model = 		S_M_Y_XMech_02
							vCoords = 		<<259.9900, 2575.7729, 44.1230>>
							fHeading = 		41.572
							weapon = 		WEAPONTYPE_SAWNOFFSHOTGUN
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 3			
							model = 		S_M_Y_XMech_02
							vCoords = 		<<254.2800, 2587.0071, 44.0060>>
							fHeading = 		138.772
							weapon = 		WEAPONTYPE_DLC_WRENCH
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 4			
							model = 		S_M_Y_XMech_02
							vCoords = 		<<253.4550, 2585.8811, 44.0830>>
							fHeading = 		318.172
							weapon = 		WEAPONTYPE_DLC_WRENCH
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 5			
							model = 		S_M_Y_XMech_02
							vCoords = 		<<266.5280, 2583.7849, 43.9010>>
							fHeading = 		96.171
							weapon = 		WEAPONTYPE_DLC_WRENCH
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
					ENDSWITCH
				BREAK
				CASE CHS_GS1_VARIATION_3
					SWITCH iPed				
						CASE 0			
							model = 		S_M_Y_XMech_01
							vCoords = 		<<126.7347, 6624.9751, 30.7936>>
							fHeading = 		130.1974
							weapon = 		WEAPONTYPE_SAWNOFFSHOTGUN
							sScenario = 	""
						BREAK			
						CASE 1			
							model = 		S_M_Y_XMech_01
							vCoords = 		<<132.3850, 6626.9551, 30.6900>>
							fHeading = 		59.398
							weapon = 		WEAPONTYPE_SAWNOFFSHOTGUN
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 2			
							model = 		S_M_Y_XMech_02
							vCoords = 		<<133.3490, 6631.8101, 30.6910>>
							fHeading = 		307.597
							weapon = 		WEAPONTYPE_SAWNOFFSHOTGUN
							sScenario = 		"CODE_HUMAN_MEDIC_TEND_TO_DEAD"
						BREAK			
						CASE 3			
							model = 		S_M_Y_XMech_02
							vCoords = 		<<131.9940, 6638.1519, 30.7920>>
							fHeading = 		224.797
							weapon = 		WEAPONTYPE_DLC_WRENCH
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 4			
							model = 		S_M_Y_XMech_02
							vCoords = 		<<121.8380, 6626.9751, 30.9380>>
							fHeading = 		186.397
							weapon = 		WEAPONTYPE_DLC_WRENCH
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 5			
							model = 		S_M_Y_XMech_02
							vCoords = 		<<122.0170, 6625.8442, 30.9440>>
							fHeading = 		2.797
							weapon = 		WEAPONTYPE_DLC_WRENCH
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_UNMARKED_WEAPONS
			SWITCH eSubvariation
				CASE CHS_UW_ELYSIAN_ISLAND
					SWITCH iPed				
						CASE 0			
							model = 		G_M_M_Korboss_01
							vCoords = 		<<470.5960, -2933.0740, 7.2800>>
							fHeading = 		239.596
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 1			
							model = 		G_M_M_Korboss_01
							vCoords = 		<<470.0850, -2933.6169, 7.2510>>
							fHeading = 		239.596
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 2			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<506.7560, -2933.1250, 5.0440>>
							fHeading = 		216.396
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 3			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<507.6710, -2934.3140, 5.0440>>
							fHeading = 		38.796
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_COP_IDLES"
						BREAK			
						CASE 4			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<471.7960, -2932.7280, 5.0440>>
							fHeading = 		103.596
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 5			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<490.2700, -2912.6689, 5.7360>>
							fHeading = 		89.195
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_MEDIC_TEND_TO_DEAD"
						BREAK			
						CASE 6			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<528.5820, -2902.0701, 5.7360>>
							fHeading = 		267.795
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_PATROL_1H"
						BREAK			
						CASE 7			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<460.6630, -2903.8711, 5.7360>>
							fHeading = 		-0.805
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_PATROL_1H"
						BREAK			
						CASE 8			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<508.6930, -2921.2361, 5.7360>>
							fHeading = 		176.794
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_PATROL_1H"
						BREAK			
						CASE 9			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<460.3300, -2923.5559, 5.7360>>
							fHeading = 		269.195
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_PATROL_1H"
						BREAK			
						CASE 10			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<433.7122, -2919.3020, 8.4623>>
							fHeading = 		198.594
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_MEDIC_TEND_TO_DEAD"
						BREAK			
						CASE 11			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<478.5650, -2901.0439, 5.7360>>
							fHeading = 		11.394
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_BINOCULARS"
						BREAK			
						CASE 12			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<481.9496, -2938.0271, 5.0488>>
							fHeading = 		333.3957
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 13			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<484.4362, -2939.1479, 5.0445>>
							fHeading = 		356.3954
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 14			
							model = 		G_M_Y_Korean_01
							vCoords = 		<<483.1351, -2935.3936, 5.0445>>
							fHeading = 		151.1952
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 15			
							model = 		G_M_Y_Korean_01
							vCoords = 		<<484.7707, -2936.1990, 5.0445>>
							fHeading = 		163.9948
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_UW_GRAPESEED
					SWITCH iPed				
						CASE 0			
							model = 		G_M_Y_Lost_01
							vCoords = 		<<2302.7261, 4883.9780, 43.0310>>
							fHeading = 		0
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 1			
							model = 		G_M_Y_Lost_01
							vCoords = 		<<2302.2241, 4884.8281, 43.0340>>
							fHeading = 		0
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 2			
							model = 		S_M_Y_Sheriff_01
							vCoords = 		<<2303.3159, 4886.6870, 40.8080>>
							fHeading = 		155.999
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 3			
							model = 		S_M_Y_Sheriff_01
							vCoords = 		<<2311.7290, 4893.4248, 40.8080>>
							fHeading = 		169.999
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 4			
							model = 		S_M_Y_Sheriff_01
							vCoords = 		<<2311.5730, 4892.0059, 40.8080>>
							fHeading = 		350.999
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_COP_IDLES"
						BREAK			
						CASE 5			
							model = 		S_M_Y_Sheriff_01
							vCoords = 		<<2338.8989, 4862.7871, 40.8082>>
							fHeading = 		221.998
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_GUARD_STAND"
						BREAK			
						CASE 6			
							model = 		S_M_Y_Sheriff_01
							vCoords = 		<<2297.5601, 4868.0469, 40.8080>>
							fHeading = 		134.598
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_MEDIC_TEND_TO_DEAD"
						BREAK			
						CASE 7			
							model = 		S_M_Y_Sheriff_01
							vCoords = 		<<2348.3911, 4877.5078, 40.8170>>
							fHeading = 		92.598
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 8			
							model = 		S_M_Y_Sheriff_01
							vCoords = 		<<2328.7749, 4895.9492, 40.8130>>
							fHeading = 		44.398
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_PATROL_1H"
						BREAK			
						CASE 9			
							model = 		S_M_Y_Sheriff_01
							vCoords = 		<<2349.9990, 4868.7100, 40.8080>>
							fHeading = 		315.997
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_PATROL_1H"
						BREAK			
						CASE 10			
							model = 		S_M_Y_Sheriff_01
							vCoords = 		<<2306.0481, 4864.0288, 40.8080>>
							fHeading = 		40.197
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_PATROL_1H"
						BREAK			
						CASE 11			
							model = 		S_M_Y_Sheriff_01
							vCoords = 		<<2318.0488, 4840.4585, 40.8082>>
							fHeading = 		151.7968
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_MEDIC_TIME_OF_DEATH"
						BREAK
						CASE 12			
							model = 		S_M_Y_Sheriff_01
							vCoords = 		<<2293.0269, 4891.7427, 40.2081>>
							fHeading = 		84.3993
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 13		
							model = 		S_M_Y_Sheriff_01
							vCoords = 		<<2293.2021, 4893.6528, 40.1593>>
							fHeading = 		75.3992
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK
						CASE 14			
							model = 		G_M_Y_Lost_01
							vCoords = 		<<2290.6858, 4891.9409, 40.1231>>
							fHeading = 		273.3993
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 15		
							model = 		G_M_Y_Lost_01
							vCoords = 		<<2290.7695, 4894.1714, 40.0624>>
							fHeading = 		243.7993
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK							
					ENDSWITCH				
				BREAK
				CASE CHS_UW_STONER
					SWITCH iPed				
						CASE 0			
							model = 		S_M_Y_Construct_01
							vCoords = 		<<312.8100, 2839.0959, 44.6680>>
							fHeading = 		-66.201
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 1			
							model = 		S_M_Y_Construct_01
							vCoords = 		<<312.8100, 2839.0959, 44.6680>>
							fHeading = 		-66.201
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 2			
							model = 		S_M_Y_Sheriff_01
							vCoords = 		<<311.9200, 2838.6030, 42.4360>>
							fHeading = 		296.799
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 3			
							model = 		S_M_Y_Sheriff_01
							vCoords = 		<<301.7780, 2833.3359, 42.4470>>
							fHeading = 		347.399
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 4			
							model = 		S_M_Y_Sheriff_01
							vCoords = 		<<301.9490, 2834.7300, 42.4520>>
							fHeading = 		167.998
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_COP_IDLES"
						BREAK			
						CASE 5			
							model = 		S_M_Y_Sheriff_01
							vCoords = 		<<284.5100, 2839.1609, 42.6420>>
							fHeading = 		314.198
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_MEDIC_TIME_OF_DEATH"
						BREAK			
						CASE 6			
							model = 		S_M_Y_Sheriff_01
							vCoords = 		<<266.3612, 2852.6606, 42.6197>>
							fHeading = 		114.5969
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_PATROL_1H"
						BREAK			
						CASE 7			
							model = 		S_M_Y_Sheriff_01
							vCoords = 		<<298.3113, 2893.5190, 42.6075>>
							fHeading = 		298.1968
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_PATROL_1H"
						BREAK			
						CASE 8			
							model = 		S_M_Y_Sheriff_01
							vCoords = 		<<300.2500, 2896.4041, 42.6070>>
							fHeading = 		338.397
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_MEDIC_TIME_OF_DEATH"
						BREAK			
						CASE 9			
							model = 		S_M_Y_Sheriff_01
							vCoords = 		<<306.4100, 2883.5779, 42.4630>>
							fHeading = 		36.196
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_PATROL_1H"
						BREAK			
						CASE 10			
							model = 		S_M_Y_Sheriff_01
							vCoords = 		<<294.5170, 2865.8411, 42.6420>>
							fHeading = 		231.796
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 11			
							model = 		S_M_Y_Sheriff_01
							vCoords = 		<<347.5510, 2857.1621, 42.4350>>
							fHeading = 		298.996
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_PATROL_1H"
						BREAK
						CASE 12		
							model = 		S_M_Y_Sheriff_01
							vCoords = 		<<290.1660, 2834.2012, 42.4414>>
							fHeading = 		323.1979
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 13			
							model = 		S_M_Y_Sheriff_01
							vCoords = 		<<292.4416, 2833.1372, 42.4361>>
							fHeading = 		327.3979
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK
						CASE 14		
							model = 		S_M_Y_Construct_01
							vCoords = 		<<291.3243, 2836.2546, 42.4790>>
							fHeading = 		169.9987
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 15		
							model = 		S_M_Y_Construct_01
							vCoords = 		<<293.5510, 2835.4636, 42.4527>>
							fHeading = 		145.9985
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK						
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_STEAL_EMP
			SWITCH eSubvariation
				CASE CHS_EMP_ULSA1
					SWITCH iPed				
						CASE 0			
							model = 		S_M_M_Security_01
							vCoords = 		<<-1754.0330, 210.1220, 63.3770>>
							fHeading = 		114.999
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 1			
							model = 		S_M_M_Security_01
							vCoords = 		<<-1737.6270, 219.0640, 63.4490>>
							fHeading = 		123.999
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_GUARD_STAND"
						BREAK			
						CASE 2			
							model = 		S_M_M_Security_01
							vCoords = 		<<-1764.6290, 207.5720, 63.3720>>
							fHeading = 		226.599
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 3			
							model = 		S_M_Y_Grip_01
							vCoords = 		<<-1755.0300, 209.5020, 63.3810>>
							fHeading = 		292.398
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 4			
							model = 		S_M_M_Scientist_01
							vCoords = 		<<-1750.0861, 209.4840, 63.3920>>
							fHeading = 		29.798
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_MOBILE_FILM_SHOCKING"
						BREAK			
						CASE 5			
							model = 		S_M_M_Scientist_01
							vCoords = 		<<-1752.5760, 215.4720, 63.3740>>
							fHeading = 		254.398
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"CODE_HUMAN_MEDIC_TIME_OF_DEATH"
						BREAK
						CASE 6			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("MP_M_SecuroGuard_01"))
							vCoords = 		<<-1054.6957, -2347.3384, 19.5290>>
							fHeading = 		318
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK			
						CASE 7			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("MP_M_SecuroGuard_01"))
							vCoords = 		<<-1058.1057, -2325.2705, 12.9446>>
							fHeading = 		147.3986
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 8			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("MP_M_SecuroGuard_01"))
							vCoords = 		<<-1058.8545, -2326.4617, 12.9446>>
							fHeading = 		325.9988
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 9			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("MP_M_SecuroGuard_01"))
							vCoords = 		<<-1034.4648, -2351.0876, 19.5264>>
							fHeading = 		240.9985
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 10			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("MP_M_SecuroGuard_01"))
							vCoords = 		<<-1065.3988, -2342.9866, 19.5290>>
							fHeading = 		82.7997
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 11			
							model = 		S_M_M_Pilot_02
							vCoords = 		<<-1066.5850, -2342.8027, 19.5290>>
							fHeading = 		257.7981
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 12			
							model = 		S_M_M_Pilot_02
							vCoords = 		<<-1062.8085, -2335.9910, 19.5295>>
							fHeading = 		149.1979
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_WELDING"
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_EMP_ULSA2
					SWITCH iPed				
						CASE 0			
							model = 		S_M_M_Security_01
							vCoords = 		<<-1697.1230, 151.3260, 63.3710>>
							fHeading = 		35.599
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 1			
							model = 		S_M_M_Security_01
							vCoords = 		<<-1688.4091, 158.8320, 62.9580>>
							fHeading = 		215.198
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_GUARD_STAND"
						BREAK			
						CASE 2			
							model = 		S_M_Y_Grip_01
							vCoords = 		<<-1697.9060, 152.3360, 63.3710>>
							fHeading = 		210.998
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 3			
							model = 		S_M_M_Security_01
							vCoords = 		<<-1695.3730, 132.6060, 63.3410>>
							fHeading = 		35.599
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 4			
							model = 		S_M_M_Scientist_01
							vCoords = 		<<-1696.6500, 147.9520, 63.3710>>
							fHeading = 		300.599
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_MOBILE_FILM_SHOCKING"
						BREAK			
						CASE 5			
							model = 		S_M_M_Scientist_01
							vCoords = 		<<-1691.6530, 153.1970, 63.1830>>
							fHeading = 		68.198
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"CODE_HUMAN_MEDIC_TIME_OF_DEATH"
						BREAK	
						CASE 6			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("MP_M_SecuroGuard_01"))
							vCoords = 		<<-1199.2786, -2367.1069, 12.9452>>
							fHeading = 		303.5986
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 7			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("MP_M_SecuroGuard_01"))
							vCoords = 		<<-1198.2581, -2366.4153, 12.9451>>
							fHeading = 		124.1983
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 8			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("MP_M_SecuroGuard_01"))
							vCoords = 		<<-1216.7095, -2369.0554, 14.2447>>
							fHeading = 		242.7982
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_GUARD_STAND"
						BREAK			
						CASE 9			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("MP_M_SecuroGuard_01"))
							vCoords = 		<<-1207.5491, -2363.7703, 12.9452>>
							fHeading = 		242.7982
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 10			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("MP_M_SecuroGuard_01"))
							vCoords = 		<<-1202.7948, -2356.9910, 12.9451>>
							fHeading = 		31.7979
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 11			
							model = 		S_M_M_Pilot_02
							vCoords = 		<<-1203.5759, -2355.9250, 12.9452>>
							fHeading = 		211.1978
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 12			
							model = 		S_M_M_Pilot_02
							vCoords = 		<<-1208.0867, -2359.5781, 12.9452>>
							fHeading = 		330.1975
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_WELDING"
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_EMP_ULSA3
					SWITCH iPed				
						CASE 0			
							model = 		S_M_Y_Grip_01
							vCoords = 		<<-1614.5389, 230.1710, 58.8700>>
							fHeading = 		232.625
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 1			
							model = 		S_M_M_Security_01
							vCoords = 		<<-1613.4270, 229.5640, 58.8240>>
							fHeading = 		59.224
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 2			
							model = 		S_M_M_Security_01
							vCoords = 		<<-1598.4480, 223.8650, 58.0980>>
							fHeading = 		25.024
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 3			
							model = 		S_M_M_Security_01
							vCoords = 		<<-1627.4900, 240.9820, 58.7230>>
							fHeading = 		293.024
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_GUARD_STAND"
						BREAK			
						CASE 4			
							model = 		S_M_M_Scientist_01
							vCoords = 		<<-1609.8010, 229.7900, 58.6970>>
							fHeading = 		323.824
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_MOBILE_FILM_SHOCKING"
						BREAK			
						CASE 5			
							model = 		S_M_M_Scientist_01
							vCoords = 		<<-1608.4871, 227.3750, 58.5100>>
							fHeading = 		173.223
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"CODE_HUMAN_MEDIC_TIME_OF_DEATH"
						BREAK		
						CASE 6			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("MP_M_SecuroGuard_01"))
							vCoords = 		<<-1156.2150, -2429.2253, 12.9451>>
							fHeading = 		49.9998
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 7			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("MP_M_SecuroGuard_01"))
							vCoords = 		<<-1157.3236, -2428.3608, 12.9451>>
							fHeading = 		230.5995
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 8			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("MP_M_SecuroGuard_01"))
							vCoords = 		<<-1156.8135, -2440.2073, 19.2669>>
							fHeading = 		330.7992
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_GUARD_STAND"
						BREAK			
						CASE 9			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("MP_M_SecuroGuard_01"))
							vCoords = 		<<-1138.0103, -2432.1440, 12.9451>>
							fHeading = 		4.9989
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 10			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("MP_M_SecuroGuard_01"))
							vCoords = 		<<-1138.7987, -2439.1128, 12.9452>>
							fHeading = 		244.9985
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 11			
							model = 		S_M_M_Pilot_02
							vCoords = 		<<-1138.2343, -2430.8938, 12.9452>>
							fHeading = 		189.1985
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 12			
							model = 		S_M_M_Pilot_02
							vCoords = 		<<-1143.1339, -2432.7937, 12.9451>>
							fHeading = 		322.3983
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_WELDING"
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_CELEB_DISPOSE_OF_CAR
			SWITCH iPed				
				CASE 0			
					model = 		A_M_Y_ACULT_02
					vCoords = 		<<-1042.4885, 4906.5640, 207.5494>>
					fHeading = 		342.9982
					weapon = 		WEAPONTYPE_SAWNOFFSHOTGUN
					sScenario = 		""
				BREAK			
				CASE 1			
					model = 		A_M_O_ACULT_02
					vCoords = 		<<-1043.5717, 4906.4341, 207.7991>>
					fHeading = 		336.3976
					weapon = 		WEAPONTYPE_ASSAULTRIFLE
					sScenario = 		""
				BREAK			
				CASE 2			
					model = 		A_M_O_ACULT_02
					vCoords = 		<<-1107.2253, 4924.3779, 216.3243>>
					fHeading = 		197.9967
					weapon = 		WEAPONTYPE_SAWNOFFSHOTGUN
					sScenario = 		""
				BREAK			
				CASE 3			
					model = 		A_M_Y_ACULT_02
					vCoords = 		<<-1045.0543, 4913.9565, 207.4532>>
					fHeading = 		272.1967
					weapon = 		WEAPONTYPE_SAWNOFFSHOTGUN
					sScenario = 		""
				BREAK			
				CASE 4			
					model = 		A_M_Y_ACULT_02
					vCoords = 		<<-1100.568, 4931.567, 217.45 >>
					fHeading = 		-107.8
					weapon = 		WEAPONTYPE_PISTOL
					sScenario = 		"amb@world_human_seat_wall@male@hands_by_sides@base"
				BREAK			
				CASE 5			
					model = 		A_M_O_ACULT_02
					vCoords = 		<<-1045.9788, 4912.6792, 207.7690>>
					fHeading = 		256.7957
					weapon = 		WEAPONTYPE_ASSAULTRIFLE
					sScenario = 		""
				BREAK			
				CASE 6			
					model = 		A_M_Y_ACULT_02
					vCoords = 		<<-1063.1794, 4902.1147, 211.0420>>
					fHeading = 		317.3952
					weapon = 		WEAPONTYPE_PISTOL
					sScenario = 		"amb@medic@standing@kneel@idle_a"
				BREAK			
				CASE 7			
					model = 		A_F_Y_Hiker_01
					vCoords = 		<<-1062.7526, 4902.7798, 210.9196>>
					fHeading = 		245.1945
					weapon = 		WEAPONTYPE_UNARMED
					sScenario = 		""
				BREAK			
				CASE 8			
					model = 		A_M_Y_ACULT_02
					vCoords = 		<<-1091.7428, 4906.9590, 213.7626>>
					fHeading = 		106.5944
					weapon = 		WEAPONTYPE_PISTOL
					sScenario = 		"melee@unarmed@streamed_variations"
				BREAK			
				CASE 9			
					model = 		A_F_Y_Hiker_01
					vCoords = 		<<-1092.7440, 4906.5215, 213.8819>>
					fHeading = 		205.5944
					weapon = 		WEAPONTYPE_UNARMED
					sScenario = 		""
				BREAK			
				CASE 10			
					model = 		A_M_Y_ACULT_01
					vCoords = 		<<-1134.4354, 4948.9385, 221.2737>>
					fHeading = 		257.1959
					weapon = 		WEAPONTYPE_DLC_GUSENBERG
					sScenario = 		"CODE_HUMAN_PATROL_2H"
				BREAK			
				CASE 11			
					model = 		A_M_O_ACULT_02
					vCoords = 		<<-1132.9801, 4944.3462, 221.2736>>
					fHeading = 		257.1959
					weapon = 		WEAPONTYPE_SAWNOFFSHOTGUN
					sScenario = 		"WORLD_HUMAN_DRINKING"
				BREAK			
				CASE 12			
					model = 		A_M_Y_ACULT_02
					vCoords = 		<<-1136.8912, 4933.9805, 221.2736>>
					fHeading = 		221.7955
					weapon = 		WEAPONTYPE_PISTOL
					sScenario = 		"WORLD_HUMAN_SMOKING"
				BREAK			
				CASE 13			
					model = 		A_M_O_ACULT_02
					vCoords = 		<<-1132.7002, 4945.4136, 221.2736>>
					fHeading = 		250.3957
					weapon = 		WEAPONTYPE_PISTOL
					sScenario = 		"WORLD_HUMAN_SMOKING"
				BREAK			
				CASE 14			
					model = 		A_M_Y_ACULT_02
					vCoords = 		<<-1130.6991, 4950.8906, 221.2735>>
					fHeading = 		250.3957
					weapon = 		WEAPONTYPE_PISTOL
					sScenario = 		"WORLD_HUMAN_DRINKING"
				BREAK			
				CASE 15			
					model = 		A_M_O_ACULT_02
					vCoords = 		<<-1124.8379, 4926.0435, 217.9715>>
					fHeading = 		52.3947
					weapon = 		WEAPONTYPE_PISTOL
					sScenario = 		"WORLD_HUMAN_SMOKING"
				BREAK			
				CASE 16			
					model = 		A_M_Y_ACULT_02
					vCoords = 		<<-1125.6849, 4926.8379, 218.0779>>
					fHeading = 		225.9943
					weapon = 		WEAPONTYPE_PISTOL
					sScenario = 		"WORLD_HUMAN_DRINKING"
				BREAK			
				CASE 17			
					model = 		A_M_O_ACULT_02
					vCoords = 		<<-1118.6499, 4937.3091, 217.4864>>
					fHeading = 		191.9234
					weapon = 		WEAPONTYPE_PISTOL
					sScenario = 		"amb@medic@standing@kneel@idle_a"
				BREAK			
				CASE 18			
					model = 		A_F_Y_Hiker_01
					vCoords = 		<<-1118.2412, 4936.3486, 217.4585>>
					fHeading = 		31.1935
					weapon = 		WEAPONTYPE_UNARMED
					sScenario = 		""
				BREAK			
				CASE 19			
					model = 		A_M_O_ACULT_02
					vCoords = 		<<-1124.4554, 4955.1265, 219.4982>>
					fHeading = 		185.993
					weapon = 		WEAPONTYPE_ASSAULTRIFLE
					sScenario = 		""
				BREAK			
			ENDSWITCH				
		BREAK
	ENDSWITCH
	SWITCH eVariation
		CASE CHV_SEWER_TUNNEL_DRILL_1
			SWITCH eSubvariation
				CASE CHS_STD1_VARIATION_1
					SWITCH iPed				
						CASE 0			
							model = 		S_M_Y_Construct_02
							vCoords = 		<<786.3642, 2287.9395, 47.7166>>
							fHeading = 		164.5994
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 1			
							model = 		S_M_Y_Construct_02
							vCoords = 		<<786.3642, 2287.9395, 47.7166>>
							fHeading = 		164.5994
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_STD1_VARIATION_2
					SWITCH iPed				
						CASE 0			
							model = 		S_M_Y_Construct_02
							vCoords = 		<<1216.5472, -3341.9543, 4.8014>>
							fHeading = 		88.5998
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 1			
							model = 		S_M_Y_Construct_02
							vCoords = 		<<1216.5472, -3341.9543, 4.8014>>
							fHeading = 		88.5998
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_STD1_VARIATION_3
					SWITCH iPed				
						CASE 0			
							model = 		S_M_Y_Construct_02
							vCoords = 		<<-2209.9294, 4252.1719, 46.3948>>
							fHeading = 		94.1999
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 1			
							model = 		S_M_Y_Construct_02
							vCoords = 		<<-2209.9294, 4252.1719, 46.3948>>
							fHeading = 		94.1999
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_SEWER_TUNNEL_DRILL_2
			SWITCH eSubvariation
				CASE CHS_STD2_VARIATION_1
					SWITCH iPed				
						CASE 0			
							model = 		S_M_Y_Construct_01
							vCoords = 		<<2016.2640, 4987.0469, 41.1030>>
							fHeading = 		140.6
							weapon = 		WEAPONTYPE_DLC_WRENCH
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 1			
							model = 		S_M_Y_Construct_02
							vCoords = 		<<2014.8688, 4994.1582, 40.1536>>
							fHeading = 		226.9989
							weapon = 		WEAPONTYPE_HAMMER
							sScenario = 		"CODE_HUMAN_MEDIC_TEND_TO_DEAD"
						BREAK			
						CASE 2			
							model = 		S_M_Y_Construct_01
							vCoords = 		<<2015.8031, 4972.6729, 40.4352>>
							fHeading = 		121.9989
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_WELDING"
						BREAK			
						CASE 3			
							model = 		S_M_Y_Construct_02
							vCoords = 		<<2007.0359, 4983.2334, 40.3592>>
							fHeading = 		3.3975
							weapon = 		WEAPONTYPE_DLC_WRENCH
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 4			
							model = 		S_M_Y_Construct_01
							vCoords = 		<<2021.2930, 4977.0059, 40.2440>>
							fHeading = 		88.2332
							weapon = 		WEAPONTYPE_HAMMER
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK			
						CASE 5			
							model = 		S_M_Y_Construct_02
							vCoords = 		<<2006.3419, 4975.6323, 40.5996>>
							fHeading = 		296.7967
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 6			
							model = 		S_M_Y_Construct_01
							vCoords = 		<<2008.0477, 4976.6030, 40.5530>>
							fHeading = 		117.1975
							weapon = 		WEAPONTYPE_DLC_WRENCH
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK
						CASE 7			
							model = 		S_M_M_Security_01
							vCoords = 		<<2021.2017, 4968.4717, 40.3321>>
							fHeading = 		133.9995
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_PATROL_1H"
						BREAK			
						CASE 8			
							model = 		S_M_M_Security_01
							vCoords = 		<<2037.7665, 4987.4194, 38.9767>>
							fHeading = 		219.9992
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK
					ENDSWITCH				
				BREAK
				CASE CHS_STD2_VARIATION_2
					SWITCH iPed				
						CASE 0			
							model = 		S_M_Y_Construct_01
							vCoords = 		<<2646.9768, 2801.3418, 33.0614>>
							fHeading = 		294.3929
							weapon = 		WEAPONTYPE_DLC_WRENCH
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 1			
							model = 		S_M_Y_Construct_02
							vCoords = 		<<2648.3745, 2802.0105, 33.1169>>
							fHeading = 		109.5919
							weapon = 		WEAPONTYPE_HAMMER
							sScenario = 		"WORLD_HUMAN_STAND_IMPATIENT"
						BREAK			
						CASE 2			
							model = 		S_M_Y_Construct_01
							vCoords = 		<<2674.8430, 2791.1450, 39.5940>>
							fHeading = 		84.992
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK			
						CASE 3			
							model = 		S_M_Y_Construct_01
							vCoords = 		<<2656.5249, 2802.7480, 33.1580>>
							fHeading = 		273.192
							weapon = 		WEAPONTYPE_DLC_WRENCH
							sScenario = 		"WORLD_HUMAN_LEANING"
						BREAK			
						CASE 4			
							model = 		S_M_Y_Construct_01
							vCoords = 		<<2648.9971, 2791.6431, 32.7160>>
							fHeading = 		178.191
							weapon = 		WEAPONTYPE_HAMMER
							sScenario = 		"WORLD_HUMAN_WELDING"
						BREAK			
						CASE 5			
							model = 		S_M_Y_Construct_02
							vCoords = 		<<2634.1570, 2798.7720, 32.8240>>
							fHeading = 		259.391
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_SEAT_LEDGE_EATING"
						BREAK			
						CASE 6			
							model = 		S_M_Y_Construct_02
							vCoords = 		<<2644.1663, 2811.5620, 32.8816>>
							fHeading = 		236.1538
							weapon = 		WEAPONTYPE_DLC_WRENCH
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK
						CASE 7			
							model = 		S_M_M_Security_01
							vCoords = 		<<2626.3970, 2780.0576, 32.7654>>
							fHeading = 		236.9998
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_PATROL_1H"
						BREAK			
						CASE 8			
							model = 		S_M_M_Security_01
							vCoords = 		<<2622.3840, 2829.9333, 32.9902>>
							fHeading = 		74.7996
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_PATROL_1H"
						BREAK
					ENDSWITCH				
				BREAK
				CASE CHS_STD2_VARIATION_3
					SWITCH iPed				
						CASE 0			
							model = 		S_M_Y_Construct_02
							vCoords = 		<<1389.8273, -2064.4602, 50.9986>>
							fHeading = 		270.1942
							weapon = 		WEAPONTYPE_DLC_WRENCH
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 1			
							model = 		S_M_Y_Construct_01
							vCoords = 		<<1394.9938, -2072.3574, 50.9985>>
							fHeading = 		200.5936
							weapon = 		WEAPONTYPE_HAMMER
							sScenario = 		"CODE_HUMAN_MEDIC_TIME_OF_DEATH"
						BREAK			
						CASE 2			
							model = 		S_M_Y_Construct_01
							vCoords = 		<<1398.4618, -2058.3887, 50.9986>>
							fHeading = 		244.1938
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"CODE_HUMAN_MEDIC_TEND_TO_DEAD"
						BREAK			
						CASE 3			
							model = 		S_M_Y_Construct_02
							vCoords = 		<<1391.5394, -2064.3899, 50.9985>>
							fHeading = 		91.1926
							weapon = 		WEAPONTYPE_DLC_WRENCH
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 4			
							model = 		S_M_Y_Construct_01
							vCoords = 		<<1402.9103, -2064.3203, 50.9985>>
							fHeading = 		165.5895
							weapon = 		WEAPONTYPE_HAMMER
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 5			
							model = 		S_M_Y_Construct_02
							vCoords = 		<<1385.4030, -2075.8569, 50.9985>>
							fHeading = 		42.39
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK			
						CASE 6			
							model = 		S_M_Y_Construct_01
							vCoords = 		<<1391.1351, -2071.5737, 50.9986>>
							fHeading = 		132.9931
							weapon = 		WEAPONTYPE_DLC_WRENCH
							sScenario = 		""
						BREAK
						CASE 7			
							model = 		S_M_M_Security_01
							vCoords = 		<<1387.6257, -2047.1637, 50.9985>>
							fHeading = 		171.199
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK			
						CASE 8			
							model = 		S_M_M_Security_01
							vCoords = 		<<1388.1549, -2040.8708, 50.9986>>
							fHeading = 		83.1987
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_PATROL_1H"
						BREAK
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_RIOT_VAN
			SWITCH eSubvariation
				CASE CHS_RV_VARIATION_1
					SWITCH iPed				
						CASE 0			
							model = 		G_M_Y_MexGoon_02
							vCoords = 		<<1165.5840, -1646.2750, 35.9200>>
							fHeading = 		174.343
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 1			
							model = 		G_M_Y_MexGoon_02
							vCoords = 		<<1167.7800, -1647.8650, 35.9070>>
							fHeading = 		216.343
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 2			
							model = 		G_M_Y_MexGoon_02
							vCoords = 		<<1165.8500, -1651.6550, 35.8430>>
							fHeading = 		22.542
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 3			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<1160.6250, -1663.5430, 35.5110>>
							fHeading = 		209.942
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_COP_IDLES"
						BREAK			
						CASE 4			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<1156.4659, -1660.0430, 35.6050>>
							fHeading = 		44.342
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 5			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<1167.4590, -1644.5900, 35.9200>>
							fHeading = 		169.141
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_MEDIC_TEND_TO_DEAD"
						BREAK			
						CASE 6			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<1164.9850, -1648.2889, 35.9200>>
							fHeading = 		280.74
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_MEDIC_TIME_OF_DEATH"
						BREAK			
						CASE 7			
							model = 		s_m_m_fibsec_01
							vCoords = 		<<1155.5830, -1659.2350, 35.5900>>
							fHeading = 		225.34
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 8			
							model = 		s_m_m_fibsec_01
							vCoords = 		<<1162.1100, -1647.3700, 35.9200>>
							fHeading = 		275.94
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK			
						CASE 9			
							model = 		s_m_m_fibsec_01
							vCoords = 		<<-632.6570, -2166.9021, 52.3100>>
							fHeading = 		53.799
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 10			
							model = 		s_m_m_fibsec_01
							vCoords = 		<<-633.5100, -2167.2529, 52.2660>>
							fHeading = 		53.799
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 11			
							model = 		s_m_m_fibsec_01
							vCoords = 		<<-223.3810, -623.7680, 33.8790>>
							fHeading = 		69.2
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 12			
							model = 		s_m_m_fibsec_01
							vCoords = 		<<-223.3810, -623.7680, 33.8790>>
							fHeading = 		69.2
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 13			
							model = 		s_m_m_fibsec_01
							vCoords = 		<<-1710.8304, 92.7530, 67.5218>>
							fHeading = 		292.9999
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 14			
							model = 		s_m_m_fibsec_01
							vCoords = 		<<-1710.8304, 92.7530, 67.5218>>
							fHeading = 		292.9999
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_RV_VARIATION_2
					SWITCH iPed				
						CASE 0			
							model = 		A_M_M_Hillbilly_02
							vCoords = 		<<1777.8540, 3329.6179, 40.2700>>
							fHeading = 		294.199
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 1			
							model = 		A_M_M_Hillbilly_02
							vCoords = 		<<1779.6520, 3332.0740, 40.2140>>
							fHeading = 		345.398
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 2			
							model = 		A_M_M_Hillbilly_02
							vCoords = 		<<1776.2939, 3335.1001, 40.2150>>
							fHeading = 		184.998
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 3			
							model = 		S_M_Y_Sheriff_01
							vCoords = 		<<1783.3929, 3333.9810, 40.1530>>
							fHeading = 		303.601
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_COP_IDLES"
						BREAK			
						CASE 4			
							model = 		S_M_Y_Sheriff_01
							vCoords = 		<<1777.1100, 3340.0010, 40.0190>>
							fHeading = 		78.4
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 5			
							model = 		S_M_Y_Sheriff_01
							vCoords = 		<<1780.3220, 3329.5420, 40.2490>>
							fHeading = 		50.4
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_MEDIC_TEND_TO_DEAD"
						BREAK			
						CASE 6			
							model = 		S_M_Y_Sheriff_01
							vCoords = 		<<1778.4139, 3335.2791, 40.1660>>
							fHeading = 		168.8
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_MEDIC_TIME_OF_DEATH"
						BREAK			
						CASE 7			
							model = 		s_m_m_fibsec_01
							vCoords = 		<<1775.9080, 3340.2119, 40.0440>>
							fHeading = 		258.999
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 8			
							model = 		s_m_m_fibsec_01
							vCoords = 		<<1772.3110, 3336.2749, 40.3470>>
							fHeading = 		230.799
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK			
						CASE 9			
							model = 		s_m_m_fibsec_01
							vCoords = 		<<757.8180, 2263.2400, 50.1160>>
							fHeading = 		254.8
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 10			
							model = 		s_m_m_fibsec_01
							vCoords = 		<<756.8760, 2263.3611, 50.1370>>
							fHeading = 		254.8
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 11			
							model = 		s_m_m_fibsec_01
							vCoords = 		<<-2140.9031, 2304.3960, 37.4470>>
							fHeading = 		268.8
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 12			
							model = 		s_m_m_fibsec_01
							vCoords = 		<<-2140.9351, 2304.9409, 37.4460>>
							fHeading = 		268.8
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 13			
							model = 		s_m_m_fibsec_01
							vCoords = 		<<-217.5443, 3991.1667, 39.5280>>
							fHeading = 		5.6
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 14			
							model = 		s_m_m_fibsec_01
							vCoords = 		<<-217.5443, 3991.1667, 39.5280>>
							fHeading = 		5.6
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_RV_VARIATION_3
					SWITCH iPed				
						CASE 0			
							model = 		G_M_Y_Lost_01
							vCoords = 		<<1690.0861, 4912.6250, 41.0780>>
							fHeading = 		49.799
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 1			
							model = 		G_M_Y_Lost_01
							vCoords = 		<<1688.1210, 4917.4990, 41.0780>>
							fHeading = 		166.798
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 2			
							model = 		G_M_Y_Lost_01
							vCoords = 		<<1684.3400, 4916.5981, 41.0780>>
							fHeading = 		336.998
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 3			
							model = 		S_M_Y_Sheriff_01
							vCoords = 		<<1679.2400, 4924.6812, 41.0660>>
							fHeading = 		54.598
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_COP_IDLES"
						BREAK			
						CASE 4			
							model = 		S_M_Y_Sheriff_01
							vCoords = 		<<1683.0380, 4922.1191, 41.0780>>
							fHeading = 		20.398
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 5			
							model = 		S_M_Y_Sheriff_01
							vCoords = 		<<1685.8370, 4917.3521, 41.0780>>
							fHeading = 		196.398
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_MEDIC_TEND_TO_DEAD"
						BREAK			
						CASE 6			
							model = 		S_M_Y_Sheriff_01
							vCoords = 		<<1691.5510, 4915.9268, 41.0780>>
							fHeading = 		136.998
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_MEDIC_TIME_OF_DEATH"
						BREAK			
						CASE 7			
							model = 		s_m_m_fibsec_01
							vCoords = 		<<1682.6030, 4923.3501, 41.0780>>
							fHeading = 		192.397
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 8			
							model = 		s_m_m_fibsec_01
							vCoords = 		<<1690.0229, 4921.0732, 41.0780>>
							fHeading = 		165.197
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK			
						CASE 9			
							model = 		s_m_m_fibsec_01
							vCoords = 		<<-39.2720, 6305.1001, 31.7110>>
							fHeading = 		35.598
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 10			
							model = 		s_m_m_fibsec_01
							vCoords = 		<<-39.2720, 6305.1001, 31.7110>>
							fHeading = 		35.598
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 11			
							model = 		s_m_m_fibsec_01
							vCoords = 		<<693.2080, 3582.5791, 33.8630>>
							fHeading = 		278.798
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 12			
							model = 		s_m_m_fibsec_01
							vCoords = 		<<693.2080, 3582.5791, 33.8630>>
							fHeading = 		278.798
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 13			
							model = 		s_m_m_fibsec_01
							vCoords = 		<<-2262.6116, 4351.7598, 45.0578>>
							fHeading = 		191.2
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 14			
							model = 		s_m_m_fibsec_01
							vCoords = 		<<-2262.6116, 4351.7598, 45.0578>>
							fHeading = 		191.2
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_MERRYWEATHER_CONVOY
			SWITCH eSubvariation
				CASE CHS_MWC_LSIA
					SWITCH iPed				
						CASE 0			
							model = 		s_m_m_chemsec_01
							vCoords = 		<<-1440.1641, 2722.8174, 7.8147>>
							fHeading = 		201.9501
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK			
						CASE 1			
							model = 		s_m_m_chemsec_01
							vCoords = 		<<-1440.1641, 2722.8174, 7.8147>>
							fHeading = 		201.9501
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK			
						CASE 2			
							model = 		s_m_m_chemsec_01
							vCoords = 		<<-1373.4484, -2734.9282, 12.9450>>
							fHeading = 		179.5965
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 3			
							model = 		s_m_m_chemsec_01
							vCoords = 		<<-1373.4236, -2736.4683, 12.9449>>
							fHeading = 		1.7959
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 4			
							model = 		s_m_m_chemsec_01
							vCoords = 		<<-1364.1709, -2733.1506, 12.9450>>
							fHeading = 		308.3955
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_MEDIC_TEND_TO_DEAD"
						BREAK			
						CASE 5			
							model = 		s_m_m_chemsec_01
							vCoords = 		<<-1367.2870, -2742.4695, 12.9450>>
							fHeading = 		155.9948
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK			
						CASE 6			
							model = 		s_m_m_chemsec_01
							vCoords = 		<<-1373.3085, -2741.0154, 12.9450>>
							fHeading = 		155.9948
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 7			
							model = 		s_m_m_chemsec_01
							vCoords = 		<<-1369.5995, -2730.8005, 16.9>>
							fHeading = 		323.3937
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 8			
							model = 		s_m_m_chemsec_01
							vCoords = 		<<-1360.6116, -2715.3972, 16.9>>
							fHeading = 		55.2011
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 9			
							model = 		s_m_m_chemsec_01
							vCoords = 		<<-1335.7977, -2678.4326, 16.9>>
							fHeading = 		132.7452
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK	
						CASE 10			
							model = 		s_m_m_chemsec_01
							vCoords = 		<<-1444.6876, 2735.0161, 9.8261>>
							fHeading = 		198.3501
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		""
						BREAK			
						CASE 11			
							model = 		s_m_m_chemsec_01
							vCoords = 		<<-1444.6876, 2735.0161, 9.8261>>
							fHeading = 		198.3501
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		""
						BREAK			
						CASE 12			
							model = 		s_m_m_chemsec_01
							vCoords = 		<<-1442.4031, 2729.4653, 34.9609>>
							fHeading = 		200.1501
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		""
						BREAK			
						CASE 13			
							model = 		s_m_m_chemsec_01
							vCoords = 		<<-1442.4031, 2729.4653, 34.9609>>
							fHeading = 		200.1501
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		""
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_MWC_SANDY_SHORES
					SWITCH iPed				
						CASE 0			
							model = 		s_m_m_chemsec_01
							vCoords = 		<<2563.9807, -680.2990, 53.4104>>
							fHeading = 		114.3998
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK			
						CASE 1			
							model = 		s_m_m_chemsec_01
							vCoords = 		<<2563.9807, -680.2990, 53.4104>>
							fHeading = 		114.3998
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK			
						CASE 2			
							model = 		s_m_m_chemsec_01
							vCoords = 		<<1168.9558, 3041.6724, 39.5341>>
							fHeading = 		280.3988
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 3			
							model = 		s_m_m_chemsec_01
							vCoords = 		<<1170.3290, 3041.7441, 39.5342>>
							fHeading = 		87.7982
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 4			
							model = 		s_m_m_chemsec_01
							vCoords = 		<<1163.8542, 3033.0413, 39.4330>>
							fHeading = 		140.998
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_MEDIC_TEND_TO_DEAD"
						BREAK			
						CASE 5			
							model = 		s_m_m_chemsec_01
							vCoords = 		<<1176.7220, 3034.5828, 39.5322>>
							fHeading = 		290.1972
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK			
						CASE 6			
							model = 		s_m_m_chemsec_01
							vCoords = 		<<1174.1492, 3038.6292, 39.5341>>
							fHeading = 		291.3969
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 7			
							model = 		s_m_m_chemsec_01
							vCoords = 		<<1161.9474, 3038.6777, 44.0>>
							fHeading = 		145.5965
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 8			
							model = 		s_m_m_chemsec_01
							vCoords = 		<<1148.3109, 3031.8601, 44.0>>
							fHeading = 		145.0705
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 9			
							model = 		s_m_m_chemsec_01
							vCoords = 		<<1104.1165, 3022.5024, 44.0>>
							fHeading = 		264.9843
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK	
						CASE 10			
							model = 		s_m_m_chemsec_01
							vCoords = 		<<2576.4387, -674.7156, 52.4160>>
							fHeading = 		114.5998
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		""
						BREAK			
						CASE 11			
							model = 		s_m_m_chemsec_01
							vCoords = 		<<2576.4387, -674.7156, 52.4160>>
							fHeading = 		114.5998
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		""
						BREAK			
						CASE 12			
							model = 		s_m_m_chemsec_01
							vCoords = 		<<2569.4995, -677.5176, 81.3357>>
							fHeading = 		115.5998
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		""
						BREAK			
						CASE 13			
							model = 		s_m_m_chemsec_01
							vCoords = 		<<2569.4995, -677.5176, 81.3357>>
							fHeading = 		115.5998
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		""
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_EXPLOSIVES_1
			SWITCH eSubvariation
				CASE CHS_EXP1_VARIATION_1
					SWITCH iPed				
						CASE 0			
							model = 		A_M_M_Hillbilly_02
							vCoords = 		<<1744.4150, 3864.5120, 33.5920>>
							fHeading = 		199.397
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_PARTYING"
						BREAK			
						CASE 1			
							model = 		A_M_M_Hillbilly_02
							vCoords = 		<<1744.7531, 3863.0459, 33.5780>>
							fHeading = 		19.997
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"WORLD_HUMAN_PARTYING"
						BREAK			
						CASE 2			
							model = 		A_M_M_Hillbilly_02
							vCoords = 		<<1751.1479, 3859.4900, 33.5350>>
							fHeading = 		173.997
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_STUPOR"
						BREAK			
						CASE 3			
							model = 		A_M_M_Hillbilly_02
							vCoords = 		<<1750.6240, 3868.9690, 33.5250>>
							fHeading = 		314.596
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 4			
							model = 		A_M_M_Hillbilly_02
							vCoords = 		<<1741.8710, 3867.8459, 33.6060>>
							fHeading = 		142.396
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_MEDIC_TEND_TO_DEAD"
						BREAK			
						CASE 5			
							model = 		A_M_M_Hillbilly_02
							vCoords = 		<<1746.0450, 3870.7791, 33.6290>>
							fHeading = 		254.597
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"WORLD_HUMAN_PARTYING"
						BREAK			
						CASE 6			
							model = 		A_M_M_Hillbilly_02
							vCoords = 		<<1751.1331, 3863.9250, 33.5930>>
							fHeading = 		266.796
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_PARTYING"
						BREAK			
						CASE 7			
							model = 		A_M_M_Hillbilly_02
							vCoords = 		<<1747.3000, 3869.7739, 33.6110>>
							fHeading = 		40.197
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 8			
							model = 		G_F_Y_Lost_01
							vCoords = 		<<1752.2180, 3863.7759, 33.5620>>
							fHeading = 		88.197
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"WORLD_HUMAN_PROSTITUTE_HIGH_CLASS"
						BREAK			
						CASE 9			
							model = 		G_F_Y_Lost_01
							vCoords = 		<<1747.3900, 3871.0491, 33.5910>>
							fHeading = 		146.996
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 10			
							model = 		G_F_Y_Lost_01
							vCoords = 		<<1747.8490, 3866.4910, 33.5730>>
							fHeading = 		302.595
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_BUM_SLUMPED"
						BREAK
						CASE 11			
							model = 		A_M_M_Hillbilly_02
							vCoords = 		<<1746.2389, 3859.9312, 33.5647>>
							fHeading = 		212.3968
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 12			
							model = 		A_M_M_Hillbilly_02
							vCoords = 		<<1759.4030, 3870.6001, 33.7020>>
							fHeading = 		327.597
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 	"amb@world_human_bum_slumped@male@laying_on_right_side@base"
						BREAK			
					ENDSWITCH				
				BREAK
				
				CASE CHS_EXP1_VARIATION_2
					SWITCH iPed				
						CASE 0			
							model = 		A_M_M_Hillbilly_02
							vCoords = 		<<-40.2330, 3025.4580, 39.9940>>
							fHeading = 		76.599
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_PARTYING"
						BREAK			
						CASE 1			
							model = 		A_M_M_Hillbilly_02
							vCoords = 		<<-41.6360, 3026.0701, 39.9050>>
							fHeading = 		251.799
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_PARTYING"
						BREAK			
						CASE 2			
							model = 		A_M_M_Hillbilly_02
							vCoords = 		<<-34.4300, 3027.7629, 39.9990>>
							fHeading = 		45.999
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_MEDIC_TEND_TO_DEAD"
						BREAK			
						CASE 3			
							model = 		A_M_M_Hillbilly_02
							vCoords = 		<<-29.4270, 3028.0400, 39.9620>>
							fHeading = 		270.999
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_STUPOR"
						BREAK			
						CASE 4			
							model = 		A_M_M_Hillbilly_02
							vCoords = 		<<-35.4300, 3024.6650, 39.9330>>
							fHeading = 		83.198
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 5			
							model = 		A_M_M_Hillbilly_02
							vCoords = 		<<-36.6950, 3024.5071, 39.9510>>
							fHeading = 		306.798
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_PARTYING"
						BREAK			
						CASE 6			
							model = 		A_M_M_Hillbilly_02
							vCoords = 		<<-33.6040, 3020.0869, 39.7620>>
							fHeading = 		203.398
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 7			
							model = 		A_M_M_Hillbilly_02
							vCoords = 		<<-32.7600, 3023.5601, 39.8660>>
							fHeading = 		355.397
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"WORLD_HUMAN_PARTYING"
						BREAK			
						CASE 8			
							model = 		G_F_Y_Lost_01
							vCoords = 		<<-32.5010, 3024.7209, 39.8930>>
							fHeading = 		179.397
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"WORLD_HUMAN_PROSTITUTE_HIGH_CLASS"
						BREAK			
						CASE 9			
							model = 		G_F_Y_Lost_01
							vCoords = 		<<-36.0880, 3025.6760, 39.9760>>
							fHeading = 		184.197
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 10			
							model = 		G_F_Y_Lost_01
							vCoords = 		<<-36.9110, 3020.8491, 39.8320>>
							fHeading = 		339.396
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_BUM_SLUMPED"
						BREAK
						CASE 11			
							model = 		A_M_M_Hillbilly_02
							vCoords = 		<<-44.4232, 3028.2261, 39.7501>>
							fHeading = 		12.5986
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 12			
							model = 		A_M_M_Hillbilly_02
							vCoords = 		<<-35.5654, 3037.7209, 40.0204>>
							fHeading = 		14.7979
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"amb@world_human_bum_slumped@male@laying_on_right_side@base"
						BREAK
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	SWITCH eVariation
		CASE CHV_FLIGHT_SCHEDULE
			SWITCH eSubvariation
				CASE CHS_FS_ELYSIAN_ISLAND
					SWITCH iPed				
						CASE 0			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<453.5060, -3072.6799, 5.0990>>
							fHeading = 		360.599
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 1			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<460.9470, -3083.7939, 5.0700>>
							fHeading = 		71.599
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 2			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<459.3710, -3083.4341, 5.0700>>
							fHeading = 		256.399
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 3			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<462.2430, -3096.2791, 5.0700>>
							fHeading = 		92.598
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 4			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<452.9610, -3091.0471, 5.0700>>
							fHeading = 		186.798
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"CODE_HUMAN_MEDIC_TEND_TO_DEAD"
						BREAK			
						CASE 5			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<457.0160, -3107.4199, 5.0700>>
							fHeading = 		186.798
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 6			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<467.5740, -3111.4851, 5.0700>>
							fHeading = 		186.798
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 7			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<468.5130, -3107.6560, 5.0700>>
							fHeading = 		121.798
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 8			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<449.9050, -3103.8330, 5.0700>>
							fHeading = 		265.598
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 9			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<464.3630, -3074.4121, 5.0700>>
							fHeading = 		189.397
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 10			
							model = 		S_M_Y_Pilot_01
							vCoords = 		<<-1696.5170, 5456.9839, 144.3740>>
							fHeading = 		189.397
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
					ENDSWITCH					
				BREAK
				CASE CHS_FS_SANDY_SHORES
					SWITCH iPed				
						CASE 0			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<1700.3750, 3276.7839, 40.1500>>
							fHeading = 		216.599
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 1			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<1690.6840, 3274.7080, 40.0060>>
							fHeading = 		172.799
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 2			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<1693.5350, 3284.1179, 40.1470>>
							fHeading = 		159.598
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 3			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<1693.0880, 3282.7771, 40.1430>>
							fHeading = 		344.398
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 4			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<1689.1121, 3289.0630, 40.1470>>
							fHeading = 		131.598
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_MEDIC_TEND_TO_DEAD"
						BREAK			
						CASE 5			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<1682.9420, 3288.3850, 40.1470>>
							fHeading = 		237.198
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_SMOKING_POT"
						BREAK			
						CASE 6			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<1694.7321, 3294.3311, 40.1470>>
							fHeading = 		57.798
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_MEDIC_TIME_OF_DEATH"
						BREAK			
						CASE 7			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<1694.7200, 3288.3201, 40.1470>>
							fHeading = 		202.997
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 8			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<1700.0861, 3286.6880, 47.9220>>
							fHeading = 		210.197
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 9			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<1688.8571, 3277.2400, 40.0840>>
							fHeading = 		123.197
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 10			
							model = 		S_M_Y_Pilot_01
							vCoords = 		<<-1889.8190, -2590.0491, 101.1950>>
							fHeading = 		322.798
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_FS_POWER_STATION
					SWITCH iPed				
						CASE 0			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<2681.7700, 1429.5720, 23.5380>>
							fHeading = 		270.799
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 1			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<2672.5359, 1440.6350, 23.5010>>
							fHeading = 		359.198
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 2			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<2671.0620, 1438.8290, 23.5010>>
							fHeading = 		267.598
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 3			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<2668.0171, 1422.7230, 23.5010>>
							fHeading = 		169.997
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 4			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<2673.2000, 1429.7600, 23.5010>>
							fHeading = 		302.597
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 5			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<2674.3479, 1430.5930, 23.5010>>
							fHeading = 		122.997
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 6			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<2678.3870, 1425.3650, 23.5090>>
							fHeading = 		241.596
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_MEDIC_TEND_TO_DEAD"
						BREAK			
						CASE 7			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<2670.3950, 1429.0610, 23.5010>>
							fHeading = 		356.195
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 8			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<2668.6960, 1435.6169, 23.5010>>
							fHeading = 		88.195
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_WELDING"
						BREAK			
						CASE 9			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<2678.3711, 1433.7490, 23.5090>>
							fHeading = 		351.395
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_SMOKING_POT"
						BREAK			
						CASE 10			
							model = 		S_M_Y_Pilot_01
							vCoords = 		<<3663.2551, 5082.0210, 214.0425>>
							fHeading = 		140.1996
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_MERRYWEATHER_TEST_SITE
			SWITCH eSubvariation
				CASE CHS_MWTS_VARIATION_1
					SWITCH iPed				
						CASE 0			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<333.8646, 3409.4473, 35.6850>>
							fHeading = 		204.7972
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 1			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<330.6271, 3408.1367, 35.7072>>
							fHeading = 		204.7972
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 2			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<341.4654, 3414.8457, 35.5489>>
							fHeading = 		21.9969
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 3			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<321.8176, 3413.7888, 35.6866>>
							fHeading = 		157.9966
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 4			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<341.9211, 3428.3113, 37.8226>>
							fHeading = 		345.7963
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 5			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<329.0147, 3416.2554, 35.6095>>
							fHeading = 		343.7962
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 6			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<329.4037, 3417.5027, 35.5889>>
							fHeading = 		163.9956
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 7			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<346.4772, 3422.5952, 35.2605>>
							fHeading = 		293.795
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 8			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<346.4280, 3418.3601, 35.4139>>
							fHeading = 		259.5948
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_MEDIC_TEND_TO_DEAD"
						BREAK			
						CASE 9			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<351.6558, 3420.2561, 37.7952>>
							fHeading = 		259.5948
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 10			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<335.2214, 3420.9590, 35.4574>>
							fHeading = 		55.5946
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 11			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<325.6721, 3402.9607, 35.7506>>
							fHeading = 		19.3946
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK	
						CASE 12			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<318.8043, 3397.5933, 37.6025>>
							fHeading = 		163.996
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 13			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<357.0643, 3421.3853, 35.1697>>
							fHeading = 		163.996
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 14			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<356.7172, 3419.9810, 35.2404>>
							fHeading = 		341.5956
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_COP_IDLES"
						BREAK			
						CASE 15			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<340.7185, 3424.2056, 35.2879>>
							fHeading = 		163.996
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK			
						CASE 16			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<317.7766, 3402.6765, 35.7573>>
							fHeading = 		309.1954
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 17			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<345.8737, 3407.7153, 35.5883>>
							fHeading = 		110.1951
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 18			
							model = 		hc_gunman
							vCoords = 		<<344.7527, 3407.2832, 35.6011>>
							fHeading = 		285.5946
							weapon = 		WEAPONTYPE_DLC_RAILGUN
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 19			
							model = 		hc_gunman
							vCoords = 		<<336.3897, 3416.6538, 35.5472>>
							fHeading = 		345.9943
							weapon = 		WEAPONTYPE_DLC_RAILGUN
							sScenario = 		"WORLD_HUMAN_GUARD_STAND"
						BREAK
					ENDSWITCH				
				BREAK
				CASE CHS_MWTS_VARIATION_2
					SWITCH iPed				
						CASE 0			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<533.0010, -2845.6499, 5.0450>>
							fHeading = 		58.398
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 1			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<540.3350, -2849.9709, 5.0450>>
							fHeading = 		240.198
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 2			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<532.9290, -2854.9919, 7.6680>>
							fHeading = 		151.998
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 3			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<543.0050, -2847.6609, 5.0450>>
							fHeading = 		151.998
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 4			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<549.4980, -2848.1541, 5.0450>>
							fHeading = 		60.997
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 5			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<547.0870, -2835.5410, 5.0450>>
							fHeading = 		148.397
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 6			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<537.3960, -2828.2800, 5.0450>>
							fHeading = 		18.597
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 7			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<541.6710, -2836.3069, 5.0450>>
							fHeading = 		124.197
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 8			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<540.4350, -2837.1721, 5.0450>>
							fHeading = 		303.596
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 9			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<547.7670, -2843.8250, 5.0450>>
							fHeading = 		241.596
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_MEDIC_TEND_TO_DEAD"
						BREAK			
						CASE 10			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<533.8630, -2837.0779, 5.0450>>
							fHeading = 		342.796
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 11			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<550.4180, -2842.0330, 7.5520>>
							fHeading = 		342.796
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 12			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<515.3879, -2824.9465, 7.1105>>
							fHeading = 		58.398
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 13			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<523.4219, -2829.8682, 5.0454>>
							fHeading = 		325.9977
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 14			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<522.3855, -2836.0168, 5.0454>>
							fHeading = 		60.1971
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_COP_IDLES"
						BREAK			
						CASE 15			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<521.0564, -2835.2644, 5.0454>>
							fHeading = 		239.7965
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 16			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<535.5506, -2850.6770, 5.0454>>
							fHeading = 		330.9964
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK			
						CASE 17			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<530.5748, -2830.6077, 5.0454>>
							fHeading = 		92.5979
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 18			
							model = 		hc_gunman
							vCoords = 		<<529.1155, -2830.7964, 5.0454>>
							fHeading = 		272.3977
							weapon = 		WEAPONTYPE_DLC_RAILGUN
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 19			
							model = 		hc_gunman
							vCoords = 		<<543.2274, -2841.9561, 5.0454>>
							fHeading = 		172.3974
							weapon = 		WEAPONTYPE_DLC_RAILGUN
							sScenario = 		"WORLD_HUMAN_GUARD_STAND"
						BREAK
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	SWITCH eVariation
		CASE CHV_EXPLOSIVES_2
			SWITCH eSubvariation
				CASE CHS_EXP2_VARIATION_1
					SWITCH iPed				
						CASE 0			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_securoguard_01"))
							vCoords = 		<<843.2550, -1988.8030, 28.3010>>
							fHeading = 		326.799
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 1			
							model = 		S_M_M_HighSec_01
							vCoords = 		<<844.0440, -1987.7750, 28.3010>>
							fHeading = 		137.199
							weapon = 		WEAPONTYPE_DLC_PISTOL50
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 2			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_securoguard_01"))
							vCoords = 		<<841.1050, -1995.0430, 31.8240>>
							fHeading = 		355.599
							weapon = 		WEAPONTYPE_HEAVYSNIPER
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 3			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_securoguard_01"))
							vCoords = 		<<850.2350, -1995.4390, 28.9850>>
							fHeading = 		87.199
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 4			
							model = 		S_M_M_HighSec_02
							vCoords = 		<<846.2850, -1992.5760, 28.3010>>
							fHeading = 		164.399
							weapon = 		WEAPONTYPE_DLC_PISTOL50
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 5			
							model = 		S_M_M_HighSec_01
							vCoords = 		<<1022.7510, -3101.6230, -40.0000>>
							fHeading = 		342.996
							weapon = 		WEAPONTYPE_DLC_PISTOL50
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 6			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_securoguard_01"))
							vCoords = 		<<1023.0580, -3100.2500, -40.0000>>
							fHeading = 		158.195
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 7			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_securoguard_01"))
							vCoords = 		<<1000.0480, -3097.0161, -40.0000>>
							fHeading = 		167.994
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 8			
							model = 		S_M_M_HighSec_01
							vCoords = 		<<999.7570, -3098.6489, -40.0000>>
							fHeading = 		346.794
							weapon = 		WEAPONTYPE_DLC_PISTOL50
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 9			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_securoguard_01"))
							vCoords = 		<<1015.2970, -3093.8850, -40.0000>>
							fHeading = 		357.794
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 10			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_securoguard_01"))
							vCoords = 		<<1014.6020, -3112.3130, -40.0000>>
							fHeading = 		360.393
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK			
						CASE 11			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_securoguard_01"))
							vCoords = 		<<1010.0200, -3099.8560, -40.0000>>
							fHeading = 		97.193
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 12			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_securoguard_01"))
							vCoords = 		<<1014.4810, -3105.8601, -40.0000>>
							fHeading = 		277.392
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 13			
							model = 		S_M_M_HighSec_02
							vCoords = 		<<1020.6510, -3105.8750, -40.0000>>
							fHeading = 		238.392
							weapon = 		WEAPONTYPE_DLC_PISTOL50
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 14			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_securoguard_01"))
							vCoords = 		<<1003.0200, -3106.6860, -40.0000>>
							fHeading = 		209.791
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
			
					ENDSWITCH				
				BREAK
				
				CASE CHS_EXP2_VARIATION_2
					SWITCH iPed				
						CASE 0			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_securoguard_01"))
							vCoords = 		<<-1893.4320, -3034.8220, 12.9440>>
							fHeading = 		304.599
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 1			
							model = 		S_M_M_HighSec_01
							vCoords = 		<<-1892.3610, -3034.1279, 12.9440>>
							fHeading = 		121.198
							weapon = 		WEAPONTYPE_DLC_PISTOL50
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 2			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_securoguard_01"))
							vCoords = 		<<-1897.1310, -3031.5200, 12.9440>>
							fHeading = 		240.198
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 3			
							model = 		S_M_M_HighSec_02
							vCoords = 		<<-1893.0730, -3028.3420, 12.9440>>
							fHeading = 		70.598
							weapon = 		WEAPONTYPE_DLC_PISTOL50
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 4			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_securoguard_01"))
							vCoords = 		<<-1899.7371, -3038.7629, 12.9440>>
							fHeading = 		239.197
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 5			
							model = 		S_M_M_HighSec_01
							vCoords = 		<<1022.7510, -3101.6230, -40.0000>>
							fHeading = 		342.996
							weapon = 		WEAPONTYPE_DLC_PISTOL50
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 6			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_securoguard_01"))
							vCoords = 		<<1023.0580, -3100.2500, -40.0000>>
							fHeading = 		158.195
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 7			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_securoguard_01"))
							vCoords = 		<<1000.0480, -3097.0161, -40.0000>>
							fHeading = 		167.994
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 8			
							model = 		S_M_M_HighSec_01
							vCoords = 		<<999.7570, -3098.6489, -40.0000>>
							fHeading = 		346.794
							weapon = 		WEAPONTYPE_DLC_PISTOL50
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 9			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_securoguard_01"))
							vCoords = 		<<1015.2970, -3093.8850, -40.0000>>
							fHeading = 		357.794
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 10			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_securoguard_01"))
							vCoords = 		<<1014.6020, -3112.3130, -40.0000>>
							fHeading = 		360.393
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK			
						CASE 11			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_securoguard_01"))
							vCoords = 		<<1010.0200, -3099.8560, -40.0000>>
							fHeading = 		97.193
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 12			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_securoguard_01"))
							vCoords = 		<<1014.4810, -3105.8601, -40.0000>>
							fHeading = 		277.392
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 13			
							model = 		S_M_M_HighSec_02
							vCoords = 		<<1020.6510, -3105.8750, -40.0000>>
							fHeading = 		238.392
							weapon = 		WEAPONTYPE_DLC_PISTOL50
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 14			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_securoguard_01"))
							vCoords = 		<<1003.0200, -3106.6860, -40.0000>>
							fHeading = 		209.791
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
		
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_FIREFIGHTER_OUTFIT_2
			SWITCH eSubvariation
				CASE CHS_FF2_VARIATION_1
					SWITCH iPed				
						CASE 0			
							model = 		S_M_Y_Sheriff_01
							vCoords = 		<<1438.3580, 2794.0259, 51.4870>>
							fHeading = 		280.001
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_POLICE_CROWD_CONTROL"
						BREAK			
						CASE 1			
							model = 		S_M_Y_Sheriff_01
							vCoords = 		<<1429.8900, 2798.6641, 51.4320>>
							fHeading = 		303.801
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_COP_IDLES"
						BREAK			
						CASE 2			
							model = 		S_M_Y_Fireman_01
							vCoords = 		<<1431.0370, 2799.7610, 51.4410>>
							fHeading = 		122.8
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 3			
							model = 		S_M_Y_Fireman_01
							vCoords = 		<<1432.2250, 2797.8259, 53.8730>>
							fHeading = 		122.8
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 4			
							model = 		S_M_Y_Fireman_01
							vCoords = 		<<1435.0520, 2786.4060, 51.3220>>
							fHeading = 		48.6
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 5			
							model = 		A_F_Y_Runner_01
							vCoords = 		<<1440.4330, 2794.3560, 51.5190>>
							fHeading = 		112
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 6			
							model = 		A_F_Y_Runner_01
							vCoords = 		<<1441.1331, 2795.6279, 51.5220>>
							fHeading = 		132
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_STAND_IMPATIENT"
						BREAK			
						CASE 7			
							model = 		A_M_M_Hillbilly_01
							vCoords = 		<<1423.2274, 2794.5010, 51.4060>>
							fHeading = 		260
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_STUPOR"
						BREAK			
						CASE 8			
							model = 		S_M_Y_Sheriff_01
							vCoords = 		<<1424.7629, 2794.1790, 51.3850>>
							fHeading = 		79.601
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_MEDIC_TIME_OF_DEATH"
						BREAK			
					ENDSWITCH
				BREAK
				CASE CHS_FF2_VARIATION_2
					SWITCH iPed				
						CASE 0			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<-787.9350, 579.1470, 125.5330>>
							fHeading = 		74.26
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_COP_IDLES"
						BREAK			
						CASE 1			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<-786.5270, 588.3110, 125.8150>>
							fHeading = 		36.26
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_POLICE_CROWD_CONTROL"
						BREAK			
						CASE 2			
							model = 		S_M_Y_Fireman_01
							vCoords = 		<<-789.2040, 579.6320, 125.5880>>
							fHeading = 		240.26
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 3			
							model = 		S_M_Y_Fireman_01
							vCoords = 		<<-782.6380, 585.0150, 125.6480>>
							fHeading = 		187.26
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 4			
							model = 		S_M_Y_Fireman_01
							vCoords = 		<<-789.5470, 584.1880, 128.2230>>
							fHeading = 		187.26
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 5			
							model = 		A_F_Y_Runner_01
							vCoords = 		<<-788.8240, 590.5710, 126.0560>>
							fHeading = 		223.66
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 6			
							model = 		A_F_Y_Runner_01
							vCoords = 		<<-787.3820, 590.2750, 125.9210>>
							fHeading = 		215.26
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_STAND_IMPATIENT"
						BREAK			
						CASE 7			
							model = 		A_M_Y_BusiCas_01
							vCoords = 		<<-783.9950, 573.3050, 125.1460>>
							fHeading = 		340.659
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_STUPOR"
						BREAK			
						CASE 8			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<-783.5217, 574.5789, 125.2025>>
							fHeading = 		155.46
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_MEDIC_TIME_OF_DEATH"
						BREAK			
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_VAULT_LASER_1
			SWITCH eSubvariation
				CASE CHS_VL1_GRAND_SENORA_DESERT
					SWITCH iPed				
						CASE 0			
							model = 		S_M_Y_MARINE_03
							vCoords = 		<<1038.4940, 3186.2280, 38.1020>>
							fHeading = 		208.198
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_WINDOW_SHOP_BROWSE"
						BREAK			
						CASE 1			
							model = 		S_M_Y_MARINE_03
							vCoords = 		<<1039.0740, 3194.8159, 37.8820>>
							fHeading = 		238.597
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 2			
							model = 		S_M_Y_MARINE_03
							vCoords = 		<<1020.5710, 3184.5391, 37.8710>>
							fHeading = 		256.998
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_GUARD_STAND"
						BREAK			
						CASE 3			
							model = 		S_M_Y_MARINE_03
							vCoords = 		<<1031.0990, 3206.9990, 37.2150>>
							fHeading = 		192.398
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_PATROL_1H"
						BREAK			
						CASE 4			
							model = 		S_M_Y_MARINE_03
							vCoords = 		<<1050.2830, 3188.0681, 38.3770>>
							fHeading = 		163.597
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK			
						CASE 5			
							model = 		S_M_Y_MARINE_03
							vCoords = 		<<1021.9820, 3197.2410, 37.4840>>
							fHeading = 		160.997
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 6			
							model = 		S_M_Y_MARINE_03
							vCoords = 		<<1036.1560, 3189.9829, 37.9560>>
							fHeading = 		90.397
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 7			
							model = 		S_M_Y_MARINE_03
							vCoords = 		<<1034.5940, 3189.8401, 37.8680>>
							fHeading = 		271.797
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"WORLD_HUMAN_COP_IDLES"
						BREAK			
						CASE 8			
							model = 		S_M_Y_MARINE_03
							vCoords = 		<<1026.9880, 3189.7830, 37.6940>>
							fHeading = 		67.997
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_MEDIC_TEND_TO_DEAD"
						BREAK			
						CASE 9			
							model = 		S_M_Y_MARINE_03
							vCoords = 		<<1042.4301, 3201.9790, 37.5190>>
							fHeading = 		159.997
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 10			
							model = 		S_M_Y_MARINE_03
							vCoords = 		<<1041.8979, 3200.4241, 37.6080>>
							fHeading = 		337.796
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_MUSCLE_FLEX"
						BREAK			
						CASE 11			
							model = 		S_M_Y_MARINE_03
							vCoords = 		<<1025.9200, 3193.5190, 37.5890>>
							fHeading = 		128.196
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 12			
							model = 		S_M_Y_MARINE_03
							vCoords = 		<<1017.0470, 3238.5767, 37.1446>>
							fHeading = 		45.7969
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 13			
							model = 		S_M_Y_MARINE_03
							vCoords = 		<<1015.9453, 3239.7417, 37.1173>>
							fHeading = 		227.1967
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 14			
							model = 		S_M_Y_MARINE_03
							vCoords = 		<<1021.8156, 3239.0432, 37.1498>>
							fHeading = 		336.9964
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_MEDIC_KNEEL"
						BREAK			
						CASE 15			
							model = 		S_M_Y_MARINE_03
							vCoords = 		<<1036.0488, 3173.7629, 40.1433>>
							fHeading = 		225.3993
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 16			
							model = 		S_M_Y_MARINE_03
							vCoords = 		<<1034.1927, 3240.6287, 39.3538>>
							fHeading = 		225.3993
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 17			
							model = 		S_M_Y_MARINE_03
							vCoords = 		<<1041.3794, 3240.2131, 36.7635>>
							fHeading = 		279.7991
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 18			
							model = 		S_M_Y_MARINE_03
							vCoords = 		<<1043.0967, 3180.6814, 37.9722>>
							fHeading = 		210.7989
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 19			
							model = 		S_M_Y_MARINE_03
							vCoords = 		<<1024.2268, 3178.9404, 38.0401>>
							fHeading = 		139.9988
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_VL1_LA_MESA
					SWITCH iPed				
						CASE 0			
							model = 		S_M_Y_MARINE_03
							vCoords = 		<<1226.7360, -2368.7461, 48.8800>>
							fHeading = 		88.106
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_WINDOW_SHOP_BROWSE"
						BREAK			
						CASE 1			
							model = 		S_M_Y_MARINE_03
							vCoords = 		<<1248.0940, -2363.8970, 48.4620>>
							fHeading = 		341.105
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 2			
							model = 		S_M_Y_MARINE_03
							vCoords = 		<<1235.3060, -2362.4180, 48.7630>>
							fHeading = 		222.105
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 3			
							model = 		S_M_Y_MARINE_03
							vCoords = 		<<1236.6670, -2353.2129, 49.0200>>
							fHeading = 		215.105
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"WORLD_HUMAN_GUARD_STAND"
						BREAK			
						CASE 4			
							model = 		S_M_Y_MARINE_03
							vCoords = 		<<1244.0120, -2377.8870, 47.6200>>
							fHeading = 		165.505
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"WORLD_HUMAN_GUARD_STAND"
						BREAK			
						CASE 5			
							model = 		S_M_Y_MARINE_03
							vCoords = 		<<1250.4530, -2341.4199, 49.3430>>
							fHeading = 		328.504
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 6			
							model = 		S_M_Y_MARINE_03
							vCoords = 		<<1221.5480, -2352.8740, 58.9920>>
							fHeading = 		286.303
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"WORLD_HUMAN_BINOCULARS"
						BREAK			
						CASE 7			
							model = 		S_M_Y_MARINE_03
							vCoords = 		<<1229.8850, -2341.1250, 58.9920>>
							fHeading = 		244.903
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"WORLD_HUMAN_BINOCULARS"
						BREAK			
						CASE 8			
							model = 		S_M_Y_MARINE_03
							vCoords = 		<<1257.8970, -2365.3799, 48.6990>>
							fHeading = 		198.902
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_COP_IDLES"
						BREAK			
						CASE 9			
							model = 		S_M_Y_MARINE_03
							vCoords = 		<<1258.5710, -2367.0930, 48.6330>>
							fHeading = 		22.502
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 10			
							model = 		S_M_Y_MARINE_03
							vCoords = 		<<1243.4449, -2361.1479, 48.6140>>
							fHeading = 		120.902
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK			
						CASE 11			
							model = 		S_M_Y_MARINE_03
							vCoords = 		<<1239.1580, -2359.2729, 48.7820>>
							fHeading = 		129.702
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 12			
							model = 		S_M_Y_MARINE_03
							vCoords = 		<<1237.1459, -2396.4121, 49.2111>>
							fHeading = 		165.505
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 13			
							model = 		S_M_Y_MARINE_03
							vCoords = 		<<1266.1398, -2321.1323, 50.7058>>
							fHeading = 		165.505
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 14			
							model = 		S_M_Y_MARINE_03
							vCoords = 		<<1259.4846, -2328.3845, 49.7569>>
							fHeading = 		308.7046
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_MEDIC_KNEEL"
						BREAK			
						CASE 15			
							model = 		S_M_Y_MARINE_03
							vCoords = 		<<1249.1783, -2394.0706, 47.1654>>
							fHeading = 		278.7043
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 16			
							model = 		S_M_Y_MARINE_03
							vCoords = 		<<1241.3491, -2402.6506, 46.4727>>
							fHeading = 		162.7038
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 17			
							model = 		S_M_Y_MARINE_03
							vCoords = 		<<1235.4061, -2382.7700, 47.2344>>
							fHeading = 		217.9036
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 18			
							model = 		S_M_Y_MARINE_03
							vCoords = 		<<1236.3529, -2383.8132, 47.1624>>
							fHeading = 		37.3035
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_COP_IDLES"
						BREAK			
						CASE 19			
							model = 		S_M_Y_MARINE_03
							vCoords = 		<<1262.5804, -2318.0664, 49.2169>>
							fHeading = 		343.7033
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_VAULT_LASER_2
			SWITCH eSubvariation
				CASE CHS_VL2_POWER_STATION
					SWITCH iPed				
						CASE 0			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_avongoon"))
							vCoords = 		<<2838.0830, 1463.3521, 23.5630>>
							fHeading = 		128.599
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 1			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_avongoon"))
							vCoords = 		<<2837.0271, 1462.5760, 23.5610>>
							fHeading = 		306.798
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 2			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_avongoon"))
							vCoords = 		<<2818.1360, 1463.5680, 23.7360>>
							fHeading = 		259.198
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_GUARD_STAND"
						BREAK			
						CASE 3			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_avongoon"))
							vCoords = 		<<2845.0608, 1458.8174, 31.5986>>
							fHeading = 		46.7977
							weapon = 		WEAPONTYPE_HEAVYSNIPER
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 4			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_avongoon"))
							vCoords = 		<<2844.7820, 1465.1710, 23.5550>>
							fHeading = 		348.146
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 5			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_avongoon"))
							vCoords = 		<<2846.0061, 1462.3190, 23.5640>>
							fHeading = 		163.145
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_MEDIC_TEND_TO_DEAD"
						BREAK			
						CASE 6			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_avongoon"))
							vCoords = 		<<2828.6211, 1464.5100, 23.5550>>
							fHeading = 		317.945
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK			
						CASE 7			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_avongoon"))
							vCoords = 		<<2844.3989, 1472.5320, 23.5550>>
							fHeading = 		84.197
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 8			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_avongoon"))
							vCoords = 		<<2816.1238, 1465.3490, 33.3519>>
							fHeading = 		275.3969
							weapon = 		WEAPONTYPE_HEAVYSNIPER
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 9			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("u_m_y_juggernaut_01"))
							vCoords = 		<<2842.6399, 1456.9580, 23.7360>>
							fHeading = 		75.597
							weapon = 		WEAPONTYPE_MINIGUN
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 10			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_avongoon"))
							vCoords = 		<<1098.5300, -3197.0400, -39.9930>>
							fHeading = 		77.799
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 11			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_avongoon"))
							vCoords = 		<<1098.0341, -3193.8650, -39.9930>>
							fHeading = 		98.799
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 12			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_avongoon"))
							vCoords = 		<<1098.3361, -3195.5117, -39.9935>>
							fHeading = 		98.9989
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 13			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_avongoon"))
							vCoords = 		<<1095.0680, -3198.3770, -39.9930>>
							fHeading = 		43.599
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_VL2_ZANCUDO
					SWITCH iPed				
						CASE 0			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_avongoon"))
							vCoords = 		<<-2016.2020, 3400.3191, 30.1410>>
							fHeading = 		351.799
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 1			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_avongoon"))
							vCoords = 		<<-2038.0450, 3382.4150, 30.2660>>
							fHeading = 		102.199
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 2			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_avongoon"))
							vCoords = 		<<-2026.5880, 3386.5969, 30.1810>>
							fHeading = 		127.598
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 3			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_avongoon"))
							vCoords = 		<<-2023.4490, 3379.7773, 34.6177>>
							fHeading = 		0.1975
							weapon = 		WEAPONTYPE_HEAVYSNIPER
							sScenario = 		"WORLD_HUMAN_BINOCULARS"
						BREAK			
						CASE 4			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_avongoon"))
							vCoords = 		<<-2022.0050, 3387.1689, 30.0450>>
							fHeading = 		271.798
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 5			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_avongoon"))
							vCoords = 		<<-2020.7310, 3387.1499, 29.9810>>
							fHeading = 		92.398
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 6			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_avongoon"))
							vCoords = 		<<-2010.3600, 3381.9050, 30.2500>>
							fHeading = 		259.527
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"CODE_HUMAN_MEDIC_TEND_TO_DEAD"
						BREAK			
						CASE 7			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_avongoon"))
							vCoords = 		<<-2003.4569, 3393.4404, 40.7472>>
							fHeading = 		80.9269
							weapon = 		WEAPONTYPE_HEAVYSNIPER
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 8			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_avongoon"))
							vCoords = 		<<-2012.8820, 3371.1289, 30.3230>>
							fHeading = 		201.926
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 9			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("u_m_y_juggernaut_01"))
							vCoords = 		<<-2018.1110, 3384.2500, 30.2310>>
							fHeading = 		33.726
							weapon = 		WEAPONTYPE_MINIGUN
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 10			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_avongoon"))
							vCoords = 		<<1098.5300, -3197.0400, -39.9930>>
							fHeading = 		77.799
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 11			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_avongoon"))
							vCoords = 		<<1098.0341, -3193.8650, -39.9930>>
							fHeading = 		98.799
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 12			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_avongoon"))
							vCoords = 		<<1098.3361, -3195.5117, -39.9935>>
							fHeading = 		98.9989
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 13			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_avongoon"))
							vCoords = 		<<1095.0680, -3198.3770, -39.9930>>
							fHeading = 		43.599
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
					ENDSWITCH	
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_WEAPONS_STASH
			SWITCH eSubvariation
				CASE CHS_WST_SAN_ANDREAS
					SWITCH iPed				
						CASE 0			
							model = 		G_M_Y_MexGoon_01
							vCoords = 		<<967.8080, -1829.0680, 30.2380>>
							fHeading = 		6.4
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK			
						CASE 1			
							model = 		G_M_Y_MexGoon_01
							vCoords = 		<<978.5730, -1830.3160, 30.2420>>
							fHeading = 		56.799
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 2			
							model = 		G_M_Y_MexGoon_01
							vCoords = 		<<975.1080, -1825.3960, 30.1460>>
							fHeading = 		131.599
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_MEDIC_TEND_TO_DEAD"
						BREAK			
						CASE 3			
							model = 		G_F_Y_Vagos_01
							vCoords = 		<<977.5140, -1829.5200, 30.2140>>
							fHeading = 		247.599
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 4			
							model = 		G_F_Y_Vagos_01
							vCoords = 		<<980.9240, -1825.8470, 30.2090>>
							fHeading = 		221.199
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_WINDOW_SHOP_BROWSE"
						BREAK			
						CASE 5			
							model = 		G_M_Y_MexGoon_01
							vCoords = 		<<1116.4930, -3146.3960, -38.0630>>
							fHeading = 		360.789
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		""
						BREAK			
						CASE 6			
							model = 		G_M_Y_MexGoon_01
							vCoords = 		<<1111.7640, -3144.1770, -38.0630>>
							fHeading = 		360.789
							weapon = 		WEAPONTYPE_SAWNOFFSHOTGUN
							sScenario = 		""
						BREAK			
						CASE 7			
							model = 		G_M_Y_MexGoon_01
							vCoords = 		<<1122.2922, -3152.1829, -38.0628>>
							fHeading = 		98.9887
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		""
						BREAK			
						CASE 8			
							model = 		G_M_Y_MexGoon_01
							vCoords = 		<<1123.2880, -3148.2090, -38.0630>>
							fHeading = 		118.788
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 9			
							model = 		G_F_Y_Vagos_01
							vCoords = 		<<1122.2710, -3148.4951, -38.0630>>
							fHeading = 		306.588
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 10			
							model = 		G_F_Y_Vagos_01
							vCoords = 		<<1116.6870, -3155.8220, -38.0630>>
							fHeading = 		34.588
							weapon = 		WEAPONTYPE_SAWNOFFSHOTGUN
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 11			
							model = 		G_M_Y_MexGoon_01
							vCoords = 		<<1125.1530, -3160.1760, -38.0630>>
							fHeading = 		294.788
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		""
						BREAK			
						CASE 12			
							model = 		G_M_Y_MexGoon_01
							vCoords = 		<<1115.8130, -3161.3840, -37.8700>>
							fHeading = 		282.187
							weapon = 		WEAPONTYPE_SAWNOFFSHOTGUN
							sScenario = 		""
						BREAK			
						CASE 13			
							model = 		G_F_Y_Vagos_01
							vCoords = 		<<1120.3490, -3162.2920, -37.8700>>
							fHeading = 		21.387
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		""
						BREAK			
						CASE 14			
							model = 		G_F_Y_Vagos_01
							vCoords = 		<<1102.3710, -3152.7859, -38.5190>>
							fHeading = 		79.387
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 15			
							model = 		G_M_Y_MexGoon_01
							vCoords = 		<<1101.2330, -3152.1460, -38.5190>>
							fHeading = 		241.386
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 16			
							model = 		G_M_Y_MexGoon_01
							vCoords = 		<<1109.6290, -3151.3979, -38.5190>>
							fHeading = 		10.786
							weapon = 		WEAPONTYPE_SAWNOFFSHOTGUN
							sScenario = 		"WORLD_HUMAN_WINDOW_SHOP_BROWSE"
						BREAK			
						CASE 17			
							model = 		G_M_Y_MexGoon_01
							vCoords = 		<<1115.7340, -3150.4209, -38.0630>>
							fHeading = 		244.786
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK			
						CASE 18			
							model = 		G_M_Y_MexGoon_02
							vCoords = 		<<961.4141, -1818.7623, 30.2312>>
							fHeading = 		83.9987
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"WORLD_HUMAN_GUARD_STAND"
						BREAK			
						CASE 19			
							model = 		G_M_Y_MexGoon_02
							vCoords = 		<<961.9948, -1812.0955, 30.2313>>
							fHeading = 		92.3986
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK			
						CASE 20			
							model = 		G_M_Y_MexGoon_02
							vCoords = 		<<971.7352, -1823.1600, 30.1213>>
							fHeading = 		120.1985
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 21			
							model = 		G_F_Y_Vagos_01
							vCoords = 		<<970.4577, -1823.6887, 30.1104>>
							fHeading = 		304.7987
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK				
		
					ENDSWITCH				
				BREAK
				CASE CHS_WST_PALETO_BAY
					SWITCH iPed				
						CASE 0			
							model = 		G_M_Y_Lost_01
							vCoords = 		<<-80.4820, 6494.5190, 30.4910>>
							fHeading = 		7.6
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 1			
							model = 		G_M_Y_Lost_01
							vCoords = 		<<-79.9730, 6502.0610, 30.4910>>
							fHeading = 		221.199
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK			
						CASE 2			
							model = 		G_M_Y_Lost_01
							vCoords = 		<<-85.0600, 6491.9380, 30.4910>>
							fHeading = 		320.599
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_MEDIC_TEND_TO_DEAD"
						BREAK			
						CASE 3			
							model = 		G_F_Y_Lost_01
							vCoords = 		<<-80.6620, 6495.5000, 30.4910>>
							fHeading = 		196.398
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 4			
							model = 		G_F_Y_Lost_01
							vCoords = 		<<-84.0850, 6494.9639, 30.4910>>
							fHeading = 		136.598
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_MEDIC_TEND_TO_DEAD"
						BREAK			
						CASE 5			
							model = 		G_M_Y_Lost_01
							vCoords = 		<<1116.4930, -3146.3960, -38.0630>>
							fHeading = 		360.789
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		""
						BREAK			
						CASE 6			
							model = 		G_M_Y_Lost_01
							vCoords = 		<<1111.7640, -3144.1770, -38.0630>>
							fHeading = 		360.789
							weapon = 		WEAPONTYPE_SAWNOFFSHOTGUN
							sScenario = 		""
						BREAK			
						CASE 7			
							model = 		G_M_Y_Lost_01
							vCoords = 		<<1122.2922, -3152.1829, -38.0628>>
							fHeading = 		98.9887
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		""
						BREAK			
						CASE 8			
							model = 		G_M_Y_Lost_01
							vCoords = 		<<1123.2880, -3148.2090, -38.0630>>
							fHeading = 		118.788
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 9			
							model = 		G_F_Y_Lost_01
							vCoords = 		<<1122.2710, -3148.4951, -38.0630>>
							fHeading = 		306.588
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 10			
							model = 		G_F_Y_Lost_01
							vCoords = 		<<1116.6870, -3155.8220, -38.0630>>
							fHeading = 		34.588
							weapon = 		WEAPONTYPE_SAWNOFFSHOTGUN
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 11			
							model = 		G_M_Y_Lost_01
							vCoords = 		<<1125.1530, -3160.1760, -38.0630>>
							fHeading = 		294.788
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		""
						BREAK			
						CASE 12			
							model = 		G_M_Y_Lost_01
							vCoords = 		<<1115.8130, -3161.3840, -37.8700>>
							fHeading = 		282.187
							weapon = 		WEAPONTYPE_SAWNOFFSHOTGUN
							sScenario = 		""
						BREAK			
						CASE 13			
							model = 		G_F_Y_Lost_01
							vCoords = 		<<1120.3490, -3162.2920, -37.8700>>
							fHeading = 		21.387
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		""
						BREAK			
						CASE 14			
							model = 		G_F_Y_Lost_01
							vCoords = 		<<1102.3710, -3152.7859, -38.5190>>
							fHeading = 		79.387
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 15			
							model = 		G_M_Y_Lost_01
							vCoords = 		<<1101.2330, -3152.1460, -38.5190>>
							fHeading = 		241.386
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 16			
							model = 		G_M_Y_Lost_01
							vCoords = 		<<1109.6290, -3151.3979, -38.5190>>
							fHeading = 		10.786
							weapon = 		WEAPONTYPE_SAWNOFFSHOTGUN
							sScenario = 		"WORLD_HUMAN_WINDOW_SHOP_BROWSE"
						BREAK			
						CASE 17			
							model = 		G_M_Y_Lost_01
							vCoords = 		<<1115.7340, -3150.4209, -38.0630>>
							fHeading = 		244.786
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK			
						CASE 18			
							model = 		G_M_Y_Lost_01
							vCoords = 		<<-67.8768, 6483.4951, 30.4565>>
							fHeading = 		203.5997
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"WORLD_HUMAN_GUARD_STAND"
						BREAK			
						CASE 19			
							model = 		G_M_Y_Lost_01
							vCoords = 		<<-75.0707, 6476.3228, 30.4566>>
							fHeading = 		249.7995
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK			
						CASE 20			
							model = 		G_M_Y_Lost_01
							vCoords = 		<<-74.4785, 6502.2095, 30.4909>>
							fHeading = 		273.5993
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 21			
							model = 		G_F_Y_Lost_01
							vCoords = 		<<-73.3995, 6502.3477, 30.4909>>
							fHeading = 		103.1978
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
	
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_VAULT_DRILL_2
			SWITCH eSubvariation
				CASE CHS_VD2_VARIATION_1
					SWITCH iPed				
						CASE 0			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<324.4000, -274.3800, 52.9090>>
							fHeading = 		112.4
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 1			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<322.2030, -270.8920, 52.9020>>
							fHeading = 		122.4
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 2			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<313.0390, -268.4210, 52.9260>>
							fHeading = 		191.599
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 3			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<310.4690, -266.3770, 52.9420>>
							fHeading = 		135.199
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 4			
							model = 		mp_g_m_pros_01
							vCoords = 		<<297.9930, -268.3990, 53.0190>>
							fHeading = 		258.799
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		""
						BREAK			
						CASE 5			
							model = 		mp_g_m_pros_01
							vCoords = 		<<315.6540, -277.6600, 53.1650>>
							fHeading = 		4.398
							weapon = 		WEAPONTYPE_ASSAULTSHOTGUN
							sScenario = 		""
						BREAK			
						CASE 6			
							model = 		mp_g_m_pros_01
							vCoords = 		<<309.1910, -277.4560, 53.1650>>
							fHeading = 		293.798
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		""
						BREAK			
						CASE 7			
							model = 		mp_g_m_pros_01
							vCoords = 		<<310.3860, -283.1490, 53.1800>>
							fHeading = 		228.998
							weapon = 		WEAPONTYPE_ASSAULTSHOTGUN
							sScenario = 		"CODE_HUMAN_MEDIC_TEND_TO_DEAD"
						BREAK			
						CASE 8			
							model = 		A_F_Y_Business_04
							vCoords = 		<<313.7840, -281.0400, 53.1650>>
							fHeading = 		354.398
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 9			
							model = 		mp_g_m_pros_01
							vCoords = 		<<311.6120, -282.9536, 53.1647>>
							fHeading = 		88.3979
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK		
					ENDSWITCH				
				BREAK
				CASE CHS_VD2_VARIATION_2
					SWITCH iPed				
						CASE 0			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<-2973.4131, 486.3880, 14.2800>>
							fHeading = 		292.799
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 1			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<-2975.5569, 481.9200, 14.2570>>
							fHeading = 		268.399
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 2			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<-2972.5891, 479.0380, 14.3960>>
							fHeading = 		292.399
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 3			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<-2972.1750, 474.0860, 14.3900>>
							fHeading = 		322.999
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 4			
							model = 		mp_g_m_pros_01
							vCoords = 		<<-2966.9590, 493.1280, 14.3060>>
							fHeading = 		136.598
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		""
						BREAK			
						CASE 5			
							model = 		mp_g_m_pros_01
							vCoords = 		<<-2964.6760, 484.0480, 14.6970>>
							fHeading = 		102.398
							weapon = 		WEAPONTYPE_ASSAULTSHOTGUN
							sScenario = 		""
						BREAK			
						CASE 6			
							model = 		mp_g_m_pros_01
							vCoords = 		<<-2962.8960, 480.5320, 14.7120>>
							fHeading = 		75.198
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		""
						BREAK			
						CASE 7			
							model = 		mp_g_m_pros_01
							vCoords = 		<<-2957.7361, 480.9300, 14.7120>>
							fHeading = 		53.198
							weapon = 		WEAPONTYPE_ASSAULTSHOTGUN
							sScenario = 		"CODE_HUMAN_MEDIC_TEND_TO_DEAD"
						BREAK			
						CASE 8			
							model = 		A_F_Y_Business_04
							vCoords = 		<<-2960.5908, 482.8255, 14.6970>>
							fHeading = 		105.7998
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 9			
							model = 		mp_g_m_pros_01
							vCoords = 		<<-2957.0320, 481.6078, 14.6970>>
							fHeading = 		152.3978
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_VALET_SERVICE
			SWITCH eSubvariation
				CASE CHS_VS_VARIATION_1
					SWITCH iPed				
						CASE 0			
							model = 		S_M_Y_Valet_01
							vCoords = 		<<-1611.7250, -506.2200, 34.5900>>
							fHeading = 		354.399
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 1			
							model = 		S_M_Y_Valet_01
							vCoords = 		<<-1611.6970, -505.1780, 34.6450>>
							fHeading = 		176.398
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 2			
							model = 		S_M_Y_Valet_01
							vCoords = 		<<-1621.6670, -510.3110, 34.4370>>
							fHeading = 		228.998
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 3			
							model = 		A_M_Y_Vinewood_03
							vCoords = 		<<-1507.9821, -695.6620, 29.2740>>
							fHeading = 		44.998
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 4			
							model = 		A_M_Y_VinDouche_01
							vCoords = 		<<-1334.2115, -519.4623, 31.3541>>
							fHeading = 		124.3998
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 5			
							model = 		S_M_M_Bouncer_01
							vCoords = 		<<-1662.2769, -532.5541, 35.0290>>
							fHeading = 		140.5992
							weapon = 		WEAPONTYPE_DLC_PISTOL50
							sScenario = 		"WORLD_HUMAN_GUARD_STAND"
						BREAK			
						CASE 6			
							model = 		S_M_M_Bouncer_01
							vCoords = 		<<-1659.9404, -536.4473, 34.0984>>
							fHeading = 		140.5992
							weapon = 		WEAPONTYPE_DLC_PISTOL50
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK			
						CASE 7			
							model = 		A_F_Y_Hipster_02
							vCoords = 		<<-1663.8107, -537.9222, 34.2025>>
							fHeading = 		200.1989
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 8			
							model = 		A_F_Y_Hipster_02
							vCoords = 		<<-1663.6176, -538.7332, 34.1369>>
							fHeading = 		29.1987
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 9			
							model = 		S_M_Y_Waiter_01
							vCoords = 		<<-1670.4060, -534.5148, 34.7744>>
							fHeading = 		234.1985
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
					ENDSWITCH				
				BREAK
				
				CASE CHS_VS_VARIATION_2
					SWITCH iPed				
						CASE 0			
							model = 		S_M_Y_Valet_01
							vCoords = 		<<-672.7210, -2232.3770, 4.9520>>
							fHeading = 		156.399
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 1			
							model = 		S_M_Y_Valet_01
							vCoords = 		<<-673.0740, -2233.4700, 4.9520>>
							fHeading = 		337.199
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 2			
							model = 		S_M_Y_Valet_01
							vCoords = 		<<-684.7530, -2233.9500, 4.9500>>
							fHeading = 		271.798
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 3			
							model = 		A_M_Y_Vinewood_03
							vCoords = 		<<-564.7140, -2070.7251, 7.2930>>
							fHeading = 		223.2
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 4			
							model = 		A_M_Y_VinDouche_01
							vCoords = 		<<-177.6185, -2181.6375, 11.2682>>
							fHeading = 		108.5998
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 5			
							model = 		S_M_M_Bouncer_01
							vCoords = 		<<-706.1904, -2277.9592, 12.4604>>
							fHeading = 		235.5993
							weapon = 		WEAPONTYPE_DLC_PISTOL50
							sScenario = 		"WORLD_HUMAN_GUARD_STAND"
						BREAK			
						CASE 6			
							model = 		S_M_M_Bouncer_01
							vCoords = 		<<-701.3764, -2273.1553, 12.4604>>
							fHeading = 		229.9993
							weapon = 		WEAPONTYPE_DLC_PISTOL50
							sScenario = 		"WORLD_HUMAN_GUARD_STAND"
						BREAK			
						CASE 7			
							model = 		A_F_Y_Hipster_02
							vCoords = 		<<-697.9911, -2281.3977, 12.0602>>
							fHeading = 		110.5991
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 8			
							model = 		A_F_Y_Hipster_02
							vCoords = 		<<-699.0630, -2281.6768, 12.0601>>
							fHeading = 		291.9986
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 9			
							model = 		S_M_Y_Waiter_01
							vCoords = 		<<-690.6028, -2280.0674, 12.0342>>
							fHeading = 		49.1982
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
					ENDSWITCH				
				BREAK
				
				CASE CHS_VS_VARIATION_3
					SWITCH iPed				
						CASE 0			
							model = 		S_M_Y_Valet_01
							vCoords = 		<<-512.0820, -452.7690, 33.2730>>
							fHeading = 		33.199
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 1			
							model = 		S_M_Y_Valet_01
							vCoords = 		<<-507.0200, -446.4580, 33.2010>>
							fHeading = 		117.399
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 2			
							model = 		S_M_Y_Valet_01
							vCoords = 		<<-507.9080, -447.0000, 33.2010>>
							fHeading = 		298.398
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 3			
							model = 		A_M_Y_Vinewood_03
							vCoords = 		<<-397.8300, -380.3130, 33.5600>>
							fHeading = 		83.2
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 4			
							model = 		A_M_Y_VinDouche_01
							vCoords = 		<<-936.2927, -257.4178, 40.5456>>
							fHeading = 		241.7998
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 5			
							model = 		S_M_M_Bouncer_01
							vCoords = 		<<-565.3585, -441.9904, 33.3533>>
							fHeading = 		183.3992
							weapon = 		WEAPONTYPE_DLC_PISTOL50
							sScenario = 		"WORLD_HUMAN_GUARD_STAND"
						BREAK			
						CASE 6			
							model = 		S_M_M_Bouncer_01
							vCoords = 		<<-575.4048, -441.8575, 33.3552>>
							fHeading = 		182.7992
							weapon = 		WEAPONTYPE_DLC_PISTOL50
							sScenario = 		"WORLD_HUMAN_GUARD_STAND"
						BREAK			
						CASE 7			
							model = 		A_F_Y_Hipster_02
							vCoords = 		<<-569.9623, -443.2910, 33.3248>>
							fHeading = 		242.7991
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 8			
							model = 		A_F_Y_Hipster_02
							vCoords = 		<<-569.1341, -443.9207, 33.3112>>
							fHeading = 		57.999
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 9			
							model = 		S_M_Y_Waiter_01
							vCoords = 		<<-583.3883, -446.1967, 33.2881>>
							fHeading = 		308.1986
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_VAULT_CONTENTS
			RETURN GET_VAULT_CONTENTS_PED_DATA(iPed, eSubvariation, iExtraParam)
		CASE CHV_STEALTH_OUTFITS
			SWITCH eSubvariation
				CASE CHS_SO_VARIATION_1
					SWITCH iPed				
						CASE 0			
							model = 		S_M_Y_BLACKOPS_02
							vCoords = 		<<3568.6389, 3660.2100, 32.9280>>
							fHeading = 		70.799
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 1			
							model = 		S_M_Y_BLACKOPS_02
							vCoords = 		<<3567.4180, 3660.7080, 32.9790>>
							fHeading = 		240.399
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 2			
							model = 		S_M_Y_BLACKOPS_02
							vCoords = 		<<3573.6641, 3667.1150, 32.8890>>
							fHeading = 		17.998
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 3			
							model = 		S_M_Y_BLACKOPS_02
							vCoords = 		<<3577.0110, 3660.0869, 32.8990>>
							fHeading = 		236.198
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_MEDIC_TEND_TO_DEAD"
						BREAK			
						CASE 4			
							model = 		S_M_Y_BLACKOPS_02
							vCoords = 		<<3568.9319, 3666.6360, 32.8910>>
							fHeading = 		255.998
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 5			
							model = 		S_M_Y_BLACKOPS_02
							vCoords = 		<<3494.2019, 3668.2900, 32.8880>>
							fHeading = 		83.798
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 6			
							model = 		S_M_Y_BLACKOPS_02
							vCoords = 		<<3572.3630, 3685.6260, 40.0070>>
							fHeading = 		350.798
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 7			
							model = 		S_M_Y_BLACKOPS_02
							vCoords = 		<<3560.5759, 3646.1140, 40.3400>>
							fHeading = 		78.397
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 8			
							model = 		S_M_Y_BLACKOPS_02
							vCoords = 		<<3439.8181, 3669.0120, 40.3400>>
							fHeading = 		81.397
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 9			
							model = 		S_M_Y_BLACKOPS_02
							vCoords = 		<<3584.6140, 3683.3359, 40.0070>>
							fHeading = 		141.397
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK			
						CASE 10			
							model = 		S_M_Y_BLACKOPS_02
							vCoords = 		<<3427.6328, 3761.6511, 29.6426>>
							fHeading = 		213.3978
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_GUARD_STAND"
						BREAK			
						CASE 11			
							model = 		S_M_Y_BLACKOPS_02
							vCoords = 		<<3493.7192, 3678.5642, 34.4625>>
							fHeading = 		269.6023
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 12			
							model = 		S_M_Y_BLACKOPS_02
							vCoords = 		<<3475.2830, 3669.9214, 34.9972>>
							fHeading = 		173.1977
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 13			
							model = 		S_M_Y_BLACKOPS_02
							vCoords = 		<<3450.9934, 3721.3318, 35.6427>>
							fHeading = 		80.1974
							weapon = 		WEAPONTYPE_SNIPERRIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 14			
							model = 		S_M_Y_BLACKOPS_02
							vCoords = 		<<3468.7041, 3708.3535, 35.5854>>
							fHeading = 		166.197
							weapon = 		WEAPONTYPE_SNIPERRIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 15			
							model = 		S_M_Y_BLACKOPS_02
							vCoords = 		<<3534.0256, 3679.7822, 44.2328>>
							fHeading = 		169.1969
							weapon = 		WEAPONTYPE_SNIPERRIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 16			
							model = 		S_M_Y_BLACKOPS_02
							vCoords = 		<<3436.0598, 3677.2759, 40.3415>>
							fHeading = 		316.1964
							weapon = 		WEAPONTYPE_SNIPERRIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 17			
							model = 		S_M_Y_BLACKOPS_02
							vCoords = 		<<3441.6924, 3705.5132, 30.8448>>
							fHeading = 		170.4018
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			

					ENDSWITCH				
				BREAK
				
				CASE CHS_SO_VARIATION_2
					SWITCH iPed				
						CASE 0			
							model = 		S_M_Y_BLACKOPS_02
							vCoords = 		<<-1770.7190, 3096.9260, 31.8010>>
							fHeading = 		129.397
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 1			
							model = 		S_M_Y_BLACKOPS_02
							vCoords = 		<<-1771.9100, 3095.9790, 31.8010>>
							fHeading = 		304.397
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 2			
							model = 		S_M_Y_BLACKOPS_02
							vCoords = 		<<-1765.0510, 3098.5540, 31.8060>>
							fHeading = 		328.197
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 3			
							model = 		S_M_Y_BLACKOPS_02
							vCoords = 		<<-1775.3580, 3104.3879, 31.8010>>
							fHeading = 		333.197
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK			
						CASE 4			
							model = 		S_M_Y_BLACKOPS_02
							vCoords = 		<<-1770.8190, 3092.4390, 31.8040>>
							fHeading = 		238.996
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 5			
							model = 		S_M_Y_BLACKOPS_02
							vCoords = 		<<-1778.0890, 3066.8850, 31.8110>>
							fHeading = 		238.996
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 6			
							model = 		S_M_Y_BLACKOPS_02
							vCoords = 		<<-1791.4700, 3087.5520, 31.8100>>
							fHeading = 		110.196
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 7			
							model = 		S_M_Y_BLACKOPS_02
							vCoords = 		<<-1781.4091, 3093.4331, 31.8020>>
							fHeading = 		200.995
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_MEDIC_TEND_TO_DEAD"
						BREAK			
						CASE 8			
							model = 		S_M_Y_BLACKOPS_02
							vCoords = 		<<-1779.2900, 3081.1899, 31.8050>>
							fHeading = 		331.995
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 9			
							model = 		S_M_Y_BLACKOPS_02
							vCoords = 		<<-1766.9080, 3084.8789, 31.8120>>
							fHeading = 		42.195
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK		
						CASE 10			
							model = 		S_M_Y_BLACKOPS_02
							vCoords = 		<<-1758.3929, 3137.0151, 31.7931>>
							fHeading = 		272.9966
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_GUARD_STAND"
						BREAK			
						CASE 11			
							model = 		S_M_Y_BLACKOPS_02
							vCoords = 		<<-1742.5409, 3110.7922, 41.7229>>
							fHeading = 		111.3962
							weapon = 		WEAPONTYPE_SNIPERRIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 12			
							model = 		S_M_Y_BLACKOPS_02
							vCoords = 		<<-1789.6372, 3122.7141, 41.4589>>
							fHeading = 		258.9956
							weapon = 		WEAPONTYPE_SNIPERRIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 13			
							model = 		S_M_Y_BLACKOPS_02
							vCoords = 		<<-1749.0782, 3097.5300, 33.5080>>
							fHeading = 		258.9956
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 14			
							model = 		S_M_Y_BLACKOPS_02
							vCoords = 		<<-1751.1597, 3054.9841, 34.5157>>
							fHeading = 		258.9956
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 15			
							model = 		S_M_Y_BLACKOPS_02
							vCoords = 		<<-1757.7235, 3095.1050, 31.8163>>
							fHeading = 		5.1952
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 16			
							model = 		S_M_Y_BLACKOPS_02
							vCoords = 		<<-1732.1771, 3019.1272, 36.6500>>
							fHeading = 		40.7989
							weapon = 		WEAPONTYPE_SNIPERRIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 17			
							model = 		S_M_Y_BLACKOPS_02
							vCoords = 		<<-1739.1423, 3082.2537, 31.8455>>
							fHeading = 		99.5988
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_GUARD_STAND"
						BREAK			

					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_PLASTIC_EXPLOSIVES
			SWITCH eSubvariation
				CASE CHS_PE_VARIATION_1
					SWITCH iPed				
						CASE 0			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<3824.2190, 4461.6108, 2.3280>>
							fHeading = 		53.999
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 1			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<3823.1250, 4462.3452, 2.3940>>
							fHeading = 		233.398
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 2			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<3866.5220, 4464.3389, 1.7320>>
							fHeading = 		288.198
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_BINOCULARS"
						BREAK			
						CASE 3			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<3857.0891, 4460.0190, 0.8450>>
							fHeading = 		3.798
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_MEDIC_TEND_TO_DEAD"
						BREAK			
						CASE 4			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<3836.3010, 4464.5640, 1.6520>>
							fHeading = 		88.598
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 5			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<3828.8721, 4465.2368, 1.7150>>
							fHeading = 		143.797
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 6			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<3853.5891, 4464.3159, 1.7230>>
							fHeading = 		147.997
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 7			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<3852.9031, 4463.2090, 1.7140>>
							fHeading = 		328.197
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 8			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<4101.2402, 4725.4180, -0.7360>>
							fHeading = 		136.246
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 9			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<4101.2441, 4725.3862, -0.7110>>
							fHeading = 		136.246
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK			
						CASE 10			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<4115.5220, 4699.4780, 1.9950>>
							fHeading = 		136.246
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 11			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<4115.5781, 4699.5991, 1.8930>>
							fHeading = 		136.246
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK			
						CASE 12			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<4107.6392, 4713.1982, 20.2980>>
							fHeading = 		136.246
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 13			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<4107.6392, 4713.1982, 20.2980>>
							fHeading = 		136.246
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK
						CASE 14			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<4093.1450, 4696.4839, 4.0400>>
							fHeading = 		143.797
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 15			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<4093.5840, 4696.4092, 4.0100>>
							fHeading = 		143.797
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK
					ENDSWITCH
				BREAK
				CASE CHS_PE_VARIATION_2
					SWITCH iPed				
						CASE 0			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<-1577.6550, 5169.7930, 18.5750>>
							fHeading = 		147
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 1			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<-1578.4120, 5168.5322, 18.5550>>
							fHeading = 		326.399
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 2			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<-1571.1639, 5182.2451, 16.8800>>
							fHeading = 		21.599
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_BINOCULARS"
						BREAK			
						CASE 3			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<-1581.8340, 5195.7559, 3.0040>>
							fHeading = 		209.399
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 4			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<-1609.2010, 5261.6328, 2.9790>>
							fHeading = 		146.398
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 5			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<-1608.0360, 5248.1172, 2.9790>>
							fHeading = 		258.598
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 6			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<-1606.5341, 5247.8599, 2.9790>>
							fHeading = 		80.398
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 7			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<-1592.9880, 5212.8418, 3.0030>>
							fHeading = 		307.397
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK			
						CASE 8			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<-1824.4260, 5657.5542, 2.2000>>
							fHeading = 		203.939
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 9			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<-1824.4170, 5657.5498, 2.2130>>
							fHeading = 		203.939
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK			
						CASE 10			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<-1839.6300, 5672.2891, 2.9500>>
							fHeading = 		203.939
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 11			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<-1839.5610, 5672.2969, 3.0180>>
							fHeading = 		203.939
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK			
						CASE 12			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<-1833.0350, 5664.0078, 13.4040>>
							fHeading = 		203.939
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 13			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<-1833.0350, 5664.0078, 13.4040>>
							fHeading = 		203.939
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK
						CASE 14			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<-1842.3541, 5655.0195, 1.9315>>
							fHeading = 		258.598
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 15			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<-1842.9865, 5654.3779, 2.2421>>
							fHeading = 		258.598
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK
					ENDSWITCH
				BREAK
				CASE CHS_PE_VARIATION_3
					SWITCH iPed				
						CASE 0			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<111.8880, -2257.0391, 5.0870>>
							fHeading = 		309.999
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 1			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<112.9690, -2256.1111, 5.0870>>
							fHeading = 		129.399
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 2			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<115.7770, -2264.7480, 5.0870>>
							fHeading = 		-1.001
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK			
						CASE 3			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<83.6410, -2254.5591, 5.0860>>
							fHeading = 		241.598
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 4			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<84.7640, -2255.3359, 5.0860>>
							fHeading = 		50.398
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 5			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<72.5240, -2257.8560, 5.0860>>
							fHeading = 		131.599
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_BINOCULARS"
						BREAK			
						CASE 6			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<78.9450, -2258.3840, 5.0860>>
							fHeading = 		175.799
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_MEDIC_TEND_TO_DEAD"
						BREAK			
						CASE 7			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<90.2560, -2256.8169, 5.0860>>
							fHeading = 		87.399
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 8			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<-99.7840, -2306.4771, 1.6890>>
							fHeading = 		193.445
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 9			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<-99.7710, -2306.4709, 1.6690>>
							fHeading = 		193.445
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK			
						CASE 10			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<-71.5600, -2313.1689, 1.5730>>
							fHeading = 		193.445
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 11			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<-71.3220, -2313.0439, 1.2800>>
							fHeading = 		193.445
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK			
						CASE 12			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<-84.6380, -2310.7820, 14.7510>>
							fHeading = 		193.445
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 13			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<-84.6380, -2310.7820, 14.7510>>
							fHeading = 		193.445
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK
						CASE 14			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<-83.3113, -2295.1296, 1.2149>>
							fHeading = 		87.399
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 15			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<-83.3318, -2295.1282, 1.1904>>
							fHeading = 		87.399
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK
					ENDSWITCH
				BREAK
				CASE CHS_PE_VARIATION_4
					SWITCH iPed				
						CASE 0			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<724.6170, 4170.9902, 39.7090>>
							fHeading = 		121.6
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 1			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<723.5650, 4170.3242, 39.7090>>
							fHeading = 		301.799
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 2			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<720.9180, 4157.5210, 37.2850>>
							fHeading = 		196.999
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_BINOCULARS"
						BREAK			
						CASE 3			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<714.1230, 4152.7129, 34.7840>>
							fHeading = 		0.198
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 4			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<714.4390, 4121.1172, 34.7840>>
							fHeading = 		34.398
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 5			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<713.4290, 4122.2920, 34.7840>>
							fHeading = 		222.198
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 6			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<711.7830, 4093.7881, 33.7330>>
							fHeading = 		94.198
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_MEDIC_TEND_TO_DEAD"
						BREAK			
						CASE 7			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<714.2570, 4098.1831, 34.7900>>
							fHeading = 		273.598
							weapon = 		WEAPONTYPE_CARBINERIFLE
							sScenario = 		"WORLD_HUMAN_WINDOW_SHOP_BROWSE"
						BREAK			
						CASE 8			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<355.7300, 3946.5129, 31.7860>>
							fHeading = 		241.396
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 9			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<355.7960, 3946.5271, 31.6970>>
							fHeading = 		241.396
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 10			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<337.6760, 3957.6970, 30.6150>>
							fHeading = 		241.396
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 11			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<336.6260, 3958.2900, 31.6900>>
							fHeading = 		241.396
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 12			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<352.9000, 3956.8030, 43.4910>>
							fHeading = 		241.396
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 13			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<353.0370, 3956.7891, 43.5590>>
							fHeading = 		241.396
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK
						CASE 14			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<354.1402, 3971.4619, 31.4896>>
							fHeading = 		94.198
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 15			
							model = 		S_M_Y_BLACKOPS_01
							vCoords = 		<<354.0485, 3970.9373, 31.2380>>
							fHeading = 		94.198
							weapon = 		WEAPONTYPE_MICROSMG
							sScenario = 		""
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_INSIDE_VALET	
			SWITCH eSubvariation
				CASE CHS_IMV_VARIATION_1
					SWITCH iPed				
						CASE 0			
							model = 		S_M_M_StrPreach_01
							vCoords = 		<<-1692.6969, -290.7501, 50.8833>>
							fHeading = 		262
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 1			
							model = 		A_F_O_GenStreet_01
							vCoords = 		<<-1691.5249, -291.0629, 50.8833>>
							fHeading = 		82.399
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 2			
							model = 		A_F_Y_Business_01
							vCoords = 		<<265.5160, -1357.7040, 23.5380>>
							fHeading = 		340
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_PROSTITUTE_HIGH_CLASS"
						BREAK			
						CASE 3			
							model = 		S_M_M_Paramedic_01
							vCoords = 		<<266.4830, -1355.9980, 23.5380>>
							fHeading = 		152.799
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 4			
							model = 		S_M_M_Paramedic_01
							vCoords = 		<<294.7525, -1352.0780, 23.5378>>
							fHeading = 		164.9996
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 5			
							model = 		S_M_M_Scientist_01
							vCoords = 		<<281.6482, -1347.6028, 23.5378>>
							fHeading = 		104.9994
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK			
						CASE 6			
							model = 		S_M_M_Paramedic_01
							vCoords = 		<<280.5379, -1348.0228, 23.5378>>
							fHeading = 		284.3989
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 7			
							model = 		S_M_M_Scientist_01
							vCoords = 		<<263.6818, -1337.7336, 23.5378>>
							fHeading = 		326.3987
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_WINDOW_SHOP_BROWSE"
						BREAK			
						CASE 8			
							model = 		S_M_M_Paramedic_01
							vCoords = 		<<272.5207, -1341.2748, 23.5378>>
							fHeading = 		139.7981
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 9			
							model = 		S_M_M_Scientist_01
							vCoords = 		<<252.0774, -1351.8857, 23.5378>>
							fHeading = 		194.3979
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 10			
							model = 		S_M_M_Paramedic_01
							vCoords = 		<<252.4672, -1353.0100, 23.5378>>
							fHeading = 		10.7977
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 11			
							model = 		S_M_M_Scientist_01
							vCoords = 		<<283.3123, -1338.0422, 23.5378>>
							fHeading = 		229.1973
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 12			
							model = 		S_M_M_Security_01
							vCoords = 		<<256.5058, -1357.7915, 23.5378>>
							fHeading = 		231.9973
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK			
						CASE 13			
							model = 		S_M_M_Scientist_01
							vCoords = 		<<261.5110, -1342.2437, 23.5378>>
							fHeading = 		320.5969
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 14			
							model = 		S_M_M_Janitor
							vCoords = 		<<264.5140, -1349.7758, 23.5378>>
							fHeading = 		66.7963
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"CODE_HUMAN_MEDIC_TEND_TO_DEAD"
						BREAK			
						CASE 15			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("u_m_y_corpse_01")) 
							vCoords = 		<<291.4934, -1345.9064, 24.5740>>
							fHeading = 		149.996
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 16			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("u_m_y_corpse_01"))
							vCoords = 		<<294.37, -1352.9164, 24.5831>>
							fHeading = 		-121.45
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 17			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("u_m_y_corpse_01"))
							vCoords = 		<<284.2056, -1338.3494, 24.5831>>
							fHeading = 		157.7954
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK		
						CASE 18			
							model = 		S_M_M_Security_01
							vCoords = 		<<244.0110, -1367.4497, 23.5378>>
							fHeading = 		326.1996
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_GUARD_STAND"
						BREAK			
						CASE 19
							model = 		A_M_Y_Business_01
							vCoords = 		<<-1688.9951, -296.1086, 50.8121>>
							fHeading = 		232.3996
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 	"WORLD_HUMAN_SMOKING"
						BREAK
					ENDSWITCH				
				BREAK
				CASE CHS_IMV_VARIATION_2
					SWITCH iPed				
						CASE 0			
							model = 		S_M_M_StrPreach_01
							vCoords = 		<<-1654.8341, -292.8372, 50.7348>>
							fHeading = 		92.7996
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 1			
							model = 		A_F_O_GenStreet_01
							vCoords = 		<<-1655.8207, -292.8655, 50.7473>>
							fHeading = 		279.9985
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 2			
							model = 		A_F_Y_Business_01
							vCoords = 		<<265.5160, -1357.7040, 23.5380>>
							fHeading = 		340
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_PROSTITUTE_HIGH_CLASS"
						BREAK			
						CASE 3			
							model = 		S_M_M_Paramedic_01
							vCoords = 		<<266.4830, -1355.9980, 23.5380>>
							fHeading = 		152.799
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 4			
							model = 		S_M_M_Paramedic_01
							vCoords = 		<<294.7525, -1352.0780, 23.5378>>
							fHeading = 		164.9996
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 5			
							model = 		S_M_M_Scientist_01
							vCoords = 		<<281.6482, -1347.6028, 23.5378>>
							fHeading = 		104.9994
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK			
						CASE 6			
							model = 		S_M_M_Paramedic_01
							vCoords = 		<<280.5379, -1348.0228, 23.5378>>
							fHeading = 		284.3989
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 7			
							model = 		S_M_M_Scientist_01
							vCoords = 		<<263.6818, -1337.7336, 23.5378>>
							fHeading = 		326.3987
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_WINDOW_SHOP_BROWSE"
						BREAK			
						CASE 8			
							model = 		S_M_M_Paramedic_01
							vCoords = 		<<272.5207, -1341.2748, 23.5378>>
							fHeading = 		139.7981
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 9			
							model = 		S_M_M_Scientist_01
							vCoords = 		<<252.0774, -1351.8857, 23.5378>>
							fHeading = 		194.3979
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 10			
							model = 		S_M_M_Paramedic_01
							vCoords = 		<<252.4672, -1353.0100, 23.5378>>
							fHeading = 		10.7977
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 11			
							model = 		S_M_M_Scientist_01
							vCoords = 		<<283.3123, -1338.0422, 23.5378>>
							fHeading = 		229.1973
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 12			
							model = 		S_M_M_Security_01
							vCoords = 		<<256.5058, -1357.7915, 23.5378>>
							fHeading = 		231.9973
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK			
						CASE 13			
							model = 		S_M_M_Scientist_01
							vCoords = 		<<261.5110, -1342.2437, 23.5378>>
							fHeading = 		320.5969
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 14			
							model = 		S_M_M_Janitor
							vCoords = 		<<264.5140, -1349.7758, 23.5378>>
							fHeading = 		66.7963
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"CODE_HUMAN_MEDIC_TEND_TO_DEAD"
						BREAK			
						CASE 15			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("u_m_y_corpse_01"))
							vCoords = 		<<291.4934, -1345.9064, 24.5740>>
							fHeading = 		149.996
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 16			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("u_m_y_corpse_01"))
							vCoords = 		<<294.37, -1352.9164, 24.5831>>
							fHeading = 		-121.45
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 17			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("u_m_y_corpse_01"))
							vCoords = 		<<284.2056, -1338.3494, 24.5831>>
							fHeading = 		157.7954
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK		
						CASE 18			
							model = 		S_M_M_Security_01
							vCoords = 		<<244.0110, -1367.4497, 23.5378>>
							fHeading = 		326.1996
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_GUARD_STAND"
						BREAK			
						CASE 19
							model = 		A_M_Y_Business_01
							vCoords = 		<<-1659.6351, -287.7571, 50.8833>>
							fHeading = 		235.1993
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 	"WORLD_HUMAN_SMOKING"
						BREAK
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_ELECTRIC_DRILLS_1
			SWITCH eSubvariation
				CASE CHS_ED1_VARIATION_1
					SWITCH iPed				
						CASE 0			
							model = 		S_M_Y_Construct_01
							vCoords = 		<<-159.6670, -1096.5070, 41.1390>>
							fHeading = 		250.397
							weapon = 		WEAPONTYPE_DLC_WRENCH
							sScenario = 		"WORLD_HUMAN_WELDING"
						BREAK			
						CASE 1			
							model = 		S_M_Y_Construct_01
							vCoords = 		<<-180.4825, -1069.4579, 41.1393>>
							fHeading = 		306.1967
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 2			
							model = 		S_M_Y_Construct_01
							vCoords = 		<<-179.3471, -1068.5991, 41.1393>>
							fHeading = 		130.9968
							weapon = 		WEAPONTYPE_DLC_WRENCH
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 3			
							model = 		S_M_Y_Construct_01
							vCoords = 		<<-153.2850, -1076.9530, 41.1390>>
							fHeading = 		255.597
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 4			
							model = 		S_M_Y_Construct_01
							vCoords = 		<<-168.1800, -1062.4160, 41.1390>>
							fHeading = 		161.397
							weapon = 		WEAPONTYPE_HAMMER
							sScenario = 		"WORLD_HUMAN_HAMMERING"
						BREAK			
						CASE 5			
							model = 		S_M_Y_Construct_01
							vCoords = 		<<-153.3240, -1087.9189, 41.1390>>
							fHeading = 		162.795
							weapon = 		WEAPONTYPE_DLC_WRENCH
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 6			
							model = 		S_M_Y_Construct_01
							vCoords = 		<<-159.8070, -1084.4041, 41.1390>>
							fHeading = 		339.795
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 7			
							model = 		S_M_Y_Construct_01
							vCoords = 		<<-180.0150, -1056.2600, 41.1750>>
							fHeading = 		343.994
							weapon = 		WEAPONTYPE_HAMMER
							sScenario = 		"WORLD_HUMAN_SEAT_LEDGE_EATING"
						BREAK			
						CASE 8			
							model = 		S_M_Y_Construct_01
							vCoords = 		<<-155.0000, -1072.1370, 41.1390>>
							fHeading = 		192.794
							weapon = 		WEAPONTYPE_HAMMER
							sScenario = 		"CODE_HUMAN_MEDIC_TEND_TO_DEAD"
						BREAK			
						CASE 9			
							model = 		S_M_M_Security_01
							vCoords = 		<<-203.2728, -1098.5194, 20.7420>>
							fHeading = 		163.3984
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_PATROL_1H"
						BREAK					
					ENDSWITCH								
				BREAK
				CASE CHS_ED1_VARIATION_2
					SWITCH iPed				
						CASE 0			
							model = 		S_M_Y_Construct_01
							vCoords = 		<<-1110.4561, -966.0960, 1.3490>>
							fHeading = 		95.598
							weapon = 		WEAPONTYPE_HAMMER
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 1			
							model = 		S_M_Y_Construct_01
							vCoords = 		<<-1130.1730, -957.0910, 1.1500>>
							fHeading = 		331.397
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 2			
							model = 		S_M_Y_Construct_01
							vCoords = 		<<-1129.3370, -955.7070, 1.1500>>
							fHeading = 		148.997
							weapon = 		WEAPONTYPE_DLC_WRENCH
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 3			
							model = 		S_M_Y_Construct_01
							vCoords = 		<<-1118.6940, -963.9970, 1.1500>>
							fHeading = 		116.797
							weapon = 		WEAPONTYPE_DLC_WRENCH
							sScenario = 		"WORLD_HUMAN_WELDING"
						BREAK			
						CASE 4			
							model = 		S_M_Y_Construct_01
							vCoords = 		<<-1129.8870, -962.7350, 1.1500>>
							fHeading = 		38.196
							weapon = 		WEAPONTYPE_HAMMER
							sScenario = 		"WORLD_HUMAN_HAMMERING"
						BREAK			
						CASE 5			
							model = 		S_M_Y_Construct_01
							vCoords = 		<<-1126.8322, -966.7576, 5.6321>>
							fHeading = 		300.1628
							weapon = 		WEAPONTYPE_DLC_WRENCH
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 6			
							model = 		S_M_Y_Construct_01
							vCoords = 		<<-1122.7900, -959.8670, 5.6320>>
							fHeading = 		318.596
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_SEAT_LEDGE_EATING"
						BREAK			
						CASE 7			
							model = 		S_M_Y_Construct_01
							vCoords = 		<<-1121.5590, -958.4850, 1.1500>>
							fHeading = 		308.195
							weapon = 		WEAPONTYPE_HAMMER
							sScenario = 		"CODE_HUMAN_MEDIC_TEND_TO_DEAD"
						BREAK			
						CASE 8			
							model = 		S_M_Y_Construct_01
							vCoords = 		<<-1131.2390, -957.3750, 5.6320>>
							fHeading = 		227.194
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK			
						CASE 9			
							model = 		S_M_M_Security_01
							vCoords = 		<<-1103.7274, -971.7468, 1.1502>>
							fHeading = 		300.1987
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_PATROL_1H"
						BREAK								
					ENDSWITCH								
				BREAK
				CASE CHS_ED1_VARIATION_3
					SWITCH iPed				
						CASE 0			
							model = 		S_M_Y_Construct_01
							vCoords = 		<<1301.7527, -745.9771, 64.5834>>
							fHeading = 		25.8579
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 1			
							model = 		S_M_Y_Construct_01
							vCoords = 		<<1301.3207, -744.8085, 64.5834>>
							fHeading = 		201.6581
							weapon = 		WEAPONTYPE_DLC_WRENCH
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 2			
							model = 		S_M_Y_Construct_01
							vCoords = 		<<1300.4301, -734.0350, 63.7500>>
							fHeading = 		174.458
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 3			
							model = 		S_M_Y_Construct_01
							vCoords = 		<<1332.6350, -747.0790, 65.8930>>
							fHeading = 		32.457
							weapon = 		WEAPONTYPE_HAMMER
							sScenario = 		"CODE_HUMAN_MEDIC_TEND_TO_DEAD"
						BREAK			
						CASE 4			
							model = 		S_M_Y_Construct_01
							vCoords = 		<<1299.3210, -749.9570, 64.5830>>
							fHeading = 		248.857
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 5			
							model = 		S_M_Y_Construct_01
							vCoords = 		<<1314.8690, -756.4850, 65.4150>>
							fHeading = 		75.857
							weapon = 		WEAPONTYPE_DLC_WRENCH
							sScenario = 		""
						BREAK			
						CASE 6			
							model = 		S_M_Y_Construct_01
							vCoords = 		<<1322.7489, -750.2767, 64.5996>>
							fHeading = 		258.2552
							weapon = 		WEAPONTYPE_HAMMER
							sScenario = 		"CODE_HUMAN_MEDIC_TEND_TO_DEAD"
						BREAK			
						CASE 7			
							model = 		S_M_Y_Construct_01
							vCoords = 		<<1300.6340, -740.7726, 63.6512>>
							fHeading = 		156.656
							weapon = 		WEAPONTYPE_HAMMER
							sScenario = 		"WORLD_HUMAN_HAMMERING"
						BREAK			
						CASE 8			
							model = 		S_M_Y_Construct_01
							vCoords = 		<<1322.7542, -766.4131, 65.4148>>
							fHeading = 		345.0543
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 9			
							model = 		S_M_M_Security_01
							vCoords = 		<<1351.6938, -744.6300, 66.1903>>
							fHeading = 		258.7975
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_PATROL_1H"
						BREAK						
					ENDSWITCH								
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_GUARD_PATROL_ROUTES
			SWITCH eSubvariation
				CASE CHS_GP_VARIATION_1
					SWITCH iPed				
						CASE 0			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_02"))
							vCoords = 		<<-394.4004, 1228.5613, 324.6416>>
							fHeading = 		16.1955
							weapon = 		WEAPONTYPE_ASSAULTSHOTGUN
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 1			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_02"))
							vCoords = 		<<-395.3940, 1229.4033, 324.6416>>
							fHeading = 		9.5959
							weapon = 		WEAPONTYPE_ASSAULTSHOTGUN
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 2			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_01"))
							vCoords = 		<<-394.0607, 1188.8871, 324.6416>>
							fHeading = 		100.1957
							weapon = 		WEAPONTYPE_DLC_SPECIALCARBINE_MK2
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK			
						CASE 3			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_02"))
							vCoords = 		<<-414.5060, 1199.2791, 324.6420>>
							fHeading = 		161.595
							weapon = 		WEAPONTYPE_DLC_SPECIALCARBINE_MK2
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 4			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_02"))
							vCoords = 		<<-388.7260, 1218.4760, 324.6420>>
							fHeading = 		340.795
							weapon = 		WEAPONTYPE_DLC_SPECIALCARBINE_MK2
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 5			
							model = 		S_M_M_HighSec_01
							vCoords = 		<<-396.4819, 1230.4559, 324.6416>>
							fHeading = 		219.7934
							weapon = 		WEAPONTYPE_SMG
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 6			
							model = 		S_M_M_HighSec_01
							vCoords = 		<<-388.9728, 1232.7249, 324.6639>>
							fHeading = 		315.1935
							weapon = 		WEAPONTYPE_SMG
							sScenario = 		"WORLD_HUMAN_GUARD_STAND"
						BREAK			
						CASE 7			
							model = 		S_M_M_HighSec_01
							vCoords = 		<<-395.3733, 1231.2190, 324.6416>>
							fHeading = 		175.1937
							weapon = 		WEAPONTYPE_COMBATPISTOL
							sScenario = 		"CODE_HUMAN_PATROL_1H"
						BREAK			
						CASE 8			
							model = 		S_M_M_HighSec_01
							vCoords = 		<<-381.0791, 1213.8595, 324.6416>>
							fHeading = 		223.5928
							weapon = 		WEAPONTYPE_COMBATPISTOL
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 9			
							model = 		S_M_M_HighSec_01
							vCoords = 		<<-416.0226, 1227.8524, 324.6416>>
							fHeading = 		142.5929
							weapon = 		WEAPONTYPE_SMG
							sScenario = 		"WORLD_HUMAN_GUARD_STAND"
						BREAK			
						CASE 10			
							model = 		S_M_M_HighSec_01
							vCoords = 		<<-400.8593, 1236.3958, 324.6666>>
							fHeading = 		91.9926
							weapon = 		WEAPONTYPE_SMG
							sScenario = 		"WORLD_HUMAN_GUARD_STAND"
						BREAK			
						CASE 11			
							model = 		S_M_M_HighSec_01
							vCoords = 		<<-423.9005, 1213.8126, 324.7585>>
							fHeading = 		259.7921
							weapon = 		WEAPONTYPE_COMBATPISTOL
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK			
						CASE 12			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_01"))
							vCoords = 		<<-402.9070, 1220.5123, 324.6416>>
							fHeading = 		329.7944
							weapon = 		WEAPONTYPE_ASSAULTSHOTGUN
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 13			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_01"))
							vCoords = 		<<-394.8968, 1208.9413, 324.6415>>
							fHeading = 		259.9937
							weapon = 		WEAPONTYPE_ASSAULTSHOTGUN
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK						
					ENDSWITCH				
				BREAK
				CASE CHS_GP_VARIATION_2
					SWITCH iPed				
						CASE 0			
							model = 		S_M_M_HighSec_01
							vCoords = 		<<-815.9691, -748.5759, 22.1584>>
							fHeading = 		99.7956
							weapon = 		WEAPONTYPE_COMBATPISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 1			
							model = 		S_M_M_HighSec_01
							vCoords = 		<<-817.2044, -748.1785, 22.1983>>
							fHeading = 		110.7955
							weapon = 		WEAPONTYPE_COMBATPISTOL
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 2			
							model = 		S_M_M_HighSec_01
							vCoords = 		<<-814.0886, -754.1527, 21.6055>>
							fHeading = 		96.1954
							weapon = 		WEAPONTYPE_SMG
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK			
						CASE 3			
							model = 		S_M_M_HighSec_01
							vCoords = 		<<-817.5076, -772.3126, 19.9733>>
							fHeading = 		178.1951
							weapon = 		WEAPONTYPE_SMG
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 4			
							model = 		S_M_M_HighSec_01
							vCoords = 		<<-828.8356, -751.7772, 21.8394>>
							fHeading = 		133.9945
							weapon = 		WEAPONTYPE_SMG
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 5			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_02"))
							vCoords = 		<<-818.4957, -749.0944, 22.1067>>
							fHeading = 		283.7945
							weapon = 		WEAPONTYPE_ASSAULTSHOTGUN
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 6			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_02"))
							vCoords = 		<<-818.8607, -750.1984, 21.9962>>
							fHeading = 		301.9939
							weapon = 		WEAPONTYPE_ASSAULTSHOTGUN
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 7			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_02"))
							vCoords = 		<<-835.5110, -751.4545, 21.8727>>
							fHeading = 		347.3934
							weapon = 		WEAPONTYPE_DLC_SPECIALCARBINE_MK2
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 8			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_01"))
							vCoords = 		<<-841.8185, -758.9114, 21.2212>>
							fHeading = 		358.3933
							weapon = 		WEAPONTYPE_DLC_SPECIALCARBINE_MK2
							sScenario = 		"WORLD_HUMAN_SMOKING_POT"
						BREAK			
						CASE 9			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_01"))
							vCoords = 		<<-826.1185, -769.9764, 20.1625>>
							fHeading = 		174.5929
							weapon = 		WEAPONTYPE_DLC_SPECIALCARBINE_MK2
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK			
						CASE 10			
							model = 		S_M_M_HighSec_01
							vCoords = 		<<-819.2469, -741.0138, 22.7966>>
							fHeading = 		175.1929
							weapon = 		WEAPONTYPE_COMBATPISTOL
							sScenario = 		"WORLD_HUMAN_GUARD_STAND"
						BREAK			
						CASE 11			
							model = 		S_M_M_HighSec_01
							vCoords = 		<<-811.0562, -745.5544, 22.4834>>
							fHeading = 		322.1924
							weapon = 		WEAPONTYPE_COMBATPISTOL
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 12			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_01"))
							vCoords = 		<<-848.2853, -755.9657, 21.5181>>
							fHeading = 		84.7932
							weapon = 		WEAPONTYPE_ASSAULTSHOTGUN
							sScenario = 		"WORLD_HUMAN_GUARD_STAND"
						BREAK			
						CASE 13			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_02"))
							vCoords = 		<<-829.5856, -752.7479, 21.7434>>
							fHeading = 		315.7928
							weapon = 		WEAPONTYPE_ASSAULTSHOTGUN
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
					ENDSWITCH
				BREAK
				CASE CHS_GP_VARIATION_3
					SWITCH iPed				
						CASE 0			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_02"))
							vCoords = 		<<-343.0366, -945.0983, 30.0806>>
							fHeading = 		296.797
							weapon = 		WEAPONTYPE_ASSAULTSHOTGUN
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 1			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_02"))
							vCoords = 		<<-341.9846, -945.8945, 30.0806>>
							fHeading = 		340.1969
							weapon = 		WEAPONTYPE_ASSAULTSHOTGUN
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 2			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_02"))
							vCoords = 		<<-331.7060, -916.5250, 30.0810>>
							fHeading = 		333.597
							weapon = 		WEAPONTYPE_DLC_SPECIALCARBINE_MK2
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 3			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_01"))
							vCoords = 		<<-322.7680, -962.1660, 30.0810>>
							fHeading = 		153.396
							weapon = 		WEAPONTYPE_DLC_SPECIALCARBINE_MK2
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 4			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_02"))
							vCoords = 		<<-337.3811, -940.4931, 30.0806>>
							fHeading = 		63.9956
							weapon = 		WEAPONTYPE_DLC_SPECIALCARBINE_MK2
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK			
						CASE 5			
							model = 		S_M_M_HighSec_01
							vCoords = 		<<-341.7957, -944.2359, 30.0806>>
							fHeading = 		140.195
							weapon = 		WEAPONTYPE_COMBATPISTOL
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 6			
							model = 		S_M_M_HighSec_01
							vCoords = 		<<-314.2129, -912.1263, 30.0798>>
							fHeading = 		227.9944
							weapon = 		WEAPONTYPE_COMBATPISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 7			
							model = 		S_M_M_HighSec_01
							vCoords = 		<<-313.4216, -929.6177, 30.0806>>
							fHeading = 		261.3938
							weapon = 		WEAPONTYPE_SMG
							sScenario = 		"WORLD_HUMAN_GUARD_STAND"
						BREAK			
						CASE 8			
							model = 		S_M_M_HighSec_01
							vCoords = 		<<-347.9760, -907.9510, 30.0790>>
							fHeading = 		76.994
							weapon = 		WEAPONTYPE_COMBATPISTOL
							sScenario = 		"CODE_HUMAN_PATROL_1H"
						BREAK			
						CASE 9			
							model = 		S_M_M_HighSec_01
							vCoords = 		<<-347.3018, -925.8479, 30.0806>>
							fHeading = 		47.1938
							weapon = 		WEAPONTYPE_SMG
							sScenario = 		"WORLD_HUMAN_GUARD_STAND"
						BREAK			
						CASE 10			
							model = 		S_M_M_HighSec_01
							vCoords = 		<<-351.6156, -948.6747, 30.0806>>
							fHeading = 		185.9947
							weapon = 		WEAPONTYPE_SMG
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 11			
							model = 		S_M_M_HighSec_01
							vCoords = 		<<-329.1168, -933.8895, 30.0806>>
							fHeading = 		164.9946
							weapon = 		WEAPONTYPE_COMBATPISTOL
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK			
						CASE 12			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_01"))
							vCoords = 		<<-341.9293, -954.4040, 30.0806>>
							fHeading = 		71.9964
							weapon = 		WEAPONTYPE_ASSAULTSHOTGUN
							sScenario = 		"WORLD_HUMAN_GUARD_STAND"
						BREAK			
						CASE 13			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_01"))
							vCoords = 		<<-313.3585, -912.9413, 30.0801>>
							fHeading = 		43.1963
							weapon = 		WEAPONTYPE_DLC_SPECIALCARBINE_MK2
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK								
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_POLICE_AUCTION
			SWITCH eSubvariation
				CASE CHS_PA_VARIATION_1
					SWITCH iPed				
						CASE 0			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<830.3521, -1268.9019, 25.2758>>
							fHeading = 		47.6
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 1			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<829.3000, -1267.9869, 25.2736>>
							fHeading = 		225.3996
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 2			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<961.7870, -3002.7515, -40.6349>>
							fHeading = 		38.1467
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 3			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<960.9654, -3001.7769, -40.6349>>
							fHeading = 		217.9467
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 4			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<960.3370, -3011.2759, -40.6470>>
							fHeading = 		2.347
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK						
						CASE 5			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<995.4680, -2988.8440, -40.6470>>
							fHeading = 		9.146
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 6			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<972.0552, -3013.3555, -40.6470>>
							fHeading = 		92.3457
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_PATROL_1H"
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_PA_VARIATION_2
					SWITCH iPed				
						CASE 0			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<-562.4526, -148.3983, 37.0165>>
							fHeading = 		56.3995
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 1			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<-563.6218, -147.6238, 37.0549>>
							fHeading = 		236.5993
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 2			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<961.7870, -3002.7515, -40.6349>>
							fHeading = 		38.1467
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 3			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<960.9654, -3001.7769, -40.6349>>
							fHeading = 		217.9467
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 4			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<960.3370, -3011.2759, -40.6470>>
							fHeading = 		2.347
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK					
						CASE 5			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<995.4680, -2988.8440, -40.6470>>
							fHeading = 		9.146
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 6			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<972.0552, -3013.3555, -40.6470>>
							fHeading = 		92.3457
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_PATROL_1H"
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_GRUPPE_SECHS_VAN_2
			SWITCH eSubvariation
				CASE CHS_GS2_VARIATION_1
					SWITCH iPed				
						CASE 0			
							model = 		S_M_M_Security_01
							vCoords = 		<<972.8080, -1212.5009, 24.5289>>
							fHeading = 		6.5978
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 	"WORLD_HUMAN_GUARD_STAND"
						BREAK			
						CASE 1			
							model = 		S_M_M_Security_01
							vCoords = 		<<994.3610, -1224.4919, 24.2980>>
							fHeading = 		313.798
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 2			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_02"))
							vCoords = 		<<995.3420, -1223.4500, 24.2920>>
							fHeading = 		132.797
							weapon = 		WEAPONTYPE_DLC_SPECIALCARBINE
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 3			
							model = 		S_M_M_Security_01
							vCoords = 		<<986.2110, -1251.1169, 24.5070>>
							fHeading = 		217.397
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_PATROL_1H"
						BREAK			
						CASE 4			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_02"))
							vCoords = 		<<999.9550, -1234.3300, 24.2920>>
							fHeading = 		90.797
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK			
						CASE 5			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_02"))
							vCoords = 		<<990.4400, -1216.3101, 24.3770>>
							fHeading = 		88.797
							weapon = 		WEAPONTYPE_DLC_SPECIALCARBINE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 6			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_02"))
							vCoords = 		<<983.7190, -1224.6689, 24.4080>>
							fHeading = 		33.596
							weapon = 		WEAPONTYPE_DLC_SPECIALCARBINE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 7			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_01"))
							vCoords = 		<<1007.5630, -1222.8430, 24.1390>>
							fHeading = 		268.396
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_WINDOW_SHOP_BROWSE"
						BREAK			
						CASE 8			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_01"))
							vCoords = 		<<1010.2100, -1235.3740, 24.2650>>
							fHeading = 		124.996
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_WELDING"
						BREAK			
						CASE 9			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_01"))
							vCoords = 		<<1017.0240, -1235.6860, 24.2980>>
							fHeading = 		176.195
							weapon = 		WEAPONTYPE_DLC_SPECIALCARBINE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
					ENDSWITCH
				BREAK
				CASE CHS_GS2_VARIATION_2
					SWITCH iPed				
						CASE 0			
							model = 		S_M_M_Security_01
							vCoords = 		<<-690.4870, -596.6210, 24.3080>>
							fHeading = 		297.798
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_PATROL_1H"
						BREAK			
						CASE 1			
							model = 		S_M_M_Security_01
							vCoords = 		<<-676.5660, -606.4110, 24.3080>>
							fHeading = 		297.798
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 2			
							model = 		S_M_M_Security_01
							vCoords = 		<<-668.5420, -628.7840, 24.3060>>
							fHeading = 		169.998
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_PATROL_1H"
						BREAK			
						CASE 3			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_02"))
							vCoords = 		<<-675.2200, -605.5980, 24.3080>>
							fHeading = 		119.398
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 4			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_02"))
							vCoords = 		<<-685.7260, -598.7270, 24.3080>>
							fHeading = 		352.197
							weapon = 		WEAPONTYPE_DLC_SPECIALCARBINE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 5			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_02"))
							vCoords = 		<<-677.0380, -615.5270, 24.3070>>
							fHeading = 		259.597
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_WINDOW_SHOP_BROWSE"
						BREAK			
						CASE 6			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_01"))
							vCoords = 		<<-672.5760, -624.9260, 24.3060>>
							fHeading = 		176.997
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK			
						CASE 7			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_01"))
							vCoords = 		<<-661.6760, -619.5330, 24.3080>>
							fHeading = 		176.997
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_WELDING"
						BREAK			
						CASE 8			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_02"))
							vCoords = 		<<-670.1129, -593.5109, 24.3080>>
							fHeading = 		265.9974
							weapon = 		WEAPONTYPE_DLC_SPECIALCARBINE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 9			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_01"))
							vCoords = 		<<-688.3002, -629.9582, 24.2982>>
							fHeading = 		92.3971
							weapon = 		WEAPONTYPE_DLC_SPECIALCARBINE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_ARMORED_EQUIPMENT_1
			SWITCH iPed				
				CASE 0			
					model = 		S_M_Y_BLACKOPS_01
					vCoords = 		<<876.1334, -3240.6003, -99.0087>>
					fHeading = 		296.598
					weapon = 		WEAPONTYPE_PISTOL
					sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
				BREAK			
				CASE 1			
					model = 		S_M_Y_BLACKOPS_01
					vCoords = 		<<877.2245, -3239.9050, -99.0472>>
					fHeading = 		124.3974
					weapon = 		WEAPONTYPE_CARBINERIFLE
					sScenario = 		"WORLD_HUMAN_DRINKING"
				BREAK			
				CASE 2			
					model = 		S_M_Y_BLACKOPS_01
					vCoords = 		<<852.4462, -3246.2671, -99.6332>>
					fHeading = 		90.7973
					weapon = 		WEAPONTYPE_CARBINERIFLE
					sScenario = 		"CODE_HUMAN_PATROL_2H"
				BREAK			
				CASE 3			
					model = 		S_M_Y_BLACKOPS_01
					vCoords = 		<<832.1021, -3242.6372, -99.6991>>
					fHeading = 		90.7973
					weapon = 		WEAPONTYPE_PISTOL
					sScenario = 		""
				BREAK			
				CASE 4			
					model = 		S_M_Y_BLACKOPS_01
					vCoords = 		<<855.9549, -3229.8589, -99.4805>>
					fHeading = 		168.1969
					weapon = 		WEAPONTYPE_PISTOL
					sScenario = 		""
				BREAK			
				CASE 5			
					model = 		S_M_Y_BLACKOPS_01
					vCoords = 		<<856.6563, -3212.0872, -99.4786>>
					fHeading = 		6.9966
					weapon = 		WEAPONTYPE_CARBINERIFLE
					sScenario = 		""
				BREAK			
				CASE 6			
					model = 		S_M_Y_BLACKOPS_01
					vCoords = 		<<867.9493, -3232.0574, -99.2944>>
					fHeading = 		174.3963
					weapon = 		WEAPONTYPE_PISTOL
					sScenario = 		""
				BREAK			
				CASE 7			
					model = 		S_M_Y_BLACKOPS_01
					vCoords = 		<<870.4949, -3184.9963, -97.3735>>
					fHeading = 		104.396
					weapon = 		WEAPONTYPE_CARBINERIFLE
					sScenario = 		""
				BREAK			
				CASE 8			
					model = 		S_M_Y_BLACKOPS_01
					vCoords = 		<<893.6403, -3178.8096, -98.1236>>
					fHeading = 		233.3957
					weapon = 		WEAPONTYPE_PISTOL
					sScenario = 		""
				BREAK			
				CASE 9			
					model = 		S_M_Y_BLACKOPS_01
					vCoords = 		<<894.7396, -3179.8140, -98.1141>>
					fHeading = 		60.7956
					weapon = 		WEAPONTYPE_CARBINERIFLE
					sScenario = 		""
				BREAK			
				CASE 10			
					model = 		S_M_Y_BLACKOPS_01
					vCoords = 		<<891.3053, -3198.1965, -99.1963>>
					fHeading = 		67.5955
					weapon = 		WEAPONTYPE_PISTOL
					sScenario = 		""
				BREAK			
				CASE 11			
					model = 		S_M_Y_BLACKOPS_01
					vCoords = 		<<889.9914, -3197.8098, -99.1963>>
					fHeading = 		253.3952
					weapon = 		WEAPONTYPE_CARBINERIFLE
					sScenario = 		""
				BREAK			
				CASE 12			
					model = 		S_M_Y_BLACKOPS_01
					vCoords = 		<<888.2822, -3212.4397, -99.1963>>
					fHeading = 		200.3951
					weapon = 		WEAPONTYPE_PISTOL
					sScenario = 		""
				BREAK			
				CASE 13			
					model = 		S_M_Y_BLACKOPS_01
					vCoords = 		<<895.0668, -3223.9529, -99.2561>>
					fHeading = 		130.1951
					weapon = 		WEAPONTYPE_CARBINERIFLE
					sScenario = 		""
				BREAK			
				CASE 14			
					model = 		S_M_Y_BLACKOPS_01
					vCoords = 		<<910.6221, -3213.8284, -99.2322>>
					fHeading = 		92.1949
					weapon = 		WEAPONTYPE_PISTOL
					sScenario = 		""
				BREAK			
				CASE 15			
					model = 		S_M_Y_BLACKOPS_01
					vCoords = 		<<909.0012, -3213.8323, -99.2289>>
					fHeading = 		272.5945
					weapon = 		WEAPONTYPE_CARBINERIFLE
					sScenario = 		""
				BREAK			
				CASE 16			
					model = 		S_M_Y_BLACKOPS_01
					vCoords = 		<<906.6805, -3230.6943, -99.2943>>
					fHeading = 		47.5941
					weapon = 		WEAPONTYPE_PISTOL
					sScenario = 		""
				BREAK			
				CASE 17			
					model = 		S_M_Y_BLACKOPS_01
					vCoords = 		<<903.8074, -3237.2434, -99.2943>>
					fHeading = 		89.994
					weapon = 		WEAPONTYPE_CARBINERIFLE
					sScenario = 		"CODE_HUMAN_PATROL_2H"
				BREAK			
				CASE 18			
					model = 		S_M_Y_BLACKOPS_01
					vCoords = 		<<944.5753, -3228.5212, -99.2935>>
					fHeading = 		124.7937
					weapon = 		WEAPONTYPE_PISTOL
					sScenario = 		""
				BREAK			
				CASE 19			
					model = 		S_M_Y_BLACKOPS_01
					vCoords = 		<<943.3908, -3229.3806, -99.2936>>
					fHeading = 		295.1934
					weapon = 		WEAPONTYPE_CARBINERIFLE
					sScenario = 		""
				BREAK			
				CASE 20			
					model = 		S_M_Y_BLACKOPS_01
					vCoords = 		<<935.2187, -3209.8066, -99.2686>>
					fHeading = 		81.7931
					weapon = 		WEAPONTYPE_PISTOL
					sScenario = 		""
				BREAK			
				CASE 21			
					model = 		S_M_Y_BLACKOPS_01
					vCoords = 		<<933.8470, -3209.5205, -99.2675>>
					fHeading = 		263.1927
					weapon = 		WEAPONTYPE_CARBINERIFLE
					sScenario = 		""
				BREAK			
				CASE 22			
					model = 		S_M_Y_BLACKOPS_01
					vCoords = 		<<921.3503, -3198.2781, -99.2621>>
					fHeading = 		239.3927
					weapon = 		WEAPONTYPE_PISTOL
					sScenario = 		""
				BREAK			
				CASE 23			
					model = 		S_M_Y_BLACKOPS_01
					vCoords = 		<<933.4905, -3220.1211, -99.2789>>
					fHeading = 		326.1923
					weapon = 		WEAPONTYPE_CARBINERIFLE
					sScenario = 		""
				BREAK			
				CASE 24			
					model = 		S_M_Y_BLACKOPS_01
					vCoords = 		<<923.8032, -3230.3555, -99.2831>>
					fHeading = 		348.5921
					weapon = 		WEAPONTYPE_PISTOL
					sScenario = 		""
				BREAK			
				CASE 25			
					model = 		S_M_Y_BLACKOPS_01
					vCoords = 		<<921.1023, -3219.8833, -99.2686>>
					fHeading = 		348.5921
					weapon = 		WEAPONTYPE_CARBINERIFLE
					sScenario = 		""
				BREAK			
				CASE 26			
					model = 		S_M_Y_BLACKOPS_01
					vCoords = 		<<942.3767, -3194.9214, -99.2647>>
					fHeading = 		141.5915
					weapon = 		WEAPONTYPE_PISTOL
					sScenario = 		""
				BREAK			
				CASE 27			
					model = 		S_M_Y_BLACKOPS_01
					vCoords = 		<<946.4700, -3209.0098, -99.2762>>
					fHeading = 		141.5915
					weapon = 		WEAPONTYPE_CARBINERIFLE
					sScenario = 		""
				BREAK			
				CASE 28			
					model = 		S_M_Y_BLACKOPS_01
					vCoords = 		<<934.9174, -3239.7559, -99.2983>>
					fHeading = 		108.9913
					weapon = 		WEAPONTYPE_PISTOL
					sScenario = 		""
				BREAK			
				CASE 29			
					model = 		S_M_Y_BLACKOPS_01
					vCoords = 		<<904.9303, -3204.3628, -98.1879>>
					fHeading = 		22.9911
					weapon = 		WEAPONTYPE_CARBINERIFLE
					sScenario = 		""
				BREAK			
			ENDSWITCH				
		BREAK
	ENDSWITCH
	SWITCH eVariation
		CASE CHV_SERVER_FARM
			SWITCH iPed				
				CASE 0			
							model = 		S_M_M_CIASEC_01
					GET_SERVER_FARM_PED_DATA(eSubvariation, iPed, iExtraParam, vCoords, fHeading)
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK			
				CASE 1			
					model = 		S_M_M_Security_01
					vCoords = 		<<2150.9087, 2909.7854, -85.8001>>
					fHeading = 		281.9994
					weapon = 		WEAPONTYPE_PISTOL
					sScenario = 		"CODE_HUMAN_PATROL_1H"
				BREAK			
				CASE 2			
					model = 		S_M_M_Security_01
					vCoords = 		<<2077.3210, 2893.4927, -85.8001>>
					fHeading = 		176.3991
					weapon = 		WEAPONTYPE_PISTOL
					sScenario = 		"CODE_HUMAN_PATROL_1H"
				BREAK			
				CASE 3			
					model = 		S_M_M_Security_01
					vCoords = 		<<1997.6541, 2946.8938, -85.8001>>
					fHeading = 		-4.2012
					weapon = 		WEAPONTYPE_PISTOL
					sScenario = 		"CODE_HUMAN_PATROL_1H"
				BREAK			
				CASE 4			
					model = 		S_M_M_Security_01
					vCoords = 		<<2028.3529, 2933.1570, -85.8001>>
					fHeading = 		91.5985
					weapon = 		WEAPONTYPE_PISTOL
					sScenario = 		"CODE_HUMAN_PATROL_1H"
				BREAK			
				CASE 5			
					model = 		S_M_M_Security_01
					vCoords = 		<<2112.0508, 2946.3887, -85.7881>>
					fHeading = 		352.9983
					weapon = 		WEAPONTYPE_PISTOL
					sScenario = 		"CODE_HUMAN_PATROL_1H"
				BREAK			
				CASE 6			
					model = 		S_M_M_Security_01
					vCoords = 		<<2186.8679, 2916.6567, -85.8001>>
					fHeading = 		97.5981
					weapon = 		WEAPONTYPE_PISTOL
					sScenario = 		"CODE_HUMAN_PATROL_1H"
				BREAK			
				CASE 7			
					model = 		S_M_M_Security_01
					vCoords = 		<<2234.3005, 2902.6074, -85.8001>>
					fHeading = 		177.9978
					weapon = 		WEAPONTYPE_PISTOL
					sScenario = 		"CODE_HUMAN_PATROL_1H"
				BREAK			
				CASE 8			
					model = 		S_M_M_Security_01
					vCoords = 		<<2259.0801, 2899.9792, -85.7881>>
					fHeading = 		84.9976
					weapon = 		WEAPONTYPE_PISTOL
					sScenario = 		"CODE_HUMAN_PATROL_1H"
				BREAK			
				CASE 9			
					model = 		S_M_M_Security_01
					vCoords = 		<<2318.5222, 2947.2439, -85.8001>>
					fHeading = 		359.5973
					weapon = 		WEAPONTYPE_PISTOL
					sScenario = 		"CODE_HUMAN_PATROL_1H"
				BREAK			
				CASE 10			
					model = 		S_M_M_Security_01
					vCoords = 		<<2355.1609, 2933.5837, -85.7881>>
					fHeading = 		274.9967
					weapon = 		WEAPONTYPE_PISTOL
					sScenario = 		"CODE_HUMAN_PATROL_1H"
				BREAK			
				CASE 11			
					model = 		S_M_M_Security_01
					vCoords = 		<<2183.5117, 2928.5017, -85.8001>>
					fHeading = 		273.9974
					weapon = 		WEAPONTYPE_PISTOL
					sScenario = 		"CODE_HUMAN_PATROL_1H"
				BREAK			
				CASE 12			
					model = 		S_M_M_Security_01
					vCoords = 		<<2152.9966, 2913.4287, -85.8001>>
					fHeading = 		97.9971
					weapon = 		WEAPONTYPE_PISTOL
					sScenario = 		"CODE_HUMAN_PATROL_1H"
				BREAK			
				CASE 13
					SWITCH eSubvariation
						CASE CHS_SF_VARIATION_1
							model = 		S_M_M_CIASEC_01
							vCoords = 		<<224.5080, -450.1855, 43.3472>>
							fHeading = 		300.3978
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK
						
						CASE CHS_SF_VARIATION_2
							model = 		S_M_M_CIASEC_01
							vCoords = 		<<126.3955, -688.4967, 41.0292>>
							fHeading = 		99.1987
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK
						
						CASE CHS_SF_VARIATION_3
							model = 		S_M_M_CIASEC_01
							vCoords = 		<<318.5681, -1592.2581, 31.5940>>
							fHeading = 		11.6
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK
			ENDSWITCH				
		BREAK
				CASE 14
					SWITCH eSubvariation
						CASE CHS_SF_VARIATION_1
							model = 		S_M_M_CIASEC_01
							vCoords = 		<<225.5666, -449.5945, 43.3472>>
							fHeading = 		119.7973
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK
						
						CASE CHS_SF_VARIATION_2
							model = 		S_M_M_CIASEC_01
							vCoords = 		<<124.7648, -688.7768, 41.0292>>
							fHeading = 		277.9984
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK
						
						CASE CHS_SF_VARIATION_3
							model = 		S_M_M_CIASEC_01
							vCoords = 		<<318.2209, -1590.9106, 31.5940>>
							fHeading = 		192.1999
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_VAULT_KEY_CARDS_2
			SWITCH eSubvariation
				CASE CHS_VKC2_VARIATION_1
					SWITCH iPed				
						CASE 0			
							model = 		S_M_M_Prisguard_01
							vCoords = 		<<2383.6418, 1001.2664, 85.6709>>
							fHeading = 		226.1995
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 1			
							model = 		S_M_M_Prisguard_01
							vCoords = 		<<2384.9463, 1001.7201, 85.7167>>
							fHeading = 		226.1995
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 2			
							model = 		S_M_M_Prisguard_01
							vCoords = 		<<1661.9744, 2397.1443, 61.7378>>
							fHeading = 		310.3992
							weapon = 		WEAPONTYPE_SNIPERRIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 3			
							model = 		S_M_M_Prisguard_01
							vCoords = 		<<1820.6924, 2477.0774, 61.7152>>
							fHeading = 		67.3988
							weapon = 		WEAPONTYPE_SNIPERRIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 4			
							model = 		S_M_M_Prisguard_01
							vCoords = 		<<1792.1753, 2608.0908, 44.5650>>
							fHeading = 		314.1984
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 5			
							model = 		S_M_M_Prisguard_01
							vCoords = 		<<1793.3943, 2609.1821, 44.5650>>
							fHeading = 		133.5981
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 6			
							model = 		S_M_M_Prisguard_01
							vCoords = 		<<1791.6991, 2593.7651, 44.7957>>
							fHeading = 		271.9976
							weapon = 		WEAPONTYPE_ASSAULTSHOTGUN
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 7
							SWITCH iExtraParam
								CASE 0
									model = 		S_M_M_Prisguard_01
									vCoords = 		<<1757.8574, 2411.7927, 61.7348>>
									fHeading = 		72.9972
									weapon = 		WEAPONTYPE_ASSAULTSHOTGUN
									sScenario = 		"WORLD_HUMAN_BINOCULARS"
								BREAK			
								CASE 1			
									model = 		S_M_M_Prisguard_01
									vCoords = 		<<1762.0253, 2413.8027, 44.3627>>
									fHeading = 		342.3996
									weapon = 		WEAPONTYPE_ASSAULTSHOTGUN
									sScenario = 		"WORLD_HUMAN_LEANING"
								BREAK			
								CASE 2			
									model = 		S_M_M_Prisguard_01
									vCoords = 		<<1762.1301, 2412.0090, 61.8748>>
									fHeading = 		255.3992
									weapon = 		WEAPONTYPE_ASSAULTSHOTGUN
									sScenario = 		"WORLD_HUMAN_CLIPBOARD"
								BREAK
							ENDSWITCH
						BREAK
						CASE 8
							model = 		S_M_M_Prisguard_01
							vCoords = 		<<1830.7048, 2605.1421, 44.5720>>
							fHeading = 		0
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK
					ENDSWITCH
				BREAK
				CASE CHS_VKC2_VARIATION_2
					SWITCH iPed				
						CASE 0			
							model = 		S_M_M_Prisguard_01
							vCoords = 		<<-1893.1556, 764.3487, 143.0364>>
							fHeading = 		131.3998
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 1			
							model = 		S_M_M_Prisguard_01
							vCoords = 		<<-1893.1556, 764.3487, 143.0364>>
							fHeading = 		131.3998
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 2			
							model = 		S_M_M_Prisguard_01
							vCoords = 		<<1822.9998, 2601.3396, 44.6055>>
							fHeading = 		164.9992
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 3			
							model = 		S_M_M_Prisguard_01
							vCoords = 		<<1792.9332, 2601.9160, 44.5650>>
							fHeading = 		217.9992
							weapon = 		WEAPONTYPE_ASSAULTSHOTGUN
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 4			
							model = 		S_M_M_Prisguard_01
							vCoords = 		<<1793.7317, 2600.8499, 44.5650>>
							fHeading = 		34.599
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 5			
							model = 		S_M_M_Prisguard_01
							vCoords = 		<<1544.1489, 2470.8623, 61.7407>>
							fHeading = 		293.5987
							weapon = 		WEAPONTYPE_SNIPERRIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 6			
							model = 		S_M_M_Prisguard_01
							vCoords = 		<<1573.5497, 2679.5132, 61.7704>>
							fHeading = 		262.7984
							weapon = 		WEAPONTYPE_SNIPERRIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 7			
							SWITCH iExtraParam
								CASE 0
									model = 		S_M_M_Prisguard_01
									vCoords = 		<<1536.3568, 2583.9487, 61.8522>>
									fHeading = 		239.7983
									weapon = 		WEAPONTYPE_ASSAULTSHOTGUN
									sScenario = 		"WORLD_HUMAN_BINOCULARS"
								BREAK			
								CASE 1			
									model = 		S_M_M_Prisguard_01
									vCoords = 		<<1535.1722, 2587.7820, 44.5394>>
									fHeading = 		178.7996
									weapon = 		WEAPONTYPE_ASSAULTSHOTGUN
									sScenario = 		"WORLD_HUMAN_LEANING"
								BREAK			
								CASE 2			
									model = 		S_M_M_Prisguard_01
									vCoords = 		<<1537.5997, 2583.2241, 44.3265>>
									fHeading = 		231.7994
									weapon = 		WEAPONTYPE_ASSAULTSHOTGUN
									sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
								BREAK	
							ENDSWITCH
						BREAK
						CASE 8
							model = 		S_M_M_Prisguard_01
							vCoords = 		<<1830.7048, 2605.1421, 44.5720>>
							fHeading = 		0
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK
					ENDSWITCH				
				BREAK
				CASE CHS_VKC2_VARIATION_3
					SWITCH iPed				
						CASE 0			
							model = 		S_M_M_Prisguard_01
							vCoords = 		<<-99.5746, 2981.6448, 39.2284>>
							fHeading = 		104.7998
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 1			
							model = 		S_M_M_Prisguard_01
							vCoords = 		<<-99.5746, 2981.6448, 39.2284>>
							fHeading = 		104.7998
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 2			
							model = 		S_M_M_Prisguard_01
							vCoords = 		<<1802.0245, 2613.4346, 44.5885>>
							fHeading = 		214.5995
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 3			
							model = 		S_M_M_Prisguard_01
							vCoords = 		<<1802.8219, 2612.3179, 44.5979>>
							fHeading = 		31.7993
							weapon = 		WEAPONTYPE_ASSAULTSHOTGUN
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 4			
							model = 		S_M_M_Prisguard_01
							vCoords = 		<<1792.7314, 2622.0393, 44.5650>>
							fHeading = 		272.1989
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK			
						CASE 5			
							model = 		S_M_M_Prisguard_01
							vCoords = 		<<1844.8713, 2699.3486, 61.9813>>
							fHeading = 		100.7985
							weapon = 		WEAPONTYPE_SNIPERRIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 6			
							model = 		S_M_M_Prisguard_01
							vCoords = 		<<1652.8590, 2756.4417, 61.9090>>
							fHeading = 		250.9981
							weapon = 		WEAPONTYPE_SNIPERRIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 7			
							SWITCH iExtraParam
								CASE 0
									model = 		S_M_M_Prisguard_01
									vCoords = 		<<1770.4221, 2760.1482, 61.9183>>
									fHeading = 		120.9979
									weapon = 		WEAPONTYPE_ASSAULTSHOTGUN
									sScenario = 		"WORLD_HUMAN_BINOCULARS"
								BREAK			
								CASE 1			
									model = 		S_M_M_Prisguard_01
									vCoords = 		<<1774.6270, 2763.5486, 44.7370>>
									fHeading = 		304.7997
									weapon = 		WEAPONTYPE_ASSAULTSHOTGUN
									sScenario = 		"CODE_HUMAN_MEDIC_TEND_TO_DEAD"
								BREAK			
								CASE 2			
									model = 		S_M_M_Prisguard_01
									vCoords = 		<<1775.1113, 2761.7834, 62.0498>>
									fHeading = 		348.3994
									weapon = 		WEAPONTYPE_ASSAULTSHOTGUN
									sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
								BREAK	
							ENDSWITCH
						BREAK
						CASE 8
							model = 		S_M_M_Prisguard_01
							vCoords = 		<<1820.4977, 2604.8135, 44.5755>>
							fHeading = 		294.7997
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_DRONES
			SWITCH eSubvariation
				CASE CHS_DRONES_VARIATION_1
					SWITCH iPed				
						CASE 0			
							model = 		A_M_O_Tramp_01
							vCoords = 		<<201.9638, -946.9131, 29.6918>>
							fHeading = 		60.1999
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"amb@world_human_bum_slumped@male@laying_on_left_side@base"
						BREAK			
						CASE 1			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<200.9366, -946.3013, 29.6918>>
							fHeading = 		234.3995
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_MEDIC_KNEEL"
						BREAK			
						CASE 2			
							model = 		A_M_Y_Hipster_01
							vCoords = 		<<-1600.8063, 178.8218, 58.4082>>
							fHeading = 		220.3995
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 3			
							model = 		A_M_Y_Hipster_01
							vCoords = 		<<-1599.9083, 177.8907, 58.3772>>
							fHeading = 		43.1994
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_SMOKING_POT"
						BREAK			
						CASE 4			
							model = 		s_m_y_robber_01
							vCoords = 		<<-1801.7997, -1175.5111, 12.0175>>
							fHeading = 		244.1991
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"amb@world_human_drug_dealer_hard@male@base"
						BREAK			
						CASE 5			
							model = 		A_M_Y_Hipster_01
							vCoords = 		<<-1800.7168, -1175.9113, 12.0175>>
							fHeading = 		64.799
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_STAND_IMPATIENT"
						BREAK			
						CASE 6			
							model = 		S_F_Y_Hooker_02
							vCoords = 		<<1137.0776, -647.0551, 55.7438>>
							fHeading = 		264.5988
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_PROSTITUTE_HIGH_CLASS"
						BREAK			
						CASE 7			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<1138.1993, -647.3765, 55.7438>>
							fHeading = 		70.9985
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 8			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<1141.5062, -643.2009, 55.7674>>
							fHeading = 		9.9979
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_COP_IDLES"
						BREAK			
						CASE 9			
							model = 		A_M_Y_BusiCas_01
							vCoords = 		<<1141.2263, -641.9304, 55.7538>>
							fHeading = 		183.7977
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_STAND_IMPATIENT"
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_DRONES_VARIATION_2
					SWITCH iPed				
						CASE 0			
							model = 		A_F_Y_Runner_01
							vCoords = 		<<-421.1701, 1165.1880, 324.9041>>
							fHeading = 		133.1975
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"amb@world_human_yoga@female@base"
						BREAK			
						CASE 1			
							model = 		A_F_Y_Runner_01
							vCoords = 		<<-422.7558, 1164.3541, 324.9041>>
							fHeading = 		311.7972
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"amb@world_human_yoga@female@base"
						BREAK			
						CASE 2			
							model = 		A_M_O_Tramp_01
							vCoords = 		<<-540.2769, -221.5623, 36.6498>>
							fHeading = 		303.5969
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"amb@world_human_bum_slumped@male@laying_on_left_side@base"
						BREAK			
						CASE 3			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<-539.2714, -220.9787, 36.6498>>
							fHeading = 		118.7962
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_MEDIC_KNEEL"
						BREAK			
						CASE 4			
							model = 		S_M_M_HighSec_01
							vCoords = 		<<-1297.7701, 89.0614, 53.9142>>
							fHeading = 		214.9985
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_GUARD_STAND"
						BREAK			
						CASE 5			
							model = 		A_M_M_Golfer_01
							vCoords = 		<<-1291.0017, 81.6132, 53.9056>>
							fHeading = 		67.3982
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 6			
							model = 		G_M_Y_FamCA_01
							vCoords = 		<<98.6566, -1946.3273, 19.7616>>
							fHeading = 		26.5981
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_SMOKING_POT"
						BREAK			
						CASE 7			
							model = 		G_M_Y_FamCA_01
							vCoords = 		<<97.5387, -1945.4105, 19.7537>>
							fHeading = 		249.1978
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"amb@world_human_drug_dealer_hard@male@base"
						BREAK			
						CASE 8			
							model = 		G_M_Y_FamCA_01
							vCoords = 		<<98.6898, -1945.1001, 19.7792>>
							fHeading = 		154.9976
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_DRONES_VARIATION_3
					SWITCH iPed				
						CASE 0			
							model = 		s_m_y_robber_01
							vCoords = 		<<-1230.7446, -785.5246, 16.9440>>
							fHeading = 		31.1981
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"amb@world_human_drug_dealer_hard@male@base"
						BREAK			
						CASE 1			
							model = 		A_M_Y_Hipster_01
							vCoords = 		<<-1231.4973, -784.5881, 17.0185>>
							fHeading = 		216.1978
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_STAND_IMPATIENT"
						BREAK			
						CASE 2			
							model = 		A_F_Y_Runner_01
							vCoords = 		<<-102.6448, 902.8670, 235.3854>>
							fHeading = 		169.3976
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"amb@world_human_yoga@female@base"
						BREAK			
						CASE 3			
							model = 		A_F_Y_Runner_01
							vCoords = 		<<-103.0928, 901.4701, 235.3857>>
							fHeading = 		348.7974
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"amb@world_human_yoga@female@base"
						BREAK			
						CASE 4			
							model = 		A_F_Y_BEACH_01
							vCoords = 		<<450.6516, -225.1551, 54.9727>>
							fHeading = 		246.5967
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"amb@world_human_sunbathe@female@front@idle_a"
						BREAK			
						CASE 5			
							model = 		A_F_Y_BEACH_01
							vCoords = 		<<451.3102, -223.8495, 54.9727>>
							fHeading = 		267.5969
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"amb@world_human_sunbathe@female@front@idle_a"
						BREAK			
						CASE 6			
							model = 		A_M_O_Tramp_01
							vCoords = 		<<-266.9598, -1884.0511, 26.7554>>
							fHeading = 		229.5967
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"amb@world_human_bum_slumped@male@laying_on_left_side@base"
						BREAK			
						CASE 7			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<-266.0345, -1884.6654, 26.7554>>
							fHeading = 		50.7965
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_MEDIC_KNEEL"
						BREAK			
						CASE 8			
							model = 		A_M_M_GenFat_02
							vCoords = 		<<1294.5455, -1729.3080, 53.4464>>
							fHeading = 		294.9962
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 9			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<1294.1395, -1728.6218, 52.6761>>
							fHeading = 		207.7957
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_COP_IDLES"
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_SECURITY_CAMERAS
			SWITCH eSubvariation
				CASE CHS_SC_LA_MESA
					SWITCH iPed				
						CASE 0			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("g_m_importexport_01"))
							vCoords = 		<<-500.9749, -72.8896, 40.7269>>
							fHeading = 		71.8012
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 1			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("g_f_importexport_01"))
							vCoords = 		<<762.7510, -1640.4041, 29.1350>>
							fHeading = 		11.599
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 2			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("g_m_importexport_01"))
							vCoords = 		<<762.5770, -1638.6810, 29.2150>>
							fHeading = 		190.999
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 3			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("g_f_importexport_01"))
							vCoords = 		<<768.5100, -1643.4780, 29.0910>>
							fHeading = 		349.999
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"WORLD_HUMAN_GUARD_STAND"
						BREAK			
						CASE 4			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("g_m_importexport_01"))
							vCoords = 		<<770.0000, -1636.3800, 31.9260>>
							fHeading = 		97.999
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"WORLD_HUMAN_SMOKING_POT"
						BREAK			
						CASE 5			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("g_m_importexport_01"))
							vCoords = 		<<773.0660, -1632.1730, 29.9640>>
							fHeading = 		97.999
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
						CASE 6			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("g_m_importexport_01"))
							vCoords = 		<<771.3450, -1644.6450, 36.5450>>
							fHeading = 		10.999
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_PATROL_1H"
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_SC_BURTON
					SWITCH iPed				
						CASE 0			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("g_m_importexport_01"))
							vCoords = 		<<-362.7420, -184.4960, 38.7940>>
							fHeading = 		76.2
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 1			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("g_f_importexport_01"))
							vCoords = 		<<-138.7820, -1630.9690, 31.5870>>
							fHeading = 		198.799
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 2			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("g_m_importexport_01"))
							vCoords = 		<<-138.5100, -1632.0909, 31.4720>>
							fHeading = 		18.599
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 3			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("g_m_importexport_01"))
							vCoords = 		<<-146.8390, -1632.7791, 32.0820>>
							fHeading = 		233.999
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"WORLD_HUMAN_GUARD_STAND"
						BREAK			
						CASE 4			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("g_f_importexport_01"))
							vCoords = 		<<-141.3500, -1640.6880, 31.4880>>
							fHeading = 		312.198
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 5			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("g_m_importexport_01"))
							vCoords = 		<<-140.3380, -1639.6820, 31.4660>>
							fHeading = 		126.398
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"WORLD_HUMAN_SMOKING_POT"
						BREAK			
						CASE 6			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("g_m_importexport_01"))
							vCoords = 		<<-126.3420, -1630.3470, 31.1010>>
							fHeading = 		48.197
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_SC_VESPUCCI
					SWITCH iPed				
						CASE 0			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("g_m_importexport_01"))
							vCoords = 		<<-225.1050, 22.4000, 57.0810>>
							fHeading = 		76.2
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 1			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("g_f_importexport_01"))
							vCoords = 		<<-1294.6600, -1305.4301, 3.5360>>
							fHeading = 		48.8
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 2			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("g_m_importexport_01"))
							vCoords = 		<<-1295.8400, -1304.6000, 3.5690>>
							fHeading = 		232.399
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 3			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("g_f_importexport_01"))
							vCoords = 		<<-1297.7950, -1310.0950, 3.6950>>
							fHeading = 		136.199
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 4			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("g_m_importexport_01"))
							vCoords = 		<<-1298.5110, -1311.0950, 3.7150>>
							fHeading = 		326.799
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"WORLD_HUMAN_SMOKING_POT"
						BREAK			
						CASE 5			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("g_m_importexport_01"))
							vCoords = 		<<-1307.6110, -1317.7180, 3.8760>>
							fHeading = 		16.598
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"WORLD_HUMAN_GUARD_STAND"
						BREAK			
						CASE 6			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("g_m_importexport_01"))
							vCoords = 		<<-1308.0320, -1303.0870, 3.8390>>
							fHeading = 		288.198
							weapon = 		WEAPONTYPE_ASSAULTRIFLE
							sScenario = 		"CODE_HUMAN_PATROL_2H"
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_ARMORED_EQUIPMENT_2
			SWITCH iPed				
				CASE 0			
					model = 		S_M_Y_BLACKOPS_01
					vCoords = 		<<3526.3508, 3673.6091, 27.1211>>
					fHeading = 		260.8992
					weapon = 		WEAPONTYPE_CARBINERIFLE
					sScenario = 		"CODE_HUMAN_PATROL_2H"
				BREAK			
				CASE 1			
					model = 		S_M_Y_BLACKOPS_01
					vCoords = 		<<3537.6091, 3665.3279, 27.1219>>
					fHeading = 		253.4985
					weapon = 		WEAPONTYPE_PISTOL
					sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
				BREAK			
				CASE 2			
					model = 		S_M_Y_BLACKOPS_01
					vCoords = 		<<3539.0222, 3664.9961, 27.1219>>
					fHeading = 		78.6987
					weapon = 		WEAPONTYPE_PISTOL
					sScenario = 		"WORLD_HUMAN_CLIPBOARD"
				BREAK			
				CASE 3			
					model = 		S_M_Y_BLACKOPS_01
					vCoords = 		<<3529.0903, 3652.5713, 26.5216>>
					fHeading = 		353.898
					weapon = 		WEAPONTYPE_CARBINERIFLE
					sScenario = 		"CODE_HUMAN_PATROL_2H"
				BREAK			
				CASE 4			
					model = 		S_M_Y_BLACKOPS_01
					vCoords = 		<<3534.6260, 3646.0613, 26.5216>>
					fHeading = 		345.098
					weapon = 		WEAPONTYPE_PISTOL
					sScenario = 		""
				BREAK			
				CASE 5			
					model = 		S_M_Y_BLACKOPS_01
					vCoords = 		<<3527.6506, 3650.1208, 26.5216>>
					fHeading = 		73.6974
					weapon = 		WEAPONTYPE_PISTOL
					sScenario = 		""
				BREAK			
				CASE 6			
					model = 		S_M_Y_BLACKOPS_01
					vCoords = 		<<3544.2866, 3645.3994, 27.1219>>
					fHeading = 		73.6974
					weapon = 		WEAPONTYPE_PISTOL
					sScenario = 		""
				BREAK			
				CASE 7			
					model = 		S_M_Y_BLACKOPS_01
					vCoords = 		<<3540.7085, 3643.1157, 27.1219>>
					fHeading = 		162.0972
					weapon = 		WEAPONTYPE_PISTOL
					sScenario = 		""
				BREAK			
				CASE 8			
					model = 		S_M_Y_BLACKOPS_01
					vCoords = 		<<3547.9194, 3645.4109, 27.1219>>
					fHeading = 		172.0965
					weapon = 		WEAPONTYPE_CARBINERIFLE
					sScenario = 		""
				BREAK			
				CASE 9			
					model = 		S_M_Y_BLACKOPS_01
					vCoords = 		<<3548.5159, 3641.6199, 27.1219>>
					fHeading = 		88.6964
					weapon = 		WEAPONTYPE_CARBINERIFLE
					sScenario = 		""
				BREAK			
				CASE 10			
					model = 		S_M_Y_BLACKOPS_01
					vCoords = 		<<3550.3276, 3656.0510, 27.1219>>
					fHeading = 		6.0962
					weapon = 		WEAPONTYPE_CARBINERIFLE
					sScenario = 		""
				BREAK			
				CASE 11			
					model = 		S_M_Y_BLACKOPS_01
					vCoords = 		<<3548.9236, 3651.0281, 27.1219>>
					fHeading = 		40.2962
					weapon = 		WEAPONTYPE_PISTOL
					sScenario = 		""
				BREAK			
				CASE 12			
					model = 		S_M_Y_BLACKOPS_01
					vCoords = 		<<3533.4873, 3648.7114, 26.5216>>
					fHeading = 		174.4951
					weapon = 		WEAPONTYPE_CARBINERIFLE
					sScenario = 		""
				BREAK			
				CASE 13			
					model = 		S_M_Y_BLACKOPS_01
					vCoords = 		<<3537.6550, 3659.7122, 27.1219>>
					fHeading = 		174.4951
					weapon = 		WEAPONTYPE_CARBINERIFLE
					sScenario = 		"WORLD_HUMAN_WINDOW_SHOP_BROWSE"
				BREAK			
				CASE 14			
					model = 		S_M_Y_BLACKOPS_01
					vCoords = 		<<3529.1196, 3646.7683, 26.5216>>
					fHeading = 		175.2984
					weapon = 		WEAPONTYPE_CARBINERIFLE
					sScenario = 		""
				BREAK			
				CASE 15			
					model = 		S_M_Y_BLACKOPS_01
					vCoords = 		<<3551.4866, 3722.3225, 36.3527>>
					fHeading = 		192.9288
					weapon = 		WEAPONTYPE_PISTOL
					sScenario = 		"WORLD_HUMAN_DRINKING"
				BREAK			
				CASE 16			
					model = 		S_M_Y_BLACKOPS_01
					vCoords = 		<<3551.9412, 3720.6313, 36.3528>>
					fHeading = 		9.7288
					weapon = 		WEAPONTYPE_CARBINERIFLE
					sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
				BREAK			
				CASE 17			
					model = 		S_M_Y_BLACKOPS_01
					vCoords = 		<<3556.1729, 3727.6040, 36.3527>>
					fHeading = 		53.5278
					weapon = 		WEAPONTYPE_PISTOL
					sScenario = 		"WORLD_HUMAN_SMOKING"
				BREAK			
				CASE 18			
					model = 		S_M_Y_BLACKOPS_01
					vCoords = 		<<3546.7632, 3715.2361, 36.3527>>
					fHeading = 		338.7279
					weapon = 		WEAPONTYPE_CARBINERIFLE
					sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
				BREAK	
				CASE 19			
					model = 		A_M_M_Hillbilly_02
					vCoords = 		<<2633.4702, 3290.5686, 54.7334>>
					fHeading = 		149.3988
					weapon = 		WEAPONTYPE_PISTOL
					sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
				BREAK			
				CASE 20			
					model = 		A_M_M_Hillbilly_02
					vCoords = 		<<2631.5920, 3282.3997, 54.2597>>
					fHeading = 		5.398
					weapon = 		WEAPONTYPE_PISTOL
					sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
				BREAK			
				CASE 21			
					model = 		A_M_M_Hillbilly_02
					vCoords = 		<<2631.4702, 3283.4407, 54.2624>>
					fHeading = 		182.3978
					weapon = 		WEAPONTYPE_PISTOL
					sScenario = 		"WORLD_HUMAN_DRINKING"
				BREAK			
				CASE 22			
					model = 		A_M_M_Hillbilly_02
					vCoords = 		<<2626.4631, 3282.7385, 54.2719>>
					fHeading = 		130.1971
					weapon = 		WEAPONTYPE_PISTOL
					sScenario = 		"WORLD_HUMAN_GUARD_STAND"
				BREAK			
				CASE 23			
					model = 		A_M_M_Hillbilly_02
					vCoords = 		<<2620.7205, 3275.8503, 54.2510>>
					fHeading = 		237.1978
					weapon = 		WEAPONTYPE_PISTOL
					sScenario = 		"WORLD_HUMAN_SMOKING_POT"
				BREAK			
			ENDSWITCH				
		BREAK
		CASE CHV_FIB_RAID
			SWITCH eSubvariation
				CASE CHS_FIB_VARIATION_1
					SWITCH iPed				
						CASE 0			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<-1.2192, -1544.5367, 28.3309>>
							fHeading = 		226.4579
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_COP_IDLES"
						BREAK			
						CASE 1			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<-17.1057, -1534.5450, 28.7525>>
							fHeading = 		53.8579
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_COP_IDLES"
						BREAK			
						CASE 2			
							model = 		s_m_m_fibsec_01
							vCoords = 		<<-7.0509, -1539.3245, 28.4629>>
							fHeading = 		269.2576
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK
						CASE 3			
							model = 		G_M_Y_FamCA_01
							vCoords = 		<<-9.0860, -1531.9760, 28.7630>>
							fHeading = 		213.058
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 4			
							model = 		G_M_Y_FamCA_01
							vCoords = 		<<-5.0290, -1531.1240, 28.7030>>
							fHeading = 		153.058
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 5			
							model = 		G_M_Y_FamCA_01
							vCoords = 		<<-5.9360, -1533.7950, 28.6750>>
							fHeading = 		270.658
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 6			
							model = 		S_M_M_Paramedic_01
							vCoords = 		<<-4.2470, -1532.1310, 28.6670>>
							fHeading = 		27.257
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"CODE_HUMAN_MEDIC_TEND_TO_DEAD"
						BREAK			
						CASE 7			
							model = 		A_M_Y_Business_01
							vCoords = 		<<129.4940, -757.8150, 241.1520>>
							fHeading = 		249.4
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_WINDOW_SHOP_BROWSE"
						BREAK			
						CASE 8			
							model = 		A_M_Y_Business_01
							vCoords = 		<<147.6310, -748.6910, 241.1520>>
							fHeading = 		249.4
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 9			
							model = 		A_M_Y_Business_01
							vCoords = 		<<149.0170, -738.2700, 241.1520>>
							fHeading = 		293.399
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 10			
							model = 		A_F_Y_Business_02
							vCoords = 		<<150.0610, -737.6110, 241.1520>>
							fHeading = 		137.399
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_PROSTITUTE_HIGH_CLASS"
						BREAK			
						CASE 11			
							model = 		A_F_Y_Business_02
							vCoords = 		<<143.1530, -756.6700, 241.1520>>
							fHeading = 		77.399
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_WINDOW_SHOP_BROWSE"
						BREAK			
						CASE 12			
							model = 		A_F_Y_Business_02
							vCoords = 		<<120.5810, -761.8320, 241.1520>>
							fHeading = 		33.599
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 13			
							model = 		A_M_Y_Business_01
							vCoords = 		<<120.1710, -760.7380, 241.1520>>
							fHeading = 		199.799
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 14			
							model = 		A_M_Y_Business_01
							vCoords = 		<<117.7330, -733.9310, 241.1520>>
							fHeading = 		157.998
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_GUARD_STAND"
						BREAK			
						CASE 15			
							model = 		A_M_Y_Business_01
							vCoords = 		<<123.1750, -726.6460, 241.1520>>
							fHeading = 		337.998
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_MEDIC_KNEEL"
						BREAK			
						CASE 16			
							model = 		A_M_Y_Business_01
							vCoords = 		<<117.8050, -744.5890, 241.1520>>
							fHeading = 		204.798
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK			
						CASE 17			
							model = 		A_M_Y_Business_01
							vCoords = 		<<118.3350, -745.5210, 241.1520>>
							fHeading = 		25.197
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 18			
							model = 		A_M_Y_Business_01
							vCoords = 		<<107.2100, -744.0290, 241.1520>>
							fHeading = 		70.197
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_BINOCULARS"
						BREAK			
						CASE 19			
							model = 		A_M_Y_Business_01
							vCoords = 		<<113.8910, -758.1720, 44.7550>>
							fHeading = 		70.197
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_GUARD_STAND"
						BREAK			
						CASE 20			
							model = 		A_F_Y_Business_02
							vCoords = 		<<113.3420, -749.4510, 44.7520>>
							fHeading = 		61.597
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 21			
							model = 		A_M_Y_Business_01
							vCoords = 		<<112.3790, -748.8870, 44.7520>>
							fHeading = 		233.197
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK	
						CASE 22			
							model = 		s_m_m_fibsec_01
							vCoords = 		<<-6.0125, -1539.2930, 28.4833>>
							fHeading = 		92.2576
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_MEDIC_TIME_OF_DEATH"
						BREAK			
						CASE 23			
							model = 		s_m_m_fibsec_01
							vCoords = 		<<-11.8218, -1536.9612, 28.5513>>
							fHeading = 		330.4573
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK			
						CASE 24			
							model = 		s_m_m_fibsec_01
							vCoords = 		<<-7.8219, -1530.3574, 28.7656>>
							fHeading = 		189.4568
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_MEDIC_TEND_TO_DEAD"
						BREAK			
						CASE 25			
							model = 		S_M_M_Security_01
							vCoords = 		<<118.0645, -752.7473, 44.7516>>
							fHeading = 		73.3999
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_GUARD_STAND"
						BREAK			
						CASE 26			
							model = 		S_M_M_Security_01
							vCoords = 		<<116.4720, -759.3703, 44.7548>>
							fHeading = 		337.3996
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_GUARD_STAND"
						BREAK			

					ENDSWITCH				
				BREAK
				
				CASE CHS_FIB_VARIATION_2
					SWITCH iPed				
						CASE 0			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<-991.6983, -1480.4572, 4.4711>>
							fHeading = 		214
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_COP_IDLES"
						BREAK			
						CASE 1			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<-992.0188, -1470.6320, 4.1135>>
							fHeading = 		301.9983
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_COP_IDLES"
						BREAK			
						CASE 2			
							model = 		s_m_m_fibsec_01
							vCoords = 		<<-994.6081, -1476.1565, 4.2201>>
							fHeading = 		342.9988
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
		
						CASE 3			
							model = 		G_M_Y_BallaOrig_01
							vCoords = 		<<-998.4640, -1473.9730, 4.1300>>
							fHeading = 		353.198
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 4			
							model = 		G_M_Y_BallaOrig_01
							vCoords = 		<<-1002.6250, -1470.9160, 4.0250>>
							fHeading = 		266.198
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 5			
							model = 		G_M_Y_BallaOrig_01
							vCoords = 		<<-1000.2080, -1471.2870, 4.0620>>
							fHeading = 		184.998
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 6			
							model = 		S_M_M_Paramedic_01
							vCoords = 		<<-997.3210, -1474.2330, 4.1490>>
							fHeading = 		74.798
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"CODE_HUMAN_MEDIC_TEND_TO_DEAD"
						BREAK			
						CASE 7			
							model = 		A_M_Y_Business_01
							vCoords = 		<<129.4940, -757.8150, 241.1520>>
							fHeading = 		249.4
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_WINDOW_SHOP_BROWSE"
						BREAK			
						CASE 8			
							model = 		A_M_Y_Business_01
							vCoords = 		<<147.6310, -748.6910, 241.1520>>
							fHeading = 		249.4
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 9			
							model = 		A_M_Y_Business_01
							vCoords = 		<<149.0170, -738.2700, 241.1520>>
							fHeading = 		293.399
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 10			
							model = 		A_F_Y_Business_02
							vCoords = 		<<150.0610, -737.6110, 241.1520>>
							fHeading = 		137.399
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_PROSTITUTE_HIGH_CLASS"
						BREAK			
						CASE 11			
							model = 		A_F_Y_Business_02
							vCoords = 		<<143.1530, -756.6700, 241.1520>>
							fHeading = 		77.399
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_WINDOW_SHOP_BROWSE"
						BREAK			
						CASE 12			
							model = 		A_F_Y_Business_02
							vCoords = 		<<120.5810, -761.8320, 241.1520>>
							fHeading = 		33.599
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 13			
							model = 		A_M_Y_Business_01
							vCoords = 		<<120.1710, -760.7380, 241.1520>>
							fHeading = 		199.799
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 14			
							model = 		A_M_Y_Business_01
							vCoords = 		<<117.7330, -733.9310, 241.1520>>
							fHeading = 		157.998
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_GUARD_STAND"
						BREAK			
						CASE 15			
							model = 		A_M_Y_Business_01
							vCoords = 		<<123.1750, -726.6460, 241.1520>>
							fHeading = 		337.998
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_MEDIC_KNEEL"
						BREAK			
						CASE 16			
							model = 		A_M_Y_Business_01
							vCoords = 		<<117.8050, -744.5890, 241.1520>>
							fHeading = 		204.798
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK			
						CASE 17			
							model = 		A_M_Y_Business_01
							vCoords = 		<<118.3350, -745.5210, 241.1520>>
							fHeading = 		25.197
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 18			
							model = 		A_M_Y_Business_01
							vCoords = 		<<107.2100, -744.0290, 241.1520>>
							fHeading = 		70.197
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_BINOCULARS"
						BREAK			
						CASE 19			
							model = 		A_M_Y_Business_01
							vCoords = 		<<113.8910, -758.1720, 44.7550>>
							fHeading = 		70.197
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_GUARD_STAND"
						BREAK			
						CASE 20			
							model = 		A_F_Y_Business_02
							vCoords = 		<<113.3420, -749.4510, 44.7520>>
							fHeading = 		61.597
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 21			
							model = 		A_M_Y_Business_01
							vCoords = 		<<112.3790, -748.8870, 44.7520>>
							fHeading = 		233.197
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK		
						CASE 22			
							model = 		s_m_m_fibsec_01
							vCoords = 		<<-994.3516, -1475.0024, 4.1979>>
							fHeading = 		155.1985
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_MEDIC_TIME_OF_DEATH"
						BREAK			
						CASE 23			
							model = 		s_m_m_fibsec_01
							vCoords = 		<<-995.4233, -1470.3132, 4.0832>>
							fHeading = 		155.1985
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK			
						CASE 24			
							model = 		s_m_m_fibsec_01
							vCoords = 		<<-1001.9797, -1469.7865, 4.0122>>
							fHeading = 		155.1985
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_MEDIC_TEND_TO_DEAD"
						BREAK			
						CASE 25			
							model = 		S_M_M_Security_01
							vCoords = 		<<118.0645, -752.7473, 44.7516>>
							fHeading = 		73.3999
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_GUARD_STAND"
						BREAK			
						CASE 26			
							model = 		S_M_M_Security_01
							vCoords = 		<<116.4720, -759.3703, 44.7548>>
							fHeading = 		337.3996
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_GUARD_STAND"
						BREAK			

					ENDSWITCH				
				BREAK
				
				CASE CHS_FIB_VARIATION_3
					SWITCH iPed				
						CASE 0			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<59.3309, 53.6467, 72.5151>>
							fHeading = 		72.799
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_COP_IDLES"
						BREAK			
						CASE 1			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<71.5547, 49.6196, 72.5151>>
							fHeading = 		248.9979
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_COP_IDLES"
						BREAK			
						CASE 2			
							model = 		s_m_m_fibsec_01
							vCoords = 		<<63.8121, 48.3512, 72.5151>>
							fHeading = 		34.3979
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 3			
							model = 		G_M_Y_Azteca_01
							vCoords = 		<<68.3810, 52.7330, 72.5150>>
							fHeading = 		86.398
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 4			
							model = 		G_M_Y_Azteca_01
							vCoords = 		<<65.2180, 54.1100, 72.5150>>
							fHeading = 		347.198
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 5			
							model = 		G_M_Y_Azteca_01
							vCoords = 		<<65.6490, 51.3860, 72.5150>>
							fHeading = 		291.997
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 6			
							model = 		S_M_M_Paramedic_01
							vCoords = 		<<66.3440, 54.3750, 72.5150>>
							fHeading = 		96.997
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"CODE_HUMAN_MEDIC_TEND_TO_DEAD"
						BREAK			
						CASE 7			
							model = 		A_M_Y_Business_01
							vCoords = 		<<129.4940, -757.8150, 241.1520>>
							fHeading = 		249.4
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_WINDOW_SHOP_BROWSE"
						BREAK			
						CASE 8			
							model = 		A_M_Y_Business_01
							vCoords = 		<<147.6310, -748.6910, 241.1520>>
							fHeading = 		249.4
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 9			
							model = 		A_M_Y_Business_01
							vCoords = 		<<149.0170, -738.2700, 241.1520>>
							fHeading = 		293.399
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 10			
							model = 		A_F_Y_Business_02
							vCoords = 		<<150.0610, -737.6110, 241.1520>>
							fHeading = 		137.399
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_PROSTITUTE_HIGH_CLASS"
						BREAK			
						CASE 11			
							model = 		A_F_Y_Business_02
							vCoords = 		<<143.1530, -756.6700, 241.1520>>
							fHeading = 		77.399
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_WINDOW_SHOP_BROWSE"
						BREAK			
						CASE 12			
							model = 		A_F_Y_Business_02
							vCoords = 		<<120.5810, -761.8320, 241.1520>>
							fHeading = 		33.599
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 13			
							model = 		A_M_Y_Business_01
							vCoords = 		<<120.1710, -760.7380, 241.1520>>
							fHeading = 		199.799
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 14			
							model = 		A_M_Y_Business_01
							vCoords = 		<<117.7330, -733.9310, 241.1520>>
							fHeading = 		157.998
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_GUARD_STAND"
						BREAK			
						CASE 15			
							model = 		A_M_Y_Business_01
							vCoords = 		<<123.1750, -726.6460, 241.1520>>
							fHeading = 		337.998
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_MEDIC_KNEEL"
						BREAK			
						CASE 16			
							model = 		A_M_Y_Business_01
							vCoords = 		<<117.8050, -744.5890, 241.1520>>
							fHeading = 		204.798
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK			
						CASE 17			
							model = 		A_M_Y_Business_01
							vCoords = 		<<118.3350, -745.5210, 241.1520>>
							fHeading = 		25.197
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 18			
							model = 		A_M_Y_Business_01
							vCoords = 		<<107.2100, -744.0290, 241.1520>>
							fHeading = 		70.197
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_BINOCULARS"
						BREAK			
						CASE 19			
							model = 		A_M_Y_Business_01
							vCoords = 		<<113.8910, -758.1720, 44.7550>>
							fHeading = 		70.197
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_GUARD_STAND"
						BREAK			
						CASE 20			
							model = 		A_F_Y_Business_02
							vCoords = 		<<113.3420, -749.4510, 44.7520>>
							fHeading = 		61.597
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 21			
							model = 		A_M_Y_Business_01
							vCoords = 		<<112.3790, -748.8870, 44.7520>>
							fHeading = 		233.197
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK
						CASE 22			
							model = 		s_m_m_fibsec_01
							vCoords = 		<<63.1395, 49.2212, 72.5151>>
							fHeading = 		214.5978
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_MEDIC_TIME_OF_DEATH"
						BREAK			
						CASE 23			
							model = 		s_m_m_fibsec_01
							vCoords = 		<<67.9454, 47.3964, 72.5151>>
							fHeading = 		324.7975
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK			
						CASE 24			
							model = 		s_m_m_fibsec_01
							vCoords = 		<<69.1319, 51.4192, 72.5151>>
							fHeading = 		26.7973
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_MEDIC_TEND_TO_DEAD"
						BREAK			
						CASE 25			
							model = 		S_M_M_Security_01
							vCoords = 		<<118.0645, -752.7473, 44.7516>>
							fHeading = 		73.3999
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_GUARD_STAND"
						BREAK			
						CASE 26			
							model = 		S_M_M_Security_01
							vCoords = 		<<116.4720, -759.3703, 44.7548>>
							fHeading = 		337.3996
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_GUARD_STAND"
						BREAK
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_VAULT_KEY_CARDS_1
			SWITCH eSubvariation
				CASE CHS_VKC1_VARIATION_1
					SWITCH iPed				
						CASE 0			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_01"))
							vCoords = 		<<-379.0916, 218.5396, 82.6566>>
							fHeading = 		354.399
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 	"amb@world_human_bum_standing@drunk@idle_a"
						BREAK			
						CASE 1			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("A_M_Y_Yoga_01"))
							vCoords = 		<<152.7964, -1002.4796, -100.0000>>
							fHeading = 		146.5996
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_MUSCLE_FLEX"
						BREAK			
						CASE 2			
							model = 		S_F_Y_Hooker_02
							vCoords = 		<<152.2456, -1003.1400, -100.0000>>
							fHeading = 		334.3994
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_PROSTITUTE_HIGH_CLASS"
						BREAK			
						CASE 3			
							model = 		S_F_Y_Hooker_02
							vCoords = 		<<154.0473, -1000.5384, -100.0000>>
							fHeading = 		8.9991
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 	"WORLD_HUMAN_WINDOW_SHOP_BROWSE"
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_VKC1_VARIATION_2
					SWITCH iPed				
						CASE 0			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("s_m_y_westsec_01"))
							vCoords = 		<<1217.6399, -416.2425, 66.7629>>
							fHeading = 		71
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 	"amb@world_human_bum_standing@drunk@idle_a"

						BREAK			
						CASE 1			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("A_M_Y_Yoga_01"))
							vCoords = 		<<152.7964, -1002.4796, -100.0000>>
							fHeading = 		146.5996
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 	"WORLD_HUMAN_MUSCLE_FLEX"
						BREAK			
						CASE 2			
							model = 		S_F_Y_Hooker_02
							vCoords = 		<<152.2456, -1003.1400, -100.0000>>
							fHeading = 		334.3994
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 	"WORLD_HUMAN_PROSTITUTE_HIGH_CLASS"
						BREAK			
						CASE 3			
							model = 		S_F_Y_Hooker_02
							vCoords = 		<<154.0473, -1000.5384, -100.0000>>
							fHeading = 		8.9991
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 	"WORLD_HUMAN_WINDOW_SHOP_BROWSE"
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_INSIDE_CASHIER
			SWITCH eSubvariation
				CASE CHS_IMC_VARIATION_1
					SWITCH iPed				
						CASE 0			
							model = 		A_F_Y_BevHills_03
							vCoords = 		<<-1992.1814, 235.0137, 86.2056>>
							fHeading = 		354.3994
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"AMB@WORLD_HUMAN_PARTYING@FEMALE@PARTYING_BEER@IDLE_A"
						BREAK			
						CASE 1			
							model = 		A_F_Y_BevHills_03
							vCoords = 		<<-1985.7150, 244.3130, 86.2123>>
							fHeading = 		52.1992
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"amb@world_human_drinking@beer@female@idle_a"
						BREAK			
						CASE 2			
							model = 		A_F_Y_BevHills_03
							vCoords = 		<<-1987.3900, 229.2740, 86.2180>>
							fHeading = 		180.799
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_SMOKING_POT"
						BREAK			
						CASE 3			
							model = 		A_F_Y_BevHills_03
							vCoords = 		<<-1988.5360, 228.8380, 86.2180>>
							fHeading = 		243.999
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 	"amb@world_human_bum_standing@drunk@idle_a"
						BREAK			
						CASE 4			
							model = 		A_F_Y_BevHills_03
							vCoords = 		<<-1981.4789, 244.0570, 86.6181>>
							fHeading = 		112.3987
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 5			
							model = 		A_F_Y_BevHills_03
							vCoords = 		<<-1997.3564, 240.2484, 86.4182>>
							fHeading = 		173.3985
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 	"amb@world_human_bum_standing@drunk@idle_a"
						BREAK			
						CASE 6			
							model = 		A_F_Y_Beach_01
							vCoords = 		<<-1991.8274, 242.9920, 86.2530>>
							fHeading = 		207.7982
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_SEAT_LEDGE"
						BREAK			
						CASE 7			
							model = 		A_F_Y_Beach_01
							vCoords = 		<<-1984.8964, 236.3168, 86.2179>>
							fHeading = 		108.3981
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_SEAT_LEDGE"
						BREAK			
						CASE 8			
							model = 		A_F_Y_BevHills_03
							vCoords = 		<<-1996.8002, 239.1917, 86.4182>>
							fHeading = 		78.798
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 9			
							model = 		A_F_Y_BevHills_03
							vCoords = 		<<-1991.3373, 248.4781, 86.7496>>
							fHeading = 		111.5976
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_SEAT_WALL"
						BREAK			
						CASE 10			
							model = 		A_M_Y_BevHills_02
							vCoords = 		<<-1986.4194, 245.0237, 86.2111>>
							fHeading = 		220.7973
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"amb@world_human_drinking@beer@male@idle_a"
						BREAK			
						CASE 11			
							model = 		A_M_Y_BevHills_02
							vCoords = 		<<-1991.6742, 236.3148, 86.2024>>
							fHeading = 		138.597
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 	"amb@world_human_bum_standing@drunk@idle_a"
						BREAK			
						CASE 12			
							model = 		A_M_Y_BevHills_02
							vCoords = 		<<-1992.9299, 236.0621, 86.2086>>
							fHeading = 		231.5967
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"AMB@WORLD_HUMAN_PARTYING@MALE@PARTYING_BEER@IDLE_A"
						BREAK			
						CASE 13			
							model = 		A_M_Y_BevHills_02
							vCoords = 		<<-1997.9623, 239.3170, 86.4182>>
							fHeading = 		287.9965
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 14			
							model = 		A_M_Y_BevHills_02
							vCoords = 		<<-1987.1670, 228.4460, 86.2180>>
							fHeading = 		55.396
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"amb@world_human_drinking@beer@male@idle_a"
						BREAK			
						CASE 15			
							model = 		A_M_Y_BevHills_02
							vCoords = 		<<-1994.8320, 231.8301, 86.4182>>
							fHeading = 		12.1961
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 	"amb@world_human_bum_standing@drunk@idle_a"
						BREAK			
						CASE 16			
							model = 		A_M_Y_BevHills_02
							vCoords = 		<<-1995.1306, 232.7961, 86.4182>>
							fHeading = 		189.9959
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"amb@world_human_drinking@beer@male@idle_a"
						BREAK			
						CASE 17			
							model = 		A_M_M_Beach_02
							vCoords = 		<<-1990.9215, 243.2388, 86.2535>>
							fHeading = 		189.9959
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_SEAT_LEDGE"
						BREAK			
						CASE 18			
							model = 		A_M_M_Beach_02
							vCoords = 		<<-1984.6597, 235.3724, 86.2180>>
							fHeading = 		103.3956
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_SEAT_LEDGE"
						BREAK			
						CASE 19			
							model = 		A_M_Y_BevHills_02
							vCoords = 		<<-1983.6487, 239.3822, 86.2149>>
							fHeading = 		68.1954
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"amb@world_human_drinking@beer@male@idle_a"
						BREAK			
						CASE 20			
							model = 		A_M_Y_BevHills_01
							vCoords = 		<<-1979.3031, 238.6312, 86.6181>>
							fHeading = 		112.395
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"amb@world_human_bum_slumped@male@laying_on_right_side@base"
						BREAK			
						CASE 21			
							model = 		A_M_Y_BevHills_01
							vCoords = 		<<-1994.3839, 227.1585, 86.4182>>
							fHeading = 		339.195
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"AMB@WORLD_HUMAN_STUPOR@MALE_LOOKING_LEFT@IDLE_A"
						BREAK			
						CASE 22			
							model = 		A_M_Y_BevHills_01
							vCoords = 		<<-1995.9628, 244.9864, 86.2162>>
							fHeading = 		241.9945
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"AMB@WORLD_HUMAN_STUPOR@MALE@IDLE_A"
						BREAK			
						CASE 23			
							model = 		A_M_Y_BevHills_01
							vCoords = 		<<-1998.8850, 248.1931, 86.2775>>
							fHeading = 		326.594
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"amb@world_human_bum_slumped@male@laying_on_left_side@base"
						BREAK			
						CASE 24			
							model = 		A_M_Y_BevHills_01
							vCoords = 		<<-1983.2400, 227.1971, 86.1890>>
							fHeading = 		199.994
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"AMB@WORLD_HUMAN_STUPOR@MALE@BASE"
						BREAK			
						CASE 25			
							model = 		A_F_Y_BevHills_01
							vCoords = 		<<-1977.8992, 230.1320, 86.5181>>
							fHeading = 		344.393
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"AMB@WORLD_HUMAN_STUPOR@MALE_LOOKING_LEFT@IDLE_A"
						BREAK			
						CASE 26			
							model = 		A_F_Y_BevHills_01
							vCoords = 		<<-1998.7943, 250.7475, 86.1625>>
							fHeading = 		165.1932
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"AMB@WORLD_HUMAN_STUPOR@MALE_LOOKING_LEFT@IDLE_A"
						BREAK			
						CASE 27			
							model = 		A_F_Y_BevHills_01
							vCoords = 		<<-1997.6915, 235.3564, 86.3182>>
							fHeading = 		285.993
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"amb@world_human_bum_slumped@male@laying_on_right_side@base"
						BREAK			
						CASE 28			
							model = 		A_F_Y_BevHills_01
							vCoords = 		<<-1988.5469, 222.7071, 86.0842>>
							fHeading = 		285.9929
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"AMB@WORLD_HUMAN_STUPOR@MALE@IDLE_A"
						BREAK			
						CASE 29			
							model = 		A_F_Y_BevHills_01
							vCoords = 		<<-1984.0271, 230.6864, 86.1113>>
							fHeading = 		22.9926
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"amb@world_human_bum_slumped@male@laying_on_left_side@base"
						BREAK			
					ENDSWITCH
				BREAK
				CASE CHS_IMC_VARIATION_2
					SWITCH iPed				
						CASE 0			
							model = 		A_F_Y_BevHills_03
							vCoords = 		<<-1245.7771, 624.1511, 139.8005>>
							fHeading = 		345.5993
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"AMB@WORLD_HUMAN_PARTYING@FEMALE@PARTYING_BEER@IDLE_A"
						BREAK			
						CASE 1			
							model = 		A_F_Y_BevHills_03
							vCoords = 		<<-1227.7555, 615.0831, 137.5443>>
							fHeading = 		149.5986
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 	"amb@world_human_bum_standing@drunk@idle_a"
						BREAK			
						CASE 2			
							model = 		A_F_Y_BevHills_03
							vCoords = 		<<-1225.3066, 627.9747, 137.5457>>
							fHeading = 		179.9987
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 3			
							model = 		A_F_Y_BevHills_03
							vCoords = 		<<-1223.0396, 621.7156, 137.5444>>
							fHeading = 		234.5987
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"AMB@WORLD_HUMAN_PARTYING@FEMALE@PARTYING_BEER@IDLE_A"
						BREAK			
						CASE 4			
							model = 		A_F_Y_BevHills_03
							vCoords = 		<<-1221.5808, 625.4520, 137.5443>>
							fHeading = 		127.9984
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"amb@world_human_drinking@beer@female@idle_a"
						BREAK			
						CASE 5			
							model = 		A_F_Y_Beach_01
							vCoords = 		<<-1228.0721, 621.2917, 137.5443>>
							fHeading = 		115.3982
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_SEAT_LEDGE"
						BREAK			
						CASE 6			
							model = 		A_F_Y_Beach_01
							vCoords = 		<<-1238.4346, 617.2955, 137.5621>>
							fHeading = 		271.3979
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_SEAT_LEDGE"
						BREAK			
						CASE 7			
							model = 		A_F_Y_BevHills_03
							vCoords = 		<<-1230.3657, 627.0688, 137.5448>>
							fHeading = 		274.3979
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"amb@world_human_drinking@beer@female@idle_a"
						BREAK			
						CASE 8			
							model = 		A_F_Y_BevHills_03
							vCoords = 		<<-1229.6025, 627.0201, 137.5448>>
							fHeading = 		90.5974
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 	"amb@world_human_bum_standing@drunk@idle_a"
						BREAK			
						CASE 9			
							model = 		A_F_Y_BevHills_03
							vCoords = 		<<-1239.4728, 621.3275, 137.5454>>
							fHeading = 		311.1972
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"AMB@WORLD_HUMAN_PARTYING@FEMALE@PARTYING_BEER@IDLE_A"
						BREAK			
						CASE 10			
							model = 		A_M_Y_BevHills_02
							vCoords = 		<<-1245.3572, 624.9998, 139.8005>>
							fHeading = 		149.5969
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 	"amb@world_human_bum_standing@drunk@idle_a"
						BREAK			
						CASE 11			
							model = 		A_M_Y_BevHills_02
							vCoords = 		<<-1238.6132, 621.8661, 137.5454>>
							fHeading = 		120.5966
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"AMB@WORLD_HUMAN_PARTYING@MALE@PARTYING_BEER@IDLE_A"
						BREAK			
						CASE 12			
							model = 		A_M_Y_BevHills_02
							vCoords = 		<<-1238.6826, 615.9537, 137.5986>>
							fHeading = 		269.9961
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_SEAT_LEDGE"
						BREAK			
						CASE 13			
							model = 		A_M_M_Beach_02
							vCoords = 		<<-1227.7896, 619.8589, 137.5921>>
							fHeading = 		92.3957
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_SEAT_LEDGE"
						BREAK			
						CASE 14			
							model = 		A_M_M_Beach_02
							vCoords = 		<<-1228.3015, 614.4007, 137.5443>>
							fHeading = 		320.5953
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"amb@world_human_drinking@beer@male@idle_a"
						BREAK			
						CASE 15			
							model = 		A_M_Y_BevHills_02
							vCoords = 		<<-1224.2847, 627.5590, 137.5457>>
							fHeading = 		111.9953
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_SMOKING_POT"
						BREAK			
						CASE 16			
							model = 		A_M_Y_BevHills_02
							vCoords = 		<<-1222.9542, 620.3975, 137.5443>>
							fHeading = 		17.1955
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"AMB@WORLD_HUMAN_PARTYING@MALE@PARTYING_BEER@IDLE_A"
						BREAK			
						CASE 17			
							model = 		A_M_Y_BevHills_02
							vCoords = 		<<-1221.7808, 621.2669, 137.5444>>
							fHeading = 		81.7953
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 	"amb@world_human_bum_standing@drunk@idle_a"
						BREAK			
						CASE 18			
							model = 		A_M_Y_BevHills_02
							vCoords = 		<<-1222.5374, 624.9349, 137.5447>>
							fHeading = 		286.3951
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"amb@world_human_drinking@beer@male@idle_a"
						BREAK			
						CASE 19			
							model = 		A_M_Y_BevHills_02
							vCoords = 		<<-1229.8149, 628.0495, 137.5446>>
							fHeading = 		168.7948
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"AMB@WORLD_HUMAN_PARTYING@MALE@PARTYING_BEER@IDLE_A"
						BREAK			
						CASE 20			
							model = 		A_M_Y_BevHills_01
							vCoords = 		<<-1240.9147, 628.0532, 139.8005>>
							fHeading = 		89.995
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"AMB@WORLD_HUMAN_STUPOR@MALE_LOOKING_RIGHT@IDLE_A"
						BREAK			
						CASE 21			
							model = 		A_M_Y_BevHills_01
							vCoords = 		<<-1232.6840, 631.5599, 137.5447>>
							fHeading = 		205.395
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"AMB@WORLD_HUMAN_STUPOR@MALE@BASE"
						BREAK			
						CASE 22			
							model = 		A_M_Y_BevHills_01
							vCoords = 		<<-1218.4358, 632.5105, 137.5457>>
							fHeading = 		175.795
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"amb@world_human_bum_slumped@male@laying_on_right_side@base"
						BREAK			
						CASE 23			
							model = 		A_M_Y_BevHills_01
							vCoords = 		<<-1223.3358, 617.0759, 137.5443>>
							fHeading = 		35.194
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"amb@world_human_bum_slumped@male@laying_on_left_side@base"
						BREAK			
						CASE 24			
							model = 		A_M_Y_BevHills_01
							vCoords = 		<<-1231.1895, 636.7089, 141.7506>>
							fHeading = 		164.9939
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"AMB@WORLD_HUMAN_STUPOR@MALE@IDLE_A"
						BREAK			
						CASE 25			
							model = 		A_F_Y_BevHills_01
							vCoords = 		<<-1247.6072, 629.5013, 139.6504>>
							fHeading = 		165.794
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"AMB@WORLD_HUMAN_STUPOR@MALE@IDLE_A"
						BREAK			
						CASE 26			
							model = 		A_F_Y_BevHills_01
							vCoords = 		<<-1220.5365, 638.6677, 141.6500>>
							fHeading = 		158.5939
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"AMB@WORLD_HUMAN_STUPOR@MALE@BASE"
						BREAK			
						CASE 27			
							model = 		A_F_Y_BevHills_01
							vCoords = 		<<-1219.9492, 636.3511, 141.7580>>
							fHeading = 		70.3939
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"amb@world_human_bum_slumped@male@laying_on_right_side@base"
						BREAK			
						CASE 28			
							model = 		A_F_Y_BevHills_01
							vCoords = 		<<-1237.9539, 627.6002, 137.4451>>
							fHeading = 		227.3929
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"amb@world_human_bum_slumped@male@laying_on_left_side@base"
						BREAK			
						CASE 29			
							model = 		A_F_Y_BevHills_01
							vCoords = 		<<-1221.5239, 630.9381, 137.4457>>
							fHeading = 		301.9929
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"AMB@WORLD_HUMAN_STUPOR@MALE_LOOKING_LEFT@IDLE_A"
						BREAK			
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_NOOSE_OUTFITS
			SWITCH eSubvariation
				CASE CHS_NOOSE_VARIATION_1
					SWITCH iPed				
						CASE 0			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<459.5256, -981.0970, 29.6896>>
							fHeading = 		275.198
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 1			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<436.5175, -986.4150, 29.6896>>
							fHeading = 		83.1988
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_WINDOW_SHOP_BROWSE"
						BREAK			
						CASE 2			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<446.5177, -982.4299, 29.6896>>
							fHeading = 		225.5977
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 3			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<447.5372, -983.4433, 29.6896>>
							fHeading = 		43.3977
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 4			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<444.5430, -995.2350, 29.6900>>
							fHeading = 		183.197
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 5			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<441.2923, -987.6406, 29.6896>>
							fHeading = 		2.9968
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
						BREAK			
						CASE 6			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<458.2460, -990.7520, 29.6900>>
							fHeading = 		141.197
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 7			
							model = 		S_M_Y_Swat_01
							vCoords = 		<<457.4840, -991.7640, 29.6900>>
							fHeading = 		312.197
							weapon = 		WEAPONTYPE_ADVANCEDRIFLE
							sScenario = 		"WORLD_HUMAN_STAND_IMPATIENT"
						BREAK			
						CASE 8			
							model = 		S_M_Y_Swat_01
							vCoords = 		<<451.1530, -992.0020, 29.6900>>
							fHeading = 		277.997
							weapon = 		WEAPONTYPE_ADVANCEDRIFLE
							sScenario = 		"WORLD_HUMAN_PUSH_UPS"
						BREAK			
						CASE 9			
							model = 		S_F_Y_Cop_01
							vCoords = 		<<439.8897, -995.0646, 29.6896>>
							fHeading = 		51.3956
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 10			
							model = 		S_F_Y_Cop_01
							vCoords = 		<<444.1029, -975.3378, 29.6896>>
							fHeading = 		4.9958
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_WINDOW_SHOP_BROWSE"
						BREAK			
						CASE 11			
							model = 		S_F_Y_Cop_01
							vCoords = 		<<458.3160, -985.3140, 29.6900>>
							fHeading = 		11.596
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 12			
							model = 		S_M_Y_Swat_01
							vCoords = 		<<451.2820, -976.1850, 29.6896>>
							fHeading = 		173.9977
							weapon = 		WEAPONTYPE_ADVANCEDRIFLE
							sScenario = 		"WORLD_HUMAN_SIT_UPS"
						BREAK			
						CASE 13			
							model = 		S_M_Y_Swat_01
							vCoords = 		<<438.9787, -994.2447, 29.6896>>
							fHeading = 		229.1976
							weapon = 		WEAPONTYPE_ADVANCEDRIFLE
							sScenario = 		"WORLD_HUMAN_MUSCLE_FLEX"
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_CELEB_AFTER_PARTY
			SWITCH iPed				
				CASE 0			
					model = 		A_M_M_Paparazzi_01
					vCoords = 		<<764.0983, -27.6755, 81.1616>>
					fHeading = 		181.5998
					weapon = 		WEAPONTYPE_UNARMED
					sScenario = 		""
				BREAK			
				CASE 1			
					model = 		A_M_M_Paparazzi_01
					vCoords = 		<<1117.2035, 406.8876, 91.0414>>
					fHeading = 		181.5998
					weapon = 		WEAPONTYPE_UNARMED
					sScenario = 		""
				BREAK			
				CASE 2			
					model = 		A_M_M_Paparazzi_01
					vCoords = 		<<820.5889, -75.8835, 80.6720>>
					fHeading = 		181.5998
					weapon = 		WEAPONTYPE_UNARMED
					sScenario = 		""
				BREAK			
				CASE 3			
					model = 		A_M_M_Paparazzi_01
					vCoords = 		<<820.1974, -76.4171, 81.0424>>
					fHeading = 		181.5998
					weapon = 		WEAPONTYPE_UNARMED
					sScenario = 		""
				BREAK			
				CASE 4			
					model = 		A_M_M_Paparazzi_01
					vCoords = 		<<1079.4165, 272.8305, 87.7809>>
					fHeading = 		333.6723
					weapon = 		WEAPONTYPE_PISTOL
					sScenario = 		""
				BREAK			
				CASE 5			
					model = 		A_M_M_Paparazzi_01
					vCoords = 		<<1079.8312, 272.3289, 87.9300>>
					fHeading = 		333.6723
					weapon = 		WEAPONTYPE_UNARMED
					sScenario = 		""
				BREAK			
				CASE 6			
					model = 		S_M_M_Pilot_01
					vCoords = 		<<-285.5739, -619.6708, 51.1548>>
					fHeading = 		333.6723
					weapon = 		WEAPONTYPE_UNARMED
					sScenario = 		""
				BREAK			
				CASE 7			
					model = 		S_M_Y_Cop_01
					vCoords = 		<<-1124.8870, -857.0914, 12.5367>>
					fHeading = 		148.3264
					weapon = 		WEAPONTYPE_PUMPSHOTGUN
					sScenario = 		"WORLD_HUMAN_DRINKING"
				BREAK			
				CASE 8			
					model = 		S_M_Y_Cop_01
					vCoords = 		<<-1125.3538, -858.3229, 12.5513>>
					fHeading = 		346.126
					weapon = 		WEAPONTYPE_PISTOL
					sScenario = 		"WORLD_HUMAN_COP_IDLES"
				BREAK			
				CASE 9			
					model = 		S_M_Y_Cop_01
					vCoords = 		<<-1142.5671, -845.2712, 13.3637>>
					fHeading = 		76.9256
					weapon = 		WEAPONTYPE_PUMPSHOTGUN
					sScenario = 		"WORLD_HUMAN_DRINKING"
				BREAK			
				CASE 10			
					model = 		S_M_Y_Cop_01
					vCoords = 		<<-1143.6364, -844.6091, 13.4141>>
					fHeading = 		222.7254
					weapon = 		WEAPONTYPE_PISTOL
					sScenario = 		"WORLD_HUMAN_COP_IDLES"
				BREAK			
				CASE 11			
					model = 		S_M_Y_Cop_01
					vCoords = 		<<-1114.3730, -847.0674, 12.4113>>
					fHeading = 		142.3251
					weapon = 		WEAPONTYPE_PISTOL
					sScenario = 		"WORLD_HUMAN_STAND_MOBILE"
				BREAK			
			ENDSWITCH				
		BREAK
		CASE CHV_MAINTENANCE_OUTFITS_2
			SWITCH eSubvariation
				CASE CHS_MO2_VARIATION_1
					SWITCH iPed				
						CASE 0			
							model = 		S_M_Y_Construct_01
							vCoords = 		<<-56.9130, -2654.3921, 5.0010>>
							fHeading = 		170.799
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 1			
							model = 		S_M_Y_Construct_01
							vCoords = 		<<-68.8450, -2655.5100, 5.0010>>
							fHeading = 		1.399
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 2			
							model = 		S_M_Y_Construct_01
							vCoords = 		<<-64.2620, -2651.9399, 5.0010>>
							fHeading = 		323.599
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 3			
							model = 		S_M_Y_Construct_01
							vCoords = 		<<-63.5270, -2650.8779, 5.0010>>
							fHeading = 		143.998
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 4			
							model = 		S_M_Y_Construct_01
							vCoords = 		<<-54.8930, -2651.6279, 5.0010>>
							fHeading = 		286.598
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"CODE_HUMAN_MEDIC_TIME_OF_DEATH"
						BREAK			
					ENDSWITCH
				BREAK
				CASE CHS_MO2_VARIATION_2
					SWITCH iPed				
						CASE 0			
							model = 		S_M_Y_Construct_01
							vCoords = 		<<1694.7841, -1729.5250, 111.5560>>
							fHeading = 		122.198
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 1			
							model = 		S_M_Y_Construct_01
							vCoords = 		<<1699.1030, -1726.0250, 111.5970>>
							fHeading = 		57.198
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 2			
							model = 		S_M_Y_Construct_01
							vCoords = 		<<1697.9969, -1725.5000, 111.5810>>
							fHeading = 		244.398
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 3			
							model = 		S_M_Y_Construct_01
							vCoords = 		<<1690.9790, -1725.0959, 111.5360>>
							fHeading = 		35.997
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"CODE_HUMAN_MEDIC_TIME_OF_DEATH"
						BREAK			
						CASE 4			
							model = 		S_M_Y_Construct_01
							vCoords = 		<<1700.4270, -1736.2460, 111.5860>>
							fHeading = 		276.397
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
					ENDSWITCH								
				BREAK
				CASE CHS_MO2_VARIATION_3
					SWITCH iPed				
						CASE 0			
							model = 		S_M_Y_Construct_01
							vCoords = 		<<868.5210, -2336.0720, 29.3430>>
							fHeading = 		336.398
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_CLIPBOARD"
						BREAK			
						CASE 1			
							model = 		S_M_Y_Construct_01
							vCoords = 		<<868.2140, -2341.2949, 29.3320>>
							fHeading = 		295.998
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 2			
							model = 		S_M_Y_Construct_01
							vCoords = 		<<869.2850, -2340.6731, 29.3340>>
							fHeading = 		118.798
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 3			
							model = 		S_M_Y_Construct_01
							vCoords = 		<<874.0220, -2330.3669, 29.3460>>
							fHeading = 		223.798
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"CODE_HUMAN_MEDIC_TIME_OF_DEATH"
						BREAK			
						CASE 4			
							model = 		S_M_Y_Construct_01
							vCoords = 		<<864.8940, -2326.8291, 29.3460>>
							fHeading = 		177.598
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
					ENDSWITCH							
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_BUGSTAR_OUTFITS_2
			SWITCH eSubvariation
				CASE CHS_BO2_VARIATION_1
					SWITCH iPed
						CASE 0			
							model = 		S_M_Y_XMech_01
							vCoords = 		<<-452.9459, -1698.3210, 17.9021>>
							fHeading = 		156.7988
							weapon = 		WEAPONTYPE_DLC_WRENCH
							sScenario = 		"amb@world_human_vehicle_mechanic@male@base"
						BREAK			
						CASE 1			
							model = 		S_M_Y_XMech_01
							vCoords = 		<<-450.5099, -1700.2419, 17.9049>>
							fHeading = 		-147.4019
							weapon = 		WEAPONTYPE_DLC_WRENCH
							sScenario = 		"CODE_HUMAN_MEDIC_KNEEL"
						BREAK			
						CASE 2			
							model = 		S_M_Y_XMech_01
							vCoords = 		<<-450.8187, -1692.0441, 17.9519>>
							fHeading = 		162.7978
							weapon = 		WEAPONTYPE_DLC_WRENCH
							sScenario = 		"WORLD_HUMAN_WINDOW_SHOP_BROWSE"
						BREAK			
						CASE 3			
							model = 		S_M_Y_Garbage
							vCoords = 		<<-462.1817, -1689.2816, 17.9438>>
							fHeading = 		165.1999
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
						CASE 4			
							model = 		S_M_Y_Garbage
							vCoords = 		<<-467.0273, -1693.6381, 17.8938>>
							fHeading = 		80.9996
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 5			
							model = 		S_M_Y_Garbage
							vCoords = 		<<-468.2792, -1693.5182, 17.8848>>
							fHeading = 		263.7992
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
					ENDSWITCH
				BREAK
				CASE CHS_BO2_VARIATION_2
					SWITCH iPed
						CASE 0			
							model = 		S_M_Y_XMech_01
							vCoords = 		<<-508.2643, -1732.7173, 18.0883>>
							fHeading = 		319.3991
							weapon = 		WEAPONTYPE_DLC_WRENCH
							sScenario = 		"amb@world_human_vehicle_mechanic@male@base"
						BREAK			
						CASE 1			
							model = 		S_M_Y_XMech_01
							vCoords = 		<<-512.0393, -1737.9635, 18.2724>>
							fHeading = 		323.9983
							weapon = 		WEAPONTYPE_DLC_WRENCH
							sScenario = 		"WORLD_HUMAN_WINDOW_SHOP_BROWSE"
						BREAK			
						CASE 2			
							model = 		S_M_Y_XMech_01
							vCoords = 		<<-505.8047, -1737.3713, 17.9584>>
							fHeading = 		158.7976
							weapon = 		WEAPONTYPE_DLC_WRENCH
							sScenario = 		"CODE_HUMAN_MEDIC_KNEEL"
						BREAK			
						CASE 3			
							model = 		S_M_Y_Garbage
							vCoords = 		<<-507.1451, -1723.6267, 18.3251>>
							fHeading = 		212.3995
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 4			
							model = 		S_M_Y_Garbage
							vCoords = 		<<-506.5356, -1724.4666, 18.3584>>
							fHeading = 		31.1993
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 5			
							model = 		S_M_Y_Garbage
							vCoords = 		<<-501.3901, -1718.7174, 18.3203>>
							fHeading = 		109.3991
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
					ENDSWITCH
				BREAK
				CASE CHS_BO2_VARIATION_3
					SWITCH iPed
						CASE 0			
							model = 		S_M_Y_XMech_01
							vCoords = 		<<-649.7794, -1713.5759, 23.6585>>
							fHeading = 		342.9984
							weapon = 		WEAPONTYPE_DLC_WRENCH
							sScenario = 		"amb@world_human_vehicle_mechanic@male@base"
						BREAK			
						CASE 1			
							model = 		S_M_Y_XMech_01
							vCoords = 		<<-651.3679, -1719.8601, 23.6630>>
							fHeading = 		341.7983
							weapon = 		WEAPONTYPE_DLC_WRENCH
							sScenario = 		"WORLD_HUMAN_WINDOW_SHOP_BROWSE"
						BREAK			
						CASE 2			
							model = 		S_M_Y_XMech_01
							vCoords = 		<<-647.4766, -1710.9381, 23.6349>>
							fHeading = 		21.3981
							weapon = 		WEAPONTYPE_DLC_WRENCH
							sScenario = 		"CODE_HUMAN_MEDIC_KNEEL"
						BREAK			
						CASE 3			
							model = 		S_M_Y_Garbage
							vCoords = 		<<-642.6048, -1726.1827, 23.4039>>
							fHeading = 		-2.6019
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_HANG_OUT_STREET"
						BREAK			
						CASE 4			
							model = 		S_M_Y_Garbage
							vCoords = 		<<-642.6591, -1724.8367, 23.4145>>
							fHeading = 		183.9979
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_DRINKING"
						BREAK			
						CASE 5			
							model = 		S_M_Y_Garbage
							vCoords = 		<<-635.6046, -1721.6844, 23.2714>>
							fHeading = 		52.9976
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		"WORLD_HUMAN_SMOKING"
						BREAK			
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_VAULT_DRILL_1
			SWITCH eSubvariation
				CASE CHS_VD1_VARIATION_1
					SWITCH iPed				
						CASE 0			
							model = 		mp_g_m_pros_01
							vCoords = 		<<765.2330, -3195.9700, 5.0220>>
							fHeading = 		266.799
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 1			
							model = 		mp_g_m_pros_01
							vCoords = 		<<765.2330, -3194.3550, 5.0220>>
							fHeading = 		266.799
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 2			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<767.8800, -3196.8650, 4.9010>>
							fHeading = 		77.799
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 3			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<768.2890, -3193.9351, 4.9010>>
							fHeading = 		91.599
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 4			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<753.2030, -3194.9861, 5.0780>>
							fHeading = 		83.595
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_MEDIC_TIME_OF_DEATH"
						BREAK			
						CASE 5			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<760.5150, -3192.9690, 5.0780>>
							fHeading = 		70.393
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_MEDIC_TEND_TO_DEAD"
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_VD1_VARIATION_2
					SWITCH iPed				
						CASE 0			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<-1349.4000, -896.2210, 12.5550>>
							fHeading = 		17.4
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 1			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<-1347.7020, -899.5120, 12.4890>>
							fHeading = 		254.599
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		""
						BREAK			
						CASE 2			
							model = 		mp_g_m_pros_01
							vCoords = 		<<-1349.9041, -894.8190, 12.5620>>
							fHeading = 		196.599
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 3			
							model = 		mp_g_m_pros_01
							vCoords = 		<<-1346.1260, -899.9610, 12.4540>>
							fHeading = 		73.399
							weapon = 		WEAPONTYPE_UNARMED
							sScenario = 		""
						BREAK			
						CASE 4			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<-1340.9189, -894.8040, 12.4380>>
							fHeading = 		317.999
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_MEDIC_TIME_OF_DEATH"
						BREAK			
						CASE 5			
							model = 		S_M_Y_Cop_01
							vCoords = 		<<-1344.7450, -891.9540, 12.4750>>
							fHeading = 		317.999
							weapon = 		WEAPONTYPE_PISTOL
							sScenario = 		"CODE_HUMAN_MEDIC_TEND_TO_DEAD"
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH

	CSH_PED_DATA data
	data.model = model
	data.vCoords = vCoords
	data.fHeading = fHeading
	data.weapon = weapon
	data.sScenario = sScenario
	
	RETURN data

ENDFUNC

FUNC VECTOR GET_CSH_PED_SPAWN_COORDS(INT iPed, CSH_VARIATION eVariation, CSH_SUBVARIATION eSubvariation, FREEMODE_DELIVERY_DROPOFFS eDropOff = FREEMODE_DELIVERY_DROPOFF_INVALID, INT iRespawnLocation = 0, INT iExtraParam = -1)

	UNUSED_PARAMETER(eDropOff)
	UNUSED_PARAMETER(iRespawnLocation)
	UNUSED_PARAMETER(iExtraParam)

	CSH_PED_DATA data = GET_CSH_PED_DATA(iPed, eVariation, eSubvariation, iExtraParam)
	RETURN data.vCoords

ENDFUNC

FUNC FLOAT GET_CSH_PED_SPAWN_HEADING(INT iPed, CSH_VARIATION eVariation, CSH_SUBVARIATION eSubvariation, FREEMODE_DELIVERY_DROPOFFS eDropOff = FREEMODE_DELIVERY_DROPOFF_INVALID, INT iRespawnLocation = 0, INT iExtraParam = -1)

	UNUSED_PARAMETER(eDropOff)
	UNUSED_PARAMETER(iRespawnLocation)
	UNUSED_PARAMETER(iExtraParam)

	CSH_PED_DATA data = GET_CSH_PED_DATA(iPed, eVariation, eSubvariation, iExtraParam)
	RETURN data.fHeading
	
ENDFUNC

FUNC MODEL_NAMES GET_CSH_AMBUSH_PED_MODEL(INT iAmbushPedBit, CSH_VARIATION eVariation, CSH_SUBVARIATION eSubvariation, MODEL_NAMES eVehicleModel = DUMMY_MODEL_FOR_SCRIPT, INT iExtraParam = -1)
	UNUSED_PARAMETER(eSubvariation)
//	UNUSED_PARAMETER(iAmbushPedBit)
	UNUSED_PARAMETER(eVehicleModel)
	UNUSED_PARAMETER(iExtraParam)
	
	SWITCH eVariation
		CASE CHV_ARCADE_CABINETS
			SWITCH eSubvariation
				CASE CHS_AC_ELYSIAN_ISLAND				RETURN G_M_Y_BallaOrig_01
				CASE CHS_AC_DOWNTOWN_VINEWOOD			RETURN G_M_M_ChiGoon_02
				CASE CHS_AC_PALETO_BAY					RETURN G_M_Y_Lost_02
				CASE CHS_AC_LA_MESA						RETURN G_M_Y_BallaOrig_01
				CASE CHS_AC_CHUMASH						RETURN G_M_Y_Korean_01
			ENDSWITCH
		BREAK
		CASE CHV_LOST_MC_BIKES
			IF IS_INT_EVEN(iAmbushPedBit)
				RETURN G_M_Y_Lost_01
			ENDIF
			
			RETURN G_F_Y_LOST_01
		CASE CHV_WEAPONS_SMUGGLERS				RETURN G_M_M_ArmGoon_01
		CASE CHV_WEAPONS_STASH
			SWITCH eSubvariation
				CASE CHS_WST_SAN_ANDREAS		RETURN G_M_Y_MexGoon_02
				CASE CHS_WST_PALETO_BAY			RETURN G_M_Y_Lost_01
			ENDSWITCH
		BREAK
		CASE CHV_CARGO_SHIP
			SWITCH eSubvariation
				CASE CHS_CS_VARIATION_1			RETURN G_M_M_ChiGoon_02
				CASE CHS_CS_VARIATION_2			RETURN G_M_Y_Lost_01
				CASE CHS_CS_VARIATION_3			RETURN G_M_M_ArmGoon_01
			ENDSWITCH
		BREAK
		CASE CHV_MERRYWEATHER_TEST_SITE			RETURN S_M_Y_BLACKOPS_01
		CASE CHV_STEALTH_OUTFITS				RETURN S_M_Y_BLACKOPS_02
		CASE CHV_PLASTIC_EXPLOSIVES				RETURN S_M_Y_BLACKOPS_01
		CASE CHV_ARMORED_EQUIPMENT_1			RETURN S_M_Y_BLACKOPS_01
		CASE CHV_EXPLOSIVES_2					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("MP_M_SecuroGuard_01"))
		CASE CHV_ARMORED_EQUIPMENT_2			RETURN S_M_Y_BLACKOPS_01
		CASE CHV_EXPLOSIVES_1					RETURN A_M_M_HILLBILLY_02
		CASE CHV_SECURITY_CAMERAS
			IF IS_INT_EVEN(iAmbushPedBit)
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("g_m_importexport_01")) 
			ENDIF
			
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("g_f_importexport_01"))
		CASE CHV_VAULT_DRILL_1					RETURN mp_g_m_pros_01
		CASE CHV_VAULT_DRILL_2					RETURN mp_g_m_pros_01
		CASE CHV_VAULT_LASER_2					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_avongoon"))
		CASE CHV_FLIGHT_SCHEDULE				RETURN S_M_Y_BLACKOPS_01
		CASE CHV_MERRYWEATHER_CONVOY			RETURN s_m_m_chemsec_01
		CASE CHV_DRAG_RACE						RETURN G_M_M_ChiGoon_02
	ENDSWITCH
	
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

FUNC MODEL_NAMES GET_CSH_PED_MODEL(INT iPed, CSH_VARIATION eVariation, CSH_SUBVARIATION eSubvariation, INT iExtraParam = -1)

	UNUSED_PARAMETER(iExtraParam)
	
	CSH_PED_DATA data = GET_CSH_PED_DATA(iPed, eVariation, eSubvariation, iExtraParam)
	RETURN data.model

ENDFUNC

FUNC VECTOR GET_CSH_PED_RESPAWN_COORDS(INT iPed, CSH_VARIATION eVariation, CSH_SUBVARIATION eSubvariation, FREEMODE_DELIVERY_DROPOFFS eDropOff = FREEMODE_DELIVERY_DROPOFF_INVALID, INT iRespawnLocation = 0)
	
	UNUSED_PARAMETER(iPed)
	UNUSED_PARAMETER(eVariation)
	UNUSED_PARAMETER(eSubvariation)
	UNUSED_PARAMETER(eDropOff)
	UNUSED_PARAMETER(iRespawnLocation)

//	SWITCH eVariation
//		
//	ENDSWITCH
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC FLOAT GET_CSH_PED_RESPAWN_HEADING(INT iPed, CSH_VARIATION eVariation, CSH_SUBVARIATION eSubvariation, FREEMODE_DELIVERY_DROPOFFS eDropOff = FREEMODE_DELIVERY_DROPOFF_INVALID, INT iRespawnLocation = 0)

	UNUSED_PARAMETER(iPed)
	UNUSED_PARAMETER(eVariation)
	UNUSED_PARAMETER(eSubvariation)
	UNUSED_PARAMETER(eDropOff)
	UNUSED_PARAMETER(iRespawnLocation)

//	SWITCH eVariation
//		
//	ENDSWITCH
	
	RETURN 0.0
ENDFUNC

FUNC VECTOR GET_CSH_PED_COWER_TASK_COORDS(INT iPed, CSH_VARIATION eVariation, CSH_SUBVARIATION eSubvariation, INT iExtraParam = -1)

	UNUSED_PARAMETER(iExtraParam)
	UNUSED_PARAMETER(iPed)

	SWITCH eVariation
		CASE CHV_MAX
			SWITCH eSubvariation
				CASE CHS_MAX
				
				BREAK
			ENDSWITCH		
		BREAK
	ENDSWITCH

	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

STRUCT CSH_PROP_DATA

	MODEL_NAMES model
	VECTOR vCoords
	FLOAT fHeading
	VECTOR vRotation

ENDSTRUCT

FUNC CSH_PROP_DATA GET_CSH_PROP_DATA(INT iProp, CSH_VARIATION eVariation, CSH_SUBVARIATION eSubvariation, INT iExtraParam)

	MODEL_NAMES model = DUMMY_MODEL_FOR_SCRIPT
	VECTOR vCoords = <<0.0, 0.0, 0.0>>
	FLOAT fHeading = 0.0
	VECTOR vRotate = <<0.0, 0.0, 0.0>>

	SWITCH eVariation
		CASE CHV_ARCADE_CABINETS
			SWITCH eSubvariation
				CASE CHS_AC_ELYSIAN_ISLAND
					SWITCH iProp				
						CASE 0			
							model = 		prop_box_wood04a
							vCoords = 		<<-480.8990, -2816.9651, 5.0030>>
							fHeading = 		190.999
							vRotate = 		<<0.0000, 0.0000, -169.0010>>
						BREAK			
						CASE 1			
							model = 		prop_boxpile_07d
							vCoords = 		<<-481.0050, -2829.3391, 5.0010>>
							fHeading = 		223.599
							vRotate = 		<<0.0000, 0.0000, -136.4010>>
						BREAK			
						CASE 2			
							model = 		prop_boxpile_07d
							vCoords = 		<<-491.3200, -2828.2200, 5.0010>>
							fHeading = 		135.199
							vRotate = 		<<0.0000, 0.0000, 135.1990>>
						BREAK			
						CASE 3			
							model = 		prop_boxpile_07d
							vCoords = 		<<-471.6780, -2827.7771, 6.3020>>
							fHeading = 		43.999
							vRotate = 		<<0.0000, 0.0000, 43.9990>>
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_AC_DOWNTOWN_VINEWOOD
					SWITCH iProp				
						CASE 0			
							model = 		prop_boxpile_07d
							vCoords = 		<<73.9790, 116.5810, 78.1540>>
							fHeading = 		242.8
							vRotate = 		<<0.0000, 0.0000, -117.2000>>
						BREAK			
						CASE 1			
							model = 		prop_boxpile_07d
							vCoords = 		<<59.6860, 126.5730, 78.2400>>
							fHeading = 		238.6
							vRotate = 		<<0.0000, 0.0000, -121.4000>>
						BREAK			
						CASE 2			
							model = 		prop_boxpile_07d
							vCoords = 		<<68.4260, 125.8470, 78.1900>>
							fHeading = 		158
							vRotate = 		<<0.0000, 0.0000, 158.0000>>
						BREAK			
						CASE 3			
							model = 		prop_box_wood04a
							vCoords = 		<<65.7230, 118.8690, 78.1560>>
							fHeading = 		218
							vRotate = 		<<0.0000, 0.0000, -142.0000>>
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_AC_PALETO_BAY
					SWITCH iProp				
						CASE 0			
							model = 		prop_boxpile_07d
							vCoords = 		<<-433.5090, 6140.2529, 30.4790>>
							fHeading = 		223.4
							vRotate = 		<<0.0000, 0.0000, -136.6000>>
						BREAK			
						CASE 1			
							model = 		prop_boxpile_07d
							vCoords = 		<<-442.0930, 6137.0239, 30.4790>>
							fHeading = 		135.4
							vRotate = 		<<0.0000, 0.0000, 135.4000>>
						BREAK			
						CASE 2			
							model = 		prop_boxpile_07d
							vCoords = 		<<-432.1320, 6130.6592, 30.4790>>
							fHeading = 		135.4
							vRotate = 		<<0.0000, 0.0000, 135.4000>>
						BREAK			
						CASE 3			
							model = 		prop_box_wood04a
							vCoords = 		<<-424.1590, 6129.8999, 30.4780>>
							fHeading = 		15.399
							vRotate = 		<<0.0000, 0.0000, 15.3990>>
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_AC_LA_MESA
					SWITCH iProp				
						CASE 0			
							model = 		prop_boxpile_07d
							vCoords = 		<<906.9270, -1261.2629, 24.6000>>
							fHeading = 		124.6
							vRotate = 		<<0.0000, 0.0000, 124.6000>>
						BREAK			
						CASE 1			
							model = 		prop_boxpile_07d
							vCoords = 		<<917.2520, -1254.4550, 24.5210>>
							fHeading = 		130.8
							vRotate = 		<<0.0000, 0.0000, 130.8000>>
						BREAK			
						CASE 2			
							model = 		prop_boxpile_07d
							vCoords = 		<<906.0160, -1258.1670, 24.5960>>
							fHeading = 		35
							vRotate = 		<<0.0000, 0.0000, 35.0000>>
						BREAK			
						CASE 3			
							model = 		prop_box_wood04a
							vCoords = 		<<910.8260, -1261.1320, 24.5610>>
							fHeading = 		264.399
							vRotate = 		<<0.0000, 0.0000, -95.6010>>
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_AC_CHUMASH
					SWITCH iProp				
						CASE 0			
							model = 		prop_boxpile_07d
							vCoords = 		<<-3152.5271, 1133.9480, 19.8490>>
							fHeading = 		64.199
							vRotate = 		<<0.0000, 0.0000, 64.1990>>
						BREAK			
						CASE 1			
							model = 		prop_boxpile_07d
							vCoords = 		<<-3166.4131, 1137.7230, 20.1010>>
							fHeading = 		334.799
							vRotate = 		<<0.0000, 0.0000, -25.2010>>
						BREAK			
						CASE 2			
							model = 		prop_boxpile_07d
							vCoords = 		<<-3163.2791, 1125.3459, 19.9060>>
							fHeading = 		65.999
							vRotate = 		<<0.0000, 0.0000, 65.9990>>
						BREAK			
						CASE 3			
							model = 		prop_box_wood04a
							vCoords = 		<<-3160.2812, 1134.7423, 20.0390>>
							fHeading = 		229.62
							vRotate = 		<<-1.4000, 1.0000, -130.3800>>
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK

		CASE CHV_MAINTENANCE_OUTFITS_2
			SWITCH eSubvariation
				CASE CHS_MO2_VARIATION_1				
					SWITCH iProp				
						CASE 0			
							model = 		prop_barrel_pile_01
							vCoords = 		<<-60.9200, -2656.3560, 5.3030>>
							fHeading = 		181.398
							vRotate = 		<<0.0000, 0.0000, -178.6020>>
						BREAK			
						CASE 1			
							model = 		prop_barrel_pile_03
							vCoords = 		<<-53.0200, -2651.6440, 5.0000>>
							fHeading = 		78.798
							vRotate = 		<<0.0000, 0.0000, 78.7980>>
						BREAK			
						CASE 2			
							model = 		prop_barrel_pile_04
							vCoords = 		<<-62.6220, -2651.8940, 5.0010>>
							fHeading = 		255.598
							vRotate = 		<<0.0000, 0.0000, -104.4020>>
						BREAK			
						CASE 3			
							model = 		prop_barrel_pile_04
							vCoords = 		<<-55.1900, -2661.4121, 5.0050>>
							fHeading = 		107.198
							vRotate = 		<<0.0000, 0.0000, 107.1980>>
						BREAK			
						CASE 4			
							model = 		prop_jerrycan_01a
							vCoords = 		<<-54.1290, -2651.4839, 5.0120>>
							fHeading = 		281.998
							vRotate = 		<<0.0000, 0.0000, -78.0020>>
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_MO2_VARIATION_2			
					SWITCH iProp				
						CASE 0			
							model = 		prop_barrel_pile_04
							vCoords = 		<<1697.5160, -1734.0300, 111.5600>>
							fHeading = 		113
							vRotate = 		<<0.0000, 0.0000, 113.0000>>
						BREAK			
						CASE 1			
							model = 		prop_barrel_pile_04
							vCoords = 		<<1698.7880, -1724.7841, 111.5800>>
							fHeading = 		170.599
							vRotate = 		<<0.0000, 0.0000, 170.5990>>
						BREAK			
						CASE 2			
							model = 		prop_barrel_pile_03
							vCoords = 		<<1689.9110, -1723.6110, 111.5350>>
							fHeading = 		198.599
							vRotate = 		<<0.0000, 0.0000, -161.4010>>
						BREAK			
						CASE 3			
							model = 		prop_barrel_pile_01
							vCoords = 		<<1700.1960, -1734.4580, 111.9180>>
							fHeading = 		98.399
							vRotate = 		<<0.0000, 0.0000, 98.3990>>
						BREAK			
						CASE 4			
							model = 		prop_jerrycan_01a
							vCoords = 		<<1690.5490, -1724.4561, 111.5470>>
							fHeading = 		212.397
							vRotate = 		<<0.0000, 0.0000, -147.6020>>
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_MO2_VARIATION_3									
					SWITCH iProp				
						CASE 0			
							model = 		prop_barrel_pile_04
							vCoords = 		<<869.5390, -2341.7500, 29.3320>>
							fHeading = 		226.199
							vRotate = 		<<0.0000, 0.0000, -133.8010>>
						BREAK			
						CASE 1			
							model = 		prop_barrel_pile_03
							vCoords = 		<<875.0840, -2330.9419, 29.3450>>
							fHeading = 		145.199
							vRotate = 		<<0.0000, 0.0000, 145.1990>>
						BREAK			
						CASE 2			
							model = 		prop_barrel_pile_01
							vCoords = 		<<867.7440, -2327.1479, 29.6480>>
							fHeading = 		174.199
							vRotate = 		<<0.0000, 0.0000, 174.1990>>
						BREAK			
						CASE 3			
							model = 		prop_barrel_pile_04
							vCoords = 		<<865.8140, -2335.1770, 29.3450>>
							fHeading = 		300.199
							vRotate = 		<<0.0000, 0.0000, -59.8010>>
						BREAK			
						CASE 4			
							model = 		prop_jerrycan_01a
							vCoords = 		<<874.5520, -2330.8240, 29.3570>>
							fHeading = 		225.999
							vRotate = 		<<0.0000, 0.0000, -134.0010>>
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK	
			
		CASE CHV_LOST_MC_BIKES
		
			model = INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Ch_Ramp_Lock_01a")) 	
			
			SWITCH iProp				
				CASE 0						
				CASE 1			
					vCoords = 		GET_CSH_VEHICLE_SPAWN_COORDS(eVariation, eSubvariation, LMC_BENSON_ONE, DEFAULT, iExtraParam)
					fHeading = 		GET_CSH_VEHICLE_SPAWN_HEADING(eVariation, eSubvariation, LMC_BENSON_ONE, DEFAULT, iExtraParam)
					vRotate = 		<<0.0000, 0.0000, 0.0000>>
				BREAK	
				CASE 2			
				CASE 3			
					vCoords = 		GET_CSH_VEHICLE_SPAWN_COORDS(eVariation, eSubvariation, LMC_BENSON_TWO, DEFAULT, iExtraParam)
					fHeading = 		GET_CSH_VEHICLE_SPAWN_HEADING(eVariation, eSubvariation, LMC_BENSON_TWO, DEFAULT, iExtraParam)
					vRotate = 		<<0.0000, 0.0000, 0.0000>>
				BREAK
				CASE 4			
				CASE 5			
					vCoords = 		GET_CSH_VEHICLE_SPAWN_COORDS(eVariation, eSubvariation, LMC_BENSON_THREE, DEFAULT, iExtraParam)
					fHeading = 		GET_CSH_VEHICLE_SPAWN_HEADING(eVariation, eSubvariation, LMC_BENSON_THREE, DEFAULT, iExtraParam)
					vRotate = 		<<0.0000, 0.0000, 0.0000>>
				BREAK
				CASE 6			
				CASE 7			
					vCoords = 		GET_CSH_VEHICLE_SPAWN_COORDS(eVariation, eSubvariation, LMC_BENSON_FOUR, DEFAULT, iExtraParam)
					fHeading = 		GET_CSH_VEHICLE_SPAWN_HEADING(eVariation, eSubvariation, LMC_BENSON_FOUR, DEFAULT, iExtraParam)
					vRotate = 		<<0.0000, 0.0000, 0.0000>>
				BREAK
				CASE 8			
				CASE 9			
					vCoords = 		GET_CSH_VEHICLE_SPAWN_COORDS(eVariation, eSubvariation, LMC_BENSON_FIVE, DEFAULT, iExtraParam)
					fHeading = 		GET_CSH_VEHICLE_SPAWN_HEADING(eVariation, eSubvariation, LMC_BENSON_FIVE, DEFAULT, iExtraParam)
					vRotate = 		<<0.0000, 0.0000, 0.0000>>
				BREAK
				CASE 10			
				CASE 11			
					vCoords = 		GET_CSH_VEHICLE_SPAWN_COORDS(eVariation, eSubvariation, LMC_BENSON_SIX, DEFAULT, iExtraParam)
					fHeading = 		GET_CSH_VEHICLE_SPAWN_HEADING(eVariation, eSubvariation, LMC_BENSON_SIX, DEFAULT, iExtraParam)
					vRotate = 		<<0.0000, 0.0000, 0.0000>>
				BREAK
			ENDSWITCH	
		BREAK
		CASE CHV_INSIDE_MAID
			SWITCH iProp				
				CASE 0			
					model = 		bkr_prop_coke_painkiller_01a
					vCoords = 		<<259.4605, -996.5207, -99.5630>>
					fHeading = 		70.9989
					vRotate = 		<<0.0000, 0.0000, 70.9988>>
				BREAK			
				CASE 1			
					model = 		p_tumbler_cs2_s
					vCoords = 		<<259.3880, -996.8710, -99.5150>>
					fHeading = 		99.399
					vRotate = 		<<0.0000, 0.0000, 99.3990>>
				BREAK			
				CASE 2			
					model = 		p_whiskey_bottle_s
					vCoords = 		<<259.2267, -996.9167, -99.4233>>
					fHeading = 		92.9987
					vRotate = 		<<0.0000, -0.0000, 92.9987>>
				BREAK			
				CASE 3
					model = 		prop_amb_handbag_01	
					
					SWITCH iExtraParam
						CASE 0
							vCoords = 		<<258.2953, -996.5695, -99.5324>>
							fHeading = 		0.0
							vRotate = 		<<-84.9937, 149.3419, -180.0000>>
						BREAK
						CASE 1
							vCoords = 		<<262.9693, -1002.0899, -99.3300>>
							fHeading = 		255.2509
							vRotate = 		<<89.9802, -104.7491, 0.0000>>
						BREAK
						CASE 2
							vCoords = 		<<266.2009, -996.4588, -98.9924>>
							fHeading = 		7.233
							vRotate = 		<<90.0000, 7.2330, 0.0000>>
						BREAK
					ENDSWITCH					
				BREAK
				CASE 4
					model = 		bkr_prop_coke_painkiller_01a
					vCoords = 		<<260.4647, -996.8118, -100.0093>>
					fHeading = 		328.2001
					vRotate = 		<<0.0000, 0.0000, -31.7999>>
				BREAK
				CASE 5
					model = 		prop_dummy_car
					vCoords = 		<<252.814,-1001.137,-98.010>>
					fHeading = 		0
					vRotate = 		<<0.0000, 0.0000, 0.0000>>
				BREAK
				
			ENDSWITCH				
		BREAK
		CASE CHV_INSIDE_VALET	
			SWITCH iProp				
				CASE 0			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_morgue_01a")) 	
					
					SWITCH iExtraParam
						CASE 0
							vCoords = 		<<278.4850,-1338.4880, 24.531>>
							fHeading = 		229.295
							vRotate = 		<<0.0000, 0.0000, -130.7050>>
						BREAK
						CASE 1
							vCoords = 		<<251.9872, -1348.7991, 24.5181>>
							fHeading = 		139.9994
							vRotate = 		<<0.0000, -0.0000, 139.9994>>
						BREAK
						CASE 2
							vCoords = 		<<296.5931, -1349.7579, 24.5724>>
							fHeading = 		320.5991
							vRotate = 		<<0.0000, 0.0000, -39.4009>>
						BREAK
					ENDSWITCH
				BREAK
				CASE 1
					model = 		xm_prop_x17_bag_01a
					vCoords = 		<<-1691.7910, -294.0686, 50.8123>>
					fHeading = 		0.0
					vRotate = 		<<0.0000, 0.0000, 0.0>>
				BREAK
				CASE 2
					model = 		xm_prop_x17_bag_01a
					vCoords = 		<<-1691.7910, -296.0686, 50.8123>>
					fHeading = 		0.0
					vRotate = 		<<0.0000, 0.0000, 0.0>>
				BREAK
			ENDSWITCH				
		BREAK		
		CASE CHV_WEAPONS_STASH
			SWITCH eSubvariation
				CASE CHS_WST_SAN_ANDREAS
					SWITCH iProp				
						CASE 0			
							model = 		prop_box_wood05b
							vCoords = 		<<1112.1420, -3142.8340, -37.9810>>
							fHeading = 		179.798
							vRotate = 		<<0.0000, 0.0000, 179.7980>>
						BREAK			
						CASE 1			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("v_ret_ml_beerpis2"))
							vCoords = 		<<1116.6270, -3145.4470, -37.0680>>
							fHeading = 		52.597
							vRotate = 		<<0.0000, 0.0000, 52.5970>>
						BREAK			
						CASE 2			
							model = 		bkr_prop_coke_table01a
							vCoords = 		<<1116.2970, -3142.8711, -38.0630>>
							fHeading = 		0.197
							vRotate = 		<<0.0000, 0.0000, 0.1970>>
						BREAK			
						CASE 3			
							model = 		bkr_prop_weed_scales_01a
							vCoords = 		<<1114.4091, -3145.0291, -37.1390>>
							fHeading = 		244.596
							vRotate = 		<<0.0000, 0.0000, -115.4040>>
						BREAK			
						CASE 4			
							model = 		bkr_prop_weed_bigbag_open_01a
							vCoords = 		<<1113.5560, -3144.7539, -37.2250>>
							fHeading = 		279.396
							vRotate = 		<<0.0000, 0.0000, -80.6040>>
						BREAK			
						CASE 5			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ba_prop_battle_crate_beer_02")) 
							vCoords = 		<<1116.5100, -3154.5171, -38.0630>>
							fHeading = 		91.396
							vRotate = 		<<0.0000, 0.0000, 91.3960>>
						BREAK			
						CASE 6			
							model = 		prop_box_wood05a
							vCoords = 		<<1113.9041, -3147.9041, -37.9810>>
							fHeading = 		181.195
							vRotate = 		<<0.0000, 0.0000, -178.8050>>
						BREAK	
						CASE 7			
							model = 		prop_toolchest_05
							vCoords = 		<<1106.3450, -3162.2339, -38.5400>>
							fHeading = 		159
							vRotate = 		<<0.0000, 0.0000, 159.0000>>
						BREAK			
						CASE 8			
							model = 		prop_toolchest_05
							vCoords = 		<<1111.1130, -3161.8069, -38.5190>>
							fHeading = 		266.6
							vRotate = 		<<0.0000, 0.0000, -93.4000>>
						BREAK			
						CASE 9			
							model = 		prop_toolchest_04
							vCoords = 		<<1110.6930, -3160.2251, -38.5190>>
							fHeading = 		310.999
							vRotate = 		<<0.0000, 0.0000, -49.0010>>
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_WST_PALETO_BAY
					SWITCH iProp				
						CASE 0			
							model = 		prop_box_wood05b
							vCoords = 		<<1112.1420, -3142.8340, -37.9810>>
							fHeading = 		179.798
							vRotate = 		<<0.0000, 0.0000, 179.7980>>
						BREAK			
						CASE 1			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("v_ret_ml_beerpis2")) 
							vCoords = 		<<1116.6270, -3145.4470, -37.0680>>
							fHeading = 		52.597
							vRotate = 		<<0.0000, 0.0000, 52.5970>>
						BREAK			
						CASE 2			
							model = 		bkr_prop_coke_table01a
							vCoords = 		<<1116.2970, -3142.8711, -38.0630>>
							fHeading = 		0.197
							vRotate = 		<<0.0000, 0.0000, 0.1970>>
						BREAK			
						CASE 3			
							model = 		bkr_prop_weed_scales_01a
							vCoords = 		<<1114.4091, -3145.0291, -37.1390>>
							fHeading = 		244.596
							vRotate = 		<<0.0000, 0.0000, -115.4040>>
						BREAK			
						CASE 4			
							model = 		bkr_prop_weed_bigbag_open_01a
							vCoords = 		<<1113.5560, -3144.7539, -37.2250>>
							fHeading = 		279.396
							vRotate = 		<<0.0000, 0.0000, -80.6040>>
						BREAK			
						CASE 5			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ba_prop_battle_crate_beer_02"))  
							vCoords = 		<<1116.5100, -3154.5171, -38.0630>>
							fHeading = 		91.396
							vRotate = 		<<0.0000, 0.0000, 91.3960>>
						BREAK			
						CASE 6			
							model = 		prop_box_wood05a
							vCoords = 		<<1113.9041, -3147.9041, -37.9810>>
							fHeading = 		181.195
							vRotate = 		<<0.0000, 0.0000, -178.8050>>
						BREAK		
						CASE 7			
							model = 		prop_toolchest_05
							vCoords = 		<<1106.3450, -3162.2339, -38.5400>>
							fHeading = 		159
							vRotate = 		<<0.0000, 0.0000, 159.0000>>
						BREAK			
						CASE 8			
							model = 		prop_toolchest_05
							vCoords = 		<<1111.1130, -3161.8069, -38.5190>>
							fHeading = 		266.6
							vRotate = 		<<0.0000, 0.0000, -93.4000>>
						BREAK			
						CASE 9			
							model = 		prop_toolchest_04
							vCoords = 		<<1110.6930, -3160.2251, -38.5190>>
							fHeading = 		310.999
							vRotate = 		<<0.0000, 0.0000, -49.0010>>
						BREAK	
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_DRONES
			SWITCH eSubvariation
				CASE CHS_DRONES_VARIATION_1
					SWITCH iProp				
						CASE 0			
							model = 		DRONE_MODEL()
							vCoords = 		<<201.3850, -937.4080, 54.0090>>
							fHeading = 		0
							vRotate = 		<<0.0000, 0.0000, 0.0000>>
						BREAK			
						CASE 1			
							model = 		DRONE_MODEL()
							vCoords = 		<<-1024.8800, -2710.2690, 31.6480>>
							fHeading = 		332.8
							vRotate = 		<<0.0000, 0.0000, -27.2000>>
						BREAK			
						CASE 2			
							model = 		DRONE_MODEL()
							vCoords = 		<<-1604.1820, 167.6250, 76.6850>>
							fHeading = 		177.4
							vRotate = 		<<0.0000, 0.0000, 177.4000>>
						BREAK			
						CASE 3			
							model = 		DRONE_MODEL()
							vCoords = 		<<-1794.4091, -1178.0050, 29.3880>>
							fHeading = 		137.6
							vRotate = 		<<0.0000, 0.0000, 137.6000>>
						BREAK			
						CASE 4			
							model = 		DRONE_MODEL()
							vCoords = 		<<1145.9170, -643.9830, 73.2210>>
							fHeading = 		315.599
							vRotate = 		<<0.0000, 0.0000, -44.4010>>
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_DRONES_VARIATION_2
					SWITCH iProp				
						CASE 0			
							model = 		DRONE_MODEL()
							vCoords = 		<<-1259.4860, -1480.4550, 21.3860>>
							fHeading = 		182.399
							vRotate = 		<<0.0000, 0.0000, -177.6010>>
						BREAK			
						CASE 1			
							model = 		DRONE_MODEL()
							vCoords = 		<<-412.7140, 1170.1310, 342.4690>>
							fHeading = 		317.799
							vRotate = 		<<0.0000, 0.0000, -42.2010>>
						BREAK			
						CASE 2			
							model = 		DRONE_MODEL()
							vCoords = 		<<-538.8170, -214.3400, 54.2180>>
							fHeading = 		28.799
							vRotate = 		<<0.0000, 0.0000, 28.7990>>
						BREAK			
						CASE 3			
							model = 		DRONE_MODEL()
							vCoords = 		<<-1293.9475, 83.6042, 67.8382>>
							fHeading = 		24.399
							vRotate = 		<<0.0000, 0.0000, 24.3990>>
						BREAK			
						CASE 4			
							model = 		DRONE_MODEL()
							vCoords = 		<<103.1320, -1939.1030, 36.8620>>
							fHeading = 		225.999
							vRotate = 		<<0.0000, 0.0000, -134.0010>>
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_DRONES_VARIATION_3
					SWITCH iProp				
						CASE 0			
							model = 		DRONE_MODEL()
							vCoords = 		<<-1228.3920, -776.0340, 34.4230>>
							fHeading = 		222.799
							vRotate = 		<<0.0000, 0.0000, -137.2010>>
						BREAK			
						CASE 1			
							model = 		DRONE_MODEL()
							vCoords = 		<<-103.8640, 905.0740, 253.0510>>
							fHeading = 		213.799
							vRotate = 		<<0.0000, 0.0000, -146.2010>>
						BREAK			
						CASE 2			
							model = 		DRONE_MODEL()
							vCoords = 		<<455.0340, -226.1110, 72.5590>>
							fHeading = 		251.799
							vRotate = 		<<0.0000, 0.0000, -108.2010>>
						BREAK			
						CASE 3			
							model = 		DRONE_MODEL()
							vCoords = 		<<-263.7680, -1895.9709, 43.5770>>
							fHeading = 		170.599
							vRotate = 		<<0.0000, 0.0000, 170.5990>>
						BREAK			
						CASE 4			
							model = 		DRONE_MODEL()	
							vCoords = 		<<1290.2841, -1728.5229, 69.8900>>
							fHeading = 		258.998
							vRotate = 		<<0.0000, 0.0000, -101.0020>>
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_WEAPONS_SMUGGLERS
			SWITCH eSubvariation
				CASE CHS_WSM_VARIATION_1
					SWITCH iProp				
						CASE 0			
							model = 		prop_box_wood04a
							vCoords = 		<<2850.7102, -77.5716, 1.6474>>
							fHeading = 		337.7988
							vRotate = 		<<6.8773, 5.0134, -22.5027>>
						BREAK			
						CASE 1			
							model = 		prop_boxpile_01a
							vCoords = 		<<2840.4312, -100.8019, 1.3914>>
							fHeading = 		349
							vRotate = 		<<-4.8003, 3.1000, -11.0000>>
						BREAK			
						CASE 2			
							model = 		sm_Prop_Smug_Crate_S_Bones
							vCoords = 		<<2843.4937, -99.8923, 1.3542>>
							fHeading = 		238.3932
							vRotate = 		<<-1.4995, -7.7575, -121.7084>>
						BREAK			
						CASE 3			
							model = 		sm_Prop_Smug_Crate_S_Bones
							vCoords = 		<<2856.8904, -72.4263, 1.9036>>
							fHeading = 		283.2914
							vRotate = 		<<-7.3025, -2.0704, -76.8407>>
						BREAK			
						CASE 4			
							model = 		sm_Prop_Smug_Crate_S_Bones
							vCoords = 		<<2854.6221, -101.7428, 0.9127>>
							fHeading = 		154.5587
							vRotate = 		<<0.0000, -0.0000, 154.5587>>
						BREAK			
						CASE 5			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_crate_01a")) 
							vCoords = 		<<2852.3552, -102.1898, 0.6714>>
							fHeading = 		103.9585
							vRotate = 		<<0.0000, -0.0000, 103.9585>>
						BREAK			
						CASE 6			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_crate_01a")) 
							vCoords = 		<<2856.6638, -74.3918, 1.6880>>
							fHeading = 		145.9583
							vRotate = 		<<1.8617, -9.2207, 146.1084>>
						BREAK			
						CASE 7			
							model = 		prop_box_ammo05b
							vCoords = 		<<2851.1147, -78.0169, 2.7303>>
							fHeading = 		0.1082
							vRotate = 		<<8.3000, 2.6000, 0.1082>>
						BREAK			
						CASE 8			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_crate_full_01a")) 
							vCoords = 		<<2854.9880, -71.1974, 1.7281>>
							fHeading = 		38.5346
							vRotate = 		<<1.2488, 7.4859, 38.4529>>
						BREAK			
						CASE 9			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_crate_full_01a")) 
							vCoords = 		<<2853.8457, -105.8384, 0.8819>>
							fHeading = 		189.4529
							vRotate = 		<<0.0000, -0.0000, -170.5471>>
						BREAK	
						CASE 10			
							model = 		prop_worklight_04d
							vCoords = 		<<2839.3157, -105.1366, 2.3353>>
							fHeading = 		138.7996
							vRotate = 		<<4.6108, -0.3254, 138.8127>>
						BREAK			
						CASE 11			
							model = 		prop_worklight_04d
							vCoords = 		<<2849.6060, -67.2811, 2.2734>>
							fHeading = 		36.9235
							vRotate = 		<<-3.2434, 6.8644, 37.1181>>
						BREAK			
						CASE 12			
							model = 		prop_worklight_04d
							vCoords = 		<<2845.1799, -72.5939, 2.4758>>
							fHeading = 		80.318
							vRotate = 		<<0.0000, 0.0000, 80.3180>>
						BREAK	
					ENDSWITCH				
				BREAK
				CASE CHS_WSM_VARIATION_2
					SWITCH iProp				
						CASE 0			
							model = 		prop_box_wood04a
							vCoords = 		<<2534.0681, -1221.4139, 1.2410>>
							fHeading = 		318.8
							vRotate = 		<<0.0000, 0.0000, -41.2000>>
						BREAK			
						CASE 1			
							model = 		prop_boxpile_01a
							vCoords = 		<<2537.9043, -1215.3849, 1.4277>>
							fHeading = 		17.6
							vRotate = 		<<1.0000, 0.0000, 17.6000>>
						BREAK			
						CASE 2			
							model = 		sm_Prop_Smug_Crate_S_Bones
							vCoords = 		<<2540.8650, -1215.9200, 1.7280>>
							fHeading = 		322.4
							vRotate = 		<<0.0000, 0.0000, -37.6000>>
						BREAK			
						CASE 3			
							model = 		sm_Prop_Smug_Crate_S_Bones
							vCoords = 		<<2542.2351, -1227.6949, 0.8850>>
							fHeading = 		17.6
							vRotate = 		<<0.0000, 0.0000, 17.6000>>
						BREAK			
						CASE 4			
							model = 		sm_Prop_Smug_Crate_S_Bones
							vCoords = 		<<2533.1160, -1231.8459, 1.1110>>
							fHeading = 		149.799
							vRotate = 		<<0.0000, 0.0000, 149.7990>>
						BREAK			
						CASE 5			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_crate_01a")) 
							vCoords = 		<<2534.8611, -1231.4280, 0.7750>>
							fHeading = 		205.199
							vRotate = 		<<0.0000, 0.0000, -154.8010>>
						BREAK			
						CASE 6			
							model = 		prop_box_ammo05b
							vCoords = 		<<2533.5090, -1221.2650, 2.3890>>
							fHeading = 		335.798
							vRotate = 		<<0.0000, 0.0000, -24.2020>>
						BREAK			
						CASE 7			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_crate_01a")) 	
							vCoords = 		<<2542.2729, -1225.3053, 0.5737>>
							fHeading = 		322.9987
							vRotate = 		<<0.0000, 0.0000, -37.0012>>
						BREAK			
						CASE 8			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_crate_full_01a")) 
							vCoords = 		<<2540.6794, -1220.2213, 1.0314>>
							fHeading = 		45.2
							vRotate = 		<<5.9203, -1.2771, 45.2660>>
						BREAK			
						CASE 9			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_crate_full_01a")) 	
							vCoords = 		<<2534.0818, -1227.7709, 0.7493>>
							fHeading = 		147.8658
							vRotate = 		<<5.3987, -4.2095, 148.0644>>
						BREAK	
						CASE 10			
							model = 		prop_worklight_04d
							vCoords = 		<<2545.7449, -1215.1683, 1.9381>>
							fHeading = 		325.8
							vRotate = 		<<0.0000, 0.0000, -34.2000>>
						BREAK			
						CASE 11			
							model = 		prop_worklight_04d
							vCoords = 		<<2528.1362, -1228.2031, 1.5767>>
							fHeading = 		105.1999
							vRotate = 		<<0.0000, -0.0000, 105.1999>>
						BREAK			
						CASE 12			
							model = 		prop_worklight_04d
							vCoords = 		<<2532.0876, -1216.2664, 1.9348>>
							fHeading = 		52.5997
							vRotate = 		<<0.0000, 0.0000, 52.5997>>
						BREAK
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_DRAG_RACE
			SWITCH eSubvariation
				CASE CHS_DR_VARIATION_1
					SWITCH iProp				
						CASE 0			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_start_gate_01b")) 
							vCoords = 		<<1413.0990, 3010.4951, 39.5350>>
							fHeading = 		316.2
							vRotate = 		<<0.0000, 0.0000, -43.8000>>
						BREAK			
						CASE 1			
							model = 		prop_beachf_01_cr
							vCoords = 		<<1387.6940, 2997.8960, 39.7740>>
							fHeading = 		-43.8
							vRotate = 		<<0.0000, 0.0000, -43.8000>>
						BREAK			
						CASE 2			
							model = 		prop_beachf_01_cr
							vCoords = 		<<1399.4120, 2985.1609, 39.7730>>
							fHeading = 		21.2
							vRotate = 		<<0.0000, 0.0000, 21.2000>>
						BREAK			
						CASE 3			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_tyre_wall_03c")) 
							vCoords = 		<<1399.8220, 3011.4141, 39.5450>>
							fHeading = 		26.8
							vRotate = 		<<0.0000, 0.0000, 26.8000>>
						BREAK			
						CASE 4			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_tyre_wall_03c"))
							vCoords = 		<<1413.6379, 2996.7480, 39.5410>>
							fHeading = 		60.4
							vRotate = 		<<0.0000, 0.0000, 60.4000>>
						BREAK			
						CASE 5			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_tyre_wall_03c"))
							vCoords = 		<<1426.3480, 3010.7791, 39.5330>>
							fHeading = 		33.2
							vRotate = 		<<0.0000, 0.0000, 33.2000>>
						BREAK			
						CASE 6			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_tyre_wall_03c"))
							vCoords = 		<<1412.8409, 3023.5310, 39.5330>>
							fHeading = 		56
							vRotate = 		<<0.0000, 0.0000, 56.0000>>
						BREAK		
						CASE 7			
							model = 		prop_worklight_03b
							vCoords = 		<<1409.1613, 2990.4351, 39.6442>>
							fHeading = 		223.7996
							vRotate = 		<<0.0000, -0.0000, -136.2004>>
						BREAK			
						CASE 8			
							model = 		prop_worklight_03b
							vCoords = 		<<1392.6335, 3007.4192, 39.6501>>
							fHeading = 		46.1996
							vRotate = 		<<0.0000, 0.0000, 46.1995>>
						BREAK			
						CASE 9			
							model = 		prop_worklight_03b
							vCoords = 		<<1387.9796, 2996.0752, 39.6584>>
							fHeading = 		95.3994
							vRotate = 		<<5.2832, 4.3954, 95.1965>>
						BREAK			
						CASE 10			
							model = 		prop_worklight_03b
							vCoords = 		<<1397.8396, 2986.0122, 39.6505>>
							fHeading = 		172.5962
							vRotate = 		<<0.0000, -0.0000, 172.5962>>
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_DR_VARIATION_2
					SWITCH iProp				
						CASE 0			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_start_gate_01b"))
							vCoords = 		<<-1464.0780, -2432.1040, 12.9480>>
							fHeading = 		330.6
							vRotate = 		<<0.0000, 0.0000, -29.4000>>
						BREAK			
						CASE 1			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_tyre_wall_03c"))
							vCoords = 		<<-1478.5120, -2433.7971, 12.9450>>
							fHeading = 		210.6
							vRotate = 		<<0.0000, 0.0000, -149.4000>>
						BREAK			
						CASE 2			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_tyre_wall_03c"))
							vCoords = 		<<-1458.4220, -2445.2700, 12.9450>>
							fHeading = 		267
							vRotate = 		<<0.0000, 0.0000, -93.0000>>
						BREAK			
						CASE 3			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_tyre_wall_03c"))
							vCoords = 		<<-1469.3140, -2419.1770, 12.9450>>
							fHeading = 		-93
							vRotate = 		<<0.0000, 0.0000, -93.0000>>
						BREAK			
						CASE 4			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_tyre_wall_03c"))
							vCoords = 		<<-1450.5660, -2429.8330, 12.9450>>
							fHeading = 		219.6
							vRotate = 		<<0.0000, 0.0000, -140.4000>>
						BREAK			
						CASE 5			
							model = 		prop_beachf_01_cr
							vCoords = 		<<-1491.6200, -2446.0840, 13.1650>>
							fHeading = 		360.2
							vRotate = 		<<0.0000, 0.0000, 0.2000>>
						BREAK			
						CASE 6			
							model = 		prop_beachf_01_cr
							vCoords = 		<<-1462.3051, -2462.3091, 13.1650>>
							fHeading = 		24
							vRotate = 		<<0.0000, 0.0000, 23.9990>>
						BREAK		
						CASE 7			
							model = 		prop_worklight_03b
							vCoords = 		<<-1463.7579, -2462.1045, 13.0408>>
							fHeading = 		204.9996
							vRotate = 		<<0.0000, -0.0000, -155.0004>>
						BREAK			
						CASE 8			
							model = 		prop_worklight_03b
							vCoords = 		<<-1490.6067, -2446.9707, 13.0408>>
							fHeading = 		99.3997
							vRotate = 		<<0.0000, -0.0000, 99.3997>>
						BREAK			
						CASE 9			
							model = 		prop_worklight_03b
							vCoords = 		<<-1459.8429, -2447.4995, 13.0408>>
							fHeading = 		265.3994
							vRotate = 		<<0.0000, -0.0000, -94.6006>>
						BREAK			
						CASE 10			
							model = 		prop_worklight_03b
							vCoords = 		<<-1479.9226, -2436.2815, 13.0408>>
							fHeading = 		28.7991
							vRotate = 		<<0.0000, 0.0000, 28.7991>>
						BREAK			
					ENDSWITCH			
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_CARGO_SHIP
			SWITCH eSubvariation
				CASE CHS_CS_VARIATION_1
					SWITCH iProp
						CASE 0			
							model = 		prop_container_03_ld
							vCoords = 		<<-139.9100, -2359.9451, 19.6240>>
							fHeading = 		359.8
							vRotate = 		<<0.0000, 0.0000, -0.2000>>
						BREAK			
						CASE 1			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("sm_prop_smug_cont_01a")) 
							vCoords = 		<<-155.2740, -2365.4709, 19.6340>>
							fHeading = 		359.798
							vRotate = 		<<0.0000, 0.0000, -0.2020>>
						BREAK			
						CASE 2			
							model = 		prop_container_03_ld
							vCoords = 		<<-158.1480, -2387.3379, 4.9990>>
							fHeading = 		71.6
							vRotate = 		<<0.0000, 0.0000, 71.6000>>
						BREAK			
						CASE 3			
							model = 		prop_cntrdoor_ld_l
							vCoords = 		<<-141.2260, -2363.3420, 21.0270>>
							fHeading = 		179.2
							vRotate = 		<<0.0000, 0.0000, 179.2000>>
						BREAK			
						CASE 4			
							model = 		prop_cntrdoor_ld_l
							vCoords = 		<<-155.3450, -2389.6440, 6.4020>>
							fHeading = 		289.8
							vRotate = 		<<0.0000, 0.0000, -70.2000>>
						BREAK			
						CASE 5			
							model = 		prop_cntrdoor_ld_r
							vCoords = 		<<-138.6280, -2363.3569, 21.0270>>
							fHeading = 		141.8
							vRotate = 		<<0.0000, 0.0000, 141.8000>>
						BREAK			
						CASE 6			
							model = 		prop_cntrdoor_ld_r
							vCoords = 		<<-154.5270, -2387.1790, 6.4010>>
							fHeading = 		211.999
							vRotate = 		<<0.0000, 0.0000, -148.0010>>
						BREAK	
						CASE 7			
							model = 		prop_worklight_03b
							vCoords = 		<<-147.3170, -2394.2468, 5.0957>>
							fHeading = 		185.0003
							vRotate = 		<<0.0000, -0.0000, -174.9997>>
						BREAK			
						CASE 8			
							model = 		prop_worklight_03b
							vCoords = 		<<-133.6713, -2369.0056, 19.7216>>
							fHeading = 		260.6005
							vRotate = 		<<0.0000, -0.0000, -99.3995>>
						BREAK			
						CASE 9			
							model = 		prop_worklight_03b
							vCoords = 		<<-157.9934, -2372.3450, 19.7312>>
							fHeading = 		158.4003
							vRotate = 		<<0.0000, -0.0000, 158.4003>>
						BREAK			
					ENDSWITCH
				BREAK
				CASE CHS_CS_VARIATION_2
					SWITCH iProp
						CASE 0			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("sm_prop_smug_cont_01a"))
							vCoords = 		<<-18.2390, 6195.8682, 30.3270>>
							fHeading = 		218.8
							vRotate = 		<<0.0000, 0.0000, -141.2000>>
						BREAK			
						CASE 1			
							model = 		prop_container_03_ld
							vCoords = 		<<-7.7960, 6195.9019, 38.7540>>
							fHeading = 		170
							vRotate = 		<<0.0000, 0.0000, 170.0000>>
						BREAK			
						CASE 2			
							model = 		prop_container_03_ld
							vCoords = 		<<6.4460, 6212.5391, 30.4730>>
							fHeading = 		246.4
							vRotate = 		<<0.0000, 0.0000, -113.6000>>
						BREAK			
						CASE 3			
							model = 		prop_cntrdoor_ld_l
							vCoords = 		<<-5.9340, 6199.0259, 40.1510>>
							fHeading = 		299.399
							vRotate = 		<<0.0000, 0.0000, -60.6010>>
						BREAK			
						CASE 4			
							model = 		prop_cntrdoor_ld_l
							vCoords = 		<<3.8530, 6215.0918, 31.8700>>
							fHeading = 		51.599
							vRotate = 		<<0.0000, 0.0000, 51.5990>>
						BREAK			
						CASE 5			
							model = 		prop_cntrdoor_ld_r
							vCoords = 		<<2.8110, 6212.7168, 31.8770>>
							fHeading = 		14.799
							vRotate = 		<<0.0000, 0.0000, 14.7990>>
						BREAK			
						CASE 6			
							model = 		prop_cntrdoor_ld_r
							vCoords = 		<<-8.4790, 6199.4722, 40.1560>>
							fHeading = 		20.999
							vRotate = 		<<0.0000, 0.0000, 20.9990>>
						BREAK	
						CASE 7			
							model = 		prop_worklight_03b
							vCoords = 		<<-12.8183, 6197.2026, 38.8509>>
							fHeading = 		130.1989
							vRotate = 		<<0.0000, -0.0000, 130.1989>>
						BREAK			
						CASE 8			
							model = 		prop_worklight_03b
							vCoords = 		<<-9.6341, 6215.1699, 30.3760>>
							fHeading = 		72.5994
							vRotate = 		<<-3.8638, 1.3176, 72.6439>>
						BREAK			
						CASE 9			
							model = 		prop_worklight_03b
							vCoords = 		<<-24.1782, 6208.2676, 30.1560>>
							fHeading = 		46.2438
							vRotate = 		<<0.0000, 0.0000, 46.2438>>
						BREAK				
					ENDSWITCH
				BREAK
				CASE CHS_CS_VARIATION_3
					SWITCH iProp
						CASE 0			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("sm_prop_smug_cont_01a"))
							vCoords = 		<<581.3300, -1882.0620, 24.4260>>
							fHeading = 		56
							vRotate = 		<<0.0000, 0.0000, 56.0000>>
						BREAK			
						CASE 1			
							model = 		prop_container_03_ld
							vCoords = 		<<584.4280, -1865.5890, 29.6060>>
							fHeading = 		337.8
							vRotate = 		<<-1.8000, 0.0000, -22.2000>>
						BREAK			
						CASE 2			
							model = 		prop_container_03_ld
							vCoords = 		<<603.2480, -1866.2620, 23.7350>>
							fHeading = 		305.4
							vRotate = 		<<0.0000, 0.0000, -54.6000>>
						BREAK			
						CASE 3			
							model = 		prop_cntrdoor_ld_l
							vCoords = 		<<581.9610, -1868.1810, 31.1180>>
							fHeading = 		219.4
							vRotate = 		<<0.0000, 0.0000, -140.6000>>
						BREAK			
						CASE 4			
							model = 		prop_cntrdoor_ld_l
							vCoords = 		<<599.7250, -1867.1680, 25.1380>>
							fHeading = 		153.4
							vRotate = 		<<0.0000, 0.0000, 153.4000>>
						BREAK			
						CASE 5			
							model = 		prop_cntrdoor_ld_r
							vCoords = 		<<601.2250, -1869.2860, 25.1380>>
							fHeading = 		69.199
							vRotate = 		<<0.0000, 0.0000, 69.1990>>
						BREAK			
						CASE 6			
							model = 		prop_cntrdoor_ld_r
							vCoords = 		<<584.3656, -1869.1670, 31.1129>>
							fHeading = 		140.5999
							vRotate = 		<<0.0000, -0.0000, 140.5999>>
						BREAK	
						CASE 7			
							model = 		prop_worklight_03b
							vCoords = 		<<595.1964, -1886.7062, 24.2718>>
							fHeading = 		218.9997
							vRotate = 		<<0.0000, -0.0000, -141.0002>>
						BREAK			
						CASE 8			
							model = 		prop_worklight_03b
							vCoords = 		<<581.4160, -1875.2980, 29.9710>>
							fHeading = 		154.5998
							vRotate = 		<<0.0000, -0.0000, 154.5998>>
						BREAK			
						CASE 9			
							model = 		prop_worklight_03b
							vCoords = 		<<577.7391, -1893.1251, 24.0764>>
							fHeading = 		94.5995
							vRotate = 		<<0.0000, -0.0000, 94.5995>>
						BREAK	
						CASE 10			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("sm_prop_smug_cont_01a"))
							vCoords = 		<<575.9977, -1906.9957, 23.9687>>
							fHeading = 		211.7986
							vRotate = 		<<0.0000, -0.0000, -148.2013>>
						BREAK			
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_ELECTRIC_DRILLS_2
			SWITCH eSubvariation
				CASE CHS_ED2_VARIATION_1
					SWITCH iProp				
						CASE 0			
							model = 		prop_woodpile_01c
							vCoords = 		<<-922.9380, 398.2050, 78.1320>>
							fHeading = 		-174.478
							vRotate = 		<<0.0000, 0.0000, -174.4780>>
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_ED2_VARIATION_2
					SWITCH iProp				
						CASE 0			
							model = 		prop_woodpile_01c
							vCoords = 		<<-387.9230, 6259.1709, 30.5050>>
							fHeading = 		327.6
							vRotate = 		<<0.0000, 0.0000, -32.4000>>
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_ED2_VARIATION_3
					SWITCH iProp				
						CASE 0			
							model = 		prop_woodpile_01c
							vCoords = 		<<1145.7040, 2103.5520, 54.8060>>
							fHeading = 		27.198
							vRotate = 		<<0.0000, 0.0000, 27.1980>>
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_ZANCUDO_SHIPMENT
			SWITCH eSubvariation
				CASE CHS_ZS_VARIATION_1
					SWITCH iProp				
						CASE 0			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_air_cargo_04a"))
							vCoords = 		<<-2443.6321, 2974.7339, 31.8000>>
							fHeading = 		236.599
							vRotate = 		<<0.0000, 0.0000, -123.4010>>
						BREAK			
						CASE 1			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_air_cargo_04a"))
							vCoords = 		<<-2451.4719, 2968.2041, 31.8000>>
							fHeading = 		83.799
							vRotate = 		<<0.0000, 0.0000, 83.7990>>
						BREAK			
						CASE 2			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_crate_01b")) 
							vCoords = 		<<-2445.8950, 2960.3130, 31.8100>>
							fHeading = 		114.198
							vRotate = 		<<0.0000, 0.0000, 114.1980>>
						BREAK			
						CASE 3			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_cargo_04b"))	
							vCoords = 		<<-2457.0791, 2959.4189, 31.8100>>
							fHeading = 		106.798
							vRotate = 		<<0.0000, 0.0000, 106.7980>>
						BREAK			
						CASE 4			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_crate_01a"))	 
							vCoords = 		<<-2453.9331, 2952.6841, 31.8100>>
							fHeading = 		123.598
							vRotate = 		<<0.0000, 0.0000, 123.5980>>
						BREAK			
						CASE 5			
							model = 		prop_conc_sacks_02a
							vCoords = 		<<-2435.1411, 2972.3931, 31.8360>>
							fHeading = 		240.197
							vRotate = 		<<0.0000, 0.0000, -119.8030>>
						BREAK			
						CASE 6			
							model = 		prop_conc_sacks_02a
							vCoords = 		<<-2434.0471, 2974.4800, 31.8360>>
							fHeading = 		-119.803
							vRotate = 		<<0.0000, 0.0000, -119.8030>>
						BREAK			
						CASE 7			
							model = 		gr_prop_gr_target_02b
							vCoords = 		<<-2435.2141, 2972.4331, 32.6470>>
							fHeading = 		240.999
							vRotate = 		<<0.0000, 0.0000, -119.0010>>
						BREAK			
						CASE 8			
							model = 		gr_prop_gr_target_02b
							vCoords = 		<<-2434.1660, 2974.5220, 32.6480>>
							fHeading = 		-119.001
							vRotate = 		<<0.0000, 0.0000, -119.0010>>
						BREAK		
						CASE 9			
							model = 		prop_barier_conc_02a
							vCoords = 		<<-2433.0591, 2999.0938, 31.8471>>
							fHeading = 		146.7981
							vRotate = 		<<0.0000, -0.0000, 146.7980>>
						BREAK			
						CASE 10			
							model = 		prop_barier_conc_02a
							vCoords = 		<<-2431.2756, 2990.3311, 31.8475>>
							fHeading = 		146.798
							vRotate = 		<<0.0000, -0.0000, 146.7980>>
						BREAK			
					ENDSWITCH	
				BREAK
				CASE CHS_ZS_VARIATION_2
					SWITCH iProp				
						CASE 0			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_cargo_04b"))
							vCoords = 		<<-2275.9280, 3175.8330, 31.8100>>
							fHeading = 		148.999
							vRotate = 		<<0.0000, 0.0000, 148.9990>>
						BREAK			
						CASE 1			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_air_cargo_04a"))
							vCoords = 		<<-2281.9399, 3182.9880, 31.8000>>
							fHeading = 		23.999
							vRotate = 		<<0.0000, 0.0000, 23.9990>>
						BREAK			
						CASE 2			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_air_cargo_04a"))
							vCoords = 		<<-2289.8799, 3179.8220, 31.8000>>
							fHeading = 		328.398
							vRotate = 		<<0.0000, 0.0000, -31.6020>>
						BREAK			
						CASE 3			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_crate_01b"))
							vCoords = 		<<-2285.4790, 3173.2490, 31.8100>>
							fHeading = 		23.598
							vRotate = 		<<0.0000, 0.0000, 23.5980>>
						BREAK			
						CASE 4			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_crate_01a"))	
							vCoords = 		<<-2290.4690, 3191.1069, 31.8100>>
							fHeading = 		331.798
							vRotate = 		<<0.0000, 0.0000, -28.2020>>
						BREAK			
						CASE 5			
							model = 		prop_conc_sacks_02a
							vCoords = 		<<-2270.1660, 3181.9719, 31.8360>>
							fHeading = 		55.998
							vRotate = 		<<0.0000, 0.0000, 55.9980>>
						BREAK			
						CASE 6			
							model = 		prop_conc_sacks_02a
							vCoords = 		<<-2268.8931, 3184.1030, 31.8360>>
							fHeading = 		55.998
							vRotate = 		<<0.0000, 0.0000, 55.9980>>
						BREAK			
						CASE 7			
							model = 		gr_prop_gr_target_02b
							vCoords = 		<<-2270.1030, 3181.9341, 32.6690>>
							fHeading = 		55.998
							vRotate = 		<<0.0000, 0.0000, 55.9980>>
						BREAK			
						CASE 8			
							model = 		gr_prop_gr_target_02b
							vCoords = 		<<-2268.7891, 3184.0449, 32.6560>>
							fHeading = 		55.998
							vRotate = 		<<0.0000, 0.0000, 55.9980>>
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_ZS_VARIATION_3
					SWITCH iProp				
						CASE 0			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_cargo_04b"))
							vCoords = 		<<-2083.0359, 3258.3010, 31.8100>>
							fHeading = 		59.2
							vRotate = 		<<0.0000, 0.0000, 59.2000>>
						BREAK			
						CASE 1			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_air_cargo_04a"))
							vCoords = 		<<-2076.7749, 3269.5920, 31.8000>>
							fHeading = 		83.999
							vRotate = 		<<0.0000, 0.0000, 83.9990>>
						BREAK			
						CASE 2			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_air_cargo_04a"))
							vCoords = 		<<-2083.4089, 3249.5381, 31.8000>>
							fHeading = 		252.199
							vRotate = 		<<0.0000, 0.0000, -107.8010>>
						BREAK			
						CASE 3			
							model = 		prop_conc_sacks_02a
							vCoords = 		<<-2089.5740, 3241.8569, 31.8360>>
							fHeading = 		147.799
							vRotate = 		<<0.0000, 0.0000, 147.7990>>
						BREAK			
						CASE 4			
							model = 		prop_conc_sacks_02a
							vCoords = 		<<-2087.2959, 3240.4441, 31.8360>>
							fHeading = 		147.799
							vRotate = 		<<0.0000, 0.0000, 147.7990>>
						BREAK			
						CASE 5			
							model = 		gr_prop_gr_target_02b
							vCoords = 		<<-2087.2539, 3240.5110, 32.6400>>
							fHeading = 		147.799
							vRotate = 		<<0.0000, 0.0000, 147.7990>>
						BREAK			
						CASE 6			
							model = 		gr_prop_gr_target_02b
							vCoords = 		<<-2089.4800, 3241.9680, 32.6490>>
							fHeading = 		147.799
							vRotate = 		<<0.0000, 0.0000, 147.7990>>
						BREAK			
						CASE 7			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_crate_01b"))
							vCoords = 		<<-2075.0859, 3257.5620, 31.8100>>
							fHeading = 		185.798
							vRotate = 		<<0.0000, 0.0000, -174.2020>>
						BREAK			
						CASE 8			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_crate_01a"))	
							vCoords = 		<<-2069.2471, 3246.3560, 31.8100>>
							fHeading = 		292.598
							vRotate = 		<<0.0000, 0.0000, -67.4020>>
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_MAINTENANCE_OUTFITS_1
			SWITCH eSubvariation
				CASE CHS_MO1_VARIATION_1
					SWITCH iProp				
						CASE 0			
							model = 		prop_cleaning_trolly
							vCoords = 		<<-1559.0829, -3232.5034, 25.3424>>
							fHeading = 		292.998
							vRotate = 		<<0.0000, 0.0000, -67.0020>>
						BREAK			
						CASE 1			
							model = 		prop_toolchest_01
							vCoords = 		<<-1562.2488, -3239.2468, 28.6341>>
							fHeading = 		275.9999
							vRotate = 		<<0.0000, 0.0000, -84.0001>>
						BREAK			
						CASE 2			
							model = 		prop_paint_stepl01
							vCoords = 		<<-1594.3215, -3236.6472, 12.9648>>
							fHeading = 		215.7977
							vRotate = 		<<0.0000, -0.0000, -144.2023>>
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_MO1_VARIATION_2
					SWITCH iProp				
						CASE 0			
							model = 		prop_cleaning_trolly
							vCoords = 		<<-1172.2290, -239.1050, 36.9410>>
							fHeading = 		91.199
							vRotate = 		<<0.0000, 0.0000, 91.1990>>
						BREAK			
						CASE 1			
							model = 		prop_paint_stepl01
							vCoords = 		<<-1169.9590, -234.3790, 36.9620>>
							fHeading = 		266.599
							vRotate = 		<<0.0000, 0.0000, -93.4010>>
						BREAK			
						CASE 2			
							model = 		prop_toolchest_01
							vCoords = 		<<-1153.2325, -224.7761, 36.9110>>
							fHeading = 		89.3979
							vRotate = 		<<0.0000, 0.0000, 89.3979>>
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_MO1_VARIATION_3
					SWITCH iProp				
						CASE 0			
							model = 		prop_toolchest_01
							vCoords = 		<<-2183.8513, 4290.9219, 52.8154>>
							fHeading = 		206.9989
							vRotate = 		<<0.0000, -0.0000, -153.0011>>
						BREAK			
						CASE 1			
							model = 		prop_cleaning_trolly
							vCoords = 		<<-2174.6011, 4289.3511, 48.0660>>
							fHeading = 		356.599
							vRotate = 		<<0.0000, 0.0000, -3.4010>>
						BREAK			
						CASE 2			
							model = 		prop_paint_stepl01
							vCoords = 		<<-2186.2029, 4291.3101, 52.8350>>
							fHeading = 		13.399
							vRotate = 		<<0.0000, 0.0000, 13.3990>>
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_GRUPPE_SECHS_VAN_1
			SWITCH eSubvariation
				CASE CHS_GS1_VARIATION_1
					SWITCH iProp				
						CASE 0			
							model = 		prop_carcreeper
							vCoords = 		<<484.9670, -1279.4180, 28.5350>>
							fHeading = 		286.799
							vRotate = 		<<0.0000, 0.0000, -73.2010>>
						BREAK			
						CASE 1			
							model = 		prop_carjack
							vCoords = 		<<477.5360, -1281.8330, 28.5390>>
							fHeading = 		119.598
							vRotate = 		<<0.0000, 0.0000, 119.5980>>
						BREAK			
						CASE 2			
							model = 		prop_toolchest_05
							vCoords = 		<<477.1720, -1286.3560, 28.5610>>
							fHeading = 		179.798
							vRotate = 		<<0.0000, 0.0000, 179.7980>>
						BREAK			
						CASE 3			
							model = 		prop_compressor_02
							vCoords = 		<<481.4040, -1277.1520, 28.5670>>
							fHeading = 		345.798
							vRotate = 		<<0.0000, 0.0000, -14.2020>>
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_GS1_VARIATION_2
					SWITCH iProp				
						CASE 0			
							model = 		prop_carcreeper
							vCoords = 		<<257.6980, 2575.3340, 44.2250>>
							fHeading = 		136.199
							vRotate = 		<<4.9050, 1.1040, 136.1520>>
						BREAK			
						CASE 1			
							model = 		prop_carjack
							vCoords = 		<<253.2840, 2580.0439, 44.3020>>
							fHeading = 		64.152
							vRotate = 		<<-0.6760, 3.7870, 64.1740>>
						BREAK			
						CASE 2			
							model = 		prop_toolchest_05
							vCoords = 		<<260.8610, 2583.1531, 43.8330>>
							fHeading = 		11.374
							vRotate = 		<<0.0000, 0.0000, 11.3740>>
						BREAK			
						CASE 3			
							model = 		prop_compressor_02
							vCoords = 		<<262.3700, 2576.3130, 44.1080>>
							fHeading = 		235.374
							vRotate = 		<<0.0000, 0.0000, -124.6260>>
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_GS1_VARIATION_3
					SWITCH iProp				
						CASE 0			
							model = 		prop_carcreeper
							vCoords = 		<<130.9720, 6625.0010, 30.7290>>
							fHeading = 		200.399
							vRotate = 		<<0.0000, 0.0000, -159.6010>>
						BREAK			
						CASE 1			
							model = 		prop_carjack
							vCoords = 		<<125.0090, 6625.6602, 30.7960>>
							fHeading = 		113.399
							vRotate = 		<<0.0000, 0.0000, 113.3990>>
						BREAK			
						CASE 2			
							model = 		prop_toolchest_05
							vCoords = 		<<133.8950, 6632.5181, 30.6900>>
							fHeading = 		314.799
							vRotate = 		<<0.0000, 0.0000, -45.2010>>
						BREAK			
						CASE 3			
							model = 		prop_compressor_02
							vCoords = 		<<133.8090, 6628.5508, 30.6790>>
							fHeading = 		246.398
							vRotate = 		<<0.0000, 0.0000, -113.6020>>
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_UNMARKED_WEAPONS
			SWITCH eSubvariation
				CASE CHS_UW_ELYSIAN_ISLAND
					SWITCH iProp				
						CASE 0			
							model = 		prop_byard_rampold_cr
							vCoords = 		<<515.0360, -2930.1880, 5.1410>>
							fHeading = 		90.2
							vRotate = 		<<0.0000, 10.6000, 90.2000>>
						BREAK			
						CASE 1			
							model = 		prop_byard_rampold_cr
							vCoords = 		<<487.1860, -2930.1990, 5.1320>>
							fHeading = 		89.8
							vRotate = 		<<0.0000, 10.6100, 89.8000>>
						BREAK			
						CASE 2			
							model = 		prop_box_wood04a
							vCoords = 		<<426.8930, -2916.4221, 5.7390>>
							fHeading = 		147.999
							vRotate = 		<<0.0000, 0.0000, 147.9990>>
						BREAK			
						CASE 3			
							model = 		prop_box_wood04a
							vCoords = 		<<473.6740, -2911.4680, 5.7390>>
							fHeading = 		179.799
							vRotate = 		<<0.0000, 0.0000, 179.7990>>
						BREAK			
						CASE 4			
							model = 		prop_box_wood04a
							vCoords = 		<<517.5610, -2912.7810, 5.7390>>
							fHeading = 		89.598
							vRotate = 		<<0.0000, 0.0000, 89.5980>>
						BREAK			
						CASE 5			
							model = 		prop_box_wood05a
							vCoords = 		<<488.9040, -2912.7180, 5.8180>>
							fHeading = 		90.198
							vRotate = 		<<0.0000, 0.0000, 90.1980>>
						BREAK			
						CASE 6			
							model = 		prop_box_wood05a
							vCoords = 		<<510.6640, -2905.1589, 5.8180>>
							fHeading = 		270.197
							vRotate = 		<<0.0000, 0.0000, -89.8030>>
						BREAK			
						CASE 7			
							model = 		prop_box_wood05a
							vCoords = 		<<434.1940, -2920.8330, 8.5440>>
							fHeading = 		-89.803
							vRotate = 		<<0.0000, 0.0000, -89.8030>>
						BREAK			
						CASE 8			
							model = 		prop_box_wood05b
							vCoords = 		<<433.9670, -2904.4270, 8.5440>>
							fHeading = 		1.397
							vRotate = 		<<0.0000, 0.0000, 1.3970>>
						BREAK			
						CASE 9			
							model = 		prop_box_wood05b
							vCoords = 		<<481.4960, -2905.1599, 5.8180>>
							fHeading = 		89.597
							vRotate = 		<<0.0000, 0.0000, 89.5970>>
						BREAK			
						CASE 10			
							model = 		prop_box_wood05b
							vCoords = 		<<547.9810, -2918.6130, 5.8180>>
							fHeading = 		89.597
							vRotate = 		<<0.0000, 0.0000, 89.5970>>
						BREAK		
						CASE 11			
							model = 		prop_box_wood05a
							vCoords = 		<<489.4852, -2932.0776, 5.1266>>
							fHeading = 		252.1989
							vRotate = 		<<0.0000, -0.0000, -107.8011>>
						BREAK			
						CASE 12			
							model = 		prop_box_wood04a
							vCoords = 		<<485.2677, -2933.0488, 5.0471>>
							fHeading = 		155.7988
							vRotate = 		<<0.0000, -0.0000, 155.7988>>
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_UW_GRAPESEED
					SWITCH iProp				
						CASE 0			
							model = 		prop_box_wood04a
							vCoords = 		<<2356.8030, 4875.9590, 40.8060>>
							fHeading = 		71
							vRotate = 		<<0.0000, 0.0000, 71.0000>>
						BREAK			
						CASE 1			
							model = 		prop_box_wood04a
							vCoords = 		<<2318.4270, 4838.5479, 40.8110>>
							fHeading = 		6
							vRotate = 		<<0.0000, 0.0000, 6.0000>>
						BREAK			
						CASE 2			
							model = 		prop_box_wood04a
							vCoords = 		<<2332.3640, 4855.9419, 40.8110>>
							fHeading = 		46.6
							vRotate = 		<<0.0000, 0.0000, 46.6000>>
						BREAK			
						CASE 3			
							model = 		prop_box_wood05a
							vCoords = 		<<2331.2930, 4897.6470, 40.9080>>
							fHeading = 		48.6
							vRotate = 		<<0.0000, 0.0000, 48.6000>>
						BREAK			
						CASE 4			
							model = 		prop_box_wood05a
							vCoords = 		<<2354.8589, 4875.3359, 40.8970>>
							fHeading = 		227.999
							vRotate = 		<<0.0000, 0.0000, -132.0010>>
						BREAK			
						CASE 5			
							model = 		prop_box_wood05a
							vCoords = 		<<2317.4529, 4856.8379, 40.8900>>
							fHeading = 		327.399
							vRotate = 		<<0.0000, 0.0000, -32.6010>>
						BREAK			
						CASE 6			
							model = 		prop_box_wood05a
							vCoords = 		<<2296.6379, 4867.0508, 40.8900>>
							fHeading = 		135.199
							vRotate = 		<<0.0000, 0.0000, 135.1990>>
						BREAK			
						CASE 7			
							model = 		prop_box_wood05b
							vCoords = 		<<2320.4851, 4839.8589, 40.8900>>
							fHeading = 		216.398
							vRotate = 		<<0.0000, 0.0000, -143.6020>>
						BREAK			
						CASE 8			
							model = 		prop_box_wood05b
							vCoords = 		<<2333.9961, 4857.4229, 40.8900>>
							fHeading = 		54.598
							vRotate = 		<<0.0000, 0.0000, 54.5980>>
						BREAK			
						CASE 9			
							model = 		prop_box_wood05b
							vCoords = 		<<2340.1230, 4893.2798, 40.9220>>
							fHeading = 		260.998
							vRotate = 		<<0.0000, 0.0000, -99.0020>>
						BREAK			
						CASE 10			
							model = 		prop_box_wood05b
							vCoords = 		<<2291.4951, 4876.5659, 40.8900>>
							fHeading = 		135.4
							vRotate = 		<<0.0000, 0.0000, 135.4000>>
						BREAK	
						CASE 11			
							model = 		prop_box_wood05a
							vCoords = 		<<2296.5364, 4888.1680, 40.5733>>
							fHeading = 		168.399
							vRotate = 		<<3.5558, 3.6831, 168.2846>>
						BREAK			
						CASE 12			
							model = 		prop_box_wood04a
							vCoords = 		<<2295.9111, 4892.1924, 40.2669>>
							fHeading = 		244.7997
							vRotate = 		<<0.0000, -0.0000, -115.2003>>
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_UW_STONER
					SWITCH iProp				
						CASE 0			
							model = 		prop_box_wood04a
							vCoords = 		<<289.2830, 2854.3069, 42.6450>>
							fHeading = 		76.399
							vRotate = 		<<0.0000, 0.0000, 76.3990>>
						BREAK			
						CASE 1			
							model = 		prop_box_wood04a
							vCoords = 		<<335.1110, 2862.7820, 42.4390>>
							fHeading = 		11.399
							vRotate = 		<<0.0000, 0.0000, 11.3990>>
						BREAK			
						CASE 2			
							model = 		prop_box_wood04a
							vCoords = 		<<296.5740, 2881.2930, 42.4970>>
							fHeading = 		285.599
							vRotate = 		<<0.0000, 0.0000, -74.4010>>
						BREAK			
						CASE 3			
							model = 		prop_box_wood05a
							vCoords = 		<<288.6630, 2823.8220, 44.0630>>
							fHeading = 		293.799
							vRotate = 		<<0.0000, 0.0000, -66.2010>>
						BREAK			
						CASE 4			
							model = 		prop_box_wood05a
							vCoords = 		<<285.6590, 2840.1970, 42.7250>>
							fHeading = 		308.999
							vRotate = 		<<0.0000, 0.0000, -51.0010>>
						BREAK			
						CASE 5			
							model = 		prop_box_wood05a
							vCoords = 		<<300.7090, 2898.0559, 42.6890>>
							fHeading = 		338.199
							vRotate = 		<<0.0000, 0.0000, -21.8010>>
						BREAK			
						CASE 6			
							model = 		prop_box_wood05a
							vCoords = 		<<272.9220, 2876.6521, 42.6930>>
							fHeading = 		213.998
							vRotate = 		<<0.0000, 0.0000, -146.0020>>
						BREAK			
						CASE 7			
							model = 		prop_box_wood05b
							vCoords = 		<<256.4530, 2874.5271, 42.6900>>
							fHeading = 		107.798
							vRotate = 		<<0.0000, 0.0000, 107.7980>>
						BREAK			
						CASE 8			
							model = 		prop_box_wood05b
							vCoords = 		<<315.1860, 2892.1221, 45.5500>>
							fHeading = 		30.798
							vRotate = 		<<0.0000, 0.0000, 30.7980>>
						BREAK			
						CASE 9			
							model = 		prop_box_wood05b
							vCoords = 		<<317.9290, 2886.2910, 45.4700>>
							fHeading = 		32.798
							vRotate = 		<<0.0000, 0.0000, 32.7980>>
						BREAK			
						CASE 10			
							model = 		prop_box_wood05b
							vCoords = 		<<332.7340, 2862.3501, 42.5180>>
							fHeading = 		25.198
							vRotate = 		<<0.0000, 0.0000, 25.1980>>
						BREAK	
						CASE 11			
							model = 		prop_box_wood05a
							vCoords = 		<<293.5779, 2837.7476, 42.5706>>
							fHeading = 		347.5986
							vRotate = 		<<0.0000, 0.0000, -12.4014>>
						BREAK			
						CASE 12			
							model = 		prop_box_wood04a
							vCoords = 		<<295.3759, 2833.5017, 42.4387>>
							fHeading = 		290.1989
							vRotate = 		<<0.0000, 0.0000, -69.8011>>
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_STEAL_EMP
			SWITCH eSubvariation
				CASE CHS_EMP_ULSA1
					SWITCH iProp				
						CASE 0			
							model = 		prop_box_wood05b
							vCoords = 		<<-1754.6520, 212.3440, 63.4510>>
							fHeading = 		236.599
							vRotate = 		<<0.0000, 0.0000, -123.4010>>
						BREAK			
						CASE 1			
							model = 		prop_flattruck_01d
							vCoords = 		<<-1751.5410, 215.1980, 63.4620>>
							fHeading = 		173.799
							vRotate = 		<<0.0000, 0.0000, 173.7990>>
						BREAK			
						CASE 2			
							model = 		prop_flattruck_01a
							vCoords = 		<<-1752.5010, 213.2630, 63.3740>>
							fHeading = 		33.199
							vRotate = 		<<0.0000, 0.0000, 33.1990>>
						BREAK			
						CASE 3			
							model = 		prop_box_wood07a
							vCoords = 		<<-1751.0120, 210.7450, 63.4770>>
							fHeading = 		216.199
							vRotate = 		<<0.0000, 0.0000, -143.8010>>
						BREAK	
						CASE 4		
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_emp_01b"))
							vCoords = 		<<-1750.9207, 210.8424, 64.5439>>
							fHeading = 		35.9988
							vRotate = 		<<0.0000, 0.0000, 35.9988>>
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_EMP_ULSA2
					SWITCH iProp				
						CASE 0			
							model = 		prop_flattruck_01d
							vCoords = 		<<-1692.5920, 153.5850, 63.2150>>
							fHeading = 		333.6
							vRotate = 		<<0.0000, 0.0000, -26.4000>>
						BREAK			
						CASE 1			
							model = 		prop_flattruck_01a
							vCoords = 		<<-1693.6390, 146.6410, 63.3710>>
							fHeading = 		226.6
							vRotate = 		<<0.0000, 0.0000, -133.4000>>
						BREAK			
						CASE 2			
							model = 		prop_box_wood05b
							vCoords = 		<<-1698.8910, 149.4940, 63.4540>>
							fHeading = 		81.399
							vRotate = 		<<0.0000, 0.0000, 81.3990>>
						BREAK			
						CASE 3			
							model = 		prop_box_wood07a
							vCoords = 		<<-1695.5560, 148.5820, 63.4600>>
							fHeading = 		119.399
							vRotate = 		<<0.0000, 0.0000, 119.3990>>
						BREAK			
						CASE 4			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_emp_01b"))
							vCoords = 		<<-1695.5889, 148.6816, 64.5269>>
							fHeading = 		119.5991
							vRotate = 		<<0.0000, -0.0000, 119.5991>>
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_EMP_ULSA3
					SWITCH iProp				
						CASE 0			
							model = 		prop_box_wood05b
							vCoords = 		<<-1613.8430, 226.4940, 59.0320>>
							fHeading = 		121.399
							vRotate = 		<<0.0000, 0.0000, 121.3990>>
						BREAK			
						CASE 1			
							model = 		prop_flattruck_01d
							vCoords = 		<<-1608.6860, 226.3330, 58.4960>>
							fHeading = 		79.599
							vRotate = 		<<3.6670, -0.8120, 79.6250>>
						BREAK			
						CASE 2			
							model = 		prop_flattruck_01a
							vCoords = 		<<-1611.0090, 232.6630, 58.7860>>
							fHeading = 		33.425
							vRotate = 		<<0.0000, 0.0000, 33.4250>>
						BREAK			
						CASE 3			
							model = 		prop_box_wood07a
							vCoords = 		<<-1609.2230, 230.8530, 58.7820>>
							fHeading = 		329.025
							vRotate = 		<<0.0000, 4.0000, -30.9750>>
						BREAK
						CASE 4		
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_emp_01b"))
							vCoords = 		<<-1609.0685, 230.7363, 59.8414>>
							fHeading = 		329.025
							vRotate = 		<<0.0000, 4.0000, -30.9750>>
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_SEWER_TUNNEL_DRILL_2
			SWITCH eSubvariation
				CASE CHS_STD2_VARIATION_1
					SWITCH iProp				
					CASE 0			
						model = 		prop_conc_blocks01c
						vCoords = 		<<2022.7494, 4976.6953, 40.2265>>
						fHeading = 		279.7984
						vRotate = 		<<0.0000, 0.0000, -80.2016>>
					BREAK			
					CASE 1			
						model = 		prop_generator_03b
						vCoords = 		<<2010.0903, 4992.1748, 40.3269>>
						fHeading = 		176.2328
						vRotate = 		<<0.0000, -0.0000, 176.2328>>
					BREAK			
				ENDSWITCH				
				BREAK
				CASE CHS_STD2_VARIATION_2
					SWITCH iProp				
						CASE 0			
							model = 		prop_conc_blocks01c
							vCoords = 		<<2650.7671, 2799.6028, 33.1618>>
							fHeading = 		249.7949
							vRotate = 		<<0.0000, -0.0000, -110.2051>>
						BREAK			
						CASE 1			
							model = 		prop_generator_03b
							vCoords = 		<<2656.6221, 2796.9543, 33.1774>>
							fHeading = 		45.1543
							vRotate = 		<<0.0000, 0.0000, 45.1543>>
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_STD2_VARIATION_3
					SWITCH iProp				
						CASE 0			
							model = 		prop_conc_blocks01c
							vCoords = 		<<1391.1479, -2066.5127, 50.9987>>
							fHeading = 		4.5989
							vRotate = 		<<0.0000, 0.0000, 4.5989>>
						BREAK			
						CASE 1			
							model = 		prop_generator_03b
							vCoords = 		<<1399.4448, -2059.7544, 51.0579>>
							fHeading = 		153.9965
							vRotate = 		<<0.0000, -0.0000, 153.9965>>
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_RIOT_VAN
			SWITCH eSubvariation
				CASE CHS_RV_VARIATION_1
					SWITCH iProp				
						CASE 0			
							model = 		prop_barrier_work05
							vCoords = 		<<1160.8900, -1664.2900, 35.5120>>
							fHeading = 		31.399
							vRotate = 		<<1.2910, 4.9300, 31.3440>>
						BREAK			
						CASE 1			
							model = 		prop_barrier_work05
							vCoords = 		<<1162.4130, -1653.8190, 35.9170>>
							fHeading = 		301.144
							vRotate = 		<<0.0000, 0.0000, -58.8560>>
						BREAK			
						CASE 2			
							model = 		bkr_prop_weed_bigbag_open_01a
							vCoords = 		<<1165.7209, -1647.8890, 35.9260>>
							fHeading = 		334.543
							vRotate = 		<<0.0000, 0.0000, -25.4570>>
						BREAK			
						CASE 3			
							model = 		prop_idol_case_02
							vCoords = 		<<1167.3260, -1645.7200, 35.9850>>
							fHeading = 		351.943
							vRotate = 		<<0.0000, 0.0000, -8.0570>>
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_RV_VARIATION_2
					SWITCH iProp				
						CASE 0			
							model = 		prop_barrier_work05
							vCoords = 		<<1784.1890, 3334.4670, 40.1460>>
							fHeading = 		301.398
							vRotate = 		<<0.0000, 0.0000, -58.6020>>
						BREAK			
						CASE 1			
							model = 		prop_barrier_work05
							vCoords = 		<<1778.3860, 3342.6919, 39.8260>>
							fHeading = 		335.598
							vRotate = 		<<-3.8620, 0.1230, -24.3980>>
						BREAK			
						CASE 2			
							model = 		bkr_prop_weed_bigbag_open_01a
							vCoords = 		<<1779.3719, 3329.9729, 40.2410>>
							fHeading = 		51.202
							vRotate = 		<<0.0000, 0.0000, 51.2020>>
						BREAK			
						CASE 3			
							model = 		prop_idol_case_02
							vCoords = 		<<1778.0750, 3334.0640, 40.2560>>
							fHeading = 		351.202
							vRotate = 		<<0.0000, 0.0000, -8.7980>>
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_RV_VARIATION_3
					SWITCH iProp				
						CASE 0			
							model = 		prop_barrier_work05
							vCoords = 		<<1678.4969, 4925.2588, 41.0660>>
							fHeading = 		234.599
							vRotate = 		<<0.0000, 0.0000, -125.4010>>
						BREAK			
						CASE 1			
							model = 		prop_barrier_work05
							vCoords = 		<<1692.6240, 4925.9619, 41.0830>>
							fHeading = 		145.799
							vRotate = 		<<0.0000, 0.0000, 145.7990>>
						BREAK			
						CASE 2			
							model = 		bkr_prop_weed_bigbag_open_01a
							vCoords = 		<<1691.1780, 4915.1592, 41.0760>>
							fHeading = 		169.198
							vRotate = 		<<0.0000, 0.0000, 169.1980>>
						BREAK			
						CASE 3			
							model = 		prop_idol_case_02
							vCoords = 		<<1686.3370, 4916.2749, 41.1440>>
							fHeading = 		76.198
							vRotate = 		<<0.0000, 0.0000, 76.1980>>
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
			
			SWITCH iProp				
				CASE 4			
					model = 		V_SERV_ABOX_02
					vCoords = 		GET_CSH_VEHICLE_SPAWN_COORDS(eVariation, eSubvariation, 4)
				BREAK			
				CASE 5			
					model = 		V_SERV_ABOX_02
					vCoords = 		GET_CSH_VEHICLE_SPAWN_COORDS(eVariation, eSubvariation, 5)
				BREAK			
				CASE 6			
					model = 		V_SERV_ABOX_02
					vCoords = 		GET_CSH_VEHICLE_SPAWN_COORDS(eVariation, eSubvariation, 6)
				BREAK			
				CASE 7			
					model = 		V_SERV_ABOX_02
					vCoords = 		GET_CSH_VEHICLE_SPAWN_COORDS(eVariation, eSubvariation, 7)
				BREAK			
			ENDSWITCH
		BREAK
		CASE CHV_MERRYWEATHER_CONVOY
			SWITCH eSubvariation
				CASE CHS_MWC_LSIA
					SWITCH iProp				
						CASE 0			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_cargo_04b"))
							vCoords = 		<<-1376.4995, -2731.3511, 12.9449>>
							fHeading = 		274.9987
							vRotate = 		<<0.0000, 0.0000, -85.0013>>
						BREAK			
						CASE 1			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_cargo_04b"))
							vCoords = 		<<-1366.8997, -2740.4954, 12.9449>>
							fHeading = 		148.9984
							vRotate = 		<<0.0000, -0.0000, 148.9984>>
						BREAK			
						CASE 2			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_air_cargo_04a"))
							vCoords = 		<<-1362.2219, -2732.2783, 12.9349>>
							fHeading = 		22.9981
							vRotate = 		<<0.0000, 0.0000, 22.9981>>
						BREAK			
						CASE 3			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_air_cargo_04a"))
							vCoords = 		<<-1376.7905, -2737.3923, 12.9349>>
							fHeading = 		332.3981
							vRotate = 		<<0.0000, 0.0000, -27.6019>>
						BREAK
						CASE 4			
							model = 		prop_worklight_04d
							vCoords = 		<<-1370.6188, -2743.3618, 13.4332>>
							fHeading = 		185.7996
							vRotate = 		<<0.0000, -0.0000, -174.2003>>
						BREAK			
						CASE 5			
							model = 		prop_worklight_04d
							vCoords = 		<<-1378.0195, -2734.9714, 13.4332>>
							fHeading = 		86.5996
							vRotate = 		<<0.0000, 0.0000, 86.5996>>
						BREAK			
						CASE 6			
							model = 		prop_worklight_04d
							vCoords = 		<<-1365.8564, -2737.8135, 13.4332>>
							fHeading = 		223.7993
							vRotate = 		<<0.0000, -0.0000, -136.2006>>
						BREAK			
						CASE 7			
							model = 		prop_worklight_04d
							vCoords = 		<<-1374.2952, -2727.6099, 13.4332>>
							fHeading = 		31.3995
							vRotate = 		<<0.0000, 0.0000, 31.3995>>
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_MWC_SANDY_SHORES
					SWITCH iProp				
						CASE 0			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_cargo_04b"))
							vCoords = 		<<1165.5132, 3045.0132, 39.4070>>
							fHeading = 		131.1996
							vRotate = 		<<0.0000, -0.0000, 131.1996>>
						BREAK			
						CASE 1			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_cargo_04b"))
							vCoords = 		<<1174.7660, 3034.9990, 39.5341>>
							fHeading = 		106.1994
							vRotate = 		<<0.0000, -0.0000, 106.1993>>
						BREAK			
						CASE 2			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_air_cargo_04a"))
							vCoords = 		<<1171.9138, 3044.7783, 39.4846>>
							fHeading = 		93.7993
							vRotate = 		<<0.0000, -0.0000, 93.7993>>
						BREAK			
						CASE 3			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_air_cargo_04a"))
							vCoords = 		<<1162.2953, 3031.5532, 39.3604>>
							fHeading = 		48.9992
							vRotate = 		<<0.0000, 0.0000, 48.9992>>
						BREAK
						CASE 4			
							model = 		prop_worklight_04d
							vCoords = 		<<1178.2386, 3042.8682, 40.0224>>
							fHeading = 		295.5999
							vRotate = 		<<0.0000, 0.0000, -64.4001>>
						BREAK			
						CASE 5			
							model = 		prop_worklight_04d
							vCoords = 		<<1172.3937, 3032.3328, 40.0116>>
							fHeading = 		219.9996
							vRotate = 		<<0.0000, -0.0000, -140.0004>>
						BREAK			
						CASE 6			
							model = 		prop_worklight_04d
							vCoords = 		<<1169.5621, 3045.8850, 39.9251>>
							fHeading = 		350.7993
							vRotate = 		<<0.0000, 0.0000, -9.2007>>
						BREAK			
						CASE 7			
							model = 		prop_worklight_04d
							vCoords = 		<<1164.9656, 3031.6221, 39.8939>>
							fHeading = 		157.3992
							vRotate = 		<<0.0000, -0.0000, 157.3992>>
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_EXPLOSIVES_1
			SWITCH eSubvariation
				CASE CHS_EXP1_VARIATION_1
					SWITCH iProp				
						CASE 0			
							model = 		bkr_prop_meth_table01a
							vCoords = 		<<1751.6890, 3869.6189, 33.5350>>
							fHeading = 		122.2
							vRotate = 		<<0.0000, 0.0000, 122.2000>>
						BREAK			
						CASE 1			
							model = 		prop_crate_11e
							vCoords = 		<<1746.8040, 3863.8469, 33.5710>>
							fHeading = 		231.999
							vRotate = 		<<0.0000, 0.0000, -128.0010>>
						BREAK			
						CASE 2			
							model = 		prop_crate_11e
							vCoords = 		<<1745.7990, 3867.7161, 33.5860>>
							fHeading = 		112.999
							vRotate = 		<<0.0000, 0.0000, 112.9990>>
						BREAK			
						CASE 3			
							model = 		prop_boombox_01
							vCoords = 		<<1741.9709, 3863.9729, 35.6630>>
							fHeading = 		104
							vRotate = 		<<0.0000, 0.0000, 104.0000>>
						BREAK			
						CASE 4			
							model = 		prop_meth_setup_01
							vCoords = 		<<1740.0750, 3866.2639, 33.8160>>
							fHeading = 		324.8
							vRotate = 		<<0.0000, 0.0000, -35.2000>>
						BREAK			
						CASE 5			
							model = 		prop_beer_pissh
							vCoords = 		<<1747.8873, 3856.1353, 33.6675>>
							fHeading = 		0
							vRotate = 		<<0.0000, 0.0000, 0.0000>>
						BREAK			
						CASE 6			
							model = 		prop_beer_pissh
							vCoords = 		<<1748.1176, 3856.2710, 33.6736>>
							fHeading = 		233.9999
							vRotate = 		<<0.0000, -0.0000, -126.0001>>
						BREAK			
						CASE 7			
							model = 		prop_beer_pissh
							vCoords = 		<<1748.3228, 3856.4114, 33.6675>>
							fHeading = 		284.9997
							vRotate = 		<<0.0000, 0.0000, -75.0003>>
						BREAK			
						CASE 8			
							model = 		prop_beer_pissh
							vCoords = 		<<1748.5593, 3856.5266, 33.6806>>
							fHeading = 		34.3995
							vRotate = 		<<0.0000, 0.0000, 34.3995>>
						BREAK			
						CASE 9			
							model = 		prop_beer_pissh
							vCoords = 		<<1748.7407, 3856.6589, 33.6857>>
							fHeading = 		261.1995
							vRotate = 		<<0.0000, -0.0000, -98.8005>>
						BREAK			
						CASE 10			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("v_ret_ml_beerpis2"))
							vCoords = 		<<1749.1481, 3857.5039, 33.7239>>
							fHeading = 		230.5993
							vRotate = 		<<0.0000, -0.0000, -129.4007>>
						BREAK			

					ENDSWITCH				
				BREAK
				CASE CHS_EXP1_VARIATION_2
					SWITCH iProp				
						CASE 0			
							model = 		prop_meth_setup_01
							vCoords = 		<<-36.0410, 3029.6321, 40.2190>>
							fHeading = 		218.6
							vRotate = 		<<0.0000, 0.0000, -141.4000>>
						BREAK			
						CASE 1			
							model = 		bkr_prop_meth_table01a
							vCoords = 		<<-33.3630, 3018.7061, 39.7110>>
							fHeading = 		20.999
							vRotate = 		<<0.0000, 0.0000, 20.9990>>
						BREAK			
						CASE 2			
							model = 		prop_crate_11e
							vCoords = 		<<-35.8590, 3023.2620, 39.8930>>
							fHeading = 		353.799
							vRotate = 		<<0.0000, 0.0000, -6.2010>>
						BREAK			
						CASE 3			
							model = 		prop_crate_11e
							vCoords = 		<<-33.9130, 3024.5281, 39.9040>>
							fHeading = 		260.799
							vRotate = 		<<0.0000, 0.0000, -99.2010>>
						BREAK			
						CASE 4			
							model = 		prop_boombox_01
							vCoords = 		<<-31.3543, 3020.5142, 41.0880>>
							fHeading = 		216.2001
							vRotate = 		<<-1.0001, -0.0000, -143.7999>>
						BREAK			
						CASE 5			
							model = 		prop_beer_pissh
							vCoords = 		<<-45.1002, 3031.4111, 40.5824>>
							fHeading = 		163.5984
							vRotate = 		<<0.0000, -0.0000, 163.5983>>
						BREAK			
						CASE 6			
							model = 		prop_beer_pissh
							vCoords = 		<<-44.9033, 3031.4724, 40.5760>>
							fHeading = 		90.1981
							vRotate = 		<<0.0000, -0.0000, 90.1981>>
						BREAK			
						CASE 7			
							model = 		prop_beer_pissh
							vCoords = 		<<-44.6862, 3031.5447, 40.5704>>
							fHeading = 		331.1979
							vRotate = 		<<0.0000, 0.0000, -28.8021>>
						BREAK			
						CASE 8			
							model = 		prop_beer_pissh
							vCoords = 		<<-45.2606, 3031.4014, 40.5764>>
							fHeading = 		228.198
							vRotate = 		<<0.0000, -0.0000, -131.8020>>
						BREAK			
						CASE 9			
							model = 		prop_beer_pissh
							vCoords = 		<<-45.4885, 3031.3738, 40.5754>>
							fHeading = 		94.9979
							vRotate = 		<<0.0000, -0.0000, 94.9979>>
						BREAK			
						CASE 10			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("v_ret_ml_beerpis2"))
							vCoords = 		<<-45.0983, 3030.2820, 39.9122>>
							fHeading = 		53.3977
							vRotate = 		<<0.0000, 0.0000, 53.3977>>
						BREAK			
			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_FLIGHT_SCHEDULE
			SWITCH eSubvariation
				CASE CHS_FS_ELYSIAN_ISLAND
					SWITCH iProp				
						CASE 0			
							model = 		prop_conc_sacks_02a
							vCoords = 		<<453.5290, -3071.7949, 5.1250>>
							fHeading = 		177.399
							vRotate = 		<<0.0000, 0.0000, 177.3990>>
						BREAK			
						CASE 1			
							model = 		prop_conc_sacks_02a
							vCoords = 		<<455.2750, -3095.8010, 5.0960>>
							fHeading = 		179.599
							vRotate = 		<<0.0000, 0.0000, 179.5990>>
						BREAK			
						CASE 2			
							model = 		prop_conc_sacks_02a
							vCoords = 		<<461.4160, -3103.5630, 5.0960>>
							fHeading = 		122.399
							vRotate = 		<<0.0000, 0.0000, 122.3990>>
						BREAK			
						CASE 3			
							model = 		prop_box_wood04a
							vCoords = 		<<461.3190, -3091.5449, 5.0730>>
							fHeading = 		34.199
							vRotate = 		<<0.0000, 0.0000, 34.1990>>
						BREAK			
						CASE 4			
							model = 		prop_box_wood04a
							vCoords = 		<<451.5620, -3101.9099, 5.0730>>
							fHeading = 		45.199
							vRotate = 		<<0.0000, 0.0000, 45.1990>>
						BREAK			
						CASE 5			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_cargo_04a"))
							vCoords = 		<<457.1730, -3109.3081, 5.0700>>
							fHeading = 		87.199
							vRotate = 		<<0.0000, 0.0000, 87.1990>>
						BREAK			
						CASE 6			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_cargo_04a"))
							vCoords = 		<<460.5340, -3096.3379, 5.0700>>
							fHeading = 		88.399
							vRotate = 		<<0.0000, 0.0000, 88.3990>>
						BREAK			
						CASE 7			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_air_cargo_04a"))
							vCoords = 		<<466.7870, -3088.5601, 5.0600>>
							fHeading = 		88.399
							vRotate = 		<<0.0000, 0.0000, 88.3990>>
						BREAK			
						CASE 8			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_air_cargo_04a"))
							vCoords = 		<<466.0590, -3101.2581, 5.0600>>
							fHeading = 		46.399
							vRotate = 		<<0.0000, 0.0000, 46.3990>>
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_FS_SANDY_SHORES
					SWITCH iProp				
						CASE 0			
							model = 		prop_conc_sacks_02a
							vCoords = 		<<1700.9020, 3276.0349, 40.1780>>
							fHeading = 		33.198
							vRotate = 		<<0.0000, 0.0000, 33.1980>>
						BREAK			
						CASE 1			
							model = 		prop_conc_sacks_02a
							vCoords = 		<<1690.4070, 3273.6541, 40.0090>>
							fHeading = 		346.998
							vRotate = 		<<0.0000, 0.0000, -13.0020>>
						BREAK			
						CASE 2			
							model = 		prop_conc_sacks_02a
							vCoords = 		<<1704.5861, 3286.0769, 40.1700>>
							fHeading = 		113.398
							vRotate = 		<<0.0000, 0.0000, 113.3980>>
						BREAK			
						CASE 3			
							model = 		prop_box_wood04a
							vCoords = 		<<1697.3390, 3281.1750, 40.0990>>
							fHeading = 		0
							vRotate = 		<<0.0000, 0.0000, -0.0000>>
						BREAK			
						CASE 4			
							model = 		prop_box_wood04a
							vCoords = 		<<1690.8260, 3281.2390, 40.1420>>
							fHeading = 		273
							vRotate = 		<<0.0000, 0.0000, -87.0000>>
						BREAK			
						CASE 5			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_cargo_04a"))
							vCoords = 		<<1695.1660, 3287.0430, 40.1470>>
							fHeading = 		194.799
							vRotate = 		<<0.0000, 0.0000, -165.2010>>
						BREAK			
						CASE 6			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_cargo_04a"))
							vCoords = 		<<1687.9370, 3288.3611, 40.1470>>
							fHeading = 		305.399
							vRotate = 		<<0.0000, 0.0000, -54.6010>>
						BREAK			
						CASE 7			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_air_cargo_04a"))
							vCoords = 		<<1700.7111, 3285.4089, 40.1360>>
							fHeading = 		123.799
							vRotate = 		<<0.0000, 0.0000, 123.7990>>
						BREAK			
						CASE 8			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_air_cargo_04a"))
							vCoords = 		<<1709.4390, 3276.5259, 40.1400>>
							fHeading = 		72.599
							vRotate = 		<<0.0000, 0.0000, 72.5990>>
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_FS_POWER_STATION
					SWITCH iProp				
						CASE 0			
							model = 		prop_conc_sacks_02a
							vCoords = 		<<2682.8159, 1429.6021, 23.5250>>
							fHeading = 		88.8
							vRotate = 		<<0.0000, 0.0000, 88.8000>>
						BREAK			
						CASE 1			
							model = 		prop_conc_sacks_02a
							vCoords = 		<<2682.7229, 1435.2740, 23.5280>>
							fHeading = 		88.8
							vRotate = 		<<0.0000, 0.0000, 88.8000>>
						BREAK			
						CASE 2			
							model = 		prop_conc_sacks_02a
							vCoords = 		<<2672.5071, 1441.5590, 23.5260>>
							fHeading = 		177.8
							vRotate = 		<<0.0000, 0.0000, 177.8000>>
						BREAK			
						CASE 3			
							model = 		prop_box_wood04a
							vCoords = 		<<2674.4790, 1427.4980, 23.5030>>
							fHeading = 		24.399
							vRotate = 		<<0.0000, 0.0000, 24.3990>>
						BREAK			
						CASE 4			
							model = 		prop_box_wood04a
							vCoords = 		<<2670.9861, 1431.3920, 23.5030>>
							fHeading = 		318.399
							vRotate = 		<<0.0000, 0.0000, -41.6010>>
						BREAK			
						CASE 5			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_air_cargo_04a"))
							vCoords = 		<<2678.1411, 1432.2560, 23.4910>>
							fHeading = 		260.399
							vRotate = 		<<0.0000, 0.0000, -99.6010>>
						BREAK			
						CASE 6			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_air_cargo_04a"))
							vCoords = 		<<2672.6421, 1421.4890, 23.4910>>
							fHeading = 		207.399
							vRotate = 		<<0.0000, 0.0000, -152.6010>>
						BREAK			
						CASE 7			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_cargo_04a"))
							vCoords = 		<<2679.8730, 1424.6870, 23.5590>>
							fHeading = 		64.998
							vRotate = 		<<0.0000, 0.0000, 64.9980>>
						BREAK			
						CASE 8			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_cargo_04a"))
							vCoords = 		<<2667.8169, 1420.8470, 23.6920>>
							fHeading = 		268.998
							vRotate = 		<<0.0000, 0.0000, -91.0020>>
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_MERRYWEATHER_TEST_SITE
			SWITCH eSubvariation
				CASE CHS_MWTS_VARIATION_1
					SWITCH iProp				
						CASE 0			
							model = 		xm_prop_x17_trail_01a
							vCoords = 		<<345.0588, 3411.7256, 36.0560>>
							fHeading = 		201.5995
							vRotate = 		<<0.5000, -1.0001, -158.4005>>
						BREAK			
						CASE 1			
							model = 		xm_prop_x17_trail_02a
							vCoords = 		<<323.4577, 3419.3701, 36.1081>>
							fHeading = 		253.5988
							vRotate = 		<<-0.4000, -1.1001, -106.4012>>
						BREAK			
						CASE 2			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_hesco_06"))
							vCoords = 		<<341.3437,3426.3872,34.9126>>
							fHeading = 		254.1985
							vRotate = 		<<0.0000, -0.0000, -105.8015>>
						BREAK			
						CASE 3			
							model = 		prop_conc_sacks_02a
							vCoords = 		<<335.0998, 3406.6650, 35.7380>>
							fHeading = 		23.5984
							vRotate = 		<<0.0000, 0.0000, 23.5984>>
						BREAK			
						CASE 4			
							model = 		prop_conc_sacks_02a
							vCoords = 		<<331.7958, 3405.3574, 35.7553>>
							fHeading = 		23.5984
							vRotate = 		<<0.0000, 0.0000, 23.5984>>
						BREAK			
						CASE 5			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_cargo_04b"))
							vCoords = 		<<336.0936, 3414.7202, 35.5881>>
							fHeading = 		170.1981
							vRotate = 		<<0.0000, -0.0000, 170.1980>>
						BREAK			
						CASE 6			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_cargo_04b"))
							vCoords = 		<<320.0231, 3404.2981, 35.7508>>
							fHeading = 		128.1977
							vRotate = 		<<0.0000, -0.0000, 128.1977>>
						BREAK			
						CASE 7			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_air_cargo_04a"))
							vCoords = 		<<333.0345, 3412.1648, 35.6396>>
							fHeading = 		128.1977
							vRotate = 		<<0.0000, -0.0000, 128.1977>>
						BREAK			
						CASE 8			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_air_cargo_04a"))
							vCoords = 		<<335.7364, 3423.8640, 35.3659>>
							fHeading = 		18.9975
							vRotate = 		<<0.0000, 0.0000, 18.9975>>
						BREAK			
						CASE 9			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("gr_Prop_GR_CratesPile_01a"))
							vCoords = 		<<348.7556, 3417.9490, 35.4017>>
							fHeading = 		68.5974
							vRotate = 		<<0.0000, 0.0000, 68.5974>>
						BREAK			
						CASE 10			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("gr_Prop_GR_CratesPile_01a"))
							vCoords = 		<<332.7584, 3415.7378, 35.5935>>
							fHeading = 		8.9973
							vRotate = 		<<0.0000, 0.0000, 8.9973>>
						BREAK			
						CASE 11			
							model = 		gr_prop_gr_target_02a
							vCoords = 		<<335.0481, 3406.7139, 36.5714>>
							fHeading = 		21.1973
							vRotate = 		<<0.0000, 0.0000, 21.1973>>
						BREAK			
						CASE 12			
							model = 		gr_prop_gr_target_02a
							vCoords = 		<<331.7506, 3405.3887, 36.5892>>
							fHeading = 		21.1973
							vRotate = 		<<0.0000, 0.0000, 21.1973>>
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_MWTS_VARIATION_2
					SWITCH iProp				
						CASE 0			
							model = 		xm_prop_x17_trail_02a
							vCoords = 		<<554.4850, -2850.9490, 5.5710>>
							fHeading = 		149.6
							vRotate = 		<<0.0000, 0.0000, 149.6000>>
						BREAK			
						CASE 1			
							model = 		xm_prop_x17_trail_01a
							vCoords = 		<<547.2540, -2830.6621, 5.5550>>
							fHeading = 		330.799
							vRotate = 		<<0.0000, 0.0000, -29.2000>>
						BREAK			
						CASE 2			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_hesco_06"))
							vCoords = 		<<534.0440, -2853.1870, 5.0580>>
							fHeading = 		60.399
							vRotate = 		<<0.0000, 0.0000, 60.3990>>
						BREAK			
						CASE 3			
							model = 		prop_conc_sacks_02a
							vCoords = 		<<530.8210, -2844.4270, 5.0710>>
							fHeading = 		240.199
							vRotate = 		<<0.0000, 0.0000, -119.8010>>
						BREAK			
						CASE 4			
							model = 		prop_conc_sacks_02a
							vCoords = 		<<542.7410, -2851.2959, 5.0710>>
							fHeading = 		58.999
							vRotate = 		<<0.0000, 0.0000, 58.9990>>
						BREAK			
						CASE 5			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_cargo_04b"))
							vCoords = 		<<543.3630, -2839.8191, 5.0450>>
							fHeading = 		355.198
							vRotate = 		<<0.0000, 0.0000, -4.8020>>
						BREAK			
						CASE 6			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_cargo_04b"))
							vCoords = 		<<532.4510, -2841.0930, 5.0450>>
							fHeading = 		331.198
							vRotate = 		<<0.0000, 0.0000, -28.8020>>
						BREAK			
						CASE 7			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_air_cargo_04a"))
							vCoords = 		<<537.8730, -2829.7451, 5.0350>>
							fHeading = 		289.198
							vRotate = 		<<0.0000, 0.0000, -70.8020>>
						BREAK			
						CASE 8			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_air_cargo_04a"))
							vCoords = 		<<522.7830, -2831.2251, 5.0350>>
							fHeading = 		60.198
							vRotate = 		<<0.0000, 0.0000, 60.1980>>
						BREAK			
						CASE 9			
							model = 		gr_prop_gr_target_02a
							vCoords = 		<<530.8640, -2844.4419, 5.9150>>
							fHeading = 		60.198
							vRotate = 		<<0.0000, 0.0000, 60.1980>>
						BREAK			
						CASE 10			
							model = 		gr_prop_gr_target_02a
							vCoords = 		<<542.6930, -2851.3010, 5.8950>>
							fHeading = 		60.198
							vRotate = 		<<0.0000, 0.0000, 60.1980>>
						BREAK			
						CASE 11			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("gr_Prop_GR_CratesPile_01a"))
							vCoords = 		<<548.9640, -2844.6431, 5.0450>>
							fHeading = 		300.998
							vRotate = 		<<0.0000, 0.0000, -59.0020>>
						BREAK			
						CASE 12			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("gr_Prop_GR_CratesPile_01a"))
							vCoords = 		<<534.5450, -2848.3210, 5.0450>>
							fHeading = 		237.798
							vRotate = 		<<0.0000, 0.0000, -122.2020>>
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	SWITCH eVariation
		CASE CHV_EXPLOSIVES_2
			SWITCH eSubvariation
				CASE CHS_EXP2_VARIATION_1
					SWITCH iProp				
						CASE 0			
							model = 		gr_prop_gr_gunsmithsupl_02a
							vCoords = 		<<845.9560, -1994.4420, 28.3040>>
							fHeading = 		269.199
							vRotate = 		<<0.0000, 0.0000, -90.8010>>
						BREAK			
						CASE 1			
							model = 		imp_prop_covered_vehicle_03a
							vCoords = 		<<1022.5060, -3106.6321, -39.9980>>
							fHeading = 		349
							vRotate = 		<<0.0000, 0.0000, -11.0000>>
						BREAK			
						CASE 2			
							model = 		imp_prop_covered_vehicle_01a
							vCoords = 		<<1022.1960, -3096.1311, -39.9940>>
							fHeading = 		-172.2
							vRotate = 		<<0.0000, 0.0000, -172.2000>>
						BREAK			
						CASE 3			
							model = 		gr_prop_gr_gunsmithsupl_02a
							vCoords = 		<<1006.1100, -3097.0950, -39.9980>>
							fHeading = 		90.2
							vRotate = 		<<0.0000, 0.0000, 90.2000>>
						BREAK			
						CASE 4			
							model = 		gr_prop_gr_gunsmithsupl_02a
							vCoords = 		<<1015.6040, -3091.8750, -39.9980>>
							fHeading = 		178.999
							vRotate = 		<<0.0000, 0.0000, 178.9990>>
						BREAK			
						CASE 5			
							model = 		gr_prop_gr_gunsmithsupl_02a
							vCoords = 		<<1013.2190, -3103.2649, -39.9980>>
							fHeading = 		90.799
							vRotate = 		<<0.0000, 0.0000, 90.7990>>
						BREAK			
						CASE 6			
							model = 		gr_prop_gr_gunsmithsupl_02a
							vCoords = 		<<993.8560, -3108.9729, -39.9980>>
							fHeading = 		90.599
							vRotate = 		<<0.0000, 0.0000, 90.5990>>
						BREAK			
						CASE 7			
							model = 		gr_prop_gr_gunsmithsupl_01a
							vCoords = 		<<1003.6780, -3108.1021, -39.9970>>
							fHeading = 		90.599
							vRotate = 		<<0.0000, 0.0000, 90.5990>>
						BREAK			
						CASE 8			
							model = 		gr_prop_gr_gunsmithsupl_01a
							vCoords = 		<<1008.5220, -3091.6230, -39.9970>>
							fHeading = 		2.199
							vRotate = 		<<0.0000, 0.0000, 2.1990>>
						BREAK			
						CASE 9			
							model = 		gr_prop_gr_gunsmithsupl_01a
							vCoords = 		<<1015.6500, -3102.5149, -39.9970>>
							fHeading = 		270.799
							vRotate = 		<<0.0000, 0.0000, -89.2010>>
						BREAK			
						CASE 10			
							model = 		gr_prop_gr_gunsmithsupl_01a
							vCoords = 		<<1010.9260, -3097.2209, -39.9970>>
							fHeading = 		89.198
							vRotate = 		<<0.0000, 0.0000, 89.1980>>
						BREAK			
						CASE 11			
							model = 		prop_box_wood08a
							vCoords = 		<<1018.3380, -3097.0530, -39.9990>>
							fHeading = 		90.397
							vRotate = 		<<0.0000, 0.0000, 90.3970>>
						BREAK			
						CASE 12			
							model = 		prop_box_wood08a
							vCoords = 		<<1006.1260, -3091.7000, -39.9990>>
							fHeading = 		90.397
							vRotate = 		<<0.0000, 0.0000, 90.3970>>
						BREAK			
						CASE 13			
							model = 		prop_box_wood08a
							vCoords = 		<<1003.5890, -3102.8560, -39.9990>>
							fHeading = 		90.397
							vRotate = 		<<0.0000, 0.0000, 90.3970>>
						BREAK			
						CASE 14			
							model = 		prop_box_wood08a
							vCoords = 		<<1010.7990, -3108.6941, -39.9990>>
							fHeading = 		89.797
							vRotate = 		<<0.0000, 0.0000, 89.7970>>
						BREAK			
						CASE 15			
							model = 		prop_box_wood04a
							vCoords = 		<<1008.2710, -3100.7720, -39.9970>>
							fHeading = 		75.797
							vRotate = 		<<0.0000, 0.0000, 75.7970>>
						BREAK			
						CASE 16			
							model = 		prop_box_wood04a
							vCoords = 		<<998.9270, -3107.0420, -39.9970>>
							fHeading = 		33.396
							vRotate = 		<<0.0000, 0.0000, 33.3960>>
						BREAK			
						CASE 17			
							model = 		prop_box_wood04a
							vCoords = 		<<1016.6970, -3106.4041, -39.9970>>
							fHeading = 		2.996
							vRotate = 		<<0.0000, 0.0000, 2.9960>>
						BREAK			
						CASE 18			
							model = 		prop_box_wood04a
							vCoords = 		<<1010.6660, -3093.7749, -39.9970>>
							fHeading = 		-0.204
							vRotate = 		<<0.0000, 0.0000, -0.2040>>
						BREAK			
						CASE 19			
							model = 		prop_box_wood08a
							vCoords = 		<<1025.9882, -3108.9727, -39.9990>>
							fHeading = 		344.9969
							vRotate = 		<<0.0000, 0.0000, -15.0032>>
						BREAK			
						CASE 20			
							model = 		prop_box_wood08a
							vCoords = 		<<1025.8480, -3096.2908, -39.9990>>
							fHeading = 		359.5965
							vRotate = 		<<0.0000, 0.0000, -0.4035>>
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_EXP2_VARIATION_2
					SWITCH iProp				
						CASE 0			
							model = 		gr_prop_gr_gunsmithsupl_01a
							vCoords = 		<<-1894.5590, -3027.9470, 12.9480>>
							fHeading = 		148.999
							vRotate = 		<<0.0000, 0.0000, 148.9990>>
						BREAK			
						CASE 1			
							model = 		imp_prop_covered_vehicle_03a
							vCoords = 		<<1022.5060, -3106.6321, -39.9980>>
							fHeading = 		349
							vRotate = 		<<0.0000, 0.0000, -11.0000>>
						BREAK			
						CASE 2			
							model = 		imp_prop_covered_vehicle_01a
							vCoords = 		<<1022.1960, -3096.1311, -39.9940>>
							fHeading = 		-172.2
							vRotate = 		<<0.0000, 0.0000, -172.2000>>
						BREAK			
						CASE 3			
							model = 		gr_prop_gr_gunsmithsupl_02a
							vCoords = 		<<1006.1100, -3097.0950, -39.9980>>
							fHeading = 		90.2
							vRotate = 		<<0.0000, 0.0000, 90.2000>>
						BREAK			
						CASE 4			
							model = 		gr_prop_gr_gunsmithsupl_02a
							vCoords = 		<<1015.6040, -3091.8750, -39.9980>>
							fHeading = 		178.999
							vRotate = 		<<0.0000, 0.0000, 178.9990>>
						BREAK			
						CASE 5			
							model = 		gr_prop_gr_gunsmithsupl_02a
							vCoords = 		<<1013.2190, -3103.2649, -39.9980>>
							fHeading = 		90.799
							vRotate = 		<<0.0000, 0.0000, 90.7990>>
						BREAK			
						CASE 6			
							model = 		gr_prop_gr_gunsmithsupl_02a
							vCoords = 		<<993.8560, -3108.9729, -39.9980>>
							fHeading = 		90.599
							vRotate = 		<<0.0000, 0.0000, 90.5990>>
						BREAK			
						CASE 7			
							model = 		gr_prop_gr_gunsmithsupl_01a
							vCoords = 		<<1003.6780, -3108.1021, -39.9970>>
							fHeading = 		90.599
							vRotate = 		<<0.0000, 0.0000, 90.5990>>
						BREAK			
						CASE 8			
							model = 		gr_prop_gr_gunsmithsupl_01a
							vCoords = 		<<1008.5220, -3091.6230, -39.9970>>
							fHeading = 		2.199
							vRotate = 		<<0.0000, 0.0000, 2.1990>>
						BREAK			
						CASE 9			
							model = 		gr_prop_gr_gunsmithsupl_01a
							vCoords = 		<<1015.6500, -3102.5149, -39.9970>>
							fHeading = 		270.799
							vRotate = 		<<0.0000, 0.0000, -89.2010>>
						BREAK			
						CASE 10			
							model = 		gr_prop_gr_gunsmithsupl_01a
							vCoords = 		<<1010.9260, -3097.2209, -39.9970>>
							fHeading = 		89.198
							vRotate = 		<<0.0000, 0.0000, 89.1980>>
						BREAK			
						CASE 11			
							model = 		prop_box_wood08a
							vCoords = 		<<1018.3380, -3097.0530, -39.9990>>
							fHeading = 		90.397
							vRotate = 		<<0.0000, 0.0000, 90.3970>>
						BREAK			
						CASE 12			
							model = 		prop_box_wood08a
							vCoords = 		<<1006.1260, -3091.7000, -39.9990>>
							fHeading = 		90.397
							vRotate = 		<<0.0000, 0.0000, 90.3970>>
						BREAK			
						CASE 13			
							model = 		prop_box_wood08a
							vCoords = 		<<1003.5890, -3102.8560, -39.9990>>
							fHeading = 		90.397
							vRotate = 		<<0.0000, 0.0000, 90.3970>>
						BREAK			
						CASE 14			
							model = 		prop_box_wood08a
							vCoords = 		<<1010.7990, -3108.6941, -39.9990>>
							fHeading = 		89.797
							vRotate = 		<<0.0000, 0.0000, 89.7970>>
						BREAK			
						CASE 15			
							model = 		prop_box_wood04a
							vCoords = 		<<1008.2710, -3100.7720, -39.9970>>
							fHeading = 		75.797
							vRotate = 		<<0.0000, 0.0000, 75.7970>>
						BREAK			
						CASE 16			
							model = 		prop_box_wood04a
							vCoords = 		<<998.9270, -3107.0420, -39.9970>>
							fHeading = 		33.396
							vRotate = 		<<0.0000, 0.0000, 33.3960>>
						BREAK			
						CASE 17			
							model = 		prop_box_wood04a
							vCoords = 		<<1016.6970, -3106.4041, -39.9970>>
							fHeading = 		2.996
							vRotate = 		<<0.0000, 0.0000, 2.9960>>
						BREAK			
						CASE 18			
							model = 		prop_box_wood04a
							vCoords = 		<<1010.6660, -3093.7749, -39.9970>>
							fHeading = 		-0.204
							vRotate = 		<<0.0000, 0.0000, -0.2040>>
						BREAK			
						CASE 19			
							model = 		prop_box_wood08a
							vCoords = 		<<1025.9882, -3108.9727, -39.9990>>
							fHeading = 		344.9969
							vRotate = 		<<0.0000, 0.0000, -15.0032>>
						BREAK			
						CASE 20			
							model = 		prop_box_wood08a
							vCoords = 		<<1025.8480, -3096.2908, -39.9990>>
							fHeading = 		359.5965
							vRotate = 		<<0.0000, 0.0000, -0.4035>>
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_VAULT_LASER_1
			SWITCH eSubvariation
				CASE CHS_VL1_GRAND_SENORA_DESERT
					SWITCH iProp				
						CASE 0			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_air_cargo_04a"))
							vCoords = 		<<1034.1429, 3193.8960, 37.7090>>
							fHeading = 		224.799
							vRotate = 		<<0.0000, 0.0000, -135.2010>>
						BREAK			
						CASE 1			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_air_cargo_04a"))
							vCoords = 		<<1049.0100, 3198.8491, 37.8640>>
							fHeading = 		179.999
							vRotate = 		<<5.1960, 0.7520, 179.9650>>
						BREAK			
						CASE 2			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_cargo_04a"))
							vCoords = 		<<1030.5690, 3200.6841, 37.4360>>
							fHeading = 		101.092
							vRotate = 		<<0.0000, 0.0000, 101.0920>>
						BREAK			
						CASE 3			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_cargo_04a"))
							vCoords = 		<<1025.3990, 3190.3931, 37.6840>>
							fHeading = 		66.892
							vRotate = 		<<0.0000, 0.0000, 66.8920>>
						BREAK			
						CASE 4			
							model = 		xm_prop_x17_trail_01a
							vCoords = 		<<1032.8280, 3212.0620, 37.4460>>
							fHeading = 		11.092
							vRotate = 		<<-3.0000, 0.0000, 11.0920>>
						BREAK			
						CASE 5			
							model = 		sm_prop_smug_jammer
							vCoords = 		<<1039.4720, 3184.5430, 38.1850>>
							fHeading = 		27.199
							vRotate = 		<<0.0000, 0.0000, 27.1990>>
						BREAK			
						CASE 6			
							model = 		sm_prop_smug_jammer
							vCoords = 		<<1045.24, 3184.20, 38.19>>
							fHeading = 		288.199
							vRotate = 		<<0.0000, 0.0000, -71.8010>>
						BREAK			
						CASE 7			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_air_cargo_04a"))
							vCoords = 		<<1023.1060, 3203.6060, 37.2580>>
							fHeading = 		154.599
							vRotate = 		<<0.0000, 0.0000, 154.5990>>
						BREAK			
						CASE 8			
							model = 		gr_prop_gr_rsply_crate02a
							vCoords = 		<<1020.1970, 3188.1499, 38.478>>
							fHeading = 		269.599
							vRotate = 		<<0.0000, -1.6140, -90.4010>>
						BREAK			
						CASE 9			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_crate_01a"))
							vCoords = 		<<1019.2250, 3187.0071, 38.27>>
							fHeading = 		170.399
							vRotate = 		<<0.0000, -0.0000, 170.3990>>
						BREAK			
						CASE 10			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_crate_01a"))
							vCoords = 		<<1046.0680, 3192.2061, 38.70>>
							fHeading = 		344.197
							vRotate = 		<<-2.0000, -2.0000, -15.8030>>
						BREAK			
						CASE 11			
							model = 		gr_prop_gr_rsply_crate02a
							vCoords = 		<<1047.25, 3190.49, 38.87>>
							fHeading = 		332.997
							vRotate = 		<<0.0000, -1.9000, -27.0030>>
						BREAK			
						CASE 12			
							model = 		sm_prop_smug_jammer
							vCoords = 		<<1039.8475, 3245.5918, 36.8734>>
							fHeading = 		102.9988
							vRotate = 		<<0.0000, -0.0000, 102.9987>>
						BREAK			
						CASE 13			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_air_cargo_04a"))
							vCoords = 		<<1024.5322, 3239.0154, 37.1065>>
							fHeading = 		154.599
							vRotate = 		<<0.0000, -0.0000, 154.5990>>
						BREAK			
						CASE 14			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_cargo_04a"))
							vCoords = 		<<1020.0136, 3240.9016, 37.0950>>
							fHeading = 		11.4919
							vRotate = 		<<0.0000, 0.0000, 11.4918>>
						BREAK			
						CASE 15			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_crate_01a"))
							vCoords = 		<<1022.4215, 3240.4053, 37.1491>>
							fHeading = 		335.2918
							vRotate = 		<<0.0000, 0.0000, -24.7082>>
						BREAK			
						CASE 16			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_crate_01a"))
							vCoords = 		<<1015.1219, 3235.8562, 37.1531>>
							fHeading = 		34.8918
							vRotate = 		<<0.0000, 0.0000, 34.8918>>
						BREAK			
						CASE 17			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_cargo_04a"))
							vCoords = 		<<1016.4434, 3232.2893, 37.1000>>
							fHeading = 		341.4919
							vRotate = 		<<2.2870, 1.4320, -18.5082>>
						BREAK			
						CASE 18			
							model = 		prop_worklight_03b
							vCoords = 		<<1025.4713, 3207.8110, 37.2104>>
							fHeading = 		35.9996
							vRotate = 		<<0.0000, 0.0000, 35.9996>>
						BREAK			
						CASE 19			
							model = 		prop_worklight_03b
							vCoords = 		<<1032.3398, 3181.4011, 37.9898>>
							fHeading = 		186.7991
							vRotate = 		<<0.0000, -0.0000, -173.2009>>
						BREAK			
						CASE 20			
							model = 		prop_worklight_03b
							vCoords = 		<<1045.2581, 3206.2732, 37.3863>>
							fHeading = 		310.3991
							vRotate = 		<<0.0000, 0.0000, -49.6009>>
						BREAK			
						CASE 21			
							model = 		prop_worklight_03b
							vCoords = 		<<1021.3463, 3232.5308, 37.1220>>
							fHeading = 		185.5986
							vRotate = 		<<0.0000, -0.0000, -174.4013>>
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_VL1_LA_MESA
					SWITCH iProp				
						CASE 0			
							model = 		xm_prop_x17_trail_01a
							vCoords = 		<<690.4220, -1497.5530, 9.1970>>
							fHeading = 		279.4
							vRotate = 		<<0.0000, 0.0000, -80.6000>>
						BREAK			
						CASE 1			
							model = 		sm_prop_smug_jammer
							vCoords = 		<<676.7380, -1484.5341, 8.7290>>
							fHeading = 		197.2
							vRotate = 		<<0.0000, 0.0000, -162.8000>>
						BREAK			
						CASE 2			
							model = 		sm_prop_smug_jammer
							vCoords = 		<<675.8900, -1514.4990, 8.7090>>
							fHeading = 		287.2
							vRotate = 		<<0.0000, 0.0000, -72.8000>>
						BREAK			
						CASE 3			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_air_cargo_04a"))
							vCoords = 		<<686.9140, -1489.6000, 8.7190>>
							fHeading = 		237.4
							vRotate = 		<<0.0000, 0.0000, -122.6000>>
						BREAK			
						CASE 4			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_air_cargo_04a"))
							vCoords = 		<<676.8680, -1503.0200, 8.6990>>
							fHeading = 		196.999
							vRotate = 		<<0.0000, 0.0000, -163.0000>>
						BREAK			
						CASE 5			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_air_cargo_04a"))
							vCoords = 		<<686.9680, -1508.4890, 8.6990>>
							fHeading = 		40.199
							vRotate = 		<<0.0000, 0.0000, 40.1990>>
						BREAK			
						CASE 6			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_cargo_04a"))
							vCoords = 		<<681.1100, -1498.5439, 8.7120>>
							fHeading = 		174
							vRotate = 		<<0.0000, 0.0000, 174.0000>>
						BREAK			
						CASE 7			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_cargo_04a"))
							vCoords = 		<<665.9890, -1495.3960, 9.6840>>
							fHeading = 		68.399
							vRotate = 		<<0.0000, 0.0000, 68.3990>>
						BREAK			
						CASE 8			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_crate_01a"))
							vCoords = 		<<657.1230, -1501.1670, 10.3617>>
							fHeading = 		286.568
							vRotate = 		<<0.0000, 0.0000, -73.4320>>
						BREAK			
						CASE 9			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_crate_01a"))
							vCoords = 		<<684.2910, -1513.9000, 9.3450>>
							fHeading = 		127.997
							vRotate = 		<<0.0000, 0.0000, 127.9970>>
						BREAK			
						CASE 10			
							model = 		gr_prop_gr_rsply_crate02a
							vCoords = 		<<686.3149, -1514.4180, 9.5768>>
							fHeading = 		300.596
							vRotate = 		<<0.0000, 0.0000, -59.4040>>
						BREAK			
						CASE 11			
							model = 		gr_prop_gr_rsply_crate02a
							vCoords = 		<<658.8230, -1501.3580, 10.5300>>
							fHeading = 		201.796
							vRotate = 		<<0.0000, 0.0000, -158.2040>>
						BREAK		
						CASE 12			
							model = 		sm_prop_smug_jammer
							vCoords = 		<<1243.6533, -2403.2747, 46.6022>>
							fHeading = 		341.5998
							vRotate = 		<<5.4469, -2.4867, -18.2819>>
						BREAK			
						CASE 13			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_cargo_04a"))
							vCoords = 		<<1251.0326, -2393.7654, 47.2083>>
							fHeading = 		276.1308
							vRotate = 		<<0.0000, 1.1000, -83.8692>>
						BREAK			
						CASE 14			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_cargo_04a"))
							vCoords = 		<<1260.9666, -2324.6182, 49.7471>>
							fHeading = 		-83.8692
							vRotate = 		<<0.0000, 0.0000, -83.8692>>
						BREAK			
						CASE 15			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_air_cargo_04a"))
							vCoords = 		<<1263.3954, -2327.1541, 49.8386>>
							fHeading = 		74.275
							vRotate = 		<<0.0000, 0.0000, 74.2749>>
						BREAK			
						CASE 16			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_crate_01a"))
							vCoords = 		<<1249.0850, -2396.2512, 47.0510>>
							fHeading = 		201.675
							vRotate = 		<<0.0340, 2.1130, -158.3250>>
						BREAK			
						CASE 17			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_crate_01a"))
							vCoords = 		<<1260.4979, -2327.4067, 49.7671>>
							fHeading = 		307.275
							vRotate = 		<<0.0000, 0.0000, -52.7250>>
						BREAK			
						CASE 18			
							model = 		prop_worklight_03b
							vCoords = 		<<1239.0341, -2350.1541, 49.0940>>
							fHeading = 		12.4
							vRotate = 		<<0.0000, 0.0000, 12.4000>>
						BREAK			
						CASE 19			
							model = 		prop_worklight_03b
							vCoords = 		<<1227.0222, -2374.9390, 48.3069>>
							fHeading = 		107.3995
							vRotate = 		<<-2.3059, -8.1679, 107.2349>>
						BREAK			
						CASE 20			
							model = 		prop_worklight_03b
							vCoords = 		<<1256.8875, -2380.9346, 48.2057>>
							fHeading = 		257.4345
							vRotate = 		<<5.1038, -1.0566, -102.5184>>
						BREAK			
						CASE 21			
							model = 		prop_worklight_03b
							vCoords = 		<<1259.7632, -2360.3511, 49.1940>>
							fHeading = 		282.8813
							vRotate = 		<<4.6510, 0.8525, -77.1534>>
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_VAULT_LASER_2
			SWITCH eSubvariation
				CASE CHS_VL2_POWER_STATION
					SWITCH iProp				
						CASE 0			
							model = 		prop_conc_sacks_02a
							vCoords = 		<<2836.9460, 1464.7600, 23.5550>>
							fHeading = 		211.399
							vRotate = 		<<0.0000, 0.0000, -148.6010>>
						BREAK			
						CASE 1			
							model = 		prop_conc_sacks_02a
							vCoords = 		<<2840.3069, 1465.7061, 23.5570>>
							fHeading = 		187.399
							vRotate = 		<<0.0000, 0.0000, -172.6010>>
						BREAK			
						CASE 2			
							model = 		gr_prop_gr_gunsmithsupl_03a
							vCoords = 		<<2845.5300, 1460.9510, 23.5700>>
							fHeading = 		165.797
							vRotate = 		<<0.0000, 0.0000, 165.7970>>
						BREAK			
						CASE 3			
							model = 		prop_boxpile_05a
							vCoords = 		<<2848.9341, 1463.3210, 23.5550>>
							fHeading = 		271.397
							vRotate = 		<<0.0000, 0.0000, -88.6030>>
						BREAK			
						CASE 4			
							model = 		prop_flattruck_01c
							vCoords = 		<<2841.8330, 1460.0590, 23.7360>>
							fHeading = 		326.946
							vRotate = 		<<0.0000, 0.0000, -33.0540>>
						BREAK			
						CASE 5			
							model = 		prop_box_ammo05b
							vCoords = 		<<1090.3320, -3195.7500, -39.1920>>
							fHeading = 		187.999
							vRotate = 		<<0.0000, 0.0000, -172.0000>>
						BREAK			
						CASE 6			
							model = 		gr_prop_gr_gunsmithsupl_01a
							vCoords = 		<<1100.8260, -3197.0061, -39.9900>>
							fHeading = 		271.199
							vRotate = 		<<0.0000, 0.0000, -88.8010>>
						BREAK			
						CASE 7			
							model = 		gr_prop_gr_gunsmithsupl_03a
							vCoords = 		<<1097.9050, -3198.7561, -39.9910>>
							fHeading = 		179.199
							vRotate = 		<<0.0000, 0.0000, 179.1990>>
						BREAK			
						CASE 8			
							model = 		gr_prop_gr_speeddrill_01a
							vCoords = 		<<1091.8890, -3192.8159, -39.9930>>
							fHeading = 		359.799
							vRotate = 		<<0.0000, 0.0000, -0.2010>>
						BREAK			
						CASE 9			
							model = 		gr_prop_gr_speeddrill_01a
							vCoords = 		<<1086.8230, -3192.0640, -39.9930>>
							fHeading = 		89.199
							vRotate = 		<<0.0000, 0.0000, 89.1990>>
						BREAK			
						CASE 10			
							model = 		prop_boxpile_05a
							vCoords = 		<<1100.4230, -3194.6580, -39.9930>>
							fHeading = 		273.799
							vRotate = 		<<0.0000, 0.0000, -86.2010>>
						BREAK		
						CASE 11			
							model = 		gr_prop_gr_gunsmithsupl_01a
							vCoords = 		<<1093.9873, -3193.8777, -39.9901>>
							fHeading = 		180.1972
							vRotate = 		<<0.0000, -0.0000, -179.8027>>
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_VL2_ZANCUDO
					SWITCH iProp				
						CASE 0			
							model = 		prop_conc_sacks_02a
							vCoords = 		<<-2015.9709, 3401.2141, 30.1550>>
							fHeading = 		167.6
							vRotate = 		<<0.0000, 0.0000, 167.6000>>
						BREAK			
						CASE 1			
							model = 		prop_conc_sacks_02a
							vCoords = 		<<-2039.0750, 3382.2051, 30.2980>>
							fHeading = 		279.8
							vRotate = 		<<0.0000, 0.0000, -80.2000>>
						BREAK			
						CASE 2			
							model = 		gr_prop_gr_gunsmithsupl_03a
							vCoords = 		<<-2023.1219, 3381.7380, 30.1800>>
							fHeading = 		174.199
							vRotate = 		<<0.0000, 0.0000, 174.1990>>
						BREAK			
						CASE 3			
							model = 		prop_boxpile_05a
							vCoords = 		<<-2024.7220, 3389.0020, 30.1380>>
							fHeading = 		60.799
							vRotate = 		<<0.0000, 0.0000, 60.7990>>
						BREAK			
						CASE 4			
							model = 		prop_flattruck_01c
							vCoords = 		<<-2008.9760, 3381.5471, 30.2640>>
							fHeading = 		346.399
							vRotate = 		<<4.2620, -3.4430, -13.4730>>
						BREAK			
						CASE 5			
							model = 		prop_box_ammo05b
							vCoords = 		<<1090.3320, -3195.7500, -39.1920>>
							fHeading = 		187.999
							vRotate = 		<<0.0000, 0.0000, -172.0000>>
						BREAK			
						CASE 6			
							model = 		gr_prop_gr_gunsmithsupl_01a
							vCoords = 		<<1100.8260, -3197.0061, -39.9900>>
							fHeading = 		271.199
							vRotate = 		<<0.0000, 0.0000, -88.8010>>
						BREAK			
						CASE 7			
							model = 		gr_prop_gr_gunsmithsupl_03a
							vCoords = 		<<1097.9050, -3198.7561, -39.9910>>
							fHeading = 		179.199
							vRotate = 		<<0.0000, 0.0000, 179.1990>>
						BREAK			
						CASE 8			
							model = 		gr_prop_gr_speeddrill_01a
							vCoords = 		<<1091.8890, -3192.8159, -39.9930>>
							fHeading = 		359.799
							vRotate = 		<<0.0000, 0.0000, -0.2010>>
						BREAK			
						CASE 9			
							model = 		gr_prop_gr_speeddrill_01a
							vCoords = 		<<1086.8230, -3192.0640, -39.9930>>
							fHeading = 		89.199
							vRotate = 		<<0.0000, 0.0000, 89.1990>>
						BREAK			
						CASE 10			
							model = 		prop_boxpile_05a
							vCoords = 		<<1100.4230, -3194.6580, -39.9930>>
							fHeading = 		273.799
							vRotate = 		<<0.0000, 0.0000, -86.2010>>
						BREAK			
						CASE 11			
							model = 		gr_prop_gr_gunsmithsupl_01a
							vCoords = 		<<1093.9873, -3193.8777, -39.9901>>
							fHeading = 		180.1972
							vRotate = 		<<0.0000, -0.0000, -179.8027>>
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_FIREFIGHTER_OUTFIT_2
			SWITCH eSubvariation
				CASE CHS_FF2_VARIATION_1
					SWITCH iProp				
						CASE 0			
							model = 		prop_barrier_work05
							vCoords = 		<<1439.2480, 2794.2710, 51.5110>>
							fHeading = 		100.999
							vRotate = 		<<0.0000, 0.0000, 100.9990>>
						BREAK			
						CASE 1			
							model = 		prop_barrier_work05
							vCoords = 		<<1432.7200, 2801.8301, 51.4650>>
							fHeading = 		177.399
							vRotate = 		<<-0.0770, 3.9890, 177.4010>>
						BREAK			
					ENDSWITCH
				BREAK
				CASE CHS_FF2_VARIATION_2
					SWITCH iProp				
						CASE 0			
							model = 		prop_barrier_work05
							vCoords = 		<<-786.9750, 589.1830, 125.8660>>
							fHeading = 		212.4
							vRotate = 		<<-2.1200, 6.3970, -147.4810>>
						BREAK			
						CASE 1			
							model = 		prop_barrier_work05
							vCoords = 		<<-784.3890, 572.3560, 125.1020>>
							fHeading = 		157.319
							vRotate = 		<<-2.2310, -2.9880, 157.2610>>
						BREAK			
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_STEALTH_OUTFITS
			SWITCH eSubvariation
				CASE CHS_SO_VARIATION_1
					SWITCH iProp				
						CASE 0			
							model = 		prop_container_ld_pu
							vCoords = 		<<3575.1030, 3673.9839, 32.8870>>
							fHeading = 		350
							vRotate = 		<<0.0000, 0.0000, -10.0000>>
						BREAK			
						CASE 1			
							model = 		prop_container_ld_pu
							vCoords = 		<<3561.9871, 3655.2729, 32.8870>>
							fHeading = 		259.4
							vRotate = 		<<0.0000, 0.0000, -100.6000>>
						BREAK			
						CASE 2			
							model = 		gr_prop_gr_gunsmithsupl_03a
							vCoords = 		<<3600.3259, 3672.0520, 32.8740>>
							fHeading = 		348.599
							vRotate = 		<<0.0000, 0.0000, -11.4010>>
						BREAK			
						CASE 3			
							model = 		gr_prop_gr_crates_rifles_04a
							vCoords = 		<<3602.8201, 3667.7419, 32.8720>>
							fHeading = 		169.4
							vRotate = 		<<0.0000, 0.0000, 169.4000>>
						BREAK			
						CASE 4			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_cargo_04a"))
							vCoords = 		<<3578.2271, 3659.1990, 32.8990>>
							fHeading = 		230.599
							vRotate = 		<<0.0000, 0.0000, -129.4010>>
						BREAK			
						CASE 5			
							model = 		prop_dog_cage_01
							vCoords = 		<<3578.5330, 3661.2839, 33.4340>>
							fHeading = 		269.399
							vRotate = 		<<0.0000, 0.0000, -90.6010>>
						BREAK			
						CASE 6			
							model = 		prop_dog_cage_01
							vCoords = 		<<3565.2051, 3654.1260, 33.4260>>
							fHeading = 		135.399
							vRotate = 		<<0.0000, 0.0000, 135.3990>>
						BREAK			
						CASE 7			
							model = 		prop_boxpile_07d
							vCoords = 		<<3601.4370, 3663.5569, 32.8730>>
							fHeading = 		168.399
							vRotate = 		<<0.0000, 0.0000, 168.3990>>
						BREAK			
						CASE 8			
							model = 		prop_box_wood02a_mws
							vCoords = 		<<3576.0979, 3658.4351, 32.8980>>
							fHeading = 		303.398
							vRotate = 		<<0.0000, 0.0000, -56.6020>>
						BREAK			
						CASE 9			
							model = 		prop_flattruck_01b
							vCoords = 		<<3586.3330, 3669.9570, 32.8880>>
							fHeading = 		319.599
							vRotate = 		<<0.0000, 0.0000, -40.4010>>
						BREAK			
						CASE 10			
							model = 		prop_boxpile_07d
							vCoords = 		<<3592.2029, 3668.8970, 32.8730>>
							fHeading = 		170.799
							vRotate = 		<<0.0000, 0.0000, 170.7990>>
						BREAK			
						CASE 11			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_cargo_04a"))
							vCoords = 		<<3598.1589, 3667.7649, 32.8720>>
							fHeading = 		209.599
							vRotate = 		<<0.0000, 0.0000, -150.4010>>
						BREAK			
						CASE 12			
							model = 		prop_box_wood02a_mws
							vCoords = 		<<3596.4871, 3673.0371, 32.8720>>
							fHeading = 		348.398
							vRotate = 		<<0.0000, 0.0000, -11.6020>>
						BREAK			
						CASE 13			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_cargo_04a"))
							vCoords = 		<<3589.2600, 3660.2720, 32.8720>>
							fHeading = 		344.799
							vRotate = 		<<0.0000, 0.0000, -15.2010>>
						BREAK			
						CASE 14			
							model = 		prop_flattruck_01b
							vCoords = 		<<3599.0200, 3659.8181, 32.8720>>
							fHeading = 		209.199
							vRotate = 		<<0.0000, 0.0000, -150.8010>>
						BREAK			
						CASE 15			
							model = 		hei_prop_hei_securitypanel
							vCoords = 		<< 3588.147, 3667.437, 34.265 >>
							fHeading = 		-99.928
							vRotate = 		<<0.0000, 0.0000, -99.9280>>
						BREAK			
					ENDSWITCH				
				BREAK
				
				CASE CHS_SO_VARIATION_2
					SWITCH iProp				
						CASE 0			
							model = 		prop_container_ld_pu
							vCoords = 		<<-1777.3530, 3103.2339, 31.8020>>
							fHeading = 		59.8
							vRotate = 		<<0.0000, 0.0000, 59.8000>>
						BREAK			
						CASE 1			
							model = 		prop_container_ld_pu
							vCoords = 		<<-1780.5660, 3079.0090, 31.8040>>
							fHeading = 		149.799
							vRotate = 		<<0.0000, 0.0000, 149.7990>>
						BREAK			
						CASE 2			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_cargo_04a"))
							vCoords = 		<<-1788.3440, 3086.1440, 31.8060>>
							fHeading = 		328.799
							vRotate = 		<<0.0000, 0.0000, -31.2010>>
						BREAK			
						CASE 3			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_cargo_04a"))
							vCoords = 		<<-1780.8149, 3091.9050, 31.8000>>
							fHeading = 		21.199
							vRotate = 		<<0.0000, 0.0000, 21.1990>>
						BREAK			
						CASE 4			
							model = 		gr_prop_gr_crates_rifles_02a
							vCoords = 		<<-1811.1210, 3104.0520, 31.8420>>
							fHeading = 		331.198
							vRotate = 		<<0.0000, 0.0000, -28.8020>>
						BREAK			
						CASE 5			
							model = 		gr_prop_gr_gunsmithsupl_03a
							vCoords = 		<<-1812.2159, 3102.0000, 31.8440>>
							fHeading = 		60.598
							vRotate = 		<<0.0000, 0.0000, 60.5980>>
						BREAK			
						CASE 6			
							model = 		prop_box_wood02a_mws
							vCoords = 		<<-1813.7690, 3093.7739, 31.8420>>
							fHeading = 		150.598
							vRotate = 		<<0.0000, 0.0000, 150.5980>>
						BREAK			
						CASE 7			
							model = 		prop_box_wood02a_mws
							vCoords = 		<<-1802.0690, 3105.4080, 31.8420>>
							fHeading = 		343.598
							vRotate = 		<<0.0000, 0.0000, -16.4020>>
						BREAK			
						CASE 8			
							model = 		prop_boxpile_07d
							vCoords = 		<<-1800.2830, 3103.7380, 31.8430>>
							fHeading = 		240.598
							vRotate = 		<<0.0000, 0.0000, -119.4020>>
						BREAK			
						CASE 9			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_cargo_04a"))
							vCoords = 		<<-1796.2310, 3097.7749, 31.8420>>
							fHeading = 		323.599
							vRotate = 		<<0.0000, 0.0000, -36.4010>>
						BREAK			
						CASE 10			
							model = 		prop_boxpile_07d
							vCoords = 		<<-1768.5800, 3086.1021, 31.8110>>
							fHeading = 		239.598
							vRotate = 		<<0.0000, 0.0000, -120.4020>>
						BREAK			
						CASE 11			
							model = 		prop_flattruck_01b
							vCoords = 		<<-1766.0490, 3087.2380, 31.8120>>
							fHeading = 		358.598
							vRotate = 		<<0.0000, 0.0000, -1.4020>>
						BREAK			
						CASE 12			
							model = 		prop_flattruck_01b
							vCoords = 		<<-1783.9270, 3099.1489, 31.8080>>
							fHeading = 		106.598
							vRotate = 		<<0.0000, 0.0000, 106.5980>>
						BREAK		
						CASE 13			
							model = 		hei_prop_hei_securitypanel
							vCoords = 		<<-1790.9550, 3094.1238, 33.3360>>
							fHeading = 		60.085
							vRotate = 		<<0.0000, 0.0000, 60.0850>>
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_PLASTIC_EXPLOSIVES
			SWITCH eSubvariation
				CASE CHS_PE_VARIATION_1
					SWITCH iProp				
						CASE 0			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("apa_mp_apa_crashed_usaf_01a"))
							vCoords = 		<<4106.0161, 4718.0161, -92.5930>>
							fHeading = 		359.996
							vRotate = 		<<-13.4570, 0.0000, -0.0040>>
						BREAK			
						CASE 1			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("xm_int_lev_scuba_gear"))
							vCoords = 		<<3817.9780, 4470.0532, 4.3240>>
							fHeading = 		313.444
							vRotate = 		<<0.0000, 0.0000, -46.5560>>
						BREAK
						CASE 2			
							model = 		xm_prop_x17_scuba_tank
							vCoords = 		<<3817.3059, 4469.6660, 2.4540>>
							fHeading = 		45.599
							vRotate = 		<<-84.1000, -180.0000, 45.5990>>
						BREAK			
						CASE 3			
							model = 		xm_prop_x17_scuba_tank
							vCoords = 		<<3817.3799, 4469.9429, 2.2570>>
							fHeading = 		311.599
							vRotate = 		<<0.0000, 0.0000, -48.4010>>
						BREAK			
						CASE 4			
							model = 		prop_crate_01a
							vCoords = 		<<3817.4131, 4469.5762, 2.3100>>
							fHeading = 		224.999
							vRotate = 		<<0.0000, -5.2200, -135.0010>>
						BREAK			
						CASE 5			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_crate_01a"))
							vCoords = 		<<4106.9658, 4707.6968, -93.9164>>
							fHeading = 		288.5996
							vRotate = 		<<-10.8000, -33.1420, -71.4003>>
						BREAK			
						CASE 6			
							model = 		gr_prop_gr_adv_case
							vCoords = 		<<4104.7358, 4707.6436, -93.5604>>
							fHeading = 		-60
							vRotate = 		<<-30.2940, -0.0000, -60.0000>>
						BREAK			
						CASE 7			
							model = 		gr_prop_gr_adv_case
							vCoords = 		<<4115.8608, 4726.1987, -96.9029>>
							fHeading = 		-60
							vRotate = 		<<0.0000, -35.8232, -60.0000>>
						BREAK			
						CASE 8			
							model = 		gr_prop_gr_adv_case
							vCoords = 		<<4096.2695, 4721.5791, -86.0653>>
							fHeading = 		-60
							vRotate = 		<<0.0000, -9.2869, -60.0000>>
						BREAK			
						CASE 9			
							model = 		prop_worklight_02a
							vCoords = 		<<4105.9385, 4707.6582, -93.9366>>
							fHeading = 		187.9991
							vRotate = 		<<-40.5790, -0.0000, -172.0009>>
						BREAK			
						CASE 10			
							model = 		prop_worklight_02a
							vCoords = 		<<4114.8364, 4727.1787, -97.2058>>
							fHeading = 		300.1992
							vRotate = 		<<-18.0890, -0.0000, -59.8008>>
						BREAK			
						CASE 11			
							model = 		prop_worklight_02a
							vCoords = 		<<4102.2852, 4728.0308, -93.3896>>
							fHeading = 		33.199
							vRotate = 		<<-0.0000, 31.6090, 33.1990>>
						BREAK
					ENDSWITCH
				BREAK
				CASE CHS_PE_VARIATION_2
					SWITCH iProp				
						CASE 0			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("xm_int_lev_scuba_gear"))
							vCoords = 		<<-1597.5010, 5205.3809, 4.1340>>
							fHeading = 		203.398
							vRotate = 		<<0.0000, 0.0000, -156.6020>>
						BREAK			
						CASE 1			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("apa_mp_apa_crashed_usaf_01a"))
							vCoords = 		<<-1835.3459, 5661.0122, -69.8810>>
							fHeading = 		325.654
							vRotate = 		<<-7.8770, 0.0000, -34.3460>>
						BREAK
						CASE 2			
							model = 		prop_crate_01a
							vCoords = 		<<-1592.4083, 5201.5762, 3.2853>>
							fHeading = 		24.1977
							vRotate = 		<<0.0000, 0.0000, 24.1977>>
						BREAK			
						CASE 3			
							model = 		xm_prop_x17_scuba_tank
							vCoords = 		<<-1592.5117, 5201.7017, 3.4000>>
							fHeading = 		205.9975
							vRotate = 		<<-85.3090, -180.0000, 25.9975>>
						BREAK			
						CASE 4			
							model = 		xm_prop_x17_scuba_tank
							vCoords = 		<<-1592.4320, 5201.9863, 3.2372>>
							fHeading = 		302.7471
							vRotate = 		<<-10.7710, -0.0000, -57.2529>>
						BREAK			
						CASE 5			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_crate_01a"))
							vCoords = 		<<-1838.2749, 5652.6597, -70.3794>>
							fHeading = 		258.598
							vRotate = 		<<1.4000, -42.1000, -101.4020>>
						BREAK			
						CASE 6			
							model = 		gr_prop_gr_adv_case
							vCoords = 		<<-1840.1556, 5654.5322, -70.8820>>
							fHeading = 		251.598
							vRotate = 		<<-44.0001, -7.2000, -108.4020>>
						BREAK			
						CASE 7			
							model = 		gr_prop_gr_adv_case
							vCoords = 		<<-1827.7435, 5656.2100, -69.2056>>
							fHeading = 		250.498
							vRotate = 		<<0.0000, -0.0000, -109.5020>>
						BREAK			
						CASE 8			
							model = 		gr_prop_gr_adv_case
							vCoords = 		<<-1851.5787, 5661.2656, -63.8449>>
							fHeading = 		250.498
							vRotate = 		<<-12.7001, 21.0000, -109.5020>>
						BREAK			
						CASE 9			
							model = 		prop_worklight_02a
							vCoords = 		<<-1839.3376, 5653.2017, -70.6862>>
							fHeading = 		160.1998
							vRotate = 		<<-6.3004, 6.8000, 160.1997>>
						BREAK			
						CASE 10			
							model = 		prop_worklight_02a
							vCoords = 		<<-1827.3058, 5657.4326, -69.5261>>
							fHeading = 		267.7997
							vRotate = 		<<8.4000, -0.0000, -92.2003>>
						BREAK			
						CASE 11			
							model = 		prop_worklight_02a
							vCoords = 		<<-1825.2163, 5666.3398, -69.8777>>
							fHeading = 		291.7996
							vRotate = 		<<18.2000, -9.3000, -68.2004>>
						BREAK
					ENDSWITCH
				BREAK
				CASE CHS_PE_VARIATION_3
					SWITCH iProp				
						CASE 0			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("xm_int_lev_scuba_gear"))
							vCoords = 		<<118.0040, -2266.1270, 6.1850>>
							fHeading = 		178.598
							vRotate = 		<<0.0000, 0.0000, 178.5980>>
						BREAK			
						CASE 1			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("apa_mp_apa_crashed_usaf_01a"))
							vCoords = 		<<-93.3590, -2310.3000, -24.5930>>
							fHeading = 		80.446
							vRotate = 		<<-7.1620, 0.0000, 80.4460>>
						BREAK
						CASE 2			
							model = 		prop_crate_01a
							vCoords = 		<<117.8038, -2265.8936, 5.0869>>
							fHeading = 		269.9998
							vRotate = 		<<0.0000, -0.0000, -90.0002>>
						BREAK			
						CASE 3			
							model = 		xm_prop_x17_scuba_tank
							vCoords = 		<<117.6442, -2265.9155, 5.1687>>
							fHeading = 		269.9998
							vRotate = 		<<-89.1998, -180.0000, 89.9998>>
						BREAK			
						CASE 4			
							model = 		xm_prop_x17_scuba_tank
							vCoords = 		<<117.4720, -2265.7666, 5.0315>>
							fHeading = 		1.3992
							vRotate = 		<<-4.8003, -0.0000, 1.3992>>
						BREAK			
						CASE 5			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_crate_01a"))
							vCoords = 		<<-81.0217, -2312.6238, -21.9018>>
							fHeading = 		45.4989
							vRotate = 		<<-15.5000, -39.3999, 45.4989>>
						BREAK			
						CASE 6			
							model = 		gr_prop_gr_adv_case
							vCoords = 		<<-82.4597, -2314.2327, -22.2320>>
							fHeading = 		148.4985
							vRotate = 		<<-1.0000, 25.5000, 148.4985>>
						BREAK			
						CASE 7			
							model = 		gr_prop_gr_adv_case
							vCoords = 		<<-95.8997, -2316.0627, -23.6876>>
							fHeading = 		148.4985
							vRotate = 		<<16.9000, 13.0000, 148.4985>>
						BREAK			
						CASE 8			
							model = 		gr_prop_gr_adv_case
							vCoords = 		<<-96.6712, -2301.7498, -23.4082>>
							fHeading = 		186.4983
							vRotate = 		<<26.0000, -0.0000, -173.5017>>
						BREAK			
						CASE 9			
							model = 		prop_worklight_02a
							vCoords = 		<<-82.1312, -2313.1526, -22.5797>>
							fHeading = 		249.0979
							vRotate = 		<<0.0000, -0.0000, -110.9022>>
						BREAK			
						CASE 10			
							model = 		prop_worklight_02a
							vCoords = 		<<-97.1345, -2316.5071, -23.8219>>
							fHeading = 		184.8979
							vRotate = 		<<27.2000, -0.0000, -175.1021>>
						BREAK			
						CASE 11			
							model = 		prop_worklight_02a
							vCoords = 		<<-92.3670, -2303.9541, -24.0447>>
							fHeading = 		346.098
							vRotate = 		<<13.7000, -0.0000, -13.9020>>
						BREAK
					ENDSWITCH
				BREAK
				CASE CHS_PE_VARIATION_4
					SWITCH iProp				
						CASE 0			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("apa_mp_apa_crashed_usaf_01a"))
							vCoords = 		<<351.6090, 3950.9231, -8.6970>>
							fHeading = 		68.997
							vRotate = 		<<-0.4130, 0.0000, 68.9970>>
						BREAK			
						CASE 1			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("xm_int_lev_scuba_gear"))
							vCoords = 		<<715.0720, 4098.1035, 36.1887>>
							fHeading = 		268.5995
							vRotate = 		<<0.0000, -0.0000, -91.4005>>
						BREAK
						CASE 2			
							model = 		prop_crate_01a
							vCoords = 		<<714.7887, 4098.0781, 34.7905>>
							fHeading = 		-0.2
							vRotate = 		<<0.0000, 0.0000, -0.2000>>
						BREAK			
						CASE 3			
							model = 		xm_prop_x17_scuba_tank
							vCoords = 		<<714.8036, 4097.9185, 34.9063>>
							fHeading = 		359.8
							vRotate = 		<<-85.4997, -180.0000, 179.8000>>
						BREAK			
						CASE 4			
							model = 		xm_prop_x17_scuba_tank
							vCoords = 		<<714.6030, 4097.7300, 34.7423>>
							fHeading = 		93.7996
							vRotate = 		<<-6.2001, -0.0000, 93.7996>>
						BREAK			
						CASE 5			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_crate_01a"))
							vCoords = 		<<363.2671, 3946.2549, -9.5563>>
							fHeading = 		36.4978
							vRotate = 		<<0.0000, -37.6000, 36.4978>>
						BREAK			
						CASE 6			
							model = 		gr_prop_gr_adv_case
							vCoords = 		<<362.9233, 3948.4841, -9.1797>>
							fHeading = 		67.2976
							vRotate = 		<<-14.0000, -0.0000, 67.2976>>
						BREAK			
						CASE 7			
							model = 		gr_prop_gr_adv_case
							vCoords = 		<<338.4879, 3946.2065, -8.8015>>
							fHeading = 		43.2976
							vRotate = 		<<0.0000, -17.1000, 43.2976>>
						BREAK			
						CASE 8			
							model = 		gr_prop_gr_adv_case
							vCoords = 		<<355.3201, 3959.8325, -8.7153>>
							fHeading = 		130.2974
							vRotate = 		<<0.0000, -0.0000, 130.2974>>
						BREAK			
						CASE 9			
							model = 		prop_worklight_02a
							vCoords = 		<<362.6080, 3947.5840, -9.4082>>
							fHeading = 		255.6973
							vRotate = 		<<0.0000, -0.0000, -104.3027>>
						BREAK			
						CASE 10			
							model = 		prop_worklight_02a
							vCoords = 		<<353.7045, 3959.9624, -9.0119>>
							fHeading = 		334.6971
							vRotate = 		<<0.0000, 0.0000, -25.3029>>
						BREAK			
						CASE 11			
							model = 		prop_worklight_02a
							vCoords = 		<<337.6557, 3947.6443, -9.1872>>
							fHeading = 		115.6969
							vRotate = 		<<4.5000, 7.3000, 115.6969>>
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_ELECTRIC_DRILLS_1
			SWITCH eSubvariation
				CASE CHS_ED1_VARIATION_1
					SWITCH iProp				
						CASE 0			
							model = 		prop_conc_blocks01a
							vCoords = 		<<-175.9010, -1065.6560, 41.1440>>
							fHeading = 		249.4
							vRotate = 		<<0.0000, 0.0000, -110.6000>>
						BREAK			
						CASE 1			
							model = 		prop_conc_blocks01a
							vCoords = 		<<-166.4910, -1084.3630, 41.1440>>
							fHeading = 		160
							vRotate = 		<<0.0000, 0.0000, 160.0000>>
						BREAK			
						CASE 2			
							model = 		prop_cementbags01
							vCoords = 		<<-161.1890, -1068.6270, 41.1400>>
							fHeading = 		104.799
							vRotate = 		<<0.0000, 0.0000, 104.7990>>
						BREAK			
						CASE 3			
							model = 		prop_conc_blocks01c
							vCoords = 		<<-175.7960, -1077.5890, 41.1390>>
							fHeading = 		88.599
							vRotate = 		<<0.0000, 0.0000, 88.5990>>
						BREAK			
						CASE 4			
							model = 		prop_pallettruck_01
							vCoords = 		<<-159.1260, -1070.3750, 41.1410>>
							fHeading = 		301.799
							vRotate = 		<<0.0000, 0.0000, -58.2010>>
						BREAK			
						CASE 5			
							model = 		prop_woodpile_01c
							vCoords = 		<<-160.0990, -1078.2321, 41.1450>>
							fHeading = 		272.599
							vRotate = 		<<0.0000, 0.0000, -87.4010>>
						BREAK			
						CASE 6			
							model = 		prop_woodpile_01c
							vCoords = 		<<-157.4840, -1080.1121, 41.1450>>
							fHeading = 		313.198
							vRotate = 		<<0.0000, 0.0000, -46.8020>>
						BREAK			
						CASE 7			
							model = 		prop_generator_03b
							vCoords = 		<<-155.4530, -1073.6331, 41.1990>>
							fHeading = 		105.598
							vRotate = 		<<0.0000, 0.0000, 105.5980>>
						BREAK			
						CASE 8			
							model = 		prop_conc_blocks01c
							vCoords = 		<<-163.9830, -1076.4480, 41.1390>>
							fHeading = 		135.398
							vRotate = 		<<0.0000, 0.0000, 135.3980>>
						BREAK			
						CASE 9			
							model = 		prop_cementbags01
							vCoords = 		<<-157.4010, -1089.4700, 41.1400>>
							fHeading = 		34.599
							vRotate = 		<<0.0000, 0.0000, 34.5990>>
						BREAK
						CASE 10			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_parking_hut_2"))
							vCoords = 		<<-195.8714, -1088.1708, 20.6744>>
							fHeading = 		69.999
							vRotate = 		<<0.0000, 0.0000, 69.9989>>
						BREAK			
						CASE 11			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_toolbox_01b"))
							vCoords = 		<<-172.3737, -1070.9142, 41.1612>>
							fHeading = 		285.9998
							vRotate = 		<<0.0000, 0.0000, -74.0003>>
						BREAK			
						CASE 12			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_toolbox_01b"))
							vCoords = 		<<-161.4660, -1076.9758, 41.1442>>
							fHeading = 		3.5994
							vRotate = 		<<0.0000, 0.0000, 3.5994>>
						BREAK			
						CASE 13			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_toolbox_01b"))
							vCoords = 		<<-167.7487, -1084.0592, 41.1412>>
							fHeading = 		70.7994
							vRotate = 		<<0.0000, 0.0000, 70.7994>>
						BREAK			
						CASE 14			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_toolbox_01b"))
							vCoords = 		<<-157.8310, -1088.3921, 41.1410>>
							fHeading = 		35.1989
							vRotate = 		<<0.0000, 0.0000, 35.1989>>
						BREAK			
						CASE 15			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_toolbox_01b"))
							vCoords = 		<<-162.0758, -1066.5585, 41.1412>>
							fHeading = 		249.9989
							vRotate = 		<<0.0000, -0.0000, -110.0011>>
						BREAK	
						CASE 16			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_drills_hat03x"))
							vCoords = 		<<-196.2189, -1088.9557, 21.9507>>
							fHeading = 		-21.601
							vRotate = 		<<-1.9340, 0.0000, -21.6010>>
						BREAK			
					ENDSWITCH
				BREAK
				CASE CHS_ED1_VARIATION_2
					SWITCH iProp				
						CASE 0			
							model = 		prop_cementbags01
							vCoords = 		<<-1128.0430, -967.2090, 5.6330>>
							fHeading = 		273.4
							vRotate = 		<<0.0000, 0.0000, -86.6000>>
						BREAK			
						CASE 1			
							model = 		prop_cementbags01
							vCoords = 		<<-1126.8361, -957.2810, 1.1510>>
							fHeading = 		324.599
							vRotate = 		<<0.0000, 0.0000, -35.4010>>
						BREAK			
						CASE 2			
							model = 		prop_conc_blocks01a
							vCoords = 		<<-1131.9380, -956.5880, 5.6370>>
							fHeading = 		45.199
							vRotate = 		<<0.0000, 0.0000, 45.1990>>
						BREAK			
						CASE 3			
							model = 		prop_conc_blocks01a
							vCoords = 		<<-1116.9070, -965.7290, 1.1880>>
							fHeading = 		300.199
							vRotate = 		<<0.0000, 0.0000, -59.8010>>
						BREAK			
						CASE 4			
							model = 		prop_woodpile_01c
							vCoords = 		<<-1122.1860, -973.3130, 5.6550>>
							fHeading = 		190.999
							vRotate = 		<<0.0000, 0.0000, -169.0010>>
						BREAK			
						CASE 5			
							model = 		prop_woodpile_01c
							vCoords = 		<<-1126.4480, -952.3270, 1.1560>>
							fHeading = 		85.399
							vRotate = 		<<0.0000, 0.0000, 85.3990>>
						BREAK			
						CASE 6			
							model = 		prop_conc_blocks01c
							vCoords = 		<<-1131.8080, -952.5750, 1.1500>>
							fHeading = 		34.799
							vRotate = 		<<0.0000, 0.0000, 34.7990>>
						BREAK			
						CASE 7			
							model = 		prop_conc_blocks01c
							vCoords = 		<<-1123.9650, -969.8400, 1.1500>>
							fHeading = 		2.999
							vRotate = 		<<0.0000, 0.0000, 2.9990>>
						BREAK			
						CASE 8			
							model = 		prop_pallettruck_01
							vCoords = 		<<-1126.2080, -959.2230, 1.1520>>
							fHeading = 		144.399
							vRotate = 		<<0.0000, 0.0000, 144.3990>>
						BREAK			
						CASE 9			
							model = 		prop_generator_03b
							vCoords = 		<<-1120.3110, -957.5390, 1.2100>>
							fHeading = 		127.398
							vRotate = 		<<0.0000, 0.0000, 127.3980>>
						BREAK	
						CASE 10			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_parking_hut_2"))
							vCoords = 		<<-1109.1711, -970.9182, 1.1390>>
							fHeading = 		300.3987
							vRotate = 		<<0.0000, 0.0000, -59.6013>>
						BREAK			
						CASE 11			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_toolbox_01b"))
							vCoords = 		<<-1125.2562, -957.9663, 1.1521>>
							fHeading = 		325.6
							vRotate = 		<<0.0000, 0.0000, -34.4000>>
						BREAK			
						CASE 12			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_toolbox_01b"))
							vCoords = 		<<-1132.4006, -951.6277, 1.1520>>
							fHeading = 		33.7998
							vRotate = 		<<0.0000, 0.0000, 33.7998>>
						BREAK			
						CASE 13			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_toolbox_01b"))
							vCoords = 		<<-1125.1764, -969.5287, 1.1520>>
							fHeading = 		93.5998
							vRotate = 		<<0.0000, -0.0000, 93.5998>>
						BREAK			
						CASE 14			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_toolbox_01b"))
							vCoords = 		<<-1130.4806, -953.7077, 5.6341>>
							fHeading = 		209.9994
							vRotate = 		<<0.0000, -0.0000, -150.0006>>
						BREAK			
						CASE 15			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_toolbox_01b"))
							vCoords = 		<<-1123.7086, -971.9485, 5.6341>>
							fHeading = 		11.5993
							vRotate = 		<<0.0000, 0.0000, 11.5993>>
						BREAK
						CASE 16			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_drills_hat03x"))
							vCoords = 		<<-1109.5931, -970.1598, 2.4143>>
							fHeading = 		17.2
							vRotate = 		<<0.0000, 0.0000, 17.2000>>
						BREAK	
					ENDSWITCH
				BREAK
				CASE CHS_ED1_VARIATION_3
					SWITCH iProp				
						CASE 0			
							model = 		prop_cementbags01
							vCoords = 		<<1294.1940, -740.8610, 64.5840>>
							fHeading = 		3
							vRotate = 		<<0.0000, 0.0000, 3.0000>>
						BREAK			
						CASE 1			
							model = 		prop_cementbags01
							vCoords = 		<<1313.7860, -762.7270, 65.4150>>
							fHeading = 		316.8
							vRotate = 		<<0.0000, 0.0000, -43.2000>>
						BREAK			
						CASE 2			
							model = 		prop_conc_blocks01a
							vCoords = 		<<1292.7610, -735.9140, 63.2090>>
							fHeading = 		308.4
							vRotate = 		<<0.0000, 0.0000, -51.6000>>
						BREAK			
						CASE 3			
							model = 		prop_conc_blocks01a
							vCoords = 		<<1326.6910, -742.2810, 65.3430>>
							fHeading = 		165.199
							vRotate = 		<<4.2530, 3.7550, 165.0600>>
						BREAK			
						CASE 4			
							model = 		prop_woodpile_01c
							vCoords = 		<<1298.5240, -752.4840, 64.5890>>
							fHeading = 		124.66
							vRotate = 		<<0.0000, 0.0000, 124.6600>>
						BREAK			
						CASE 5			
							model = 		prop_woodpile_01c
							vCoords = 		<<1323.8870, -756.5060, 65.4200>>
							fHeading = 		183.859
							vRotate = 		<<0.0000, 0.0000, -176.1410>>
						BREAK			
						CASE 6			
							model = 		prop_conc_blocks01c
							vCoords = 		<<1290.4821, -750.0410, 64.5840>>
							fHeading = 		140.459
							vRotate = 		<<0.0000, 0.0000, 140.4590>>
						BREAK			
						CASE 7			
							model = 		prop_conc_blocks01c
							vCoords = 		<<1316.2560, -763.7820, 65.4150>>
							fHeading = 		5.859
							vRotate = 		<<0.0000, 0.0000, 5.8590>>
						BREAK			
						CASE 8			
							model = 		prop_pallettruck_01
							vCoords = 		<<1293.2469, -743.5300, 64.6280>>
							fHeading = 		130.659
							vRotate = 		<<0.0000, 0.0000, 130.6590>>
						BREAK			
						CASE 9			
							model = 		prop_generator_03b
							vCoords = 		<<1330.9310, -746.1560, 65.7780>>
							fHeading = 		121.659
							vRotate = 		<<0.0000, 0.0000, 121.6590>>
						BREAK	
						CASE 10			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_parking_hut_2"))
							vCoords = 		<<1347.3162, -746.9082, 66.2132>>
							fHeading = 		255.2987
							vRotate = 		<<0.0000, -1.0001, -104.7013>>
						BREAK					
						CASE 11			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_toolbox_01b"))
							vCoords = 		<<1294.6040, -741.8377, 64.5850>>
							fHeading = 		185.2002
							vRotate = 		<<0.0000, -0.0000, -174.7998>>
						BREAK			
						CASE 12			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_toolbox_01b"))
							vCoords = 		<<1292.0950, -752.6929, 64.5854>>
							fHeading = 		334.5996
							vRotate = 		<<0.0000, 0.0000, -25.4004>>
						BREAK			
						CASE 13			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_toolbox_01b"))
							vCoords = 		<<1322.7997, -755.9841, 65.4268>>
							fHeading = 		94.3994
							vRotate = 		<<0.0000, -0.0000, 94.3994>>
						BREAK			
						CASE 14			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_toolbox_01b"))
							vCoords = 		<<1315.2631, -762.5413, 65.4170>>
							fHeading = 		334.999
							vRotate = 		<<0.0000, 0.0000, -25.0010>>
						BREAK			
						CASE 15			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_toolbox_01b"))
							vCoords = 		<<1297.9009, -751.7552, 64.5854>>
							fHeading = 		34.5993
							vRotate = 		<<0.0000, 0.0000, 34.5993>>
						BREAK	
						CASE 16			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_drills_hat03x"))
							vCoords = 		<<1347.5417, -746.0052, 67.4748>>
							fHeading = 		-100.2
							vRotate = 		<<0.0000, -0.0000, -100.2000>>
						BREAK	
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_POLICE_AUCTION
			SWITCH iProp				
				CASE 0			
					model = 		imp_prop_covered_vehicle_03a
					vCoords = 		<<1004.1430, -2999.1541, -40.6450>>
					fHeading = 		177.749
					vRotate = 		<<0.0000, 0.0000, 177.7490>>
				BREAK			
				CASE 1			
					model = 		imp_prop_covered_vehicle_03a
					vCoords = 		<<983.7540, -3008.5481, -40.6450>>
					fHeading = 		114.948
					vRotate = 		<<0.0000, 0.0000, 114.9480>>
				BREAK			
				CASE 2			
					model = 		imp_prop_covered_vehicle_01a
					vCoords = 		<<985.3200, -3003.9709, -40.6410>>
					fHeading = 		358.948
					vRotate = 		<<0.0000, 0.0000, -1.0520>>
				BREAK			
				CASE 3			
					model = 		imp_prop_covered_vehicle_01a
					vCoords = 		<<992.1240, -2991.0410, -40.6410>>
					fHeading = 		88.148
					vRotate = 		<<0.0000, 0.0000, 88.1480>>
				BREAK			
			ENDSWITCH	
		BREAK			
		CASE CHV_GUARD_PATROL_ROUTES
			SWITCH eSubvariation
				CASE CHS_GP_VARIATION_1
					SWITCH iProp
						CASE 0
							model = 		hei_p_attache_case_shut_s
							vCoords = 		<<-410.6150, 1227.8149, 329.6420>>
							fHeading = 		0.0
							vRotate = 		<<0.0000, 0.0000, 0.0>>
						BREAK
						CASE 1
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Ch_Schedule_01a"))
							vCoords = 		<<-416.6150, 1227.8149, 329.6310>>
							fHeading = 		0.0
							vRotate = 		<<0.0000, 0.0000, 0.0>>
						BREAK
					ENDSWITCH
				BREAK
				CASE CHS_GP_VARIATION_2
					SWITCH iProp
						CASE 0
							model = 		hei_p_attache_case_shut_s
							vCoords = 		<<-1691.2510, -944.0940, 10.6760>>
							fHeading = 		0.0
							vRotate = 		<<0.0000, 0.0000, 0.0>>
						BREAK
						CASE 1
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Ch_Schedule_01a"))
							vCoords = 		<<-1697.2510, -944.0940, 10.6760>>
							fHeading = 		0.0
							vRotate = 		<<0.0000, 0.0000, 0.0>>
						BREAK
					ENDSWITCH
				BREAK
				CASE CHS_GP_VARIATION_3
					SWITCH iProp
						CASE 0
							model = 		hei_p_attache_case_shut_s
							vCoords = 		<<-325.5620, -928.0100, 30.0810>>
							fHeading = 		0.0
							vRotate = 		<<0.0000, 0.0000, 0.0>>
						BREAK
						CASE 1
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Ch_Schedule_01a"))
							vCoords = 		<<-331.5620, -928.0100, 30.0810>>
							fHeading = 		0.0
							vRotate = 		<<0.0000, 0.0000, 0.0>>
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_ARMORED_EQUIPMENT_1
			SWITCH iProp				
				CASE 0			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_cargo_04b"))
					vCoords = 		<<929.7776, -3201.2083, -99.2630>>
					fHeading = 		6.5994
					vRotate = 		<<0.0000, 0.0000, 6.5994>>
				BREAK			
				CASE 1			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_cargo_04b"))
					vCoords = 		<<944.3569, -3222.8806, -99.2889>>
					fHeading = 		278.3993
					vRotate = 		<<0.0000, 0.0000, -81.6008>>
				BREAK			
				CASE 2			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_cargo_04b"))
					vCoords = 		<<916.6121, -3221.7290, -99.2668>>
					fHeading = 		275.3992
					vRotate = 		<<0.0000, 0.0000, -84.6008>>
				BREAK			
				CASE 3			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_cargo_03a"))
					vCoords = 		<<931.3261, -3240.0596, -99.2972>>
					fHeading = 		270.7991
					vRotate = 		<<0.0000, 0.0000, -89.2009>>
				BREAK			
				CASE 4			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_cargo_03a"))
					vCoords = 		<<932.2062, -3217.3945, -99.2622>>
					fHeading = 		208.799
					vRotate = 		<<0.0000, -0.0000, -151.2010>>
				BREAK			
				CASE 5			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_cargo_03a"))
					vCoords = 		<<948.1146, -3200.7651, -99.2675>>
					fHeading = 		140.7991
					vRotate = 		<<0.0000, -0.0000, 140.7990>>
				BREAK			
				CASE 6			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_air_cargo_04a"))
					vCoords = 		<<935.2929, -3218.2734, -99.2732>>
					fHeading = 		75.7988
					vRotate = 		<<0.0000, 0.0000, 75.7988>>
				BREAK			
				CASE 7			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_air_cargo_04a"))
					vCoords = 		<<943.0101, -3233.8171, -99.2917>>
					fHeading = 		75.7988
					vRotate = 		<<0.0000, 0.0000, 75.7988>>
				BREAK			
				CASE 8			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_air_cargo_04a"))
					vCoords = 		<<922.8120, -3209.9656, -99.2564>>
					fHeading = 		6.1987
					vRotate = 		<<0.0000, 0.0000, 6.1987>>
				BREAK			
				CASE 9			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_cargo_02a"))
					vCoords = 		<<923.4695, -3232.7915, -99.2858>>
					fHeading = 		7.3987
					vRotate = 		<<0.0000, 0.0000, 7.3987>>
				BREAK			
				CASE 10			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_cargo_02a"))
					vCoords = 		<<934.0608, -3226.1201, -99.2863>>
					fHeading = 		10.3987
					vRotate = 		<<0.0000, 0.0000, 10.3987>>
				BREAK			
				CASE 11			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_cargo_02a"))
					vCoords = 		<<939.0582, -3206.6633, -99.2680>>
					fHeading = 		305.3986
					vRotate = 		<<0.0000, 0.0000, -54.6014>>
				BREAK			
				CASE 12			
					model = 		prop_box_wood08a
					vCoords = 		<<948.5339, -3216.4929, -99.2853>>
					fHeading = 		322.7984
					vRotate = 		<<0.0000, 0.0000, -37.2016>>
				BREAK			
				CASE 13			
					model = 		prop_box_wood08a
					vCoords = 		<<929.6020, -3211.0674, -99.2649>>
					fHeading = 		288.5983
					vRotate = 		<<0.0000, 0.0000, -71.4017>>
				BREAK			
				CASE 14			
					model = 		prop_box_wood08a
					vCoords = 		<<920.0751, -3196.9485, -99.2612>>
					fHeading = 		233.398
					vRotate = 		<<0.0000, -0.0000, -126.6020>>
				BREAK			
			ENDSWITCH				
		BREAK
		CASE CHV_SERVER_FARM
			SWITCH iProp				
				CASE 0			
					SWITCH eSubvariation
						CASE CHS_SF_VARIATION_1
							model = 		hei_prop_hei_keypad_02
							vCoords = 		<<2475.6736,-435.1521,93.2891>>
							fHeading = 		0.0974
							vRotate = 		<<0.0081, 0.0000, 0.0974>>
						BREAK
						CASE CHS_SF_VARIATION_2
							model = 		hei_prop_hei_keypad_02
							vCoords = 		<<2511.8481,-319.1823,93.292>>
							fHeading = 		224.6059
							vRotate = 		<<-0.5000, 0.0000, -135.3941>>
						BREAK
						CASE CHS_SF_VARIATION_3
							model = 		hei_prop_hei_keypad_02
							vCoords = 		<<2472.4070,-333.1102,93.2953>>
							fHeading = 		179.9049
							vRotate = 		<<0.0000, -0.0000, 179.9049>>
						BREAK
					ENDSWITCH
				BREAK			
				CASE 1			
					model = 		xm_prop_x17_server_farm_cctv_01
					vCoords = 		<<2034.1432, 2909.9978, -83.3774>>
					fHeading = 		96.9768
					vRotate = 		<<11.3280, 90.1679, 96.9768>>
				BREAK			
				CASE 2			
					model = 		xm_prop_x17_server_farm_cctv_01
					vCoords = 		<<2092.5916, 2923.7346, -83.4386>>
					fHeading = 		82.3608
					vRotate = 		<<13.2260, -89.6825, 82.3608>>
				BREAK			
				CASE 3			
					model = 		xm_prop_x17_server_farm_cctv_01
					vCoords = 		<<2060.5947, 2945.8152, -83.3707>>
					fHeading = 		358.0278
					vRotate = 		<<14.2460, 91.2470, -1.9722>>
				BREAK			
				CASE 4			
					model = 		xm_prop_x17_server_farm_cctv_01
					vCoords = 		<<2128.0559, 2913.6475, -83.3646>>
					fHeading = 		187.5436
					vRotate = 		<<11.5090, -90.4121, -172.4563>>
				BREAK			
				CASE 5			
					model = 		xm_prop_x17_server_farm_cctv_01
					vCoords = 		<<2169.0374, 2936.1760, -83.3585>>
					fHeading = 		190.0077
					vRotate = 		<<20.1090, 90.0260, 10.0077>>
				BREAK			
				CASE 6			
					model = 		xm_prop_x17_server_farm_cctv_01
					vCoords = 		<<2210.6316, 2927.1753, -83.3971>>
					fHeading = 		357.431
					vRotate = 		<<24.8840, 90.4889, -2.5690>>
				BREAK			
				CASE 7			
					model = 		xm_prop_x17_server_farm_cctv_01
					vCoords = 		<<2281.6794, 2914.7124, -83.5866>>
					fHeading = 		180.0568
					vRotate = 		<<13.7520, -90.0053, -179.9432>>
				BREAK			
				CASE 8			
					model = 		xm_prop_x17_server_farm_cctv_01
					vCoords = 		<<2342.0542, 2901.4927, -83.4383>>
					fHeading = 		256.044
					vRotate = 		<<10.3000, -90.0179, -103.9560>>
				BREAK			
				CASE 9			
					model = 		xm_prop_x17_server_farm_cctv_01
					vCoords = 		<<2302.3179, 2943.4468, -83.3780>>
					fHeading = 		272.0438
					vRotate = 		<<16.1850, -89.9849, -87.9562>>
				BREAK			
				CASE 10			
					model = 		xm_prop_x17_server_farm_cctv_01
					vCoords = 		<<2175.9619, 2903.9590, -83.4708>>
					fHeading = 		302.496
					vRotate = 		<<11.4120, -90.0970, -57.5040>>
				BREAK
				CASE 11			
					model = 		xm_prop_base_staff_desk_01
					vCoords = 		<<2121.7559, 2935.8784, -85.8001>>
					fHeading = 		170.1998
					vRotate = 		<<0.0000, -0.0000, 170.1998>>
				BREAK			
				CASE 12			
					model = 		xm_prop_base_staff_desk_01
					vCoords = 		<<2310.8442, 2909.9946, -85.8001>>
					fHeading = 		0.1989
					vRotate = 		<<0.0000, 0.0000, 0.1989>>
				BREAK			
				CASE 13			
					model = 		xm_prop_base_staff_desk_01
					vCoords = 		<<2280.3831, 2890.0789, -85.8001>>
					fHeading = 		180
					vRotate = 		<<0.0000, -0.0000, -179.9999>>
				BREAK			

			ENDSWITCH				
		BREAK
		CASE CHV_ARMORED_EQUIPMENT_2
			SWITCH iProp				
				CASE 0			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_crate_01b"))
					vCoords = 		<<3749.1096, 3810.2781, 1.6057>>
					fHeading = 		85.8
					vRotate = 		<<12.6000, 4.0000, 85.8000>>
				BREAK			
				CASE 1			
					model = 		xm_prop_x17_scuba_tank
					vCoords = 		<<3748.8860, 3810.7327, 2.9263>>
					fHeading = 		349
					vRotate = 		<<-83.6005, 159.1999, -11.0000>>
				BREAK			
				CASE 2			
					model = 		xm_prop_x17_scuba_tank
					vCoords = 		<<3747.3499, 3810.4780, 2.5028>>
					fHeading = 		81.3286
					vRotate = 		<<13.6030, 3.0000, 81.3286>>
				BREAK			
				CASE 3			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_casino_shutter01x"))
					vCoords = 		<<3550.2827, 3658.4404, 27.1516>>
					fHeading = 		349.4966
					vRotate = 		<<0.0000, 0.0000, -10.5034>>
				BREAK			
				CASE 4			
					model = 		gr_prop_gr_crates_rifles_02a
					vCoords = 		<<3552.6069, 3656.5088, 27.1219>>
					fHeading = 		349.4963
					vRotate = 		<<0.0000, 0.0000, -10.5037>>
				BREAK			
				CASE 5			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_crate_01a"))
					vCoords = 		<<3549.7861, 3649.5374, 27.1219>>
					fHeading = 		260.0957
					vRotate = 		<<0.0000, -0.0000, -99.9043>>
				BREAK			
				CASE 6			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_cargo_04a"))
					vCoords = 		<<3548.6743, 3654.1436, 27.1219>>
					fHeading = 		80.2953
					vRotate = 		<<0.0000, 0.0000, 80.2953>>
				BREAK			
				CASE 7			
					model = 		xm_prop_rsply_crate04a
					vCoords = 		<<3548.5845, 3656.7678, 27.1219>>
					fHeading = 		80.8945
					vRotate = 		<<0.0000, 0.0000, 80.8945>>
				BREAK			
				CASE 8			
					model = 		prop_mil_crate_02
					vCoords = 		<<3550.8044, 3657.6511, 27.7024>>
					fHeading = 		169.894
					vRotate = 		<<0.0000, -0.0000, 169.8940>>
				BREAK			
				CASE 9			
					model = 		prop_mil_crate_02
					vCoords = 		<<3548.2783, 3652.4775, 27.7024>>
					fHeading = 		259.2938
					vRotate = 		<<0.0000, -0.0000, -100.7062>>
				BREAK			
				CASE 10			
					model = 		gr_prop_gr_crates_weapon_mix_01a
					vCoords = 		<<3548.0681, 3643.9790, 27.1233>>
					fHeading = 		78.8931
					vRotate = 		<<0.0000, 0.0000, 78.8931>>
				BREAK			
				CASE 11			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_crate_01a"))
					vCoords = 		<<3549.4500, 3647.3140, 27.1219>>
					fHeading = 		259.8953
					vRotate = 		<<0.0000, -0.0000, -100.1047>>
				BREAK			
			ENDSWITCH				
		BREAK
		CASE CHV_FIB_RAID
			SWITCH eSubvariation
				CASE CHS_FIB_VARIATION_1
					SWITCH iProp				
						CASE 0			
							model = 		prop_barrier_work05
							vCoords = 		<<-0.5969, -1545.1781, 28.3937>>
							fHeading = 		229
							vRotate = 		<<0.0000, -0.0000, -131.0000>>
						BREAK			
						CASE 1			
							model = 		prop_barrier_work05
							vCoords = 		<<-17.9848, -1533.8326, 28.7904>>
							fHeading = 		232.7999
							vRotate = 		<<0.0000, -0.0000, -127.2001>>
						BREAK			
						CASE 2			
							model = 		bkr_prop_meth_smallbag_01a
							vCoords = 		<<-8.6220, -1533.0350, 28.7370>>
							fHeading = 		294.257
							vRotate = 		<<0.0000, 0.0000, -65.7430>>
						BREAK			
						CASE 3			
							model = 		prop_cs_duffel_01
							vCoords = 		<<-6.3690, -1530.6960, 28.8680>>
							fHeading = 		267.057
							vRotate = 		<<0.0000, 0.0000, -92.9430>>
						BREAK			
						CASE 4			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("W_PI_PISTOL"))
							vCoords = 		<<-7.0246, -1534.8423, 28.6902>>
							fHeading = 		189.4568
							vRotate = 		<<87.0000, -0.0000, -170.5432>>
						BREAK			
						CASE 5			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("W_PI_PISTOL"))
							vCoords = 		<<-10.1189, -1533.0302, 28.7727>>
							fHeading = 		253.2569
							vRotate = 		<<87.9002, -0.0000, -106.7431>>
						BREAK			
						CASE 6			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("W_PI_PISTOL"))
							vCoords = 		<<-5.9045, -1531.9275, 28.7207>>
							fHeading = 		65.4564
							vRotate = 		<<-85.1001, 0.0000, 65.4564>>
						BREAK			
						CASE 7			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_fib_01a"))
							vCoords = 		<<-8.8350, -1533.4146, 28.7422>>
							fHeading = 		72.1999
							vRotate = 		<<0.0000, 0.0000, 72.1999>>
						BREAK			
						CASE 8			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_fib_01a"))
							vCoords = 		<<-9.9619, -1532.7776, 28.7741>>
							fHeading = 		270.7996
							vRotate = 		<<0.0000, 0.0000, -89.2004>>
						BREAK			
						CASE 9			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_fib_01a"))
							vCoords = 		<<-7.0539, -1534.6144, 28.6843>>
							fHeading = 		277.5995
							vRotate = 		<<0.0000, 0.0000, -82.4005>>
						BREAK			
						CASE 10			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_fib_01a"))
							vCoords = 		<<-6.6291, -1530.9336, 28.7350>>
							fHeading = 		105.1991
							vRotate = 		<<0.0000, -0.0000, 105.1991>>
						BREAK			
						CASE 11			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_fib_01a"))
							vCoords = 		<<-5.6894, -1531.9340, 28.7041>>
							fHeading = 		247.3986
							vRotate = 		<<0.0000, -0.0000, -112.6014>>
						BREAK			
	
					ENDSWITCH				
				BREAK
				
				CASE CHS_FIB_VARIATION_2
					SWITCH iProp				
						CASE 0			
							model = 		prop_barrier_work05
							vCoords = 		<<-991.1094, -1481.2567, 4.5466>>
							fHeading = 		34.2
							vRotate = 		<<-4.0803, -0.0739, 34.1974>>
						BREAK			
						CASE 1			
							model = 		prop_barrier_work05
							vCoords = 		<<-991.3354, -1470.2877, 4.1166>>
							fHeading = 		304.8
							vRotate = 		<<0.0000, 0.0000, -55.2000>>
						BREAK			
						CASE 2			
							model = 		bkr_prop_meth_smallbag_01a
							vCoords = 		<<-999.7210, -1472.8311, 4.0970>>
							fHeading = 		101.997
							vRotate = 		<<0.0000, 0.0000, 101.9970>>
						BREAK			
						CASE 3			
							model = 		prop_cs_duffel_01
							vCoords = 		<<-998.1030, -1470.9430, 5.0000>>
							fHeading = 		26.397
							vRotate = 		<<0.0000, 0.0000, 26.3970>>
						BREAK			
						CASE 4			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("W_PI_PISTOL"))
							vCoords = 		<<-998.3810, -1475.2646, 4.1743>>
							fHeading = 		155.1985
							vRotate = 		<<-82.4003, 0.0000, 155.1985>>
						BREAK			
						CASE 5			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("W_PI_PISTOL"))
							vCoords = 		<<-1003.0379, -1471.6292, 4.0449>>
							fHeading = 		155.1984
							vRotate = 		<<-87.6011, -38.7001, 155.1984>>
						BREAK			
						CASE 6			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("W_PI_PISTOL"))
							vCoords = 		<<-1000.7441, -1471.9691, 4.0828>>
							fHeading = 		295.7983
							vRotate = 		<<86.5999, -0.0000, -64.2017>>
						BREAK			
						CASE 7			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_fib_01a"))
							vCoords = 		<<-998.2512, -1475.5352, 4.1707>>
							fHeading = 		184
							vRotate = 		<<0.0000, -0.0000, -176.0000>>
						BREAK			
						CASE 8			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_fib_01a"))
							vCoords = 		<<-997.9575, -1470.5896, 4.8606>>
							fHeading = 		345.6001
							vRotate = 		<<0.0000, 0.0000, -14.3999>>
						BREAK			
						CASE 9			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_fib_01a"))
							vCoords = 		<<-999.8531, -1473.1736, 4.1090>>
							fHeading = 		100.4
							vRotate = 		<<0.0000, -0.0000, 100.3999>>
						BREAK			
						CASE 10			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_fib_01a"))
							vCoords = 		<<-1000.9069, -1471.7362, 4.0710>>
							fHeading = 		30.1998
							vRotate = 		<<0.0000, 0.0000, 30.1998>>
						BREAK			
						CASE 11			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_fib_01a"))
							vCoords = 		<<-1003.2233, -1471.4984, 4.0299>>
							fHeading = 		41.1998
							vRotate = 		<<0.0000, 0.0000, 41.1998>>
						BREAK			
		
					ENDSWITCH				
				BREAK
				
				CASE CHS_FIB_VARIATION_3
					SWITCH iProp				
						CASE 0			
							model = 		prop_barrier_work05
							vCoords = 		<<60.0980, 53.2410, 72.5200>>
							fHeading = 		70.4
							vRotate = 		<<0.0000, 0.0000, 70.4000>>
						BREAK			
						CASE 1			
							model = 		prop_barrier_work05
							vCoords = 		<<70.5510, 49.8730, 72.5200>>
							fHeading = 		69.8
							vRotate = 		<<0.0000, 0.0000, 69.8000>>
						BREAK			
						CASE 2			
							model = 		bkr_prop_meth_smallbag_01a
							vCoords = 		<<67.0050, 52.5780, 72.5150>>
							fHeading = 		96.997
							vRotate = 		<<0.0000, 0.0000, 96.9970>>
						BREAK			
						CASE 3			
							model = 		prop_cs_duffel_01
							vCoords = 		<<67.6190, 50.9100, 72.6490>>
							fHeading = 		134.797
							vRotate = 		<<0.0000, 0.0000, 134.7970>>
						BREAK			
						CASE 4			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("W_PI_PISTOL"))
							vCoords = 		<<69.7042, 53.3614, 72.5323>>
							fHeading = 		44.9973
							vRotate = 		<<85.1000, -0.0000, 44.9973>>
						BREAK			
						CASE 5			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("W_PI_PISTOL"))
							vCoords = 		<<64.3188, 54.6784, 72.5223>>
							fHeading = 		146.997
							vRotate = 		<<-88.2002, -0.0000, 146.9970>>
						BREAK			
						CASE 6			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("W_PI_PISTOL"))
							vCoords = 		<<66.1524, 50.3473, 72.5313>>
							fHeading = 		325.1968
							vRotate = 		<<89.0000, -0.0000, -34.8032>>
						BREAK			
						CASE 7			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_fib_01a"))
							vCoords = 		<<64.1692, 54.4683, 72.5154>>
							fHeading = 		143.5998
							vRotate = 		<<0.0000, -0.0000, 143.5998>>
						BREAK			
						CASE 8			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_fib_01a"))
							vCoords = 		<<66.6517, 52.6004, 72.5154>>
							fHeading = 		97.3997
							vRotate = 		<<0.0000, -0.0000, 97.3996>>
						BREAK			
						CASE 9			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_fib_01a"))
							vCoords = 		<<66.0286, 50.1414, 72.5154>>
							fHeading = 		142.9995
							vRotate = 		<<0.0000, -0.0000, 142.9995>>
						BREAK			
						CASE 10			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_fib_01a"))
							vCoords = 		<<67.7440, 51.2718, 72.5154>>
							fHeading = 		323.3991
							vRotate = 		<<0.0000, 0.0000, -36.6009>>
						BREAK			
						CASE 11			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_fib_01a"))
							vCoords = 		<<69.9338, 53.4790, 72.5154>>
							fHeading = 		230.1989
							vRotate = 		<<0.0000, -0.0000, -129.8011>>
						BREAK			
		
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_INSIDE_CASHIER
			SWITCH eSubvariation
				CASE CHS_IMC_VARIATION_1
					SWITCH iProp				
						CASE 0			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_gazebo_01"))
							vCoords = 		<<-1987.1566, 226.8538, 86.2184>>
							fHeading = 		18.8
							vRotate = 		<<0.0000, 0.0000, 18.8000>>
						BREAK			
						CASE 1			
							model = 		prop_t_coffe_table_02
							vCoords = 		<<-1992.1050, 232.1465, 86.4195>>
							fHeading = 		108.3986
							vRotate = 		<<0.0000, -0.0000, 108.3986>>
						BREAK			
						CASE 2			
							model = 		prop_t_coffe_table_02
							vCoords = 		<<-1989.7104, 247.0049, 86.4229>>
							fHeading = 		198.3985
							vRotate = 		<<0.0000, -0.0000, -161.6015>>
						BREAK			
						CASE 3			
							model = 		prop_champset
							vCoords = 		<<-1984.4392, 237.1406, 86.1775>>
							fHeading = 		257.5984
							vRotate = 		<<0.0000, -0.0000, -102.4017>>
						BREAK			
						CASE 4			
							model = 		prop_champset
							vCoords = 		<<-1992.6969, 242.8868, 86.2046>>
							fHeading = 		79.398
							vRotate = 		<<0.0000, 0.0000, 79.3980>>
						BREAK			
						CASE 5			
							model = 		prop_ball_box
							vCoords = 		<<-1995.5674, 237.4114, 86.7889>>
							fHeading = 		71.3979
							vRotate = 		<<0.0000, 0.0000, 71.3979>>
						BREAK			
						CASE 6			
							model = 		prop_ball_box
							vCoords = 		<<-1982.1881, 234.0718, 86.5720>>
							fHeading = 		19.7978
							vRotate = 		<<0.0000, 0.0000, 19.7978>>
						BREAK			
						CASE 7			
							model = 		bkr_prop_coke_powder_02
							vCoords = 		<<-1990.5089, 246.8646, 86.6343>>
							fHeading = 		89.1996
							vRotate = 		<<0.0000, 0.0000, 89.1996>>
						BREAK			
						CASE 8			
							model = 		prop_clubset
							vCoords = 		<<-1999.8685, 249.5088, 86.2696>>
							fHeading = 		226.5999
							vRotate = 		<<0.0000, -0.0000, -133.4001>>
						BREAK			
						CASE 9			
							model = 		prop_clubset
							vCoords = 		<<-1979.0369, 237.5894, 86.6199>>
							fHeading = 		341.3997
							vRotate = 		<<0.0000, 0.0000, -18.6003>>
						BREAK			
						CASE 10			
							model = 		prop_clubset
							vCoords = 		<<-1992.2029, 227.9703, 86.4201>>
							fHeading = 		34.1991
							vRotate = 		<<0.0000, 0.0000, 34.1991>>
						BREAK			
						CASE 11			
							model = 		prop_boombox_01
							vCoords = 		<<-1984.3528, 228.4109, 86.9879>>
							fHeading = 		288.7984
							vRotate = 		<<0.0000, -1.0001, -71.2016>>
						BREAK			
						CASE 12			
							model = 		prop_boombox_01
							vCoords = 		<<-1985.7041, 246.9464, 86.9719>>
							fHeading = 		335.7982
							vRotate = 		<<0.0000, -0.5000, -24.2018>>
						BREAK					
						CASE 13			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ba_prop_battle_champ_closed"))
							vCoords = 		<<-1991.7159, 231.8488, 86.8436>>
							fHeading = 		111.7998
							vRotate = 		<<0.0000, -0.0000, 111.7998>>
						BREAK			
						CASE 14			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ba_prop_battle_champ_closed"))
							vCoords = 		<<-1989.2976, 246.8380, 86.8459>>
							fHeading = 		7.7996
							vRotate = 		<<0.0000, 0.0000, 7.7996>>
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_IMC_VARIATION_2
					SWITCH iProp				
						CASE 0			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_gazebo_01"))
							vCoords = 		<<-1221.7194, 622.8810, 137.5445>>
							fHeading = 		328.3999
							vRotate = 		<<0.0000, 0.0000, -31.6001>>
						BREAK			
						CASE 1			
							model = 		prop_t_coffe_table_02
							vCoords = 		<<-1233.6254, 626.8489, 137.7512>>
							fHeading = 		30.5999
							vRotate = 		<<0.0000, 0.0000, 30.5999>>
						BREAK			
						CASE 2			
							model = 		prop_t_coffe_table_02
							vCoords = 		<<-1247.0519, 625.9851, 140.0073>>
							fHeading = 		242.3998
							vRotate = 		<<0.0000, -0.0000, -117.6002>>
						BREAK			
						CASE 3			
							model = 		prop_ball_box
							vCoords = 		<<-1242.9524, 627.0115, 140.1712>>
							fHeading = 		210.9997
							vRotate = 		<<0.0000, -0.0000, -149.0003>>
						BREAK			
						CASE 4			
							model = 		prop_ball_box
							vCoords = 		<<-1230.6169, 631.7613, 137.9155>>
							fHeading = 		160.5998
							vRotate = 		<<0.0000, -0.0000, 160.5997>>
						BREAK			
						CASE 5			
							model = 		prop_champset
							vCoords = 		<<-1227.0530, 621.2071, 137.5516>>
							fHeading = 		-0.2048
							vRotate = 		<<0.0000, 0.0000, -0.2048>>
						BREAK			
						CASE 6			
							model = 		prop_champset
							vCoords = 		<<-1239.0471, 618.4333, 137.5514>>
							fHeading = 		2.9949
							vRotate = 		<<0.0000, 0.0000, 2.9949>>
						BREAK			
						CASE 7			
							model = 		bkr_prop_coke_powder_02
							vCoords = 		<<-1246.8123, 626.6232, 140.2189>>
							fHeading = 		178.3929
							vRotate = 		<<0.0000, -0.0000, 178.3929>>
						BREAK			
						CASE 8			
							model = 		prop_clubset
							vCoords = 		<<-1221.9602, 637.2698, 141.7518>>
							fHeading = 		114.9938
							vRotate = 		<<0.0000, -0.0000, 114.9937>>
						BREAK			
						CASE 9			
							model = 		prop_clubset
							vCoords = 		<<-1237.0670, 628.5007, 137.5474>>
							fHeading = 		203.1932
							vRotate = 		<<0.0000, -0.0000, -156.8067>>
						BREAK			
						CASE 10			
							model = 		prop_clubset
							vCoords = 		<<-1224.6273, 616.5411, 137.5461>>
							fHeading = 		273.3932
							vRotate = 		<<0.0000, 0.0000, -86.6068>>
						BREAK			
						CASE 11			
							model = 		prop_boombox_01
							vCoords = 		<<-1226.0677, 622.3452, 137.7960>>
							fHeading = 		102.9928
							vRotate = 		<<0.0000, -0.0000, 102.9928>>
						BREAK			
						CASE 12			
							model = 		prop_boombox_01
							vCoords = 		<<-1221.9751, 628.4426, 137.7822>>
							fHeading = 		29.5926
							vRotate = 		<<0.0000, 0.0000, 29.5926>>
						BREAK						
						CASE 13			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ba_prop_battle_champ_closed"))
							vCoords = 		<<-1233.7418, 626.4158, 138.1736>>
							fHeading = 		27.8
							vRotate = 		<<0.0000, 0.0000, 27.8000>>
						BREAK			
						CASE 14			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("ba_prop_battle_champ_closed"))
							vCoords = 		<<-1247.0387, 625.6423, 140.4289>>
							fHeading = 		61.5999
							vRotate = 		<<0.0000, 0.0000, 61.5999>>
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	SWITCH eVariation
		CASE CHV_NOOSE_OUTFITS
			 SWITCH iProp				
				CASE 0			
					model = 		gr_prop_gr_gunsmithsupl_03a
					vCoords = 		<<461.2468, -981.0782, 29.6919>>
					fHeading = 		269.7983
					vRotate = 		<<0.0000, -0.0000, -90.2017>>
				BREAK			
				CASE 1			
					model = 		INT_TO_ENUM(MODEL_NAMES, HASH("prop_mb_crate_01a"))
					vCoords = 		<<459.9608, -979.4615, 29.6896>>
					fHeading = 		346.9981
					vRotate = 		<<0.0000, 0.0000, -13.0019>>
				BREAK			
				CASE 2			
					model = 		xm_prop_rsply_crate04a
					vCoords = 		<<459.6687, -982.8396, 29.6896>>
					fHeading = 		180.398
					vRotate = 		<<0.0000, -0.0000, -179.6020>>
				BREAK			
			ENDSWITCH
		BREAK				
		CASE CHV_BUGSTAR_OUTFITS_2
			SWITCH eSubvariation
				CASE CHS_BO2_VARIATION_1
					SWITCH iProp				
						CASE 15			
							model = 		prop_toolchest_05
							vCoords = 		<<-449.7964, -1701.2385, 17.8622>>
							fHeading = 		212.5981
							vRotate = 		<<0.0000, -0.0000, -147.4019>>
						BREAK			
						CASE 16			
							model = 		prop_worklight_03b
							vCoords = 		<<-448.7127, -1690.3872, 18.0680>>
							fHeading = 		316.3975
							vRotate = 		<<0.0000, 0.0000, -43.6025>>
						BREAK			
						CASE 17			
							model = 		prop_worklight_02a
							vCoords = 		<<-458.1500, -1692.3685, 17.9526>>
							fHeading = 		251.9971
							vRotate = 		<<0.0000, -0.0000, -108.0029>>
						BREAK			
						CASE 18			
							model = 		prop_worklight_02a
							vCoords = 		<<-470.0335, -1696.3813, 17.8480>>
							fHeading = 		151.7968
							vRotate = 		<<0.0000, -0.0000, 151.7968>>
						BREAK			
						DEFAULT
							vCoords = 		<< 972.100-iProp*0.5, -3.4, 59.390 >>
							model = HEI_PROP_HEIST_BINBAG
						BREAK
					ENDSWITCH
				BREAK
				CASE CHS_BO2_VARIATION_2
					SWITCH iProp				
						CASE 15			
							model = 		prop_toolchest_05
							vCoords = 		<<-506.3443, -1738.4512, 17.9428>>
							fHeading = 		162.1989
							vRotate = 		<<0.0000, -6.0004, 162.1988>>
						BREAK			
						CASE 16			
							model = 		prop_worklight_03b
							vCoords = 		<<-513.9033, -1737.2457, 18.3505>>
							fHeading = 		116.9967
							vRotate = 		<<0.0000, -0.0000, 116.9966>>
						BREAK			
						CASE 17			
							model = 		prop_worklight_02a
							vCoords = 		<<-501.2501, -1714.0795, 18.3853>>
							fHeading = 		322.7961
							vRotate = 		<<2.5182, -3.5189, -37.1265>>
						BREAK			
						CASE 18			
							model = 		prop_worklight_02a
							vCoords = 		<<-500.7286, -1727.3915, 18.1883>>
							fHeading = 		247.2732
							vRotate = 		<<1.8655, 5.7474, -112.8204>>
						BREAK			
						DEFAULT
							vCoords = 		<< 972.100-iProp*0.5, -3.4, 59.390 >>
							model = HEI_PROP_HEIST_BINBAG
						BREAK
					ENDSWITCH
				BREAK
				CASE CHS_BO2_VARIATION_3
					SWITCH iProp				
						CASE 15			
							model = 		prop_toolchest_05
							vCoords = 		<<-648.0164, -1709.8615, 23.6117>>
							fHeading = 		23.1986
							vRotate = 		<<0.0000, 0.0000, 23.1986>>
						BREAK			
						CASE 16			
							model = 		prop_worklight_03b
							vCoords = 		<<-653.0271, -1726.2396, 23.7595>>
							fHeading = 		162.7795
							vRotate = 		<<0.0000, -0.0000, 162.7795>>
						BREAK			
						CASE 17			
							model = 		prop_worklight_02a
							vCoords = 		<<-636.7729, -1719.0695, 23.3549>>
							fHeading = 		320.9792
							vRotate = 		<<0.0000, 0.0000, -39.0208>>
						BREAK			
						CASE 18			
							model = 		prop_worklight_02a
							vCoords = 		<<-643.9019, -1707.3193, 23.6203>>
							fHeading = 		5.9791
							vRotate = 		<<0.0000, 0.0000, 5.9791>>
						BREAK
						DEFAULT
							vCoords = 		<< 972.100-iProp*0.5, -3.4, 59.390 >>
							model = HEI_PROP_HEIST_BINBAG
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_CELEB_AFTER_PARTY
			model = INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Ch_Camera_01"))
		BREAK	
		CASE CHV_VALET_SERVICE
			SWITCH eSubvariation
				CASE CHS_VS_VARIATION_1
					SWITCH iProp
						CASE 0
							model =				INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_vw_valet_01a"))
							vCoords = 			<<-1667.8550, -538.3564, 34.3699>>
							fHeading =			143.9997
							vRotate =			<<0.0000, -4.5032, 143.9997>>
						BREAK
					ENDSWITCH
				BREAK
				CASE CHS_VS_VARIATION_2
					SWITCH iProp
						CASE 0
							model =				INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_vw_valet_01a"))
							vCoords = 			<<-695.4580, -2288.2783, 12.0530>>
							fHeading =			225.3998
							vRotate =			<<0.0000, -0.0000, -134.6002>>
						BREAK
					ENDSWITCH
				BREAK
				CASE CHS_VS_VARIATION_3
					SWITCH iProp
						CASE 0
							model =				INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_vw_valet_01a"))
							vCoords = 			<<-573.6411, -444.4714, 33.2879>>
							fHeading =			180.4002
							vRotate =			<<0.0000, -0.0000, -179.5998>>
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CHV_VAULT_DRILL_1
			SWITCH eSubvariation
				CASE CHS_VD1_VARIATION_1
					SWITCH iProp				
						CASE 0			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_vw_board_01a"))
							vCoords = 		<<752.1170, -3195.1919, 5.0780>>
							fHeading = 		91.599
							vRotate = 		<<0.0000, 0.0000, 91.5990>>
						BREAK			
						CASE 1			
							model = 		prop_conc_sacks_02a
							vCoords = 		<<757.5400, -3198.3689, 5.1040>>
							fHeading = 		178.598
							vRotate = 		<<0.0000, 0.0000, 178.5980>>
						BREAK			
						CASE 2			
							model = 		prop_conc_sacks_02a
							vCoords = 		<<760.0000, -3198.4060, 5.1040>>
							fHeading = 		178.598
							vRotate = 		<<0.0000, 0.0000, 178.5980>>
						BREAK			
						CASE 3			
							model = 		gr_prop_gr_target_02a
							vCoords = 		<<757.5590, -3198.3059, 5.9150>>
							fHeading = 		178.598
							vRotate = 		<<0.0000, 0.0000, 178.5980>>
						BREAK			
						CASE 4			
							model = 		gr_prop_gr_target_02a
							vCoords = 		<<760.0120, -3198.3521, 5.9190>>
							fHeading = 		178.598
							vRotate = 		<<0.0000, 0.0000, 178.5980>>
						BREAK			
						CASE 5			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_bench_02b"))
							vCoords = 		<<756.4990, -3192.2100, 5.0790>>
							fHeading = 		359.598
							vRotate = 		<<0.0000, 0.0000, -0.4020>>
						BREAK			
						CASE 6			
							model = 		gr_prop_gr_gunsmithsupl_01a
							vCoords = 		<<759.0720, -3192.5330, 5.0820>>
							fHeading = 		90.596
							vRotate = 		<<0.0000, 0.0000, 90.5960>>
						BREAK			
						CASE 7			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_vw_table_01a"))
							vCoords = 		<<755.1120, -3196.5911, 5.0880>>
							fHeading = 		301.795
							vRotate = 		<<0.0000, 0.0000, -58.2050>>
						BREAK			
						CASE 8			
							model = 		imp_prop_covered_vehicle_03a
							vCoords = 		<<759.3870, -3195.4021, 5.0800>>
							fHeading = 		80.595
							vRotate = 		<<0.0000, 0.0000, 80.5950>>
						BREAK	
						CASE 9			
							model = 		prop_worklight_03a
							vCoords = 		<<762.5468, -3198.1375, 5.0575>>
							fHeading = 		225.5998
							vRotate = 		<<0.0000, -0.0000, -134.4002>>
						BREAK			
						CASE 10			
							model = 		prop_worklight_03a
							vCoords = 		<<752.3613, -3192.3694, 5.0555>>
							fHeading = 		52.1996
							vRotate = 		<<0.0000, 0.0000, 52.1996>>
						BREAK			
					ENDSWITCH				
				BREAK
				CASE CHS_VD1_VARIATION_2
					SWITCH iProp				
						CASE 0			
							model = 		prop_conc_sacks_02a
							vCoords = 		<<-1340.1860, -896.9330, 12.4510>>
							fHeading = 		217.799
							vRotate = 		<<0.0000, 0.0000, -142.2010>>
						BREAK			
						CASE 1			
							model = 		prop_conc_sacks_02a
							vCoords = 		<<-1341.9790, -898.3440, 12.4500>>
							fHeading = 		217.799
							vRotate = 		<<0.0000, 0.0000, -142.2010>>
						BREAK			
						CASE 2			
							model = 		gr_prop_gr_target_02a
							vCoords = 		<<-1340.2159, -896.8650, 13.2720>>
							fHeading = 		-142.201
							vRotate = 		<<0.0000, 0.0000, -142.2010>>
						BREAK			
						CASE 3			
							model = 		gr_prop_gr_target_02a
							vCoords = 		<<-1341.9980, -898.2940, 13.2760>>
							fHeading = 		-142.201
							vRotate = 		<<0.0000, 0.0000, -142.2010>>
						BREAK			
						CASE 4			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_bench_02b"))
							vCoords = 		<<-1346.8459, -891.0690, 12.4780>>
							fHeading = 		37.799
							vRotate = 		<<0.0000, 0.0000, 37.7990>>
						BREAK			
						CASE 5			
							model = 		gr_prop_gr_gunsmithsupl_01a
							vCoords = 		<<-1343.6689, -890.7730, 12.4760>>
							fHeading = 		37.598
							vRotate = 		<<0.0000, 0.0000, 37.5980>>
						BREAK			
						CASE 6			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_vw_board_01a"))
							vCoords = 		<<-1340.5010, -894.0520, 12.4460>>
							fHeading = 		308.598
							vRotate = 		<<0.0000, 0.0000, -51.4020>>
						BREAK			
						CASE 7			
							model = 		imp_prop_covered_vehicle_03a
							vCoords = 		<<-1344.0520, -896.2450, 12.4480>>
							fHeading = 		332.598
							vRotate = 		<<0.0000, 0.0000, -27.4020>>
						BREAK			
						CASE 8			
							model = 		INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_vw_table_01a"))
							vCoords = 		<<-1342.2190, -892.3370, 12.4820>>
							fHeading = 		126.997
							vRotate = 		<<0.0000, 0.0000, 126.9970>>
						BREAK			
						CASE 9			
							model = 		prop_sec_gate_01d
							vCoords = 		<<-1346.5010, -895.5600, 16.8890>>
							fHeading = 		127.599
							vRotate = 		<<0.0000, 0.0000, 127.5990>>
						BREAK	
						CASE 10			
							model = 		prop_worklight_01a
							vCoords = 		<<-1339.0382, -895.8981, 12.3967>>
							fHeading = 		250.7997
							vRotate = 		<<0.0000, -0.0000, -109.2003>>
						BREAK			
						CASE 11			
							model = 		prop_worklight_01a
							vCoords = 		<<-1345.6561, -895.5504, 12.4655>>
							fHeading = 		124.9494
							vRotate = 		<<0.0000, -0.0000, 124.9494>>
						BREAK			
					ENDSWITCH				
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH

	CSH_PROP_DATA data
	data.model = model
	data.vCoords = vCoords
	data.fHeading = fHeading
	data.vRotation = vRotate
	
	RETURN data

ENDFUNC

FUNC MODEL_NAMES GET_CSH_PROP_MODEL(INT iProp, CSH_VARIATION eVariation, CSH_SUBVARIATION eSubvariation, INT iExtraParam = -1)
	
	UNUSED_PARAMETER(iExtraParam)
	
	CSH_PROP_DATA data = GET_CSH_PROP_DATA(iProp, eVariation, eSubvariation, iExtraParam)
	RETURN data.model
	
ENDFUNC

FUNC VECTOR GET_CSH_PROP_COORDS(INT iProp, CSH_VARIATION eVariation, CSH_SUBVARIATION eSubvariation, INT iExtraParam = -1)
	
	UNUSED_PARAMETER(iExtraParam)
	
	CSH_PROP_DATA data = GET_CSH_PROP_DATA(iProp, eVariation, eSubvariation, iExtraParam)
	RETURN data.vCoords
	
ENDFUNC

FUNC FLOAT GET_CSH_PROP_HEADING(INT iProp, CSH_VARIATION eVariation, CSH_SUBVARIATION eSubvariation, INT iExtraParam = -1)
	
	UNUSED_PARAMETER(iExtraParam)
	
	CSH_PROP_DATA data = GET_CSH_PROP_DATA(iProp, eVariation, eSubvariation, iExtraParam)
	RETURN data.fHeading
	
ENDFUNC

FUNC VECTOR GET_CSH_PROP_ROTATION(INT iProp, CSH_VARIATION eVariation, CSH_SUBVARIATION eSubvariation, INT iExtraParam = -1)
	
	UNUSED_PARAMETER(iExtraParam)
	
	CSH_PROP_DATA data = GET_CSH_PROP_DATA(iProp, eVariation, eSubvariation, iExtraParam)
	RETURN data.vRotation
	
ENDFUNC

FUNC VECTOR GET_CSH_SUPPORT_VEHICLE_SPAWN_COORDS(INT iVehicle)
	
	SWITCH iVehicle
		CASE 0			RETURN <<-2364.3950, 3351.1047, 31.8329>>
		CASE 1			RETURN <<-2316.0933, 3353.1084, 31.8368>>
		CASE 2			RETURN <<-2334.4307, 3363.3738, 31.8321>>
		CASE 3			RETURN <<-2353.2769, 3374.9424, 31.8329>>
		CASE 4			RETURN <<-2372.0039, 3385.2944, 31.8329>>
		CASE 5			RETURN <<-2224.3394, 3248.8999, 31.8102>>
		CASE 6			RETURN <<-2238.1506, 3223.9824, 31.8102>>
		CASE 7			RETURN <<-2251.9897, 3200.8274, 31.8100>>
		CASE 8			RETURN <<-2262.2456, 3183.0215, 31.8100>>
		CASE 9			RETURN <<-2383.7446, 3361.9934, 31.8329>>
	ENDSWITCH
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC FLOAT GET_CSH_SUPPORT_VEHICLE_SPAWN_HEADING(INT iVehicle)

	// Specific locations have specific coords for spawning support vehicles
	SWITCH iVehicle
		CASE 0			RETURN 329.9994
		CASE 1			RETURN 150.7997
		CASE 2			RETURN 150.7997
		CASE 3			RETURN 150.3997
		CASE 4			RETURN 150.3997
		CASE 5			RETURN 239.3995
		CASE 6			RETURN 238.5995
		CASE 7			RETURN 238.5995
		CASE 8			RETURN 238.5995
		CASE 9			RETURN 331.7995
	ENDSWITCH
	
	RETURN 0.0
ENDFUNC

FUNC VECTOR GET_CSH_SHARED_VEHICLE_SPAWN_COORD(CSH_VARIATION eVariation, INT iVehicle, BOOL bSupportVehicle = FALSE)
	
	UNUSED_PARAMETER(eVariation)
	
	IF bSupportVehicle
		RETURN GET_CSH_SUPPORT_VEHICLE_SPAWN_COORDS(iVehicle)
	ENDIF

	SWITCH iVehicle
		CASE 0			RETURN <<910.9286, 45.9237, 79.8989>>
		CASE 1			RETURN <<918.8007, 58.7535, 79.8989>>
		CASE 2			RETURN <<903.7056, 34.5147, 79.1406>>
		CASE 3			RETURN <<896.7097, 23.5363, 78.1498>>
		CASE 4			RETURN <<926.4149, 71.0739, 78.3736>>
		CASE 5			RETURN <<913.8373, 35.9826, 79.6427>>
		CASE 6			RETURN <<903.3990, 19.5460, 78.1507>>
		CASE 7			RETURN <<929.6061, 61.3647, 79.3707>>
		CASE 8			RETURN <<908.7288, 28.0629, 78.9269>>
	ENDSWITCH
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC FLOAT GET_CSH_SHARED_VEHICLE_SPAWN_HEADING(CSH_VARIATION eVariation, INT iVehicle, BOOL bSupportVehicle = FALSE)
	
	UNUSED_PARAMETER(eVariation)
	
	IF bSupportVehicle
		RETURN GET_CSH_SUPPORT_VEHICLE_SPAWN_HEADING(iVehicle)
	ENDIF

	SWITCH iVehicle
		CASE 0			RETURN 148.3990
		CASE 1			RETURN 148.5990
		CASE 2			RETURN 148.3990
		CASE 3			RETURN 148.3990
		CASE 4			RETURN 148.5990
		CASE 5			RETURN 327.9990
		CASE 6			RETURN 327.5990
		CASE 7			RETURN 327.9990
		CASE 8			RETURN 327.5990
	ENDSWITCH
	
	RETURN 0.0
ENDFUNC

#ENDIF
