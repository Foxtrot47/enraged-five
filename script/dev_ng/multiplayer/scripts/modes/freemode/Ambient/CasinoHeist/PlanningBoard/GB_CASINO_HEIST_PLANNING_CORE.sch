/* ===================================================================================== *\
		MISSION NAME	:	GB_CASINO_HEIST_PLANNING_CORE.sch
		AUTHOR			:	Steve Tiley - 14/08/2019
		DESCRIPTION		:	Core header script for the casino heist planning boards
							which drives the script states
\* ===================================================================================== */

USING "GB_CASINO_HEIST_PLANNING_BOARD_SETUP.sch"
USING "GB_CASINO_HEIST_PLANNING_BOARD_PREP.sch"
USING "GB_CASINO_HEIST_PLANNING_BOARD_FINALE.sch"

#IF FEATURE_CASINO_HEIST

//╒═══════════════════════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡    SCRIPT INITIALISATION    ╞═════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════════════════╛

FUNC BOOL PROCESS_PRE_GAME(CASINO_HEIST_CLIENT_DATA& sClientData)
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PROCESS_PRE_GAME - NOT NETWORK_IS_GAME_IN_PROGRESS() - RETURN FALSE.")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(sClientData.sFinaleData.iBitSet, ciCASINO_HEIST_FINALE_BOARD_BITSET__CLEANUP_FOR_WALK_OUT)
		CLEAR_BIT(sClientData.sFinaleData.iBitSet, ciCASINO_HEIST_FINALE_BOARD_BITSET__CLEANUP_FOR_WALK_OUT)
		CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PROCESS_PRE_GAME - Set to cleanup for walk out but script wasnt running, clear this.")
	ENDIF
	
	IF IS_BIT_SET(sClientData.sFinaleData.iBitSet, ciCASINO_HEIST_FINALE_BOARD_BITSET__RETURN_TO_MAINTAIN)
		CLEAR_BIT(sClientData.sFinaleData.iBitSet, ciCASINO_HEIST_FINALE_BOARD_BITSET__RETURN_TO_MAINTAIN)
		CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PROCESS_PRE_GAME - Set to return to maintain but script wasnt running, clear this.")
	ENDIF

	NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
	
	IF !g_bCasinoHeistPlanningScriptActive
		g_bCasinoHeistPlanningScriptActive = TRUE
		CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PROCESS_PRE_GAME - Casino heist planning script is active.")
	ENDIF
	
	CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PROCESS_PRE_GAME - Completed.")
	RETURN TRUE
ENDFUNC

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡    MAIN LOOP    ╞═════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

FUNC BOOL INITIALISE_CASINO_HEIST_BOARDS(CASINO_HEIST_CLIENT_DATA& sClientData)
	
	IF NOT NETWORK_IS_ACTIVITY_SESSION()
		sClientData.piLeader = GET_OWNER_OF_SIMPLE_INTERIOR_LOCAL_PLAYER_IS_IN()
		IF sClientData.piLeader != INVALID_PLAYER_INDEX()
			CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | INITIALISE_CASINO_HEIST_BOARDS - Leader is ", GET_PLAYER_NAME(sClientData.piLeader))
		ELSE
			CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | INITIALISE_CASINO_HEIST_BOARDS - Leader is INVALID_PLAYER_INDEX")
		ENDIF
	ENDIF

	IF sClientData.piLeader != INVALID_PLAYER_INDEX()
	AND INITIALISE_CASINO_HEIST_SETUP_BOARD(sClientData)
	AND INITIALISE_CASINO_HEIST_PREP_BOARD(sClientData)
	AND INITIALISE_CASINO_HEIST_FINALE_BOARD(sClientData)
	
		IF NOT IS_BIT_SET(GlobalplayerBD_CasinoHeist[NETWORK_PLAYER_ID_TO_INT()].iGeneralBitset, ciCASINO_HEIST_GENERAL_BITSET__INITIALISED_BOARDS)
			SET_BIT(GlobalplayerBD_CasinoHeist[NETWORK_PLAYER_ID_TO_INT()].iGeneralBitset, ciCASINO_HEIST_GENERAL_BITSET__INITIALISED_BOARDS)
			CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | INITIALISE_CASINO_HEIST_BOARDS - ciCASINO_HEIST_GENERAL_BITSET__INITIALISED_BOARDS set")
		ENDIF
	
		CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | INITIALISE_CASINO_HEIST_BOARDS - All boards have been initialised.")
		RETURN TRUE
	ENDIF

	CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | INITIALISE_CASINO_HEIST_BOARDS - Waiting for all boards to intialise...")
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_CASINO_HEIST_BOARDS(CASINO_HEIST_CLIENT_DATA& sClientData)
	MAINTAIN_CASINO_HEIST_SETUP_BOARD(sClientData)
	MAINTAIN_CASINO_HEIST_PREP_BOARD(sClientData)
	MAINTAIN_CASINO_HEIST_FINALE_BOARD(sClientData)
	MAINTAIN_CASINO_HEIST_BOARDS_GENERAL_CHECKS(sClientData)
	MAINTAIN_CASINO_HEIST_BOARDS_HIGHLIGHT_UPDATE(sClientData)
	MAINTAIN_CASINO_HEIST_BOARDS_DETAILS_PANEL(sClientData)
	MAINTAIN_CASINO_HEIST_DIALOGUE(sClientData)
	MAINTAIN_CASIOO_HEIST_AUDIO_BANK_LOADING(sClientData)
	MAINTAIN_CASINO_HEIST_HELP_TRIGGERS(sClientData)
ENDPROC


//╒════════════════════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡    SCRIPT TERMINATION    ╞═════════════════════════════════╡
//╘════════════════════════════════════════════════════════════════════════════════════════════╛

PROC MAINTAIN_CLEANUP_CHECKS(CASINO_HEIST_CLIENT_DATA& sClientData)
	IF PUBLIC_CASINO_HEIST_BOARD__SHOULD_BOARDS_CLEAN_UP(sClientData)
	OR PUBLIC_CASINO_HEIST_BOARD__SHOULD_BOARDS_CLEAN_UP_IMMEDIATELY()
		IF !sClientData.bCleanupRequested
		AND NOT PUBLIC_CASINO_HEIST_BOARD__SHOULD_BOARDS_CLEAN_UP_IMMEDIATELY()
			PUBLIC_SET_CASINO_HEIST_FSM_STATES_TO_CLEANUP(sClientData)
			sClientData.bCleanupRequested = TRUE
		ELSE
			IF ARE_ALL_CASINO_HEIST_BOARDS_CLEANED_UP(sClientData)
			OR PUBLIC_CASINO_HEIST_BOARD__SHOULD_BOARDS_CLEAN_UP_IMMEDIATELY()
				CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | MAINTAIN_CLEANUP_CHECKS - All heist boards are cleaned up. Moving to terminate script.")
				SET_SCRIPT_STAGE(STAGE_CLEANUP)
				sClientData.bCleanupRequested = FALSE
			ELSE
				CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | MAINTAIN_CLEANUP_CHECKS - Waiting for all heist boards to be in cleanup state...")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SCRIPT_CLEANUP()
	PUBLIC_CLEAN_CASINO_HEIST_ALL()
	CLEAR_INTERACT_HELP()
	RELEASE_RENDERTARGETS()
	
	IF NETWORK_PLAYER_ID_TO_INT() != -1
	AND IS_BIT_SET(GlobalplayerBD_CasinoHeist[NETWORK_PLAYER_ID_TO_INT()].iGeneralBitset, ciCASINO_HEIST_GENERAL_BITSET__INITIALISED_BOARDS)
		CLEAR_BIT(GlobalplayerBD_CasinoHeist[NETWORK_PLAYER_ID_TO_INT()].iGeneralBitset, ciCASINO_HEIST_GENERAL_BITSET__INITIALISED_BOARDS)
		CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | SCRIPT_CLEANUP - ciCASINO_HEIST_GENERAL_BITSET__INITIALISED_BOARDS cleared")
	ENDIF
	
	IF IS_BIT_SET(GlobalplayerBD_CasinoHeist[NETWORK_PLAYER_ID_TO_INT()].iGeneralBitset, ciCASINO_HEIST_GENERAL_BITSET__INTERACTING_WITH_BOARDS)
		CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | SCRIPT_CLEANUP - ciCASINO_HEIST_GENERAL_BITSET__INTERACTING_WITH_BOARDS cleared")
		CLEAR_BIT(GlobalplayerBD_CasinoHeist[NETWORK_PLAYER_ID_TO_INT()].iGeneralBitset, ciCASINO_HEIST_GENERAL_BITSET__INTERACTING_WITH_BOARDS)
	ENDIF
	
	IF g_bCasinoHeistPlanningScriptActive
		g_bCasinoHeistPlanningScriptActive = FALSE
		CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | SCRIPT_CLEANUP - Casino heist planning script is terminating.")
	ENDIF
	
	TERMINATE_THIS_THREAD()
ENDPROC

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════════╡    SCRIPT    ╞═════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

PROC PROCESS_CASINO_HEIST_PLANNING(CASINO_HEIST_CLIENT_DATA& sClientData)
		
	MAINTAIN_CLEANUP_CHECKS(sClientData)
	
	SWITCH GET_SCRIPT_STAGE()
		// Check whether we need to load/display the planning board or the finale board, prep the scaleforms and grab the data about the strand/finale
		CASE STAGE_INIT
			IF INITIALISE_CASINO_HEIST_BOARDS(sClientData)
				SET_SCRIPT_STAGE(STAGE_RUNNING)
			ENDIF
		BREAK
		
		// Maintain the planning board for setup missions
		CASE STAGE_RUNNING
			MAINTAIN_CASINO_HEIST_BOARDS(sClientData)
		BREAK
		
		// Cleanup
		CASE STAGE_CLEANUP
			SCRIPT_CLEANUP()
		BREAK
	ENDSWITCH
	
	CLEAR_CASINO_HEIST_PLANNING_BOARDS_DISABLED_THIS_FRAME()

ENDPROC

#ENDIF //FEATURE_CASINO_HEIST


// EOF
