/* ===================================================================================== *\
		MISSION NAME	:	GB_CASINO_HEIST_PLANNING_BOARD_FINALE.sch
		AUTHOR			:	Steve Tiley - 14/08/2019
		DESCRIPTION		:	Header for the Casino Heist Finale board.
\* ===================================================================================== */



// INCLUDE FILES.
USING "globals.sch"
USING "commands_network.sch"
USING "net_include.sch"
USING "net_FPS_cam.sch"
USING "MP_Scaleform_Functions.sch"
USING "hud_drawing.sch"

#IF FEATURE_CASINO_HEIST
USING "gb_casino_heist_planning_support.sch"
USING "gb_casino_heist_planning_scaleform.sch"

/* ===================================================================================== *\
			DEBUG ONLY
\* ===================================================================================== */

#IF IS_DEBUG_BUILD

PROC PRIVATE_DEBUG_CASINO_HEIST_PLANNINGALE_ADD_FAKE_PLAYER(CASINO_HEIST_CLIENT_DATA &sClientData)

	sClientData.sFinaleData.sFinaleConfig.iTotalPlayers++
	
	CASINO_HEIST_FINALE_BOARD_ITEMS eItemSlot = PUBLIC_CASINO_HEIST_FINALE_BOARD__GET_CREW_SLOT_FOR_INDEX(sClientData.sFinaleData.sFinaleConfig.iTotalPlayers-1)
	
	PUBLIC_CASINO_HEIST_FINALE_BOARD__SET_CREW_MEMBER_DETAILS(sClientData.sFinaleData.sScaleformData.siMainBoard, eItemSlot, "", "")

ENDPROC

PROC PRIVATE_PROCESS_CASINO_HEIST_FINALE_WIDGET_COMMANDS(CASINO_HEIST_CLIENT_DATA &sClientData)

	IF sClientData.sFinaleData.sDebugData.bStart
	
		IF sClientData.sFinaleData.sStateData.eState != CHBS_IDLE
			sClientData.sFinaleData.sStateData.eState = CHBS_IDLE
		ENDIF
		
		sClientData.sFinaleData.sStateData.eState = CHBS_INIT
		sClientData.sFinaleData.sDebugData.bStart = FALSE
		
		CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "FSM - Debug Starting Planning")
	ENDIF
		
	IF sClientData.sFinaleData.sDebugData.bAddFakePlayer
		PRIVATE_DEBUG_CASINO_HEIST_PLANNINGALE_ADD_FAKE_PLAYER(sClientData)
		sClientData.sFinaleData.sDebugData.bAddFakePlayer = FALSE
	ENDIF
	
ENDPROC

PROC PRIVATE_CASINO_HEIST_FINALE_WIDGET_UPDATE(CASINO_HEIST_CLIENT_DATA &sClientData)

	IF NOT sClientData.sFinaleData.sDebugData.bCreatedWidgets
		IF DOES_WIDGET_GROUP_EXIST(sClientData.sFinaleData.sDebugData.wgParentGroup)
			SET_CURRENT_WIDGET_GROUP(sClientData.sFinaleData.sDebugData.wgParentGroup)
			
			sClientData.sFinaleData.sDebugData.wgActiveGroup = START_WIDGET_GROUP("  Casino Heist (Finale)") 
			
				ADD_WIDGET_STRING(" - DEBUG SETTINGS - ")
				ADD_WIDGET_BOOL("Start", sClientData.sFinaleData.sDebugData.bStart)
				ADD_WIDGET_BOOL("Reset", sClientData.sFinaleData.sDebugData.bReset)
				ADD_WIDGET_BOOL("Player Visibility", sClientData.sFinaleData.sDebugData.bPlayerVisible)
				ADD_WIDGET_BOOL("Lock Rendering to Player", sClientData.sFinaleData.sDebugData.bLockToPlayer)
				
				ADD_WIDGET_STRING(" - DEBUG ACTIONS - ")
				ADD_WIDGET_BOOL("Interact", sClientData.sFinaleData.sStateData.bInteract)
				ADD_WIDGET_BOOL("Add Player", sClientData.sFinaleData.sDebugData.bAddFakePlayer)
				
				START_WIDGET_GROUP("Component Toggle")
					ADD_WIDGET_BOOL("Component Toggle: Scaleform", sClientData.sFinaleData.sScaleformData.bEnableScaleform)
					ADD_WIDGET_BOOL("Component Toggle: Camera", sClientData.sCameraData.bEnableCamera)
				STOP_WIDGET_GROUP()

				START_WIDGET_GROUP("Scaleform")
					ADD_WIDGET_BOOL("Verbose Scaleform Logging", sClientData.sFinaleData.sScaleformData.bVerboseDebugging)
					
					ADD_WIDGET_VECTOR_SLIDER("Main Board - World Pos", sClientData.sFinaleData.sScaleformData.vMainBoardPosition, -9999.9, 9999.9, 1.0)
					ADD_WIDGET_VECTOR_SLIDER("Main Board - World Rot", sClientData.sFinaleData.sScaleformData.vMainBoardRotation, -1000.0, 1000.0, 1.0)
					ADD_WIDGET_VECTOR_SLIDER("Main Board - Size", sClientData.sFinaleData.sScaleformData.vMainBoardSize, 0.0, 100.0, 1.0)
					ADD_WIDGET_VECTOR_SLIDER("Main Board - World Size", sClientData.sFinaleData.sScaleformData.vMainBoardWorldSize, 0.0, 100.0, 1.0)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Cameras")
				
					ADD_WIDGET_INT_SLIDER("Cam Position", sClientData.sCameraData.iPositionId, 0, 10, 1)
					ADD_WIDGET_VECTOR_SLIDER("Cam Initial Pos Offset", sClientData.sCameraData.vCamInitialOffset, -100.0, 100.0, 0.1)
					
					ADD_WIDGET_VECTOR_SLIDER("Cam Initial Rot Offset", sClientData.sCameraData.vCamInitialOffsetRot, -180.0, 180.0, 0.1)
				
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Highlight / Cursor Data") 
					ADD_WIDGET_INT_SLIDER("iHighlight: ", 		sClientData.sCursorData.iHighlight, -100, 100, 1)
					ADD_WIDGET_INT_SLIDER("iSubHighlight: ", 	sClientData.sCursorData.iSubHighlight, -100, 100, 1)
					ADD_WIDGET_INT_SLIDER("iRightArrow: ", 		sClientData.sCursorData.iRightArrow, -100, 100, 1)
					ADD_WIDGET_INT_SLIDER("iLeftArrow: ", 		sClientData.sCursorData.iLeftArrow, -100, 100, 1)
					ADD_WIDGET_INT_SLIDER("iSubItemSelection: ",sClientData.sCursorData.iSubItemSelection, -100, 100, 1)
				STOP_WIDGET_GROUP()
				
			STOP_WIDGET_GROUP()
			
			CLEAR_CURRENT_WIDGET_GROUP(sClientData.sFinaleData.sDebugData.wgParentGroup)
			
			sClientData.sFinaleData.sDebugData.bCreatedWidgets = TRUE
		ENDIF
	ENDIF

	PRIVATE_PROCESS_CASINO_HEIST_FINALE_WIDGET_COMMANDS(sClientData)

ENDPROC

#ENDIF

/* ===================================================================================== *\
			BOARD INITIALISATION
\* ===================================================================================== */

//GET_UGC_CONTENT_STRUCT 	sGetContentData
//INT iContentDataBS

FUNC BOOL INITIALISE_CASINO_HEIST_FINALE_BOARD(CASINO_HEIST_CLIENT_DATA& sClientData)
	IF NOT IS_BIT_SET(sClientData.sFinaleData.iBitSet, ciCASINO_HEIST_FINALE_BOARD_BITSET__SETUP)
		IF REGISTER_AND_LINK_RENDER_TARGET(	GET_CASINO_HEIST_BOARD_RENDERTARGET_STRING(CHBT_FINALE), 
											GET_CASINO_HEIST_BOARD_RENDERTARGET_MODEL(CHBT_FINALE))
											
			sClientData.sFinaleData.sScaleformData.iRenderTargetID = GET_RENDER_TARGET_ID(GET_CASINO_HEIST_BOARD_RENDERTARGET_STRING(CHBT_FINALE))
			
//			INT index, iPointer
//			
//			FOR index = 0 TO FMMC_MAX_AT_ONCE_ROCKSTAR_CREATED-1
//				
//				IF g_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[index].iRootContentIdHash = GET_HASH_KEY("FoYo77DO80qcG9x9Dm39oQ")
//					iPointer = index
//					BREAKLOOP
//				ENDIF
//				
//			ENDFOR
		
//			IF NOT FMMC_LOAD_MISSION_DATA(	sGetContentData, 
//											g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iPointer].tlName, 
//											iContentDataBS, 
//											g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iPointer].iType, 
//											FALSE, 
//											TRUE, 
//											FALSE, 
//											FALSE, 
//											TRUE, 
//											FALSE,
//											FALSE, 
//											TRUE,
//											FALSE,
//											FALSE)
//					
//				CDEBUG3LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | INITIALISE_CASINO_HEIST_FINALE_BOARD - Downloading mission data for contentId: ", g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iPointer].tlName)
//				RETURN FALSE
//			ENDIF
			
			sClientData.sFinaleData.sPositionData.vBoardPosition = GET_CASINO_HEIST_BOARD_POSITION(CHBT_SETUP)
			
			PUBLIC_CASINO_HEIST_BOARD__SET_CAMERA_POSITION_ID(sClientData, CHCAM_FINALE_MAIN)
			sClientData.sCameraData.vCamInitialOffset = << 1.0, -2.5, -1.2 >>
			sClientData.sCameraData.vCamInitialOffsetRot = << 0.0, 0.0, 0.0 >>
			
			sClientData.sCameraData.vBaseRotation.X = 0.0
			sClientData.sCameraData.vBaseRotation.Y = 0.0
			sClientData.sCameraData.vBaseRotation.Z = -180.0
			
			IF NOT CAN_PLAYER_USE_THIS_CASINO_HEIST_PLANNING_BOARD(sClientData, CHBT_FINALE)
				sClientData.sFinaleData.sScaleformData.bDrawBoard = FALSE
				sClientData.sFinaleData.sScaleformData.bDrawInstructions = FALSE
				CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | INITIALISE_CASINO_HEIST_FINALE_BOARD - Player cannot use the finale board")
			ENDIF
			
			
			IF NETWORK_IS_ACTIVITY_SESSION()
			
				sClientData.sFinaleData.iEntranceDisplaying = ENUM_TO_INT(CASINO_HEIST_ACCESS_POINT__NONE)
				sClientData.sFinaleData.iEntranceDisplayingCached = ENUM_TO_INT(CASINO_HEIST_ACCESS_POINT__NONE)
				sClientData.sFinaleData.iExitDisplaying = ENUM_TO_INT(CASINO_HEIST_ACCESS_POINT__NONE)
				sClientData.sFinaleData.iExitDisplayingCached = ENUM_TO_INT(CASINO_HEIST_ACCESS_POINT__NONE)
				sClientData.sFinaleData.iDropOffDisplaying = ENUM_TO_INT(CASINO_HEIST_DROPOFF_LOCATION__NONE)
				sClientData.sFinaleData.iDropOffDisplayingCached = ENUM_TO_INT(CASINO_HEIST_DROPOFF_LOCATION__NONE)
				sClientData.sFinaleData.iOutfitInDisplaying = ENUM_TO_INT(CASINO_HEIST_OUTFIT_IN__NONE)
				sClientData.sFinaleData.iOutfitInDisplayingCached = ENUM_TO_INT(CASINO_HEIST_OUTFIT_IN__NONE)
				sClientData.sFinaleData.iOutfitOutDisplaying = ENUM_TO_INT(CASINO_HEIST_OUTFIT_OUT__NONE)
				sClientData.sFinaleData.iOutfitOutDisplayingCached = ENUM_TO_INT(CASINO_HEIST_OUTFIT_OUT__NONE)
			
			ELSE
			
				sClientData.sFinaleData.iEntranceDisplaying = ENUM_TO_INT(CASINO_HEIST_ACCESS_POINT__MAIN_ENTRANCE)
				sClientData.sFinaleData.iEntranceDisplayingCached = ENUM_TO_INT(CASINO_HEIST_ACCESS_POINT__MAIN_ENTRANCE)
				sClientData.sFinaleData.iExitDisplaying = ENUM_TO_INT(CASINO_HEIST_ACCESS_POINT__MAIN_ENTRANCE)
				sClientData.sFinaleData.iExitDisplayingCached = ENUM_TO_INT(CASINO_HEIST_ACCESS_POINT__MAIN_ENTRANCE)
				sClientData.sFinaleData.iDropOffDisplaying = ENUM_TO_INT(CASINO_HEIST_DROPOFF_LOCATION__SHORT_1)
				sClientData.sFinaleData.iDropOffDisplayingCached = ENUM_TO_INT(CASINO_HEIST_DROPOFF_LOCATION__SHORT_1)
				IF HAS_PLAYER_ACQUIRED_CASINO_HEIST_OUTFIT(sClientData.piLeader, CASINO_HEIST_OUTFIT_IN__BUGSTAR)
					sClientData.sFinaleData.iOutfitInDisplaying = ENUM_TO_INT(CASINO_HEIST_OUTFIT_IN__BUGSTAR)
					sClientData.sFinaleData.iOutfitInDisplayingCached = ENUM_TO_INT(CASINO_HEIST_OUTFIT_IN__BUGSTAR)
				ELIF HAS_PLAYER_ACQUIRED_CASINO_HEIST_OUTFIT(sClientData.piLeader, CASINO_HEIST_OUTFIT_IN__MECHANIC)
					sClientData.sFinaleData.iOutfitInDisplaying = ENUM_TO_INT(CASINO_HEIST_OUTFIT_IN__MECHANIC)
					sClientData.sFinaleData.iOutfitInDisplayingCached = ENUM_TO_INT(CASINO_HEIST_OUTFIT_IN__MECHANIC)
				ELIF HAS_PLAYER_ACQUIRED_CASINO_HEIST_OUTFIT(sClientData.piLeader, CASINO_HEIST_OUTFIT_IN__GRUPPE_SECHS)
					sClientData.sFinaleData.iOutfitInDisplaying = ENUM_TO_INT(CASINO_HEIST_OUTFIT_IN__MECHANIC)
					sClientData.sFinaleData.iOutfitInDisplayingCached = ENUM_TO_INT(CASINO_HEIST_OUTFIT_IN__MECHANIC)
				ENDIF
				IF HAS_PLAYER_ACQUIRED_CASINO_HEIST_OUTFIT(sClientData.piLeader, CASINO_HEIST_OUTFIT_OUT__SWAT)
					sClientData.sFinaleData.iOutfitOutDisplaying = ENUM_TO_INT(CASINO_HEIST_OUTFIT_OUT__SWAT)
					sClientData.sFinaleData.iOutfitOutDisplayingCached = ENUM_TO_INT(CASINO_HEIST_OUTFIT_OUT__SWAT)
				ELIF HAS_PLAYER_ACQUIRED_CASINO_HEIST_OUTFIT(sClientData.piLeader, CASINO_HEIST_OUTFIT_OUT__FIREMAN)
					sClientData.sFinaleData.iOutfitOutDisplaying = ENUM_TO_INT(CASINO_HEIST_OUTFIT_OUT__FIREMAN)
					sClientData.sFinaleData.iOutfitOutDisplayingCached = ENUM_TO_INT(CASINO_HEIST_OUTFIT_OUT__FIREMAN)
				ENDIF
				sClientData.sFinaleData.sFinaleConfig.bLaunchButtonEnabled = TRUE
			
			ENDIF
			
			CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | INITIALISE_CASINO_HEIST_FINALE_BOARD - Initialisation complete.")
			SET_BIT(sClientData.sFinaleData.iBitSet, ciCASINO_HEIST_FINALE_BOARD_BITSET__SETUP)
		ENDIF
	ENDIF
	
	RETURN IS_BIT_SET(sClientData.sFinaleData.iBitSet, ciCASINO_HEIST_FINALE_BOARD_BITSET__SETUP)
ENDFUNC



/* ===================================================================================== *\
			Methods, Wrappers / Misc functionality.
\* ===================================================================================== */

FUNC BOOL PRIVATE_CASINO_HEIST_FINALE_BOARD__IS_TODO_LIST_UPDATE_REQUIRED(CASINO_HEIST_CLIENT_DATA &sClientData)
	RETURN IS_BIT_SET(sClientData.sFinaleData.iBitSet, ciCASINO_HEIST_FINALE_BOARD_BITSET__UPDATE_TODO_LIST)
ENDFUNC

PROC PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_TODO_LIST_UPDATE_REQUIRED(CASINO_HEIST_CLIENT_DATA &sClientData, BOOL bRequired)
	CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_TODO_LIST_UPDATE_REQUIRED - ", GET_STRING_FROM_BOOL(bRequired))
	IF bRequired
		SET_BIT(sClientData.sFinaleData.iBitSet, ciCASINO_HEIST_FINALE_BOARD_BITSET__UPDATE_TODO_LIST)
	ELSE
		CLEAR_BIT(sClientData.sFinaleData.iBitSet, ciCASINO_HEIST_FINALE_BOARD_BITSET__UPDATE_TODO_LIST)
	ENDIF
ENDPROC

FUNC BOOL PRIVATE_CASINO_HEIST_FINALE_BOARD__IS_OPTIONAL_LIST_UPDATE_REQUIRED(CASINO_HEIST_CLIENT_DATA &sClientData)
	RETURN IS_BIT_SET(sClientData.sFinaleData.iBitSet, ciCASINO_HEIST_FINALE_BOARD_BITSET__UPDATE_OPTIONAL_LIST)
ENDFUNC

PROC PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_OPTIONAL_LIST_UPDATE_REQUIRED(CASINO_HEIST_CLIENT_DATA &sClientData, BOOL bRequired)
	CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_OPTIONAL_LIST_UPDATE_REQUIRED - ", GET_STRING_FROM_BOOL(bRequired))
	IF bRequired
		SET_BIT(sClientData.sFinaleData.iBitSet, ciCASINO_HEIST_FINALE_BOARD_BITSET__UPDATE_OPTIONAL_LIST)
	ELSE
		CLEAR_BIT(sClientData.sFinaleData.iBitSet, ciCASINO_HEIST_FINALE_BOARD_BITSET__UPDATE_OPTIONAL_LIST)
	ENDIF
ENDPROC

FUNC BOOL PRIVATE_CASINO_HEIST_FINALE_BOARD__IS_CREW_MEMBER_DETAILS_UPDATE_REQUIRED(CASINO_HEIST_CLIENT_DATA &sClientData)
	RETURN IS_BIT_SET(sClientData.sFinaleData.iBitSet, ciCASINO_HEIST_FINALE_BOARD_BITSET__UPDATE_CREW_MEMBER_DETAILS)
ENDFUNC

PROC PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_CREW_MEMBER_DETAILS_UPDATE_REQUIRED(CASINO_HEIST_CLIENT_DATA &sClientData, BOOL bRequired)
	CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_CREW_MEMBER_DETAILS_UPDATE_REQUIRED - ", GET_STRING_FROM_BOOL(bRequired))
	IF bRequired
		SET_BIT(sClientData.sFinaleData.iBitSet, ciCASINO_HEIST_FINALE_BOARD_BITSET__UPDATE_CREW_MEMBER_DETAILS)
	ELSE
		CLEAR_BIT(sClientData.sFinaleData.iBitSet, ciCASINO_HEIST_FINALE_BOARD_BITSET__UPDATE_CREW_MEMBER_DETAILS)
	ENDIF
ENDPROC

FUNC BOOL PRIVATE_CASINO_HEIST_FINALE_BOARD__IS_CREW_MEMBER_PANEL_VISIBLE_UPDATE_REQUIRED(CASINO_HEIST_CLIENT_DATA &sClientData)
	RETURN IS_BIT_SET(sClientData.sFinaleData.iBitSet, ciCASINO_HEIST_FINALE_BOARD_BITSET__UPDATE_CREW_MEMBER_PANEL_VISIBLE)
ENDFUNC

PROC PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_CREW_MEMBER_PANEL_VISIBLE_UPDATE_REQUIRED(CASINO_HEIST_CLIENT_DATA &sClientData, BOOL bRequired)
	CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_CREW_MEMBER_PANEL_VISIBLE_UPDATE_REQUIRED - ", GET_STRING_FROM_BOOL(bRequired))
	IF bRequired
		SET_BIT(sClientData.sFinaleData.iBitSet, ciCASINO_HEIST_FINALE_BOARD_BITSET__UPDATE_CREW_MEMBER_PANEL_VISIBLE)
	ELSE
		CLEAR_BIT(sClientData.sFinaleData.iBitSet, ciCASINO_HEIST_FINALE_BOARD_BITSET__UPDATE_CREW_MEMBER_PANEL_VISIBLE)
	ENDIF
ENDPROC

FUNC BOOL PRIVATE_CASINO_HEIST_FINALE_BOARD__IS_HEADING_UPDATE_REQUIRED(CASINO_HEIST_CLIENT_DATA &sClientData)
	RETURN IS_BIT_SET(sClientData.sFinaleData.iBitSet, ciCASINO_HEIST_FINALE_BOARD_BITSET__UPDATE_HEADING)
ENDFUNC

PROC PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_HEADING_UPDATE_REQUIRED(CASINO_HEIST_CLIENT_DATA &sClientData, BOOL bRequired)
	CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_HEADING_UPDATE_REQUIRED - ", GET_STRING_FROM_BOOL(bRequired))
	IF bRequired
		SET_BIT(sClientData.sFinaleData.iBitSet, ciCASINO_HEIST_FINALE_BOARD_BITSET__UPDATE_HEADING)
	ELSE
		CLEAR_BIT(sClientData.sFinaleData.iBitSet, ciCASINO_HEIST_FINALE_BOARD_BITSET__UPDATE_HEADING)
	ENDIF
ENDPROC

FUNC BOOL PRIVATE_CASINO_HEIST_FINALE_BOARD__IS_BUTTON_VISIBILITY_UPDATE_REQUIRED(CASINO_HEIST_CLIENT_DATA &sClientData)
	RETURN IS_BIT_SET(sClientData.sFinaleData.iBitSet, ciCASINO_HEIST_FINALE_BOARD_BITSET__UPDATE_BUTTON_VISIBILITY)
ENDFUNC

PROC PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_BUTTON_VISIBILITY_UPDATE_REQUIRED(CASINO_HEIST_CLIENT_DATA &sClientData, BOOL bRequired)
	CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_BUTTON_VISIBILITY_UPDATE_REQUIRED - ", GET_STRING_FROM_BOOL(bRequired))
	IF bRequired
		SET_BIT(sClientData.sFinaleData.iBitSet, ciCASINO_HEIST_FINALE_BOARD_BITSET__UPDATE_BUTTON_VISIBILITY)
	ELSE
		CLEAR_BIT(sClientData.sFinaleData.iBitSet, ciCASINO_HEIST_FINALE_BOARD_BITSET__UPDATE_BUTTON_VISIBILITY)
	ENDIF
ENDPROC

FUNC BOOL PRIVATE_CASINO_HEIST_FINALE_BOARD__IS_ENTRANCE_BUTTON_UPDATE_REQUIRED(CASINO_HEIST_CLIENT_DATA &sClientData)
	RETURN IS_BIT_SET(sClientData.sFinaleData.iBitSet, ciCASINO_HEIST_FINALE_BOARD_BITSET__UPDATE_ENTRANCE_BUTTON)
ENDFUNC

PROC PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_ENTRANCE_BUTTON_UPDATE_REQUIRED(CASINO_HEIST_CLIENT_DATA &sClientData, BOOL bRequired)
	CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_ENTRANCE_BUTTON_UPDATE_REQUIRED - ", GET_STRING_FROM_BOOL(bRequired))
	IF bRequired
		SET_BIT(sClientData.sFinaleData.iBitSet, ciCASINO_HEIST_FINALE_BOARD_BITSET__UPDATE_ENTRANCE_BUTTON)
	ELSE
		CLEAR_BIT(sClientData.sFinaleData.iBitSet, ciCASINO_HEIST_FINALE_BOARD_BITSET__UPDATE_ENTRANCE_BUTTON)
	ENDIF
ENDPROC

FUNC BOOL PRIVATE_CASINO_HEIST_FINALE_BOARD__IS_EXIT_BUTTON_UPDATE_REQUIRED(CASINO_HEIST_CLIENT_DATA &sClientData)
	RETURN IS_BIT_SET(sClientData.sFinaleData.iBitSet, ciCASINO_HEIST_FINALE_BOARD_BITSET__UPDATE_EXIT_BUTTON)
ENDFUNC

PROC PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_EXIT_BUTTON_UPDATE_REQUIRED(CASINO_HEIST_CLIENT_DATA &sClientData, BOOL bRequired)
	CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_EXIT_BUTTON_UPDATE_REQUIRED - ", GET_STRING_FROM_BOOL(bRequired))
	IF bRequired
		SET_BIT(sClientData.sFinaleData.iBitSet, ciCASINO_HEIST_FINALE_BOARD_BITSET__UPDATE_EXIT_BUTTON)
	ELSE
		CLEAR_BIT(sClientData.sFinaleData.iBitSet, ciCASINO_HEIST_FINALE_BOARD_BITSET__UPDATE_EXIT_BUTTON)
	ENDIF
ENDPROC

FUNC BOOL PRIVATE_CASINO_HEIST_FINALE_BOARD__IS_DROP_OFF_BUTTON_UPDATE_REQUIRED(CASINO_HEIST_CLIENT_DATA &sClientData)
	RETURN IS_BIT_SET(sClientData.sFinaleData.iBitSet, ciCASINO_HEIST_FINALE_BOARD_BITSET__UPDATE_DROP_OFF_BUTTON)
ENDFUNC

PROC PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_DROP_OFF_BUTTON_UPDATE_REQUIRED(CASINO_HEIST_CLIENT_DATA &sClientData, BOOL bRequired)
	CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_DROP_OFF_BUTTON_UPDATE_REQUIRED - ", GET_STRING_FROM_BOOL(bRequired))
	IF bRequired
		SET_BIT(sClientData.sFinaleData.iBitSet, ciCASINO_HEIST_FINALE_BOARD_BITSET__UPDATE_DROP_OFF_BUTTON)
	ELSE
		CLEAR_BIT(sClientData.sFinaleData.iBitSet, ciCASINO_HEIST_FINALE_BOARD_BITSET__UPDATE_DROP_OFF_BUTTON)
	ENDIF
ENDPROC

FUNC BOOL PRIVATE_CASINO_HEIST_FINALE_BOARD__IS_OUTFIT_IN_BUTTON_UPDATE_REQUIRED(CASINO_HEIST_CLIENT_DATA &sClientData)
	RETURN IS_BIT_SET(sClientData.sFinaleData.iBitSet, ciCASINO_HEIST_FINALE_BOARD_BITSET__UPDATE_OUTFIT_IN_BUTTON)
ENDFUNC

PROC PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_OUTFIT_IN_BUTTON_UPDATE_REQUIRED(CASINO_HEIST_CLIENT_DATA &sClientData, BOOL bRequired)
	CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_OUTFIT_IN_BUTTON_UPDATE_REQUIRED - ", GET_STRING_FROM_BOOL(bRequired))
	IF bRequired
		SET_BIT(sClientData.sFinaleData.iBitSet, ciCASINO_HEIST_FINALE_BOARD_BITSET__UPDATE_OUTFIT_IN_BUTTON)
	ELSE
		CLEAR_BIT(sClientData.sFinaleData.iBitSet, ciCASINO_HEIST_FINALE_BOARD_BITSET__UPDATE_OUTFIT_IN_BUTTON)
	ENDIF
ENDPROC

FUNC BOOL PRIVATE_CASINO_HEIST_FINALE_BOARD__IS_OUTFIT_OUT_BUTTON_UPDATE_REQUIRED(CASINO_HEIST_CLIENT_DATA &sClientData)
	RETURN IS_BIT_SET(sClientData.sFinaleData.iBitSet, ciCASINO_HEIST_FINALE_BOARD_BITSET__UPDATE_OUTFIT_OUT_BUTTON)
ENDFUNC

PROC PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_OUTFIT_OUT_BUTTON_UPDATE_REQUIRED(CASINO_HEIST_CLIENT_DATA &sClientData, BOOL bRequired)
	CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_OUTFIT_OUT_BUTTON_UPDATE_REQUIRED - ", GET_STRING_FROM_BOOL(bRequired))
	IF bRequired
		SET_BIT(sClientData.sFinaleData.iBitSet, ciCASINO_HEIST_FINALE_BOARD_BITSET__UPDATE_OUTFIT_OUT_BUTTON)
	ELSE
		CLEAR_BIT(sClientData.sFinaleData.iBitSet, ciCASINO_HEIST_FINALE_BOARD_BITSET__UPDATE_OUTFIT_OUT_BUTTON)
	ENDIF
ENDPROC

FUNC BOOL PRIVATE_CASINO_HEIST_FINALE_BOARD__IS_WEAPON_BUTTON_UPDATE_REQUIRED(CASINO_HEIST_CLIENT_DATA &sClientData)
	RETURN IS_BIT_SET(sClientData.sFinaleData.iBitSet, ciCASINO_HEIST_FINALE_BOARD_BITSET__UPDATE_WEAPON_BUTTON)
ENDFUNC

PROC PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_WEAPON_BUTTON_UPDATE_REQUIRED(CASINO_HEIST_CLIENT_DATA &sClientData, BOOL bRequired)
	CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_WEAPON_BUTTON_UPDATE_REQUIRED - ", GET_STRING_FROM_BOOL(bRequired))
	IF bRequired
		SET_BIT(sClientData.sFinaleData.iBitSet, ciCASINO_HEIST_FINALE_BOARD_BITSET__UPDATE_WEAPON_BUTTON)
	ELSE
		CLEAR_BIT(sClientData.sFinaleData.iBitSet, ciCASINO_HEIST_FINALE_BOARD_BITSET__UPDATE_WEAPON_BUTTON)
	ENDIF
ENDPROC

FUNC BOOL PRIVATE_CASINO_HEIST_FINALE_BOARD__IS_VEHICLE_BUTTON_UPDATE_REQUIRED(CASINO_HEIST_CLIENT_DATA &sClientData)
	RETURN IS_BIT_SET(sClientData.sFinaleData.iBitSet, ciCASINO_HEIST_FINALE_BOARD_BITSET__UPDATE_VEHICLE_BUTTON)
ENDFUNC

PROC PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_VEHICLE_BUTTON_UPDATE_REQUIRED(CASINO_HEIST_CLIENT_DATA &sClientData, BOOL bRequired)
	CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_VEHICLE_BUTTON_UPDATE_REQUIRED - ", GET_STRING_FROM_BOOL(bRequired))
	IF bRequired
		SET_BIT(sClientData.sFinaleData.iBitSet, ciCASINO_HEIST_FINALE_BOARD_BITSET__UPDATE_VEHICLE_BUTTON)
	ELSE
		CLEAR_BIT(sClientData.sFinaleData.iBitSet, ciCASINO_HEIST_FINALE_BOARD_BITSET__UPDATE_VEHICLE_BUTTON)
	ENDIF
ENDPROC

FUNC BOOL PRIVATE_CASINO_HEIST_FINALE_BOARD__IS_HACKING_BUTTON_UPDATE_REQUIRED(CASINO_HEIST_CLIENT_DATA &sClientData)
	RETURN IS_BIT_SET(sClientData.sFinaleData.iBitSet, ciCASINO_HEIST_FINALE_BOARD_BITSET__UPDATE_HACKING_BUTTON)
ENDFUNC

PROC PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_HACKING_BUTTON_UPDATE_REQUIRED(CASINO_HEIST_CLIENT_DATA &sClientData, BOOL bRequired)
	CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_HACKING_BUTTON_UPDATE_REQUIRED - ", GET_STRING_FROM_BOOL(bRequired))
	IF bRequired
		SET_BIT(sClientData.sFinaleData.iBitSet, ciCASINO_HEIST_FINALE_BOARD_BITSET__UPDATE_HACKING_BUTTON)
	ELSE
		CLEAR_BIT(sClientData.sFinaleData.iBitSet, ciCASINO_HEIST_FINALE_BOARD_BITSET__UPDATE_HACKING_BUTTON)
	ENDIF
ENDPROC


///    Clean the headshot out of memory.
/// PARAMS:
///    index - Player whose headshot is getting cleaned up.
PROC PRIVATE_CLEAN_SINGLE_CASINO_HEIST_FINALE_HEADSHOT(CASINO_HEIST_PED_HEADSHOT &sHeadshotData, BOOL bUnregister = TRUE)

	CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "Headshots - Cleaning specific player's headshot: ", GET_PLAYER_NAME(sHeadshotData.playerID))

	IF bUnregister
		IF sHeadshotData.eHeadshotState = CHHS_REQUESTING
		OR sHeadshotData.eHeadshotState = CHHS_AVAILABLE
			IF IS_PEDHEADSHOT_VALID(sHeadshotData.pedHeadshotID)
				UNREGISTER_PEDHEADSHOT(sHeadshotData.pedHeadshotID)
			ENDIF
		ENDIF
	ENDIF
	
	sHeadshotData.playerID = INVALID_PLAYER_INDEX()
	sHeadshotData.eHeadshotState = CHHS_NULL
	sHeadshotData.pedHeadshotID = NULL
	sHeadshotData.sHeadshotText = "NULL"
ENDPROC

PROC PRIVATE_CLEAN_ALL_CASINO_HEIST_FINALE_HEADSHOTS(CASINO_HEIST_CLIENT_DATA &sClientData)

	INT index

	FOR index = 0 TO (MAX_CASINO_HEIST_PLAYERS-1)
		// Clean the headshot array.
		IF sClientData.sFinaleData.sFinaleConfig.sHeadshotData[index].playerID != INVALID_PLAYER_INDEX()
			PRIVATE_CLEAN_SINGLE_CASINO_HEIST_FINALE_HEADSHOT(sClientData.sFinaleData.sFinaleConfig.sHeadshotData[index], TRUE)
		ENDIF
	ENDFOR

ENDPROC

/// PURPOSE:
///    Find available slot to load headshot into - index corresponds to value passed to scaleform
/// PARAMS:
///    playerID - Player to store ready for a headshot.
/// RETURNS:
///   	INT - The index of the stored player. -1 if it falls through.
FUNC INT PRIVATE_INIT_CASINO_HEIST_FINALE_HEADSHOT_REQUEST(CASINO_HEIST_CLIENT_DATA &sClientData, PLAYER_INDEX playerID)
	
	IF playerID = INVALID_PLAYER_INDEX() 
		RETURN CASINO_HEIST_INVALID
	ENDIF
	
	INT i
	
	REPEAT MAX_CASINO_HEIST_PLAYERS i
		IF sClientData.sFinaleData.sFinaleConfig.sHeadshotData[i].playerID = playerID
			// Player already exists in request array, no need to coninue.
			RETURN i	
		ENDIF
	ENDREPEAT
	
	REPEAT MAX_CASINO_HEIST_PLAYERS i
		IF sClientData.sFinaleData.sFinaleConfig.sHeadshotData[i].playerID = INVALID_PLAYER_INDEX()
			CDEBUG3LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] PRIVATE_INIT_CASINO_HEIST_FINALE_HEADSHOT_REQUEST - Resetting headshot request for player: ", 
				GET_PLAYER_NAME(playerID))
				
			sClientData.sFinaleData.sFinaleConfig.sHeadshotData[i].playerID = playerID
			sClientData.sFinaleData.sFinaleConfig.sHeadshotData[i].eHeadshotState = CHHS_NULL
			RETURN i
		ENDIF
	ENDREPEAT
	
	RETURN CASINO_HEIST_INVALID
ENDFUNC

/// PURPOSE:
///    Check to see if we are already requesting and loading a ped headshot.
/// RETURNS:
///    BOOLEAN - True if requesting or loading.
FUNC BOOL PRIVATE_ARE_ANY_CASINO_HEIST_FINALE_HEADSHOTS_LOADING(CASINO_HEIST_CLIENT_DATA &sClientData)

	INT i
	FOR i = 0 TO (MAX_CASINO_HEIST_PLAYERS-1)
		IF sClientData.sFinaleData.sFinaleConfig.sHeadshotData[i].eHeadshotState = CHHS_REQUESTING
			RETURN TRUE
		ENDIF
	ENDFOR

	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Loads ped headshot into hesitHeadshots array. Script requests texture first before loading it into the scaleform
/// PARAMS:
///    CASINO_HEIST_CLIENT_DATA - General gang ops data struct
///    CASINO_HEIST_PED_HEADSHOT - Specific headshot data to verify
/// RETURNS:
///    BOOLEAN - True when headshot is ready for use. 
FUNC BOOL PRIVATE_IS_CASINO_HEIST_FINALE_HEADSHOT_READY(CASINO_HEIST_CLIENT_DATA &sClientData, CASINO_HEIST_PED_HEADSHOT &sHeadshotData)	

	STRING 	sHeadshotTexture
	INT 	index
	BOOL 	bHasFound = FALSE
	
	SWITCH sHeadshotData.eHeadshotState
		
		CASE CHHS_NULL
		
			IF NOT PRIVATE_ARE_ANY_CASINO_HEIST_FINALE_HEADSHOTS_LOADING(sClientData)
				sHeadshotData.eHeadshotState = CHHS_REQUESTING
				CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] [HEADSHOT] PRIVATE_IS_CASINO_HEIST_FINALE_HEADSHOT_READY - Requesting headshot for player: ", GET_PLAYER_NAME(sHeadshotData.playerID))
			ENDIF
			
		BREAK
		
		// Script must initially request the headshot
		CASE CHHS_REQUESTING
		
			IF IS_PED_INJURED(GET_PLAYER_PED(sHeadshotData.playerID))
				RETURN FALSE
			ENDIF
			
			IF sHeadshotData.pedHeadshotID = NULL
				sHeadshotData.pedHeadshotID = Get_HeadshotID_For_Player(sHeadshotData.playerID)
			ELSE
			
				sHeadshotData.sHeadshotText = GET_PEDHEADSHOT_TXD_STRING(sHeadshotData.pedHeadshotID)					
				
				REPEAT g_numGeneratedHeadshotsMP index
					IF (g_sGeneratedHeadshotsMP[index].hsdPlayerID = sHeadshotData.playerID)
						bHasFound = (sHeadshotData.pedHeadshotID = g_sGeneratedHeadshotsMP[index].hsdHeadshotID)
						BREAKLOOP
					ENDIF
				ENDREPEAT
				
				IF NOT bHasFound
					CDEBUG3LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] [HEADSHOT] PRIVATE_IS_CASINO_HEIST_FINALE_HEADSHOT_READY - Error! Player doesn't have a headshot request in the global request array. Player: ", 
						GET_PLAYER_NAME(sHeadshotData.playerID))
								
					sHeadshotData.pedHeadshotID = NULL
					RETURN FALSE
				ENDIF
				
				IF IS_STRING_NULL_OR_EMPTY(sHeadshotData.sHeadshotText)
				OR ARE_STRINGS_EQUAL(" ", sHeadshotData.sHeadshotText)
					CDEBUG3LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] [HEADSHOT] PRIVATE_IS_CASINO_HEIST_FINALE_HEADSHOT_READY - Error! Headshot system returned an empty string, retrying.")
					sHeadshotData.pedHeadshotID = NULL
					RETURN FALSE
				ENDIF
				
				CDEBUG3LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] [HEADSHOT] PRIVATE_IS_CASINO_HEIST_FINALE_HEADSHOT_READY - Global headshot data has loaded, moving to available.")
				sHeadshotData.eHeadshotState = CHHS_AVAILABLE		
				
				PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_CREW_MEMBER_DETAILS_UPDATE_REQUIRED(sClientData, TRUE)
				
				RETURN TRUE
			ENDIF
			
		BREAK
			
		// Now we can use the headshot
		CASE CHHS_AVAILABLE
	
			sHeadshotTexture = GET_PEDHEADSHOT_TXD_STRING(sHeadshotData.pedHeadshotID)
			sHeadshotData.sHeadshotText = sHeadshotTexture
			CDEBUG3LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] [HEADSHOT] PRIVATE_IS_CASINO_HEIST_FINALE_HEADSHOT_READY - Finished loading headshot, texture string: ", sHeadshotTexture)
			
		RETURN TRUE
			
	ENDSWITCH

	RETURN FALSE
	
ENDFUNC

FUNC BOOL PRIVATE_ARE_ALL_CASINO_HEIST_FINALE_HEADSHOTS_LOADED(CASINO_HEIST_CLIENT_DATA &sClientData)

	INT index
	BOOL bAllReady = TRUE

	FOR index = 0 TO (MAX_CASINO_HEIST_PLAYERS-1)
	
		IF sClientData.sFinaleData.sFinaleConfig.iPlayerOrder[index] = CASINO_HEIST_INVALID
			RELOOP
		ENDIF
		
		IF NOT IS_NET_PLAYER_OK(sClientData.sFinaleData.sFinaleConfig.sHeadshotData[index].playerID)
			RELOOP
		ENDIF
			
		IF sClientData.sFinaleData.sFinaleConfig.sHeadshotData[index].eHeadshotState != CHHS_AVAILABLE
			CDEBUG3LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] [HEADSHOT] PRIVATE_ARE_ALL_CASINO_HEIST_FINALE_HEADSHOTS_LOADED - Client ", index, "'s headshot isn't ready.")
			bAllReady = FALSE
			BREAKLOOP
		ENDIF
		
	ENDFOR
	
	RETURN bAllReady

ENDFUNC

/// PURPOSE:
///    Get headshot for a heist member. All headshots must be laoded fully before we display a single one, so this will
///    return an empty label until that happens.
/// PARAMS:
///    playerID - The member whose headshot to retrieve.
/// RETURNS:
///    Headshot TXD string (already preloaded).
FUNC TEXT_LABEL_23 PRIVATE_GET_CASINO_HEIST_FINALE_HEADSHOT(CASINO_HEIST_CLIENT_DATA &sClientData, PLAYER_INDEX playerID)

	TEXT_LABEL_23 sPortraitSlot
	
	INT index = PRIVATE_INIT_CASINO_HEIST_FINALE_HEADSHOT_REQUEST(sClientData, playerID)

	IF index = CASINO_HEIST_INVALID
		CDEBUG3LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] [HEADSHOT] PRIVATE_GET_CASINO_HEIST_FINALE_HEADSHOT - return 1")
		RETURN sPortraitSlot
	ENDIF
	
	IF sClientData.sFinaleData.sFinaleConfig.sHeadshotData[index].bHeadshotsLoaded
		CDEBUG3LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] [HEADSHOT] PRIVATE_GET_CASINO_HEIST_FINALE_HEADSHOT - return 2")
		RETURN sClientData.sFinaleData.sFinaleConfig.sHeadshotData[index].sHeadshotText 
	ENDIF
	
	IF PRIVATE_IS_CASINO_HEIST_FINALE_HEADSHOT_READY(sClientData, sClientData.sFinaleData.sFinaleConfig.sHeadshotData[index])
		IF IS_BIT_SET(sClientData.iBitSet, CASINO_HEIST_BOARD_BITSET__IN_CORONA)
			IF PRIVATE_ARE_ALL_CASINO_HEIST_FINALE_HEADSHOTS_LOADED(sClientData)				
				sPortraitSlot = sClientData.sFinaleData.sFinaleConfig.sHeadshotData[index].sHeadshotText
				
				IF NOT sClientData.sFinaleData.sFinaleConfig.sHeadshotData[index].bHeadshotsLoaded
					sClientData.sFinaleData.sFinaleConfig.sHeadshotData[index].bHeadshotsLoaded = TRUE
					PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_CREW_MEMBER_DETAILS_UPDATE_REQUIRED(sClientData, TRUE)
					CDEBUG3LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] [HEADSHOT] PRIVATE_GET_CASINO_HEIST_FINALE_HEADSHOT - return 3")
				ENDIF
			ENDIF
		ELSE
			sPortraitSlot = sClientData.sFinaleData.sFinaleConfig.sHeadshotData[index].sHeadshotText
			CDEBUG3LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] [HEADSHOT] PRIVATE_GET_CASINO_HEIST_FINALE_HEADSHOT - return 4")
		ENDIF
	ELSE
		
	ENDIF

	RETURN sPortraitSlot
ENDFUNC

/// PURPOSE:
///    Check the passed player's ready status (net safe).
/// PARAMS:
///    piTarget - Player to check
/// RETURNS:
///    TRUE if READY
FUNC BOOL PRIVATE_IS_CASINO_HEIST_FINALE_PLAYER_READY(CASINO_HEIST_CLIENT_DATA &sClientData, PLAYER_INDEX piTarget)

	INT index
	FOR index = 0 TO (MAX_CASINO_HEIST_PLAYERS-1)
		IF INT_TO_PLAYERINDEX(sClientData.sFinaleData.sFinaleConfig.iPlayerOrder[index]) != piTarget 
			RELOOP
		ENDIF
		
		RETURN sClientData.sFinaleData.sFinaleConfig.iReadyState[index] = CASINO_HEIST_FINALE_READY
	ENDFOR
	
	RETURN FALSE

ENDFUNC

/// PURPOSE:
///    Get the cut percentage for a specific player.
/// PARAMS:
///    index - Index for the player whose cut to get (0 = Leader, etc).
/// RETURNS:
///    INT - Cut percent.
FUNC INT PRIVATE_GET_CASINO_HEIST_FINALE_CUT(CASINO_HEIST_CLIENT_DATA &sClientData, INT index)
	RETURN sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[index]	
ENDFUNC

PROC PRIVATE_UPDATE_CASINO_HEIST_FINALE_CUTS(CASINO_HEIST_CLIENT_DATA &sClientData, INT iTarget, INT iModifier)

	INT iMinimumCut = 15
	INT iMaxCut = 85
	
	IF sClientData.sFinaleData.sFinaleConfig.iTotalPlayers = 1
		iMaxCut = 100
		CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] Rewards - iMaxCut set to 100 as this is a solo job")
	ENDIF
	
	IF iModifier < 0 	
	
		// DECREMENT TARGET, INCREASE POT.
		INT iTargetCut = sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[iTarget]
		IF iTargetCut < ABSI(iModifier)
			iModifier = iTargetCut
		ENDIF
		
		IF sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[iTarget] < iMinimumCut

			CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] Rewards - Cannot decrease target cut! Target already less cut than allowed minimum! Target cut: ", 
				sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[iTarget], ", Minimum: ", iMinimumCut)
			
			// Because the target has somehow ended up with less than the minimum, calculate how much under they are and add it back onto the target.
			INT iRem
			iRem = iMinimumCut - sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[iTarget]
			sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[iTarget] += iRem
			
			CDEBUG3LN(DEBUG_CASINO_HEIST_PLANNING, "Rewards - Added missing remainder: ", iRem, ", to target total: ", 
				(sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[iTarget] - iRem), " to rebalance player cuts.")

			PLAY_SOUND_FRONTEND(-1,"Highlight_Error","DLC_HEIST_PLANNING_BOARD_SOUNDS")

		ELIF sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[iTarget] = iMinimumCut

			CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] Rewards - Cannot decrease target cut, target already at minimum: ", iMinimumCut)
			
			PLAY_SOUND_FRONTEND(-1,"Highlight_Error","DLC_HEIST_PLANNING_BOARD_SOUNDS")
			
		ELSE
		
			CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] Rewards - Decrease target cut. iModifier: ", iModifier)
			
			CDEBUG3LN(DEBUG_CASINO_HEIST_PLANNING, "  ... DECREASE - Pre cut modifications: ")
			CDEBUG3LN(DEBUG_CASINO_HEIST_PLANNING, "  ... Pot(%): 	", sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[MAX_CASINO_HEIST_PLAYERS])
			CDEBUG3LN(DEBUG_CASINO_HEIST_PLANNING, "  ... Target(%): 	", sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[iTarget])
			
			// Validate that the values are legal (i.e. within 0 and 100).
			IF sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[MAX_CASINO_HEIST_PLAYERS] <= (100 - ABSI(iModifier))
				sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[MAX_CASINO_HEIST_PLAYERS] = sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[MAX_CASINO_HEIST_PLAYERS] + ABSI(iModifier)
				sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[iTarget] = sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[iTarget] - ABSI(iModifier)
			ENDIF
		
			CDEBUG3LN(DEBUG_CASINO_HEIST_PLANNING, "  ... DECREASE - Post cut modifications: ")
			CDEBUG3LN(DEBUG_CASINO_HEIST_PLANNING, "  ... Pot(%): 	", sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[MAX_CASINO_HEIST_PLAYERS])
			CDEBUG3LN(DEBUG_CASINO_HEIST_PLANNING, "  ... Target(%): 	", sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[iTarget])
	
			IF iModifier = 0
				PLAY_SOUND_FROM_COORD(-1,"Error", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
			ELSE
				PLAY_SOUND_FRONTEND(-1,"Highlight_Move","DLC_HEIST_PLANNING_BOARD_SOUNDS")
			ENDIF

		ENDIF
		
		IF sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[iTarget] <= iMinimumCut
			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_NOTE_6") // TODO: Replace label
				CLEAR_HELP()
				PRINT_HELP("HEIST_NOTE_6", CASINO_HEIST_FINALE_CUT_WARN_TIME) // TODO: Replace label
			ENDIF
		ENDIF
		
	ELSE 			
		// INCREMENT TARGET, DECREASE POT.
		INT iPotCut = sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[MAX_CASINO_HEIST_PLAYERS]
		IF iPotCut < ABSI(iModifier)
			iModifier = iPotCut
		ENDIF

		CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] Rewards - Increase target cut. iModifier: ", iModifier)
		
		IF sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[iTarget] > iMaxCut

			CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] Rewards - Cannot increase target cut! Already greater cut than allowed maximum! Target cut: ", 
				sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[iTarget], ", Maximum: ", iMaxCut)
			
			// Because the target has somehow ended up with more than the maximum, calculate how much over they are and 
			// add it back onto the pot.
			INT iRem
			iRem = sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[iTarget] - iMaxCut
			sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[MAX_CASINO_HEIST_PLAYERS] += iRem

			CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] Rewards - Added remainder: ", iRem, ", to pot total: ", 
				(sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[MAX_CASINO_HEIST_PLAYERS] - iRem), " to rebalance player cuts.")
			
			PLAY_SOUND_FRONTEND(-1,"Highlight_Error","DLC_HEIST_PLANNING_BOARD_SOUNDS")
			
		ELIF sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[iTarget] = iMaxCut
		
			CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] Rewards - Cannot increase target cut, already at maximum: ", iMaxCut)
			
			PLAY_SOUND_FRONTEND(-1,"Highlight_Error","DLC_HEIST_PLANNING_BOARD_SOUNDS")
			
		ELSE
		
			CDEBUG3LN(DEBUG_CASINO_HEIST_PLANNING, "  ... INCREASE - Pre cut modifications: ")
			CDEBUG3LN(DEBUG_CASINO_HEIST_PLANNING, "  ... Pot(%): 	", sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[MAX_CASINO_HEIST_PLAYERS])
			CDEBUG3LN(DEBUG_CASINO_HEIST_PLANNING, "  ... Target(%): 	", sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[iTarget])
		
			// Validate that the values are legal (i.e. within 0 and 100).
			IF sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[MAX_CASINO_HEIST_PLAYERS] >= ABSI(iModifier)
				sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[MAX_CASINO_HEIST_PLAYERS] = sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[MAX_CASINO_HEIST_PLAYERS] - ABSI(iModifier)
				sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[iTarget] = sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[iTarget] + ABSI(iModifier)
			ENDIF
			
			CDEBUG3LN(DEBUG_CASINO_HEIST_PLANNING, "  ... INCREASE - Post cut modifications: ")
			CDEBUG3LN(DEBUG_CASINO_HEIST_PLANNING, "  ... Pot(%): 	", sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[MAX_CASINO_HEIST_PLAYERS])
			CDEBUG3LN(DEBUG_CASINO_HEIST_PLANNING, "  ... Target(%): 	", sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[iTarget])
			
			IF iModifier = 0
				PLAY_SOUND_FRONTEND(-1,"Highlight_Error","DLC_HEIST_PLANNING_BOARD_SOUNDS")
			ELSE
				PLAY_SOUND_FRONTEND(-1,"Highlight_Move","DLC_HEIST_PLANNING_BOARD_SOUNDS")
			ENDIF
			
		ENDIF
		
		IF sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[iTarget] >= iMaxCut
			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_NOTE_5") // TODO: Replace labels
				CLEAR_HELP()
				PRINT_HELP("HEIST_NOTE_5", CASINO_HEIST_FINALE_CUT_WARN_TIME) // TODO: Replace labels
			ENDIF
		ENDIF
		
	ENDIF


ENDPROC

/// PURPOSE:
///    Reaplce and existing playerOrder array with fresh data.
/// PARAMS:
///    iTargetArray - Old array to replace.
FUNC INT BUILD_PLAYER_ORDER_LIST(CASINO_HEIST_CLIENT_DATA &sClientData)

	IF NOT NETWORK_IS_ACTIVITY_SESSION()
		sClientData.sFinaleData.sFinaleConfig.iPlayerOrder[0] = NATIVE_TO_INT(sClientData.piLeader)
		sClientData.sFinaleData.sFinaleConfig.iPlayerOrder[1] = -1
		sClientData.sFinaleData.sFinaleConfig.iPlayerOrder[2] = -1
		sClientData.sFinaleData.sFinaleConfig.iPlayerOrder[3] = -1
		RETURN 1
	ENDIF

	PLAYER_INDEX thisPlayer = INVALID_PLAYER_INDEX()
	INT index
	
	FOR index = 0 TO (MAX_CASINO_HEIST_PLAYERS-1)
		sClientData.sFinaleData.sFinaleConfig.iPlayerOrder[index] = -1
	ENDFOR
	
	INT iPointer = 0

	FOR index = 0 TO (NUM_NETWORK_PLAYERS-1)
	    thisPlayer = INT_TO_PLAYERINDEX(index)
	    IF (IS_NET_PLAYER_OK(thisPlayer))
			IF thisPlayer <> INVALID_PLAYER_INDEX()
				IF NOT IS_THIS_PLAYER_SCTV_OR_JOINING_AS_SCTV(thisPlayer) 
					IF iPointer > (MAX_CASINO_HEIST_PLAYERS-1)
						CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "Setup - Error! Attempted to add more clients than allowed to planning board.")
					ELSE
						// Add player to the player order array.
				        sClientData.sFinaleData.sFinaleConfig.iPlayerOrder[iPointer] = NATIVE_TO_INT(thisPlayer)
						iPointer++ // Player added to array, manually increment the pointer.
					ENDIF
				ENDIF
		    ENDIF
		ENDIF
	ENDFOR
	
	RETURN iPointer

ENDFUNC

/// PURPOSE:
///    Configure various misc client & server data required for the initialisation of the board.
/// PARAMS:
///    bIsLeader - 
/// RETURNS:
///    TRUE when finished.
FUNC BOOL PRIVATE_HAVE_CONFIGURED_CASINO_HEIST_FINALE_SETUP_DATA(CASINO_HEIST_CLIENT_DATA &sClientData)

	sClientData.sFinaleData.sFinaleConfig.iTotalPlayers = BUILD_PLAYER_ORDER_LIST(sClientData)

	IF sClientData.piLeader != PLAYER_ID()
		RETURN TRUE
	ENDIF
	
	SWITCH sClientData.sFinaleData.sFinaleConfig.iTotalPlayers
		CASE 1
			sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[0] = 100
			sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[1] = 0
			sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[2] = 0
			sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[3] = 0
			sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[4] = 0	// zero the kitty
		BREAK
		CASE 2
			sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[0] = 85
			sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[1] = 15
			sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[2] = 0
			sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[3] = 0
			sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[4] = 0	// zero the kitty
		BREAK
		CASE 3
			sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[0] = 70
			sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[1] = 15
			sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[2] = 15
			sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[3] = 0
			sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[4] = 0	// zero the kitty
		BREAK
		
		CASE 4
			sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[0] = 55
			sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[1] = 15
			sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[2] = 15
			sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[3] = 15
			sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[4] = 0	// zero the kitty
		BREAK
	ENDSWITCH
	
	RETURN TRUE

ENDFUNC

FUNC CASINO_HEIST_FINALE_BOARD_ITEMS PRIVATE_GET_CREW_MEMBER_BOARD_ITEM(INT index)
	SWITCH index
		CASE 0		RETURN CASINO_HEIST_FINALE_BOARD_ITEM__CREW_MEMBER_1
		CASE 1		RETURN CASINO_HEIST_FINALE_BOARD_ITEM__CREW_MEMBER_2
		CASE 2		RETURN CASINO_HEIST_FINALE_BOARD_ITEM__CREW_MEMBER_3
		CASE 3		RETURN CASINO_HEIST_FINALE_BOARD_ITEM__CREW_MEMBER_4
	ENDSWITCH
	RETURN CASINO_HEIST_FINALE_BOARD_ITEM__INVALID
ENDFUNC

FUNC BOOL PRIVATE_SHOULD_CREW_MEMBER_BUTTON_BE_VISIBLE(CASINO_HEIST_CLIENT_DATA &sClientData, INT index)
	IF index = 0
	AND NOT NETWORK_IS_ACTIVITY_SESSION()
		RETURN TRUE
	ENDIF
	IF sClientData.sFinaleData.sFinaleConfig.iPlayerOrder[index] = CASINO_HEIST_INVALID
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

FUNC BOOL PRIVATE_SHOULD_FINALE_BOARD_ITEM_BE_VISIBLE(CASINO_HEIST_CLIENT_DATA &sClientData, CASINO_HEIST_FINALE_BOARD_ITEMS eBoardItem)
	SWITCH eBoardItem
		CASE CASINO_HEIST_FINALE_BOARD_ITEM__LAUNCH_HEIST_BUTTON
			IF sClientData.piLeader = PLAYER_ID()
				IF sClientData.sFinaleData.sFinaleConfig.bLaunchButtonEnabled = FALSE
					RETURN FALSE
				ENDIF
			ELSE
				RETURN FALSE
			ENDIF
		BREAK
		CASE CASINO_HEIST_FINALE_BOARD_ITEM__OUTFIT_IN
			SWITCH GET_PLAYER_CASINO_HEIST_APPROACH(sClientData.piLeader) 
				CASE CASINO_HEIST_APPROACH_TYPE__STEALTH
				CASE CASINO_HEIST_APPROACH_TYPE__DIRECT
					RETURN FALSE
			ENDSWITCH
		BREAK
		CASE CASINO_HEIST_FINALE_BOARD_ITEM__OUTFIT_OUT
			SWITCH GET_PLAYER_CASINO_HEIST_APPROACH(sClientData.piLeader) 
				CASE CASINO_HEIST_APPROACH_TYPE__STEALTH
				CASE CASINO_HEIST_APPROACH_TYPE__DIRECT
					RETURN FALSE
					
				CASE CASINO_HEIST_APPROACH_TYPE__SUBTERFUGE
					RETURN HAS_PLAYER_ACQUIRED_ANY_CASINO_HEIST_OUTFIT_OUT(sClientData.piLeader)
			ENDSWITCH
		BREAK
		
		CASE CASINO_HEIST_FINALE_BOARD_ITEM__CREW_MEMBER_1
			RETURN PRIVATE_SHOULD_CREW_MEMBER_BUTTON_BE_VISIBLE(sClientData, 0)
		CASE CASINO_HEIST_FINALE_BOARD_ITEM__CREW_MEMBER_2
			IF NOT NETWORK_IS_ACTIVITY_SESSION()
				RETURN FALSE
			ENDIF
			RETURN PRIVATE_SHOULD_CREW_MEMBER_BUTTON_BE_VISIBLE(sClientData, 1)
		CASE CASINO_HEIST_FINALE_BOARD_ITEM__CREW_MEMBER_3
			IF NOT NETWORK_IS_ACTIVITY_SESSION()
				RETURN FALSE
			ENDIF
			RETURN PRIVATE_SHOULD_CREW_MEMBER_BUTTON_BE_VISIBLE(sClientData, 2)
		CASE CASINO_HEIST_FINALE_BOARD_ITEM__CREW_MEMBER_4
			IF NOT NETWORK_IS_ACTIVITY_SESSION()
				RETURN FALSE
			ENDIF
			RETURN PRIVATE_SHOULD_CREW_MEMBER_BUTTON_BE_VISIBLE(sClientData, 3)
	ENDSWITCH
	RETURN TRUE
ENDFUNC

FUNC INT PRIVATE_GET_CREW_MEMBER_SLOT(CASINO_HEIST_FINALE_BOARD_ITEMS eBoardItem)
	SWITCH eBoardItem
		CASE CASINO_HEIST_FINALE_BOARD_ITEM__CREW_MEMBER_1		RETURN 0
		CASE CASINO_HEIST_FINALE_BOARD_ITEM__CREW_MEMBER_2		RETURN 1
		CASE CASINO_HEIST_FINALE_BOARD_ITEM__CREW_MEMBER_3		RETURN 2
		CASE CASINO_HEIST_FINALE_BOARD_ITEM__CREW_MEMBER_4		RETURN 3
	ENDSWITCH
	RETURN CASINO_HEIST_INVALID
ENDFUNC

FUNC INT PRIVATE_GET_SUPPORT_CREW_CUT(CASINO_HEIST_CLIENT_DATA &sClientData)
	INT iTake = GET_CASINO_HEIST_TARGET_VALUE(GET_PLAYER_CASINO_HEIST_TARGET(sClientData.piLeader))
	
	IF GET_PLAYER_CASINO_HEIST_APPROACH(sClientData.piLeader) = GET_PLAYER_CASINO_HEIST_HARD_APPROACH(sClientData.piLeader)
		iTake = ROUND(iTake * GET_CASINO_HEIST_TARGET_VALUE_DIFFICULTY_MODIFIER(DIFF_HARD))
	ENDIF
	
	INT iCut
	
	iCut += GET_CASINO_HEIST_GUNMAN_CUT(GET_PLAYER_CASINO_HEIST_WEAPONS_EXPERT(sClientData.piLeader))
	iCut += GET_CASINO_HEIST_DRIVER_CUT(GET_PLAYER_CASINO_HEIST_DRIVER(sClientData.piLeader))
	iCut += GET_CASINO_HEIST_HACKER_CUT(GET_PLAYER_CASINO_HEIST_HACKER(sClientData.piLeader))
	iCut += g_sMPTunables.iCH_LESTER_CUT // Lester
	
	CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_GET_SUPPORT_CREW_CUT - target value: $", iTake)
	CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_GET_SUPPORT_CREW_CUT - crew/Lester deductions: ", iCut, "%")
	
	iCut = ((iTake/100) * iCut)

	CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_GET_SUPPORT_CREW_CUT - cut: $", iCut)

	RETURN iCut
ENDFUNC

FUNC INT PRIVATE_GET_POTENTIAL_TAKE(CASINO_HEIST_CLIENT_DATA &sClientData)

	INT iTake = GET_CASINO_HEIST_TARGET_VALUE(GET_PLAYER_CASINO_HEIST_TARGET(sClientData.piLeader))
	
	IF GET_PLAYER_CASINO_HEIST_APPROACH(sClientData.piLeader) = GET_PLAYER_CASINO_HEIST_HARD_APPROACH(sClientData.piLeader)
		iTake = ROUND(iTake * GET_CASINO_HEIST_TARGET_VALUE_DIFFICULTY_MODIFIER(DIFF_HARD))
	ENDIF
	
	IF NETWORK_IS_ACTIVITY_SESSION()
		FLOAT fModifier = 1.0
		SWITCH INT_TO_ENUM(CASINO_HEIST_DROPOFF_LOCATIONS, sClientData.sFinaleData.iDropOffDisplaying)
			CASE CASINO_HEIST_DROPOFF_LOCATION__SHORT_1
			CASE CASINO_HEIST_DROPOFF_LOCATION__SHORT_2
			CASE CASINO_HEIST_DROPOFF_LOCATION__SHORT_3
				fModifier = g_sMPTunables.fCH_BUYER_MOD_SHORT
			BREAK
			CASE CASINO_HEIST_DROPOFF_LOCATION__MEDIUM_1
			CASE CASINO_HEIST_DROPOFF_LOCATION__MEDIUM_2
			CASE CASINO_HEIST_DROPOFF_LOCATION__MEDIUM_3
				fModifier = g_sMPTunables.fCH_BUYER_MOD_MED
			BREAK
			CASE CASINO_HEIST_DROPOFF_LOCATION__LONG_1
			CASE CASINO_HEIST_DROPOFF_LOCATION__LONG_2
			CASE CASINO_HEIST_DROPOFF_LOCATION__LONG_3
				fModifier = g_sMPTunables.fCH_BUYER_MOD_LONG
			BREAK
		ENDSWITCH
	
		CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_GET_POTENTIAL_TAKE - drop off modifier: ", fModifier)
	
		iTake = ROUND(iTake*fModifier)
	ENDIF

	CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_GET_POTENTIAL_TAKE - take: $", iTake)

	RETURN iTake
ENDFUNC

FUNC STRING PRIVATE_GET_ACCESS_POINT_LABEL(CASINO_HEIST_CLIENT_DATA &sClientData, CASINO_HEIST_ACCESS_POINT eAccessPoint)
	UNUSED_PARAMETER(sClientData)
	SWITCH eAccessPoint
		CASE CASINO_HEIST_ACCESS_POINT__MAIN_ENTRANCE								RETURN "CH_ACCPNT_0"
		CASE CASINO_HEIST_ACCESS_POINT__STAFF_ENTRANCE								RETURN "CH_ACCPNT_1"
		CASE CASINO_HEIST_ACCESS_POINT__GARBAGE_ENTRANCE							RETURN "CH_ACCPNT_2"
		CASE CASINO_HEIST_ACCESS_POINT__SOUTH_WEST_ROOF_TERRACE_ENTRANCE			RETURN "CH_ACCPNT_3"
		CASE CASINO_HEIST_ACCESS_POINT__NORTH_WEST_ROOF_TERRACE_ENTRANCE			RETURN "CH_ACCPNT_4"
		CASE CASINO_HEIST_ACCESS_POINT__SOUTH_EAST_ROOF_TERRACE_ENTRANCE			RETURN "CH_ACCPNT_5"
		CASE CASINO_HEIST_ACCESS_POINT__NORTH_EAST_ROOF_TERRACE_ENTRANCE			RETURN "CH_ACCPNT_6"
		CASE CASINO_HEIST_ACCESS_POINT__SOUTH_HELIPAD_ENTRANCE						RETURN "CH_ACCPNT_7"
		CASE CASINO_HEIST_ACCESS_POINT__NORTH_HELIPAD_ENTRANCE						RETURN "CH_ACCPNT_8"
		CASE CASINO_HEIST_ACCESS_POINT__SECURITY_VEHICLE_ENTRANCE					RETURN "CH_ACCPNT_9"
		CASE CASINO_HEIST_ACCESS_POINT__SEWER_ENTRANCE								RETURN "CH_ACCPNT_10"
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC STRING PRIVATE_GET_DROP_OFF_LABEL(CASINO_HEIST_DROPOFF_LOCATIONS eDropOff)
	SWITCH eDropOff
		CASE CASINO_HEIST_DROPOFF_LOCATION__SHORT_1
		CASE CASINO_HEIST_DROPOFF_LOCATION__SHORT_2
		CASE CASINO_HEIST_DROPOFF_LOCATION__SHORT_3
			RETURN "CH_DROPOFF_0"
		CASE CASINO_HEIST_DROPOFF_LOCATION__MEDIUM_1
		CASE CASINO_HEIST_DROPOFF_LOCATION__MEDIUM_2
		CASE CASINO_HEIST_DROPOFF_LOCATION__MEDIUM_3
			RETURN "CH_DROPOFF_1"
		CASE CASINO_HEIST_DROPOFF_LOCATION__LONG_1
		CASE CASINO_HEIST_DROPOFF_LOCATION__LONG_2
		CASE CASINO_HEIST_DROPOFF_LOCATION__LONG_3
			RETURN "CH_DROPOFF_2"
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC STRING PRIVATE_GET_OUTFIT_LABEL(CASINO_HEIST_OUTFITS eOutfit)
	SWITCH eOutfit
		CASE CASINO_HEIST_OUTFIT_IN__NONE
		CASE CASINO_HEIST_OUTFIT_OUT__NONE
			RETURN ""
		CASE CASINO_HEIST_OUTFIT_IN__BUGSTAR
			RETURN "CH_OUTFIT_0"
		CASE CASINO_HEIST_OUTFIT_IN__MECHANIC
			RETURN "CH_OUTFIT_1"
		CASE CASINO_HEIST_OUTFIT_IN__GRUPPE_SECHS
			RETURN "CH_OUTFIT_2"
		CASE CASINO_HEIST_OUTFIT_IN__CELEBRITY
			RETURN "CH_OUTFIT_3"
		CASE CASINO_HEIST_OUTFIT_OUT__SWAT
			RETURN "CH_OUTFIT_4"
		CASE CASINO_HEIST_OUTFIT_OUT__FIREMAN
			RETURN "CH_OUTFIT_5"
		CASE CASINO_HEIST_OUTFIT_OUT__HIGH_ROLLER
			RETURN "CH_OUTFIT_6"
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC STRING PRIVATE_GET_FINALE_BOARD_TODO_LIST_ITEM(CASINO_HEIST_CLIENT_DATA &sClientData, INT iTodoListSlot)
	SWITCH iTodoListSlot
		CASE 0		
			IF PUBLIC_GET_CHOSEN_APPROACH(sClientData) = CASINO_HEIST_APPROACH_TYPE__SUBTERFUGE
				RETURN "CH_TODO_FIN_1"
			ENDIF
			RETURN "CH_TODO_FIN_0"
		CASE 1			RETURN "CH_TODO_FIN_2"
		CASE 2			RETURN "CH_TODO_FIN_3"
		CASE 3
			IF NETWORK_IS_ACTIVITY_SESSION()
				RETURN "CH_TODO_FIN_4"
			ENDIF
		BREAK
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC BOOL PRIVATE_GET_FINALE_BOARD_TODO_LIST_ITEM_COMPLETED(CASINO_HEIST_CLIENT_DATA &sClientData, INT iTodoListSlot)
	SWITCH iTodoListSlot
		CASE 0			
			IF PUBLIC_GET_CHOSEN_APPROACH(sClientData) = CASINO_HEIST_APPROACH_TYPE__SUBTERFUGE
				RETURN sClientData.sFinaleData.iOutfitInDisplayingCached != ENUM_TO_INT(CASINO_HEIST_OUTFIT_IN__NONE)
			ENDIF
			RETURN sClientData.sFinaleData.iEntranceDisplayingCached != ENUM_TO_INT(CASINO_HEIST_ACCESS_POINT__NONE)
		CASE 1			RETURN sClientData.sFinaleData.iExitDisplayingCached != ENUM_TO_INT(CASINO_HEIST_ACCESS_POINT__NONE)
		CASE 2			RETURN sClientData.sFinaleData.iDropOffDisplayingCached != ENUM_TO_INT(CASINO_HEIST_DROPOFF_LOCATION__NONE)
		CASE 3
			IF NETWORK_IS_ACTIVITY_SESSION()
				RETURN (sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[MAX_CASINO_HEIST_PLAYERS] <= 0)
			ENDIF
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC STRING PRIVATE_GET_FINALE_BOARD_OPTIONAL_LIST_ITEM(CASINO_HEIST_CLIENT_DATA &sClientData, INT iOptionalListSlot)
	SWITCH iOptionalListSlot
		CASE 0			RETURN "CH_OPT_FIN_0"
		CASE 1			RETURN "CH_OPT_FIN_1"
		CASE 2			
			IF PUBLIC_GET_CHOSEN_APPROACH(sClientData) = CASINO_HEIST_APPROACH_TYPE__SUBTERFUGE
				RETURN "CH_OPT_FIN_2"
			ENDIF
		BREAK
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC BOOL PRIVATE_GET_FINALE_BOARD_OPTIONAL_LIST_ITEM_COMPLETED(CASINO_HEIST_CLIENT_DATA &sClientData, INT iOptionalListSlot)
	SWITCH iOptionalListSlot
		CASE 0			RETURN HAS_PLAYER_PURCHASED_CASINO_HEIST_DECOY_CREW_MEMBER(sClientData.piLeader)
		CASE 1			RETURN HAS_PLAYER_PURCHASED_CASINO_HEIST_SWITCH_GETAWAY_VEHICLE(sClientData.piLeader)
		CASE 2			
			IF PUBLIC_GET_CHOSEN_APPROACH(sClientData) = CASINO_HEIST_APPROACH_TYPE__SUBTERFUGE
				RETURN sClientData.sFinaleData.iOutfitOutDisplaying != ENUM_TO_INT(CASINO_HEIST_OUTFIT_OUT__NONE)
			ENDIF
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC INT PRIVATE_GET_FINALE_ENTRANCE_IMAGE_ID(CASINO_HEIST_CLIENT_DATA &sClientData)
	SWITCH INT_TO_ENUM(CASINO_HEIST_ACCESS_POINT, sClientData.sFinaleData.iEntranceDisplaying)
		CASE CASINO_HEIST_ACCESS_POINT__MAIN_ENTRANCE							RETURN 2
		CASE CASINO_HEIST_ACCESS_POINT__STAFF_ENTRANCE							RETURN 11
		CASE CASINO_HEIST_ACCESS_POINT__GARBAGE_ENTRANCE						RETURN 1
		CASE CASINO_HEIST_ACCESS_POINT__SOUTH_WEST_ROOF_TERRACE_ENTRANCE		RETURN 10
		CASE CASINO_HEIST_ACCESS_POINT__NORTH_WEST_ROOF_TERRACE_ENTRANCE		RETURN 5
		CASE CASINO_HEIST_ACCESS_POINT__SOUTH_EAST_ROOF_TERRACE_ENTRANCE		RETURN 8
		CASE CASINO_HEIST_ACCESS_POINT__NORTH_EAST_ROOF_TERRACE_ENTRANCE		RETURN 3
		CASE CASINO_HEIST_ACCESS_POINT__SOUTH_HELIPAD_ENTRANCE					RETURN 9
		CASE CASINO_HEIST_ACCESS_POINT__NORTH_HELIPAD_ENTRANCE					RETURN 4
		CASE CASINO_HEIST_ACCESS_POINT__SECURITY_VEHICLE_ENTRANCE				RETURN 6
		CASE CASINO_HEIST_ACCESS_POINT__SEWER_ENTRANCE							RETURN 7
	ENDSWITCH
	RETURN -1
ENDFUNC

FUNC INT PRIVATE_GET_FINALE_EXIT_IMAGE_ID(CASINO_HEIST_CLIENT_DATA &sClientData)
	SWITCH INT_TO_ENUM(CASINO_HEIST_ACCESS_POINT, sClientData.sFinaleData.iExitDisplaying)
		CASE CASINO_HEIST_ACCESS_POINT__MAIN_ENTRANCE							RETURN 2
		CASE CASINO_HEIST_ACCESS_POINT__STAFF_ENTRANCE							RETURN 11
		CASE CASINO_HEIST_ACCESS_POINT__GARBAGE_ENTRANCE						RETURN 1
		CASE CASINO_HEIST_ACCESS_POINT__SOUTH_WEST_ROOF_TERRACE_ENTRANCE		RETURN 10
		CASE CASINO_HEIST_ACCESS_POINT__NORTH_WEST_ROOF_TERRACE_ENTRANCE		RETURN 5
		CASE CASINO_HEIST_ACCESS_POINT__SOUTH_EAST_ROOF_TERRACE_ENTRANCE		RETURN 8
		CASE CASINO_HEIST_ACCESS_POINT__NORTH_EAST_ROOF_TERRACE_ENTRANCE		RETURN 3
		CASE CASINO_HEIST_ACCESS_POINT__SOUTH_HELIPAD_ENTRANCE					RETURN 9
		CASE CASINO_HEIST_ACCESS_POINT__NORTH_HELIPAD_ENTRANCE					RETURN 4
		CASE CASINO_HEIST_ACCESS_POINT__SECURITY_VEHICLE_ENTRANCE				RETURN 6
		CASE CASINO_HEIST_ACCESS_POINT__SEWER_ENTRANCE							RETURN 7
	ENDSWITCH
	RETURN -1
ENDFUNC

FUNC INT PRIVATE_GET_FINALE_DROP_OFF_IMAGE_ID(CASINO_HEIST_CLIENT_DATA &sClientData)
	SWITCH INT_TO_ENUM(CASINO_HEIST_DROPOFF_LOCATIONS, sClientData.sFinaleData.iDropOffDisplaying)
		CASE CASINO_HEIST_DROPOFF_LOCATION__SHORT_1
		CASE CASINO_HEIST_DROPOFF_LOCATION__SHORT_2
		CASE CASINO_HEIST_DROPOFF_LOCATION__SHORT_3
			RETURN 1
		CASE CASINO_HEIST_DROPOFF_LOCATION__MEDIUM_1
		CASE CASINO_HEIST_DROPOFF_LOCATION__MEDIUM_2
		CASE CASINO_HEIST_DROPOFF_LOCATION__MEDIUM_3
			RETURN 2
		CASE CASINO_HEIST_DROPOFF_LOCATION__LONG_1
		CASE CASINO_HEIST_DROPOFF_LOCATION__LONG_2
		CASE CASINO_HEIST_DROPOFF_LOCATION__LONG_3 
			RETURN 3
	ENDSWITCH
	RETURN 0
ENDFUNC

FUNC INT PRIVATE_GET_FINALE_OUTFIT_IN_IMAGE_ID(CASINO_HEIST_CLIENT_DATA &sClientData)

	SWITCH INT_TO_ENUM(CASINO_HEIST_OUTFITS, sClientData.sFinaleData.iOutfitInDisplaying)
		CASE CASINO_HEIST_OUTFIT_IN__BUGSTAR			RETURN 1
		CASE CASINO_HEIST_OUTFIT_IN__MECHANIC			RETURN 2
		CASE CASINO_HEIST_OUTFIT_IN__GRUPPE_SECHS		RETURN 3
		CASE CASINO_HEIST_OUTFIT_IN__CELEBRITY			RETURN 4
	ENDSWITCH

	RETURN -1
ENDFUNC

FUNC INT PRIVATE_GET_FINALE_OUTFIT_OUT_IMAGE_ID(CASINO_HEIST_CLIENT_DATA &sClientData)

	SWITCH INT_TO_ENUM(CASINO_HEIST_OUTFITS, sClientData.sFinaleData.iOutfitOutDisplaying)
		CASE CASINO_HEIST_OUTFIT_OUT__SWAT				RETURN 2
		CASE CASINO_HEIST_OUTFIT_OUT__FIREMAN			RETURN 1
		CASE CASINO_HEIST_OUTFIT_OUT__HIGH_ROLLER		RETURN 3
	ENDSWITCH

	RETURN -1
ENDFUNC

FUNC INT PRIVATE_GET_FINALE_WEAPON_IMAGE_ID(CASINO_HEIST_CLIENT_DATA &sClientData)
	SWITCH GET_PLAYER_CASINO_HEIST_WEAPONS_EXPERT(sClientData.piLeader)
		CASE CASINO_HEIST_WEAPON_EXPERT__KARL_ABOLAJI
			SWITCH GET_PLAYER_CASINO_HEIST_APPROACH(sClientData.piLeader)
				CASE CASINO_HEIST_APPROACH_TYPE__STEALTH
					SWITCH GET_PLAYER_CASINO_HEIST_WEAPONS_LOADOUT(sClientData.piLeader)
						CASE CASINO_HEIST_WEAPON_LOADOUT__DEFAULT					RETURN -1
						CASE CASINO_HEIST_WEAPON_LOADOUT__ALTERNATE					RETURN -1
					ENDSWITCH
				BREAK
				CASE CASINO_HEIST_APPROACH_TYPE__SUBTERFUGE
					SWITCH GET_PLAYER_CASINO_HEIST_WEAPONS_LOADOUT(sClientData.piLeader)
						CASE CASINO_HEIST_WEAPON_LOADOUT__DEFAULT					RETURN -1
						CASE CASINO_HEIST_WEAPON_LOADOUT__ALTERNATE					RETURN -1
					ENDSWITCH
				BREAK
				CASE CASINO_HEIST_APPROACH_TYPE__DIRECT
					SWITCH GET_PLAYER_CASINO_HEIST_WEAPONS_LOADOUT(sClientData.piLeader)
						CASE CASINO_HEIST_WEAPON_LOADOUT__DEFAULT					RETURN 1
						CASE CASINO_HEIST_WEAPON_LOADOUT__ALTERNATE					RETURN 2
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CASINO_HEIST_WEAPON_EXPERT__GUSTAVO_MOTA
			SWITCH GET_PLAYER_CASINO_HEIST_APPROACH(sClientData.piLeader)
				CASE CASINO_HEIST_APPROACH_TYPE__STEALTH
					SWITCH GET_PLAYER_CASINO_HEIST_WEAPONS_LOADOUT(sClientData.piLeader)
						CASE CASINO_HEIST_WEAPON_LOADOUT__DEFAULT					RETURN -1
						CASE CASINO_HEIST_WEAPON_LOADOUT__ALTERNATE					RETURN -1
					ENDSWITCH
				BREAK
				CASE CASINO_HEIST_APPROACH_TYPE__SUBTERFUGE
					SWITCH GET_PLAYER_CASINO_HEIST_WEAPONS_LOADOUT(sClientData.piLeader)
						CASE CASINO_HEIST_WEAPON_LOADOUT__DEFAULT					RETURN -1
						CASE CASINO_HEIST_WEAPON_LOADOUT__ALTERNATE					RETURN -1
					ENDSWITCH
				BREAK
				CASE CASINO_HEIST_APPROACH_TYPE__DIRECT
					SWITCH GET_PLAYER_CASINO_HEIST_WEAPONS_LOADOUT(sClientData.piLeader)
						CASE CASINO_HEIST_WEAPON_LOADOUT__DEFAULT					RETURN 3
						CASE CASINO_HEIST_WEAPON_LOADOUT__ALTERNATE					RETURN 4
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CASINO_HEIST_WEAPON_EXPERT__CHARLIE
			SWITCH GET_PLAYER_CASINO_HEIST_APPROACH(sClientData.piLeader)
				CASE CASINO_HEIST_APPROACH_TYPE__STEALTH
					SWITCH GET_PLAYER_CASINO_HEIST_WEAPONS_LOADOUT(sClientData.piLeader)
						CASE CASINO_HEIST_WEAPON_LOADOUT__DEFAULT					RETURN -1
						CASE CASINO_HEIST_WEAPON_LOADOUT__ALTERNATE					RETURN -1
					ENDSWITCH
				BREAK
				CASE CASINO_HEIST_APPROACH_TYPE__SUBTERFUGE
					SWITCH GET_PLAYER_CASINO_HEIST_WEAPONS_LOADOUT(sClientData.piLeader)
						CASE CASINO_HEIST_WEAPON_LOADOUT__DEFAULT					RETURN -1
						CASE CASINO_HEIST_WEAPON_LOADOUT__ALTERNATE					RETURN -1
					ENDSWITCH
				BREAK
				CASE CASINO_HEIST_APPROACH_TYPE__DIRECT
					SWITCH GET_PLAYER_CASINO_HEIST_WEAPONS_LOADOUT(sClientData.piLeader)
						CASE CASINO_HEIST_WEAPON_LOADOUT__DEFAULT					RETURN 5
						CASE CASINO_HEIST_WEAPON_LOADOUT__ALTERNATE					RETURN 6
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CASINO_HEIST_WEAPON_EXPERT__WEAPONS_EXPERT
			SWITCH GET_PLAYER_CASINO_HEIST_APPROACH(sClientData.piLeader)
				CASE CASINO_HEIST_APPROACH_TYPE__STEALTH
					SWITCH GET_PLAYER_CASINO_HEIST_WEAPONS_LOADOUT(sClientData.piLeader)
						CASE CASINO_HEIST_WEAPON_LOADOUT__DEFAULT					RETURN -1
						CASE CASINO_HEIST_WEAPON_LOADOUT__ALTERNATE					RETURN -1
					ENDSWITCH
				BREAK
				CASE CASINO_HEIST_APPROACH_TYPE__SUBTERFUGE
					SWITCH GET_PLAYER_CASINO_HEIST_WEAPONS_LOADOUT(sClientData.piLeader)
						CASE CASINO_HEIST_WEAPON_LOADOUT__DEFAULT					RETURN -1
						CASE CASINO_HEIST_WEAPON_LOADOUT__ALTERNATE					RETURN -1
					ENDSWITCH
				BREAK
				CASE CASINO_HEIST_APPROACH_TYPE__DIRECT
					SWITCH GET_PLAYER_CASINO_HEIST_WEAPONS_LOADOUT(sClientData.piLeader)
						CASE CASINO_HEIST_WEAPON_LOADOUT__DEFAULT					RETURN 7
						CASE CASINO_HEIST_WEAPON_LOADOUT__ALTERNATE					RETURN 8
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CASINO_HEIST_WEAPON_EXPERT__PACKIE_MCREARY
			SWITCH GET_PLAYER_CASINO_HEIST_APPROACH(sClientData.piLeader)
				CASE CASINO_HEIST_APPROACH_TYPE__STEALTH
					SWITCH GET_PLAYER_CASINO_HEIST_WEAPONS_LOADOUT(sClientData.piLeader)
						CASE CASINO_HEIST_WEAPON_LOADOUT__DEFAULT					RETURN -1
						CASE CASINO_HEIST_WEAPON_LOADOUT__ALTERNATE					RETURN -1
					ENDSWITCH
				BREAK
				CASE CASINO_HEIST_APPROACH_TYPE__SUBTERFUGE
					SWITCH GET_PLAYER_CASINO_HEIST_WEAPONS_LOADOUT(sClientData.piLeader)
						CASE CASINO_HEIST_WEAPON_LOADOUT__DEFAULT					RETURN -1
						CASE CASINO_HEIST_WEAPON_LOADOUT__ALTERNATE					RETURN -1
					ENDSWITCH
				BREAK
				CASE CASINO_HEIST_APPROACH_TYPE__DIRECT
					SWITCH GET_PLAYER_CASINO_HEIST_WEAPONS_LOADOUT(sClientData.piLeader)
						CASE CASINO_HEIST_WEAPON_LOADOUT__DEFAULT					RETURN 9
						CASE CASINO_HEIST_WEAPON_LOADOUT__ALTERNATE					RETURN 10
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN -1
ENDFUNC

FUNC INT PRIVATE_GET_FINALE_VEHICLE_IMAGE_ID(CASINO_HEIST_CLIENT_DATA &sClientData)
	SWITCH GET_PLAYER_CASINO_HEIST_DRIVER(sClientData.piLeader)
		CASE CASINO_HEIST_DRIVER__KARIM_DENZ
			SWITCH GET_PLAYER_CASINO_HEIST_VEHICLE_SELECTION(sClientData.piLeader)
				CASE CASINO_HEIST_VEHICLE_SELECTION__DEFAULT				RETURN 1
				CASE CASINO_HEIST_VEHICLE_SELECTION__ALTERNATE				RETURN 2
				CASE CASINO_HEIST_VEHICLE_SELECTION__ALTERNATE_2			RETURN 3
				CASE CASINO_HEIST_VEHICLE_SELECTION__ALTERNATE_3			RETURN 4
			ENDSWITCH
		BREAK
		CASE CASINO_HEIST_DRIVER__TALIANA_MARTINEZ
			SWITCH GET_PLAYER_CASINO_HEIST_VEHICLE_SELECTION(sClientData.piLeader)
				CASE CASINO_HEIST_VEHICLE_SELECTION__DEFAULT				RETURN 5
				CASE CASINO_HEIST_VEHICLE_SELECTION__ALTERNATE				RETURN 6
				CASE CASINO_HEIST_VEHICLE_SELECTION__ALTERNATE_2			RETURN 7
				CASE CASINO_HEIST_VEHICLE_SELECTION__ALTERNATE_3			RETURN 8
			ENDSWITCH
		BREAK
		CASE CASINO_HEIST_DRIVER__EDDIE_TOH
			SWITCH GET_PLAYER_CASINO_HEIST_VEHICLE_SELECTION(sClientData.piLeader)
				CASE CASINO_HEIST_VEHICLE_SELECTION__DEFAULT				RETURN 9
				CASE CASINO_HEIST_VEHICLE_SELECTION__ALTERNATE				RETURN 10
				CASE CASINO_HEIST_VEHICLE_SELECTION__ALTERNATE_2			RETURN 11
				CASE CASINO_HEIST_VEHICLE_SELECTION__ALTERNATE_3			RETURN 12
			ENDSWITCH
		BREAK
		CASE CASINO_HEIST_DRIVER__ZACH
			SWITCH GET_PLAYER_CASINO_HEIST_VEHICLE_SELECTION(sClientData.piLeader)
				CASE CASINO_HEIST_VEHICLE_SELECTION__DEFAULT				RETURN 13
				CASE CASINO_HEIST_VEHICLE_SELECTION__ALTERNATE				RETURN 14
				CASE CASINO_HEIST_VEHICLE_SELECTION__ALTERNATE_2			RETURN 15
				CASE CASINO_HEIST_VEHICLE_SELECTION__ALTERNATE_3			RETURN 16
			ENDSWITCH
		BREAK
		CASE CASINO_HEIST_DRIVER__WEAPONS_EXPERT
			SWITCH GET_PLAYER_CASINO_HEIST_VEHICLE_SELECTION(sClientData.piLeader)
				CASE CASINO_HEIST_VEHICLE_SELECTION__DEFAULT				RETURN 17
				CASE CASINO_HEIST_VEHICLE_SELECTION__ALTERNATE				RETURN 18
				CASE CASINO_HEIST_VEHICLE_SELECTION__ALTERNATE_2			RETURN 19
				CASE CASINO_HEIST_VEHICLE_SELECTION__ALTERNATE_3			RETURN 20
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN -1
ENDFUNC

FUNC INT PRIVATE_GET_FINALE_HACKING_IMAGE_ID(CASINO_HEIST_CLIENT_DATA &sClientData)
	UNUSED_PARAMETER(sClientData)
	RETURN 1
ENDFUNC

FUNC INT PRIVATE_GET_MAP_MARKER_STATE_FOR_DROPOFF(CASINO_HEIST_CLIENT_DATA &sClientData)
	SWITCH INT_TO_ENUM(CASINO_HEIST_DROPOFF_LOCATIONS, sClientData.sFinaleData.iDropOffDisplaying)
		CASE CASINO_HEIST_DROPOFF_LOCATION__NONE
			RETURN CASINO_HEIST_MAP_MARKER_STATE__HIDDEN
			
		CASE CASINO_HEIST_DROPOFF_LOCATION__SHORT_1
		CASE CASINO_HEIST_DROPOFF_LOCATION__SHORT_2
		CASE CASINO_HEIST_DROPOFF_LOCATION__SHORT_3
			RETURN CASINO_HEIST_MAP_MARKER_STATE__SHORT
		CASE CASINO_HEIST_DROPOFF_LOCATION__MEDIUM_1
		CASE CASINO_HEIST_DROPOFF_LOCATION__MEDIUM_2
		CASE CASINO_HEIST_DROPOFF_LOCATION__MEDIUM_3
			RETURN CASINO_HEIST_MAP_MARKER_STATE__MEDIUM
		CASE CASINO_HEIST_DROPOFF_LOCATION__LONG_1
		CASE CASINO_HEIST_DROPOFF_LOCATION__LONG_2
		CASE CASINO_HEIST_DROPOFF_LOCATION__LONG_3
			RETURN CASINO_HEIST_MAP_MARKER_STATE__LONG
	ENDSWITCH
	
	RETURN CASINO_HEIST_MAP_MARKER_STATE__HIDDEN
ENDFUNC

FUNC BOOL PRIVATE_CASINO_HEIST_FINALE_BOARD__IS_BOARD_ITEM_LAUNCH_BUTTON(CASINO_HEIST_FINALE_BOARD_ITEMS eBoardItem)
	SWITCH eBoardItem
		CASE CASINO_HEIST_FINALE_BOARD_ITEM__LAUNCH_HEIST_BUTTON
			RETURN TRUE
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL PRIVATE_CASINO_HEIST_FINALE_BOARD__IS_BOARD_ITEM_A_PLAYER(CASINO_HEIST_FINALE_BOARD_ITEMS eBoardItem)
	SWITCH eBoardItem
		CASE CASINO_HEIST_FINALE_BOARD_ITEM__CREW_MEMBER_1
		CASE CASINO_HEIST_FINALE_BOARD_ITEM__CREW_MEMBER_2
		CASE CASINO_HEIST_FINALE_BOARD_ITEM__CREW_MEMBER_3
		CASE CASINO_HEIST_FINALE_BOARD_ITEM__CREW_MEMBER_4
			RETURN TRUE
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL PRIVATE_ARE_MULTIPLE_ENTRY_DISGUISES_AVAILABLE(CASINO_HEIST_CLIENT_DATA &sClientData)
	INT iCount
	IF HAS_PLAYER_ACQUIRED_CASINO_HEIST_OUTFIT(sClientData.piLeader, CASINO_HEIST_OUTFIT_IN__BUGSTAR)
		iCount++
	ENDIF
	IF HAS_PLAYER_ACQUIRED_CASINO_HEIST_OUTFIT(sClientData.piLeader, CASINO_HEIST_OUTFIT_IN__MECHANIC)
		iCount++
	ENDIF
	IF HAS_PLAYER_ACQUIRED_CASINO_HEIST_OUTFIT(sClientData.piLeader, CASINO_HEIST_OUTFIT_IN__GRUPPE_SECHS)
		iCount++
	ENDIF
	IF HAS_PLAYER_ACQUIRED_CASINO_HEIST_OUTFIT(sClientData.piLeader, CASINO_HEIST_OUTFIT_IN__CELEBRITY)
		iCount++
	ENDIF
	RETURN (iCount > 1)
ENDFUNC

FUNC BOOL PRIVATE_ARE_MULTIPLE_EXIT_DISGUISES_AVAILABLE(CASINO_HEIST_CLIENT_DATA &sClientData)
	INT iCount
	IF HAS_PLAYER_ACQUIRED_CASINO_HEIST_OUTFIT(sClientData.piLeader, CASINO_HEIST_OUTFIT_OUT__SWAT)
		iCount++
	ENDIF
	IF HAS_PLAYER_ACQUIRED_CASINO_HEIST_OUTFIT(sClientData.piLeader, CASINO_HEIST_OUTFIT_OUT__FIREMAN)
		iCount++
	ENDIF
	IF HAS_PLAYER_ACQUIRED_CASINO_HEIST_OUTFIT(sClientData.piLeader, CASINO_HEIST_OUTFIT_OUT__HIGH_ROLLER)
		iCount++
	ENDIF
	RETURN (iCount > 1)
ENDFUNC

FUNC BOOL PRIVATE_CASINO_HEIST_FINALE_BOARD__CAN_BOARD_ITEM_BE_MODIFIED(CASINO_HEIST_CLIENT_DATA &sClientData, CASINO_HEIST_FINALE_BOARD_ITEMS eBoardItem)
	IF PRIVATE_CASINO_HEIST_FINALE_BOARD__IS_BOARD_ITEM_A_PLAYER(eBoardItem)
		RETURN TRUE
	ENDIF
	SWITCH eBoardItem
		CASE CASINO_HEIST_FINALE_BOARD_ITEM__ENTRANCE_LOCATION
			IF GET_PLAYER_CASINO_HEIST_APPROACH(sClientData.piLeader) = CASINO_HEIST_APPROACH_TYPE__SUBTERFUGE
				RETURN FALSE
			ENDIF
			RETURN TRUE
		CASE CASINO_HEIST_FINALE_BOARD_ITEM__EXIT_LOCATION
		CASE CASINO_HEIST_FINALE_BOARD_ITEM__DROP_OFF_LOCATION
			RETURN TRUE
		CASE CASINO_HEIST_FINALE_BOARD_ITEM__OUTFIT_IN
			IF NOT NETWORK_IS_ACTIVITY_SESSION()
				RETURN PRIVATE_ARE_MULTIPLE_ENTRY_DISGUISES_AVAILABLE(sClientData)
			ENDIF
			RETURN TRUE
		CASE CASINO_HEIST_FINALE_BOARD_ITEM__OUTFIT_OUT
			IF NOT NETWORK_IS_ACTIVITY_SESSION()
				RETURN PRIVATE_ARE_MULTIPLE_EXIT_DISGUISES_AVAILABLE(sClientData)
			ENDIF
			RETURN TRUE
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL PRIVATE_CASINO_HEIST_FINALE_BOARD__IS_BOARD_ITEM_PURCHASEABLE(CASINO_HEIST_CLIENT_DATA &sClientData, CASINO_HEIST_FINALE_BOARD_ITEMS eBoardItem)
	SWITCH eBoardItem
		CASE CASINO_HEIST_FINALE_BOARD_ITEM__DECOY_VEHICLE
			RETURN !HAS_PLAYER_PURCHASED_CASINO_HEIST_DECOY_CREW_MEMBER(sClientData.piLeader)
		CASE CASINO_HEIST_FINALE_BOARD_ITEM__SWITCH_VEHICLE
			RETURN !HAS_PLAYER_PURCHASED_CASINO_HEIST_SWITCH_GETAWAY_VEHICLE(sClientData.piLeader)
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC INT PRIVATE_GET_WARNING_SCREEN_FOR_FINALE_BOARD_ITEM(CASINO_HEIST_FINALE_BOARD_ITEMS eBoardItem)
	SWITCH eBoardItem	
		CASE CASINO_HEIST_FINALE_BOARD_ITEM__DECOY_VEHICLE		
			IF NETWORK_CAN_SPEND_MONEY(GET_FINALE_BOARD_ITEM_COST(eBoardItem), FALSE, TRUE, FALSE)
				RETURN CASINO_HEIST_WARNING_SCREEN__CONFIRM_PURCHASE_DECOY
			ELSE
				RETURN CASINO_HEIST_WARNING_SCREEN__CANNOT_AFFORD_DECOY
			ENDIF
		BREAK
		CASE CASINO_HEIST_FINALE_BOARD_ITEM__SWITCH_VEHICLE
			IF NETWORK_CAN_SPEND_MONEY(GET_FINALE_BOARD_ITEM_COST(eBoardItem), FALSE, TRUE, FALSE)
				RETURN CASINO_HEIST_WARNING_SCREEN__CONFIRM_PURCHASE_SWITCH_VEHICLE
			ELSE
				RETURN CASINO_HEIST_WARNING_SCREEN__CANNOT_AFFORD_SWITCH_VEHICLE
			ENDIF
		BREAK
	ENDSWITCH
	RETURN CASINO_HEIST_WARNING_SCREEN__NONE
ENDFUNC

PROC PRIVATE_UPDATE_CASINO_HEIST_FINALE_WARNING_SCREEN(CASINO_HEIST_CLIENT_DATA &sClientData)
	IF sClientData.sFinaleData.iWarningScreen = CASINO_HEIST_WARNING_SCREEN__NONE
//		CDEBUG3LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_UPDATE_CASINO_HEIST_PREP_WARNING_SCREEN - NONE")
		EXIT
	ENDIF
	
	CDEBUG3LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_UPDATE_CASINO_HEIST_FINALE_WARNING_SCREEN - sClientData.sFinaleData.iWarningScreen = ", sClientData.sFinaleData.iWarningScreen)
		
	BOOL bConfirm
	IF PUBLIC_CASINO_HEIST_MAINTAIN_WARNING_SCREEN(sClientData, sClientData.sFinaleData.iWarningScreen, bConfirm)
		IF bConfirm
			CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_UPDATE_CASINO_HEIST_FINALE_WARNING_SCREEN - Confirm/accept")
		
			SWITCH sClientData.sFinaleData.iWarningScreen
				CASE CASINO_HEIST_WARNING_SCREEN__QUIT_FROM_FINALE
					CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_UPDATE_CASINO_HEIST_FINALE_WARNING_SCREEN - CASINO_HEIST_WARNING_SCREEN__QUIT_FROM_FINALE - Player chose to quit")

					SET_BIT(GlobalplayerBD_CasinoHeist[NETWORK_PLAYER_ID_TO_INT()].iGeneralBitset, ciCASINO_HEIST_GENERAL_BITSET__QUIT_FROM_FINALE_BOARD)

				BREAK
				
				CASE CASINO_HEIST_WARNING_SCREEN__CONFIRM_PURCHASE_DECOY
					CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_UPDATE_CASINO_HEIST_FINALE_WARNING_SCREEN - CASINO_HEIST_WARNING_SCREEN__CONFIRM_PURCHASE_DECOY - Player chose to purchase decoy")
					SET_PLAYER_HAS_PURCHASED_CASINO_HEIST_DECOY_CREW_MEMBER(TRUE)
					PUBLIC_CASINO_HEIST_BOARD_COMMON__SET_TICK(sClientData.sFinaleData.sScaleformData.siMainBoard, ENUM_TO_INT(CASINO_HEIST_FINALE_BOARD_ITEM__DECOY_VEHICLE), TRUE)
					PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_OPTIONAL_LIST_UPDATE_REQUIRED(sClientData, TRUE)
					PUBLIC_SET_CASINO_HEIST_DO_INSTR_BTN_UPDATE(sClientData.sFinaleData.sScaleformData)
					
					IF USE_SERVER_TRANSACTIONS()
						INT iTransaction
						TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_SPEND_CASINO_HEIST_SKIP_MISSION, GET_FINALE_BOARD_ITEM_COST(CASINO_HEIST_FINALE_BOARD_ITEM__DECOY_VEHICLE), iTransaction, FALSE, TRUE)
						g_cashTransactionData[iTransaction].cashInfo.iNumCrates = sClientData.sCursorData.iHighlight
						CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | [GTA$] PRIVATE_UPDATE_CASINO_HEIST_FINALE_WARNING_SCREEN - Decoy vehicle purchased - SERVICE_SPEND_CASINO_HEIST_SKIP_MISSION, $", GET_FINALE_BOARD_ITEM_COST(CASINO_HEIST_FINALE_BOARD_ITEM__DECOY_VEHICLE))
					ELSE
						NETWORK_SPEND_CASINO_HEIST_SKIP_MISSION(sClientData.sCursorData.iHighlight, GET_FINALE_BOARD_ITEM_COST(CASINO_HEIST_FINALE_BOARD_ITEM__DECOY_VEHICLE), FALSE, TRUE)
						CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | [GTA$] PRIVATE_UPDATE_CASINO_HEIST_FINALE_WARNING_SCREEN - Decoy vehicle purchased - NETWORK_SPEND_CASINO_HEIST_SKIP_MISSION, $", GET_FINALE_BOARD_ITEM_COST(CASINO_HEIST_FINALE_BOARD_ITEM__DECOY_VEHICLE))
					ENDIF
					
					GlobalplayerBD_CasinoHeist[NETWORK_PLAYER_ID_TO_INT()].iSyncId++
					SET_BIT(GlobalplayerBD_CasinoHeist[NETWORK_PLAYER_ID_TO_INT()].iListSyncBitSet, CASINO_HEIST_LIST_SYNC_BITSET__DECOY_GUNMAN_PURCHASED)
				BREAK
				
				CASE CASINO_HEIST_WARNING_SCREEN__CANNOT_AFFORD_DECOY
					CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_UPDATE_CASINO_HEIST_FINALE_WARNING_SCREEN - CASINO_HEIST_WARNING_SCREEN__CANNOT_AFFORD_DECOY - Player cannot afford decoy")
				BREAK
				
				CASE CASINO_HEIST_WARNING_SCREEN__CONFIRM_PURCHASE_SWITCH_VEHICLE
					CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_UPDATE_CASINO_HEIST_FINALE_WARNING_SCREEN - CASINO_HEIST_WARNING_SCREEN__CONFIRM_PURCHASE_SWITCH_VEHICLE - Player chose to purchase switch vehicle")
					SET_PLAYER_HAS_PURCHASED_CASINO_HEIST_SWITCH_GETAWAY_VEHICLE(TRUE)
					PUBLIC_CASINO_HEIST_BOARD_COMMON__SET_TICK(sClientData.sFinaleData.sScaleformData.siMainBoard, ENUM_TO_INT(CASINO_HEIST_FINALE_BOARD_ITEM__SWITCH_VEHICLE), TRUE)
					PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_OPTIONAL_LIST_UPDATE_REQUIRED(sClientData, TRUE)
					PUBLIC_SET_CASINO_HEIST_DO_INSTR_BTN_UPDATE(sClientData.sFinaleData.sScaleformData)
					
					IF USE_SERVER_TRANSACTIONS()
						INT iTransaction
						TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_SPEND_CASINO_HEIST_SKIP_MISSION, GET_FINALE_BOARD_ITEM_COST(CASINO_HEIST_FINALE_BOARD_ITEM__SWITCH_VEHICLE), iTransaction, FALSE, TRUE)
						g_cashTransactionData[iTransaction].cashInfo.iNumCrates = sClientData.sCursorData.iHighlight
						CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | [GTA$] PRIVATE_UPDATE_CASINO_HEIST_FINALE_WARNING_SCREEN - Switch vehicle purchased - SERVICE_SPEND_CASINO_HEIST_SKIP_MISSION, $", GET_FINALE_BOARD_ITEM_COST(CASINO_HEIST_FINALE_BOARD_ITEM__SWITCH_VEHICLE))
					ELSE
						NETWORK_SPEND_CASINO_HEIST_SKIP_MISSION(sClientData.sCursorData.iHighlight, GET_FINALE_BOARD_ITEM_COST(CASINO_HEIST_FINALE_BOARD_ITEM__SWITCH_VEHICLE), FALSE, TRUE)
						CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | [GTA$] PRIVATE_UPDATE_CASINO_HEIST_FINALE_WARNING_SCREEN - Switch vehicle purchased - NETWORK_SPEND_CASINO_HEIST_SKIP_MISSION, $", GET_FINALE_BOARD_ITEM_COST(CASINO_HEIST_FINALE_BOARD_ITEM__SWITCH_VEHICLE))
					ENDIF
					
					GlobalplayerBD_CasinoHeist[NETWORK_PLAYER_ID_TO_INT()].iSyncId++
					SET_BIT(GlobalplayerBD_CasinoHeist[NETWORK_PLAYER_ID_TO_INT()].iListSyncBitSet, CASINO_HEIST_LIST_SYNC_BITSET__CLEAN_VEHICLE_PURCHASED)
				BREAK
				
				CASE CASINO_HEIST_WARNING_SCREEN__CANNOT_AFFORD_SWITCH_VEHICLE
					CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_UPDATE_CASINO_HEIST_FINALE_WARNING_SCREEN - CASINO_HEIST_WARNING_SCREEN__CANNOT_AFFORD_SWITCH_VEHICLE - Player cannot afford switch vehicle")
				BREAK
				
			ENDSWITCH
			
			sClientData.sFinaleData.iWarningScreen = CASINO_HEIST_WARNING_SCREEN__NONE
			sClientData.sFinaleData.bWarningScreenBuffer = TRUE
		ELSE
			CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_UPDATE_CASINO_HEIST_FINALE_WARNING_SCREEN - Cancel/reject")
			sClientData.sFinaleData.iWarningScreen = CASINO_HEIST_WARNING_SCREEN__NONE
			sClientData.sFinaleData.bWarningScreenBuffer = TRUE
			sClientData.sFinaleData.iWarningScreenBS = 0
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL PRIVATE_IS_ENTRANCE_AVAILABLE(CASINO_HEIST_CLIENT_DATA &sClientData)
	
	CASINO_HEIST_ACCESS_POINT eAccessPoint = INT_TO_ENUM(CASINO_HEIST_ACCESS_POINT, sClientData.sFinaleData.iEntranceDisplaying)
		
	IF eAccessPoint != CASINO_HEIST_ACCESS_POINT__NONE
		IF NOT HAS_PLAYER_SCOPED_THIS_CASINO_HEIST_ACCESS_POINT(sClientData.piLeader, eAccessPoint)
			RETURN FALSE
		ENDIF
	ELSE
		RETURN FALSE
	ENDIF
	
	SWITCH GET_PLAYER_CASINO_HEIST_APPROACH(sClientData.piLeader)
		CASE CASINO_HEIST_APPROACH_TYPE__STEALTH
			SWITCH eAccessPoint
				CASE CASINO_HEIST_ACCESS_POINT__MAIN_ENTRANCE
				CASE CASINO_HEIST_ACCESS_POINT__SEWER_ENTRANCE
				CASE CASINO_HEIST_ACCESS_POINT__SECURITY_VEHICLE_ENTRANCE
					RETURN FALSE
			ENDSWITCH
		BREAK
		CASE CASINO_HEIST_APPROACH_TYPE__SUBTERFUGE
			SWITCH INT_TO_ENUM(CASINO_HEIST_OUTFITS, sClientData.sFinaleData.iOutfitInDisplaying)
				CASE CASINO_HEIST_OUTFIT_IN__BUGSTAR
					IF eAccessPoint != CASINO_HEIST_ACCESS_POINT__STAFF_ENTRANCE
						RETURN FALSE
					ENDIF
				BREAK
				CASE CASINO_HEIST_OUTFIT_IN__MECHANIC
					IF eAccessPoint != CASINO_HEIST_ACCESS_POINT__GARBAGE_ENTRANCE
						RETURN FALSE
					ENDIF
				BREAK
				CASE CASINO_HEIST_OUTFIT_IN__GRUPPE_SECHS
					IF eAccessPoint != CASINO_HEIST_ACCESS_POINT__SECURITY_VEHICLE_ENTRANCE
						RETURN FALSE
					ENDIF
				BREAK
				CASE CASINO_HEIST_OUTFIT_IN__CELEBRITY
					IF eAccessPoint != CASINO_HEIST_ACCESS_POINT__MAIN_ENTRANCE
						RETURN FALSE
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE CASINO_HEIST_APPROACH_TYPE__DIRECT
			SWITCH eAccessPoint
				CASE CASINO_HEIST_ACCESS_POINT__SECURITY_VEHICLE_ENTRANCE
					RETURN FALSE
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL PRIVATE_FIND_NEXT_ENTRANCE(CASINO_HEIST_CLIENT_DATA &sClientData, BOOL bIncrement)
	BOOL bFound
	
	WHILE (NOT bFound)
	
		IF bIncrement
			sClientData.sFinaleData.iEntranceDisplaying++
			
			CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_FIND_NEXT_ENTRANCE - Increment - sClientData.sFinaleData.iEntranceDisplaying = ", sClientData.sFinaleData.iEntranceDisplaying)
			
			IF sClientData.sFinaleData.iEntranceDisplaying > ENUM_TO_INT(CASINO_HEIST_ACCESS_POINT__SEWER_ENTRANCE)
				sClientData.sFinaleData.iEntranceDisplaying = ENUM_TO_INT(CASINO_HEIST_ACCESS_POINT__NONE)
				CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_FIND_NEXT_ENTRANCE - Loop round to start")
			ENDIF
		ELSE
			sClientData.sFinaleData.iEntranceDisplaying--
			
			CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_FIND_NEXT_ENTRANCE - Decrement - sClientData.sFinaleData.iEntranceDisplaying = ", sClientData.sFinaleData.iEntranceDisplaying)
			
			IF sClientData.sFinaleData.iEntranceDisplaying < ENUM_TO_INT(CASINO_HEIST_ACCESS_POINT__NONE)
				sClientData.sFinaleData.iEntranceDisplaying = ENUM_TO_INT(CASINO_HEIST_ACCESS_POINT__SEWER_ENTRANCE)
				CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_FIND_NEXT_ENTRANCE - Loop round to end")
			ENDIF
		ENDIF
		
		IF PRIVATE_IS_ENTRANCE_AVAILABLE(sClientData)
			bFound = TRUE
			RETURN TRUE
		ENDIF
	ENDWHILE
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PRIVATE_IS_EXIT_AVAILABLE(CASINO_HEIST_CLIENT_DATA &sClientData)
	
	CASINO_HEIST_ACCESS_POINT eAccessPoint = INT_TO_ENUM(CASINO_HEIST_ACCESS_POINT, sClientData.sFinaleData.iExitDisplaying)
		
	IF eAccessPoint != CASINO_HEIST_ACCESS_POINT__NONE
		IF NOT HAS_PLAYER_SCOPED_THIS_CASINO_HEIST_ACCESS_POINT(sClientData.piLeader, eAccessPoint)
			RETURN FALSE
		ENDIF
	ELSE
		RETURN FALSE
	ENDIF
	
	SWITCH GET_PLAYER_CASINO_HEIST_APPROACH(sClientData.piLeader)
		CASE CASINO_HEIST_APPROACH_TYPE__STEALTH
			SWITCH eAccessPoint
				CASE CASINO_HEIST_ACCESS_POINT__MAIN_ENTRANCE
				CASE CASINO_HEIST_ACCESS_POINT__SEWER_ENTRANCE
				CASE CASINO_HEIST_ACCESS_POINT__SECURITY_VEHICLE_ENTRANCE
					RETURN FALSE
			ENDSWITCH
		BREAK
		CASE CASINO_HEIST_APPROACH_TYPE__SUBTERFUGE
			SWITCH eAccessPoint
				CASE CASINO_HEIST_ACCESS_POINT__MAIN_ENTRANCE
				CASE CASINO_HEIST_ACCESS_POINT__SEWER_ENTRANCE
				CASE CASINO_HEIST_ACCESS_POINT__SECURITY_VEHICLE_ENTRANCE
					RETURN FALSE
			ENDSWITCH
		BREAK
		CASE CASINO_HEIST_APPROACH_TYPE__DIRECT
			SWITCH eAccessPoint
				CASE CASINO_HEIST_ACCESS_POINT__SEWER_ENTRANCE
				CASE CASINO_HEIST_ACCESS_POINT__SECURITY_VEHICLE_ENTRANCE
					RETURN FALSE
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL PRIVATE_FIND_NEXT_EXIT(CASINO_HEIST_CLIENT_DATA &sClientData, BOOL bIncrement)
	BOOL bFound
	
	WHILE (NOT bFound)
	
		IF bIncrement
			sClientData.sFinaleData.iExitDisplaying++
			
			CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_FIND_NEXT_EXIT - Increment - sClientData.sFinaleData.iExitDisplaying = ", sClientData.sFinaleData.iExitDisplaying)
			
			IF sClientData.sFinaleData.iExitDisplaying > ENUM_TO_INT(CASINO_HEIST_ACCESS_POINT__SEWER_ENTRANCE)
				sClientData.sFinaleData.iExitDisplaying = ENUM_TO_INT(CASINO_HEIST_ACCESS_POINT__NONE)
				CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_FIND_NEXT_EXIT - Loop round to start")
			ENDIF
		ELSE
			sClientData.sFinaleData.iExitDisplaying--
			
			CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_FIND_NEXT_EXIT - Decrement - sClientData.sFinaleData.iExitDisplaying = ", sClientData.sFinaleData.iExitDisplaying)
			
			IF sClientData.sFinaleData.iExitDisplaying < ENUM_TO_INT(CASINO_HEIST_ACCESS_POINT__NONE)
				sClientData.sFinaleData.iExitDisplaying = ENUM_TO_INT(CASINO_HEIST_ACCESS_POINT__SEWER_ENTRANCE)
				CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_FIND_NEXT_EXIT - Loop round to end")
			ENDIF
		ENDIF
		
		IF PRIVATE_IS_EXIT_AVAILABLE(sClientData)
			bFound = TRUE
			RETURN TRUE
		ENDIF
	ENDWHILE
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PRIVATE_IS_DROP_OFF_AVAILABLE(CASINO_HEIST_CLIENT_DATA &sClientData)
	
	CASINO_HEIST_DROPOFF_LOCATIONS eDropOff = INT_TO_ENUM(CASINO_HEIST_DROPOFF_LOCATIONS, sClientData.sFinaleData.iDropOffDisplaying)
		
	SWITCH eDropOff	
		CASE CASINO_HEIST_DROPOFF_LOCATION__SHORT_1
//		CASE CASINO_HEIST_DROPOFF_LOCATION__SHORT_2
//		CASE CASINO_HEIST_DROPOFF_LOCATION__SHORT_3
		CASE CASINO_HEIST_DROPOFF_LOCATION__MEDIUM_1
//		CASE CASINO_HEIST_DROPOFF_LOCATION__MEDIUM_2
//		CASE CASINO_HEIST_DROPOFF_LOCATION__MEDIUM_3
		CASE CASINO_HEIST_DROPOFF_LOCATION__LONG_1
//		CASE CASINO_HEIST_DROPOFF_LOCATION__LONG_2
//		CASE CASINO_HEIST_DROPOFF_LOCATION__LONG_3
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PRIVATE_FIND_NEXT_DROP_OFF(CASINO_HEIST_CLIENT_DATA &sClientData, BOOL bIncrement)
	BOOL bFound
	
	WHILE (NOT bFound)
	
		IF bIncrement
			sClientData.sFinaleData.iDropOffDisplaying++
			
			CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_FIND_DROP_OFF - Increment - sClientData.sFinaleData.iDropOffDisplaying = ", sClientData.sFinaleData.iDropOffDisplaying)
			
			IF sClientData.sFinaleData.iDropOffDisplaying > ENUM_TO_INT(CASINO_HEIST_DROPOFF_LOCATION__LONG_3)
				sClientData.sFinaleData.iDropOffDisplaying = ENUM_TO_INT(CASINO_HEIST_DROPOFF_LOCATION__NONE)
				CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_FIND_DROP_OFF - Loop round to start")
			ENDIF
		ELSE
			sClientData.sFinaleData.iDropOffDisplaying--
			
			CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_FIND_DROP_OFF - Decrement - sClientData.sFinaleData.iDropOffDisplaying = ", sClientData.sFinaleData.iDropOffDisplaying)
			
			IF sClientData.sFinaleData.iDropOffDisplaying < ENUM_TO_INT(CASINO_HEIST_DROPOFF_LOCATION__NONE)
				sClientData.sFinaleData.iDropOffDisplaying = ENUM_TO_INT(CASINO_HEIST_DROPOFF_LOCATION__LONG_3)
				CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_FIND_DROP_OFF - Loop round to end")
			ENDIF
		ENDIF
		
		IF PRIVATE_IS_DROP_OFF_AVAILABLE(sClientData)
			bFound = TRUE
			RETURN TRUE
		ENDIF
	ENDWHILE
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PRIVATE_IS_OUTFIT_IN_AVAILABLE(CASINO_HEIST_CLIENT_DATA &sClientData)
	
	CASINO_HEIST_OUTFITS eOutfit = INT_TO_ENUM(CASINO_HEIST_OUTFITS, sClientData.sFinaleData.iOutfitInDisplaying)
		
	SWITCH eOutfit
		CASE CASINO_HEIST_OUTFIT_IN__BUGSTAR
		CASE CASINO_HEIST_OUTFIT_IN__MECHANIC
		CASE CASINO_HEIST_OUTFIT_IN__GRUPPE_SECHS
		CASE CASINO_HEIST_OUTFIT_IN__CELEBRITY
			RETURN HAS_PLAYER_ACQUIRED_CASINO_HEIST_OUTFIT(sClientData.piLeader, eOutfit)
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PRIVATE_FIND_NEXT_OUTFIT_IN(CASINO_HEIST_CLIENT_DATA &sClientData, BOOL bIncrement)
	BOOL bFound
	
	WHILE (NOT bFound)
	
		IF bIncrement
			sClientData.sFinaleData.iOutfitInDisplaying++
			
			CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_FIND_NEXT_OUTFIT_IN - Increment - sClientData.sFinaleData.iOutfitInDisplaying = ", sClientData.sFinaleData.iOutfitInDisplaying)
			
			IF sClientData.sFinaleData.iOutfitInDisplaying > ENUM_TO_INT(CASINO_HEIST_OUTFIT_IN__CELEBRITY)
				sClientData.sFinaleData.iOutfitInDisplaying = ENUM_TO_INT(CASINO_HEIST_OUTFIT_IN__NONE)
				CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_FIND_NEXT_OUTFIT_IN - Loop round to start")
			ENDIF
		ELSE
			sClientData.sFinaleData.iOutfitInDisplaying--
			
			CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_FIND_NEXT_OUTFIT_IN - Decrement - sClientData.sFinaleData.iOutfitInDisplaying = ", sClientData.sFinaleData.iOutfitInDisplaying)
			
			IF sClientData.sFinaleData.iOutfitInDisplaying < ENUM_TO_INT(CASINO_HEIST_OUTFIT_IN__NONE)
				sClientData.sFinaleData.iOutfitInDisplaying = ENUM_TO_INT(CASINO_HEIST_OUTFIT_IN__CELEBRITY)
				CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_FIND_NEXT_OUTFIT_IN - Loop round to end")
			ENDIF
		ENDIF
		
		IF PRIVATE_IS_OUTFIT_IN_AVAILABLE(sClientData)
			bFound = TRUE
			RETURN TRUE
		ENDIF
	ENDWHILE
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PRIVATE_IS_OUTFIT_OUT_AVAILABLE(CASINO_HEIST_CLIENT_DATA &sClientData)
	
	CASINO_HEIST_OUTFITS eOutfit = INT_TO_ENUM(CASINO_HEIST_OUTFITS, sClientData.sFinaleData.iOutfitOutDisplaying)
		
	SWITCH eOutfit
		CASE CASINO_HEIST_OUTFIT_OUT__SWAT
		CASE CASINO_HEIST_OUTFIT_OUT__FIREMAN
		CASE CASINO_HEIST_OUTFIT_OUT__HIGH_ROLLER
			RETURN HAS_PLAYER_ACQUIRED_CASINO_HEIST_OUTFIT(sClientData.piLeader, eOutfit)
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PRIVATE_FIND_NEXT_OUTFIT_OUT(CASINO_HEIST_CLIENT_DATA &sClientData, BOOL bIncrement)
	BOOL bFound
	
	WHILE (NOT bFound)
	
		IF bIncrement
			sClientData.sFinaleData.iOutfitOutDisplaying++
			
			CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_FIND_NEXT_OUTFIT_OUT - Increment - sClientData.sFinaleData.iOutfitOutDisplaying = ", sClientData.sFinaleData.iOutfitOutDisplaying)
			
			IF sClientData.sFinaleData.iOutfitOutDisplaying > ENUM_TO_INT(CASINO_HEIST_OUTFIT_OUT__HIGH_ROLLER)
				sClientData.sFinaleData.iOutfitOutDisplaying = ENUM_TO_INT(CASINO_HEIST_OUTFIT_OUT__NONE)
				CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_FIND_NEXT_OUTFIT_OUT - Loop round to start")
			ENDIF
		ELSE
			sClientData.sFinaleData.iOutfitOutDisplaying--
			
			CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_FIND_NEXT_OUTFIT_OUT - Decrement - sClientData.sFinaleData.iOutfitOutDisplaying = ", sClientData.sFinaleData.iOutfitOutDisplaying)
			
			IF sClientData.sFinaleData.iOutfitOutDisplaying < ENUM_TO_INT(CASINO_HEIST_OUTFIT_OUT__NONE)
				sClientData.sFinaleData.iOutfitOutDisplaying = ENUM_TO_INT(CASINO_HEIST_OUTFIT_OUT__HIGH_ROLLER)
				CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_FIND_NEXT_OUTFIT_OUT - Loop round to end")
			ENDIF
		ENDIF
		
		IF PRIVATE_IS_OUTFIT_OUT_AVAILABLE(sClientData)
			bFound = TRUE
			RETURN TRUE
		ENDIF
	ENDWHILE
	
	RETURN FALSE
ENDFUNC

PROC PRIVATE_UPDATE_SELECTED_FINALE_BOARD_ITEM(CASINO_HEIST_CLIENT_DATA &sClientData, CASINO_HEIST_FINALE_BOARD_ITEMS eBoardItem, CASINO_HEIST_CONTROL_TYPE eControlType)
//	BOOL bUpdateLists = TRUE

	SWITCH eBoardItem	
		CASE CASINO_HEIST_FINALE_BOARD_ITEM__ENTRANCE_LOCATION
			SWITCH eControlType
				CASE CHCT_LEFT
					IF PRIVATE_FIND_NEXT_ENTRANCE(sClientData, FALSE)
						PLAY_SOUND_FRONTEND(-1,"Highlight_Move","DLC_HEIST_PLANNING_BOARD_SOUNDS")
						PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_ENTRANCE_BUTTON_UPDATE_REQUIRED(sClientData, TRUE)
						PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_HEADING_UPDATE_REQUIRED(sClientData, TRUE)
					ENDIF
				BREAK
				CASE CHCT_RIGHT
					IF PRIVATE_FIND_NEXT_ENTRANCE(sClientData, TRUE)
						PLAY_SOUND_FRONTEND(-1,"Highlight_Move","DLC_HEIST_PLANNING_BOARD_SOUNDS")
						PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_ENTRANCE_BUTTON_UPDATE_REQUIRED(sClientData, TRUE)
						PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_HEADING_UPDATE_REQUIRED(sClientData, TRUE)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE CASINO_HEIST_FINALE_BOARD_ITEM__EXIT_LOCATION
			SWITCH eControlType
				CASE CHCT_LEFT
					IF PRIVATE_FIND_NEXT_EXIT(sClientData, FALSE)
						PLAY_SOUND_FRONTEND(-1,"Highlight_Move","DLC_HEIST_PLANNING_BOARD_SOUNDS")
						PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_EXIT_BUTTON_UPDATE_REQUIRED(sClientData, TRUE)
						PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_HEADING_UPDATE_REQUIRED(sClientData, TRUE)
					ENDIF
				BREAK
				CASE CHCT_RIGHT
					IF PRIVATE_FIND_NEXT_EXIT(sClientData, TRUE)
						PLAY_SOUND_FRONTEND(-1,"Highlight_Move","DLC_HEIST_PLANNING_BOARD_SOUNDS")
						PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_EXIT_BUTTON_UPDATE_REQUIRED(sClientData, TRUE)
						PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_HEADING_UPDATE_REQUIRED(sClientData, TRUE)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE CASINO_HEIST_FINALE_BOARD_ITEM__DROP_OFF_LOCATION
			SWITCH eControlType
				CASE CHCT_LEFT
					IF PRIVATE_FIND_NEXT_DROP_OFF(sClientData, FALSE)
						PLAY_SOUND_FRONTEND(-1,"Highlight_Move","DLC_HEIST_PLANNING_BOARD_SOUNDS")
						PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_DROP_OFF_BUTTON_UPDATE_REQUIRED(sClientData, TRUE)
						PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_HEADING_UPDATE_REQUIRED(sClientData, TRUE)
					ENDIF
				BREAK
				CASE CHCT_RIGHT
					IF PRIVATE_FIND_NEXT_DROP_OFF(sClientData, TRUE)
						PLAY_SOUND_FRONTEND(-1,"Highlight_Move","DLC_HEIST_PLANNING_BOARD_SOUNDS")
						PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_DROP_OFF_BUTTON_UPDATE_REQUIRED(sClientData, TRUE)
						PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_HEADING_UPDATE_REQUIRED(sClientData, TRUE)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE CASINO_HEIST_FINALE_BOARD_ITEM__OUTFIT_IN
			SWITCH eControlType
				CASE CHCT_LEFT
					IF PRIVATE_FIND_NEXT_OUTFIT_IN(sClientData, FALSE)
						PLAY_SOUND_FRONTEND(-1,"Highlight_Move","DLC_HEIST_PLANNING_BOARD_SOUNDS")
						PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_OUTFIT_IN_BUTTON_UPDATE_REQUIRED(sClientData, TRUE)
						PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_HEADING_UPDATE_REQUIRED(sClientData, TRUE)
					ENDIF
				BREAK
				CASE CHCT_RIGHT
					IF PRIVATE_FIND_NEXT_OUTFIT_IN(sClientData, TRUE)
						PLAY_SOUND_FRONTEND(-1,"Highlight_Move","DLC_HEIST_PLANNING_BOARD_SOUNDS")
						PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_OUTFIT_IN_BUTTON_UPDATE_REQUIRED(sClientData, TRUE)
						PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_HEADING_UPDATE_REQUIRED(sClientData, TRUE)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE CASINO_HEIST_FINALE_BOARD_ITEM__OUTFIT_OUT
			SWITCH eControlType
				CASE CHCT_LEFT
					IF PRIVATE_FIND_NEXT_OUTFIT_OUT(sClientData, FALSE)
						PLAY_SOUND_FRONTEND(-1,"Highlight_Move","DLC_HEIST_PLANNING_BOARD_SOUNDS")
						PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_OUTFIT_OUT_BUTTON_UPDATE_REQUIRED(sClientData, TRUE)
						PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_HEADING_UPDATE_REQUIRED(sClientData, TRUE)
					ENDIF
				BREAK
				CASE CHCT_RIGHT
					IF PRIVATE_FIND_NEXT_OUTFIT_OUT(sClientData, TRUE)
						PLAY_SOUND_FRONTEND(-1,"Highlight_Move","DLC_HEIST_PLANNING_BOARD_SOUNDS")
						PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_OUTFIT_OUT_BUTTON_UPDATE_REQUIRED(sClientData, TRUE)
						PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_HEADING_UPDATE_REQUIRED(sClientData, TRUE)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		// Cash distributions
		CASE CASINO_HEIST_FINALE_BOARD_ITEM__CREW_MEMBER_1
		CASE CASINO_HEIST_FINALE_BOARD_ITEM__CREW_MEMBER_2
		CASE CASINO_HEIST_FINALE_BOARD_ITEM__CREW_MEMBER_3
		CASE CASINO_HEIST_FINALE_BOARD_ITEM__CREW_MEMBER_4
			INT iModifier, iPlayerSlot
		
			SWITCH eControlType 
			
				CASE CHCT_LEFT
				
					iModifier = (CASINO_HEIST_FINALE_CUT_STEP) * (-1) // flip to negative for decrement.
				
				BREAK
				
				CASE CHCT_RIGHT
				
					// Check that the pot [0,1,2,3,4] has at least positive percent for the amount that will be added to the highlighted player.
					//							   ^
					
					IF sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[MAX_CASINO_HEIST_PLAYERS] >= CASINO_HEIST_FINALE_CUT_STEP
						iModifier = (CASINO_HEIST_FINALE_CUT_STEP)
					ENDIF
				
				BREAK
				
			ENDSWITCH
			
			iPlayerSlot = PRIVATE_GET_CREW_MEMBER_SLOT(eBoardItem)
			
			PRIVATE_UPDATE_CASINO_HEIST_FINALE_CUTS(sClientData, iPlayerSlot, iModifier)
			
			CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] Inputs (edit) - Changed cut with modifier: ", 
				iModifier, " for member in slot: ", iPlayerSlot)
				
			INT index
			FOR index = 0 TO MAX_CASINO_HEIST_PLAYERS
				CDEBUG3LN(DEBUG_CASINO_HEIST_PLANNING, "  ... iRewardPercentage[",index,"] = ", sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[index])
			ENDFOR
		BREAK
	ENDSWITCH
	
//	IF bUpdateLists
//		PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_TODO_LIST_UPDATE_REQUIRED(sClientData, TRUE)
//		PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_OPTIONAL_LIST_UPDATE_REQUIRED(sClientData, TRUE)
//	ENDIF
ENDPROC


/// PURPOSE:
///    Maintain the scaleform instructional movie (bottom right). This handles all of it's own state and is mostly automated. If a new state is added to the
///    planning board, support must also be extended here.
/// PARAMS:
///    sClientData.sFinaleData.sScaleformData.sButtons - 
PROC PRIVATE_UPDATE_CASINO_HEIST_FINALE_INSTR_BTNS(CASINO_HEIST_CLIENT_DATA &sClientData)
	
	// This is to make the map/board switching control more
	// friendly on PC.
//	CONTROL_ACTION caGoToMapInput	= INPUT_FRONTEND_LB
//	CONTROL_ACTION caGoToBoardInput = INPUT_FRONTEND_RB
	
//	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
//		caGoToMapInput = INPUT_FRONTEND_X
//		caGoToBoardInput = INPUT_FRONTEND_CANCEL
//	ENDIF
	
	SPRITE_PLACEMENT spPlacementLocation = GET_SCALEFORM_INSTRUCTIONAL_BUTTON_POSITION()
	
	// Don't render the buttons if the player is typing in network chat.
	IF NETWORK_TEXT_CHAT_IS_TYPING()
		EXIT
	ENDIF
	
	BOOL bUpdateInstructions = sClientData.sFinaleData.sScaleformData.bUpdateInstructions
	
	IF PUBLIC_IS_ANY_CASINO_HEIST_DIALOGUE_CURRENTLY_PLAYING(sClientData)
	OR sClientData.sFinaleData.iTutorialStage > 0
		IF !sClientData.sFinaleData.sScaleformData.bDrawSuppressedInstructions
			bUpdateInstructions = TRUE
			sClientData.sFinaleData.sScaleformData.bDrawSuppressedInstructions = TRUE
		ENDIF
	ELSE
		IF sClientData.sFinaleData.sScaleformData.bDrawSuppressedInstructions
			bUpdateInstructions = TRUE
			sClientData.sFinaleData.sScaleformData.bDrawSuppressedInstructions = FALSE
		ENDIF
	ENDIF
	
	IF NOT bUpdateInstructions
	
		RUN_SCALEFORM_INSTRUCTIONAL_BUTTONS(sClientData.sFinaleData.sScaleformData.siButtons,
											spPlacementLocation,
											sClientData.sFinaleData.sScaleformData.sButtons,
											FALSE)
	
		EXIT
	ENDIF
	
	IF NOT GET_IS_WIDESCREEN()
	OR GET_CURRENT_LANGUAGE() = LANGUAGE_JAPANESE
		SET_SCALEFORM_INSTRUCTIONAL_BUTTON_WRAP(sClientData.sFinaleData.sScaleformData.sButtons, 0.5)
	ELSE
		SET_SCALEFORM_INSTRUCTIONAL_BUTTON_WRAP(sClientData.sFinaleData.sScaleformData.sButtons, 0.7)
	ENDIF
	
	REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(sClientData.sFinaleData.sScaleformData.sButtons)
	
	// TODO: Replace all the labels in here.
	
	IF IS_BIT_SET(sClientData.sFinaleData.sFinaleConfig.iListSyncBitSet, CASINO_HEIST_LIST_SYNC_BITSET__SELECTIONS_MADE)
		ADD_SCALEFORM_INSTRUCTIONAL_BUTTON_WITH_INT_AS_TIME("", "HEIST_IB_CONT", sClientData.sFinaleData.iForceLaunchTime, sClientData.sFinaleData.sScaleformData.sButtons)
	ENDIF
	
	CASINO_HEIST_FINALE_BOARD_ITEMS eBoardItem = INT_TO_ENUM(CASINO_HEIST_FINALE_BOARD_ITEMS, sClientData.sCursorData.iHighlight)
		
	IF sClientData.sFinaleData.sScaleformData.bDrawSuppressedInstructions
	
//		ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_Y, 
//			"CH_INSTR_INFO", sClientData.sSetupData.sScaleformData.sButtons, TRUE) // More info.
		
		IF PUBLIC_GET_CASINO_HEIST_CAMERA_ZOOM_AVAILABLE_FOR_POSITION_ID(sClientData.sCameraData)
		
			IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
				ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_SNIPER_ZOOM, 
					"CH_INSTR_ZOOM", sClientData.sFinaleData.sScaleformData.sButtons, TRUE) // Zoom.
			ELSE
				ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_FRONTEND_TRIGGERS, 
					"CH_INSTR_ZOOM", sClientData.sFinaleData.sScaleformData.sButtons) // Zoom. 
			ENDIF
			
		ENDIF
	
		ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_LOOK, 
			"HEIST_IB_LOOK", sClientData.sFinaleData.sScaleformData.sButtons) // Look Around
	
	ELSE
	
		SWITCH sClientData.sFinaleData.sStateData.eSubState

			CASE CHBSS_VIEWING

				IF sClientData.piLeader = PLAYER_ID()
				
					IF PRIVATE_CASINO_HEIST_FINALE_BOARD__IS_BOARD_ITEM_LAUNCH_BUTTON(eBoardItem)
						ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT, 
							"CH_INSTR_SEL", sClientData.sFinaleData.sScaleformData.sButtons, TRUE)
							
					ELIF PRIVATE_CASINO_HEIST_FINALE_BOARD__CAN_BOARD_ITEM_BE_MODIFIED(sClientData, eBoardItem)
						ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT, 
							PICK_STRING(NETWORK_IS_ACTIVITY_SESSION(), "CH_INSTR_MODI", "CH_INSTR_PREV"), sClientData.sFinaleData.sScaleformData.sButtons, TRUE)
							
					ENDIF
					
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL, 
						"CH_INSTR_QUIT", sClientData.sFinaleData.sScaleformData.sButtons, TRUE) // Go back.
					
					IF PRIVATE_CASINO_HEIST_FINALE_BOARD__IS_BOARD_ITEM_PURCHASEABLE(sClientData, eBoardItem)
						ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_X, 
							"CH_INSTR_PURCH", sClientData.sFinaleData.sScaleformData.sButtons, TRUE)
					ENDIF
					
				ELSE
					IF NETWORK_IS_ACTIVITY_SESSION()
						IF PRIVATE_IS_CASINO_HEIST_FINALE_PLAYER_READY(sClientData, PLAYER_ID())
							ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT, 
								"HEIST_IB_UNRDY", sClientData.sFinaleData.sScaleformData.sButtons, TRUE)
						ELSE
							ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT, 
								"HEIST_IB_RDY", sClientData.sFinaleData.sScaleformData.sButtons, TRUE)
						ENDIF
						
						ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL, 
							"CH_INSTR_QUIT", sClientData.sFinaleData.sScaleformData.sButtons, TRUE) // Go back.
					ENDIF
				ENDIF
				
				IF PUBLIC_CASINO_HEIST_BOARD_COMMON__IS_HIGHLIGHTED_BOARD_ITEM_ON_FINALE_BOARD(sClientData)
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_Y, 
						"CH_INSTR_INFO", sClientData.sFinaleData.sScaleformData.sButtons, TRUE) // More info.
				ENDIF
				
				IF NOT NETWORK_IS_ACTIVITY_SESSION()
	//				IF CAN_PLAYER_USE_THIS_CASINO_HEIST_PLANNING_BOARD(sClientData, CHBT_SETUP)
	//					ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_RB, 
	//						"CH_INSTR_SETUP", sClientData.sFinaleData.sScaleformData.sButtons, TRUE) // Setup Board.
	//				ENDIF	
							
					IF CAN_PLAYER_USE_THIS_CASINO_HEIST_PLANNING_BOARD(sClientData, CHBT_PREP)
						ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_LB, 
							"CH_INSTR_PREP", sClientData.sFinaleData.sScaleformData.sButtons, TRUE) // Prep Board.
					ENDIF
				ENDIF
			
				// Can't look around or zoom when using mouse on the planning board.
				IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_SNIPER_ZOOM, 
						"CH_INSTR_ZOOM", sClientData.sFinaleData.sScaleformData.sButtons, TRUE) // Zoom.
				ELSE
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_FRONTEND_TRIGGERS, 
						"CH_INSTR_ZOOM", sClientData.sFinaleData.sScaleformData.sButtons) // Zoom. 
				ENDIF
				
				ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_LOOK, 
					"HEIST_IB_LOOK", sClientData.sFinaleData.sScaleformData.sButtons) // Look Around
					
				ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_MOVE, 
					"HEIST_IB_NAV", sClientData.sFinaleData.sScaleformData.sButtons) // Navigate controls.
					
				IF PRIVATE_CASINO_HEIST_FINALE_BOARD__IS_BOARD_ITEM_A_PLAYER(eBoardItem)
				
					IF IS_PS3_VERSION() OR IS_PLAYSTATION_PLATFORM() OR IS_PC_VERSION()
						ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_SELECT, 
							"HEIST_IB_PSN", sClientData.sFinaleData.sScaleformData.sButtons, TRUE)
					ELIF IS_XBOX360_VERSION() OR IS_XBOX_PLATFORM()
						ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(
							GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_SELECT), 
								"HEIST_IB_XBL", sClientData.sFinaleData.sScaleformData.sButtons)
					ELSE
						ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(
							GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_SELECT), 
								"HEIST_IB_PSNXBL", sClientData.sFinaleData.sScaleformData.sButtons)
					ENDIF
					
				ENDIF
				
			BREAK
			
			CASE CHBSS_EDITING
			
				IF sClientData.piLeader != PLAYER_ID()
					EXIT
				ENDIF
				
				IF NETWORK_IS_ACTIVITY_SESSION()
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT, 
						"CH_INSTR_SET", sClientData.sFinaleData.sScaleformData.sButtons, TRUE) // Set.
				ENDIF	
				
				ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL, 
					PICK_STRING(NETWORK_IS_ACTIVITY_SESSION(), "CH_INSTR_CANC", "CH_INSTR_BACK"), sClientData.sFinaleData.sScaleformData.sButtons, TRUE) // Cancel/Back.
				
				IF PUBLIC_CASINO_HEIST_BOARD_COMMON__IS_HIGHLIGHTED_BOARD_ITEM_ON_FINALE_BOARD(sClientData)
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_Y, 
						"CH_INSTR_INFO", sClientData.sFinaleData.sScaleformData.sButtons, TRUE) // More info.
				ENDIF
				
				IF PRIVATE_CASINO_HEIST_FINALE_BOARD__IS_BOARD_ITEM_A_PLAYER(eBoardItem)
						
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_FRONTEND_DPAD_LR, 
						"CH_INSTR_CUT", sClientData.sFinaleData.sScaleformData.sButtons) // Change Cut.
						
				ELIF eBoardItem = CASINO_HEIST_FINALE_BOARD_ITEM__ENTRANCE_LOCATION
				
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_FRONTEND_DPAD_LR, 
						"CH_INSTR_ENT", sClientData.sFinaleData.sScaleformData.sButtons) // Change Entrance.
				
				ELIF eBoardItem = CASINO_HEIST_FINALE_BOARD_ITEM__EXIT_LOCATION
				
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_FRONTEND_DPAD_LR, 
						"CH_INSTR_EXT", sClientData.sFinaleData.sScaleformData.sButtons) // Change Exit.
				
				ELIF eBoardItem = CASINO_HEIST_FINALE_BOARD_ITEM__DROP_OFF_LOCATION
				
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_FRONTEND_DPAD_LR, 
						"CH_INSTR_BUY", sClientData.sFinaleData.sScaleformData.sButtons) // Change Buyer.
				
				ELIF eBoardItem = CASINO_HEIST_FINALE_BOARD_ITEM__OUTFIT_IN
				
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_FRONTEND_DPAD_LR, 
						"CH_INSTR_OUTI", sClientData.sFinaleData.sScaleformData.sButtons) // Change Outfit (Entry).
				
				ELIF eBoardItem = CASINO_HEIST_FINALE_BOARD_ITEM__OUTFIT_OUT
						
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_FRONTEND_DPAD_LR, 
						"CH_INSTR_OUTO", sClientData.sFinaleData.sScaleformData.sButtons) // Change Outfit (Exit).
						
				ENDIF
				
				ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_LOOK, 
					"HEIST_IB_LOOK", sClientData.sFinaleData.sScaleformData.sButtons) // Look Around

			BREAK
			
		ENDSWITCH
		
	ENDIF
	
	CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "Buttons - Updated instructional buttons for position: ", ENUM_TO_INT(sClientData.sCameraData.ePositionId))
	
	RUN_SCALEFORM_INSTRUCTIONAL_BUTTONS(sClientData.sFinaleData.sScaleformData.siButtons,
										spPlacementLocation,
										sClientData.sFinaleData.sScaleformData.sButtons,
										sClientData.sFinaleData.sScaleformData.bUpdateInstructions)
											
	PUBLIC_SET_CASINO_HEIST_DO_INSTR_BTN_UPDATE(sClientData.sFinaleData.sScaleformData, FALSE)
								
ENDPROC

PROC PRIVATE_RENDER_CASINO_HEIST_FINALE_SCALEFORM(CASINO_HEIST_CLIENT_DATA &sClientData)

	IF sClientData.sFinaleData.sScaleformData.bDrawBoard
		IF HAS_SCALEFORM_MOVIE_LOADED(sClientData.sFinaleData.sScaleformData.siMainBoard)
		
			SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
			SET_TEXT_RENDER_ID(sClientData.sFinaleData.sScaleformData.iRenderTargetID)
			SWITCH ciHEIST_RT_TYPE
				CASE 0																										BREAK
				CASE 1 SET_SCALEFORM_MOVIE_TO_USE_LARGE_RT(sClientData.sFinaleData.sScaleformData.siMainBoard, TRUE)			BREAK
				CASE 2 SET_SCALEFORM_MOVIE_TO_USE_SUPER_LARGE_RT(sClientData.sFinaleData.sScaleformData.siMainBoard, TRUE)	BREAK
			ENDSWITCH
			DRAW_SCALEFORM_MOVIE(sClientData.sFinaleData.sScaleformData.siMainBoard, 
								cfCASINO_HEIST_SCREEN_CENTRE_X, cfCASINO_HEIST_SCREEN_CENTRE_Y, 
								cfCASINO_HEIST_SCREEN_WIDTH, cfCASINO_HEIST_SCREEN_HEIGHT, 
								ciHEIST_SCREEN_RED, ciHEIST_SCREEN_GREEN, ciHEIST_SCREEN_BLUE, ciHEIST_SCREEN_ALPHA)
			SET_TEXT_RENDER_ID(GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID())
			SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(FALSE)	
									
			IF sClientData.sFinaleData.sScaleformData.bVerboseDebugging
				CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "Render - Index: ", NATIVE_TO_INT(sClientData.sFinaleData.sScaleformData.siMainBoard))
				CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "  ... World Pos: ", sClientData.sFinaleData.sScaleformData.vMainBoardPosition)
				CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "  ... World Rot: ", sClientData.sFinaleData.sScaleformData.vMainBoardRotation)
				CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "  ... Board Size: ", sClientData.sFinaleData.sScaleformData.vMainBoardSize)
				CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "  ... World Size: ", sClientData.sFinaleData.sScaleformData.vMainBoardWorldSize)
			ENDIF
		ENDIF
		
	ELSE
		IF CAN_PLAYER_START_CASINO_HEIST_FINALE(sClientData.piLeader)
			sClientData.sFinaleData.sScaleformData.bDrawBoard = TRUE
			CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_RENDER_CASINO_HEIST_FINALE_SCALEFORM - Not rendering but player can start the finale, so we need to start drawing")
		ENDIF
	ENDIF
	
	IF sClientData.sFinaleData.sScaleformData.bDrawInstructions
	AND GET_CORONA_STATUS() != CORONA_STATUS_FLOW_CUTSCENE
		PRIVATE_UPDATE_CASINO_HEIST_FINALE_INSTR_BTNS(sClientData)
	ENDIF

ENDPROC

PROC PRIVATE_CLEAR_CASINO_HEIST_FINALE_CAMERA(CASINO_HEIST_CLIENT_DATA &sClientData)

	CLEAR_FIRST_PERSON_CAMERA(sClientData.sCameraData.sFPSCam)
	SET_WIDESCREEN_BORDERS(FALSE,0)	

	CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "Camera - Cleared FPS camera.")
	
ENDPROC

PROC PRIVATE_TOGGLE_CASINO_HEIST_FINALE_READY_STATE(CASINO_HEIST_CLIENT_DATA &sClientData)
	
	INT index
	FOR index = 0 TO (MAX_CASINO_HEIST_PLAYERS-1)
		IF INT_TO_PLAYERINDEX(sClientData.sFinaleData.sFinaleConfig.iPlayerOrder[index]) != PLAYER_ID()
			RELOOP
		ENDIF
		
		IF sClientData.sFinaleData.sFinaleConfig.iReadyState[index] = CASINO_HEIST_FINALE_READY
			sClientData.sFinaleData.sFinaleConfig.iReadyState[index] = CASINO_HEIST_FINALE_NOT_READY
			CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "Ready - Set local state to: CASINO_HEIST_FINALE_NOT_READY")
		ELSE
			sClientData.sFinaleData.sFinaleConfig.iReadyState[index] = CASINO_HEIST_FINALE_READY
			CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "Ready - Set local state to: CASINO_HEIST_FINALE_READY")
		ENDIF
		
	ENDFOR
	
	PUBLIC_SET_CASINO_HEIST_DO_INSTR_BTN_UPDATE(sClientData.sFinaleData.sScaleformData, TRUE)
	
ENDPROC

PROC PRIVATE_MONITOR_CASINO_HEIST_FINALE_LAUNCH(CASINO_HEIST_CLIENT_DATA &sClientData)

	IF sClientData.piLeader = PLAYER_ID()
	OR NATIVE_TO_INT(sClientData.piLeader) = -1
		EXIT
	ENDIF

	IF GlobalplayerBD_CasinoHeist[NATIVE_TO_INT(sClientData.piLeader)].iReadyState[0] = CASINO_HEIST_FINALE_READY
	
		// TODO: Add the rest of the logic for launching.
		CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "LAUNCH - GO GO GO - LEADER HAS AUTHORISED LAUNCH!")
		PUBLIC_PROGRESS_CASINO_HEIST_FSM(sClientData.sFinaleData.sStateData, CHBT_FINALE)
		
	ENDIF

ENDPROC

PROC PRIVATE_CLEAR_LAUNCH_BUTTON_DISABLED_HELP()
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CH_HELP_LBDR0")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CH_HELP_LBDR1")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CH_HELP_LBDR2")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CH_HELP_LBDR3")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CH_HELP_LBDR4")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CH_HELP_LBDR5")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CH_HELP_LBDR6")
		CLEAR_HELP()
	ENDIF
ENDPROC

PROC PRIVATE_PRINT_HELP_FOR_LAUNCH_BUTTON_DISABLED_REASON(CASINO_HEIST_CLIENT_DATA &sClientData)
	SWITCH sClientData.sFinaleData.sFinaleConfig.iLaunchButtonDisabledReason
		CASE CASINO_HEIST_LAUNCH_BUTTON_DISABLED_REASON_EDITING
			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CH_HELP_LBDR0")
				PRINT_HELP("CH_HELP_LBDR0")
			ENDIF
		BREAK
		CASE CASINO_HEIST_LAUNCH_BUTTON_DISABLED_REASON_MONEY_IN_KITTY
			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CH_HELP_LBDR1")
				PRINT_HELP("CH_HELP_LBDR1")
			ENDIF
		BREAK
		CASE CASINO_HEIST_LAUNCH_BUTTON_DISABLED_REASON_SOMEONE_NOT_READY
			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CH_HELP_LBDR2")
				PRINT_HELP("CH_HELP_LBDR2")
			ENDIF
		BREAK
		CASE CASINO_HEIST_LAUNCH_BUTTON_DISABLED_REASON_NO_DROPOFF_SELECTED
			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CH_HELP_LBDR3")
				PRINT_HELP("CH_HELP_LBDR3")
			ENDIF
		BREAK
		CASE CASINO_HEIST_LAUNCH_BUTTON_DISABLED_REASON_NO_ENTRANCE_SELECTED
			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CH_HELP_LBDR4")
				PRINT_HELP("CH_HELP_LBDR4")
			ENDIF
		BREAK
		CASE CASINO_HEIST_LAUNCH_BUTTON_DISABLED_REASON_NO_EXIT_SELECTED
			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CH_HELP_LBDR5")
				PRINT_HELP("CH_HELP_LBDR5")
			ENDIF
		BREAK
		CASE CASINO_HEIST_LAUNCH_BUTTON_DISABLED_REASON_NO_OUTFIT_IN_SELECTED
			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CH_HELP_LBDR6")
				PRINT_HELP("CH_HELP_LBDR6")
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC PRIVATE_RANDOMISE_DROP_OFF_LOCATION_FOR_LAUNCH(CASINO_HEIST_CLIENT_DATA &sClientData)
	CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] PRIVATE_RANDOMISE_DROP_OFF_LOCATION_FOR_LAUNCH - Current: ", CASINO_HEIST_MISSION_DATA__DROPOFF_LOCATION_STRING_FOR_DEBUG(INT_TO_ENUM(CASINO_HEIST_DROPOFF_LOCATIONS, sClientData.sFinaleData.iDropOffDisplaying)))
	sClientData.sFinaleData.iDropOffDisplaying += GET_RANDOM_INT_IN_RANGE(0, 3)
	GlobalplayerBD_CasinoHeist[NATIVE_TO_INT(PLAYER_ID())].sMissionConfig.iDropOffLocation = sClientData.sFinaleData.iDropOffDisplaying
	CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] PRIVATE_RANDOMISE_DROP_OFF_LOCATION_FOR_LAUNCH - New: ", CASINO_HEIST_MISSION_DATA__DROPOFF_LOCATION_STRING_FOR_DEBUG(INT_TO_ENUM(CASINO_HEIST_DROPOFF_LOCATIONS, sClientData.sFinaleData.iDropOffDisplaying)))
ENDPROC


PROC PRIVATE_LAUNCH_CASINO_HEIST_FINALE(CASINO_HEIST_CLIENT_DATA &sClientData)
	
	IF sClientData.piLeader != PLAYER_ID()
		EXIT
	ENDIF
	
	IF sClientData.sFinaleData.sFinaleConfig.bLaunchButtonEnabled = FALSE
		CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] PRIVATE_LAUNCH_CASINO_HEIST_FINALE - bLaunchButtonEnabled = FALSE")
		PRIVATE_PRINT_HELP_FOR_LAUNCH_BUTTON_DISABLED_REASON(sClientData)
		EXIT
	ENDIF
	
	PRIVATE_CLEAR_LAUNCH_BUTTON_DISABLED_HELP()
	
	sClientData.sFinaleData.sFinaleConfig.iReadyState[0] = CASINO_HEIST_FINALE_READY
	GlobalplayerBD_CasinoHeist[NETWORK_PLAYER_ID_TO_INT()].iReadyState[0] = CASINO_HEIST_FINALE_READY
	PRIVATE_RANDOMISE_DROP_OFF_LOCATION_FOR_LAUNCH(sClientData)
	
	// TODO: Add the rest of the logic for launching.
	CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] PRIVATE_LAUNCH_CASINO_HEIST_FINALE - Launching finale!")
//	PUBLIC_PROGRESS_CASINO_HEIST_FSM(sClientData.sFinaleData.sStateData, CHBT_FINALE)
	
ENDPROC

PROC PRIVATE_LAUNCH_CASINO_HEIST_FINALE_LOBBY(CASINO_HEIST_CLIENT_DATA &sClientData)
	
	IF sClientData.piLeader != PLAYER_ID()
		EXIT
	ENDIF
	
	IF GET_CASINO_HEIST_MISSION_ARRAY_POS(GET_CASINO_HEIST_INTRO_MISSION_FROM_APPROACH_TYPE(GET_PLAYER_CASINO_HEIST_APPROACH(sClientData.piLeader))) = -1
		CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_LAUNCH_CASINO_HEIST_FINALE_LOBBY - Failed to find array pos. Dont set launch flag.")
		EXIT
	ENDIF
	
	PUBLIC_CASINO_HEIST_BOARD__SET_BIT(sClientData, CASINO_HEIST_BOARD_BITSET__LAUNCH_FINALE_MISSION)
	PUBLIC_TOGGLE_CASINO_HEIST_INTERACTION(sClientData, FALSE)
	sClientData.sFinaleData.sScaleformData.bDrawInstructions = FALSE
	
	// TODO: Add the rest of the logic for launching.
	CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_LAUNCH_CASINO_HEIST_FINALE_LOBBY - Launching finale lobby!")

	
ENDPROC

//PROC PRIVATE_START_CASINO_HEIST_FINALE_EDIT_MODE(CASINO_HEIST_CLIENT_DATA &sClientData)
//
//	IF sClientData.sFinaleData.sStateData.eSubState = CHBSS_EDITING
//		EXIT
//	ENDIF
//	
//	sClientData.sFinaleData.sStateData.eSubState = CHBSS_EDITING
//	sClientData.sFinaleData.sScaleformData.bUpdateInstructions = TRUE
//	
//	INT index
//	FOR index = 0 TO MAX_CASINO_HEIST_PLAYERS
//	
//		IF index >= MAX_CASINO_HEIST_PLAYERS
//			sClientData.sFinaleData.sFinaleConfig.iRewardPercentagesCache[MAX_CASINO_HEIST_PLAYERS] = sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[MAX_CASINO_HEIST_PLAYERS]
//			RELOOP
//		ENDIF
//	
//		sClientData.sFinaleData.sFinaleConfig.iRewardPercentagesCache[index] = sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[index]
//	ENDFOR
//	
//	PUBLIC_SET_CASINO_HEIST_DO_INSTR_BTN_UPDATE(sClientData.sFinaleData.sScaleformData, TRUE)
//	
//	CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "Editing - Moving into leader edit mode.")
//
//ENDPROC

PROC PRIVATE_START_EDIT_MODE_FOR_FINALE_BOARD_ITEM(CASINO_HEIST_CLIENT_DATA &sClientData, CASINO_HEIST_FINALE_BOARD_ITEMS eBoardItem)
	IF sClientData.sFinaleData.sStateData.eSubState = CHBSS_EDITING
		EXIT
	ENDIF
	
	// Cache everyone's cuts if we're modifying player cuts, so we can revert to them if they hit B to cancel any changes
	IF PRIVATE_CASINO_HEIST_FINALE_BOARD__IS_BOARD_ITEM_A_PLAYER(eBoardItem)
		INT index
		FOR index = 0 TO MAX_CASINO_HEIST_PLAYERS
		
			// Kitty
			IF index >= MAX_CASINO_HEIST_PLAYERS
				sClientData.sFinaleData.sFinaleConfig.iRewardPercentagesCache[MAX_CASINO_HEIST_PLAYERS] = sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[MAX_CASINO_HEIST_PLAYERS]
				RELOOP
			ENDIF
		
			sClientData.sFinaleData.sFinaleConfig.iRewardPercentagesCache[index] = sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[index]
		ENDFOR
	ELSE
		SWITCH eBoardItem
			CASE CASINO_HEIST_FINALE_BOARD_ITEM__ENTRANCE_LOCATION
				sClientData.sFinaleData.iEntranceDisplayingCached = sClientData.sFinaleData.iEntranceDisplaying
			BREAK
			CASE CASINO_HEIST_FINALE_BOARD_ITEM__EXIT_LOCATION
				sClientData.sFinaleData.iExitDisplayingCached = sClientData.sFinaleData.iExitDisplaying
			BREAK
			CASE CASINO_HEIST_FINALE_BOARD_ITEM__DROP_OFF_LOCATION
				sClientData.sFinaleData.iDropOffDisplayingCached = sClientData.sFinaleData.iDropOffDisplaying
			BREAK
			CASE CASINO_HEIST_FINALE_BOARD_ITEM__OUTFIT_IN
				sClientData.sFinaleData.iOutfitInDisplayingCached = sClientData.sFinaleData.iOutfitInDisplaying
			BREAK
			CASE CASINO_HEIST_FINALE_BOARD_ITEM__OUTFIT_OUT
				sClientData.sFinaleData.iOutfitOutDisplayingCached = sClientData.sFinaleData.iOutfitOutDisplaying
			BREAK
		ENDSWITCH
	ENDIF
	
	sClientData.sFinaleData.sStateData.eSubState = CHBSS_EDITING
	sClientData.sFinaleData.sScaleformData.bUpdateInstructions = TRUE
		
	PUBLIC_CASINO_HEIST_BOARD_COMMON__SET_SELECTION_ARROWS_STATE(sClientData.sFinaleData.sScaleformData.siMainBoard, ENUM_TO_INT(eBoardItem), CASINO_HEIST_SELECTION_ARROW_STATE__VISIBLE_IN_USE)
	PUBLIC_SET_CASINO_HEIST_DO_INSTR_BTN_UPDATE(sClientData.sFinaleData.sScaleformData, TRUE)
	
	IF sClientData.piLeader = PLAYER_ID()
		PUBLIC_SET_CASINO_HEIST_DIALOGUE_SHOULD_TRIGGER(sClientData, PUBLIC_GET_DIALOGUE_TO_TRIGGER_ON_EDIT_MODE_START(sClientData))
	ENDIF
	
	IF NOT NETWORK_IS_ACTIVITY_SESSION()
		SET_CASINO_HEIST_PLANNING_HELP_TRIGGER(sClientData, CASINO_HEIST_PLANNING_HELP_TEXT__FINALE_EXPLAIN_PREVIEW)
	ELSE
		IF eBoardItem = CASINO_HEIST_FINALE_BOARD_ITEM__DROP_OFF_LOCATION
			SET_CASINO_HEIST_PLANNING_HELP_TRIGGER(sClientData, CASINO_HEIST_PLANNING_HELP_TEXT__FINALE_EXPLAIN_BUYER)
		ELIF eBoardItem = CASINO_HEIST_FINALE_BOARD_ITEM__ENTRANCE_LOCATION
		OR eBoardItem = CASINO_HEIST_FINALE_BOARD_ITEM__EXIT_LOCATION
			SET_CASINO_HEIST_PLANNING_HELP_TRIGGER(sClientData, CASINO_HEIST_PLANNING_HELP_TEXT__FINALE_EXPLAIN_ENTRANCE_AND_EXIT)
		ELIF eBoardItem = CASINO_HEIST_FINALE_BOARD_ITEM__OUTFIT_IN
			SET_CASINO_HEIST_PLANNING_HELP_TRIGGER(sClientData, CASINO_HEIST_PLANNING_HELP_TEXT__FINALE_EXPLAIN_SUBTERFUGE_ENTRANCE)
		ENDIF
	ENDIF
	
	IF sClientData.sCameraData.ePositionId != PUBLIC_GET_CAMERA_POSTION_FOR_BOARD_ITEM(sClientData)
		sClientData.sCameraData.ePositionId = PUBLIC_GET_CAMERA_POSTION_FOR_BOARD_ITEM(sClientData)
		CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_START_EDIT_MODE_FOR_FINALE_BOARD_ITEM - Move camera")
	ENDIF
	
	PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
	
	CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_START_EDIT_MODE_FOR_FINALE_BOARD_ITEM - starting edit mode for finale board item ", ENUM_TO_INT(eBoardItem))
ENDPROC

FUNC CASINO_HEIST_ACCESS_POINT PRIVATE_GET_CASINO_HEIST_FINALE_BOARD_ENTRANCE_FROM_OUTFIT_IN(CASINO_HEIST_CLIENT_DATA &sClientData)
	SWITCH INT_TO_ENUM(CASINO_HEIST_OUTFITS, sClientData.sFinaleData.iOutfitInDisplaying)
		CASE CASINO_HEIST_OUTFIT_IN__BUGSTAR		RETURN CASINO_HEIST_ACCESS_POINT__STAFF_ENTRANCE
		CASE CASINO_HEIST_OUTFIT_IN__MECHANIC		RETURN CASINO_HEIST_ACCESS_POINT__GARBAGE_ENTRANCE
		CASE CASINO_HEIST_OUTFIT_IN__GRUPPE_SECHS	RETURN CASINO_HEIST_ACCESS_POINT__SECURITY_VEHICLE_ENTRANCE
		CASE CASINO_HEIST_OUTFIT_IN__CELEBRITY		RETURN CASINO_HEIST_ACCESS_POINT__MAIN_ENTRANCE
	ENDSWITCH
	RETURN CASINO_HEIST_ACCESS_POINT__NONE
ENDFUNC

PROC PRIVATE_STOP_EDIT_MODE_FOR_FINALE_BOARD_ITEM(CASINO_HEIST_CLIENT_DATA &sClientData, CASINO_HEIST_FINALE_BOARD_ITEMS eBoardItem, BOOL bConfirm = FALSE)
	IF sClientData.sFinaleData.sStateData.eSubState = CHBSS_VIEWING
		EXIT
	ENDIF
	
	sClientData.sFinaleData.sStateData.eSubState = CHBSS_VIEWING
	sClientData.sFinaleData.sScaleformData.bUpdateInstructions = TRUE
		
	PUBLIC_CASINO_HEIST_BOARD_COMMON__SET_SELECTION_ARROWS_STATE(sClientData.sFinaleData.sScaleformData.siMainBoard, ENUM_TO_INT(eBoardItem), CASINO_HEIST_SELECTION_ARROW_STATE__VISIBLE)
	PUBLIC_SET_CASINO_HEIST_DO_INSTR_BTN_UPDATE(sClientData.sFinaleData.sScaleformData, TRUE)
	PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_TODO_LIST_UPDATE_REQUIRED(sClientData, TRUE)
	PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_OPTIONAL_LIST_UPDATE_REQUIRED(sClientData, TRUE)
		
	IF bConfirm
		PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
		
		IF NETWORK_IS_ACTIVITY_SESSION()
			GlobalplayerBD_CasinoHeist[NETWORK_PLAYER_ID_TO_INT()].iSyncId++
		
			SWITCH eBoardItem
				CASE CASINO_HEIST_FINALE_BOARD_ITEM__ENTRANCE_LOCATION
					sClientData.sFinaleData.iEntranceDisplayingCached = sClientData.sFinaleData.iEntranceDisplaying
					
					SWITCH INT_TO_ENUM(CASINO_HEIST_ACCESS_POINT, sClientData.sFinaleData.iEntranceDisplaying)
						CASE CASINO_HEIST_ACCESS_POINT__SOUTH_WEST_ROOF_TERRACE_ENTRANCE
						CASE CASINO_HEIST_ACCESS_POINT__NORTH_WEST_ROOF_TERRACE_ENTRANCE
						CASE CASINO_HEIST_ACCESS_POINT__SOUTH_EAST_ROOF_TERRACE_ENTRANCE
						CASE CASINO_HEIST_ACCESS_POINT__NORTH_EAST_ROOF_TERRACE_ENTRANCE
						CASE CASINO_HEIST_ACCESS_POINT__SOUTH_HELIPAD_ENTRANCE
						CASE CASINO_HEIST_ACCESS_POINT__NORTH_HELIPAD_ENTRANCE
							PUBLIC_SET_CASINO_HEIST_DIALOGUE_SHOULD_TRIGGER(sClientData, CASINO_HEIST_PLANNING_DIALOGUE__FINALE_CHOOSE_ROOF_ENTRANCE)
							CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_STOP_EDIT_MODE_FOR_FINALE_BOARD_ITEM - Player has hit confirm on a roof entrance, play dialogue")
						BREAK
					ENDSWITCH
					
					SET_BIT(GlobalplayerBD_CasinoHeist[NETWORK_PLAYER_ID_TO_INT()].iListSyncBitSet, CASINO_HEIST_LIST_SYNC_BITSET__ENTRANCE_CONFIRMED)
				BREAK
				CASE CASINO_HEIST_FINALE_BOARD_ITEM__EXIT_LOCATION
					sClientData.sFinaleData.iExitDisplayingCached = sClientData.sFinaleData.iExitDisplaying
					SET_BIT(GlobalplayerBD_CasinoHeist[NETWORK_PLAYER_ID_TO_INT()].iListSyncBitSet, CASINO_HEIST_LIST_SYNC_BITSET__EXIT_CONFIRMED)
				BREAK
				CASE CASINO_HEIST_FINALE_BOARD_ITEM__DROP_OFF_LOCATION
					sClientData.sFinaleData.iDropOffDisplayingCached = sClientData.sFinaleData.iDropOffDisplaying
					SET_BIT(GlobalplayerBD_CasinoHeist[NETWORK_PLAYER_ID_TO_INT()].iListSyncBitSet, CASINO_HEIST_LIST_SYNC_BITSET__DROP_OFF_CONFIRMED)
				BREAK
				CASE CASINO_HEIST_FINALE_BOARD_ITEM__OUTFIT_IN
					sClientData.sFinaleData.iOutfitInDisplayingCached = sClientData.sFinaleData.iOutfitInDisplaying
					
					IF GET_PLAYER_CASINO_HEIST_APPROACH(sClientData.piLeader) = CASINO_HEIST_APPROACH_TYPE__SUBTERFUGE
						IF sClientData.sFinaleData.iEntranceDisplaying != ENUM_TO_INT(PRIVATE_GET_CASINO_HEIST_FINALE_BOARD_ENTRANCE_FROM_OUTFIT_IN(sClientData))
							sClientData.sFinaleData.iEntranceDisplaying = ENUM_TO_INT(PRIVATE_GET_CASINO_HEIST_FINALE_BOARD_ENTRANCE_FROM_OUTFIT_IN(sClientData))
							sClientData.sFinaleData.iEntranceDisplayingCached = sClientData.sFinaleData.iEntranceDisplaying
							SET_BIT(GlobalplayerBD_CasinoHeist[NETWORK_PLAYER_ID_TO_INT()].iListSyncBitSet, CASINO_HEIST_LIST_SYNC_BITSET__ENTRANCE_CONFIRMED)
							PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_ENTRANCE_BUTTON_UPDATE_REQUIRED(sClientData, TRUE)
							PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_HEADING_UPDATE_REQUIRED(sClientData, TRUE)
							CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_STOP_EDIT_MODE_FOR_FINALE_BOARD_ITEM - Player has hit confirm on outfit in, forcing entrance to ", sClientData.sFinaleData.iEntranceDisplaying)
						ENDIF
					ENDIF
					SET_BIT(GlobalplayerBD_CasinoHeist[NETWORK_PLAYER_ID_TO_INT()].iListSyncBitSet, CASINO_HEIST_LIST_SYNC_BITSET__OUTFIT_IN_CONFIRMED)
				BREAK
				CASE CASINO_HEIST_FINALE_BOARD_ITEM__OUTFIT_OUT
					sClientData.sFinaleData.iOutfitOutDisplayingCached = sClientData.sFinaleData.iOutfitOutDisplaying
					SET_BIT(GlobalplayerBD_CasinoHeist[NETWORK_PLAYER_ID_TO_INT()].iListSyncBitSet, CASINO_HEIST_LIST_SYNC_BITSET__OUTFIT_OUT_CONFIRMED)
				BREAK
			ENDSWITCH
		ENDIF
	ELSE
		PLAY_SOUND_FRONTEND(-1, "BACK", "HUD_FRONTEND_DEFAULT_SOUNDSET")
	
		IF PRIVATE_CASINO_HEIST_FINALE_BOARD__IS_BOARD_ITEM_A_PLAYER(eBoardItem)
	
			INT index
			FOR index = 0 TO MAX_CASINO_HEIST_PLAYERS
			
				IF index >= MAX_CASINO_HEIST_PLAYERS
					sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[MAX_CASINO_HEIST_PLAYERS] = sClientData.sFinaleData.sFinaleConfig.iRewardPercentagesCache[MAX_CASINO_HEIST_PLAYERS]
					RELOOP
				ENDIF
			
				sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[index] = sClientData.sFinaleData.sFinaleConfig.iRewardPercentagesCache[index]
			ENDFOR
	
		ELSE
			SWITCH eBoardItem
				CASE CASINO_HEIST_FINALE_BOARD_ITEM__ENTRANCE_LOCATION
					IF sClientData.sFinaleData.iEntranceDisplayingCached != sClientData.sFinaleData.iEntranceDisplaying
						sClientData.sFinaleData.iEntranceDisplaying = sClientData.sFinaleData.iEntranceDisplayingCached
						PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_ENTRANCE_BUTTON_UPDATE_REQUIRED(sClientData, TRUE)
					ENDIF
				BREAK
				CASE CASINO_HEIST_FINALE_BOARD_ITEM__EXIT_LOCATION
					IF sClientData.sFinaleData.iExitDisplayingCached != sClientData.sFinaleData.iExitDisplaying
						sClientData.sFinaleData.iExitDisplaying = sClientData.sFinaleData.iExitDisplayingCached
						PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_EXIT_BUTTON_UPDATE_REQUIRED(sClientData, TRUE)
					ENDIF
				BREAK
				CASE CASINO_HEIST_FINALE_BOARD_ITEM__DROP_OFF_LOCATION
					IF sClientData.sFinaleData.iDropOffDisplayingCached != sClientData.sFinaleData.iDropOffDisplaying
						sClientData.sFinaleData.iDropOffDisplaying = sClientData.sFinaleData.iDropOffDisplayingCached
						PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_DROP_OFF_BUTTON_UPDATE_REQUIRED(sClientData, TRUE)
					ENDIF
				BREAK
				CASE CASINO_HEIST_FINALE_BOARD_ITEM__OUTFIT_IN
					IF sClientData.sFinaleData.iOutfitInDisplayingCached != sClientData.sFinaleData.iOutfitInDisplaying
						sClientData.sFinaleData.iOutfitInDisplaying = sClientData.sFinaleData.iOutfitInDisplayingCached
						PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_OUTFIT_IN_BUTTON_UPDATE_REQUIRED(sClientData, TRUE)
					ENDIF
				BREAK
				CASE CASINO_HEIST_FINALE_BOARD_ITEM__OUTFIT_OUT
					IF sClientData.sFinaleData.iOutfitOutDisplayingCached != sClientData.sFinaleData.iOutfitOutDisplaying
						sClientData.sFinaleData.iOutfitOutDisplaying = sClientData.sFinaleData.iOutfitOutDisplayingCached
						PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_OUTFIT_OUT_BUTTON_UPDATE_REQUIRED(sClientData, TRUE)
					ENDIF
				BREAK
			ENDSWITCH
			
			PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_HEADING_UPDATE_REQUIRED(sClientData, TRUE)
		ENDIF
	ENDIF
	
	IF sClientData.sCameraData.ePositionId != CHCAM_FINALE_MAIN
		sClientData.sCameraData.ePositionId = CHCAM_FINALE_MAIN
		CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_STOP_EDIT_MODE_FOR_FINALE_BOARD_ITEM - Move camera back to main view for finale board")
	ENDIF
	
	CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_STOP_EDIT_MODE_FOR_FINALE_BOARD_ITEM - stopping edit mode for finale board item ", ENUM_TO_INT(eBoardItem))
ENDPROC

/// PURPOSE:
///    Manage and maintain the UP/DOWN selection indexes driven from by front-end controls.
/// PARAMS:
///    eHighlightType - ENUM - Direction of travel for the highlighter.
PROC PRIVATE_UPDATE_CASINO_HEIST_FINALE_CONTROL_SELECTIONS(CASINO_HEIST_CLIENT_DATA &sClientData, CASINO_HEIST_CONTROL_TYPE eControlType = CHCT_NONE)

	// Casino Heist planning board never needs a sub-highlighter (dual-axis selector).
	sClientData.sCursorData.iSubHighlight = 0

	SWITCH eControlType
	
		CASE CHCT_DOWN
		
			PUBLIC_CASINO_HEIST_BOARD_COMMON__SET_INPUT_EVENT(sClientData.sFinaleData.sScaleformData.siMainBoard, INPUT_FRONTEND_DOWN)

			CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "FINALE_BOARD - Inputs - Moving highlighter DOWN.")
			
			PLAY_SOUND_FRONTEND(-1,"Highlight_Move","DLC_HEIST_PLANNING_BOARD_SOUNDS")

			BREAK
			
		CASE CHCT_UP
		
			PUBLIC_CASINO_HEIST_BOARD_COMMON__SET_INPUT_EVENT(sClientData.sFinaleData.sScaleformData.siMainBoard, INPUT_FRONTEND_UP)
			
			CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "FINALE_BOARD - Inputs - Moving highlighter UP.")
			
			PLAY_SOUND_FRONTEND(-1,"Highlight_Move","DLC_HEIST_PLANNING_BOARD_SOUNDS")
			
			BREAK
			
		CASE CHCT_LEFT
		
			PUBLIC_CASINO_HEIST_BOARD_COMMON__SET_INPUT_EVENT(sClientData.sFinaleData.sScaleformData.siMainBoard, INPUT_FRONTEND_LEFT)
		
			CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "FINALE_BOARD - Inputs - Moving highlighter LEFT.")
			
			PLAY_SOUND_FRONTEND(-1,"Highlight_Move","DLC_HEIST_PLANNING_BOARD_SOUNDS")
		
			BREAK
			
		CASE CHCT_RIGHT
		
			PUBLIC_CASINO_HEIST_BOARD_COMMON__SET_INPUT_EVENT(sClientData.sFinaleData.sScaleformData.siMainBoard, INPUT_FRONTEND_RIGHT)
		
			CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "FINALE_BOARD - Inputs - Moving highlighter RIGHT.")
			
			PLAY_SOUND_FRONTEND(-1,"Highlight_Move","DLC_HEIST_PLANNING_BOARD_SOUNDS")
		
			BREAK
			
	ENDSWITCH

ENDPROC

PROC PRIVATE_UPDATE_CASINO_HEIST_FINALE_INPUTS(CASINO_HEIST_CLIENT_DATA &sClientData)

	// Dont process inputs whilst we're transitioning to another board
	IF PUBLIC_CASINO_HEIST_BOARD__IS_BIT_SET(sClientData, CASINO_HEIST_BOARD_BITSET__TRANSITIONING)
	OR sClientData.sInputData.bInputBuffer
	OR sClientData.sFinaleData.iWarningScreen != CASINO_HEIST_WARNING_SCREEN__NONE
	OR sClientData.sFinaleData.bWarningScreenBuffer
	OR sClientData.sFinaleData.sScaleformData.bGetSelectionRequested
	OR PUBLIC_IS_ANY_CASINO_HEIST_DIALOGUE_CURRENTLY_PLAYING(sClientData)
	OR (IS_PLAYER_IN_CORONA() AND GET_CORONA_STATUS() != CORONA_STATUS_CASINO_HEIST_PLANNING)
		EXIT
	ENDIF

	CASINO_HEIST_CONTROL_TYPE 			eControlType 	= PUBLIC_GET_CASINO_HEIST_INPUTS_THIS_FRAME_ONLY(sClientData.sInputData)
	CASINO_HEIST_FINALE_BOARD_ITEMS 	eBoardItem 		= INT_TO_ENUM(CASINO_HEIST_FINALE_BOARD_ITEMS, sClientData.sCursorData.iHighlight)
	
	SWITCH sClientData.sFinaleData.sStateData.eSubState
			
		CASE CHBSS_VIEWING

			SWITCH eControlType

				// Member Select / Map post-it select	
				CASE CHCT_UP
				CASE CHCT_DOWN
				CASE CHCT_LEFT
				CASE CHCT_RIGHT
				
					PRIVATE_UPDATE_CASINO_HEIST_FINALE_CONTROL_SELECTIONS(sClientData, eControlType)
					PUBLIC_CASINO_HEIST_BOARD__SET_HIGHLIGHT_UPDATE_REQUIRED(sClientData, TRUE)
					PRIVATE_CLEAR_LAUNCH_BUTTON_DISABLED_HELP()
					PUBLIC_HIDE_DETAILS_PANEL(sClientData)
					
				BREAK
				
				// Camera Pan
				CASE CHCT_LB
				
					IF NOT NETWORK_IS_ACTIVITY_SESSION()
						PUBLIC_CASINO_HEIST_BOARD__TRANSITION_TO_BOARD(sClientData, CHBT_PREP)
					ENDIF
					
				BREAK
				
				CASE CHCT_RB
				
//					IF NOT NETWORK_IS_ACTIVITY_SESSION()
//						PUBLIC_CASINO_HEIST_BOARD__TRANSITION_TO_BOARD(sClientData, CHBT_SETUP)
//					ENDIF
					
				BREAK
				
				// Accept / Modify / Ready / Unready
				CASE CHCT_A
				
					IF sClientData.piLeader = PLAYER_ID()
				
						IF PRIVATE_CASINO_HEIST_FINALE_BOARD__IS_BOARD_ITEM_LAUNCH_BUTTON(eBoardItem)
							IF NETWORK_IS_ACTIVITY_SESSION()
								// Set we're ready, move to outfit selection
								PRIVATE_LAUNCH_CASINO_HEIST_FINALE(sClientData)
								PLAY_SOUND_FRONTEND(-1,"Highlight_Accept","DLC_HEIST_PLANNING_BOARD_SOUNDS")
							ELSE
								// Launch the finale lobby
								
								PRIVATE_LAUNCH_CASINO_HEIST_FINALE_LOBBY(sClientData)
								PLAY_SOUND_FRONTEND(-1,"Highlight_Accept","DLC_HEIST_PLANNING_BOARD_SOUNDS")
							ENDIF
						ENDIF
										
						IF PRIVATE_CASINO_HEIST_FINALE_BOARD__CAN_BOARD_ITEM_BE_MODIFIED(sClientData, eBoardItem)
							PRIVATE_START_EDIT_MODE_FOR_FINALE_BOARD_ITEM(sClientData, eBoardItem)
						ENDIF
						
						IF eBoardItem = CASINO_HEIST_FINALE_BOARD_ITEM__ENTRANCE_LOCATION
						AND GET_PLAYER_CASINO_HEIST_APPROACH(sClientData.piLeader) = CASINO_HEIST_APPROACH_TYPE__SUBTERFUGE
							SET_CASINO_HEIST_PLANNING_HELP_TRIGGER(sClientData, CASINO_HEIST_PLANNING_HELP_TEXT__FINALE_EXPLAIN_SUBTERFUGE_ENTRANCE)
						ENDIF
					
					ELSE
						IF NETWORK_IS_ACTIVITY_SESSION()
							PRIVATE_TOGGLE_CASINO_HEIST_FINALE_READY_STATE(sClientData)
							PLAY_SOUND_FRONTEND(-1,"Highlight_Accept","DLC_HEIST_PLANNING_BOARD_SOUNDS")
						ENDIF
					ENDIF

				BREAK
				
				CASE CHCT_X
				
					IF sClientData.piLeader = PLAYER_ID()
						IF PRIVATE_CASINO_HEIST_FINALE_BOARD__IS_BOARD_ITEM_PURCHASEABLE(sClientData, eBoardItem)
							sClientData.sFinaleData.iWarningScreen = PRIVATE_GET_WARNING_SCREEN_FOR_FINALE_BOARD_ITEM(eBoardItem)
							PLAY_SOUND_FRONTEND(-1,"Highlight_Accept","DLC_HEIST_PLANNING_BOARD_SOUNDS")
							PRIVATE_CLEAR_LAUNCH_BUTTON_DISABLED_HELP()
						ENDIF
					ENDIF
				BREAK
				
				// Back / Cancel Modify
				CASE CHCT_B
				
					IF NETWORK_IS_ACTIVITY_SESSION()
						sClientData.sFinaleData.iWarningScreen = CASINO_HEIST_WARNING_SCREEN__QUIT_FROM_FINALE
						PLAY_SOUND_FRONTEND(-1,"Highlight_Accept","DLC_HEIST_PLANNING_BOARD_SOUNDS")
						PRIVATE_CLEAR_LAUNCH_BUTTON_DISABLED_HELP()
					ELSE
						PUBLIC_TOGGLE_CASINO_HEIST_INTERACTION(sClientData, FALSE)
					ENDIF
					
				BREAK
				
				// More info
				CASE CHCT_Y
				
					IF PUBLIC_CASINO_HEIST_BOARD_COMMON__IS_HIGHLIGHTED_BOARD_ITEM_ON_FINALE_BOARD(sClientData)
						PUBLIC_TOGGLE_DETAILS_PANEL(sClientData)
					ENDIF
					
				BREAK
				
				
				// View Profile
				CASE CHCT_SELECT
			
					IF PRIVATE_CASINO_HEIST_FINALE_BOARD__IS_BOARD_ITEM_A_PLAYER(eBoardItem)
						
						PRIVATE_CLEAR_LAUNCH_BUTTON_DISABLED_HELP()
						
						PLAYER_INDEX playerID
						GAMER_HANDLE ghSelectedPlayer	
						playerID = INT_TO_PLAYERINDEX(sClientData.sFinaleData.sFinaleConfig.iPlayerOrder[PRIVATE_GET_CREW_MEMBER_SLOT(eBoardItem)])

						CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "Inputs - Loading player profile: ", GET_PLAYER_NAME(playerID))
						
			            ghSelectedPlayer = GET_GAMER_HANDLE_PLAYER(playerID)
			            PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
			            NETWORK_SHOW_PROFILE_UI(ghSelectedPlayer)
						
					ENDIF

				BREAK
				
			ENDSWITCH
		
		BREAK
		
		CASE CHBSS_EDITING

			SWITCH eControlType
				CASE CHCT_LEFT
				CASE CHCT_RIGHT
				
					PRIVATE_UPDATE_SELECTED_FINALE_BOARD_ITEM(sClientData, eBoardItem, eControlType)
				
				BREAK
					
				CASE CHCT_A
				
					IF NETWORK_IS_ACTIVITY_SESSION()
						// Confirm your selected crew member
						PRIVATE_STOP_EDIT_MODE_FOR_FINALE_BOARD_ITEM(sClientData, eBoardItem, TRUE)
					ENDIF
					
				BREAK
				
				CASE CHCT_B
				
					// Leave edit mode
					PRIVATE_STOP_EDIT_MODE_FOR_FINALE_BOARD_ITEM(sClientData, eBoardItem)

				BREAK
				
				CASE CHCT_Y
					IF PUBLIC_CASINO_HEIST_BOARD_COMMON__IS_HIGHLIGHTED_BOARD_ITEM_ON_FINALE_BOARD(sClientData)
						PUBLIC_TOGGLE_DETAILS_PANEL(sClientData)
					ENDIF
				BREAK
				
			ENDSWITCH
		
		BREAK
	
	ENDSWITCH

ENDPROC

/// PURPOSE:
///    Get the headset/voice state of the player passed in. Based loosely on the voice controls used in the corona.
FUNC INT PRIVATE_GET_CASINO_HEIST_FINALE_PLAYER_HEADSET_STATUS(PLAYER_INDEX playerID)
		
	IF playerID != INVALID_PLAYER_INDEX()
	AND NETWORK_IS_PLAYER_ACTIVE(playerID)
			
		GAMER_HANDLE ghTargetPlayer = GET_GAMER_HANDLE_PLAYER(playerID)
		
		// Is the gamer we are going to check valid?
		IF IS_GAMER_HANDLE_VALID(ghTargetPlayer)
		
			// Check to see if we can communicate with the target player.
			IF NETWORK_CAN_COMMUNICATE_WITH_GAMER(ghTargetPlayer)
				IF NETWORK_IS_GAMER_TALKING(ghTargetPlayer)
					RETURN ENUM_TO_INT(ACTIVE_HEADSET)
				ELIF NETWORK_IS_GAMER_MUTED_BY_ME(ghTargetPlayer)
					RETURN ENUM_TO_INT(MUTED_HEADSET)
				ELSE
					RETURN ENUM_TO_INT(INACTIVE_HEADSET)
				ENDIF
			ELSE
				IF NETWORK_IS_GAMER_MUTED_BY_ME(ghTargetPlayer)
					RETURN ENUM_TO_INT(MUTED_HEADSET)
				ELSE
					RETURN ENUM_TO_INT(ICON_EMPTY)
				ENDIF
			ENDIF
		ENDIF	
	
	ENDIF
	
	RETURN ENUM_TO_INT(ICON_EMPTY)
	
ENDFUNC

FUNC BOOL PRIVATE_SHOULD_SET_FINALE_BOARD_MEMBER_DETAILS(CASINO_HEIST_CLIENT_DATA &sClientData, PLAYER_INDEX playerID)
	IF NETWORK_IS_ACTIVITY_SESSION()
		RETURN TRUE
	ELIF (playerID = sClientData.piLeader)
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

PROC PRIVATE_UPDATE_CASINO_HEIST_FINALE_BOARD_ITEMS(CASINO_HEIST_CLIENT_DATA &sClientData)

	IF NOT CAN_PLAYER_USE_THIS_CASINO_HEIST_PLANNING_BOARD(sClientData, CHBT_FINALE)
		EXIT
	ENDIF
	
	INT index
	INT iCut
	PLAYER_INDEX playerID
	TEXT_LABEL_23 tlPortrait
	CASINO_HEIST_FINALE_BOARD_ITEMS eItemSlot
	BOOL bUpdateMembersDetails = PRIVATE_CASINO_HEIST_FINALE_BOARD__IS_CREW_MEMBER_DETAILS_UPDATE_REQUIRED(sClientData)
	
	FOR index = 0 TO (MAX_CASINO_HEIST_PLAYERS-1)
		IF sClientData.sFinaleData.sFinaleConfig.iPlayerOrder[index] = CASINO_HEIST_INVALID
			RELOOP
		ENDIF
		
		IF NETWORK_IS_ACTIVITY_SESSION()
			playerID = INT_TO_PLAYERINDEX(sClientData.sFinaleData.sFinaleConfig.iPlayerOrder[index])
		ELSE
			playerID = sClientData.piLeader
		ENDIF
		
		tlPortrait = PRIVATE_GET_CASINO_HEIST_FINALE_HEADSHOT(sClientData, playerID)
		
		eItemSlot = PUBLIC_CASINO_HEIST_FINALE_BOARD__GET_CREW_SLOT_FOR_INDEX(index)
		
		iCut = sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[index]
		
		IF PRIVATE_SHOULD_SET_FINALE_BOARD_MEMBER_DETAILS(sClientData, playerID)
		
			IF bUpdateMembersDetails
				PUBLIC_CASINO_HEIST_FINALE_BOARD__SET_CREW_MEMBER_DETAILS(sClientData.sFinaleData.sScaleformData.siMainBoard, 
																			eItemSlot, 
																			GET_PLAYER_NAME(playerID), 
																			tlPortrait)
																			
				PUBLIC_CASINO_HEIST_BOARD_COMMON__SET_BUTTON_VISIBLE(sClientData.sFinaleData.sScaleformData.siMainBoard,
																		ENUM_TO_INT(PRIVATE_GET_CREW_MEMBER_BOARD_ITEM(index)),
																		TRUE)
																		
				PUBLIC_CASINO_HEIST_BOARD_COMMON__SET_BUTTON_ENABLED(sClientData.sFinaleData.sScaleformData.siMainBoard,
																		ENUM_TO_INT(PRIVATE_GET_CREW_MEMBER_BOARD_ITEM(index)),
																		TRUE)
			ENDIF
			
			PUBLIC_CASINO_HEIST_FINALE_BOARD__SET_CREW_MEMBER_STATE(sClientData.sFinaleData.sScaleformData.siMainBoard, 
																		eItemSlot, 
																		PRIVATE_IS_CASINO_HEIST_FINALE_PLAYER_READY(sClientData, playerID),
																		PRIVATE_GET_CASINO_HEIST_FINALE_PLAYER_HEADSET_STATUS(playerID))
																		
			PUBLIC_CASINO_HEIST_FINALE_BOARD__SET_CREW_MEMBER_CUT(sClientData.sFinaleData.sScaleformData.siMainBoard, 
																		eItemSlot, 
																		iCut)
																		
		ELSE
			IF bUpdateMembersDetails
				PUBLIC_CASINO_HEIST_BOARD_COMMON__SET_BUTTON_VISIBLE(sClientData.sFinaleData.sScaleformData.siMainBoard,
																		ENUM_TO_INT(PRIVATE_GET_CREW_MEMBER_BOARD_ITEM(index)),
																		FALSE)
																		
				PUBLIC_CASINO_HEIST_BOARD_COMMON__SET_BUTTON_ENABLED(sClientData.sFinaleData.sScaleformData.siMainBoard,
																		ENUM_TO_INT(PRIVATE_GET_CREW_MEMBER_BOARD_ITEM(index)),
																		FALSE)
			ENDIF
		ENDIF
	ENDFOR
	
	IF bUpdateMembersDetails
		PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_CREW_MEMBER_DETAILS_UPDATE_REQUIRED(sClientData, FALSE)
	ENDIF
	
	IF PRIVATE_CASINO_HEIST_FINALE_BOARD__IS_CREW_MEMBER_PANEL_VISIBLE_UPDATE_REQUIRED(sClientData)
	OR GET_CASINO_HEIST_FINALE_BOARD_ITEMS_SHOULD_UPDATE()
	
		IF NETWORK_IS_ACTIVITY_SESSION()
			PUBLIC_CASINO_HEIST_FINALE_BOARD__SET_LAUNCH_BUTTON_LABEL(sClientData.sFinaleData.sScaleformData.siMainBoard, "CH_FINALE_LBLH")
			PUBLIC_CASINO_HEIST_BOARD_COMMON__SET_BUTTON_GREYED_OUT(sClientData.sFinaleData.sScaleformData.siMainBoard, ENUM_TO_INT(CASINO_HEIST_FINALE_BOARD_ITEM__LAUNCH_HEIST_BUTTON), TRUE)
		ELSE
			PUBLIC_CASINO_HEIST_FINALE_BOARD__SET_LAUNCH_BUTTON_LABEL(sClientData.sFinaleData.sScaleformData.siMainBoard, "CH_FINALE_LBS")
			PUBLIC_CASINO_HEIST_BOARD_COMMON__SET_BUTTON_GREYED_OUT(sClientData.sFinaleData.sScaleformData.siMainBoard, ENUM_TO_INT(CASINO_HEIST_FINALE_BOARD_ITEM__LAUNCH_HEIST_BUTTON), FALSE)
		ENDIF
	
		PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_CREW_MEMBER_PANEL_VISIBLE_UPDATE_REQUIRED(sClientData, FALSE)
	ENDIF

	IF PRIVATE_CASINO_HEIST_FINALE_BOARD__IS_TODO_LIST_UPDATE_REQUIRED(sClientData)
	OR GET_CASINO_HEIST_FINALE_BOARD_ITEMS_SHOULD_UPDATE()
	
		IF NETWORK_IS_ACTIVITY_SESSION()
	
			PUBLIC_CASINO_HEIST_BOARD_COMMON__CLEAR_TODO_LIST(sClientData.sFinaleData.sScaleformData.siMainBoard)
			
			REPEAT ciCASINO_HEIST_MAX_FINALE_BOARD_TODO_LIST_ITEMS index
				IF NOT IS_STRING_NULL_OR_EMPTY(PRIVATE_GET_FINALE_BOARD_TODO_LIST_ITEM(sClientData, index))
					PUBLIC_CASINO_HEIST_BOARD_COMMON__ADD_TODO_LIST_ITEM(sClientData.sFinaleData.sScaleformData.siMainBoard,
																		PRIVATE_GET_FINALE_BOARD_TODO_LIST_ITEM(sClientData, index),
																		PRIVATE_GET_FINALE_BOARD_TODO_LIST_ITEM_COMPLETED(sClientData, index))
				ENDIF
			ENDREPEAT
			
		ENDIF
	
		PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_TODO_LIST_UPDATE_REQUIRED(sClientData, FALSE)
	ENDIF
	
	IF PRIVATE_CASINO_HEIST_FINALE_BOARD__IS_OPTIONAL_LIST_UPDATE_REQUIRED(sClientData)
	OR GET_CASINO_HEIST_FINALE_BOARD_ITEMS_SHOULD_UPDATE()
	
		IF NETWORK_IS_ACTIVITY_SESSION()
	
			PUBLIC_CASINO_HEIST_BOARD_COMMON__CLEAR_OPTIONAL_LIST(sClientData.sFinaleData.sScaleformData.siMainBoard)
		
			REPEAT ciCASINO_HEIST_MAX_FINALE_BOARD_OPTIONAL_LIST_ITEMS index
				IF NOT IS_STRING_NULL_OR_EMPTY(PRIVATE_GET_FINALE_BOARD_OPTIONAL_LIST_ITEM(sClientData, index))
					PUBLIC_CASINO_HEIST_BOARD_COMMON__ADD_OPTIONAL_LIST_ITEM(sClientData.sFinaleData.sScaleformData.siMainBoard,
																		PRIVATE_GET_FINALE_BOARD_OPTIONAL_LIST_ITEM(sClientData, index),
																		PRIVATE_GET_FINALE_BOARD_OPTIONAL_LIST_ITEM_COMPLETED(sClientData, index))
				ENDIF
			ENDREPEAT
			
		ENDIF
	
		PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_OPTIONAL_LIST_UPDATE_REQUIRED(sClientData, FALSE)
	ENDIF
	
	IF PRIVATE_CASINO_HEIST_FINALE_BOARD__IS_HEADING_UPDATE_REQUIRED(sClientData)
	OR GET_CASINO_HEIST_FINALE_BOARD_ITEMS_SHOULD_UPDATE()
	
		PUBLIC_CASINO_HEIST_FINALE_BOARD__SET_HEADINGS(sClientData.sFinaleData.sScaleformData.siMainBoard,
														PUBLIC_GET_APPROACH_LABEL(PUBLIC_GET_CHOSEN_APPROACH(sClientData)), 
														PUBLIC_GET_TARGET_LABEL(GET_PLAYER_CASINO_HEIST_TARGET(sClientData.piLeader)),
														PUBLIC_GET_CASINO_HEIST_SETUP_COST(sClientData),
														PRIVATE_GET_POTENTIAL_TAKE(sClientData),
														PRIVATE_GET_SUPPORT_CREW_CUT(sClientData),
														PRIVATE_GET_ACCESS_POINT_LABEL(sClientData, INT_TO_ENUM(CASINO_HEIST_ACCESS_POINT, sClientData.sFinaleData.iEntranceDisplaying)),
														PRIVATE_GET_ACCESS_POINT_LABEL(sClientData, INT_TO_ENUM(CASINO_HEIST_ACCESS_POINT, sClientData.sFinaleData.iExitDisplaying)),
														PRIVATE_GET_DROP_OFF_LABEL(INT_TO_ENUM(CASINO_HEIST_DROPOFF_LOCATIONS, sClientData.sFinaleData.iDropOffDisplaying)),
														PRIVATE_GET_OUTFIT_LABEL(INT_TO_ENUM(CASINO_HEIST_OUTFITS, sClientData.sFinaleData.iOutfitInDisplaying)),
														PRIVATE_GET_OUTFIT_LABEL(INT_TO_ENUM(CASINO_HEIST_OUTFITS, sClientData.sFinaleData.iOutfitOutDisplaying)))
	
		PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_HEADING_UPDATE_REQUIRED(sClientData, FALSE)
	ENDIF
	
	IF PRIVATE_CASINO_HEIST_FINALE_BOARD__IS_BUTTON_VISIBILITY_UPDATE_REQUIRED(sClientData)
	OR GET_CASINO_HEIST_FINALE_BOARD_ITEMS_SHOULD_UPDATE()
		
		CASINO_HEIST_FINALE_BOARD_ITEMS eBoardItem
		REPEAT CASINO_HEIST_FINALE_BOARD_ITEM__MAX eBoardItem
			PUBLIC_CASINO_HEIST_BOARD_COMMON__SET_BUTTON_VISIBLE(sClientData.sFinaleData.sScaleformData.siMainBoard, ENUM_TO_INT(eBoardItem), PRIVATE_SHOULD_FINALE_BOARD_ITEM_BE_VISIBLE(sClientData, eBoardItem))
		ENDREPEAT
		
		PUBLIC_CASINO_HEIST_BOARD_COMMON__SET_TICK(sClientData.sFinaleData.sScaleformData.siMainBoard, ENUM_TO_INT(CASINO_HEIST_FINALE_BOARD_ITEM__DECOY_VEHICLE), HAS_PLAYER_PURCHASED_CASINO_HEIST_DECOY_CREW_MEMBER(sClientData.piLeader))
		PUBLIC_CASINO_HEIST_BOARD_COMMON__SET_TICK(sClientData.sFinaleData.sScaleformData.siMainBoard, ENUM_TO_INT(CASINO_HEIST_FINALE_BOARD_ITEM__SWITCH_VEHICLE), HAS_PLAYER_PURCHASED_CASINO_HEIST_SWITCH_GETAWAY_VEHICLE(sClientData.piLeader))
	
		PUBLIC_CASINO_HEIST_BOARD_COMMON__SET_BUTTON_IMAGE(sClientData.sFinaleData.sScaleformData.siMainBoard, ENUM_TO_INT(CASINO_HEIST_FINALE_BOARD_ITEM__DECOY_VEHICLE), 1)
		PUBLIC_CASINO_HEIST_BOARD_COMMON__SET_BUTTON_IMAGE(sClientData.sFinaleData.sScaleformData.siMainBoard, ENUM_TO_INT(CASINO_HEIST_FINALE_BOARD_ITEM__SWITCH_VEHICLE), 1)
	
		PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_BUTTON_VISIBILITY_UPDATE_REQUIRED(sClientData, FALSE)
	ENDIF
	
	IF PRIVATE_CASINO_HEIST_FINALE_BOARD__IS_ENTRANCE_BUTTON_UPDATE_REQUIRED(sClientData)
	OR GET_CASINO_HEIST_FINALE_BOARD_ITEMS_SHOULD_UPDATE()
	
		PUBLIC_CASINO_HEIST_BOARD_COMMON__SET_BUTTON_IMAGE(sClientData.sFinaleData.sScaleformData.siMainBoard, 
															ENUM_TO_INT(CASINO_HEIST_FINALE_BOARD_ITEM__ENTRANCE_LOCATION),
															PRIVATE_GET_FINALE_ENTRANCE_IMAGE_ID(sClientData))
	
		IF sClientData.sFinaleData.iEntranceDisplaying = ENUM_TO_INT(CASINO_HEIST_ACCESS_POINT__NONE)
			PUBLIC_CASINO_HEIST_FINALE_BOARD__SET_NOT_SELECTED_PAPER_VISIBLE(sClientData.sFinaleData.sScaleformData.siMainBoard, ENUM_TO_INT(CASINO_HEIST_FINALE_BOARD_ITEM__ENTRANCE_LOCATION), TRUE)
		ELSE
			PUBLIC_CASINO_HEIST_FINALE_BOARD__SET_NOT_SELECTED_PAPER_VISIBLE(sClientData.sFinaleData.sScaleformData.siMainBoard, ENUM_TO_INT(CASINO_HEIST_FINALE_BOARD_ITEM__ENTRANCE_LOCATION), FALSE)
		ENDIF
		
		IF GET_PLAYER_CASINO_HEIST_APPROACH(sClientData.piLeader) = CASINO_HEIST_APPROACH_TYPE__SUBTERFUGE
			PUBLIC_CASINO_HEIST_BOARD_COMMON__SET_SELECTION_ARROWS_STATE(sClientData.sFinaleData.sScaleformData.siMainBoard, ENUM_TO_INT(CASINO_HEIST_FINALE_BOARD_ITEM__ENTRANCE_LOCATION), CASINO_HEIST_SELECTION_ARROW_STATE__INVISIBLE)
		ENDIF
	
		PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_ENTRANCE_BUTTON_UPDATE_REQUIRED(sClientData, FALSE)
	ENDIF
	
	IF PRIVATE_CASINO_HEIST_FINALE_BOARD__IS_EXIT_BUTTON_UPDATE_REQUIRED(sClientData)
	OR GET_CASINO_HEIST_FINALE_BOARD_ITEMS_SHOULD_UPDATE()
	
		PUBLIC_CASINO_HEIST_BOARD_COMMON__SET_BUTTON_IMAGE(sClientData.sFinaleData.sScaleformData.siMainBoard, 
															ENUM_TO_INT(CASINO_HEIST_FINALE_BOARD_ITEM__EXIT_LOCATION),
															PRIVATE_GET_FINALE_EXIT_IMAGE_ID(sClientData))
	
		IF sClientData.sFinaleData.iExitDisplaying = ENUM_TO_INT(CASINO_HEIST_ACCESS_POINT__NONE)
			PUBLIC_CASINO_HEIST_FINALE_BOARD__SET_NOT_SELECTED_PAPER_VISIBLE(sClientData.sFinaleData.sScaleformData.siMainBoard, ENUM_TO_INT(CASINO_HEIST_FINALE_BOARD_ITEM__EXIT_LOCATION), TRUE)
		ELSE
			PUBLIC_CASINO_HEIST_FINALE_BOARD__SET_NOT_SELECTED_PAPER_VISIBLE(sClientData.sFinaleData.sScaleformData.siMainBoard, ENUM_TO_INT(CASINO_HEIST_FINALE_BOARD_ITEM__EXIT_LOCATION), FALSE)
		ENDIF
	
		PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_EXIT_BUTTON_UPDATE_REQUIRED(sClientData, FALSE)
	ENDIF
	
	IF PRIVATE_CASINO_HEIST_FINALE_BOARD__IS_DROP_OFF_BUTTON_UPDATE_REQUIRED(sClientData)
	OR GET_CASINO_HEIST_FINALE_BOARD_ITEMS_SHOULD_UPDATE()
	
		PUBLIC_CASINO_HEIST_BOARD_COMMON__SET_BUTTON_IMAGE(sClientData.sFinaleData.sScaleformData.siMainBoard, 
															ENUM_TO_INT(CASINO_HEIST_FINALE_BOARD_ITEM__DROP_OFF_LOCATION),
															PRIVATE_GET_FINALE_DROP_OFF_IMAGE_ID(sClientData))
	
		IF sClientData.sFinaleData.iDropOffDisplaying = ENUM_TO_INT(CASINO_HEIST_DROPOFF_LOCATION__NONE)
			PUBLIC_CASINO_HEIST_FINALE_BOARD__SET_NOT_SELECTED_PAPER_VISIBLE(sClientData.sFinaleData.sScaleformData.siMainBoard, ENUM_TO_INT(CASINO_HEIST_FINALE_BOARD_ITEM__DROP_OFF_LOCATION), TRUE)
		ELSE
			PUBLIC_CASINO_HEIST_FINALE_BOARD__SET_NOT_SELECTED_PAPER_VISIBLE(sClientData.sFinaleData.sScaleformData.siMainBoard, ENUM_TO_INT(CASINO_HEIST_FINALE_BOARD_ITEM__DROP_OFF_LOCATION), FALSE)
		ENDIF
		
		PUBLIC_CASINO_HEIST_FINALE_BOARD__SET_MAP_MARKER_STATE(sClientData.sFinaleData.sScaleformData.siMainBoard, PRIVATE_GET_MAP_MARKER_STATE_FOR_DROPOFF(sClientData))
	
		PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_DROP_OFF_BUTTON_UPDATE_REQUIRED(sClientData, FALSE)
	ENDIF
	
	IF PRIVATE_CASINO_HEIST_FINALE_BOARD__IS_OUTFIT_IN_BUTTON_UPDATE_REQUIRED(sClientData)
	OR GET_CASINO_HEIST_FINALE_BOARD_ITEMS_SHOULD_UPDATE()
	
		IF GET_PLAYER_CASINO_HEIST_APPROACH(sClientData.piLeader) = CASINO_HEIST_APPROACH_TYPE__SUBTERFUGE
			PUBLIC_CASINO_HEIST_BOARD_COMMON__SET_BUTTON_IMAGE(sClientData.sFinaleData.sScaleformData.siMainBoard, 
																ENUM_TO_INT(CASINO_HEIST_FINALE_BOARD_ITEM__OUTFIT_IN),
																PRIVATE_GET_FINALE_OUTFIT_IN_IMAGE_ID(sClientData))
																
			IF sClientData.sFinaleData.iOutfitInDisplaying = ENUM_TO_INT(CASINO_HEIST_OUTFIT_IN__NONE)
				PUBLIC_CASINO_HEIST_FINALE_BOARD__SET_NOT_SELECTED_PAPER_VISIBLE(sClientData.sFinaleData.sScaleformData.siMainBoard, ENUM_TO_INT(CASINO_HEIST_FINALE_BOARD_ITEM__OUTFIT_IN), TRUE)
			ELSE
				PUBLIC_CASINO_HEIST_FINALE_BOARD__SET_NOT_SELECTED_PAPER_VISIBLE(sClientData.sFinaleData.sScaleformData.siMainBoard, ENUM_TO_INT(CASINO_HEIST_FINALE_BOARD_ITEM__OUTFIT_IN), FALSE)
			ENDIF
			
			IF NOT NETWORK_IS_ACTIVITY_SESSION()
				IF PRIVATE_ARE_MULTIPLE_ENTRY_DISGUISES_AVAILABLE(sClientData)
					PUBLIC_CASINO_HEIST_BOARD_COMMON__SET_SELECTION_ARROWS_STATE(sClientData.sFinaleData.sScaleformData.siMainBoard, ENUM_TO_INT(CASINO_HEIST_FINALE_BOARD_ITEM__OUTFIT_IN), CASINO_HEIST_SELECTION_ARROW_STATE__VISIBLE)
				ELSE
					PUBLIC_CASINO_HEIST_BOARD_COMMON__SET_SELECTION_ARROWS_STATE(sClientData.sFinaleData.sScaleformData.siMainBoard, ENUM_TO_INT(CASINO_HEIST_FINALE_BOARD_ITEM__OUTFIT_IN), CASINO_HEIST_SELECTION_ARROW_STATE__INVISIBLE)
				ENDIF
			ENDIF
																
		ELSE
			PUBLIC_CASINO_HEIST_BOARD_COMMON__SET_BUTTON_VISIBLE(sClientData.sFinaleData.sScaleformData.siMainBoard, 
																ENUM_TO_INT(CASINO_HEIST_FINALE_BOARD_ITEM__OUTFIT_IN),
																FALSE)
		ENDIF
	
		PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_OUTFIT_IN_BUTTON_UPDATE_REQUIRED(sClientData, FALSE)
	ENDIF
	
	IF PRIVATE_CASINO_HEIST_FINALE_BOARD__IS_OUTFIT_OUT_BUTTON_UPDATE_REQUIRED(sClientData)
	OR GET_CASINO_HEIST_FINALE_BOARD_ITEMS_SHOULD_UPDATE()
	
		IF GET_PLAYER_CASINO_HEIST_APPROACH(sClientData.piLeader) = CASINO_HEIST_APPROACH_TYPE__SUBTERFUGE
			PUBLIC_CASINO_HEIST_BOARD_COMMON__SET_BUTTON_IMAGE(sClientData.sFinaleData.sScaleformData.siMainBoard, 
																ENUM_TO_INT(CASINO_HEIST_FINALE_BOARD_ITEM__OUTFIT_OUT),
																PRIVATE_GET_FINALE_OUTFIT_OUT_IMAGE_ID(sClientData))
																
			IF sClientData.sFinaleData.iOutfitOutDisplaying = ENUM_TO_INT(CASINO_HEIST_OUTFIT_OUT__NONE)
				PUBLIC_CASINO_HEIST_FINALE_BOARD__SET_NOT_SELECTED_PAPER_VISIBLE(sClientData.sFinaleData.sScaleformData.siMainBoard, ENUM_TO_INT(CASINO_HEIST_FINALE_BOARD_ITEM__OUTFIT_OUT), TRUE)
			ELSE
				PUBLIC_CASINO_HEIST_FINALE_BOARD__SET_NOT_SELECTED_PAPER_VISIBLE(sClientData.sFinaleData.sScaleformData.siMainBoard, ENUM_TO_INT(CASINO_HEIST_FINALE_BOARD_ITEM__OUTFIT_OUT), FALSE)
			ENDIF
			
			IF NOT NETWORK_IS_ACTIVITY_SESSION()
				IF PRIVATE_ARE_MULTIPLE_EXIT_DISGUISES_AVAILABLE(sClientData)
					PUBLIC_CASINO_HEIST_BOARD_COMMON__SET_SELECTION_ARROWS_STATE(sClientData.sFinaleData.sScaleformData.siMainBoard, ENUM_TO_INT(CASINO_HEIST_FINALE_BOARD_ITEM__OUTFIT_OUT), CASINO_HEIST_SELECTION_ARROW_STATE__VISIBLE)
				ELSE
					PUBLIC_CASINO_HEIST_BOARD_COMMON__SET_SELECTION_ARROWS_STATE(sClientData.sFinaleData.sScaleformData.siMainBoard, ENUM_TO_INT(CASINO_HEIST_FINALE_BOARD_ITEM__OUTFIT_OUT), CASINO_HEIST_SELECTION_ARROW_STATE__INVISIBLE)
				ENDIF
			ENDIF
			
		ELSE
			PUBLIC_CASINO_HEIST_BOARD_COMMON__SET_BUTTON_VISIBLE(sClientData.sFinaleData.sScaleformData.siMainBoard, 
																ENUM_TO_INT(CASINO_HEIST_FINALE_BOARD_ITEM__OUTFIT_OUT),
																FALSE)
		ENDIF
	
		PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_OUTFIT_OUT_BUTTON_UPDATE_REQUIRED(sClientData, FALSE)
	ENDIF
	
	IF PRIVATE_CASINO_HEIST_FINALE_BOARD__IS_WEAPON_BUTTON_UPDATE_REQUIRED(sClientData)
	OR GET_CASINO_HEIST_FINALE_BOARD_ITEMS_SHOULD_UPDATE()
	
		PUBLIC_CASINO_HEIST_BOARD_COMMON__SET_BUTTON_IMAGE(sClientData.sFinaleData.sScaleformData.siMainBoard, 
															ENUM_TO_INT(CASINO_HEIST_FINALE_BOARD_ITEM__WEAPON),
															PRIVATE_GET_FINALE_WEAPON_IMAGE_ID(sClientData))
	
		PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_WEAPON_BUTTON_UPDATE_REQUIRED(sClientData, FALSE)
	ENDIF
	
	IF PRIVATE_CASINO_HEIST_FINALE_BOARD__IS_VEHICLE_BUTTON_UPDATE_REQUIRED(sClientData)
	OR GET_CASINO_HEIST_FINALE_BOARD_ITEMS_SHOULD_UPDATE()
	
		PUBLIC_CASINO_HEIST_BOARD_COMMON__SET_BUTTON_IMAGE(sClientData.sFinaleData.sScaleformData.siMainBoard, 
															ENUM_TO_INT(CASINO_HEIST_FINALE_BOARD_ITEM__GETAWAY_VEHICLE),
															PRIVATE_GET_FINALE_VEHICLE_IMAGE_ID(sClientData))
	
		PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_VEHICLE_BUTTON_UPDATE_REQUIRED(sClientData, FALSE)
	ENDIF
	
	IF PRIVATE_CASINO_HEIST_FINALE_BOARD__IS_HACKING_BUTTON_UPDATE_REQUIRED(sClientData)
	OR GET_CASINO_HEIST_FINALE_BOARD_ITEMS_SHOULD_UPDATE()
	
		PUBLIC_CASINO_HEIST_BOARD_COMMON__SET_BUTTON_IMAGE(sClientData.sFinaleData.sScaleformData.siMainBoard, 
															ENUM_TO_INT(CASINO_HEIST_FINALE_BOARD_ITEM__HACKING_DEVICE),
															PRIVATE_GET_FINALE_HACKING_IMAGE_ID(sClientData))
	
		PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_HACKING_BUTTON_UPDATE_REQUIRED(sClientData, FALSE)
	ENDIF
	
	SET_CASINO_HEIST_FINALE_BOARD_ITEMS_SHOULD_UPDATE(FALSE)

ENDPROC

PROC PRIVATE_UPDATE_CASINO_HEIST_FINALE_LAUNCH_REQUIREMENTS(CASINO_HEIST_CLIENT_DATA &sClientData)
	
	IF sClientData.piLeader != PLAYER_ID()
		EXIT
	ENDIF
	
	IF NOT NETWORK_IS_ACTIVITY_SESSION()
		EXIT
	ENDIF
	
	BOOL bCanLaunch = TRUE
	BOOL bAllReady = TRUE
	INT index

	// Leader has already selected to launch, no need to run checks.
	IF sClientData.sFinaleData.sFinaleConfig.iReadyState[0] = CASINO_HEIST_FINALE_READY
		EXIT
	ENDIF

	sClientData.sFinaleData.sFinaleConfig.iLaunchButtonDisabledReason = CASINO_HEIST_LAUNCH_BUTTON_DISABLED_REASON_NO_REASON

	// Requirement checks in order of computational expense.

	IF sClientData.sFinaleData.sStateData.eSubState = CHBSS_EDITING
		bCanLaunch = FALSE
		sClientData.sFinaleData.sFinaleConfig.iLaunchButtonDisabledReason = CASINO_HEIST_LAUNCH_BUTTON_DISABLED_REASON_EDITING
		
		CDEBUG2LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_UPDATE_CASINO_HEIST_FINALE_LAUNCH_REQUIREMENTS - FAILED - In Edit mode.")
		
	ENDIF
	
	IF bCanLaunch
	
		IF sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[MAX_CASINO_HEIST_PLAYERS] > 0
			bCanLaunch = FALSE
			sClientData.sFinaleData.sFinaleConfig.iLaunchButtonDisabledReason = CASINO_HEIST_LAUNCH_BUTTON_DISABLED_REASON_MONEY_IN_KITTY
			
			CDEBUG2LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_UPDATE_CASINO_HEIST_FINALE_LAUNCH_REQUIREMENTS - FAILED - Money still in the kitty.")
		ENDIF
		
	ENDIF
	
	IF bCanLaunch
		IF sClientData.sFinaleData.iDropOffDisplayingCached = ENUM_TO_INT(CASINO_HEIST_DROPOFF_LOCATION__NONE)
			bCanLaunch = FALSE	
			sClientData.sFinaleData.sFinaleConfig.iLaunchButtonDisabledReason = CASINO_HEIST_LAUNCH_BUTTON_DISABLED_REASON_NO_DROPOFF_SELECTED
			
			CDEBUG2LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_UPDATE_CASINO_HEIST_FINALE_LAUNCH_REQUIREMENTS - FAILED - Drop off location / Buyer has not been chosen")
		ENDIF
	ENDIF
	
	IF bCanLaunch
		IF sClientData.sFinaleData.iEntranceDisplayingCached = ENUM_TO_INT(CASINO_HEIST_ACCESS_POINT__NONE)
			bCanLaunch = FALSE	
			sClientData.sFinaleData.sFinaleConfig.iLaunchButtonDisabledReason = CASINO_HEIST_LAUNCH_BUTTON_DISABLED_REASON_NO_ENTRANCE_SELECTED
			
			CDEBUG2LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_UPDATE_CASINO_HEIST_FINALE_LAUNCH_REQUIREMENTS - FAILED - Entrance has not been chosen")
		ENDIF
	ENDIF
	
	IF bCanLaunch
		IF sClientData.sFinaleData.iExitDisplayingCached = ENUM_TO_INT(CASINO_HEIST_ACCESS_POINT__NONE)
			bCanLaunch = FALSE	
			sClientData.sFinaleData.sFinaleConfig.iLaunchButtonDisabledReason = CASINO_HEIST_LAUNCH_BUTTON_DISABLED_REASON_NO_EXIT_SELECTED
			
			CDEBUG2LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_UPDATE_CASINO_HEIST_FINALE_LAUNCH_REQUIREMENTS - FAILED - Entrance has not been chosen")
		ENDIF
	ENDIF
	
	IF bCanLaunch
		IF GET_PLAYER_CASINO_HEIST_APPROACH(sClientData.piLeader) = CASINO_HEIST_APPROACH_TYPE__SUBTERFUGE
			IF sClientData.sFinaleData.iOutfitInDisplayingCached = ENUM_TO_INT(CASINO_HEIST_OUTFIT_IN__NONE)
				bCanLaunch = FALSE	
				sClientData.sFinaleData.sFinaleConfig.iLaunchButtonDisabledReason = CASINO_HEIST_LAUNCH_BUTTON_DISABLED_REASON_NO_OUTFIT_IN_SELECTED
				
				CDEBUG2LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_UPDATE_CASINO_HEIST_FINALE_LAUNCH_REQUIREMENTS - FAILED - Outfit In has not been chosen")
			ENDIF
		ENDIF
	ENDIF
	
	IF bCanLaunch
		IF sClientData.sFinaleData.sFinaleConfig.bLaunchButtonVisible = FALSE
			CDEBUG2LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_UPDATE_CASINO_HEIST_FINALE_LAUNCH_REQUIREMENTS - Criteria met for launch button being made visible!")
			PUBLIC_CASINO_HEIST_BOARD_COMMON__SET_BUTTON_ENABLED(sClientData.sFinaleData.sScaleformData.siMainBoard, ENUM_TO_INT(CASINO_HEIST_FINALE_BOARD_ITEM__LAUNCH_HEIST_BUTTON), TRUE)
			PUBLIC_CASINO_HEIST_BOARD_COMMON__SET_BUTTON_VISIBLE(sClientData.sFinaleData.sScaleformData.siMainBoard, ENUM_TO_INT(CASINO_HEIST_FINALE_BOARD_ITEM__LAUNCH_HEIST_BUTTON), TRUE)
			PUBLIC_SET_CASINO_HEIST_DO_INSTR_BTN_UPDATE(sClientData.sFinaleData.sScaleformData, TRUE)
			SET_BIT(GlobalplayerBD_CasinoHeist[NETWORK_PLAYER_ID_TO_INT()].iListSyncBitSet, CASINO_HEIST_LIST_SYNC_BITSET__SELECTIONS_MADE)
			SET_BIT(sClientData.sFinaleData.sFinaleConfig.iListSyncBitSet, CASINO_HEIST_LIST_SYNC_BITSET__SELECTIONS_MADE) 
			GlobalplayerBD_CasinoHeist[NETWORK_PLAYER_ID_TO_INT()].iSyncId++
			sClientData.sFinaleData.sFinaleConfig.bLaunchButtonVisible = TRUE
		ENDIF
	ENDIF
	
	IF bCanLaunch
	
		FOR index = 1 TO (MAX_CASINO_HEIST_PLAYERS-1)
			
			IF sClientData.sFinaleData.sFinaleConfig.iPlayerOrder[index] = CASINO_HEIST_INVALID
				RELOOP
			ENDIF
			
			IF NOT PRIVATE_IS_CASINO_HEIST_FINALE_PLAYER_READY(	sClientData, 
															INT_TO_PLAYERINDEX(sClientData.sFinaleData.sFinaleConfig.iPlayerOrder[index]))
				bCanLaunch = FALSE
				bAllReady = FALSE
				sClientData.sFinaleData.sFinaleConfig.iLaunchButtonDisabledReason = CASINO_HEIST_LAUNCH_BUTTON_DISABLED_REASON_SOMEONE_NOT_READY
				
				CDEBUG2LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_UPDATE_CASINO_HEIST_FINALE_LAUNCH_REQUIREMENTS - FAILED - ", 
														GET_PLAYER_NAME(INT_TO_PLAYERINDEX(sClientData.sFinaleData.sFinaleConfig.iPlayerOrder[index])), " is not ready.")
				
				BREAKLOOP
			ENDIF
				
		ENDFOR
	
	ENDIF
	
	IF bCanLaunch
	
		IF NOT bAllReady
			bCanLaunch = FALSE	
		ENDIF
		
	ENDIF
	
	// Requirements processed, perform actions based on the result.
	
	IF bCanLaunch
		IF sClientData.sFinaleData.sFinaleConfig.bLaunchButtonEnabled = FALSE
			PUBLIC_CASINO_HEIST_BOARD_COMMON__SET_BUTTON_GREYED_OUT(sClientData.sFinaleData.sScaleformData.siMainBoard, ENUM_TO_INT(CASINO_HEIST_FINALE_BOARD_ITEM__LAUNCH_HEIST_BUTTON), FALSE)
			CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_UPDATE_CASINO_HEIST_FINALE_LAUNCH_REQUIREMENTS - Launch requirements have been met, launch button made visible and enabled. Heist can be started.")
			sClientData.sFinaleData.sFinaleConfig.bLaunchButtonEnabled = TRUE
		ENDIF
	ELSE
		IF sClientData.sFinaleData.sFinaleConfig.bLaunchButtonEnabled = TRUE
			PUBLIC_CASINO_HEIST_BOARD_COMMON__SET_BUTTON_GREYED_OUT(sClientData.sFinaleData.sScaleformData.siMainBoard, ENUM_TO_INT(CASINO_HEIST_FINALE_BOARD_ITEM__LAUNCH_HEIST_BUTTON), TRUE)
			CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_UPDATE_CASINO_HEIST_FINALE_LAUNCH_REQUIREMENTS - Launch requirements have not been met, launch button made invisible and disabled. Heist can not be started.")
			sClientData.sFinaleData.sFinaleConfig.bLaunchButtonEnabled = FALSE
		ENDIF
	ENDIF
	
ENDPROC

PROC PRIVATE_UNLOAD_CASINO_HEIST_FINALE_SCRIPT_RESOURCES(CASINO_HEIST_CLIENT_DATA &sClientData)

	RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_MPHEIST/HEIST_PLANNING_BOARD")
	
	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sClientData.sFinaleData.sScaleformData.siMainBoard)
	
	CLEAR_FIRST_PERSON_CAMERA(sClientData.sCameraData.sFPSCam, TRUE, TRUE)
	
	IF NOT NETWORK_IS_ACTIVITY_SESSION()
	AND sClientData.bPlayerControlRemoved
	AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
		sClientData.bPlayerControlRemoved = FALSE
	ENDIF
	
	PUBLIC_CLEAN_CASINO_HEIST_FINALE_DATA(sClientData.piLeader = PLAYER_ID())
	
	CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "Cleanup - Released all script assets and finished cleanup.")

ENDPROC


PROC PRIVATE_UPDATE_CASINO_HEIST_FINALE_NET_DATA(CASINO_HEIST_CLIENT_DATA &sClientData)

	IF NOT IS_PLAYER_IN_CORONA()
		EXIT
	ENDIF

	IF GET_CORONA_STATUS() = CORONA_STATUS_CASINO_HEIST_PLANNING
		IF g_sCasinoHeistData.piLeader = INVALID_PLAYER_INDEX()
		OR NOT NETWORK_IS_PLAYER_ACTIVE(g_sCasinoHeistData.piLeader)
			EXIT
		ENDIF
	ENDIF

	INT iLocalPlayer, iTracker, iLeader, iPlayer
	iLocalPlayer = NETWORK_PLAYER_ID_TO_INT()

	BOOL bUpdateCrewMemberDetailsRequired = FALSE
	BOOL bUpdatePlayerStatusRequired = FALSE
	BOOL bUpdateCashDistributionRequired = FALSE
	BOOL bUpdateLists = FALSE

	IF sClientData.piLeader = PLAYER_ID()

		FOR iTracker = 0 TO MAX_CASINO_HEIST_PLAYERS
			
			// Stuff that needs 5 array slots
			IF iTracker >= MAX_CASINO_HEIST_PLAYERS
				IF GlobalplayerBD_CasinoHeist[iLocalPlayer].iRewardPercentages[MAX_CASINO_HEIST_PLAYERS] != sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[MAX_CASINO_HEIST_PLAYERS]
					GlobalplayerBD_CasinoHeist[iLocalPlayer].iRewardPercentages[MAX_CASINO_HEIST_PLAYERS] = sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[MAX_CASINO_HEIST_PLAYERS]
					bUpdateCashDistributionRequired = TRUE
					CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] bUpdateCashDistributionRequired = TRUE [1]")
				ENDIF
				
				RELOOP
			ENDIF
			
			// Stuff that needs 4 array slots
			IF GlobalplayerBD_CasinoHeist[iLocalPlayer].iRewardPercentages[iTracker] != sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[iTracker]
				GlobalplayerBD_CasinoHeist[iLocalPlayer].iRewardPercentages[iTracker] = sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[iTracker]
				IF sClientData.sFinaleData.sFinaleConfig.iPlayerOrder[iTracker] = NETWORK_PLAYER_ID_TO_INT()
					g_TransitionSessionNonResetVars.iGangOpsHeistCutPercent = GlobalplayerBD_CasinoHeist[iLocalPlayer].iRewardPercentages[iTracker]
					g_sCasino_Heist_Finale_Telemetry_data.percentage = g_TransitionSessionNonResetVars.iGangOpsHeistCutPercent
					CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] g_sCasino_Heist_Finale_Telemetry_data.percentage = ", g_sCasino_Heist_Finale_Telemetry_data.percentage)
				ENDIF
				bUpdateCashDistributionRequired = TRUE
				CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] bUpdateCashDistributionRequired = TRUE [2]")
			ENDIF
			
			IF GlobalplayerBD_CasinoHeist[iLocalPlayer].iPlayerOrder[iTracker] != sClientData.sFinaleData.sFinaleConfig.iPlayerOrder[iTracker]
				GlobalplayerBD_CasinoHeist[iLocalPlayer].iPlayerOrder[iTracker] = sClientData.sFinaleData.sFinaleConfig.iPlayerOrder[iTracker]
				bUpdateCrewMemberDetailsRequired = TRUE
				CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] bUpdateRequired = TRUE ")
			ENDIF
			
			// Ready state is set by the client, so the leader must read instead of write.
			FOR iPlayer = 0 TO (NUM_NETWORK_PLAYERS-1)
				IF sClientData.sFinaleData.sFinaleConfig.iPlayerOrder[iTracker] != iPlayer
				OR sClientData.sFinaleData.sFinaleConfig.iPlayerOrder[iTracker] = NATIVE_TO_INT(sClientData.piLeader)
					RELOOP
				ENDIF
					
				IF sClientData.sFinaleData.sFinaleConfig.iReadyState[iTracker] != GlobalplayerBD_CasinoHeist[iPlayer].iReadyState[iTracker]
					sClientData.sFinaleData.sFinaleConfig.iReadyState[iTracker] = GlobalplayerBD_CasinoHeist[iPlayer].iReadyState[iTracker]
					GlobalplayerBD_CasinoHeist[iLocalPlayer].iReadyState[iTracker] = sClientData.sFinaleData.sFinaleConfig.iReadyState[iTracker]
					bUpdatePlayerStatusRequired = TRUE
					CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] bUpdatePlayerStatusRequired = TRUE ")
				ENDIF
				
//				IF sClientData.sFinaleData.bRunningTutorial[iTracker] != GlobalplayerBD_CasinoHeist[iPlayer].bRunningTutorial
//					sClientData.sFinaleData.bRunningTutorial[iTracker] = GlobalplayerBD_CasinoHeist[iPlayer].bRunningTutorial
//					GlobalplayerBD_CasinoHeist[iLocalPlayer].bTutorialInProgress[iTracker] = sClientData.sFinaleData.bRunningTutorial[iTracker]
//					CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] member ", iTracker, " is doing tutorial")
//					GlobalplayerBD_CasinoHeist[iLocalPlayer].iSyncId++
//				ENDIF
			ENDFOR
			
		ENDFOR
		
		IF bUpdateCrewMemberDetailsRequired
			GlobalplayerBD_CasinoHeist[iLocalPlayer].iSyncId++
			PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_CREW_MEMBER_DETAILS_UPDATE_REQUIRED(sClientData, TRUE)
			CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] Net Sync (L) - Net update crew member details available, updating iSyncId: ", GlobalplayerBD_CasinoHeist[iLocalPlayer].iSyncId)
		ENDIF
		
		IF bUpdateCashDistributionRequired
			GlobalplayerBD_CasinoHeist[iLocalPlayer].iSyncId++
			CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] Net Sync (L) - Net update cash distribution available, updating iSyncId: ", GlobalplayerBD_CasinoHeist[iLocalPlayer].iSyncId)
		ENDIF
		
		IF bUpdatePlayerStatusRequired
			GlobalplayerBD_CasinoHeist[iLocalPlayer].iSyncId++
			CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] Net Sync (L) - Net update player status available, updating iSyncId: ", GlobalplayerBD_CasinoHeist[iLocalPlayer].iSyncId)
		ENDIF
		
		IF GlobalplayerBD_CasinoHeist[iLocalPlayer].sMissionConfig.iDropOffLocation != sClientData.sFinaleData.iDropOffDisplaying
			GlobalplayerBD_CasinoHeist[iLocalPlayer].iSyncId++
			GlobalplayerBD_CasinoHeist[iLocalPlayer].sMissionConfig.iDropOffLocation = sClientData.sFinaleData.iDropOffDisplaying
			CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] Net Sync (L) - Drop off out of sync, updating BD to: ", sClientData.sFinaleData.iDropOffDisplaying)
		ENDIF
		
		IF GlobalplayerBD_CasinoHeist[iLocalPlayer].sMissionConfig.iEntranceChosen != sClientData.sFinaleData.iEntranceDisplaying
			GlobalplayerBD_CasinoHeist[iLocalPlayer].iSyncId++
			GlobalplayerBD_CasinoHeist[iLocalPlayer].sMissionConfig.iEntranceChosen = sClientData.sFinaleData.iEntranceDisplaying
			CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] Net Sync (L) - Entrance out of sync, updating BD to: ", sClientData.sFinaleData.iEntranceDisplaying)
		ENDIF
		
		IF GlobalplayerBD_CasinoHeist[iLocalPlayer].sMissionConfig.iExitChosen != sClientData.sFinaleData.iExitDisplaying
			GlobalplayerBD_CasinoHeist[iLocalPlayer].iSyncId++
			GlobalplayerBD_CasinoHeist[iLocalPlayer].sMissionConfig.iExitChosen = sClientData.sFinaleData.iExitDisplaying
			CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] Net Sync (L) - Exit out of sync, updating BD to: ", sClientData.sFinaleData.iExitDisplaying)
		ENDIF
		
		IF GET_PLAYER_CASINO_HEIST_APPROACH(PLAYER_ID()) = CASINO_HEIST_APPROACH_TYPE__SUBTERFUGE
			IF GlobalplayerBD_CasinoHeist[iLocalPlayer].sMissionConfig.iSubterfugeOutfitsIn != sClientData.sFinaleData.iOutfitInDisplaying
				GlobalplayerBD_CasinoHeist[iLocalPlayer].iSyncId++
				GlobalplayerBD_CasinoHeist[iLocalPlayer].sMissionConfig.iSubterfugeOutfitsIn = sClientData.sFinaleData.iOutfitInDisplaying
				CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] Net Sync (L) - Outfit in out of sync, updating BD to: ", sClientData.sFinaleData.iOutfitInDisplaying)
			ENDIF
			
			IF GlobalplayerBD_CasinoHeist[iLocalPlayer].sMissionConfig.iSubterfugeOutfitsOut != sClientData.sFinaleData.iOutfitOutDisplaying
				GlobalplayerBD_CasinoHeist[iLocalPlayer].iSyncId++
				GlobalplayerBD_CasinoHeist[iLocalPlayer].sMissionConfig.iSubterfugeOutfitsOut = sClientData.sFinaleData.iOutfitOutDisplaying
				CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] Net Sync (L) - Outfit out out of sync, updating BD to: ", sClientData.sFinaleData.iOutfitOutDisplaying)
			ENDIF
		ENDIF
	
	ELSE
	
		INT iMySlot
	
		iLeader = NATIVE_TO_INT(sClientData.piLeader)
	
		IF iLeader <= CASINO_HEIST_INVALID
			EXIT
		ENDIF
		
		// TODO: Does this need a search loop?
		// Ready state is set by the client, so push the data ready for the leader to pick it up.
		FOR iTracker = 0 TO (MAX_CASINO_HEIST_PLAYERS-1)
			IF sClientData.sFinaleData.sFinaleConfig.iPlayerOrder[iTracker] != NETWORK_PLAYER_ID_TO_INT()
				RELOOP
			ENDIF
			
			GlobalplayerBD_CasinoHeist[NETWORK_PLAYER_ID_TO_INT()].iReadyState[iTracker] = sClientData.sFinaleData.sFinaleConfig.iReadyState[iTracker]
			iMySlot = iTracker
		ENDFOR
		
		IF GlobalplayerBD_CasinoHeist[iLeader].iSyncId > sClientData.sFinaleData.sFinaleConfig.iSyncId
			
			FOR iTracker = 0 TO MAX_CASINO_HEIST_PLAYERS
						
				// Stuff that needs 5 array slots
				IF iTracker >= MAX_CASINO_HEIST_PLAYERS
					IF sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[MAX_CASINO_HEIST_PLAYERS] != GlobalplayerBD_CasinoHeist[iLeader].iRewardPercentages[MAX_CASINO_HEIST_PLAYERS]
						sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[MAX_CASINO_HEIST_PLAYERS] = GlobalplayerBD_CasinoHeist[iLeader].iRewardPercentages[MAX_CASINO_HEIST_PLAYERS]
						bUpdateCashDistributionRequired = TRUE
					ENDIF
					
					RELOOP
				ENDIF

				// Stuff that needs 4 array slots
				IF sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[iTracker] != GlobalplayerBD_CasinoHeist[iLeader].iRewardPercentages[iTracker]
					sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[iTracker] = GlobalplayerBD_CasinoHeist[iLeader].iRewardPercentages[iTracker]
					bUpdateCashDistributionRequired = TRUE
					
//					IF sClientData.sFinaleData.sFinaleConfig.iPlayerOrder[iTracker] = iLeader
//						g_sJobGangopsInfo.m_infos.m_leaderscashcutpercentage = sClientData.sFinaleData.iRewardPercentages[iTracker]
//						CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] Update leader cut percent (not leader): ", g_sJobGangopsInfo.m_infos.m_leaderscashcutpercentage)
//					ENDIF
//					
					IF iMySlot = iTracker
						g_TransitionSessionNonResetVars.iGangOpsHeistCutPercent = GlobalplayerBD_CasinoHeist[iLeader].iRewardPercentages[iTracker]
						g_sCasino_Heist_Finale_Telemetry_data.percentage = g_TransitionSessionNonResetVars.iGangOpsHeistCutPercent
						CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] Update my cut percent (not leader): ", g_sJobGangopsInfo.m_infos.m_cashcutpercentage)
					ENDIF
				ENDIF
				
				IF sClientData.sFinaleData.sFinaleConfig.iPlayerOrder[iTracker] != GlobalplayerBD_CasinoHeist[iLeader].iPlayerOrder[iTracker]
					sClientData.sFinaleData.sFinaleConfig.iPlayerOrder[iTracker] = GlobalplayerBD_CasinoHeist[iLeader].iPlayerOrder[iTracker]
					bUpdateCrewMemberDetailsRequired = TRUE
				ENDIF
				
				IF sClientData.sFinaleData.sFinaleConfig.iReadyState[iTracker] != GlobalplayerBD_CasinoHeist[iLeader].iReadyState[iTracker]
					IF iMySlot != iTracker
						sClientData.sFinaleData.sFinaleConfig.iReadyState[iTracker] = GlobalplayerBD_CasinoHeist[iLeader].iReadyState[iTracker]
					ENDIF
					bUpdatePlayerStatusRequired = TRUE
				ENDIF
				
//				IF sClientData.sFinaleData.bRunningTutorial[iTracker] != GlobalplayerBD_CasinoHeist[iLeader].bTutorialInProgress[iTracker]
//					IF iMySlot != iTracker
//						sClientData.sFinaleData.bRunningTutorial[iTracker] = GlobalplayerBD_CasinoHeist[iLeader].bTutorialInProgress[iTracker]
//					ENDIF
//				ENDIF
				
			ENDFOR
			
			
			IF bUpdateCrewMemberDetailsRequired
				PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_CREW_MEMBER_DETAILS_UPDATE_REQUIRED(sClientData, TRUE)
				CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] Net Sync - Net update crew member details available")
			ENDIF
			
			IF bUpdateCashDistributionRequired
				CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] Net Sync - Net update cash distribution available")
			ENDIF
			
			IF bUpdatePlayerStatusRequired
				CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] Net Sync - Net update player status available")
			ENDIF
			
			IF sClientData.sFinaleData.iEntranceDisplaying != GlobalplayerBD_CasinoHeist[iLeader].sMissionConfig.iEntranceChosen
				sClientData.sFinaleData.iEntranceDisplaying = GlobalplayerBD_CasinoHeist[iLeader].sMissionConfig.iEntranceChosen
				PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_ENTRANCE_BUTTON_UPDATE_REQUIRED(sClientData, TRUE)
				PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_HEADING_UPDATE_REQUIRED(sClientData, TRUE)
				CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] Net Sync - Net update entrance displaying available")
			ENDIF
			
			IF sClientData.sFinaleData.iExitDisplaying != GlobalplayerBD_CasinoHeist[iLeader].sMissionConfig.iExitChosen
				sClientData.sFinaleData.iExitDisplaying = GlobalplayerBD_CasinoHeist[iLeader].sMissionConfig.iExitChosen
				PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_EXIT_BUTTON_UPDATE_REQUIRED(sClientData, TRUE)
				PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_HEADING_UPDATE_REQUIRED(sClientData, TRUE)
				CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] Net Sync - Net update exit displaying available")
			ENDIF
			
			IF sClientData.sFinaleData.iDropOffDisplaying != GlobalplayerBD_CasinoHeist[iLeader].sMissionConfig.iDropOffLocation
				sClientData.sFinaleData.iDropOffDisplaying = GlobalplayerBD_CasinoHeist[iLeader].sMissionConfig.iDropOffLocation
				PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_DROP_OFF_BUTTON_UPDATE_REQUIRED(sClientData, TRUE)
				PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_HEADING_UPDATE_REQUIRED(sClientData, TRUE)
				CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] Net Sync - Net update drop off displaying available")
			ENDIF
			
			IF GET_PLAYER_CASINO_HEIST_APPROACH(sClientData.piLeader) = CASINO_HEIST_APPROACH_TYPE__SUBTERFUGE
				IF sClientData.sFinaleData.iOutfitInDisplaying != GlobalplayerBD_CasinoHeist[iLeader].sMissionConfig.iSubterfugeOutfitsIn
					sClientData.sFinaleData.iOutfitInDisplaying = GlobalplayerBD_CasinoHeist[iLeader].sMissionConfig.iSubterfugeOutfitsIn
					PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_OUTFIT_IN_BUTTON_UPDATE_REQUIRED(sClientData, TRUE)
					PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_HEADING_UPDATE_REQUIRED(sClientData, TRUE)
					CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] Net Sync - Net update outfit in displaying available")
				ENDIF
				
				IF sClientData.sFinaleData.iOutfitOutDisplaying != GlobalplayerBD_CasinoHeist[iLeader].sMissionConfig.iSubterfugeOutfitsOut
					sClientData.sFinaleData.iOutfitOutDisplaying = GlobalplayerBD_CasinoHeist[iLeader].sMissionConfig.iSubterfugeOutfitsOut
					PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_OUTFIT_OUT_BUTTON_UPDATE_REQUIRED(sClientData, TRUE)
					PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_HEADING_UPDATE_REQUIRED(sClientData, TRUE)
					CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] Net Sync - Net update outfit out displaying available")
				ENDIF
			ENDIF
			
			sClientData.sFinaleData.sFinaleConfig.iSyncId = GlobalplayerBD_CasinoHeist[iLeader].iSyncId
			CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] Net Sync (M) - Net update available, updating iSyncId: ", sClientData.sFinaleData.sFinaleConfig.iSyncId)

			IF IS_BIT_SET(GlobalplayerBD_CasinoHeist[iLeader].iListSyncBitSet, CASINO_HEIST_LIST_SYNC_BITSET__ENTRANCE_CONFIRMED)
			AND NOT IS_BIT_SET(sClientData.sFinaleData.sFinaleConfig.iListSyncBitSet, CASINO_HEIST_LIST_SYNC_BITSET__ENTRANCE_CONFIRMED) 
				bUpdateLists = TRUE
				sClientData.sFinaleData.iEntranceDisplayingCached = GlobalplayerBD_CasinoHeist[iLeader].sMissionConfig.iEntranceChosen
				SET_BIT(sClientData.sFinaleData.sFinaleConfig.iListSyncBitSet, CASINO_HEIST_LIST_SYNC_BITSET__ENTRANCE_CONFIRMED) 
				CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] Net Sync (M) - Net list update available, entrance confirmed")
			ENDIF
			
			IF IS_BIT_SET(GlobalplayerBD_CasinoHeist[iLeader].iListSyncBitSet, CASINO_HEIST_LIST_SYNC_BITSET__EXIT_CONFIRMED)
			AND NOT IS_BIT_SET(sClientData.sFinaleData.sFinaleConfig.iListSyncBitSet, CASINO_HEIST_LIST_SYNC_BITSET__EXIT_CONFIRMED) 
				bUpdateLists = TRUE
				sClientData.sFinaleData.iExitDisplayingCached = GlobalplayerBD_CasinoHeist[iLeader].sMissionConfig.iExitChosen
				SET_BIT(sClientData.sFinaleData.sFinaleConfig.iListSyncBitSet, CASINO_HEIST_LIST_SYNC_BITSET__EXIT_CONFIRMED) 
				CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] Net Sync (M) - Net list update available, exit confirmed")
			ENDIF
			
			IF IS_BIT_SET(GlobalplayerBD_CasinoHeist[iLeader].iListSyncBitSet, CASINO_HEIST_LIST_SYNC_BITSET__DROP_OFF_CONFIRMED)
			AND NOT IS_BIT_SET(sClientData.sFinaleData.sFinaleConfig.iListSyncBitSet, CASINO_HEIST_LIST_SYNC_BITSET__DROP_OFF_CONFIRMED) 
				bUpdateLists = TRUE
				sClientData.sFinaleData.iDropOffDisplayingCached = GlobalplayerBD_CasinoHeist[iLeader].sMissionConfig.iDropOffLocation
				SET_BIT(sClientData.sFinaleData.sFinaleConfig.iListSyncBitSet, CASINO_HEIST_LIST_SYNC_BITSET__DROP_OFF_CONFIRMED) 
				CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] Net Sync (M) - Net list update available, drop off confirmed")
			ENDIF
			
			IF IS_BIT_SET(GlobalplayerBD_CasinoHeist[iLeader].iListSyncBitSet, CASINO_HEIST_LIST_SYNC_BITSET__OUTFIT_IN_CONFIRMED)
			AND NOT IS_BIT_SET(sClientData.sFinaleData.sFinaleConfig.iListSyncBitSet, CASINO_HEIST_LIST_SYNC_BITSET__OUTFIT_IN_CONFIRMED) 
				bUpdateLists = TRUE
				sClientData.sFinaleData.iOutfitInDisplayingCached = GlobalplayerBD_CasinoHeist[iLeader].sMissionConfig.iSubterfugeOutfitsIn
				SET_BIT(sClientData.sFinaleData.sFinaleConfig.iListSyncBitSet, CASINO_HEIST_LIST_SYNC_BITSET__OUTFIT_IN_CONFIRMED) 
				CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] Net Sync (M) - Net list update available, outfit in confirmed")
			ENDIF
			
			IF IS_BIT_SET(GlobalplayerBD_CasinoHeist[iLeader].iListSyncBitSet, CASINO_HEIST_LIST_SYNC_BITSET__OUTFIT_OUT_CONFIRMED)
			AND NOT IS_BIT_SET(sClientData.sFinaleData.sFinaleConfig.iListSyncBitSet, CASINO_HEIST_LIST_SYNC_BITSET__OUTFIT_OUT_CONFIRMED) 
				bUpdateLists = TRUE
				sClientData.sFinaleData.iOutfitOutDisplayingCached = GlobalplayerBD_CasinoHeist[iLeader].sMissionConfig.iSubterfugeOutfitsOut
				SET_BIT(sClientData.sFinaleData.sFinaleConfig.iListSyncBitSet, CASINO_HEIST_LIST_SYNC_BITSET__OUTFIT_OUT_CONFIRMED) 
				CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] Net Sync (M) - Net list update available, outfit out confirmed")
			ENDIF
			
			IF IS_BIT_SET(GlobalplayerBD_CasinoHeist[iLeader].iListSyncBitSet, CASINO_HEIST_LIST_SYNC_BITSET__SELECTIONS_MADE)
			AND NOT IS_BIT_SET(sClientData.sFinaleData.sFinaleConfig.iListSyncBitSet, CASINO_HEIST_LIST_SYNC_BITSET__SELECTIONS_MADE) 
				PUBLIC_SET_CASINO_HEIST_DO_INSTR_BTN_UPDATE(sClientData.sFinaleData.sScaleformData, TRUE)
				SET_BIT(sClientData.sFinaleData.sFinaleConfig.iListSyncBitSet, CASINO_HEIST_LIST_SYNC_BITSET__SELECTIONS_MADE) 
				CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] Net Sync (M) - Net list update available, selections have been made")
			ENDIF
			
			IF IS_BIT_SET(GlobalplayerBD_CasinoHeist[iLeader].iListSyncBitSet, CASINO_HEIST_LIST_SYNC_BITSET__DECOY_GUNMAN_PURCHASED)
			AND NOT IS_BIT_SET(sClientData.sFinaleData.sFinaleConfig.iListSyncBitSet, CASINO_HEIST_LIST_SYNC_BITSET__DECOY_GUNMAN_PURCHASED) 
				bUpdateLists = TRUE
				PUBLIC_CASINO_HEIST_BOARD_COMMON__SET_TICK(sClientData.sFinaleData.sScaleformData.siMainBoard, ENUM_TO_INT(CASINO_HEIST_FINALE_BOARD_ITEM__DECOY_VEHICLE), TRUE)
				SET_BIT(sClientData.sFinaleData.sFinaleConfig.iListSyncBitSet, CASINO_HEIST_LIST_SYNC_BITSET__DECOY_GUNMAN_PURCHASED) 
				CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] Net Sync (M) - Net list update available, decoy gunman purchased")
			ENDIF
			
			IF IS_BIT_SET(GlobalplayerBD_CasinoHeist[iLeader].iListSyncBitSet, CASINO_HEIST_LIST_SYNC_BITSET__CLEAN_VEHICLE_PURCHASED)
			AND NOT IS_BIT_SET(sClientData.sFinaleData.sFinaleConfig.iListSyncBitSet, CASINO_HEIST_LIST_SYNC_BITSET__CLEAN_VEHICLE_PURCHASED) 
				bUpdateLists = TRUE
				PUBLIC_CASINO_HEIST_BOARD_COMMON__SET_TICK(sClientData.sFinaleData.sScaleformData.siMainBoard, ENUM_TO_INT(CASINO_HEIST_FINALE_BOARD_ITEM__SWITCH_VEHICLE), TRUE)
				SET_BIT(sClientData.sFinaleData.sFinaleConfig.iListSyncBitSet, CASINO_HEIST_LIST_SYNC_BITSET__CLEAN_VEHICLE_PURCHASED) 
				CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] Net Sync (M) - Net list update available, clean vehicle purchased")
			ENDIF
			
			IF bUpdateLists
				PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_TODO_LIST_UPDATE_REQUIRED(sClientData, TRUE)
				PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_OPTIONAL_LIST_UPDATE_REQUIRED(sClientData, TRUE)
				CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] Net Sync (M) - Net list update available, updating")
			ENDIF
		ENDIF

	ENDIF

ENDPROC

PROC PRIVATE_PREPARE_CASINO_HEIST_FINALE_HEADSHOTS(CASINO_HEIST_CLIENT_DATA &sClientData)

	INT i = 0

	REPEAT MAX_CASINO_HEIST_PLAYERS i
		sClientData.sFinaleData.sFinaleConfig.sHeadshotData[i].playerID = INVALID_PLAYER_INDEX()
	ENDREPEAT
	
	CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] PRIVATE_PREPARE_CASINO_HEIST_FINALE_HEADSHOTS - Prepared headshot system ready for requests.")
ENDPROC

FUNC BOOL SET_VARS_WHEN_LAUNCHING_CASINO_HEIST_FINALE(CASINO_HEIST_CLIENT_DATA &sClientData)
	
	CASINO_HEIST_APPROACH_TYPE eApproach = GET_PLAYER_CASINO_HEIST_APPROACH(sClientData.piLeader)
	CASINO_HEIST_MISSIONS eMission = GET_CASINO_HEIST_INTRO_MISSION_FROM_APPROACH_TYPE(eApproach)
	INT iArrayPos = GET_CASINO_HEIST_MISSION_ARRAY_POS(eMission)

	IF iArrayPos = -1
		PRINTLN("[CASINO_HEIST] SET_VARS_WHEN_LAUNCHING_CASINO_HEIST_FINALE - iArrayPos = -1. Candidate args present? ", GET_STRING_FROM_BOOL(GET_COMMANDLINE_PARAM_EXISTS("sc_UseRockstarCandidateMissions")))
		RETURN FALSE
	ENDIF

	TEXT_LABEL_23 tlName 	= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArrayPos].tlName
	INT iType 				= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArrayPos].iType
	INT iMaxPlayers 		= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArrayPos].iMaxPlayers

	PRINTLN("[TS] ****************************************************")
	PRINTLN("[TS] * [CASINO_HEIST] * SET_VARS_WHEN_LAUNCHING_CASINO_HEIST_FINALE tlName = ", tlName, "- ", GET_CLOUD_TIME_AS_INT(), ", F - ", GET_FRAME_COUNT(), " *")
	PRINTLN("[TS] ****************************************************")
	PRINTLN("[CASINO_HEIST] SET_VARS_WHEN_LAUNCHING_CASINO_HEIST_FINALE - tlName = ", tlName)
	PRINTLN("[CASINO_HEIST] SET_VARS_WHEN_LAUNCHING_CASINO_HEIST_FINALE - iType = ", iType)
	PRINTLN("[CASINO_HEIST] SET_VARS_WHEN_LAUNCHING_CASINO_HEIST_FINALE - eApproach = ", eApproach)
	PRINTLN("[CASINO_HEIST] SET_VARS_WHEN_LAUNCHING_CASINO_HEIST_FINALE - eMission = ", eMission)
	PRINTLN("[CASINO_HEIST] SET_VARS_WHEN_LAUNCHING_CASINO_HEIST_FINALE - iArrayPos = ", iArrayPos)

	SET_PLAYER_LEAVING_CORONA_VECTOR(GET_PLAYER_PERCEIVED_COORDS(PLAYER_ID()))
	NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_NO_COLLISION | NSPC_CAN_BE_TARGETTED | NSPC_FREEZE_POSITION | NSPC_USE_PLAYER_FADE_OUT) 
	g_TransitionSessionNonResetVars.bSaveBeforeCoronaVehicle = TRUE
	
	SET_MP_DECORATOR_BIT(PLAYER_ID(), MP_DECORATOR_BS_INVALID_TO_SPECTATE)
	
	iType = FMMC_TYPE_GB_CASINO_HEIST
	
	CLEAR_PAUSE_MENU_IS_USING_UGC()
	SET_TRANSITION_SESSIONS_SKIP_JOB_WARNING()	
	SET_FM_JOB_ENTERY_TYPE(ciMISSION_ENTERY_TYPE_CASINO_HEIST_BOARD)
	SET_TRANSITION_SESSIONS_LAUNCHING_CASINO_HEIST_FROM_BOARD()
	
	IF (AM_I_TRANSITION_SESSIONS_STARTING_ON_CALL()
	OR SHOULD_TRANSITION_SESSIONS_CLEAN_UP_ON_CALL_NOW())
		SET_TRANSITION_SESSIONS_CLEAN_UP_ON_CALL_NOW()
		PRINTLN("[CASINO_HEIST] SET_VARS_WHEN_LAUNCHING_CASINO_HEIST_FINALE - On call, setting to clean up on call")
		
	//If we are
	ELSE
		g_SimpleInteriorData.bAcceptedSimpleIntSameSessionHeistInv = TRUE
		SET_TRANSITION_SESSIONS_QUICK_MATCH_TYPE(iType)
		IF GB_GET_NUM_GOONS_IN_LOCAL_GANG() <= 3
			SMV_FLOW_SET_LAUNCHING_SMV_IN_OFFICE()
		ENDIF
		SET_GAMER_HANDLE_OF_PLAYER_LAUNCHING_SMPL_INT_HEIST(GET_GAMER_HANDLE_PLAYER(PLAYER_ID()))
		SET_MY_TRANSITION_SESSION_CONTENT_ID(tlName)
		SET_TRANSITION_SESSIONS_QUICK_MATCH_MAX_PLAYERS(iMaxPlayers)	
		CLEAR_TRANSITION_SESSIONS_NEED_TO_SKYCAM_UP_NOT_WARP()							
		SET_TRANSITION_SESSIONS_STARTING_QUICK_MATCH()	
		CLEAR_TRANSITION_SESSIONS_NEED_TO_WARP_TO_START_SKYCAM()
		CLEAR_TRANSITION_SESSIONS_CORONA_CONTROLLER_MAINTAIN_CAMERA()
		SET_TRANSITION_SESSIONS_FORCE_ME_HOST_QUICK_MATCH()
		
		VECTOR vCamPosTemp
		BOOL bTemp
		VECTOR vZero = << 0.0, 0.0, 0.0 >>
		INT iSetupBS
		SET_BIT(iSetupBS, ciCORONA_CAMERA_SETUP_BS_CASINO_HEIST)
		SETUP_CORONA_CAMERA(g_sTransitionSessionData.ciCam, FMMC_TYPE_MISSION, 0, vZero, vCamPosTemp, vZero, TRUE, FALSE, vCamPosTemp, bTemp, bTemp, FALSE, iSetupBS)
		TOGGLE_RENDERPHASES(FALSE)
		
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmLauncherGameState = FMMC_LAUNCHER_STATE_LOAD_MISSION_FOR_TRANSITION_SESSION
		
		PRINTLN("[CASINO_HEIST] SET_VARS_WHEN_LAUNCHING_CASINO_HEIST_FINALE - Found mission at set vars, good to go!")
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PRIVATE_CASINO_HEIST_FINALE_LAUNCH_MISSION(CASINO_HEIST_CLIENT_DATA &sClientData)
	IF PUBLIC_CASINO_HEIST_BOARD__IS_BIT_SET(sClientData, CASINO_HEIST_BOARD_BITSET__LAUNCH_FINALE_MISSION)
	AND NOT PUBLIC_CASINO_HEIST_BOARD__IS_BIT_SET(sClientData, CASINO_HEIST_BOARD_BITSET__LAUNCHED_FINALE_MISSION)
		IF sClientData.piLeader = PLAYER_ID()
			IF AM_I_TRANSITION_SESSIONS_STARTING_ON_CALL()
			OR HAS_ON_CALL_TRANSITION_SESSION_BEEN_SET_UP()
				CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] [MISSION_LAUNCH] - MAINTAIN_LAUNCH_FINALE_MISSION - I am on call, cleaning up on call now... SET_TRANSITION_SESSIONS_CLEAN_UP_ON_CALL_NOW")
				SET_TRANSITION_SESSIONS_CLEAN_UP_ON_CALL_NOW()
			ELSE
				IF NOT PUBLIC_CASINO_HEIST_BOARD__IS_BIT_SET(sClientData, CASINO_HEIST_BOARD_BITSET__LAUNCHED_FINALE_MISSION)
					IF SET_VARS_WHEN_LAUNCHING_CASINO_HEIST_FINALE(sClientData)
						CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] [MISSION_LAUNCH] - MAINTAIN_LAUNCH_FINALE_MISSION - Launching finale for approach: ", CASINO_HEIST_MISSION_DATA__APPROACH_TYPE_STRING_FOR_DEBUG(GET_PLAYER_CASINO_HEIST_APPROACH(sClientData.piLeader)))
						PUBLIC_CASINO_HEIST_BOARD__SET_BIT(sClientData, CASINO_HEIST_BOARD_BITSET__LAUNCHED_FINALE_MISSION)
						PUBLIC_CASINO_HEIST_BOARD__SET_BIT(sClientData, CASINO_HEIST_BOARD_BITSET__DO_TOGGLE_RENDERPHASES)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELIF PUBLIC_CASINO_HEIST_BOARD__IS_BIT_SET(sClientData, CASINO_HEIST_BOARD_BITSET__DO_TOGGLE_RENDERPHASES)
		IF NOT PUBLIC_CASINO_HEIST_BOARD__IS_BIT_SET(sClientData, CASINO_HEIST_BOARD_BITSET__TOGGLED_RENDERPHASES)
			PUBLIC_CASINO_HEIST_BOARD__SET_BIT(sClientData, CASINO_HEIST_BOARD_BITSET__TOGGLED_RENDERPHASES)
			#IF IS_DEBUG_BUILD 
			IF NOT GET_COMMANDLINE_PARAM_EXISTS("sc_toggleHeistRenderPhases")
			#ENDIF
			TOGGLE_RENDERPHASES(FALSE) 
			TOGGLE_RENDERPHASES(FALSE)		
			#IF IS_DEBUG_BUILD 
			ENDIF
			#ENDIF
			CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] [MISSION_LAUNCH] PRIVATE_MAINTAIN_LAUNCHING_MISSION - CASINO_HEIST_BOARD_BITSET__TOGGLED_RENDERPHASES set")
		ENDIF
		PUBLIC_CASINO_HEIST_BOARD__CLEAR_BIT(sClientData, CASINO_HEIST_BOARD_BITSET__DO_TOGGLE_RENDERPHASES)
		CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] [MISSION_LAUNCH] PRIVATE_MAINTAIN_LAUNCHING_MISSION - CASINO_HEIST_BOARD_BITSET__DO_TOGGLE_RENDERPHASES cleared")
	ENDIF
ENDPROC

PROC PRIVATE_UPDATE_CASINO_HEIST_FINALE_BOARD_TUTORIALS(CASINO_HEIST_CLIENT_DATA &sClientData)
	
	IF PLAYER_ID() != sClientData.piLeader
		EXIT
	ENDIF
		
	// First play through
	IF NOT HAS_PLAYER_COMPLETED_CASINO_HEIST(sClientData.piLeader)
	
		// Do finale introduction first in freemode
		IF NOT HAS_LOCAL_PLAYER_SEEN_CASINO_HEIST_PLANNING_TUTORIAL(CASINO_HEIST_PLANNING_TUTORIAL__FINALE_INTRODUCTION)
					
			SWITCH sClientData.sFinaleData.iTutorialStage
				CASE 0
					PUBLIC_SET_CASINO_HEIST_DIALOGUE_SHOULD_TRIGGER(sClientData, CASINO_HEIST_PLANNING_DIALOGUE__FINALE_FINALE_BOARD_INTRO)
					sClientData.sFinaleData.iTutorialStage++
				BREAK
				CASE 1
					IF PUBLIC_HAS_CASINO_HEIST_DIALOGUE_FINISHED_PLAYING(sClientData, CASINO_HEIST_PLANNING_DIALOGUE__FINALE_FINALE_BOARD_INTRO)
						SET_PLAYER_HAS_SEEN_CASINO_HEIST_PLANNING_TUTORIAL(CASINO_HEIST_PLANNING_TUTORIAL__FINALE_INTRODUCTION, TRUE)
						sClientData.sFinaleData.iTutorialStage = 0
					ENDIF
				BREAK
			ENDSWITCH
			
//		ELSE
//			
//			// Trigger dialogue for the first button you're on
//			IF NETWORK_IS_ACTIVITY_SESSION()
//			AND !sClientData.sDialogueData.bTriggeredFinaleIntro
//				SWITCH GET_PLAYER_CASINO_HEIST_APPROACH(sClientData.piLeader)
//					CASE CASINO_HEIST_APPROACH_TYPE__STEALTH		
//					CASE CASINO_HEIST_APPROACH_TYPE__DIRECT
//						PUBLIC_SET_CASINO_HEIST_DIALOGUE_SHOULD_TRIGGER(sClientData, CASINO_HEIST_PLANNING_DIALOGUE__FINALE_ENTRANCE_EXPLANATION)
//					BREAK
//					CASE CASINO_HEIST_APPROACH_TYPE__SUBTERFUGE
//						PUBLIC_SET_CASINO_HEIST_DIALOGUE_SHOULD_TRIGGER(sClientData, CASINO_HEIST_PLANNING_DIALOGUE__FINALE_OUTFIT_IN_EXPLANATION)
//					BREAK
//				ENDSWITCH
//				sClientData.sDialogueData.bTriggeredFinaleIntro = TRUE
//			ENDIF
			
		ENDIF
		
	ELSE
	
		IF NETWORK_IS_ACTIVITY_SESSION()
	
			// Play the replay introduction
			IF NOT HAS_LOCAL_PLAYER_SEEN_CASINO_HEIST_PLANNING_TUTORIAL(CASINO_HEIST_PLANNING_TUTORIAL__REPLAY_FINALE_INTRODUCTION)
						
				SWITCH sClientData.sFinaleData.iTutorialStage
					CASE 0
						PUBLIC_SET_CASINO_HEIST_DIALOGUE_SHOULD_TRIGGER(sClientData, CASINO_HEIST_PLANNING_DIALOGUE__REPLAY_FINALE_INTRO)
						sClientData.sFinaleData.iTutorialStage++
					BREAK
					CASE 1
						IF PUBLIC_HAS_CASINO_HEIST_DIALOGUE_FINISHED_PLAYING(sClientData, CASINO_HEIST_PLANNING_DIALOGUE__REPLAY_FINALE_INTRO)
							SET_PLAYER_HAS_SEEN_CASINO_HEIST_PLANNING_TUTORIAL(CASINO_HEIST_PLANNING_TUTORIAL__REPLAY_FINALE_INTRODUCTION, TRUE)
							sClientData.sFinaleData.iTutorialStage = 0
						ENDIF
					BREAK
				ENDSWITCH
				
			ELSE
				// Trigger dialogue for the first button you're in if you've not done that approach before
				IF !sClientData.sDialogueData.bTriggeredFinaleIntro
					SWITCH GET_PLAYER_CASINO_HEIST_APPROACH(sClientData.piLeader)
						CASE CASINO_HEIST_APPROACH_TYPE__STEALTH		
						CASE CASINO_HEIST_APPROACH_TYPE__DIRECT
							IF NOT HAS_PLAYER_COMPLETED_CASINO_HEIST(sClientData.piLeader, CASINO_HEIST_APPROACH_TYPE__STEALTH)
							AND NOT HAS_PLAYER_COMPLETED_CASINO_HEIST(sClientData.piLeader, CASINO_HEIST_APPROACH_TYPE__DIRECT)
								PUBLIC_SET_CASINO_HEIST_DIALOGUE_SHOULD_TRIGGER(sClientData, CASINO_HEIST_PLANNING_DIALOGUE__FINALE_ENTRANCE_EXPLANATION)
							ENDIF
						BREAK
						CASE CASINO_HEIST_APPROACH_TYPE__SUBTERFUGE
							IF NOT HAS_PLAYER_COMPLETED_CASINO_HEIST(sClientData.piLeader, CASINO_HEIST_APPROACH_TYPE__SUBTERFUGE)
								PUBLIC_SET_CASINO_HEIST_DIALOGUE_SHOULD_TRIGGER(sClientData, CASINO_HEIST_PLANNING_DIALOGUE__FINALE_OUTFIT_IN_EXPLANATION)
							ENDIF
						BREAK
					ENDSWITCH
					sClientData.sDialogueData.bTriggeredFinaleIntro = TRUE
				ENDIF
				
			ENDIF
			
		ENDIF
		
	ENDIF
ENDPROC

PROC PRIVATE_SET_CUTS_FOR_FORCE_LAUNCH(CASINO_HEIST_CLIENT_DATA &sClientData)
	SWITCH sClientData.sFinaleData.sFinaleConfig.iTotalPlayers
		CASE 1
			CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] PRIVATE_SET_CUTS_FOR_FORCE_LAUNCH - 1 player")
		
			IF sClientData.piLeader = PLAYER_ID()
				GlobalplayerBD_CasinoHeist[NETWORK_PLAYER_ID_TO_INT()].iRewardPercentages[0] = 100
				GlobalplayerBD_CasinoHeist[NETWORK_PLAYER_ID_TO_INT()].iRewardPercentages[1] = 0
				GlobalplayerBD_CasinoHeist[NETWORK_PLAYER_ID_TO_INT()].iRewardPercentages[2] = 0
				GlobalplayerBD_CasinoHeist[NETWORK_PLAYER_ID_TO_INT()].iRewardPercentages[3] = 0
				GlobalplayerBD_CasinoHeist[NETWORK_PLAYER_ID_TO_INT()].iRewardPercentages[4] = 0
			ENDIF
			
			sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[0] = 100
			sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[1] = 0
			sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[2] = 0
			sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[3] = 0
			sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[4] = 0	// zero the kitty
		BREAK
		CASE 2
			CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] PRIVATE_SET_CUTS_FOR_FORCE_LAUNCH - 2 players")
		
			IF sClientData.piLeader = PLAYER_ID()
				GlobalplayerBD_CasinoHeist[NETWORK_PLAYER_ID_TO_INT()].iRewardPercentages[0] = 85
				GlobalplayerBD_CasinoHeist[NETWORK_PLAYER_ID_TO_INT()].iRewardPercentages[1] = 15
				GlobalplayerBD_CasinoHeist[NETWORK_PLAYER_ID_TO_INT()].iRewardPercentages[2] = 0
				GlobalplayerBD_CasinoHeist[NETWORK_PLAYER_ID_TO_INT()].iRewardPercentages[3] = 0
				GlobalplayerBD_CasinoHeist[NETWORK_PLAYER_ID_TO_INT()].iRewardPercentages[4] = 0
			ENDIF
			
			sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[0] = 85
			sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[1] = 15
			sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[2] = 0
			sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[3] = 0
			sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[4] = 0	// zero the kitty
		BREAK
		CASE 3
			CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] PRIVATE_SET_CUTS_FOR_FORCE_LAUNCH - 3 players")
		
			IF sClientData.piLeader = PLAYER_ID()
				GlobalplayerBD_CasinoHeist[NETWORK_PLAYER_ID_TO_INT()].iRewardPercentages[0] = 70
				GlobalplayerBD_CasinoHeist[NETWORK_PLAYER_ID_TO_INT()].iRewardPercentages[1] = 15
				GlobalplayerBD_CasinoHeist[NETWORK_PLAYER_ID_TO_INT()].iRewardPercentages[2] = 15
				GlobalplayerBD_CasinoHeist[NETWORK_PLAYER_ID_TO_INT()].iRewardPercentages[3] = 0
				GlobalplayerBD_CasinoHeist[NETWORK_PLAYER_ID_TO_INT()].iRewardPercentages[4] = 0
			ENDIF
			
			sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[0] = 70
			sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[1] = 15
			sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[2] = 15
			sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[3] = 0
			sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[4] = 0	// zero the kitty
		BREAK
		CASE 4
			CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] PRIVATE_SET_CUTS_FOR_FORCE_LAUNCH - 4 players")
		
			IF sClientData.piLeader = PLAYER_ID()
				GlobalplayerBD_CasinoHeist[NETWORK_PLAYER_ID_TO_INT()].iRewardPercentages[0] = 55
				GlobalplayerBD_CasinoHeist[NETWORK_PLAYER_ID_TO_INT()].iRewardPercentages[1] = 15
				GlobalplayerBD_CasinoHeist[NETWORK_PLAYER_ID_TO_INT()].iRewardPercentages[2] = 15
				GlobalplayerBD_CasinoHeist[NETWORK_PLAYER_ID_TO_INT()].iRewardPercentages[3] = 15
				GlobalplayerBD_CasinoHeist[NETWORK_PLAYER_ID_TO_INT()].iRewardPercentages[4] = 0
			ENDIF
			
			sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[0] = 55
			sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[1] = 15
			sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[2] = 15
			sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[3] = 15
			sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[4] = 0	// zero the kitty
		BREAK
	ENDSWITCH

	INT iTemp, iMySlot
	FOR iTemp = 0 TO (MAX_CASINO_HEIST_PLAYERS-1)
		IF sClientData.sFinaleData.sFinaleConfig.iPlayerOrder[iTemp] != NETWORK_PLAYER_ID_TO_INT()
			RELOOP
		ENDIF
		iMySlot = iTemp
		CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] PRIVATE_SET_CUTS_FOR_FORCE_LAUNCH - iMySlot = ", iMySlot)
	ENDFOR
	
	g_TransitionSessionNonResetVars.iGangOpsHeistCutPercent = sClientData.sFinaleData.sFinaleConfig.iRewardPercentages[iMySlot]
	g_sCasino_Heist_Finale_Telemetry_data.percentage = g_TransitionSessionNonResetVars.iGangOpsHeistCutPercent
	CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] PRIVATE_SET_CUTS_FOR_FORCE_LAUNCH - g_TransitionSessionNonResetVars.iGangOpsHeistCutPercent = ", g_TransitionSessionNonResetVars.iGangOpsHeistCutPercent)
	CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] PRIVATE_SET_CUTS_FOR_FORCE_LAUNCH - g_sCasino_Heist_Finale_Telemetry_data.percentage = ", g_sCasino_Heist_Finale_Telemetry_data.percentage)
ENDPROC

PROC PRIVATE_UPDATE_CASINO_HEIST_FINALE_FORCE_LAUNCH(CASINO_HEIST_CLIENT_DATA &sClientData)
	IF IS_BIT_SET(sClientData.iBitSet, CASINO_HEIST_BOARD_BITSET__IN_CORONA)
		INT iLeader = NATIVE_TO_INT(sClientData.piLeader)
	
		IF iLeader != -1
			IF NOT HAS_NET_TIMER_STARTED(GlobalplayerBD_CasinoHeist[iLeader].stFinaleLaunchTimer)
				IF sClientData.piLeader = PLAYER_ID()
					IF IS_BIT_SET(sClientData.sFinaleData.sFinaleConfig.iListSyncBitSet, CASINO_HEIST_LIST_SYNC_BITSET__SELECTIONS_MADE)
						START_NET_TIMER(GlobalplayerBD_CasinoHeist[iLeader].stFinaleLaunchTimer)
					ENDIF
				ENDIF
			ELSE
				sClientData.sFinaleData.iForceLaunchTime = (120000 - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(GlobalplayerBD_CasinoHeist[iLeader].stFinaleLaunchTimer))
				
				IF sClientData.sFinaleData.iForceLaunchTime > 0
					// Update instructional buttons once per second to update the timer
					IF sClientData.sFinaleData.iForceLaunchTime < (sClientData.sFinaleData.iLastSecondCached-1000)
						PUBLIC_SET_CASINO_HEIST_DO_INSTR_BTN_UPDATE(sClientData.sFinaleData.sScaleformData, TRUE)
						sClientData.sFinaleData.iLastSecondCached = sClientData.sFinaleData.iForceLaunchTime
					ENDIF
				ELSE
					sClientData.sFinaleData.iForceLaunchTime = 0
					
					PRIVATE_SET_CUTS_FOR_FORCE_LAUNCH(sClientData)
					
					IF sClientData.piLeader = PLAYER_ID()
						sClientData.sFinaleData.sFinaleConfig.iReadyState[0] = CASINO_HEIST_FINALE_READY
						GlobalplayerBD_CasinoHeist[NETWORK_PLAYER_ID_TO_INT()].iReadyState[0] = CASINO_HEIST_FINALE_READY
						SET_BIT(GlobalplayerBD_CasinoHeist[NETWORK_PLAYER_ID_TO_INT()].iGeneralBitset, ciCASINO_HEIST_GENERAL_BITSET__FORCED_FINALE_LAUNCH)
						
						CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] PRIVATE_UPDATE_CASINO_HEIST_FINALE_FORCE_LAUNCH - Force launch timer has expired.")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


/* ===================================================================================== *\
			STATE MACHINE - STATE METHODS AND MAIN UPDATE
\* ===================================================================================== */

PROC PRIVATE_CASINO_HEIST_FINALE_IDLE_UPDATE(CASINO_HEIST_CLIENT_DATA &sClientData)
	IF NOT PUBLIC_CASINO_HEIST_BOARD__SHOULD_BOARDS_CLEAN_UP(sClientData)
	AND NOT PUBLIC_CASINO_HEIST_BOARD__SHOULD_BOARDS_CLEAN_UP_IMMEDIATELY()
		PUBLIC_PROGRESS_CASINO_HEIST_FSM(sClientData.sFinaleData.sStateData, CHBT_FINALE)
	ENDIF
ENDPROC

PROC PRIVATE_CASINO_HEIST_FINALE_INIT_ENTRY(CASINO_HEIST_CLIENT_DATA &sClientData)
	CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "FSM - INIT ENTRY")
	
	UNUSED_PARAMETER(sClientData)
ENDPROC

PROC PRIVATE_CASINO_HEIST_FINALE_INIT_UPDATE(CASINO_HEIST_CLIENT_DATA &sClientData)
	PUBLIC_PROGRESS_CASINO_HEIST_FSM(sClientData.sFinaleData.sStateData, CHBT_FINALE)
ENDPROC

PROC PRIVATE_CASINO_HEIST_FINALE_INIT_EXIT(CASINO_HEIST_CLIENT_DATA &sClientData)
	CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "FSM - INIT EXIT")
	PUBLIC_SET_CASINO_HEIST_HIGHLIGHTER_STATE(sClientData.sCursorData, CASINO_HEIST_FINALE_HIGHLIGHT_NULL, TRUE)
	PUBLIC_SET_CASINO_HEIST_DO_INSTR_BTN_UPDATE(sClientData.sFinaleData.sScaleformData, TRUE)
ENDPROC



PROC PRIVATE_CASINO_HEIST_FINALE_LOADING_ENTRY(CASINO_HEIST_CLIENT_DATA &sClientData)
	CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "FSM - LOADING ENTRY")
	PUBLIC_CASINO_HEIST_FINALE_BOARD__REQUEST_SCALEFORM(sClientData.sFinaleData.sScaleformData)
	PRIVATE_PREPARE_CASINO_HEIST_FINALE_HEADSHOTS(sClientData)
ENDPROC

PROC PRIVATE_CASINO_HEIST_FINALE_LOADING_UPDATE(CASINO_HEIST_CLIENT_DATA &sClientData)
	
	BOOL bReady = TRUE
	
	IF NOT PUBLIC_HAVE_CASINO_HEIST_SCALEFORM_ASSETS_LOADED(sClientData.sFinaleData.sScaleformData)
		CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "Loading - Waiting for assets to load.")
		bReady = FALSE
	ENDIF
	
	IF NOT PRIVATE_HAVE_CONFIGURED_CASINO_HEIST_FINALE_SETUP_DATA(sClientData)
		CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "Loading - Waiting for data setup to complete.")
		bReady = FALSE
	ENDIF
	
	IF bReady
		CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "Loading - Moving to create Casino Heist board with ",sClientData.sFinaleData.sFinaleConfig.iTotalPlayers," members.")
	
		PUBLIC_PROGRESS_CASINO_HEIST_FSM(sClientData.sFinaleData.sStateData, CHBT_FINALE)
	ENDIF
	
ENDPROC

PROC PRIVATE_CASINO_HEIST_FINALE_LOADING_EXIT(CASINO_HEIST_CLIENT_DATA &sClientData)
	CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "FSM - LOADING EXIT")
	PRIVATE_UPDATE_CASINO_HEIST_FINALE_NET_DATA(sClientData)
	
	PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_TODO_LIST_UPDATE_REQUIRED(sClientData, TRUE)
	PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_OPTIONAL_LIST_UPDATE_REQUIRED(sClientData, TRUE)
	PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_CREW_MEMBER_DETAILS_UPDATE_REQUIRED(sClientData, TRUE)
	PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_CREW_MEMBER_PANEL_VISIBLE_UPDATE_REQUIRED(sClientData, TRUE)
	PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_HEADING_UPDATE_REQUIRED(sClientData, TRUE)
	PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_BUTTON_VISIBILITY_UPDATE_REQUIRED(sClientData, TRUE)
	
	PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_ENTRANCE_BUTTON_UPDATE_REQUIRED(sClientData, TRUE)
	PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_EXIT_BUTTON_UPDATE_REQUIRED(sClientData, TRUE)
	PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_DROP_OFF_BUTTON_UPDATE_REQUIRED(sClientData, TRUE)

	IF GET_PLAYER_CASINO_HEIST_APPROACH(sClientData.piLeader) = CASINO_HEIST_APPROACH_TYPE__SUBTERFUGE
		PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_OUTFIT_IN_BUTTON_UPDATE_REQUIRED(sClientData, TRUE)
		PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_OUTFIT_OUT_BUTTON_UPDATE_REQUIRED(sClientData, TRUE)
	ENDIF

	PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_WEAPON_BUTTON_UPDATE_REQUIRED(sClientData, TRUE)
	PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_VEHICLE_BUTTON_UPDATE_REQUIRED(sClientData, TRUE)
	PRIVATE_CASINO_HEIST_FINALE_BOARD__SET_HACKING_BUTTON_UPDATE_REQUIRED(sClientData, TRUE)
ENDPROC



PROC PRIVATE_CASINO_HEIST_FINALE_DISPLAYING_ENTRY(CASINO_HEIST_CLIENT_DATA &sClientData)
	CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "FSM - DISPLAYING ENTRY")
	
	UNUSED_PARAMETER(sClientData)
	
	IF IS_BIT_SET(GlobalplayerBD_CasinoHeist[NETWORK_PLAYER_ID_TO_INT()].iGeneralBitset, ciCASINO_HEIST_GENERAL_BITSET__INTERACTING_WITH_BOARDS)
		CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "[CASINO_HEIST] | PRIVATE_CASINO_HEIST_FINALE_DISPLAYING_ENTRY - ciCASINO_HEIST_GENERAL_BITSET__INTERACTING_WITH_BOARDS cleared")
		CLEAR_BIT(GlobalplayerBD_CasinoHeist[NETWORK_PLAYER_ID_TO_INT()].iGeneralBitset, ciCASINO_HEIST_GENERAL_BITSET__INTERACTING_WITH_BOARDS)
	ENDIF
ENDPROC

PROC PRIVATE_CASINO_HEIST_FINALE_DISPLAYING_UPDATE(CASINO_HEIST_CLIENT_DATA &sClientData)
	
	PRIVATE_UPDATE_CASINO_HEIST_FINALE_NET_DATA(sClientData)
	PRIVATE_RENDER_CASINO_HEIST_FINALE_SCALEFORM(sClientData)
	PRIVATE_UPDATE_CASINO_HEIST_FINALE_BOARD_ITEMS(sClientData)
	PUBLIC_CASINO_HEIST_BOARD_COMMON__CLEANUP_INSTRUCTIONS_WHEN_ONLY_DISPLAYING(sClientData)
	
	IF sClientData.sFinaleData.sStateData.bInteract
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		IF NOT NETWORK_IS_ACTIVITY_SESSION()
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
		ENDIF
		sClientData.bPlayerControlRemoved = TRUE
		PUBLIC_PROGRESS_CASINO_HEIST_FSM(sClientData.sFinaleData.sStateData, CHBT_FINALE)
	ENDIF
	
ENDPROC

PROC PRIVATE_CASINO_HEIST_FINALE_DISPLAYING_EXIT(CASINO_HEIST_CLIENT_DATA &sClientData)
	CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "FSM - DISPLAYING EXIT")
	
	UNUSED_PARAMETER(sClientData)
ENDPROC



PROC PRIVATE_CASINO_HEIST_FINALE_INUSE_ENTRY(CASINO_HEIST_CLIENT_DATA &sClientData)
	CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "FSM - INUSE ENTRY")
	
	IF NOT PUBLIC_CASINO_HEIST_BOARD__IS_BIT_SET(sClientData, CASINO_HEIST_BOARD_BITSET__TRANSITIONING)
		PUBLIC_INIT_CASINO_HEIST_CAMERA(sClientData, CHCAM_FINALE_MAIN)
	ENDIF
	IF NETWORK_IS_ACTIVITY_SESSION()
		IF GET_PLAYER_CASINO_HEIST_APPROACH(sClientData.piLeader) = CASINO_HEIST_APPROACH_TYPE__SUBTERFUGE
			PUBLIC_SET_CASINO_HEIST_HIGHLIGHTER_STATE(sClientData.sCursorData, ENUM_TO_INT(CASINO_HEIST_FINALE_BOARD_ITEM__OUTFIT_IN))
		ELSE
			PUBLIC_SET_CASINO_HEIST_HIGHLIGHTER_STATE(sClientData.sCursorData, ENUM_TO_INT(CASINO_HEIST_FINALE_BOARD_ITEM__ENTRANCE_LOCATION))
		ENDIF
	ELSE
		IF sClientData.piLeader = PLAYER_ID()
			PUBLIC_SET_CASINO_HEIST_HIGHLIGHTER_STATE(sClientData.sCursorData, ENUM_TO_INT(CASINO_HEIST_FINALE_BOARD_ITEM__LAUNCH_HEIST_BUTTON))
			SET_CASINO_HEIST_PLANNING_HELP_TRIGGER(sClientData, CASINO_HEIST_PLANNING_HELP_TEXT__FINALE_EXPLAIN_BOARD)
		ELSE
			IF GET_PLAYER_CASINO_HEIST_APPROACH(sClientData.piLeader) = CASINO_HEIST_APPROACH_TYPE__SUBTERFUGE
				PUBLIC_SET_CASINO_HEIST_HIGHLIGHTER_STATE(sClientData.sCursorData, ENUM_TO_INT(CASINO_HEIST_FINALE_BOARD_ITEM__OUTFIT_IN))
			ELSE
				PUBLIC_SET_CASINO_HEIST_HIGHLIGHTER_STATE(sClientData.sCursorData, ENUM_TO_INT(CASINO_HEIST_FINALE_BOARD_ITEM__ENTRANCE_LOCATION))
			ENDIF
		ENDIF
	ENDIF
	PUBLIC_CASINO_HEIST_BOARD_COMMON__SET_CURRENT_SELECTION(sClientData.sFinaleData.sScaleformData.siMainBoard, sClientData.sCursorData.iHighlight)
	sClientData.sFinaleData.sScaleformData.bDrawInstructions = TRUE
ENDPROC

PROC PRIVATE_CASINO_HEIST_FINALE_INUSE_UPDATE(CASINO_HEIST_CLIENT_DATA &sClientData)

	PUBLIC_CASINO_HEIST_MAINTAIN_EXCLUSIVE_INPUT()
	PRIVATE_UPDATE_CASINO_HEIST_FINALE_FORCE_LAUNCH(sClientData)
	PRIVATE_UPDATE_CASINO_HEIST_FINALE_BOARD_TUTORIALS(sClientData)
	PRIVATE_UPDATE_CASINO_HEIST_FINALE_NET_DATA(sClientData)
	PRIVATE_RENDER_CASINO_HEIST_FINALE_SCALEFORM(sClientData)
	PRIVATE_UPDATE_CASINO_HEIST_FINALE_WARNING_SCREEN(sClientData)
	PRIVATE_UPDATE_CASINO_HEIST_FINALE_INPUTS(sClientData)
	PRIVATE_UPDATE_CASINO_HEIST_FINALE_BOARD_ITEMS(sClientData)
	PUBLIC_UPDATE_CASINO_HEIST_CAMERA(sClientData)
	PUBLIC_CASINO_HEIST_HIDE_HUD()
	PUBLIC_CLEAR_INPUT_BUFFERS(sClientData, CHBT_FINALE)
	
	IF sClientData.piLeader = PLAYER_ID()
		PRIVATE_UPDATE_CASINO_HEIST_FINALE_LAUNCH_REQUIREMENTS(sClientData)
	ELSE
		PRIVATE_MONITOR_CASINO_HEIST_FINALE_LAUNCH(sClientData)
	ENDIF
	
	IF NOT sClientData.sFinaleData.sStateData.bInteract
		IF NOT PUBLIC_CASINO_HEIST_BOARD__IS_BIT_SET(sClientData, CASINO_HEIST_BOARD_BITSET__TRANSITIONING)
			IF sClientData.bPlayerControlRemoved
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				IF NOT NETWORK_IS_ACTIVITY_SESSION()
					FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
				ENDIF
				sClientData.bPlayerControlRemoved = FALSE
			ENDIF
			PRIVATE_CLEAR_CASINO_HEIST_FINALE_CAMERA(sClientData)
		ENDIF
		REQUEST_SAVE(SSR_REASON_CASINO_HEIST, STAT_SAVETYPE_END_MISSION)
		PUBLIC_REGRESS_CASINO_HEIST_FSM(sClientData.sFinaleData.sStateData, CHBT_FINALE)
	ENDIF
	
ENDPROC

PROC PRIVATE_CASINO_HEIST_FINALE_INUSE_EXIT(CASINO_HEIST_CLIENT_DATA &sClientData)
	CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "FSM - INUSE EXIT")
	
	IF NOT PUBLIC_CASINO_HEIST_BOARD__IS_BIT_SET(sClientData, CASINO_HEIST_BOARD_BITSET__TRANSITIONING)
		PUBLIC_SET_CASINO_HEIST_HIGHLIGHTER_STATE(sClientData.sCursorData)
		sClientData.sFinaleData.sScaleformData.bDrawInstructions = FALSE
	ENDIF
	
ENDPROC



PROC PRIVATE_CASINO_HEIST_FINALE_CLEANUP_ENTRY(CASINO_HEIST_CLIENT_DATA &sClientData)
	CDEBUG1LN(DEBUG_CASINO_HEIST_PLANNING, "FSM - CLEANUP ENTRY")
	
	PRIVATE_UNLOAD_CASINO_HEIST_FINALE_SCRIPT_RESOURCES(sClientData)
	PUBLIC_CLEAN_CASINO_HEIST_ALL()
	PUBLIC_CLEAN_CASINO_HEIST_SCALEFORM_CACHE()
	PUBLIC_CASINO_HEIST_BOARD_COMMON__WIPE_BOARD_CLEAN(sClientData.sFinaleData.sScaleformData)
ENDPROC

PROC PRIVATE_CASINO_HEIST_FINALE_CLEANUP_UPDATE(CASINO_HEIST_CLIENT_DATA &sClientData)
	UNUSED_PARAMETER(sClientData)
ENDPROC

PROC PRIVATE_CASINO_HEIST_FINALE_UPDATE(CASINO_HEIST_CLIENT_DATA &sClientData)
	
	SWITCH (sClientData.sFinaleData.sStateData.eState)
	
		CASE CHBS_IDLE
		
			PRIVATE_CASINO_HEIST_FINALE_IDLE_UPDATE(sClientData)
		
		BREAK
		
		CASE CHBS_INIT
		
			IF NOT PUBLIC_CASINO_HEIST_HAS_RUN_ENTRY(sClientData.sFinaleData.sStateData)
				PRIVATE_CASINO_HEIST_FINALE_INIT_ENTRY(sClientData)
				PUBLIC_CASINO_HEIST_SET_COMPLETED_ENTRY(sClientData.sFinaleData.sStateData)
			ENDIF
			
			PRIVATE_CASINO_HEIST_FINALE_IDLE_UPDATE(sClientData)
			
			IF NOT PUBLIC_CASINO_HEIST_HAS_RUN_EXIT(sClientData.sFinaleData.sStateData)
				PRIVATE_CASINO_HEIST_FINALE_INIT_EXIT(sClientData)
				PUBLIC_CASINO_HEIST_SET_COMPLETED_EXIT(sClientData.sFinaleData.sStateData)
			ENDIF
			
		BREAK
		
		CASE CHBS_LOADING
		
			IF NOT PUBLIC_CASINO_HEIST_HAS_RUN_ENTRY(sClientData.sFinaleData.sStateData)
				PRIVATE_CASINO_HEIST_FINALE_LOADING_ENTRY(sClientData)
				PUBLIC_CASINO_HEIST_SET_COMPLETED_ENTRY(sClientData.sFinaleData.sStateData)
			ENDIF
			
			PRIVATE_CASINO_HEIST_FINALE_LOADING_UPDATE(sClientData)
			
			IF NOT PUBLIC_CASINO_HEIST_HAS_RUN_EXIT(sClientData.sFinaleData.sStateData)
				PRIVATE_CASINO_HEIST_FINALE_LOADING_EXIT(sClientData)
				PUBLIC_CASINO_HEIST_SET_COMPLETED_EXIT(sClientData.sFinaleData.sStateData)
			ENDIF
		
		BREAK
		
		CASE CHBS_DISPLAYING
		
			IF NOT PUBLIC_CASINO_HEIST_HAS_RUN_ENTRY(sClientData.sFinaleData.sStateData)
				PRIVATE_CASINO_HEIST_FINALE_DISPLAYING_ENTRY(sClientData)
				PUBLIC_CASINO_HEIST_SET_COMPLETED_ENTRY(sClientData.sFinaleData.sStateData)
			ENDIF
			
			PRIVATE_CASINO_HEIST_FINALE_DISPLAYING_UPDATE(sClientData)
			
			IF NOT PUBLIC_CASINO_HEIST_HAS_RUN_EXIT(sClientData.sFinaleData.sStateData)
				PRIVATE_CASINO_HEIST_FINALE_DISPLAYING_EXIT(sClientData)
				PUBLIC_CASINO_HEIST_SET_COMPLETED_EXIT(sClientData.sFinaleData.sStateData)
			ENDIF
		
		BREAK
		
		CASE CHBS_INUSE
		
			IF NOT PUBLIC_CASINO_HEIST_HAS_RUN_ENTRY(sClientData.sFinaleData.sStateData)
				PRIVATE_CASINO_HEIST_FINALE_INUSE_ENTRY(sClientData)
				PUBLIC_CASINO_HEIST_SET_COMPLETED_ENTRY(sClientData.sFinaleData.sStateData)
			ENDIF
			
			PRIVATE_CASINO_HEIST_FINALE_INUSE_UPDATE(sClientData)
			
			IF NOT PUBLIC_CASINO_HEIST_HAS_RUN_EXIT(sClientData.sFinaleData.sStateData)
				PRIVATE_CASINO_HEIST_FINALE_INUSE_EXIT(sClientData)
				PUBLIC_CASINO_HEIST_SET_COMPLETED_EXIT(sClientData.sFinaleData.sStateData)
			ENDIF
		
		BREAK
		
		CASE CHBS_CLEANUP
		
			IF NOT PUBLIC_CASINO_HEIST_HAS_RUN_ENTRY(sClientData.sFinaleData.sStateData)
				PRIVATE_CASINO_HEIST_FINALE_CLEANUP_ENTRY(sClientData)
				PUBLIC_CASINO_HEIST_SET_COMPLETED_ENTRY(sClientData.sFinaleData.sStateData)
			ENDIF
			
			PRIVATE_CASINO_HEIST_FINALE_CLEANUP_UPDATE(sClientData)
		
		BREAK
	
	ENDSWITCH

ENDPROC

PROC MAINTAIN_CASINO_HEIST_FINALE_BOARD(CASINO_HEIST_CLIENT_DATA &sClientData)
	
	#IF IS_DEBUG_BUILD
	PRIVATE_CASINO_HEIST_FINALE_WIDGET_UPDATE(sClientData)
	#ENDIF

	PRIVATE_CASINO_HEIST_FINALE_UPDATE(sClientData)
	PRIVATE_CASINO_HEIST_FINALE_LAUNCH_MISSION(sClientData)
	
ENDPROC

#ENDIF

// EOF

