/// Gives players simple missions to do while not on a main Job.
///    Dave W

/// Look at MAINTAIN_AMMO_DROP_CLIENT() For launching

USING "globals.sch"

// Game Headers
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_camera.sch"
USING "commands_hud.sch"

// Network Headers
USING "net_include.sch"
USING "net_events.sch"

// 

USING "net_mission.sch"
USING "net_scoring_common.sch"

USING "commands_path.sch"
USING "net_blips.sch"
USING "commands_zone.sch"

USING "hud_drawing.sch"
USING "net_ambience.sch"
USING "FM_Unlocks_header.sch"


USING "shop_public.sch"
USING "net_garages.sch"
USING "net_gameDir.sch"



USING "net_mission_details_overlay.sch"
USING "net_hud_displays.sch"
USING "net_mission_joblist.sch"

USING "net_wait_zero.sch"

USING "net_cash_transactions.sch"

#IF IS_DEBUG_BUILD
	USING "profiler.sch"
#ENDIF

USING "net_gang_boss.sch"


TWEAK_INT MIN_PLAYERS_FOR_LAUNCH	1 // Martin's launcher will handle actual min players (2)

CONST_FLOAT MIN_DISTANCE_FOR_SPAWN 300.0

CONST_INT MAX_AM_PEDS			2
CONST_INT MAX_AM_VEH			2

CONST_INT MAX_SPAWN_POS			5

CONST_INT TIME_LAUNCH_TIMEOUT	300000

TWEAK_INT TIME_NEAR_PLAYERS_INITIAL	360000



// Game States
CONST_INT GAME_STATE_INI 		0
CONST_INT GAME_STATE_INI_SPAWN	1
CONST_INT GAME_STATE_RUNNING	2
CONST_INT GAME_STATE_LEAVE		3
CONST_INT GAME_STATE_FAILED		4
CONST_INT GAME_STATE_TERMINATE_DELAY 5
CONST_INT GAME_STATE_END		6

//-- Mission types
CONST_INT AM_MISSION_KILL_TARGET_IN_VEH	0

// Mission States
CONST_INT MISSION_WAIT_FOR_LAUNCH		0
CONST_INT MISSION_CREATE_ASSETS			1
CONST_INT MISSION_CHECK_FOR_WIN			2
CONST_INT MISSION_WAIT_FOR_ASSETS		3
CONST_INT MISSION_DO_INTRO_PHONECALL	4
CONST_INT MISSION_COMPLETE_OBJECTIVE	5
CONST_INT MISSION_CLEANUP				6
CONST_INT MISSION_RESET					7




// Target ped States
CONST_INT TARGET_PED_CREATED			0
CONST_INT TARGET_PED_TOUR_IN_CAR		1
CONST_INT TARGET_PED_FLEE_IN_CAR		2
CONST_INT TARGET_PED_LEAVE_CAR			3
CONST_INT TARGET_PED_FLEE_ON_FOOT		4

#IF IS_DEBUG_BUILD
	CONST_INT biSdebug_SomeoneDebugSkipped		0
#ENDIF

CONST_INT biS_EnoughPlayersReadyForLaunch		0
CONST_INT biS_FindSpawnPosForKillTarget			1
CONST_INT biS_GotSpawnCandidates				2
CONST_INT biS_MissionLaunched					3
CONST_INT biS_SomeoneCompleted					4
CONST_INT biS_ServerFoundSomeoneStillRunning	5
CONST_INT biS_AllPlayersFinished				6
CONST_INT biS_SomeoneDamagedVeh1				7
CONST_INT biS_SomeoneDamagedPed1				8
CONST_INT biS_GiveDriveFastWanderTask			9
CONST_INT biS_LaunchTimeOut						10
CONST_INT biS_NobodyNearTarget					11
CONST_INT biS_InitialDistanceCheckExpired		12
CONST_INT biS_CanReserveAssets					13
CONST_INT biS_SomeoneAimingAtTarget				14
CONST_INT biS_TwoMinutesLeft					15
CONST_INT biS_KilledByOffMission				16
CONST_INT biS_FoundNewPlayer					17
CONST_INT biS_StaggeredTargetKilledByOffMission	18
CONST_INT biS_StuckOnRoofOrSide					19

// Everyone can read this data, only the server can update it. 
STRUCT ServerBroadcastData

	INT iServerBitSet
	INT iServerGameState
	INT iServerMissionProg
	INT iEntityCreateAreaId = -1
	INT iCandidateSpawnBitset
	INT iMissionType = -1
	INT iTargetPedState = TARGET_PED_CREATED
	INT iPlayersOnMission
	
	PLAYER_INDEX playerCompleted
	NETWORK_INDEX niPed[MAX_AM_PEDS]
	NETWORK_INDEX niVeh[MAX_AM_VEH]
	
	INT iNumReservedVeh = 0
	INT iNumReservedPed = 0
	SCRIPT_TIMER timeLaunchTimeout
	SCRIPT_TIMER timeTerminate
	SCRIPT_TIMER timeNearTarget
	
	#IF IS_DEBUG_BUILD
		INT iServerDebugBitset
	#ENDIF
ENDSTRUCT
ServerBroadcastData serverBD
INT iServerStaggeredCount
INT iFindCreationPosStaggeredPlayerCount

SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerData
 

// Player bits
CONST_INT biP_NotDoneAnything			0
CONST_INT biP_KilledTarget				1
CONST_INT biP_PlayerOnMission			2
CONST_INT biP_DamagedVehicle1			3
CONST_INT biP_DamagedPed1				4
CONST_INT biP_NearTarget				5
CONST_INT biP_AimAtTarget				6
CONST_INT biP_KilledByOffMission		7
CONST_INT biP_StuckOnRoofOrSide			8

STRUCT PlayerBroadcastData
	INT iPlayerBitset
	INT iGameState
	INT iMissionProg
	
	#IF IS_DEBUG_BUILD
		BOOL bSpassed
	#ENDIF
ENDSTRUCT
PlayerBroadcastData PlayerBD[NUM_NETWORK_PLAYERS]

INT iPlayerStaggeredCount

INT iIntroPhoneCount

INT iBoolsBitSet
CONST_INT BiL_ServerLoopedThroughEveryone			0
CONST_INT biL_CurrentCandidateValid					1
CONST_INT biL_LocalLoopedThroughEveryone			2
CONST_INT biL_DOnePlayerWonTick						3
CONST_INT biL_DoneIntroPhonecall					4
CONST_INT biL_ServStaggeredAllFinished				5
CONST_INT biL_DoneInitialObjective					6
CONST_INT biL_PausedObjectiveText					7
CONST_INT biL_PhonecallFailed						8
CONST_INT biL_NobodyNearTarget						9
CONST_INT biL_SetBlipName							10
CONST_INT biL_GotPhonecallLabel						11
CONST_INT biL_SetBlipNameVeh						12
CONST_INT biL_SetAsTargetPriority					13
CONST_INT biL_SomeoneAimingAtTarget					14
CONST_INT biL_TargetDeadNotSureWhoKilled			15
CONST_INT biL_JoinedTooLate							16
CONST_INT biL_SentInvite							17
CONST_INT biL_AcceptedInvite						18
CONST_INT biL_PhonecallPlaying						19
CONST_INT biL_StuckOnRoofOrSide						20




SCRIPT_TIMER timeNotDoneAnything
SCRIPT_TIMER timeRetryPhonecall
SCRIPT_TIMER timeKilledOffMission

INT iNumberOfPlayersReady


//-- For entity creation
INT iCandidateCount
INT iCandidate

INT iLocalReservedVeh = 0
INT iLocalReservePed = 0

AI_BLIP_STRUCT blipTarget

structPedsForConversation 	speechForMission
STRING sRootIdIntroPhonecall

CONST_INT LEST_NPC_BUSINESS_TARGET			HASH("LEST_NPC_BUSINESS_TARGET")


#IF IS_DEBUG_BUILD
	BOOL bDisableAllDebug = FALSE
	BOOL bWdDoStaggeredDebug = FALSE
	BOOL bWdForceBlipTarget
	BOOL bWdBlipDebug
	BLIP_INDEX blipWdTarget
	PROC NET_DW_PRINT(STRING sText)
		IF NOT bDisableAllDebug
			NET_PRINT_TIME() NET_PRINT("[amMissionLaunch] [DSW] ") NET_PRINT_STRINGS(" ", sText) NET_NL()
		ENDIF
	ENDPROC 

	PROC NET_DW_PRINT_STRING_INT(STRING sText1, INT i)
		IF NOT bDisableAllDebug
			NET_PRINT_TIME() NET_PRINT("[amMissionLaunch] [DSW] ") NET_PRINT_STRING_INT(sText1, i) NET_NL()
		ENDIF
	ENDPROC

	PROC NET_DW_PRINT_STRING_FLOAT(STRING sText1, FLOAT f)
		IF NOT bDisableAllDebug
			NET_PRINT_TIME() NET_PRINT("[amMissionLaunch] [DSW] ") NET_PRINT_STRING_FLOAT(sText1, f) NET_NL()
		ENDIF
	ENDPROC

	PROC NET_DW_PRINT_STRING_VECTOR(STRING sText1, VECTOR v)
		IF NOT bDisableAllDebug
			NET_PRINT_TIME() NET_PRINT("[amMissionLaunch] [DSW] ") NET_PRINT_STRING_VECTOR(sText1, v) NET_NL()
		ENDIF
	ENDPROC
	
	PROC NET_DW_PRINT_STRINGS(STRING sText1, STRING sText2)
		IF NOT bDisableAllDebug
			NET_PRINT_TIME() NET_PRINT("[amMissionLaunch] [DSW] ") NET_PRINT_STRINGS(sText1, sText2) NET_NL()
		ENDIF
	ENDPROC
	
	PROC NET_DW_PRINT_STRING_WITH_PLAYER_NAME(STRING sText, PLAYER_INDEX player)
		IF NOT bDisableAllDebug
			NET_PRINT_TIME() NET_PRINT("[amMissionLaunch] [DSW] ") NET_PRINT_STRINGS(" ", sText) NET_PRINT_STRINGS(" ", GET_PLAYER_NAME(player)) NET_NL()
		ENDIF
	ENDPROC
	
	PROC NET_DW_PRINT_HOST_NAME()
		PLAYER_INDEX playerHost = NETWORK_GET_HOST_OF_SCRIPT("am_mission_launch")
		NET_PRINT_TIME() 
		IF playerHost <> INVALID_PLAYER_INDEX()
			NET_PRINT("[amMissionLaunch] [DSW] Current host... ") NET_PRINT(GET_PLAYER_NAME(playerHost)) 
		ELSE
			NET_PRINT("[amMissionLaunch] [DSW] NET_DW_PRINT_HOST_NAME - Failed to get host! Possibly just left ") 
		ENDIF
		NET_NL() 
	ENDPROC
	
	BOOL bWdClearPhonecallStat
	PROC CREATE_WIDGETS()
		START_WIDGET_GROUP("Ambient missions")
			//Profiling widgets
			#IF SCRIPT_PROFILER_ACTIVE
			CREATE_SCRIPT_PROFILER_WIDGET()
			#ENDIF		
			ADD_WIDGET_BOOL("Debug blip", bWdBlipDebug)
			ADD_WIDGET_BOOL("Clear phonecall stat", bWdClearPhonecallStat)
			START_WIDGET_GROUP("Player")
				ADD_WIDGET_INT_READ_ONLY("Mission Prog", PlayerBD[PARTICIPANT_ID_TO_INT()].iMissionProg)
				ADD_BIT_FIELD_WIDGET("Local", iBoolsBitSet)
				ADD_BIT_FIELD_WIDGET("Player", PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet)
				ADD_WIDGET_BOOL("Force blip target", bWdForceBlipTarget)
				
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Server")
				ADD_WIDGET_BOOL("Staggered debug ", bWdDoStaggeredDebug)
				ADD_WIDGET_INT_READ_ONLY("Server Prog", serverBD.iServerMissionProg)
				ADD_WIDGET_INT_SLIDER("Min players", MIN_PLAYERS_FOR_LAUNCH, 1, 10, 1)
				ADD_WIDGET_INT_SLIDER("Min Distance Timeout", TIME_NEAR_PLAYERS_INITIAL, 1000, 99999999, 1)
				ADD_BIT_FIELD_WIDGET("Server", serverBD.iServerBitSet)
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP() 
	ENDPROC
	
	PROC UPDATE_WIDGETS()
		IF bWdForceBlipTarget
			IF NOT IS_NET_PED_INJURED(serverBD.niPed[0])
				blipWdTarget = ADD_BLIP_FOR_ENTITY(NET_TO_PED(serverBD.niPed[0]))
				SET_BLIP_PRIORITY(blipWdTarget, BLIPPRIORITY_HIGHEST)
				bWdForceBlipTarget = FALSE
			ENDIF
		ENDIF
		
		IF bWdClearPhonecallStat
			bWdClearPhonecallStat = FALSE
			
			INT iStatInt = g_MP_STAT_FM_CUT_DONE[GET_SLOT_NUMBER(-1)]
			CLEAR_BIT(iStatInt, biFmCut_LaunchedKillTarget)
			SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_FM_CUT_DONE, iStatInt)
		ENDIF
	ENDPROC
	
	PROC DO_DEBUG_KEYS()
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
			PlayerBD[PARTICIPANT_ID_TO_INT()].bSpassed = TRUE
			NET_DW_PRINT("I S-Skipped game dir mission")
		ENDIF
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
			IF IS_NET_PLAYER_OK(PLAYER_ID())
				NET_DW_PRINT_STRING_INT("Player ok for j-skip, missionProg = ", PlayerBD[PARTICIPANT_ID_TO_INT()].iMissionProg)
				SWITCH PlayerBD[PARTICIPANT_ID_TO_INT()].iMissionProg
					CASE MISSION_COMPLETE_OBJECTIVE
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPed[0])
							IF NOT IS_NET_PED_INJURED(serverBD.niPed[0])
								J_SKIP_MP(GET_ENTITY_COORDS(NET_TO_PED(serverBD.niPed[0])), <<10.0, 10.0, 10.0>>)
							ENDIF
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
		ENDIF
	ENDPROC
#ENDIF

FUNC BOOL SHOULD_HIDE_LOCAL_UI()
//	IF HANDLE_DEPRECATED_HIDE_BLIP(ciPI_HIDE_MENU_DEPRECATED_AMB_KILL_TARGET)
//		RETURN TRUE
//	ENDIF
	
//	RETURN FALSE
	
	// Content disabled, forcibly hiding
	RETURN TRUE
ENDFUNC

//PURPOSE:checks to see if the vehicle is stuck or undrivable
FUNC BOOL LESTER_TARGET_VEHICLE_STUCK_CHECKS()
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[0])
		IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niVeh[0])
			IF IS_VEHICLE_STUCK_TIMER_UP(NET_TO_VEH(serverBD.niVeh[0]), VEH_STUCK_ON_ROOF, ROOF_TIME)
			OR IS_VEHICLE_STUCK_TIMER_UP(NET_TO_VEH(serverBD.niVeh[0]), VEH_STUCK_ON_SIDE, SIDE_TIME)
				#IF IS_DEBUG_BUILD
					NET_DW_PRINT("[LESTER_TARGET_VEHICLE_STUCK_CHECKS] Vehicle is stuck on side / roof")
				#ENDIF
				
				RETURN TRUE
				
			ENDIF 
		ELSE
			RETURN TRUE
		ENDIF 
	ENDIF 
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_ON_ANY_OTHER_AMBIENT_SCRIPT(PLAYER_INDEX PlayerID)
	IF IS_PLAYER_ON_ANY_MP_AMBIENT_SCRIPT(PlayerID)
		IF NOT IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PlayerID, MPAM_TYPE_GAMEDIR_MISSION)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_NEAR_TARGET(INT iMission)
	SWITCH iMission
		CASE AM_MISSION_KILL_TARGET_IN_VEH
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPed[0])
				IF NOT IS_NET_PED_INJURED(serverBD.niPed[0])
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(NET_TO_PED(serverBD.niPed[0]))) < 40000 //200m
							RETURN TRUE	
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_AIMING_AT_TARGET(INT iMission)
	SWITCH iMission
		CASE AM_MISSION_KILL_TARGET_IN_VEH
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPed[0])
				IF NOT IS_NET_PED_INJURED(serverBD.niPed[0])
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						IF IS_PLAYER_AIMING_AT_PED(NET_TO_PED(serverBD.niPed[0]))
						
							RETURN TRUE	
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC
PROC MAINTAIN_LOCAL_PLAYER_CHECKS()
	INT iPart = PARTICIPANT_ID_TO_INT()
	
	
	//-- Ped / vehicle / object reservation
	IF iLocalReservedVeh <> serverBD.iNumReservedVeh
		iLocalReservedVeh = serverBD.iNumReservedVeh
		RESERVE_NETWORK_MISSION_VEHICLES(iLocalReservedVeh)
		#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("iLocalReservedVeh updated, new value iLocalReservedVeh = ", iLocalReservedVeh) #ENDIF
	ENDIF
	
	IF iLocalReservePed <> serverBD.iNumReservedPed
		iLocalReservePed = serverBD.iNumReservedPed
		RESERVE_NETWORK_MISSION_PEDS(iLocalReservePed)
		#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("iLocalReservePed updated, new value iLocalReservePed = ", iLocalReservePed) #ENDIF
	ENDIF
	
	IF IS_NET_PLAYER_OK(PLAYER_ID(), FALSE)
		//-- CHeck for me not being on a job for a while
		IF NOT IS_BIT_SET(PlayerBD[iPart].iPlayerBitset, biP_NotDoneAnything)
			IF PlayerBD[PARTICIPANT_ID_TO_INT()].iMissionProg = MISSION_WAIT_FOR_LAUNCH
				IF GET_LOCAL_PLAYER_CORONA_POS_STATE() = 0
					//-- Now gets done in header file, before launching
					SET_BIT(PlayerBD[iPart].iPlayerBitset, biP_NotDoneAnything)
				ENDIF
			
			ENDIF
		ELSE	
			IF IS_BIT_SET(PlayerBD[iPart].iPlayerBitset, biP_NotDoneAnything)
				IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
				OR IS_PLAYER_ON_ANY_OTHER_AMBIENT_SCRIPT(PLAYER_ID())
				OR GET_LOCAL_PLAYER_CORONA_POS_STATE() > 0
					RESET_NET_TIMER(timeNotDoneAnything)
					CLEAR_BIT(PlayerBD[iPart].iPlayerBitset, biP_NotDoneAnything)
					#IF IS_DEBUG_BUILD NET_DW_PRINT("Clearing biP_NotDoneAnything as I'm on a mission") #ENDIF
				ELSE
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
							IF NOT IS_BIT_SET(iBoolsBitSet, biL_PausedObjectiveText)
								SET_BIT(iBoolsBitSet, biL_PausedObjectiveText)
								Pause_Objective_Text()
								#IF IS_DEBUG_BUILD NET_DW_PRINT("Pauseing objective text as player control off") #ENDIF
							ENDIF
						ELSE
							IF IS_BIT_SET(iBoolsBitSet, biL_PausedObjectiveText)
								CLEAR_BIT(iBoolsBitSet, biL_PausedObjectiveText)
								IF NOT IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(),MPAM_TYPE_CINEMA)
									unpause_Objective_Text()
								ENDIF
								#IF IS_DEBUG_BUILD NET_DW_PRINT("unpauseing objective text as player control off") #ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(PlayerBD[iPart].iPlayerBitset, biP_DamagedVehicle1)
			IF PlayerBD[PARTICIPANT_ID_TO_INT()].iMissionProg > MISSION_WAIT_FOR_ASSETS
			AND PlayerBD[PARTICIPANT_ID_TO_INT()].iMissionProg < MISSION_CLEANUP
				IF IS_BIT_SET(mpglobals.gameDirGlobalData.iGlobalDataBitset, biGameDir_DamagedtargetVehicle1)
					SET_BIT(PlayerBD[iPart].iPlayerBitset, biP_DamagedVehicle1)
					#IF IS_DEBUG_BUILD NET_DW_PRINT("Setting biP_DamagedVehicle1 as global is set") #ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		
		IF PlayerBD[PARTICIPANT_ID_TO_INT()].iMissionProg > MISSION_WAIT_FOR_ASSETS
			//-- Check for player being near a targe
			IF NOT IS_BIT_SET(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitset, biP_NearTarget)
				IF IS_PLAYER_NEAR_TARGET(serverBD.iMissionType)
					SET_BIT(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitset, biP_NearTarget)
					#IF IS_DEBUG_BUILD NET_DW_PRINT("Setting biP_NearTarget as I'm near target") #ENDIF
				ENDIF
			ELSE	
				IF NOT IS_PLAYER_NEAR_TARGET(serverBD.iMissionType)
					CLEAR_BIT(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitset, biP_NearTarget)
					#IF IS_DEBUG_BUILD NET_DW_PRINT("Clearing biP_NearTarget as I'm far from target") #ENDIF
				ENDIF
			ENDIF
			
			//-- Check for player aiming at target
			IF NOT IS_BIT_SET(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitset, biP_AimAtTarget)
				IF IS_PLAYER_AIMING_AT_TARGET(serverBD.iMissionType)
					SET_BIT(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitset, biP_AimAtTarget)
					#IF IS_DEBUG_BUILD NET_DW_PRINT("Setting biP_AimAtTarget as I'm aiming ") #ENDIF
				ENDIF
			ENDIF
			
			//-- Check for target vehicle stuck
			IF NOT IS_BIT_SET(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitset, biP_StuckOnRoofOrSide)
				IF LESTER_TARGET_VEHICLE_STUCK_CHECKS()
					SET_BIT(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitset, biP_StuckOnRoofOrSide)
					#IF IS_DEBUG_BUILD NET_DW_PRINT("Setting biP_StuckOnRoofOrSide as I think target stuck ") #ENDIF
				ENDIF
			ENDIF
			
			
		ENDIF
		
		
		IF NOT IS_BIT_SET(iBoolsBitSet, biL_SetAsTargetPriority)
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPed[0])
				IF NOT IS_NET_PED_INJURED(serverBD.niPed[0])
					SET_ENTITY_IS_TARGET_PRIORITY(NET_TO_PED(serverBD.niPed[0]), TRUE)
					SET_BIT(iBoolsBitSet, biL_SetAsTargetPriority)
					#IF IS_DEBUG_BUILD NET_DW_PRINT("Set as target priority") #ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	
	
ENDPROC

FUNC INT GET_CLIENT_MISSION_STATE(INT iPlayer)
	RETURN playerBD[iPlayer].iGameState
ENDFUNC

FUNC INT GET_SERVER_MISSION_STATE()
	RETURN serverBD.iServerGameState

ENDFUNC

FUNC BOOL DO_ALL_ASSETS_EXIST_FOR_MISSION_TYPE(INT iMission)
	SWITCH iMission
		CASE AM_MISSION_KILL_TARGET_IN_VEH
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[0])
			AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPed[0])
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC SETUP_GLOBALS_FOR_MISSION_TYPE(INT iMission)
	SWITCH iMission
		CASE AM_MISSION_KILL_TARGET_IN_VEH
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[0])
				mpglobals.gameDirGlobalData.vehTarget[0] = NET_TO_VEH(serverBD.niVeh[0])
			ENDIF
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPed[0])
				mpglobals.gameDirGlobalData.pedTarget[0] = NET_TO_PED(serverBD.niPed[0])
			ENDIF
			
		BREAK
	ENDSWITCH
ENDPROC

PROC ADD_PHONECALL_PED_FOR_MISSION_TYPE(INT iMission)
	SWITCH iMission
		CASE AM_MISSION_KILL_TARGET_IN_VEH
			ADD_CONTACT_TO_PHONEBOOK (CHAR_LESTER, MULTIPLAYER_BOOK, FALSE)
			ADD_PED_FOR_DIALOGUE(speechForMission, 1, NULL, "Lester")
		BREAK
	ENDSWITCH
ENDPROC

FUNC enumCharacterList GET_CHAR_FOR_INTRO_PHONECALL(INT iMission)
	enumCharacterList charPhone = CHAR_BLANK_ENTRY
	SWITCH iMission
		CASE AM_MISSION_KILL_TARGET_IN_VEH
			charPhone = CHAR_LESTER
		BREAK
	ENDSWITCH
	
	RETURN charPhone
ENDFUNC

PROC GET_ROOT_ID_FOR_INTRO_PHONECALLS(INT iMission)
	//INT iRand

	IF NOT IS_BIT_SET(iBoolsBitSet, biL_GotPhonecallLabel)
		SWITCH iMission
			CASE AM_MISSION_KILL_TARGET_IN_VEH
				
//				iRand = GET_RANDOM_INT_IN_RANGE(1, 4)	
//				IF iRand = 1 sRootIdIntroPhonecall = "FMA_KIL1"
//				ELIF iRand = 2 sRootIdIntroPhonecall = "FMA_KIL2"
//				ELIF iRand = 3 sRootIdIntroPhonecall = "FMA_KIL3"
//				ENDIF
				sRootIdIntroPhonecall = "FMA_KIL1"
				SET_BIT(iBoolsBitSet, biL_GotPhonecallLabel)
				#IF IS_DEBUG_BUILD NET_DW_PRINT_STRINGS("[GET_ROOT_ID_FOR_INTRO_PHONECALLS] sRootIdIntroPhonecall = ", sRootIdIntroPhonecall) #ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	
ENDPROC
FUNC BOOL DO_INTRO_PHONECALL_FOR_MISSION_TYPE(INT iMission)
	enumCharacterList charPhoneCall
	
	SWITCH iIntroPhoneCount
		CASE 0
			IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE)
				IF NOT IS_SCRIPT_HUD_DISPLAYING(HUDPART_AWARDS ) 
					ADD_PHONECALL_PED_FOR_MISSION_TYPE(iMission)
					iIntroPhoneCount++
					#IF IS_DEBUG_BUILD NET_DW_PRINT("[DO_INTRO_PHONECALL_FOR_MISSION_TYPE] iIntroPhoneCount = 1") #ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
			charPhoneCall = GET_CHAR_FOR_INTRO_PHONECALL(iMission)
			GET_ROOT_ID_FOR_INTRO_PHONECALLS(iMission)
								
			IF Request_MP_Comms_Message(speechForMission, charPhoneCall, "FM_1AU", sRootIdIntroPhonecall)
				CLEAR_BIT(iBoolsBitSet, biL_GotPhonecallLabel)
				iIntroPhoneCount++
				#IF IS_DEBUG_BUILD NET_DW_PRINT("[DO_INTRO_PHONECALL_FOR_MISSION_TYPE] iIntroPhoneCount = 2") #ENDIF
			ENDIF
		BREAK
		
		CASE 2
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_KILL_TARGET_IN_VEH()
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPed[0])
			
		IF NOT IS_NET_PED_INJURED(serverBD.niPed[0])
			//-- For interaction menu quick gps
			IF DOES_BLIP_EXIST(g_blipLesterTarget)
				IF SHOULD_HIDE_LOCAL_UI()
				OR ARE_TEMP_HIDDEN_BLIPS_ACTIVE()
					SET_BLIP_DISPLAY(g_blipLesterTarget, DISPLAY_NOTHING)
				ELSE
					SET_BLIP_DISPLAY(g_blipLesterTarget, DISPLAY_BOTH)
				ENDIF
			ELSE
				g_blipLesterTarget = ADD_BLIP_FOR_ENTITY(NET_TO_PED(serverBD.niPed[0]))
				SET_BLIP_COLOUR(g_blipLesterTarget, BLIP_COLOUR_RED)
				SET_BLIP_SCALE(g_blipLesterTarget, 0.0)
				#IF IS_DEBUG_BUILD NET_DW_PRINT("[PROCESS_KILL_TARGET_IN_VEH] Added global blip g_blipLesterTarget") #ENDIF
			ENDIF
			
			
			IF DOES_BLIP_EXIST(blipTarget.blipId)
				IF SHOULD_HIDE_LOCAL_UI()
				OR ARE_TEMP_HIDDEN_BLIPS_ACTIVE()
					SET_BLIP_DISPLAY(blipTarget.blipId, DISPLAY_NOTHING)
				ELSE
					SET_BLIP_DISPLAY(blipTarget.blipId, DISPLAY_BOTH)
				ENDIF
				IF NOT IS_BIT_SET(iBoolsBitSet, biL_SetBlipName)
					SET_BLIP_NAME_FROM_TEXT_FILE(blipTarget.blipId, "FM_GDM_BLP")
					SET_BIT(iBoolsBitSet, biL_SetBlipName)
					#IF IS_DEBUG_BUILD NET_DW_PRINT("Set blip name") #ENDIF
				ENDIF
			ENDIF
			
			IF DOES_BLIP_EXIST(blipTarget.VehicleBlipID)
				IF SHOULD_HIDE_LOCAL_UI()
				OR ARE_TEMP_HIDDEN_BLIPS_ACTIVE()
					SET_BLIP_DISPLAY(blipTarget.VehicleBlipID, DISPLAY_NOTHING)
				ELSE
					SET_BLIP_DISPLAY(blipTarget.VehicleBlipID, DISPLAY_BOTH)
				ENDIF
				IF NOT IS_BIT_SET(iBoolsBitSet, biL_SetBlipNameVeh)
					SET_BLIP_NAME_FROM_TEXT_FILE(blipTarget.VehicleBlipID, "FM_GDM_BLP")
					SET_BIT(iBoolsBitSet, biL_SetBlipNameVeh)
					#IF IS_DEBUG_BUILD NET_DW_PRINT("Set Veh blip name") #ENDIF
				ENDIF
			ENDIF

			IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneInitialObjective)
				IF NOT IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_WATCHING_MP_TV)
				AND NOT IS_PLAYER_IN_MP_PROPERTY(PLAYER_ID(), FALSE)
					IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
						IF !SHOULD_HIDE_LOCAL_UI()
							Print_Objective_Text("FM_GDM_KIL") // Kill the target
						ENDIF
						SET_BIT(iBoolsBitSet, biL_DoneInitialObjective)
						#IF IS_DEBUG_BUILD NET_DW_PRINT("Done initial objective") #ENDIF
					ENDIF
				ENDIF
			ELSE
				IF IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_WATCHING_MP_TV)
					Clear_Any_Objective_Text_From_This_Script()
					CLEAR_BIT(iBoolsBitSet, biL_DoneInitialObjective)
					#IF IS_DEBUG_BUILD NET_DW_PRINT("Clearing objective text because GLOBAL_SPEC_BS_WATCHING_MP_TV") #ENDIF
				ELIF IS_BROWSER_OPEN()
					Clear_Any_Objective_Text_From_This_Script()
					CLEAR_BIT(iBoolsBitSet, biL_DoneInitialObjective)
					#IF IS_DEBUG_BUILD NET_DW_PRINT("Clearing objective text because g_bBrowserVisible") #ENDIF
				ELIF IS_PLAYER_IN_MP_PROPERTY(PLAYER_ID(), FALSE)
					Clear_Any_Objective_Text_From_This_Script()
					CLEAR_BIT(iBoolsBitSet, biL_DoneInitialObjective)
					#IF IS_DEBUG_BUILD NET_DW_PRINT("Clearing objective text because IS_PLAYER_IN_MP_PROPERTY") #ENDIF
				ENDIF
			ENDIF
		ELSE 
			IF IS_BIT_SET(iBoolsBitSet, biL_DoneInitialObjective)
				Clear_Any_Objective_Text_From_This_Script()
				CLEAR_BIT(iBoolsBitSet, biL_DoneInitialObjective)
			ENDIF
		ENDIF

		WEAPON_TYPE wPlayer
		
		//-- Cash and xp reward
		//-- From 1202252 - 
		//-- Give XP as per the following formula: Rank of player (max 100) * 20
		
		//-- Cash from 1432509
		//-- Give cash as per the following: Random amount between $500 and $2000 rounded to nearest $50.
		
		
		PLAYER_INDEX playerKilledTarget
		
		IF NOT IS_BIT_SET(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitset, biP_KilledTarget)
			playerKilledTarget = NETWORK_GET_DESTROYER_OF_NETWORK_ID(serverBD.niPed[0], wPlayer)
			
			IF playerKilledTarget = PLAYER_ID()
				SET_BIT(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitset, biP_KilledTarget)
				#IF IS_DEBUG_BUILD NET_DW_PRINT("Giving Cash and xp...") #ENDIF
				INT iPlayerRank = GET_RANK_FROM_XP_VALUE(GET_PLAYER_XP(PLAYER_ID()), TRUE)
				

				
				#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("Player rank... ", iPlayerRank) #ENDIF
				IF iPlayerRank > 100
					iPlayerRank = 100
				ENDIF
				INT iXpToGive = iPlayerRank * 20
				#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_FLOAT("RP modifier... ", g_sMPTunables.fxp_tunable_Lester_NPC_Target ) #ENDIF
				#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("RP before modifier... ", iXpToGive) #ENDIF
				
				iXpToGive = ROUND(g_sMPTunables.fxp_tunable_Lester_NPC_Target * iXpToGive)
				
				#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("RP After modifier... ", iXpToGive) #ENDIF
				GIVE_LOCAL_PLAYER_XP(eXPTYPE_CO_OP_AMBIENTFM, "XPT_KAIE",XPTYPE_ACTION, XPCATEGORY_ACTION_KILLS,iXpToGive) // XPT_KAIE = "killed Hostile" xp
				
					
				#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_FLOAT("Giving Cash - High cap modifier = ", g_sMPTunables.fearnings_High_Lester_NPC_target_modifier ) #ENDIF
				
				#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_FLOAT("Giving Cash - Low cap modifier = ", g_sMPTunables.fearnings_Low_Lester_NPC_target_modifier ) #ENDIF
				
				INT iLowCap
				IF g_sMPTunables.fearnings_Low_Lester_NPC_target_modifier <> 0.0 
					iLowCap = ROUND(g_sMPTunables.fearnings_Low_Lester_NPC_target_modifier)
				ELSE
					iLowCap = 2000
				ENDIF
				
				
				#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("Using iLowCap = ", iLowCap) #ENDIF
				
				INT iHighCap
				IF g_sMPTunables.fearnings_High_Lester_NPC_target_modifier <> 0.0 
					iHighCap = ROUND(g_sMPTunables.fearnings_High_Lester_NPC_target_modifier)
				ELSE
					iHighCap = 8000
				ENDIF
				#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("Using iHighCap = ", iHighCap) #ENDIF
				
				INT iMinRand
				IF iLowCap >= 100
					iMinRand = iLowCap / 100	
				ENDIF
				#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("iMinRand = ", iMinRand) #ENDIF
				
				INT iMaxRand
				IF iHighCap >= 100
					iMaxRand = iHighCap / 100
				ENDIF
				#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("iMaxRand = ", iMaxRand) #ENDIF
				
				INT iCashRand = GET_RANDOM_INT_IN_RANGE(iMinRand, (iMaxRand + 1)) * 100
				#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("iCashRand before multiply by tunable  = ", iCashRand) #ENDIF
				
				iCashRand = MULTIPLY_CASH_BY_TUNABLE(iCashRand)
				
				#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("iCashRand after tunable applied =   = ", iCashRand) #ENDIF
				IF iCashRand > iHighCap
					iCashRand = iHighCap
				ENDIF
				#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("Giving cash... ", iCashRand) #ENDIF
				
				PRINTLN(" LESTER KILL TARGET - [MAGNATE_GANG_BOSS] - Cash before boss cut: $", iCashRand)
				GB_HANDLE_GANG_BOSS_CUT(iCashRand)
				PRINTLN(" LESTER KILL TARGET - [MAGNATE_GANG_BOSS] - Cash after boss cut: $", iCashRand)
				
				INT iScriptTransactionIndex
				IF USE_SERVER_TRANSACTIONS()
					IF iCashRand > 0
						TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_LESTER_TARGET_KILL, iCashRand, iScriptTransactionIndex, DEFAULT, DEFAULT)
						g_cashTransactionData[iScriptTransactionIndex].cashInfo.iItemHash = LEST_NPC_BUSINESS_TARGET
					ENDIF
				ELSE
					NETWORK_EARN_FROM_AI_TARGET_KILL(iCashRand, LEST_NPC_BUSINESS_TARGET)
				ENDIF
				
				SET_LAST_JOB_DATA(LAST_JOB_ASSASSINATION, iCashRand)
			//	GIVE_LOCAL_PLAYER_FM_CASH(iCashRand)
				#IF IS_DEBUG_BUILD NET_DW_PRINT("I killed the target!") #ENDIF
			ELSE
				IF playerKilledTarget <> INVALID_PLAYER_INDEX()
					//-- Check for someone other than me killing tarrget
					IF NETWORK_IS_PLAYER_ACTIVE(playerKilledTarget)
						#IF IS_DEBUG_BUILD
							NET_DW_PRINT_STRING_WITH_PLAYER_NAME("I think this player killed the target... ", playerKilledTarget)
						#ENDIF
						
						IF NOT IS_BIT_SET(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitset, biP_KilledByOffMission)
							IF NOT NETWORK_IS_PLAYER_A_PARTICIPANT(playerKilledTarget)
								SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitset, biP_KilledByOffMission)
								#IF IS_DEBUG_BUILD
									NET_DW_PRINT_STRING_WITH_PLAYER_NAME("Target killed by non-participant... ", playerKilledTarget)
								#ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF		
			ENDIF
		ENDIF
	ENDIF
	
	
ENDPROC

PROC MAINTAIN_TICKERS()
	IF PlayerBD[PARTICIPANT_ID_TO_INT()].iMissionProg >= MISSION_COMPLETE_OBJECTIVE
	AND PlayerBD[PARTICIPANT_ID_TO_INT()].iMissionProg < MISSION_CLEANUP
		IF IS_BIT_SET(serverBD.iServerBitSet, biS_SomeoneCompleted)
			IF NOT IS_BIT_SET(iBoolsBitSet, biL_DOnePlayerWonTick) 
				#IF IS_DEBUG_BUILD NET_DW_PRINT("About to do Player won ticker...") #ENDIF
			//	IF serverBD.playerCompleted <> PLAYER_ID()
					
					IF IS_NET_PLAYER_OK(serverBD.playerCompleted, FALSE)
						PRINT_TICKER_WITH_PLAYER_NAME("FM_TGDM_KIL", serverBD.playerCompleted)	
						#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_WITH_PLAYER_NAME("...with this player ", serverBD.playerCompleted) #ENDIF
					ELSE
						#IF IS_DEBUG_BUILD NET_DW_PRINT("... but the winning player isn't ok") #ENDIF
					ENDIF
					
			//	ELSE
			//		#IF IS_DEBUG_BUILD NET_DW_PRINT("... but it was me that won") #ENDIF
			//	ENDIF
				SET_BIT(iBoolsBitSet, biL_DOnePlayerWonTick)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL DO_INTRO_PHONECALL_CLIENT()
	IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneIntroPhonecall)
		IF NOT IS_BIT_SET(iBoolsBitSet, biL_PhonecallFailed)
		OR (IS_BIT_SET(iBoolsBitSet, biL_PhonecallFailed) AND HAS_NET_TIMER_EXPIRED(timeRetryPhonecall, 600000))
			IF DO_INTRO_PHONECALL_FOR_MISSION_TYPE(serverBD.iMissionType)
				SET_BIT(iBoolsBitSet, biL_DoneIntroPhonecall)
				CLEAR_BIT(iBoolsBitSet, biL_PhonecallFailed)
				#IF IS_DEBUG_BUILD NET_DW_PRINT("Triggered intro phonecall") #ENDIF
			ENDIF
		ENDIF
	ELSE	
		IF NOT CHECK_CELLPHONE_LAST_CALL_REJECTED()
			IF NOT IS_BIT_SET(iBoolsBitSet, biL_PhonecallPlaying)
				IF IS_CELLPHONE_CONVERSATION_PLAYING() 
					#IF IS_DEBUG_BUILD NET_DW_PRINT("Phonecall playing") #ENDIF
					SET_BIT(iBoolsBitSet, biL_PhonecallPlaying)
				ELSE
					IF NOT Did_MP_Comms_Complete_Successfully()
					AND NOT Is_MP_Comms_Still_Playing()
						SET_BIT(iBoolsBitSet, biL_PhonecallFailed)
						CLEAR_BIT(iBoolsBitSet, biL_DoneIntroPhonecall)
						START_NET_TIMER(timeRetryPhonecall)
						iIntroPhoneCount = 0
						#IF IS_DEBUG_BUILD NET_DW_PRINT("Phonecall failed to start COnv") #ENDIF
					ENDIF
				ENDIF
			ELSE
				IF NOT Is_MP_Comms_Still_Playing()
					IF Did_MP_Comms_Complete_Successfully()
						RETURN TRUE
					ELSE
						#IF IS_DEBUG_BUILD NET_DW_PRINT("Phonecall failed to finish, but will continue.") #ENDIF
						RETURN TRUE
//						SET_BIT(iBoolsBitSet, biL_PhonecallFailed)
//						CLEAR_BIT(iBoolsBitSet, biL_DoneIntroPhonecall)
//						START_NET_TIMER(timeRetryPhonecall)
//						iIntroPhoneCount = 0
						
					ENDIF
				ENDIF
			ENDIF
		ELSE
			SET_BIT(iBoolsBitSet, biL_PhonecallFailed)
			CLEAR_BIT(iBoolsBitSet, biL_DoneIntroPhonecall)
			START_NET_TIMER(timeRetryPhonecall)
			iIntroPhoneCount = 0
			#IF IS_DEBUG_BUILD NET_DW_PRINT("Phonecall rejected") #ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE	
ENDFUNC

FUNC BOOL DO_INVITE_CLIENT()
	IF NOT IS_BIT_SET(iBoolsBitSet, biL_SentInvite)
		Store_Basic_Invite_Details_For_Joblist_From_NPC(CHAR_LESTER, FMMC_TYPE_LESTER_KILL_TARGET, "")
		SET_BIT(iBoolsBitSet, biL_SentInvite)
		#IF IS_DEBUG_BUILD NET_DW_PRINT("[DO_INVITE_CLIENT] Sent invite") #ENDIF
	ELSE 
		IF NOT IS_BIT_SET(iBoolsBitSet, biL_AcceptedInvite)
			IF (Has_Player_Accepted_Basic_Invite_From_NPC(CHAR_LESTER, FMMC_TYPE_LESTER_KILL_TARGET))
				SET_BIT(iBoolsBitSet, biL_AcceptedInvite)
				#IF IS_DEBUG_BUILD NET_DW_PRINT("[DO_INVITE_CLIENT] Accepted invite") #ENDIF
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC
PROC PROCESS_CLIENT_STAGGERED_LOOP()
	INT i
	
	
	PLAYER_INDEX player
	
	IF IS_BIT_SET(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitset, biP_PlayerOnMission)
		IF iPlayerStaggeredCount >= COUNT_OF(PlayerBD) 
			iPlayerStaggeredCount = 0
			CLEAR_BIT(iBoolsBitSet, biL_LocalLoopedThroughEveryone)
			
		ENDIF
		
		i = iPlayerStaggeredCount
		
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
			player = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
			IF IS_NET_PLAYER_OK(player, FALSE)
				
			ENDIF
		ENDIF
		
		iPlayerStaggeredCount++
		
		IF iServerStaggeredCount >= COUNT_OF(PlayerBD) 
			SET_BIT(iBoolsBitSet, biL_LocalLoopedThroughEveryone)
			
			
		ENDIF
	ENDIF
ENDPROC
PROC PROCESS_MISSION_CLIENT()
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///     Server picks mission type
	///     Only one at a time
	///     I go on mission if I haven't done anything for a while


	MAINTAIN_LOCAL_PLAYER_CHECKS()
	
	MAINTAIN_TICKERS()
	
	//-- Check for launch timeout
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_LaunchTimeOut)
		IF PlayerBD[PARTICIPANT_ID_TO_INT()].iMissionProg < MISSION_CLEANUP
			PlayerBD[PARTICIPANT_ID_TO_INT()].iMissionProg = MISSION_CLEANUP
			#IF IS_DEBUG_BUILD 
				NET_DW_PRINT("iMissionProg = MISSION_CLEANUP as biS_LaunchTimeOut is set")
				NET_DW_PRINT_HOST_NAME()
			#ENDIF
		ENDIF
	ENDIF
	
	//-- Check for someone completeing
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_SomeoneCompleted)
		IF PlayerBD[PARTICIPANT_ID_TO_INT()].iMissionProg <= MISSION_CLEANUP
			PlayerBD[PARTICIPANT_ID_TO_INT()].iMissionProg = MISSION_CLEANUP
			#IF IS_DEBUG_BUILD 
				NET_DW_PRINT("iMissionProg = MISSION_CLEANUP as biS_SomeoneCompleted is set")
				NET_DW_PRINT_HOST_NAME()
			#ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_KilledByOffMission)
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_SomeoneCompleted)
			IF PlayerBD[PARTICIPANT_ID_TO_INT()].iMissionProg <= MISSION_CLEANUP
				PlayerBD[PARTICIPANT_ID_TO_INT()].iMissionProg = MISSION_CLEANUP
				TickerData.TickerEvent = TICKER_EVENT_GAMEDIR_TARGET_TIMEOUT
				BROADCAST_TICKER_EVENT(TickerData,ALL_PLAYERS_ON_SCRIPT(TRUE))
				#IF IS_DEBUG_BUILD 
					NET_DW_PRINT("iMissionProg = MISSION_CLEANUP as biS_KilledByOffMission is set")
					NET_DW_PRINT_HOST_NAME()
				#ENDIF
			ENDIF
		ENDIF
	ENDIF

	//-- Check for timeout due to nobody near target
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_NobodyNearTarget)
		IF PlayerBD[PARTICIPANT_ID_TO_INT()].iMissionProg <= MISSION_CLEANUP
			PlayerBD[PARTICIPANT_ID_TO_INT()].iMissionProg = MISSION_CLEANUP
			TickerData.TickerEvent = TICKER_EVENT_GAMEDIR_TARGET_TIMEOUT
			BROADCAST_TICKER_EVENT(TickerData,ALL_PLAYERS_ON_SCRIPT(TRUE))
			#IF IS_DEBUG_BUILD 
				NET_DW_PRINT("iMissionProg = MISSION_CLEANUP as biS_NobodyNearTarget is set")
				NET_DW_PRINT_HOST_NAME()
			#ENDIF
		ENDIF
	ENDIF
	
	//-- Check for s-skipping
	#IF IS_DEBUG_BUILD
		IF IS_BIT_SET(serverBD.iServerDebugBitset, biSdebug_SomeoneDebugSkipped)
			IF PlayerBD[PARTICIPANT_ID_TO_INT()].iMissionProg < MISSION_CLEANUP
				PlayerBD[PARTICIPANT_ID_TO_INT()].iMissionProg = MISSION_CLEANUP
				NET_DW_PRINT("iMissionProg = MISSION_CLEANUP as biSdebug_SomeoneDebugSkipped is set")
				NET_DW_PRINT_HOST_NAME()
			ENDIF
		ENDIF
	#ENDIF
	
	//-- Check for myself going on a DM or something
	IF PlayerBD[PARTICIPANT_ID_TO_INT()].iMissionProg > MISSION_WAIT_FOR_LAUNCH
		IF PlayerBD[PARTICIPANT_ID_TO_INT()].iMissionProg < MISSION_CLEANUP
			IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
			OR IS_FM_MISSION_LAUNCH_IN_PROGRESS()
			OR GET_LOCAL_PLAYER_CORONA_POS_STATE() > 0
			OR IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_GANGHIDEOUT)
			OR IS_PLAYER_IN_CORONA() 

				PlayerBD[PARTICIPANT_ID_TO_INT()].iMissionProg = MISSION_CLEANUP
				#IF IS_DEBUG_BUILD NET_DW_PRINT("iMissionProg -> MISSION_CLEANUP as I'm on a mission")
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//-- Check for local player being a spectator
	IF PlayerBD[PARTICIPANT_ID_TO_INT()].iMissionProg > MISSION_WAIT_FOR_LAUNCH
		IF PlayerBD[PARTICIPANT_ID_TO_INT()].iMissionProg < MISSION_CLEANUP
			IF IS_PLAYER_SPECTATING(PLAYER_ID())
				PlayerBD[PARTICIPANT_ID_TO_INT()].iMissionProg = MISSION_CLEANUP
				#IF IS_DEBUG_BUILD NET_DW_PRINT("iMissionProg -> MISSION_CLEANUP as I'm spectating")
				#ENDIF
			
			ENDIF
		ENDIF
	ENDIF
		
	
	SWITCH PlayerBD[PARTICIPANT_ID_TO_INT()].iMissionProg
		CASE MISSION_WAIT_FOR_LAUNCH
			
			IF IS_BIT_SET(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitset, biP_NotDoneAnything)
				IF IS_BIT_SET(serverBD.iServerBitSet, biS_MissionLaunched)	
					IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_TwoMinutesLeft)
						SET_BIT(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitset, biP_PlayerOnMission)
						SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_GAMEDIR_MISSION, TRUE)
						PlayerBD[PARTICIPANT_ID_TO_INT()].iMissionProg = MISSION_WAIT_FOR_ASSETS
						#IF IS_DEBUG_BUILD NET_DW_PRINT("iMissionProg MISSION_WAIT_FOR_LAUNCH -> MISSION_WAIT_FOR_ASSETS")
						#ENDIF
					ELSE
						//-- DOn't bother proceeding if there's less than 2 minutes left until auto-cleanup
						PlayerBD[PARTICIPANT_ID_TO_INT()].iMissionProg = MISSION_CLEANUP
						SET_BIT(iBoolsBitSet, biL_JoinedTooLate)
						#IF IS_DEBUG_BUILD NET_DW_PRINT("iMissionProg MISSION_WAIT_FOR_LAUNCH -> MISSION_CLEANUP as biS_TwoMinutesLeft is set")
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
		CASE MISSION_WAIT_FOR_ASSETS
			IF DO_ALL_ASSETS_EXIST_FOR_MISSION_TYPE(serverBD.iMissionType)
				SETUP_GLOBALS_FOR_MISSION_TYPE(serverBD.iMissionType)
				PlayerBD[PARTICIPANT_ID_TO_INT()].iMissionProg = MISSION_DO_INTRO_PHONECALL
				#IF IS_DEBUG_BUILD 
					NET_DW_PRINT("iMissionProg MISSION_WAIT_FOR_ASSETS -> MISSION_DO_INTRO_PHONECALL")
					NET_DW_PRINT_HOST_NAME()
				#ENDIF
			ENDIF
		BREAK
		
		CASE MISSION_DO_INTRO_PHONECALL
			IF NOT HAS_LESTER_KILL_TARGET_LAUNCHED_AT_LEAST_ONCE()
				IF DO_INTRO_PHONECALL_CLIENT()
				
					PlayerBD[PARTICIPANT_ID_TO_INT()].iMissionProg = MISSION_COMPLETE_OBJECTIVE
					RESET_REMINDER_TIMER_FOR_ACTIVITY(FMMC_TYPE_LESTER_REQUEST_JOB)
					SET_LESTER_KILL_TARGET_LAUNCHED_AT_LEAST_ONCE()
					#IF IS_DEBUG_BUILD NET_DW_PRINT("iMissionProg MISSION_DO_INTRO_PHONECALL -> MISSION_COMPLETE_OBJECTIVE")
					#ENDIF
				ELSE
					IF IS_BIT_SET(iBoolsBitSet, biL_PhonecallFailed)
						PlayerBD[PARTICIPANT_ID_TO_INT()].iMissionProg = MISSION_CLEANUP
						#IF IS_DEBUG_BUILD NET_DW_PRINT("iMissionProg MISSION_DO_INTRO_PHONECALL -> MISSION_CLEANUP as biL_PhonecallFailed is set")
						#ENDIF
					ENDIF
				ENDIF
			ELSE
				IF DO_INVITE_CLIENT()
					PlayerBD[PARTICIPANT_ID_TO_INT()].iMissionProg = MISSION_COMPLETE_OBJECTIVE
					#IF IS_DEBUG_BUILD NET_DW_PRINT("iMissionProg MISSION_DO_INTRO_PHONECALL -> MISSION_COMPLETE_OBJECTIVE")
					#ENDIF
				ELSE
					IF IS_BIT_SET(iBoolsBitSet, biL_PhonecallFailed)
						PlayerBD[PARTICIPANT_ID_TO_INT()].iMissionProg = MISSION_CLEANUP
						#IF IS_DEBUG_BUILD NET_DW_PRINT("iMissionProg MISSION_DO_INTRO_PHONECALL -> MISSION_CLEANUP as biL_PhonecallFailed is set")
						#ENDIF
					ENDIF
					
					IF IS_BIT_SET(serverBD.iServerBitSet, biS_TwoMinutesLeft)
						//-- DOn't bother proceeding if there's less than 2 minutes left until auto-cleanup
						Cancel_Basic_Invite_From_NPC(CHAR_LESTER, FMMC_TYPE_LESTER_KILL_TARGET)
						PlayerBD[PARTICIPANT_ID_TO_INT()].iMissionProg = MISSION_CLEANUP
						SET_BIT(iBoolsBitSet, biL_JoinedTooLate)
						#IF IS_DEBUG_BUILD NET_DW_PRINT("iMissionProg MISSION_DO_INTRO_PHONECALL -> MISSION_CLEANUP as biS_TwoMinutesLeft is set")
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE MISSION_COMPLETE_OBJECTIVE
			IF serverBD.iMissionType = AM_MISSION_KILL_TARGET_IN_VEH
				PROCESS_KILL_TARGET_IN_VEH()
			ENDIF
		BREAK
		
		CASE MISSION_CLEANUP
			PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitset = 0
			CLEAR_BIT(iBoolsBitSet, biL_DOnePlayerWonTick)
			CLEAR_BIT(iBoolsBitSet, biL_DoneIntroPhonecall)
			iIntroPhoneCount = 0
			
			#IF IS_DEBUG_BUILD
				NET_DW_PRINT("Case MISSION_CLEANUP - cancelling ivite")
			#ENDIF
			Cancel_Basic_Invite_From_NPC(CHAR_LESTER, FMMC_TYPE_LESTER_KILL_TARGET)
			IF DOES_BLIP_EXIST(g_blipLesterTarget)
				REMOVE_BLIP(g_blipLesterTarget)
				#IF IS_DEBUG_BUILD NET_DW_PRINT("CASE MISSION_CLEANUP removing g_blipLesterTarget") #ENDIF
			ENDIF
			CLEANUP_AI_PED_BLIP(blipTarget)
			Clear_Any_Objective_Text_From_This_Script()
			SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_GAMEDIR_MISSION, FALSE)
			PlayerBD[PARTICIPANT_ID_TO_INT()].iMissionProg = MISSION_RESET
			#IF IS_DEBUG_BUILD NET_DW_PRINT("iMissionProg MISSION_CLEANUP -> MISSION_RESET")
			#ENDIF
		BREAK
		
		CASE MISSION_RESET
			IF serverBD.iServerMissionProg = MISSION_WAIT_FOR_LAUNCH
			AND NOT IS_BIT_SET(serverBD.iServerBitSet, biS_LaunchTimeOut)
			#IF IS_DEBUG_BUILD AND NOT IS_BIT_SET(serverBD.iServerDebugBitset, biSdebug_SomeoneDebugSkipped) #ENDIF
				PlayerBD[PARTICIPANT_ID_TO_INT()].iMissionProg = MISSION_WAIT_FOR_LAUNCH
				#IF IS_DEBUG_BUILD NET_DW_PRINT("iMissionProg MISSION_RESET -> MISSION_WAIT_FOR_LAUNCH")
				#ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	
ENDPROC	

PROC CHECK_FOR_ENTITY_DAMAGE(INT iPart)
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_SomeoneDamagedVeh1)
			IF IS_BIT_SET(PlayerBD[ipart].iPlayerBitset, biP_DamagedVehicle1)
				SET_BIT(serverBD.iServerBitSet, biS_SomeoneDamagedVeh1)
				#IF IS_DEBUG_BUILD
					NET_DW_PRINT_STRING_WITH_PLAYER_NAME("Server thinks this player damaged Vehicle 1 ", NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart)))
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC	

PROC PROCESS_SERVER_STAGGERED_LOOP()
	INT i
	
	INT iNonStaggeredLoop
	INT iPlayer
	PLAYER_INDEX player
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF iServerStaggeredCount >= COUNT_OF(PlayerBD) 
			iServerStaggeredCount = 0
			iNumberOfPlayersReady = 0
			CLEAR_BIT(iBoolsBitSet, BiL_ServerLoopedThroughEveryone)
			SET_BIT(iBoolsBitSet, biL_ServStaggeredAllFinished)
			CLEAR_BIT(serverBD.iServerBitSet, biS_ServerFoundSomeoneStillRunning)
			CLEAR_BIT(serverBD.iServerBitSet, biS_FoundNewPlayer)
			CLEAR_BIT(iBoolsBitSet, biL_SomeoneAimingAtTarget)	
			CLEAR_BIT(iBoolsBitSet, biS_StuckOnRoofOrSide)
			
			//-- Check for being killed by off mission.
			//-- If target is killed, need to wait until we've looped through all on mission players to look for a killer
			//-- before deciding it was an off-mission player
//			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_KilledByOffMission)
//				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPed[0])
//					IF IS_NET_PED_INJURED(serverBD.niPed[0])
//						IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_SomeoneCompleted)
//							IF NOT IS_BIT_SET(iBoolsBitSet, biL_TargetDeadNotSureWhoKilled)
//								SET_BIT(iBoolsBitSet, biL_TargetDeadNotSureWhoKilled)
//								#IF IS_DEBUG_BUILD
//									NET_DW_PRINT("Set biL_TargetDeadNotSureWhoKilled as Target killed but not sure who killed him.")
//								#ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF

			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_KilledByOffMission)
				IF IS_BIT_SET(serverBD.iServerBitSet, biS_StaggeredTargetKilledByOffMission)
					IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_SomeoneCompleted)
						SET_BIT(serverBD.iServerBitSet, biS_KilledByOffMission)
						#IF IS_DEBUG_BUILD
							NET_DW_PRINT("[PROCESS_SERVER_STAGGERED_LOOP] Set biS_KilledByOffMission.")
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
				
			
			IF HAS_NET_TIMER_STARTED(serverBD.timeNearTarget)
				IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_InitialDistanceCheckExpired)
					IF HAS_NET_TIMER_EXPIRED(serverBD.timeNearTarget, TIME_NEAR_PLAYERS_INITIAL)
						SET_BIT(serverBD.iServerBitSet, biS_InitialDistanceCheckExpired)
						#IF IS_DEBUG_BUILD NET_DW_PRINT("Setting biS_InitialDistanceCheckExpired as TIME_NEAR_PLAYERS_INITIAL expired") #ENDIF
					
					ENDIF
					
					IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_TwoMinutesLeft)
						IF HAS_NET_TIMER_EXPIRED(serverBD.timeNearTarget, (TIME_NEAR_PLAYERS_INITIAL - 120000))
							SET_BIT(serverBD.iServerBitSet, biS_TwoMinutesLeft)
							#IF IS_DEBUG_BUILD NET_DW_PRINT("Setting biS_TwoMinutesLeft as two minutes left") #ENDIF
						ENDIF
					ENDIF
				ELSE
					SET_BIT(iBoolsBitSet, biL_NobodyNearTarget)
				ENDIF
				
				
			ENDIF
			
			
		ENDIF
		
		i = iServerStaggeredCount
		
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
			player = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
			iPlayer = NATIVE_TO_INT(player)
			IF IS_BIT_SET(iBoolsBitSet, biL_ServStaggeredAllFinished)
				IF PlayerBD[i].iMissionProg < MISSION_RESET
					CLEAR_BIT(iBoolsBitSet, biL_ServStaggeredAllFinished)
					#IF IS_DEBUG_BUILD
						IF bWdDoStaggeredDebug
							NET_DW_PRINT_STRING_WITH_PLAYER_NAME("[ServerStaggered] Clearing biL_ServStaggeredAllFinished as this player not finished ", player )
						ENDIF

						

					#ENDIF
				ENDIF
			ENDIF
			
			
			//-- Count how many players are ready
			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_EnoughPlayersReadyForLaunch)
				IF IS_BIT_SET(PlayerBD[i].iPlayerBitset, biP_NotDoneAnything)
					#IF IS_DEBUG_BUILD
						IF bWdDoStaggeredDebug
							NET_DW_PRINT_STRING_WITH_PLAYER_NAME("[ServerStaggered] Server thinks this player is ready for ambient mission ", player )
						ENDIF
					#ENDIF
					iNumberOfPlayersReady++
				ENDIF
			ELSE
				IF IS_BIT_SET(PlayerBD[i].iPlayerBitset, biP_PlayerOnMission)
					#IF IS_DEBUG_BUILD
						IF bWdDoStaggeredDebug
							NET_DW_PRINT_STRING_WITH_PLAYER_NAME("[ServerStaggered] Server thinks this player is on mission ", player )
						ENDIF
					#ENDIF
					
					//-- Check for new players joining and then reset the nearby player timer if so, so everyone has a reasonable amount of time
					IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_FoundNewPlayer)
						IF NOT IS_BIT_SET(serverBD.iPlayersOnMission, iPlayer)
							IF PlayerBD[i].iMissionProg > MISSION_DO_INTRO_PHONECALL
								SET_BIT(serverBD.iPlayersOnMission, iPlayer)
								SET_BIT(serverBD.iServerBitSet, biS_FoundNewPlayer)
								#IF IS_DEBUG_BUILD
									NET_DW_PRINT_STRING_WITH_PLAYER_NAME("[ServerStaggered] Server thinks this player is new ", player )
								#ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					
					IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_ServerFoundSomeoneStillRunning)
						SET_BIT(serverBD.iServerBitSet, biS_ServerFoundSomeoneStillRunning)
					ENDIF
					
					IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_SomeoneCompleted)
						IF IS_BIT_SET(PlayerBD[i].iPlayerBitset, biP_KilledTarget)
							serverBD.playerCompleted = player
							SET_BIT(serverBD.iServerBitSet, biS_SomeoneCompleted)
							CLEAR_BIT(iBoolsBitSet, biL_TargetDeadNotSureWhoKilled)
							CLEAR_BIT(serverBD.iServerBitSet, biS_StaggeredTargetKilledByOffMission)
							#IF IS_DEBUG_BUILD	
								NET_DW_PRINT_STRING_WITH_PLAYER_NAME("[ServerStaggered] Server thinks this player completed mission ", player )
							#ENDIF
						ENDIF
					ENDIF
					
					//-- CHeck for target being killed by off-mission player
					IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_SomeoneCompleted)
						IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_StaggeredTargetKilledByOffMission)
							IF IS_BIT_SET(PlayerBD[i].iPlayerBitset, biP_KilledByOffMission)
								SET_BIT(serverBD.iServerBitSet, biS_StaggeredTargetKilledByOffMission)
								#IF IS_DEBUG_BUILD	
									NET_DW_PRINT_STRING_WITH_PLAYER_NAME("[ServerStaggered] Server thinks this player detected target killed by off-mission player ", player )
								#ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					//--Check for player's near target
					IF IS_BIT_SET(iBoolsBitSet, biL_NobodyNearTarget)
						IF IS_BIT_SET(PlayerBD[i].iPlayerBitset, biP_NearTarget)
							CLEAR_BIT(iBoolsBitSet, biL_NobodyNearTarget)
							#IF IS_DEBUG_BUILD
								IF bWdDoStaggeredDebug
									NET_DW_PRINT_STRING_WITH_PLAYER_NAME("[ServerStaggered] Server thinks this player near target ", player )
								ENDIF
							#ENDIF
						ENDIF
					ENDIF
					
					//-- CHeck for someone aiming at target
					IF NOT IS_BIT_SET(iBoolsBitSet, biL_SomeoneAimingAtTarget)	
						IF IS_BIT_SET(PlayerBD[i].iPlayerBitset, biP_AimAtTarget)
							SET_BIT(iBoolsBitSet, biL_SomeoneAimingAtTarget)
							#IF IS_DEBUG_BUILD
								IF bWdDoStaggeredDebug
									NET_DW_PRINT_STRING_WITH_PLAYER_NAME("[ServerStaggered] Server thinks this player aiming at target ", player )
								ENDIF
							#ENDIF
						ENDIF
					ENDIF
					
					//-- Check for target stuck on roof/ side
					IF NOT IS_BIT_SET(iBoolsBitSet, biL_StuckOnRoofOrSide)	
						IF IS_BIT_SET(PlayerBD[i].iPlayerBitset, biP_StuckOnRoofOrSide)
							SET_BIT(iBoolsBitSet, biL_StuckOnRoofOrSide)
							#IF IS_DEBUG_BUILD
								IF bWdDoStaggeredDebug
									NET_DW_PRINT_STRING_WITH_PLAYER_NAME("[ServerStaggered] Server thinks this player detected car stuck on roof.side ", player )
								ENDIF
							#ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			#IF IS_DEBUG_BUILD
				IF NOT IS_BIT_SET(serverBD.iServerDebugBitset, biSdebug_SomeoneDebugSkipped)
					IF IS_NET_PLAYER_OK(player, FALSE)
						IF PlayerBD[i].bSpassed
							SET_BIT(serverBD.iServerDebugBitset, biSdebug_SomeoneDebugSkipped)
							NET_DW_PRINT_STRING_WITH_PLAYER_NAME("Server thinks this player pressed s ", player)
						ENDIF
					ENDIF
				ENDIF
				
					IF NOT IS_BIT_SET(serverBD.iServerDebugBitset, biSdebug_SomeoneDebugSkipped)
						IF MPGlobalsAmbience.bKillActiveEvent 
							SET_BIT(serverBD.iServerDebugBitset, biSdebug_SomeoneDebugSkipped)
							MPGlobalsAmbience.bKillActiveEvent = FALSE
							NET_DW_PRINT("Ending as server thinks bKillActiveEvent is set ")
						ENDIF
					ENDIF
			#ENDIF
			//-- Check for this participant damaging any of the target entities
			
			
		ENDIF
		
		FOR iNonStaggeredLoop = 0 TO NUM_NETWORK_PLAYERS - 1
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iNonStaggeredLoop))
				CHECK_FOR_ENTITY_DAMAGE(iNonStaggeredLoop)
			ENDIF
		ENDFOR
		
		iServerStaggeredCount++
		
		IF iServerStaggeredCount >= COUNT_OF(PlayerBD) 
			//-- Check for being killed by an off mission player
			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_KilledByOffMission)
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPed[0])
					IF IS_NET_PED_INJURED(serverBD.niPed[0])
						IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_SomeoneCompleted)
							IF IS_BIT_SET(iBoolsBitSet, biL_TargetDeadNotSureWhoKilled)
								IF NOT HAS_NET_TIMER_STARTED(timeKilledOffMission)
									START_NET_TIMER(timeKilledOffMission)
									#IF IS_DEBUG_BUILD
										NET_DW_PRINT("biL_TargetDeadNotSureWhoKilled is set, starting timeKilledOffMission")
									#ENDIF
								ELSE
									IF HAS_NET_TIMER_EXPIRED(timeKilledOffMission, 2000)
										SET_BIT(serverBD.iServerBitSet, biS_KilledByOffMission)
										#IF IS_DEBUG_BUILD
											NET_DW_PRINT("Set biS_KilledByOffMission as Target killed but don't know who killed him.")
										#ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			SET_BIT(iBoolsBitSet, BiL_ServerLoopedThroughEveryone)
			
			//-- CHeck there's enough players to launch mission
			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_EnoughPlayersReadyForLaunch)
				IF iNumberOfPlayersReady >= MIN_PLAYERS_FOR_LAUNCH
					SET_BIT(serverBD.iServerBitSet, biS_EnoughPlayersReadyForLaunch)
					#IF IS_DEBUG_BUILD	NET_DW_PRINT_STRING_INT("Setting biS_EnoughPlayersReadyForLaunch as iNumberOfPlayersReady = ", iNumberOfPlayersReady)
					#ENDIF
				ENDIF
			ENDIF
			
			//-- Check for someone aiming at target
			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_SOmeoneAimingAtTarget)
				IF IS_BIT_SET(iBoolsBitSet, biL_SomeoneAimingAtTarget)
					SET_BIT(serverBD.iServerBitSet, biS_SOmeoneAimingAtTarget)
					#IF IS_DEBUG_BUILD NET_DW_PRINT("Server set biS_SOmeoneAimingAtTarget") #ENDIF
				ENDIF
			ENDIF
			
			//-- Check for target stuck on roof
			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_StuckOnRoofOrSide)
				IF IS_BIT_SET(iBoolsBitSet, biL_StuckOnRoofOrSide)
					SET_BIT(serverBD.iServerBitSet, biS_StuckOnRoofOrSide)
					#IF IS_DEBUG_BUILD NET_DW_PRINT("Server set biS_StuckOnRoofOrSide") #ENDIF
				ENDIF
			ENDIF
			
			//-- Check for all players finishing
			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_AllPlayersFinished)
				IF IS_BIT_SET(iBoolsBitSet, biL_ServStaggeredAllFinished)
					IF serverBD.iServerMissionProg > MISSION_CREATE_ASSETS
					AND serverBD.iServerMissionProg <= MISSION_RESET
						#IF IS_DEBUG_BUILD NET_DW_PRINT("Server set biS_AllPlayersFinished") #ENDIF
						SET_BIT(serverBD.iServerBitSet, biS_AllPlayersFinished)		
					ELIF IS_BIT_SET(serverBD.iServerBitSet, biS_LaunchTimeOut)
						#IF IS_DEBUG_BUILD NET_DW_PRINT("Server set biS_AllPlayersFinished as timeout reached") #ENDIF
						SET_BIT(serverBD.iServerBitSet, biS_AllPlayersFinished)	
					ENDIF
				ENDIF
			ENDIF
			
			//-- Reset the nearby player timer if someone new joins
			IF IS_BIT_SET(serverBD.iServerBitSet, biS_FoundNewPlayer)
				CLEAR_BIT(serverBD.iServerBitSet, biS_FoundNewPlayer)
				CLEAR_BIT(serverBD.iServerBitSet, biS_TwoMinutesLeft)
				CLEAR_BIT(serverBD.iServerBitSet, biS_InitialDistanceCheckExpired)
				RESET_NET_TIMER(serverBD.timeNearTarget)
				START_NET_TIMER(serverBD.timeNearTarget)
				#IF IS_DEBUG_BUILD NET_DW_PRINT("Resetting nearby timer as a new player has joined") #ENDIF
			ENDIF
			
			//-- Check for timeout due to nobody being near the target
			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_NobodyNearTarget)
				IF IS_BIT_SET(serverBD.iServerBitSet, biS_InitialDistanceCheckExpired)
					IF IS_BIT_SET(iBoolsBitSet, biL_NobodyNearTarget)
						SET_BIT(serverBD.iServerBitSet, biS_NobodyNearTarget)
						#IF IS_DEBUG_BUILD NET_DW_PRINT("Setting biS_NobodyNearTarget because nobody near target")#ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			
		ENDIF
	ENDIF
ENDPROC

FUNC INT GET_NEXT_AMBIENT_MISSION_TYPE()
	RETURN AM_MISSION_KILL_TARGET_IN_VEH
ENDFUNC



FUNC VECTOR GET_START_COORD_FOR_MISSION_TYPE(INT iType, INT iCand)
	VECTOR v = <<0.0, 0.0, 0.0>>
	SWITCH iType
		CASE AM_MISSION_KILL_TARGET_IN_VEH
			IF iCand = 0 	v = <<-1228.9904, -1092.2307, 7.0465>>
			ELIF iCand = 1 v = <<-618.6293, 252.6611, 80.5897>>  
			ELIF iCand = 2 v = <<796.6768, -2029.6017, 28.1346>>
			ELIF iCand = 3 v = <<-1622.8806, 1253.5157, 139.6713>>
			ELIF iCand = 4 v = <<1123.2698, 1878.9169, 65.7957>>
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN v
ENDFUNC

FUNC BOOL GET_KILL_TARGET_START_POSITION(VECTOR &vPos, FLOAT &fHead)
	
	VECTOR vPos1, vPos2, vCreate
	FLOAT fCreateHead
	
	
	IF NOT ARE_VECTORS_EQUAL(vPos, <<0.0, 0.0, 0.0>>)
		RETURN TRUE
	ENDIF
	
	
	
	SWITCH iCandidate
		CASE 0
			IF IS_BIT_SET(serverBD.iCandidateSpawnBitset, iCandidate)
				#IF IS_DEBUG_BUILD NET_DW_PRINT("[GET_KILL_TARGET_START_POSITION] iCandidate = 0 bit is set ") #ENDIF
				vPos1 = <<-1232.8418, -1095.0028 , 2.2331>>
				vPos2 = <<-1226.2480, -1089.8192, 10.1513>>   
				vCreate = GET_START_COORD_FOR_MISSION_TYPE(AM_MISSION_KILL_TARGET_IN_VEH, 0)
				fCreateHead = 19.7853
			ENDIF
		BREAK
		
		CASE 1
			IF IS_BIT_SET(serverBD.iCandidateSpawnBitset, iCandidate)
				#IF IS_DEBUG_BUILD NET_DW_PRINT("[GET_KILL_TARGET_START_POSITION] iCandidate = 1 bit is set ") #ENDIF
				vPos1 = <<-622.0966, 250.3288, 75.7075>>
				vPos2 = <<-614.8635, 255.2288, 88.7755>> 
				vCreate = GET_START_COORD_FOR_MISSION_TYPE(AM_MISSION_KILL_TARGET_IN_VEH, 1)
				fCreateHead = 266.4686
			ENDIF
		BREAK
		
		CASE 2
			IF IS_BIT_SET(serverBD.iCandidateSpawnBitset, iCandidate)
				#IF IS_DEBUG_BUILD NET_DW_PRINT("[GET_KILL_TARGET_START_POSITION] iCandidate = 2 bit is set ") #ENDIF
				vPos1 = <<794.4784, -2034.3386, 25.2469>>
				vPos2 = <<798.8026, -2026.0035 , 33.2793>>  
				vCreate = GET_START_COORD_FOR_MISSION_TYPE(AM_MISSION_KILL_TARGET_IN_VEH, 2)
				fCreateHead = 355.9428
			ENDIF
		BREAK
		
		CASE 3
			IF IS_BIT_SET(serverBD.iCandidateSpawnBitset, iCandidate)
				#IF IS_DEBUG_BUILD NET_DW_PRINT("[GET_KILL_TARGET_START_POSITION] iCandidate = 3 bit is set ") #ENDIF
				vPos1 =  <<-1623.9193,  1249.8311 , 135.2495>>
				vPos2 =  <<-1621.5536, 1257.9800, 145.2376>>
				vCreate = GET_START_COORD_FOR_MISSION_TYPE(AM_MISSION_KILL_TARGET_IN_VEH, 3)	
				fCreateHead = 353.7463 
			ENDIF
		BREAK
		
		CASE 4
			IF IS_BIT_SET(serverBD.iCandidateSpawnBitset, iCandidate)
				#IF IS_DEBUG_BUILD NET_DW_PRINT("[GET_KILL_TARGET_START_POSITION] iCandidate = 4 bit is set ") #ENDIF
				vPos1 =  <<1118.0770, 1875.0347, 60.3776>>
				vPos2 =  <<1127.6384, 1883.0828, 70.1053>>  
				vCreate =	GET_START_COORD_FOR_MISSION_TYPE(AM_MISSION_KILL_TARGET_IN_VEH, 4)	
				fCreateHead =201.3967
			ENDIF
		BREAK
	/*	
		CASE 5
			vPos1 =  << -1634.7076, -890.8490, 6.9700 >>
			vPos2 =   << -1633.9034, -879.8690, 9.1264 >>  
			vCreate = << -1634.0059, -885.3531, 8.0518 >> 	 
			fCreateHead = 321.3100
		BREAK
		
		CASE 6
			vPos1 =  << -338.4312, -947.4233, 28.0788 >>
			vPos2 =  << -331.5926, -944.4080, 32.0788 >> 
			vCreate = << -334.8547, -945.2789, 30.0788 >>
			fCreateHead = 69.0442
		BREAK
		
		CASE 7
			vPos1 =  << 1093.9250, 245.6548, 77.9908 >>
			vPos2 =   << 1094.2432, 255.0715,82.8556 >> 
			vCreate =  << 1093.6864, 250.4772, 79.8556 >>
			fCreateHead = 328.5602 
		BREAK
		
		CASE 8
			vPos1 =  << -1407.8231, 58.1796, 50.8018 >>
			vPos2 =   << -1400.1899, 63.9074, 54.3222 >>
			vCreate = << -1404.4590, 62.1459, 52.0258 >>
			fCreateHead = 241.2814 
		BREAK
		
		CASE 9
			vPos1 =  << -1230.3242, -1656.8137, 2.0412 >>
			vPos2 =  << -1226.4288, -1648.1426, 4.1986 >>
			vCreate = << -1228.9010, -1652.3970, 3.1204 >>
			fCreateHead = 305.0972
		BREAK
		*/
	ENDSWITCH
	
	IF iCandidate < MAX_SPAWN_POS
		IF NOT ARE_VECTORS_EQUAL(vPos1, << 0.0, 0.0, 0.0>>)
		
			IF serverBD.iEntityCreateAreaId = -1
				#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[GET_KILL_TARGET_START_POSITION] [NETWORK_ADD_ENTITY_AREA] adding area to check, candidate...", iCandidate) #ENDIF
				serverBD.iEntityCreateAreaId = NETWORK_ADD_ENTITY_AREA(vPos1, vPos2)
			ELSE
				IF NETWORK_ENTITY_AREA_DOES_EXIST(serverBD.iEntityCreateAreaId)
					IF NETWORK_ENTITY_AREA_HAVE_ALL_REPLIED(serverBD.iEntityCreateAreaId)
						IF NOT NETWORK_ENTITY_AREA_IS_OCCUPIED(serverBD.iEntityCreateAreaId)
							IF NOT CAN_ANY_PLAYER_SEE_POINT(vCreate)
								#IF IS_DEBUG_BUILD NET_DW_PRINT("[GET_KILL_TARGET_START_POSITION]... got start pos, Calling NETWORK_REMOVE_ENTITY_AREA ") #ENDIF
								vPos = vCreate
								fHead = fCreateHead
								NETWORK_REMOVE_ENTITY_AREA(serverBD.iEntityCreateAreaId)
								RETURN TRUE
							ELSE
								#IF IS_DEBUG_BUILD NET_DW_PRINT("[GET_KILL_TARGET_START_POSITION] [NETWORK_REMOVE_ENTITY_AREA]... area is clear, but point on screeb ") #ENDIF
								iCandidate++
								NETWORK_REMOVE_ENTITY_AREA(serverBD.iEntityCreateAreaId)
								serverBD.iEntityCreateAreaId = -1
							ENDIF
						ELSE
							#IF IS_DEBUG_BUILD NET_DW_PRINT("[GET_KILL_TARGET_START_POSITION] [NETWORK_REMOVE_ENTITY_AREA] ... area is occupied ") #ENDIF
							iCandidate++
							NETWORK_REMOVE_ENTITY_AREA(serverBD.iEntityCreateAreaId)
							serverBD.iEntityCreateAreaId = -1
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD NET_DW_PRINT("[GET_KILL_TARGET_START_POSITION]... not everyone has replied ") #ENDIF
					ENDIF
				ELSE
				//	#IF IS_DEBUG_BUILD NET_DW_PRINT("GET_HIGH_PRIORITY_VEHICLE_START_POSITION... area doesn't exist ") #ENDIF
				ENDIF
			ENDIF
		ELSE	
		//	#IF IS_DEBUG_BUILD NET_DW_PRINT("GET_HIGH_PRIORITY_VEHICLE_START_POSITION... iCandidate >= MAX_HIGH_PRIORITY_VEHICLE_START_POSITIONS! ") #ENDIF
			iCandidate++
		ENDIF
	ELSE	
		iCandidate = 0
	ENDIF
	
	RETURN FALSE
ENDFUNC
FUNC MODEL_NAMES GET_KILL_TARGET_PED_MODEL()
	RETURN A_M_Y_BUSINESS_03
ENDFUNC

FUNC MODEL_NAMES GET_KILL_TARGET_VEHICLE_MODEL()
	RETURN CAVALCADE
ENDFUNC


FUNC BOOL CREATE_KILL_TARGET_ASSETS()
	MODEL_NAMES mTargetPed = GET_KILL_TARGET_PED_MODEL()
	MODEL_NAMES mTargetVeh = GET_KILL_TARGET_VEHICLE_MODEL()
	VECTOR vCreate
	FLOAT fHead
	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[0])
		IF serverBD.iNumReservedVeh = 0
			RESERVE_NETWORK_MISSION_VEHICLES(1)
			serverBD.iNumReservedVeh = 1
		ENDIF
		
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_CanReserveAssets)
			IF CAN_REGISTER_MISSION_VEHICLES(1)
				IF CAN_REGISTER_MISSION_PEDS(1)
					IF CAN_RESERVE_NETWORK_VEHICLES_FOR_THIS_SCRIPT(1, TRUE, TRUE)
						IF CAN_RESERVE_NETWORK_PEDS_FOR_THIS_SCRIPT(1, TRUE, TRUE)
							SET_BIT(serverBD.iServerBitSet, biS_CanReserveAssets)
							#IF IS_DEBUG_BUILD NET_DW_PRINT("[CREATE_KILL_TARGET_ASSETS] Set biS_CanReserveAssets") #ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		IF IS_BIT_SET(serverBD.iServerBitSet, biS_CanReserveAssets)
			IF REQUEST_LOAD_MODEL(mTargetVeh)
				IF REQUEST_LOAD_MODEL(mTargetPed)
					IF GET_KILL_TARGET_START_POSITION(vCreate, fHead)
						IF CREATE_NET_VEHICLE(serverBD.niVeh[0], mTargetVeh,  vCreate, fHead)
							#IF IS_DEBUG_BUILD NET_DW_PRINT("[CREATE_KILL_TARGET_ASSETS] Created target vehicle") #ENDIF
							SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(NET_TO_VEH(serverBD.niVeh[0]), TRUE)
						//	SET_ENTITY_CAN_ONLY_BE_DAMAGED_BY_SCRIPT_PARTICIPANTS(NET_TO_ENT(serverBD.niVeh[0]), TRUE)
							IF DECOR_IS_REGISTERED_AS_TYPE("Not_Allow_As_Saved_Veh",DECOR_TYPE_INT)
								DECOR_SET_INT(NET_TO_VEH(serverBD.niVeh[0]),"Not_Allow_As_Saved_Veh",MP_NOT_ALLOWED_AS_SAVED_VEHICLE_DEC_SIMEON)
							ENDIF
							serverBD.iNumReservedVeh = 1
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[0])
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPed[0])
			IF serverBD.iNumReservedPed = 0
				RESERVE_NETWORK_MISSION_PEDS(1)
				serverBD.iNumReservedPed = 1
			ENDIF
			IF CAN_REGISTER_MISSION_PEDS(1)
				IF REQUEST_LOAD_MODEL(mTargetPed)
					IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niVeh[0])
						IF NETWORK_HAS_CONTROL_OF_NETWORK_ID( serverBD.niVeh[0])
							IF CREATE_NET_PED_IN_VEHICLE(serverBD.niPed[0], serverBD.niVeh[0], PEDTYPE_MISSION, mTargetPed, VS_DRIVER)
								#IF IS_DEBUG_BUILD NET_DW_PRINT("[CREATE_KILL_TARGET_ASSETS] Created target ped") #ENDIF
								SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(NET_TO_PED(serverBD.niPed[0]), TRUE)
							//	SET_ENTITY_CAN_ONLY_BE_DAMAGED_BY_SCRIPT_PARTICIPANTS(NET_TO_ENT(serverBD.niPed[0]), TRUE)
								SET_ENTITY_IS_TARGET_PRIORITY(NET_TO_PED(serverBD.niPed[0]), TRUE)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NET_TO_PED(serverBD.niPed[0]), TRUE)
								SET_PED_RELATIONSHIP_GROUP_HASH(NET_TO_PED(serverBD.niPed[0]),rgFM_AiHate)
								SET_ENTITY_IS_TARGET_PRIORITY(NET_TO_PED(serverBD.niPed[0]), TRUE)
								SET_ENTITY_HEALTH(NET_TO_PED(serverBD.niPed[0]), ROUND(200*g_sMPTunables.fAiHealthModifier))

								serverBD.iNumReservedPed = 1
							ENDIF
						ELSE
							#IF IS_DEBUG_BUILD NET_DW_PRINT("[CREATE_KILL_TARGET_ASSETS] Can't create mission ped as don't have control of vehicle!. Please tell Dave W if spamming") #ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE	
				#IF IS_DEBUG_BUILD NET_DW_PRINT("[CREATE_KILL_TARGET_ASSETS] Can't register mission ped! Please tell Dave W if spamming") #ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPed[0])
	OR NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[0])
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC
FUNC BOOL CREATE_ASSETS_FOR_MISSION_TYPE(INT iType)
	SWITCH iType
		CASE AM_MISSION_KILL_TARGET_IN_VEH
			IF CREATE_KILL_TARGET_ASSETS()
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC



/// PURPOSE:
///    Want to get a list of possible spawn locations for the target. There's a total of MAX_SPAWN_POS possible locations
///    Any location where all players are > MIN_DISTANCE_FOR_SPAWN are valid
///    Valid locations are stored in a bitset, serverBD.iCandidateSpawnBitset, iCandidateCount
/// RETURNS:
///    
FUNC BOOL GET_ENTITY_CREATION_LOCATION_CANDIDATES_FOR_TARGET()
	PLAYER_INDEX player
	PED_INDEX pedPlayer
	VECTOR vPos, vPlayerPos
	FLOAT fDistance
	INT i
	//-- Loop through each candidate pos
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_FindSpawnPosForKillTarget)
		IF iFindCreationPosStaggeredPlayerCount >= COUNT_OF(PlayerBD) 
			IF iCandidateCount < MAX_SPAWN_POS 
				iCandidateCount++
			ENDIF
			iFindCreationPosStaggeredPlayerCount = 0
			
		//	#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[GET_ENTITY_CREATION_LOCATION_CANDIDATES_FOR_TARGET] Checking candidate... ", iCandidateCount) #ENDIF
		ENDIF
		
		IF iFindCreationPosStaggeredPlayerCount = 0
			SET_BIT(iBoolsBitSet, biL_CurrentCandidateValid)
			#IF IS_DEBUG_BUILD 
				NET_DW_PRINT_STRING_INT("[GET_ENTITY_CREATION_LOCATION_CANDIDATES_FOR_TARGET] Checking candidate... ", iCandidateCount) 
			#ENDIF
		ENDIF
		
		IF iCandidateCount < MAX_SPAWN_POS
			i = iFindCreationPosStaggeredPlayerCount
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
				player = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
				IF IS_NET_PLAYER_OK(player)
					pedPlayer = GET_PLAYER_PED(player)
					IF IS_BIT_SET(iBoolsBitSet, biL_CurrentCandidateValid)
						vPos = GET_START_COORD_FOR_MISSION_TYPE(serverBD.iMissionType, iCandidateCount)
						vPlayerPos = GET_ENTITY_COORDS(pedPlayer, FALSE)
						fDistance =  GET_DISTANCE_BETWEEN_COORDS(vPos, vPlayerPos) 
						IF fDistance < MIN_DISTANCE_FOR_SPAWN
							CLEAR_BIT(iBoolsBitSet, biL_CurrentCandidateValid)
							#IF IS_DEBUG_BUILD 
								NET_DW_PRINT_STRING_INT("[GET_ENTITY_CREATION_LOCATION_CANDIDATES_FOR_TARGET] rejecting candidate ", iCandidateCount)
								NET_DW_PRINT_STRING_WITH_PLAYER_NAME("[GET_ENTITY_CREATION_LOCATION_CANDIDATES_FOR_TARGET] because player ", player)
								NET_DW_PRINT_STRING_FLOAT("[GET_ENTITY_CREATION_LOCATION_CANDIDATES_FOR_TARGET] is this far away ", fDistance)
							#ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			iFindCreationPosStaggeredPlayerCount++
			
			IF iFindCreationPosStaggeredPlayerCount = COUNT_OF(PlayerBD) 
				IF IS_BIT_SET(iBoolsBitSet, biL_CurrentCandidateValid)
					SET_BIT(serverBD.iCandidateSpawnBitset, iCandidateCount)
					#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[GET_ENTITY_CREATION_LOCATION_CANDIDATES_FOR_TARGET] Think this candidate is valid ", iCandidateCount)
					#ENDIF
				ENDIF
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
		
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PROCESS_ENTITY_CREATION_SERVER()
	IF serverBD.iMissionType = AM_MISSION_KILL_TARGET_IN_VEH
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_FindSpawnPosForKillTarget)
			#IF IS_DEBUG_BUILD NET_DW_PRINT("[PROCESS_ENTITY_CREATION_SERVER] Setting biS_FindSpawnPosForKillTarget") #ENDIF
			iCandidateCount = 0
			iCandidate = GET_RANDOM_INT_IN_RANGE(0, MAX_SPAWN_POS)
			#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[PROCESS_ENTITY_CREATION_SERVER] iCandidate initial value = ", iCandidate) #ENDIF
			iFindCreationPosStaggeredPlayerCount = 0
			serverBD.iEntityCreateAreaId = -1
			serverBD.iCandidateSpawnBitset = 0
			SET_BIT(serverBD.iServerBitSet, biS_FindSpawnPosForKillTarget)
		ENDIF
		
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_GotSpawnCandidates)
			IF GET_ENTITY_CREATION_LOCATION_CANDIDATES_FOR_TARGET()
				SET_BIT(serverBD.iServerBitSet, biS_GotSpawnCandidates)
				#IF IS_DEBUG_BUILD NET_DW_PRINT("[PROCESS_ENTITY_CREATION_SERVER] Setting biS_GotSpawnCandidates") #ENDIF
			ENDIF
		ELSE
			IF CREATE_KILL_TARGET_ASSETS()
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
		
	RETURN FALSE
ENDFUNC	

PROC PROCESS_TARGET_PED_BRAIN()
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		SWITCH serverBD.iTargetPedState
			CASE TARGET_PED_CREATED
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPed[0])
				AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[0])
					serverBD.iTargetPedState = TARGET_PED_TOUR_IN_CAR
					#IF IS_DEBUG_BUILD NET_DW_PRINT("[PROCESS_TARGET_PED_BRAIN] TARGET_PED_CREATED -> TARGET_PED_TOUR_IN_CAR") #ENDIF
				ENDIF
			BREAK
			
			CASE TARGET_PED_TOUR_IN_CAR
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPed[0])
					IF NOT IS_NET_PED_INJURED(serverBD.niPed[0])
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[0])
							IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niVeh[0])
								IF IS_PED_SITTING_IN_VEHICLE(NET_TO_PED(serverBD.niPed[0]), NET_TO_VEH(serverBD.niVeh[0]))
									IF IS_BIT_SET(serverBD.iServerBitSet, biS_SomeoneDamagedVeh1)
										SET_BIT(serverBD.iServerBitSet, biS_GiveDriveFastWanderTask)
										serverBD.iTargetPedState = TARGET_PED_FLEE_IN_CAR
										#IF IS_DEBUG_BUILD NET_DW_PRINT("[PROCESS_TARGET_PED_BRAIN] TARGET_PED_TOUR_IN_CAR -> TARGET_PED_FLEE_IN_CAR as biS_SomeoneDamagedVeh1") #ENDIF
									ELIF IS_BIT_SET(serverBD.iServerBitSet, biS_SomeoneAimingAtTarget) 
										SET_BIT(serverBD.iServerBitSet, biS_GiveDriveFastWanderTask)
										serverBD.iTargetPedState = TARGET_PED_FLEE_IN_CAR
										#IF IS_DEBUG_BUILD NET_DW_PRINT("[PROCESS_TARGET_PED_BRAIN] TARGET_PED_TOUR_IN_CAR -> TARGET_PED_FLEE_IN_CAR as biS_SomeoneAimingAtTarget") #ENDIF								
									ELIF IS_BIT_SET(serverBD.iServerBitSet, biS_StuckOnRoofOrSide) 
										serverBD.iTargetPedState = TARGET_PED_LEAVE_CAR
										#IF IS_DEBUG_BUILD NET_DW_PRINT("[PROCESS_TARGET_PED_BRAIN] TARGET_PED_TOUR_IN_CAR -> TARGET_PED_LEAVE_CAR as car stuck") #ENDIF
									ENDIF
								ELSE	
									serverBD.iTargetPedState = TARGET_PED_FLEE_ON_FOOT
									#IF IS_DEBUG_BUILD NET_DW_PRINT("[PROCESS_TARGET_PED_BRAIN] TARGET_PED_TOUR_IN_CAR -> TARGET_PED_FLEE_ON_FOOT 1") #ENDIF
								ENDIF
							ELSE	
								serverBD.iTargetPedState = TARGET_PED_LEAVE_CAR
								#IF IS_DEBUG_BUILD NET_DW_PRINT("[PROCESS_TARGET_PED_BRAIN] TARGET_PED_TOUR_IN_CAR -> TARGET_PED_LEAVE_CAR 1") #ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
						
			BREAK
			
			CASE TARGET_PED_FLEE_IN_CAR
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPed[0])
					IF NOT IS_NET_PED_INJURED(serverBD.niPed[0])
						IF NOT IS_PED_SITTING_IN_ANY_VEHICLE(NET_TO_PED(serverBD.niPed[0]))
							serverBD.iTargetPedState = TARGET_PED_FLEE_ON_FOOT
							#IF IS_DEBUG_BUILD NET_DW_PRINT("[PROCESS_TARGET_PED_BRAIN] TARGET_PED_FLEE_IN_CAR -> TARGET_PED_FLEE_ON_FOOT 1") #ENDIF
						ELSE	
							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[0])
								IF NOT IS_NET_VEHICLE_DRIVEABLE(serverBD.niVeh[0])
									serverBD.iTargetPedState = TARGET_PED_LEAVE_CAR
									#IF IS_DEBUG_BUILD NET_DW_PRINT("[PROCESS_TARGET_PED_BRAIN] TARGET_PED_FLEE_IN_CAR -> TARGET_PED_LEAVE_CAR 1") #ENDIF	
								ELIF IS_BIT_SET(serverBD.iServerBitSet, biS_StuckOnRoofOrSide) 
									serverBD.iTargetPedState = TARGET_PED_LEAVE_CAR
									#IF IS_DEBUG_BUILD NET_DW_PRINT("[PROCESS_TARGET_PED_BRAIN] TARGET_PED_FLEE_IN_CAR -> TARGET_PED_LEAVE_CAR as car stuck") #ENDIF							
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE TARGET_PED_LEAVE_CAR
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPed[0])
					IF NOT IS_NET_PED_INJURED(serverBD.niPed[0])
						IF NOT IS_PED_IN_ANY_VEHICLE(NET_TO_PED(serverBD.niPed[0]))
							serverBD.iTargetPedState = TARGET_PED_FLEE_ON_FOOT
							#IF IS_DEBUG_BUILD NET_DW_PRINT("[PROCESS_TARGET_PED_BRAIN] TARGET_PED_LEAVE_CAR -> TARGET_PED_FLEE_ON_FOOT 1") #ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE TARGET_PED_FLEE_ON_FOOT
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC


PROC PROCESS_TARGET_PED_BODY()
	SWITCH serverBD.iTargetPedState
		CASE TARGET_PED_TOUR_IN_CAR
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[0])
					IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niVeh[0])
						IF NOT IS_NET_PED_INJURED(serverBD.niPed[0])
							IF IS_PED_SITTING_IN_VEHICLE(NET_TO_PED(serverBD.niPed[0]), NET_TO_VEH(serverBD.niVeh[0]))
								IF (GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.niPed[0]), SCRIPT_TASK_VEHICLE_DRIVE_WANDER) != PERFORMING_TASK
								AND GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.niPed[0]), SCRIPT_TASK_VEHICLE_DRIVE_WANDER) != WAITING_TO_START_TASK)
									TASK_VEHICLE_DRIVE_WANDER(NET_TO_PED(serverBD.niPed[0]), NET_TO_VEH(serverBD.niVeh[0]), 20.0, DRIVINGMODE_STOPFORCARS)
									#IF IS_DEBUG_BUILD 
										NET_DW_PRINT("[PROCESS_TARGET_PED_BODY] I've given ped TASK_VEHICLE_DRIVE_WANDER") 
										NET_DW_PRINT_HOST_NAME()
									#ENDIF 
								
								ENDIF
								
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE TARGET_PED_FLEE_IN_CAR
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh[0])
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPed[0])
						IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niVeh[0])
							IF NOT IS_NET_PED_INJURED(serverBD.niPed[0])
								IF IS_PED_SITTING_IN_VEHICLE(NET_TO_PED(serverBD.niPed[0]), NET_TO_VEH(serverBD.niVeh[0]))
									IF (GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.niPed[0]), SCRIPT_TASK_VEHICLE_DRIVE_WANDER) != PERFORMING_TASK
									AND GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.niPed[0]), SCRIPT_TASK_VEHICLE_DRIVE_WANDER) != WAITING_TO_START_TASK)
									OR IS_BIT_SET(serverBD.iServerBitSet, biS_GiveDriveFastWanderTask)
										TASK_VEHICLE_DRIVE_WANDER(NET_TO_PED(serverBD.niPed[0]), NET_TO_VEH(serverBD.niVeh[0]), 30.0, DRIVINGMODE_AVOIDCARS)
										IF IS_BIT_SET(serverBD.iServerBitSet, biS_GiveDriveFastWanderTask)
											CLEAR_BIT(serverBD.iServerBitSet, biS_GiveDriveFastWanderTask)
										ENDIF
										#IF IS_DEBUG_BUILD 
											NET_DW_PRINT("[PROCESS_TARGET_PED_BODY] I've given ped TASK_VEHICLE_DRIVE_WANDER FAST") 
											NET_DW_PRINT_HOST_NAME()
										#ENDIF
									ENDIF
									
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE TARGET_PED_LEAVE_CAR
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPed[0])
				IF NOT IS_NET_PED_INJURED(serverbd.niPed[0])
					IF GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.niPed[0]), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != PERFORMING_TASK
					AND GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.niPed[0]), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != WAITING_TO_START_TASK
						IF MAINTAIN_OWNERSHIP_OF_NETWORK_ID(serverBD.niPed[0])
							IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niPed[0])
								TASK_LEAVE_ANY_VEHICLE(NET_TO_PED(serverBD.niPed[0]))
								#IF IS_DEBUG_BUILD 
									NET_DW_PRINT("[PROCESS_TARGET_PED_BODY] I've given ped TASK_LEAVE_ANY_VEHICLE ") 
									NET_DW_PRINT_HOST_NAME()
								#ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE TARGET_PED_FLEE_ON_FOOT
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPed[0])
				IF NOT IS_NET_PED_INJURED(serverbd.niPed[0])
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						IF GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.niPed[0]), SCRIPT_TASK_SMART_FLEE_PED) != PERFORMING_TASK
						AND GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.niPed[0]), SCRIPT_TASK_SMART_FLEE_PED) != WAITING_TO_START_TASK
											
							IF MAINTAIN_OWNERSHIP_OF_NETWORK_ID(serverBD.niPed[0])
								IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niPed[0])
									TASK_SMART_FLEE_PED(NET_TO_PED(serverBD.niPed[0]), PLAYER_PED_ID(), 500, -1, FALSE, TRUE)
									#IF IS_DEBUG_BUILD 
										NET_DW_PRINT("[PROCESS_TARGET_PED_BODY] I've given ped TASK_SMART_FLEE_PED ") 
										NET_DW_PRINT_HOST_NAME()
									#ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	
	#IF IS_DEBUG_BUILD
		IF bWdBlipDebug
			NET_DW_PRINT("[bWdBlipDebug] About to process blip...")	
		ENDIF
	#ENDIF
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niPed[0])
	//	IF DOES_BLIP_EXIST(blipTarget.blipId)
		IF  PlayerBD[PARTICIPANT_ID_TO_INT()].iMissionProg >= MISSION_COMPLETE_OBJECTIVE 
		AND PlayerBD[PARTICIPANT_ID_TO_INT()].iMissionProg < MISSION_CLEANUP
		AND NOT IS_PLAYER_IN_MP_PROPERTY(PLAYER_ID(), FALSE)
			#IF IS_DEBUG_BUILD
				IF bWdBlipDebug
					NET_DW_PRINT("[bWdBlipDebug] Updating blip!")	
				ENDIF
			#ENDIF
	
			//-- Force blip on is broke!
			UPDATE_ENEMY_NET_PED_BLIP( serverBD.niPed[0], blipTarget, DEFAULT, DEFAULT, TRUE ) 
			
			
		ELSE
			#IF IS_DEBUG_BUILD
				IF bWdBlipDebug
					NET_DW_PRINT("[bWdBlipDebug] Cleaning up blip!!")	
				ENDIF
			#ENDIF
			
		//	IF DOES_BLIP_EXIST(blipTarget.blipId)
				CLEANUP_AI_PED_BLIP(blipTarget)
		//	ENDIF

		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
			IF bWdBlipDebug
				NET_DW_PRINT("[bWdBlipDebug] Network index doesn't exist!.")	
			ENDIF
		#ENDIF
	ENDIF
ENDPROC

PROC CLEANUP_ENTITIES_FOR_MISSION_TYPE(INT iType)
	SWITCH iType
		CASE AM_MISSION_KILL_TARGET_IN_VEH
			CLEANUP_NET_ID(serverBD.niPed[0])
			CLEANUP_NET_ID(serverBD.niVeh[0])
		BREAK
	ENDSWITCH
ENDPROC
PROC PROCESS_MISSION_SERVER()
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		SWITCH serverBD.iServerMissionProg
			CASE MISSION_WAIT_FOR_LAUNCH
				IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_LaunchTimeOut)
					IF HAS_NET_TIMER_EXPIRED(serverBD.timeLaunchTimeout, TIME_LAUNCH_TIMEOUT)
						SET_BIT(serverBD.iServerBitSet, biS_LaunchTimeOut)
						#IF IS_DEBUG_BUILD NET_DW_PRINT("[PROCESS_MISSION_SERVER] Setting biS_LaunchTimeOut as timeout expired")
						#ENDIF
					ENDIF
				ENDIF
				
				IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_LaunchTimeOut)
					IF IS_BIT_SET(serverBD.iServerBitSet, biS_EnoughPlayersReadyForLaunch)
						serverBD.iMissionType = GET_NEXT_AMBIENT_MISSION_TYPE()
						serverBD.iServerMissionProg = MISSION_CREATE_ASSETS
						serverBD.iCandidateSpawnBitset = 0
						#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("serverBD.iServerMissionProg MISSION_WAIT_FOR_LAUNCH -> MISSION_CREATE_ASSETS with iMissionType = ",serverBD.iMissionType)
						#ENDIF
					ENDIF	
				ENDIF
			BREAK
			
			CASE MISSION_CREATE_ASSETS
				IF PROCESS_ENTITY_CREATION_SERVER()
					SET_BIT(serverBD.iServerBitSet, biS_MissionLaunched)
					CLEAR_BIT(serverBD.iServerBitSet, biS_CanReserveAssets)
					START_NET_TIMER(serverBD.timeNearTarget)
					serverBD.iServerMissionProg = MISSION_CHECK_FOR_WIN
					#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("serverBD.iServerMissionProg MISSION_CREATE_ASSETS -> MISSION_CHECK_FOR_WIN with iMissionType = ",serverBD.iMissionType)
					#ENDIF
				ENDIF
			BREAK
			
			CASE MISSION_CHECK_FOR_WIN
				IF IS_BIT_SET(serverBD.iServerBitSet, biS_SomeoneCompleted)
					CLEANUP_ENTITIES_FOR_MISSION_TYPE(serverBD.iMissionType)
					serverBD.iServerMissionProg = MISSION_RESET
					#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("serverBD.iServerMissionProg MISSION_CHECK_FOR_WIN -> MISSION_RESET with iMissionType = ",serverBD.iMissionType)
					#ENDIF
				ENDIF
			BREAK
			
			CASE MISSION_RESET
				CLEANUP_ENTITIES_FOR_MISSION_TYPE(serverBD.iMissionType)
			/*	CLEAR_BIT(serverBD.iServerBitSet, biS_EnoughPlayersReadyForLaunch)
				CLEAR_BIT(serverBD.iServerBitSet, biS_FindSpawnPosForKillTarget)
				CLEAR_BIT(serverBD.iServerBitSet, biS_GotSpawnCandidates)
				CLEAR_BIT(serverBD.iServerBitSet, biS_MissionLaunched)
				CLEAR_BIT(serverBD.iServerBitSet, biS_SomeoneCompleted)
				CLEAR_BIT(serverBD.iServerBitSet, biS_ServerFoundSomeoneStillRunning)
				serverBD.iServerMissionProg = MISSION_WAIT_FOR_LAUNCH
				#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("serverBD.iServerMissionProg MISSION_RESET -> MISSION_WAIT_FOR_LAUNCH with iMissionType = ",serverBD.iMissionType)
				#ENDIF */
			BREAK
			
		ENDSWITCH
	ENDIF
ENDPROC	

PROC CLEANUP_SCRIPT()
	#IF IS_DEBUG_BUILD
		NET_DW_PRINT("Running cleanup")
		g_bLaunchAmbMiss = FALSE
	#ENDIF
	Clear_Any_Objective_Text_From_This_Script()
	SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_GAMEDIR_MISSION, FALSE)
	
	#IF IS_DEBUG_BUILD
		NET_DW_PRINT("Running cleanup - Canceling invite")
	#ENDIF
	Cancel_Basic_Invite_From_NPC(CHAR_LESTER, FMMC_TYPE_LESTER_KILL_TARGET)
	
	CLEAR_BIT(mpglobals.gameDirGlobalData.iGlobalDataBitset, biGameDir_PlayerOnGameDirMission)
	CLEAR_BIT(mpglobals.gameDirGlobalData.iGlobalDataBitset, biGameDir_DamagedtargetVehicle1)
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF PARTICIPANT_ID_TO_INT() > -1
			PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitset = 0
		ENDIF
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()	
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			IF NETWORK_ENTITY_AREA_DOES_EXIST(serverBD.iEntityCreateAreaId)
				NETWORK_REMOVE_ENTITY_AREA(serverBD.iEntityCreateAreaId)
				#IF IS_DEBUG_BUILD NET_DW_PRINT("Cleanup calling NETWORK_REMOVE_ENTITY_AREA") #ENDIF
			ENDIF
			serverBD.iEntityCreateAreaId = -1
		ENDIF
	ENDIF
	
	IF DOES_BLIP_EXIST(g_blipLesterTarget)
		REMOVE_BLIP(g_blipLesterTarget)
		#IF IS_DEBUG_BUILD NET_DW_PRINT("Cleanup removing g_blipLesterTarget") #ENDIF
	ENDIF
	CLEAR_BIT(iBoolsBitSet, biL_DOnePlayerWonTick)
	CLEAR_BIT(iBoolsBitSet, biL_DoneIntroPhonecall)
	iIntroPhoneCount = 0
	IF DOES_BLIP_EXIST(blipTarget.blipId)
		REMOVE_BLIP(blipTarget.blipId)
	ENDIF
	//TERMINATE_THIS_THREAD()
	//RESET_FMMC_MISSION_VARIABLES(FALSE, FALSE, ciFMMC_END_OF_MISSION_STATUS_PASSED)
	#IF IS_DEBUG_BUILD
		NET_DW_PRINT("Clear biGameDir_PlayerOnGameDirMission")
	#ENDIF

	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

/// PURPOSE:
///    Do necessary pre game start ini.
PROC PROCESS_PRE_GAME(MP_MISSION_DATA missionScriptArgs)
	// This marks the script as a net script, and handles any instancing setup. 
	SETUP_MP_MISSION_FOR_NETWORK(GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(missionScriptArgs.mdID.idMission) , missionScriptArgs)
	
	// This makes sure the net script is active, waits until it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
	
	// This script will not be paused if another script calls PAUSE_GAME
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	
//	RESERVE_NETWORK_MISSION_PEDS(MAX_AM_PEDS)
//	RESERVE_NETWORK_MISSION_VEHICLES(MAX_AM_VEH)
	
	// KGM: Wait for the first network broadcast before moving on - call this as the last instruction in pre-game
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		#IF IS_DEBUG_BUILD
			NET_DW_PRINT("Failed to receive initial network broadcast. Cleaning up.")
		#ENDIF
		CLEANUP_SCRIPT()
	ENDIF
	
	SET_BIT(mpglobals.gameDirGlobalData.iGlobalDataBitset, biGameDir_PlayerOnGameDirMission)
	CLEAR_BIT(mpglobals.gameDirGlobalData.iGlobalDataBitset, biGameDir_DamagedtargetVehicle1)

	#IF IS_DEBUG_BUILD
		NET_DW_PRINT("Set biGameDir_PlayerOnGameDirMission")
		NET_DW_PRINT("Launching....V1")
	#ENDIF
ENDPROC

FUNC BOOL ARE_ALL_PLAYERS_FINISHED()
	
	RETURN IS_BIT_SET(serverBD.iServerBitSet, biS_AllPlayersFinished)
ENDFUNC

FUNC BOOL HAVE_MISSION_END_CONDITIONS_BEEN_MET()
	IF ARE_ALL_PLAYERS_FINISHED()
		#IF IS_DEBUG_BUILD NET_DW_PRINT("[HAVE_MISSION_END_CONDITIONS_BEEN_MET] True becasue ARE_ALL_PLAYERS_FINISHED") #ENDIF
		RETURN TRUE	
	ENDIF
	RETURN FALSE
ENDFUNC
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                      Main Loop                          ///////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

SCRIPT(MP_MISSION_DATA missionScriptArgs)
	
	
	PROCESS_PRE_GAME(missionScriptArgs)	
	
	#IF IS_DEBUG_BUILD
		CREATE_WIDGETS()	
	#ENDIF
	
	
	
	WHILE TRUE
	
		MP_LOOP_WAIT_ZERO()
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
			SCRIPT_PROFILER_START_OF_FRAME()
		#ENDIF
		#ENDIF		
		
		

		// If we have a match end event, bail.
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		OR NOT IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE)
			CLEANUP_SCRIPT()
		ENDIF	
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER( "Cleanup checks")
		#ENDIF
		#ENDIF 
		
		#IF IS_DEBUG_BUILD
			UPDATE_WIDGETS()
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER( "UPDATE_WIDGETS")
			#ENDIF
			
			DO_DEBUG_KEYS()
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER( "DO_DEBUG_KEYS")
			#ENDIF
		#ENDIF
		
		
// *****************************************************************************************************************************************************************	
// *****************************************************************************************************************************************************************	
//												LOCAL PROCESSING
// *****************************************************************************************************************************************************************	
// *****************************************************************************************************************************************************************
		
		
		// -----------------------------------
		// Process your game logic.....
		SWITCH GET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT())
			
			// Wait until the server gives the all go before moving on
			CASE GAME_STATE_INI
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_RUNNING
					#IF IS_DEBUG_BUILD NET_DW_PRINT("playerBD[PARTICIPANT_ID_TO_INT()].iGameState GAME_STATE_INI >  GAME_STATE_RUNNING") #ENDIF
					
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY
					
					#IF IS_DEBUG_BUILD NET_DW_PRINT("playerBD[PARTICIPANT_ID_TO_INT()].iGameState GAME_STATE_INI >  GAME_STATE_TERMINATE_DELAY") #ENDIF
					
				ENDIF
			BREAK
			
			// Main gameplay state.
			CASE GAME_STATE_RUNNING
				#IF IS_DEBUG_BUILD
					IF bWdBlipDebug
						NET_DW_PRINT("[bWdBlipDebug] bWdBlipDebug is set")
					ENDIF
				#ENDIF
				
				
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING	
					PROCESS_TARGET_PED_BODY()
					PROCESS_MISSION_CLIENT()
				//	IF NOT HAS_FM_CAR_MOD_TUT_BEEN_DONE()
				//		PROCESS_CAR_MOD_TUTORIAL_CLIENT()
				//	ENDIF
				
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY
					#IF IS_DEBUG_BUILD NET_DW_PRINT("playerBD[PARTICIPANT_ID_TO_INT()].iGameState GAME_STATE_RUNNING >  GAME_STATE_TERMINATE_DELAY") #ENDIF
				ENDIF
				
				
				//-- Leave if we start a mission
				IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
				OR IS_PLAYER_ON_ANY_OTHER_AMBIENT_SCRIPT(PLAYER_ID())
				OR IS_BIT_SET(iBoolsBitSet, biL_PhonecallFailed)
				OR IS_BIT_SET(iBoolsBitSet, biL_JoinedTooLate)
			//	OR GET_LOCAL_PLAYER_CORONA_POS_STATE() > 0
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
					#IF IS_DEBUG_BUILD 
						IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
						OR IS_PLAYER_ON_ANY_OTHER_AMBIENT_SCRIPT(PLAYER_ID())
							NET_DW_PRINT("playerBD[PARTICIPANT_ID_TO_INT()].iGameState GAME_STATE_RUNNING > GAME_STATE_END as on mission ") 
						ELIF IS_BIT_SET(iBoolsBitSet, biL_PhonecallFailed)
							NET_DW_PRINT("playerBD[PARTICIPANT_ID_TO_INT()].iGameState GAME_STATE_RUNNING > GAME_STATE_END as biL_PhonecallFailed ") 
						ELIF IS_BIT_SET(iBoolsBitSet, biL_JoinedTooLate)
							NET_DW_PRINT("playerBD[PARTICIPANT_ID_TO_INT()].iGameState GAME_STATE_RUNNING > GAME_STATE_END as biL_JoinedTooLate ")
						ENDIF
						
						
					#ENDIF
					
					
				ENDIF
								
			BREAK
			
			CASE GAME_STATE_TERMINATE_DELAY 
				SET_MISSION_FINISHED(serverBD.timeTerminate)
		      	IF IS_MISSION_READY_TO_CLEANUP(serverBD.timeTerminate)
		            playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
					#IF IS_DEBUG_BUILD NET_DW_PRINT("playerBD[PARTICIPANT_ID_TO_INT()].iGameState GAME_STATE_TERMINATE_DELAY >  GAME_STATE_END") #ENDIF
		      	ENDIF
			BREAK
			
			// The game stage the local player is placed when we want him to leave a mission
			CASE GAME_STATE_LEAVE

				playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
				#IF IS_DEBUG_BUILD NET_DW_PRINT("playerBD[PARTICIPANT_ID_TO_INT()].iGameState GAME_STATE_LEAVE >  GAME_STATE_END") #ENDIF

			FALLTHRU
			
			//Cleans up then terminates the mission
			CASE GAME_STATE_END
				#IF IS_DEBUG_BUILD NET_DW_PRINT("About to run cleanup from GAME_STATE_END") #ENDIF
				CLEANUP_SCRIPT()
			BREAK

		ENDSWITCH
		
	 	

		
// *****************************************************************************************************************************************************************	
// *****************************************************************************************************************************************************************	
//												SERVER PROCESSING
// *****************************************************************************************************************************************************************	
// *****************************************************************************************************************************************************************	
		
		
		
		// -----------------------------------
		// Process server game logic		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			
			SWITCH GET_SERVER_MISSION_STATE()
				
				// For CnC group missions, we may want to wait for all players to have launched the script
				CASE GAME_STATE_INI
					START_NET_TIMER(serverBD.timeLaunchTimeout)
					serverBD.iServerGameState = GAME_STATE_RUNNING
					#IF IS_DEBUG_BUILD NET_DW_PRINT("serverBD.iServerGameState = GAME_STATE_RUNNING") #ENDIF
						
				BREAK
				
				// Look for game end conditions
				CASE GAME_STATE_RUNNING
				//	MAINTAIN_SERVER_STAGGERED_LOOP()
				//	MAINTAIN_CAR_MOD_TUTORIAL_SERVER()
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE
						ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_HOLD_UP_TUTORIAL_SERVER")
					#ENDIF
					#ENDIF		
					PROCESS_SERVER_STAGGERED_LOOP()
					
					PROCESS_TARGET_PED_BRAIN()
					
					PROCESS_MISSION_SERVER()
					IF HAVE_MISSION_END_CONDITIONS_BEEN_MET()
						serverBD.iServerGameState = GAME_STATE_END
						#IF IS_DEBUG_BUILD NET_DW_PRINT ("serverBD.iServerGameState GAME_STATE_RUNNING > GAME_STATE_END ")#ENDIF
						
					ENDIF
					
				BREAK
				
				CASE GAME_STATE_END
				BREAK
				
			ENDSWITCH
			
		ENDIF
		
		
		
		#IF IS_DEBUG_BUILD 
			#IF SCRIPT_PROFILER_ACTIVE
				SCRIPT_PROFILER_END_OF_FRAME()
			#ENDIF
		#ENDIF			
	
	ENDWHILE

ENDSCRIPT
