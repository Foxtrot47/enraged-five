//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        AM_MP_JUGGALO_HIDEOUT.sc															//
// Description: Script for managing the interior of the solomons office. access, spawning etc. 				//
//				is managed by AM_MP_SMPL_INTERIOR_* script while this script is launched by simple interior	//
//				script to handle anything specific to the office.											//
// Written by:  Mattias Aksli																				//
// Date:  		10/08/2022																					//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_path.sch"
USING "commands_event.sch"
USING "net_include.sch"
USING "net_events.sch"
USING "net_spawn.sch"
USING "net_ambience.sch"
USING "net_wait_zero.sch"
USING "net_collectables_pickups.sch"

#IF FEATURE_DLC_2_2022
USING "net_simple_interior.sch"
USING "net_simple_interior_juggalo_hideout.sch"
USING "net_peds_script_management.sch"
USING "net_MP_CCTV.sch"

#IF IS_DEBUG_BUILD
USING "net_debug_log.sch"
#ENDIF

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ VARIABLES ╞═════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

//////////////////////////////////////////////////////////////
// LOCAL SCRIPT BIT SETS

//m_InteriorData.iBS
CONST_INT BS_JUGGALO_HIDEOUT_INTERIOR_REFRESH_ON_INIT					0
CONST_INT BS_JUGGALO_HIDEOUT_LOAD_SCENE_STARTED							1
CONST_INT BS_JUGGALO_HIDEOUT_CALLED_CLEAR_HELP							2
CONST_INT BS_JUGGALO_HIDEOUT_MINIGAME_LAUNCHER_ACTIVE					3
CONST_INT BS_JUGGALO_GET_OUT_OF_WAY_CLEARED_THIS_FRAME					4
CONST_INT BS_JUGGALO_IS_CAR_MOD_SCRIPT_READY							5
CONST_INT BS_JUGGALO_TAKE_CONTROL_OF_CARMOD								6

//////////////////////////////////////////////////////////////
// BROADCAST DATA BIT SETS

// m_PlayerBD.iBS - Use with SET_LOCAL_PLAYER_BROADCAST_BIT
CONST_INT BS_JUGGALO_HIDEOUT_PLAYER_BD_READY_TO_WARP_OUT				0

/// End bit set const INTs
//////////////////////////////////////////////////////////////

// iServerBS

// .iCarSpawnBS[]
CONST_INT CARSPAWN_BS_CREATE_VEHICLES_BS_MODS_LOADED			0
CONST_INT CARSPAWN_BS_REQUEST_VEHICLE_CLONE_CREATION			1
CONST_INT CARSPAWN_BS_ASSIGN_VEHICLE_TO_INTERIOR				2
CONST_INT CARSPAWN_BS_REQUEST_TO_MOVE_OUT_OF_WAY_FOR_VEHICLE	3
CONST_INT CARSPAWN_BS_FADE_OUT_HIDEOUT_VEHICLE					4
CONST_INT CARSPAWN_BS_RESERVE_HIDEOUT_VEHICLE_INDEX				5

CONST_INT JUGGALO_HIDEOUT_ROOM_KEY -114640481

CONST_INT JUGGALO_HIDEOUT_VEHICLE_ACID_LAB		0
CONST_INT JUGGALO_HIDEOUT_VEHICLE_MANCHEZ3		1
CONST_INT JUGGALO_HIDEOUT_VEHICLE_JOURNEY2		2

ENUM JUGGALO_HIDEOUT_SERVER_STATE
	JUGGALO_HIDEOUT_SERVER_STATE_LOADING,
	JUGGALO_HIDEOUT_SERVER_STATE_IDLE
ENDENUM

ENUM JUGGALO_HIDEOUT_CLIENT_STATE
	JUGGALO_HIDEOUT_STATE_LOAD_INTERIOR,
	JUGGALO_HIDEOUT_STATE_IDLE
ENDENUM

CONST_INT WALK_OUT_SAFETY_TIME 							7000
CONST_FLOAT SAFE_DISTANCE 								5.0

//Walk out server bitset const
CONST_INT WLK_OUT_SERVER_RUN_PLAYER_CLEAN_UP_CHECKS		0
//Walk out player bitset const
CONST_INT WLK_OUT_PLAYER_IM_OUT_OF_THE_WAY				0
//Walk out local bitset const
CONST_INT WLK_OUT_LOCAL_SET_UP_WALK_OUT					0

STRUCT GET_OUT_OF_THE_WAY_SERVER_VARS
	INT iPlayerBitSet
	INT iCreationBitSet
	INT iClientRequestBS
ENDSTRUCT

STRUCT GET_OUT_OF_THE_WAY_PLAYER_VARS
	INT iPlayerBitSet
	VECTOR vGetOutOfWayLoc
ENDSTRUCT

STRUCT GET_OUT_OF_THE_WAY_LOCAL_VARS
	INT iProgressBitSet
	VECTOR vSafePos
	SCRIPT_TIMER controlTimer
	SCRIPT_TIMER getOutOfWayTimer
ENDSTRUCT
GET_OUT_OF_THE_WAY_LOCAL_VARS sGetOutOfWayVars

STRUCT ServerBroadcastData
	JUGGALO_HIDEOUT_SERVER_STATE eState = JUGGALO_HIDEOUT_SERVER_STATE_LOADING	
	INT iServerBS
	
	GET_OUT_OF_THE_WAY_SERVER_VARS sGetOutOfTheWay
	
	VEHICLE_INDEX customVehicleVehIDs[MAX_SAVED_VEHS_IN_JUGGALO_HIDEOUT]
	NETWORK_INDEX customVehicleNetIDs[MAX_SAVED_VEHS_IN_JUGGALO_HIDEOUT]
	INT iReserved
	INT iEnteringVeh = -1
	INT iCarSpawnBS[MAX_SAVED_VEHS_IN_JUGGALO_HIDEOUT]
ENDSTRUCT

STRUCT PlayerBroadcastData
	INT iBS
	MP_CCTV_CLIENT_DATA_STRUCT MPCCTVClient
	GET_OUT_OF_THE_WAY_PLAYER_VARS sGetOutOfTheWay
ENDSTRUCT

STRUCT JUGGALO_HIDEOUT_DATA
	//Bit sets
	INT iBS
		
	//Script state
	INT iScriptInstance
	JUGGALO_HIDEOUT_CLIENT_STATE eState
	
	//Interior data
	SIMPLE_INTERIORS eSimpleInteriorID = SIMPLE_INTERIOR_JUGGALO_HIDEOUT
	SIMPLE_INTERIOR_DETAILS	SimpleInteriorDetails
	INTERIOR_INSTANCE_INDEX iInteriorID
	VECTOR vInteriorPosition
	FLOAT fInteriorHeading
	TEXT_LABEL_63 sInteriorType
	BOOL bScriptRelaunched
	MP_CCTV_LOCAL_DATA_STRUCT MPCCTVLocal
	PLAYER_INDEX pOwner
	
	//Loading data
	SCRIPT_TIMER tPinInMemTimer
	INT iPinInMemTimeToExpire = 10000
	
	THREADID tiMinigameLauncherThread
	
	// Get out of the way
	VECTOR vServerWalkOutLocation
	SCRIPT_TIMER st_ServerWalkOutTimeout
	INT iServerGetOutOfWayPartCheck
	
	SCRIPT_TIMER failSafeClearVehicleDelay[MAX_SAVED_VEHS_IN_JUGGALO_HIDEOUT]
	
	// Ped data
	BOOL bPedScriptLaunched = FALSE
	SCRIPT_TIMER stPedLaunchingBailTimer
	
	// Mod Shop
	SCRIPT_TIMER sCarModScriptRunTimer
	THREADID CarModThread
	STRING sChildOfChildScript
	
	// Blips
	BLIP_INDEX blipModShop
ENDSTRUCT

JUGGALO_HIDEOUT_DATA m_InteriorData
ServerBroadcastData m_ServerBD
PlayerBroadcastData m_PlayerBD[NUM_NETWORK_PLAYERS]

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ FUNCTIONS ╞══════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

#IF IS_DEBUG_BUILD
FUNC STRING DEBUG_GET_JUGGALO_HIDEOUT_STATE_NAME(JUGGALO_HIDEOUT_CLIENT_STATE eState)
	SWITCH eState 
		CASE JUGGALO_HIDEOUT_STATE_LOAD_INTERIOR	RETURN "JUGGALO_HIDEOUT_STATE_LOAD_INTERIOR"
		CASE JUGGALO_HIDEOUT_STATE_IDLE				RETURN "JUGGALO_HIDEOUT_STATE_IDLE"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC STRING DEBUG_GET_JUGGALO_HIDEOUT_SERVER_STATE_NAME(JUGGALO_HIDEOUT_SERVER_STATE eState)
	SWITCH eState
		CASE JUGGALO_HIDEOUT_SERVER_STATE_LOADING	RETURN "JUGGALO_HIDEOUT_SERVER_STATE_LOADING"
		CASE JUGGALO_HIDEOUT_SERVER_STATE_IDLE		RETURN "JUGGALO_HIDEOUT_SERVER_STATE_IDLE"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC STRING DEBUG_GET_PLAYER_BROADCAST_BIT_NAME(INT iBit)
	SWITCH iBit
		CASE 0				RETURN "UNUSED"
	ENDSWITCH
	
	RETURN "NULL"
ENDFUNC

//PROC CREATE_DEBUG_WIDGETS()
//	START_WIDGET_GROUP("AM_MP_JUGGALO_HIDEOUT")
//	DANCE_MAINTAIN_WIDGETS(m_InteriorData.dance)
//	STOP_WIDGET_GROUP()
//ENDPROC

//PROC UPDATE_DEBUG_WIDGETS()
//
//ENDPROC
#ENDIF

FUNC BOOL STORE_JUGGALO_HIDEOUT_PROPERTY_INTERIOR_ID()
	TEXT_LABEL_63 tl63InteriorType
	VECTOR vInteriorPosition
	FLOAT fInteriorHeading
	
	GET_SIMPLE_INTERIOR_INTERIOR_TYPE_AND_POSITION(m_InteriorData.eSimpleInteriorID, tl63InteriorType, vInteriorPosition, fInteriorHeading)
	m_InteriorData.iInteriorID = GET_INTERIOR_AT_COORDS_WITH_TYPE(vInteriorPosition, tl63InteriorType)
	
	RETURN IS_VALID_INTERIOR(m_InteriorData.iInteriorID)
ENDFUNC

FUNC BOOL IS_JUGGALO_HIDEOUT_STATE(JUGGALO_HIDEOUT_CLIENT_STATE eState)
	RETURN m_InteriorData.eState = eState
ENDFUNC

FUNC BOOL IS_JUGGALO_HIDEOUT_SERVER_STATE(JUGGALO_HIDEOUT_SERVER_STATE eState)
	RETURN m_ServerBD.eState = eState
ENDFUNC

PROC SET_JUGGALO_HIDEOUT_STATE(JUGGALO_HIDEOUT_CLIENT_STATE eState)
	CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_JUGGALO_HIDEOUT - SET_JUGGALO_HIDEOUT_STATE - New state: ", DEBUG_GET_JUGGALO_HIDEOUT_STATE_NAME(eState))
	m_InteriorData.eState = eState
ENDPROC

PROC SET_JUGGALO_HIDEOUT_SERVER_STATE(JUGGALO_HIDEOUT_SERVER_STATE eState)
	CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_JUGGALO_HIDEOUT - SET_JUGGALO_HIDEOUT_SERVER_STATE - New state: ", DEBUG_GET_JUGGALO_HIDEOUT_SERVER_STATE_NAME(eState))
	m_ServerBD.eState = eState
ENDPROC

PROC SET_LOCAL_PLAYER_BROADCAST_BIT(INT iBit, BOOL bSet)
	IF bSet
		IF NOT IS_BIT_SET(m_PlayerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, iBit)
			CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_JUGGALO_HIDEOUT - SET_LOCAL_PLAYER_BROADCAST_BIT - Setting bit ", DEBUG_GET_PLAYER_BROADCAST_BIT_NAME(iBit))
			SET_BIT(m_PlayerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, iBit)
		ENDIF
	ELSE
		IF IS_BIT_SET(m_PlayerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, iBit)	
			CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_JUGGALO_HIDEOUT - SET_LOCAL_PLAYER_BROADCAST_BIT - Clearing bit ", DEBUG_GET_PLAYER_BROADCAST_BIT_NAME(iBit))
			CLEAR_BIT(m_PlayerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, iBit)
		ENDIF
	ENDIF
ENDPROC

PROC CLEANUP_CCTV(BOOL bReturnControl)
	CLEANUP_MP_CCTV_CLIENT(m_PlayerBD[NATIVE_TO_INT(PLAYER_ID())].MPCCTVClient, m_InteriorData.MPCCTVLocal, bReturnControl)
ENDPROC

PROC CLEAN_UP_PERSONAL_CAR_MOD()
	PRINTLN("CLEAN_UP_PERSONAL_CAR_MOD (juggalo hideout) - called")
	g_bCleanUpCarmodShop = TRUE
	g_sShopSettings.bShopScriptLaunched[ENUM_TO_INT(CARMOD_SHOP_PERSONALMOD)] = FALSE
	CLEAR_BIT(m_InteriorData.iBS, BS_JUGGALO_IS_CAR_MOD_SCRIPT_READY)
	RESET_NET_TIMER(m_InteriorData.sCarModScriptRunTimer)
	IF NOT IS_STRING_NULL_OR_EMPTY(m_InteriorData.sChildOfChildScript)
		SET_SCRIPT_AS_NO_LONGER_NEEDED(m_InteriorData.sChildOfChildScript)
	ENDIF	
ENDPROC

PROC CLEANUP_JUGGALO_HIDEOUT_INTERIOR_BLIPS()
	SAFE_REMOVE_BLIP(m_InteriorData.blipModShop)
ENDPROC

PROC SCRIPT_CLEANUP()
	CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_JUGGALO_HIDEOUT - SCRIPT_CLEANUP")
		
	RESET_NET_TIMER(g_SimpleInteriorData.tIntAccessFallback)
		
	SET_RADAR_ZOOM_PRECISE(0)
	
	g_SimpleInteriorData.bForceDrinkActivitiesCleanup = FALSE
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_NotAllowedToJackAnyPlayers, FALSE)
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableStartEngine, FALSE)
		SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
		SET_ENTITY_CAN_BE_DAMAGED(PLAYER_PED_ID(), TRUE)
		SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(PLAYER_PED_ID(), TRUE)
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID() ,PCF_DontActivateRagdollFromVehicleImpact,FALSE)
	ENDIF
	
	BOOL bReturnControl = TRUE
	
	IF IS_SKYSWOOP_MOVING()
	OR IS_SKYSWOOP_IN_SKY()
	OR IS_TRANSITION_ACTIVE()
	OR IS_TRANSITION_RUNNING()
	OR IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
		bReturnControl = FALSE
	ENDIF
	
	CLEANUP_CCTV(bReturnControl)
	CLEAN_UP_PERSONAL_CAR_MOD()
	CLEANUP_JUGGALO_HIDEOUT_INTERIOR_BLIPS()
	
	IF m_InteriorData.pOwner != PLAYER_ID()
		BROADCAST_REQUEST_VEHICLE_EXISTENCE(m_InteriorData.pOwner, FALSE, FALSE, FALSE, FALSE, FALSE, TRUE)
	ENDIF
	
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

FUNC BOOL DETERMINE_INTERIOR_OWNER()
	IF IS_LOCAL_PLAYER_IN_JUGGALO_HIDEOUT_THEY_OWN()
		
		m_InteriorData.pOwner = PLAYER_ID()
		
		CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_JUGGALO_HIDEOUT - DETERMINE_INTERIOR_OWNER - Player: ", GET_PLAYER_NAME(m_InteriorData.pOwner), " is the owner. AccessBS = ", g_SimpleInteriorData.iAccessBS)
		RETURN TRUE
		
	ELIF g_SimpleInteriorData.iAccessBS > 0
		IF GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner != INVALID_PLAYER_INDEX()
			IF IS_NET_PLAYER_OK(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner, FALSE)
				
				m_InteriorData.pOwner = GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner
				
				CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_JUGGALO_HIDEOUT - DETERMINE_INTERIOR_OWNER - Interior Owner: ", GET_PLAYER_NAME(m_InteriorData.pOwner))
				RETURN TRUE
			ELSE
				CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_JUGGALO_HIDEOUT - DETERMINE_INTERIOR_OWNER - Interior Owner: ", GET_PLAYER_NAME(m_InteriorData.pOwner), " holding up script due to IS_NET_PLAYER_OK")
				
				IF NOT HAS_NET_TIMER_STARTED(g_SimpleInteriorData.tIntAccessFallback)
					START_NET_TIMER(g_SimpleInteriorData.tIntAccessFallback)
				ENDIF
				
				IF HAS_NET_TIMER_EXPIRED(g_SimpleInteriorData.tIntAccessFallback, g_SimpleInteriorData.iIntAccessFallbackTime)
					
					m_InteriorData.pOwner = GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner
					RESET_NET_TIMER(g_SimpleInteriorData.tIntAccessFallback)
					
					CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_JUGGALO_HIDEOUT - DETERMINE_INTERIOR_OWNER - Interior Owner: ", GET_PLAYER_NAME(m_InteriorData.pOwner), " timer expired for IS_NET_PLAYER_OK, continuing to launch script.")
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		IF (GET_FRAME_COUNT() % 60) = 0
			CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_JUGGALO_HIDEOUT - DETERMINE_INTERIOR_OWNER - Unable to determine interior owner, g_SimpleInteriorData.iAccessBS = ", g_SimpleInteriorData.iAccessBS)
		ENDIF
		#ENDIF
		
		IF NOT HAS_NET_TIMER_STARTED(g_SimpleInteriorData.tIntAccessFallback)
			START_NET_TIMER(g_SimpleInteriorData.tIntAccessFallback)
		ENDIF
		
		IF HAS_NET_TIMER_EXPIRED(g_SimpleInteriorData.tIntAccessFallback, g_SimpleInteriorData.iIntAccessFallbackTime)
			
			//We should not set this to INVALID_PLAYER_INDEX as this script will shutdown and leave the parent script waiting resulting in a black screen bug ala: url:bugstar:4883458
			//Setting this to the local player will either put us in our own club or kick us back to the correct exterior
			m_InteriorData.pOwner = PLAYER_ID()
			SET_BIT(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_JUGGALO_HIDEOUT_ACCESS_BS_OWNER)
			SET_SIMPLE_INTERIOR_CHILD_SCRIPT_NOT_ABLE_TO_DETERMINE_VALID_OWNER(TRUE)
			RESET_NET_TIMER(g_SimpleInteriorData.tIntAccessFallback)
			
			CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_JUGGALO_HIDEOUT - DETERMINE_INTERIOR_OWNER - Unable to determine interior owner, returning TRUE.")
			RETURN TRUE
		ENDIF
	ENDIF

	#IF IS_DEBUG_BUILD
	IF (GET_FRAME_COUNT() % 60) = 0
		CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_JUGGALO_HIDEOUT - DETERMINE_INTERIOR_OWNER - Trying to determine interior owner, g_SimpleInteriorData.iAccessBS = ", g_SimpleInteriorData.iAccessBS)
	ENDIF
	#ENDIF
	
	RETURN FALSE
ENDFUNC

PROC INITIALISE_ENTITY_SETS()
	IF m_InteriorData.pOwner <> INVALID_PLAYER_INDEX()
		IF IS_VALID_INTERIOR(m_InteriorData.iInteriorID)
			ACTIVATE_JUGGALO_HIDEOUT_ENTITY_SET(m_InteriorData.eSimpleInteriorID, JUGGALO_HIDEOUT_ENTITY_SET_ROLLER_DOOR_CLOSED)
		ENDIF
	ENDIF
ENDPROC

PROC INITIALISE()
	IF m_InteriorData.eSimpleInteriorID != SIMPLE_INTERIOR_INVALID
		g_bPlayerLeavingCurrentInteriorInVeh = FALSE
		
		GET_SIMPLE_INTERIOR_DETAILS(m_InteriorData.eSimpleInteriorID, m_InteriorData.SimpleInteriorDetails, FALSE)
		
		IF g_SimpleInteriorData.iExitMenuOption > -1
			g_SimpleInteriorData.iExitMenuOption = -1
		ENDIF
		
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_NotAllowedToJackAnyPlayers, TRUE)
			SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
			SET_ENTITY_CAN_BE_DAMAGED(PLAYER_PED_ID(), FALSE)
			SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(PLAYER_PED_ID(), FALSE)
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DontActivateRagdollFromVehicleImpact,TRUE)
		ENDIF
		
		STORE_JUGGALO_HIDEOUT_PROPERTY_INTERIOR_ID()
		INITIALISE_ENTITY_SETS()
		
		REQUEST_SCRIPT("Apartment_Minigame_Launcher")
		
		// Return Dax's Journey to the hideout for the owner
		IF m_InteriorData.pOwner = PLAYER_ID()
		AND NOT IS_DAX_JOURNEY_INSIDE_HIDEOUT(PLAYER_ID())
			CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_JUGGALO_HIDEOUT - INITIALISE - Owner entered property, setting Dax's Journey inside")
			SET_DAX_JOURNEY_INSIDE_HIDEOUT(TRUE)
		ENDIF
	ELSE
		CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_JUGGALO_HIDEOUT - INITIALISE - Unable to initialise, invalid simple interior index.")
	ENDIF
ENDPROC

PROC SCRIPT_INITIALISE(SIMPLE_INTERIOR_CHILD_SCRIPT_DATA &scriptData)

	CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_JUGGALO_HIDEOUT - SCRIPT_INITIALISE - Launching instance ", m_InteriorData.iScriptInstance)
	
	WHILE NOT DETERMINE_INTERIOR_OWNER()
		
		IF NOT NETWORK_IS_GAME_IN_PROGRESS()
			#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_JUGGALO_HIDEOUT - SCRIPT_INITIALISE - NETWORK_IS_GAME_IN_PROGRESS = FALSE")
			#ENDIF
			SCRIPT_CLEANUP()
		ENDIF
		
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_JUGGALO_HIDEOUT - SCRIPT_INITIALISE - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE = TRUE")
			#ENDIF
			SCRIPT_CLEANUP()
		ENDIF
		
		IF SHOULD_THIS_SIMPLE_INTERIOR_CHILD_THREAD_TERMINATE(scriptData.iScriptInstance, scriptData.eSimpleInteriorID)
			#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_JUGGALO_HIDEOUT - SCRIPT_INITIALISE - SHOULD_THIS_SIMPLE_INTERIOR_CHILD_THREAD_TERMINATE = TRUE")
			#ENDIF
			SCRIPT_CLEANUP()
		ENDIF
		
		IF IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_JUGGALO_HIDEOUT_ACCESS_BS_OWNER_LEFT_GAME)
			#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_JUGGALO_HIDEOUT - SCRIPT_INITIALISE - SIMPLE_INTERIOR_INTERIOR_TEMPLATE_ACCESS_BS_OWNER_LEFT_GAME = TRUE")
			#ENDIF
			SCRIPT_CLEANUP()
		ENDIF

		WAIT(0)
	ENDWHILE
	
	BROADCAST_REQUEST_VEHICLE_EXISTENCE(m_InteriorData.pOwner, TRUE, FALSE, FALSE, FALSE, FALSE, TRUE)
	m_InteriorData.eSimpleInteriorID 	= scriptData.eSimpleInteriorID
	m_InteriorData.iScriptInstance 		= scriptData.iScriptInstance
	
	NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE, m_InteriorData.iScriptInstance)
	
	// Ensures net script is active, waits until it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(m_ServerBD, SIZE_OF(m_ServerBD))
	
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(m_PlayerBD, SIZE_OF(m_PlayerBD))
	
	IF NOT Wait_For_First_Network_Broadcast() 
		CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_JUGGALO_HIDEOUT - SCRIPT_INITIALISE - Failed to receive initial network broadcast.")
		SCRIPT_CLEANUP()
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_JUGGALO_HIDEOUT - SCRIPT_INITIALISE - Initialised.")
	ELSE
		CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_JUGGALO_HIDEOUT - SCRIPT_INITIALISE - NETWORK_IS_GAME_IN_PROGRESS = FALSE")
		SCRIPT_CLEANUP()
	ENDIF
	
	g_bLaunchedMissionFrmSMPLIntLaptop = FALSE
	INITIALISE()
ENDPROC

PROC LOAD_INTERIOR()
	
	IF NOT IS_BIT_SET(m_InteriorData.iBS, BS_JUGGALO_HIDEOUT_INTERIOR_REFRESH_ON_INIT)
	
		IF NOT m_InteriorData.bScriptRelaunched 
			
			GET_SIMPLE_INTERIOR_INTERIOR_TYPE_AND_POSITION(m_InteriorData.eSimpleInteriorID, m_InteriorData.sInteriorType, m_InteriorData.vInteriorPosition, m_InteriorData.fInteriorHeading, TRUE)
			m_InteriorData.iInteriorID = GET_INTERIOR_AT_COORDS_WITH_TYPE(m_InteriorData.vInteriorPosition, m_InteriorData.sInteriorType)
			
			IF IS_VALID_INTERIOR(m_InteriorData.iInteriorID) AND IS_INTERIOR_READY(m_InteriorData.iInteriorID)
				
				BOOL bReady = TRUE
				
				IF bReady
					SET_SIMPLE_INTERIOR_CHILD_SCRIPT_READY(TRUE)
					
					IF IS_PLAYER_IN_JUGGALO_HIDEOUT_PROPERTY(PLAYER_ID())
					
						INT iRoomKey = GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID())
					
						IF iRoomKey = ENUM_TO_INT(JUGGALO_HIDEOUT_ROOM_KEY)
							FORCE_ROOM_FOR_ENTITY(PLAYER_PED_ID(), m_InteriorData.iInteriorID, iRoomKey)
							CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_JUGGALO_HIDEOUT - LOAD_INTERIOR - Forcing room for player, we are inside the interior.")
						ELSE
							CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_JUGGALO_HIDEOUT - LOAD_INTERIOR - Player is in the JUGGALO_HIDEOUT but unable to determine if we should call FORCE_ROOM_FOR_ENTITY")
						ENDIF
					ELSE
						CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_JUGGALO_HIDEOUT - LOAD_INTERIOR - Preventing room force, not inside interior yet.")
					ENDIF
					
					REPIN_AND_REFRESH_SIMPLE_INTERIOR()
					
					SET_SIMPLE_INTERIOR_CHILD_SCRIPT_ALLOWS_FOR_WARP(TRUE)
					SET_BIT(m_InteriorData.iBS, BS_JUGGALO_HIDEOUT_INTERIOR_REFRESH_ON_INIT)
					CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_JUGGALO_HIDEOUT - LOAD_INTERIOR - Setting BS_JUGGALO_HIDEOUT_INTERIOR_REFRESH_ON_INIT")
					
					RESET_NET_TIMER(m_InteriorData.tPinInMemTimer)
				ENDIF
			ELSE
				IF NOT HAS_NET_TIMER_STARTED(m_InteriorData.tPinInMemTimer)
					CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_JUGGALO_HIDEOUT - LOAD_INTERIOR - Pinning interior: ", NATIVE_TO_INT(m_InteriorData.iInteriorID), " in memory.")
					PIN_INTERIOR_IN_MEMORY_VALIDATE(m_InteriorData.iInteriorID)
					START_NET_TIMER(m_InteriorData.tPinInMemTimer)
				ENDIF
				
				IF HAS_NET_TIMER_EXPIRED(m_InteriorData.tPinInMemTimer, m_InteriorData.iPinInMemTimeToExpire)
					RESET_NET_TIMER(m_InteriorData.tPinInMemTimer)
				ENDIF
				
				#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_JUGGALO_HIDEOUT - LOAD_INTERIOR - Waiting for a valid and ready interior with ID: ", NATIVE_TO_INT(m_InteriorData.iInteriorID), " valid? ",
					IS_VALID_INTERIOR(m_InteriorData.iInteriorID), " ready? ", IS_INTERIOR_READY(m_InteriorData.iInteriorID), " disabled? ", IS_INTERIOR_DISABLED(m_InteriorData.iInteriorID))
				#ENDIF
			ENDIF
		ELSE
			SET_SIMPLE_INTERIOR_CHILD_SCRIPT_ALLOWS_FOR_WARP(TRUE)
			SET_BIT(m_InteriorData.iBS, BS_JUGGALO_HIDEOUT_INTERIOR_REFRESH_ON_INIT)
			CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_JUGGALO_HIDEOUT - LOAD_INTERIOR - Setting BS_JUGGALO_HIDEOUT_INTERIOR_REFRESH_ON_INIT")
		ENDIF
	ENDIF
	
	IF CAN_SIMPLE_INTERIOR_CHILD_SCRIPT_TAKE_CONTROL()
	AND IS_BIT_SET(m_InteriorData.iBS, BS_JUGGALO_HIDEOUT_INTERIOR_REFRESH_ON_INIT)
		
		BOOL bReady = TRUE
	
		IF NOT IS_BIT_SET(m_InteriorData.iBS, BS_JUGGALO_HIDEOUT_LOAD_SCENE_STARTED)
			
			IF IS_PLAYER_SWITCH_IN_PROGRESS()
				PRINTLN("AM_MP_JUGGALO_HIDEOUT - LOAD_INTERIOR - Can't start load scene player switch is in progress")
				EXIT
			ENDIF
			
			VECTOR vLoadCoords
			
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
				vLoadCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
			ELSE
				PRINTLN("AM_MP_JUGGALO_HIDEOUT - LOAD_INTERIOR - Can't start load scene player is dead")
				EXIT
			ENDIF
			
			NEW_LOAD_SCENE_START_SPHERE(vLoadCoords, 25.0)
			
			SET_BIT(m_InteriorData.iBS, BS_JUGGALO_HIDEOUT_LOAD_SCENE_STARTED)
			
			PRINTLN("AM_MP_JUGGALO_HIDEOUT - LOAD_INTERIOR - Setting BS_JUGGALO_HIDEOUT_LOAD_SCENE_STARTED")			
			bReady = FALSE
		ELSE
			IF IS_NEW_LOAD_SCENE_ACTIVE()
				
				IF NOT IS_NEW_LOAD_SCENE_LOADED()
					#IF IS_DEBUG_BUILD
					PRINTLN("AM_MP_JUGGALO_HIDEOUT - LOAD_INTERIOR - IS_NEW_LOAD_SCENE_LOADED = FALSE")
					#ENDIF
					bReady = FALSE
				ELSE
					GET_SIMPLE_INTERIOR_INTERIOR_TYPE_AND_POSITION(m_InteriorData.eSimpleInteriorID, m_InteriorData.sInteriorType, m_InteriorData.vInteriorPosition, m_InteriorData.fInteriorHeading)
					m_InteriorData.iInteriorID = GET_INTERIOR_AT_COORDS_WITH_TYPE(m_InteriorData.vInteriorPosition, m_InteriorData.sInteriorType)
					
					IF IS_VALID_INTERIOR(m_InteriorData.iInteriorID) 
					AND IS_INTERIOR_READY(m_InteriorData.iInteriorID)
						#IF IS_DEBUG_BUILD
						PRINTLN("AM_MP_JUGGALO_HIDEOUT - LOAD_INTERIOR - Player is inside interior bounds, load scene is ready.")
						#ENDIF
						NEW_LOAD_SCENE_STOP()
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT HAVE_ALL_PEDS_BEEN_CREATED()
			PRINTLN("AM_MP_JUGGALO_HIDEOUT- LOAD_INTERIOR - Waiting for the peds to be created.")
			bReady = FALSE
			
			IF HAS_NET_TIMER_EXPIRED(m_InteriorData.stPedLaunchingBailTimer, g_iPedScriptLaunchingBailTimerMS)
				PRINTLN("AM_MP_AUTO_SHOP - LOAD_INTERIOR - Ped script has taken longer than: ", g_iPedScriptLaunchingBailTimerMS, "ms so setting g_bInitPedsCreated to TRUE.")
				g_bInitPedsCreated = TRUE
			ENDIF
		ENDIF		
						
		IF bReady
			SET_JUGGALO_HIDEOUT_STATE(JUGGALO_HIDEOUT_STATE_IDLE)
			
			//The office is very small so don't bother with the input gait
			g_SimpleInteriorData.bDontSimulateInputGaitOnEntrance = TRUE
		ENDIF
	ELSE
		CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_JUGGALO_HIDEOUT - LOAD_INTERIOR - CAN_SIMPLE_INTERIOR_CHILD_SCRIPT_TAKE_CONTROL = ", CAN_SIMPLE_INTERIOR_CHILD_SCRIPT_TAKE_CONTROL(), ", BS_JUGGALO_HIDEOUT_INTERIOR_REFRESH_ON_INIT = ", IS_BIT_SET(m_InteriorData.iBS, BS_JUGGALO_HIDEOUT_INTERIOR_REFRESH_ON_INIT))
	ENDIF
ENDPROC

PROC MAINTAIN_JH_CCTV()
	IF IS_SKYSWOOP_MOVING()
	OR IS_SKYSWOOP_IN_SKY()
	OR IS_TRANSITION_ACTIVE()
	OR IS_TRANSITION_RUNNING()
		IF m_PlayerBD[NATIVE_TO_INT(PLAYER_ID())].MPCCTVClient.eStage = MP_CCTV_CLIENT_STAGE_ACTIVATED
			CLEANUP_CCTV(FALSE)
		ENDIF
	ELSE
		CLIENT_MAINTAIN_MP_CCTV(m_PlayerBD[NATIVE_TO_INT(PLAYER_ID())].MPCCTVClient, m_InteriorData.MPCCTVLocal, m_InteriorData.eSimpleInteriorID)
		
		IF g_SimpleInteriorData.bForceCCTVCleanup
			CLEANUP_CCTV(FALSE)
			
			g_SimpleInteriorData.bForceCCTVCleanup = FALSE
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_PLAYER_FLAGS()
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
	AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
	AND NOT g_bEnableJumpingOffHalfTrackInProperty
		SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisablePlayerJumping, TRUE)
		SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisablePlayerVaulting, TRUE)
		SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisablePlayerAutoVaulting, TRUE)
	ENDIF
ENDPROC

PROC RUN_MINIGAME_LAUNCHER()
	IF NOT IS_BIT_SET(m_InteriorData.iBS, BS_JUGGALO_HIDEOUT_MINIGAME_LAUNCHER_ACTIVE)
		STRING sScriptName = "Apartment_Minigame_Launcher"
		
		IF IS_PLAYER_IN_JUGGALO_HIDEOUT_PROPERTY(PLAYER_ID())
		AND NOT IS_THREAD_ACTIVE(m_InteriorData.tiMinigameLauncherThread)
		AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(sScriptName)) < 1
		AND NOT NETWORK_IS_SCRIPT_ACTIVE(sScriptName, GET_SIMPLE_INTERIOR_INT_SCRIPT_INSTANCE() + 32, TRUE)
			IF NOT HAS_SCRIPT_LOADED(sScriptName)
				REQUEST_SCRIPT(sScriptName)
				CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "REQUEST LAUCNHER SCRIPT")
			ELSE
				IF NOT IS_THREAD_ACTIVE(m_InteriorData.tiMinigameLauncherThread)
				AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(sScriptName)) < 1
				AND HAS_SCRIPT_LOADED(sScriptName)
				AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					m_InteriorData.tiMinigameLauncherThread = START_NEW_SCRIPT(sScriptName, DEFAULT_STACK_SIZE)
					
					CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "<MINIGAME LAUNCHER> Minigame Launcher script starting up...")
					
					SET_SCRIPT_AS_NO_LONGER_NEEDED(sScriptName)
					
					SET_BIT(m_InteriorData.iBS, BS_JUGGALO_HIDEOUT_MINIGAME_LAUNCHER_ACTIVE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC VECTOR GET_SAFE_WALK_TO_COORD()
	RETURN <<580.7203, -2605.9392, -50.6470>> // Go to center of warehouse for now until acid lab coords are confirmed.
ENDFUNC

FUNC BOOL IS_PLAYER_CLEAR_OF_JUGGALO_WAREHOUSE_VEHICLE(VECTOR vPos)
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF GET_DISTANCE_BETWEEN_COORDS(vPos, GET_ENTITY_COORDS(PLAYER_PED_ID()), FALSE) < SAFE_DISTANCE
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL MAINTAIN_GET_OUT_OF_THE_WAY_JUGGALO_WAREHOUSE()
	INT iMyPart = PARTICIPANT_ID_TO_INT()
	
	IF iMyPart = -1
		RETURN FALSE
	ENDIF
	
	INT iMyGBD = NATIVE_TO_INT(PLAYER_ID())
	
	IF (IS_BIT_SET(m_ServerBD.sGetOutOfTheWay.iPlayerBitSet, iMyGBD)
	OR IS_BIT_SET(sGetOutOfWayVars.iProgressBitSet, WLK_OUT_LOCAL_SET_UP_WALK_OUT))
		IF HAS_NET_TIMER_STARTED(sGetOutOfWayVars.getOutOfWayTimer)
		AND NOT HAS_NET_TIMER_EXPIRED(sGetOutOfWayVars.getOutOfWayTimer, 5000)
			SET_BIT(m_PlayerBD[iMyGBD].sGetOutOfTheWay.iPlayerBitSet, WLK_OUT_PLAYER_IM_OUT_OF_THE_WAY)
			CLEAR_BIT(sGetOutOfWayVars.iProgressBitSet, WLK_OUT_LOCAL_SET_UP_WALK_OUT)
			RESET_NET_TIMER(sGetOutOfWayVars.controlTimer)
		ELSE
			IF NOT IS_BIT_SET(m_PlayerBD[iMyGBD].sGetOutOfTheWay.iPlayerBitSet, WLK_OUT_PLAYER_IM_OUT_OF_THE_WAY)
				IF NOT IS_BIT_SET(sGetOutOfWayVars.iProgressBitSet, WLK_OUT_LOCAL_SET_UP_WALK_OUT)
					IF IS_NET_PLAYER_OK(PLAYER_ID())
						NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_CLEAR_TASKS)
						sGetOutOfWayVars.vSafePos = GET_SAFE_WALK_TO_COORD()
						TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), sGetOutOfWayVars.vSafePos, PEDMOVE_WALK, WALK_OUT_SAFETY_TIME)
						REINIT_NET_TIMER(sGetOutOfWayVars.controlTimer, TRUE)
						SET_BIT(sGetOutOfWayVars.iProgressBitSet, WLK_OUT_LOCAL_SET_UP_WALK_OUT)
					ENDIF
				ELSE
					IF HAS_NET_TIMER_EXPIRED(sGetOutOfWayVars.controlTimer, WALK_OUT_SAFETY_TIME, TRUE)
					OR IS_PLAYER_CLEAR_OF_JUGGALO_WAREHOUSE_VEHICLE(sGetOutOfWayVars.vSafePos)
						IF IS_NET_PLAYER_OK(PLAYER_ID())
							IF NOT IS_PLAYER_CLEAR_OF_JUGGALO_WAREHOUSE_VEHICLE(sGetOutOfWayVars.vSafePos)
								IF GET_DISTANCE_BETWEEN_COORDS(sGetOutOfWayVars.vSafePos, GET_ENTITY_COORDS(PLAYER_PED_ID())) > 0.5
									SET_ENTITY_COORDS_NO_OFFSET(PLAYER_PED_ID(), sGetOutOfWayVars.vSafePos)
								ENDIF
							ENDIF
						ENDIF
						
						SET_BIT(m_PlayerBD[iMyGBD].sGetOutOfTheWay.iPlayerBitSet, WLK_OUT_PLAYER_IM_OUT_OF_THE_WAY)
						CLEAR_BIT(sGetOutOfWayVars.iProgressBitSet, WLK_OUT_LOCAL_SET_UP_WALK_OUT)
						RESET_NET_TIMER(sGetOutOfWayVars.controlTimer)
						NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					ELSE
						IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) != PERFORMING_TASK
						AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) != WAITING_TO_START_TASK
							TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), sGetOutOfWayVars.vSafePos, PEDMOVE_WALK, WALK_OUT_SAFETY_TIME)
						ENDIF
					ENDIF
					
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ELSE
		CLEAR_BIT(m_PlayerBD[iMyGBD].sGetOutOfTheWay.iPlayerBitSet, WLK_OUT_PLAYER_IM_OUT_OF_THE_WAY)
		
		IF m_PlayerBD[iMyGBD].sGetOutOfTheWay.iPlayerBitSet != 0
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SERVER_CLEANUP_PLAYERS_ARE_IN_THE_WAY_JUGGALO_WAREHOUSE_FLAGS()
	IF IS_BIT_SET(m_ServerBD.sGetOutOfTheWay.iCreationBitSet, WLK_OUT_SERVER_RUN_PLAYER_CLEAN_UP_CHECKS)
		BOOL bActive
		PLAYER_INDEX playerID
		
		bActive = FALSE
		playerID = INT_TO_PLAYERINDEX(m_InteriorData.iServerGetOutOfWayPartCheck)
		
		IF IS_NET_PLAYER_OK(playerID, FALSE)
			IF NETWORK_IS_PLAYER_A_PARTICIPANT(playerID)
				bActive = TRUE
				
				IF IS_BIT_SET(m_ServerBD.sGetOutOfTheWay.iPlayerBitSet, m_InteriorData.iServerGetOutOfWayPartCheck)
					IF IS_BIT_SET(m_PlayerBD[NATIVE_TO_INT(playerID)].sGetOutOfTheWay.iPlayerBitSet, WLK_OUT_PLAYER_IM_OUT_OF_THE_WAY)
						CLEAR_BIT(m_ServerBD.sGetOutOfTheWay.iPlayerBitSet, m_InteriorData.iServerGetOutOfWayPartCheck)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF bActive = FALSE
			IF IS_BIT_SET(m_ServerBD.sGetOutOfTheWay.iPlayerBitSet, m_InteriorData.iServerGetOutOfWayPartCheck)
				CLEAR_BIT(m_ServerBD.sGetOutOfTheWay.iPlayerBitSet, m_InteriorData.iServerGetOutOfWayPartCheck)
			ENDIF
		ENDIF
		
		m_InteriorData.iServerGetOutOfWayPartCheck++
		
		IF m_InteriorData.iServerGetOutOfWayPartCheck >= NUM_NETWORK_PLAYERS
			m_InteriorData.iServerGetOutOfWayPartCheck = 0
			
			IF m_ServerBD.sGetOutOfTheWay.iPlayerBitSet = 0
				CLEAR_BIT(m_ServerBD.sGetOutOfTheWay.iCreationBitSet, WLK_OUT_SERVER_RUN_PLAYER_CLEAN_UP_CHECKS)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_GET_OUT_OF_THE_WAY_JUGGALO_WAREHOUSE_WRAPPER()
	IF IS_BIT_SET(m_InteriorData.iBS, BS_JUGGALO_GET_OUT_OF_WAY_CLEARED_THIS_FRAME)
		CLEAR_BIT(m_InteriorData.iBS, BS_JUGGALO_GET_OUT_OF_WAY_CLEARED_THIS_FRAME)
	ENDIF
		
	IF MAINTAIN_GET_OUT_OF_THE_WAY_JUGGALO_WAREHOUSE()
		m_PlayerBD[NATIVE_TO_INT(PLAYER_ID())].sGetOutOfTheWay.iPlayerBitSet = 0
		sGetOutOfWayVars.iProgressBitSet = 0
	ENDIF
	
	SERVER_CLEANUP_PLAYERS_ARE_IN_THE_WAY_JUGGALO_WAREHOUSE_FLAGS()
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════╡ ACID LAB ╞═════════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

FUNC BOOL SHOULD_ALL_WAREHOUSE_VEHICLES_EXIST_FOR_EXIT_CUTSCENE()
	RETURN FALSE
ENDFUNC

FUNC BOOL GET_NEARBY_ACID_LAB_FOR_JUGGALO_WAREHOUSE(PLAYER_INDEX pOwnerOfAcidLab, VEHICLE_INDEX &acidLabVeh)
	
	INT iTotal, iInstance
	STRING scriptName
	iTotal = GET_ALL_VEHICLES(g_PoolAcidLabs)
	
	INT i
	REPEAT iTotal i
		IF DOES_ENTITY_EXIST(g_PoolAcidLabs[i])
		AND NOT IS_ENTITY_DEAD(g_PoolAcidLabs[i])		
			
			IF IS_ENTITY_A_VEHICLE(g_PoolAcidLabs[i])
				IF NOT IS_ENTITY_DEAD(g_PoolAcidLabs[i])
					IF DECOR_IS_REGISTERED_AS_TYPE("Player_Acid_Lab", DECOR_TYPE_INT)
						IF GET_ENTITY_MODEL(g_PoolAcidLabs[i]) = GET_ACID_LAB_MODEL()
							IF IS_VEHICLE_A_PERSONAL_ACID_LAB(g_PoolAcidLabs[i])
								scriptName = GET_ENTITY_SCRIPT(g_PoolAcidLabs[i],iInstance)
								IF IS_NET_PLAYER_OK(pOwnerOfAcidLab)
								AND NOT IS_STRING_NULL_OR_EMPTY(scriptName)
									IF GET_OWNER_OF_PERSONAL_ACID_LAB(g_PoolAcidLabs[i]) = pOwnerOfAcidLab
									AND ARE_STRINGS_EQUAL(scriptName,"am_vehicle_spawn")
										acidLabVeh = g_PoolAcidLabs[i]
										PRINTLN("GET_NEARBY_ACID_LAB_FOR_JUGGALO_WAREHOUSE - found a vehicle owner is: ", GET_PLAYER_NAME(pOwnerOfAcidLab) )
										RETURN TRUE
									ENDIF
								ENDIF	
							ENDIF	
						ENDIF	
					ENDIF	
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL GET_OWNER_ACID_LAB_VEHICLE_INDEX(VEHICLE_INDEX &acidLabVeh)
	VEHICLE_INDEX localacidLabVeh
	
	IF GET_NEARBY_ACID_LAB_FOR_JUGGALO_WAREHOUSE(m_InteriorData.pOwner, localacidLabVeh)
		acidLabVeh = localacidLabVeh
	ELSE
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "GET_OWNER_ACID_LAB_VEHICLE_INDEX localacidLabVeh doesn't exist")
		#ENDIF
		IF HAS_START_TO_MOVE_PEDS_IN_CLONE_ACID_LAB(m_InteriorData.pOwner)
			IF IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(m_InteriorData.pOwner))
				localacidLabVeh = GET_VEHICLE_PED_IS_IN(GET_PLAYER_PED(m_InteriorData.pOwner))
				IF GET_OWNER_OF_PERSONAL_ARMORY_AIRCRAFT(localacidLabVeh) = m_InteriorData.pOwner
					acidLabVeh = localacidLabVeh
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "GET_OWNER_ACID_LAB_VEHICLE_INDEX owner is driving in #acidlab# found his #acidlab#")
					#ENDIF
				ENDIF
			ENDIF
		ELSE
			localacidLabVeh = GET_CLOSEST_VEHICLE(GET_ACID_LAB_HIDEOUT_SPAWN_POINT() + <<(NATIVE_TO_INT(m_InteriorData.pOwner) * 32),0,0>>, 30.0, GET_ACID_LAB_MODEL()
										, VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES | VEHICLE_SEARCH_FLAG_ALLOW_TRAILERS)
			
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "GET_OWNER_ACID_LAB_VEHICLE_INDEX can't find owner's #acidlab# trying closest vehicle")
			#ENDIF
			
			IF DOES_ENTITY_EXIST(localacidLabVeh)
				IF GET_OWNER_OF_PERSONAL_ACID_LAB(localacidLabVeh) = m_InteriorData.pOwner
					acidLabVeh = localacidLabVeh
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "GET_OWNER_ACID_LAB_VEHICLE_INDEX found #acidlab# with get_closet_vehicle")
					#ENDIF	
				ENDIF
			ENDIF
		ENDIF	
	ENDIF
	
	IF DOES_ENTITY_EXIST(acidLabVeh)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "GET_OWNER_ACID_LAB_VEHICLE_INDEX true")
		#ENDIF
		RETURN TRUE
	ENDIF	
	
	RETURN FALSE
ENDFUNC

PROC SERVER_CHECK_ANY_PLAYERS_ARE_IN_THE_WAY_OF_JUGGALO_WAREHOUSE_VEHICLES(VECTOR vCreationPos)
	PLAYER_INDEX playerID
	INT iLoop
	VECTOR vPlayerPos
	FLOAT fdistance
	FLOAT fDistanceToCheck = 5
	
	IF NOT IS_BIT_SET(m_InteriorData.iBS, BS_JUGGALO_GET_OUT_OF_WAY_CLEARED_THIS_FRAME)
		m_ServerBD.sGetOutOfTheWay.iCreationBitSet = 0
		m_ServerBD.sGetOutOfTheWay.iPlayerBitSet = 0
		SET_BIT(m_InteriorData.iBS, BS_JUGGALO_GET_OUT_OF_WAY_CLEARED_THIS_FRAME)
	ENDIF
	
	IF ARE_VECTORS_ALMOST_EQUAL(m_InteriorData.vServerWalkOutLocation, vCreationPos, 0.1)
		IF NOT HAS_NET_TIMER_STARTED(m_InteriorData.st_ServerWalkOutTimeout)
			START_NET_TIMER(m_InteriorData.st_ServerWalkOutTimeout, TRUE)
		ELSE
			IF HAS_NET_TIMER_EXPIRED(m_InteriorData.st_ServerWalkOutTimeout, 15000, TRUE)
				EXIT
			ENDIF
		ENDIF
	ELSE
		RESET_NET_TIMER(m_InteriorData.st_ServerWalkOutTimeout)
	ENDIF
	
	m_InteriorData.vServerWalkOutLocation = vCreationPos
	
	REPEAT NUM_NETWORK_PLAYERS iLoop
		playerID = INT_TO_PLAYERINDEX(iLoop)
		
		IF IS_NET_PLAYER_OK(playerID, FALSE)
			IF NETWORK_IS_PLAYER_A_PARTICIPANT(playerID)
				IF NOT IS_BIT_SET(m_ServerBD.sGetOutOfTheWay.iPlayerBitSet, iLoop)
					IF IS_NET_PLAYER_OK(playerID, TRUE, TRUE)
						IF NOT IS_THIS_PLAYER_ACTIVE_IN_CORONA(playerID)
						AND NOT IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(playerID))
							vPlayerPos = GET_ENTITY_COORDS(GET_PLAYER_PED(playerID))
							fdistance = GET_DISTANCE_BETWEEN_COORDS(vPlayerPos, vCreationPos, FALSE)
							
							IF fDistance < fDistanceToCheck
							AND (vPlayerPos.z >= -55.0)
								SET_BIT(m_ServerBD.sGetOutOfTheWay.iPlayerBitSet, iLoop)
								SET_BIT(m_ServerBD.sGetOutOfTheWay.iCreationBitSet, WLK_OUT_SERVER_RUN_PLAYER_CLEAN_UP_CHECKS)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

FUNC BOOL IS_IT_SAFE_TO_CREATE_A_JUGGALO_WAREHOUSE_VEHICLE()
	RETURN NOT IS_BIT_SET(m_ServerBD.sGetOutOfTheWay.iCreationBitSet, WLK_OUT_SERVER_RUN_PLAYER_CLEAN_UP_CHECKS)
ENDFUNC

PROC UPDATE_HIDEOUT_GLOBAL_VEHICLE_INDEX()
	INT i
	REPEAT MAX_SAVED_VEHS_IN_JUGGALO_HIDEOUT i
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(m_ServerBD.customVehicleNetIDs[i])
			IF (GET_ENTITY_MODEL(NET_TO_VEH(m_ServerBD.customVehicleNetIDs[i])) = GET_ACID_LAB_MODEL())
				g_viAcidLabInJuggaloWarehouse = NET_TO_VEH(m_ServerBD.customVehicleNetIDs[i])
			ELIF (GET_ENTITY_MODEL(NET_TO_VEH(m_ServerBD.customVehicleNetIDs[i])) = MANCHEZ3)
				g_viSupportBikeInJuggaloWarehouse = NET_TO_VEH(m_ServerBD.customVehicleNetIDs[i])
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC MAINTAIN_CONTROL_OF_HIDEOUT_PERSONAL_CARMOD_VEHICLES()
	
	IF IS_PLAYER_STARTED_TO_USE_PERSONAL_CAR_MOD(PLAYER_ID())
		IF NOT IS_BIT_SET(m_InteriorData.iBS, BS_JUGGALO_TAKE_CONTROL_OF_CARMOD)
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(m_ServerBD.customVehicleNetIDs[JUGGALO_HIDEOUT_VEHICLE_ACID_LAB])
			AND NETWORK_HAS_CONTROL_OF_ENTITY(NET_TO_ENT(m_ServerBD.customVehicleNetIDs[JUGGALO_HIDEOUT_VEHICLE_ACID_LAB]))
			AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(m_ServerBD.customVehicleNetIDs[JUGGALO_HIDEOUT_VEHICLE_MANCHEZ3])
			AND NETWORK_HAS_CONTROL_OF_ENTITY(NET_TO_ENT(m_ServerBD.customVehicleNetIDs[JUGGALO_HIDEOUT_VEHICLE_MANCHEZ3]))
			AND NETWORK_HAS_ENTITY_BEEN_REGISTERED_WITH_THIS_THREAD(NET_TO_ENT(m_ServerBD.customVehicleNetIDs[JUGGALO_HIDEOUT_VEHICLE_ACID_LAB]))
			AND NETWORK_HAS_ENTITY_BEEN_REGISTERED_WITH_THIS_THREAD(NET_TO_ENT(m_ServerBD.customVehicleNetIDs[JUGGALO_HIDEOUT_VEHICLE_MANCHEZ3]))
				SET_BIT(m_InteriorData.iBS, BS_JUGGALO_TAKE_CONTROL_OF_CARMOD)
				SET_NETWORK_ID_CAN_MIGRATE(m_ServerBD.customVehicleNetIDs[JUGGALO_HIDEOUT_VEHICLE_ACID_LAB], FALSE)
				SET_NETWORK_ID_CAN_MIGRATE(m_ServerBD.customVehicleNetIDs[JUGGALO_HIDEOUT_VEHICLE_MANCHEZ3], FALSE)
			ENDIF
		ENDIF	
	ELSE
		IF IS_BIT_SET(m_InteriorData.iBS, BS_JUGGALO_TAKE_CONTROL_OF_CARMOD)
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(m_ServerBD.customVehicleNetIDs[JUGGALO_HIDEOUT_VEHICLE_ACID_LAB])
			AND NETWORK_HAS_CONTROL_OF_ENTITY(NET_TO_ENT(m_ServerBD.customVehicleNetIDs[JUGGALO_HIDEOUT_VEHICLE_ACID_LAB]))
			AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(m_ServerBD.customVehicleNetIDs[JUGGALO_HIDEOUT_VEHICLE_MANCHEZ3])
			AND NETWORK_HAS_CONTROL_OF_ENTITY(NET_TO_ENT(m_ServerBD.customVehicleNetIDs[JUGGALO_HIDEOUT_VEHICLE_MANCHEZ3]))
			AND NETWORK_HAS_ENTITY_BEEN_REGISTERED_WITH_THIS_THREAD(NET_TO_ENT(m_ServerBD.customVehicleNetIDs[JUGGALO_HIDEOUT_VEHICLE_ACID_LAB]))
			AND NETWORK_HAS_ENTITY_BEEN_REGISTERED_WITH_THIS_THREAD(NET_TO_ENT(m_ServerBD.customVehicleNetIDs[JUGGALO_HIDEOUT_VEHICLE_MANCHEZ3]))
				CLEAR_BIT(m_InteriorData.iBS, BS_JUGGALO_TAKE_CONTROL_OF_CARMOD)
				SET_NETWORK_ID_CAN_MIGRATE(m_ServerBD.customVehicleNetIDs[JUGGALO_HIDEOUT_VEHICLE_ACID_LAB], TRUE)
				SET_NETWORK_ID_CAN_MIGRATE(m_ServerBD.customVehicleNetIDs[JUGGALO_HIDEOUT_VEHICLE_MANCHEZ3], TRUE)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC MAINTAIN_JUGGALO_HIDEOUT_INTERIOR_BLIPS()
	IF NOT IS_PLAYER_IN_ZOOMED_OUT_RADAR_OR_MAP(PLAYER_ID())
		IF m_InteriorData.pOwner = PLAYER_ID()
			ADD_BASIC_BLIP(m_InteriorData.blipModShop, <<568.6929, -438.6230, -69.6470>>, RADAR_TRACE_GR_MOC_UPGRADE, "BLIP_839")						
		ENDIF
	ELSE
		CLEANUP_JUGGALO_HIDEOUT_INTERIOR_BLIPS()
	ENDIF
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════╡ VEHICLES ╞═════════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

FUNC MODEL_NAMES GET_HIDEOUT_VEHICLE_MODEL(INT iVehIndex)
	SWITCH iVehIndex
		CASE JUGGALO_HIDEOUT_VEHICLE_ACID_LAB	RETURN GET_ACID_LAB_MODEL()
		CASE JUGGALO_HIDEOUT_VEHICLE_MANCHEZ3	RETURN MANCHEZ3
		CASE JUGGALO_HIDEOUT_VEHICLE_JOURNEY2	RETURN JOURNEY2
	ENDSWITCH
	
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

FUNC BOOL SHOULD_HIDEOUT_VEHICLE_EXIST(INT iVehIndex)
	IF NETWORK_IS_IN_MP_CUTSCENE()
		RETURN FALSE
	ENDIF
	
	SWITCH iVehIndex
		CASE JUGGALO_HIDEOUT_VEHICLE_ACID_LAB
			IF IS_OWNERS_ACID_LAB_INSIDE_JUGGALO_HIDEOUT(m_InteriorData.pOwner)
			AND IS_ACID_LAB_UNDER_MAP(m_InteriorData.pOwner)
			AND NOT IS_PLAYER_REQUESTED_ACID_LAB_IN_FREEMODE(m_InteriorData.pOwner)
				RETURN TRUE
			ENDIF		
		BREAK
		CASE JUGGALO_HIDEOUT_VEHICLE_MANCHEZ3
			// @todo - add functions same as above
			// Same as acid lab + more for requesting bike
			IF IS_OWNERS_ACID_LAB_INSIDE_JUGGALO_HIDEOUT(m_InteriorData.pOwner)
			AND IS_ACID_LAB_UNDER_MAP(m_InteriorData.pOwner)
			AND NOT IS_PLAYER_REQUESTED_ACID_LAB_IN_FREEMODE(m_InteriorData.pOwner)
				RETURN TRUE
			ENDIF
		BREAK
		CASE JUGGALO_HIDEOUT_VEHICLE_JOURNEY2
			// @todo - add functions same as above
			IF IS_DAX_JOURNEY_INSIDE_HIDEOUT(m_InteriorData.pOwner)
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC VECTOR GET_HIDEOUT_VEHICLE_POSITION(INT iVehIndex)
	SWITCH iVehIndex
		CASE JUGGALO_HIDEOUT_VEHICLE_ACID_LAB	RETURN GET_ACID_LAB_HIDEOUT_SPAWN_POINT()
		CASE JUGGALO_HIDEOUT_VEHICLE_MANCHEZ3	RETURN <<567.3000, -443.6138, -70.1761>>
		CASE JUGGALO_HIDEOUT_VEHICLE_JOURNEY2	RETURN <<572.0479, -416.8825, -70.1668>>
	ENDSWITCH
	
	RETURN ZERO_VECTOR()
ENDFUNC

FUNC FLOAT GET_HIDEOUT_VEHICLE_HEADING(INT iVehIndex)
	SWITCH iVehIndex
		CASE JUGGALO_HIDEOUT_VEHICLE_ACID_LAB	RETURN GET_ACID_LAB_HIDEOUT_HEADING()
		CASE JUGGALO_HIDEOUT_VEHICLE_MANCHEZ3	RETURN 14.0400
		CASE JUGGALO_HIDEOUT_VEHICLE_JOURNEY2	RETURN 113.0400
	ENDSWITCH
	
	RETURN 0.0
ENDFUNC

FUNC BOOL IS_OWNER_REQUESTED_HIDEOUT_VEHICLE_IN_FREEMODE(INT iVehIndex)
	SWITCH iVehIndex
		CASE JUGGALO_HIDEOUT_VEHICLE_ACID_LAB	RETURN IS_PLAYER_REQUESTED_ACID_LAB_IN_FREEMODE(m_InteriorData.pOwner)
		CASE JUGGALO_HIDEOUT_VEHICLE_MANCHEZ3	RETURN FALSE
		CASE JUGGALO_HIDEOUT_VEHICLE_JOURNEY2	RETURN FALSE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_OWNERS_HIDEOUT_VEHICLE_UNDER_MAP(INT iVehIndex)
	SWITCH iVehIndex
		CASE JUGGALO_HIDEOUT_VEHICLE_ACID_LAB	RETURN IS_ACID_LAB_UNDER_MAP(m_InteriorData.pOwner)
		CASE JUGGALO_HIDEOUT_VEHICLE_MANCHEZ3	RETURN TRUE
		CASE JUGGALO_HIDEOUT_VEHICLE_JOURNEY2	RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_OWNERS_HIDEOUT_VEHICLE_INSIDE_JUGGALO_HIDEOUT(INT iVehIndex)
	SWITCH iVehIndex
		CASE JUGGALO_HIDEOUT_VEHICLE_ACID_LAB	RETURN IS_OWNERS_ACID_LAB_INSIDE_JUGGALO_HIDEOUT(m_InteriorData.pOwner)
		CASE JUGGALO_HIDEOUT_VEHICLE_MANCHEZ3	RETURN TRUE
		CASE JUGGALO_HIDEOUT_VEHICLE_JOURNEY2	RETURN IS_DAX_JOURNEY_INSIDE_HIDEOUT(m_InteriorData.pOwner)
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC BROADCAST_CREATE_OWNERS_HIDEOUT_VEHICLE_UNDER_MAP(INT iVehIndex, BOOL bCreationConfirmation)
	SWITCH iVehIndex
		CASE JUGGALO_HIDEOUT_VEHICLE_ACID_LAB
			BROADCAST_CREATE_ACID_LAB_UNDER_MAP(m_InteriorData.pOwner, bCreationConfirmation)
		BREAK
		CASE JUGGALO_HIDEOUT_VEHICLE_MANCHEZ3
			// @TODO
		BREAK
		CASE JUGGALO_HIDEOUT_VEHICLE_JOURNEY2
			// @TODO
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL GET_OWNERS_HIDEOUT_VEHICLE_INDEX(INT iVehIndex, VEHICLE_INDEX &ref_Veh)
	SWITCH iVehIndex
		CASE JUGGALO_HIDEOUT_VEHICLE_ACID_LAB	RETURN GET_OWNER_ACID_LAB_VEHICLE_INDEX(ref_Veh)
		CASE JUGGALO_HIDEOUT_VEHICLE_MANCHEZ3	RETURN TRUE
		CASE JUGGALO_HIDEOUT_VEHICLE_JOURNEY2	RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_SKIP_HIDEOUT_VEHICLE_INDEX(INT iVehIndex)
	SWITCH iVehIndex
		CASE JUGGALO_HIDEOUT_VEHICLE_ACID_LAB	RETURN FALSE
		CASE JUGGALO_HIDEOUT_VEHICLE_MANCHEZ3	RETURN TRUE
		CASE JUGGALO_HIDEOUT_VEHICLE_JOURNEY2	RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CREATE_HIDEOUT_VEHICLE(INT i)
	VECTOR vCoord = GET_HIDEOUT_VEHICLE_POSITION(i)
	FLOAT fHeading = GET_HIDEOUT_VEHICLE_HEADING(i)
	MODEL_NAMES eModel = GET_HIDEOUT_VEHICLE_MODEL(i)
	VEHICLE_INDEX tempVehicle

	IF NOT IS_NET_VEHICLE_DRIVEABLE(m_ServerBD.customVehicleNetIDs[i])
		IF IS_MODEL_IN_CDIMAGE(eModel)
			IF REQUEST_LOAD_MODEL(eModel)
				IF NOT IS_BIT_SET(m_ServerBD.iCarSpawnBS[i], CARSPAWN_BS_RESERVE_HIDEOUT_VEHICLE_INDEX)
					IF CAN_RESERVE_NETWORK_VEHICLES_FOR_THIS_SCRIPT(GET_NUM_RESERVED_MISSION_VEHICLES() + 1, FALSE, TRUE)
						RESERVE_LOCAL_NETWORK_MISSION_VEHICLES(GET_NUM_RESERVED_MISSION_VEHICLES() + 1)
						m_ServerBD.iReserved++
						SET_BIT(m_ServerBD.iCarSpawnBS[i], CARSPAWN_BS_RESERVE_HIDEOUT_VEHICLE_INDEX)
						PRINTLN("[MAINTAIN_VEHICLES] - CREATE_HIDEOUT_VEHICLE - CARSPAWN_BS_RESERVE_HIDEOUT_VEHICLE_INDEX TRUE for i: ", i)
					ENDIF
				ELSE
					SERVER_CHECK_ANY_PLAYERS_ARE_IN_THE_WAY_OF_JUGGALO_WAREHOUSE_VEHICLES(vCoord)
					
					IF NOT IS_IT_SAFE_TO_CREATE_A_JUGGALO_WAREHOUSE_VEHICLE()
					AND NOT SHOULD_ALL_WAREHOUSE_VEHICLES_EXIST_FOR_EXIT_CUTSCENE()
						PRINTLN("[MAINTAIN_VEHICLES] CREATE_HIDEOUT_VEHICLE - IS_IT_SAFE_TO_CREATE_A_JUGGALO_WAREHOUSE_VEHICLE = FALSE for i: ", i)
						RETURN FALSE
					ENDIF
					
					IF CAN_REGISTER_MISSION_ENTITIES(0, 1, 0, 0)
						IF NETWORK_IS_IN_MP_CUTSCENE() 
							SET_NETWORK_CUTSCENE_ENTITIES(TRUE)	
						ENDIF
						
						tempVehicle = CREATE_VEHICLE(eModel, vCoord, fHeading)
						m_ServerBD.customVehicleNetIDs[i] = VEH_TO_NET(tempVehicle)
						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(tempVehicle, TRUE)
						
						SET_ENTITY_INVINCIBLE(tempVehicle, TRUE)
						
						IF i = JUGGALO_HIDEOUT_VEHICLE_ACID_LAB
							IF DECOR_IS_REGISTERED_AS_TYPE("Player_Acid_Lab", DECOR_TYPE_INT)
								IF NOT DECOR_EXIST_ON(tempVehicle, "Player_Acid_Lab")
								AND IS_NET_PLAYER_OK(m_InteriorData.pOwner, FALSE)
									DECOR_SET_INT(tempVehicle, "Player_Acid_Lab", NETWORK_HASH_FROM_PLAYER_HANDLE(m_InteriorData.pOwner))
								ENDIF
							ENDIF
						ENDIF
						
						IF DOES_ENTITY_EXIST(m_ServerBD.customVehicleVehIDs[i])
							VEHICLE_SETUP_STRUCT_MP sDataVeh
							GET_VEHICLE_SETUP_MP(m_ServerBD.customVehicleVehIDs[i], sDataVeh)
							SET_VEHICLE_SETUP_MP(tempVehicle, sDataVeh)
							PRINTLN("[MAINTAIN_VEHICLES] CREATE_HIDEOUT_VEHICLE - Applying setup for i: ", i)
						ENDIF
						
						//SET_ENTITY_COLLISION(tempVehicle, FALSE)
						FREEZE_ENTITY_POSITION(tempVehicle, TRUE)
						SET_ENTITY_COORDS_NO_OFFSET(tempVehicle, vCoord)
						SET_VEHICLE_ON_GROUND_PROPERLY(tempVehicle)
						SET_ENTITY_HEADING(tempVehicle, fHeading)
						SET_ENTITY_COLLISION(tempVehicle, TRUE)
						//FREEZE_ENTITY_POSITION(tempVehicle, FALSE)
						
						IF i = JUGGALO_HIDEOUT_VEHICLE_ACID_LAB
							SET_REDUCED_SUSPENSION_FORCE(tempVehicle, TRUE)
						ENDIF
						
						NETWORK_FADE_IN_ENTITY(tempVehicle, TRUE, FALSE)
						
						SET_ENTITY_VISIBLE(tempVehicle, TRUE)
						SET_VEHICLE_ENGINE_ON(tempVehicle, FALSE, TRUE)
						
						NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(tempVehicle, TRUE)
						SET_VEHICLE_NOT_STEALABLE_AMBIENTLY(tempVehicle, FALSE)
						SET_VEHICLE_FULLBEAM(tempVehicle, FALSE)
						SET_VEHICLE_LIGHTS(tempVehicle, FORCE_VEHICLE_LIGHTS_OFF)
						
						SET_VEHICLE_DOORS_LOCKED(tempVehicle, VEHICLELOCK_CANNOT_ENTER)
						SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(tempVehicle, TRUE)
						
						IF m_InteriorData.pOwner != INVALID_PLAYER_INDEX()
							IF IS_NET_PLAYER_OK(m_InteriorData.pOwner, FALSE, FALSE)
							AND i = JUGGALO_HIDEOUT_VEHICLE_ACID_LAB
								SET_VEHICLE_DOORS_LOCKED(tempVehicle, VEHICLELOCK_UNLOCKED)
								SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(tempVehicle, m_InteriorData.pOwner, FALSE)
							ENDIF
						ENDIF
						
						SET_VEHICLE_FIXED(tempVehicle)
				        SET_ENTITY_HEALTH(tempVehicle, 1000)
				        SET_VEHICLE_ENGINE_HEALTH(tempVehicle, 1000)
				        SET_VEHICLE_PETROL_TANK_HEALTH(tempVehicle, 1000)
						SET_VEHICLE_DIRT_LEVEL(tempVehicle, 0.0)
		                SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(tempVehicle, TRUE)
						SET_VEHICLE_RESPECTS_LOCKS_WHEN_HAS_DRIVER(tempVehicle, TRUE)
						SET_ENTITY_CAN_BE_DAMAGED(tempVehicle, FALSE)
						SET_VEHICLE_RADIO_ENABLED(tempVehicle, FALSE)
						
						IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
							FORCE_ROOM_FOR_ENTITY(tempVehicle, m_InteriorData.iInteriorID, GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()))
						ENDIF
						
						SET_NETWORK_ID_ALWAYS_EXISTS_FOR_PLAYER(m_ServerBD.customVehicleNetIDs[i], PLAYER_ID(), FALSE)
						
						IF NETWORK_IS_IN_MP_CUTSCENE()
							SET_NETWORK_CUTSCENE_ENTITIES(FALSE)
						ENDIF
						
						PRINTLN("[MAINTAIN_VEHICLES] - CREATE_HIDEOUT_VEHICLE - Creating vehicle: ", i, " at: ", GET_STRING_FROM_VECTOR(vCoord))
						SET_MODEL_AS_NO_LONGER_NEEDED(eModel)
						
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_VEHICLE_POSITION(INT iVehIndex)
	FLOAT fDistance
	
	IF NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(m_InteriorData.pOwner)
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(m_ServerBD.customVehicleNetIDs[iVehIndex])
		AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(m_ServerBD.customVehicleNetIDs[iVehIndex])
		AND IS_ENTITY_ALIVE(NET_TO_VEH(m_ServerBD.customVehicleNetIDs[iVehIndex]))
			fDistance = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(NET_TO_VEH(m_ServerBD.customVehicleNetIDs[iVehIndex])), GET_HIDEOUT_VEHICLE_POSITION(iVehIndex), FALSE)
			
			IF fDistance > 1.0
			AND fDistance < 15.0
				SET_ENTITY_COORDS_NO_OFFSET(NET_TO_VEH(m_ServerBD.customVehicleNetIDs[iVehIndex]), GET_HIDEOUT_VEHICLE_POSITION(iVehIndex))
				SET_ENTITY_ROTATION(NET_TO_VEH(m_ServerBD.customVehicleNetIDs[iVehIndex]), <<0.0, 0.0, GET_HIDEOUT_VEHICLE_HEADING(iVehIndex)>>)
				SET_VEHICLE_ON_GROUND_PROPERLY(NET_TO_VEH(m_ServerBD.customVehicleNetIDs[iVehIndex]))
				PRINTLN("[MAINTAIN_VEHICLES] - MAINTAIN_VEHICLE_POSITION - resetting vehicle position for iVehIndex: ", iVehIndex)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles all the vehicles located in the juggalo hideout including the Acid Lab, Manchez & Journey2
PROC SERVER_MAINTAIN_VEHICLES()

	BOOL bDeleteVeh = FALSE
	
	IF m_InteriorData.pOwner = INVALID_PLAYER_INDEX()
		PRINTLN("[MAINTAIN_VEHICLES] Invalid owner.")	
		EXIT
	ENDIF
	
	IF NOT IS_NET_PLAYER_OK(m_InteriorData.pOwner)
		PRINTLN("[MAINTAIN_VEHICLES] Owner is not okay.")	
		EXIT
	ENDIF
	
	IF IS_VALID_INTERIOR(m_InteriorData.iInteriorID)
	AND NOT IS_INTERIOR_READY(m_InteriorData.iInteriorID)
		PRINTLN("[MAINTAIN_VEHICLES] Waiting for interior.")	
		EXIT
	ENDIF
	
	IF NOT IS_PLAYER_IN_JUGGALO_HIDEOUT_PROPERTY(PLAYER_ID())
		PRINTLN("[MAINTAIN_VEHICLES] Not in property yet.")	
		EXIT
	ENDIF
	
	INT i
	REPEAT MAX_SAVED_VEHS_IN_JUGGALO_HIDEOUT i
		IF GET_HIDEOUT_VEHICLE_MODEL(i) != DUMMY_MODEL_FOR_SCRIPT
		AND IS_VEHICLE_AVAILABLE_FOR_GAME(GET_HIDEOUT_VEHICLE_MODEL(i), TRUE)
			
			// CREATE VEHICLES
			IF NOT IS_OWNER_REQUESTED_HIDEOUT_VEHICLE_IN_FREEMODE(i)
				IF NOT IS_OWNERS_HIDEOUT_VEHICLE_UNDER_MAP(i)
					IF IS_OWNERS_HIDEOUT_VEHICLE_INSIDE_JUGGALO_HIDEOUT(i)
					AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(m_ServerBD.customVehicleNetIDs[i])
						PRINTLN("[MAINTAIN_VEHICLES] Vehicle not in FM - Owner = player: ", PLAYER_ID() = m_InteriorData.pOwner, " vehindex i: ", i)
						
						IF NOT IS_BIT_SET(m_ServerBD.iCarSpawnBS[i], CARSPAWN_BS_REQUEST_VEHICLE_CLONE_CREATION)
							BROADCAST_CREATE_OWNERS_HIDEOUT_VEHICLE_UNDER_MAP(i, FALSE)
							SET_BIT(m_ServerBD.iCarSpawnBS[i], CARSPAWN_BS_REQUEST_VEHICLE_CLONE_CREATION)
							PRINTLN("[MAINTAIN_VEHICLES] CARSPAWN_BS_REQUEST_VEHICLE_CLONE_CREATION TRUE for i: ", i)
						ENDIF
					ENDIF	
				ELSE
					IF SHOULD_HIDEOUT_VEHICLE_EXIST(i)
						IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(m_ServerBD.customVehicleNetIDs[i])
							IF NOT IS_BIT_SET(m_ServerBD.iCarSpawnBS[i], CARSPAWN_BS_ASSIGN_VEHICLE_TO_INTERIOR)
								IF GET_OWNERS_HIDEOUT_VEHICLE_INDEX(i, m_ServerBD.customVehicleVehIDs[i])
								OR SHOULD_SKIP_HIDEOUT_VEHICLE_INDEX(i)
									SET_BIT(m_ServerBD.iCarSpawnBS[i], CARSPAWN_BS_REQUEST_TO_MOVE_OUT_OF_WAY_FOR_VEHICLE)
									SET_BIT(m_ServerBD.iCarSpawnBS[i], CARSPAWN_BS_ASSIGN_VEHICLE_TO_INTERIOR)
									PRINTLN("[MAINTAIN_VEHICLES] CARSPAWN_BS_ASSIGN_VEHICLE_TO_INTERIOR TRUE for i: ", i)
								ENDIF	
							ELSE
								IF DOES_ENTITY_EXIST(m_ServerBD.customVehicleVehIDs[i])
								OR SHOULD_SKIP_HIDEOUT_VEHICLE_INDEX(i)
									IF CREATE_HIDEOUT_VEHICLE(i)
										CLEAR_BIT(m_ServerBD.iCarSpawnBS[i], CARSPAWN_BS_REQUEST_TO_MOVE_OUT_OF_WAY_FOR_VEHICLE)
										CLEAR_BIT(m_ServerBD.iCarSpawnBS[i], CARSPAWN_BS_ASSIGN_VEHICLE_TO_INTERIOR)
										BROADCAST_CREATE_OWNERS_HIDEOUT_VEHICLE_UNDER_MAP(i, TRUE)
										PRINTLN("[MAINTAIN_VEHICLES] CREATE_HIDEOUT_VEHICLE TRUE for i: ", i)
									ELSE
										PRINTLN("[MAINTAIN_VEHICLES] Creating clone for i: ", i, " ...")
									ENDIF
								ELIF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(m_ServerBD.customVehicleNetIDs[i])
									CLEAR_BIT(m_ServerBD.iCarSpawnBS[i], CARSPAWN_BS_ASSIGN_VEHICLE_TO_INTERIOR)
									PRINTLN("[MAINTAIN_VEHICLES] CARSPAWN_BS_ASSIGN_VEHICLE_TO_INTERIOR FALSE vehicle i: ", i, " doesn't exist")
								ELSE
									PRINTLN("[MAINTAIN_VEHICLES] m_ServerBD.customVehicleVehIDs[ ", i, "] doesn't exist")
								ENDIF	
							ENDIF
						ELSE
							MAINTAIN_VEHICLE_POSITION(i)
						ENDIF
					ELIF IS_OWNERS_HIDEOUT_VEHICLE_INSIDE_JUGGALO_HIDEOUT(i)
						IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(m_ServerBD.customVehicleNetIDs[i])
						AND m_InteriorData.pOwner = PLAYER_ID()
							IF CREATE_HIDEOUT_VEHICLE(i)
								CLEAR_BIT(m_ServerBD.iCarSpawnBS[i], CARSPAWN_BS_REQUEST_TO_MOVE_OUT_OF_WAY_FOR_VEHICLE)
								CLEAR_BIT(m_ServerBD.iCarSpawnBS[i], CARSPAWN_BS_ASSIGN_VEHICLE_TO_INTERIOR)
								PRINTLN("[MAINTAIN_VEHICLES] CREATE_HIDEOUT_VEHICLE TRUE or i: ", i)
							ENDIF
						ENDIF
					ELSE
						PRINTLN("[MAINTAIN_VEHICLES] SHOULD_HIDEOUT_VEHICLE_EXIST FALSE for i: ", i)
						bDeleteVeh = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			// DELETE VEHICLES
			IF (!IS_OWNERS_HIDEOUT_VEHICLE_INSIDE_JUGGALO_HIDEOUT(i)
			OR IS_OWNER_REQUESTED_HIDEOUT_VEHICLE_IN_FREEMODE(i))
			AND NOT SHOULD_ALL_WAREHOUSE_VEHICLES_EXIST_FOR_EXIT_CUTSCENE()
			OR bDeleteVeh
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(m_ServerBD.customVehicleNetIDs[i])
					IF IS_NET_VEHICLE_DRIVEABLE(m_ServerBD.customVehicleNetIDs[i])
						IF GET_VEHICLE_DOOR_LOCK_STATUS(NET_TO_VEH(m_ServerBD.customVehicleNetIDs[i])) != VEHICLELOCK_LOCKED
							IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(m_ServerBD.customVehicleNetIDs[i])
								PRINTLN("[MAINTAIN_VEHICLES] DELETE - Locking doors for i: ", i)
								SET_VEHICLE_DOORS_LOCKED(NET_TO_VEH(m_ServerBD.customVehicleNetIDs[i]), VEHICLELOCK_LOCKED)
							ELSE
								NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(m_ServerBD.customVehicleNetIDs[i])
							ENDIF
						ENDIF
						
						IF NOT HAS_NET_TIMER_STARTED(m_InteriorData.failSafeClearVehicleDelay[i])
							START_NET_TIMER(m_InteriorData.failSafeClearVehicleDelay[i], TRUE)
						ENDIF
						
						IF IS_VEHICLE_EMPTY(NET_TO_VEH(m_ServerBD.customVehicleNetIDs[i]), TRUE, TRUE)
						OR HAS_NET_TIMER_EXPIRED(m_InteriorData.failSafeClearVehicleDelay[i], 3000, TRUE)
							IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(m_ServerBD.customVehicleNetIDs[i])
								IF NOT NETWORK_IS_ENTITY_FADING(NET_TO_VEH(m_ServerBD.customVehicleNetIDs[i]))
								AND IS_BIT_SET(m_ServerBD.iCarSpawnBS[i], CARSPAWN_BS_FADE_OUT_HIDEOUT_VEHICLE)
									DELETE_NET_ID(m_ServerBD.customVehicleNetIDs[i])
									RESERVE_LOCAL_NETWORK_MISSION_VEHICLES(GET_NUM_RESERVED_MISSION_VEHICLES(FALSE, RESERVATION_LOCAL_ONLY) - 1)
									m_ServerBD.iReserved--
									CLEAR_BIT(m_ServerBD.iCarSpawnBS[i], CARSPAWN_BS_RESERVE_HIDEOUT_VEHICLE_INDEX)
									CLEAR_BIT(m_ServerBD.iCarSpawnBS[i], CARSPAWN_BS_FADE_OUT_HIDEOUT_VEHICLE)
									PRINTLN("[MAINTAIN_VEHICLES] DELETE - 1 Deleted vehicle for i: ", i)
								ELSE
									IF NOT IS_BIT_SET(m_ServerBD.iCarSpawnBS[i], CARSPAWN_BS_FADE_OUT_HIDEOUT_VEHICLE)
										NETWORK_FADE_OUT_ENTITY(NET_TO_VEH(m_ServerBD.customVehicleNetIDs[i]), FALSE, TRUE)
										SET_BIT(m_ServerBD.iCarSpawnBS[i], CARSPAWN_BS_FADE_OUT_HIDEOUT_VEHICLE)
									ENDIF
								ENDIF
							ELSE
								NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(m_ServerBD.customVehicleNetIDs[i])
							ENDIF
						ELSE
							IF IS_NET_PLAYER_OK(PLAYER_ID())
								IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(m_ServerBD.customVehicleNetIDs[i]))
									PRINTLN("[MAINTAIN_VEHICLES] DELETE - Leaving vehicle for i: ", i)
									TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID(), 0, ECF_WARP_IF_DOOR_IS_BLOCKED)
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(m_ServerBD.customVehicleNetIDs[i])
							DELETE_NET_ID(m_ServerBD.customVehicleNetIDs[i])
							RESERVE_LOCAL_NETWORK_MISSION_VEHICLES(GET_NUM_RESERVED_MISSION_VEHICLES(FALSE, RESERVATION_LOCAL_ONLY) - 1)
							m_ServerBD.iReserved--
							CLEAR_BIT(m_ServerBD.iCarSpawnBS[i], CARSPAWN_BS_RESERVE_HIDEOUT_VEHICLE_INDEX)
							PRINTLN("[MAINTAIN_VEHICLES] DELETE - 2 Deleted vehicle for i: ", i)
						ELSE
							NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(m_ServerBD.customVehicleNetIDs[i])
						ENDIF
					ENDIF
				ELSE
					RESET_NET_TIMER(m_InteriorData.failSafeClearVehicleDelay[i])
				ENDIF
			ENDIF

		ELSE
			PRINTLN("[MAINTAIN_VEHICLES] Delete veh i: ", i, " Available in game? ", IS_VEHICLE_AVAILABLE_FOR_GAME(GET_HIDEOUT_VEHICLE_MODEL(i), TRUE))
		ENDIF
	ENDREPEAT
ENDPROC

FUNC BOOL MAINTAIN_LAUNCHING_CARMOD_SCRIPT()
	
	IF m_InteriorData.pOwner != INVALID_PLAYER_INDEX()

		//Make sure the car mod script is not started too early after being killed
		//Make sure the car mod script cleanup flag is false
		IF g_bCleanUpCarmodShop = TRUE
			IF NOT HAS_NET_TIMER_STARTED(m_InteriorData.sCarModScriptRunTimer)
				START_NET_TIMER(m_InteriorData.sCarModScriptRunTimer)
			ELSE
				IF HAS_NET_TIMER_EXPIRED(m_InteriorData.sCarModScriptRunTimer, 8000)
					g_bCleanUpCarmodShop = FALSE
					RESET_NET_TIMER(m_InteriorData.sCarModScriptRunTimer)
					#IF IS_DEBUG_BUILD
					PRINTLN("MAINTAIN_LAUNCHING_CARMOD_SCRIPT - Setting g_bCleanUpCarmodShop To FALSE")
					#ENDIF
				ENDIF	
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(m_InteriorData.iBS, BS_JUGGALO_IS_CAR_MOD_SCRIPT_READY)
			m_InteriorData.sChildOfChildScript = "juggalo_hideout_carmod"
			REQUEST_SCRIPT(m_InteriorData.sChildOfChildScript)
			IF HAS_SCRIPT_LOADED(m_InteriorData.sChildOfChildScript)
			AND NOT IS_THREAD_ACTIVE(m_InteriorData.CarModThread)
			AND !g_bCleanUpCarmodShop

				g_iCarModInstance = m_InteriorData.iScriptInstance
				
				IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(m_InteriorData.sChildOfChildScript)) < 1
					IF NOT NETWORK_IS_SCRIPT_ACTIVE(m_InteriorData.sChildOfChildScript, g_iCarModInstance, TRUE)
						SHOP_LAUNCHER_STRUCT sShopLauncherData
						sShopLauncherData.bLinkedShop = FALSE
						sShopLauncherData.eShop = CARMOD_SHOP_PERSONALMOD
						sShopLauncherData.ePersonalCarModVariation = PERSONAL_CAR_MOD_VARIATION_JUGGALO_HIDEOUT
						
						g_iPersonalCarModVariation = ENUM_TO_INT(sShopLauncherData.ePersonalCarModVariation)
						m_InteriorData.CarModThread = START_NEW_SCRIPT_WITH_ARGS(m_InteriorData.sChildOfChildScript, sShopLauncherData, SIZE_OF(sShopLauncherData), CAR_MOD_SHOP_STACK_SIZE)
						SET_SCRIPT_AS_NO_LONGER_NEEDED(m_InteriorData.sChildOfChildScript)
						SET_BIT(m_InteriorData.iBS, BS_JUGGALO_IS_CAR_MOD_SCRIPT_READY)
						g_sShopSettings.bShopScriptLaunched[ENUM_TO_INT(CARMOD_SHOP_PERSONALMOD)] = TRUE
						g_sShopSettings.bShopScriptLaunchedInMP[ENUM_TO_INT(CARMOD_SHOP_PERSONALMOD)] = NETWORK_IS_GAME_IN_PROGRESS()
						PRINTLN("MAINTAIN_LAUNCHING_CARMOD_SCRIPT - TRUE sShopLauncherData.ePersonalCarModVariation: ", sShopLauncherData.ePersonalCarModVariation)
						RETURN TRUE
					ELSE
						#IF IS_DEBUG_BUILD
							PRINTLN("MAINTAIN_LAUNCHING_CARMOD_SCRIPT: Unable to start shop script for juggalo_hideout_carmod - last instance still active")
						#ENDIF					
					ENDIF
				ELSE
					PRINTLN("MAINTAIN_LAUNCHING_CARMOD_SCRIPT - carmod is already running")
					RETURN TRUE 
				ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
				CLEAN_UP_PERSONAL_CAR_MOD()
				TERMINATE_THREAD(m_InteriorData.CarModThread)
			ENDIF
			#ENDIF
		
		RETURN TRUE
		ENDIF
	ELSE
		PRINTLN("MAINTAIN_LAUNCHING_CARMOD_SCRIPT - m_InteriorData.pOwner is invalid")
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTLN("MAINTAIN_LAUNCHING_CARMOD_SCRIPT - return false")
	#ENDIF
	RETURN FALSE
ENDFUNC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════╡ MAIN LOGIC PROC ╞══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC RUN_MAIN_CLIENT_LOGIC()
	// NOTE: Only update procedures should be called from 
	// within RUN_MAIN_CLIENT_LOGIC. Custom logic should be 
	// placed inside of its own procedure and referenced here.

	MAINTAIN_PLAYER_FLAGS()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_PLAYER_FLAGS")
	#ENDIF	
	#ENDIF
	
	MAINTAIN_LAUNCHING_CARMOD_SCRIPT()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_LAUNCHING_CARMOD_SCRIPT")
	#ENDIF	
	#ENDIF
	
	//Staff peds
	MAINTAIN_PED_SCRIPT_LAUNCHING(PED_LOCATION_JUGGALO_HIDEOUT, m_InteriorData.bPedScriptLaunched, m_InteriorData.iScriptInstance)
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_PED_SCRIPT_LAUNCHING")
	#ENDIF
	#ENDIF
	
	IF IS_JUGGALO_HIDEOUT_STATE(JUGGALO_HIDEOUT_STATE_LOAD_INTERIOR)
		
		LOAD_INTERIOR()
	
	ELIF IS_JUGGALO_HIDEOUT_STATE(JUGGALO_HIDEOUT_STATE_IDLE)
		
		RUN_MINIGAME_LAUNCHER()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("RUN_MINIGAME_LAUNCHER")
		#ENDIF
		#ENDIF
		
		UPDATE_HIDEOUT_GLOBAL_VEHICLE_INDEX()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("UPDATE_HIDEOUT_GLOBAL_VEHICLE_INDEX")
		#ENDIF
		#ENDIF
		
		MAINTAIN_CONTROL_OF_HIDEOUT_PERSONAL_CARMOD_VEHICLES()
		UPDATE_HIDEOUT_GLOBAL_VEHICLE_INDEX()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_CONTROL_OF_HIDEOUT_PERSONAL_CARMOD_VEHICLES")
		#ENDIF
		#ENDIF
		
		MAINTAIN_JUGGALO_HIDEOUT_INTERIOR_BLIPS()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_JUGGALO_HIDEOUT_INTERIOR_BLIPS")
		#ENDIF
		#ENDIF
		
		IF NETWORK_IS_ACTIVITY_SESSION()
			
			//Only update logic for an activity session.
		
		ELSE
			
			IF NOT IS_BIT_SET(m_InteriorData.iBS, BS_JUGGALO_HIDEOUT_CALLED_CLEAR_HELP)
				
				IF NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
				AND IS_PLAYER_IN_JUGGALO_HIDEOUT_PROPERTY(PLAYER_ID())
				//AND NOT SHOULD_KEEP_EXTERIOR_CAM_FOR_THIS_SIMPLE_INTERIOR(m_InteriorData.eSimpleInteriorID)
					IF IS_SCREEN_FADED_IN()
						CLEAR_HELP()
						SET_BIT(m_InteriorData.iBS, BS_JUGGALO_HIDEOUT_CALLED_CLEAR_HELP)
						PRINTLN("AM_MP_JUGGALO_HIDEOUT - Calling clear help")
					ENDIF
				ENDIF
			ENDIF
			
			MAINTAIN_JH_CCTV()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_JH_CCTV")
			#ENDIF
			#ENDIF
			
			//Add functions that should run while in the interior here
			//MAINTAIN_MOVIE_PROP_MODEL_HIDES()
		ENDIF
	ENDIF
ENDPROC

PROC RUN_MAIN_SERVER_LOGIC()
	
	IF IS_JUGGALO_HIDEOUT_SERVER_STATE(JUGGALO_HIDEOUT_SERVER_STATE_LOADING)
		SET_JUGGALO_HIDEOUT_SERVER_STATE(JUGGALO_HIDEOUT_SERVER_STATE_IDLE)
	ELIF IS_JUGGALO_HIDEOUT_SERVER_STATE(JUGGALO_HIDEOUT_SERVER_STATE_IDLE)
	
		SERVER_MAINTAIN_VEHICLES()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("SERVER_MAINTAIN_VEHICLES")
		#ENDIF
		#ENDIF
		
	ENDIF
ENDPROC
#ENDIF // FEATURE_DLC_2_2022

SCRIPT #IF FEATURE_DLC_2_2022 (SIMPLE_INTERIOR_CHILD_SCRIPT_DATA scriptData) #ENDIF
	
	#IF FEATURE_DLC_2_2022
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		SCRIPT_INITIALISE(scriptData)
	ELSE
		SCRIPT_CLEANUP()
	ENDIF
	
	WHILE TRUE
		MP_LOOP_WAIT_ZERO()
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		SCRIPT_PROFILER_START_OF_FRAME() 
		#ENDIF
		#ENDIF
		
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_JUGGALO_HIDEOUT - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE = TRUE")
			#ENDIF
			SCRIPT_CLEANUP()
		ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE")
		#ENDIF	
		#ENDIF
		
		IF SHOULD_THIS_SIMPLE_INTERIOR_CHILD_THREAD_TERMINATE(scriptData.iScriptInstance, scriptData.eSimpleInteriorID)
			#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_JUGGALO_HIDEOUT - SHOULD_THIS_SIMPLE_INTERIOR_CHILD_THREAD_TERMINATE = TRUE")
			#ENDIF
			SCRIPT_CLEANUP()
		ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("SHOULD_THIS_SIMPLE_INTERIOR_CHILD_THREAD_TERMINATE")
		#ENDIF	
		#ENDIF
		
		IF NETWORK_GET_HOST_OF_THIS_SCRIPT() != INT_TO_NATIVE(PARTICIPANT_INDEX , -1)
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iSimpleInteriorHost = NATIVE_TO_INT(NETWORK_GET_PLAYER_INDEX(NETWORK_GET_HOST_OF_THIS_SCRIPT()))
		ENDIF
		
		MAINTAIN_GET_OUT_OF_THE_WAY_JUGGALO_WAREHOUSE_WRAPPER()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_GET_OUT_OF_THE_WAY_JUGGALO_WAREHOUSE_WRAPPER")
		#ENDIF
		#ENDIF
		
		IF IS_JUGGALO_HIDEOUT_SERVER_STATE(JUGGALO_HIDEOUT_SERVER_STATE_IDLE)
			RUN_MAIN_CLIENT_LOGIC()
		ENDIF		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("RUN_MAIN_CLIENT_LOGIC")
		#ENDIF	
		#ENDIF
		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			RUN_MAIN_SERVER_LOGIC()
		ENDIF			
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("RUN_MAIN_SERVER_LOGIC")
		#ENDIF	
		#ENDIF
		
		#IF IS_DEBUG_BUILD
//		UPDATE_DEBUG_WIDGETS()
		
		#IF SCRIPT_PROFILER_ACTIVE
		SCRIPT_PROFILER_END_OF_FRAME() 
		#ENDIF
		
		#ENDIF
		
	ENDWHILE
	#ENDIF	// FEATURE_DLC_2_2022
	
ENDSCRIPT
