//AM_PRISON
//Prison response
//Author: Barton Slade
//Date: 7/3/2013
USING "globals.sch"

// Game Headers
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_brains.sch"

// Network Headers
USING "net_include.sch"
USING "net_rank_ui.sch"
USING "net_hud_displays.sch"
USING "net_weaponswap.sch"
USING "minigame_uiinputs.sch"
USING "freemode_header.sch"
USING "FM_Unlocks_Header.sch"
USING "context_control_public.sch"

USING "area_checks.sch"
USING "freemode_events_header.sch"
USING "mp_globals_fm.sch"

USING "net_wait_zero.sch"

#IF IS_DEBUG_BUILD
USING "net_debug_log.sch"
#ENDIF

CONST_FLOAT PRISON_SHUTDOWN_DIST		1030225.0 //1015*1015
CONST_FLOAT ARMYBASE_FLYBY_TIME			20.0

ENUM PRISON_HOST_UPDATE_STATE
	PH_UPDATE_INVALID = -1,
	PH_UPDATE_INIT,
	PH_UPDATE_WAIT,
	PH_UPDATE_CLEANUP
ENDENUM

ENUM PRISON_CLIENT_UPDATE_STATE
	PC_UPDATE_INVALID = -1,
	PC_UPDATE_INIT,
	PC_UPDATE_WAIT,
	PC_UPDATE_CLEANUP
ENDENUM

ENUM PRISON_HOST_FLAGS
	PHF_PEDS_CREATED = BIT0
ENDENUM

ENUM PRISON_PLAYER_FLAGS
	PPF_ASSETS_REQUESTED = BIT0,
	PPF_ASSETS_LOADED = BIT1
ENDENUM

STRUCT PRISON_SERVER_BROADCAST_DATA
	PRISON_HOST_UPDATE_STATE	hostState
	INT iHostFlags
ENDSTRUCT

STRUCT PRISON_PLAYER_BROADCAST_DATA
	INT iPlayerFlags
ENDSTRUCT

STRUCT PRISON_LOCAL_DATA
	PRISON_CLIENT_UPDATE_STATE clientState
	structTimer	tmrFlyBy
	SCENARIO_BLOCKING_INDEX sbiPrison
ENDSTRUCT

PRISON_SERVER_BROADCAST_DATA prisonServerBD
PRISON_PLAYER_BROADCAST_DATA prisonPlayerBD[NUM_NETWORK_PLAYERS]
PRISON_LOCAL_DATA			 prisonLocalD

FUNC BOOL P_HOST_CAN_CLEANUP()
	IF NETWORK_GET_NUM_PARTICIPANTS() >	1
		RETURN FALSE
	ENDIF
	
	IF VDIST2( GET_ENTITY_COORDS( PLAYER_PED_ID(), FALSE ), <<1693.1636, 2559.6162, 44.5649>> ) >= PRISON_SHUTDOWN_DIST
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

PROC CLEANUP_SCRIPT()
	
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_NET_AMBIENT, "=== PRISON === Ending Script.")
	#ENDIF
	
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

/// PURPOSE:
///    Do necessary pre game start ini.
PROC PROCESS_PRE_GAME(MP_MISSION_DATA missionScriptArgs)
	
	// This marks the script as a net script, and handles any instancing setup. 
	SETUP_MP_MISSION_FOR_NETWORK(GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(missionScriptArgs.mdID.idMission) , missionScriptArgs)
	
	// This makes sure the net script is active, waits until it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	// Reserving peds for the script
	//RESERVE_NETWORK_MISSION_PEDS(ARMYBASE_NUM_GUARDS + ARMYBASE_MAX_NUM_PLANES)
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(prisonServerBD, SIZE_OF(prisonServerBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(prisonPlayerBD, SIZE_OF(prisonPlayerBD))
	
	// This script will not be paused if another script calls PAUSE_GAME
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	
	IF NOT Wait_For_First_Network_Broadcast()
		PRINTLN("Failed to receive initial broadcast data for Prison MP. Cleaning up.")
		CLEANUP_SCRIPT()
	ENDIF
	
	IF HAS_FORCE_CLEANUP_OCCURRED()
		PRINTLN("Force cleanup of Prison MP")
		CLEANUP_SCRIPT()
	ENDIF
ENDPROC

FUNC BOOL UPDATE_NET_PRISON_HOST()
	SWITCH prisonServerBD.hostState
		CASE PH_UPDATE_INIT
			CDEBUG1LN( DEBUG_NET_AMBIENT, "UPDATE_NET_PRISON_HOST - Switching from PH_UPDATE_INIT to PH_UPDATE_WAIT" )
			prisonServerBD.hostState = PH_UPDATE_WAIT
		BREAK
		CASE PH_UPDATE_WAIT
			
			IF P_HOST_CAN_CLEANUP()
				CDEBUG1LN( DEBUG_NET_AMBIENT, "UPDATE_NET_PRISON_HOST - Switching from PH_UPDATE_WAIT to PH_CLEANUP" )
				prisonServerBD.hostState = PH_UPDATE_CLEANUP
			ENDIF
		BREAK
		CASE PH_UPDATE_CLEANUP
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL UPDATE_NET_PRISON_CLIENT()
	SWITCH prisonLocalD.clientState
		CASE PC_UPDATE_INIT
			IF g_bDeactivateRestrictedAreas OR IS_AREA_RESTRICTED( eRestrictedArea_Prison )
				prisonLocalD.sbiPrison = ADD_SCENARIO_BLOCKING_AREA(<<1413.7028, 2341.5168, -232.3945>>, <<2016.2095, 2998.8931, 161.0299>>)
			ENDIF
		
			SET_BITMASK_AS_ENUM( prisonPlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerFlags, PPF_ASSETS_REQUESTED )
			CDEBUG1LN( DEBUG_NET_AMBIENT, "UPDATE_NET_PRISON_CLIENT - Switching from PC_UPDATE_INIT to PC_UPDATE_WAIT_FOR_ASSETS" )
			prisonLocalD.clientState = PC_UPDATE_WAIT
		BREAK
		CASE PC_UPDATE_WAIT
			IF VDIST2( GET_ENTITY_COORDS( PLAYER_PED_ID(), FALSE ), <<1693.1636, 2559.6162, 44.5649>> ) >= PRISON_SHUTDOWN_DIST
				CDEBUG1LN( DEBUG_NET_AMBIENT, "UPDATE_NET_PRISON_CLIENT - Switching from PC_UPDATE_WAIT to PC_UPDATE_CLEANUP" )
				prisonLocalD.clientState = PC_UPDATE_CLEANUP
			ENDIF
		BREAK
		CASE PC_UPDATE_CLEANUP	
			IF g_bDeactivateRestrictedAreas OR IS_AREA_RESTRICTED( eRestrictedArea_Prison )
				REMOVE_SCENARIO_BLOCKING_AREA(prisonLocalD.sbiPrison)
			ENDIF
			
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL NET_PRISON_UPDATE()
	BOOL bRetVal = FALSE
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF UPDATE_NET_PRISON_HOST()
			bRetVal = TRUE
		ENDIF
	ENDIF
	
	IF UPDATE_NET_PRISON_CLIENT()
		bRetVal = TRUE
	ENDIF
	
	RETURN bRetVal
ENDFUNC

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                      Main Loop                             ////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
SCRIPT(MP_MISSION_DATA missionScriptArgs) 
	// Carry out all the initial game starting duties. 
	IF NETWORK_IS_GAME_IN_PROGRESS()
		PROCESS_PRE_GAME(missionScriptArgs)
	ELSE
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "=== PRISON === LAUNCH FAIL TIME IS ", GET_CLOCK_HOURS())
		#ENDIF
		CLEANUP_SCRIPT()
	ENDIF
	
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_NET_AMBIENT, "=== PRISON === Started Script!")
	
	#ENDIF
	// Main loop.
	WHILE TRUE

		// One wait to rule them all. This can be the ONLY wait from here on in.....
		MP_LOOP_WAIT_ZERO()
		
		//CLIENT_MAINTAIN_RESERVATIONS()
		
		// If we have a match end event, bail.
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		OR NETWORK_IS_IN_TUTORIAL_SESSION()
			CLEANUP_SCRIPT()
		ENDIF
		
		IF NETWORK_IS_GAME_IN_PROGRESS()	//is network game
			IF NET_PRISON_UPDATE()
				CLEANUP_SCRIPT()
			ENDIF
			
			IF NOT IS_LOCAL_PLAYER_ON_ANY_FM_MISSION() OR IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MISSION)
				IF NOT g_bDeactivateRestrictedAreas AND NOT IS_AREA_RESTRICTED( eRestrictedArea_Prison )
				AND NOT IS_PLAYER_SCTV(PLAYER_ID())
				AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
				AND IS_PLAYER_CONTROL_ON(PLAYER_ID())
				AND NOT IS_PLAYER_SPECTATING(PLAYER_ID())
				AND NOT SHOULD_BLOCK_WANTED_OVERRIDE_BASED_ON_TYPE(FM_EVENT_GET_PLAYER_CURRENT_FM_EVENT(PLAYER_ID())) 
				AND NOT IS_FM_PLAYER_CRITICAL_TO_THIS_FM_EVENT(PLAYER_ID(), FMMC_TYPE_HUNT_THE_BEAST) 
				AND NOT GB_IS_PLAYER_ON_CONTRABAND_MISSION(PLAYER_ID())
					IF IS_COORD_IN_SPECIFIED_AREA( GET_ENTITY_COORDS( PLAYER_PED_ID(), FALSE ), AC_PRISON, 100 )
						IF NOT IS_ENTITY_DEAD( PLAYER_PED_ID() )
							IF IS_PED_IN_ANY_PLANE(PLAYER_PED_ID())
								OR IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
								IF NOT IS_TIMER_STARTED(prisonLocalD.tmrFlyBy)
									START_TIMER_NOW(prisonLocalD.tmrFlyBy)
								ELIF GET_TIMER_IN_SECONDS(prisonLocalD.tmrFlyBy) > ARMYBASE_FLYBY_TIME
									IF GET_PLAYER_WANTED_LEVEL( PLAYER_ID() ) < 4
										REPORT_CRIME( PLAYER_ID(), CRIME_TERRORIST_ACTIVITY, GET_WANTED_LEVEL_THRESHOLD(4) - GET_WANTED_LEVEL_THRESHOLD( GET_PLAYER_WANTED_LEVEL( PLAYER_ID() ) ))
									ELSE
										REPORT_POLICE_SPOTTED_PLAYER( PLAYER_ID() )
									ENDIF
								ENDIF
							ELSE
								IF GET_PLAYER_WANTED_LEVEL( PLAYER_ID() ) < 4
									REPORT_CRIME( PLAYER_ID(), CRIME_TERRORIST_ACTIVITY, GET_WANTED_LEVEL_THRESHOLD(4) - GET_WANTED_LEVEL_THRESHOLD( GET_PLAYER_WANTED_LEVEL( PLAYER_ID() ) ))
								ELSE
									REPORT_POLICE_SPOTTED_PLAYER( PLAYER_ID() )
								ENDIF
							ENDIF
						ELSE
							IF IS_TIMER_STARTED(prisonLocalD.tmrFlyBy)
								CANCEL_TIMER(prisonLocalD.tmrFlyBy)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			CLEANUP_SCRIPT()
		ENDIF
	ENDWHILE
ENDSCRIPT

