//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        AM_MP_PROPERTY_EXT.sc																		//
// Description: Controls entrance to purchased properties in MP												//
// Written by:  Conor McGuire																				//
// Date: 2013-03-12 (ISO 8601) 																				//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
// Game Headers
USING "commands_network.sch"
USING "net_include.sch"
USING "context_control_public.sch"
USING "net_ambience.sch"
USING "net_spawn.sch"
USING "net_nodes.sch"
USING "fmmc_launcher_menu.sch"
USING "net_saved_vehicles.sch"
USING "net_realty_details.sch"
USING "net_MP_TV.sch"
USING "net_MP_Radio.sch"
USING "net_freemode_cut.sch"
USING "net_realty_new.sch"

USING "minigames_helpers.sch"
USING "net_doors.sch"

USING "net_realty_1_scene_public.sch"		//WALK_INTO_PROPERTY_CUTSCENE()
USING "net_realty_2_scene_public.sch"		//DO_GARAGE_PED_ENTER_CUTSCENE(FALSE)
USING "net_realty_3_scene_public.sch"		//DO_GARAGE_CAR_ENTER_CUTSCENE(FALSE)

USING "net_realty_4_scene_public.sch"		//WALK_OUT_OF_PROPERTY()
USING "net_realty_5_scene_public.sch"		//DO_GARAGE_PED_LEAVE_CUTSCENE()
USING "net_realty_6_scene_public.sch"		//DO_GARAGE_CAR_LEAVE_CUTSCENE()

USING "net_wait_zero.sch"

USING "freemode_events_header.sch"

USING "net_office.sch"


STRUCT PED_COMPONENT_STRUCT
	INT NewDrawableNumber = 0
	INT NewTextureNumber = 0
	INT NewPaletteNumber = 0
ENDSTRUCT

STRUCT PED_PROP_STRUCT
	INT iPropIndex = -1
	INT iTexIndex = 0
ENDSTRUCT

STRUCT PED_DATA
	MODEL_NAMES mnPedModel
	PED_COMPONENT_STRUCT pcsComponents[NUM_PED_COMPONENTS]
	PED_PROP_STRUCT ppsProps[NUM_PED_PROPS]
	
	VECTOR vPos
	VECTOR vRot
	STRING sAnimDict
	STRING sAnimClip
ENDSTRUCT



CONST_INT STAGE_INIT								0
CONST_INT STAGE_CHECK_FOR_IN_LOCATE					1 
CONST_INT STAGE_SETUP_FOR_MENU						2
CONST_INT STAGE_USING_MENU							3
//CONST_INT STAGE_INTRO_IF_APPLICABLE				3
CONST_INT STAGE_SETUP_FOR_USING_PROPERTY			4
CONST_INT STAGE_ENTER_PROPERTY						5
CONST_INT STAGE_LEAVE_ENTRANCE_AREA					6
CONST_INT STAGE_BUZZED_INTO_PROPERTY				7
CONST_INT STAGE_WARP_TO_ENTRANCE_AREA_THEN_LEAVE 	8

CONST_INT MENU_TYPE_PURCHASE				0
CONST_INT MENU_TYPE_BUZZER					1
CONST_INT MENU_TYPE_AUTH_ENTRY				2
CONST_INT MENU_TYPE_REPLACE_PROPERTY		3
CONST_INT MENU_TYPE_PROPERTY_ENTRY_OPTIONS	4
CONST_INT MENU_TYPE_SELECT_CUSTOM_INTERIOR	5

CONST_INT LOCAL_BS_STARTED_FADE					0
CONST_INT LOCAL_BS_SKIP_ENTER_FADE				1
CONST_INT LOCAL_BS_CLEANUP_MP_PROPERTIES		2
CONST_INT LOCAL_BS_NO_FADE						3
CONST_INT LOCAL_BS_STARTED_CUTSCENE				4
CONST_INT LOCAL_BS_SETUP_MENU					5
CONST_INT LOCAL_BS_TAKEN_AWAY_CONTROL			6
CONST_INT LOCAL_BS_WARPED_IN_GARAGE				7
CONST_INT LOCAL_BS_WANTED_HELP_TEXT				8
//CONST_INT LOCAL_BS_SETUP_SPECIFIC_SPAWN_POS	9
CONST_INT LOCAL_BS_JUST_BOUGHT_PROPERTY			10
CONST_INT LOCAL_BS_INVALID_VEHICLE_HELP			11
CONST_INT LOCAL_BS_AWAITING_BUZZER_RESPONSE		12
CONST_INT LOCAL_BS_ENTER_NO_WARP				13
CONST_INT LOCAL_BS_ENTER_INGORE_PLAYER_OK		14
CONST_INT LOCAL_BS_USING_BUZZER_MENU			15
CONST_INT LOCAL_BS_DELETED_ENTERING_VEHICLE		16
CONST_INT LOCAL_BS_ENTER_FOOT_PUT_CAR_IN_GAR	17
CONST_INT LOCAL_BS_GARAGE_COMPLETELY_FULL		18
CONST_INT LOCAL_BS_BUZZER_REQUESTED_ANIM		19
CONST_INT LOCAL_BS_BUZZER_GIVE_GOTO_TASK		20
CONST_INT LOCAL_BS_BUZZER_GIVE_RING_TASK		21
CONST_INT LOCAL_BS_BUZZER_TRIGGERED_LOAD_SCENE	22
CONST_INT LOCAL_BS_PURCHASE_MENU_REQUEST_SF		23
CONST_INT LOCAL_BS_PURCHASE_MENU_SHOWING_WARN	24
CONST_INT LOCAL_BS_STOP_CUSTOM_MENU_CHECK		25
CONST_INT LOCAL_BS_FORCE_TERMINATED_INTERNET	26
CONST_INT LOCAL_BS_PURCHASED_CUSTOM_APT			27
CONST_INT LOCAL_BS_ASSOCIATE_DENIED				28

CONST_INT ENTRY_CS_STAGE_INIT_DATA				0
CONST_INT ENTRY_CS_STAGE_WAIT_FOR_MODELS		1	
CONST_INT ENTRY_CS_STAGE_TRIGGER_CUT			2
CONST_INT ENTRY_CS_STAGE_RUN_CUT				3
CONST_INT ENTRY_CS_STAGE_RUN_SECOND_CUT			4
CONST_INT ENTRY_CS_STAGE_HANDLE_PLAYER_PED		5

CONST_INT ENTRY_CS_BS_GET_DATA					0
CONST_INT ENTRY_CS_BS_STARTED_MP_CUTSCENE		1
CONST_INT ENTRY_CS_BS_TOOK_AWAY_CONTROL			2
CONST_INT ENTRY_CS_BS_SECOND_CUT				3
CONST_INT ENTRY_CS_BS_APT_DOOR_MODEL_HIDE		4
CONST_INT ENTRY_CS_BS_STARTED_FAKE_CUT			5

ENUM ENTRY_CUTSCENE_TYPE
	ENTRY_CUTSCENE_TYPE_BUZZER,
	ENTRY_CUTSCENE_TYPE_FRONT_DOOR
ENDENUM

ENUM SYNC_SCENE_ELEMENTS
	SYNC_SCENE_PED_A,
	SYNC_SCENE_DOOR,
	SYNC_SCENE_CAM,
	SYNC_SCENE_MAX_ELEMENTS
ENDENUM

//PROPERTY_TYPE_FOR_IS_PLAYER_BLOCKED_FROM_PROPERTY ePropertyType = PROPERTY_STANDARD

STRUCT ENTRY_CUTSCENE_DATA
	PED_INDEX playerClone
	
	OBJECT_INDEX objIDs[2]
	CAMERA_INDEX cameras[2]
	SCRIPT_TIMER timer
	INT iStage
	INT iSyncedSceneID = -1
	INT iBS
	
	STRING animDictionary
	STRING animName[SYNC_SCENE_MAX_ELEMENTS]
	
	VECTOR vCamLoc[2]
	VECTOR vCamRot[2]
	FLOAT fCamFOV[2]
	
	VECTOR vSyncSceneLoc
	VECTOR vSyncSceneRot
	
	FLOAT fSyncedSceneCompleteStage
	
	INT iSoundID
ENDSTRUCT
ENTRY_CUTSCENE_DATA entryCutData

GAMER_HANDLE heistLeader
#IF IS_DEBUG_BUILD
BOOL bBypassChecksAllowHeistEntry
BOOL bSetFakeHeistData
#ENDIF

BOOL bHadMissionCriticalEntityOnDropoff

BOOL bUseNewEntryAnims

MP_PROP_OFFSET_STRUCT tempPropOffset
MP_CAM_OFFSET_STRUCT tempCamOffset

CONST_INT NOT_ALLOWED_ACCESS_ON_VEHICLE_NOT_PERSONAL	1

FLOAT fGroundHeightWalk[MAX_ENTRANCES]

CONST_INT WIS_INIT						0
CONST_INT WIS_WAIT_FOR_DOOR_CONTROL		1
CONST_INT WIS_GIVE_SEQUENCE				2
CONST_INT WIS_WAIT_FOR_SEQUENCE_END		3
CONST_INT WIS_WAIT_FOR_LOAD				4
CONST_INT WIS_CLEANUP					5
INT iWalkInStage = WIS_INIT

CONST_INT WALK_IN_TIMEOUT	10000
SCRIPT_TIMER iWalkInTimer

SCRIPT_TIMER iCameraTimer

SCRIPT_TIMER pauseMenuDelay

SCRIPT_TIMER menuInputDelay
INT iMenuInputDelay

INT iCurrentMenuType
INT iLocalStage
INT iLocalBS

INT iCurrentInteriorVariationTooPurchase	= -1
INT iCustomApartmentTooPurchase				= -1
INT iAvailable_Interior_var_count 			= 0
BOOL bInteriorToPurchaseSelected			= FALSE
CONST_INT MAX_INTERIOR_VARIATIONS			9
INT ciPI_TYPE_M2_APT_VAR_AVAILABLE_OPTIONS[MAX_INTERIOR_VARIATIONS]

CONST_INT LOCAL_BS2_bCleanupOldPersonalVehicle		0
CONST_INT LOCAL_BS2_bClearedHeistPanCutscene		1
CONST_INT LOCAL_BS2_bKeepWantedFlag					2
CONST_INT LOCAL_BS2_bResetOnCallForTransition		3
CONST_INT LOCAL_BS2_bKeepScriptCameras				4
CONST_INT LOCAL_BS2_bEnteringAnotherPlayerProperty	5
CONST_INT LOCAL_BS2_bFadeOutWarpIntoGarage			6 //Bug 1615873 (this doesn't work so commenting out)
CONST_INT LOCAL_BS2_bCarEntryAborted				7
CONST_INT LOCAL_BS2_bKeepPurchaseContext			8
CONST_INT LOCAL_BS2_bWalkInTriggered				9
CONST_INT LOCAL_BS2_PlayerInVehicleDoingHeistDelivery	10
//CONST_INT LOCAL_BS2_MadeVehicleIndestructable		11
CONST_INT LOCAL_BS2_TriggeredAcceptWarning			12

CONST_INT LOCAL_BS2_bRunPropertySelectMenu			13
CONST_INT LOCAL_BS2_loadedGarageWalkInAnim			14
CONST_INT LOCAL_BS2_AddedEnteringDecorator			15
CONST_INT LOCAL_BS2_WarpAndLeavePointsAdded			16
CONST_INT LOCAL_BS2_GroupAccessAllowed				17
CONST_INT LOCAL_BS2_DoNotBuzzOthers				18
CONST_INT LOCAL_BS2_KeepInvalidText				19
CONST_INT LOCAL_BS2_PlayerWalkingOut				20
CONST_INT LOCAL_BS2_SetEntryData					21
CONST_INT LOCAL_BS2_DisabledRadar					22
CONST_INT LOCAL_BS2_FakeGarageReverse				23
CONST_INT LOCAL_BS2_VehBelongsToOtherGarage		24
CONST_INT LOCAL_BS2_VehBelongsToStorage			25
CONST_INT LOCAL_BS2_LockCarOnMenu					26
CONST_INT LOCAL_BS2_RemoveStickyBombs				27
CONST_INT LOCAL_BS2_ShapeTestRunning				28
CONST_INT LOCAL_BS2_DisabledStore					29

BOOL bLeavingEntranceArea

INT iLocalBS2


VECTOR vWorldPointLoc

INT iCurrentPropertyID
INT iPackageDropPropID = -1

INT iEntrancePlayerIsUsing

INT iKnowOwnerState

INT iReplaceSpecificPropertySlot = -1

INT iRejectedEntryBS
SCRIPT_TIMER buzzerTimer

SCRIPT_TIMER cinematicDelay

CAMERA_INDEX propertyCam0, propertyCam1

INT iWarpingOutOfPropertyID


OBJECT_INDEX fakeBuilding6GarageObj

SCRIPT_TIMER failSafeEntryTimer
SCRIPT_TIMER walkToBuzzerTimer

SCRIPT_TIMER leftBuzzerFailSafe

INT iBuzzerContextIntention = NEW_CONTEXT_INTENTION
INT iPurchaseContextIntention = NEW_CONTEXT_INTENTION

OWNED_PROPERTIES_IN_BUILDING ownedDetails

INT iPlayersInBuilding
INT iPlayersPropertyNum[NUM_NETWORK_PLAYERS]
INT iSlowLoopCounter

INT iSelectedPropertyToBuy

INT iVehicleSaveOffset

MP_REALITY_WARP_IN_STRUCT warpInControl

LOCK_STATE carDoorLocks

CONST_INT MAX_EXTERIOR_DOORS	3

CONST_INT MAX_MAP_HOLE_PROTECTORS	3
OBJECT_INDEX mapHoleProtectors[MAX_MAP_HOLE_PROTECTORS]

OBJECT_INDEX createdBuzzers[MAX_ENTRANCES]

OBJECT_INDEX doorPlanks[2]

OBJECT_INDEX revolvingDoor

BOOL bInteriorPinned = FALSE
BOOL bGrabbedOfficeGarageDoorInterior = FALSE
INTERIOR_INSTANCE_INDEX officeGarageDoorInterior
OBJECT_INDEX officeGarageDoors[2]
OBJECT_INDEX officeGarageExternalDoor
FLOAT fDoorSlideOpenSpeed = 3.0
FLOAT fDoorSlideCloseSpeed = 1.5
#IF IS_DEBUG_BUILD
BOOL bRecreateGarageDoors
#ENDIF

MP_DOOR_DETAILS propertyDoors[MAX_EXTERIOR_DOORS]

//#IF NOT FEATURE_HEIST_PLANNING
//VEHICLE_INDEX playerVehicles[NUM_NETWORK_PLAYERS]
//#ENDIF
BOOL bPropertyDoorGarage[MAX_EXTERIOR_DOORS]

MP_PROP_BLOCKING_OBJECTS_STRUCT blockObjectDetails

INT iPedsInCar

CONST_INT ALLOW_FAKE_GARAGE_DEBUG	0

BOOL bStoredEntranceLocations
#IF IS_DEBUG_BUILD
INT iCantEnterFramePrintDelay
BOOL db_bFullDebug
BOOL bDebugTestBuzzer

BOOL db_bBlockPlayerBeast
BOOL db_bBlockPlayerGen
BOOL db_bClearBlockPlayer
BOOL db_bBypassVehicleRestrictions
#ENDIF

#IF ALLOW_FAKE_GARAGE_DEBUG
#IF IS_DEBUG_BUILD
BOOL db_bDisplayDoorRequestState

FLOAT fDebugTestShapeTestHeightOffset = -0.25
FLOAT fDebugTestShapeTestRadius = 0.5
BOOL bDebugTestShapeTestDraw
BOOL bDebugVehicleModelOffset
BOOL bDebugMiscPlayerHeading
#ENDIF
#ENDIF

//INT iRotatingDoorSpeed = 30
//BOOL bServerTakenControlOfRevolvingDoor

FUNC FLOAT GET_INTERP_POINT_FLOAT(FLOAT fStartPos, FLOAT fEndPos, FLOAT fStartTime, FLOAT fEndTime, FLOAT fPointTime)
	RETURN ((((fEndPos - fStartPos) / (fEndTime - fStartTime)) * (fPointTime - fStartTime)) + fStartPos)
ENDFUNC

FUNC VECTOR GET_INTERP_POINT_VECTOR(VECTOR vStartPos, VECTOR vEndPos, FLOAT fStartTime, FLOAT fEndTime, FLOAT fPointTime)
	RETURN <<GET_INTERP_POINT_FLOAT(vStartPos.X, vEndPos.X, fStartTime, fEndTime, fPointTime), GET_INTERP_POINT_FLOAT(vStartPos.Y, vEndPos.Y, fStartTime, fEndTime, fPointTime), GET_INTERP_POINT_FLOAT(vStartPos.Z, vEndPos.Z, fStartTime, fEndTime, fPointTime)>>
ENDFUNC

#IF IS_DEBUG_BUILD


#IF ALLOW_FAKE_GARAGE_DEBUG
BOOL bDebugFakeGarageInterp
BOOL bDebugFakeGaragePause
FLOAT fDebugFakeGaragePauseTime = -1
BOOL bDebugFakeGarageLoop
FLOAT fDebugFakeGaragePhase
VECTOR vDebugFakeGaragePoint1, vDebugFakeGarageRotation1
VECTOR vDebugFakeGaragePoint2, vDebugFakeGarageRotation2
INT iDebugFakeGarageDuration = 1000
FLOAT fDebugFakeGarageStartTime = -1
BOOL bDebugFakeGarageSetCar1, bDebugFakeGarageSetCar2
BOOL bDebugFakeGarageRender
BOOL bDebugFakeGarageOutput
BOOL bDebugFakeGarageUnfreezeCar
BOOL bDebugFakeGarageGhostCar
VEHICLE_INDEX vehGhost1, vehGhost2
BOOL bDebugFakeGarageCamEnable
BOOL bDebugFakeGarageCamGrab1
VECTOR vDebugFakeGarageCamCoord1
VECTOR vDebugFakeGarageCamRot1
FLOAT fDebugFakeGarageCamFov1 = 50.0
BOOL bDebugFakeGarageCamGrab2
BOOL bDebugFakeGarageCamRotateFlip
VECTOR vDebugFakeGarageCamCoord2
VECTOR vDebugFakeGarageCamRot2
FLOAT fDebugFakeGarageCamFov2 = 50.0
INT iDebugFakeGarageCamDuration = 1000
FLOAT fDebugFakeGarageCamShake = 0.3
INT camGraphTypeFakeGarage = ENUM_TO_INT(GRAPH_TYPE_DECEL)
CAMERA_INDEX camFakeGarage
#ENDIF

#ENDIF

INT iBuzzerPressSyncedSceneTimer

INT iBuzzerPressSyncedScene

INT iBuzzerWalkAwayShapeTest	//0 = 180, 1 = 135, 2 = 225, 3 = 90, 4 = 270

BLIP_INDEX forSaleBlip

INT iBuzzerID = -1

CONST_INT SERVER_STAGE_INIT			0
CONST_INT SERVER_STAGE_RUNNING		1

MP_PROP_FOR_SALE_STRUCT forSaleDetails

CONST_INT SERVER_DOOR_UNLOCKED	0
CONST_INT SERVER_DOOR_LOCKED	1

//CONST_INT SERVER_DOOR_STATES 2

STRUCT ServerBroadcastData
	NETWORK_INDEX	normalDoorIDs[MAX_EXTERIOR_DOORS]
	//NETWORK_INDEX 	forSaleSign
	
	//NETWORK_INDEX 	revolvingDoor //only used for high end 14-15
	INT iStage
//	INT iRequestDoorUsageBS[MAX_EXTERIOR_DOORS]
//	INT iPlayersInsideEntranceBS[MAX_EXTERIOR_DOORS]
	NETWORK_INDEX niGarageAttendant
	
	BOOL bGarageAttendantCreated = FALSE
ENDSTRUCT
ServerBroadcastData serverBD

INT iDoorSetBS[MAX_EXTERIOR_DOORS]


OBJECT_INDEX garageBlockingObj

OBJECT_INDEX entryBlockingObj
OBJECT_INDEX forSaleSign

SCRIPT_TIMER serverDoorStateTimeout[MAX_EXTERIOR_DOORS]

#IF ACTIVATE_PROPERTY_CS_DEBUG
structSceneTool_Launcher rag_net_realty_1_scene, rag_net_realty_2_scene, rag_net_realty_3_scene
structSceneTool_Launcher rag_net_realty_4_scene, rag_net_realty_5_scene, rag_net_realty_6_scene
#ENDIF

//CONST_INT PLAYER_BD_BS_REQUEST_USE_OF_DOOR0		0
//CONST_INT PLAYER_BD_BS_REQUEST_USE_OF_DOOR1		1
//CONST_INT PLAYER_BD_BS_REQUEST_USE_OF_DOOR2		2

CONST_INT PLAYER_BD_BS_ENTERING_GARAGE			3
CONST_INT PLAYER_BD_BS_READY_FOR_VEHICLE_FADE	4

CONST_INT PLAYER_BD_BS_DOING_HEIST_DROPOFF		5
CONST_INT PLAYER_BD_BS_CAR_IN_GARAGE_LOCATE		6
CONST_INT PLAYER_BD_BS_ABLE_TO_ENTER_PROPERTY	7

SCRIPT_TIMER failSafeWarp

PLAYER_INDEX warpInWithDriverID

VEHICLE_INDEX heistUnlockCar

MP_PROPERTY_NON_AXIS_DETAILS entranceArea[2]
//#IF NOT FEATURE_HEIST_PLANNING
//INT iLocalGarageCollisionFlag[2]
//#ENDIF
//INT iLocalEntryCollisionFlag

INT iBoolsBitSet
CONST_INT iBS_LAUNCH_STORE				0
CONST_INT iBS_STORE_ACTIVE				1

STRUCT PlayerBroadcastData
	INT iBS
ENDSTRUCT
PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]

PROC START_MENU_INPUT_DELAY(INT iDelayTime)
	REINIT_NET_TIMER(menuInputDelay,TRUE)
	iMenuInputDelay = iDelayTime
ENDPROC

FUNC BOOL CAN_PLAYER_USE_MENU_INPUT()
	IF HAS_NET_TIMER_STARTED(menuInputDelay)
		IF HAS_NET_TIMER_EXPIRED(menuInputDelay,iMenuInputDelay,TRUE)
			RETURN TRUE
		ENDIF
	ENDIF		
	RETURN FALSE
ENDFUNC

PROC SET_PROPERTY_ENTER_STAGE(INT iStage)
	#IF IS_DEBUG_BUILD
	SWITCH iStage
		CASE PROP_ENTER_STAGE_INIT
			CDEBUG1LN(DEBUG_SAFEHOUSE, "SET_PROPERTY_ENTER_STAGE: = PROP_ENTER_STAGE_INIT")
		BREAK
		CASE PROP_ENTER_STAGE_FULL
			CDEBUG1LN(DEBUG_SAFEHOUSE, "SET_PROPERTY_ENTER_STAGE: = PROP_ENTER_STAGE_FULL")
		BREAK
		CASE PROP_ENTER_STAGE_NO_VEHICLE
			CDEBUG1LN(DEBUG_SAFEHOUSE, "SET_PROPERTY_ENTER_STAGE: = PROP_ENTER_STAGE_NO_VEHICLE")
		BREAK
		CASE PROP_ENTER_STAGE_VEHICLE
			CDEBUG1LN(DEBUG_SAFEHOUSE, "SET_PROPERTY_ENTER_STAGE: = PROP_ENTER_STAGE_VEHICLE")
		BREAK
		CASE PROP_ENTER_STAGE_FAKE_CUTSCENE_HEIST
			CDEBUG1LN(DEBUG_SAFEHOUSE, "SET_PROPERTY_ENTER_STAGE: = PROP_ENTER_STAGE_FAKE_CUTSCENE_HEIST")
		BREAK
	ENDSWITCH
	#ENDIF
//	IF iStage = PROP_ENTER_STAGE_VEHICLE
//		IF iCurrentPropertyID = PROPERTY_HIGH_APT_1
//		OR iCurrentPropertyID = PROPERTY_HIGH_APT_2
//		OR iCurrentPropertyID = PROPERTY_HIGH_APT_3
//		OR iCurrentPropertyID = PROPERTY_HIGH_APT_4
//			//SET_BIT(ilocalBS,LOCAL_BS_NO_FADE)
//			//CDEBUG1LN(DEBUG_SAFEHOUSE, "SET_PROPERTY_ENTER_STAGE: skiping fade for vehicle entry")
//		ENDIF
//	ENDIF
	IF iStage = PROP_ENTER_STAGE_FULL
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_CAN_BE_TARGETTED | NSPC_ALLOW_PLAYER_DAMAGE)
		START_MENU_INPUT_DELAY(500)
	ENDIF
	warpInControl.iStage = iStage
ENDPROC

//PROC INIT_CURRENT_PROPERTY_DETAILS()
//	INT i
//	REPEAT MAX_MP_PROPERTIES i
//		IF ARE_VECTORS_ALMOST_EQUAL(vWorldPointLoc,GET_MP_PROPERTY_WORLD_POINT(i),5)
//			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Launched for property #", i)
//			GET_MP_PROPERTY_DETAILS(mpProperties[iCurrentPropertyID],i)
//			EXIT
//		ENDIF
//	ENDREPEAT
//	#IF IS_DEBUG_BUILD
//	CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Launched but no at any property's locations???? See Conor")
//	CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Launched at : ",vWorldPointLoc)
//	SCRIPT_ASSERT("AM_MP_PROPERTY_EXT: Launched but no at any property's locations???? See Conor")
//	#ENDIF
//ENDPROC

PROC INIT_CURRENT_BUILDING_DETAILS()
	INT i
	BUILDING_PROPERTIES buildingProperties
	REPEAT MP_PROPERTY_BUILDING_MAX i
		VECTOR tempVector = GET_MP_PROPERTY_BUILDING_WORLD_POINT(i)
		IF tempVector.x >0
		ENDIF
		CDEBUG3LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Launched at : ",vWorldPointLoc, ", building world point at ", i, " Name: ", GET_BUILDING_ENUM_NAME(i), " = ", tempVector, ", distance: ", GET_DISTANCE_BETWEEN_COORDS(vWorldPointLoc, GET_MP_PROPERTY_BUILDING_WORLD_POINT(i)))
		IF ARE_VECTORS_ALMOST_EQUAL(vWorldPointLoc,GET_MP_PROPERTY_BUILDING_WORLD_POINT(i),5)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Launched at : ",vWorldPointLoc, ", building world point at ", i, " Name: ", GET_BUILDING_ENUM_NAME(i), " = ", tempVector, ", distance: ", GET_DISTANCE_BETWEEN_COORDS(vWorldPointLoc, GET_MP_PROPERTY_BUILDING_WORLD_POINT(i)))
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Launched for property building #", i)
			GET_BUILDING_PROPERTIES(buildingProperties,i)
			iCurrentPropertyID = buildingProperties.iPropertiesInBuilding[0]
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: iCurrentPropertyID = ", iCurrentPropertyID)
			GET_FOR_SALE_DETAILS(forSaleDetails,i)
			EXIT
		ENDIF
	ENDREPEAT
	#IF IS_DEBUG_BUILD
	CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Launched but not at any property's locations???? See Conor")
	CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Launched at : ",vWorldPointLoc)
	SCRIPT_ASSERT("AM_MP_PROPERTY_EXT: Launched but not at any property's locations???? See Conor")
	#ENDIF
	CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Killing script")
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC


FUNC VECTOR GET_OFFICE_GARAGE_EXTERNAL_DOOR_POSITION()
	SWITCH mpProperties[iCurrentPropertyID].iBuildingID
		CASE MP_PROPERTY_OFFICE_BUILDING_1 RETURN <<-1550.147, -559.428, 24.755>> 
		CASE MP_PROPERTY_OFFICE_BUILDING_2 RETURN <<-1414.345, -477.138, 32.413>> 
		CASE MP_PROPERTY_OFFICE_BUILDING_4 RETURN <<-79.488, -786.100, 37.338>> 
	ENDSWITCH
	PRINTLN("GET_OFFICE_GARAGE_EXTERNAL_DOOR_POSITION - iCurrentPropertyID = ", iCurrentPropertyID, ", mpProperties[iCurrentPropertyID].iBuildingID = ", mpProperties[iCurrentPropertyID].iBuildingID)
	SCRIPT_ASSERT("GET_OFFICE_GARAGE_EXTERNAL_DOOR_POSITION - building doesnt have external office door")	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC
FUNC VECTOR GET_OFFICE_GARAGE_EXTERNAL_DOOR_ROTATION()
	SWITCH mpProperties[iCurrentPropertyID].iBuildingID
		CASE MP_PROPERTY_OFFICE_BUILDING_1 RETURN <<0.0, 0.0, 34.0>> 
		CASE MP_PROPERTY_OFFICE_BUILDING_2 RETURN <<0.0, 0.0, -56.5>>
		CASE MP_PROPERTY_OFFICE_BUILDING_4 RETURN <<0.0, 0.0, 14.750>>
	ENDSWITCH
	PRINTLN("GET_OFFICE_GARAGE_EXTERNAL_DOOR_ROTATION - iCurrentPropertyID = ", iCurrentPropertyID, ", mpProperties[iCurrentPropertyID].iBuildingID = ", mpProperties[iCurrentPropertyID].iBuildingID)
	SCRIPT_ASSERT("GET_OFFICE_GARAGE_EXTERNAL_DOOR_ROTATION - building doesnt have external office door")	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC
FUNC MODEL_NAMES GET_OFFICE_GARAGE_EXTERNAL_DOOR_MODEL()
	SWITCH mpProperties[iCurrentPropertyID].iBuildingID
		CASE MP_PROPERTY_OFFICE_BUILDING_1 RETURN INT_TO_ENUM(MODEL_NAMES,  HASH("imp_Prop_ImpEx_Gate_sm_13"))
		CASE MP_PROPERTY_OFFICE_BUILDING_2 RETURN INT_TO_ENUM(MODEL_NAMES,  HASH("imp_Prop_ImpEx_Gate_sm_15"))
		CASE MP_PROPERTY_OFFICE_BUILDING_4 RETURN INT_TO_ENUM(MODEL_NAMES,  HASH("imp_Prop_ImpEx_Gate_01"))
	ENDSWITCH
	PRINTLN("GET_OFFICE_GARAGE_EXTERNAL_DOOR_MODEL - iCurrentPropertyID = ", iCurrentPropertyID, ", mpProperties[iCurrentPropertyID].iBuildingID = ", mpProperties[iCurrentPropertyID].iBuildingID)
	SCRIPT_ASSERT("GET_OFFICE_GARAGE_EXTERNAL_DOOR_MODEL - building doesnt have external office door")	
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC
		

PROC CREATE_OFFICE_GARAGE_DOORS()
	IF mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_OFFICE_BUILDING_1
	OR mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_OFFICE_BUILDING_2
	OR mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_OFFICE_BUILDING_3
	OR mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_OFFICE_BUILDING_4
		INT i
		MODEL_NAMES ModelName
		VECTOR vPosition
		VECTOR vRotation
		
		// internal doors
		REPEAT 2 i
			
			vPosition = GET_OFFICE_GARAGE_VECTOR_AS_OFFSET(mpProperties[iCurrentPropertyID].iBuildingID, 8 + i, FALSE)
			vRotation = GET_OFFICE_GARAGE_VECTOR_AS_OFFSET(mpProperties[iCurrentPropertyID].iBuildingID, 8 + i, TRUE)
			
			IF i = 0
				ModelName = INT_TO_ENUM(MODEL_NAMES,  HASH("imp_prop_impexp_liftdoor_l"))
			ELSE
				ModelName = INT_TO_ENUM(MODEL_NAMES,  HASH("imp_prop_impexp_liftdoor_r"))
			ENDIF
			
			REQUEST_MODEL(ModelName)
			officeGarageDoors[i] = CREATE_OBJECT_NO_OFFSET(ModelName, vPosition, FALSE, FALSE, TRUE)
			FREEZE_ENTITY_POSITION(officeGarageDoors[i], TRUE)
			SET_ENTITY_PROOFS(officeGarageDoors[i], TRUE, TRUE, TRUE, TRUE, TRUE, TRUE)
			SET_ENTITY_INVINCIBLE(officeGarageDoors[i], TRUE)
			SET_ENTITY_DYNAMIC(officeGarageDoors[i], FALSE)
			SET_ENTITY_ROTATION(officeGarageDoors[i], vRotation)
			
			CDEBUG1LN(DEBUG_SAFEHOUSE, "CREATE_OFFICE_GARAGE_DOORS - created door at ", vPosition)
		ENDREPEAT
		
		// create the external door.
		IF NOT (mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_OFFICE_BUILDING_3)
		
			vPosition = GET_OFFICE_GARAGE_EXTERNAL_DOOR_POSITION()
			vRotation = GET_OFFICE_GARAGE_EXTERNAL_DOOR_ROTATION()
			ModelName = GET_OFFICE_GARAGE_EXTERNAL_DOOR_MODEL()

			REQUEST_MODEL(ModelName)
			officeGarageExternalDoor = CREATE_OBJECT_NO_OFFSET(ModelName, vPosition, FALSE, FALSE, TRUE)
			FREEZE_ENTITY_POSITION(officeGarageExternalDoor, TRUE)
			SET_ENTITY_PROOFS(officeGarageExternalDoor, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE)
			SET_ENTITY_INVINCIBLE(officeGarageExternalDoor, TRUE)
			SET_ENTITY_DYNAMIC(officeGarageExternalDoor, FALSE)
			SET_ENTITY_ROTATION(officeGarageExternalDoor, vRotation)
			
			CDEBUG1LN(DEBUG_SAFEHOUSE, "CREATE_OFFICE_GARAGE_DOORS - created external door at ", vPosition)		
		ENDIF
		
	ENDIF
ENDPROC

PROC UPDATE_OFFICE_GARAGE_DOORS_ROOM()
	
	INT i

	IF NOT bGrabbedOfficeGarageDoorInterior
		REPEAT 2 i
			IF DOES_ENTITY_EXIST(officeGarageDoors[i])

				STRING sInteriorType

				SWITCH iCurrentPropertyID
					CASE PROPERTY_OFFICE_1
					CASE PROPERTY_OFFICE_1_GARAGE_LVL1
					CASE PROPERTY_OFFICE_1_GARAGE_LVL2
					CASE PROPERTY_OFFICE_1_GARAGE_LVL3
						sInteriorType = "hei_sm_13_carpark"
					BREAK
					
					CASE PROPERTY_OFFICE_2_BASE
					CASE PROPERTY_OFFICE_2_GARAGE_LVL1
					CASE PROPERTY_OFFICE_2_GARAGE_LVL2
					CASE PROPERTY_OFFICE_2_GARAGE_LVL3
						sInteriorType = "hei_sm_15_carpark"
					BREAK
					
					CASE PROPERTY_OFFICE_3
					CASE PROPERTY_OFFICE_3_GARAGE_LVL1
					CASE PROPERTY_OFFICE_3_GARAGE_LVL2
					CASE PROPERTY_OFFICE_3_GARAGE_LVL3					
						sInteriorType = "hei_dt1_02_carpark"
					BREAK
					
					CASE PROPERTY_OFFICE_4
					CASE PROPERTY_OFFICE_4_GARAGE_LVL1
					CASE PROPERTY_OFFICE_4_GARAGE_LVL2
					CASE PROPERTY_OFFICE_4_GARAGE_LVL3	
						sInteriorType = "hei_dt1_11_carpark"
					BREAK
				ENDSWITCH
				
				IF NOT IS_STRING_NULL(sInteriorType)
					officeGarageDoorInterior = GET_INTERIOR_AT_COORDS_WITH_TYPE(GET_ENTITY_COORDS(officeGarageDoors[i]), sInteriorType)
					IF IS_VALID_INTERIOR(officeGarageDoorInterior)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "UPDATE_OFFICE_GARAGE_DOORS_ROOM - setting bGrabbedOfficeGarageDoorInterior = tRUE")
						bGrabbedOfficeGarageDoorInterior = TRUE
						
						PIN_INTERIOR_IN_MEMORY(officeGarageDoorInterior)
						bInteriorPinned = TRUE
					ENDIF
				ENDIF
				
			ENDIF	
		ENDREPEAT
	ELSE

		IF IS_VALID_INTERIOR(officeGarageDoorInterior)
		
			IF IS_INTERIOR_READY(officeGarageDoorInterior)

				STRING sRoomName
				
//				CDEBUG1LN(DEBUG_SAFEHOUSE, "UPDATE_OFFICE_GARAGE_DOORS_ROOM - iCurrentPropertyID")
				
				SWITCH iCurrentPropertyID
					CASE PROPERTY_OFFICE_1
					CASE PROPERTY_OFFICE_1_GARAGE_LVL1
					CASE PROPERTY_OFFICE_1_GARAGE_LVL2
					CASE PROPERTY_OFFICE_1_GARAGE_LVL3					
						sRoomName = "GtaMloRoom001"
					BREAK			
					CASE PROPERTY_OFFICE_2_BASE
					CASE PROPERTY_OFFICE_2_GARAGE_LVL1
					CASE PROPERTY_OFFICE_2_GARAGE_LVL2
					CASE PROPERTY_OFFICE_2_GARAGE_LVL3					
						sRoomName = "GtaMloRoom002"
					BREAK			
					CASE PROPERTY_OFFICE_3
					CASE PROPERTY_OFFICE_3_GARAGE_LVL1
					CASE PROPERTY_OFFICE_3_GARAGE_LVL2
					CASE PROPERTY_OFFICE_3_GARAGE_LVL3						
						sRoomName = "GtaMloRoom018"
					BREAK			
					CASE PROPERTY_OFFICE_4
					CASE PROPERTY_OFFICE_4_GARAGE_LVL1
					CASE PROPERTY_OFFICE_4_GARAGE_LVL2
					CASE PROPERTY_OFFICE_4_GARAGE_LVL3						
						sRoomName = "GtaMloRoom001"
					BREAK
				ENDSWITCH
						
				IF NOT IS_STRING_NULL(sRoomName)
					REPEAT 2 i
						IF DOES_ENTITY_EXIST(officeGarageDoors[i])			
							FORCE_ROOM_FOR_ENTITY(officeGarageDoors[i], officeGarageDoorInterior, GET_HASH_KEY(sRoomName))
						ENDIF
					ENDREPEAT
				ENDIF
			
			ELSE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "UPDATE_OFFICE_GARAGE_DOORS_ROOM - setting bGrabbedOfficeGarageDoorInterior = tRUE")
			ENDIF
			
		
		ELSE
			bGrabbedOfficeGarageDoorInterior = FALSE	
			bInteriorPinned = FALSE
		ENDIF
		
	ENDIF
	
	
	#IF IS_DEBUG_BUILD
		IF (bRecreateGarageDoors)
			REPEAT 2 i
				IF DOES_ENTITY_EXIST(officeGarageDoors[i])
					DELETE_OBJECT(officeGarageDoors[i])
					PRINTLN("deleted garage door ", i)
				ENDIF
			ENDREPEAT
			CREATE_OFFICE_GARAGE_DOORS()
			bRecreateGarageDoors = FALSE
		ENDIF
	#ENDIF	

ENDPROC



PROC REGISTER_PROPERTY_DOORS()
	INT i
	BOOL bDoorGarage
	REPEAT MAX_EXTERIOR_DOORS i
		bDoorGarage = FALSE
		propertyDoors[i].vCoords = <<0,0,0>>
		bPropertyDoorGarage[i] = FALSE
		propertyDoors[i].iDoorHash = 0
		IF GET_BUILDING_DOOR_DETAILS(tempPropOffset,propertyDoors[i],bDoorGarage,mpProperties[iCurrentPropertyID].iBuildingID,i)
			SETUP_MP_DOOR(propertyDoors[i])
		ENDIF
		//buildingDoorDetails[i] = doorLocationDetails
		bPropertyDoorGarage[i] = bDoorGarage
		
		IF bPropertyDoorGarage[i]
			IF propertyDoors[i].iDoorHash != 0
				IF mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_BUILDING_1
					DOOR_SYSTEM_SET_AUTOMATIC_RATE(propertyDoors[i].iDoorHash, 0.4, FALSE, FALSE)
				ELSE
					DOOR_SYSTEM_SET_AUTOMATIC_RATE(propertyDoors[i].iDoorHash, 0.6, FALSE, FALSE)
				ENDIF
				IF mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_BUILDING_5
					DOOR_SYSTEM_SET_AUTOMATIC_DISTANCE(propertyDoors[i].iDoorHash,15,FALSE,FALSE)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
//	
//	IF mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_BUILDING_6 
//		CREATE_MODEL_HIDE(<<-935.0067,-378.3667,39.2081>> , 0.02, PROP_QL_REVOLVING_DOOR, TRUE)	//Hide revolving door. One created by server
//	ENDIF
	
	IF mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_BUILDING_38
		CREATE_MODEL_HIDE(<<2494.2651,1587.5674,31.7398>> , 0.02, PROP_FNCLINK_02GATE3, TRUE)	//gates
		CREATE_MODEL_HIDE(<<2484.0889,1566.9988,31.7453>> , 0.02, PROP_FNCLINK_02GATE3, TRUE)	//gates
	ENDIF
	
//	#IF FEATURE_BIKER
//	IF iCurrentPropertyID = PROPERTY_CLUBHOUSE_1_BASE_A
//		CREATE_MODEL_HIDE(<<247.6964, -1803.0883, 26.1131>>, 10, PROP_FNCLINK_03GATE2, TRUE)
//	ENDIF
//	#ENDIF
	
//	IF mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_BUILDING_57
//		//CREATE_MODEL_HIDE(<<-1494.7855,514.3803,117.7965>> , 1, PROP_FNCRES_02C, TRUE)	//gates
//		CREATE_MODEL_HIDE(<<-1477.9321,525.6279,117.9921>> , 5, PROP_LRGGATE_01_R, TRUE)	//gates
//		CREATE_MODEL_HIDE(<<-1488.1262,520.6134,118.0117>> , 5, PROP_LRGGATE_01_L, TRUE)	//gates
//	ENDIF
ENDPROC

// Do necessary pre game start ini.
// Returns FALSE if the script fails to receive an initial network broadcast.
FUNC BOOL PROCESS_PRE_GAME()
	NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE)
	
	REGISTER_PROPERTY_DOORS()
	
	CREATE_OFFICE_GARAGE_DOORS()
	
	// This makes sure the net script is active, waits untull it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
		
	//RESERVE_NETWORK_MISSION_OBJECTS(MAX_EXTERIOR_DOORS+3)
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
	
	// KGM: Wait for the first network broadcast before moving on - call this as the last instruction in pre-game
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		RETURN FALSE
	ENDIF
	
	// This script will not be paused if another script calls PAUSE_GAME
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	

	
	RETURN TRUE
ENDFUNC

PROC SET_LOCAL_STAGE(INT iStage)
	#IF IS_DEBUG_BUILD
	SWITCH istage
		CASE STAGE_CHECK_FOR_IN_LOCATE
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: property building #",mpProperties[iCurrentPropertyID].iBuildingID ," setting stage to be STAGE_CHECK_FOR_IN_LOCATE")
		BREAK
		CASE STAGE_SETUP_FOR_MENU
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: property building #",mpProperties[iCurrentPropertyID].iBuildingID ," setting stage to be STAGE_SETUP_FOR_MENU")
		BREAK
		CASE STAGE_USING_MENU
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: property building #",mpProperties[iCurrentPropertyID].iBuildingID," setting stage to be STAGE_USING_MENU")
		BREAK
//		CASE STAGE_INTRO_IF_APPLICABLE
//			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: property building #",mpProperties[iCurrentPropertyID].iBuildingID," setting stage to be STAGE_INTRO_IF_APPLICABLE")
//		BREAK
		CASE STAGE_SETUP_FOR_USING_PROPERTY
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: property building #",mpProperties[iCurrentPropertyID].iBuildingID ," setting stage to be STAGE_SETUP_FOR_USING_PROPERTY")
		BREAK
		CASE STAGE_ENTER_PROPERTY
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: property building #",mpProperties[iCurrentPropertyID].iBuildingID ," setting stage to be STAGE_ENTER_PROPERTY")
		BREAK
		CASE STAGE_WARP_TO_ENTRANCE_AREA_THEN_LEAVE
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: property building #",mpProperties[iCurrentPropertyID].iBuildingID ," setting stage to be STAGE_WARP_TO_ENTRANCE_AREA_THEN_LEAVE")
		BREAK
		CASE STAGE_LEAVE_ENTRANCE_AREA
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: property building #",mpProperties[iCurrentPropertyID].iBuildingID ," setting stage to be STAGE_LEAVE_ENTRANCE_AREA")
		BREAK
		CASE STAGE_BUZZED_INTO_PROPERTY
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: property building #",mpProperties[iCurrentPropertyID].iBuildingID ," setting stage to be STAGE_BUZZED_INTO_PROPERTY")
		BREAK
	ENDSWITCH
	#ENDIF
	IF iStage = STAGE_SETUP_FOR_USING_PROPERTY
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)//,NSPC_PREVENT_VISIBILITY_CHANGES)
		IF NOT g_bPropInteriorScriptRunning
			globalPropertyEntryData.bLeftOrRejectedBuzzer = FALSE
			CDEBUG1LN(DEBUG_SAFEHOUSE, "SET_LOCAL_STAGE: globalPropertyEntryData.bLeftOrRejectedBuzzer = FALSE")
		ENDIF
		CLEAR_BIT(iLocalBS2,LOCAL_BS2_RemoveStickyBombs)
		IF IS_PROPERTY_OFFICE_GARAGE(iCurrentPropertyID)
			IF mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iType = ENTRANCE_TYPE_GARAGE
				CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iIsPlayerGoingToModGarageBS, biIsPlayerGoingToModGarageTrue)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "SET_LOCAL_STAGE: biIsPlayerGoingToModGarageTrue cleared")
			ENDIF
		ENDIF
	ENDIF
	IF iStage = STAGE_LEAVE_ENTRANCE_AREA
		bLeavingEntranceArea = TRUE
		CDEBUG1LN(DEBUG_SAFEHOUSE, "SET_LOCAL_STAGE: bLeavingEntranceArea = TRUE")
	ENDIF
	IF iStage = STAGE_USING_MENU
		START_MENU_INPUT_DELAY(500)
	ENDIF
	iLocalStage = iStage
ENDPROC

PROC CLEAR_TRANSITION_SESSION_DETAILS_ON_ENTRY_IF_SUITABLE(INT iCallID)
	IF NOT (Is_Player_Currently_On_MP_Heist(PLAYER_ID())
	OR Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID()))
	AND g_bAllowPropTransitionDataClearOnExterior
		UNUSED_PARAMETER(iCallID)
		PRINTLN("CLEAR_TRANSITION_SESSION_DETAILS_ON_ENTRY_IF_SUITABLE: clearing data on entry call#",iCallID)
		CLEAR_TRANSITION_SESSION_PROPERTY_INVITE_DETAILS()
	ENDIF
ENDPROC


PROC CLEAR_INVALID_VEHICLE_HELP()
	IF IS_BIT_SET(iLocalBS, LOCAL_BS_INVALID_VEHICLE_HELP)
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MP_PROP_IVD_VEH")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MP_PROP_IVD_VEH1")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MP_PROP_IVD_VEH2")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MP_PROP_IVD_3a")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MP_PROP_IVD_3b")
		//OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MP_PROP_IVD_3c")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MP_PROP_IVD_3d")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MP_PROP_IVD_3e")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MP_PROP_IVD_3f")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MP_PROP_IVD_3g")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PPA_GEN0")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PPA_GENCLUB")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PPA_GEN1")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PPA_BEAST0")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PPA_BEAST1")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PPA_CONTRA0")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PPA_CONTRA0b")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PPA_CONTRA1")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PPA_CONTRA2")
			CLEAR_HELP()
		ENDIF
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Clearing invalid vehicle help")
	ENDIF
	CLEAR_BIT(iLocalBS, LOCAL_BS_INVALID_VEHICLE_HELP)
ENDPROC

FUNC BOOL IS_ENTERING_DISABLED_DUE_TO_EXIT_WARP()
	IF iWarpingOutOfPropertyID != 0
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC CLEAR_ENTERING_DISABLED_DUE_TO_EXIT_WARP()
	IF NOT HAS_NET_TIMER_STARTED(tempPropertyExtMenu.exitDoorDelay)
	OR (HAS_NET_TIMER_STARTED(tempPropertyExtMenu.exitDoorDelay) AND HAS_NET_TIMER_EXPIRED_READ_ONLY(tempPropertyExtMenu.exitDoorDelay,2000,TRUE))
		IF iWarpingOutOfPropertyID != 0
			iWarpingOutOfPropertyID = 0
			CLEAR_BIT(iLocalBS2,LOCAL_BS2_PlayerWalkingOut)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "CLEAR_ENTERING_DISABLED_DUE_TO_EXIT_WARP: Setting iWarpingOutOfPropertyID = 0")
			#IF IS_DEBUG_BUILD
			IF NOT HAS_NET_TIMER_STARTED(tempPropertyExtMenu.exitDoorDelay)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "CLEAR_ENTERING_DISABLED_DUE_TO_EXIT_WARP: timer hasn't started")
			ELSE	
				CDEBUG1LN(DEBUG_SAFEHOUSE, "CLEAR_ENTERING_DISABLED_DUE_TO_EXIT_WARP: timer expired")
			ENDIF
			#ENDIF
		ENDIF
	ENDIF
ENDPROC

//FUNC BOOL IS_NPC_IN_VEHICLE()
//	INT iSeat
//	VEHICLE_INDEX theVeh
//	PED_INDEX pedIndex
//	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//		theVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
//		IF IS_VEHICLE_DRIVEABLE(theVeh)
//			FOR iSeat = ENUM_TO_INT(VS_DRIVER) TO ENUM_TO_INT(VS_EXTRA_RIGHT_3)
//				IF NOT IS_VEHICLE_SEAT_FREE(theVeh, INT_TO_ENUM(VEHICLE_SEAT, iSeat),TRUE)
//					pedIndex = GET_PED_IN_VEHICLE_SEAT(theVeh,INT_TO_ENUM(VEHICLE_SEAT, iSeat),TRUE)
//					IF DOES_ENTITY_EXIST(pedIndex)
//						IF NOT IS_PED_A_PLAYER(pedIndex)
//							//IF NOT IS_PED_INJURED(pedIndex)
//							
//								RETURN TRUE
//							//ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDFOR
//		ENDIF
//	ENDIF
//	RETURN FALSE
//ENDFUNC

//FUNC BOOL IS_PLAYER_BLOCKED_FROM_PROPERTY()
//	IF g_iPreventPropertyAccessBS != 0
//		RETURN TRUE
//	ENDIF
//	IF FM_EVENT_IS_PLAYER_CRITICAL_TO_FM_EVENT(PLAYER_ID())
//		RETURN TRUE
//	ENDIF
//	IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_TIME_TRIAL)
//		RETURN TRUE
//	ENDIF
////	IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bInfectionParticipant
////		RETURN TRUE
////	ENDIF
//	RETURN FALSE
//ENDFUNC

PROC MAINTAIN_PLAYERS_DOING_HEIST_DROPOFF()
//	IF (Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID())
//	OR Is_Player_Currently_On_MP_Heist(PLAYER_ID()))
//	AND g_bShouldThisPlayerGetPulledIntoApartment
//	
//	ENDIF

	IF (g_bGarageDropOff OR g_bPropertyDropOff)
	AND (IS_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITITCAL_ENTITY() OR g_bShouldThisPlayerGetPulledIntoApartment)
	AND iCurrentPropertyID > 0 
	AND iCurrentPropertyID <= MAX_MP_PROPERTIES
	AND g_iHeistPropertyID > 0 
	AND g_iHeistPropertyID <= MAX_MP_PROPERTIES
	AND mpProperties[iCurrentPropertyID].iBuildingID = mpProperties[g_iHeistPropertyID].iBuildingID
		#IF IS_DEBUG_BUILD
		IF NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_DOING_HEIST_DROPOFF)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_PLAYERS_DOING_HEIST_DROPOFF: setting PLAYER_BD_BS_DOING_HEIST_DROPOFF")
		ENDIF
		#ENDIF
		SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_DOING_HEIST_DROPOFF)
	ELSE
		#IF IS_DEBUG_BUILD
		IF IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_DOING_HEIST_DROPOFF)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_PLAYERS_DOING_HEIST_DROPOFF: clearing PLAYER_BD_BS_DOING_HEIST_DROPOFF")
		ENDIF
		#ENDIF
		CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_DOING_HEIST_DROPOFF)
	ENDIF
ENDPROC

FUNC BOOL SHOULD_BYPASS_CHECKS_FOR_ENTRY(BOOL bGarage = TRUE)
	IF bGarage
	
	ENDIF

	IF g_iHeistPropertyID < 0//added by rowan to fix 2068141
	OR iCurrentPropertyID < 0
		#IF IS_DEBUG_BUILD
		IF bBypassChecksAllowHeistEntry
			IF g_iHeistPropertyID < 0
				g_iHeistPropertyID = iCurrentPropertyID 
			ENDIF
		ELSE
		#ENDIF
		
		RETURN FALSE
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
	ENDIF
	
	IF bGarage
		IF (((g_bGarageDropOff OR g_bPropertyDropOff)
		AND (IS_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITITCAL_ENTITY() OR bHadMissionCriticalEntityOnDropoff)
		AND mpProperties[iCurrentPropertyID].iBuildingID = mpProperties[g_iHeistPropertyID].iBuildingID)
		#IF IS_DEBUG_BUILD
		OR bBypassChecksAllowHeistEntry
		#ENDIF )
			RETURN TRUE
		#IF IS_DEBUG_BUILD
		ELSE
			
			IF db_bFullDebug
				IF NOT (g_bGarageDropOff OR g_bPropertyDropOff)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "SHOULD_BYPASS_CHECKS_FOR_ENTRY: not garage dropoff")
				ENDIF
				IF NOT IS_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITITCAL_ENTITY()
					CDEBUG1LN(DEBUG_SAFEHOUSE, "SHOULD_BYPASS_CHECKS_FOR_ENTRY: not mission critical")
				ENDIF
				IF mpProperties[iCurrentPropertyID].iBuildingID != mpProperties[g_iHeistPropertyID].iBuildingID
					CDEBUG1LN(DEBUG_SAFEHOUSE, "SHOULD_BYPASS_CHECKS_FOR_ENTRY: iCurrentPropertyID iBuildingID = ", mpProperties[iCurrentPropertyID].iBuildingID,
								" g_TransitionSessionNonResetVars.iLastPropertyIndex iBuilding:",mpProperties[g_iHeistPropertyID].iBuildingID  )
				ENDIF
			ENDIF
		#ENDIF
		ENDIf
	ELSE
		IF ((g_bPropertyDropOff
		AND IS_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITITCAL_ENTITY()
		AND mpProperties[iCurrentPropertyID].iBuildingID = mpProperties[g_iHeistPropertyID].iBuildingID
		AND IS_GAMER_HANDLE_VALID(heistLeader))
		#IF IS_DEBUG_BUILD
		OR bBypassChecksAllowHeistEntry
		#ENDIF )
			RETURN TRUE
		#IF IS_DEBUG_BUILD
		ELSE
			IF db_bFullDebug
				IF NOT g_bPropertyDropOff
					CDEBUG1LN(DEBUG_SAFEHOUSE, "SHOULD_BYPASS_CHECKS_FOR_ENTRY: not property dropoff")
				ENDIF
				IF NOT IS_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITITCAL_ENTITY()
					CDEBUG1LN(DEBUG_SAFEHOUSE, "SHOULD_BYPASS_CHECKS_FOR_ENTRY: not mission critical")
				ENDIF
				IF mpProperties[iCurrentPropertyID].iBuildingID != mpProperties[g_iHeistPropertyID].iBuildingID
					CDEBUG1LN(DEBUG_SAFEHOUSE, "SHOULD_BYPASS_CHECKS_FOR_ENTRY: iCurrentPropertyID iBuildingID = ", mpProperties[iCurrentPropertyID].iBuildingID,
								" g_TransitionSessionNonResetVars.iLastPropertyIndex iBuilding:",mpProperties[g_iHeistPropertyID].iBuildingID  )
				ENDIF
				IF NOT IS_GAMER_HANDLE_VALID(heistLeader)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "SHOULD_BYPASS_CHECKS_FOR_ENTRY: heist leader is invalid")
				ENDIF 
			ENDIF
		#ENDIF
		ENDIF
	ENDIF
	
	#IF FEATURE_GEN9_EXCLUSIVE
	IF IS_PROPERTY_OFFICE(iCurrentPropertyID)
		IF SHOULD_ALLOW_OFFICE_ENTRY_DURING_MISSION()
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_PROPERTY_CLUBHOUSE(iCurrentPropertyID)
		IF SHOULD_ALLOW_CLUBHOUSE_ENTRY_DURING_MISSION()
			RETURN TRUE
		ENDIF
	ENDIF
	#ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_DRIVING_INTO_PROPERTY_GARAGE(VEHICLE_INDEX theVehicle, MODEL_NAMES theModel, BOOL& bReverseIn)
	VECTOR vFrontTopLeft, vFrontTopRight, vBackTopLeft, vBackTopRight	
	DETERMINE_4_TOP_VECTORS_OF_MODEL_BOX_IN_WORLD_COORDS(theVehicle, theModel, vFrontTopLeft, vFrontTopRight, vBackTopLeft, vBackTopRight)
	
	VECTOR vFrontBottomLeft, vFrontBottomRight, vBackBottomLeft, vBackBottomRight
	DETERMINE_4_BOTTOM_VECTORS_OF_MODEL_BOX_IN_WORLD_COORDS(theVehicle, theModel, vFrontBottomLeft, vFrontBottomRight, vBackBottomLeft, vBackBottomRight)
	
	VECTOR vFrontMiddleLeft, vFrontMiddleRight, vBackMiddleLeft, vBackMiddleRight
	
	vFrontMiddleLeft = GET_INTERP_POINT_VECTOR(vFrontBottomLeft, vFrontTopLeft, 0.0, 1.0, 0.5)
	vFrontMiddleRight = GET_INTERP_POINT_VECTOR(vFrontBottomRight, vFrontTopRight, 0.0, 1.0, 0.5)
	vBackMiddleLeft = GET_INTERP_POINT_VECTOR(vBackBottomLeft, vBackTopLeft, 0.0, 1.0, 0.5)
	vBackMiddleRight = GET_INTERP_POINT_VECTOR(vBackBottomRight, vBackTopRight, 0.0, 1.0, 0.5)
	
	IF IS_POINT_IN_ANGLED_AREA(vFrontMiddleLeft,mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].locateDetails.vPos1,
								mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].locateDetails.vPos2,
								mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].locateDetails.fWidth)
	OR IS_POINT_IN_ANGLED_AREA(vFrontMiddleRight,mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].locateDetails.vPos1,
								mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].locateDetails.vPos2,
								mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].locateDetails.fWidth)
		SET_BIT(iLocalBS2,LOCAL_BS2_KeepInvalidText)
		SET_BIT(iLocalBS2,LOCAL_BS2_bKeepWantedFlag)
		
		//CDEBUG1LN(DEBUG_SAFEHOUSE, "IS_PLAYER_DRIVING_INTO_PROPERTY_GARAGE: in locate setting LOCAL_BS2_KeepInvalidText = TRUE")
		
		IF IS_ENTERING_DISABLED_DUE_TO_EXIT_WARP()
			RETURN FALSE
		ENDIF
		
		IF IS_HEADING_ACCEPTABLE_CORRECTED(GET_ENTITY_HEADING(theVehicle),mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].locateDetails.fEnterHeading,45 #IF IS_DEBUG_BUILD,FALSE #ENDIF)	
			IF NOT SHOULD_BYPASS_CHECKS_FOR_ENTRY()
			AND IS_NPC_IN_VEHICLE()
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					IF NOT IS_BIT_SET(iLocalBS, LOCAL_BS_INVALID_VEHICLE_HELP)
						IF IS_PROPERTY_CLUBHOUSE(iCurrentPropertyID)
							PRINT_HELP("INV_VEH_GAR0_CH")
						ELSE
							PRINT_HELP("INV_VEH_GAR0")
						ENDIF
						CDEBUG1LN(DEBUG_SAFEHOUSE, "Printing: INV_VEH_GAR0")
						SET_BIT(iLocalBS, LOCAL_BS_INVALID_VEHICLE_HELP)
					ENDIF
				ENDIF
				CDEBUG1LN(DEBUG_SAFEHOUSE, "player Not allowed in garage with NPC-1")
				RETURN FALSE
			ENDIF
			bReverseIn = FALSE
			RETURN TRUE
		ENDIF
	ELIF IS_POINT_IN_ANGLED_AREA(vBackMiddleLeft,mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].locateDetails.vPos1,
								mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].locateDetails.vPos2,
								mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].locateDetails.fWidth)
	OR IS_POINT_IN_ANGLED_AREA(vBackMiddleRight,mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].locateDetails.vPos1,
								mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].locateDetails.vPos2,
								mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].locateDetails.fWidth)
		SET_BIT(iLocalBS2,LOCAL_BS2_KeepInvalidText)
		SET_BIT(iLocalBS2,LOCAL_BS2_bKeepWantedFlag)
		//CDEBUG1LN(DEBUG_SAFEHOUSE, "IS_PLAYER_DRIVING_INTO_PROPERTY_GARAGE: in locate setting  LOCAL_BS2_KeepInvalidText = TRUE")
		IF IS_ENTERING_DISABLED_DUE_TO_EXIT_WARP()
			RETURN FALSE
		ENDIF
		IF IS_HEADING_ACCEPTABLE_CORRECTED(GET_ENTITY_HEADING(theVehicle),mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].locateDetails.fEnterHeading-180,45 #IF IS_DEBUG_BUILD,FALSE #ENDIF)
			IF NOT SHOULD_BYPASS_CHECKS_FOR_ENTRY()
			AND IS_NPC_IN_VEHICLE()
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					IF NOT IS_BIT_SET(iLocalBS, LOCAL_BS_INVALID_VEHICLE_HELP)
						IF IS_PROPERTY_CLUBHOUSE(iCurrentPropertyID)
							PRINT_HELP("INV_VEH_GAR0_CH")
						ELSE
							PRINT_HELP("INV_VEH_GAR0")
						ENDIF
						CDEBUG1LN(DEBUG_SAFEHOUSE, "Printing: INV_VEH_GAR0")
						SET_BIT(iLocalBS, LOCAL_BS_INVALID_VEHICLE_HELP)
					ENDIF
				ENDIF
				CDEBUG1LN(DEBUG_SAFEHOUSE, "player Not allowed in garage with NPC-2")
				RETURN FALSE
			ENDIF
			bReverseIn = TRUE
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

//FUNC BOOL IS_VEHICLE_AN_EXCEPTION_FOR_GARAGE(MODEL_NAMES theModel)
//	SWITCH theModel
//		CASE SANCHEZ
//		CASE SANCHEZ2
//		CASE BLAZER
//		CASE BLAZER2
//		CASE DUNE
//			RETURN TRUE
//		BREAK
//	ENDSWITCH	
//	RETURN FALSE
//ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_ENTERING_OR_EXITING_A_VEHICLE()
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF GET_IS_TASK_ACTIVE(PLAYER_PED_ID(),CODE_TASK_EXIT_VEHICLE)
			RETURN TRUE
		ENDIF
		IF IS_PED_IN_ANY_VEHICLE_OR_WAITING_TO_START_TASK_ENTER_VEHICLE(PLAYER_PED_ID())
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

//FUNC BOOL IS_VEHICLE_SUITABLE_FOR_GARAGE(VEHICLE_INDEX theVehicle, INT iProperty)
//	MODEL_NAMES theModel
//	theModel = GET_ENTITY_MODEL(theVehicle)
//	BOOL bPersonalVehicle, bAnotherPlayerPersonalVehicle, bVehicleAbortedEntry
//	INT iDLCVehSlot
//	#IF IS_DEBUG_BUILD
//	IF db_bBypassVehicleRestrictions
//		RETURN TRUE
//	ENDIF
//	#ENDIF
//	
//	//VEHICLE_MODEL_FAIL_ENUM failReason
//	IF DECOR_IS_REGISTERED_AS_TYPE("Player_Vehicle", DECOR_TYPE_INT) 
//		IF DECOR_EXIST_ON(theVehicle, "Player_Vehicle") 
//			IF DECOR_GET_INT(theVehicle, "Player_Vehicle") = NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID())
//				bPersonalVehicle = TRUE
//				CDEBUG1LN(DEBUG_SAFEHOUSE, "bPersonalVehicle = TRUE")
//			ELSE
//				CDEBUG1LN(DEBUG_SAFEHOUSE, "bAnotherPlayerPersonalVehicle = TRUE")
//				bAnotherPlayerPersonalVehicle = TRUE
//			ENDIF
//		ENDIF
//	ENDIF
//	IF DECOR_IS_REGISTERED_AS_TYPE("Veh_Modded_By_Player", DECOR_TYPE_INT) 
//		IF DECOR_EXIST_ON(theVehicle, "Veh_Modded_By_Player") 
//			IF DECOR_GET_INT(theVehicle, "Veh_Modded_By_Player") != NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID())
//				CDEBUG1LN(DEBUG_SAFEHOUSE, "bAnotherPlayerPersonalVehicle = TRUE - modded by another player")
//				bAnotherPlayerPersonalVehicle = TRUE
//			ENDIF
//		ENDIF
//	ENDIF
//	INT iDecoratorValue
//	IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
//		IF DECOR_EXIST_ON(theVehicle, "MPBitset")	
//			iDecoratorValue = DECOR_GET_INT(theVehicle, "MPBitset")
//		ENDIF
//		IF IS_BIT_SET(iDecoratorValue, MP_DECORATOR_BS_ENTERING_INTO_GARAGE)	
//			bVehicleAbortedEntry = TRUE
//			CDEBUG1LN(DEBUG_SAFEHOUSE, "bVehicleAbortedEntry = TRUE")
//		ENDIF
//		
//	ENDIF
//	#IF NOT FEATURE_BIKER
//	IF iProperty != 0
//		
//	ENDIF
//	#ENDIF
//	
//	IF g_sMPTunables.bDISABLE_PROPERTY_VEHICLE_ACCESS
//		IF NOT IS_PAUSE_MENU_ACTIVE()
//		AND NOT IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
//		AND NOT IS_CUSTOM_MENU_ON_SCREEN()
//			IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
//				IF NOT IS_BIT_SET(iLocalBS, LOCAL_BS_INVALID_VEHICLE_HELP)
//					IF IS_VEHICLE_MY_PERSONAL_VEHICLE(theVehicle)
//						PRINT_HELP("PROP_BLOCK_1")
//						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Printing help PROP_BLOCK_1")
//					ELSE
//						PRINT_HELP("PROP_BLOCK_2")
//						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Printing help PROP_BLOCK_2")
//					ENDIF
//					SET_BIT(iLocalBS, LOCAL_BS_INVALID_VEHICLE_HELP)
//				ENDIF
//			ENDIF
//		ENDIF
//		RETURN FALSE
//	ENDIF
//	
//	#IF FEATURE_BIKER
//	IF IS_PROPERTY_CLUBHOUSE(iProperty)
//		IF (IS_THIS_MODEL_A_BIKE(theModel)
//		OR IS_THIS_MODEL_A_QUADBIKE(theModel))
//		AND NOT IS_VEHICLE_A_CYCLE(theModel)
//			//RETURN TRUE
//		ELSE
//			PRINTLN("IS_VEHICLE_SUITABLE_FOR_GARAGE: false clubhouse and not a bike or quadbike vehModel = ", GET_MODEL_NAME_FOR_DEBUG(theModel))
//			IF NOT IS_PAUSE_MENU_ACTIVE()
//			AND NOT IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
//			AND NOT IS_CUSTOM_MENU_ON_SCREEN()
//				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
//					IF NOT IS_BIT_SET(iLocalBS, LOCAL_BS_INVALID_VEHICLE_HELP)
//						PRINT_HELP("MP_PROP_IVD_VEH4")
//						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Printing help MP_PROP_IVD_VEH4")
//						SET_BIT(iLocalBS, LOCAL_BS_INVALID_VEHICLE_HELP)
//					ENDIF
//				ENDIF
//			ENDIF
//			RETURN FALSE
//		ENDIF
//	ENDIF
//	#ENDIF
//	
//	#IF FEATURE_IMPORT_EXPORT
//	IF IS_SVM_VEHICLE(theVehicle)
//		IF NOT IS_PAUSE_MENU_ACTIVE()
//		AND NOT IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
//		AND NOT IS_CUSTOM_MENU_ON_SCREEN()
//			IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
//				IF NOT IS_BIT_SET(iLocalBS, LOCAL_BS_INVALID_VEHICLE_HELP)
//					IF IS_PROPERTY_CLUBHOUSE(iProperty)
//						PRINT_HELP("MP_PROP_IVD_VEH4")
//					ELSE
//						PRINT_HELP("MP_PROP_IVD_VEH")
//					ENDIF
//					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Printing help MP_PROP_IVD_VEH - 123")
//					SET_BIT(iLocalBS, LOCAL_BS_INVALID_VEHICLE_HELP)
//				ENDIF
//			ENDIF
//		ENDIF
//		RETURN FALSE	
//	ENDIF
//	IF IS_PROPERTY_OFFICE_GARAGE(iProperty)
//	OR IS_PROPERTY_OFFICE(iProperty)
//		IF IS_VEHICLE_A_CYCLE(theModel)
//			IF NOT IS_PAUSE_MENU_ACTIVE()
//			AND NOT IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
//			AND NOT IS_CUSTOM_MENU_ON_SCREEN()
//				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
//					IF NOT IS_BIT_SET(iLocalBS, LOCAL_BS_INVALID_VEHICLE_HELP)
//						PRINT_HELP("MP_PROP_IVD_VEH")
//						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Printing help MP_PROP_IVD_VEH - 12")
//						SET_BIT(iLocalBS, LOCAL_BS_INVALID_VEHICLE_HELP)
//					ENDIF
//				ENDIF
//			ENDIF
//			RETURN FALSE	
//		ENDIF
//	ENDIF
//	#ENDIF
//
//
//	IF SHOULD_BYPASS_CHECKS_FOR_ENTRY()
//		IF NOT IS_PAUSE_MENU_ACTIVE()
//		AND NOT IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
//		AND NOT IS_CUSTOM_MENU_ON_SCREEN()
//			RETURN TRUE
//		ENDIF
//	ELSE
//		IF IS_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITITCAL_ENTITY()
//		OR IS_PLAYER_BLOCKED_FROM_PROPERTY(FALSE, GET_PROPERTY_TYPE_FOR_IS_PLAYER_BLOCKED_FROM_PROPERTY(iCurrentPropertyID))
//			IF NOT IS_PAUSE_MENU_ACTIVE()
//			AND NOT IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
//			AND NOT IS_CUSTOM_MENU_ON_SCREEN()
//				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
//					IF NOT IS_BIT_SET(iLocalBS, LOCAL_BS_INVALID_VEHICLE_HELP)
//						IF IS_PLAYER_BLOCKED_FROM_PROPERTY(FALSE, GET_PROPERTY_TYPE_FOR_IS_PLAYER_BLOCKED_FROM_PROPERTY(iCurrentPropertyID))
//							PRINT_REASON_PLAYER_IS_BLOCKED_FROM_PROPERTY(TRUE)
//						ELSE
//							PRINT_HELP("CUST_GAR_MISO")
//						ENDIF
//						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Printing help MP_PROP_IVD_VEH3: player owns DLC content but it is excluded from garage")
//						SET_BIT(iLocalBS, LOCAL_BS_INVALID_VEHICLE_HELP)
//					ENDIF
//				ENDIF
//			ENDIF
//		ELSE
//			IF NOT DOES_DECORATOR_PREVENT_ALLOWANCE_IN_GARAGE(theVehicle)
//			AND NOT bAnotherPlayerPersonalVehicle
//			AND NOT bVehicleAbortedEntry
//			AND IS_MODEL_VALID_FOR_PERSONAL_VEHICLE(GET_ENTITY_MODEL(theVehicle))
//			AND NETWORK_GET_ENTITY_IS_NETWORKED(theVehicle)
//				iDLCVehSlot = GET_DLC_VEHICLE_DATA_SLOT_FOR_VEHICLE(theModel)
//				IF NOT IS_VEHICLE_AVAILABLE_FOR_GAME(theModel, TRUE)
//					IF NOT IS_PAUSE_MENU_ACTIVE()
//					AND NOT IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
//					AND NOT IS_CUSTOM_MENU_ON_SCREEN()
//						IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
//							IF NOT IS_BIT_SET(iLocalBS, LOCAL_BS_INVALID_VEHICLE_HELP)
//								IF iDLCVehSlot != -1
//								AND NOT IS_DLC_VEHICLE_SLOT_SUITABLE_FOR_GARAGE(iDLCVehSlot)
//									PRINT_HELP("MP_PROP_IVD_VEH")
//									CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Printing help MP_PROP_IVD_VEH: player doesn't own DLC content but it is excluded from garage anyway")
//								ELSE
//									PRINT_HELP("MP_PROP_IVD_4")
//									CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Printing help    : player does not own DLC content can't store vehicle")
//									SET_BIT(iLocalBS, LOCAL_BS_INVALID_VEHICLE_HELP)
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//				ELIF iDLCVehSlot != -1
//				AND NOT IS_DLC_VEHICLE_SLOT_SUITABLE_FOR_GARAGE(iDLCVehSlot)
//					IF NOT IS_PAUSE_MENU_ACTIVE()
//					AND NOT IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
//					AND NOT IS_CUSTOM_MENU_ON_SCREEN()
//						IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
//							IF NOT IS_BIT_SET(iLocalBS, LOCAL_BS_INVALID_VEHICLE_HELP)
//								PRINT_HELP("MP_PROP_IVD_VEH")
//								CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Printing help MP_PROP_IVD_VEH: player owns DLC content but it is excluded from garage")
//								SET_BIT(iLocalBS, LOCAL_BS_INVALID_VEHICLE_HELP)
//							ENDIF
//						ENDIF
//					ENDIF
//				ELIF NOT IS_MP_PREMIUM_VEHICLE(theModel)	
//				OR (IS_MP_PREMIUM_VEHICLE(theModel) AND	 bPersonalVehicle)
//	//				IF IS_VEHICLE_SAFE_FOR_MOD_SHOP(theVehicle,FALSE,failReason)
//	//				IF IS_VEHICLE_A_CYCLE(theModel)
//	//				OR IS_VEHICLE_AN_EXCEPTION_FOR_GARAGE(theModel)
//						RETURN TRUE
//	//				ELSE
//	//					IF failReason = VEH_MODEL_FAIL_INVALID_VEHICLE
//	//						IF NOT IS_PAUSE_MENU_ACTIVE()
//	//						AND NOT IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
//	//						AND NOT IS_CUSTOM_MENU_ON_SCREEN()
//	//							IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
//	//								IF NOT IS_BIT_SET(iLocalBS, LOCAL_BS_INVALID_VEHICLE_HELP)
//	//									PRINT_HELP("MP_PROP_IVD_VEH")
//	//									CDEBUG1LN(DEBUG_SAFEHOUSE, "Printing: MP_PROP_IVD_VEH")
//	//									SET_BIT(iLocalBS, LOCAL_BS_INVALID_VEHICLE_HELP)
//	//								ENDIF
//	//							ENDIF
//	//						ENDIF
//	//					ELIF failReason = VEH_MODEL_FAIL_COP_VEHICLE
//	//						IF NOT IS_PAUSE_MENU_ACTIVE()
//	//						AND NOT IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
//	//						AND NOT IS_CUSTOM_MENU_ON_SCREEN()
//	//							IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
//	//								IF NOT IS_BIT_SET(iLocalBS, LOCAL_BS_INVALID_VEHICLE_HELP)
//	//									PRINT_HELP("MP_PROP_IVD_VEH1")
//	//									CDEBUG1LN(DEBUG_SAFEHOUSE, "Printing: MP_PROP_IVD_VEH1")
//	//									SET_BIT(iLocalBS, LOCAL_BS_INVALID_VEHICLE_HELP)
//	//								ENDIF
//	//							ENDIF
//	//						ENDIF
//	//					//EL
//	//					ENDIF
//					//ENDIF
//				ELSE
//					IF IS_MP_PREMIUM_VEHICLE(theModel)
//						IF NOT IS_PAUSE_MENU_ACTIVE()
//						AND NOT IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
//						AND NOT IS_CUSTOM_MENU_ON_SCREEN()
//							IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
//								IF NOT IS_BIT_SET(iLocalBS, LOCAL_BS_INVALID_VEHICLE_HELP)
//									PRINT_HELP(GET_SITE_FROM_VEHICLE(theModel))
//									CDEBUG1LN(DEBUG_SAFEHOUSE, "Printing: ", GET_SITE_FROM_VEHICLE(theModel))
//									SET_BIT(iLocalBS, LOCAL_BS_INVALID_VEHICLE_HELP)
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//			ELSE
//				IF NOT IS_PAUSE_MENU_ACTIVE()
//				AND NOT IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
//				AND NOT IS_CUSTOM_MENU_ON_SCREEN()
//					IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
//						IF NOT IS_BIT_SET(iLocalBS, LOCAL_BS_INVALID_VEHICLE_HELP)
//							IF IS_PROPERTY_CLUBHOUSE(iProperty)
//							AND bAnotherPlayerPersonalVehicle
//								PRINT_HELP("MP_PROP_IVD_VEH3")
//							ELSE
//								PRINT_HELP("MP_PROP_IVD_VEH")
//							ENDIF
//							
//							SET_BIT(iLocalBS, LOCAL_BS_INVALID_VEHICLE_HELP)
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
//	
//	RETURN FALSE
//ENDFUNC

/// PURPOSE:
///    To prevent access into garage in special cases where garage locate completely covers buzzer.  
FUNC BOOL DISABLE_WALK_IN_FOR_BUZZER_LOCATE()
	SWITCH mpProperties[iCurrentPropertyID].iBuildingID
		CASE MP_PROPERTY_BUILDING_7
			SWITCH iEntrancePlayerIsUsing
				CASE 1
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(),<<-634.766479,52.733532,42.303310>>,
													<<-630.410400,52.705669,48.118065>>,
													3.500000,
													FALSE,TRUE,TM_ON_FOOT)
						RETURN TRUE
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE MP_PROPERTY_BUILDING_2
			SWITCH iEntrancePlayerIsUsing
				CASE 1
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(),<<-281.222504,-991.745361,22.908405>>,
													<<-276.880646,-993.169128,27.328550>>,
													3.500000,
													FALSE,TRUE,TM_ON_FOOT)
						RETURN TRUE
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE MP_PROPERTY_BUILDING_3
			SWITCH iEntrancePlayerIsUsing
				CASE 1
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(),<<-1454.700928,-498.933228,31.232906>>,
													<<-1451.583740,-503.054321,34.242401>>,
													2.250000,
													FALSE,TRUE,TM_ON_FOOT)
						RETURN TRUE
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE MP_PROPERTY_BUILDING_4
			SWITCH iEntrancePlayerIsUsing
				CASE 1
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(),<<-819.030945,-439.883911,35.194271>>,
													<<-823.445496,-442.112579,39.639885>> ,
													3.000000,
													FALSE,TRUE,TM_ON_FOOT)
						RETURN TRUE
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		
		CASE MP_PROPERTY_BUILDING_6
			SWITCH iEntrancePlayerIsUsing
				CASE 1
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(),<<-876.873169,-358.780823,34.578461>>,
													<<-874.390808,-363.407959,38.623848>> ,
													2.750000,
													FALSE,TRUE,TM_ON_FOOT)
						RETURN TRUE
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_PLAYER_BYPASS_DRIVER_CHECKS_TO_PROCEED(PED_INDEX &driverPed)
	IF NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_DOING_HEIST_DROPOFF)
		RETURN FALSE
	ENDIF
	PLAYER_INDEX driverPlayer
	IF NOT IS_PED_INJURED(driverPed)
		IF IS_PED_A_PLAYER(driverPed)
			IF NETWORK_GET_PLAYER_INDEX_FROM_PED(driverPed) != PLAYER_ID()
				driverPlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(driverPed)
				IF IS_NET_PLAYER_OK(driverPlayer)
					IF IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_DOING_HEIST_DROPOFF)
					AND NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(driverPlayer)].iBS,PLAYER_BD_BS_CAR_IN_GARAGE_LOCATE)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_INT: CAN_PLAYER_BYPASS_DRIVER_CHECKS_TO_PROCEED FALSE")
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_PLAYER_SKIP_OFFICE_GARAGE_MENU(INT iNumOwnedProperties)
	IF iNumOwnedProperties = 2
	AND IS_PROPERTY_OFFICE(iCurrentPropertyID)
	AND mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iType = ENTRANCE_TYPE_GARAGE
	AND GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_OFFICE_GARAGE_LVL1) = PROPERTY_OFFICE_1_GARAGE_LVL1
	AND GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_OFFICE_GARAGE_LVL2) <= 0
	AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC


FUNC INT GET_PROPERTY_ID_FOR_FIRST_OFFICE_GARAGE_IN_BUILDING(INT iBuilding)
	BUILDING_PROPERTIES building
	GET_BUILDING_PROPERTIES(building,iBuilding)
	INT i
	REPEAT building.iNumProperties i
		PRINTLN("GET_PROPERTY_ID_FOR_FIRST_OFFICE_GARAGE_IN_BUILDING: building.iPropertiesInBuilding[",i,"] = ",building.iPropertiesInBuilding[i])
		IF GET_OFFICE_GARAGE_BASE_ID_FROM_PROPERTY(building.iPropertiesInBuilding[i]) = PROPERTY_OFFICE_1_GARAGE_LVL1
			RETURN building.iPropertiesInBuilding[i]											
		ENDIF
	ENDREPEAT
	RETURN iCurrentPropertyID
ENDFUNC

FUNC BOOL IS_PLAYER_IN_OFFICE_EXTERIOR_GARAGE()
	//only used fro office 3 for now just hard coding.
	INTERIOR_INSTANCE_INDEX theInterior = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<-170.2406, -631.9297, 31.4243>>,"hei_dt1_02_carpark")
	IF GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = theInterior 
		//AND GET_ROOM_KEY_FROM_ENTITY(aPedsNearBathroom[iBathroomPedsLoop]) = iBathroomRoomKey 
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC MOVE_OFFICE_GARAGE_BLIP(BOOL bCleanup = FALSE)
	BLIP_INDEX theBlip = mpPropMaintain.PropertyBlips[mpProperties[iCurrentPropertyID].iBuildingID][2]

	IF DOES_BLIP_EXIST(theBlip)
		IF iCurrentPropertyID = PROPERTY_OFFICE_3
		OR iCurrentPropertyID = PROPERTY_OFFICE_3_GARAGE_LVL1
		OR iCurrentPropertyID = PROPERTY_OFFICE_3_GARAGE_LVL2
		OR iCurrentPropertyID = PROPERTY_OFFICE_3_GARAGE_LVL3
			IF IS_NET_PLAYER_OK(PLAYER_ID())
			AND IS_PLAYER_IN_OFFICE_EXTERIOR_GARAGE()
			AND NOT bCleanup
				IF NOT ARE_VECTORS_ALMOST_EQUAL(GET_BLIP_COORDS(theBlip), <<-145.7487, -580.9215, 31.6066>>)
					SET_BLIP_COORDS(theBlip, <<-145.7487, -580.9215, 31.6066>>)
					PRINTLN("MOVE_OFFICE_GARAGE_BLIP: moving arcadius garage blip to ",<<-145.7487, -580.9215, 31.6066>>)
				ENDIF
			ELSE
				IF ARE_VECTORS_ALMOST_EQUAL(GET_BLIP_COORDS(theBlip), <<-145.7487, -580.9215, 31.6066>>)
					SET_BLIP_COORDS(theBlip,mpProperties[iCurrentPropertyID].vBlipLocation[2])
					PRINTLN("MOVE_OFFICE_GARAGE_BLIP: moving arcadius garage blip BACK to ",mpProperties[iCurrentPropertyID].vBlipLocation[2]," cleanup = ",bCleanup)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_ENTERING_THE_PROPERTY(BOOL bVehiclesValidForEntry, INT iNotAllowedReason = -1)
	VEHICLE_INDEX theVehicle
	//VEHICLE_MODEL_FAIL_ENUM failReason
	//BOOL bPersonalVehicle
	BOOl bReverseIn
	//BOOL bAnotherPlayerPersonalVehicle
	PED_INDEX theDriver
	//BOOL bBypassEntryChecks
	//INT iDLCVehSlot
	IF NOT IS_PLAYER_TELEPORT_ACTIVE()
	AND NOT IS_PLAYER_GETTING_IN_OR_OUT_OF_VEHICLE(PLAYER_ID())
	AND NOT IS_PLAYER_IN_CORONA()
	AND NOT IS_POST_MISSION_CLEANUP_REQUIRED_OR_ACTIVE(PLAYER_ID())
	AND NOT IS_BIT_SET(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_AcceptedInvite)
	AND NOT (mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iType = ENTRANCE_TYPE_GARAGE AND g_BGScript_PreventEnteringGar)
		IF IS_PROPERTY_OFFICE(iCurrentPropertyID)
		AND mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iType = ENTRANCE_TYPE_GARAGE
		AND GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_OFFICE_GARAGE_LVL1) != PROPERTY_OFFICE_1_GARAGE_LVL1
			RETURN FALSE
		ENDIF
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(),TRUE)
		AND mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iType = ENTRANCE_TYPE_GARAGE
		AND bVehiclesValidForEntry
			//VECTOR vVelocity
			theVehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			theDriver = GET_PED_IN_VEHICLE_SEAT(theVehicle, VS_DRIVER)
			IF PLAYER_PED_ID() = theDriver
			OR CAN_PLAYER_BYPASS_DRIVER_CHECKS_TO_PROCEED(theDriver)
				IF NOT HAS_NET_TIMER_STARTED(tempPropertyExtMenu.exitDoorDelay)
				OR HAS_NET_TIMER_EXPIRED_READ_ONLY(tempPropertyExtMenu.exitDoorDelay,1000,TRUE)
					//IF NOT HAS_NET_TIMER_STARTED(tempPropertyExtMenu.exitDoorDelay)
					//OR HAS_NET_TIMER_EXPIRED_READ_ONLY(tempPropertyExtMenu.exitDoorDelay,1000)
						MODEL_NAMES theModel
						theModel = GET_ENTITY_MODEL(theVehicle)
						
						IF IS_MODEL_IN_CDIMAGE(theModel)
							IF IS_PLAYER_DRIVING_INTO_PROPERTY_GARAGE(theVehicle,theModel,bReverseIn)
								BOOL bPrintedReason = IS_BIT_SET(iLocalBS, LOCAL_BS_INVALID_VEHICLE_HELP)
								//bBypassEntryChecks = SHOULD_BYPASS_CHECKS_FOR_ENTRY()
								IF IS_VEHICLE_SUITABLE_FOR_PROPERTY(theVehicle,iCurrentPropertyID,ownedDetails, SHOULD_BYPASS_CHECKS_FOR_ENTRY(),bPrintedReason, DEFAULT, DEFAULT #IF IS_DEBUG_BUILD ,db_bBypassVehicleRestrictions #ENDIF )
									IF iNotAllowedReason = NOT_ALLOWED_ACCESS_ON_VEHICLE_NOT_PERSONAL
										IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
											IF NOT IS_BIT_SET(iLocalBS, LOCAL_BS_INVALID_VEHICLE_HELP)
												PRINT_HELP("MP_CLU_PERSVO")
												SET_BIT(iLocalBS, LOCAL_BS_INVALID_VEHICLE_HELP)
											ENDIF
										ENDIF
									ELSE
										VECTOR vVelocity
										vVelocity = GET_ENTITY_SPEED_VECTOR(theVehicle,TRUE)
										//CDEBUG1LN(DEBUG_SAFEHOUSE, "Current car speed vector is: ", vVelocity)
										IF (NOT bReverseIn AND vVelocity.y > 0)
										OR (bReverseIn AND vVelocity.y < 0)
											IF IS_BIT_SET(iLocalBS2,LOCAL_BS2_PlayerInVehicleDoingHeistDelivery)
											AND NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_DOING_HEIST_DROPOFF)
												CDEBUG1LN(DEBUG_SAFEHOUSE, "IS_PLAYER_ENTERING_THE_PROPERTY: FALSE heist dropoff player in vehicle")
											ELSE
												CLEAR_INVALID_VEHICLE_HELP()
												CDEBUG1LN(DEBUG_SAFEHOUSE, "IS_PLAYER_ENTERING_THE_PROPERTY: Entered in a vehicle")
												SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_CAR_IN_GARAGE_LOCATE)
												RETURN TRUE
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								IF bPrintedReason
									SET_BIT(iLocalBS, LOCAL_BS_INVALID_VEHICLE_HELP)
								ENDIF
							ENDIF
						ENDIF
					//ENDIF
				ENDIF
			//ELSE
			//	CDEBUG1LN(DEBUG_SAFEHOUSE, "IS_PLAYER_ENTERING_THE_PROPERTY: (PLAYER_PED_ID() != GET_PED_IN_VEHICLE_SEAT(theVehicle, VS_DRIVER))")
			ENDIF
		ELSE
//			CDEBUG2LN(DEBUG_SAFEHOUSE, "IS_PLAYER_ENTERING_THE_PROPERTY: iCurrentPropertyID: ", iCurrentPropertyID, ", Name: ", GET_PROPERTY_ENUM_NAME(iCurrentPropertyID)," vPos1 = ", mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].locateDetails.vPos1, ", vPos2 = ", mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].locateDetails.vPos2, ", width = ", mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].locateDetails.fWidth)
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(),mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].locateDetails.vPos1,
													mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].locateDetails.vPos2,
													mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].locateDetails.fWidth,
													FALSE,TRUE,TM_ON_FOOT)
													
				SET_BIT(iLocalBS2,LOCAL_BS2_KeepInvalidText)
				SET_BIT(iLocalBS2,LOCAL_BS2_bKeepWantedFlag)
				IF IS_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITITCAL_ENTITY()
				AND (mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iType != ENTRANCE_TYPE_HOUSE
				OR NOT (SHOULD_BYPASS_CHECKS_FOR_ENTRY(FALSE)
				AND mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iType = ENTRANCE_TYPE_HOUSE))
				OR IS_PLAYER_BLOCKED_FROM_PROPERTY(FALSE, GET_PROPERTY_TYPE_FOR_IS_PLAYER_BLOCKED_FROM_PROPERTY(iCurrentPropertyID))
				OR iNotAllowedReason != 0
					IF NOT IS_PAUSE_MENU_ACTIVE()
					AND NOT IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
					AND NOT IS_CUSTOM_MENU_ON_SCREEN()
						IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
							IF NOT IS_BIT_SET(iLocalBS, LOCAL_BS_INVALID_VEHICLE_HELP)
								IF IS_PLAYER_BLOCKED_FROM_PROPERTY(FALSE, GET_PROPERTY_TYPE_FOR_IS_PLAYER_BLOCKED_FROM_PROPERTY(iCurrentPropertyID))
									IF mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iType = ENTRANCE_TYPE_GARAGE
										PRINT_REASON_PLAYER_IS_BLOCKED_FROM_PROPERTY(TRUE,iCurrentPropertyID,ownedDetails)
									ELSE
										PRINT_REASON_PLAYER_IS_BLOCKED_FROM_PROPERTY(FALSE,iCurrentPropertyID,ownedDetails)
									ENDIF
								ELSE
									IF mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iType = ENTRANCE_TYPE_GARAGE
//										#IF FEATURE_BIKER
//										IF iNotAllowedReason = NOT_ALLOWED_ACCESS_ON_VEHICLE_NOT_PERSONAL
//											PRINT_HELP("MP_CLU_PERSVO")
//										ELSE
//										#ENDIF
										PRINT_HELP("CUST_GAR_MISO")
//										#IF FEATURE_BIKER
//										ENDIF
//										#ENDIF
									ELSE
										IF IS_PROPERTY_OFFICE(iCurrentPropertyID)
											PRINT_HELP("CUST_OFF_MISO")
										ELSE
											IF IS_PROPERTY_CLUBHOUSE(iCurrentPropertyID)
												PRINT_HELP("CUST_CLU_MISO")
											ELSE
												PRINT_HELP("CUST_APT_MISO")
											ENDIF	
										ENDIF
									ENDIF
								ENDIF
								//CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Printing help MP_PROP_IVD_VEH: player owns DLC content but it is excluded from garage")
								SET_BIT(iLocalBS, LOCAL_BS_INVALID_VEHICLE_HELP)
							ENDIF
						ENDIF
					ENDIF
				ELSE
					//CDEBUG1LN(DEBUG_SAFEHOUSE, "Player in angled area")
					IF IS_ENTERING_DISABLED_DUE_TO_EXIT_WARP()
						RETURN FALSE
					ENDIF
					//CDEBUG1LN(DEBUG_SAFEHOUSE, "Checking heading")
					IF IS_HEADING_ACCEPTABLE_CORRECTED(GET_ENTITY_HEADING(PLAYER_PED_ID()),mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].locateDetails.fEnterHeading,30 #IF IS_DEBUG_BUILD,FALSE #ENDIF)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "IS_HEADING_ACCEPTABLE = TRUE")
						IF NOT IS_PLAYER_GETTING_IN_OR_OUT_OF_VEHICLE(PLAYER_ID())
						AND NOT IS_LOCAL_PLAYER_ENTERING_OR_EXITING_A_VEHICLE()
							//CDEBUG1LN(DEBUG_SAFEHOUSE, "Heading OK checking time")
							IF NOT HAS_NET_TIMER_STARTED(tempPropertyExtMenu.exitDoorDelay)
							OR HAS_NET_TIMER_EXPIRED_READ_ONLY(tempPropertyExtMenu.exitDoorDelay,1000,TRUE)
								IF NOT IS_LOCAL_PLAYER_ENTERING_OR_EXITING_A_VEHICLE()
									IF NOT DISABLE_WALK_IN_FOR_BUZZER_LOCATE()
										//CDEBUG1LN(DEBUG_SAFEHOUSE, "IS_PLAYER_ENTERING_THE_PROPERTY: Entered on foot")
										CLEAR_INVALID_VEHICLE_HELP()
										RETURN TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						CDEBUG2LN(DEBUG_SAFEHOUSE, "IS_HEADING_ACCEPTABLE = FALSE")
					ENDIF
				ENDIF
			ELSE
//				CDEBUG2LN(DEBUG_SAFEHOUSE, "IS_ENTITY_IN_ANGLED_AREA = FALSE")
			ENDIF
		ENDIF
		
		//CDEBUG1LN(DEBUG_SAFEHOUSE, "IS_PLAYER_ENTERING_THE_PROPERTY: Setting g_iWarpingOutOfPropertyWithID = 0")
		
	ELSE
		
		CDEBUG2LN(DEBUG_SAFEHOUSE, "IS_PLAYER_ENTERING_THE_PROPERTY = FALSE ??")
	ENDIF
	
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_IN_PURCHASE_LOCATE()
	IF GET_SKYBLUR_STAGE() = SKYBLUR_NONE
		DRAW_MARKER(MARKER_CYLINDER, mpProperties[iCurrentPropertyID].vPurchaseLocation + <<0,0,-0.25>>, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, <<0.75, 0.75, 0.75>>, 255, 0, 128, 178)
	ENDIF
	IF mpProperties[iCurrentPropertyID].iType = PROPERTY_TYPE_GARAGE
	AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(),TRUE)
		//DRAW_MARKER(MARKER_CYLINDER, mpProperties[iCurrentPropertyID].vPurchaseLocation, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, <<2, 2, 0.75>>, 255, 0, 128, 178)
		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),mpProperties[iCurrentPropertyID].vPurchaseLocation,<<3, 3, 2>>,FALSE,TRUE,TM_ANY)
			RETURN TRUE								
		ENDIF
	ELSE
		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),mpProperties[iCurrentPropertyID].vPurchaseLocation,<<1, 1, 2>>,FALSE,TRUE,TM_ON_FOOT)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FLOAT fGroundHeight[MAX_ENTRANCES]

FUNC VECTOR GET_CLUBHOUSE_GARAGE_MARKER_LOCATION(INT iProperty)
	SWITCH iProperty
		CASE PROPERTY_CLUBHOUSE_1_BASE_A RETURN <<258.9073, -1801.8328, 26.4431>>
		CASE PROPERTY_CLUBHOUSE_2_BASE_A RETURN <<-1469.5718, -928.6930, 9.1729>>
		CASE PROPERTY_CLUBHOUSE_3_BASE_A RETURN <<39.5519, -1022.4520, 28.5048>>
		CASE PROPERTY_CLUBHOUSE_4_BASE_A RETURN <<52.9387, 2787.5789, 56.8783>>
		CASE PROPERTY_CLUBHOUSE_5_BASE_A RETURN <<-354.2221, 6066.8062, 30.4985>>
		CASE PROPERTY_CLUBHOUSE_6_BASE_A RETURN <<1731.3073, 3707.9756, 33.1480>>
		CASE PROPERTY_CLUBHOUSE_7_BASE_B RETURN <<943.4094, -1458.2656, 30.4547>>
		CASE PROPERTY_CLUBHOUSE_8_BASE_B RETURN <<178.0314, 308.2870, 104.3706>>
		CASE PROPERTY_CLUBHOUSE_9_BASE_B RETURN <<-17.0586, -194.2749, 51.3703>>
		CASE PROPERTY_CLUBHOUSE_10_BASE_B RETURN <<2467.3728, 4101.1025, 37.0647>>
		CASE PROPERTY_CLUBHOUSE_11_BASE_B RETURN <<-34.8764, 6423.6885, 30.4308>>
		CASE PROPERTY_CLUBHOUSE_12_BASE_B RETURN <<-1147.1165, -1578.1838, 3.4245>>
	ENDSWITCH
	RETURN <<0,0,0>>
ENDFUNC

FUNC BOOL DOES_PLAYER_HAVE_GROUP_ACCESS_TO_PROPERTY()
	IF IS_BIT_SET(iLocalBS2, LOCAL_BS2_GroupAccessAllowed)
		RETURN TRUE
	ENDIF
	IF IS_PROPERTY_CLUBHOUSE(iCurrentPropertyID) 
	AND DOES_MY_BOSS_OWN_PROPERTY_IN_BUILDING(mpProperties[iCurrentPropertyID].iBuildingID,FALSE)
	AND GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID(), FALSE)
		SET_BIT(iLocalBS2, LOCAL_BS2_GroupAccessAllowed)
		PRINTLN("DOES_PLAYER_HAVE_GROUP_ACCESS_TO_PROPERTY- TRUE")
		RETURN TRUE
	ENDIF
				
	RETURN FALSE
ENDFUNC

PROC DRAW_BUZZER_LOCATE(INT iEntrance)
	IF (SHOULD_BYPASS_CHECKS_FOR_ENTRY(FALSE)
	AND mpProperties[iCurrentPropertyID].entrance[iEntrance].iType = ENTRANCE_TYPE_HOUSE)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "DRAW_BUZZER_LOCATE: bypassing drawn for heist drop off")
		EXIT
	ENDIF
	IF Is_Player_Currently_On_MP_Heist(PLAYER_ID())
	OR Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID())
		IF SHOULD_BYPASS_CHECKS_FOR_ENTRY()
		AND mpProperties[iCurrentPropertyID].entrance[iEntrance].iType = ENTRANCE_TYPE_GARAGE
		AND g_FMMC_STRUCT.iRootContentIDHash = g_sMPTunables.iroot_id_HASH_Tutorial_Scope_Out    
			CDEBUG1LN(DEBUG_SAFEHOUSE, "DRAW_BUZZER_LOCATE: bypassing drawn for heist garage drop off - fleeca")
			EXIT
//		ELSE
//			CDEBUG1LN(DEBUG_SAFEHOUSE, "SHOULD_BYPASS_CHECKS_FOR_ENTRY() = ",SHOULD_BYPASS_CHECKS_FOR_ENTRY())
//			CDEBUG1LN(DEBUG_SAFEHOUSE, "mpProperties[iCurrentPropertyID].entrance[",iEntrance,"].iType = ",mpProperties[iCurrentPropertyID].entrance[iEntrance].iType)
//			CDEBUG1LN(DEBUG_SAFEHOUSE, "GET_STRAND_ROOT_CONTENT_ID_HASH() = ",g_FMMC_STRUCT.iRootContentIDHash)
//			CDEBUG1LN(DEBUG_SAFEHOUSE, "g_sMPTunables.iroot_id_HASH_Tutorial_Scope_Out = ",g_sMPTunables.iroot_id_HASH_Tutorial_Scope_Out)
		ENDIF
	ENDIF
	//CDEBUG1LN(DEBUG_SAFEHOUSE, "DRAW_BUZZER_LOCATE: iEntrance = ",iEntrance)
	//url:bugstar:2580256 - Can we hide the blue Coronas for all the stilt House entry cutscenes
	IF iLocalStage = STAGE_ENTER_PROPERTY	
	AND IS_PROPERTY_STILT_APARTMENT(iCurrentPropertyID)
		EXIT
	ENDIF
	VECTOR vCaronaPos
	VECTOR vCaronaScale
	INT iR, iG, iB, iA
	IF GET_SKYBLUR_STAGE() = SKYBLUR_NONE
	AND NOT IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(PLAYER_ID())
		IF fGroundHeight[iEntrance] = 0.0
			GET_GROUND_Z_FOR_3D_COORD(mpProperties[iCurrentPropertyID].entrance[iEntrance].vBuzzerLoc + <<0.0, 0.0, 0.5>>, fGroundHeight[iEntrance])
		ENDIF
		
		GET_HUD_COLOUR(HUD_COLOUR_BLUE,iR, iG, iB, iA)
		
		//Setup the base carona position
		vCaronaPos = mpProperties[iCurrentPropertyID].entrance[iEntrance].vBuzzerLoc
		vCaronaScale = <<0.75, 0.75, 0.75>>
		vCaronaPos.Z = (fGroundHeight[iEntrance] + -0.25 )
				
		//Offset the carona position for the stilr properties
		IF iCurrentPropertyID = PROPERTY_STILT_APT_1_BASE_B
			IF iEntrance = 0
				vCaronaPos.Y += 0.28
			ENDIF
		ELIF iCurrentPropertyID = PROPERTY_STILT_APT_2_B
			IF iEntrance = 0
				vCaronaPos.Y -= 0.45
				vCaronaPos.X += 0.2
				vCaronaPos.Z -= 0.25
			ENDIF
		ELIF iCurrentPropertyID = PROPERTY_STILT_APT_4_B
			IF iEntrance = 0
				vCaronaPos.Y += 0.5
				vCaronaPos.X += 0.286
				vCaronaPos.Z -= 0.25
			ENDIF
		ELIF iCurrentPropertyID = PROPERTY_STILT_APT_5_BASE_A
			IF iEntrance = 0
				vCaronaPos.Y += 0.07
				vCaronaPos.X += 0.11
			ELIF iEntrance = 1
				vCaronaPos.Y -= 0.449
				vCaronaPos.X += 0.074
				vCaronaPos.Z += 0.3
				vCaronaScale = <<0.5, 0.5, 0.5>>
			ENDIF
		ELIF iCurrentPropertyID = PROPERTY_STILT_APT_7_A
			IF iEntrance = 0
				vCaronaPos.X -= 0.113
				vCaronaPos.Y += 0.1	
				vCaronaPos.Z += 0.1
				vCaronaScale = <<0.65, 0.65, 0.65>>
			ENDIF
		ELIF iCurrentPropertyID = PROPERTY_STILT_APT_8_A
			IF iEntrance = 0
				vCaronaPos.X += 0.487
				vCaronaPos.Y -= 0.1
			ELIF iEntrance = 1
				vCaronaPos.Y -= 0.562
				vCaronaPos.X += 0.45
			ENDIF
		ELIF iCurrentPropertyID = PROPERTY_OFFICE_2_BASE
			IF iEntrance = 2
				vCaronaPos.X -= 0.1
				vCaronaPos.Y += 0.3
			ENDIF
		ELIF iCurrentPropertyID = PROPERTY_OFFICE_3
			IF iEntrance = 2
				vCaronaPos.X -= 0.3
				vCaronaPos.Y -= 0.5
			ENDIF
		ENDIF
		//CDEBUG1LN(DEBUG_SAFEHOUSE, "DRAW_BUZZER_LOCATE: drawing at ",vCaronaPos)
		DRAW_MARKER(MARKER_CYLINDER, vCaronaPos, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, vCaronaScale, iR, iG, iB, 255)

		//CDEBUG1LN(DEBUG_SAFEHOUSE, "Drawing marker at: ", mpProperties[iCurrentPropertyID].entrance[iEntrance].vBuzzerLoc)
		IF IS_PROPERTY_CLUBHOUSE(iCurrentPropertyID)
		AND mpProperties[iCurrentPropertyID].entrance[iEntrance].iType = ENTRANCE_TYPE_GARAGE
		AND (DOES_LOCAL_PLAYER_OWN_PROPERTY_IN_BUILDING(mpProperties[iCurrentPropertyID].iBuildingID,ownedDetails)
		OR DOES_PLAYER_HAVE_GROUP_ACCESS_TO_PROPERTY())
			vCaronaPos = GET_CLUBHOUSE_GARAGE_MARKER_LOCATION(iCurrentPropertyID)
			IF fGroundHeight[iEntrance] = 0.0
				GET_GROUND_Z_FOR_3D_COORD(vCaronaPos+ <<0.0, 0.0, 0.5>>, fGroundHeight[iEntrance])
			ENDIF
			GET_HUD_COLOUR(HUD_COLOUR_BLUE,iR, iG, iB, iA)
			vCaronaScale = <<0.75, 0.75, 0.75>>
			vCaronaPos.Z = (fGroundHeight[iEntrance] + -0.25 )
			DRAW_MARKER(MARKER_CYLINDER, vCaronaPos, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, vCaronaScale, iR, iG, iB, 255)
		ENDIF
	ENDIF

	
ENDPROC

//PROC DRAW_ENTRANCE_MARKER(INT iEntrance)
//	IF GET_SKYBLUR_STAGE() = SKYBLUR_NONE
//		INT iR, iG, iB, iA
//		GET_HUD_COLOUR(HUD_COLOUR_WHITE,iR, iG, iB, iA)
//		DRAW_MARKER(MARKER_CYLINDER, mpProperties[iCurrentPropertyID].entrance[iEntrance].vEntranceMarkerLoc+ <<0,0,-0.25>>, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, <<0.75, 0.75, 0.75>>, iR, iG, iB, 255)
//		//CDEBUG1LN(DEBUG_SAFEHOUSE, "Drawing marker at: ", mpProperties[iCurrentPropertyID].entrance[iEntrance].vBuzzerLoc)
//	ENDIF
//ENDPROC

FUNC BOOL IS_PLAYER_IN_BUZZER_LOCATION(INT iEntrance)//, INT iKnowState)
	//BOOl bDisplayBuzzerCorona
	//CDEBUG1LN(DEBUG_SAFEHOUSE, "iKnowState = ", iKnowState)
	//CDEBUG1LN(DEBUG_SAFEHOUSE, "mpProperties[iCurrentPropertyID].entrance[iEntrance].iType = ", mpProperties[iCurrentPropertyID].entrance[iEntrance].iType)
//	IF IS_BIT_SET(iKnowState,1)
//	AND mpProperties[iCurrentPropertyID].entrance[iEntrance].iType = ENTRANCE_TYPE_HOUSE
//		bDisplayBuzzerCorona = TRUE
//	ENDIF
//	IF IS_BIT_SET(iKnowState,2)
//	AND mpProperties[iCurrentPropertyID].entrance[iEntrance].iType = ENTRANCE_TYPE_GARAGE
//		bDisplayBuzzerCorona = TRUE
//	ENDIF
	//CDEBUG1LN(DEBUG_SAFEHOUSE, "IS_PLAYER_IN_BUZZER_LOCATION: Checking player is in buzzer for entrance #", iEntrance, " (FC= ",GET_FRAME_COUNT())
	//IF bDisplayBuzzerCorona
	VECTOR vLocateSize = <<1, 1, 2>>
	IF mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_BUILDING_23
		vLocateSize = <<1.6, 1.6, 2>>
	ENDIF
	IF mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_BUILDING_60
	OR mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_BUILDING_62
		vLocateSize = <<0.75, 0.75, 2>>
	ENDIF
	IF mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_BUILDING_1
	AND mpProperties[iCurrentPropertyID].entrance[iEntrance].iType = ENTRANCE_TYPE_HOUSE
		vLocateSize = <<1.6, 1.6, 2>>
	ENDIF
	
	IF mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_OFFICE_BUILDING_3
	AND mpProperties[iCurrentPropertyID].entrance[iEntrance].iType = ENTRANCE_TYPE_HOUSE
		vLocateSize = <<1.6, 1.6, 2>>
	ENDIF
	
	IF (mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_OFFICE_BUILDING_1
	OR mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_OFFICE_BUILDING_2
	OR mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_OFFICE_BUILDING_3)
	AND mpProperties[iCurrentPropertyID].entrance[iEntrance].iType = ENTRANCE_TYPE_GARAGE
		vLocateSize = <<1.6, 1.6, 2>>
	ENDIF
	
	IF NOT IS_PLAYER_TELEPORT_ACTIVE()
	AND NOT IS_PLAYER_IN_CORONA()
	AND NOT IS_POST_MISSION_CLEANUP_REQUIRED_OR_ACTIVE(PLAYER_ID())
	AND NOT IS_BIT_SET(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_AcceptedInvite)
		//Account for Property 3
		IF (mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_BUILDING_3 
		AND mpProperties[iCurrentPropertyID].entrance[iEntrance].iType = ENTRANCE_TYPE_HOUSE
		AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(),<<-1441.711182,-543.508911,33.242393>>,<<-1440.725586,-544.946777,36.492393>>,1.750000))
		//Account for property Stilt 8A
		OR (mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_STILT_BUILDING_8_A 
		AND mpProperties[iCurrentPropertyID].entrance[iEntrance].iType = ENTRANCE_TYPE_GARAGE
		AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(),<<-746.749756,601.368530,141.274338>>,<<-747.467041,602.151794,143.575226>>  ,2.060000))
		//All other properties
		OR ((mpProperties[iCurrentPropertyID].iBuildingID != MP_PROPERTY_BUILDING_3 OR mpProperties[iCurrentPropertyID].entrance[iEntrance].iType != ENTRANCE_TYPE_HOUSE)
		AND ((mpProperties[iCurrentPropertyID].iBuildingID != MP_PROPERTY_STILT_BUILDING_8_A OR mpProperties[iCurrentPropertyID].entrance[iEntrance].iType != ENTRANCE_TYPE_GARAGE)
		AND IS_ENTITY_AT_COORD(PLAYER_PED_ID(),mpProperties[iCurrentPropertyID].entrance[iEntrance].vBuzzerLoc,vLocateSize,FALSE,TRUE,TM_ON_FOOT))) 
			SET_BIT(iLocalBS2,LOCAL_BS2_bKeepWantedFlag)
			SET_BIT(iLocalBS2,LOCAL_BS2_KeepInvalidText)
			//CDEBUG1LN(DEBUG_SAFEHOUSE, "SET_BIT(iLocalBS2,LOCAL_BS2_bKeepWantedFlag) buzzer locate(FC= ",GET_FRAME_COUNT())
			IF IS_LOCAL_PLAYER_ENTERING_OR_EXITING_A_VEHICLE()
				#IF IS_DEBUG_BUILD
				IF db_bFullDebug
					CDEBUG1LN(DEBUG_SAFEHOUSE, "IS_PLAYER_IN_BUZZER_LOCATION- IS_LOCAL_PLAYER_ENTERING_OR_EXITING_A_VEHICLE")
				ENDIF
				#ENDIF
				RETURN FALSE
			ENDIF
			IF IS_PLAYER_GETTING_IN_OR_OUT_OF_VEHICLE(PLAYER_ID())
				#IF IS_DEBUG_BUILD
				IF db_bFullDebug
					CDEBUG1LN(DEBUG_SAFEHOUSE, "IS_PLAYER_IN_BUZZER_LOCATION- IS_LOCAL_PLAYER_ENTERING_OR_EXITING_A_VEHICLE")
				ENDIF
				#ENDIF
				RETURN FALSE
			ENDIF
			IF IS_PED_RAGDOLL(PLAYER_PED_ID())
				#IF IS_DEBUG_BUILD
				IF db_bFullDebug
					CDEBUG1LN(DEBUG_SAFEHOUSE, "IS_PLAYER_IN_BUZZER_LOCATION- IS_PED_RAGDOLL")
				ENDIF
				#ENDIF
				RETURN FALSE
			ENDIF
			//IF IS_ENTERING_DISABLED_DUE_TO_EXIT_WARP()
			//	RETURN FALSE
			//ENDIF
			IF NOT HAS_NET_TIMER_STARTED(tempPropertyExtMenu.exitDoorDelay)
			OR HAS_NET_TIMER_EXPIRED_READ_ONLY(tempPropertyExtMenu.exitDoorDelay,2000,TRUE)
				#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_SAFEHOUSE, "IS_PLAYER_IN_BUZZER_LOCATION- TRUE")
				#ENDIF
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD

		IF IS_PLAYER_TELEPORT_ACTIVE()
			CDEBUG1LN(DEBUG_SAFEHOUSE, "IS_PLAYER_IN_BUZZER_LOCATION- IS_PLAYER_TELEPORT_ACTIVE")
		ENDIF
		IF IS_PLAYER_IN_CORONA()
			CDEBUG1LN(DEBUG_SAFEHOUSE, "IS_PLAYER_IN_BUZZER_LOCATION- IS_PLAYER_IN_CORONA")
		ENDIF
		IF IS_POST_MISSION_CLEANUP_REQUIRED_OR_ACTIVE(PLAYER_ID())
			CDEBUG1LN(DEBUG_SAFEHOUSE, "IS_PLAYER_IN_BUZZER_LOCATION- IS_POST_MISSION_CLEANUP_REQUIRED_OR_ACTIVE")	
		ENDIF
		IF IS_BIT_SET(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_AcceptedInvite)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "IS_PLAYER_IN_BUZZER_LOCATION- biITA_AcceptedInvite")
		ENDIF

		#ENDIF
		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),mpProperties[iCurrentPropertyID].entrance[iEntrance].vBuzzerLoc,vLocateSize,FALSE,TRUE,TM_ON_FOOT)
			SET_BIT(iLocalBS2,LOCAL_BS2_bKeepWantedFlag)
			SET_BIT(iLocalBS2,LOCAL_BS2_KeepInvalidText)
			//CDEBUG1LN(DEBUG_SAFEHOUSE, "SET_BIT(iLocalBS2,LOCAL_BS2_bKeepWantedFlag) buzzer locate (TELEPORT ACTIVE)(FC= ",GET_FRAME_COUNT())
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC ADD_MENU_OPTION_FOR_PLAYER(INT& iMenuCounter, PLAYER_INDEX playerID, BOOL bRangBuzzer)
//	IF NOT bAuthMenu
		//IF mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iType = ENTRANCE_TYPE_GARAGE
			ADD_MENU_ITEM_TEXT(iMenuCounter, "MP_PROP_MENU1", 1,TRUE)
		//ELSE
		//	ADD_MENU_ITEM_TEXT(iMenuCounter, "MP_PROP_MENU1b", 1,TRUE)
		//ENDIF
		//ADD_MENU_ITEM_TEXT_COMPONENT_PLAYER_NAME(GET_PLAYER_NAME(playerID))
		ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL(GET_PLAYER_NAME(playerID))
		IF bRangBuzzer
			ADD_MENU_ITEM_TEXT(iMenuCounter, "", 1,TRUE)
			ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_TICK)
		ENDIF
//	ELSE
//		ADD_MENU_ITEM_TEXT(iMenuCounter, "STRING", 1,TRUE)
//		ADD_MENU_ITEM_TEXT_COMPONENT_PLAYER_NAME(GET_PLAYER_NAME(playerID))
//		IF bRangBuzzer
//			ADD_MENU_ITEM_TEXT(iMenuCounter, "", 1,TRUE)
//			ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_TICK)
//		ENDIF
//	ENDIF
	iMenuCounter++
ENDPROC

PROC CHECK_AVAILABLE_INTERIOR_COUNT()
	//Get a count of the interior variations not disabled by tuneables
	INT j
	iAvailable_Interior_var_count = 0
	REPEAT GET_MAX_INTERIOR_VARIATIONS_FOR_PROPERTY(iCustomApartmentTooPurchase) j
		IF NOT DISABLE_INTERIOR_VARIATION(j, iCustomApartmentTooPurchase)
			CDEBUG1LN(DEBUG_AMBIENT, "CHECK_AVAILABLE_INTERIOR_COUNT: Check availability of interior variations: ", j, " value: ", (j))
			ciPI_TYPE_M2_APT_VAR_AVAILABLE_OPTIONS[iAvailable_Interior_var_count] = (j)
			iAvailable_Interior_var_count ++
		ENDIF
	ENDREPEAT
	
	IF ciPI_TYPE_M2_APT_VAR_AVAILABLE_OPTIONS[0] = 0
	ENDIF
	CDEBUG1LN(DEBUG_AMBIENT, "iAvailable_Interior_var_count: ", iAvailable_Interior_var_count)
ENDPROC

PROC SETUP_PROPERTY_OPTIONS_MENU(INT iMenuType)

	CHECK_AVAILABLE_INTERIOR_COUNT()
	UPDATE_PROPERTY_VALUES()
	
	INT iMenuCounter = 0
	//INT iTradeIn
	
	INT i, iBuildingLoop
	//GAMER_HANDLE tempHandle
//	BOOL addOptionForPlayer
	PLAYER_INDEX playerID
	//NETWORK_CLAN_DESC myClan
	//NETWORK_CLAN_DESC otherClan
	INT iPropertyLoopID
	INT iFirstOfficeGarageProperty
	
	
	BOOL bHasPropertyInBuilding
	tempPropertyExtMenu.iAllNearby = -1
	tempPropertyExtMenu.iAllFriendsCrew = -1
	tempPropertyExtMenu.iOrganisationNearby = -1
	tempPropertyExtMenu.iAllRoofOption = -1
	tempPropertyExtMenu.iMaxVertSel = 0
	
	CLEAR_MENU_DATA()
	REPEAT NUM_NETWORK_PLAYERS+4 i
		tempPropertyExtMenu.menuPlayerArray[i]= INVALID_PLAYER_INDEX()
		tempPropertyExtMenu.iPropertyID[i] = 0
	ENDREPEAT
	
	IF iCurrentPropertyID > 0
	AND iCurrentPropertyID > MAX_MP_PROPERTIES
		BUILDING_PROPERTIES buildingProperties
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: iCurrentPropertyID is out of range?? : ",iCurrentPropertyID)
		SCRIPT_ASSERT("AM_MP_PROPERTY_EXT: iCurrentPropertyID is out of range??")
		REPEAT MP_PROPERTY_BUILDING_MAX i
			IF ARE_VECTORS_ALMOST_EQUAL(vWorldPointLoc,GET_MP_PROPERTY_BUILDING_WORLD_POINT(i),5)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Launched for property building #", i)
				GET_BUILDING_PROPERTIES(buildingProperties,i)
				iCurrentPropertyID = buildingProperties.iPropertiesInBuilding[0]
				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: iCurrentPropertyID is out of range setting to : ",iCurrentPropertyID)
			ENDIF
		ENDREPEAT
	ENDIF
	
	i = 0
	TEXT_LABEL_15 tlShortPropertyName
	IF (iCurrentMenuType = MENU_TYPE_PURCHASE OR iCurrentMenuType = MENU_TYPE_REPLACE_PROPERTY OR iCurrentMenuType = MENU_TYPE_SELECT_CUSTOM_INTERIOR)
	AND IS_BIT_SET(tempPropertyExtMenu.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
		REMOVE_MENU_HELP_KEYS()
		ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_ACCEPT, "BB_SELECT")
		ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_CANCEL, "BB_BACK")
		SET_CURRENT_MENU_ITEM(-1)
	ELSE
		SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT,MENU_ITEM_TEXT)
		SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT, FONT_RIGHT)
		SET_MENU_ITEM_TOGGLEABLE(FALSE, FALSE)	
		
		SWITCH iMenuType
			CASE MENU_TYPE_SELECT_CUSTOM_INTERIOR
				IF IS_PROPERTY_CUSTOM_APARTMENT(iCustomApartmentTooPurchase)
					SET_MENU_TITLE("PM_APT_TVARIANT")
				ELIF IS_PROPERTY_OFFICE(iCustomApartmentTooPurchase)
					SET_MENU_TITLE("PM_OFF_TVARIANT")
				ENDIF
				INT iIter
				INT IMenuItemCount
				REPEAT GET_MAX_INTERIOR_VARIATIONS_FOR_PROPERTY(iCustomApartmentTooPurchase) iIter
					IF NOT DISABLE_INTERIOR_VARIATION(iIter, iCustomApartmentTooPurchase)
						//TEXT_LABEL_15 tlInteriorLabel
						IF IS_PROPERTY_CUSTOM_APARTMENT(iCustomApartmentTooPurchase)
							 tlShortPropertyName = "PM_APT_VAR_"
						ELIF IS_PROPERTY_OFFICE(iCustomApartmentTooPurchase)
							 tlShortPropertyName = "PM_OFF_VAR_"
						ENDIF
						 tlShortPropertyName += (ciPI_TYPE_M2_APT_VAR_AVAILABLE_OPTIONS[IMenuItemCount])
						ADD_MENU_ITEM_TEXT(IMenuItemCount, tlShortPropertyName,0,TRUE)
						IMenuItemCount ++
					ENDIF
				ENDREPEAT
				tempPropertyExtMenu.iMaxVertSel = IMenuItemCount
				SET_CURRENT_MENU_ITEM(tempPropertyExtMenu.iCurVertSel)
				
				REMOVE_MENU_HELP_KEYS()
				ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_ACCEPT, "BB_SELECT")
				ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_CANCEL, "BB_BACK")
			BREAK
			CASE MENU_TYPE_PURCHASE
				IF mpProperties[iCurrentPropertyID].iType = PROPERTY_TYPE_GARAGE
					SET_MENU_TITLE("MP_PROP_GEN0b")
				ELIF mpProperties[iCurrentPropertyID].iType = PROPERTY_TYPE_OFFICE
					SET_MENU_TITLE("MP_PROP_GEN0c")
				ELSE
					SET_MENU_TITLE("MP_PROP_GEN0")
				ENDIF
				REPEAT mpProperties[iCurrentPropertyID].building.iNumProperties i
					iPropertyLoopID = mpProperties[iCurrentPropertyID].building.iPropertiesInBuilding[i]
					IF NOT IS_PROPERTY_SALE_BLOCKED_BY_TUNEABLES(iCurrentPropertyID)
						
						INT iRebateSlot
						iRebateSlot = -1
						IF NOT DOES_LANGUAGE_NEED_CONDENSED_FONT_FOR_PROPERTY_NAME()
							IF NOT DOES_LANGUAGE_NEED_TO_USE_SHORT_PROPERTY_NAME()
							AND NOT DOES_PROPERTY_NEED_TO_USE_SHORT_NAME(iPropertyLoopID)
								IF IS_SALE_ACTIVE_FOR_PROPERTY(iPropertyLoopID)
								AND NOT DOES_LOCAL_PLAYER_OWN_PROPERTY(iPropertyLoopID)
									CDEBUG1LN(DEBUG_SAFEHOUSE, "SETTING OPTIONS: iCurrentPropertyID ", iCurrentPropertyID)
									ADD_MENU_ITEM_TEXT(iMenuCounter,mpProperties[iPropertyLoopID].tl_PropertyName,1,TRUE)
									ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_DISCOUNT)
								ELIF IS_REBATE_ACTIVE_FOR_PROPERTY(mpProperties[iPropertyLoopID].tl_PropertyName, iRebateSlot)
								AND NOT DOES_LOCAL_PLAYER_OWN_PROPERTY(iPropertyLoopID)
									CDEBUG1LN(DEBUG_SAFEHOUSE, "SETTING OPTIONS: iCurrentPropertyID ", iCurrentPropertyID)
									ADD_MENU_ITEM_TEXT(iMenuCounter,mpProperties[iPropertyLoopID].tl_PropertyName,1,TRUE)
									ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_DISCOUNT)
								ELSE
									CDEBUG1LN(DEBUG_SAFEHOUSE, "SETTING OPTIONS: iCurrentPropertyID ", iCurrentPropertyID)
									ADD_MENU_ITEM_TEXT(iMenuCounter,mpProperties[iPropertyLoopID].tl_PropertyName,0,TRUE)
								ENDIF
							ELSE
								IF IS_SALE_ACTIVE_FOR_PROPERTY(iPropertyLoopID)
								AND NOT DOES_LOCAL_PLAYER_OWN_PROPERTY(iPropertyLoopID)
									tlShortPropertyName = mpProperties[iPropertyLoopID].tl_PropertyName
									tlShortPropertyName += "S"
									ADD_MENU_ITEM_TEXT(iMenuCounter,tlShortPropertyName,1,TRUE)
									ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_DISCOUNT)
								ELIF IS_REBATE_ACTIVE_FOR_PROPERTY(mpProperties[iPropertyLoopID].tl_PropertyName, iRebateSlot)
								AND NOT DOES_LOCAL_PLAYER_OWN_PROPERTY(iPropertyLoopID)
									tlShortPropertyName = mpProperties[iPropertyLoopID].tl_PropertyName
									tlShortPropertyName += "S"
									ADD_MENU_ITEM_TEXT(iMenuCounter,tlShortPropertyName,1,TRUE)
									ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_DISCOUNT)
								ELSE
									tlShortPropertyName = mpProperties[iPropertyLoopID].tl_PropertyName
									tlShortPropertyName += "S"
									ADD_MENU_ITEM_TEXT(iMenuCounter,tlShortPropertyName,0,TRUE)
								ENDIF
							ENDIF
						ELSE
							IF IS_SALE_ACTIVE_FOR_PROPERTY(iPropertyLoopID)
							AND NOT DOES_LOCAL_PLAYER_OWN_PROPERTY(iPropertyLoopID)
								ADD_MENU_ITEM_TEXT(iMenuCounter,"PIM_DNAME",2,TRUE)
								ADD_MENU_ITEM_TEXT_COMPONENT_PLAYER_NAME(GET_FILENAME_FOR_AUDIO_CONVERSATION(mpProperties[iPropertyLoopID].tl_PropertyName), FALSE, TRUE)
								ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_DISCOUNT)
							ELIF IS_REBATE_ACTIVE_FOR_PROPERTY(mpProperties[iPropertyLoopID].tl_PropertyName, iRebateSlot)
							AND NOT DOES_LOCAL_PLAYER_OWN_PROPERTY(iPropertyLoopID)
								ADD_MENU_ITEM_TEXT(iMenuCounter,"PIM_DNAME",2,TRUE)
								ADD_MENU_ITEM_TEXT_COMPONENT_PLAYER_NAME(GET_FILENAME_FOR_AUDIO_CONVERSATION(mpProperties[iPropertyLoopID].tl_PropertyName), FALSE, TRUE)
								ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_DISCOUNT)
							ELSE
								ADD_MENU_ITEM_TEXT(iMenuCounter,"PIM_DNAME",1,TRUE)
								ADD_MENU_ITEM_TEXT_COMPONENT_PLAYER_NAME(GET_FILENAME_FOR_AUDIO_CONVERSATION(mpProperties[iPropertyLoopID].tl_PropertyName), FALSE, TRUE)
							ENDIF
						ENDIF
						IF NOT DOES_LOCAL_PLAYER_OWN_PROPERTY(iPropertyLoopID)
							IF NOT SHOULD_PROPERTY_BE_FREE_FOR_PLAYER(iPropertyLoopID)
								ADD_MENU_ITEM_TEXT(iMenuCounter,"CUST_GAR_COST",1,TRUE)
								ADD_MENU_ITEM_TEXT_COMPONENT_INT(CEIL(GET_VALUE_OF_PROPERTY(iPropertyLoopID)*g_sMPTunables.fPropertyMultiplier))
							ELSE
								ADD_MENU_ITEM_TEXT(iMenuCounter,"ITEM_FREE")
							ENDIF
						ELSE
							ADD_MENU_ITEM_TEXT(iMenuCounter,"",1,TRUE)
							ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_TICK)
						ENDIF
						tempPropertyExtMenu.iSelectableProperties[iMenuCounter] = iPropertyLoopID
						CDEBUG1LN(DEBUG_SAFEHOUSE, "tempPropertyExtMenu.iSelectableProperties[",iMenuCounter,"] = ", iPropertyLoopID)
						iMenuCounter++
					ELSE
						CDEBUG1LN(DEBUG_SAFEHOUSE, " Disable purchase (tuneables) of tempPropertyExtMenu.iSelectableProperties[",iMenuCounter,"] = ", iPropertyLoopID)
					ENDIF
					IF iSelectedPropertyToBuy > 0
						IF iSelectedPropertyToBuy = iPropertyLoopID
							tempPropertyExtMenu.iCurVertSel = i
							CDEBUG1LN(DEBUG_SAFEHOUSE, "Setting current selection to be ", tempPropertyExtMenu.iCurVertSel)
						ENDIF
					ENDIF
				ENDREPEAT
				iSelectedPropertyToBuy = 0
				tempPropertyExtMenu.iMaxVertSel = iMenuCounter
				SET_CURRENT_MENU_ITEM(tempPropertyExtMenu.iCurVertSel)
				
				CLEAR_BIT(tempPropertyExtMenu.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
				//Once
				REMOVE_MENU_HELP_KEYS()
				ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_ACCEPT, "BB_SELECT")
				ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_CANCEL, "BB_BACK")
			BREAK
			CASE MENU_TYPE_BUZZER
				IF IS_PROPERTY_OFFICE(iCurrentPropertyID)
					IF mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iType = ENTRANCE_TYPE_GARAGE
						SET_MENU_TITLE("PROP_OFFG_M_1")
					ELSE
						SET_MENU_TITLE("PROP_OFF_M_1")
					ENDIF
				ELIF mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iType = ENTRANCE_TYPE_GARAGE
					SET_MENU_TITLE("MP_PROP_GEN1b")
				ELSE
					IF IS_PROPERTY_CLUBHOUSE(iCurrentPropertyID)
						SET_MENU_TITLE("PROP_CLU_M_1")
					ELSE
						SET_MENU_TITLE("MP_PROP_GEN1")
					ENDIF
				ENDIF
				IF DOES_LOCAL_PLAYER_OWN_PROPERTY_IN_BUILDING(mpProperties[iCurrentPropertyID].iBuildingID,ownedDetails)
					IF ownedDetails.iNumProperties = 1
					AND NOT IS_PROPERTY_OFFICE_GARAGE(ownedDetails.iReturnedPropertyIDs[0])
						IF PROPERTY_NEAR_PLYS_OPT_AVAILABLE(GET_PROPERTY_ENTRANCE_APPROXIMATE_LOCATION(iCurrentPropertyID,iEntrancePlayerIsUsing),tempPropertyExtMenu.bFriendsNearby, tempPropertyExtMenu.bOrganisationNearby, iCurrentPropertyID,iEntrancePlayerIsUsing)
							IF tempPropertyExtMenu.bOrganisationNearby
								IF IS_PROPERTY_CLUBHOUSE(iCurrentPropertyID)
									IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID())
									 	ADD_MENU_ITEM_TEXT(iMenuCounter,"PROP_HEI_CLUB",0,TRUE)
									ELSE
										ADD_MENU_ITEM_TEXT(iMenuCounter,"PROP_HEI_E_3",0,TRUE)
									ENDIF
								ELSE
									ADD_MENU_ITEM_TEXT(iMenuCounter,"PROP_HEI_E_3",0,TRUE)
								ENDIF
								tempPropertyExtMenu.menuPlayerArray[iMenuCounter]= PLAYER_ID()
								tempPropertyExtMenu.iPropertyID[iMenuCounter] = ownedDetails.iReturnedPropertyIDs[0]
								tempPropertyExtMenu.iOrganisationNearby = iMenuCounter	
								iMenuCounter++
							ENDIF	
							IF Is_Player_Currently_On_MP_Heist(PLAYER_ID())
						    OR Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID())
						    OR Is_Player_Currently_On_MP_Contact_Mission(PLAYER_ID())
								ADD_MENU_ITEM_TEXT(iMenuCounter,"PROP_HEI_E_0",0,TRUE)
								tempPropertyExtMenu.menuPlayerArray[iMenuCounter]= PLAYER_ID()
								tempPropertyExtMenu.iPropertyID[iMenuCounter] = ownedDetails.iReturnedPropertyIDs[0]
								tempPropertyExtMenu.iAllNearby = iMenuCounter
								iMenuCounter++
							ENDIF
							IF tempPropertyExtMenu.bFriendsNearby
								ADD_MENU_ITEM_TEXT(iMenuCounter,"PROP_HEI_E_2",0,TRUE)
								tempPropertyExtMenu.menuPlayerArray[iMenuCounter]= PLAYER_ID()
								tempPropertyExtMenu.iPropertyID[iMenuCounter] = ownedDetails.iReturnedPropertyIDs[0]
								tempPropertyExtMenu.iAllFriendsCrew = iMenuCounter	
								iMenuCounter++
							ENDIF
						ENDIF
					ENDIF
					REPEAT ownedDetails.iNumProperties i
						IF ownedDetails.iReturnedPropertyIDs[i] > 0
							IF mpProperties[ownedDetails.iReturnedPropertyIDs[i]].iBuildingID = mpProperties[iCurrentPropertyID].iBuildingID
							AND NOT (IS_PROPERTY_OFFICE(ownedDetails.iReturnedPropertyIDs[i])
							AND mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iType = ENTRANCE_TYPE_GARAGE
							AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()))
								#IF FEATURE_GEN9_EXCLUSIVE
								IF IS_PLAYER_ON_MP_INTRO()
								AND IS_PROPERTY_OFFICE_GARAGE(ownedDetails.iReturnedPropertyIDs[i])
									CDEBUG1LN(DEBUG_SAFEHOUSE, "Skipping office garage for MP intro")
								ELSE
								#ENDIF
									IF IS_PROPERTY_OFFICE(ownedDetails.iReturnedPropertyIDs[i])
										ADD_MENU_ITEM_TEXT(iMenuCounter,"MP_PROP_OFFENT",0,TRUE)
									ELSE
										IF iFirstOfficeGarageProperty = 0
										AND IS_PROPERTY_OFFICE_GARAGE(ownedDetails.iReturnedPropertyIDs[i])
											iFirstOfficeGarageProperty = ownedDetails.iReturnedPropertyIDs[i]
										ENDIF
										IF NOT DOES_PROPERTY_NEED_TO_USE_SHORT_NAME(ownedDetails.iReturnedPropertyIDs[i])
											IF mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iType = ENTRANCE_TYPE_GARAGE
												//ADD_MENU_ITEM_TEXT(iMenuCounter,"MP_PROP_MENUG",0,TRUE)	
												ADD_MENU_ITEM_TEXT(iMenuCounter,mpProperties[ownedDetails.iReturnedPropertyIDs[i]].tl_PropertyName,0,TRUE)
											ELSE
												//ADD_MENU_ITEM_TEXT(iMenuCounter,"MP_PROP_MENU0",0,TRUE)
												ADD_MENU_ITEM_TEXT(iMenuCounter,mpProperties[ownedDetails.iReturnedPropertyIDs[i]].tl_PropertyName,0,TRUE)
											ENDIF
										ELSE
											tlShortPropertyName = mpProperties[ownedDetails.iReturnedPropertyIDs[i]].tl_PropertyName
											tlShortPropertyName += "S"
											IF mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iType = ENTRANCE_TYPE_GARAGE
												ADD_MENU_ITEM_TEXT(iMenuCounter,tlShortPropertyName,0,TRUE)
											ELSE
												ADD_MENU_ITEM_TEXT(iMenuCounter,tlShortPropertyName,0,TRUE)
											ENDIF
										ENDIF
									ENDIF
									tempPropertyExtMenu.menuPlayerArray[iMenuCounter]= PLAYER_ID()
									tempPropertyExtMenu.iPropertyID[iMenuCounter] = ownedDetails.iReturnedPropertyIDs[i]
									CDEBUG1LN(DEBUG_SAFEHOUSE, "tempPropertyExtMenu.iPropertyID[",iMenuCounter,"] = ", tempPropertyExtMenu.iPropertyID[iMenuCounter])
									iMenuCounter++
								#IF FEATURE_GEN9_EXCLUSIVE
								ENDIF
								#ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
					IF (IS_PROPERTY_OFFICE(iCurrentPropertyID)
					OR IS_PROPERTY_OFFICE_GARAGE(iCurrentPropertyID))
					AND DOES_PLAYER_OWNS_OFFICE_MOD_GARAGE(PLAYER_ID())
					AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(),TRUE)
					#IF FEATURE_GEN9_EXCLUSIVE
					AND NOT IS_PLAYER_ON_MP_INTRO()
					#ENDIF
						ADD_MENU_ITEM_TEXT(iMenuCounter,"PROP_OFFG_SHOP",0,TRUE)
						tempPropertyExtMenu.menuPlayerArray[iMenuCounter]= PLAYER_ID()
						tempPropertyExtMenu.iPropertyID[iMenuCounter] = iFirstOfficeGarageProperty
						tempPropertyExtMenu.iExitToOfficeFromGarageToMod = iMenuCounter
						CDEBUG1LN(DEBUG_SAFEHOUSE,"Adding modshop option")
						CDEBUG1LN(DEBUG_SAFEHOUSE, "tempPropertyExtMenu.iPropertyID[",iMenuCounter,"] = ", tempPropertyExtMenu.iPropertyID[iMenuCounter])
						iMenuCounter++
					ENDIF
				ENDIF
				
				
				i = 0
				IF NOT IS_BIT_SET(iLocalBS2,LOCAL_BS2_DoNotBuzzOthers)
				#IF FEATURE_GEN9_EXCLUSIVE
				AND NOT IS_PLAYER_ON_MP_INTRO()
				#ENDIF
					REPEAT NUM_NETWORK_PLAYERS i
						//addOptionForPlayer = FALSE
						playerID = INT_TO_PLAYERINDEX(i)
						//iBuddyLoop = 0
						IF playerID != PLAYER_ID()
							IF IS_NET_PLAYER_OK(playerID,FALSE)
								CDEBUG1LN(DEBUG_SAFEHOUSE, "SETUP_PROPERTY_OPTIONS_MENU: checking player: ", GET_PLAYER_NAME(playerID))
								CDEBUG1LN(DEBUG_SAFEHOUSE, "in property = ",GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iCurrentlyInsideProperty)
								CDEBUG1LN(DEBUG_SAFEHOUSE, "owned flag = ",IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iBS,PROPERTY_BROADCAST_BS_IN_OWNED_PROPERTY))
								bHasPropertyInBuilding = FALSE
								REPEAT mpProperties[iCurrentPropertyID].building.iNumProperties iBuildingLoop
									IF GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iCurrentlyInsideProperty = mpProperties[iCurrentPropertyID].building.iPropertiesInBuilding[iBuildingLoop]
									AND IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iBS,PROPERTY_BROADCAST_BS_IN_OWNED_PROPERTY)
									AND NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_EXITING_PROPERTY)
										bHasPropertyInBuilding = TRUE
										SET_BIT(iPlayersInBuilding, NATIVE_TO_INT(playerID))
										iPlayersPropertyNum[i] = GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iCurrentlyInsideProperty
		//								tempHandle = GET_GAMER_HANDLE_PLAYER(playerID)
		//								IF NETWORK_IS_FRIEND(tempHandle)	
		//									addOptionForPlayer = TRUE
		//								ENDIF
		//								IF NOT addOptionForPlayer
		//									/*REPEAT g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedBuddies.iNumBuddies iBuddyLoop
		//										IF NETWORK_ARE_HANDLES_THE_SAME(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedBuddies.BuddyGamerInfo[iBuddyLoop].Handle,tempHandle)
		//											addOptionForPlayer = TRUE
		//											iBuddyLoop = g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedBuddies.iNumBuddies
		//										ENDIF
		//									ENDREPEAT*/
		//									IF NOT addOptionForPlayer
		//										myClan = GET_PLAYER_CREW(PLAYER_ID())
		//										otherClan = GET_PLAYER_CREW(playerID)
		//										IF myClan.Id = otherClan.Id
		//											addOptionForPlayer = TRUE
		//										ENDIF
		//									ENDIF
		//								ENDIF
		//								
		//								IF addOptionForPlayer
											tempPropertyExtMenu.menuPlayerArray[iMenuCounter] = playerID
											IF IS_BIT_SET(iLocalBS,LOCAL_BS_AWAITING_BUZZER_RESPONSE)
											AND playerID = globalPropertyEntryData.ownerID
												tempPropertyExtMenu.iCurVertSel = iMenuCounter
												PRINTLN("SETUP_PROPERTY_OPTIONS_MENU: setting current vert selection to match player you are buzzing: ",tempPropertyExtMenu.iCurVertSel)
											ENDIF
											ADD_MENU_OPTION_FOR_PLAYER(iMenuCounter,playerID,FALSE)
										//ENDIF
										CDEBUG1LN(DEBUG_SAFEHOUSE, "SETUP_PROPERTY_OPTIONS_MENU: add option for ", GET_PLAYER_NAME(playerID))
									ENDIF
								ENDREPEAT
								IF NOT bHasPropertyInBuilding
									CLEAR_BIT(iPlayersInBuilding, NATIVE_TO_INT(playerID))
								ENDIF
							ELSE
								CDEBUG1LN(DEBUG_SAFEHOUSE, "SETUP_PROPERTY_OPTIONS_MENU: playerID: not ok",i)
								CLEAR_BIT(iPlayersInBuilding, NATIVE_TO_INT(playerID))
							ENDIF
						ENDIF
					ENDREPEAT
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "SETUP_PROPERTY_OPTIONS_MENU: LOCAL_BS2_DoNotBuzzOthers is true skipping player list")
				ENDIF
				
				IF iMenuCounter <= 0
					ADD_MENU_ITEM_TEXT(iMenuCounter,"PROP_LEAVE_B",0,TRUE)
				ENDIF
				
				tempPropertyExtMenu.iMaxVertSel = iMenuCounter
				SET_CURRENT_MENU_ITEM(tempPropertyExtMenu.iCurVertSel)
						
				REMOVE_MENU_HELP_KEYS()
				IF mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iType = ENTRANCE_TYPE_GARAGE
					//IF iMenuCounter > 0
						ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_ACCEPT, "BB_SELECT")
					//ENDIF
					ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_CANCEL, "BB_BACK")
				ELSE
					//IF iMenuCounter > 0
						ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "BB_SELECT")
					//ENDIF
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "BB_BACK")
				ENDIF
				
			BREAK
			CASE MENU_TYPE_REPLACE_PROPERTY
				SET_MENU_TITLE("MP_REP_PROP_0")
				REPEAT MAX_OWNED_PROPERTIES i
					IF i = PROPERTY_OWNED_SLOT_APT_6  
					AND NOT g_sMPtunables.bENABLE_BIKER_PROPERTY
					
					ELSE
						IF ( NOT IS_PROPERTY_SLOT_RESTRICTED_SLOT_ONLY(i) AND iReplaceSpecificPropertySlot = -1)
						OR (IS_PROPERTY_SLOT_RESTRICTED_SLOT_ONLY(i) AND iReplaceSpecificPropertySlot = i)
							iPropertyLoopID = GET_OWNED_PROPERTY(i)
							IF iPropertyLoopID > 0
								IF NOT DOES_PROPERTY_NEED_TO_USE_SHORT_NAME(iPropertyLoopID)
									ADD_MENU_ITEM_TEXT(iMenuCounter,mpProperties[iPropertyLoopID].tl_PropertyName,0,TRUE)
								ELSE
									//For custom apartments we need to use the shorter names
									tlShortPropertyName = mpProperties[iPropertyLoopID].tl_PropertyName
									tlShortPropertyName += "S"
									ADD_MENU_ITEM_TEXT(iMenuCounter,tlShortPropertyName,0,TRUE)
								ENDIF
								ADD_MENU_ITEM_TEXT(iMenuCounter,"CUST_GAR_COST",1,TRUE)
								ADD_MENU_ITEM_TEXT_COMPONENT_INT(GET_VALUE_OF_CURRENTLY_OWNED_PROPERTY(i))
								tempPropertyExtMenu.iSelectableProperties[iMenuCounter] = iPropertyLoopID
								tempPropertyExtMenu.iSelectablePropertySlots[iMenuCounter] = i
								iMenuCounter++
							ELSE
								IF i > 0
									IF IS_PROPERTY_SLOT_RESTRICTED_SLOT_ONLY(i) 
										IF iReplaceSpecificPropertySlot = i
											ADD_MENU_ITEM_TEXT(iMenuCounter,"MP_REP_PROP_1",0)
											tempPropertyExtMenu.iSelectableProperties[iMenuCounter] = 0
											tempPropertyExtMenu.iSelectablePropertySlots[iMenuCounter] = i
											iMenuCounter++
										ENDIF
									ELSE
										IF i <= PROPERTY_OWNED_SLOT_APT_5
											IF GET_OWNED_PROPERTY(i-1) > 0
												ADD_MENU_ITEM_TEXT(iMenuCounter,"MP_REP_PROP_1",0)
												tempPropertyExtMenu.iSelectableProperties[iMenuCounter] = 0
												tempPropertyExtMenu.iSelectablePropertySlots[iMenuCounter] = i
												iMenuCounter++
											ENDIF
										ELIF i = PROPERTY_OWNED_SLOT_APT_6
										AND g_sMPtunables.bENABLE_BIKER_PROPERTY
											IF GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_APT_5) > 0
												ADD_MENU_ITEM_TEXT(iMenuCounter,"MP_REP_PROP_1",0)
												tempPropertyExtMenu.iSelectableProperties[iMenuCounter] = 0
												tempPropertyExtMenu.iSelectablePropertySlots[iMenuCounter] = i
												iMenuCounter++
											ENDIF
										ELIF i = PROPERTY_OWNED_SLOT_APT_7
											IF GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_APT_6) > 0
												ADD_MENU_ITEM_TEXT(iMenuCounter,"MP_REP_PROP_1",0)
												tempPropertyExtMenu.iSelectableProperties[iMenuCounter] = 0
												tempPropertyExtMenu.iSelectablePropertySlots[iMenuCounter] = i
												iMenuCounter++
											ENDIF
										ELIF i = PROPERTY_OWNED_SLOT_APT_8
											IF GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_APT_7) > 0
												ADD_MENU_ITEM_TEXT(iMenuCounter,"MP_REP_PROP_1",0)
												tempPropertyExtMenu.iSelectableProperties[iMenuCounter] = 0
												tempPropertyExtMenu.iSelectablePropertySlots[iMenuCounter] = i
												iMenuCounter++
											ENDIF
										ELIF i = PROPERTY_OWNED_SLOT_APT_9
											IF GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_APT_8) > 0
												ADD_MENU_ITEM_TEXT(iMenuCounter,"MP_REP_PROP_1",0)
												tempPropertyExtMenu.iSelectableProperties[iMenuCounter] = 0
												tempPropertyExtMenu.iSelectablePropertySlots[iMenuCounter] = i
												iMenuCounter++
											ENDIF
										ELIF i = PROPERTY_OWNED_SLOT_APT_10  //MAX_OWNED_PROPERTIES //OLD PROPERTIES
											IF GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_APT_9) > 0
												ADD_MENU_ITEM_TEXT(iMenuCounter,"MP_REP_PROP_1",0)
												tempPropertyExtMenu.iSelectableProperties[iMenuCounter] = 0
												tempPropertyExtMenu.iSelectablePropertySlots[iMenuCounter] = i
												iMenuCounter++
											ENDIF
										#ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT

				tempPropertyExtMenu.iMaxVertSel = iMenuCounter
				SET_CURRENT_MENU_ITEM(tempPropertyExtMenu.iCurVertSel)
				
				//Once
				REMOVE_MENU_HELP_KEYS()
				ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_ACCEPT, "BB_SELECT")
				ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_CANCEL, "BB_BACK")
				ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_FRONTEND_DPAD_UD,"HUD_INPUT1C")
			BREAK
			CASE MENU_TYPE_PROPERTY_ENTRY_OPTIONS
				IF tempPropertyExtMenu.iExitToOfficeFromGarageToMod = 99
					SET_MENU_TITLE("PROP_OFFG_SHOP")
				ELSE
				SET_MENU_TITLE(mpProperties[tempPropertyExtMenu.iExitSelection].tl_PropertyName)
				ENDIF
							
				//IF PROPERTY_NEAR_FRIENDS_OPT_AVAILABLE
				IF PROPERTY_NEAR_PLYS_OPT_AVAILABLE(GET_PROPERTY_ENTRANCE_APPROXIMATE_LOCATION(iCurrentPropertyID,iEntrancePlayerIsUsing),tempPropertyExtMenu.bFriendsNearby, tempPropertyExtMenu.bOrganisationNearby,  iCurrentPropertyID,iEntrancePlayerIsUsing)
					IF tempPropertyExtMenu.bOrganisationNearby
						ADD_MENU_ITEM_TEXT(iMenuCounter,"PROP_HEI_E_3",0,TRUE)
						tempPropertyExtMenu.menuPlayerArray[iMenuCounter]= PLAYER_ID()
						tempPropertyExtMenu.iPropertyID[iMenuCounter] = tempPropertyExtMenu.iExitSelection
						tempPropertyExtMenu.iOrganisationNearby = iMenuCounter	
						iMenuCounter++
					ENDIF	
					IF Is_Player_Currently_On_MP_Heist(PLAYER_ID())
				    OR Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID())
				    OR Is_Player_Currently_On_MP_Contact_Mission(PLAYER_ID())
						ADD_MENU_ITEM_TEXT(iMenuCounter,"PROP_HEI_E_0",0,TRUE)
						tempPropertyExtMenu.menuPlayerArray[iMenuCounter]= PLAYER_ID()
						tempPropertyExtMenu.iPropertyID[iMenuCounter] = tempPropertyExtMenu.iExitSelection
						tempPropertyExtMenu.iAllNearby = iMenuCounter
						iMenuCounter++
					ENDIF
					IF tempPropertyExtMenu.bFriendsNearby
						ADD_MENU_ITEM_TEXT(iMenuCounter,"PROP_HEI_E_2",0,TRUE)
						tempPropertyExtMenu.menuPlayerArray[iMenuCounter]= PLAYER_ID()
						tempPropertyExtMenu.iPropertyID[iMenuCounter] = tempPropertyExtMenu.iExitSelection
						tempPropertyExtMenu.iAllFriendsCrew = iMenuCounter	
						iMenuCounter++
					ENDIF
				ENDIF
				
				ADD_MENU_ITEM_TEXT(iMenuCounter,"PROP_HEI_E_1",0,TRUE)
				tempPropertyExtMenu.menuPlayerArray[iMenuCounter]= PLAYER_ID()
				tempPropertyExtMenu.iPropertyID[iMenuCounter] = tempPropertyExtMenu.iExitSelection
				iMenuCounter++
				
				tempPropertyExtMenu.iMaxVertSel = iMenuCounter
				SET_CURRENT_MENU_ITEM(tempPropertyExtMenu.iCurVertSel)
				
				REMOVE_MENU_HELP_KEYS()
				ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_ACCEPT , "BB_SELECT")
				ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_CANCEL, "BB_BACK")
				ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_FRONTEND_DPAD_UD,"HUD_INPUT1C")
			BREAK
		ENDSWITCH
	ENDIF
	NET_PRINT("SETUP_PROPERTY_OPTIONS_MENU: setup menu") NET_NL()
ENDPROC

PROC SETUP_PROPERTY_MENU_DESCRIPTION_BIT(INT iBit)
	REINIT_NET_TIMER(tempPropertyExtMenu.menuDescriptionTimer)
	SET_BIT(tempPropertyExtMenu.iMenuDescriptionBS,iBit)
ENDPROC

PROC DO_WARNING_MESSAGE(INT iValue, BOOL bSmallerGarage)
//	IF NOT IS_BIT_SET(iLocalBS,LOCAL_BS_PURCHASE_MENU_SHOWING_WARN)
//		BEGIN_SCALEFORM_MOVIE_METHOD(warningscreen, "SHOW_POPUP_WARNING")
//			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
//			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BRSCRWTEX")   //title 2nd
//			
//			IF bSmallerGarage
//				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BRDISPROP2") //3rd
//			ELSE
//				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BRDISPROP") //3rd
//			ENDIF
//			
//	        BEGIN_TEXT_COMMAND_SCALEFORM_STRING("BRSHETPROSU") // 4th
//			
//			INT ival = iValue
//			IF ival < 0
//				ival *= -1			
//			ENDIF
//			IF iValue < 0  //3rd
//				ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL("BRSHETMAK") 
//			ELSE
//				ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL("BRSHETPRSA") 
//			ENDIF
//	        ADD_TEXT_COMPONENT_FORMATTED_INTEGER(ival, INTEGER_FORMAT_COMMA_SEPARATORS)
//	        END_TEXT_COMMAND_SCALEFORM_STRING()
//			
//			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)			   //background 5th
//		END_SCALEFORM_MOVIE_METHOD() 	
//		SET_BIT(iLocalBS,LOCAL_BS_PURCHASE_MENU_SHOWING_WARN)
//		
//	ENDIF
//	DRAW_SCALEFORM_MOVIE_FULLSCREEN(warningscreen,255,255,255,0)
	STRING pHeaderTextLabel
	STRING pBodyTextLabel
	STRING pBodySubTextLabel = NULL
	INT NumberToInsert = -1
	STRING pFirstSubStringTextLabel = NULL
	
	pHeaderTextLabel = ("BRSCRWTEX")				//"Alert"
	IF bSmallerGarage
		pBodyTextLabel = ("BRDISPROP2B1")			//"You already own a property, purchasing this one will trade in your old one.~n~
													// You will also lose access to some of your stored vehicles as the Garage is smaller.  The trade in will ~a~ you $~1~.""
		pBodySubTextLabel = ("BRSHETPROSUB1")		//"Do you wish to continue with this purchase?
	ELSE
		pBodyTextLabel = ("BRDISPROPB1")			//"You already own a property, purchasing this one will trade in your old one.  The trade in will ~a~ you $~1~.""
		pBodySubTextLabel = ("BRSHETPROSUB1")		//"Do you wish to continue with this purchase?
	ENDIF
	NumberToInsert = iValue
	IF NumberToInsert < 0
		NumberToInsert *= -1			
	ENDIF
	IF iValue < 0  //3rd
		pFirstSubStringTextLabel = ("BRSHETMAK")	//make
	ELSE
		pFirstSubStringTextLabel = ("BRSHETPRSA")	//save
	ENDIF
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "DO_WARNING_MESSAGE(pHeaderTextLabel:\"",
			pHeaderTextLabel, "\", pBodyTextLabel:\"",
			pBodyTextLabel, "\", pBodySubTextLabel:\"",
			pBodySubTextLabel, "\", NumberToInsert:\"",
			NumberToInsert, "\", pFirstSubStringTextLabel:\"",
			pFirstSubStringTextLabel, "\")")

	warpInControl.iWarningScreenButtonBS = FE_WARNING_OKCANCEL
	SET_WARNING_MESSAGE_WITH_HEADER_AND_SUBSTRING_FLAGS(pHeaderTextLabel,
			pBodyTextLabel,
			warpInControl.iWarningScreenButtonBS,
			pBodySubTextLabel,
			TRUE,
			NumberToInsert,
			WARNING_MESSAGE_DEFAULT,
			pFirstSubStringTextLabel)
ENDPROC

PROC HANDLE_MENU_DESCRIPTION(INT iMenuType)
	INT iTradeIn
	//item description
	INT iSelectedProperty
	BOOL bSmallerGarage
	TEXT_LABEL_15 tlDisc
	SWITCH iMenuType
		CASE MENU_TYPE_SELECT_CUSTOM_INTERIOR
			IF IS_BIT_SET(tempPropertyExtMenu.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
				SET_CURRENT_MENU_ITEM_DESCRIPTION("CUST_GAR_M_CON",100)
				ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING(mpProperties[iCustomApartmentTooPurchase].tl_PropertyName)
				ADD_CURRENT_MENU_ITEM_DESCRIPTION_INT(CEIL(GET_VALUE_OF_PROPERTY(iCustomApartmentTooPurchase)*g_sMPTunables.fPropertyMultiplier))
			ELSE
				IF IS_PROPERTY_CUSTOM_APARTMENT(iCustomApartmentTooPurchase)
					tlDisc = "PM_APT_P_DESC_"
				ELIF IS_PROPERTY_OFFICE(iCustomApartmentTooPurchase)
					tlDisc = "PM_OFF_P_DESC_"
				ENDIF
				tlDisc += ciPI_TYPE_M2_APT_VAR_AVAILABLE_OPTIONS[tempPropertyExtMenu.iCurVertSel]
				
				SET_CURRENT_MENU_ITEM_DESCRIPTION(tlDisc)
				
				IF tempPropertyExtMenu.iCurVertSel > GET_MAX_INTERIOR_VARIATIONS_FOR_PROPERTY(iCurrentPropertyID)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_MENU_DESCRIPTION Selection not valid for custom interior type: iCurVertSel ", tempPropertyExtMenu.iCurVertSel)
				ENDIF
			ENDIF
		BREAK
		CASE MENU_TYPE_PURCHASE
			IF IS_BIT_SET(iLocalBS,LOCAL_BS_SETUP_MENU)
				IF tempPropertyExtMenu.iMenuDescriptionBS = 0 
					iSelectedProperty = tempPropertyExtMenu.iSelectableProperties[tempPropertyExtMenu.iCurVertSel]
					IF IS_BIT_SET(tempPropertyExtMenu.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
						IF GET_OWNED_PROPERTY(0) > 0
						AND (iReplaceSpecificPropertySlot = -1 OR GET_OWNED_PROPERTY(iReplaceSpecificPropertySlot) > 0)

						ELSE
							SET_CURRENT_MENU_ITEM_DESCRIPTION("CUST_GAR_M_CON",100)
							ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING(mpProperties[iSelectedProperty].tl_PropertyName)
							ADD_CURRENT_MENU_ITEM_DESCRIPTION_INT(CEIL(GET_VALUE_OF_PROPERTY(iSelectedProperty)*g_sMPTunables.fPropertyMultiplier))
						ENDIF
					ELSE
						CLEAR_BIT(iLocalBS,LOCAL_BS_PURCHASE_MENU_SHOWING_WARN)
						SET_CURRENT_MENU_ITEM_DESCRIPTION(mpProperties[iSelectedProperty].tl_PropertyDetails,100)
					ENDIF	
					
					INT iRebateSlot
					IF IS_SALE_ACTIVE_FOR_PROPERTY(iSelectedProperty)
					AND NOT DOES_LOCAL_PLAYER_OWN_PROPERTY(iSelectedProperty)
						tlDisc = ""
						GET_CURRENT_MENU_ITEM_DISCOUNT(tlDisc)
						IF GET_HASH_KEY(tlDisc) = 0
							SET_CURRENT_MENU_ITEM_DISCOUNT("MP_PROP_SALE", 0)
							ADD_CURRENT_MENU_ITEM_DISCOUNT_INT(GET_MP_PROPERTY_ORIGINAL_VALUE(iSelectedProperty))
						ENDIF
					ELIF IS_REBATE_ACTIVE_FOR_PROPERTY(mpProperties[iSelectedProperty].tl_PropertyName, iRebateSlot)
					AND NOT DOES_LOCAL_PLAYER_OWN_PROPERTY(iSelectedProperty)
						tlDisc = ""
						GET_CURRENT_MENU_ITEM_DISCOUNT(tlDisc)
						IF GET_HASH_KEY(tlDisc) = 0
							SET_CURRENT_MENU_ITEM_DISCOUNT("MP_PROP_REBATE", 0)
						ENDIF
					ENDIF
					
				//Fail text
				ELSE
					IF IS_BIT_SET(tempPropertyExtMenu.iMenuDescriptionBS,MP_PROP_MENU_DES_NO_MONEY)
						SET_CURRENT_MENU_ITEM_DESCRIPTION("CUST_GAR_M_NM",100)
					ELIF IS_BIT_SET(tempPropertyExtMenu.iMenuDescriptionBS,MP_PROP_MENU_DES_ON_HEIST)
						SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_PROP_HEI_0",100)
					ELIF IS_BIT_SET(tempPropertyExtMenu.iMenuDescriptionBS,MP_PROP_MENU_DES_3RD_DISABLE)	
						SET_CURRENT_MENU_ITEM_DESCRIPTION("PROP3RDDIS",100)
					ELIF IS_BIT_SET(tempPropertyExtMenu.iMenuDescriptionBS,MP_PROP_MENU_DES_OWNS_PROPERTY)
						IF mpProperties[tempPropertyExtMenu.iSelectableProperties[tempPropertyExtMenu.iCurVertSel]].iType = PROPERTY_TYPE_GARAGE
							SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_PROP_FS_1",100)
						ELSE
							SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_PROP_FS_0",100)
						ENDIF
					ELIF IS_BIT_SET(tempPropertyExtMenu.iMenuDescriptionBS,	MP_PROP_MENU_DES_NOT_BOSS)
						SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_OFF_FS_1",100)
					ELIF IS_BIT_SET(tempPropertyExtMenu.iMenuDescriptionBS,	MP_PROP_MENU_DES_NO_YACHT)
						SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_OFF_FS_2",100)
					ENDIF
					
					IF HAS_NET_TIMER_EXPIRED(tempPropertyExtMenu.menuDescriptionTimer,MP_PROP_MENU_DES_TIME)
						RESET_NET_TIMER(tempPropertyExtMenu.menuDescriptionTimer)
						tempPropertyExtMenu.iMenuDescriptionBS = 0
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE MENU_TYPE_BUZZER
			IF tempPropertyExtMenu.iMenuDescriptionBS = 0 
				IF NOT IS_BIT_SET(iLocalBS,LOCAL_BS_AWAITING_BUZZER_RESPONSE)
					IF tempPropertyExtMenu.iCurVertSel = tempPropertyExtMenu.iAllNearby
						IF IS_PROPERTY_OFFICE(iCurrentPropertyID)
							IF tempPropertyExtMenu.iCurVertSel >= 0
								IF IS_PROPERTY_OFFICE(tempPropertyExtMenu.iSelectableProperties[tempPropertyExtMenu.iCurVertSel])
									SET_CURRENT_MENU_ITEM_DESCRIPTION("PROP_OFF_M_11",100)
								ELIF IS_PROPERTY_OFFICE_GARAGE(tempPropertyExtMenu.iSelectableProperties[tempPropertyExtMenu.iCurVertSel])
									IF tempPropertyExtMenu.iCurVertSel = tempPropertyExtMenu.iExitToOfficeFromGarageToMod
										SET_CURRENT_MENU_ITEM_DESCRIPTION("PROP_OFFGS_M_11",100)
									ELSE
										SET_CURRENT_MENU_ITEM_DESCRIPTION("PROP_OFFG_M_11",100)
									ENDIF
								ENDIF
							ELSE
								SET_CURRENT_MENU_ITEM_DESCRIPTION("PROP_OFF_M_11",100)
							ENDIF
						ELSE
							IF IS_PROPERTY_CLUBHOUSE(iCurrentPropertyID)
								SET_CURRENT_MENU_ITEM_DESCRIPTION("PROP_CLU_M_11",100)
							ELSE
								SET_CURRENT_MENU_ITEM_DESCRIPTION("PROP_HEI_E_0b",100)
							ENDIF
						ENDIF
					ELIF tempPropertyExtMenu.iCurVertSel = tempPropertyExtMenu.iAllFriendsCrew
						
						IF IS_PROPERTY_OFFICE(iCurrentPropertyID)
							IF tempPropertyExtMenu.iCurVertSel >= 0
								IF IS_PROPERTY_OFFICE(tempPropertyExtMenu.iSelectableProperties[tempPropertyExtMenu.iCurVertSel])
									SET_CURRENT_MENU_ITEM_DESCRIPTION("PROP_OFF_M_13",100)
								ELIF IS_PROPERTY_OFFICE_GARAGE(tempPropertyExtMenu.iSelectableProperties[tempPropertyExtMenu.iCurVertSel])
									IF tempPropertyExtMenu.iCurVertSel = tempPropertyExtMenu.iExitToOfficeFromGarageToMod
										SET_CURRENT_MENU_ITEM_DESCRIPTION("PROP_OFFGS_M_13",100)
									ELSE
										SET_CURRENT_MENU_ITEM_DESCRIPTION("PROP_OFFG_M_13",100)
									ENDIF
								ENDIF
							ELSE
								SET_CURRENT_MENU_ITEM_DESCRIPTION("PROP_OFF_M_13",100)
							ENDIF
						ELSE
							IF IS_PROPERTY_CLUBHOUSE(iCurrentPropertyID)
								SET_CURRENT_MENU_ITEM_DESCRIPTION("PROP_CLU_M_13",100)
							ELSE	
								SET_CURRENT_MENU_ITEM_DESCRIPTION("PROP_HEI_E_2b",100)
							ENDIF
						ENDIF
					ELIF tempPropertyExtMenu.iCurVertSel = tempPropertyExtMenu.iOrganisationNearby
						IF IS_PROPERTY_OFFICE(tempPropertyExtMenu.iSelectableProperties[tempPropertyExtMenu.iCurVertSel])
							SET_CURRENT_MENU_ITEM_DESCRIPTION("PROP_HEI_I_3b3",100)
						ELIF IS_PROPERTY_OFFICE_GARAGE(tempPropertyExtMenu.iSelectableProperties[tempPropertyExtMenu.iCurVertSel])
							IF tempPropertyExtMenu.iCurVertSel = tempPropertyExtMenu.iExitToOfficeFromGarageToMod
								SET_CURRENT_MENU_ITEM_DESCRIPTION("OFFGS_M_ORG",100)
							ELSE
								SET_CURRENT_MENU_ITEM_DESCRIPTION("OFFG_M_ORG",100)
							ENDIF
						ELIF IS_PROPERTY_CLUBHOUSE(iCurrentPropertyID)
							SET_CURRENT_MENU_ITEM_DESCRIPTION("PROP_HEI_I_CL2",100)
						ELSE
							SET_CURRENT_MENU_ITEM_DESCRIPTION("PROP_HEI_I_3b1",100)
						ENDIF
					ELSE
						IF tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel]= PLAYER_ID()
							IF mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iType = ENTRANCE_TYPE_GARAGE
							AND NOT (IS_PROPERTY_OFFICE(iCurrentPropertyID) OR IS_PROPERTY_OFFICE_GARAGE(iCurrentPropertyID))
								SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_PROP_MENUGb",100)	
							ELIF IS_PROPERTY_CLUBHOUSE(iCurrentPropertyID)
								SET_CURRENT_MENU_ITEM_DESCRIPTION("PROP_CLU_M_2",100)
							ELIF IS_PROPERTY_OFFICE(iCurrentPropertyID)
							OR IS_PROPERTY_OFFICE_GARAGE(iCurrentPropertyID)
								IF tempPropertyExtMenu.iCurVertSel >= 0
									IF IS_PROPERTY_OFFICE(tempPropertyExtMenu.iPropertyID[tempPropertyExtMenu.iCurVertSel])
										SET_CURRENT_MENU_ITEM_DESCRIPTION("PROP_OFF_M_2",100)
									ELIF IS_PROPERTY_OFFICE_GARAGE(tempPropertyExtMenu.iPropertyID[tempPropertyExtMenu.iCurVertSel])
										IF tempPropertyExtMenu.iCurVertSel = tempPropertyExtMenu.iExitToOfficeFromGarageToMod
											SET_CURRENT_MENU_ITEM_DESCRIPTION("PROP_OFFG_SHOPb",100)
										ELSE
											SET_CURRENT_MENU_ITEM_DESCRIPTION("PROP_OFFG_M_2",100)
										ENDIF
									ENDIF
								ELSE
									SET_CURRENT_MENU_ITEM_DESCRIPTION("PROP_OFF_M_2",100)
								ENDIF
							ELSE
								SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_PROP_MENU0b",100)
							ENDIF
						ELSE
							IF tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel] != INVALID_PLAYER_INDEX()
								IF mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iType = ENTRANCE_TYPE_GARAGE
									SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_PROP_MENU1bb",100)
								ELIF IS_PROPERTY_CLUBHOUSE(iCurrentPropertyID)
									SET_CURRENT_MENU_ITEM_DESCRIPTION("PROP_CLU_M_3",100)
								ELIF IS_PROPERTY_OFFICE(iCurrentPropertyID)
									IF tempPropertyExtMenu.iCurVertSel >= 0
										IF IS_PROPERTY_OFFICE(tempPropertyExtMenu.iSelectableProperties[tempPropertyExtMenu.iCurVertSel])
											SET_CURRENT_MENU_ITEM_DESCRIPTION("PROP_OFF_M_3",100)
										ELIF IS_PROPERTY_OFFICE_GARAGE(tempPropertyExtMenu.iSelectableProperties[tempPropertyExtMenu.iCurVertSel])
											SET_CURRENT_MENU_ITEM_DESCRIPTION("PROP_OFFG_M_3",100)
										ENDIF
									ELSE
										SET_CURRENT_MENU_ITEM_DESCRIPTION("PROP_OFF_M_3",100)
									ENDIF
								ELSE
									SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_PROP_MENU1b",100)
								ENDIF
								//ADD_CURRENT_MENU_ITEM_DESCRIPTION_PLAYER_NAME(GET_PLAYER_NAME(tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel]))
							ENDIF
						ENDIF
					ENDIF
				ELSE
//					IF mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iType = ENTRANCE_TYPE_GARAGE
//						SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_PROP_MENU1cb",100)
//					ELSE
						
					//ENDIF
					IF tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel] != INVALID_PLAYER_INDEX()
						SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_PROP_MENU1c",100)
						ADD_CURRENT_MENU_ITEM_DESCRIPTION_PLAYER_NAME(GET_PLAYER_NAME(tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel]))
					ENDIF
				ENDIF
			ELSE
				IF IS_BIT_SET(tempPropertyExtMenu.iMenuDescriptionBS,MP_PROP_MENU_DES_REJECTED_ENTRY)
					SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_PROP_DES0",100)
				ENDIF
				IF HAS_NET_TIMER_EXPIRED(tempPropertyExtMenu.menuDescriptionTimer,MP_PROP_MENU_DES_TIME)
					RESET_NET_TIMER(tempPropertyExtMenu.menuDescriptionTimer)
					tempPropertyExtMenu.iMenuDescriptionBS = 0
				ENDIF
			ENDIF
		BREAK
		CASE MENU_TYPE_REPLACE_PROPERTY
			IF IS_BIT_SET(iLocalBS,LOCAL_BS_SETUP_MENU)
				IF tempPropertyExtMenu.iMenuDescriptionBS = 0 
					iSelectedProperty = tempPropertyExtMenu.iSelectableProperties[tempPropertyExtMenu.iCurVertSel]
					IF IS_BIT_SET(tempPropertyExtMenu.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
						IF iSelectedProperty > 0
							//IF (IS_BIT_SET(iLocalBS,LOCAL_BS_PURCHASE_MENU_REQUEST_SF) AND HAS_SCALEFORM_MOVIE_LOADED(warningscreen))
								IF iCustomApartmentTooPurchase != -1
									iSelectedPropertyToBuy = iCustomApartmentTooPurchase
								ENDIF
								IF GET_PROPERTY_GARAGE_SIZE(iSelectedPropertyToBuy) < GET_PROPERTY_GARAGE_SIZE(iSelectedProperty)
									bSmallerGarage = TRUE
								ENDIF
								iTradeIn = GET_VALUE_OF_CURRENTLY_OWNED_PROPERTY(tempPropertyExtMenu.iSelectablePropertySlots[tempPropertyExtMenu.iCurVertSel])
									
									
			//						IF iTradeIn > 0
			//							IF GET_VALUE_OF_PROPERTY(iCurrentPropertyID)-iTradeIn < 0
			//								SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_PROP_REFUND",100)
			//								ADD_CURRENT_MENU_ITEM_DESCRIPTION_INT(iTradeIn)
			//								ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING(mpProperties[iSelectedProperty].tl_PropertyName)
			//								ADD_CURRENT_MENU_ITEM_DESCRIPTION_INT(ABSI(CEIL(GET_VALUE_OF_PROPERTY(iSelectedProperty)*g_sMPTunables.fPropertyMultiplier)-iTradeIn))
			//							ELSE
			//								SET_CURRENT_MENU_ITEM_DESCRIPTION("CUST_GAR_M_CONb",100)
			//								ADD_CURRENT_MENU_ITEM_DESCRIPTION_INT(iTradeIn)
			//								ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING(mpProperties[iSelectedProperty].tl_PropertyName)
			//								ADD_CURRENT_MENU_ITEM_DESCRIPTION_INT(CEIL(GET_VALUE_OF_PROPERTY(iSelectedProperty)*g_sMPTunables.fPropertyMultiplier)-iTradeIn)	
			//							ENDIF
			//						ELSE
			//							
			//							SET_CURRENT_MENU_ITEM_DESCRIPTION("CUST_GAR_M_CON",100)
			//							ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING(mpProperties[iSelectedProperty].tl_PropertyName)
			//							ADD_CURRENT_MENU_ITEM_DESCRIPTION_INT(CEIL(GET_VALUE_OF_PROPERTY(iSelectedProperty)*g_sMPTunables.fPropertyMultiplier))
			//						ENDIF
									
								DO_WARNING_MESSAGE(iTradeIn, bSmallerGarage)
							//ELSE
							//	CDEBUG1LN(DEBUG_SAFEHOUSE, "waiting for warning movie to load.")
							//ENDIF
						ELSE
							IF tempPropertyExtMenu.iSelectableProperties[tempPropertyExtMenu.iCurVertSel] >0
								SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_REP_PROP_2",100)
							ELSE
								SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_REP_PROP_5",100)
							ENDIF
						ENDIF
					ELSE
						CLEAR_BIT(iLocalBS,LOCAL_BS_PURCHASE_MENU_SHOWING_WARN)
						//IF tempPropertyExtMenu.iSelectableProperties[tempPropertyExtMenu.iCurVertSel] >0
							SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_REP_PROP_3",100)
						//ENDIF
					ENDIF	
				//Fail text
				ELSE
					IF IS_BIT_SET(tempPropertyExtMenu.iMenuDescriptionBS,MP_PROP_MENU_DES_NO_MONEY)
						SET_CURRENT_MENU_ITEM_DESCRIPTION("CUST_GAR_M_NM",100)
					ELIF IS_BIT_SET(tempPropertyExtMenu.iMenuDescriptionBS,MP_PROP_MENU_DES_ON_HEIST)
						SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_PROP_HEI_0",100)
					ELIF IS_BIT_SET(tempPropertyExtMenu.iMenuDescriptionBS,MP_PROP_MENU_DES_3RD_DISABLE)	
						SET_CURRENT_MENU_ITEM_DESCRIPTION("PROP3RDDIS",100)
					ELIF IS_BIT_SET(tempPropertyExtMenu.iMenuDescriptionBS,MP_PROP_MENU_DES_OWNS_PROPERTY)
						IF mpProperties[tempPropertyExtMenu.iSelectableProperties[tempPropertyExtMenu.iCurVertSel]].iType = PROPERTY_TYPE_GARAGE
							SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_PROP_FS_1",100)
						ELSE
							SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_PROP_FS_0",100)
						ENDIF
					ENDIF
					IF HAS_NET_TIMER_EXPIRED(tempPropertyExtMenu.menuDescriptionTimer,MP_PROP_MENU_DES_TIME)
						RESET_NET_TIMER(tempPropertyExtMenu.menuDescriptionTimer)
						tempPropertyExtMenu.iMenuDescriptionBS = 0
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE MENU_TYPE_PROPERTY_ENTRY_OPTIONS
			IF tempPropertyExtMenu.iMenuDescriptionBS = 0 
				IF tempPropertyExtMenu.iCurVertSel = tempPropertyExtMenu.iAllNearby
					IF IS_PROPERTY_OFFICE(tempPropertyExtMenu.iExitSelection)
						SET_CURRENT_MENU_ITEM_DESCRIPTION("PROP_OFF_M_11",100)
					ELIF IS_PROPERTY_OFFICE_GARAGE(tempPropertyExtMenu.iExitSelection)
						IF tempPropertyExtMenu.iExitToOfficeFromGarageToMod = 99
							SET_CURRENT_MENU_ITEM_DESCRIPTION("PROP_OFFGS_M_11",100)
						ELSE
							SET_CURRENT_MENU_ITEM_DESCRIPTION("PROP_OFFG_M_11",100)
						ENDIF
					ELSE
						IF IS_PROPERTY_CLUBHOUSE(iCurrentPropertyID)
							SET_CURRENT_MENU_ITEM_DESCRIPTION("PROP_CLU_M_11",100)
						ELSE
							SET_CURRENT_MENU_ITEM_DESCRIPTION("PROP_HEI_E_0b",100)
						ENDIF
					ENDIF
				ELIF tempPropertyExtMenu.iCurVertSel = tempPropertyExtMenu.iAllFriendsCrew
					IF IS_PROPERTY_OFFICE(tempPropertyExtMenu.iExitSelection)
						SET_CURRENT_MENU_ITEM_DESCRIPTION("PROP_OFF_M_13",100)
					ELIF IS_PROPERTY_OFFICE_GARAGE(tempPropertyExtMenu.iExitSelection)
						IF tempPropertyExtMenu.iExitToOfficeFromGarageToMod = 99
							SET_CURRENT_MENU_ITEM_DESCRIPTION("PROP_OFFGS_M_13",100)
						ELSE
							SET_CURRENT_MENU_ITEM_DESCRIPTION("PROP_OFFG_M_13",100)
						ENDIF
					ELSE
						IF IS_PROPERTY_CLUBHOUSE(iCurrentPropertyID)
							SET_CURRENT_MENU_ITEM_DESCRIPTION("PROP_CLU_M_13",100)
						ELSE
							SET_CURRENT_MENU_ITEM_DESCRIPTION("PROP_HEI_E_2b",100)
						ENDIF
					ENDIF
				ELIF tempPropertyExtMenu.iCurVertSel = tempPropertyExtMenu.iOrganisationNearby
					IF IS_PROPERTY_OFFICE(tempPropertyExtMenu.iExitSelection)
						IF tempPropertyExtMenu.iExitToOfficeFromGarageToMod = 99
							SET_CURRENT_MENU_ITEM_DESCRIPTION("OFFGS_M_ORG",100)
						ELSE
							SET_CURRENT_MENU_ITEM_DESCRIPTION("PROP_HEI_I_3b3",100)
						ENDIF
					ELIF IS_PROPERTY_CLUBHOUSE(iCurrentPropertyID)
						SET_CURRENT_MENU_ITEM_DESCRIPTION("PROP_HEI_I_CL2",100)
					ELIF IS_PROPERTY_OFFICE_GARAGE(tempPropertyExtMenu.iExitSelection)
						IF tempPropertyExtMenu.iExitToOfficeFromGarageToMod = 99
							SET_CURRENT_MENU_ITEM_DESCRIPTION("OFFGS_M_ORG",100)
						ELSE
							SET_CURRENT_MENU_ITEM_DESCRIPTION("OFFG_M_ORG",100)
						ENDIF
					ELSE
						SET_CURRENT_MENU_ITEM_DESCRIPTION("PROP_HEI_I_3b1",100)
					ENDIF
				ELSE
					IF IS_PROPERTY_OFFICE(tempPropertyExtMenu.iExitSelection)
						SET_CURRENT_MENU_ITEM_DESCRIPTION("PROP_OFF_ENTb",100)
					ELIF IS_PROPERTY_OFFICE_GARAGE(tempPropertyExtMenu.iExitSelection)
						IF tempPropertyExtMenu.iExitToOfficeFromGarageToMod = 99
							SET_CURRENT_MENU_ITEM_DESCRIPTION("PROP_OFFG_SHOPb",100)
						ELSE
							SET_CURRENT_MENU_ITEM_DESCRIPTION("PROP_OFFG_ENTb",100)
						ENDIF
					ELSE
					SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_PROP_MENU0b",100)
					ENDIF
				ENDIF
			ENDIF
		BREAK
//		CASE MENU_TYPE_AUTH_ENTRY
//			IF tempPropertyExtMenu.iMaxVertSel > 0
//				IF tempPropertyExtMenu.iCurVertSel = 0
//					SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_PROP_MENU2e",100)
//				ELSE
//					SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_PROP_MENU2b",100)
//					ADD_CURRENT_MENU_ITEM_DESCRIPTION_PLAYER_NAME(GET_PLAYER_NAME(tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel]))	
//				ENDIF
//			ELSE
//				SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_PROP_MENU2c",100)
//			ENDIF
//		BREAK
	ENDSWITCH
ENDPROC

PROC HANDLE_RESPONSE_TO_ENTRY_REQUEST()
	INT i
	BOOL bResetTimer = TRUE
	IF IS_BIT_SET(iLocalBS,LOCAL_BS_AWAITING_BUZZER_RESPONSE)
		IF IS_NET_PLAYER_OK(tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel],FALSE)
			IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iRequestEntryBS,NATIVE_TO_INT(tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel]))
				IF NOT IS_PLAYER_IN_PROPERTY((tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel]),FALSE)
				OR (GlobalplayerBD_FM[NATIVE_TO_INT(tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel])].propertyDetails.iCurrentlyInsideProperty <= 0) // added by Neil F. for 1822930
				OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel])].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_EXITING_PROPERTY)

					CLEAR_BIT(iLocalBS,LOCAL_BS_AWAITING_BUZZER_RESPONSE)
					CLEAR_BIT(iLocalBS,LOCAL_BS_SETUP_MENU)
					//NETWORK_SET_TALKER_PROXIMITY(CHAT_PROXIMITY_FM)
					NETWORK_CLEAR_VOICE_CHANNEL()
					RESET_NET_TIMER(buzzerTimer)
					//SETUP_SPECIFIC_SPAWN_LOCATION(mpProperties[iCurrentPropertyID].house.exits[0].vOutPlayerLoc,mpProperties[iCurrentPropertyID].house.exits[0].fOutPlayerHeading, 10, FALSE)
					//SET_LOCAL_STAGE(PROP_STAGE_LEAVE_ENTRANCE_AREA)
					GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iRequestEntryBS = 0
					globalPropertyEntryData.bLeftOrRejectedBuzzer = TRUE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "globalPropertyEntryData.bLeftOrRejectedBuzzer = TRUE")
					CDEBUG1LN(DEBUG_SAFEHOUSE, "GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iRequestEntryBS = 0")
					CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_RESPONSE_TO_ENTRY_REQUEST: property owner is no longer in propety cleaning up")
				ELSE
					IF GlobalplayerBD_FM[NATIVE_TO_INT(tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel])].propertyDetails.entryResponse[NATIVE_TO_INT(PLAYER_ID())].iResponse = PROPERTY_BROADCAST_RESPONSE_YES	
						globalPropertyEntryData.ownerID = tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel]
						globalPropertyEntryData.ownerHandle = GET_GAMER_HANDLE_PLAYER(tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel])
						iCurrentPropertyID = GlobalplayerBD_FM[NATIVE_TO_INT(tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel])].propertyDetails.iCurrentlyInsideProperty
						CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_RESPONSE_TO_ENTRY_REQUEST:setting owner as ",NATIVE_TO_INT(globalPropertyEntryData.ownerID))
						SET_BIT(iLocalBS2,LOCAL_BS2_bEnteringAnotherPlayerProperty)
						//GET_MP_PROPERTY_DETAILS(mpProperties[iCurrentPropertyID],mpProperties[iCurrentPropertyID].iIndex)
						IF IS_PLAYER_IN_MP_GARAGE(tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel],TRUE)
							REPEAT mpProperties[iCurrentPropertyID].iNumEntrances i
								IF mpProperties[iCurrentPropertyID].entrance[i].iType = ENTRANCE_TYPE_GARAGE
									globalPropertyEntryData.iEntrance = i
									i = mpProperties[iCurrentPropertyID].iNumEntrances
								ENDIF
							ENDREPEAT
						ELSE
							globalPropertyEntryData.iEntrance = 0
						ENDIF
						globalPropertyEntryData.iVariation = -1
						PRINTLN("AMP_MP_PROPERTY_EXT: reset variation on entry to property - 6")
						CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_RESPONSE_TO_ENTRY_REQUEST:player recieved permission to enter property #",mpProperties[iCurrentPropertyID].iIndex)
						globalPropertyEntryData.iPropertyEntered =  GlobalplayerBD_FM[NATIVE_TO_INT(tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel])].propertyDetails.iCurrentlyInsideProperty
						PRINTLN("1 globalPropertyEntryData.iPropertyEntered = ",globalPropertyEntryData.iPropertyEntered)
						//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iRequestEntryBS = 0
						CLEAR_TRANSITION_SESSION_DETAILS_ON_ENTRY_IF_SUITABLE(7)
						globalPropertyEntryData.bBuzzAccepted = TRUE
						DISPLAY_RADAR(FALSE)
						SET_BIT(iLocalBS2,LOCAL_BS2_DisabledRadar)
						SET_LOCAL_STAGE(STAGE_BUZZED_INTO_PROPERTY)
						CLEAR_BIT(iLocalBS,LOCAL_BS_AWAITING_BUZZER_RESPONSE)
						
//						
						EXIT
					ELIF GlobalplayerBD_FM[NATIVE_TO_INT(tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel])].propertyDetails.entryResponse[NATIVE_TO_INT(PLAYER_ID())].iResponse = PROPERTY_BROADCAST_RESPONSE_NO
					//OR HAS_NET_TIMER_EXPIRED(buzzerTimer,60000) 
						SET_BIT(iRejectedEntryBS,NATIVE_TO_INT(tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel]) )
						CLEAR_BIT(iLocalBS,LOCAL_BS_AWAITING_BUZZER_RESPONSE)
						CLEAR_BIT(iLocalBS,LOCAL_BS_SETUP_MENU)
						//NETWORK_SET_TALKER_PROXIMITY(CHAT_PROXIMITY_FM)
						NETWORK_CLEAR_VOICE_CHANNEL()
						RESET_NET_TIMER(buzzerTimer)
						//SETUP_SPECIFIC_SPAWN_LOCATION(mpProperties[iCurrentPropertyID].house.exits[0].vOutPlayerLoc,mpProperties[iCurrentPropertyID].house.exits[0].fOutPlayerHeading, 10, FALSE)
						//SET_LOCAL_STAGE(PROP_STAGE_LEAVE_ENTRANCE_AREA)
						globalPropertyEntryData.bLeftOrRejectedBuzzer = TRUE
						CDEBUG1LN(DEBUG_SAFEHOUSE, "globalPropertyEntryData.bLeftOrRejectedBuzzer 2 = TRUE")
						GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iRequestEntryBS = 0
						CDEBUG1LN(DEBUG_SAFEHOUSE, "No Response: GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iRequestEntryBS = 0")
						EXIT
					ENDIF
				ENDIF
				//CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Waiting for response from property owner")
			ELSE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Awaitng response but bit is not set to current selection??")
			ENDIF
		ELSE
			CLEAR_BIT(iLocalBS,LOCAL_BS_AWAITING_BUZZER_RESPONSE)
			CLEAR_BIT(iLocalBS,LOCAL_BS_SETUP_MENU)
			globalPropertyEntryData.bLeftOrRejectedBuzzer = TRUE
			CDEBUG1LN(DEBUG_SAFEHOUSE, "globalPropertyEntryData.bLeftOrRejectedBuzzer 3 = TRUE")
			NETWORK_CLEAR_VOICE_CHANNEL()
			CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_RESPONSE_TO_ENTRY_REQUEST: removing player from voice channel property owner is no longer OK")
			//NETWORK_SET_TALKER_PROXIMITY(CHAT_PROXIMITY_FM)
			RESET_NET_TIMER(buzzerTimer)
			//SETUP_SPECIFIC_SPAWN_LOCATION(mpProperties[iCurrentPropertyID].house.exits[0].vOutPlayerLoc,mpProperties[iCurrentPropertyID].house.exits[0].fOutPlayerHeading, 10, FALSE)
			//SET_LOCAL_STAGE(PROP_STAGE_LEAVE_ENTRANCE_AREA)
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iRequestEntryBS = 0
			CDEBUG1LN(DEBUG_SAFEHOUSE, "Property owner no longer ok: GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iRequestEntryBS = 0")
			CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_RESPONSE_TO_ENTRY_REQUEST: property owner is no longer OK cleaning up")
		ENDIF
	ELSE
		IF globalPropertyEntryData.bLeftOrRejectedBuzzer
			IF NOT g_bPropInteriorScriptRunning
			AND NOT IS_BIT_SET(mpPropMaintain.iBS, MP_PROP_MAINTAIN_BS_LAUNCHED_INT_SCRIPT)
				bResetTimer = FALSE
				IF HAS_NET_TIMER_EXPIRED(leftBuzzerFailSafe,2000,TRUE)
					PRINTLN("HANDLE_RESPONSE_TO_ENTRY_REQUEST: globalPropertyEntryData.bLeftOrRejectedBuzzer = FALSE failsafe")
				ENDIF
				PRINTLN("HANDLE_RESPONSE_TO_ENTRY_REQUEST: player still in menu after bLeftOrRejectedBuzzer")
			ENDIF
		ENDIF
	ENDIF
	IF bResetTimer
		RESET_NET_TIMER(leftBuzzerFailSafe)
	ENDIF
ENDPROC

PROC CLEANUP_ENTER_STRUCT()
	warpInControl.iStage = 0
	warpInControl.iWarningScreenButtonBS = FE_WARNING_NONE
	warpInControl.ibuttonBS = 0
	warpInControl.iVehicleSlot = -1
	warpInControl.iBS = 0
ENDPROC

PROC CLEANUP_THIS_PROPERTY_CAM(CAMERA_INDEX &propertyCam)
	IF DOES_CAM_EXIST(propertyCam)
		IF NOT IS_BIT_SET(iLocalBS2,LOCAL_BS2_bKeepScriptCameras)
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
		ENDIF
		
		IF IS_CAM_ACTIVE(propertyCam)
			SET_CAM_ACTIVE(propertyCam,FALSE)
		ENDIF
		DESTROY_CAM(propertyCam)
	ENDIF	
ENDPROC
PROC CLEANUP_PROPERTY_CAMERAS()
	CLEANUP_THIS_PROPERTY_CAM(propertyCam0)
	CLEANUP_THIS_PROPERTY_CAM(propertyCam1)
ENDPROC

PROC RESET_PROPERTY_MENU()
	CLEANUP_MENU_ASSETS()
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		tempPropertyExtMenu.menuPlayerArray[i] = INVALID_PLAYER_INDEX()
	ENDREPEAT
	RESET_NET_TIMER(tempPropertyExtMenu.menuDescriptionTimer)
	RESET_NET_TIMER(tempPropertyExtMenu.menuNavigationDelay)
	tempPropertyExtMenu.iMenuDescriptionBS = 0
	tempPropertyExtMenu.iMaxVertSel = 0
	tempPropertyExtMenu.iCurVertSel = 0
	tempPropertyExtMenu.iButtonBS = 0
	
	tempPropertyExtMenu.iExitSelection = -1
	tempPropertyExtMenu.iMoveBetweenSelection = -1
	tempPropertyExtMenu.iAllExitOption = -1
	tempPropertyExtMenu.iAllGarageOption = -1
	
	IF IS_BIT_SET(iLocalBS,LOCAL_BS_AWAITING_BUZZER_RESPONSE)
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iRequestEntryBS = 0
		CDEBUG1LN(DEBUG_SAFEHOUSE, "RESET_PROPERTY_MENU: GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iRequestEntryBS = 0")
	ENDIF
	CLEAR_BIT(iLocalBS,LOCAL_BS_SETUP_MENU)
	CLEAR_BIT(iLocalBS,LOCAL_BS_AWAITING_BUZZER_RESPONSE)
	CLEAR_BIT(iLocalBS,LOCAL_BS_PURCHASE_MENU_REQUEST_SF)
	CLEAR_BIT(iLocalBS,LOCAL_BS_PURCHASE_MENU_SHOWING_WARN)
	CLEAR_BIT(iLocalBS,LOCAL_BS_STOP_CUSTOM_MENU_CHECK)
	//IF warningscreen != NULL
	//	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(warningscreen)
	//ENDIF
	
	//Reset the variable used to control the interior variation submenu
	iCustomApartmentTooPurchase = -1
	bInteriorToPurchaseSelected = FALSE
	tempPropertyExtMenu.iExitToOfficeFromGarageToMod = -1
	CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: RESET_PROPERTY_MENU called")
ENDPROC

PROC CLEANUP_ENTRY_CS()
	IF entryCutData.iSyncedSceneID >= 0
		IF NOT IS_PED_INJURED(entryCutData.playerClone)
			CLEAR_PED_TASKS(entryCutData.playerClone)
		ENDIF
		//IF IS_SYNCHRONIZED_SCENE_RUNNING(entryCutData.iSyncedSceneID)
			
		entryCutData.iSyncedSceneID = -1 
		CDEBUG1LN(DEBUG_SAFEHOUSE, "CLEANUP_ENTRY_CS- killing sync scene")
		//ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(entryCutData.playerClone)
		DELETE_PED(entryCutData.playerClone)
	ENDIF
	RESET_NET_TIMER(entryCutData.timer)
	IF DOES_CAM_EXIST(entryCutData.cameras[0])
		DESTROY_CAM(entryCutData.cameras[0])
	ENDIF
	
	entryCutData.iStage = ENTRY_CS_STAGE_INIT_DATA
	IF IS_PROPERTY_OFFICE(iCurrentPropertyID) AND IS_PLAYER_ENTERING_PROPERTY(PLAYER_ID())
		CLEAR_BIT(entryCutData.iBS,ENTRY_CS_BS_STARTED_MP_CUTSCENE)
		PRINTLN("AM_MP_PROPERTY_EXT: Skipping cutscene cleanup for office property")
	ENDIF
	IF IS_BIT_SET(entryCutData.iBS,ENTRY_CS_BS_STARTED_MP_CUTSCENE)
		IF NOT g_sTransitionSessionData.bMissionCutsceneInProgress 
			CDEBUG1LN(DEBUG_SAFEHOUSE, "CLEANUP_ENTRY_CS- bMissionCutsceneInProgress = FALSE, do CLEANUP_MP_CUTSCENE.")
			CLEANUP_MP_CUTSCENE(FALSE,FALSE)
		ELSE
			CDEBUG1LN(DEBUG_SAFEHOUSE, "CLEANUP_ENTRY_CS- bMissionCutsceneInProgress = TRUE, SKIP CLEANUP_MP_CUTSCENE.")
		ENDIF
	ENDIF
	
//	IF IS_BIT_SET(entryCutData.iBS,ENTRY_CS_BS_TOOK_AWAY_CONTROL)
//		NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//		CLEAR_BIT(entryCutData.iBS,ENTRY_CS_BS_TOOK_AWAY_CONTROL)	
//		CLEAR_BIT(iLocalBS, LOCAL_BS_TAKEN_AWAY_CONTROL)
//	ENDIF
	
	INT i
	REPEAT SYNC_SCENE_MAX_ELEMENTS i
		entryCutData.animName[i] = ""
	ENDREPEAT
	IF IS_BIT_SET(iLocalBS, LOCAL_BS_BUZZER_REQUESTED_ANIM)
//		#IF NOT FEATURE_HEIST_PLANNING
//			REMOVE_ANIM_DICT("mp_doorbell")
//		#ENDIF
			REMOVE_ANIM_DICT(entryCutData.animDictionary)
		CLEAR_BIT(iLocalBS, LOCAL_BS_BUZZER_REQUESTED_ANIM)
	ENDIF
	entryCutData.animDictionary = ""
	i = 0
	REPEAT 2 i
		entryCutData.vCamLoc[i] = <<0,0,0>>
		entryCutData.vCamRot[i] = <<0,0,0>>
		entryCutData.fCamFOV[i] = 0
	ENDREPEAT

	entryCutData.vSyncSceneLoc = <<0,0,0>>
	entryCutData.vSyncSceneRot = <<0,0,0>>
	
//	IF IS_BIT_SET(entryCutData.iBS,ENTRY_CS_BS_APT_DOOR_MODEL_HIDE)
//		REMOVE_MODEL_HIDE(propertyDoors[1].vCoords,1,propertyDoors[1].doorModel)
//	ENDIF
	
	IF IS_BIT_SET(entryCutData.iBS,ENTRY_CS_BS_APT_DOOR_MODEL_HIDE)
		IF GET_PROPERTY_BUILDING(iCurrentPropertyID) <> MP_PROPERTY_BUILDING_5
			//IF GET_PROPERTY_SIZE_TYPE(iCurrentPropertyID) = PROP_SIZE_TYPE_LARGE_APT
			IF NOT ARE_VECTORS_EQUAL(propertyDoors[1].vCoords,<<0,0,0>>)
				REMOVE_MODEL_HIDE(propertyDoors[1].vCoords,1,propertyDoors[1].doorModel)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "REMOVING model hide 6  at ",propertyDoors[1].vCoords, " model: ",propertyDoors[1].doorModel)
			ELSE
				REMOVE_MODEL_HIDE(propertyDoors[0].vCoords,1,propertyDoors[0].doorModel)	
				CDEBUG1LN(DEBUG_SAFEHOUSE, "REMOVING  model hide 7  at ",propertyDoors[0].vCoords, " model: ",propertyDoors[0].doorModel)
			ENDIF
		ELSE
			REMOVE_MODEL_HIDE(propertyDoors[0].vCoords,1,propertyDoors[0].doorModel)	
			CDEBUG1LN(DEBUG_SAFEHOUSE, "REMOVING  model hide 8  at ",propertyDoors[0].vCoords, " model: ",propertyDoors[0].doorModel)
		ENDIF
		CLEAR_BIT(entryCutData.iBS,ENTRY_CS_BS_APT_DOOR_MODEL_HIDE)
	ENDIF
	
	IF DOES_ENTITY_EXIST(entryCutData.objIDs[0])
		DELETE_OBJECT(entryCutData.objIDs[0])
	ENDIF
	IF DOES_ENTITY_EXIST(entryCutData.playerClone)
		DELETE_PED(entryCutData.playerClone)
	ENDIF
	entryCutData.iBS = 0
	
	IF entryCutData.iSoundID >= 0
		STOP_SOUND(entryCutData.iSoundID)
	ENDIF
	
	//DELETE_CLONED_ENTITIES_AND_CLEAR_REQUESTS(4)
	CDEBUG1LN(DEBUG_SAFEHOUSE, "CLEANUP_ENTRY_CS- called")
ENDPROC

PROC RESET_HAD_HEIST_DROPOFF_ITEM(INT iCall)
	IF iCall != 0
	
	ENDIF
	
	IF bHadMissionCriticalEntityOnDropoff
		bHadMissionCriticalEntityOnDropoff = FALSE
		PRINTLN("bHadMissionCriticalEntityOnDropoff Set False #",icall)
	ENDIF
ENDPROC

PROC FORCE_UNDERMAP_FOR_ENTRY(INT iCall)
	VEHICLE_INDEX thisVehicle
	PED_INDEX pedTemp
	INT i,j
	FLOAT fDistance = 200
	IF iCall != 0
	ENDIF
	IF IS_BIT_SET(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_CAR_MADE_INVISIBLE_ON_EXTERIOR)
		IF NOT IS_BIT_SET(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_FORCED_BENEATH_MAP_ON_ENTRY)
			IF NOT globalPropertyEntryData.bInteriorWarpDone
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(),TRUE)
					thisVehicle= GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(),TRUE)
					IF IS_VEHICLE_DRIVEABLE(thisVehicle)
						IF NETWORK_HAS_CONTROL_OF_ENTITY(thisVehicle)
							IF NOT NETWORK_IS_ENTITY_FADING(thisVehicle)
								globalPropertyEntryData.vBeforeUnderMapLocation = GET_ENTITY_COORDS(thisVehicle)
								IF globalPropertyEntryData.vBeforeUnderMapLocation.z > 0
									REPEAT mpProperties[iCurrentPropertyID].iNumEntrances j
										IF fDistance > GET_DISTANCE_BETWEEN_COORDS(globalPropertyEntryData.vBeforeUnderMapLocation,mpProperties[iCurrentPropertyID].vBlipLocation[j])
											fDistance = GET_DISTANCE_BETWEEN_COORDS(globalPropertyEntryData.vBeforeUnderMapLocation,mpProperties[iCurrentPropertyID].vBlipLocation[j])
										ENDIF
									ENDREPEAT
									IF fDistance <= 50
										FOR i = ENUM_TO_INT(VS_DRIVER) TO ENUM_TO_INT(VS_EXTRA_RIGHT_3)
											IF i < GET_VEHICLE_MODEL_NUMBER_OF_SEATS(GET_ENTITY_MODEL(thisVehicle))
												IF NOT IS_VEHICLE_SEAT_FREE(thisVehicle, INT_TO_ENUM(VEHICLE_SEAT, i),TRUE)
													pedTemp = GET_PED_IN_VEHICLE_SEAT(thisVehicle, INT_TO_ENUM(VEHICLE_SEAT, i),TRUE)
													IF DOES_ENTITY_EXIST(pedTemp)
													AND IS_PED_A_PLAYER(pedTemp)
														IF NOT NETWORK_IS_PLAYER_IN_MP_CUTSCENE(NETWORK_GET_PLAYER_INDEX_FROM_PED(pedTemp))
															PRINTLN("FORCE_UNDERMAP_FOR_ENTRY: waiting for passengers to be in CS")
															EXIT
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDFOR
										
										globalPropertyEntryData.vBeforeUnderMapLocation = GET_ENTITY_COORDS(thisVehicle)
										SET_ENTITY_COORDS(thisVehicle,globalPropertyEntryData.vBeforeUnderMapLocation -<<0,0,10>>)
										PRINTLN("FORCE_UNDERMAP_FOR_ENTRY: Call# ",iCall," Forcing vehicle beneath map")
										FREEZE_ENTITY_POSITION(thisVehicle,TRUE)
										SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_FORCED_BENEATH_MAP_ON_ENTRY)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF NOT NETWORK_IS_PLAYER_FADING(PLAYER_ID())
						globalPropertyEntryData.vBeforeUnderMapLocation = GET_ENTITY_COORDS(PLAYER_PED_ID())
						IF globalPropertyEntryData.vBeforeUnderMapLocation.z > 0
							REPEAT mpProperties[iCurrentPropertyID].iNumEntrances j
								IF fDistance > GET_DISTANCE_BETWEEN_COORDS(globalPropertyEntryData.vBeforeUnderMapLocation,mpProperties[iCurrentPropertyID].vBlipLocation[j])
									fDistance = GET_DISTANCE_BETWEEN_COORDS(globalPropertyEntryData.vBeforeUnderMapLocation,mpProperties[iCurrentPropertyID].vBlipLocation[j])
								ENDIF
							ENDREPEAT
							IF fDistance <= 50
								SET_ENTITY_COORDS(PLAYER_PED_ID(),globalPropertyEntryData.vBeforeUnderMapLocation -<<0,0,10>>)
								NET_SET_PLAYER_CONTROL(PLAYER_ID(),FALSE,NSPC_FREEZE_POSITION)
								PRINTLN("FORCE_UNDERMAP_FOR_ENTRY: Call# ",iCall," Forcing player beneath map")
								SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_FORCED_BENEATH_MAP_ON_ENTRY)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF NOT globalPropertyEntryData.bInteriorWarpDone
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(),TRUE)
					thisVehicle= GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(),TRUE)
					IF IS_VEHICLE_DRIVEABLE(thisVehicle)
						IF NETWORK_HAS_CONTROL_OF_ENTITY(thisVehicle)
							FREEZE_ENTITY_POSITION(thisVehicle,TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC FORCE_BACK_INTO_POSITION_ABORTED_ENTRY(INT iCall)
	VEHICLE_INDEX thisVehicle
	IF iCall != 0
	ENDIF
	IF IS_BIT_SET(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_FORCED_BENEATH_MAP_ON_ENTRY)
	AND NOT ARE_VECTORS_EQUAL(globalPropertyEntryData.vBeforeUnderMapLocation,<<0,0,0>>)
		IF NOT globalPropertyEntryData.bInteriorWarpDone
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(),TRUE)
				thisVehicle= GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(),TRUE)
				IF IS_VEHICLE_DRIVEABLE(thisVehicle)
					IF NETWORK_HAS_CONTROL_OF_ENTITY(thisVehicle)
						SET_ENTITY_COORDS(thisVehicle,globalPropertyEntryData.vBeforeUnderMapLocation)
						PRINTLN("FORCE_BACK_INTO_POSITION_ABORTED_ENTRY: Call# ",iCall," Forcing vehicle back to original location")
						CLEAR_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_FORCED_BENEATH_MAP_ON_ENTRY)
					ENDIF
				ENDIF
			ELSE
				SET_ENTITY_COORDS(PLAYER_PED_ID(),globalPropertyEntryData.vBeforeUnderMapLocation)
				PRINTLN("FORCE_BACK_INTO_POSITION_ABORTED_ENTRY: Call# ",iCall," Forcing player back to original location")
				CLEAR_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_FORCED_BENEATH_MAP_ON_ENTRY)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC RESET_TO_ENTRANCE_TO_PROPERTY_STAGE()
	//IF bReturnControl
	IF IS_BIT_SET(iLocalBS, LOCAL_BS_FORCE_TERMINATED_INTERNET)
		MP_FORCE_TERMINATE_INTERNET_CLEAR()
		CLEAR_BIT(iLocalBS, LOCAL_BS_FORCE_TERMINATED_INTERNET)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Force terminate internet cleared - 1")
	ENDIF
	IF globalPropertyEntryData.iGarageIntroProperty = iCurrentPropertyID
		globalPropertyEntryData.iGarageIntroProperty = -1
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: - CLEARING globalPropertyEntryData.iGarageIntroProperty = ", iCurrentPropertyID, " ... = -1")
	ENDIF
	
	
	iCurrentPropertyID = mpProperties[iCurrentPropertyID].building.iPropertiesInBuilding[0]
	
		CLEANUP_ENTRY_CS()
	RESET_HAD_HEIST_DROPOFF_ITEM(2)
	IF IS_PROPERTY_OFFICE(iCurrentPropertyID)
		IF IS_AUDIO_SCENE_ACTIVE("EXEC1_Enter_Office_From_Ground_Scene")
			STOP_AUDIO_SCENE("EXEC1_Enter_Office_From_Ground_Scene")
			PRINTLN("STOP_AUDIO_SCENE - EXEC1_Enter_Office_From_Ground_Scene -1 ")
		ENDIF
		IF IS_AUDIO_SCENE_ACTIVE("EXEC1_Enter_Office_From_Roof_Scene")
			STOP_AUDIO_SCENE("EXEC1_Enter_Office_From_Roof_Scene")
			PRINTLN("STOP_AUDIO_SCENE - EXEC1_Enter_Office_From_Roof_Scene -1 ")
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iLocalBS, LOCAL_BS_TAKEN_AWAY_CONTROL)
	OR IS_BIT_SET(entryCutData.iBS,ENTRY_CS_BS_TOOK_AWAY_CONTROL)
		IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_MP_PROPERTY_INT")) <= 0
		AND NOT g_bPropInteriorScriptRunning)
		OR globalPropertyEntryData.bLeftOrRejectedBuzzer
		OR bLeavingEntranceArea
			IF NOT IS_PLAYER_TELEPORT_ACTIVE()
			AND NOT IS_PLAYER_IN_CORONA()
			AND NOT IS_POST_MISSION_CLEANUP_REQUIRED_OR_ACTIVE(PLAYER_ID())
			AND IS_SKYSWOOP_AT_GROUND()
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				CLEAR_BIT(iLocalBS, LOCAL_BS_TAKEN_AWAY_CONTROL)
				CLEAR_BIT(entryCutData.iBS,ENTRY_CS_BS_TOOK_AWAY_CONTROL)
			ELSE
				PRINTLN("AM_MP_PROPERTY_EXT: RESET_TO_ENTRANCE_TO_PROPERTY_STAGE not turning on player control as IS_PLAYER_TELEPORT_ACTIVE")
			ENDIF
		ELSE
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: RESET_TO_ENTRANCE_TO_PROPERTY_STAGE not turning on player control as interior is running")
			CLEAR_BIT(iLocalBS, LOCAL_BS_TAKEN_AWAY_CONTROL)
			CLEAR_BIT(entryCutData.iBS,ENTRY_CS_BS_TOOK_AWAY_CONTROL)
		ENDIF
	ENDIF
	bLeavingEntranceArea = FALSE
	
	CLEAR_BIT(ilocalBS,LOCAL_BS2_TriggeredAcceptWarning)
	CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_USING_EXT)
	CDEBUG1LN(DEBUG_SAFEHOUSE, "PROPERTY_BROADCAST_BS_PLAYER_USING_EXT- CLEARED")
	CLEAR_BIT(iLocalBS2,LOCAL_BS2_bWalkInTriggered)
	CLEAR_BIT(iLocalBS2,LOCAL_BS2_bClearedHeistPanCutscene)
	
	IF IS_BIT_SET(ilocalBS,LOCAL_BS_STARTED_FADE)
		IF IS_SCREEN_FADING_OUT()
		OR IS_SCREEN_FADED_OUT()
			DO_SCREEN_FADE_IN(500)		
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: RESET_TO_ENTRANCE_TO_PROPERTY_STAGE force fading screen back in")
		ENDIF
		CLEAR_BIT(ilocalBS,LOCAL_BS_STARTED_FADE)
	ENDIF
	//ENDIF
	CLEAR_BIT(iLocalBS, LOCAL_BS_DELETED_ENTERING_VEHICLE)
	CLEAR_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_ENTER_GARAGE)
	IF globalPropertyEntryData.iGarageIntroProperty = iCurrentPropertyID
		globalPropertyEntryData.iGarageIntroProperty = -1
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: - CLEARING globalPropertyEntryData.iGarageIntroProperty 451 = ", iCurrentPropertyID, " ... = -1")
	ENDIF
	IF IS_BIT_SET(iLocalBS, LOCAL_BS_BUZZER_REQUESTED_ANIM)
//		#IF NOT FEATURE_HEIST_PLANNING
//			REMOVE_ANIM_DICT("mp_doorbell")
//		#ENDIF
			REMOVE_ANIM_DICT(entryCutData.animDictionary)
		CLEAR_BIT(iLocalBS, LOCAL_BS_BUZZER_REQUESTED_ANIM)
	ENDIF
//	IF IS_BIT_SET(iLocalBS,LOCAL_BS_SETUP_SPECIFIC_SPAWN_POS)
//		CLEAR_SPECIFIC_SPAWN_LOCATION()
//		CLEAR_BIT(iLocalBS,LOCAL_BS_SETUP_SPECIFIC_SPAWN_POS)
//	ENDIF
	warpInWithDriverID= INVALID_PLAYER_INDEX()
	SET_LOCAL_STAGE(STAGE_CHECK_FOR_IN_LOCATE)

	//RELEASE_CONTEXT_INTENTION(iBuzzerContextIntention)
	CLEAR_INVALID_VEHICLE_HELP()
	CLEANUP_ENTER_STRUCT()
	CLEANUP_PROPERTY_CAMERAS()
	RESET_PROPERTY_MENU()
	RESET_NET_TIMER(failSafeEntryTimer)
	CLEAR_BIT(iLocalBS2,LOCAL_BS2_VehBelongsToOtherGarage)
	CDEBUG1LN(DEBUG_SAFEHOUSE, "LOCAL_BS2_VehBelongsToOtherGarage = FALSE - 2")
	IF iPurchaseContextIntention != NEW_CONTEXT_INTENTION
		RELEASE_CONTEXT_INTENTION(iPurchaseContextIntention)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Releasing the context intention for the purchase menu. (RESET)")
	ENDIF
	IF IS_BIT_SET(iLocalBS2,LOCAL_BS2_DisabledRadar)
		DISPLAY_RADAR(TRUE)
	ENDIF
	
	IF IS_BIT_SET(iLocalBS2,LOCAL_BS2_SetEntryData)
		//CLEAR_GLOBAL_ENTRY_DATA()	//Some of this stuff might need to be cleared, but some might still be needed. SO don#t clear just now.
		CLEAR_FOCUS()
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: RESET_TO_ENTRANCE_TO_PROPERTY_STAGE - CLEAR FOCUS")
	ENDIF
	CLEAR_BIT(iLocalBS2,LOCAL_BS2_DisabledRadar)
	
	CLEAR_BIT(iLocalBS, LOCAL_BS_BUZZER_GIVE_GOTO_TASK)	
	CLEAR_BIT(iLocalBS, LOCAL_BS_BUZZER_GIVE_RING_TASK)	
	CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_ENTERING_GARAGE)
	CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_READY_FOR_VEHICLE_FADE)
	IF globalPropertyEntryData.bLeftOrRejectedBuzzer
	OR IS_BIT_SET(iLocalBS2,LOCAL_BS2_SetEntryData)
		IF NOT g_bPropInteriorScriptRunning
		AND NOT IS_BIT_SET(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_AcceptedInvite)
		AND NOT IS_BIT_SET(mpPropMaintain.iBS, MP_PROP_MAINTAIN_BS_LAUNCHED_INT_SCRIPT)
			IF globalPropertyEntryData.bLeftOrRejectedBuzzer
				CDEBUG1LN(DEBUG_SAFEHOUSE, "globalPropertyEntryData.bLeftOrRejectedBuzzer clearing global entry data")
			ELIF IS_BIT_SET(iLocalBS2,LOCAL_BS2_SetEntryData)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "LOCAL_BS2_SetEntryData clearing global entry data")
			ENDIF
			CLEAR_GLOBAL_ENTRY_DATA()
			CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_SPAWNING_IN_A_PROPERTY)
		
		ENDIF
	ENDIF
	CLEAR_BIT(iLocalBS2,LOCAL_BS2_SetEntryData)
	IF IS_BIT_SET(iLocalBS2,LOCAL_BS2_LockCarOnMenu)
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF DOES_ENTITY_EXIST(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
				IF NETWORK_HAS_CONTROL_OF_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
					IF carDoorLocks = VEHICLELOCK_NONE
						SET_VEHICLE_DOORS_LOCKED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),VEHICLELOCK_UNLOCKED)
					ELSE
						SET_VEHICLE_DOORS_LOCKED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),carDoorLocks)
					ENDIF
					CDEBUG1LN(DEBUG_SAFEHOUSE, "Player un locking car doors for menu")
				ENDIF	
			ENDIF
		ELSE
			IF IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_DOING_HEIST_DROPOFF)
				IF DOES_ENTITY_EXIST(heistUnlockCar)
					IF NETWORK_HAS_CONTROL_OF_ENTITY(heistUnlockCar)
						SET_VEHICLE_DOORS_LOCKED(heistUnlockCar,VEHICLELOCK_UNLOCKED)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: player unlocked vehicle after driving in for heist dropoff")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(PLAYER_PED_ID(), KNOCKOFFVEHICLE_DEFAULT)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: LOCAL_BS2_LockCarOnMenu = FALSE")
		CLEAR_BIT(iLocalBS2,LOCAL_BS2_LockCarOnMenu)
	ENDIF
	
	IF IS_BIT_SET(iLocalBS2,LOCAL_BS2_DisabledStore)
		SET_STORE_ENABLED(TRUE)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "enabling store reset to entrance stage- EXT")
		CLEAR_BIT(iLocalBS2,LOCAL_BS2_DisabledStore)
	ENDIF
	
	CLEAR_BIT(iLocalBS2,LOCAL_BS2_DoNotBuzzOthers)
	iSelectedPropertyToBuy = 0

	IF DOES_CAM_EXIST(propertyCam0)
		DESTROY_CAM(propertyCam0)
	ENDIF
	IF DOES_CAM_EXIST(propertyCam1)
		DESTROY_CAM(propertyCam1)
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
	ENDIF

	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_MP_PROPERTY_INT")) <= 0
	AND NOT g_bPropInteriorScriptRunning
		CLEAR_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_ENTER_GARAGE)
		CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_ENTERING_A_PROPERTY)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Clearing GLOBAL_PROPERTY_ENTRY_BS_ENTER_GARAGE")
	ENDIF
	VEHICLE_INDEX vehFadeOut
	IF IS_BIT_SET(iLocalBS2,LOCAL_BS2_bFadeOutWarpIntoGarage)
		IF IS_BIT_SET(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_CAR_MADE_INVISIBLE_ON_EXTERIOR)
			//IF NOT IS_PLAYER_VISIBLE_TO_SCRIPT(PLAYER_ID())	
			//ENDIF
			//NETWORK_SET_LOCAL_PLAYER_INVINCIBLE_TIME(0)
			IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
				
				vehFadeOut = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				IF GET_PED_IN_VEHICLE_SEAT(vehFadeOut) = PLAYER_PED_ID()
					
					//NETWORK_FADE_IN_ENTITY(vehFadeOut, TRUE)
					SET_ENTITY_VISIBLE(vehFadeOut,TRUE)
					SET_ENTITY_VISIBLE(PLAYER_PED_ID(),TRUE)
					//SET_BIT(iLocalBS2,LOCAL_BS2_bFadeOutWarpIntoGarage)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "NETWORK_FADE_IN_ENTITY called from exterior script- VEH")
					CLEAR_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_CAR_MADE_INVISIBLE_ON_EXTERIOR)
				ENDIF
			ELSE
				//FADE_OUT_LOCAL_PLAYER(FALSE)
				//NETWORK_FADE_IN_ENTITY(PLAYER_PED_ID(), TRUE)
				SET_ENTITY_VISIBLE(PLAYER_PED_ID(),TRUE)
				NETWORK_SET_LOCAL_PLAYER_INVINCIBLE_TIME(0)
				CLEAR_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_CAR_MADE_INVISIBLE_ON_EXTERIOR)
				//SET_BIT(iLocalBS2,LOCAL_BS2_bFadeOutWarpIntoGarage)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "NETWORK_FADE_IN_ENTITY called from exterior script - PED")
			ENDIF
			FORCE_BACK_INTO_POSITION_ABORTED_ENTRY(1)
			CLEAR_BIT(iLocalBS2,LOCAL_BS2_bFadeOutWarpIntoGarage)
		ENDIF
	ENDIF
	
	IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_MP_PROPERTY_INT")) <= 0
	AND NOT g_bPropInteriorScriptRunning)
	OR IS_PLAYER_SWITCH_IN_PROGRESS()
		IF g_bMadeCarInvunerableOnEntry
			IF IS_NET_PLAYER_OK(PLAYER_ID(),TRUE,FALSE)
				IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
					vehFadeOut = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					IF GET_PED_IN_VEHICLE_SEAT(vehFadeOut) = PLAYER_PED_ID()
						SET_ENTITY_CAN_BE_DAMAGED(vehFadeOut,TRUE)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "SET_ENTITY_CAN_BE_DAMAGED TRUE called from exterior script- VEH")	
					ELSE
						CDEBUG1LN(DEBUG_SAFEHOUSE, "SET_ENTITY_CAN_BE_DAMAGED not driver")	
					ENDIF
				ELIF DOES_ENTITY_EXIST(warpInControl.vehID)
					IF NETWORK_HAS_CONTROL_OF_ENTITY(warpInControl.vehID)
						IF IS_VEHICLE_DRIVEABLE(warpInControl.vehID)
							SET_ENTITY_CAN_BE_DAMAGED(warpInControl.vehID,TRUE)
							SET_VEHICLE_DOORS_LOCKED(warpInControl.vehID,VEHICLELOCK_UNLOCKED)
							CDEBUG1LN(DEBUG_SAFEHOUSE, "SET_ENTITY_CAN_BE_DAMAGED TRUE called from exterior script- VEH")	
						ELSE
							CDEBUG1LN(DEBUG_SAFEHOUSE, "SET_ENTITY_CAN_BE_DAMAGED not driveable")	
						ENDIF
					ELSE
						CDEBUG1LN(DEBUG_SAFEHOUSE, "SET_ENTITY_CAN_BE_DAMAGED not in control")	
					ENDIF
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "SET_ENTITY_CAN_BE_DAMAGED not sitting in vehicle")	
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "SET_ENTITY_CAN_BE_DAMAGED not ok player")	
			ENDIF
			CDEBUG1LN(DEBUG_SAFEHOUSE, "SET_ENTITY_CAN_BE_DAMAGED cleared flag to check")	
			g_bMadeCarInvunerableOnEntry = FALSE
		ENDIF
	ELSE
		CDEBUG1LN(DEBUG_SAFEHOUSE, "SET_ENTITY_CAN_BE_DAMAGED bypassed interior script is running no switch")	
	ENDIF
	
	CLEAR_BIT(iLocalBS,LOCAL_BS_STARTED_CUTSCENE)

	CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Clearing PROPERTY_BROADCAST_BS_PLAYER_ENTERING_A_PROPERTY")
	CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT:RESET_TO_ENTRANCE_TO_PROPERTY_STAGE called") NET_NL()
ENDPROC

FUNC BOOL SHOULD_MENU_UPDATE()
	PLAYER_INDEX playerID
	//addOptionForPlayer = FALSE
	INT iBuildingLoop
	
	SWITCH iCurrentMenuType 
		CASE MENU_TYPE_PURCHASE
		
		BREAK
		
		CASE MENU_TYPE_BUZZER
			playerID = INT_TO_PLAYERINDEX(iSlowLoopCounter)
			IF playerID != PLAYER_ID()
				IF NOT IS_BIT_SET(iPlayersInBuilding, NATIVE_TO_INT(playerID))
					IF IS_NET_PLAYER_OK(playerID,FALSE)
						REPEAT mpProperties[iCurrentPropertyID].building.iNumProperties iBuildingLoop
							IF GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iCurrentlyInsideProperty = mpProperties[iCurrentPropertyID].building.iPropertiesInBuilding[iBuildingLoop]
							AND IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iBS,PROPERTY_BROADCAST_BS_IN_OWNED_PROPERTY)	
							AND NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_EXITING_PROPERTY)
								CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Menu should update as players in building bit is not set for player in property")
								RETURN TRUE
							ENDIF
						ENDREPEAT
					ENDIF
				ELSE
					IF IS_NET_PLAYER_OK(playerID)
						IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iBS,PROPERTY_BROADCAST_BS_IN_OWNED_PROPERTY)
							CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Menu should update as player no longer in property")
							RETURN TRUE
						ELSE
							IF GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iCurrentlyInsideProperty != iPlayersPropertyNum[iSlowLoopCounter]
								CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Menu should update as player in property now in a new property")
								RETURN TRUE
							ENDIF
							IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_EXITING_PROPERTY)
								CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Menu should update as player in property now exiting")
								RETURN TRUE
							ENDIF
						ENDIF
					ELSE
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Menu should update as player in property not ok")
						RETURN TRUE
					ENDIF
					
				ENDIF
			ENDIF
			IF HAVE_CONTROLS_CHANGED(FRONTEND_CONTROL)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: HAVE_CONTROLS_CHANGED returned true")
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	iSlowLoopCounter++
	IF iSlowLoopCounter >= NUM_NETWORK_PLAYERS
		iSlowLoopCounter = 0
	ENDIF
	IF tempPropertyExtMenu.iAllNearby != -1
	OR tempPropertyExtMenu.iAllFriendsCrew != -1
	OR tempPropertyExtMenu.iOrganisationNearby != -1
		IF PROPERTY_NEAR_PLYS_OPT_AVAILABLE(GET_PROPERTY_ENTRANCE_APPROXIMATE_LOCATION(iCurrentPropertyID,iEntrancePlayerIsUsing),tempPropertyExtMenu.bFriendsNearby, tempPropertyExtMenu.bOrganisationNearby,  iCurrentPropertyID,iEntrancePlayerIsUsing)
			IF Is_Player_Currently_On_MP_Heist(PLAYER_ID())
			OR Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID())
			OR Is_Player_Currently_On_MP_Contact_Mission(PLAYER_ID())
				IF tempPropertyExtMenu.iAllNearby = -1
					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: tempPropertyExtMenu.iAllNearby = -1")
					RETURN TRUE
				ENDIF
			ENDIF
			IF tempPropertyExtMenu.bFriendsNearby
				IF tempPropertyExtMenu.iAllFriendsCrew = -1
					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: tempPropertyExtMenu.iAllFriendsCrew = -1")
					RETURN TRUE
				ENDIF
			ENDIF
			IF tempPropertyExtMenu.bOrganisationNearby
				IF tempPropertyExtMenu.iOrganisationNearby = -1
					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: tempPropertyExtMenu.iOrganisationNearby  = -1")
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: PROPERTY_NEAR_PLYS_OPT_AVAILABLE returned false")
			RETURN TRUE
		ENDIF
	ENDIF
			
	RETURN FALSE
ENDFUNC

FUNC BOOL HANDLE_PROPERTY_OPTIONS_MENU_NAVIGATION_DELAY(CONTROL_ACTION theInput)
	IF (NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL,theInput)
	AND NOT IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,theInput))
	OR HAS_NET_TIMER_EXPIRED(tempPropertyExtMenu.menuNavigationDelay,250)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC


BOOL bProcessingBasket = FALSE		
INT iProcessingBasketStage = SHOP_BASKET_STAGE_ADD

STRUCT PROPERTY_SHOPPING_BASKET_STRUCT
	INT m_iProcessSuccess = -1

	INT m_iItemId = -1
	INT m_iInventoryKey = -1

	INT m_iSellingPrice = 0
	INT m_iSellingItemId = 0
ENDSTRUCT
PROPERTY_SHOPPING_BASKET_STRUCT sPropertyShoppingBasket

FUNC BOOL PROCESSING_PROPERTY_SHOPPING_BASKET(INT &iProcessSuccess, INT iPrice, INT iStatValue,
        SHOP_ITEM_CATEGORIES eCategory, ITEM_ACTION_TYPES eAction,
        INT iItemId, INT iInventoryKey, INT iReplacePropertySlot, INT iProperty,
        INT iSellingPrice = 0, INT iSellingItemId = 0)
		
		INT iItemInteriorID = GET_PROPERTY_INTERIOR_KEY_FOR_CATALOGUE(iCurrentInteriorVariationTooPurchase, iProperty)
		INT iInventoryInteriorID = GET_PROPERTY_INTERIOR_INVENTORY_KEY_FOR_CATALOGUE(iReplacePropertySlot)
    IF bProcessingBasket
        SWITCH iProcessingBasketStage
            // Add item to basket
            CASE SHOP_BASKET_STAGE_ADD
				
				IF (iPrice <= 0)
				OR (iPrice > 0 AND NETWORK_CAN_SPEND_MONEY(iPrice,FALSE,TRUE,FALSE))
					//
				ELSE
					CPRINTLN(DEBUG_NET_AMBIENT, "[BASKET] - failed for having insufficient cash (iPrice:$", iPrice, " > bank:$", NETWORK_GET_VC_BANK_BALANCE(), " + wallet:$", NETWORK_GET_VC_WALLET_BALANCE(), ")")
					iProcessingBasketStage = SHOP_BASKET_STAGE_FAILED
					RETURN TRUE
				ENDIF
				
				// Override the players cash.
				INT iWalletAmount, iBankAmount, iTempPrice
				iWalletAmount = 0
				iBankAmount = 0
				iTempPrice = (iPrice - iSellingPrice)
				// Bank first
				IF (NETWORK_GET_VC_BANK_BALANCE() > 0)
					IF NETWORK_GET_VC_BANK_BALANCE() >= iTempPrice
						iBankAmount = iTempPrice
					ELSE
						iBankAmount = iTempPrice-(iTempPrice-NETWORK_GET_VC_BANK_BALANCE())
					ENDIF
					iTempPrice -= iBankAmount
				ENDIF
				IF iTempPrice > 0
					IF (NETWORK_GET_VC_WALLET_BALANCE() > 0)
						IF NETWORK_GET_VC_WALLET_BALANCE() >= iTempPrice
							iWalletAmount = iTempPrice
						ELSE
							iWalletAmount = iTempPrice-(iTempPrice-NETWORK_GET_VC_WALLET_BALANCE())
						ENDIF
						iTempPrice -= iWalletAmount
					ENDIF
				ENDIF
				#IF IS_DEBUG_BUILD
				IF iTempPrice > 0
					CASSERTLN(DEBUG_NET_AMBIENT, "PROCESSING_PROPERTY_SHOPPING_BASKET - Player can't afford this item! iTempPrice=", iTempPrice, ", iWalletAmount=", iWalletAmount, ", iBankAmount", iBankAmount)
				ENDIF
				#ENDIF
				
                CPRINTLN(DEBUG_NET_AMBIENT, "[BASKET] - Adding property to basket eCategory:", GET_SHOP_ITEM_CATEGORIES_DEBUG_STRING(eCategory), ", eAction:", GET_CASH_TRANSACTION_ACTION_TYPE_DEBUG_STRING(eAction), ", iPrice:$", iPrice, ", iItemId:", iItemId, ", iInventoryKey:", iInventoryKey)
                IF NETWORK_REQUEST_BASKET_TRANSACTION(NET_SHOP_TTYPE_BASKET, eCategory, iItemId, eAction, 1, iPrice, iStatValue, CATALOG_ITEM_FLAG_BANK_THEN_WALLET, iInventoryKey)
                AND (NETWORK_REQUEST_BASKET_TRANSACTION(NET_SHOP_TTYPE_BASKET, eCategory, iItemInteriorID, eAction, 1, 0, 0, CATALOG_ITEM_FLAG_BANK_THEN_WALLET, iInventoryInteriorID))
                    // Property we are trading in.
                    IF iSellingItemId != 0
                        CPRINTLN(DEBUG_NET_AMBIENT, "[BASKET] - Adding trade property to basket eCategory:", GET_SHOP_ITEM_CATEGORIES_DEBUG_STRING(eCategory), ", eAction:", GET_CASH_TRANSACTION_ACTION_TYPE_DEBUG_STRING(eAction), ", iSellingPrice:$", iSellingPrice)
                        IF NETWORK_REQUEST_BASKET_TRANSACTION(NET_SHOP_TTYPE_BASKET, eCategory, iSellingItemId, eAction, 1, iSellingPrice, 1, CATALOG_ITEM_FLAG_BANK_THEN_WALLET, iInventoryKey)
                            //
                        ELSE
                            CPRINTLN(DEBUG_NET_AMBIENT, "[BASKET] - failed to add selling item to basket")
                            iProcessingBasketStage = SHOP_BASKET_STAGE_FAILED
                        ENDIF
                    ENDIF
                    
                    IF iProcessingBasketStage != SHOP_BASKET_STAGE_FAILED
                        IF NETWORK_START_BASKET_TRANSACTION_CHECKOUT()
                            CPRINTLN(DEBUG_NET_AMBIENT, "[BASKET] - basket checkout started")
							
							CPRINTLN(DEBUG_NET_AMBIENT, "CHANGE_FAKE_MP_CASH(wallet=$-", iWalletAmount, ", bank=$-", iBankAmount, "): iPrice=$", iPrice, ", bankBalance=$", NETWORK_GET_VC_BANK_BALANCE(), ", walletBalance=$", NETWORK_GET_VC_WALLET_BALANCE())
							USE_FAKE_MP_CASH(TRUE)
							CHANGE_FAKE_MP_CASH(-iWalletAmount, -iBankAmount)
							
							iProcessingBasketStage = SHOP_BASKET_STAGE_PENDING
                        ELSE
                            CPRINTLN(DEBUG_NET_AMBIENT, "[BASKET] - failed to start basket checkout")
                            iProcessingBasketStage = SHOP_BASKET_STAGE_FAILED
                        ENDIF
                    ENDIF
                ELSE
                    CPRINTLN(DEBUG_NET_AMBIENT, "[BASKET] - failed to add item to basket")
                    iProcessingBasketStage = SHOP_BASKET_STAGE_FAILED
                ENDIF
            BREAK
            // Pending
            CASE SHOP_BASKET_STAGE_PENDING
				INT iBasketTransactionID
				iBasketTransactionID = GET_BASKET_TRANSACTION_SCRIPT_INDEX()
				
                IF IS_CASH_TRANSACTION_COMPLETE(iBasketTransactionID)
                    IF GET_CASH_TRANSACTION_STATUS(iBasketTransactionID) = CASH_TRANSACTION_STATUS_SUCCESS
                        CPRINTLN(DEBUG_NET_AMBIENT, "[BASKET] - Basket transaction [", iBasketTransactionID, "] finished, success!")
                        //DELETE_CASH_TRANSACTION(iBasketTransactionID) -- we delete this later
						iProcessingBasketStage = SHOP_BASKET_STAGE_SUCCESS
                    ELSE
                        CPRINTLN(DEBUG_NET_AMBIENT, "[BASKET] - Basket transaction [", iBasketTransactionID, "] finished, failed!")
						DELETE_CASH_TRANSACTION(iBasketTransactionID)
                        iProcessingBasketStage = SHOP_BASKET_STAGE_FAILED
                    ENDIF
                ENDIF
            BREAK
            
            // Success
            CASE SHOP_BASKET_STAGE_SUCCESS
                bProcessingBasket = FALSE
                iProcessingBasketStage = SHOP_BASKET_STAGE_ADD
				
				USE_FAKE_MP_CASH(FALSE)
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH_CHANGE)
                
                iProcessSuccess = 2
                RETURN FALSE
            BREAK
            
            //Failed
            CASE SHOP_BASKET_STAGE_FAILED
                bProcessingBasket = FALSE
                iProcessingBasketStage = SHOP_BASKET_STAGE_ADD
				
				USE_FAKE_MP_CASH(FALSE)
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH_CHANGE)
                
                iProcessSuccess = 0
                RETURN FALSE
            BREAK
        ENDSWITCH
        
        RETURN TRUE
    ENDIF
    
    iProcessingBasketStage = SHOP_BASKET_STAGE_ADD
    iProcessSuccess = -1
    RETURN FALSE
ENDFUNC

FUNC BOOL PURCHASE_PROPERTY(INT iPropertyID, INT iReplacePropertySlot)

	//Override the property to purchase when the flag from selecting an interior has been set
	IF bInteriorToPurchaseSelected
		IF iCustomApartmentTooPurchase != -1
		OR iCustomApartmentTooPurchase = PROPERTY_CUSTOM_APT_1_BASE
		OR iCustomApartmentTooPurchase = PROPERTY_CUSTOM_APT_2
		OR iCustomApartmentTooPurchase = PROPERTY_CUSTOM_APT_3
			iPropertyID = iCustomApartmentTooPurchase
			CDEBUG1LN(DEBUG_SAFEHOUSE, "PURCHASE_PROPERTY Overriding iPropertyID: ", iPropertyID, " iCurrentInteriorVariationTooPurchase: ", iCurrentInteriorVariationTooPurchase)
		ELSE
			CASSERTLN(DEBUG_SAFEHOUSE, "bInteriorToPurchaseSelected is TRUE but an invalid apartment was specified. iCustomApartmentTooPurchase: ", iCustomApartmentTooPurchase)
		ENDIF
	ENDIF
	IF iReplacePropertySlot <= -1
		iReplacePropertySlot = 0
	ENDIF
	
	IF g_bPropertyExtDisableCriminalEnterpriseStarterPack
		IF SHOULD_PROPERTY_BE_FREE_FOR_PLAYER(iPropertyID)
		AND COULD_PROPERTY_BE_FREE_FOR_PLAYER_WITH_CRIMINAL_ENTERPRISE_STARTER_PACK(iPropertyID)
			CWARNINGLN(DEBUG_NET_AMBIENT, "PURCHASE_PROPERTY: disabled property purchase based on global flag")
			PLAY_SOUND_FRONTEND(-1, "Click_Fail","WEB_NAVIGATION_SOUNDS_PHONE") 
			RETURN FALSE
		ENDIF
	ENDIF
	
	INT iTradeIn = GET_VALUE_OF_CURRENTLY_OWNED_PROPERTY(iReplacePropertySlot)
	TEXT_LABEL_15 tl_15PropertyName
	INT iFullCost
	IF NOT SHOULD_PROPERTY_BE_FREE_FOR_PLAYER(iPropertyID)
		iFullCost = CEIL((GET_VALUE_OF_PROPERTY(iPropertyID)*g_sMPTunables.fPropertyMultiplier))
	ELSE
		iFullCost = 0
	ENDif
	
	INT iCost = iFullCost-iTradeIn
	
//	IF NETWORK_GET_VC_BANK_BALANCE() >= iCost
//		IF iCost > 0 AND NETWORK_CAN_SPEND_MONEY(iCost,TRUE,FALSE,FALSE)
//			CDEBUG1LN(DEBUG_SAFEHOUSE, "CDM: Player can spend money!-1")
//		ELSE
//			CDEBUG1LN(DEBUG_SAFEHOUSE, "CDM: Player cant spend money!-1")
//		ENDIF
//	ENDIF
	
	IF (iCost <= 0)
	OR (iCost > 0 AND NETWORK_CAN_SPEND_MONEY(iCost,FALSE,TRUE,FALSE))
	OR bProcessingBasket
	    IF USE_SERVER_TRANSACTIONS()
	    	IF NOT bProcessingBasket
				INT iPropertyVariation = 0
				BOOL bStarterPackItem = FALSE
				IF SHOULD_PROPERTY_BE_FREE_FOR_PLAYER(iPropertyID)
					IF NOT COULD_PROPERTY_BE_FREE_FOR_PLAYER_WITH_CRIMINAL_ENTERPRISE_STARTER_PACK(iPropertyID)
						iPropertyVariation = 1
					ELSE
						bStarterPackItem = TRUE
					ENDIF
				ENDIF
				
		        iProcessingBasketStage = SHOP_BASKET_STAGE_ADD
		        sPropertyShoppingBasket.m_iItemId = GET_PROPERTY_KEY_FOR_CATALOGUE(iPropertyID, iPropertyVariation, bStarterPackItem)
		        sPropertyShoppingBasket.m_iInventoryKey = GET_PROPERTY_INVENTORY_KEY_FOR_CATALOGUE(iReplacePropertySlot)
		        
		        #IF IS_DEBUG_BUILD
		        tl_15PropertyName= GET_PROPERTY_NAME(iPropertyID)
		        IF IS_STRING_NULL_OR_EMPTY(tl_15PropertyName)
		            CASSERTLN(DEBUG_NET_AMBIENT, "PURCHASE_PROPERTY: label for last clicked property index #", iPropertyID, " is empty, $", iFullCost, "!!")
		        ELSE
		            CDEBUG1LN(DEBUG_NET_AMBIENT, "PURCHASE_PROPERTY: label for last clicked property index #", iPropertyID, " is \"", tl_15PropertyName, "\", $", iFullCost, "!")
		        ENDIF
		        #ENDIF
		        //for selling property
		        sPropertyShoppingBasket.m_iSellingPrice = 0
		        sPropertyShoppingBasket.m_iSellingItemId = 0
		        INT iReplaceProperty = GET_OWNED_PROPERTY(iReplacePropertySlot)
				
				IF iReplaceProperty > 0
					INT iSellingPropertyVariation = 0
					BOOL bSellingPropertyStarterPackItem = FALSE
					IF SHOULD_PROPERTY_BE_FREE_FOR_PLAYER(iReplaceProperty)
					AND iTradeIn = 0
						IF NOT COULD_PROPERTY_BE_FREE_FOR_PLAYER_WITH_CRIMINAL_ENTERPRISE_STARTER_PACK(iReplaceProperty)
							iSellingPropertyVariation = 1
						ELSE
							bSellingPropertyStarterPackItem = TRUE
						ENDIF
						iTradeIn = 0
					ENDIF
		            sPropertyShoppingBasket.m_iSellingPrice = iTradeIn
		            sPropertyShoppingBasket.m_iSellingItemId = GET_PROPERTY_KEY_FOR_CATALOGUE(iReplaceProperty, iSellingPropertyVariation, bSellingPropertyStarterPackItem)
		            CPRINTLN(DEBUG_NET_AMBIENT, "[BASKET] - Trading in property slot ", iReplacePropertySlot, " with sale value $", sPropertyShoppingBasket.m_iSellingPrice)
		            
		            #IF IS_DEBUG_BUILD
		            tl_15PropertyName = GET_PROPERTY_NAME(iReplaceProperty)
		            IF IS_STRING_NULL_OR_EMPTY(tl_15PropertyName)
		                CASSERTLN(DEBUG_NET_AMBIENT, "PURCHASE_PROPERTY: label for traded property[", iReplacePropertySlot, "] index #", iReplaceProperty, " is empty, $", iTradeIn, "!")
		            ELSE
		                CDEBUG1LN(DEBUG_NET_AMBIENT, "PURCHASE_PROPERTY: label for traded property[", iReplacePropertySlot, "] index #", iReplaceProperty, " is \"", tl_15PropertyName, "\", $", iTradeIn, "!")
		            ENDIF
		            #ENDIF
		        ENDIF
		        //
		        
		        sPropertyShoppingBasket.m_iProcessSuccess = -1
		        bProcessingBasket = TRUE
			ENDIF
	        ///////////////////////////////////////////
	        ///      TRANSACTION FOR PROPERTY
	        WHILE PROCESSING_PROPERTY_SHOPPING_BASKET(sPropertyShoppingBasket.m_iProcessSuccess, iFullCost, iReplacePropertySlot, CATEGORY_INVENTORY_PROPERTIE, NET_SHOP_ACTION_BUY_PROPERTY, sPropertyShoppingBasket.m_iItemId, sPropertyShoppingBasket.m_iInventoryKey, iReplacePropertySlot, iPropertyID, sPropertyShoppingBasket.m_iSellingPrice, sPropertyShoppingBasket.m_iSellingItemId)
	            CDEBUG3LN(DEBUG_NET_AMBIENT, "PURCHASE_PROPERTY: processing stage ", iProcessingBasketStage, "...")
				RETURN FALSE
	        ENDWHILE
	        
	        SWITCH sPropertyShoppingBasket.m_iProcessSuccess
	            CASE 0
	                CWARNINGLN(DEBUG_NET_AMBIENT, "PURCHASE_PROPERTY: failed to process transaction")
	                PLAY_SOUND_FRONTEND(-1, "Click_Fail","WEB_NAVIGATION_SOUNDS_PHONE") 
	                RETURN FALSE
	            BREAK
	            CASE 2
	                CPRINTLN(DEBUG_NET_AMBIENT, "PURCHASE_PROPERTY: success!!")
	            BREAK
	            DEFAULT
	                CASSERTLN(DEBUG_NET_AMBIENT, "PURCHASE_PROPERTY: unknown sPropertyShoppingBasket.m_iProcessSuccess: \"", sPropertyShoppingBasket.m_iProcessSuccess, "\"")
	            BREAK
	        ENDSWITCH
	    ELSE
	        CPRINTLN(DEBUG_NET_AMBIENT, "PURCHASE_PROPERTY: ignore NETWORK_REQUEST_BASKET_TRANSACTION")
	    ENDIF
		
		
		
		IF GET_OWNED_PROPERTY(iReplacePropertySlot) > 0 
			CDEBUG1LN(DEBUG_SAFEHOUSE, "PURCHASE_PROPERTY: incrementing trade in.")
				INCREMENT_BY_MP_INT_CHARACTER_AWARD(MP_AWARD_TRADE_IN_YOUR_PROPERTY,1)
			
			IF HAS_MP_CRIMINAL_ENTERPRISE_PREMIUM_OR_STARTER_ACCESS()
				INT iReplaceProperty = GET_OWNED_PROPERTY(iReplacePropertySlot)
				IF SHOULD_PROPERTY_BE_FREE_FOR_PLAYER(iReplaceProperty)
					SWITCH iReplaceProperty
						CASE PROPERTY_OFFICE_2_BASE			SET_PLAYER_PURCHASED_CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT(CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_PROPERTY_MAZE_BANK_WEST_EXECUTIVE_OFFICE, FALSE) BREAK
						CASE PROPERTY_CLUBHOUSE_4_BASE_A	SET_PLAYER_PURCHASED_CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT(CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_PROPERTY_GREAT_CHAPARRAL_BIKER_CLUBHOUSE, FALSE) BREAK
						CASE PROPERTY_LOW_APT_2				SET_PLAYER_PURCHASED_CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT(CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_PROPERTY_1561_SAN_VITAS_STREET_APARTMENT, FALSE) BREAK
						CASE PROPERTY_GARAGE_NEW_19			SET_PLAYER_PURCHASED_CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT(CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_PROPERTY_1337_EXCEPTIONALISTS_WAY_10_CAR_GARAGE, FALSE) BREAK
					ENDSWITCH
				ENDIF
			ENDIF
		ENDIF
		globalPropertyEntryData.iCostOfPropertyJustBought = iCost
		IF globalPropertyEntryData.iCostOfPropertyJustBought >= 0
			IF iTradeIn >= 0
				IF USE_SERVER_TRANSACTIONS()
					CDEBUG1LN(DEBUG_SAFEHOUSE, "[CASH] NET_GAMESERVER_SET_TELEMETRY_NONCE_SEED - basket")
					NET_GAMESERVER_SET_TELEMETRY_NONCE_SEED(GET_CASH_TRANSACTION_ID_FROM_INDEX(GET_BASKET_TRANSACTION_SCRIPT_INDEX()))
				ENDIF
				
				tl_15PropertyName = GET_PROPERTY_NAME(GET_OWNED_PROPERTY(iReplacePropertySlot))
				NETWORK_EARN_FROM_PROPERTY(iTradeIn, GET_HASH_KEY(tl_15PropertyName))
				CPRINTLN(DEBUG_SAFEHOUSE, "PURCHASE_PROPERTY: Trading property \"", tl_15PropertyName, "\" for $", iTradeIn)
			ENDIF
			tl_15PropertyName = GET_PROPERTY_NAME(iPropertyID)
			
			IF USE_SERVER_TRANSACTIONS()
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[CASH] NET_GAMESERVER_SET_TELEMETRY_NONCE_SEED - basket")
				NET_GAMESERVER_SET_TELEMETRY_NONCE_SEED(GET_CASH_TRANSACTION_ID_FROM_INDEX(GET_BASKET_TRANSACTION_SCRIPT_INDEX()))
			ENDIF
			
			NETWORK_BUY_PROPERTY(iFullCost,GET_HASH_KEY(tl_15PropertyName),FALSE,TRUE)
			CPRINTLN(DEBUG_SAFEHOUSE, "PURCHASE_PROPERTY: Buying property \"", tl_15PropertyName, "\" for $", iFullCost)
			
			IF USE_SERVER_TRANSACTIONS()
				DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
			ENDIF
			
		ELSE 
			IF globalPropertyEntryData.iCostOfPropertyJustBought < 0
				IF iTradeIn >= 0
					IF USE_SERVER_TRANSACTIONS()
						CDEBUG1LN(DEBUG_SAFEHOUSE, "[CASH] NET_GAMESERVER_SET_TELEMETRY_NONCE_SEED - basket")
						NET_GAMESERVER_SET_TELEMETRY_NONCE_SEED(GET_CASH_TRANSACTION_ID_FROM_INDEX(GET_BASKET_TRANSACTION_SCRIPT_INDEX()))
					ENDIF
					
					tl_15PropertyName = GET_PROPERTY_NAME(GET_OWNED_PROPERTY(iReplacePropertySlot))
					NETWORK_EARN_FROM_PROPERTY(iTradeIn, GET_HASH_KEY(tl_15PropertyName))
					CPRINTLN(DEBUG_SAFEHOUSE, "PURCHASE_PROPERTY: Trading property \"", tl_15PropertyName, "\" for $", iTradeIn)
				ENDIF
				
				IF USE_SERVER_TRANSACTIONS()
					CDEBUG1LN(DEBUG_SAFEHOUSE, "[CASH] NET_GAMESERVER_SET_TELEMETRY_NONCE_SEED - basket")
					NET_GAMESERVER_SET_TELEMETRY_NONCE_SEED(GET_CASH_TRANSACTION_ID_FROM_INDEX(GET_BASKET_TRANSACTION_SCRIPT_INDEX()))
				ENDIF
				
				tl_15PropertyName = GET_PROPERTY_NAME(iPropertyID)
				NETWORK_BUY_PROPERTY(iFullCost,GET_HASH_KEY(tl_15PropertyName),FALSE,TRUE)
				CPRINTLN(DEBUG_SAFEHOUSE, "PURCHASE_PROPERTY: Buying property \"", tl_15PropertyName, "\" for $", iFullCost)
				
				IF USE_SERVER_TRANSACTIONS()
					DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
				ENDIF
			ENDIF
		ENDIF
		IF GET_OWNED_PROPERTY(iReplacePropertySlot) > 0
			REQUEST_SYSTEM_ACTIVITY_TYPE_MOVED_HOUSE()
		ENDIF
		IF iReplacePropertySlot = PROPERTY_OWNED_SLOT_APT_1  //MAX_OWNED_PROPERTIES
			IF GET_OWNED_PROPERTY(iReplacePropertySlot) <= 0 
				REQUEST_SYSTEM_ACTIVITY_TYPE_PURCHASED_FIRST_PROPERTY()
			ENDIF
			SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_PROPERTY_HOUSE,iPropertyID)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PROPERTY_HOUSE_TIME,GET_CLOUD_TIME_AS_INT())
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PROPERTY_HOUSE_NEW,iPropertyID)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PROPERTY_HOUSE_NEW_TIME,GET_CLOUD_TIME_AS_INT())
			IF NOT SHOULD_PROPERTY_BE_FREE_FOR_PLAYER(iPropertyID)
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iCurrentPropertyValue = CEIL(TO_FLOAT(GET_VALUE_OF_PROPERTY(iPropertyID))*g_sMPTunables.fPropertyMultiplier)
			ELSE
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iCurrentPropertyValue = 0
			ENDIF
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PROPERTY_HOUSE_NEW_VALUE,g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iCurrentPropertyValue)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Player just purchased property for slot 0")
		ELIF iReplacePropertySlot = PROPERTY_OWNED_SLOT_APT_2
			IF GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_APT_2) <= 0
				REQUEST_SYSTEM_ACTIVITY_TYPE_PURCHASED_SECOND_PROPERTY()
			ENDIF
			SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_MULTI_PROPERTY_1, iPropertyID)
			IF NOT SHOULD_PROPERTY_BE_FREE_FOR_PLAYER(iPropertyID)
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiProperty1Value = CEIL(TO_FLOAT(GET_VALUE_OF_PROPERTY(iPropertyID))*g_sMPTunables.fPropertyMultiplier)
			ELSE
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiProperty1Value = 0
			ENDIF
			SET_MP_INT_CHARACTER_STAT(MP_STAT_MULTI_PROPERTY_VALUE_1,g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiProperty1Value)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Player just purchased property for slot 1")
		ELIF iReplacePropertySlot = PROPERTY_OWNED_SLOT_APT_3
			IF GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_APT_3) <= 0
				REQUEST_SYSTEM_ACTIVITY_TYPE_PURCHASED_THIRD_PROPERTY()
			ENDIF
			SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_MULTI_PROPERTY_2, iPropertyID)
			IF NOT SHOULD_PROPERTY_BE_FREE_FOR_PLAYER(iPropertyID)
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[0] = CEIL(TO_FLOAT(GET_VALUE_OF_PROPERTY(iPropertyID))*g_sMPTunables.fPropertyMultiplier)
			ELSE
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[0] = 0
			ENDIF
			SET_MP_INT_CHARACTER_STAT(MP_STAT_MULTI_PROPERTY_VALUE_2,g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[0])
			PRINTLN("AM_MP_PROPERTY_EXT: Player just purchased property for slot 2")
		ELIF iReplacePropertySlot = PROPERTY_OWNED_SLOT_APT_4
			SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_MULTI_PROPERTY_3, iPropertyID)
			IF NOT SHOULD_PROPERTY_BE_FREE_FOR_PLAYER(iPropertyID)
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[1] = CEIL(TO_FLOAT(GET_VALUE_OF_PROPERTY(iPropertyID))*g_sMPTunables.fPropertyMultiplier)
			ELSE
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[1] = 0
			ENDIF
			SET_MP_INT_CHARACTER_STAT(MP_STAT_MULTI_PROPERTY_VALUE_3,g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[1])
			PRINTLN("AM_MP_PROPERTY_EXT: Player just purchased property for slot 3")
		ELIF iReplacePropertySlot = PROPERTY_OWNED_SLOT_APT_5
			SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_MULTI_PROPERTY_4, iPropertyID)
			IF NOT SHOULD_PROPERTY_BE_FREE_FOR_PLAYER(iPropertyID)
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[2] = CEIL(TO_FLOAT(GET_VALUE_OF_PROPERTY(iPropertyID))*g_sMPTunables.fPropertyMultiplier)
			ELSE
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[2] = 0
			ENDIF
			SET_MP_INT_CHARACTER_STAT(MP_STAT_MULTI_PROPERTY_VALUE_4,g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[2])
			PRINTLN("AM_MP_PROPERTY_EXT: Player just purchased property for slot 4")
		ELIF iReplacePropertySlot = PROPERTY_OWNED_SLOT_OFFICE_0
			SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_PROP_OFFICE, iPropertyID)
			IF NOT SHOULD_PROPERTY_BE_FREE_FOR_PLAYER(iPropertyID)
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[PROPERTY_OWNED_SLOT_OFFICE_0-2] = CEIL(TO_FLOAT(GET_VALUE_OF_PROPERTY(iPropertyID))*g_sMPTunables.fPropertyMultiplier)
			ELSE
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[PROPERTY_OWNED_SLOT_OFFICE_0-2] = 0
			ENDIF
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PROP_OFFICE_VALUE,g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[PROPERTY_OWNED_SLOT_OFFICE_0-2])
			PRINTLN("AM_MP_PROPERTY_EXT: Player just purchased property for slot: ",PROPERTY_OWNED_SLOT_OFFICE_0)
		ELIF iReplacePropertySlot = PROPERTY_OWNED_SLOT_CLUBHOUSE
			SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_PROP_CLUBHOUSE, iPropertyID)
			IF NOT SHOULD_PROPERTY_BE_FREE_FOR_PLAYER(iPropertyID)
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[PROPERTY_OWNED_SLOT_CLUBHOUSE-2] = CEIL(TO_FLOAT(GET_VALUE_OF_PROPERTY(iPropertyID))*g_sMPTunables.fPropertyMultiplier)
			ELSE
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[PROPERTY_OWNED_SLOT_CLUBHOUSE-2] = 0
			ENDIF
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PROP_CLUBHOUSE_VALUE,g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[PROPERTY_OWNED_SLOT_CLUBHOUSE-2])
			PRINTLN("AM_MP_PROPERTY_EXT: Player just purchased property for slot: ",PROPERTY_OWNED_SLOT_CLUBHOUSE)
		ELIF iReplacePropertySlot = PROPERTY_OWNED_SLOT_APT_6
			SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_MULTI_PROPERTY_5, iPropertyID)
			IF NOT SHOULD_PROPERTY_BE_FREE_FOR_PLAYER(iPropertyID)
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[PROPERTY_OWNED_SLOT_APT_6-2] = CEIL(TO_FLOAT(GET_VALUE_OF_PROPERTY(iPropertyID))*g_sMPTunables.fPropertyMultiplier)
			ELSE
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[PROPERTY_OWNED_SLOT_APT_6-2] = 0
			ENDIF
			SET_MP_INT_CHARACTER_STAT(MP_STAT_MULTI_PROPERTY_VALUE_5,g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[PROPERTY_OWNED_SLOT_APT_6-2])
			PRINTLN("AM_MP_PROPERTY_EXT: Player just purchased property for slot: ",PROPERTY_OWNED_SLOT_APT_6)
		ELIF iReplacePropertySlot = PROPERTY_OWNED_SLOT_OFFICE_GARAGE_LVL1
			SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_PROP_OFFICE_GAR1, PROPERTY_OFFICE_1_GARAGE_LVL1)
			IF NOT SHOULD_PROPERTY_BE_FREE_FOR_PLAYER(PROPERTY_OFFICE_1_GARAGE_LVL1)
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[PROPERTY_OWNED_SLOT_OFFICE_GARAGE_LVL1-2] = CEIL(TO_FLOAT(GET_VALUE_OF_PROPERTY(PROPERTY_OFFICE_1_GARAGE_LVL1))*g_sMPTunables.fPropertyMultiplier)
			ELSE
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[PROPERTY_OWNED_SLOT_OFFICE_GARAGE_LVL1-2] = 0
			ENDIF
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PROP_OFFICE_VALUE,g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[PROPERTY_OWNED_SLOT_OFFICE_GARAGE_LVL1-2])
			PRINTLN("AM_MP_PROPERTY_EXT: Player just purchased property for slot: ",PROPERTY_OWNED_SLOT_OFFICE_GARAGE_LVL1)
		ELIF iReplacePropertySlot = PROPERTY_OWNED_SLOT_OFFICE_GARAGE_LVL2
			SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_PROP_OFFICE_GAR1, PROPERTY_OFFICE_1_GARAGE_LVL2)
			IF NOT SHOULD_PROPERTY_BE_FREE_FOR_PLAYER(PROPERTY_OFFICE_1_GARAGE_LVL2)
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[PROPERTY_OWNED_SLOT_OFFICE_GARAGE_LVL2-2] = CEIL(TO_FLOAT(GET_VALUE_OF_PROPERTY(PROPERTY_OFFICE_1_GARAGE_LVL2))*g_sMPTunables.fPropertyMultiplier)
			ELSE
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[PROPERTY_OWNED_SLOT_OFFICE_GARAGE_LVL2-2] = 0
			ENDIF
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PROP_OFFICE_VALUE,g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[PROPERTY_OWNED_SLOT_OFFICE_GARAGE_LVL2-2])
			PRINTLN("AM_MP_PROPERTY_EXT: Player just purchased property for slot: ",PROPERTY_OWNED_SLOT_OFFICE_GARAGE_LVL2)
		ELIF iReplacePropertySlot = PROPERTY_OWNED_SLOT_OFFICE_GARAGE_LVL3
			SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_PROP_OFFICE_GAR1, PROPERTY_OFFICE_1_GARAGE_LVL3)
			IF NOT SHOULD_PROPERTY_BE_FREE_FOR_PLAYER(PROPERTY_OFFICE_1_GARAGE_LVL3)
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[PROPERTY_OWNED_SLOT_OFFICE_GARAGE_LVL3-2] = CEIL(TO_FLOAT(GET_VALUE_OF_PROPERTY(PROPERTY_OFFICE_1_GARAGE_LVL3))*g_sMPTunables.fPropertyMultiplier)
			ELSE
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[PROPERTY_OWNED_SLOT_OFFICE_GARAGE_LVL3-2] = 0
			ENDIF
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PROP_OFFICE_VALUE,g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[PROPERTY_OWNED_SLOT_OFFICE_GARAGE_LVL3-2])
			PRINTLN("AM_MP_PROPERTY_EXT: Player just purchased property for slot: ",PROPERTY_OWNED_SLOT_OFFICE_GARAGE_LVL3)
		ELIF iReplacePropertySlot = PROPERTY_OWNED_SLOT_APT_7
			SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_MULTI_PROPERTY_6, iPropertyID)
			IF NOT SHOULD_PROPERTY_BE_FREE_FOR_PLAYER(iPropertyID)
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[PROPERTY_OWNED_SLOT_APT_7-2] = CEIL(TO_FLOAT(GET_VALUE_OF_PROPERTY(iPropertyID))*g_sMPTunables.fPropertyMultiplier)
			ELSE
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[PROPERTY_OWNED_SLOT_APT_7-2] = 0
			ENDIF
			SET_MP_INT_CHARACTER_STAT(MP_STAT_MULTI_PROPERTY_VALUE_6,g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[PROPERTY_OWNED_SLOT_APT_7-2])
			PRINTLN("AM_MP_PROPERTY_EXT: Player just purchased property for slot: ",PROPERTY_OWNED_SLOT_APT_7)
		ELIF iReplacePropertySlot = PROPERTY_OWNED_SLOT_APT_8
			SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_MULTI_PROPERTY_7, iPropertyID)
			IF NOT SHOULD_PROPERTY_BE_FREE_FOR_PLAYER(iPropertyID)
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[PROPERTY_OWNED_SLOT_APT_8-2] = CEIL(TO_FLOAT(GET_VALUE_OF_PROPERTY(iPropertyID))*g_sMPTunables.fPropertyMultiplier)
			ELSE
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[PROPERTY_OWNED_SLOT_APT_8-2] = 0
			ENDIF
			SET_MP_INT_CHARACTER_STAT(MP_STAT_MULTI_PROPERTY_VALUE_7,g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[PROPERTY_OWNED_SLOT_APT_8-2])
			PRINTLN("AM_MP_PROPERTY_EXT: Player just purchased property for slot: ",PROPERTY_OWNED_SLOT_APT_8)
		ELIF iReplacePropertySlot = PROPERTY_OWNED_SLOT_APT_9 //CDM PROPERTY STAT UPDATE //OLD PROPERTIES
			SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_MULTI_PROPERTY_8, iPropertyID)
			IF NOT SHOULD_PROPERTY_BE_FREE_FOR_PLAYER(iPropertyID)
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[PROPERTY_OWNED_SLOT_APT_9-2] = CEIL(TO_FLOAT(GET_VALUE_OF_PROPERTY(iPropertyID))*g_sMPTunables.fPropertyMultiplier)
			ELSE
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[PROPERTY_OWNED_SLOT_APT_9-2] = 0
			ENDIF
			SET_MP_INT_CHARACTER_STAT(MP_STAT_MULTI_PROPERTY_VALUE_8,g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[PROPERTY_OWNED_SLOT_APT_9-2])
			PRINTLN("AM_MP_PROPERTY_EXT: Player just purchased property for slot: ",PROPERTY_OWNED_SLOT_APT_9)
		ELIF iReplacePropertySlot = PROPERTY_OWNED_SLOT_APT_10
			SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_MULTI_PROPERTY_9, iPropertyID)
			IF NOT SHOULD_PROPERTY_BE_FREE_FOR_PLAYER(iPropertyID)
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[PROPERTY_OWNED_SLOT_APT_10-2] = CEIL(TO_FLOAT(GET_VALUE_OF_PROPERTY(iPropertyID))*g_sMPTunables.fPropertyMultiplier)
			ELSE
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[PROPERTY_OWNED_SLOT_APT_10-2] = 0
			ENDIF
			SET_MP_INT_CHARACTER_STAT(MP_STAT_MULTI_PROPERTY_VALUE_9,g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[PROPERTY_OWNED_SLOT_APT_10-2])
			PRINTLN("AM_MP_PROPERTY_EXT: Player just purchased property for slot: ",PROPERTY_OWNED_SLOT_APT_10)
		ENDIF
		
		INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_NO_PROPERTIES_UNLOCK)
		CLEAR_BIT(iLocalBS,LOCAL_BS_SETUP_MENU)

		SWITCH GET_PROPERTY_SIZE_TYPE(iPropertyID)
			CASE PROP_SIZE_TYPE_SMALL_APT
			CASE PROP_SIZE_TYPE_MED_APT	
			CASE PROP_SIZE_TYPE_LARGE_APT
				SET_MP_BOOL_CHARACTER_STAT(MP_STAT_CL_BUY_APPARTMENT, TRUE)
				SET_MP_BOOL_CHARACTER_STAT(MP_STAT_CL_BUY_GARAGE , TRUE)
			FALLTHRU// because all houses have garages
			CASE PROP_SIZE_TYPE_2_GAR		
			CASE PROP_SIZE_TYPE_6_GAR	
			CASE PROP_SIZE_TYPE_10_GAR	
				SET_MP_BOOL_CHARACTER_STAT(MP_STAT_CL_BUY_GARAGE , TRUE)
			BREAK
		ENDSWITCH

		#IF FEATURE_GEN9_EXCLUSIVE
		IF IS_PROPERTY_HIGH_END_APARTMENT(iPropertyID)
			NETWORK_POST_UDS_ACTIVITY_END("FleecaBuyHighEndApartment", UDS_ACTIVITY_END_REASON_COMPLETED)
		ENDIF
		#ENDIF

		CHECK_AMERICAN_DREAM_ACHIEVEMENT(TRUE)
		//REQUEST_SAVE(STAT_SAVETYPE_SCRIPT_MP_GLOBALS)
		//REQUEST_SAVE(STAT_SAVETYPE_END_SHOPPING)
		REQUEST_SAVE(SSR_REASON_PURCHASE_PROPERTY, STAT_SAVETYPE_IMMEDIATE_FLUSH)

		RESET_TO_ENTRANCE_TO_PROPERTY_STAGE()
		
		IF IS_PROPERTY_CUSTOM_APARTMENT(iPropertyID)
		AND NOT IS_BIT_SET(iLocalBS,LOCAL_BS_PURCHASED_CUSTOM_APT)
			SET_BIT(iLocalBS,LOCAL_BS_PURCHASED_CUSTOM_APT)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "Purchased a custom apartement")
		ENDIF
		
		IF SHOULD_PROPERTY_BE_FREE_FOR_PLAYER(iPropertyID)
			IF HAS_MP_CRIMINAL_ENTERPRISE_PREMIUM_OR_STARTER_ACCESS()
				SWITCH iPropertyID
					CASE PROPERTY_OFFICE_2_BASE			SET_PLAYER_PURCHASED_CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT(CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_PROPERTY_MAZE_BANK_WEST_EXECUTIVE_OFFICE) BREAK
					CASE PROPERTY_CLUBHOUSE_4_BASE_A	SET_PLAYER_PURCHASED_CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT(CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_PROPERTY_GREAT_CHAPARRAL_BIKER_CLUBHOUSE) BREAK
					CASE PROPERTY_LOW_APT_2				SET_PLAYER_PURCHASED_CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT(CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_PROPERTY_1561_SAN_VITAS_STREET_APARTMENT) BREAK
					CASE PROPERTY_GARAGE_NEW_19			SET_PLAYER_PURCHASED_CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT(CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_PROPERTY_1337_EXCEPTIONALISTS_WAY_10_CAR_GARAGE) BREAK
					
					DEFAULT
						CASSERTLN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: unknown free Criminal Enterprise Starter Pack property ", iPropertyID)
					BREAK
				ENDSWITCH
			ENDIF
		ENDIF
		
		NET_PRINT("AM_MP_PROPERTY_EXT: player just bought property ") NET_NL()
		
		//If this is a custom apartment set the selected interior variant
		IF IS_PROPERTY_CUSTOMISABLE(iPropertyID)		
			IF iReplacePropertySlot = 0
				SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_APPARTMENT_VAR_0, (iCurrentInteriorVariationTooPurchase + 1))
				PRINTLN("PURCHASE_PROPERTY: set heavily accessed stat for Custom APT (Slot 0) to ",(iCurrentInteriorVariationTooPurchase + 1))
			ELIF iReplacePropertySlot = 1
				SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_APPARTMENT_VAR_1, (iCurrentInteriorVariationTooPurchase + 1))
				PRINTLN("PURCHASE_PROPERTY: set heavily accessed stat for Custom APT (Slot 1) to ",(iCurrentInteriorVariationTooPurchase + 1))
			ELIF iReplacePropertySlot = 2
				SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_APPARTMENT_VAR_2, (iCurrentInteriorVariationTooPurchase + 1))
				PRINTLN("PURCHASE_PROPERTY: set heavily accessed stat for Custom APT (Slot 2) to ",(iCurrentInteriorVariationTooPurchase + 1))
			ELIF iReplacePropertySlot = 3
				SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_APPARTMENT_VAR_3, (iCurrentInteriorVariationTooPurchase + 1))
				PRINTLN("PURCHASE_PROPERTY: set heavily accessed stat for Custom APT (Slot 3) to ",(iCurrentInteriorVariationTooPurchase + 1))
			ELIF iReplacePropertySlot = 4
				SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_APPARTMENT_VAR_4, (iCurrentInteriorVariationTooPurchase + 1))
				PRINTLN("PURCHASE_PROPERTY: set heavily accessed stat for Custom APT (Slot 4) to ",(iCurrentInteriorVariationTooPurchase + 1))
			ELIF iReplacePropertySlot = PROPERTY_OWNED_SLOT_OFFICE_0
				SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_PROP_OFFICE_VAR, (iCurrentInteriorVariationTooPurchase + 1))
				PRINTLN("PURCHASE_PROPERTY: set heavily accessed stat for Custom Office (Slot 5) to ",(iCurrentInteriorVariationTooPurchase + 1))
			ELIF iReplacePropertySlot = PROPERTY_OWNED_SLOT_CLUBHOUSE
				SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_PROP_CLUBHOUSE_VAR, (iCurrentInteriorVariationTooPurchase + 1))
				PRINTLN("PURCHASE_PROPERTY: set heavily accessed stat for CLUBHOUSE (Slot ",PROPERTY_OWNED_SLOT_CLUBHOUSE,") to ",(iCurrentInteriorVariationTooPurchase + 1))
			ELIF iReplacePropertySlot = PROPERTY_OWNED_SLOT_APT_6
				SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_APPARTMENT_VAR_5, (iCurrentInteriorVariationTooPurchase + 1))
				PRINTLN("PURCHASE_PROPERTY: set heavily accessed stat for  Custom APT (Slot ",PROPERTY_OWNED_SLOT_APT_6,") to ",(iCurrentInteriorVariationTooPurchase + 1))
			#IF FEATURE_SUMMER_2020
			ELIF iReplacePropertySlot = PROPERTY_OWNED_SLOT_APT_7
				SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_APPARTMENT_VAR_6, (iCurrentInteriorVariationTooPurchase + 1))
				PRINTLN("PURCHASE_PROPERTY: set heavily accessed stat for  Custom APT (Slot ",PROPERTY_OWNED_SLOT_APT_7,") to ",(iCurrentInteriorVariationTooPurchase + 1))
			ELIF iReplacePropertySlot = PROPERTY_OWNED_SLOT_APT_8 //max_owned_properties
				SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_APPARTMENT_VAR_7, (iCurrentInteriorVariationTooPurchase + 1))
				PRINTLN("PURCHASE_PROPERTY: set heavily accessed stat for  Custom APT (Slot ",PROPERTY_OWNED_SLOT_APT_8,") to ",(iCurrentInteriorVariationTooPurchase + 1))
			#ENDIF
			ELIF iReplacePropertySlot = PROPERTY_OWNED_SLOT_APT_9
				SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_APPARTMENT_VAR_8, (iCurrentInteriorVariationTooPurchase + 1))
				PRINTLN("PURCHASE_PROPERTY: set heavily accessed stat for  Custom APT (Slot ",PROPERTY_OWNED_SLOT_APT_9,") to ",(iCurrentInteriorVariationTooPurchase + 1))
			ELIF iReplacePropertySlot = PROPERTY_OWNED_SLOT_APT_10 //max_owned_properties //OLD PROPERTIES
				SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_APPARTMENT_VAR_9, (iCurrentInteriorVariationTooPurchase + 1))
				PRINTLN("PURCHASE_PROPERTY: set heavily accessed stat for  Custom APT (Slot ",PROPERTY_OWNED_SLOT_APT_10,") to ",(iCurrentInteriorVariationTooPurchase + 1))
//			#IF FEATURE_IMPORT_EXPORT	
//			ELIF iReplacePropertySlot = PROPERTY_OWNED_SLOT_IE_WAREHOUSE
//				SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_PROP_VEH_WAREHOUSE_VAR, (iCurrentInteriorVariationTooPurchase + 1))
//				PRINTLN("PURCHASE_PROPERTY: set heavily accessed stat for VEH_WAREHOUSE (Slot ",PROPERTY_OWNED_SLOT_IE_WAREHOUSE,")) to ",(iCurrentInteriorVariationTooPurchase + 1))
//			#ENDIF
			ENDIF
		ENDIF
		
		//Reset the variable used to control the interior variation submenu
		iCustomApartmentTooPurchase 			= -1
		iCurrentInteriorVariationTooPurchase	= -1
		bInteriorToPurchaseSelected 			= FALSE
		
		//Update network spend telemetry
		REQUEST_NET_SPEND_COMMON_DATA_UPDATE()
		
		RETURN TRUE
	ENDIF
	//Reset the variable used to control the interior variation submenu
	iCustomApartmentTooPurchase 			= -1
	iCurrentInteriorVariationTooPurchase	= -1
	bInteriorToPurchaseSelected 			= FALSE
	RETURN FALSE
ENDFUNC

FUNC BOOL RUN_PROPERTY_PURCHASE(INT iSelectedProperty, INT iMaxTradeIn) 
	IF NOT IS_BIT_SET(tempPropertyExtMenu.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
	
		INT iCost = CEIL((GET_VALUE_OF_PROPERTY(iSelectedProperty)*g_sMPTunables.fPropertyMultiplier)-iMaxTradeIn)
	
		IF (iCost <= 0)
		OR (iCost > 0 AND NETWORK_CAN_SPEND_MONEY(iCost,FALSE,TRUE,FALSE))
		OR bProcessingBasket
			IF g_sMPTUNABLES.bdisable_purchase_of_third_property
			AND tempPropertyExtMenu.iCurVertSel >= 2
				SETUP_PROPERTY_MENU_DESCRIPTION_BIT(MP_PROP_MENU_DES_3RD_DISABLE)
				CLEAR_BIT(iLocalBS,LOCAL_BS_SETUP_MENU)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "Disabling purchase of 3rd property - 1")
			ELSE
				REMOVE_MENU_HELP_KEYS()
				ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_ACCEPT, "BB_YES")
				ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_CANCEL, "BB_NO")
				SET_BIT(tempPropertyExtMenu.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
				SET_BIT(tempPropertyExtMenu.iButtonBS,PROP_BUTTON_BS_PRESSED_ACCEPT)
			ENDIF
		ELSE
			IF NOT IS_BIT_SET(iBoolsBitSet, iBS_STORE_ACTIVE)
				STORE_LAST_VIEWED_SHOP_ITEM_FOR_COMMERCE_STORE(GET_PROPERTY_KEY_FOR_CATALOGUE_NONPC(iSelectedProperty), CEIL(mpProperties[iSelectedProperty].iValue*g_sMPTunables.fPropertyMultiplier),0)
				LAUNCH_STORE_CASH_ALERT()
				SET_BIT(iBoolsBitSet, iBS_STORE_ACTIVE)
				//NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, TRUE, TRUE, TRUE, TRUE, TRUE, FALSE, TRUE, FALSE)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "  -->  AM_MP_PROPERTY_EXT - LAUNCH_STORE_CASH_ALERT - CALLED- A")
			ENDIF
			SETUP_PROPERTY_MENU_DESCRIPTION_BIT(MP_PROP_MENU_DES_NO_MONEY)
			CLEAR_BIT(iLocalBS,LOCAL_BS_SETUP_MENU)
		ENDIF
	ELSE
		IF PURCHASE_PROPERTY(iSelectedProperty,iReplaceSpecificPropertySlot)

			NET_PRINT("RUN_PROPERTY_PURCHASE player just bought property") NET_NL()
			RETURN TRUE
			//SET_BIT(iLocalBS,LOCAL_BS_JUST_BOUGHT_PROPERTY)
			//iEntrancePlayerIsUsing = 0
			//SET_LOCAL_STAGE(STAGE_INTRO_IF_APPLICABLE)
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC HANDLE_PROPERTY_OPTIONS_MENU()
	INT iMaxTradeIn
	HIDE_HELP_TEXT_THIS_FRAME()
	DISABLE_DPADDOWN_THIS_FRAME()
	DISABLE_SELECTOR_THIS_FRAME()
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_FRONTEND_CANCEL)
	REINIT_NET_TIMER(cinematicDelay,TRUE)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_FRONTEND_LEFT)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_FRONTEND_RIGHT)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_FRONTEND_UP)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_FRONTEND_DOWN)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_CURSOR_SCROLL_UP)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_CURSOR_SCROLL_DOWN)
	
	BOOL bCursorAccept = FALSE
	BOOL bUsingCursor = FALSE
	// Handle mouse clicks on the buzzer menu
	IF iCurrentMenuType = MENU_TYPE_BUZZER
	AND mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iType != ENTRANCE_TYPE_GARAGE
		HANDLE_CURSOR_HIGHLIGHTED_MENU_ITEMS()
		IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
			SET_MOUSE_CURSOR_THIS_FRAME()
			IF IS_MENU_CURSOR_ACCEPT_PRESSED()
				IF g_iMenuCursorItem != tempPropertyExtMenu.iCurVertSel
					tempPropertyExtMenu.iCurVertSel = g_iMenuCursorItem
				ELSE
					bCursorAccept = TRUE
				ENDIF
			ENDIF
			bUsingCursor = TRUE
		ENDIF
	ENDIF
	
//	IF IS_NET_PLAYER_OK(PLAYER_ID())
//		SET_PED_RESET_FLAG(PLAYER_PED_ID(),PED_RESET_FLAGS)
//	ENDIF
	//INT iTradeIn
	INT iTempSelectionVert
	INT iSelectedProperty
	INT i
	INT iCost
	INT iReplacementPropertySlot
	
	BOOL bInputUsedThisFrame = FALSE
	
	
	
	BOOL bBypassCustomiseOptions = FALSE
	IF iCurrentMenuType = MENU_TYPE_PURCHASE
	AND IS_PROPERTY_OFFICE(tempPropertyExtMenu.iSelectableProperties[tempPropertyExtMenu.iCurVertSel])
		iCurrentInteriorVariationTooPurchase = PROPERTY_VARIATION_4 -1 //adds one later
		bBypassCustomiseOptions = TRUE
	ENDIF
	
	IF IS_COMMERCE_STORE_OPEN()
	OR IS_PAUSE_MENU_ACTIVE()
		EXIT
	ENDIF
	
	IF IS_BIT_SET(iBoolsBitSet, iBS_STORE_ACTIVE)
		IF NOT g_sShopSettings.bProcessStoreAlert
			CLEAR_BIT(iBoolsBitSet, iBS_LAUNCH_STORE)
			CLEAR_BIT(iBoolsBitSet, iBS_STORE_ACTIVE)
			//NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			CLEAR_BIT(tempPropertyExtMenu.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "  -->  CONTACT REQUESTS - LAUNCH_STORE_CASH_ALERT - CLEARED")
		ENDIF
		EXIT
	ENDIF
	
	IF (iCurrentMenuType = MENU_TYPE_PURCHASE OR (iCurrentMenuType = MENU_TYPE_REPLACE_PROPERTY AND iSelectedPropertyToBuy >0))
	AND IS_BIT_SET(tempPropertyExtMenu.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
	AND NOT IS_BIT_SET(iBoolsBitSet, iBS_STORE_ACTIVE)
		IF NOT IS_BIT_SET(iLocalBS,LOCAL_BS_PURCHASE_MENU_REQUEST_SF)
			//warningscreen = REQUEST_SCALEFORM_MOVIE("popup_warning")
			SET_BIT(iLocalBS,LOCAL_BS_PURCHASE_MENU_REQUEST_SF)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Requesting warning scaleform movie.")
		ENDIF
	ELSE
//		IF warningscreen != NULL
//			SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(warningscreen)
//			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: cleaning up scaleform movie.")
			CLEAR_BIT(iLocalBS,LOCAL_BS_PURCHASE_MENU_REQUEST_SF)
		//ENDIF
	ENDIF
	
	//Cleanup if another Custom Menu is displaying before ours
	IF NOT IS_BIT_SET(iLocalBS,LOCAL_BS_STOP_CUSTOM_MENU_CHECK)
		IF IS_CUSTOM_MENU_ON_SCREEN()
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: CUSTOM MENU ALREADY ON SCREEN EXIT HANDLE_PROPERTY_OPTIONS_MENU")
			RESET_TO_ENTRANCE_TO_PROPERTY_STAGE()
			EXIT
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_AcceptedInvite)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT:  biITA_AcceptedInvite aborting menu")
		RESET_TO_ENTRANCE_TO_PROPERTY_STAGE()
		EXIT
	ENDIF
	
	IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_USING_PROPERTY)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: PROPERTY_BROADCAST_BS_PLAYER_USING_PROPERTY true aborting")
		RESET_TO_ENTRANCE_TO_PROPERTY_STAGE()
		EXIT
	ENDIF
	CDEBUG1LN(DEBUG_SAFEHOUSE, "bProcessingBasket = ",bProcessingBasket)
	IF LOAD_MENU_ASSETS(DEFAULT, DEFAULT, TRUE)
	
		
		//Stop Doing Cutsom Menu Check
		IF NOT IS_BIT_SET(iLocalBS,LOCAL_BS_STOP_CUSTOM_MENU_CHECK)
			SET_BIT(iLocalBS,LOCAL_BS_STOP_CUSTOM_MENU_CHECK)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: LOCAL_BS_STOP_CUSTOM_MENU_CHECK set")
		ENDIF
		IF NOT IS_BIT_SET(iLocalBS,LOCAL_BS_SETUP_MENU)
			CLEAR_HELP()
			SETUP_PROPERTY_OPTIONS_MENU(iCurrentMenuType)
			SET_BIT(iLocalBS,LOCAL_BS_SETUP_MENU)
			HANDLE_MENU_DESCRIPTION(iCurrentMenuType)
			//IF iCurrentMenuType = MENU_TYPE_PURCHASE
			//AND IS_BIT_SET(tempPropertyExtMenu.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
				//nothing
			//ELSE
				DRAW_MENU()
			//ENDIF
		ELSE
			IF NOT IS_PAUSE_MENU_ACTIVE()
			AND NOT IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
				IF NOT IS_BIT_SET(tempPropertyExtMenu.iButtonBS,PROP_BUTTON_BS_PRESSED_ACCEPT)
					IF (IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT) OR bProcessingBasket OR bCursorAccept = TRUE)
					AND CAN_PLAYER_USE_MENU_INPUT()
					AND NOT bInputUsedThisFrame
						bInputUsedThisFrame = TRUE
						IF globalPropertyEntryData.bLeftOrRejectedBuzzer
						AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_MP_PROPERTY_INT")) > 0
							CDEBUG1LN(DEBUG_SAFEHOUSE, "Not allowing accept while previous script is terminating")
							HANDLE_MENU_DESCRIPTION(iCurrentMenuType)
							DRAW_MENU()
							EXIT 
						ENDIF
						SWITCH iCurrentMenuType
							CASE MENU_TYPE_SELECT_CUSTOM_INTERIOR
								PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FREEMODE_SOUNDSET")
								
								IF NOT IS_BIT_SET(tempPropertyExtMenu.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
									iCurrentInteriorVariationTooPurchase = ciPI_TYPE_M2_APT_VAR_AVAILABLE_OPTIONS[tempPropertyExtMenu.iCurVertSel]
								ENDIF
								IF iCurrentInteriorVariationTooPurchase < 0
								OR iCurrentInteriorVariationTooPurchase > 3
									CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_PROPERTY_OPTIONS_MENU: Interior variation not valid")
								ENDIF
								
								bInteriorToPurchaseSelected 	= TRUE								
								iMaxTradeIn 					= 0
								
								REPEAT MAX_OWNED_PROPERTIES i
									IF iReplaceSpecificPropertySlot = -1
									OR iReplaceSpecificPropertySlot = i
										IF iMaxTradeIn < GET_VALUE_OF_CURRENTLY_OWNED_PROPERTY(i)
											iMaxTradeIn = GET_VALUE_OF_CURRENTLY_OWNED_PROPERTY(i)
										ENDIF
									ENDIF
								ENDREPEAT
								IF iReplaceSpecificPropertySlot != -1
									iMaxTradeIn = GET_VALUE_OF_CURRENTLY_OWNED_PROPERTY(iReplaceSpecificPropertySlot)
								ENDIF

								IF GET_OWNED_PROPERTY(0) > 0
								AND (iReplaceSpecificPropertySlot = -1 OR GET_OWNED_PROPERTY(iReplaceSpecificPropertySlot) > 0)
									iCurrentMenuType = MENU_TYPE_REPLACE_PROPERTY									
									SETUP_PROPERTY_OPTIONS_MENU(MENU_TYPE_REPLACE_PROPERTY)
								ELSE
									IF RUN_PROPERTY_PURCHASE(iCustomApartmentTooPurchase, iMaxTradeIn)
										EXIT
									ENDIF
								ENDIF
							BREAK
							CASE MENU_TYPE_PURCHASE
								IF NOT bProcessingBasket
									PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FREEMODE_SOUNDSET")
								ENDIF
//								IF DOES_PLAYER_OWN_PROPERTY(mpProperties[iCurrentPropertyID].iIndex)
//									NET_PRINT("HANDLE_PROPERTY_OPTIONS_MENU player owns property and confirmed") NET_NL()
//									SET_LOCAL_STAGE(STAGE_INTRO_IF_APPLICABLE)
//								ELSE
								iSelectedProperty = tempPropertyExtMenu.iSelectableProperties[tempPropertyExtMenu.iCurVertSel]
								IF DOES_LOCAL_PLAYER_OWN_PROPERTY(iSelectedProperty)
									SETUP_PROPERTY_MENU_DESCRIPTION_BIT(MP_PROP_MENU_DES_OWNS_PROPERTY)
								ELIF IS_PROPERTY_OFFICE(iSelectedProperty)
								AND (NOT DOES_PLAYER_OWN_PRIVATE_YACHT(PLAYER_ID()) OR NOT GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID()))
									IF NOT DOES_PLAYER_OWN_PRIVATE_YACHT(PLAYER_ID())
										SETUP_PROPERTY_MENU_DESCRIPTION_BIT(MP_PROP_MENU_DES_NO_YACHT)
									ELIF NOT GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID())
										SETUP_PROPERTY_MENU_DESCRIPTION_BIT(MP_PROP_MENU_DES_NOT_BOSS)
									ENDIF
								ELSE
									iReplaceSpecificPropertySlot = -1
									iMaxTradeIn = 0
									REPEAT MAX_OWNED_PROPERTIES i
										IF iMaxTradeIn < GET_VALUE_OF_CURRENTLY_OWNED_PROPERTY(i)
											iMaxTradeIn = GET_VALUE_OF_CURRENTLY_OWNED_PROPERTY(i)
										ENDIF
									ENDREPEAT
									//#IF FEATURE_EXECUTIVE //TEMP TEMP TEMP
									IF IS_PROPERTY_RESTRICTED_SLOT_ONLY(iSelectedProperty)
										iMaxTradeIn = GET_VALUE_OF_CURRENTLY_OWNED_PROPERTY(GET_PROPERTY_RESTRICTED_SLOT(iSelectedProperty))
									ENDIF
									
									//IF IS_PROPERTY_OFFICE(iSelectedProperty) AND bBypassCustomiseOptions
										IF IS_PROPERTY_RESTRICTED_SLOT_ONLY(iSelectedProperty)
											iReplaceSpecificPropertySlot = GET_PROPERTY_RESTRICTED_SLOT(iSelectedProperty)
											PRINTLN("AM_MP_PROPERTY_INT: 3 iReplaceSpecificPropertySlot = ",iReplaceSpecificPropertySlot)
										ENDIF
									//ENDIF
									//#ENDIF
									
									INT iTotalCost
									iTotalCost = CEIL((GET_VALUE_OF_PROPERTY(iSelectedProperty)*g_sMPTunables.fPropertyMultiplier)-iMaxTradeIn)
									//#IF FEATURE_EXECUTIVE
//									IF IS_PROPERTY_RESTRICTED_SLOT_ONLY(iSelectedProperty)
//										iReplaceSpecificPropertySlot = GET_PROPERTY_RESTRICTED_SLOT(iSelectedProperty)
//										PRINTLN("AM_MP_PROPERTY_INT: 2 iReplaceSpecificPropertySlot = ",iReplaceSpecificPropertySlot)
//									ENDIF
									//#ENDIF
									
									IF NOT bInteriorToPurchaseSelected
									AND (NOT IS_PROPERTY_OFFICE(iSelectedProperty) OR NOT bBypassCustomiseOptions)
									AND IS_PROPERTY_CUSTOMISABLE(iSelectedProperty)
										IF (iTotalCost <= 0)
										OR (iTotalCost > 0 AND NETWORK_CAN_SPEND_MONEY(iTotalCost,FALSE,TRUE,FALSE))
											tempPropertyExtMenu.iCurVertSel = 0
											iCurrentMenuType = MENU_TYPE_SELECT_CUSTOM_INTERIOR
											iCustomApartmentTooPurchase = iSelectedProperty
											IF IS_PROPERTY_RESTRICTED_SLOT_ONLY(iSelectedProperty)
												iReplaceSpecificPropertySlot = GET_PROPERTY_RESTRICTED_SLOT(iSelectedProperty)
												PRINTLN("AM_MP_PROPERTY_INT: 1 iReplaceSpecificPropertySlot = ",iReplaceSpecificPropertySlot)
											ENDIF
											
											SETUP_PROPERTY_OPTIONS_MENU(MENU_TYPE_SELECT_CUSTOM_INTERIOR)
										ELSE
											//call this function to launch the store cash alert
											IF RUN_PROPERTY_PURCHASE(iSelectedProperty, iMaxTradeIn)
												EXIT
											ENDIF
										ENDIF
									ELIF GET_OWNED_PROPERTY(0) > 0
									AND (iReplaceSpecificPropertySlot = -1 OR GET_OWNED_PROPERTY(iReplaceSpecificPropertySlot) > 0)
										CLEAR_BIT(iLocalBS,LOCAL_BS_SETUP_MENU)	
										iSelectedPropertyToBuy = tempPropertyExtMenu.iSelectableProperties[tempPropertyExtMenu.iCurVertSel]
										tempPropertyExtMenu.iCurVertSel = 0
										iCurrentMenuType = MENU_TYPE_REPLACE_PROPERTY
									ELSE
										IF RUN_PROPERTY_PURCHASE(iSelectedProperty, iMaxTradeIn)
											EXIT
										ENDIF
									ENDIF
								ENDIF
							BREAK
							CASE MENU_TYPE_BUZZER
								IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_MP_PROPERTY_INT")) > 0
									CDEBUG1LN(DEBUG_SAFEHOUSE, "Not allowing accept while previous script is terminating-b")
									HANDLE_MENU_DESCRIPTION(iCurrentMenuType)
									DRAW_MENU()
									EXIT 
								ENDIF
								SET_BIT(tempPropertyExtMenu.iButtonBS,PROP_BUTTON_BS_PRESSED_ACCEPT)
								IF tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel] = PLAYER_ID()
								AND tempPropertyExtMenu.iPropertyID[tempPropertyExtMenu.iCurVertSel] > 0
									PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FREEMODE_SOUNDSET")
									IF ownedDetails.iNumProperties >= 2
									AND PROPERTY_NEAR_PLYS_OPT_AVAILABLE(GET_PROPERTY_ENTRANCE_APPROXIMATE_LOCATION(iCurrentPropertyID,iEntrancePlayerIsUsing),tempPropertyExtMenu.bFriendsNearby, tempPropertyExtMenu.bOrganisationNearby,  iCurrentPropertyID,iEntrancePlayerIsUsing)
									AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(),TRUE)	
										tempPropertyExtMenu.iExitSelection = tempPropertyExtMenu.iPropertyID[tempPropertyExtMenu.iCurVertSel]
										IF IS_PROPERTY_OFFICE_GARAGE(tempPropertyExtMenu.iExitSelection)
											IF tempPropertyExtMenu.iCurVertSel = tempPropertyExtMenu.iExitToOfficeFromGarageToMod
												PRINTLN("HANDLE_PROPERTY_OPTIONS_MENU: player will be entering directly to mod shop")
												tempPropertyExtMenu.iExitToOfficeFromGarageToMod = 99
											ENDIF
										ENDIF
										HANDLE_MENU_DESCRIPTION(iCurrentMenuType)
										DRAW_MENU()
										iCurrentMenuType = MENU_TYPE_PROPERTY_ENTRY_OPTIONS
										CLEAR_BIT(iLocalBS,LOCAL_BS_SETUP_MENU)
										tempPropertyExtMenu.iCurVertSel = 0
										PRINTLN("HANDLE_PROPERTY_OPTIONS_MENU: player moving to MENU_TYPE_PROPERTY_ENTRY_OPTIONS")
										EXIT 
									ELSE
										NET_PRINT("HANDLE_PROPERTY_OPTIONS_MENU player used buzzer menu and choose to enter their own house") NET_NL()
										iCurrentPropertyID = tempPropertyExtMenu.iPropertyID[tempPropertyExtMenu.iCurVertSel]
										CDEBUG1LN(DEBUG_SAFEHOUSE, "Player entered property # ", iCurrentPropertyID)
										GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iRequestEntryBS = 0
										CDEBUG1LN(DEBUG_SAFEHOUSE, "STAGE_SETUP_FOR_USING_PROPERTY: GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iRequestEntryBS = 0")
										DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
										IF tempPropertyExtMenu.iCurVertSel = tempPropertyExtMenu.iAllNearby
											SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_INVITE_NEARBY_PLAYERS_INSIDE)
											BROADCAST_PROPERTY_NEARBY_PLAYERS_INVITE_EVENT(PLAYERS_IN_RANGE_OF_POINT(GET_PLAYER_COORDS(PLAYER_ID()),NORMAL_APARTMENT_ENTRY_DISTANCE,TRUE,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE),iCurrentPropertyID)
										ELIF tempPropertyExtMenu.iCurVertSel = tempPropertyExtMenu.iAllFriendsCrew
											BROADCAST_PROPERTY_NEARBY_PLAYERS_INVITE_EVENT(PLAYERS_IN_RANGE_OF_POINT(GET_PLAYER_COORDS(PLAYER_ID()),NORMAL_APARTMENT_ENTRY_DISTANCE,FALSE,TRUE,TRUE,DEFAULT,DEFAULT,DEFAULT,TRUE),iCurrentPropertyID)
											SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_INVITE_CREW_FRIEND_INSIDE)
										ELIF tempPropertyExtMenu.iCurVertSel = tempPropertyExtMenu.iOrganisationNearby
											BROADCAST_PROPERTY_NEARBY_PLAYERS_INVITE_EVENT(PLAYERS_IN_RANGE_OF_POINT(GET_PLAYER_COORDS(PLAYER_ID()),NORMAL_APARTMENT_ENTRY_DISTANCE,FALSE,FALSE,FALSE,DEFAULT,DEFAULT,TRUE,TRUE),iCurrentPropertyID)
											SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_INVITE_ORGANISATION_INSIDE)
										ENDIF
										
										SET_LOCAL_STAGE(STAGE_SETUP_FOR_USING_PROPERTY)
										IF IS_PROPERTY_OFFICE_GARAGE(iCurrentPropertyID)
											IF tempPropertyExtMenu.iCurVertSel = tempPropertyExtMenu.iExitToOfficeFromGarageToMod
												PRINTLN("HANDLE_PROPERTY_OPTIONS_MENU: player entering directly to mod shop - 2 biIsPlayerGoingToModGarageTrue = TRUE")
												SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iIsPlayerGoingToModGarageBS, biIsPlayerGoingToModGarageTrue)
											ELSE
												PRINTLN("HANDLE_PROPERTY_OPTIONS_MENU: player entering office garage biIsPlayerGoingToModGarageTrue = FALSE")
												SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iIsPlayerGoingToModGarageBS, biIsPlayerGoingToModGarageFalse)
												CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iIsPlayerGoingToModGarageBS, biIsPlayerGoingToModGarageTrue)
											ENDIF
										ENDIF
									ENDIF
								ELIF tempPropertyExtMenu.iMaxVertSel <= 0
								 	SET_LOCAL_STAGE(STAGE_LEAVE_ENTRANCE_AREA)
									IF IS_BIT_SET(iLocalBS,LOCAL_BS_AWAITING_BUZZER_RESPONSE)
										//NETWORK_SET_TALKER_PROXIMITY(CHAT_PROXIMITY_FM)
										globalPropertyEntryData.ownerID = INVALID_PLAYER_INDEX()
										CDEBUG1LN(DEBUG_SAFEHOUSE, "GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iRequestEntryBS = 0")
										CDEBUG1LN(DEBUG_SAFEHOUSE, "globalPropertyEntryData.bLeftOrRejectedBuzzer 5 = TRUE")
										GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iRequestEntryBS = 0	
										NETWORK_CLEAR_VOICE_CHANNEL()
										RESET_NET_TIMER(buzzerTimer)
										globalPropertyEntryData.bLeftOrRejectedBuzzer = TRUE
									ENDIF
									CLEAR_BIT(iLocalBS,LOCAL_BS_AWAITING_BUZZER_RESPONSE)
									EXIT
								ELSE
									IF iBuzzerID = -1
										iBuzzerID = GET_SOUND_ID()
										IF IS_PROPERTY_OFFICE(iCurrentPropertyID)
											PLAY_SOUND_FRONTEND(iBuzzerID , "DOOR_Intercom_MASTER")
											CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: playing sound DOOR_Intercom_MASTER")	
										ELSE
											PLAY_SOUND_FRONTEND(iBuzzerID , "DOOR_BUZZ","MP_PLAYER_APARTMENT")
										ENDIF
									ELSE
										IF HAS_SOUND_FINISHED(iBuzzerID)
											iBuzzerID = GET_SOUND_ID()
											IF IS_PROPERTY_OFFICE(iCurrentPropertyID)
												PLAY_SOUND_FRONTEND(iBuzzerID , "DOOR_Intercom_MASTER")
												CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: playing sound DOOR_Intercom_MASTER")	
											ELSE
												PLAY_SOUND_FRONTEND(iBuzzerID , "DOOR_BUZZ","MP_PLAYER_APARTMENT")
											ENDIF
										ENDIF
									ENDIF
								
									CDEBUG1LN(DEBUG_SAFEHOUSE, "PLAY_SOUND_FRONTEND  called")
									IF NOT IS_BIT_SET(iLocalBS,LOCAL_BS_AWAITING_BUZZER_RESPONSE)
										IF tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel] != INVALID_PLAYER_INDEX()
										AND IS_NET_PLAYER_OK(tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel])
											IF NOT IS_BIT_SET(iRejectedEntryBS,NATIVE_TO_INT(tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel]) )
											AND (GlobalplayerBD_FM[NATIVE_TO_INT(tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel])].propertyDetails.iCurrentlyInsideProperty > 0) // added by Neil F. for 1822930

												iCurrentPropertyID = GlobalplayerBD_FM[NATIVE_TO_INT(tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel])].propertyDetails.iCurrentlyInsideProperty
												IF iCurrentPropertyID > 0
													globalPropertyEntryData.iVariation = -1
													PRINTLN("AMP_MP_PROPERTY_EXT: reset variation on entry to property - 5")
													SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iRequestEntryBS,NATIVE_TO_INT(tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel]))
													CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_PROPERTY_OPTIONS_MENU: apt GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iRequestEntryBS = ",GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iRequestEntryBS)
													SET_BIT(iLocalBS,LOCAL_BS_AWAITING_BUZZER_RESPONSE)

													INT instance  
											        
													iCurrentPropertyID = GlobalplayerBD_FM[NATIVE_TO_INT(tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel])].propertyDetails.iCurrentlyInsideProperty
													REPEAT MAX_OWNED_PROPERTIES i
														IF GlobalplayerBD_FM[NATIVE_TO_INT(tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel])].propertyDetails.iOwnedProperty[i] = iCurrentPropertyID
															instance = NATIVE_TO_INT(tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel])+(NUM_NETWORK_PLAYERS+1)*i
														ENDIF
													ENDREPEAT
													//NETWORK_SET_VOICE_CHANNEL(FM_VOICE_CHANNEL_PROPERTY0+instance)
													IF instance > 0
													
													ENDIF
													//CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_PROPERTY_OPTIONS_MENU: TURNING OFF PROXIMITY CHAT FOR BUZZER MENU!!")
													CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_PROPERTY_OPTIONS_MENU: set in voice session #",FM_VOICE_CHANNEL_PROPERTY0+instance) 
			                						globalPropertyEntryData.ownerID = tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel]
													globalPropertyEntryData.ownerHandle = GET_GAMER_HANDLE_PLAYER(tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel])
													globalPropertyEntryData.iPropertyEntered =  GlobalplayerBD_FM[NATIVE_TO_INT(tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel])].propertyDetails.iCurrentlyInsideProperty
													PRINTLN("26 globalPropertyEntryData.iPropertyEntered = ",globalPropertyEntryData.iPropertyEntered)
													IF IS_PLAYER_IN_MP_GARAGE(tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel],TRUE)
														REPEAT mpProperties[iCurrentPropertyID].iNumEntrances i
															IF mpProperties[iCurrentPropertyID].entrance[i].iType = ENTRANCE_TYPE_GARAGE
																globalPropertyEntryData.iEntrance = i
																i = mpProperties[iCurrentPropertyID].iNumEntrances
															ENDIF
														ENDREPEAT
													ELSE
														globalPropertyEntryData.iEntrance = 0
													ENDIF
													
													CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_PROPERTY_OPTIONS_MENU: setting owner as ",NATIVE_TO_INT(globalPropertyEntryData.ownerID))
													SET_BIT(iLocalBS2,LOCAL_BS2_bEnteringAnotherPlayerProperty)
													CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_PROPERTY_OPTIONS_MENU: player asking permission to enter property #",mpProperties[iCurrentPropertyID].iIndex)
													IF mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iType = ENTRANCE_TYPE_GARAGE
	//													DISABLE_MP_PROPERTY_INTERIORS(iCurrentPropertyID,FALSE)
	//													theInterior = GET_INTERIOR_AT_COORDS(mpProperties[iCurrentPropertyID].garage.vInPlayerLoc)
	//													CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Getting interior for position: ",mpProperties[iCurrentPropertyID].garage.vInPlayerLoc)
	//													PIN_INTERIOR_IN_MEMORY(theInterior)
	//													NEW_LOAD_SCENE_START_SPHERE(mpProperties[iCurrentPropertyID].garage.vInPlayerLoc,30)
														SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_BUZZER_ENTER_GARAGE)
													ELSE
	//													DISABLE_MP_PROPERTY_INTERIORS(iCurrentPropertyID,FALSE)
	//													theInterior = GET_INTERIOR_AT_COORDS(mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].vInPlayerLoc)
	//													CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Getting interior for position: ",mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].vInPlayerLoc)
	//													PIN_INTERIOR_IN_MEMORY(theInterior)
	//													NEW_LOAD_SCENE_START_SPHERE(mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].vInPlayerLoc,30)
														SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_BUZZER_ENTER_HOUSE)
													ENDIF
													CLEAR_TRANSITION_SESSION_DETAILS_ON_ENTRY_IF_SUITABLE(6)
													globalPropertyEntryData.iPropertyEntered = iCurrentPropertyID
													PRINTLN("25 globalPropertyEntryData.iPropertyEntered = ",globalPropertyEntryData.iPropertyEntered)
													//SET_BIT(ilocalBS,LOCAL_BS_BUZZER_TRIGGERED_LOAD_SCENE)
													//NETWORK_SET_TALKER_PROXIMITY(0.0)
													CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_PROPERTY_OPTIONS_MENU: TURNING OFF PROXIMITY CHAT FOR BUZZER MENU!!")
													CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_PROPERTY_OPTIONS_MENU: set in tutorial session #",instance, " to talk on buzzer") 

													REINIT_NET_TIMER(buzzerTimer)
													CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_PROPERTY_OPTIONS_MENU player has requested entry into property owned by ", GET_PLAYER_NAME(tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel]))
												ELSE
													SET_LOCAL_STAGE(STAGE_LEAVE_ENTRANCE_AREA)
													IF IS_BIT_SET(iLocalBS,LOCAL_BS_AWAITING_BUZZER_RESPONSE)
														//NETWORK_SET_TALKER_PROXIMITY(CHAT_PROXIMITY_FM)
														globalPropertyEntryData.ownerID = INVALID_PLAYER_INDEX()
														GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iRequestEntryBS = 0	
														CDEBUG1LN(DEBUG_SAFEHOUSE, "GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iRequestEntryBS = 0")
														CDEBUG1LN(DEBUG_SAFEHOUSE, "globalPropertyEntryData.bLeftOrRejectedBuzzer 4 = TRUE")
														NETWORK_CLEAR_VOICE_CHANNEL()
														RESET_NET_TIMER(buzzerTimer)
														globalPropertyEntryData.bLeftOrRejectedBuzzer = TRUE
													ENDIF
													CLEAR_BIT(iLocalBS,LOCAL_BS_AWAITING_BUZZER_RESPONSE)
													CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_PROPERTY_OPTIONS_MENU: Aborting iCurrentPropertyID = ",iCurrentPropertyID)
													EXIT
												ENDIF
											ELSE
												SETUP_PROPERTY_MENU_DESCRIPTION_BIT(MP_PROP_MENU_DES_REJECTED_ENTRY)
											ENDIF
										//ELSE	
										////	CLEAR_BIT(iLocalBS,LOCAL_BS_SETUP_MENU)
										///	CLEAR_BIT(iLocalBS,LOCAL_BS_AWAITING_BUZZER_RESPONSE)
										ENDIF
									ENDIF
								ENDIF
							BREAK
							CASE MENU_TYPE_REPLACE_PROPERTY
									IF iCustomApartmentTooPurchase != -1
										iSelectedPropertyToBuy = iCustomApartmentTooPurchase
									ENDIF
								IF NOT bProcessingBasket
									PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FREEMODE_SOUNDSET")
								ENDIF
								PRINTLN("tempPropertyExtMenu.iCurVertSel = ",tempPropertyExtMenu.iCurVertSel)
								IF iReplaceSpecificPropertySlot != -1
									iReplacementPropertySlot = iReplaceSpecificPropertySlot
								ELSE
									iReplacementPropertySlot = tempPropertyExtMenu.iSelectablePropertySlots[tempPropertyExtMenu.iCurVertSel]
								ENDIF
								
								iMaxTradeIn = GET_VALUE_OF_CURRENTLY_OWNED_PROPERTY(iReplacementPropertySlot)

								IF NOT IS_BIT_SET(tempPropertyExtMenu.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)

									iCost = CEIL((GET_VALUE_OF_PROPERTY(iSelectedPropertyToBuy)*g_sMPTunables.fPropertyMultiplier)-iMaxTradeIn)

									IF (iCost <= 0)
									OR (iCost > 0 AND NETWORK_CAN_SPEND_MONEY(iCost,FALSE,TRUE,FALSE))
									OR bProcessingBasket

										IF g_sMPTUNABLES.bdisable_purchase_of_third_property
										AND iReplacementPropertySlot >= 2
											SETUP_PROPERTY_MENU_DESCRIPTION_BIT(MP_PROP_MENU_DES_3RD_DISABLE)
											CLEAR_BIT(iLocalBS,LOCAL_BS_SETUP_MENU)
											CDEBUG1LN(DEBUG_SAFEHOUSE, "Disabling purchase of 3rd property-2")
										ELSE
											IF iReplacementPropertySlot <= 0												
												IF PURCHASE_PROPERTY(iSelectedPropertyToBuy,iReplacementPropertySlot)

													NET_PRINT("HANDLE_PROPERTY_OPTIONS_MENU player just bought property replace 1") NET_NL()
													EXIT
													//SET_BIT(iLocalBS,LOCAL_BS_JUST_BOUGHT_PROPERTY)
													//iEntrancePlayerIsUsing = 0
													//SET_LOCAL_STAGE(STAGE_INTRO_IF_APPLICABLE)
												ENDIF
											ELIF (Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID())
											OR Is_Player_Currently_On_MP_Heist(PLAYER_ID()))
												SETUP_PROPERTY_MENU_DESCRIPTION_BIT(MP_PROP_MENU_DES_ON_HEIST)
												SET_BIT(tempPropertyExtMenu.iButtonBS,PROP_BUTTON_BS_PRESSED_ACCEPT)
												CDEBUG1LN(DEBUG_SAFEHOUSE, "can't trade property on heist")
											ELSE
												SET_BIT(tempPropertyExtMenu.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
												SET_BIT(tempPropertyExtMenu.iButtonBS,PROP_BUTTON_BS_PRESSED_ACCEPT)
												IF GET_OWNED_PROPERTY(tempPropertyExtMenu.iCurVertSel) >0
													CLEAR_BIT(iLocalBS,LOCAL_BS_SETUP_MENU)
												ENDIF
												CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: alert for trade in.")
											ENDIF
										ENDIF
									ELSE
										CDEBUG1LN(DEBUG_SAFEHOUSE, "CDM: Player go to store!")
										IF NOT IS_BIT_SET(iBoolsBitSet, iBS_STORE_ACTIVE)
											STORE_LAST_VIEWED_SHOP_ITEM_FOR_COMMERCE_STORE(GET_PROPERTY_KEY_FOR_CATALOGUE_NONPC(iSelectedPropertyToBuy), CEIL(mpProperties[iSelectedPropertyToBuy].iValue*g_sMPTunables.fPropertyMultiplier),0)
											LAUNCH_STORE_CASH_ALERT()
											SET_BIT(iBoolsBitSet, iBS_STORE_ACTIVE)
											//NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, TRUE, TRUE, TRUE, TRUE, TRUE, FALSE, TRUE, FALSE)
											CDEBUG1LN(DEBUG_SAFEHOUSE, "  -->  AM_MP_PROPERTY_EXT - LAUNCH_STORE_CASH_ALERT - CALLED- B")
										ENDIF
										SETUP_PROPERTY_MENU_DESCRIPTION_BIT(MP_PROP_MENU_DES_NO_MONEY)
										CLEAR_BIT(iLocalBS,LOCAL_BS_SETUP_MENU)
									ENDIF
									
								ELSE
									
									IF PURCHASE_PROPERTY(iSelectedPropertyToBuy,iReplacementPropertySlot)

										NET_PRINT("HANDLE_PROPERTY_OPTIONS_MENU player just bought property replace 2") NET_NL()
										EXIT
										//SET_BIT(iLocalBS,LOCAL_BS_JUST_BOUGHT_PROPERTY)
										//iEntrancePlayerIsUsing = 0
										//SET_LOCAL_STAGE(STAGE_INTRO_IF_APPLICABLE)
									ENDIF
								ENDIF
							BREAK
							CASE MENU_TYPE_PROPERTY_ENTRY_OPTIONS
								IF tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel] = PLAYER_ID()
									PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FREEMODE_SOUNDSET")
									NET_PRINT("HANDLE_PROPERTY_OPTIONS_MENU player used MENU_TYPE_PROPERTY_ENTRY_OPTIONS menu and choose to enter their own house") NET_NL()
									iCurrentPropertyID = tempPropertyExtMenu.iExitSelection
									CDEBUG1LN(DEBUG_SAFEHOUSE, "Player entered property # ", iCurrentPropertyID)
									GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iRequestEntryBS = 0
									CDEBUG1LN(DEBUG_SAFEHOUSE, "STAGE_SETUP_FOR_USING_PROPERTY: GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iRequestEntryBS = 0")
									DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
									IF tempPropertyExtMenu.iCurVertSel = tempPropertyExtMenu.iAllNearby
										SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_INVITE_NEARBY_PLAYERS_INSIDE)
										BROADCAST_PROPERTY_NEARBY_PLAYERS_INVITE_EVENT(PLAYERS_IN_RANGE_OF_POINT(GET_PLAYER_COORDS(PLAYER_ID()),NORMAL_APARTMENT_ENTRY_DISTANCE,TRUE,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE),iCurrentPropertyID)
									ELIF tempPropertyExtMenu.iCurVertSel = tempPropertyExtMenu.iAllFriendsCrew
										BROADCAST_PROPERTY_NEARBY_PLAYERS_INVITE_EVENT(PLAYERS_IN_RANGE_OF_POINT(GET_PLAYER_COORDS(PLAYER_ID()),NORMAL_APARTMENT_ENTRY_DISTANCE,FALSE,TRUE,TRUE,DEFAULT,DEFAULT,DEFAULT,TRUE),iCurrentPropertyID)
										SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_INVITE_CREW_FRIEND_INSIDE)
									ELIF tempPropertyExtMenu.iCurVertSel = tempPropertyExtMenu.iOrganisationNearby
										BROADCAST_PROPERTY_NEARBY_PLAYERS_INVITE_EVENT(PLAYERS_IN_RANGE_OF_POINT(GET_PLAYER_COORDS(PLAYER_ID()),NORMAL_APARTMENT_ENTRY_DISTANCE,FALSE,FALSE,FALSE,DEFAULT,DEFAULT,TRUE,TRUE),iCurrentPropertyID)
										SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_INVITE_ORGANISATION_INSIDE)
									ENDIF
									
									
									SET_LOCAL_STAGE(STAGE_SETUP_FOR_USING_PROPERTY)
									IF IS_PROPERTY_OFFICE_GARAGE(iCurrentPropertyID)
										IF tempPropertyExtMenu.iExitToOfficeFromGarageToMod = 99
											SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iIsPlayerGoingToModGarageBS, biIsPlayerGoingToModGarageTrue)
											PRINTLN("HANDLE_PROPERTY_OPTIONS_MENU: player entering directly to mod shop biIsPlayerGoingToModGarageTrue 1 = TRUE")
										ELSE
											SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iIsPlayerGoingToModGarageBS, biIsPlayerGoingToModGarageFalse)
											CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iIsPlayerGoingToModGarageBS, biIsPlayerGoingToModGarageTrue)
											PRINTLN("HANDLE_PROPERTY_OPTIONS_MENU: player entering office garage biIsPlayerGoingToModGarageTrue 1 = FALSE")
										ENDIF
									ENDIF
								ENDIF
							BREAK
							
//							CASE MP_PROP_MENU_TYPE_AUTH_ENTRY
//								IF tempPropertyExtMenu.iCurVertSel = 0
//									NET_PRINT("MAINTAIN_MP_PROPERTIES: Player triggered leaving house afterlooking at buzzer menu.") NET_NL()
//									SET_LOCAL_STAGE(PROP_STAGE_STARTING_EXIT)
//									RELEASE_CONTEXT_INTENTION(control.iBuzzerContextIntention)
//								ELSE
//									GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.entryResponse[NATIVE_TO_INT(tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel])].iResponse = PROPERTY_BROADCAST_RESPONSE_YES
//									CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_PROPERTY_OPTIONS_MENU allowing ", GET_PLAYER_NAME(tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel]), " to enter your property")
//									CLEAR_BIT(iLocalBS,LOCAL_BS_SETUP_MENU)
//									//CLEAR_BIT(iLocalBS,LOCAL_BS_VIEW_AUTH_ENTRY_MENU)
//									EXIT
//								ENDIF
//							BREAK
						ENDSWITCH
						SET_BIT(tempPropertyExtMenu.iButtonBS,PROP_BUTTON_BS_PRESSED_ACCEPT)
					ENDIF
				ELSE
					IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT)
					OR (bUsingCursor AND bCursorAccept = FALSE)
						CLEAR_BIT(tempPropertyExtMenu.iButtonBS,PROP_BUTTON_BS_PRESSED_ACCEPT)
					ENDIF
				ENDIF
				IF NOT IS_BIT_SET(tempPropertyExtMenu.iButtonBS,PROP_BUTTON_BS_PRESSED_CANCEL)
					IF (IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_CANCEL) AND NOT bProcessingBasket)
					AND CAN_PLAYER_USE_MENU_INPUT()
					AND NOT bInputUsedThisFrame
						bInputUsedThisFrame = TRUE
						PLAY_SOUND_FRONTEND(-1, "CANCEL","HUD_FREEMODE_SOUNDSET")
						IF NOT IS_BIT_SET(tempPropertyExtMenu.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
							NET_PRINT("HANDLE_PROPERTY_OPTIONS_MENU player exited menu") NET_NL()
							IF iCurrentMenuType != MENU_TYPE_PROPERTY_ENTRY_OPTIONS
								globalPropertyEntryData.bLeftOrRejectedBuzzer = TRUE
								CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: globalPropertyEntryData.bLeftOrRejectedBuzzer = TRUE. Exited menu")
							ENDIF
							SWITCH iCurrentMenuType 
								CASE MENU_TYPE_BUZZER
									//SETUP_SPECIFIC_SPAWN_LOCATION(mpProperties[iCurrentPropertyID].house.exits[0].vOutPlayerLoc,mpProperties[iCurrentPropertyID].house.exits[0].fOutPlayerHeading, 10, FALSE)
									SET_LOCAL_STAGE(STAGE_LEAVE_ENTRANCE_AREA)
									IF IS_BIT_SET(iLocalBS,LOCAL_BS_AWAITING_BUZZER_RESPONSE)
										//NETWORK_SET_TALKER_PROXIMITY(CHAT_PROXIMITY_FM)
										globalPropertyEntryData.ownerID = INVALID_PLAYER_INDEX()
										CDEBUG1LN(DEBUG_SAFEHOUSE, "GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iRequestEntryBS = 0")
										CDEBUG1LN(DEBUG_SAFEHOUSE, "globalPropertyEntryData.bLeftOrRejectedBuzzer 5 = TRUE")
										GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iRequestEntryBS = 0	
										NETWORK_CLEAR_VOICE_CHANNEL()
										RESET_NET_TIMER(buzzerTimer)
										globalPropertyEntryData.bLeftOrRejectedBuzzer = TRUE
									ENDIF
									CLEAR_BIT(iLocalBS,LOCAL_BS_AWAITING_BUZZER_RESPONSE)
									EXIT
								BREAK
								CASE MENU_TYPE_PURCHASE
									RESET_TO_ENTRANCE_TO_PROPERTY_STAGE()
									EXIT	
								BREAK
								CASE MENU_TYPE_SELECT_CUSTOM_INTERIOR
									iCustomApartmentTooPurchase = -1
									iCurrentMenuType = MENU_TYPE_PURCHASE
									CLEAR_BIT(iLocalBS,LOCAL_BS_SETUP_MENU)
									EXIT
								BREAK
//								CASE MENU_TYPE_AUTH_ENTRY
//									GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.entryResponse[NATIVE_TO_INT(tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel])].iResponse = PROPERTY_BROADCAST_RESPONSE_NO
//									CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_PROPERTY_OPTIONS_MENU preventing ", GET_PLAYER_NAME(tempPropertyExtMenu.menuPlayerArray[tempPropertyExtMenu.iCurVertSel]), " from entering your property")
//									CLEANUP_MENU_ASSETS()
//									CLEAR_BIT(iLocalBS,LOCAL_BS_SETUP_MENU)
//									CLEAR_BIT(iLocalBS,LOCAL_BS_VIEW_AUTH_ENTRY_MENU)
//									EXIT
//								BREAK
								CASE MENU_TYPE_REPLACE_PROPERTY
									iCurrentMenuType = MENU_TYPE_PURCHASE
									CLEAR_BIT(iLocalBS,LOCAL_BS_SETUP_MENU)
									EXIT
								BREAK
								CASE MENU_TYPE_PROPERTY_ENTRY_OPTIONS
									HANDLE_MENU_DESCRIPTION(iCurrentMenuType)
									DRAW_MENU()
									iCurrentMenuType = MENU_TYPE_BUZZER
									CLEAR_BIT(iLocalBS,LOCAL_BS_SETUP_MENU)
									EXIT
								BREAK
							ENDSWITCH
							//RESET_TO_ENTRANCE_TO_PROPERTY_STAGE()
							EXIT
						ELSE
							NET_PRINT("HANDLE_PROPERTY_OPTIONS_MENU pressed cancel on menu") NET_NL()
							//REMOVE_MENU_HELP_KEYS()
							//ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_ACCEPT, "BB_SELECT")
							//ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_CANCEL, "BB_BACK")
							IF iCurrentMenuType = MENU_TYPE_PURCHASE
							OR iCurrentMenuType = MENU_TYPE_REPLACE_PROPERTY
								CLEAR_BIT(iLocalBS,LOCAL_BS_SETUP_MENU)
								NET_PRINT("HANDLE_PROPERTY_OPTIONS_MENU pressed cancel reset menu") NET_NL()
							ENDIF
							CLEAR_BIT(tempPropertyExtMenu.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
						ENDIF
						SET_BIT(tempPropertyExtMenu.iButtonBS,PROP_BUTTON_BS_PRESSED_CANCEL)
					ENDIF
				ELSE
					IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_CANCEL)
						CLEAR_BIT(tempPropertyExtMenu.iButtonBS,PROP_BUTTON_BS_PRESSED_CANCEL)
					ENDIF
				ENDIF
				
				iTempSelectionVert = tempPropertyExtMenu.iCurVertSel
				IF tempPropertyExtMenu.iMaxVertSel > 0
				AND NOT IS_BIT_SET(iLocalBS,LOCAL_BS_AWAITING_BUZZER_RESPONSE)
				AND NOT IS_BIT_SET(tempPropertyExtMenu.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
				AND NOT bProcessingBasket
					IF NOT IS_BIT_SET(tempPropertyExtMenu.iButtonBS,PROP_BUTTON_BS_PRESSED_UP)
						IF (IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_UP)
						OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_UP)
						OR (IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) AND iCurrentMenuType = MENU_TYPE_BUZZER AND mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iType != ENTRANCE_TYPE_GARAGE AND IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_UP)))
						AND NOT bInputUsedThisFrame
							bInputUsedThisFrame = TRUE
							tempPropertyExtMenu.iCurVertSel--
							PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN","HUD_FREEMODE_SOUNDSET")
							SET_BIT(tempPropertyExtMenu.iButtonBS,PROP_BUTTON_BS_PRESSED_UP)
							RESET_NET_TIMER(tempPropertyExtMenu.menuNavigationDelay)
						ENDIF
					ELSE
						IF HANDLE_PROPERTY_OPTIONS_MENU_NAVIGATION_DELAY(INPUT_FRONTEND_UP)
							CLEAR_BIT(tempPropertyExtMenu.iButtonBS,PROP_BUTTON_BS_PRESSED_UP)	
						ENDIF
					ENDIF
					IF NOT IS_BIT_SET(tempPropertyExtMenu.iButtonBS,PROP_BUTTON_BS_PRESSED_DOWN)
						IF (IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_DOWN)
						OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_DOWN)
						OR (IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) AND iCurrentMenuType = MENU_TYPE_BUZZER AND mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iType != ENTRANCE_TYPE_GARAGE AND IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_DOWN)))
						AND NOT bInputUsedThisFrame
							bInputUsedThisFrame = TRUE
							tempPropertyExtMenu.iCurVertSel++
							PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN","HUD_FREEMODE_SOUNDSET")
							SET_BIT(tempPropertyExtMenu.iButtonBS,PROP_BUTTON_BS_PRESSED_DOWN)
							RESET_NET_TIMER(tempPropertyExtMenu.menuNavigationDelay)
						ENDIF
					ELSE
						IF HANDLE_PROPERTY_OPTIONS_MENU_NAVIGATION_DELAY(INPUT_FRONTEND_DOWN)
							CLEAR_BIT(tempPropertyExtMenu.iButtonBS,PROP_BUTTON_BS_PRESSED_DOWN)	
						ENDIF
					ENDIF
					
					IF tempPropertyExtMenu.iCurVertSel > tempPropertyExtMenu.iMaxVertSel-1
						tempPropertyExtMenu.iCurVertSel = 0
					ENDIF
					IF tempPropertyExtMenu.iCurVertSel < 0
						tempPropertyExtMenu.iCurVertSel = tempPropertyExtMenu.iMaxVertSel -1
					ENDIF
				
					IF tempPropertyExtMenu.iCurVertSel != iTempSelectionVert
						tempPropertyExtMenu.iMenuDescriptionBS = 0
						CDEBUG1LN(DEBUG_SAFEHOUSE, "Updating menu vertical selection changed")
						CLEAR_BIT(iLocalBS,LOCAL_BS_SETUP_MENU)
					ENDIF
				ENDIF
				
				HANDLE_MENU_DESCRIPTION(iCurrentMenuType)
				//IF iCurrentMenuType = MENU_TYPE_PURCHASE
				//AND IS_BIT_SET(tempPropertyExtMenu.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
					//nothing
				//ELSE
					DRAW_MENU()
				//ENDIF
			ENDIF
			IF SHOULD_MENU_UPDATE()
				CDEBUG1LN(DEBUG_SAFEHOUSE, "Updating menu SHOULD_MENU_UPDATE() = true")
				CLEAR_BIT(iLocalBS,LOCAL_BS_SETUP_MENU)
			ENDIF
		ENDIF
	ENDIF
ENDPROC


FUNC BOOL REQUEST_TO_USE_DOOR(INT iDoor, BOOL bRequestControl = TRUE)// #IF IS_DEBUG_BUILD, BOOL bRunDebug = FALSE #ENDIF)
	IF propertyDoors[iDoor].iDoorHash = 0
		//CDEBUG1LN(DEBUG_SAFEHOUSE, "REQUEST_TO_USE_DOOR: There is no door continuing on")
		RETURN TRUE
	ELSE
		IF bUseNewEntryAnims //GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_BUILDING_1
			IF NOT bRequestControl
				IF NOT IS_BIT_SET(iDoorSetBS[iDoor], SERVER_DOOR_LOCKED)
					IF DOOR_SYSTEM_GET_DOOR_PENDING_STATE(propertyDoors[iDoor].iDoorHash) = DOORSTATE_INVALID
						IF DOOR_SYSTEM_GET_DOOR_STATE(propertyDoors[iDoor].iDoorHash) != DOORSTATE_LOCKED
							DOOR_SYSTEM_SET_HOLD_OPEN(propertyDoors[iDoor].iDoorHash,FALSE)
							DOOR_SYSTEM_SET_DOOR_STATE(propertyDoors[iDoor].iDoorHash,DOORSTATE_LOCKED,FALSE,TRUE)
							RESET_NET_TIMER(serverDoorStateTimeout[iDoor])
							SET_BIT(iDoorSetBS[iDoor], SERVER_DOOR_LOCKED)
							CLEAR_BIT(iDoorSetBS[iDoor], SERVER_DOOR_UNLOCKED)
							CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_DOOR_USAGE_REQUESTS_SERVER: Setting door  (STATE NOT LOCKED) #", iDoor, " to be locked")
						ENDIF
					ELSE
						IF DOOR_SYSTEM_GET_DOOR_PENDING_STATE(propertyDoors[iDoor].iDoorHash) != DOORSTATE_LOCKED
							DOOR_SYSTEM_SET_HOLD_OPEN(propertyDoors[iDoor].iDoorHash,FALSE)
							DOOR_SYSTEM_SET_DOOR_STATE(propertyDoors[iDoor].iDoorHash,DOORSTATE_LOCKED,FALSE,TRUE)
							RESET_NET_TIMER(serverDoorStateTimeout[iDoor])
							CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_DOOR_USAGE_REQUESTS_SERVER: Setting door  (STATE NOT PENDING LOCKED)  #", iDoor, " to be locked")
							SET_BIT(iDoorSetBS[iDoor], SERVER_DOOR_LOCKED)
							CLEAR_BIT(iDoorSetBS[iDoor], SERVER_DOOR_UNLOCKED)
						ENDIF
					ENDIF					
				ELSE
					IF NOT HAS_NET_TIMER_STARTED(serverDoorStateTimeout[iDoor])
						START_NET_TIMER(serverDoorStateTimeout[iDoor],TRUE)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_DOOR_USAGE_REQUESTS_SERVER: Setting door state (Locked) timer for door  #", iDoor)
					ENDIF
					RETURN TRUE
				ENDIF
			ELSE
				IF NOT IS_BIT_SET(iDoorSetBS[iDoor], SERVER_DOOR_UNLOCKED)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "CDM: trying to unlock door.")
					IF DOOR_SYSTEM_GET_DOOR_PENDING_STATE(propertyDoors[iDoor].iDoorHash) = DOORSTATE_INVALID
						IF DOOR_SYSTEM_GET_DOOR_STATE(propertyDoors[iDoor].iDoorHash) != DOORSTATE_UNLOCKED
							DOOR_SYSTEM_SET_DOOR_STATE(propertyDoors[iDoor].iDoorHash,DOORSTATE_UNLOCKED,FALSE,TRUE)
							DOOR_SYSTEM_SET_HOLD_OPEN(propertyDoors[iDoor].iDoorHash,TRUE)
							RESET_NET_TIMER(serverDoorStateTimeout[iDoor])
							CLEAR_BIT(iDoorSetBS[iDoor], SERVER_DOOR_LOCKED)
							SET_BIT(iDoorSetBS[iDoor], SERVER_DOOR_UNLOCKED)
							CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_DOOR_USAGE_REQUESTS_SERVER: Setting door (STATE NOT UNLOCKED) #", iDoor, " to be open")
						ENDIF
					ELSE
						IF DOOR_SYSTEM_GET_DOOR_PENDING_STATE(propertyDoors[iDoor].iDoorHash) != DOORSTATE_UNLOCKED
							DOOR_SYSTEM_SET_DOOR_STATE(propertyDoors[iDoor].iDoorHash,DOORSTATE_UNLOCKED,FALSE,TRUE)
							DOOR_SYSTEM_SET_HOLD_OPEN(propertyDoors[iDoor].iDoorHash,TRUE)
							RESET_NET_TIMER(serverDoorStateTimeout[iDoor])
							CLEAR_BIT(iDoorSetBS[iDoor], SERVER_DOOR_LOCKED)
							SET_BIT(iDoorSetBS[iDoor], SERVER_DOOR_UNLOCKED)
							CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_DOOR_USAGE_REQUESTS_SERVER: Setting door (STATE NOT PENDING UNLOCKED) #", iDoor, " to be open")
						ENDIF
					ENDIF
				ELSE
					IF NOT HAS_NET_TIMER_STARTED(serverDoorStateTimeout[iDoor])
						START_NET_TIMER(serverDoorStateTimeout[iDoor],TRUE)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_DOOR_USAGE_REQUESTS_SERVER: Setting door state (UN-Locked) timer for door  #", iDoor)
					ENDIF
					RETURN TRUE
				ENDIF
			ENDIF
			IF HAS_NET_TIMER_STARTED(serverDoorStateTimeout[iDoor])
				IF HAS_NET_TIMER_EXPIRED(serverDoorStateTimeout[iDoor],1000,TRUE)
					RESET_NET_TIMER(serverDoorStateTimeout[iDoor])
					CLEAR_BIT(iDoorSetBS[iDoor], SERVER_DOOR_LOCKED)
					CLEAR_BIT(iDoorSetBS[iDoor], SERVER_DOOR_UNLOCKED)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_DOOR_USAGE_REQUESTS_SERVER: Clearing door state timer for door  #", iDoor)
				ENDIF
			ENDIF
		ENDIF
//		#IF NOT FEATURE_HEIST_PLANNING
//		ELSE
//			#IF IS_DEBUG_BUILD
//			IF bRunDebug
//				IF DOOR_SYSTEM_GET_DOOR_PENDING_STATE(propertyDoors[iDoor].iDoorHash) = DOORSTATE_INVALID
//					IF DOOR_SYSTEM_GET_DOOR_STATE(propertyDoors[iDoor].iDoorHash) != DOORSTATE_UNLOCKED
//						IF NETWORK_HAS_CONTROL_OF_DOOR(propertyDoors[iDoor].iDoorHash)
//							DOOR_SYSTEM_SET_DOOR_STATE(propertyDoors[iDoor].iDoorHash,DOORSTATE_UNLOCKED,TRUE,TRUE)
//						ELSE
//							NETWORK_REQUEST_CONTROL_OF_DOOR(propertyDoors[iDoor].iDoorHash)
//						ENDIF
//					ENDIF
//				ELSE
//					IF DOOR_SYSTEM_GET_DOOR_PENDING_STATE(propertyDoors[iDoor].iDoorHash) != DOORSTATE_UNLOCKED
//						IF NETWORK_HAS_CONTROL_OF_DOOR(propertyDoors[iDoor].iDoorHash)
//							DOOR_SYSTEM_SET_DOOR_STATE(propertyDoors[iDoor].iDoorHash,DOORSTATE_UNLOCKED,TRUE,TRUE)
//						ELSE
//							NETWORK_REQUEST_CONTROL_OF_DOOR(propertyDoors[iDoor].iDoorHash)
//						ENDIF
//					ENDIF
//				ENDIF
//
//			ENDIF
//			#ENDIF
//		
//			IF NOT bRequestControl
//				IF IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, PLAYER_BD_BS_REQUEST_USE_OF_DOOR0+iDoor)
//					CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, PLAYER_BD_BS_REQUEST_USE_OF_DOOR0+iDoor)
//					//RESET_NET_TIMER(requestingUseOfDoorTimer)
//					CDEBUG1LN(DEBUG_SAFEHOUSE, "REQUEST_TO_USE_DOOR(EXT): Local player clearing request to use door #",iDoor," player #",NATIVE_TO_INT(PLAYER_ID()) )
//				ENDIF	
//			ELSE
//				IF NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, PLAYER_BD_BS_REQUEST_USE_OF_DOOR0+iDoor)
//					SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, PLAYER_BD_BS_REQUEST_USE_OF_DOOR0+iDoor)
//					//REINIT_NET_TIMER(requestingUseOfDoorTimer,TRUE)
//					CDEBUG1LN(DEBUG_SAFEHOUSE, "REQUEST_TO_USE_DOOR(EXT): Local player requesting to use door #",iDoor," player #",NATIVE_TO_INT(PLAYER_ID()) )
//				ENDIF
//				//IF serverBD.iRequestDoorUsageBS[iDoor] != 0
//				IF IS_BIT_SET(serverBD.iRequestDoorUsageBS[iDoor],NATIVE_TO_INT(PLAYER_ID()))
//				OR IS_BIT_SET(serverBD.iPlayersInsideEntranceBS[iDoor],NATIVE_TO_INT(PLAYER_ID()))
//					RETURN TRUE
//				ENDIF
//				//ENDIF
//			ENDIF
//		ENDIF
//		#ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

// === Garage Cutscene Fudge Two ===


#IF IS_DEBUG_BUILD
//	BOOl bDebugLaunchGarageCarEnterCutscene = FALSE
//	BOOl bDebugSeenGarageCarEnterCutscene = FALSE
//	INT iDebugGarageCarEnterCutsceneStage = 0
//	
//	BOOl bDebugLaunchGaragePedEnterCutscene = FALSE
//	BOOl bDebugSeenGaragePedEnterCutscene = FALSE
//	INT iDebugGaragePedEnterCutsceneStage = 0
#ENDIF

CAMERA_INDEX camGarageCutscene0, camGarageCutscene1
INT iGarageCutTimer
INT iFadeDelayTimer

CONST_INT GARAGE_CUT_SKIP_TIME_SHORT			5000
CONST_INT GARAGE_CUT_SKIP_TIME_LONG				10000
CONST_INT GARAGE_CUT_SKIP_TIME_BEAT				0666

//iGarageCutsceneBitset
CONST_INT GARAGE_CUT_BS_INTERPOLATE_STARTED		0

INT iGarageCutsceneBitset


FUNC BOOL IS_TASK_ONGOING(PED_INDEX thisPed, SCRIPT_TASK_NAME thisTaskName)
	IF GET_SCRIPT_TASK_STATUS(thisPed, thisTaskName) = PERFORMING_TASK
	OR GET_SCRIPT_TASK_STATUS(thisPed, thisTaskName) = WAITING_TO_START_TASK
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC REQUEST_GARAGE_DOOR_OPEN()//#IF IS_DEBUG_BUILD BOOL bRunDebug = FALSE #ENDIF)
	REQUEST_TO_USE_DOOR(2, TRUE)// #IF IS_DEBUG_BUILD, bRunDebug  #ENDIF)
ENDPROC

PROC REQUEST_GARAGE_DOOR_CLOSE()//#IF IS_DEBUG_BUILD BOOL bRunDebug = FALSE #ENDIF)
	REQUEST_TO_USE_DOOR(2, FALSE)// #IF IS_DEBUG_BUILD, bRunDebug  #ENDIF)
ENDPROC

FUNC BOOL IS_GARAGE_DOOR_UNLOCKED()
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_GARAGE_DOOR_LOCKED()
	
	RETURN TRUE
ENDFUNC

VEHICLE_INDEX vehFakeGarageEnter


ENUM GARAGE_CAR_ENTER_CUTSCENE_STAGE
	GARAGE_CAR_ENTER_CUTSCENE_INIT = 0,
	GARAGE_CAR_ENTER_CUTSCENE_LOAD_CUT,
	GARAGE_CAR_ENTER_CUTSCENE_TRIGGER_CUT,
	GARAGE_CAR_ENTER_CUTSCENE_WAIT_FOR_OPEN_DOOR,
	GARAGE_CAR_ENTER_CUTSCENE_WAIT_FOR_INSIDE,
	GARAGE_CAR_ENTER_CUTSCENE_WAIT_FOR_CLOSE_DOOR,
	GARAGE_CAR_ENTER_CUTSCENE_WAIT_FOR_CLOSE_DOOR_beat,
	GARAGE_CAR_ENTER_CUTSCENE_OFFICE_GARAGE_PAUSE,
	GARAGE_CAR_ENTER_CUTSCENE_OFFICE_GARAGE_WAIT_FADE,
	GARAGE_CAR_ENTER_CUTSCENE_COMPLETE
ENDENUM
GARAGE_CAR_ENTER_CUTSCENE_STAGE garageCarEnterCutStage


BOOL bLoadedElevatorSounds

FUNC BOOL PlayerIsInArcadiusUndergroundCarPark()
	RETURN IS_ENTITY_IN_ANGLED_AREA (PLAYER_PED_ID(), <<-181.591, -663.244, 24.799>>, <<-146.266, -566.319, 39.349>>, 88.375)
ENDFUNC

PROC UPDATE_OFFICE_GARAGE_AUDIO()
	
	// if arcadius load the audio bank
	IF (mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_OFFICE_BUILDING_3)
		IF NOT IS_CURRENT_MISSION_THIS_TUNER_ROBBERY_FINALE(TR_UNION_DEPOSITORY) 
			IF NOT (bLoadedElevatorSounds)
				IF PlayerIsInArcadiusUndergroundCarPark()
					IF REQUEST_SCRIPT_AUDIO_BANK("DLC_IMPORTEXPORT/GARAGE_ELEVATOR")
						bLoadedElevatorSounds = TRUE
						CDEBUG1LN(DEBUG_SAFEHOUSE, "UPDATE_OFFICE_GARAGE_AUDIO - loaded elevator sounds")
					ENDIF
				ENDIF
			ELSE
				IF NOT PlayerIsInArcadiusUndergroundCarPark()
				AND NOT (garageCarEnterCutStage > GARAGE_CAR_ENTER_CUTSCENE_TRIGGER_CUT)
					RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_IMPORTEXPORT/GARAGE_ELEVATOR")
					bLoadedElevatorSounds = FALSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "UPDATE_OFFICE_GARAGE_AUDIO - unloaded elevator sounds")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

BOOL bStartedGarageDoorSound
INT iGarageDoorSound

FUNC BOOL SLIDE_OFFICE_GARAGE_DOORS(BOOL bOpen, FLOAT &fSpeed)
	INT i
	
	VECTOR vTargetPosition
	VECTOR vCurrentPosition
	VECTOR vec
	FLOAT fDist
	
	BOOL bComplete[2]
		
	IF NOT (bStartedGarageDoorSound) 
		iGarageDoorSound = GET_SOUND_ID()
		IF (bOpen)			
			IF ((mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_OFFICE_BUILDING_3) AND (bLoadedElevatorSounds))
				PLAY_SOUND_FRONTEND(iGarageDoorSound, "Elevator_Doors_Opening_Loop", "DLC_IE_Garage_Elevator_Sounds")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "SLIDE_OFFICE_GARAGE_DOORS - started sound Elevator_Doors_Opening_Loop. sound id ", iGarageDoorSound)
			ELSE
				//PLAY_SOUND_FROM_COORD(iGarageDoorSound, "Garage_Door_Open_Loop", GET_ENTITY_COORDS(officeGarageDoors[0]), "GTAO_Script_Doors_Sounds")
				PLAY_SOUND_FRONTEND(iGarageDoorSound, "Garage_Door_Open_Loop", "GTAO_Script_Doors_Sounds")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "SLIDE_OFFICE_GARAGE_DOORS - started sound Garage_Door_Open_Loop. sound id ", iGarageDoorSound)
			ENDIF
		ELSE
			IF ((mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_OFFICE_BUILDING_3) AND (bLoadedElevatorSounds))
				PLAY_SOUND_FRONTEND(iGarageDoorSound, "Elevator_Doors_Closing_Loop", "DLC_IE_Garage_Elevator_Sounds")		
				CDEBUG1LN(DEBUG_SAFEHOUSE, "SLIDE_OFFICE_GARAGE_DOORS - started sound Elevator_Doors_Closing_Loop. sound id ", iGarageDoorSound)
			ELSE
				//PLAY_SOUND_FROM_COORD(iGarageDoorSound, "Garage_Door_Close_Loop", GET_ENTITY_COORDS(officeGarageDoors[0]), "GTAO_Script_Doors_Sounds")		
				PLAY_SOUND_FRONTEND(iGarageDoorSound, "Garage_Door_Close_Loop", "GTAO_Script_Doors_Sounds")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "SLIDE_OFFICE_GARAGE_DOORS - started sound Garage_Door_Close_Loop. sound id ", iGarageDoorSound)
			ENDIF
		ENDIF
		bStartedGarageDoorSound = TRUE
	ENDIF
		
	// if closing door make sure car fully inside.
	IF (bOpen = FALSE)
		IF DOES_ENTITY_EXIST(vehFakeGarageEnter)
			
			IF (mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_OFFICE_BUILDING_3)
				VECTOR vPos1, vPos2
				vPos1 = GET_OFFICE_GARAGE_VECTOR_AS_OFFSET(mpProperties[iCurrentPropertyID].iBuildingID, 10, FALSE)
				vPos2 = GET_OFFICE_GARAGE_VECTOR_AS_OFFSET(mpProperties[iCurrentPropertyID].iBuildingID, 11, FALSE)
				vTargetPosition = ((vPos1 + vPos2) * 0.5)
			ELSE
				vTargetPosition = GET_OFFICE_GARAGE_EXTERNAL_DOOR_POSITION()				
			ENDIF						
			
			VECTOR vMin, vMax
			GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(vehFakeGarageEnter), vMin, vMax)
			
			VECTOR vFront, vBack
			vFront = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehFakeGarageEnter, <<0.0, ((vMax.y - vMin.y) * 0.5), 0.0>>)
			vBack = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehFakeGarageEnter, <<0.0, ((vMax.y - vMin.y) * -0.5), 0.0>>)
			
			CDEBUG1LN(DEBUG_SAFEHOUSE, "SLIDE_OFFICE_GARAGE_DOORS - vFront ", vFront)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "SLIDE_OFFICE_GARAGE_DOORS - vBack ", vBack)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "SLIDE_OFFICE_GARAGE_DOORS - vTargetPosition ", vTargetPosition)
			
			VECTOR vFrontToFinish
			VECTOR vBackToFinish
			vFrontToFinish = vTargetPosition - vFront
			vBackToFinish = vTargetPosition - vBack
			
			IF VMAG(vFrontToFinish) < VMAG(vBackToFinish)
				// front is nearer the exit
				CDEBUG1LN(DEBUG_SAFEHOUSE, "SLIDE_OFFICE_GARAGE_DOORS - front is nearer the exit ")
				RETURN FALSE
			ELSE
				IF (DOT_PRODUCT(vFrontToFinish,vBackToFinish) > 0.0) 
					// car is fully past the door
					CDEBUG1LN(DEBUG_SAFEHOUSE, "SLIDE_OFFICE_GARAGE_DOORS - car is fully past the door ")
				ELSE
					// car is not past the door.
					CDEBUG1LN(DEBUG_SAFEHOUSE, "SLIDE_OFFICE_GARAGE_DOORS - car is not past the door ")
					RETURN FALSE
				ENDIF
				
			ENDIF
			
		ENDIF
	ENDIF
	
	// slide doors
	IF (mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_OFFICE_BUILDING_3)
		REPEAT 2 i
			IF DOES_ENTITY_EXIST(officeGarageDoors[i])
				
				bComplete[i] = FALSE
			
				IF NOT (bOpen)
					vTargetPosition = GET_OFFICE_GARAGE_VECTOR_AS_OFFSET(mpProperties[iCurrentPropertyID].iBuildingID, 8 + i, FALSE)
				ELSE
					vTargetPosition = GET_OFFICE_GARAGE_VECTOR_AS_OFFSET(mpProperties[iCurrentPropertyID].iBuildingID, 10 + i, FALSE)
				ENDIF
				
				vCurrentPosition = GET_ENTITY_COORDS(officeGarageDoors[i])
				
				vec = vTargetPosition - vCurrentPosition
				
				fDist = 0.0
				fDist = fDist +@ fSpeed
				
				IF (VMAG(vec) < fDist)
					SET_ENTITY_COORDS_NO_OFFSET(officeGarageDoors[i], vTargetPosition)
					bComplete[i] = TRUE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "SLIDE_OFFICE_GARAGE_DOORS - sliding door finished ", i)
				ELSE
					vec /= VMAG(vec)
					vec *= fDist
					SET_ENTITY_COORDS_NO_OFFSET(officeGarageDoors[i], vCurrentPosition+vec)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "SLIDE_OFFICE_GARAGE_DOORS - sliding door ", i, " current pos ", vCurrentPosition, " target pos ", vTargetPosition)
				ENDIF
				
				
			ENDIF
		ENDREPEAT
		
		REPEAT 2 i
			IF NOT (bComplete[i])
				RETURN FALSE
			ENDIF
		ENDREPEAT
		
	ELSE
	
		IF DOES_ENTITY_EXIST(officeGarageExternalDoor)

			vTargetPosition = GET_OFFICE_GARAGE_EXTERNAL_DOOR_POSITION()

			IF (bOpen)
				vTargetPosition += <<0.0, 0.0, 4.35>>
			ENDIF
			
			vCurrentPosition = GET_ENTITY_COORDS(officeGarageExternalDoor)
			
			vec = vTargetPosition - vCurrentPosition
			
			fDist = 0.0
			fDist = fDist +@ fSpeed
			
			IF (VMAG(vec) < fDist)
				SET_ENTITY_COORDS_NO_OFFSET(officeGarageExternalDoor, vTargetPosition)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "SLIDE_OFFICE_GARAGE_DOORS - sliding external door finished ")
			ELSE
				vec /= VMAG(vec)
				vec *= fDist
				SET_ENTITY_COORDS_NO_OFFSET(officeGarageExternalDoor, vCurrentPosition+vec)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "SLIDE_OFFICE_GARAGE_DOORS - sliding external door current pos ", vCurrentPosition, " target pos ", vTargetPosition)
				RETURN FALSE
			ENDIF
			
			
		ENDIF

	ENDIF
	
	// stop sound
	IF (bStartedGarageDoorSound)
		STOP_SOUND(iGarageDoorSound)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "SLIDE_OFFICE_GARAGE_DOORS - stopping sound id ", iGarageDoorSound)
		bStartedGarageDoorSound = FALSE
		
		// play 'going up' after doors close
		IF (bOpen = FALSE)
			IF ((mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_OFFICE_BUILDING_3) AND (bLoadedElevatorSounds))
				iGarageDoorSound = GET_SOUND_ID()
				PLAY_SOUND_FRONTEND(iGarageDoorSound, "Speech_Going_Up", "DLC_IE_Garage_Elevator_Sounds")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "SLIDE_OFFICE_GARAGE_DOORS - playing audio Speech_Going_Up, sound id ", iGarageDoorSound)
			ENDIF
		ENDIF
		
	ENDIF
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "SLIDE_OFFICE_GARAGE_DOORS - returning true ")
	RETURN TRUE

ENDFUNC

FUNC BOOL IS_GARAGE_DOOR_OPEN()

	IF IS_PROPERTY_OFFICE_GARAGE(iCurrentPropertyID)
		RETURN SLIDE_OFFICE_GARAGE_DOORS(TRUE, fDoorSlideOpenSpeed)
	ENDIF

	IF propertyDoors[2].iDoorHash = 0
		RETURN TRUE
	ENDIF
	
	
	//CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: IS_GARAGE_DOOR_OPEN  ratio is: ",DOOR_SYSTEM_GET_OPEN_RATIO(propertyDoors[2].iDoorHash))
	IF DOOR_SYSTEM_GET_OPEN_RATIO(propertyDoors[2].iDoorHash) >= 0.65
	OR mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_BUILDING_5 AND DOOR_SYSTEM_GET_OPEN_RATIO(propertyDoors[2].iDoorHash) >= 0.5
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_GARAGE_DOOR_CLOSED()

	IF IS_PROPERTY_OFFICE_GARAGE(iCurrentPropertyID)
		RETURN SLIDE_OFFICE_GARAGE_DOORS(FALSE, fDoorSlideCloseSpeed)
	ENDIF

	IF propertyDoors[2].iDoorHash = 0
		RETURN TRUE
	ENDIF
	IF DOOR_SYSTEM_GET_OPEN_RATIO(propertyDoors[2].iDoorHash) <= 0.1
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC RESET_GARAGE_CUTSCENE(BOOL bClearTasks = FALSE, BOOL bTurnOffRenderScriptCams = TRUE)
	
	IF bTurnOffRenderScriptCams
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
	ENDIf
	IF IS_BIT_SET(iLocalBS2,LOCAL_BS2_loadedGarageWalkInAnim)
		REMOVE_ANIM_DICT("anim@apt_trans@garage")
		CLEAR_BIT(iLocalBS2,LOCAL_BS2_loadedGarageWalkInAnim)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "ANIMATED_WALK_INTO_GARAGE: removing anim@apt_trans@garage")
	ENDIF
	IF DOES_CAM_EXIST(camGarageCutscene0)
		DESTROY_CAM(camGarageCutscene0)
	ENDIF
	IF DOES_CAM_EXIST(camGarageCutscene1)
		DESTROY_CAM(camGarageCutscene1)
	ENDIF
	IF g_bPropInteriorScriptRunning
	OR IS_BIT_SET(mpPropMaintain.iBS, MP_PROP_MAINTAIN_BS_LAUNCHED_INT_SCRIPT)
		globalPropertyEntryData.bLeftOrRejectedBuzzer = TRUE
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: globalPropertyEntryData.bLeftOrRejectedBuzzer = TRUE. RESET_GARAGE_CUTSCENE")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("DLC_IE_Garage_Elevator_Enter_Scene")
		STOP_AUDIO_SCENE("DLC_IE_Garage_Elevator_Enter_Scene")
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT - RESET_GARAGE_CUTSCENE - stopped audio scene DLC_IE_Garage_Elevator_Enter_Scene")										
	ENDIF
	
	IF bUseNewEntryAnims //GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_BUILDING_1
		IF IS_AUDIO_SCENE_ACTIVE("DLC_MPHEIST_DRIVE_INTO_GARAGE_SCENE")
			STOP_AUDIO_SCENE("DLC_MPHEIST_DRIVE_INTO_GARAGE_SCENE")	
		ENDIF
		IF IS_AUDIO_SCENE_ACTIVE("DLC_Biker_Clubhouse_Enter_In_Vehicle_Scene")
			STOP_AUDIO_SCENE("DLC_Biker_Clubhouse_Enter_In_Vehicle_Scene")	
		ENDIF
		CLEANUP_MP_CUTSCENE(DEFAULT,FALSE)
	ENDIF
	CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: REQUEST_TO_USE_DOOR closing garage door - 1")
	REQUEST_GARAGE_DOOR_CLOSE()
	RESET_TO_ENTRANCE_TO_PROPERTY_STAGE()
	iGarageCutsceneBitset = 0
	
	IF (bClearTasks)
		// redundant
	ENDIF
	
							//playerId	bHasControl	bVisible	bClearTasks	bHasCollision	bFreezePos
	NET_SET_PLAYER_CONTROL(	PLAYER_ID(),TRUE)
	CLEAR_BIT(iLocalBS, LOCAL_BS_TAKEN_AWAY_CONTROL)
	CLEAR_BIT(entryCutData.iBS,ENTRY_CS_BS_TOOK_AWAY_CONTROL)
ENDPROC


//PROC CONCEAL_ALL_OTHER_PLAYERS(BOOL bHide)
//	INT iParticipant
//	PARTICIPANT_INDEX partTemp
//	PLAYER_INDEX playerTemp
//	
//	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
//		partTemp = INT_TO_PARTICIPANTINDEX(iParticipant)
//		IF NETWORK_IS_PARTICIPANT_ACTIVE(partTemp)
//			playerTemp = NETWORK_GET_PLAYER_INDEX(partTemp)
//			IF IS_NET_PLAYER_OK(playerTemp)
//		       	IF playerTemp!= PLAYER_ID()
//					NETWORK_CONCEAL_PLAYER(playerTemp, bHide)
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDREPEAT
//ENDPROC

BOOL bCleanupFakeGarageEnter
CONST_INT NUM_VEHICLE_SEATS 9
//CONST_FLOAT fBaseVehicleOffset -0.6448
FLOAT fFakeVehicleOffset
PED_INDEX pedFakeGarageEnter[NUM_VEHICLE_SEATS]
MP_FAKE_GARAGE_DRIVE_STRUCT fakeGarageDriveData
OBJECT_INDEX vehCrateGarageEnter

ENUM FAKE_GARAGE_CAR_ENTER_CUTSCENE_STAGE
	FAKE_GARAGE_CAR_ENTER_CUTSCENE_INIT = 0,
	FAKE_GARAGE_CAR_ENTER_CUTSCENE_START,
	FAKE_GARAGE_CAR_ENTER_CUTSCENE_RUNNING,
	FAKE_GARAGE_CAR_ENTER_CUTSCENE_COMPLETE
ENDENUM

FAKE_GARAGE_CAR_ENTER_CUTSCENE_STAGE fakeGarageCarEnterCutsceneStage

FAKE_GARAGE_CAR_ENTER_CUTSCENE_STAGE iRunCarEntryFadeStage

PROC RESET_FAKE_GARAGE_CAR_ENTER_CUTSCENE(BOOL bClearTasks = FALSE, BOOL bTurnOffRenderScriptCams = TRUE)
	RESET_GARAGE_CUTSCENE(bClearTasks, bTurnOffRenderScriptCams)
	iPedsInCar = 0
	iFadeDelayTimer = 0
	fakeGarageCarEnterCutsceneStage = FAKE_GARAGE_CAR_ENTER_CUTSCENE_INIT
ENDPROC

PROC PLAY_ENTER_GARGE_OPENING_SOUND()

	INT iSoundID = GET_SOUND_ID()
//	IF GET_PROPERTY_SIZE_TYPE(mpProperties[iCurrentProperty].iIndex) = PROP_SIZE_TYPE_LARGE_APT
		PLAY_SOUND_FRONTEND(iSoundID,"GARAGE_DOOR_SCRIPTED_CLOSE")
		SET_VARIABLE_ON_SOUND(iSoundID, "hold", 2.25) 
//	ENDIF
	CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: PLAY_ENTER_GARGE_OPENING_SOUND open audio being played")
ENDPROC

PROC DELETE_CLONED_ENTITIES_AND_CLEAR_REQUESTS(INT iCall)
	IF iCall = 0
		//
	ENDIF
	INT i
	IF DOES_ENTITY_EXIST(vehCrateGarageEnter)
		SET_MODEL_AS_NO_LONGER_NEEDED(GET_ENTITY_MODEL(vehCrateGarageEnter))
		DELETE_OBJECT(vehCrateGarageEnter)
	ENDIF
	IF DOES_ENTITY_EXIST(vehFakeGarageEnter)
		SET_MODEL_AS_NO_LONGER_NEEDED(GET_ENTITY_MODEL(vehFakeGarageEnter))
		DELETE_VEHICLE(vehFakeGarageEnter)
	ENDIF
	
	REPEAT COUNT_OF(pedFakeGarageEnter) i
		IF DOES_ENTITY_EXIST(pedFakeGarageEnter[i])
			DELETE_PED(pedFakeGarageEnter[i])
		ENDIF
	ENDREPEAT
	CDEBUG1LN(DEBUG_SAFEHOUSE, "DELETE_CLONED_ENTITIES_AND_CLEAR_REQUESTS: called: ",iCall)
ENDPROC

PROC CLEANUP_FAKE_GARAGE_CAR_ENTER_THIS_CUTSCENE(BOOL bIgnoreStageCheck)
	IF bIgnoreStageCheck
	OR (fakeGarageCarEnterCutsceneStage > FAKE_GARAGE_CAR_ENTER_CUTSCENE_INIT
	AND fakeGarageCarEnterCutsceneStage < FAKE_GARAGE_CAR_ENTER_CUTSCENE_COMPLETE)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "CLEANUP_FAKE_GARAGE_CAR_ENTER_THIS_CUTSCENE- Called this frame")
		iPedsInCar = 0
		IF IS_BIT_SET(iLocalBS, LOCAL_BS_TAKEN_AWAY_CONTROL)
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)  
			CLEAR_BIT(iLocalBS, LOCAL_BS_TAKEN_AWAY_CONTROL)
			CLEAR_BIT(entryCutData.iBS,ENTRY_CS_BS_TOOK_AWAY_CONTROL)
		ENDIF
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
		//CONCEAL_ALL_OTHER_PLAYERS(FALSE)
		//NETWORK_SET_IN_MP_CUTSCENE(FALSE, FALSE)
		//MAKE_PLAYER_SAFE_FOR_MP_CUTSCENE(FALSE, FALSE, TRUE, FALSE)
		CLEANUP_MP_CUTSCENE(TRUE)
		IF IS_AUDIO_SCENE_ACTIVE("DLC_Biker_Clubhouse_Enter_In_Vehicle_Scene")
			STOP_AUDIO_SCENE("DLC_Biker_Clubhouse_Enter_In_Vehicle_Scene")	
		ENDIF
		bCleanupFakeGarageEnter = FALSE
		DELETE_CLONED_ENTITIES_AND_CLEAR_REQUESTS(1)
		
		IF g_bPropInteriorScriptRunning
		OR IS_BIT_SET(mpPropMaintain.iBS, MP_PROP_MAINTAIN_BS_LAUNCHED_INT_SCRIPT)
			globalPropertyEntryData.bLeftOrRejectedBuzzer = TRUE
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: globalPropertyEntryData.bLeftOrRejectedBuzzer = TRUE. bRun = FALSE ")
		ENDIF
		globalPropertyEntryData.bWalkInSeqDone = FALSE
		RESET_FAKE_GARAGE_CAR_ENTER_CUTSCENE(TRUE)
		RESET_TO_ENTRANCE_TO_PROPERTY_STAGE()
	ENDIF
ENDPROC 

PROC RESET_FADED_OUT_GARAGE_CAR_ENTER(BOOL bClearTasks = FALSE, BOOL bTurnOffRenderScriptCams = TRUE)
	RESET_GARAGE_CUTSCENE(bClearTasks, bTurnOffRenderScriptCams)
	iPedsInCar = 0
	iFadeDelayTimer = 0
	iRunCarEntryFadeStage = FAKE_GARAGE_CAR_ENTER_CUTSCENE_INIT
ENDPROC


PROC CLEANUP_FADED_OUT_GARAGE_CAR_ENTER(BOOL bIgnoreStageCheck)
	IF bIgnoreStageCheck
	OR (iRunCarEntryFadeStage > FAKE_GARAGE_CAR_ENTER_CUTSCENE_INIT
	AND iRunCarEntryFadeStage < FAKE_GARAGE_CAR_ENTER_CUTSCENE_COMPLETE)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "CLEANUP_FADED_OUT_GARAGE_CAR_ENTER- Called this frame")
		iPedsInCar = 0
		IF IS_BIT_SET(iLocalBS, LOCAL_BS_TAKEN_AWAY_CONTROL)
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)  
			CLEAR_BIT(iLocalBS, LOCAL_BS_TAKEN_AWAY_CONTROL)
			CLEAR_BIT(entryCutData.iBS,ENTRY_CS_BS_TOOK_AWAY_CONTROL)
		ENDIF
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
		//CONCEAL_ALL_OTHER_PLAYERS(FALSE)
		//NETWORK_SET_IN_MP_CUTSCENE(FALSE, FALSE)
		//MAKE_PLAYER_SAFE_FOR_MP_CUTSCENE(FALSE, FALSE, TRUE, FALSE)
		CLEANUP_MP_CUTSCENE(TRUE)
		IF IS_AUDIO_SCENE_ACTIVE("DLC_Biker_Clubhouse_Enter_In_Vehicle_Scene")
			STOP_AUDIO_SCENE("DLC_Biker_Clubhouse_Enter_In_Vehicle_Scene")	
		ENDIF
		bCleanupFakeGarageEnter = FALSE

		IF g_bPropInteriorScriptRunning
		OR IS_BIT_SET(mpPropMaintain.iBS, MP_PROP_MAINTAIN_BS_LAUNCHED_INT_SCRIPT)
			globalPropertyEntryData.bLeftOrRejectedBuzzer = TRUE
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: globalPropertyEntryData.bLeftOrRejectedBuzzer = TRUE. bRun = FALSE ")
		ENDIF
		globalPropertyEntryData.bWalkInSeqDone = FALSE
		RESET_FAKE_GARAGE_CAR_ENTER_CUTSCENE(TRUE)
		RESET_TO_ENTRANCE_TO_PROPERTY_STAGE()
	ENDIF
ENDPROC 

FUNC BOOL RUN_FADED_OUT_GARAGE_CAR_ENTER()
	INT i
	
	BOOL bRun = FALSE
	VEHICLE_INDEX vehPlayer
	//CDEBUG1LN(DEBUG_SAFEHOUSE, "CDM: Current Stack 3 = ",GET_CURRENT_STACK_SIZE())
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF iRunCarEntryFadeStage <= FAKE_GARAGE_CAR_ENTER_CUTSCENE_START
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(),TRUE)
				vehPlayer = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(),TRUE)
				
				IF IS_VEHICLE_DRIVEABLE(vehPlayer)
					IF NETWORK_HAS_CONTROL_OF_ENTITY(vehPlayer)
						SET_ENTITY_CAN_BE_DAMAGED(vehPlayer,FALSE)
						g_bMadeCarInvunerableOnEntry = TRUE
						CDEBUG1LN(DEBUG_SAFEHOUSE, "SET_ENTITY_CAN_BE_DAMAGED FALSE 13543 called from exterior script- VEH")	
					ENDIF
					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: vehPlayer exists and is driveable - bRun = TRUE")
					bRun = TRUE
				ENDIF
			ENDIF
		ELSE
			bRun = TRUE
		ENDIF
	ENDIF
	
	IF bRun
		FORCE_UNDERMAP_FOR_ENTRY(1)
		IF iRunCarEntryFadeStage > FAKE_GARAGE_CAR_ENTER_CUTSCENE_START
			HIDE_HUD_AND_RADAR_THIS_FRAME()
		ENDIF
		SWITCH iRunCarEntryFadeStage
			CASE FAKE_GARAGE_CAR_ENTER_CUTSCENE_INIT
				globalPropertyEntryData.bWalkInSeqDone = FALSE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: iRunCarEntryFadeStage = FAKE_GARAGE_CAR_ENTER_CUTSCENE_INIT")
				iRunCarEntryFadeStage = FAKE_GARAGE_CAR_ENTER_CUTSCENE_START
				iGarageCutTimer = GET_GAME_TIMER()
				iFadeDelayTimer = 0
				DO_SCREEN_FADE_OUT(500)
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				SET_BIT(iLocalBS, LOCAL_BS_TAKEN_AWAY_CONTROL)
			BREAK
			CASE FAKE_GARAGE_CAR_ENTER_CUTSCENE_START
				IF IS_SCREEN_FADED_OUT()
					SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, PLAYER_BD_BS_ENTERING_GARAGE)
					
					IF (GET_GAME_TIMER() - iGarageCutTimer) < 10000
						FOR i = ENUM_TO_INT(VS_DRIVER) TO ENUM_TO_INT(VS_EXTRA_RIGHT_3)
							IF i < GET_VEHICLE_MODEL_NUMBER_OF_SEATS(GET_ENTITY_MODEL(vehPlayer))
								PED_INDEX pedSeat
								PLAYER_INDEX playerSeat
								IF NOT IS_VEHICLE_SEAT_FREE(vehPlayer, INT_TO_ENUM(VEHICLE_SEAT, i),TRUE)
									pedSeat = GET_PED_IN_VEHICLE_SEAT(vehPlayer, INT_TO_ENUM(VEHICLE_SEAT, i),TRUE)
									IF NOT IS_PED_INJURED(pedSeat)
										IF IS_PED_A_PLAYER(pedSeat)
											playerSeat = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedSeat)
											IF playerSeat != PLAYER_ID()
												IF IS_NET_PLAYER_OK(playerSeat)
													IF NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_ENTERING_GARAGE)
														CDEBUG1LN(DEBUG_SAFEHOUSE, "Waiting for player #",NATIVE_TO_INT(playerSeat), " to be ready in garage")
														RETURN FALSE
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDFOR
					ENDIF
					IF iFadeDelayTimer = 0
						iFadeDelayTimer = GET_GAME_TIMER()
					ENDIF
					IF NOT IS_BIT_SET(iLocalBS2,LOCAL_BS2_bFadeOutWarpIntoGarage)
						IF (GET_GAME_TIMER() - iFadeDelayTimer) > 250
							//IF GET_PROPERTY_SIZE_TYPE(mpProperties[iCurrentPropertyID].iIndex) != PROP_SIZE_TYPE_LARGE_APT
							//AND mpProperties[iCurrentPropertyID].iIndex != PROPERTY_LOW_APT_7
							IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
								VEHICLE_INDEX vehFadeOut
								vehFadeOut = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
								
								IF GET_PED_IN_VEHICLE_SEAT(vehFadeOut) = PLAYER_PED_ID()
									NETWORK_FADE_OUT_ENTITY(vehFadeOut, TRUE, TRUE)
									SET_VEHICLE_SIREN(vehFadeOut,FALSE)
									SET_BIT(iLocalBS2,LOCAL_BS2_bFadeOutWarpIntoGarage)
									
									SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_CAR_MADE_INVISIBLE_ON_EXTERIOR)
									CDEBUG1LN(DEBUG_SAFEHOUSE, "NETWORK_FADE_OUT_ENTITY: called from HANDLE_WARP_INTO_GARAGE()")
								ENDIF
							ELSE
								NETWORK_FADE_OUT_ENTITY(PLAYER_PED_ID(), TRUE, TRUE)
								SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_CAR_MADE_INVISIBLE_ON_EXTERIOR)
								CDEBUG1LN(DEBUG_SAFEHOUSE, "NETWORK_FADE_OUT_ENTITY: called from HANDLE_WARP_INTO_GARAGE() - PED")
								SET_BIT(iLocalBS2,LOCAL_BS2_bFadeOutWarpIntoGarage)
							ENDIF
						ELSE
							CDEBUG1LN(DEBUG_SAFEHOUSE, "Waiting iFadeDelayTimer")
							RETURN FALSE
						ENDIF
						
						//ENDIF
					ENDIF

					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: iRunCarEntryFadeStage = FAKE_GARAGE_CAR_ENTER_CUTSCENE_START")
					//NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					//SET_BIT(iLocalBS, LOCAL_BS_TAKEN_AWAY_CONTROL)

					MAKE_PLAYER_SAFE_FOR_MP_CUTSCENE(FALSE, FALSE, TRUE, TRUE)
					
					IF IS_PROPERTY_CLUBHOUSE(iCurrentPropertyID)
						SET_MULTIHEAD_SAFE(TRUE, TRUE)
					ENDIF
					
					START_MP_CUTSCENE(TRUE)
					//SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
					
					iGarageCutsceneBitset = 0
					
					iGarageCutTimer = GET_GAME_TIMER()
					
					
					iRunCarEntryFadeStage = FAKE_GARAGE_CAR_ENTER_CUTSCENE_RUNNING

					PLAY_ENTER_GARGE_OPENING_SOUND()
				ELSE
					IF NOT IS_SCREEN_FADING_OUT()
						DO_SCREEN_FADE_OUT(0)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: iRunCarEntryFadeStage = waiting for fade")
					ENDIF
				ENDIF
			BREAK
			CASE FAKE_GARAGE_CAR_ENTER_CUTSCENE_RUNNING
				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: iRunCarEntryFadeStage = FAKE_GARAGE_CAR_ENTER_CUTSCENE_RUNNING")
				MAKE_PLAYER_SAFE_FOR_MP_CUTSCENE(FALSE, FALSE, TRUE, TRUE)
				iRunCarEntryFadeStage = FAKE_GARAGE_CAR_ENTER_CUTSCENE_COMPLETE
			BREAK
			CASE FAKE_GARAGE_CAR_ENTER_CUTSCENE_COMPLETE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: iRunCarEntryFadeStage = FAKE_GARAGE_CAR_ENTER_CUTSCENE_COMPLETE")
				globalPropertyEntryData.bWalkInSeqDone = TRUE
				bCleanupFakeGarageEnter = TRUE
				iPedsInCar = 0
				RETURN TRUE
			BREAK
		ENDSWITCH
	ELSE
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: bRun = FALSE - cleaning up")
		CLEANUP_FADED_OUT_GARAGE_CAR_ENTER(TRUE)
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DO_FAKE_GARAGE_CAR_ENTER_THIS_CUTSCENE()
	INT i
	
	BOOL bRun = FALSE
	
	VEHICLE_INDEX vehPlayer
	//CDEBUG1LN(DEBUG_SAFEHOUSE, "CDM: Current Stack 3 = ",GET_CURRENT_STACK_SIZE())
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF fakeGarageCarEnterCutsceneStage <= FAKE_GARAGE_CAR_ENTER_CUTSCENE_START
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(),TRUE)
				vehPlayer = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(),TRUE)
				
				IF IS_VEHICLE_DRIVEABLE(vehPlayer)
					IF NETWORK_HAS_CONTROL_OF_ENTITY(vehPlayer)
						SET_ENTITY_CAN_BE_DAMAGED(vehPlayer,FALSE)
						g_bMadeCarInvunerableOnEntry = TRUE
						CDEBUG1LN(DEBUG_SAFEHOUSE, "SET_ENTITY_CAN_BE_DAMAGED FALSE 1 called from exterior script- VEH")	
					ENDIF
					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: vehPlayer exists and is driveable - bRun = TRUE")
					
					IF fakeGarageCarEnterCutsceneStage = FAKE_GARAGE_CAR_ENTER_CUTSCENE_INIT
						GET_MP_FAKE_GARAGE_DRIVE_DATA(fakeGarageDriveData, GET_PROPERTY_BUILDING(iCurrentPropertyID))
					ENDIF
					
					bRun = TRUE
				ENDIF
			ENDIF
		ELSE
			bRun = TRUE
		ENDIF
	ENDIF
	

	IF bRun
		FORCE_UNDERMAP_FOR_ENTRY(1)
		REPEAT NUM_VEHICLE_SEATS i
			IF DOES_ENTITY_EXIST(pedFakeGarageEnter[i])
			AND NOT IS_PED_INJURED(pedFakeGarageEnter[i])
				SET_PED_CONFIG_FLAG(pedFakeGarageEnter[i], PCF_DisableAutoEquipHelmetsInBikes, TRUE) 
				SET_PED_RESET_FLAG(pedFakeGarageEnter[i], PRF_PreventGoingIntoShuntInVehicleState, TRUE)
			ENDIF
		ENDREPEAT
		
		IF fakeGarageCarEnterCutsceneStage > FAKE_GARAGE_CAR_ENTER_CUTSCENE_START
			HIDE_HUD_AND_RADAR_THIS_FRAME()
		ENDIF
		SWITCH fakeGarageCarEnterCutsceneStage
			CASE FAKE_GARAGE_CAR_ENTER_CUTSCENE_INIT
				globalPropertyEntryData.bWalkInSeqDone = FALSE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: fakeGarageCarEnterCutsceneStage = FAKE_GARAGE_CAR_ENTER_CUTSCENE_INIT")
				
				//Reverse Check
				CLEAR_BIT(iLocalBS2,LOCAL_BS2_FakeGarageReverse)
				
				IF IS_HEADING_ACCEPTABLE_CORRECTED(GET_ENTITY_HEADING(vehPlayer), mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].locateDetails.fEnterHeading - 180.0, 45.0 #IF IS_DEBUG_BUILD,FALSE #ENDIF)	
					SET_BIT(iLocalBS2,LOCAL_BS2_FakeGarageReverse)
				ENDIF
				
				//Clones
				CREATE_CUTSCENE_VEHICLE_CLONE(vehFakeGarageEnter, vehPlayer, fakeGarageDriveData.vStartPoint, fakeGarageDriveData.vStartRotation.Z)
				SET_VEHICLE_DOORS_LOCKED(vehFakeGarageEnter,VEHICLELOCK_UNLOCKED)
				SET_VEHICLE_ON_GROUND_PROPERLY(vehFakeGarageEnter)
				SET_ENTITY_COLLISION(vehFakeGarageEnter, FALSE)
				FREEZE_ENTITY_POSITION(vehFakeGarageEnter, TRUE)
				
				VECTOR vFakeGaragePlayerCarCoords
				
				vFakeGaragePlayerCarCoords = GET_ENTITY_COORDS(vehFakeGarageEnter)
				
				fFakeVehicleOffset = vFakeGaragePlayerCarCoords.Z - fakeGarageDriveData.vStartPoint.Z
				
				SET_ENTITY_COORDS(vehFakeGarageEnter, fakeGarageDriveData.vStartPoint + <<0.0, 0.0, -10.0>>)
				
				PED_INDEX pedTemp
				MODEL_NAMES modelTemp
				iPedsInCar = 0
				
				//CDEBUG1LN(DEBUG_SAFEHOUSE, "CDM: Current Stack 4 = ",GET_CURRENT_STACK_SIZE())
				FOR i = ENUM_TO_INT(VS_DRIVER) TO ENUM_TO_INT(VS_EXTRA_RIGHT_3)
					IF i < GET_VEHICLE_MODEL_NUMBER_OF_SEATS(GET_ENTITY_MODEL(vehPlayer))
						IF NOT IS_VEHICLE_SEAT_FREE(vehPlayer, INT_TO_ENUM(VEHICLE_SEAT, i),TRUE)
							pedTemp = GET_PED_IN_VEHICLE_SEAT(vehPlayer, INT_TO_ENUM(VEHICLE_SEAT, i),TRUE)
							
							IF DOES_ENTITY_EXIST(pedTemp)
								modelTemp = GET_ENTITY_MODEL(pedTemp)
								
								IF (IS_PED_A_PLAYER(pedTemp) AND NOT IS_PED_FEMALE(pedTemp))
								OR (NOT IS_PED_A_PLAYER(pedTemp) AND IS_PED_MALE(pedTemp))
									pedFakeGarageEnter[iPedsInCar] = CREATE_PED(PEDTYPE_CIVMALE, modelTemp, GET_ENTITY_COORDS(pedTemp) + <<0.0, 0.0, -10.0>>, GET_ENTITY_HEADING(pedTemp), FALSE, FALSE)
								ELSE
									pedFakeGarageEnter[iPedsInCar] = CREATE_PED(PEDTYPE_CIVFEMALE, modelTemp, GET_ENTITY_COORDS(pedTemp) + <<0.0, 0.0, -10.0>>, GET_ENTITY_HEADING(pedTemp), FALSE, FALSE)
								ENDIF
								CLONE_PED_TO_TARGET(pedTemp, pedFakeGarageEnter[iPedsInCar])
								SET_PED_CONFIG_FLAG(pedFakeGarageEnter[iPedsInCar], PCF_DisableAutoEquipHelmetsInBikes, TRUE) 	
								PRINTLN("DO_FAKE_GARAGE_CAR_ENTER_THIS_CUTSCENE: creating a ped clone for seat #",i)
								FREEZE_ENTITY_POSITION(pedFakeGarageEnter[iPedsInCar],TRUE)
								TASK_ENTER_VEHICLE(pedFakeGarageEnter[iPedsInCar], vehFakeGarageEnter, 1, INT_TO_ENUM(VEHICLE_SEAT, i), DEFAULT, ECF_WARP_PED)
								iPedsInCar++
							ENDIF
						ENDIF
					ENDIF
				ENDFOR
				IF IS_VEHICLE_DRIVEABLE(vehFakeGarageEnter)
					SET_VEHICLE_LIGHTS(vehFakeGarageEnter, SET_VEHICLE_LIGHTS_ON)
					SET_VEHICLE_SIREN(vehFakeGarageEnter,FALSE)
					INT bLightsOn, bHighBeamOn
					IF GET_VEHICLE_LIGHTS_STATE(vehPlayer,bLightsOn, bHighBeamOn)
						IF bHighBeamOn > 0
							SET_VEHICLE_FULLBEAM(vehFakeGarageEnter, TRUE)
							CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT:  setting lights on full beam driving in.")
						ENDIF
					ENDIF
				ENDIF
			
				
				//CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				
				fakeGarageCarEnterCutsceneStage = FAKE_GARAGE_CAR_ENTER_CUTSCENE_START
				iGarageCutTimer = GET_GAME_TIMER()
				iFadeDelayTimer = 0
			BREAK
			CASE FAKE_GARAGE_CAR_ENTER_CUTSCENE_START
				IF (GET_GAME_TIMER() - iGarageCutTimer) < 5000
					REPEAT iPedsInCar i
						IF DOES_ENTITY_EXIST(pedFakeGarageEnter[i])
							IF NOT IS_PED_INJURED(pedFakeGarageEnter[i])
								IF NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedFakeGarageEnter[i])
									CDEBUG1LN(DEBUG_SAFEHOUSE, "Waiting on streaming for ped #",i )
									RETURN FALSE
								ENDIF
								IF NOT HAS_PED_HEAD_BLEND_FINISHED(pedFakeGarageEnter[i])
									CDEBUG1LN(DEBUG_SAFEHOUSE, "Waiting on head blend for ped #",i )
									RETURN FALSE
								ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "Waiting on streaming for ped BYPASSED for timer.")
				ENDIF
				SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, PLAYER_BD_BS_ENTERING_GARAGE)
				IF (GET_GAME_TIMER() - iGarageCutTimer) < 10000
					FOR i = ENUM_TO_INT(VS_DRIVER) TO ENUM_TO_INT(VS_EXTRA_RIGHT_3)
						IF i < GET_VEHICLE_MODEL_NUMBER_OF_SEATS(GET_ENTITY_MODEL(vehPlayer))
							PED_INDEX pedSeat
							PLAYER_INDEX playerSeat
							IF NOT IS_VEHICLE_SEAT_FREE(vehPlayer, INT_TO_ENUM(VEHICLE_SEAT, i),TRUE)
								pedSeat = GET_PED_IN_VEHICLE_SEAT(vehPlayer, INT_TO_ENUM(VEHICLE_SEAT, i),TRUE)
								IF NOT IS_PED_INJURED(pedSeat)
									IF IS_PED_A_PLAYER(pedSeat)
										playerSeat = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedSeat)
										IF playerSeat != PLAYER_ID()
											IF IS_NET_PLAYER_OK(playerSeat)
												IF NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_ENTERING_GARAGE)
													CDEBUG1LN(DEBUG_SAFEHOUSE, "Waiting for player #",NATIVE_TO_INT(playerSeat), " to be ready in garage")
													RETURN FALSE
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDFOR
				ENDIF
				IF iFadeDelayTimer = 0
					iFadeDelayTimer = GET_GAME_TIMER()
				ENDIF
				IF NOT IS_BIT_SET(iLocalBS2,LOCAL_BS2_bFadeOutWarpIntoGarage)
					IF (GET_GAME_TIMER() - iFadeDelayTimer) > 250
						//IF GET_PROPERTY_SIZE_TYPE(mpProperties[iCurrentPropertyID].iIndex) != PROP_SIZE_TYPE_LARGE_APT
						//AND mpProperties[iCurrentPropertyID].iIndex != PROPERTY_LOW_APT_7
						IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
							VEHICLE_INDEX vehFadeOut
							vehFadeOut = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
							
							IF GET_PED_IN_VEHICLE_SEAT(vehFadeOut) = PLAYER_PED_ID()
								NETWORK_FADE_OUT_ENTITY(vehFadeOut, TRUE, TRUE)
								SET_VEHICLE_SIREN(vehFadeOut,FALSE)
								SET_BIT(iLocalBS2,LOCAL_BS2_bFadeOutWarpIntoGarage)
								
								SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_CAR_MADE_INVISIBLE_ON_EXTERIOR)
								CDEBUG1LN(DEBUG_SAFEHOUSE, "NETWORK_FADE_OUT_ENTITY: called from HANDLE_WARP_INTO_GARAGE()")
							ENDIF
						ELSE
							NETWORK_FADE_OUT_ENTITY(PLAYER_PED_ID(), TRUE, TRUE)
							SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_CAR_MADE_INVISIBLE_ON_EXTERIOR)
							CDEBUG1LN(DEBUG_SAFEHOUSE, "NETWORK_FADE_OUT_ENTITY: called from HANDLE_WARP_INTO_GARAGE() - PED")
							SET_BIT(iLocalBS2,LOCAL_BS2_bFadeOutWarpIntoGarage)
						ENDIF
					ELSE
						CDEBUG1LN(DEBUG_SAFEHOUSE, "Waiting iFadeDelayTimer")
						RETURN FALSE
					ENDIF
					
					//ENDIF
				ENDIF

				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: fakeGarageCarEnterCutsceneStage = FAKE_GARAGE_CAR_ENTER_CUTSCENE_START")
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				SET_BIT(iLocalBS, LOCAL_BS_TAKEN_AWAY_CONTROL)
				//NETWORK_SET_IN_MP_CUTSCENE(TRUE, TRUE)
				//SET_LOCAL_PLAYER_VISIBLE_IN_CUTSCENE(TRUE,FALSE)
				//SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
				MAKE_PLAYER_SAFE_FOR_MP_CUTSCENE(FALSE, FALSE, TRUE, TRUE)
				
				IF IS_PROPERTY_CLUBHOUSE(iCurrentPropertyID)
					SET_MULTIHEAD_SAFE(TRUE, TRUE)
				ENDIF
				
				START_MP_CUTSCENE(TRUE)
				//SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
				
				iGarageCutsceneBitset = 0
				
				iGarageCutTimer = GET_GAME_TIMER()
				
				//Create Camera
				IF DOES_CAM_EXIST(camGarageCutscene0)
					DESTROY_CAM(camGarageCutscene0)
				ENDIF
				
				camGarageCutscene0 = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
				
				SET_CAM_PARAMS(camGarageCutscene0, 
								fakeGarageDriveData.vCamCoord1,
								fakeGarageDriveData.vCamRot1,
								fakeGarageDriveData.fCamFov1)
				
				IF NOT ARE_VECTORS_EQUAL(fakeGarageDriveData.vCamCoord2, <<0.0, 0.0, 0.0>>)
					SET_CAM_PARAMS(camGarageCutscene0, 
									fakeGarageDriveData.vCamCoord2,
									fakeGarageDriveData.vCamRot2,
									fakeGarageDriveData.fCamFov2,
									fakeGarageDriveData.iCamDuration + 2000,
									fakeGarageDriveData.camGraphType,
									fakeGarageDriveData.camGraphType)
				ENDIF
				SET_CAM_FAR_CLIP(camGarageCutscene0,1000)
				SHAKE_CAM(camGarageCutscene0, "HAND_SHAKE", fakeGarageDriveData.fCamShake)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				fakeGarageCarEnterCutsceneStage = FAKE_GARAGE_CAR_ENTER_CUTSCENE_RUNNING
				IF DOES_ENTITY_EXIST(vehFakeGarageEnter)
					IF IS_VEHICLE_DRIVEABLE(vehFakeGarageEnter)
						SET_ENTITY_COORDS_NO_OFFSET(vehFakeGarageEnter, GET_INTERP_POINT_VECTOR(fakeGarageDriveData.vStartPoint + <<0.0, 0.0, fFakeVehicleOffset>>, fakeGarageDriveData.vEndPoint + <<0.0, 0.0, fFakeVehicleOffset>>, TO_FLOAT(iGarageCutTimer), TO_FLOAT(iGarageCutTimer) +  fakeGarageDriveData.fDuration, CLAMP(TO_FLOAT(GET_GAME_TIMER()), TO_FLOAT(iGarageCutTimer), TO_FLOAT(iGarageCutTimer) +  fakeGarageDriveData.fDuration)))
						IF IS_BIT_SET(iLocalBS2,LOCAL_BS2_FakeGarageReverse) AND NOT IS_PED_ON_ANY_BIKE(PLAYER_PED_ID())
							SET_ENTITY_ROTATION(vehFakeGarageEnter, GET_INTERP_POINT_VECTOR(<<0.0 - fakeGarageDriveData.vStartRotation.X, 0.0 - fakeGarageDriveData.vStartRotation.Y, fakeGarageDriveData.vStartRotation.Z + 180.0>>, <<0.0 - fakeGarageDriveData.vEndRotation.X, 0.0 - fakeGarageDriveData.vEndRotation.Y, fakeGarageDriveData.vEndRotation.Z + 180.0>>, TO_FLOAT(iGarageCutTimer), TO_FLOAT(iGarageCutTimer) +  fakeGarageDriveData.fDuration, CLAMP(TO_FLOAT(GET_GAME_TIMER()), TO_FLOAT(iGarageCutTimer), TO_FLOAT(iGarageCutTimer) +  fakeGarageDriveData.fDuration)))
						ELSE
							SET_ENTITY_ROTATION(vehFakeGarageEnter, GET_INTERP_POINT_VECTOR(fakeGarageDriveData.vStartRotation, fakeGarageDriveData.vEndRotation, TO_FLOAT(iGarageCutTimer), TO_FLOAT(iGarageCutTimer) +  fakeGarageDriveData.fDuration, CLAMP(TO_FLOAT(GET_GAME_TIMER()), TO_FLOAT(iGarageCutTimer), TO_FLOAT(iGarageCutTimer) +  fakeGarageDriveData.fDuration)))
						ENDIF
						SET_ENTITY_COLLISION(vehFakeGarageEnter, FALSE)
						FREEZE_ENTITY_POSITION(vehFakeGarageEnter, TRUE)
					ENDIF
				ENDIF
				IF IS_PROPERTY_CLUBHOUSE(iCurrentPropertyID)
					IF NOT IS_AUDIO_SCENE_ACTIVE("DLC_Biker_Clubhouse_Enter_In_Vehicle_Scene")
						START_AUDIO_SCENE("DLC_Biker_Clubhouse_Enter_In_Vehicle_Scene")	
					ENDIF
				ENDIF
				PLAY_ENTER_GARGE_OPENING_SOUND()
				
			BREAK
			CASE FAKE_GARAGE_CAR_ENTER_CUTSCENE_RUNNING
				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: fakeGarageCarEnterCutsceneStage = FAKE_GARAGE_CAR_ENTER_CUTSCENE_RUNNING")
				
				IF NOT IS_CAM_RENDERING(camGarageCutscene0)
					MAKE_PLAYER_SAFE_FOR_MP_CUTSCENE(FALSE, FALSE, TRUE, TRUE)
					
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
				ENDIF
				IF DOES_ENTITY_EXIST(vehFakeGarageEnter)
					IF IS_VEHICLE_DRIVEABLE(vehFakeGarageEnter)
						SET_ENTITY_COORDS_NO_OFFSET(vehFakeGarageEnter, GET_INTERP_POINT_VECTOR(fakeGarageDriveData.vStartPoint + <<0.0, 0.0, fFakeVehicleOffset>>, fakeGarageDriveData.vEndPoint + <<0.0, 0.0, fFakeVehicleOffset>>, TO_FLOAT(iGarageCutTimer), TO_FLOAT(iGarageCutTimer) +  fakeGarageDriveData.fDuration, CLAMP(TO_FLOAT(GET_GAME_TIMER()), TO_FLOAT(iGarageCutTimer), TO_FLOAT(iGarageCutTimer) +  fakeGarageDriveData.fDuration)))
						IF IS_BIT_SET(iLocalBS2,LOCAL_BS2_FakeGarageReverse) AND NOT IS_PED_ON_ANY_BIKE(PLAYER_PED_ID())
							SET_ENTITY_ROTATION(vehFakeGarageEnter, GET_INTERP_POINT_VECTOR(<<0.0 - fakeGarageDriveData.vStartRotation.X, 0.0 - fakeGarageDriveData.vStartRotation.Y, fakeGarageDriveData.vStartRotation.Z + 180.0>>, <<0.0 - fakeGarageDriveData.vEndRotation.X, 0.0 - fakeGarageDriveData.vEndRotation.Y, fakeGarageDriveData.vEndRotation.Z + 180.0>>, TO_FLOAT(iGarageCutTimer), TO_FLOAT(iGarageCutTimer) +  fakeGarageDriveData.fDuration, CLAMP(TO_FLOAT(GET_GAME_TIMER()), TO_FLOAT(iGarageCutTimer), TO_FLOAT(iGarageCutTimer) +  fakeGarageDriveData.fDuration)))
						ELSE
							SET_ENTITY_ROTATION(vehFakeGarageEnter, GET_INTERP_POINT_VECTOR(fakeGarageDriveData.vStartRotation, fakeGarageDriveData.vEndRotation, TO_FLOAT(iGarageCutTimer), TO_FLOAT(iGarageCutTimer) +  fakeGarageDriveData.fDuration, CLAMP(TO_FLOAT(GET_GAME_TIMER()), TO_FLOAT(iGarageCutTimer), TO_FLOAT(iGarageCutTimer) +  fakeGarageDriveData.fDuration)))
						ENDIF
						SET_ENTITY_COLLISION(vehFakeGarageEnter, FALSE)
						FREEZE_ENTITY_POSITION(vehFakeGarageEnter, TRUE)
					ENDIF
				ENDIF
				
				
				IF TO_FLOAT(GET_GAME_TIMER()) > TO_FLOAT(iGarageCutTimer) + fakeGarageDriveData.fDuration
				AND TO_FLOAT(GET_GAME_TIMER()) > TO_FLOAT(iGarageCutTimer + fakeGarageDriveData.iCamDuration + 2600) // + 1000 for bug 1635631
					fakeGarageCarEnterCutsceneStage = FAKE_GARAGE_CAR_ENTER_CUTSCENE_COMPLETE
				ENDIF
			BREAK
			CASE FAKE_GARAGE_CAR_ENTER_CUTSCENE_COMPLETE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: fakeGarageCarEnterCutsceneStage = FAKE_GARAGE_CAR_ENTER_CUTSCENE_COMPLETE")
				globalPropertyEntryData.bWalkInSeqDone = TRUE
				bCleanupFakeGarageEnter = TRUE
				IF DOES_ENTITY_EXIST(vehCrateGarageEnter)
					DELETE_OBJECT(vehCrateGarageEnter)
				ENDIF
				IF DOES_ENTITY_EXIST(vehFakeGarageEnter)
					DELETE_VEHICLE(vehFakeGarageEnter)
				ENDIF
				
				REPEAT COUNT_OF(pedFakeGarageEnter) i
					IF DOES_ENTITY_EXIST(pedFakeGarageEnter[i])
						DELETE_PED(pedFakeGarageEnter[i])
					ENDIF
				ENDREPEAT
				iPedsInCar = 0
				RETURN TRUE
			BREAK
		ENDSWITCH
	ELSE
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: bRun = FALSE - cleaning up")
		CLEANUP_FAKE_GARAGE_CAR_ENTER_THIS_CUTSCENE(TRUE)
	ENDIF
	
	RETURN FALSE
ENDFUNC



PROC RESET_GARAGE_CAR_ENTER_CUTSCENE(INT iReason, BOOL bClearTasks = FALSE, BOOL bTurnOffRenderScriptCams = TRUE)

	CDEBUG1LN(DEBUG_SAFEHOUSE, "=== CUTSCENE === RESET_GARAGE_CAR_ENTER_CUTSCENE(",iReason,")")
	IF iReason != 0
	
	ENDIF
	IF g_bPropInteriorScriptRunning
	OR IS_BIT_SET(mpPropMaintain.iBS, MP_PROP_MAINTAIN_BS_LAUNCHED_INT_SCRIPT)
		globalPropertyEntryData.bLeftOrRejectedBuzzer = TRUE
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: globalPropertyEntryData.bLeftOrRejectedBuzzer = TRUE. RESET_GARAGE_CAR_ENTER_CUTSCENE")
	ENDIF
	RESET_GARAGE_CUTSCENE(bClearTasks, bTurnOffRenderScriptCams)
	
	iFadeDelayTimer = 0
	DELETE_CLONED_ENTITIES_AND_CLEAR_REQUESTS(2)
//	#IF IS_DEBUG_BUILD
//		bDebugLaunchGarageCarEnterCutscene = FALSE
//		bDebugSeenGarageCarEnterCutscene = FALSE
//	#ENDIF
	garageCarEnterCutStage = GARAGE_CAR_ENTER_CUTSCENE_INIT
	
ENDPROC

FUNC BOOL IS_VEHICLE_ROUGHLY_FACING_THIS_DIRECTION(VEHICLE_INDEX veh, FLOAT idealHeading, FLOAT acceptableRange = 30.0)
	FLOAT upperLimit, lowerLimit
	upperLimit = idealHeading + (acceptableRange/2)
	IF upperLimit > 360
		upperLimit -= 360.0
	ENDIF
	
	lowerLimit = idealHeading - (acceptableRange/2)
	IF lowerLimit < 0
		lowerLimit += 360.0
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(veh)
		IF upperLimit > lowerLimit
			IF GET_ENTITY_HEADING(veh) < upperLimit
			AND GET_ENTITY_HEADING(veh) > lowerLimit
				RETURN TRUE
			ELSE
				RETURN FALSE
			ENDIF
		ELSE
			IF GET_ENTITY_HEADING(veh) < upperLimit
			OR GET_ENTITY_HEADING(veh) > lowerLimit
				RETURN TRUE
			ELSE
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

BOOL bWarpVeh

PROC APPLY_DRIVING_INTO_GARAGE_DECORATOR(VEHICLE_INDEX theVeh, INT ID = -1)
	INT iDecoratorValue
	IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
		IF DECOR_EXIST_ON(theVeh, "MPBitset")	
			iDecoratorValue = DECOR_GET_INT(theVeh, "MPBitset")
		ENDIF
		IF NOT IS_BIT_SET(iDecoratorValue, MP_DECORATOR_BS_ENTERING_INTO_GARAGE)	
			SET_BIT(iDecoratorValue, MP_DECORATOR_BS_ENTERING_INTO_GARAGE)		
			DECOR_SET_INT(theVeh, "MPBitset", iDecoratorValue)
			PRINTLN("AM_MP_PROPERTY_EXT: Applying entering garage decorator to vehicle")
			SET_BIT(iLocalBS2,LOCAL_BS2_AddedEnteringDecorator)
		ENDIF
		IF ID != -1
			PRINTLN("AM_MP_PROPERTY_EXT: APPLY_DRIVING_INTO_GARAGE_DECORATOR # ",ID)
		ENDIF
	ENDIF
ENDPROC

PROC REMOVE_DRIVING_INTO_GARAGE_DECORATOR(VEHICLE_INDEX theVeh, INT ID = -1)
	INT iDecoratorValue
	IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
		IF DECOR_EXIST_ON(theVeh, "MPBitset")	
			iDecoratorValue = DECOR_GET_INT(theVeh, "MPBitset")
		ENDIF
		IF IS_BIT_SET(iLocalBS2,LOCAL_BS2_AddedEnteringDecorator)
			PRINTLN("AM_MP_PROPERTY_EXT: Removing entering garage decorator from vehicle")
			CLEAR_BIT(iDecoratorValue, MP_DECORATOR_BS_ENTERING_INTO_GARAGE)
			DECOR_SET_INT(theVeh, "MPBitset", iDecoratorValue)
			IF GET_ENTITY_MODEL(theVeh) = DELUXO
			OR GET_ENTITY_MODEL(theVeh) = OPPRESSOR2
				SET_DISABLE_HOVER_MODE_FLIGHT(theVeh,FALSE)
			ENDIF
		ELSE
			PRINTLN("AM_MP_PROPERTY_EXT: NOT Removing entering garage decorator from vehicle not set by this script")
		ENDIF
		IF ID != -1
			PRINTLN("AM_MP_PROPERTY_EXT: REMOVE_DRIVING_INTO_GARAGE_DECORATOR # ",ID)
		ENDIF
	ENDIF
ENDPROC

//STRUCT DO_GARAGE_CAR_ENTER_THIS_CUTSCENE_STUCT started putting this in left in case I end up needed to do this
//	VEHICLE_INDEX thisVehicle
//	BOOL bFadeWaitingForPlayers
//	PED_INDEX pedTemp
//	PED_INDEX pedTemp2
//	MODEL_NAMES modelTemp
//	INT i
//	INT iLoopPeds
//	INT iMissionVehicleID = -1
//	PED_INDEX pedSeat
//	PLAYER_INDEX playerSeat
//	INT bLightsOn
//	INT bHighBeamOn
//	VEHICLE_INDEX vehPlayer
//	VECTOR V1
//	VECTOR V2
//	VECTOR vDiff	
//	VECTOR vFrontTopLeft
//	VECTOR vFrontTopRight
//	VECTOR vBackTopLeft
//	VECTOR vBackTopRight	
//	VECTOR vPos0
//	VECTOR vPos1
//ENDSTRUCT

FUNC BOOL DO_GARAGE_CAR_ENTER_THIS_CUTSCENE(BOOL bResetCoords, STRUCT_net_realty_3_SCENE &scene)
	VEHICLE_INDEX thisVehicle
	BOOL bFadeWaitingForPlayers
	PED_INDEX pedTemp, pedTemp2
	MODEL_NAMES modelTemp
	INT i
	INT iLoopPeds
	INT iMissionVehicleID = -1
	
	// -- url:bugstar:2192816 This should disable agitation for lester when entering the garage at the end of fleeca - scope out. ST.
	
	IF AM_I_ON_A_HEIST()
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(),TRUE)
			IF GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) = GRANGER
				pedTemp = GET_PED_IN_VEHICLE_SEAT(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),VS_BACK_RIGHT)
				IF DOES_ENTITY_EXIST(pedTemp)
					IF IS_ENTITY_ALIVE(pedTemp)
						//IF NETWORK_HAS_CONTROL_OF_ENTITY(pedTemp)
							//IF NOT IS_AMBIENT_SPEECH_DISABLED(pedTemp)
								STOP_CURRENT_PLAYING_AMBIENT_SPEECH(pedTemp)
								CDEBUG1LN(DEBUG_SAFEHOUSE, "DO_GARAGE_CAR_ENTER_THIS_CUTSCENE - Disabling Lester's Audio")
							//ENDIF
						//ENDIF
					ENDIF
				ENDIF
				pedTemp2 = GET_PED_IN_VEHICLE_SEAT(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),VS_BACK_LEFT)
				IF DOES_ENTITY_EXIST(pedTemp2)
					IF IS_ENTITY_ALIVE(pedTemp2)
						//IF NETWORK_HAS_CONTROL_OF_ENTITY(pedTemp2)						
							//IF NOT IS_AMBIENT_SPEECH_DISABLED(pedTemp2)
								STOP_CURRENT_PLAYING_AMBIENT_SPEECH(pedTemp2)
								CDEBUG1LN(DEBUG_SAFEHOUSE, "DO_GARAGE_CAR_ENTER_THIS_CUTSCENE - Disabling Paige's Audio")
							//ENDIF
						//ENDIF
					ENDIF
				ENDIF
				FOR iLoopPeds = 0 TO GET_VEHICLE_MODEL_NUMBER_OF_SEATS(GRANGER)
					IF DOES_ENTITY_EXIST(pedFakeGarageEnter[iLoopPeds])
						IF IS_ENTITY_ALIVE(pedFakeGarageEnter[iLoopPeds])
							STOP_CURRENT_PLAYING_AMBIENT_SPEECH(pedFakeGarageEnter[iLoopPeds])
							DISABLE_PED_PAIN_AUDIO(pedFakeGarageEnter[iLoopPeds], TRUE)
							CDEBUG1LN(DEBUG_SAFEHOUSE, "DO_GARAGE_CAR_ENTER_THIS_CUTSCENE - Disabling Audio for cloned ped in vehicle seat ", iLoopPeds)
						ENDIF
					ENDIF
				ENDFOR
			ENDIF
		ENDIF
	ENDIF
	// --
	
	IF garageCarEnterCutStage > GARAGE_CAR_ENTER_CUTSCENE_TRIGGER_CUT
		SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_READY_FOR_VEHICLE_FADE)
		IF iFadeDelayTimer != 0
			IF (GET_GAME_TIMER() - iFadeDelayTimer) > 250
				IF NOT IS_BIT_SET(iLocalBS2,LOCAL_BS2_bFadeOutWarpIntoGarage)
					
					
					//IF GET_PROPERTY_SIZE_TYPE(mpProperties[iCurrentPropertyID].iIndex) != PROP_SIZE_TYPE_LARGE_APT
					//AND mpProperties[iCurrentPropertyID].iIndex != PROPERTY_LOW_APT_7
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(),TRUE)
						thisVehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(),TRUE)
						IF IS_VEHICLE_DRIVEABLE(thisVehicle)
							IF GET_PED_IN_VEHICLE_SEAT(thisVehicle) = PLAYER_PED_ID()
								FOR i = ENUM_TO_INT(VS_DRIVER) TO ENUM_TO_INT(VS_EXTRA_RIGHT_3)
									IF i < GET_VEHICLE_MODEL_NUMBER_OF_SEATS(GET_ENTITY_MODEL(thisVehicle))
										PED_INDEX pedSeat
										PLAYER_INDEX playerSeat
										IF NOT IS_VEHICLE_SEAT_FREE(thisVehicle, INT_TO_ENUM(VEHICLE_SEAT, i),TRUE)
											pedSeat = GET_PED_IN_VEHICLE_SEAT(thisVehicle, INT_TO_ENUM(VEHICLE_SEAT, i),TRUE)
											IF NOT IS_PED_INJURED(pedSeat)
												IF IS_PED_A_PLAYER(pedSeat)
													playerSeat = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedSeat)
													IF playerSeat != PLAYER_ID()
														IF IS_NET_PLAYER_OK(playerSeat)
															IF NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_READY_FOR_VEHICLE_FADE)
																CDEBUG1LN(DEBUG_SAFEHOUSE, "Waiting for player #",NATIVE_TO_INT(playerSeat), " to be ready for fade- 2")
																bFadeWaitingForPlayers = TRUE
															ENDIF
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDFOR
								IF NOT bFadeWaitingForPlayers
									IF NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_DOING_HEIST_DROPOFF)
										NETWORK_FADE_OUT_ENTITY(thisVehicle, TRUE, TRUE)
										SET_VEHICLE_SIREN(thisVehicle,FALSE)
										SET_BIT(iLocalBS2,LOCAL_BS2_bFadeOutWarpIntoGarage)
										SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_CAR_MADE_INVISIBLE_ON_EXTERIOR)
										CDEBUG1LN(DEBUG_SAFEHOUSE, "DO_GARAGE_CAR_ENTER_THIS_CUTSCENE-NETWORK_FADE_OUT_ENTITY: called from DO_GARAGE_CAR_ENTER_THIS_CUTSCENE")
									ELSE
										CDEBUG1LN(DEBUG_SAFEHOUSE, "DO_GARAGE_CAR_ENTER_THIS_CUTSCENE-not fading out vehicle for heist dropoff")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					//ENDIF
				ENDIF
			ENDIF
		ENDIF
		FORCE_UNDERMAP_FOR_ENTRY(3)
	ELIF garageCarEnterCutStage > GARAGE_CAR_ENTER_CUTSCENE_INIT
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(),TRUE)
			thisVehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(),TRUE)
			IF IS_VEHICLE_DRIVEABLE(thisVehicle)
				IF NETWORK_HAS_CONTROL_OF_ENTITY(thisVehicle)
					SET_ENTITY_CAN_BE_DAMAGED(thisVehicle,FALSE)
					g_bMadeCarInvunerableOnEntry = TRUE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "SET_ENTITY_CAN_BE_DAMAGED FALSE 2 called from exterior script- VEH")
				ENDIF
				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: setting vehicle as undamagable")
			ENDIF
		ENDIF
	ENDIF
	
	IF garageCarEnterCutStage < GARAGE_CAR_ENTER_CUTSCENE_WAIT_FOR_OPEN_DOOR
		DISABLE_HDTEX_THIS_FRAME()
	ENDIF
	
	
//	IF mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_BUILDING_1
//		//IF DOOR_SYSTEM_GET_OPEN_RATIO(propertyDoors[2].iDoorHash) >= 0.05
//		//AND DOOR_SYSTEM_GET_OPEN_RATIO(propertyDoors[2].iDoorHash) <= 0.3
//		IF DOOR_SYSTEM_GET_AUTOMATIC_RATE(propertyDoors[2].iDoorHash) != 0.8
//			DOOR_SYSTEM_SET_AUTOMATIC_RATE(propertyDoors[2].iDoorHash,0.8,FALSE)
//			CDEBUG1LN(DEBUG_SAFEHOUSE, "CDM: setting door speed 0.8")
//		ENDIF
////		ELIF DOOR_SYSTEM_GET_OPEN_RATIO(propertyDoors[2].iDoorHash) >= 0.3
////		AND DOOR_SYSTEM_GET_OPEN_RATIO(propertyDoors[2].iDoorHash) <= 0.7
////			IF DOOR_SYSTEM_GET_AUTOMATIC_RATE(propertyDoors[2].iDoorHash) != 1
////				DOOR_SYSTEM_SET_AUTOMATIC_RATE(propertyDoors[2].iDoorHash,1,FALSE)
////				CDEBUG1LN(DEBUG_SAFEHOUSE, "CDM: setting door speed 1")
////			ENDIF
////		ELIF DOOR_SYSTEM_GET_OPEN_RATIO(propertyDoors[2].iDoorHash) >= 0.7
////		AND DOOR_SYSTEM_GET_OPEN_RATIO(propertyDoors[2].iDoorHash) <= 1
////			IF DOOR_SYSTEM_GET_AUTOMATIC_RATE(propertyDoors[2].iDoorHash) != 0.1
////				DOOR_SYSTEM_SET_AUTOMATIC_RATE(propertyDoors[2].iDoorHash,0.1,FALSE)
////				CDEBUG1LN(DEBUG_SAFEHOUSE, "CDM: setting door speed 0.1 - 2")
////			ENDIF
////		ENDIF
//	ENDIF
		
	
	IF garageCarEnterCutStage > GARAGE_CAR_ENTER_CUTSCENE_TRIGGER_CUT
		HIDE_HUD_AND_RADAR_THIS_FRAME()
	ENDIF
	
	
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		
		REPEAT NUM_VEHICLE_SEATS i
			IF DOES_ENTITY_EXIST(pedFakeGarageEnter[iPedsInCar])
				SET_PED_CONFIG_FLAG(pedFakeGarageEnter[iPedsInCar], PCF_DisableAutoEquipHelmetsInBikes, TRUE) 	
			ENDIF
		ENDREPEAT
		#IF ACTIVATE_PROPERTY_CS_DEBUG
		BOOL bRequestDoorDebug = FALSE
		IF rag_net_realty_3_scene.bEnableTool
			bRequestDoorDebug = TRUE
		ENDIF
		#ENDIF
		
		FLOAT fDistance
		//CDEBUG1LN(DEBUG_SAFEHOUSE, "CDM: Current Stack 5 = ",GET_CURRENT_STACK_SIZE())
		SWITCH garageCarEnterCutStage
		
			CASE GARAGE_CAR_ENTER_CUTSCENE_INIT
				IF bUseNewEntryAnims //GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_BUILDING_1
					CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_READY_FOR_VEHICLE_FADE)
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(),TRUE)
						thisVehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(),TRUE)
						IF IS_VEHICLE_DRIVEABLE(thisVehicle)
							CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: DO_GARAGE_CAR_ENTER_THIS_CUTSCENE = FAKE_GARAGE_CAR_ENTER_CUTSCENE_INIT")
							
							
							//Reverse Check
							CLEAR_BIT(iLocalBS2,LOCAL_BS2_FakeGarageReverse)
							
							IF IS_HEADING_ACCEPTABLE_CORRECTED(GET_ENTITY_HEADING(thisVehicle), mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].locateDetails.fEnterHeading - 180.0, 45.0 #IF IS_DEBUG_BUILD,FALSE #ENDIF)	
								SET_BIT(iLocalBS2,LOCAL_BS2_FakeGarageReverse)
							ENDIF
							
							
							
							//Clones
							CREATE_CUTSCENE_VEHICLE_CLONE(vehFakeGarageEnter, thisVehicle, GET_ENTITY_COORDS(thisVehicle)-<<0,0,20>>, GET_ENTITY_HEADING(thisVehicle))
							//CDEBUG1LN(DEBUG_SAFEHOUSE, "CDM: Current Stack 6 = ",GET_CURRENT_STACK_SIZE())
							SET_VEHICLE_DOORS_LOCKED(vehFakeGarageEnter,VEHICLELOCK_UNLOCKED)
							SET_ENTITY_CAN_BE_DAMAGED(vehFakeGarageEnter,FALSE)
							//FREEZE_ENTITY_POSITION(vehFakeGarageEnter,TRUE)
							CDEBUG1LN(DEBUG_SAFEHOUSE, "DO_GARAGE_CAR_ENTER_THIS_CUTSCENE-Creating vehicle clone")
							iPedsInCar = 0
							FOR i = ENUM_TO_INT(VS_DRIVER) TO ENUM_TO_INT(VS_EXTRA_RIGHT_3)
								IF i < GET_VEHICLE_MODEL_NUMBER_OF_SEATS(GET_ENTITY_MODEL(thisVehicle))
									IF NOT IS_VEHICLE_SEAT_FREE(thisVehicle, INT_TO_ENUM(VEHICLE_SEAT, i),TRUE)
										pedTemp = GET_PED_IN_VEHICLE_SEAT(thisVehicle, INT_TO_ENUM(VEHICLE_SEAT, i),TRUE)
										
										IF DOES_ENTITY_EXIST(pedTemp)
											modelTemp = GET_ENTITY_MODEL(pedTemp)
											
											IF (IS_PED_A_PLAYER(pedTemp) AND NOT IS_PED_FEMALE(pedTemp))
											OR (NOT IS_PED_A_PLAYER(pedTemp) AND IS_PED_MALE(pedTemp))
												pedFakeGarageEnter[iPedsInCar] = CREATE_PED(PEDTYPE_CIVMALE, modelTemp, GET_ENTITY_COORDS(pedTemp) + <<0.0, 0.0, -10.0>>, GET_ENTITY_HEADING(pedTemp), FALSE, FALSE)
											ELSE
												pedFakeGarageEnter[iPedsInCar] = CREATE_PED(PEDTYPE_CIVFEMALE, modelTemp, GET_ENTITY_COORDS(pedTemp) + <<0.0, 0.0, -10.0>>, GET_ENTITY_HEADING(pedTemp), FALSE, FALSE)
											ENDIF
											CLONE_PED_TO_TARGET(pedTemp, pedFakeGarageEnter[iPedsInCar])
											SET_PED_CONFIG_FLAG(pedFakeGarageEnter[iPedsInCar], PCF_DisableAutoEquipHelmetsInBikes, TRUE) 	
											
											TASK_ENTER_VEHICLE(pedFakeGarageEnter[iPedsInCar], vehFakeGarageEnter, 1, INT_TO_ENUM(VEHICLE_SEAT, i), DEFAULT, ECF_WARP_PED)
											CDEBUG1LN(DEBUG_SAFEHOUSE, "DO_GARAGE_CAR_ENTER_THIS_CUTSCENE- creating ped in seat #",i, " ped # ", iPedsIncar)
											iPedsInCar++
											
										ENDIF
									ENDIF
								ENDIF
							ENDFOR
							IF IS_VEHICLE_DRIVEABLE(vehFakeGarageEnter)
								SET_VEHICLE_LIGHTS(vehFakeGarageEnter, SET_VEHICLE_LIGHTS_ON)
								SET_VEHICLE_SIREN(vehFakeGarageEnter,FALSE)
								INT bLightsOn, bHighBeamOn
								IF GET_VEHICLE_LIGHTS_STATE(thisVehicle,bLightsOn, bHighBeamOn)
									IF bHighBeamOn > 0
										SET_VEHICLE_FULLBEAM(vehFakeGarageEnter, TRUE)
										CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT:  setting lights on full beam driving in.")
									ENDIF
								ENDIF
							ENDIF
							
							garageCarEnterCutStage = GARAGE_CAR_ENTER_CUTSCENE_LOAD_CUT
							iGarageCutTimer = GET_GAME_TIMER()
							CDEBUG1LN(DEBUG_SAFEHOUSE, "DO_GARAGE_CAR_ENTER_THIS_CUTSCENE- iGarageCutTimer = ", iGarageCutTimer)
							iFadeDelayTimer = 0
							
							// url:bugstar:2194315
							START_AUDIO_SCENE("DLC_MPHEIST_DRIVE_INTO_GARAGE_SCENE")
							
							// url:bugstar:2124223 - End of Scope out - Radio just cuts out on building pan
							IF warpInControl.iStage = PROP_ENTER_STAGE_FAKE_CUTSCENE_HEIST
								TRIGGER_MUSIC_EVENT("MP_MC_RADIO_FADE")
							ENDIF
						ENDIF
					ELSE
						RESET_GARAGE_CAR_ENTER_CUTSCENE(12,TRUE)
						RESET_TO_ENTRANCE_TO_PROPERTY_STAGE()
						
						CDEBUG1LN(DEBUG_SAFEHOUSE, "DO_GARAGE_CAR_ENTER_THIS_CUTSCENE: aborting player not in vehicle")
					ENDIF
				ELSE
				
				garageCarEnterCutStage = GARAGE_CAR_ENTER_CUTSCENE_TRIGGER_CUT
				
				ENDIF
			BREAK
			
				// url:bugstar:2163452 - Scope Out - Driving into garage at Del Perro Heights results in a pop of the player car
				CASE GARAGE_CAR_ENTER_CUTSCENE_LOAD_CUT
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(),TRUE)
						thisVehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(),TRUE)
						IF IS_VEHICLE_DRIVEABLE(thisVehicle)
							IF DECOR_IS_REGISTERED_AS_TYPE("Heist_Veh_ID",DECOR_TYPE_INT)
								IF DECOR_EXIST_ON(thisVehicle,"Heist_Veh_ID")
									iMissionVehicleID = DECOR_GET_INT(thisVehicle,"Heist_Veh_ID")
								ENDIF
							ENDIF
						ENDIF
					ELSE
						RESET_GARAGE_CAR_ENTER_CUTSCENE(13,TRUE)
						RESET_TO_ENTRANCE_TO_PROPERTY_STAGE()
						
						CDEBUG1LN(DEBUG_SAFEHOUSE, "DO_GARAGE_CAR_ENTER_THIS_CUTSCENE: aborting player not in vehicle")
					ENDIF
					
					IF iMissionVehicleID >= 0
						modelTemp = GET_VEHICLE_CRATE_MODEL_RC(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iMissionVehicleID].iVehBitSet, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iMissionVehicleID].iVehBitsetTwo, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iMissionVehicleID].iVehBitsetSeven, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iMissionVehicleID].mn)
						IF modelTemp != DUMMY_MODEL_FOR_SCRIPT
							IF NOT FMMC_SET_THIS_VEHICLE_CRATES_ROWAN_C_NON_NETWORKED(vehFakeGarageEnter,g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iMissionVehicleID].iVehBitSet,g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iMissionVehicleID].iVehBitsetTwo,g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iMissionVehicleID].iVehBitsetSeven,vehCrateGarageEnter)
								CDEBUG1LN(DEBUG_SAFEHOUSE, "DO_GARAGE_CAR_ENTER_THIS_CUTSCENE- FMMC_SET_THIS_VEHICLE_CRATES_ROWAN_C waiting for crate to be created.")
								RETURN FALSE
							ENDIF
						ENDIF
					ENDIF
					IF (GET_GAME_TIMER() - iGarageCutTimer) < 5000
						REPEAT iPedsInCar i
							IF DOES_ENTITY_EXIST(pedFakeGarageEnter[i])
								IF NOT IS_PED_INJURED(pedFakeGarageEnter[i])
									IF NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedFakeGarageEnter[i])
										CDEBUG1LN(DEBUG_SAFEHOUSE, "DO_GARAGE_CAR_ENTER_THIS_CUTSCENE-GARAGE_CAR_ENTER_CUTSCENE_TRIGGER_CUT Waiting on streaming for ped #",i )
										RETURN FALSE
									ENDIF
									IF NOT HAS_PED_HEAD_BLEND_FINISHED(pedFakeGarageEnter[i])
										CDEBUG1LN(DEBUG_SAFEHOUSE, "DO_GARAGE_CAR_ENTER_THIS_CUTSCENE-GARAGE_CAR_ENTER_CUTSCENE_TRIGGER_CUT Waiting on head blend for ped #",i )
										RETURN FALSE
									ENDIF
								ENDIF
							ENDIF
						ENDREPEAT
					ENDIF
					//CDEBUG1LN(DEBUG_SAFEHOUSE, "CDM: Current Stack 7 = ",GET_CURRENT_STACK_SIZE())
					SET_LOCAL_PLAYER_VISIBLE_IN_CUTSCENE(FALSE, TRUE)
					NETWORK_SET_IN_MP_CUTSCENE(TRUE,TRUE)
					garageCarEnterCutStage = GARAGE_CAR_ENTER_CUTSCENE_TRIGGER_CUT
				FALLTHRU

			CASE GARAGE_CAR_ENTER_CUTSCENE_TRIGGER_CUT
				IF bUseNewEntryAnims //GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_BUILDING_1
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(),TRUE)
						thisVehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(),TRUE)
						IF IS_VEHICLE_DRIVEABLE(thisVehicle)
							IF DECOR_IS_REGISTERED_AS_TYPE("Heist_Veh_ID",DECOR_TYPE_INT)
								IF DECOR_EXIST_ON(thisVehicle,"Heist_Veh_ID")
									iMissionVehicleID = DECOR_GET_INT(thisVehicle,"Heist_Veh_ID")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				
					SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, PLAYER_BD_BS_ENTERING_GARAGE)
					IF (GET_GAME_TIMER() - iGarageCutTimer) < 10000
						FOR i = ENUM_TO_INT(VS_DRIVER) TO ENUM_TO_INT(VS_EXTRA_RIGHT_3)
							IF i < GET_VEHICLE_MODEL_NUMBER_OF_SEATS(GET_ENTITY_MODEL(thisVehicle))
								PED_INDEX pedSeat
								PLAYER_INDEX playerSeat
								IF NOT IS_VEHICLE_SEAT_FREE(thisVehicle, INT_TO_ENUM(VEHICLE_SEAT, i),TRUE)
									pedSeat = GET_PED_IN_VEHICLE_SEAT(thisVehicle, INT_TO_ENUM(VEHICLE_SEAT, i),TRUE)
									IF NOT IS_PED_INJURED(pedSeat)
										IF IS_PED_A_PLAYER(pedSeat)
											playerSeat = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedSeat)
											IF playerSeat != PLAYER_ID()
												IF IS_NET_PLAYER_OK(playerSeat)
													IF NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_ENTERING_GARAGE)
														CDEBUG1LN(DEBUG_SAFEHOUSE, "Waiting for player #",NATIVE_TO_INT(playerSeat), " to be ready in garage- 2")
														RETURN FALSE
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDFOR
					ENDIF
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(),TRUE)
						thisVehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(),TRUE)
						IF IS_VEHICLE_DRIVEABLE(thisVehicle)
							IF NETWORK_HAS_CONTROL_OF_ENTITY(thisVehicle)
								SET_ENTITY_CAN_BE_DAMAGED(thisVehicle,FALSE)
								g_bMadeCarInvunerableOnEntry = TRUE
								CDEBUG1LN(DEBUG_SAFEHOUSE, "SET_ENTITY_CAN_BE_DAMAGED FALSE 3 called from exterior script- VEH")
							ENDIF
							
							START_MP_CUTSCENE(TRUE)
							SET_BIT(entryCutData.iBS,ENTRY_CS_BS_STARTED_MP_CUTSCENE)
							CLEAR_BIT(iLocalBS2,LOCAL_BS2_bCarEntryAborted)
							//#1581711
	//						IF NOT IS_POSITION_OCCUPIED(
	//								scene.mPlacers[net_realty_3_SCENE_PLACER_resetCoords].vPos,
	//								2.0,
	//								FALSE, TRUE, FALSE, FALSE, FALSE, thisVehicle)
								
								
	//							IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
	//								SET_VEHICLE_LIGHTS(thisVehicle,SET_VEHICLE_LIGHTS_ON)
	//							ENDIF
								
														//playerId	bHasControl	bVisible	bClearTasks	bHasCollision	bFreezePos	
								NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
								SET_BIT(iLocalBS, LOCAL_BS_TAKEN_AWAY_CONTROL)
								iGarageCutsceneBitset = 0
								
								iGarageCutTimer = GET_GAME_TIMER()
	//							IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
									IF bResetCoords
									ENDIF
									IF IS_VEHICLE_DRIVEABLE(vehFakeGarageEnter)
										SET_ENTITY_COORDS(vehFakeGarageEnter, scene.mPlacers[net_realty_3_SCENE_PLACER_resetCoords].vPos)
										SET_ENTITY_HEADING(vehFakeGarageEnter, scene.mPlacers[net_realty_3_SCENE_PLACER_resetCoords].fRot)
										SET_VEHICLE_ON_GROUND_PROPERLY(vehFakeGarageEnter)
									ENDIF
									IF DOES_ENTITY_EXIST(fakeBuilding6GarageObj)
										SET_ENTITY_COORDS(fakeBuilding6GarageObj,(<<-878.027,-359.452,36.249>> + <<0,0,2.5>>))
										SET_ENTITY_ROTATION(fakeBuilding6GarageObj, <<0,0,26.88>>)
										FREEZE_ENTITY_POSITION(fakeBuilding6GarageObj, TRUE)
										SET_ENTITY_COLLISION(fakeBuilding6GarageObj, FALSE)	
									ENDIF		
								//	ENDIF
								//ENDIF
		//						camGarageCutscene0 = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
		//						SET_CAM_ACTIVE(camGarageCutscene0, TRUE)
		//						SET_CAM_PARAMS(camGarageCutscene0, 
		//								<<-801.0135, 332.7260, 85.4419>>,
		//								<<4.8393, -0.0098, -162.9744>>, 
		//								40.1354)
		//						SET_CAM_PARAMS(camGarageCutscene0, 
		//								<<-798.7095, 333.4185, 85.4418>>, 
		//								<<4.8393, -0.0098, -162.1292>>, 
		//								30.0228, 10000, GRAPH_TYPE_SIN_ACCEL_DECEL,GRAPH_TYPE_SIN_ACCEL_DECEL)
		//						SHAKE_CAM(camGarageCutscene0, "HAND_SHAKE", 0.25)
		//						RENDER_SCRIPT_CAMS(TRUE, FALSE)
								IF iFadeDelayTimer = 0
									iFadeDelayTimer = GET_GAME_TIMER()
								ENDIF
								
								SceneTool_ExecutePan(scene.mPans[net_realty_3_SCENE_PAN_descent], camGarageCutscene0, camGarageCutscene1)
								SET_CAM_FAR_CLIP(camGarageCutscene0,1000)
								SET_CAM_FAR_CLIP(camGarageCutscene1,1000)
								#IF IS_DEBUG_BUILD
								CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === garageCarEnterCutStage = GARAGE_CAR_ENTER_CUTSCENE_WAIT_FOR_OPEN_DOOR")
								#ENDIF
								bWarpVeh =FALSE
								
								garageCarEnterCutStage = GARAGE_CAR_ENTER_CUTSCENE_WAIT_FOR_OPEN_DOOR
								
								
								IF IS_PROPERTY_OFFICE_GARAGE(iCurrentPropertyID)
									IF NOT IS_AUDIO_SCENE_ACTIVE("DLC_IE_Garage_Elevator_Enter_Scene")
										START_AUDIO_SCENE("DLC_IE_Garage_Elevator_Enter_Scene")
										CDEBUG1LN(DEBUG_SAFEHOUSE, "=== CUTSCENE === garageCarEnterCutStage - started audio scene DLC_IE_Garage_Elevator_Enter_Scene")
									ENDIF
								ENDIF
								
								//CDEBUG1LN(DEBUG_SAFEHOUSE, "CDM: Current Stack 8 = ",GET_CURRENT_STACK_SIZE())
	//						ELSE	
	//							SET_BIT(iLocalBS2,LOCAL_BS2_bCarEntryAborted)
	//							//SCRIPT_ASSERT("area occupied!")
	//							#IF IS_DEBUG_BUILD
	//							CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === (AREA OCCUPIED) garageCarEnterCutStage = GARAGE_CAR_ENTER_CUTSCENE_COMPLETE")
	//							#ENDIF
	//							garageCarEnterCutStage = GARAGE_CAR_ENTER_CUTSCENE_COMPLETE
	//						ENDIF
						ELSE
							RESET_GARAGE_CAR_ENTER_CUTSCENE(1,TRUE)
						ENDIF
					ELSE
						IF IS_VEHICLE_DRIVEABLE(warpInControl.vehID)
							IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(),SCRIPT_TASK_ENTER_VEHICLE) != PERFORMING_TASK
							AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(),SCRIPT_TASK_ENTER_VEHICLE) != WAITING_TO_START_TASK
								TASK_ENTER_VEHICLE(PLAYER_PED_ID(),warpInControl.vehID)
							ENDIF
						ENDIF
						//RESET_GARAGE_CAR_ENTER_CUTSCENE(TRUE)
					ENDIF
				ELSE

				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(),TRUE)
					thisVehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(),TRUE)
					IF IS_VEHICLE_DRIVEABLE(thisVehicle)
						IF NETWORK_HAS_CONTROL_OF_ENTITY(thisVehicle)
							SET_ENTITY_CAN_BE_DAMAGED(thisVehicle,FALSE)
							g_bMadeCarInvunerableOnEntry = TRUE
							CDEBUG1LN(DEBUG_SAFEHOUSE, "SET_ENTITY_CAN_BE_DAMAGED FALSE 4 called from exterior script- VEH")
						ENDIF
						CLEAR_BIT(iLocalBS2,LOCAL_BS2_bCarEntryAborted)
						//#1581711
						IF NOT IS_POSITION_OCCUPIED(
								scene.mPlacers[net_realty_3_SCENE_PLACER_resetCoords].vPos,
								2.0,
								FALSE, TRUE, FALSE, FALSE, FALSE, thisVehicle)
							
							
							IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
								SET_VEHICLE_LIGHTS(thisVehicle,SET_VEHICLE_LIGHTS_ON)
								SET_VEHICLE_SIREN(thisVehicle,FALSE)
							ENDIF
							
													//playerId	bHasControl	bVisible	bClearTasks	bHasCollision	bFreezePos	
							NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
							SET_BIT(iLocalBS, LOCAL_BS_TAKEN_AWAY_CONTROL)
							iGarageCutsceneBitset = 0
							
							iGarageCutTimer = GET_GAME_TIMER()
							IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
								IF bResetCoords
									SET_ENTITY_COORDS(thisVehicle, scene.mPlacers[net_realty_3_SCENE_PLACER_resetCoords].vPos)
									SET_ENTITY_HEADING(thisVehicle, scene.mPlacers[net_realty_3_SCENE_PLACER_resetCoords].fRot)
								ENDIF
							ENDIF
	//						camGarageCutscene0 = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
	//						SET_CAM_ACTIVE(camGarageCutscene0, TRUE)
	//						SET_CAM_PARAMS(camGarageCutscene0, 
	//								<<-801.0135, 332.7260, 85.4419>>,
	//								<<4.8393, -0.0098, -162.9744>>, 
	//								40.1354)
	//						SET_CAM_PARAMS(camGarageCutscene0, 
	//								<<-798.7095, 333.4185, 85.4418>>, 
	//								<<4.8393, -0.0098, -162.1292>>, 
	//								30.0228, 10000, GRAPH_TYPE_SIN_ACCEL_DECEL,GRAPH_TYPE_SIN_ACCEL_DECEL)
	//						SHAKE_CAM(camGarageCutscene0, "HAND_SHAKE", 0.25)
	//						RENDER_SCRIPT_CAMS(TRUE, FALSE)
							SceneTool_ExecutePan(scene.mPans[net_realty_3_SCENE_PAN_descent], camGarageCutscene0, camGarageCutscene1)
							SET_CAM_FAR_CLIP(camGarageCutscene0,1000)
							SET_CAM_FAR_CLIP(camGarageCutscene1,1000)
							#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === garageCarEnterCutStage = GARAGE_CAR_ENTER_CUTSCENE_WAIT_FOR_OPEN_DOOR")
							#ENDIF
							bWarpVeh =FALSE
							
							IF IS_PROPERTY_OFFICE_GARAGE(iCurrentPropertyID)
								IF NOT IS_AUDIO_SCENE_ACTIVE("DLC_IE_Garage_Elevator_Enter_Scene")
									START_AUDIO_SCENE("DLC_IE_Garage_Elevator_Enter_Scene")
									CDEBUG1LN(DEBUG_SAFEHOUSE, "=== CUTSCENE === garageCarEnterCutStage - started audio scene DLC_IE_Garage_Elevator_Enter_Scene")
								ENDIF
							ENDIF
							
							garageCarEnterCutStage = GARAGE_CAR_ENTER_CUTSCENE_WAIT_FOR_OPEN_DOOR
							
						ELSE	
							SET_BIT(iLocalBS2,LOCAL_BS2_bCarEntryAborted)
							//SCRIPT_ASSERT("area occupied!")
							#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === (AREA OCCUPIED) garageCarEnterCutStage = GARAGE_CAR_ENTER_CUTSCENE_COMPLETE")
							#ENDIF
							garageCarEnterCutStage = GARAGE_CAR_ENTER_CUTSCENE_COMPLETE
						ENDIF
					ELSE
						RESET_GARAGE_CAR_ENTER_CUTSCENE(2,TRUE)
					ENDIF
				ELSE
					IF IS_VEHICLE_DRIVEABLE(warpInControl.vehID)
						IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(),SCRIPT_TASK_ENTER_VEHICLE) != PERFORMING_TASK
						AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(),SCRIPT_TASK_ENTER_VEHICLE) != WAITING_TO_START_TASK
							TASK_ENTER_VEHICLE(PLAYER_PED_ID(),warpInControl.vehID)
						ENDIF
					ENDIF
					//RESET_GARAGE_CAR_ENTER_CUTSCENE(TRUE)
				ENDIF
				ENDIF
			BREAK
			
			CASE GARAGE_CAR_ENTER_CUTSCENE_WAIT_FOR_OPEN_DOOR
				IF bUseNewEntryAnims //GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_BUILDING_1
					IF GET_GAME_TIMER() - iGarageCutTimer > GARAGE_CUT_SKIP_TIME_SHORT
						garageCarEnterCutStage = GARAGE_CAR_ENTER_CUTSCENE_COMPLETE
					ENDIF
					
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF IS_VEHICLE_DRIVEABLE(vehFakeGarageEnter)
							
							//IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
								REQUEST_GARAGE_DOOR_OPEN(#IF ACTIVATE_PROPERTY_CS_DEBUG bRequestDoorDebug #ENDIF)
							//ENDIF
							
							IF IS_GARAGE_DOOR_UNLOCKED()
								IF IS_GARAGE_DOOR_OPEN()

									IF NOT IS_PED_INJURED(pedFakeGarageEnter[0])
									TASK_VEHICLE_DRIVE_TO_COORD(pedFakeGarageEnter[0], vehFakeGarageEnter,
												scene.mMarkers[net_realty_3_SCENE_MARKER_gotoA].vPos, 5.0,
												DRIVINGSTYLE_STRAIGHTLINE,
												GET_ENTITY_MODEL(vehFakeGarageEnter), DRIVINGMODE_PLOUGHTHROUGH, 1.0, 100.0)
									ENDIF
									
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === garageCarEnterCutStage = GARAGE_CAR_ENTER_CUTSCENE_WAIT_FOR_INSIDE")
									#ENDIF
									
									iGarageCutTimer = GET_GAME_TIMER()
									
									// url:bugstar:2194315
									VEHICLE_INDEX vehPlayer
									vehPlayer = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
									
									IF DOES_ENTITY_EXIST(vehPlayer)
										PLAY_SOUND_FROM_ENTITY(-1, "Engine_Revs", vehPlayer, "DLC_HEISTS_GENERIC_SOUNDS")
									ENDIF
									
									garageCarEnterCutStage = GARAGE_CAR_ENTER_CUTSCENE_WAIT_FOR_INSIDE
								ELSE
									IF NOT IS_PROPERTY_OFFICE_GARAGE(iCurrentPropertyID)
										CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Waiting for door to open to drive in. Current ratio = ", DOOR_SYSTEM_GET_OPEN_RATIO(propertyDoors[2].iDoorHash))
									ENDIF
									
									IF IS_VEHICLE_DRIVEABLE(vehFakeGarageEnter)
										IF NOT bWarpVeh
											VECTOR V1, V2, vDiff
											V1 = GET_ENTITY_COORDS(vehFakeGarageEnter)
											V2 = scene.mMarkers[net_realty_3_SCENE_MARKER_gotoA].vPos
											
											vDiff = V2-V1
											
											FLOAT fOffHead
											fOffHead = GET_HEADING_FROM_VECTOR_2D(vDiff.x, vDiff.y)
											
											IF NOT IS_VEHICLE_ROUGHLY_FACING_THIS_DIRECTION(vehFakeGarageEnter, fOffHead)
												SET_ENTITY_COORDS(vehFakeGarageEnter, scene.mPlacers[net_realty_3_SCENE_PLACER_resetCoords].vPos)
												SET_ENTITY_HEADING(vehFakeGarageEnter, scene.mPlacers[net_realty_3_SCENE_PLACER_resetCoords].fRot)
												SET_VEHICLE_ON_GROUND_PROPERLY(vehFakeGarageEnter)
												CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === Player vehicle warped")
											ENDIF
											
											bWarpVeh =TRUE
										ENDIF
									ENDIF
									IF NOT IS_PED_INJURED(pedFakeGarageEnter[0])
										IF NOT IS_TASK_ONGOING(pedFakeGarageEnter[0], SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD)
											VECTOR vFrontTopLeft, vFrontTopRight, vBackTopLeft, vBackTopRight	
											DETERMINE_4_TOP_VECTORS_OF_MODEL_BOX_IN_WORLD_COORDS(vehFakeGarageEnter, GET_ENTITY_MODEL(vehFakeGarageEnter),
													vFrontTopLeft, vFrontTopRight,  vBackTopLeft, vBackTopRight)

											VECTOR vPos0, vPos1
											vPos0 = scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].vPos0 
											vPos1 = scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].vPos1
											
											IF vPos0.z > vPos1.z
												vPos0.z = vPos1.z 
											ELSE
												vPos1.z = vPos0.z 
											ENDIF
											
											IF IS_POINT_IN_ANGLED_AREA(vFrontTopLeft, 	vPos0, vPos1 + <<0,0,scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].fHeight>>, scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].fWidth)
											OR IS_POINT_IN_ANGLED_AREA(vFrontTopRight, 	vPos0, vPos1 + <<0,0,scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].fHeight>>, scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].fWidth)
											OR IS_POINT_IN_ANGLED_AREA(vBackTopLeft, 	vPos0, vPos1 + <<0,0,scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].fHeight>>, scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].fWidth)
											OR IS_POINT_IN_ANGLED_AREA(vBackTopRight, 	vPos0, vPos1 + <<0,0,scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].fHeight>>, scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].fWidth)
												TASK_VEHICLE_DRIVE_TO_COORD(pedFakeGarageEnter[0], vehFakeGarageEnter,
														scene.mMarkers[net_realty_3_SCENE_MARKER_gotoREVERSE].vPos, 5.0, DRIVINGSTYLE_REVERSING,
														GET_ENTITY_MODEL(vehFakeGarageEnter), DRIVINGMODE_PLOUGHTHROUGH, 1.0, 100.0)
												CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === Player vehicle warped")
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ELSE
								
								CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Waiting for the garage door to be unlocked")
							ENDIF
						ELSE
							RESET_GARAGE_CAR_ENTER_CUTSCENE(3,TRUE)
						ENDIF
					ELSE
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: telling ped to get into vehicle as they are not inside.")
						IF IS_VEHICLE_DRIVEABLE(warpInControl.vehID)
							IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(),SCRIPT_TASK_ENTER_VEHICLE) != PERFORMING_TASK
							AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(),SCRIPT_TASK_ENTER_VEHICLE) != WAITING_TO_START_TASK
								TASK_ENTER_VEHICLE(PLAYER_PED_ID(),warpInControl.vehID)
							ENDIF
						ENDIF
					ENDIF
				ELSE
				
				IF GET_GAME_TIMER() - iGarageCutTimer > GARAGE_CUT_SKIP_TIME_SHORT
					garageCarEnterCutStage = GARAGE_CAR_ENTER_CUTSCENE_COMPLETE
				ENDIF
				
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					thisVehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					IF IS_VEHICLE_DRIVEABLE(thisVehicle)
						
						//IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
							REQUEST_GARAGE_DOOR_OPEN(#IF ACTIVATE_PROPERTY_CS_DEBUG bRequestDoorDebug #ENDIF)
						//ENDIF
						
						IF IS_GARAGE_DOOR_UNLOCKED()
							IF IS_GARAGE_DOOR_OPEN()
							
								IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
									TASK_VEHICLE_DRIVE_TO_COORD(PLAYER_PED_ID(), thisVehicle,
											scene.mMarkers[net_realty_3_SCENE_MARKER_gotoA].vPos, 5.0,
											DRIVINGSTYLE_STRAIGHTLINE,
											GET_ENTITY_MODEL(thisVehicle), DRIVINGMODE_PLOUGHTHROUGH, 1.0, 100.0)
								ENDIF
								
								#IF IS_DEBUG_BUILD
								CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === garageCarEnterCutStage = GARAGE_CAR_ENTER_CUTSCENE_WAIT_FOR_INSIDE")
								#ENDIF
								
								iGarageCutTimer = GET_GAME_TIMER()
								
								garageCarEnterCutStage = GARAGE_CAR_ENTER_CUTSCENE_WAIT_FOR_INSIDE
							ELSE
								IF NOT IS_PROPERTY_OFFICE_GARAGE(iCurrentPropertyID)
									CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Waiting for door to open to drive in. Current ratio = ", DOOR_SYSTEM_GET_OPEN_RATIO(propertyDoors[2].iDoorHash))
								ENDIF
								IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
									IF IS_VEHICLE_DRIVEABLE(thisVehicle)
										IF NOT bWarpVeh
											VECTOR V1, V2, vDiff
											V1 = GET_ENTITY_COORDS(thisVehicle)
											V2 = scene.mMarkers[net_realty_3_SCENE_MARKER_gotoA].vPos
											
											vDiff = V2-V1
											
											FLOAT fOffHead
											fOffHead = GET_HEADING_FROM_VECTOR_2D(vDiff.x, vDiff.y)
											
											IF NOT IS_VEHICLE_ROUGHLY_FACING_THIS_DIRECTION(thisVehicle, fOffHead)
												SET_ENTITY_COORDS(thisVehicle, scene.mPlacers[net_realty_3_SCENE_PLACER_resetCoords].vPos)
												SET_ENTITY_HEADING(thisVehicle, scene.mPlacers[net_realty_3_SCENE_PLACER_resetCoords].fRot)
												CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === Player vehicle warped")
											ENDIF
											
											bWarpVeh =TRUE
										ENDIF
									ENDIF
									IF NOT IS_TASK_ONGOING(PLAYER_PED_ID(), SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD)
										VECTOR vFrontTopLeft, vFrontTopRight, vBackTopLeft, vBackTopRight	
										DETERMINE_4_TOP_VECTORS_OF_MODEL_BOX_IN_WORLD_COORDS(thisVehicle, GET_ENTITY_MODEL(thisVehicle),
												vFrontTopLeft, vFrontTopRight,  vBackTopLeft, vBackTopRight)

										VECTOR vPos0, vPos1
										vPos0 = scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].vPos0 
										vPos1 = scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].vPos1
										
										IF vPos0.z > vPos1.z
											vPos0.z = vPos1.z 
										ELSE
											vPos1.z = vPos0.z 
										ENDIF
										
										IF IS_POINT_IN_ANGLED_AREA(vFrontTopLeft, 	vPos0, vPos1 + <<0,0,scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].fHeight>>, scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].fWidth)
										OR IS_POINT_IN_ANGLED_AREA(vFrontTopRight, 	vPos0, vPos1 + <<0,0,scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].fHeight>>, scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].fWidth)
										OR IS_POINT_IN_ANGLED_AREA(vBackTopLeft, 	vPos0, vPos1 + <<0,0,scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].fHeight>>, scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].fWidth)
										OR IS_POINT_IN_ANGLED_AREA(vBackTopRight, 	vPos0, vPos1 + <<0,0,scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].fHeight>>, scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].fWidth)
											TASK_VEHICLE_DRIVE_TO_COORD(PLAYER_PED_ID(), thisVehicle,
													scene.mMarkers[net_realty_3_SCENE_MARKER_gotoREVERSE].vPos, 5.0, DRIVINGSTYLE_REVERSING,
													GET_ENTITY_MODEL(thisVehicle), DRIVINGMODE_PLOUGHTHROUGH, 1.0, 100.0)
											CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === Player vehicle warped")
										ENDIF
									ENDIF
								ENDIf
							ENDIF
						ELSE
							
							CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Waiting for the garage door to be unlocked")
						ENDIF
					ELSE
						RESET_GARAGE_CAR_ENTER_CUTSCENE(4,TRUE)
					ENDIF
				ELSE
					IF IS_VEHICLE_DRIVEABLE(warpInControl.vehID)
						IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(),SCRIPT_TASK_ENTER_VEHICLE) != PERFORMING_TASK
						AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(),SCRIPT_TASK_ENTER_VEHICLE) != WAITING_TO_START_TASK
							TASK_ENTER_VEHICLE(PLAYER_PED_ID(),warpInControl.vehID)
						ENDIF
					ENDIF
				ENDIF
				ENDIF
			BREAK
			
			CASE GARAGE_CAR_ENTER_CUTSCENE_WAIT_FOR_INSIDE
				
				IF GET_GAME_TIMER() - iGarageCutTimer > GARAGE_CUT_SKIP_TIME_LONG
					#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === garageCarEnterCutStage = GARAGE_CAR_ENTER_CUTSCENE_WAIT_FOR_INSIDE - timed out")
					#ENDIF
					garageCarEnterCutStage = GARAGE_CAR_ENTER_CUTSCENE_COMPLETE
				ENDIF
				IF bUseNewEntryAnims //GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_BUILDING_1
					IF IS_VEHICLE_DRIVEABLE(vehFakeGarageEnter)
//						VECTOR vTempTempTemp
//						PRINTLN("CDM TEMP: distance = ",VDIST2(GET_ENTITY_COORDS(vehFakeGarageEnter), scene.mMarkers[net_realty_3_SCENE_MARKER_gotoA].vPos))
//						vTempTempTemp = scene.mMarkers[net_realty_3_SCENE_MARKER_gotoA].vPos
//						PRINTLN("scene.mMarkers[net_realty_3_SCENE_MARKER_gotoA].vPos= ",vTempTempTemp)
//						vTempTempTemp = GET_ENTITY_COORDS(vehFakeGarageEnter)
//						PRINTLN("GET_ENTITY_COORDS(vehFakeGarageEnter)= ",vTempTempTemp)
						
						fDistance = 16
						IF GET_ENTITY_MODEL(vehFakeGarageEnter) = VIGILANTE
							fDistance = 24
						ENDIF
						
						IF VDIST2(GET_ENTITY_COORDS(vehFakeGarageEnter), scene.mMarkers[net_realty_3_SCENE_MARKER_gotoA].vPos) < fDistance
							
							IF NOT IS_PED_INJURED(pedFakeGarageEnter[0])
								TASK_VEHICLE_DRIVE_TO_COORD(pedFakeGarageEnter[0], vehFakeGarageEnter,
										scene.mMarkers[net_realty_3_SCENE_MARKER_gotoB].vPos, 5.0, DRIVINGSTYLE_STRAIGHTLINE,
										GET_ENTITY_MODEL(vehFakeGarageEnter), DRIVINGMODE_PLOUGHTHROUGH, 1.0, 100.0)
							ENDIF
							#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === garageCarEnterCutStage = GARAGE_CAR_ENTER_CUTSCENE_WAIT_FOR_CLOSE_DOOR")
							#ENDIF
							
							iGarageCutTimer = GET_GAME_TIMER()
							
							garageCarEnterCutStage = GARAGE_CAR_ENTER_CUTSCENE_WAIT_FOR_CLOSE_DOOR
						ENDIF
					ENDIF
				ELSE
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					thisVehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					IF IS_VEHICLE_DRIVEABLE(thisVehicle)
						
						IF VDIST2(GET_ENTITY_COORDS(thisVehicle), scene.mMarkers[net_realty_3_SCENE_MARKER_gotoA].vPos) < 16.0
							
							IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
								TASK_VEHICLE_DRIVE_TO_COORD(PLAYER_PED_ID(), thisVehicle,
										scene.mMarkers[net_realty_3_SCENE_MARKER_gotoB].vPos, 5.0, DRIVINGSTYLE_STRAIGHTLINE,
										GET_ENTITY_MODEL(thisVehicle), DRIVINGMODE_PLOUGHTHROUGH, 1.0, 100.0)
							ENDIF
							
							#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === garageCarEnterCutStage = GARAGE_CAR_ENTER_CUTSCENE_WAIT_FOR_CLOSE_DOOR")
							#ENDIF
							
							iGarageCutTimer = GET_GAME_TIMER()
							
							garageCarEnterCutStage = GARAGE_CAR_ENTER_CUTSCENE_WAIT_FOR_CLOSE_DOOR
						ENDIF
					ELSE
						garageCarEnterCutStage = GARAGE_CAR_ENTER_CUTSCENE_COMPLETE
						////RESET_GARAGE_CAR_ENTER_CUTSCENE(TRUE)
					ENDIF
				ELSE
					garageCarEnterCutStage = GARAGE_CAR_ENTER_CUTSCENE_COMPLETE
					//RESET_GARAGE_CAR_ENTER_CUTSCENE(TRUE)
				ENDIF
				ENDIF
				
			BREAK
			
			CASE GARAGE_CAR_ENTER_CUTSCENE_WAIT_FOR_CLOSE_DOOR
				
				IF GET_GAME_TIMER() - iGarageCutTimer > GARAGE_CUT_SKIP_TIME_LONG
					
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === garageCarEnterCutStage = GARAGE_CAR_ENTER_CUTSCENE_COMPLETE [time > GARAGE_CUT_SKIP_TIME_LONG]")
					#ENDIF
					
					garageCarEnterCutStage = GARAGE_CAR_ENTER_CUTSCENE_COMPLETE
				ENDIF
				
				IF bUseNewEntryAnims //GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_BUILDING_1
					IF IS_VEHICLE_DRIVEABLE(vehFakeGarageEnter)
						
						//IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
							CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: REQUEST_TO_USE_DOOR closing garage door - 2")
							REQUEST_GARAGE_DOOR_CLOSE()
						//ENDIF
						
						IF IS_GARAGE_DOOR_CLOSED()
						
							#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === garageCarEnterCutStage = GARAGE_CAR_ENTER_CUTSCENE_WAIT_FOR_CLOSE_DOOR_beat [door closed]")
							#ENDIF
							
							iGarageCutTimer = GET_GAME_TIMER()
							
							garageCarEnterCutStage = GARAGE_CAR_ENTER_CUTSCENE_WAIT_FOR_CLOSE_DOOR_beat
						ENDIF
					ENDIF
				ELSE
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					thisVehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					IF IS_VEHICLE_DRIVEABLE(thisVehicle)
						
						//IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
							CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: REQUEST_TO_USE_DOOR closing garage door - 2")
							REQUEST_GARAGE_DOOR_CLOSE()
						//ENDIF
						
						IF IS_GARAGE_DOOR_CLOSED()
						
							#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === garageCarEnterCutStage = GARAGE_CAR_ENTER_CUTSCENE_WAIT_FOR_CLOSE_DOOR_beat [door closed]")
							#ENDIF
							
							iGarageCutTimer = GET_GAME_TIMER()
							
							garageCarEnterCutStage = GARAGE_CAR_ENTER_CUTSCENE_WAIT_FOR_CLOSE_DOOR_beat
						ENDIF
					ELSE
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: REQUEST_TO_USE_DOOR closing garage door - 3")
						REQUEST_GARAGE_DOOR_CLOSE()
						
					
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === garageCarEnterCutStage = GARAGE_CAR_ENTER_CUTSCENE_COMPLETE [vehicle not drivable]")
						#ENDIF
						
						garageCarEnterCutStage = GARAGE_CAR_ENTER_CUTSCENE_COMPLETE
						//RESET_GARAGE_CAR_ENTER_CUTSCENE(TRUE)
					ENDIF
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: REQUEST_TO_USE_DOOR closing garage door - 4")
					REQUEST_GARAGE_DOOR_CLOSE()
					
					
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === garageCarEnterCutStage = GARAGE_CAR_ENTER_CUTSCENE_COMPLETE [not in vehicle]")
					#ENDIF
					
					garageCarEnterCutStage = GARAGE_CAR_ENTER_CUTSCENE_COMPLETE
					//RESET_GARAGE_CAR_ENTER_CUTSCENE(TRUE)
				ENDIF
				ENDIF
			BREAK
			
			CASE GARAGE_CAR_ENTER_CUTSCENE_WAIT_FOR_CLOSE_DOOR_beat
				
				IF GET_GAME_TIMER() - iGarageCutTimer > GARAGE_CUT_SKIP_TIME_BEAT
					
					IF bUseNewEntryAnims //GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_BUILDING_1
						IF warpInControl.iStage != PROP_ENTER_STAGE_FAKE_CUTSCENE_HEIST
							//CLEANUP_MP_CUTSCENE(DEFAULT,FALSE)
						ENDIF
					ENDIF
					
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === garageCarEnterCutStage = GARAGE_CAR_ENTER_CUTSCENE_COMPLETE [time > GARAGE_CUT_SKIP_TIME_BEAT]")
					#ENDIF
					
					IF NOT IS_PROPERTY_OFFICE_GARAGE(iCurrentPropertyID)
						garageCarEnterCutStage = GARAGE_CAR_ENTER_CUTSCENE_COMPLETE
					ELSE
						IF (mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_OFFICE_BUILDING_3) 
							iGarageCutTimer = GET_GAME_TIMER()
							garageCarEnterCutStage = GARAGE_CAR_ENTER_CUTSCENE_OFFICE_GARAGE_PAUSE
						ELSE
							garageCarEnterCutStage = GARAGE_CAR_ENTER_CUTSCENE_COMPLETE
						ENDIF
					ENDIF
						
				ENDIF
				
			BREAK
			
			CASE GARAGE_CAR_ENTER_CUTSCENE_OFFICE_GARAGE_PAUSE
				IF GET_GAME_TIMER() - iGarageCutTimer > 1000
					garageCarEnterCutStage = GARAGE_CAR_ENTER_CUTSCENE_COMPLETE
				ENDIF
			BREAK
			
			CASE GARAGE_CAR_ENTER_CUTSCENE_COMPLETE
				
				#IF ACTIVATE_PROPERTY_CS_DEBUG
				IF bRequestDoorDebug
					garageCarEnterCutStage = GARAGE_CAR_ENTER_CUTSCENE_INIT
				ENDIF
				#ENDIF
				
				IF NOT IS_PROPERTY_OFFICE_GARAGE(iCurrentPropertyID)
				
				DELETE_CLONED_ENTITIES_AND_CLEAR_REQUESTS(3)
				
				
				ELSE
					
				ENDIF

				// url:bugstar:2194315
				STOP_AUDIO_SCENE("DLC_MPHEIST_DRIVE_INTO_GARAGE_SCENE")

				RETURN TRUE
				
			BREAK
		
		ENDSWITCH
	ELSE
		RESET_GARAGE_CAR_ENTER_CUTSCENE(5,TRUE)
		RESET_TO_ENTRANCE_TO_PROPERTY_STAGE()
		
		CDEBUG1LN(DEBUG_SAFEHOUSE, "DO_GARAGE_CAR_ENTER_THIS_CUTSCENE: aborting player not ok")
	ENDIF
	
	RETURN FALSE
ENDFUNC


PROC createFakeBuilding6GarageObject()
		
	CONST_FLOAT fBUILDING_6_GARAGE_EXTRA_Z	1.2		//2.0
	
	IF NOT DOES_ENTITY_EXIST(fakeBuilding6GarageObj)
		MODEL_NAMES fakeBuilding6GarageModel = PROP_BH1_08_MP_GAR
		REQUEST_MODEL(fakeBuilding6GarageModel)
		IF HAS_MODEL_LOADED(fakeBuilding6GarageModel)
			IF fBUILDING_6_GARAGE_EXTRA_Z = 1.2
				fakeBuilding6GarageObj = CREATE_OBJECT(fakeBuilding6GarageModel, <<-878.027,-359.452,-36.249>>, FALSE,FALSE,TRUE)
			ELSE
			
			fakeBuilding6GarageObj = CREATE_OBJECT(fakeBuilding6GarageModel, <<-878.027,-359.452,36.249>> + <<0,0,fBUILDING_6_GARAGE_EXTRA_Z>>, FALSE,FALSE,TRUE)
			
			ENDIF
			SET_ENTITY_ROTATION(fakeBuilding6GarageObj, <<0,0,26.88>>)
			FREEZE_ENTITY_POSITION(fakeBuilding6GarageObj, TRUE)
			SET_ENTITY_COLLISION(fakeBuilding6GarageObj, FALSE)
			
			SET_MODEL_AS_NO_LONGER_NEEDED(fakeBuilding6GarageModel)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL DO_REAL_GARAGE_CAR_ENTER_CUTSCENE(BOOL bResetCoords)
	//CDEBUG1LN(DEBUG_SAFEHOUSE, "CDM: Current Stack 9 = ",GET_CURRENT_STACK_SIZE())
	STRUCT_NET_REALTY_3_SCENE scene
	//CDEBUG1LN(DEBUG_SAFEHOUSE, "CDM: Current Stack 10 = ",GET_CURRENT_STACK_SIZE())
	IF NOT Private_Get_net_realty_3_scene(GET_PROPERTY_BUILDING(iCurrentPropertyID), scene)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "     ----->>>>>     STRUCT_NET_REALTY_3_SCENE scene - iWalkInStage = WIS_CLEANUP - C")
		RETURN TRUE
	ENDIF
	
	IF iCurrentPropertyID = PROPERTY_LOW_APT_7
		DISABLE_OCCLUSION_THIS_FRAME()
	ENDIF
	
	//#1629282 - BUILDING_6- Garage Door Doesnt cover entrance - you see through gap at the top
	IF iCurrentPropertyID = PROPERTY_HIGH_APT_14
	OR iCurrentPropertyID = PROPERTY_HIGH_APT_15
		createFakeBuilding6GarageObject()
	ENDIF 
	
	IF DO_GARAGE_CAR_ENTER_THIS_CUTSCENE(bResetCoords, scene)
		IF DOES_ENTITY_EXIST(fakeBuilding6GarageObj)
			DELETE_OBJECT(fakeBuilding6GarageObj)
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DO_GARAGE_CAR_ENTER_CUTSCENE(BOOL bResetCoords)
	VEHICLE_INDEX vehID
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		vehID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		
		IF (GET_ENTITY_MODEL(vehID) = APC 
		AND GET_VEHICLE_MOD(vehID,MOD_ROOF) >= 0)
		OR GET_ENTITY_MODEL(vehID) = HALFTRACK
		OR GET_ENTITY_MODEL(vehID) = INSURGENT3
		OR GET_ENTITY_MODEL(vehID) = CARACARA
		OR GET_ENTITY_MODEL(vehID) = PATRIOT2
		OR GET_ENTITY_MODEL(vehID) = OPPRESSOR2
		OR GET_ENTITY_MODEL(vehID) = ZHABA
		OR GET_ENTITY_MODEL(vehID) = YOUGA3
		#IF FEATURE_HEIST_ISLAND
		OR GET_ENTITY_MODEL(vehID) = SLAMTRUCK
		#ENDIF
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: RUN_FADED_OUT_GARAGE_CAR_ENTER running")
			IF RUN_FADED_OUT_GARAGE_CAR_ENTER()
				IF IS_PROPERTY_OFFICE_GARAGE(iCurrentPropertyID)
					g_bDoGarageElevator = TRUE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: DO_GARAGE_CAR_ENTER_CUTSCENE - told to play interal elevator cs.")
				ENDIF
				RETURN TRUE
			ENDIF
			RETURN FALSE
		ENDIF
	ENDIF

	IF GET_PROPERTY_SIZE_TYPE(mpProperties[iCurrentPropertyID].iIndex) = PROP_SIZE_TYPE_LARGE_APT
	AND NOT IS_PROPERTY_STILT_APARTMENT(iCurrentPropertyID)
	OR iCurrentPropertyID = PROPERTY_LOW_APT_7
	OR IS_PROPERTY_OFFICE_GARAGE(iCurrentPropertyID)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: DO_REAL_GARAGE_CAR_ENTER_CUTSCENE running")
		IF DO_REAL_GARAGE_CAR_ENTER_CUTSCENE(bResetCoords)
		
			IF IS_PROPERTY_OFFICE_GARAGE(iCurrentPropertyID)
				g_bDoGarageElevator = TRUE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: DO_GARAGE_CAR_ENTER_CUTSCENE - told to play interal elevator cs.")
			ENDIF
		
			RETURN TRUE
		ENDIF
	ELSE	//Med / Low
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: DO_FAKE_GARAGE_CAR_ENTER_THIS_CUTSCENE running")
		IF DO_FAKE_GARAGE_CAR_ENTER_THIS_CUTSCENE()
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC



ENUM GARAGE_PED_ENTER_CUTSCENE_STAGE
	GARAGE_PED_ENTER_CUTSCENE_INIT = 0,
	GARAGE_PED_ENTER_CUTSCENE_TRIGGER_CUT,
	GARAGE_PED_ENTER_CUTSCENE_WAIT_FOR_OPEN_DOOR,
	GARAGE_PED_ENTER_CUTSCENE_WAIT_FOR_INSIDE,
	GARAGE_PED_ENTER_CUTSCENE_WAIT_FOR_CLOSE_DOOR,
	GARAGE_PED_ENTER_CUTSCENE_WAIT_FOR_CLOSE_DOOR_BEAT,
	GARAGE_PED_ENTER_CUTSCENE_COMPLETE
ENDENUM
GARAGE_PED_ENTER_CUTSCENE_STAGE garagePedEnterCutStage

PROC RESET_GARAGE_PED_ENTER_CUTSCENE(BOOL bClearTasks = FALSE, BOOL bTurnOffRenderScriptCams = TRUE)

	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === RESET_GARAGE_PED_ENTER_CUTSCENE()")
	#ENDIF
	CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: REQUEST_TO_USE_DOOR closing garage door - 5")
	REQUEST_GARAGE_DOOR_CLOSE()
	RESET_GARAGE_CUTSCENE(bClearTasks, bTurnOffRenderScriptCams)
	RESET_TO_ENTRANCE_TO_PROPERTY_STAGE()
	IF g_bPropInteriorScriptRunning
	OR IS_BIT_SET(mpPropMaintain.iBS, MP_PROP_MAINTAIN_BS_LAUNCHED_INT_SCRIPT)
		globalPropertyEntryData.bLeftOrRejectedBuzzer = TRUE
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: globalPropertyEntryData.bLeftOrRejectedBuzzer = TRUE. RESET_GARAGE_PED_ENTER_CUTSCENE")
	ENDIF
//	#IF IS_DEBUG_BUILD
//		bDebugLaunchGaragePedEnterCutscene = FALSE
//		bDebugSeenGaragePedEnterCutscene = FALSE
//	#ENDIF
	garagePedEnterCutStage = GARAGE_PED_ENTER_CUTSCENE_INIT
	
ENDPROC

FUNC BOOL IS_OK_TO_PROCEED_FOR_NEW_ENTRY()
	IF bUseNewEntryAnims //GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_BUILDING_1	
		IF garagePedEnterCutStage > GARAGE_PED_ENTER_CUTSCENE_INIT
			IF IS_PED_INJURED(pedFakeGarageEnter[0])
				CDEBUG1LN(DEBUG_SAFEHOUSE, "IS_OK_TO_PROCEED_FOR_NEW_ENTRY - clone injured")
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL DO_GARAGE_PED_ENTER_THIS_CUTSCENE(BOOL bResetCoords, STRUCT_NET_REALTY_2_SCENE &scene)
	
	IF IS_NET_PLAYER_OK(PLAYER_ID())
	AND IS_OK_TO_PROCEED_FOR_NEW_ENTRY()
		
		#IF ACTIVATE_PROPERTY_CS_DEBUG
		BOOL bRequestDoorDebug = FALSE
		IF rag_net_realty_2_scene.bEnableTool
			bRequestDoorDebug = TRUE
		ENDIF
		#ENDIF
		
		PED_INDEX thePed = PLAYER_PED_ID()
		IF bUseNewEntryAnims //GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_BUILDING_1
		thePed = pedFakeGarageEnter[0]
		ENDIF
	
		SWITCH garagePedEnterCutStage
			
			CASE GARAGE_PED_ENTER_CUTSCENE_INIT
				IF bUseNewEntryAnims //GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_BUILDING_1
					NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)//,NSPC_PREVENT_VISIBILITY_CHANGES)
					SET_BIT(iLocalBS, LOCAL_BS_TAKEN_AWAY_CONTROL)
					iGarageCutsceneBitset = 0
					iGarageCutTimer = GET_GAME_TIMER()
				
					//IF IS_PED_MALE(PLAYER_PED_ID()) 
					IF NOT IS_PLAYER_FEMALE()
						pedFakeGarageEnter[0] = CREATE_PED(PEDTYPE_CIVMALE, GET_ENTITY_MODEL(PLAYER_PED_ID()), GET_ENTITY_COORDS(PLAYER_PED_ID()) + <<0.0, 0.0, -10.0>>, GET_ENTITY_HEADING(PLAYER_PED_ID()), FALSE, FALSE)
					ELSE
						pedFakeGarageEnter[0] = CREATE_PED(PEDTYPE_CIVFEMALE, GET_ENTITY_MODEL(PLAYER_PED_ID()), GET_ENTITY_COORDS(PLAYER_PED_ID()) + <<0.0, 0.0, -10.0>>, GET_ENTITY_HEADING(PLAYER_PED_ID()), FALSE, FALSE)
					ENDIF
					CLONE_PED_TO_TARGET(PLAYER_PED_ID(), pedFakeGarageEnter[0])
					FREEZE_ENTITY_POSITION(pedFakeGarageEnter[0],TRUE)
					SET_ENTITY_PROOFS(pedFakeGarageEnter[0],TRUE,TRUE,TRUE,TRUE,TRUE,TRUE)
					fakeGarageCarEnterCutsceneStage = FAKE_GARAGE_CAR_ENTER_CUTSCENE_START
					iGarageCutTimer = GET_GAME_TIMER()
					garagePedEnterCutStage = GARAGE_PED_ENTER_CUTSCENE_TRIGGER_CUT
				ELSE
				garagePedEnterCutStage = GARAGE_PED_ENTER_CUTSCENE_TRIGGER_CUT
				ENDIF
			BREAK
		
			CASE GARAGE_PED_ENTER_CUTSCENE_TRIGGER_CUT
				IF bUseNewEntryAnims //GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_BUILDING_1
					IF (GET_GAME_TIMER() - iGarageCutTimer) < 5000
						IF DOES_ENTITY_EXIST(pedFakeGarageEnter[0])
							IF NOT IS_PED_INJURED(pedFakeGarageEnter[0])
								IF NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedFakeGarageEnter[0])
									CDEBUG1LN(DEBUG_SAFEHOUSE, "Waiting on streaming for player copy-112" )
									RETURN FALSE
								ENDIF
								IF NOT HAS_PED_HEAD_BLEND_FINISHED(pedFakeGarageEnter[0])
									CDEBUG1LN(DEBUG_SAFEHOUSE, "Waiting on head blend for player copy-112" )
									RETURN FALSE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(),TRUE)
					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
											//playerId	bHasControl	bVisible	bClearTasks	bHasCollision	bFreezePos					
					NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)//,NSPC_PREVENT_VISIBILITY_CHANGES)
					SET_BIT(iLocalBS, LOCAL_BS_TAKEN_AWAY_CONTROL)
					iGarageCutsceneBitset = 0
					iGarageCutTimer = GET_GAME_TIMER()
					IF bUseNewEntryAnims //GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_BUILDING_1
						SET_ENTITY_COORDS(thePed, scene.mPlacers[net_realty_2_SCENE_PLACER_resetCoords].vPos)
						SET_ENTITY_HEADING(thePed, scene.mPlacers[net_realty_2_SCENE_PLACER_resetCoords].fRot)
						FREEZE_ENTITY_POSITION(pedFakeGarageEnter[0],FALSE)
					ELSE
					
					IF bResetCoords
						SET_ENTITY_COORDS(PLAYER_PED_ID(), scene.mPlacers[net_realty_2_SCENE_PLACER_resetCoords].vPos)
						SET_ENTITY_HEADING(PLAYER_PED_ID(), scene.mPlacers[net_realty_2_SCENE_PLACER_resetCoords].fRot)
					ENDIF
					ENDIF
//					camGarageCutscene0 = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
//					SET_CAM_ACTIVE(camGarageCutscene0, TRUE)
//					SET_CAM_PARAMS(	camGarageCutscene0, 
//									<<-794.4904, 306.3370, 87.0839>>,
//									<<17.7837, 0.0000, 17.6488>>, 
//									46.6333)
//					SHAKE_CAM(camGarageCutscene0, "HAND_SHAKE", 0.25)
//					RENDER_SCRIPT_CAMS(TRUE, FALSE)		
					IF bUseNewEntryAnims //GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_BUILDING_1
						IF NOT NETWORK_IS_IN_MP_CUTSCENE()
							SET_LOCAL_PLAYER_VISIBLE_IN_CUTSCENE(FALSE, TRUE)
							START_MP_CUTSCENE(TRUE)
							NETWORK_FADE_OUT_ENTITY(PLAYER_PED_ID(), TRUE, TRUE)
						ENDIF
						SET_BIT(entryCutData.iBS,ENTRY_CS_BS_STARTED_MP_CUTSCENE)
						IF DOES_ENTITY_EXIST(entryCutData.objIDs[0])
							DELETE_OBJECT(entryCutData.objIDs[0])
						ENDIF
						IF DOES_ENTITY_EXIST(entryCutData.playerClone)
							DELETE_PED(entryCutData.playerClone)
						ENDIF
						START_AUDIO_SCENE("DLC_MPHEIST_DRIVE_INTO_GARAGE_SCENE")	
					ENDIF
					SceneTool_ExecuteCut(scene.mCuts[net_realty_2_SCENE_CUT_start], camGarageCutscene0)
					
					#IF ACTIVATE_PROPERTY_CS_DEBUG
					IF rag_net_realty_2_scene.bEnableTool
					ENDIF
					#ENDIF
					
					
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === garagePedEnterCutStage = GARAGE_PED_ENTER_CUTSCENE_WAIT_FOR_OPEN_DOOR")
					#ENDIF
					
					garagePedEnterCutStage = GARAGE_PED_ENTER_CUTSCENE_WAIT_FOR_OPEN_DOOR
					IF GET_PROPERTY_SIZE_TYPE(mpProperties[iCurrentPropertyID].iIndex) != PROP_SIZE_TYPE_LARGE_APT
					AND mpProperties[iCurrentPropertyID].iIndex != PROPERTY_LOW_APT_7
						DO_SCREEN_FADE_OUT(500)
						garagePedEnterCutStage = GARAGE_PED_ENTER_CUTSCENE_COMPLETE
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: fading skipping entrance as there is none for ped on foot for: ",mpProperties[iCurrentPropertyID].iIndex)
					ENDIF
				ELSE
					IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(),SCRIPT_TASK_LEAVE_ANY_VEHICLE) != PERFORMING_TASK
					AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(),SCRIPT_TASK_LEAVE_ANY_VEHICLE) != WAITING_TO_START_TASK
						TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
					ENDIF
				ENDIF
				
			BREAK
			
			CASE GARAGE_PED_ENTER_CUTSCENE_WAIT_FOR_OPEN_DOOR
				
				IF GET_GAME_TIMER() - iGarageCutTimer > GARAGE_CUT_SKIP_TIME_SHORT
					garagePedEnterCutStage = GARAGE_PED_ENTER_CUTSCENE_COMPLETE
				ENDIF
				
				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(),TRUE)
					
					REQUEST_GARAGE_DOOR_OPEN(#IF ACTIVATE_PROPERTY_CS_DEBUG bRequestDoorDebug #ENDIF)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "CDM: requesting open door")
					IF IS_GARAGE_DOOR_UNLOCKED()
						IF IS_GARAGE_DOOR_OPEN()
							IF VDIST(GET_ENTITY_COORDS(thePed), scene.mMarkers[net_realty_2_SCENE_MARKER_gotoA].vPos) < (30)
								TASK_GO_STRAIGHT_TO_COORD(thePed, scene.mMarkers[net_realty_2_SCENE_MARKER_gotoA].vPos, PEDMOVE_WALK)
							ENDIF
							#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === garagePedEnterCutStage = GARAGE_PED_ENTER_CUTSCENE_WAIT_FOR_INSIDE")
							#ENDIF
							
							iGarageCutTimer = GET_GAME_TIMER()
							
							garagePedEnterCutStage = GARAGE_PED_ENTER_CUTSCENE_WAIT_FOR_INSIDE
						ELSE
							CDEBUG1LN(DEBUG_SAFEHOUSE, "CDM: Waiting for open door.")
							IF NOT IS_TASK_ONGOING(thePed, SCRIPT_TASK_GO_STRAIGHT_TO_COORD)
								VECTOR vPos0, vPos1
								vPos0 = scene.mAngAreas[NET_REALTY_2_SCENE_ANGAREA_area0].vPos0 
								vPos1 = scene.mAngAreas[NET_REALTY_2_SCENE_ANGAREA_area0].vPos1
								
								IF vPos0.z > vPos1.z
									vPos0.z = vPos1.z 
								ELSE
									vPos1.z = vPos0.z 
								ENDIF
								
								IF IS_ENTITY_IN_ANGLED_AREA(thePed,
										vPos0,
										vPos1 + <<0,0,scene.mAngAreas[NET_REALTY_2_SCENE_ANGAREA_area0].fHeight>>,
										scene.mAngAreas[NET_REALTY_2_SCENE_ANGAREA_area0].fWidth)

									TASK_GO_STRAIGHT_TO_COORD(thePed,
											scene.mPlacers[NET_REALTY_2_SCENE_PLACER_gotoA].vPos, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP,
											scene.mPlacers[NET_REALTY_2_SCENE_PLACER_gotoA].fRot)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF GET_SCRIPT_TASK_STATUS(thePed,SCRIPT_TASK_LEAVE_ANY_VEHICLE) != PERFORMING_TASK
					AND GET_SCRIPT_TASK_STATUS(thePed,SCRIPT_TASK_LEAVE_ANY_VEHICLE) != WAITING_TO_START_TASK
						TASK_LEAVE_ANY_VEHICLE(thePed)
					ENDIF
				ENDIF
				
			BREAK
			
			CASE GARAGE_PED_ENTER_CUTSCENE_WAIT_FOR_INSIDE
				
				IF GET_GAME_TIMER() - iGarageCutTimer > GARAGE_CUT_SKIP_TIME_LONG
					garagePedEnterCutStage = GARAGE_PED_ENTER_CUTSCENE_COMPLETE
				ENDIF
				
				IF NOT IS_BIT_SET(iGarageCutsceneBitset, GARAGE_CUT_BS_INTERPOLATE_STARTED)
					IF GET_GAME_TIMER() - iGarageCutTimer > 500
					OR mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_BUILDING_5
//						SET_CAM_PARAMS(camGarageCutscene0,
//								<<-794.7794, 306.1613, 86.8848>>,
//								<<-2.6647, 0.0000, 7.7371>>,
//								46.6333, 5000,
//								GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
						SceneTool_ExecutePan(scene.mPans[net_realty_2_SCENE_PAN_descent], camGarageCutscene0, camGarageCutscene1)
						
						SET_BIT(iGarageCutsceneBitset, GARAGE_CUT_BS_INTERPOLATE_STARTED)
					ENDIF
				ENDIF
				
				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(),TRUE)
					IF VDIST2(GET_ENTITY_COORDS(thePed), scene.mMarkers[NET_REALTY_2_SCENE_MARKER_findCoord].vPos) < (8.0*8.0)
							
						TASK_GO_STRAIGHT_TO_COORD(thePed, scene.mMarkers[NET_REALTY_2_SCENE_MARKER_gotoB].vPos, PEDMOVE_WALK)
						
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === garagePedEnterCutStage = GARAGE_PED_ENTER_CUTSCENE_WAIT_FOR_CLOSE_DOOR")
						#ENDIF
						
						iGarageCutTimer = GET_GAME_TIMER()
						
						garagePedEnterCutStage = GARAGE_PED_ENTER_CUTSCENE_WAIT_FOR_CLOSE_DOOR
					ENDIF
					
					#IF ACTIVATE_PROPERTY_CS_DEBUG
					IF rag_net_realty_2_scene.bEnableTool
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_NET_AMBIENT,
								"=== CUTSCENE === garagePedEnterCutStage = dist: ",
								VDIST(GET_ENTITY_COORDS(thePed), scene.mMarkers[NET_REALTY_2_SCENE_MARKER_findCoord].vPos))
						#ENDIF
					ENDIF
					#ENDIF

				ELSE
					IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(),SCRIPT_TASK_LEAVE_ANY_VEHICLE) != PERFORMING_TASK
					AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(),SCRIPT_TASK_LEAVE_ANY_VEHICLE) != WAITING_TO_START_TASK
						TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
					ENDIF
				ENDIF
				
			BREAK
			
			CASE GARAGE_PED_ENTER_CUTSCENE_WAIT_FOR_CLOSE_DOOR
				
				IF GET_GAME_TIMER() - iGarageCutTimer > GARAGE_CUT_SKIP_TIME_LONG
					garagePedEnterCutStage = GARAGE_PED_ENTER_CUTSCENE_COMPLETE
				ENDIF
				
				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(),TRUE)
				
					VECTOR vPos0, vPos1
					vPos0 = scene.mAngAreas[NET_REALTY_2_SCENE_ANGAREA_area0].vPos0 
					vPos1 = scene.mAngAreas[NET_REALTY_2_SCENE_ANGAREA_area0].vPos1
					
					IF vPos0.z > vPos1.z
						vPos0.z = vPos1.z 
					ELSE
						vPos1.z = vPos0.z 
					ENDIF
					
					IF IS_ENTITY_IN_ANGLED_AREA(thePed,
							vPos0,
							vPos1 + <<0,0,scene.mAngAreas[NET_REALTY_2_SCENE_ANGAREA_area0].fHeight>>,
							scene.mAngAreas[NET_REALTY_2_SCENE_ANGAREA_area0].fWidth)
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === garagePedEnterCutStage, wait for ped to be out of angled area before closing")
						#ENDIF
					ELSE
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: REQUEST_TO_USE_DOOR closing garage door - 6")
						REQUEST_GARAGE_DOOR_CLOSE()
						
						IF IS_GARAGE_DOOR_CLOSED()
						AND VDIST2(GET_ENTITY_COORDS(thePed), scene.mMarkers[NET_REALTY_2_SCENE_MARKER_findCoord].vPos) < (8.0 * 8.0)
							#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === garagePedEnterCutStage = GARAGE_PED_ENTER_CUTSCENE_WAIT_FOR_CLOSE_DOOR_BEAT")
							#ENDIF
							
							iGarageCutTimer = GET_GAME_TIMER()
						
							garagePedEnterCutStage = GARAGE_PED_ENTER_CUTSCENE_WAIT_FOR_CLOSE_DOOR_BEAT
						ENDIF
					ENDIF
					
				ELSE
					IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(),SCRIPT_TASK_LEAVE_ANY_VEHICLE) != PERFORMING_TASK
					AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(),SCRIPT_TASK_LEAVE_ANY_VEHICLE) != WAITING_TO_START_TASK
						TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
					ENDIF
				ENDIF
				
			BREAK
			
			CASE GARAGE_PED_ENTER_CUTSCENE_WAIT_FOR_CLOSE_DOOR_BEAT
				IF GET_GAME_TIMER() - iGarageCutTimer > GARAGE_CUT_SKIP_TIME_BEAT
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === garagePedEnterCutStage = GARAGE_PED_ENTER_CUTSCENE_COMPLETE")
					#ENDIF
					garagePedEnterCutStage = GARAGE_PED_ENTER_CUTSCENE_COMPLETE
				ENDIF
			BREAK
			
			CASE GARAGE_PED_ENTER_CUTSCENE_COMPLETE
				IF NOT IS_SCREEN_FADING_OUT()
					IF IS_SCREEN_FADED_OUT()
						RENDER_SCRIPT_CAMS(FALSE, FALSE)
						IF DOES_CAM_EXIST(camGarageCutscene0)
							DESTROY_CAM(camGarageCutscene0)
						ENDIF
					ENDIF
					#IF ACTIVATE_PROPERTY_CS_DEBUG
					IF bRequestDoorDebug
						garagePedEnterCutStage = GARAGE_PED_ENTER_CUTSCENE_INIT
					ENDIF
					#ENDIF
					IF IS_AUDIO_SCENE_ACTIVE("DLC_MPHEIST_DRIVE_INTO_GARAGE_SCENE")
						STOP_AUDIO_SCENE("DLC_MPHEIST_DRIVE_INTO_GARAGE_SCENE")	
					ENDIF
					RETURN TRUE
				ENDIF
				
			BREAK
		
		ENDSWITCH
	ELSE
		RESET_GARAGE_PED_ENTER_CUTSCENE(TRUE)
		RESET_TO_ENTRANCE_TO_PROPERTY_STAGE()
	ENDIF
	
	RETURN FALSE
ENDFUNC

//#IF FEATURE_LOWRIDER_CONTENT
//STRUCT ANIM_WALK_INTO_GARAGE_STRUCT
//	VECTOR vBaseLoc
//	VECTOR vRot
//	MODEL_NAMES Model
//	//CAMERA_INDEX camera
//	INT iSyncedSceneID = -1
//	
//	//PED_INDEX theClone
//	//OBJECT_INDEX garageDoor
//ENDSTRUCT
//
////GET_PROPERTY_BUILDING(GET_OWNED_PROPERTY(1))
//PROC GET_ANIMATED_WALK_INTO_GARAGE_DATA(INT iBuilding,ANIM_WALK_INTO_GARAGE_STRUCT &garageInfo)
//	SWITCH iBuilding
//		CASE MP_PROPERTY_BUILDING_1	//HIGH 1, 2, 3, 4
//			garageInfo.vBaseLoc = << -796.085, 313.687, 86.632 >>
//			garageInfo.vRot = << 0.000, 0.000, 0.100 >>
//			garageInfo.Model = hei_prop_ss1_mpint_garage2
//		BREAK
//		CASE MP_PROPERTY_BUILDING_2	//HIGH 5, 6
//			garageInfo.vBaseLoc = << 0,0,0 >>
//			garageInfo.vRot = << 0.000, 0.000, 0.100 >>
//			garageInfo.Model = hei_prop_dt1_20_mp_gar2
//		BREAK
//		CASE MP_PROPERTY_BUILDING_3	//HIGH 7, 8, BUSINESS HIGH 1
//			garageInfo.vBaseLoc = << 0,0,0 >>
//			garageInfo.vRot = << 0.000, 0.000, 0.100 >>
//			garageInfo.Model = hei_prop_ss1_mpint_garage2
//		BREAK
//		CASE MP_PROPERTY_BUILDING_4	//HIGH 9, 10, 11
//			garageInfo.vBaseLoc = << 0,0,0 >>
//			garageInfo.vRot = << 0.000, 0.000, 0.100 >>
//			garageInfo.Model = hei_prop_ss1_mpint_garage2
//		BREAK
//		CASE MP_PROPERTY_BUILDING_5	//HIGH 12, 13
//			garageInfo.vBaseLoc = << 0,0,0 >>
//			garageInfo.vRot = << 0.000, 0.000, 0.100 >>
//			garageInfo.Model = hei_prop_ss1_mpint_garage2
//		BREAK
//		CASE MP_PROPERTY_BUILDING_6	//HIGH 14, 15
//			garageInfo.vBaseLoc = << 0,0,0 >>
//			garageInfo.vRot = << 0.000, 0.000, 0.100 >>
//			garageInfo.Model = hei_prop_ss1_mpint_garage2
//		BREAK
//		CASE MP_PROPERTY_BUILDING_7	//HIGH 16, 17
//			garageInfo.vBaseLoc = << 0,0,0 >>
//			garageInfo.vRot = << 0.000, 0.000, 0.100 >>
//			garageInfo.Model = hei_prop_ss1_mpint_garage2
//		BREAK
//		CASE MP_PROPERTY_BUILDING_23	//PROPERTY_LOW_APT_7
//			garageInfo.vBaseLoc = << 0,0,0 >>
//			garageInfo.vRot = << 0.000, 0.000, 0.100 >>
//			garageInfo.Model = hei_prop_ss1_mpint_garage2
//		BREAK
//	ENDSWITCH
//ENDPROC
//
//ANIM_WALK_INTO_GARAGE_STRUCT walkIntoGarageData
//PROC CLEAR_ANIM_WALK_INTO_GARAGE_STRUCT()
//	walkIntoGarageData.vBaseLoc = <<0,0,0>>
//	walkIntoGarageData.vRot = <<0,0,0>>
//	walkIntoGarageData.Model = DUMMY_MODEL_FOR_SCRIPT
//	walkIntoGarageData.iSyncedSceneID = -1
//ENDPROC
//
//FUNC BOOL ANIMATED_WALK_INTO_GARAGE()
//
//	IF IS_NET_PLAYER_OK(PLAYER_ID())
//	#IF FEATURE_HEIST_PLANNING
//	AND IS_OK_TO_PROCEED_FOR_NEW_ENTRY()
//	#ENDIF
//		
//		PED_INDEX thePed = pedFakeGarageEnter[0]
//		SWITCH garagePedEnterCutStage
//			
//			CASE GARAGE_PED_ENTER_CUTSCENE_INIT
//				IF NOT IS_BIT_SET(iLocalBS2,LOCAL_BS2_loadedGarageWalkInAnim)
//					CDEBUG1LN(DEBUG_SAFEHOUSE, "ANIMATED_WALK_INTO_GARAGE: requesting anim@apt_trans@garage")
//					REQUEST_ANIM_DICT("anim@apt_trans@garage")
//				ENDIF
//				IF HAS_ANIM_DICT_LOADED("anim@apt_trans@garage")
//					GET_ANIMATED_WALK_INTO_GARAGE_DATA(GET_PROPERTY_BUILDING(iCurrentPropertyID), walkIntoGarageData)
//					NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)//,NSPC_PREVENT_VISIBILITY_CHANGES)
//					SET_BIT(iLocalBS, LOCAL_BS_TAKEN_AWAY_CONTROL)
//					iGarageCutsceneBitset = 0
//					iGarageCutTimer = GET_GAME_TIMER()
//				
//					//IF IS_PED_MALE(PLAYER_PED_ID()) 
//					IF NOT IS_PLAYER_FEMALE()
//						pedFakeGarageEnter[0] = CREATE_PED(PEDTYPE_CIVMALE, GET_ENTITY_MODEL(PLAYER_PED_ID()), GET_ENTITY_COORDS(PLAYER_PED_ID()) + <<0.0, 0.0, -10.0>>, GET_ENTITY_HEADING(PLAYER_PED_ID()), FALSE, FALSE)
//					ELSE
//						pedFakeGarageEnter[0] = CREATE_PED(PEDTYPE_CIVFEMALE, GET_ENTITY_MODEL(PLAYER_PED_ID()), GET_ENTITY_COORDS(PLAYER_PED_ID()) + <<0.0, 0.0, -10.0>>, GET_ENTITY_HEADING(PLAYER_PED_ID()), FALSE, FALSE)
//					ENDIF
//					CLONE_PED_TO_TARGET(PLAYER_PED_ID(), pedFakeGarageEnter[0])
//					FREEZE_ENTITY_POSITION(pedFakeGarageEnter[0],TRUE)
//					IF DOES_ENTITY_EXIST(entryCutData.objIDs[0])
//						DELETE_OBJECT(entryCutData.objIDs[0])
//					ENDIF
//					entryCutData.objIDs[0] = CREATE_OBJECT(walkIntoGarageData.Model,GET_ENTITY_COORDS(PLAYER_PED_ID()) + <<0.0, 0.0, -20.0>>,FALSE,FALSE,TRUE)FREEZE_ENTITY_POSITION(entryCutData.objIDs[0],TRUE)
//					fakeGarageCarEnterCutsceneStage = FAKE_GARAGE_CAR_ENTER_CUTSCENE_START
//					iGarageCutTimer = GET_GAME_TIMER()
//					garagePedEnterCutStage = GARAGE_PED_ENTER_CUTSCENE_TRIGGER_CUT
//				ENDIF
//			BREAK
//		
//			CASE GARAGE_PED_ENTER_CUTSCENE_TRIGGER_CUT
//				IF (GET_GAME_TIMER() - iGarageCutTimer) < 5000
//					IF DOES_ENTITY_EXIST(thePed)
//						IF NOT IS_PED_INJURED(thePed)
//							IF NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(thePed)
//								CDEBUG1LN(DEBUG_SAFEHOUSE, "Waiting on streaming for player copy-112" )
//								RETURN FALSE
//							ENDIF
//							IF NOT HAS_PED_HEAD_BLEND_FINISHED(thePed)
//								CDEBUG1LN(DEBUG_SAFEHOUSE, "Waiting on head blend for player copy-112" )
//								RETURN FALSE
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(),TRUE)
//					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
//											//playerId	bHasControl	bVisible	bClearTasks	bHasCollision	bFreezePos					
//					NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)//,NSPC_PREVENT_VISIBILITY_CHANGES)
//					SET_BIT(iLocalBS, LOCAL_BS_TAKEN_AWAY_CONTROL)
//					iGarageCutsceneBitset = 0
//					iGarageCutTimer = GET_GAME_TIMER()
//					CREATE_MODEL_HIDE(walkIntoGarageData.vBaseLoc,2,walkIntoGarageData.Model,TRUE)	
//					CDEBUG1LN(DEBUG_SAFEHOUSE, "Creating model hide ",walkIntoGarageData.vBaseLoc, " model: ",walkIntoGarageData.Model)
//					
//					IF DOES_ENTITY_EXIST(entryCutData.objIDs[0])
//						SET_ENTITY_COORDS_NO_OFFSET(entryCutData.objIDs[0],
//													GET_ANIM_INITIAL_OFFSET_POSITION("anim@apt_trans@garage","gar_walk_1_door",walkIntoGarageData.vBaseLoc,walkIntoGarageData.vRot,0.0))
//						SET_ENTITY_ROTATION(entryCutData.objIDs[0],
//													GET_ANIM_INITIAL_OFFSET_ROTATION("anim@apt_trans@garage","gar_walk_1_door",walkIntoGarageData.vBaseLoc,walkIntoGarageData.vRot,0.0))
//						CDEBUG1LN(DEBUG_SAFEHOUSE, "ANIMATED_WALK_INTO_GARAGE: setting garage door into position")
//					ENDIF
//					IF DOES_ENTITY_EXIST(thePed)
//						SET_ENTITY_COORDS_NO_OFFSET(thePed,
//													GET_ANIM_INITIAL_OFFSET_POSITION("anim@apt_trans@garage","gar_walk_1",walkIntoGarageData.vBaseLoc,walkIntoGarageData.vRot,0.0))
//						SET_ENTITY_ROTATION(thePed,
//													GET_ANIM_INITIAL_OFFSET_ROTATION("anim@apt_trans@garage","gar_walk_1",walkIntoGarageData.vBaseLoc,walkIntoGarageData.vRot,0.0))
//						CDEBUG1LN(DEBUG_SAFEHOUSE, "ANIMATED_WALK_INTO_GARAGE: setting clone into position")
//					ENDIF
////					camGarageCutscene0 = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
////					SET_CAM_ACTIVE(camGarageCutscene0, TRUE)
////					SET_CAM_PARAMS(	camGarageCutscene0, 
////									<<-794.4904, 306.3370, 87.0839>>,
////									<<17.7837, 0.0000, 17.6488>>, 
////									46.6333)
////					SHAKE_CAM(camGarageCutscene0, "HAND_SHAKE", 0.25)
////					RENDER_SCRIPT_CAMS(TRUE, FALSE)		
//
//					IF NOT NETWORK_IS_IN_MP_CUTSCENE()
//						SET_LOCAL_PLAYER_VISIBLE_IN_CUTSCENE(FALSE, TRUE)
//						START_MP_CUTSCENE(TRUE)
//						NETWORK_FADE_OUT_ENTITY(PLAYER_PED_ID(), TRUE, TRUE)
//					ENDIF
//					SET_BIT(entryCutData.iBS,ENTRY_CS_BS_STARTED_MP_CUTSCENE)
//					
//					IF DOES_ENTITY_EXIST(entryCutData.playerClone)
//						DELETE_PED(entryCutData.playerClone)
//					ENDIF
//					START_AUDIO_SCENE("DLC_MPHEIST_DRIVE_INTO_GARAGE_SCENE")	
//
//					
//					#IF IS_DEBUG_BUILD
//					CPRINTLN(DEBUG_NET_AMBIENT, "ANIMATED_WALK_INTO_GARAGE = GARAGE_PED_ENTER_CUTSCENE_WAIT_FOR_OPEN_DOOR")
//					#ENDIF
//					walkIntoGarageData.iSyncedSceneID = CREATE_SYNCHRONIZED_SCENE(walkIntoGarageData.vBaseLoc,walkIntoGarageData.vRot)
//					IF NOT IS_PED_INJURED(thePed)
//						//VECTOR sceneRotation = << -1.725, -0.000, 2.215 >>
//						TASK_SYNCHRONIZED_SCENE(thePed,walkIntoGarageData.iSyncedSceneID, "anim@apt_trans@garage", "gar_walk_1",
//															INSTANT_BLEND_IN,INSTANT_BLEND_OUT,SYNCED_SCENE_DONT_INTERRUPT,RBF_NONE)
//					ENDIF
//					IF DOES_ENTITY_EXIST(entryCutData.objIDs[0])
//						PLAY_ENTITY_ANIM(entryCutData.objIDs[0],"gar_walk_1_door","anim@apt_trans@garage",
//												INSTANT_BLEND_IN,FALSE,FALSE,DEFAULT,0.0,ENUM_TO_INT(AF_USE_MOVER_EXTRACTION))
//					ENDIF
//					camGarageCutscene0 = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", FALSE)
//					SET_CAM_ACTIVE(camGarageCutscene0,TRUE)
//					PLAY_CAM_ANIM(camGarageCutscene0, "gar_walk_1_cam", "anim@apt_trans@garage",walkIntoGarageData.vBaseLoc,walkIntoGarageData.vRot)
//					RENDER_SCRIPT_CAMS(TRUE, FALSE) 
//					
//					
//					garagePedEnterCutStage = GARAGE_PED_ENTER_CUTSCENE_WAIT_FOR_OPEN_DOOR
//				ELSE
//					IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(),SCRIPT_TASK_LEAVE_ANY_VEHICLE) != PERFORMING_TASK
//					AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(),SCRIPT_TASK_LEAVE_ANY_VEHICLE) != WAITING_TO_START_TASK
//						TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
//					ENDIF
//				ENDIF
//				
//			BREAK
//			
//			CASE GARAGE_PED_ENTER_CUTSCENE_WAIT_FOR_OPEN_DOOR
//				
//				IF GET_SYNCHRONIZED_SCENE_PHASE(walkIntoGarageData.iSyncedSceneID) >= 0.99
//				OR GET_GAME_TIMER() - iGarageCutTimer > GARAGE_CUT_SKIP_TIME_LONG
//					garagePedEnterCutStage = GARAGE_PED_ENTER_CUTSCENE_COMPLETE
//				ENDIF
//				
//
//				
//			BREAK
//			
////			CASE GARAGE_PED_ENTER_CUTSCENE_WAIT_FOR_INSIDE
////				
////				IF GET_GAME_TIMER() - iGarageCutTimer > GARAGE_CUT_SKIP_TIME_LONG
////					garagePedEnterCutStage = GARAGE_PED_ENTER_CUTSCENE_COMPLETE
////				ENDIF
////		
////				
////			BREAK
////			
////			CASE GARAGE_PED_ENTER_CUTSCENE_WAIT_FOR_CLOSE_DOOR
////				
////				IF GET_GAME_TIMER() - iGarageCutTimer > GARAGE_CUT_SKIP_TIME_LONG
////					garagePedEnterCutStage = GARAGE_PED_ENTER_CUTSCENE_COMPLETE
////				ENDIF
////				
////			BREAK
////			
////			CASE GARAGE_PED_ENTER_CUTSCENE_WAIT_FOR_CLOSE_DOOR_BEAT
////				IF GET_GAME_TIMER() - iGarageCutTimer > GARAGE_CUT_SKIP_TIME_BEAT
////					#IF IS_DEBUG_BUILD
////					CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === garagePedEnterCutStage = GARAGE_PED_ENTER_CUTSCENE_COMPLETE")
////					#ENDIF
////					garagePedEnterCutStage = GARAGE_PED_ENTER_CUTSCENE_COMPLETE
////				ENDIF
////			BREAK
//			
//			CASE GARAGE_PED_ENTER_CUTSCENE_COMPLETE
//				IF NOT IS_SCREEN_FADING_OUT()
//					IF IS_SCREEN_FADED_OUT()
//						RENDER_SCRIPT_CAMS(FALSE, FALSE)
//						IF DOES_CAM_EXIST(camGarageCutscene0)
//							DESTROY_CAM(camGarageCutscene0)
//						ENDIF
//					ENDIF
//
//					IF IS_AUDIO_SCENE_ACTIVE("DLC_MPHEIST_DRIVE_INTO_GARAGE_SCENE")
//						STOP_AUDIO_SCENE("DLC_MPHEIST_DRIVE_INTO_GARAGE_SCENE")	
//					ENDIF
//					FORCE_PLAYER_LOCALLY_VISIBLE_FOR_FADE()
//					RETURN TRUE
//				ENDIF
//				
//			BREAK
//		
//		ENDSWITCH
//	ELSE
//		RESET_GARAGE_PED_ENTER_CUTSCENE(TRUE)
//		RESET_TO_ENTRANCE_TO_PROPERTY_STAGE()
//	ENDIF
//	
//	RETURN FALSE
//
////	ANIM_WALK_INTO_GARAGE_STRUCT walkIntoGarageData 
////	GET_ANIMATED_WALK_INTO_GARAGE_DATA(GET_PROPERTY_BUILDING(iCurrentPropertyID), walkIntoGarageData)
////	
////	walkIntoGarageData.camera = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", FALSE)
////	SET_CAM_ACTIVE(walkIntoGarageData.camera,TRUE)
////	PLAY_SYNCHRONIZED_CAM_ANIM(walkIntoGarageData.camera, walkIntoGarageData.iSyncedSceneID, "gar_walk_1_cam", "anim@apt_trans@garage")
////	RENDER_SCRIPT_CAMS(TRUE, FALSE) 
////	IF DOES_ENTITY_EXIST(walkIntoGarageData.garageDoor)
////		SET_ENTITY_COORDS_NO_OFFSET(walkIntoGarageData.garageDoor)
////									GET_ANIM_INITIAL_OFFSET_POSITION("anim@apt_trans@garage","gar_walk_1_door",walkIntoGarageData.vBaseLoc,walkIntoGarageData.vRot,0.0))
////		SET_ENTITY_ROTATION(walkIntoGarageData.garageDoor,
////									GET_ANIM_INITIAL_OFFSET_ROTATION("anim@apt_trans@garage","gar_walk_1_door",walkIntoGarageData.vBaseLoc,walkIntoGarageData.vRot,0.0))
////		CDEBUG1LN(DEBUG_SAFEHOUSE, "ANIMATED_WALK_INTO_GARAGE: setting garage door into position")
////	ENDIF
////	IF DOES_ENTITY_EXIST(walkIntoGarageData.theClone)
////		SET_ENTITY_COORDS_NO_OFFSET(walkIntoGarageData.theClone)
////									GET_ANIM_INITIAL_OFFSET_POSITION("anim@apt_trans@garage","gar_walk_1",walkIntoGarageData.vBaseLoc,walkIntoGarageData.vRot,0.0))
////		SET_ENTITY_ROTATION(walkIntoGarageData.theClone,
////									GET_ANIM_INITIAL_OFFSET_ROTATION("anim@apt_trans@garage","gar_walk_1",walkIntoGarageData.vBaseLoc,walkIntoGarageData.vRot,0.0))
////		CDEBUG1LN(DEBUG_SAFEHOUSE, "ANIMATED_WALK_INTO_GARAGE: setting clone into position")
////	ENDIF
////	walkIntoGarageData.garageDoor = CREATE_OBJECT(walkIntoGarageData.Model,<<0,0,0>>,FALSE,FALSE,TRUE)
////	TASK_SYNCHRONIZED_SCENE(walkIntoGarageData.theClone, walkIntoGarageData.iSyncedSceneID, "gar_walk_1", "anim@apt_trans@garage", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
////	PLAY_SYNCHRONIZED_ENTITY_ANIM(walkIntoGarageData.garageDoor, walkIntoGarageData.iSyncedSceneID, "gar_walk_1_door","anim@apt_trans@garage", INSTANT_BLEND_IN )
////	
////	CREATE_MODEL_HIDE(walkIntoGarageData.vBaseLoc,2,walkIntoGarageData.Model,TRUE)	
////	CDEBUG1LN(DEBUG_SAFEHOUSE, "Creating model hide ",walkIntoGarageData.vBaseLoc, " model: ",walkIntoGarageData.Model)
//			
//ENDFUNC
//#ENDIF


FUNC BOOL DO_GARAGE_PED_ENTER_CUTSCENE(BOOL bResetCoords)
	
	STRUCT_NET_REALTY_2_SCENE scene
	IF NOT Private_Get_net_realty_2_scene(GET_PROPERTY_BUILDING(iCurrentPropertyID), scene)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "     ----->>>>>     STRUCT_NET_REALTY_2_SCENE scene - iWalkInStage = WIS_CLEANUP - C")
		RETURN TRUE
	ENDIF
	IF iCurrentPropertyID = PROPERTY_LOW_APT_7
		DISABLE_OCCLUSION_THIS_FRAME()
	ENDIF
	
//	#IF FEATURE_LOWRIDER_CONTENT
//	IF GET_PROPERTY_SIZE_TYPE(iCurrentPropertyID) = PROP_SIZE_TYPE_LARGE_APT
//	OR iCurrentPropertyID = PROPERTY_LOW_APT_7
//		IF ANIMATED_WALK_INTO_GARAGE()
//			RETURN TRUE
//		ELSE
//			RETURN FALSE
//		ENDIF
//	ENDIF
//	#ENDIF
	
	//#1629282 - BUILDING_6- Garage Door Doesnt cover entrance - you see through gap at the top
	IF iCurrentPropertyID = PROPERTY_HIGH_APT_14
	OR iCurrentPropertyID = PROPERTY_HIGH_APT_15
		createFakeBuilding6GarageObject()
	ENDIF 
	
	IF DO_GARAGE_PED_ENTER_THIS_CUTSCENE(bResetCoords, scene)
		IF DOES_ENTITY_EXIST(fakeBuilding6GarageObj)
			DELETE_OBJECT(fakeBuilding6GarageObj)
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//=== End Garage Cutscene Fudge Two ===

PROC PLAY_ENTERING_GARAGE_SOUND()
	INT iSoundID = GET_SOUND_ID()
	IF GET_PROPERTY_SIZE_TYPE(iCurrentPropertyID) != PROP_SIZE_TYPE_LARGE_APT
	AND NOT IS_PROPERTY_CLUBHOUSE(iCurrentPropertyID)
	AND NOT IS_PROPERTY_OFFICE_GARAGE(iCurrentPropertyID)
		PLAY_SOUND_FRONTEND(iSoundID,"GARAGE_DOOR_SCRIPTED_OPEN")
		SET_VARIABLE_ON_SOUND(iSoundID, "hold", 2.25) 
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: PLAY_ENTERING_GARAGE_SOUND open audio being played")
	ENDIF
ENDPROC

FUNC BOOL IS_ANYONE_TRYING_TO_ENTER_CURRENT_VEHICLE()
	VEHICLE_INDEX theVeh
	PLAYER_INDEX vehiclePlayer
	INT i
	VECTOR vCoords
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			theVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			IF IS_VEHICLE_DRIVEABLE(theVeh)
				vCoords = GET_PLAYER_COORDS(PLAYER_ID())
				REPEAT NUM_NETWORK_PLAYERS i
					vehiclePlayer = INT_TO_PLAYERINDEX(i)
					IF vehiclePlayer != PLAYER_ID()
						IF IS_NET_PLAYER_OK(vehiclePlayer,TRUE,FALSE)
							IF GET_DISTANCE_BETWEEN_COORDS(vCoords,GET_PLAYER_COORDS(vehiclePlayer)) <= 125
								IF NOT IS_REMOTE_PLAYER_IN_NON_CLONED_VEHICLE(vehiclePlayer)
									IF GET_VEHICLE_PED_IS_ENTERING(GET_PLAYER_PED(vehiclePlayer)) = theVeh
										RETURN TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL CHECK_EVERYONE_IN_VEHICLE_IS_READY()
	VEHICLE_INDEX theVeh
	PED_INDEX vehiclePed
	PLAYER_INDEX vehiclePlayer
	INT iSeat
	BOOL bPlayerInVehicleDoingDropOff
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			theVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			IF IS_VEHICLE_DRIVEABLE(theVeh)
				FOR iSeat = ENUM_TO_INT(VS_DRIVER) TO ENUM_TO_INT(VS_EXTRA_RIGHT_3)
					IF NOT IS_VEHICLE_SEAT_FREE(theVeh, INT_TO_ENUM(VEHICLE_SEAT, iSeat),TRUE)
						vehiclePed = GET_PED_IN_VEHICLE_SEAT(theVeh,INT_TO_ENUM(VEHICLE_SEAT, iSeat),TRUE)
						IF NOT IS_PED_INJURED(vehiclePed)
							IF IS_PED_A_PLAYER(vehiclePed)
								vehiclePlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(vehiclePed)
								IF IS_NET_PLAYER_OK(vehiclePlayer)
								AND vehiclePlayer != PLAYER_ID()
									CDEBUG1LN(DEBUG_SAFEHOUSE, "CHECK_EVERYONE_IN_VEHICLE_IS_READY: checking player: ", GET_PLAYER_NAME(vehiclePlayer))
									IF warpInWithDriverID != vehiclePlayer
										IF IS_BIT_SET(playerBD[NATIVE_TO_INT(vehiclePlayer)].iBS,PLAYER_BD_BS_DOING_HEIST_DROPOFF)
											bPlayerInVehicleDoingDropOff = TRUE
										ELSE	
											IF IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_DOING_HEIST_DROPOFF)
												RETURN FALSE
											ELIF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(vehiclePlayer)].propertyDetails.iBS,PROPERTY_BROADCAST_BS_WARPING_WITH_OWNER_IN_CAR)
												RETURN FALSE	
											ENDIF
										ENDIF
										
										
//										#IF NOT FEATURE_HEIST_PLANNING
//										IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(vehiclePlayer)].propertyDetails.iBS,PROPERTY_BROADCAST_BS_WARPING_WITH_OWNER_IN_CAR)
//											RETURN FALSE
//										ENDIF	
//										#ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDFOR
				IF bPlayerInVehicleDoingDropOff
				AND NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_DOING_HEIST_DROPOFF)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "CHECK_EVERYONE_IN_VEHICLE_IS_READY: someone in vehicle is doing heist drop off but you are not returning false")
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_WARP_INTO_GARAGE_ABORT(VEHICLE_INDEX vehID)//, MODEL_NAMES theModel, BOOL bPersonalVehicle)
//	INT iDLCVehSlot
//	IF DOES_DECORATOR_PREVENT_ALLOWANCE_IN_GARAGE(vehID)
//		RETURN TRUE
//	ENDIF
//	iDLCVehSlot = GET_DLC_VEHICLE_DATA_SLOT_FOR_VEHICLE(theModel)
//	IF NOT IS_VEHICLE_AVAILABLE_FOR_GAME(theModel)
//		RETURN TRUE
//	ENDIF
//	IF iDLCVehSlot != -1
//	AND NOT IS_DLC_VEHICLE_SLOT_SUITABLE_FOR_GARAGE(iDLCVehSlot)
//		RETURN TRUE
//	ENDIF
//	IF IS_MP_PREMIUM_VEHICLE(theModel)	
//	AND	NOT bPersonalVehicle
//		RETURN TRUE
//	ENDIF
	BOOL bPrintedReason = IS_BIT_SET(iLocalBS, LOCAL_BS_INVALID_VEHICLE_HELP)
	IF NOT IS_VEHICLE_SUITABLE_FOR_PROPERTY(vehID,iCurrentPropertyID,ownedDetails, SHOULD_BYPASS_CHECKS_FOR_ENTRY(),bPrintedReason, DEFAULT, DEFAULT #IF IS_DEBUG_BUILD ,db_bBypassVehicleRestrictions #ENDIF)
		IF bPrintedReason
			SET_BIT(iLocalBS, LOCAL_BS_INVALID_VEHICLE_HELP)
		ENDIF
		RETURN TRUE
	ENDIF
	IF bPrintedReason
		SET_BIT(iLocalBS, LOCAL_BS_INVALID_VEHICLE_HELP)
	ENDIF
	RETURN FALSE
ENDFUNC

PROC CLEAR_VEHICLE_OF_PROJECTILES()
	VEHICLE_INDEX vehClear
	
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			vehClear = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			IF IS_VEHICLE_DRIVEABLE(vehClear)
				IF HAS_VEHICLE_GOT_PROJECTILE_ATTACHED(PLAYER_PED_ID(), vehClear)
					CLEAR_AREA_OF_PROJECTILES(GET_ENTITY_COORDS(vehClear), 5.0)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL CHECK_CURRENT_VEHICLE_SLOT(MODEL_NAMES vehicleModel,INT iSlot, BOOL &bOtherGarage, BOOL &bStorage)
	bOtherGarage = FALSE
	bStorage = FALSE
	INT i
	BOOL bOwnedProperty
	REPEAT MAX_OWNED_PROPERTIES i
		IF IS_PROPERTY_SLOT_NO_PER_VEHS_PROPERTY(i)
			//do nothing
		ELSE
			IF GET_OWNED_PROPERTY_SLOT_INDEX(iCurrentPropertyID) = i
				bOwnedProperty = TRUE
				IF DOES_VEHICLE_SLOT_BELONG_TO_GARAGE(vehicleModel,iSlot,i)
					IF DOES_VEHICLE_SLOT_BELONG_TO_GARAGE(vehicleModel,iSlot,i,DEFAULT,DEFAULT,TRUE)
						RETURN TRUE	
					ELSE
						bStorage = TRUE
					ENDIF
				ENDIF
			ELSE
				IF DOES_VEHICLE_SLOT_BELONG_TO_GARAGE(vehicleModel,iSlot,i)
					bOtherGarage = TRUE
				ELSE
					IF GET_OWNED_PROPERTY(i) > 0
						bOtherGarage = TRUE
						bStorage = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	INT iDisplaySlot
	IF iSlot >= 0
		MPSV_GET_DISPLAY_SLOT_FROM_SAVE_SLOT(iSlot,iDisplaySlot)
	//	PRINTLN("CHECK_CURRENT_VEHICLE_SLOT: iDisplaySlot = ",iDisplaySlot)
		IF iDisplaySlot = DISPLAY_SLOT_START_ARMOURY_TRUCK
			bOtherGarage = TRUE
		ENDIF
	ENDIF
	IF iSlot >= 0
		MPSV_GET_DISPLAY_SLOT_FROM_SAVE_SLOT(iSlot,iDisplaySlot)
	//	PRINTLN("CHECK_CURRENT_VEHICLE_SLOT: iDisplaySlot = ",iDisplaySlot)
		IF iDisplaySlot = DISPLAY_SLOT_TEROBYTE_OPP2
			bOtherGarage = TRUE
		ENDIF
		#IF FEATURE_HEIST_ISLAND
		IF iDisplaySlot = DISPLAY_SLOT_SUB_AQUA_CAR
			bOtherGarage = TRUE
		ENDIF
		#ENDIF
	ENDIF
	
//	IF GET_OWNED_PROPERTY_SLOT_INDEX(iCurrentPropertyID) = 0
//		
//	ELIF GET_OWNED_PROPERTY_SLOT_INDEX(iCurrentPropertyID) = 1
//		IF DOES_VEHICLE_SLOT_BELONG_TO_GARAGE(vehicleModel,iSlot,1)
//			RETURN TRUE	
//		ELIF DOES_VEHICLE_SLOT_BELONG_TO_GARAGE(vehicleModel,iSlot,0)
//			bOtherGarage = TRUE
//		ELSE
//			IF GET_OWNED_PROPERTY(0) > 0
//				bOtherGarage = TRUE
//				bStorage = TRUE
//			ENDIF
//		ENDIF
	
	IF NOT bOwnedProperty
	#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_SAFEHOUSE, "CHECK_CURRENT_VEHICLE_SLOT: Checking a slot for a property but you don't own it!")
		CDEBUG1LN(DEBUG_SAFEHOUSE, "CHECK_CURRENT_VEHICLE_SLOT: iCurrentPropertyID = ",iCurrentPropertyID)
		REPEAT MAX_OWNED_PROPERTIES i
		CDEBUG1LN(DEBUG_SAFEHOUSE, "CHECK_CURRENT_VEHICLE_SLOT: GET_OWNED_PROPERTY(",i,") = ",GET_OWNED_PROPERTY(i))
		ENDREPEAT
		SCRIPT_ASSERT("CHECK_CURRENT_VEHICLE_SLOT: Checking a slot for a property but you don't own it!- See Conor")
		#ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_MORS_MUTUAL_VEHICLE_BEING_DELIVERED()
	IF CURRENT_SAVED_VEHICLE_SLOT() >= 0
	AND IS_BIT_SET(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_RECREATE_AT_MORS_MUTUAL)
	AND IS_BIT_SET(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_OUT_GARAGE)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC



FUNC BOOL HANDLE_WARP_INTO_GARAGE()
	//if player isnt in vehicle or not driver we dont care so just go in.
	//NETWORK_INDEX personalVehNetID
	BOOl bPersonalVehicle
	BOOL bAnotherPlayerPersonalVehicle
	INT iSlotToUse, iDisplaySlotToUse
	BOOL bOtherGarage
	BOOL bStorage
	MODEL_NAMES vehicleModel
	tempCamOffset.vLoc = <<0,0,0>>
	tempCamOffset.vRot = <<0,0,0>>
	tempCamOffset.iFov = 0
	INT iDuration
	INT i
	REPEAT MAX_OWNED_PROPERTIES i
		IF GET_OWNED_PROPERTY(i) = iCurrentPropertyID
			iVehicleSaveOffset = GET_PROPERTY_SLOT_DISPLAY_START_INDEX(i)
		ENDIF
	ENDREPEAT
	VEHICLE_INDEX vehFadeOut
	
	FORCE_UNDERMAP_FOR_ENTRY(2)

	SWITCH warpInControl.iStage
		CASE PROP_ENTER_STAGE_INIT
			globalPropertyEntryData.iVehSlot = -1
			globalPropertyEntryData.iReplaceSlot = -1
			globalPropertyEntryData.iOldVehicleSlot = -1
			SET_REPLACING_VEHICLE_ON_ENTRY(FALSE)
			globalPropertyEntryData.iGarageIntroProperty = -1
			CLEAR_BIT(ilocalBS,LOCAL_BS_STARTED_CUTSCENE)
			CLEAR_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_CAR_FROM_STORAGE)
			CLEAR_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_CAR_REPLACEMENT)
			IF SHOULD_BYPASS_CHECKS_FOR_ENTRY()
				IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
					heistUnlockCar = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				ENDIF
				CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_WARP_INTO_GARAGE: bypassing checks going straight to run drive in CS")
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_WARPING_INTO_GARAGE_CS)
				IF CHECK_EVERYONE_IN_VEHICLE_IS_READY()
				AND NOT IS_ANYONE_TRYING_TO_ENTER_CURRENT_VEHICLE()
					g_HeistGarageDropoffPanStarted = TRUE
					IF g_bPropertyDropOff
						g_HeistApartmentDropoffPanStarted = TRUE
						CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_WARP_INTO_GARAGE: g_HeistApartmentDropoffPanStarted = TRUE")
					ENDIF
					SET_PROPERTY_ENTER_STAGE(PROP_ENTER_STAGE_FAKE_CUTSCENE_HEIST)
					globalPropertyEntryData.iGarageIntroProperty = iCurrentPropertyID
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_WARP_INTO_GARAGE: bypassing checks waiting for players in car to be ready")
				ENDIF
				bHadMissionCriticalEntityOnDropoff = TRUE
				PRINTLN("bHadMissionCriticalEntityOnDropoff Set True 1")
				BREAK
			ENDIF
			//bFadeOutWarpIntoGarage = FALSE	//Bug 1615873 (this doesn't work so commenting out)
			RESET_NET_TIMER(failSafeEntryTimer)
			CLEAR_BIT(iLocalBS2,LOCAL_BS2_VehBelongsToOtherGarage)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "LOCAL_BS2_VehBelongsToOtherGarage = FALSE")
			
			IF warpInWithDriverID != INVALID_PLAYER_INDEX()
			AND IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_WARPING_WITH_OWNER_IN_CAR)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_WARP_INTO_GARAGE: player is in car with owner jumping to car entry stage")
				SET_PROPERTY_ENTER_STAGE(PROP_ENTER_STAGE_VEHICLE)
			ELSE
				CLEAR_BIT(iLocalBS2,LOCAL_BS2_bCleanupOldPersonalVehicle)
				SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_ENTERING_GARAGE)
				IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
					warpInControl.vehID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					vehicleModel = GET_ENTITY_MODEL(warpInControl.vehID)
					IF DECOR_IS_REGISTERED_AS_TYPE("Player_Vehicle", DECOR_TYPE_INT) 
						IF DECOR_EXIST_ON(warpInControl.vehID, "Player_Vehicle") 
							IF DECOR_GET_INT(warpInControl.vehID, "Player_Vehicle") = NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID())
								bPersonalVehicle = TRUE
							ELSE
								bAnotherPlayerPersonalVehicle = TRUE
							ENDIF
						ENDIF
					ENDIF
					IF SHOULD_WARP_INTO_GARAGE_ABORT(warpInControl.vehID)//,vehicleModel,bPersonalVehicle)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "Player in a vehicle not allowed into garage. Aborting")
						IF g_bPropInteriorScriptRunning
						OR IS_BIT_SET(mpPropMaintain.iBS, MP_PROP_MAINTAIN_BS_LAUNCHED_INT_SCRIPT)
							globalPropertyEntryData.bLeftOrRejectedBuzzer = TRUE
							CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: globalPropertyEntryData.bLeftOrRejectedBuzzer = TRUE. Player in a vehicle not allowed into garage")
						ENDIF
						RESET_TO_ENTRANCE_TO_PROPERTY_STAGE()
						IF garageCarEnterCutStage != GARAGE_CAR_ENTER_CUTSCENE_INIT
							RESET_GARAGE_CAR_ENTER_CUTSCENE(6)	
						ENDIF
						IF garagePedEnterCutStage != GARAGE_PED_ENTER_CUTSCENE_INIT
							RESET_GARAGE_PED_ENTER_CUTSCENE()	
						ENDIF
						RETURN FALSE
					ENDIF
					IF (PLAYER_PED_ID() != GET_PED_IN_VEHICLE_SEAT(warpInControl.vehID, VS_DRIVER))
						SET_PROPERTY_ENTER_STAGE(PROP_ENTER_STAGE_NO_VEHICLE)
					ELSE
						IF DOES_PLAYER_HAVE_GROUP_ACCESS_TO_PROPERTY()
							IF globalPropertyEntryData.ownerID != INVALID_PLAYER_INDEX()
							AND IS_NET_PLAYER_OK(globalPropertyEntryData.ownerID,FALSE)
								IF NOT HAS_NET_TIMER_STARTED(iWalkInTimer)  //re-used timer to keep variables down
									START_NET_TIMER(iWalkInTimer,TRUE)  //re-used timer to keep variables down
									PRINTLN("HANDLE_WARP_INTO_GARAGE: starting timer")	
								ELSE
									
									IF REQUEST_GUEST_PARKING_FROM_PLAYER(globalPropertyEntryData.ownerID,iCurrentPropertyID,globalPropertyEntryData.iVehSlot)
										SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_GUEST_PARKING_REQUIRED)
										SET_PROPERTY_ENTER_STAGE(PROP_ENTER_STAGE_VEHICLE)
										CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_WARP_INTO_GARAGE: player entering with guest parking")
									ELIF HAS_NET_TIMER_EXPIRED(iWalkInTimer,10000,TRUE)  //re-used timer to keep variables down
										CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_WARP_INTO_GARAGE: group access but timer for guest parking expired.")
										SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_NOT_IN_A_VEHICLE)
										IF g_bPropInteriorScriptRunning
										OR IS_BIT_SET(mpPropMaintain.iBS, MP_PROP_MAINTAIN_BS_LAUNCHED_INT_SCRIPT)
											globalPropertyEntryData.bLeftOrRejectedBuzzer = TRUE
											CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: globalPropertyEntryData.bLeftOrRejectedBuzzer = TRUE. group access but timer for guest parking expired.")
										ENDIF
										RESET_TO_ENTRANCE_TO_PROPERTY_STAGE()
										IF garageCarEnterCutStage != GARAGE_CAR_ENTER_CUTSCENE_INIT
											RESET_GARAGE_CAR_ENTER_CUTSCENE(6)	
										ENDIF
										IF garagePedEnterCutStage != GARAGE_PED_ENTER_CUTSCENE_INIT
											RESET_GARAGE_PED_ENTER_CUTSCENE()	
										ENDIF

										RETURN FALSE
									ELSE
										CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_WARP_INTO_GARAGE: waiting for guest parking")
									ENDIF
								ENDIF
								BREAK
							ELSE
								CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_WARP_INTO_GARAGE: group access but owner is invalid! entering without vehicle")
								SET_PROPERTY_ENTER_STAGE(PROP_ENTER_STAGE_NO_VEHICLE)
							ENDIF
						ENDIF
						IF IS_ANYONE_TRYING_TO_ENTER_CURRENT_VEHICLE()
							CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_WARP_INTO_GARAGE: delaying entry player is entering the vehicle")
							BREAK
						ENDIF
						CDEBUG1LN(DEBUG_SAFEHOUSE, "Entering garage property ID is # ", mpProperties[iCurrentPropertyID].iIndex)
						IF bAnotherPlayerPersonalVehicle
							CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_WARP_INTO_GARAGE: player in another players personal vehicle not warping in with them")
							SET_PROPERTY_ENTER_STAGE(PROP_ENTER_STAGE_NO_VEHICLE)
						ELIF bPersonalVehicle
							MPSV_GET_DISPLAY_SLOT_FROM_SAVE_SLOT(CURRENT_SAVED_VEHICLE_SLOT(),globalPropertyEntryData.iVehSlot)
							IF CURRENT_SAVED_VEHICLE_SLOT() >= 0
							AND IS_BIT_SET(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_OUT_GARAGE)
							AND CHECK_CURRENT_VEHICLE_SLOT(vehicleModel, CURRENT_SAVED_VEHICLE_SLOT(),bOtherGarage,bStorage)
								SET_BIT(iLocalBS2,LOCAL_BS2_bCleanupOldPersonalVehicle)
								CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: bCleanupOldPersonalVehicle = TRUE - A")
								CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_WARP_INTO_GARAGE: Adding personal vehicle back into slot #",CURRENT_SAVED_VEHICLE_SLOT() ," that is was taken out of")
								SET_PROPERTY_ENTER_STAGE(PROP_ENTER_STAGE_VEHICLE)
							ELSE
								IF bOtherGarage
								OR bStorage
									IF bOtherGarage
									AND g_sMPTUNABLES.bdisable_transfer_vehicle_between_garage
										IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
											PRINT_HELP("PROPNOTRANS")
										ENDIF
										REINIT_NET_TIMER(tempPropertyExtMenu.exitDoorDelay,TRUE)
										globalPropertyEntryData.iVehSlot = -1
										RESET_TO_ENTRANCE_TO_PROPERTY_STAGE()
										CDEBUG1LN(DEBUG_SAFEHOUSE, "aborting vehicle garage transfer")
										RETURN FALSE
									ENDIF
									#IF IS_DEBUG_BUILD
										CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_WARP_INTO_GARAGE: Player is in a personal vehicle belonging to other garage: Last used vehicle = ",CURRENT_SAVED_VEHICLE_SLOT())
									#ENDIF
									SET_BIT(iLocalBS2,LOCAL_BS2_VehBelongsToOtherGarage)
									iDisplaySlotToUse = MP_SAVE_VEHICLE_GET_EMPTY_DISPLAY_SLOT(MP_SAVE_VEH_SEARCH_EMPTY_FIRST,FALSE,vehicleModel,iCurrentPropertyID)
									MPSV_GET_SAVE_SLOT_FROM_DISPLAY_SLOT(iDisplaySlotToUse,iSlotToUse)
									IF iSlotToUse < 0
										iSlotToUse = MP_SAVE_VEHICLE_GET_EMPTY_SAVE_SLOT(MP_SAVE_VEH_SEARCH_EMPTY_FIRST,FALSE, vehicleModel)
									ELSE
										CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_WARP_INTO_GARAGE: MP_SAVE_VEHICLE_GET_EMPTY_SAVE_SLOT: Empty display but save game data - 1??? SLOT #",iSlotToUse)
										SCRIPT_ASSERT("HANDLE_WARP_INTO_GARAGE: MP_SAVE_VEHICLE_GET_EMPTY_SAVE_SLOT: Empty display but save game data - 1??? -see conor ")
									ENDIF
									globalPropertyEntryData.iOldVehicleSlot = globalPropertyEntryData.iVehSlot
									IF iSlotToUse != -1
									AND iDisplaySlotToUse != -1
										globalPropertyEntryData.iVehSlot = iDisplaySlotToUse
										CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: bCleanupOldPersonalVehicle = TRUE - B")
										SET_BIT(iLocalBS2,LOCAL_BS2_bCleanupOldPersonalVehicle)
										CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_WARP_INTO_GARAGE: Player is in a personal vehicle belonging to other garage moving it from display slot #", globalPropertyEntryData.iOldVehicleSlot," to display slot #",globalPropertyEntryData.iVehSlot)
										SET_PROPERTY_ENTER_STAGE(PROP_ENTER_STAGE_VEHICLE)
									ELSE
//										IF globalPropertyEntryData.iVehSlot > mpProperties[iCurrentPropertyID].iGarageSize + iVehicleSaveOffset
//											globalPropertyEntryData.iReplaceSlot = globalPropertyEntryData.iVehSlot
//											CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_WARP_INTO_GARAGE: Personal vehicle is in slot higher #",globalPropertyEntryData.iVehSlot ,",than current garage size = ",mpProperties[iCurrentPropertyID].iGarageSize)
//										ENDIF
										iDisplaySlotToUse = MP_SAVE_VEHICLE_GET_EMPTY_DISPLAY_SLOT(MP_SAVE_VEH_SEARCH_EMPTY_FIRST,TRUE,vehicleModel,iCurrentPropertyID)
										MPSV_GET_SAVE_SLOT_FROM_DISPLAY_SLOT(iDisplaySlotToUse,iSlotToUse)
										IF iSlotToUse < 0
											iSlotToUse = MP_SAVE_VEHICLE_GET_EMPTY_SAVE_SLOT(MP_SAVE_VEH_SEARCH_EMPTY_FIRST,TRUE, vehicleModel)
										ELSE
											CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_WARP_INTO_GARAGE: MP_SAVE_VEHICLE_GET_EMPTY_SAVE_SLOT: Empty display but save game data - 2??? SLOT #",iSlotToUse)
											SCRIPT_ASSERT("HANDLE_WARP_INTO_GARAGE: MP_SAVE_VEHICLE_GET_EMPTY_SAVE_SLOT: Empty display but save game data - 2??? -see conor ")
										ENDIF
										IF iSlotToUse != -1
										AND iDisplaySlotToUse != -1
											globalPropertyEntryData.iVehSlot = iDisplaySlotToUse
											CLEAR_BIT(iLocalBS, LOCAL_BS_GARAGE_COMPLETELY_FULL)	
											CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_WARP_INTO_GARAGE: A Personal vehicle last used vehicle is INVALID going to replace another vehicle going to warning (will store in display slot) #",globalPropertyEntryData.iVehSlot)
											SET_PROPERTY_ENTER_STAGE(PROP_ENTER_STAGE_FULL)
										ELSE
											SET_BIT(iLocalBS, LOCAL_BS_GARAGE_COMPLETELY_FULL)
											SET_PROPERTY_ENTER_STAGE(PROP_ENTER_STAGE_FULL)
										ENDIF
									ENDIF
								ELSE
									#IF IS_DEBUG_BUILD
										CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_WARP_INTO_GARAGE: Player is in a personal vehicle but their last used vehicle is INVALID!- See Conor: Last used vehicle = ",CURRENT_SAVED_VEHICLE_SLOT())
										//SCRIPT_ASSERT("HANDLE_WARP_INTO_GARAGE: Player is in a personal vehicle but their last used vehicle is INVALID!- See Conor")
									#ENDIF
									
									iDisplaySlotToUse = MP_SAVE_VEHICLE_GET_EMPTY_DISPLAY_SLOT(MP_SAVE_VEH_SEARCH_EMPTY_FIRST,FALSE,vehicleModel,iCurrentPropertyID)
									IF iDisplaySlotToUse >= 0
										MPSV_GET_SAVE_SLOT_FROM_DISPLAY_SLOT(iDisplaySlotToUse,iSlotToUse)
										IF iSlotToUse < 0
											iSlotToUse = MP_SAVE_VEHICLE_GET_EMPTY_SAVE_SLOT(MP_SAVE_VEH_SEARCH_EMPTY_FIRST,FALSE, vehicleModel)
										ELSE
											CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_WARP_INTO_GARAGE: MP_SAVE_VEHICLE_GET_EMPTY_SAVE_SLOT: Empty display but save game data - 3??? SLOT #",iSlotToUse)
											SCRIPT_ASSERT("HANDLE_WARP_INTO_GARAGE: MP_SAVE_VEHICLE_GET_EMPTY_SAVE_SLOT: Empty display but save game data - 3??? -see conor ")
										ENDIF
									ENDIF
									IF iSlotToUse != -1
									AND iDisplaySlotToUse != -1
										IF CURRENT_SAVED_VEHICLE_SLOT() >= 0
											globalPropertyEntryData.iOldVehicleSlot = globalPropertyEntryData.iVehSlot
										ENDIF
										globalPropertyEntryData.iVehSlot = iDisplaySlotToUse
										CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: bCleanupOldPersonalVehicle = TRUE - B")
										SET_BIT(iLocalBS2,LOCAL_BS2_bCleanupOldPersonalVehicle)
										CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_WARP_INTO_GARAGE: Personal vehicle last used vehicle is INVALID putting car in display slot #",globalPropertyEntryData.iVehSlot)
										SET_PROPERTY_ENTER_STAGE(PROP_ENTER_STAGE_VEHICLE)
									ELSE
										IF globalPropertyEntryData.iVehSlot >= mpProperties[iCurrentPropertyID].iGarageSize + iVehicleSaveOffset
											globalPropertyEntryData.iOldVehicleSlot = globalPropertyEntryData.iVehSlot
											SET_BIT(iLocalBS2,LOCAL_BS2_VehBelongsToStorage)
											SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_CAR_FROM_STORAGE)
											CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_WARP_INTO_GARAGE: Personal vehicle is in display slot higher #",globalPropertyEntryData.iVehSlot ,",than current garage size = ",mpProperties[iCurrentPropertyID].iGarageSize)
										ENDIF
										iDisplaySlotToUse = MP_SAVE_VEHICLE_GET_EMPTY_DISPLAY_SLOT(MP_SAVE_VEH_SEARCH_EMPTY_FIRST,TRUE,vehicleModel,iCurrentPropertyID)
										MPSV_GET_SAVE_SLOT_FROM_DISPLAY_SLOT(iDisplaySlotToUse,iSlotToUse)
										IF iSlotToUse < 0
											iSlotToUse = MP_SAVE_VEHICLE_GET_EMPTY_SAVE_SLOT(MP_SAVE_VEH_SEARCH_EMPTY_FIRST,TRUE, vehicleModel)
										ELSE
											CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_WARP_INTO_GARAGE: MP_SAVE_VEHICLE_GET_EMPTY_SAVE_SLOT: Empty display but save game data - 4??? SLOT #",iSlotToUse)
											SCRIPT_ASSERT("HANDLE_WARP_INTO_GARAGE: MP_SAVE_VEHICLE_GET_EMPTY_SAVE_SLOT: Empty display but save game data - 4??? -see conor ")
										ENDIF
										IF iSlotToUse != -1
										AND iDisplaySlotToUse != -1
											globalPropertyEntryData.iVehSlot = iDisplaySlotToUse
											CLEAR_BIT(iLocalBS, LOCAL_BS_GARAGE_COMPLETELY_FULL)	
											CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_WARP_INTO_GARAGE: B Personal vehicle last used vehicle is INVALID going to replace another vehicle going to warning (will store in display slot) #",globalPropertyEntryData.iVehSlot)
											SET_PROPERTY_ENTER_STAGE(PROP_ENTER_STAGE_FULL)
										ELSE
											SET_BIT(iLocalBS, LOCAL_BS_GARAGE_COMPLETELY_FULL)
											SET_PROPERTY_ENTER_STAGE(PROP_ENTER_STAGE_FULL)
										ENDIF
									ENDIF
								ENDIF
							ENDIF	
						ELSE
							
							iDisplaySlotToUse = MP_SAVE_VEHICLE_GET_EMPTY_DISPLAY_SLOT(MP_SAVE_VEH_SEARCH_EMPTY_FIRST,FALSE,vehicleModel,iCurrentPropertyID)
							IF iDisplaySlotToUse >= 0
								MPSV_GET_SAVE_SLOT_FROM_DISPLAY_SLOT(iDisplaySlotToUse,iSlotToUse)
								IF iSlotToUse < 0
									iSlotToUse = MP_SAVE_VEHICLE_GET_EMPTY_SAVE_SLOT(MP_SAVE_VEH_SEARCH_EMPTY_FIRST,FALSE, vehicleModel)
								ELSE
									CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_WARP_INTO_GARAGE: MP_SAVE_VEHICLE_GET_EMPTY_SAVE_SLOT: Empty display but save game data - 5??? SLOT #",iSlotToUse)
									SCRIPT_ASSERT("HANDLE_WARP_INTO_GARAGE: MP_SAVE_VEHICLE_GET_EMPTY_SAVE_SLOT: Empty display but save game data - 5??? -see conor ")
								ENDIF
							ENDIF
							IF iSlotToUse != -1
							AND iDisplaySlotToUse != -1
								globalPropertyEntryData.iVehSlot = iDisplaySlotToUse
								IF NOT IS_PERSONAL_VEHICLE_DRIVEABLE_IN_FREEMODE()
									IF NOT IS_MORS_MUTUAL_VEHICLE_BEING_DELIVERED()
										SET_LAST_USED_VEHICLE_SLOT(iSlotToUse)
										CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_WARP_INTO_GARAGE: setting car you are driving into garage as last used vehicle slot ID #",iSlotToUse)
									ENDIF
								ENDIF
								CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_WARP_INTO_GARAGE: Not Personal vehicle and an empty slot available adding vehicle to display slot ID #",globalPropertyEntryData.iVehSlot)
								SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_ADDING_NEW_VEHICLE)
								CDEBUG1LN(DEBUG_SAFEHOUSE, "GLOBAL_PROPERTY_ENTRY_BS_ADDING_NEW_VEHICLE - TRUE - 1")
								SET_PROPERTY_ENTER_STAGE(PROP_ENTER_STAGE_VEHICLE)
							ELSE
								iDisplaySlotToUse = MP_SAVE_VEHICLE_GET_EMPTY_DISPLAY_SLOT(MP_SAVE_VEH_SEARCH_EMPTY_FIRST,TRUE,vehicleModel,iCurrentPropertyID)
								IF iDisplaySlotToUse >= 0
									MPSV_GET_SAVE_SLOT_FROM_DISPLAY_SLOT(iDisplaySlotToUse,iSlotToUse)
									IF iSlotToUse < 0
										iSlotToUse = MP_SAVE_VEHICLE_GET_EMPTY_SAVE_SLOT(MP_SAVE_VEH_SEARCH_EMPTY_FIRST,TRUE, vehicleModel)
//									ELSE
//										CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_WARP_INTO_GARAGE: MP_SAVE_VEHICLE_GET_EMPTY_SAVE_SLOT: Empty display but save game data - 6??? SLOT #",iSlotToUse)
//										SCRIPT_ASSERT("HANDLE_WARP_INTO_GARAGE: MP_SAVE_VEHICLE_GET_EMPTY_SAVE_SLOT: Empty display but save game data - 6??? -see conor ")
									ENDIF
								ENDIF
								IF iSlotToUse != -1
								AND iDisplaySlotToUse != -1
									globalPropertyEntryData.iVehSlot = iDisplaySlotToUse
									CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_WARP_INTO_GARAGE: Not Personal vehicle and only slot available is car out one display Slot ID #",globalPropertyEntryData.iVehSlot)
									CLEAR_BIT(iLocalBS, LOCAL_BS_GARAGE_COMPLETELY_FULL)
									SET_PROPERTY_ENTER_STAGE(PROP_ENTER_STAGE_FULL)
								ELSE
									SET_BIT(iLocalBS, LOCAL_BS_GARAGE_COMPLETELY_FULL)
									CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_WARP_INTO_GARAGE: Not Personal vehicle and not slot available is car out one display Slot ID #",globalPropertyEntryData.iVehSlot)
									SET_PROPERTY_ENTER_STAGE(PROP_ENTER_STAGE_FULL)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF (GET_PROPERTY_SIZE_TYPE(mpProperties[iCurrentPropertyID].iIndex) != PROP_SIZE_TYPE_LARGE_APT
					AND mpProperties[iCurrentPropertyID].iIndex != PROPERTY_LOW_APT_7)
					OR IS_PROPERTY_STILT_APARTMENT(iCurrentPropertyID)
						IF NOT IS_BIT_SET(ilocalBS,LOCAL_BS_STARTED_FADE)
							DO_SCREEN_FADE_OUT(500)
							SET_BIT(ilocalBS,LOCAL_BS_STARTED_FADE)
						ENDIF
					ENDIF
					IF NOT IS_PLAYER_GETTING_IN_OR_OUT_OF_VEHICLE(PLAYER_ID())
					AND NOT IS_LOCAL_PLAYER_ENTERING_OR_EXITING_A_VEHICLE()
						//CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
						SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_NOT_IN_A_VEHICLE)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "Entering garage no vehicle property ID is # ", mpProperties[iCurrentPropertyID].iIndex)
						SET_PROPERTY_ENTER_STAGE(PROP_ENTER_STAGE_NO_VEHICLE)
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE PROP_ENTER_STAGE_FULL
			//IF IS_BIT_SET(iLocalBS, LOCAL_BS_GARAGE_COMPLETELY_FULL)
				warpInControl.iWarningScreenButtonBS = FE_WARNING_OKCANCEL
				IF IS_BIT_SET(iLocalBS2,LOCAL_BS2_VehBelongsToStorage)
					SET_WARNING_MESSAGE_WITH_HEADER("CUST_GAR_FULL","CUST_GAR_REP3",warpInControl.iWarningScreenButtonBS)
				ELIF IS_BIT_SET(iLocalBS2,LOCAL_BS2_VehBelongsToOtherGarage)
					SET_WARNING_MESSAGE_WITH_HEADER("CUST_GAR_FULL","CUST_GAR_REP2",warpInControl.iWarningScreenButtonBS)
				ELSE
					SET_WARNING_MESSAGE_WITH_HEADER("CUST_GAR_FULL","CUST_GAR_REP",warpInControl.iWarningScreenButtonBS)
				ENDIF
				IF NOT IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
				OR NOT IS_NET_PLAYER_OK(PLAYER_ID())
					REINIT_NET_TIMER(tempPropertyExtMenu.exitDoorDelay,TRUE)
					RESET_TO_ENTRANCE_TO_PROPERTY_STAGE()
					RETURN FALSE
				ELSE
					
					vehFadeOut = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					IF NOT IS_VEHICLE_DRIVEABLE(vehFadeOut)
					OR IS_VEHICLE_ATTACHED_TO_ANY_CARGOBOB(vehFadeOut)
						REINIT_NET_TIMER(tempPropertyExtMenu.exitDoorDelay,TRUE)
						RESET_TO_ENTRANCE_TO_PROPERTY_STAGE()
						CDEBUG1LN(DEBUG_SAFEHOUSE, "vehicle destroyed or attached to cargobob while entering aborting")
						RETURN FALSE
					ENDIF
					IF NOT IS_BIT_SET(warpInControl.iButtonBS,PROP_BUTTON_BS_PRESSED_ACCEPT)
						IF (IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT) OR IS_BIT_SET(iLocalBS2,LOCAL_BS2_TriggeredAcceptWarning))
						AND CAN_PLAYER_USE_MENU_INPUT()
						AND NOT IS_ANYONE_TRYING_TO_ENTER_CURRENT_VEHICLE()
							SET_BIT(iLocalBS2,LOCAL_BS2_TriggeredAcceptWarning)
							SET_BIT(ilocalBS,LOCAL_BS_SKIP_ENTER_FADE)
							IF IS_BIT_SET(ilocalBS,LOCAL_BS_SKIP_ENTER_FADE)
								CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Skipping fade as bit set")
								SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_WARPING_INTO_GARAGE_FADE)
								GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iEnteringPropertyID = iCurrentPropertyID
								IF IS_BIT_SET(iLocalBS2,LOCAL_BS2_SetEntryData)
									GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.EnteringPropertyOwner = globalPropertyEntryData.ownerID
									#IF IS_DEBUG_BUILD
									IF IS_NET_PLAYER_OK(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.EnteringPropertyOwner)
										PRINTLN("AM_MP_PROPERTY_EXT: setting EnteringPropertyOwner 5 =  ",GET_PLAYER_NAME(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.EnteringPropertyOwner))
									ELSE
										PRINTLN("AM_MP_PROPERTY_EXT: setting EnteringPropertyOwner 6 =  #",NATIVE_TO_INT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.EnteringPropertyOwner))
									ENDIF
									#ENDIF
								ENDIF
							ELSE
								SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_WARPING_INTO_GARAGE_CS)
								GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iEnteringPropertyID = iCurrentPropertyID
								IF IS_BIT_SET(iLocalBS2,LOCAL_BS2_SetEntryData)
									GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.EnteringPropertyOwner = globalPropertyEntryData.ownerID
									#IF IS_DEBUG_BUILD
									IF IS_NET_PLAYER_OK(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.EnteringPropertyOwner)
										PRINTLN("AM_MP_PROPERTY_EXT: setting EnteringPropertyOwner 7 =  ",GET_PLAYER_NAME(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.EnteringPropertyOwner))
									ELSE
										PRINTLN("AM_MP_PROPERTY_EXT: setting EnteringPropertyOwner 8 =  #",NATIVE_TO_INT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.EnteringPropertyOwner))
									ENDIF
									#ENDIF
								ENDIF
							ENDIF
							CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_WARP_INTO_GARAGE: player is attempting to enter garage after accepting warning waiting for passengers to be ready")
							IF CHECK_EVERYONE_IN_VEHICLE_IS_READy()
								NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
								vehFadeOut = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
								IF GET_PED_IN_VEHICLE_SEAT(vehFadeOut) = PLAYER_PED_ID()
									SET_ENTITY_VISIBLE(vehFadeOut,FALSE)
									SET_ENTITY_VISIBLE(PLAYER_PED_ID(),FALSE)
									SET_VEHICLE_FIXED(vehFadeOut)
							        SET_ENTITY_HEALTH(vehFadeOut, 1000) 
							        SET_VEHICLE_ENGINE_HEALTH(vehFadeOut, 1000) 
							        SET_VEHICLE_PETROL_TANK_HEALTH(vehFadeOut, 1000) 
									SET_BIT(iLocalBS2,LOCAL_BS2_bFadeOutWarpIntoGarage)
									SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_CAR_MADE_INVISIBLE_ON_EXTERIOR)
									CDEBUG1LN(DEBUG_SAFEHOUSE, "NETWORK_FADE_OUT_ENTITY: called from HANDLE_WARP_INTO_GARAGE()")
								ENDIF
								SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_NOT_IN_A_VEHICLE)
		//						ELSE
		//							
		////							SET_ENTITY_VISIBLE(PLAYER_PED_ID(),FALSE)
		////							SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_CAR_MADE_INVISIBLE_ON_EXTERIOR)
		////							CDEBUG1LN(DEBUG_SAFEHOUSE, "NETWORK_FADE_OUT_ENTITY: called from HANDLE_WARP_INTO_GARAGE() - PED")
		////							SET_BIT(iLocalBS2,LOCAL_BS2_bFadeOutWarpIntoGarage)
		//						ENDIF
								PLAY_SOUND_FRONTEND(-1, "OK", "HUD_FRONTEND_DEFAULT_SOUNDSET")
								MP_SAVE_VEHICLE_STORE_CAR_DETAILS_STRUCT(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),globalPropertyEntryData.replacingVehicle,TRUE)
								
								// Kenneth R. - We also need to cache some info for the car app.
								globalPropertyEntryData.bCachedModInfo_Setup = FALSE
								VEHICLE_INDEX vehID
								vehID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
								IF GET_NUM_MOD_KITS(vehID) > 0
									SET_VEHICLE_MOD_KIT(vehID, 0)
								ENDIF
								IF GET_VEHICLE_MOD_KIT(vehID) >= 0
									globalPropertyEntryData.iCachedModInfo_EngineCount 		= GET_NUM_VEHICLE_MODS(vehID, MOD_ENGINE)+1
									globalPropertyEntryData.iCachedModInfo_BrakesCount 		= GET_NUM_VEHICLE_MODS(vehID, MOD_BRAKES)+1
									globalPropertyEntryData.iCachedModInfo_ExhaustCount 	= GET_NUM_VEHICLE_MODS(vehID, MOD_EXHAUST)+1
									globalPropertyEntryData.iCachedModInfo_WheelsCount 		= GET_NUM_VEHICLE_MODS(vehID, MOD_WHEELS)+1
									globalPropertyEntryData.iCachedModInfo_HornCount 		= GET_NUM_VEHICLE_MODS(vehID, MOD_HORN)+1
									globalPropertyEntryData.iCachedModInfo_ArmourCount 		= GET_NUM_VEHICLE_MODS(vehID, MOD_ARMOUR)+1
									globalPropertyEntryData.iCachedModInfo_SuspensionCount 	= GET_NUM_VEHICLE_MODS(vehID, MOD_SUSPENSION)+1
									globalPropertyEntryData.iCachedModInfo_Colours 			= GET_VEHICLE_COLOURS_WHICH_CAN_BE_SET(vehID)
									globalPropertyEntryData.eCachedModInfo_MKT 				= GET_VEHICLE_MOD_KIT_TYPE(vehID)
									globalPropertyEntryData.fCachedModInfo_PriceModifier 	= GET_VEHICLE_MOD_PRICE_MODIFIER(vehID)
									globalPropertyEntryData.bCachedModInfo_Setup 			= TRUE
								ENDIF
								
								SET_REPLACING_VEHICLE_ON_ENTRY(TRUE)
								
								IF globalPropertyEntryData.iReplaceSlot >= 0
									CLEAR_MP_SAVED_VEHICLE_SLOT(globalPropertyEntryData.iReplaceSlot)
									CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT; Destroying previous personal vehicle outside range of garage size.")
								ENDIF
								globalPropertyEntryData.iVehSlot = -1
								CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: bCleanupOldPersonalVehicle = TRUE - C")
								NET_PRINT("HANDLE_WARP_INTO_GARAGE entering garage going to replace a vehicle") NET_NL()
								SET_PROPERTY_ENTER_STAGE(PROP_ENTER_STAGE_VEHICLE)
								SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_REPLACING_CAR)
								SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_CAR_REPLACEMENT)
								SET_BIT(iLocalBS2,LOCAL_BS2_bCleanupOldPersonalVehicle)
								CLEAR_BIT(ilocalBS,LOCAL_BS2_TriggeredAcceptWarning)
							ENDIF
							SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_ADDING_NEW_VEHICLE)
							CDEBUG1LN(DEBUG_SAFEHOUSE, "GLOBAL_PROPERTY_ENTRY_BS_ADDING_NEW_VEHICLE - TRUE - 2")
							RETURN FALSE
						ENDIF
					ELSE
						IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT)
							CLEAR_BIT(warpInControl.iButtonBS,PROP_BUTTON_BS_PRESSED_ACCEPT)
						ENDIF
					ENDIF
					IF NOT IS_BIT_SET(warpInControl.iButtonBS,PROP_BUTTON_BS_PRESSED_CANCEL)
						IF (IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_CANCEL) OR IS_SYSTEM_UI_BEING_DISPLAYED())
						AND CAN_PLAYER_USE_MENU_INPUT()
						
							PLAY_SOUND_FRONTEND(-1, "CANCEL", "HUD_FRONTEND_DEFAULT_SOUNDSET")
							NET_PRINT("HANDLE_WARP_INTO_GARAGE NOT entering garage avoiding overwritting vehicle") NET_NL()
							globalPropertyEntryData.bLeftOrRejectedBuzzer = TRUE
							globalPropertyEntryData.iVehSlot = -1
							globalPropertyEntryData.iReplaceSlot = -1
							globalPropertyEntryData.iOldVehicleSlot = -1
							CLEAR_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_CAR_FROM_STORAGE)
							CLEAR_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_CAR_REPLACEMENT)
							REINIT_NET_TIMER(tempPropertyExtMenu.exitDoorDelay,TRUE)
							RESET_TO_ENTRANCE_TO_PROPERTY_STAGE()
						ENDIF
					ELSE
						IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_CANCEL)
							CLEAR_BIT(warpInControl.iButtonBS,PROP_BUTTON_BS_PRESSED_CANCEL)
						ENDIF
					ENDIF
				ENDIF
//			//ELSE
//				IF globalPropertyEntryData.iVehSlot = -1
//					NET_PRINT("HANDLE_WARP_INTO_GARAGE globalPropertyEntryData.iVehSlot = -1 and garage is full entering on foot!") NET_NL()
//					SET_PROPERTY_ENTER_STAGE(PROP_ENTER_STAGE_NO_VEHICLE)
//				ELSE
//					warpInControl.iWarningScreenButtonBS = FE_WARNING_OKCANCEL
//					SET_WARNING_MESSAGE_WITH_HEADER("GAR_REP_WARNH", "GAR_FULL_REPB1",warpInControl.iWarningScreenButtonBS,"GAR_FULL_REPB2")
//					IF NOT IS_BIT_SET(warpInControl.iButtonBS,PROP_BUTTON_BS_PRESSED_ACCEPT)
//						IF IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT)
//							//PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FREEMODE_SOUNDSET")
//							NET_PRINT("HANDLE_WARP_INTO_GARAGE entering garage overwritting current personal vehicle") NET_NL()
//							MP_SAVE_VEHICLE_STORE_CAR_DETAILS_IN_SLOT(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),globalPropertyEntryData.iVehSlot,TRUE,TRUE,TRUE,TRUE)
//							REQUEST_SAVE(STAT_SAVETYPE_SCRIPT_MP_GLOBALS)
//							SET_PROPERTY_ENTER_STAGE(PROP_ENTER_STAGE_VEHICLE)
//							//SET_BIT(ilocalBS,LOCAL_BS_SKIP_ENTER_FADE)
//						ENDIF
//					ELSE
//						IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT)
//							CLEAR_BIT(warpInControl.iButtonBS,PROP_BUTTON_BS_PRESSED_ACCEPT)
//						ENDIF
//					ENDIF	
//					IF NOT IS_BIT_SET(warpInControl.iButtonBS,PROP_BUTTON_BS_PRESSED_CANCEL)
//						IF IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_CANCEL)
//							//PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FREEMODE_SOUNDSET")
//							NET_PRINT("HANDLE_WARP_INTO_GARAGE NOT entering garage avoiding overwritting vehicle") NET_NL()
//							REINIT_NET_TIMER(tempPropertyExtMenu.exitDoorDelay,TRUE)
//							RESET_TO_ENTRANCE_TO_PROPERTY_STAGE()
//						ENDIF
//					ELSE
//						IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_CANCEL)
//							CLEAR_BIT(warpInControl.iButtonBS,PROP_BUTTON_BS_PRESSED_CANCEL)
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
		BREAK
		
		CASE PROP_ENTER_STAGE_NO_VEHICLE
			
			IF NOT IS_BIT_SET(ilocalBS,LOCAL_BS_STARTED_FADE)
				IF NOT IS_BIT_SET(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_ENTER_GARAGE)
					SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_ENTER_GARAGE)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: GLOBAL_PROPERTY_ENTRY_BS_ENTER_GARAGE set -4 ")
				ENDIF
				
				globalPropertyEntryData.iGarageIntroProperty = iCurrentPropertyID
				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: - globalPropertyEntryData.iGarageIntroProperty = ", iCurrentPropertyID)
				IF DO_GARAGE_PED_ENTER_CUTSCENE(FALSE)
					IF globalPropertyEntryData.iGarageIntroProperty = iCurrentPropertyID
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: - CLEARING globalPropertyEntryData.iGarageIntroProperty = ", iCurrentPropertyID, " ... = -1")
						globalPropertyEntryData.iGarageIntroProperty = -1
					ENDIF
					IF IS_NET_PLAYER_OK(PLAYER_ID())
						IF NOT IS_BIT_SET(ilocalBS,LOCAL_BS_SKIP_ENTER_FADE)
							SET_BIT(ilocalBS,LOCAL_BS_NO_FADE)
						ENDIF
						IF NOT IS_BIT_SET(ilocalBS,LOCAL_BS_NO_FADE)
							IF IS_BIT_SET(ilocalBS,LOCAL_BS_SKIP_ENTER_FADE)
								CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: setting skip of fade due to bit being set (PROP_ENTER_STAGE_NO_VEHICLE)")
								DO_SCREEN_FADE_OUT(0)
							ELSE
								DO_SCREEN_FADE_OUT(500)
							ENDIF
						ENDIF
						PLAY_ENTERING_GARAGE_SOUND()
						//SET_BIT(ilocalBS,LOCAL_BS_NO_FADE)
						warpInControl.ibuttonBS = 0
						CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_WARP_INTO_GARAGE: setting fade started") 
						SET_BIT(ilocalBS,LOCAL_BS_STARTED_FADE)
					ELSE
						SET_BIT(ilocalBS,LOCAL_BS_CLEANUP_MP_PROPERTIES)
						//CLEANUP_MP_PROPERTIES(control)
					ENDIF
				ENDIF
			ELSE
				//Rowan c, fix for PT 1451570
				IF IS_BIT_SET(ilocalBS,LOCAL_BS_NO_FADE)
				OR IS_SCREEN_FADED_OUT()
				OR IS_BIT_SET(ilocalBS,LOCAL_BS_STARTED_CUTSCENE)
					//SET_BIT(g_iExteriorScriptProcessingWarpBS,0)	
					IF NOT IS_BIT_SET(ilocalBS,LOCAL_BS_STARTED_CUTSCENE)
						IF NOT IS_BIT_SET(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_ENTER_GARAGE)
							SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_ENTER_GARAGE)
							CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: GLOBAL_PROPERTY_ENTRY_BS_ENTER_GARAGE set -3 ")
						ENDIF
						IF NOT IS_BIT_SET(iMP_Prop_RetainBS,MP_PROP_RETAIN_VEH_TRUE)
							SET_BIT(iMP_Prop_RetainBS,MP_PROP_RETAIN_VEH_TRUE)
						ELSE
							IF IS_BIT_SET( iMP_Prop_RetainBS,MP_PROP_RETAIN_VEH_RETAINED)
								SET_BIT(ilocalBS,LOCAL_BS_STARTED_CUTSCENE)
							ENDIF
						ENDIF
						IF IS_PROPERTY_CLUBHOUSE(iCurrentPropertyID)
						OR IS_PROPERTY_OFFICE(iCurrentPropertyID)
							globalPropertyEntryData.bWalkInSeqDone = TRUE
						ENDIF
					ELSE
						IF NOT IS_BIT_SET(ilocalBS,LOCAL_BS_WARPED_IN_GARAGE)
							IF NOT IS_BIT_SET(ilocalBS,LOCAL_BS_ENTER_NO_WARP)
								IF IS_BIT_SET(ilocalBS,LOCAL_BS_BUZZER_TRIGGERED_LOAD_SCENE)
									IF (NOT IS_NEW_LOAD_SCENE_ACTIVE() OR IS_NEW_LOAD_SCENE_LOADED())
									//AND IS_INTERIOR_READY(theInterior)
										SET_ENTITY_COORDS(PLAYER_PED_ID(),mpProperties[iCurrentPropertyID].garage.vInPlayerLoc)
										SET_ENTITY_HEADING(PLAYER_PED_ID(),mpProperties[iCurrentPropertyID].garage.fInPlayerHeading)
										CLEAR_AREA(mpProperties[iCurrentPropertyID].garage.vInPlayerLoc,30,TRUE)
										CLEAR_AREA_OF_VEHICLES(mpProperties[iCurrentPropertyID].garage.vInPlayerLoc,30,FALSE,FALSE,TRUE,TRUE)	//This will only clear the Wrecked Vehicles due to that flag being set.
										SET_BIT(ilocalBS,LOCAL_BS_WARPED_IN_GARAGE)
										CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: warp into garage interior has completed (loaded scene outside0")
									#IF IS_DEBUG_BUILD
									ELSE
										IF IS_NEW_LOAD_SCENE_LOADED()
											CDEBUG1LN(DEBUG_SAFEHOUSE, "waiting for new load scene- 123")
										ENDIF
										//IF IS_INTERIOR_READY(theInterior)
										//	CDEBUG1LN(DEBUG_SAFEHOUSE, "waiting for interior- 123")
										//ENDIF
									#ENDIF
									ENDIF
								ELSE
									IF globalPropertyEntryData.bInteriorWarpDone
									//IF NET_WARP_TO_COORD(mpProperties[iCurrentPropertyID].garage.vInPlayerLoc,mpProperties[iCurrentPropertyID].garage.fInPlayerHeading,FALSE,FALSE)
										//CLEAR_AREA(mpProperties[iCurrentPropertyID].garage.vInPlayerLoc,30,TRUE)
										//CLEAR_AREA_OF_VEHICLES(mpProperties[iCurrentPropertyID].garage.vInPlayerLoc,30,FALSE,FALSE,TRUE,TRUE)
										SET_BIT(ilocalBS,LOCAL_BS_WARPED_IN_GARAGE)
										CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: warp into garage interior has completed")
									ENDIF
								ENDIF
							ELSE
								CLEAR_AREA(mpProperties[iCurrentPropertyID].garage.vInPlayerLoc,30,TRUE)
								CLEAR_AREA_OF_VEHICLES(mpProperties[iCurrentPropertyID].garage.vInPlayerLoc,30,FALSE,FALSE,TRUE,TRUE)	//This will only clear the Wrecked Vehicles due to that flag being set.
								SET_BIT(ilocalBS,LOCAL_BS_WARPED_IN_GARAGE)	
								CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: warp into garage interior has completed")
							ENDIF
						ELSE
							IF IS_BIT_SET(ilocalBS,LOCAL_BS_BUZZER_TRIGGERED_LOAD_SCENE)
								NEW_LOAD_SCENE_STOP()
							ENDIF
							RETURN TRUE
						ENDIF
					ENDIF
				ELSE
					IF NOT IS_SCREEN_FADING_OUT()
					AND NOT IS_SCREEN_FADED_OUT()
					AND NOT IS_SCREEN_FADING_IN()
						IF NOT IS_BIT_SET(ilocalBS,LOCAL_BS_NO_FADE)
							IF IS_BIT_SET(ilocalBS,LOCAL_BS_SKIP_ENTER_FADE)
								CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: setting skip of fade due to bit being set (PROP_ENTER_STAGE_NO_VEHICLE- FAILSAFE)")
								DO_SCREEN_FADE_OUT(0)
							ELSE
								CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: fade (PROP_ENTER_STAGE_NO_VEHICLE- FAILSAFE)")
								DO_SCREEN_FADE_OUT(500)
							ENDIF
						ENDIF
					ENDIF
					IF NOT HAS_NET_TIMER_STARTED(failSafeEntryTimer)
						START_NET_TIMER(failSafeEntryTimer,TRUE)
					ELSE
						IF HAS_NET_TIMER_EXPIRED(failSafeEntryTimer,10000,TRUE)
							CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT:HANDLE_WARP_INTO_GARAGE failed failsafe timer was triggered(PROP_ENTER_STAGE_NO_VEHICLE)")
							RESET_TO_ENTRANCE_TO_PROPERTY_STAGE()
						ENDIF
					ENDIF
					CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_WARP_INTO_GARAGE: stuck waiting for fade") 
				ENDIF
			ENDIF
		BREAK
		
		CASE PROP_ENTER_STAGE_VEHICLE
			IF NOT IS_BIT_SET(iLocalBS2,LOCAL_BS2_AddedEnteringDecorator)
				IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
					vehFadeOut = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					IF GET_PED_IN_VEHICLE_SEAT(vehFadeOut) = PLAYER_PED_ID()
						APPLY_DRIVING_INTO_GARAGE_DECORATOR(vehFadeOut,1)
						IF GET_ENTITY_MODEL(vehFadeOut) = DELUXO
						OR GET_ENTITY_MODEL(vehFadeOut) = OPPRESSOR2
							SET_DISABLE_HOVER_MODE_FLIGHT(vehFadeOut,TRUE)
							SET_SPECIAL_FLIGHT_MODE_TARGET_RATIO(vehFadeOut,0)
						ENDIF
					ENDIF
				ENDIF
			ENDIF	
			IF fakeGarageCarEnterCutsceneStage > FAKE_GARAGE_CAR_ENTER_CUTSCENE_START
				HIDE_HUD_AND_RADAR_THIS_FRAME()
			ENDIF
			IF NOT IS_BIT_SET(ilocalBS,LOCAL_BS_STARTED_FADE)
				IF NOT IS_BIT_SET(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_ENTER_GARAGE)
					SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_ENTER_GARAGE)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: GLOBAL_PROPERTY_ENTRY_BS_ENTER_GARAGE set -2 ")
				ENDIF
				IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_WARPING_WITH_OWNER_IN_CAR)
					IF warpInWithDriverID != INVALID_PLAYER_INDEX()
						IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(warpInWithDriverID)].propertyDetails.iBS,PROPERTY_BROADCAST_BS_WARPING_INTO_GARAGE_FADE)
							CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Skipping fade as owner has skipped it")
							SET_BIT(ilocalBS,LOCAL_BS_SKIP_ENTER_FADE)
							IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(warpInWithDriverID)].propertyDetails.iBS,PROPERTY_BROADCAST_BS_REPLACING_CAR)
								SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_CAR_REPLACEMENT)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				IF IS_BIT_SET(ilocalBS,LOCAL_BS_SKIP_ENTER_FADE)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Skipping fade as bit set")
					SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_WARPING_INTO_GARAGE_FADE)
					GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iEnteringPropertyID = iCurrentPropertyID
					IF IS_BIT_SET(iLocalBS2,LOCAL_BS2_SetEntryData)
						GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.EnteringPropertyOwner = globalPropertyEntryData.ownerID
						#IF IS_DEBUG_BUILD
						IF IS_NET_PLAYER_OK(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.EnteringPropertyOwner)
							PRINTLN("AM_MP_PROPERTY_EXT: setting EnteringPropertyOwner 3 =  ",GET_PLAYER_NAME(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.EnteringPropertyOwner))
						ELSE
							PRINTLN("AM_MP_PROPERTY_EXT: setting EnteringPropertyOwner 4 =  #",NATIVE_TO_INT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.EnteringPropertyOwner))
						ENDIF
						#ENDIF
					ENDIF
				ELSE
					SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_WARPING_INTO_GARAGE_CS)
					GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iEnteringPropertyID = iCurrentPropertyID
					IF IS_BIT_SET(iLocalBS2,LOCAL_BS2_SetEntryData)
						GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.EnteringPropertyOwner = globalPropertyEntryData.ownerID
						#IF IS_DEBUG_BUILD
						IF IS_NET_PLAYER_OK(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.EnteringPropertyOwner)
							PRINTLN("AM_MP_PROPERTY_EXT: setting EnteringPropertyOwner 1 =  ",GET_PLAYER_NAME(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.EnteringPropertyOwner))
						ELSE
							PRINTLN("AM_MP_PROPERTY_EXT: setting EnteringPropertyOwner 2 =  #",NATIVE_TO_INT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.EnteringPropertyOwner))
						ENDIF
						#ENDIF
					ENDIF
				ENDIF
				
				
				IF CHECK_EVERYONE_IN_VEHICLE_IS_READY()
//					IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_REPLACING_CAR)
//						vehFadeOut = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
//						IF GET_PED_IN_VEHICLE_SEAT(vehFadeOut) = PLAYER_PED_ID()
//							SET_ENTITY_VISIBLE(vehFadeOut,FALSE)
//							SET_ENTITY_VISIBLE(PLAYER_PED_ID(),FALSE)
//							SET_VEHICLE_FIXED(vehFadeOut)
//					        SET_ENTITY_HEALTH(vehFadeOut, 1000) 
//					        SET_VEHICLE_ENGINE_HEALTH(vehFadeOut, 1000) 
//					        SET_VEHICLE_PETROL_TANK_HEALTH(vehFadeOut, 1000) 
//							SET_BIT(iLocalBS2,LOCAL_BS2_bFadeOutWarpIntoGarage)
//							SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_CAR_MADE_INVISIBLE_ON_EXTERIOR)
//							CDEBUG1LN(DEBUG_SAFEHOUSE, "NETWORK_FADE_OUT_ENTITY: called from HANDLE_WARP_INTO_GARAGE()")
//						ENDIF
//					ENDIF
				
					IF NOT IS_BIT_SET(ilocalBS,LOCAL_BS_SKIP_ENTER_FADE)
						CLEAR_VEHICLE_OF_PROJECTILES()
						globalPropertyEntryData.iGarageIntroProperty = iCurrentPropertyID
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: - globalPropertyEntryData.iGarageIntroProperty = ", iCurrentPropertyID)
						IF DOES_ENTITY_EXIST(warpInControl.vehID)
							IF NETWORK_HAS_CONTROL_OF_ENTITY(warpInControl.vehID)
								SET_VEHICLE_DOORS_LOCKED(warpInControl.vehID,VEHICLELOCK_LOCKED)
							ENDIF
						ENDIF
						IF DO_GARAGE_CAR_ENTER_CUTSCENE(FALSE)
							IF globalPropertyEntryData.iGarageIntroProperty = iCurrentPropertyID
								CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: - CLEARING globalPropertyEntryData.iGarageIntroProperty = ", iCurrentPropertyID, " ... = -1")
								globalPropertyEntryData.iGarageIntroProperty = -1
							ENDIF
							IF IS_NET_PLAYER_OK(PLAYER_ID())
							OR IS_BIT_SET(ilocalBS,LOCAL_BS_ENTER_INGORE_PLAYER_OK)
								IF NOT IS_BIT_SET(ilocalBS,LOCAL_BS_SKIP_ENTER_FADE)
								AND NOT IS_BIT_SET(iLocalBS2,LOCAL_BS2_bCarEntryAborted)
									SET_BIT(ilocalBS,LOCAL_BS_NO_FADE)
								ENDIF
								IF NOT IS_BIT_SET(ilocalBS,LOCAL_BS_NO_FADE)
									IF IS_BIT_SET(ilocalBS,LOCAL_BS_SKIP_ENTER_FADE)
										CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: setting skip of fade due to bit being set (PROP_ENTER_STAGE_VEHICLE)")
										DO_SCREEN_FADE_OUT(0)
									ELSE
										DO_SCREEN_FADE_OUT(500)
									ENDIF
								ENDIF
								//SET_BIT(ilocalBS,LOCAL_BS_NO_FADE)
								PLAY_ENTERING_GARAGE_SOUND()
								IF DOES_ENTITY_EXIST(warpInControl.vehID)
									IF NETWORK_HAS_CONTROL_OF_ENTITY(warpInControl.vehID)
										SET_VEHICLE_DOORS_LOCKED(warpInControl.vehID,VEHICLELOCK_LOCKED)
									ENDIF
								ENDIF
								//CLEAR_PED_TASKS(PLAYER_PED_ID())
								warpInControl.ibuttonBS = 0
								SET_BIT(ilocalBS,LOCAL_BS_STARTED_FADE)
							ELSE
								SET_BIT(ilocalBS,LOCAL_BS_CLEANUP_MP_PROPERTIES)
							ENDIF
						ELSE
							CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: doing enter CS")
						ENDIF
					ELSE
						IF globalPropertyEntryData.iGarageIntroProperty = iCurrentPropertyID
							CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: - CLEARING globalPropertyEntryData.iGarageIntroProperty = ", iCurrentPropertyID, " ... = -1")
							globalPropertyEntryData.iGarageIntroProperty = -1
						ENDIF
						DO_SCREEN_FADE_OUT(0)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: setting skip of fade due to bit being set (PROP_ENTER_STAGE_VEHICLE) NO CS")
						PLAY_ENTERING_GARAGE_SOUND()
						IF DOES_ENTITY_EXIST(warpInControl.vehID)
							IF NETWORK_HAS_CONTROL_OF_ENTITY(warpInControl.vehID)
								SET_VEHICLE_DOORS_LOCKED(warpInControl.vehID,VEHICLELOCK_LOCKED)
							ENDIF
						ENDIF
						warpInControl.ibuttonBS = 0
						SET_BIT(ilocalBS,LOCAL_BS_STARTED_FADE)
					ENDIF
				#IF IS_DEBUG_BUILD
				ELSE
					IF NOT IS_BIT_SET(ilocalBS,LOCAL_BS_SKIP_ENTER_FADE)
						globalPropertyEntryData.iGarageIntroProperty = iCurrentPropertyID
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: - waiting for all ready globalPropertyEntryData.iGarageIntroProperty = ", iCurrentPropertyID)
					ENDIF
					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: CHECK_EVERYONE_IN_VEHICLE_IS_READY waiting. Property being entered = ",GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iEnteringPropertyID)
				#ENDIF
				ENDIF
			ELSE
				IF NOT IS_BIT_SET(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_ENTER_GARAGE)
					SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_ENTER_GARAGE)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: GLOBAL_PROPERTY_ENTRY_BS_ENTER_GARAGE set -1 ")
				ENDIF
				//IF FORCE_EVERYONE_FROM_MY_CAR(FALSE,TRUE)
					IF IS_BIT_SET(ilocalBS,LOCAL_BS_NO_FADE)
					OR IS_SCREEN_FADED_OUT()
					OR IS_BIT_SET(ilocalBS,LOCAL_BS_STARTED_CUTSCENE)
						IF IS_SCREEN_FADED_OUT()
						AND IS_BIT_SET(iLocalBS2,LOCAL_BS2_bCarEntryAborted)
							IF NOT IS_BIT_SET(iLocalBS2,LOCAL_BS2_bFadeOutWarpIntoGarage)
							
								//IF GET_PROPERTY_SIZE_TYPE(mpProperties[iCurrentPropertyID].iIndex) != PROP_SIZE_TYPE_LARGE_APT
								//AND mpProperties[iCurrentPropertyID].iIndex != PROPERTY_LOW_APT_7
								IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
									vehFadeOut = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
									
									IF GET_PED_IN_VEHICLE_SEAT(vehFadeOut) = PLAYER_PED_ID()
										NETWORK_FADE_OUT_ENTITY(vehFadeOut, TRUE, TRUE)
										SET_VEHICLE_SIREN(vehFadeOut,FALSE)
										SET_BIT(iLocalBS2,LOCAL_BS2_bFadeOutWarpIntoGarage)
										SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_CAR_MADE_INVISIBLE_ON_EXTERIOR)
										CDEBUG1LN(DEBUG_SAFEHOUSE, "NETWORK_FADE_OUT_ENTITY: called from HANDLE_WARP_INTO_GARAGE() - Q")
									ENDIF
								ELSE
									NETWORK_FADE_OUT_ENTITY(PLAYER_PED_ID(), TRUE, TRUE)
									SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_CAR_MADE_INVISIBLE_ON_EXTERIOR)
									CDEBUG1LN(DEBUG_SAFEHOUSE, "NETWORK_FADE_OUT_ENTITY: called from HANDLE_WARP_INTO_GARAGE() - PED - Q")
									SET_BIT(iLocalBS2,LOCAL_BS2_bFadeOutWarpIntoGarage)
								ENDIF
								
								
								//ENDIF
							ENDIF
						ENDIF
						
						
						//SET_BIT(g_iExteriorScriptProcessingWarpBS,0)
						IF NOT IS_BIT_SET(ilocalBS,LOCAL_BS_STARTED_CUTSCENE)
							VEHICLE_INDEX trailerVehicle
							IF DOES_ENTITY_EXIST(warpInControl.vehID)
								IF IS_VEHICLE_DRIVEABLE(warpInControl.vehID)
									IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),warpInControl.vehID,TRUE)
									AND NETWORK_HAS_CONTROL_OF_ENTITY(warpInControl.vehID)
										GET_VEHICLE_TRAILER_VEHICLE(warpInControl.vehID,trailerVehicle)
										IF DOES_ENTITY_EXIST(trailerVehicle)
											DETACH_VEHICLE_FROM_TRAILER(warpInControl.vehID)
											PRINTLN("AM_MP_PROPERTY_EXT: Detaching trailer from vehicle")
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							IF globalPropertyEntryData.bChoosenToReplaceVehicle
								IF DOES_ENTITY_EXIST(warpInControl.vehID)
									IF IS_VEHICLE_DRIVEABLE(warpInControl.vehID)
										IF DECOR_IS_REGISTERED_AS_TYPE("Player_Vehicle", DECOR_TYPE_INT) 
											IF DECOR_EXIST_ON(warpInControl.vehID, "Player_Vehicle") 
												IF DECOR_GET_INT(warpInControl.vehID, "Player_Vehicle") = NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID())
													bPersonalVehicle = TRUE
													PRINTLN("Player entering in a personal vehicle- 1")
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								IF IS_BIT_SET(iLocalBS2,LOCAL_BS2_bCleanupOldPersonalVehicle)
									//IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),warpInControl.vehID,TRUE)
									//	CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
									//ENDIF
									IF globalPropertyEntryData.iVehSlot >= 0
										MPSV_GET_SAVE_SLOT_FROM_DISPLAY_SLOT(globalPropertyEntryData.iVehSlot,iSlotToUse)
										IF iSlotToUse >= 0
											CLEAR_BIT(g_MpSavedVehicles[iSlotToUse].iVehicleBS,MP_SAVED_VEHICLE_OUT_GARAGE)
										ENDIF
									ENDIF
									IF globalPropertyEntryData.iOldVehicleSlot >= 0
										MPSV_GET_SAVE_SLOT_FROM_DISPLAY_SLOT(globalPropertyEntryData.iOldVehicleSlot,iSlotToUse)
										IF iSlotToUse >= 0
											CLEAR_BIT(g_MpSavedVehicles[iSlotToUse].iVehicleBS,MP_SAVED_VEHICLE_OUT_GARAGE)
										ENDIF
									ENDIF
									bPersonalVehicleCleanupDontLeaveVehicle = TRUE
									CDEBUG1LN(DEBUG_SAFEHOUSE, "bPersonalVehicleCleanupDontLeaveVehicle = TRUE - A")
									IF CURRENT_SAVED_VEHICLE_SLOT() >= 0
										IF IS_BIT_SET(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_OUT_GARAGE)
											CLEAR_BIT(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_OUT_GARAGE)
											CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Clearing out of garage flag for vehicle A: ",CURRENT_SAVED_VEHICLE_SLOT())
										ENDIF
									ENDIF
									CLEANUP_MP_SAVED_VEHICLE(TRUE,FALSE)
									CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Clearing player's personal vehicle - 0")
								ENDIF
								
								IF NOT bPersonalVehicle
								AND DOES_ENTITY_EXIST(warpInControl.vehID)
									IF IS_VEHICLE_DRIVEABLE(warpInControl.vehID)
										IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),warpInControl.vehID,TRUE)
											CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
										ENDIF
										//IF IS_VEHICLE_EMPTY(warpInControl.vehID)

//										///SET_BIT(iLocalBS, LOCAL_BS_DELETED_ENTERING_VEHICLE)
//									//ENDIF
											IF NOT IS_BIT_SET(iLocalBS, LOCAL_BS_DELETED_ENTERING_VEHICLE)
												//IF NOT IS_ENTITY_A_MISSION_ENTITY(warpInControl.vehID)
													IF NETWORK_HAS_CONTROL_OF_ENTITY(warpInControl.vehID)
														SET_ENTITY_AS_MISSION_ENTITY(warpInControl.vehID,FALSE,TRUE)
														DELETE_VEHICLE(warpInControl.vehID)
														SET_BIT(iLocalBS, LOCAL_BS_DELETED_ENTERING_VEHICLE)
														CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Deleted vehicle player is entering in.")
													ELSE
														NETWORK_REQUEST_CONTROL_OF_ENTITY(warpInControl.vehID)
														CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Waiting to get control of entity.")
													ENDIF
												//ELSE
												//	CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Entering garage in a script vehicle NOT deleting")
												//	SET_BIT(iLocalBS, LOCAL_BS_DELETED_ENTERING_VEHICLE)
												//ENDIF	
												CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Waiting for delete vehicle flag to be set")
												BREAK
											ENDIF
										//ENDIF
									ELSE
										CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Waiting for replacement vehicle to be destroyed")
										BREAK
									ENDIF
								#IF IS_DEBUG_BUILD
								ELSE
									CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: globalPropertyEntryData.bChoosenToReplaceVehicle - bpersonal = ",bPersonalVehicle)
									CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: globalPropertyEntryData.bChoosenToReplaceVehicle - DOES_ENTITY_EXIST(warpInControl.vehID) = ",DOES_ENTITY_EXIST(warpInControl.vehID))
								#ENDIF
								ENDIF
							ELSE
								IF IS_BIT_SET(iLocalBS2,LOCAL_BS2_bCleanupOldPersonalVehicle)
									IF globalPropertyEntryData.iVehSlot >= 0
										MPSV_GET_SAVE_SLOT_FROM_DISPLAY_SLOT(globalPropertyEntryData.iVehSlot,iSlotToUse)
										IF iSlotToUse >= 0
											CLEAR_BIT(g_MpSavedVehicles[iSlotToUse].iVehicleBS,MP_SAVED_VEHICLE_OUT_GARAGE)
											CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Clearing out of garage flag for vehicle X: ",iSlotToUse)
										ENDIF
									ENDIF
									IF globalPropertyEntryData.iOldVehicleSlot >= 0
										MPSV_GET_SAVE_SLOT_FROM_DISPLAY_SLOT(globalPropertyEntryData.iOldVehicleSlot,iSlotToUse)
										IF iSlotToUse >= 0
											CLEAR_BIT(g_MpSavedVehicles[iSlotToUse].iVehicleBS,MP_SAVED_VEHICLE_OUT_GARAGE)
											CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Clearing out of garage flag for vehicle Y: ",iSlotToUse)
										ENDIF
									ENDIF
									IF CURRENT_SAVED_VEHICLE_SLOT() >= 0
										IF IS_BIT_SET(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_OUT_GARAGE)
											CLEAR_BIT(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_OUT_GARAGE)
											CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Clearing out of garage flag for vehicle B: ",CURRENT_SAVED_VEHICLE_SLOT())
										ENDIF
									ENDIF
									bPersonalVehicleCleanupDontLeaveVehicle = TRUE
									CDEBUG1LN(DEBUG_SAFEHOUSE, "bPersonalVehicleCleanupDontLeaveVehicle = TRUE - B")
									CLEANUP_MP_SAVED_VEHICLE(FALSE, TRUE,DEFAULT,DEFAULT,FALSE)
									CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Clearing old personal vehicle when entering garage. -1")
								ENDIF
							ENDIF
							
							CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: started CS")
							
							
							SET_BIT(ilocalBS,LOCAL_BS_STARTED_CUTSCENE)
						ELSE
							IF NOT IS_BIT_SET(ilocalBS,LOCAL_BS_WARPED_IN_GARAGE)
								IF NOT IS_BIT_SET(ilocalBS,LOCAL_BS_ENTER_NO_WARP)
									IF NOT IS_BIT_SET(iLocalBS, LOCAL_BS_DELETED_ENTERING_VEHICLE)
//										IF DOES_ENTITY_EXIST(warpInControl.vehID)
//											IF NOT IS_ENTITY_A_MISSION_ENTITY(warpInControl.vehID)
//												IF NETWORK_HAS_CONTROL_OF_ENTITY(warpInControl.vehID)
//												
//													SET_ENTITY_AS_MISSION_ENTITY(warpInControl.vehID,FALSE,FALSE)
//													DELETE_VEHICLE(warpInControl.vehID)
//													SET_BIT(iLocalBS, LOCAL_BS_DELETED_ENTERING_VEHICLE)
//													CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Deleted vehicle player is entering in.")
//												ELSE
//													NETWORK_REQUEST_CONTROL_OF_ENTITY(warpInControl.vehID)
//												ENDIF
//											ELSE
//												CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Entering garage in a script vehicle NOT deleting")
//												SET_BIT(iLocalBS, LOCAL_BS_DELETED_ENTERING_VEHICLE)
//											ENDIF		
//										ELSE
											SET_BIT(iLocalBS, LOCAL_BS_DELETED_ENTERING_VEHICLE)
//										ENDIF
									ELSE
										IF IS_BIT_SET(ilocalBS,LOCAL_BS_BUZZER_TRIGGERED_LOAD_SCENE)
											IF (NOT IS_NEW_LOAD_SCENE_ACTIVE() OR IS_NEW_LOAD_SCENE_LOADED())
											//AND IS_INTERIOR_READY(theInterior)
												SET_ENTITY_COORDS(PLAYER_PED_ID(),mpProperties[iCurrentPropertyID].garage.vInPlayerLoc)
												SET_ENTITY_HEADING(PLAYER_PED_ID(),mpProperties[iCurrentPropertyID].garage.fInPlayerHeading)
												CLEAR_AREA(mpProperties[iCurrentPropertyID].garage.vInPlayerLoc,30,TRUE)
												CLEAR_AREA_OF_VEHICLES(mpProperties[iCurrentPropertyID].garage.vInPlayerLoc,30,FALSE,FALSE,TRUE,TRUE)	//This will only clear the Wrecked Vehicles due to that flag being set.
												SET_BIT(ilocalBS,LOCAL_BS_WARPED_IN_GARAGE)
												CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: warp into garage interior has completed (loaded scene outside0")
											#IF IS_DEBUG_BUILD
											ELSE
												IF IS_NEW_LOAD_SCENE_LOADED()
													CDEBUG1LN(DEBUG_SAFEHOUSE, "waiting for new load scene- 124")
												ENDIF
												//IF IS_INTERIOR_READY(theInterior)
												//	CDEBUG1LN(DEBUG_SAFEHOUSE, "waiting for interior- 124")
												//ENDIF
											#ENDIF
											ENDIF
										ELSE
											IF globalPropertyEntryData.bInteriorWarpDone
											//IF NET_WARP_TO_COORD(mpProperties[iCurrentPropertyID].garage.vInPlayerLoc,mpProperties[iCurrentPropertyID].garage.fInPlayerHeading,FALSE,FALSE)
												//CLEAR_AREA(mpProperties[iCurrentPropertyID].garage.vInPlayerLoc,30,TRUE)
												//CLEAR_AREA_OF_VEHICLES(mpProperties[iCurrentPropertyID].garage.vInPlayerLoc,30,FALSE,FALSE,TRUE,TRUE)
												bPersonalVehicleCleanupDontLeaveVehicle = FALSE
												CDEBUG1LN(DEBUG_SAFEHOUSE, "bPersonalVehicleCleanupDontLeaveVehicle = FALSE - A")
												SET_BIT(ilocalBS,LOCAL_BS_WARPED_IN_GARAGE)
												CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: LOCAL_BS_WARPED_IN_GARAGE - 1")
											//ENDIF
											ENDIF
										ENDIF
									ENDIF
								ELSE
									CLEAR_AREA(mpProperties[iCurrentPropertyID].garage.vInPlayerLoc,30,TRUE)
									CLEAR_AREA_OF_VEHICLES(mpProperties[iCurrentPropertyID].garage.vInPlayerLoc,30,FALSE,FALSE,TRUE,TRUE)	//This will only clear the Wrecked Vehicles due to that flag being set.
									SET_BIT(ilocalBS,LOCAL_BS_WARPED_IN_GARAGE)
									CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: LOCAL_BS_WARPED_IN_GARAGE - 2")
								ENDIF
							ELSE
								//CLEAR_BIT(g_iExteriorScriptProcessingWarpBS,0)
								IF globalPropertyEntryData.bInteriorWarpDone
									IF IS_BIT_SET(ilocalBS,LOCAL_BS_BUZZER_TRIGGERED_LOAD_SCENE)
										NEW_LOAD_SCENE_STOP()
									ENDIF
									RETURN TRUE
								#IF IS_DEBUG_BUILD
								ELSE
									CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_WARP_INTO_GARAGE:waiting for globalPropertyEntryData.bInteriorWarpDone") 
								#ENDIF
								ENDIF
							ENDIF
						ENDIF
					#IF IS_DEBUG_BUILD
					ELSE
						CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_WARP_INTO_GARAGE: stuck waiting for fade") 
					#ENDIF
					ENDIF
				//ENDIF
			ENDIF
		BREAK
		CASE PROP_ENTER_STAGE_FAKE_CUTSCENE_HEIST
			CLEAR_VEHICLE_OF_PROJECTILES()
			IF NOT globalPropertyEntryData.bInteriorWarpDone
				IF DOES_ENTITY_EXIST(warpInControl.vehID)
					IF NETWORK_HAS_CONTROL_OF_ENTITY(warpInControl.vehID)
						SET_VEHICLE_DOORS_LOCKED(warpInControl.vehID,VEHICLELOCK_LOCKED)
					ENDIF
				ENDIF
			ENDIF
			//CDEBUG1LN(DEBUG_SAFEHOUSE, "CDM: Current Stack 2 = ",GET_CURRENT_STACK_SIZE())
			IF DO_GARAGE_CAR_ENTER_CUTSCENE(FALSE)
				IF NOT IS_BIT_SET(entryCutData.iBS,ENTRY_CS_BS_STARTED_FAKE_CUT)
					
					propertyCam0 = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
					propertyCam1 = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
					GET_HEIST_FAKE_GARAGE_ENTRY_PAN_DATA(mpProperties[iCurrentPropertyID].iBuildingID,0,tempCamOffset,iDuration)
					SET_CAM_COORD(propertyCam0,tempCamOffset.vLoc)
					SET_CAM_ROT(propertyCam0,tempCamOffset.vRot)
					SET_CAM_FOV(propertyCam0,TO_FLOAT(tempCamOffset.iFov))
					GET_HEIST_FAKE_GARAGE_ENTRY_PAN_DATA(mpProperties[iCurrentPropertyID].iBuildingID,1,tempCamOffset,iDuration)
					SET_CAM_COORD(propertyCam1,tempCamOffset.vLoc)
					SET_CAM_ROT(propertyCam1,tempCamOffset.vRot)
					SET_CAM_FOV(propertyCam1,TO_FLOAT(tempCamOffset.iFov))

					SHAKE_CAM(propertyCam0,		"HAND_SHAKE", 0.25)
					SHAKE_CAM(propertyCam1,		"HAND_SHAKE", 0.25)
						
					//IF pan.fDuration > 0.1
						SET_CAM_ACTIVE_WITH_INTERP(propertyCam1, propertyCam0, iDuration)
					//ELSE
					//	SET_CAM_ACTIVE(propertyCam0, TRUE)
					//ENDIF
					
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
					SET_BIT(entryCutData.iBS,ENTRY_CS_BS_STARTED_FAKE_CUT)
					REINIT_NET_TIMER(entryCutData.timer,TRUE)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Started heist garage pan")
					globalPropertyEntryData.iVariation = -1
					PRINTLN("AMP_MP_PROPERTY_EXT: reset variation on entry to property - 4")
					IF SHOULD_BYPASS_CHECKS_FOR_ENTRY()
						IF NETWORK_IS_GAMER_IN_MY_SESSION(heistLeader)
							globalPropertyEntryData.ownerID = NETWORK_GET_PLAYER_FROM_GAMER_HANDLE(heistLeader)
							REPEAT MAX_OWNED_PROPERTIES i
								IF GlobalplayerBD_FM[NATIVE_TO_INT(globalPropertyEntryData.ownerID)].propertyDetails.iOwnedProperty[i] = g_iHeistPropertyID
									globalPropertyEntryData.iVariation = GlobalplayerBD_FM[NATIVE_TO_INT(globalPropertyEntryData.ownerID)].propertyDetails.iPropertyVariant[i]
									CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: heist data owner in game: setting globalPropertyEntryData.iVariation = ",globalPropertyEntryData.iVariation)
								ENDIF
							ENDREPEAT
							CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: heist data owner in game")
						ELSE
							globalPropertyEntryData.ownerID = player_id()
							CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: heist data owner NOT in game")
						ENDIF
						globalPropertyEntryData.ownerHandle = heistLeader
						IF IS_NET_PLAYER_OK(globalPropertyEntryData.ownerID ,FALSE,FALSE)
							CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: setting entry data based on heist data: iProperty ", GET_PLAYER_NAME(globalPropertyEntryData.ownerID ))
							#IF IS_DEBUG_BUILD
							DEBUG_PRINT_GAMER_HANDLE(globalPropertyEntryData.ownerHandle)
							#ENDIF
						ENDIF
						SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_HEIST_DROPOFF)
						bHadMissionCriticalEntityOnDropoff = TRUE
						PRINTLN("bHadMissionCriticalEntityOnDropoff Set True 6")
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Skipping anims inside as doing heist drop off (GARAGE)")
						globalPropertyEntryData.iPropertyEntered = mpProperties[g_iHeistPropertyID].iIndex
						PRINTLN("24 globalPropertyEntryData.iPropertyEntered = ",globalPropertyEntryData.iPropertyEntered)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: g_iApartmentDropoffWarpRadius = ",g_iApartmentDropoffWarpRadius)
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(),TRUE)
							IF IS_VEHICLE_DRIVEABLE(warpInControl.vehID)
							AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(),warpInControl.vehID)
							AND	GET_PED_IN_VEHICLE_SEAT(warpInControl.vehID, VS_DRIVER) = PLAYER_PED_ID()		
								IF g_iApartmentDropoffWarpRadius > 0
									IF g_iApartmentDropoffWarpRadius = iALWAYS_WARP_TO_APARTMENT_DISTANCE 
										BROADCAST_PROPERTY_NEARBY_PLAYERS_INVITE_EVENT(ALL_PLAYERS(FALSE,FALSE),globalPropertyEntryData.iPropertyEntered ,globalPropertyEntryData.iVariation, -1 ,TRUE,FALSE,NATIVE_TO_INT(globalPropertyEntryData.ownerID))
									ELSE
										BROADCAST_PROPERTY_NEARBY_PLAYERS_INVITE_EVENT(PLAYERS_IN_RANGE_OF_POINT(GET_PLAYER_COORDS(PLAYER_ID()),g_iApartmentDropoffWarpRadius,TRUE),globalPropertyEntryData.iPropertyEntered ,globalPropertyEntryData.iVariation, -1 ,TRUE,FALSE,NATIVE_TO_INT(globalPropertyEntryData.ownerID))
									ENDIF
								ENDIF
							ELSE
								CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Not BROADCAST_PROPERTY_NEARBY_PLAYERS_INVITE_EVENT as player is not driver")
							ENDIF
						ELSE
							CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Entered garage on foot for heist drop off sending invite.")
							IF g_iApartmentDropoffWarpRadius > 0
								IF g_iApartmentDropoffWarpRadius = iALWAYS_WARP_TO_APARTMENT_DISTANCE 
									BROADCAST_PROPERTY_NEARBY_PLAYERS_INVITE_EVENT(ALL_PLAYERS(FALSE,FALSE),globalPropertyEntryData.iPropertyEntered ,globalPropertyEntryData.iVariation, -1 ,TRUE,FALSE,NATIVE_TO_INT(globalPropertyEntryData.ownerID))
								ELSE
									BROADCAST_PROPERTY_NEARBY_PLAYERS_INVITE_EVENT(PLAYERS_IN_RANGE_OF_POINT(GET_PLAYER_COORDS(PLAYER_ID()),g_iApartmentDropoffWarpRadius,TRUE),globalPropertyEntryData.iPropertyEntered ,globalPropertyEntryData.iVariation, -1 ,TRUE,FALSE,NATIVE_TO_INT(globalPropertyEntryData.ownerID))
								ENDIF
							ENDIF	
						ENDIF
					ELSE

						globalPropertyEntryData.ownerID = player_id()
						globalPropertyEntryData.ownerHandle = GET_GAMER_HANDLE_PLAYER(player_id())
						globalPropertyEntryData.iPropertyEntered = mpProperties[iCurrentPropertyID].iIndex
						PRINTLN("23 globalPropertyEntryData.iPropertyEntered = ",globalPropertyEntryData.iPropertyEntered)
					ENDIF

					SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_ENTER_HOUSE)
					
					globalPropertyEntryData.iEntrance = iEntrancePlayerIsUsing
					CLEAR_TRANSITION_SESSION_DETAILS_ON_ENTRY_IF_SUITABLE(5)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: ENTERED HOUSE Setting globalPropertyEntryData.iPropertyEntered to be: ", globalPropertyEntryData.iPropertyEntered )
					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: ENTERED HOUSE Setting iEntrancePlayerIsUsing to be: ", iEntrancePlayerIsUsing )
					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: local player owns this property setting flags accordingly")
					SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_ENTERING_A_PROPERTY)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: setting PROPERTY_BROADCAST_BS_PLAYER_ENTERING_A_PROPERTY- D")
					SET_PLAYER_BROADCAST_PLAYING_PROPERTY_TRANSITION_CUTSCENE(mpProperties[iCurrentPropertyID].iIndex)
					//SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_ENTER_VIA_EXTERIOR)
					//IF iEntrancePlayerIsUsing > mpProperties[iCurrentPropertyID].iNumEntrances
						globalPropertyEntryData.iEntrance = 0
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: forcing entrance to be: ", globalPropertyEntryData.iEntrance)
					//ENDIF
					SET_BIT(iLocalBS2,LOCAL_BS2_SetEntryData)
					NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_NOT_IN_A_VEHICLE)
					//CDEBUG1LN(DEBUG_SAFEHOUSE, "CDM: Current Stack 11 = ",GET_CURRENT_STACK_SIZE())

					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: hesit entry setting not in vehicle")
				ELSE 
//					IF IS_BIT_SET(iLocalBS2,LOCAL_BS2_LockCarOnMenu)
//						IF IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_DOING_HEIST_DROPOFF)
//							IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//								IF DOES_ENTITY_EXIST(heistUnlockCar)
//									IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(heistUnlockCar)
//										IF NOT NETWORK_IS_IN_MP_CUTSCENE()
//											NETWORK_REQUEST_CONTROL_OF_ENTITY(heistUnlockCar)
//											CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: waiting for control for warp in vehicle before cleaning up")
//										ELSE
//											CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: waiting for cutscene to be over before cleaning up")
//										ENDIF
//										BREAK
//									ELSE
//										SET_VEHICLE_DOORS_LOCKED(heistUnlockCar,VEHICLELOCK_UNLOCKED)
//										CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: unlocking doors")
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
					IF NOT g_HeistGarageDropoffPanComplete
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: heist garage pan running")
						
						// url:bugstar:2089759 - Can we drop the Scope Out mission passed a tad sooner?  Right when the cam hits the top of the pan
						IF NOT g_HeistGarageDropoffSplashReady
						AND DOES_CAM_EXIST(propertyCam1)
						AND NOT IS_CAM_INTERPOLATING(propertyCam1)
							CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Setting g_HeistGarageDropoffSplashReady" )
							g_HeistGarageDropoffSplashReady = TRUE
							IF globalPropertyEntryData.iGarageIntroProperty = iCurrentPropertyID
								CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: - CLEARING globalPropertyEntryData.iGarageIntroProperty = ", iCurrentPropertyID, " ... = -1")
								globalPropertyEntryData.iGarageIntroProperty = -1
							ENDIF
							IF IS_BIT_SET(iLocalBS, LOCAL_BS_TAKEN_AWAY_CONTROL)
								CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Clearing LOCAL_BS_TAKEN_AWAY_CONTROL  WITHOUT returning control for heist dropoff.")
								CLEAR_BIT(iLocalBS, LOCAL_BS_TAKEN_AWAY_CONTROL)
							ENDIF
						ENDIF
						
//						GET_HEIST_FAKE_GARAGE_ENTRY_PAN_DATA(mpProperties[iCurrentPropertyID].iBuildingID,1,tempCamOffset,iDuration)
//						IF HAS_NET_TIMER_EXPIRED(entryCutData.timer,iDuration,TRUE)
//							g_HeistGarageDropoffPanComplete = TRUE
//							CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: g_HeistGarageDropoffPanComplete = TRUE")
//						ENDIF
						globalPropertyEntryData.bWalkInSeqDone = TRUE
						
					ELSE
						IF NOT IS_BIT_SET(iLocalBS2,LOCAL_BS2_bClearedHeistPanCutscene)
							
							IF bUseNewEntryAnims //GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_BUILDING_1
								IF warpInControl.iStage = PROP_ENTER_STAGE_FAKE_CUTSCENE_HEIST 
									//CLEANUP_MP_CUTSCENE(DEFAULT,FALSE)
									SET_BIT(iLocalBS2,LOCAL_BS2_bClearedHeistPanCutscene)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
//					#IF IS_DEBUG_BUILD
					
					IF globalPropertyEntryData.bInteriorWarpDone
						RESET_TO_ENTRANCE_TO_PROPERTY_STAGE()
					ENDIF
//					#ENDIF
				ENDIF	
			ENDIF
		BREAK
		
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HANDLE_WARP_INTO_HOUSE()
	BOOl bPersonalVehicle
	BOOL bAnotherPlayerPersonalVehicle
	INT iSlotToUse,iDisplaySlot
	MODEL_NAMES vehicleModel
	SWITCH warpInControl.iStage
		CASE PROP_ENTER_STAGE_INIT
			CLEAR_BIT(ilocalBS,LOCAL_BS_STARTED_CUTSCENE)
			IF IS_BIT_SET(iLocalBS,LOCAL_BS_JUST_BOUGHT_PROPERTY)
				IF IS_NET_PLAYER_OK(PLAYER_ID())
					warpInControl.vehID = GET_PLAYERS_LAST_VEHICLE()
					IF NOT DOES_ENTITY_EXIST(warpInControl.vehID)
						SET_PROPERTY_ENTER_STAGE(PROP_ENTER_STAGE_NO_VEHICLE)
						RETURN FALSE
					ENDIF
					IF NOT IS_VEHICLE_DRIVEABLE(warpInControl.vehID)
						SET_PROPERTY_ENTER_STAGE(PROP_ENTER_STAGE_NO_VEHICLE)
						RETURN FALSE
					ENDIF
						
					vehicleModel = GET_ENTITY_MODEL(warpInControl.vehID)
					IF (PLAYER_PED_ID() = GET_PED_IN_VEHICLE_SEAT(warpInControl.vehID, VS_DRIVER))
					OR IS_VEHICLE_EMPTY(warpInControl.vehID)
						IF DECOR_IS_REGISTERED_AS_TYPE("Player_Vehicle", DECOR_TYPE_INT) 
							IF DECOR_EXIST_ON(warpInControl.vehID, "Player_Vehicle") 
								IF DECOR_GET_INT(warpInControl.vehID, "Player_Vehicle") = NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID())
									bPersonalVehicle = TRUE
								ELSE
									bAnotherPlayerPersonalVehicle = TRUE
								ENDIF
							ENDIF
						ENDIF

						CDEBUG1LN(DEBUG_SAFEHOUSE, "Entering house property ID is # ", mpProperties[iCurrentPropertyID].iIndex)
						IF bAnotherPlayerPersonalVehicle
							CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_WARP_INTO_HOUSE: you were in someone else's personal vehicle not taking in to garage.")
							SET_PROPERTY_ENTER_STAGE(PROP_ENTER_STAGE_NO_VEHICLE)
						ELSE
							IF bPersonalVehicle
								//MP_SAVE_VEHICLE_STORE_CAR_DETAILS_IN_SLOT(warpInControl.vehID,CURRENT_SAVED_VEHICLE_SLOT(),TRUE,TRUE,FALSE,FALSE)
								MPSV_GET_SAVE_SLOT_FROM_DISPLAY_SLOT(CURRENT_SAVED_VEHICLE_SLOT(),globalPropertyEntryData.iVehSlot)
								SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_NOT_IN_A_VEHICLE)
								CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_WARP_INTO_HOUSE: Adding personal vehicle back into display slot #",globalPropertyEntryData.iVehSlot ," that is was taken out of")
								SET_PROPERTY_ENTER_STAGE(PROP_ENTER_STAGE_VEHICLE)
							ELSE
								
								iDisplaySlot = MP_SAVE_VEHICLE_GET_EMPTY_DISPLAY_SLOT(MP_SAVE_VEH_SEARCH_EMPTY_FIRST,TRUE,vehicleModel,iCurrentPropertyID)
								MPSV_GET_SAVE_SLOT_FROM_DISPLAY_SLOT(iDisplaySlot,iSlotToUse)
								IF iSlotToUse < 0
									iSlotToUse = MP_SAVE_VEHICLE_GET_EMPTY_SAVE_SLOT(MP_SAVE_VEH_SEARCH_EMPTY_FIRST,TRUE, vehicleModel)
								ELSE
									CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_WARP_INTO_HOUSE: MP_SAVE_VEHICLE_GET_EMPTY_SAVE_SLOT: Empty display but save game data - 7??? SLOT #",iSlotToUse)
									SCRIPT_ASSERT("HANDLE_WARP_INTO_HOUSE: MP_SAVE_VEHICLE_GET_EMPTY_SAVE_SLOT: Empty display but save game data - 7??? -see conor ")
								ENDIF
								IF iSlotToUse != -1
								AND iDisplaySlot != -1
									//MP_SAVE_VEHICLE_STORE_CAR_DETAILS_IN_SLOT(warpInControl.vehID,iSlotToUse,TRUE,TRUE,FALSE,TRUE)
									globalPropertyEntryData.iVehSlot = iDisplaySlot
									SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_ADDING_NEW_VEHICLE)
									CDEBUG1LN(DEBUG_SAFEHOUSE, "GLOBAL_PROPERTY_ENTRY_BS_ADDING_NEW_VEHICLE - TRUE - 3")
									CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_WARP_INTO_HOUSE: Personal vehicle doesn't exists adding current vehicle to garagein slot #",globalPropertyEntryData.iVehSlot)
									SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_NOT_IN_A_VEHICLE)
									SET_PROPERTY_ENTER_STAGE(PROP_ENTER_STAGE_VEHICLE)
								ELSE
									SET_PROPERTY_ENTER_STAGE(PROP_ENTER_STAGE_NO_VEHICLE)
								ENDIF
							ENDIF
						ENDIF
					ELSE
						SET_PROPERTY_ENTER_STAGE(PROP_ENTER_STAGE_NO_VEHICLE)
						RETURN FALSE
					ENDIF
				ELSE
					SET_BIT(ilocalBS,LOCAL_BS_CLEANUP_MP_PROPERTIES)
				ENDIF
			ELSE
				SET_PROPERTY_ENTER_STAGE(PROP_ENTER_STAGE_NO_VEHICLE)
			ENDIF
		BREAK
		CASE PROP_ENTER_STAGE_NO_VEHICLE
			IF NOT IS_BIT_SET(ilocalBS,LOCAL_BS_STARTED_FADE)
				IF IS_NET_PLAYER_OK(PLAYER_ID())
				OR IS_BIT_SET(ilocalBS,LOCAL_BS_ENTER_INGORE_PLAYER_OK)
					IF NOT IS_BIT_SET(ilocalBS,LOCAL_BS_SKIP_ENTER_FADE)
						SET_BIT(ilocalBS,LOCAL_BS_NO_FADE)
					ENDIF
					IF NOT IS_BIT_SET(ilocalBS,LOCAL_BS_NO_FADE)
						IF IS_BIT_SET(ilocalBS,LOCAL_BS_SKIP_ENTER_FADE)
							DO_SCREEN_FADE_OUT(0)
						ELSE
							DO_SCREEN_FADE_OUT(500)
						ENDIF
					ENDIF
					CDEBUG1LN(DEBUG_SAFEHOUSE, "Entering house property ID is # ", mpProperties[iCurrentPropertyID].iIndex)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_WARP_INTO_HOUSE: setting fade started") 
					SET_BIT(ilocalBS,LOCAL_BS_STARTED_FADE)
					//IF GET_PROPERTY_SIZE_TYPE(mpProperties[iCurrentPropertyID].iIndex) = PROP_SIZE_TYPE_LARGE_APT
					//	SET_BIT(ilocalBS,LOCAL_BS_BUZZER_TRIGGERED_LOAD_SCENE)
					//ENDIF
				ELSE
					SET_BIT(ilocalBS,LOCAL_BS_CLEANUP_MP_PROPERTIES)
				ENDIF
			ELSE
				//Rowan c, fix for PT 1449237
				IF IS_BIT_SET(ilocalBS,LOCAL_BS_NO_FADE)
				OR IS_SCREEN_FADED_OUT()
				OR IS_BIT_SET(ilocalBS,LOCAL_BS_STARTED_CUTSCENE)
					//SET_BIT(g_iExteriorScriptProcessingWarpBS,0)
					IF NOT IS_BIT_SET(ilocalBS,LOCAL_BS_STARTED_CUTSCENE)
						IF NOT IS_BIT_SET(iMP_Prop_RetainBS,MP_PROP_RETAIN_VEH_TRUE)
							SET_BIT(iMP_Prop_RetainBS,MP_PROP_RETAIN_VEH_TRUE)
						ELSE
							IF IS_BIT_SET( iMP_Prop_RetainBS,MP_PROP_RETAIN_VEH_RETAINED)
								SET_BIT(ilocalBS,LOCAL_BS_STARTED_CUTSCENE)
							ENDIF
						ENDIF
					ELSE
						IF NOT IS_BIT_SET(ilocalBS,LOCAL_BS_WARPED_IN_GARAGE)
							IF NOT IS_BIT_SET(ilocalBS,LOCAL_BS_ENTER_NO_WARP)
								IF IS_BIT_SET(ilocalBS,LOCAL_BS_BUZZER_TRIGGERED_LOAD_SCENE)
									IF (NOT IS_NEW_LOAD_SCENE_ACTIVE() OR IS_NEW_LOAD_SCENE_LOADED())
									//AND IS_INTERIOR_READY(theInterior)
										IF NOT IS_BIT_SET(iLocalBS2,LOCAL_BS2_bEnteringAnotherPlayerProperty)
											SET_ENTITY_COORDS(PLAYER_PED_ID(),mpProperties[iCurrentPropertyID].entrance[0].vInPlayerLoc)
											SET_ENTITY_HEADING(PLAYER_PED_ID(),mpProperties[iCurrentPropertyID].entrance[0].fInPlayerHeading)
											CLEAR_AREA(mpProperties[iCurrentPropertyID].garage.vInPlayerLoc,30,TRUE)
											CLEAR_AREA_OF_VEHICLES(mpProperties[iCurrentPropertyID].garage.vInPlayerLoc,30,FALSE,FALSE,TRUE,TRUE)	//This will only clear the Wrecked Vehicles due to that flag being set.
										ELSE
											SET_ENTITY_COORDS(PLAYER_PED_ID(),mpProperties[iCurrentPropertyID].house.buzzerEnter.vOutsidePlayerPos)
											SET_ENTITY_HEADING(PLAYER_PED_ID(),mpProperties[iCurrentPropertyID].house.buzzerEnter.fOutsidePlayerHeading)
											CLEAR_AREA(mpProperties[iCurrentPropertyID].garage.vInPlayerLoc,30,TRUE)
											CLEAR_AREA_OF_VEHICLES(mpProperties[iCurrentPropertyID].garage.vInPlayerLoc,30,FALSE,FALSE,TRUE,TRUE)	//This will only clear the Wrecked Vehicles due to that flag being set.
										ENDIF
										SET_BIT(ilocalBS,LOCAL_BS_WARPED_IN_GARAGE)
										CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: warp into house interior has completed (loaded scene outside)")
									#IF IS_DEBUG_BUILD
									ELSE
										IF IS_NEW_LOAD_SCENE_LOADED()
											CDEBUG1LN(DEBUG_SAFEHOUSE, "waiting for new load scene- 125")
										ENDIF
										//IF IS_INTERIOR_READY(theInterior)
										//	CDEBUG1LN(DEBUG_SAFEHOUSE, "waiting for interior- 125")
										//ENDIF
									#ENDIF
									ENDIF
								ELSE
									IF globalPropertyEntryData.bInteriorWarpDone
//									IF NOT bEnteringAnotherPlayerProperty
//									//globalPropertyEntryData.ownerID = INVALID_PLAYER_INDEX()
//									//OR globalPropertyEntryData.ownerID = PLAYER_ID()
//										IF NET_WARP_TO_COORD(mpProperties[iCurrentPropertyID].entrance[0].vInPlayerLoc,mpProperties[iCurrentPropertyID].entrance[0].fInPlayerHeading,FALSE,FALSE)
//											CLEAR_AREA(mpProperties[iCurrentPropertyID].entrance[0].vInPlayerLoc,30,TRUE)
//											CLEAR_AREA_OF_VEHICLES(mpProperties[iCurrentPropertyID].entrance[0].vInPlayerLoc,30,FALSE,FALSE,TRUE,TRUE)	//This will only clear the Wrecked Vehicles due to that flag being set.
											SET_BIT(ilocalBS,LOCAL_BS_WARPED_IN_GARAGE)
											CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: warp into house interior has completed")
//										ENDIF
//									ELSE
//										IF NET_WARP_TO_COORD(mpProperties[iCurrentPropertyID].house.buzzerEnter.vOutsidePlayerPos,mpProperties[iCurrentPropertyID].house.buzzerEnter.fOutsidePlayerHeading,FALSE,FALSE)
//											CLEAR_AREA(mpProperties[iCurrentPropertyID].house.buzzerEnter.vOutsidePlayerPos,30,TRUE)
//											CLEAR_AREA_OF_VEHICLES(mpProperties[iCurrentPropertyID].house.buzzerEnter.vOutsidePlayerPos,30,FALSE,FALSE,TRUE,TRUE)	//This will only clear the Wrecked Vehicles due to that flag being set.
											SET_BIT(ilocalBS,LOCAL_BS_WARPED_IN_GARAGE)
											CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: warp into house interior has completed at outside of interior")
										//ENDIF
									ENDIF
								ENDIF
							ELSE
								CLEAR_AREA(mpProperties[iCurrentPropertyID].entrance[0].vInPlayerLoc,30,TRUE)
								CLEAR_AREA_OF_VEHICLES(mpProperties[iCurrentPropertyID].entrance[0].vInPlayerLoc,30,FALSE,FALSE,TRUE,TRUE)	//This will only clear the Wrecked Vehicles due to that flag being set.
								SET_BIT(ilocalBS,LOCAL_BS_WARPED_IN_GARAGE)
								CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: house warp no warp flag set finishing")
							ENDIF
						ELSE
							IF IS_BIT_SET(ilocalBS,LOCAL_BS_BUZZER_TRIGGERED_LOAD_SCENE)
								NEW_LOAD_SCENE_STOP()
							ENDIF
							RETURN TRUE
						ENDIF
					ENDIF
				#IF IS_DEBUG_BUILD
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_WARP_INTO_HOUSE: stuck waiting for fade") 
				#ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE PROP_ENTER_STAGE_VEHICLE
			IF NOT IS_BIT_SET(ilocalBS,LOCAL_BS_STARTED_FADE)

					IF IS_NET_PLAYER_OK(PLAYER_ID())
					OR IS_BIT_SET(ilocalBS,LOCAL_BS_ENTER_INGORE_PLAYER_OK)
						IF NOT IS_BIT_SET(ilocalBS,LOCAL_BS_SKIP_ENTER_FADE)
							SET_BIT(ilocalBS,LOCAL_BS_NO_FADE)
						ENDIF
						IF NOT IS_BIT_SET(ilocalBS,LOCAL_BS_NO_FADE)
							IF IS_BIT_SET(ilocalBS,LOCAL_BS_SKIP_ENTER_FADE)
								DO_SCREEN_FADE_OUT(0)
							ELSE
								DO_SCREEN_FADE_OUT(500)
							ENDIF
						ENDIF
						SET_BIT(ilocalBS,LOCAL_BS_NO_FADE)
						IF DOES_ENTITY_EXIST(warpInControl.vehID)
							IF NETWORK_HAS_CONTROL_OF_ENTITY(warpInControl.vehID)
								SET_VEHICLE_DOORS_LOCKED(warpInControl.vehID,VEHICLELOCK_LOCKED)
							ENDIF
						ENDIF
						warpInControl.ibuttonBS = 0
						SET_BIT(ilocalBS,LOCAL_BS_STARTED_FADE)
					ELSE
						RESET_GARAGE_CAR_ENTER_CUTSCENE(7,TRUE)
						SET_BIT(ilocalBS,LOCAL_BS_CLEANUP_MP_PROPERTIES)
					ENDIF

			ELSE
				//IF FORCE_EVERYONE_FROM_MY_CAR(FALSE,TRUE)
					IF IS_BIT_SET(ilocalBS,LOCAL_BS_NO_FADE)
					OR IS_SCREEN_FADED_OUT()
					OR IS_BIT_SET(ilocalBS,LOCAL_BS_STARTED_CUTSCENE)
					//	SET_BIT(g_iExteriorScriptProcessingWarpBS,0)
						IF NOT IS_BIT_SET(ilocalBS,LOCAL_BS_STARTED_CUTSCENE)
							RESET_GARAGE_CAR_ENTER_CUTSCENE(8,TRUE)
							IF CURRENT_SAVED_VEHICLE_SLOT() >= 0
								IF IS_BIT_SET(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_OUT_GARAGE)
									CLEAR_BIT(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_OUT_GARAGE)
									CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Clearing out of garage flag for vehicle C: ",CURRENT_SAVED_VEHICLE_SLOT())
								ENDIF
							ENDIF
							CLEANUP_MP_SAVED_VEHICLE(TRUE)

							IF DOES_ENTITY_EXIST(warpInControl.vehID)
								IF IS_VEHICLE_DRIVEABLE(warpInControl.vehID)
									IF (PLAYER_PED_ID() = GET_PED_IN_VEHICLE_SEAT(warpInControl.vehID, VS_DRIVER))
										CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
									ELSE
										SET_BIT(iLocalBS, LOCAL_BS_DELETED_ENTERING_VEHICLE)
									ENDIF
									IF NOT IS_BIT_SET(iLocalBS, LOCAL_BS_DELETED_ENTERING_VEHICLE)
										IF NOT IS_ENTITY_A_MISSION_ENTITY(warpInControl.vehID)
											IF NETWORK_HAS_CONTROL_OF_ENTITY(warpInControl.vehID)
												SET_ENTITY_AS_MISSION_ENTITY(warpInControl.vehID,FALSE,TRUE)
												DELETE_VEHICLE(warpInControl.vehID)
												SET_BIT(iLocalBS, LOCAL_BS_DELETED_ENTERING_VEHICLE)
												CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Deleted vehicle player is entering in.")
											ELSE
												NETWORK_REQUEST_CONTROL_OF_ENTITY(warpInControl.vehID)
											ENDIF
										ELSE
											CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Entering garage in a script vehicle NOT deleting")
											SET_BIT(iLocalBS, LOCAL_BS_DELETED_ENTERING_VEHICLE)
										ENDIF	
									ENDIF
								ENDIF
							ENDIF
							

							SET_BIT(ilocalBS,LOCAL_BS_STARTED_CUTSCENE)
						ELSE
							IF NOT IS_BIT_SET(ilocalBS,LOCAL_BS_WARPED_IN_GARAGE)
								IF NOT IS_BIT_SET(ilocalBS,LOCAL_BS_ENTER_NO_WARP)
									IF NOT IS_BIT_SET(iLocalBS, LOCAL_BS_DELETED_ENTERING_VEHICLE)
										IF DOES_ENTITY_EXIST(warpInControl.vehID)
											IF NOT IS_ENTITY_A_MISSION_ENTITY(warpInControl.vehID)
												IF NETWORK_HAS_CONTROL_OF_ENTITY(warpInControl.vehID)
												
													SET_ENTITY_AS_MISSION_ENTITY(warpInControl.vehID,FALSE,TRUE)
													DELETE_VEHICLE(warpInControl.vehID)
													SET_BIT(iLocalBS, LOCAL_BS_DELETED_ENTERING_VEHICLE)
													CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Deleted vehicle player is entering in.")
												ELSE
													NETWORK_REQUEST_CONTROL_OF_ENTITY(warpInControl.vehID)
												ENDIF
											ELSE
												CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Entering garage in a script vehicle NOT deleting")
												SET_BIT(iLocalBS, LOCAL_BS_DELETED_ENTERING_VEHICLE)
											ENDIF		
										ELSE
											SET_BIT(iLocalBS, LOCAL_BS_DELETED_ENTERING_VEHICLE)
										ENDIF
									ELSE
										SET_PROPERTY_ENTER_STAGE(PROP_ENTER_STAGE_NO_VEHICLE)
									ENDIF
								ELSE
									SET_PROPERTY_ENTER_STAGE(PROP_ENTER_STAGE_NO_VEHICLE)
								ENDIF
							ELSE
								SET_PROPERTY_ENTER_STAGE(PROP_ENTER_STAGE_NO_VEHICLE)
							ENDIF
						ENDIF
						
					#IF IS_DEBUG_BUILD
					ELSE
						CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_WARP_INTO_HOUSE: stuck waiting for fade") 
					#ENDIF
					ENDIF
				//ENDIF
			ENDIF
		BREAK
	ENDSWITCH
			
	RETURN FALSE
ENDFUNC



FUNC BOOL SHOULD_BUZZER_MENU_ACTIVATE(BOOL bLocalPlayerOwnsPropertyInBuilding)
	IF ownedDetails.iNumProperties > 1
		RETURN TRUE
	ELIF (IS_PROPERTY_OFFICE(iCurrentPropertyID) OR IS_PROPERTY_OFFICE_GARAGE(iCurrentPropertyID))
	AND mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iType = ENTRANCE_TYPE_HOUSE
		IF GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_OFFICE_GARAGE_LVL1) = PROPERTY_OFFICE_1_GARAGE_LVL1
			RETURN TRUE
		ENDIF
	ENDIF
	#IF IS_DEBUG_BUILD
	IF bDebugTestBuzzer 
		RETURN TRUE
	ENDIF
	#ENDIF
	IF (mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iType = ENTRANCE_TYPE_HOUSE
	AND PROPERTY_NEAR_PLYS_OPT_AVAILABLE(GET_PROPERTY_ENTRANCE_APPROXIMATE_LOCATION(iCurrentPropertyID,iEntrancePlayerIsUsing),tempPropertyExtMenu.bFriendsNearby, tempPropertyExtMenu.bOrganisationNearby,  iCurrentPropertyID,iEntrancePlayerIsUsing))
	AND bLocalPlayerOwnsPropertyInBuilding
	AND NOT GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE)
		RETURN TRUE
	ENDIF
	RETURN FALSE
											
ENDFUNC		

FUNC BOOL DOES_PLAYER_HAVE_ACCESS_TO_ENTRANCE(INT iEntrance,INT iBuilding)
	INT i, iBuildingLoop, k
	BUILDING_PROPERTIES building
	GET_BUILDING_PROPERTIES(building,iBuilding)
	IF IS_PROPERTY_OFFICE(iCurrentPropertyID)
	AND mpProperties[iCurrentPropertyID].entrance[iEntrance].iType = ENTRANCE_TYPE_GARAGE
		//PRINTLN(" DOES_PLAYER_HAVE_ACCESS_TO_ENTRANCE iEntrance= ",iEntrance," iBuilding = ",iBuilding)//CDM TEMP
		IF GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_OFFICE_GARAGE_LVL1) = PROPERTY_OFFICE_1_GARAGE_LVL1
			//PRINTLN(" DOES_PLAYER_HAVE_ACCESS_TO_ENTRANCE 1 TRUE iEntrance= ",iEntrance," iBuilding = ",iBuilding)//CDM TEMP
			RETURN TRUE
		ELSE
			REPEAT NUM_NETWORK_PLAYERS i
				PLAYER_INDEX playerID = INT_TO_PLAYERINDEX(i)
				IF playerID != PLAYER_ID()
					IF IS_NET_PLAYER_OK(playerID,FALSE)
						REPEAT mpProperties[iCurrentPropertyID].building.iNumProperties iBuildingLoop
							IF GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iCurrentlyInsideProperty = building.iPropertiesInBuilding[iBuildingLoop]
								REPEAT MAX_OWNED_PROPERTIES k
									IF GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iOwnedProperty[k] = building.iPropertiesInBuilding[iBuildingLoop]
									OR (GET_OFFICE_GARAGE_BASE_ID_FROM_PROPERTY(building.iPropertiesInBuilding[iBuildingLoop]) = GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iOwnedProperty[k]
									AND DOES_PLAYER_OWN_THIS_OFFICE_GARAGE(playerID,building.iPropertiesInBuilding[iBuildingLoop]))
										//PRINTLN(" DOES_PLAYER_HAVE_ACCESS_TO_ENTRANCE 2 TRUE iEntrance= ",iEntrance," iBuilding = ",iBuilding)//CDM TEMP
										RETURN TRUE
									ENDIF
								ENDREPEAT
							ENDIF
						ENDREPEAT
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_DISTANCE_TO_OFFICE_GARAGE_OK(FLOAT fDistance)
	IF iCurrentPropertyID = PROPERTY_OFFICE_1
		IF fDistance <= 70
			RETURN TRUE
		ENDIF
	ELIF iCurrentPropertyID = PROPERTY_OFFICE_2_BASE 
		IF fDistance <= 55
			RETURN TRUE
		ENDIF
	ELIF iCurrentPropertyID = PROPERTY_OFFICE_3
		IF fDistance <= 100
			RETURN TRUE
		ENDIF
	ELIF iCurrentPropertyID = PROPERTY_OFFICE_4
		IF fDistance <= 65
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC LOOP_ENTRANCES()
	INT j
	FLOAT fDistance
	BOOL bLocalPlayerOwnsPropertyInBuilding
	BOOL bAllowedInWithVehicle
	INT iNotAllowedReason
	IF NOT IS_PLAYER_TELEPORT_ACTIVE()
	AND NOT IS_PLAYER_IN_CORONA()
	AND NOT IS_POST_MISSION_CLEANUP_REQUIRED_OR_ACTIVE(PLAYER_ID())
	AND NOT IS_PLAYER_PARTICIPATING_IN_A_SKYDIVE_CHALLENGE(PLAYER_ID())
		IF DOES_LOCAL_PLAYER_OWN_PROPERTY_IN_BUILDING(mpProperties[iCurrentPropertyID].iBuildingID,ownedDetails)
			bLocalPlayerOwnsPropertyInBuilding = TRUE
			//CDEBUG2LN(DEBUG_SAFEHOUSE, "Player owns property in building (FC= ",GET_FRAME_COUNT())
		ENDIF
		//CDEBUG1LN(DEBUG_SAFEHOUSE, "NOT IS_PLAYER_TELEPORT_ACTIVE() ")
		REPEAT mpProperties[iCurrentPropertyID].iNumEntrances j
			//iEntrancePlayerIsUsing = j
			iEntrancePlayerIsUsing = j
			fDistance = GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()),mpProperties[iCurrentPropertyID].entrance[j].vEntranceMarkerLoc)
			//CDEBUG2LN(DEBUG_SAFEHOUSE, "LOOP_ENTRANCES: vEntranceMarkerLoc = ", mpProperties[iCurrentPropertyID].entrance[j].vEntranceMarkerLoc, ", iEntrancePlayerIsUsing = ",iEntrancePlayerIsUsing) //CDM TEMP
			IF fDistance <= 30
			OR (mpProperties[iCurrentPropertyID].entrance[j].iType = ENTRANCE_TYPE_GARAGE AND IS_DISTANCE_TO_OFFICE_GARAGE_OK(fDistance))

				//PRINTLN("CDM BOOP: checking entance #",j) 
				IF ((bLocalPlayerOwnsPropertyInBuilding OR DOES_ANYONE_IN_GAME_OWN_PROPERTY_IN_BUILDING(iKnowOwnerState,mpProperties[iCurrentPropertyID].iBuildingID))
				AND DOES_PLAYER_HAVE_ACCESS_TO_ENTRANCE(iEntrancePlayerIsUsing,mpProperties[iCurrentPropertyID].iBuildingID))

				#IF IS_DEBUG_BUILD
				OR bDebugTestBuzzer
				#ENDIF
				OR (SHOULD_BYPASS_CHECKS_FOR_ENTRY(FALSE)
				AND mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iType = ENTRANCE_TYPE_HOUSE)
				OR DOES_PLAYER_HAVE_GROUP_ACCESS_TO_PROPERTY()
					//PRINTLN("CDM BOOP: draw buzzer time") 
					//PRINTLN("Ready to draw buzzer ")//CDM TEMP
//					#IF FEATURE_EXECUTIVE
//					IF IS_PROPERTY_OFFICE(iCurrentPropertyID)
//					AND iEntrancePlayerIsUsing = 1 //roof entrance for office
//						IF bLocalPlayerOwnsPropertyInBuilding
//							DRAW_BUZZER_LOCATE(j)
//						ENDIF
//					ELSE
//					#ENDIF
					DRAW_BUZZER_LOCATE(j)
//					#IF FEATURE_EXECUTIVE
//					ENDIF
//					#ENDIF
//					IF DOES_LOCAL_PLAYER_OWN_PROPERTY_IN_BUILDING(mpProperties[iCurrentPropertyID].iBuildingID)
//						DRAW_ENTRANCE_MARKER(j)
//					ENDIF
				//OR DOES_LOCAL_PLAYER_OWN_PROPERTY_IN_BUILDING(
					//CDEBUG1LN(DEBUG_SAFEHOUSE, "CDM: iKnowOwnerState = ",iKnowOwnerState )
					IF IS_PLAYER_IN_BUZZER_LOCATION(j)//,iKnowOwnerState)
						IF (bLocalPlayerOwnsPropertyInBuilding
						AND iKnowOwnerState= 0)
						OR (SHOULD_BYPASS_CHECKS_FOR_ENTRY(FALSE) 
						AND mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iType = ENTRANCE_TYPE_HOUSE)
						OR DOES_PLAYER_HAVE_GROUP_ACCESS_TO_PROPERTY()
//						#IF FEATURE_EXECUTIVE
//						OR (bLocalPlayerOwnsPropertyInBuilding AND IS_PROPERTY_OFFICE(iCurrentPropertyID) AND iEntrancePlayerIsUsing = 1)
//						#ENDIF
							#IF IS_DEBUG_BUILD
							IF db_bFullDebug
								CDEBUG1LN(DEBUG_SAFEHOUSE, "LOOP_ENTRANCES - ABC: iKnowOwnerState = ",iKnowOwnerState)
							ENDIF
							#ENDIF
							IF NOT IS_ENTERING_DISABLED_DUE_TO_EXIT_WARP()
								IF (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) <= 0
								OR ARE_PLAYER_STARS_GREYED_OUT(PLAYER_ID()))
								AND NOT g_bDisablePropertyAccessForLoseCopMissionObj
									IF (SHOULD_BYPASS_CHECKS_FOR_ENTRY(FALSE)
									AND mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iType = ENTRANCE_TYPE_HOUSE)
										IF g_iHeistPropertyID <= 0
											IF bLocalPlayerOwnsPropertyInBuilding
											AND ownedDetails.iReturnedPropertyIDs[0] > 0
												iCurrentPropertyID = ownedDetails.iReturnedPropertyIDs[0]
											ENDIF
										ELSE
											iCurrentPropertyID = g_iHeistPropertyID
										ENDIF
										SET_LOCAL_STAGE(STAGE_SETUP_FOR_USING_PROPERTY)
										globalPropertyEntryData.ownerID = INVALID_PLAYER_INDEX()
										CLEANUP_MENU_ASSETS()
										bHadMissionCriticalEntityOnDropoff = TRUE
										PRINTLN("bHadMissionCriticalEntityOnDropoff Set True 5")
										CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Entering based on Heist data")
										EXIT
									ELSE
										IF IS_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITITCAL_ENTITY()
										OR IS_PLAYER_BLOCKED_FROM_PROPERTY(FALSE, GET_PROPERTY_TYPE_FOR_IS_PLAYER_BLOCKED_FROM_PROPERTY(iCurrentPropertyID))
											IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
												IF NOT IS_BIT_SET(ilocalBS, LOCAL_BS_WANTED_HELP_TEXT)
													IF IS_PLAYER_BLOCKED_FROM_PROPERTY(FALSE, GET_PROPERTY_TYPE_FOR_IS_PLAYER_BLOCKED_FROM_PROPERTY(iCurrentPropertyID))
														IF mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iType = ENTRANCE_TYPE_GARAGE
															PRINT_REASON_PLAYER_IS_BLOCKED_FROM_PROPERTY(TRUE,iCurrentPropertyID,ownedDetails)
														ELSE
															PRINT_REASON_PLAYER_IS_BLOCKED_FROM_PROPERTY(FALSE,iCurrentPropertyID,ownedDetails)
														ENDIF
													ELSE
														IF mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iType = ENTRANCE_TYPE_GARAGE
															PRINT_HELP("CUST_GAR_MISO")
														ELSE
															IF IS_PROPERTY_OFFICE(iCurrentPropertyID)
																PRINT_HELP("CUST_OFF_MISO")
															ELSE
																IF IS_PROPERTY_CLUBHOUSE(iCurrentPropertyID)
																	PRINT_HELP("CUST_CLU_MISO")
																ELSE
																	PRINT_HELP("CUST_APT_MISO")
																ENDIF	
															ENDIF
														ENDIF
													ENDIF	
													
													SET_BIT(iLocalBS, LOCAL_BS_WANTED_HELP_TEXT)
												ENDIF
											ENDIF
										ELSE
											CLEAR_BIT(ilocalBS, LOCAL_BS_WANTED_HELP_TEXT)
											
											IF DOES_PLAYER_HAVE_GROUP_ACCESS_TO_PROPERTY() 
												CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_MP_PROPERTIES: Player is entering a property DOES_PLAYER_HAVE_GROUP_ACCESS_TO_PROPERTY() is true") NET_NL()
											ELSE
												CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_MP_PROPERTIES: Player is entering their property no one known owns it as well or garage (BUZZER)") NET_NL()
											ENDIF
											
											IF SHOULD_BUZZER_MENU_ACTIVATE(bLocalPlayerOwnsPropertyInBuilding)
												//bRunPropertySelectMenu = TRUE
												SET_BIT(iLocalBS2, LOCAL_BS2_bRunPropertySelectMenu)
												SET_LOCAL_STAGE(STAGE_SETUP_FOR_MENU)
												iCurrentMenuType = MENU_TYPE_BUZZER
												CDEBUG1LN(DEBUG_SAFEHOUSE, "A Player entered property building # ", mpProperties[iCurrentPropertyID].iBuildingID)
												iCurrentPropertyID = mpProperties[iCurrentPropertyID].iIndex
												CDEBUG1LN(DEBUG_SAFEHOUSE, "A Player entered property # ", mpProperties[iCurrentPropertyID].iIndex)
											ELSE
												SET_LOCAL_STAGE(STAGE_SETUP_FOR_USING_PROPERTY)
												IF DOES_PLAYER_HAVE_GROUP_ACCESS_TO_PROPERTY() 
													CDEBUG1LN(DEBUG_SAFEHOUSE, "C Player entered property building # ", mpProperties[iCurrentPropertyID].iBuildingID)
													iCurrentPropertyID = mpProperties[iCurrentPropertyID].iIndex
													CDEBUG1LN(DEBUG_SAFEHOUSE, "C Player entered property # ", mpProperties[iCurrentPropertyID].iIndex)	
												ELSE
													CDEBUG1LN(DEBUG_SAFEHOUSE, "B Player entered property building # ", ownedDetails.iReturnedPropertyIDs[0])
													iCurrentPropertyID = ownedDetails.iReturnedPropertyIDs[0]
													CDEBUG1LN(DEBUG_SAFEHOUSE, "B Player entered property # ", ownedDetails.iReturnedPropertyIDs[0])
												ENDIF
											ENDIF
											
											
											DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
											
											globalPropertyEntryData.ownerID = INVALID_PLAYER_INDEX()
											CLEANUP_MENU_ASSETS()
											EXIT
										ENDIF
									
									ENDIF
								ELSE
									IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
										IF NOT IS_BIT_SET(ilocalBS, LOCAL_BS_WANTED_HELP_TEXT)
											IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
												IF mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iType = ENTRANCE_TYPE_GARAGE
													PRINT_HELP("CUST_GAR_WH")
												ELIF IS_PROPERTY_OFFICE(iCurrentPropertyID)
													PRINT_HELP("CUST_OFF_WH")
												ELIF IS_PROPERTY_CLUBHOUSE(iCurrentPropertyID)
													PRINT_HELP("CUST_CLU_WH")
												ELSE
													PRINT_HELP("CUST_APT_WH")
												ENDIF
											ELIF g_bDisablePropertyAccessForLoseCopMissionObj
												IF mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iType = ENTRANCE_TYPE_GARAGE
													PRINT_HELP("PROP_GAR_HEI_WOH")
												ELIF IS_PROPERTY_OFFICE(iCurrentPropertyID)
													PRINT_HELP("PROP_OFF_HEI_WOH")
												ELSE
													PRINT_HELP("PROP_APT_HEI_WOH")
												ENDIF
											ENDIF
											SET_BIT(iLocalBS, LOCAL_BS_WANTED_HELP_TEXT)
										ENDIF
									ENDIF
								ENDIF
							ELSE
								CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: can't enter buzzer as ")
							ENDIF
						ELSE
							iEntrancePlayerIsUsing = j
							#IF IS_DEBUG_BUILD
							IF db_bFullDebug
								CDEBUG1LN(DEBUG_SAFEHOUSE, "LOOP_ENTRANCES - DEF")
							ENDIF
							#ENDIF
							IF (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) <= 0
							OR ARE_PLAYER_STARS_GREYED_OUT(PLAYER_ID()))
							AND NOT g_bDisablePropertyAccessForLoseCopMissionObj
							
								IF (IS_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITITCAL_ENTITY()
								AND NOT (SHOULD_BYPASS_CHECKS_FOR_ENTRY(FALSE)
								AND mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iType = ENTRANCE_TYPE_HOUSE)
								)
								OR IS_PLAYER_BLOCKED_FROM_PROPERTY(FALSE, GET_PROPERTY_TYPE_FOR_IS_PLAYER_BLOCKED_FROM_PROPERTY(iCurrentPropertyID))
									//CDEBUG1LN(DEBUG_SAFEHOUSE, "CDM: mission critical!")
									IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
										IF NOT IS_BIT_SET(ilocalBS, LOCAL_BS_WANTED_HELP_TEXT)
											IF IS_PLAYER_BLOCKED_FROM_PROPERTY(FALSE, GET_PROPERTY_TYPE_FOR_IS_PLAYER_BLOCKED_FROM_PROPERTY(iCurrentPropertyID))
												IF mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iType = ENTRANCE_TYPE_GARAGE
													PRINT_REASON_PLAYER_IS_BLOCKED_FROM_PROPERTY(TRUE,iCurrentPropertyID,ownedDetails)
												ELSE
													PRINT_REASON_PLAYER_IS_BLOCKED_FROM_PROPERTY(FALSE,iCurrentPropertyID,ownedDetails)
												ENDIF
											ELSE
												IF mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iType = ENTRANCE_TYPE_GARAGE
													PRINT_HELP("CUST_GAR_MISO")
												ELSE
													IF IS_PROPERTY_OFFICE(iCurrentPropertyID)
														PRINT_HELP("CUST_OFF_MISO")
													ELSE
														IF IS_PROPERTY_CLUBHOUSE(iCurrentPropertyID)
															PRINT_HELP("CUST_CLU_MISO")
														ELSE
															PRINT_HELP("CUST_APT_MISO")
														ENDIF
													ENDIF
												ENDIF
											ENDIF
											SET_BIT(iLocalBS, LOCAL_BS_WANTED_HELP_TEXT)
										ENDIF
									ENDIF
								ELSE
									//CDEBUG1LN(DEBUG_SAFEHOUSE, "CDM: ok for context")
//									#IF FEATURE_EXECUTIVE
//									IF IS_PROPERTY_OFFICE(iCurrentPropertyID)
//									AND j = 1  //roof entrance has no buzzer!
//										//do nothing
//									ELSE
//									#ENDIF
										IF iBuzzerContextIntention = NEW_CONTEXT_INTENTION
											IF mpProperties[iCurrentPropertyID].entrance[j].iType = ENTRANCE_TYPE_HOUSE
												IF IS_PROPERTY_OFFICE(iCurrentPropertyID)
													REGISTER_CONTEXT_INTENTION(iBuzzerContextIntention,CP_MEDIUM_PRIORITY,"MP_BUZZ_OFF")
												ELSE
													IF IS_PROPERTY_CLUBHOUSE(iCurrentPropertyID)
														REGISTER_CONTEXT_INTENTION(iBuzzerContextIntention,CP_MEDIUM_PRIORITY,"MP_BUZZ_CLU")
													ELSE
														REGISTER_CONTEXT_INTENTION(iBuzzerContextIntention,CP_MEDIUM_PRIORITY,"MP_PROP_BUZZ1")
													ENDIF
												ENDIF
											ELSE
												REGISTER_CONTEXT_INTENTION(iBuzzerContextIntention,CP_MEDIUM_PRIORITY,"MP_PROP_BUZZ1b")
											ENDIF
											CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: registered context intention for buzzer")
										ENDIF
										IF HAS_CONTEXT_BUTTON_TRIGGERED(iBuzzerContextIntention)
											//CDEBUG1LN(DEBUG_SAFEHOUSE, "someone in game owns property")
											CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Player is starting enter menu as someone known owns it as well") NET_NL()
											CDEBUG1LN(DEBUG_SAFEHOUSE, "Player starting enter menu building # ", mpProperties[iCurrentPropertyID].iBuildingID)
											j = mpProperties[iCurrentPropertyID].iNumEntrances
											RELEASE_CONTEXT_INTENTION(iBuzzerContextIntention)
											SET_LOCAL_STAGE(STAGE_SETUP_FOR_MENU)
											iCurrentMenuType = MENU_TYPE_BUZZER
											CLEANUP_MENU_ASSETS()
											EXIT
										ENDIF
//									#IF FEATURE_EXECUTIVE
//									ENDIF
//									#ENDIF
								ENDIF
							ELSE
								//CDEBUG1LN(DEBUG_SAFEHOUSE, "CDM: wanted! = ")
								IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
									IF NOT IS_BIT_SET(ilocalBS, LOCAL_BS_WANTED_HELP_TEXT)
										IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
											IF mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iType = ENTRANCE_TYPE_GARAGE
												PRINT_HELP("CUST_GAR_WH")
											ELIF IS_PROPERTY_OFFICE(iCurrentPropertyID)
												PRINT_HELP("CUST_OFF_WH")
											ELIF IS_PROPERTY_CLUBHOUSE(iCurrentPropertyID)
												PRINT_HELP("CUST_CLU_WH")
											ELSE
												PRINT_HELP("CUST_APT_WH")
											ENDIF
										ELIF g_bDisablePropertyAccessForLoseCopMissionObj
											IF mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iType = ENTRANCE_TYPE_GARAGE
												PRINT_HELP("PROP_GAR_HEI_WOH")
											ELIF IS_PROPERTY_OFFICE(iCurrentPropertyID)
												PRINT_HELP("PROP_OFF_HEI_WOH")
											ELSE
												PRINT_HELP("PROP_APT_HEI_WOH")
											ENDIF
										ENDIF
										SET_BIT(ilocalBS, LOCAL_BS_WANTED_HELP_TEXT)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
//						CDEBUG2LN(DEBUG_SAFEHOUSE, "LOOP_ENTRANCES - IS_PLAYER_IN_BUZZER_LOCATION = FALSE")
					ENDIF
				//ELSE
//					IF DOES_LOCAL_PLAYER_OWN_PROPERTY_IN_BUILDING(mpProperties[iCurrentPropertyID].iBuildingID)
//						DRAW_ENTRANCE_MARKER(j)
//					ENDIF
				ELSE
					IF DOES_ANYONE_IN_GAME_OWN_PROPERTY_IN_BUILDING(iKnowOwnerState,mpProperties[iCurrentPropertyID].iBuildingID)
					ELSE
						CDEBUG2LN(DEBUG_SAFEHOUSE, "LOOP_ENTRANCES - DOES_ANYONE_IN_GAME_OWN_PROPERTY_IN_BUILDING = FALSE")
					ENDIF
					
					IF bLocalPlayerOwnsPropertyInBuilding
					ELSE
						CDEBUG2LN(DEBUG_SAFEHOUSE, "LOOP_ENTRANCES - bLocalPlayerOwnsPropertyInBuilding = FALSE")
					ENDIF
					
					IF SHOULD_BYPASS_CHECKS_FOR_ENTRY(FALSE)
					ELSE
						CDEBUG2LN(DEBUG_SAFEHOUSE, "LOOP_ENTRANCES - SHOULD_BYPASS_CHECKS_FOR_ENTRY = FALSE")
					ENDIF
					
					IF mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iType = ENTRANCE_TYPE_HOUSE
						
					ELSE
						CDEBUG1LN(DEBUG_SAFEHOUSE, "LOOP_ENTRANCES - iType != ENTRANCE_TYPE_HOUSE")
					ENDIF

					IF NOT DOES_PLAYER_HAVE_GROUP_ACCESS_TO_PROPERTY()
						CDEBUG2LN(DEBUG_SAFEHOUSE, "LOOP_ENTRANCES - NOT DOES_PLAYER_HAVE_GROUP_ACCESS_TO_PROPERTY")
					ENDIF
				ENDIF
			#IF IS_DEBUG_BUILD
			ELSE
				IF GET_GAME_TIMER() % 2500 < 50
					CDEBUG3LN(DEBUG_SAFEHOUSE, "LOOP_ENTRANCES - fDistance = ", fDistance)
				ENDIF
				#ENDIF
			ENDIF
			IF fDistance <= 10
			OR (fDistance <= 30 AND iCurrentPropertyID = PROPERTY_GARAGE_NEW_9)
			OR (fDistance <= 50 AND iCurrentPropertyID = PROPERTY_OFFICE_1 AND mpProperties[iCurrentPropertyID].entrance[j].iType = ENTRANCE_TYPE_GARAGE)
			OR (fDistance <= 55 AND iCurrentPropertyID = PROPERTY_OFFICE_2_BASE AND mpProperties[iCurrentPropertyID].entrance[j].iType = ENTRANCE_TYPE_GARAGE)
			OR (fDistance <= 100 AND iCurrentPropertyID = PROPERTY_OFFICE_3 AND mpProperties[iCurrentPropertyID].entrance[j].iType = ENTRANCE_TYPE_GARAGE)
			OR (fDistance <= 65 AND iCurrentPropertyID = PROPERTY_OFFICE_4 AND mpProperties[iCurrentPropertyID].entrance[j].iType = ENTRANCE_TYPE_GARAGE)
				//IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(),mpProperties[iCurrentPropertyID].entrance[j].vBuzzerLoc,<<1, 1, 2>>,FALSE,TRUE,TM_ON_FOOT)
					iEntrancePlayerIsUsing = j
					IF (bLocalPlayerOwnsPropertyInBuilding 
					AND DOES_PLAYER_HAVE_ACCESS_TO_ENTRANCE(iEntrancePlayerIsUsing,mpProperties[iCurrentPropertyID].iBuildingID)
					)
					OR (mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iType = ENTRANCE_TYPE_GARAGE
					AND SHOULD_BYPASS_CHECKS_FOR_ENTRY())
					OR (SHOULD_BYPASS_CHECKS_FOR_ENTRY(FALSE)
					AND mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iType = ENTRANCE_TYPE_HOUSE)
					OR DOES_PLAYER_HAVE_GROUP_ACCESS_TO_PROPERTY()
					
						bAllowedInWithVehicle = bLocalPlayerOwnsPropertyInBuilding
						PRINTLN("CDM: bAllowedInWithVehicle 1= ",bAllowedInWithVehicle) 
						IF (mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iType = ENTRANCE_TYPE_GARAGE
						AND (SHOULD_BYPASS_CHECKS_FOR_ENTRY()
						OR (DOES_PLAYER_HAVE_GROUP_ACCESS_TO_PROPERTY() 
						AND IS_THERE_GUEST_PARKING_AVAILABLE_FROM_PLAYER(warpinControl.propertyOwner))))
							IF NOT g_sMPTunables.bBIKER_CLUBHOUSE_DISABLE_MEMBER_SPACES
							OR SHOULD_BYPASS_CHECKS_FOR_ENTRY()
								bAllowedInWithVehicle = TRUE
								
								IF NOT SHOULD_BYPASS_CHECKS_FOR_ENTRY()
								AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
									IF NOT IS_LOCAL_PLAYER_IN_PERSONAL_VEHICLE()
										iNotAllowedReason = NOT_ALLOWED_ACCESS_ON_VEHICLE_NOT_PERSONAL
									ENDIF
								ENDIF
							ELSE
								PRINTLN("CDM: bAllowedInWithVehicle: false for prospect as bBIKER_CLUBHOUSE_DISABLE_MEMBER_SPACES")
							ENDIF
						ENDIF
						PRINTLN("CDM: bAllowedInWithVehicle 2= ",bAllowedInWithVehicle,"DOES_PLAYER_HAVE_GROUP_ACCESS_TO_PROPERTY() = ",DOES_PLAYER_HAVE_GROUP_ACCESS_TO_PROPERTY()," and ",IS_THERE_GUEST_PARKING_AVAILABLE_FROM_PLAYER(warpinControl.propertyOwner) ) 
						PRINTLN("iNotAllowedReason = ",iNotAllowedReason)
						//bLocalPlayerOwnsPropertyInBuilding = TRUE
						IF IS_PLAYER_ENTERING_THE_PROPERTY(bAllowedInWithVehicle,iNotAllowedReason)
						AND NOT IS_PLAYER_IN_BUZZER_LOCATION(iEntrancePlayerIsUsing)
						//IF iEntrancePlayerIsUsing = 0
//						IF DOES_ANYONE_IN_GAME_OWN_PROPERTY_IN_BUILDING(iKnowOwnerState,mpProperties[iCurrentPropertyID].iBuildingID)
//							IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) <= 0
//								CLEAR_BIT(ilocalBS, LOCAL_BS_WANTED_HELP_TEXT)
//								//AND mpProperties[iCurrentPropertyID].iType = PROPERTY_TYPE_GARAGE_HOUSE
//								CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Player is starting enter menu as someone known owns it as well") NET_NL()
//								CDEBUG1LN(DEBUG_SAFEHOUSE, "Player starting enter menu property building # ", mpProperties[iCurrentPropertyID].iBuildingID)
//								SET_LOCAL_STAGE(STAGE_SETUP_FOR_MENU)
//								iCurrentMenuType = MENU_TYPE_BUZZER
//								CLEANUP_MENU_ASSETS()
//								EXIT
//							ELSE
//								IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
//									IF NOT IS_BIT_SET(ilocalBS, LOCAL_BS_WANTED_HELP_TEXT)
//										IF mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iType = ENTRANCE_TYPE_GARAGE
//											PRINT_HELP("CUST_GAR_WH")
//										ELSE
//											PRINT_HELP("CUST_APT_WH")
//										ENDIF
//										SET_BIT(iLocalBS, LOCAL_BS_WANTED_HELP_TEXT)
//									ENDIF
//								ENDIF
//							ENDIF
//						ELSE
							IF bLocalPlayerOwnsPropertyInBuilding
							OR (mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iType = ENTRANCE_TYPE_GARAGE
							AND SHOULD_BYPASS_CHECKS_FOR_ENTRY())
							OR (SHOULD_BYPASS_CHECKS_FOR_ENTRY(FALSE)
							AND mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iType = ENTRANCE_TYPE_HOUSE)
							OR DOES_PLAYER_HAVE_GROUP_ACCESS_TO_PROPERTY()
								IF (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) <= 0
								OR ARE_PLAYER_STARS_GREYED_OUT(PLAYER_ID()))
								AND NOT g_bDisablePropertyAccessForLoseCopMissionObj
//									IF 
//									#IF FEATURE_HEIST_PLANNING
//									OR (mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iType = ENTRANCE_TYPE_HOUSE
//									AND PROPERTY_NEAR_PLYS_OPT_AVAILABLE(GET_PROPERTY_ENTRANCE_APPROXIMATE_LOCATION(iCurrentPropertyID,iEntrancePlayerIsUsing),tempPropertyExtMenu.bFriendsNearby,iCurrentPropertyID,iEntrancePlayerIsUsing))
//									#ENDIF
									IF (mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iType = ENTRANCE_TYPE_GARAGE
									AND SHOULD_BYPASS_CHECKS_FOR_ENTRY())
									OR (SHOULD_BYPASS_CHECKS_FOR_ENTRY(FALSE)
									AND mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iType = ENTRANCE_TYPE_HOUSE)
										CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Entering based on heist data - 2")
										bHadMissionCriticalEntityOnDropoff = TRUE
										PRINTLN("bHadMissionCriticalEntityOnDropoff Set True 4")
										SET_LOCAL_STAGE(STAGE_SETUP_FOR_USING_PROPERTY)
										EXIT
									ELSE
									
									IF IS_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITITCAL_ENTITY()
									OR IS_PLAYER_BLOCKED_FROM_PROPERTY(FALSE, GET_PROPERTY_TYPE_FOR_IS_PLAYER_BLOCKED_FROM_PROPERTY(iCurrentPropertyID))
										IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
											IF NOT IS_BIT_SET(ilocalBS, LOCAL_BS_WANTED_HELP_TEXT)
												IF IS_PLAYER_BLOCKED_FROM_PROPERTY(FALSE, GET_PROPERTY_TYPE_FOR_IS_PLAYER_BLOCKED_FROM_PROPERTY(iCurrentPropertyID))
													IF mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iType = ENTRANCE_TYPE_GARAGE
														PRINT_REASON_PLAYER_IS_BLOCKED_FROM_PROPERTY(TRUE,iCurrentPropertyID,ownedDetails)
													ELSE
														PRINT_REASON_PLAYER_IS_BLOCKED_FROM_PROPERTY(FALSE,iCurrentPropertyID,ownedDetails)
													ENDIF
												ELSE
													IF mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iType = ENTRANCE_TYPE_GARAGE
														PRINT_HELP("CUST_GAR_MISO")
													ELSE
														IF IS_PROPERTY_OFFICE(iCurrentPropertyID)
															PRINT_HELP("CUST_OFF_MISO")
														ELSE
															IF IS_PROPERTY_CLUBHOUSE(iCurrentPropertyID)
																PRINT_HELP("CUST_CLU_MISO")
															ELSE
																PRINT_HELP("CUST_APT_MISO")
															ENDIF
														ENDIF
													ENDIF
												ENDIF
												SET_BIT(iLocalBS, LOCAL_BS_WANTED_HELP_TEXT)
											ENDIF
										ENDIF
									ELSE
										CLEAR_BIT(ilocalBS, LOCAL_BS_WANTED_HELP_TEXT)
										CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_MP_PROPERTIES: Player is entering their property no one known owns it as well or garage") NET_NL()
										
										IF ((ownedDetails.iNumProperties > 1  AND NOT SHOULD_PLAYER_SKIP_OFFICE_GARAGE_MENU(ownedDetails.iNumProperties) )
										OR (mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iType = ENTRANCE_TYPE_HOUSE
										AND PROPERTY_NEAR_PLYS_OPT_AVAILABLE(GET_PROPERTY_ENTRANCE_APPROXIMATE_LOCATION(iCurrentPropertyID,iEntrancePlayerIsUsing),tempPropertyExtMenu.bFriendsNearby, tempPropertyExtMenu.bOrganisationNearby,  iCurrentPropertyID,iEntrancePlayerIsUsing)))
										AND NOT DOES_PLAYER_HAVE_GROUP_ACCESS_TO_PROPERTY()
											SET_BIT(iLocalBS2, LOCAL_BS2_bRunPropertySelectMenu)
											SET_BIT(iLocalBS2,LOCAL_BS2_DoNotBuzzOthers)
											SET_LOCAL_STAGE(STAGE_SETUP_FOR_MENU)
											iCurrentMenuType = MENU_TYPE_BUZZER
											CDEBUG1LN(DEBUG_SAFEHOUSE, "Player entered property building # ", mpProperties[iCurrentPropertyID].iBuildingID, ", Name: ", GET_BUILDING_ENUM_NAME(mpProperties[iCurrentPropertyID].iBuildingID))
											iCurrentPropertyID = mpProperties[iCurrentPropertyID].iIndex
										
											CDEBUG1LN(DEBUG_SAFEHOUSE, "Player entered property # ", mpProperties[iCurrentPropertyID].iIndex, ", Name: ", GET_PROPERTY_ENUM_NAME(mpProperties[iCurrentPropertyID].iIndex))
										ELSE
											
											SET_LOCAL_STAGE(STAGE_SETUP_FOR_USING_PROPERTY)
											IF DOES_PLAYER_HAVE_GROUP_ACCESS_TO_PROPERTY() 
												CDEBUG1LN(DEBUG_SAFEHOUSE, "D Player entered property building # ", mpProperties[iCurrentPropertyID].iBuildingID, ", Name: ", GET_BUILDING_ENUM_NAME(mpProperties[iCurrentPropertyID].iBuildingID))
												iCurrentPropertyID = mpProperties[iCurrentPropertyID].iIndex
												IF SHOULD_PLAYER_SKIP_OFFICE_GARAGE_MENU(ownedDetails.iNumProperties)
													iCurrentPropertyID = GET_PROPERTY_ID_FOR_FIRST_OFFICE_GARAGE_IN_BUILDING(mpProperties[iCurrentPropertyID].iBuildingID)
													CDEBUG1LN(DEBUG_SAFEHOUSE, "Overriding entered property for office garage to: ", iCurrentPropertyID)
												ENDIF
												CDEBUG1LN(DEBUG_SAFEHOUSE, "D Player entered property # ", ownedDetails.iReturnedPropertyIDs[0], ", Name: ", GET_PROPERTY_ENUM_NAME(ownedDetails.iReturnedPropertyIDs[0]))
											ELSE
												CDEBUG1LN(DEBUG_SAFEHOUSE, "E Player entered property building # ", mpProperties[iCurrentPropertyID].iBuildingID, ", Name: ", GET_BUILDING_ENUM_NAME(mpProperties[iCurrentPropertyID].iBuildingID))
												iCurrentPropertyID = ownedDetails.iReturnedPropertyIDs[0]
												IF SHOULD_PLAYER_SKIP_OFFICE_GARAGE_MENU(ownedDetails.iNumProperties)
													iCurrentPropertyID = GET_PROPERTY_ID_FOR_FIRST_OFFICE_GARAGE_IN_BUILDING(mpProperties[iCurrentPropertyID].iBuildingID)
													CDEBUG1LN(DEBUG_SAFEHOUSE, "Overriding entered property for office garage to: ", iCurrentPropertyID)
												ENDIF
												CDEBUG1LN(DEBUG_SAFEHOUSE, "E Player entered property # ", ownedDetails.iReturnedPropertyIDs[0], ", Name: ", GET_PROPERTY_ENUM_NAME(ownedDetails.iReturnedPropertyIDs[0]))
											ENDIF
										ENDIF
										DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
//										IF NOT (GET_PROPERTY_SIZE_TYPE(iCurrentPropertyID) != PROP_SIZE_TYPE_LARGE_APT
//										AND mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iType = ENTRANCE_TYPE_GARAGE)
											NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
//										ENDIF
										SET_BIT(iLocalBS, LOCAL_BS_TAKEN_AWAY_CONTROL)
										
										globalPropertyEntryData.ownerID = INVALID_PLAYER_INDEX()
										CLEANUP_MENU_ASSETS()
										EXIT
									ENDIF
									
									ENDIF
								ELSE
									IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
										IF NOT IS_BIT_SET(ilocalBS, LOCAL_BS_WANTED_HELP_TEXT)
											IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
												IF mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iType = ENTRANCE_TYPE_GARAGE
													PRINT_HELP("CUST_GAR_WH")
												ELIF IS_PROPERTY_OFFICE(iCurrentPropertyID)
													PRINT_HELP("CUST_OFF_WH")
												ELIF IS_PROPERTY_CLUBHOUSE(iCurrentPropertyID)
													PRINT_HELP("CUST_CLU_WH")
												ELSE
													PRINT_HELP("CUST_APT_WH")
												ENDIF
											ELIF g_bDisablePropertyAccessForLoseCopMissionObj
												IF mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iType = ENTRANCE_TYPE_GARAGE
													PRINT_HELP("PROP_GAR_HEI_WOH")
												ELIF IS_PROPERTY_OFFICE(iCurrentPropertyID)
													PRINT_HELP("PROP_OFF_HEI_WOH")
												ELSE
													PRINT_HELP("PROP_APT_HEI_WOH")
												ENDIF
											ENDIF
											SET_BIT(iLocalBS, LOCAL_BS_WANTED_HELP_TEXT)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					//ENDIF
						///ELSE
						//	NET_PRINT("MAINTAIN_MP_PROPERTIES: Player is entering their property no one known owns it as well") NET_NL()
						//	CDEBUG1LN(DEBUG_SAFEHOUSE, "Player entered property # ", mpProperties[iCurrentPropertyID].iIndex)
						//	SET_LOCAL_STAGE(STAGE_INTRO_IF_APPLICABLE)
						//	globalPropertyEntryData.ownerID = INVALID_PLAYER_INDEX()
						//	CLEANUP_MENU_ASSETS()
						//ENDIF
//					ELSE
//						IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
//							IF NOT IS_BIT_SET(ilocalBS, LOCAL_BS_WANTED_HELP_TEXT)
//								PRINT_HELP("CUST_GAR_WH")
//								SET_BIT(iLocalBS, LOCAL_BS_WANTED_HELP_TEXT)
//							ENDIF
//						ENDIF
					ENDIF
			ENDIF
		ENDREPEAT
	#IF IS_DEBUG_BUILD
	ELSE
		IF db_bFullDebug
			IF IS_PLAYER_TELEPORT_ACTIVE()
				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Loop entrances IS_PLAYER_TELEPORT_ACTIVE TRUE ")
			ELIF IS_PLAYER_IN_CORONA()
				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Loop entrances IS_PLAYER_IN_CORONA TRUE ")
			ELIF IS_CUSTOM_MENU_ON_SCREEN()
				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Loop entrances IS_POST_MISSION_CLEANUP_REQUIRED_OR_ACTIVE	TRUE ")
			ELIF IS_PLAYER_PARTICIPATING_IN_A_SKYDIVE_CHALLENGE(PLAYER_ID())
				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Loop entrances IS_PLAYER_PARTICIPATING_IN_A_SKYDIVE_CHALLENGE	TRUE ")
			ENDIF
		ENDIF
	#ENDIF
	ENDIF
ENDPROC


PROC HANDLE_PURCHASE_LOCATE()
	IF forSaleDetails.locate.fWidth != 0
		IF IS_NET_PLAYER_OK(PLAYER_ID())
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(),forSaleDetails.locate.vPos1,forSaleDetails.locate.vPos2,forSaleDetails.locate.fWidth,FALSE,TRUE,TM_ON_FOOT)
				IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) <= 0
				AND NOT IS_PLAYER_GETTING_IN_OR_OUT_OF_VEHICLE(PLAYER_ID())
				AND NOT IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_TIME_TRIAL)
				AND NOT IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_RC_TIME_TRIAL)
					IF iPurchaseContextIntention = NEW_CONTEXT_INTENTION
						IF mpProperties[iCurrentPropertyID].iType = PROPERTY_TYPE_GARAGE
							REGISTER_CONTEXT_INTENTION(iPurchaseContextIntention,CP_MEDIUM_PRIORITY,"MP_PROP_PUR1")
						ELIF IS_PROPERTY_OFFICE(iCurrentPropertyID)
							REGISTER_CONTEXT_INTENTION(iPurchaseContextIntention,CP_MEDIUM_PRIORITY,"MP_PROP_OFF_BUY")
						ELSE
							REGISTER_CONTEXT_INTENTION(iPurchaseContextIntention,CP_MEDIUM_PRIORITY,"MP_PROP_PUR0")
						ENDIF
					ELSE
						IF HAS_CONTEXT_BUTTON_TRIGGERED(iPurchaseContextIntention)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
							IF IS_PLAYER_GETTING_IN_OR_OUT_OF_VEHICLE(PLAYER_ID())
								CLEAR_PED_TASKS(PLAYER_PED_ID())
								CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_M_PROPERTY_EXT: Clearing ped tasks they are trying to enter vehicle while using property menu")
							ENDIF
							//CDEBUG1LN(DEBUG_SAFEHOUSE, "someone in game owns property")
							CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Player trigger purchase of property option") NET_NL()
							RELEASE_CONTEXT_INTENTION(iPurchaseContextIntention)
							SET_LOCAL_STAGE(STAGE_SETUP_FOR_MENU)
							iCurrentMenuType = MENU_TYPE_PURCHASE
							CLEANUP_MENU_ASSETS()
							EXIT
						ENDIF
					ENDIF
					SET_BIT(iLocalBS2,LOCAL_BS2_bKeepPurchaseContext)
				ELSE
					SET_BIT(iLocalBS2,LOCAL_BS2_bKeepWantedFlag)
					IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
						IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
							IF NOT IS_BIT_SET(ilocalBS, LOCAL_BS_WANTED_HELP_TEXT)
								PRINT_HELP("CUST_PROP_W")
								SET_BIT(ilocalBS, LOCAL_BS_WANTED_HELP_TEXT)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC RETAIN_BUZZER_MARKER()
	FLOAT fDistance
	BOOL bLocalPlayerOwnsPropertyInBuilding
	INT j
	BOOl bDrawnBuzzer
	IF NOT IS_PLAYER_TELEPORT_ACTIVE()
	
		IF DOES_LOCAL_PLAYER_OWN_PROPERTY_IN_BUILDING(mpProperties[iCurrentPropertyID].iBuildingID,ownedDetails)
			bLocalPlayerOwnsPropertyInBuilding = TRUE
		ENDIF
		REPEAT mpProperties[iCurrentPropertyID].iNumEntrances j
			bDrawnBuzzer = FALSE
			fDistance = GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()),mpProperties[iCurrentPropertyID].entrance[j].vEntranceMarkerLoc)
			IF fDistance <= 30
			OR (fDistance <= 70 AND iCurrentPropertyID = PROPERTY_OFFICE_1 AND mpProperties[iCurrentPropertyID].entrance[j].iType = ENTRANCE_TYPE_GARAGE)
			OR (fDistance <= 55 AND iCurrentPropertyID = PROPERTY_OFFICE_2_BASE AND mpProperties[iCurrentPropertyID].entrance[j].iType = ENTRANCE_TYPE_GARAGE)
			OR (fDistance <= 100 AND iCurrentPropertyID = PROPERTY_OFFICE_3 AND mpProperties[iCurrentPropertyID].entrance[j].iType = ENTRANCE_TYPE_GARAGE)
			OR (fDistance <= 65 AND iCurrentPropertyID = PROPERTY_OFFICE_4 AND mpProperties[iCurrentPropertyID].entrance[j].iType = ENTRANCE_TYPE_GARAGE)
				IF SHOULD_BYPASS_CHECKS_FOR_ENTRY(FALSE)
				AND mpProperties[iCurrentPropertyID].entrance[j].iType = ENTRANCE_TYPE_HOUSE
					//DRAW_BUZZER_LOCATE(j)
					bDrawnBuzzer = TRUE
				ENDIF
				IF NOT bDrawnBuzzer
					IF DOES_ANYONE_IN_GAME_OWN_PROPERTY_IN_BUILDING(iKnowOwnerState,mpProperties[iCurrentPropertyID].iBuildingID)
					OR bLocalPlayerOwnsPropertyInBuilding
						DRAW_BUZZER_LOCATE(j)
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

PROC SET_ENTRY_CS_STAGE(INT iStage)
	#IF IS_DEBUG_BUILD
	SWITCH iStage
		CASE ENTRY_CS_STAGE_INIT_DATA
			CDEBUG1LN(DEBUG_SAFEHOUSE, "SET_ENTRY_CS_STAGE - ENTRY_CS_STAGE_INIT_DATA")
		BREAK
		CASE ENTRY_CS_STAGE_WAIT_FOR_MODELS
			CDEBUG1LN(DEBUG_SAFEHOUSE, "SET_ENTRY_CS_STAGE - ENTRY_CS_STAGE_WAIT_FOR_MODELS")
		BREAK
		CASE ENTRY_CS_STAGE_TRIGGER_CUT
			CDEBUG1LN(DEBUG_SAFEHOUSE, "SET_ENTRY_CS_STAGE - ENTRY_CS_STAGE_TRIGGER_CUT")
		BREAK
		CASE ENTRY_CS_STAGE_RUN_CUT
			CDEBUG1LN(DEBUG_SAFEHOUSE, "SET_ENTRY_CS_STAGE - ENTRY_CS_STAGE_RUN_CUT")
		BREAK
		CASE ENTRY_CS_STAGE_RUN_SECOND_CUT
			CDEBUG1LN(DEBUG_SAFEHOUSE, "SET_ENTRY_CS_STAGE - ENTRY_CS_STAGE_RUN_SECOND_CUT")
		BREAK
		CASE ENTRY_CS_STAGE_HANDLE_PLAYER_PED
			CDEBUG1LN(DEBUG_SAFEHOUSE, "SET_ENTRY_CS_STAGE - ENTRY_CS_STAGE_HANDLE_PLAYER_PED")
		BREAK
	ENDSWITCH
	#ENDIF
	entryCutData.iStage = iStage
ENDPROC

INT iApartmentDoorSoundStage
INT iApartmentBuzzerSoundStage

PROC DO_APARTMENT_BUZZER_PRESS_SOUNDS()
	FLOAT fSoundTime 
	IF GET_PROPERTY_SIZE_TYPE(mpProperties[iCurrentPropertyID].iIndex) = PROP_SIZE_TYPE_LARGE_APT

		IF NOT IS_STRING_NULL_OR_EMPTY(entryCutData.animName[SYNC_SCENE_PED_A] )
			IF ARE_STRINGS_EQUAL(entryCutData.animName[SYNC_SCENE_PED_A] ,"Buzz_Short")
				fSoundTime = 0.353
			ELIF ARE_STRINGS_EQUAL(entryCutData.animName[SYNC_SCENE_PED_A] ,"Buzz_Reg")
				fSoundTime = 0.320
			ELSE
				fSoundTime = 0.373
			ENDIF
		ENDIF
		IF entryCutData.iSyncedSceneID >= 0
			IF IS_SYNCHRONIZED_SCENE_RUNNING(entryCutData.iSyncedSceneID)
				SWITCH iApartmentBuzzerSoundStage
					CASE 0	//Door starts opening
						IF GET_SYNCHRONIZED_SCENE_PHASE(entryCutData.iSyncedSceneID) >= fSoundTime
							IF NOT IS_PED_INJURED(entryCutData.playerClone)
								IF entryCutData.iSoundID < 0
									entryCutData.iSoundID = GET_SOUND_ID()
								ENDIF
								PLAY_SOUND_FROM_ENTITY(entryCutData.iSoundID, "DOOR_BUZZ_ONESHOT_MASTER",entryCutData.playerClone, "GTAO_APT_DOOR_DOWNSTAIRS_GLASS_SOUNDS")
								CDEBUG1LN(DEBUG_SAFEHOUSE, "DO_APARTMENT_BUZZER_PRESS_SOUNDS- Doing buzzer one shot")
							ENDIF
							
							iApartmentBuzzerSoundStage++
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC DO_APARTMENT_DOOR_SOUNDS()
	
	FLOAT fDoorOpen, fDoorShutting//, fDoorClosed
	
	IF bUseNewEntryAnims
		IF NOT IS_STRING_NULL_OR_EMPTY(entryCutData.animDictionary)
			IF ARE_STRINGS_EQUAL(entryCutData.animDictionary,"anim@apt_trans@hinge_l")
				fDoorOpen = 0.271
				fDoorShutting = 0.411
				//fDoorClosed = 0.542
			ELSE
				fDoorOpen = 0.242
				fDoorShutting = 0.418
				//fDoorClosed = 0.598
			ENDIF
		ENDIF
		IF entryCutData.iSyncedSceneID >= 0
			IF IS_SYNCHRONIZED_SCENE_RUNNING(entryCutData.iSyncedSceneID)
				SWITCH iApartmentDoorSoundStage
					CASE 0	//Door starts opening
					
						IF GET_SYNCHRONIZED_SCENE_PHASE(entryCutData.iSyncedSceneID) >= fDoorOpen
							IF entryCutData.iSoundID < 0
								entryCutData.iSoundID = GET_SOUND_ID()
							ENDIF
							
							IF GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_BUILDING_57	
							OR GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_BUILDING_58	
							OR GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_BUILDING_59	
							OR GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_BUILDING_60	
							OR GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_BUILDING_61	
							OR GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_BUILDING_62	
							OR GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_BUILDING_63
								PLAY_SOUND_FRONTEND(entryCutData.iSoundID, "WOODEN_DOOR_OPEN_NO_HANDLE_AT")
							ELIF IS_PROPERTY_STILT_APARTMENT(iCurrentPropertyID)
								PLAY_SOUND_FRONTEND(entryCutData.iSoundID, "PUSH", "GTAO_APT_DOOR_DOWNSTAIRS_WOOD_SOUNDS")
							ELIF GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_CLUBHOUSE_BUILDING_6
								PLAY_SOUND_FRONTEND(entryCutData.iSoundID, "PUSH", "GTAO_APT_DOOR_DOWNSTAIRS_GENERIC_SOUNDS")
							ELIF (GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_OFFICE_BUILDING_1
							OR GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_OFFICE_BUILDING_2
							OR GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_OFFICE_BUILDING_3
							OR GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_OFFICE_BUILDING_4)
							AND iEntrancePlayerIsUsing = 1
								PLAY_SOUND_FRONTEND(entryCutData.iSoundID, "PUSH", "GTAO_APT_DOOR_ROOF_METAL_SOUNDS")
							ELIF GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_CLUBHOUSE_BUILDING_1
							OR GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_CLUBHOUSE_BUILDING_2
							OR GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_CLUBHOUSE_BUILDING_3
							OR GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_CLUBHOUSE_BUILDING_4
							OR GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_CLUBHOUSE_BUILDING_5
							OR GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_CLUBHOUSE_BUILDING_7
							OR GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_CLUBHOUSE_BUILDING_8
							OR GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_CLUBHOUSE_BUILDING_9
							OR GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_CLUBHOUSE_BUILDING_10
							OR GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_CLUBHOUSE_BUILDING_11
							OR GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_CLUBHOUSE_BUILDING_12
								PLAY_SOUND_FRONTEND(entryCutData.iSoundID, "PUSH", "GTAO_APT_DOOR_ROOF_METAL_SOUNDS")
							ELSE
								PLAY_SOUND_FRONTEND(entryCutData.iSoundID, "PUSH", "GTAO_APT_DOOR_DOWNSTAIRS_GLASS_SOUNDS")
							ENDIF
							iApartmentDoorSoundStage++
						ENDIF
					BREAK
					CASE 1	//Door begins to swing shut
						IF GET_SYNCHRONIZED_SCENE_PHASE(entryCutData.iSyncedSceneID) >= fDoorShutting

							IF entryCutData.iSoundID < 0
								entryCutData.iSoundID = GET_SOUND_ID()
							ENDIF
							IF GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_BUILDING_57	
							OR GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_BUILDING_58	
							OR GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_BUILDING_59	
							OR GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_BUILDING_60	
							OR GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_BUILDING_61	
							OR GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_BUILDING_62	
							OR GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_BUILDING_63
							ELIF IS_PROPERTY_STILT_APARTMENT(iCurrentPropertyID)
								PLAY_SOUND_FRONTEND(entryCutData.iSoundID, "LIMIT", "GTAO_APT_DOOR_DOWNSTAIRS_WOOD_SOUNDS")
							ELIF GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_CLUBHOUSE_BUILDING_6
								PLAY_SOUND_FRONTEND(entryCutData.iSoundID, "LIMIT", "GTAO_APT_DOOR_DOWNSTAIRS_GENERIC_SOUNDS")
							ELIF (GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_OFFICE_BUILDING_1
							OR GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_OFFICE_BUILDING_2
							OR GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_OFFICE_BUILDING_3
							OR GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_OFFICE_BUILDING_4)
							AND iEntrancePlayerIsUsing = 1
								PLAY_SOUND_FRONTEND(entryCutData.iSoundID, "LIMIT", "GTAO_APT_DOOR_ROOF_METAL_SOUNDS")
							ELIF GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_CLUBHOUSE_BUILDING_1
							OR GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_CLUBHOUSE_BUILDING_2
							OR GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_CLUBHOUSE_BUILDING_3
							OR GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_CLUBHOUSE_BUILDING_4
							OR GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_CLUBHOUSE_BUILDING_5
							OR GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_CLUBHOUSE_BUILDING_7
							OR GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_CLUBHOUSE_BUILDING_8
							OR GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_CLUBHOUSE_BUILDING_9
							OR GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_CLUBHOUSE_BUILDING_10
							OR GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_CLUBHOUSE_BUILDING_11
							OR GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_CLUBHOUSE_BUILDING_12
								PLAY_SOUND_FRONTEND(entryCutData.iSoundID, "LIMIT", "GTAO_APT_DOOR_ROOF_METAL_SOUNDS")
							ELSE
								PLAY_SOUND_FRONTEND(entryCutData.iSoundID, "LIMIT", "GTAO_APT_DOOR_DOWNSTAIRS_GLASS_SOUNDS")
							ENDIF

							iApartmentDoorSoundStage++
						ENDIF
					BREAK
//					CASE 2	//Door has closed
//						IF GET_SYNCHRONIZED_SCENE_PHASE(entryCutData.iSyncedSceneID) >= fDoorClosed
//							IF entryCutData.iSoundID < 0
//								entryCutData.iSoundID = GET_SOUND_ID()
//							ENDIF
//							PLAY_SOUND_FRONTEND(entryCutData.iSoundID, "SWING_SHUT", "GTAO_APT_DOOR_DOWNSTAIRS_GLASS_SOUNDS")
//							
//							iApartmentDoorSoundStage++
//						ENDIF
//					BREAK
				ENDSWITCH
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC STRING GET_FRONT_DOOR_ENTRY_CUTSCENE_ANIM_DICT(INT iBuildingID)
	STRING returnString
	returnString = "anim@apt_trans@hinge_r"
	SWITCH iBuildingID
		CASE MP_PROPERTY_BUILDING_17
		CASE MP_PROPERTY_BUILDING_20
		CASE MP_PROPERTY_BUILDING_58
		CASE MP_PROPERTY_STILT_BUILDING_5_BASE_A
			returnString = "anim@apt_trans@hinge_l"
		BREAK
	ENDSWITCH
	CDEBUG2LN(DEBUG_SAFEHOUSE, "GET_FRONT_DOOR_ENTRY_CUTSCENE_ANIM_DICT: iBuildingID = ", iBuildingID, ", AnimDict = ", returnString)
	RETURN returnString
ENDFUNC

FUNC BOOL GET_ENTRY_CUTSCENE_DATA(ENTRY_CUTSCENE_TYPE type, BOOL bSkipSecondCut = FALSE)
	
	MODEL_NAMES modelToLoad
	IF NOT IS_BIT_SET(entryCutData.iBS,ENTRY_CS_BS_GET_DATA)
		iApartmentDoorSoundStage = 0
		SWITCH type
			CASE ENTRY_CUTSCENE_TYPE_BUZZER
				
				entryCutData.animDictionary = "ANIM@APT_TRANS@BUZZER"
				IF mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_BUILDING_5
				AND mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iType = ENTRANCE_TYPE_GARAGE
					entryCutData.animName[SYNC_SCENE_PED_A] = "buzz_short"
				ELSE
					entryCutData.animName[SYNC_SCENE_PED_A] = "buzz_reg"
				ENDIF

				GET_TRANSITION_BUZZ_CAM_DETAILS(mpProperties[iCurrentPropertyID].iBuildingID, tempCamOffset,iEntrancePlayerIsUsing) 
//				tempPropOffset = GET_EXTERIOR_SYNC_SCENE_LOCATION(mpProperties[iCurrentPropertyID].iBuildingID, EXTERIOR_SYNC_SCENE_ELEMENT_APT_BUZZ_CAM)
				//cam <<-779.8222, 312.4101, 86.2709>> <<-9.3334, 0.0000, -44.5438>>
				entryCutData.vCamLoc[0] = tempCamOffset.vLoc
				entryCutData.vCamRot[0] = tempCamOffset.vRot
				entryCutData.fCamFOV[0] = TO_FLOAT(tempCamOffset.iFov)
				#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: EXT: ENTRY ANIM: CAMERA tempPropOffset: ",  tempPropOffset.vLoc, ", ROT: ", tempPropOffset.vRot)
				#ENDIF
				//scene << -778.860, 313.440, 84.670 >> << -1.725, -0.000, 2.215 >>
				//tempPropOffset = GET_EXTERIOR_SYNC_SCENE_LOCATION(mpProperties[iCurrentPropertyID].iBuildingID,EXTERIOR_SYNC_SCENE_ELEMENT_APT_BUZZ_LOC)
				GET_TRANSITION_BUZZ_SCENE_DETAILS(mpProperties[iCurrentPropertyID].iBuildingID, tempPropOffset,iEntrancePlayerIsUsing) 
				#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: EXT: ENTRY ANIM: Buzzer Scene tempPropOffset: ",  tempPropOffset.vLoc, ", ROT: ", tempPropOffset.vRot)
				#ENDIF
				entryCutData.vSyncSceneLoc = tempPropOffset.vLoc
				entryCutData.vSyncSceneRot = tempPropOffset.vRot
				IF NOT DOES_ENTITY_EXIST(entryCutData.playerClone)
					//IF IS_PED_MALE(PLAYER_PED_ID()) 
					IF NOT IS_PLAYER_FEMALE()
						entryCutData.playerClone = CREATE_PED(PEDTYPE_CIVMALE, GET_ENTITY_MODEL(PLAYER_PED_ID()), GET_ENTITY_COORDS(PLAYER_PED_ID()) + <<0.0, 0.0, -10.0>>, GET_ENTITY_HEADING(PLAYER_PED_ID()), FALSE, FALSE)
					ELSE
						entryCutData.playerClone = CREATE_PED(PEDTYPE_CIVFEMALE, GET_ENTITY_MODEL(PLAYER_PED_ID()), GET_ENTITY_COORDS(PLAYER_PED_ID()) + <<0.0, 0.0, -10.0>>, GET_ENTITY_HEADING(PLAYER_PED_ID()), FALSE, FALSE)
					ENDIF
					CLONE_PED_TO_TARGET(PLAYER_PED_ID(), entryCutData.playerClone)
					UPDATE_MC_EMBLEM(GET_PLAYER_INDEX(), entryCutData.playerClone)
				ENDIF
				FREEZE_ENTITY_POSITION(entryCutData.playerClone,TRUE)
				SET_ENTITY_PROOFS(entryCutData.playerClone,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE)
				entryCutData.fSyncedSceneCompleteStage = 0.6
				CDEBUG1LN(DEBUG_SAFEHOUSE, "GET_ENTRY_CUTSCENE_DATA- for buzzer")
			BREAK
			
			CASE ENTRY_CUTSCENE_TYPE_FRONT_DOOR
				entryCutData.animDictionary = "anim@apt_trans@hinge_r"
				IF mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_BUILDING_17
				OR mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_BUILDING_20
				OR mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_BUILDING_58
				OR mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_STILT_BUILDING_1_BASE_B		
				OR mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_STILT_BUILDING_2_B								
				OR mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_STILT_BUILDING_4_B				
				OR mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_STILT_BUILDING_5_BASE_A					
				OR mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_STILT_BUILDING_7_A
				OR mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_STILT_BUILDING_13_A	
				OR mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_OFFICE_BUILDING_2 AND iEntrancePlayerIsUsing = 0
				OR mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_OFFICE_BUILDING_1 
				OR (mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_OFFICE_BUILDING_4)// AND iEntrancePlayerIsUsing = 0)
				OR mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_CLUBHOUSE_BUILDING_1
				OR mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_CLUBHOUSE_BUILDING_4
				OR mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_CLUBHOUSE_BUILDING_5
				OR mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_CLUBHOUSE_BUILDING_6
				OR mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_CLUBHOUSE_BUILDING_7
				OR mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_CLUBHOUSE_BUILDING_8
				OR mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_CLUBHOUSE_BUILDING_9
				OR mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_CLUBHOUSE_BUILDING_10
				OR mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_CLUBHOUSE_BUILDING_11
				OR mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_CLUBHOUSE_BUILDING_12
					entryCutData.animDictionary = "anim@apt_trans@hinge_l"
//				ELIF mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_BUILDING_16
//					entryCutData.animDictionary = "anim@apt_trans@hinge_r_offset"
				ENDIF
				
				// office 2 hinge 2
				CDEBUG1LN(DEBUG_SAFEHOUSE, "GET_ENTRY_CUTSCENE_DATA: animDictionary = ", entryCutData.animDictionary)
//				entryCutData.animDictionary = GET_FRONT_DOOR_ENTRY_CUTSCENE_ANIM_DICT(mpProperties[iCurrentPropertyID].iBuildingID)
				entryCutData.animName[SYNC_SCENE_PED_A] = "ext_player"
				entryCutData.animName[SYNC_SCENE_DOOR] = "ext_door"
				IF iCurrentPropertyID != PROPERTY_OFFICE_2_BASE
					modelToLoad = GET_SPECIAL_PROPERTY_DOOR_FOR_ANIMATION(mpProperties[iCurrentPropertyID].iBuildingID,iEntrancePlayerIsUsing )
					 
					CDEBUG1LN(DEBUG_SAFEHOUSE, "GET_ENTRY_CUTSCENE_DATA- using GET_SPECIAL_PROPERTY_DOOR_FOR_ANIMATION, doorModel = ", modelToLoad)
				ELIF IS_PROPERTY_OFFICE(iCurrentPropertyID)
					modelToLoad = GET_SPECIAL_PROPERTY_DOOR_FOR_ANIMATION(mpProperties[iCurrentPropertyID].iBuildingID,iEntrancePlayerIsUsing )
					
					CDEBUG1LN(DEBUG_SAFEHOUSE, "GET_ENTRY_CUTSCENE_DATA- using GET_SPECIAL_PROPERTY_DOOR_FOR_ANIMATION, doorModel = ", modelToLoad)
				ENDIF
				IF modelToLoad != DUMMY_MODEL_FOR_SCRIPT
					CDEBUG1LN(DEBUG_SAFEHOUSE, "GET_ENTRY_CUTSCENE_DATA- Creating a special door object")
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "GET_ENTRY_CUTSCENE_DATA- Creating a non special door object")
					IF GET_PROPERTY_BUILDING(iCurrentPropertyID) != MP_PROPERTY_BUILDING_5
						//IF GET_PROPERTY_SIZE_TYPE(iCurrentPropertyID) = PROP_SIZE_TYPE_LARGE_APT
						IF IS_PROPERTY_OFFICE(iCurrentPropertyID)
							modelToLoad = propertyDoors[iEntrancePlayerIsUsing].doorModel
						ELSE
							IF NOT ARE_VECTORS_EQUAL(propertyDoors[1].vCoords,<<0,0,0>>)
								modelToLoad = propertyDoors[1].doorModel
							ELSE
								modelToLoad = propertyDoors[0].doorModel
							ENDIF
						ENDIF
					ELSE
						modelToLoad = propertyDoors[0].doorModel
					ENDIF
				ENDIF
				
				IF NOT REQUEST_LOAD_MODEL(modelToLoad)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "Waiting for model to load 121331232")
					RETURN FALSE
				ENDIF
				
				
//				tempPropOffset = GET_EXTERIOR_SYNC_SCENE_LOCATION(mpProperties[iCurrentPropertyID].iBuildingID,EXTERIOR_SYNC_SCENE_ELEMENT_APT_ENTER_CAM)
//				entryCutData.vCamLoc[0] = tempPropOffset.vLoc
//				entryCutData.vCamRot[0] = tempPropOffset.vRot
//				entryCutData.fCamFOV[0] = 35
				
				tempCamOffset.vLoc = <<0,0,0>>
				tempCamOffset.vRot = <<0,0,0>>
				tempCamOffset.iFov = 0
				//CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: TRANS: mpProperties[iCurrentPropertyID].iBuildingID: ", mpProperties[iCurrentPropertyID].iBuildingID)
				GET_PROPERTY_TRANSITION_CAM_DETAILS(mpProperties[iCurrentPropertyID].iBuildingID, iEntrancePlayerIsUsing, TRUE, tempCamOffset) //WARPING WORK CDM 176870
				entryCutData.vCamLoc[0] = tempCamOffset.vLoc
				entryCutData.vCamRot[0] = tempCamOffset.vRot
				entryCutData.fCamFOV[0] = TO_FLOAT(tempCamOffset.iFov)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: TRANS: vCameraPos", tempCamOffset.vLoc)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: TRANS: vCameraRot", tempCamOffset.vRot)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: TRANS: fCameraFov", tempCamOffset.iFov)
				
				
				//tempPropOffset = GET_EXTERIOR_SYNC_SCENE_LOCATION(mpProperties[iCurrentPropertyID].iBuildingID,EXTERIOR_SYNC_SCENE_ELEMENT_APT_ENTER_LOC)
				GET_TRANSITION_ENTER_SCENE_DETAILS(mpProperties[iCurrentPropertyID].iBuildingID, tempPropOffset,iEntrancePlayerIsUsing )
				entryCutData.vSyncSceneLoc = tempPropOffset.vLoc
				entryCutData.vSyncSceneRot = tempPropOffset.vRot
				CDEBUG1LN(DEBUG_SAFEHOUSE, "Entry enter scene location", entryCutData.vSyncSceneLoc)
				SET_BIT(entryCutData.iBS,ENTRY_CS_BS_SECOND_CUT)
				IF bSkipSecondCut
					CLEAR_BIT(entryCutData.iBS,ENTRY_CS_BS_SECOND_CUT)
				ENDIF
								
				IF NOT DOES_ENTITY_EXIST(entryCutData.objIDs[0])

				AND NOT IS_BUILDING_INDEPENDENCE_DAY_APARTMENT(GET_PROPERTY_BUILDING(iCurrentPropertyID))
				//AND GET_PROPERTY_BUILDING(iCurrentPropertyID) != MP_PROPERTY_STILT_BUILDING_5_BASE_A
				AND NOT IS_PROPERTY_CLUBHOUSE(iCurrentPropertyID)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "GET_ENTRY_CUTSCENE_DATA - Door doesn't exist and property isn't independence day so making doors")
//					entryCutData.objIDs[0] = CREATE_OBJECT_NO_OFFSET(propertyDoors[1].doorModel, propertyDoors[1].vCoords  + <<0.0, 0.0, -20.0>>,FALSE,FALSE,TRUE)
//					SET_BIT(entryCutData.iBS,ENTRY_CS_BS_APT_DOOR_MODEL_HIDE)
					IF GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_BUILDING_6
						entryCutData.objIDs[0] = CREATE_OBJECT_NO_OFFSET(modelToLoad, GET_PLAYER_COORDS(PLAYER_ID()) + <<0.0, 0.0, -20.0>>,FALSE,FALSE,TRUE)
					ELIF GET_PROPERTY_BUILDING(iCurrentPropertyID) != MP_PROPERTY_BUILDING_5
						CDEBUG1LN(DEBUG_SAFEHOUSE, "GET_ENTRY_CUTSCENE_DATA- Creating <> MP_PROPERTY_BUILDING_5")
						//IF GET_PROPERTY_SIZE_TYPE(iCurrentPropertyID) = PROP_SIZE_TYPE_LARGE_APT
						IF IS_PROPERTY_OFFICE(iCurrentPropertyID)
							entryCutData.objIDs[0] = CREATE_OBJECT_NO_OFFSET(modelToLoad, propertyDoors[iEntrancePlayerIsUsing].vCoords  + <<0.0, 0.0, -20.0>>,FALSE,FALSE,TRUE)
							CDEBUG1LN(DEBUG_SAFEHOUSE, "GET_ENTRY_CUTSCENE_DATA- iEntrancePlayerIsUsing",iEntrancePlayerIsUsing)
						ELSE
							IF NOT ARE_VECTORS_EQUAL(propertyDoors[1].vCoords,<<0,0,0>>)
								entryCutData.objIDs[0] = CREATE_OBJECT_NO_OFFSET(modelToLoad, propertyDoors[1].vCoords  + <<0.0, 0.0, -20.0>>,FALSE,FALSE,TRUE)
							ELSE
								entryCutData.objIDs[0] = CREATE_OBJECT_NO_OFFSET(modelToLoad, propertyDoors[0].vCoords  + <<0.0, 0.0, -20.0>>,FALSE,FALSE,TRUE)
	//							IF GET_PROPERTY_BUILDING(iCurrentPropertyID) != MP_PROPERTY_BUILDING_63
	//								SET_ENTITY_ROTATION(modelToLoad, propertyDoors[0].v)	
	//							ENDIF
							ENDIF
						ENDIF
					ELSE
						entryCutData.objIDs[0] = CREATE_OBJECT_NO_OFFSET(modelToLoad, propertyDoors[0].vCoords  + <<0.0, 0.0, -20.0>>,FALSE,FALSE,TRUE)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "GET_ENTRY_CUTSCENE_DATA- Creating last else")
					ENDIF
					
//					IF GET_PROPERTY_BUILDING(iCurrentPropertyID) <> MP_PROPERTY_BUILDING_5
//						IF GET_PROPERTY_SIZE_TYPE(iCurrentPropertyID) != PROP_SIZE_TYPE_LARGE_APT
//							CDEBUG1LN(DEBUG_SAFEHOUSE, "propertyDoors[0].vCoords = ",propertyDoors[0].vCoords)
//							CDEBUG1LN(DEBUG_SAFEHOUSE, "entryCutData.animDictionary = ",entryCutData.animDictionary)
//							IF mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_BUILDING_17
//							OR mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_BUILDING_20
//							OR mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_BUILDING_58
//								entryCutData.objIDs[0] = CREATE_OBJECT_NO_OFFSET(Prop_ss1_mpint_door_l, propertyDoors[0].vCoords  + <<0.0, 0.0, -20.0>>,FALSE,FALSE,TRUE)
//							ELSE
//								entryCutData.objIDs[0] = CREATE_OBJECT_NO_OFFSET(Prop_ss1_mpint_door_r, propertyDoors[0].vCoords  + <<0.0, 0.0, -20.0>>,FALSE,FALSE,TRUE)
//							ENDIF
//						ELSE
//							entryCutData.objIDs[0] = CREATE_OBJECT_NO_OFFSET(propertyDoors[1].doorModel, propertyDoors[1].vCoords  + <<0.0, 0.0, -20.0>>,FALSE,FALSE,TRUE)
//						ENDIF
//						SET_BIT(entryCutData.iBS,ENTRY_CS_BS_APT_DOOR_MODEL_HIDE)
//					ELSE
//						entryCutData.objIDs[0] = CREATE_OBJECT_NO_OFFSET(propertyDoors[0].doorModel, propertyDoors[0].vCoords  + <<0.0, 0.0, -20.0>>,FALSE,FALSE,TRUE)
//					ENDIF
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "GET_ENTRY_CUTSCENE_DATA - Door either already exists or property is an independence day apartment")
				ENDIF
				
				//IF IS_PED_MALE(PLAYER_PED_ID()) 
				IF NOT IS_PLAYER_FEMALE()
					entryCutData.playerClone = CREATE_PED(PEDTYPE_CIVMALE, GET_ENTITY_MODEL(PLAYER_PED_ID()), GET_ENTITY_COORDS(PLAYER_PED_ID()) + <<0.0, 0.0, -10.0>>, GET_ENTITY_HEADING(PLAYER_PED_ID()), FALSE, FALSE)
				ELSE
					entryCutData.playerClone = CREATE_PED(PEDTYPE_CIVFEMALE, GET_ENTITY_MODEL(PLAYER_PED_ID()), GET_ENTITY_COORDS(PLAYER_PED_ID()) + <<0.0, 0.0, -10.0>>, GET_ENTITY_HEADING(PLAYER_PED_ID()), FALSE, FALSE)
				ENDIF
				CLONE_PED_TO_TARGET(PLAYER_PED_ID(), entryCutData.playerClone)
				UPDATE_MC_EMBLEM(GET_PLAYER_INDEX(), entryCutData.playerClone)
				FREEZE_ENTITY_POSITION(entryCutData.playerClone,TRUE)
				SET_ENTITY_PROOFS(entryCutData.playerClone,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "GET_ENTRY_CUTSCENE_DATA- for front door")
				entryCutData.fSyncedSceneCompleteStage = 0.6
			BREAK
		ENDSWITCH
		
		SET_BIT(entryCutData.iBS,ENTRY_CS_BS_GET_DATA)
	ENDIF
	RETURN TRUE
ENDFUNC

FUNC BOOL RUN_ENTRY_CUTSCENE(ENTRY_CUTSCENE_TYPE type, BOOL bSkipSecondCut = FALSE)
	
	IF IS_BIT_SET(iLocalBS2, LOCAL_BS2_bRunPropertySelectMenu)
//		#IF NOT FEATURE_HEIST_PLANNING
//		RETURN TRUE
//		#ENDIF
	ENDIF
	BOOL bDoorGarage 
	IF NOT IS_GAMEPLAY_CAM_RENDERING()
		HIDE_HUD_AND_RADAR_THIS_FRAME()
	ENDIF
	STRUCT_net_realty_1_scene scene
	IF Private_Get_net_realty_1_scene(GET_PROPERTY_BUILDING(iCurrentPropertyID), scene  , iEntrancePlayerIsUsing )
	ENDIF
	FLOAT fStartPhase = 0.0
	IF type = ENTRY_CUTSCENE_TYPE_FRONT_DOOR
	AND entryCutData.iStage < ENTRY_CS_STAGE_RUN_SECOND_CUT
		DO_APARTMENT_DOOR_SOUNDS()
		fStartPhase = 0.15
	ENDIF
	IF type = ENTRY_CUTSCENE_TYPE_FRONT_DOOR
	AND IS_PROPERTY_CLUBHOUSE(iCurrentPropertyID)
		fStartPhase = 0.2
	ENDIF
	IF type = ENTRY_CUTSCENE_TYPE_BUZZER
		DO_APARTMENT_BUZZER_PRESS_SOUNDS()
		IF NOT IS_PED_INJURED(entryCutData.playerClone)
			SET_PED_RESET_FLAG(entryCutData.playerClone,PRF_DisableHighHeels,TRUE)
		ENDIF
	ENDIF
	//MP_PROP_OFFSET_STRUCT playerOffset
	CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: TRANS: PROPERTY_EXT RUN_ENTRY_CUTSCENE: type #",type," iStage: ", entryCutData.iStage)
	SWITCH entryCutData.iStage
		
		CASE ENTRY_CS_STAGE_INIT_DATA
			iApartmentBuzzerSoundStage = 0
			SET_BIT(iLocalBS, LOCAL_BS_BUZZER_REQUESTED_ANIM)
			IF GET_ENTRY_CUTSCENE_DATA(type,bSkipSecondCut)
				REQUEST_ANIM_DICT(entryCutData.animDictionary)
				IF HAS_ANIM_DICT_LOADED(entryCutData.animDictionary)
					REINIT_NET_TIMER(entryCutData.timer,TRUE)
					SET_ENTRY_CS_STAGE(ENTRY_CS_STAGE_WAIT_FOR_MODELS)
				ENDIF
			ENDIF
		BREAK
		
		CASE ENTRY_CS_STAGE_WAIT_FOR_MODELS
			IF NOT HAS_NET_TIMER_EXPIRED(entryCutData.timer,5000,TRUE)
				IF DOES_ENTITY_EXIST(entryCutData.playerClone)
					IF NOT IS_PED_INJURED(entryCutData.playerClone)
						IF NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(entryCutData.playerClone)
							CDEBUG1LN(DEBUG_SAFEHOUSE, "Waiting player clone to stream" )
							RETURN FALSE
						ENDIF
						IF NOT HAS_PED_HEAD_BLEND_FINISHED(entryCutData.playerClone)
							CDEBUG1LN(DEBUG_SAFEHOUSE, "Waiting player clone to finish head blend" )
							RETURN FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			SET_ENTRY_CS_STAGE(ENTRY_CS_STAGE_HANDLE_PLAYER_PED)
		BREAK
		
		CASE ENTRY_CS_STAGE_HANDLE_PLAYER_PED
			
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_NO_COLLISION | NSPC_FREEZE_POSITION | NSPC_PREVENT_VISIBILITY_CHANGES | NSPC_SET_INVISIBLE)
			
			//SET_LOCAL_PLAYER_INVISIBLE_LOCALLY()
			NETWORK_FADE_OUT_ENTITY(PLAYER_PED_ID(), TRUE, TRUE)
			
			CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: EXT ENTRY ANIM: - fading player and mp_cutscene ")
			
			SET_BIT(entryCutData.iBS,ENTRY_CS_BS_TOOK_AWAY_CONTROL)
			SET_LOCAL_PLAYER_VISIBLE_IN_CUTSCENE(FALSE, TRUE)
			
			IF IS_PROPERTY_CLUBHOUSE(iCurrentPropertyID)
				SET_MULTIHEAD_SAFE(TRUE, TRUE)
			ENDIF
			
			START_MP_CUTSCENE(TRUE)
			SET_BIT(entryCutData.iBS,ENTRY_CS_BS_STARTED_MP_CUTSCENE)
			SET_ENTRY_CS_STAGE(ENTRY_CS_STAGE_TRIGGER_CUT)
		FALLTHRU // url:bugstar:2167563 - Character pops / disappears right before camera cut of player entering apartment building
		
		CASE ENTRY_CS_STAGE_TRIGGER_CUT
			
			//Create Camera
			IF DOES_CAM_EXIST(entryCutData.cameras[0])
				DESTROY_CAM(entryCutData.cameras[0])
			ENDIF
			
			IF type = ENTRY_CUTSCENE_TYPE_FRONT_DOOR
				IF GET_PROPERTY_BUILDING(iCurrentPropertyID) <> MP_PROPERTY_BUILDING_5
//					IF GET_PROPERTY_SIZE_TYPE(iCurrentPropertyID) = PROP_SIZE_TYPE_LARGE_APT
//					OR NOT ARE_VECTORS_EQUAL(propertyDoors[1].vCoords,<<0,0,0>>)
					IF NOT ARE_VECTORS_EQUAL(propertyDoors[1].vCoords,<<0,0,0>>)
						CREATE_MODEL_HIDE(propertyDoors[1].vCoords,1,propertyDoors[1].doorModel,TRUE)	
						CDEBUG1LN(DEBUG_SAFEHOUSE, "Creating model hide 0  at ",propertyDoors[1].vCoords, " model: ",propertyDoors[1].doorModel)
					ELSE
						CREATE_MODEL_HIDE(propertyDoors[0].vCoords,1,propertyDoors[0].doorModel,TRUE)	
						CDEBUG1LN(DEBUG_SAFEHOUSE, "Creating model hide 1  at ",propertyDoors[0].vCoords, " model: ",propertyDoors[0].doorModel)
					ENDIF
				ELSE
					CREATE_MODEL_HIDE(propertyDoors[0].vCoords,1,propertyDoors[0].doorModel,TRUE)	
					CDEBUG1LN(DEBUG_SAFEHOUSE, "Creating model hide 2  at ",propertyDoors[0].vCoords, " model: ",propertyDoors[0].doorModel)
				ENDIF
				SET_BIT(entryCutData.iBS,ENTRY_CS_BS_APT_DOOR_MODEL_HIDE)

				IF DOES_ENTITY_EXIST(entryCutData.objIDs[0])
					SET_ENTITY_COORDS_NO_OFFSET(entryCutData.objIDs[0],
												GET_ANIM_INITIAL_OFFSET_POSITION(entryCutData.animDictionary,entryCutData.animName[SYNC_SCENE_DOOR],entryCutData.vSyncSceneLoc,entryCutData.vSyncSceneRot,fStartPhase))
					SET_ENTITY_ROTATION(entryCutData.objIDs[0],
												GET_ANIM_INITIAL_OFFSET_ROTATION(entryCutData.animDictionary,entryCutData.animName[SYNC_SCENE_DOOR],entryCutData.vSyncSceneLoc,entryCutData.vSyncSceneRot,fStartPhase))
					CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: setting entity coords: here")
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: entity does not exist??")
				ENDIF
				
				
			ENDIF	
			entryCutData.cameras[0] = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
				
			SET_CAM_PARAMS(entryCutData.cameras[0],
							entryCutData.vCamLoc[0],
							entryCutData.vCamRot[0],
							entryCutData.fCamFOV[0])
			
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: TRANS: CAMERA LOC: ", entryCutData.vCamLoc[0], ", ROT: ", entryCutData.vCamRot[0])
			#ENDIF
				
			SET_CAM_FAR_CLIP(entryCutData.cameras[0],1000)
			SHAKE_CAM(entryCutData.cameras[0], "HAND_SHAKE", 0.25)

			//NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_NO_COLLISION | NSPC_FREEZE_POSITION)
			
			//NETWORK_FADE_OUT_ENTITY(PLAYER_PED_ID(), TRUE, TRUE)
			//CDEBUG1LN(DEBUG_SAFEHOUSE, "NETWORK_FADE_OUT_ENTITY: RUN_ENTRY_CUTSCENE- fading player ")
			
			//SET_BIT(entryCutData.iBS,ENTRY_CS_BS_TOOK_AWAY_CONTROL)
			//SET_LOCAL_PLAYER_VISIBLE_IN_CUTSCENE(FALSE, TRUE)
			//START_MP_CUTSCENE(TRUE)
			SET_BIT(entryCutData.iBS,ENTRY_CS_BS_STARTED_MP_CUTSCENE)

			REINIT_NET_TIMER(entryCutData.timer,TRUE)	
			
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			FREEZE_ENTITY_POSITION(entryCutData.playerClone,FALSE)
			entryCutData.iSyncedSceneID = CREATE_SYNCHRONIZED_SCENE(entryCutData.vSyncSceneLoc,entryCutData.vSyncSceneRot)
			IF NOT IS_PED_INJURED(entryCutData.playerClone)
				//VECTOR sceneRotation = << -1.725, -0.000, 2.215 >>
				
				TASK_SYNCHRONIZED_SCENE(entryCutData.playerClone, entryCutData.iSyncedSceneID, 
													entryCutData.animDictionary, entryCutData.animName[SYNC_SCENE_PED_A], 
													INSTANT_BLEND_IN,INSTANT_BLEND_OUT,SYNCED_SCENE_DONT_INTERRUPT,RBF_NONE)
			ENDIF

			IF NOT IS_BUILDING_INDEPENDENCE_DAY_APARTMENT(GET_PROPERTY_BUILDING(iCurrentPropertyID)) // don't run any door anims on the independence day no need to 
			//AND mpProperties[iCurrentPropertyID].iBuildingID != MP_PROPERTY_STILT_BUILDING_1_BASE_B		
			//AND mpProperties[iCurrentPropertyID].iBuildingID != MP_PROPERTY_STILT_BUILDING_2_B				
			//AND mpProperties[iCurrentPropertyID].iBuildingID != MP_PROPERTY_STILT_BUILDING_3_B				
			//AND mpProperties[iCurrentPropertyID].iBuildingID != MP_PROPERTY_STILT_BUILDING_4_B				
			//AND mpProperties[iCurrentPropertyID].iBuildingID != MP_PROPERTY_STILT_BUILDING_5_BASE_A					
			//AND mpProperties[iCurrentPropertyID].iBuildingID != MP_PROPERTY_STILT_BUILDING_7_A				
			//AND mpProperties[iCurrentPropertyID].iBuildingID != MP_PROPERTY_STILT_BUILDING_8_A							
			//AND mpProperties[iCurrentPropertyID].iBuildingID != MP_PROPERTY_STILT_BUILDING_10_A				
//			AND mpProperties[iCurrentPropertyID].iBuildingID != MP_PROPERTY_STILT_BUILDING_11_A	
			//AND mpProperties[iCurrentPropertyID].iBuildingID != MP_PROPERTY_STILT_BUILDING_13_A	
				IF DOES_ENTITY_EXIST(entryCutData.objIDs[0])
					//VECTOR sceneRotation = << -1.725, -0.000, 2.215 >>
					IF GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_BUILDING_6
						SET_ENTITY_ROTATION(entryCutData.objIDs[0],
													GET_ANIM_INITIAL_OFFSET_ROTATION(entryCutData.animDictionary, entryCutData.animName[SYNC_SCENE_DOOR], << -931.625, -385.063, 37.763 >>, << 0.000, 0.000, -64.000 >>, 0.0))
						SET_ENTITY_COORDS_NO_OFFSET(entryCutData.objIDs[0],
													GET_ANIM_INITIAL_OFFSET_POSITION(entryCutData.animDictionary, entryCutData.animName[SYNC_SCENE_DOOR], << -931.625, -385.063, 37.763 >>, << 0.000, 0.000, -64.000 >>, 0.0))
						PLAY_ENTITY_ANIM(entryCutData.objIDs[0],entryCutData.animName[SYNC_SCENE_DOOR],entryCutData.animDictionary,
											INSTANT_BLEND_IN,FALSE,FALSE,DEFAULT,fStartPhase,ENUM_TO_INT(AF_USE_MOVER_EXTRACTION))
					ELIF GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_BUILDING_14

						GET_BUILDING_DOOR_DETAILS(tempPropOffset,propertyDoors[0],bDoorGarage,mpProperties[iCurrentPropertyID].iBuildingID,1)
						SET_ENTITY_COORDS_NO_OFFSET(entryCutData.objIDs[0], tempPropOffset.vLoc)
						SET_ENTITY_ROTATION(entryCutData.objIDs[0], tempPropOffset.vRot)
						PLAY_ENTITY_ANIM(entryCutData.objIDs[0],
													entryCutData.animName[ENUM_TO_INT(SYNC_SCENE_DOOR)], entryCutData.animDictionary,
													INSTANT_BLEND_IN,FALSE, FALSE, DEFAULT, 0.153,ENUM_TO_INT(AF_USE_MOVER_EXTRACTION))
													
													
						CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: RUN_ENTRY_CUTSCENE: ENTRY_CS_STAGE_TRIGGER_CUT: MP_PROPERTY_BUILDING_14")		
					
					ELIF  GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_BUILDING_16

						GET_BUILDING_DOOR_DETAILS(tempPropOffset,propertyDoors[0],bDoorGarage,mpProperties[iCurrentPropertyID].iBuildingID,1)
						SET_ENTITY_COORDS_NO_OFFSET(entryCutData.objIDs[0], tempPropOffset.vLoc)
						SET_ENTITY_ROTATION(entryCutData.objIDs[0], <<0,0,-90>>)
						PLAY_ENTITY_ANIM(entryCutData.objIDs[0],
													entryCutData.animName[ENUM_TO_INT(SYNC_SCENE_DOOR)], entryCutData.animDictionary,
													INSTANT_BLEND_IN,FALSE, FALSE, DEFAULT, 0.153,ENUM_TO_INT(AF_USE_MOVER_EXTRACTION))
													
													
						CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: RUN_ENTRY_CUTSCENE: ENTRY_CS_STAGE_TRIGGER_CUT: MP_PROPERTY_BUILDING_16")
					
					ELIF  GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_BUILDING_19
					
						GET_BUILDING_DOOR_DETAILS(tempPropOffset,propertyDoors[0],bDoorGarage,mpProperties[iCurrentPropertyID].iBuildingID,1)
						SET_ENTITY_COORDS_NO_OFFSET(entryCutData.objIDs[0], tempPropOffset.vLoc)
						SET_ENTITY_ROTATION(entryCutData.objIDs[0], <<0,0, -52>>)
						PLAY_ENTITY_ANIM(entryCutData.objIDs[0],
													entryCutData.animName[ENUM_TO_INT(SYNC_SCENE_DOOR)], entryCutData.animDictionary,
													INSTANT_BLEND_IN,FALSE, FALSE, DEFAULT, 0.153,ENUM_TO_INT(AF_USE_MOVER_EXTRACTION))
													
													
						CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: RUN_ENTRY_CUTSCENE: ENTRY_CS_STAGE_TRIGGER_CUT: MP_PROPERTY_BUILDING_19")
					
					ELIF  GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_BUILDING_21
					
						GET_BUILDING_DOOR_DETAILS(tempPropOffset,propertyDoors[0],bDoorGarage,mpProperties[iCurrentPropertyID].iBuildingID,0)
						SET_ENTITY_COORDS_NO_OFFSET(entryCutData.objIDs[0], tempPropOffset.vLoc)
						SET_ENTITY_ROTATION(entryCutData.objIDs[0], tempPropOffset.vRot)
						PLAY_ENTITY_ANIM(entryCutData.objIDs[0],
													entryCutData.animName[ENUM_TO_INT(SYNC_SCENE_DOOR)], entryCutData.animDictionary,
													INSTANT_BLEND_IN,FALSE, FALSE, DEFAULT, 0.153,ENUM_TO_INT(AF_USE_MOVER_EXTRACTION))
													
													
						CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: RUN_ENTRY_CUTSCENE: ENTRY_CS_STAGE_TRIGGER_CUT: MP_PROPERTY_BUILDING_21")
						
					ELIF  GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_BUILDING_22
					
						GET_BUILDING_DOOR_DETAILS(tempPropOffset,propertyDoors[0],bDoorGarage,mpProperties[iCurrentPropertyID].iBuildingID,1)
						SET_ENTITY_COORDS_NO_OFFSET(entryCutData.objIDs[0], tempPropOffset.vLoc)
						SET_ENTITY_ROTATION(entryCutData.objIDs[0], <<0,0, 230>>)
						PLAY_ENTITY_ANIM(entryCutData.objIDs[0],
													entryCutData.animName[ENUM_TO_INT(SYNC_SCENE_DOOR)], entryCutData.animDictionary,
													INSTANT_BLEND_IN,FALSE, FALSE, DEFAULT, 0.153,ENUM_TO_INT(AF_USE_MOVER_EXTRACTION))
													
													
						CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: RUN_ENTRY_CUTSCENE: ENTRY_CS_STAGE_TRIGGER_CUT: MP_PROPERTY_BUILDING_22")
						
					ELIF  GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_BUILDING_23
					
						GET_BUILDING_DOOR_DETAILS(tempPropOffset,propertyDoors[0],bDoorGarage,mpProperties[iCurrentPropertyID].iBuildingID,1)
						SET_ENTITY_COORDS_NO_OFFSET(entryCutData.objIDs[0], tempPropOffset.vLoc)
						SET_ENTITY_ROTATION(entryCutData.objIDs[0], <<0,0,50>>)
						PLAY_ENTITY_ANIM(entryCutData.objIDs[0],
													entryCutData.animName[ENUM_TO_INT(SYNC_SCENE_DOOR)], entryCutData.animDictionary,
													INSTANT_BLEND_IN,FALSE, FALSE, DEFAULT, 0.153,ENUM_TO_INT(AF_USE_MOVER_EXTRACTION))
													
													
						CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: RUN_ENTRY_CUTSCENE: ENTRY_CS_STAGE_TRIGGER_CUT: MP_PROPERTY_BUILDING_23")
					ELIF  GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_STILT_BUILDING_12_A
						SET_ENTITY_ROTATION(entryCutData.objIDs[0],
													GET_ANIM_INITIAL_OFFSET_ROTATION(entryCutData.animDictionary, entryCutData.animName[SYNC_SCENE_DOOR], << -1295.462, 453.962, 96.359 >>, << 0.000, -0.000, -180.000 >>, 0.0))
						SET_ENTITY_COORDS_NO_OFFSET(entryCutData.objIDs[0],
													GET_ANIM_INITIAL_OFFSET_POSITION(entryCutData.animDictionary, entryCutData.animName[SYNC_SCENE_DOOR], << -1295.462, 453.962, 96.359 >>, << 0.000, -0.000, -180.000 >>, 0.0))
						PLAY_ENTITY_ANIM(entryCutData.objIDs[0],entryCutData.animName[SYNC_SCENE_DOOR],entryCutData.animDictionary,
											INSTANT_BLEND_IN,FALSE,FALSE,DEFAULT,fStartPhase,ENUM_TO_INT(AF_USE_MOVER_EXTRACTION))
					
					ELIF  GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_STILT_BUILDING_5_BASE_A
						
						GET_BUILDING_DOOR_DETAILS(tempPropOffset,propertyDoors[0],bDoorGarage,mpProperties[iCurrentPropertyID].iBuildingID,0)
						SET_ENTITY_COORDS_NO_OFFSET(entryCutData.objIDs[0], tempPropOffset.vLoc)
						SET_ENTITY_ROTATION(entryCutData.objIDs[0], tempPropOffset.vRot)
						PLAY_ENTITY_ANIM(entryCutData.objIDs[0],entryCutData.animName[SYNC_SCENE_DOOR],entryCutData.animDictionary,
											INSTANT_BLEND_IN,FALSE,FALSE,DEFAULT,0.070,ENUM_TO_INT(AF_USE_MOVER_EXTRACTION))
											
					ELIF GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_STILT_BUILDING_13_A
					OR GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_STILT_BUILDING_1_BASE_B

						GET_BUILDING_DOOR_DETAILS(tempPropOffset,propertyDoors[0],bDoorGarage,mpProperties[iCurrentPropertyID].iBuildingID,0)
						SET_ENTITY_COORDS_NO_OFFSET(entryCutData.objIDs[0], tempPropOffset.vLoc)
						SET_ENTITY_ROTATION(entryCutData.objIDs[0], tempPropOffset.vRot)
						PLAY_ENTITY_ANIM(entryCutData.objIDs[0],entryCutData.animName[SYNC_SCENE_DOOR],entryCutData.animDictionary,
											INSTANT_BLEND_IN,FALSE,FALSE,DEFAULT,fStartPhase,ENUM_TO_INT(AF_USE_MOVER_EXTRACTION))						
					
					ELIF  GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_STILT_BUILDING_3_B
						
						SET_ENTITY_COORDS_NO_OFFSET(entryCutData.objIDs[0],  <<-753.498,619.340,143.2394>>)
						SET_ENTITY_ROTATION(entryCutData.objIDs[0], <<180,0, -71.5>>)
						PLAY_ENTITY_ANIM(entryCutData.objIDs[0],entryCutData.animName[SYNC_SCENE_DOOR],entryCutData.animDictionary,
											INSTANT_BLEND_IN,FALSE,FALSE,DEFAULT,fStartPhase,ENUM_TO_INT(AF_USE_MOVER_EXTRACTION))	
				
					ELSE
						PLAY_SYNCHRONIZED_ENTITY_ANIM(entryCutData.objIDs[0],entryCutData.iSyncedSceneID,
																		entryCutData.animName[SYNC_SCENE_DOOR],entryCutData.animDictionary,
																		INSTANT_BLEND_IN,INSTANT_BLEND_OUT)
					ENDIF
				ENDIF
			ENDIF
			SET_SYNCHRONIZED_SCENE_PHASE(entryCutData.iSyncedSceneID,fStartPhase)
			
			SET_ENTRY_CS_STAGE(ENTRY_CS_STAGE_RUN_CUT)
		BREAK
		CASE ENTRY_CS_STAGE_RUN_CUT
			IF HAS_NET_TIMER_EXPIRED(entryCutData.timer,5000,TRUE)
				IF IS_BIT_SET(entryCutData.iBS,ENTRY_CS_BS_SECOND_CUT)
					REINIT_NET_TIMER(iCameraTimer,TRUE)
					REINIT_NET_TIMER(iWalkInTimer,TRUE)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "ENTRY_CS_STAGE_RUN_CUT timer expired")
					SceneTool_ExecutePan(scene.mPans[net_realty_1_scene_PAN_establishing], propertyCam0, propertyCam1)
					IF NOT IS_PED_INJURED(entryCutData.playerClone)
					AND type = ENTRY_CUTSCENE_TYPE_FRONT_DOOR
						SET_ENTITY_VISIBLE(entryCutData.playerClone,FALSE)
					ENDIF
					IF IS_PROPERTY_OFFICE(iCurrentPropertyID)
						IF iEntrancePlayerIsUsing = 0
							START_AUDIO_SCENE("EXEC1_Enter_Office_From_Ground_Scene")
							PRINTLN("START_AUDIO_SCENE - EXEC1_Enter_Office_From_Ground_Scene")
						ELSE
							START_AUDIO_SCENE("EXEC1_Enter_Office_From_Roof_Scene")
							PRINTLN("START_AUDIO_SCENE - EXEC1_Enter_Office_From_Roof_Scene")
						ENDIF
					ENDIF
					
					SET_ENTRY_CS_STAGE(ENTRY_CS_STAGE_RUN_SECOND_CUT)
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "ENTRY_CS_STAGE_RUN_CUT timer has not expired")
					RETURN TRUE
				ENDIF
			ENDIF
			IF entryCutData.iSyncedSceneID >= 0
				IF IS_SYNCHRONIZED_SCENE_RUNNING(entryCutData.iSyncedSceneID)
					IF GET_SYNCHRONIZED_SCENE_PHASE(entryCutData.iSyncedSceneID) > entryCutData.fSyncedSceneCompleteStage
						IF IS_BIT_SET(entryCutData.iBS,ENTRY_CS_BS_SECOND_CUT)
							CDEBUG1LN(DEBUG_SAFEHOUSE, "ENTRY_CS_STAGE_RUN_CUT scene is complete")
							REINIT_NET_TIMER(iCameraTimer,TRUE)
							REINIT_NET_TIMER(iWalkInTimer,TRUE)
							SceneTool_ExecutePan(scene.mPans[net_realty_1_scene_PAN_establishing], propertyCam0, propertyCam1)
							IF NOT IS_PED_INJURED(entryCutData.playerClone)
							AND type = ENTRY_CUTSCENE_TYPE_FRONT_DOOR
								SET_ENTITY_VISIBLE(entryCutData.playerClone,FALSE)
							ENDIF
							
							// url:bugstar:2195535
							IF type = ENTRY_CUTSCENE_TYPE_FRONT_DOOR
							AND DOES_ENTITY_EXIST(entryCutData.objIDs[0])
								SET_ENTITY_VISIBLE(entryCutData.objIDs[0], FALSE)
								IF GET_PROPERTY_BUILDING(iCurrentPropertyID) <> MP_PROPERTY_BUILDING_5
									//IF GET_PROPERTY_SIZE_TYPE(iCurrentPropertyID) = PROP_SIZE_TYPE_LARGE_APT
									IF NOT ARE_VECTORS_EQUAL(propertyDoors[1].vCoords,<<0,0,0>>)
										REMOVE_MODEL_HIDE(propertyDoors[1].vCoords,1,propertyDoors[1].doorModel)
										CDEBUG1LN(DEBUG_SAFEHOUSE, "REMOVING model hide 0  at ",propertyDoors[1].vCoords, " model: ",propertyDoors[1].doorModel)
									ELSE
										REMOVE_MODEL_HIDE(propertyDoors[0].vCoords,1,propertyDoors[0].doorModel)	
										CDEBUG1LN(DEBUG_SAFEHOUSE, "REMOVING  model hide 1  at ",propertyDoors[0].vCoords, " model: ",propertyDoors[0].doorModel)
									ENDIF
								ELSE
									REMOVE_MODEL_HIDE(propertyDoors[0].vCoords,1,propertyDoors[0].doorModel)	
									CDEBUG1LN(DEBUG_SAFEHOUSE, "REMOVING  model hide 2  at ",propertyDoors[0].vCoords, " model: ",propertyDoors[0].doorModel)
								ENDIF
								CLEAR_BIT(entryCutData.iBS,ENTRY_CS_BS_APT_DOOR_MODEL_HIDE)
							ENDIF
							IF IS_PROPERTY_OFFICE(iCurrentPropertyID)
								IF iEntrancePlayerIsUsing = 0
									START_AUDIO_SCENE("EXEC1_Enter_Office_From_Ground_Scene")
									PRINTLN("START_AUDIO_SCENE - EXEC1_Enter_Office_From_Ground_Scene")
								ELSE
									START_AUDIO_SCENE("EXEC1_Enter_Office_From_Roof_Scene")
									PRINTLN("START_AUDIO_SCENE - EXEC1_Enter_Office_From_Roof_Scene")
								ENDIF
							ENDIF
							SET_ENTRY_CS_STAGE(ENTRY_CS_STAGE_RUN_SECOND_CUT)
						ELSE
							IF type = ENTRY_CUTSCENE_TYPE_FRONT_DOOR
							AND DOES_ENTITY_EXIST(entryCutData.objIDs[0])
								SET_ENTITY_VISIBLE(entryCutData.objIDs[0], FALSE)
								
								IF GET_PROPERTY_BUILDING(iCurrentPropertyID) <> MP_PROPERTY_BUILDING_5
									//IF GET_PROPERTY_SIZE_TYPE(iCurrentPropertyID) = PROP_SIZE_TYPE_LARGE_APT
									IF NOT ARE_VECTORS_EQUAL(propertyDoors[1].vCoords,<<0,0,0>>)
										REMOVE_MODEL_HIDE(propertyDoors[1].vCoords,1,propertyDoors[1].doorModel)
										CDEBUG1LN(DEBUG_SAFEHOUSE, "REMOVING model hide 3  at ",propertyDoors[1].vCoords, " model: ",propertyDoors[1].doorModel)
									ELSE
										REMOVE_MODEL_HIDE(propertyDoors[0].vCoords,1,propertyDoors[0].doorModel)	
										CDEBUG1LN(DEBUG_SAFEHOUSE, "REMOVING  model hide 4  at ",propertyDoors[0].vCoords, " model: ",propertyDoors[0].doorModel)
									ENDIF
								ELSE
									REMOVE_MODEL_HIDE(propertyDoors[0].vCoords,1,propertyDoors[0].doorModel)	
									CDEBUG1LN(DEBUG_SAFEHOUSE, "REMOVING  model hide 5  at ",propertyDoors[0].vCoords, " model: ",propertyDoors[0].doorModel)
								ENDIF
								CLEAR_BIT(entryCutData.iBS,ENTRY_CS_BS_APT_DOOR_MODEL_HIDE)
							ENDIF
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF IS_BIT_SET(entryCutData.iBS,ENTRY_CS_BS_SECOND_CUT)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "ENTRY_CS_STAGE_RUN_CUT scene is <= 0")
					REINIT_NET_TIMER(iCameraTimer,TRUE)
					REINIT_NET_TIMER(iWalkInTimer,TRUE)
					SceneTool_ExecutePan(scene.mPans[net_realty_1_scene_PAN_establishing], propertyCam0, propertyCam1)
					IF NOT IS_PED_INJURED(entryCutData.playerClone)
					AND type = ENTRY_CUTSCENE_TYPE_FRONT_DOOR
						SET_ENTITY_VISIBLE(entryCutData.playerClone,FALSE)
					ENDIF
					IF IS_PROPERTY_OFFICE(iCurrentPropertyID)
						IF iEntrancePlayerIsUsing = 0
							START_AUDIO_SCENE("EXEC1_Enter_Office_From_Ground_Scene")
							PRINTLN("START_AUDIO_SCENE - EXEC1_Enter_Office_From_Ground_Scene")
						ELSE
							START_AUDIO_SCENE("EXEC1_Enter_Office_From_Roof_Scene")
							PRINTLN("START_AUDIO_SCENE - EXEC1_Enter_Office_From_Roof_Scene")
						ENDIF
					ENDIF
					SET_ENTRY_CS_STAGE(ENTRY_CS_STAGE_RUN_SECOND_CUT)
				ELSE
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		CASE ENTRY_CS_STAGE_RUN_SECOND_CUT
			IF HAS_NET_TIMER_EXPIRED(iCameraTimer, ROUND((scene.mPans[net_realty_1_scene_PAN_establishing].fDuration + scene.fExitDelay) * 1000.0),TRUE)
				IF entryCutData.iSoundID >= 0
					STOP_SOUND(entryCutData.iSoundID)
				ENDIF
				RETURN TRUE
			ELSE
				IF type = ENTRY_CUTSCENE_TYPE_FRONT_DOOR
					DO_APARTMENT_DOOR_SOUNDS()
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_PLAYER_FINISHED_BUZZ_ANIM()
	
	IF IS_BIT_SET(iLocalBS2, LOCAL_BS2_bRunPropertySelectMenu)
//		#IF NOT FEATURE_HEIST_PLANNING
//		RETURN TRUE
//		#ENDIF
		IF mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iType = ENTRANCE_TYPE_GARAGE
			RETURN TRUE
		ENDIF
	ENDIF
	
	
	IF bUseNewEntryAnims
		IF DOES_ANYONE_IN_GAME_OWN_PROPERTY_IN_BUILDING(iKnowOwnerState,mpProperties[iCurrentPropertyID].iBuildingID)
		AND NOT IS_BIT_SET(iLocalBS2, LOCAL_BS2_bRunPropertySelectMenu)
		#IF IS_DEBUG_BUILD
		OR bDebugTestBuzzer 
		#ENDIF
			GET_ENTRY_CUTSCENE_DATA(ENTRY_CUTSCENE_TYPE_BUZZER)
			IF NOT RUN_ENTRY_CUTSCENE(ENTRY_CUTSCENE_TYPE_BUZZER)
				SET_BIT(iLocalBS2,LOCAL_BS2_bWalkInTriggered)
				RETURN FALSE
			ELSE
				RETURN TRUE
			ENDIF
		ELSE
			GET_ENTRY_CUTSCENE_DATA(ENTRY_CUTSCENE_TYPE_FRONT_DOOR,TRUE)
			IF NOT RUN_ENTRY_CUTSCENE(ENTRY_CUTSCENE_TYPE_FRONT_DOOR,TRUE)
				SET_BIT(iLocalBS2,LOCAL_BS2_bWalkInTriggered)
				RETURN FALSE
			ELSE
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	SET_BIT(iLocalBS, LOCAL_BS_BUZZER_REQUESTED_ANIM)
	//TASK_LOOK_AT_COORD(PLAYER_PED_ID(), oMachine, 2000, SLF_WHILE_NOT_IN_FOV)
	
	REQUEST_ANIM_DICT("mp_doorbell")
	IF HAS_ANIM_DICT_LOADED("mp_doorbell")									
		IF NOT IS_BIT_SET(iLocalBS, LOCAL_BS_BUZZER_GIVE_GOTO_TASK)	
			tempPropOffset.vLoc = <<0,0,0>>
			tempPropOffset.vRot = <<0,0,0>>
			GET_BUZZER_SYNCED_SCENE_OFFSET(mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].vBuzzerProp,
									mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].vBuzzerPropRot, 
									tempPropOffset,mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iBuzzerType)
			
			IF fGroundHeightWalk[iEntrancePlayerIsUsing] = 0.0
				GET_GROUND_Z_FOR_3D_COORD(tempPropOffset.vLoc + <<0.0, 0.0, 0.5>>, fGroundHeightWalk[iEntrancePlayerIsUsing])
			ENDIF
			
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Telling player to slide to position ", <<tempPropOffset.vLoc.X, tempPropOffset.vLoc.Y, fGroundHeightWalk[iEntrancePlayerIsUsing]>>)
			TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(),<<tempPropOffset.vLoc.X, tempPropOffset.vLoc.Y, fGroundHeightWalk[iEntrancePlayerIsUsing]>>,
										PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP,tempPropOffset.vRot.z, 0.1)
			
			SET_BIT(iLocalBS, LOCAL_BS_BUZZER_GIVE_GOTO_TASK)	
			REINIT_NET_TIMER(walkToBuzzerTimer,TRUE)
		ELSE
			IF NOT IS_BIT_SET(iLocalBS, LOCAL_BS_BUZZER_GIVE_RING_TASK)
				tempPropOffset.vLoc = <<0,0,0>>
				tempPropOffset.vRot = <<0,0,0>>
				GET_BUZZER_SYNCED_SCENE_OFFSET(mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].vBuzzerProp ,
										mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].vBuzzerPropRot, 
										tempPropOffset,mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iBuzzerType)
				
				IF fGroundHeightWalk[iEntrancePlayerIsUsing] = 0.0
					GET_GROUND_Z_FOR_3D_COORD(tempPropOffset.vLoc + <<0.0, 0.0, 0.5>>, fGroundHeightWalk[iEntrancePlayerIsUsing])
				ENDIF
				
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), tempPropOffset.vLoc, <<0.2, 0.2, 3.0>>)
			      	//sceneShutterSwitch = CREATE_SYNCHRONIZED_SCENE(sceneDuckUnderPos - <<0.0, 0.41, 0.0>> + <<-0.18, -0.06, 0.0>>, sceneDuckUnderRot)
			     	//TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), sceneShutterSwitch, sAnimDictPrologue5_Duck, "press_button_player2", WALK_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_NONE, WALK_BLEND_IN)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "NETWORK_CREATE_SYNCHRONISED_SCENE: at coords: ",<<tempPropOffset.vLoc.X, tempPropOffset.vLoc.Y, fGroundHeightWalk[iEntrancePlayerIsUsing]>>," with rotation:", tempPropOffset.vRot)
					iBuzzerPressSyncedScene = NETWORK_CREATE_SYNCHRONISED_SCENE(<<tempPropOffset.vLoc.X, tempPropOffset.vLoc.Y, fGroundHeightWalk[iEntrancePlayerIsUsing]>>, 
																				tempPropOffset.vRot, EULER_YXZ)
					
					NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iBuzzerPressSyncedScene, 
																	"mp_doorbell", "ring_bell_a", WALK_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS, 
																	RBF_NONE, WALK_BLEND_IN)										
					
					NETWORK_START_SYNCHRONISED_SCENE(iBuzzerPressSyncedScene)
					
					iBuzzerPressSyncedSceneTimer = -1
					
					iBuzzerWalkAwayShapeTest = 0
					CLEAR_BIT(iLocalBS2,LOCAL_BS2_ShapeTestRunning)
					
					SET_BIT(iLocalBS, LOCAL_BS_BUZZER_GIVE_RING_TASK)
					RESET_NET_TIMER(walkToBuzzerTimer)
				ELSE
					IF NOT HAS_NET_TIMER_STARTED(walkToBuzzerTimer)
						REINIT_NET_TIMER(walkToBuzzerTimer,TRUE)
					ELSE
						IF HAS_NET_TIMER_EXPIRED(walkToBuzzerTimer,20000,TRUE)
							SET_ENTITY_COORDS(PLAYER_PED_ID(),<<tempPropOffset.vLoc.X, tempPropOffset.vLoc.Y, fGroundHeightWalk[iEntrancePlayerIsUsing]>>)
							SET_ENTITY_HEADING(PLAYER_PED_ID(),tempPropOffset.vRot.z)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				INT iLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iBuzzerPressSyncedScene)
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSceneID)
					IF iBuzzerPressSyncedSceneTimer = -1
						iBuzzerPressSyncedSceneTimer = GET_GAME_TIMER() + ROUND((2.433333 * 1000) + 1000 / NORMAL_BLEND_OUT)
					ENDIF
					
					IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalSceneID) > 0.3
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC



//FUNC BOOL HAS_PLAYER_FINISHED_DOOR_ENTER_ANIM(INT iDoor)
//	//TASK_LOOK_AT_COORD(PLAYER_PED_ID(), oMachine, 2000, SLF_WHILE_NOT_IN_FOV)
//	SET_BIT(iLocalBS, LOCAL_BS_BUZZER_REQUESTED_ANIM)
//	REQUEST_ANIM_DICT("mp_doorbell")
//	IF HAS_ANIM_DICT_LOADED("mp_doorbell")	
//		VECTOR vWalkToLoc
//		VECTOR vWalkToRot
//		vWalkToLoc = GET_ANIM_INITIAL_OFFSET_POSITION("mp_doorbell", "player_enter_r_peda",buildingDoorDetails[iDoor].vLoc,buildingDoorDetails[iDoor].vRot,0)
//		vWalkToRot = GET_ANIM_INITIAL_OFFSET_ROTATION("mp_doorbell", "player_enter_r_peda",buildingDoorDetails[iDoor].vLoc,buildingDoorDetails[iDoor].vRot,0)
//		IF NOT IS_BIT_SET(iLocalBS, LOCAL_BS_BUZZER_GIVE_GOTO_TASK)	
//			
//			TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(),vWalkToLoc,
//							PEDMOVE_WALK,DEFAULT_TIME_BEFORE_WARP,0.1,ENAV_DEFAULT, vWalkToRot.z )
//			NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_CAN_BE_TARGETTED | NSPC_ALLOW_PLAYER_DAMAGE | NSPC_LEAVE_CAMERA_CONTROL_ON)	
//			SET_BIT(iLocalBS, LOCAL_BS_BUZZER_GIVE_GOTO_TASK)	
//		ELSE
//			IF NOT IS_BIT_SET(iLocalBS, LOCAL_BS_BUZZER_GIVE_RING_TASK)		
//				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.normalDoorIDs[0])
//					IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),  vWalkToLoc, <<0.2, 0.2, 3.0>>)
//				      	CDEBUG1LN(DEBUG_SAFEHOUSE, "NETWORK_CREATE_SYNCHRONISED_SCENE: at coords: ",buildingDoorDetails[0].vLoc ," with rotation:", buildingDoorDetails[0].vRot)
//						iEnterFrontDoorSyncedScene = NETWORK_CREATE_SYNCHRONISED_SCENE(buildingDoorDetails[0].vLoc , 
//																					buildingDoorDetails[0].vRot, EULER_YXZ)
//						
//						NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(),iEnterFrontDoorSyncedScene, 
//																		"mp_doorbell", "player_enter_r_peda", WALK_BLEND_IN, WALK_BLEND_OUT,SYNCED_SCENE_DONT_INTERRUPT|SYNCED_SCENE_USE_PHYSICS, 
//																		RBF_NONE,WALK_BLEND_IN)	
//						NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_OBJ(serverBD.normalDoorIDs[0]),iEnterFrontDoorSyncedScene, 
//																		"mp_doorbell", "player_enter_r_door", INSTANT_BLEND_IN, INSTANT_BLEND_OUT,SYNCED_SCENE_DONT_INTERRUPT|SYNCED_SCENE_USE_PHYSICS)	
//						NETWORK_START_SYNCHRONISED_SCENE(iEnterFrontDoorSyncedScene)
//						SET_BIT(iLocalBS, LOCAL_BS_BUZZER_GIVE_RING_TASK)
//					ENDIF
//				ELSE
//					NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.normalDoorIDs[0])
//				ENDIF
//			ELSE
//				INT iLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iEnterFrontDoorSyncedScene)
//				IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSceneID)
//					IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalSceneID) > 0.8
//						RETURN TRUE
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
//	RETURN FALSE
//ENDFUNC

PROC CLEANUP_BODYGUARD()
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niGarageAttendant)
		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niGarageAttendant)
		AND NETWORK_GET_NUM_PARTICIPANTS() = 1
			CDEBUG1LN(DEBUG_SAFEHOUSE, "CLEANUP_BODYGUARD: deleting niGarageAttendant")
			
			DELETE_NET_ID(serverBD.niGarageAttendant)
		ENDIF
	ENDIF
ENDPROC


FUNC INT GET_FREE_PROPERTY_ENTRANCE_SLOT()
	INT i	
	
	REPEAT MAX_ACTIVE_PROPERTY_ENTRANCES i
		IF ARE_VECTORS_EQUAL(mpPropMaintain.vMyPropertyEntrances[i], << 0.0, 0.0, 0.0>>)
			RETURN i		
		ENDIF
	ENDREPEAT
	
	RETURN -1
ENDFUNC

PROC STORE_THIS_PROPERTY_ENTRANCE_LOCATIONS()
	INT i
	INT iSlot
	VECTOR vEntrance
	
	IF (!bStoredEntranceLocations)
		IF iCurrentPropertyID > -1
			INT iNumEntrances = mpProperties[iCurrentPropertyID].iNumEntrances
			IF iNumEntrances > 0
				REPEAT iNumEntrances i	
					vEntrance =  GET_PROPERTY_ENTRANCE_APPROXIMATE_LOCATION(iCurrentPropertyID, i)
					IF NOT ARE_VECTORS_EQUAL(vEntrance, << 0.0, 0.0, 0.0>>)
						iSlot = GET_FREE_PROPERTY_ENTRANCE_SLOT()
						IF iSlot > -1
							mpPropMaintain.vMyPropertyEntrances[iSlot] = vEntrance
							CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: [STORE_PROPERTY_ENTRANCE_LOCATIONS] Set mpPropMaintain.vMyPropertyEntrances[", iSlot, "] = ", vEntrance, " (Property = ", iCurrentPropertyID, " entrance = ", i, ")")  
						ELSE
							CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: [STORE_PROPERTY_ENTRANCE_LOCATIONS] iSlot = -1!") 
						ENDIF 
					ELSE
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: [STORE_PROPERTY_ENTRANCE_LOCATIONS] Entrance loc is 0! iCurrentPropertyID = ", iCurrentPropertyID, " Entrance = ", i)
					ENDIF
				ENDREPEAT
				bStoredEntranceLocations = TRUE
			ELSE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: [STORE_PROPERTY_ENTRANCE_LOCATIONS] Not storing entarnce coords as # entrances = ", iNumEntrances) 
			ENDIF
		ELSE
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: [STORE_PROPERTY_ENTRANCE_LOCATIONS] iCurrentPropertyID < -1! iCurrentPropertyID = ", iCurrentPropertyID)
		ENDIF
	ENDIF
ENDPROC

PROC CLEAR_PROPERTY_ENTRANCE_LOCATION(VECTOR vLoc)
	CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: [CLEAR_PROPERTY_ENTRANCE_LOCATION] Called with vLoc = ", vLoc)
	INT i	
		
	REPEAT MAX_ACTIVE_PROPERTY_ENTRANCES i
		IF ARE_VECTORS_EQUAL(vLoc, mpPropMaintain.vMyPropertyEntrances[i])
			mpPropMaintain.vMyPropertyEntrances[i] = << 0.0, 0.0, 0.0 >>
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: [CLEAR_PROPERTY_ENTRANCE_LOCATION] Found location ", vLoc, " at array pos'n ", i)
		ENDIF
	ENDREPEAT
	
	
ENDPROC

PROC CLEAR_THIS_PROPERTY_ENTRANCE_LOCATIONS()
	INT i
	VECTOR vEntrance
	IF bStoredEntranceLocations
		IF iCurrentPropertyID > -1
			INT iNumEntrances = mpProperties[iCurrentPropertyID].iNumEntrances
			IF iNumEntrances > 0
				REPEAT iNumEntrances i	
					vEntrance =  GET_PROPERTY_ENTRANCE_APPROXIMATE_LOCATION(iCurrentPropertyID, i)
					CLEAR_PROPERTY_ENTRANCE_LOCATION(vEntrance)
				ENDREPEAT
			ENDIF
		ENDIF
	ENDIF 
ENDPROC
 // FEATURE_GUNRUNNING

PROC CLEANUP_SCRIPT()
	CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT cleanup script called for building #", mpProperties[iCurrentPropertyID].iBuildingID)
	
	CLEAR_THIS_PROPERTY_ENTRANCE_LOCATIONS()
	
	CLEANUP_BODYGUARD()
	
	MOVE_OFFICE_GARAGE_BLIP(TRUE)
	
	IF (bInteriorPinned)
		IF IS_VALID_INTERIOR(officeGarageDoorInterior)
		AND IS_INTERIOR_READY(officeGarageDoorInterior)
			UNPIN_INTERIOR(officeGarageDoorInterior)	
		ENDIF
		bInteriorPinned = FALSE
	ENDIF
	
	IF (bLoadedElevatorSounds)
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_IMPORTEXPORT/GARAGE_ELEVATOR")
	ENDIF
	
	INT i
	REPEAT 2 i
		IF DOES_ENTITY_EXIST(officeGarageDoors[i])
			DELETE_OBJECT(officeGarageDoors[i])
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: deleting officeGarageDoors ", i)
		ENDIF
	ENDREPEAT	
	IF DOES_ENTITY_EXIST(officeGarageExternalDoor)
		DELETE_OBJECT(officeGarageExternalDoor)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: deleting officeGarageDoors ", i)
	ENDIF
	
	
	IF IS_BIT_SET(iLocalBS, LOCAL_BS_FORCE_TERMINATED_INTERNET)
		MP_FORCE_TERMINATE_INTERNET_CLEAR()
		CLEAR_BIT(iLocalBS, LOCAL_BS_FORCE_TERMINATED_INTERNET)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Force terminate internet cleared - 2")
	ENDIF
	IF iPackageDropPropID = iCurrentPropertyID
		//SET_LOCAL_PLAYER_CAN_COLLECT_PORTABLE_PICKUPS(TRUE)
		SET_MAX_NUM_PORTABLE_PICKUPS_CARRIED_BY_PLAYER(DUMMY_MODEL_FOR_SCRIPT,-1)
		MPGlobalsAmbience.g_bPropertyReset = TRUE
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[RCC MISSION] - EXT CLEANUP PROPERTY SO ALLOWING PICKUP: ",iCurrentPropertyID)
	ENDIF
	
	IF DOES_BLIP_EXIST(forSaleBlip)
		REMOVE_BLIP(forSaleBlip)
	ENDIF
	IF IS_BIT_SET(iLocalBS2,LOCAL_BS2_SetEntryData)
		CLEAR_FOCUS()
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: CLEAR_FOCUS() - 1")
	ENDIF
	RESET_TO_ENTRANCE_TO_PROPERTY_STAGE()
	CLEAR_PLAYER_BROADCAST_PLAYING_PROPERTY_TRANSITION_CUTSCENE()
	
	DELETE_CLONED_ENTITIES_AND_CLEAR_REQUESTS(99)
	
	BOOL bDriver
	IF bCleanupFakeGarageEnter
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: bCleanupFakeGarageEnter = TRUE - cleaning up MP cutscene")
		//this is killing interior load scene NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)  
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
		//CONCEAL_ALL_OTHER_PLAYERS(FALSE)
		//NETWORK_SET_IN_MP_CUTSCENE(FALSE, FALSE)
		//MAKE_PLAYER_SAFE_FOR_MP_CUTSCENE(FALSE, FALSE, TRUE, FALSE)
		
		IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
			VEHICLE_INDEX tempVeh
			tempVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			IF GET_PED_IN_VEHICLE_SEAT(tempVeh) = PLAYER_PED_ID()
				bDriver = TRUE
			ENDIF
		ENDIF
		IF HAS_PURCHASE_FIRST_GARAGE_CUTSCENE_BEEN_DONE() // Dave W. DOn't call CLEANUP_MP_CUTSCENE() if the intro to garages cutscene is running / about to run
		OR NOT bDriver
			CLEANUP_MP_CUTSCENE(TRUE,FALSE)
		ENDIF
		
		IF NOT IS_PED_IN_ANY_VEHICLE_OR_WAITING_TO_START_TASK_ENTER_VEHICLE(PLAYER_PED_ID())
			SET_ENTITY_COLLISION(PLAYER_PED_ID(), TRUE)	
		ENDIF
		bCleanupFakeGarageEnter = FALSE
	ENDIF
	IF IS_NET_PLAYER_OK(PLAYER_ID(),FALSE,FALSE)
		
		playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS = 0
		IF mpProperties[iCurrentPropertyID].iBuildingID = mpProperties[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iEnteringPropertyID].iBuildingID
			CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_WARPING_INTO_GARAGE_CS)
			CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_WARPING_WITH_OWNER_IN_CAR)
			CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_WARPING_INTO_GARAGE_FADE)
			CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_REPLACING_CAR)
			CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_USING_EXT)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "PROPERTY_BROADCAST_BS_PLAYER_USING_EXT- CLEARED-2")
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iEnteringPropertyID = 0
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.EnteringPropertyOwner = INVALID_PLAYER_INDEX()
		ENDIF
	ENDIF
	
	IF iBuzzerContextIntention != NEW_CONTEXT_INTENTION
		RELEASE_CONTEXT_INTENTION(iBuzzerContextIntention)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Releasing the context intention on script cleanup")
	ENDIF
	
	IF mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_BUILDING_6
		REMOVE_MODEL_HIDE(<<-935.040,-378.360,39.180>>,2,PROP_QL_REVOLVING_DOOR,TRUE)
	ENDIF
	IF mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_BUILDING_38
		REMOVE_MODEL_HIDE(<<2494.2651,1587.5674,31.7398>> , 0.02, PROP_FNCLINK_02GATE3)	//gates
		REMOVE_MODEL_HIDE(<<2484.0889,1566.9988,31.7453>> , 0.02, PROP_FNCLINK_02GATE3)	//gates
	ENDIF
	
//	#IF FEATURE_BIKER
//	IF iCurrentPropertyID = PROPERTY_CLUBHOUSE_1_BASE_A
//		REMOVE_MODEL_HIDE(<<247.6964, -1803.0883, 26.1131>>, 10, PROP_FNCLINK_03GATE2)
//	ENDIF
//	#ENDIF
	
//	IF mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_BUILDING_57
//		//REMOVE_MODEL_HIDE(<<-1494.7855,514.3803,117.7965>> , 1, PROP_FNCRES_02C)	//gates
//		REMOVE_MODEL_HIDE(<<-1477.9321,525.6279,117.9921>> , 5, PROP_LRGGATE_01_R)	//gates
//		REMOVE_MODEL_HIDE(<<-1488.1262,520.6134,118.0117>> , 5, PROP_LRGGATE_01_L)	//gates
//	ENDIF
	
	IF IS_BIT_SET(iLocalBS2,LOCAL_BS2_bFadeOutWarpIntoGarage)
		IF IS_BIT_SET(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_CAR_MADE_INVISIBLE_ON_EXTERIOR)
			//IF NOT IS_PLAYER_VISIBLE_TO_SCRIPT(PLAYER_ID())	
			//ENDIF
			//NETWORK_SET_LOCAL_PLAYER_INVINCIBLE_TIME(0)
			IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
				VEHICLE_INDEX vehFadeOut
				vehFadeOut = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				IF GET_PED_IN_VEHICLE_SEAT(vehFadeOut) = PLAYER_PED_ID()
					
					//NETWORK_FADE_IN_ENTITY(vehFadeOut, TRUE)
					SET_ENTITY_VISIBLE(vehFadeOut,TRUE)
					SET_ENTITY_VISIBLE(PLAYER_PED_ID(),TRUE)
					//SET_BIT(iLocalBS2,LOCAL_BS2_bFadeOutWarpIntoGarage)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "NETWORK_FADE_IN_ENTITY called from exterior script- VEH")
					CLEAR_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_CAR_MADE_INVISIBLE_ON_EXTERIOR)
				ENDIF
			ELSE
				//FADE_OUT_LOCAL_PLAYER(FALSE)
				//NETWORK_FADE_IN_ENTITY(PLAYER_PED_ID(), TRUE)
				SET_ENTITY_VISIBLE(PLAYER_PED_ID(),TRUE)
				NETWORK_SET_LOCAL_PLAYER_INVINCIBLE_TIME(0)
				CLEAR_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_CAR_MADE_INVISIBLE_ON_EXTERIOR)
				//SET_BIT(iLocalBS2,LOCAL_BS2_bFadeOutWarpIntoGarage)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "NETWORK_FADE_IN_ENTITY called from exterior script - PED")
			ENDIF
			FORCE_BACK_INTO_POSITION_ABORTED_ENTRY(2)
			CLEAR_BIT(iLocalBS2,LOCAL_BS2_bFadeOutWarpIntoGarage)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iLocalBS,LOCAL_BS_PURCHASED_CUSTOM_APT)
		CLEAR_BIT(iLocalBS,LOCAL_BS_PURCHASED_CUSTOM_APT)
	ENDIF

	
	DELETE_OBJECT(forSaleSign)
	bPersonalVehicleCleanupDontLeaveVehicle = FALSE
	CDEBUG1LN(DEBUG_SAFEHOUSE, "bPersonalVehicleCleanupDontLeaveVehicle = FALSE - B")
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
	
ENDPROC

//FUNC BOOL DOES_SERVER_FOR_SALE_SIGN_EXIST()
//	IF forSaleDetails.locate.fWidth = 0
//		RETURN TRUE
//	ENDIF
//	
//	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.forSaleSign)
//		RETURN TRUE
//	ENDIF
//	RETURN FALSE
//ENDFUNC

FUNC BOOL DOES_SERVER_DOOR_EXIST(INT iDoor)
	IF iDoor = 0
		RETURN TRUE
	ENDIF
	IF ARE_VECTORS_EQUAL(propertyDoors[iDoor].vCoords, <<0,0,0>>)
		RETURN TRUE
	ENDIF
	//IF bVisible
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.normalDoorIDs[iDoor])
			RETURN TRUE
		ENDIF
	//ELSE
//		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.invisibleDoorIDs[iDoor])
//			RETURN TRUE
//		ENDIF
	//ENDIF
	RETURN FALSE
ENDFUNC

//#IF NOT FEATURE_HEIST_PLANNING
//FUNC BOOL DOES_LOCAL_GARAGE_BLOCKING_OBJECT_EXIST()
//	IF ARE_VECTORS_EQUAL(blockObjectDetails.vGarageLoc, <<0,0,0>>)
//		RETURN TRUE
//	ENDIF
//
//	IF DOES_ENTITY_EXIST(garageBlockingObj)
//		RETURN TRUE
//	ENDIF
//
//	RETURN FALSE
//ENDFUNC
//
//FUNC BOOL DOES_LOCAL_ENTRY_BLOCKING_OBJECT_EXIST()
//	IF ARE_VECTORS_EQUAL(blockObjectDetails.vEntryLoc, <<0,0,0>>)
//		RETURN TRUE
//	ENDIF
//
//	IF DOES_ENTITY_EXIST(entryBlockingObj)
//		RETURN TRUE
//	ENDIF
//
//	RETURN FALSE
//ENDFUNC
//#ENDIF


PROC GET_MAP_HOLE_PROTECTOR_DATA(MP_PROP_OFFSET_STRUCT &positionData,INT iIndex)
	IF mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_BUILDING_9	
		SWITCH iIndex
			CASE 0 //interior left wall
				positionData.vLoc = <<7.941,40.869,70.586>>	
				positionData.vRot = <<0,0,71.30>>
			BREAK
			
			CASE 1 //interior right wall
				positionData.vLoc = <<2.611,42.849,70.586>>	
				positionData.vRot = <<0,0,71.30>>
			BREAK
			
			CASE 2 //interior back wall
				positionData.vLoc = <<4.881,42.099,70.586>>	
				positionData.vRot = <<0,0,160>>
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

FUNC BOOL CREATE_MAP_HOLE_PROTECTOR(INT iIndex)
	IF mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_BUILDING_9	
		IF NOT DOES_ENTITY_EXIST(mapHoleProtectors[iIndex])
			IF REQUEST_LOAD_MODEL(prop_ss1_mpint_garage)
				tempPropOffset.vLoc = <<0,0,0>>
				tempPropOffset.vRot = <<0,0,0>>
				GET_MAP_HOLE_PROTECTOR_DATA(tempPropOffset,iIndex)
				mapHoleProtectors[iIndex] = CREATE_OBJECT(prop_ss1_mpint_garage,tempPropOffset.vLoc,FALSE,FALSE,TRUE)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "CREATE_MAP_HOLE_PROTECTOR: created at ",tempPropOffset.vLoc)
				SET_ENTITY_ROTATION(mapHoleProtectors[iIndex],tempPropOffset.vRot)
				FREEZE_ENTITY_POSITION(mapHoleProtectors[iIndex],TRUE)
				SET_ENTITY_PROOFS(mapHoleProtectors[iIndex],TRUE,TRUE,TRUE,TRUE,TRUE,TRUE)
				SET_ENTITY_VISIBLE(mapHoleProtectors[iIndex],FALSE)
				SET_ENTITY_INVINCIBLE(mapHoleProtectors[iIndex],TRUE)
				SET_ENTITY_DYNAMIC(mapHoleProtectors[iIndex],FALSE)
			ENDIF
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL CREATE_BUZZER_PROPS_IF_NEEDED()
	INT i
	IF IS_PROPERTY_MISSING_BUZZERS(iCurrentPropertyID)
		REPEAT MAX_ENTRANCES i
			IF NOT ARE_VECTORS_EQUAL(mpProperties[iCurrentPropertyID].entrance[i].vBuzzerProp,<<0,0,0>>)
				IF NOT DOES_ENTITY_EXIST(createdBuzzers[i])
					IF REQUEST_LOAD_MODEL(GET_PROPERTY_BUZZER_MODEL(iCurrentPropertyID))
						createdBuzzers[i] = CREATE_OBJECT(GET_PROPERTY_BUZZER_MODEL(iCurrentPropertyID),mpProperties[iCurrentPropertyID].entrance[i].vBuzzerProp,FALSE,FALSE,TRUE)
						IF IS_PROPERTY_OFFICE(iCurrentPropertyID)
						AND i = 1
							SET_ENTITY_COORDS_NO_OFFSET(createdBuzzers[i],mpProperties[iCurrentPropertyID].entrance[i].vBuzzerProp)
						ENDIF
						CDEBUG1LN(DEBUG_SAFEHOUSE, "CREATE_BUZZER_PROPS_IF_NEEDED: created #",i," at ",mpProperties[iCurrentPropertyID].entrance[i].vBuzzerProp)
						SET_ENTITY_ROTATION(createdBuzzers[i],mpProperties[iCurrentPropertyID].entrance[i].vBuzzerPropRot)
						FREEZE_ENTITY_POSITION(createdBuzzers[i],TRUE)
						SET_ENTITY_PROOFS(createdBuzzers[i],TRUE,TRUE,TRUE,TRUE,TRUE,TRUE)
						SET_ENTITY_INVINCIBLE(createdBuzzers[i],TRUE)
						SET_ENTITY_DYNAMIC(createdBuzzers[i],FALSE)
					ENDIF
					RETURN FALSE
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	RETURN TRUE
ENDFUNC



FUNC BOOL CREATE_DOOR_PLANKS()
	IF iCurrentPropertyID = PROPERTY_IND_DAY_MEDIUM_2
		IF NOT DOES_ENTITY_EXIST(doorPlanks[0])
			IF REQUEST_LOAD_MODEL(Prop_Cons_plank)
				doorPlanks[0] = CREATE_OBJECT(Prop_Cons_plank,<<1340.5740, -1578.2190, 54.861>>,FALSE,FALSE,TRUE)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "CREATE_DOOR_PLANKS: created at ",<<1340.5740, -1578.2190, 54.861>>)
				SET_ENTITY_ROTATION(doorPlanks[0] ,<<-90.4000, 104.2000, 50.8000>>)
				FREEZE_ENTITY_POSITION(doorPlanks[0],TRUE)
				SET_ENTITY_PROOFS(doorPlanks[0],TRUE,TRUE,TRUE,TRUE,TRUE,TRUE)
				SET_ENTITY_INVINCIBLE(doorPlanks[0],TRUE)
				SET_ENTITY_DYNAMIC(doorPlanks[0],FALSE)
			ENDIF
			RETURN FALSE
		ENDIF
		IF NOT DOES_ENTITY_EXIST(doorPlanks[1])
			IF REQUEST_LOAD_MODEL(Prop_Cons_plank)
				doorPlanks[1] = CREATE_OBJECT(Prop_Cons_plank,<<1340.6680, -1578.1560, 54.248>>,FALSE,FALSE,TRUE)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "CREATE_DOOR_PLANKS: created at ",<<1340.6680, -1578.1560, 54.248>>)
				SET_ENTITY_ROTATION(doorPlanks[1] ,<<-111.6000, -2.3000, -55.6000>>)
				FREEZE_ENTITY_POSITION(doorPlanks[1],TRUE)
				SET_ENTITY_PROOFS(doorPlanks[1],TRUE,TRUE,TRUE,TRUE,TRUE,TRUE)
				SET_ENTITY_INVINCIBLE(doorPlanks[0],TRUE)
				SET_ENTITY_DYNAMIC(doorPlanks[1],FALSE)
			ENDIF
			RETURN FALSE
		ENDIF
	ELIF iCurrentPropertyID = PROPERTY_CLUBHOUSE_3_BASE_A
	OR iCurrentPropertyID = PROPERTY_CLUBHOUSE_9_BASE_B
	OR iCurrentPropertyID = PROPERTY_CLUBHOUSE_11_BASE_B
		IF NOT DOES_ENTITY_EXIST(doorPlanks[0])
			IF REQUEST_LOAD_MODEL(prop_barrier_work06a)
				IF iCurrentPropertyID = PROPERTY_CLUBHOUSE_3_BASE_A
					doorPlanks[0] = CREATE_OBJECT_NO_OFFSET(prop_barrier_work06a,<<35.5731, -1033.4091, 28.4974>>,FALSE,FALSE,TRUE)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "CREATE_DOOR_PLANKS: created at ",<<35.5731, -1033.4091, 28.4974>>)
					SET_ENTITY_ROTATION(doorPlanks[0] ,<<0.0000, -0.0000, 67.4000>>)
				ELIF iCurrentPropertyID = PROPERTY_CLUBHOUSE_9_BASE_B
					doorPlanks[0] = CREATE_OBJECT(prop_barrier_work06a,<<-26.0706, -190.4464, 51.3284>>,FALSE,FALSE,TRUE)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "CREATE_DOOR_PLANKS: created at ",<<-26.0706, -190.4464, 51.3284>>)
					SET_ENTITY_ROTATION(doorPlanks[0] ,<<0.0000, -0.0000, -19.8000>>)
				ELIF iCurrentPropertyID = PROPERTY_CLUBHOUSE_11_BASE_B
					doorPlanks[0] = CREATE_OBJECT(prop_barrier_work06a,<<-42.1150, 6417.3618, 30.4484>>,FALSE,FALSE,TRUE)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "CREATE_DOOR_PLANKS: created at ",<<-42.1150, 6417.3618, 30.4484>>)
					SET_ENTITY_ROTATION(doorPlanks[0] ,<<0.0000, -0.0000, 43.0000>>)
				ENDIF
				FREEZE_ENTITY_POSITION(doorPlanks[0],TRUE)
				SET_ENTITY_PROOFS(doorPlanks[0],TRUE,TRUE,TRUE,TRUE,TRUE,TRUE)
				SET_ENTITY_INVINCIBLE(doorPlanks[0],TRUE)
				SET_ENTITY_DYNAMIC(doorPlanks[0],FALSE)
			ENDIF
			RETURN FALSE
		ENDIF
	ENDIF

	RETURN TRUE
ENDFUNC

FUNC BOOL DOES_LOCAL_REVOLVING_DOOR_EXIST()
	IF mpProperties[iCurrentPropertyID].iBuildingID != MP_PROPERTY_BUILDING_6
		RETURN TRUE
	ENDIF
	
	IF DOES_ENTITY_EXIST(revolvingDoor)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CREATE_REVOLVING_DOOR()
	IF NOT DOES_LOCAL_REVOLVING_DOOR_EXIST()
		IF REQUEST_LOAD_MODEL(PROP_QL_REVOLVING_DOOR)
			CREATE_MODEL_HIDE(<<-935.040,-378.360,39.180>>,2,PROP_QL_REVOLVING_DOOR,TRUE)
			revolvingDoor = CREATE_OBJECT_NO_OFFSET(PROP_QL_REVOLVING_DOOR, <<-935.040,-378.360,39.180>>,FALSE,FALSE,TRUE)
			SET_ENTITY_ROTATION(revolvingDoor, <<0,0,162.0>>)
			FREEZE_ENTITY_POSITION(revolvingDoor,TRUE)
			SET_ENTITY_PROOFS(revolvingDoor,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE)
			SET_ENTITY_INVINCIBLE(revolvingDoor,TRUE)
			SET_ENTITY_DYNAMIC(revolvingDoor,FALSE)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "CREATE_REVOLVING_DOOR: created at ",<<-935.040,-378.360,39.180>>)
			RETURN TRUE
		ENDIF
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

FUNC BOOL CREATE_ENTRY_AREAS_MAP_HOLE_PROTECTORS()
	IF mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_BUILDING_9	
		INT i
		REPEAT MAX_MAP_HOLE_PROTECTORS i
			IF NOT CREATE_MAP_HOLE_PROTECTOR(i)
				RETURN FALSE
			ENDIF
		ENDREPEAT
	ENDIF
	CDEBUG1LN(DEBUG_SAFEHOUSE, "CREATE_ENTRY_AREAS_MAP_HOLE_PROTECTORS() - finished")
	RETURN TRUE
ENDFUNC

FUNC BOOL CHECK_SERVER_EXTERIOR_OBJECTS_EXIST()
//	INT i
//	REPEAT MAX_EXTERIOR_DOORS i
//		IF i > MAX_EXTERIOR_DOORS
////			IF NOT DOES_SERVER_DOOR_EXIST(TRUE,i)
////				RETURN FALSE
////			ENDIF
//			IF NOT DOES_SERVER_DOOR_EXIST(TRUE,i)
//				RETURN FALSE
//			ENDIF
//		ENDIF
//	ENDREPEAT
//	REPEAT MAX_EXTERIOR_DOORS i
//		IF DOES_SERVER_DOOR_EXIST(FALSE,i)
//			SET_ENTITY_NO_COLLISION_ENTITY(NET_TO_OBJ(serverBD.normalDoorIDs[i]),NET_TO_OBJ(serverBD.invisibleDoorIDs[i]),FALSE)
//		ENDIF
//	ENDREPEAT
	//IF NOT DOES_SERVER_GARAGE_BLOCKING_OBJECT_EXIST()
	//	RETURN FALSE
	//ENDIF
	//IF NOT DOES_SERVER_ENTRY_BLOCKING_OBJECT_EXIST()
	//	RETURN FALSE
	//ENDIF
//	IF NOT DOES_SERVER_FOR_SALE_SIGN_EXIST()
//		RETURN FALSE
//	ENDIF
//	IF NOT DOES_SERVER_REVOLVING_DOOR_EXIST()
//		RETURN FALSE
//	ENDIF
	RETURN TRUE
ENDFUNC

FUNC BOOL SERVER_CREATE_EXTERIOR_OBJECTS()
//	INT i
//	REPEAT MAX_EXTERIOR_DOORS i
//		IF i > MAX_EXTERIOR_DOORS
////			IF NOT DOES_SERVER_DOOR_EXIST(TRUE,i)
////				IF CAN_RESERVE_NETWORK_OBJECTS_FOR_THIS_SCRIPT(GET_NUM_RESERVED_MISSION_OBJECTS()+ 1,false,true)
////					RESERVE_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS()+ 1)
////					IF CAN_REGISTER_MISSION_ENTITIES(0,0,1,0)
////						OBJECT_INDEX obj
////						CLEAR_AREA_OF_OBJECTS(buildingDoorDetails[i].vLoc,1)
////						obj = CREATE_OBJECT_NO_OFFSET(propertyDoors.doorModel, buildingDoorDetails[i].vLoc,TRUE,TRUE,TRUE)
////						SET_ENTITY_ROTATION(obj, buildingDoorDetails[i].vRot)
////						serverBD.normalDoorIDs[i] = OBJ_TO_NET(obj)
////						NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(obj,TRUE)
////						FREEZE_ENTITY_POSITION(obj,TRUE)
////						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: SERVER_CREATE_DOORS: server created NORMAL door location: ",buildingDoorDetails[i].vLoc , " rotation: ",buildingDoorDetails[i].vRot)
////					ENDIF
////				ENDIF
////				RETURN FALSE
////			ENDIF
//			IF NOT DOES_SERVER_DOOR_EXIST(FALSE,i)
//				IF CAN_RESERVE_NETWORK_OBJECTS_FOR_THIS_SCRIPT(GET_NUM_RESERVED_MISSION_OBJECTS()+ 1,false,true)
//					RESERVE_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS()+ 1)
//					IF CAN_REGISTER_MISSION_ENTITIES(0,0,1,0)
//						OBJECT_INDEX obj
//						CLEAR_AREA_OF_OBJECTS(buildingDoorDetails[i].vLoc,1)
//						obj = CREATE_OBJECT_NO_OFFSET(propertyDoors.doorModel, buildingDoorDetails[i].vLoc,TRUE,TRUE,TRUE)
//						SET_ENTITY_ROTATION(obj, buildingDoorDetails[i].vRot)
//						serverBD.invisibleDoorIDs[i] = OBJ_TO_NET(obj)
//						NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(obj,TRUE)
//						FREEZE_ENTITY_POSITION(obj,TRUE)
//						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: SERVER_CREATE_DOORS: server created INVISIBLE door location: ",buildingDoorDetails[i].vLoc , " rotation: ",buildingDoorDetails[i].vRot)
//					ENDIF
//				ENDIF
//				RETURN FALSE
//			ENDIF	
//		ENDIF
//	ENDREPEAT

//	IF NOT DOES_SERVER_FOR_SALE_SIGN_EXIST()
//		IF REQUEST_LOAD_MODEL(forSaleDetails.saleSign)
//			IF CAN_RESERVE_NETWORK_OBJECTS_FOR_THIS_SCRIPT(MAX_EXTERIOR_DOORS+3,false,true)
//				//RESERVE_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS()+ 1)
//				IF CAN_REGISTER_MISSION_ENTITIES(0,0,1,0)
//					OBJECT_INDEX obj
//					CLEAR_AREA_OF_OBJECTS(forSaleDetails.vPropLoc,3)
//					obj = CREATE_OBJECT_NO_OFFSET(forSaleDetails.saleSign, forSaleDetails.vPropLoc,TRUE,TRUE,TRUE)
//					SET_ENTITY_ROTATION(obj, forSaleDetails.vPropRot)
//					serverBD.forSaleSign = OBJ_TO_NET(obj)
//					NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(obj,TRUE)
//					FREEZE_ENTITY_POSITION(obj,TRUE)
//					SET_ENTITY_PROOFS(obj,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE)
//					SET_ENTITY_INVINCIBLE(obj,TRUE)
//					SET_ENTITY_DYNAMIC(obj,FALSE)
//					SET_DISABLE_FRAG_DAMAGE(obj,TRUE)
//					SET_DISABLE_BREAKING(NET_TO_OBJ(serverBD.forSaleSign),TRUE)
//					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: SERVER_CREATE_EXTERIOR_OBJECTS: server created FOR SALE SIGN location: ",forSaleDetails.vPropLoc , " rotation: ",forSaleDetails.vPropRot)
//				ENDIF
//			ENDIF
//		ENDIF
//		RETURN FALSE
//	ENDIF
	
//	IF NOT DOES_SERVER_REVOLVING_DOOR_EXIST()
//		IF REQUEST_LOAD_MODEL(PROP_QL_REVOLVING_DOOR)
//			IF CAN_RESERVE_NETWORK_OBJECTS_FOR_THIS_SCRIPT(MAX_EXTERIOR_DOORS+3,false,true)
//				//RESERVE_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS()+ 1)
//				IF CAN_REGISTER_MISSION_ENTITIES(0,0,1,0)
//					OBJECT_INDEX obj
//					obj = CREATE_OBJECT_NO_OFFSET(PROP_QL_REVOLVING_DOOR, <<-935.040,-378.360,39.180>>,TRUE,TRUE,TRUE)
//					SET_ENTITY_ROTATION(obj, <<0,0,162.0>>)
//					serverBD.revolvingDoor = OBJ_TO_NET(obj)
//					NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(obj,TRUE)
//					FREEZE_ENTITY_POSITION(obj,TRUE)
//					SET_ENTITY_PROOFS(obj,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE)
//					SET_ENTITY_INVINCIBLE(obj,TRUE)
//					SET_ENTITY_DYNAMIC(obj,FALSE)
//					//SET_DISABLE_FRAG_DAMAGE(obj,TRUE)
//					//SET_DISABLE_BREAKING(NET_TO_OBJ(serverBD.entryBlockingObj),TRUE)
//					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: SERVER_CREATE_EXTERIOR_OBJECTS: server created REVOLING DOOR OBJECT location: ",<<-935.0067,-378.3667,39.2081>> , " rotation: ",<<0,0,251.5>>)
//				ENDIF
//			ENDIF
//		ENDIF
//		RETURN FALSE
//	ENDIF
	CLEAR_AREA_OF_OBJECTS(forSaleDetails.vPropLoc,3)
	RETURN TRUE
ENDFUNC

FUNC BOOL DOES_FOR_SALE_SIGN_EXIST()
	IF forSaleDetails.locate.fWidth = 0
		RETURN TRUE
	ENDIF
	RETURN DOES_ENTITY_EXIST(forSaleSign)
ENDFUNC

FUNC BOOL IS_THERE_A_PROPERTY_TO_BUY()
	IF NOT IS_FM_TYPE_UNLOCKED(FMMC_TYPE_CUSTOM_CAR_GARAGE)
		RETURN FALSE
	ENDIF
	INT i
	REPEAT mpProperties[iCurrentPropertyID].building.iNumProperties i
		IF NOT DOES_LOCAL_PLAYER_OWN_PROPERTY(mpProperties[iCurrentPropertyID].building.iPropertiesInBuilding[i])
		AND NOT IS_PROPERTY_SALE_BLOCKED_BY_TUNEABLES(iCurrentPropertyID)
			RETURN TRUE
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

FUNC BOOL MAINTAIN_FOR_SALE_SIGN()
	IF IS_THERE_A_PROPERTY_TO_BUY()
		IF NOT DOES_FOR_SALE_SIGN_EXIST()
			IF REQUEST_LOAD_MODEL(forSaleDetails.saleSign)
				forSaleSign = CREATE_OBJECT_NO_OFFSET(forSaleDetails.saleSign, forSaleDetails.vPropLoc,FALSE,FALSE,TRUE)
				SET_ENTITY_ROTATION(forSaleSign, forSaleDetails.vPropRot)
				FREEZE_ENTITY_POSITION(forSaleSign,TRUE)
				SET_ENTITY_PROOFS(forSaleSign,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE)
				SET_ENTITY_INVINCIBLE(forSaleSign,TRUE)
				SET_ENTITY_DYNAMIC(forSaleSign,FALSE)
				SET_DISABLE_FRAG_DAMAGE(forSaleSign,TRUE)
				SET_DISABLE_BREAKING(forSaleSign,TRUE)
				SET_MODEL_AS_NO_LONGER_NEEDED(forSaleDetails.saleSign)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: MAINTAIN_FOR_SALE_SIGN: created FOR SALE SIGN location: ",forSaleDetails.vPropLoc , " rotation: ",forSaleDetails.vPropRot)
				RETURN TRUE
			ENDIF
			RETURN FALSE
		ENDIF
	ELSE
		IF DOES_ENTITY_EXIST(forSaleSign)
			DELETE_OBJECT(forSaleSign)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: MAINTAIN_FOR_SALE_SIGN: deleted: ",forSaleDetails.vPropLoc , " rotation: ",forSaleDetails.vPropRot)
		ENDIF
	ENDIF
	RETURN TRUE
ENDFUNC

//#IF NOT FEATURE_HEIST_PLANNING
//PROC SET_VEHICLE_COLLISION_FLAGS(PLAYER_INDEX playerID, INT i,BOOL bTurnOff)
//	VEHICLE_INDEX vehID
//	
//	IF IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(playerID))
//	AND bTurnOff
//		vehID = GET_VEHICLE_PED_IS_IN(GET_PLAYER_PED(playerID))
//		IF NOT IS_VEHICLE_SEAT_FREE(vehID)
//			IF GET_PED_IN_VEHICLE_SEAT(vehID) = GET_PLAYER_PED(playerID)
//				IF IS_BIT_SET(iLocalGarageCollisionFlag[1],i)
//					IF playerVehicles[i] != vehID
//						IF DOES_ENTITY_EXIST(playerVehicles[i])
//							SET_ENTITY_NO_COLLISION_ENTITY(playerVehicles[i],garageBlockingObj,TRUE)
//							#IF IS_DEBUG_BUILD
//							IF IS_NET_PLAYER_OK(playerID,FALSE)
//								CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_BLOCKING_OBJECTS_COLLISION_FLAGS: setting up player: ", GET_PLAYER_NAME(PLAYER_ID())," OLD car so it DOES collide with garage blocking object")
//							ELSE
//								CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_BLOCKING_OBJECTS_COLLISION_FLAGS: setting up player ID: ",i," OLD car so it DOES collide with garage blocking object")
//							ENDIF
//							#ENDIF
//							CLEAR_BIT(iLocalGarageCollisionFlag[1],i)
//						ENDIF
//					ENDIF
//				ENDIF
//					IF DOES_ENTITY_EXIST(vehID)
//						playerVehicles[i] = vehID
//						SET_ENTITY_NO_COLLISION_ENTITY(vehID,garageBlockingObj,FALSE)
//						SET_BIT(iLocalGarageCollisionFlag[1],i)
//		//				#IF IS_DEBUG_BUILD
//		//				IF IS_NET_PLAYER_OK(playerID,FALSE)
//						//	CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_BLOCKING_OBJECTS_COLLISION_FLAGS: setting up player: ", GET_PLAYER_NAME(PLAYER_ID())," NEW car so it DOES NOT collide with garage blocking object")
//		//				ELSE
//		//					CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_BLOCKING_OBJECTS_COLLISION_FLAGS: setting up player ID: ",i," NEW car so it DOES NOT collide with garage blocking object")
//		//				ENDIF
//		//				#ENDIF
//					ENDIF
//			//ENDIF
//			ENDIF
//		ENDIF
//	ELSE
//		IF IS_BIT_SET(iLocalGarageCollisionFlag[1],i)
//			IF DOES_ENTITY_EXIST(playerVehicles[i])
//					SET_ENTITY_NO_COLLISION_ENTITY(playerVehicles[i],garageBlockingObj,TRUE)
//				#IF IS_DEBUG_BUILD
//				IF IS_NET_PLAYER_OK(playerID,FALSE)
//					CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_BLOCKING_OBJECTS_COLLISION_FLAGS: cleaning up player: ", GET_PLAYER_NAME(PLAYER_ID())," car so it collides with garage blocking object")
//				ELSE
//					CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_BLOCKING_OBJECTS_COLLISION_FLAGS: cleaning up player ID: ",i," car so it collides with garage blocking object")
//				ENDIF
//				#ENDIF
//			ENDIF
//			CLEAR_BIT(iLocalGarageCollisionFlag[1],i)
//		ENDIF
//	ENDIF
//ENDPROC
//#ENDIF

FUNC BOOL CREATE_LOCAL_BLOCKING_OBJECTS()

	IF GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_BUILDING_6
		IF NOT DOES_ENTITY_EXIST(entryBlockingObj)
			IF REQUEST_LOAD_MODEL(prop_dummy_car)
				IF CAN_REGISTER_MISSION_ENTITIES(0,0,1,0)
					CLEAR_AREA_OF_OBJECTS(<<-935.1886,-378.4521,38.8713>>,3)
					entryBlockingObj = CREATE_OBJECT_NO_OFFSET(prop_dummy_car, <<-935.1886,-378.4521,38.8713>>,FALSE,FALSE,TRUE)
					SET_ENTITY_ROTATION(entryBlockingObj, <<0,0,26>>)
					//NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(garageBlockingObj,TRUE)
					FREEZE_ENTITY_POSITION(entryBlockingObj,TRUE)
					SET_ENTITY_PROOFS(entryBlockingObj,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE)
					SET_ENTITY_VISIBLE(entryBlockingObj,FALSE)
					SET_ENTITY_INVINCIBLE(entryBlockingObj,TRUE)
					SET_ENTITY_DYNAMIC(entryBlockingObj,FALSE)
					SET_CAN_CLIMB_ON_ENTITY(entryBlockingObj,FALSE)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: CREATE_LOCAL_BLOCKING_OBJECTS: property #",iCurrentPropertyID,"created Revolving door block 1 ")
				ENDIF
			ENDIF
			RETURN FALSE
		ENDIF
		IF NOT DOES_ENTITY_EXIST(garageBlockingObj)
			IF REQUEST_LOAD_MODEL(prop_dummy_car)
				IF CAN_REGISTER_MISSION_ENTITIES(0,0,1,0)
					CLEAR_AREA_OF_OBJECTS(<<-935.1886,-378.4521,39.8713>>,3)
					garageBlockingObj = CREATE_OBJECT_NO_OFFSET(prop_dummy_car, <<-935.1886,-378.4521,39.8713>>,FALSE,FALSE,TRUE)
					SET_ENTITY_ROTATION(garageBlockingObj, <<0,0,26>>)
					//NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(garageBlockingObj,TRUE)
					FREEZE_ENTITY_POSITION(garageBlockingObj,TRUE)
					SET_ENTITY_PROOFS(garageBlockingObj,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE)
					SET_ENTITY_VISIBLE(garageBlockingObj,FALSE)
					SET_ENTITY_INVINCIBLE(garageBlockingObj,TRUE)
					SET_ENTITY_DYNAMIC(garageBlockingObj,FALSE)
					SET_CAN_CLIMB_ON_ENTITY(garageBlockingObj,FALSE)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: CREATE_LOCAL_BLOCKING_OBJECTS: property #",iCurrentPropertyID,"created Revolving door block 2 ")
					SET_MODEL_AS_NO_LONGER_NEEDED(prop_dummy_car)
				ENDIF
			ENDIF
			RETURN FALSE
		ENDIF
	ENDIF
	RETURN TRUE
//	IF NOT DOES_LOCAL_GARAGE_BLOCKING_OBJECT_EXIST()
//		IF REQUEST_LOAD_MODEL(blockObjectDetails.mn_GarageModel)
//			//IF CAN_RESERVE_NETWORK_OBJECTS_FOR_THIS_SCRIPT(GET_NUM_RESERVED_MISSION_OBJECTS()+ 1,false,true)
//			//	RESERVE_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS()+ 1)
//				IF CAN_REGISTER_MISSION_ENTITIES(0,0,1,0)
//					CLEAR_AREA_OF_OBJECTS(blockObjectDetails.vGarageLoc,3)
//					garageBlockingObj = CREATE_OBJECT_NO_OFFSET(blockObjectDetails.mn_GarageModel, blockObjectDetails.vGarageLoc,FALSE,FALSE,TRUE)
//					SET_ENTITY_ROTATION(garageBlockingObj, blockObjectDetails.vGarageRot)
//					//NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(garageBlockingObj,TRUE)
//					FREEZE_ENTITY_POSITION(garageBlockingObj,TRUE)
//					SET_ENTITY_PROOFS(garageBlockingObj,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE)
//					SET_ENTITY_VISIBLE(garageBlockingObj,FALSE)
//					SET_ENTITY_INVINCIBLE(garageBlockingObj,TRUE)
//					SET_ENTITY_DYNAMIC(garageBlockingObj,FALSE)
//					SET_CAN_CLIMB_ON_ENTITY(garageBlockingObj,FALSE)
//					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: CREATE_LOCAL_BLOCKING_OBJECTS: property #",iCurrentPropertyID,"server created GARAGE BLOCKING OBJECT location: ",blockObjectDetails.vGarageLoc , " rotation: ",blockObjectDetails.vGarageRot)
//				ENDIF
//			//ENDIF
//		ENDIF
//		RETURN FALSE
//	ENDIF
//	IF NOT DOES_LOCAL_ENTRY_BLOCKING_OBJECT_EXIST()
//		IF REQUEST_LOAD_MODEL(blockObjectDetails.mn_EntryModel)
//			//IF CAN_RESERVE_NETWORK_OBJECTS_FOR_THIS_SCRIPT(GET_NUM_RESERVED_MISSION_OBJECTS()+ 1,false,true)
//				//RESERVE_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS()+ 1)
//				IF CAN_REGISTER_MISSION_ENTITIES(0,0,1,0)
//					CLEAR_AREA_OF_OBJECTS(blockObjectDetails.vEntryLoc,3)
//					entryBlockingObj = CREATE_OBJECT_NO_OFFSET(blockObjectDetails.mn_EntryModel, blockObjectDetails.vEntryLoc,FALSE,FALSE,TRUE)
//					SET_ENTITY_ROTATION(entryBlockingObj, blockObjectDetails.vEntryRot)
//					//NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(entryBlockingObj,TRUE)
//					FREEZE_ENTITY_POSITION(entryBlockingObj,TRUE)
//					SET_ENTITY_VISIBLE(entryBlockingObj,FALSE)
//					SET_ENTITY_PROOFS(entryBlockingObj,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE)
//					SET_ENTITY_INVINCIBLE(entryBlockingObj,TRUE)
//					SET_ENTITY_DYNAMIC(entryBlockingObj,FALSE)
//					SET_CAN_CLIMB_ON_ENTITY(entryBlockingObj,FALSE)
//					//SET_DISABLE_FRAG_DAMAGE(obj,TRUE)
//					//SET_DISABLE_BREAKING(NET_TO_OBJ(entryBlockingObj),TRUE)
//					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: CREATE_LOCAL_BLOCKING_OBJECTS:  property #",iCurrentPropertyID," s created ENTRY BLOCKING OBJECT location: ",blockObjectDetails.vEntryLoc , " rotation: ",blockObjectDetails.vEntryRot)
//				ENDIF
//			//ENDIF
//		ENDIF
//		RETURN FALSE
//	ENDIF
//	#IF FEATURE_HEIST_PLANNING
//	ENDIF
//	#ENDIF
//	RETURN TRUE
ENDFUNC

//PROC MAINTAIN_BLOCKING_OBJECTS_COLLISION_FLAGS()
//	
//	#IF FEATURE_HEIST_PLANNING
//	IF bUseNewEntryAnims
//		EXIT
//	ENDIF	
//	#ENDIF
//
//	#IF NOT FEATURE_HEIST_PLANNING	
//	INT i, j
//	PLAYER_INDEX playerID
//	IF DOES_ENTITY_EXIST(entryBlockingObj)
//	OR DOES_ENTITY_EXIST(garageBlockingObj)
//		REPEAT NUM_NETWORK_PLAYERS i
//			playerID = INT_TO_PLAYERINDEX(i)
//			IF IS_NET_PLAYER_OK(playerID,FALSE,FALSE)
//				IF DOES_LOCAL_GARAGE_BLOCKING_OBJECT_EXIST()
//					IF DOES_ENTITY_EXIST(garageBlockingObj)
//						IF IS_BIT_SET(serverBD.iRequestDoorUsageBS[2],i)
//						OR IS_BIT_SET(serverBD.iPlayersInsideEntranceBS[2],i)
//							
//							SET_VEHICLE_COLLISION_FLAGS(playerID,i,TRUE)
//							IF NOT IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(playerID))
//								//IF NOT IS_BIT_SET(iLocalGarageCollisionFlag[0],i)
//									SET_ENTITY_NO_COLLISION_ENTITY(GET_PLAYER_PED(playerID),garageBlockingObj,FALSE)
//									SET_BIT(iLocalGarageCollisionFlag[0],i)
//									//#IF IS_DEBUG_BUILD
//									//IF NOT IS_BIT_SET(iLocalGarageCollisionFlag[0],i)
//									CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_BLOCKING_OBJECTS_COLLISION_FLAGS: Setting player #",i ," Name = ", GET_PLAYER_NAME(playerID), " can walk through garage for property #", iCurrentPropertyID)
//									//ENDIF
//									//#ENDIF
//								
//								//ENDIF
//							ENDIF
//							
//						ELSE
//							SET_VEHICLE_COLLISION_FLAGS(playerID,i,FALSE)
//							
//							IF NOT IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(playerID))
//								IF IS_BIT_SET(iLocalGarageCollisionFlag[0],i)
//									SET_ENTITY_NO_COLLISION_ENTITY(GET_PLAYER_PED(playerID),garageBlockingObj,TRUE)	
//									CLEAR_BIT(iLocalGarageCollisionFlag[0],i)
//									CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_BLOCKING_OBJECTS_COLLISION_FLAGS: Setting player #",i ," Name = ", GET_PLAYER_NAME(playerID), " can NOT walk through garage for property #", iCurrentPropertyID)
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//				IF DOES_LOCAL_ENTRY_BLOCKING_OBJECT_EXIST()
//					IF DOES_ENTITY_EXIST(entryBlockingObj)
//						IF IS_BIT_SET(serverBD.iRequestDoorUsageBS[0],i) OR IS_BIT_SET(serverBD.iRequestDoorUsageBS[1],i)
//						OR IS_BIT_SET(serverBD.iPlayersInsideEntranceBS[0],i) OR IS_BIT_SET(serverBD.iPlayersInsideEntranceBS[1],i)
//							//IF NOT IS_BIT_SET(iLocalEntryCollisionFlag,i)
//								SET_ENTITY_NO_COLLISION_ENTITY(GET_PLAYER_PED(playerID),entryBlockingObj,FALSE)
//								SET_BIT(iLocalEntryCollisionFlag,i)
//								CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_BLOCKING_OBJECTS_COLLISION_FLAGS: Setting player #",i ," Name = ", GET_PLAYER_NAME(playerID), " can walk through entry area for property #", iCurrentPropertyID)
//							//ENDIF
//						ELSE
//							IF IS_BIT_SET(iLocalEntryCollisionFlag,i)
//								SET_ENTITY_NO_COLLISION_ENTITY(GET_PLAYER_PED(playerID),entryBlockingObj,TRUE)	
//								CLEAR_BIT(iLocalEntryCollisionFlag,i)
//								CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_BLOCKING_OBJECTS_COLLISION_FLAGS: Setting player #",i ," Name = ", GET_PLAYER_NAME(playerID), " can NOT walk through entry area for property #", iCurrentPropertyID)
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDREPEAT
//	ENDIF
//	#ENDIF
//	
//	
//	INT iEntranceArea
//	j= 0
//	IF iLocalStage < STAGE_SETUP_FOR_USING_PROPERTY
//		IF IS_NET_PLAYER_OK(PLAYER_ID())
//			REPEAT MAX_EXTERIOR_DOORS j
//				IF j <= 1
//					iEntranceArea = 0
//					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(),entranceArea[iEntranceArea].vPos1,entranceArea[iEntranceArea].vPos2,entranceArea[iEntranceArea].fWidth)
//						REQUEST_TO_USE_DOOR(0)
//						REQUEST_TO_USE_DOOR(1)
//					ELSE
//						//IF NOT g_bPlayerInProcessOfExitingInterior
//						REQUEST_TO_USE_DOOR(0,FALSE)
//						REQUEST_TO_USE_DOOR(1,FALSE)
//						//ENDIF
//					ENDIF
//				ELSE
//					iEntranceArea = 1
//					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(),entranceArea[iEntranceArea].vPos1,entranceArea[iEntranceArea].vPos2,entranceArea[iEntranceArea].fWidth)
//						IF iCurrentPropertyID = PROPERTY_LOW_APT_7
//							DISABLE_OCCLUSION_THIS_FRAME()
//						ENDIF
//						REQUEST_TO_USE_DOOR(2)
//					ELSE
//						IF NOT g_bPlayerInProcessOfExitingInterior
//							REQUEST_TO_USE_DOOR(2,FALSE)
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDREPEAT
//		ENDIF
//	ENDIF
//	//do player door collision
//ENDPROC

PROC MAINTAIN_DOOR_USAGE_REQUESTS_SERVER()

	IF bUseNewEntryAnims		
		IF NOT CHECK_SERVER_EXTERIOR_OBJECTS_EXIST()
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: SERVER IN STAGE RUNNING: but hasn't created it's server objects, so creating them now. :)")
			SERVER_CREATE_EXTERIOR_OBJECTS()
		ENDIF
		
		EXIT
	ENDIF	
//		INT i, j
//	PLAYER_INDEX playerID
//	
//	INT iEntranceArea
//	REPEAT MAX_EXTERIOR_DOORS j
//		IF propertyDoors[j].iDoorHash != 0
////		OR (mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_BUILDING_6 AND j = 0)
//			IF NOT (mpProperties[iCurrentPropertyID].iBuildingID = MP_PROPERTY_BUILDING_6 AND j = 0)
//				IF serverBD.iRequestDoorUsageBS[j] = 0
//				AND serverBD.iPlayersInsideEntranceBS[j] = 0
//					IF NOT IS_BIT_SET(iDoorSetBS[j], SERVER_DOOR_LOCKED)
//						IF DOOR_SYSTEM_GET_DOOR_PENDING_STATE(propertyDoors[j].iDoorHash) = DOORSTATE_INVALID
//							IF DOOR_SYSTEM_GET_DOOR_STATE(propertyDoors[j].iDoorHash) != DOORSTATE_LOCKED
//								IF NETWORK_HAS_CONTROL_OF_DOOR(propertyDoors[j].iDoorHash)
//									DOOR_SYSTEM_SET_DOOR_STATE(propertyDoors[j].iDoorHash,DOORSTATE_LOCKED,TRUE,TRUE)
//									RESET_NET_TIMER(serverDoorStateTimeout[j])
//									SET_BIT(iDoorSetBS[j], SERVER_DOOR_LOCKED)
//									CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_DOOR_USAGE_REQUESTS_SERVER: Setting door  (STATE NOT LOCKED) #", j, " to be locked")
//								ELSE
//									NETWORK_REQUEST_CONTROL_OF_DOOR(propertyDoors[j].iDoorHash)
//								ENDIF
//							ENDIF
//						ELSE
//							IF DOOR_SYSTEM_GET_DOOR_PENDING_STATE(propertyDoors[j].iDoorHash) != DOORSTATE_LOCKED
//								IF NETWORK_HAS_CONTROL_OF_DOOR(propertyDoors[j].iDoorHash)
//									DOOR_SYSTEM_SET_DOOR_STATE(propertyDoors[j].iDoorHash,DOORSTATE_LOCKED,TRUE,TRUE)
//									RESET_NET_TIMER(serverDoorStateTimeout[j])
//									CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_DOOR_USAGE_REQUESTS_SERVER: Setting door  (STATE NOT PENDING LOCKED)  #", j, " to be locked")
//									SET_BIT(iDoorSetBS[j], SERVER_DOOR_LOCKED)
//								ELSE
//									NETWORK_REQUEST_CONTROL_OF_DOOR(propertyDoors[j].iDoorHash)
//								ENDIF
//							ENDIF
//						ENDIF					
//					ELSE
//						IF NOT HAS_NET_TIMER_STARTED(serverDoorStateTimeout[j])
//							START_NET_TIMER(serverDoorStateTimeout[j],TRUE)
//							CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_DOOR_USAGE_REQUESTS_SERVER: Setting door state (Locked) timer for door  #", j)
//	//					ELSE
//	//						IF HAS_NET_TIMER_EXPIRED(serverDoorStateTimeout[j],1000,TRUE)
//	//							CLEAR_BIT(serverBD.iDoorSetBS[j], SERVER_DOOR_LOCKED)
//	//						ENDIF
//						ENDIF
//					ENDIF
//				ELSE
//					IF NOT IS_BIT_SET(iDoorSetBS[j], SERVER_DOOR_UNLOCKED)
//						IF DOOR_SYSTEM_GET_DOOR_PENDING_STATE(propertyDoors[j].iDoorHash) = DOORSTATE_INVALID
//							IF DOOR_SYSTEM_GET_DOOR_STATE(propertyDoors[j].iDoorHash) != DOORSTATE_UNLOCKED
//								IF NETWORK_HAS_CONTROL_OF_DOOR(propertyDoors[j].iDoorHash)
//									DOOR_SYSTEM_SET_DOOR_STATE(propertyDoors[j].iDoorHash,DOORSTATE_UNLOCKED,TRUE,TRUE)
//									RESET_NET_TIMER(serverDoorStateTimeout[j])
//									SET_BIT(iDoorSetBS[j], SERVER_DOOR_UNLOCKED)
//									CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_DOOR_USAGE_REQUESTS_SERVER: Setting door (STATE NOT UNLOCKED) #", j, " to be open")
//								ELSE
//									NETWORK_REQUEST_CONTROL_OF_DOOR(propertyDoors[j].iDoorHash)
//								ENDIF
//							ENDIF
//						ELSE
//							IF DOOR_SYSTEM_GET_DOOR_PENDING_STATE(propertyDoors[j].iDoorHash) != DOORSTATE_UNLOCKED
//								IF NETWORK_HAS_CONTROL_OF_DOOR(propertyDoors[j].iDoorHash)
//									DOOR_SYSTEM_SET_DOOR_STATE(propertyDoors[j].iDoorHash,DOORSTATE_UNLOCKED,TRUE,TRUE)
//									RESET_NET_TIMER(serverDoorStateTimeout[j])
//									SET_BIT(iDoorSetBS[j], SERVER_DOOR_UNLOCKED)
//									CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_DOOR_USAGE_REQUESTS_SERVER: Setting door (STATE NOT PENDING UNLOCKED) #", j, " to be open")
//								ELSE
//									NETWORK_REQUEST_CONTROL_OF_DOOR(propertyDoors[j].iDoorHash)
//								ENDIF
//							ENDIF
//						ENDIF
//					ELSE
//						IF NOT HAS_NET_TIMER_STARTED(serverDoorStateTimeout[j])
//							START_NET_TIMER(serverDoorStateTimeout[j],TRUE)
//							CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_DOOR_USAGE_REQUESTS_SERVER: Setting door state (UN-Locked) timer for door  #", j)
//	//					ELSE
//	//						IF HAS_NET_TIMER_EXPIRED(serverDoorStateTimeout[j],1000,TRUE)
//	//							CLEAR_BIT(serverBD.iDoorSetBS[j], SERVER_DOOR_UNLOCKED)
//	//						ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//			IF HAS_NET_TIMER_STARTED(serverDoorStateTimeout[j])
//				IF HAS_NET_TIMER_EXPIRED(serverDoorStateTimeout[j],1000,TRUE)
//					RESET_NET_TIMER(serverDoorStateTimeout[j])
//					CLEAR_BIT(iDoorSetBS[j], SERVER_DOOR_LOCKED)
//					CLEAR_BIT(iDoorSetBS[j], SERVER_DOOR_UNLOCKED)
//					CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_DOOR_USAGE_REQUESTS_SERVER: Clearing door state timer for door  #", j)
//				ENDIF
//			ENDIF
//			REPEAT NUM_NETWORK_PLAYERS i
//				playerID = INT_TO_PLAYERINDEX(i)
//				IF IS_NET_PLAYER_OK(playerID)
//					IF j <= 1
//						iEntranceArea = 0
//					ELSE
//						iEntranceArea = 1
//					ENDIF
//					IF NOT IS_BIT_SET(GlobalplayerBD_FM[i].propertyDetails.iBS, PROPERTY_BROADCAST_BS_PLAYER_ENTERING_A_PROPERTY)
//						IF entranceArea[iEntranceArea].fWidth > 0
//							IF NOT IS_BIT_SET(serverBD.iRequestDoorUsageBS[j],i)
//								IF iEntranceArea = 0
//									IF IS_ENTITY_IN_ANGLED_AREA(GET_PLAYER_PED(playerID),entranceArea[iEntranceArea].vPos1,entranceArea[iEntranceArea].vPos2,entranceArea[iEntranceArea].fWidth)
//										SET_BIT(serverBD.iPlayersInsideEntranceBS[j],i)
//									ELSE
//										CLEAR_BIT(serverBD.iPlayersInsideEntranceBS[j],i)
//									ENDIF
//								ELSE
//									IF NOT IS_BIT_SET(playerBD[i].iBS,PLAYER_BD_BS_ENTERING_GARAGE)
//										IF IS_ENTITY_IN_ANGLED_AREA(GET_PLAYER_PED(playerID),entranceArea[iEntranceArea].vPos1,entranceArea[iEntranceArea].vPos2,entranceArea[iEntranceArea].fWidth)
//											SET_BIT(serverBD.iPlayersInsideEntranceBS[j],i)
//										ELSE
//											CLEAR_BIT(serverBD.iPlayersInsideEntranceBS[j],i)
//										ENDIF
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//				IF NOT IS_BIT_SET(serverBD.iPlayersInsideEntranceBS[j],i)
//					IF NOT IS_BIT_SET(serverBD.iRequestDoorUsageBS[j],i)
//						IF IS_NET_PLAYER_OK(playerID)
//							IF NETWORK_IS_PLAYER_A_PARTICIPANT(playerID)
//								IF IS_BIT_SET(playerBD[i].iBS, PLAYER_BD_BS_REQUEST_USE_OF_DOOR0+j)
//									IF NOT IS_BIT_SET(serverBD.iRequestDoorUsageBS[j],i)
//										SET_BIT(serverBD.iRequestDoorUsageBS[j],i)
//										CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_DOOR_USAGE_REQUESTS_SERVER: setting flag door open #", j," open for player #", i)
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
//					ELSE
//						//IF IS_BIT_SET(serverBD.iRequestDoorUsageBS[j],i)
//							#IF IS_DEBUG_BUILD
//							#IF ALLOW_FAKE_GARAGE_DEBUG
//							IF db_bDisplayDoorRequestState
//								CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_DOOR_USAGE_REQUESTS_SERVER: outstanding door #", j," request for player #", i)
//							ENDIF
//							#ENDIF
//							#ENDIF
//							
//							IF NOT IS_NET_PLAYER_OK(playerID,FALSE)
//								CLEAR_BIT(serverBD.iRequestDoorUsageBS[j],i)
//								CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_DOOR_USAGE_REQUESTS_SERVER: outstanding door #", j, " cleared request (not ok) from player #", i)
//							ELSE
//								#IF IS_DEBUG_BUILD
//								#IF ALLOW_FAKE_GARAGE_DEBUG
//								IF db_bDisplayDoorRequestState
//									CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_DOOR_USAGE_REQUESTS_SERVER: player OK")
//								ENDIF
//								#ENDIF
//								#ENDIF
//								
//								IF NOT NETWORK_IS_PLAYER_A_PARTICIPANT(playerID)
//									CLEAR_BIT(serverBD.iRequestDoorUsageBS[j],i)
//									CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_DOOR_USAGE_REQUESTS_SERVER: outstanding door #", j, " cleared request (not participant) from player #", i)
//								ENDIF
//								
//								IF NOT IS_BIT_SET(playerBD[i].iBS, PLAYER_BD_BS_REQUEST_USE_OF_DOOR0+j)
//									CLEAR_BIT(serverBD.iRequestDoorUsageBS[j],i)
//									CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_DOOR_USAGE_REQUESTS_SERVER: outstanding door #", j, " cleared request (not requesting) from player #", i)
//								#IF IS_DEBUG_BUILD
//								#IF ALLOW_FAKE_GARAGE_DEBUG
//								ELSE
//								IF db_bDisplayDoorRequestState
//									CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_DOOR_USAGE_REQUESTS_SERVER: player still requesting door")
//								ENDIF
//								#ENDIF
//								#ENDIF
//								ENDIF
//							ENDIF
//							
//						//ENDIF
//					ENDIF
//				ENDIF
//			ENDREPEAT
//		ENDIF
//	ENDREPEAT
ENDPROC


//FUNC BOOL HANDLE_REQUESTED_DOOR_STATE(INT iDoor)
//	
//	RETURN FALSE
//ENDFUNC



//╔═════════════════════════════════════════════════════════════════════════════╗
//║								WALK INTO PROPERTY																		
//╚═════════════════════════════════════════════════════════════════════════════╝


PROC WALK_IN_CLEANUP(BOOL bSetPlayerControlOn)
	IF bSetPlayerControlOn
		IF IS_NET_PLAYER_OK(PLAYER_ID())
		AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		ELSE
			CDEBUG1LN(DEBUG_SAFEHOUSE, "     ----->>>>>     not returning control switch in progress or player not ok")
		ENDIF
		CLEAR_BIT(iLocalBS, LOCAL_BS_TAKEN_AWAY_CONTROL)
		CLEAR_BIT(entryCutData.iBS,ENTRY_CS_BS_TOOK_AWAY_CONTROL)
	ENDIF
	
	globalPropertyEntryData.bWalkInSeqDone = TRUE
	iWalkInStage = WIS_INIT
	CLEANUP_PROPERTY_CAMERAS()
	//SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_OpenDoorArmIK, FALSE) 
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "     ----->>>>>     WALK_IN_CLEANUP - DONE")
ENDPROC

//PURPOSE: Controls the player walking into a property through a door
FUNC BOOL WALK_INTO_THIS_PROPERTY(BOOL bResetCoords, STRUCT_net_realty_1_scene &scene)
	
	//Check to see if we should cleanup
	IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
	OR IS_PLAYER_SWITCH_IN_PROGRESS()
		iWalkInStage = WIS_CLEANUP
		CDEBUG1LN(DEBUG_SAFEHOUSE, "     ----->>>>>     WALK_INTO_THIS_PROPERTY - iWalkInStage = WIS_CLEANUP - A")
	ENDIF
	
	IF iWalkInStage <=WIS_WAIT_FOR_DOOR_CONTROL
		RETAIN_BUZZER_MARKER()
	ENDIF
		
	
	#IF ACTIVATE_PROPERTY_CS_DEBUG
	BOOL bRequestDoorDebug = FALSE
	IF rag_net_realty_1_scene.bEnableTool
		bRequestDoorDebug = TRUE
	ENDIF
	#ENDIF
	
	IF iWalkInStage = WIS_INIT
	OR iWalkInStage = WIS_WAIT_FOR_DOOR_CONTROL
	OR iWalkInStage = WIS_GIVE_SEQUENCE
	OR iWalkInStage = WIS_WAIT_FOR_SEQUENCE_END
		CDEBUG1LN(DEBUG_SAFEHOUSE, "WALK_INTO_THIS_PROPERTY: calling REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()")
		REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()
	ENDIF 
	
	//Process Stages
	CDEBUG1LN(DEBUG_SAFEHOUSE, "WALK_INTO_THIS_PROPERTY: iWalkInStage: ", iWalkInStage)
	SWITCH iWalkInStage
		CASE WIS_INIT
			globalPropertyEntryData.bWalkInSeqDone = FALSE
			IF bUseNewEntryAnims
				IF NOT IS_BIT_SET(iLocalBS2,LOCAL_BS2_bWalkInTriggered)
					IF RUN_ENTRY_CUTSCENE(ENTRY_CUTSCENE_TYPE_FRONT_DOOR) //moved to pre menu
						iWalkInStage = WIS_WAIT_FOR_LOAD
						SET_BIT(iLocalBS2,LOCAL_BS2_bWalkInTriggered)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "     ----->>>>>     WALK_INTO_THIS_PROPERTY - iWalkInStage = WIS_WAIT_FOR_LOAD - YTV")
					ENDIF
				ELSE
					iWalkInStage = WIS_WAIT_FOR_LOAD
					CDEBUG1LN(DEBUG_SAFEHOUSE, "     ----->>>>>     WALK_INTO_THIS_PROPERTY - iWalkInStage = WIS_WAIT_FOR_LOAD - bypassing as already run walk in sequence.")
				ENDIF
			ELSE
		
			IF mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iType = ENTRANCE_TYPE_GARAGE
			
			#IF ACTIVATE_PROPERTY_CS_DEBUG
			AND NOT rag_net_realty_1_scene.bEnableTool
			#ENDIF
			
				iWalkInStage = WIS_WAIT_FOR_LOAD
				CDEBUG1LN(DEBUG_SAFEHOUSE, "     ----->>>>>     WALK_INTO_THIS_PROPERTY - iWalkInStage = WIS_WAIT_FOR_LOAD - GGG")
			ELSE
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				SET_BIT(iLocalBS, LOCAL_BS_TAKEN_AWAY_CONTROL)
				//SET_BIT(ilocalBS, LOCAL_BS_TAKEN_AWAY_CONTROL) //NEED ????
				
				//DISPLAY_RADAR(FALSE)
				//Temp warp player
				
				//Create Camera
				/*IF NOT DOES_CAM_EXIST(propertyCam)
					propertyCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE) 
					SET_CAM_ACTIVE(propertyCam, TRUE)
					SET_CAM_PARAMS(propertyCam,  vCam1Pos, vCam1Rot, fCam1Fov)		//<<-769.830200,308.202576,88.705116>>, <<-14.696461,-0.000000,61.902519>>
					SET_CAM_PARAMS(propertyCam,  vCam2Pos, vCam2Rot, fCam2Fov, iCamInterp, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
					SHAKE_CAM(propertyCam, "HAND_SHAKE", 0.25)
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "     ----->>>>>     WALK_INTO_THIS_PROPERTY - CAM 1 ", vCam1Pos, " ", vCam1Rot, " ", fCam1Fov)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "     ----->>>>>     WALK_INTO_THIS_PROPERTY - CAM 2 ", vCam2Pos, " ", vCam2Rot, " ", fCam2Fov)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "     ----->>>>>     WALK_INTO_THIS_PROPERTY - CAM TIME ", iCamInterp)
				ENDIF
				
				REINIT_NET_TIMER(iCameraTimer)*/
				globalPropertyEntryData.bWalkInSeqDone = FALSE
				//SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_OpenDoorArmIK, TRUE) 
				SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_SearchForClosestDoor, TRUE)
				//SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_OpenDoorArmIK, TRUE) CODE CHANGES REQUIRED RANDOM NUMBER #1123124893
				
				HIDE_HUD_AND_RADAR_THIS_FRAME()
				
				iWalkInStage = WIS_WAIT_FOR_DOOR_CONTROL
				CDEBUG1LN(DEBUG_SAFEHOUSE, "     ----->>>>>     WALK_INTO_THIS_PROPERTY - iWalkInStage = WIS_WAIT_FOR_DOOR_CONTROL - A")
			ENDIF
			ENDIF
		BREAK
		
		CASE WIS_WAIT_FOR_DOOR_CONTROL
			REQUEST_TO_USE_DOOR(0)
			REQUEST_TO_USE_DOOR(1)
			IF REQUEST_TO_USE_DOOR(0, DEFAULT #IF ACTIVATE_PROPERTY_CS_DEBUG, bRequestDoorDebug #ENDIF)
			AND REQUEST_TO_USE_DOOR(1, DEFAULT #IF ACTIVATE_PROPERTY_CS_DEBUG, bRequestDoorDebug #ENDIF)
				iWalkInStage = WIS_GIVE_SEQUENCE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "     ----->>>>>     WALK_INTO_THIS_PROPERTY - iWalkInStage = WIS_GIVE_SEQUENCE - A")
			ELIF GET_PROPERTY_SIZE_TYPE(iCurrentPropertyID) < PROP_SIZE_TYPE_SMALL_APT
				iWalkInStage = WIS_GIVE_SEQUENCE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "     ----->>>>>     WALK_INTO_THIS_PROPERTY - iWalkInStage = WIS_GIVE_SEQUENCE - B")
			ELSE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "     ----->>>>>     WALK_INTO_THIS_PROPERTY - WIS_WAIT_FOR_DOOR_CONTROL")
				
				#IF ACTIVATE_PROPERTY_CS_DEBUG
				IF rag_net_realty_1_scene.bEnableTool
					iWalkInStage = WIS_GIVE_SEQUENCE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "     ----->>>>>     WALK_INTO_THIS_PROPERTY - iWalkInStage = WIS_GIVE_SEQUENCE - C")
				ENDIF
				#ENDIF
				
			ENDIF
			HIDE_HUD_AND_RADAR_THIS_FRAME()
		BREAK
		
		CASE WIS_GIVE_SEQUENCE
			
			IF bResetCoords
				SET_ENTITY_COORDS(PLAYER_PED_ID(), scene.mPlacers[net_realty_1_scene_PLACER_playerPos].vPos)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), scene.mPlacers[net_realty_1_scene_PLACER_playerPos].fRot)
			ENDIF
			
//			SEQUENCE_INDEX siTemp
//			OPEN_SEQUENCE_TASK(siTemp)
//				TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, scene.mMarkers[net_realty_1_scene_MARKER_playerWalk0].vPos, PEDMOVEBLENDRATIO_WALK, -1, 0.7, ENAV_NO_STOPPING)
//				TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, scene.mMarkers[net_realty_1_scene_MARKER_playerWalk1].vPos, PEDMOVEBLENDRATIO_WALK, -1, 0.7, ENAV_NO_STOPPING)
//				TASK_GO_STRAIGHT_TO_COORD(NULL, scene.mMarkers[net_realty_1_scene_MARKER_playerWalk2].vPos, PEDMOVEBLENDRATIO_WALK, -1)
//			CLOSE_SEQUENCE_TASK(siTemp)
//			TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), siTemp)
//			FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK)
			TASK_FLUSH_ROUTE()
			IF iCurrentPropertyID = PROPERTY_IND_DAY_MEDIUM_3
			AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(),<<-105.851982,6528.291992,28.911039>>,<<-103.993401,6530.095703,31.722012>>,2.250000)
				TASK_EXTEND_ROUTE( <<-106.0008, 6530.2056, 28.8582>>)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "CDM: walking to  <<-106.0008, 6530.2056, 28.8582>>")
			ELSE
				TASK_EXTEND_ROUTE(scene.mMarkers[net_realty_1_scene_MARKER_playerWalk0].vPos)
			ENDIF
			
			TASK_EXTEND_ROUTE(scene.mMarkers[net_realty_1_scene_MARKER_playerWalk1].vPos)
			TASK_EXTEND_ROUTE(scene.mMarkers[net_realty_1_scene_MARKER_playerWalk2].vPos)
			TASK_FOLLOW_POINT_ROUTE(PLAYER_PED_ID(), PEDMOVE_WALK, TICKET_SINGLE )

			//Create Camera
//			IF NOT DOES_CAM_EXIST(propertyCam)
//				propertyCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE) 
//				SET_CAM_ACTIVE(propertyCam, TRUE)
//				SET_CAM_PARAMS(propertyCam,  vCam1Pos, vCam1Rot, fCam1Fov)		//<<-769.830200,308.202576,88.705116>>, <<-14.696461,-0.000000,61.902519>>
//				SET_CAM_PARAMS(propertyCam,  vCam2Pos, vCam2Rot, fCam2Fov, iCamInterp, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
//				SHAKE_CAM(propertyCam, "HAND_SHAKE", 0.25)
//				RENDER_SCRIPT_CAMS(TRUE, FALSE)
//				CDEBUG1LN(DEBUG_SAFEHOUSE, "     ----->>>>>     WALK_INTO_THIS_PROPERTY - CAM 1 ", vCam1Pos, " ", vCam1Rot, " ", fCam1Fov)
//				CDEBUG1LN(DEBUG_SAFEHOUSE, "     ----->>>>>     WALK_INTO_THIS_PROPERTY - CAM 2 ", vCam2Pos, " ", vCam2Rot, " ", fCam2Fov)
//				CDEBUG1LN(DEBUG_SAFEHOUSE, "     ----->>>>>     WALK_INTO_THIS_PROPERTY - CAM TIME ", iCamInterp)
//			ENDIF
			SceneTool_ExecutePan(scene.mPans[net_realty_1_scene_PAN_establishing], propertyCam0, propertyCam1)
			
			REINIT_NET_TIMER(iCameraTimer,TRUE)
			
			//SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_OpenDoorArmIK, TRUE)
			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_SearchForClosestDoor, TRUE)
			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_OpenDoorArmIK, TRUE) //CODE CHANGES REQUIRED RANDOM NUMBER #1123124893
			
			REINIT_NET_TIMER(iWalkInTimer,TRUE)
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			
			iWalkInStage = WIS_WAIT_FOR_SEQUENCE_END
			CDEBUG1LN(DEBUG_SAFEHOUSE, "     ----->>>>>     WALK_INTO_THIS_PROPERTY - iWalkInStage = WIS_WAIT_FOR_SEQUENCE_END - A")
		BREAK
		
		CASE WIS_WAIT_FOR_SEQUENCE_END
			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_SearchForClosestDoor, TRUE)
			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_OpenDoorArmIK, TRUE) //CODE CHANGES REQUIRED RANDOM NUMBER #1123124893
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			
			IF HAS_NET_TIMER_EXPIRED(iCameraTimer, ROUND((scene.mPans[net_realty_1_scene_PAN_establishing].fDuration + scene.fExitDelay) * 1000.0),TRUE)
				IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_PERFORM_SEQUENCE) != WAITING_TO_START_TASK
				AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
					//SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_OpenDoorArmIK, FALSE) 
					iWalkInStage = WIS_WAIT_FOR_LOAD
					CDEBUG1LN(DEBUG_SAFEHOUSE, "     ----->>>>>     WALK_INTO_THIS_PROPERTY - iWalkInStage = WIS_WAIT_FOR_LOAD - A")
				ELIF HAS_NET_TIMER_EXPIRED(iWalkInTimer, WALK_IN_TIMEOUT,TRUE)
					//SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_OpenDoorArmIK, FALSE) 
					iWalkInStage = WIS_WAIT_FOR_LOAD
					CDEBUG1LN(DEBUG_SAFEHOUSE, "     ----->>>>>     WALK_INTO_THIS_PROPERTY - iWalkInStage = WIS_WAIT_FOR_LOAD - B")
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "     ----->>>>>     WALK_INTO_THIS_PROPERTY - PROCESSING...")
				ENDIF
			ENDIF
		BREAK
		
		CASE WIS_WAIT_FOR_LOAD
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			IF globalPropertyEntryData.bWalkInSeqDone = FALSE	// STAY ON HERE FOR NOW - CONOR WILL DO CLEANUP ONCE HE IS READY
				globalPropertyEntryData.bWalkInSeqDone = TRUE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "     ----->>>>>     WALK_INTO_THIS_PROPERTY - globalPropertyEntryData.bWalkInSeqDone = TRUE")
			ENDIF
			
			#IF ACTIVATE_PROPERTY_CS_DEBUG
			IF rag_net_realty_1_scene.bEnableTool
				iWalkInStage = WIS_CLEANUP
				CDEBUG1LN(DEBUG_SAFEHOUSE, "     ----->>>>>     WALK_INTO_THIS_PROPERTY - iWalkInStage = WIS_CLEANUP - D")
			ENDIF
			#ENDIF
			
		BREAK
		
		CASE WIS_CLEANUP
			WALK_IN_CLEANUP(TRUE)
			RETURN TRUE
		BREAK
		
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Controls the player walking into a property through a door
FUNC BOOL WALK_INTO_PROPERTY_CUTSCENE(BOOL bResetCoords)
//	IF IS_PROPERTY_MISSING_OPENING_DOORS(iCurrentPropertyID)
//		IF NOT IS_BIT_SET(ilocalBS,LOCAL_BS_STARTED_FADE)
//			DO_SCREEN_FADE_OUT(500)
//			SET_BIT(ilocalBS,LOCAL_BS_STARTED_FADE)
//		ELSE
//			IF IS_SCREEN_FADED_OUT()
//				globalPropertyEntryData.bWalkInSeqDone = TRUE
//				RETURN TRUE
//			ENDIF	
//		ENDIF
//	ELSE
		STRUCT_net_realty_1_scene scene
		IF NOT Private_Get_net_realty_1_scene(GET_PROPERTY_BUILDING(iCurrentPropertyID), scene , iEntrancePlayerIsUsing )
			iWalkInStage = WIS_CLEANUP
			globalPropertyEntryData.bWalkInSeqDone = TRUE
			CDEBUG1LN(DEBUG_SAFEHOUSE, "     ----->>>>>     WALK_INTO_PROPERTY - iWalkInStage = WIS_CLEANUP - C, Building = ", GET_BUILDING_ENUM_NAME(GET_PROPERTY_BUILDING(iCurrentPropertyID)))
			RETURN TRUE
		ENDIF
		
//	IF iCurrentPropertyID = PROPERTY_HIGH_APT_14
//	OR iCurrentPropertyID = PROPERTY_HIGH_APT_15
//		IF FALSE
//			CLONE_PED_TO_TARGET()
//			NETWORK_FADE_OUT_ENTITY()
//		ENDIF
//	ENDIF
	//
	
		IF WALK_INTO_THIS_PROPERTY(bResetCoords, scene)
			RETURN TRUE
		ENDIF
//	ENDIF
	RETURN FALSE
ENDFUNC


//╔═════════════════════════════════════════════════════════════════════════════╗
//║								PROPERTY TO BUY									
//╚═════════════════════════════════════════════════════════════════════════════╝

PROC MAINTAIN_FOR_SALE_BLIP()
	IF forSaleDetails.locate.fWidth != 0
	AND IS_FM_TYPE_UNLOCKED(FMMC_TYPE_CUSTOM_CAR_GARAGE)
	AND CAN_PLAYER_USE_PROPERTY()
	AND NOT IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID(),TRUE)
	AND NOT SHOULD_PROPERTY_BLIPS_BE_HIDDEN()
	AND NOT GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(PLAYER_ID())
	AND NOT IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_TIME_TRIAL)
	AND NOT IS_GLOBAL_FLAG_BIT_SET(eGLOBALFLAGBITSET_PLAYING_TUNER_ELEVATOR_PASS)
	#IF FEATURE_GEN9_EXCLUSIVE
	AND NOT IS_PLAYER_ON_MP_INTRO()
	#ENDIF
		IF IS_THERE_A_PROPERTY_TO_BUY()
		AND NOT DOES_LOCAL_PLAYER_OWN_PROPERTY_IN_BUILDING(mpProperties[iCurrentPropertyID].iBuildingID,ownedDetails)
		AND DOES_ENTITY_EXIST(forSaleSign)
			IF NOT DOES_BLIP_EXIST(forSaleBlip)
				forSaleBlip = CREATE_BLIP_FOR_COORD(forSaleDetails.vPropLoc)
				SET_BLIP_AS_SHORT_RANGE(forSaleBlip,TRUE)
				IF mpProperties[iCurrentPropertyID].iType = PROPERTY_TYPE_GARAGE
					SET_BLIP_SPRITE(forSaleBlip,RADAR_TRACE_GARAGE_FOR_SALE)
					SET_BLIP_NAME_FROM_TEXT_FILE(forSaleBlip,"MP_PROP_SALE0")
				ELIF IS_PROPERTY_OFFICE(iCurrentPropertyID)
					SET_BLIP_SPRITE(forSaleBlip,RADAR_TRACE_PROPERTY_FOR_SALE)
					SET_BLIP_NAME_FROM_TEXT_FILE(forSaleBlip,"MP_PROP_SALE2")
				ELSE
					SET_BLIP_SPRITE(forSaleBlip,RADAR_TRACE_PROPERTY_FOR_SALE)
					SET_BLIP_NAME_FROM_TEXT_FILE(forSaleBlip,"MP_PROP_SALE1")
				ENDIF
				SET_BLIP_PRIORITY(forSaleBlip,BLIPPRIORITY_LOW_MED)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_FOR_SALE_BLIP: adding blip for property")
			ENDIF
		ELSE
			IF DOES_BLIP_EXIST(forSaleBlip)
				REMOVE_BLIP(forSaleBlip)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_FOR_SALE_BLIP: removing blip for property")
			ENDIF
		ENDIF
	ELSE
		IF DOES_BLIP_EXIST(forSaleBlip)
			REMOVE_BLIP(forSaleBlip)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_FOR_SALE_BLIP: removing blip for property")
			IF IS_GLOBAL_FLAG_BIT_SET(eGLOBALFLAGBITSET_PLAYING_TUNER_ELEVATOR_PASS)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_FOR_SALE_BLIP: removed because on elevator pass mission")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_WARP_WITH_DRIVER()
	VEHICLE_INDEX theVeh
	PED_INDEX vehiclePed
	PLAYER_INDEX vehiclePlayer
	INT i, iSeat
	BOOL bPlayerInVehicleDoingDropOff
	CLEAR_BIT(iLocalBS2,LOCAL_BS2_PlayerInVehicleDoingHeistDelivery)
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		AND NOT IS_PLAYER_GETTING_IN_OR_OUT_OF_VEHICLE(PLAYER_ID())
			theVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			IF IS_VEHICLE_DRIVEABLE(theVeh)
				IF NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_DOING_HEIST_DROPOFF)
				AND (Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID())
				OR Is_Player_Currently_On_MP_Heist(PLAYER_ID()))
					FOR iSeat = ENUM_TO_INT(VS_DRIVER) TO ENUM_TO_INT(VS_EXTRA_RIGHT_3)
						IF NOT IS_VEHICLE_SEAT_FREE(theVeh, INT_TO_ENUM(VEHICLE_SEAT, iSeat),TRUE)
							vehiclePed = GET_PED_IN_VEHICLE_SEAT(theVeh,INT_TO_ENUM(VEHICLE_SEAT, iSeat),TRUE)
							IF NOT IS_PED_INJURED(vehiclePed)
								IF IS_PED_A_PLAYER(vehiclePed)
									vehiclePlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(vehiclePed)
									IF IS_NET_PLAYER_OK(vehiclePlayer)
										IF IS_BIT_SET(playerBD[NATIVE_TO_INT(vehiclePlayer)].iBS,PLAYER_BD_BS_DOING_HEIST_DROPOFF)
											SET_BIT(iLocalBS2,LOCAL_BS2_PlayerInVehicleDoingHeistDelivery)
											IF IS_BIT_SET(playerBD[NATIVE_TO_INT(vehiclePlayer)].iBS,PLAYER_BD_BS_CAR_IN_GARAGE_LOCATE)
												bPlayerInVehicleDoingDropOff = TRUE
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDFOR
					IF bPlayerInVehicleDoingDropOff = TRUE
						IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(),SCRIPT_TASK_LEAVE_ANY_VEHICLE) != PERFORMING_TASK
						AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(),SCRIPT_TASK_LEAVE_ANY_VEHICLE) != WAITING_TO_START_TASK
							TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
							CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_WARP_WITH_OWNER- Getting player to leave vehicle due to someone in vehicle doing a heist drop off but not local player")
							EXIT
						ENDIF
					ENDIF
				ENDIF
				vehiclePed = GET_PED_IN_VEHICLE_SEAT(theVeh)
				IF NOT IS_PED_INJURED(vehiclePed)
					IF IS_PED_A_PLAYER(vehiclePed)
						vehiclePlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(vehiclePed)
						IF vehiclePlayer != PLAYER_ID()
						AND IS_NET_PLAYER_OK(vehiclePlayer)
							IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(vehiclePlayer)].propertyDetails.iBS,PROPERTY_BROADCAST_BS_WARPING_INTO_GARAGE_CS)
							AND SHOULD_BYPASS_CHECKS_FOR_ENTRY()
								warpInWithDriverID = vehiclePlayer
								SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_WARPING_WITH_OWNER_IN_CAR)
								REPEAT mpProperties[iCurrentPropertyID].iNumEntrances i
									IF mpProperties[iCurrentPropertyID].entrance[i].iType = ENTRANCE_TYPE_GARAGE
										globalPropertyEntryData.iEntrance = i
										i = mpProperties[iCurrentPropertyID].iNumEntrances
										iEntrancePlayerIsUsing = globalPropertyEntryData.iEntrance
									ENDIF
								ENDREPEAT
								CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_WARP_WITH_OWNER: entering heist property with driver")
								NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
								SET_BIT(iLocalBS, LOCAL_BS_TAKEN_AWAY_CONTROL)
								SET_LOCAL_STAGE(STAGE_SETUP_FOR_USING_PROPERTY)
								bHadMissionCriticalEntityOnDropoff = TRUE
								PRINTLN("bHadMissionCriticalEntityOnDropoff Set True 3")
							ELSE
								IF (IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(vehiclePlayer)].propertyDetails.iBS,PROPERTY_BROADCAST_BS_WARPING_INTO_GARAGE_CS)
								OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(vehiclePlayer)].propertyDetails.iBS,PROPERTY_BROADCAST_BS_WARPING_INTO_GARAGE_FADE))
								AND GlobalplayerBD_FM[NATIVE_TO_INT(vehiclePlayer)].propertyDetails.iEnteringPropertyID > 0
								AND GET_PROPERTY_BUILDING(GlobalplayerBD_FM[NATIVE_TO_INT(vehiclePlayer)].propertyDetails.iEnteringPropertyID) = GET_PROPERTY_BUILDING(iCurrentPropertyID)
									
									IF IS_PLAYER_BLOCKED_FROM_PROPERTY(FALSE, GET_PROPERTY_TYPE_FOR_IS_PLAYER_BLOCKED_FROM_PROPERTY(iCurrentPropertyID))
									OR IS_WARNING_MESSAGE_ACTIVE()
										IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(),SCRIPT_TASK_LEAVE_ANY_VEHICLE) != PERFORMING_TASK
										AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(),SCRIPT_TASK_LEAVE_ANY_VEHICLE) != WAITING_TO_START_TASK
											TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
											CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_WARP_WITH_OWNER- Getting player to leave vehicle as they are blocked while driver wants to enter")
											EXIT
										ENDIF
									ELSE
										globalPropertyEntryData.iVariation = -1
										PRINTLN("AMP_MP_PROPERTY_EXT: reset variation on entry to property - 3")
										globalPropertyEntryData.ownerID = GlobalplayerBD_FM[NATIVE_TO_INT(vehiclePlayer)].propertyDetails.EnteringPropertyOwner
										IF globalPropertyEntryData.ownerID = INVALID_PLAYER_INDEX()
										OR NOT IS_NET_PLAYER_OK(globalPropertyEntryData.ownerID,FALSE)
											globalPropertyEntryData.ownerID = vehiclePlayer
											globalPropertyEntryData.ownerHandle = GET_GAMER_HANDLE_PLAYER(vehiclePlayer)
											PRINTLN("AMP_MP_PROPERTY_EXT: set owner of property entered based on owner driver has")
										ELSE
											globalPropertyEntryData.ownerHandle = GET_GAMER_HANDLE_PLAYER(globalPropertyEntryData.ownerID )
											PRINTLN("AMP_MP_PROPERTY_EXT: set owner of property entered based driver")
										ENDIF
										iCurrentPropertyID = GlobalplayerBD_FM[NATIVE_TO_INT(vehiclePlayer)].propertyDetails.iEnteringPropertyID
										GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iEnteringPropertyID = iCurrentPropertyID
										GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.EnteringPropertyOwner = globalPropertyEntryData.ownerID
										CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_WARP_WITH_OWNER:setting owner as ",NATIVE_TO_INT(globalPropertyEntryData.ownerID))
										SET_BIT(iLocalBS2,LOCAL_BS2_bEnteringAnotherPlayerProperty)
										REPEAT mpProperties[iCurrentPropertyID].iNumEntrances i
											IF mpProperties[iCurrentPropertyID].entrance[i].iType = ENTRANCE_TYPE_GARAGE
												globalPropertyEntryData.iEntrance = i
												i = mpProperties[iCurrentPropertyID].iNumEntrances
												iEntrancePlayerIsUsing = globalPropertyEntryData.iEntrance
											ENDIF
										ENDREPEAT
										CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_WARP_WITH_OWNER:setting entrance = ",iEntrancePlayerIsUsing )
										globalPropertyEntryData.iPropertyEntered = GlobalplayerBD_FM[NATIVE_TO_INT(vehiclePlayer)].propertyDetails.iEnteringPropertyID
										PRINTLN("22 globalPropertyEntryData.iPropertyEntered = ",globalPropertyEntryData.iPropertyEntered)
										DISPLAY_RADAR(FALSE)
										SET_BIT(iLocalBS2,LOCAL_BS2_DisabledRadar)
										SET_BIT(iLocalBS2,LOCAL_BS2_SetEntryData)
										warpInWithDriverID = vehiclePlayer
										SET_FOCUS_POS_AND_VEL(GET_PLAYER_COORDS(PLAYER_ID()),<<0,0,0>>)
										SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_ENTERING_A_PROPERTY)
										CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: setting PROPERTY_BROADCAST_BS_PLAYER_ENTERING_A_PROPERTY- A")
										SET_PLAYER_BROADCAST_PLAYING_PROPERTY_TRANSITION_CUTSCENE(globalPropertyEntryData.iPropertyEntered)
										SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_WARPING_WITH_OWNER_IN_CAR)
										NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
										SET_BIT(iLocalBS, LOCAL_BS_TAKEN_AWAY_CONTROL)
										DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
										
										CLEAR_TRANSITION_SESSION_DETAILS_ON_ENTRY_IF_SUITABLE(4)
										SET_LOCAL_STAGE(STAGE_SETUP_FOR_USING_PROPERTY)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
PROC CREATE_WIDGETS()
	#IF ACTIVATE_PROPERTY_CS_DEBUG
	INT iBuilding
	#ENDIF
	TEXT_LABEL_15 str = "PROP_EXT - "
	str += GET_PROPERTY_BUILDING(iCurrentPropertyID)//Private_Get_Net_Realty_Building_Name_And_Description(GET_PROPERTY_BUILDING(iCurrentPropertyID))
	
	START_WIDGET_GROUP(str)
		ADD_WIDGET_BOOL("Print full debug",db_bFullDebug)
		ADD_WIDGET_BOOL("g_bLauncherWillControlMiniMap",g_bLauncherWillControlMiniMap)
		ADD_WIDGET_BOOL("Block player BEAST",db_bBlockPlayerBeast)
		ADD_WIDGET_BOOL("Block player GEN",db_bBlockPlayerGen)
		ADD_WIDGET_BOOL("Clear player block",db_bClearBlockPlayer)
		ADD_WIDGET_BOOL("Bypass vehicle restrictions", db_bBypassVehicleRestrictions)
		START_WIDGET_GROUP("Heist Entry Debug")
			ADD_WIDGET_BOOL("bBypassChecksAllowHeistEntry",bBypassChecksAllowHeistEntry)
			ADD_WIDGET_BOOL("force walk in complete",globalPropertyEntryData.bInteriorWarpDone)
			ADD_WIDGET_BOOL("g_bPropertyDropOff",g_bPropertyDropOff)
			ADD_WIDGET_BOOL("g_bGarageDropOff",g_bGarageDropOff)
			ADD_WIDGET_BOOL("g_bGotMissionCriticalEntity",g_bGotMissionCriticalEntity)
			ADD_WIDGET_INT_SLIDER("g_iHeistPropertyID",g_iHeistPropertyID,-1,MAX_MP_PROPERTIES,1)
		STOP_WIDGET_GROUP()
		#IF ALLOW_FAKE_GARAGE_DEBUG
		ADD_WIDGET_BOOL("db_bDisplayDoorRequestState", db_bDisplayDoorRequestState)
		#ENDIF
		#IF ACTIVATE_PROPERTY_CS_DEBUG
		START_SCENETOOL_LAUNCHER("\"Walk into property\" scene editor", rag_net_realty_1_scene)
			REPEAT MP_PROPERTY_BUILDING_MAX iBuilding
				IF iBuilding = 0
					ADD_TO_WIDGET_COMBO("pick a building")
				ELSE
					ADD_TO_WIDGET_COMBO(Private_Get_Net_Realty_Building_Name_And_Description(iBuilding))
				ENDIF
			ENDREPEAT
		STOP_SCENETOOL_LAUNCHER(rag_net_realty_1_scene)
		rag_net_realty_1_scene.iLaunchSceneID = GET_PROPERTY_BUILDING(iCurrentPropertyID)
		
		START_SCENETOOL_LAUNCHER("\"Walk into garage\" scene editor", rag_net_realty_2_scene)
			REPEAT MP_PROPERTY_BUILDING_MAX iBuilding
				IF iBuilding = 0
					ADD_TO_WIDGET_COMBO("pick a building")
				ELSE
					ADD_TO_WIDGET_COMBO(Private_Get_Net_Realty_Building_Name_And_Description(iBuilding))
				ENDIF
			ENDREPEAT
		STOP_SCENETOOL_LAUNCHER(rag_net_realty_2_scene)
		rag_net_realty_2_scene.iLaunchSceneID = GET_PROPERTY_BUILDING(iCurrentPropertyID)
		
		START_SCENETOOL_LAUNCHER("\"Drive into garage\" scene editor", rag_net_realty_3_scene)
			REPEAT MP_PROPERTY_BUILDING_MAX iBuilding
				IF iBuilding = 0
					ADD_TO_WIDGET_COMBO("pick a building")
				ELSE
					ADD_TO_WIDGET_COMBO(Private_Get_Net_Realty_Building_Name_And_Description(iBuilding))
				ENDIF
			ENDREPEAT
		STOP_SCENETOOL_LAUNCHER(rag_net_realty_3_scene)
		rag_net_realty_3_scene.iLaunchSceneID = GET_PROPERTY_BUILDING(iCurrentPropertyID)
		
		START_SCENETOOL_LAUNCHER("\"Walk out of property\" scene editor", rag_net_realty_4_scene)
			REPEAT MP_PROPERTY_BUILDING_MAX iBuilding
				IF iBuilding = 0
					ADD_TO_WIDGET_COMBO("pick a building")
				ELSE
					ADD_TO_WIDGET_COMBO(Private_Get_Net_Realty_Building_Name_And_Description(iBuilding))
				ENDIF
			ENDREPEAT
		STOP_SCENETOOL_LAUNCHER(rag_net_realty_4_scene)
		rag_net_realty_4_scene.iLaunchSceneID = GET_PROPERTY_BUILDING(iCurrentPropertyID)
		
		START_SCENETOOL_LAUNCHER("\"Walk out of garage\" scene editor", rag_net_realty_5_scene)
			REPEAT MP_PROPERTY_BUILDING_MAX iBuilding
				IF iBuilding = 0
					ADD_TO_WIDGET_COMBO("pick a building")
				ELSE
					ADD_TO_WIDGET_COMBO(Private_Get_Net_Realty_Building_Name_And_Description(iBuilding))
				ENDIF
			ENDREPEAT
		STOP_SCENETOOL_LAUNCHER(rag_net_realty_5_scene)
		rag_net_realty_5_scene.iLaunchSceneID = GET_PROPERTY_BUILDING(iCurrentPropertyID)
		
		START_SCENETOOL_LAUNCHER("\"Drive out of garage\" scene editor", rag_net_realty_6_scene)
			REPEAT MP_PROPERTY_BUILDING_MAX iBuilding
				IF iBuilding = 0
					ADD_TO_WIDGET_COMBO("pick a building")
				ELSE
					ADD_TO_WIDGET_COMBO(Private_Get_Net_Realty_Building_Name_And_Description(iBuilding))
				ENDIF
			ENDREPEAT
		STOP_SCENETOOL_LAUNCHER(rag_net_realty_6_scene)
		rag_net_realty_6_scene.iLaunchSceneID = GET_PROPERTY_BUILDING(iCurrentPropertyID)
		#ENDIF
//		START_WIDGET_GROUP("Garage Cutscenes")
//			ADD_WIDGET_BOOL("Run Garage Car Enter Cutscene", bDebugLaunchGarageCarEnterCutscene)
//			ADD_WIDGET_INT_READ_ONLY("Garage Car Enter Cutscene Stage", iDebugGarageCarEnterCutsceneStage)
//			ADD_WIDGET_BOOL("Run Garage Ped Enter Cutscene", bDebugLaunchGaragePedEnterCutscene)
//			ADD_WIDGET_INT_READ_ONLY("Garage Ped Enter Cutscene Stage", iDebugGaragePedEnterCutsceneStage)
//		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Buzzers")
			ADD_WIDGET_BOOL("Test Buzzer", bDebugTestBuzzer)
			#IF ALLOW_FAKE_GARAGE_DEBUG
			ADD_WIDGET_FLOAT_SLIDER("Shapetest Height Offset", fDebugTestShapeTestHeightOffset, -2.0, 2.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("Shapetest Radius", fDebugTestShapeTestRadius, 0.0, 2.0, 0.01)
			ADD_WIDGET_BOOL("Shapetest Debug Draw", bDebugTestShapeTestDraw)
			#ENDIF
		STOP_WIDGET_GROUP()


		#IF ALLOW_FAKE_GARAGE_DEBUG
		START_WIDGET_GROUP("Misc")
			ADD_WIDGET_BOOL("Print GET_ENTITY_HEADING(PLAYER_PED_ID())", bDebugMiscPlayerHeading)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Buzzers")
			ADD_WIDGET_BOOL("Test Buzzer", bDebugTestBuzzer)
			ADD_WIDGET_FLOAT_SLIDER("Shapetest Height Offset", fDebugTestShapeTestHeightOffset, -2.0, 2.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("Shapetest Radius", fDebugTestShapeTestRadius, 0.0, 2.0, 0.01)
			ADD_WIDGET_BOOL("Shapetest Debug Draw", bDebugTestShapeTestDraw)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Vehicle Model Offset")
			ADD_WIDGET_BOOL("Output Offset", bDebugVehicleModelOffset)
		STOP_WIDGET_GROUP()
		#ENDIF
		
		IF IS_PROPERTY_OFFICE(iCurrentPropertyID)
		OR IS_PROPERTY_OFFICE_GARAGE(iCurrentPropertyID)
			START_WIDGET_GROUP("office garage")
				ADD_WIDGET_BOOL("bRecreateGarageDoors", bRecreateGarageDoors)
				ADD_WIDGET_BOOL("bGrabbedOfficeGarageDoorInterior", bGrabbedOfficeGarageDoorInterior)
				ADD_WIDGET_FLOAT_SLIDER("fDoorSlideOpenSpeed", fDoorSlideOpenSpeed, 0.0, 10.0, 0.001)
				ADD_WIDGET_FLOAT_SLIDER("fDoorSlideCloseSpeed", fDoorSlideCloseSpeed, 0.0, 10.0, 0.001)
			STOP_WIDGET_GROUP()
		ENDIF
		
		START_WIDGET_GROUP("FAKE \"Drive into garage\" scene editor")
			#IF ALLOW_FAKE_GARAGE_DEBUG
			ADD_WIDGET_BOOL("Play", bDebugFakeGarageInterp)
			ADD_WIDGET_BOOL("Pause", bDebugFakeGaragePause)
			ADD_WIDGET_BOOL("Loop", bDebugFakeGarageLoop)
			ADD_WIDGET_FLOAT_SLIDER("Phase", fDebugFakeGaragePhase, 0.0, 1.0, 0.1)
			ADD_WIDGET_STRING("")
			START_WIDGET_GROUP("Vehicle")
				ADD_WIDGET_BOOL("Set Start Point (At Car)", bDebugFakeGarageSetCar1)
				ADD_WIDGET_VECTOR_SLIDER("Start Point", vDebugFakeGaragePoint1, -10000.0, 10000.0, 0.01)
				ADD_WIDGET_VECTOR_SLIDER("Start Rotation", vDebugFakeGarageRotation1, -180.0, 180.0, 1.0)
				ADD_WIDGET_STRING("")
				ADD_WIDGET_BOOL("Set End Point (Ahead Of Car)", bDebugFakeGarageSetCar2)
				ADD_WIDGET_VECTOR_SLIDER("End Point", vDebugFakeGaragePoint2, -10000.0, 10000.0, 0.01)
				ADD_WIDGET_VECTOR_SLIDER("End Rotation", vDebugFakeGarageRotation2, -180.0, 180.0, 1.0)
				ADD_WIDGET_STRING("")
				ADD_WIDGET_INT_SLIDER("Duration", iDebugFakeGarageDuration, 0, 60000, 1)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Camera")
				ADD_WIDGET_BOOL("Enable", bDebugFakeGarageCamEnable)
				ADD_WIDGET_STRING("")
				ADD_WIDGET_BOOL("Grab Start Camera", bDebugFakeGarageCamGrab1)
				ADD_WIDGET_VECTOR_SLIDER("Coordinates", vDebugFakeGarageCamCoord1, -10000.0, 10000.0, 0.01)
				ADD_WIDGET_VECTOR_SLIDER("Rotation", vDebugFakeGarageCamRot1, -180.0, 180.0, 1.0)
				ADD_WIDGET_FLOAT_SLIDER("FOV", fDebugFakeGarageCamFov1, 1.0, 130.0, 0.1)
				ADD_WIDGET_STRING("")
				ADD_WIDGET_BOOL("Grab End Camera", bDebugFakeGarageCamGrab2)
				ADD_WIDGET_VECTOR_SLIDER("Coordinates", vDebugFakeGarageCamCoord2, -10000.0, 10000.0, 0.01)
				ADD_WIDGET_VECTOR_SLIDER("Rotation", vDebugFakeGarageCamRot2, -180.0, 180.0, 1.0)
				ADD_WIDGET_FLOAT_SLIDER("FOV", fDebugFakeGarageCamFov2, 1.0, 130.0, 0.1)
				ADD_WIDGET_BOOL("Flip Rotation (if camera rotates the wrong way around)", bDebugFakeGarageCamRotateFlip)
				ADD_WIDGET_STRING("")
				ADD_WIDGET_INT_SLIDER("Pan Duration", iDebugFakeGarageCamDuration, 0, 60000, 1)
				ADD_WIDGET_FLOAT_SLIDER("Shake", fDebugFakeGarageCamShake, 0.0, 1.0, 0.01)
				START_NEW_WIDGET_COMBO()
					ADD_TO_WIDGET_COMBO("GRAPH_TYPE_LINEAR")
					ADD_TO_WIDGET_COMBO("GRAPH_TYPE_SIN_ACCEL_DECEL")
					ADD_TO_WIDGET_COMBO("GRAPH_TYPE_ACCEL")
					ADD_TO_WIDGET_COMBO("GRAPH_TYPE_DECEL")
				STOP_WIDGET_COMBO("GRAPH_TYPE", camGraphTypeFakeGarage)
			STOP_WIDGET_GROUP()
			ADD_WIDGET_STRING("")
			ADD_WIDGET_BOOL("Debug Lines", bDebugFakeGarageRender)
			ADD_WIDGET_BOOL("Ghost Cars", bDebugFakeGarageGhostCar)
			ADD_WIDGET_BOOL("Release Player Car", bDebugFakeGarageUnfreezeCar)
			ADD_WIDGET_STRING("")
			ADD_WIDGET_BOOL("Output Script", bDebugFakeGarageOutput)			
			#ENDIF
			#IF NOT ALLOW_FAKE_GARAGE_DEBUG
			ADD_WIDGET_STRING("ALLOW_FAKE_GARAGE_DEBUG disabled")
			#ENDIF
		STOP_WIDGET_GROUP()

//		START_WIDGET_GROUP("Test door request")
//			ADD_WIDGET_BOOL("Door 0",bDebugRequestDoor[0])
//			ADD_WIDGET_BOOL("Door 1",bDebugRequestDoor[1])
//			ADD_WIDGET_BOOL("Door 2",bDebugRequestDoor[2])
//			ADD_WIDGET_BOOL("Output revolving door data", bOutputRevolvingDoorData)
//			//ADD_WIDGET_INT_SLIDER("Rotating door speed",iRotatingDoorSpeed,0,10000,1)
//		STOP_WIDGET_GROUP()
//		START_WIDGET_GROUP("blockObjectDetails")
////			ADD_WIDGET_BOOL("bTestBlockObjectDetails", bTestBlockObjectDetails)
//			
//			str = "mn_GarageModel: \""
//			IF blockObjectDetails.mn_GarageModel = DUMMY_MODEL_FOR_SCRIPT
//				str += "dummy"
//			ELSE
//				str += GET_MODEL_NAME_FOR_DEBUG(blockObjectDetails.mn_GarageModel)
//			ENDIF
//			str += "\""
//			ADD_WIDGET_STRING(str)
//			ADD_WIDGET_VECTOR_SLIDER("vGarageLoc", blockObjectDetails.vGarageLoc, -5000, 5000, 0.01)
//			ADD_WIDGET_VECTOR_SLIDER("vGarageRot", blockObjectDetails.vGarageRot, -180, 180, 1.0)
//			
//			str = "mn_EntryModel: \""
//			IF blockObjectDetails.mn_EntryModel = DUMMY_MODEL_FOR_SCRIPT
//				str += "dummy"
//			ELSE
//				str += GET_MODEL_NAME_FOR_DEBUG(blockObjectDetails.mn_EntryModel)
//			ENDIF
//			str += "\""
//			ADD_WIDGET_STRING(str)
//			ADD_WIDGET_VECTOR_SLIDER("vEntryLoc", blockObjectDetails.vEntryLoc, -5000, 5000, 0.01)
//			ADD_WIDGET_VECTOR_SLIDER("vEntryRot", blockObjectDetails.vEntryRot, -180, 180, 1.0)
//		STOP_WIDGET_GROUP()
		
			IF IS_PROPERTY_OFFICE(iCurrentPropertyID)
				CREATE_HELI_DOCK_DEBUG_WIDGETS(g_ExteriorHeliDockData)
			ENDIF
		
	STOP_WIDGET_GROUP()
ENDPROC

PROC MAINTAIN_DEBUG()
//	IF db_bRequestDoor[0]
//		SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, PLAYER_BD_BS_REQUEST_USE_OF_DOOR0)
//		IF HAS_PLAYER_FINISHED_DOOR_ENTER_ANIM(0)
//			db_bRequestDoor[0] = FALSE
//			CLEAR_BIT(iLocalBS, LOCAL_BS_BUZZER_GIVE_GOTO_TASK)	
//			CLEAR_BIT(iLocalBS, LOCAL_BS_BUZZER_GIVE_RING_TASK)	
//		ENDIF
//	ELSE
//		CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, PLAYER_BD_BS_REQUEST_USE_OF_DOOR0)
//	ENDIF
//	
//	IF db_bRequestDoor[1]
//		SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, PLAYER_BD_BS_REQUEST_USE_OF_DOOR1)
//	ELSE
//		CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, PLAYER_BD_BS_REQUEST_USE_OF_DOOR1)
//	ENDIF
//	
//	IF db_bRequestDoor[2]
//		SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, PLAYER_BD_BS_REQUEST_USE_OF_DOOR2)
//	ELSE
//		CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, PLAYER_BD_BS_REQUEST_USE_OF_DOOR2)
//	ENDIF
	
//	iDebugGarageCarEnterCutsceneStage = ENUM_TO_INT(garageCarEnterCutStage)
//	IF bDebugLaunchGarageCarEnterCutscene
//		bDebugSeenGarageCarEnterCutscene = TRUE
//		DO_GARAGE_CAR_ENTER_CUTSCENE(FALSE)
//	ELSE
//		IF bDebugSeenGarageCarEnterCutscene
//			RESET_GARAGE_CAR_ENTER_CUTSCENE(TRUE)
//		ENDIF
//	ENDIF
//	
//	iDebugGaragePedEnterCutsceneStage = ENUM_TO_INT(garagePedEnterCutStage)
//	IF bDebugLaunchGaragePedEnterCutscene
//		bDebugSeenGaragePedEnterCutscene = TRUE
//		DO_GARAGE_PED_ENTER_CUTSCENE(FALSE)
//	ELSE
//		IF bDebugSeenGaragePedEnterCutscene
//			RESET_GARAGE_PED_ENTER_CUTSCENE(TRUE)
//		ENDIF
//	ENDIF
	IF db_bBlockPlayerBeast
		SET_PLAYER_BLOCK_FROM_PROPERTY_FLAG(PPAF_FREEMODE_EVENT_BEAST)
		db_bBlockPlayerBeast = FALSE
	ENDIF
	
	IF db_bBlockPlayerGen
		SET_PLAYER_BLOCK_FROM_PROPERTY_FLAG(PPAF_GEN)
		db_bBlockPlayerGen = FALSE
	ENDIF
	
	IF db_bClearBlockPlayer
		CLEAR_PLAYER_BLOCK_FROM_PROPERTY_FLAG(PPAF_FREEMODE_EVENT_BEAST)
		CLEAR_PLAYER_BLOCK_FROM_PROPERTY_FLAG(PPAF_GEN)
		db_bClearBlockPlayer = FALSE
	ENDIF
	
	
	// Edit net_realty_1_scene
	#IF ACTIVATE_PROPERTY_CS_DEBUG
	IF rag_net_realty_1_scene.bEnableTool
		
//		SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, PLAYER_BD_BS_REQUEST_USE_OF_DOOR0)
//		SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, PLAYER_BD_BS_REQUEST_USE_OF_DOOR1)
//		SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, PLAYER_BD_BS_REQUEST_USE_OF_DOOR2)
		
		Private_Edit_net_realty_1_scene(rag_net_realty_1_scene, rag_net_realty_1_scene.iLaunchSceneID, &WALK_INTO_THIS_PROPERTY,entryBlockingObj,garageBlockingObj)
		rag_net_realty_1_scene.bEnableTool = FALSE
	ENDIF
	IF rag_net_realty_2_scene.bEnableTool
		
//		SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, PLAYER_BD_BS_REQUEST_USE_OF_DOOR0)
//		SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, PLAYER_BD_BS_REQUEST_USE_OF_DOOR1)
//		SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, PLAYER_BD_BS_REQUEST_USE_OF_DOOR2)
		
		Private_Edit_net_realty_2_scene(rag_net_realty_2_scene, rag_net_realty_2_scene.iLaunchSceneID, &DO_GARAGE_PED_ENTER_THIS_CUTSCENE,entryBlockingObj,garageBlockingObj)
		rag_net_realty_2_scene.bEnableTool = FALSE
	ENDIF
	IF rag_net_realty_3_scene.bEnableTool
		
//		SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, PLAYER_BD_BS_REQUEST_USE_OF_DOOR0)
//		SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, PLAYER_BD_BS_REQUEST_USE_OF_DOOR1)
//		SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, PLAYER_BD_BS_REQUEST_USE_OF_DOOR2)
		
		Private_Edit_net_realty_3_scene(rag_net_realty_3_scene, rag_net_realty_3_scene.iLaunchSceneID, &DO_GARAGE_CAR_ENTER_THIS_CUTSCENE,entryBlockingObj,garageBlockingObj)
		rag_net_realty_3_scene.bEnableTool = FALSE
	ENDIF
	IF rag_net_realty_4_scene.bEnableTool
		
//		SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, PLAYER_BD_BS_REQUEST_USE_OF_DOOR0)
//		SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, PLAYER_BD_BS_REQUEST_USE_OF_DOOR1)
//		SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, PLAYER_BD_BS_REQUEST_USE_OF_DOOR2)
		
		SCRIPT_TIMER iWalkOutCamEndTimer
		
		Private_Edit_NET_REALTY_4_SCENE(rag_net_realty_4_scene, rag_net_realty_4_scene.iLaunchSceneID,
				iCurrentPropertyID, propertyCam0, propertyCam1,
				iWalkInTimer, iCameraTimer, iWalkOutCamEndTimer,
				entryBlockingObj,garageBlockingObj)
		rag_net_realty_4_scene.bEnableTool = FALSE
		
	ENDIF
	IF rag_net_realty_5_scene.bEnableTool
		
//		SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, PLAYER_BD_BS_REQUEST_USE_OF_DOOR0)
//		SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, PLAYER_BD_BS_REQUEST_USE_OF_DOOR1)
//		SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, PLAYER_BD_BS_REQUEST_USE_OF_DOOR2)
		
		Private_Edit_NET_REALTY_5_SCENE(rag_net_realty_5_scene, rag_net_realty_5_scene.iLaunchSceneID,iCurrentPropertyID,entryBlockingObj,garageBlockingObj)
		rag_net_realty_5_scene.bEnableTool = FALSE
	ENDIF
	IF rag_net_realty_6_scene.bEnableTool
		
//		SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, PLAYER_BD_BS_REQUEST_USE_OF_DOOR0)
//		SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, PLAYER_BD_BS_REQUEST_USE_OF_DOOR1)
//		SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, PLAYER_BD_BS_REQUEST_USE_OF_DOOR2)
		
		Private_Edit_NET_REALTY_6_SCENE(rag_net_realty_6_scene, iCurrentPropertyID, rag_net_realty_6_scene.iLaunchSceneID, propertyDoors[2].iDoorHash,entryBlockingObj,garageBlockingObj)
		rag_net_realty_6_scene.bEnableTool = FALSE
	ENDIF
	#ENDIF
	#IF ALLOW_FAKE_GARAGE_DEBUG
	IF bDebugMiscPlayerHeading
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: GET_ENTITY_HEADING(PLAYER_PED_ID()) = ", GET_ENTITY_HEADING(PLAYER_PED_ID()))
	ENDIF
	IF bDebugVehicleModelOffset
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			VEHICLE_INDEX vehIndex = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			
			IF IS_VEHICLE_DRIVEABLE(vehIndex)
				VECTOR vVehicleCoords = GET_ENTITY_COORDS(vehIndex)
				VECTOR vVehicleDimensionsMin, vVehicleDimensionsMax
				
				GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(vehIndex), vVehicleDimensionsMin, vVehicleDimensionsMax)
				
				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Vehicle Model Offset - ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(vehIndex)))
				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Vehicle Model Offset - GET_ENTITY_COORDS(<<", vVehicleCoords.X, ", ", vVehicleCoords.Y, ", ", vVehicleCoords.Z, ">>)")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Vehicle Model Offset - Dimension Z Min = ", vVehicleDimensionsMin.Z)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Vehicle Model Offset - Z Offset = ", vVehicleDimensionsMin.Z - vVehicleCoords.Z)
				
				SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("AM_MP_PROPERTY_EXT: Vehicle Model Offset - ")
				SAVE_STRING_TO_DEBUG_FILE(GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(vehIndex)))
				SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("AM_MP_PROPERTY_EXT: Vehicle Model Offset - GET_ENTITY_COORDS(<<")
				SAVE_FLOAT_TO_DEBUG_FILE(vVehicleCoords.X)
				SAVE_STRING_TO_DEBUG_FILE(", ")
				SAVE_FLOAT_TO_DEBUG_FILE(vVehicleCoords.Y)
				SAVE_STRING_TO_DEBUG_FILE(", ")
				SAVE_FLOAT_TO_DEBUG_FILE(vVehicleCoords.Z)
				SAVE_STRING_TO_DEBUG_FILE(">>)")
				SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("AM_MP_PROPERTY_EXT: Vehicle Model Offset - Dimension Z Min = ")
				SAVE_FLOAT_TO_DEBUG_FILE(vVehicleDimensionsMin.Z)
				SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("AM_MP_PROPERTY_EXT: Vehicle Model Offset - Z Offset = ")
				SAVE_FLOAT_TO_DEBUG_FILE(vVehicleDimensionsMin.Z - vVehicleCoords.Z)
				SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_NEWLINE_TO_DEBUG_FILE()
			ENDIF
		ENDIF
		
		bDebugVehicleModelOffset = FALSE
	ENDIF
	
	IF bDebugTestShapeTestDraw
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
		
		INT i
		
		VECTOR vColour
		
		VECTOR vOffset
		
		REPEAT 5 i	CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: ShapeTestDraw [i=", i, "]")
			IF iBuzzerWalkAwayShapeTest = i
				vColour = <<255.0, 0.0, 0.0>>
			ELSE
				vColour = <<0.0, 0.0, 255.0>>
			ENDIF
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: ShapeTestDraw [vColour=", vColour.X, ", ", vColour.Y, ", ", vColour.Z, "]")
			IF i = 0
				vOffset = <<0.0, -4.0, 0.0>>
			ELIF i = 1
				vOffset = <<-2.83, -2.83, 0.0>>
			ELIF i = 2
				vOffset = <<2.83, -2.83, 0.0>>
			ELIF i = 3
				vOffset = <<-4.0, 0.0, 0.0>>
			ELIF i = 4
				vOffset = <<4.0, 0.0, 0.0>>
			ELSE
				vOffset = <<0.0, 0.0, 0.0>>
			ENDIF
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: ShapeTestDraw [vOffset=<<", vOffset.X, ", ", vOffset.Y, ", ", vOffset.Z, ">>]")
			IF NOT ARE_VECTORS_EQUAL(vOffset, <<0.0, 0.0, 0.0>>)
				VECTOR vNearOffset
				VECTOR vFarOffset
				FLOAT fNearVerticalOffset
				FLOAT fFarVerticalOffset
				
				vNearOffset = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), vOffset / 4.0) + <<0.0, 0.0, fDebugTestShapeTestHeightOffset>>
				vFarOffset = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), vOffset) + <<0.0, 0.0, fDebugTestShapeTestHeightOffset>>
				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: ShapeTestDraw [vNearOffset=<<", vNearOffset.X, ", ", vNearOffset.Y, ", ", vNearOffset.Z, ">>]")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: ShapeTestDraw [vFarOffset=<<", vFarOffset.X, ", ", vFarOffset.Y, ", ", vFarOffset.Z, ">>]")
				fNearVerticalOffset = 0.0
				fFarVerticalOffset = 0.0
				
				GET_GROUND_Z_FOR_3D_COORD(vNearOffset + <<0.0, 0.0, 1.0>>, fNearVerticalOffset)
				IF vNearOffset.Z < fNearVerticalOffset
					fNearVerticalOffset = 0.0
				ENDIF
				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: ShapeTestDraw [fNearVerticalOffset=", fNearVerticalOffset, "]")
				GET_GROUND_Z_FOR_3D_COORD(vFarOffset + <<0.0, 0.0, 1.0>>, fFarVerticalOffset)
				IF vFarOffset.Z < fFarVerticalOffset
					fFarVerticalOffset = 0.0
				ENDIF
				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: ShapeTestDraw [fFarVerticalOffset=", fFarVerticalOffset, "]")
				
				IF fNearVerticalOffset <> 0.0 AND fFarVerticalOffset <> 0.0 AND ABSF(fFarVerticalOffset - fNearVerticalOffset) < 2.0
					DRAW_DEBUG_SPHERE(<<vNearOffset.X, vNearOffset.Y, fNearVerticalOffset + 1.0>>, fDebugTestShapeTestRadius, ROUND(vColour.X), ROUND(vColour.Y), ROUND(vColour.Z))
					DRAW_DEBUG_SPHERE(<<vFarOffset.X, vFarOffset.Y, fFarVerticalOffset + 1.0>>, fDebugTestShapeTestRadius, ROUND(vColour.X), ROUND(vColour.Y), ROUND(vColour.Z))
					DRAW_DEBUG_LINE(<<vNearOffset.X, vNearOffset.Y, fNearVerticalOffset + 1.0>>, <<vFarOffset.X, vFarOffset.Y, fFarVerticalOffset + 1.0>>, ROUND(vColour.X), ROUND(vColour.Y), ROUND(vColour.Z))
				ENDIF
			ELSE
				DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(PLAYER_PED_ID()), fDebugTestShapeTestRadius, 255, 0, 0)
			ENDIF
		ENDREPEAT
	ENDIF
	
	IF bDebugFakeGarageInterp
		BOOL bCleanupInterp = FALSE
		IF fDebugFakeGarageStartTime = -1
			fDebugFakeGarageStartTime = TO_FLOAT(GET_GAME_TIMER())
			
			IF bDebugFakeGarageCamEnable
				IF NOT DOES_CAM_EXIST(camFakeGarage)
					camFakeGarage = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, vDebugFakeGarageCamCoord1, vDebugFakeGarageCamRot1, fDebugFakeGarageCamFov1)
					//SET_CAM_PARAMS(camFakeGarage, vDebugFakeGarageCamCoord2, vDebugFakeGarageCamRot2, fDebugFakeGarageCamFov2, iDebugFakeGarageCamDuration, INT_TO_ENUM(CAMERA_GRAPH_TYPE, camGraphTypeFakeGarage), INT_TO_ENUM(CAMERA_GRAPH_TYPE, camGraphTypeFakeGarage))
					SHAKE_CAM(camFakeGarage, "HAND_SHAKE", fDebugFakeGarageCamShake)
					SET_CAM_SHAKE_AMPLITUDE(camFakeGarage, fDebugFakeGarageCamShake)
					SET_CAM_ACTIVE(camFakeGarage, TRUE)
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
				ELSE
					SET_CAM_PARAMS(camFakeGarage, vDebugFakeGarageCamCoord1, vDebugFakeGarageCamRot1, fDebugFakeGarageCamFov1)
					//SET_CAM_PARAMS(camFakeGarage, vDebugFakeGarageCamCoord2, vDebugFakeGarageCamRot2, fDebugFakeGarageCamFov2, iDebugFakeGarageCamDuration, INT_TO_ENUM(CAMERA_GRAPH_TYPE, camGraphTypeFakeGarage), INT_TO_ENUM(CAMERA_GRAPH_TYPE, camGraphTypeFakeGarage))
					SHAKE_CAM(camFakeGarage, "HAND_SHAKE", fDebugFakeGarageCamShake)
					SET_CAM_SHAKE_AMPLITUDE(camFakeGarage, fDebugFakeGarageCamShake)
					SET_CAM_ACTIVE(camFakeGarage, TRUE)
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
				ENDIF
			ENDIF
		ELIF TO_FLOAT(GET_GAME_TIMER()) > fDebugFakeGarageStartTime + TO_FLOAT(iDebugFakeGarageDuration)
		AND bDebugFakeGaragePause = FALSE
		AND TO_FLOAT(GET_GAME_TIMER()) > fDebugFakeGarageStartTime + TO_FLOAT(iDebugFakeGarageCamDuration)
			bCleanupInterp = TRUE
		ELSE
//			SET_GLOBAL_POSITION_OFFSET_FOR_RECORDED_VEHICLE_PLAYBACK
//			SET_ADDITIONAL_ROTATION_FOR_RECORDED_VEHICLE_PLAYBACK
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				VEHICLE_INDEX vehIndex = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				IF NOT IS_ENTITY_DEAD(vehIndex)
					SET_ENTITY_COORDS_NO_OFFSET(vehIndex, GET_INTERP_POINT_VECTOR(vDebugFakeGaragePoint1, vDebugFakeGaragePoint2, fDebugFakeGarageStartTime, fDebugFakeGarageStartTime + TO_FLOAT(iDebugFakeGarageDuration), CLAMP(TO_FLOAT(GET_GAME_TIMER()), fDebugFakeGarageStartTime, fDebugFakeGarageStartTime + TO_FLOAT(iDebugFakeGarageDuration))))
					SET_ENTITY_ROTATION(vehIndex, GET_INTERP_POINT_VECTOR(vDebugFakeGarageRotation1, vDebugFakeGarageRotation2, fDebugFakeGarageStartTime, fDebugFakeGarageStartTime + TO_FLOAT(iDebugFakeGarageDuration), CLAMP(TO_FLOAT(GET_GAME_TIMER()), fDebugFakeGarageStartTime, fDebugFakeGarageStartTime + TO_FLOAT(iDebugFakeGarageDuration))))
					SET_ENTITY_COLLISION(vehIndex, FALSE)
					FREEZE_ENTITY_POSITION(vehIndex, TRUE)
					SET_VEHICLE_ENGINE_ON(vehIndex, TRUE, TRUE)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Setting car position in debug")
					
					IF DOES_CAM_EXIST(camFakeGarage)
						FLOAT fPhase
						
						IF INT_TO_ENUM(CAMERA_GRAPH_TYPE, camGraphTypeFakeGarage) = GRAPH_TYPE_LINEAR
							fPhase = (1.0 / TO_FLOAT(iDebugFakeGarageCamDuration)) * CLAMP(TO_FLOAT(GET_GAME_TIMER()) - fDebugFakeGarageStartTime, 0.0, TO_FLOAT(iDebugFakeGarageCamDuration))
						ELIF INT_TO_ENUM(CAMERA_GRAPH_TYPE, camGraphTypeFakeGarage) = GRAPH_TYPE_SIN_ACCEL_DECEL
							fPhase = GET_GRAPH_TYPE_SIN_ACCEL_DECEL(fDebugFakeGaragePhase)
						ELIF INT_TO_ENUM(CAMERA_GRAPH_TYPE, camGraphTypeFakeGarage) = GRAPH_TYPE_ACCEL
							fPhase = GET_GRAPH_TYPE_ACCEL(fDebugFakeGaragePhase)
						ELIF INT_TO_ENUM(CAMERA_GRAPH_TYPE, camGraphTypeFakeGarage) = GRAPH_TYPE_DECEL
							fPhase = GET_GRAPH_TYPE_DECEL(fDebugFakeGaragePhase)
						ENDIF
						
						SET_CAM_ACTIVE(camFakeGarage, TRUE)
						
						SET_CAM_PARAMS(camFakeGarage, 
										GET_INTERP_POINT_VECTOR(vDebugFakeGarageCamCoord1, vDebugFakeGarageCamCoord2, 0.0, 1.0, fPhase), 
										GET_INTERP_POINT_VECTOR(vDebugFakeGarageCamRot1, vDebugFakeGarageCamRot2, 0.0, 1.0, fPhase), 
										GET_INTERP_POINT_FLOAT(fDebugFakeGarageCamFov1, fDebugFakeGarageCamFov2, 0.0, 1.0, fPhase))
					ENDIF
				ELSE
					bCleanupInterp = TRUE
				ENDIF
			ELSE
				bCleanupInterp = TRUE
			ENDIF
		ENDIF
		IF bCleanupInterp
			fDebugFakeGarageStartTime = -1
			bDebugFakeGarageInterp = FALSE
		ENDIF
	ENDIF
	IF bDebugFakeGaragePause
		fDebugFakeGarageStartTime = TO_FLOAT(GET_GAME_TIMER()) - fDebugFakeGaragePauseTime
		IF fDebugFakeGaragePauseTime <> (TO_FLOAT(iDebugFakeGarageDuration) * fDebugFakeGaragePhase)
			IF iDebugFakeGarageDuration >= iDebugFakeGarageCamDuration
				fDebugFakeGaragePauseTime = (TO_FLOAT(iDebugFakeGarageDuration) * fDebugFakeGaragePhase)
			ELSE
				fDebugFakeGaragePauseTime = (TO_FLOAT(iDebugFakeGarageCamDuration) * fDebugFakeGaragePhase)
			ENDIF
		ENDIF
	ELSE
		IF iDebugFakeGarageDuration >= iDebugFakeGarageCamDuration
			fDebugFakeGaragePhase = (1.0 / TO_FLOAT(iDebugFakeGarageDuration)) * CLAMP(TO_FLOAT(GET_GAME_TIMER()) - fDebugFakeGarageStartTime, 0.0, TO_FLOAT(iDebugFakeGarageDuration))
		ELSE
			fDebugFakeGaragePhase = (1.0 / TO_FLOAT(iDebugFakeGarageCamDuration)) * CLAMP(TO_FLOAT(GET_GAME_TIMER()) - fDebugFakeGarageStartTime, 0.0, TO_FLOAT(iDebugFakeGarageCamDuration))
		ENDIF
		IF GET_GAME_TIMER() < fDebugFakeGarageStartTime + TO_FLOAT(iDebugFakeGarageDuration)
			IF iDebugFakeGarageDuration >= iDebugFakeGarageCamDuration
				fDebugFakeGaragePauseTime = (TO_FLOAT(iDebugFakeGarageDuration) * fDebugFakeGaragePhase)
			ELSE
				fDebugFakeGaragePauseTime = (TO_FLOAT(iDebugFakeGarageCamDuration) * fDebugFakeGaragePhase)
			ENDIF
		ENDIF
	ENDIF
	IF bDebugFakeGarageLoop
		IF bDebugFakeGarageInterp = FALSE
			bDebugFakeGarageInterp = TRUE
		ENDIF
	ENDIF
	IF bDebugFakeGarageSetCar1
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			VEHICLE_INDEX vehIndex = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			IF NOT IS_ENTITY_DEAD(vehIndex)
				vDebugFakeGaragePoint1 = GET_ENTITY_COORDS(vehIndex)
				vDebugFakeGarageRotation1 = GET_ENTITY_ROTATION(vehIndex)
			ENDIF
		ENDIF
		
		bDebugFakeGarageSetCar1 = FALSE
	ENDIF
	IF bDebugFakeGarageSetCar2
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			VEHICLE_INDEX vehIndex = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			IF NOT IS_ENTITY_DEAD(vehIndex)
				vDebugFakeGaragePoint2 = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehIndex, <<0.0, 10.0, 0.0>>)
				vDebugFakeGarageRotation2 = GET_ENTITY_ROTATION(vehIndex)
			ENDIF
		ENDIF
		
		bDebugFakeGarageSetCar2 = FALSE
	ENDIF
	IF bDebugFakeGarageRender
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
		
		DRAW_DEBUG_SPHERE(vDebugFakeGaragePoint1, 0.5)
		DRAW_DEBUG_SPHERE(vDebugFakeGaragePoint2, 0.5)
		DRAW_DEBUG_LINE(vDebugFakeGaragePoint1, vDebugFakeGaragePoint2)
	ENDIF
	IF bDebugFakeGarageGhostCar
		BOOL bCleanupGhostCar = FALSE
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			VEHICLE_INDEX vehIndex = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			MODEL_NAMES vehModel = GET_ENTITY_MODEL(vehIndex)
			IF NOT IS_ENTITY_DEAD(vehIndex)
				IF NOT DOES_ENTITY_EXIST(vehGhost1)
				OR NOT DOES_ENTITY_EXIST(vehGhost2)
					REQUEST_MODEL(vehModel)
					IF HAS_MODEL_LOADED(vehModel)
						IF NOT DOES_ENTITY_EXIST(vehGhost1)
							vehGhost1 = CREATE_VEHICLE(vehModel, vDebugFakeGaragePoint1, vDebugFakeGarageRotation1.Z)
							FREEZE_ENTITY_POSITION(vehGhost1, TRUE)
							SET_ENTITY_COLLISION(vehGhost1, FALSE)
							SET_ENTITY_INVINCIBLE(vehGhost1, TRUE)
						ENDIF
						IF NOT DOES_ENTITY_EXIST(vehGhost2)
							vehGhost2 = CREATE_VEHICLE(vehModel, vDebugFakeGaragePoint2, vDebugFakeGarageRotation2.Z)
							FREEZE_ENTITY_POSITION(vehGhost2, TRUE)
							SET_ENTITY_COLLISION(vehGhost2, FALSE)
							SET_ENTITY_INVINCIBLE(vehGhost2, TRUE)
						ENDIF
						SET_MODEL_AS_NO_LONGER_NEEDED(vehModel)
					ENDIF
				ELSE
					IF GET_ENTITY_MODEL(vehGhost1) = vehModel
						SET_ENTITY_COORDS_NO_OFFSET(vehGhost1, vDebugFakeGaragePoint1)
						SET_ENTITY_ROTATION(vehGhost1, vDebugFakeGarageRotation1)
					ELSE
						bCleanupGhostCar = TRUE
					ENDIF
					
					IF GET_ENTITY_MODEL(vehGhost2) = vehModel
						SET_ENTITY_COORDS_NO_OFFSET(vehGhost2, vDebugFakeGaragePoint2)
						SET_ENTITY_ROTATION(vehGhost2, vDebugFakeGarageRotation2)
					ELSE
						bCleanupGhostCar = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		IF bCleanupGhostCar
			IF DOES_ENTITY_EXIST(vehGhost1)
				DELETE_VEHICLE(vehGhost1)
			ENDIF
			IF DOES_ENTITY_EXIST(vehGhost2)
				DELETE_VEHICLE(vehGhost2)
			ENDIF
		ENDIF
	ELSE
		IF DOES_ENTITY_EXIST(vehGhost1)
			DELETE_VEHICLE(vehGhost1)
		ENDIF
		IF DOES_ENTITY_EXIST(vehGhost2)
			DELETE_VEHICLE(vehGhost2)
		ENDIF
	ENDIF
	IF bDebugFakeGarageOutput
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("--- AM MP PROPERTY EXT - ")
		SAVE_INT_TO_DEBUG_FILE(mpProperties[iCurrentPropertyID].iBuildingID)
		SAVE_STRING_TO_DEBUG_FILE(" ---")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("fakeGarageDriveData.vStartPoint = <<")
		SAVE_FLOAT_TO_DEBUG_FILE(vDebugFakeGaragePoint1.X)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_FLOAT_TO_DEBUG_FILE(vDebugFakeGaragePoint1.Y)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_FLOAT_TO_DEBUG_FILE(vDebugFakeGaragePoint1.Z)
		SAVE_STRING_TO_DEBUG_FILE(">>")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("fakeGarageDriveData.vStartRotation = <<")
		SAVE_FLOAT_TO_DEBUG_FILE(vDebugFakeGarageRotation1.X)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_FLOAT_TO_DEBUG_FILE(vDebugFakeGarageRotation1.Y)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_FLOAT_TO_DEBUG_FILE(vDebugFakeGarageRotation1.Z)
		SAVE_STRING_TO_DEBUG_FILE(">>")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("fakeGarageDriveData.vEndPoint = <<")
		SAVE_FLOAT_TO_DEBUG_FILE(vDebugFakeGaragePoint2.X)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_FLOAT_TO_DEBUG_FILE(vDebugFakeGaragePoint2.Y)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_FLOAT_TO_DEBUG_FILE(vDebugFakeGaragePoint2.Z)
		SAVE_STRING_TO_DEBUG_FILE(">>")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("fakeGarageDriveData.vEndRotation = <<")
		SAVE_FLOAT_TO_DEBUG_FILE(vDebugFakeGarageRotation2.X)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_FLOAT_TO_DEBUG_FILE(vDebugFakeGarageRotation2.Y)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_FLOAT_TO_DEBUG_FILE(vDebugFakeGarageRotation2.Z)
		SAVE_STRING_TO_DEBUG_FILE(">>")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("fakeGarageDriveData.fDuration = ")
		SAVE_INT_TO_DEBUG_FILE(iDebugFakeGarageDuration)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		IF bDebugFakeGarageCamEnable
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("fakeGarageDriveData.vCamCoord1 = <<")
		SAVE_FLOAT_TO_DEBUG_FILE(vDebugFakeGarageCamCoord1.X)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_FLOAT_TO_DEBUG_FILE(vDebugFakeGarageCamCoord1.Y)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_FLOAT_TO_DEBUG_FILE(vDebugFakeGarageCamCoord1.Z)
		SAVE_STRING_TO_DEBUG_FILE(">>")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("fakeGarageDriveData.vCamRot1 = <<")
		SAVE_FLOAT_TO_DEBUG_FILE(vDebugFakeGarageCamRot1.X)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_FLOAT_TO_DEBUG_FILE(vDebugFakeGarageCamRot1.Y)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_FLOAT_TO_DEBUG_FILE(vDebugFakeGarageCamRot1.Z)
		SAVE_STRING_TO_DEBUG_FILE(">>")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("fakeGarageDriveData.fCamFov1 = ")
		SAVE_FLOAT_TO_DEBUG_FILE(fDebugFakeGarageCamFov1)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("fakeGarageDriveData.vCamCoord2 = <<")
		SAVE_FLOAT_TO_DEBUG_FILE(vDebugFakeGarageCamCoord2.X)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_FLOAT_TO_DEBUG_FILE(vDebugFakeGarageCamCoord2.Y)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_FLOAT_TO_DEBUG_FILE(vDebugFakeGarageCamCoord2.Z)
		SAVE_STRING_TO_DEBUG_FILE(">>")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("fakeGarageDriveData.vCamRot2 = <<")
		SAVE_FLOAT_TO_DEBUG_FILE(vDebugFakeGarageCamRot2.X)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_FLOAT_TO_DEBUG_FILE(vDebugFakeGarageCamRot2.Y)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_FLOAT_TO_DEBUG_FILE(vDebugFakeGarageCamRot2.Z)
		SAVE_STRING_TO_DEBUG_FILE(">>")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("fakeGarageDriveData.fCamFov2 = ")
		SAVE_FLOAT_TO_DEBUG_FILE(fDebugFakeGarageCamFov2)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("fakeGarageDriveData.iCamDuration = ")
		SAVE_INT_TO_DEBUG_FILE(iDebugFakeGarageCamDuration)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("fakeGarageDriveData.fCamShake = ")
		SAVE_FLOAT_TO_DEBUG_FILE(fDebugFakeGarageCamShake)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		IF INT_TO_ENUM(CAMERA_GRAPH_TYPE, camGraphTypeFakeGarage) = GRAPH_TYPE_LINEAR
			SAVE_STRING_TO_DEBUG_FILE("fakeGarageDriveData.camGraphType = GRAPH_TYPE_LINEAR")
		ELIF INT_TO_ENUM(CAMERA_GRAPH_TYPE, camGraphTypeFakeGarage) = GRAPH_TYPE_SIN_ACCEL_DECEL
			SAVE_STRING_TO_DEBUG_FILE("fakeGarageDriveData.camGraphType = GRAPH_TYPE_SIN_ACCEL_DECEL")
		ELIF INT_TO_ENUM(CAMERA_GRAPH_TYPE, camGraphTypeFakeGarage) = GRAPH_TYPE_ACCEL
			SAVE_STRING_TO_DEBUG_FILE("fakeGarageDriveData.camGraphType = GRAPH_TYPE_ACCEL")
		ELIF INT_TO_ENUM(CAMERA_GRAPH_TYPE, camGraphTypeFakeGarage) = GRAPH_TYPE_DECEL
			SAVE_STRING_TO_DEBUG_FILE("fakeGarageDriveData.camGraphType = GRAPH_TYPE_DECEL")
		ENDIF
		SAVE_NEWLINE_TO_DEBUG_FILE()
		ENDIF
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("-----------------------------")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		bDebugFakeGarageOutput = FALSE
	ENDIF
	IF bDebugFakeGarageCamGrab1
		vDebugFakeGarageCamCoord1 = GET_FINAL_RENDERED_CAM_COORD()
		vDebugFakeGarageCamRot1 = GET_FINAL_RENDERED_CAM_ROT()
		fDebugFakeGarageCamFov1 = GET_FINAL_RENDERED_CAM_FOV()
		
		bDebugFakeGarageCamGrab1 = FALSE
	ENDIF
	IF bDebugFakeGarageCamGrab2
		vDebugFakeGarageCamCoord2 = GET_FINAL_RENDERED_CAM_COORD()
		vDebugFakeGarageCamRot2 = GET_FINAL_RENDERED_CAM_ROT()
		fDebugFakeGarageCamFov2 = GET_FINAL_RENDERED_CAM_FOV()
		
		bDebugFakeGarageCamGrab2 = FALSE
	ENDIF
	IF bDebugFakeGarageCamRotateFlip
		IF vDebugFakeGarageCamRot2.Z < vDebugFakeGarageCamRot1.Z
			vDebugFakeGarageCamRot2.Z += 360.0
		ELSE
			vDebugFakeGarageCamRot2.Z -= 360.0
		ENDIF
		
		bDebugFakeGarageCamRotateFlip = FALSE
	ENDIF
	IF bDebugFakeGarageUnfreezeCar
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			VEHICLE_INDEX vehIndex = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			IF NOT IS_ENTITY_DEAD(vehIndex)
				SET_ENTITY_COORDS_NO_OFFSET(vehIndex, vDebugFakeGaragePoint1)
				SET_ENTITY_ROTATION(vehIndex, vDebugFakeGarageRotation1)
				SET_ENTITY_COLLISION(vehIndex, TRUE)
				FREEZE_ENTITY_POSITION(vehIndex, FALSE)
				SET_VEHICLE_ON_GROUND_PROPERLY(vehIndex)
			ENDIF
		ENDIF
		
		IF DOES_CAM_EXIST(camFakeGarage)
			SET_CAM_ACTIVE(camFakeGarage, FALSE)
			STOP_CAM_SHAKING(camFakeGarage, TRUE)
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			DESTROY_CAM(camFakeGarage)
		ENDIF
		
		bDebugFakeGarageUnfreezeCar = FALSE
	ENDIF
	IF NOT bDebugFakeGarageCamEnable
		IF DOES_CAM_EXIST(camFakeGarage)
			SET_CAM_ACTIVE(camFakeGarage, FALSE)
			STOP_CAM_SHAKING(camFakeGarage, TRUE)
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			DESTROY_CAM(camFakeGarage)
		ENDIF
	ENDIF
	#ENDIF
	
		IF IS_PROPERTY_OFFICE(iCurrentPropertyID)
			MAINTAIN_HELI_DOCK_DEBUG_WIDGETS(g_ExteriorHeliDockData)
		ENDIF
	
//	INT i
//	REPEAT MAX_EXTERIOR_DOORS i
//		IF bDebugRequestDoor[i]
//			SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, PLAYER_BD_BS_REQUEST_USE_OF_DOOR0+i)
//			#IF IS_DEBUG_BUILD
//			IF bOutputRevolvingDoorData
//				CDEBUG1LN(DEBUG_SAFEHOUSE, "Player is requesting server open door #",i)
//			ENDIF
//			#ENDIF
//			bDebugRequestDoorActivated[i] = TRUE
//		ELSE
//			IF bDebugRequestDoorActivated[i]
//				CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, PLAYER_BD_BS_REQUEST_USE_OF_DOOR0+i)
//				bDebugRequestDoorActivated[i] = FALSE
//				#IF IS_DEBUG_BUILD
//				IF bOutputRevolvingDoorData
//					CDEBUG1LN(DEBUG_SAFEHOUSE, "Player is clearing debug request server open door #",i)
//				ENDIF
//				#ENDIF
//			ENDIF
//		ENDIF
//	ENDREPEAT
	
//	IF bTestBlockObjectDetails
//		OBJECT_INDEX GarageObj, EntryObj
//		
//		IF blockObjectDetails.mn_GarageModel != DUMMY_MODEL_FOR_SCRIPT
//			REQUEST_MODEL(blockObjectDetails.mn_GarageModel)
//			WHILE NOT HAS_MODEL_LOADED(blockObjectDetails.mn_GarageModel)
//				WAIT(0)
//			ENDWHILE
//		ENDIF
//		IF blockObjectDetails.mn_EntryModel != DUMMY_MODEL_FOR_SCRIPT
//			REQUEST_MODEL(blockObjectDetails.mn_EntryModel)
//			WHILE NOT HAS_MODEL_LOADED(blockObjectDetails.mn_EntryModel)
//				WAIT(0)
//			ENDWHILE
//		ENDIF
//		
//		IF ARE_VECTORS_EQUAL(blockObjectDetails.vGarageLoc, <<0,0,0>>)
//			blockObjectDetails.vGarageLoc = GET_MP_PROPERTY_BUILDING_WORLD_POINT(GET_PROPERTY_BUILDING(iCurrentPropertyID))
//			blockObjectDetails.vEntryRot = GET_ENTITY_ROTATION(PLAYER_PED_ID())
//		ENDIF
//		IF ARE_VECTORS_EQUAL(blockObjectDetails.vEntryLoc, <<0,0,0>>)
//			blockObjectDetails.vEntryLoc = GET_MP_PROPERTY_BUILDING_WORLD_POINT(GET_PROPERTY_BUILDING(iCurrentPropertyID))
//			blockObjectDetails.vEntryRot = GET_ENTITY_ROTATION(PLAYER_PED_ID()) * -1.0
//		ENDIF
//		
//		IF blockObjectDetails.mn_GarageModel != DUMMY_MODEL_FOR_SCRIPT
//			GarageObj = CREATE_OBJECT_NO_OFFSET(blockObjectDetails.mn_GarageModel, blockObjectDetails.vGarageLoc,TRUE,TRUE,TRUE)
//			SET_ENTITY_ROTATION(GarageObj, blockObjectDetails.vGarageRot)
//		ENDIF
//		IF blockObjectDetails.mn_EntryModel != DUMMY_MODEL_FOR_SCRIPT
//			EntryObj = CREATE_OBJECT_NO_OFFSET(blockObjectDetails.mn_EntryModel, blockObjectDetails.vEntryLoc,TRUE,TRUE,TRUE)
//			SET_ENTITY_ROTATION(EntryObj, blockObjectDetails.vEntryRot)
//		ENDIF
//		
//		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
//		
//		WHILE bTestBlockObjectDetails
//			IF blockObjectDetails.mn_GarageModel != DUMMY_MODEL_FOR_SCRIPT
//				DRAW_DEBUG_SPHERE(blockObjectDetails.vGarageLoc, 1.0,								000, 000, 255, 255)
//				DRAW_DEBUG_LINE(blockObjectDetails.vGarageLoc, GET_ENTITY_COORDS(PLAYER_PED_ID()),	000, 000, 255, 255)
//				SET_ENTITY_COORDS(GarageObj, blockObjectDetails.vGarageLoc)
//				SET_ENTITY_ROTATION(GarageObj, blockObjectDetails.vGarageRot)
//			ENDIF
//			IF blockObjectDetails.mn_EntryModel != DUMMY_MODEL_FOR_SCRIPT
//				DRAW_DEBUG_SPHERE(blockObjectDetails.vEntryLoc, 1.0,								255, 000, 000, 255)
//				DRAW_DEBUG_LINE(blockObjectDetails.vEntryLoc, GET_ENTITY_COORDS(PLAYER_PED_ID()),	255, 000, 000, 255)
//				SET_ENTITY_COORDS(EntryObj, blockObjectDetails.vEntryLoc)
//				SET_ENTITY_ROTATION(EntryObj, blockObjectDetails.vEntryRot)
//			ENDIF
//			
//			WAIT(0)
//		ENDWHILE
//		
//		IF blockObjectDetails.mn_GarageModel != DUMMY_MODEL_FOR_SCRIPT
//			DELETE_OBJECT(GarageObj)
//		ENDIF
//		IF blockObjectDetails.mn_EntryModel != DUMMY_MODEL_FOR_SCRIPT
//			DELETE_OBJECT(EntryObj)
//		ENDIF
//		
//		OPEN_DEBUG_FILE()
//		SAVE_NEWLINE_TO_DEBUG_FILE()
//		SAVE_STRING_TO_DEBUG_FILE("		// -- -- GET_MP_PROP_BLOCKING_OBJECT_DETAILS()  -- -- -- //")SAVE_NEWLINE_TO_DEBUG_FILE()
//		
//		SAVE_STRING_TO_DEBUG_FILE("		CASE ")
//		SAVE_STRING_TO_DEBUG_FILE(Private_Get_Net_Realty_Building_Name(GET_PROPERTY_BUILDING(iCurrentPropertyID)))
//		SAVE_STRING_TO_DEBUG_FILE("			//")
//		SAVE_STRING_TO_DEBUG_FILE(Private_Get_Net_Realty_Building_Description(GET_PROPERTY_BUILDING(iCurrentPropertyID)))
//		SAVE_NEWLINE_TO_DEBUG_FILE()
//		
//		SAVE_STRING_TO_DEBUG_FILE("			blockObjectDetails.mn_GarageModel = ")SAVE_STRING_TO_DEBUG_FILE(GET_MODEL_NAME_FOR_DEBUG(blockObjectDetails.mn_GarageModel))SAVE_NEWLINE_TO_DEBUG_FILE()
//		SAVE_STRING_TO_DEBUG_FILE("			blockObjectDetails.vGarageLoc = ")SAVE_VECTOR_TO_DEBUG_FILE(blockObjectDetails.vGarageLoc)SAVE_NEWLINE_TO_DEBUG_FILE()
//		SAVE_STRING_TO_DEBUG_FILE("			blockObjectDetails.vGarageRot = ")SAVE_VECTOR_TO_DEBUG_FILE(blockObjectDetails.vGarageRot)SAVE_NEWLINE_TO_DEBUG_FILE()
//		SAVE_STRING_TO_DEBUG_FILE("			blockObjectDetails.mn_EntryModel = ")SAVE_STRING_TO_DEBUG_FILE(GET_MODEL_NAME_FOR_DEBUG(blockObjectDetails.mn_EntryModel))SAVE_NEWLINE_TO_DEBUG_FILE()
//		SAVE_STRING_TO_DEBUG_FILE("			blockObjectDetails.vEntryLoc = ")SAVE_VECTOR_TO_DEBUG_FILE(blockObjectDetails.vEntryLoc)SAVE_NEWLINE_TO_DEBUG_FILE()
//		SAVE_STRING_TO_DEBUG_FILE("			blockObjectDetails.vEntryRot = ")SAVE_VECTOR_TO_DEBUG_FILE(blockObjectDetails.vEntryRot)SAVE_NEWLINE_TO_DEBUG_FILE()
//		SAVE_STRING_TO_DEBUG_FILE("		BREAK")SAVE_NEWLINE_TO_DEBUG_FILE()
//		SAVE_STRING_TO_DEBUG_FILE("		// -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- //")SAVE_NEWLINE_TO_DEBUG_FILE()
//		SAVE_NEWLINE_TO_DEBUG_FILE()
//		CLOSE_DEBUG_FILE()
//		
//		bTestBlockObjectDetails = FALSE
//	ENDIF
ENDPROC

#ENDIF

PROC KILL_ON_CALL_FOR_TRANSITION_STAGES()
	SWITCH iLocalStage
		CASE STAGE_USING_MENU
		CASE STAGE_SETUP_FOR_USING_PROPERTY	
		CASE STAGE_ENTER_PROPERTY	
		CASE STAGE_BUZZED_INTO_PROPERTY		
			IF NOT IS_BIT_SET(iLocalBS2,LOCAL_BS2_bResetOnCallForTransition)
				SET_TRANSITION_SESSIONS_RESTART_ON_CALL()
				SET_BIT(iLocalBS2,LOCAL_BS2_bResetOnCallForTransition)
			ELSE
				SET_TRANSITION_SESSIONS_ON_CALL_DISABLED_THIS_FRAME_AFTER_RESTART()
			ENDIF
		BREAK
		DEFAULT
			CLEAR_BIT(iLocalBS2,LOCAL_BS2_bResetOnCallForTransition)
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL PHONE_MISSION_LAUNCHING_APPS_ACTIVE()
	IF DOES_SCRIPT_EXIST("appMPJobListNEW")
	AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appMPJobListNEW")) > 0
		CDEBUG1LN(DEBUG_SAFEHOUSE, "CAN_PLAYER_DRIVE_OUT_OF_GARAGE- False appMPJobListNew")
		REINIT_NET_TIMER(pauseMenuDelay,TRUE)
		RETURN TRUE
	ENDIF
	IF DOES_SCRIPT_EXIST("appJIPMP")
	AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appJIPMP")) > 0
		CDEBUG1LN(DEBUG_SAFEHOUSE, "CAN_PLAYER_DRIVE_OUT_OF_GARAGE- False appJIPMP")
		REINIT_NET_TIMER(pauseMenuDelay,TRUE)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_PLAYER_BYPASS_GENERAL_ACCESS_CHECKS_FOR_HEIST()
	IF SHOULD_BYPASS_CHECKS_FOR_ENTRY(FALSE)
	OR SHOULD_BYPASS_CHECKS_FOR_ENTRY(TRUE)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ANY_CONTEXT_ACTIVE()
	INT i
	REPEAT MAX_CONTEXT_INTENTION i
		IF g_IntentionList[i].bUsed
			RETURN TRUE
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

PROC DO_GANG_BOSS_ASSOCIATE_HELP()
	FLOAT fDistance
	BOOL bInRangeOfEntry
	INT j
	IF NOT GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(),FALSE)
		EXIT
	ENDIF
	
	IF IS_BIT_SET(ilocalBS, LOCAL_BS_ASSOCIATE_DENIED)
		EXIT
	ENDIF

	REPEAT mpProperties[iCurrentPropertyID].iNumEntrances j
		fDistance = GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()),mpProperties[iCurrentPropertyID].vBlipLocation[j])
		//CDEBUG2LN(DEBUG_SAFEHOUSE, "LOOP_ENTRANCES: vEntranceMarkerLoc = ", mpProperties[iCurrentPropertyID].entrance[j].vEntranceMarkerLoc, ", iEntrancePlayerIsUsing = ",iEntrancePlayerIsUsing)
		IF fDistance <= 30
			bInRangeOfEntry = TRUE
			BREAKLOOP
		ENDIF
	ENDREPEAT
	//PRINTLN("DO_GANG_BOSS_ASSOCIATE_HELP: checking range")
	IF bInRangeOfEntry
	AND NOT IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(PLAYER_ID())
	AND NOT SVM_FLOW_IS_CURRENT_MISSION_SVM_FLOW()
		IF NOT IS_PLAYER_BLOCKED_FROM_PROPERTY()
			INT iOwnershipStatus = GET_BUILDING_OWNERSHIP_STATUS(mpProperties[iCurrentPropertyID].iBuildingID)
			IF iOwnershipStatus = BUILDING_OWNERSHIP_STATUS_PLAYER_OWNS_PROPERTY_BUT_IN_GANG
				//PRINTLN("DO_GANG_BOSS_ASSOCIATE_HELP: checking!")
				//IF NOT IS_BIT_SET(ilocalBS, LOCAL_BS_ASSOCIATE_DENIED)
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					//PRINTLN("Player is a member of a gang")
					IF IS_PROPERTY_OFFICE(iCurrentPropertyID) 
						IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID())
							PRINT_HELP("CLU_ASSOC_BO",DEFAULT_HELP_TEXT_TIME+500)
							PRINTLN("DO_GANG_BOSS_ASSOCIATE_HELP- Printing help CLU_ASSOC_B")
						ELSE
							PRINT_HELP("OFF_ASSOC_B", DEFAULT_HELP_TEXT_TIME+500)
							PRINTLN("DO_GANG_BOSS_ASSOCIATE_HELP- Printing help")
						ENDIF
						SET_BIT(ilocalBS, LOCAL_BS_ASSOCIATE_DENIED)
					ELIF IS_PROPERTY_CLUBHOUSE(iCurrentPropertyID)
						IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID())
							PRINT_HELP("CLU_ASSOC_B",DEFAULT_HELP_TEXT_TIME+500)
							PRINTLN("DO_GANG_BOSS_ASSOCIATE_HELP- Printing help CLU_ASSOC_BO")
						ELSE
							PRINT_HELP("CLU_PROS_B",DEFAULT_HELP_TEXT_TIME+500)
							PRINTLN("DO_GANG_BOSS_ASSOCIATE_HELP- Printing help CLU_PROS_B")
						ENDIF
						SET_BIT(ilocalBS, LOCAL_BS_ASSOCIATE_DENIED)
					ENDIF	
				ENDIF
				//SET_BIT(ilocalBS, LOCAL_BS_ASSOCIATE_DENIED)
			ENDIF
		#IF IS_DEBUG_BUILD
		ELIF db_bFullDebug
			PRINTLN("DO_GANG_BOSS_ASSOCIATE_HELP- not printing player is blocked.")
		#ENDIF
		ENDIF
	ENDIF
	//PRINTLN("DO_GANG_BOSS_ASSOCIATE_HELP- called this frame")
ENDPROC



FUNC BOOL CAN_PLAYER_ACCESS_THIS_PROPERTY(INT &iKnowState)

	IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE)
		IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID(), FALSE)
			IF IS_PROPERTY_CLUBHOUSE(iCurrentPropertyID) 
			AND NOT DOES_ANYONE_I_KNOW_OWN_PROPERTY(iKnowState, iCurrentPropertyID, FALSE #IF IS_DEBUG_BUILD, db_bFullDebug #ENDIF)
			AND NOT DOES_PLAYER_HAVE_GROUP_ACCESS_TO_PROPERTY()
				RETURN FALSE
			ENDIF
			IF IS_PROPERTY_OFFICE(iCurrentPropertyID) 
			AND NOT DOES_ANYONE_I_KNOW_OWN_PROPERTY(iKnowState, iCurrentPropertyID, FALSE #IF IS_DEBUG_BUILD, db_bFullDebug #ENDIF)
				IF DOES_PLAYER_OWN_OFFICE_GARAGE_IN_BUILDING(PLAYER_ID(),MPProperties[iCurrentPropertyID].iBuildingID)
				
				ELSE
					RETURN FALSE
				ENDIF
			ENDIF
		ELSE
			IF IS_PROPERTY_OFFICE(iCurrentPropertyID) 
			AND NOT DOES_ANYONE_I_KNOW_OWN_PROPERTY(iKnowState, iCurrentPropertyID, FALSE #IF IS_DEBUG_BUILD, db_bFullDebug #ENDIF)
				IF DOES_PLAYER_OWN_OFFICE_GARAGE_IN_BUILDING(PLAYER_ID(),MPProperties[iCurrentPropertyID].iBuildingID)
				
				ELSE
					RETURN FALSE
				ENDIF
			ENDIF
			IF IS_PROPERTY_CLUBHOUSE(iCurrentPropertyID) 
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	IF IS_PROPERTY_CLUBHOUSE(iCurrentPropertyID) 
	AND (Is_Player_Currently_On_MP_Heist(PLAYER_ID())
	OR Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID()))
		#IF IS_DEBUG_BUILD
		IF db_bFullDebug
			PRINTLN("CAN_PLAYER_ACCESS_THIS_PROPERTY: False can't access clubhouse on heist")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF

	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_GUEST_ENTRY_ABORT()
	IF IS_BIT_SET(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_GUEST_PARKING_REQUIRED)
	AND GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iPVConcealInPlayerOwnedLocation = -1
		PRINTLN("AM_MP_PROPERTY_EXT:SHOULD_GUEST_ENTRY_ABORT- TRUE ")
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC GET_GARAGE_ATTENDANT_POSITION(VECTOR &vPos, VECTOR &vRot, INT iProperty)
	CDEBUG1LN(DEBUG_SAFEHOUSE, "GET_GARAGE_ATTENDANT_POSITION: iProperty = ", iProperty)
	
	SWITCH iProperty
		CASE PROPERTY_OFFICE_1
			CDEBUG1LN(DEBUG_SAFEHOUSE, "GET_GARAGE_ATTENDANT_POSITION: PROPERTY_OFFICE_1")
			vPos  = <<-1538.0, -572.3, 25.7>>
			vRot = <<0.0, 0.0, 90.0>>
		BREAK
		
		CASE PROPERTY_OFFICE_2_BASE
			CDEBUG1LN(DEBUG_SAFEHOUSE, "GET_GARAGE_ATTENDANT_POSITION: PROPERTY_OFFICE_2")
			vPos =  <<-1367.5, -470.4, 31.6>>
			vRot = <<0.0, 0.0, 97.0>>
		BREAK
		
		CASE PROPERTY_OFFICE_3
			CDEBUG1LN(DEBUG_SAFEHOUSE, "GET_GARAGE_ATTENDANT_POSITION: PROPERTY_OFFICE_3")
			vPos =  <<-148.5, -580.5, 32.4>>
			vRot = <<0.0, 0.0, 165.0>>
		BREAK
		
		CASE PROPERTY_OFFICE_4
			CDEBUG1LN(DEBUG_SAFEHOUSE, "GET_GARAGE_ATTENDANT_POSITION: PROPERTY_OFFICE_4")
			vPos = <<-80.7, -817.1, 36.0>>
			vRot = <<0.0, 0.0, 0.0>>
		BREAK
		
		DEFAULT
			CDEBUG1LN(DEBUG_SAFEHOUSE, "GET_GARAGE_ATTENDANT_POSITION: returning <<0.0, 0.0, 0.0>>")
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL CREATE_GARAGE_ATTENDANT()
	IF IS_PROPERTY_OFFICE(iCurrentPropertyID)
	AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_VENETIAN_JOB(g_FMMC_STRUCT.iAdversaryModeType)
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_UNION_DEPOSITORY)
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niGarageAttendant)
			VECTOR vAttendantPos
			VECTOR vAttendantRot
			
			REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("S_M_M_HighSec_01")))
			
			IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("S_M_M_HighSec_01")))
				IF CAN_RESERVE_NETWORK_OBJECTS_FOR_THIS_SCRIPT(GET_NUM_RESERVED_MISSION_PEDS() + 1, FALSE, TRUE)
					RESERVE_NETWORK_MISSION_PEDS(GET_NUM_RESERVED_MISSION_OBJECTS() + 1)
					
					IF CAN_REGISTER_MISSION_PEDS(1)
						IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niGarageAttendant)
							PED_DATA pedData
							PED_INDEX piTempGarageAttendant
							
							GET_GARAGE_ATTENDANT_POSITION(vAttendantPos, vAttendantRot, iCurrentPropertyID)
							
							CDEBUG1LN(DEBUG_SAFEHOUSE, "CREATE_GARAGE_ATTENDANT_PED_FOR_FLOW_PLAY: creating serverBD.niGarageAttendant, vAttendantPos = ", vAttendantPos)
							
							piTempGarageAttendant = CREATE_PED(PEDTYPE_CIVMALE, INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("S_M_M_HighSec_01")) , vAttendantPos, vAttendantRot.Z)
							
							pedData.pcsComponents[0].NewDrawableNumber = 1
							pedData.pcsComponents[0].NewTextureNumber = 2
							pedData.pcsComponents[4].NewDrawableNumber = 0
							pedData.pcsComponents[4].NewTextureNumber = 1
							pedData.pcsComponents[5].NewDrawableNumber = 0
							pedData.pcsComponents[5].NewTextureNumber  = 1
							
							INT j
							REPEAT NUM_PED_COMPONENTS j
								SET_PED_COMPONENT_VARIATION(piTempGarageAttendant, INT_TO_ENUM(PED_COMPONENT , j), pedData.pcsComponents[j].NewDrawableNumber, pedData.pcsComponents[j].NewTextureNumber, pedData.pcsComponents[j].NewPaletteNumber)
							ENDREPEAT
							
							SET_PED_HAS_AI_BLIP(piTempGarageAttendant, TRUE)
							REMOVE_ALL_PED_WEAPONS(piTempGarageAttendant)
							
							SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("S_M_M_HighSec_01")))
							
							serverBD.niGarageAttendant = PED_TO_NET(piTempGarageAttendant)
							
							RETURN TRUE
						ENDIF
					ELSE
						CDEBUG1LN(DEBUG_SAFEHOUSE, "CREATE_ACTIVITY_PEDS_FOR_ACTIVITY_CHECK: CAN_REGISTER_MISSION_PEDS = FALSE")
					ENDIF
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "CREATE_ACTIVITY_PEDS_FOR_ACTIVITY_CHECK: CAN_RESERVE_NETWORK_OBJECTS_FOR_THIS_SCRIPT = FALSE")
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "CREATE_ACTIVITY_PEDS_FOR_ACTIVITY_CHECK: anims and/or ped model not loaded yet")
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_BODYGUARD()
	IF NOT serverBD.bGarageAttendantCreated
		IF CREATE_GARAGE_ATTENDANT()
			IF NOT IS_PED_INJURED(NET_TO_PED(serverBD.niGarageAttendant))
				SET_PED_CONFIG_FLAG(NET_TO_PED(serverBD.niGarageAttendant), PCF_DontBehaveLikeLaw, TRUE)
				SET_PED_CONFIG_FLAG(NET_TO_PED(serverBD.niGarageAttendant), PCF_DontInfluenceWantedLevel, TRUE)
			
				serverBD.bGarageAttendantCreated = TRUE
			ENDIF
		ENDIF
	ELSE
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niGarageAttendant)
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niGarageAttendant)
				IF NOT IS_PED_INJURED(NET_TO_PED(serverBD.niGarageAttendant))
					SET_PED_RESET_FLAG(NET_TO_PED(serverBD.niGarageAttendant), PRF_IgnoreCombatTaunts, TRUE)
					SET_PED_RESET_FLAG(NET_TO_PED(serverBD.niGarageAttendant), PRF_DisableAgitation, TRUE)
				ENDIF
			ELSE
				NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.niGarageAttendant)
			ENDIF
		ENDIF
	ENDIF
ENDPROC



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                      Main Loop                             ////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
SCRIPT(coords_struct in_coords)
	bUseNewEntryAnims = TRUE

	vWorldPointLoc = in_coords.vec_coord[0]
	CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: started at ",vWorldPointLoc)
	IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: cleaning up as SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE() BEFORE PROCESS PREGAME COMPLETE!")
		CLEANUP_SCRIPT()
	ENDIF
	INIT_CURRENT_BUILDING_DETAILS()
	warpInWithDriverID= INVALID_PLAYER_INDEX()
	//REGISTER_PROPERTY_DOORS()
	IF NOT PROCESS_PRE_GAME()
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: cleaning up as script failed to receive an initial network broadcast.")
		CLEANUP_SCRIPT()
	ENDIF
	
	//Float fExitHeading
	//FLOAT fDistance
	//INT j
	//INT iBuildingLoop
	
	GET_MP_PROP_BLOCKING_OBJECT_DETAILS(blockObjectDetails,GET_PROPERTY_BUILDING(iCurrentPropertyID))
	START_NET_TIMER(tempPropertyExtMenu.exitDoorDelay,TRUE)
	
	iWarpingOutOfPropertyID = g_iWarpingOutOfPropertyWithID 
	CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: iWarpingOutOfPropertyID = ",iWarpingOutOfPropertyID)
	
	#IF IS_DEBUG_BUILD
	CREATE_WIDGETS()
	#ENDIF
	entranceArea[0] = GET_BUILDING_EXT_ENTRANCE_AREA(GET_PROPERTY_BUILDING(iCurrentPropertyID),0)
	entranceArea[1] = GET_BUILDING_EXT_ENTRANCE_AREA(GET_PROPERTY_BUILDING(iCurrentPropertyID),1)
	
	INT i
	INT iTempState = -1
	PLAYER_INDEX playerID
	
	PLAYER_INDEX ownerPlayerID
	INT iPropertyToEnter
	
	WHILE TRUE
		IF IS_PROPERTY_OFFICE(iCurrentPropertyID)
		OR IS_PROPERTY_OFFICE_GARAGE(iCurrentPropertyID)
			UPDATE_OFFICE_GARAGE_DOORS_ROOM()
			UPDATE_OFFICE_GARAGE_AUDIO()			
		ENDIF
		
		CLEAR_BIT(iLocalBS2, LOCAL_BS2_GroupAccessAllowed)
//		#IF FEATURE_EXECUTIVE
//		IF g_bPlayerInProcessOfExitingInterior
//			IF NOT IS_BIT_SET(ilocalBS, LOCAL_BS_ASSOCIATE_DENIED)
//				SET_BIT(ilocalBS, LOCAL_BS_ASSOCIATE_DENIED)
//				PRINTLN("AM_MP_PROPERTY_EXT: Setting player has already seen LOCAL_BS_ASSOCIATE_DENIED text")
//			ENDIF	
//		ENDIF
//		#ENDIF
		// One wait to rule them all. This can be the ONLY wait from here on in.....
		MP_LOOP_WAIT_ZERO()
		
	
		
		IF HAS_NET_TIMER_STARTED(cinematicDelay)
			IF NOT HAS_NET_TIMER_EXPIRED(cinematicDelay,500,TRUE)
				SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_FRONTEND_CANCEL)
			ELSE
				RESET_NET_TIMER(cinematicDelay)
			ENDIF
		ENDIF
		//reset at the start of each frame
		iKnowOwnerState = 0
		// If we have a match end event, bail.
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: cleaning up as SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()")
			IF iLocalStage >= STAGE_SETUP_FOR_USING_PROPERTY
				CLEAR_GLOBAL_ENTRY_DATA()
				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: clearing global data for multiplayer thread terminate")
			ENDIF
			CLEANUP_SCRIPT()
		ENDIF
		
		IF IS_NET_PLAYER_OK(PLAYER_ID())
			IF iLocalStage <= STAGE_USING_MENU
			AND GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()),vWorldPointLoc,FALSE) > 135
				IF NOT IS_PLAYER_TELEPORT_ACTIVE()
				AND NOT IS_PLAYER_IN_CORONA()
				AND NOT IS_POST_MISSION_CLEANUP_REQUIRED_OR_ACTIVE(PLAYER_ID())
					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: cleaning up as outside of distance")
					CLEANUP_SCRIPT()
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: want to clean up as outside distance, but player teleport active!")
				ENDIF
			ENDIF
		ENDIF
		
		IF g_iHeistLeaderPlayerID >= 0
			IF NOT IS_GAMER_HANDLE_VALID(heistLeader)
				IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(g_iHeistLeaderPlayerID))
					heistLeader = GET_GAMER_HANDLE_PLAYER(INT_TO_PLAYERINDEX(g_iHeistLeaderPlayerID))
					#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Assigning heistLeader from player: ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(g_iHeistLeaderPlayerID)), " Handle to be:")
					DEBUG_PRINT_GAMER_HANDLE(heistLeader)
					#ENDIF
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: NOT Assigning heistLeader player NOT ok!")
				ENDIF
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
		IF bBypassChecksAllowHeistEntry
			IF NOT IS_GAMER_HANDLE_VALID(heistLeader)
				heistLeader = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
				bSetFakeHeistData = TRUE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "Setting fake heist data as heist leader is invalid")
			ENDIF
		ELSE
			IF bSetFakeHeistData
				CLEAR_GAMER_HANDLE_STRUCT(heistLeader)
				bSetFakeHeistData = FALSE
			ENDIF
		ENDIF
		#ENDIF
		IF IS_BIT_SET(entryCutData.iBS,ENTRY_CS_BS_STARTED_MP_CUTSCENE)
			REPEAT NUM_NETWORK_PLAYERS i
				playerID = INT_TO_PLAYERINDEX(i)
				IF playerID != PLAYER_ID()
					IF IS_NET_PLAYER_OK(playerID,FALSE,FALSE)
						IF IS_PLAYER_IN_CUTSCENE(playerID)
							SET_PLAYER_INVISIBLE_LOCALLY(playerID,TRUE)
							CDEBUG1LN(DEBUG_SAFEHOUSE, "PROP_EXT:hiding player in cutscene: ", GET_PLAYER_NAME(playerID))
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
		//		IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
//		//OR IS_FM_MISSION_LAUNCH_IN_PROGRESS()
//			
//			//IF IS_PLAYER_ON_ANY_FM_JOB(PLAYER_ID(), TRUE)
//			//	IF NOT IS_PLAYER_ON_MP_MISSION(PLAYER_ID(), eFM_MISSION_CLOUD)
//					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: cleaning up as a mission is ongoing for player.")
//					CLEANUP_SCRIPT()
//				ENDIF
//			ENDIF
//		ENDIF
		#IF IS_DEBUG_BUILD
		MAINTAIN_DEBUG()
		#ENDIF
		
//		IF iLocalStage > STAGE_INIT
//			MAINTAIN_BLOCKING_OBJECTS_COLLISION_FLAGS()
//		ENDIF
		
		MAINTAIN_FOR_SALE_BLIP()
		
		KILL_ON_CALL_FOR_TRANSITION_STAGES()
		IF IS_PAUSE_MENU_ACTIVE_EX()
			REINIT_NET_TIMER(pauseMenuDelay,TRUE)
		ELSE
			IF HAS_NET_TIMER_EXPIRED_READ_ONLY(pauseMenuDelay,1500,TRUE)
				RESET_NET_TIMER(pauseMenuDelay)
			ENDIF
		ENDIF
		
		//CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: iLocalStage: ", iLocalStage)
		
		IF IS_NET_PLAYER_OK(PLAYER_ID())
		AND IS_BIT_SET(iLocalBS,LOCAL_BS_PURCHASED_CUSTOM_APT)
		AND	NOT IS_HELP_MESSAGE_BEING_DISPLAYED()	
				PRINT_HELP("PM_APT_CUSTOM_PURCHASED")
				CLEAR_BIT(iLocalBS,LOCAL_BS_PURCHASED_CUSTOM_APT)
		ENDIF
		
		
		SWITCH iLocalStage
			CASE STAGE_INIT
				IF MAINTAIN_FOR_SALE_SIGN()
				AND CREATE_ENTRY_AREAS_MAP_HOLE_PROTECTORS()
				AND CREATE_LOCAL_BLOCKING_OBJECTS()
				AND CREATE_BUZZER_PROPS_IF_NEEDED()
				AND CREATE_DOOR_PLANKS()
				AND CREATE_REVOLVING_DOOR()
					SET_LOCAL_STAGE(STAGE_CHECK_FOR_IN_LOCATE)
				#IF IS_DEBUG_BUILD
				ELSE
					
					IF NOT MAINTAIN_FOR_SALE_SIGN()
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: NOT CHECK_SERVER_EXTERIOR_OBJECTS_EXIST")
					ELIF NOT CREATE_ENTRY_AREAS_MAP_HOLE_PROTECTORS()
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: NOT CREATE_ENTRY_AREAS_MAP_HOLE_PROTECTORS")
					ELIF NOT CREATE_LOCAL_BLOCKING_OBJECTS()
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: NOT CREATE_LOCAL_BLOCKING_OBJECTS")
					ELIF NOT CREATE_BUZZER_PROPS_IF_NEEDED()
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: NOT CREATE_BUZZER_PROPS_IF_NEEDED")
					ELIF NOT CREATE_DOOR_PLANKS()
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: NOT CREATE_DOOR_PLANKS")
					ELIF NOT CREATE_REVOLVING_DOOR()
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: CREATE_REVOLVING_DOOR")
					ENDIF
					IF NATIVE_TO_INT(NETWORK_GET_HOST_OF_THIS_SCRIPT()) >= 0
						IF NETWORK_IS_PARTICIPANT_ACTIVE(NETWORK_GET_HOST_OF_THIS_SCRIPT())
							playerID = NETWORK_GET_PLAYER_INDEX( NETWORK_GET_HOST_OF_THIS_SCRIPT())
							IF IS_NET_PLAYER_OK(playerID)
								CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: waiting for host: ",GET_PLAYER_NAME(playerID), " to create object")
							ENDIF
						ENDIF
					ENDIF
				#ENDIF
				ENDIF
			BREAK
		
			CASE STAGE_CHECK_FOR_IN_LOCATE
				MAINTAIN_FOR_SALE_SIGN()
				MOVE_OFFICE_GARAGE_BLIP()
				CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_CAR_IN_GARAGE_LOCATE)
				IF IS_PLAYER_IN_PROPERTY(PLAYER_ID(),TRUE)
				OR g_bPropInteriorScriptRunning
//					#IF IS_DEBUG_BUILD
//					CDEBUG1LN(DEBUG_SAFEHOUSE, "GlobalPropertyEntryData.bLeftOrRejectedBuzzer = ",GlobalPropertyEntryData.bLeftOrRejectedBuzzer)
//					CDEBUG1LN(DEBUG_SAFEHOUSE, "g_bPlayerInProcessOfExitingInterior = ",g_bPlayerInProcessOfExitingInterior)
//					CDEBUG1LN(DEBUG_SAFEHOUSE, "globalPropertyEntryData.iPropertyEntered = ",globalPropertyEntryData.iPropertyEntered)
//					#ENDIF
					IF NOT GlobalPropertyEntryData.bLeftOrRejectedBuzzer
					AND NOT g_bPlayerInProcessOfExitingInterior
					AND globalPropertyEntryData.iPropertyEntered <= 0

						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Cleaning up script as player is inside property.bLeftOrRejectedBuzzer = ",GlobalPropertyEntryData.bLeftOrRejectedBuzzer,
													" g_bPropInteriorScriptRunning = ",g_bPropInteriorScriptRunning,
													" IS_PLAYER_IN_PROPERTY = ", IS_PLAYER_IN_PROPERTY(PLAYER_ID(),TRUE))
						CLEANUP_SCRIPT()
						BREAK
					ENDIF
				ENDIF
				IF g_bPlayerInProcessOfExitingInterior
					SET_BIT(iLocalBS2,LOCAL_BS2_PlayerWalkingOut)
					#IF IS_DEBUG_BUILD
//					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: g_bPlayerInProcessOfExitingInterior = TRUE this frame")
					#ENDIF
				
				ENDIF
				IF bUseNewEntryAnims //GET_PROPERTY_BUILDING(iCurrentPropertyID) = MP_PROPERTY_BUILDING_1
					IF g_bPlayerInProcessOfExitingInterior
					OR g_bPlayerStillExitingInterior
						IF IS_BIT_SET(mpPropIntRequestExtDoor.iBS,MP_PROP_INT_REQUEST_EXT_DOOR_OPEN)
							REQUEST_GARAGE_DOOR_OPEN()
						ELSE
							REQUEST_GARAGE_DOOR_CLOSE()
						ENDIF
					ENDIF
				ELSE
				IF g_bPlayerInProcessOfExitingInterior
				OR g_bPlayerStillExitingInterior
					
					IF DOES_ENTITY_EXIST(garageBlockingObj)
						SET_GAMEPLAY_CAM_IGNORE_ENTITY_COLLISION_THIS_UPDATE(garageBlockingObj)
						
						IF IS_NET_PLAYER_OK(PLAYER_ID())	
							FLOAT fvdist
							fvdist = VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(garageBlockingObj))
							
							IF fvdist < 8.0
								REQUEST_GARAGE_DOOR_OPEN()
							ENDIF
							
							IF NOT g_bPlayerInProcessOfExitingInterior
							AND g_bPlayerStillExitingInterior
								PRINTSTRING("prop VDIST: ")
								PRINTFLOAT(fvdist)
								PRINTNL()
								
								IF fvdist > 3.0
									CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: REQUEST_TO_USE_DOOR closing garage door - 7")
									REQUEST_GARAGE_DOOR_CLOSE()
									g_bPlayerStillExitingInterior = FALSE
								ENDIF
							ENDIF
						ENDIF				
					ENDIF
				ENDIF
				ENDIF
				RESET_HAD_HEIST_DROPOFF_ITEM(1)
				//bContextStillValid = FALSE
				CLEAR_BIT(iLocalBS2,LOCAL_BS2_bKeepWantedFlag)
				CLEAR_BIT(iLocalBS2,LOCAL_BS2_KeepInvalidText)
				CLEAR_BIT(iLocalBS2,LOCAL_BS2_bKeepPurchaseContext)
				IF IS_PLAYER_TELEPORT_ACTIVE()
				OR IS_PLAYER_IN_CORONA()
				OR IS_POST_MISSION_CLEANUP_REQUIRED_OR_ACTIVE(PLAYER_ID())
				OR IS_PLAYER_GETTING_IN_OR_OUT_OF_VEHICLE(PLAYER_ID())
					//CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Reseting timer player is warping or in corona or entering a vehicle")
					REINIT_NET_TIMER(tempPropertyExtMenu.exitDoorDelay,TRUE)
				ENDIF
				
				IF IS_NET_PLAYER_OK(PLAYER_ID())
					IF entranceArea[0].fWidth > 0
					AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(),entranceArea[0].vPos1,entranceArea[0].vPos2,entranceArea[0].fWidth)
						SET_BIT(iLocalBS2,LOCAL_BS2_KeepInvalidText)
						IF NOT HAS_NET_TIMER_STARTED(failSafeWarp)
							START_NET_TIMER(failSafeWarp,TRUE)
						ELSE
							IF HAS_NET_TIMER_EXPIRED_READ_ONLY(failSafeWarp,45000,TRUE)
								SET_ENTITY_COORDS(PLAYER_PED_ID(),mpProperties[iCurrentPropertyID].house.exits[0].vOutPlayerLoc)
								PRINTLN("AM_MP_PROPERTY_EXT: Failsafe warp triggered- 1")
							ENDIF
						ENDIF
					ELIF entranceArea[1].fWidth > 0
					AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(),entranceArea[1].vPos1,entranceArea[1].vPos2,entranceArea[1].fWidth)
						SET_BIT(iLocalBS2,LOCAL_BS2_KeepInvalidText)
						IF NOT HAS_NET_TIMER_STARTED(failSafeWarp)
							START_NET_TIMER(failSafeWarp,TRUE)
						ELSE
							IF HAS_NET_TIMER_EXPIRED_READ_ONLY(failSafeWarp,45000,TRUE)
								SET_ENTITY_COORDS(PLAYER_PED_ID(),mpProperties[iCurrentPropertyID].garage.vExitPlayerPos)
								PRINTLN("AM_MP_PROPERTY_EXT: Failsafe warp triggered- 2")
							ENDIF	
						ENDIF
					ELSE
						RESET_NET_TIMER(failSafeWarp)
					ENDIF
				ENDIF
				#IF IS_DEBUG_BUILD
					iCantEnterFramePrintDelay++
					IF iCantEnterFramePrintDelay >= 30
						db_bFullDebug = TRUE
						IF iCantEnterFramePrintDelay > 32
							iCantEnterFramePrintDelay = 0
							db_bFullDebug = FALSE
						ENDIF
						CDEBUG3LN(DEBUG_SAFEHOUSE, "db_bFullDebug = ", db_bFullDebug)
						IF db_bFullDebug
							CDEBUG3LN(DEBUG_SAFEHOUSE, "State of IS_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITITCAL_ENTITY() = ", IS_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITITCAL_ENTITY())
						ENDIF
					ENDIF
					#IF ALLOW_FAKE_GARAGE_DEBUG
						iCantEnterFramePrintDelay = 0
					#ENDIF
					//CDEBUG1LN(DEBUG_SAFEHOUSE, "iCantEnterFramePrintDelay = ",iCantEnterFramePrintDelay)
				#ENDIF
				//CDEBUG1LN(DEBUG_SAFEHOUSE, "Setting LOCAL_BS2_KeepInvalidText = FALSE (FC= ",GET_FRAME_COUNT())
				MAINTAIN_PLAYERS_DOING_HEIST_DROPOFF()
				MAINTAIN_WARP_WITH_DRIVER()
				IF IS_FM_TYPE_UNLOCKED(FMMC_TYPE_CUSTOM_CAR_GARAGE)
				OR SHOULD_PLAYER_BYPASS_GENERAL_ACCESS_CHECKS_FOR_HEIST()
					iRejectedEntryBS = 0
					IF IS_NET_PLAYER_OK(PLAYER_ID())	
					
						iTempState = -1
					
						IF CAN_PLAYER_USE_PROPERTY(SHOULD_PLAYER_BYPASS_GENERAL_ACCESS_CHECKS_FOR_HEIST() #IF IS_DEBUG_BUILD, db_bFullDebug #ENDIF)
							//PRINTLN("CDM BOOP: CAN_PLAYER_USE_PROPERTY = TRUE") 
							DO_GANG_BOSS_ASSOCIATE_HELP()
							IF CAN_PLAYER_ACCESS_THIS_PROPERTY(iTempState)
								//PRINTLN("CDM BOOP: CAN_PLAYER_ACCESS_THIS_PROPERTY(iTempState) = TRUE") 
						
								IF NOT g_bPlayerInProcessOfExitingInterior
									//IF DOES_LOCAL_PLAYER_OWN_PROPERTY_IN_BUILDING(mpProperties[iCurrentPropertyID].iBuildingID)
										//CDEBUG1LN(DEBUG_SAFEHOUSE, "Player own Property")
									IF NOT IS_PAUSE_MENU_ACTIVE_EX()
									AND NOT IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
									AND NOT IS_CUSTOM_MENU_ON_SCREEN()
									AND NOT IS_BROWSER_OPEN()
									AND NOT IS_COMMERCE_STORE_OPEN()
									AND NOT IS_STORE_PENDING_NETWORK_SHUTDOWN_TO_OPEN()
									AND NOT HAS_NET_TIMER_STARTED(pauseMenuDelay)
									AND NOT IS_INTERACTION_MENU_OPEN()
									AND NOT PROPERTY_HAS_JUST_ACCEPTED_A_MISSION(#IF IS_DEBUG_BUILD db_bFullDebug #ENDIF)
									AND NOT PHONE_MISSION_LAUNCHING_APPS_ACTIVE()
									AND IS_PLAYER_CONTROL_ON(PLAYER_ID())
									AND NOT IS_BIT_SET(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_AcceptedInvite)
									AND NOT IS_TRIP_SKIP_IN_PROGRESS_GLOBAL()
									AND NOT MPGlobalsAmbience.R2Pdata.bLaunchRaceToPoint
									AND NOT GB_IS_PLAYER_JOINING_A_GANG(PLAYER_ID())
									AND NOT globalPropertyEntryData.bLeftOrRejectedBuzzer
										//PRINTLN("CDM BOOP: looping entrances") 
										LOOP_ENTRANCES()
										IF IS_THERE_A_PROPERTY_TO_BUY()
										#IF FEATURE_GEN9_EXCLUSIVE
										AND NOT IS_PLAYER_ON_MP_INTRO()
										#ENDIF
											HANDLE_PURCHASE_LOCATE()
										ENDIF
									ELSE
										SET_BIT(iLocalBS2,LOCAL_BS2_bKeepWantedFlag)
										SET_BIT(iLocalBS2,LOCAL_BS2_KeepInvalidText)
										#IF IS_DEBUG_BUILD
											IF db_bFullDebug
												CDEBUG1LN(DEBUG_SAFEHOUSE,"AM_MP_PROPERTY_EXT: not looping entrances")
												IF IS_PAUSE_MENU_ACTIVE_EX()
													CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: IS_PAUSE_MENU_ACTIVE()	TRUE ")
												ELIF IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
													CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)	TRUE ")
												ELIF IS_CUSTOM_MENU_ON_SCREEN()
													CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: IS_CUSTOM_MENU_ON_SCREEN()	TRUE ")
												ELIF IS_BROWSER_OPEN()
													CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: IS_BROWSER_OPEN()	TRUE ")
												ELIF IS_INTERACTION_MENU_OPEN()
													CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: IS_INTERACTION_MENU_OPEN()	TRUE ")
												ELIF PROPERTY_HAS_JUST_ACCEPTED_A_MISSION(FALSE)
													CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: PROPERTY_HAS_JUST_ACCEPTED_A_MISSION()	TRUE ")
												ELIF PHONE_MISSION_LAUNCHING_APPS_ACTIVE()
													CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: PHONE_MISSION_LAUNCHING_APPS_ACTIVE()	TRUE ")
												ELIF IS_COMMERCE_STORE_OPEN()
													CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: IS_COMMERCE_STORE_OPEN())	TRUE ")	
												ELIF IS_STORE_PENDING_NETWORK_SHUTDOWN_TO_OPEN()
													CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: IS_STORE_PENDING_NETWORK_SHUTDOWN_TO_OPEN()	TRUE ")
												ELIF HAS_NET_TIMER_STARTED(pauseMenuDelay)
													CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: HAS_NET_TIMER_STARTED(pauseMenuDelay)	TRUE ")
												ELIF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
													CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: IS_PLAYER_CONTROL_ON(PLAYER_ID())	FALSE ")
												ELIF IS_BIT_SET(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_AcceptedInvite)
													CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: biITA_AcceptedInvite set not allowing entry ")
												ELIF IS_TRIP_SKIP_IN_PROGRESS_GLOBAL()
													CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: IS_TRIP_SKIP_IN_PROGRESS_GLOBAL set not allowing entry ")	
												ELIF MPGlobalsAmbience.R2Pdata.bLaunchRaceToPoint
													CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: MPGlobalsAmbience.R2Pdata.bLaunchRaceToPoint set not allowing entry ")	
												ELIF GB_IS_PLAYER_JOINING_A_GANG(PLAYER_ID())
													CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: GB_IS_PLAYER_JOINING_A_GANG not allowing entry ")	
												ELIF globalPropertyEntryData.bLeftOrRejectedBuzzer
													CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: globalPropertyEntryData.bLeftOrRejectedBuzzer is true not allowing entry ")	
												ENDIF
											ENDIF
										#ENDIF
									ENDIF
								#IF IS_DEBUG_BUILD
								ELSE
									IF db_bFullDebug
										CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: NOT - g_bPlayerInProcessOfExitingInterior	")
									ENDIF
								#ENDIF
								ENDIF
							ELSE
								#IF IS_DEBUG_BUILD
								IF db_bFullDebug
									IF IS_PROPERTY_OFFICE(iCurrentPropertyID) 
										CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: property is office")
									ENDIF
									IF IS_PROPERTY_CLUBHOUSE(iCurrentPropertyID) 
										CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: property is clubhouse")
									ENDIF
									IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE)
										CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: player is gang member not boss")
									ENDIF
									IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID(), FALSE)
										CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: player is BIKER gang member not boss")
									ENDIF
									IF DOES_ANYONE_I_KNOW_OWN_PROPERTY(iTempState, iCurrentPropertyID, FALSE #IF IS_DEBUG_BUILD, db_bFullDebug #ENDIF)
										CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: DOES_ANYONE_I_KNOW_OWN_PROPERTY is true")
									ENDIF
									CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: CAN_PLAYER_ACCESS_THIS_PROPERTY is FALSE")

								ENDIF
								#ENDIF
							ENDIF
						#IF IS_DEBUG_BUILD
						ELSE
							IF db_bFullDebug
								CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: NOT - CAN_PLAYER_USE_PROPERTY()	")
							ENDIF
						#ENDIF
						ENDIF
					#IF IS_DEBUG_BUILD
					ELSE
						IF db_bFullDebug
							CDEBUG2LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: NOT - IS_NET_PLAYER_OK(PLAYER_ID())	")
						ENDIF
					#ENDIF
					ENDIF
				#IF IS_DEBUG_BUILD
				ELSE
					IF db_bFullDebug
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: NOT - IS_FM_TYPE_UNLOCKED(FMMC_TYPE_CUSTOM_CAR_GARAGE)")
					ENDIF
				#ENDIF
				ENDIF
				IF NOT IS_BIT_SET(iLocalBS2,LOCAL_BS2_bKeepWantedFlag)
					IF IS_BIT_SET(ilocalBS, LOCAL_BS_WANTED_HELP_TEXT)
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CUST_GAR_WH")
						OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CUST_APT_WH")
						OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CUST_GAR_MISO")
						OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CUST_APT_MISO")
						OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CUST_PROP_W")
						OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PROP_GAR_HEI_WOH")
						OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PROP_APT_HEI_WOH")
						OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PPA_GEN0")
						OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PPA_GEN1")
						OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PPA_BEAST0")
						OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PPA_BEAST1")
						OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CUST_OFF_WH")
						OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PROP_OFF_HEI_WOH")
						OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PPA_CONTRA0")
						OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PPA_CONTRA0b")
						OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PPA_CONTRA1")
						OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PPA_CONTRA2")
						OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CUST_OFF_MISO")
						OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CUST_CLU_WH")
						OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PPA_GENCLUB")
						OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CUST_CLU_MISO")
							CLEAR_HELP()
						ENDIF
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Clearing wanted help message")
						CLEAR_BIT(ilocalBS,LOCAL_BS_WANTED_HELP_TEXT)
					ENDIF
				ENDIF
				IF NOT IS_BIT_SET(iLocalBS2,LOCAL_BS2_KeepInvalidText)
					IF iWarpingOutOfPropertyID != 0
						IF NOT IS_PLAYER_TELEPORT_ACTIVE()
						AND NOT IS_PLAYER_IN_CORONA()
						AND NOT g_bPlayerInProcessOfExitingInterior
						AND NOT IS_POST_MISSION_CLEANUP_REQUIRED_OR_ACTIVE(PLAYER_ID())
							IF IS_BIT_SET(iLocalBS2,LOCAL_BS2_PlayerWalkingOut)
								REINIT_NET_TIMER(tempPropertyExtMenu.exitDoorDelay,TRUE)
								CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: tempPropertyExtMenu.exitDoorDelay started as player is walking out of apartment")
								CLEAR_BIT(iLocalBS2,LOCAL_BS2_PlayerWalkingOut)
							ENDIF
							CLEAR_ENTERING_DISABLED_DUE_TO_EXIT_WARP()
						ENDIF
					ENDIF
					IF iBuzzerContextIntention != NEW_CONTEXT_INTENTION
						RELEASE_CONTEXT_INTENTION(iBuzzerContextIntention)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Releasing the context intention for the buzzer.")
					ENDIF
					CLEAR_INVALID_VEHICLE_HELP()
				ENDIF
				IF IS_BIT_SET(iLocalBS2,LOCAL_BS2_bKeepWantedFlag)
				OR IS_BIT_SET(iLocalBS2,LOCAL_BS2_KeepInvalidText)
					IF IS_BIT_SET(ilocalBS, LOCAL_BS_WANTED_HELP_TEXT)
					OR IS_BIT_SET(iLocalBS, LOCAL_BS_INVALID_VEHICLE_HELP)
					OR IS_ANY_CONTEXT_ACTIVE()
						IF iPurchaseContextIntention = NEW_CONTEXT_INTENTION 
						AND iBuzzerContextIntention = NEW_CONTEXT_INTENTION
							SUPPRESS_CONTEXT_BUTTON_ACTIONS_THIS_FRAME()
							CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: suppressing context for help text")
						ENDIF
						//IS_CONTEXT_INTENTION_HELP_DISPLAYING(iPurchaseContextIntention))
					ENDIF
				ENDIF
				IF NOT IS_BIT_SET(iLocalBS2,LOCAL_BS2_bKeepPurchaseContext)
					IF iPurchaseContextIntention != NEW_CONTEXT_INTENTION
						RELEASE_CONTEXT_INTENTION(iPurchaseContextIntention)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Releasing the context intention for the purchase menu.")
					ENDIF
				ENDIF
				
				STORE_THIS_PROPERTY_ENTRANCE_LOCATIONS()
				
			BREAK
			CASE STAGE_SETUP_FOR_MENU
				IF iBuzzerContextIntention != NEW_CONTEXT_INTENTION
					RELEASE_CONTEXT_INTENTION(iBuzzerContextIntention)
				ENDIF
				IF iPurchaseContextIntention != NEW_CONTEXT_INTENTION
					RELEASE_CONTEXT_INTENTION(iPurchaseContextIntention)
				ENDIF
				DISABLE_CELLPHONE_THIS_FRAME_ONLY()
				IF IS_NET_PLAYER_OK(PLAYER_ID())
				AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
				AND NOT g_bMissionEnding
				AND NOT g_bAbortPropertyMenus
				AND NOT g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistCelebration
				AND CAN_PLAYER_USE_PROPERTY(SHOULD_PLAYER_BYPASS_GENERAL_ACCESS_CHECKS_FOR_HEIST())
				AND NOT PROPERTY_HAS_JUST_ACCEPTED_A_MISSION(#IF IS_DEBUG_BUILD db_bFullDebug #ENDIF)
				AND NOT IS_BIT_SET(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_AcceptedInvite)
					IF NOT MP_FORCE_TERMINATE_INTERNET_ACTIVE()
						MP_FORCE_TERMINATE_INTERNET()
						SET_BIT(iLocalBS, LOCAL_BS_FORCE_TERMINATED_INTERNET)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Force terminating internet for entry into property.-1")
					ENDIF
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
					
					IF NOT IS_BIT_SET(iLocalBS2,LOCAL_BS2_LockCarOnMenu)
					AND NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_DOING_HEIST_DROPOFF)
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							IF DOES_ENTITY_EXIST(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
								carDoorLocks = GET_VEHICLE_DOOR_LOCK_STATUS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
								IF NETWORK_HAS_CONTROL_OF_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
									SET_VEHICLE_DOORS_LOCKED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),VEHICLELOCK_LOCKED)
									CDEBUG1LN(DEBUG_SAFEHOUSE, "Player locking car doors for menu")
								ENDIF
								SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(PLAYER_PED_ID(), KNOCKOFFVEHICLE_NEVER)
							ENDIF	
						ENDIF
						SET_BIT(iLocalBS2,LOCAL_BS2_LockCarOnMenu)
					ENDIF
					IF iCurrentMenuType = MENU_TYPE_BUZZER
						
						IF HAS_PLAYER_FINISHED_BUZZ_ANIM()
							IF bUseNewEntryAnims
								IF DOES_CAM_EXIST(entryCutData.cameras[0])
									DESTROY_CAM(entryCutData.cameras[0])
								ENDIF
							ENDIF
							DISPLAY_RADAR(FALSE)
							SET_BIT(iLocalBS2,LOCAL_BS2_DisabledRadar)
							IF (IS_PROPERTY_OFFICE(iCurrentPropertyID) OR IS_PROPERTY_OFFICE_GARAGE(iCurrentPropertyID))
							AND mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iType = ENTRANCE_TYPE_GARAGE
								NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE,NSPC_FREEZE_POSITION)
							ELSE
								NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE,NSPC_FREEZE_POSITION|NSPC_NO_COLLISION|NSPC_SET_INVISIBLE)
							ENDIF
							SET_BIT(iLocalBS, LOCAL_BS_TAKEN_AWAY_CONTROL)
							globalPropertyEntryData.ownerID = INVALID_PLAYER_INDEX()
							globalPropertyEntryData.bLeftOrRejectedBuzzer = FALSE
							CDEBUG1LN(DEBUG_SAFEHOUSE, "STAGE_SETUP_FOR_MENU: globalPropertyEntryData.bLeftOrRejectedBuzzer = FALSE")
							GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iRequestEntryBS = 0
							CDEBUG1LN(DEBUG_SAFEHOUSE, "STAGE_SETUP_FOR_MENU: GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iRequestEntryBS = 0")
							CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: resetting globalPropertyEntryData.ownerID= INVALID_PLAYER_INDEX()") 
							IF NOT DOES_CAM_EXIST(propertyCam0)
								propertyCam0 = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", mpProperties[iCurrentPropertyID].camData.vPos,mpProperties[iCurrentPropertyID].camData.vRot,mpProperties[iCurrentPropertyID].camData.fFOV)
								SET_CAM_ACTIVE(propertyCam0,TRUE)
								RENDER_SCRIPT_CAMS(TRUE, FALSE)
							ENDIF
							globalPropertyEntryData.iCostOfPropertyJustBought = 0
							CLEAR_BIT(iLocalBS2, LOCAL_BS2_bRunPropertySelectMenu)
							
							
							SET_LOCAL_STAGE(STAGE_USING_MENU)
						ENDIF
					ELSE
						
						IF bUseNewEntryAnims
						AND mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iType != ENTRANCE_TYPE_GARAGE
						AND iCurrentMenuType != MENU_TYPE_PURCHASE
							GET_ENTRY_CUTSCENE_DATA(ENTRY_CUTSCENE_TYPE_FRONT_DOOR,TRUE)
							IF NOT RUN_ENTRY_CUTSCENE(ENTRY_CUTSCENE_TYPE_FRONT_DOOR,TRUE)
								
								BREAK
							ENDIF
						ELSE
							IF mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iType != ENTRANCE_TYPE_GARAGE
							AND iCurrentMenuType != MENU_TYPE_PURCHASE
								SET_BIT(iLocalBS2,LOCAL_BS2_bWalkInTriggered)
							ENDIF
							DISPLAY_RADAR(FALSE)
							SET_BIT(iLocalBS2,LOCAL_BS2_DisabledRadar)
							NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_CAN_BE_TARGETTED | NSPC_ALLOW_PLAYER_DAMAGE | NSPC_PREVENT_EVERYBODY_BACKOFF)
							SET_BIT(ilocalBS,LOCAL_BS_TAKEN_AWAY_CONTROL)
							
							IF NOT DOES_CAM_EXIST(propertyCam0)
								propertyCam0 = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", mpProperties[iCurrentPropertyID].camData.vPos,mpProperties[iCurrentPropertyID].camData.vRot,mpProperties[iCurrentPropertyID].camData.fFOV)
								SET_CAM_ACTIVE(propertyCam0,TRUE)
								RENDER_SCRIPT_CAMS(TRUE, FALSE)
							ENDIF
							globalPropertyEntryData.iCostOfPropertyJustBought = 0
							SET_LOCAL_STAGE(STAGE_USING_MENU)
						ENDIF
					ENDIF
				ELSE
					IF g_bAbortPropertyMenus
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: aborting menu. 1 g_bAbortPropertyMenus = TRUE")
					ENDIF
					RESET_TO_ENTRANCE_TO_PROPERTY_STAGE()
				ENDIF
			BREAK
			
			CASE STAGE_USING_MENU
				CLEAR_BIT(iLocalBS, LOCAL_BS_BUZZER_GIVE_GOTO_TASK)	
				CLEAR_BIT(iLocalBS, LOCAL_BS_BUZZER_GIVE_RING_TASK)	
				DISABLE_CELLPHONE_THIS_FRAME_ONLY()
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_USING_EXT)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "PROPERTY_BROADCAST_BS_PLAYER_USING_EXT- SET- 1")
				IF IS_NET_PLAYER_OK(PLAYER_ID())
				AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
				AND NOT IS_STORE_PENDING_NETWORK_SHUTDOWN_TO_OPEN()
				AND NOT g_bMissionEnding
				AND NOT g_bAbortPropertyMenus
				AND NOT g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistCelebration
				AND CAN_PLAYER_USE_PROPERTY(SHOULD_PLAYER_BYPASS_GENERAL_ACCESS_CHECKS_FOR_HEIST())	
				AND NOT IS_BIT_SET(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_AcceptedInvite)
				AND NOT PROPERTY_HAS_JUST_ACCEPTED_A_MISSION(#IF IS_DEBUG_BUILD db_bFullDebug #ENDIF)
				AND NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_USING_PROPERTY)
					IF NOT MP_FORCE_TERMINATE_INTERNET_ACTIVE()
						MP_FORCE_TERMINATE_INTERNET()
						SET_BIT(iLocalBS, LOCAL_BS_FORCE_TERMINATED_INTERNET)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Force terminating internet for entry into property.-2")
					ENDIF
					//IF iCurrentMenuType = MENU_TYPE_BUZZER
						IF NOT IS_BIT_SET(iLocalBS2,LOCAL_BS2_LockCarOnMenu)
						AND NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_DOING_HEIST_DROPOFF)
							IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
								IF DOES_ENTITY_EXIST(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
									carDoorLocks = GET_VEHICLE_DOOR_LOCK_STATUS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
									IF NETWORK_HAS_CONTROL_OF_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
										SET_VEHICLE_DOORS_LOCKED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),VEHICLELOCK_LOCKED)
										CDEBUG1LN(DEBUG_SAFEHOUSE, "Player locking car doors for menu")
									ENDIF
									SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(PLAYER_PED_ID(), KNOCKOFFVEHICLE_NEVER)
								ENDIF	
							ENDIF
							SET_BIT(iLocalBS2,LOCAL_BS2_LockCarOnMenu)
						ENDIF
					//ENDIF
					HANDLE_PROPERTY_OPTIONS_MENU()
					HANDLE_RESPONSE_TO_ENTRY_REQUEST()
				ELSE
					IF g_bAbortPropertyMenus
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: aborting menu. 2 g_bAbortPropertyMenus = TRUE")
					ENDIF
					IF IS_SCREEN_FADING_OUT()
					AND IS_BIT_SET(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_AcceptedInvite)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Not aborting menu screen is fading.")
					ELSE
						IF iCurrentMenuType = MENU_TYPE_BUZZER
					
							IF NOT globalPropertyEntryData.bLeftOrRejectedBuzzer
							AND NOT IS_BIT_SET(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_AcceptedInvite)
								CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: globalPropertyEntryData.bLeftOrRejectedBuzzer = TRUE. STAGE_USING_MENU")
								globalPropertyEntryData.bLeftOrRejectedBuzzer = TRUE
							ELSE
								RESET_TO_ENTRANCE_TO_PROPERTY_STAGE()
							ENDIF
						ELSE
							RESET_TO_ENTRANCE_TO_PROPERTY_STAGE()
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE STAGE_SETUP_FOR_USING_PROPERTY
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_USING_EXT)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "PROPERTY_BROADCAST_BS_PLAYER_USING_EXT- SET- 2")
				IF NOT MP_FORCE_TERMINATE_INTERNET_ACTIVE()
					MP_FORCE_TERMINATE_INTERNET()
					SET_BIT(iLocalBS, LOCAL_BS_FORCE_TERMINATED_INTERNET)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Force terminating internet for entry into property.-3")
				ENDIF
				IF NOT IS_BIT_SET(iLocalBS2,LOCAL_BS2_LockCarOnMenu)
				AND NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_DOING_HEIST_DROPOFF)
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF DOES_ENTITY_EXIST(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
							carDoorLocks = GET_VEHICLE_DOOR_LOCK_STATUS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
							IF NETWORK_HAS_CONTROL_OF_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
								SET_VEHICLE_DOORS_LOCKED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),VEHICLELOCK_LOCKED)
								CDEBUG1LN(DEBUG_SAFEHOUSE, "Player locking car doors for menu")
							ENDIF
							SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(PLAYER_PED_ID(), KNOCKOFFVEHICLE_NEVER)
						ENDIF	
					ENDIF
					SET_BIT(iLocalBS2,LOCAL_BS2_LockCarOnMenu)
				ENDIF
				CLEAR_BIT(iLocalBS2,LOCAL_BS2_DoNotBuzzOthers)
				IF iBuzzerContextIntention != NEW_CONTEXT_INTENTION
					RELEASE_CONTEXT_INTENTION(iBuzzerContextIntention)
				ENDIF
				DISABLE_CELLPHONE_THIS_FRAME_ONLY()
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
				IF NOT IS_BIT_SET(iLocalBS2,LOCAL_BS2_DisabledStore)
					SET_STORE_ENABLED(FALSE)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "disabling store player has entered- EXT")
					SET_BIT(iLocalBS2,LOCAL_BS2_DisabledStore)
				ENDIF
				
				IF IS_NET_PLAYER_OK(PLAYER_ID())
				AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
				AND (CAN_PLAYER_USE_PROPERTY(SHOULD_PLAYER_BYPASS_GENERAL_ACCESS_CHECKS_FOR_HEIST()) OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_WARPING_WITH_OWNER_IN_CAR))
				AND NOT PROPERTY_HAS_JUST_ACCEPTED_A_MISSION(#IF IS_DEBUG_BUILD db_bFullDebug #ENDIF)	
				AND NOT SHOULD_GUEST_ENTRY_ABORT()
					IF (mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iType = ENTRANCE_TYPE_GARAGE
					AND SHOULD_BYPASS_CHECKS_FOR_ENTRY())
					OR (SHOULD_BYPASS_CHECKS_FOR_ENTRY(FALSE)
					AND mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iType = ENTRANCE_TYPE_HOUSE)
						//nothing
					ELSE
					IF IS_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITITCAL_ENTITY()
						iPackageDropPropID = iCurrentPropertyID
						//SET_LOCAL_PLAYER_CAN_COLLECT_PORTABLE_PICKUPS(FALSE)
						SET_MAX_NUM_PORTABLE_PICKUPS_CARRIED_BY_PLAYER(DUMMY_MODEL_FOR_SCRIPT,0) 
						CDEBUG1LN(DEBUG_SAFEHOUSE, "[RCC MISSION] - ENTERING PROPERTY SO DROPPING PICKUP: ",iCurrentPropertyID)
					ELSE
						CDEBUG1LN(DEBUG_SAFEHOUSE, "[RCC MISSION] - NOT DROP PICKUP! no mission critical ")
					ENDIF
					
					ENDIF
					IF NOT IS_BIT_SET(iLocalBS2,LOCAL_BS2_RemoveStickyBombs)
						REMOVE_ALL_STICKY_BOMBS_FROM_ENTITY(PLAYER_PED_ID())
						CDEBUG1LN(DEBUG_SAFEHOUSE, "CDM: removed sticky bombs from ped")
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							IF DOES_ENTITY_EXIST(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
								IF NETWORK_HAS_CONTROL_OF_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
									REMOVE_ALL_STICKY_BOMBS_FROM_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
									CDEBUG1LN(DEBUG_SAFEHOUSE, "CDM: removed sticky bombs from vehicle ped is in")
								ENDIF
							ENDIF
						ENDIF
						SET_BIT(iLocalBS2,LOCAL_BS2_RemoveStickyBombs)
					ENDIF
					
					IF mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iType = ENTRANCE_TYPE_GARAGE
						IF NOT IS_BIT_SET(iLocalBS2,LOCAL_BS2_SetEntryData)
							globalPropertyEntryData.iVariation = -1
							PRINTLN("AMP_MP_PROPERTY_EXT: reset variation on entry to property - 2")
							globalPropertyEntryData.iPropertyEntered = mpProperties[iCurrentPropertyID].iIndex
							PRINTLN("21 globalPropertyEntryData.iPropertyEntered = ",globalPropertyEntryData.iPropertyEntered)
							globalPropertyEntryData.iEntrance = iEntrancePlayerIsUsing
							IF IS_PROPERTY_OFFICE(globalPropertyEntryData.iPropertyEntered)
								CLEAR_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_ENTER_HOUSE)
								SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_ENTER_GARAGE)
								CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: forcing enter Office for office garage")
								globalPropertyEntryData.iEntrance = 0
							ENDIF
							
							IF DOES_PLAYER_HAVE_GROUP_ACCESS_TO_PROPERTY()
								globalPropertyEntryData.ownerID = GB_GET_LOCAL_PLAYER_GANG_BOSS()
								globalPropertyEntryData.ownerHandle = GET_GAMER_HANDLE_PLAYER(globalPropertyEntryData.ownerID)
								SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_DO_SOLO_WALK_INTERIOR)
								CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: setting owner based on gang boss data")
								RESET_NET_TIMER(iWalkInTimer) //re-used timer to keep variables down
							ELSE
								globalPropertyEntryData.ownerID = player_id()
								globalPropertyEntryData.ownerHandle = GET_GAMER_HANDLE_PLAYER(player_id())
								CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: not group access")
							ENDIF
							CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: ENTERED GARAGE Setting globalPropertyEntryData.iPropertyEntered to be: ", globalPropertyEntryData.iPropertyEntered )
							CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: ENTERED GARAGE Setting iEntrancePlayerIsUsing to be: ", iEntrancePlayerIsUsing )
							#IF IS_DEBUG_BUILD
							CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: ENTERED GARAGE owner Handle = ")
							DEBUG_PRINT_GAMER_HANDLE(globalPropertyEntryData.ownerHandle)
							#ENDIF
							//SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_ENTER_VIA_EXTERIOR)
						
							IF iEntrancePlayerIsUsing > mpProperties[iCurrentPropertyID].iNumEntrances
								globalPropertyEntryData.iEntrance = 1
								CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: iEntrancePlayerIsUsing > mpProperties[iCurrentPropertyID].iNumEntrances forcing entrance to be: ", globalPropertyEntryData.iEntrance)
							ENDIF
							
							CLEAR_TRANSITION_SESSION_DETAILS_ON_ENTRY_IF_SUITABLE(3)
							DISPLAY_RADAR(FALSE)
							SET_BIT(iLocalBS2,LOCAL_BS2_DisabledRadar)
							SET_BIT(iLocalBS2,LOCAL_BS2_SetEntryData)
							SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_ENTERING_A_PROPERTY)
							CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: setting PROPERTY_BROADCAST_BS_PLAYER_ENTERING_A_PROPERTY- B")
							SET_PLAYER_BROADCAST_PLAYING_PROPERTY_TRANSITION_CUTSCENE(mpProperties[iCurrentPropertyID].iIndex)
							SET_FOCUS_POS_AND_VEL(GET_PLAYER_COORDS(PLAYER_ID()),<<0,0,0>>)
						ENDIF
						IF HANDLE_WARP_INTO_GARAGE()
							CLEAR_BIT(iLocalBS, LOCAL_BS_TAKEN_AWAY_CONTROL)
							//RESET_TO_ENTRANCE_TO_PROPERTY_STAGE()
							CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_ENTERING_GARAGE)
							CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_READY_FOR_VEHICLE_FADE)
							CLEAR_FOCUS()
							CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: CLEAR_FOCUS() - 2")
							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
								IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
									VEHICLE_INDEX vehPlayer
									vehPlayer = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
									
									IF GET_PED_IN_VEHICLE_SEAT(vehPlayer, VS_DRIVER) <> PLAYER_PED_ID()
										IF NOT IS_PROPERTY_CLUBHOUSE(iCurrentPropertyID)
										OR NOT g_bDoingClbhCutscene
											CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Calling RENDER_SCRIPT_CAMS(FALSE, FALSE, DEFAULT, DEFAULT, TRUE) as I'm a passenger")
											RENDER_SCRIPT_CAMS(FALSE, FALSE, DEFAULT, DEFAULT, TRUE)
											SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
											SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
										ELSE
											CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Not dealing with the cameras as the player is entering to do the clubhouse cutscene")
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							CLEAR_BIT(iLocalBS2,LOCAL_BS2_SetEntryData)
							CLEANUP_SCRIPT()
						ENDIF
					ELSE
						RETAIN_BUZZER_MARKER()

						SET_BIT(ilocalBS,LOCAL_BS_NO_FADE)
						IF NOT IS_BIT_SET(iLocalBS2,LOCAL_BS2_SetEntryData)
							globalPropertyEntryData.iVariation = -1
							PRINTLN("AMP_MP_PROPERTY_EXT: reset variation on entry to property - 1")
							globalPropertyEntryData.iPropertyEntered = mpProperties[iCurrentPropertyID].iIndex
							PRINTLN("21 globalPropertyEntryData.iPropertyEntered = ",globalPropertyEntryData.iPropertyEntered)
							IF (SHOULD_BYPASS_CHECKS_FOR_ENTRY(FALSE)
							AND mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].iType = ENTRANCE_TYPE_HOUSE)
								IF NETWORK_IS_GAMER_IN_MY_SESSION(heistLeader)
									globalPropertyEntryData.ownerID = NETWORK_GET_PLAYER_FROM_GAMER_HANDLE(heistLeader)
									REPEAT MAX_OWNED_PROPERTIES i
										IF GlobalplayerBD_FM[NATIVE_TO_INT(globalPropertyEntryData.ownerID)].propertyDetails.iOwnedProperty[i] = g_iHeistPropertyID
											globalPropertyEntryData.iVariation = GlobalplayerBD_FM[NATIVE_TO_INT(globalPropertyEntryData.ownerID)].propertyDetails.iPropertyVariant[i]
											CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: heist data owner in game: setting globalPropertyEntryData.iVariation = ",globalPropertyEntryData.iVariation)
										ENDIF
									ENDREPEAT
									CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: heist data owner in game")
								ELSE
									globalPropertyEntryData.ownerID = player_id()
									CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: heist data owner NOT in game")
								ENDIF
								globalPropertyEntryData.ownerHandle = heistLeader
								IF IS_NET_PLAYER_OK(globalPropertyEntryData.ownerID ,FALSE,FALSE)
									CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: setting entry data based on heist data: iProperty " ,g_iHeistPropertyID, " owned by: ", GET_PLAYER_NAME(globalPropertyEntryData.ownerID ))
									#IF IS_DEBUG_BUILD
									DEBUG_PRINT_GAMER_HANDLE(globalPropertyEntryData.ownerHandle)
									#ENDIF
								ENDIF
								globalPropertyEntryData.iPropertyEntered = mpProperties[g_iHeistPropertyID].iIndex
								PRINTLN("20 globalPropertyEntryData.iPropertyEntered = ",globalPropertyEntryData.iPropertyEntered)
								SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_HEIST_DROPOFF)
								g_HeistApartmentDropoffPanStarted = TRUE
								CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: g_iApartmentDropoffWarpRadius = ",g_iApartmentDropoffWarpRadius)
								CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Skipping anims inside as doing heist drop off")
								IF g_iApartmentDropoffWarpRadius > 0
									IF g_iApartmentDropoffWarpRadius = iALWAYS_WARP_TO_APARTMENT_DISTANCE 
										BROADCAST_PROPERTY_NEARBY_PLAYERS_INVITE_EVENT(ALL_PLAYERS(FALSE,FALSE),globalPropertyEntryData.iPropertyEntered ,globalPropertyEntryData.iVariation, -1 , TRUE,FALSE,NATIVE_TO_INT(globalPropertyEntryData.ownerID))
									ELSE
										BROADCAST_PROPERTY_NEARBY_PLAYERS_INVITE_EVENT(PLAYERS_IN_RANGE_OF_POINT(GET_PLAYER_COORDS(PLAYER_ID()),g_iApartmentDropoffWarpRadius,TRUE),globalPropertyEntryData.iPropertyEntered, globalPropertyEntryData.iVariation,-1 ,TRUE,FALSE,NATIVE_TO_INT(globalPropertyEntryData.ownerID))
									ENDIF
								ENDIF
								bHadMissionCriticalEntityOnDropoff = TRUE
								PRINTLN("bHadMissionCriticalEntityOnDropoff Set True 2")
							ELSE
								IF DOES_PLAYER_HAVE_GROUP_ACCESS_TO_PROPERTY()
									globalPropertyEntryData.ownerID = GB_GET_LOCAL_PLAYER_GANG_BOSS()
									globalPropertyEntryData.ownerHandle = GET_GAMER_HANDLE_PLAYER(globalPropertyEntryData.ownerID)
									SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_DO_SOLO_WALK_INTERIOR)
									CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: setting owner based on gang boss data - 2")
								ELSE
									globalPropertyEntryData.ownerID = player_id()
									globalPropertyEntryData.ownerHandle = GET_GAMER_HANDLE_PLAYER(player_id())
									CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: setting owner-  no group access")
								ENDIF
							ENDIF
							SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_ENTER_HOUSE)
							globalPropertyEntryData.iEntrance = iEntrancePlayerIsUsing
							IF IS_PROPERTY_OFFICE_GARAGE(globalPropertyEntryData.iPropertyEntered)
								CLEAR_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_ENTER_HOUSE)
								SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_ENTER_GARAGE)
								CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: forcing enter garage for office garage")
								globalPropertyEntryData.iEntrance = 2
							ENDIF
							CLEAR_TRANSITION_SESSION_DETAILS_ON_ENTRY_IF_SUITABLE(2)
							CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: ENTERED HOUSE Setting globalPropertyEntryData.iPropertyEntered to be: ", globalPropertyEntryData.iPropertyEntered, ", name = ", GET_PROPERTY_ENUM_NAME(globalPropertyEntryData.iPropertyEntered) )
							CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: ENTERED HOUSE Setting iEntrancePlayerIsUsing to be: ", iEntrancePlayerIsUsing )
							CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: local player owns this property setting flags accordingly")
							SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_ENTERING_A_PROPERTY)
							CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: setting PROPERTY_BROADCAST_BS_PLAYER_ENTERING_A_PROPERTY- D")
							SET_PLAYER_BROADCAST_PLAYING_PROPERTY_TRANSITION_CUTSCENE(mpProperties[iCurrentPropertyID].iIndex)
							//SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_ENTER_VIA_EXTERIOR)
							IF iEntrancePlayerIsUsing > mpProperties[iCurrentPropertyID].iNumEntrances
								globalPropertyEntryData.iEntrance = 0
								CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: iEntrancePlayerIsUsing > mpProperties[iCurrentPropertyID].iNumEntrances forcing entrance to be: ", globalPropertyEntryData.iEntrance)
							ENDIF
							SET_FOCUS_POS_AND_VEL(GET_PLAYER_COORDS(PLAYER_ID()),<<0,0,0>>)
							SET_BIT(iLocalBS2,LOCAL_BS2_SetEntryData)
						ENDIF
						
						NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						SET_BIT(iLocalBS, LOCAL_BS_TAKEN_AWAY_CONTROL)
						SET_LOCAL_STAGE(STAGE_ENTER_PROPERTY)

					ENDIF
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: cleaning up cutscenes for invalid player")
					IF g_bPropInteriorScriptRunning
					OR IS_BIT_SET(mpPropMaintain.iBS, MP_PROP_MAINTAIN_BS_LAUNCHED_INT_SCRIPT)
						globalPropertyEntryData.bLeftOrRejectedBuzzer = TRUE
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: globalPropertyEntryData.bLeftOrRejectedBuzzer = TRUE. STAGE_SETUP_FOR_USING_PROPERTY")
					ENDIF
					IF fakeGarageCarEnterCutsceneStage > FAKE_GARAGE_CAR_ENTER_CUTSCENE_INIT
					AND fakeGarageCarEnterCutsceneStage < FAKE_GARAGE_CAR_ENTER_CUTSCENE_COMPLETE
						CLEANUP_FAKE_GARAGE_CAR_ENTER_THIS_CUTSCENE(FALSE)
					ELIF SHOULD_GUEST_ENTRY_ABORT()
						CLEANUP_FAKE_GARAGE_CAR_ENTER_THIS_CUTSCENE(TRUE)
					ELIF iRunCarEntryFadeStage > FAKE_GARAGE_CAR_ENTER_CUTSCENE_INIT
					AND iRunCarEntryFadeStage < FAKE_GARAGE_CAR_ENTER_CUTSCENE_COMPLETE
						CLEANUP_FADED_OUT_GARAGE_CAR_ENTER(FALSE)
					ELSE
						
						RESET_TO_ENTRANCE_TO_PROPERTY_STAGE()
					ENDIF
					IF garageCarEnterCutStage != GARAGE_CAR_ENTER_CUTSCENE_INIT
						RESET_GARAGE_CAR_ENTER_CUTSCENE(9)	
					ENDIF
					IF IS_NET_PLAYER_OK(PLAYER_ID())
						IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
							IF DOES_ENTITY_EXIST(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
								IF GET_PED_IN_VEHICLE_SEAT(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) = PLAYER_PED_ID()
									REMOVE_DRIVING_INTO_GARAGE_DECORATOR(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),1)
								ENDIF
							ENDIF
						ENDIF	
					ENDIF
					IF garagePedEnterCutStage != GARAGE_PED_ENTER_CUTSCENE_INIT
						RESET_GARAGE_PED_ENTER_CUTSCENE()	
					ENDIF
				ENDIF
			BREAK
			
			// Added for when helicopter docking fails due to owner quiting so we can just spawn players outside the building
			CASE STAGE_WARP_TO_ENTRANCE_AREA_THEN_LEAVE
				GET_TRANSITION_ANIM_CUSTOM_EXIT_SPAWN_COORDS(GET_PROPERTY_BUILDING(iCurrentPropertyID), 0, 0, tempPropOffset)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT - STAGE_WARP_TO_ENTRANCE_AREA_THEN_LEAVE: Attempting warp")
				//IF WARP_TO_SPAWN_LOCATION(SPAWN_LOCATION_NEAREST_RESPAWN_POINT, FALSE, FALSE, FALSE, TRUE)
				IF NET_WARP_TO_COORD(tempPropOffset.vLoc, tempPropOffset.vRot.Z, FALSE, FALSE)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT - STAGE_WARP_TO_ENTRANCE_AREA_THEN_LEAVE: Warp successful!")
					//USE_CUSTOM_SPAWN_POINTS(FALSE)
					//CLEAR_CUSTOM_SPAWN_POINTS()
					CLEAR_BIT(iLocalBS2, LOCAL_BS2_WarpAndLeavePointsAdded)
					
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
						SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
					ENDIF
					
					IF IS_SCREEN_FADED_OUT()
						DO_SCREEN_FADE_IN(500)
					ENDIF
					
					RESET_TO_ENTRANCE_TO_PROPERTY_STAGE()
				ENDIF
				
				// Use of proper spawn points caused stack overflow, since this is a fallback case just spawn us at first spawn point outside office
				/*
				IF NOT IS_BIT_SET(iLocalBS2, LOCAL_BS2_WarpAndLeavePointsAdded)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT - STAGE_WARP_TO_ENTRANCE_AREA_THEN_LEAVE: Adding custom spawn points")
					
					USE_CUSTOM_SPAWN_POINTS(TRUE,FALSE,TRUE,0.0,2,DEFAULT,DEFAULT,0.1,DEFAULT,0.1)
					
					REPEAT 4 i
						GET_TRANSITION_ANIM_CUSTOM_EXIT_SPAWN_COORDS(GET_PROPERTY_BUILDING(iCurrentPropertyID), 0, i, tempPropOffset)
						ADD_CUSTOM_SPAWN_POINT(tempPropOffset.vLoc, tempPropOffset.vRot.z, 1.0 - (TO_FLOAT(i)/TO_FLOAT(NUM_NETWORK_PLAYERS)) #IF IS_DEBUG_BUILD,0.2 #ENDIF)	
						
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT - STAGE_WARP_TO_ENTRANCE_AREA_THEN_LEAVE: ADD_CUSTOM_SPAWN_POINT: i: ", i , " vLoc = ", tempPropOffset.vLoc)
					ENDREPEAT
					
					SET_BIT(iLocalBS2, LOCAL_BS2_WarpAndLeavePointsAdded)
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT - STAGE_WARP_TO_ENTRANCE_AREA_THEN_LEAVE: Attempting warp")
					
					IF WARP_TO_SPAWN_LOCATION(SPAWN_LOCATION_CUSTOM_SPAWN_POINTS,FALSE,FALSE,FALSE,TRUE)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT - STAGE_WARP_TO_ENTRANCE_AREA_THEN_LEAVE: Warp successful!")
						USE_CUSTOM_SPAWN_POINTS(FALSE)
						CLEAR_CUSTOM_SPAWN_POINTS()
						CLEAR_BIT(iLocalBS2, LOCAL_BS2_WarpAndLeavePointsAdded)
						
						IF IS_SCREEN_FADED_OUT()
							DO_SCREEN_FADE_IN(500)
						ENDIF
						
						RESET_TO_ENTRANCE_TO_PROPERTY_STAGE()
					ENDIF
				ENDIF
				*/
			BREAK
			
			CASE STAGE_LEAVE_ENTRANCE_AREA
				BOOL bShapeTestComplete
				
				bShapeTestComplete = FALSE
				
				IF GET_GAME_TIMER() > iBuzzerPressSyncedSceneTimer
					SHAPETEST_INDEX shapeTest
					SHAPETEST_STATUS shapeTestStatus
					INT bShapeTestHitSomething
					VECTOR vShapeTestPos
					VECTOR vShapeTestNormal
					ENTITY_INDEX shapeTestEntityIndex
					
					bShapeTestHitSomething = 0
					vShapeTestPos = <<0.0, 0.0, 0.0>>
					vShapeTestNormal = <<0.0, 0.0, 0.0>>
					shapeTestEntityIndex = NULL
					
					VECTOR vOffset
					
					//0 = 180, 1 = 135, 2 = 225, 3 = 90, 4 = 270
					
					// 	   \|/
					//  3 - O - 4
					//	   /|\
					//    1 0 2
					
					IF iBuzzerWalkAwayShapeTest = 0
						vOffset = <<0.0, -4.0, 0.0>>
					ELIF iBuzzerWalkAwayShapeTest = 1
						vOffset = <<-2.83, -2.83, 0.0>>
					ELIF iBuzzerWalkAwayShapeTest = 2
						vOffset = <<2.83, -2.83, 0.0>>
					ELIF iBuzzerWalkAwayShapeTest = 3
						vOffset = <<-4.0, 0.0, 0.0>>
					ELIF iBuzzerWalkAwayShapeTest = 4
						vOffset = <<4.0, 0.0, 0.0>>
					ELSE
						vOffset = <<0.0, 0.0, 0.0>>
					ENDIF
					
					FLOAT fShapeTestHeightOffset
					FLOAT fShapeTestRadius
					VECTOR vNearOffset
					VECTOR vFarOffset
					FLOAT fNearVerticalOffset
					FLOAT fFarVerticalOffset
					
					fShapeTestHeightOffset = -0.25
					fShapeTestRadius = 0.5
					
					#IF IS_DEBUG_BUILD
					#IF ALLOW_FAKE_GARAGE_DEBUG
					IF fShapeTestHeightOffset <> fDebugTestShapeTestHeightOffset
						fShapeTestHeightOffset = fDebugTestShapeTestHeightOffset
					ENDIF
					IF fShapeTestRadius <> fDebugTestShapeTestRadius
						fShapeTestRadius = fDebugTestShapeTestRadius
					ENDIF
					#ENDIF
					#ENDIF
					
					vNearOffset = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), vOffset / 4.0) + <<0.0, 0.0, fShapeTestHeightOffset>>
					vFarOffset = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), vOffset) + <<0.0, 0.0, fShapeTestHeightOffset>>
					
					fNearVerticalOffset = 0.0
					fFarVerticalOffset = 0.0
					
					GET_GROUND_Z_FOR_3D_COORD(vNearOffset + <<0.0, 0.0, 1.0>>, fNearVerticalOffset)
					IF vNearOffset.Z < fNearVerticalOffset
						fNearVerticalOffset = 0.0
					ENDIF
					GET_GROUND_Z_FOR_3D_COORD(vFarOffset + <<0.0, 0.0, 1.0>>, fFarVerticalOffset)
					IF vFarOffset.Z < fFarVerticalOffset
						fFarVerticalOffset = 0.0
					ENDIF
					
					IF NOT IS_BIT_SET(iLocalBS2,LOCAL_BS2_ShapeTestRunning)
						IF fNearVerticalOffset <> 0.0 AND fFarVerticalOffset <> 0.0 AND ABSF(fFarVerticalOffset - fNearVerticalOffset) < 2.0
							shapeTest = START_SHAPE_TEST_CAPSULE(<<vNearOffset.X, vNearOffset.Y, fNearVerticalOffset + 1.0>>, <<vFarOffset.X, vFarOffset.Y, fFarVerticalOffset + 1.0>>, fShapeTestRadius, SCRIPT_INCLUDE_ALL, PLAYER_PED_ID())
							CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: SHAPETEST[STARTING]")
							SET_BIT(iLocalBS2,LOCAL_BS2_ShapeTestRunning)
						ELSE
							CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: SHAPETEST[GroundHeightFail]")
							iBuzzerWalkAwayShapeTest++
							CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: SHAPETEST[iBuzzerWalkAwayShapeTest=", iBuzzerWalkAwayShapeTest, "]")
						ENDIF
					ENDIF
					
					IF IS_BIT_SET(iLocalBS2,LOCAL_BS2_ShapeTestRunning)
						shapeTestStatus = GET_SHAPE_TEST_RESULT(shapeTest, bShapeTestHitSomething, vShapeTestPos, vShapeTestNormal, shapeTestEntityIndex)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: SHAPETEST[RUNNING]")
						IF shapeTestStatus != SHAPETEST_STATUS_RESULTS_NOTREADY
							IF shapeTestStatus = SHAPETEST_STATUS_RESULTS_READY
								CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: SHAPETEST[SHAPETEST_STATUS_RESULTS_READY]")
								CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: SHAPETEST[HitSomething=", bShapeTestHitSomething, "]")
								CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: SHAPETEST[shapeTestEntityIndex=", DOES_ENTITY_EXIST(shapeTestEntityIndex), "]")
								IF bShapeTestHitSomething = 1
								OR DOES_ENTITY_EXIST(shapeTestEntityIndex)
									CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: SHAPETEST[TRUE]")
									iBuzzerWalkAwayShapeTest++
									CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: SHAPETEST[iBuzzerWalkAwayShapeTest=", iBuzzerWalkAwayShapeTest, "]")
									CLEAR_BIT(iLocalBS2,LOCAL_BS2_ShapeTestRunning)
									CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: SHAPETEST[LOCAL_BS2_ShapeTestRunning=FALSE]")
								ELSE
									CLEAR_BIT(iLocalBS2,LOCAL_BS2_ShapeTestRunning)
									CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: SHAPETEST[LOCAL_BS2_ShapeTestRunning=FALSE]")
									bShapeTestComplete = TRUE
									CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: SHAPETEST[bShapeTestComplete=TRUE]")
								ENDIF
							#IF IS_DEBUG_BUILD
							ELSE
								CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: SHAPETEST[SHAPETEST_STATUS_NONEXISTENT]")
							#ENDIF
							ENDIF
						#IF IS_DEBUG_BUILD
						ELSE
							CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: SHAPETEST[SHAPETEST_STATUS_RESULTS_NOTREADY]")
						#ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: SHAPETEST[LAST CHECK iBuzzerWalkAwayShapeTest=", iBuzzerWalkAwayShapeTest, "]")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: SHAPETEST[LAST CHECK bShapeTestComplete=", bShapeTestComplete, "]")
				
				IF IS_NET_PLAYER_OK(PLAYER_ID())
				AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
				AND GET_GAME_TIMER() > iBuzzerPressSyncedSceneTimer
				AND (bShapeTestComplete OR iBuzzerWalkAwayShapeTest >= 5)	//0 = 180, 1 = 135, 2 = 225, 3 = 90, 4 = 270
					//IF WARP_TO_SPAWN_LOCATION(SPAWN_LOCATION_AT_SPECIFIC_COORDS_IF_POSSIBLE,FALSE, FALSE, FALSE)
					//	CLEAR_SPECIFIC_SPAWN_LOCATION()
//					IF mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].fInPlayerHeading < 0
//						fExitHeading = mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].fInPlayerHeading +180
//					ELSE
//						fExitHeading = mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].fInPlayerHeading -180  
//					ENDIF
					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: iEntrance = ", iEntrancePlayerIsUsing)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Leave entrance area property in heading = ", mpProperties[iCurrentPropertyID].entrance[iEntrancePlayerIsUsing].fInPlayerHeading)
					
					IF iBuzzerWalkAwayShapeTest = 0
						SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 2000, 180, TRUE)
					ELIF iBuzzerWalkAwayShapeTest = 1
						SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 2000, 135, TRUE)
					ELIF iBuzzerWalkAwayShapeTest = 2
						SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 2000, 225, TRUE)
					ELIF iBuzzerWalkAwayShapeTest = 3
						SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 2000, 90, TRUE)
					ELIF iBuzzerWalkAwayShapeTest = 4
						SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 2000, 270, TRUE)
					ENDIF
					//CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Telling player to walk in direction: ",fExitHeading)
					//TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(),mpProperties[iCurrentPropertyID].house.exits[0].vOutPlayerLoc,PEDMOVE_WALK)
					IF IS_PROPERTY_OFFICE(iCurrentPropertyID)
						iWarpingOutOfPropertyID = iCurrentPropertyID
					ENDIF
					RESET_TO_ENTRANCE_TO_PROPERTY_STAGE()
					REINIT_NET_TIMER(tempPropertyExtMenu.exitDoorDelay,TRUE)
					//CLEAR_BIT(iLocalBS,LOCAL_BS_SETUP_SPECIFIC_SPAWN_POS)
					//ENDIF
				ENDIF
			BREAK
			
			CASE STAGE_ENTER_PROPERTY
				DISABLE_CELLPHONE_THIS_FRAME_ONLY()
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_USING_EXT)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "PROPERTY_BROADCAST_BS_PLAYER_USING_EXT- SET- 3")
				IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
				OR IS_PLAYER_SWITCH_IN_PROGRESS()
					RESET_TO_ENTRANCE_TO_PROPERTY_STAGE()
					IF garageCarEnterCutStage != GARAGE_CAR_ENTER_CUTSCENE_INIT
						RESET_GARAGE_CAR_ENTER_CUTSCENE(10)	
					ENDIF
					IF garagePedEnterCutStage != GARAGE_PED_ENTER_CUTSCENE_INIT
						RESET_GARAGE_PED_ENTER_CUTSCENE()	
					ENDIF
				ENDIF
//				IF iCurrentPropertyID = PROPERTY_HIGH_APT_1	
//				OR iCurrentPropertyID = PROPERTY_HIGH_APT_2	
//				OR iCurrentPropertyID = PROPERTY_HIGH_APT_3	
//				OR iCurrentPropertyID = PROPERTY_HIGH_APT_4
					WALK_INTO_PROPERTY_CUTSCENE(FALSE)
//				ELSE
//					globalPropertyEntryData.bWalkInSeqDone = TRUE
//				ENDIF
				IF globalPropertyEntryData.bForceCleanupExterior
//					IF iCurrentPropertyID = PROPERTY_HIGH_APT_1	
//					OR iCurrentPropertyID = PROPERTY_HIGH_APT_2	
//					OR iCurrentPropertyID = PROPERTY_HIGH_APT_3	
//					OR iCurrentPropertyID = PROPERTY_HIGH_APT_4
						IF globalPropertyEntryData.bWalkInSeqDone = TRUE	//iWalkInStage >= WIS_WAIT_FOR_LOAD
							globalPropertyEntryData.bForceCleanupExterior = FALSE
							CLEAR_FOCUS()
							CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: CLEAR_FOCUS() - 3")
							CLEAR_BIT(iLocalBS2,LOCAL_BS2_SetEntryData)
							CLEANUP_PROPERTY_CAMERAS()
							//CDEBUG1LN(DEBUG_SAFEHOUSE, "     ----->>>>>     WALK_INTO_PROPERTY - CLEANUP CAMERA")
						ENDIF
//					ELSE
//						globalPropertyEntryData.bForceCleanupExterior = FALSE
//					ENDIF
				ELSE
					IF globalPropertyEntryData.bInteriorWarpDone
						//bKeepScriptCameras = TRUE
						//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iRequestEntryBS = 0
						
						CDEBUG1LN(DEBUG_SAFEHOUSE, "     ----->>>>>     WALK_INTO_PROPERTY - Call WALK_IN_CLEANUP - C")
						WALK_IN_CLEANUP(FALSE)
						IF NOT IS_BIT_SET(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_InitDone)
							globalPropertyEntryData.bInteriorWarpDone = FALSE
							CDEBUG1LN(DEBUG_SAFEHOUSE, "1 globalPropertyEntryData.bInteriorWarpDone = FALSE") 
						ENDIF
						globalPropertyEntryData.bBuzzAccepted = FALSE
						globalPropertyEntryData.bForceCleanupExterior = FALSE
						DISPLAY_RADAR(TRUE)
						CLEAR_BIT(iLocalBS2,LOCAL_BS2_DisabledRadar)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "Player has starting internal synced scene killing exterior script")
						CLEANUP_SCRIPT()
					ENDIF
				ENDIF
			BREAK
			
			CASE STAGE_BUZZED_INTO_PROPERTY
				IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
				OR IS_PLAYER_SWITCH_IN_PROGRESS()
					RESET_TO_ENTRANCE_TO_PROPERTY_STAGE()
					IF garageCarEnterCutStage != GARAGE_CAR_ENTER_CUTSCENE_INIT
						RESET_GARAGE_CAR_ENTER_CUTSCENE(11)	
					ENDIF
					IF garagePedEnterCutStage != GARAGE_PED_ENTER_CUTSCENE_INIT
						RESET_GARAGE_PED_ENTER_CUTSCENE()	
					ENDIF
				ENDIF
				DISABLE_CELLPHONE_THIS_FRAME_ONLY()
				globalPropertyEntryData.bWalkInSeqDone = TRUE
				
				IF globalPropertyEntryData.bInteriorWarpDone
					//bKeepScriptCameras = TRUE
					//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iRequestEntryBS = 0
					IF NOT IS_BIT_SET(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_InitDone)
						globalPropertyEntryData.bInteriorWarpDone = FALSE
						CDEBUG1LN(DEBUG_SAFEHOUSE, "2 globalPropertyEntryData.bInteriorWarpDone = FALSE") 
					ENDIF
					globalPropertyEntryData.bBuzzAccepted = FALSE
					DISPLAY_RADAR(TRUE)
					CLEAR_BIT(iLocalBS2,LOCAL_BS2_DisabledRadar)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "Player has starting internal synced scene killing exterior script")
					CLEANUP_SCRIPT()
				
				ENDIF
			BREAK
		ENDSWITCH
		
		
			IF IS_PROPERTY_OFFICE(iCurrentPropertyID)
			AND g_ExteriorHeliDockData.iBuildingID = GET_PROPERTY_BUILDING(iCurrentPropertyID)
			
				RUN_BUILDING_HELI_DOCK_CONTROL(g_ExteriorHeliDockData)
				
				IF HAS_HELI_DOCK_FINISHED(g_ExteriorHeliDockData)
					
					GET_OWNER_AND_PROPERTY_TO_ENTER_AFTER_HELI_DOCK_LANDING(g_ExteriorHeliDockData, ownerPlayerID, iPropertyToEnter)
					
					IF ownerPlayerID = INVALID_PLAYER_INDEX()
					OR HAS_NET_TIMER_EXPIRED(g_ExteriorHeliDockData.timerWaitForInvite, 20000) // this timer is really a fallback that should never happen as whole procedure includes extensive checks on owner, but... just in case, it's better than players possibly getting stuck on black screen forever
						#IF IS_DEBUG_BUILD
							CDEBUG1LN(DEBUG_PROPERTY, "[HELI_DOCK] AM_MP_PROPERTY_EXT: Player owner is no longer with us or we timed out waiting for them to invite us, canceling!")
						#ENDIF
						
						// Leave the apartment like nothing ever happened
						SET_LOCAL_STAGE(STAGE_WARP_TO_ENTRANCE_AREA_THEN_LEAVE)
						CLEANUP_HELI_DOCK_CUTSCENE(g_ExteriorHeliDockData)
						RESET_HELI_DOCK_DATA(g_ExteriorHeliDockData)
						STOP_HELI_DOCK_AUDIO_SCENES()
						
						//SET_HELI_DOCK_STATE(g_ExteriorHeliDockData, HELI_DOCK_STATE_CANCELING_LANDING)
					ELSE
						IF ownerPlayerID = PLAYER_ID()
							#IF IS_DEBUG_BUILD
								CDEBUG1LN(DEBUG_PROPERTY, "[HELI_DOCK] AM_MP_PROPERTY_EXT: We are the owner, we're inviting others to the office.")
							#ENDIF
							
							#IF IS_DEBUG_BUILD
							IF NOT g_ExteriorHeliDockData.d_bDontSendBroadcastInvite
							#ENDIF
							
							// Enter + invite others
							
							// Retrieve the variant of the property. Since this mechanism only works for offices 
							// and players can only have one office hardwired to PROPERTY_OWNED_SLOT_OFFICE_0 we can just
							// get the variant from that slot without looping through all properties and checking which
							// one we are about to enter
							globalPropertyEntryData.iVariation = GlobalplayerBD_FM[NATIVE_TO_INT(ownerPlayerID)].propertyDetails.iPropertyVariant[PROPERTY_OWNED_SLOT_OFFICE_0]
							
							iCurrentPropertyID = iPropertyToEnter
							GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iRequestEntryBS = 0
							//SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_INVITE_NEARBY_PLAYERS_INSIDE)
							BROADCAST_PROPERTY_NEARBY_PLAYERS_INVITE_EVENT(PLAYERS_INVOLVED_IN_HELI_DOCK(g_ExteriorHeliDockData, FALSE), iCurrentPropertyID, globalPropertyEntryData.iVariation, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
							SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_SPAWNING_IN_A_PROPERTY)	
							globalPropertyEntryData.ownerID = ownerPlayerID
							globalPropertyEntryData.ownerHandle = GET_GAMER_HANDLE_PLAYER(ownerPlayerID)
							//iWalkInStage = WIS_CLEANUP
							//globalPropertyEntryData.bWalkInSeqDone = TRUE
							globalPropertyEntryData.iEntrance = 0
							globalPropertyEntryData.iPropertyEntered = iPropertyToEnter
							PRINTLN("19 globalPropertyEntryData.iPropertyEntered = ",globalPropertyEntryData.iPropertyEntered)
							//globalPropertyEntryData.bInteriorWarpDone = TRUE
							//globalPropertyEntryData.bForceCleanupExterior = FALSE
							globalPropertyEntryData.iVehSlot 						= -1
							globalPropertyEntryData.iOldVehicleSlot					= -1
							globalPropertyEntryData.iCostOfPropertyJustBought 		= 0
							globalPropertyEntryData.iTradeInValueOfPreviousProperty = 0
							
							SET_BIT(globalPropertyEntryData.iBS, GLOBAL_PROPERTY_ENTRY_BS_ENTER_FROM_HELICOPTER)
							
							CLEAR_TRANSITION_SESSION_DETAILS_ON_ENTRY_IF_SUITABLE(1)
							
							CLEANUP_SCRIPT()
							
							#IF IS_DEBUG_BUILD
							ELSE
								#IF IS_DEBUG_BUILD
									CDEBUG1LN(DEBUG_PROPERTY, "[HELI_DOCK] AM_MP_PROPERTY_EXT: d_bDontSendBroadcastInvite is TRUE so we'll just walk out")
								#ENDIF
								SET_LOCAL_STAGE(STAGE_WARP_TO_ENTRANCE_AREA_THEN_LEAVE)
								CLEANUP_HELI_DOCK_CUTSCENE(g_ExteriorHeliDockData)
								RESET_HELI_DOCK_DATA(g_ExteriorHeliDockData)
							ENDIF
							#ENDIF
							
						ELSE
							// Wait for being invivted
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		
		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			
			SWITCH serverBD.iStage
				CASE SERVER_STAGE_INIT
					IF SERVER_CREATE_EXTERIOR_OBJECTS()
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Server created exterior objects")
						serverBD.iStage = SERVER_STAGE_RUNNING
					ENDIF
				BREAK
				
				CASE SERVER_STAGE_RUNNING
					PROCESS_BODYGUARD()	
					
					MAINTAIN_DOOR_USAGE_REQUESTS_SERVER()
				BREAK
			
			ENDSWITCH
		ENDIF
	ENDWHILE

ENDSCRIPT
//EOF
