//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        am_luxury_showroom.sch																				//
// Description: Script for maintaining the luxury showroom located opposite the music studio. Resposible for 		// 
//				creating vehicles, managing transactions, prompt, browsing etc. 									//
//																													//
// Written by:  Joe Davis																							//
// Date:  		17/02/2022																							//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "mp_globals_new_features_tu.sch"

#IF FEATURE_DLC_1_2022

USING "luxury_showroom_public.sch"
USING "net_purchase_vehicle.sch"
USING "fmmc_header.sch"
USING "Summer2022/vehicle_overhead_display.sch"

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════════════╡ CONSTS ╞═════════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

// m_sLocalData.iBS
CONST_INT BS_LUXURY_SHOWROOM_PURCHASING_FROM_BROWSE_STATE					0
CONST_INT BS_LUXURY_SHOWROOM_MENU_DELAY_TIMER_ACTIVE						1
CONST_INT BS_LUXURY_SHOWROOM_MENU_OPTION_ACCEPTED							2
CONST_INT BS_LUXURY_SHOWROOM_MENU_OPTION_BACK								3
CONST_INT BS_LUXURY_SHOWROOM_MENU_REBUILD									4
CONST_INT BS_LUXURY_SHOWROOM_SWITCHED_VEHICLES								5
CONST_INT BS_LUXURY_SHOWROOM_START_HEADING_OFFSET_ON_LEFT					6

CONST_INT ciNUM_SHOWCASE_VEHICLES			2

CONST_FLOAT cfPROMPT_HEADING_LEEWAY 		50.0

CONST_INT ciSHOWROOM_ENTER_MENU_OPTION_BROWSE			0
CONST_INT ciSHOWROOM_ENTER_MENU_OPTION_PURCHASE			1

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════════════╡ ENUMS ╞══════════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

ENUM SHOWROOM_STATE
	SHOWROOM_STATE_CREATE_VEHICLES,
	SHOWROOM_STATE_IDLE,
	SHOWROOM_STATE_DISPLAY_MENU,
	SHOWROOM_STATE_BROWSE_VEHICLE,
	SHOWROOM_STATE_PURCHASE_VEHICLE
ENDENUM

ENUM PROMPT_TRIGGER_LOCATES
	PROMPT_TRIGGER_LEFT_WINDOW = 0,
	PROMPT_TRIGGER_RIGHT_WINDOW,
	PROMPT_TRIGGER_SIDE_WINDOW,
	
	PROMPT_TRIGGER_COUNT
ENDENUM

ENUM ENTER_MENU_LAYER
	EML_DEFAULT,
	EML_CONFIRM_PURCHASE
ENDENUM

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════════════╡ STRUCTS ╞════════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

STRUCT ENTER_MENU_DATA
	INT iCurrentSelection
	SCRIPT_TIMER tAnalogueDelayTimer
	ENTER_MENU_LAYER eLayer = EML_DEFAULT
ENDSTRUCT

STRUCT LUXURY_SHOWROOM_CAM_DATA
	CAMERA_INDEX camera
	CAMERA_INDEX cameraInterp
	
	FLOAT fCamZoomAlpha
	FLOAT fRotationAngle
	FLOAT fZCamOffset
	FLOAT fZCamOffsetInterp
	FLOAT fHeading
	FLOAT fFinalRot
	FLOAT fFinalRotInterp
	
	SCALEFORM_INDEX scaleformButtons
	SCALEFORM_INSTRUCTIONAL_BUTTONS sButtons
ENDSTRUCT

STRUCT SERVER_BROADCAST_DATA
	INT iBS
ENDSTRUCT
SERVER_BROADCAST_DATA serverBD

STRUCT PLAYER_BROADCAST_DATA
	INT iBS
ENDSTRUCT
PLAYER_BROADCAST_DATA playerBD[NUM_NETWORK_PLAYERS]

STRUCT LUXURY_SHOWROOM_DATA
	INT iBS, iPromptStagger, iVehToPurchase
	INT iEnterInput = NEW_CONTEXT_INTENTION
	INT iVehPrice = -1
	STRING strVehName
	
	VEHICLE_INDEX vehShowcase[ciNUM_SHOWCASE_VEHICLES]
	
	SHOWROOM_STATE eFlowState = SHOWROOM_STATE_CREATE_VEHICLES
	
	VEHICLE_PURCHASE_STRUCT sBuyVehicle
	LUXURY_SHOWROOM_CAM_DATA sCamData
	ENTER_MENU_DATA sEnterMenu
		
	ABOVE_CAR_STATS_DISPLAY aboveCarStatsDisplay
	PLAYER_NAMES_SCALEFORM_DATA namesDisplayData
ENDSTRUCT
LUXURY_SHOWROOM_DATA m_sLocalData

#IF IS_DEBUG_BUILD
STRUCT LUXURY_SHOWROOM_VEHICLE_DEBUG_DATA
	VECTOR vCoords
	VECTOR vCachedCoords
	VECTOR vRotation
	VECTOR vCachedRotation
ENDSTRUCT

STRUCT LUXURY_SHOWROOM_CAMERA_DEBUG_DATA
	BOOL bEnableDebug
	BOOL bDrawDebug
	BOOL bOutputCamData
	BOOL bResetValues
	
	INT iSwitchInterpDuration
	INT iSwitchGraphType
	VECTOR vPanOffset
	FLOAT fDistanceFromVeh
	FLOAT fMaxFOV
	FLOAT fMinFOV
	FLOAT fStartHeadingOffset
	FLOAT fNearDOF
	FLOAT fFarDOF
	FLOAT fMinZOffset
	FLOAT fMaxZOffset
	FLOAT fShakeAmp
	FLOAT fDOFStrength
	FLOAT fInterpAlpha
	FLOAT fZoomSpeed
ENDSTRUCT

STRUCT LUXURY_SHOWROOM_DEBUG_DATA
	BOOL bEnableDebug
	BOOL bOutputCoords
	BOOL bDrawOnScreenDebug
	
	LUXURY_SHOWROOM_VEHICLE_DEBUG_DATA sVehicleDebugData[ciNUM_SHOWCASE_VEHICLES]
	LUXURY_SHOWROOM_CAMERA_DEBUG_DATA sCamDebug
ENDSTRUCT
LUXURY_SHOWROOM_DEBUG_DATA m_sDebugData
#ENDIF

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════════════╡ DATA ╞══════════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Returns the model of the given showcase vehicle index, controlled by tunables. 
FUNC MODEL_NAMES GET_SHOWCASE_VEHICLE_MODEL(INT iVehIndex)
	SWITCH iVehIndex
		CASE 0		RETURN INT_TO_ENUM(MODEL_NAMES, g_sMPTunables.iLUXURY_SHOWCASE_VEHICLE_1_MODEL_HASH)	BREAK  	// Left Window
		CASE 1		RETURN INT_TO_ENUM(MODEL_NAMES, g_sMPTunables.iLUXURY_SHOWCASE_VEHICLE_2_MODEL_HASH)	BREAK	// Right Window
	ENDSWITCH
	
	ASSERTLN("[AM_LUXURY_SHOWROOM] GET_SHOWCASE_VEHICLE_MODEL - Invalid vehicle index.")
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

/// PURPOSE:
///    Returns the coords of the given showcase vehicle index.. 
FUNC VECTOR GET_SHOWCASE_VEHICLE_COORDS(INT iVehIndex)
	SWITCH iVehIndex
		CASE 0		RETURN <<-790.3807, -236.1494, 37.0000>> // Left Window
		CASE 1		RETURN <<-786.5285, -243.1308, 37.0000>> // Right Window
	ENDSWITCH
	
	ASSERTLN("[AM_LUXURY_SHOWROOM] GET_SHOWCASE_VEHICLE_COORDS - Invalid vehicle index.")
	RETURN ZERO_VECTOR()
ENDFUNC

/// PURPOSE:
///    Returns the rotation of the given showcase vehicle index.. 
FUNC VECTOR GET_SHOWCASE_VEHICLE_ROTATION(INT iVehIndex)
	SWITCH iVehIndex
		CASE 0		RETURN <<-0.0000, -0.0000, 160.0000>> // Left Window
		CASE 1		RETURN <<0.0000, -0.0000, 90.0000>> // Right Window
	ENDSWITCH
	
	ASSERTLN("[AM_LUXURY_SHOWROOM] GET_SHOWCASE_VEHICLE_ROTATION - Invalid vehicle index.")
	RETURN ZERO_VECTOR()
ENDFUNC

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════════════╡ DEBUG ╞══════════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

#IF IS_DEBUG_BUILD

DEBUGONLY FUNC STRING GET_SHOWROOM_STATE_AS_STRING(SHOWROOM_STATE eState)
	SWITCH eState
		CASE SHOWROOM_STATE_CREATE_VEHICLES					RETURN "SHOWROOM_STATE_CREATE_VEHICLES"
		CASE SHOWROOM_STATE_IDLE							RETURN "SHOWROOM_STATE_IDLE"
		CASE SHOWROOM_STATE_DISPLAY_MENU					RETURN "SHOWROOM_STATE_DISPLAY_MENU"
		CASE SHOWROOM_STATE_BROWSE_VEHICLE					RETURN "SHOWROOM_STATE_BROWSE_VEHICLE"
		CASE SHOWROOM_STATE_PURCHASE_VEHICLE				RETURN "SHOWROOM_STATE_PURCHASE_VEHICLE"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

DEBUGONLY PROC INIT_SHOWCASE_VEHICLE_DEBUG_POSITIONS(LUXURY_SHOWROOM_VEHICLE_DEBUG_DATA &DebugData, VECTOR vCoords, VECTOR vRotation)
	DebugData.vCoords 			= vCoords
	DebugData.vCachedCoords 	= vCoords
	DebugData.vRotation			= vRotation
	DebugData.vCachedRotation	= vRotation
ENDPROC

DEBUGONLY PROC INIT_DEBUG_CAM_VALUES()
	m_sDebugData.sCamDebug.iSwitchGraphType			= 1
	m_sDebugData.sCamDebug.iSwitchInterpDuration 	= 300
	m_sDebugData.sCamDebug.vPanOffset 				= <<-0.5, 0.0, 0>>
	m_sDebugData.sCamDebug.fDistanceFromVeh 		= 4.0
	m_sDebugData.sCamDebug.fMaxFOV 					= 42.5
	m_sDebugData.sCamDebug.fMinFOV 					= 32.5
	m_sDebugData.sCamDebug.fStartHeadingOffset		= 45.0
	m_sDebugData.sCamDebug.fNearDOF 				= 0.15
	m_sDebugData.sCamDebug.fFarDOF 					= 10.0
	m_sDebugData.sCamDebug.fMinZOffset 				= 0.7
	m_sDebugData.sCamDebug.fMaxZOffset 				= 3.5
	m_sDebugData.sCamDebug.fShakeAmp 				= 0.1
	m_sDebugData.sCamDebug.fDOFStrength 			= 0.88
	m_sDebugData.sCamDebug.fInterpAlpha 			= 0.125
	m_sDebugData.sCamDebug.fZoomSpeed 				= 0.05
ENDPROC

DEBUGONLY PROC INIT_LUXURY_SHOWROOM_DEBUG_DATA()
	INT iVeh
	REPEAT ciNUM_SHOWCASE_VEHICLES iVeh
		INIT_SHOWCASE_VEHICLE_DEBUG_POSITIONS(m_sDebugData.sVehicleDebugData[iVeh], GET_SHOWCASE_VEHICLE_COORDS(iVeh), GET_SHOWCASE_VEHICLE_ROTATION(iVeh))
	ENDREPEAT
	
	INIT_DEBUG_CAM_VALUES()
ENDPROC

DEBUGONLY PROC CREATE_LUXURY_SHOWROOM_WIDGETS()
	INIT_LUXURY_SHOWROOM_DEBUG_DATA()
	
	START_WIDGET_GROUP("am_luxury_showroom")
		START_WIDGET_GROUP("Script")
			ADD_WIDGET_BOOL("Enable Debug", m_sDebugData.bEnableDebug)
			ADD_WIDGET_BOOL("Draw On Screen Debug", m_sDebugData.bDrawOnScreenDebug)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Vehicle Placement")
			ADD_WIDGET_BOOL("Output Placement Coords", m_sDebugData.bOutputCoords)
			
			START_WIDGET_GROUP("Left Vehicle")
				ADD_WIDGET_VECTOR_SLIDER("Coords", m_sDebugData.sVehicleDebugData[0].vCoords, -99999.9, 99999.9, 0.01)
				ADD_WIDGET_VECTOR_SLIDER("Rotation", m_sDebugData.sVehicleDebugData[0].vRotation, -360.9, 360.9, 0.1)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Right Vehicle")
				ADD_WIDGET_VECTOR_SLIDER("Coords", m_sDebugData.sVehicleDebugData[1].vCoords, -99999.9, 99999.9, 0.01)
				ADD_WIDGET_VECTOR_SLIDER("Rotation", m_sDebugData.sVehicleDebugData[1].vRotation, -360.9, 360.9, 0.1)
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Camera Debug")
			ADD_WIDGET_STRING("Toggles")
			ADD_WIDGET_BOOL("Enable Debug", m_sDebugData.sCamDebug.bEnableDebug)
			ADD_WIDGET_BOOL("Draw Debug", m_sDebugData.sCamDebug.bDrawDebug)
			ADD_WIDGET_BOOL("Output Data", m_sDebugData.sCamDebug.bOutputCamData)
			ADD_WIDGET_BOOL("Reset Values", m_sDebugData.sCamDebug.bResetValues)
			
			ADD_WIDGET_STRING("When using LB / RB to switch between vehicles")
			ADD_WIDGET_INT_SLIDER("Switch Interp Duration", m_sDebugData.sCamDebug.iSwitchInterpDuration, 0, 5000, 10)
			ADD_WIDGET_INT_SLIDER("Switch Interp Type", m_sDebugData.sCamDebug.iSwitchGraphType, 0, ENUM_TO_INT(GRAPH_TYPE_MAX) - 1, 1)
			
			ADD_WIDGET_STRING("The Initial vehicle angle when inspecting vehicle.")
			ADD_WIDGET_FLOAT_SLIDER("Start Heading Offset", m_sDebugData.sCamDebug.fStartHeadingOffset, -360.0, 360.0, 1.0)
			
			ADD_WIDGET_STRING("Zoom")
			ADD_WIDGET_FLOAT_SLIDER("Min FOV", m_sDebugData.sCamDebug.fMinFOV, 0, 150.0, 1.0)
			ADD_WIDGET_FLOAT_SLIDER("Max FOV", m_sDebugData.sCamDebug.fMaxFOV, 0, 150.0, 1.0)
			ADD_WIDGET_FLOAT_SLIDER("Zoom Alpha", m_sDebugData.sCamDebug.fZoomSpeed, 0, 1.0, 0.01)
			
			ADD_WIDGET_STRING("DOF")
			ADD_WIDGET_FLOAT_SLIDER("Near DOF", m_sDebugData.sCamDebug.fNearDOF, 0, 100.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Far DOF", m_sDebugData.sCamDebug.fFarDOF, 0, 100.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("DOF Strength", m_sDebugData.sCamDebug.fDOFStrength, 0, 1.0, 0.01)
			
			ADD_WIDGET_STRING("Camera Height / look speed.")
			ADD_WIDGET_FLOAT_SLIDER("Min Z Offset", m_sDebugData.sCamDebug.fMinZOffset, 0, 10.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Max Z Offset", m_sDebugData.sCamDebug.fMaxZOffset, 0, 10.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Look Speed", m_sDebugData.sCamDebug.fInterpAlpha, 0, 1.0, 0.01)
			
			ADD_WIDGET_STRING("Shake is set on initialisation")
			ADD_WIDGET_FLOAT_SLIDER("Shake Amplitude", m_sDebugData.sCamDebug.fShakeAmp, 0, 1.0, 0.01)
			
			ADD_WIDGET_STRING("Position related to vehicle coords")
			ADD_WIDGET_VECTOR_SLIDER("Pan Offset", m_sDebugData.sCamDebug.vPanOffset, -9999.0, 9999.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Distance From Vehicle", m_sDebugData.sCamDebug.fDistanceFromVeh, 0, 4.5, 0.1)
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
ENDPROC

DEBUGONLY PROC UPDATE_SHOWCASE_VEHICLE_DEBUG_POSITION(LUXURY_SHOWROOM_VEHICLE_DEBUG_DATA &sDebugData, VEHICLE_INDEX &Vehicle)
	
	IF IS_ENTITY_ALIVE(Vehicle)
		IF NOT ARE_VECTORS_EQUAL(sDebugData.vCoords, sDebugData.vCachedCoords)
			sDebugData.vCachedCoords = sDebugData.vCoords
			SET_ENTITY_COORDS_NO_OFFSET(Vehicle, sDebugData.vCoords)
		ENDIF
		
		IF NOT ARE_VECTORS_EQUAL(sDebugData.vRotation, sDebugData.vCachedRotation)
			sDebugData.vCachedRotation = sDebugData.vRotation
			SET_ENTITY_ROTATION(Vehicle, sDebugData.vRotation)
		ENDIF
	ENDIF
	
ENDPROC

DEBUGONLY PROC OUTPUT_SHOWCASE_VEHICLE_DEBUG_POSITIONS()
	INT iVeh
	OPEN_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("-------- LUXURY SHOWCASE VEHICLE COORDS ----------")
		REPEAT ciNUM_SHOWCASE_VEHICLES iVeh
			TEXT_LABEL_31 tlVeh = "Vehicle - "
			tlVeh += GET_STRING_FROM_INT(iVeh)
			
			IF iVeh = 0
				tlVeh += " - Left window"
			ELSE
				tlVeh += " - Right window"
			ENDIF
			
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE(tlVeh)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("Coords = ") SAVE_VECTOR_TO_DEBUG_FILE(m_sDebugData.sVehicleDebugData[iVeh].vCoords)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("Rotation = ") SAVE_VECTOR_TO_DEBUG_FILE(m_sDebugData.sVehicleDebugData[iVeh].vRotation)
		ENDREPEAT
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("-------- END LUXURY SHOWCASE VEHICLE COORDS ----------")
	CLOSE_DEBUG_FILE()
	
ENDPROC

DEBUGONLY PROC UPDATE_LUXURY_SHOWROOM_WIDGETS()
	TEXT_LABEL_63 tlText
	VECTOR vTextPos = <<0.1, 0.1, 0.0>>
	VECTOR vTextOffset = << 0.0, 0.02, 0.0>>
	INT iRed 	= 0
	INT iGreen 	= 255
	INT iBlue 	= 0
	
	IF m_sDebugData.bDrawOnScreenDebug
	OR GET_COMMANDLINE_PARAM_EXISTS("sc_DrawLuxuryShowroomDebug")
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
		
		tlText = "----- Luxury Showroom Debug -----"
		vTextPos += vTextOffset
		
		tlText = "Current State: "
		tlText += GET_SHOWROOM_STATE_AS_STRING(m_sLocalData.eFlowState)
		DRAW_DEBUG_TEXT_2D(Get_String_From_TextLabel(tlText), vTextPos, iRed, iGreen, iBlue)
		vTextPos += vTextOffset
		
		tlText = "Prompt Stagger: "
		tlText += GET_STRING_FROM_INT(m_sLocalData.iPromptStagger)
		DRAW_DEBUG_TEXT_2D(Get_String_From_TextLabel(tlText), vTextPos, iRed, iGreen, iBlue)
		vTextPos += vTextOffset
		
		tlText = "Current Veh Index: "
		tlText += GET_STRING_FROM_INT(m_sLocalData.iVehToPurchase)
		DRAW_DEBUG_TEXT_2D(Get_String_From_TextLabel(tlText), vTextPos, iRed, iGreen, iBlue)
		vTextPos += vTextOffset
	ENDIF
	
	IF m_sDebugData.bEnableDebug
		INT iVeh
		REPEAT ciNUM_SHOWCASE_VEHICLES iVeh
			UPDATE_SHOWCASE_VEHICLE_DEBUG_POSITION(m_sDebugData.sVehicleDebugData[iVeh], m_sLocalData.vehShowcase[iVeh])
		ENDREPEAT
		
		IF m_sDebugData.bOutputCoords
			OUTPUT_SHOWCASE_VEHICLE_DEBUG_POSITIONS()
			m_sDebugData.bOutputCoords = FALSE
		ENDIF
	ENDIF
	
	// --- Camera ---
	IF m_sDebugData.sCamDebug.bDrawDebug
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
		
		tlText = "----- Camera Debug -----"
		vTextPos += vTextOffset
		
		tlText = "Switch Interp Duration: "
		tlText += GET_STRING_FROM_INT(m_sDebugData.sCamDebug.iSwitchInterpDuration)
		DRAW_DEBUG_TEXT_2D(Get_String_From_TextLabel(tlText), vTextPos, iRed, iGreen, iBlue)
		vTextPos += vTextOffset
		
		tlText = "Switch Graph Type: "
		tlText += GET_CAMERA_GRAPH_TYPE_NAME(INT_TO_ENUM(CAMERA_GRAPH_TYPE, m_sDebugData.sCamDebug.iSwitchGraphType))
		DRAW_DEBUG_TEXT_2D(Get_String_From_TextLabel(tlText), vTextPos, iRed, iGreen, iBlue)
		vTextPos += vTextOffset
		vTextPos += vTextOffset
		
		tlText = "Initial Heading Offset: "
		tlText += GET_STRING_FROM_FLOAT(m_sDebugData.sCamDebug.fStartHeadingOffset)
		DRAW_DEBUG_TEXT_2D(Get_String_From_TextLabel(tlText), vTextPos, iRed, iGreen, iBlue)
		vTextPos += vTextOffset
		
		tlText = "Min FOV: "
		tlText += GET_STRING_FROM_FLOAT(m_sDebugData.sCamDebug.fMinFOV)
		DRAW_DEBUG_TEXT_2D(Get_String_From_TextLabel(tlText), vTextPos, iRed, iGreen, iBlue)
		vTextPos += vTextOffset
		
		tlText = "Max FOV: "
		tlText += GET_STRING_FROM_FLOAT(m_sDebugData.sCamDebug.fMaxFOV)
		DRAW_DEBUG_TEXT_2D(Get_String_From_TextLabel(tlText), vTextPos, iRed, iGreen, iBlue)
		vTextPos += vTextOffset
		
		tlText = "Zoom Speed: "
		tlText += GET_STRING_FROM_FLOAT(m_sDebugData.sCamDebug.fZoomSpeed)
		DRAW_DEBUG_TEXT_2D(Get_String_From_TextLabel(tlText), vTextPos, iRed, iGreen, iBlue)
		vTextPos += vTextOffset
		vTextPos += vTextOffset
		
		tlText = "Near DOF: "
		tlText += GET_STRING_FROM_FLOAT(m_sDebugData.sCamDebug.fNearDOF)
		DRAW_DEBUG_TEXT_2D(Get_String_From_TextLabel(tlText), vTextPos, iRed, iGreen, iBlue)
		vTextPos += vTextOffset
		
		tlText = "Far DOF: "
		tlText += GET_STRING_FROM_FLOAT(m_sDebugData.sCamDebug.fFarDOF)
		DRAW_DEBUG_TEXT_2D(Get_String_From_TextLabel(tlText), vTextPos, iRed, iGreen, iBlue)
		vTextPos += vTextOffset
		
		tlText = "DOF Strength: "
		tlText += GET_STRING_FROM_FLOAT(m_sDebugData.sCamDebug.fDOFStrength)
		DRAW_DEBUG_TEXT_2D(Get_String_From_TextLabel(tlText), vTextPos, iRed, iGreen, iBlue)
		vTextPos += vTextOffset
		vTextPos += vTextOffset
		
		tlText = "Min Z Offset: "
		tlText += GET_STRING_FROM_FLOAT(m_sDebugData.sCamDebug.fMinZOffset)
		DRAW_DEBUG_TEXT_2D(Get_String_From_TextLabel(tlText), vTextPos, iRed, iGreen, iBlue)
		vTextPos += vTextOffset
		
		tlText = "Max Z Offset: "
		tlText += GET_STRING_FROM_FLOAT(m_sDebugData.sCamDebug.fMaxZOffset)
		DRAW_DEBUG_TEXT_2D(Get_String_From_TextLabel(tlText), vTextPos, iRed, iGreen, iBlue)
		vTextPos += vTextOffset
		
		tlText = "Look Speed: "
		tlText += GET_STRING_FROM_FLOAT(m_sDebugData.sCamDebug.fInterpAlpha)
		DRAW_DEBUG_TEXT_2D(Get_String_From_TextLabel(tlText), vTextPos, iRed, iGreen, iBlue)
		vTextPos += vTextOffset
		
		tlText = "Shake Amplitude: "
		tlText += GET_STRING_FROM_FLOAT(m_sDebugData.sCamDebug.fShakeAmp)
		DRAW_DEBUG_TEXT_2D(Get_String_From_TextLabel(tlText), vTextPos, iRed, iGreen, iBlue)
		vTextPos += vTextOffset
		vTextPos += vTextOffset
		
		tlText = "Pan Offset: "
		tlText += GET_STRING_FROM_VECTOR(m_sDebugData.sCamDebug.vPanOffset)
		DRAW_DEBUG_TEXT_2D(Get_String_From_TextLabel(tlText), vTextPos, iRed, iGreen, iBlue)
		vTextPos += vTextOffset
		
		tlText = "Distance From Vehicle: "
		tlText += GET_STRING_FROM_FLOAT(m_sDebugData.sCamDebug.fDistanceFromVeh)
		DRAW_DEBUG_TEXT_2D(Get_String_From_TextLabel(tlText), vTextPos, iRed, iGreen, iBlue)
		vTextPos += vTextOffset
		
	ENDIF
	
	IF m_sDebugData.sCamDebug.bResetValues
		INIT_DEBUG_CAM_VALUES()
		m_sDebugData.sCamDebug.bResetValues = FALSE
	ENDIF
	
	IF m_sDebugData.sCamDebug.bOutputCamData
		OPEN_DEBUG_FILE()
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("-------- LUXURY SHOWROOM CAM DATA ----------")
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("iSwitchInterpDuration = ") SAVE_INT_TO_DEBUG_FILE(m_sDebugData.sCamDebug.iSwitchInterpDuration)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("eGraphType = ") SAVE_STRING_TO_DEBUG_FILE(GET_CAMERA_GRAPH_TYPE_NAME(INT_TO_ENUM(CAMERA_GRAPH_TYPE, m_sDebugData.sCamDebug.iSwitchGraphType)))
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("vPanOffset = ") SAVE_VECTOR_TO_DEBUG_FILE(m_sDebugData.sCamDebug.vPanOffset)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("fDistanceFromVeh = ") SAVE_FLOAT_TO_DEBUG_FILE(m_sDebugData.sCamDebug.fDistanceFromVeh)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("fMaxFOV = ") SAVE_FLOAT_TO_DEBUG_FILE(m_sDebugData.sCamDebug.fMaxFOV)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("fMinFOV = ") SAVE_FLOAT_TO_DEBUG_FILE(m_sDebugData.sCamDebug.fMinFOV)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("fStartHeadingOffset = ") SAVE_FLOAT_TO_DEBUG_FILE(m_sDebugData.sCamDebug.fStartHeadingOffset)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("fNearDOF = ") SAVE_FLOAT_TO_DEBUG_FILE(m_sDebugData.sCamDebug.fNearDOF)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("fFarDOF = ") SAVE_FLOAT_TO_DEBUG_FILE(m_sDebugData.sCamDebug.fFarDOF)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("fMinZOffset = ") SAVE_FLOAT_TO_DEBUG_FILE(m_sDebugData.sCamDebug.fMinZOffset)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("fMaxZOffset = ") SAVE_FLOAT_TO_DEBUG_FILE(m_sDebugData.sCamDebug.fMaxZOffset)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("fShakeAmp = ") SAVE_FLOAT_TO_DEBUG_FILE(m_sDebugData.sCamDebug.fShakeAmp)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("fDOFStrength = ") SAVE_FLOAT_TO_DEBUG_FILE(m_sDebugData.sCamDebug.fDOFStrength)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("fInterpAlpha = ") SAVE_FLOAT_TO_DEBUG_FILE(m_sDebugData.sCamDebug.fInterpAlpha)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("fZoomSpeed = ") SAVE_FLOAT_TO_DEBUG_FILE(m_sDebugData.sCamDebug.fZoomSpeed)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("-------- END OF LUXURY SHOWROOM CAM DATA ----------")
			SAVE_NEWLINE_TO_DEBUG_FILE()
		CLOSE_DEBUG_FILE()
	
		m_sDebugData.sCamDebug.bOutputCamData = FALSE
	ENDIF
ENDPROC

#ENDIF // #IF IS_DEBUG_BUILD

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════════════╡ FUNCTIONS ╞══════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

// ========= TRIGGER LOCATES ==========

/// PURPOSE:
///    Returns the prompt trigger enum based on the current stagger (updated each frame while in idle state).
FUNC PROMPT_TRIGGER_LOCATES GET_PROMPT_TRIGGER_FROM_STAGGER()
	RETURN INT_TO_ENUM(PROMPT_TRIGGER_LOCATES, m_sLocalData.iPromptStagger)
ENDFUNC

/// PURPOSE:
///    Grabs the loacte data for each prompt trigger.
PROC GET_PROMPT_TRIGGER_LOCATE_DATA(PROMPT_TRIGGER_LOCATES ePromptLocate, VECTOR &VecCoors1, VECTOR &VecCoors2, FLOAT &fWidth)
	SWITCH ePromptLocate
		CASE PROMPT_TRIGGER_LEFT_WINDOW
			VecCoors1 	= <<-796.557251,-234.641235,35.940319>>
			VecCoors2 	= <<-792.602600,-241.474136,39.506424>>
			fWidth		= 3.000000
		BREAK
		CASE PROMPT_TRIGGER_RIGHT_WINDOW
			VecCoors1 	= <<-792.120911,-242.443405,35.932930>>
			VecCoors2 	= <<-789.096191,-247.521255,39.440041>>
			fWidth		= 3.000000
		BREAK
		CASE PROMPT_TRIGGER_SIDE_WINDOW
			VecCoors1 	= <<-786.542175,-248.434769,35.991272>>
			VecCoors2 	= <<-780.420166,-245.935638,39.440041>>
			fWidth		= 3.000000
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Grabs the vehicle index associated with each prompt trigger. So 2 prompt triggers can be used to interact with 1 vehicle. 
FUNC INT GET_SHOWCASE_VEHICLE_INDEX_FOR_ENTRY_LOCATE(PROMPT_TRIGGER_LOCATES eLocate)
	
	SWITCH eLocate
		CASE PROMPT_TRIGGER_LEFT_WINDOW
			RETURN 0
		BREAK
		CASE PROMPT_TRIGGER_RIGHT_WINDOW
		CASE PROMPT_TRIGGER_SIDE_WINDOW
			RETURN 1
		BREAK
	ENDSWITCH
	
	ASSERTLN("[AM_LUXURY_SHOWROOM] GET_SHOWCASE_VEHICLE_INDEX_FOR_ENTRY_LOCATE - Invalid eLocate: ", ENUM_TO_INT(eLocate))
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Contains various checks to determine if the player should trigger the prompt.
FUNC BOOL IS_LOCAL_PLAYER_IN_PROMPT_TRIGGER_LOCATE(BOOL bCheckHeading = TRUE)
	VECTOR vLocate1, vLocate2
	FLOAT fWidth
	
	GET_PROMPT_TRIGGER_LOCATE_DATA(GET_PROMPT_TRIGGER_FROM_STAGGER(), vLocate1, vLocate2, fWidth)
	
	IF IS_VECTOR_ZERO(vLocate1)
	OR IS_VECTOR_ZERO(vLocate2)
		ASSERTLN("[AM_LUXURY_SHOWROOM] IS_LOCAL_PLAYER_IN_PROMPT_TRIGGER_LOCATE - Zero vector for index: ", m_sLocalData.iPromptStagger)
		RETURN FALSE
	ENDIF
	
	IF NOT IS_ENTITY_ALIVE(PLAYER_PED_ID())
		RETURN FALSE
	ENDIF
	
	IF IS_PED_RAGDOLL(PLAYER_PED_ID())
		RETURN FALSE
	ENDIF
	
	IF bCheckHeading
		INT iVehIndex = GET_SHOWCASE_VEHICLE_INDEX_FOR_ENTRY_LOCATE(GET_PROMPT_TRIGGER_FROM_STAGGER())
		
		IF iVehIndex < 0 OR iVehIndex >= ciNUM_SHOWCASE_VEHICLES
			RETURN FALSE
		ENDIF
		
		IF NOT IS_ENTITY_ALIVE(m_sLocalData.vehShowcase[iVehIndex])
			RETURN FALSE
		ENDIF
		
		VECTOR vTargetHeadingCoord = GET_ENTITY_COORDS(m_sLocalData.vehShowcase[iVehIndex])
		VECTOR vPedCoord = GET_ENTITY_COORDS(PLAYER_PED_ID())
		FLOAT fPedHeading = GET_ENTITY_HEADING(PLAYER_PED_ID())
		FLOAT fDirHeading = GET_HEADING_BETWEEN_VECTORS_2D(vPedCoord, vTargetHeadingCoord)
		
		IF (IS_HEADING_ACCEPTABLE_CORRECTED(fDirHeading, fPedHeading, cfPROMPT_HEADING_LEEWAY))
		AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vLocate1, vLocate2, fWidth, DEFAULT, DEFAULT, TM_ON_FOOT)
			RETURN TRUE
		ENDIF
	ELSE
		RETURN IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vLocate1, vLocate2, fWidth, DEFAULT, DEFAULT, TM_ON_FOOT)
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Wrapper for setting the main showroom flow state. 
PROC SET_SHOWROOM_STATE(SHOWROOM_STATE eNewState)
	IF m_sLocalData.eFlowState != eNewState
		PRINTLN("[AM_LUXURY_SHOWROOM] SET_SHOWROOM_STATE - Setting state from ", GET_SHOWROOM_STATE_AS_STRING(m_sLocalData.eFlowState), " to ", GET_SHOWROOM_STATE_AS_STRING(eNewState))
		m_sLocalData.eFlowState = eNewState
	ENDIF
ENDPROC

/// PURPOSE:
///    Updates various staggers declared in m_sLocalData.
PROC MAINTAIN_SCRIPT_STAGGERS()
	IF m_sLocalData.eFlowState = SHOWROOM_STATE_IDLE
	AND NOT IS_LOCAL_PLAYER_IN_PROMPT_TRIGGER_LOCATE()
		m_sLocalData.iPromptStagger = (m_sLocalData.iPromptStagger + 1) % ENUM_TO_INT(PROMPT_TRIGGER_COUNT)
	ENDIF
ENDPROC

/// PURPOSE:
///    Stores the vehicle index for purchasing, and heading for prompt. 
FUNC VEHICLE_INDEX GET_SHOWCASE_VEHICLE_TO_PURCHASE()
	IF m_sLocalData.iVehToPurchase < 0 OR m_sLocalData.iVehToPurchase >= ciNUM_SHOWCASE_VEHICLES
		RETURN NULL
	ENDIF
	
	RETURN m_sLocalData.vehShowcase[m_sLocalData.iVehToPurchase]
ENDFUNC

PROC STORE_PRICE_OF_SHOWCASE_VEHICLE(VEHICLE_INDEX veh)
	INT iNewPrice = GET_PRICE_OF_VEHICLE(m_sLocalData.sBuyVehicle, veh)
	
	IF iNewPrice = -1
		ASSERTLN("[AM_LUXURY_SHOWROOM] STORE_PRICE_OF_SHOWCASE_VEHICLE - iNewPrice is -1. Is vehicle alive?")
	ENDIF
	
	IF m_sLocalData.iVehPrice != iNewPrice
		m_sLocalData.iVehPrice = iNewPrice
	ENDIF
ENDPROC

PROC STORE_NAME_OF_SHOWCASE_VEHICLE(VEHICLE_INDEX veh)
	IF IS_ENTITY_ALIVE(veh)
		m_sLocalData.strVehName = GET_NAME_OF_VEHICLE(GET_ENTITY_MODEL(veh))
	ELSE
		m_sLocalData.strVehName = ""
	ENDIF
ENDPROC

// ========= VEHICLES ==========

/// PURPOSE:
///    Returns all the mod, colour, livery etc data for each vehicle. 
PROC GET_SHOWCASE_VEHICLE_SETUP_DATA(INT iVehIndex, VEHICLE_SETUP_STRUCT_MP &sData, MODEL_NAMES eVehModel)
	SWITCH iVehIndex
		CASE 0
			sData.VehicleSetup.iColour1 								= g_sMPTunables.iLUXURY_SHOWCASE_VEHICLE_1_COLOUR_1
			sData.VehicleSetup.iColour2 								= g_sMPTunables.iLUXURY_SHOWCASE_VEHICLE_1_COLOUR_2
			sData.VehicleSetup.iColourExtra1 							= g_sMPTunables.iLUXURY_SHOWCASE_VEHICLE_1_EXTRA_COLOUR_1
			sData.VehicleSetup.iColourExtra2 							= g_sMPTunables.iLUXURY_SHOWCASE_VEHICLE_1_EXTRA_COLOUR_2
			sData.iColour5 												= g_sMPTunables.iLUXURY_SHOWCASE_VEHICLE_1_COLOUR_5
			sData.iColour6 												= g_sMPTunables.iLUXURY_SHOWCASE_VEHICLE_1_COLOUR_6
			sData.VehicleSetup.iModIndex[MOD_LIVERY] 					= g_sMPTunables.iLUXURY_SHOWCASE_VEHICLE_1_LIVERY
			sData.VehicleSetup.iWheelType								= g_sMPTunables.iLUXURY_SHOWCASE_VEHICLE_1_WHEEL_TYPE
			sData.VehicleSetup.iModIndex[MOD_WHEELS]					= g_sMPTunables.iLUXURY_SHOWCASE_VEHICLE_1_WHEEL_ID
			sData.VehicleSetup.iWindowTintColour						= g_sMPTunables.iLUXURY_SHOWCASE_VEHICLE_1_WINDOW_TINT_COLOUR
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] 		= g_sMPTunables.iLUXURY_SHOWCASE_VEHICLE_1_XENON_LIGHTS
		BREAK
		CASE 1
			sData.VehicleSetup.iColour1 								= g_sMPTunables.iLUXURY_SHOWCASE_VEHICLE_2_COLOUR_1
			sData.VehicleSetup.iColour2 								= g_sMPTunables.iLUXURY_SHOWCASE_VEHICLE_2_COLOUR_2
			sData.VehicleSetup.iColourExtra1 							= g_sMPTunables.iLUXURY_SHOWCASE_VEHICLE_2_EXTRA_COLOUR_1
			sData.VehicleSetup.iColourExtra2 							= g_sMPTunables.iLUXURY_SHOWCASE_VEHICLE_2_EXTRA_COLOUR_2
			sData.iColour5 												= g_sMPTunables.iLUXURY_SHOWCASE_VEHICLE_2_COLOUR_5
			sData.iColour6 												= g_sMPTunables.iLUXURY_SHOWCASE_VEHICLE_2_COLOUR_6
			sData.VehicleSetup.iModIndex[MOD_LIVERY] 					= g_sMPTunables.iLUXURY_SHOWCASE_VEHICLE_2_LIVERY
			sData.VehicleSetup.iWheelType								= g_sMPTunables.iLUXURY_SHOWCASE_VEHICLE_2_WHEEL_TYPE
			sData.VehicleSetup.iModIndex[MOD_WHEELS]					= g_sMPTunables.iLUXURY_SHOWCASE_VEHICLE_2_WHEEL_ID
			sData.VehicleSetup.iWindowTintColour						= g_sMPTunables.iLUXURY_SHOWCASE_VEHICLE_2_WINDOW_TINT_COLOUR
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] 		= g_sMPTunables.iLUXURY_SHOWCASE_VEHICLE_2_XENON_LIGHTS
		BREAK
	ENDSWITCH
	
	ADD_PURCHASE_VEHICLE_EXTRAS(eVehModel, sData.VehicleSetup)
ENDPROC

/// PURPOSE:
///    Deletes all showcase vehicles.
PROC DELETE_SHOWCASE_VEHICLES()
	INT i
	REPEAT ciNUM_SHOWCASE_VEHICLES i
		IF DOES_ENTITY_EXIST(m_sLocalData.vehShowcase[i])
			DELETE_VEHICLE(m_sLocalData.vehShowcase[i])
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Creates the vehicles (locally) that appear behind the glass windows in the luxury showroom. 
/// RETURNS:
///    TRUE once created.
FUNC BOOL CREATE_SHOWCASE_VEHICLES()
	
	VEHICLE_SETUP_STRUCT_MP sSetupData
	
	INT i
	REPEAT ciNUM_SHOWCASE_VEHICLES i
		IF NOT IS_ENTITY_ALIVE(m_sLocalData.vehShowcase[i])
			MODEL_NAMES eVehModel = GET_SHOWCASE_VEHICLE_MODEL(i)
			
			IF eVehModel != DUMMY_MODEL_FOR_SCRIPT
			AND IS_MODEL_IN_CDIMAGE(eVehModel)
			AND IS_MODEL_A_VEHICLE(eVehModel)
				IF REQUEST_LOAD_MODEL(eVehModel)
					
					IF NETWORK_IS_IN_MP_CUTSCENE()
						SET_NETWORK_CUTSCENE_ENTITIES(TRUE)
					ENDIF
					
					m_sLocalData.vehShowcase[i] = CREATE_VEHICLE(eVehModel, GET_SHOWCASE_VEHICLE_COORDS(i), DEFAULT, FALSE, FALSE)
					
					// --- Positioning ---
					SET_ENTITY_COORDS_NO_OFFSET(m_sLocalData.vehShowcase[i], GET_SHOWCASE_VEHICLE_COORDS(i))
					SET_ENTITY_ROTATION(m_sLocalData.vehShowcase[i], GET_SHOWCASE_VEHICLE_ROTATION(i))
					FREEZE_ENTITY_POSITION(m_sLocalData.vehShowcase[i], TRUE)
					
					// --- Flags ---
					SET_ENTITY_INVINCIBLE(m_sLocalData.vehShowcase[i], TRUE)
					SET_VEHICLE_NOT_STEALABLE_AMBIENTLY(m_sLocalData.vehShowcase[i], TRUE)
					SET_ENTITY_COLLISION(m_sLocalData.vehShowcase[i], TRUE)
					SET_VEHICLE_FULLBEAM(m_sLocalData.vehShowcase[i], FALSE)
					SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(m_sLocalData.vehShowcase[i], TRUE)
					SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(m_sLocalData.vehShowcase[i], TRUE)
					SET_VEHICLE_DOORS_LOCKED(m_sLocalData.vehShowcase[i], VEHICLELOCK_CANNOT_ENTER)
					SET_VEHICLE_RADIO_ENABLED(m_sLocalData.vehShowcase[i], FALSE)
					SET_VEHICLE_DOORS_SHUT(m_sLocalData.vehShowcase[i], TRUE)
					
					// --- Appearence ---
					SET_ENTITY_CAN_BE_DAMAGED(m_sLocalData.vehShowcase[i], FALSE)
					SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(m_sLocalData.vehShowcase[i], FALSE)
					SET_VEHICLE_ENGINE_ON(m_sLocalData.vehShowcase[i], FALSE, TRUE)
					SET_VEHICLE_LIGHTS(m_sLocalData.vehShowcase[i], FORCE_VEHICLE_LIGHTS_OFF)
					SET_VEHICLE_FIXED(m_sLocalData.vehShowcase[i])
					SET_ENTITY_MAX_HEALTH(m_sLocalData.vehShowcase[i], 1000)
			        SET_ENTITY_HEALTH(m_sLocalData.vehShowcase[i], 1000)
			        SET_VEHICLE_ENGINE_HEALTH(m_sLocalData.vehShowcase[i], 1000)
			        SET_VEHICLE_PETROL_TANK_HEALTH(m_sLocalData.vehShowcase[i], 1000)
					SET_VEHICLE_DIRT_LEVEL(m_sLocalData.vehShowcase[i], 0.0)
					SET_ENTITY_VISIBLE(m_sLocalData.vehShowcase[i], TRUE)
					REQUEST_VEHICLE_HIGH_DETAIL_MODEL(m_sLocalData.vehShowcase[i])
					SET_ENTITY_PROOFS(m_sLocalData.vehShowcase[i], TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, DEFAULT, TRUE)
					
					GET_SHOWCASE_VEHICLE_SETUP_DATA(i, sSetupData, eVehModel)
					SET_VEHICLE_SETUP_MP(m_sLocalData.vehShowcase[i], sSetupData, FALSE, TRUE, TRUE)
					
					SET_MODEL_AS_NO_LONGER_NEEDED(eVehModel)
					
					IF NETWORK_IS_IN_MP_CUTSCENE()
						SET_NETWORK_CUTSCENE_ENTITIES(FALSE)
					ENDIF
					
					PRINTLN("[AM_LUXURY_SHOWROOM] CREATE_SHOWCASE_VEHICLES - Created vehicle: ", i)
				ENDIF
				
				RETURN FALSE
			ENDIF
		ELIF NOT SET_VEHICLE_ON_GROUND_PROPERLY(m_sLocalData.vehShowcase[i])
			PRINTLN("[AM_LUXURY_SHOWROOM] CREATE_SHOWCASE_VEHICLES - Placing vehicle on ground... ", i)
			RETURN FALSE
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Sets the index used for the array m_sLocalData.vehShowcase[]
PROC SET_SHOWCASE_VEHICLE_INDEX_TO_PURCHASE(INT iVehIndex)
	IF iVehIndex < 0 OR iVehIndex >= ciNUM_SHOWCASE_VEHICLES
		EXIT
	ENDIF
	
	IF m_sLocalData.iVehToPurchase != iVehIndex
		m_sLocalData.iVehToPurchase = iVehIndex
		PRINTLN("[AM_LUXURY_SHOWROOM] SET_SHOWCASE_VEHICLE_INDEX_TO_PURCHASE - m_sLocalData.iVehToPurchase = ", m_sLocalData.iVehToPurchase)
	ENDIF
ENDPROC

/// PURPOSE:
///    Contains various checks to determine if the local player can purchase a showcase vehicle.
FUNC BOOL CAN_PURCHASE_SHOWCASE_VEHICLE()
	IF IS_BIT_SET(g_sLuxuryShowroom.iBS, BS_LUXURY_SHOWROOM_GLOBAL_DISABLE_PURCHASE)
		RETURN FALSE
	ENDIF
	
	IF PLAYER_ID() = INVALID_PLAYER_INDEX()
		RETURN FALSE
	ENDIF
	
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		RETURN FALSE
	ENDIF
	
	IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	IF NOT IS_ENTITY_ALIVE(PLAYER_PED_ID())
		RETURN FALSE
	ENDIF
	
	IF NETWORK_IS_ACTIVITY_SESSION()
		RETURN FALSE
	ENDIF
	
	IF NOT IS_SCREEN_FADED_IN()
		RETURN FALSE
	ENDIF
	
	IF NOT IS_SKYSWOOP_AT_GROUND()
		RETURN FALSE
	ENDIF	
	
	IF IS_BROWSER_OPEN()
		RETURN FALSE
	ENDIF
	
	IF IS_PHONE_ONSCREEN()
		RETURN FALSE
	ENDIF	
	
	IF IS_PLAYER_CRITICAL_TO_ANY_EVENT(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	IF IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
		RETURN FALSE
	ENDIF	
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		RETURN FALSE
	ENDIF	
	
	IF IS_PAUSE_MENU_ACTIVE()
		RETURN FALSE
	ENDIF
	
	IF IS_LOCAL_PLAYER_ON_ANY_FM_MISSION()
		RETURN FALSE
	ENDIF
	
	IF GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID())
	AND GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) != -1
		RETURN FALSE
	ENDIF
	
	IF IS_INTERACTION_MENU_OPEN()
		RETURN FALSE
	ENDIF
	
	IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
		RETURN FALSE
	ENDIF
	
	IF NETWORK_IS_IN_MP_CUTSCENE()
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Returns the controls type and action for purchasing the vehicle (covers both controller and keyboard).
PROC GET_SHOWCASE_VEHICLE_CONTROLS_PURCHASE(CONTROL_TYPE &eControlType, CONTROL_ACTION &eControlAction)
	eControlType	= FRONTEND_CONTROL
	eControlAction	= INPUT_FRONTEND_ACCEPT
ENDPROC

PROC GET_SHOWCASE_VEHICLE_CONTROLS_PURCHASE_SECONDARY(CONTROL_TYPE &eControlType, CONTROL_ACTION &eControlAction)
	eControlType	= FRONTEND_CONTROL
	eControlAction	= INPUT_SCRIPT_RLEFT
ENDPROC

/// PURPOSE:
///    Returns true when the player wants to purchase a vehicle. 
FUNC BOOL IS_PURCHASE_SHOWCASE_VEH_JUST_PRESSED(BOOL bSecondaryButton = FALSE)
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
	AND NOT IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
		CONTROL_TYPE eControlType
		CONTROL_ACTION eControlAction
		
		IF bSecondaryButton
			GET_SHOWCASE_VEHICLE_CONTROLS_PURCHASE_SECONDARY(eControlType, eControlAction)
		ELSE
			GET_SHOWCASE_VEHICLE_CONTROLS_PURCHASE(eControlType, eControlAction)
		ENDIF
		
		IF IS_CONTROL_JUST_PRESSED(eControlType, eControlAction)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Tells net_purchase_vehicle.sch what controls are used for purchasing a vehicle. 
PROC SET_PURCHASE_SHOWCASE_VEHICLE_CONTROLS(BOOL bSecondaryButton = FALSE)
	IF bSecondaryButton
		m_sLocalData.sBuyVehicle.fpVehPurchaseControls = &GET_SHOWCASE_VEHICLE_CONTROLS_PURCHASE_SECONDARY
	ELSE
		m_sLocalData.sBuyVehicle.fpVehPurchaseControls = &GET_SHOWCASE_VEHICLE_CONTROLS_PURCHASE
	ENDIF
	
	SET_VEHICLE_PURCHASE_TRIGGER_CONTROLS(m_sLocalData.sBuyVehicle, m_sLocalData.sBuyVehicle.fpVehPurchaseControls)
ENDPROC

/// PURPOSE:
///    Wrapper for the function pointer T_VEHICLE_PURCHASE_ADD_MOD_PRICE
FUNC BOOL PURCHASE_VEH_ADD_MOD_PRICE()
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Wrapper for the function pointer T_VEHICLE_PURCHASE_ADD_MOD_PRICE
FUNC BOOL PURCHASE_VEH_REMOVE_MOD_PRICE()
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Call this for one frame to determine if mod prices should be added when purchasing a vehicle. 
PROC ADD_MOD_PRICES_WHEN_PURCHASING_VEHICLE(VEHICLE_PURCHASE_STRUCT &sPurchaseStruct, BOOL bAddModPrice)
	IF bAddModPrice
		SET_PURCHASE_VEHICLE_ADD_MOD_PRICE_TO_VEHICLE_PRICE(sPurchaseStruct, &PURCHASE_VEH_ADD_MOD_PRICE)
	ELSE
		SET_PURCHASE_VEHICLE_ADD_MOD_PRICE_TO_VEHICLE_PRICE(sPurchaseStruct, &PURCHASE_VEH_REMOVE_MOD_PRICE)
	ENDIF
ENDPROC

/// ============ BROWSE VEHICLE CAMERA ============ 

FUNC CONTROL_ACTION GET_CAM_CONTROL_ACTION_BACK()
	RETURN INPUT_FRONTEND_CANCEL
ENDFUNC

FUNC CONTROL_ACTION_GROUP GET_CAM_CONTROL_ACTION_MOVE_CAM()
//	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
//		RETURN INPUT_CURSOR_ACCEPT
//	ENDIF
	
	RETURN INPUTGROUP_LOOK
ENDFUNC

FUNC CONTROL_ACTION GET_CAM_CONTROL_ACTION_ZOOM_CAM()
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		RETURN INPUT_CURSOR_CANCEL
	ENDIF
	
	RETURN INPUT_FRONTEND_LT
ENDFUNC

FUNC CONTROL_ACTION_GROUP GET_CAM_CONTROL_ACTION_SWITCH_VEHICLES()
	RETURN INPUTGROUP_FRONTEND_BUMPERS
ENDFUNC

PROC INIT_BROWSE_CAM_INSTRUCTIONAL_BUTTONS()
	RESET_SCALEFORM_INSTRUCTIONAL_BUTTON(m_sLocalData.sCamData.sButtons)
	ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, GET_CAM_CONTROL_ACTION_BACK()), "LUX_SRM_BACK", m_sLocalData.sCamData.sButtons) // Back
	
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT), "LUX_SRM_MOVE", m_sLocalData.sCamData.sButtons) // Move Camera
	ELSE
		ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, GET_CAM_CONTROL_ACTION_MOVE_CAM()), "LUX_SRM_MOVE", m_sLocalData.sCamData.sButtons) // Move Camera
	ENDIF
	
	ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, GET_CAM_CONTROL_ACTION_ZOOM_CAM()), "LUX_SRM_ZOOM", m_sLocalData.sCamData.sButtons) // Zoom
	ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, GET_CAM_CONTROL_ACTION_SWITCH_VEHICLES()), "LUX_SRM_VEH", m_sLocalData.sCamData.sButtons) // Vehicles
	
	IF IS_THIS_VEHICLE_SAFE_TO_PURCHASE(GET_SHOWCASE_VEHICLE_TO_PURCHASE())
		CONTROL_TYPE eType
		CONTROL_ACTION eAction
		GET_SHOWCASE_VEHICLE_CONTROLS_PURCHASE_SECONDARY(eType, eAction)
		ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(eType, eAction), "LUX_SRM_BUY", m_sLocalData.sCamData.sButtons) // Purchase
	ENDIF
ENDPROC

PROC UPDATE_BROWSE_CAM_INSTRUCTIONAL_BUTTONS()
	INIT_BROWSE_CAM_INSTRUCTIONAL_BUTTONS()
	SPRITE_PLACEMENT sPlacement = GET_SCALEFORM_INSTRUCTIONAL_BUTTON_POSITION()
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD_PRIORITY_HIGH)
	RUN_SCALEFORM_INSTRUCTIONAL_BUTTONS(m_sLocalData.sCamData.scaleformButtons, sPlacement, m_sLocalData.sCamData.sButtons)
ENDPROC

PROC CLEANUP_BROWSE_CAM_INSTRUCTIONAL_BUTTONS()
	IF HAS_SCALEFORM_MOVIE_LOADED(m_sLocalData.sCamData.scaleformButtons)
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(m_sLocalData.sCamData.scaleformButtons)
	ENDIF
ENDPROC

FUNC STRING GET_SHOWROOM_AUDIO_SCENE()
	RETURN "GTAO_Auto_Store_Scene"
ENDFUNC

PROC SHOWROOM_AUDIO_SCENE_TOGGLE(BOOL bEnable)
	IF bEnable
		IF NOT IS_AUDIO_SCENE_ACTIVE(GET_SHOWROOM_AUDIO_SCENE())
			START_AUDIO_SCENE(GET_SHOWROOM_AUDIO_SCENE())
		ENDIF
	ELSE
		IF IS_AUDIO_SCENE_ACTIVE(GET_SHOWROOM_AUDIO_SCENE())
			STOP_AUDIO_SCENE(GET_SHOWROOM_AUDIO_SCENE())
		ENDIF
	ENDIF
ENDPROC

PROC CLEANUP_BROWSE_CAMERA()
	m_sLocalData.sCamData.fCamZoomAlpha = 0.0
	m_sLocalData.sCamData.fRotationAngle = 0.0
	m_sLocalData.sCamData.fZCamOffset = 0.0
	m_sLocalData.sCamData.fZCamOffsetInterp = 0.0
	m_sLocalData.sCamData.fFinalRot = 0.0
	m_sLocalData.sCamData.fFinalRotInterp = 0.0
	m_sLocalData.sCamData.fHeading = 0.0
	
	IF DOES_CAM_EXIST(m_sLocalData.sCamData.camera)
	AND IS_CAM_ACTIVE(m_sLocalData.sCamData.camera)
		SET_CAM_ACTIVE(m_sLocalData.sCamData.camera, FALSE)
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		DESTROY_CAM(m_sLocalData.sCamData.camera)
		REMOVE_MULTIPLAYER_WALLET_CASH()  //Display cash on hud
		REMOVE_MULTIPLAYER_BANK_CASH()
		SHOWROOM_AUDIO_SCENE_TOGGLE(FALSE)
	ENDIF
	
	IF DOES_CAM_EXIST(m_sLocalData.sCamData.cameraInterp)
		DESTROY_CAM(m_sLocalData.sCamData.camera)
	ENDIF
	
	IF IS_BIT_SET(m_sLocalData.iBS, BS_LUXURY_SHOWROOM_SWITCHED_VEHICLES)
		CLEAR_BIT(m_sLocalData.iBS, BS_LUXURY_SHOWROOM_SWITCHED_VEHICLES)
	ENDIF
	
	CLEANUP_BROWSE_CAM_INSTRUCTIONAL_BUTTONS()
ENDPROC

PROC UPDATE_BROWSE_CAM_CONTROLS(LUXURY_SHOWROOM_CAM_DATA &ref_sCamData)
	IF IS_PAUSE_MENU_ACTIVE()
	OR IS_WARNING_MESSAGE_ACTIVE()
		EXIT
	ENDIF
	
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		FLOAT fMouseXSpeed 	= 200.0
		FLOAT fMouseYSpeed 	= 30.0
		BOOL bUpdateCursor = (GET_VEHICLE_PURCHASE_STATE(m_sLocalData.sBuyVehicle) != VPS_PROPERTY_MENU)
		
		IF bUpdateCursor
			SET_MOUSE_CURSOR_THIS_FRAME()
		ENDIF
		
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
			IF bUpdateCursor
				SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_HAND_GRAB)
			ENDIF
			ref_sCamData.fRotationAngle -= (g_fMenuCursorXMoveDistance * fMouseXSpeed)
			ref_sCamData.fZCamOffset += (g_fMenuCursorYMoveDistance * fMouseYSpeed)
		ELIF bUpdateCursor
			SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_HAND_OPEN)
		ENDIF
		
		UPDATE_MENU_CURSOR_GLOBALS()
	ELSE
		FLOAT fDeadZone 	= 16
		FLOAT fStickXSpeed 	= 0.025
		FLOAT fStickYSpeed 	= 0.001
	
		INT iLeftX, iLeftY, iRightX, iRightY
		GET_CONTROL_VALUE_OF_ANALOGUE_STICKS_UNBOUND(iLeftX, iLeftY, iRightX, iRightY, TRUE)
		iRightX *= -1
		
		IF IS_LOOK_INVERTED()
			iRightY	*= -1
		ENDIF
		
		IF ((iRightX < -fDeadZone) OR (iRightX > fDeadZone))
			ref_sCamData.fRotationAngle += (iRightX * fStickXSpeed)
		ENDIF
		IF ((iRightY < -fDeadZone) OR (iRightY > fDeadZone))
			ref_sCamData.fZCamOffset += (iRightY * fStickYSpeed)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Main camera for veiwing the vehicle. Should provide a full 360 view.
PROC UPDATE_SHOWROOM_CAMERA(VEHICLE_INDEX veh)
	IF NOT IS_ENTITY_ALIVE(veh)
		EXIT
	ENDIF
	
	// ----- Local Cam variables -----
	FLOAT fGroundZ
	VECTOR vVehCoords = GET_ENTITY_COORDS(veh)
	FLOAT fDefaultYOffset
	FLOAT fDefaultZOffset
	VECTOR vCamPos
	INT iSwitchInterpDuration 	= 400
	VECTOR vPanOffset 			= <<-0.1000, 0.0000, 0.0000>>
	FLOAT fDistanceFromVeh 		= 4.0000
	FLOAT fMaxFOV 				= 45.3000
	FLOAT fMinFOV 				= 32.5000
	FLOAT fStartHeadingOffset
	FLOAT fNearDOF 				= 0.1500
	FLOAT fFarDOF 				= 10.0000
	FLOAT fMinZOffset 			= 1.2500
	FLOAT fMaxZOffset 			= 2.8700
	FLOAT fShakeAmp 			= 0.1000
	FLOAT fDOFStrength 			= 0.8800
	FLOAT fInterpAlpha 			= 0.1250
	
	// url:bugstar:7720507 - Luxury Autos - Would it be possible to tweak the default camera heading when initially inspecting the vehicle on the left? 
	IF IS_BIT_SET(m_sLocalData.iBS, BS_LUXURY_SHOWROOM_START_HEADING_OFFSET_ON_LEFT)
		fStartHeadingOffset = -35.0000
		CLEAR_BIT(m_sLocalData.iBS, BS_LUXURY_SHOWROOM_START_HEADING_OFFSET_ON_LEFT)
	ELSE
		fStartHeadingOffset = 35.0000
	ENDIF
	
	// ----- ZOOM -----
	FLOAT fMaxZoomAlpha = 1.0
	FLOAT fMinZoomAlpha = 0
	FLOAT fZoomSpeed 	= 0.0500
	
	CAMERA_GRAPH_TYPE eGraphType = GRAPH_TYPE_SIN_ACCEL_DECEL
	
	IF NOT GET_GROUND_Z_FOR_3D_COORD(vVehCoords, fGroundZ)
		fGroundZ = 36.733 // Z coord of the display platform.
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF m_sDebugData.sCamDebug.bEnableDebug
		iSwitchInterpDuration 	= m_sDebugData.sCamDebug.iSwitchInterpDuration
		eGraphType				= INT_TO_ENUM(CAMERA_GRAPH_TYPE, m_sDebugData.sCamDebug.iSwitchGraphType)
		vPanOffset 				= m_sDebugData.sCamDebug.vPanOffset
		fDistanceFromVeh 		= m_sDebugData.sCamDebug.fDistanceFromVeh
		fMaxFOV 				= m_sDebugData.sCamDebug.fMaxFOV
		fMinFOV 				= m_sDebugData.sCamDebug.fMinFOV
		fStartHeadingOffset 	= m_sDebugData.sCamDebug.fStartHeadingOffset
		fNearDOF 				= m_sDebugData.sCamDebug.fNearDOF
		fFarDOF 				= m_sDebugData.sCamDebug.fFarDOF
		fMinZOffset 			= m_sDebugData.sCamDebug.fMinZOffset
		fMaxZOffset 			= m_sDebugData.sCamDebug.fMaxZOffset
		fShakeAmp 				= m_sDebugData.sCamDebug.fShakeAmp
		fDOFStrength 			= m_sDebugData.sCamDebug.fDOFStrength
		fInterpAlpha 			= m_sDebugData.sCamDebug.fInterpAlpha
		fZoomSpeed 				= m_sDebugData.sCamDebug.fZoomSpeed
	ENDIF
	#ENDIF
	
	// ----- Create camera -----
	IF NOT DOES_CAM_EXIST(m_sLocalData.sCamData.camera)
		m_sLocalData.sCamData.camera = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
		SHAKE_CAM(m_sLocalData.sCamData.camera, "HAND_SHAKE", fShakeAmp)
		
		FLOAT fHeading = (GET_ENTITY_HEADING(veh) + fStartHeadingOffset) // Start with a cool side view of vehicle.
		fHeading = NORMALIZE_ANGLE(fHeading)
		
		m_sLocalData.sCamData.fRotationAngle = fDefaultYOffset
		m_sLocalData.sCamData.fHeading = fHeading
		m_sLocalData.sCamData.fFinalRot = fHeading + m_sLocalData.sCamData.fRotationAngle
		m_sLocalData.sCamData.fZCamOffset = fDefaultZOffset
		
		vCamPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vVehCoords, m_sLocalData.sCamData.fFinalRot, <<0, fDistanceFromVeh, 0>>)
		vCamPos.z = fGroundZ + m_sLocalData.sCamData.fZCamOffset
		
		SET_CAM_COORD(m_sLocalData.sCamData.camera, vCamPos)
		POINT_CAM_AT_COORD(m_sLocalData.sCamData.camera, vVehCoords)
		m_sLocalData.sCamData.fZCamOffsetInterp = m_sLocalData.sCamData.fZCamOffset
		m_sLocalData.sCamData.fFinalRotInterp = m_sLocalData.sCamData.fFinalRot
		
		SET_CAM_ACTIVE(m_sLocalData.sCamData.camera, TRUE)
		SET_CAM_USE_SHALLOW_DOF_MODE(m_sLocalData.sCamData.camera, TRUE)
		SET_CAM_NEAR_DOF(m_sLocalData.sCamData.camera, fNearDOF)
		SET_CAM_FAR_DOF(m_sLocalData.sCamData.camera, fFarDOF)
		SET_CAM_DOF_STRENGTH(m_sLocalData.sCamData.camera, fDOFStrength)
		SET_CAM_FOV(m_sLocalData.sCamData.camera, fMaxFOV)
		RENDER_SCRIPT_CAMS(TRUE, FALSE)
		
		SET_MULTIPLAYER_WALLET_CASH()  //Display cash on hud
		SET_MULTIPLAYER_BANK_CASH()
		SHOWROOM_AUDIO_SCENE_TOGGLE(TRUE)
	ENDIF
	
	IF NOT DOES_CAM_EXIST(m_sLocalData.sCamData.cameraInterp)
		m_sLocalData.sCamData.cameraInterp = CREATE_CAMERA()
		SET_CAM_USE_SHALLOW_DOF_MODE(m_sLocalData.sCamData.cameraInterp, TRUE)
		SET_CAM_NEAR_DOF(m_sLocalData.sCamData.cameraInterp, fNearDOF)
		SET_CAM_FAR_DOF(m_sLocalData.sCamData.cameraInterp, fFarDOF)
		SET_CAM_DOF_STRENGTH(m_sLocalData.sCamData.cameraInterp, fDOFStrength)
	ENDIF
	
	// ----- Update -----
	IF DOES_CAM_EXIST(m_sLocalData.sCamData.camera)
		IF NOT IS_CAM_INTERPOLATING(m_sLocalData.sCamData.camera)
			UPDATE_BROWSE_CAM_CONTROLS(m_sLocalData.sCamData)

			VECTOR vBrowseCamLookAt = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(veh, ZERO_VECTOR())
			
			SET_USE_HI_DOF()
			
			m_sLocalData.sCamData.fRotationAngle = NORMALIZE_ANGLE(m_sLocalData.sCamData.fRotationAngle)
			m_sLocalData.sCamData.fFinalRot = m_sLocalData.sCamData.fHeading + m_sLocalData.sCamData.fRotationAngle
			m_sLocalData.sCamData.fZCamOffset = CLAMP(m_sLocalData.sCamData.fZCamOffset, fMinZOffset, fMaxZOffset)
			
			// Interp to the default pos.
			m_sLocalData.sCamData.fFinalRotInterp += ((CALCULATE_DIFFERENCE_BETWEEN_ANGLES(m_sLocalData.sCamData.fFinalRotInterp, m_sLocalData.sCamData.fFinalRot) * fInterpAlpha))
			m_sLocalData.sCamData.fZCamOffsetInterp += ((m_sLocalData.sCamData.fZCamOffset - m_sLocalData.sCamData.fZCamOffsetInterp) * fInterpAlpha)
			
			m_sLocalData.sCamData.fZCamOffsetInterp = CLAMP(m_sLocalData.sCamData.fZCamOffsetInterp, fMinZOffset, fMaxZOffset)	
				
			vCamPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(veh, ZERO_VECTOR()), m_sLocalData.sCamData.fFinalRotInterp, <<0, fDistanceFromVeh, 0>>)
			vCamPos.z = fGroundZ + m_sLocalData.sCamData.fZCamOffsetInterp
			
			VECTOR vCameraXVector = CROSS_PRODUCT(NORMALISE_VECTOR(vBrowseCamLookAt - vCamPos), <<0, 0, 1>>)
			VECTOR vCameraZVector = CROSS_PRODUCT(NORMALISE_VECTOR(vBrowseCamLookAt - vCamPos), vCameraXVector)
			vBrowseCamLookAt += vCameraXVector * vPanOffset.x
			vBrowseCamLookAt += vCameraZVector * vPanOffset.z
			
			SET_CAM_COORD(m_sLocalData.sCamData.camera, vCamPos)
			POINT_CAM_AT_COORD(m_sLocalData.sCamData.camera,
					GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vBrowseCamLookAt, GET_ENTITY_HEADING(Veh), ZERO_VECTOR()))
			
			IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, GET_CAM_CONTROL_ACTION_ZOOM_CAM())
				IF m_sLocalData.sCamData.fCamZoomAlpha < fMaxZoomAlpha
					SET_CAM_SHAKE_AMPLITUDE(m_sLocalData.sCamData.camera, (fMaxZoomAlpha - m_sLocalData.sCamData.fCamZoomAlpha) * fShakeAmp)
					m_sLocalData.sCamData.fCamZoomAlpha += fZoomSpeed // Zoom in
				ENDIF
				
				IF m_sLocalData.sCamData.fCamZoomAlpha > fMaxZoomAlpha
					m_sLocalData.sCamData.fCamZoomAlpha = fMaxZoomAlpha
				ENDIF
			ELSE
				IF m_sLocalData.sCamData.fCamZoomAlpha > fMinZoomAlpha
					SET_CAM_SHAKE_AMPLITUDE(m_sLocalData.sCamData.camera, (fMaxZoomAlpha - m_sLocalData.sCamData.fCamZoomAlpha) * fShakeAmp)
					m_sLocalData.sCamData.fCamZoomAlpha -= fZoomSpeed // Zoom out
				ENDIF
				IF m_sLocalData.sCamData.fCamZoomAlpha < fMinZoomAlpha
					m_sLocalData.sCamData.fCamZoomAlpha = fMinZoomAlpha
				ENDIF
			ENDIF
			
			FLOAT fCamZoomFov = COSINE_INTERP_FLOAT(fMaxFOV, fMinFOV, m_sLocalData.sCamData.fCamZoomAlpha)
			SET_CAM_FOV(m_sLocalData.sCamData.camera, fCamZoomFov)
			CAMERA_PREVENT_COLLISION_SETTINGS_FOR_TRIPLEHEAD_IN_INTERIORS_THIS_UPDATE()
			
			// --- If the player has switch vehicles while browsing, interp the camera to it's new position (set just above) ---
			IF IS_BIT_SET(m_sLocalData.iBS, BS_LUXURY_SHOWROOM_SWITCHED_VEHICLES)
				IF DOES_CAM_EXIST(m_sLocalData.sCamData.cameraInterp)
					SET_CAM_COORD(m_sLocalData.sCamData.cameraInterp, GET_CAM_COORD(m_sLocalData.sCamData.camera))
					SET_CAM_ROT(m_sLocalData.sCamData.cameraInterp, GET_CAM_ROT(m_sLocalData.sCamData.camera))
					SET_CAM_FOV(m_sLocalData.sCamData.cameraInterp, GET_CAM_FOV(m_sLocalData.sCamData.camera))
					SET_CAM_NEAR_DOF(m_sLocalData.sCamData.cameraInterp, GET_CAM_NEAR_DOF(m_sLocalData.sCamData.camera))
					SET_CAM_FAR_DOF(m_sLocalData.sCamData.cameraInterp, GET_CAM_FAR_DOF(m_sLocalData.sCamData.camera))
					SET_CAM_DOF_STRENGTH(m_sLocalData.sCamData.cameraInterp, GET_CAM_DOF_STRENGTH(m_sLocalData.sCamData.camera))
					SET_CAM_ACTIVE_WITH_INTERP(m_sLocalData.sCamData.camera, m_sLocalData.sCamData.cameraInterp, iSwitchInterpDuration, eGraphType, eGraphType)
					PLAY_SOUND_FROM_ENTITY(-1, "Change_Vehicle", veh, "GTAO_Auto_Store_Sounds")
					CLEAR_BIT(m_sLocalData.iBS, BS_LUXURY_SHOWROOM_SWITCHED_VEHICLES)
				ENDIF
			ENDIF
		ELSE
			SET_USE_HI_DOF()
		ENDIF
	ENDIF
ENDPROC

PROC PREP_PLAYER_FOR_SHOP_MENU()
	SET_TEMP_PASSIVE_MODE(TRUE)
	NET_SET_PLAYER_CONTROL_FLAGS eControlFlags = NSPC_ALLOW_PLAYER_DAMAGE | NSPC_DONT_STOP_OTHER_CARS_AROUND_PLAYER | NSPC_PREVENT_EVERYBODY_BACKOFF | NSPC_ALLOW_PAD_SHAKE | NSPC_CAN_BE_TARGETTED | NSPC_REENABLE_CONTROL_ON_DEATH | NSPC_LEAVE_CAMERA_CONTROL_ON
	NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, eControlFlags)
	DISABLE_INTERACTION_MENU()
ENDPROC

PROC PREP_PLAYER_FOR_EXITING_SHOP_MENU()
	IF IS_SKYSWOOP_AT_GROUND()
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	ENDIF
	
	ENABLE_INTERACTION_MENU()
	CLEANUP_TEMP_PASSIVE_MODE()
ENDPROC

PROC START_HEADING_OFFSET_ON_LEFT()
	IF m_sLocalData.iVehToPurchase = 0
		IF NOT IS_BIT_SET(m_sLocalData.iBS, BS_LUXURY_SHOWROOM_START_HEADING_OFFSET_ON_LEFT)
			SET_BIT(m_sLocalData.iBS, BS_LUXURY_SHOWROOM_START_HEADING_OFFSET_ON_LEFT)
		ENDIF
	ELIF IS_BIT_SET(m_sLocalData.iBS, BS_LUXURY_SHOWROOM_START_HEADING_OFFSET_ON_LEFT)
		CLEAR_BIT(m_sLocalData.iBS, BS_LUXURY_SHOWROOM_START_HEADING_OFFSET_ON_LEFT)
	ENDIF
ENDPROC

PROC GO_BACK_TO_PREVIOUS_STATE_FROM_PURCHASE()
	IF IS_BIT_SET(m_sLocalData.iBS, BS_LUXURY_SHOWROOM_PURCHASING_FROM_BROWSE_STATE)
		CLEAR_BIT(m_sLocalData.iBS, BS_LUXURY_SHOWROOM_PURCHASING_FROM_BROWSE_STATE)
		START_HEADING_OFFSET_ON_LEFT()
		SET_SHOWROOM_STATE(SHOWROOM_STATE_BROWSE_VEHICLE)
	ELSE
		PREP_PLAYER_FOR_EXITING_SHOP_MENU()
		SET_SHOWROOM_STATE(SHOWROOM_STATE_IDLE)
	ENDIF
ENDPROC

PROC ENABLE_CONTROL_ACTIONS_FOR_MENU()
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
ENDPROC

PROC DISABLE_CONTROL_ACTIONS_WHILE_IN_TRIGGER_LOCATE()
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
	
	IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
	ENDIF
ENDPROC

PROC DISABLE_CONTROL_ACTIONS_WHILE_BROWSING_VEHICLE()
	DISABLE_ALL_CONTROL_ACTIONS(PLAYER_CONTROL)
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, GET_CAM_CONTROL_ACTION_BACK())
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, GET_CAM_CONTROL_ACTION_ZOOM_CAM())
	
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
	ELSE
		ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_RS)
	ENDIF
	
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_LB)
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_RB)
	
	CONTROL_TYPE eType
	CONTROL_ACTION eAction
	GET_SHOWCASE_VEHICLE_CONTROLS_PURCHASE_SECONDARY(eType, eAction)
	ENABLE_CONTROL_ACTION(eType, eAction)
ENDPROC

FUNC BOOL SHOULD_EXIT_BROWSE_VEHICLE_STATE()
	IF NOT CAN_PURCHASE_SHOWCASE_VEHICLE()
		RETURN TRUE
	ENDIF
	
	IF NOT IS_LOCAL_PLAYER_IN_PROMPT_TRIGGER_LOCATE(FALSE)
		RETURN TRUE
	ENDIF
	
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, GET_CAM_CONTROL_ACTION_BACK())
			RETURN TRUE
		ENDIF
	ELSE
		IF NOT IS_PAUSE_MENU_ACTIVE()
		AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_CAM_CONTROL_ACTION_BACK())
			PLAY_SOUND_FRONTEND(-1, "BACK", "HUD_FRONTEND_DEFAULT_SOUNDSET")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC HANDLE_ENTER_MENU_PC_CONTROL(ENTER_MENU_DATA &sData, BOOL &bPrevious, BOOL &bNext, BOOL  &bAccept, BOOL &bBack)
	IF IS_PC_VERSION()
	AND NOT NETWORK_TEXT_CHAT_IS_TYPING()
		IF IS_USING_CURSOR(FRONTEND_CONTROL)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LR)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UD)
			DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
			ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_ACCEPT)
			ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_CANCEL)
			SET_INPUT_EXCLUSIVE(PLAYER_CONTROL, INPUT_CURSOR_ACCEPT)
			SET_INPUT_EXCLUSIVE(PLAYER_CONTROL, INPUT_CURSOR_CANCEL)
			ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_SCROLL_UP)
			ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_SCROLL_DOWN)
			DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_LOOK_LR)
			DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_LOOK_UD)
			
			HANDLE_CURSOR_HIGHLIGHTED_MENU_ITEMS()
			HANDLE_MENU_CURSOR(FALSE)
		ENDIF
		
		IF IS_MENU_CURSOR_ACCEPT_PRESSED()
			IF g_iMenuCursorItem = sData.iCurrentSelection
				bAccept = TRUE
			ELSE
				sData.iCurrentSelection = g_iMenuCursorItem
				SET_CURRENT_MENU_ITEM(sData.iCurrentSelection)
			ENDIF
		ENDIF
		
		IF IS_MENU_CURSOR_CANCEL_PRESSED()
			bBack = TRUE
		ENDIF
		
		IF IS_MENU_CURSOR_SCROLL_UP_PRESSED()
		OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
			bPrevious = TRUE
		ENDIF
		
		IF IS_MENU_CURSOR_SCROLL_DOWN_PRESSED()
		OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
			bNext = TRUE
		ENDIF
	ENDIF
ENDPROC

PROC HANDLE_ENTER_MENU_INPUT(ENTER_MENU_DATA &sData, INT iNumberOfMenuOptions)
	BOOL bPrevious, bNext
	
	INT iLeftY
	INT iMenuInputDelay = 150
	
	IF IS_USING_CURSOR(FRONTEND_CONTROL)
		iMenuInputDelay = 110
	ENDIF
	
	SET_CLEAR_ON_CALL_HUD_THIS_FRAME()
	HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
	
	BOOL bAccept = (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT))
	BOOL bBack = (IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL) OR IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL))
	iLeftY = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_MOVE_UD)
	
	IF IS_PLAYER_IN_CORONA()
	OR IS_PHONE_ONSCREEN()
		bAccept = FALSE
	ENDIF
	
	IF NOT IS_BIT_SET(m_sLocalData.iBS, BS_LUXURY_SHOWROOM_MENU_DELAY_TIMER_ACTIVE)
		IF IS_USING_CURSOR(FRONTEND_CONTROL)
			HANDLE_ENTER_MENU_PC_CONTROL(sData, bPrevious, bNext, bAccept, bBack)
		ELSE
			bPrevious = ((iLeftY < 100) OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP))
			bNext = ((iLeftY > 150) OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN))
		ENDIF
		
		IF bPrevious OR bNext
			START_NET_TIMER(sData.tAnalogueDelayTimer)
			SET_BIT(m_sLocalData.iBS, BS_LUXURY_SHOWROOM_MENU_DELAY_TIMER_ACTIVE)
		ENDIF
	ELIF HAS_NET_TIMER_EXPIRED(sData.tAnalogueDelayTimer, iMenuInputDelay)
		RESET_NET_TIMER(sData.tAnalogueDelayTimer)
		CLEAR_BIT(m_sLocalData.iBS, BS_LUXURY_SHOWROOM_MENU_DELAY_TIMER_ACTIVE)
	ENDIF
	
	IF sData.eLayer = EML_DEFAULT
		IF bNext
			sData.iCurrentSelection++
			
			IF sData.iCurrentSelection >= iNumberOfMenuOptions
				sData.iCurrentSelection = ciSHOWROOM_ENTER_MENU_OPTION_BROWSE
			ENDIF
			
			PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET")
		ENDIF
		
		IF bPrevious
			sData.iCurrentSelection--
			
			IF sData.iCurrentSelection < ciSHOWROOM_ENTER_MENU_OPTION_BROWSE
				sData.iCurrentSelection = (iNumberOfMenuOptions - 1)
			ENDIF
			
			PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET")
		ENDIF
	ENDIF
	
	IF bAccept
		PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
		SET_BIT(m_sLocalData.iBS, BS_LUXURY_SHOWROOM_MENU_OPTION_ACCEPTED)
	ENDIF
	
	IF bBack
	OR IS_BROWSER_OPEN()
		PLAY_SOUND_FRONTEND(-1, "BACK", "HUD_FRONTEND_DEFAULT_SOUNDSET")
		
		IF sData.eLayer = EML_DEFAULT
			sData.iCurrentSelection = ciSHOWROOM_ENTER_MENU_OPTION_BROWSE
		ENDIF
		
		SET_BIT(m_sLocalData.iBS, BS_LUXURY_SHOWROOM_MENU_OPTION_BACK)
	ENDIF
ENDPROC

FUNC BOOL SHOULD_ENTER_MENU_BE_REBUILT(ENTER_MENU_DATA &sData)
	UNUSED_PARAMETER(sData)
	RETURN FALSE
ENDFUNC

PROC BUILD_ENTER_MENU(ENTER_MENU_DATA &sData)	
	SETUP_MENU_WITH_TITLE("LUX_SRM_M_TTL")
	
	REQUEST_STREAMED_TEXTURE_DICT(GET_LUXURY_SHOWROOM_MENU_TXD())
	IF HAS_STREAMED_TEXTURE_DICT_LOADED(GET_LUXURY_SHOWROOM_MENU_TXD())
		SET_MENU_USES_HEADER_GRAPHIC(TRUE, GET_LUXURY_SHOWROOM_MENU_TXD(), GET_LUXURY_SHOWROOM_MENU_TXD())	
	ENDIF
	
	ADD_MENU_ITEM_TEXT(ciSHOWROOM_ENTER_MENU_OPTION_BROWSE, "LUX_SRM_M_O_0", 0, TRUE)
	ADD_MENU_ITEM_TEXT(ciSHOWROOM_ENTER_MENU_OPTION_PURCHASE, "LUX_SRM_M_O_1", 0, IS_THIS_VEHICLE_SAFE_TO_PURCHASE(GET_SHOWCASE_VEHICLE_TO_PURCHASE()))
	SET_CURRENT_MENU_ITEM(sData.iCurrentSelection)
	
	IF sData.eLayer = EML_CONFIRM_PURCHASE
		ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "ITEM_YES")
	ELSE
		ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "ITEM_SELECT")
	ENDIF
	
	ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "ITEM_BACK")
ENDPROC

PROC MAINTAIN_ENTER_MENU_DESCRIPTION(ENTER_MENU_DATA &sData)
	UNUSED_PARAMETER(sData)
	
	SWITCH sData.iCurrentSelection
		CASE ciSHOWROOM_ENTER_MENU_OPTION_BROWSE
			SET_CURRENT_MENU_ITEM_DESCRIPTION("LUX_SRM_M_D_0")
			ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING(m_sLocalData.strVehName)
		BREAK
		CASE ciSHOWROOM_ENTER_MENU_OPTION_PURCHASE
			IF sData.eLayer = EML_CONFIRM_PURCHASE
				SET_CURRENT_MENU_ITEM_DESCRIPTION("BUY_VEH_CONF")
				ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING(m_sLocalData.strVehName)
				ADD_CURRENT_MENU_ITEM_DESCRIPTION_INT(m_sLocalData.iVehPrice)
			ELSE
				IF IS_THIS_VEHICLE_SAFE_TO_PURCHASE(GET_SHOWCASE_VEHICLE_TO_PURCHASE())
					SET_CURRENT_MENU_ITEM_DESCRIPTION("LUX_SRM_M_D_1")
					ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING(m_sLocalData.strVehName)
					ADD_CURRENT_MENU_ITEM_DESCRIPTION_INT(m_sLocalData.iVehPrice)
				ELSE
					SET_CURRENT_MENU_ITEM_DESCRIPTION("LUX_SRM_H_SOON")
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Main update for the enter menu. Called every frame. 
PROC DRAW_ENTER_MENU(ENTER_MENU_DATA &sData)
	IF NOT IS_PAUSE_MENU_ACTIVE_EX()
		HANDLE_ENTER_MENU_INPUT(sData, 2)
		
		IF SHOULD_ENTER_MENU_BE_REBUILT(sData)
			SET_BIT(m_sLocalData.iBS, BS_LUXURY_SHOWROOM_MENU_REBUILD)
		ENDIF
		
		REQUEST_STREAMED_TEXTURE_DICT(GET_LUXURY_SHOWROOM_MENU_TXD())
		
		IF LOAD_MENU_ASSETS()
		AND HAS_STREAMED_TEXTURE_DICT_LOADED(GET_LUXURY_SHOWROOM_MENU_TXD())
			IF IS_BIT_SET(m_sLocalData.iBS, BS_LUXURY_SHOWROOM_MENU_REBUILD)
				CLEAR_MENU_DATA()
				BUILD_ENTER_MENU(sData)
				CLEAR_BIT(m_sLocalData.iBS, BS_LUXURY_SHOWROOM_MENU_REBUILD)
			ELSE
				SET_CURRENT_MENU_ITEM(sData.iCurrentSelection)
			ENDIF
			
			SET_SCRIPT_GFX_ALIGN(UI_ALIGN_IGNORE, UI_ALIGN_IGNORE)
			SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
			SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
		
			MAINTAIN_ENTER_MENU_DESCRIPTION(sData)
			DRAW_MENU()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			HIDE_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_MP_RANK_BAR)
		ENDIF
	ENDIF
ENDPROC

PROC CLEANUP_ENTER_MENU(ENTER_MENU_DATA &sData)
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(GET_LUXURY_SHOWROOM_MENU_TXD())
	sData.iCurrentSelection = ciSHOWROOM_ENTER_MENU_OPTION_BROWSE
	sData.eLayer = EML_DEFAULT
	RESET_NET_TIMER(sData.tAnalogueDelayTimer)
	CLEANUP_MENU_ASSETS()
ENDPROC

/// PURPOSE:
///    While browsing, player can switch between vehicles instantly - url:bugstar:7606522
PROC MAINTAIN_SWITCH_BETWEEN_SHOWCASE_VEHICLES()
	IF DOES_CAM_EXIST(m_sLocalData.sCamData.camera)
	AND IS_CAM_INTERPOLATING(m_sLocalData.sCamData.camera)
		EXIT
	ENDIF
	
	INT iNewVehToPurchase = m_sLocalData.iVehToPurchase
	
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LB)
		iNewVehToPurchase--
	ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RB)
		iNewVehToPurchase++
	ENDIF
	
	IF iNewVehToPurchase < 0
		iNewVehToPurchase = ciNUM_SHOWCASE_VEHICLES - 1
	ELIF iNewVehToPurchase >= ciNUM_SHOWCASE_VEHICLES
		iNewVehToPurchase = 0
	ENDIF
	
	IF iNewVehToPurchase != m_sLocalData.iVehToPurchase
		SET_BIT(m_sLocalData.iBS, BS_LUXURY_SHOWROOM_SWITCHED_VEHICLES)
		SET_SHOWCASE_VEHICLE_INDEX_TO_PURCHASE(iNewVehToPurchase)
		SET_SELECTED_PURCHASE_VEHICLE_INDEX(m_sLocalData.sBuyVehicle, &GET_SHOWCASE_VEHICLE_TO_PURCHASE)
	ENDIF
ENDPROC

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════════════╡ STATES ╞═════════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

PROC MAINTAIN_STATE_CREATE_VEHICLES()
	IF CREATE_SHOWCASE_VEHICLES()
		SET_SHOWROOM_STATE(SHOWROOM_STATE_IDLE)
	ENDIF
ENDPROC

PROC MAINTAIN_STATE_IDLE()
	IF CAN_PURCHASE_SHOWCASE_VEHICLE()
	AND IS_LOCAL_PLAYER_IN_PROMPT_TRIGGER_LOCATE()
		DISABLE_CONTROL_ACTIONS_WHILE_IN_TRIGGER_LOCATE()
		
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("LUX_SRM_MSN")
			CLEAR_HELP()
		ENDIF
		
		IF GET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID())
			SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
		ENDIF
		
		IF m_sLocalData.iEnterInput = NEW_CONTEXT_INTENTION
			REGISTER_CONTEXT_INTENTION(m_sLocalData.iEnterInput, CP_HIGH_PRIORITY, "LUX_SRM_ENTER")
		ELIF HAS_CONTEXT_BUTTON_TRIGGERED(m_sLocalData.iEnterInput)
			PREP_PLAYER_FOR_SHOP_MENU()
			
			IF IS_PLAYER_IN_FIRST_PERSON_CAMERA()
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			ENDIF
			
			SET_SHOWCASE_VEHICLE_INDEX_TO_PURCHASE(GET_SHOWCASE_VEHICLE_INDEX_FOR_ENTRY_LOCATE(GET_PROMPT_TRIGGER_FROM_STAGGER()))
			SET_BIT(m_sLocalData.iBS, BS_LUXURY_SHOWROOM_MENU_REBUILD)
			RELEASE_CONTEXT_INTENTION(m_sLocalData.iEnterInput)
			SET_SELECTED_PURCHASE_VEHICLE_INDEX(m_sLocalData.sBuyVehicle, &GET_SHOWCASE_VEHICLE_TO_PURCHASE)
			ADD_MOD_PRICES_WHEN_PURCHASING_VEHICLE(m_sLocalData.sBuyVehicle, TRUE)
			STORE_PRICE_OF_SHOWCASE_VEHICLE(GET_SHOWCASE_VEHICLE_TO_PURCHASE())
			STORE_NAME_OF_SHOWCASE_VEHICLE(GET_SHOWCASE_VEHICLE_TO_PURCHASE())
			PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
			SET_SHOWROOM_STATE(SHOWROOM_STATE_DISPLAY_MENU)
		ENDIF
	ELIF m_sLocalData.iEnterInput != NEW_CONTEXT_INTENTION
		RELEASE_CONTEXT_INTENTION(m_sLocalData.iEnterInput)
	ELIF IS_LOCAL_PLAYER_IN_PROMPT_TRIGGER_LOCATE()
	AND (IS_LOCAL_PLAYER_ON_ANY_FM_MISSION()
	OR (GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID()) AND GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) != -1))
		IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("LUX_SRM_MSN")
			PRINT_HELP_FOREVER("LUX_SRM_MSN")
		ENDIF
	ELIF IS_LOCAL_PLAYER_IN_PROMPT_TRIGGER_LOCATE() AND (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0)
		IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("LUX_SRM_WANTED")
			PRINT_HELP_FOREVER("LUX_SRM_WANTED")
		ENDIF
	ELSE
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("LUX_SRM_MSN")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("LUX_SRM_WANTED")
			CLEAR_HELP()
		ENDIF
	ENDIF
	
ENDPROC

PROC MAINTAIN_STATE_DISPLAY_MENU()
		
	// --- Go back to idle state if player has either left the locate, or can't purchase a vehicle. ---
	IF NOT CAN_PURCHASE_SHOWCASE_VEHICLE()
	OR NOT IS_LOCAL_PLAYER_IN_PROMPT_TRIGGER_LOCATE(FALSE)
		PREP_PLAYER_FOR_EXITING_SHOP_MENU()
		CLEANUP_ENTER_MENU(m_sLocalData.sEnterMenu)
		SET_SHOWROOM_STATE(SHOWROOM_STATE_IDLE)
		EXIT
	ENDIF
	
	ENABLE_CONTROL_ACTIONS_FOR_MENU()
	DISABLE_FRONTEND_THIS_FRAME()
	DRAW_ENTER_MENU(m_sLocalData.sEnterMenu)

	IF IS_BIT_SET(m_sLocalData.iBS, BS_LUXURY_SHOWROOM_MENU_OPTION_ACCEPTED)
		SWITCH m_sLocalData.sEnterMenu.eLayer
			CASE EML_DEFAULT
				SWITCH m_sLocalData.sEnterMenu.iCurrentSelection
					CASE ciSHOWROOM_ENTER_MENU_OPTION_BROWSE
						INIT_BROWSE_CAM_INSTRUCTIONAL_BUTTONS()
						CLEANUP_ENTER_MENU(m_sLocalData.sEnterMenu)
						START_HEADING_OFFSET_ON_LEFT()
						SET_SHOWROOM_STATE(SHOWROOM_STATE_BROWSE_VEHICLE)
					BREAK
					CASE ciSHOWROOM_ENTER_MENU_OPTION_PURCHASE
						IF IS_THIS_VEHICLE_SAFE_TO_PURCHASE(GET_SHOWCASE_VEHICLE_TO_PURCHASE())
							m_sLocalData.sEnterMenu.eLayer = EML_CONFIRM_PURCHASE
							SET_BIT(m_sLocalData.iBS, BS_LUXURY_SHOWROOM_MENU_REBUILD)
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
			CASE EML_CONFIRM_PURCHASE
				SWITCH m_sLocalData.sEnterMenu.iCurrentSelection
					CASE ciSHOWROOM_ENTER_MENU_OPTION_BROWSE
						// Do nothing
					BREAK
					CASE ciSHOWROOM_ENTER_MENU_OPTION_PURCHASE
						IF IS_THIS_VEHICLE_SAFE_TO_PURCHASE(GET_SHOWCASE_VEHICLE_TO_PURCHASE())
							SET_VEH_PURCHASE_LOCAL_BS(m_sLocalData.sBuyVehicle, VEH_PURCHASE_LOCAL_BS_PROCEED_WITH_PURCHASE)
							SET_VEH_PURCHASE_LOCAL_BS(m_sLocalData.sBuyVehicle, VEH_PURCHASE_LOCAL_BS_SKIP_WARNING_SCREEN)
							CLEANUP_ENTER_MENU(m_sLocalData.sEnterMenu)
							SET_SHOWROOM_STATE(SHOWROOM_STATE_PURCHASE_VEHICLE)
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
		ENDSWITCH
		
		CLEAR_BIT(m_sLocalData.iBS, BS_LUXURY_SHOWROOM_MENU_OPTION_ACCEPTED)
	ELIF IS_BIT_SET(m_sLocalData.iBS, BS_LUXURY_SHOWROOM_MENU_OPTION_BACK)
		SWITCH m_sLocalData.sEnterMenu.eLayer
			CASE EML_DEFAULT
				PREP_PLAYER_FOR_EXITING_SHOP_MENU()
				CLEANUP_ENTER_MENU(m_sLocalData.sEnterMenu)
				SET_SHOWROOM_STATE(SHOWROOM_STATE_IDLE)
			BREAK
			CASE EML_CONFIRM_PURCHASE
				m_sLocalData.sEnterMenu.eLayer = EML_DEFAULT
				SET_BIT(m_sLocalData.iBS, BS_LUXURY_SHOWROOM_MENU_REBUILD)
			BREAK
		ENDSWITCH
		
		CLEAR_BIT(m_sLocalData.iBS, BS_LUXURY_SHOWROOM_MENU_OPTION_BACK)
	ENDIF
ENDPROC

PROC MAINTAIN_STATE_BROWSE_VEHICLE()
	
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		DISABLE_FRONTEND_THIS_FRAME()
	ENDIF
	
	// --- Bail ---
	IF SHOULD_EXIT_BROWSE_VEHICLE_STATE()
		CLEANUP_BROWSE_CAMERA()
		CLEANUP_BROWSE_CAM_INSTRUCTIONAL_BUTTONS()
		
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("LUX_SRM_H_SOON")
			CLEAR_HELP()
		ENDIF
		
		PREP_PLAYER_FOR_EXITING_SHOP_MENU()
		SET_SHOWROOM_STATE(SHOWROOM_STATE_IDLE)
		EXIT
	ENDIF
	
	DISABLE_CONTROL_ACTIONS_WHILE_BROWSING_VEHICLE()
	DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	MAINTAIN_SWITCH_BETWEEN_SHOWCASE_VEHICLES()
	UPDATE_SHOWROOM_CAMERA(GET_SHOWCASE_VEHICLE_TO_PURCHASE())
	
	// --- Main update while cam is active ---
	IF DOES_CAM_EXIST(m_sLocalData.sCamData.camera)
		IF NOT IS_THIS_VEHICLE_SAFE_TO_PURCHASE(GET_SHOWCASE_VEHICLE_TO_PURCHASE())
			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("LUX_SRM_H_SOON")
				PRINT_HELP_FOREVER("LUX_SRM_H_SOON")
			ENDIF
		ELIF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("LUX_SRM_H_SOON")
			CLEAR_HELP()
		ENDIF
		
		HIDE_HUD_AND_RADAR_THIS_FRAME()
		SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH)
		SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_MP_CASH)
		SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_MP_MESSAGE)
		SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH_CHANGE)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LR)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UD)
		
		IF NOT IS_WARNING_MESSAGE_ACTIVE()
		AND NOT IS_COMMERCE_STORE_OPEN()
			UPDATE_BROWSE_CAM_INSTRUCTIONAL_BUTTONS()
		ENDIF
		
		IF NOT IS_CAM_INTERPOLATING(m_sLocalData.sCamData.camera)
			IF IS_THIS_VEHICLE_SAFE_TO_PURCHASE(GET_SHOWCASE_VEHICLE_TO_PURCHASE())
			AND IS_PURCHASE_SHOWCASE_VEH_JUST_PRESSED(TRUE)
				SET_VEH_PURCHASE_LOCAL_BS(m_sLocalData.sBuyVehicle, VEH_PURCHASE_LOCAL_BS_PROCEED_WITH_PURCHASE)
				SET_SELECTED_PURCHASE_VEHICLE_INDEX(m_sLocalData.sBuyVehicle, &GET_SHOWCASE_VEHICLE_TO_PURCHASE)
				STORE_PRICE_OF_SHOWCASE_VEHICLE(GET_SHOWCASE_VEHICLE_TO_PURCHASE())
				STORE_NAME_OF_SHOWCASE_VEHICLE(GET_SHOWCASE_VEHICLE_TO_PURCHASE())
				SET_BIT(m_sLocalData.iBS, BS_LUXURY_SHOWROOM_PURCHASING_FROM_BROWSE_STATE)
				PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				SET_SHOWROOM_STATE(SHOWROOM_STATE_PURCHASE_VEHICLE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_STATE_PURCHASE_VEHICLE()
	
	// --- Go back to idle state if player has either left the locate, or can't purchase a vehicle. ---
	IF NOT CAN_PURCHASE_SHOWCASE_VEHICLE()
	OR NOT IS_LOCAL_PLAYER_IN_PROMPT_TRIGGER_LOCATE(FALSE)
		IF IS_BIT_SET(m_sLocalData.iBS, BS_LUXURY_SHOWROOM_PURCHASING_FROM_BROWSE_STATE)
			CLEANUP_BROWSE_CAMERA()
			CLEANUP_BROWSE_CAM_INSTRUCTIONAL_BUTTONS()
			
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("LUX_SRM_H_SOON")
				CLEAR_HELP()
			ENDIF
		ENDIF
		
		PREP_PLAYER_FOR_EXITING_SHOP_MENU()
		CLEANUP_PURCHASE_VEHICLE_DATA(m_sLocalData.sBuyVehicle, FALSE)
		SET_SHOWROOM_STATE(SHOWROOM_STATE_IDLE)
		EXIT
	ENDIF
	
	BOOL bSecondaryButton = IS_BIT_SET(m_sLocalData.iBS, BS_LUXURY_SHOWROOM_PURCHASING_FROM_BROWSE_STATE)
	SET_PURCHASE_SHOWCASE_VEHICLE_CONTROLS(bSecondaryButton)
	DISABLE_ALL_MOVEMENT_CONTROLS_THIS_FRAME()
	
	IF IS_BIT_SET(m_sLocalData.iBS, BS_LUXURY_SHOWROOM_PURCHASING_FROM_BROWSE_STATE)
		HIDE_HUD_AND_RADAR_THIS_FRAME()
		DISABLE_CONTROL_ACTIONS_WHILE_BROWSING_VEHICLE()
		UPDATE_SHOWROOM_CAMERA(GET_SHOWCASE_VEHICLE_TO_PURCHASE())
	ENDIF
	
	PURCHASE_SELECTED_VEHICLE(m_sLocalData.sBuyVehicle)
	IF IS_VEHICLE_PURCHASE_RESULT_READY(m_sLocalData.sBuyVehicle)
		GO_BACK_TO_PREVIOUS_STATE_FROM_PURCHASE()
		CLEANUP_PURCHASE_VEHICLE_DATA(m_sLocalData.sBuyVehicle, FALSE)
		PRINTLN("[AM_LUXURY_SHOWROOM] MAINTAIN_STATE_PURCHASE_VEHICLE - Purchase complete!")
	ENDIF
ENDPROC

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════════════╡ MAIN ╞═══════════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Terminates this thread if this function returns TRUE. 
FUNC BOOL SHOULD_TERMINATE_SCRIPT()
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		PRINTLN("[AM_LUXURY_SHOWROOM] SHOULD_TERMINATE_SCRIPT - NETWORK_IS_GAME_IN_PROGRESS is FALSE")
		RETURN TRUE
	ENDIF
	
	IF g_sMPTunables.bDISABLE_LUXURY_SHOWROOM
		RETURN TRUE
	ENDIF
	
	IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		PRINTLN("[AM_LUXURY_SHOWROOM] SHOULD_TERMINATE_SCRIPT - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE is TRUE")
		RETURN TRUE
	ENDIF
	
	IF (IS_LOCAL_PLAYER_IN_ANY_SIMPLE_INTERIOR() AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID()))
		PRINTLN("[AM_LUXURY_SHOWROOM] SHOULD_TERMINATE_SCRIPT - IS_LOCAL_PLAYER_IN_ANY_SIMPLE_INTERIOR is TRUE")
		RETURN TRUE
	ENDIF
	
	IF (IS_PLAYER_WALKING_INTO_SIMPLE_INTERIOR(PLAYER_ID()) AND IS_SCREEN_FADED_OUT())
		PRINTLN("[AM_LUXURY_SHOWROOM] SHOULD_TERMINATE_SCRIPT - IS_PLAYER_WALKING_INTO_SIMPLE_INTERIOR is TRUE")
		RETURN TRUE
	ENDIF
	
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
	AND GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()), GET_LUXURY_SHOWROOM_COORDS()) > cfLUXURY_SHOWROOM_KILL_DISTANCE
		PRINTLN("[AM_LUXURY_SHOWROOM] SHOULD_TERMINATE_SCRIPT - IS_PLAYER_NEAR_LUXURY_SHOWROOM is FALSE")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Main script cleanup which terminates this thread.
PROC SCRIPT_CLEANUP()
	PRINTLN("[AM_LUXURY_SHOWROOM] Cleaning up.")
	DELETE_SHOWCASE_VEHICLES()
	CLEANUP_BROWSE_CAMERA()
	RELEASE_CONTEXT_INTENTION(m_sLocalData.iEnterInput)
	CLEANUP_ENTER_MENU(m_sLocalData.sEnterMenu)
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("LUX_SRM_H_SOON")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("LUX_SRM_MSN")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("LUX_SRM_WANTED")
		CLEAR_HELP()
	ENDIF
	
	IF m_sLocalData.eFlowState > SHOWROOM_STATE_IDLE
		PREP_PLAYER_FOR_EXITING_SHOP_MENU()
	ENDIF
		
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

/// PURPOSE:
///    Main script initialise.
PROC SCRIPT_INITIALISE()
	PRINTLN("[AM_LUXURY_SHOWROOM] Initialisied")
	
	NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE)
	HANDLE_NET_SCRIPT_INITIALISATION()
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
	
	IF NOT Wait_For_First_Network_Broadcast()
		PRINTLN("[AM_LUXURY_SHOWROOM] FAILED TO RECEIVE INITIAL NETWORK BROADCAST - SCRIPT CLEANUP")
		SCRIPT_CLEANUP()
	ENDIF
	
	m_sLocalData.sBuyVehicle.fpVehPurchaseControls = &GET_SHOWCASE_VEHICLE_CONTROLS_PURCHASE
	VEHICLE_PURCHASE_INIT(m_sLocalData.sBuyVehicle, m_sLocalData.sBuyVehicle.fpVehPurchaseControls)
	SET_PURCHASE_VEHICLE_DELIVERY_CHARACTER(m_sLocalData.sBuyVehicle, CHAR_LUXURY_AUTOS_MPEMAIL)
	
	INIT_ABOVE_CAR_DISPLAY_STATS(m_sLocalData.aboveCarStatsDisplay)
	ADD_MOD_PRICES_WHEN_PURCHASING_VEHICLE(m_sLocalData.sBuyVehicle, TRUE)
	
	#IF IS_DEBUG_BUILD
	
	CREATE_LUXURY_SHOWROOM_WIDGETS()
	
	#IF SCRIPT_PROFILER_ACTIVE
	CREATE_SCRIPT_PROFILER_WIDGET() 
	#ENDIF
	
	#ENDIF
ENDPROC

/// PURPOSE:
///    Main script update.
PROC SCRIPT_UPDATE()
	SWITCH m_sLocalData.eFlowState
		CASE SHOWROOM_STATE_CREATE_VEHICLES
			MAINTAIN_STATE_CREATE_VEHICLES()
		BREAK
		CASE SHOWROOM_STATE_IDLE
			MAINTAIN_STATE_IDLE()
		BREAK
		CASE SHOWROOM_STATE_DISPLAY_MENU
			MAINTAIN_STATE_DISPLAY_MENU()
		BREAK
		CASE SHOWROOM_STATE_BROWSE_VEHICLE
			MAINTAIN_STATE_BROWSE_VEHICLE()
		BREAK
		CASE SHOWROOM_STATE_PURCHASE_VEHICLE
			MAINTAIN_STATE_PURCHASE_VEHICLE()
		BREAK
	ENDSWITCH
	
	MAINTAIN_DISPLAY_NAMES_AND_CREWS(m_sLocalData.namesDisplayData, m_sLocalData.aboveCarStatsDisplay, (m_sLocalData.eFlowState > SHOWROOM_STATE_IDLE))
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_DISPLAY_NAMES_AND_CREWS")
	#ENDIF	
	#ENDIF		
	
	MAINTAIN_SCRIPT_STAGGERS()
	
	#IF IS_DEBUG_BUILD
	UPDATE_LUXURY_SHOWROOM_WIDGETS()
	#ENDIF
ENDPROC

#ENDIF // #IF FEATURE_DLC_1_2022

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════════════╡ MAIN SCRIPT ╞════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛
SCRIPT
	#IF FEATURE_DLC_1_2022
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		SCRIPT_INITIALISE()
	ELSE
		SCRIPT_CLEANUP()
	ENDIF
	
	// ----- Main Loop -----
	WHILE TRUE
		MP_LOOP_WAIT_ZERO()
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		SCRIPT_PROFILER_START_OF_FRAME() 
		#ENDIF
		#ENDIF
		
		IF SHOULD_TERMINATE_SCRIPT()
		#IF IS_DEBUG_BUILD
		OR (GET_COMMANDLINE_PARAM_EXISTS("sc_ResettableLuxuryShowroom")
		AND IS_DEBUG_KEY_JUST_RELEASED(KEY_0, KEYBOARD_MODIFIER_ALT, ""))
		#ENDIF
			SCRIPT_CLEANUP()
		ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("SHOULD_TERMINATE_SCRIPT")
		#ENDIF	
		#ENDIF
	
		SCRIPT_UPDATE()
	ENDWHILE
	
	#ENDIF // #IF FEATURE_DLC_1_2022
ENDSCRIPT
