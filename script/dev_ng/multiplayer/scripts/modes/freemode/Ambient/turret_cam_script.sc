//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        turret_cam_script.sc																		//
// Description: Script to create and run a configurable "turret cam" (see turret_cam_public.sch).			//
//																											//
// 				The main design point for this set up is to try to push implementation 						//
//				specific script elsewhere. The rationale is that it should be easier to maintain the core 	//
//				system here without the clutter of hundreds of special cases. 								//
//																											//
//				It should also improve the workflow for creating new turrets. All of the spaghetti required //
//				to interface with the rest of the game and the new special cases and controls can be put in //
//				one place without having to dance around "if this_type AND loads_of_weird_edge_cases".		//	
//																											//
//				For an example of a concrete implementation please see arena_spectator_turret_public.sch	//
//																											//
//				Sections:																					//
//				* DEBUG-ONLY																				//
//				* HELPER																					//
//				* FULL-AUTO																					//
//				* HOMING-MISSILE																			//
//				* PILOTED-MISSILE																			//
//				* CONFIG-SETTINGS		- This section is where the config options are applied, updated		//
//										  and cleaned up.													//
//				* CORE-LOGIC			- Script update, init, cleanup and misc items.						//
//																											//
//				Supporting headers:																			//
//				* turret_cam_def.sch 	- struct, enum, look-up tables, and const defenitions.				//
//				* turret_cam_public.sch - "public" support methods.											//
//				* turret_cam_sup.sch 	- "private" support methods.										//
//																											//
// Written by:  Online Technical Team: Orlando C-H                                                          //
// Date:  		12/09/2018																					//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

// 	@@!TODO / Issues:
//  * There's no real reason some of the settings are ARGS and some are CONFIG.
//	* Create dummy object for turrets with no parent so that parent logic always works?
//	* Some of these methods should be in _sup or in script_weapon_common with a little tweaking.
// 	* Improve "loading" logic to create clear config "wait for" load requirements (wait for this, start streaming that and continue etc).

USING "globals.sch"
USING "turret_cam_def.sch"
USING "turret_cam_sup.sch"
USING "turret_manager_public.sch"
USING "script_weapon_common.sch"
USING "net_wait_zero.sch"
USING "net_drone.sch"
USING "hacker_combo_scanner.sch"
USING "net_spawn.sch"

TWEAK_FLOAT ci_MIN_BOX_X 0.015

TURRET_SETTINGS m_settings
TURRET_CAM_LOCAL_DATA m_script

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// DEBUG-ONLY																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

#IF IS_DEBUG_BUILD
ARENA_TURRET_DEBUG_DATA m_debug

FUNC STRING GET_STATE_STRING(TURRET_CAM_STATE eState)
	SWITCH eState
		CASE TCS_LOAD				RETURN "LOAD"					
		CASE TCS_UPDATE				RETURN "UPDATE"					
		DEFAULT 					RETURN "UNKNOWN_STATE"
	ENDSWITCH
ENDFUNC

PROC SET_CONTENTS_OF_TEXT_WIDGET_SAFE(TEXT_WIDGET_ID id, STRING sContents)
	IF NATIVE_TO_INT(id) = 0
		EXIT
	ENDIF
	IF IS_STRING_NULL(sContents)
		sContents = ""
	ENDIF
	SET_CONTENTS_OF_TEXT_WIDGET(id, sContents)
ENDPROC

FUNC TEXT_WIDGET_ID ADD_TEXT_WIDGET_WITH_STRING(STRING sTitle, STRING sInitial)
	IF IS_STRING_NULL(sTitle)
		sTitle = "NULL"
	ENDIF
	TEXT_WIDGET_ID id = ADD_TEXT_WIDGET(sTitle)
	
	IF IS_STRING_NULL(sInitial)
		sInitial = ""
	ENDIF
	SET_CONTENTS_OF_TEXT_WIDGET(id, sInitial)
	
	RETURN id
ENDFUNC

PROC CREATE_WIDGETS()
	START_WIDGET_GROUP("turret cam script")
		INT i
		START_NEW_WIDGET_COMBO()
		REPEAT TCS_COUNT i
			
		ENDREPEAT
		STOP_WIDGET_COMBO("State", m_debug.iState)
		START_WIDGET_GROUP("Debug")
			ADD_WIDGET_BOOL("Draw firing lines", m_debug.bDrawFiringLines)		
			
			START_WIDGET_GROUP("Homing missiles")
				ADD_WIDGET_INT_READ_ONLY("Target attempt entity", m_debug.iHomingMissileTargetEnt)
				ADD_WIDGET_INT_READ_ONLY("Shapetest entity hit", m_debug.iHomingMissileShapetestEnt)
				ADD_WIDGET_INT_READ_ONLY("Shapetest id", m_debug.iHomingMissileShapetestId)
				ADD_WIDGET_INT_READ_ONLY("Aqr soundId", m_script.homingMissile.iAcquiringSoundId)
				ADD_WIDGET_INT_READ_ONLY("Lock soundId", m_script.homingMissile.iLockedSoundId)
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()

		START_WIDGET_GROUP("Settings")
			START_WIDGET_GROUP("Turret coords")
				ADD_WIDGET_VECTOR_SLIDER("", m_settings.vTurretCoords, -5000.0, 5000.0, 1.0)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Joint (z)")
				ADD_WIDGET_VECTOR_SLIDER("offset", m_settings.cfg.vJointZ, -5000.0, 5000.0, 1.0)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Turret rotation")
				ADD_WIDGET_VECTOR_SLIDER("", m_settings.vTurretRotation, -5000.0, 5000.0, 1.0)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Limits")
				ADD_WIDGET_FLOAT_SLIDER("Heading min", m_settings.cfg.camLimits.fMinHeading, -180.0, 180.0, 1.0)
				ADD_WIDGET_FLOAT_SLIDER("Heading max", m_settings.cfg.camLimits.fMaxHeading, -180.0, 180.0, 1.0)
				ADD_WIDGET_FLOAT_SLIDER("Pitch min", m_settings.cfg.camLimits.fMinPitch, -180.0, 180.0, 1.0)
				ADD_WIDGET_FLOAT_SLIDER("Pitch max", m_settings.cfg.camLimits.fMaxPitch, -180.0, 180.0, 1.0)
				ADD_WIDGET_FLOAT_SLIDER("Fov min", m_settings.cfg.camLimits.fMinFov, 30, 100, 1.0)
				ADD_WIDGET_FLOAT_SLIDER("Fov max", m_settings.cfg.camLimits.fMaxFov, 30, 100, 1.0)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Speeds")
				ADD_WIDGET_FLOAT_SLIDER("Base zoom speed", m_settings.cfg.camSpeeds.fBaseZoomSpeed, 0.0, 100.0, 1.0)
				ADD_WIDGET_FLOAT_SLIDER("Base rot speed", m_settings.cfg.camSpeeds.fBaseRotSpeed, 0.0, 100.0, 1.0)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Weapon")
				ADD_WIDGET_INT_SLIDER("Damage", m_settings.cfg.weapon.iWeaponDamage, 0, 1000, 10)
				ADD_WIDGET_INT_SLIDER("Time between shots", m_settings.cfg.weapon.iTimeBetweenShotsMs, 0, 30000, 250)
				START_WIDGET_GROUP("Full auto")
					ADD_WIDGET_FLOAT_SLIDER("Bullet spread (degrees)", m_settings.cfg.fullAuto.fSpreadAngleMax,0.0, 180.0, 1.0)
					ADD_WIDGET_INT_SLIDER("Ammo duration (ms)", m_settings.cfg.fullAuto.iAmmoDurationMs, 0, 10000, 500)
					ADD_WIDGET_INT_SLIDER("Reload duration (ms)", m_settings.cfg.fullAuto.iReloadDurationMs, 0, 10000, 500)
				STOP_WIDGET_GROUP()
				START_WIDGET_GROUP("Homing missile")
					ADD_WIDGET_FLOAT_SLIDER("Min target box size", ci_MIN_BOX_X, 0.0, 1.0, 0.01)
				STOP_WIDGET_GROUP()
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Hud")
				ADD_WIDGET_STRING(m_settings.res.sStreamedHudTex)
				ADD_WIDGET_STRING(m_settings.res.sHudScaleformName)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Audio")
				START_WIDGET_GROUP("Hud")
					m_debug.tw_hudAudioBank = ADD_TEXT_WIDGET_WITH_STRING("Bank", m_script.audio.hud.sAudioBank)
					m_debug.tw_hudSoundSet = ADD_TEXT_WIDGET_WITH_STRING("Sound Set", m_script.audio.hud.sHudSoundSet)
					m_debug.tw_hudAudioScene = ADD_TEXT_WIDGET_WITH_STRING("Audio Scene", m_script.audio.hud.sHudAudioScene)
					m_debug.tw_hudAudioPan = ADD_TEXT_WIDGET_WITH_STRING("Sound : Pan", m_script.audio.hud.sPan)
					m_debug.tw_hudAudioZoom = ADD_TEXT_WIDGET_WITH_STRING("Sound : Zoom", m_script.audio.hud.sZoom)
					m_debug.tw_hudAudioBackground = ADD_TEXT_WIDGET_WITH_STRING("Sound : Background", m_script.audio.hud.sBackground)
				STOP_WIDGET_GROUP()			
				START_WIDGET_GROUP("Weapon")
					m_debug.tw_weaponAudioSoundSet = ADD_TEXT_WIDGET_WITH_STRING("Sound Set", m_script.audio.weapon.sWeaponSoundSet)
					m_debug.tw_weaponAudioFire = ADD_TEXT_WIDGET_WITH_STRING("Sound : Fire", m_script.audio.weapon.sFire)
				STOP_WIDGET_GROUP()
			STOP_WIDGET_GROUP()		
			START_WIDGET_GROUP("Loading")
				ADD_WIDGET_INT_SLIDER("Settings Bs", m_settings.cfg.load.iBs, 0, 255, 1)
				ADD_WIDGET_INT_SLIDER("Load scene wait time", m_settings.cfg.load.iLoadSceneWaitTimeMs, 0, 5000, 1)
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()	
	STOP_WIDGET_GROUP()
ENDPROC

PROC MAINTAIN_WIDGETS()	
	m_debug.iState = ENUM_TO_INT(m_script.eState)
	IF m_debug.bDrawFiringLines
		DRAW_DEBUG_LINE_WITH_TWO_COLOURS(m_debug.vFireStart, m_debug.vFireEndPoint)
	ENDIF
ENDPROC
#ENDIF // IS_DEBUG_BUILD


//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// HELPER																									//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

PROC SET_TURRET_CAM_STATE(TURRET_CAM_STATE eState)
	CDEBUG1LN(DEBUG_NET_TURRET, "[TURRET_CAM]) Set state from ", GET_STATE_STRING(m_script.eState), " to ", GET_STATE_STRING(eState))
	m_script.eState = eState
ENDPROC

FUNC BOOL IS_CONTROL_FIRE_JUST_PRESSED()
	RETURN IS_BIT_SET(g_iBsTurretCam, ci_TURRET_CAM_BS_SIM_INPUT_FIRE)
		OR IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RT)
ENDFUNC

FUNC BOOL IS_CONTROL_FIRE_PRESSED()
	RETURN IS_BIT_SET(g_iBsTurretCam, ci_TURRET_CAM_BS_SIM_INPUT_FIRE)
		OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RT)
ENDFUNC

PROC TURRET_CAM_INIT_PC_SCRIPTED_CONTROLS(STRING sSchemeName)
	IF IS_STRING_NULL_OR_EMPTY(sSchemeName)
		DEBUG_PRINTCALLSTACK()
		CDEBUG1LN(DEBUG_NET_TURRET, "[TURRET_CAM] PC CONTROLS ! IS_STRING_NULL_OR_EMPTY(sSchemeName)")
		EXIT
	ENDIF
	
	IF IS_BIT_SET(m_script.iBsLocal, ci_TURRET_CAM_LOCAL_BS_LOADED_PC_CONTROLS)
		CDEBUG1LN(DEBUG_NET_TURRET, "[TURRET_CAM] PC CONTROLS ! Already loaded ", sSchemeName)
	ELSE
		CDEBUG3LN(DEBUG_NET_TURRET, "[TURRET_CAM] PC CONTROLS Load ", sSchemeName)
		INIT_PC_SCRIPTED_CONTROLS(sSchemeName)
		SET_BIT(m_script.iBsLocal, ci_TURRET_CAM_LOCAL_BS_LOADED_PC_CONTROLS)
	ENDIF
ENDPROC

PROC TURRET_CAM_SHUTDOWN_PC_SCRIPTED_CONTROLS()
	IF IS_BIT_SET(m_script.iBsLocal, ci_TURRET_CAM_LOCAL_BS_LOADED_PC_CONTROLS)
		CDEBUG3LN(DEBUG_NET_TURRET, "[TURRET_CAM] PC CONTROLS Shutdown")
		SHUTDOWN_PC_SCRIPTED_CONTROLS()
		CLEAR_BIT(m_script.iBsLocal, ci_TURRET_CAM_LOCAL_BS_LOADED_PC_CONTROLS)
	ELSE
		CDEBUG1LN(DEBUG_NET_TURRET, "[TURRET_CAM] PC CONTROLS ! Not loaded ")
	ENDIF
ENDPROC

/// PURPOSE:
///    Continually check if PC control map should be suppressed or loaded.
///    Disable the controls with (m_script.iBsLocal, ci_TURRET_CAM_LOCAL_BS_SUPPRESS_PC_CONTROLS_THIS_FRAME)
PROC UPDATE_PC_SCRIPTED_CONTROLS()
	IF IS_BIT_SET(m_script.iBsLocal, ci_TURRET_CAM_LOCAL_BS_SUPPRESS_PC_CONTROLS_THIS_FRAME)
		IF IS_BIT_SET(m_script.iBsLocal, ci_TURRET_CAM_LOCAL_BS_LOADED_PC_CONTROLS)
			TURRET_CAM_SHUTDOWN_PC_SCRIPTED_CONTROLS()
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(m_script.iBsLocal, ci_TURRET_CAM_LOCAL_BS_LOADED_PC_CONTROLS)
		AND NOT IS_STRING_NULL_OR_EMPTY(m_settings.cfg.txt15InputMap)
			TURRET_CAM_INIT_PC_SCRIPTED_CONTROLS(m_settings.cfg.txt15InputMap)
		ENDIF
	ENDIF
ENDPROC

PROC UPDATE_CAMERA()
	// Cache parent data
	IF m_settings.eAttachType <> TCAT_NONE
		m_script.transformData.vParentWorldCoords = GET_ENTITY_COORDS(m_settings.entParent)
		m_script.transformData.vParentWorldRot = GET_ENTITY_ROTATION(m_settings.entParent)
	ENDIF
	
	CAM_OUTPUT results
	IF NOT IS_BIT_SET(m_script.iBsLocal, ci_TURRET_CAM_LOCAL_BS_FREEZE_CAM_THIS_FRAME)
		// Handle input
		results = UPDATE_TURRET_TRANSFORM(m_script.turretCam, m_script.vRelativeRotation, m_settings.cfg.camLimits, m_settings.cfg.camSpeeds.fBaseRotSpeed, m_settings.cfg.camSpeeds.fBaseZoomSpeed)
	ENDIF

	// Aplpy z joint offset
	VECTOR vRot = m_settings.vTurretRotation + m_script.vRelativeRotation
	VECTOR vJointZ = ROTATE_VECTOR_ABOUT_Z(m_settings.cfg.vJointZ,vRot.z)
	VECTOR vCoords = m_settings.vTurretCoords + vJointZ
	
	// Apply input changes to cam
	SWITCH m_settings.eAttachType
		CASE TCAT_NONE	APPLY_TURRET_TRANSFORM_UPDATE_STATIC(m_script.turretCam, vCoords, vRot) BREAK
		CASE TCAT_OBJ 	APPLY_TURRET_TRANSFORM_UPDATE_ENT_HEADING(m_script.turretCam, vCoords, vRot, m_settings.entParent) BREAK
	ENDSWITCH
	
	// Apply audio update based on input results and preset sound config.
	MAINTAIN_CAM_HUD_AUDIO(	m_script.audio.hud.sAudioBank, m_script.audio.hud.sHudSoundSet,
							m_script.audio.hud.iPanId, m_script.audio.hud.sPan,
							m_script.audio.hud.iZoomId, m_script.audio.hud.sZoom,
							m_script.audio.hud.iBackgroundId, m_script.audio.hud.sBackground,
							m_script.audio.hud.iTakeDamageId, m_script.audio.hud.sTakeDamage,
							m_script.audio.hud.iLowHealthLoopId, m_script.audio.hud.sLowHealthLoop,
							results.fFovChange, results.vRotChange,
							m_script.damageTaken.iDamage, m_script.damageTaken.iHeadshots > 0)
							
	// Cache these values for the rest of the script...
	m_script.transformData.vCamWorldCoords = GET_CAM_COORD(m_script.turretCam)
	m_script.transformData.vCamWorldRot = GET_CAM_ROT(m_script.turretCam)
ENDPROC

/// PURPOSE:
///    Calculate the start and end points for the projectile for a scripted weapon.
///    See SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY_NEW parameters.
/// PARAMS:
///    fStartPointDistance - Move the start point this far forwards
///    fEndPointDistance - Distance from start point
///    out_vStart - Start location
///    out_vEnd - End location
///    out_vFiringDir - Initial forwards direction 
///    out_vFiringRot - Initial rotation
PROC GET_PROJECTILE_START_AND_END_POINT_ADVANCED(	FLOAT fStartPointDistance,
													FLOAT fEndPointDistance, 
													VECTOR& out_vStart, 
													VECTOR& out_vEnd,
													VECTOR& out_vFiringDir,
													VECTOR& out_vFiringRot)

	IF m_settings.cfg.staticFirePoints.iCount = 0
		out_vFiringRot = GET_CAM_ROT(m_script.turretCam)
		out_vFiringDir = EULER_TO_DIRECTION_VECTOR(out_vFiringRot)
		out_vStart = m_script.transformData.vCamWorldCoords + (fStartPointDistance * out_vFiringDir)
		out_vEnd = GET_TURRET_PROJECTILE_ENDPOINT(out_vStart, fEndPointDistance)
	ELSE
		VECTOR vCamForwardsDir = EULER_TO_DIRECTION_VECTOR(GET_CAM_ROT(m_script.turretCam)) 
		VECTOR vCamRootPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(m_settings.entParent, m_settings.vTurretCoords) 
		VECTOR vPointPos = GET_ENTITY_BONE_POSTION(m_settings.entParent, m_settings.cfg.staticFirePoints.iBoneIds[0])
		VECTOR vPointOffset = vPointPos - vCamRootPos
		out_vStart = vPointPos // Store "best" solution in out_vStart

		INT iClosestPoint = 0
		FLOAT fLargestDot = DOT_PRODUCT(vPointOffset, vCamForwardsDir)
		
		INT i
		FOR i = 1 TO (m_settings.cfg.staticFirePoints.iCount - 1)
			vPointPos = GET_ENTITY_BONE_POSTION(m_settings.entParent, m_settings.cfg.staticFirePoints.iBoneIds[i])
			vPointOffset = vPointPos - vCamRootPos
			
			FLOAT fDot = DOT_PRODUCT(vPointOffset, vCamForwardsDir)
			IF fDot > fLargestDot
				fLargestDot = fDot
				iClosestPoint = i
				out_vStart = vPointPos // Store "best" solution in out_vStart
			ENDIF

		ENDFOR		

		out_vFiringRot = GET_ENTITY_BONE_ROTATION(m_settings.entParent, m_settings.cfg.staticFirePoints.iBoneIds[iClosestPoint])
		out_vFiringDir = EULER_TO_DIRECTION_VECTOR(out_vFiringRot)
		
		out_vStart += fStartPointDistance * out_vFiringDir
		out_vEnd = out_vStart + (fEndPointDistance * out_vFiringDir)
	ENDIF
	
	#IF IS_DEBUG_BUILD
	m_debug.vFireStart = out_vStart
	m_debug.vFireEndPoint = out_vEnd
	#ENDIF
ENDPROC

/// PURPOSE:
///    Calculate the start and end points for the projectile for a scripted weapon.
///    See SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY_NEW parameters.   
/// PARAMS:
///    fEndPointDistance - Move the start point this far forwards
///    out_vStart - Start location
///    out_vEnd - End location
PROC GET_PROJECTILE_START_AND_END_POINT(FLOAT fEndPointDistance, 
										VECTOR& out_vStart, 
										VECTOR& out_vEnd)
	VECTOR vThrowAway
	GET_PROJECTILE_START_AND_END_POINT_ADVANCED(0.0, fEndPointDistance, out_vStart, out_vEnd, vThrowAway, vThrowAway)
ENDPROC


//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// FULL-AUTO																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

FUNC BOOL IS_FULL_AUTO_RELOADING()
	RETURN NOT HAS_NET_TIMER_STARTED(m_script.fullAuto.reloadTimer)
		OR HAS_NET_TIMER_EXPIRED(m_script.fullAuto.reloadTimer, m_settings.cfg.fullAuto.iReloadDurationMs)
ENDFUNC

PROC START_FULL_AUTO_RELOAD()
	REINIT_NET_TIMER(m_script.fullAuto.reloadTimer)
	m_script.fullAuto.iAmmoTime = 0
ENDPROC

PROC PROCESS_FIRING_WEAPON_FULL_AUTO()
	SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_UseScriptedWeaponFirePosition, TRUE)
	
	BOOL bWeaponUsesReload =  m_settings.cfg.fullAuto.iAmmoDurationMs > 0
	BOOL bReloading = bWeaponUsesReload AND NOT HAS_NET_TIMER_EXPIRED(m_script.fullAuto.reloadTimer, m_settings.cfg.fullAuto.iReloadDurationMs)
	
	// Draw little ammo hud if we're using ammo
	IF bWeaponUsesReload
		HUD_COLOURS hcBarColour
		STRING sText
		INT iMeterValue, iMeterMax 
		
		IF bReloading
			hcBarColour = HUD_COLOUR_RED
			sText = "TC_RELOAD"
			iMeterMax = m_settings.cfg.fullAuto.iReloadDurationMs
			iMeterValue = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(m_script.fullAuto.reloadTimer)
		ELSE
			hcBarColour = HUD_COLOUR_WHITE
		 	sText = "TC_AMMO"
			iMeterMax = m_settings.cfg.fullAuto.iAmmoDurationMs
			iMeterValue = iMeterMax - m_script.fullAuto.iAmmoTime
		ENDIF 
		
		DRAW_GENERIC_METER(iMeterValue, iMeterMax, sText, hcBarColour, DEFAULT, HUDORDER_NINETHBOTTOM)
	ENDIF
	
	//@@ add this as an option?
	IF bWeaponUsesReload
	AND NOT bReloading
		IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RRIGHT)
			START_FULL_AUTO_RELOAD()
		ENDIF
	ENDIF
	
	IF NOT bReloading
	AND IS_CONTROL_FIRE_PRESSED()
		m_script.fullAuto.iAmmoTime += ROUND(GET_FRAME_TIME() * 1000)
		
		IF HAS_NET_TIMER_EXPIRED(m_script.lastShotTime, m_settings.cfg.weapon.iTimeBetweenShotsMs)
			REINIT_NET_TIMER(m_script.lastShotTime)

			// If this gun is using ammo (well, firing duration)...
			IF m_settings.cfg.fullAuto.iAmmoDurationMs > 0
			AND m_script.fullAuto.iAmmoTime >= m_settings.cfg.fullAuto.iAmmoDurationMs
				REINIT_NET_TIMER(m_script.fullAuto.reloadTimer)
				m_script.fullAuto.iAmmoTime = 0
			ENDIF

			VECTOR vStartCoord, vEndCoord, vDir, vRot
			GET_PROJECTILE_START_AND_END_POINT_ADVANCED(0.0, m_settings.cfg.weapon.fRange, vStartCoord, vEndCoord, vDir, vRot)
			
			// Rand spread
			VECTOR vSpread = m_settings.cfg.fullAuto.fSpreadAngleMax * GET_RANDOM_POINT_IN_SPHERE_UNIFORM(<<0,0,0>>, 1.0)
			vSpread.y = 0.0
			vRot += vSpread
			vEndCoord = vStartCoord + VMAG(vEndCoord - vStartCoord) * EULER_TO_DIRECTION_VECTOR(vRot) 
			
			// Fire from slightly below and behind camera position so we can see bullet tracer on screen
			vStartCoord.z -= 1.0
					
			// Override default firing lines debug
			#IF IS_DEBUG_BUILD
			m_debug.vFireStart = vStartCoord
			m_debug.vFireEndPoint = vEndCoord
			#ENDIF
			
			SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY_NEW(vStartCoord, vEndCoord, m_settings.cfg.weapon.iWeaponDamage, 
																FALSE, m_settings.cfg.weapon.eShotType, PLAYER_PED_ID(), DEFAULT,
																DEFAULT, DEFAULT, m_settings.entParent, DEFAULT, DEFAULT, DEFAULT,
																DEFAULT, DEFAULT, m_settings.eAttachType <> TCAT_NONE)
			
			IF m_script.audio.weapon.iFireId <> -1
			AND HAS_SOUND_FINISHED(m_script.audio.weapon.iFireId)
				PLAY_SOUND_FROM_COORD(m_script.audio.weapon.iFireId, m_script.audio.weapon.sFire, vStartCoord, m_script.audio.weapon.sWeaponSoundSet, TRUE, 500)
			ENDIF
		ENDIF
	ELSE
		IF m_script.audio.weapon.iFireId <> -1
		AND NOT HAS_SOUND_FINISHED(m_script.audio.weapon.iFireId)
			STOP_SOUND(m_script.audio.weapon.iFireId)
		ENDIF
	ENDIF
ENDPROC


//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// HOMING-MISSILE																							//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

/// PURPOSE:
///    Find players vehicle closest to screen centre who is not on same team as local player
///    and is in range (fMaxDistFromCam).
/// PARAMS:
///    fMaxDistFromCam - Max range a player will be selected.
/// RETURNS:
///    VEHICLE_INDEX of players vehicle that meets the conditions.
FUNC VEHICLE_INDEX TURRET_CAM_FIND_PLAYER_VEHICLE_CLOSEST_TO_CENTRE(FLOAT fMaxDistFromCam)
	INT iLocalTeam = GET_PLAYER_TEAM(PLAYER_ID())
	BOOL bCheckTeam = (iLocalTeam <> -1)
	
	PLAYER_INDEX piClosest
	FLOAT fSmallestDist2 = cf_TARGET_ACQUISITION_RANGE_2 // How far from screen centre will we scan?
	BOOL bFound = FALSE
	FLOAT fMaxWorldDist2 = fMaxDistFromCam * fMaxDistFromCam
	
	INT iPlayer
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		PLAYER_INDEX player = INT_TO_NATIVE(PLAYER_INDEX, iPlayer)
		PED_INDEX ped = GET_PLAYER_PED(player)
		
		IF NOT NETWORK_IS_PLAYER_ACTIVE(player)
		OR NOT IS_NET_PLAYER_OK(player)
		OR (bCheckTeam AND GET_PLAYER_TEAM(player) = iLocalTeam) 
		OR VMAG2(m_script.transformData.vCamWorldCoords - GET_ENTITY_COORDS(ped)) > fMaxWorldDist2
			RELOOP
		ENDIF
		
		FLOAT fDist2 = GET_ENT_DIST2_TO_SCREEN_CENTRE(ped)
		IF fDist2 < fSmallestDist2
			piClosest = player
			fSmallestDist2 = fDist2
			bFound = TRUE
		ENDIF
	ENDREPEAT
	
	IF bFound
		CDEBUG1LN(DEBUG_NET_TURRET, "Homing missile - TURRET_CAM_FIND_PLAYER_VEHICLE_CLOSEST_TO_CENTRE - Closest player == ", NATIVE_TO_INT(piClosest))
			
		PED_INDEX pedClosest = GET_PLAYER_PED(piClosest)
		
		IF IS_PED_IN_ANY_VEHICLE(pedClosest)
		
			VEHICLE_INDEX vehClosest =  GET_VEHICLE_PED_IS_IN(pedClosest)
			
			IF NOT IS_ENTITY_DEAD(vehClosest)
				RETURN vehClosest
			ENDIF
			
		ENDIF
	ENDIF
	
	RETURN INT_TO_NATIVE(VEHICLE_INDEX, -1)
ENDFUNC

PROC DRAW_BOX_AROUND_TARGET(ENTITY_INDEX target, BOOL bFlash, HUD_COLOURS eColour)
	
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED("helicopterhud")
		EXIT
	ENDIF
	
	FLOAT  fBoxDrawSizeX, fBoxDrawSizeY
	VECTOR vTargetPos
	INT r, g, b, a
	
	// So box doesn't overlay hud elements (i.e. radar).
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
	
	vTargetPos = GET_ENTITY_COORDS(target)
	
	// So box doesn't lag.
	SET_DRAW_ORIGIN(vTargetPos)
	
	// Get box data.
	fBoxDrawSizeX = GET_ENTITY_BOX_SIZE(target, m_script.turretCam, 0.5)
	fBoxDrawSizeX = CLAMP(fBoxDrawSizeX, ci_MIN_BOX_X, fBoxDrawSizeX)
	fBoxDrawSizeY = fBoxDrawSizeX * GET_ASPECT_RATIO(FALSE)
	GET_HUD_COLOUR(eColour, r, g, b, a)
	
	// Draw box.
	INT iBoxAlpha = PICK_INT(bFlash AND (GET_GAME_TIMER() % 300) < 150, CEIL(TO_FLOAT(a)/4.0), a)
	DRAW_SPRITE("helicopterhud", "hud_outline", 0.0, 0.0, fBoxDrawSizeX, fBoxDrawSizeY, 0, r, g, b, iBoxAlpha)
	
	CLEAR_DRAW_ORIGIN()
ENDPROC

PROC HOMING_MISSILE_SHAPETEST_START(ENTITY_INDEX target)
	CDEBUG3LN(DEBUG_NET_TURRET, "HOMING_MISSILE_START_SHAPETEST - Starting new LOS probe")
	VECTOR vStartPoint = GET_CAM_COORD(m_script.turretCam)
	VECTOR vEndPoint = GET_ENTITY_COORDS(target)
	vEndPoint += 2.0 * NORMALISE_VECTOR(vEndPoint - vStartPoint)
	m_script.homingMissile.shapetestId = START_SHAPE_TEST_LOS_PROBE(vStartPoint, vEndPoint, SCRIPT_INCLUDE_ALL, DEFAULT, SCRIPT_SHAPETEST_OPTION_IGNORE_NO_COLLISION)
	
	IF NATIVE_TO_INT(m_script.homingMissile.shapetestId) = 0 
		CDEBUG1LN(DEBUG_NET_TURRET, "[TURRET_CAM]   Couldn't create LOS probe test")
	ENDIF
ENDPROC

FUNC BOOL HOMING_MISSILE_SHAPETEST_FINISHED(SHAPETEST_INDEX& ref_id, ENTITY_INDEX target, BOOL& out_bResult)
	out_bResult = FALSE
	IF NATIVE_TO_INT(ref_id) = 0
	OR NATIVE_TO_INT(target) = -1
		CDEBUG3LN(DEBUG_NET_TURRET, "HOMING_MISSILE_SHAPETEST_FINISHED (true) : shapetestId = ", NATIVE_TO_INT(ref_id), " entityId = ", NATIVE_TO_INT(target))
		RETURN TRUE
	ENDIF
    
	INT iHitSomething
   	VECTOR vNormalAtPosHit, vTargetHitPos
 	ENTITY_INDEX entityHit
	SHAPETEST_STATUS eShapetestStatus = GET_SHAPE_TEST_RESULT(ref_id, iHitSomething, vTargetHitPos, vNormalAtPosHit, entityHit)
	
	IF NOT IS_ENTITY_A_VEHICLE(entityHit)
		IF IS_ENTITY_A_PED(entityHit)
			PED_INDEX pedHit = GET_PED_INDEX_FROM_ENTITY_INDEX(entityHit)
			IF IS_PED_IN_ANY_VEHICLE(pedHit)
				entityHit = GET_VEHICLE_PED_IS_IN(pedHit)
			ENDIF
		ENDIF		
	ENDIF
	
	IF eShapetestStatus = SHAPETEST_STATUS_RESULTS_READY
		IF iHitSomething <> 0
			IF DOES_ENTITY_EXIST(entityHit)
				out_bResult = (target = entityHit) AND (NOT IS_ENTITY_DEAD(entityHit)) 
				CDEBUG3LN(DEBUG_NET_TURRET, " Homing missiles --- Los probe success : target found. Los = ", out_bResult)
			ELSE
				CDEBUG3LN(DEBUG_NET_TURRET, " Homing missiles --- Los probe success : target not found")
			ENDIF
		ELSE
			CDEBUG3LN(DEBUG_NET_TURRET, " Homing missiles --- LOS probe didn't hit anything...")
	 	ENDIF
		RETURN TRUE
	ELIF eShapetestStatus = SHAPETEST_STATUS_NONEXISTENT
		CDEBUG3LN(DEBUG_NET_TURRET, " Homing missiles --- Los probe failed")
		RETURN TRUE
	ENDIF	
	RETURN FALSE
ENDFUNC

PROC HOMING_MISSILE_STOP_TARGETING()
	CDEBUG3LN(DEBUG_NET_TURRET, "HOMING_MISSILE_STOP_TARGETING")
	m_script.homingMissile.entShapetestHit = INT_TO_NATIVE(ENTITY_INDEX, -1)
	m_script.homingMissile.entTargetAttempt = INT_TO_NATIVE(ENTITY_INDEX, -1)
	STOP_AND_RELEASE_SOUND_ID(m_script.homingMissile.iAcquiringSoundId)
	STOP_AND_RELEASE_SOUND_ID(m_script.homingMissile.iLockedSoundId)
ENDPROC

PROC MAINTAIN_HOMING_MISSILE_PLAYER_TARGETS()
	BOOL bShapetestSuccess
	IF HOMING_MISSILE_SHAPETEST_FINISHED(m_script.homingMissile.shapetestId, m_script.homingMissile.entTargetAttempt, bShapetestSuccess)
		IF bShapetestSuccess
			// We have a new target...
			IF m_script.homingMissile.entShapetestHit <> m_script.homingMissile.entTargetAttempt
				REINIT_NET_TIMER(m_script.homingMissile.targetLockedTimer)
				STOP_AND_RELEASE_SOUND_ID(m_script.homingMissile.iAcquiringSoundId)
				STOP_AND_RELEASE_SOUND_ID(m_script.homingMissile.iLockedSoundId)
			ENDIF
			m_script.homingMissile.entShapetestHit = m_script.homingMissile.entTargetAttempt
		ELSE
			HOMING_MISSILE_STOP_TARGETING()
			VEHICLE_INDEX vehClosest = TURRET_CAM_FIND_PLAYER_VEHICLE_CLOSEST_TO_CENTRE(m_settings.cfg.weapon.fRange)
			m_script.homingMissile.entTargetAttempt = vehClosest
		ENDIF
		
		IF NATIVE_TO_INT(m_script.homingMissile.entTargetAttempt) <> -1
			HOMING_MISSILE_SHAPETEST_START(m_script.homingMissile.entTargetAttempt)
		ENDIF
	ENDIF
	
	IF NATIVE_TO_INT(m_script.homingMissile.entShapetestHit) <> -1
		// Check whether the entity is still on screen....
		IF m_script.homingMissile.entTargetAttempt <> m_script.homingMissile.entShapetestHit
		OR IS_ENTITY_DEAD(m_script.homingMissile.entShapetestHit)
		OR GET_ENT_DIST2_TO_SCREEN_CENTRE(m_script.homingMissile.entShapetestHit) > cf_TARGET_VALIDITY_RANGE_2
		OR VMAG(GET_ENTITY_COORDS(m_script.homingMissile.entShapetestHit) - m_script.transformData.vCamWorldCoords) > m_settings.cfg.weapon.fRange	
			HOMING_MISSILE_STOP_TARGETING()
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_FIRING_WEAPON_HOMING_MISSILE()
	MAINTAIN_HOMING_MISSILE_PLAYER_TARGETS()

	IF NATIVE_TO_INT(m_script.homingMissile.entShapetestHit) <> -1
		BOOL bLockedOn = HAS_NET_TIMER_EXPIRED(m_script.homingMissile.targetLockedTimer, ci_TARGET_ACQUISITION_TIME_MS)
		HUD_COLOURS eColour
		IF bLockedOn
			IF IS_ENTITY_A_VEHICLE(m_script.homingMissile.entTargetAttempt)
				VEHICLE_INDEX vehTarget = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(m_script.homingMissile.entTargetAttempt)
				SET_VEHICLE_HOMING_LOCKEDONTO_STATE(vehTarget, HLOS_ACQUIRED)
			ENDIF
			
			STOP_AND_RELEASE_SOUND_ID(m_script.homingMissile.iAcquiringSoundId)
			PLAY_SOUND_FRONTEND_WITH_NEW_ID(m_script.homingMissile.iLockedSoundId, "Bleep", "DLC_IE_Steal_EITS_Sounds")
			eColour = HUD_COLOUR_RED
		ELSE	
			IF IS_ENTITY_A_VEHICLE(m_script.homingMissile.entTargetAttempt)
				VEHICLE_INDEX vehTarget = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(m_script.homingMissile.entTargetAttempt)
				SET_VEHICLE_HOMING_LOCKEDONTO_STATE(vehTarget, HLOS_ACQUIRING)
			ENDIF
			
			PLAY_SOUND_FRONTEND_WITH_NEW_ID(m_script.homingMissile.iAcquiringSoundId, "VULKAN_LOCK_ON_AMBER")
			eColour = HUD_COLOUR_BLUE
		ENDIF
		DRAW_BOX_AROUND_TARGET(m_script.homingMissile.entTargetAttempt, NOT bLockedOn, eColour)
		
		IF IS_CONTROL_FIRE_JUST_PRESSED()
			CDEBUG3LN(DEBUG_NET_TURRET, "PROCESS_FIRING_WEAPON_HOMING_MISSILE : input pressed with target LOS. bLockedOn = ", bLockedOn)
			
			IF m_settings.cfg.homingMissile.eModel <> DUMMY_MODEL_FOR_SCRIPT
			AND NOT HAS_MODEL_LOADED(m_settings.cfg.homingMissile.eModel)
				PLAY_SOUND_FRONTEND(-1, "ERROR", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				CDEBUG3LN(DEBUG_NET_TURRET, "Model not loaded ", GET_MODEL_NAME_FOR_DEBUG(m_settings.cfg.homingMissile.eModel))
				EXIT
			ENDIF
			
			IF bLockedOn 
			AND HAS_NET_TIMER_EXPIRED(m_script.lastShotTime, m_settings.cfg.weapon.iTimeBetweenShotsMs)
				CDEBUG3LN(DEBUG_NET_TURRET, "PROCESS_FIRING_WEAPON_HOMING_MISSILE : Firing")
				
				REINIT_NET_TIMER(m_script.lastShotTime)

				VECTOR vStartCoord, vEndCoord
				GET_PROJECTILE_START_AND_END_POINT(1.0, vStartCoord, vEndCoord)
				
				SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY_NEW(vStartCoord, vEndCoord, m_settings.cfg.weapon.iWeaponDamage, 
																		TRUE, m_settings.cfg.weapon.eShotType, PLAYER_PED_ID(),
																		DEFAULT, DEFAULT, DEFAULT, m_settings.entParent, DEFAULT, DEFAULT,
																		m_script.homingMissile.entShapetestHit, DEFAULT, DEFAULT, 
																		m_settings.eAttachType <> TCAT_NONE)
																		
				PLAY_SOUND_FROM_COORD(m_script.audio.weapon.iFireId, m_script.audio.weapon.sFire, vStartCoord, m_script.audio.weapon.sWeaponSoundSet, TRUE, 500)
			ELSE
				PLAY_SOUND_FRONTEND(-1, "ERROR", "HUD_FRONTEND_DEFAULT_SOUNDSET")
			ENDIF	
		ENDIF
	ENDIF
		
	#IF IS_DEBUG_BUILD
	m_debug.iHomingMissileTargetEnt = NATIVE_TO_INT(m_script.homingMissile.entTargetAttempt)
	m_debug.iHomingMissileShapetestEnt = NATIVE_TO_INT(m_script.homingMissile.entShapetestHit)
	m_debug.iHomingMissileShapetestId = NATIVE_TO_INT(m_script.homingMissile.shapetestId)
	#ENDIF
ENDPROC


//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// PILOTED-MISSILE																							//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

PROC PROCESS_FIRING_WEAPON_PILOTED_MISSILE()
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_MP_DRONE")) < 1
		IF NOT m_script.pilotedMissile.bLaunchedDroneScriptFromHere
			CDEBUG1LN(DEBUG_NET_TURRET, "[TURRET_CAM]PROCESS_FIRING_WEAPON_PILOTED_MISSILE: Launching drone script from turret_cam_script")
			m_script.pilotedMissile.bLaunchedDroneScriptFromHere = TRUE
		ENDIF
		IF NOT START_DRONE(DRONE_TYPE_ARENA_WARS_MISSILES, <<0,0,0>>, m_script.transformData.vCamWorldCoords, <<0,0,0>>)
			IF IS_CONTROL_FIRE_JUST_PRESSED()
				PLAY_SOUND_FRONTEND(-1, "ERROR", "HUD_FRONTEND_DEFAULT_SOUNDSET")
			ENDIF
			CDEBUG1LN(DEBUG_NET_TURRET, "[TURRET_CAM]PROCESS_FIRING_WEAPON_PILOTED_MISSILE: Drone script not loaded. global kill drone script ", IS_BIT_SET(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_SET_KILL_SCRIPT))
			EXIT
		ENDIF
	ENDIF
	
	IF m_script.pilotedMissile.bDoingMissileTransition
		IF IS_SCREEN_FADED_OUT()
			IF m_script.pilotedMissile.bRequestedMissileStart
				IF IS_PLAYER_USING_DRONE(PLAYER_ID())
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME_SHORT)
					PLAY_SOUND_FROM_COORD(m_script.audio.weapon.iFireId, m_script.audio.weapon.sFire, m_script.pilotedMissile.vStartCoord, m_script.audio.weapon.sWeaponSoundSet, TRUE, 500)
					m_script.pilotedMissile.bRequestedMissileStart = FALSE
					m_script.pilotedMissile.bDoingMissileTransition = FALSE
				ENDIF
			ELSE
				m_script.pilotedMissile.bRequestedMissileStart = TRUE
				CDEBUG1LN(DEBUG_NET_TURRET, "[TURRET_CAM]PROCESS_FIRING_WEAPON_PILOTED_MISSILE: Firing piloted missile")
				
				REINIT_NET_TIMER(m_script.lastShotTime)
				SET_BIT(g_iBsTurretCam, ci_TURRET_CAM_BS_LAUNCH_DRONE_MISSILE)	
				
				VECTOR vEndCoord, vFireDir, vFireRot
				GET_PROJECTILE_START_AND_END_POINT_ADVANCED(3, 100, m_script.pilotedMissile.vStartCoord, vEndCoord, vFireDir, vFireRot)
				START_DRONE_FROM_ARENA_WARS_MISSILE(TRUE)
				SET_DRONE_COORD_AND_ROTATION(m_script.pilotedMissile.vStartCoord, vFireRot)
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_LOCAL_PLAYER_USING_DRONE()
	OR m_script.pilotedMissile.bDoingMissileTransition
		SET_BIT(m_script.iBsLocal, ci_TURRET_CAM_LOCAL_BS_HIDE_HUD_THIS_FRAME)
		SET_BIT(m_script.iBsLocal, ci_TURRET_CAM_LOCAL_BS_FREEZE_CAM_THIS_FRAME)
		SET_BIT(m_script.iBsLocal, ci_TURRET_CAM_LCOAL_BS_SUPPRESS_HELP_THIS_FRAME)
		
		// Once the screen has fully faded out & we're in the missile...
		IF NOT m_script.pilotedMissile.bDoingMissileTransition
			SET_BIT(m_script.iBsLocal, ci_TURRET_CAM_LOCAL_BS_SUPPRESS_PC_CONTROLS_THIS_FRAME)
			SET_BIT(m_script.iBsLocal, ci_TURRET_CAM_LOCAL_BS_SUPPRESS_TIMECYCLE_RESET_THIS_FRAME)
		ENDIF
		
		EXIT
	ENDIF
		
	IF IS_CONTROL_FIRE_JUST_PRESSED()
		IF HAS_NET_TIMER_EXPIRED(m_script.lastShotTime, m_settings.cfg.weapon.iTimeBetweenShotsMs)
			CDEBUG1LN(DEBUG_NET_TURRET, "[TURRET_CAM]PROCESS_FIRING_WEAPON_PILOTED_MISSILE: piloted missile input")
			m_script.pilotedMissile.bDoingMissileTransition = TRUE
			DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME_SHORT)
		ELSE
			PLAY_SOUND_FRONTEND(-1, "ERROR", "HUD_FRONTEND_DEFAULT_SOUNDSET")
		ENDIF
	ENDIF	
ENDPROC

PROC UPDATE_WEAPON()
	SWITCH m_settings.cfg.weapon.eFiringMode 
		CASE WM_FULL_AUTO 		PROCESS_FIRING_WEAPON_FULL_AUTO() 		BREAK
		CASE WM_HOMING_MISSILE	PROCESS_FIRING_WEAPON_HOMING_MISSILE() 	BREAK
		CASE WM_PILOTED_MISSILE	PROCESS_FIRING_WEAPON_PILOTED_MISSILE() BREAK
	ENDSWITCH
ENDPROC

PROC DRAW_TURRET_HUD()
	// Bail if special case no-hud
	IF m_settings.cfg.eHudType = TCHT_NONE
		EXIT
	ENDIF
	
	IF IS_BIT_SET(m_script.iBsLocal, ci_TURRET_CAM_LOCAL_BS_HIDE_HUD_THIS_FRAME)
		EXIT
	ENDIF	
	
	IF NOT HAS_SCALEFORM_MOVIE_LOADED(m_script.hudScaleformId)
		m_script.hudScaleformId = REQUEST_SCALEFORM_MOVIE(m_settings.res.sHudScaleformName)
		CDEBUG1LN(DEBUG_NET_TURRET, "[TURRET_CAM] - DRAW_TURRET_HUD scaleform not loaded ", m_settings.res.sHudScaleformName)
		EXIT
	ENDIF
	
	VECTOR vCamRot = GET_CAM_ROT(m_script.turretCam)
	
	SWITCH m_settings.cfg.eHudType
		CASE TCHT_ARENA_APOC
		CASE TCHT_ARENA_SCIFI
		CASE TCHT_ARENA_CONS		
			// Bit of "special case" logic for the Arena Huds
			IF NOT m_script.bHaveSetArenaHudWeaponType
				m_script.bHaveSetArenaHudWeaponType = TRUE
				ARENA_HUD_SET_ZOOM_IS_VISIBLE(m_script.hudScaleformId, m_settings.cfg.arenaHud.bZoomVisible)
				ARENA_HUD_SET_WEAPON_ICONS(	m_script.hudScaleformId,
											m_settings.cfg.arenaHud.eIconMg, 
											m_settings.cfg.arenaHud.eIconHm, 
											m_settings.cfg.arenaHud.eIconPm)
			ENDIF	
			FALLTHRU
			
		CASE TCHT_TURRET_CAM
			UPDATE_HUD_TURRET_CAM(	m_script.hudScaleformId, 
									0.0, 
									GET_CAM_FOV(m_script.turretCam),
							 		m_settings.cfg.camLimits.fMinFov, 
									m_settings.cfg.camLimits.fMaxFov, 
							 		vCamRot.z)
			BREAK
	ENDSWITCH
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD_PRIORITY_LOW)
	DRAW_SCALEFORM_MOVIE_FULLSCREEN(m_script.hudScaleformId, 255, 255, 255, 0)
ENDPROC


//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CONFIG-SETTINGS																							//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

/// PURPOSE:
///    Assert that all the bits between [0, ci_TURRET_CAM_CONFIG_BS_LAST_VALUE] are CLEAR.
///    Print the index of any set bits.
///    NOTE: Pass in (iBs ^ -1) to iBsClearedSettings to check that all iBs bits in range are SET.
DEBUGONLY PROC ASSERT_ALL_CFG_BITS_CLEAR(INT iBsClearedSettings, STRING sPrintTitle)
	UNUSED_PARAMETER(sPrintTitle)
	INT iMask = SHIFT_LEFT(1, ci_TURRET_CAM_CONFIG_BS_LAST_VALUE + 1) - 1
	INT iRemaining = iBsClearedSettings & iMask
	IF iRemaining <> 0
		CASSERTLN(DEBUG_NET_TURRET, sPrintTitle," requested but logic has not been added. Failed update configs bits: ", 
						iRemaining, ". See follwoing prints.")
						
		CDEBUG1LN(DEBUG_NET_TURRET, sPrintTitle, "   Remaining bits = ", iRemaining)				
		CDEBUG1LN(DEBUG_NET_TURRET, sPrintTitle, "   Bit indices:")				
		INT i
		REPEAT ci_TURRET_CAM_CONFIG_BS_LAST_VALUE i
			IF IS_BIT_SET(iRemaining, i)
				CDEBUG1LN(DEBUG_NET_TURRET, sPrintTitle, "      ", i)
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

/// PURPOSE:
///    Pass in the const ALLBITS (-1) to cleanup everything.
PROC CONFIG_CLEANUP(INT iBsConfigSettings) 
	// NOTE: Set unused resources names to "". 
	//		 Even if cleanup isn't required for any particular
	//		 Config bit, follow the pattern and add a CLEAR_BIT_IF_SET
	//		 block for it. This will catch bugs.
	
	IF CLEAR_BIT_IF_SET(iBsConfigSettings, ci_TURRET_CAM_CONFIG_BS_LIMITS)
	ENDIF
	
	IF CLEAR_BIT_IF_SET(iBsConfigSettings, ci_TURRET_CAM_CONFIG_BS_SPEEDS)
	ENDIF
	
	IF CLEAR_BIT_IF_SET(iBsConfigSettings, ci_TURRET_CAM_CONFIG_BS_WMODE)
		// Need to reset Arena Hud specifc check in case we switch
		// over to using one of the weapons / the hud.
		m_script.bHaveSetArenaHudWeaponType = FALSE 
		
		SWITCH m_settings.cfg.weapon.eFiringMode
			CASE WM_FULL_AUTO
				SET_PARTICLE_FX_BULLET_IMPACT_SCALE(1.0)
				//RESET_NET_TIMER(m_script.fullAuto.reloadTimer)
				BREAK
			CASE WM_PILOTED_MISSILE
				IF m_script.pilotedMissile.bDoingMissileTransition
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME_SHORT)
				ENDIF
				m_script.pilotedMissile.bRequestedMissileStart = FALSE
				m_script.pilotedMissile.bDoingMissileTransition = FALSE	
				
				IF m_script.pilotedMissile.bLaunchedDroneScriptFromHere
					m_script.pilotedMissile.bLaunchedDroneScriptFromHere = FALSE
					SET_KILL_DRONE_SCRIPT(TRUE)
				ENDIF
				
				BREAK
			CASE WM_HOMING_MISSILE
				HOMING_MISSILE_STOP_TARGETING()
				IF m_settings.cfg.homingMissile.eModel <> DUMMY_MODEL_FOR_SCRIPT
					SET_MODEL_AS_NO_LONGER_NEEDED(m_settings.cfg.homingMissile.eModel)
					m_settings.cfg.homingMissile.eModel = DUMMY_MODEL_FOR_SCRIPT
				ENDIF
				BREAK
		ENDSWITCH
		
		IF NOT IS_STRING_NULL_OR_EMPTY(m_settings.res.sStreamedHudTex)
			SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(m_settings.res.sStreamedHudTex)
			m_settings.res.sStreamedHudTex = ""
		ENDIF
	ENDIF
	
	IF CLEAR_BIT_IF_SET(iBsConfigSettings, ci_TURRET_CAM_CONFIG_BS_LOAD)
	ENDIF
	
	IF CLEAR_BIT_IF_SET(iBsConfigSettings, ci_TURRET_CAM_CONFIG_BS_HUD)
		// Need to reset Arena Hud specifc check in case we switch
		// over to using one of the weapons / the hud.
		m_script.bHaveSetArenaHudWeaponType = FALSE 
		
		m_settings.res.sHudScaleformName = ""
		IF m_script.hudScaleformId <> INT_TO_NATIVE(SCALEFORM_INDEX, -1)
			SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(m_script.hudScaleformId)
			m_script.hudScaleformId = INT_TO_NATIVE(SCALEFORM_INDEX, -1)
		ENDIF
	ENDIF
	
	IF CLEAR_BIT_IF_SET(iBsConfigSettings, ci_TURRET_CAM_CONFIG_BS_WEAPON_SOUNDS)
		STOP_AND_RELEASE_SOUND_ID(m_script.audio.weapon.iFireId)
	ENDIF
	
	IF CLEAR_BIT_IF_SET(iBsConfigSettings, ci_TURRET_CAM_CONFIG_BS_HUD_SOUNDS)
		STOP_AUDIO_SCENES()
		
		UNHINT_SCRIPT_AUDIO_BANK()
		IF NOT IS_STRING_NULL_OR_EMPTY(m_script.audio.hud.sAudioBank)
			RELEASE_NAMED_SCRIPT_AUDIO_BANK(m_script.audio.hud.sAudioBank)
		ENDIF
		
		STOP_AND_RELEASE_SOUND_ID(m_script.audio.hud.iPanId)
		STOP_AND_RELEASE_SOUND_ID(m_script.audio.hud.iZoomId)
		STOP_AND_RELEASE_SOUND_ID(m_script.audio.hud.iBackgroundId)
		STOP_AND_RELEASE_SOUND_ID(m_script.audio.hud.iTakeDamageId)
		STOP_AND_RELEASE_SOUND_ID(m_script.audio.hud.iLowHealthLoopId)
	ENDIF
	
	IF CLEAR_BIT_IF_SET(iBsConfigSettings, ci_TURRET_CAM_CONFIG_BS_INSTRUCTIONAL)
		IF m_settings.cfg.instButtons.iCount > 0
			CLEANUP_MENU_ASSETS()
		ENDIF
	ENDIF
	
	IF CLEAR_BIT_IF_SET(iBsConfigSettings, ci_TURRET_CAM_CONFIG_BS_HELP)
		IF NOT IS_STRING_NULL_OR_EMPTY(m_settings.cfg.txt15HelpLabel)
		AND IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(m_settings.cfg.txt15HelpLabel)
			CLEAR_HELP()
		ENDIF
	ENDIF

	IF CLEAR_BIT_IF_SET(iBsConfigSettings, ci_TURRET_CAM_CONFIG_BS_JOINT_Z)
	ENDIF
	
	IF CLEAR_BIT_IF_SET(iBsConfigSettings, ci_TURRET_CAM_CONFIG_BS_STAT_FIRE_PTS)
	ENDIF
	
	IF CLEAR_BIT_IF_SET(iBsConfigSettings, ci_TURRET_CAM_CONFIG_BS_PC_INPUT_MAP)
		IF NOT IS_STRING_NULL_OR_EMPTY(m_settings.cfg.txt15InputMap)
			TURRET_CAM_SHUTDOWN_PC_SCRIPTED_CONTROLS()
			m_settings.cfg.txt15InputMap = ""
		ENDIF
	ENDIF

	IF CLEAR_BIT_IF_SET(iBsConfigSettings, ci_TURRET_CAM_CONFIG_BS_ARENA_HUD)
		m_script.bHaveSetArenaHudWeaponType = FALSE 
	ENDIF

	ASSERT_ALL_CFG_BITS_CLEAR(iBsConfigSettings, "CFG CLEANUP")
ENDPROC

// Update the derived data & start loading assets
/// PURPOSE:
///    This function is used to register all resources used to 
///    m_settings.res, and then start streaming any regostered
///    resources.
/// PARAMS:
///    iBsConfigSettings - Specify which config parts to load.
PROC CONFIG_LOAD(INT iBsConfigSettings)
	//    This will be called during a config update so care has to
	//    taken not to leak resources (i.e. shadow m_script.hudScaleformId
	//    with a new scaleformId even though we use the same HUD after update).
	
	INT iBsCopy = iBsConfigSettings
	
	// ** Set config derived data **
	//
	// (Even if no load is required, you must explicitly show this to reduce bugs:)
	IF CLEAR_BIT_IF_SET(iBsCopy, ci_TURRET_CAM_CONFIG_BS_LIMITS)
	ENDIF
	
	IF CLEAR_BIT_IF_SET(iBsCopy, ci_TURRET_CAM_CONFIG_BS_SPEEDS)
	ENDIF
	
	IF CLEAR_BIT_IF_SET(iBsCopy, ci_TURRET_CAM_CONFIG_BS_WMODE)
		m_settings.cfg.weapon = g_turretCamConfig.weapon		
		
		SWITCH m_settings.cfg.weapon.eFiringMode
			CASE WM_FULL_AUTO
				// @@ this could get moved over to a config setting (even if part of full auto only)
				SET_PARTICLE_FX_BULLET_IMPACT_SCALE(cf_TURRET_CAM_FULL_AUTO_IMPACT_SCALE)
				
				// No ammo spent (and not reloading)?
				IF m_script.fullAuto.iAmmoTime = 0
				AND IS_FULL_AUTO_RELOADING()
					// Prevent intial reload.
					START_NET_TIMER_RETROACTIVELY(m_script.fullAuto.reloadTimer, m_settings.cfg.fullAuto.iReloadDurationMs)
				ELSE
					START_FULL_AUTO_RELOAD()
				ENDIF
				BREAK
				
			CASE WM_HOMING_MISSILE
					m_settings.res.sStreamedHudTex = "helicopterhud"
					m_settings.cfg.homingMissile = g_turretCamConfig.homingMissile
					IF m_settings.cfg.homingMissile.eModel <> DUMMY_MODEL_FOR_SCRIPT
						REQUEST_MODEL(m_settings.cfg.homingMissile.eModel)
					ENDIF
				BREAK
				
			DEFAULT BREAK
		ENDSWITCH
		
		IF NOT IS_STRING_NULL_OR_EMPTY(m_settings.res.sStreamedHudTex)
			REQUEST_STREAMED_TEXTURE_DICT(m_settings.res.sStreamedHudTex)	
		ENDIF
	ENDIF
	
	IF CLEAR_BIT_IF_SET(iBsCopy, ci_TURRET_CAM_CONFIG_BS_HUD)
		SWITCH m_settings.cfg.eHudType
			CASE TCHT_NONE		 	m_settings.res.sHudScaleformName = ""							BREAK
			CASE TCHT_TURRET_CAM 	m_settings.res.sHudScaleformName = "turret_cam"					BREAK
			CASE TCHT_ARENA_APOC 	m_settings.res.sHudScaleformName = "ARENA_GUN_CAM_APOCALYPSE"	BREAK
			CASE TCHT_ARENA_SCIFI 	m_settings.res.sHudScaleformName = "ARENA_GUN_CAM_SCIFI"		BREAK
			CASE TCHT_ARENA_CONS 	m_settings.res.sHudScaleformName = "ARENA_GUN_CAM_CONSUMER"		BREAK			
			DEFAULT 				m_settings.res.sHudScaleformName = "UNDEFINED"	BREAK
		ENDSWITCH	
		
		IF NOT IS_STRING_NULL_OR_EMPTY(m_settings.res.sHudScaleformName)
			m_script.hudScaleformId = REQUEST_SCALEFORM_MOVIE(m_settings.res.sHudScaleformName)
		ENDIF
	ENDIF
	
	IF CLEAR_BIT_IF_SET(iBsCopy, ci_TURRET_CAM_CONFIG_BS_WEAPON_SOUNDS)
		m_script.audio.weapon.iFireId = -1
		
		// Set up weapon fire sounds
		SWITCH m_settings.cfg.eWeaponSounds
			CASE TCWS_ARENA_TURRET_SOUNDS
				m_script.audio.weapon.sWeaponSoundSet = "dlc_aw_Arena_Spectator_Turret_turret_Sounds"
				m_script.audio.weapon.sFire = "Fire_MG_Loop" 
				m_script.audio.weapon.iFireId = GET_SOUND_ID()
				BREAK	
			CASE TCWS_ARENA_CONTESTANT_MG_SOUNDS
				m_script.audio.weapon.sWeaponSoundSet = "dlc_aw_Arena_Gun_Turret_Sounds"
				m_script.audio.weapon.sFire = "Fire_MG_Loop" 
				m_script.audio.weapon.iFireId = GET_SOUND_ID()
				BREAK	
			CASE TCWS_ARENA_CONTESTANT_HM_SOUNDS
				m_script.audio.weapon.sWeaponSoundSet = "dlc_aw_Arena_Gun_Turret_Sounds"
				m_script.audio.weapon.sFire = "Fire_Rockets_Oneshot" 
				m_script.audio.weapon.iFireId = GET_SOUND_ID()
				BREAK	
			CASE TCWS_ARENA_CONTESTANT_PM_SOUNDS
				m_script.audio.weapon.sWeaponSoundSet = "dlc_aw_Arena_Gun_Turret_Sounds"
				m_script.audio.weapon.sFire = "Launch_Piloted_Missile" 
				m_script.audio.weapon.iFireId = GET_SOUND_ID()
				BREAK				
			DEFAULT 
				CDEBUG3LN(DEBUG_NET_TURRET, "CONFIG_LOAD ci_TURRET_CAM_CONFIG_BS_WEAPON_SOUNDS : DEFAULT")
				m_script.audio.weapon.sWeaponSoundSet = NULL
				m_script.audio.weapon.sFire = NULL
				m_script.audio.weapon.iFireId = -1
				BREAK
		ENDSWITCH

		#IF IS_DEBUG_BUILD
		SET_CONTENTS_OF_TEXT_WIDGET_SAFE(m_debug.tw_weaponAudioSoundSet, m_script.audio.weapon.sWeaponSoundSet)
		SET_CONTENTS_OF_TEXT_WIDGET_SAFE(m_debug.tw_weaponAudioFire, m_script.audio.weapon.sFire)
		#ENDIF //IS_DEBUG_BUILD
	ENDIF
	
	IF CLEAR_BIT_IF_SET(iBsCopy, ci_TURRET_CAM_CONFIG_BS_HUD_SOUNDS)
		m_script.audio.hud.iBackgroundId = -1
		m_script.audio.hud.iPanId = -1
		m_script.audio.hud.iZoomId = -1
		m_script.audio.hud.iTakeDamageId = -1
		m_script.audio.hud.iLowHealthLoopId = -1
		
		// Set up sounds for this hud
		// Cleanup will UNHINT all banks associated with the script.
		SWITCH m_settings.cfg.eHudSounds
			CASE TCHS_ARENA_CAM
				m_script.audio.hud.sAudioBank = "SCRIPT\\POLICE_CHOPPER_CAM"
				m_script.audio.hud.sHudAudioScene = "dlc_aw_arena_turret_scene"
				m_script.audio.hud.sHudSoundSet = "dlc_aw_Arena_Spectator_Turret_turret_Sounds"
				
				m_script.audio.hud.sBackground = "Turret_Camera_Hum_Loop"
				m_script.audio.hud.iBackgroundId = GET_SOUND_ID()
				
				m_script.audio.hud.sPan = "Pan"
				m_script.audio.hud.iPanId = GET_SOUND_ID()
				
				m_script.audio.hud.sZoom = "Zoom"
				m_script.audio.hud.iZoomId = GET_SOUND_ID()
				BREAK
			CASE TCHS_ARENA_CONTESTANT_CAM
				m_script.audio.hud.sAudioBank = "SCRIPT\\POLICE_CHOPPER_CAM"
				m_script.audio.hud.sHudAudioScene = "dlc_aw_arena_turret_scene"
				m_script.audio.hud.sHudSoundSet = "dlc_aw_Arena_Gun_Turret_Sounds"
				
				m_script.audio.hud.sBackground = "Turret_Camera_Hum_Loop"
				m_script.audio.hud.iBackgroundId = GET_SOUND_ID()
				
				m_script.audio.hud.sPan = "Pan"
				m_script.audio.hud.iPanId = GET_SOUND_ID()
				
				m_script.audio.hud.sZoom = "Zoom"
				m_script.audio.hud.iZoomId = GET_SOUND_ID()		
				
				m_script.audio.hud.sTakeDamage = "Take_Damage"
				m_script.audio.hud.iTakeDamageId = GET_SOUND_ID()	
				
				m_script.audio.hud.sLowHealthLoop = "Low_Health_Warning"
				m_script.audio.hud.iLowHealthLoopId = GET_SOUND_ID()	
				BREAK
			DEFAULT
				CDEBUG3LN(DEBUG_NET_TURRET, "CONFIG_LOAD ci_TURRET_CAM_CONFIG_BS_HUD_SOUNDS : DEFAULT")
				m_script.audio.hud.sAudioBank = NULL
				m_script.audio.hud.sHudSoundSet = NULL
				
				m_script.audio.hud.sBackground = NULL
				m_script.audio.hud.iBackgroundId = -1
				
				m_script.audio.hud.sPan = NULL
				m_script.audio.hud.iPanId = -1
				
				m_script.audio.hud.sZoom = NULL
				m_script.audio.hud.iZoomId = -1
				BREAK
		ENDSWITCH
		
		IF NOT IS_STRING_NULL_OR_EMPTY(m_script.audio.hud.sAudioBank)
			HINT_SCRIPT_AUDIO_BANK(m_script.audio.hud.sAudioBank)
		ENDIF

		#IF IS_DEBUG_BUILD
		SET_CONTENTS_OF_TEXT_WIDGET_SAFE(m_debug.tw_hudAudioBank, m_script.audio.hud.sAudioBank)
		SET_CONTENTS_OF_TEXT_WIDGET_SAFE(m_debug.tw_hudSoundSet, m_script.audio.hud.sHudSoundSet)
		SET_CONTENTS_OF_TEXT_WIDGET_SAFE(m_debug.tw_hudAudioScene, m_script.audio.hud.sHudAudioScene)
		SET_CONTENTS_OF_TEXT_WIDGET_SAFE(m_debug.tw_hudAudioPan, m_script.audio.hud.sPan)
		SET_CONTENTS_OF_TEXT_WIDGET_SAFE(m_debug.tw_hudAudioZoom, m_script.audio.hud.sZoom)
		SET_CONTENTS_OF_TEXT_WIDGET_SAFE(m_debug.tw_hudAudioBackground, m_script.audio.hud.sBackground)		
		#ENDIF //IS_DEBUG_BUILD
	ENDIF
	
	// @@ special case?
	IF CLEAR_BIT_IF_SET(iBsCopy, ci_TURRET_CAM_CONFIG_BS_LOAD)
	ENDIF
	
	IF CLEAR_BIT_IF_SET(iBsCopy, ci_TURRET_CAM_CONFIG_BS_INSTRUCTIONAL)
		IF m_settings.cfg.instButtons.iCount > 0
			LOAD_MENU_ASSETS()
		ENDIF
	ENDIF
	
	IF CLEAR_BIT_IF_SET(iBsCopy, ci_TURRET_CAM_CONFIG_BS_HELP)
	ENDIF
	
	IF CLEAR_BIT_IF_SET(iBsCopy, ci_TURRET_CAM_CONFIG_BS_JOINT_Z)
	ENDIF
	
	IF CLEAR_BIT_IF_SET(iBsCopy, ci_TURRET_CAM_CONFIG_BS_STAT_FIRE_PTS)
	ENDIF

	IF CLEAR_BIT_IF_SET(iBsCopy, ci_TURRET_CAM_CONFIG_BS_PC_INPUT_MAP)
		IF NOT IS_STRING_NULL_OR_EMPTY(m_settings.cfg.txt15InputMap)
			TURRET_CAM_INIT_PC_SCRIPTED_CONTROLS(m_settings.cfg.txt15InputMap)
		ENDIF
	ENDIF

	IF CLEAR_BIT_IF_SET(iBsCopy, ci_TURRET_CAM_CONFIG_BS_ARENA_HUD)
	ENDIF

	ASSERT_ALL_CFG_BITS_CLEAR(iBsCopy, "CFG LOAD")
	TURRET_CAM_PRINT_CONFIG(m_settings.cfg)
ENDPROC

/// PURPOSE:
///    Set data from global config. If one of the cfg bits isn't set then
///    we just call the relevant function with all default values to set
///    the global before reading it.
PROC CONFIG_INIT()
	IF NOT IS_BIT_SET(g_iBsTurretCamConfigSettings, ci_TURRET_CAM_CONFIG_BS_LIMITS)
		TURRET_CAM_CONFIG_CAM_LIMITS()
	ENDIF
	
	IF NOT IS_BIT_SET(g_iBsTurretCamConfigSettings, ci_TURRET_CAM_CONFIG_BS_SPEEDS)
		TURRET_CAM_CONFIG_CAM_SPEEDS()
	ENDIF	

	IF NOT IS_BIT_SET(g_iBsTurretCamConfigSettings, ci_TURRET_CAM_CONFIG_BS_WMODE)
		TURRET_CAM_CONFIG_WEAPON_NONE()				
	ENDIF
	
	IF NOT IS_BIT_SET(g_iBsTurretCamConfigSettings, ci_TURRET_CAM_CONFIG_BS_LOAD)
		TURRET_CAM_CONFIG_SET_LOAD_ARGS()
	ENDIF
	
	IF NOT IS_BIT_SET(g_iBsTurretCamConfigSettings, ci_TURRET_CAM_CONFIG_BS_HUD)
		TURRET_CAM_CONFIG_HUD()
	ENDIF
	
	IF NOT IS_BIT_SET(g_iBsTurretCamConfigSettings, ci_TURRET_CAM_CONFIG_BS_HUD_SOUNDS)
		TURRET_CAM_CONFIG_HUD_SOUNDS()
	ENDIF
	
	IF NOT IS_BIT_SET(g_iBsTurretCamConfigSettings, ci_TURRET_CAM_CONFIG_BS_WEAPON_SOUNDS)
		TURRET_CAM_CONFIG_WEAPON_SOUNDS()
	ENDIF
	
	// If no buttons were added then we will ignore the instructional button scaleform.
	IF NOT IS_BIT_SET(g_iBsTurretCamConfigSettings, ci_TURRET_CAM_CONFIG_BS_INSTRUCTIONAL)
		SET_BIT(g_iBsTurretCamConfigSettings, ci_TURRET_CAM_CONFIG_BS_INSTRUCTIONAL)
		g_turretCamConfig.instButtons.iCount = 0
	ENDIF
	
	IF NOT IS_BIT_SET(g_iBsTurretCamConfigSettings, ci_TURRET_CAM_CONFIG_BS_HELP)
		TURRET_CAM_CONFIG_ADD_HELP_TEXT()
	ENDIF
	
	IF NOT IS_BIT_SET(g_iBsTurretCamConfigSettings, ci_TURRET_CAM_CONFIG_BS_JOINT_Z)
		TURRET_CAM_CONFIG_SET_JOINT_Z(<<0,0,0>>)
	ENDIF
	
	IF NOT IS_BIT_SET(g_iBsTurretCamConfigSettings, ci_TURRET_CAM_CONFIG_BS_STAT_FIRE_PTS)
		SET_BIT(g_iBsTurretCamConfigSettings, ci_TURRET_CAM_CONFIG_BS_STAT_FIRE_PTS)
		g_turretCamConfig.staticFirePoints.iCount = 0
	ENDIF
	
	IF NOT IS_BIT_SET(g_iBsTurretCamConfigSettings, ci_TURRET_CAM_CONFIG_BS_PC_INPUT_MAP)
		TURRET_CAM_CONFIG_SET_INPUT_MAP()
	ENDIF
	
	IF NOT IS_BIT_SET(g_iBsTurretCamConfigSettings, ci_TURRET_CAM_CONFIG_BS_ARENA_HUD)
		TURRET_CAM_CONFIG_ARENA_HUD()
	ENDIF
	
	// If you're hitting this assert:
	// * No default values have been set up for the config bits shown in the assert.
	// * You need to add a call to the config function for any unset config bits:
	//   see examples above.
	// * If you can't call the config func with default args you must set the relevant bit
	//	 after you've manually assigned default values (see ci_TURRET_CAM_CONFIG_BS_STAT_FIRE_PTS).
	ASSERT_ALL_CFG_BITS_CLEAR(g_iBsTurretCamConfigSettings ^ -1, "CFG INIT")
	
	m_settings.cfg = g_turretCamConfig
	
	CONFIG_LOAD(g_iBsTurretCamConfigSettings)
	SET_TURRET_CAM_STATE(TCS_LOAD)
ENDPROC

PROC CONFIG_UPDATE(INT iBsConfigSettings)
	CDEBUG1LN(DEBUG_NET_TURRET, "[TURRET_CAM]CONFIG_UPDATE...")
	
	// Take a copy so we can flip bits to determine
	// whether the requested config update is implemented.
	INT iBsCopy = iBsConfigSettings
	
	// Cleanup the changing elements
	CONFIG_CLEANUP(iBsConfigSettings)
	
	// Apply config changes
	IF CLEAR_BIT_IF_SET(iBsCopy, ci_TURRET_CAM_CONFIG_BS_WMODE)
		REINIT_NET_TIMER(m_script.lastShotTime)
		m_settings.cfg.weapon = g_turretCamConfig.weapon
	ENDIF
	
	IF CLEAR_BIT_IF_SET(iBsCopy, ci_TURRET_CAM_CONFIG_BS_WEAPON_SOUNDS)
		m_settings.cfg.eWeaponSounds = g_turretCamConfig.eWeaponSounds		
	ENDIF
	
	IF CLEAR_BIT_IF_SET(iBsCopy, ci_TURRET_CAM_CONFIG_BS_STAT_FIRE_PTS)
		m_settings.cfg.staticFirePoints = g_turretCamConfig.staticFirePoints		
	ENDIF
	
	IF CLEAR_BIT_IF_SET(iBsCopy, ci_TURRET_CAM_CONFIG_BS_HELP)
		m_settings.cfg.txt15HelpLabel = g_turretCamConfig.txt15HelpLabel
	ENDIF
	
	IF CLEAR_BIT_IF_SET(iBsCopy, ci_TURRET_CAM_CONFIG_BS_ARENA_HUD)
		m_settings.cfg.arenaHud = g_turretCamConfig.arenaHud
	ENDIF
	
	ASSERT_ALL_CFG_BITS_CLEAR(iBsCopy, "CFG UPDATE")
	
	// Load new elements
	CONFIG_LOAD(iBsConfigSettings)
ENDPROC


//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CORE-LOGIC																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

PROC UPDATE_INSTRUCTIONAL_BUTTONS()
	IF IS_BIT_SET(g_iBsTurretCam, ci_TURRET_CAM_BS_FORCE_INSTR_BTN_RELOAD)
		CLEAR_BIT(g_iBsTurretCam, ci_TURRET_CAM_BS_FORCE_INSTR_BTN_RELOAD)
		CDEBUG1LN(DEBUG_NET_TURRET, "[TURRET_CAM] forcing instructional button reload")
		m_script.bHaveSetInstButtons = FALSE
	ENDIF
	TURRET_CAM_MAINTAIN_INSTRUCTIONAL_BUTTONS(m_settings.cfg.instButtons, m_script.bHaveSetInstButtons)
ENDPROC

PROC UPDATE_TIMECYCLE_MODIFIER()
	IF NOT IS_BIT_SET(m_script.iBsLocal, ci_TURRET_CAM_LOCAL_BS_SUPPRESS_TIMECYCLE_RESET_THIS_FRAME)
		INT iCurTimecyleModId = GET_TIMECYCLE_MODIFIER_INDEX()
		IF m_script.iTimecycleModId <> iCurTimecyleModId
			SET_TIMECYCLE_MODIFIER("eyeinthesky")
			m_script.iTimecycleModId = GET_TIMECYCLE_MODIFIER_INDEX()
			CDEBUG2LN(DEBUG_NET_TURRET, "[TURRET_CAM] TIMECYCLE Resetting modifier. Index from ", iCurTimecyleModId, " to ", m_script.iTimecycleModId)
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_UPDATE()
	IF IS_BIT_SET(g_iBsTurretCam, ci_TURRET_CAM_BS_CONFIG_UPDATE)
		CLEAR_BIT(g_iBsTurretCam, ci_TURRET_CAM_BS_CONFIG_UPDATE)
		CONFIG_UPDATE(g_iBsTurretCamConfigUpdate)	
	ENDIF
	
	// Update damage tracker
	IF m_script.damageTaken.iFrame <> g_turretDamageTaken.iFrame
		m_script.damageTaken = g_turretDamageTaken
		CDEBUG1LN(DEBUG_NET_TURRET, "[TURRET_CAM] Damage taken recently: ", m_script.damageTaken.iDamage, " headshot = ", m_script.damageTaken.iHeadshots)
	ELSE
		m_script.damageTaken.iDamage = 0
		m_script.damageTaken.iHeadshots = 0
	ENDIF
	
	IF NOT IS_PAUSE_MENU_ACTIVE()
	AND NOT IS_BROWSER_OPEN()
		UPDATE_WEAPON()
		UPDATE_CAMERA()
		DRAW_TURRET_HUD()
		UPDATE_PC_SCRIPTED_CONTROLS()
		UPDATE_TIMECYCLE_MODIFIER()
		UPDATE_INSTRUCTIONAL_BUTTONS()
	ENDIF 
	
	TURRET_CAM_MAINTAIN_HELP_TEXT(m_settings.cfg.txt15HelpLabel, IS_BIT_SET(m_script.iBsLocal, ci_TURRET_CAM_LCOAL_BS_SUPPRESS_HELP_THIS_FRAME))
	MAINTAIN_TURRET_WEAPON_BLIP(m_script.blipForeground, m_script.blipBackground)

	// Clear *this frame only* bits
	CLEAR_BIT(g_iBsTurretCam, ci_TURRET_CAM_BS_SIM_INPUT_FIRE)
	CLEAR_BIT(m_script.iBsLocal, ci_TURRET_CAM_LOCAL_BS_HIDE_HUD_THIS_FRAME)
	CLEAR_BIT(m_script.iBsLocal, ci_TURRET_CAM_LOCAL_BS_FREEZE_CAM_THIS_FRAME)
	CLEAR_BIT(m_script.iBsLocal, ci_TURRET_CAM_LCOAL_BS_SUPPRESS_HELP_THIS_FRAME)		
	CLEAR_BIT(m_script.iBsLocal, ci_TURRET_CAM_LOCAL_BS_SUPPRESS_PC_CONTROLS_THIS_FRAME)
	CLEAR_BIT(m_script.iBsLocal, ci_TURRET_CAM_LOCAL_BS_SUPPRESS_TIMECYCLE_RESET_THIS_FRAME)
ENDPROC

FUNC BOOL DO_STATIC_CAM_LOAD_SCENE(VECTOR vSceneCoords, NEWLOADSCENE_FLAGS sceneFlags = 0)
	IF IS_BIT_SET(m_script.iBsLocal, ci_TURRET_CAM_LOCAL_BS_LOAD_SCENE)		
		IF IS_NEW_LOAD_SCENE_LOADED()
			CDEBUG3LN(DEBUG_NET_TURRET, "[DO_STATIC_CAM_LOAD_SCENE] - TRUE")	
			RETURN TRUE
		ENDIF
	ELSE	
		IF NEW_LOAD_SCENE_START_SPHERE(vSceneCoords, 100, sceneFlags)
			CDEBUG3LN(DEBUG_NET_TURRET, "[DO_STATIC_CAM_LOAD_SCENE] - STARTED ")	
			SET_BIT(m_script.iBsLocal, ci_TURRET_CAM_LOCAL_BS_LOAD_SCENE)		
		ENDIF
	ENDIF		
	RETURN FALSE
ENDFUNC


// create camera
PROC MAINTAIN_LOAD()
	IF NOT IS_SCREEN_FADING_OUT()
	AND NOT IS_SCREEN_FADED_OUT()
	AND IS_BIT_SET(m_settings.cfg.load.iBs, ci_TURRET_CAM_LOAD_BS_FADEOUT)
		DO_SCREEN_FADE_OUT(m_settings.cfg.load.iLoadSceneWaitTimeMs)
	ENDIF
	
	IF HAS_NET_TIMER_EXPIRED(m_script.loadSceneTimer, m_settings.cfg.load.iLoadSceneWaitTimeMs)
		IF NOT IS_BIT_SET(m_settings.cfg.load.iBs, ci_TURRET_CAM_LOAD_BS_LOADSCENE)
		OR DO_STATIC_CAM_LOAD_SCENE(m_settings.vTurretCoords)
		
			IF NOT DOES_CAM_EXIST(m_script.turretCam)
				CDEBUG2LN(DEBUG_NET_TURRET, ") - Creating cam")
				m_script.turretCam = CREATE_CAMERA(DEFAULT, TRUE)
				
				// Put cam in correct location 
				SET_BIT(m_script.iBsLocal, ci_TURRET_CAM_LOCAL_BS_FREEZE_CAM_THIS_FRAME)
				UPDATE_CAMERA()
				CLEAR_BIT(m_script.iBsLocal, ci_TURRET_CAM_LOCAL_BS_FREEZE_CAM_THIS_FRAME)
				
				// World coords cached in m_script.transformData.vCamWorldCoords by UPDATE_CAMERA call
				CDEBUG1LN(DEBUG_NET_TURRET, "[TURRET_CAM]CONFIG_LOAD: SET_SCRIPT_FIRE_POSITION: ", m_script.transformData.vCamWorldCoords)
				SET_SCRIPT_FIRE_POSITION(m_script.transformData.vCamWorldCoords)
				
			ELSE
				CDEBUG2LN(DEBUG_NET_TURRET, ") - Created cam")
			
				IF IS_BIT_SET(m_settings.cfg.load.iBs, ci_TURRET_CAM_LOAD_BS_LOADSCENE)
					NEW_LOAD_SCENE_STOP()
				ENDIF
				
				RENDER_SCRIPT_CAMS(TRUE, FALSE, 0 , TRUE, TRUE)
				
				MAINTAIN_TURRET_WEAPON_BLIP(m_script.blipForeground, m_script.blipBackground)
				DRAW_TURRET_HUD()			
						
				IF NOT IS_STRING_NULL_OR_EMPTY(m_script.audio.hud.sHudAudioScene)
					START_AUDIO_SCENE(m_script.audio.hud.sHudAudioScene)
				ENDIF 
				
				REINIT_NET_TIMER(m_script.lastShotTime)
						
				SET_BIT(g_iBsTurretCam, ci_TURRET_CAM_BS_LOADED_INTO_CAM)
				SET_TURRET_CAM_STATE(TCS_UPDATE)
				
				// We only want to push/pop timecycle if there is one active when we start up the cam...
				IF GET_TIMECYCLE_MODIFIER_INDEX() >= 0
					CDEBUG2LN(DEBUG_NET_TURRET, "[TURRET_CAM] TIMECYCLE ID pushing ", GET_TIMECYCLE_MODIFIER_INDEX())
					m_script.bPopTimecycle = TRUE
					PUSH_TIMECYCLE_MODIFIER() 
				ENDIF
				// This could be set up as a config option if necessary
				SET_TIMECYCLE_MODIFIER("eyeinthesky")
				m_script.iTimecycleModId = GET_TIMECYCLE_MODIFIER_INDEX()
				CDEBUG2LN(DEBUG_NET_TURRET, "[TURRET_CAM] TIMECYCLE ID = ", m_script.iTimecycleModId)
				
				IF IS_BIT_SET(m_settings.cfg.load.iBs, ci_TURRET_CAM_LOAD_BS_FADEIN)
					IF IS_SCREEN_FADING_OUT()
					OR IS_SCREEN_FADED_OUT()
						DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			CDEBUG3LN(DEBUG_NET_TURRET, ") - MAINTAIN_VEHICLE_WEAPON_CAMERA_CREATION - waiting on load scene")
		ENDIF
	ENDIF	
ENDPROC

PROC RUN_MAIN_CLIENT_LOGIC()
	DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	SET_CINEMATIC_BUTTON_ACTIVE(FALSE)
	
	SWITCH m_script.eState
		CASE TCS_LOAD	MAINTAIN_LOAD()		BREAK
		CASE TCS_UPDATE	MAINTAIN_UPDATE()	BREAK
	ENDSWITCH	
ENDPROC

PROC INITIALISE(TURRET_CAM_ARGS& ref_args)
	m_settings.vTurretCoords = ref_args.vPos
	m_settings.vTurretRotation = ref_args.vRot
	m_settings.eAttachType = ref_args.eAttachType
	m_settings.entParent = ref_args.entParent

	CDEBUG3LN(DEBUG_NET_TURRET, ") - INITIALISE - vTurretCoords   ", m_settings.vTurretCoords)
	CDEBUG3LN(DEBUG_NET_TURRET, ") - INITIALISE - vTurretRotation ", m_settings.vTurretRotation)
	CDEBUG3LN(DEBUG_NET_TURRET, ") - INITIALISE - eAttachType     ", m_settings.eAttachType)
	CDEBUG3LN(DEBUG_NET_TURRET, ") - INITIALISE - entParent       ", NATIVE_TO_INT(m_settings.entParent))
	
	m_script.homingMissile.entTargetAttempt = INT_TO_NATIVE(ENTITY_INDEX, -1)
	
	PED_INDEX pedLocal = PLAYER_PED_ID()
	IF IS_ENTITY_ALIVE(pedLocal)
	AND IS_PED_IN_ANY_VEHICLE(pedLocal, TRUE)
		VEHICLE_INDEX veh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		IF NETWORK_HAS_CONTROL_OF_ENTITY(veh)
			STORE_RADIO_STATION_FOR_RESPAWN(TRUE)
			SET_VEHICLE_RADIO_ENABLED(veh, FALSE)
		ENDIF
	ENDIF
	
	IF NOT IS_INTERACTION_MENU_DISABLED()
		SET_BIT(m_script.iBsLocal, ci_TURRET_CAM_LOCAL_BS_DISABLED_PIM)
		DISABLE_INTERACTION_MENU()
	ENDIF
	
	CONFIG_INIT()

	#IF IS_DEBUG_BUILD
	CREATE_WIDGETS()
	#ENDIF
ENDPROC

PROC SCRIPT_CLEANUP()		
	CDEBUG3LN(DEBUG_NET_TURRET, "SCRIPT_CLEANUP")
	
	// If we were in the middle of loading and were able to fade out - fade in.
	IF m_script.eState = TCS_LOAD
	AND IS_BIT_SET(m_settings.cfg.load.iBs, ci_TURRET_CAM_LOAD_BS_FADEOUT)
		CDEBUG3LN(DEBUG_NET_TURRET, "   Fading in")
		DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME_SHORT)
	ENDIF
	
	CONFIG_CLEANUP(ALLBITS)

	// Minimap
	//
	IF IS_RADAR_MAP_DISABLED()
		DISABLE_RADAR_MAP(FALSE)
	ENDIF

	SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
	
	UNLOCK_MINIMAP_ANGLE() 
	UNLOCK_MINIMAP_POSITION()

	PED_INDEX pedLocal = PLAYER_PED_ID()
	IF IS_ENTITY_ALIVE(pedLocal)
	AND IS_PED_IN_ANY_VEHICLE(pedLocal, TRUE)
		VEHICLE_INDEX veh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		IF NETWORK_HAS_CONTROL_OF_ENTITY(veh)
			SET_VEHICLE_RADIO_ENABLED(veh, TRUE)
			RESTORE_RADIO_FOR_SPAWNED_VEHICLE(TRUE)
		ENDIF
	ENDIF


	// Only enable the interaction menu if this script disabled it...
	IF IS_BIT_SET(m_script.iBsLocal, ci_TURRET_CAM_LOCAL_BS_DISABLED_PIM)
		ENABLE_INTERACTION_MENU()
	ENDIF
	
	REMOVE_SCRIPT_FIRE_POSITION()

	CLEAR_TIMECYCLE_MODIFIER()
	IF m_script.bPopTimecycle
		POP_TIMECYCLE_MODIFIER()
		CDEBUG2LN(DEBUG_NET_TURRET, "[TURRET_CAM] TIMECYCLE ID popping ", GET_TIMECYCLE_MODIFIER_INDEX())
	ENDIF
	
	REMOVE_BLIP_IF_EXIST(m_script.blipForeground)
	REMOVE_BLIP_IF_EXIST(m_script.blipBackground)
	
	g_iTurretScriptLaunchUid = TLRI_NONE
	g_iBsTurretCam = 0

	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()	
ENDPROC
   
FUNC BOOL SHOULD_KILL_THIS_SCRIPT()
	IF m_settings.eAttachType <> TCAT_NONE
		IF NOT DOES_ENTITY_EXIST(m_settings.entParent)
		OR IS_ENTITY_DEAD(m_settings.entParent)
			CDEBUG1LN(DEBUG_NET_TURRET, "[TURRET_CAM]SHOULD_KILL_THIS_SCRIPT - Turret parent dead : killing script.")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
		CDEBUG1LN(DEBUG_NET_TURRET, "[TURRET_CAM]SHOULD_KILL_THIS_SCRIPT - Player not Ok.")
		RETURN TRUE				
	ENDIF
	
	// NOTE: 	DO NOT SET THIS GLOBAL DIRECTLY. 
	//			Use TURRET_MANAGER_UNLOCK() or KILL_TURRET_CAM_SCRIPT()
	IF g_bKillTurretScript
		CDEBUG1LN(DEBUG_NET_TURRET, "[TURRET_CAM]SHOULD_KILL_THIS_SCRIPT - g_bKillTurretScript = TRUE")
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

PROC SCRIPT_INITIALISE(TURRET_CAM_ARGS& ref_args)
	CDEBUG1LN(DEBUG_NET_TURRET, "[TURRET_CAM]) - SCRIPT_INITIALISE - Launching instance: ", -1)
	
	SET_BIT(g_iBsTurretCam, ci_TURRET_CAM_BS_STARTED_SCRIPT)
	IF NETWORK_IS_GAME_IN_PROGRESS()
		NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE, -1)
		// This makes sure the net script is active, waits untill it is.
		HANDLE_NET_SCRIPT_INITIALISATION()
		CDEBUG3LN(DEBUG_NET_TURRET, ") - INITIALISED")
	ELSE
		CDEBUG3LN(DEBUG_NET_TURRET, ") - NETWORK_IS_GAME_IN_PROGRESS = FALSE - SCRIPT CLEANUP")
		SCRIPT_CLEANUP()
	ENDIF
	
	INITIALISE(ref_args)
ENDPROC

SCRIPT(TURRET_CAM_ARGS args) 
	IF NETWORK_IS_GAME_IN_PROGRESS() 
		SCRIPT_INITIALISE(args)	
	ELSE
		SCRIPT_CLEANUP()
	ENDIF
	
	WHILE TRUE	
		
		MP_LOOP_WAIT_ZERO()
		
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
		
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			CDEBUG1LN(DEBUG_NET_TURRET, "[TURRET_CAM]) - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE = TRUE")
			SCRIPT_CLEANUP()
		ENDIF
		
		IF SHOULD_KILL_THIS_SCRIPT()
			CDEBUG1LN(DEBUG_NET_TURRET, "[TURRET_CAM]) - SHOULD_KILL_THIS_SCRIPT = TRUE")
			SCRIPT_CLEANUP()
		ENDIF
		
		#IF IS_DEBUG_BUILD
		MAINTAIN_WIDGETS()
		#ENDIF
		
		RUN_MAIN_CLIENT_LOGIC()
	ENDWHILE
	
ENDSCRIPT
