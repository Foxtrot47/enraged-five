USING "commands_debug.sch"
USING "script_maths.sch"
USING "rage_builtins.sch"
USING "cutscene_help.sch"

ENUM CAR_MEET_EXTERIOR_SEATS
	 CAR_MEET_EXTERIOR_SEAT_0
	,CAR_MEET_EXTERIOR_SEAT_1
	,CAR_MEET_EXTERIOR_SEAT_2
	,CAR_MEET_EXTERIOR_SEAT_3
	,CAR_MEET_EXTERIOR_SEAT_4
	,CAR_MEET_EXTERIOR_SEAT_5
	,CAR_MEET_EXTERIOR_SEAT_6
	,CAR_MEET_EXTERIOR_SEAT_7
	,CAR_MEET_EXTERIOR_SEAT_8
	,CAR_MEET_EXTERIOR_SEAT_9
	,CAR_MEET_EXTERIOR_SEAT_10
	,CAR_MEET_EXTERIOR_SEAT_11
	,CAR_MEET_EXTERIOR_SEAT_12
	,CAR_MEET_EXTERIOR_SEAT_13
	,CAR_MEET_EXTERIOR_SEAT_14
	,CAR_MEET_EXTERIOR_SEAT_COUNT
ENDENUM

ENUM CAR_MEET_INTERIOR_SEATS
	CAR_MEET_INTERIOR_SEAT_0
	,CAR_MEET_INTERIOR_SEAT_1
	,CAR_MEET_INTERIOR_SEAT_COUNT
ENDENUM

TWEAK_FLOAT cfCASINO_EXTERIOR_SEAT_LOCATE_WIDTH		1.25

PROC GET_CAR_MEET_EXTERIOR_SEAT_WORLD_TRANSFORM(CAR_MEET_EXTERIOR_SEATS eSeat, VECTOR& vPos, FLOAT& fZRot)
	SWITCH eSeat
		// Area 1 (most northern seating area)
		CASE CAR_MEET_EXTERIOR_SEAT_0		vPos = << 752.663, -1783.328, 48.3 >>	fZRot = -37.800		BREAK	// Clubhouse	// Camp chair
		CASE CAR_MEET_EXTERIOR_SEAT_1		vPos = << 750.850, -1782.746, 48.3 >>	fZRot = 11.880 		BREAK	// Clubhouse	// Sofa right side
		CASE CAR_MEET_EXTERIOR_SEAT_2		vPos = << 750.000, -1786.550, 48.3 >>	fZRot = 149.040 	BREAK	// Clubhouse	// Camp chair
		
		// Area 2 (second most north)
		CASE CAR_MEET_EXTERIOR_SEAT_3		vPos = << 778.800, -1831.568, 48.3 >>	fZRot = -172.440 	BREAK	// Clubhouse	// Sofa right side
		CASE CAR_MEET_EXTERIOR_SEAT_4		vPos = << 780.289, -1827.899, 48.3 >>	fZRot = -30.240 	BREAK	// Office		// Camp chair
		CASE CAR_MEET_EXTERIOR_SEAT_5		vPos = << 778.538, -1827.125, 48.3 >>	fZRot = 0.0 		BREAK	// Clubhouse	// Camp chair
		CASE CAR_MEET_EXTERIOR_SEAT_6		vPos = << 777.238, -1830.767, 48.3 >>	fZRot = 142.200 	BREAK	// Clubhouse	// Camp chair
		
		// Area 3
		CASE CAR_MEET_EXTERIOR_SEAT_7		vPos = << 768.375, -1863.114, 48.3 >>	fZRot = 9.0 		BREAK	// Clubhouse	// Camp chair
		CASE CAR_MEET_EXTERIOR_SEAT_8		vPos = << 769.575, -1864.481, 48.3 >>	fZRot = -90.0 		BREAK	// Clubhouse	// Camp chair	
		CASE CAR_MEET_EXTERIOR_SEAT_9		vPos = << 768.863, -1866.382, 48.3 >>	fZRot = -145.0 		BREAK	// Office		// Camp chair
		
		// Western elevated area
		CASE CAR_MEET_EXTERIOR_SEAT_10		vPos = << 747.361, -1769.411, 52.311 >>	fZRot = -163.440	BREAK	// Clubhouse	// Camp chair
		CASE CAR_MEET_EXTERIOR_SEAT_11		vPos = << 748.837, -1767.993, 52.311 >>	fZRot = -128.160	BREAK	// Office		// Camp chair
		CASE CAR_MEET_EXTERIOR_SEAT_12		vPos = << 748.061, -1766.240, 52.311 >>	fZRot = -9.720		BREAK	// Office		// Camp chair
		
		// Eastern elevated area
		CASE CAR_MEET_EXTERIOR_SEAT_13		vPos = << 765.861, -1776.777, 52.311 >>	fZRot = -151.560	BREAK	// Office		// Camp chair
		CASE CAR_MEET_EXTERIOR_SEAT_14		vPos = << 764.161, -1777.399, 52.311 >>	fZRot = 159.840		BREAK	// Clubhouse	// Camp chair
	ENDSWITCH
ENDPROC

PROC GET_CAR_MEET_INTERIOR_SEAT_WORLD_TRANSFORM(CAR_MEET_INTERIOR_SEATS eSeat, VECTOR& vPos, FLOAT& fZRot)
	SWITCH eSeat
		CASE CAR_MEET_INTERIOR_SEAT_0		vPos = <<-2145.383, 1156.800, -25.372>>	fZRot = -75.0		BREAK	// Office		// Camp chair
		CASE CAR_MEET_INTERIOR_SEAT_1		vPos = <<-2145.434, 1155.453, -25.372>>	fZRot = -100.0 		BREAK	// Office		// Camp chair
	ENDSWITCH
ENDPROC

FUNC VECTOR GET_CAR_MEET_EXTERIOR_SEAT_ENTRY_LOCATE_OFFSET_0()
	RETURN <<-0.5, -0.5, 0.0>>
ENDFUNC

FUNC VECTOR GET_CAR_MEET_EXTERIOR_SEAT_ENTRY_LOCATE_OFFSET_1()
	RETURN <<0.5, -0.5, 2.0>>
ENDFUNC

FUNC FLOAT GET_CAR_MEET_EXTERIOR_SEAT_WIDTH()
	RETURN cfCASINO_EXTERIOR_SEAT_LOCATE_WIDTH
ENDFUNC

FUNC FLOAT GET_CAR_MEET_EXTERIOR_SEAT_ROTATION_OFFSET(SEAT_TYPE eType, FLOAT fRotation)
	SWITCH eType
		CASE SEAT_TYPE_OFFICE_ANIM
		CASE SEAT_TYPE_CLUBHOUSE_ANIM
			RETURN fRotation
		CASE SEAT_TYPE_BASE_ANIM
			RETURN fRotation - 180.0
	ENDSWITCH
	
	RETURN fRotation
ENDFUNC

PROC SETUP_CAR_MEET_EXTERIOR_SEAT(CAR_MEET_EXTERIOR_SEATS eSeat, SEATS_LOCAL_DATA& data, INT iSeatIndex, SEAT_TYPE eType)
	VECTOR vSeatPos
	FLOAT fSeatZRot
	
	GET_CAR_MEET_EXTERIOR_SEAT_WORLD_TRANSFORM(eSeat, vSeatPos, fSeatZRot)
	
	data.public.seats[iSeatIndex].eType = eType
	data.public.seats[iSeatIndex].animation.vPos = vSeatPos
	data.public.seats[iSeatIndex].animation.vRot = <<0.0, 0.0, fSeatZRot>>
	data.public.seats[iSeatIndex].entryLocate.v0 = ROTATE_VECTOR_ABOUT_Z(GET_CAR_MEET_EXTERIOR_SEAT_ENTRY_LOCATE_OFFSET_0(), GET_CAR_MEET_EXTERIOR_SEAT_ROTATION_OFFSET(eType, fSeatZRot)) + vSeatPos
	data.public.seats[iSeatIndex].entryLocate.v1 = ROTATE_VECTOR_ABOUT_Z(GET_CAR_MEET_EXTERIOR_SEAT_ENTRY_LOCATE_OFFSET_1(), GET_CAR_MEET_EXTERIOR_SEAT_ROTATION_OFFSET(eType, fSeatZRot)) + vSeatPos
	data.public.seats[iSeatIndex].entryLocate.fWidth = GET_CAR_MEET_EXTERIOR_SEAT_WIDTH()
ENDPROC

PROC SETUP_CAR_MEET_INTERIOR_SEAT(CAR_MEET_INTERIOR_SEATS eSeat, SEATS_LOCAL_DATA& data, INT iSeatIndex, SEAT_TYPE eType)
	FLOAT fSeatZRot
	
	VECTOR vSeatPos
	
	GET_CAR_MEET_INTERIOR_SEAT_WORLD_TRANSFORM(eSeat, vSeatPos, fSeatZRot)
	
	data.public.seats[iSeatIndex].eType = eType
	data.public.seats[iSeatIndex].animation.vPos = vSeatPos
	data.public.seats[iSeatIndex].animation.vRot = <<0.0, 0.0, fSeatZRot>>
	data.public.seats[iSeatIndex].entryLocate.v0 = ROTATE_VECTOR_ABOUT_Z(GET_CAR_MEET_EXTERIOR_SEAT_ENTRY_LOCATE_OFFSET_0(), GET_CAR_MEET_EXTERIOR_SEAT_ROTATION_OFFSET(eType, fSeatZRot)) + vSeatPos
	data.public.seats[iSeatIndex].entryLocate.v1 = ROTATE_VECTOR_ABOUT_Z(GET_CAR_MEET_EXTERIOR_SEAT_ENTRY_LOCATE_OFFSET_1(), GET_CAR_MEET_EXTERIOR_SEAT_ROTATION_OFFSET(eType, fSeatZRot)) + vSeatPos
	data.public.seats[iSeatIndex].entryLocate.fWidth = GET_CAR_MEET_EXTERIOR_SEAT_WIDTH()
ENDPROC
