USING "globals.sch"
USING "rage_builtins.sch"
USING "robberies/fm_content_tuner_robbery_core.sch"

CONST_INT WAREDEF_PED_GROUP_CROOKS				0
CONST_INT WAREDEF_PED_GROUP_COPS				1

CONST_INT WAREDEF_TICKER_COLLECTED_ANTI			0
CONST_INT WAREDEF_TICKER_COLLECTED_INSURGENT	1
CONST_INT WAREDEF_TICKER_COLLECTED_CRATES		2
CONST_INT WAREDEF_TICKER_DELIVERED_ANTI			3
CONST_INT WAREDEF_TICKER_DELIVERED_INSURGENT	4
CONST_INT WAREDEF_TICKER_DELIVERED_CRATES		5

CONST_INT MISSION_ENTITY_ANTI					0
CONST_INT MISSION_ENTITY_CRATES					1
CONST_INT MISSION_ENTITY_INSURGENT				2

CONST_INT WAREDEF_ATTACH_CRATE_1				0
CONST_INT WAREDEF_ATTACH_CRATE_2				1
CONST_INT WAREDEF_ATTACH_CRATE_3				2

CONST_INT WAREDEF_PED_BEHAVIOUR_DEFAULT			0
CONST_INT WAREDEF_PED_BEHAVIOUR_FLATBED			1

CONST_INT WAREDEF_HELP_TEXT_START				0
CONST_INT WAREDEF_HELP_TEXT_DELIVER_FIRST		1
CONST_INT WAREDEF_HELP_TEXT_RANGE_AA			2

CONST_INT WAREDEF_TEXT_MESSAGE_ONE_DELIVERED	0

//////////////////////
/// LOGIC OVERRIDES
//////////////////////

ENUM WAREDEF_MODE_STATE
	eWAREDEFSTATE_COLLECT = 0,
	eWAREDEFSTATE_DELIVER = 1,
	eWAREDEFSTATE_END = 2
ENDENUM

ENUM WAREDEF_CLIENT_MODE_STATE
	eWAREDEFSTATECLIENT_COLLECT = 0,
	eWAREDEFSTATECLIENT_DELIVER = 1,
	eWAREDEFSTATECLIENT_HELP_DELIVER = 2,
	eWAREDEFSTATECLIENT_RECOVER= 3,
	eWAREDEFSTATECLIENT_END = 4
ENDENUM

ENUM WAREDEF_PED_TASK
	eWAREDEFPEDTASK_IDLE = 0,
	eWAREDEFPEDTASK_SCENARIOS = 1,
	eWAREDEFPEDTASK_PATROL = 2,
	eWAREDEFPEDTASK_ATTACK = 3
ENDENUM

ENUM WAREDEF_FLATBED_PED_TASK
	eWAREDEFFLATBEDPEDTASK_IDLE = 0,
	eWAREDEFFLATBEDPEDTASK_FOLLOW_ROUTE = 1,
	eWAREDEFFLATBEDPEDTASK_WANDER = 2,
	eWAREDEFFLATBEDPEDTASK_FLEE = 3,
	eWAREDEFFLATBEDPEDTASK_ATTACK = 4
ENDENUM

ENUM WAREDEF_TAGS
	WAREDEF_TAGS_WASTELANDER = 0,
	WAREDEF_TAGS_FLATBED = 1,
	WAREDEF_TAGS_AA_TRAILER = 2,
	WAREDEF_TAGS_FLATBED_DRIVER = 3,
	WAREDEF_TAGS_FLATBED_PASSENGER = 4,
	WAREDEF_TAGS_FLATBED_CRATE_ONE = 5,
	WAREDEF_TAGS_FLATBED_CRATE_TWO = 6,
	WAREDEF_TAGS_FLATBED_CRATE_THREE = 7,
	WAREDEF_TAGS_INSURGENT = 8
ENDENUM

ENUM WAREDEF_DIALOGUE
	eWAREDEFDIALOGUE_OPENING,
	eWAREDEFDIALOGUE_TIME_RUNNING_OUT
ENDENUM

ENUM WAREDEF_MUSIC
	eWAREDEFMUSIC_INIT = -1,
	eWAREDEFMUSIC_START,
	eWAREDEFMUSIC_GUNFIGHT,
	eWAREDEFMUSIC_DELIVER
ENDENUM

FUNC WAREDEF_MODE_STATE GET_WAREDEF_MODE_STATE()
	RETURN INT_TO_ENUM(WAREDEF_MODE_STATE, GET_MODE_STATE_ID())
ENDFUNC

FUNC WAREDEF_CLIENT_MODE_STATE GET_WAREDEF_CLIENT_MODE_STATE()
	RETURN INT_TO_ENUM(WAREDEF_CLIENT_MODE_STATE, GET_CLIENT_MODE_STATE_ID())
ENDFUNC

FUNC BOOL WAREDEF_SHOULD_ACTIVATE_AMBUSH()	

	IF HAS_ANY_PED_GROUP_BEEN_TRIGGERED()
		RETURN TRUE
	ENDIF
	
	RETURN HAS_ANY_MISSION_ENTITY_BEEN_COLLECTED_FOR_THE_FIRST_TIME()
ENDFUNC

FUNC INT WAREDEF_DELIVERED_COUNT()
	RETURN GET_NUMBER_OF_DELIVERED_MISSION_ENTITIES_BY_PLAYER_GANG(LOCAL_PLAYER_INDEX)
ENDFUNC

FUNC FLOAT WAREDEF_AMBUSH_DISMISS_RANGE()	
	
	IF WAREDEF_DELIVERED_COUNT() >= ( GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() -1 )
		RETURN 300.00
	ENDIF

	RETURN 100.00
ENDFUNC

FUNC FLOAT WAREDEF_AMBUSH_CLEANUP_RANGE()	
	RETURN 450.00
ENDFUNC

FUNC FLOAT WAREDEF_AMBUSH_RANGE()	
	RETURN 150.00
ENDFUNC

FUNC eENTITY_TYPE WAREDEF_GET_AMBUSH_TARGET_TYPE()
	RETURN eENTITYTYPE_PLAYER
ENDFUNC

FUNC BOOL WAREDEF_AMBUSH_PRIORITY_TARGET(INT iTarget, ENTITY_INDEX targetId)
	UNUSED_PARAMETER(targetId)
	
	RETURN IS_PLAYER_IN_POSSESSION_OF_ANY_MISSION_ENTITY(INT_TO_NATIVE(PLAYER_INDEX, iTarget))
ENDFUNC

FUNC BOOL WAREDEF_PLAYER_IN_CLUBHOUSE()
	RETURN GET_MISSION_INTERIOR_PLAYER_IS_IN() = eFMCINTERIOR_BIKER_CLUBHOUSE
ENDFUNC

FUNC NETWORK_INDEX WAREDEF_AA_NET_ID()
	
	RETURN serverBD.sVehicle[GET_TAG_ID(WAREDEF_TAGS_AA_TRAILER)].netId
ENDFUNC

FUNC VEHICLE_INDEX WAREDEF_AA_VEH_ID()

	VEHICLE_INDEX vehID
	NETWORK_INDEX netID = WAREDEF_AA_NET_ID()

	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(netID)
		RETURN NET_TO_VEH(netID)
	ENDIF

	RETURN vehID

ENDFUNC 

///////////////////////
///    OBJECTIVE TEXT
///////////////////////

FUNC STRING WAREDEF_GET_ITEM_LABEL_FOR_OBJECTIVE_TEXT(BOOL bPlural = FALSE)

	IF bPlural
		RETURN "ROB_ITM_ARMS"
	ENDIF

	RETURN "ROB_ITM_ARM"
ENDFUNC

PROC WAREDEF_OBJECTIVE_TEXT_COLLECT()

	INT iNumDelivered = WAREDEF_DELIVERED_COUNT()
	BOOL bPlural = ( iNumDelivered < ( GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() -1 ) )
	STRING sParamMainText = "TR_STEAL_TMRY"
	
	IF ( iNumDelivered >= ( GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() -1 ) AND IS_ANY_MISSION_ENTITY_HELD() )
	OR iNumDelivered = GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION()
	
		CLEAR_OBJECTIVE()
	ELSE

		Print_Objective_Text_With_Coloured_Text_Label(sParamMainText, WAREDEF_GET_ITEM_LABEL_FOR_OBJECTIVE_TEXT(bPlural), HUD_COLOUR_BLUE)	
	ENDIF
ENDPROC

PROC WAREDEF_HELP_DELIVER_OBJECTIVE_TEXT()
	Print_Objective_Text_With_String("ROB_OT_WD_HDEL", WAREDEF_GET_ITEM_LABEL_FOR_OBJECTIVE_TEXT(), DEFAULT, HUD_COLOUR_WHITE)	
ENDPROC

PROC WAREDEF_OBJECTIVE_TEXT_DELIVER()
	Print_Objective_Text_With_String("ROB_OT_WD_DEL", WAREDEF_GET_ITEM_LABEL_FOR_OBJECTIVE_TEXT(), DEFAULT, HUD_COLOUR_WHITE)
ENDPROC

PROC WAREDEF_OBJECTIVE_TEXT_HELP_DELIVER()
	IF NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(LOCAL_PLAYER_INDEX)
		WAREDEF_HELP_DELIVER_OBJECTIVE_TEXT()
	ELSE
		CLEAR_OBJECTIVE()
	ENDIF
ENDPROC

PROC WAREDEF_OBJECTIVE_TEXT_RECOVER()
	Print_Objective_Text_With_Coloured_Text_Label("BBOT_RECOVER", WAREDEF_GET_ITEM_LABEL_FOR_OBJECTIVE_TEXT(), HUD_COLOUR_RED)
ENDPROC

FUNC STRING WAREDEF_TICKER_LOCAL(MISSION_TICKER_DATA &sTickerData)

	SWITCH sTickerData.iInt1
		CASE WAREDEF_TICKER_COLLECTED_ANTI
			RETURN "ROB_TIC_WD5"
		BREAK
		CASE WAREDEF_TICKER_COLLECTED_INSURGENT
			RETURN "ROB_TIC_WD7"
		BREAK
		CASE WAREDEF_TICKER_COLLECTED_CRATES
			RETURN "ROB_TIC_WD9"
		BREAK
		CASE WAREDEF_TICKER_DELIVERED_ANTI
			RETURN "ROB_TIC_WD12"
		BREAK
		CASE WAREDEF_TICKER_DELIVERED_INSURGENT
			RETURN "ROB_TIC_WD1"
		BREAK
		CASE WAREDEF_TICKER_DELIVERED_CRATES
			RETURN "ROB_TIC_WD3"
		BREAK
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC STRING WAREDEF_TICKER_REMOTE(MISSION_TICKER_DATA &sTickerData)
	SWITCH sTickerData.iInt1
		CASE WAREDEF_TICKER_COLLECTED_ANTI
			RETURN "ROB_TIC_WD6"
		BREAK
		CASE WAREDEF_TICKER_COLLECTED_INSURGENT
			RETURN "ROB_TIC_WD8"
		BREAK
		CASE WAREDEF_TICKER_COLLECTED_CRATES
			RETURN "ROB_TIC_WD10"
		BREAK
		CASE WAREDEF_TICKER_DELIVERED_ANTI
			RETURN "ROB_TIC_WD11"
		BREAK
		CASE WAREDEF_TICKER_DELIVERED_INSURGENT
			RETURN "ROB_TIC_WD2"
		BREAK
		CASE WAREDEF_TICKER_DELIVERED_CRATES
			RETURN "ROB_TIC_WD4"
		BREAK
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC STRING WAREDEF_HELP_TEXT_STRING(INT iHelp) 

	SWITCH iHelp
		CASE WAREDEF_HELP_TEXT_START			RETURN "TR_HT_WD1"
		CASE WAREDEF_HELP_TEXT_DELIVER_FIRST	RETURN "TR_HT_WD2"
		CASE WAREDEF_HELP_TEXT_RANGE_AA			RETURN "TR_HT_WD4"
	ENDSWITCH
	
	RETURN ""

ENDFUNC

FUNC BOOL WAREDEF_HELP_TEXT_TRIGGER(INT iHelp)

	SWITCH iHelp
		CASE WAREDEF_HELP_TEXT_START		

			RETURN TRUE
			
		CASE WAREDEF_HELP_TEXT_DELIVER_FIRST	
		
			RETURN WAREDEF_DELIVERED_COUNT() > 0
			
		CASE WAREDEF_HELP_TEXT_RANGE_AA
		
			RETURN !IS_VEHICLE_BIT_SET(GET_TAG_ID(WAREDEF_TAGS_AA_TRAILER), eVEHICLEBITSET_ATTACHED_TO_TRAILER) AND ( VDIST2(DATA.MissionEntity.MissionEntities[MISSION_ENTITY_ANTI].vCoords, LOCAL_PLAYER_COORD) <= 10.00 * 10.00 ) 
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL WAREDEF_RESET_TEXT_TIMER()

	IF HAS_NET_TIMER_STARTED(sLocalVariables.stMissionTimer)
		RESET_NET_TIMER(sLocalVariables.stMissionTimer)
	ENDIF

	RETURN TRUE
ENDFUNC

FUNC BOOL WAREDEF_GET_TEXT_MESSAGE_SHOULD_BE_SENT(INT iTextMessage)
	SWITCH iTextMessage
		CASE WAREDEF_TEXT_MESSAGE_ONE_DELIVERED			RETURN WAREDEF_DELIVERED_COUNT() > 0 AND NOT IS_PLAYER_WALKING_OUT_OF_SIMPLE_INTERIOR(LOCAL_PLAYER_INDEX) AND HAS_NET_TIMER_EXPIRED(sLocalVariables.stMissionTimer, 5000) AND WAREDEF_RESET_TEXT_TIMER()
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC enumCharacterList WAREDEF_GET_TEXT_MESSAGE_CHARACTER(INT iTextMessage)
	SWITCH iTextMessage
		CASE WAREDEF_TEXT_MESSAGE_ONE_DELIVERED	
			RETURN CHAR_SESSANTA
	ENDSWITCH
	
	RETURN CHAR_BLOCKED
ENDFUNC

FUNC STRING WAREDEF_GET_TEXT_MESSAGE_LABEL(INT iTextMessage)

	SWITCH iTextMessage
		CASE WAREDEF_TEXT_MESSAGE_ONE_DELIVERED	
		
			IF GET_SEEDED_INT_IN_RANGE(2) = 0
				RETURN "TR_TXT_WD1b"
			ENDIF
		
			RETURN "TR_TXT_WD1a"
			
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC BOOL WAREDEF_SHOULD_ENABLE_BOTTOM_RIGHT_HUD(INT iHUD)
	UNUSED_PARAMETER(iHUD)
	
	RETURN TRUE
ENDFUNC

FUNC INT WAREDEF_GET_BOTTOM_RIGHT_HUD_VALUE(INT iHUD)
	UNUSED_PARAMETER(iHUD)
	
	RETURN WAREDEF_DELIVERED_COUNT()
ENDFUNC

FUNC STRING WAREDEF_GET_DIALOGUE_ROOT(INT iDialogue)

	SWITCH INT_TO_ENUM(WAREDEF_DIALOGUE, iDialogue)
		CASE eWAREDEFDIALOGUE_OPENING
			SWITCH GET_SUBVARIATION()
				CASE TRS_WD_LOCATION_1
					RETURN "TNRP_KW_1A"
				CASE TRS_WD_LOCATION_2
					RETURN "TNRP_KW_1B"
			ENDSWITCH
		BREAK
		CASE eWAREDEFDIALOGUE_TIME_RUNNING_OUT
			RETURN "TNRP_GN_5A"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC STRING WAREDEF_GET_DIALOGUE_SUBTITLE_GROUP(INT iDialogue)
	UNUSED_PARAMETER(iDialogue)
	
	RETURN "TNRPAUD"
ENDFUNC

FUNC STRING WAREDEF_GET_DIALOGUE_CHARACTER(INT iDialogue, INT iSpeaker)
	UNUSED_PARAMETER(iDialogue)
	UNUSED_PARAMETER(iSpeaker)
	
	RETURN "TUN_SESSANTA"
ENDFUNC

FUNC enumCharacterList WAREDEF_GET_DIALOGUE_CELLPHONE_CHARACTER(INT iDialogue)
	UNUSED_PARAMETER(iDialogue)
	
	RETURN CHAR_SESSANTA
ENDFUNC

FUNC BOOL WAREDEF_SHOULD_DIALOGUE_USE_HEADSET_AUDIO(INT iDialogue)

	IF WAREDEF_GET_DIALOGUE_CELLPHONE_CHARACTER(iDialogue) = CHAR_KDJ
		RETURN FALSE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL WAREDEF_SHOULD_TRIGGER_DIALOGUE(INT iDialogue)
	SWITCH INT_TO_ENUM(WAREDEF_DIALOGUE, iDialogue)
		CASE eWAREDEFDIALOGUE_OPENING
			RETURN TRUE
		CASE eWAREDEFDIALOGUE_TIME_RUNNING_OUT
			RETURN IS_GENERIC_BIT_SET(eGENERICBITSET_NEAR_MISSION_END)
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC INT WAREDEF_DIALOGUE_DELAY(INT iDialogue)

	UNUSED_PARAMETER(iDialogue)

//	SWITCH INT_TO_ENUM(WAREDEF_DIALOGUE, iDialogue)
//		CASE eWAREDEFDIALOGUE_CLUB		RETURN 2000									
//	ENDSWITCH
	
	RETURN 0

ENDFUNC

FUNC INT WAREDEF_GET_NEXT_MUSIC_EVENT(INT iCurrentEvent)
	SWITCH INT_TO_ENUM(WAREDEF_MUSIC, iCurrentEvent)
		
		CASE eWAREDEFMUSIC_INIT
			RETURN ENUM_TO_INT(eWAREDEFMUSIC_START)
		
		CASE eWAREDEFMUSIC_START
			IF HAS_ANY_PED_GROUP_BEEN_TRIGGERED()
				RETURN ENUM_TO_INT(eWAREDEFMUSIC_GUNFIGHT)
			ENDIF
		BREAK
		
		CASE eWAREDEFMUSIC_GUNFIGHT
			IF GET_WAREDEF_CLIENT_MODE_STATE() >= eWAREDEFSTATECLIENT_DELIVER
				RETURN ENUM_TO_INT(eWAREDEFMUSIC_DELIVER)
			ENDIF
		BREAK
		
		CASE eWAREDEFMUSIC_DELIVER
		
		BREAK
	ENDSWITCH
	
	RETURN -1
ENDFUNC

FUNC STRING WAREDEF_GET_MUSIC_EVENT_STRING(INT iEvent)
	SWITCH INT_TO_ENUM(WAREDEF_MUSIC, iEvent)
		CASE eWAREDEFMUSIC_START
			RETURN "TUNER_DELIVERING_START"
		
		CASE eWAREDEFMUSIC_GUNFIGHT
			RETURN "TUNER_GUNFIGHT"
		
		CASE eWAREDEFMUSIC_DELIVER
			RETURN "TUNER_MED_INTENSITY"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC FM_CONTENT_PM_CALL_BITSET WAREDEF_POST_MISSION_CALL()

	SWITCH GET_SUBVARIATION()
		CASE TRS_WD_LOCATION_1	
			IF WAREDEF_DELIVERED_COUNT() = 0	
				RETURN eFM_CONTENT_PM_CALL_TUNER_WAREHOUSE_DEFENCES_FAIL
			ELIF WAREDEF_DELIVERED_COUNT() = data.MissionEntity.iCount
				RETURN eFM_CONTENT_PM_CALL_TUNER_WAREHOUSE_DEFENCES_PASS
			ENDIF
			
			RETURN eFM_CONTENT_PM_CALL_TUNER_WAREHOUSE_DEFENCES_PARTIAL 	
	ENDSWITCH

	IF WAREDEF_DELIVERED_COUNT() = 0	
		RETURN eFM_CONTENT_PM_CALL_TUNER_WAREHOUSE_DEFENCES_SUBVARIATION_1_FAIL
	ELIF WAREDEF_DELIVERED_COUNT() = data.MissionEntity.iCount
		RETURN eFM_CONTENT_PM_CALL_TUNER_WAREHOUSE_DEFENCES_SUBVARIATION_1_PASS
	ENDIF
	
	RETURN eFM_CONTENT_PM_CALL_TUNER_WAREHOUSE_DEFENCES_SUBVARIATION_1_PARTIAL
ENDFUNC

FUNC BOOL WAREDEF_SHOULD_START_MODE_TIMER()
	RETURN HAS_DIALOGUE_FINISHED_ENUM(eWAREDEFDIALOGUE_OPENING)
ENDFUNC

///////////////////////
///    PEDS
///////////////////////

FUNC INT WAREDEF_FLATBED_NUM_COORDS(INT iPed)

	UNUSED_PARAMETER(iPed)
	
	SWITCH GET_SUBVARIATION()
		CASE TRS_WD_LOCATION_1		RETURN 5
		CASE TRS_WD_LOCATION_2		RETURN 6	
	ENDSWITCH
	
	RETURN 1
ENDFUNC

FUNC VECTOR WAREDEF_FLATBED_GO_TO_COORDS(INT iPed, INT iCheckpointStage)

	UNUSED_PARAMETER(iPed)
	
	SWITCH GET_SUBVARIATION()
		CASE TRS_WD_LOCATION_1	
			SWITCH iCheckpointStage			
				CASE 0	RETURN <<7.3807, 265.2610, 108.3780>>
				CASE 1	RETURN <<-1307.4415, 227.7126, 57.7688>>
				CASE 2	RETURN <<-1977.0083, -163.4534, 30.2833>>
				CASE 3	RETURN <<-1607.2268, -749.5388, 10.4194>>
				CASE 4	RETURN <<196.8242, -527.7421, 33.0848>>
			ENDSWITCH
		BREAK
		CASE TRS_WD_LOCATION_2		
			SWITCH iCheckpointStage			
				CASE 0	RETURN <<-188.6555, 251.5753, 91.5098>>
				CASE 1	RETURN <<529.4993, -101.6781, 63.4310>>
				CASE 2	RETURN <<523.7943, -516.7352, 44.2659>>
				CASE 3	RETURN <<1016.8028, -946.8285, 29.3562>>
				CASE 4	RETURN <<1316.8658, -2407.3638, 49.8858>>
				CASE 5 	RETURN <<-31.8815, -2580.3125, 31.0762>>
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN <<765.6818, -3009.2024, 4.8005>>
ENDFUNC

FUNC BOOL WAREDEF_IS_PED_FLATBED(INT iPed)
	IF data.Ped.Peds[iPed].iVehicle != (-1)
	AND data.Ped.Peds[iPed].eSeat = VS_DRIVER 
	AND data.Ped.Peds[iPed].iVehicle = GET_TAG_ID(WAREDEF_TAGS_FLATBED)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL WAREDEF_SHOULD_PED_FOLLOW_ROUTE(INT iPed, PED_INDEX pedID)
	UNUSED_PARAMETER(pedId)
	UNUSED_PARAMETER(iPed)
	RETURN TRUE
ENDFUNC

FUNC BOOL WAREDEF_SHOULD_PED_ATTACK(INT iPed, PED_INDEX pedID)
	
	IF iPed = GET_TAG_ID(WAREDEF_TAGS_FLATBED_DRIVER)
	OR iPed = GET_TAG_ID(WAREDEF_TAGS_FLATBED_PASSENGER)
		RETURN !IS_PED_IN_ANY_VEHICLE(pedId)
	ENDIF
	
	RETURN HAS_PED_BEEN_TRIGGERED(iPed)
ENDFUNC

FUNC BOOL WAREDEF_IS_PED_A_PATROL_PED(INT iPed)
	INT i
	REPEAT data.Patrol.iCount i
		IF data.Patrol.Patrols[i].iPed = iPed
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

FUNC BOOL WAREDEF_SHOULD_PED_PATROL(INT iPed, PED_INDEX pedID)
	UNUSED_PARAMETER(pedId)	
	RETURN WAREDEF_IS_PED_A_PATROL_PED(iPed)
ENDFUNC

FUNC BOOL WAREDEF_SHOULD_PED_DO_SCENARIOS(INT iPed, PED_INDEX pedID)
	UNUSED_PARAMETER(pedId)
	UNUSED_PARAMETER(iPed)
	RETURN TRUE
ENDFUNC

FUNC BOOL WAREDEF_PED_TO_WANDER(INT iPed, PED_INDEX pedId)
	UNUSED_PARAMETER(pedId)
	
	RETURN IS_PED_BIT_SET(iPed, ePEDBITSET_I_HAVE_REACHED_FINAL_CHECKPOINT)
ENDFUNC

PROC WAREDEF_SETUP_PED_TRIGGERS(INT iPed)
	ADD_PED_TRIGGER(iPed, &PED_TRIGGER_HAS_PED_BEEN_TARGETTED)
	ADD_PED_TRIGGER(iPed, &PED_TRIGGER_HAS_PED_TRIGGER_AREA_BEEN_ACTIVATED)
	ADD_PED_TRIGGER(iPed, &PED_TRIGGER_HAS_PED_BEEN_DAMAGED)
	ADD_PED_TRIGGER(iPed, &PED_TRIGGER_HAS_PED_VEHICLE_BEEN_DAMAGED)
ENDPROC

PROC WAREDEF_SETUP_PED_TASKS(INT iBehaviour)

	SWITCH iBehaviour
		CASE WAREDEF_PED_BEHAVIOUR_DEFAULT						
			ADD_PED_TASK(iBehaviour, eWAREDEFPEDTASK_IDLE, ePEDTASK_IDLE)
				ADD_PED_TASK_TRANSITION(iBehaviour, eWAREDEFPEDTASK_IDLE, eWAREDEFPEDTASK_SCENARIOS, &WAREDEF_SHOULD_PED_DO_SCENARIOS)
				
			ADD_PED_TASK(iBehaviour, eWAREDEFPEDTASK_SCENARIOS, ePEDTASK_DO_SCENARIOS)
				ADD_PED_TASK_TRANSITION(iBehaviour, eWAREDEFPEDTASK_SCENARIOS, eWAREDEFPEDTASK_ATTACK, &WAREDEF_SHOULD_PED_ATTACK)
				ADD_PED_TASK_TRANSITION(iBehaviour, eWAREDEFPEDTASK_SCENARIOS, eWAREDEFPEDTASK_PATROL, &WAREDEF_SHOULD_PED_PATROL)
				
			ADD_PED_TASK(iBehaviour, eWAREDEFPEDTASK_PATROL, ePEDTASK_PATROL)
				ADD_PED_TASK_TRANSITION(iBehaviour, eWAREDEFPEDTASK_PATROL, eWAREDEFPEDTASK_ATTACK, &WAREDEF_SHOULD_PED_ATTACK)
				
			ADD_PED_TASK(iBehaviour, eWAREDEFPEDTASK_ATTACK, ePEDTASK_ATTACK_HATED_TARGETS)
		BREAK
		CASE WAREDEF_PED_BEHAVIOUR_FLATBED		
			ADD_PED_TASK(iBehaviour, eWAREDEFPEDTASK_IDLE, ePEDTASK_IDLE)
				ADD_PED_TASK_TRANSITION(iBehaviour, eWAREDEFPEDTASK_IDLE, eWAREDEFFLATBEDPEDTASK_FOLLOW_ROUTE, &WAREDEF_SHOULD_PED_FOLLOW_ROUTE)

			ADD_PED_TASK(iBehaviour, eWAREDEFFLATBEDPEDTASK_FOLLOW_ROUTE, ePEDTASK_GO_TO_COORD_ANY_MEANS)
				ADD_PED_TASK_TRANSITION(iBehaviour, eWAREDEFFLATBEDPEDTASK_FOLLOW_ROUTE, eWAREDEFFLATBEDPEDTASK_WANDER, &WAREDEF_PED_TO_WANDER)
				ADD_PED_TASK_TRANSITION(iBehaviour, eWAREDEFFLATBEDPEDTASK_FOLLOW_ROUTE, eWAREDEFFLATBEDPEDTASK_FLEE, &WAREDEF_SHOULD_PED_ATTACK)
			
			ADD_PED_TASK(iBehaviour, eWAREDEFFLATBEDPEDTASK_WANDER, ePEDTASK_FLEE_IN_VEHICLE)
				ADD_PED_TASK_TRANSITION(iBehaviour, eWAREDEFFLATBEDPEDTASK_WANDER, eWAREDEFFLATBEDPEDTASK_FLEE, &WAREDEF_SHOULD_PED_ATTACK)

			ADD_PED_TASK(iBehaviour, eWAREDEFFLATBEDPEDTASK_FLEE, ePEDTASK_FLEE_IN_VEHICLE)
			
		BREAK
	ENDSWITCH
	
ENDPROC

FUNC INT WAREDEF_GET_PED_BEHAVIOUR(INT iPed)

	IF WAREDEF_IS_PED_FLATBED(iPed)
		RETURN WAREDEF_PED_BEHAVIOUR_FLATBED
	ENDIF

	RETURN WAREDEF_PED_BEHAVIOUR_DEFAULT
ENDFUNC

///////////////////////
///    PROPS
///////////////////////

FUNC VECTOR WAREDEF_PROP_ROTATION(INT iProp)

	IF iProp = GET_TAG_ID(WAREDEF_TAGS_FLATBED_CRATE_ONE)
		RETURN <<0.0, 0.0, 0.0>>
	ENDIF
	
	IF iProp = GET_TAG_ID(WAREDEF_TAGS_FLATBED_CRATE_TWO)		
		RETURN <<0.0000, 0.0000, 0.0000>>
	ENDIF
	
	IF iProp = GET_TAG_ID(WAREDEF_TAGS_FLATBED_CRATE_THREE)			
		RETURN <<0.0000, 0.0000, 0.0000>>
	ENDIF

	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

PROC THERMAL_CLIENT_PROP_MAINTAIN(INT iProp, ENTITY_INDEX propId, BOOL bDead)

	IF iProp = GET_TAG_ID(WAREDEF_TAGS_FLATBED_CRATE_ONE)
	OR iProp = GET_TAG_ID(WAREDEF_TAGS_FLATBED_CRATE_TWO)		
	OR iProp = GET_TAG_ID(WAREDEF_TAGS_FLATBED_CRATE_THREE)			
		IF NOT bDead
			IF NOT IS_PROP_BIT_SET(iProp, ePROPBITSET_SET_COLLISION)
			AND NOT IS_PROP_CLIENT_BIT_SET(iProp, LOCAL_PARTICIPANT_INDEX, ePROPCLIENTBITSET_SET_COLLISION)
				IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sProp[iProp].netID)
					IF IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(propId)
						SET_ENTITY_COLLISION(propId, TRUE)
						SET_PROP_CLIENT_BIT(iProp, ePROPCLIENTBITSET_SET_COLLISION)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF	

ENDPROC


///////////////////////
///    Vehicles
///////////////////////

PROC WAREDEF_VEHICLE_SET_ATTRIBUTES(INT iVehicle, VEHICLE_INDEX vehId)
	UNUSED_PARAMETER(iVehicle)
	UNUSED_PARAMETER(vehId)
ENDPROC

FUNC BOOL WAREDEF_APPLY_VEHICLE_MODS(INT iVehicle, VEHICLE_SETUP_STRUCT_MP &sData)

	SWITCH data.Vehicle.Vehicles[iVehicle].model	
		CASE trailersmall2
			sData.VehicleSetup.eModel = trailersmall2
			sData.VehicleSetup.tlPlateText = "88NGJ146"
			sData.VehicleSetup.iColour1 = 16
			sData.VehicleSetup.iColour2 = 16
			sData.VehicleSetup.iColourExtra1 = 16
			sData.VehicleSetup.iColourExtra2 = 16
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_BULLETPROOF_TYRES)
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 9
		BREAK
		CASE INSURGENT3
			sData.VehicleSetup.eModel = INSURGENT3
			sData.VehicleSetup.tlPlateText = "03YBC659"
			sData.VehicleSetup.iColour1 = 16
			sData.VehicleSetup.iColour2 = 16
			sData.VehicleSetup.iColourExtra1 = 16
			sData.VehicleSetup.iColourExtra2 = 16
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 3
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_0)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_BULLETPROOF_TYRES)
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 2
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 9
		BREAK
		DEFAULT
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC FLOAT WAREDEF_GET_VEHICLE_NEAR_RANGE(INT iVehicle)
	IF iVehicle = GET_TAG_ID(WAREDEF_TAGS_AA_TRAILER)
		RETURN 50.00
	ENDIF
	
	RETURN 0.0
ENDFUNC

FUNC VECTOR WAREDEF_TRAILER_OFFSET(INT iVehicle)

	UNUSED_PARAMETER(iVehicle)
	
	RETURN <<0.0, -2.30, 1.29>>
ENDFUNC

FUNC VECTOR WAREDEF_TRAILER_ROT(INT iVehicle)

	UNUSED_PARAMETER(iVehicle)
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

PROC WAREDEF_VEHICLE_ON_CLEANUP(INT iVehicle)
	SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(NET_TO_VEH(serverBD.sVehicle[iVehicle].netId),TRUE)
ENDPROC

PROC WAREDEF_VEHICLE_CLIENT(INT iVehicle, VEHICLE_INDEX vehId, BOOL bDriveable, BOOL bInVehicle, INT iMissionEntityForCarrier)

	UNUSED_PARAMETER(bInVehicle)
	UNUSED_PARAMETER(iMissionEntityForCarrier)
	
	IF GET_END_REASON() != eENDREASON_NO_REASON_YET 
		IF IS_PED_IN_VEHICLE(LOCAL_PED_INDEX, vehId)
			IF GET_SCRIPT_TASK_STATUS(LOCAL_PED_INDEX, SCRIPT_TASK_LEAVE_VEHICLE) != PERFORMING_TASK
	        	IF GET_SCRIPT_TASK_STATUS(LOCAL_PED_INDEX, SCRIPT_TASK_LEAVE_VEHICLE) != WAITING_TO_START_TASK
					TASK_LEAVE_VEHICLE(LOCAL_PED_INDEX, vehId) // This will also park the vehicle.
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF iVehicle = GET_TAG_ID(WAREDEF_TAGS_INSURGENT)
	
		IF bDriveable
		
			IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
				IF NOT IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_COLLISION_TOGGLE)
				AND NOT IS_VEHICLE_CLIENT_BIT_SET(iVehicle, LOCAL_PARTICIPANT_INDEX, eVEHICLECLIENTBITSET_COLLISION_TOGGLE)
					IF IS_ENTITY_ATTACHED(vehId)
						SET_ENTITY_COLLISION(vehId, TRUE)
						SET_VEHICLE_CLIENT_BIT(iVehicle, eVEHICLECLIENTBITSET_COLLISION_TOGGLE)
					ENDIF
				ELSE
					IF IS_MISSION_ENTITY_BIT_SET(MISSION_ENTITY_INSURGENT, eMISSIONENTITYBITSET_DELIVERED)						
						
						IF IS_ENTITY_ATTACHED(vehId)
							PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [VEH_", iVehicle, "] WAREDEF_VEHICLE_CLIENT - Detaching veh from any entity.")
							DETACH_ENTITY(vehId)
						ENDIF
						
						PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [VEH_", iVehicle, "] WAREDEF_VEHICLE_CLIENT - Deleted veh.")
						DELETE_NET_ID(serverBD.sVehicle[iVehicle].netId)
					
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELIF IS_MISSION_ENTITY_BIT_SET(MISSION_ENTITY_ANTI, eMISSIONENTITYBITSET_DELIVERED)
		IF bDriveable
			IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
				IF GET_ENTITY_MODEL(vehId) = NIGHTSHARK
					IF NOT IS_ANY_PLAYER_IN_VEHICLE(vehID)
						IF VDIST(GET_ENTITY_COORDS(vehId, FALSE), GET_DROPOFF_COORDS()) < 10.00
							IF IS_VEHICLE_ATTACHED_TO_TRAILER(vehID)
								DETACH_VEHICLE_FROM_TRAILER(vehID)
								PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [VEH_", iVehicle, "] WAREDEF_VEHICLE_CLIENT - NIGHTSHARK, DETACH_VEHICLE_FROM_TRAILER ")
								EXIT
							ENDIF
							PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [VEH_", iVehicle, "] WAREDEF_VEHICLE_CLIENT - NIGHTSHARK, Cleaned up veh.")
//							DELETE_NET_ID(serverBD.sVehicle[iVehicle].netId)
							DELETE_VEHICLE(vehID)
						ENDIF
					ENDIF
				ENDIF
			ENDIF	
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL WAREDEF_VEHICLE_BLIP_ENABLE(INT iVehicle)

	IF iVehicle = GET_TAG_ID(WAREDEF_TAGS_AA_TRAILER)
	OR iVehicle = GET_TAG_ID(WAREDEF_TAGS_FLATBED)
	OR iVehicle = GET_TAG_ID(WAREDEF_TAGS_WASTELANDER)
		RETURN HAS_DIALOGUE_FINISHED_ENUM(eWAREDEFDIALOGUE_OPENING)
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC STRING WAREDEF_VEHICLE_BLIP_NAME(INT iVehicle)
	UNUSED_PARAMETER(iVehicle)
	RETURN "TR_BLIP_ARM"
ENDFUNC

FUNC BOOL WAREDEF_VEHICLE_BLIP_FLASH(INT iVehicle)

	IF iVehicle = GET_TAG_ID(WAREDEF_TAGS_AA_TRAILER)
		RETURN IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(WAREDEF_HELP_TEXT_STRING(WAREDEF_HELP_TEXT_RANGE_AA))
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC INT WAREDEF_GET_TICKER(INT iMissionEntity, BOOL bDelivered)
	IF bDelivered
		SWITCH iMissionEntity
			CASE MISSION_ENTITY_ANTI			RETURN WAREDEF_TICKER_DELIVERED_ANTI				
			CASE MISSION_ENTITY_CRATES			RETURN WAREDEF_TICKER_DELIVERED_CRATES			
			CASE MISSION_ENTITY_INSURGENT		RETURN WAREDEF_TICKER_DELIVERED_INSURGENT			
		ENDSWITCH
	ELSE
		SWITCH iMissionEntity
			CASE MISSION_ENTITY_ANTI			RETURN WAREDEF_TICKER_COLLECTED_ANTI		
			CASE MISSION_ENTITY_CRATES			RETURN WAREDEF_TICKER_COLLECTED_CRATES		
			CASE MISSION_ENTITY_INSURGENT		RETURN WAREDEF_TICKER_COLLECTED_INSURGENT		
		ENDSWITCH
	ENDIF
	
	RETURN -1
ENDFUNC

PROC WAREDEF_MISSION_ENTITY_ON_COLLECT(INT iMissionEntity)	
	MISSION_TICKER_DATA sTickerData
	sTickerData.iInt1 = WAREDEF_GET_TICKER(iMissionEntity, FALSE)
	SEND_FM_CONTENT_TICKER_EVENT_WITH_DATA(eFMCTICKER_CUSTOM, sTickerData) 
ENDPROC

PROC WAREDEF_MISSION_ENTITY_ON_DELIVERED(INT iMissionEntity, BOOL bLocalDelivered)
	MISSION_TICKER_DATA sTickerData
	IF bLocalDelivered
		sTickerData.iInt1 = WAREDEF_GET_TICKER(iMissionEntity, TRUE)
		SEND_FM_CONTENT_TICKER_EVENT_WITH_DATA(eFMCTICKER_CUSTOM, sTickerData) 
	ENDIF
ENDPROC

FUNC BOOL WAREDEF_SHOULD_MISSION_ENTITY_BE_COLLECTABLE(INT iMissionEntity)

	IF iMissionEntity = MISSION_ENTITY_ANTI
		INT iVehicle = GET_TAG_ID(WAREDEF_TAGS_AA_TRAILER)
		ENTITY_INDEX eiVeh
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
			eiVeh = NET_TO_ENT(serverBD.sVehicle[iVehicle].netId)
			IF IS_ENTITY_ALIVE(eiVeh)
				IF IS_ENTITY_ATTACHED(eiVeh)
					PRINTLN("WAREDEF_SHOULD_MISSION_ENTITY_BE_COLLECTABLE, TRUE ", iVehicle)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
		
		PRINTLN("WAREDEF_SHOULD_MISSION_ENTITY_BE_COLLECTABLE, FALSE ", iVehicle)
		
		RETURN FALSE
	ENDIF

	RETURN TRUE
ENDFUNC

FUNC BOOL WAREDEF_MISSION_ENTITY_BLIP_ENABLE(INT iMissionEntity)

	UNUSED_PARAMETER(iMissionEntity)
		
	RETURN FALSE
ENDFUNC

PROC WAREDEF_SETUP_CLIENT_STATES()
		
	ADD_CLIENT_MODE_STATES_DELIVERY(eWAREDEFSTATECLIENT_COLLECT, eWAREDEFSTATECLIENT_DELIVER, eWAREDEFSTATECLIENT_HELP_DELIVER, eWAREDEFSTATECLIENT_RECOVER, eWAREDEFSTATECLIENT_END,
		&WAREDEF_OBJECTIVE_TEXT_COLLECT, &WAREDEF_OBJECTIVE_TEXT_DELIVER, &WAREDEF_OBJECTIVE_TEXT_HELP_DELIVER, &WAREDEF_OBJECTIVE_TEXT_RECOVER)	
	
	ADD_CLIENT_MODE_STATE(eWAREDEFSTATECLIENT_END, eMODESTATE_END, &EMPTY)
ENDPROC

PROC WAREDEF_SETUP_STATES()
		
	ADD_MODE_STATE(eWAREDEFSTATE_COLLECT, eMODESTATE_COLLECT_MISSION_ENTITY)
		ADD_MODE_STATE_TRANSITION_DEFAULT(eWAREDEFSTATE_COLLECT, eWAREDEFSTATE_DELIVER)
		
	ADD_MODE_STATE(eWAREDEFSTATE_DELIVER, eMODESTATE_DELIVER_MISSION_ENTITY)
		ADD_MODE_STATE_TRANSITION_DEFAULT(eWAREDEFSTATE_DELIVER, eWAREDEFSTATE_END)
	
	ADD_MODE_STATE(eWAREDEFSTATE_END, eMODESTATE_END)
ENDPROC	

PROC WAREDEF_SCRIPT_CLEANUP()
	IF GB_GET_LOCAL_PLAYER_MISSION_HOST() = LOCAL_PLAYER_INDEX
		INT iWareDefBS
		
		#IF IS_DEBUG_BUILD
		iWareDefBS = GET_MP_INT_CHARACTER_STAT(MP_STAT_TUNER_BUNKER_TRUCK_BS)
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - WAREDEF_SCRIPT_CLEANUP - Previous value of MP_STAT_TUNER_BUNKER_TRUCK_BS = ", iWareDefBS)
		iWareDefBS = 0
		#ENDIF
		
		INT iMissionEntity
		REPEAT data.MissionEntity.iCount iMissionEntity
			IF IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_DELIVERED)
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - WAREDEF_SCRIPT_CLEANUP - eMISSIONENTITYBITSET_DELIVERED iMissionEntity = ", iMissionEntity)
				SET_BIT(iWareDefBS, iMissionEntity)
			ENDIF
		ENDREPEAT
		
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - WAREDEF_SCRIPT_CLEANUP - Setting value of MP_STAT_TUNER_BUNKER_TRUCK_BS = ", iWareDefBS)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_TUNER_BUNKER_TRUCK_BS, iWareDefBS)
	ENDIF
ENDPROC

FUNC BOOL WAREDEF_CLIENT_INIT()	
	RETURN TRUE
ENDFUNC

FUNC BOOL WAREDEF_SERVER_INIT()	
	RETURN TRUE
ENDFUNC

PROC WAREDEF_CLIENT_PROCESSING()

	IF LOCAL_VEHICLE_INDEX = WAREDEF_AA_VEH_ID()
	AND IS_MISSION_ENTITY_BIT_SET(MISSION_ENTITY_ANTI, eMISSIONENTITYBITSET_DELIVERED)
		BROADCAST_LEAVE_VEHICLE(ALL_PLAYERS_IN_VEHICLE(LOCAL_VEHICLE_INDEX), FALSE, 0.0, 0)
	ENDIF
	
ENDPROC

PROC WAREDEF_SERVER_PROCESSING()

ENDPROC

FUNC BOOL WAREDEF_SHOULD_MISSION_PARTIAL_PASS()
	INT iDelivered = WAREDEF_DELIVERED_COUNT()
	
	IF iDelivered > 0
	AND iDelivered < data.MissionEntity.iCount
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC WAREDEF_GET_MISSION_PARTIAL_PASS_VALUES(INT& iCount, INT& iTotal)
	iCount = WAREDEF_DELIVERED_COUNT()
	iTotal = data.MissionEntity.iCount
ENDPROC

FUNC BOOL WAREDEF_SHOULD_MISSION_PASS()
	IF WAREDEF_DELIVERED_COUNT() = data.MissionEntity.iCount
		RETURN TRUE
	ENDIF
	
	RETURN GET_END_REASON() = eENDREASON_MISSION_PASSED
ENDFUNC

PROC WAREDEF_SET_PLACEMENT_DATA()

	SET_DATA_BIT(eDATABITSET_ALLOW_PARTIAL_COMPLETION)

	data.Ambush.iSpawnDelay = 500

	SET_VEHICLE_DATA_BIT(GET_TAG_ID(WAREDEF_TAGS_AA_TRAILER), eVEHICLEDATABITSET_CHECK_NEAR_VEHICLE)
	
	SET_VEHICLE_DATA_BIT(GET_TAG_ID(WAREDEF_TAGS_AA_TRAILER), eVEHICLEDATABITSET_BLIP_FADE_BASED_ON_DISTANCE)
	SET_VEHICLE_DATA_BIT(GET_TAG_ID(WAREDEF_TAGS_FLATBED), eVEHICLEDATABITSET_BLIP_FADE_BASED_ON_DISTANCE)
	SET_VEHICLE_DATA_BIT(GET_TAG_ID(WAREDEF_TAGS_WASTELANDER), eVEHICLEDATABITSET_BLIP_FADE_BASED_ON_DISTANCE)
	
//	SET_VEHICLE_DATA_BIT(GET_TAG_ID(WAREDEF_TAGS_INSURGENT), eVEHICLEDATABITSET_FADE_DELETE_ON_END)
//	SET_VEHICLE_DATA_BIT(GET_TAG_ID(WAREDEF_TAGS_INSURGENT), eVEHICLEDATABITSET_DELETE_ON_CLEANUP)
//	
//	SET_VEHICLE_DATA_BIT(GET_TAG_ID(WAREDEF_TAGS_AA_TRAILER), eVEHICLEDATABITSET_FADE_DELETE_ON_END)
//	SET_VEHICLE_DATA_BIT(GET_TAG_ID(WAREDEF_TAGS_AA_TRAILER), eVEHICLEDATABITSET_DELETE_ON_CLEANUP)
	
	SET_MISSION_ENTITY_DATA_BIT(MISSION_ENTITY_ANTI, eMISSIONENTITYDATABITSET_UNCOLLECTABLE_ON_SPAWN)

	data.Prop.AttachedProp[WAREDEF_ATTACH_CRATE_1].iPropIndex = GET_TAG_ID(WAREDEF_TAGS_FLATBED_CRATE_ONE)
	data.Prop.AttachedProp[WAREDEF_ATTACH_CRATE_1].eParentType = ET_VEHICLE
	data.Prop.AttachedProp[WAREDEF_ATTACH_CRATE_1].iParentIndex = GET_TAG_ID(WAREDEF_TAGS_WASTELANDER)
	data.Prop.AttachedProp[WAREDEF_ATTACH_CRATE_1].vOffset = <<0.0, 1.0, 0.85>>
	
	data.Prop.AttachedProp[WAREDEF_ATTACH_CRATE_2].iPropIndex = GET_TAG_ID(WAREDEF_TAGS_FLATBED_CRATE_TWO)
	data.Prop.AttachedProp[WAREDEF_ATTACH_CRATE_2].eParentType = ET_VEHICLE
	data.Prop.AttachedProp[WAREDEF_ATTACH_CRATE_2].iParentIndex = GET_TAG_ID(WAREDEF_TAGS_WASTELANDER)
	data.Prop.AttachedProp[WAREDEF_ATTACH_CRATE_2].vOffset = <<0.0, -1.0, 0.85>>
	
	data.Prop.AttachedProp[WAREDEF_ATTACH_CRATE_3].iPropIndex = GET_TAG_ID(WAREDEF_TAGS_FLATBED_CRATE_THREE)
	data.Prop.AttachedProp[WAREDEF_ATTACH_CRATE_3].eParentType = ET_VEHICLE
	data.Prop.AttachedProp[WAREDEF_ATTACH_CRATE_3].iParentIndex = GET_TAG_ID(WAREDEF_TAGS_WASTELANDER)
	data.Prop.AttachedProp[WAREDEF_ATTACH_CRATE_3].vOffset = <<0.0, -3.0, 0.85>>
	
	
	//Bottom right HUD.
	data.BottomRightHUD[0].eType = eBOTTOMRIGHTHUD_NUMBER
	data.BottomRightHUD[0].iMaxValue = 3
	data.BottomRightHUD[0].sTitle = "TR_HUD_ARMS"

	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), " WAREDEF_SET_PLACEMENT_DATA - done ")
ENDPROC

#IF IS_DEBUG_BUILD
FUNC BOOL WAREDEF_OVERRIDE_J_SKIP()

	VECTOR vPos, vTemp
	FLOAT fHeading//, fTemp
	
	SWITCH GET_WAREDEF_MODE_STATE()
		CASE eWAREDEFSTATE_COLLECT
		
			vTemp = data.MissionEntity.MissionEntities[MISSION_ENTITY_CRATES].vCoords
		
			SWITCH WAREDEF_DELIVERED_COUNT() 
				CASE 1
					vTemp = data.MissionEntity.MissionEntities[MISSION_ENTITY_ANTI].vCoords
				BREAK
				CASE 2
					vTemp = data.MissionEntity.MissionEntities[MISSION_ENTITY_INSURGENT].vCoords
				BREAK
			ENDSWITCH
		
			IF NOT IS_ENTITY_AT_COORD(LOCAL_PED_INDEX, vTemp, <<10.0,10.0,10.0>>)
				vPos = vTemp
			ENDIF
		BREAK
	ENDSWITCH
	
	IF NOT IS_VECTOR_ZERO(vPos)
		SET_ENTITY_COORDS(LOCAL_PED_INDEX, vPos)
		SET_ENTITY_HEADING(LOCAL_PED_INDEX, fHeading)
		SET_GAMEPLAY_CAM_RELATIVE_HEADING()
		
		RETURN TRUE
	ENDIF

	RETURN FALSE
	
ENDFUNC
#ENDIF

PROC WAREDEF_SETUP_LOGIC()

	#IF IS_DEBUG_BUILD
	logic.Debug.OverrideJSkip = &WAREDEF_OVERRIDE_J_SKIP
	#ENDIF
	
	logic.Data.Setup = &WAREDEF_SET_PLACEMENT_DATA
	
	logic.ModeTimer.Enable = &WAREDEF_SHOULD_START_MODE_TIMER
	
	logic.StateMachine.SetupStates = &WAREDEF_SETUP_STATES
	logic.StateMachine.SetupClientStates = &WAREDEF_SETUP_CLIENT_STATES
	
	logic.Init.Client = &WAREDEF_CLIENT_INIT
	logic.Init.Server = &WAREDEF_SERVER_INIT
	
	logic.Mission.PartialPass = &WAREDEF_SHOULD_MISSION_PARTIAL_PASS
	logic.Mission.PartialPassValues = &WAREDEF_GET_MISSION_PARTIAL_PASS_VALUES
	logic.Mission.Pass = &WAREDEF_SHOULD_MISSION_PASS
	
	logic.MissionEntity.OnCollect = &WAREDEF_MISSION_ENTITY_ON_COLLECT
	logic.MissionEntity.OnDelivery = &WAREDEF_MISSION_ENTITY_ON_DELIVERED
	logic.MissionEntity.ShouldBeCollectable = &WAREDEF_SHOULD_MISSION_ENTITY_BE_COLLECTABLE
	logic.MissionEntity.Blip.Enable = &WAREDEF_MISSION_ENTITY_BLIP_ENABLE
	
	logic.Hud.Tickers.CustomStringLocal = &WAREDEF_TICKER_LOCAL
	logic.Hud.Tickers.CustomStringRemote = &WAREDEF_TICKER_REMOTE
	
	logic.BottomRightHUD.Enable = &WAREDEF_SHOULD_ENABLE_BOTTOM_RIGHT_HUD
	logic.BottomRightHUD.Value = &WAREDEF_GET_BOTTOM_RIGHT_HUD_VALUE
	
	logic.HelpText.Text = &WAREDEF_HELP_TEXT_STRING
	logic.HelpText.Trigger = &WAREDEF_HELP_TEXT_TRIGGER
	
	logic.TextMessages.ShouldSend = &WAREDEF_GET_TEXT_MESSAGE_SHOULD_BE_SENT
	logic.TextMessages.Character = &WAREDEF_GET_TEXT_MESSAGE_CHARACTER
	logic.TextMessages.Label = &WAREDEF_GET_TEXT_MESSAGE_LABEL
	
	logic.Dialogue.Root = &WAREDEF_GET_DIALOGUE_ROOT
	logic.Dialogue.SubtitleGroup = &WAREDEF_GET_DIALOGUE_SUBTITLE_GROUP
	logic.Dialogue.Character = &WAREDEF_GET_DIALOGUE_CHARACTER
	logic.Dialogue.CellphoneCharacter = &WAREDEF_GET_DIALOGUE_CELLPHONE_CHARACTER
	logic.Dialogue.UseHeadsetAudio = &WAREDEF_SHOULD_DIALOGUE_USE_HEADSET_AUDIO
	logic.Dialogue.ShouldTrigger = &WAREDEF_SHOULD_TRIGGER_DIALOGUE
	logic.Dialogue.Delay = &WAREDEF_DIALOGUE_DELAY
	
	logic.Audio.MusicTrigger.Event = &WAREDEF_GET_NEXT_MUSIC_EVENT
	logic.Audio.MusicTrigger.EventString = &WAREDEF_GET_MUSIC_EVENT_STRING
	logic.Audio.MusicTrigger.StopString = &GET_TUNER_ROBBERY_MUSIC_STOP_EVENT_STRING
	logic.Audio.MusicTrigger.FailString = &GET_TUNER_ROBBERY_MUSIC_FAIL_EVENT_STRING
	
	logic.Phonecall.PostMissionCall = &WAREDEF_POST_MISSION_CALL
	
	logic.Ped.Behaviour.Setup = &WAREDEF_SETUP_PED_TASKS
	logic.Ped.Behaviour.Get = &WAREDEF_GET_PED_BEHAVIOUR
	logic.Ped.Triggers.Setup = &WAREDEF_SETUP_PED_TRIGGERS
	logic.Ped.Task.GoToCoordAnyMeans.NumCoords = &WAREDEF_FLATBED_NUM_COORDS
	logic.Ped.Task.GoToCoordAnyMeans.Coords = &WAREDEF_FLATBED_GO_TO_COORDS
	
	logic.Vehicle.Attributes = &WAREDEF_VEHICLE_SET_ATTRIBUTES
	logic.Vehicle.Mods = &WAREDEF_APPLY_VEHICLE_MODS
	logic.Vehicle.NearRange = &WAREDEF_GET_VEHICLE_NEAR_RANGE
	logic.Vehicle.Trailer.Offset = &WAREDEF_TRAILER_OFFSET
	logic.Vehicle.Trailer.Rotation = &WAREDEF_TRAILER_ROT
	logic.Vehicle.OnCleanup = &WAREDEF_VEHICLE_ON_CLEANUP
	logic.Vehicle.Client = &WAREDEF_VEHICLE_CLIENT
	logic.Vehicle.Blip.Enable = &WAREDEF_VEHICLE_BLIP_ENABLE
	logic.Vehicle.Blip.Sprite = &TUNER_MISSION_ENTITY_BLIP_SPRITE
	logic.Vehicle.Blip.Name = &WAREDEF_VEHICLE_BLIP_NAME
	logic.Vehicle.Blip.ConstantFlash = &WAREDEF_VEHICLE_BLIP_FLASH
	
	logic.Prop.Rotation = &WAREDEF_PROP_ROTATION
	logic.Prop.Client = &THERMAL_CLIENT_PROP_MAINTAIN
	
	logic.Maintain.Client = &WAREDEF_CLIENT_PROCESSING
	logic.Maintain.Server = &WAREDEF_SERVER_PROCESSING
	
	logic.Ambush.ShouldActivate = &WAREDEF_SHOULD_ACTIVATE_AMBUSH
	logic.Ambush.DropoffDismissRange = &WAREDEF_AMBUSH_DISMISS_RANGE
	logic.Ambush.CleanupRange = &WAREDEF_AMBUSH_CLEANUP_RANGE
	logic.Ambush.SpawnOffsetRange = &WAREDEF_AMBUSH_RANGE
	logic.Ambush.ShouldPrioritiseTarget = &WAREDEF_AMBUSH_PRIORITY_TARGET
	logic.Ambush.TargetEntityType = &WAREDEF_GET_AMBUSH_TARGET_TYPE
	
	logic.Cleanup.ScriptEnd = &WAREDEF_SCRIPT_CLEANUP
ENDPROC

