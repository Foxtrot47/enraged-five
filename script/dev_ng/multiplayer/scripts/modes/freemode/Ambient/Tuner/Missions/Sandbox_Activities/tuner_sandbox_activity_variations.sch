
USING "globals.sch"
USING "rage_builtins.sch"

CONST_INT ENABLE_CONTENT	FEATURE_TUNER

#IF ENABLE_CONTENT
USING "variations/tuner_sandbox_activity_time_trial.sch"
USING "variations/tuner_sandbox_activity_sprint_race.sch"
USING "variations/tuner_sandbox_activity_head_to_head.sch"
USING "variations/tuner_sandbox_activity_checkpoint_dash.sch"

/// PURPOSE: Use to set up the logic pointers for your mission
///    
PROC SETUP_MISSION_LOGIC()
	SWITCH GET_VARIATION()
		CASE SAV_TIME_TRIAL				TIME_TRIAL_LOGIC()				BREAK
		CASE SAV_SPRINT					SPRINT_RACE_SETUP_LOGIC()		BREAK
		CASE SAV_CHECKPOINT_DASH 		CHECKPOINT_DASH_LOGIC()			BREAK
		CASE SAV_HEAD_TO_HEAD			HEAD_TO_HEAD_LOGIC()			BREAK
	ENDSWITCH
ENDPROC
#ENDIF
