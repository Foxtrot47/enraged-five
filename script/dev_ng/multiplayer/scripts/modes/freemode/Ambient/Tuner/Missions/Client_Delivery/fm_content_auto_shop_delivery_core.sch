USING "globals.sch"
USING "rage_builtins.sch"

USING "fm_content_auto_shop_delivery_structs.sch"
USING "fm_content_gb_mission_structs.sch"
USING "fm_content_generic_structs.sch"

CONST_INT ENABLE_CONTENT	FEATURE_TUNER

#IF ENABLE_CONTENT

FUNC STRING GET_MISSION_PLACEMENT_DATA_FILENAME()
	RETURN ""
ENDFUNC

//----------------------
//	ENUMS
//----------------------

ENUM eCUSDEL_STATE
	eCD_STATE_COLLECT,
	eCD_STATE_DELIVER,
	
	eCD_STATE_END
ENDENUM

ENUM eCUSDEL_CLIENT_STATE
	eCD_CLIENTSTATE_COLLECT,
	eCD_CLIENTSTATE_DELIVER,
	eCD_CLIENTSTATE_HELP_DELIVER,
	eCD_CLIENTSTATE_RECOVER,
	
	eCD_CLIENTSTATE_END
ENDENUM

ENUM eCUSDEL_HELP_TEXT
	eCUSDEL_HELPTEXT_DELIVERY,
	
	eCUSDEL_HELPTEXT_MAX
ENDENUM

ENUM eCUSDEL_TICKER_EVENT
	eCUSDEL_TICKEREVENT_DELIVERY_FINISHED,
	
	eCUSDEL_TICKEREVENT_MAX
ENDENUM

ENUM eCUSDEL_TEXT_MESSAGES
	eCUSDEL_TEXTMESSAGES_PERFECTLY_DELIVERED = 0
ENDENUM

//----------------------
//	LOCAL VARIABLES
//----------------------

SCRIPT_TIMER TimerForPlayerWarps
CONST_INT TimeBeforePlayerWarpsIntoVehicle		2000		

BOOL bWarpedInVeh
BOOL bFadeInAfterWarp

INT iVehicleDamagesAmount = -1

CONST_INT CUSDEL_VEHICLE_TO_DELIVER_ID 0

//----------------------
//	CORE
//----------------------

FUNC TUNER_CLIENT_DELIVERY_VARIATION GET_VARIATION()
	RETURN serverBD.sMissionData.eVariation
ENDFUNC

FUNC TUNER_CLIENT_DELIVERY_SUBVARIATION GET_SUBVARIATION()
	RETURN serverBD.sMissionData.eSubvariation
ENDFUNC

FUNC MODEL_NAMES CUSDEL_GET_DELIVERY_MODEL()
	RETURN serverBD.sMissionData.eDeliveryModel
ENDFUNC

FUNC STRING GET_VARIATION_NAME_FOR_DEBUG()
	RETURN TUNER_CLIENT_DELIVERY_GET_VARIATION_NAME_FOR_DEBUG(GET_VARIATION())
ENDFUNC

FUNC STRING GET_SUBVARIATION_NAME_FOR_DEBUG()
	RETURN TUNER_CLIENT_DELIVERY_GET_SUBVARIATION_NAME_FOR_DEBUG(GET_SUBVARIATION())
ENDFUNC

FUNC STRING CUSDEL_GET_VEHICLE_NAME()
	RETURN GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(CUSDEL_GET_DELIVERY_MODEL())
ENDFUNC

FUNC STRING CUSDEL_GET_VEHICLE_NAME_FROM_MODEL()
	SWITCH CUSDEL_GET_DELIVERY_MODEL()
		CASE WEEVIL RETURN "WEEVIL_TCD"
		CASE BRIOSO2 RETURN "BRIOSO2_TCD"
		CASE RHAPSODY RETURN "RHAPSODY_TCD"
		CASE CLUB RETURN "CLUB_TCD"
		CASE ISSI3 RETURN "ISSI3_TCD"
		CASE NEBULA RETURN "NEBULA_TCD"
		CASE DYNASTY RETURN "DYNASTY_TCD"
		CASE FAGALOA RETURN "FAGALOA_TCD"
		CASE FUTO RETURN "FUTO_TCD" // Futo
		CASE ASBO RETURN "ASBO_TCD"
		CASE SENTINEL RETURN "SENTINEL3_TCD" // Sentinel XS
		CASE DOMINATOR3 RETURN "DOMINATOR3_TCD"
		CASE VAMOS RETURN "VAMOS_TCD"
		CASE HERMES RETURN "HERMES_TCD"
		CASE COQUETTE3 RETURN "COQUETTE3_TCD"
		CASE RIATA RETURN "RIATA_TCD" // Riata
		CASE EVERON RETURN "EVERON_TCD"
		CASE GLENDALE2 RETURN "GLENDALE2_TCD"
		CASE SCHAFTER3 RETURN "SCHAFTER3_TCD"
		CASE WARRENER RETURN "WARRENER_TCD" // Warrener
		CASE PRIMO2 RETURN "PRIMO2_TCD"
		CASE SEMINOLE2 RETURN "SEMINOLE2_TCD"
		CASE REBLA RETURN "REBLA_TCD"
		CASE SWINGER RETURN "SWINGER_TCD"
		CASE FLASHGT RETURN "FLASHGT_TCD"
		CASE RAIDEN RETURN "RAIDEN_TCD"
		CASE SURANO RETURN "SURANO_CON_TCD" // Surano Convertible
		CASE PENUMBRA2 RETURN "PENUMBRA2_TCD"
		CASE VSTR RETURN "VSTR_TCD"
		CASE JUGULAR RETURN "jugular_TCD"
		CASE TOROS RETURN "TOROS_TCD"
		CASE ENTITY2 RETURN "ENTITY2_TCD"
		CASE TEMPESTA RETURN "TEMPESTA_TCD"
		CASE THRAX RETURN "THRAX_TCD"
		CASE SC1 RETURN "SC1_TCD"
		CASE GP1 RETURN "GP1_TCD"
		CASE CHEETAH2 RETURN "CHEETAH2_TCD"
		CASE NEO RETURN "NEO_TCD"
		CASE COMET2 RETURN "COMET2_TCD" // Pfister Comet
		CASE PARAGON RETURN "PARAGON_TCD"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC BOOL GET_MISSION_PLACEMENT_DATA()
	RETURN TRUE
ENDFUNC

PROC CUSDEL_ADD_MISSION_TICKER(eCUSDEL_TICKER_EVENT eTicker)
	MISSION_TICKER_DATA sTickerData
	sTickerData.iInt1 = ENUM_TO_INT(eTicker)
	SEND_FM_CONTENT_TICKER_EVENT_WITH_DATA(eFMCTICKER_CUSTOM, sTickerData)
ENDPROC

FUNC BOOL SETUP_VARIATION(INT iVariation, INT iSubvariation)

	serverBD.sMissionData.eVariation = INT_TO_ENUM(TUNER_CLIENT_DELIVERY_VARIATION, iVariation)
	serverBD.sMissionData.eSubvariation = INT_TO_ENUM(TUNER_CLIENT_DELIVERY_SUBVARIATION, iSubvariation)
	
	serverBD.sMissionData.eDeliveryModel = GET_AUTO_SHOP_CLIENT_VEHICLE_MODEL(GET_LOCAL_PLAYER_AUTO_SHOP_CLIENT_VEHICLE_DELIVERY_SLOT())
	
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] SETUP_VARIATION - Variation: ", GET_VARIATION_NAME_FOR_DEBUG())
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] SETUP_VARIATION - Subvariation: ", GET_SUBVARIATION_NAME_FOR_DEBUG())
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] SETUP_VARIATION - Vehicle: ", CUSDEL_GET_VEHICLE_NAME())
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] SETUP_VARIATION - New Vehicle name: ", CUSDEL_GET_VEHICLE_NAME_FROM_MODEL())
	
	
	RETURN TRUE
ENDFUNC

FUNC BOOL CUSDEL_GET_MISSION_FINISH_STATE()

	IF IS_GENERIC_BIT_SET(eGENERICBITSET_I_WON)
		PRINTLN("[DAVOR-TEST] Mission Passed.")
		RETURN TRUE
	ELSE
		PRINTLN("[DAVOR-TEST] Mission Failed.")		
		RETURN FALSE
	ENDIF
	
ENDFUNC

// Using this instead of GET_TUNER_CLIENT_VEHICLE_PRICE because of bugstar://7107711
// (using new potential pay base values)
FUNC INT CUSDEL_GET_CLIENT_VEHICLE_PRICE(MODEL_NAMES vehicleModel)
	SWITCH GET_TUNER_CLIENT_VEHICLE_PRICE_GROUP(vehicleModel)
		CASE 1			RETURN 	g_sMPTunables.iTUNER_CLIENT_VEHICLE_DELIVERY_LOW_PAYMENT
		CASE 2			RETURN 	g_sMPTunables.iTUNER_CLIENT_VEHICLE_DELIVERY_MID_PAYMENT
		CASE 3			RETURN 	g_sMPTunables.iTUNER_CLIENT_VEHICLE_DELIVERY_HIGH_PAYMENT
	ENDSWITCH
	RETURN 0
ENDFUNC

FUNC INT CUSDEL_GET_VEHICLE_POTENTIAL_PAY()
	RETURN CUSDEL_GET_CLIENT_VEHICLE_PRICE(CUSDEL_GET_DELIVERY_MODEL())
ENDFUNC

PROC ACTIVATE_FREEMODE_DELIVERY_DROPOFF(BOOL bActive)
	IF bActive
		IF FREEMODE_DELIVERY_GET_ACTIVE_LOCAL_DROPOFF() = FREEMODE_DELIVERY_DROPOFF_INVALID
			FREEMODE_DELIVERY_ACTIVATE_DROPOFF_FOR_FM_EVENT(GET_DROPOFF(), serverBD.sMissionID)
		ENDIF
	ELSE
		FREEMODE_DELIVERY_DEACTIVATE_DROPOFF_FOR_RIVAL_PACKAGE_FOR_MISSION(serverBD.sMissionID)
	ENDIF
ENDPROC

FUNC FREEMODE_DELIVERABLE_TYPE GET_MISSION_ENTITY_DELIVERABLE_TYPE()
	RETURN FMC_DELIVERABLE
ENDFUNC

FUNC BOOL SETUP_DROP_OFFS()
	IF IS_GENERIC_BIT_SET(eGENERICBITSET_SETUP_DROP_OFFS)
	OR logic.Delivery.Dropoff.Setup = NULL
		RETURN TRUE
	ENDIF

	INT i
	REPEAT COUNT_OF(sDeliveryLocal.sDropoffs) i
		sDeliveryLocal.sDropoffs[i].eDropoff = CALL logic.Delivery.Dropoff.Setup(i)
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - SETUP_DROP_OFFS - drop off ", i, " = ", sDeliveryLocal.sDropoffs[i].eDropoff)
	ENDREPEAT
	sDeliveryLocal.iCount = i
	
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - SETUP_DROP_OFFS - Dropoff Count: ", sDeliveryLocal.iCount)
	
	SET_GENERIC_BIT(eGENERICBITSET_SETUP_DROP_OFFS)
	
	RETURN TRUE
ENDFUNC

PROC CLEANUP_DROP_OFFS()

ENDPROC

FUNC FREEMODE_DELIVERY_DROPOFFS CUSDEL_SETUP_DROPOFF(INT iDropoff)
	IF iDropoff = 0
		SWITCH GET_VARIATION()
			CASE TCDV_HIGH
				SWITCH GET_SUBVARIATION()
					CASE TCDS_HIGH_LOC_1			RETURN FMC_DROPOFF_AUTO_SHOP_CUSTOMER_H1
					CASE TCDS_HIGH_LOC_2			RETURN FMC_DROPOFF_AUTO_SHOP_CUSTOMER_H2
					CASE TCDS_HIGH_LOC_3			RETURN FMC_DROPOFF_AUTO_SHOP_CUSTOMER_H3
					CASE TCDS_HIGH_LOC_4			RETURN FMC_DROPOFF_AUTO_SHOP_CUSTOMER_H4
					CASE TCDS_HIGH_LOC_5			RETURN FMC_DROPOFF_AUTO_SHOP_CUSTOMER_H5
					CASE TCDS_HIGH_LOC_6			RETURN FMC_DROPOFF_AUTO_SHOP_CUSTOMER_H6
					CASE TCDS_HIGH_LOC_7			RETURN FMC_DROPOFF_AUTO_SHOP_CUSTOMER_H7
					CASE TCDS_HIGH_LOC_8			RETURN FMC_DROPOFF_AUTO_SHOP_CUSTOMER_H8
				ENDSWITCH
			BREAK
			CASE TCDV_LOW
				SWITCH  GET_SUBVARIATION()
					CASE TCDS_LOW_LOC_1			RETURN FMC_DROPOFF_AUTO_SHOP_CUSTOMER_L1
					CASE TCDS_LOW_LOC_2			RETURN FMC_DROPOFF_AUTO_SHOP_CUSTOMER_L2
					CASE TCDS_LOW_LOC_3			RETURN FMC_DROPOFF_AUTO_SHOP_CUSTOMER_L3
					CASE TCDS_LOW_LOC_4			RETURN FMC_DROPOFF_AUTO_SHOP_CUSTOMER_L4
					CASE TCDS_LOW_LOC_5			RETURN FMC_DROPOFF_AUTO_SHOP_CUSTOMER_L5
					CASE TCDS_LOW_LOC_6			RETURN FMC_DROPOFF_AUTO_SHOP_CUSTOMER_L6
					CASE TCDS_LOW_LOC_7			RETURN FMC_DROPOFF_AUTO_SHOP_CUSTOMER_L7
					CASE TCDS_LOW_LOC_8			RETURN FMC_DROPOFF_AUTO_SHOP_CUSTOMER_L8
				ENDSWITCH
			BREAK
		ENDSWITCH
	ENDIF
	RETURN FREEMODE_DELIVERY_DROPOFF_INVALID
ENDFUNC

//----------------------
//	OBJECTIVES
//----------------------

PROC CUSDEL_OBJTXT_COLLECT()
	Print_Objective_Text_With_Coloured_Text_Label("TCD_OT_ENTER", CUSDEL_GET_VEHICLE_NAME_FROM_MODEL(), HUD_COLOUR_BLUE)
ENDPROC

PROC CUSDEL_OBJTXT_DELIVER()
	Print_Objective_Text_With_Coloured_Text_Label("TCD_OT_DELI", CUSDEL_GET_VEHICLE_NAME_FROM_MODEL(), HUD_COLOUR_WHITE)
ENDPROC

PROC CUSDEL_OBJTXT_HELP_DELIVER()
	Print_Objective_Text_With_Coloured_Text_Label("TCD_OT_HDELI", CUSDEL_GET_VEHICLE_NAME_FROM_MODEL(), HUD_COLOUR_WHITE)
ENDPROC

//----------------------
//	BIG MESSAGE
//----------------------

FUNC STRING GET_START_BIG_MESSAGE_TITLE()
	RETURN "TCD_BM_TITLE" // AUTO SHOP SERVICE
ENDFUNC

FUNC STRING GET_START_BIG_MESSAGE_STRAPLINE()
	RETURN CUSDEL_GET_VEHICLE_NAME()
ENDFUNC

FUNC STRING GET_START_BIG_MESSAGE_SUBSTRING()
	RETURN "" 
ENDFUNC

PROC DISPLAY_START_BIG_MESSAGE()
	SETUP_NEW_BIG_MESSAGE_WITH_STRING(BIG_MESSAGE_GB_START_OF_JOB, GET_START_BIG_MESSAGE_SUBSTRING(), GET_START_BIG_MESSAGE_STRAPLINE(), GET_START_BIG_MESSAGE_TITLE(), DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, BIG_MESSAGE_BIT_DO_NOT_CLEANUP_GB_MESSAGES)
ENDPROC

FUNC STRING GET_END_BIG_MESSAGE_SUCCESS_SUBSTRING()
	RETURN CUSDEL_GET_VEHICLE_NAME_FROM_MODEL()
ENDFUNC

FUNC STRING GET_END_BIG_MESSAGE_SUCCESS_STRAPLINE()
	RETURN "TCD_BM_PASS"
ENDFUNC

FUNC STRING GET_END_BIG_MESSAGE_SUCCESS_TITLE()
	RETURN "TCD_BM_TITLE"
ENDFUNC

FUNC STRING GET_END_BIG_MESSAGE_FAIL_SUBSTRING()
	RETURN CUSDEL_GET_VEHICLE_NAME_FROM_MODEL()
ENDFUNC

FUNC STRING GET_END_BIG_MESSAGE_FAIL_STRAPLINE()
	RETURN "TCD_BM_FAIL"
ENDFUNC

FUNC STRING GET_END_BIG_MESSAGE_FAIL_TITLE()
	RETURN "TCD_BM_TITLE"
ENDFUNC

PROC DISPLAY_END_BIG_MESSAGE()
	IF NOT IS_GENERIC_BIT_SET(eGENERICBITSET_I_WON)
		SETUP_NEW_BIG_MESSAGE_WITH_STRING(BIG_MESSAGE_GB_END_OF_JOB_FAIL, GET_END_BIG_MESSAGE_FAIL_SUBSTRING(), GET_END_BIG_MESSAGE_FAIL_STRAPLINE(), GET_END_BIG_MESSAGE_FAIL_TITLE())
	ENDIF
ENDPROC

FUNC BOOL CUSDEL_IS_VEHICLE_IN_GOOD_CONDITION()
	INT vehInitialValue  = CUSDEL_GET_VEHICLE_POTENTIAL_PAY()
	INT vehTotalValueLeft = vehInitialValue  - GET_VEHICLE_VALUE_LOST()
	
	FLOAT vehValuePercentage = (vehTotalValueLeft * 100.0) / vehInitialValue

	IF vehValuePercentage > 80 AND vehValuePercentage <= 100
		PRINTLN("[DAVOR-TEST] Vehicle is in GOOD condition, returning TRUE.: ", vehValuePercentage)
		RETURN TRUE
	ELSE
		PRINTLN("[DAVOR-TEST] Vehicle is in BAD condition, returning FALSE.: ", vehValuePercentage)
		RETURN FALSE
	ENDIF
	
ENDFUNC

PROC CUSDEL_SETTING_END_SHARD()
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
	AND NOT GB_IS_PLAYER_MEMBER_OF_A_GANG(LOCAL_PLAYER_INDEX, FALSE)
		serverBD.sMissionData.iServiceCostAmount = GET_TUNER_CLIENT_VEHICLE_PRICE(CUSDEL_GET_DELIVERY_MODEL())
		PRINTLN("[DAVOR-TEST] Boss Service Cost calculation: ", serverBD.sMissionData.iServiceCostAmount)
	ENDIF
	
	PRINTLN("[DAVOR-TEST] ------------- SETTING END SHARD -------------")
	missionEndShardData.bPass = HAVE_ALL_MISSION_ENTITIES_BEEN_DELIVERED()
	missionEndShardData.bRunOnReturnToFreemode = HAVE_ALL_MISSION_ENTITIES_BEEN_DELIVERED()
	missionEndShardData.eType = MISSION_END_SHARD_TYPE_TUNER_DELIVERY
	missionEndShardData.iTake[0] = CUSDEL_GET_VEHICLE_POTENTIAL_PAY() //Payment for delivery
	missionEndShardData.iTake[1] = serverBD.sMissionData.iServiceCostAmount //Service costs
	missionEndShardData.iMissionFee = -GET_VEHICLE_VALUE_LOST() //Damages
	missionEndShardData.playerCut[0].iCut = serverBD.sMissionData.iVehicleBonusAmount // Satisfaction (I would have used iTake, but max is 2)
	missionEndShardData.iApproachID = ENUM_TO_INT(CUSDEL_GET_DELIVERY_MODEL()) // Vehicle

ENDPROC

PROC CUSDEL_SETUP_MODE_STATES()
	ADD_MODE_STATE(eCD_STATE_COLLECT, eMODESTATE_COLLECT_MISSION_ENTITY)
		ADD_MODE_STATE_TRANSITION_DEFAULT(eCD_STATE_COLLECT, eCD_STATE_DELIVER)
		
	ADD_MODE_STATE(eCD_STATE_DELIVER, eMODESTATE_DELIVER_MISSION_ENTITY)
		ADD_MODE_STATE_TRANSITION_DEFAULT(eCD_STATE_DELIVER, eCD_STATE_END)
		
	ADD_MODE_STATE(eCD_STATE_END, eMODESTATE_END)
ENDPROC

PROC CUSDEL_SETUP_CLIENT_MODE_STATES()
	ADD_CLIENT_MODE_STATES_DELIVERY(eCD_CLIENTSTATE_COLLECT, eCD_CLIENTSTATE_DELIVER, eCD_CLIENTSTATE_HELP_DELIVER, eCD_CLIENTSTATE_RECOVER, eCD_CLIENTSTATE_END,
									&CUSDEL_OBJTXT_COLLECT,  &CUSDEL_OBJTXT_DELIVER,  &CUSDEL_OBJTXT_HELP_DELIVER,  &EMPTY)
									
	ADD_CLIENT_MODE_STATE(eCD_CLIENTSTATE_END, eMODESTATE_END, &EMPTY)
ENDPROC

/////////////////////
//    REWARDS     //
///////////////////

PROC CUSDEL_SETUP_SPECIAL_REWARDS()
	INT iCharacterAward = GET_MP_INT_CHARACTER_AWARD(MP_AWARD_AUTO_SHOP)
	iCharacterAward += 1
	SET_MP_INT_CHARACTER_AWARD(MP_AWARD_AUTO_SHOP, iCharacterAward)
ENDPROC

PROC _DEPRECATED_GIVE_REWARD()
	IF IS_GENERIC_CLIENT_BIT_SET(LOCAL_PARTICIPANT_INDEX, eGENERICCLIENTBITSET_COMPLETED_REWARDS)
		EXIT
	ENDIF
	
	IF GET_END_REASON() != eENDREASON_NO_REASON_YET
		IF iEarnValue = -1 AND IS_GENERIC_BIT_SET(eGENERICBITSET_I_WON)
			IF GET_LOCAL_PLAYER_AUTO_SHOP_CLIENT_VEHICLE_DELIVERY_SLOT() != -1
				serverBD.sMissionData.iVehicleBonusAmount = GET_AUTO_SHOP_CLIENT_VEHICLE_BONUS_AMOUNT(GET_LOCAL_PLAYER_AUTO_SHOP_CLIENT_VEHICLE_DELIVERY_SLOT())
			ENDIF
			iVehicleDamagesAmount = GET_VEHICLE_VALUE_LOST()
			iEarnValue = (CUSDEL_GET_VEHICLE_POTENTIAL_PAY()+GET_TUNER_CLIENT_VEHICLE_PRICE(CUSDEL_GET_DELIVERY_MODEL())-GET_VEHICLE_VALUE_LOST())+serverBD.sMissionData.iVehicleBonusAmount
			
			IF iEarnValue < 0 // If the earn value is below 0, don't give any reward.
				iEarnValue = 0
			ENDIF
		ENDIF
		
		IF iEarnValue = -1
			iEarnValue = 0 // To avoid errors (in case the mission entity wasn't delivered).
		ENDIF

		REMOVE_TUNER_CLIENT_VEHICLE(iEarnValue, REMOVE_CONTRA_MISSION_PASSED, missionEndShardData.eAutoShopResult, IS_GENERIC_BIT_SET(eGENERICBITSET_I_WON))
		PRINTLN("[DAVOR-TEST] ######## Checking eAutoShopResult ########")
		IF missionEndShardData.eAutoShopResult = CONTRABAND_TRANSACTION_STATE_SUCCESS OR missionEndShardData.eAutoShopResult = CONTRABAND_TRANSACTION_STATE_FAILED
			SET_GENERIC_CLIENT_BIT(eGENERICCLIENTBITSET_COMPLETED_REWARDS)
		ENDIF
		
		// Safe check so the members of the gang do not stop the mission from ending.
		INT iSlotToRemove = GET_LOCAL_PLAYER_AUTO_SHOP_CLIENT_VEHICLE_DELIVERY_SLOT()
		IF iSlotToRemove < 0 OR iSlotToRemove >= MAX_SAVED_CLIENT_VEHS_IN_AUTOSHOP
			SET_GENERIC_CLIENT_BIT(eGENERICCLIENTBITSET_COMPLETED_REWARDS)
		ENDIF
		
		PRINTLN("[DAVOR-TEST] Trying transaction again..")
	ENDIF
	
ENDPROC

PROC PROCESS_ON_DELIVERY_CASH_REWARD(INT iCashToGive)
	IF iCashToGive > 0
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] PROCESS_ON_DELIVERY_CASH_REWARD - iCashToGive = ", iCashToGive)
	
		IF USE_SERVER_TRANSACTIONS()
			INT iTransactionIndex
			TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_AUTO_SHOP_DELIVERY_AWARD, iCashToGive, iTransactionIndex, FALSE, FALSE, FALSE)
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] PROCESS_ON_DELIVERY_CASH_REWARD - TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION, SERVICE_EARN_AUTO_SHOP_DELIVERY_AWARD")
		ELSE
			 NETWORK_EARN_AUTOSHOP_INCOME(iCashToGive, FMMC_TYPE_TUNER_CLIENT_DELIVERY)
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] PROCESS_ON_DELIVERY_CASH_REWARD - NETWORK_EARN_AUTOSHOP_INCOME")
		ENDIF
		
		sTelemetry.iCashEarned += iCashToGive
	ELSE
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] PROCESS_ON_DELIVERY_CASH_REWARD - iCashToGive <= 0")
	ENDIF
ENDPROC

PROC PROCESS_ON_DELIVERY_RP_REWARD(INT iRpToGive)
	IF iRpToGive > 0
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] PROCESS_ON_DELIVERY_RP_REWARD - iRpToGive = ", iRpToGive)
	
		GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD, PLAYER_PED_ID(), "", XPTYPE_COMPLETE, XPCATEGORY_FM_CONTENT_TUNER_AUTO_SHOP_DELIVERY, iRpToGive)
		
		sTelemetry.iRpEarned += iRpToGive
	ELSE
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] PROCESS_ON_DELIVERY_RP_REWARD - iRpToGive <= 0")
	ENDIF
ENDPROC

FUNC INT CUSDEL_ON_DELIVERY_CASH(BOOL bLocalDelivered)
	
	IF bLocalDelivered
		IF GB_IS_PLAYER_MEMBER_OF_A_GANG(LOCAL_PLAYER_INDEX, FALSE) // To avoid computing the IF below if we are the BOSS
			IF IS_PED_IN_VEHICLE(LOCAL_PED_INDEX, NET_TO_VEH(serverBD.sVehicle[CUSDEL_VEHICLE_TO_DELIVER_ID].netId))
				IF NOT IS_PED_IN_VEHICLE(GET_PLAYER_PED(GB_GET_LOCAL_PLAYER_GANG_BOSS()), NET_TO_VEH(serverBD.sVehicle[CUSDEL_VEHICLE_TO_DELIVER_ID].netId))
					PRINTLN("[DAVOR-TEST] Rewarded $5000 to the member for deliverying the vehicle.")
					RETURN 5000
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN 0
ENDFUNC

FUNC INT CUSDEL_ON_DELIVERY_RP(BOOL bLocalDelivered)
	IF bLocalDelivered
		IF GB_IS_PLAYER_MEMBER_OF_A_GANG(LOCAL_PLAYER_INDEX, FALSE) // To avoid computing the IF below if we are the BOSS
			IF IS_PED_IN_VEHICLE(LOCAL_PED_INDEX, NET_TO_VEH(serverBD.sVehicle[CUSDEL_VEHICLE_TO_DELIVER_ID].netId))
				IF NOT IS_PED_IN_VEHICLE(GET_PLAYER_PED(GB_GET_LOCAL_PLAYER_GANG_BOSS()), NET_TO_VEH(serverBD.sVehicle[CUSDEL_VEHICLE_TO_DELIVER_ID].netId))
					PRINTLN("[DAVOR-TEST] Rewarded 500 RP to the member for deliverying the vehicle.")
					RETURN 500
				ENDIF
			ENDIF
		ELSE // We are the boss
			PRINTLN("[DAVOR-TEST] Rewarded 500 RP to the BOSS for deliverying the vehicle.")
			RETURN 500
		ENDIF
	ENDIF
	RETURN 0
ENDFUNC

FUNC BOOL SHOULD_DRAW_MARKERS_ABOVE_CARRIERS()
	RETURN TRUE
ENDFUNC

FUNC BLIP_SPRITE GET_MISSION_ENTITY_OVERRIDE_SPRITE()
	RETURN GET_FM_CONTENT_ENTITY_BLIP_SPRITE(LOCAL_PLAYER_INDEX)
ENDFUNC

PROC CUSDEL_SETUP_PLACEMENT_DATA()
	data.MissionEntity.iCount = 1
	data.MissionEntity.MissionEntities[0].iCarrierVehicle = 0
	data.MissionEntity.MissionEntities[0].model = PROP_DRUG_PACKAGE
	
	INT iSpawnPoint = 0
	WHILE TRUE
		IF GB_IS_PLAYER_MEMBER_OF_A_GANG(LOCAL_PLAYER_INDEX, FALSE)
			GET_AUTO_SHOP_EXTERIOR_SPAWN_POINT_IN_VEHICLE(GET_OWNED_AUTO_SHOP_SIMPLE_INTERIOR(GB_GET_LOCAL_PLAYER_GANG_BOSS()), iSpawnPoint, data.MissionEntity.MissionEntities[0].vCoords, data.MissionEntity.MissionEntities[0].fHeading)
		ELSE
			GET_AUTO_SHOP_EXTERIOR_SPAWN_POINT_IN_VEHICLE(GET_OWNED_AUTO_SHOP_SIMPLE_INTERIOR(LOCAL_PLAYER_INDEX), iSpawnPoint, data.MissionEntity.MissionEntities[0].vCoords, data.MissionEntity.MissionEntities[0].fHeading)
		ENDIF
	
		IF NOT IS_POINT_OK_FOR_NET_ENTITY_CREATION(data.MissionEntity.MissionEntities[0].vCoords)
			IF iSpawnPoint < 3
				iSpawnPoint++
			ELSE
				PRINTLN("[GET_AUTO_SHOP_EXTERIOR_SPAWN_POINT_IN_VEHICLE] - NO VEHICLE SPAWN POINT IN GOOD CONDITION, USING LAST OPTION.")
				BREAKLOOP // If the third point is not good but we run out of options.. spawn it anyways.
			ENDIF
		ELSE
			BREAKLOOP
		ENDIF
	ENDWHILE
	
	data.Vehicle.iCount = 1
	data.Vehicle.Vehicles[0].vCoords = data.MissionEntity.MissionEntities[0].vCoords
	data.Vehicle.Vehicles[0].fHeading = data.MissionEntity.MissionEntities[0].fHeading
	data.Vehicle.Vehicles[0].model = CUSDEL_GET_DELIVERY_MODEL()
	
	VECTOR vPos = FMC_GET_IN_VEHICLE_DROPOFF_LOCATION(CUSDEL_SETUP_DROPOFF(0))
	
	data.Population.Blockers[0].eType = ePOPULATIONBLOCKTYPE_VEHICLE_GEN
	data.Population.Blockers[0].vMin =  GET_AREA_EXTENT(vPos, 55.0)
	data.Population.Blockers[0].vMax =  GET_AREA_EXTENT(vPos, 55.0, TRUE)
	
	data.Population.iCount = 1

	//SET_VEHICLE_DATA_BIT(CUSDEL_VEHICLE_TO_DELIVER_ID, eVEHICLEDATABITSET_FADE_DELETE_ON_END)
	PRINTLN("[DAVOR-TEST] The delivery position is: ", vPos)
	
	data.ModeTimer.iTimeLimit = 30
	data.ModeTimer.eDisplay = eMODETIMERDISPLAYTYPE_NEAR_END
ENDPROC

FUNC INT CUSDEL_GET_NUM_HITS_VALUE()
	INT iTotalBonusDamage
	IF GET_VEHICLE_VALUE_NUM_HITS() >= 1
		iTotalBonusDamage += 400
	ENDIF
	IF GET_VEHICLE_VALUE_NUM_HITS() >= 2
		iTotalBonusDamage += 320
	ENDIF
	IF GET_VEHICLE_VALUE_NUM_HITS() >= 3
		iTotalBonusDamage += 240
	ENDIF
	RETURN iTotalBonusDamage
ENDFUNC

FUNC INT CUSDEL_VEHICLE_OFFER_REDUCTION_COST()
	RETURN 62
ENDFUNC

FUNC BOOL CUSDEL_ENABLE_VEHICLE_VALUE()
	RETURN TRUE
ENDFUNC

FUNC VEHICLE_INDEX CUSDEL_GET_VEHICLE_VALUE_VEHICLE()
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[0].netId)
		RETURN NET_TO_VEH(serverBD.sVehicle[0].netId)
	ENDIF
	RETURN INT_TO_NATIVE(VEHICLE_INDEX, -1)
ENDFUNC

PROC CUSDEL_VEHICLE_CLIENT_MAINTAIN(INT iVehicle, VEHICLE_INDEX vehID, BOOL bDriveable, BOOL bInVehicle, INT iMissionEntityForCarrier)
	UNUSED_PARAMETER(iMissionEntityForCarrier)
	
	IF GET_END_REASON() != eENDREASON_NO_REASON_YET 
		IF IS_CUTSCENE_PLAYING() AND iVehicle = 0
			//PRINTLN("[DAVOR-TEST] Cutscene is active!")
			NETWORK_FADE_OUT_ENTITY(NET_TO_ENT(serverBD.sVehicle[CUSDEL_VEHICLE_TO_DELIVER_ID].netId), TRUE, TRUE)
		ENDIF
	ENDIF
	
	IF GET_END_REASON() = eENDREASON_NO_REASON_YET // No need to handle any of this if the mission is finishing.
		IF iVehicle = 0
		AND bDriveable
		AND (GB_GET_LOCAL_PLAYER_GANG_BOSS() = LOCAL_PLAYER_INDEX
		OR NOT GB_IS_PLAYER_MEMBER_OF_A_GANG(LOCAL_PLAYER_INDEX, FALSE))
			IF NOT bWarpedInVeh
				IF NOT bInVehicle
				AND NOT IS_MISSION_SERVER_BIT_SET(eMISSIONSERVERBITSET_MISSION_WARP_DONE)
					PRINTLN("[DAVOR-TEST] Entering vehicle..")
					TASK_ENTER_VEHICLE(LOCAL_PED_INDEX, vehId, DEFAULT, VS_DRIVER, DEFAULT, ECF_WARP_PED)
					bWarpedInVeh = TRUE
					SET_MISSION_SERVER_BIT(eMISSIONSERVERBITSET_MISSION_WARP_DONE)
				ENDIF
			ELSE
				IF NOT bFadeInAfterWarp
					IF NOT IS_LOCAL_PLAYER_IN_ANY_SIMPLE_INTERIOR()
					AND NOT IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
						IF IS_SCREEN_FADED_OUT()
							IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(TimerForPlayerWarps, TimeBeforePlayerWarpsIntoVehicle) 
								FADE_SCREEN_IN()
								bFadeInAfterWarp = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF	
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC FLOAT CUSDEL_GET_VEHICLE_DAMAGE_SCALE(INT iVehicle)
	UNUSED_PARAMETER(iVehicle)
	RETURN 1.0
ENDFUNC

PROC CUSDEL_SET_VEHICLE_ATTRIBUTES(INT iVehicle, VEHICLE_INDEX vehId)
	IF iVehicle = 0
		SET_VEHICLE_WEAPON_DAMAGE_SCALE(vehId, 0.2)
	ENDIF
ENDPROC

FUNC BOOL CUSDEL_SET_VEHICLE_MODS(INT iVehicle, VEHICLE_SETUP_STRUCT_MP& sData)
	IF iVehicle = 0
		sData = GET_AUTO_SHOP_CLIENT_VEHICLE_SETUP_STRUCT(GET_LOCAL_PLAYER_AUTO_SHOP_CLIENT_VEHICLE_DELIVERY_SLOT())
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC STRING CUSDEL_HELP_TEXT(INT iHelpText)
	SWITCH INT_TO_ENUM(eCUSDEL_HELP_TEXT, iHelpText)
		CASE eCUSDEL_HELPTEXT_DELIVERY
			RETURN "TR_HT_ASDEL"
		BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC BOOL CUSDEL_HELP_TEXT_TRIGGER(INT iHelpText)
	SWITCH INT_TO_ENUM(eCUSDEL_HELP_TEXT, iHelpText)
		CASE eCUSDEL_HELPTEXT_DELIVERY
			IF IS_GENERIC_BIT_SET(eGENERICBITSET_COMPLETED_JOB_START_BIG_MESSAGE)
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC STRING CUSDEL_TICKER_EVENT_LOCAL(MISSION_TICKER_DATA &sTickerData)
	IF sTickerData.iInt1 != -1
		SWITCH INT_TO_ENUM(eCUSDEL_TICKER_EVENT, sTickerData.iInt1)
			CASE eCUSDEL_TICKEREVENT_DELIVERY_FINISHED	RETURN "TR_TIC_CUSDELL"		
		ENDSWITCH
	ENDIF
	
	RETURN ""
ENDFUNC

FUNC STRING CUSDEL_TICKER_EVENT_REMOTE(MISSION_TICKER_DATA &sTickerData)
	IF sTickerData.iInt1 != -1
		SWITCH INT_TO_ENUM(eCUSDEL_TICKER_EVENT, sTickerData.iInt1)
			CASE eCUSDEL_TICKEREVENT_DELIVERY_FINISHED	RETURN "TR_TIC_CUSDELR"		
		ENDSWITCH
	ENDIF
	
	RETURN ""
ENDFUNC

PROC CUSDEL_ON_MISSION_ENTITY_DELIVERED(INT iMissionEntity, BOOL bLocalDelivered)
	UNUSED_PARAMETER(iMissionEntity)

	IF bLocalDelivered
		CUSDEL_ADD_MISSION_TICKER(eCUSDEL_TICKEREVENT_DELIVERY_FINISHED)
	ENDIF
ENDPROC

FUNC STRING CUSDEL_MISSION_ENTITY_BLIP_NAME(INT iMissionEntity)
	UNUSED_PARAMETER(iMissionEntity)
	
	RETURN "TR_BLIP_CVEH"
ENDFUNC

FUNC BOOL CUSDEL_MISSION_HOLD_END()
	
	IF NOT IS_GENERIC_BIT_SET(eGENERICBITSET_I_WON)
		RETURN FALSE
	ENDIF
	
	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[CUSDEL_VEHICLE_TO_DELIVER_ID].netId)
		RETURN FALSE
	ENDIF
	
	IF NOT IS_ENTITY_VISIBLE(NET_TO_ENT(serverBD.sVehicle[CUSDEL_VEHICLE_TO_DELIVER_ID].netId))
		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[CUSDEL_VEHICLE_TO_DELIVER_ID].netId)
			//PRINTLN("[DAVOR-TEST] Waiting fot the timer..")
			IF HAS_NET_TIMER_EXPIRED(TimerForPlayerWarps, 2000)
				//PRINTLN("[DAVOR-TEST] Timer ended..")
				IF NOT IS_PED_IN_VEHICLE(LOCAL_PED_INDEX, NET_TO_VEH(serverBD.sVehicle[CUSDEL_VEHICLE_TO_DELIVER_ID].netId))
					PRINTLN("[DAVOR-TEST] Cleaning up the vehicle..")
					DELETE_NET_ID(serverBD.sVehicle[CUSDEL_VEHICLE_TO_DELIVER_ID].netId)
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

/////////////////////////
///    TEXT MESSAGES  ///
/////////////////////////

FUNC enumCharacterList CUSDEL_TEXT_MESSAGES_CHARACTER(INT iText)
	UNUSED_PARAMETER(iText)
	RETURN CHAR_SESSANTA
ENDFUNC

FUNC BOOL CUSDEL_TEXT_MESSAGES_SHOULD_SEND(INT iText)
	SWITCH INT_TO_ENUM(eCUSDEL_TEXT_MESSAGES, iText)
		CASE eCUSDEL_TEXTMESSAGES_PERFECTLY_DELIVERED
			
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC STRING CUSDEL_TEXT_MESSAGES_LABEL(INT iText)
	INT iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
	SWITCH INT_TO_ENUM(eCUSDEL_TEXT_MESSAGES, iText)
		CASE eCUSDEL_TEXTMESSAGES_PERFECTLY_DELIVERED
			IF iRand = 0
				RETURN "TR_TXT_CUSDEL1"
			ELSE
				RETURN "TR_TXT_CUSDEL2"
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

PROC END_OF_MISSION_TEXT_MESSAGES()
	PRINTLN("[DAVOR-TEST] Vehicle Damage Amount: ", iVehicleDamagesAmount)
	PRINTLN("[DAVOR-TEST] Vehicle Bonus Amount: ", serverBD.sMissionData.iVehicleBonusAmount )
	

	SET_BIT(g_sTunerFlow.BitSet, ciTUNERGENERALBITSET_COMPLETED_AUTO_SHOP_DELIVERY)

	IF iVehicleDamagesAmount = 0
		IF serverBD.sMissionData.iVehicleBonusAmount = 0 // If bonus = 0, vehicle specced as asked.
			g_sTunerFlow.Variation = 0
		ELIF serverBD.sMissionData.iVehicleBonusAmount > 0
			g_sTunerFlow.Variation = 1
		ELSE
			g_sTunerFlow.Variation = 2
		ENDIF
	
	ELIF iVehicleDamagesAmount > 0 AND CUSDEL_IS_VEHICLE_IN_GOOD_CONDITION() // Little damages.
		IF serverBD.sMissionData.iVehicleBonusAmount = 0
			g_sTunerFlow.Variation = 3
		ELIF serverBD.sMissionData.iVehicleBonusAmount > 0
			g_sTunerFlow.Variation = 4
		ELSE
			g_sTunerFlow.Variation = 5
		ENDIF
		
	ELIF iVehicleDamagesAmount > 0 AND NOT CUSDEL_IS_VEHICLE_IN_GOOD_CONDITION() // A lot of damages.
		IF serverBD.sMissionData.iVehicleBonusAmount = 0
			g_sTunerFlow.Variation = 6
		ELIF serverBD.sMissionData.iVehicleBonusAmount > 0
			g_sTunerFlow.Variation = 7
		ELSE
			g_sTunerFlow.Variation = 8
		ENDIF
	ENDIF
	
	PRINTLN("[DAVOR-TEST] Final variation: ", g_sTunerFlow.Variation)
	
	IF g_sTunerFlow.Variation != -1
		g_sTunerFlow.SendJustCompletedText = TRUE
	ENDIF

ENDPROC

PROC CUSDEL_SCRIPT_CLEANUP()
	CUSDEL_SETTING_END_SHARD()
	
	IF IS_GENERIC_BIT_SET(eGENERICBITSET_I_WON)
		IF NOT GB_IS_PLAYER_MEMBER_OF_A_GANG(LOCAL_PLAYER_INDEX, FALSE) // Only the BOSS (Owner of the Auto Shop) gets the reward.
			END_OF_MISSION_TEXT_MESSAGES()
			CUSDEL_SETUP_SPECIAL_REWARDS()
			AWARD_CAR_CLUB_REP(EARN_REP_TYPE_AUTO_SHOP_CUSTOMER_DELIVERY)
		ENDIF
	ENDIF
	
	missionEndShardData.eAutoShopResult = CONTRABAND_TRANSACTION_STATE_DEFAULT	
ENDPROC

FUNC BOOL CUSDEL_CLIENT_INIT()
	//FADE_SCREEN_OUT()
	RETURN TRUE
ENDFUNC

/// PURPOSE: Use to set up the logic pointers for your mission
///    
PROC SETUP_MISSION_LOGIC()
	logic.Data.Setup = &CUSDEL_SETUP_PLACEMENT_DATA
	
	logic.Init.Client = &CUSDEL_CLIENT_INIT
	
	logic.Delivery.Dropoff.Setup = &CUSDEL_SETUP_DROPOFF
	
	logic.Cleanup.ScriptEnd = &CUSDEL_SCRIPT_CLEANUP
	
	logic.StateMachine.SetupClientStates = &CUSDEL_SETUP_CLIENT_MODE_STATES
	logic.StateMachine.SetupStates = &CUSDEL_SETUP_MODE_STATES
	
	logic.VehicleValue.BaseValue = &CUSDEL_GET_VEHICLE_POTENTIAL_PAY
	logic.VehicleValue.Enable = &CUSDEL_ENABLE_VEHICLE_VALUE
	logic.VehicleValue.Vehicle = &CUSDEL_GET_VEHICLE_VALUE_VEHICLE
	logic.VehicleValue.NumHitsCost = &CUSDEL_GET_NUM_HITS_VALUE
	logic.VehicleValue.OfferReductionCost = &CUSDEL_VEHICLE_OFFER_REDUCTION_COST
	
	logic.Vehicle.Client = &CUSDEL_VEHICLE_CLIENT_MAINTAIN
	logic.Vehicle.DamageScale = &CUSDEL_GET_VEHICLE_DAMAGE_SCALE
	logic.Vehicle.Attributes = &CUSDEL_SET_VEHICLE_ATTRIBUTES
	logic.Vehicle.Mods = &CUSDEL_SET_VEHICLE_MODS
	
	logic.HelpText.Text = &CUSDEL_HELP_TEXT
	logic.HelpText.Trigger = &CUSDEL_HELP_TEXT_TRIGGER
	
	logic.Hud.Tickers.CustomStringLocal = &CUSDEL_TICKER_EVENT_LOCAL
	logic.Hud.Tickers.CustomStringRemote = &CUSDEL_TICKER_EVENT_REMOTE
	
	logic.MissionEntity.OnDelivery = &CUSDEL_ON_MISSION_ENTITY_DELIVERED
	logic.MissionEntity.Blip.Name = &CUSDEL_MISSION_ENTITY_BLIP_NAME
	
	logic.Mission.HoldEnd = &CUSDEL_MISSION_HOLD_END
	
	logic.Reward.OnDeliveryCash = &CUSDEL_ON_DELIVERY_CASH
	logic.Reward.OnDeliveryRp = &CUSDEL_ON_DELIVERY_RP
	
	// Text Messages
	//logic.TextMessages.Character = &CUSDEL_TEXT_MESSAGES_CHARACTER
	//logic.TextMessages.ShouldSend = &CUSDEL_TEXT_MESSAGES_SHOULD_SEND
	//logic.TextMessages.Label = &CUSDEL_TEXT_MESSAGES_LABEL
	
ENDPROC
#ENDIF
