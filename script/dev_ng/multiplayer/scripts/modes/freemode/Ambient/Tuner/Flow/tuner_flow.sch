

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_camera.sch"
USING "commands_pad.sch"
USING "commands_script.sch"
USING "flow_public_core_override.sch"
USING "net_wait_zero.sch"
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_event.sch"
USING "net_include.sch"
USING "net_events.sch"
USING "net_spawn.sch"
USING "net_ambience.sch"
USING "net_GameDir.sch"
USING "net_gang_boss.sch"


#IF FEATURE_TUNER

#IF IS_DEBUG_BUILD
PROC MAINTAIN_TUNER_FLOW_DEBUG(TUNER_FLOW_DATA &sData)
	IF sData.debug_ResetFlow
		IF DOES_PLAYER_OWN_A_AUTO_SHOP(PLAYER_ID())
			DEBUG_REMOVE_AUTO_SHOP()
			SET_PLAYER_PURCHASED_CAR_CLUB_MEMBERSHIP(FALSE)
			BROADCAST_PLAYER_PURCHASED_AUTO_SHOP()
		ENDIF
			
		TUNER_FLOW_DATA sNewData
		sData = sNewData
					
		SET_CAR_MEET_INTRO_SCENE_BEEN_VIEWED(FALSE)
		SET_LOCAL_PLAYER_HAS_VIEWED_AUTO_SHOP_SETUP_CUTSCENE(FALSE)
		SET_LOCAL_PLAYER_HAS_VIEWED_AUTO_SHOP_FINISHED_SETUP_CUTSCENE(FALSE)
			
		RESET_TUNER_STATS()
			
		REQUEST_SAVE(SSR_REASON_DEBUG, STAT_SAVETYPE_START_MATCH)
	ENDIF
ENDPROC
#ENDIF

FUNC BOOL IS_SAFE_FOR_TUNER_FLOW_MESSAGES(FREEMODE_FLOW_CACHE_DATA &sData)
	IF sData.bNETWORK_IS_ACTIVITY_SESSION
	OR sData.bNETWORK_IS_PLAYER_IN_MP_CUTSCENE
	OR NOT sData.bIS_PLAYER_PLAYING
	OR sData.bIS_PLAYER_IN_CUTSCENE
	OR sData.bIS_PLAYER_IN_CORONA
	OR sData.bBlockedMissionType
	OR mpglobalsambience.bIsInImportExportCar
	OR IS_LOCAL_PLAYER_AN_ANIMAL()
	OR NOT sData.bIS_SKYSWOOP_AT_GROUND
	OR !sData.bbiTrigTut_CompletedInitalAmbientTut
	OR sData.bIS_BROWSER_OPEN
	OR sData.bFLOW_MESSAGES_SUPPRESSED_IN_CURRENT_INTERIOR
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

PROC MAINTAIN_SAFE_TO_DISPLAY_TUNER_FLOW_NOTIFICATIONS(TUNER_FLOW_DATA &sData, BOOL bOkayForFreemodeHelp, FREEMODE_FLOW_CACHE_DATA &sCacheData)
	IF bOkayForFreemodeHelp
	AND NOT sCacheData.bPLAYER_PERMANENT_TO_GANG_BOSS_MISSION
	AND NOT sCacheData.bIS_PLAYER_PERMANENT_PARTICIPANT_IN_FM_EVENT
	AND NOT sCacheData.bNETWORK_IS_ACTIVITY_SESSION
	AND NOT sCacheData.bIS_PLAYER_IN_NIGHTCLUB
	AND sCacheData.bIS_SCREEN_FADED_IN
	AND NOT sCacheData.bIS_SCREEN_FADING_IN 
	AND sCacheData.bIS_GAMEPLAY_CAM_RENDERING
	AND IS_SAFE_FOR_TUNER_FLOW_MESSAGES(sCacheData)
	AND NOT sCacheData.bIS_PLAYER_IN_CUTSCENE 
		IF NOT IS_BIT_SET(sData.BitSet, ciTUNER_FLOW_BITSET__SAFE_TO_DISPLAY)
			SET_BIT(sData.BitSet, ciTUNER_FLOW_BITSET__SAFE_TO_DISPLAY)
			PRINTLN("[TUNER_FLOW] | MAINTAIN_SAFE_TO_DISPLAY_TUNER_FLOW_NOTIFICATIONS - Safe")
		ENDIF
	ELSE
		IF IS_BIT_SET(sData.BitSet, ciTUNER_FLOW_BITSET__SAFE_TO_DISPLAY)
			CLEAR_BIT(sData.BitSet, ciTUNER_FLOW_BITSET__SAFE_TO_DISPLAY)
			PRINTLN("[TUNER_FLOW] | MAINTAIN_SAFE_TO_DISPLAY_TUNER_FLOW_NOTIFICATIONS - Not Safe")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_PROCESS_TUNER_FLOW()
	IF NETWORK_IS_ACTIVITY_SESSION()
	OR IS_PLAYER_IN_CORONA()
	OR GB_IS_LOCAL_PLAYER_PERMANENT_TO_GANG_BOSS_MISSION()
	OR FM_EVENT_IS_PLAYER_CRITICAL_TO_FM_EVENT(PLAYER_ID())
	OR IS_LOCAL_PLAYER_AN_ANIMAL()
	#IF FEATURE_GEN9_EXCLUSIVE
	OR IS_PLAYER_ON_MP_INTRO()
	#ENDIF
	OR IS_PLAYER_TEST_DRIVING_A_VEHICLE(PLAYER_ID())
		RETURN FALSE
	ENDIF

	RETURN TRUE
ENDFUNC

PROC CLEANUP_TUNER_FLOW(TUNER_FLOW_DATA &sData)
	IF sData.Stage != TFS_INIT_VARS
		sData.Stage = TFS_INIT_VARS
	ENDIF
	IF HAS_NET_TIMER_STARTED(sData.Timer)
		RESET_NET_TIMER(sData.Timer)
	ENDIF
ENDPROC

FUNC STRING GET_RANDOM_TUNER_POST_ROBBERY_TEXT_STRING()
	SWITCH GET_RANDOM_INT_IN_RANGE(0, 3)
		CASE 0	RETURN "TFLOW_PROB_T0"
		CASE 1	RETURN "TFLOW_PROB_T1"
		CASE 2	RETURN "TFLOW_PROB_T2"
	ENDSWITCH
	RETURN ""
ENDFUNC

PROC MAINTAIN_TUNER_FLOW_MESSAGES(TUNER_FLOW_DATA &sData)
	INT iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_TUNER_GEN_BS)
	
	IF NOT HAS_PLAYER_PURCHASED_CAR_CLUB_MEMBERSHIP(PLAYER_ID())
	AND IS_PLAYER_IN_CAR_MEET_PROPERTY(PLAYER_ID())
		IF NOT IS_BIT_SET(sData.BitSet, ciTUNER_FLOW_BITSET__SHOWN_CAR_MEET_MEMBERSHIP_HELP)
			IF IS_BIT_SET(sData.BitSet, ciTUNER_FLOW_BITSET__SHOWN_AUTOSHOP_HELP)
			OR DOES_PLAYER_OWN_A_AUTO_SHOP(PLAYER_ID())
				IF NOT HAS_NET_TIMER_STARTED(sData.Timer)
					START_NET_TIMER(sData.Timer)
					PRINTLN("[TUNER_FLOW] | MAINTAIN_TUNER_FLOW_HELP - Started timer for car meet help")
				ELSE
					IF HAS_NET_TIMER_EXPIRED(sData.Timer, PICK_INT(DOES_PLAYER_OWN_A_AUTO_SHOP(PLAYER_ID()), 60000, 15000))
					AND IS_BIT_SET(sData.BitSet, ciTUNER_FLOW_BITSET__SAFE_TO_DISPLAY)
						PRINT_HELP("TFLOW_CMEET_H")
						IF IS_PLAYER_IN_CAR_MEET_PROPERTY(PLAYER_ID())
							SET_BIT(g_SimpleInteriorData.iNinthBS, BS9_SIMPLE_INTERIOR_CAR_MEET_FLASH_MIMI_BLIP)
						ENDIF
						SET_BIT(sData.BitSet, ciTUNER_FLOW_BITSET__SHOWN_CAR_MEET_MEMBERSHIP_HELP)
						PRINTLN("[TUNER_FLOW] | MAINTAIN_TUNER_FLOW_HELP - car meet help done.")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(sData.BitSet, ciTUNER_FLOW_BITSET__SHOWN_AUTOSHOP_BOUGHT_HELP_1)
	AND NOT IS_BIT_SET(sData.BitSet, ciTUNER_FLOW_BITSET__SHOWN_AUTOSHOP_BOUGHT_HELP_2)
		IF IS_BIT_SET(sData.BitSet, ciTUNER_FLOW_BITSET__SAFE_TO_DISPLAY)
			PRINT_HELP("TFLOW_SHOP_H2")
			SET_BIT(sData.BitSet, ciTUNER_FLOW_BITSET__SHOWN_AUTOSHOP_BOUGHT_HELP_2)
			PRINTLN("[TUNER_FLOW] | MAINTAIN_TUNER_FLOW_HELP - Autoshop bought help 2 done.")
		ENDIF
	ENDIF
	
	IF HAS_PLAYER_COMPLETED_TUNER_ROBBERY_PREP_ONE(PLAYER_ID())
	AND HAS_PLAYER_COMPLETED_TUNER_ROBBERY_PREP_TWO(PLAYER_ID())
	AND IS_PLAYER_IN_AUTO_SHOP_THEY_OWN(PLAYER_ID())
		IF NOT IS_BIT_SET(iStatInt, ciTUNERGENERALBITSET_SHOWN_PREPS_COMPLETED_HELP)
		AND IS_BIT_SET(sData.BitSet, ciTUNER_FLOW_BITSET__SAFE_TO_DISPLAY)
			PRINT_HELP("TFLOW_PREP_H")
			PRINTLN("[TUNER_FLOW] | MAINTAIN_TUNER_FLOW_HELP - preps completed help done.")
			SET_BIT(iStatInt, ciTUNERGENERALBITSET_SHOWN_PREPS_COMPLETED_HELP)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_TUNER_GEN_BS, iStatInt)
		ENDIF
	ENDIF
	
	IF IS_PLAYER_IN_AUTO_SHOP_THEY_OWN(PLAYER_ID())
	AND IS_BIT_SET(sData.BitSet, ciTUNER_FLOW_BITSET__JOBS_UPDATED_CORRESPONDANCE_DONE)
		IF NOT IS_BIT_SET(sData.BitSet, ciTUNER_FLOW_BITSET__JOBS_UPDATED_HELP_DONE)
		AND IS_BIT_SET(sData.BitSet, ciTUNER_FLOW_BITSET__SAFE_TO_DISPLAY)
			PRINT_HELP("TFLOW_RFRSH_H2")
			PRINTLN("[TUNER_FLOW] | MAINTAIN_TUNER_FLOW_HELP - new jobs help done.")
			SET_BIT(sData.BitSet, ciTUNER_FLOW_BITSET__JOBS_UPDATED_HELP_DONE)
		ENDIF
	ENDIF
	
	IF sData.SendJustCompletedText
		IF IS_BIT_SET(sData.BitSet, ciTUNERGENERALBITSET_COMPLETED_AUTO_SHOP_DELIVERY) // Finished Auto Shop Delivery Mission.
			IF NOT HAS_NET_TIMER_STARTED(sData.Timer)
				START_NET_TIMER(sData.Timer)
				PRINTLN("[TUNER_FLOW] | MAINTAIN_TUNER_FLOW_HELP - Started timer for post AUTO SHOP text")
			ELSE
				IF HAS_NET_TIMER_EXPIRED(sData.Timer, 15000)
					STRING sLabel
					INT iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
					
					SWITCH sData.Variation
					   CASE 0
							IF iRand = 0
								sLabel = "TR_TXT_CUSDEL1"
							ELSE
								sLabel = "TR_TXT_CUSDEL2"
							ENDIF
						BREAK
						CASE 1
							IF iRand = 0
								sLabel = "TR_TXT_CUSDEL3"
							ELSE
								sLabel = "TR_TXT_CUSDEL4"
							ENDIF
						BREAK
						CASE 2
							IF iRand = 0
								sLabel = "TR_TXT_CUSDEL5"
							ELSE
								sLabel = "TR_TXT_CUSDEL6"
							ENDIF
						BREAK
						CASE 3
							IF iRand = 0
								sLabel = "TR_TXT_CUSDEL7"
							ELSE
								sLabel = "TR_TXT_CUSDEL8"
							ENDIF
						BREAK
						CASE 4
							IF iRand = 0
								sLabel = "TR_TXT_CUSDEL9"
							ELSE
								sLabel = "TR_TXT_CUSDEL10"
							ENDIF
						BREAK
						CASE 5
							IF iRand = 0
								sLabel = "TR_TXT_CUSDEL11"
							ELSE
								sLabel = "TR_TXT_CUSDEL12"
							ENDIF
						BREAK
						CASE 6
							IF iRand = 0
								sLabel = "TR_TXT_CUSDEL13"
							ELSE
								sLabel = "TR_TXT_CUSDEL14"
							ENDIF
						BREAK
						CASE 7
							IF iRand = 0
								sLabel = "TR_TXT_CUSDEL15"
							ELSE
								sLabel = "TR_TXT_CUSDEL16"
							ENDIF
						BREAK
						CASE 8
							IF iRand = 0
								sLabel = "TR_TXT_CUSDEL17"
							ELSE
								sLabel = "TR_TXT_CUSDEL18"
							ENDIF
						BREAK
						
					ENDSWITCH
					
					
					IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_SESSANTA, sLabel, TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED)
						sData.SendJustCompletedText = FALSE
						sData.Variation = -1
						CLEAR_BIT(sData.BitSet, ciTUNERGENERALBITSET_COMPLETED_AUTO_SHOP_DELIVERY)
						PRINTLN("[TUNER_FLOW] | MAINTAIN_TUNER_FLOW_HELP - Post auto shop delivery mission text sent.")
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF NOT HAS_NET_TIMER_STARTED(sData.Timer)
				START_NET_TIMER(sData.Timer)
				PRINTLN("[TUNER_FLOW] | MAINTAIN_TUNER_FLOW_HELP - Started timer for post robbery text")
			ELSE
				IF HAS_NET_TIMER_EXPIRED(sData.Timer, FLOW_TIMINGS__TUNER_POST_ROBBERY_CONTACT_DELAY)
				AND IS_BIT_SET(sData.BitSet, ciTUNER_FLOW_BITSET__SAFE_TO_DISPLAY)
					IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_KDJ, GET_RANDOM_TUNER_POST_ROBBERY_TEXT_STRING(), TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED)
						sData.SendJustCompletedText = FALSE
						PRINTLN("[TUNER_FLOW] | MAINTAIN_TUNER_FLOW_HELP - Post robbery text sent.")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(sData.BitSet, ciTUNER_FLOW_BITSET__SEND_1K_VEHICLES_TEXT)
	AND NOT IS_BIT_SET(iStatInt, ciTUNERGENERALBITSET_SENT_1K_VEHICLE_TEXT)
		IF NOT HAS_NET_TIMER_STARTED(sData.Timer)
			START_NET_TIMER(sData.Timer)
			PRINTLN("[TUNER_FLOW] | MAINTAIN_TUNER_FLOW_HELP - Started timer for 1k vehicles text")
		ELSE
			IF HAS_NET_TIMER_EXPIRED(sData.Timer, 5000)
			AND IS_BIT_SET(sData.BitSet, ciTUNER_FLOW_BITSET__SAFE_TO_DISPLAY)
				IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_MP_BRUCIE, "TFLOW_VL1KT", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED)
					CLEAR_BIT(sData.BitSet, ciTUNER_FLOW_BITSET__SEND_1K_VEHICLES_TEXT)
					SET_BIT(iStatInt, ciTUNERGENERALBITSET_SENT_1K_VEHICLE_TEXT)
					SET_MP_INT_CHARACTER_STAT(MP_STAT_TUNER_GEN_BS, iStatInt)
					PRINTLN("[TUNER_FLOW] | MAINTAIN_TUNER_FLOW_HELP - 1k vehicles text done.")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(sData.BitSet, ciTUNER_FLOW_BITSET__TRASHED_SETUP_MISSION_CAR)  
		IF IS_BIT_SET(sData.BitSet, ciTUNER_FLOW_BITSET__SAFE_TO_DISPLAY)
		AND NOT IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())
		AND NOT GB_IS_PLAYER_ON_TUNER_ROBBERY_PREP_MISSION(PLAYER_ID())
		AND NOT IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(PLAYER_ID())
		AND NOT IS_SCREEN_FADED_OUT()
			IF NOT HAS_NET_TIMER_STARTED(sData.Timer)
				START_NET_TIMER(sData.Timer)
				PRINTLN("[TUNER_FLOW] | MAINTAIN_TUNER_FLOW_HELP - ciTUNER_FLOW_BITSET__TRASHED_SETUP_MISSION_CAR - Started timer for setup trashed vehicle text")
			ELSE
				IF HAS_NET_TIMER_EXPIRED(sData.Timer, 5000)
					IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_SESSANTA, "TR_TXT_STPTRSH", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED)
						CLEAR_BIT(sData.BitSet, ciTUNER_FLOW_BITSET__TRASHED_SETUP_MISSION_CAR) 
						PRINTLN("[TUNER_FLOW] | MAINTAIN_TUNER_FLOW_HELP - ciTUNER_FLOW_BITSET__TRASHED_SETUP_MISSION_CAR - Post setup trashed vehicle text sent.")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	#IF FEATURE_FIXER
	IF HAS_LOCAL_PLAYER_STARTED_METAL_DETECTOR_COLLECTABLE()
	AND IS_BIT_SET(sData.BitSet, ciTUNER_FLOW_BITSET__SHOW_METAL_DETECTOR_STARTED_HELP)
	AND IS_BIT_SET(sData.BitSet, ciTUNER_FLOW_BITSET__SAFE_TO_DISPLAY)
	AND NOT IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()
		PRINT_HELP("TR_MD_HELP_0")
		PRINTLN("[TUNER_FLOW] | MAINTAIN_TUNER_FLOW_HELP - metal detector help done.")
		CLEAR_BIT(sData.BitSet, ciTUNER_FLOW_BITSET__SHOW_METAL_DETECTOR_STARTED_HELP)
	ENDIF
	#ENDIF
	
ENDPROC

FUNC BOOL IS_PLAYER_IN_RANGE_FOR_TUNER_FLOW_INTRO_EMAIL()
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		RETURN IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<765.398621,-1877.854004,78.255112>>, <<779.884338,-1731.327515,8.538422>>, 150.000000)
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC STRING GET_RANDOM_TUNER_REFRESH_TEXT_STRING()
	SWITCH GET_RANDOM_INT_IN_RANGE(0, 3)
		CASE 0	RETURN "TFLOW_RFRSH_T0"
		CASE 1	RETURN "TFLOW_RFRSH_T1"
		CASE 2	RETURN "TFLOW_RFRSH_T2"
	ENDSWITCH
	RETURN ""
ENDFUNC

PROC MAINTAIN_TUNER_DAILY_REFRESH(TUNER_FLOW_DATA &sData)

	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_TunerReviewPlayRobberies")
		EXIT
	ENDIF
	#ENDIF

	IF NOT DOES_PLAYER_OWN_A_AUTO_SHOP(PLAYER_ID())
		RESET_NET_TIMER(sData.DailyRefreshTimer)
		EXIT
	ENDIF
	
	BOOL bGenerate = FALSE
	INT iTime = GET_MP_INT_CHARACTER_STAT(MP_STAT_TUNER_REFRESH_POSIX)
	
	// Generate initial robberies if we've not yet done so.
	IF DOES_PLAYER_OWN_A_AUTO_SHOP(PLAYER_ID())
	AND NOT HAVE_INITIAL_TUNER_JOBS_BEEN_GENERATED()
		PRINTLN("[TUNER_FLOW] | MAINTAIN_INITIALISE_ROBBERIES - Player owns an autoshop and has not yet generated initial jobs. bGenerate = TRUE")
		bGenerate = TRUE
		SET_INITIAL_TUNER_JOBS_HAVE_BEEN_GENERATED()
	ENDIF

	IF !bGenerate
	AND iTime != 0
	AND HAS_LOCAL_PLAYER_COMPLETED_ANY_TUNER_ROBBERY()
	AND NOT DOES_LOCAL_PLAYER_HAVE_ACTIVE_TUNER_ROBBERY()
	AND NOT IS_PLAYER_IN_AUTO_SHOP_THEY_OWN(PLAYER_ID())
		IF GET_CLOUD_TIME_AS_INT() >= iTime
			IF HAS_NET_TIMER_EXPIRED(sData.DailyRefreshTimer, 30000)
			AND IS_BIT_SET(sData.BitSet, ciTUNER_FLOW_BITSET__SAFE_TO_DISPLAY)
				RESET_NET_TIMER(sData.DailyRefreshTimer)
				bGenerate = TRUE
				PRINTLN("[TUNER] | MAINTAIN_TUNER_DAILY_REFRESH - Daily refresh time has passed, need to generate new jobs. bGenerate = TRUE")
			
				IF NOT IS_BIT_SET(sData.BitSet, ciTUNER_FLOW_BITSET__JOBS_UPDATED_CORRESPONDANCE_DONE)
					INT iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_TUNER_GEN_BS)
					BOOL bSendText = TRUE
					IF NOT IS_BIT_SET(iStatInt, ciTUNERGENERALBITSET_DONE_JOB_UPDATE_CALL)
						SET_BIT(sData.BitSet, ciTUNER_FLOW_BITSET__DO_JOBS_UPDATED_CALL)
						PRINTLN("[TUNER] | MAINTAIN_TUNER_DAILY_REFRESH - ciTUNER_FLOW_BITSET__DO_JOBS_UPDATED_CALL set")
						bSendText = FALSE
					ELIF NOT IS_BIT_SET(iStatInt, ciTUNERGENERALBITSET_DONE_JOB_UPDATE_TEXT_1)
						SET_BIT(iStatInt, ciTUNERGENERALBITSET_DONE_JOB_UPDATE_TEXT_1)
						PRINTLN("[TUNER] | MAINTAIN_TUNER_DAILY_REFRESH - ciTUNERGENERALBITSET_DONE_JOB_UPDATE_TEXT_1 set")
					ELIF NOT IS_BIT_SET(iStatInt, ciTUNERGENERALBITSET_DONE_JOB_UPDATE_TEXT_2)
						SET_BIT(iStatInt, ciTUNERGENERALBITSET_DONE_JOB_UPDATE_TEXT_2)
						PRINTLN("[TUNER] | MAINTAIN_TUNER_DAILY_REFRESH - ciTUNERGENERALBITSET_DONE_JOB_UPDATE_TEXT_2 set")
					ELIF NOT IS_BIT_SET(iStatInt, ciTUNERGENERALBITSET_DONE_JOB_UPDATE_TEXT_3)
						SET_BIT(iStatInt, ciTUNERGENERALBITSET_DONE_JOB_UPDATE_TEXT_3)
						PRINTLN("[TUNER] | MAINTAIN_TUNER_DAILY_REFRESH - ciTUNERGENERALBITSET_DONE_JOB_UPDATE_TEXT_3 set")
					ELSE
						PRINT_TICKER("TFLOW_RFRSH_T")
						bSendText = FALSE
					ENDIF
					
					SET_MP_INT_CHARACTER_STAT(MP_STAT_TUNER_GEN_BS, iStatInt)
					SET_BIT(sData.BitSet, ciTUNER_FLOW_BITSET__JOBS_UPDATED_CORRESPONDANCE_DONE)
					
					IF bSendText
						IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_SESSANTA, GET_RANDOM_TUNER_REFRESH_TEXT_STRING(), TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED)
							PRINT_HELP("TFLOW_RFRSH_H1")
							PRINTLN("[TUNER] | MAINTAIN_TUNER_DAILY_REFRESH - Sent daily refresh text")
						ENDIF
					ENDIF
				ELSE
					PRINT_TICKER("TFLOW_RFRSH_T")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bGenerate
		GENERATE_NEW_TUNER_ROBBERIES()
	ENDIF
ENDPROC

PROC MAINTAIN_TUNER_CALL_CHECKS(TUNER_FLOW_DATA &sData)
	
	IF DOES_PLAYER_OWN_A_AUTO_SHOP(PLAYER_ID())
	AND NOT IS_BIT_SET(GET_MP_INT_CHARACTER_STAT(MP_STAT_TUNER_GEN_BS), ciTUNERGENERALBITSET_DONE_CUSTOMER_CAR_INTRO_CALL)
	AND NOT IS_BIT_SET(sData.BitSet, ciTUNER_FLOW_BITSET__DO_CUSTOMER_CARS_INTRO_CALL)
		IF IS_PLAYER_IN_AUTO_SHOP_THEY_OWN(PLAYER_ID())
			SET_BIT(sData.BitSet, ciTUNER_FLOW_BITSET__DO_CUSTOMER_CARS_INTRO_CALL)
			PRINTLN("[TUNER] | MAINTAIN_TUNER_CALL_CHECKS - Player is in their auto shop and hasnt heard the customer car intro call, setting ciTUNER_FLOW_BITSET__DO_CUSTOMER_CARS_INTRO_CALL so it is done when they leave.")
		ENDIF
	ENDIF
	
ENDPROC

FUNC INT GET_TUNER_EMAIL_TIMER()
	RETURN g_sMPTunables.iFLOW_TIMINGS_CAR_MEET_EMAIL_TIMER
ENDFUNC

/// PURPOSE:
///    Main controller for the flow of the Casino Heist
/// PARAMS:
///    sData - 	the struct of data containing information about the flow
///    bOkayForFreemodeHelp - Whether it is safe to display flow messages (help, text messages, phonecalls etc).
PROC MAINTAIN_TUNER_FLOW(TUNER_FLOW_DATA &sData, BOOL bOkayForFreemodeHelp, FREEMODE_FLOW_CACHE_DATA &sCacheData)
	
	MAINTAIN_SAFE_TO_DISPLAY_TUNER_FLOW_NOTIFICATIONS(sData, bOkayForFreemodeHelp, sCacheData)
	MAINTAIN_TUNER_FLOW_MESSAGES(sData)
	
	#IF IS_DEBUG_BUILD
	MAINTAIN_TUNER_FLOW_DEBUG(sData)
	#ENDIF

	IF NOT SHOULD_PROCESS_TUNER_FLOW()
		CLEANUP_TUNER_FLOW(sData)
		EXIT
	ENDIF

	MAINTAIN_TUNER_DAILY_REFRESH(sData)
	MAINTAIN_TUNER_CALL_CHECKS(sData)

	SWITCH sData.Stage
	
		CASE TFS_INIT_VARS
			IF NOT HAS_LOCAL_PLAYER_VIEWED_CAR_MEET_MOCAP()
				IF NOT IS_BIT_SET(sData.BitSet, ciTUNER_FLOW_BITSET__SENT_CAR_MEET_EMAIL)
					sData.Stage = TFS_SEND_CAR_MEET_EMAIL
					PRINTLN("[TUNER_FLOW] | TFS_INIT_VARS - Need to watch car meet mocap, send email.")
				ELSE
					sData.Stage = TFS_WATCH_CAR_MEET_MOCAP
					PRINTLN("[TUNER_FLOW] | TFS_INIT_VARS - Need to watch car meet mocap, already sent email.")
				ENDIF
			ELIF NOT DOES_PLAYER_OWN_A_AUTO_SHOP(PLAYER_ID())
				IF NOT IS_BIT_SET(sData.BitSet, ciTUNER_FLOW_BITSET__SHOWN_AUTOSHOP_HELP)
					sData.Stage = TFS_SHOW_AUTOSHOP_HELP
					PRINTLN("[TUNER_FLOW] | TFS_INIT_VARS - Need to buy auto shop, show help.")
				ELSE
					sData.Stage = TFS_PURCHASE_AUTOSHOP
					PRINTLN("[TUNER_FLOW] | TFS_INIT_VARS - Need to buy auto shop, already shown help.")
				ENDIF
			ELIF NOT HAS_LOCAL_PLAYER_VIEWED_AUTO_SHOP_SETUP_CUTSCENE()
				sData.Stage = TFS_WATCH_AUTOSHOP_MOCAP_ONE
				PRINTLN("[TUNER_FLOW] | TFS_INIT_VARS - Need to watch auto shop mocap")
			ELIF NOT HAS_LOCAL_PLAYER_COMPLETED_AUTO_SHOP_SETUP_MISSION(TRUE)
				sData.Stage = TFS_COMPLETE_SETUP_MISSION
				PRINTLN("[TUNER_FLOW] | TFS_INIT_VARS - Need to complete auto shop setup")
			ELSE
				sData.Stage = TFS_IDLE
				PRINTLN("[TUNER_FLOW] | TFS_INIT_VARS - Flow done, move to idle")
			ENDIF
		BREAK
		
		CASE TFS_SEND_CAR_MEET_EMAIL
			IF NOT IS_BIT_SET(sData.BitSet, ciTUNER_FLOW_BITSET__SENT_CAR_MEET_EMAIL)
				IF NOT HAS_NET_TIMER_STARTED(sData.Timer)
					START_NET_TIMER(sData.Timer)
					PRINTLN("[TUNER_FLOW] | TFS_SEND_CAR_MEET_EMAIL - Started time for the email")
				ELSE
					IF HAS_NET_TIMER_EXPIRED(sData.Timer, GET_TUNER_EMAIL_TIMER())
					OR IS_PLAYER_IN_RANGE_FOR_TUNER_FLOW_INTRO_EMAIL()
						IF IS_BIT_SET(sData.BitSet, ciTUNER_FLOW_BITSET__SAFE_TO_DISPLAY)
							IF SEND_EMAIL_MESSAGE_TO_CURRENT_PLAYER(CHAR_LS_CAR_MEET_MPEMAIL, "TFLOW_MAIL", EMAIL_UNLOCKED, EMAIL_NOT_CRITICAL, EMAIL_AUTO_UNLOCK_AFTER_READ, MPE_NO_REPLY_REQUIRED)
								PRINT_HELP("TFLOW_MAIL_H")
								SET_SIMPLE_INTERIOR_GLOBAL_BIT(SI_BS_FlashBlips, SIMPLE_INTERIOR_CAR_MEET)
								FORCE_SIMPLE_INTERIOR_BLIPS_UPDATE()
								RESET_NET_TIMER(sData.Timer)
								SET_BIT(sData.BitSet, ciTUNER_FLOW_BITSET__SENT_CAR_MEET_EMAIL)
								PRINTLN("[TUNER_FLOW] | TFS_SEND_CAR_MEET_EMAIL - Email sent")
								sData.Stage = TFS_WATCH_CAR_MEET_MOCAP
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				sData.Stage = TFS_WATCH_CAR_MEET_MOCAP
				PRINTLN("[TUNER_FLOW] | TFS_SEND_CAR_MEET_EMAIL - Email already sent")
			ENDIF
		BREAK
		
		CASE TFS_WATCH_CAR_MEET_MOCAP
			IF HAS_LOCAL_PLAYER_VIEWED_CAR_MEET_MOCAP()
				SET_BIT(sData.BitSet, ciTUNER_FLOW_BITSET__CAR_MEET_MOCAP_DONE_THIS_BOOT)
				sData.Stage = TFS_SHOW_AUTOSHOP_HELP
				PRINTLN("[TUNER_FLOW] | TFS_WATCH_CAR_MEET_MOCAP - Car meet mocap viewed.")
			ENDIF
		BREAK
		
		CASE TFS_SHOW_AUTOSHOP_HELP
			IF NOT IS_BIT_SET(sData.BitSet, ciTUNER_FLOW_BITSET__SHOWN_AUTOSHOP_HELP)
				IF IS_BIT_SET(sData.BitSet, ciTUNER_FLOW_BITSET__CAR_MEET_MOCAP_DONE_THIS_BOOT)
					IF IS_BIT_SET(sData.BitSet, ciTUNER_FLOW_BITSET__SAFE_TO_DISPLAY)
						PRINT_HELP("TFLOW_ASHOP_H")
						SET_BIT(sData.BitSet, ciTUNER_FLOW_BITSET__SHOWN_AUTOSHOP_HELP)
						RESET_NET_TIMER(sData.Timer)
						sData.Stage = TFS_PURCHASE_AUTOSHOP
						PRINTLN("[TUNER_FLOW] | TFS_SHOW_AUTOSHOP_HELP - Autoshop help done.")
					ENDIF
				ELSE
					IF NOT HAS_NET_TIMER_STARTED(sData.Timer)
						START_NET_TIMER(sData.Timer)
						PRINTLN("[TUNER_FLOW] | TFS_SHOW_AUTOSHOP_HELP - Started timer for auto shop text")
					ELSE
						IF HAS_NET_TIMER_EXPIRED(sData.Timer, GET_TUNER_EMAIL_TIMER())
							IF IS_BIT_SET(sData.BitSet, ciTUNER_FLOW_BITSET__SAFE_TO_DISPLAY)
								IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_KDJ, "TFLOW_ASHOP_T", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED)
									PRINT_HELP("TFLOW_ASHOP_H")
									RESET_NET_TIMER(sData.Timer)
									SET_BIT(sData.BitSet, ciTUNER_FLOW_BITSET__SHOWN_AUTOSHOP_HELP)
									PRINTLN("[TUNER_FLOW] | TFS_SHOW_AUTOSHOP_HELP - Auto shop text sent")
									sData.Stage = TFS_PURCHASE_AUTOSHOP
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				sData.Stage = TFS_PURCHASE_AUTOSHOP
				PRINTLN("[TUNER_FLOW] | TFS_SHOW_AUTOSHOP_HELP - Autoshop help already done.")
			ENDIF
		BREAK
		
		CASE TFS_PURCHASE_AUTOSHOP			
			IF DOES_PLAYER_OWN_A_AUTO_SHOP(PLAYER_ID())
				sData.Stage = TFS_WATCH_AUTOSHOP_MOCAP_ONE
				PRINTLN("[TUNER_FLOW] | TFS_PURCHASE_AUTOSHOP - Player owns an autoshop")
			ENDIF
		BREAK
		
		CASE TFS_WATCH_AUTOSHOP_MOCAP_ONE
			IF HAS_LOCAL_PLAYER_VIEWED_AUTO_SHOP_SETUP_CUTSCENE()
				sData.Stage = TFS_COMPLETE_SETUP_MISSION
				PRINTLN("[TUNER_FLOW] | TFS_WATCH_AUTOSHOP_MOCAP_ONE - Player has viewed setup mocap")
			ELSE
				IF NOT IS_PLAYER_IN_AUTO_SHOP_THEY_OWN(PLAYER_ID())
				AND NOT IS_BIT_SET(sData.BitSet, ciTUNER_FLOW_BITSET__SHOWN_AUTOSHOP_HELP)
					IF NOT HAS_NET_TIMER_STARTED(sData.Timer)
						START_NET_TIMER(sData.Timer)
						PRINTLN("[TUNER_FLOW] | TFS_WATCH_AUTOSHOP_MOCAP_ONE - Started timer for auto shop visit reminder")
					ELSE
						IF HAS_NET_TIMER_EXPIRED(sData.Timer, GET_TUNER_EMAIL_TIMER())
							IF IS_BIT_SET(sData.BitSet, ciTUNER_FLOW_BITSET__SAFE_TO_DISPLAY)
								IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_KDJ, "TFLOW_ASHOP_R", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED)
									RESET_NET_TIMER(sData.Timer)
									SET_BIT(sData.BitSet, ciTUNER_FLOW_BITSET__SHOWN_AUTOSHOP_HELP)
									PRINTLN("[TUNER_FLOW] | TFS_WATCH_AUTOSHOP_MOCAP_ONE - Auto shop visit text sent")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE TFS_COMPLETE_SETUP_MISSION
			IF HAS_LOCAL_PLAYER_COMPLETED_AUTO_SHOP_SETUP_MISSION(TRUE)
				// Set stats for which robberies are available here
				sData.Stage = TFS_WATCH_AUTOSHOP_MOCAP_TWO
				PRINTLN("[TUNER_FLOW] | TFS_COMPLETE_SETUP_MISSION - Setup mission completed.")
			ENDIF
		BREAK
		
		CASE TFS_WATCH_AUTOSHOP_MOCAP_TWO
			// No check for this at present.
		BREAK
		
	ENDSWITCH
ENDPROC

#ENDIF
