
USING "globals.sch"
USING "rage_builtins.sch"
USING "fm_content_includes.sch"

//----------------------
//	CONSTANTS
//----------------------

CONST_INT ENABLE_VEHICLE_VALUE 		1
CONST_INT MAX_NUM_HELP_TEXTS		1
CONST_INT MAX_NUM_POP_BLOCKERS		1

//----------------------
//	ENUMS
//----------------------


//----------------------
//	SERVER VARIABLES
//----------------------

STRUCT MISSION_SPECIFIC_SERVER_DATA
	
	TUNER_CLIENT_DELIVERY_VARIATION eVariation = TCDV_INVALID
	TUNER_CLIENT_DELIVERY_SUBVARIATION eSubvariation = TCDS_INVALID
	
	MODEL_NAMES eDeliveryModel = DUMMY_MODEL_FOR_SCRIPT
	
	INT iVehicleBonusAmount = -1
	INT iServiceCostAmount = -1
ENDSTRUCT

//----------------------
//	LOCAL VARIABLES
//----------------------
INT iEarnValue = -1

//----------------------
//	BITSET WRAPPERS
//----------------------

ENUM eMISSION_SERVER_BITSET
	eMISSIONSERVERBITSET_MISSION_WARP_DONE,
	eMISSIONSERVERBITSET_END
ENDENUM
