
USING "globals.sch"
USING "rage_builtins.sch"

CONST_INT ENABLE_CONTENT	FEATURE_TUNER

#IF ENABLE_CONTENT
USING "variations/fm_content_tuner_robbery_elevator_pass.sch"
USING "variations/fm_content_tuner_robbery_vault_key_codes.sch"
USING "variations/fm_content_tuner_robbery_scope_transporter.sch"
USING "variations/fm_content_tuner_robbery_computer_virus.sch"
USING "variations/fm_content_tuner_robbery_thermal_charges.sch"
USING "variations/fm_content_tuner_robbery_signal_jammers.sch"
USING "variations/fm_content_tuner_robbery_container_manifest.sch"
USING "variations/fm_content_tuner_robbery_train_schedule.sch"
USING "variations/fm_content_tuner_robbery_inside_man.sch"
USING "variations/fm_content_tuner_robbery_stunt_ramp.sch"
USING "variations/fm_content_tuner_robbery_IAA_pass.sch"
USING "variations/fm_content_tuner_robbery_setup.sch"
USING "variations/fm_content_tuner_robbery_sewer_schematics.sch"
USING "variations/fm_content_tuner_robbery_meth_lab_locations.sch"
USING "variations/fm_content_tuner_robbery_meth_tanker.sch"
USING "variations/fm_content_tuner_robbery_locate_bunker.sch"
USING "variations/fm_content_tuner_robbery_warehouse_defences.sch"

/// PURPOSE: Use to set up the logic pointers for your mission
///    
PROC SETUP_MISSION_LOGIC()
	SWITCH GET_VARIATION()
		CASE TRV_SETUP				ROBBERY_SETUP_LOGIC()				BREAK
		CASE TRV_THERMAL_CHARGES	THERMAL_SETUP_LOGIC()				BREAK
		CASE TRV_LOCATE_BUNKER		LB_SETUP_LOGIC()					BREAK
		CASE TRV_WAREHOUSE_DEFENCES	WAREDEF_SETUP_LOGIC()				BREAK
		CASE TRV_TRAIN_SCHEDULE		TRAIN_SCHEDULE_SETUP_LOGIC()		BREAK
		CASE TRV_STUNT_RAMP			STUNT_RAMP_SETUP_LOGIC()			BREAK
		CASE TRV_COMPUTER_VIRUS		CVIRUS_SETUP_LOGIC()				BREAK
		CASE TRV_METH_LAB_LOCATIONS	METH_SETUP_LOGIC()					BREAK
		CASE TRV_SCOPE_TRANSPORTER	SCOPE_TRANSPORTER_SETUP_LOGIC()		BREAK
		CASE TRV_ELEVATOR_PASS		EP_SETUP_LOGIC()					BREAK
		CASE TRV_SIGNAL_JAMMERS		SIGNAL_JAMMERS_SETUP_LOGIC()		BREAK
		CASE TRV_METH_TANKER		MTANK_SETUP_LOGIC()					BREAK
		CASE TRV_INSIDE_MAN			INMAN_SETUP_LOGIC()					BREAK
		CASE TRV_CONTAINER_MANIFEST CONTAINER_SETUP_LOGIC()				BREAK
		CASE TRV_IAA_PASS 			IAA_SETUP_LOGIC()					BREAK
		CASE TRV_SEWER_SCHEMATICS	SEWER_SCHEMATICS_LOGIC()			BREAK
		CASE TRV_VAULT_KEY_CODES	VKCODE_SETUP_LOGIC()				BREAK
	ENDSWITCH
ENDPROC
#ENDIF
