
USING "globals.sch"

CONST_INT ENABLE_CONTENT	FEATURE_TUNER

#IF ENABLE_CONTENT
USING "tuner_planning_support.sch"

//----------------------
//	GLOBAL DATA WRAPPERS
//----------------------

FUNC NET_HEIST_PLANNING_GENERIC_BOARD_TYPE TUNER_GET_BOARD_TYPE()
	RETURN NHPG_BOARD__TUNER
ENDFUNC

FUNC VECTOR TUNER_GET_BOARD_POSITION()
	RETURN <<0,0,0>>
ENDFUNC

FUNC NET_HEIST_PLANNING_GENERIC_CAMERA_LOCATION TUNER_GET_INITIAL_CAMERA_POSITION()
	RETURN NHPG_CAMLOCATION__PRIMARY
ENDFUNC


//----------------------
//	DATA INITIALISATION
//----------------------

FUNC BOOL SHOULD_SHOW_MAIN_SCREEN(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData)
	IF DOES_PLAYER_HAVE_ACTIVE_TUNER_ROBBERY(sData.piLeader)
	AND GET_PLAYER_ACTIVE_TUNER_ROBBERY(sData.piLeader) != TR_NONE
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC INIT_SELECTION_SCREEN_DATA(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData, STRUCT_SELECTION_SCREEN_DATA &sSelectionData)
	sSelectionData.Robbery[ITEM_ONE].Image = GET_TUNER_ROBBERY_IMAGE(GET_PLAYER_TUNER_ROBBERY_AT_INDEX(sData.piLeader, ITEM_ONE))
	sSelectionData.Robbery[ITEM_ONE].Name = GET_TUNER_ROBBERY_NAME(GET_PLAYER_TUNER_ROBBERY_AT_INDEX(sData.piLeader, ITEM_ONE))
	sSelectionData.Robbery[ITEM_ONE].Description = GET_TUNER_ROBBERY_DESCRIPTION(GET_PLAYER_TUNER_ROBBERY_AT_INDEX(sData.piLeader, ITEM_ONE))
	sSelectionData.Robbery[ITEM_ONE].Reward = GET_TUNER_ROBBERY_REWARD(GET_PLAYER_TUNER_ROBBERY_AT_INDEX(sData.piLeader, ITEM_ONE))
	sSelectionData.Robbery[ITEM_ONE].State = GET_TUNER_ROBBERY_STATE(sData, ITEM_ONE)
	sSelectionData.Robbery[ITEM_ONE].Selectable = IS_TUNER_ROBBERY_SELECTABLE(sData, ITEM_ONE)
	
	sSelectionData.Robbery[ITEM_TWO].Image = GET_TUNER_ROBBERY_IMAGE(GET_PLAYER_TUNER_ROBBERY_AT_INDEX(sData.piLeader, ITEM_TWO))
	sSelectionData.Robbery[ITEM_TWO].Name = GET_TUNER_ROBBERY_NAME(GET_PLAYER_TUNER_ROBBERY_AT_INDEX(sData.piLeader, ITEM_TWO))
	sSelectionData.Robbery[ITEM_TWO].Description = GET_TUNER_ROBBERY_DESCRIPTION(GET_PLAYER_TUNER_ROBBERY_AT_INDEX(sData.piLeader, ITEM_TWO))
	sSelectionData.Robbery[ITEM_TWO].Reward = GET_TUNER_ROBBERY_REWARD(GET_PLAYER_TUNER_ROBBERY_AT_INDEX(sData.piLeader, ITEM_TWO))
	sSelectionData.Robbery[ITEM_TWO].State = GET_TUNER_ROBBERY_STATE(sData, ITEM_TWO)
	sSelectionData.Robbery[ITEM_TWO].Selectable = IS_TUNER_ROBBERY_SELECTABLE(sData, ITEM_TWO)
	
	sSelectionData.Robbery[ITEM_THREE].Image = GET_TUNER_ROBBERY_IMAGE(GET_PLAYER_TUNER_ROBBERY_AT_INDEX(sData.piLeader, ITEM_THREE))
	sSelectionData.Robbery[ITEM_THREE].Name = GET_TUNER_ROBBERY_NAME(GET_PLAYER_TUNER_ROBBERY_AT_INDEX(sData.piLeader, ITEM_THREE))
	sSelectionData.Robbery[ITEM_THREE].Description = GET_TUNER_ROBBERY_DESCRIPTION(GET_PLAYER_TUNER_ROBBERY_AT_INDEX(sData.piLeader, ITEM_THREE))
	sSelectionData.Robbery[ITEM_THREE].Reward = GET_TUNER_ROBBERY_REWARD(GET_PLAYER_TUNER_ROBBERY_AT_INDEX(sData.piLeader, ITEM_THREE))
	sSelectionData.Robbery[ITEM_THREE].State = GET_TUNER_ROBBERY_STATE(sData, ITEM_THREE)
	sSelectionData.Robbery[ITEM_THREE].Selectable = IS_TUNER_ROBBERY_SELECTABLE(sData, ITEM_THREE)
	
	sSelectionData.NumCompleted = GET_PLAYER_NUM_TUNER_ROBBERIES_COMPLETED(sData.piLeader)
	sSelectionData.Earnings = GET_PLAYER_TOTAL_TUNER_ROBBERY_EARNINGS(sData.piLeader)
	
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | INIT_SELECTION_SCREEN_DATA - Start **************************************************************************************")
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] |  ")
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] |  sSelectionData.Robbery[ITEM_ONE].Image = ", sSelectionData.Robbery[ITEM_ONE].Image)
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] |  sSelectionData.Robbery[ITEM_ONE].Name = ", sSelectionData.Robbery[ITEM_ONE].Name, " (", GET_FILENAME_FOR_AUDIO_CONVERSATION(sSelectionData.Robbery[ITEM_ONE].Name), ")")
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] |  sSelectionData.Robbery[ITEM_ONE].Description = ", sSelectionData.Robbery[ITEM_ONE].Description, " (", GET_FILENAME_FOR_AUDIO_CONVERSATION(sSelectionData.Robbery[ITEM_ONE].Description), ")")
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] |  sSelectionData.Robbery[ITEM_ONE].Reward = ", sSelectionData.Robbery[ITEM_ONE].Reward)
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] |  sSelectionData.Robbery[ITEM_ONE].State = ", JOB_STATE_TO_STRING(sSelectionData.Robbery[ITEM_ONE].State))
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] |  sSelectionData.Robbery[ITEM_ONE].Selectable = ", GET_STRING_FROM_BOOL(sSelectionData.Robbery[ITEM_ONE].Selectable))
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] |  ")
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] |  sSelectionData.Robbery[ITEM_TWO].Image = ", sSelectionData.Robbery[ITEM_TWO].Image)
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] |  sSelectionData.Robbery[ITEM_TWO].Name = ", sSelectionData.Robbery[ITEM_TWO].Name, " (", GET_FILENAME_FOR_AUDIO_CONVERSATION(sSelectionData.Robbery[ITEM_TWO].Name), ")")
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] |  sSelectionData.Robbery[ITEM_TWO].Description = ", sSelectionData.Robbery[ITEM_TWO].Description, " (", GET_FILENAME_FOR_AUDIO_CONVERSATION(sSelectionData.Robbery[ITEM_TWO].Description), ")")
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] |  sSelectionData.Robbery[ITEM_TWO].Reward = ", sSelectionData.Robbery[ITEM_TWO].Reward)
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] |  sSelectionData.Robbery[ITEM_TWO].State = ", JOB_STATE_TO_STRING(sSelectionData.Robbery[ITEM_TWO].State))
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] |  sSelectionData.Robbery[ITEM_TWO].Selectable = ", GET_STRING_FROM_BOOL(sSelectionData.Robbery[ITEM_TWO].Selectable))
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] |  ")
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] |  sSelectionData.Robbery[ITEM_THREE].Image = ", sSelectionData.Robbery[ITEM_THREE].Image)
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] |  sSelectionData.Robbery[ITEM_THREE].Name = ", sSelectionData.Robbery[ITEM_THREE].Name, " (", GET_FILENAME_FOR_AUDIO_CONVERSATION(sSelectionData.Robbery[ITEM_THREE].Name), ")")
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] |  sSelectionData.Robbery[ITEM_THREE].Description = ", sSelectionData.Robbery[ITEM_THREE].Description, " (", GET_FILENAME_FOR_AUDIO_CONVERSATION(sSelectionData.Robbery[ITEM_THREE].Description), ")")
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] |  sSelectionData.Robbery[ITEM_THREE].Reward = ", sSelectionData.Robbery[ITEM_THREE].Reward)
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] |  sSelectionData.Robbery[ITEM_THREE].State = ", JOB_STATE_TO_STRING(sSelectionData.Robbery[ITEM_THREE].State))
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] |  sSelectionData.Robbery[ITEM_THREE].Selectable = ", GET_STRING_FROM_BOOL(sSelectionData.Robbery[ITEM_THREE].Selectable))
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] |  ")
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] |  sSelectionData.NumCompleted = ", sSelectionData.NumCompleted)
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] |  sSelectionData.Earnings = ", sSelectionData.Earnings)
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] |  ")
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | INIT_SELECTION_SCREEN_DATA - End **************************************************************************************")
ENDPROC

PROC INIT_MAIN_SCREEN_DATA(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData, STRUCT_MAIN_SCREEN_DATA &sMainData)
	sMainData.Name = GET_TUNER_ROBBERY_NAME(GET_PLAYER_ACTIVE_TUNER_ROBBERY(sData.piLeader))
	sMainData.Description = GET_TUNER_ROBBERY_DESCRIPTION(GET_PLAYER_ACTIVE_TUNER_ROBBERY(sData.piLeader))
	sMainData.Reward = GET_TUNER_ROBBERY_REWARD(GET_PLAYER_ACTIVE_TUNER_ROBBERY(sData.piLeader))
	sMainData.Loadout[ITEM_ONE] = "LOADOUT1"
	sMainData.Loadout[ITEM_TWO] = "LOADOUT2"
	
	sMainData.Prep[ITEM_ONE].Name = GET_TUNER_ROBBERY_PREP_NAME(GET_PLAYER_ACTIVE_TUNER_ROBBERY(sData.piLeader), PREP_ONE)
	sMainData.Prep[ITEM_ONE].Description = GET_TUNER_ROBBERY_PREP_DESCRIPTION(GET_PLAYER_ACTIVE_TUNER_ROBBERY(sData.piLeader), PREP_ONE)
	sMainData.Prep[ITEM_ONE].Image = GET_TUNER_ROBBERY_PREP_IMAGE(GET_PLAYER_ACTIVE_TUNER_ROBBERY(sData.piLeader), PREP_ONE)
	sMainData.Prep[ITEM_ONE].State = GET_TUNER_ROBBERY_PREP_STATE(sData, PREP_ONE)
	sMainData.Prep[ITEM_ONE].Selectable = IS_TUNER_ROBBERY_PREP_SELECTABLE(sData, PREP_ONE)
	
	sMainData.Prep[ITEM_TWO].Name = GET_TUNER_ROBBERY_PREP_NAME(GET_PLAYER_ACTIVE_TUNER_ROBBERY(sData.piLeader), ITEM_TWO)
	sMainData.Prep[ITEM_TWO].Description = GET_TUNER_ROBBERY_PREP_DESCRIPTION(GET_PLAYER_ACTIVE_TUNER_ROBBERY(sData.piLeader), ITEM_TWO)
	sMainData.Prep[ITEM_TWO].Image = GET_TUNER_ROBBERY_PREP_IMAGE(GET_PLAYER_ACTIVE_TUNER_ROBBERY(sData.piLeader), ITEM_TWO)
	sMainData.Prep[ITEM_TWO].State = GET_TUNER_ROBBERY_PREP_STATE(sData, ITEM_TWO)
	sMainData.Prep[ITEM_TWO].Selectable = IS_TUNER_ROBBERY_PREP_SELECTABLE(sData, ITEM_TWO)
	
	sMainData.Finale.Name = GET_TUNER_ROBBERY_FINALE_NAME(GET_PLAYER_ACTIVE_TUNER_ROBBERY(sData.piLeader))
	sMainData.Finale.Description = GET_TUNER_ROBBERY_FINALE_DESCRIPTION(GET_PLAYER_ACTIVE_TUNER_ROBBERY(sData.piLeader))
	sMainData.Finale.Image = GET_TUNER_ROBBERY_FINALE_IMAGE(GET_PLAYER_ACTIVE_TUNER_ROBBERY(sData.piLeader))
	sMainData.Finale.State = GET_TUNER_ROBBERY_FINALE_STATE(sData)
	sMainData.Finale.Selectable = IS_TUNER_ROBBERY_FINALE_SELECTABLE(sData)
	
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | INIT_MAIN_SCREEN_DATA - Start **************************************************************************************")
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] |  ")
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] |  sMainData.Name = ", sMainData.Name, " (", GET_FILENAME_FOR_AUDIO_CONVERSATION(sMainData.Name), ")")
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] |  sMainData.Description = ", sMainData.Description, " (", GET_FILENAME_FOR_AUDIO_CONVERSATION(sMainData.Description), ")")
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] |  sMainData.Reward = ", sMainData.Reward)
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] |  sMainData.Loadout[ITEM_ONE] = ", sMainData.Loadout[ITEM_ONE])
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] |  sMainData.Loadout[ITEM_TWO] = ", sMainData.Loadout[ITEM_TWO])
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] |  ")
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] |  sMainData.Prep[ITEM_ONE].Name = ", sMainData.Prep[ITEM_ONE].Name, " (", GET_FILENAME_FOR_AUDIO_CONVERSATION(sMainData.Prep[ITEM_ONE].Name), ")")
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] |  sMainData.Prep[ITEM_ONE].Description = ", sMainData.Prep[ITEM_ONE].Description, " (", GET_FILENAME_FOR_AUDIO_CONVERSATION(sMainData.Prep[ITEM_ONE].Description), ")")
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] |  sMainData.Prep[ITEM_ONE].Image = ", sMainData.Prep[ITEM_ONE].Image)
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] |  sMainData.Prep[ITEM_ONE].State = ", JOB_STATE_TO_STRING(sMainData.Prep[ITEM_ONE].State))
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] |  sMainData.Prep[ITEM_ONE].Selectable = ", GET_STRING_FROM_BOOL(sMainData.Prep[ITEM_ONE].Selectable))
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] |  ")
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] |  sMainData.Prep[ITEM_TWO].Name = ", sMainData.Prep[ITEM_TWO].Name, " (", GET_FILENAME_FOR_AUDIO_CONVERSATION(sMainData.Prep[ITEM_TWO].Name), ")")
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] |  sMainData.Prep[ITEM_TWO].Description = ", sMainData.Prep[ITEM_TWO].Description, " (", GET_FILENAME_FOR_AUDIO_CONVERSATION(sMainData.Prep[ITEM_TWO].Description), ")")
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] |  sMainData.Prep[ITEM_TWO].Image = ", sMainData.Prep[ITEM_TWO].Image)
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] |  sMainData.Prep[ITEM_TWO].State = ", JOB_STATE_TO_STRING(sMainData.Prep[ITEM_TWO].State))
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] |  sMainData.Prep[ITEM_TWO].Selectable = ", GET_STRING_FROM_BOOL(sMainData.Prep[ITEM_TWO].Selectable))
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] |  ")
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] |  sMainData.Finale.Name = ", sMainData.Finale.Name, " (", GET_FILENAME_FOR_AUDIO_CONVERSATION(sMainData.Finale.Name), ")")
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] |  sMainData.Finale.Description = ", sMainData.Finale.Description, " (", GET_FILENAME_FOR_AUDIO_CONVERSATION(sMainData.Finale.Description), ")")
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] |  sMainData.Finale.Image = ", sMainData.Finale.Image)
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] |  sMainData.Finale.State = ", JOB_STATE_TO_STRING(sMainData.Finale.State))
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] |  sMainData.Finale.Selectable = ", GET_STRING_FROM_BOOL(sMainData.Finale.Selectable))
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] |  ")
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | INIT_MAIN_SCREEN_DATA - End **************************************************************************************")
ENDPROC

//----------------------
//	INTERACTION
//----------------------
		
FUNC BOOL TUNER_CAN_INTERACT_WITH_BOARD()
	IF IS_ENTITY_IN_ANGLED_AREA( LOCAL_PED_INDEX, <<-1350.245361,137.303787,-96.109337>>, <<-1347.281982,138.094025,-94.109337>>, 1.750000)
	AND IS_HEADING_ACCEPTABLE(GET_ENTITY_HEADING(LOCAL_PED_INDEX), 198.0, 75.0)	
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC STRING TUNER_GET_PROMPT_HELP()
	IF NOT HAS_PLAYER_COMPLETED_AUTO_SHOP_SETUP_MISSION(LOCAL_PLAYER_INDEX)
		RETURN "TUN_INT_STP"
	ENDIF
	IF NOT DOES_PLAYER_HAVE_ACTIVE_TUNER_ROBBERY(LOCAL_PLAYER_INDEX)
	AND NOT HAS_PLAYER_COMPLETED_ANY_TUNER_ROBBERY(LOCAL_PLAYER_INDEX)
		RETURN "TUN_INT_CON2"
	ENDIF
	RETURN "TUN_INT_CON"
ENDFUNC

PROC TUNER_INTERACTION_ON_START(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData)
	IF NOT HAS_PLAYER_COMPLETED_AUTO_SHOP_SETUP_MISSION(LOCAL_PLAYER_INDEX)
		HANDLE_FREEMODE_MISSION_LAUNCH(sData, HSI_INVALID, TRV_SETUP)
	ELSE
		sData.sStateData.bInteract = TRUE
	ENDIF
ENDPROC

//----------------------
//	STATE MACHINE
//----------------------

PROC TUNER_STATE_MACHINE_ON_ENTRY(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData)
	SWITCH sData.sStateData.eState
		CASE NHPG_BOARDSTATE__IDLE
		
			BREAK
			
		CASE NHPG_BOARDSTATE__INIT
						
			IF SHOULD_SHOW_MAIN_SCREEN(sData)
				INIT_MAIN_SCREEN_DATA(sData, sMainScreenData)
			ELSE
				INIT_SELECTION_SCREEN_DATA(sData, sSelectionScreenData)
			ENDIF
			
			eBoardStyle = GET_TUNER_ROBBERY_BOARD_STYLE(sData)
			CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | TUNER_STATE_MACHINE_ON_ENTRY - NHPG_BOARDSTATE__INIT - eBoardStyle = ", ENUM_TO_INT(eBoardStyle))
			
			BREAK
		
		CASE NHPG_BOARDSTATE__LOADING
			
			REQUEST_SCRIPT_AUDIO_BANK("DLC_MPHEIST/HEIST_PLANNING_BOARD")
			
			BREAK
		
		CASE NHPG_BOARDSTATE__DISPLAYING
		
			IF HAS_PLAYER_COMPLETED_AUTO_SHOP_SETUP_MISSION(sData.piLeader)
				IF SHOULD_SHOW_MAIN_SCREEN(sData)
					SCALEFORM_SET_STYLE(sData, eBoardStyle)
					SCALEFORM_SHOW_MAIN_SCREEN(sData, sMainScreenData)
				ELSE
					SCALEFORM_SET_STYLE(sData, eBoardStyle)
					SCALEFORM_SHOW_SELECTION_SCREEN(sData, sSelectionScreenData)
				ENDIF
			ELSE
				SCALEFORM_SET_STYLE(sData, eBoardStyle)
				SCALEFORM_SHOW_BLANK_SCREEN(sData)
			ENDIF
		
			BREAK
		
		CASE NHPG_BOARDSTATE__INUSE
						
			IF NOT HAS_LOCAL_PLAYER_SEEN_JOB_BOARD_INUSE_HELP()
				PRINT_HELP("TUNPLAN_HELP_0")
				SET_SEEN_JOB_BOARD_INUSE_HELP(TRUE)
				SET_DIALOGUE_TRIGGER(sData, NHPG_DIALOGUE__CONTRACT_SELECTION)
			ENDIF
						
			BREAK
		
		CASE NHPG_BOARDSTATE__CLEANUP
		
			BREAK
	ENDSWITCH
ENDPROC

PROC TUNER_STATE_MACHINE_ON_UPDATE(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData)
	SWITCH sData.sStateData.eState
		CASE NHPG_BOARDSTATE__IDLE
		
			BREAK
			
		CASE NHPG_BOARDSTATE__INIT
			
			BREAK
		
		CASE NHPG_BOARDSTATE__LOADING
			
			BREAK
		
		CASE NHPG_BOARDSTATE__DISPLAYING
		
			BREAK
		
		CASE NHPG_BOARDSTATE__INUSE
						
			UPDATE_ROLLOVER(sData)
			UPDATE_SELECTION(sData)
						
			BREAK
		
		CASE NHPG_BOARDSTATE__CLEANUP
		
			BREAK
	ENDSWITCH
ENDPROC

PROC TUNER_STATE_MACHINE_ON_EXIT(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData)
	SWITCH sData.sStateData.eCachedState
		CASE NHPG_BOARDSTATE__IDLE
		
			BREAK
			
		CASE NHPG_BOARDSTATE__INIT
			
			BREAK
		
		CASE NHPG_BOARDSTATE__LOADING
			
			BREAK
		
		CASE NHPG_BOARDSTATE__DISPLAYING
		
			BREAK
		
		CASE NHPG_BOARDSTATE__INUSE
						
			BREAK
		
		CASE NHPG_BOARDSTATE__CLEANUP
		
			BREAK
	ENDSWITCH
ENDPROC

		
//----------------------
//	RENDER TARGET
//----------------------
		
FUNC STRING TUNER_GET_RENDERTARGET_NAME()
	RETURN "modgarage_01"
ENDFUNC

FUNC MODEL_NAMES TUNER_GET_RENDERTARGET_MODEL()
	RETURN INT_TO_ENUM(MODEL_NAMES, HASH("tr_prop_tr_planning_board_01a"))
ENDFUNC

//----------------------
//	SCALEFORM
//----------------------
		
FUNC STRING TUNER_SCALEFORM_GET_MOVIE()
	RETURN "AUTO_SHOP_BOARD"
ENDFUNC

FUNC HEIST_BOARD_RENDER_TYPE TUNER_SCALEFORM_GET_RENDER_TYPE()
	RETURN HBRT_SUPERLARGE
ENDFUNC

FUNC FLOAT TUNER_SCALEFORM_GET_CENTRE_X()
	RETURN 0.25	//HEIST_BOARD_X
ENDFUNC

FUNC FLOAT TUNER_SCALEFORM_GET_CENTRE_Y()
	RETURN 0.5	//HEIST_BOARD_Y
ENDFUNC

FUNC FLOAT TUNER_SCALEFORM_GET_WIDTH()
	RETURN 0.5	//HEIST_BOARD_W
ENDFUNC

FUNC FLOAT TUNER_SCALEFORM_GET_HEIGHT()
	RETURN 1.0	//HEIST_BOARD_H
ENDFUNC

PROC TUNER_SCALEFORM_ALT_RENDER(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData)
	UNUSED_PARAMETER(sData)
ENDPROC

//----------------------
//	INPUT
//----------------------
		
PROC PLAY_SOUND_ON_INPUT(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData, HEIST_BOARD_CONTROL_TYPE eControlType, HEIST_SCREEN_ITEMS eItem)
		
	SWITCH eControlType
	
		CASE HBCT_UP
				
			IF eItem = HSI_ROBBERY_SELECTION_1
			OR eItem = HSI_ROBBERY_PREP_1
				PLAY_HEIST_SCREEN_SOUND(sData, HSS_HIGHLIGHT_ERROR)		
			ELSE
				PLAY_HEIST_SCREEN_SOUND(sData, HSS_HIGHLIGHT_MOVE)
			ENDIF
		
			BREAK
		
		CASE HBCT_DOWN
		
			IF eItem = HSI_ROBBERY_SELECTION_3
			OR eItem = HSI_ROBBERY_FINALE
				PLAY_HEIST_SCREEN_SOUND(sData, HSS_HIGHLIGHT_ERROR)
			ELSE
				PLAY_HEIST_SCREEN_SOUND(sData, HSS_HIGHLIGHT_MOVE)
			ENDIF
		
			BREAK
		
		CASE HBCT_LEFT
				
			PLAY_HEIST_SCREEN_SOUND(sData, HSS_HIGHLIGHT_ERROR)
		
			BREAK
		
		CASE HBCT_RIGHT
		
			PLAY_HEIST_SCREEN_SOUND(sData, HSS_HIGHLIGHT_ERROR)
		
			BREAK
	
		CASE HBCT_A
		
			IF eItem = HSI_ROBBERY_SELECTION_1
			OR eItem = HSI_ROBBERY_SELECTION_2
			OR eItem = HSI_ROBBERY_SELECTION_3
				IF IS_PLAYER_TUNER_ROBBERY_ON_COOLDOWN(sData.piLeader, GET_PLAYER_TUNER_ROBBERY_AT_INDEX(sData.piLeader, GET_SLOT_FROM_SCREEN_ITEM(eItem)))
					PLAY_HEIST_SCREEN_SOUND(sData, HSS_HIGHLIGHT_ERROR)
					EXIT
				ENDIF
			ENDIF
		
			IF eItem = HSI_ROBBERY_PREP_1
			OR eItem = HSI_ROBBERY_PREP_2
			OR eItem = HSI_ROBBERY_FINALE
//				PLAY_HEIST_SCREEN_SOUND(sData, HSS_CONTINUE_ACCEPTED)		// Moved this to the point where the mission launches, so we know to play error sound if it fails
			ELSE
				PLAY_HEIST_SCREEN_SOUND(sData, HSS_HIGHLIGHT_ACCEPT)
			ENDIF
		
			BREAK
		
		CASE HBCT_B
		
			PLAY_HEIST_SCREEN_SOUND(sData, HSS_HIGHLIGHT_CANCEL)
		
			BREAK
		
	
	ENDSWITCH
	
ENDPROC
		
PROC TUNER_INPUT_ON_EVENT(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData, HEIST_BOARD_CONTROL_TYPE eControlType, INT &iBitSet)
	
	IF bCurrentRolloverRequested
	OR bCurrentSelectionRequested
		EXIT
	ENDIF
	
	HEIST_SCREEN_ITEMS eItem = INT_TO_ENUM(HEIST_SCREEN_ITEMS, sData.sCursorData.iHighlight)

	PLAY_SOUND_ON_INPUT(sData, eControlType, eItem)

	SWITCH eControlType
	
		CASE HBCT_UP
					
			SCALEFORM_SET_INPUT_EVENT(sData.sScaleformData.siMainBoard, INPUT_FRONTEND_UP)
			SCALEFORM_REQUEST_CURRENT_ROLLOVER(sData.sScaleformData.siMainBoard)
		
			BREAK
		
		CASE HBCT_DOWN
		
			SCALEFORM_SET_INPUT_EVENT(sData.sScaleformData.siMainBoard, INPUT_FRONTEND_DOWN)
			SCALEFORM_REQUEST_CURRENT_ROLLOVER(sData.sScaleformData.siMainBoard)
		
			BREAK
		
		CASE HBCT_LEFT
		
			SCALEFORM_SET_INPUT_EVENT(sData.sScaleformData.siMainBoard, INPUT_FRONTEND_LEFT)
			SCALEFORM_REQUEST_CURRENT_ROLLOVER(sData.sScaleformData.siMainBoard)
		
			BREAK
		
		CASE HBCT_RIGHT
		
			SCALEFORM_SET_INPUT_EVENT(sData.sScaleformData.siMainBoard, INPUT_FRONTEND_RIGHT)
			SCALEFORM_REQUEST_CURRENT_ROLLOVER(sData.sScaleformData.siMainBoard)
		
			BREAK
	
		CASE HBCT_LB
						
			BREAK
		
		CASE HBCT_RB

			BREAK
	
		CASE HBCT_A
		
			IF sDialogueData.eDialogueCurrentlyPlaying != NHPG_DIALOGUE__INVALID
				EXIT
			ENDIF
		
			SCALEFORM_REQUEST_CURRENT_SELECTION(sData.sScaleformData.siMainBoard)
			SCALEFORM_REQUEST_CURRENT_ROLLOVER(sData.sScaleformData.siMainBoard)

			BREAK
		
		CASE HBCT_B
			
			IF sDialogueData.eDialogueCurrentlyPlaying != NHPG_DIALOGUE__INVALID
				EXIT
			ENDIF
			
			SET_BIT(iBitSet, HEIST_BOARD_INPUT_BITSET__END_INTERACTION)
			CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | TUNER_INPUT_ON_EVENT - B pressed on root menu item, we shoud end interaction. HEIST_BOARD_INPUT_BITSET__END_INTERACTION set.")
		
			BREAK
		
		CASE HBCT_X
			
			BREAK
		
		CASE HBCT_Y
		
			BREAK
			
		CASE HBCT_SELECT

			BREAK
	
	ENDSWITCH
ENDPROC

//----------------------
//	CAMERA
//----------------------
		
FUNC VECTOR TUNER_CAMERA_GET_POSITION_FOR_POSITION_ID(NET_HEIST_PLANNING_GENERIC_CAMERA_LOCATION eLocation)
	SWITCH eLocation
		CASE NHPG_CAMLOCATION__PRIMARY
			RETURN <<-1349.2153, 139.0860, -94.9017>>

	ENDSWITCH
	
	RETURN <<0,0,0>>
ENDFUNC

FUNC VECTOR TUNER_CAMERA_GET_ROTATION_FOR_POSITION_ID(NET_HEIST_PLANNING_GENERIC_CAMERA_LOCATION eLocation)
	SWITCH eLocation
		CASE NHPG_CAMLOCATION__PRIMARY
			RETURN <<-0.7074, -0.0428, -164.7957>>

	ENDSWITCH
	
	RETURN <<0,0,0>>
ENDFUNC

FUNC FLOAT TUNER_CAMERA_GET_FOV_FOR_POSITION_ID(NET_HEIST_PLANNING_GENERIC_CAMERA_LOCATION eLocation)
	SWITCH eLocation
		CASE NHPG_CAMLOCATION__PRIMARY
			RETURN 56.2117

	ENDSWITCH
	
	RETURN 0.0
ENDFUNC

FUNC BOOL TUNER_CAMERA_GET_ALLOW_STICK_LOOK()
	RETURN TRUE
ENDFUNC

FUNC BOOL TUNER_CAMERA_IS_ZOOM_ENABLED()
	RETURN TRUE
ENDFUNC

FUNC FLOAT TUNER_CAMERA_SHAKE_AMPLITUDE()
	RETURN FPC_HANDSHAKE
ENDFUNC
		
//----------------------
//	HELP
//----------------------
		
FUNC STRING TUNER_GET_HELP_TEXT_LABEL(HEIST_BOARD_HELP eHelp)
	SWITCH eHelp
		CASE HBH_BECOME_BOSS			
			RETURN "TUN_HELP_BBOSS"
	ENDSWITCH
	RETURN ""
ENDFUNC
		
//----------------------
//	CLEANUP
//----------------------
				
FUNC BOOL TUNER_CLEANUP_SHOULD_CLEANUP()
	
	IF DOES_ENTITY_EXIST(LOCAL_PED_INDEX)
	AND IS_NET_PLAYER_OK(LOCAL_PLAYER_INDEX)
		IF NOT IS_PLAYER_IN_CORONA()
		AND NOT IS_TRANSITION_SESSION_LAUNCHING()
		AND NOT IS_TRANSITION_SESSION_RESTARTING()
			IF NOT IS_PLAYER_IN_AUTO_SHOP(LOCAL_PLAYER_INDEX)
				CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | TUNER_CLEANUP_SHOULD_CLEANUP - TRUE - Not in auto shop")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_SIMPLE_INTERIOR_AN_AUTO_SHOP(g_SimpleInteriorData.eSimpleInteriorIDToKeepKilledForHeistLaunch)
		CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | TUNER_CLEANUP_SHOULD_CLEANUP - TRUE - IS_SIMPLE_INTERIOR_AN_AUTO_SHOP(g_SimpleInteriorData.eSimpleInteriorIDToKeepKilledForHeistLaunch)")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//----------------------
//	MISSION
//----------------------
		
FUNC INT TUNER_GET_MISSION_ARRAY_POS(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData)
	RETURN GET_TUNER_ROBBERY_FINALE_MISSION_ARRAY_POS(GET_PLAYER_ACTIVE_TUNER_ROBBERY(sData.piLeader))
ENDFUNC

//----------------------
//	STATUS
//----------------------

FUNC BOOL TUNER_STATUS_IS_SETUP_REQUIRED()
	RETURN FALSE
ENDFUNC

FUNC BOOL TUNER_STATUS_IS_ACTIVE()
	RETURN TRUE
ENDFUNC

PROC TUNER_STATUS_SET_ACTIVE()

ENDPROC

FUNC INT TUNER_STATUS_GET_SETUP_COST()
	RETURN 0
ENDFUNC

FUNC STRING TUNER_STATUS_GET_SETUP_CONTEXT()
	RETURN ""
ENDFUNC
		
FUNC TEXT_LABEL_63 TUNER_STATUS_GET_WARNING_TEXT()
	TEXT_LABEL_63 tlReturn
	RETURN tlReturn
ENDFUNC
		
FUNC BOOL TUNER_STATUS_GET_ON_COOLDOWN()
	RETURN FALSE
ENDFUNC

FUNC STRING TUNER_STATUS_GET_ON_COOLDOWN_HELP()
	RETURN ""
ENDFUNC

FUNC BOOL TUNER_STATUS_PAY_FOR_SETUP()
	RETURN TRUE
ENDFUNC

//----------------------
//	DIALOGUE
//----------------------
	
FUNC BOOL TUNER_DIALOGUE_IS_ENABLED()
	RETURN TRUE
ENDFUNC

FUNC STRING TUNER_DIALOGUE_GET_ROOT(NET_HEIST_PLANNING_GENERIC_DIALOGUE eDialogue)
	SWITCH(eDialogue)
		CASE NHPG_DIALOGUE__CONTRACT_SELECTION							RETURN "TNSS_PB_CI1"
		CASE NHPG_DIALOGUE__UNION_DEPOSITORY_CONTRACT_INTRO				RETURN "TNSS_PB_UD1"
		CASE NHPG_DIALOGUE__MILITARY_CONVOY_CONTRACT_INTRO				RETURN "TNSS_PB_MC1"
		CASE NHPG_DIALOGUE__FLEECA_BANKS_CONTRACT_INTRO					RETURN "TNSS_PB_FB1"
		CASE NHPG_DIALOGUE__FREIGHT_TRAIN_CONTRACT_INTRO				RETURN "TNSS_PB_FT1"
		CASE NHPG_DIALOGUE__BOLINGBROKE_CONTRACT_INTRO					RETURN "TNSS_PB_BB1"
		CASE NHPG_DIALOGUE__IAA_RAID_CONTRACT_INTRO						RETURN "TNSS_PB_IA1"
		CASE NHPG_DIALOGUE__METH_JOB_CONTRACT_INTRO						RETURN "TNSS_PB_MJ1"
		CASE NHPG_DIALOGUE__BUNKER_RAID_CONTRACT_INTRO					RETURN "TNSS_PB_BR1"
		CASE NHPG_DIALOGUE__GENERAL_CONTRACT_INTRO						RETURN "TNSS_PB_GC1"
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC STRING TUNER_DIALOGUE_GET_BLOCK(NET_HEIST_PLANNING_GENERIC_DIALOGUE eDialogue)
	UNUSED_PARAMETER(eDialogue)
	RETURN "TNSSAUD"
ENDFUNC

FUNC INT TUNER_DIALOGUE_GET_SPEAKER_ID(NET_HEIST_PLANNING_GENERIC_DIALOGUE eDialogue)
	UNUSED_PARAMETER(eDialogue)
	RETURN 2
ENDFUNC

FUNC STRING TUNER_DIALOGUE_GET_VOICE_ID(NET_HEIST_PLANNING_GENERIC_DIALOGUE eDialogue)
	UNUSED_PARAMETER(eDialogue)
	RETURN "TUN_SESSANTA"
ENDFUNC
		
FUNC BOOL TUNER_DIALOGUE_CAN_TRIGGER(NET_HEIST_PLANNING_GENERIC_DIALOGUE eDialogue)
	IF IS_BIT_SET(GET_MP_INT_CHARACTER_STAT(MP_STAT_TUNER_BOARD_DIALOGUE), ENUM_TO_INT(eDialogue))
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC
		
PROC TUNER_DIALOGUE_ON_TRIGGERED(NET_HEIST_PLANNING_GENERIC_DIALOGUE eDialogue)
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | [DIALOGUE] TUNER_DIALOGUE_ON_TRIGGERED - calling with eDialogue ", eDialogue, " (", NET_HEIST_PLANNING_GENERIC_DIALOGUE_TO_STRING(eDialogue), ")")
	
	INT iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_TUNER_BOARD_DIALOGUE)
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | [DIALOGUE] TUNER_DIALOGUE_ON_TRIGGERED - value before: ", iStatInt)
		
	SET_BIT(iStatInt, ENUM_TO_INT(eDialogue))
	SET_MP_INT_CHARACTER_STAT(MP_STAT_TUNER_BOARD_DIALOGUE, iStatInt)
		
	CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | [DIALOGUE] TUNER_DIALOGUE_ON_TRIGGERED - value after: ", iStatInt)
ENDPROC

//----------------------
//	WARNING SCREEN
//----------------------

FUNC STRING TUNER_WARNING_SCREEN_WARNING(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData)
	SWITCH sData.iCustomWarningID
		CASE WARNING_SCREEN_CONFIRM_ROBBERY_SELECTION
			RETURN "FM_CSC_QUIT"
		BREAK
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC STRING TUNER_WARNING_SCREEN_WARNING_TEXT(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData)
	SWITCH sData.iCustomWarningID
		CASE WARNING_SCREEN_CONFIRM_ROBBERY_SELECTION
			RETURN "TUNPLAN_WARN0"
		BREAK
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC STRING TUNER_WARNING_SCREEN_SUB_LABEL_ONE(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData)
	SWITCH sData.iCustomWarningID
		CASE WARNING_SCREEN_CONFIRM_ROBBERY_SELECTION
			SWITCH INT_TO_ENUM(HEIST_SCREEN_ITEMS, sData.sCursorData.iHighlight)
				CASE HSI_ROBBERY_SELECTION_1
					RETURN GET_TUNER_ROBBERY_WARNING_STRING(GET_PLAYER_TUNER_ROBBERY_AT_INDEX(sData.piLeader, ITEM_ONE))
				BREAK
				CASE HSI_ROBBERY_SELECTION_2
					RETURN GET_TUNER_ROBBERY_WARNING_STRING(GET_PLAYER_TUNER_ROBBERY_AT_INDEX(sData.piLeader, ITEM_TWO))
				BREAK
				CASE HSI_ROBBERY_SELECTION_3
					RETURN GET_TUNER_ROBBERY_WARNING_STRING(GET_PLAYER_TUNER_ROBBERY_AT_INDEX(sData.piLeader, ITEM_THREE))
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN ""
ENDFUNC

PROC TUNER_WARNING_SCREEN_ON_ACCEPT(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData)
	BOOL bShowMainScreen
	
	SWITCH sData.iCustomWarningID
		CASE WARNING_SCREEN_CONFIRM_ROBBERY_SELECTION
			SWITCH INT_TO_ENUM(HEIST_SCREEN_ITEMS, sData.sCursorData.iHighlight)
				CASE HSI_ROBBERY_SELECTION_1
					SET_CURRENT_TUNER_ROBBERY(GET_PLAYER_TUNER_ROBBERY_AT_INDEX(sData.piLeader, ITEM_ONE))
					bShowMainScreen = TRUE
				BREAK
				CASE HSI_ROBBERY_SELECTION_2
					SET_CURRENT_TUNER_ROBBERY(GET_PLAYER_TUNER_ROBBERY_AT_INDEX(sData.piLeader, ITEM_TWO))
					bShowMainScreen = TRUE
				BREAK
				CASE HSI_ROBBERY_SELECTION_3
					SET_CURRENT_TUNER_ROBBERY(GET_PLAYER_TUNER_ROBBERY_AT_INDEX(sData.piLeader, ITEM_THREE))
					bShowMainScreen = TRUE
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	IF bShowMainScreen
		GENERATE_TUNER_MATCH_HISTORY_PLAYSTATS()
		INIT_MAIN_SCREEN_DATA(sData, sMainScreenData)
		SCALEFORM_SET_STYLE(sData, eBoardStyle)
		SCALEFORM_SHOW_MAIN_SCREEN(sData, sMainScreenData)
		SET_DIALOGUE_TRIGGER(sData, GET_DIALOGUE_FROM_TUNER_ROBBERY(GET_PLAYER_ACTIVE_TUNER_ROBBERY(sData.piLeader)))
		IF NOT IS_BIT_SET(GET_MP_INT_CHARACTER_STAT(MP_STAT_TUNER_GEN_BS), ciTUNERGENERALBITSET_SHOWN_COMPLETE_PREPS_HELP)
			PRINT_HELP(TUNER_GET_PREPS_COMPLETE_HELP_TO_SHOW(GET_PLAYER_ACTIVE_TUNER_ROBBERY(sData.piLeader)))
			INT iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_TUNER_GEN_BS)
			SET_BIT(iStatInt, ciTUNERGENERALBITSET_SHOWN_COMPLETE_PREPS_HELP)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_TUNER_GEN_BS, iStatInt)
		ENDIF
	ENDIF
ENDPROC

//----------------------
//	COMMON OVERRIDES
//----------------------

PROC COMMON_PREGAME()

	IF !g_bTunerPlanningScriptActive
		g_bTunerPlanningScriptActive = TRUE
	ENDIF

ENDPROC

PROC COMMON_CLEANUP()

	IF g_bTunerPlanningScriptActive
		g_bTunerPlanningScriptActive = FALSE
	ENDIF

	RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_MPHEIST/HEIST_PLANNING_BOARD")

ENDPROC

PROC COMMON_MAINTAIN(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData)
	IF sData.piLeader != INVALID_PLAYER_INDEX()
		IF sData.piLeader = LOCAL_PLAYER_INDEX
			IF bLaunchedMission
			OR GlobalPlayerBD_NetHeistPlanningGeneric[NATIVE_TO_INT(sData.piLeader)].bLaunchedPrepFromBoard
			OR IS_PLAYER_WALKING_OUT_OF_SIMPLE_INTERIOR(LOCAL_PLAYER_INDEX)
				CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | COMMON_MAINTAIN - We're being sent outside!")
				IF IS_SCREEN_FADED_OUT()
				AND NOT IS_SCREEN_FADING_OUT()
					IF sData.sStateData.bInteract
						sData.sStateData.bInteract = FALSE
						sData.sScaleformData.bDrawInstructions = FALSE
						CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | COMMON_MAINTAIN - Stop interaction!")
					ENDIF
				ELSE
					CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | COMMON_MAINTAIN - faded in or in process of fading out...")
				ENDIF
			ENDIF
		ELIF GlobalPlayerBD_NetHeistPlanningGeneric[NATIVE_TO_INT(sData.piLeader)].bLaunchedPrepFromBoard
		AND GB_IS_LOCAL_PLAYER_MEMBER_OF_THIS_GANG(sData.piLeader)
		AND !bLaunchedMission
			TRIGGER_EXIT_FROM_SIMPLE_INTERIOR_NOW()
			CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | COMMON_MAINTAIN - leader has launched a prep mission, trigger exit.")
			bLaunchedMission = TRUE
		ENDIF
		
		IF sData.piLeader != LOCAL_PLAYER_INDEX
		AND eCurrentScreen = HS_SELECTION
		AND SHOULD_SHOW_MAIN_SCREEN(sData)
			CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | COMMON_MAINTAIN - Looking at selection screen, leader has an active robbery - switch to main screen.")
			INIT_MAIN_SCREEN_DATA(sData, sMainScreenData)
			SCALEFORM_SET_STYLE(sData, eBoardStyle)
			SCALEFORM_SHOW_MAIN_SCREEN(sData, sMainScreenData)
		ENDIF
	ENDIF
ENDPROC

//----------------------
//	LOGIC DATA
//----------------------

FUNC BOOL INITIALISE_BOARD_LOGIC_DATA(NET_HEIST_PLANNING_GENERIC_CLIENT_DATA &sData)
	
	IF NOT IS_BIT_SET(sData.iBitSet, NHPG_BITSET_BOARD_LOGIC_DATA_INITIALISED)
	
		// Temp so it compiles
		IF LOCAL_PLAYER_INDEX = PLAYER_ID()
		
		ENDIF
		IF LOCAL_PED_INDEX = PLAYER_PED_ID()
		
		ENDIF
	
		//
		// Set applicable global data here:
		//

		sData.eBoardType = TUNER_GET_BOARD_TYPE()
		sData.sPositionData.vBoardPosition = TUNER_GET_BOARD_POSITION()
		sData.sPositionData.vBoardSoundPosition = sData.sPositionData.vBoardPosition
		sData.sCameraData.ePositionId = TUNER_GET_INITIAL_CAMERA_POSITION()

		sData.sCoronaCameraData.vCamPos = <<-1348.9563, 138.2791, -94.7272>>
		sData.sCoronaCameraData.vCamRot = <<-2.4593, -0.0000, -164.9275>>
		sData.sCoronaCameraData.fCamFoV = 70.0

		IF NOT NETWORK_IS_ACTIVITY_SESSION()
			sData.piLeader = GET_OWNER_OF_SIMPLE_INTERIOR_LOCAL_PLAYER_IS_IN()
		ENDIF

		CDEBUG1LN(DEBUG_NET_HEIST_PLANNING_GENERIC, "[NHPG] [",scriptName,"] | INITIALISE_BOARD_LOGIC_DATA - sData.piLeader = ", GET_PLAYER_NAME(sData.piLeader))

		//
		// Set logic data function pointers here:
		//
		
		logic.Interaction.CanInteract = &TUNER_CAN_INTERACT_WITH_BOARD
		logic.Interaction.Prompt = &TUNER_GET_PROMPT_HELP
		logic.Interaction.OnStart = &TUNER_INTERACTION_ON_START

		logic.StateMachine.OnEntry = &TUNER_STATE_MACHINE_ON_ENTRY
		logic.StateMachine.OnExit = &TUNER_STATE_MACHINE_ON_EXIT
		logic.StateMachine.OnUpdate = &TUNER_STATE_MACHINE_ON_UPDATE
		
		logic.Scaleform.RenderTarget.Name = &TUNER_GET_RENDERTARGET_NAME
		logic.Scaleform.RenderTarget.Model = &TUNER_GET_RENDERTARGET_MODEL
		
		logic.Scaleform.MainBoard.Movie = &TUNER_SCALEFORM_GET_MOVIE
		logic.Scaleform.MainBoard.RenderType = &TUNER_SCALEFORM_GET_RENDER_TYPE
		logic.Scaleform.MainBoard.BoardCentreX = &TUNER_SCALEFORM_GET_CENTRE_X
		logic.Scaleform.MainBoard.BoardCentreY = &TUNER_SCALEFORM_GET_CENTRE_Y
		logic.Scaleform.MainBoard.BoardWidth = &TUNER_SCALEFORM_GET_WIDTH
		logic.Scaleform.MainBoard.BoardHeight = &TUNER_SCALEFORM_GET_HEIGHT
		logic.Scaleform.MainBoard.AltRender = &TUNER_SCALEFORM_ALT_RENDER
		
		logic.Input.OnEvent = &TUNER_INPUT_ON_EVENT
		
		logic.Camera.Position = &TUNER_CAMERA_GET_POSITION_FOR_POSITION_ID
		logic.Camera.Rotation = &TUNER_CAMERA_GET_ROTATION_FOR_POSITION_ID
		logic.Camera.FoV = &TUNER_CAMERA_GET_FOV_FOR_POSITION_ID
		logic.Camera.StickLook = &TUNER_CAMERA_GET_ALLOW_STICK_LOOK
		logic.Camera.Zoom.Enable = &TUNER_CAMERA_IS_ZOOM_ENABLED
		logic.Camera.Shake.Amplitude = &TUNER_CAMERA_SHAKE_AMPLITUDE
		
		logic.HelpText.Label = &TUNER_GET_HELP_TEXT_LABEL
				
		logic.Cleanup.ShouldCleanup = &TUNER_CLEANUP_SHOULD_CLEANUP
		
		logic.Mission.ArrayPos = &TUNER_GET_MISSION_ARRAY_POS
		
		logic.HeistStatus.SetupRequired = &TUNER_STATUS_IS_SETUP_REQUIRED
		logic.HeistStatus.IsActive = &TUNER_STATUS_IS_ACTIVE
		logic.HeistStatus.SetActive = &TUNER_STATUS_SET_ACTIVE
		logic.HeistStatus.SetupCost = &TUNER_STATUS_GET_SETUP_COST
		logic.HeistStatus.SetupContext = &TUNER_STATUS_GET_SETUP_CONTEXT
		logic.HeistStatus.WarningText = &TUNER_STATUS_GET_WARNING_TEXT
		logic.HeistStatus.OnCooldown = &TUNER_STATUS_GET_ON_COOLDOWN
		logic.HeistStatus.OnCooldownHelp = &TUNER_STATUS_GET_ON_COOLDOWN_HELP
		logic.HeistStatus.PayForSetup = &TUNER_STATUS_PAY_FOR_SETUP
		
		logic.Dialogue.Enable = &TUNER_DIALOGUE_IS_ENABLED
		logic.Dialogue.Root = &TUNER_DIALOGUE_GET_ROOT
		logic.Dialogue.Block = &TUNER_DIALOGUE_GET_BLOCK
		logic.Dialogue.SpeakerID = &TUNER_DIALOGUE_GET_SPEAKER_ID
		logic.Dialogue.VoiceID = &TUNER_DIALOGUE_GET_VOICE_ID
		logic.Dialogue.CanTrigger = &TUNER_DIALOGUE_CAN_TRIGGER
		logic.Dialogue.OnTriggered = &TUNER_DIALOGUE_ON_TRIGGERED
		
		logic.WarningScreen.Warning = &TUNER_WARNING_SCREEN_WARNING
		logic.WarningScreen.WarningText = &TUNER_WARNING_SCREEN_WARNING_TEXT
		logic.WarningScreen.SubLabelOne = &TUNER_WARNING_SCREEN_SUB_LABEL_ONE
		logic.WarningScreen.OnAccept = &TUNER_WARNING_SCREEN_ON_ACCEPT
		
		SET_BIT(sData.iBitSet, NHPG_BITSET_BOARD_LOGIC_DATA_INITIALISED)
	ENDIF

	RETURN IS_BIT_SET(sData.iBitSet, NHPG_BITSET_BOARD_LOGIC_DATA_INITIALISED)
	
ENDFUNC

#ENDIF
