
USING "globals.sch"
USING "rage_builtins.sch"
USING "fm_content_includes.sch"

//----------------------
//	CONSTANTS
//----------------------

CONST_INT MAX_NUM_VEHICLES	1

//----------------------
//	SERVER VARIABLES
//----------------------

STRUCT MISSION_SPECIFIC_SERVER_DATA
	
	VEHICLE_LIST_VARIATION eVariation = VLV_INVALID
	VEHICLE_LIST_SUBVARIATION eSubvariation = VLS_INVALID

ENDSTRUCT

//----------------------
//	LOCAL VARIABLES
//----------------------


//----------------------
//	BITSET WRAPPERS
//----------------------

