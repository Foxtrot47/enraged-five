USING "auto_shop_interior_seating_activities.sch"

PROC CLEAR_SEAT_DATA(SEATS_LOCAL_DATA& data, INT iIndex)
	data.public.seats[iIndex].eType = SEAT_INVALID
	data.public.seats[iIndex].entryLocate.v0 = <<0.0, 0.0, 0.0>>
	data.public.seats[iIndex].entryLocate.v1 = <<0.0, 0.0, 0.0>>
	data.public.seats[iIndex].entryLocate.fWidth = 0.0
	data.public.seats[iIndex].animation.vPos = <<0.0, 0.0, 0.0>>
ENDPROC

PROC SEATS_BLOCK_MANUAL_VARIATION_CHANGE(SEATS_LOCAL_DATA& data)
	IS_BIT_SET_ENUM(data.private.iBSGeneral, PSGB_BLOCK_MANUAL_VAR_CHANGE)
ENDPROC

PROC SEATS_UNBLOCK_MANUAL_VARIATION_CHANGE(SEATS_LOCAL_DATA& data)
	CLEAR_BIT_ENUM(data.private.iBSGeneral, PSGB_BLOCK_MANUAL_VAR_CHANGE)
ENDPROC

PROC SET_ADDITIONAL_CHECK_BEFORE_PROMPT(SEATS_LOCAL_DATA& data, ADDITIONAL_CHECK_BEFORE_PROMPT cCheck)
	PRINTLN("[AUTO_SHOP_INT_SEATS] Setting ADDITIONAL_CHECK_BEFORE_PROMPT callback from ", GET_THIS_SCRIPT_NAME())
	
	data.private.cCanPlayerUseSeat = cCheck
	
	SET_BIT_ENUM(data.private.iBSGeneral, PSGB_ADDITIONAL_CHECK_BEFORE_PROMPT)
ENDPROC

PROC REMOVE_ADDITIONAL_CHECK_BEFORE_PROMPT(SEATS_LOCAL_DATA& data)
	PRINTLN("[AUTO_SHOP_INT_SEATS] Clearing ADDITIONAL_CHECK_BEFORE_PROMPT callback from ", GET_THIS_SCRIPT_NAME())
	
	CLEAR_BIT_ENUM(data.private.iBSGeneral, PSGB_ADDITIONAL_CHECK_BEFORE_PROMPT)
ENDPROC

PROC SET_ALT_HELP_ENTER(SEATS_LOCAL_DATA& data, USE_ALTERNATIVE_HELP_TEXT cUseText)
	PRINTLN("[AUTO_SHOP_INT_SEATS] Setting USE_ALTERNATIVE_HELP_TEXT cAltHelpEnter callback from ", GET_THIS_SCRIPT_NAME())
	
	data.private.cAltHelpEnter = cUseText
	
	SET_BIT_ENUM(data.private.iBSGeneral, PSGB_ALT_HELP_ENTER)
ENDPROC

PROC REMOVE_ALT_HELP_ENTER(SEATS_LOCAL_DATA& data)
	PRINTLN("[AUTO_SHOP_INT_SEATS] Clearing USE_ALTERNATIVE_HELP_TEXT cAltHelpEnter callback from ", GET_THIS_SCRIPT_NAME())
	
	CLEAR_BIT_ENUM(data.private.iBSGeneral, PSGB_ALT_HELP_ENTER)
ENDPROC

PROC SET_ALT_HELP_EXIT(SEATS_LOCAL_DATA& data, USE_ALTERNATIVE_HELP_TEXT cUseText)
	PRINTLN("[AUTO_SHOP_INT_SEATS] Setting USE_ALTERNATIVE_HELP_TEXT cAltHelpExit callback from ", GET_THIS_SCRIPT_NAME())
	
	data.private.cAltHelpExit = cUseText
	
	SET_BIT_ENUM(data.private.iBSGeneral, PSGB_ALT_HELP_EXIT)
ENDPROC

PROC REMOVE_ALT_HELP_EXIT(SEATS_LOCAL_DATA& data)
	PRINTLN("[AUTO_SHOP_INT_SEATS] Clearing USE_ALTERNATIVE_HELP_TEXT cAltHelpEnter callback from ", GET_THIS_SCRIPT_NAME())
	
	CLEAR_BIT_ENUM(data.private.iBSGeneral, PSGB_ALT_HELP_EXIT)
ENDPROC

PROC SEATS_BLOCK_DEFAULT_EXIT_THIS_FRAME(SEATS_LOCAL_DATA& data)
	SET_BIT_ENUM(data.private.iBSGeneral, PSGB_BLOCK_EXIT_THIS_FRAME)
ENDPROC

PROC SEATS_LEAVE_SEAT(SEATS_LOCAL_DATA& data)
	PRINTLN("[AUTO_SHOP_INT_SEATS] Setting PSGB_GET_OUT_OF_SEAT : A script wants the player to leave the seat as if player chose to leave.")
	
	SET_BIT_ENUM(data.private.iBSGeneral, PSGB_GET_OUT_OF_SEAT)
ENDPROC

FUNC BOOL IS_PLAYER_IN_ANY_SEAT(SEATS_LOCAL_DATA& data, BOOL bIncludeEntryAnim = FALSE, BOOL bIncludeExitAnim = FALSE)
	IF (data.private.eState > SS_PROMPT)
	AND (bIncludeEntryAnim OR IS_BIT_SET_ENUM(data.private.iBSGeneral, PSGB_ENTRY_COMPLETE))
	AND (bIncludeExitAnim OR data.private.eState < SS_CLEANUP)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_IN_SEAT(SEATS_LOCAL_DATA& data, INT iSeatIndex, BOOL bIncludeEntryAnim = FALSE, BOOL bIncludeExitAnim = FALSE)
	IF (data.private.iInSeatLocate = iSeatIndex)
	AND IS_PLAYER_IN_ANY_SEAT(data, bIncludeEntryAnim, bIncludeExitAnim)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_SEAT_INDEX_OF_LOCATE_I_AM_IN(SEATS_LOCAL_DATA& data)
	RETURN data.private.iInSeatLocate
ENDFUNC
