//////////////////////////////////////////////////////////////////////////////////////////
// Name:        AM_HOLD_UP																//
// Description: Player enters a store and holds it up to get some cash			 		//
// Written by:  Ryan Baker																//
// Date: 07/04/2010																		//
//////////////////////////////////////////////////////////////////////////////////////////

//************************************************//
//** PLACES THAT DATA IS ADDED FOR NEW HOLD UPS **//

//* mp_globals_ambience.sch
//- CRIM_HOLD_UP_POINT
//- START_TIMER_SHOP247

//* net_ambience.sch
//- HOLD_UP_DETAILS

//* crim_amb_launcher.sc
//- UPDATE_CRIM_AMBIENT_WIDGETS
//- SET_NEAREST_HOLDUP_TIMER
//- INITIALISE_CRIMINAL_AMBIENCE
//- GET_NEAREST_HOLD_UP_TIMERS

//* AM_HOLD_UP.sc
//- GET_STORED_DATA

//												  //
//************************************************//


USING "globals.sch"

// Game Headers
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_path.sch"
USING "commands_interiors.sch"
USING "dialogue_public.sch"
USING "commands_event.sch"
USING "commands_money.sch"

// Network Headers
USING "net_events.sch"
USING "net_spawn.sch"
USING "net_hold_ups.sch"
USING "net_hold_up_menu.sch"

// Net Headers
USING "net_mission.sch"
USING "net_scoring_common.sch"
USING "net_ambience.sch"
USING "help_at_location.sch"
USING "commands_object.sch"
USING "net_xp_animation.sch"
USING "achievement_public.sch"
USING "socialclub_leaderboard.sch"

USING "net_wait_zero.sch"

USING "net_cash_transactions.sch"

USING "net_gang_boss.sch"

USING "net_synced_ambient_pickups.sch"

#IF IS_DEBUG_BUILD
USING "net_debug_log.sch"
#ENDIF

///////////////////////////////////////////////////////////
////////////////// VARIABLES FOR MISSION //////////////////

//CONST_INT NUM_MISSION_PLAYERS	= 


CONST_INT SHOUT_DECREASE_DELAY			50		//500
CONST_FLOAT SHOUT_DECREASE_AMOUNT		0.02	//2

CONST_INT ATTACK_CHANCE_SHOP_247		1
CONST_INT ATTACK_CHANCE_LIQUOR			3
CONST_INT ATTACK_CHANCE_GAS_STATION		2

//INT iNetTimer = 0

// Game States. 
CONST_INT GAME_STATE_INI 				0
CONST_INT GAME_STATE_INI_SPAWN			1
CONST_INT GAME_STATE_RUNNING			2
//CONST_INT GAME_STATE_TUT_LEAVE_AREA		3
CONST_INT GAME_STATE_LEAVE				4
CONST_INT GAME_STATE_TERMINATE_DELAY	5
CONST_INT GAME_STATE_END				6

ENUM HERO_STAGE_ENUM
	eHERO_CREATED,
	eHERO_WAIT,
	eHERO_GET_INTO_POSITION,
	eHERO_WARNING,
	eHERO_ATTACK,
	eHERO_HANDS_UP,
	eHERO_CLEANUP
ENDENUM

//Server Bitset Data
//CONST_INT biS_AnyCrewArrested	0
//CONST_INT biS_AllCrewArrested	1
CONST_INT biS_AllCrewDead		2
CONST_INT biS_AllCrewLeftArea	3

CONST_INT biS_AnyCrewHasWeaponDrawn				4
CONST_INT biS_AnyCrewHasDamagedStaff			5
CONST_INT biS_AnyCrewHasDemandedCash			6
CONST_INT biS_StaffReactToWeaponDone			7
//CONST_INT biS_CashPickupCreated					8
CONST_INT biS_SyncSceneStarted					9
CONST_INT biS_CashBagDroppedEarly				10
CONST_INT biS_CashPickedUp						11
CONST_INT biS_AnyCrewHasWeaponDrawnNearStaff	12
CONST_INT biS_AnyCrewInShop						13
CONST_INT biS_Line4Done							14
CONST_INT biS_Line5Done							15
CONST_INT biS_AnyCrewHasAimedAtStaff			16
CONST_INT biS_SyncSceneStartedRunning			17
CONST_INT biS_StaffDeadAndBagEmpty				18
CONST_INT biS_CashBagDroppedAndEmpty			19
CONST_INT biS_CashBagAvailableForPickup			20
CONST_INT biS_StaffDead							21
CONST_INT biS_ReactToShoutLineDone				22
CONST_INT biS_ReactToGunShotLineDone			23
CONST_INT biS_ReactToShoutLineTrigger			24
CONST_INT biS_ReactToGunShotLineTrigger			25
CONST_INT biS_ReactToGunLineDone				26
CONST_INT biS_AnyCrewInBackRoom					27
CONST_INT biS_AnyCrewAtEntrance					28
CONST_INT biS_SwapTillBroadcast					29
CONST_INT biS_ReactToWantedLevelLineDone		30
CONST_INT biS_ReactToUnarmedAimLineDone			31

CONST_INT biS2_AnyCrewWithWantedLevelInShop		0
CONST_INT biS2_AnyCrewHasUnarmedAimedAtStaff	1
CONST_INT biS2_DoStaffAttackCheck				2
CONST_INT biS2_MakeStaffAttack					3
CONST_INT biS2_AnyCrewIsAimingAtStaff			4
CONST_INT biS2_CopsArrived						5
CONST_INT biS2_ReactToCopArrival				6
CONST_INT biS2_HoldUpDoneCheck					7
CONST_INT biS2_SnackBuyCheck					8
CONST_INT biS2_ReactToSnackLineDone				9
CONST_INT biS2_StaffDeadReducedReservation		10
CONST_INT biS2_IsNewShift						11
CONST_INT biS2_IsEndShift						12
CONST_INT biS2_RestrictedAreaCheck				13
CONST_INT biS2_RestrictedAreaDone				14
CONST_INT biS2_CashBagMadeVisible				15
CONST_INT biS2_RegisterRobberManually			16
CONST_INT biS2_CashBagPreventCollectionCleared	17

CONST_INT biSH_TryToGrabHero					0
CONST_INT biSH_GotHero							1
//CONST_INT biSH_MadeHeroAttack					2
CONST_INT biSH_WarningLine						3
CONST_INT biSH_AttackLine						4
CONST_INT biSH_TurnForWarning					5
CONST_INT biSH_WarningLineDone					6
CONST_INT biSH_AttackLineDone					7
CONST_INT biSH_AnyCrewHasDamagedHero			8
CONST_INT biSH_AnyCrewHasAimedAtHero			9

// The server broadcast data.
// Everyone can read this data, only the server can update it
STRUCT ServerBroadcastData
	ServerBroadcastDataMenu sMenuData
	
	INT iServerGameState
		
//	NETWORK_INDEX niRegStaff
	NETWORK_INDEX niCashBag
	NETWORK_INDEX niTill
	NETWORK_INDEX niHero
	
	NETWORK_INDEX niSnacks[SNACK_TOTAL]
	
	INT iServerBitSet
	INT iServerBitSet2
	INT iServerHeroBitSet
	
//	INT iServerSnackBitSet
	INT iServerWhichSnack
	INT iSnackSCeneID
//	SNACK_BUY_ANIM_STATE eSnackBuyAnimState = SNACKBUY_DONE
	
	INT iReaction
	INT iSceneID
	INT iShockingEvent = 0
	INT iStaffDialogueSet
	INT iAggressivePlayer = -1
	
	FLOAT fCurrentShout = 0.0
	SCRIPT_TIMER iShoutDecreaseTimer
	
	INT iCashShare
	FLOAT fCashBagFillAmount = 0.0
	VECTOR vPickedUpCashBagPos
	INT iNumPlayersInStore
	
	FLOAT fRegStaffHeading
	
	INT iReservedPeds = 1
	INT iReservedObjects = 8
	
//	STAFF_STAGE_ENUM eStaffState
	INT iStaffVariation
	TEXT_LABEL_15 tlStaffVoice
	TEXT_LABEL_63 tlStaffAmbientVoice
	
	HERO_STAGE_ENUM eHeroState
	SCRIPT_TIMER iHeroAttackTimer
	
	SCRIPT_TIMER MissionTerminateDelayTimer
	
//	INT iTotalCashSpent
//	INT iSnackInventory[SNACK_TOTAL]
	FLOAT fCopDelay = 20.0
	
	//PED_INDEX PlayerInControlOfStaff
	PLAYER_INDEX StaffKiller
ENDSTRUCT
ServerBroadcastData serverBD





CONST_INT biPH_TurnForWarning			0
CONST_INT biPH_WarningLineDone			1
CONST_INT biPH_AttackLineDone			2
CONST_INT biPH_PlayerHasAimedAtHero		3

// The client broadcast data.
// Everyone can read this data, only the local player only the server can update it

STRUCT PlayerBroadcastData
	PlayerBroadcastDataMenu sMenuData
	
	INT iGameState
	
//	INT iPlayerBitSet
	INT iPlayerHeroBitSet
	
//	INT iPlayerSnackBitSet
//	INT iPlayerWhichSnack
	
//	INT iCashSpent
	
	FLOAT fShout
	VECTOR vPickedUpCashBagPos
ENDSTRUCT
PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]

///// MISSION VARIABLES /////

MP_SHOP_SNACKS shopSnacks

BOOL bSnackHeadingReached

STRING sHeadAdditiveAnim

MODEL_NAMES StaffModel = MP_M_ShopKeep_01

VECTOR vRegStaff
//STRING sRegStaffRoom
VECTOR vBackRoomCower
FLOAT fBackRoomCowerHeading

INT iLocalSceneID = -1
VECTOR scenePosition
VECTOR sceneRotation

INT iLocalSnackSceneID = -1

//SEQUENCE_INDEX StaffAttackSeq
SEQUENCE_INDEX StaffEmptyRegisterSeq
SEQUENCE_INDEX StaffHandsUpSeq
SEQUENCE_INDEX StaffHandsUpLoopSeq
SEQUENCE_INDEX StaffFleeToBackRoomSeq
SEQUENCE_INDEX StaffFleeToBackRoomAttackSeq
SEQUENCE_INDEX StaffCowerInPlace

//INT iLocalCashShare
INT iStaggeredParticipant
INT iSnackOrderer
INT iSnackShoplifter

//BLIP_INDEX HoldUpBlip

//MODEL_NAMES CashBagModel = P_Poly_Bag_01_S	//PROP_POLY_BAG_01 //GET_HOLD_UP_MODEL(HUP_MODEL_CASH_BAG)	//PROP_PAPER_BAG_01	//PROP_POLY_BAG_01	//PROP_POLY_BAG_MONEY	//GET_HOLD_UP_MODEL(HUP_MODEL_CASH_BAG)	//PROP_PAPER_BAG_01
VECTOR vCashBagPos
FLOAT fCashBagHeading

//MODEL_NAMES TillModel = GET_HOLD_UP_MODEL(HUP_MODEL_TILL)
VECTOR vTillPos
FLOAT fTillHeading
MODEL_NAMES ArtTill = prop_till_01	//v_ret_gc_cashreg	//PROP_TILL_02

//** TWH - RLB - (fix for bug 1432522 - double hold up cash).
INT iMinCash = ROUND(g_sMPTunables.fearnings_Low_Hold_Ups_modifier) 		//1000	//600 
INT iMaxCash = (ROUND(g_sMPTunables.fearnings_High_Hold_Ups_modifier)+1) 	//2001	//1801
INT iRegStaffAttackChance = 1
//INT iExtraStaffAttackChance = 2

//CONST_INT iMinKillCash	200
//CONST_INT iMaxKillCash	601

//INT iDelayEndTimer
SCRIPT_TIMER iTurnTimer
SCRIPT_TIMER iMeleeTimer

INTERIOR_INSTANCE_INDEX interiorID
INT iRoomHashMain
INT iRoomHashMain2
INT iRoomHashBack
VECTOR vStoreBlipLocation
VECTOR vDoorCoord

VECTOR vStoreMin
VECTOR vStoreMax
FLOAT fStoreWidth

VECTOR vBackRoomMin
VECTOR vBackRoomMax
FLOAT fBackRoomWidth


structPedsForConversation sSpeech

//INT iHoldUpCounter = 0
//FLOAT fShortestDistance = 9999.9


INT iStaffDraw[5]
INT iStaffTexture[5]

FLOAT fSyncSceneRate
SCRIPT_TIMER iSyncSceneDelayTimer
SCRIPT_TIMER iSnackSyncSceneDelayTimer
SCRIPT_TIMER iSnackFailSifeTimer
CONST_INT SNACK_SYNC_FAILSAFE 2000
CONST_INT SYNC_SCENE_DELAY	100	//500
SCRIPT_TIMER iSyncSceneTurnTimer
SCRIPT_TIMER iSnackSyncSceneTurnTimer
CONST_INT SYNC_SCENE_TURN_DELAY	1500
SCRIPT_TIMER iSnackMoveItemTimer
CONST_INT SYNC_SCENE_SNACK_DELAY 1000
CONST_INT SYNC_SCENE_SNACK_BUFFER 125

//FLOAT fIntimSyncSceneRate
//INT iIntimTarget
//INT iIntimCurrent = 25
//CONST_INT INTIM_INCREMENT	1

SCRIPT_TIMER TryToGetHeroTimer
BLIP_INDEX HeroBlip

INT iEarnedXP
INT iEarnedCash


/*#IF IS_DEBUG_BUILD
#IF FEATURE_HEIST_PLANNING
//SCRIPT_TIMER iLotteryBuyTicketDelay
CONST_INT BUY_LOTTERY_DELAY	120000
INT iLotteryTicketsJustBought
//INT iLotteryPreviousWeek
INT iLotteryTicketsAlreadyBought

INT iLotteryEndCheckStage
CONST_INT LECS_WAIT			0
CONST_INT LECS_SAVE			1
CONST_INT LECS_CHECK_SAVE	2
CONST_INT LECS_WRITE		3
CONST_INT LECS_REFUND		4

INT iLotteryBitSet
CONST_INT biL_DoStartChecks				0
CONST_INT biL_WeekChanged				1
CONST_INT biL_TicketsBought				2
CONST_INT biL_BuyingTicketsDisabled		3
CONST_INT biL_SaveFailed				4
CONST_INT biL_RefundDone				5
CONST_INT biL_WriteDone					6
CONST_INT biL_LeaderboardTestPassed		7
CONST_INT biL_LeaderboardTestFailed		8

{----- Lottery -----} //TEXT
[SNK_ITEM7:SNK_MNU]
Lottery Ticket
[SNK_ITEM7_D:SNK_MNU]
Are you feelin' lucky?
[SNK_LOTTFU:SNK_MNU]
DONE
[SNK_LOTCLS:SNK_MNU]
Lottery Closed

#ENDIF
#ENDIF*/

// Widgets & Debug.
#IF IS_DEBUG_BUILD
	BOOL bHostEndMissionNow
	BOOL bTryToGrabHeroDebug
#ENDIF

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  MAIN PROCS                            ////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

FUNC INT SMASH_AND_GRAB_WANTED_CAP()
	RETURN g_sMPTunables.igb_smashandgrab_wanted_cap
ENDFUNC

FUNC BOOL GB_IS_LOCAL_PLAYER_ON_SMASH_AND_GRAB()
	IF GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID())
	AND GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_GB_CHAL_ROB_SHOP
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC



//PURPOSE: DEBUG Check for bug 442780
PROC SHOP_KEEPER_ALIVE_CHECK_ASSERT(INT iNum)
	IF IS_NET_PED_INJURED(serverBD.sMenuData.niRegStaff)
		TEXT_LABEL_63 tlAssert = "SHOP_KEEPER_ALIVE_CHECK_ASSERT - Check "
		tlAssert += iNum
		NET_SCRIPT_ASSERT(tlAssert)
	ENDIF
ENDPROC

//Grab data for the store the player is in
FUNC BOOL GOT_NEAREST_STORE_DATA()
	
	//IF GET_NEAREST_HOLD_UP_AREA(iHoldUpCounter, fShortestDistance, shopSnacks.NearestHoldUpPoint)
	IF GET_NEAREST_HOLD_UP_AREA_LOOP(shopSnacks.NearestHoldUpPoint)
	
		//NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - iHoldUpCounter = ") NET_PRINT_INT(iHoldUpCounter) NET_NL()
		NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - shopSnacks.NearestHoldUpPoint = ") NET_PRINT_INT(ENUM_TO_INT(shopSnacks.NearestHoldUpPoint)) NET_NL()
		
		//VECTOR vSyncSceneOffset = <<-0.738572, 0.915935, -1.01>>	//-0.51>>	//-0.010000>>
		//VECTOR vTempSyncSceneOffset = <<0.738572, -0.915935, 0.0>>
		//VECTOR vCashBagOffset = <<-0.040857, 0.366089-0.066, -0.428174>>
		
		HOLD_UP_DETAILS(shopSnacks.NearestHoldUpPoint, interiorID, iRoomHashMain, iRoomHashMain2, iRoomHashBack, vStoreBlipLocation, vDoorCoord)
		HOLD_UP_DETAILS_MANUAL(shopSnacks.NearestHoldUpPoint, vStoreMin, vStoreMax, fStoreWidth)
		HOLD_UP_BACKROOM_DETAILS(shopSnacks.NearestHoldUpPoint, vBackRoomMin, vBackRoomMax, fBackRoomWidth)
		
		SWITCH shopSnacks.NearestHoldUpPoint
			
			CASE eCRIM_HUP_SHOP247_1
				scenePosition = <<1393.10, 3605.89, 35.20-0.2>>	//1393.128, 3605.960, 34.991 >>
				sceneRotation = <<0, 0, 21.89>>
				StaffModel = GET_HOLD_UP_MODEL(HUP_MODEL_STAFF)
				//vRegStaff = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(scenePosition, sceneRotation.z, vSyncSceneOffset)
				////vRegStaff = (scenePosition + vSyncSceneOffset)	//1392.2279, 3606.6104, 33.9809>>
				//fRegStaffHeading = (180 + sceneRotation.z) 		//201.6640
				vBackRoomCower = <<1394.8521, 3609.5088, 33.9809>> 	//<<1392.6583, 3610.8711, 33.9809>>
				fBackRoomCowerHeading = 116.5329					//328.0416
				//iMinCash = 600
				//iMaxCash = 1801
				iRegStaffAttackChance = ATTACK_CHANCE_SHOP_247
				
				//Ped Components
				IF IS_TIME_BETWEEN_THESE_HOURS(6, 18)
					iStaffDraw[0]		= 0
					iStaffTexture[0]	= 0
					iStaffDraw[1]		= 0
					iStaffTexture[1]	= 0
					iStaffDraw[2]		= 0
					iStaffTexture[2]	= 0
					iStaffDraw[3]		= 0
					iStaffTexture[3]	= 0
					iStaffDraw[4]		= 0
					iStaffTexture[4] 	= 0
				ELSE
					iStaffDraw[0]		= 1
					iStaffTexture[0]	= 0
					iStaffDraw[1]		= 1
					iStaffTexture[1]	= 1
					iStaffDraw[2]		= 1
					iStaffTexture[2]	= 2
					iStaffDraw[3]		= 0
					iStaffTexture[3]	= 1
					iStaffDraw[4]		= 0
					iStaffTexture[4] 	= 0
				ENDIF
				
				//vCashBagPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(scenePosition, sceneRotation.z, vCashBagOffset)
				//ArtTill = v_ret_gc_cashreg
			BREAK
			
			CASE eCRIM_HUP_SHOP247_2
				sceneRotation = <<0, 0, -160>>	//180+25.9167 >>
				scenePosition = <<-3041.38, 584.35, 8.11-0.2>>	//-3041.699, 584.222, 7.952>>
				StaffModel = GET_HOLD_UP_MODEL(HUP_MODEL_STAFF)
				//vRegStaff = <<-3040.4194, 583.6247, 6.9048>> 
				//fRegStaffHeading = 25.9167
				vBackRoomCower = <<-3047.5117, 588.9807, 6.9089>> 
				fBackRoomCowerHeading = 178.8753
				//iMinCash = 600
				//iMaxCash = 1801
				iRegStaffAttackChance = ATTACK_CHANCE_SHOP_247
				//vCashBagPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(scenePosition, sceneRotation.z, vCashBagOffset)
				
				//Ped Components
				IF IS_TIME_BETWEEN_THESE_HOURS(6, 18)
					iStaffDraw[0]		= 0
					iStaffTexture[0]	= 1
					iStaffDraw[1]		= 1
					iStaffTexture[1]	= 0
					iStaffDraw[2]		= 0
					iStaffTexture[2]	= 1
					iStaffDraw[3]		= 0
					iStaffTexture[3]	= 1
					iStaffDraw[4]		= 0
					iStaffTexture[4] 	= 1
				ELSE
					iStaffDraw[0]		= 2
					iStaffTexture[0]	= 0
					iStaffDraw[1]		= 0
					iStaffTexture[1]	= 0
					iStaffDraw[2]		= 1
					iStaffTexture[2]	= 2
					iStaffDraw[3]		= 0
					iStaffTexture[3]	= 0
					iStaffDraw[4]		= 0
					iStaffTexture[4] 	= 1
				ENDIF
				
				//ArtTill = v_ret_gc_cashreg
			BREAK
			
			CASE eCRIM_HUP_SHOP247_3
				scenePosition = <<-3244.56, 1000.74, 13.03-0.2>>	// -3244.589, 1000.726, 12.840 >>
				sceneRotation = <<0, 0, 175.52>>
				StaffModel = GET_HOLD_UP_MODEL(HUP_MODEL_STAFF)
				//vRegStaff = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(scenePosition, sceneRotation.z, vSyncSceneOffset)
				////vRegStaff = (scenePosition + vSyncSceneOffset)	//<<-3243.8020, 999.6802, 11.8307>>
				//fRegStaffHeading = (180 + sceneRotation.z)		//0.2528
				vBackRoomCower = <<-3249.1143, 1006.5576, 11.8307>>		//-3248.2161, 1007.4696, 11.8307>> 
				fBackRoomCowerHeading = 191.5940						//125.1173
				//iMinCash = 600
				//iMaxCash = 1801
				iRegStaffAttackChance = ATTACK_CHANCE_SHOP_247
				
				//Ped Components
				IF IS_TIME_BETWEEN_THESE_HOURS(6, 18)
					iStaffDraw[0]		= 0
					iStaffTexture[0]	= 2
					iStaffDraw[1]		= 1
					iStaffTexture[1]	= 1
					iStaffDraw[2]		= 0
					iStaffTexture[2]	= 2
					iStaffDraw[3]		= 0
					iStaffTexture[3]	= 0
					iStaffDraw[4]		= 0
					iStaffTexture[4] 	= 2
				ELSE
					iStaffDraw[0]		= 2
					iStaffTexture[0]	= 1
					iStaffDraw[1]		= 0
					iStaffTexture[1]	= 0
					iStaffDraw[2]		= 1
					iStaffTexture[2]	= 0
					iStaffDraw[3]		= 0
					iStaffTexture[3]	= 1
					iStaffDraw[4]		= 0
					iStaffTexture[4] 	= 2
				ENDIF
				
				//vCashBagPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(scenePosition, sceneRotation.z, vCashBagOffset)
				//ArtTill = v_ret_gc_cashreg
			BREAK
			
			CASE eCRIM_HUP_SHOP247_4
				sceneRotation = <<0, 0, -82.38>>	//180+109.5189 >>
				scenePosition = <<548.82, 2668.93, 42.36-0.2>>	//548.830, 2669.050, 42.150>>
				StaffModel = GET_HOLD_UP_MODEL(HUP_MODEL_STAFF)
				//vRegStaff = <<549.6904, 2669.9011, 41.1565>>
				//fRegStaffHeading = 109.5189
				vBackRoomCower = <<543.0796, 2663.9673, 41.1565>>
				fBackRoomCowerHeading = 228.4295
				//iMinCash = 600
				//iMaxCash = 1801
				iRegStaffAttackChance = ATTACK_CHANCE_SHOP_247
				
				//Ped Components
				IF IS_TIME_BETWEEN_THESE_HOURS(6, 18)
					iStaffDraw[0]		= 1
					iStaffTexture[0]	= 0
					iStaffDraw[1]		= 2
					iStaffTexture[1]	= 0
					iStaffDraw[2]		= 1
					iStaffTexture[2]	= 0
					iStaffDraw[3]		= 0
					iStaffTexture[3]	= 1
					iStaffDraw[4]		= 0
					iStaffTexture[4] 	= 0
				ELSE
					iStaffDraw[0]		= 0
					iStaffTexture[0]	= 0
					iStaffDraw[1]		= 2
					iStaffTexture[1]	= 1
					iStaffDraw[2]		= 1
					iStaffTexture[2]	= 1
					iStaffDraw[3]		= 0
					iStaffTexture[3]	= 0
					iStaffDraw[4]		= 0
					iStaffTexture[4] 	= 0
				ENDIF
				
				//vCashBagPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(scenePosition, sceneRotation.z, vCashBagOffset)
				//ArtTill = v_ret_gc_cashreg
			BREAK
			
			CASE eCRIM_HUP_SHOP247_5
				sceneRotation = <<0, 0, 180>>
				scenePosition = <<2554.88, 381.47, 108.82-0.2>>	//2554.750, 381.460, 108.620>>
				StaffModel = GET_HOLD_UP_MODEL(HUP_MODEL_STAFF)
				//vRegStaff = <<2555.6887, 380.4448, 107.6230>>
				//fRegStaffHeading = 1.5205
				vBackRoomCower = <<2549.8501, 387.1622, 107.6230>>
				fBackRoomCowerHeading = 197.2994
				//iMinCash = 600
				//iMaxCash = 1801
				iRegStaffAttackChance = ATTACK_CHANCE_SHOP_247
				
				//Ped Components
				IF IS_TIME_BETWEEN_THESE_HOURS(6, 18)
					iStaffDraw[0]		= 2
					iStaffTexture[0]	= 0
					iStaffDraw[1]		= 2
					iStaffTexture[1]	= 1
					iStaffDraw[2]		= 1
					iStaffTexture[2]	= 1
					iStaffDraw[3]		= 0
					iStaffTexture[3]	= 0
					iStaffDraw[4]		= 0
					iStaffTexture[4] 	= 1
				ELSE
					iStaffDraw[0]		= 0
					iStaffTexture[0]	= 1
					iStaffDraw[1]		= 1
					iStaffTexture[1]	= 0
					iStaffDraw[2]		= 0
					iStaffTexture[2]	= 2
					iStaffDraw[3]		= 0
					iStaffTexture[3]	= 1
					iStaffDraw[4]		= 0
					iStaffTexture[4] 	= 1
				ENDIF
				
				//vCashBagPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(scenePosition, sceneRotation.z, vCashBagOffset)
				//ArtTill = v_ret_gc_cashreg
			BREAK
			
			CASE eCRIM_HUP_SHOP247_6
				sceneRotation = <<0, 0, 155>>	//180+334.8105 >>
				scenePosition = <<2676.26, 3281.04, 55.44-0.2>>	//2676.190, 3281.050, 55.250>>
				StaffModel = GET_HOLD_UP_MODEL(HUP_MODEL_STAFF)
				//vRegStaff = <<2676.4375, 3279.8049, 54.2411>>
				//fRegStaffHeading = 334.8105
				vBackRoomCower = <<2671.3508, 3283.1360, 54.2411>>
				fBackRoomCowerHeading = 296.5427
				//iMinCash = 600
				//iMaxCash = 1801
				iRegStaffAttackChance = ATTACK_CHANCE_SHOP_247
				
				//Ped Components
				IF IS_TIME_BETWEEN_THESE_HOURS(6, 18)
					iStaffDraw[0]		= 2
					iStaffTexture[0]	= 1
					iStaffDraw[1]		= 0
					iStaffTexture[1]	= 0
					iStaffDraw[2]		= 1
					iStaffTexture[2]	= 2
					iStaffDraw[3]		= 0
					iStaffTexture[3]	= 1
					iStaffDraw[4]		= 0
					iStaffTexture[4] 	= 2
				ELSE
					iStaffDraw[0]		= 0
					iStaffTexture[0]	= 2
					iStaffDraw[1]		= 2
					iStaffTexture[1]	= 1
					iStaffDraw[2]		= 0
					iStaffTexture[2]	= 2
					iStaffDraw[3]		= 0
					iStaffTexture[3]	= 0
					iStaffDraw[4]		= 0
					iStaffTexture[4] 	= 2
				ENDIF
				
				//vCashBagPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(scenePosition, sceneRotation.z, vCashBagOffset)
				//ArtTill = v_ret_gc_cashreg
			BREAK
			
			CASE eCRIM_HUP_SHOP247_7
				sceneRotation = <<0, 0, 63.3>>	//180+251.2202 >>
				scenePosition = <<1729.40, 6417.08, 35.24-0.2>>	//1729.450, 6417.100, 35.040>>
				StaffModel = GET_HOLD_UP_MODEL(HUP_MODEL_STAFF)
				//vRegStaff = <<1728.1240, 6416.7378, 34.0372>>
				//fRegStaffHeading = 251.2202
				vBackRoomCower = <<1733.9670, 6421.4951, 34.0372>>
				fBackRoomCowerHeading = 130.9518
				//iMinCash = 600
				//iMaxCash = 1801
				iRegStaffAttackChance = ATTACK_CHANCE_SHOP_247
				
				//Ped Components
				IF IS_TIME_BETWEEN_THESE_HOURS(6, 18)
					iStaffDraw[0]		= 2
					iStaffTexture[0]	= 2
					iStaffDraw[1]		= 1
					iStaffTexture[1]	= 0
					iStaffDraw[2]		= 0
					iStaffTexture[2]	= 0
					iStaffDraw[3]		= 0
					iStaffTexture[3]	= 0
					iStaffDraw[4]		= 0
					iStaffTexture[4] 	= 0
				ELSE
					iStaffDraw[0]		= 1
					iStaffTexture[0]	= 0
					iStaffDraw[1]		= 0
					iStaffTexture[1]	= 0
					iStaffDraw[2]		= 1
					iStaffTexture[2]	= 0
					iStaffDraw[3]		= 0
					iStaffTexture[3]	= 1
					iStaffDraw[4]		= 0
					iStaffTexture[4] 	= 0
				ENDIF
				
				//vCashBagPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(scenePosition, sceneRotation.z, vCashBagOffset)
				//ArtTill = v_ret_gc_cashreg
			BREAK
			
			CASE eCRIM_HUP_SHOP247_8
				sceneRotation = <<0, 0, 117>>	//180+302.9095 >>
				scenePosition = <<1959.40, 3742.33, 32.54-0.2>>	//1959.360, 3742.350, 32.350>>
				StaffModel = GET_HOLD_UP_MODEL(HUP_MODEL_STAFF)
				//vRegStaff = <<1959.0765, 3740.9016, 31.3438>>
				//fRegStaffHeading = 302.9095
				vBackRoomCower = <<1958.9199, 3746.2673, 31.3438>>
				fBackRoomCowerHeading = 73.6245
				//iMinCash = 600
				//iMaxCash = 1801
				iRegStaffAttackChance = ATTACK_CHANCE_SHOP_247
				
				//Ped Components
				IF IS_TIME_BETWEEN_THESE_HOURS(6, 18)
					iStaffDraw[0]		= 0
					iStaffTexture[0]	= 0
					iStaffDraw[1]		= 1
					iStaffTexture[1]	= 1
					iStaffDraw[2]		= 0
					iStaffTexture[2]	= 1
					iStaffDraw[3]		= 0
					iStaffTexture[3]	= 1
					iStaffDraw[4]		= 0
					iStaffTexture[4] 	= 1
				ELSE
					iStaffDraw[0]		= 2
					iStaffTexture[0]	= 0
					iStaffDraw[1]		= 0
					iStaffTexture[1]	= 0
					iStaffDraw[2]		= 1
					iStaffTexture[2]	= 1
					iStaffDraw[3]		= 0
					iStaffTexture[3]	= 0
					iStaffDraw[4]		= 0
					iStaffTexture[4] 	= 1
				ENDIF
				
				//vCashBagPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(scenePosition, sceneRotation.z, vCashBagOffset)
				//ArtTill = v_ret_gc_cashreg
			BREAK
			
			CASE eCRIM_HUP_SHOP247_9
				sceneRotation = <<0, 0, 92>>	//180+272.4298 >>
				scenePosition = <<25.03, -1344.96, 29.69-0.2>>	//22.910, -1344.350, 29.490>>+<<2, -0.4, 0>>
				StaffModel = GET_HOLD_UP_MODEL(HUP_MODEL_STAFF)
				//vRegStaff = <<21.9484, -1345.4098, 28.4939>>
				//fRegStaffHeading = 272.4298
				vBackRoomCower = <<30.5721, -1339.7816, 28.4939>>
				fBackRoomCowerHeading = 110.7699
				//iMinCash = 600
				//iMaxCash = 1801
				iRegStaffAttackChance = ATTACK_CHANCE_SHOP_247
				
				//Ped Components
				IF IS_TIME_BETWEEN_THESE_HOURS(6, 18)
					iStaffDraw[0]		= 0
					iStaffTexture[0]	= 1
					iStaffDraw[1]		= 2
					iStaffTexture[1]	= 0
					iStaffDraw[2]		= 0
					iStaffTexture[2]	= 2
					iStaffDraw[3]		= 0
					iStaffTexture[3]	= 0
					iStaffDraw[4]		= 0
					iStaffTexture[4] 	= 2
				ELSE
					iStaffDraw[0]		= 2
					iStaffTexture[0]	= 1
					iStaffDraw[1]		= 1
					iStaffTexture[1]	= 0
					iStaffDraw[2]		= 1
					iStaffTexture[2]	= 2
					iStaffDraw[3]		= 0
					iStaffTexture[3]	= 1
					iStaffDraw[4]		= 0
					iStaffTexture[4] 	= 2
				ENDIF
				
				//vCashBagPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(scenePosition, sceneRotation.z, vCashBagOffset)
				//ArtTill = v_ret_gc_cashreg
			BREAK
			
			CASE eCRIM_HUP_SHOP247_10
				sceneRotation = <<0, 0, 75>>	//180+260.9120 >>
				scenePosition = <<373.68, 328.56, 103.77-0.2>>	//373.660, 328.550, 103.570>>
				StaffModel = GET_HOLD_UP_MODEL(HUP_MODEL_STAFF)
				//vRegStaff = <<372.4784, 327.8670, 102.5664>>
				//fRegStaffHeading = 260.9120
				vBackRoomCower = <<376.2976, 331.8158, 102.5664>>
				fBackRoomCowerHeading = 52.0064
				//iMinCash = 600
				//iMaxCash = 1801
				iRegStaffAttackChance = ATTACK_CHANCE_SHOP_247
				
				//Ped Components
				IF IS_TIME_BETWEEN_THESE_HOURS(6, 18)
					iStaffDraw[0]		= 0
					iStaffTexture[0]	= 2
					iStaffDraw[1]		= 2
					iStaffTexture[1]	= 1
					iStaffDraw[2]		= 1
					iStaffTexture[2]	= 0
					iStaffDraw[3]		= 0
					iStaffTexture[3]	= 1
					iStaffDraw[4]		= 0
					iStaffTexture[4] 	= 0
				ELSE
					iStaffDraw[0]		= 2
					iStaffTexture[0]	= 2
					iStaffDraw[1]		= 1
					iStaffTexture[1]	= 1
					iStaffDraw[2]		= 0
					iStaffTexture[2]	= 0
					iStaffDraw[3]		= 0
					iStaffTexture[3]	= 0
					iStaffDraw[4]		= 0
					iStaffTexture[4] 	= 0
				ENDIF
				
				//vCashBagPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(scenePosition, sceneRotation.z, vCashBagOffset)
				//ArtTill = v_ret_gc_cashreg
			BREAK
			
			
			//LIQUOR STORES
			CASE eCRIM_HUP_LIQUOR_1
				sceneRotation = <<0, 0, 0>>	//180+188.7864 >>
				scenePosition = <<1165.96, 2710.20, 38.25-0.105>>	//1195.000, 2709.700, 38.130>>+<<-29.04, 0.39, 0>>
				StaffModel = GET_HOLD_UP_MODEL(HUP_MODEL_STAFF)
				//vRegStaff = <<1194.0237, 2710.9353, 37.1333>>
				//fRegStaffHeading = 188.7864
				vBackRoomCower = <<1168.9711, 2719.1179, 36.0632>>
				fBackRoomCowerHeading = 136.5945
				//iMinCash = 600
				//iMaxCash = 1801
				iRegStaffAttackChance = ATTACK_CHANCE_LIQUOR
				
				//Ped Components
				IF IS_TIME_BETWEEN_THESE_HOURS(10, 22)
					iStaffDraw[0]		= 1
					iStaffTexture[0]	= 0
					iStaffDraw[1]		= 0
					iStaffTexture[1]	= 0
					iStaffDraw[2]		= 1
					iStaffTexture[2]	= 1
					iStaffDraw[3]		= 0
					iStaffTexture[3]	= 0
					iStaffDraw[4]		= 1
					iStaffTexture[4] 	= 0
				ELSE
					iStaffDraw[0]		= 0
					iStaffTexture[0]	= 0
					iStaffDraw[1]		= 2
					iStaffTexture[1]	= 0
					iStaffDraw[2]		= 0
					iStaffTexture[2]	= 1
					iStaffDraw[3]		= 0
					iStaffTexture[3]	= 1
					iStaffDraw[4]		= 1
					iStaffTexture[4] 	= 0
				ENDIF
				
				//vCashBagPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(scenePosition, sceneRotation.z, vCashBagOffset)
				//ArtTill = v_ret_gc_cashreg
			BREAK
			
			CASE eCRIM_HUP_LIQUOR_2
				sceneRotation = <<0, 0, -92>>	//180+90.8472 >>
				scenePosition = <<-2967.03, 390.90, 15.23-0.2>>	//-2967.015, 390.900, 15.040>>
				StaffModel = GET_HOLD_UP_MODEL(HUP_MODEL_STAFF)
				//vRegStaff = <<-2966.0461, 391.4274, 14.0433>>
				//fRegStaffHeading = 90.8472
				vBackRoomCower = <<-2962.9829, 391.9788, 14.0433>>
				fBackRoomCowerHeading = 176.1174 
				//iMinCash = 600
				//iMaxCash = 1801
				iRegStaffAttackChance = ATTACK_CHANCE_LIQUOR
				
				//Ped Components
				IF IS_TIME_BETWEEN_THESE_HOURS(10, 22)
					iStaffDraw[0]		= 2
					iStaffTexture[0]	= 0
					iStaffDraw[1]		= 1
					iStaffTexture[1]	= 0
					iStaffDraw[2]		= 1
					iStaffTexture[2]	= 2
					iStaffDraw[3]		= 0
					iStaffTexture[3]	= 1
					iStaffDraw[4]		= 1
					iStaffTexture[4] 	= 0
				ELSE
					iStaffDraw[0]		= 0
					iStaffTexture[0]	= 1
					iStaffDraw[1]		= 2
					iStaffTexture[1]	= 1
					iStaffDraw[2]		= 0
					iStaffTexture[2]	= 2
					iStaffDraw[3]		= 0
					iStaffTexture[3]	= 0
					iStaffDraw[4]		= 1
					iStaffTexture[4] 	= 0
				ENDIF
				
				//vCashBagPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(scenePosition, sceneRotation.z, vCashBagOffset)
				//ArtTill = v_ret_gc_cashreg
			BREAK
			
			CASE eCRIM_HUP_LIQUOR_3
				sceneRotation = <<0, 0, -145>>	//180+45.3706 >>
				scenePosition = <<-1222.33, -907.82, 12.52-0.2>>	//-1222.400, -907.870, 12.320>>
				StaffModel = GET_HOLD_UP_MODEL(HUP_MODEL_STAFF)
				//vRegStaff = <<-1220.9966, -908.1617, 11.3263>>
				//fRegStaffHeading = 45.3706
				vBackRoomCower = <<-1218.2826, -915.7103, 10.3264>>
				fBackRoomCowerHeading = 43.8031
				//iMinCash = 600
				//iMaxCash = 1801
				iRegStaffAttackChance = ATTACK_CHANCE_LIQUOR
				
				//Ped Components
				IF IS_TIME_BETWEEN_THESE_HOURS(10, 22)
					iStaffDraw[0]		= 2
					iStaffTexture[0]	= 1
					iStaffDraw[1]		= 1
					iStaffTexture[1]	= 1
					iStaffDraw[2]		= 0
					iStaffTexture[2]	= 0
					iStaffDraw[3]		= 0
					iStaffTexture[3]	= 0
					iStaffDraw[4]		= 1
					iStaffTexture[4] 	= 0
				ELSE
					iStaffDraw[0]		= 0
					iStaffTexture[0]	= 2
					iStaffDraw[1]		= 1
					iStaffTexture[1]	= 0
					iStaffDraw[2]		= 1
					iStaffTexture[2]	= 0
					iStaffDraw[3]		= 0
					iStaffTexture[3]	= 1
					iStaffDraw[4]		= 1
					iStaffTexture[4] 	= 0
				ENDIF
				
				//vCashBagPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(scenePosition, sceneRotation.z, vCashBagOffset)
				//ArtTill = v_ret_gc_cashreg
			BREAK
			
			CASE eCRIM_HUP_LIQUOR_4
				sceneRotation = <<0, 0, 103>>	//180+287.9766 >>
				scenePosition = <<1134.81, -982.36, 46.60-0.2>>	//1134.780, -982.350, 46.410>>
				StaffModel = GET_HOLD_UP_MODEL(HUP_MODEL_STAFF)
				//vRegStaff = <<1133.6482, -983.5804, 45.4158>>
				//fRegStaffHeading = 287.9766
				vBackRoomCower = <<1130.1548, -979.2816, 45.4158>>
				fBackRoomCowerHeading = 269.2587 
				//iMinCash = 600
				//iMaxCash = 1801
				iRegStaffAttackChance = ATTACK_CHANCE_LIQUOR
				
				//Ped Components
				IF IS_TIME_BETWEEN_THESE_HOURS(10, 22)
					iStaffDraw[0]		= 2
					iStaffTexture[0]	= 2
					iStaffDraw[1]		= 2
					iStaffTexture[1]	= 0
					iStaffDraw[2]		= 0
					iStaffTexture[2]	= 1
					iStaffDraw[3]		= 0
					iStaffTexture[3]	= 1
					iStaffDraw[4]		= 1
					iStaffTexture[4] 	= 0
				ELSE
					iStaffDraw[0]		= 1
					iStaffTexture[0]	= 0
					iStaffDraw[1]		= 0
					iStaffTexture[1]	= 0
					iStaffDraw[2]		= 1
					iStaffTexture[2]	= 1
					iStaffDraw[3]		= 0
					iStaffTexture[3]	= 0
					iStaffDraw[4]		= 1
					iStaffTexture[4] 	= 0
				ENDIF
				
				//vCashBagPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(scenePosition, sceneRotation.z, vCashBagOffset)
				//ArtTill = v_ret_gc_cashreg
			BREAK
			
			CASE eCRIM_HUP_LIQUOR_5
				sceneRotation = <<0, 0, -41>>	//180+140.9939 >>
				scenePosition = <<-1486.67, -378.46, 40.35-0.2>>	//-1486.650, -378.450, 40.160>>
				StaffModel = GET_HOLD_UP_MODEL(HUP_MODEL_STAFF)
				//vRegStaff = <<-1486.5422, -377.0771, 39.1634>>
				//fRegStaffHeading = 140.9939
				vBackRoomCower = <<-1479.1631, -375.0302, 38.1633>>		//-1479.3383, -375.4899, 38.1634>>
				fBackRoomCowerHeading = 36.5415							//355.5296
				//iMinCash = 600
				//iMaxCash = 1801
				iRegStaffAttackChance = ATTACK_CHANCE_LIQUOR
				
				//Ped Components
				IF IS_TIME_BETWEEN_THESE_HOURS(10, 22)
					iStaffDraw[0]		= 0
					iStaffTexture[0]	= 0
					iStaffDraw[1]		= 2
					iStaffTexture[1]	= 1
					iStaffDraw[2]		= 0
					iStaffTexture[2]	= 2
					iStaffDraw[3]		= 0
					iStaffTexture[3]	= 1
					iStaffDraw[4]		= 1
					iStaffTexture[4] 	= 0
				ELSE
					iStaffDraw[0]		= 2
					iStaffTexture[0]	= 0
					iStaffDraw[1]		= 1
					iStaffTexture[1]	= 0
					iStaffDraw[2]		= 1
					iStaffTexture[2]	= 2
					iStaffDraw[3]		= 0
					iStaffTexture[3]	= 0
					iStaffDraw[4]		= 1
					iStaffTexture[4] 	= 0
				ENDIF
				
				//vCashBagPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(scenePosition, sceneRotation.z, vCashBagOffset)
				//ArtTill = v_ret_gc_cashreg
			BREAK
			
			//GAS STATIONS
			CASE eCRIM_HUP_GAS_1
				sceneRotation = <<0, 0, 145>>	//180+324.4477 >>
				scenePosition = <<1698.33-0.04, 4923.33, 42.12-0.06>>	//1696.750, 4924.420, 42.070>>
				StaffModel = GET_HOLD_UP_MODEL(HUP_MODEL_STAFF)
				//vRegStaff = <<1697.0768, 4923.1348, 41.0636>>
				//fRegStaffHeading = 324.4477
				vBackRoomCower = <<1707.3031, 4918.3101, 41.0636>>
				fBackRoomCowerHeading = 24.9178
				//iMinCash = 600
				//iMaxCash = 1801
				iRegStaffAttackChance = ATTACK_CHANCE_GAS_STATION
				
				//Ped Components
				IF IS_TIME_BETWEEN_THESE_HOURS(8, 20)
					iStaffDraw[0]		= 0
					iStaffTexture[0]	= 1
					iStaffDraw[1]		= 0
					iStaffTexture[1]	= 0
					iStaffDraw[2]		= 1
					iStaffTexture[2]	= 0
					iStaffDraw[3]		= 0
					iStaffTexture[3]	= 0
					iStaffDraw[4]		= 1
					iStaffTexture[4] 	= 0
				ELSE
					iStaffDraw[0]		= 2
					iStaffTexture[0]	= 1
					iStaffDraw[1]		= 1
					iStaffTexture[1]	= 1
					iStaffDraw[2]		= 0
					iStaffTexture[2]	= 0
					iStaffDraw[3]		= 0
					iStaffTexture[3]	= 1
					iStaffDraw[4]		= 1
					iStaffTexture[4] 	= 0
				ENDIF
				
				//vCashBagPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(scenePosition, sceneRotation.z, vCashBagOffset+<<0, 0.02, 0>>)
				//ArtTill = PROP_TILL_02	//NEXT TO PROP_TILL_03
			BREAK
			
			CASE eCRIM_HUP_GAS_2
				sceneRotation = <<0, 0, -87>>	//180+91.8565 >>
				scenePosition = <<-706.69, -913.69, 19.27-0.06>>	//-706.660, -913.740, 19.230>>
				StaffModel = GET_HOLD_UP_MODEL(HUP_MODEL_STAFF)
				//vRegStaff = <<-705.8762, -914.4979, 18.2156>>
				//fRegStaffHeading = 91.8565
				vBackRoomCower = <<-709.7998, -907.1352, 18.2156>>
				fBackRoomCowerHeading = 291.6504
				//iMinCash = 600
				//iMaxCash = 1801
				iRegStaffAttackChance = ATTACK_CHANCE_GAS_STATION
				
				//Ped Components
				IF IS_TIME_BETWEEN_THESE_HOURS(8, 20)
					iStaffDraw[0]		= 0
					iStaffTexture[0]	= 2
					iStaffDraw[1]		= 1
					iStaffTexture[1]	= 0
					iStaffDraw[2]		= 1
					iStaffTexture[2]	= 1
					iStaffDraw[3]		= 0
					iStaffTexture[3]	= 1
					iStaffDraw[4]		= 1
					iStaffTexture[4] 	= 0
				ELSE
					iStaffDraw[0]		= 2
					iStaffTexture[0]	= 2
					iStaffDraw[1]		= 2
					iStaffTexture[1]	= 0
					iStaffDraw[2]		= 0
					iStaffTexture[2]	= 1
					iStaffDraw[3]		= 0
					iStaffTexture[3]	= 0
					iStaffDraw[4]		= 1
					iStaffTexture[4] 	= 0
				ENDIF
				
				//vCashBagPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(scenePosition, sceneRotation.z, vCashBagOffset+<<0, 0.02, 0>>)
				//ArtTill = PROP_TILL_02
			BREAK
			
			CASE eCRIM_HUP_GAS_3
				sceneRotation = <<0, 0, -125>>	//180+49.4793 >>
				scenePosition = <<-47.23, -1757.64, 29.48-0.06>>	//-47.300, -1757.750, 29.440>>
				StaffModel = GET_HOLD_UP_MODEL(HUP_MODEL_STAFF)
				//vRegStaff = <<-47.1968, -1758.7783, 28.4210>>
				//fRegStaffHeading = 49.4793
				vBackRoomCower = <<-40.4200, -1751.4232, 28.4210>>
				fBackRoomCowerHeading = 145.6553
				//iMinCash = 600
				//iMaxCash = 1801
				iRegStaffAttackChance = ATTACK_CHANCE_GAS_STATION
				
				//Ped Components
				IF IS_TIME_BETWEEN_THESE_HOURS(8, 20)
					iStaffDraw[0]		= 1
					iStaffTexture[0]	= 0
					iStaffDraw[1]		= 1
					iStaffTexture[1]	= 1
					iStaffDraw[2]		= 1
					iStaffTexture[2]	= 2
					iStaffDraw[3]		= 0
					iStaffTexture[3]	= 0
					iStaffDraw[4]		= 1
					iStaffTexture[4] 	= 0
				ELSE
					iStaffDraw[0]		= 0
					iStaffTexture[0]	= 0
					iStaffDraw[1]		= 2
					iStaffTexture[1]	= 0
					iStaffDraw[2]		= 0
					iStaffTexture[2]	= 2
					iStaffDraw[3]		= 0
					iStaffTexture[3]	= 1
					iStaffDraw[4]		= 1
					iStaffTexture[4] 	= 0
				ENDIF
				
				//vCashBagPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(scenePosition, sceneRotation.z, vCashBagOffset+<<0, 0.02, 0>>)
				//ArtTill = PROP_TILL_02
			BREAK
			
			CASE eCRIM_HUP_GAS_4
				sceneRotation = <<0, 0, -74>>	//180+98.5888 >>
				scenePosition = <<1164.16, -322.90, 69.26-0.06>>	//1164.200, -323.000, 69.220>>
				StaffModel = GET_HOLD_UP_MODEL(HUP_MODEL_STAFF)
				//vRegStaff = <<1165.0978, -323.5840, 68.2051>>
				//fRegStaffHeading = 98.5888
				vBackRoomCower = <<1159.6815, -314.2540, 68.2051>>
				fBackRoomCowerHeading = 232.6337
				//iMinCash = 600
				//iMaxCash = 1801
				iRegStaffAttackChance = ATTACK_CHANCE_GAS_STATION
				
				//Ped Components
				IF IS_TIME_BETWEEN_THESE_HOURS(8, 20)
					iStaffDraw[0]		= 2
					iStaffTexture[0]	= 0
					iStaffDraw[1]		= 2
					iStaffTexture[1]	= 1
					iStaffDraw[2]		= 0
					iStaffTexture[2]	= 0
					iStaffDraw[3]		= 0
					iStaffTexture[3]	= 1
					iStaffDraw[4]		= 1
					iStaffTexture[4] 	= 0
				ELSE
					iStaffDraw[0]		= 0
					iStaffTexture[0]	= 1
					iStaffDraw[1]		= 2
					iStaffTexture[1]	= 0
					iStaffDraw[2]		= 0
					iStaffTexture[2]	= 0
					iStaffDraw[3]		= 0
					iStaffTexture[3]	= 0
					iStaffDraw[4]		= 1
					iStaffTexture[4] 	= 0
				ENDIF
				
				//vCashBagPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(scenePosition, sceneRotation.z, vCashBagOffset+<<0, 0.02, 0>>)
				//ArtTill = PROP_TILL_02
			BREAK
			
			CASE eCRIM_HUP_GAS_5
				scenePosition = <<-1820.50, 793.78, 138.32-0.22>>	//-1820.400, 793.715, 138.130>>
				sceneRotation = <<0, 0, -47.3>>
				StaffModel = GET_HOLD_UP_MODEL(HUP_MODEL_STAFF)
				//vRegStaff = <<1165.0978, -323.5840, 68.2051>>
				//fRegStaffHeading = 98.5888
				vBackRoomCower = <<-1828.9071, 799.6096, 137.1776>>
				fBackRoomCowerHeading = 247.1234
				//iMinCash = 600
				//iMaxCash = 1801
				iRegStaffAttackChance = ATTACK_CHANCE_GAS_STATION
				
				//Ped Components
				IF IS_TIME_BETWEEN_THESE_HOURS(8, 20)
					iStaffDraw[0]		= 2
					iStaffTexture[0]	= 1
					iStaffDraw[1]		= 0
					iStaffTexture[1]	= 0
					iStaffDraw[2]		= 0
					iStaffTexture[2]	= 1
					iStaffDraw[3]		= 0
					iStaffTexture[3]	= 0
					iStaffDraw[4]		= 1
					iStaffTexture[4] 	= 0
				ELSE
					iStaffDraw[0]		= 0
					iStaffTexture[0]	= 2
					iStaffDraw[1]		= 2
					iStaffTexture[1]	= 1
					iStaffDraw[2]		= 0
					iStaffTexture[2]	= 1
					iStaffDraw[3]		= 0
					iStaffTexture[3]	= 1
					iStaffDraw[4]		= 1
					iStaffTexture[4] 	= 0
				ENDIF
				
				//vCashBagPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(scenePosition, sceneRotation.z, vCashBagOffset+<<0, 0.02, 0>>)
				//ArtTill = PROP_TILL_02
			BREAK
		ENDSWITCH
				
		VECTOR vTemp
		
		scenePosition = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(scenePosition, sceneRotation.z, <<0, 0.075, 0>>)
		
		//Get the Staff Position based on the Scene
		//vRegStaff = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(scenePosition, sceneRotation.z, vSyncSceneOffset)
		vRegStaff = GET_ANIM_INITIAL_OFFSET_POSITION("mp_am_hold_up", "holdup_victim_20s", scenePosition, sceneRotation)
		vTemp = GET_ANIM_INITIAL_OFFSET_ROTATION("mp_am_hold_up", "holdup_victim_20s", scenePosition, sceneRotation) 
		serverBD.fRegStaffHeading = vTemp.z		//(180 + sceneRotation.z)
		
		
		//vCashBagPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(scenePosition, sceneRotation.z, vCashBagOffset)
		vCashBagPos = GET_ANIM_INITIAL_OFFSET_POSITION("mp_am_hold_up", "holdup_victim_20s_bag", scenePosition, sceneRotation)
		vTemp = GET_ANIM_INITIAL_OFFSET_ROTATION("mp_am_hold_up", "holdup_victim_20s_bag", scenePosition, sceneRotation)
		fCashBagHeading =  vTemp.z		//sceneRotation.z
		
		//vTillPos = scenePosition
		vTillPos = GET_ANIM_INITIAL_OFFSET_POSITION("mp_am_hold_up", "holdup_victim_20s_till", scenePosition, sceneRotation)
		vTemp = GET_ANIM_INITIAL_OFFSET_ROTATION("mp_am_hold_up", "holdup_victim_20s_till", scenePosition, sceneRotation)
		fTillHeading =  vTemp.z			//(180 + sceneRotation.z)
		
		//HOLD_UP_BACKROOM_DETAILS(shopSnacks.NearestHoldUpPoint, vBackRoomMin, vBackRoomMax, fBackRoomWidth)
		
		shopSnacks.eCurrentShop = GET_SHOP_INDEX_FROM_HOLD_UP_POINT(ENUM_TO_INT(shopSnacks.NearestHoldUpPoint))
		NET_PRINT_TIME() NET_PRINT("     snacks - enum is : ") NET_PRINT_INT(ENUM_TO_INT(shopSnacks.eCurrentShop)) NET_NL()
				
		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			serverBD.iStaffVariation = iStaffDraw[0]
			/*serverBD.tlStaffVoice = "StoreOwner"
			IF serverBD.iStaffVariation > 0
				serverBD.tlStaffVoice += (serverBD.iStaffVariation+1)
			ENDIF*/
			
			IF serverBD.iStaffVariation = 0
				serverBD.tlStaffAmbientVoice = "MP_M_SHOPKEEP_01_PAKISTANI_MINI_01"
				serverBD.tlStaffVoice = "StoreOwner2"
			ELIF serverBD.iStaffVariation = 1
				serverBD.tlStaffAmbientVoice = "MP_M_SHOPKEEP_01_LATINO_MINI_01"
				serverBD.tlStaffVoice = "StoreOwner"
			ELSE
				serverBD.tlStaffAmbientVoice = "MP_M_SHOPKEEP_01_CHINESE_MINI_01"
				serverBD.tlStaffVoice = "StoreOwner3"
			ENDIF
			
		ENDIF
		NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - serverBD.iStaffVariation = ") NET_PRINT_INT(serverBD.iStaffVariation) NET_NL()
		NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - serverBD.tlStaffVoice = ") NET_PRINT(serverBD.tlStaffVoice) NET_NL()
		NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - serverBD.tlStaffAmbientVoice = ") NET_PRINT(serverBD.tlStaffAmbientVoice) NET_NL()
		
		/*AND NOT IS_BIT_SET(serverBD.iServerBitSet, biS_SwapTillBroadcast)
			enumMP_SWAPPING_MAP_OBJECTS eObject = INT_TO_ENUM(enumMP_SWAPPING_MAP_OBJECTS, (ENUM_TO_INT(MP_swapObjects_HOLD_UP_TILL_1)+ENUM_TO_INT(shopSnacks.NearestHoldUpPoint)))
			BROADCAST_SWAP_MAP_OBJECT(eObject, TRUE, TRUE)
			NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - BROADCAST_SWAP_MAP_OBJECT shopSnacks.NearestHoldUpPoint = ") NET_PRINT_INT(ENUM_TO_INT(shopSnacks.NearestHoldUpPoint)) NET_NL()
			NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - BROADCAST_SWAP_MAP_OBJECT SWAP ENUM = ") NET_PRINT_INT(ENUM_TO_INT(eObject)) NET_NL()
			SET_BIT(serverBD.iServerBitSet, biS_SwapTillBroadcast)
		ENDIF*/
		
		IF NOT IS_BIT_SET(shopSnacks.iBoolsBitSet, biHideTill)
			CREATE_MODEL_HIDE(scenePosition, 0.5, ArtTill, TRUE)
			NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - CREATE_MODEL_HIDE - shopSnacks.NearestHoldUpPoint = ") NET_PRINT_INT(ENUM_TO_INT(shopSnacks.NearestHoldUpPoint)) NET_NL()
			SET_BIT(shopSnacks.iBoolsBitSet, biHideTill)
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Create the sequences used in this script
PROC CREATE_SEQUENCES()
	//Staff Attacks Player
	/*OPEN_SEQUENCE_TASK(StaffAttackSeq)
		//TASK_AIM_GUN_AT_COORD(NULL, vStoreBlipLocation, GET_RANDOM_INT_IN_RANGE(250, 4500))
		//TASK_AIM_GUN_AT_COORD(NULL, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(NET_TO_PED(serverBD.sMenuData.niRegStaff), <<0, 5, 2>>), GET_RANDOM_INT_IN_RANGE(250, 4500))
		//TASK_AIM_GUN_AT_ENTITY(NULL, PLAYER_PED_ID(), GET_RANDOM_INT_IN_RANGE(250, 4500))
		TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 15)
	CLOSE_SEQUENCE_TASK(StaffAttackSeq)*/
	
	//TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, scenePosition, 1.0, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_ADV_SLIDE_TO_COORD_AND_ACHIEVE_HEADING_AT_END, sceneRotation.z)
	//TASK_PLAY_ANIM(NULL, "mp_am_hold_up", "guard_handsup_loop", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
		
	//Staff Empties the Cash Register
	/*OPEN_SEQUENCE_TASK(StaffEmptyRegisterSeq)
		TASK_STAND_STILL(NULL, GET_RANDOM_INT_IN_RANGE(250, 1001))
		TASK_PLAY_ANIM(NULL, "mp_amb_crim", "holdup_rifle_victim", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
		TASK_PLAY_ANIM(NULL, "mp_am_hold_up", "guard_handsup_loop", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
	CLOSE_SEQUENCE_TASK(StaffEmptyRegisterSeq)*/
	
	//Staff Hands Up and Look at Player
	OPEN_SEQUENCE_TASK(StaffHandsUpSeq)
		TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_DEFAULT)
		TASK_PLAY_ANIM(NULL, "mp_am_hold_up", "handsup_enter", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS | AF_UPPERBODY | AF_SECONDARY)
		TASK_PLAY_ANIM(NULL, "mp_am_hold_up", "handsup_base", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_UPPERBODY | AF_SECONDARY)
	CLOSE_SEQUENCE_TASK(StaffHandsUpSeq)
		
	OPEN_SEQUENCE_TASK(StaffHandsUpLoopSeq)
		//TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_DEFAULT)
		TASK_PLAY_ANIM(NULL, "mp_am_hold_up", "handsup_base", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_UPPERBODY | AF_SECONDARY)
	CLOSE_SEQUENCE_TASK(StaffHandsUpLoopSeq)
	
	OPEN_SEQUENCE_TASK(StaffFleeToBackRoomSeq)
		TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vBackRoomCower, PEDMOVEBLENDRATIO_RUN, -1, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, fBackRoomCowerHeading)
		TASK_PLAY_ANIM(NULL, "mp_am_hold_up", "cower_intro", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
		TASK_PLAY_ANIM(NULL, "mp_am_hold_up", "cower_loop", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING | AF_EXIT_AFTER_INTERRUPTED | AF_USE_KINEMATIC_PHYSICS)
		//TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
		TASK_SMART_FLEE_COORD(NULL, vBackRoomCower, 250, -1, TRUE)
		TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
	CLOSE_SEQUENCE_TASK(StaffFleeToBackRoomSeq)
	
	OPEN_SEQUENCE_TASK(StaffCowerInPlace)
		TASK_PLAY_ANIM(NULL, "mp_am_hold_up", "cower_intro", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
		TASK_PLAY_ANIM(NULL, "mp_am_hold_up", "cower_loop", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING | AF_EXIT_AFTER_INTERRUPTED | AF_USE_KINEMATIC_PHYSICS)
		TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
	CLOSE_SEQUENCE_TASK(StaffCowerInPlace)
	
	OPEN_SEQUENCE_TASK(StaffFleeToBackRoomAttackSeq)
		TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vBackRoomCower, PEDMOVEBLENDRATIO_RUN, -1, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, fBackRoomCowerHeading-180)
		//TASK_DUCK(NULL, 2000)
		TASK_SWAP_WEAPON(NULL, TRUE)
		TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 20)
	CLOSE_SEQUENCE_TASK(StaffFleeToBackRoomAttackSeq)
	
	NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - SEQUENCES CREATED    <----------     ") NET_NL()
ENDPROC

//PURPOSE: Destroys all the sequences created for this script
PROC CLEAR_SEQUENCES()
	//CLEAR_SEQUENCE_TASK(StaffAttackSeq)
	CLEAR_SEQUENCE_TASK(StaffEmptyRegisterSeq)
	CLEAR_SEQUENCE_TASK(StaffHandsUpSeq)
ENDPROC


//PURPOSE: Returns TRUE if current time is when the staff would have started a New Shift
FUNC BOOL IS_NEW_SHIFT_TIME()
	IF shopSnacks.NearestHoldUpPoint >= eCRIM_HUP_SHOP247_1
	AND shopSnacks.NearestHoldUpPoint <= eCRIM_HUP_SHOP247_10
		IF IS_TIME_BETWEEN_THESE_HOURS(6, 7)
		OR IS_TIME_BETWEEN_THESE_HOURS(18, 19)
			RETURN TRUE
		ENDIF
		RETURN FALSE
	ENDIF
	
	IF shopSnacks.NearestHoldUpPoint >= eCRIM_HUP_LIQUOR_1
	AND shopSnacks.NearestHoldUpPoint <= eCRIM_HUP_LIQUOR_5
		IF IS_TIME_BETWEEN_THESE_HOURS(10, 11)
		OR IS_TIME_BETWEEN_THESE_HOURS(22, 23)
			RETURN TRUE
		ENDIF
		RETURN FALSE
	ENDIF
	
	IF shopSnacks.NearestHoldUpPoint >= eCRIM_HUP_GAS_1
	AND shopSnacks.NearestHoldUpPoint <= eCRIM_HUP_GAS_5
		IF IS_TIME_BETWEEN_THESE_HOURS(8, 9)
		OR IS_TIME_BETWEEN_THESE_HOURS(20, 21)
			RETURN TRUE
		ENDIF
		RETURN FALSE
	ENDIF
	
	NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - IS_NEW_SHIFT_TIME - NEAREST POINT NOT VALID     <----------     ") NET_NL()
	RETURN FALSE
ENDFUNC

//PURPOSE: Returns TRUE if current time is when the staff has almost ended their shift
FUNC BOOL IS_END_SHIFT_TIME()
	IF shopSnacks.NearestHoldUpPoint >= eCRIM_HUP_SHOP247_1
	AND shopSnacks.NearestHoldUpPoint <= eCRIM_HUP_SHOP247_10
		IF IS_TIME_BETWEEN_THESE_HOURS(5, 6)
		OR IS_TIME_BETWEEN_THESE_HOURS(17, 18)
			RETURN TRUE
		ENDIF
		RETURN FALSE
	ENDIF
	
	IF shopSnacks.NearestHoldUpPoint >= eCRIM_HUP_LIQUOR_1
	AND shopSnacks.NearestHoldUpPoint <= eCRIM_HUP_LIQUOR_5
		IF IS_TIME_BETWEEN_THESE_HOURS(9, 10)
		OR IS_TIME_BETWEEN_THESE_HOURS(21, 22)
			RETURN TRUE
		ENDIF
		RETURN FALSE
	ENDIF
	
	IF shopSnacks.NearestHoldUpPoint >= eCRIM_HUP_GAS_1
	AND shopSnacks.NearestHoldUpPoint <= eCRIM_HUP_GAS_5
		IF IS_TIME_BETWEEN_THESE_HOURS(7, 8)
		OR IS_TIME_BETWEEN_THESE_HOURS(19, 20)
			RETURN TRUE
		ENDIF
		RETURN FALSE
	ENDIF
	
	NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - IS_END_SHIFT_TIME - NEAREST POINT NOT VALID     <----------     ") NET_NL()
	RETURN FALSE
ENDFUNC

// Do necessary pre game start ini.
// Returns FALSE if the script fails to receive an initial network broadcast.
FUNC BOOL PROCESS_PRE_GAME( MP_MISSION_DATA missionScriptArgs )
	
	// This marks the script as a net script, and handles any instancing setup. 
	SETUP_MP_MISSION_FOR_NETWORK(GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(missionScriptArgs.mdID.idMission) ,  missionScriptArgs)

	//Reserve Peds
	RESERVE_NETWORK_MISSION_PEDS(2)
	RESERVE_NETWORK_MISSION_OBJECTS(8)

	// This makes sure the net script is active, waits until it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
			
	// This script will not be paused if another script calls PAUSE_GAME
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	
	// KGM: Wait for the first network broadcast before moving on - call this as the last instruction in pre-game
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		RETURN FALSE
	ENDIF
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		
		REQUEST_LOAD_MODEL(GET_HOLD_UP_MODEL(HUP_MODEL_STAFF))
		REQUEST_LOAD_MODEL(GET_HOLD_UP_MODEL(HUP_MODEL_CASH_BAG))
		REQUEST_LOAD_MODEL(GET_HOLD_UP_MODEL(HUP_MODEL_TILL))
		REQUEST_ANIM_DICT("mp_am_hold_up")
		REQUEST_ANIM_DICT("mp_missheist_countrybank@cower")
		
		serverBD.iReservedObjects = 8
		
		IF IS_NEW_SHIFT_TIME()
			SET_BIT(serverBD.iServerBitSet2, biS2_IsNewShift)
		ELIF IS_END_SHIFT_TIME()
			SET_BIT(serverBD.iServerBitSet2, biS2_IsEndShift)
		ENDIF
		
		serverBD.iReaction = GET_RANDOM_INT_IN_RANGE(1, 11)
		serverBD.iStaffDialogueSet = GET_RANDOM_INT_IN_RANGE(1, 5)

		IF GET_RANDOM_BOOL()
		AND NOT IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_GANGHIDEOUT)
		AND NOT IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_RACETOPOINT)
			serverBD.iReservedPeds = 2
			SET_BIT(serverBD.iServerHeroBitSet, biSH_TryToGrabHero)
			NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - PROCESS_PRE_GAME - biSH_TryToGrabHero SET") NET_NL()
		ELSE
			serverBD.iReservedPeds = 1
			NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - PROCESS_PRE_GAME - biSH_TryToGrabHero NOT SET") NET_NL()
		ENDIF
		
		serverBD.fCopDelay = GET_RANDOM_FLOAT_IN_RANGE(18, 28)
		
		//SET_BIT(serverBD.iServerHeroBitSet, biSH_TryToGrabHero)	//TEMP TEST
	ENDIF
		
	#IF IS_DEBUG_BUILD
	g_bFM_IgnoreRankOnNextLaunchedActivity = FALSE
	#ENDIF
	
	IF GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) != FMMC_TYPE_GB_CHAL_COLLECT_MONEY
	SET_MAX_NUM_PORTABLE_PICKUPS_CARRIED_BY_PLAYER(GET_HOLD_UP_MODEL(HUP_MODEL_CASH_BAG), 0)
	ENDIF
	//SET_MISSION_SPAWN_OCCLUSION_SPHERE(vStoreBlipLocation, 12)
	
	//Check if we have already done manual rob help
	INT iStat = GET_MP_INT_CHARACTER_STAT(MP_STAT_TUTORIAL_BITSET)
	IF IS_BIT_SET(iStat, TUTBS_HOLD_UP_HELP_MANUAL_ROB_1)
	AND IS_BIT_SET(iStat, TUTBS_HOLD_UP_HELP_MANUAL_ROB_2)
		SET_BIT(shopSnacks.iBoolsBitSet2, bi2ManualRobHelpDone)
	ENDIF
	
	playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_INI
	
	NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - PROCESS_PRE_GAME - DONE    <----------     ") NET_NL()
	
	RETURN TRUE
ENDFUNC

//PURPOSE: Retursn TRUE if any player is in the shop, back room, or at the entrance.
FUNC BOOL IS_ANY_PLAYER_AT_SHOP()
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_AnyCrewInShop)
	OR IS_BIT_SET(serverBD.iServerBitSet, biS_AnyCrewInBackRoom)
	OR IS_BIT_SET(serverBD.iServerBitSet, biS_AnyCrewAtEntrance)
		RETURN TRUE	
	ENDIF
	
	//NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - IS_ANY_PLAYER_AT_SHOP - FALSE    <----------     ") NET_NL()
	RETURN FALSE	
ENDFUNC

FUNC BOOL IS_ANY_ALIVE_PLAYER_AT_SHOP()
	INT iParticipant
	
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
			PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
			PED_INDEX PlayerPedId = GET_PLAYER_PED(PlayerId)
			
			IF IS_NET_PLAYER_OK(PlayerId)
				IF GET_INTERIOR_FROM_ENTITY(PlayerPedId) = interiorID
//				IF IS_ENTITY_IN_ANGLED_AREA(PlayerPedId, vStoreMin, vStoreMax, fStoreWidth)
//				OR IS_ENTITY_IN_ANGLED_AREA(PlayerPedId, vBackRoomMin, vBackRoomMax, fBackRoomWidth)
//				OR IN_EXTRA_MAIN_AREA(PlayerPedId, shopSnacks.NearestHoldUpPoint)
					//NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - IS_ANY_ALIVE_PLAYER_AT_SHOP - TRUE    <----------     ") NET_NL()
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	//NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - IS_ANY_ALIVE_PLAYER_AT_SHOP - FALSE    <----------     ") NET_NL()
	RETURN FALSE	
ENDFUNC

//Player leaves vehicle, dies, runs out of drugs, or the vehicle becomes undriveable
FUNC BOOL MISSION_END_CHECK()
	
	//All Crooks on the Hold Up are Dead
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_AllCrewDead)
		NET_PRINT_TIME() NET_PRINT_STRINGS("[AM HOLD UP] - MISSION END - ALL CROOKS DEAD     <----------     ", GET_MP_MISSION_NAME(shopSnacks.thisHoldupMission)) NET_NL()
		RETURN TRUE
	ENDIF
		
	//Leave Area
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_AllCrewLeftArea)
		NET_PRINT_TIME() NET_PRINT_STRINGS("[AM HOLD UP] - MISSION END - CREW NOT IN AREA     <----------     ", GET_MP_MISSION_NAME(shopSnacks.thisHoldupMission)) NET_NL()
		RETURN TRUE
	ENDIF
	
	//Leave Store and Hold Up Ended
	IF NOT IS_ANY_PLAYER_AT_SHOP()	//IS_BIT_SET(serverBD.iServerBitSet, biS_AnyCrewInShop)
		IF IS_BIT_SET(serverBD.iServerBitSet, biS_CashPickedUp)
		OR IS_BIT_SET(serverBD.iServerBitSet, biS_CashBagDroppedAndEmpty)
		OR (IS_BIT_SET(serverBD.iServerBitSet, biS_SyncSceneStarted) AND NOT IS_BIT_SET(serverBD.iServerBitSet, biS_CashBagAvailableForPickup))
			
			//IF IS_BIT_SET(serverBD.iServerBitSet, biS_StaffDeadAndBagEmpty)
				BROADCAST_SET_HOLD_UP_DONE(ENUM_TO_INT(shopSnacks.NearestHoldUpPoint), TRUE, GET_HOLD_UP_HOST()) //SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iHoldUpDoneBitSet, ENUM_TO_INT(shopSnacks.NearestHoldUpPoint))
				NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - iHoldUpDoneBitSet - SET - C - HOLD UP = ") NET_PRINT_INT(ENUM_TO_INT(shopSnacks.NearestHoldUpPoint)) NET_NL()
			//ENDIF
			
			NET_PRINT_TIME() NET_PRINT_STRINGS("[AM HOLD UP] - MISSION END - NO PLAYER IN STORE AND HOLD UP ENDED    <----------     ", GET_MP_MISSION_NAME(shopSnacks.thisHoldupMission)) NET_NL()
			RETURN TRUE
		ENDIF
		
		//Shopkeeper is dead and the bag is empty
		IF IS_BIT_SET(serverBD.iServerBitSet, biS_StaffDeadAndBagEmpty)
			BROADCAST_SET_HOLD_UP_DONE(ENUM_TO_INT(shopSnacks.NearestHoldUpPoint), TRUE, GET_HOLD_UP_HOST()) //SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iHoldUpDoneBitSet, ENUM_TO_INT(shopSnacks.NearestHoldUpPoint))
			NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - iHoldUpDoneBitSet - SET - F - HOLD UP = ") NET_PRINT_INT(ENUM_TO_INT(shopSnacks.NearestHoldUpPoint)) NET_NL()
			
			NET_PRINT_TIME() NET_PRINT_STRINGS("[AM HOLD UP] - MISSION END - biS_StaffDeadAndBagEmpty    <----------     ", GET_MP_MISSION_NAME(shopSnacks.thisHoldupMission)) NET_NL()
			RETURN TRUE
		ENDIF
	ENDIF
	
	//Shopkeeper is dead and the bag is empty
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_StaffDeadAndBagEmpty)
		IF NOT IS_BIT_SET(serverBD.iServerBitSet2, biS2_HoldUpDoneCheck)
			BROADCAST_SET_HOLD_UP_DONE(ENUM_TO_INT(shopSnacks.NearestHoldUpPoint), TRUE, GET_HOLD_UP_HOST()) //SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iHoldUpDoneBitSet, ENUM_TO_INT(shopSnacks.NearestHoldUpPoint))
			NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - iHoldUpDoneBitSet - SET - F - HOLD UP = ") NET_PRINT_INT(ENUM_TO_INT(shopSnacks.NearestHoldUpPoint)) NET_NL()
			SET_BIT(serverBD.iServerBitSet2, biS2_HoldUpDoneCheck)
		ENDIF
	ENDIF
		
	//Empty Register
	/*IF IS_BIT_SET(serverBD.iServerBitSet, biS_CashPickedUp)
	AND iDelayEndTimer < GET_THE_NETWORK_TIMER()
		NET_PRINT_TIME() NET_PRINT_STRINGS("[AM HOLD UP] - MISSION END - bRegisterEmptied     <----------     ", GET_MP_MISSION_NAME(shopSnacks.thisHoldupMission)) NET_NL()
		RETURN TRUE
	ENDIF
	
	//Kill Register Staff
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_StaffDeadAndBagEmpty)
		NET_PRINT_TIME() NET_PRINT_STRINGS("[AM HOLD UP] - MISSION END - RegStaff INJURED AND CashBag EMPTY     <----------     ", GET_MP_MISSION_NAME(shopSnacks.thisHoldupMission)) NET_NL()
		RETURN TRUE
	ENDIF
	
	//Cash Bag Dropped with No Money
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_CashBagDroppedAndEmpty)
		NET_PRINT_TIME() NET_PRINT_STRINGS("[AM HOLD UP] - MISSION END - CASH BAG DROPPED WITH NO MONEY     <----------     ", GET_MP_MISSION_NAME(shopSnacks.thisHoldupMission)) NET_NL()
		RETURN TRUE
	ENDIF*/
	
	RETURN FALSE
ENDFUNC

//Server only function that checks to see if the game mode should now end. For this test script, it if time has passed.
FUNC BOOL HAVE_MISSION_END_CONDITIONS_BEEN_MET()
	
	#IF IS_DEBUG_BUILD
		IF bHostEndMissionNow
			NET_PRINT_TIME() NET_PRINT_STRINGS("[AM HOLD UP] - MISSION END - DEBUG bHostEndMissionNow     <----------     ", GET_MP_MISSION_NAME(shopSnacks.thisHoldupMission)) NET_NL()
			RETURN TRUE
		ENDIF
	#ENDIF	
	
	IF MISSION_END_CHECK()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


//Helper function to get a clients game/mission state
FUNC INT GET_CLIENT_MISSION_STATE(INT iPlayer)
	RETURN playerBD[iPlayer].iGameState
ENDFUNC

//Helper function to get the servers game/mission state
FUNC INT GET_SERVER_MISSION_STATE()
	RETURN serverBD.iServerGameState
ENDFUNC

#IF IS_DEBUG_BUILD
INT iStaffStateForWidget
//FLOAT fRed
//FLOAT fAlpha
PROC CREATE_WIDGETS()
	INT iPlayer
	TEXT_LABEL_15 tl63

	START_WIDGET_GROUP(GET_MP_MISSION_NAME(shopSnacks.thisHoldupMission))  
		
		//ADD_WIDGET_FLOAT_SLIDER("fRed", fRed, -1, HIGHEST_INT, 0.01)
		//ADD_WIDGET_FLOAT_SLIDER("fAlpha", fAlpha, -1, HIGHEST_INT, 0.01)
		
		//ADD_WIDGET_INT_READ_ONLY("iIntimCurrent", iIntimCurrent)
		//ADD_WIDGET_INT_READ_ONLY("iIntimTarget", iIntimTarget)
		
		ADD_WIDGET_BOOL("Try to Grab Hero", bTryToGrabHeroDebug)
		
		ADD_WIDGET_FLOAT_READ_ONLY("fSyncSceneRate", fSyncSceneRate)
		ADD_WIDGET_FLOAT_READ_ONLY("Server fCurrentShout", serverBD.fCurrentShout)
		ADD_WIDGET_FLOAT_READ_ONLY("Player Shout", playerBD[PARTICIPANT_ID_TO_INT()].fShout)
		ADD_WIDGET_FLOAT_READ_ONLY("fCashBagFillAmount", serverBD.fCashBagFillAmount)
		ADD_WIDGET_INT_READ_ONLY("iNumPlayersInStore", serverBD.iNumPlayersInStore)
		ADD_WIDGET_INT_READ_ONLY("eStaffState", iStaffStateForWidget)
		
		ADD_WIDGET_INT_READ_ONLY("Server iTotalCashSpent", serverBD.sMenuData.iTotalCashSpent)
		ADD_WIDGET_INT_READ_ONLY("Player iCashSpent", playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iCashSpent)
		
		//ADD_WIDGET_INT_READ_ONLY("iRegStaffAttackChance", iRegStaffAttackChance)
		//ADD_WIDGET_VECTOR_SLIDER("scenePosition", scenePosition, -5000, 5000, 1.0)
		
		//ADD_BIT_FIELD_WIDGET("Server BitSet", serverBD.iServerBitSet)
		//ADD_BIT_FIELD_WIDGET("Player BitSet", playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet)
		//ADD_BIT_FIELD_WIDGET("Local BitSet", shopSnacks.iBoolsBitSet)
		
		START_WIDGET_GROUP("Entity Reservations")
			ADD_WIDGET_INT_READ_ONLY("Peds Reserved", serverBD.iReservedPeds)
			ADD_WIDGET_INT_READ_ONLY("Objects Reserved", serverBD.iReservedObjects)
		STOP_WIDGET_GROUP()	
		
		// Gameplay debug, sever only.
		START_WIDGET_GROUP("Server Only Gameplay") 
			ADD_WIDGET_BOOL("End mission", bHostEndMissionNow)			
		STOP_WIDGET_GROUP()		
		
		// Data about the server
		START_WIDGET_GROUP("Server BD") 
				ADD_WIDGET_INT_SLIDER("S. Game state", serverBD.iServerGameState,-1, HIGHEST_INT,1)
		STOP_WIDGET_GROUP()	

		// Data about the clients. * = You.
		START_WIDGET_GROUP("Client BD")  				
			REPEAT NUM_NETWORK_PLAYERS iPlayer
				tl63 = "Player "
				tl63 += iPlayer
				IF iPlayer = PARTICIPANT_ID_TO_INT()
					tl63 += "*"
				ENDIF
				START_WIDGET_GROUP(tl63)
					ADD_WIDGET_INT_SLIDER("Game state", playerBD[iPlayer].iGameState,-1, HIGHEST_INT,1)
				STOP_WIDGET_GROUP()
			ENDREPEAT				
		STOP_WIDGET_GROUP()	
		
	STOP_WIDGET_GROUP()
ENDPROC		

PROC UPDATE_WIDGETS()		
	iStaffStateForWidget = ENUM_TO_INT(serverBD.sMenuData.eStaffState)
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF NOT IS_BIT_SET(serverBD.iServerHeroBitSet, biSH_TryToGrabHero)
			IF bTryToGrabHeroDebug = TRUE
				SET_BIT(serverBD.iServerHeroBitSet, biSH_TryToGrabHero)
			ENDIF
		ELSE
			IF bTryToGrabHeroDebug = FALSE
				bTryToGrabHeroDebug = TRUE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

#ENDIF


//PURPOSE: Gives the player XP for dropping Cargo
PROC GIVE_PASS_XP()
	IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
	OR IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_PlayerHasDemandedCash)
		//STATS
		//If this is the first time the store has been held up then increase the Award Stat
		INT iHoldUpsAwardBit
		iHoldUpsAwardBit= GET_MP_INT_CHARACTER_STAT(MP_STAT_HOLDUPS_BITSET)
		IF NOT IS_BIT_SET(iHoldUpsAwardBit, ENUM_TO_INT(shopSnacks.NearestHoldUpPoint))
			INCREMENT_BY_MP_INT_CHARACTER_AWARD(MP_AWARD_HOLD_UP_SHOPS, 1)
			SET_BIT(iHoldUpsAwardBit, ENUM_TO_INT(shopSnacks.NearestHoldUpPoint))
			SET_MP_INT_CHARACTER_STAT(MP_STAT_HOLDUPS_BITSET, iHoldUpsAwardBit) 
			
			CPRINTLN(DEBUG_ACHIEVEMENT, "[ACHIEVEMENT]: ACH43 - Empty The Register - Holdups:", GET_MP_INT_CHARACTER_AWARD (MP_AWARD_HOLD_UP_SHOPS), " of ", ACH43_HOLDUPSNEEDED)
			IF (GET_MP_INT_CHARACTER_AWARD (MP_AWARD_HOLD_UP_SHOPS) >= ACH43_HOLDUPSNEEDED)
				AWARD_ACHIEVEMENT(ACH43) // Empty The Register
			ELSE
				SET_ACHIEVEMENT_PROGRESS_SAFE(ENUM_TO_INT(ACH43),GET_MP_INT_CHARACTER_AWARD (MP_AWARD_HOLD_UP_SHOPS))
			ENDIF
		ENDIF
		
		INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_TOTAL_NO_SHOPS_HELD_UP, 1)
		
		SET_PACKED_STAT_BOOL(PACKED_MP_STARTED_HOLDUPS, TRUE)
		NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - REWARD GIVEN - HOLD UP = ") NET_PRINT_INT(ENUM_TO_INT(shopSnacks.NearestHoldUpPoint)) NET_NL()
		
		SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_AM_HOLD_UP_STORE)
		
		//XP
		/*INT iTimesPlayed = GET_MP_INT_CHARACTER_STAT(MP_STAT_TOTAL_NO_SHOPS_HELD_UP)
		IF iTimesPlayed < 1
			iTimesPlayed=1
		ENDIF
		
		iEarnedXP = ROUND((50*(TO_FLOAT(iTimesPlayed)/2)))
		IF iEarnedXP < 50
			iEarnedXP = 50
		ENDIF*/
		
		/*IF HAS_NET_TIMER_STARTED(MpGlobals.iHoldUpStreakTimer)
			iEarnedXP = (iEarnedXP*2)
			NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - GIVE_PASS_XP - HOLD UP STREAK ACTIVE - DOUBLE XP") NET_NL()
		ENDIF*/
		
		iEarnedXP = 100
		iEarnedXP = ROUND(iEarnedXP*g_sMPTunables.fxp_tunable_Hold_Ups)
		
		GIVE_LOCAL_PLAYER_XP(eXPTYPE_CO_OP_AMBIENTFM, "XPT_HOLDUP", XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_HOLD_UP, iEarnedXP, 1)
		
		//Removed for: url:bugstar:6289781 - Remove REQUEST_SAVE(SSR_REASON_FM_HOLD_UP, STAT_SAVETYPE_END_MISSION)
		//REQUEST_SAVE(SSR_REASON_FM_HOLD_UP, STAT_SAVETYPE_END_MISSION)
		
		//NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - GIVE_PASS_XP - iTimesPlayed = ") NET_PRINT_INT(iTimesPlayed) NET_NL()
		NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - GIVE_PASS_XP - iEarnedXP = ") NET_PRINT_INT(iEarnedXP) NET_NL()
	ENDIF
ENDPROC

//PURPOSE: Gives the player XP/Cash  Reward
PROC GIVE_END_HOLD_UP_REWARD()
	IF NOT IS_BIT_SET(shopSnacks.iBoolsBitSet, biXPandCashGiven)
		
		IF IS_BIT_SET(serverBD.iServerBitSet, biS_CashPickedUp)
		OR IS_BIT_SET(serverBD.iServerBitSet2, biS2_RegisterRobberManually)
//			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vStoreMin, vStoreMax, fStoreWidth)
//			OR IN_EXTRA_MAIN_AREA(PLAYER_PED_ID(), shopSnacks.NearestHoldUpPoint)
//			OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vBackRoomMin, vBackRoomMax, fBackRoomWidth)
			IF GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = interiorID
			OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vDoorCoord, <<HOLD_UP_ENTRANCE_RANGE, HOLD_UP_ENTRANCE_RANGE, HOLD_UP_ENTRANCE_RANGE_HEIGHT>>)
			
				GIVE_PASS_XP()
			
				//iLocalCashShare = serverBD.iCashShare
								
				/*GIVE_LOCAL_PLAYER_CASH_END_OF_MISSION(iLocalCashShare)
				SET_XP_ANIMATION_MULTIPLIER(XP_TRIPLE_MULT) 
				IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_CashBagPickedUp)
					SET_CASH_REWARD_FOR_LOCATION(serverBd.vPickedUpCashBagPos, iLocalCashShare)
				ELSE	
					SET_CASH_REWARD_FOR_ENTITY(PLAYER_PED_ID(), iLocalCashShare)
				ENDIF
				//SET_CASH_REWARD_FOR_LOCATION(GET_ENTITY_COORDS(NET_TO_OBJ(serverBD.niTill)), iLocalCashShare)
				iEarnedCash += iLocalCashShare
				INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_MONEY_PICKED_FROM_HOLDUPS, iLocalCashShare)*/
				
				REQUEST_SYSTEM_ACTIVITY_TYPE_ROBBED_HOLD_UP_STORE()
				
				BROADCAST_SET_HOLD_UP_DONE(ENUM_TO_INT(shopSnacks.NearestHoldUpPoint), TRUE, GET_HOLD_UP_HOST()) 	//SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iHoldUpDoneBitSet, ENUM_TO_INT(shopSnacks.NearestHoldUpPoint))
				NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - iHoldUpDoneBitSet - SET - B - HOLD UP = ") NET_PRINT_INT(ENUM_TO_INT(shopSnacks.NearestHoldUpPoint)) NET_NL()
				#IF IS_DEBUG_BUILD NET_LOG("REWARD GIVEN")	#ENDIF
				SET_BIT(shopSnacks.iBoolsBitSet, biXPandCashGiven)
			
			ELSE
				
				IF IS_BIT_SET(shopSnacks.iBoolsBitSet, biWeaponHolstered)
				AND (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0 OR IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_PlayerHasDemandedCash))
				AND IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vDoorCoord, <<HOLD_UP_GIVE_XP_RANGE, HOLD_UP_GIVE_XP_RANGE, HOLD_UP_GIVE_XP_RANGE>>)
					GIVE_PASS_XP()
					NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - biXPandCashGiven - PLAYER NOT IN STORE BUT WITHIN 20m - JUST GETS XP - HOLD UP = ") NET_PRINT_INT(ENUM_TO_INT(shopSnacks.NearestHoldUpPoint)) NET_NL()
				ELSE
					NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - biXPandCashGiven - PLAYER NOT IN STORE TO GET REWARD - HOLD UP = ") NET_PRINT_INT(ENUM_TO_INT(shopSnacks.NearestHoldUpPoint)) NET_NL()
				ENDIF
				
				SET_BIT(shopSnacks.iBoolsBitSet, biXPandCashGiven)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Return True if the Player is in front of the Ped
FUNC BOOL IS_PLAYER_IN_FRONT_OF_PED(PLAYER_INDEX Player, PED_INDEX TargetPed)

	VECTOR posA		// Player position
	VECTOR posB 	// Ped position
	VECTOR posC
	VECTOR vecAB 	// vector from Player to Ped
	VECTOR vecPed	// unit vector pointing in the direction the Ped is facing
	FLOAT fTemp
	
	// get positions
	IF IS_NET_PLAYER_OK(Player)
		posA = GET_PLAYER_COORDS(Player)
	ENDIF
	IF NOT IS_PED_INJURED(TargetPed)
		posB = GET_ENTITY_COORDS(TargetPed)
	ENDIF

	// get vec from Player to Ped
	vecAB = posB - posA
	
	// get unit vector pointing forwards from Ped
	IF NOT IS_PED_INJURED(TargetPed)
		posC = get_offset_from_entity_in_world_coords(TargetPed, <<0.0, 7.0, 0.0>>)
		vecPed = posC - posB
	ENDIF

	// take the z out
	vecAB.z = 0.0
	vecPed.z = 0.0

	// calculate dot product of vecAB and vecPed
	fTemp = DOT_PRODUCT(vecAB, vecPed)
	IF (fTemp < -0.1)
		RETURN(TRUE)
	ENDIF
	
	RETURN(FALSE)
ENDFUNC

//PURPOSE: Returns an increase in the chance of the Shopkeeper attacking based on how many times the player has robbed stores
FUNC INT GET_INCREASED_STAFF_ATTACK_CHANCE()
	INT iInCloseSuccession = (GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iHoldUpsDoneInCloseSuccession - 1) //Don't want to use the incremented value from the current Hold Up
	
	IF iInCloseSuccession >= 4
		RETURN 4
	ENDIF
	
	RETURN iInCloseSuccession
ENDFUNC

//PURPOSE: Checks to see if the Staff ped should Attack
PROC DO_STAFF_ATTACK_CHECK()
	IF NOT IS_BIT_SET(serverBD.iServerBitSet2, biS2_DoStaffAttackCheck)
		INT iRand
		iRand = GET_RANDOM_INT_IN_RANGE(0, 10)
		//iRand = 0	//TEMP TEST
		
		INT iAttackThreshold = (iRegStaffAttackChance + GET_INCREASED_STAFF_ATTACK_CHANCE())
		
		IF iRand < iAttackThreshold 
			SET_BIT(serverBD.iServerBitSet2, biS2_MakeStaffAttack)
			NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - DO_STAFF_ATTACK_CHECK - biS2_MakeStaffAttack SET    <----------     ") NET_NL()
		ENDIF
		NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - DO_STAFF_ATTACK_CHECK - biS2_DoStaffAttackCheck SET    <----------     ") NET_NL()
		SET_BIT(serverBD.iServerBitSet2, biS2_DoStaffAttackCheck)
	ENDIF
ENDPROC

//PURPOSE: Gives the Staff ped a weapon and correct relationship
PROC SETUP_STAFF_TO_ATTACK()	
	//Give Staff a Weapon
	INT iRandWeapon
	iRandWeapon = GET_RANDOM_INT_IN_RANGE(0, 12)
	IF iRandWeapon < 3
		GIVE_DELAYED_WEAPON_TO_PED(NET_TO_PED(serverBD.sMenuData.niRegStaff), WEAPONTYPE_PISTOL, 25000, FALSE)
		SET_PED_ACCURACY(NET_TO_PED(serverBD.sMenuData.niRegStaff), GET_RANDOM_INT_IN_RANGE(10, 35))
	ELIF iRandWeapon < 6
		GIVE_DELAYED_WEAPON_TO_PED(NET_TO_PED(serverBD.sMenuData.niRegStaff), WEAPONTYPE_COMBATPISTOL, 25000, FALSE)
		SET_PED_ACCURACY(NET_TO_PED(serverBD.sMenuData.niRegStaff), GET_RANDOM_INT_IN_RANGE(15, 40))
	ELIF iRandWeapon = 6
		GIVE_DELAYED_WEAPON_TO_PED(NET_TO_PED(serverBD.sMenuData.niRegStaff), WEAPONTYPE_MICROSMG, 25000, FALSE)
		SET_PED_ACCURACY(NET_TO_PED(serverBD.sMenuData.niRegStaff), GET_RANDOM_INT_IN_RANGE(10, 25))
	ELIF iRandWeapon = 7
		GIVE_DELAYED_WEAPON_TO_PED(NET_TO_PED(serverBD.sMenuData.niRegStaff), WEAPONTYPE_COMBATMG, 25000, FALSE)
		SET_PED_ACCURACY(NET_TO_PED(serverBD.sMenuData.niRegStaff), GET_RANDOM_INT_IN_RANGE(5, 15))
	ELIF iRandWeapon = 8
		GIVE_DELAYED_WEAPON_TO_PED(NET_TO_PED(serverBD.sMenuData.niRegStaff), WEAPONTYPE_BAT, 25000, FALSE)
		SET_PED_ACCURACY(NET_TO_PED(serverBD.sMenuData.niRegStaff), GET_RANDOM_INT_IN_RANGE(20, 40))
	ELIF iRandWeapon = 9
		GIVE_DELAYED_WEAPON_TO_PED(NET_TO_PED(serverBD.sMenuData.niRegStaff), WEAPONTYPE_ASSAULTRIFLE, 25000, FALSE)
		SET_PED_ACCURACY(NET_TO_PED(serverBD.sMenuData.niRegStaff), GET_RANDOM_INT_IN_RANGE(5, 30))
	ELIF iRandWeapon = 10
		GIVE_DELAYED_WEAPON_TO_PED(NET_TO_PED(serverBD.sMenuData.niRegStaff), WEAPONTYPE_SAWNOFFSHOTGUN, 25000, FALSE)
		SET_PED_ACCURACY(NET_TO_PED(serverBD.sMenuData.niRegStaff), GET_RANDOM_INT_IN_RANGE(5, 20))
	ELSE
		GIVE_DELAYED_WEAPON_TO_PED(NET_TO_PED(serverBD.sMenuData.niRegStaff), WEAPONTYPE_PUMPSHOTGUN, 25000, FALSE) 
		SET_PED_ACCURACY(NET_TO_PED(serverBD.sMenuData.niRegStaff), GET_RANDOM_INT_IN_RANGE(10, 25))
	ENDIF
	NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - SETUP_STAFF_TO_ATTACK - GIVE STAFF WEAPON - BATTY     <----------     ") NET_NL()
	
	BROADCAST_SET_HOLD_UP_DONE(ENUM_TO_INT(shopSnacks.NearestHoldUpPoint), TRUE, GET_HOLD_UP_HOST())
	NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - iHoldUpDoneBitSet - SET - D - HOLD UP = ") NET_PRINT_INT(ENUM_TO_INT(shopSnacks.NearestHoldUpPoint)) NET_NL()
		
	SET_PED_RELATIONSHIP_GROUP_HASH(NET_TO_PED(serverBD.sMenuData.niRegStaff), rgFM_AiHatePlyrLikeCops)
	SET_PED_AS_ENEMY(NET_TO_PED(serverBD.sMenuData.niRegStaff), TRUE)
	
	//Make Defensive
	SET_PED_COMBAT_MOVEMENT(NET_TO_PED(serverBD.sMenuData.niRegStaff), CM_DEFENSIVE)
	SET_PED_SPHERE_DEFENSIVE_AREA(NET_TO_PED(serverBD.sMenuData.niRegStaff), vRegStaff, 20.0)
	
	//SET_ENTITY_HEALTH(NET_TO_PED(serverBD.sMenuData.niRegStaff), 200)
	
	//**STOP_SYNC_SCENE()
	
	SET_PED_KEEP_TASK(NET_TO_PED(serverBD.sMenuData.niRegStaff), TRUE)
ENDPROC

FUNC MODEL_NAMES GET_SNACK_MODEL(SNACK_CATEGORIES eWhichSnack)
	SWITCH eWhichSnack
		CASE SNACK_SMALL		RETURN PROP_CHOC_PQ
		CASE SNACK_MEDIUM		RETURN PROP_CHOC_EGO
		CASE SNACK_LARGE		RETURN PROP_CHOC_METO
		CASE SNACK_SMOKES		RETURN PROP_LD_FAGS_01
		CASE SNACK_SODA			RETURN PROP_ECOLA_CAN
		CASE SNACK_BEER			RETURN PROP_AMB_BEER_BOTTLE
		CASE SNACK_SPRUNK		RETURN PROP_LD_CAN_01B
		/*#IF IS_DEBUG_BUILD
		#IF FEATURE_HEIST_PLANNING
		CASE SNACK_LOTTERY		RETURN PROP_NOTEPAD_01
		#ENDIF
		#ENDIF*/
		DEFAULT 	RETURN PROP_CHOC_PQ
	ENDSWITCH
ENDFUNC

FUNC VECTOR GET_SNACK_INIT_POS(SNACK_CATEGORIES eWhichSnack)
	SWITCH eWhichSnack
		CASE SNACK_SMALL		RETURN vTillPos - << 0.1, 0.2, 1.0>>
		CASE SNACK_MEDIUM		RETURN vTillPos - << 0.0, 0.2, 1.0>>
		CASE SNACK_LARGE		RETURN vTillPos - << -0.1, 0.2, 1.0>>
		CASE SNACK_SMOKES		RETURN vTillPos - << 0.1, -0.2, 1.0>>
		CASE SNACK_SODA			RETURN vTillPos - << 0.0, -0.2, 1.0>>
		CASE SNACK_BEER			RETURN vTillPos - << -0.1, -0.2, 1.0>>
		CASE SNACK_SPRUNK		RETURN vTillPos - << 0.2, -0.2, 1.0>>
		/*#IF IS_DEBUG_BUILD
		#IF FEATURE_HEIST_PLANNING
		CASE SNACK_LOTTERY		RETURN vTillPos - << -0.2, -0.2, 1.0>>
		#ENDIF
		#ENDIF*/
		DEFAULT 	RETURN vTillPos - << -0.1, 0, 1.0>>
	ENDSWITCH
ENDFUNC

FUNC STRING GET_SNACK_ANIM(SNACK_CATEGORIES eWhichSnack)
	SWITCH eWhichSnack
		CASE SNACK_SMALL		RETURN "purchase_chocbar"
		CASE SNACK_MEDIUM		RETURN "purchase_chocbar"
		CASE SNACK_LARGE		RETURN "purchase_chocbar"
		CASE SNACK_SMOKES		RETURN "purchase_cigarette"
		CASE SNACK_SODA			RETURN "purchase_beer"
		CASE SNACK_BEER			RETURN "purchase_energydrink"
		CASE SNACK_SPRUNK		RETURN "purchase_beer"
		/*#IF IS_DEBUG_BUILD
		#IF FEATURE_HEIST_PLANNING
		CASE SNACK_LOTTERY		RETURN "purchase_chocbar"
		#ENDIF
		#ENDIF*/
		DEFAULT 	RETURN "purchase_energydrink"
	ENDSWITCH
ENDFUNC

FUNC STRING GET_SNACK_CLERK_ANIM(SNACK_CATEGORIES eWhichSnack)
	SWITCH eWhichSnack
		CASE SNACK_SMALL		RETURN "purchase_chocbar_shopkeeper"
		CASE SNACK_MEDIUM		RETURN "purchase_chocbar_shopkeeper"
		CASE SNACK_LARGE		RETURN "purchase_chocbar_shopkeeper"
		CASE SNACK_SMOKES		RETURN "purchase_cigarette_shopkeeper"
		CASE SNACK_SODA			RETURN "purchase_beer_shopkeeper"
		CASE SNACK_BEER			RETURN "purchase_energydrink_shopkeeper"
		CASE SNACK_SPRUNK		RETURN "purchase_beer_shopkeeper"
		/*#IF IS_DEBUG_BUILD
		#IF FEATURE_HEIST_PLANNING
		CASE SNACK_LOTTERY		RETURN "purchase_chocbar_shopkeeper"
		#ENDIF
		#ENDIF*/
		DEFAULT 	RETURN "purchase_energydrink_shopkeeper"
	ENDSWITCH
ENDFUNC

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  MISSION PROCS                  //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

//PURPOSE: Loads and Creates the Items that are sold
FUNC BOOL CREATE_SNACKS()
	
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niSnacks[SNACK_SMALL])
	OR NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niSnacks[SNACK_MEDIUM])
	OR NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niSnacks[SNACK_LARGE])
	OR NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niSnacks[SNACK_SMOKES])
	OR NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niSnacks[SNACK_SODA])
	OR NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niSnacks[SNACK_BEER])
	OR NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niSnacks[SNACK_SPRUNK])
	//OR NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niSnacks[SNACK_LOTTERY])
		
		IF REQUEST_LOAD_MODEL(GET_SNACK_MODEL(SNACK_SMALL))
		AND REQUEST_LOAD_MODEL(GET_SNACK_MODEL(SNACK_MEDIUM))
		AND REQUEST_LOAD_MODEL(GET_SNACK_MODEL(SNACK_LARGE))
		AND REQUEST_LOAD_MODEL(GET_SNACK_MODEL(SNACK_SMOKES))
		AND REQUEST_LOAD_MODEL(GET_SNACK_MODEL(SNACK_SODA))
		AND REQUEST_LOAD_MODEL(GET_SNACK_MODEL(SNACK_BEER))
		AND REQUEST_LOAD_MODEL(GET_SNACK_MODEL(SNACK_SPRUNK))
		//AND REQUEST_LOAD_MODEL(GET_SNACK_MODEL(SNACK_LOTTERY))
			IF CAN_REGISTER_MISSION_ENTITIES(0, 0, 7, 0)	
				IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niSnacks[SNACK_SMALL])
					IF CREATE_NET_OBJ(serverBD.niSnacks[SNACK_SMALL], GET_SNACK_MODEL(SNACK_SMALL), GET_SNACK_INIT_POS(SNACK_SMALL))
						RETAIN_ENTITY_IN_INTERIOR(NET_TO_OBJ(serverBD.niSnacks[SNACK_SMALL]), GET_INTERIOR_AT_COORDS(vRegStaff))
						
						FREEZE_ENTITY_POSITION(NET_TO_OBJ(serverBD.niSnacks[SNACK_SMALL]), TRUE)
						
						SET_ENTITY_VISIBLE(NET_TO_OBJ(serverBD.niSnacks[SNACK_SMALL]), FALSE)
						
						NET_PRINT_TIME() NET_PRINT("[AM HOLD UP]      ---------->     snack_anims - CREATE SNACK_SMALL    <----------     ") NET_NL()
					ENDIF
				ENDIF
				
				IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niSnacks[SNACK_MEDIUM])
					IF CREATE_NET_OBJ(serverBD.niSnacks[SNACK_MEDIUM], GET_SNACK_MODEL(SNACK_MEDIUM), GET_SNACK_INIT_POS(SNACK_MEDIUM))
						RETAIN_ENTITY_IN_INTERIOR(NET_TO_OBJ(serverBD.niSnacks[SNACK_MEDIUM]), GET_INTERIOR_AT_COORDS(vRegStaff))
						
						FREEZE_ENTITY_POSITION(NET_TO_OBJ(serverBD.niSnacks[SNACK_MEDIUM]), TRUE)
						
						SET_ENTITY_VISIBLE(NET_TO_OBJ(serverBD.niSnacks[SNACK_MEDIUM]), FALSE)
						
						NET_PRINT_TIME() NET_PRINT("[AM HOLD UP]      ---------->     snack_anims - CREATE SNACK_MEDIUM    <----------     ") NET_NL()
					ENDIF
				ENDIF
				
				IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niSnacks[SNACK_LARGE])
					IF CREATE_NET_OBJ(serverBD.niSnacks[SNACK_LARGE], GET_SNACK_MODEL(SNACK_LARGE), GET_SNACK_INIT_POS(SNACK_LARGE))
						RETAIN_ENTITY_IN_INTERIOR(NET_TO_OBJ(serverBD.niSnacks[SNACK_LARGE]), GET_INTERIOR_AT_COORDS(vRegStaff))
						
						FREEZE_ENTITY_POSITION(NET_TO_OBJ(serverBD.niSnacks[SNACK_LARGE]), TRUE)
						
						SET_ENTITY_VISIBLE(NET_TO_OBJ(serverBD.niSnacks[SNACK_LARGE]), FALSE)
						
						NET_PRINT_TIME() NET_PRINT("[AM HOLD UP]      ---------->     snack_anims - CREATE SNACK_LARGE    <----------     ") NET_NL()
					ENDIF
				ENDIF
				
				IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niSnacks[SNACK_SMOKES])
					IF CREATE_NET_OBJ(serverBD.niSnacks[SNACK_SMOKES], GET_SNACK_MODEL(SNACK_SMOKES), GET_SNACK_INIT_POS(SNACK_SMOKES))
						RETAIN_ENTITY_IN_INTERIOR(NET_TO_OBJ(serverBD.niSnacks[SNACK_SMOKES]), GET_INTERIOR_AT_COORDS(vRegStaff))
						
						FREEZE_ENTITY_POSITION(NET_TO_OBJ(serverBD.niSnacks[SNACK_SMOKES]), TRUE)
						
						SET_ENTITY_VISIBLE(NET_TO_OBJ(serverBD.niSnacks[SNACK_SMOKES]), FALSE)
						
						NET_PRINT_TIME() NET_PRINT("[AM HOLD UP]      ---------->     snack_anims - CREATE SNACK_SMOKES    <----------     ") NET_NL()
					ENDIF
				ENDIF
				
				IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niSnacks[SNACK_SODA])
					IF CREATE_NET_OBJ(serverBD.niSnacks[SNACK_SODA], GET_SNACK_MODEL(SNACK_SODA), GET_SNACK_INIT_POS(SNACK_SODA))
						RETAIN_ENTITY_IN_INTERIOR(NET_TO_OBJ(serverBD.niSnacks[SNACK_SODA]), GET_INTERIOR_AT_COORDS(vRegStaff))
						
						FREEZE_ENTITY_POSITION(NET_TO_OBJ(serverBD.niSnacks[SNACK_SODA]), TRUE)
						
						SET_ENTITY_VISIBLE(NET_TO_OBJ(serverBD.niSnacks[SNACK_SODA]), FALSE)
						
						NET_PRINT_TIME() NET_PRINT("[AM HOLD UP]      ---------->     snack_anims - CREATE SNACK_SODA    <----------     ") NET_NL()
					ENDIF
				ENDIF
				
				IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niSnacks[SNACK_BEER])
					IF CREATE_NET_OBJ(serverBD.niSnacks[SNACK_BEER], GET_SNACK_MODEL(SNACK_BEER), GET_SNACK_INIT_POS(SNACK_BEER))
						RETAIN_ENTITY_IN_INTERIOR(NET_TO_OBJ(serverBD.niSnacks[SNACK_BEER]), GET_INTERIOR_AT_COORDS(vRegStaff))
						
						FREEZE_ENTITY_POSITION(NET_TO_OBJ(serverBD.niSnacks[SNACK_BEER]), TRUE)
						
						SET_ENTITY_VISIBLE(NET_TO_OBJ(serverBD.niSnacks[SNACK_BEER]), FALSE)
						
						NET_PRINT_TIME() NET_PRINT("[AM HOLD UP]      ---------->     snack_anims - CREATE SNACK_BEER	<----------     ") NET_NL()
					ENDIF
				ENDIF
				
				IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niSnacks[SNACK_SPRUNK])
					IF CREATE_NET_OBJ(serverBD.niSnacks[SNACK_SPRUNK], GET_SNACK_MODEL(SNACK_SPRUNK), GET_SNACK_INIT_POS(SNACK_SPRUNK))
						RETAIN_ENTITY_IN_INTERIOR(NET_TO_OBJ(serverBD.niSnacks[SNACK_SPRUNK]), GET_INTERIOR_AT_COORDS(vRegStaff))
						
						FREEZE_ENTITY_POSITION(NET_TO_OBJ(serverBD.niSnacks[SNACK_SPRUNK]), TRUE)
						
						SET_ENTITY_VISIBLE(NET_TO_OBJ(serverBD.niSnacks[SNACK_SPRUNK]), FALSE)
						
						NET_PRINT_TIME() NET_PRINT("[AM HOLD UP]      ---------->     snack_anims - CREATE SNACK_SPRUNK	<----------     ") NET_NL()
					ENDIF
				ENDIF
				
				/*IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niSnacks[SNACK_LOTTERY])
					IF CREATE_NET_OBJ(serverBD.niSnacks[SNACK_BEER], GET_SNACK_MODEL(SNACK_LOTTERY), GET_SNACK_INIT_POS(SNACK_LOTTERY))
						RETAIN_ENTITY_IN_INTERIOR(NET_TO_OBJ(serverBD.niSnacks[SNACK_LOTTERY]), GET_INTERIOR_AT_COORDS(vRegStaff))
						
						FREEZE_ENTITY_POSITION(NET_TO_OBJ(serverBD.niSnacks[SNACK_LOTTERY]), TRUE)
						
						SET_ENTITY_VISIBLE(NET_TO_OBJ(serverBD.niSnacks[SNACK_LOTTERY]), FALSE)
						
						NET_PRINT_TIME() NET_PRINT("     ---------->     snack_anims - CREATE SNACK_LOTTERY	<----------     ") NET_NL()
					ENDIF
				ENDIF*/
			ELSE
				NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - SNACK CAN'T BE REGISTERED AS MISSION ENTITY  <----------     ") NET_NL()
			ENDIF
		ELSE
			NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - SNACK MODEL NOT LOADED   <----------     ") NET_NL()
		ENDIF
	
	ELSE
		NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - ALL SNACKS CREATED   <----------     ") NET_NL()
		SET_BIT(serverBD.sMenuData.iServerSnackBitSet, biSS_SnacksCreated)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Load and Create the Staff and the Bag
FUNC BOOL CREATE_STAFF_AND_BAG()
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.sMenuData.niRegStaff)
	OR NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niCashBag)
	OR NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niTill)
		IF REQUEST_LOAD_MODEL(StaffModel)
		AND REQUEST_LOAD_MODEL(GET_HOLD_UP_MODEL(HUP_MODEL_CASH_BAG))
		AND REQUEST_LOAD_MODEL(GET_HOLD_UP_MODEL(HUP_MODEL_TILL))
			//IF CAN_REGISTER_MISSION_PEDS(1)
			//AND CAN_REGISTER_MISSION_OBJECTS(1)
			IF CAN_REGISTER_MISSION_ENTITIES(1, 0, 2, 0)	
				//Create Staff
				IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.sMenuData.niRegStaff)
					IF CREATE_NET_PED(serverBD.sMenuData.niRegStaff, PEDTYPE_CIVMALE, StaffModel, vRegStaff-<<0, 0, 1.0>>, serverBD.fRegStaffHeading)

	//					Head 		0-2		****	0-2		0		0-2
	//					Hair		0-2		****	0		0-1		0-1
	//					Torso		0-1		****	0-2		0-2
	//					Leg			0		****	0-1
	//					Special		0-1		****	0-2		0
						SET_PED_COMPONENT_VARIATION(NET_TO_PED(serverBD.sMenuData.niRegStaff), PED_COMP_HEAD, iStaffDraw[0], iStaffTexture[0])
						SET_PED_COMPONENT_VARIATION(NET_TO_PED(serverBD.sMenuData.niRegStaff), PED_COMP_HAIR, iStaffDraw[1], iStaffTexture[1])
						SET_PED_COMPONENT_VARIATION(NET_TO_PED(serverBD.sMenuData.niRegStaff), PED_COMP_TORSO, iStaffDraw[2], iStaffTexture[2])
						SET_PED_COMPONENT_VARIATION(NET_TO_PED(serverBD.sMenuData.niRegStaff), PED_COMP_LEG, iStaffDraw[3], iStaffTexture[3])
						SET_PED_COMPONENT_VARIATION(NET_TO_PED(serverBD.sMenuData.niRegStaff), PED_COMP_SPECIAL, iStaffDraw[4], iStaffTexture[4])
						//SET_PED_RANDOM_COMPONENT_VARIATION(NET_TO_PED(serverBD.sMenuData.niRegStaff))
						
						RETAIN_ENTITY_IN_INTERIOR(NET_TO_PED(serverBD.sMenuData.niRegStaff), GET_INTERIOR_AT_COORDS(vRegStaff))
												
						SET_PED_KEEP_TASK(NET_TO_PED(serverBD.sMenuData.niRegStaff), TRUE)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NET_TO_PED(serverBD.sMenuData.niRegStaff), TRUE)
						
						SET_ENTITY_IS_TARGET_PRIORITY(NET_TO_PED(serverBD.sMenuData.niRegStaff), TRUE)
						
						SET_RAGDOLL_BLOCKING_FLAGS(NET_TO_PED(serverBD.sMenuData.niRegStaff), RBF_PLAYER_IMPACT)
						
						SET_PED_CAN_EVASIVE_DIVE(NET_TO_PED(serverBD.sMenuData.niRegStaff), FALSE)
						
						SET_PED_FLEE_ATTRIBUTES(NET_TO_PED(serverBD.sMenuData.niRegStaff), FA_DISABLE_COWER, TRUE)
						
						//SET_ENTITY_HEALTH(NET_TO_PED(serverBD.sMenuData.niRegStaff), 101)
												
						REMOVE_ALL_PED_WEAPONS(NET_TO_PED(serverBD.sMenuData.niRegStaff))
						
						SET_ENTITY_HEALTH(NET_TO_PED(serverBD.sMenuData.niRegStaff), ROUND(200*g_sMPTunables.fAiHealthModifier))
						
						IF NOT DECOR_EXIST_ON(NET_TO_PED(serverBD.sMenuData.niRegStaff), "XP_Blocker")
							DECOR_SET_BOOL(NET_TO_PED(serverBD.sMenuData.niRegStaff), "XP_Blocker", TRUE)
						ENDIF
						
						ADD_PED_FOR_DIALOGUE(sSpeech, 3, NET_TO_PED(serverBD.sMenuData.niRegStaff), serverBD.tlStaffVoice, TRUE, FALSE)
						NET_PRINT_TIME() NET_PRINT_STRINGS("[AM HOLD UP] - REGISTER STAFF CREATED     <----------     ", GET_MP_MISSION_NAME(shopSnacks.thisHoldupMission)) NET_NL()
					ENDIF
				ENDIF
				
				//Create Cash Bag
				//IF CREATE_NET_OBJ(serverBD.niCashBag, GET_HOLD_UP_MODEL(HUP_MODEL_CASH_BAG), vCashBagPos)//, FALSE)
				IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niCashBag)
					OBJECT_INDEX oiTemp = CREATE_PORTABLE_PICKUP(PICKUP_PORTABLE_CRATE_UNFIXED_LOW_GLOW, vCashBagPos, FALSE, GET_HOLD_UP_MODEL(HUP_MODEL_CASH_BAG))
					
					IF DOES_ENTITY_EXIST(oiTemp)
						serverBD.niCashBag = OBJ_TO_NET(oiTemp)
						
						//SET_TEAM_PICKUP_OBJECT(NET_TO_OBJ(serverBD.niCashBag), GET_PLAYER_TEAM(PLAYER_ID()), FALSE)
						//NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - CASH BAG - SET CANNOT BE PICKED UP      <----------     ") NET_NL()
						
						//GET_ENTITY_COORDS(NET_TO_PED(serverBD.sMenuData.niRegStaff)), FALSE)
						FREEZE_ENTITY_POSITION(NET_TO_OBJ(serverBD.niCashBag), TRUE)
						SET_ENTITY_COLLISION(NET_TO_OBJ(serverBD.niCashBag), FALSE)
						SET_ENTITY_HEADING(NET_TO_OBJ(serverBD.niCashBag), fCashBagHeading)
						RETAIN_ENTITY_IN_INTERIOR(NET_TO_OBJ(serverBD.niCashBag), GET_INTERIOR_AT_COORDS(vRegStaff))
						SET_ENTITY_INVINCIBLE(NET_TO_OBJ(serverBD.niCashBag), TRUE)
						SET_PICKUP_OBJECT_GLOW_OFFSET(NET_TO_OBJ(serverBD.niCashBag), -0.2)
						
						PREVENT_COLLECTION_OF_PORTABLE_PICKUP(NET_TO_OBJ(serverBD.niCashBag), TRUE)
						
						//ATTACH_ENTITY_TO_ENTITY(NET_TO_OBJ(serverBD.niCashBag), NET_TO_PED(serverBD.sMenuData.niRegStaff), GET_PED_BONE_INDEX(NET_TO_PED(serverBD.sMenuData.niRegStaff), BONETAG_PH_L_HAND), <<0,0,-0.1+0.25>>, <<150,0,0>>, TRUE, TRUE)
						SET_ENTITY_VISIBLE(NET_TO_OBJ(serverBD.niCashBag), FALSE)
						NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - CREATE CASH BAG OBJECT    <----------     ") NET_NL()
					ENDIF
				ENDIF
				
				//Create Till Drawer
				IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niTill)
					IF CREATE_NET_OBJ(serverBD.niTill, GET_HOLD_UP_MODEL(HUP_MODEL_TILL), vTillPos-<<0, 0, 0.12>>)
						SET_ENTITY_HEADING(NET_TO_OBJ(serverBD.niTill), fTillHeading)
						RETAIN_ENTITY_IN_INTERIOR(NET_TO_OBJ(serverBD.niTill), GET_INTERIOR_AT_COORDS(vRegStaff))
						
						FREEZE_ENTITY_POSITION(NET_TO_OBJ(serverBD.niTill), TRUE)
						
						//SET_ENTITY_VISIBLE(NET_TO_OBJ(serverBD.niCashBag), FALSE)
						
						//CREATE_MODEL_HIDE(scenePosition, 1.0, //ArtTill, TRUE)
						NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - CREATE TILL DRAWER OBJECT    <----------     ") NET_NL()
					ENDIF
				ENDIF
				
			ELSE
				NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - PED/BAG/TILL CAN'T BE REGISTERED AS MISSION ENTITY  <----------     ") NET_NL()
			ENDIF
		ELSE
			NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - PED/BAG/TILL MODEL NOT LOADED   <----------     ") NET_NL()
		ENDIF
		
	//We have created the Entities
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//Cleans up the staff
PROC CLEANUP_STAFF()
//	INT i
	
	//Register Staff
	IF NOT IS_NET_PED_INJURED(serverBD.sMenuData.niRegStaff)
		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sMenuData.niRegStaff)
			CLEANUP_NET_ID(serverBD.sMenuData.niRegStaff)
			NET_PRINT_TIME() NET_PRINT_STRINGS("[AM HOLD UP] - REGISTER STAFF CLEANED UP - INJURED      <----------     ", GET_MP_MISSION_NAME(shopSnacks.thisHoldupMission)) NET_NL()
		ENDIF
	ENDIF
	
ENDPROC

//Controls the help text for Hold Ups
PROC CONTROL_HELP_TEXT()
	IF IS_NET_PLAYER_OK(PLAYER_ID())
				
		//Freemode Help Text
		IF NOT IS_BIT_SET(shopSnacks.iBoolsBitSet, biTutorialHelpDone)
		AND IS_BIT_SET(serverBD.iServerBitSet, biS_SyncSceneStartedRunning)
			IF NOT IS_NET_PED_INJURED(serverBD.sMenuData.niRegStaff)
				iLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(serverBD.iSceneID)
				IF iLocalSceneID != -1
					IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSceneID)
						IF NOT IS_BIT_SET(fmHoldUpTutStruct.iFmHoldUpTutBitset, biHT_InTriggeredHoldUp)
						AND IS_LOCAL_PLAYER_DOING_HOLD_UP_TUTORIAL()
						AND IS_LOCAL_PLAYER_USING_TUTORIAL_HOLD_UP_STORE()
							SET_BIT(fmHoldUpTutStruct.iFmHoldUpTutBitset, biHT_InTriggeredHoldUp)
							NET_PRINT_TIME() NET_PRINT_STRINGS("[AM HOLD UP]      ----->   AM_HOLD_UP - biHT_InTriggeredHoldUp SET  <-----     ", GET_MP_MISSION_NAME(shopSnacks.thisHoldupMission)) NET_NL()
						ENDIF
						
						IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
							INT iStat = GET_MP_INT_CHARACTER_STAT(MP_STAT_TUTORIAL_BITSET)
							IF NOT IS_BIT_SET(iStat, TUTBS_HOLD_UP_HELP_3)
								IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalSceneID) < 0.40
								//AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vStoreMin, vStoreMax, fStoreWidth)
								AND GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = interiorID
									PRINT_HELP("FHU_HELP3")	//~s~Shouting or shooting near the owner will make him fill the cash bag quicker.~n~The cash earned is shared between everyone holding up the store.
									SET_BIT(iStat, TUTBS_HOLD_UP_HELP_3)
									SET_MP_INT_CHARACTER_STAT(MP_STAT_TUTORIAL_BITSET, iStat)
									NET_PRINT_TIME() NET_PRINT_STRINGS("[AM HOLD UP]      ----->   AM_HOLD_UP - TUTBS_HOLD_UP_HELP_3 SET  <-----     ", GET_MP_MISSION_NAME(shopSnacks.thisHoldupMission)) NET_NL()
								ENDIF
							ELIF NOT IS_BIT_SET(iStat, TUTBS_HOLD_UP_HELP_5)
							OR NOT IS_BIT_SET(iStat, TUTBS_HOLD_UP_HELP_6)
								IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalSceneID) < 0.70
								//AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vStoreMin, vStoreMax, fStoreWidth)
								AND GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = interiorID
									PRINT_HELP("FHU_HELP4")	//~s~Wait for the Cashier to empty the register to get the full amount of cash.
									SET_BIT(iStat, TUTBS_HOLD_UP_HELP_5)
									SET_MP_INT_CHARACTER_STAT(MP_STAT_TUTORIAL_BITSET, iStat)
									NET_PRINT_TIME() NET_PRINT_STRINGS("[AM HOLD UP]      ----->   AM_HOLD_UP - TUTBS_HOLD_UP_HELP_5 SET  <-----     ", GET_MP_MISSION_NAME(shopSnacks.thisHoldupMission)) NET_NL()
									SET_BIT(shopSnacks.iBoolsBitSet, biTutorialHelpDone)
								ENDIF
							ELSE
								SET_BIT(shopSnacks.iBoolsBitSet, biTutorialHelpDone)
							ENDIF
						ENDIF
					
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//Manually Empty Till - Help Text
		IF NOT IS_FM_PLAYER_CRITICAL_TO_THIS_FM_EVENT(PLAYER_ID(), FMMC_TYPE_HUNT_THE_BEAST)
		AND NOT IS_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITITCAL_ENTITY()
		AND GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) != FMMC_TYPE_GB_CONTRABAND_BUY
		AND GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) != FMMC_TYPE_BIKER_CONTRABAND_SELL
		AND GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) != FMMC_TYPE_BIKER_CONTRABAND_DEFEND
		AND GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) != FMMC_TYPE_FMBB_CLUB_MANAGEMENT
		AND GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) != FMMC_TYPE_FMBB_SELL
		AND GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) != FMMC_TYPE_FMBB_DEFEND
		AND NOT IS_PLAYER_AN_ANIMAL(PLAYER_ID())
		AND (NOT IS_PLAYER_CRITICAL_TO_ANY_EVENT(PLAYER_ID()) 
		OR GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_GB_CHAL_ROB_SHOP)
			IF NOT IS_BIT_SET(shopSnacks.iBoolsBitSet2, bi2ManualRobHelpDone)
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					IF (IS_NET_PED_INJURED(serverBD.sMenuData.niRegStaff) OR serverBD.sMenuData.eStaffState > eSTAFF_ATTACK)
					AND NOT IS_BIT_SET(serverBD.iServerBitSet, biS_CashBagAvailableForPickup)
					AND NOT IS_BIT_SET(serverBD.iServerBitSet, biS_CashPickedUp)
					AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_CashBagPickedUp)
					AND GET_NUMBER_OF_FIRES_IN_RANGE(scenePosition, 1.5) = 0
						INT iStat = GET_MP_INT_CHARACTER_STAT(MP_STAT_TUTORIAL_BITSET)
						IF NOT IS_BIT_SET(iStat, TUTBS_HOLD_UP_HELP_MANUAL_ROB_1)
						OR NOT IS_BIT_SET(iStat, TUTBS_HOLD_UP_HELP_MANUAL_ROB_2)
							PRINT_HELP("FHU_HELPM")	//The store clerk is no longer able to empty the register. Go up to the register the store clerk was using and empty it manually.
							IF NOT IS_BIT_SET(iStat, TUTBS_HOLD_UP_HELP_MANUAL_ROB_1)
								SET_BIT(iStat, TUTBS_HOLD_UP_HELP_MANUAL_ROB_1)
								NET_PRINT_TIME() NET_PRINT_STRINGS("[AM HOLD UP]      ----->   AM_HOLD_UP - TUTBS_HOLD_UP_HELP_MANUAL_ROB_1 SET  <-----     ", GET_MP_MISSION_NAME(shopSnacks.thisHoldupMission)) NET_NL()
							ELSE
								SET_BIT(iStat, TUTBS_HOLD_UP_HELP_MANUAL_ROB_2)
								NET_PRINT_TIME() NET_PRINT_STRINGS("[AM HOLD UP]      ----->   AM_HOLD_UP - TUTBS_HOLD_UP_HELP_MANUAL_ROB_2 SET  <-----     ", GET_MP_MISSION_NAME(shopSnacks.thisHoldupMission)) NET_NL()
							ENDIF
							SET_MP_INT_CHARACTER_STAT(MP_STAT_TUTORIAL_BITSET, iStat)
							SET_BIT(shopSnacks.iBoolsBitSet2, bi2ManualRobHelpDone)
						ELSE
							SET_BIT(shopSnacks.iBoolsBitSet2, bi2ManualRobHelpDone)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FHU_HELPM")
					IF IS_BIT_SET(serverBD.iServerBitSet, biS_CashBagAvailableForPickup)
					OR IS_BIT_SET(serverBD.iServerBitSet, biS_CashPickedUp)
					OR IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_CashBagPickedUp)
					//OR GET_NUMBER_OF_FIRES_IN_RANGE(scenePosition, 1.5) = 0
						CLEAR_HELP()
						NET_PRINT_TIME() NET_PRINT_STRINGS("[AM HOLD UP]      ----->   AM_HOLD_UP - CLEAR MANUAL ROB HELP - FHU_HELPM <-----     ", GET_MP_MISSION_NAME(shopSnacks.thisHoldupMission)) NET_NL()
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		
		/*Removed for Bug 1242201
		IF IS_BIT_SET(shopSnacks.iBoolsBitSet, biCopsAlerted)
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FHU_HELP2")
				CLEAR_HELP()
			ENDIF
		ENDIF*/
		
		//Tutorial Help Text
		/*IF NOT IS_BIT_SET(shopSnacks.iBoolsBitSet, biGuardHelpDone)
			IF NETWORK_IS_IN_TUTORIAL_SESSION()
				IF NETWORK_GET_NUM_PARTICIPANTS() > 1
					PRINT_HELP("HUP_HELP4")	//~s~One player should target the shop assistant while the others stand guard.
					SET_BIT(shopSnacks.iBoolsBitSet, biGuardHelpDone)
				ENDIF
			ELSE
				SET_BIT(shopSnacks.iBoolsBitSet, biGuardHelpDone)
			ENDIF
		ENDIF*/
	ENDIF
ENDPROC

//Play the dialogue for the staff and the player
PROC CONTROL_DIALOGUE_LINE(INT iLine)
	IF IS_BIT_SET(serverBD.iServerBitSet,biS_AnyCrewInShop)
		IF NOT IS_NET_PED_INJURED(serverBD.sMenuData.niRegStaff)
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sMenuData.niRegStaff)
			OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.sMenuData.niRegStaff) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
				IF NOT IS_PED_RAGDOLL(NET_TO_PED(serverBD.sMenuData.niRegStaff))
					
					//Get Line from Dialogue Set
					TEXT_LABEL_15 tlRootLabel
					IF serverBD.iStaffVariation = 0
						tlRootLabel = "RB_SH2"
					ELIF serverBD.iStaffVariation = 1
						tlRootLabel = "RB_12_F"
					ELSE
						tlRootLabel = "RB_SH3"
					ENDIF
					tlRootLabel+=serverBD.iStaffDialogueSet
					tlRootLabel+=iLine
										
					//Check if we should use the Gun Line
					IF ARE_STRINGS_EQUAL(tlRootLabel, "RB_SH241")
						WEAPON_TYPE CurrentWeapon
						IF GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), CurrentWeapon)
							IF CurrentWeapon = WEAPONTYPE_UNARMED
							OR CurrentWeapon = WEAPONTYPE_INVALID
							OR CurrentWeapon = WEAPONTYPE_OBJECT
							OR CurrentWeapon = WEAPONTYPE_FLARE
							OR CurrentWeapon = WEAPONTYPE_DLC_SNOWBALL
							OR IS_WEAPON_A_MELEE(CurrentWeapon)
							OR IS_WEAPON_A_THROWING(CurrentWeapon)
								tlRootLabel = "RB_SH211"
								PRINTLN("[AM HOLD UP] - USE BACKUP INSTEAD OF GUN LINE")
							ENDIF
						ENDIF
					ENDIF
					
					ADD_PED_FOR_DIALOGUE(sSpeech, 3, NET_TO_PED(serverBD.sMenuData.niRegStaff), serverBD.tlStaffVoice, TRUE, FALSE)
									
					//Play Conversation
					SWITCH iLine
						CASE 0
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(NET_TO_PED(serverBD.sMenuData.niRegStaff), "SHOP_GREET", serverBD.tlStaffAmbientVoice, "SPEECH_PARAMS_FORCE", TRUE)	//SPEECH_PARAMS_FORCE_CRITICAL
							//CREATE_CONVERSATION(sSpeech, "RB_12AU", "RB_12_L0", CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN, TRUE)		//STAFF - Can I help?
							NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - CONTROL_DIALOGUE_LINE 0 - SHOP_GREET") NET_NL()
						BREAK
						
						//*****************  RECORDED
						CASE 1
							CREATE_CONVERSATION_USING_V_CONTENT_IN_DLC(sSpeech, "RB_12AU", tlRootLabel, CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT, DO_NOT_DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN, TRUE)		//STAFF - Please Sir, I don't want any trouble.
							NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - CONTROL_DIALOGUE_LINE 1 - ") NET_PRINT(tlRootLabel) NET_NL()
						BREAK
						
						CASE 2
							CREATE_CONVERSATION_USING_V_CONTENT_IN_DLC(sSpeech, "RB_12AU", tlRootLabel, CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT, DO_NOT_DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN, TRUE)		//STAFF - Okay, okay. I'm going as fast as I can.
							NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - CONTROL_DIALOGUE_LINE 2 - ") NET_PRINT(tlRootLabel) NET_NL()
						BREAK
						
						CASE 3
							CREATE_CONVERSATION_USING_V_CONTENT_IN_DLC(sSpeech, "RB_12AU", tlRootLabel, CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT, DO_NOT_DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN, TRUE)		//STAFF - That's all of it, I swear.
							NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - CONTROL_DIALOGUE_LINE 3 - ") NET_PRINT(tlRootLabel) NET_NL()
						BREAK
						//*****************
						
						CASE 4
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(NET_TO_PED(serverBD.sMenuData.niRegStaff), "SHOP_BRAVE", serverBD.tlStaffAmbientVoice, "SPEECH_PARAMS_FORCE", TRUE)
							NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - CONTROL_DIALOGUE_LINE 4 - SHOP_BRAVE") NET_NL()
							//CREATE_CONVERSATION(sSpeech, "RB_12AU", "RB_12_L3", CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN, TRUE)	//STAFF - Get the fuck out of my store asshole!
						BREAK
						
						CASE 10
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(NET_TO_PED(serverBD.sMenuData.niRegStaff), "SHOP_REMOVE_VEHICLE", serverBD.tlStaffAmbientVoice, "SPEECH_PARAMS_FORCE", TRUE)
							NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - CONTROL_DIALOGUE_LINE 10 - SHOP_REMOVE_VEHICLE") NET_NL()
							//CREATE_CONVERSATION(sSpeech, "RB_12AU", "RB_12_RV", CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN, TRUE)	//STAFF - React to Vehicle - Random
						BREAK
						
						CASE 11
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(NET_TO_PED(serverBD.sMenuData.niRegStaff), "SHOP_REACT_TO_SHOUT", serverBD.tlStaffAmbientVoice, "SPEECH_PARAMS_FORCE", TRUE)
							NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - CONTROL_DIALOGUE_LINE 11 - SHOP_REACT_TO_SHOUT") NET_NL()
							//CREATE_CONVERSATION(sSpeech, "RB_12AU", "RB_12_RS", CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN, TRUE)	//STAFF - React to Shout - Random
						BREAK
						
						CASE 12
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(NET_TO_PED(serverBD.sMenuData.niRegStaff), "SHOP_HURRYING", serverBD.tlStaffAmbientVoice, "SPEECH_PARAMS_FORCE", TRUE)
							NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - CONTROL_DIALOGUE_LINE 12 - SHOP_HURRYING") NET_NL()
							//CREATE_CONVERSATION(sSpeech, "RB_12AU", "RB_12_RG", CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN, TRUE)	//STAFF - React to Gun Shot - Random
						BREAK
						
						CASE 13
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(NET_TO_PED(serverBD.sMenuData.niRegStaff), "SHOP_NO_COPS", serverBD.tlStaffAmbientVoice, "SPEECH_PARAMS_FORCE", TRUE)
							NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - CONTROL_DIALOGUE_LINE 13 - SHOP_NO_COPS") NET_NL()
							//CREATE_CONVERSATION(sSpeech, "RB_12AU", "RB_12_RW", CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN, TRUE)	//STAFF - React to Wanted Level - Random
						BREAK
						
						CASE 14
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(NET_TO_PED(serverBD.sMenuData.niRegStaff), "SHOP_THREATENED", serverBD.tlStaffAmbientVoice, "SPEECH_PARAMS_FORCE", TRUE)
							NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - CONTROL_DIALOGUE_LINE 14 - SHOP_THREATENED") NET_NL()
							//CREATE_CONVERSATION(sSpeech, "RB_12AU", "RB_12_RU", CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN, TRUE)	//STAFF - React to Unarmed Aim - Random
						BREAK
						
						CASE 15
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(NET_TO_PED(serverBD.sMenuData.niRegStaff), "SHOP_GREET_START", serverBD.tlStaffAmbientVoice, "SPEECH_PARAMS_FORCE", TRUE)
							NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - CONTROL_DIALOGUE_LINE 15 - SHOP_GREET_START") NET_NL()
							//CREATE_CONVERSATION(sSpeech, "RB_12AU", "RB_12_SWN", CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN, TRUE)	//STAFF - Welcomes Player - New Shift
						BREAK
						CASE 16
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(NET_TO_PED(serverBD.sMenuData.niRegStaff), "SHOP_SCARED_START", serverBD.tlStaffAmbientVoice, "SPEECH_PARAMS_FORCE", TRUE)
							NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - CONTROL_DIALOGUE_LINE 16 - SHOP_SCARED_START") NET_NL()
							//CREATE_CONVERSATION(sSpeech, "RB_12AU", "RB_12_RWN", CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN, TRUE)	//STAFF - React to Weapon - New Shift
						BREAK
						CASE 17
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(NET_TO_PED(serverBD.sMenuData.niRegStaff), "SHOP_NO_COPS_START", serverBD.tlStaffAmbientVoice, "SPEECH_PARAMS_FORCE", TRUE)
							NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - CONTROL_DIALOGUE_LINE 17 - SHOP_NO_COPS_START") NET_NL()
							//CREATE_CONVERSATION(sSpeech, "RB_12AU", "RB_12_RLN", CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN, TRUE)	//STAFF - React to Wanted Level - Random - New Shift
						BREAK
						
						CASE 18
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(NET_TO_PED(serverBD.sMenuData.niRegStaff), "SHOP_GREET_END", serverBD.tlStaffAmbientVoice, "SPEECH_PARAMS_FORCE", TRUE)
							NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - CONTROL_DIALOGUE_LINE 18 - SHOP_GREET_END") NET_NL()
							//CREATE_CONVERSATION(sSpeech, "RB_12AU", "RB_12_SWE", CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN, TRUE)	//STAFF - Welcomes Player - End Shift
						BREAK
						CASE 19
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(NET_TO_PED(serverBD.sMenuData.niRegStaff), "SHOP_SCARED_END", serverBD.tlStaffAmbientVoice, "SPEECH_PARAMS_FORCE", TRUE)
							NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - CONTROL_DIALOGUE_LINE 19 - SHOP_SCARED_END") NET_NL()
							//CREATE_CONVERSATION(sSpeech, "RB_12AU", "RB_12_RWE", CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN, TRUE)	//STAFF - React to Weapon - End Shift
						BREAK
						CASE 20
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(NET_TO_PED(serverBD.sMenuData.niRegStaff), "SHOP_NO_COPS_END", serverBD.tlStaffAmbientVoice, "SPEECH_PARAMS_FORCE", TRUE)
							NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - CONTROL_DIALOGUE_LINE 20 - SHOP_NO_COPS_END") NET_NL()
							//CREATE_CONVERSATION(sSpeech, "RB_12AU", "RB_12_RLE", CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN, TRUE)	//STAFF - React to Wanted Level - Random - End Shift
						BREAK
						
						CASE 21
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(NET_TO_PED(serverBD.sMenuData.niRegStaff), "SHOP_COPS_ARRIVED", serverBD.tlStaffAmbientVoice, "SPEECH_PARAMS_FORCE", TRUE)
							NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - CONTROL_DIALOGUE_LINE 21 - SHOP_COPS_ARRIVED") NET_NL()
							//CREATE_CONVERSATION(sSpeech, "RB_12AU", "RB_12_CA", CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN, TRUE)	//STAFF - Staff React to Cop Arrival - Random
						BREAK
						
						CASE 22
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(NET_TO_PED(serverBD.sMenuData.niRegStaff), "SHOP_SELL", serverBD.tlStaffAmbientVoice, "SPEECH_PARAMS_FORCE", TRUE)
							NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - CONTROL_DIALOGUE_LINE 22 - SHOP_SELL") NET_NL()
							//CREATE_CONVERSATION(sSpeech, "RB_12AU", "RB_12_SNK", CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN, TRUE)	//STAFF - Staff React to Snack Buy - Random
						BREAK
						
						CASE 23
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(NET_TO_PED(serverBD.sMenuData.niRegStaff), "SHOP_NO_ENTRY", serverBD.tlStaffAmbientVoice, "SPEECH_PARAMS_FORCE", TRUE)
							NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - CONTROL_DIALOGUE_LINE 23 - SHOP_NO_ENTRY") NET_NL()
							//CREATE_CONVERSATION(sSpeech, "RB_12AU", "RB_12_RST", CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN, TRUE)	//STAFF - Staff React to Player entering Restricted Area - Random
						BREAK
						
						CASE 24 
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(NET_TO_PED(serverBD.sMenuData.niRegStaff), "SHOP_STEAL", serverBD.tlStaffAmbientVoice, "SPEECH_PARAMS_FORCE", TRUE)		//STAFF - Staff React to Shoplifting - Random
							NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - CONTROL_DIALOGUE_LINE 24 - SHOP_STEAL") NET_NL()
						BREAK
						
						CASE 25 
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(NET_TO_PED(serverBD.sMenuData.niRegStaff), "SCREAM_PANIC", serverBD.tlStaffAmbientVoice, "SPEECH_PARAMS_FORCE", TRUE)	//STAFF - Staff Flee - Random
							NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - CONTROL_DIALOGUE_LINE 25 - SCREAM_PANIC") NET_NL()
						BREAK
					ENDSWITCH
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

////Controls the dialogue whilst the store owner is emptying the register
//PROC CONTROL_EMPTYING_REGISTER_DIALOGUE()
//	//IF NOT IS_BIT_SET(shopSnacks.iBoolsBitSet, biRegisterEmptied)
//	//AND NOT IS_BIT_SET(shopSnacks.iBoolsBitSet, biEmptyingDialogue)
//	//AND IS_BIT_SET(shopSnacks.iBoolsBitSet, biEmptyRegisterStarted)
//	IF serverBD.sMenuData.eStaffState = eSTAFF_EMPTY_REGISTER
//		IF IS_NET_PLAYER_OK(PLAYER_ID())
//			IF NOT IS_NET_PED_INJURED(serverBD.sMenuData.niRegStaff)
//				//IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), NET_TO_PED(serverBD.sMenuData.niRegStaff), <<10, 10, 10>>)
//					
//					//IF IS_ENTITY_PLAYING_ANIM(NET_TO_PED(serverBD.sMenuData.niRegStaff), "mp_amb_crim", "holdup_rifle_victim")
//					iLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(serverBD.iSceneID)
//					IF iLocalSceneID != -1
//						IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSceneID)
//							//IF GET_ENTITY_ANIM_CURRENT_TIME(NET_TO_PED(serverBD.sMenuData.niRegStaff), "mp_amb_crim", "holdup_rifle_victim") > 0.1
//							IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalSceneID) > 0.1
//							AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//								CONTROL_DIALOGUE_LINE(2)	//STAFF - Okay, okay. I'm going as fast as I can.
//								SET_BIT(shopSnacks.iBoolsBitSet, biEmptyingDialogue)
//							ENDIF
//							
//							IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalSceneID) > 0.875
//							AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//								CONTROL_DIALOGUE_LINE(3)		//STAFF - That's all of it, I swear.
//							ENDIF
//						ENDIF
//					ENDIF
//					
//				//ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
//ENDPROC				

//PURPOSE: Returns TRUE if the SYnc Scene has finsihed
FUNC BOOL HAS_SYNC_SCENE_FINISHED()
	iLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(serverBD.iSceneID)
	IF iLocalSceneID != -1
		IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSceneID)	
			IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalSceneID) >= 1.0
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Stops the sync scene
PROC STOP_SYNC_SCENE()
	iLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(serverBD.iSceneID)
	IF iLocalSceneID != -1
		IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSceneID)	
			NETWORK_STOP_SYNCHRONISED_SCENE(serverBD.iSceneID)
			NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - STOP_SYNC_SCENE      <----------     ") NET_NL()
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Returns TRUE if the sync scene is past the specified phase
FUNC BOOL IS_PAST_SYNC_SCENE_PHASE(FLOAT fPhase)
	iLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(serverBD.iSceneID)
	IF iLocalSceneID != -1
		IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSceneID)
			IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalSceneID) >= fPhase
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Controls a player picking up the Cash Bag
PROC CONTROL_CASH_BAG_PICKUP()
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niCashBag)
		IF NOT IS_ENTITY_DEAD(NET_TO_OBJ(serverBD.niCashBag))
			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_CashPickedUp)
			AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_CashBagPickedUp)
			AND serverBD.fCashBagFillAmount > 0.0
				
				IF HAS_SYNC_SCENE_FINISHED()
				OR IS_PAST_SYNC_SCENE_PHASE(0.87)	//0.87)
				OR serverBD.sMenuData.eStaffState >= eSTAFF_HANDS_UP
				OR IS_BIT_SET(serverBD.iServerBitSet, biS_CashBagDroppedEarly)
					//RENDER_FAKE_PICKUP_GLOW(GET_ENTITY_COORDS(NET_TO_OBJ(serverBD.niCashBag)), 2)
					//DRAW_SPRITE_ON_OBJECTIVE_OBJECT_THIS_FRAME(NET_TO_OBJ(serverBD.niCashBag), MP_TAG_HOLDUP_CASH)
					
					IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_CashBagAvailbleForPickup)
						//IF NOT IS_ENTITY_IN_AIR(NET_TO_OBJ(serverBD.niCashBag))
							//SET_TEAM_PICKUP_OBJECT(NET_TO_OBJ(serverBD.niCashBag), GET_PLAYER_TEAM(PLAYER_ID()), TRUE)
							
							//STOP_SYNCHRONIZED_ENTITY_ANIM(NET_TO_OBJ(serverBD.niCashBag), NORMAL_BLEND_OUT, TRUE)
							
							IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niCashBag)
								PREVENT_COLLECTION_OF_PORTABLE_PICKUP(NET_TO_OBJ(serverBD.niCashBag), FALSE)
								NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - CASH BAG - PREVENT_COLLECTION_OF_PORTABLE_PICKUP - XX - FALSE     <----------     ") NET_NL()
								SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_CashBagPreventCollectionCleared)
							ENDIF
							//SET_PICKUP_OBJECT_GLOW_OFFSET(NET_TO_OBJ(serverBD.niCashBag), -0.2)
							IF GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) != FMMC_TYPE_GB_CHAL_COLLECT_MONEY
							SET_MAX_NUM_PORTABLE_PICKUPS_CARRIED_BY_PLAYER(GET_HOLD_UP_MODEL(HUP_MODEL_CASH_BAG), 1)
							ENDIF
							SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_CashBagAvailbleForPickup)
							NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - CASH BAG - SET CAN BE PICKED UP      <----------     ") NET_NL()
						//ELSE
						//	NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - CASH BAG - IN AIR     <----------     ") NET_NL()
						//ENDIF
					ENDIF
					
					//IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), NET_TO_OBJ(serverBD.niCashBag), <<0.75, 0.75, 1.25>>, FALSE, TRUE, TM_ON_FOOT)
					IF IS_ENTITY_ATTACHED_TO_ENTITY(NET_TO_OBJ(serverBD.niCashBag), PLAYER_PED_ID())
						playerBD[PARTICIPANT_ID_TO_INT()].vPickedUpCashBagPos = GET_ENTITY_COORDS(PLAYER_PED_ID())//NET_TO_OBJ(serverBD.niCashBag))
						GET_GROUND_Z_FOR_3D_COORD(playerBD[PARTICIPANT_ID_TO_INT()].vPickedUpCashBagPos, playerBD[PARTICIPANT_ID_TO_INT()].vPickedUpCashBagPos.z)
						IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niCashBag)
							DETACH_ENTITY(NET_TO_OBJ(serverBD.niCashBag))
							DELETE_NET_ID(serverBD.niCashBag)
							NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - CASH BAG - DELETED BY PLAYER      <----------     ") NET_NL()
						ENDIF
						

						INT iCash = GET_RANDOM_INT_IN_RANGE(iMinCash, iMaxCash)
						PRINTLN("[AM HOLD UP] - CASH BAG - iMinCash = ", iMinCash)
						PRINTLN("[AM HOLD UP] - CASH BAG - iMaxCash = ", iMaxCash)
						
						//iCash = ROUND(TO_FLOAT(iCash)/2)
						//NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - CASH BAG - CASH HALVED DUE TO FREEMODE      <----------     ") NET_NL()
						IF IS_BIT_SET(serverBD.iServerBitSet, biS_CashBagDroppedEarly)
							iCash = FLOOR(TO_FLOAT(iCash)*serverBD.fCashBagFillAmount)	//0.5 to 0.8
							NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - CASH BAG - DROPPED EARLY - PLAYER EARNS AMOUNT * ") NET_PRINT_FLOAT(serverBD.fCashBagFillAmount)  NET_NL()
						ENDIF
						
						NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - CASH BAG - BASIC CONTENTS = $") NET_PRINT_INT(iCash) NET_NL()
						IF serverBD.sMenuData.iTotalCashSpent > TOTAL_CASH_SPENT_LIMIT
							serverBD.sMenuData.iTotalCashSpent = TOTAL_CASH_SPENT_LIMIT
							NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - CASH BAG - iTotalCashSpent TOO HIGH - LIMITED TO $") NET_PRINT_INT(serverBD.sMenuData.iTotalCashSpent) NET_NL()
						ENDIF
						iCash += serverBD.sMenuData.iTotalCashSpent
						NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - CASH BAG - ADDITIONAL CONTENTS = $") NET_PRINT_INT(serverBD.sMenuData.iTotalCashSpent) NET_NL()
						NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - CASH BAG - TOTAL CONTENTS = $") NET_PRINT_INT(iCash) NET_NL()
						
						iCash = MULTIPLY_CASH_BY_TUNABLE(iCash)
						IF iCash > g_sMPTunables.iHoldUpCashRewardCap
							iCash = g_sMPTunables.iHoldUpCashRewardCap
							NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - CASH BAG - CASH ABOVE LIMIT SO CAPPED AT $") NET_PRINT_INT(iCash) NET_NL()
						ENDIF
						
						//On an FM event where the wanted is supressed
						IF FM_EVENT_IS_PLAYER_ON_ANY_FM_EVENT(PLAYER_ID())
						AND GET_WANTED_OVERRIDE_BASED_ON_TYPE(FM_EVENT_GET_PLAYER_CURRENT_FM_EVENT(PLAYER_ID())) < 1.0
							IF g_sMPTunables.fHold_Up_Event_Mult > 1.0
								g_sMPTunables.fHold_Up_Event_Mult = 1.0
							ENDIF
							PRINTLN("[AM HOLD UP] - g_sMPTunables.fHold_Up_Event_Mult - CASH ABOVE LIMIT SO CAPPED AT ", g_sMPTunables.fHold_Up_Event_Mult)
							iCash = ROUND(TO_FLOAT(iCash) * g_sMPTunables.fHold_Up_Event_Mult)
							PRINTLN("[AM HOLD UP] - iCashGrabbed *= g_sMPTunables.fHold_Up_Event_Mult  =  ", iCash)
						ENDIF
						
//						#IF FEATURE_GANG_BOSS
//						IF NOT GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID())
//						AND GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) != FMMC_TYPE_GB_CHAL_ROB_SHOP
//							PRINTLN("[AM HOLD UP][MAGNATE_GANG_BOSS] - Cash before boss cut: $", iCash)
//							GB_HANDLE_GANG_BOSS_CUT(iCash)
//							PRINTLN("[AM HOLD UP][MAGNATE_GANG_BOSS] - Cash after boss cut: $", iCash)
//						ENDIF
//						#ENDIF
						
						IF iCash > 0
							IF USE_SERVER_TRANSACTIONS()
								INT iTransactionID
								TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_HOLDUPS, iCash, iTransactionID)
							ELSE
								GIVE_LOCAL_PLAYER_CASH_END_OF_MISSION(iCash)
								NETWORK_EARN_FROM_HOLDUPS(iCash)
							ENDIF
						ENDIF
						
						MPGlobalsAmbience.sSmashAndGrabStruct.iCashStolen += iCash
						MPGlobalsAmbience.sSmashAndGrabStruct.bCollectedCash = TRUE
						MPGlobalsAmbience.sSmashAndGrabStruct.bHoldupComplete = TRUE
						iEarnedCash += iCash
						//INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_MONEY_PICKED_FROM_HOLDUPS, iCash)
					
						SET_XP_ANIMATION_MULTIPLIER(XP_TRIPLE_MULT) 
						IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_CashBagPickedUp)
							SET_CASH_REWARD_FOR_LOCATION(serverBd.vPickedUpCashBagPos, iCash)
						ELSE	
							SET_CASH_REWARD_FOR_ENTITY(PLAYER_PED_ID(), iCash)
						ENDIF
					
						SET_LAST_JOB_DATA(LAST_JOB_HOLD_UP, iCash)
					
						IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_BIKER_GANG()
							BIK_ADD_POINTS_FOR_THIS(BIK_ADD_POINTS_ROB_SHOP)
						ENDIF
					
						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_CashBagPickedUp)
						SET_BIT(shopSnacks.iBoolsBitSet2, bi2CashGrabbed)
						NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - CASH BAG - PICKED UP BY PLAYER      <----------     ") NET_NL()
						#IF IS_DEBUG_BUILD NET_LOG("Player Picked Up Cash Bag")	#ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Control the Cash bAg (visibilty, attachment, etc)
PROC CONTROL_CASH_BAG()
	IF serverBD.sMenuData.eStaffState = eSTAFF_EMPTY_REGISTER
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_CashBagDroppedEarly)
		AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_CashBagDroppedEarly)
		AND IS_BIT_SET(serverBD.iServerBitSet, biS_SyncSceneStartedRunning)
			IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niCashBag)
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niCashBag)
				OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.niCashBag) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())	
					IF NOT IS_NET_PED_INJURED(serverBD.sMenuData.niRegStaff)
												
						//Deal with the Reg Staff being pushed
						//IF IS_ENTITY_ATTACHED(NET_TO_OBJ(serverBD.niCashBag))
							IF IS_PED_RAGDOLL(NET_TO_PED(serverBD.sMenuData.niRegStaff))
							//AND NOT IS_BIT_SET(serverBD.iServerBitSet, biS_AnyCrewHasDamagedStaff)
								//**STOP_SYNC_SCENE()
								SET_ENTITY_COLLISION(NET_TO_OBJ(serverBD.niCashBag), TRUE)
								SET_ENTITY_DYNAMIC(NET_TO_OBJ(serverBD.niCashBag), TRUE)
								SET_ENTITY_VELOCITY(NET_TO_OBJ(serverBD.niCashBag), <<0, 0, -1>>)
								
								//CLEAR_PED_TASKS(NET_TO_PED(serverBD.sMenuData.niRegStaff))
								//TASK_PLAY_ANIM(NET_TO_PED(serverBD.sMenuData.niRegStaff), "mp_am_hold_up", "guard_handsup_loop", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING | AF_USE_KINEMATIC_PHYSICS)
								//TASK_PERFORM_SEQUENCE(NET_TO_PED(serverBD.sMenuData.niRegStaff), StaffHandsUpSeq)
								
								
								SET_RAGDOLL_BLOCKING_FLAGS(NET_TO_PED(serverBD.sMenuData.niRegStaff), RBF_PLAYER_IMPACT)
								//DETACH_ENTITY(NET_TO_OBJ(serverBD.niCashBag), FALSE, TRUE)
								SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_CashBagDroppedEarly)
								NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - CASH BAG OBJECT DETACHED - B      <----------     ") NET_NL()
							ENDIF
						//ENDIF
							
					ELSE
						//IF IS_ENTITY_ATTACHED(NET_TO_OBJ(serverBD.niCashBag))
							//DETACH_ENTITY(NET_TO_OBJ(serverBD.niCashBag), FALSE, TRUE)
							//**STOP_SYNC_SCENE()
							SET_ENTITY_COLLISION(NET_TO_OBJ(serverBD.niCashBag), TRUE)
							SET_ENTITY_DYNAMIC(NET_TO_OBJ(serverBD.niCashBag), TRUE)
							SET_ENTITY_VELOCITY(NET_TO_OBJ(serverBD.niCashBag), <<0, 0, -1>>)
							SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_CashBagDroppedEarly)
							NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - CASH BAG OBJECT DETACHED - C      <----------     ") NET_NL()
						//ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	CONTROL_CASH_BAG_PICKUP()
ENDPROC

//PURPOSE:
PROC CONTROL_PROGRESS_BAR()
	IF serverBD.sMenuData.eStaffState = eSTAFF_EMPTY_REGISTER
		IF IS_NET_PLAYER_OK(PLAYER_ID())
			IF NOT IS_NET_PED_INJURED(serverBD.sMenuData.niRegStaff)
				iLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(serverBD.iSceneID)
				IF iLocalSceneID != -1
					IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSceneID)
						//PUT BACK IN FOR BUG 1613252		-	REMOVED FOR BUG 1294509
						FLOAT fProgress = GET_SYNCHRONIZED_SCENE_PHASE(iLocalSceneID)
						fProgress = (fProgress * 1000)
						IF fProgress <= 890
							//DRAW_TIMER_HUD(ROUND(fProgress), 890, "HUP_PROG", GET_TEAM_HUD_COLOUR(GET_PLAYER_TEAM(PLAYER_ID())))
							DRAW_TIMER_HUD(ROUND(fProgress), 890, "HUP_PROG", GET_TEAM_HUD_COLOUR(GET_PLAYER_TEAM(PLAYER_ID())), DEFAULT, DEFAULT, HUDORDER_TOP)	//INTIMIDATION		//~s~PROGRESS
						ENDIF
						
						
						//Update player's shout
						FLOAT fShout = NETWORK_GET_PLAYER_LOUDNESS(PLAYER_ID())
						fShout -= 0.3
						IF fShout < 0
							fShout = 0
						ENDIF
						fShout = (fShout*4.0)
						playerBD[PARTICIPANT_ID_TO_INT()].fShout = fShout
						//NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - PLAYER SHOUT = ") NET_PRINT_FLOAT(playerBD[PARTICIPANT_ID_TO_INT()].fShout) NET_NL()
						
						/*REMOVED FOR BUG 1613252
						//Display Intimidation Amount
						fIntimSyncSceneRate = (serverBD.fCurrentShout+0.60)	//0.75)
						IF fIntimSyncSceneRate < 0.75
							fIntimSyncSceneRate = 0.75
						ENDIF
						IF IS_BIT_SET(serverBD.iServerBitSet2, biS2_AnyCrewIsAimingAtStaff)
							fIntimSyncSceneRate += 0.25
						ENDIF
						IF fIntimSyncSceneRate > 1.75
							fIntimSyncSceneRate = 1.75
						ENDIF
						
						iIntimTarget = ROUND((fIntimSyncSceneRate-0.75)*50)
						IF iIntimCurrent < iIntimTarget
							iIntimCurrent += INTIM_INCREMENT
						ELIF iIntimCurrent > iIntimTarget
							iIntimCurrent -= INTIM_INCREMENT
						ENDIF
						IF iIntimCurrent <= 2
						AND iIntimTarget < 2
							iIntimCurrent = 0
						ENDIF
						
						DRAW_GENERIC_METER(iIntimCurrent, 50, "HUP_PROG", GET_TEAM_HUD_COLOUR(GET_PLAYER_TEAM(PLAYER_ID())))*/
						
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Makes sure Staff dialogue lines play at set times
PROC PROCESS_STAFF_DIALOGUE()

	//Dialogue based on sync scene
	IF serverBD.sMenuData.eStaffState != eSTAFF_FLEE_TO_BACK_ROOM
	AND serverBD.sMenuData.eStaffState != eSTAFF_ATTACK
	AND serverBD.sMenuData.eSnackBuyAnimState = SNACKBUY_DONE
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_Line4Done)
		OR NOT IS_BIT_SET(serverBD.iServerBitSet, biS_Line5Done)
		OR NOT IS_BIT_SET(serverBD.iServerBitSet, biS_ReactToShoutLineDone)
		OR NOT IS_BIT_SET(serverBD.iServerBitSet, biS_ReactToGunShotLineDone)
		OR NOT IS_BIT_SET(serverBD.iServerBitSet, biS_ReactToWantedLevelLineDone)
			iLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(serverBD.iSceneID)
			IF iLocalSceneID != -1
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSceneID)
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						
						NET_PRINT_TIME() NET_PRINT("[AM HOLD UP]      ---------->    phase = ")NET_PRINT_FLOAT(GET_SYNCHRONIZED_SCENE_PHASE(iLocalSceneID)) NET_NL()
						
						//I'm going as fast as I can
						IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_Line4Done)
						AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_Line4Done)
							//IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalSceneID) > 0.17
								CONTROL_DIALOGUE_LINE(2)		//STAFF - Okay, okay. I'm going as fast as I can.
								SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_Line4Done)
								NET_PRINT_TIME() NET_PRINT("[AM HOLD UP]      ---------->    iLocalSceneID = ")NET_PRINT_INT(iLocalSceneID) NET_NL()
								NET_PRINT_TIME() NET_PRINT("[AM HOLD UP]      ---------->    serverBD.iSceneID = ")NET_PRINT_INT(serverBD.iSceneID) NET_NL()
								NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - PROCESS_STAFF_DIALOGUE - LINE 4     <----------     ") NET_NL()
							//ENDIF
						ENDIF
				
						//Thats all of it I swear
						IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_Line5Done)
						AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_Line5Done)
							IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalSceneID) > 0.87
								CONTROL_DIALOGUE_LINE(3)		//STAFF - That's all of it, I swear.
								SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_Line5Done)
								NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - PROCESS_STAFF_DIALOGUE - LINE 5     <----------     ") NET_NL()
							ENDIF
						ENDIF
						
						//Reaction to Shout Dialogue
						IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_ReactToShoutLineDone)
						AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_ReactToShoutLineDone)
							IF IS_BIT_SET(serverBD.iServerBitSet, biS_ReactToShoutLineTrigger)
							AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								CONTROL_DIALOGUE_LINE(11)
								SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_ReactToShoutLineDone)
								NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - biP_ReactToShoutLineDone - TRUE    <----------     ") NET_NL()
							ENDIF
						ENDIF
						
						//Reaction to Gun Shot Dialogue
						IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_ReactToGunShotLineDone)
						AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_ReactToGunShotLineDone)
							IF IS_BIT_SET(serverBD.iServerBitSet, biS_ReactToGunShotLineTrigger)
							AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								CONTROL_DIALOGUE_LINE(12)
								SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_ReactToGunShotLineDone)
								NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - biP_ReactToGunShotLineDone - TRUE    <----------     ") NET_NL()
							ENDIF
						ENDIF
						
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_SyncSceneStarted)
			
			//Reaction to Gun Dialogue
			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_ReactToGunLineDone)
			AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_ReactToGunLineDone)
				IF IS_BIT_SET(serverBD.iServerBitSet, biS_StaffReactToWeaponDone)
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF NOT IS_BIT_SET(serverBD.iServerBitSet2, biS2_IsNewShift)
							IF NOT IS_BIT_SET(serverBD.iServerBitSet2, biS2_IsEndShift)
								CONTROL_DIALOGUE_LINE(1)
							ELSE
								CONTROL_DIALOGUE_LINE(19)
							ENDIF
						ELSE
							CONTROL_DIALOGUE_LINE(16)
						ENDIF
						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_ReactToGunLineDone)
						NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - biP_ReactToGunLineDone - TRUE    <----------     ") NET_NL()
						EXIT
					ELSE
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE - REACTION TO GUN   <----------     ") NET_NL()
						EXIT
					ENDIF
				ENDIF
			ENDIF
			
			//Reaction to Wanted Level Dialogue
			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_ReactToWantedLevelLineDone)
			AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_ReactToWantedLevelLineDone)
			AND NOT IS_BIT_SET(serverBD.iServerBitSet, biS_StaffReactToWeaponDone)
			AND NOT IS_BIT_SET(serverBD.iServerBitSet, biS_AnyCrewHasWeaponDrawn)
				IF IS_BIT_SET(serverBD.iServerBitSet2, biS2_AnyCrewWithWantedLevelInShop)
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF (IS_BIT_SET(serverBD.iServerBitSet, biS_StaffReactToWeaponDone) AND IS_BIT_SET(serverBD.iServerBitSet, biS_AnyCrewHasWeaponDrawn))
					OR (NOT IS_BIT_SET(serverBD.iServerBitSet, biS_StaffReactToWeaponDone) AND NOT IS_BIT_SET(serverBD.iServerBitSet, biS_AnyCrewHasWeaponDrawn))
						IF NOT IS_BIT_SET(serverBD.iServerBitSet2, biS2_IsNewShift)
							IF NOT IS_BIT_SET(serverBD.iServerBitSet2, biS2_IsEndShift)
								CONTROL_DIALOGUE_LINE(13)
							ELSE
								CONTROL_DIALOGUE_LINE(20)
							ENDIF
						ELSE
							CONTROL_DIALOGUE_LINE(17)
						ENDIF
						
						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_ReactToWantedLevelLineDone)
						NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - biP_ReactToWantedLevelLineDone - TRUE    <----------     ") NET_NL()
						EXIT
					ENDIF
				ENDIF
			ENDIF
			
			//Reaction to Unarmed Aim Dialogue
			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_ReactToUnarmedAimLineDone)
			AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_ReactToUnarmedAimLineDone)
				IF IS_BIT_SET(serverBD.iServerBitSet2, biS2_AnyCrewHasUnarmedAimedAtStaff)
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					CONTROL_DIALOGUE_LINE(14)
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_ReactToUnarmedAimLineDone)
					NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - biP_ReactToUnarmedAimLineDone - TRUE    <----------     ") NET_NL()
					EXIT
				ENDIF
			ENDIF
			
			//Reaction to snack buy
			IF NOT IS_BIT_SET(serverBD.iServerBitSet2, biS2_ReactToSnackLineDone)
			AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_ReactToSnackLineDone)
				IF IS_BIT_SET(serverBD.iServerBitSet2, biS2_SnackBuyCheck)
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					CONTROL_DIALOGUE_LINE(22)
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_ReactToSnackLineDone)
					NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - biP_ReactToSnackLineDone - TRUE    <----------     ") NET_NL()
					EXIT
				ENDIF
			ENDIF
			
			//Reaction to snack shoplift
			IF NOT IS_BIT_SET(serverBD.sMenuData.iServerSnackBitSet, biSS_ReactToLiftLineDone)
			AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerSnackBitSet, biPS_ReactToLiftDone)
				IF IS_BIT_SET(serverBD.sMenuData.iServerSnackBitSet, biSS_SnackShopLiftCheck)
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					CONTROL_DIALOGUE_LINE(24)
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerSnackBitSet, biPS_ReactToLiftDone)
					NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - biP_ReactToSnackLineDone - TRUE    <----------     ") NET_NL()
					EXIT
				ENDIF
			ENDIF
			
			//Reaction to Restricted Area
			IF NOT IS_BIT_SET(serverBD.iServerBitSet2, biS2_RestrictedAreaDone)
			AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_RestrictedAreaLineDone)
				IF IS_BIT_SET(serverBD.iServerBitSet2, biS2_RestrictedAreaCheck)
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					CONTROL_DIALOGUE_LINE(23)
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_RestrictedAreaLineDone)
					NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - biP_RestrictedAreaLineDone - TRUE    <----------     ") NET_NL()
					EXIT
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF
	
	
	//Reaction to Cop Arrival
	IF NOT IS_BIT_SET(serverBD.iServerBitSet2, biS2_ReactToCopArrival)
	AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_ReactToCopArrival)
		IF IS_BIT_SET(serverBD.iServerBitSet2, biS2_CopsArrived)
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				CONTROL_DIALOGUE_LINE(21)
				SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_ReactToCopArrival)
				NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - biP_ReactToCopArrival - TRUE    <----------     ") NET_NL()
				EXIT
			//ELSE
			//	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			//	EXIT
			ENDIF
		ENDIF
	ENDIF
ENDPROC


//PURPOSE: Returns TRUE if there is a bullet in the shop
FUNC BOOL BULLET_IN_SHOP()
	//Main Shop
	IF IS_BULLET_IN_ANGLED_AREA(vStoreMin, vStoreMax, fStoreWidth, FALSE)
		NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - BULLET_IN_SHOP - MAIN SHOP - TRUE    <----------     ") NET_NL()
		RETURN TRUE
	ENDIF
	
	//Back Room
	IF IS_BULLET_IN_ANGLED_AREA(vBackRoomMin, vBackRoomMax, fBackRoomWidth, FALSE)
		NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - BULLET_IN_SHOP - BACK ROOM - TRUE    <----------     ") NET_NL()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Returns TRUE if there is a dead ped in the shop
//FUNC BOOL DEAD_PED_IN_SHOP()
//	//Main Shop
//	IF DEAD
//		NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - BULLET_IN_SHOP - MAIN SHOP - TRUE    <----------     ") NET_NL()
//		RETURN TRUE
//	ENDIF
//	
//	//Back Room
//	IF DEAD
//		NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - BULLET_IN_SHOP - BACK ROOM - TRUE    <----------     ") NET_NL()
//		RETURN TRUE
//	ENDIF
//	
//	RETURN FALSE
//ENDFUNC

//PURPOSE: Returns TRUE if there is a explosion in the shop
FUNC BOOL EXPLOSION_IN_SHOP()
	IF shopSnacks.NearestHoldUpPoint >= eCRIM_HUP_GAS_1
	AND shopSnacks.NearestHoldUpPoint <= eCRIM_HUP_GAS_5
		//Large Area including outside (so we cover petrol pumps
		IF IS_EXPLOSION_IN_SPHERE(EXP_TAG_PETROL_PUMP, vStoreMin, 40)
			NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - EXPLOSION_IN_SHOP - LARGE AREA - TRUE    <----------     ") NET_NL()
			RETURN TRUE
		ENDIF
	ENDIF
	
	//Main Shop
	IF IS_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_DONTCARE, vStoreMin, vStoreMax, fStoreWidth)
		NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - EXPLOSION_IN_SHOP - MAIN SHOP - TRUE    <----------     ") NET_NL()
		RETURN TRUE
	ENDIF
	
	//Back Room
	IF IS_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_DONTCARE, vBackRoomMin, vBackRoomMax, fBackRoomWidth)
		NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - EXPLOSION_IN_SHOP - BACK ROOM - TRUE    <----------     ") NET_NL()
		RETURN TRUE
	ENDIF
		
	RETURN FALSE
ENDFUNC

//PURPOSE: Returns TRUE if there is a bullet or explosion in the shop
FUNC BOOL BULLET_OR_EXPLOSION_IN_SHOP()

	IF BULLET_IN_SHOP()
	OR EXPLOSION_IN_SHOP()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Retursn TRUE if there is a grenade in the Shop
FUNC BOOL GRENADE_IN_SHOP()
	IF IS_PROJECTILE_TYPE_IN_ANGLED_AREA(vStoreMin, vStoreMax, fStoreWidth, WEAPONTYPE_GRENADE, FALSE)
	OR IS_PROJECTILE_TYPE_IN_ANGLED_AREA(vStoreMin, vStoreMax, fStoreWidth, WEAPONTYPE_MOLOTOV, FALSE)
	OR IS_PROJECTILE_TYPE_IN_ANGLED_AREA(vStoreMin, vStoreMax, fStoreWidth, WEAPONTYPE_STICKYBOMB, FALSE)
		NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - GRENADE_IN_SHOP - TRUE    <----------     ") NET_NL()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Retursn TRUE if there is a grenade in the Back Room
FUNC BOOL GRENADE_IN_BACKROOM()
	IF IS_PROJECTILE_TYPE_IN_ANGLED_AREA(vBackRoomMin, vBackRoomMax, fBackRoomWidth, WEAPONTYPE_GRENADE, FALSE)
	OR IS_PROJECTILE_TYPE_IN_ANGLED_AREA(vBackRoomMin, vBackRoomMax, fBackRoomWidth, WEAPONTYPE_MOLOTOV, FALSE)
	OR IS_PROJECTILE_TYPE_IN_ANGLED_AREA(vBackRoomMin, vBackRoomMax, fBackRoomWidth, WEAPONTYPE_STICKYBOMB, FALSE)
		NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - GRENADE_IN_BACKROOM - TRUE    <----------     ") NET_NL()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Retursn TRUE if Satff should flee (grenade in shop, injureed, etc)
FUNC BOOL SHOULD_STAFF_FLEE(BOOL bDoCopInAreaCheck = FALSE)

	IF IS_FM_PLAYER_CRITICAL_TO_THIS_FM_EVENT(PLAYER_ID(), FMMC_TYPE_HUNT_THE_BEAST)
		NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - SHOULD_STAFF_FLEE - IS_FM_PLAYER_CRITICAL_TO_THIS_FM_EVENT(FMMC_TYPE_HUNT_THE_BEAST)    <----------     ") NET_NL()
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_AN_ANIMAL(PLAYER_ID())
		NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - SHOULD_STAFF_FLEE - IS_PLAYER_AN_ANIMAL    <----------     ") NET_NL()
		RETURN TRUE
	ENDIF

	IF IS_BIT_SET(serverBD.iServerBitSet, biS_AnyCrewHasDamagedStaff)
		NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - SHOULD_STAFF_FLEE - biS_AnyCrewHasDamagedStaff    <----------     ") NET_NL()
		RETURN TRUE
	ENDIF
	
	IF IS_PED_RAGDOLL(NET_TO_PED(serverBD.sMenuData.niRegStaff))
		NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - SHOULD_STAFF_FLEE - STAFF RAGDOLL    <----------     ") NET_NL()
		RETURN TRUE
	ENDIF
	
	IF GRENADE_IN_SHOP()
	OR GRENADE_IN_BACKROOM()
		NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - SHOULD_STAFF_FLEE - GRENADE_IN_SHOP    <----------     ") NET_NL()
		RETURN TRUE
	ENDIF
	
	IF EXPLOSION_IN_SHOP()
		NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - SHOULD_STAFF_FLEE - EXPLOSION_IN_SHOP    <----------     ") NET_NL()
		RETURN TRUE
	ENDIF

	INT iCount
	STRUCT_ENTITY_DAMAGE_EVENT sei
	
	REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iCount
		IF GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iCount) = EVENT_NETWORK_DAMAGE_ENTITY
			IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sei, SIZE_OF(sei))
			
				IF DOES_ENTITY_EXIST(sei.VictimIndex)
				AND IS_ENTITY_A_PED(sei.VictimIndex)
				
					// Victim died in the shop
					PED_INDEX pedVictim = GET_PED_INDEX_FROM_ENTITY_INDEX(sei.VictimIndex)
					IF IS_PED_INJURED(pedVictim)
					AND GET_INTERIOR_FROM_ENTITY(pedVictim) = interiorID
					
						IF DOES_ENTITY_EXIST(sei.DamagerIndex)
						AND IS_ENTITY_A_PED(sei.DamagerIndex)
						
							// It wasn't the shop keeper who killed the ped (don't want him to flee his own kill)
							PED_INDEX pedKiller = GET_PED_INDEX_FROM_ENTITY_INDEX(sei.DamagerIndex)
							IF pedKiller != NET_TO_PED(serverBD.sMenuData.niRegStaff)
								NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - SHOULD_STAFF_FLEE - PED KILLED IN SHOP    <----------     ") NET_NL()
								RETURN TRUE		
							ENDIF
							
						ENDIF
						
					ENDIF
					
				ENDIF
				
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF bDoCopInAreaCheck = TRUE
		IF NOT IS_BIT_SET(serverBD.iServerBitSet2, biS2_CopsArrived)
			IF IS_COP_PED_IN_AREA_3D(vStoreMin-<<25, 25, 25>>, vStoreMin+<<25, 25, 25>>)
				NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - SHOULD_STAFF_FLEE - COP IN AREA    <----------     ") NET_NL()
				SET_BIT(serverBD.iServerBitSet2, biS2_CopsArrived)
				//RETURN TRUE - ONLY DO DIALOGUE DON'T FLEE ANYMORE - BUG 1228006
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Returns TRUE if the player has fired a valid weapon that is empty
FUNC BOOL HAS_EMPTY_WEAPON_BEEN_SHOT()
		
	IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_ATTACK)
	OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_ATTACK2)
		WEAPON_TYPE PlayerWeapon = GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID())
		
		IF PlayerWeapon != WEAPONTYPE_UNARMED
		AND PlayerWeapon != WEAPONTYPE_INVALID
		AND PlayerWeapon != WEAPONTYPE_VEHICLE_PLAYER_BULLET
		AND PlayerWeapon != WEAPONTYPE_VEHICLE_PLAYER_BUZZARD
		AND PlayerWeapon != WEAPONTYPE_VEHICLE_PLAYER_LASER
		AND PlayerWeapon != WEAPONTYPE_VEHICLE_WEAPON_TANK
		AND PlayerWeapon != WEAPONTYPE_FLARE
			WEAPON_GROUP PlayerWeaponGroup = GET_WEAPONTYPE_GROUP(PlayerWeapon)
		
			IF GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), PlayerWeapon) = 0
			AND PlayerWeaponGroup != WEAPONGROUP_MELEE
			AND PlayerWeaponGroup != WEAPONGROUP_LOUDHAILER
			AND PlayerWeaponGroup != WEAPONGROUP_INVALID
			AND PlayerWeaponGroup != WEAPONGROUP_FIREEXTINGUISHER
			AND PlayerWeaponGroup != WEAPONGROUP_NIGHTVISION
			AND PlayerWeaponGroup != WEAPONGROUP_PARACHUTE
			AND PlayerWeaponGroup != WEAPONGROUP_DIGISCANNER
				
//				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vStoreMin, vStoreMax, fStoreWidth)
//				//OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vBackRoomMin, vBackRoomMax, fBackRoomWidth)
//				//OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vMin, <<HOLD_UP_ENTRANCE_RANGE, HOLD_UP_ENTRANCE_RANGE, HOLD_UP_ENTRANCE_RANGE>>)
//				OR IN_EXTRA_MAIN_AREA(PLAYER_PED_ID(), shopSnacks.NearestHoldUpPoint)

				IF GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = interiorID
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Controls how the CUrrent Shout value decreases over time
PROC CONTROL_CURRENT_SHOUT_DECREASE()
	IF serverBD.fCurrentShout > 0.0
		IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(serverBD.iShoutDecreaseTimer, SHOUT_DECREASE_DELAY)	//serverBD.iShoutDecreaseTimer < GET_THE_NETWORK_TIMER()
			serverBD.fCurrentShout -= SHOUT_DECREASE_AMOUNT //0.2
			IF serverBD.fCurrentShout < 0.0
				serverBD.fCurrentShout = 0.0
			ENDIF
			RESET_NET_TIMER(serverBD.iShoutDecreaseTimer)	//serverBD.iShoutDecreaseTimer = (GET_THE_NETWORK_TIMER()+SHOUT_DECREASE_DELAY)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Make the Staff do whatever the server thinks it should be doing
FUNC BOOL PROCESS_STAFF_BODY()
	
	//Check if I have control of the Guard
	IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sMenuData.niRegStaff)
	//AND (IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.sMenuData.niRegStaff) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
		RETURN FALSE
	ENDIF
	
	IF IS_NET_PED_INJURED(serverBD.sMenuData.niRegStaff)
		IF NOT IS_BIT_SET(shopSnacks.iBoolsBitSet2, bi2ClearFacialOverride)
			CLEAR_FACIAL_IDLE_ANIM_OVERRIDE(NET_TO_PED(serverBD.sMenuData.niRegStaff))
			SET_BIT(shopSnacks.iBoolsBitSet2, bi2ClearFacialOverride)
			NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - PROCESS_STAFF_BODY - INJURED - CLEAR_FACIAL_IDLE_ANIM_OVERRIDE    <----------     ") NET_NL()
		ENDIF
		RETURN FALSE
	ENDIF
	
	//Request Cash Bag
	//IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niCashBag)
	//	IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niCashBag)
	//		NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.niCashBag)
	//	ENDIF
	//ENDIF
	
	PLAYER_INDEX PlayerId
	PED_INDEX PlayerPedId
	
	SWITCH serverBD.sMenuData.eStaffState
		CASE eSTAFF_CREATED
			//Do Nothing
			SET_PED_RESET_FLAG(NET_TO_PED(serverBD.sMenuData.niRegStaff), PRF_UseKinematicPhysics, TRUE)
		BREAK
									
		CASE eSTAFF_WAIT
			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_AnyCrewHasWeaponDrawn)
			AND NOT IS_BIT_SET(serverBD.iServerBitSet, biS_AnyCrewHasWeaponDrawnNearStaff)
				IF IS_BIT_SET(serverBD.iServerBitSet, biS_AnyCrewInShop)
					IF NOT IS_BIT_SET(shopSnacks.iBoolsBitSet, biCanIHelpDialogue)
						SET_RAGDOLL_BLOCKING_FLAGS(NET_TO_PED(serverBD.sMenuData.niRegStaff), RBF_PLAYER_IMPACT)
						TASK_LOOK_AT_ENTITY(NET_TO_PED(serverBD.sMenuData.niRegStaff), PLAYER_PED_ID(), -1, SLF_DEFAULT)
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							CONTROL_DIALOGUE_LINE(10)
						ELSE
							IF IS_BIT_SET(serverBD.iServerBitSet2, biS2_AnyCrewWithWantedLevelInShop)
								IF NOT IS_BIT_SET(serverBD.iServerBitSet2, biS2_IsNewShift)
									IF NOT IS_BIT_SET(serverBD.iServerBitSet2, biS2_IsEndShift)
										CONTROL_DIALOGUE_LINE(13)
									ELSE
										CONTROL_DIALOGUE_LINE(20)
									ENDIF
								ELSE
									CONTROL_DIALOGUE_LINE(17)
								ENDIF
								SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_ReactToWantedLevelLineDone)
								NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - eSTAFF_WAIT - biP_ReactToWantedLevelLineDone - TRUE    <----------     ") NET_NL()
							ELSE
								IF NOT IS_BIT_SET(serverBD.iServerBitSet2, biS2_IsNewShift)
									IF NOT IS_BIT_SET(serverBD.iServerBitSet2, biS2_IsEndShift)
										CONTROL_DIALOGUE_LINE(0)
									ELSE
										CONTROL_DIALOGUE_LINE(18)
									ENDIF
								ELSE
									CONTROL_DIALOGUE_LINE(15)
								ENDIF
							ENDIF
						ENDIF
						SET_BIT(shopSnacks.iBoolsBitSet, biCanIHelpDialogue)
					ELSE
						//Turn to Face Player
						IF GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.sMenuData.niRegStaff), SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY) != PERFORMING_TASK
        				AND GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.sMenuData.niRegStaff), SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY) != WAITING_TO_START_TASK
							IF HAS_NET_TIMER_EXPIRED(iTurnTimer, 5000)
								RESET_NET_TIMER(iTurnTimer)
								IF NOT bSnackHeadingReached // this gets set just as a snack anim is about to start, then cleared out after it's done
									TASK_TURN_PED_TO_FACE_ENTITY(NET_TO_PED(serverBD.sMenuData.niRegStaff), PLAYER_PED_ID(), 10000)
								ENDIF
								TASK_LOOK_AT_ENTITY(NET_TO_PED(serverBD.sMenuData.niRegStaff), PLAYER_PED_ID(), -1, SLF_DEFAULT)
								NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - MAKE STAFF TURN TO FACE PLAYER ")  NET_NL()
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			SET_PED_RESET_FLAG(NET_TO_PED(serverBD.sMenuData.niRegStaff), PRF_UseKinematicPhysics, TRUE)
		BREAK
	
		CASE eSTAFF_REACT_TO_WEAPON
			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_StaffReactToWeaponDone)
			AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_StaffReactToWeaponDone)
				PlayerPedId = PLAYER_PED_ID()
				IF serverBD.iAggressivePlayer > -1
					IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(serverBD.iAggressivePlayer))
						PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(serverBD.iAggressivePlayer))
						PlayerPedId = GET_PLAYER_PED(PlayerId)
					ENDIF
				ENDIF
				
				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					TASK_LOOK_AT_ENTITY(NET_TO_PED(serverBD.sMenuData.niRegStaff), PlayerPedId, -1, SLF_DEFAULT)
					NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - eSTAFF_REACT_TO_WEAPON - MAKE STAFF LOOK AT ") NET_PRINT(GET_PLAYER_NAME(PlayerId))  NET_NL()
					
					SET_RAGDOLL_BLOCKING_FLAGS(NET_TO_PED(serverBD.sMenuData.niRegStaff), RBF_PLAYER_IMPACT)
					//KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					//CONTROL_DIALOGUE_LINE(1)		//STAFF - Please Sir, I don't want any trouble.
					TASK_PLAY_ANIM(NET_TO_PED(serverBD.sMenuData.niRegStaff), "mp_am_hold_up", "WARY_LOOP", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING|AF_UPPERBODY|AF_SECONDARY)
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_StaffReactToWeaponDone)
				ENDIF
			//ENDIF
			//Turn to Face Player
			ELIF GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.sMenuData.niRegStaff), SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY) != PERFORMING_TASK
			AND GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.sMenuData.niRegStaff), SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY) != WAITING_TO_START_TASK
				IF HAS_NET_TIMER_EXPIRED(iTurnTimer, 5000)
					PlayerPedId = PLAYER_PED_ID()
					IF serverBD.iAggressivePlayer > -1
						IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(serverBD.iAggressivePlayer))
							PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(serverBD.iAggressivePlayer))
							PlayerPedId = GET_PLAYER_PED(PlayerId)
						ENDIF
					ENDIF
					RESET_NET_TIMER(iTurnTimer)
					TASK_TURN_PED_TO_FACE_ENTITY(NET_TO_PED(serverBD.sMenuData.niRegStaff), PlayerPedId, 10000)
					TASK_LOOK_AT_ENTITY(NET_TO_PED(serverBD.sMenuData.niRegStaff), PlayerPedId, -1, SLF_DEFAULT)
					NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - MAKE STAFF TURN TO FACE PLAYER - B")  NET_NL()
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(serverBD.iServerBitSet, biS_AnyCrewHasAimedAtStaff)
			//	//IF NOT IS_ENTITY_PLAYING_ANIM(NET_TO_PED(serverBD.sMenuData.niRegStaff), "mp_am_hold_up", "guard_handsup_loop")
			//	IF GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.sMenuData.niRegStaff), SCRIPT_TASK_PERFORM_SEQUENCE) != WAITING_TO_START_TASK
			//	AND GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.sMenuData.niRegStaff), SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
			//		//TASK_PERFORM_SEQUENCE(NET_TO_PED(serverBD.sMenuData.niRegStaff), StaffHandsUpSeq)
					SET_RAGDOLL_BLOCKING_FLAGS(NET_TO_PED(serverBD.sMenuData.niRegStaff), RBF_PLAYER_IMPACT)
			//	ENDIF
			ENDIF
			SET_PED_RESET_FLAG(NET_TO_PED(serverBD.sMenuData.niRegStaff), PRF_UseKinematicPhysics, TRUE)
		BREAK
		
		CASE eSTAFF_GET_INTO_POSITION
			IF NOT IS_BIT_SET(shopSnacks.iBoolsBitSet, biAchieveHeading)
				IF GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.sMenuData.niRegStaff), SCRIPT_TASK_ACHIEVE_HEADING) != WAITING_TO_START_TASK
				AND GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.sMenuData.niRegStaff), SCRIPT_TASK_ACHIEVE_HEADING) != PERFORMING_TASK
					SET_PED_KEEP_TASK(NET_TO_PED(serverBD.sMenuData.niRegStaff), TRUE)
					SET_PED_FLEE_ATTRIBUTES(NET_TO_PED(serverBD.sMenuData.niRegStaff), FA_USE_VEHICLE, FALSE)
					SET_PED_FLEE_ATTRIBUTES(NET_TO_PED(serverBD.sMenuData.niRegStaff), FA_PREFER_PAVEMENTS, TRUE)
					//SET_PED_FLEE_ATTRIBUTES(NET_TO_PED(serverBD.sMenuData.niRegStaff), FA_DISABLE_HANDS_UP, TRUE)
					SET_PED_FLEE_ATTRIBUTES(NET_TO_PED(serverBD.sMenuData.niRegStaff), FA_WANDER_AT_END, TRUE)
					
	//				//KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
	//				//NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - eSTAFF_GET_INTO_POSITION - KILL CONVERSATION     <----------     ") NET_NL()
	//				TEXT_LABEL_15 tlRootLabel
	//				tlRootLabel = "RB_12_L0"
	//				TEXT_LABEL_15 tlPlaying
	//				tlPlaying = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()
	//				IF ARE_STRINGS_EQUAL(tlPlaying, tlRootLabel)
						//KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						//NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - eSTAFF_GET_INTO_POSITION - KILL CONVERSATION     <----------     ") NET_NL()
	//				ENDIF
					
					//iDelayEndTimer = (GET_THE_NETWORK_TIMER()+5000)
					
					SET_RAGDOLL_BLOCKING_FLAGS(NET_TO_PED(serverBD.sMenuData.niRegStaff), RBF_PLAYER_IMPACT)
					//TASK_FOLLOW_NAV_MESH_TO_COORD(NET_TO_PED(serverBD.sMenuData.niRegStaff), vRegStaff, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_ADV_SLIDE_TO_COORD_AND_ACHIEVE_HEADING_AT_END, sceneRotation.z)
					TASK_ACHIEVE_HEADING(NET_TO_PED(serverBD.sMenuData.niRegStaff), serverBD.fRegStaffHeading, -1)
					NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - STAFF GIVEN TASK - TASK_ACHIEVE_HEADING     <----------     ") NET_NL()
					SET_BIT(shopSnacks.iBoolsBitSet, biAchieveHeading)
				ENDIF
			ENDIF
		BREAK
		
		CASE eSTAFF_EMPTY_REGISTER
			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_SyncSceneStartedRunning)	//biS_SyncSceneStarted)
			AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_SyncSceneStarted)
				
				//STRING sLabel
				//sLabel = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()
//				TEXT_LABEL_15 tlRootLabel
//				tlRootLabel = "RB_12_L0"
//				TEXT_LABEL_15 tlPlaying
//				tlPlaying = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()
//				IF ARE_STRINGS_EQUAL(tlPlaying, tlRootLabel)
					//KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					//NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - eSTAFF_EMPTY_REGISTER - KILL CONVERSATION     <----------     ") NET_NL()
//				ENDIF

				IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
					MPGlobalsAmbience.bHoldUpTriggeredEmptyReg = TRUE
					NET_PRINT_TIME() NET_PRINT_STRINGS("[AM HOLD UP] - HOST - SET bHoldUpTriggeredEmptyReg = TRUE - A    <----------     ", GET_MP_MISSION_NAME(shopSnacks.thisHoldupMission)) NET_NL()
				ENDIF
				
				//TASK_CLEAR_LOOK_AT(NET_TO_PED(serverBD.sMenuData.niRegStaff))
				PED_INDEX PedId
				PedId = PLAYER_PED_ID()
				IF MPGlobalsAmbience.piHoldUpTriggerPlayer != INVALID_PLAYER_INDEX()
					IF IS_NET_PLAYER_OK(MPGlobalsAmbience.piHoldUpTriggerPlayer)
						PedId = GET_PLAYER_PED(MPGlobalsAmbience.piHoldUpTriggerPlayer)
					ENDIF
				ENDIF
				TASK_LOOK_AT_ENTITY(NET_TO_PED(serverBD.sMenuData.niRegStaff), PedId, 30000, SLF_DEFAULT, SLF_LOOKAT_HIGH)
				//SET_IK_TARGET(NET_TO_PED(serverBD.sMenuData.niRegStaff), IK_PART_HEAD, PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_HEAD), <<0, 0, 0>>, ITF_DEFAULT)
				
				SET_FACIAL_IDLE_ANIM_OVERRIDE(NET_TO_PED(serverBD.sMenuData.niRegStaff), "mood_stressed_1")
				NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - SET_FACIAL_IDLE_ANIM_OVERRIDE - mood_stressed_1    <----------     ") NET_NL()
					
			//	BROADCAST_SET_HOLD_UP_DONE(ENUM_TO_INT(shopSnacks.NearestHoldUpPoint), TRUE, GET_HOLD_UP_HOST()) //SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iHoldUpDoneBitSet, ENUM_TO_INT(shopSnacks.NearestHoldUpPoint))
			//	NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - iHoldUpDoneBitSet - SET - A - HOLD UP = ") NET_PRINT_INT(ENUM_TO_INT(shopSnacks.NearestHoldUpPoint)) NET_NL()
				
				//CONTROL_DIALOGUE_LINE(2)	//STAFF - Okay, okay. I'm going as fast as I can.
				SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_SyncSceneStarted)
				NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - STAFF GIVEN TASK - NETWORK_START_SYNCHRONISED_SCENE     <----------     ") NET_NL()

			//Control Sync Scene Speed based on Current Shout
			ELSE
				iLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(serverBD.iSceneID)
				IF iLocalSceneID != -1
					IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSceneID)
						
						//SET_IK_TARGET(NET_TO_PED(serverBD.sMenuData.niRegStaff), IK_PART_HEAD, PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_HEAD), <<0, 0, 0>>, ITF_DEFAULT)
						//NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - SET_IK_TARGET - ITF_DEFAULT    <----------     ") NET_NL()
						
						fSyncSceneRate = (serverBD.fCurrentShout+0.60)	//0.75)						
						//IF fSyncSceneRate > 1.75
						//	fSyncSceneRate = 1.75
						//Minimum is higher if ped is being targetted
						//ELIF IS_BIT_SET(serverBD.iServerBitSet2, biS2_AnyCrewIsAimingAtStaff)
						//	IF fSyncSceneRate < 1.0
						//		fSyncSceneRate = 1.0
						//	ENDIF
						//EL
						IF fSyncSceneRate < 0.75
							fSyncSceneRate = 0.75
						ENDIF
						IF IS_BIT_SET(serverBD.iServerBitSet2, biS2_AnyCrewIsAimingAtStaff)
							fSyncSceneRate += 0.25
						ENDIF
						IF fSyncSceneRate > 1.75
							fSyncSceneRate = 1.75
						ENDIF
						SET_SYNCHRONIZED_SCENE_RATE(iLocalSceneID, fSyncSceneRate)
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE eSTAFF_HANDS_UP
			IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_StaffHandsUpLoop)
				IF GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.sMenuData.niRegStaff), SCRIPT_TASK_PERFORM_SEQUENCE) != WAITING_TO_START_TASK
				AND GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.sMenuData.niRegStaff), SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
					IF NOT IS_PED_RAGDOLL(NET_TO_PED(serverBD.sMenuData.niRegStaff))
						iLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(serverBD.iSceneID)
						IF (iLocalSceneID != -1 AND NOT IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSceneID))
						OR iLocalSceneID = -1
							//CLEAR_PED_TASKS(NET_TO_PED(serverBD.sMenuData.niRegStaff))
							TASK_PERFORM_SEQUENCE(NET_TO_PED(serverBD.sMenuData.niRegStaff), StaffHandsUpSeq)
							SET_RAGDOLL_BLOCKING_FLAGS(NET_TO_PED(serverBD.sMenuData.niRegStaff), RBF_PLAYER_IMPACT)
							SET_PED_KEEP_TASK(NET_TO_PED(serverBD.sMenuData.niRegStaff), TRUE)
							SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_StaffHandsUpLoop)
							NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - STAFF GIVEN TASK - SEQUENCE - StaffHandsUpSeq     <----------     ") NET_NL()
						ENDIF
					ENDIF
				ENDIF
			//Turn to Face Player
			ELIF GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.sMenuData.niRegStaff), SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY) != PERFORMING_TASK
			AND GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.sMenuData.niRegStaff), SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY) != WAITING_TO_START_TASK
				IF HAS_NET_TIMER_EXPIRED(iTurnTimer, 5000)
					RESET_NET_TIMER(iTurnTimer)
					PlayerPedId = PLAYER_PED_ID()
					IF serverBD.iAggressivePlayer > -1
						IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(serverBD.iAggressivePlayer))
							PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(serverBD.iAggressivePlayer))
							PlayerPedId = GET_PLAYER_PED(PlayerId)
						ENDIF
					ENDIF
					TASK_TURN_PED_TO_FACE_ENTITY(NET_TO_PED(serverBD.sMenuData.niRegStaff), PlayerPedId, 10000)
					TASK_LOOK_AT_ENTITY(NET_TO_PED(serverBD.sMenuData.niRegStaff), PLAYER_PED_ID(), -1, SLF_DEFAULT)
					NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - MAKE STAFF TURN TO FACE PLAYER - B")  NET_NL()
				ENDIF
			ENDIF
		BREAK
		
		CASE eSTAFF_HANDS_UP_LOOP_ONLY
			IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_StaffHandsUpLoop)		
				
				//IF GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.sMenuData.niRegStaff), SCRIPT_TASK_PERFORM_SEQUENCE) != WAITING_TO_START_TASK
				//AND GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.sMenuData.niRegStaff), SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
					//CLEAR_PED_TASKS(NET_TO_PED(serverBD.sMenuData.niRegStaff))
					//STOP_SYNC_SCENE()
					//iLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(serverBD.iSceneID)
					//IF (iLocalSceneID >= 0 AND NOT IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSceneID))
					//OR iLocalSceneID < 0
					
					IF iLocalSceneID = -1
					OR NOT IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSceneID)
					OR GET_SYNCHRONIZED_SCENE_PHASE(iLocalSceneID) >= 0.95
					
						STOP_SYNC_SCENE()
						
						//RESET_NET_TIMER(iTurnTimer)
						//TASK_PERFORM_SEQUENCE(NET_TO_PED(serverBD.sMenuData.niRegStaff), StaffHandsUpLoopSeq)
						//TASK_PLAY_ANIM(NET_TO_PED(serverBD.sMenuData.niRegStaff), "mp_am_hold_up", "handsup_base", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_UPPERBODY | AF_SECONDARY)
						//TASK_PLAY_ANIM(NET_TO_PED(serverBD.sMenuData.niRegStaff), "mp_am_hold_up", "handsup_base", INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_UPPERBODY | AF_SECONDARY)
						
						PlayerPedId = PLAYER_PED_ID()
						IF serverBD.iAggressivePlayer > -1
							IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(serverBD.iAggressivePlayer))
								PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(serverBD.iAggressivePlayer))
								PlayerPedId = GET_PLAYER_PED(PlayerId)
							ENDIF
						ENDIF
						
						TASK_HANDS_UP(NET_TO_PED(serverBD.sMenuData.niRegStaff), -1, PlayerPedId, -1, HANDS_UP_STRAIGHT_TO_LOOP)
						
						FORCE_PED_AI_AND_ANIMATION_UPDATE(NET_TO_PED(serverBD.sMenuData.niRegStaff))
						
						SET_RAGDOLL_BLOCKING_FLAGS(NET_TO_PED(serverBD.sMenuData.niRegStaff), RBF_PLAYER_IMPACT)
						SET_PED_KEEP_TASK(NET_TO_PED(serverBD.sMenuData.niRegStaff), TRUE)
						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_StaffHandsUpLoop)
						NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - STAFF GIVEN TASK - SEQUENCE - StaffHandsUpLoopSeq dd    <----------     ") NET_NL()
					ENDIF
				//ENDIF
				
			ENDIF
			//Turn to Face Player
			/*ELIF GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.sMenuData.niRegStaff), SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY) != PERFORMING_TASK
			AND GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.sMenuData.niRegStaff), SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY) != WAITING_TO_START_TASK
				IF HAS_NET_TIMER_EXPIRED(iTurnTimer, 5000)
					RESET_NET_TIMER(iTurnTimer)
					TASK_TURN_PED_TO_FACE_ENTITY(NET_TO_PED(serverBD.sMenuData.niRegStaff), PLAYER_PED_ID(), 10000)
					NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - MAKE STAFF TURN TO FACE PLAYER - C")  NET_NL()
				ENDIF
			ENDIF*/
		BREAK
		
		CASE eSTAFF_ATTACK
			IF GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.sMenuData.niRegStaff), SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED) != WAITING_TO_START_TASK
			AND GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.sMenuData.niRegStaff), SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED) != PERFORMING_TASK
			//IF NOT IS_PED_IN_COMBAT(NET_TO_PED(serverBD.sMenuData.niRegStaff))
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					SETUP_STAFF_TO_ATTACK()
					
					//TASK_PERFORM_SEQUENCE(NET_TO_PED(serverBD.sMenuData.niRegStaff), StaffAttackSeq)
					CLEAR_PED_TASKS(NET_TO_PED(serverBD.sMenuData.niRegStaff))
					TASK_COMBAT_HATED_TARGETS_AROUND_PED(NET_TO_PED(serverBD.sMenuData.niRegStaff), 20)
					CONTROL_DIALOGUE_LINE(4)		//STAFF - Get the fuck out of my store asshole!
					
					NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - STAFF GIVEN TASK - ATTACK     <----------     ") NET_NL()
				ENDIF
			ENDIF
		BREAK
		
		CASE eSTAFF_FLEE_TO_BACK_ROOM
			IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_StaffFleeToBackRoom)				
				STOP_SYNC_SCENE()
				
				iLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(serverBD.iSceneID)
				IF (iLocalSceneID != -1 AND NOT IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSceneID))
				OR iLocalSceneID = -1
					BOOL bTaskDone
					
					IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_AnyCrewInBackRoom)
					AND NOT GRENADE_IN_SHOP()
					AND NOT GRENADE_IN_BACKROOM()
						IF IS_BIT_SET(serverBD.iServerBitSet2, biS2_DoStaffAttackCheck)
							IF NOT IS_BIT_SET(serverBD.iServerBitSet2, biS2_MakeStaffAttack)
								//CLEAR_PED_SECONDARY_TASK(NET_TO_PED(serverBD.sMenuData.niRegStaff))
								TASK_PERFORM_SEQUENCE(NET_TO_PED(serverBD.sMenuData.niRegStaff), StaffFleeToBackRoomSeq)
								bTaskDone = TRUE
								NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - STAFF GIVEN TASK - FLEE TO BACK ROOM     <----------     ") NET_NL()
							ELSE
								//CLEAR_PED_SECONDARY_TASK(NET_TO_PED(serverBD.sMenuData.niRegStaff))
								SETUP_STAFF_TO_ATTACK()
								TASK_PERFORM_SEQUENCE(NET_TO_PED(serverBD.sMenuData.niRegStaff), StaffFleeToBackRoomAttackSeq)
								bTaskDone = TRUE
								NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - STAFF GIVEN TASK - FLEE TO BACK ROOM ATTACK     <----------     ") NET_NL()
							ENDIF
						ELSE
							NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - eSTAFF_FLEE_TO_BACK_ROOM - biS2_DoStaffAttackCheck NOT SET     <----------     ") NET_NL()
						ENDIF
					ELSE
						//CLEAR_PED_SECONDARY_TASK(NET_TO_PED(serverBD.sMenuData.niRegStaff))
						TASK_SMART_FLEE_COORD(NET_TO_PED(serverBD.sMenuData.niRegStaff), vBackRoomCower, 250, -1, TRUE)
						bTaskDone = TRUE
						NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - STAFF GIVEN TASK - FLEE FROM BACK ROOM     <----------     ") NET_NL()
					ENDIF
					
					IF bTaskDone = TRUE
						//SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NET_TO_PED(serverBD.sMenuData.niRegStaff), FALSE)
						CONTROL_DIALOGUE_LINE(25)
						SET_PED_KEEP_TASK(NET_TO_PED(serverBD.sMenuData.niRegStaff), TRUE)
						BROADCAST_SET_HOLD_UP_DONE(ENUM_TO_INT(shopSnacks.NearestHoldUpPoint), TRUE, GET_HOLD_UP_HOST())
						NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - iHoldUpDoneBitSet - SET - E - HOLD UP = ") NET_PRINT_INT(ENUM_TO_INT(shopSnacks.NearestHoldUpPoint)) NET_NL()
						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_StaffFleeToBackRoom)
					ENDIF
				ENDIF
			
			//Check they were able to perform the Task
			ELSE
				IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_FleeRouteOkayCheck)	
					NAVMESH_ROUTE_RESULT eResult
					eResult = GET_NAVMESH_ROUTE_RESULT(NET_TO_PED(serverBD.sMenuData.niRegStaff))
					IF eResult = NAVMESHROUTE_ROUTE_FOUND
						CLEAR_PED_SECONDARY_TASK(NET_TO_PED(serverBD.sMenuData.niRegStaff))
						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_FleeRouteOkayCheck)
						NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - NAVMESHROUTE_ROUTE_FOUND - biP_FleeRouteOkayCheck     <----------     ") NET_NL()	
					ELIF eResult = NAVMESHROUTE_ROUTE_NOT_FOUND
						CLEAR_PED_TASKS(NET_TO_PED(serverBD.sMenuData.niRegStaff))
						IF NOT IS_BIT_SET(serverBD.iServerBitSet2, biS2_MakeStaffAttack)
							TASK_PERFORM_SEQUENCE(NET_TO_PED(serverBD.sMenuData.niRegStaff), StaffCowerInPlace)
							NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - STAFF GIVEN TASK - COWER IN PLACE    <----------     ") NET_NL()
						ELSE
							TASK_COMBAT_HATED_TARGETS_AROUND_PED(NET_TO_PED(serverBD.sMenuData.niRegStaff), 20)
							NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - STAFF GIVEN TASK - ATTACK    <----------     ") NET_NL()
						ENDIF
						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_FleeRouteOkayCheck)
						NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - NAVMESHROUTE_ROUTE_NOT_FOUND - biP_FleeRouteOkayCheck     <----------     ") NET_NL()	
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	PROCESS_STAFF_DIALOGUE()
	
	RETURN TRUE
ENDFUNC


/// PURPOSE: Work out what the Staff should be doing
FUNC BOOL PROCESS_STAFF_BRAIN()
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		RETURN FALSE
	ENDIF
	
	CONTROL_CURRENT_SHOUT_DECREASE()
	
	IF serverBD.sMenuData.eStaffState != eSTAFF_CLEANUP
		
		IF IS_NET_PED_INJURED(serverBD.sMenuData.niRegStaff)
			serverBD.sMenuData.eStaffState = eSTAFF_CLEANUP
			RETURN FALSE
		ENDIF
		
		//Fail Safe Flee if objects are missing
		IF IS_BIT_SET(serverBD.iServerBitSet, biS_AnyCrewHasDemandedCash)
		AND NOT IS_BIT_SET(serverBD.iServerBitSet, biS_SyncSceneStarted)	
		AND serverBD.sMenuData.eStaffState < eSTAFF_FLEE_TO_BACK_ROOM
			IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niCashBag)
			OR NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niTill)
				serverBD.sMenuData.eStaffState = eSTAFF_FLEE_TO_BACK_ROOM
				NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - serverBD.sMenuData.eStaffState = eSTAFF_FLEE_TO_BACK_ROOM - Q") NET_NL()
				#IF IS_DEBUG_BUILD NET_LOG_Host("eStaffState = eSTAFF_FLEE_TO_BACK_ROOM - Q")	#ENDIF
			ENDIF
		ENDIF
		
		SWITCH serverBD.sMenuData.eStaffState
			
			CASE eSTAFF_CREATED
				// Wait until I'm in the vehicle
				IF NOT IS_NET_PED_INJURED(serverBD.sMenuData.niRegStaff)
					serverBD.sMenuData.eStaffState = eSTAFF_WAIT
					NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - serverBD.sMenuData.eStaffState = eSTAFF_WAIT") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG_Host("eStaffState = eSTAFF_WAIT")	#ENDIF
				ENDIF
			BREAK
			
			CASE eSTAFF_WAIT
				IF IS_BIT_SET(serverBD.iServerBitSet, biS_AnyCrewHasWeaponDrawn)
					//IF serverBD.iReaction  <= iRegStaffAttackChance
					//	serverBD.sMenuData.eStaffState = eSTAFF_ATTACK
					//	NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - serverBD.sMenuData.eStaffState = eSTAFF_ATTACK 1") NET_NL()
					//ELSE
						serverBD.sMenuData.eStaffState = eSTAFF_REACT_TO_WEAPON
						NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - serverBD.sMenuData.eStaffState = eSTAFF_REACT_TO_WEAPON") NET_NL()
						#IF IS_DEBUG_BUILD NET_LOG_Host("eStaffState = eSTAFF_REACT_TO_WEAPON")	#ENDIF
					//ENDIF
				//REMOVED FOR BUG 545547
				/*ELIF IS_BIT_SET(serverBD.iServerBitSet, biS_AnyCrewHasDamagedStaff)
					//IF serverBD.iReaction  <= iRegStaffAttackChance
					//	serverBD.sMenuData.eStaffState = eSTAFF_ATTACK
					//	NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - serverBD.sMenuData.eStaffState = eSTAFF_ATTACK 2") NET_NL()
					//ELSE
						serverBD.sMenuData.eStaffState = eSTAFF_GET_INTO_POSITION
						NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - serverBD.sMenuData.eStaffState = eSTAFF_GET_INTO_POSITION 1") NET_NL()
						#IF IS_DEBUG_BUILD NET_LOG_Host("eStaffState = eSTAFF_GET_INTO_POSITION 1")	#ENDIF
					//ENDIF*/
				ELIF IS_BIT_SET(serverBD.iServerBitSet, biS_AnyCrewHasDemandedCash)
				OR IS_BIT_SET(serverBD.iServerBitSet, biS_SyncSceneStarted)
					serverBD.sMenuData.eStaffState = eSTAFF_EMPTY_REGISTER	//eSTAFF_GET_INTO_POSITION
					NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - serverBD.sMenuData.eStaffState = eSTAFF_EMPTY_REGISTER A") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG_Host("eStaffState = eSTAFF_EMPTY_REGISTER A")	#ENDIF
				ELIF BULLET_OR_EXPLOSION_IN_SHOP()
				OR SHOULD_STAFF_FLEE()
					serverBD.sMenuData.eStaffState = eSTAFF_FLEE_TO_BACK_ROOM
					NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - serverBD.sMenuData.eStaffState = eSTAFF_FLEE_TO_BACK_ROOM - A") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG_Host("eStaffState = eSTAFF_FLEE_TO_BACK_ROOM - A")	#ENDIF
				ELIF HAS_EMPTY_WEAPON_BEEN_SHOT()
					serverBD.sMenuData.eStaffState = eSTAFF_ATTACK
					NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - serverBD.sMenuData.eStaffState = eSTAFF_ATTACK - A") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG_Host("eStaffState = eSTAFF_ATTACK - A")	#ENDIF
				ENDIF
			BREAK
			
			CASE eSTAFF_REACT_TO_WEAPON
				IF IS_BIT_SET(serverBD.iServerBitSet, biS_AnyCrewHasDemandedCash)
				OR IS_BIT_SET(serverBD.iServerBitSet, biS_SyncSceneStarted)
					serverBD.sMenuData.eStaffState = eSTAFF_EMPTY_REGISTER	//eSTAFF_GET_INTO_POSITION
					NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - serverBD.sMenuData.eStaffState = eSTAFF_EMPTY_REGISTER B") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG_Host("eStaffState = eSTAFF_EMPTY_REGISTER B")	#ENDIF
				/*ELIF IS_BIT_SET(serverBD.iServerBitSet, biS_AnyCrewHasDamagedStaff)
					serverBD.sMenuData.eStaffState = eSTAFF_GET_INTO_POSITION
					NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - serverBD.sMenuData.eStaffState = eSTAFF_GET_INTO_POSITION 3") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG_Host("eStaffState = eSTAFF_GET_INTO_POSITION 3")	#ENDIF*/
				ELIF BULLET_OR_EXPLOSION_IN_SHOP()
				OR SHOULD_STAFF_FLEE(TRUE)
					serverBD.sMenuData.eStaffState = eSTAFF_FLEE_TO_BACK_ROOM
					NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - serverBD.sMenuData.eStaffState = eSTAFF_FLEE_TO_BACK_ROOM - B") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG_Host("eStaffState = eSTAFF_FLEE_TO_BACK_ROOM - B")	#ENDIF
				ELIF HAS_EMPTY_WEAPON_BEEN_SHOT()
					serverBD.sMenuData.eStaffState = eSTAFF_ATTACK
					NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - serverBD.sMenuData.eStaffState = eSTAFF_ATTACK - B") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG_Host("eStaffState = eSTAFF_ATTACK - B")	#ENDIF
				ENDIF
			BREAK
			
			CASE eSTAFF_GET_INTO_POSITION
				//IF GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.sMenuData.niRegStaff), SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) = FINISHED_TASK
				IF GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.sMenuData.niRegStaff), SCRIPT_TASK_ACHIEVE_HEADING) != WAITING_TO_START_TASK
				AND GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.sMenuData.niRegStaff), SCRIPT_TASK_ACHIEVE_HEADING) != PERFORMING_TASK
					serverBD.sMenuData.eStaffState = eSTAFF_EMPTY_REGISTER
					NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - serverBD.sMenuData.eStaffState = eSTAFF_EMPTY_REGISTER") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG_Host("eStaffState = eSTAFF_EMPTY_REGISTER")	#ENDIF
				ELIF SHOULD_STAFF_FLEE(TRUE)
					serverBD.sMenuData.eStaffState = eSTAFF_FLEE_TO_BACK_ROOM
					NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - serverBD.sMenuData.eStaffState = eSTAFF_FLEE_TO_BACK_ROOM - C") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG_Host("eStaffState = eSTAFF_FLEE_TO_BACK_ROOM - C")	#ENDIF
				ELIF HAS_EMPTY_WEAPON_BEEN_SHOT()
					serverBD.sMenuData.eStaffState = eSTAFF_ATTACK
					NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - serverBD.sMenuData.eStaffState = eSTAFF_ATTACK - C") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG_Host("eStaffState = eSTAFF_ATTACK - C")	#ENDIF
				ENDIF
			BREAK
			
			CASE eSTAFF_EMPTY_REGISTER
				IF IS_BIT_SET(serverBD.iServerBitSet, biS_SyncSceneStartedRunning)
					BOOL bMoveOn
					bMoveOn = FALSE
					iLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(serverBD.iSceneID)
					IF iLocalSceneID != -1
						IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSceneID)
							//If Shot is fired nearby increase shout 
							IF IS_BULLET_IN_AREA(vRegStaff, 3.0, FALSE)
								IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_ReactToGunShotLineTrigger)
									SET_BIT(serverBD.iServerBitSet, biS_ReactToGunShotLineTrigger)
									NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - biS_ReactToGunShotLineTrigger - TRUE    <----------     ") NET_NL()
								ENDIF
								serverBD.fCurrentShout += 0.35
								NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - BULLET FIRED AROUND STAFF - INCREASE SYNC SCENE SPEED     <----------     ") NET_NL()
							ENDIF
							
							FLOAT fPhase
							fPhase = GET_SYNCHRONIZED_SCENE_PHASE(iLocalSceneID)
							
							//Update Cash Bag Fill Value
							IF fPhase >= 0.8
								serverBD.fCashBagFillAmount = 1.0
							ELIF fPhase <= 0.5
								serverBD.fCashBagFillAmount = 0.0
							ELIF fPhase > serverBD.fCashBagFillAmount
								serverBD.fCashBagFillAmount = ((fPhase-0.5)*3.33)
							ENDIF
							
							//Check if Finished
							IF fPhase >= 0.92	//1.0
								serverBD.fCashBagFillAmount = 1.0
								bMoveOn = TRUE
								NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - eSTAFF_EMPTY_REGISTER - MOVE ON - PHASE DONE") NET_NL()
							ENDIF
						ELSE
							bMoveOn = TRUE
							NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - eSTAFF_EMPTY_REGISTER - MOVE ON - SYNC SCENE NOT RUNNING") NET_NL()
						ENDIF
					ELSE
						bMoveOn = TRUE
						NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - eSTAFF_EMPTY_REGISTER - MOVE ON - iLocalSceneID INVALID") NET_NL()
					ENDIF
					
					//Check if Staff is a Gun Hero
					IF bMoveOn = TRUE
						/*INT iRand
						iRand = GET_RANDOM_INT_IN_RANGE(0, 10)
						//iRand = 0	//TEMP TEST
						IF iRand < iRegStaffAttackChance
							serverBD.sMenuData.eStaffState = eSTAFF_ATTACK
							NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - serverBD.sMenuData.eStaffState = eSTAFF_ATTACK   -   iRegStaffAttackChance = ") NET_PRINT_INT(iRegStaffAttackChance) NET_PRINT("   iRand = ") NET_PRINT_INT(iRand) NET_NL()
							#IF IS_DEBUG_BUILD NET_LOG_Host("eStaffState = eSTAFF_ATTACK")	#ENDIF
						ELSE*/
							serverBD.sMenuData.eStaffState = eSTAFF_HANDS_UP_LOOP_ONLY
							NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - serverBD.sMenuData.eStaffState = eSTAFF_HANDS_UP_LOOP_ONLY") NET_NL()
							#IF IS_DEBUG_BUILD NET_LOG_Host("eStaffState = eSTAFF_HANDS_UP_LOOP_ONLY")	#ENDIF
						//ENDIF
					ELIF SHOULD_STAFF_FLEE(TRUE)
						serverBD.sMenuData.eStaffState = eSTAFF_FLEE_TO_BACK_ROOM
						NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - serverBD.sMenuData.eStaffState = eSTAFF_FLEE_TO_BACK_ROOM - D") NET_NL()
						#IF IS_DEBUG_BUILD NET_LOG_Host("eStaffState = eSTAFF_FLEE_TO_BACK_ROOM - D")	#ENDIF
					ELIF HAS_EMPTY_WEAPON_BEEN_SHOT()
						serverBD.sMenuData.eStaffState = eSTAFF_ATTACK
						NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - serverBD.sMenuData.eStaffState = eSTAFF_ATTACK - D") NET_NL()
						#IF IS_DEBUG_BUILD NET_LOG_Host("eStaffState = eSTAFF_ATTACK - D")	#ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE eSTAFF_HANDS_UP
				IF BULLET_OR_EXPLOSION_IN_SHOP()
				OR SHOULD_STAFF_FLEE(TRUE)
					serverBD.sMenuData.eStaffState = eSTAFF_FLEE_TO_BACK_ROOM
					NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - serverBD.sMenuData.eStaffState = eSTAFF_FLEE_TO_BACK_ROOM - D") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG_Host("eStaffState = eSTAFF_FLEE_TO_BACK_ROOM - E")	#ENDIF
				ENDIF
			BREAK
			
			CASE eSTAFF_HANDS_UP_LOOP_ONLY
				DO_STAFF_ATTACK_CHECK()
				
				IF BULLET_OR_EXPLOSION_IN_SHOP()
				OR SHOULD_STAFF_FLEE(TRUE)
					serverBD.sMenuData.eStaffState = eSTAFF_FLEE_TO_BACK_ROOM
					NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - serverBD.sMenuData.eStaffState = eSTAFF_FLEE_TO_BACK_ROOM - F") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG_Host("eStaffState = eSTAFF_FLEE_TO_BACK_ROOM - F")	#ENDIF
				ELIF HAS_EMPTY_WEAPON_BEEN_SHOT()
					serverBD.sMenuData.eStaffState = eSTAFF_ATTACK
					NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - serverBD.sMenuData.eStaffState = eSTAFF_ATTACK - F") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG_Host("eStaffState = eSTAFF_ATTACK - F")	#ENDIF
				
				//Check if Staff should Attack
				ELIF IS_BIT_SET(serverBD.iServerBitSet2, biS2_MakeStaffAttack)
					IF NOT IS_BIT_SET(serverBD.iServerBitSet2, biS2_AnyCrewIsAimingAtStaff)
						serverBD.sMenuData.eStaffState = eSTAFF_ATTACK
						NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - serverBD.sMenuData.eStaffState = eSTAFF_ATTACK ")  NET_NL()
						#IF IS_DEBUG_BUILD NET_LOG_Host("eStaffState = eSTAFF_ATTACK")	#ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE eSTAFF_FLEE_TO_BACK_ROOM
				DO_STAFF_ATTACK_CHECK()
			BREAK
			
			CASE eSTAFF_ATTACK
				//Do Nothing
			BREAK
		ENDSWITCH
	
	ELSE
		IF NOT IS_BIT_SET(serverBD.iServerBitSet2, biS2_StaffDeadReducedReservation)
			IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.sMenuData.niRegStaff)
				serverBD.iReservedPeds--
				NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - REDUCED - serverBD.iReservedObjects = ") NET_PRINT_INT(serverBD.iReservedPeds) NET_NL()
				SET_BIT(serverBD.iServerBitSet2, biS2_StaffDeadReducedReservation)
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE: Make the Hero do whatever the server thinks it should be doing
FUNC BOOL PROCESS_HERO_BODY()
		
	IF NOT IS_BIT_SET(ServerBD.iServerHeroBitSet, biSH_TryToGrabHero)
		RETURN FALSE
	ENDIF
	
	IF IS_NET_PED_INJURED(serverBD.niHero)
		IF DOES_BLIP_EXIST(HeroBlip)
			REMOVE_BLIP(HeroBlip)
			NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - HERO BLIP REMOVED") NET_NL()
		ENDIF
		RETURN FALSE
	ENDIF
	
	//Control Blip
	IF serverBD.eHeroState = eHERO_ATTACK
		IF NOT DOES_BLIP_EXIST(HeroBlip)
			HeroBlip = ADD_BLIP_FOR_ENTITY(NET_TO_PED(serverBD.niHero))
			SET_BLIP_COLOUR(HeroBlip, BLIP_COLOUR_RED)
			SET_BLIP_SCALE(HeroBlip, BLIP_SIZE_NETWORK_PED)
			NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - HERO BLIP ADDED") NET_NL()
		ENDIF
	ENDIF
	
	//Check if I have control of the Hero
	IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niHero)
	//AND (IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.niHero) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
		RETURN FALSE
	ENDIF
		
	SWITCH serverBD.eHeroState
		CASE eHERO_CREATED
			//Do Nothing
		BREAK
									
		CASE eHERO_WAIT
			//Do Nothing
		BREAK
	
		CASE eHERO_GET_INTO_POSITION
			IF GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.niHero), SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) != WAITING_TO_START_TASK
			IF GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.niHero), SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) != PERFORMING_TASK
			//IF GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.niHero), SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) != FINISHED_TASK
			//NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - eHERO_GET_INTO_POSITION - CHECK D") NET_NL()
				
				IF IS_NET_PLAYER_OK(PLAYER_ID())
					//TASK_FOLLOW_NAV_MESH_TO_COORD(NET_TO_PED(serverBD.niHero), GET_ENTITY_COORDS(PLAYER_PED_ID()), PEDMOVEBLENDRATIO_RUN, -1, 3.0, ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED)
					TASK_GO_TO_ENTITY(NET_TO_PED(serverBD.niHero), PLAYER_PED_ID(), -1, 4.0, PEDMOVEBLENDRATIO_RUN)
					NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - HERO GIVEN TASK - TASK_FOLLOW_NAV_MESH_TO_COORD     <----------     ") NET_NL()
				ENDIF
				
			//ENDIF
			ENDIF
			ENDIF
		BREAK
		
		CASE eHERO_WARNING
			IF NOT IS_BIT_SET(serverBD.iServerHeroBitSet, biSH_TurnForWarning)
			AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerHeroBitSet , biPH_TurnForWarning)
				IF GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.niHero), SCRIPT_TASK_GO_TO_ENTITY) != WAITING_TO_START_TASK
				AND GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.niHero), SCRIPT_TASK_GO_TO_ENTITY) != PERFORMING_TASK
					TASK_TURN_PED_TO_FACE_ENTITY(NET_TO_PED(serverBD.niHero), PLAYER_PED_ID(), 10000)
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerHeroBitSet , biPH_TurnForWarning)
					NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - MAKE HERO TURN TO FACE PLAYER - A")  NET_NL()
				ENDIF
			ENDIF
			
			IF NOT IS_BIT_SET(serverBD.iServerHeroBitSet, biSH_WarningLineDone)
			AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerHeroBitSet , biPH_WarningLineDone)
			AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
				ADD_PED_FOR_DIALOGUE(sSpeech, 4, NET_TO_PED(serverBD.niHero), "StoreHero", TRUE, FALSE)
				/*IF IS_PED_MALE(NET_TO_PED(serverBD.niHero))
					CREATE_CONVERSATION(sSpeech, "RB_12AU", "RB_12_HWM", CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN, TRUE)
				ELSE
					CREATE_CONVERSATION(sSpeech, "RB_12AU", "RB_12_HWF", CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN, TRUE)
				ENDIF*/
				PLAY_PED_AMBIENT_SPEECH_NATIVE(NET_TO_PED(serverBD.niHero), "GENERIC_INSULT_HIGH", "SPEECH_PARAMS_FORCE")
				SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerHeroBitSet , biPH_WarningLineDone)
				NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - HERO GIVEN TASK - WARNING LINE     <----------     ") NET_NL()
			ENDIF
		BREAK
		
		CASE eHERO_ATTACK
			
			IF NOT IS_BIT_SET(serverBD.iServerHeroBitSet, biSH_AttackLineDone)
			AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerHeroBitSet , biPH_AttackLineDone)
			AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
				ADD_PED_FOR_DIALOGUE(sSpeech, 4, NET_TO_PED(serverBD.niHero), "StoreHero", TRUE, FALSE)
				/*IF IS_PED_MALE(NET_TO_PED(serverBD.niHero))
					CREATE_CONVERSATION(sSpeech, "RB_12AU", "RB_12_HAM", CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN, TRUE)
				ELSE
					CREATE_CONVERSATION(sSpeech, "RB_12AU", "RB_12_HAF", CONV_PRIORITY_VERY_HIGH, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN, TRUE)
				ENDIF*/
				PLAY_PED_AMBIENT_SPEECH_NATIVE(NET_TO_PED(serverBD.niHero), "GENERIC_FUCK_YOU", "SPEECH_PARAMS_FORCE")
				SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerHeroBitSet , biPH_AttackLineDone)
				NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - HERO GIVEN TASK - ATTACK LINE     <----------     ") NET_NL()
			ENDIF
			
			IF IS_BIT_SET(serverBD.iServerHeroBitSet, biSH_AttackLineDone)
			OR IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerHeroBitSet , biPH_AttackLineDone)
				IF GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.niHero), SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED) != WAITING_TO_START_TASK
				AND GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.niHero), SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED) != PERFORMING_TASK
					CLEAR_PED_TASKS(NET_TO_PED(serverBD.niHero))
					
					//SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NET_TO_PED(serverBD.niHero), FALSE)
					SET_PED_RELATIONSHIP_GROUP_HASH(NET_TO_PED(serverBD.niHero), rgFM_AiHatePlyrLikeCops)
					
					TASK_COMBAT_HATED_TARGETS_AROUND_PED(NET_TO_PED(serverBD.niHero), 20)
					SET_PED_KEEP_TASK(NET_TO_PED(serverBD.niHero), TRUE)
					NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - HERO GIVEN TASK - ATTACK     <----------     ") NET_NL()
				ENDIF
			ENDIF
		BREAK
		
		CASE eHERO_HANDS_UP
			IF GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.niHero), SCRIPT_TASK_PERFORM_SEQUENCE) != WAITING_TO_START_TASK
			AND GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.niHero), SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
				CLEAR_PED_TASKS(NET_TO_PED(serverBD.niHero))
				TASK_PERFORM_SEQUENCE(NET_TO_PED(serverBD.niHero), StaffHandsUpSeq)
				SET_PED_KEEP_TASK(NET_TO_PED(serverBD.niHero), TRUE)
				NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - HERO GIVEN TASK - SEQUENCE - StaffHandsUpSeq     <----------     ") NET_NL()
			
			//Turn to Face Player
			ELIF GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.niHero), SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY) != PERFORMING_TASK
			AND GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.niHero), SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY) != WAITING_TO_START_TASK
				IF HAS_NET_TIMER_EXPIRED(iTurnTimer, 5000)
					RESET_NET_TIMER(iTurnTimer)
					TASK_TURN_PED_TO_FACE_ENTITY(NET_TO_PED(serverBD.niHero), PLAYER_PED_ID(), 10000)
					NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - MAKE HERO TURN TO FACE PLAYER - B")  NET_NL()
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC


PROC PROCESS_SNACK_BUY_SCENE()
	
	
	//Create Snacks
	IF NOT IS_BIT_SET(serverBD.sMenuData.iServerSnackBitSet, biSS_SnacksCreated)
		IF serverBD.iNumPlayersInStore > 0
			CREATE_SNACKS()
			IF NOT IS_BIT_SET(serverBD.sMenuData.iServerSnackBitSet, biSS_SnacksCreated)
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[AM HOLD UP]      ---------->     SNACKS - PROCESS_SNACK_BUY_SCENE - SNACKS NOT YET CREATED    <----------     ")
				EXIT
			ENDIF
		ENDIF
	ENDIF
	
	
	FLOAT fHeading
		
	IF IS_BIT_SET(serverBD.sMenuData.iServerSnackBitSet, biSS_SnackRequestAnim)
		IF serverBD.sMenuData.eSnackBuyAnimState = SNACKBUY_DONE
		AND NOT IS_BIT_SET(serverBD.iServerBitSet, biS_SyncSceneStarted)
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[AM HOLD UP] *****[snack_anims] anim request received and accepted")
			serverBD.sMenuData.eSnackBuyAnimState = SNACKBUY_INIT
		ELSE
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[AM HOLD UP] *****[snack_anims] anim already running, forgetting anim request")
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[AM HOLD UP] *****[snack_anims] serverBD.sMenuData.eSnackBuyAnimState = ", serverBD.sMenuData.eSnackBuyAnimState)
		ENDIF
		CLEAR_BIT(serverBD.sMenuData.iServerSnackBitSet, biSS_SnackRequestAnim)
	ENDIF
	
	SWITCH serverBD.sMenuData.eSnackBuyAnimState
		CASE SNACKBUY_INIT
			
			IF TAKE_CONTROL_OF_NET_ID(serverBD.sMenuData.niRegStaff)
			AND NOT IS_NET_PED_INJURED(serverBD.sMenuData.niRegStaff)
			AND TAKE_CONTROL_OF_NET_ID(serverBD.niSnacks[serverBD.iServerWhichSnack])
			AND serverBD.sMenuData.eStaffState != eSTAFF_FLEE_TO_BACK_ROOM
			AND serverBD.sMenuData.eStaffState != eSTAFF_ATTACK
				IF NOT IS_PED_RAGDOLL(NET_TO_PED(serverBD.sMenuData.niRegStaff))	
					
					IF IS_BIT_SET(serverBD.iServerBitSet, biS_SyncSceneStarted)
					OR IS_BIT_SET(serverBD.sMenuData.iServerSnackBitSet, biSS_ShopLiftInterruptSnackBuy)
						CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[AM HOLD UP] *****[snack_anims] shop rob sync started, go to cleanup from init")
						serverBD.sMenuData.eSnackBuyAnimState = SNACKBUY_CLEANUP_ANIM
						EXIT
					ENDIF
					
					IF NOT bSnackHeadingReached
						TASK_ACHIEVE_HEADING(NET_TO_PED(serverBD.sMenuData.niRegStaff), serverBD.fRegStaffHeading, SYNC_SCENE_DELAY+SYNC_SCENE_TURN_DELAY)
						bSnackHeadingReached = TRUE
						CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[AM HOLD UP]      ---------->     SNACKS - BEFORE SYNC SCENE - TASK_ACHIEVE_HEADING     <----------     ")
					ENDIF
					
					IF HAS_NET_TIMER_EXPIRED(iSnackSyncSceneDelayTimer, SYNC_SCENE_DELAY)
						fHeading = GET_ENTITY_HEADING(NET_TO_PED(serverBD.sMenuData.niRegStaff))
						FLOAT fHeadingLow
						FLOAT fHeadingHigh
						fHeadingLow = (serverBD.fRegStaffHeading-10)
						fHeadingHigh = (serverBD.fRegStaffHeading+10)
						IF fHeadingLow < 0
							fHeadingHigh = (serverBD.fRegStaffHeading-10+360)
							fHeadingLow = (serverBD.fRegStaffHeading+10+360)
						ELIF fHeadingHigh >= 360
							fHeadingHigh = (serverBD.fRegStaffHeading-10-360)
							fHeadingLow = (serverBD.fRegStaffHeading+10-360)
						ENDIF
						
						// uncomment to debug anim pops in snack anims
//						CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "*****current heading = ", fHeading)
//						CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "*****desired heading = ", serverBD.fRegStaffHeading)
//						CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "*****heading high = ", fHeadingHigh)
//						CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "*****heading low = ", fHeadingLow)
//						CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "*****iSnackSyncSceneTurnTimer = ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(iSnackSyncSceneTurnTimer))
						
						
						IF ((fHeading < fHeadingHigh AND fHeading > fHeadingLow)
							AND HAS_NET_TIMER_EXPIRED(iSnackSyncSceneTurnTimer, SYNC_SCENE_TURN_DELAY-500))
						OR HAS_NET_TIMER_EXPIRED(iSnackSyncSceneTurnTimer, SYNC_SCENE_TURN_DELAY+SYNC_SCENE_SNACK_BUFFER+1500)
							
							CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[AM HOLD UP] *****[ShopRobberies.sc-> snack_anims] beginning serverBD.iServerWhichSnack = ", serverBD.iServerWhichSnack)
							
							
							CLEAR_PED_SECONDARY_TASK(NET_TO_PED(serverBD.sMenuData.niRegStaff))
							
							serverBD.iSnackSCeneID = NETWORK_CREATE_SYNCHRONISED_SCENE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(NET_TO_PED(serverBD.sMenuData.niRegStaff), << -0.65, 0.87, -0.02 >>), 
																					sceneRotation, EULER_YXZ, TRUE)
							
							NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(NET_TO_PED(serverBD.sMenuData.niRegStaff), serverBD.iSnackSCeneID, 
																"mp_am_hold_up", GET_SNACK_CLERK_ANIM(INT_TO_ENUM(SNACK_CATEGORIES, serverBD.iServerWhichSnack)), 
																WALK_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_ON_ABORT_STOP_SCENE | 
																SYNCED_SCENE_PRESERVE_VELOCITY | SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE, RBF_PLAYER_IMPACT)
							
							// use this for the snack item
							IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niSnacks[serverBD.iServerWhichSnack])
								SET_ENTITY_VISIBLE(NET_TO_OBJ(serverBD.niSnacks[serverBD.iServerWhichSnack]), TRUE)
								NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_OBJ(serverBD.niSnacks[serverBD.iServerWhichSnack]), serverBD.iSnackSCeneID,
																"mp_am_hold_up", GET_SNACK_ANIM(INT_TO_ENUM(SNACK_CATEGORIES, serverBD.iServerWhichSnack)), 
																SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_ON_ABORT_STOP_SCENE)
							ENDIF
							
							NETWORK_START_SYNCHRONISED_SCENE(serverBD.iSnackSCeneID)
							
							CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[AM HOLD UP] *****[snack_anims] sync scene tasked, going to play anim")
							serverBD.sMenuData.eSnackBuyAnimState = SNACKBUY_PLAY_ANIM
							
							iLocalSnackSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(serverBD.iSnackSCeneID)
							CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[AM HOLD UP] *****[snack_anims] serverBD.iSnackSCeneID = ", serverBD.iSnackSCeneID)
							CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[AM HOLD UP] *****[snack_anims] iLocalSnackSceneID = ", iLocalSnackSceneID)
							
							//SET_BIT(serverBD.iServerBitSet, biS_SyncSceneStarted)
							
							#IF IS_DEBUG_BUILD NET_LOG_Host("SYNC SCENE STARTED")
						ELSE
							CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[AM HOLD UP] - PROCESS_START_SYNC_SCENE - WAIT FOR PLAYER HEADING ", GET_ENTITY_HEADING(NET_TO_PED(serverBD.sMenuData.niRegStaff)))
							#ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE SNACKBUY_PLAY_ANIM
			
			IF IS_BIT_SET(serverBD.iServerBitSet, biS_SyncSceneStarted)
			OR IS_BIT_SET(serverBD.sMenuData.iServerSnackBitSet, biSS_ShopLiftInterruptSnackBuy)
				iLocalSnackSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(serverBD.iSnackSCeneID)
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSnackSceneID)
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[AM HOLD UP] *****[snack_anims] stopping the network sync scene due to shop rob sync scene")
					NETWORK_STOP_SYNCHRONISED_SCENE(serverBD.iSnackSCeneID)
				ENDIF
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[AM HOLD UP] *****[snack_anims] shop rob sync started, go to cleanup from play")
				serverBD.sMenuData.eSnackBuyAnimState = SNACKBUY_CLEANUP_ANIM
				EXIT
			ENDIF
			
			IF iLocalSnackSceneID != -1
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSnackSceneID)
				OR HAS_NET_TIMER_EXPIRED(iSnackFailSifeTimer, SNACK_SYNC_FAILSAFE)
					IF (IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSnackSceneID) AND GET_SYNCHRONIZED_SCENE_PHASE(iLocalSnackSceneID) = 1)
					OR HAS_NET_TIMER_EXPIRED(iSnackFailSifeTimer, SNACK_SYNC_FAILSAFE)
						IF TAKE_CONTROL_OF_NET_ID(serverBD.sMenuData.niRegStaff)
						AND NOT IS_NET_PED_INJURED(serverBD.sMenuData.niRegStaff)
						AND NOT IS_BIT_SET(serverBD.iServerBitSet, biS_SyncSceneStarted)
							CLEAR_PED_TASKS(NET_TO_PED(serverBD.sMenuData.niRegStaff))
						ENDIF
						
						NETWORK_STOP_SYNCHRONISED_SCENE(serverBD.iSnackSCeneID)
						
						CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[AM HOLD UP] *****[snack_anims] sync scene is done, go to cleanup")
						
						// use for objects
						//iSnackAppearTimer = GET_GAME_TIMER()
						serverBD.sMenuData.eSnackBuyAnimState = SNACKBUY_CLEANUP_ANIM
					ENDIF
				ELSE
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[AM HOLD UP] *****[snack_anims] sync scene not running, try again")
					//iSnackAppearTimer = GET_GAME_TIMER()
					//serverBD.sMenuData.eSnackBuyAnimState = SNACKBUY_CLEANUP_ANIM
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[AM HOLD UP] *****[snack_anims] sync scene < 0")
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[AM HOLD UP] *****[snack_anims] going for a regrab of the local scene")
				iLocalSnackSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(serverBD.iSnackSCeneID)
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[AM HOLD UP] *****[snack_anims] new iLocalSnackSceneID = ", iLocalSnackSceneID)
				//iSnackAppearTimer = GET_GAME_TIMER()
				//serverBD.sMenuData.eSnackBuyAnimState = SNACKBUY_CLEANUP_ANIM
			ENDIF
			
		BREAK
		
		CASE SNACKBUY_CLEANUP_ANIM
			
			TAKE_CONTROL_OF_NET_ID(serverBD.niSnacks[serverBD.iServerWhichSnack])
			
			IF HAS_NET_TIMER_EXPIRED(iSnackMoveItemTimer, SYNC_SCENE_SNACK_DELAY)
			AND TAKE_CONTROL_OF_NET_ID(serverBD.niSnacks[serverBD.iServerWhichSnack])
			
				IF DOES_ENTITY_EXIST(NET_TO_OBJ(serverBD.niSnacks[serverBD.iServerWhichSnack]))
					SET_ENTITY_VISIBLE(NET_TO_OBJ(serverBD.niSnacks[serverBD.iServerWhichSnack]), FALSE)
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[AM HOLD UP] *****[ShopRobberies.sc-> snack_anims] object exists, moving it")
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[AM HOLD UP] *****[ShopRobberies.sc-> snack_anims] ending serverBD.iServerWhichSnack = ", serverBD.iServerWhichSnack)
					FREEZE_ENTITY_POSITION(NET_TO_OBJ(serverBD.niSnacks[serverBD.iServerWhichSnack]), TRUE)
					SET_ENTITY_COORDS(NET_TO_OBJ(serverBD.niSnacks[serverBD.iServerWhichSnack]), GET_SNACK_INIT_POS(INT_TO_ENUM(SNACK_CATEGORIES, serverBD.iServerWhichSnack)))
				ELSE
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[AM HOLD UP] *****[ShopRobberies.sc-> snack_anims] object did not exist, not moving anything")
				ENDIF
				
				iLocalSnackSceneID = -1
				bSnackHeadingReached = FALSE
				
				IF IS_BIT_SET(serverBD.sMenuData.iServerSnackBitSet, biSS_ShopLiftInterruptSnackBuy)
					CLEAR_BIT(serverBD.sMenuData.iServerSnackBitSet, biSS_ShopLiftInterruptSnackBuy)
					CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[AM HOLD UP] *****[ShopRobberies.sc-> snack_anims] snack buy anim was interrupted by shoplift, clearing the interrupt flag")
				ENDIF
				
				RESET_NET_TIMER(iSnackMoveItemTimer)
				RESET_NET_TIMER(iSnackFailSifeTimer)
				RESET_NET_TIMER(iSnackSyncSceneDelayTimer)
				RESET_NET_TIMER(iSnackSyncSceneTurnTimer)
				CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "[AM HOLD UP] *****[ShopRobberies.sc-> snack_anims] cleanup done, going to done")
				serverBD.sMenuData.eSnackBuyAnimState = SNACKBUY_DONE
			ENDIF
		BREAK
		
		CASE SNACKBUY_DONE
			// nada here
		BREAK
		
	ENDSWITCH
	
	
ENDPROC

//PURPOSE: Controls the server starting the sync scene
PROC PROCESS_START_SYNC_SCENE()
	IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_SyncSceneStarted)
		IF MPGlobalsAmbience.bHoldUpTriggeredEmptyReg = TRUE 
		OR IS_BIT_SET(serverBD.iServerBitSet, biS_AnyCrewHasDemandedCash) 
		//OR IS_BIT_SET(playerBD[iStaggeredParticipant].sMenuData.iPlayerBitSet, biP_SyncSceneStarted)
			IF TAKE_CONTROL_OF_NET_ID(serverBD.niCashBag)
			AND TAKE_CONTROL_OF_NET_ID(serverBD.sMenuData.niRegStaff)
			AND NOT IS_NET_PED_INJURED(serverBD.sMenuData.niRegStaff)
			AND serverBD.sMenuData.eStaffState != eSTAFF_FLEE_TO_BACK_ROOM
			AND serverBD.sMenuData.eStaffState != eSTAFF_ATTACK
				IF NOT IS_PED_RAGDOLL(NET_TO_PED(serverBD.sMenuData.niRegStaff))		
					
					IF NOT IS_BIT_SET(shopSnacks.iBoolsBitSet, biHeadingBeforeSyncScene)
						TASK_ACHIEVE_HEADING(NET_TO_PED(serverBD.sMenuData.niRegStaff), serverBD.fRegStaffHeading, SYNC_SCENE_DELAY+SYNC_SCENE_TURN_DELAY)
						SET_BIT(shopSnacks.iBoolsBitSet, biHeadingBeforeSyncScene)
						NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - BEFORE SYNC SCENE - TASK_ACHIEVE_HEADING     <----------     ") NET_NL()
					ENDIF
					
					IF HAS_NET_TIMER_EXPIRED(iSyncSceneDelayTimer, SYNC_SCENE_DELAY)
						FLOAT fHeading = GET_ENTITY_HEADING(NET_TO_PED(serverBD.sMenuData.niRegStaff))
						IF (fHeading > (serverBD.fRegStaffHeading-20) AND fHeading < (serverBD.fRegStaffHeading+20))
						OR HAS_NET_TIMER_EXPIRED(iSyncSceneTurnTimer, SYNC_SCENE_TURN_DELAY)
							//TASK_LOOK_AT_ENTITY(NET_TO_PED(serverBD.sMenuData.niRegStaff), GET_PLAYER_PED(PlayerId), 2500, SLF_DEFAULT)
							CLEAR_PED_SECONDARY_TASK(NET_TO_PED(serverBD.sMenuData.niRegStaff))
							serverBD.iSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(scenePosition, sceneRotation, EULER_YXZ, TRUE)	
							NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(NET_TO_PED(serverBD.sMenuData.niRegStaff), serverBD.iSceneID, "mp_am_hold_up", "holdup_victim_20s", SLOW_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_PRESERVE_VELOCITY | SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE, RBF_PLAYER_IMPACT)
							IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niCashBag)
								//SET_ENTITY_VISIBLE(NET_TO_OBJ(serverBD.niCashBag), TRUE)
								FREEZE_ENTITY_POSITION(NET_TO_OBJ(serverBD.niCashBag), FALSE)
								//SET_ENTITY_COLLISION(NET_TO_OBJ(serverBD.niCashBag), TRUE)
								SET_ENTITY_DYNAMIC(NET_TO_OBJ(serverBD.niCashBag), TRUE)
								NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_OBJ(serverBD.niCashBag), serverBD.iSceneID, "mp_am_hold_up", "holdup_victim_20s_bag", SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_PRESERVE_VELOCITY)
							ENDIF
							IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niTill)
								NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_OBJ(serverBD.niTill), serverBD.iSceneID, "mp_am_hold_up", "holdup_victim_20s_till", SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_PRESERVE_VELOCITY)
							ENDIF
							//SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(serverBD.iSceneID, FALSE)
							NETWORK_START_SYNCHRONISED_SCENE(serverBD.iSceneID)
							NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] -    SYNCHRONISED SCENE    -    [**CREATED AND STARTED**]    <----------     ") NET_NL()
							
							/*IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niCashBag)
								FREEZE_ENTITY_POSITION(NET_TO_OBJ(serverBD.niCashBag), FALSE)
								SET_ENTITY_COLLISION(NET_TO_OBJ(serverBD.niCashBag), TRUE)
								SET_ENTITY_DYNAMIC(NET_TO_OBJ(serverBD.niCashBag), TRUE)
							ENDIF*/
							
							SET_BIT(serverBD.iServerBitSet, biS_SyncSceneStarted)
							NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - biS_SyncSceneStarted - TRUE    <----------     ") NET_NL()
							#IF IS_DEBUG_BUILD NET_LOG_Host("SYNC SCENE STARTED")	
						ELSE
							NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - PROCESS_START_SYNC_SCENE - WAIT FOR PLAYER HEADING ") NET_PRINT_FLOAT(GET_ENTITY_HEADING(NET_TO_PED(serverBD.sMenuData.niRegStaff))) NET_NL()
							#ENDIF
						ENDIF
						
					#IF IS_DEBUG_BUILD
					ELSE
						NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - PROCESS_START_SYNC_SCENE - WAIT FOR REACTION DELAY") NET_NL()
						#ENDIF
					ENDIF
				ELSE
					serverBD.sMenuData.eStaffState = eSTAFF_FLEE_TO_BACK_ROOM
					NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - serverBD.sMenuData.eStaffState = eSTAFF_FLEE_TO_BACK_ROOM - G") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG_Host("eStaffState = eSTAFF_FLEE_TO_BACK_ROOM - G")	#ENDIF
				ENDIF
			//#IF IS_DEBUG_BUILD
			//ELSE
			//	NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - PROCESS_START_SYNC_SCENE - WAITING FOR CONTROL") NET_NL()
			//	#ENDIF
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_SyncSceneStartedRunning)
			iLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(serverBD.iSceneID)
			IF iLocalSceneID != -1
				TAKE_CONTROL_OF_NET_ID(serverBD.sMenuData.niRegStaff)
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSceneID)
					/*IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niCashBag)
						SET_ENTITY_VISIBLE(NET_TO_OBJ(serverBD.niCashBag), TRUE)
						SET_ENTITY_COLLISION(NET_TO_OBJ(serverBD.niCashBag), TRUE)
						NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - MAKE CASH BAG VISIBLE") NET_NL()
					ENDIF*/
					SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE)
					SET_BIT(serverBD.iServerBitSet, biS_SyncSceneStartedRunning)
					NET_PRINT_TIME() NET_PRINT("[AM HOLD UP]    -    SYNCHRONISED SCENE    -    biS_SyncSceneStartedRunning SET    <----------     ") NET_NL()
				ENDIF
			ENDIF
		ELIF NOT IS_BIT_SET(serverBD.iServerBitSet2, biS2_CashBagMadeVisible)
			iLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(serverBD.iSceneID)
			IF iLocalSceneID != -1
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSceneID)
					TAKE_CONTROL_OF_NET_ID(serverBD.sMenuData.niRegStaff)
					IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niCashBag)
					AND TAKE_CONTROL_OF_NET_ID(serverBD.niCashBag)
					AND TAKE_CONTROL_OF_NET_ID(serverBD.sMenuData.niRegStaff)
						IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalSceneID) > 0.44	//0.45
							SET_ENTITY_VISIBLE(NET_TO_OBJ(serverBD.niCashBag), TRUE)
							SET_ENTITY_COLLISION(NET_TO_OBJ(serverBD.niCashBag), TRUE)
							NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - MAKE CASH BAG VISIBLE") NET_NL()
							SET_BIT(serverBD.iServerBitSet2, biS2_CashBagMadeVisible)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Returns a random scenraio string
FUNC STRING GET_RANDOM_SCENARIO()
	INT iRand = GET_RANDOM_INT_IN_RANGE(0, 100)
	IF iRand < 30
		NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - HERO FOUND - GET_RANDOM_SCENARIO = WORLD_HUMAN_SMOKING") NET_NL()
		RETURN "WORLD_HUMAN_SMOKING"
	//ELIF iRand >= 20
	//AND iRand < 40
	//	NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - HERO FOUND - GET_RANDOM_SCENARIO = WORLD_HUMAN_WINDOW_SHOP_BROWSE") NET_NL()
	//	RETURN "WORLD_HUMAN_WINDOW_SHOP_BROWSE"
	//ELIF iRand >= 40
	//AND iRand < 60
	//	NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - HERO FOUND - GET_RANDOM_SCENARIO = WORLD_HUMAN_HIKER_STANDING") NET_NL()
	//	RETURN "WORLD_HUMAN_HIKER_STANDING"
	ELIF iRand >= 30
	AND iRand < 70
		NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - HERO FOUND - GET_RANDOM_SCENARIO = WORLD_HUMAN_HANG_OUT_STREET") NET_NL()
		RETURN "WORLD_HUMAN_HANG_OUT_STREET"
	ELSE
		NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - HERO FOUND - GET_RANDOM_SCENARIO = WORLD_HUMAN_STAND_MOBILE") NET_NL()
		RETURN "WORLD_HUMAN_STAND_MOBILE"
	ENDIF
ENDFUNC

/// PURPOSE: Work out what the Hero should be doing
FUNC BOOL PROCESS_HERO_BRAIN()
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
	OR NOT IS_BIT_SET(serverBD.iServerHeroBitSet, biSH_TryToGrabHero)
		RETURN FALSE
	ENDIF
	
	IF serverBD.eHeroState != eHERO_CLEANUP
		
		IF serverBD.eHeroState > eHERO_CREATED
		AND IS_NET_PED_INJURED(serverBD.niHero)
			serverBD.eHeroState = eHERO_CLEANUP
			RETURN FALSE
		ENDIF
		
		SWITCH serverBD.eHeroState
			
			CASE eHERO_CREATED
				IF NOT HAS_NET_TIMER_EXPIRED(TryToGetHeroTimer, 10000)
					PED_INDEX tempPed
					tempPed = GET_RANDOM_PED_AT_COORD(vBackRoomMin, <<20, 20, 20>>)
					IF NOT IS_PED_INJURED(tempPed)
						IF NOT IS_ENTITY_A_MISSION_ENTITY(tempPed)
						 	MODEL_NAMES HeroModel 
							HeroModel = GET_ENTITY_MODEL(tempPed)
							IF HeroModel != MP_M_FREEMODE_01
							AND HeroModel != MP_F_FREEMODE_01
							AND NOT IS_COP_PED_IN_AREA_3D(vBackRoomMin, <<25, 25, 25>>)
							AND NOT IS_ENTITY_AT_COORD(tempPed, vStoreBlipLocation, <<1.0, 1.0, 2.0>>)
							AND NOT IS_PED_ARMED(tempPed, WF_INCLUDE_PROJECTILE | WF_INCLUDE_GUN | WF_INCLUDE_MELEE)
							AND NOT IS_PED_IN_ANY_VEHICLE(tempPed, TRUE)
							AND IS_PED_HUMAN(tempPed)
							AND IS_MODEL_SUITABLE_FOR_THIEF(HeroModel)
								//serverBD.niHero = PED_TO_NET(tempPed)
								NETWORK_REQUEST_CONTROL_OF_ENTITY(tempPed)
								IF NETWORK_HAS_CONTROL_OF_ENTITY(tempPed)
								
									IF SET_ENTITY_AS_NET_MISSION_ENTITY(serverBD.niHero, tempPed, TRUE, FALSE)
										GIVE_DELAYED_WEAPON_TO_PED(NET_TO_PED(serverBD.niHero), WEAPONTYPE_PISTOL, 25000, FALSE)
										SET_PED_KEEP_TASK(NET_TO_PED(serverBD.niHero), TRUE)
										serverBD.eHeroState = eHERO_WAIT
										SET_BIT(serverBD.iServerHeroBitSet, biSH_GotHero)
										
										SET_PED_RELATIONSHIP_GROUP_HASH(NET_TO_PED(serverBD.niHero), rgFM_AiDislikePlyrLikeCops)
										
										SET_PED_COMBAT_MOVEMENT(NET_TO_PED(serverBD.niHero), CM_WILLADVANCE)
										
										SET_ENTITY_HEALTH(NET_TO_PED(serverBD.niHero), ROUND(200*g_sMPTunables.fAiHealthModifier))
						
										ADD_PED_FOR_DIALOGUE(sSpeech, 4, NET_TO_PED(serverBD.niHero), "StoreHero", TRUE, FALSE)
										TASK_START_SCENARIO_IN_PLACE(NET_TO_PED(serverBD.niHero), GET_RANDOM_SCENARIO(), 0, TRUE)
										
										NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - HERO FOUND - serverBD.eHeroState = eHERO_WAIT") NET_NL()
										#IF IS_DEBUG_BUILD NET_LOG_Host("eHeroState = eHERO_WAIT")	#ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE eHERO_WAIT
				IF IS_BIT_SET(serverBD.iServerBitSet, biS_SyncSceneStarted)
				OR IS_BIT_SET(serverBD.iServerBitSet, biS_AnyCrewHasDamagedStaff)
				OR IS_BIT_SET(serverBD.iServerBitSet, biS_AnyCrewHasWeaponDrawn)
				//OR BULLET_OR_EXPLOSION_IN_SHOP()
				//OR GRENADE_IN_SHOP()
					serverBD.eHeroState = eHERO_GET_INTO_POSITION
					NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - serverBD.eHeroState = eHERO_GET_INTO_POSITION") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG_Host("eHeroState = eHERO_GET_INTO_POSITION")	#ENDIF
				ENDIF
			BREAK
			
			CASE eHERO_GET_INTO_POSITION
				IF HAS_NET_TIMER_EXPIRED(serverBD.iHeroAttackTimer, 3000)
				OR (GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.niHero), SCRIPT_TASK_GO_TO_ENTITY) != WAITING_TO_START_TASK AND GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.niHero), SCRIPT_TASK_GO_TO_ENTITY) != PERFORMING_TASK)
					RESET_NET_TIMER(serverBD.iHeroAttackTimer)
					serverBD.eHeroState = eHERO_WARNING
					NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - serverBD.eHeroState = eHERO_WARNING") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG_Host("eHeroState = eHERO_WARNING")	#ENDIF
				ENDIF
			BREAK
			
			CASE eHERO_WARNING
				IF IS_BIT_SET(serverBD.iServerBitSet, biS_AnyCrewHasDamagedStaff)
				OR IS_BIT_SET(serverBD.iServerHeroBitSet, biSH_AnyCrewHasDamagedHero)
				OR IS_BIT_SET(serverBD.iServerHeroBitSet, biSH_AnyCrewHasAimedAtHero)
				OR HAS_NET_TIMER_EXPIRED(serverBD.iHeroAttackTimer, 5000)
					IF GET_RANDOM_BOOL()
					OR HAS_NET_TIMER_EXPIRED(serverBD.iHeroAttackTimer, 5000)
						serverBD.eHeroState = eHERO_ATTACK
						NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - serverBD.eHeroState = eHERO_ATTACK") NET_NL()
						#IF IS_DEBUG_BUILD NET_LOG_Host("eHeroState = eHERO_ATTACK")	#ENDIF
					ELSE
						serverBD.eHeroState = eHERO_HANDS_UP
						NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - serverBD.eHeroState = eHERO_HANDS_UP") NET_NL()
						#IF IS_DEBUG_BUILD NET_LOG_Host("eHeroState = eHERO_HANDS_UP")	#ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE eHERO_ATTACK
				//Do Nothing
			BREAK
			
			CASE eHERO_HANDS_UP
				//Do Nothing
			BREAK
		ENDSWITCH	
	ENDIF
	
	RETURN TRUE
ENDFUNC

//PURPOSE: Creates a shocking event when the Hold Up has been triggered
PROC PROCESS_SHOCKING_EVENT()
	IF serverBD.iShockingEvent = 0
		IF serverBD.sMenuData.eStaffState >= eSTAFF_REACT_TO_WEAPON
		AND serverBD.sMenuData.eStaffState != eSTAFF_CLEANUP
			serverBD.iShockingEvent = ADD_SHOCKING_EVENT_FOR_ENTITY(EVENT_SHOCKING_MUGGING, PLAYER_PED_ID(), 60000)	//ADD_SHOCKING_EVENT_AT_POSITION(EVENT_SHOCKING_GUNSHOT_FIRED, vRegStaff, 60000)
			#IF IS_DEBUG_BUILD
			IF serverBD.iShockingEvent > 0
				NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - PROCESS_SHOCKING_EVENT - EVENT_SHOCKING_MUGGING - SHOCKING EVENT CREATED = ") NET_PRINT_INT(serverBD.iShockingEvent) NET_NL()
			ENDIF
			#ENDIF
		ENDIF
	ENDIF
ENDPROC				

//PURPOSE: Gives the Staff the tasks they need to react
PROC CONTROL_STAFF()
	PROCESS_STAFF_BODY()
ENDPROC

//PURPOSE: Alerts the Cops once the Hold Up has begun
PROC ALERT_COPS_CHECK(BOOL bDoAllChecks = TRUE)

	IF PLAYER_ID() != MPGlobalsAmbience.piBeastPlayer
	AND NOT IS_PLAYER_AN_ANIMAL(PLAYER_ID())
		IF NOT IS_BIT_SET(shopSnacks.iBoolsBitSet, biCopsAlerted)
			//IF NOT NETWORK_IS_IN_TUTORIAL_SESSION()
							
				//Staff Threatened
				/*IF serverBD.sMenuData.eStaffState >= eSTAFF_GET_INTO_POSITION
					GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iHoldUpsDoneInCloseSuccession++
					RESET_NET_TIMER(MPGlobals.iResetHoldUpSuccessionTimer)
					START_NET_TIMER(MPGlobals.iResetHoldUpSuccessionTimer)	//MPGlobals.iResetHoldUpSuccessionTimer = (GET_THE_NETWORK_TIMER() + HOLD_UP_SUCCESSION_TIMER_RESET)
					
					IF GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iHoldUpsDoneInCloseSuccession <= 2
						REPORT_CRIME(PLAYER_ID(), CRIME_HASSLE, GET_WANTED_LEVEL_THRESHOLD(2))	//SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 2)
						NET_PRINT_TIME() NET_PRINT_STRINGS("[AM HOLD UP] - REPORT_CRIME 2   <----------     ", GET_MP_MISSION_NAME(shopSnacks.thisHoldupMission)) NET_NL()
					ELIF GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iHoldUpsDoneInCloseSuccession >= 5
						REPORT_CRIME(PLAYER_ID(), CRIME_HASSLE, GET_WANTED_LEVEL_THRESHOLD(4))
						NET_PRINT_TIME() NET_PRINT_STRINGS("[AM HOLD UP] - REPORT_CRIME 4   <----------     ", GET_MP_MISSION_NAME(shopSnacks.thisHoldupMission)) NET_NL()
					ELSE
						REPORT_CRIME(PLAYER_ID(), CRIME_HASSLE, GET_WANTED_LEVEL_THRESHOLD(3))
						NET_PRINT_TIME() NET_PRINT_STRINGS("[AM HOLD UP] - REPORT_CRIME 3   <----------     ", GET_MP_MISSION_NAME(shopSnacks.thisHoldupMission)) NET_NL()
					ENDIF
					
					//SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
					NET_PRINT_TIME() NET_PRINT_STRINGS("[AM HOLD UP] - REPORT_CRIME  - eSTAFF_GET_INTO_POSITION     <----------     ", GET_MP_MISSION_NAME(shopSnacks.thisHoldupMission)) NET_NL()
					
					#IF IS_DEBUG_BUILD NET_LOG("ALERT_COPS_CHECK - DONE")	#ENDIF
					SET_BIT(shopSnacks.iBoolsBitSet, biCopsAlerted)
				ENDIF*/
				
				//Gun Drawn Away from Staff
				//IF IS_BIT_SET(serverBD.iServerBitSet, biS_AnyCrewHasWeaponDrawn)
				//AND NOT IS_BIT_SET(serverBD.iServerBitSet, biS_AnyCrewHasWeaponDrawnNearStaff)
				
				IF bDoAllChecks = TRUE
					//Do Player Weapon Drawn Check
					BOOL bPlayerHasWeaponDrawn
					BOOL bPlayerHasWeaponDrawnNotClose
					WEAPON_TYPE CurrentWeapon
					IF GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), CurrentWeapon)
						IF CurrentWeapon != WEAPONTYPE_UNARMED
						AND CurrentWeapon != WEAPONTYPE_INVALID
						AND CurrentWeapon != WEAPONTYPE_OBJECT
						AND CurrentWeapon != WEAPONTYPE_FLARE
						AND CurrentWeapon != WEAPONTYPE_DLC_SNOWBALL
							IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vRegStaff, <<2.5, 2.5, 3>>)
								bPlayerHasWeaponDrawnNotClose = TRUE
							ENDIF
							bPlayerHasWeaponDrawn = TRUE
						ENDIF
					ENDIF
					
					IF (bPlayerHasWeaponDrawnNotClose = TRUE OR IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_CashBagPickedUp))// OR serverBD.sMenuData.eStaffState >= eSTAFF_GET_INTO_POSITION)
					OR (bPlayerHasWeaponDrawn = TRUE AND serverBD.sMenuData.eStaffState >= eSTAFF_GET_INTO_POSITION)
					OR (bPlayerHasWeaponDrawn = TRUE AND IS_BIT_SET(serverBD.iServerBitSet2, biS2_RegisterRobberManually))
					OR IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_RobbedRegisterManually)
						IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_AnyCrewHasWeaponDrawnNearStaff) 
						OR serverBD.sMenuData.eStaffState >= eSTAFF_GET_INTO_POSITION
						OR IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_RobbedRegisterManually)
						OR (bPlayerHasWeaponDrawn = TRUE AND IS_BIT_SET(serverBD.iServerBitSet2, biS2_RegisterRobberManually))
							GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iHoldUpsDoneInCloseSuccession++
							RESET_NET_TIMER(MPGlobals.iResetHoldUpSuccessionTimer)
							START_NET_TIMER(MPGlobals.iResetHoldUpSuccessionTimer)	//MPGlobals.iResetHoldUpSuccessionTimer = (GET_THE_NETWORK_TIMER() + HOLD_UP_SUCCESSION_TIMER_RESET)
							
							IF GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iHoldUpsDoneInCloseSuccession <= 2
								IF GB_IS_LOCAL_PLAYER_ON_SMASH_AND_GRAB()
									IF SMASH_AND_GRAB_WANTED_CAP() < 2
										REPORT_CRIME(PLAYER_ID(), CRIME_HASSLE, GET_WANTED_LEVEL_THRESHOLD(SMASH_AND_GRAB_WANTED_CAP()))	//SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 2)
										NET_PRINT_TIME() NET_PRINT_STRINGS("[AM HOLD UP] [STMGB] - REPORT_CRIME 2 reporting crim based on Smash-and-grab wanted cap  <----------     ", GET_MP_MISSION_NAME(shopSnacks.thisHoldupMission)) NET_NL()
									ELSE
										REPORT_CRIME(PLAYER_ID(), CRIME_HASSLE, GET_WANTED_LEVEL_THRESHOLD(2))	//SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 2)
										NET_PRINT_TIME() NET_PRINT_STRINGS("[AM HOLD UP] [STMGB] - REPORT_CRIME 2   <----------     ", GET_MP_MISSION_NAME(shopSnacks.thisHoldupMission)) NET_NL()
									ENDIF
								ELSE
									REPORT_CRIME(PLAYER_ID(), CRIME_HASSLE, GET_WANTED_LEVEL_THRESHOLD(2))	//SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 2)
									NET_PRINT_TIME() NET_PRINT_STRINGS("[AM HOLD UP] - REPORT_CRIME 2   <----------     ", GET_MP_MISSION_NAME(shopSnacks.thisHoldupMission)) NET_NL()
								ENDIF
							ELIF GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iHoldUpsDoneInCloseSuccession >= 5
								IF GB_IS_LOCAL_PLAYER_ON_SMASH_AND_GRAB()
									IF SMASH_AND_GRAB_WANTED_CAP() < 3
										REPORT_CRIME(PLAYER_ID(), CRIME_HASSLE, GET_WANTED_LEVEL_THRESHOLD(SMASH_AND_GRAB_WANTED_CAP()))
									ELSE
										REPORT_CRIME(PLAYER_ID(), CRIME_HASSLE, GET_WANTED_LEVEL_THRESHOLD(3))
									ENDIF
								ELSE
									REPORT_CRIME(PLAYER_ID(), CRIME_HASSLE, GET_WANTED_LEVEL_THRESHOLD(4))
								ENDIF
								NET_PRINT_TIME() NET_PRINT_STRINGS("[AM HOLD UP] - REPORT_CRIME 4   <----------     ", GET_MP_MISSION_NAME(shopSnacks.thisHoldupMission)) NET_NL()
							ELSE
								IF GB_IS_LOCAL_PLAYER_ON_SMASH_AND_GRAB()
									IF SMASH_AND_GRAB_WANTED_CAP() < 3
										REPORT_CRIME(PLAYER_ID(), CRIME_HASSLE, GET_WANTED_LEVEL_THRESHOLD(SMASH_AND_GRAB_WANTED_CAP()))
									ELSE
										REPORT_CRIME(PLAYER_ID(), CRIME_HASSLE, GET_WANTED_LEVEL_THRESHOLD(3))
									ENDIF
								ELSE
									REPORT_CRIME(PLAYER_ID(), CRIME_HASSLE, GET_WANTED_LEVEL_THRESHOLD(3))
									NET_PRINT_TIME() NET_PRINT_STRINGS("[AM HOLD UP] - REPORT_CRIME 3   <----------     ", GET_MP_MISSION_NAME(shopSnacks.thisHoldupMission)) NET_NL()
								ENDIF
							ENDIF
							
							IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_PlayerHasDemandedCash)
								SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_GotWantedWithBlindEyePerk)
								PRINTLN("[AM HOLD UP]  biP_GotWantedWithBlindEyePerk - SET - B")
							ENDIF
							
							//SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
							NET_PRINT_TIME() NET_PRINT_STRINGS("[AM HOLD UP] - REPORT_CRIME - biS_AnyCrewHasWeaponDrawnNearStaff NOT SET     <----------     ", GET_MP_MISSION_NAME(shopSnacks.thisHoldupMission)) NET_NL()

							#IF IS_DEBUG_BUILD NET_LOG("ALERT_COPS_CHECK - DONE")	#ENDIF
							SET_BIT(shopSnacks.iBoolsBitSet, biCopsAlerted)
							SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_ReactToWantedLevelLineDone)	//STOP WANTED LEVEL LINE BEING TRIGGERED
						ENDIF
					ENDIF
				ENDIF
				
				//Melee Warning
				IF NOT IS_BIT_SET(shopSnacks.iBoolsBitSet, biCopsAlertedToMelee)
				AND NOT IS_BIT_SET(shopSnacks.iBoolsBitSet, biCopsAlerted)			//AND NOT IS_BIT_SET(serverBD.iServerBitSet, biS_AnyCrewHasWeaponDrawn)
					IF IS_PED_IN_MELEE_COMBAT(PLAYER_PED_ID()) 
						IF HAS_NET_TIMER_EXPIRED(iMeleeTimer, 2000)
						OR IS_PED_PERFORMING_MELEE_ACTION(PLAYER_PED_ID())
							REPORT_CRIME(PLAYER_ID(), CRIME_HASSLE, GET_WANTED_LEVEL_THRESHOLD(1))
							NET_PRINT_TIME() NET_PRINT_STRINGS("[AM HOLD UP] - REPORT_CRIME - biCopsAlertedToMelee SET - WL 1     <----------     ", GET_MP_MISSION_NAME(shopSnacks.thisHoldupMission)) NET_NL()
							SET_BIT(shopSnacks.iBoolsBitSet, biCopsAlertedToMelee)
							SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_PlayerHasUnarmedAimedAtStaff)
							SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_ReactToWantedLevelLineDone)	//STOP WANTED LEVEL LINE BEING TRIGGERED
							IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_PlayerHasDemandedCash)
								SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_GotWantedWithBlindEyePerk)
								PRINTLN("[AM HOLD UP]  biP_GotWantedWithBlindEyePerk - SET - C")
							ENDIF
						ENDIF
					ELSE
						IF HAS_NET_TIMER_STARTED(iMeleeTimer)
							RESET_NET_TIMER(iMeleeTimer)
							NET_PRINT_TIME() NET_PRINT_STRINGS("[AM HOLD UP] - RESET iMeleeTimer      <----------     ", GET_MP_MISSION_NAME(shopSnacks.thisHoldupMission)) NET_NL()
						ENDIF
					ENDIF
				ENDIF
			//ENDIF
		ENDIF
		
		//Alert Cops of Shopkeeper Death
		IF NOT IS_BIT_SET(shopSnacks.iBoolsBitSet, biCopsAlerted2)
			//IF NOT NETWORK_IS_IN_TUTORIAL_SESSION()
				IF IS_BIT_SET(shopSnacks.iBoolsBitSet, biWeaponHolstered)
					IF IS_NET_PED_INJURED(serverBD.sMenuData.niRegStaff)
						IF GB_IS_LOCAL_PLAYER_ON_SMASH_AND_GRAB()
							IF SMASH_AND_GRAB_WANTED_CAP() < 3
								REPORT_CRIME(PLAYER_ID(), CRIME_HASSLE, GET_WANTED_LEVEL_THRESHOLD(SMASH_AND_GRAB_WANTED_CAP()))
							ELSE
								REPORT_CRIME(PLAYER_ID(), CRIME_HASSLE, GET_WANTED_LEVEL_THRESHOLD(3))
							ENDIF
						ELSE
							REPORT_CRIME(PLAYER_ID(), CRIME_SHOOT_PED, GET_WANTED_LEVEL_THRESHOLD(3))
						ENDIF
						NET_PRINT_TIME() NET_PRINT_STRINGS("[AM HOLD UP] - REPORT_CRIME - SHOPKEEPER KILLED   <----------     ", GET_MP_MISSION_NAME(shopSnacks.thisHoldupMission)) NET_NL()
						SET_BIT(shopSnacks.iBoolsBitSet, biCopsAlerted2)
						IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_PlayerHasDemandedCash)
							SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_GotWantedWithBlindEyePerk)
							PRINTLN("[AM HOLD UP]  biP_GotWantedWithBlindEyePerk - SET - D")
						ENDIF
					ENDIF
				ENDIF
			//ENDIF
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: If the player is the Staff Killer and they are out of the shop then give a Wanted Level
PROC OUT_OF_SHOP_KILLER_CHECK()
	IF PLAYER_ID() != MPGlobalsAmbience.piBeastPlayer
	AND NOT IS_PLAYER_AN_ANIMAL(PLAYER_ID())
		//Alert Cops of Shopkeeper Death
		IF NOT IS_BIT_SET(shopSnacks.iBoolsBitSet, biCopsAlerted2)
			IF serverBD.StaffKiller = PLAYER_ID()
			OR (IS_BIT_SET(shopSnacks.iBoolsBitSet, biWeaponHolstered) AND IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vStoreMin, <<HOLD_UP_GIVE_XP_RANGE, HOLD_UP_GIVE_XP_RANGE, HOLD_UP_GIVE_XP_RANGE>>))	//Means player has been inside the shop
				//IF NOT NETWORK_IS_IN_TUTORIAL_SESSION()
					IF IS_NET_PED_INJURED(serverBD.sMenuData.niRegStaff)
						IF GB_IS_LOCAL_PLAYER_ON_SMASH_AND_GRAB()
							IF SMASH_AND_GRAB_WANTED_CAP() < 3
								REPORT_CRIME(PLAYER_ID(), CRIME_HASSLE, GET_WANTED_LEVEL_THRESHOLD(SMASH_AND_GRAB_WANTED_CAP()))
							ELSE
								REPORT_CRIME(PLAYER_ID(), CRIME_HASSLE, GET_WANTED_LEVEL_THRESHOLD(3))
							ENDIF
						ELSE
							REPORT_CRIME(PLAYER_ID(), CRIME_SHOOT_PED, GET_WANTED_LEVEL_THRESHOLD(3))
						ENDIF
						NET_PRINT_TIME() NET_PRINT_STRINGS("[AM HOLD UP] - OUT_OF_SHOP_KILLER_CHECK - REPORT CRIME - SHOPKEEPER KILLED   <----------     ", GET_MP_MISSION_NAME(shopSnacks.thisHoldupMission)) NET_NL()
						SET_BIT(shopSnacks.iBoolsBitSet, biCopsAlerted2)
						IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_PlayerHasDemandedCash)
							SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_GotWantedWithBlindEyePerk)
							PRINTLN("[AM HOLD UP]  biP_GotWantedWithBlindEyePerk - SET - E")
						ENDIF
					ENDIF
				//ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//PUSRPOSE: Sets bits according to the actions of the player (i.e. aimed a gun at the Staff)
PROC PLAYER_ACTION_CHECKS()
	CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_PlayerIsAimingAtStaff)
	
	//Check if player has aimed a gun at the Staff
	//IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_PlayerHasDemandedCash)
		IF NOT IS_NET_PED_INJURED(serverBD.sMenuData.niRegStaff)
		AND IS_NET_PLAYER_OK(PLAYER_ID())
			WEAPON_TYPE CurrentWeapon
			IF GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), CurrentWeapon)
				IF CurrentWeapon != WEAPONTYPE_UNARMED
				AND CurrentWeapon != WEAPONTYPE_OBJECT
				AND CurrentWeapon != WEAPONTYPE_FLARE
				AND CurrentWeapon != WEAPONTYPE_DLC_SNOWBALL
				AND CurrentWeapon != WEAPONTYPE_DLC_FLASHLIGHT
					IF ( IS_PLAYER_FREE_AIMING(PLAYER_ID()) AND IS_PED_FACING_PED(PLAYER_PED_ID(), NET_TO_PED(serverBD.sMenuData.niRegStaff), 45.0) AND IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), NET_TO_PED(serverBD.sMenuData.niRegStaff), <<5.0, 5.0, 2.0>>) )
					OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), NET_TO_PED(serverBD.sMenuData.niRegStaff))
					OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), NET_TO_PED(serverBD.sMenuData.niRegStaff))
						IF NOT (IS_PHONE_ONSCREEN() OR IS_CELLPHONE_CAMERA_IN_USE())
						AND (NOT IS_PLAYER_CRITICAL_TO_ANY_EVENT(PLAYER_ID()) 
						OR GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_GB_CHAL_ROB_SHOP
						OR GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_GB_CASHING_OUT)
							IF IS_PLAYER_IN_FRONT_OF_PED(PLAYER_ID(), NET_TO_PED(serverBD.sMenuData.niRegStaff))
							OR NETWORK_IS_PLAYER_TALKING(PLAYER_ID())
													
								//IF ( NETWORK_PLAYER_HAS_HEADSET(PLAYER_ID()) AND NETWORK_IS_PLAYER_TALKING(PLAYER_ID()) )	//REMOVED FOR BUG 594213
								//OR NOT NETWORK_PLAYER_HAS_HEADSET(PLAYER_ID())
								IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_PlayerHasDemandedCash)
									BROADCAST_HOLD_UP_TRIGGER_EMPTY_REG(SPECIFIC_PLAYER(NETWORK_GET_PLAYER_INDEX(NETWORK_GET_HOST_OF_THIS_SCRIPT())))
									NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - BROADCAST_HOLD_UP_TRIGGER_EMPTY_REG - CALLED") NET_NL()
									SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_PlayerHasDemandedCash)
									NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - biP_PlayerHasDemandedCash = TRUE") NET_NL()
									#IF IS_DEBUG_BUILD NET_LOG("Player Aimed at Staff")	#ENDIF
								ENDIF
								IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_PlayerHasAimedAtStaff)
									SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_PlayerHasAimedAtStaff)
									NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - biP_PlayerHasAimedAtStaff = TRUE") NET_NL()
								ENDIF
							ENDIF
														
							SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_PlayerIsAimingAtStaff)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	//ENDIF
	
	//Do Head Additive Anim
	IF NOT IS_BIT_SET(shopSnacks.iBoolsBitSet, biDoingHeadAdditiveAnim)
		IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_PlayerIsAimingAtStaff)
			//WEAPON_TYPE PlayerWeapon = GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID())
			WEAPON_TYPE PlayerWeapon
			IF GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), PlayerWeapon)
				IF PlayerWeapon != WEAPONTYPE_INVALID
				AND PlayerWeapon != WEAPONTYPE_DLC_SNOWBALL
					WEAPON_GROUP PlayerWeaponGroup = GET_WEAPONTYPE_GROUP(PlayerWeapon)
					IF PlayerWeaponGroup = WEAPONGROUP_PISTOL
						sHeadAdditiveAnim = "hold_up_head_additive_pistol"
						TASK_PLAY_ANIM(PLAYER_PED_ID(), "mp_am_hold_up", sHeadAdditiveAnim, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING|AF_UPPERBODY|AF_SECONDARY|AF_ADDITIVE)
					ELIF PlayerWeaponGroup = WEAPONGROUP_MG
					OR PlayerWeaponGroup = WEAPONGROUP_RIFLE
					OR PlayerWeaponGroup = WEAPONGROUP_SHOTGUN
					OR PlayerWeaponGroup = WEAPONGROUP_SNIPER
					OR PlayerWeaponGroup = WEAPONGROUP_SMG
						sHeadAdditiveAnim = "hold_up_head_additive_rifle"
						TASK_PLAY_ANIM(PLAYER_PED_ID(), "mp_am_hold_up", sHeadAdditiveAnim, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING|AF_UPPERBODY|AF_SECONDARY|AF_ADDITIVE)
					ELSE
						sHeadAdditiveAnim = ""
					ENDIF
					NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - HEAD ADDITIVE ANIM - STARTED ") NET_PRINT(sHeadAdditiveAnim) NET_NL()
					SET_BIT(shopSnacks.iBoolsBitSet, biDoingHeadAdditiveAnim)
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_PlayerIsAimingAtStaff)
		OR IS_PED_SHOOTING(PLAYER_PED_ID())
			//CLEAR_PED_TASKS(PLAYER_PED_ID()
			IF NOT IS_STRING_NULL_OR_EMPTY(sHeadAdditiveAnim)
				IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "mp_am_hold_up", sHeadAdditiveAnim)
					STOP_ANIM_TASK(PLAYER_PED_ID(), "mp_am_hold_up", sHeadAdditiveAnim)
				ENDIF
			ENDIF
			CLEAR_BIT(shopSnacks.iBoolsBitSet, biDoingHeadAdditiveAnim)
			NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - HEAD ADDITIVE ANIM - STOPPED ") NET_PRINT(sHeadAdditiveAnim) NET_NL()
		ENDIF
	ENDIF
	
	//Aimed at Hero
	IF IS_BIT_SET(serverBD.iServerHeroBitSet, biSH_GotHero)
		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerHeroBitSet, biPH_PlayerHasAimedAtHero)
			IF NOT IS_NET_PED_INJURED(serverBD.niHero)
			AND IS_NET_PLAYER_OK(PLAYER_ID())
				WEAPON_TYPE CurrentWeapon
				IF GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), CurrentWeapon)
					IF CurrentWeapon != WEAPONTYPE_UNARMED
					AND CurrentWeapon != WEAPONTYPE_OBJECT
					AND CurrentWeapon != WEAPONTYPE_FLARE
					AND CurrentWeapon != WEAPONTYPE_DLC_SNOWBALL
					AND NOT (IS_PHONE_ONSCREEN() OR IS_CELLPHONE_CAMERA_IN_USE())
						IF IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), NET_TO_PED(serverBD.niHero))
						OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), NET_TO_PED(serverBD.niHero))
							IF IS_PLAYER_IN_FRONT_OF_PED(PLAYER_ID(), NET_TO_PED(serverBD.niHero))
							OR NETWORK_IS_PLAYER_TALKING(PLAYER_ID())
								SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerHeroBitSet, biPH_PlayerHasAimedAtHero)
								NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - biPH_PlayerHasAimedAtHero = TRUE") NET_NL()
								#IF IS_DEBUG_BUILD NET_LOG("Player Aimed at Hero")	#ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


//PURPOSE: Controls how many entities are reserved based on serverBD
PROC MAINTAIN_RESERVED_ENTITIES()
	INT iCurrentlyReserved = GET_NUM_RESERVED_MISSION_PEDS(FALSE)
	IF serverBD.iReservedPeds != iCurrentlyReserved
		IF serverBD.iReservedPeds < iCurrentlyReserved
		OR CAN_RESERVE_NETWORK_PEDS_FOR_THIS_SCRIPT(serverBD.iReservedPeds, FALSE, TRUE)
			RESERVE_NETWORK_MISSION_PEDS(serverBD.iReservedPeds)
			NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - MAINTAIN_RESERVED_ENTITIES - RESERVED PEDS = ") NET_PRINT_INT(serverBD.iReservedPeds) NET_NL()
		ENDIF
	ENDIF
	
	iCurrentlyReserved = GET_NUM_RESERVED_MISSION_OBJECTS(FALSE)
	IF serverBD.iReservedObjects != iCurrentlyReserved
		IF serverBD.iReservedObjects < iCurrentlyReserved
		OR CAN_RESERVE_NETWORK_OBJECTS_FOR_THIS_SCRIPT(serverBD.iReservedObjects, FALSE, TRUE)
			RESERVE_NETWORK_MISSION_OBJECTS(serverBD.iReservedObjects)
			NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - MAINTAIN_RESERVED_ENTITIES - RESERVED OBJECTS = ") NET_PRINT_INT(serverBD.iReservedObjects) NET_NL()
		ENDIF
	ENDIF
ENDPROC

PROC LOAD_EXTRA_ANIMS()
	IF NOT IS_BIT_SET(shopSnacks.iBoolsBitSet2, bi2ExtraAnimsLoaded)
		REQUEST_ANIM_DICT("oddjobs@shop_robbery@rob_till")
		REQUEST_ANIM_DICT(GET_SHOPLIFT_ANIM_DICT())
		
		IF HAS_ANIM_DICT_LOADED("oddjobs@shop_robbery@rob_till")
		AND HAS_ANIM_DICT_LOADED(GET_SHOPLIFT_ANIM_DICT())
			SET_BIT(shopSnacks.iBoolsBitSet2, bi2ExtraAnimsLoaded)
			NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - LOAD_EXTRA_ANIMS - ANIMS LOADED   <----------     ") NET_NL()
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Process the Hold Up event
PROC PROCESS_HOLD_UP()
	
	MAINTAIN_RESERVED_ENTITIES()
	CONTROL_STAFF()
	CONTROL_CASH_BAG()
	GIVE_END_HOLD_UP_REWARD()
	PROCESS_HERO_BODY()
	OUT_OF_SHOP_KILLER_CHECK()
	
//	BOOL bInMainArea = IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vStoreMin, vStoreMax, fStoreWidth)
//	BOOL bInBackRoom = IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vBackRoomMin, vBackRoomMax, fBackRoomWidth)
//	BOOL bInExtraMainArea = IN_EXTRA_MAIN_AREA(PLAYER_PED_ID(), shopSnacks.NearestHoldUpPoint)

	BOOL bInMainArea
	BOOL bInBackRoom
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = interiorID
				IF MPGlobalsAmbience.sSmashAndGrabStruct.bInsideHoldupStore = FALSE
					MPGlobalsAmbience.sSmashAndGrabStruct.bInsideHoldupStore = TRUE
				ENDIF
			IF GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()) = iRoomHashMain
			OR GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()) = iRoomHashMain2
				bInMainArea = TRUE
			ELIF GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()) = iRoomHashBack
				bInBackRoom = TRUE
			ENDIF
		ELSE
				IF MPGlobalsAmbience.sSmashAndGrabStruct.bInsideHoldupStore = TRUE
					MPGlobalsAmbience.sSmashAndGrabStruct.bInsideHoldupStore = FALSE
				ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(shopSnacks.iBoolsBitSet, biWeaponHolstered)
		IF GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) != FMMC_TYPE_GB_CHAL_ROB_SHOP 
			IF bInMainArea
			OR bInBackRoom
			OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vDoorCoord, <<HOLD_UP_ENTRANCE_RANGE/2, HOLD_UP_ENTRANCE_RANGE/2, HOLD_UP_ENTRANCE_RANGE/2>>)
				IF NOT IS_PLAYER_AN_ANIMAL(PLAYER_ID())
					SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
					SET_BIT(shopSnacks.iBoolsBitSet, biWeaponHolstered)
					NET_PRINT_TIME() NET_PRINT("[AM HOLD UP]      ----->   AM_HOLD_UP - WEAPON HOLSTERED   <----------     ") NET_NL()
				ENDIF
			ENDIF
		ENDIF		
	ENDIF
	
	IF NOT IS_BIT_SET(shopSnacks.iBoolsBitSet, biStoreMenuLoaded)
	AND bInMainArea
	AND NOT (IS_PHONE_ONSCREEN() OR IS_CELLPHONE_CAMERA_IN_USE())
		IF LOAD_MENU_ASSETS("SNK_MNU", ENUM_TO_INT(shopSnacks.eCurrentShop))
			NET_PRINT_TIME() NET_PRINT("[AM HOLD UP]      ---------->     menu assets are all loaded   <----------     ") NET_NL()
			SET_BIT(shopSnacks.iBoolsBitSet, biStoreMenuLoaded)
		ELSE
			NET_PRINT_TIME() NET_PRINT("[AM HOLD UP]      ---------->     menu assets have not loaded   <----------     ") NET_NL()
		ENDIF
	ENDIF
	
	IF NOT bInMainArea
	AND IS_BIT_SET(shopSnacks.iBoolsBitSet, biStoreMenuLoaded)
		IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
		OR (DOES_CAM_EXIST(shopSnacks.ciSnacks) AND IS_CAM_ACTIVE(shopSnacks.ciSnacks))
			CLEAR_PED_TASKS(PLAYER_PED_ID())
	
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			
			IF shopSnacks.iCashSpentThisTranssation > 0
				playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iCashSpent = shopSnacks.iCashSpentThisTranssation
				shopSnacks.iCashSpentThisTranssation = 0
			ENDIF
			
			CLEANUP_TEMP_PASSIVE_MODE()
			CLEAR_BIT(shopSnacks.iBoolsBitSet2, bi2TempPassiveModeEnabled)
			
			IF DOES_CAM_EXIST(shopSnacks.ciSnacks)
				DESTROY_CAM(shopSnacks.ciSnacks)
			ENDIF
			RENDER_SCRIPT_CAMS(FALSE,FALSE)
		ENDIF
	
		CLEANUP_MENU_ASSETS(TRUE, ENUM_TO_INT(shopsnacks.eCurrentShop))
		CLEAR_BIT(shopSnacks.iBoolsBitSet, biStoreMenuLoaded)
		NET_PRINT_TIME() NET_PRINT("[AM HOLD UP]      ---------->     menu assets were cleaned out   <----------     ") NET_NL()
	ENDIF
	
	IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_RestrictedAreaEntered)
		IF bInBackRoom
		OR PED_IS_BEHIND_HOLD_UP_COUNTER(PLAYER_PED_ID(), shopSnacks.NearestHoldUpPoint)
			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_RestrictedAreaEntered)
			NET_PRINT_TIME() NET_PRINT("[AM HOLD UP]      ----->   AM_HOLD_UP - biP_RestrictedAreaEntered SET   <----------     ") NET_NL()
		ENDIF
	ENDIF
	
	IF bInMainArea
	OR bInBackRoom
//	OR bInExtraMainArea
	//OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vStoreMin, <<HOLD_UP_ENTRANCE_RANGE, HOLD_UP_ENTRANCE_RANGE, HOLD_UP_ENTRANCE_RANGE>>)
		/*IF bInBackRoom	//IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vBackRoomMin, vBackRoomMax, fBackRoomWidth)
		AND NOT bInExtraMainArea
		//OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vStoreMin, <<HOLD_UP_ENTRANCE_RANGE, HOLD_UP_ENTRANCE_RANGE, HOLD_UP_ENTRANCE_RANGE>>)
			//CLEAR_HOLD_UP_FLOATING_HELP()
			CONTROL_PROGRESS_BAR()
		ELSE*/
			LOAD_EXTRA_ANIMS()
			CONTROL_PROGRESS_BAR()
			CONTROL_HELP_TEXT()
			ALERT_COPS_CHECK()
			PLAYER_ACTION_CHECKS()
			
		/*ENDIF*/
		
		SUPPRESS_LOSING_WANTED_LEVEL_IF_HIDDEN_THIS_FRAME(PLAYER_ID())
		REPORT_POLICE_SPOTTED_PLAYER(PLAYER_ID())
		
		IF IS_BIT_SET(MPGlobalsAmbience.iHoldUpGeneralBitSet, biG_HoldUpOnActivityList)
			SET_LAW_RESPONSE_DELAY_OVERRIDE(serverBD.fCopDelay)
			REMOVE_ACTIVITY_FROM_DISPLAY_LIST(vDoorCoord,GET_ACTIVITY_NAME_FROM_FMMC_TYPE(FMMC_TYPE_HOLD_UPS))
			CLEAR_BIT(MPGlobalsAmbience.iHoldUpGeneralBitSet, biG_HoldUpOnActivityList)
			//NET_PRINT_TIME() NET_PRINT("     ----->   AM_HOLD_UP - B - REMOVED FROM ACTIVITY DISPLAY  <-----     ") NET_NL()
		ENDIF
	ELSE
		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vDoorCoord, <<HOLD_UP_ENTRANCE_RANGE, HOLD_UP_ENTRANCE_RANGE, HOLD_UP_ENTRANCE_RANGE_HEIGHT>>)
			ALERT_COPS_CHECK(FALSE)
		ENDIF
		
		//CLEAR_HOLD_UP_FLOATING_HELP()
		IF NOT IS_BIT_SET(MPGlobalsAmbience.iHoldUpGeneralBitSet, biG_HoldUpOnActivityList)
			RESET_LAW_RESPONSE_DELAY_OVERRIDE()
			ADD_NAMED_ACTIVITY_TO_DISPLAY_LIST(vDoorCoord,GET_ACTIVITY_NAME_FROM_FMMC_TYPE(FMMC_TYPE_HOLD_UPS),"")
			SET_BIT(MPGlobalsAmbience.iHoldUpGeneralBitSet, biG_HoldUpOnActivityList)
			//NET_PRINT_TIME() NET_PRINT("     ----->   AM_HOLD_UP - B - ADDED TO ACTIVITY DISPLAY  <-----     ") NET_NL()
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_ReactToShoutLineDone)
	OR IS_BIT_SET(serverBD.iServerBitSet, biS_ReactToGunShotLineDone)
	OR IS_BIT_SET(serverBD.iServerBitSet, biS_StaffReactToWeaponDone)
		SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
		//NET_PRINT_TIME() NET_PRINT("     ----->   AM_HOLD_UP - SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME   <----------     ") NET_NL()
	ENDIF
ENDPROC

////PURPOSE: If playing the Tutorial Hold Up then tell the player to leave the area
//PROC PROCESS_TUTORIAL_LEAVE_AREA()
//	
//	//Do God Text
//	IF NOT IS_BIT_SET(shopSnacks.iBoolsBitSet, biLeaveAreaTextDone)
//		
//		//If this is the first time the store has been held up then increase the Award Stat
//		INT iHoldUpsAwardBit
//		iHoldUpsAwardBit= GET_MP_INT_CHARACTER_STAT(MP_STAT_HOLDUPS_BITSET)
//		IF NOT IS_BIT_SET(iHoldUpsAwardBit, ENUM_TO_INT(shopSnacks.NearestHoldUpPoint))
//			INCREMENT_BY_MP_INT_CHARACTER_AWARD(MP_AWARD_HOLD_UP_SHOPS, 1)
//			SET_BIT(iHoldUpsAwardBit, ENUM_TO_INT(shopSnacks.NearestHoldUpPoint))
//			SET_MP_INT_CHARACTER_STAT(MP_STAT_HOLDUPS_BITSET, iHoldUpsAwardBit)
//		ENDIF
//		
//		INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_TOTAL_NO_SHOPS_HELD_UP, 1)
//		
//		GIVE_PASS_XP()
//		GIVE_LOCAL_PLAYER_CASH_END_OF_MISSION(serverBD.iCashShare)
//		
//		NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - REWARD GIVEN - HOLD UP = ") NET_PRINT_INT(ENUM_TO_INT(shopSnacks.NearestHoldUpPoint)) NET_NL()
//		SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iHoldUpDoneBitSet, ENUM_TO_INT(shopSnacks.NearestHoldUpPoint))
//		NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - iHoldUpDoneBitSet - SET - B - HOLD UP = ") NET_PRINT_INT(ENUM_TO_INT(shopSnacks.NearestHoldUpPoint)) NET_NL()
//		SET_BIT(shopSnacks.iBoolsBitSet, biXPandCashGiven)
//		
//		//Print_Objective_Text("HUP_LEAV1")	//~s~Leave the area.
//		SET_BIT(shopSnacks.iBoolsBitSet, biLeaveAreaTextDone)
//	ENDIF
//	
//	//End mission if player leaves the area
//	//IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vStoreMin, <<26, 26, 26>>)
//		playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
//		NET_PRINT_TIME() NET_PRINT_STRINGS("[AM HOLD UP] - playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END 2 - MISSION END - PROCESS_TUTORIAL_LEAVE_AREA     <----------     ", GET_MP_MISSION_NAME(shopSnacks.thisHoldupMission)) NET_NL()
//		#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_END 2")	#ENDIF
//	//ENDIF
//ENDPROC

//PURPOSE: Stops the staff from performing empty register and deletes the money bag object
PROC CLEANUP_ENTITIES()
	//Register Staff
	/*IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.sMenuData.niRegStaff)
		IF TAKE_CONTROL_OF_NET_ID(serverBD.sMenuData.niRegStaff)
			IF NOT IS_NET_PED_INJURED(serverBD.sMenuData.niRegStaff)
				CLEAR_PED_TASKS(NET_TO_PED(serverBD.sMenuData.niRegStaff))
				SET_PED_KEEP_TASK(NET_TO_PED(serverBD.sMenuData.niRegStaff), TRUE)
				TASK_STAND_STILL(NET_TO_PED(serverBD.sMenuData.niRegStaff), -1)
				CLEANUP_NET_ID(serverBD.sMenuData.niRegStaff)
				NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - REG STAFF - TASKS CLEARED AND CLEANED UP      <----------     ") NET_NL()
			ENDIF
		ENDIF
	ENDIF*/
	
	//Money Bag
	IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niCashBag)
		IF TAKE_CONTROL_OF_NET_ID(serverBD.niCashBag)
			IF IS_ENTITY_ATTACHED(NET_TO_OBJ(serverBD.niCashBag))
				DETACH_ENTITY(NET_TO_OBJ(serverBD.niCashBag))
				CLEANUP_NET_ID(serverBD.niCashBag)
				NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - CASH BAG - DETACHED AND CLEANED UP      <----------     ") NET_NL()
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//Cleanup Mission Data
PROC SCRIPT_CLEANUP()
	
	//IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CR_HOLD_UP")
    //	CLEAR_HELP()
   	//ENDIF
	//CLEAR_HOLD_UP_FLOATING_HELP()
	
	MPGlobalsAmbience.bHoldUpTriggeredEmptyReg = FALSE
	g_eCurrentShop = SHOP_ROBBERIES_SHOP_NONE
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF NOT IS_NET_PED_INJURED(serverBD.sMenuData.niRegStaff)
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sMenuData.niRegStaff)
			OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.sMenuData.niRegStaff) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
				SET_RAGDOLL_BLOCKING_FLAGS(NET_TO_PED(serverBD.sMenuData.niRegStaff), RBF_NONE)
				CLEAR_FACIAL_IDLE_ANIM_OVERRIDE(NET_TO_PED(serverBD.sMenuData.niRegStaff))
				
				IF IS_BIT_SET(shopSnacks.iBoolsBitSet, biInitialisationDone)
					IF IS_BIT_SET(shopSnacks.iBoolsBitSet, biXPandCashGiven)
						//IF GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.sMenuData.niRegStaff), SCRIPT_TASK_PERFORM_SEQUENCE) != WAITING_TO_START_TASK
						//AND GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.sMenuData.niRegStaff), SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
							IF serverBD.sMenuData.eStaffState != eSTAFF_ATTACK
								//IF NOT IS_ENTITY_PLAYING_ANIM(NET_TO_PED(serverBD.sMenuData.niRegStaff), "mp_am_hold_up", "handsup_base")
								IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_StaffFleeToBackRoom)	
									CLEAR_PED_TASKS(NET_TO_PED(serverBD.sMenuData.niRegStaff))
									TASK_PERFORM_SEQUENCE(NET_TO_PED(serverBD.sMenuData.niRegStaff), StaffFleeToBackRoomSeq)
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NET_TO_PED(serverBD.sMenuData.niRegStaff), FALSE)
									NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - SCRIPT CLEANUP - MAKE STAFF FLEE TO BACK ROOM <----------     ") NET_NL()
								ELSE
									NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - SCRIPT CLEANUP - STAFF ALREADY FLED TO BACK ROOM <----------     ") NET_NL()
								ENDIF
							ELSE
								IF GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.sMenuData.niRegStaff), SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED) != WAITING_TO_START_TASK
								AND GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.sMenuData.niRegStaff), SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED) != PERFORMING_TASK
									CLEAR_PED_TASKS(NET_TO_PED(serverBD.sMenuData.niRegStaff))
									IF NOT HAS_PED_GOT_WEAPON(NET_TO_PED(serverBD.sMenuData.niRegStaff), WEAPONTYPE_PISTOL)
										GIVE_DELAYED_WEAPON_TO_PED(NET_TO_PED(serverBD.sMenuData.niRegStaff), WEAPONTYPE_PISTOL, 25000, TRUE)
										SET_PED_ACCURACY(NET_TO_PED(serverBD.sMenuData.niRegStaff), 30)
									ENDIF
									CONTROL_DIALOGUE_LINE(4)		//STAFF - Get the fuck out of my store asshole!
									//TASK_PERFORM_SEQUENCE(NET_TO_PED(serverBD.sMenuData.niRegStaff), StaffAttackSeq)
									TASK_COMBAT_HATED_TARGETS_AROUND_PED(NET_TO_PED(serverBD.sMenuData.niRegStaff), 20)
									NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - SCRIPT CLEANUP - MAKE STAFF ATTACK    <----------     ") NET_NL()
								ENDIF
							ENDIF
						//ENDIF
					ELIF IS_BIT_SET(serverBD.iServerBitSet, biS_AllCrewLeftArea)
					OR NOT IS_ANY_PLAYER_AT_SHOP()	//IS_BIT_SET(serverBD.iServerBitSet, biS_AnyCrewInShop)
					OR NOT IS_ANY_ALIVE_PLAYER_AT_SHOP()
						//**STOP_SYNC_SCENE()
						IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_StaffFleeToBackRoom)	
							CLEAR_PED_TASKS(NET_TO_PED(serverBD.sMenuData.niRegStaff))
							TASK_PERFORM_SEQUENCE(NET_TO_PED(serverBD.sMenuData.niRegStaff), StaffFleeToBackRoomSeq)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NET_TO_PED(serverBD.sMenuData.niRegStaff), FALSE)
							NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - SCRIPT CLEANUP - MAKE STAFF FLEE TO BACK ROOM - B <----------     ") NET_NL()
						ELSE
							NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - SCRIPT CLEANUP - STAFF ALREADY FLED TO BACK ROOM - B <----------     ") NET_NL()
						ENDIF
					ELSE
						CLEAR_PED_TASKS(NET_TO_PED(serverBD.sMenuData.niRegStaff))
						SET_PED_KEEP_TASK(NET_TO_PED(serverBD.sMenuData.niRegStaff), TRUE)
						TASK_STAND_STILL(NET_TO_PED(serverBD.sMenuData.niRegStaff), -1)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NET_TO_PED(serverBD.sMenuData.niRegStaff), FALSE)
						NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - SCRIPT CLEANUP - STAFF MADE TO JUST STNAD STILL - A <----------     ") NET_NL()
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//Delete Bag if necessary
		IF IS_BIT_SET(serverBD.iServerBitSet, biS_AllCrewLeftArea)
		OR NOT IS_ANY_PLAYER_AT_SHOP()	//IS_BIT_SET(serverBD.iServerBitSet, biS_AnyCrewInShop)
		OR NETWORK_GET_NUM_PARTICIPANTS() <= 1
			IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niCashBag)
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niCashBag)
					DETACH_ENTITY(NET_TO_OBJ(serverBD.niCashBag))
					DELETE_NET_ID(serverBD.niCashBag)
					NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - CASH BAG DELETED   <----------     ") NET_NL()
				ENDIF
			ENDIF
		ENDIF
		
		//Delete Till if necessary
		IF IS_BIT_SET(serverBD.iServerBitSet, biS_AllCrewLeftArea)
		OR NOT IS_ANY_PLAYER_AT_SHOP()	//IS_BIT_SET(serverBD.iServerBitSet, biS_AnyCrewInShop)
		OR IS_BIT_SET(shopSnacks.iBoolsBitSet, biXPandCashGiven)
		OR NETWORK_GET_NUM_PARTICIPANTS() <= 1
			IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niTill)
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niTill)
					DELETE_NET_ID(serverBD.niTill)
					//REMOVE_MODEL_HIDE(scenePosition, 0.5, ArtTill, TRUE)
					//NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - REMOVE_MODEL_HIDE - shopSnacks.NearestHoldUpPoint = ") NET_PRINT_INT(ENUM_TO_INT(shopSnacks.NearestHoldUpPoint)) NET_NL()
					
					//enumMP_SWAPPING_MAP_OBJECTS eObject = INT_TO_ENUM(enumMP_SWAPPING_MAP_OBJECTS, (ENUM_TO_INT(MP_swapObjects_HOLD_UP_TILL_1)+ENUM_TO_INT(shopSnacks.NearestHoldUpPoint)))
					//BROADCAST_SWAP_MAP_OBJECT(eObject, FALSE, TRUE)
					NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - TILL DELETED   <----------     ") NET_NL()
				ENDIF
			ENDIF
		ENDIF
			
		IF USE_SERVER_TRANSACTIONS()
		AND IS_BIT_SET(MPGlobalsAmbience.iHoldUpGeneralBitSet, biG_HoldUpCashDropTransactionRequested)
		
			NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - CASH TRANSACTION TERMINATED   <----------     ") NET_NL()
			
			IF GET_CASH_TRANSACTION_STATUS(MPGlobalsAmbience.iHoldupDropCashTransactionIndex) = CASH_TRANSACTION_STATUS_PENDING
				UPDATE_CASH_TRANSACTION_FLAGS(MPGlobalsAmbience.iHoldupDropCashTransactionIndex, CTPF_AUTO_PROCESS_REPLY)
			ELSE
				DELETE_CASH_TRANSACTION(MPGlobalsAmbience.iHoldupDropCashTransactionIndex)
			ENDIF
			
			CLEAR_BIT(MPGlobalsAmbience.iHoldUpGeneralBitSet, biG_HoldUpCashDropTransactionRequested)
		ENDIF
		
		IF serverBD.iServerGameState = GAME_STATE_END
			IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.sMenuData.niRegStaff)
				IF DECOR_EXIST_ON(NET_TO_PED(serverBD.sMenuData.niRegStaff), "XP_Blocker")
					DECOR_REMOVE(NET_TO_PED(serverBD.sMenuData.niRegStaff), "XP_Blocker")
				ENDIF
			ENDIF
		ENDIF
	
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			REMOVE_SHOCKING_EVENT(serverBD.iShockingEvent)
		ENDIF
	ENDIF
	
	CLEAR_SEQUENCES()
	
	REMOVE_ANIM_DICT("mp_am_hold_up")
	//REMOVE_ANIM_DICT("misscommon@response")
	
	RESET_LAW_RESPONSE_DELAY_OVERRIDE()
	
	//CLEAR_SPAWN_AREA()
	
	//Get the current timer so player can't instantly start one as soon as he has finished.
	//GET_NETWORK_TIMER(Crim_HoldUpStartTimer)
	
	/*IF IS_BIT_SET(shopSnacks.iBoolsBitSet, biXPandCashGiven)
		UPDATE_FMMC_END_OF_MISSION_STATUS(ciFMMC_END_OF_MISSION_STATUS_PASSED, FMMC_TYPE_HOLD_UPS)
	ELSE
		IF IS_BIT_SET(serverBD.iServerBitSet, biS_StaffDeadAndBagEmpty)
		OR IS_BIT_SET(serverBD.iServerBitSet, biS_CashBagDroppedAndEmpty)
			UPDATE_FMMC_END_OF_MISSION_STATUS(ciFMMC_END_OF_MISSION_STATUS_FAILED, FMMC_TYPE_HOLD_UPS)
		ELSE
			UPDATE_FMMC_END_OF_MISSION_STATUS(ciFMMC_END_OF_MISSION_STATUS_CANCELLED, FMMC_TYPE_HOLD_UPS)
		ENDIF
	ENDIF*/
	
	//CLEAR_TUTORIAL_INVITES_ARE_BLOCKED()
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SHR_MENU")
		CLEAR_HELP()
		PRINTLN("[AM HOLD UP] - CLEANUP - CLEAR HELP 'SHR_MENU' - G")
	ENDIF
	
	MPGlobalsAmbience.piHoldUpTriggerPlayer = INVALID_PLAYER_INDEX()
	
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(GET_SHOP_HEADER_TEXTURE_STRING(shopSnacks.eCurrentShop))
	
	CLEANUP_MENU_ASSETS(TRUE, ENUM_TO_INT(shopsnacks.eCurrentShop))
	IF shopSnacks.iUseContext != NEW_CONTEXT_INTENTION
		RELEASE_CONTEXT_INTENTION(shopSnacks.iUseContext)
	ENDIF
	
	MPGlobalsAmbience.iLastShop = ENUM_TO_INT(shopsnacks.eCurrentShop)
	
	IF MPGlobalsAmbience.sSmashAndGrabStruct.bManualHoldup = TRUE
		MPGlobalsAmbience.sSmashAndGrabStruct.bManualHoldup = FALSE
	ENDIF
	IF MPGlobalsAmbience.sSmashAndGrabStruct.bHoldupComplete = TRUE
		MPGlobalsAmbience.sSmashAndGrabStruct.bHoldupComplete = FALSE
	ENDIF
	
	IF IS_BIT_SET(shopSnacks.iBoolsBitSet, biHideTill)
		REMOVE_MODEL_HIDE(scenePosition, 0.5, ArtTill, TRUE)
		NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - REMOVE_MODEL_HIDE - shopSnacks.NearestHoldUpPoint = ") NET_PRINT_INT(ENUM_TO_INT(shopSnacks.NearestHoldUpPoint)) NET_NL()
	ENDIF
	
	IF IS_BIT_SET(shopSnacks.iBoolsBitSet2, bi2TempPassiveModeEnabled)
		CLEANUP_TEMP_PASSIVE_MODE()
		CLEAR_BIT(shopSnacks.iBoolsBitSet2, bi2TempPassiveModeEnabled)
		NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - CLEANUP_TEMP_PASSIVE_MODE on cleanup") NET_NL()
	ENDIF
	
	//TELEMETRY
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF IS_BIT_SET(serverBD.iServerBitSet, biS_AnyCrewHasDemandedCash)	//MAKE SURE HOLD UP WAS ACTUALLY STARTED
		OR IS_BIT_SET(serverBD.iServerBitSet, biS_StaffDead)
			INT iStaffKilled
			IF IS_BIT_SET(serverBD.iServerBitSet, biS_StaffDead)
				iStaffKilled = 1
			ENDIF
			PLAYSTATS_HOLD_UP_MISSION_DONE(FMMC_TYPE_HOLD_UPS, iEarnedXP, iEarnedCash, iStaffKilled)
			NET_PRINT_TIME() NET_PRINT_STRINGS("[AM HOLD UP] - CLEANUP - PLAYSTATS_HOLD_UP_MISSION_DONE      <----------     ", GET_MP_MISSION_NAME(shopSnacks.thisHoldupMission)) NET_NL()
		ENDIF
	ENDIF
	
	IF DOES_CAM_EXIST(shopSnacks.ciSnacks)
		IF IS_CAM_ACTIVE(shopSnacks.ciSnacks)
			RENDER_SCRIPT_CAMS(FALSE,FALSE)
			NET_PRINT_TIME() NET_PRINT_STRINGS("[AM HOLD UP] - CLEANUP RENDER_SCRIPT_CAMS FALSE      <----------     ", GET_MP_MISSION_NAME(shopSnacks.thisHoldupMission)) NET_NL()
		ENDIF
		DESTROY_CAM(shopSnacks.ciSnacks)
		NET_PRINT_TIME() NET_PRINT_STRINGS("[AM HOLD UP] - CLEANUP - DESTROY ciSnacks CAM      <----------     ", GET_MP_MISSION_NAME(shopSnacks.thisHoldupMission)) NET_NL()
	ENDIF
			
	NET_PRINT_TIME() NET_PRINT_STRINGS("[AM HOLD UP] - CLEANUP CR_HOLD_UP MISSION      <----------     ", GET_MP_MISSION_NAME(shopSnacks.thisHoldupMission)) NET_NL()
	#IF IS_DEBUG_BUILD NET_LOG("SCRIPT_CLEANUP")	#ENDIF
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

//Check if Event for Trigger Empty Register has been received
//FUNC BOOL CHECK_FOR_PLAYER_TRIGGER_EMPTY_REG()
//	INT iCount
//	EVENT_NAMES ThisScriptEvent
//	SCRIPT_EVENT_DATA_HOLD_UP_TRIGGER_EMPTY_REG sei
//
//	//process the events
//	REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iCount)
//	
//		ThisScriptEvent = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iCount)
//
//		SWITCH ThisScriptEvent
//			CASE EVENT_NETWORK_SCRIPT_EVENT
//			
//				IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sei, SIZE_OF(STRUCT_ENTITY_DAMAGE_EVENT))					
//					NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - CHECK_FOR_PLAYER_TRIGGER_EMPTY_REG - TRUE    <----------     ") NET_NL()
//					RETURN TRUE
//				ENDIF
//				
//			BREAK
//		ENDSWITCH
//	ENDREPEAT
//	
//	RETURN FALSE
//ENDFUNC

//PURPOSE: Processes checks that need to be done each frequently
PROC PROCESS_CHECKS()
	//Check if Staff has been killed
	IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_StaffDead)
		IF IS_NET_PED_INJURED(serverBD.sMenuData.niRegStaff)
			//Track Killer
			WEAPON_TYPE TempWeapon
			serverBD.StaffKiller = NETWORK_GET_DESTROYER_OF_NETWORK_ID(serverBD.sMenuData.niRegStaff, TempWeapon)
			#IF IS_DEBUG_BUILD
			IF serverBD.StaffKiller != INVALID_PLAYER_INDEX()
				NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - biS_StaffDead - StaffKiller = ") NET_PRINT(GET_PLAYER_NAME(serverBD.StaffKiller)) NET_NL()
			ENDIF
			#ENDIF
			
			SET_BIT(serverBD.iServerBitSet, biS_StaffDead)
			NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - biS_StaffDead - TRUE    <----------     ") NET_NL()
		ENDIF
	ENDIF
	
	//Check if Cash Bag has been dropped whilst empty
	IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_CashBagDroppedAndEmpty)
		IF IS_BIT_SET(serverBD.iServerBitSet, biS_CashBagDroppedEarly)
		AND serverBD.fCashBagFillAmount <= 0.0
			SET_BIT(serverBD.iServerBitSet, biS_CashBagDroppedAndEmpty)
			NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - biS_CashBagDroppedAndEmpty - TRUE    <----------     ") NET_NL()
		ENDIF
	ENDIF
	
	//Make sure the Cash Bag has been set so it can be picked up.
	IF NOT IS_BIT_SET(serverBD.iServerBitSet2, biS2_CashBagPreventCollectionCleared)
		IF IS_BIT_SET(serverBD.iServerBitSet, biS_CashBagAvailableForPickup)
			IF TAKE_CONTROL_OF_NET_ID(serverBD.niCashBag)
				PREVENT_COLLECTION_OF_PORTABLE_PICKUP(NET_TO_OBJ(serverBD.niCashBag), FALSE)
				NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - CASH BAG - PREVENT_COLLECTION_OF_PORTABLE_PICKUP - SERVER - FALSE     <----------     ") NET_NL()
				SET_BIT(serverBD.iServerBitSet2, biS2_CashBagPreventCollectionCleared)
			ENDIF
		ENDIF
	ENDIF
	
	//Check if any event about a player triggering the empty register has been received
	IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_AnyCrewHasDemandedCash)
		IF MPGlobalsAmbience.bHoldUpTriggeredEmptyReg = TRUE	//CHECK_FOR_PLAYER_TRIGGER_EMPTY_REG()
			SET_BIT(serverBD.iServerBitSet, biS_AnyCrewHasDemandedCash)
			SET_BIT(serverBD.iServerBitSet, biS_AnyCrewHasAimedAtStaff)
			NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - biS_AnyCrewHasAimedAtStaff AND biS_AnyCrewHasDemandedCash - TRUE    <----------     ") NET_NL()
			//NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - CHECK_FOR_PLAYER_TRIGGER_EMPTY_REG = TRUE    <----------     ") NET_NL()
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Runs through the max num participents and does relevant checks
PROC MAINTAIN_MAX_PARTICIPANT_CHECKS_STAGGERED_SERVER()
	IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iStaggeredParticipant))
		PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iStaggeredParticipant))
		PED_INDEX PlayerPedId = GET_PLAYER_PED(PlayerId)
		
		//Check if any player has picked up the cash
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_CashPickedUp)
			IF IS_BIT_SET(playerBD[iStaggeredParticipant].sMenuData.iPlayerBitSet, biP_CashBagPickedUp)
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niCashBag)
					IF TAKE_CONTROL_OF_NET_ID(serverBD.niCashBag)
						DETACH_ENTITY(NET_TO_OBJ(serverBD.niCashBag))
						DELETE_NET_ID(serverBD.niCashBag)
						NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - CASH BAG - DELETED BY SERVER      <----------     ") NET_NL()
					ENDIF
				ELSE
					serverBD.vPickedUpCashBagPos = playerBD[iStaggeredParticipant].vPickedUpCashBagPos
					
					/*serverBD.iCashShare = (GET_RANDOM_INT_IN_RANGE(iMinCash, iMaxCash) / serverBD.iNumPlayersInStore)	//NETWORK_GET_NUM_PARTICIPANTS()
					
					serverBd.iCashShare = ROUND(TO_FLOAT(serverBd.iCashShare)/2)
					NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - CASH BAG - CASH HALVED DUE TO FREEMODE      <----------     ") NET_NL()
					
					//Give player less if the bag was dropped early
					IF IS_BIT_SET(serverBD.iServerBitSet, biS_CashBagDroppedEarly)
						//serverBd.iCashShare = (serverBd.iCashShare/2)
						serverBd.iCashShare = FLOOR(TO_FLOAT(serverBd.iCashShare)*serverBD.fCashBagFillAmount)	//0.5 to 0.8
						NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - CASH BAG DROPPED EARLY - PLAYER EARNS AMOUNT * ") NET_PRINT_FLOAT(serverBD.fCashBagFillAmount)  NET_NL()
					ENDIF*/
					
					/*INT iCash
					INT iTimesPlayed = 1
					iCash = (20 * iTimesPlayed)
					
					IF iCash < 100
						iCash = 100
					ELIF iCash > 500
						iCash = 500
					ENDIF
					serverBd.iCashShare = (iCash / NETWORK_GET_NUM_PARTICIPANTS())*/
					
					serverBD.iReservedObjects = 7
					NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - REDUCED - serverBD.iReservedObjects = 7     <----------     ") NET_NL()
					
					SET_BIT(serverBD.iServerBitSet, biS_CashPickedUp)
					NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - CASH BAG - PICKED UP - SET BY SERVER     <----------     ") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG_Host("CASH BAG PICKED UP")	#ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//Check if Dialogue Line 4 has been triggered
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_Line4Done)
			IF IS_BIT_SET(playerBD[iStaggeredParticipant].sMenuData.iPlayerBitSet, biP_Line4Done)
				SET_BIT(serverBD.iServerBitSet, biS_Line4Done)
				NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - biS_Line4Done - TRUE    <----------     ") NET_NL()
			ENDIF
		ENDIF
		//Check if Dialogue Line 5 has been triggered
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_Line5Done)
			IF IS_BIT_SET(playerBD[iStaggeredParticipant].sMenuData.iPlayerBitSet, biP_Line5Done)
				SET_BIT(serverBD.iServerBitSet, biS_Line5Done)
				NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - biS_Line5Done - TRUE    <----------     ") NET_NL()
			ENDIF
		ENDIF
		
		//Check if Dialogue Line React to Gun has been triggered
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_ReactToGunLineDone)
			IF IS_BIT_SET(playerBD[iStaggeredParticipant].sMenuData.iPlayerBitSet, biP_ReactToGunLineDone)
				SET_BIT(serverBD.iServerBitSet, biS_ReactToGunLineDone)
				NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - biS_ReactToGunLineDone - TRUE    <----------     ") NET_NL()
			ENDIF
		ENDIF
		//Check if Dialogue Line React to Shout has been triggered
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_ReactToShoutLineDone)
			IF IS_BIT_SET(playerBD[iStaggeredParticipant].sMenuData.iPlayerBitSet, biP_ReactToShoutLineDone)
				SET_BIT(serverBD.iServerBitSet, biS_ReactToShoutLineDone)
				NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - biS_ReactToShoutLineDone - TRUE    <----------     ") NET_NL()
			ENDIF
		ENDIF
		//Check if Dialogue Line React to Gun Shot has been triggered
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_ReactToGunShotLineDone)
			IF IS_BIT_SET(playerBD[iStaggeredParticipant].sMenuData.iPlayerBitSet, biP_ReactToGunShotLineDone)
				SET_BIT(serverBD.iServerBitSet, biS_ReactToGunShotLineDone)
				NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - biS_ReactToGunShotLineDone - TRUE    <----------     ") NET_NL()
			ENDIF
		ENDIF
		//Check if Dialogue Line React to Wanted Level has been triggered
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_ReactToWantedLevelLineDone)
			IF IS_BIT_SET(playerBD[iStaggeredParticipant].sMenuData.iPlayerBitSet, biP_ReactToWantedLevelLineDone)
				SET_BIT(serverBD.iServerBitSet, biS_ReactToWantedLevelLineDone)
				NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - biS_ReactToWantedLevelLineDone - TRUE    <----------     ") NET_NL()
			ENDIF
		ENDIF
		//Check if Dialogue Line React to Unarmed Aim has been triggered
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_ReactToUnarmedAimLineDone)
			IF IS_BIT_SET(playerBD[iStaggeredParticipant].sMenuData.iPlayerBitSet, biP_ReactToUnarmedAimLineDone)
				SET_BIT(serverBD.iServerBitSet, biS_ReactToUnarmedAimLineDone)
				NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - biS_ReactToUnarmedAimLineDone - TRUE    <----------     ") NET_NL()
			ENDIF
		ENDIF
		//Check if Dialogue Line React to Cop Arrival has been triggered
		IF NOT IS_BIT_SET(serverBD.iServerBitSet2, biS2_ReactToCopArrival)
			IF IS_BIT_SET(playerBD[iStaggeredParticipant].sMenuData.iPlayerBitSet, biP_ReactToCopArrival)
				SET_BIT(serverBD.iServerBitSet2, biS2_ReactToCopArrival)
				NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - biS2_ReactToCopArrival - TRUE    <----------     ") NET_NL()
			ENDIF
		ENDIF
		//Check if Dialogue Line React to Snack has been triggered
		IF NOT IS_BIT_SET(serverBD.iServerBitSet2, biS2_ReactToSnackLineDone)
			IF IS_BIT_SET(playerBD[iStaggeredParticipant].sMenuData.iPlayerBitSet, biP_ReactToSnackLineDone)
				SET_BIT(serverBD.iServerBitSet2, biS2_ReactToSnackLineDone)
				NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - biS2_ReactToSnackLineDone - TRUE    <----------     ") NET_NL()
			ENDIF
		ENDIF
		
		//Check if Dialogue Line React to shoplift has been triggered
		IF NOT IS_BIT_SET(serverBD.sMenuData.iServerSnackBitSet, biSS_ReactToLiftLineDone)
			IF IS_BIT_SET(playerBD[iStaggeredParticipant].sMenuData.iPlayerSnackBitSet, biPS_ReactToLiftDone)
				SET_BIT(serverBD.sMenuData.iServerSnackBitSet, biSS_ReactToLiftLineDone)
				NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - biSS_ReactToLiftLineDone - TRUE    <----------     ") NET_NL()
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(serverBD.iServerHeroBitSet, biSH_GotHero)
			//Check if Hero Warning Line has been done
			IF NOT IS_BIT_SET(serverBD.iServerHeroBitSet, biSH_WarningLineDone)
				IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerHeroBitSet, biPH_WarningLineDone)
					SET_BIT(serverBD.iServerHeroBitSet, biSH_WarningLineDone)
					NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - biSH_WarningLineDone - TRUE    <----------     ") NET_NL()
				ENDIF
			ENDIF
			//Check if Hero Attack Line has been done
			IF NOT IS_BIT_SET(serverBD.iServerHeroBitSet, biSH_AttackLineDone)
				IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerHeroBitSet, biPH_AttackLineDone)
					SET_BIT(serverBD.iServerHeroBitSet, biSH_AttackLineDone)
					NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - biSH_AttackLineDone - TRUE    <----------     ") NET_NL()
				ENDIF
			ENDIF
			//Check if Hero has been made to turn
			IF NOT IS_BIT_SET(serverBD.iServerHeroBitSet, biSH_TurnForWarning)
				IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerHeroBitSet, biPH_TurnForWarning)
					SET_BIT(serverBD.iServerHeroBitSet, biSH_TurnForWarning)
					NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - biSH_TurnForWarning - TRUE    <----------     ") NET_NL()
				ENDIF
			ENDIF
		ENDIF
		
		//Check if Cash Bag has been dropped with money
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_CashBagAvailableForPickup)
			IF IS_BIT_SET(playerBD[iStaggeredParticipant].sMenuData.iPlayerBitSet, biP_CashBagAvailbleForPickup)
				SET_BIT(serverBD.iServerBitSet, biS_CashBagAvailableForPickup)
				NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - biS_CashBagAvailableForPickup - TRUE    <----------     ") NET_NL()
			ENDIF
		ENDIF
		
		//Check if Cash Bag has been set so it can be picked up
		IF NOT IS_BIT_SET(serverBD.iServerBitSet2, biS2_CashBagPreventCollectionCleared)
			IF IS_BIT_SET(playerBD[iStaggeredParticipant].sMenuData.iPlayerBitSet, biP_CashBagPreventCollectionCleared)
				SET_BIT(serverBD.iServerBitSet2, biS2_CashBagPreventCollectionCleared)
				NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - biS2_CashBagPreventCollectionCleared - TRUE    <----------     ") NET_NL()
			ENDIF
		ENDIF
		
		//Check who has control of Shopkeeper
		/*IF NOT IS_NET_PED_INJURED(serverBD.sMenuData.niRegStaff)
			IF IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.sMenuData.niRegStaff)
				IF serverBD.PlayerInControlOfStaff != PlayerPedId
					serverBD.PlayerInControlOfStaff = PlayerPedId
					NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - PlayerInControlOfStaff = ") NET_PRINT(GET_PLAYER_NAME(PLAYER_ID())) NET_NL()
				ENDIF
			ENDIF
		ENDIF*/
		
		IF IS_NET_PLAYER_OK(PlayerId)
			//Check if Any of the Crew has Damaged the Staff
			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_AnyCrewHasDamagedStaff)
				IF NOT IS_NET_PED_INJURED(serverBD.sMenuData.niRegStaff)
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(NET_TO_PED(serverBD.sMenuData.niRegStaff), PlayerPedId)
						SET_BIT(serverBD.iServerBitSet, biS_AnyCrewHasDamagedStaff)
						NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - biS_AnyCrewHasDamagedStaff - TRUE    <----------     ") NET_NL()
						#IF IS_DEBUG_BUILD NET_LOG_Host("A CROOK HAS DAMAGED STAFF")	#ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//Hero Checks
			IF IS_BIT_SET(serverBD.iServerHeroBitSet, biSH_GotHero)
				//Check if Any of the Crew has Damaged the Hero
				IF NOT IS_BIT_SET(serverBD.iServerHeroBitSet, biSH_AnyCrewHasDamagedHero)
					IF NOT IS_NET_PED_INJURED(serverBD.niHero)
						IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(NET_TO_PED(serverBD.niHero), PlayerPedId)
							SET_BIT(serverBD.iServerHeroBitSet, biSH_AnyCrewHasDamagedHero)
							NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - biSH_AnyCrewHasDamagedHero - TRUE    <----------     ") NET_NL()
							#IF IS_DEBUG_BUILD NET_LOG_Host("A CROOK HAS DAMAGED HERO")	#ENDIF
						ENDIF
					ENDIF
				ENDIF
				//Check if Any of the Crew has Aimed at the Hero
				IF NOT IS_BIT_SET(serverBD.iServerHeroBitSet, biSH_AnyCrewHasAimedAtHero)
					IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerHeroBitSet, biPH_PlayerHasAimedAtHero)
						SET_BIT(serverBD.iServerHeroBitSet, biSH_AnyCrewHasAimedAtHero)
						NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - biSH_AnyCrewHasAimedAtHero - TRUE    <----------     ") NET_NL()
					ENDIF
				ENDIF
			ENDIF
			
			//Check if Any of the Crew has Demanded the Cash
			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_AnyCrewHasDemandedCash)
				IF IS_BIT_SET(playerBD[iStaggeredParticipant].sMenuData.iPlayerBitSet, biP_PlayerHasDemandedCash)
					SET_BIT(serverBD.iServerBitSet, biS_AnyCrewHasDemandedCash)
					NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - biS_AnyCrewHasDemandedCash - TRUE    <----------     ") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG_Host("A CROOK HAS DEMANDED CASH")	#ENDIF
				ENDIF
			ENDIF 
						
			//Check if Any of the Crew has Aimed at the Staff
			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_AnyCrewHasAimedAtStaff)
				IF IS_BIT_SET(playerBD[iStaggeredParticipant].sMenuData.iPlayerBitSet, biP_PlayerHasAimedAtStaff)
					SET_BIT(serverBD.iServerBitSet, biS_AnyCrewHasAimedAtStaff)
					NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - biS_AnyCrewHasAimedAtStaff - TRUE    <----------     ") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG_Host("A CROOK HAS AIMED AT STAFF")	#ENDIF
				ENDIF
			ENDIF
			//Check if Any of the Crew has Unarmed Aimed at the Staff
			IF NOT IS_BIT_SET(serverBD.iServerBitSet2, biS2_AnyCrewHasUnarmedAimedAtStaff)
				IF IS_BIT_SET(playerBD[iStaggeredParticipant].sMenuData.iPlayerBitSet, biP_PlayerHasUnarmedAimedAtStaff)
				//IF IS_PED_IN_MELEE_COMBAT(PlayerPedId)
					SET_BIT(serverBD.iServerBitSet2, biS2_AnyCrewHasUnarmedAimedAtStaff)
					NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - biS_AnyCrewHasUnarmedAimedAtStaff - TRUE    <----------     ") NET_NL()
				ENDIF
			ENDIF 
			
			//Check if Any of the Crew has drawn a weapon
			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_StaffReactToWeaponDone)
				IF IS_BIT_SET(playerBD[iStaggeredParticipant].sMenuData.iPlayerBitSet, biP_StaffReactToWeaponDone)
					//serverBD.iAggressivePlayer = iStaggeredParticipant
					SET_BIT(serverBD.iServerBitSet, biS_StaffReactToWeaponDone)
					NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - biS_StaffReactToWeaponDone - TRUE    <----------     ") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG_Host("STAFF REACTION DONE")	#ENDIF
				ENDIF
			ENDIF
			
			//Check if Any of the Crew has Created the Cash Pickup
			/*IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_CashPickupCreated)
				IF IS_BIT_SET(playerBD[iStaggeredParticipant].sMenuData.iPlayerBitSet, biP_CashPickupCreated)
					SET_BIT(serverBD.iServerBitSet, biS_CashPickupCreated)
					NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - biS_CashPickupCreated - TRUE    <----------     ") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG_Host("CASH PICKUP CREATED")	#ENDIF
				ENDIF
			ENDIF*/
			
//			//Check if Any of the Crew has Started the Sync Scene
//			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_SyncSceneStarted)
//				IF (MPGlobalsAmbience.bHoldUpTriggeredEmptyReg = TRUE OR IS_BIT_SET(serverBD.iServerBitSet, biS_AnyCrewHasDemandedCash) OR IS_BIT_SET(playerBD[iStaggeredParticipant].sMenuData.iPlayerBitSet, biP_SyncSceneStarted))
//				AND TAKE_CONTROL_OF_NET_ID(serverBD.niCashBag)
//				AND NOT IS_NET_PED_INJURED(serverBD.sMenuData.niRegStaff)
//				AND serverBD.sMenuData.eStaffState != eSTAFF_FLEE_TO_BACK_ROOM
//				AND serverBD.sMenuData.eStaffState != eSTAFF_ATTACK
//					//IF NOT IS_ENTITY_PLAYING_ANIM(NET_TO_PED(serverBD.sMenuData.niRegStaff), "mp_am_hold_up", "handsup_base")
//					//	IF GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.sMenuData.niRegStaff), SCRIPT_TASK_PERFORM_SEQUENCE) != WAITING_TO_START_TASK
//					//	AND GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.sMenuData.niRegStaff), SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
//					//		TASK_PERFORM_SEQUENCE(NET_TO_PED(serverBD.sMenuData.niRegStaff), StaffHandsUpSeq)
//					//		NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - BEFORE SYNC SCENE MAKE SURE STAFF HAS HANDS UP    <----------     ") NET_NL()
//					//	ENDIF
//					//ELSE
//					
//				ENDIF
//			ELSE
//				IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_SyncSceneStartedRunning)
//				AND TAKE_CONTROL_OF_NET_ID(serverBD.niCashBag)
//					iLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(serverBD.iSceneID)
//					IF iLocalSceneID >= 0
//						IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSceneID)
//							IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niCashBag)
//								SET_ENTITY_VISIBLE(NET_TO_OBJ(serverBD.niCashBag), TRUE)
//								NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - MAKE CASH BAG VISIBLE") NET_NL()
//							ENDIF
//							SET_BIT(serverBD.iServerBitSet, biS_SyncSceneStartedRunning)
//							NET_PRINT_TIME() NET_PRINT("[AM HOLD UP]    -    SYNCHRONISED SCENE    -    biS_SyncSceneStartedRunning SET    <----------     ") NET_NL()
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
			
			//Check if Any of the Crew has made the Cash Bag Drop Early
			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_CashBagDroppedEarly)
				IF IS_BIT_SET(playerBD[iStaggeredParticipant].sMenuData.iPlayerBitSet, biP_CashBagDroppedEarly)
					//Move Staff to Hands Up Stage
					IF serverBD.sMenuData.eStaffState != eSTAFF_HANDS_UP
					AND serverBD.sMenuData.eStaffState != eSTAFF_HANDS_UP_LOOP_ONLY
						serverBD.sMenuData.eStaffState = eSTAFF_HANDS_UP
					ENDIF
					SET_BIT(serverBD.iServerBitSet, biS_CashBagDroppedEarly)
					NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - biS_CashBagDroppedEarly - TRUE    <----------     ") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG_Host("CASH BAG DROPPED EARLY")	#ENDIF
				ENDIF
			ENDIF
			
			//check snack shoplift here
			// This is used only once for the dialog line.
			IF NOT IS_BIT_SET(serverBD.sMenuData.iServerSnackBitSet, biSS_SnackShopLiftCheck)
				IF IS_BIT_SET(playerBD[iStaggeredParticipant].sMenuData.iPlayerSnackBitSet, biPS_ReactToShopLift)
					SET_BIT(serverBD.sMenuData.iServerSnackBitSet, biSS_SnackShopLiftCheck)
					SET_BIT(serverBD.sMenuData.iServerSnackBitSet, biSS_ShopLiftInterruptSnackBuy)
					iSnackShoplifter = iStaggeredParticipant
					NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - biSS_SnackShopLiftCheck - TRUE    <----------     ") NET_NL()
				ENDIF
			ELSE
				IF NOT IS_BIT_SET(playerBD[iSnackShoplifter].sMenuData.iPlayerSnackBitSet, biPS_ReactToShopLift)
					CLEAR_BIT(serverBD.sMenuData.iServerSnackBitSet, biSS_SnackShopLiftCheck)
					NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - biSS_SnackShopLiftCheck - FALSE    <----------     ") NET_NL()
					IF IS_BIT_SET(serverBD.sMenuData.iServerSnackBitSet, biSS_ReactToLiftLineDone)
						CLEAR_BIT(serverBD.sMenuData.iServerSnackBitSet, biSS_ReactToLiftLineDone)
						NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - biSS_ReactToLiftLineDone - FALSE    <----------     ") NET_NL()
					ENDIF
				ENDIF
			ENDIF
			
			//check snack buy here
			// This is used only once for the dialog line.  use a different flag to trigger anims below
			IF NOT IS_BIT_SET(serverBD.iServerBitSet2, biS2_SnackBuyCheck)
				IF IS_BIT_SET(playerBD[iStaggeredParticipant].sMenuData.iPlayerBitSet, biP_ReactToSnackBuy)
					SET_BIT(serverBD.iServerBitSet2, biS2_SnackBuyCheck)
					NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - biS2_SnackBuyCheck - TRUE    <----------     ") NET_NL()
				ENDIF
			ENDIF
			
			//snack anim flag
			IF NOT IS_BIT_SET(serverBD.sMenuData.iServerSnackBitSet, biSS_SnackBought)
				IF IS_BIT_SET(playerBD[iStaggeredParticipant].sMenuData.iPlayerSnackBitSet, biPS_SnackBought)
					SET_BIT(serverBD.sMenuData.iServerSnackBitSet, biSS_SnackBought)
					SET_BIT(serverBD.sMenuData.iServerSnackBitSet, biSS_SnackRequestAnim)
					serverBD.iServerWhichSnack = playerBD[iStaggeredParticipant].sMenuData.iPlayerWhichSnack
					iSnackOrderer = iStaggeredParticipant
					NET_PRINT_TIME() NET_PRINT("[AM HOLD UP]      ---------->     snack_anims - biSS_SnackBought - TRUE    <----------     ") NET_NL()
				ENDIF
			ELSE
				IF NOT IS_BIT_SET(playerBD[iSnackOrderer].sMenuData.iPlayerSnackBitSet, biPS_SnackBought)
					CLEAR_BIT(serverBD.sMenuData.iServerSnackBitSet, biSS_SnackBought)
					NET_PRINT_TIME() NET_PRINT("[AM HOLD UP]      ---------->     snack_anims - biSS_SnackBought - FALSE    <----------     ") NET_NL()
				ENDIF
			ENDIF
			
			//Restricted Area Check
			IF NOT IS_BIT_SET(serverBD.iServerBitSet2, biS2_RestrictedAreaCheck)
				IF IS_BIT_SET(playerBD[iStaggeredParticipant].sMenuData.iPlayerBitSet, biP_RestrictedAreaEntered)
					SET_BIT(serverBD.iServerBitSet2, biS2_RestrictedAreaCheck)
					NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - biS2_RestrictedAreaCheck - TRUE    <----------     ") NET_NL()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//Increment counter
	iStaggeredParticipant++
	
	//End of Staggered Taxi Loop
	IF iStaggeredParticipant = NETWORK_GET_MAX_NUM_PARTICIPANTS()
		iStaggeredParticipant = 0
	ENDIF
ENDPROC


//PURPOSE: Runs through the max num participents and does relevant checks
PROC MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER()
	INT iParticipant
	WEAPON_TYPE CurrentWeapon
	//FLOAT fParticipantShout
	BOOL bInMain
	BOOL bInBackRoom
	INT iNumPlayerInStoreLocal
	
	//Set Bits that can then be cleared in the repeat if necessary
	SET_BIT(serverBD.iServerBitSet, biS_AllCrewDead)
	//SET_BIT(serverBD.iServerBitSet, biS_AllCrewArrested)
	//CLEAR_BIT(serverBD.iServerBitSet, biS_AnyCrewArrested)
	SET_BIT(serverBD.iServerBitSet, biS_AllCrewLeftArea)
	CLEAR_BIT(serverBD.iServerBitSet, biS_AnyCrewInShop)
	CLEAR_BIT(serverBD.iServerBitSet, biS_AnyCrewInBackRoom)
	CLEAR_BIT(serverBD.iServerBitSet, biS_AnyCrewAtEntrance)
	CLEAR_BIT(serverBD.iServerBitSet2, biS2_AnyCrewIsAimingAtStaff)
	//serverBD.iNumPlayersInStore = 0
	
	//NON LOOP CHECKS
	//Check if Staff has been killed before bag has had any money put in
	IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_StaffDeadAndBagEmpty)
		IF IS_NET_PED_INJURED(serverBD.sMenuData.niRegStaff)
			IF serverBD.fCashBagFillAmount <= 0.0
				SET_BIT(serverBD.iServerBitSet, biS_StaffDeadAndBagEmpty)
				NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - biS_StaffDeadAndBagEmpty - TRUE    <----------     ") NET_NL()
			ENDIF
		ENDIF
	ENDIF
	
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
			PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
			PED_INDEX PlayerPedId = GET_PLAYER_PED(PlayerId)
			
			//** CHECKS THAT DON'T NEED THE PLAYER TO BE ALIVE **//
			
			IF playerBD[iParticipant].sMenuData.iCashSpent > 0
				serverBD.sMenuData.iTotalCashSpent += playerBD[iParticipant].sMenuData.iCashSpent
				NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - CASH SPENT - ADD $") NET_PRINT_INT(playerBD[iParticipant].sMenuData.iCashSpent) NET_PRINT(" from ") NET_PRINT(GET_PLAYER_NAME(PlayerId)) NET_PRINT(" iTotalCashSpent = $") NET_PRINT_INT(serverBD.sMenuData.iTotalCashSpent) NET_NL()
				playerBD[iParticipant].sMenuData.iCashSpent = 0
			ENDIF
					
			//Check if all Crooks on the mission have left the area
			IF IS_BIT_SET(serverBD.iServerBitSet, biS_AllCrewLeftArea)
				IF IS_ENTITY_AT_COORD(PlayerPedId, vDoorCoord, <<30, 30, 30>>)
				//OR NETWORK_IS_IN_TUTORIAL_SESSION()
					CLEAR_BIT(serverBD.iServerBitSet, biS_AllCrewLeftArea)
				ENDIF
			ENDIF
			
			//Check if Any Crew has Robbed the Register Manually
			IF NOT IS_BIT_SET(serverBD.iServerBitSet2, biS2_RegisterRobberManually)
				IF IS_BIT_SET(playerBD[iParticipant].sMenuData.iPlayerBitSet, biP_RobbedRegisterManually)
					SET_BIT(serverBD.iServerBitSet2, biS2_RegisterRobberManually)
					NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - biS2_RegisterRobberManually - TRUE    <----------     ") NET_NL()
				ENDIF
			ENDIF
			
			//MOVED INTO STAGGERED - MARKER A
			
			//** CHECKS THAT REQUIRE THE PLAYER TO BE ALIVE **//
			IF IS_NET_PLAYER_OK(PlayerId)
				
				IF IS_BIT_SET(serverBD.iServerBitSet, biS_AllCrewDead)
					CLEAR_BIT(serverBD.iServerBitSet, biS_AllCrewDead) 
				ENDIF
				
				//IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_AnyCrewArrested)
				//	SET_BIT(serverBD.iServerBitSet, biS_AnyCrewArrested)
				//ENDIF
				
				//bInBackRoom = IS_ENTITY_IN_ANGLED_AREA(PlayerPedId, vBackRoomMin, vBackRoomMax, fBackRoomWidth)
				IF GET_INTERIOR_FROM_ENTITY(PlayerPedId) = interiorID
					IF GET_ROOM_KEY_FROM_ENTITY(PlayerPedId) = iRoomHashMain
					OR GET_ROOM_KEY_FROM_ENTITY(PlayerPedId) = iRoomHashMain2
						bInMain = TRUE
					ELIF GET_ROOM_KEY_FROM_ENTITY(PlayerPedId) = iRoomHashBack
						SET_BIT(serverBD.iServerBitSet, biS_AnyCrewInBackRoom)
						bInBackRoom = TRUE
					ENDIF
				ENDIF
				
				//Check if Any Crew are in the Shop
				//IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_AnyCrewInShop)
//					IF IS_ENTITY_IN_ANGLED_AREA(PlayerPedId, vStoreMin, vStoreMax, fStoreWidth)
//					OR IN_EXTRA_MAIN_AREA(PlayerPedId, shopSnacks.NearestHoldUpPoint)
					IF bInMain 		= TRUE
					OR bInBackRoom 	= TRUE
						SET_BIT(serverBD.iServerBitSet, biS_AnyCrewInShop)
						iNumPlayerInStoreLocal++
						
						IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_SyncSceneStarted)
						AND NOT IS_BIT_SET(serverBD.iServerBitSet, biS_StaffDead)
							//Check if Any of the Crew has drawn their weapon in the Shop
							IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_AnyCrewHasWeaponDrawn)
								IF GET_CURRENT_PED_WEAPON(PlayerPedId, CurrentWeapon)
									IF CurrentWeapon != WEAPONTYPE_UNARMED
									AND CurrentWeapon != WEAPONTYPE_INVALID
									AND CurrentWeapon != WEAPONTYPE_OBJECT
									AND CurrentWeapon != WEAPONTYPE_FLARE
									AND CurrentWeapon != WEAPONTYPE_DLC_SNOWBALL
										IF serverBD.iAggressivePlayer = -1
											serverBD.iAggressivePlayer = iParticipant
										ELSE
											IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(serverBD.iAggressivePlayer))
												serverBD.iAggressivePlayer = -1
											ENDIF
										ENDIF
										
										SET_BIT(serverBD.iServerBitSet, biS_AnyCrewHasWeaponDrawn)
										NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - biS_AnyCrewHasWeaponDrawn - TRUE    <----------     ") NET_NL()
										
										IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_AnyCrewHasWeaponDrawnNearStaff)
											IF IS_ENTITY_AT_COORD(PlayerPedId, vRegStaff, <<2.5, 2.5, 3>>)
												SET_BIT(serverBD.iServerBitSet, biS_AnyCrewHasWeaponDrawnNearStaff)
												NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - biS_AnyCrewHasWeaponDrawnNearStaff - TRUE    <----------     ") NET_NL()
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							//MOVED INTO ABOVE CHECKS
							//Check if Any of the Crew has drawn their weapon close to the Staff
							/*IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_AnyCrewHasWeaponDrawnNearStaff)
								IF IS_ENTITY_AT_COORD(PlayerPedId, vRegStaff, <<2.5, 2.5, 3>>)
									IF GET_CURRENT_PED_WEAPON(PlayerPedId, CurrentWeapon)
										IF CurrentWeapon != WEAPONTYPE_UNARMED
										AND CurrentWeapon != WEAPONTYPE_OBJECT
										AND CurrentWeapon != WEAPONTYPE_FLARE
											SET_BIT(serverBD.iServerBitSet, biS_AnyCrewHasWeaponDrawnNearStaff)
											NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - biS_AnyCrewHasWeaponDrawnNearStaff - TRUE    <----------     ") NET_NL()
											#IF IS_DEBUG_BUILD NET_LOG_Host("A CROOK HAS DRAWN WEAPON NEAR STAFF")	#ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF*/
							
							//Check if Any Crew with a Wanted Level are in the Shop
							IF NOT IS_BIT_SET(serverBD.iServerBitSet2, biS2_AnyCrewWithWantedLevelInShop)
								IF GET_PLAYER_WANTED_LEVEL(PlayerId) > 0
									SET_BIT(serverBD.iServerBitSet2, biS2_AnyCrewWithWantedLevelInShop)
									NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - biS2_AnyCrewWithWantedLevelInShop - TRUE    <----------     ") NET_NL()
								ENDIF
							ENDIF
						ENDIF
				//	ENDIF
				//ENDIF
				
				//Check if Any Crew are in the Back Room
				//IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_AnyCrewInBackRoom)
				/*	ELIF IS_ENTITY_IN_ANGLED_AREA(PlayerPedId, vBackRoomMin, vBackRoomMax, fBackRoomWidth)
						SET_BIT(serverBD.iServerBitSet, biS_AnyCrewInBackRoom)
						iNumPlayerInStoreLocal++*/
				//	ENDIF
				//ENDIF
				
				//Check if Any Crew are at the entrance
				//IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_AnyCrewAtEntrance)
					ELIF IS_ENTITY_AT_COORD(PlayerPedId, vDoorCoord, <<HOLD_UP_ENTRANCE_RANGE, HOLD_UP_ENTRANCE_RANGE, HOLD_UP_ENTRANCE_RANGE_HEIGHT>>)
						SET_BIT(serverBD.iServerBitSet, biS_AnyCrewAtEntrance)
						iNumPlayerInStoreLocal++
					ENDIF
				//ENDIF
				
				//Check if Any Crew member is aiming at the Staff ped
				IF NOT IS_BIT_SET(serverBD.iServerBitSet2, biS2_AnyCrewIsAimingAtStaff)
					IF IS_BIT_SET(playerBD[iParticipant].sMenuData.iPlayerBitSet, biP_PlayerIsAimingAtStaff)
						SET_BIT(serverBD.iServerBitSet2, biS2_AnyCrewIsAimingAtStaff)
						//NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - biS2_AnyCrewIsAimingAtStaff - TRUE    <----------     ") NET_NL()
					ENDIF
				ENDIF
				
				//Get the Loudest Player Shout
				/*IF IS_BIT_SET(serverBD.iServerBitSet, biS_SyncSceneStarted)
					fParticipantShout = NETWORK_GET_PLAYER_LOUDNESS(PlayerId)
					IF fParticipantShout > serverBD.fCurrentShout
						serverBD.fCurrentShout = fParticipantShout
						RESET_NET_TIMER(serverBD.iShoutDecreaseTimer)	//serverBD.iShoutDecreaseTimer = (GET_THE_NETWORK_TIMER()+SHOUT_DECREASE_DELAY)
					ENDIF
				ENDIF*/
				
				//Get the Loudest Player Shout
				IF IS_BIT_SET(serverBD.iServerBitSet, biS_SyncSceneStarted)
				AND NOT IS_BIT_SET(serverBD.iServerBitSet, biS_CashBagAvailableForPickup)
				AND NOT IS_BIT_SET(serverBD.iServerBitSet, biS_CashBagDroppedAndEmpty)
					IF playerBD[iStaggeredParticipant].fShout > serverBD.fCurrentShout
						serverBD.fCurrentShout = playerBD[iParticipant].fShout
						//serverBD.fCurrentShout += 0.25
						RESET_NET_TIMER(serverBD.iShoutDecreaseTimer)
						IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_ReactToShoutLineTrigger)
							IF serverBD.fCurrentShout > (0.55 + 0.2)
								SET_BIT(serverBD.iServerBitSet, biS_ReactToShoutLineTrigger)
								NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - biS_ReactToShoutLineTrigger - TRUE    <----------     ") NET_NL()
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
			//ELSE
			//	CLEAR_BIT(serverBD.iServerBitSet, biS_AllCrewArrested)
			ENDIF
			
		ENDIF
	ENDREPEAT
	
	//Update if necessary
	IF serverBD.iNumPlayersInStore != iNumPlayerInStoreLocal
		PRINTLN("[AM HOLD UP] - UPDATED serverBD.iNumPlayersInStore - FROM ", serverBD.iNumPlayersInStore, " TO ", iNumPlayerInStoreLocal)
		serverBD.iNumPlayersInStore = iNumPlayerInStoreLocal
	ENDIF
ENDPROC


//BOOL bCreatePed


PROC MAINTAIN_DROP_HOLDUP_CASH_UPON_PLAYER_DEATH()
	IF NOT IS_NET_PLAYER_OK(PLAYER_ID(), TRUE, TRUE)	//FALSE, TRUE)
		PRINTLN("[AM HOLD UP][CASH] - MAINTAIN_DROP_HOLDUP_CASH_UPON_PLAYER_DEATH - Player has died")
		BOOL bDoCleanup = TRUE
		IF IS_BIT_SET(shopSnacks.iBoolsBitSet2, bi2CashGrabbed)
		AND NOT IS_BIT_SET(shopSnacks.iBoolsBitSet2, bi2CashHasBeenDropped)
			PRINTLN("[AM HOLD UP][CASH] - MAINTAIN_DROP_HOLDUP_CASH_UPON_PLAYER_DEATH - bi2CashGrabbed is set")
			INT iPlayerCash = NETWORK_GET_VC_WALLET_BALANCE()
			INT iPlacementFlags = 0
			INT iDifference
			SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_UPRIGHT))
			SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ORIENT_TO_GROUND))
			SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_FIXED))
			
			IF iEarnedCash > g_sMPTunables.iHoldUpCashRewardCap
				iEarnedCash = g_sMPTunables.iHoldUpCashRewardCap
				NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - PLAYER NOT OKAY - CASH TO DROP ABOVE LIMIT SO CAPPED AT $") NET_PRINT_INT(iEarnedCash) NET_NL()
			ENDIF
			
			IF iPlayerCash < iEarnedCash
				PRINTLN("[AM HOLD UP][CASH] - MAINTAIN_DROP_HOLDUP_CASH_UPON_PLAYER_DEATH - iPlayerCash < iEarnedCash")
				IF iPlayerCash > 0
					PRINTLN("[AM HOLD UP][CASH] - MAINTAIN_DROP_HOLDUP_CASH_UPON_PLAYER_DEATH - iPlayerCash > 0 - iPlayerCash = ", iPlayerCash, "; iEarnedCash = ", iEarnedCash)
					IF REQUEST_LOAD_MODEL(GET_HOLD_UP_MODEL(HUP_MODEL_CASH_BAG))							
						IF NETWORK_CAN_SPEND_MONEY2(iPlayerCash, FALSE, FALSE, FALSE, iDifference)	
							IF USE_SERVER_TRANSACTIONS()
								IF NOT IS_BIT_SET(MPGlobalsAmbience.iHoldUpGeneralBitSet, biG_HoldUpCashDropTransactionRequested)
									MPGlobalsAmbience.iHoldupCashToDrop = iPlayerCash
									MPGlobalsAmbience.iHoldupCashPlacementFlags = iPlacementFlags
									MPGlobalsAmbience.vHoldupDropCashCoords = GET_DEAD_PED_PICKUP_COORDS(PLAYER_PED_ID())
									NETWORK_REQUEST_CASH_TRANSACTION(MPGlobalsAmbience.iHoldupDropCashTransactionIndex, NET_SHOP_TTYPE_SERVICE, NET_SHOP_ACTION_SPEND, CATEGORY_SERVICE_WITH_THRESHOLD, SERVICE_SPEND_CASH_DROP_HOLDUP, MPGlobalsAmbience.iHoldupCashToDrop)
									PRINTLN("[AM HOLD UP][CASH] - MAINTAIN_DROP_HOLDUP_CASH_UPON_PLAYER_DEATH - NETWORK_REQUEST_CASH_TRANSACTION - SERVICE_SPEND_CASH_DROP_HOLDUP - iPlayerCash")
									SET_BIT(MPGlobalsAmbience.iHoldUpGeneralBitSet, biG_HoldUpCashDropTransactionRequested)
									iEarnedCash = 0
									PRINTLN("[AM HOLD UP][CASH] - MAINTAIN_DROP_HOLDUP_CASH_UPON_PLAYER_DEATH - Player has dropped hold up cash")
									SET_BIT(shopSnacks.iBoolsBitSet2, bi2CashHasBeenDropped)
								ENDIF
							ELSE
								CREATE_AMBIENT_PICKUP_WITH_SAP_PICKUP_ID(PICKUP_MONEY_VARIABLE, GET_DEAD_PED_PICKUP_COORDS(PLAYER_PED_ID()), iPlacementFlags, iPlayerCash, GET_HOLD_UP_MODEL(HUP_MODEL_CASH_BAG), DEFAULT, FALSE)
								GIVE_LOCAL_PLAYER_CASH_END_OF_MISSION(-iPlayerCash)
								NETWORK_SPENT_HOLDUPS(iPlayerCash)
								INT iTransactionID
								TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_SPEND_CASH_DROP_HOLDUP, iPlayerCash, iTransactionID)
								iEarnedCash = 0
								CLEAR_LAST_JOB_DATA()
								PRINTLN("[AM HOLD UP][CASH] - MAINTAIN_DROP_HOLDUP_CASH_UPON_PLAYER_DEATH - Player has dropped hold up cash")
								SET_BIT(shopSnacks.iBoolsBitSet2, bi2CashHasBeenDropped)
							ENDIF
						ELSE
							PRINTLN("[AM HOLD UP][CASH] - MAINTAIN_DROP_HOLDUP_CASH_UPON_PLAYER_DEATH - No SERVICE_SPEND_CASH_DROP_HOLDUP cash transaction has been requested - player has insufficient funds!")
							PRINTLN("[AM HOLD UP][CASH] - MAINTAIN_DROP_HOLDUP_CASH_UPON_PLAYER_DEATH - MAINTAIN_DROP_HOLDUP_CASH_UPON_PLAYER_DEATH - Cash needed for transaction: ", iDifference)
						ENDIF
					ELSE
						bDoCleanup = FALSE
					ENDIF
				ELSE
					PRINTLN("[AM HOLD UP][CASH] - MAINTAIN_DROP_HOLDUP_CASH_UPON_PLAYER_DEATH - iPlayerCash <= 0")
					PRINTLN("[AM HOLD UP][CASH] - MAINTAIN_DROP_HOLDUP_CASH_UPON_PLAYER_DEATH - iPlayerCash = ", iPlayerCash)
					PRINTLN("[AM HOLD UP][CASH] - MAINTAIN_DROP_HOLDUP_CASH_UPON_PLAYER_DEATH - iEarnedCash =", iEarnedCash)
					PRINTLN("[AM HOLD UP][CASH] - MAINTAIN_DROP_HOLDUP_CASH_UPON_PLAYER_DEATH - No SERVICE_SPEND_CASH_DROP_HOLDUP cash transaction has been requested - player has insufficient funds!")
				ENDIF
			ELSE
				PRINTLN("[AM HOLD UP][CASH] - MAINTAIN_DROP_HOLDUP_CASH_UPON_PLAYER_DEATH - iPlayerCash > iEarnedCash")
				IF iEarnedCash > 0
					PRINTLN("[AM HOLD UP][CASH] - MAINTAIN_DROP_HOLDUP_CASH_UPON_PLAYER_DEATH - iEarnedCash > 0 - iEarnedCash = ", iEarnedCash, "; iPlayerCash = ", iPlayerCash)
					IF REQUEST_LOAD_MODEL(GET_HOLD_UP_MODEL(HUP_MODEL_CASH_BAG))							
						IF NETWORK_CAN_SPEND_MONEY2(iEarnedCash, FALSE, FALSE, FALSE, iDifference)
							IF USE_SERVER_TRANSACTIONS()
								IF NOT IS_BIT_SET(MPGlobalsAmbience.iHoldUpGeneralBitSet, biG_HoldUpCashDropTransactionRequested)	
									MPGlobalsAmbience.iHoldupCashToDrop = iEarnedCash
									MPGlobalsAmbience.iHoldupCashPlacementFlags = iPlacementFlags
									MPGlobalsAmbience.vHoldupDropCashCoords = GET_DEAD_PED_PICKUP_COORDS(PLAYER_PED_ID())
									NETWORK_REQUEST_CASH_TRANSACTION(MPGlobalsAmbience.iHoldupDropCashTransactionIndex, NET_SHOP_TTYPE_SERVICE, NET_SHOP_ACTION_SPEND, CATEGORY_SERVICE_WITH_THRESHOLD, SERVICE_SPEND_CASH_DROP_HOLDUP, MPGlobalsAmbience.iHoldupCashToDrop)
									PRINTLN("[AM HOLD UP][CASH] - MAINTAIN_DROP_HOLDUP_CASH_UPON_PLAYER_DEATH - NETWORK_REQUEST_CASH_TRANSACTION - SERVICE_SPEND_CASH_DROP_HOLDUP - iEarnedCash")
									SET_BIT(MPGlobalsAmbience.iHoldUpGeneralBitSet, biG_HoldUpCashDropTransactionRequested)
									iEarnedCash = 0
									PRINTLN("[AM HOLD UP][CASH] - MAINTAIN_DROP_HOLDUP_CASH_UPON_PLAYER_DEATH - Player has dropped hold up cash")
									SET_BIT(shopSnacks.iBoolsBitSet2, bi2CashHasBeenDropped)
								ENDIF
							ELSE
								CREATE_AMBIENT_PICKUP_WITH_SAP_PICKUP_ID(PICKUP_MONEY_VARIABLE, GET_DEAD_PED_PICKUP_COORDS(PLAYER_PED_ID()), iPlacementFlags, iEarnedCash, GET_HOLD_UP_MODEL(HUP_MODEL_CASH_BAG), DEFAULT, FALSE)
								GIVE_LOCAL_PLAYER_CASH_END_OF_MISSION(-iEarnedCash)
								NETWORK_SPENT_HOLDUPS(iEarnedCash)
								INT iTransactionID
								TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_SPEND_CASH_DROP_HOLDUP, iEarnedCash, iTransactionID)
								iEarnedCash = 0
								CLEAR_LAST_JOB_DATA()
								PRINTLN("[AM HOLD UP][CASH] - MAINTAIN_DROP_HOLDUP_CASH_UPON_PLAYER_DEATH - Player has dropped hold up cash")
								SET_BIT(shopSnacks.iBoolsBitSet2, bi2CashHasBeenDropped)
							ENDIF
						ELSE
							PRINTLN("[AM HOLD UP][CASH] - MAINTAIN_DROP_HOLDUP_CASH_UPON_PLAYER_DEATH - No SERVICE_SPEND_CASH_DROP_HOLDUP cash transaction has been requested - player has insufficient funds!")
							PRINTLN("[AM HOLD UP][CASH] - MAINTAIN_DROP_HOLDUP_CASH_UPON_PLAYER_DEATH - Cash needed for transaction: ", iDifference)
						ENDIF
					ELSE
						bDoCleanup = FALSE
					ENDIF
				ELSE
					PRINTLN("[AM HOLD UP][CASH] - MAINTAIN_DROP_HOLDUP_CASH_UPON_PLAYER_DEATH - iEarnedCash <= 0")
					PRINTLN("[AM HOLD UP][CASH] - MAINTAIN_DROP_HOLDUP_CASH_UPON_PLAYER_DEATH - iPlayerCash = ", iPlayerCash)
					PRINTLN("[AM HOLD UP][CASH] - MAINTAIN_DROP_HOLDUP_CASH_UPON_PLAYER_DEATH - iEarnedCash =", iEarnedCash)
					PRINTLN("[AM HOLD UP][CASH] - MAINTAIN_DROP_HOLDUP_CASH_UPON_PLAYER_DEATH - No SERVICE_SPEND_CASH_DROP_HOLDUP cash transaction has been requested - player has insufficient funds!")
				ENDIF
			ENDIF
		ELSE
			PRINTLN("[AM HOLD UP][CASH] - MAINTAIN_DROP_HOLDUP_CASH_UPON_PLAYER_DEATH - MAINTAIN_DROP_HOLDUP_CASH_UPON_PLAYER_DEATH - bi2CashGrabbed is ***NOT*** set")
		ENDIF
		IF bDoCleanup = TRUE
			#IF IS_DEBUG_BUILD NET_LOG("CLEANUP - PLAYER NOT OKAY")	#ENDIF
			NET_PRINT_TIME() NET_PRINT_STRINGS("[AM HOLD UP] - MAINTAIN_DROP_HOLDUP_CASH_UPON_PLAYER_DEATH - PLAYER NOT OKAY - SCRIPT CLEANUP B     <----------     ", GET_MP_MISSION_NAME(shopSnacks.thisHoldupMission)) NET_NL()
			SCRIPT_CLEANUP()
		ELSE
			NET_PRINT_TIME() NET_PRINT_STRINGS("[AM HOLD UP] - MAINTAIN_DROP_HOLDUP_CASH_UPON_PLAYER_DEATH - PLAYER NOT OKAY - TRYING TO CREATE PICKUP     <----------     ", GET_MP_MISSION_NAME(shopSnacks.thisHoldupMission)) NET_NL()
			EXIT
		ENDIF
	ENDIF
ENDPROC


//*** START - PLAYER ROBS TILL MANUALLY -------------------------------------------------------------------------

ENUM PLAYER_ROB_TILL_STATE
	PLAYERROBTILL_INIT,
	PLAYERROBTILL_WAIT_ACTIVATE,
	PLAYERROBTILL_QUICKCUT,
	PLAYERROBTILL_QUICKCUT_INTERP,
	PLAYERROBTILL_INTERP_FINISH,
	PLAYERROBTILL_CLEANUP,
	PLAYERROBTILL_SKIP,
	PLAYERROBTILL_DONE
ENDENUM
PLAYER_ROB_TILL_STATE ePlayerRobTillState = PLAYERROBTILL_INIT

CAMERA_INDEX ciTill
CAMERA_INDEX ciTillInterp
BOOL bMoneyFlag
WEAPON_TYPE eCurrentWeapon
INT iUseContext
VECTOR vRobRegisterManuallyPos

PROC UPDATE_PLAYER_ROBBING_TILL()
	
	FLOAT fAnimPhase
	INT iCashGrabbed
	BOOL bHasWeapon
	
	//VEHICLE_INDEX vehVehicleToCheck
		
	//IF ePlayerRobTillState > PLAYERROBTILL_WAIT_ACTIVATE
	//AND ePlayerRobTillState < PLAYERROBTILL_CLEANUP
	//	IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED()
	//		ePlayerRobTillState = PLAYERROBTILL_SKIP
	//		CDEBUG1LN(DEBUG_SHOP_ROBBERIES,"CUTSCENE PLAYERROBTILL_SKIP")
	//	ENDIF
	//ENDIF
	
	SWITCH ePlayerRobTillState
		CASE PLAYERROBTILL_INIT
			
			IF NOT IS_BIT_SET(shopSnacks.iBoolsBitSet2, bi2GotManualRobVector)
				IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niTill)
					IF IS_VECTOR_ZERO(vRobRegisterManuallyPos)
						vRobRegisterManuallyPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(NET_TO_OBJ(serverBD.niTill), <<0, -0.5, 0>>)
						SET_BIT(shopSnacks.iBoolsBitSet2, bi2GotManualRobVector)
					ENDIF
				ENDIF
			ENDIF
			
			// let's have a check if the clerk is incapacitated and the player has not collected any money here
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vRobRegisterManuallyPos, <<0.5, 0.5, 1.0>>)
			AND (IS_NET_PED_INJURED(serverBD.sMenuData.niRegStaff) OR serverBD.sMenuData.eStaffState > eSTAFF_ATTACK)
			AND NOT IS_BIT_SET(serverBD.iServerBitSet, biS_CashBagAvailableForPickup)
			AND NOT IS_BIT_SET(serverBD.iServerBitSet, biS_CashPickedUp)
			AND GET_NUMBER_OF_FIRES_IN_RANGE(scenePosition, 1.5) = 0
				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				AND NOT IS_PHONE_ONSCREEN()
				AND NOT IS_CUSTOM_MENU_ON_SCREEN()
				AND NOT IS_PAUSE_MENU_ACTIVE()
				AND NOT IS_FM_PLAYER_CRITICAL_TO_THIS_FM_EVENT(PLAYER_ID(), FMMC_TYPE_HUNT_THE_BEAST)
				AND NOT IS_PLAYER_AN_ANIMAL(PLAYER_ID())
				AND NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niTill)
				AND (NOT IS_PLAYER_CRITICAL_TO_ANY_EVENT(PLAYER_ID()) 
				OR GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_GB_CHAL_ROB_SHOP)
					
					PRINT_HELP("FHU_MANR")
					
					//REGISTER_CONTEXT_INTENTION(iUseContext, CP_HIGH_PRIORITY, "FHU_MANR")
					
						MPGlobalsAmbience.sSmashAndGrabStruct.bManualHoldup = TRUE
					
					ePlayerRobTillState = PLAYERROBTILL_WAIT_ACTIVATE
					PRINTLN("[AM HOLD UP] ***** UPDATE_PLAYER_ROBBING_TILL - Jumping to PLAYERROBTILL_WAIT_ACTIVATE - XX")
				ENDIF
			ENDIF
		BREAK
		
		CASE PLAYERROBTILL_WAIT_ACTIVATE
			IF NOT IS_BIT_SET(serverBD.iServerBitSet2, biS2_RegisterRobberManually)
			AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_RobbedRegisterManually)
			AND NOT IS_BIT_SET(serverBD.iServerBitSet, biS_CashBagAvailableForPickup)
			AND NOT IS_BIT_SET(serverBD.iServerBitSet, biS_CashPickedUp)
			AND IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vRobRegisterManuallyPos, <<0.5, 0.5, 1.0>>)
			AND (NOT IS_PLAYER_CRITICAL_TO_ANY_EVENT(PLAYER_ID()) 
			OR GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_GB_CHAL_ROB_SHOP)
			
				IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_CONTEXT)
				AND NOT IS_PHONE_ONSCREEN_AND_RINGING()
				AND NOT IS_CUSTOM_MENU_ON_SCREEN()
				AND NOT IS_PAUSE_MENU_ACTIVE()
				//IF HAS_CONTEXT_BUTTON_TRIGGERED(iUseContext)
						
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FHU_MANR")
						CLEAR_HELP()
					ENDIF
					
					//RELEASE_CONTEXT_INTENTION(iUseContext)
					
					// award money
					//iCashGrabbed = GET_RANDOM_INT_IN_RANGE(100, 300)
					
					//ODDJOB_ENTER_CUTSCENE() //can't use wrapper because we have to display the money hud
					
					SET_PLAYER_CONTROL(GET_PLAYER_INDEX(), FALSE, SPC_REENABLE_CONTROL_ON_DEATH|SPC_ALLOW_PLAYER_DAMAGE)
					//STOP_FIRE_IN_RANGE(GET_ENTITY_COORDS(PLAYER_PED_ID()), 15)
					//SET_PAD_CAN_SHAKE_DURING_CUTSCENE(FALSE)
					//HANG_UP_AND_PUT_AWAY_PHONE()
					//SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)		
					//DISPLAY_RADAR(FALSE)
					
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].sMenuData.iPlayerBitSet, biP_RobbedRegisterManually)
					
					ePlayerRobTillState = PLAYERROBTILL_QUICKCUT
					PRINTLN("[AM HOLD UP] ***** UPDATE_PLAYER_ROBBING_TILL - Jumping from PLAYERROBTILL_WAIT_ACTIVATE to PLAYERROBTILL_QUICKCUT")
				ELSE
					IF (GET_GAME_TIMER() % 1000) < 50
						PRINTLN("[AM HOLD UP] [ShopRobberies.sc-> update robbing till] player hasn't triggered robbery yet")
						PRINTLN("[AM HOLD UP] [ShopRobberies.sc-> update robbing till] use context is currently ", iUseContext)
						IF iUseContext = 0
							PRINTLN("[AM HOLD UP] [ShopRobberies.sc-> update robbing till] re registering use context!")
							REGISTER_CONTEXT_INTENTION(iUseContext, CP_HIGH_PRIORITY, "FHU_MANR")
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FHU_MANR")
					PRINTLN("[AM HOLD UP] ***** UPDATE_PLAYER_ROBBING_TILL - CLEAR_HELP() to clear FHU_MANR (", GET_FILENAME_FOR_AUDIO_CONVERSATION("FHU_MANR"), ")")
					CLEAR_HELP()
				ENDIF
//				
//				RELEASE_CONTEXT_INTENTION(iUseContext)
//				
//				PRINTLN("***** UPDATE_PLAYER_ROBBING_TILL - Jumping back to PLAYERROBTILL_INIT")
//				ePlayerRobTillState = PLAYERROBTILL_INIT
			ENDIF
		BREAK
		
		CASE PLAYERROBTILL_QUICKCUT
			
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				VECTOR vPlayer 
				vPlayer = GET_ENTITY_COORDS(PLAYER_PED_ID())
				vRobRegisterManuallyPos.z = (vPlayer.z-1.0)
				SET_ENTITY_COORDS(PLAYER_PED_ID(), vRobRegisterManuallyPos)	//GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vRegStaff, vRegStaff.z, <<-0.75, 0.0, 0.0>>))
				SET_ENTITY_HEADING(PLAYER_PED_ID(), GET_HEADING_FROM_VECTOR_2D(vTillPos.x-vRobRegisterManuallyPos.x, vTillPos.y-vRobRegisterManuallyPos.y))	//vRegStaff.z)
				
				SEQUENCE_INDEX siTemp
				CLEAR_SEQUENCE_TASK(siTemp)
				OPEN_SEQUENCE_TASK(siTemp)
//						IF bHasWeapon
//							TASK_SWAP_WEAPON(NULL, FALSE)
//						ENDIF
					TASK_PLAY_ANIM(NULL, "oddjobs@shop_robbery@rob_till", "enter", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, 4000, AF_USE_KINEMATIC_PHYSICS)
					TASK_PLAY_ANIM(NULL, "oddjobs@shop_robbery@rob_till", "loop", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, 4000, AF_LOOPING| AF_USE_KINEMATIC_PHYSICS)
					TASK_PLAY_ANIM(NULL, "oddjobs@shop_robbery@rob_till", "exit", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, 4000, AF_USE_KINEMATIC_PHYSICS)
//						IF bHasWeapon
//							TASK_SWAP_WEAPON(NULL, TRUE)
//						ENDIF
				CLOSE_SEQUENCE_TASK(siTemp)
				TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), siTemp)
				
				SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE)
				SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisableActionMode, TRUE)
				SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_UseKinematicPhysics, TRUE)
				
				bHasWeapon = GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), eCurrentWeapon)
				IF bHasWeapon
					PRINTLN("[AM HOLD UP] ***** UPDATE_PLAYER_ROBBING_TILL - Player has weapon, will swap it out")
					SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
				ENDIF
				
				//FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_IDLE, TRUE)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
				
				IF NOT DOES_CAM_EXIST(ciTill)
					ciTill = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<0,0,0>>, <<0,0,0>>, 50, FALSE)
				ENDIF
				
				ATTACH_CAM_TO_ENTITY(ciTill, PLAYER_PED_ID(), <<-0.1878, 3.0635, 0.6800>>)
				POINT_CAM_AT_ENTITY(ciTill, PLAYER_PED_ID(), <<-0.0129, 0.0927, 0.3008>>)
				SET_CAM_FOV(ciTill, 35.00)
				SHAKE_CAM(ciTill, "HAND_SHAKE", 0.1)
				SET_CAM_ACTIVE(ciTill, TRUE)
				
				RENDER_SCRIPT_CAMS(TRUE,FALSE)
			ENDIF
			
			ePlayerRobTillState = PLAYERROBTILL_QUICKCUT_INTERP
			PRINTLN("[AM HOLD UP] ***** UPDATE_PLAYER_ROBBING_TILL - Jumping from PLAYERROBTILL_QUICKCUT to PLAYERROBTILL_QUICKCUT_INTERP")
		BREAK
		
		CASE PLAYERROBTILL_QUICKCUT_INTERP
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF NOT DOES_CAM_EXIST(ciTillInterp)
					ciTillInterp = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<0,0,0>>, <<0,0,0>>, 50, FALSE)
				ENDIF
				
				ATTACH_CAM_TO_ENTITY(ciTillInterp, PLAYER_PED_ID(), <<-1.0346, 2.9183, 0.6800>>)
				POINT_CAM_AT_ENTITY(ciTillInterp, PLAYER_PED_ID(), <<-0.0574, 0.1074, 0.3008>>)
				SET_CAM_FOV(ciTillInterp, 35.00)
				SHAKE_CAM(ciTillInterp, "HAND_SHAKE", 0.1)
				SET_CAM_ACTIVE_WITH_INTERP(ciTillInterp, ciTill, 6000)
				
				SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_UseKinematicPhysics, TRUE)
				
				SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE)
				SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisableActionMode, TRUE)
			ENDIF
			
			ePlayerRobTillState = PLAYERROBTILL_INTERP_FINISH
			PRINTLN("[AM HOLD UP] ***** UPDATE_PLAYER_ROBBING_TILL - Jumping from PLAYERROBTILL_QUICKCUT_INTERP to PLAYERROBTILL_INTERP_FINISH")
		BREAK
		
		CASE PLAYERROBTILL_INTERP_FINISH
			IF NOT IS_CAM_INTERPOLATING(ciTillInterp)
				PRINTLN("[AM HOLD UP] ***** UPDATE_PLAYER_ROBBING_TILL - interp done, Jumping from PLAYERROBTILL_INTERP_FINISH to PLAYERROBTILL_CLEANUP")
								
				ePlayerRobTillState = PLAYERROBTILL_CLEANUP
			ELSE
				
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "oddjobs@shop_robbery@rob_till", "loop")
						fAnimPhase = GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), "oddjobs@shop_robbery@rob_till", "loop")
						
						IF ((fAnimPhase > 0.374 AND fAnimPhase <= 0.484)
						OR (fAnimPhase > 0.824 AND fAnimPhase <= 0.920))
							IF NOT bMoneyFlag
						
									INT iMinManualCash
									INT iMaxManualCash
									iMinManualCash = ROUND((iMinCash*0.75)/4)
									iMaxManualCash = ROUND((iMaxCash*0.75)/4)
									iCashGrabbed = GET_RANDOM_INT_IN_RANGE(iMinManualCash, iMaxManualCash+1)
									iCashGrabbed = MULTIPLY_CASH_BY_TUNABLE(iCashGrabbed)
									
									IF iCashGrabbed > (g_sMPTunables.iHoldUpCashRewardCap/4)
										iCashGrabbed = (g_sMPTunables.iHoldUpCashRewardCap/4)
										NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - CASH GRABBED - CASH ABOVE LIMIT SO CAPPED AT $") NET_PRINT_INT(iCashGrabbed) NET_NL()
									ENDIF
									
									//On an FM event where the wanted is supressed
									IF FM_EVENT_IS_PLAYER_ON_ANY_FM_EVENT(PLAYER_ID())
									AND GET_WANTED_OVERRIDE_BASED_ON_TYPE(FM_EVENT_GET_PLAYER_CURRENT_FM_EVENT(PLAYER_ID())) < 1.0
										IF g_sMPTunables.fHold_Up_Event_Mult > 1.0
											g_sMPTunables.fHold_Up_Event_Mult = 1.0
										ENDIF
										PRINTLN("[AM HOLD UP] - g_sMPTunables.fHold_Up_Event_Mult - CASH ABOVE LIMIT SO CAPPED AT ", g_sMPTunables.fHold_Up_Event_Mult)
										iCashGrabbed = ROUND(TO_FLOAT(iCashGrabbed) * g_sMPTunables.fHold_Up_Event_Mult)
										PRINTLN("[AM HOLD UP] - iCashGrabbed *= g_sMPTunables.fHold_Up_Event_Mult  =  ", iCashGrabbed)
									ENDIF
									
	//								#IF FEATURE_GANG_BOSS
	//								IF NOT GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID())
	//								AND GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) != FMMC_TYPE_GB_CHAL_ROB_SHOP
	//									PRINTLN("[AM HOLD UP][MAGNATE_GANG_BOSS] - Cash before boss cut: $", iCashGrabbed)
	//									GB_HANDLE_GANG_BOSS_CUT(iCashGrabbed)
	//									PRINTLN("[AM HOLD UP][MAGNATE_GANG_BOSS] - Cash after boss cut: $", iCashGrabbed)
	//								ENDIF
	//								#ENDIF
									
									IF iCashGrabbed > 0
										IF USE_SERVER_TRANSACTIONS()
											INT iTransactionID
											TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_HOLDUPS, iCashGrabbed, iTransactionID)
										ELSE
											GIVE_LOCAL_PLAYER_CASH_END_OF_MISSION(iCashGrabbed)
											NETWORK_EARN_FROM_HOLDUPS(iCashGrabbed)
										ENDIF
									ENDIF
									
									MPGlobalsAmbience.sSmashAndGrabStruct.iCashStolen += iCashGrabbed
									
									iEarnedCash += iCashGrabbed
									SET_LAST_JOB_DATA(LAST_JOB_HOLD_UP, iEarnedCash)
																
								PRINTLN("[AM HOLD UP] *****[ShopRobberies.sc->PLAYER ROBBING TILL] player grabbed ", iCashGrabbed)
								
								//iShopRobCashAmount += iCashGrabbed
								
								//Shop_Robberies.shopCashBag.iCashSoundID = GET_SOUND_ID()
								PLAY_SOUND_FRONTEND(-1, "ROBBERY_MONEY_TOTAL", "HUD_FRONTEND_CUSTOM_SOUNDSET")
								
								bMoneyFlag = TRUE
							ENDIF
						ELSE
							IF bMoneyFlag
								//ODDJOB_STOP_SOUND(Shop_Robberies.shopCashBag.iCashSoundID)
								bMoneyFlag = FALSE
							ENDIF
						ENDIF
					ENDIF
					
					SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_UseKinematicPhysics, TRUE)
					SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE)
					SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisableActionMode, TRUE)
				ENDIF
			ENDIF
		BREAK
		
		CASE PLAYERROBTILL_CLEANUP
			
			IF eCurrentWeapon <> WEAPONTYPE_UNARMED
			AND NOT IS_PED_INJURED(PLAYER_PED_ID())
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), eCurrentWeapon, TRUE)
			ENDIF
			SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), TRUE)
						
			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			
			IF DOES_CAM_EXIST(ciTill)
				DESTROY_CAM(ciTill)
			ENDIF
			
			IF DOES_CAM_EXIST(ciTillInterp)
				DESTROY_CAM(ciTillInterp)
			ENDIF
			
			RENDER_SCRIPT_CAMS(FALSE,FALSE)
			
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			//ODDJOB_EXIT_CUTSCENE()
			
			//SHOULD ALREADY HAVE WANTED LEVEL
			//SET_SHOP_ROBBERIES_WANTED_LEVEL_ON_PLAYER(1, FALSE)
			
			//GET NEW AWARD AND STATS???
			
			MPGlobalsAmbience.sSmashAndGrabStruct.bCollectedCash = TRUE
			MPGlobalsAmbience.sSmashAndGrabStruct.bHoldupComplete = TRUE
			
			IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_BIKER_GANG()
				BIK_ADD_POINTS_FOR_THIS(BIK_ADD_POINTS_ROB_SHOP)
			ENDIF
			
			ePlayerRobTillState = PLAYERROBTILL_DONE
			PRINTLN("[AM HOLD UP] ***** UPDATE_PLAYER_ROBBING_TILL - Jumping from PLAYERROBTILL_CLEANUP to PLAYERROBTILL_DONE")
		BREAK
		
		CASE PLAYERROBTILL_SKIP
			SET_CAM_ACTIVE(ciTillInterp, FALSE)
			RENDER_SCRIPT_CAMS(FALSE,FALSE)
			
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			ENDIF
			
			ePlayerRobTillState = PLAYERROBTILL_CLEANUP
			PRINTLN("[AM HOLD UP] ***** UPDATE_PLAYER_ROBBING_TILL - Jumping from PLAYERROBTILL_SKIP to PLAYERROBTILL_CLEANUP")
		BREAK
		
	ENDSWITCH
	
ENDPROC

//*** END - PLAYER ROBS TILL MANUALLY -------------------------------------------------------------------------



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                      Main Loop                      //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
SCRIPT(MP_MISSION_DATA missionScriptArgs)
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		#IF IS_DEBUG_BUILD	
			NET_PRINT_TIME() NET_PRINT_STRINGS("[AM HOLD UP] Starting mission ", GET_MP_MISSION_NAME(shopSnacks.thisHoldupMission)) NET_NL()
		#ENDIF
		
		// Carry out all the initial game starting duties. 
		IF NOT PROCESS_PRE_GAME(missionScriptArgs)
			#IF IS_DEBUG_BUILD NET_LOG("FAILED TO RECEIVE INITIAL NETWORK BROADCAST")	#ENDIF
			NET_PRINT_TIME() NET_PRINT_STRINGS("[AM HOLD UP] - MISSION END - FAILED TO RECEIVE INITIAL NETWORK BROADCAST- SCRIPT CLEANUP D     <----------     ", GET_MP_MISSION_NAME(shopSnacks.thisHoldupMission)) NET_NL()
			SCRIPT_CLEANUP()
		ENDIF
		
		// Setup widgets. Make sure this is done after PROCESS_PRE_GAME()
		#IF IS_DEBUG_BUILD
			CREATE_WIDGETS()
		#ENDIF
		
		// if coming in on a transition from the marketplace, freeze control so that script can go straight to the snack menu
		IF SHOULD_TRANSITION_SESSION_RESTORE_SHOP_STATE_ON_RESPAWN()
			NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - [STORETRANS] coming back from online store, setting control off     <----------     ") NET_NL()
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_FREEZE_POSITION | NSPC_CAN_BE_TARGETTED | NSPC_ALLOW_PLAYER_DAMAGE )
		ENDIF
	ENDIF
	
	// Main loop
	WHILE TRUE

		// One wait to rule them all. This can be the ONLY wait from here on in.....
		MP_LOOP_WAIT_ZERO() 
				
		/*IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_0)
			bCreatePed = TRUE
		ENDIF
		IF bCreatePed
			IF CREATE_STAFF_AND_BAG()
				bCreatePed = FALSE
			ENDIF
		ENDIF*/
		
		// If we have a match end event, bail
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			#IF IS_DEBUG_BUILD NET_LOG("CLEANUP - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE")	#ENDIF
			NET_PRINT_TIME() NET_PRINT_STRINGS("[AM HOLD UP] - MISSION END - SCRIPT CLEANUP B     <----------     ", GET_MP_MISSION_NAME(shopSnacks.thisHoldupMission)) NET_NL()
			SCRIPT_CLEANUP()
		ENDIF
		
		MAINTAIN_DROP_HOLDUP_CASH_UPON_PLAYER_DEATH()
			
		//End if player has entered a tutorial instance
		/*IF NETWORK_IS_IN_TUTORIAL_SESSION()
			#IF IS_DEBUG_BUILD NET_LOG("CLEANUP - PLAYER IN TUTORIAL SESSION")	#ENDIF
			NET_PRINT_TIME() NET_PRINT_STRINGS("[AM HOLD UP] - MISSION END - PLAYER IN TUTORIAL SESSION - SCRIPT CLEANUP C     <----------     ", GET_MP_MISSION_NAME(shopSnacks.thisHoldupMission)) NET_NL()
			SCRIPT_CLEANUP()
		ENDIF*/
			
		// if a robbery is taking place, make sure control is given back if transitioning from the marketplace
		IF IS_BIT_SET(serverBD.iServerBitSet, biS_AnyCrewHasDemandedCash)
			IF SHOULD_TRANSITION_SESSION_RESTORE_SHOP_STATE_ON_RESPAWN()
				NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - [STORETRANS] robbery happening, setting control on     <----------     ") NET_NL()
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				CLEANUP_TEMP_PASSIVE_MODE()
				CLEAR_BIT(shopSnacks.iBoolsBitSet2, bi2TempPassiveModeEnabled)
				CLEAR_TRANSITION_SESSION_RESTORE_SHOP_STATE_ON_RESPAWN()
			ENDIF
		ENDIF
		
		//GET_NETWORK_TIMER(iNetTimer)
		
		// Deal with the debug
		#IF IS_DEBUG_BUILD		
			UPDATE_WIDGETS()
		#ENDIF		
	
		// If we need to leave the mission, make sure we just straight to the end stage and terminate the script - DON'T NEED FOR AMBIENT MISSION
		/*IF SHOULD_PLAYER_LEAVE_MP_MISSION() 
			#IF IS_DEBUG_BUILD
			NET_PRINT_TIME() NET_PRINT_STRINGS( GET_MP_MISSION_NAME(shopSnacks.thisHoldupMission), " Leave mission = TRUE \n")
			#ENDIF
			playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_LEAVE
			#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_LEAVE")	#ENDIF
		ENDIF*/
		
		// -----------------------------------
		// Process your game logic.....
		SWITCH GET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT())
			
			// Wait until the server gives the all go before moving on
			CASE GAME_STATE_INI
				REQUEST_ANIM_DICT("mp_am_hold_up")
				
				IF HAS_ANIM_DICT_LOADED("mp_am_hold_up")
					IF GOT_NEAREST_STORE_DATA()
						REQUEST_STREAMED_TEXTURE_DICT(GET_SHOP_HEADER_TEXTURE_STRING(shopSnacks.eCurrentShop))
						IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.sMenuData.niRegStaff)
							IF GET_SERVER_MISSION_STATE() > GAME_STATE_INI
								IF NOT IS_NET_PED_INJURED(serverBD.sMenuData.niRegStaff)
									ADD_PED_FOR_DIALOGUE(sSpeech, 3, NET_TO_PED(serverBD.sMenuData.niRegStaff), serverBD.tlStaffVoice, TRUE, FALSE)
									CREATE_SEQUENCES()
								ENDIF
								SET_BIT(shopSnacks.iBoolsBitSet, biInitialisationDone)
								g_eCurrentShop = shopSnacks.eCurrentShop
								playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_RUNNING
								NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - GAME_STATE_INI - playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_RUNNING    <----------     ") NET_NL()
								#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_RUNNING")	#ENDIF
							ELSE
								NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - GAME_STATE_INI FAILING ON - GET_SERVER_MISSION_STATE   <----------     ") NET_NL()
							ENDIF
						ELSE
							NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - GAME_STATE_INI FAILING ON - NETWORK_DOES_NETWORK_ID_EXIST PED   <----------     ") NET_NL()
						ENDIF
					ELSE
						NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - GAME_STATE_INI FAILING ON - GOT_NEAREST_STORE_DATA   <----------     ") NET_NL()
					ENDIF
				ELSE
					NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - GAME_STATE_INI FAILING ON - HAS_ANIM_DICT_LOADED   <----------     ") NET_NL()
				ENDIF
				
				// Look for the server say the mission has ended
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY
					NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY 1    <----------     ") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_TERMINATE_DELAY")	#ENDIF
				ENDIF
				
			BREAK
			
			// Main gameplay state.
			CASE GAME_STATE_RUNNING

				UPDATE_MP_SHOP_SNACKS(shopSnacks, scenePosition, sceneRotation, serverBD.sMenuData, playerBD[PARTICIPANT_ID_TO_INT()].sMenuData, IS_ENTITY_IN_ANGLED_AREA(NET_TO_PED(serverBD.sMenuData.niRegStaff), vBackRoomMin, vBackRoomMax, fBackRoomWidth))
				UPDATE_PLAYER_ROBBING_TILL()
				
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					PROCESS_HOLD_UP()
				//ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_TUT_LEAVE_AREA
				//	playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TUT_LEAVE_AREA
				//	NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TUT_LEAVE_AREA 1   <----------     ") NET_NL()
				//	#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_TUT_LEAVE_AREA")	#ENDIF
				// Look for the server say the mission has ended
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY
					NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY 2   <----------     ") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_TERMINATE_DELAY")	#ENDIF
				ENDIF
								
				// Make the player end the mission if they leave the shop area
				IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vDoorCoord, <<30, 30, 30>>)
				//AND NOT NETWORK_IS_IN_TUTORIAL_SESSION()
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
					NET_PRINT_TIME() NET_PRINT_STRINGS("[AM HOLD UP] - playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END 1 - MISSION END - PLAYER NOT IN AREA     <----------     ", GET_MP_MISSION_NAME(shopSnacks.thisHoldupMission)) NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_END 1")	#ENDIF
				ENDIF
			BREAK
			
			// Tutorial Leave Area Gameplay State.
			/*CASE GAME_STATE_TUT_LEAVE_AREA
				
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_TUT_LEAVE_AREA
					PROCESS_TUTORIAL_LEAVE_AREA()
				// Look for the server say the mission has ended
				//ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
				//	playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY
				//	NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY 2   <----------     ") NET_NL()
				ENDIF
			BREAK*/
			
			//
			CASE GAME_STATE_TERMINATE_DELAY 
				SET_MISSION_FINISHED(serverBD.MissionTerminateDelayTimer)
		      	IF IS_MISSION_READY_TO_CLEANUP(serverBD.MissionTerminateDelayTimer)
		            playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
					NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END 3    <----------     ") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_END 3")	#ENDIF
		      	ENDIF
			BREAK
			
			// The game stage the local player is placed when we want him to leave a mission
			CASE GAME_STATE_LEAVE
				//LEAVE_MY_MP_MISSION(shopSnacks.thisHoldupMission)
				playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
				NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END 2    <----------     ") NET_NL()
				#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_END 2")	#ENDIF
			FALLTHRU
			
			//Cleans up then terminates the mission
			CASE GAME_STATE_END
				GIVE_END_HOLD_UP_REWARD()
				NET_PRINT_TIME() NET_PRINT_STRINGS("[AM HOLD UP] - SCRIPT CLEANUP A     <----------     ", GET_MP_MISSION_NAME(shopSnacks.thisHoldupMission)) NET_NL()
				SCRIPT_CLEANUP()
			BREAK

		ENDSWITCH
		
		// -----------------------------------
		// Process server game logic		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			
			SWITCH GET_SERVER_MISSION_STATE()
				
				CASE GAME_STATE_INI
					IF NOT IS_BIT_SET(shopSnacks.iBoolsBitSet, biNearestStoreDataGrabbed)
						REQUEST_LOAD_MODEL(GET_HOLD_UP_MODEL(HUP_MODEL_STAFF))
						REQUEST_LOAD_MODEL(GET_HOLD_UP_MODEL(HUP_MODEL_CASH_BAG))
						REQUEST_LOAD_MODEL(GET_HOLD_UP_MODEL(HUP_MODEL_TILL))
						REQUEST_ANIM_DICT("mp_am_hold_up")
						//REQUEST_ANIM_DICT("misscommon@response")
						
						IF HAS_ANIM_DICT_LOADED("mp_am_hold_up")
						//AND HAS_ANIM_DICT_LOADED("misscommon@response")
						
							IF GOT_NEAREST_STORE_DATA()
								CLEAR_AREA(scenePosition, 1.5, FALSE, TRUE, FALSE, TRUE)
								CLEAR_AREA(vBackRoomCower, 1.5, FALSE, TRUE, FALSE, TRUE)
								SET_BIT(shopSnacks.iBoolsBitSet, biNearestStoreDataGrabbed)
								NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - NEAREST STORED DATA GRABBED") NET_NL()
								#IF IS_DEBUG_BUILD NET_LOG_Host("NEAREST STORED DATA GRABBED")	#ENDIF
							ENDIF
						ENDIF
					ELSE
						IF CREATE_STAFF_AND_BAG()	//CREATE_REG_STAFF()
							serverBD.iServerGameState = GAME_STATE_RUNNING
							NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - serverBD.iServerGameState = GAME_STATE_RUNNING    <----------     ") NET_NL()
							#IF IS_DEBUG_BUILD NET_LOG_Host("iServerGameState = GAME_STATE_RUNNING")	#ENDIF
						ENDIF
					ENDIF
				BREAK
				
				// Look for game end conditions
				CASE GAME_STATE_RUNNING
					
					//SET_PED_BLOOD_DAMAGE_CLEAR_INFO(PLAYER_PED_ID(), fRed, fAlpha) //TEST FOR RAGE MODE
					
					MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER()
					MAINTAIN_MAX_PARTICIPANT_CHECKS_STAGGERED_SERVER()
					PROCESS_STAFF_BRAIN()
					PROCESS_START_SYNC_SCENE()
					PROCESS_SNACK_BUY_SCENE()
					PROCESS_SHOCKING_EVENT()
					PROCESS_HERO_BRAIN()
					PROCESS_CHECKS()
					
					#IF IS_DEBUG_BUILD
						IF bHostEndMissionNow
							serverBD.iServerGameState = GAME_STATE_END
							NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - serverBD.iServerGameState = GAME_STATE_END 1    <----------     ") NET_NL()
							#IF IS_DEBUG_BUILD NET_LOG_Host("iServerGameState = GAME_STATE_END 1")	#ENDIF
						ENDIF
					#ENDIF	
					
					IF HAVE_MISSION_END_CONDITIONS_BEEN_MET()
						CLEANUP_ENTITIES()
						serverBD.iServerGameState = GAME_STATE_END
						NET_PRINT_TIME() NET_PRINT("[AM HOLD UP] - serverBD.iServerGameState = GAME_STATE_END 2    <----------     ") NET_NL()
						#IF IS_DEBUG_BUILD NET_LOG_Host("iServerGameState = GAME_STATE_END 2")	#ENDIF
					ENDIF
					
				BREAK
				
				CASE GAME_STATE_END
					CLEANUP_ENTITIES()
				BREAK
				
			ENDSWITCH
			
		ENDIF
		
	ENDWHILE
	
ENDSCRIPT
