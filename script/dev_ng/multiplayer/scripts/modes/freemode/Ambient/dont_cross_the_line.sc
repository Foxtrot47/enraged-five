
USING "globals.sch"
USING "net_events.sch"
USING "net_wait_zero.sch"
USING "dont_touch_line_minigame.sch"

LINE_MINIGAME_DATA sLineMinigameData
CONST_INT MAX_DCTL_PLAYERS	4

#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID widgetMinigame
#ENDIF

PROC CLEANUP_SCRIPT()
	LINE_MINIGAME_CLEANUP_AND_EXIT_CLIENT(sLineMinigameData)
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

/// PURPOSE:
///    Do necessary pre game start ini.
PROC PROCESS_PRE_GAME(MP_MISSION_DATA missionScriptArgs)
	
	// This marks the script as a net script, and handles any instancing setup. 
	SETUP_MP_MISSION_FOR_NETWORK(MAX_DCTL_PLAYERS , missionScriptArgs)
	
	// This makes sure the net script is active, waits until it is.
	IF NOT HANDLE_NET_SCRIPT_INITIALISATION(DEFAULT, DEFAULT, TRUE)
		CLEANUP_SCRIPT()
	ENDIF
	
	LINE_MINIGAME_PRE_DEFINE_SERVER_DATA()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(LINE_ServerBD, SIZE_OF(LINE_ServerBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(LINE_PlayerBD, SIZE_OF(LINE_PlayerBD)) 
	
	IF NOT Wait_For_First_Network_Broadcast() 
		CLEANUP_SCRIPT()
	ENDIF
	
ENDPROC

SCRIPT(MP_MISSION_DATA missionScriptArgs)
	
	missionScriptArgs.mdID.idMission = eAM_DONT_CROSS_THE_LINE
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		PROCESS_PRE_GAME(missionScriptArgs)
	ENDIF
	
	DONT89_SETIS_RUNNING_GAME_BD_STATE(TRUE)
	WHILE TRUE
	
		MP_LOOP_WAIT_ZERO()
		
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			CLEANUP_SCRIPT()
		ENDIF
		
		IF NETWORK_IS_GAME_IN_PROGRESS()
			#IF IS_DEBUG_BUILD
				IF NOT DOES_WIDGET_GROUP_EXIST(widgetMinigame)
					widgetMinigame = START_WIDGET_GROUP("Don't Cross The Line")
					
					STOP_WIDGET_GROUP()
					
					LINE_MINIGAME_SET_PARENT_WIDGET_GROUP(widgetMinigame)
				ENDIF
			#ENDIF
		
			UPDATE_LINE_MINIGAME(sLineMinigameData)
		ELSE
			CLEANUP_SCRIPT()
		ENDIF
		
		#IF IS_DEBUG_BUILD
			IF IS_KEYBOARD_KEY_PRESSED(KEY_F)
				CLEANUP_SCRIPT()
			ENDIF
		#ENDIF
	ENDWHILE

ENDSCRIPT