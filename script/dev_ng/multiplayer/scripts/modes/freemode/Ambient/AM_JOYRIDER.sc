//////////////////////////////////////////////////////////////////////////////////////////
// Name:        AM_JOYRIDER.sc															//
// Description: Controls a joyrider in a car from the Import List driving past players	//
// Written by:  Ryan Baker																//
// Date: 09/01/2013																		//
//////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"

// Game Headers
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_path.sch"
//USING "commands_interiors.sch"
USING "commands_event.sch"

// Network Headers
USING "net_include.sch"
USING "net_events.sch"
USING "net_spawn.sch"

USING "net_mission.sch"
//
USING "net_scoring_common.sch"
USING "net_ambience.sch"

//USING "help_at_location.sch"
USING "commands_object.sch"
USING "net_xp_animation.sch"
USING "net_joyrider.sch"

USING "net_wait_zero.sch"

#IF IS_DEBUG_BUILD
USING "net_debug_log.sch"
#ENDIF

// Game States. 
CONST_INT GAME_STATE_INI 				0
CONST_INT GAME_STATE_RUNNING			1
CONST_INT GAME_STATE_LEAVE				2
CONST_INT GAME_STATE_TERMINATE_DELAY	3
CONST_INT GAME_STATE_END				4

CONST_FLOAT LEAVE_RANGE					250.0
CONST_INT CAR_SPEED						30
CONST_INT REACH_LAUNCH_LOCATION_RANGE	15

ENUM JOYRIDER_STAGE_ENUM
	eAD_JOYRIDE,
	eAD_CLEANUP
ENDENUM

//Server Bitset Data
CONST_INT biS_NoPlayerNearJoyrider	0

// The server broadcast data.
// Everyone can read this data, only the server can update it
STRUCT ServerBroadcastData
	INT iServerGameState
		
	INT iServerBitSet
	
	NETWORK_INDEX niCar
	NETWORK_INDEX niDriver
	
	MODEL_NAMES CarModel = DUMMY_MODEL_FOR_SCRIPT
	MODEL_NAMES DriverModel
	
	VECTOR vLaunchLocation
	
	JOYRIDER_STAGE_ENUM eJoyriderStage = eAD_JOYRIDE
	
	SCRIPT_TIMER MissionTerminateDelayTimer
ENDSTRUCT
ServerBroadcastData serverBD


CONST_INT biP_JoyriderExitDone			0

// The client broadcast data.
// Everyone can read this data, only the local player only the server can update it
STRUCT PlayerBroadcastData
	INT iGameState
	
	INT iPlayerBitSet
	
	JOYRIDER_STAGE_ENUM eJoyriderStage = eAD_JOYRIDE
ENDSTRUCT
PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]

///// MISSION VARIABLES /////
INT iBoolsBitSet
CONST_INT biSequencesCreated			0
CONST_INT biDestinationNodeFound		1

//BLIP_INDEX CarBlip

SEQUENCE_INDEX seqJoyride
SEQUENCE_INDEX seqExit
SEQUENCE_INDEX seqStopAndExit

INT iDestinationNodeCount

CONST_INT START_NODE		20
INT iSpawnNodeCount = START_NODE
INT iCheckCount

INT iStaggeredParticipant
BOOL bDoEndStaggeredLoopChecks

//INT iTotalVehicleCollected

// Widgets & Debug.
#IF IS_DEBUG_BUILD
	BOOL bHostEndMissionNow
	BOOL bWarpToEnd
	BOOL bDebugBlip// = TRUE
	BLIP_INDEX CarBlip
#ENDIF

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  MAIN PROCS                            ////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Helper function to get the servers game/mission state
FUNC INT GET_SERVER_MISSION_STATE()
	RETURN serverBD.iServerGameState
ENDFUNC

//PURPOSE: Clears all the sequences needed for this script
PROC CLEAR_SEQUENCES()
	CLEAR_SEQUENCE_TASK(seqJoyride)
	NET_PRINT_TIME() NET_PRINT("     ---------->     JOYRIDER - CLEAR_SEQUENCES   <----------     ") NET_NL()
ENDPROC

//Cleanup Mission Data
PROC SCRIPT_CLEANUP()
	
	#IF IS_DEBUG_BUILD
	IF DOES_BLIP_EXIST(CarBlip)
		REMOVE_BLIP(CarBlip)
	ENDIF
	#ENDIF
	
	CLEAR_SEQUENCES()
	
	IF GET_SERVER_MISSION_STATE() = GAME_STATE_END
	AND serverBD.CarModel != DUMMY_MODEL_FOR_SCRIPT
		SET_VEHICLE_MODEL_IS_SUPPRESSED(serverBD.CarModel, FALSE)
		NET_PRINT_TIME() NET_PRINT("     ---------->     JOYRIDER - SET_VEHICLE_MODEL_IS_SUPPRESSED - FALSE      <----------     ") NET_NL()
	ENDIF
	
	IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niCar)
		REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(NET_TO_VEH(serverBD.niCar), 0.0)
	ENDIF
	
	STOP_AUDIO_SCENE("JOYRIDER_RADIO_SCENE")
	
	SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_JOYRIDER, FALSE)
	
	NET_PRINT_TIME() NET_PRINT("     ---------->     JOYRIDER - CLEANUP MISSION      <----------     ") NET_NL()
	#IF IS_DEBUG_BUILD NET_LOG("SCRIPT_CLEANUP")	#ENDIF
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

//Do necessary pre game start ini.
PROC PROCESS_PRE_GAME( MP_MISSION_DATA missionScriptArgs )
	
	// This marks the script as a net script, and handles any instancing setup. 
	SETUP_MP_MISSION_FOR_NETWORK(GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(missionScriptArgs.mdID.idMission) ,  missionScriptArgs)	//NUM_NETWORK_PLAYERS, missionScriptArgs)	//
	//NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE, PARTICIPANT_ID_TO_INT())  
	
	//Reserve Entities
	RESERVE_NETWORK_MISSION_PEDS(1)
	RESERVE_NETWORK_MISSION_VEHICLES(1)

	// This makes sure the net script is active, waits untill it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
	
	// KGM: Wait for the first network broadcast before moving on
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		#IF IS_DEBUG_BUILD NET_LOG("JOYRIDER - FAILED TO RECEIVE INITIAL NETWORK BROADCAST")	#ENDIF
		NET_PRINT_TIME() NET_PRINT("     ---------->     JOYRIDER - MISSION END - FAILED TO RECEIVE INITIAL NETWORK BROADCAST - SCRIPT CLEANUP D     <----------     ") NET_NL()
		SCRIPT_CLEANUP()
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		// This script will not be paused if another script calls PAUSE_GAME
		SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		//	serverBD.vLaunchLocation = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
			IF MPGlobalsAmbience.gCarModelOnList = DUMMY_MODEL_FOR_SCRIPT
				INT iRand = GET_RANDOM_INT_IN_RANGE(0, 7)
				SWITCH iRand
					CASE 0 serverBD.CarModel = STINGERGT	BREAK
					CASE 1 serverBD.CarModel = ENTITYXF		BREAK
					CASE 2 serverBD.CarModel = FELTZER2		BREAK
					CASE 3 serverBD.CarModel = MONROE		BREAK
					CASE 4 serverBD.CarModel = COGCABRIO	BREAK
					CASE 5 serverBD.CarModel = SUPERD		BREAK
					CASE 6 serverBD.CarModel = INFERNUS		BREAK
				ENDSWITCH
				NET_PRINT_TIME() NET_PRINT("     ---------->     JOYRIDER - MPGlobalsAmbience.gCarModelOnList = DUMMY_MODEL_FOR_SCRIPT - USE RANDOM CAR MODEL ") NET_NL()
			ELSE
				serverBD.CarModel = MPGlobalsAmbience.gCarModelOnList
				MPGlobalsAmbience.gCarModelOnList = DUMMY_MODEL_FOR_SCRIPT
			ENDIF
			SET_VEHICLE_MODEL_IS_SUPPRESSED(serverBD.CarModel, TRUE)
			serverBD.DriverModel = GET_RANDOM_PED_MODEL()
			/*IF GET_CLOSEST_VEHICLE_NODE(GET_PLAYER_COORDS(PLAYER_ID()), serverBD.vLaunchLocation, NF_IGNORE_SLIPLANES)
				NET_PRINT_TIME() NET_PRINT("     ---------->     JOYRIDER - PRE_GAME DONE - vLaunchLocation = NODE ") NET_NL()
			ELSE
				serverBD.vLaunchLocation = GET_PLAYER_COORDS(PLAYER_ID())
				NET_PRINT_TIME() NET_PRINT("     ---------->     JOYRIDER - PRE_GAME DONE - vLaunchLocation = PLAYER COORDS") NET_NL()
			ENDIF*/
		ENDIF
		
		//SCRIPT_PLAYSTATS_MISSION_STARTED(GET_MISSION_NAME_FROM_TYPE(FMMC_TYPE_JOYRIDER))
		
		SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_JOYRIDER, TRUE)
		
		START_AUDIO_SCENE("JOYRIDER_RADIO_SCENE")
		
		playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_INI
		NET_PRINT_TIME() NET_PRINT("     ---------->     JOYRIDER - PRE_GAME DONE - xx     <----------     ") NET_NL()
	ELSE
		#IF IS_DEBUG_BUILD NET_LOG("JOYRIDER - NETWORK_IS_GAME_IN_PROGRESS = FALSE")	#ENDIF
		NET_PRINT_TIME() NET_PRINT("     ---------->     JOYRIDER - MISSION END - NETWORK_IS_GAME_IN_PROGRESS = FALSE - SCRIPT CLEANUP Q     <----------     ") NET_NL()
		SCRIPT_CLEANUP()
	ENDIF
ENDPROC

//Player leaves area
FUNC BOOL MISSION_END_CHECK()
	IF serverBD.eJoyriderStage = eAD_JOYRIDE
		//Car is dead
		IF NOT IS_NET_VEHICLE_DRIVEABLE(serverBD.niCar)
			NET_PRINT_TIME() NET_PRINT("     ---------->     JOYRIDER - MISSION END - CAR NOT DRIVEABLE     <----------     ") NET_NL()
			RETURN TRUE
		ELSE
			//Driver is dead
			IF IS_NET_PED_INJURED(serverBD.niDriver)
				NET_PRINT_TIME() NET_PRINT("     ---------->     JOYRIDER - MISSION END - DRIVER DEAD     <----------     ") NET_NL()
				RETURN TRUE
			ELSE
				//No Player Near Joyrider
				IF bDoEndStaggeredLoopChecks = TRUE
					IF IS_BIT_SET(serverBD.iServerBitSet, biS_NoPlayerNearJoyrider)
						NET_PRINT_TIME() NET_PRINT("     ---------->     JOYRIDER - MISSION END - NO CREW NEAR JOYRIDER - POS = ") NET_PRINT_VECTOR(GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niCar))) NET_NL()
						RETURN TRUE
					ENDIF
				ENDIF
				
				//Driver not in Car
				IF NOT IS_PED_IN_VEHICLE(NET_TO_PED(serverBD.niDriver), NET_TO_VEH(serverBD.niCar))
					NET_PRINT_TIME() NET_PRINT("     ---------->     JOYRIDER - MISSION END - DRIVER NOT IN CAR     <----------     ") NET_NL()
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//Server only function that checks to see if the game mode should now end. For this test script, it if time has passed.
FUNC BOOL HAVE_MISSION_END_CONDITIONS_BEEN_MET()
	
	#IF IS_DEBUG_BUILD
		IF bHostEndMissionNow
			RETURN TRUE
		ENDIF
	#ENDIF	
	
	IF MISSION_END_CHECK()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


//Helper function to get a clients game/mission state
FUNC INT GET_CLIENT_MISSION_STATE(INT iPlayer)
	RETURN playerBD[iPlayer].iGameState
ENDFUNC

#IF IS_DEBUG_BUILD
PROC CREATE_WIDGETS()
	INT iPlayer
	TEXT_LABEL_15 tl63

	START_WIDGET_GROUP("JOYRIDER")  
		ADD_WIDGET_BOOL("bDebugBlip", bDebugBlip)
		ADD_WIDGET_BOOL("Warp to Joyrider", bWarpToEnd)
				
		// Gameplay debug, sever only.
		START_WIDGET_GROUP("Server Only Gameplay") 
			ADD_WIDGET_BOOL("End mission", bHostEndMissionNow)			
		STOP_WIDGET_GROUP()		
		
		// Data about the server
		START_WIDGET_GROUP("Server BD") 
			ADD_WIDGET_INT_SLIDER("S. Game state", serverBD.iServerGameState,-1, HIGHEST_INT,1)
		STOP_WIDGET_GROUP()	

		// Data about the clients. * = You.
		START_WIDGET_GROUP("Client BD")  				
			REPEAT NUM_NETWORK_PLAYERS iPlayer
				tl63 = "Player "
				tl63 += iPlayer
				IF iPlayer = PARTICIPANT_ID_TO_INT()
					tl63 += "*"
				ENDIF
				START_WIDGET_GROUP(tl63)
					ADD_WIDGET_INT_SLIDER("Game state", playerBD[iPlayer].iGameState,-1, HIGHEST_INT,1)
				STOP_WIDGET_GROUP()
			ENDREPEAT				
		STOP_WIDGET_GROUP()	
		
	STOP_WIDGET_GROUP()
ENDPROC		

PROC UPDATE_WIDGETS()
	//Warp player to Joyrider
	IF bWarpToEnd = TRUE
		IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niCar)
			IF NET_WARP_TO_COORD(GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niCar))+<<0, 0, 3>>, 0, TRUE, FALSE)
				NET_PRINT_TIME() NET_PRINT("     ---------->     JOYRIDER - UPDATE_WIDGETS - bWarpToEnd DONE    <----------     ") NET_NL()
				bWarpToEnd = FALSE
			ELSE
				NET_PRINT_TIME() NET_PRINT("     ---------->     JOYRIDER - UPDATE_WIDGETS - bWarpToEnd IN PROGRESS    <----------     ") NET_NL()
			ENDIF
		ENDIF
	ENDIF
	
	IF bDebugBlip = TRUE
		IF NOT DOES_BLIP_EXIST(CarBlip)
			IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niCar)
				CarBlip = ADD_BLIP_FOR_ENTITY(NET_TO_VEH(serverBD.niCar))
				SET_BLIP_SPRITE(CarBlip, RADAR_TRACE_GANG_VEHICLE)
				SET_BLIP_COLOUR(CarBlip, BLIP_COLOUR_RED)
				SHOW_HEIGHT_ON_BLIP(CarBlip, TRUE)
				//SET_BLIP_NAME_FROM_TEXT_FILE(VehicleBlip, "VED_BLIPN")
				NET_PRINT_TIME() NET_PRINT("     ---------->     JOYRIDER - CAR BLIP ADDED ") NET_NL()
			ENDIF
		ENDIF
	ELSE
		IF DOES_BLIP_EXIST(CarBlip)
			REMOVE_BLIP(CarBlip)
			NET_PRINT_TIME() NET_PRINT("     ---------->     JOYRIDER - CAR BLIP REMOVED ") NET_NL()
		ENDIF
	ENDIF
ENDPROC

#ENDIF

//PURPOSE: Creates all the sequences needed for this script
PROC CREATE_SEQUENCES()
	IF NOT IS_BIT_SET(iBoolsBitSet, biSequencesCreated)
		IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niCar)
			
			OPEN_SEQUENCE_TASK(seqJoyride)
				TASK_VEHICLE_MISSION_COORS_TARGET(NULL, NET_TO_VEH(serverBD.niCar), serverBD.vLaunchLocation, MISSION_GOTO_RACING, CAR_SPEED, DRIVINGMODE_AVOIDCARS_RECKLESS, REACH_LAUNCH_LOCATION_RANGE, 1, FALSE)
				TASK_VEHICLE_DRIVE_WANDER(NULL, NET_TO_VEH(serverBD.niCar), CAR_SPEED, DRIVINGMODE_AVOIDCARS_RECKLESS)
			CLOSE_SEQUENCE_TASK(seqJoyride)
			
			OPEN_SEQUENCE_TASK(seqExit)
				TASK_LEAVE_ANY_VEHICLE(NULL, 0, ECF_RESUME_IF_INTERRUPTED)	//ECF_DONT_CLOSE_DOOR|
			CLOSE_SEQUENCE_TASK(seqExit)
			
			OPEN_SEQUENCE_TASK(seqStopAndExit)
				//TASK_VEHICLE_TEMP_ACTION(NULL, NET_TO_VEH(serverBD.niCar), TEMPACT_HANDBRAKESTRAIGHT_INTELLIGENT, 3000)
				//TASK_VEHICLE_MISSION(NULL, NET_TO_VEH(serverBD.niCar), NULL, MISSION_STOP, CAR_SPEED, DRIVINGMODE_AVOIDCARS_RECKLESS, REACH_LAUNCH_LOCATION_RANGE, 1)
				TASK_VEHICLE_MISSION_COORS_TARGET(NULL, NET_TO_VEH(serverBD.niCar), serverBD.vLaunchLocation, MISSION_STOP, CAR_SPEED, DRIVINGMODE_AVOIDCARS_RECKLESS, 99999, 1, FALSE)
				TASK_LEAVE_ANY_VEHICLE(NULL, 0, ECF_RESUME_IF_INTERRUPTED)	//ECF_DONT_CLOSE_DOOR|
			CLOSE_SEQUENCE_TASK(seqStopAndExit)
			
			SET_BIT(iBoolsBitSet, biSequencesCreated)
			NET_PRINT_TIME() NET_PRINT("     ---------->     JOYRIDER - CREATE_SEQUENCES   <----------     ") NET_NL()
		ELSE
			NET_PRINT_TIME() NET_PRINT("     ---------->     JOYRIDER - CREATE_SEQUENCES - NO - CAR IS NOT DRIVEABLE   <----------     ") NET_NL()
		ENDIF
	ENDIF
ENDPROC


//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  ENTITY CREATION                  /////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////

/// PURPOSE:
///    Finds coords for the near the player who launched the Joyrider.
/// RETURNS:
///    BOOL - TRUE if found cords, FALSE if not. 
FUNC BOOL GOT_FIRST_DESTINATION_POINT()
		
	IF NOT IS_BIT_SET(iBoolsBitSet, biDestinationNodeFound)
		IF GET_NTH_CLOSEST_VEHICLE_NODE(GET_PLAYER_COORDS(PLAYER_ID()), iDestinationNodeCount, serverBD.vLaunchLocation, NF_IGNORE_SLIPLANES)
			SET_BIT(iBoolsBitSet, biDestinationNodeFound)
			NET_PRINT_TIME() NET_PRINT("     ---------->     JOYRIDER - GOT_FIRST_DESTINATION_POINT - ") NET_PRINT_VECTOR(serverBD.vLaunchLocation) NET_NL()
		ELSE
			iDestinationNodeCount++
			NET_PRINT_TIME() NET_PRINT("     ---------->     JOYRIDER - GOT_FIRST_DESTINATION_POINT - FAILED - iDestinationNodeCount = ") NET_PRINT_INT(iDestinationNodeCount) NET_NL()
		ENDIF
	ENDIF
		
	RETURN IS_BIT_SET(iBoolsBitSet, biDestinationNodeFound)
ENDFUNC

/// PURPOSE:
///    Finds coords to spawn the Joyrider at.
/// RETURNS:
///    BOOL - TRUE if found cords, FALSE if not. 
FUNC BOOL GOT_JOYRIDER_CREATION_POINT(VECTOR &vPos, FLOAT &fHeading)
	
	BOOL bFoundSuitableNode
	INT iNumLanes
	VECTOR resultLinkDir
	
	IF NOT bFoundSuitableNode
		//IF GET_NTH_CLOSEST_VEHICLE_NODE_WITH_HEADING(GET_PLAYER_COORDS(NETWORK_GET_PLAYER_INDEX(PARTICIPANT_ID())), iSpawnNodeCount, vPos, fHeading, iNumLanes, NF_NONE)
		IF GENERATE_VEHICLE_CREATION_POS_FROM_PATHS(serverBD.vLaunchLocation, vPos, resultLinkDir, 0, 180, 40)
			GET_NTH_CLOSEST_VEHICLE_NODE_WITH_HEADING(vPos, 1, vPos, fHeading, iNumLanes, NF_IGNORE_SLIPLANES)
			
			IF (iNumLanes >= 1)	//2)
			OR (GET_HASH_OF_MAP_AREA_AT_COORDS(vPos) = MAP_AREA_CITY)
				IF GET_DISTANCE_BETWEEN_COORDS(<<-1367.5571, -3220.5977, 12.9448>>, serverBD.vLaunchLocation) >= 600.0 // Don't spawn in aeroplane hangars or runways.
				AND GET_DISTANCE_BETWEEN_COORDS(<<750, -3200, 6>>, serverBD.vLaunchLocation) >= 700.0
					IF GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()), vPos) <= (LEAVE_RANGE - 50.0)
						IF iCheckCount <= 5
							IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(vPos)
								NET_PRINT("JOYRIDER SERVER: GOT_JOYRIDER_CREATION_POINT - found suitable node to spawn van, spawning. iSpawnNodeCount = ")NET_PRINT_INT(iSpawnNodeCount)NET_NL()
								bFoundSuitableNode = TRUE
							ENDIF
						ELSE
							NET_PRINT("JOYRIDER SERVER: GOT_JOYRIDER_CREATION_POINT - could not find node that is ok for net entity creation after 100 checks - CLEANING UP SCRIPT")NET_NL()
							serverBD.iServerGameState = GAME_STATE_END
							NET_PRINT_TIME() NET_PRINT("     ---------->     JOYRIDER - serverBD.iServerGameState = GAME_STATE_END 5    <----------     ") NET_NL()
							#IF IS_DEBUG_BUILD NET_LOG_Host("iServerGameState = GAME_STATE_END 5")	#ENDIF
							//bFoundSuitableNode = TRUE
						ENDIF
					ELSE
						NET_PRINT("JOYRIDER SERVER: GOT_JOYRIDER_CREATION_POINT - spawn coords > 200.0m away, node cannot be used.")NET_NL()
					ENDIF
				ELSE
					NET_PRINT("JOYRIDER SERVER: GOT_JOYRIDER_CREATION_POINT - spawn coords <= 600.0m from airport/docks.")NET_NL()
				ENDIF
			ELSE
				NET_PRINT("JOYRIDER SERVER: GOT_JOYRIDER_CREATION_POINT - iNumLanes < 2, node cannot be used.")NET_NL()
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT bFoundSuitableNode
		iSpawnNodeCount += 4
		//NET_PRINT("JOYRIDER SERVER: GOT_JOYRIDER_CREATION_POINT - not found suitable node, iSpawnNodeCount = ") NET_PRINT_INT(iSpawnNodeCount) NET_NL()
		IF iSpawnNodeCount >= 80
			iSpawnNodeCount = START_NODE
			iCheckCount++
			NET_PRINT("JOYRIDER SERVER: GOT_JOYRIDER_CREATION_POINT - iCheckCount = ") NET_PRINT_INT(iCheckCount) NET_NL()
		ENDIF
	ENDIF
	
	RETURN bFoundSuitableNode
ENDFUNC

///////////////////////////////////////////
///    		CAR AND DRIVER    			///
///////////////////////////////////////////
//PURPOSE: Create the Car
FUNC BOOL CREATE_CAR()
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niCar)
		IF REQUEST_LOAD_MODEL(serverBD.CarModel)
			
			VECTOR vPos
			FLOAT fHeading
			IF GOT_JOYRIDER_CREATION_POINT(vPos, fHeading)
				IF CREATE_NET_VEHICLE(serverBD.niCar, serverBD.CarModel, vPos, fHeading)	
					//SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(NET_TO_VEH(serverBD.niCar), TRUE)
					//SET_VEHICLE_TYRES_CAN_BURST(NET_TO_VEH(serverBD.niCar), FALSE)
					
					//SET_VEHICLE_FORWARD_SPEED(NET_TO_VEH(serverBD.niCar), CAR_SPEED)
					SET_VEHICLE_ENGINE_ON(NET_TO_VEH(serverBD.niCar), TRUE, TRUE)
					
					SET_VEHICLE_IS_WANTED(NET_TO_VEH(serverBD.niCar), TRUE)
					SET_VEHICLE_IS_STOLEN(NET_TO_VEH(serverBD.niCar), TRUE)
					
					ADD_ENTITY_TO_AUDIO_MIX_GROUP(NET_TO_VEH(serverBD.niCar), "JOYRIDER_GROUP", 0.0)
					
					SET_MODEL_AS_NO_LONGER_NEEDED(serverBD.CarModel)
					NET_PRINT_TIME() NET_PRINT("     ---------->     JOYRIDER - CREATED CAR - ") NET_PRINT_VECTOR(vPos) NET_NL()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//Check we have created the vehicle
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niCar)
		//NET_PRINT_TIME() NET_PRINT("     ---------->     JOYRIDER - FAILING ON CAR CREATION") NET_NL()
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

//PURPOSE: Create the Driver
FUNC BOOL CREATE_DRIVER()
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niDriver)
	AND REQUEST_LOAD_MODEL(serverBD.DriverModel)
	AND NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niCar)
		IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niCar)
			IF CREATE_NET_PED_IN_VEHICLE(serverBD.niDriver, serverBD.niCar, PEDTYPE_CRIMINAL, serverBD.DriverModel, VS_DRIVER)
			
				SET_PED_RANDOM_COMPONENT_VARIATION(NET_TO_PED(serverBD.niDriver))
				//SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NET_TO_PED(serverBD.niDriver), TRUE)
				
				IF GET_RANDOM_BOOL()
					SET_PED_RELATIONSHIP_GROUP_HASH(NET_TO_PED(serverBD.niDriver), rgFM_AiDislike)
				ELSE
					SET_PED_RELATIONSHIP_GROUP_HASH(NET_TO_PED(serverBD.niDriver), rgFM_AiDislikePlyrLikeCops)
				ENDIF
				
				SET_PED_KEEP_TASK(NET_TO_PED(serverBD.niDriver), TRUE)
				
				//SET UP RANDOM ATTRIBUTES (Weapon, Aggressive, Coward, Skilled Fighter, etc)
				INT iRand
				IF GET_RANDOM_BOOL()
					iRand = GET_RANDOM_INT_IN_RANGE(0, 10)
					IF iRand < 5
						GIVE_DELAYED_WEAPON_TO_PED(NET_TO_PED(serverBD.niDriver), WEAPONTYPE_PISTOL, 25000, GET_RANDOM_BOOL())
					ELIF iRand > 6
						GIVE_DELAYED_WEAPON_TO_PED(NET_TO_PED(serverBD.niDriver), WEAPONTYPE_MICROSMG, 25000, GET_RANDOM_BOOL())
					ELSE
						GIVE_DELAYED_WEAPON_TO_PED(NET_TO_PED(serverBD.niDriver), WEAPONTYPE_PUMPSHOTGUN, 25000, GET_RANDOM_BOOL())
					ENDIF
					NET_PRINT_TIME() NET_PRINT("     ---------->     JOYRIDER - CREATE_DRIVER - GIVEN WEAPON ") NET_NL()
				ENDIF
								
				//Combat Ability
				iRand = GET_RANDOM_INT_IN_RANGE(0, 10)
				IF iRand = 0
				OR iRand = 1
					SET_PED_COMBAT_ABILITY(NET_TO_PED(serverBD.niDriver), CAL_POOR)
				ELIF iRand = 2
				OR iRand = 3
				OR iRand = 4
					SET_PED_COMBAT_ABILITY(NET_TO_PED(serverBD.niDriver), CAL_PROFESSIONAL)
				ELSE
					SET_PED_COMBAT_ABILITY(NET_TO_PED(serverBD.niDriver), CAL_AVERAGE)
				ENDIF
				
				//Combat Attributes
				iRand = GET_RANDOM_INT_IN_RANGE(0, 4)
				IF iRand = 0
					SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(serverBD.niDriver), CA_ALWAYS_FIGHT, TRUE)
				ELIF iRand = 1
					SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(serverBD.niDriver), CA_ALWAYS_FLEE, TRUE)
				ELIF iRand = 2
					SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(serverBD.niDriver), CA_AGGRESSIVE, TRUE)
				ENDIF
				IF GET_RANDOM_BOOL()
					SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(serverBD.niDriver), CA_CAN_TAUNT_IN_VEHICLE, TRUE)
				ENDIF
				IF GET_RANDOM_BOOL()
					SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(serverBD.niDriver), CA_DO_DRIVEBYS, TRUE)
				ENDIF
				
				//Combat Movement
				iRand = GET_RANDOM_INT_IN_RANGE(0, 4)
				IF iRand = 0
					SET_PED_COMBAT_MOVEMENT(NET_TO_PED(serverBD.niDriver), CM_WILLADVANCE)
				ELIF iRand = 1
					SET_PED_COMBAT_MOVEMENT(NET_TO_PED(serverBD.niDriver), CM_DEFENSIVE)
				ELIF iRand = 2
					SET_PED_COMBAT_MOVEMENT(NET_TO_PED(serverBD.niDriver), CM_WILLRETREAT)
				ENDIF
					
				/*OPEN_SEQUENCE_TASK(seqPlane)
					TASK_VEHICLE_DRIVE_TO_COORD(NULL, NET_TO_VEH(serverBD.niCar), serverBD.vLaunchLocation+<<0, 0, PLANE_DESTINATION_HEIGHT>>, CAR_SPEED, DRIVINGSTYLE_NORMAL, serverBD.CarModel, DRIVINGMODE_PLOUGHTHROUGH, 15, -1)
					TASK_VEHICLE_DRIVE_TO_COORD(NULL, NET_TO_VEH(serverBD.niCar), vRandomEnd, PLANE_SPEED, DRIVINGSTYLE_NORMAL, serverBD.CarModel, DRIVINGMODE_PLOUGHTHROUGH, -1, -1)
				CLOSE_SEQUENCE_TASK(seqPlane)
				TASK_PERFORM_SEQUENCE(NET_TO_PED(serverBD.niDriver), seqPlane)*/
				
				//TASK_VEHICLE_DRIVE_WANDER(NET_TO_PED(serverBD.niDriver), NET_TO_VEH(serverBD.niCar), CAR_SPEED, DRIVINGMODE_AVOIDCARS)
				
				SET_PED_ALLOW_MINOR_REACTIONS_AS_MISSION_PED(NET_TO_PED(serverBD.niDriver), TRUE)
				SET_PED_GET_OUT_UPSIDE_DOWN_VEHICLE(NET_TO_PED(serverBD.niDriver), TRUE)
				SET_PED_CONFIG_FLAG(NET_TO_PED(serverBD.niDriver), PCF_GetOutUndriveableVehicle, TRUE)
				SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(NET_TO_PED(serverBD.niDriver), KNOCKOFFVEHICLE_HARD)
				
				SET_ENTITY_HEALTH(NET_TO_PED(serverBD.niDriver), ROUND(200*g_sMPTunables.fAiHealthModifier))
				
				SET_VEHICLE_RADIO_LOUD(NET_TO_VEH(serverBD.niCar), TRUE)
				
				SET_DISABLE_PRETEND_OCCUPANTS(NET_TO_VEH(serverBD.niCar), TRUE)
				
				CREATE_SEQUENCES()
				TASK_PERFORM_SEQUENCE(NET_TO_PED(serverBD.niDriver), seqJoyride)
				
				SET_MODEL_AS_NO_LONGER_NEEDED(serverBD.DriverModel)
				NET_PRINT_TIME() NET_PRINT("     ---------->     JOYRIDER - CREATED DRIVER ") NET_NL()
			ENDIF
		ENDIF
	ENDIF
	
	//Check we have created the ped
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niDriver)
		NET_PRINT_TIME() NET_PRINT("     ---------->     JOYRIDER - FAILING ON DRIVER CREATION") NET_NL()
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

//PURPOSE: Creates the Plane and Pilot that will deliver the Vehicle
FUNC BOOL CREATE_CAR_AND_DRIVER()
	IF CAN_REGISTER_MISSION_ENTITIES(1, 1, 0, 0)
		IF REQUEST_LOAD_MODEL(serverBD.CarModel)
		AND REQUEST_LOAD_MODEL(serverBD.DriverModel)
			IF CREATE_CAR()
			AND CREATE_DRIVER()
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Controls the stop actions for the Joyrider. Make him stop and leave the vehicle if it is damaged or stuck.
PROC CONTROL_JOYRIDER_BODY()
	
	//DO STOP ACTION
	IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_JoyriderExitDone)
		IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niCar)
		AND NOT IS_NET_PED_INJURED(serverBD.niDriver)
			INT iHealth = GET_ENTITY_HEALTH(NET_TO_VEH(serverBD.niCar))
			FLOAT fSpeed = GET_ENTITY_SPEED(NET_TO_VEH(serverBD.niCar))
			IF iHealth < 200
				CREATE_SEQUENCES()
				TASK_PERFORM_SEQUENCE(NET_TO_PED(serverBD.niDriver), seqStopAndExit)
				SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_JoyriderExitDone)
				NET_PRINT_TIME() NET_PRINT("     ---------->     JOYRIDER - CONTROL_JOYRIDER_BODY - VEH HEALTH < 200 - seqStopAndExit") NET_NL()
			ELIF fSpeed < 3
				IF IS_ENTITY_UPSIDEDOWN(NET_TO_VEH(serverBD.niCar))
				OR IS_VEHICLE_STUCK_TIMER_UP(NET_TO_VEH(serverBD.niCar), VEH_STUCK_JAMMED, 10000)
				OR IS_VEHICLE_STUCK_TIMER_UP(NET_TO_VEH(serverBD.niCar), VEH_STUCK_ON_SIDE, 10000)
				OR IS_VEHICLE_STUCK_TIMER_UP(NET_TO_VEH(serverBD.niCar), VEH_STUCK_ON_ROOF, 10000)
				OR IS_VEHICLE_STUCK_TIMER_UP(NET_TO_VEH(serverBD.niCar), VEH_STUCK_HUNG_UP, 10000)
					CREATE_SEQUENCES()
					TASK_PERFORM_SEQUENCE(NET_TO_PED(serverBD.niDriver), seqExit)
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_JoyriderExitDone)
					NET_PRINT_TIME() NET_PRINT("     ---------->     JOYRIDER - CONTROL_JOYRIDER_BODY - VEH STUCK - seqExit") NET_NL()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Runs through the max num participents and does relevant checks
PROC MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER()
	//INT iParticipant
	
	//Initialise Staggered Loop
	IF iStaggeredParticipant = 0
		bDoEndStaggeredLoopChecks = FALSE
		SET_BIT(serverBD.iServerBitSet, biS_NoPlayerNearJoyrider)
		//NET_PRINT_TIME() NET_PRINT(" ---> JOYRIDER - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER - iStaggeredParticipant RESET") NET_NL()
	ENDIF
	
	BOOL bCarIsDriveable
	IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niCar)
		bCarIsDriveable = TRUE
	ENDIF
	
	//REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iStaggeredParticipant))
			PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iStaggeredParticipant))
			PED_INDEX PlayerPedId = GET_PLAYER_PED(PlayerId)
			
			//** CHECKS THAT DON'T NEED THE PLAYER TO BE ALIVE **//
			
			
			//** CHECKS THAT REQUIRE THE PLAYER TO BE ALIVE **//
			IF IS_NET_PLAYER_OK(PlayerId)
				
				//Check if Joyrider is near nobody
				IF IS_BIT_SET(serverBD.iServerBitSet, biS_NoPlayerNearJoyrider)
					IF bCarIsDriveable = TRUE
						IF IS_ENTITY_AT_ENTITY(PlayerPedId, NET_TO_VEH(serverBD.niCar), <<LEAVE_RANGE, LEAVE_RANGE, LEAVE_RANGE>>)
							CLEAR_BIT(serverBD.iServerBitSet, biS_NoPlayerNearJoyrider)
							//NET_PRINT_TIME() NET_PRINT(" ---> JOYRIDER - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER - biS_NoPlayerNearJoyrider CLEARED") NET_NL()
						ENDIF
					ELSE
						CLEAR_BIT(serverBD.iServerBitSet, biS_NoPlayerNearJoyrider)
						//NET_PRINT_TIME() NET_PRINT(" ---> JOYRIDER - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER - biS_NoPlayerNearJoyrider CLEARED - CAR NOT DRIVEABLE") NET_NL()
					ENDIF
				ENDIF
				
			ENDIF
			
		ENDIF
	//ENDREPEAT
	
	iStaggeredParticipant++
	
	//Reset Staggered Loop
	IF iStaggeredParticipant = NETWORK_GET_MAX_NUM_PARTICIPANTS()
		iStaggeredParticipant = 0
		bDoEndStaggeredLoopChecks = TRUE
		//NET_PRINT_TIME() NET_PRINT(" ---> JOYRIDER - MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER - iStaggeredParticipant RESET") NET_NL()
	ENDIF
ENDPROC


/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  MISSION PROCS                  //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

//PURPOSE: Process the JOYRIDER stages for the Client
PROC PROCESS_JOYRIDER_CLIENT()
	
	//playerBD[PARTICIPANT_ID_TO_INT()].eJoyriderStage = serverBD.eJoyriderStage
	
	SWITCH playerBD[PARTICIPANT_ID_TO_INT()].eJoyriderStage		
		CASE eAD_JOYRIDE
			CREATE_SEQUENCES()
			CONTROL_JOYRIDER_BODY()
			IF serverBD.eJoyriderStage > eAD_JOYRIDE
				//ADD_VEHICLE_BLIP()
				playerBD[PARTICIPANT_ID_TO_INT()].eJoyriderStage = eAD_CLEANUP
				NET_PRINT_TIME() NET_PRINT("     ---------->     JOYRIDER - PLAYER STAGE = eAD_CLEANUP    <----------     ") NET_NL()
			ENDIF
		BREAK
						
		CASE eAD_CLEANUP
			NET_PRINT_TIME() NET_PRINT("     ---------->     JOYRIDER - SCRIPT CLEANUP B     <----------     ") NET_NL()
			SCRIPT_CLEANUP()
		BREAK
	ENDSWITCH
ENDPROC

//PURPOSE: Process the JOYRIDER stages for the Server
PROC PROCESS_VEHICLE_DROP_SERVER()
	SWITCH serverBD.eJoyriderStage		
		CASE eAD_JOYRIDE
			//IF CONTROL_VEHICLE_DROPPING()
			//	serverBD.eJoyriderStage = eAD_PICKUP_VEHICLE
			//	NET_PRINT_TIME() NET_PRINT("     ---------->     JOYRIDER - SERVER STAGE = eAD_PICKUP_VEHICLE    <----------     ") NET_NL()
			//ENDIF
		BREAK
						
		CASE eAD_CLEANUP
			
		BREAK
	ENDSWITCH
ENDPROC

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                      Main Loop                      //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
SCRIPT(MP_MISSION_DATA missionScriptArgs)
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
	AND IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE)
		#IF IS_DEBUG_BUILD	
			NET_PRINT_TIME() NET_PRINT("MP: Starting mission ") NET_NL()
		#ENDIF
		
		// Carry out all the initial game starting duties. 
		PROCESS_PRE_GAME(missionScriptArgs)	
		
		// Setup widgets. Make sure this is done after PROCESS_PRE_GAME()
		#IF IS_DEBUG_BUILD
			CREATE_WIDGETS()
		#ENDIF
	ELSE
		#IF IS_DEBUG_BUILD NET_LOG("JOYRIDER - NETWORK_IS_GAME_IN_PROGRESS = FALSE")	#ENDIF
		NET_PRINT_TIME() NET_PRINT("     ---------->     JOYRIDER - MISSION END - NETWORK_IS_GAME_IN_PROGRESS = FALSE - SCRIPT CLEANUP P     <----------     ") NET_NL()
		SCRIPT_CLEANUP()
	ENDIF
	
	// Main loop
	WHILE TRUE

		// One wait to rule them all. This can be the ONLY wait from here on in.....
		MP_LOOP_WAIT_ZERO()
				
		// If we have a match end event, bail
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		OR SHOULD_MP_AMBIENT_SCRIPT_TERMINATE(MPAM_TYPE_JOYRIDER)
		//OR NOT IS_NET_PLAYER_OK(PLAYER_ID(), TRUE, TRUE)	//FALSE, TRUE)
			#IF IS_DEBUG_BUILD NET_LOG("SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE")	#ENDIF
			NET_PRINT_TIME() NET_PRINT("     ---------->     JOYRIDER - MISSION END - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE - SCRIPT CLEANUP B     <----------     ") NET_NL()
			SCRIPT_CLEANUP()
		ENDIF
		
		//Check if player is in a tutorial session and end
		IF NETWORK_IS_IN_TUTORIAL_SESSION()
			#IF IS_DEBUG_BUILD NET_LOG("PLAYER IN TUT SESSION")	#ENDIF
			NET_PRINT_TIME() NET_PRINT("     ---------->     JOYRIDER - MISSION END - PLAYER IN TUTORIAL SESSION - SCRIPT CLEANUP C     <----------     ") NET_NL()
			SCRIPT_CLEANUP()
		ENDIF
		
		// Deal with the debug
		#IF IS_DEBUG_BUILD		
			UPDATE_WIDGETS()
		#ENDIF		
	
		// If we need to leave the mission, make sure we just straight to the end stage and terminate the script - DON'T NEED FOR AMBIENT MISSION
		/*IF SHOULD_PLAYER_LEAVE_MP_MISSION() 
			#IF IS_DEBUG_BUILD
			NET_PRINT_TIME() NET_PRINT( GET_MP_MISSION_NAME(thisMission), " Leave mission = TRUE \n")
			#ENDIF
			playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_LEAVE
			#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_LEAVE")	#ENDIF
		ENDIF*/
		
		// -----------------------------------
		// Process your game logic.....
		SWITCH GET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT())
			
			// Wait untill the server gives the all go before moving on
			CASE GAME_STATE_INI
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					CREATE_SEQUENCES()
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_RUNNING
					NET_PRINT_TIME() NET_PRINT("     ---------->     JOYRIDER - playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_RUNNING    <----------     ") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_RUNNING")	#ENDIF
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY
					NET_PRINT_TIME() NET_PRINT("     ---------->     JOYRIDER - playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY 1    <----------     ") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_TERMINATE_DELAY")	#ENDIF
				ENDIF
			BREAK
			
			// Main gameplay state.
			CASE GAME_STATE_RUNNING
				
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					PROCESS_JOYRIDER_CLIENT()
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY
					NET_PRINT_TIME() NET_PRINT("     ---------->     JOYRIDER - playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY 2   <----------     ") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_TERMINATE_DELAY")	#ENDIF
				ENDIF
				
				// Make the player end the mission if they leave the Area
				/*IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), serverBD.vLaunchLocation, <<LEAVE_RANGE, LEAVE_RANGE, LEAVE_RANGE>>)
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
					NET_PRINT_TIME() NET_PRINT("     ---------->     JOYRIDER - playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END 6 - MISSION END - PLAYER NOT IN AREA ") NET_PRINT_VECTOR(serverBD.vLaunchLocation) NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_END 6")	#ENDIF
				ENDIF*/
			BREAK
			
			CASE GAME_STATE_TERMINATE_DELAY 
				SET_MISSION_FINISHED(serverBD.MissionTerminateDelayTimer)
		      	IF IS_MISSION_READY_TO_CLEANUP(serverBD.MissionTerminateDelayTimer)
		            playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
					NET_PRINT_TIME() NET_PRINT("     ---------->     JOYRIDER - playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END 3    <----------     ") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_END 3")	#ENDIF
		      	ENDIF
			BREAK
			
			// The game stage the local player is placed when we want him to leave a mission
			CASE GAME_STATE_LEAVE
				//LEAVE_MY_MP_MISSION(thisMission)
				playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
				NET_PRINT_TIME() NET_PRINT("     ---------->     JOYRIDER - playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END 2    <----------     ") NET_NL()
				#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_END 2")	#ENDIF
			FALLTHRU
			
			//Cleans up then terminates the mission
			CASE GAME_STATE_END
				NET_PRINT_TIME() NET_PRINT("     ---------->     JOYRIDER - SCRIPT CLEANUP A     <----------     ") NET_NL()
				SCRIPT_CLEANUP()
			BREAK

		ENDSWITCH
		
		// -----------------------------------
		// Process server game logic		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			
			SWITCH GET_SERVER_MISSION_STATE()
				
				// For CnC group missions, we may want to wait for all players to have launched the script
				CASE GAME_STATE_INI
					IF GOT_FIRST_DESTINATION_POINT()
						IF CREATE_CAR_AND_DRIVER()
							serverBD.iServerGameState = GAME_STATE_RUNNING
							NET_PRINT_TIME() NET_PRINT("     ---------->     JOYRIDER - serverBD.iServerGameState = GAME_STATE_RUNNING    <----------     ") NET_NL()
							#IF IS_DEBUG_BUILD NET_LOG_Host("iServerGameState = GAME_STATE_RUNNING")	#ENDIF
						ENDIF
					ENDIF
				BREAK
				
				// Look for game end conditions
				CASE GAME_STATE_RUNNING
					
					MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER()
					PROCESS_VEHICLE_DROP_SERVER()
					
					#IF IS_DEBUG_BUILD
						IF bHostEndMissionNow
						OR MPGlobalsAmbience.bKillActiveEvent
							MPGlobalsAmbience.bKillActiveEvent = FALSE
							serverBD.iServerGameState = GAME_STATE_END
							NET_PRINT_TIME() NET_PRINT("     ---------->     JOYRIDER - serverBD.iServerGameState = GAME_STATE_END 1    <----------     ") NET_NL()
							#IF IS_DEBUG_BUILD NET_LOG_Host("iServerGameState = GAME_STATE_END 1")	#ENDIF
						ENDIF
					#ENDIF	
					
					IF HAVE_MISSION_END_CONDITIONS_BEEN_MET()
						serverBD.iServerGameState = GAME_STATE_END
						NET_PRINT_TIME() NET_PRINT("     ---------->     JOYRIDER - serverBD.iServerGameState = GAME_STATE_END 4    <----------     ") NET_NL()
						#IF IS_DEBUG_BUILD NET_LOG_Host("iServerGameState = GAME_STATE_END 4")	#ENDIF
					ENDIF
					
				BREAK
				
				CASE GAME_STATE_END
				BREAK
				
			ENDSWITCH
			
		ENDIF
		
	ENDWHILE
	
ENDSCRIPT
