//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        AM_CASINO_PEDS.sch																						//
// Description: Script for controlling the Casino peds.																	//
// Written by:  Online Technical Team: Scott Ranken																		//
// Date:  		08/02/19																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
USING "globals.sch"
USING "net_include.sch"
USING "net_wait_zero.sch"
USING "ped_component_packing.sch"
USING "net_casino.sch"
using "RC_helper_functions.sch" 
#IF IS_DEBUG_BUILD
USING "net_spawn_debug.sch"
#ENDIF

CONST_INT PENTHOUSE_PED_MAX_PROPS 3

#IF FEATURE_CASINO

STRUCT SERVER_BROADCAST_DATA
	BOOL bServerDataInitialised = FALSE
	INT iLevel = 0
	INT iLayout = -1
ENDSTRUCT

ENUM PENTHOUSE_ACTIVITY
	
	PA_AMBIENT,
	PA_DANCING_WITH_BEER,
	PA_SIT_DRINKING,
	
	PA_HANGOUT_M_1A,
	PA_HANGOUT_M_1B,
	PA_HANGOUT_M_2A,
	PA_HANGOUT_M_2B,
	PA_HANGOUT_M_3A,
	PA_HANGOUT_M_3B,
	PA_HANGOUT_CONVO_M_1A,
	PA_HANGOUT_CONVO_M_1B,
	PA_HANGOUT_CONVO_M_2A,
	PA_HANGOUT_CONVO_M_2B,
	PA_HANGOUT_CONVO_M_3A,
	PA_HANGOUT_CONVO_M_3B,
	PA_HANGOUT_WITHDRINK_M_01A,
	PA_HANGOUT_WITHDRINK_M_01B,
	PA_HANGOUT_CONVO_WITHDRINK_M_01A,
	PA_HANGOUT_CONVO_WITHDRINK_M_01B,
	PA_HANGOUT_F_1A,
	PA_HANGOUT_F_1B,
	PA_HANGOUT_F_2A,
	PA_HANGOUT_F_2B,
	PA_HANGOUT_F_3A,
	PA_HANGOUT_F_3B,
	PA_HANGOUT_CONVO_F_1A,
	PA_HANGOUT_CONVO_F_1B,
	PA_HANGOUT_CONVO_F_2A,
	PA_HANGOUT_CONVO_F_2B,
	PA_HANGOUT_CONVO_F_3A,
	PA_HANGOUT_CONVO_F_3B,
	PA_HANGOUT_WITHDRINK_F_01A,
	PA_HANGOUT_WITHDRINK_F_01B,
	PA_HANGOUT_CONVO_WITHDRINK_F_01A,
	PA_HANGOUT_CONVO_WITHDRINK_F_01B,	
	PA_NIGHTCLUB_DRINKING_BEER_FEMALE,
	PA_NIGHTCLUB_mi_loop_f02,
	PA_NIGHTCLUB_mi_idle_c_f02,
	PA_NIGHTCLUB_mi_to_li_f02,
	PA_NIGHTCLUB_hi_dance_prop_13_v2_male,
	PA_NIGHTCLUB_hi_dance_prop_09_v1_male,
	PA_NIGHTCLUB_hi_dance_prop_17_v1_male,
	PA_NIGHTCLUB_hi_dance_crowd_15_v1_male,
	PA_NIGHTCLUB_hi_dance_crowd_09_v1_male,
	PA_NIGHTCLUB_hi_dance_crowd_13_v2_male,
	PA_NIGHTCLUB_trans_dance_crowd_li_to_mi_11_v1_male,
	PA_NIGHTCLUB_mi_dance_crowd_17_v2_female,
	PA_NIGHTCLUB_hi_dance_crowd_17_v1_female,
	PA_NIGHTCLUB_ti_loop_m01,
	PA_NIGHTCLUB_ti_loop_f02,
	PA_NIGHTCLUB_ti_idle_d_f02,
	PA_NIGHTCLUB_ti_idle_a_m03,
	PA_NIGHTCLUB_ti_idle_b_f02,
	PA_NIGHTCLUB_ti_idle_a_f02,
	PA_NIGHTCLUB_ti_loop_f01,
	PA_NIGHTCLUB_ti_idle_b_m02,
	PA_NIGHTCLUB_ti_idle_b_f01,
	PA_NIGHTCLUB_ti_idle_c_m03,
	PA_NIGHTCLUB_ti_idle_c_f03,
	PA_NIGHTCLUB_ti_loop_m02,
	PA_NIGHTCLUB_ti_idle_c_m02,
	PA_NIGHTCLUB_ti_idle_b_f03,
	PA_NIGHTCLUB_ti_idle_c_f02,
	PA_TOTAL

ENDENUM

INT iPedsCreatedThisFrame = 0
INT iAudioState

STRUCT PED_DATA
	PED_INDEX PedID
	OBJECT_INDEX ObjectID
	INT iModel
	PED_CLOTHING_COMPONENT_CONFIG ComponentConfig
	BOOL bAttachProp[PENTHOUSE_PED_MAX_PROPS]
	INT iPropPosition[PENTHOUSE_PED_MAX_PROPS]
	INT iPropVariant[PENTHOUSE_PED_MAX_PROPS]
	VECTOR vPosition
	FLOAT fHeading
	INT iActivity
	INT iPedToHeadTrack
	INT iLevel = 1
	#IF IS_DEBUG_BUILD
	BOOL bHasBeenEdited
	BOOL bApplyChanges
	TEXT_WIDGET_ID twModel
	BOOL bModelChanged
	#ENDIF
ENDSTRUCT




SERVER_BROADCAST_DATA ServerBD
CONST_INT MAX_NUM_PENTHOUSE_PEDS 50
CONST_INT MAX_NUM_PENTHOUSE_PEDS_CREATED_PER_FRAME 2
PED_DATA PenthousePed[MAX_NUM_PENTHOUSE_PEDS]


#IF IS_DEBUG_BUILD
STRING sPath = "X:/gta5/titleupdate/dev_ng/"
TEXT_LABEL_63 sFile
BOOL bOutputToFile
BOOL bShowDebug
BOOL bRecreatePeds
//BOOL bUpdatePositions
INT iLastLayout = -1
INT iCurrentLayout = -1
INT iLastLevel = -1
INT iCurrentLevel = -1
INT iCount_Level[5]

FUNC STRING GET_PED_MODEL_COMP_STRING(INT iComp)
	STRING sComp = ""
	SWITCH iComp
		CASE 0	sComp = "PED_COMP_HEAD"			BREAK
		CASE 1	sComp = "PED_COMP_BERD"			BREAK
		CASE 2	sComp = "PED_COMP_HAIR"			BREAK
		CASE 3	sComp = "PED_COMP_TORSO"		BREAK
		CASE 4	sComp = "PED_COMP_LEG"			BREAK
		CASE 5	sComp = "PED_COMP_HAND"			BREAK
		CASE 6	sComp = "PED_COMP_FEET"			BREAK
		CASE 7	sComp = "PED_COMP_TEETH"		BREAK
		CASE 8	sComp = "PED_COMP_SPECIAL"		BREAK
		CASE 9	sComp = "PED_COMP_SPECIAL2"		BREAK
		CASE 10	sComp = "PED_COMP_DECL"			BREAK 
		CASE 11 sComp = "PED_COMP_JBIB"			BREAK
	ENDSWITCH
	RETURN sComp
ENDFUNC

#ENDIF


PROC PROCESS_PRE_GAME()
	PRINTLN("[CASINO_PEDS] PROCESS_PRE_GAME")
	
	// Changed the following to a while with the 'TRY' function to fix url:bugstar:4877273 
	WHILE NOT NETWORK_TRY_TO_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE, g_iPenthousePedScriptInstance)
		PRINTLN("[CASINO_PEDS] PROCESS_PRE_GAME - Waiting to be able to launch")
		WAIT(0)
	ENDWHILE
	
	g_iPenthousePedScriptInstance++
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	
//	#IF IS_DEBUG_BUILD
//	#IF SCRIPT_PROFILER_ACTIVE
//	CREATE_SCRIPT_PROFILER_WIDGET() 
//	#ENDIF
//	#ENDIF
ENDPROC


PROC DELETE_PENTHOUSE_PED(PED_DATA &Data)
	IF DOES_ENTITY_EXIST(Data.PedID)
		DELETE_PED(Data.PedID)
	ENDIF
	IF DOES_ENTITY_EXIST(Data.ObjectID)
		DELETE_OBJECT(Data.ObjectID)
	ENDIF
ENDPROC

PROC CLEANUP_ALL_PENTHOUSE_PEDS()
	INT i
	REPEAT MAX_NUM_PENTHOUSE_PEDS i
		DELETE_PENTHOUSE_PED(PenthousePed[i])
	ENDREPEAT
ENDPROC

PROC SET_PENTHOUSE_PED_PROPERTIES(PED_DATA &Data)

	SET_ENTITY_CAN_BE_DAMAGED(Data.PedID, FALSE)
	SET_PED_AS_ENEMY(Data.PedID, FALSE)
	SET_CURRENT_PED_WEAPON(Data.PedID, WEAPONTYPE_UNARMED, TRUE)
	
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(Data.PedID, TRUE)
	SET_PED_RESET_FLAG(Data.PedID, PRF_DisablePotentialBlastReactions, TRUE)
	SET_PED_CONFIG_FLAG(Data.PedID, PCF_UseKinematicModeWhenStationary, TRUE)
	SET_PED_CONFIG_FLAG(Data.PedID, PCF_DontActivateRagdollFromExplosions, TRUE)
	SET_PED_CONFIG_FLAG(Data.PedID, PCF_DontActivateRagdollFromVehicleImpact, TRUE)
	
	SET_PED_CAN_EVASIVE_DIVE(Data.PedID, FALSE)
	SET_TREAT_AS_AMBIENT_PED_FOR_DRIVER_LOCKON(Data.PedID, TRUE)
	SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(Data.PedID, FALSE)
	SET_PED_CAN_RAGDOLL(Data.PedID, FALSE)
	SET_PED_CONFIG_FLAG(Data.PedID, PCF_DisableExplosionReactions, TRUE)
	CLEAR_PED_TASKS(Data.PedID)
ENDPROC

FUNC MODEL_NAMES GET_MODEL_FOR_PED(PED_DATA &Data)
	SWITCH Data.iModel
		CASE 0	RETURN INT_TO_ENUM(MODEL_NAMES, HASH("a_f_y_smartcaspat_01"))
		CASE 1 	RETURN INT_TO_ENUM(MODEL_NAMES, HASH("a_f_y_clubcust_01"))
		CASE 2 	RETURN INT_TO_ENUM(MODEL_NAMES, HASH("a_f_y_clubcust_03"))
		CASE 3 	RETURN INT_TO_ENUM(MODEL_NAMES, HASH("A_F_Y_Genhot_01"))
		CASE 4 	RETURN INT_TO_ENUM(MODEL_NAMES, HASH("A_F_Y_BevHills_02"))
		CASE 5	RETURN INT_TO_ENUM(MODEL_NAMES, HASH("a_m_y_smartcaspat_01"))
		CASE 6	RETURN INT_TO_ENUM(MODEL_NAMES, HASH("A_M_Y_CarClub_01"))
		CASE 7 	RETURN INT_TO_ENUM(MODEL_NAMES, HASH("G_M_Y_BallaOrig_01"))
		CASE 8 	RETURN INT_TO_ENUM(MODEL_NAMES, HASH("A_M_M_SouCent_01"))
		CASE 9 	RETURN INT_TO_ENUM(MODEL_NAMES, HASH("G_M_Y_FamFOR_01"))
		CASE 10 RETURN INT_TO_ENUM(MODEL_NAMES, HASH("a_m_y_clubcust_01"))
		CASE 11 RETURN INT_TO_ENUM(MODEL_NAMES, HASH("a_m_y_clubcust_02"))
		CASE 12 RETURN INT_TO_ENUM(MODEL_NAMES, HASH("A_M_Y_StudioParty_01"))
		CASE 13 RETURN INT_TO_ENUM(MODEL_NAMES, HASH("G_M_Y_BallaSout_01"))
		CASE 14 RETURN INT_TO_ENUM(MODEL_NAMES, HASH("G_M_Y_FamCA_01"))
		CASE 15 RETURN INT_TO_ENUM(MODEL_NAMES, HASH("G_M_Y_FamDNF_01"))
		CASE 16 RETURN INT_TO_ENUM(MODEL_NAMES, HASH("G_M_Y_MexGoon_03"))
		CASE 17 RETURN INT_TO_ENUM(MODEL_NAMES, HASH("A_M_M_StudioParty_01"))
		CASE 18 RETURN INT_TO_ENUM(MODEL_NAMES, HASH("A_M_Y_SouCent_02"))
		CASE 19 RETURN INT_TO_ENUM(MODEL_NAMES, HASH("a_m_y_vinewood_01"))
		CASE 20 RETURN INT_TO_ENUM(MODEL_NAMES, HASH("a_m_y_vinewood_02"))
		CASE 21 RETURN INT_TO_ENUM(MODEL_NAMES, HASH("A_M_Y_EastSA_02"))
		CASE 22 RETURN INT_TO_ENUM(MODEL_NAMES, HASH("A_M_Y_indian_01"))
		CASE 23 RETURN INT_TO_ENUM(MODEL_NAMES, HASH("A_F_Y_CarClub_01"))
		CASE 24 RETURN INT_TO_ENUM(MODEL_NAMES, HASH("A_F_Y_StudioParty_01"))
		CASE 25 RETURN INT_TO_ENUM(MODEL_NAMES, HASH("G_F_Y_ballas_01"))
		CASE 26 RETURN INT_TO_ENUM(MODEL_NAMES, HASH("A_F_Y_StudioParty_02"))
		CASE 27 RETURN INT_TO_ENUM(MODEL_NAMES, HASH("G_F_Y_Vagos_01"))
	ENDSWITCH
	
	RETURN INT_TO_ENUM(MODEL_NAMES, HASH("a_f_y_smartcaspat_01"))
ENDFUNC

FUNC BOOL IS_PED_MODEL_FEMALE(PED_DATA &Data)
	SWITCH Data.iModel
		CASE 0	RETURN TRUE
		CASE 1 	RETURN TRUE
		CASE 2 	RETURN TRUE
		CASE 3 	RETURN TRUE
		CASE 4 	RETURN TRUE
		CASE 5	RETURN FALSE
		CASE 6	RETURN FALSE
		CASE 7 	RETURN FALSE
		CASE 8 	RETURN FALSE
		CASE 9 	RETURN FALSE
		CASE 10 RETURN FALSE
		CASE 11 RETURN FALSE
		CASE 12 RETURN FALSE
		CASE 13 RETURN FALSE
		CASE 14 RETURN FALSE
		CASE 15 RETURN FALSE
		CASE 16 RETURN FALSE
		CASE 17 RETURN FALSE
		CASE 18 RETURN FALSE
		CASE 19 RETURN FALSE
		CASE 20 RETURN FALSE
		CASE 21 RETURN FALSE
		CASE 22 RETURN FALSE
		CASE 23 RETURN TRUE
		CASE 24 RETURN TRUE
		CASE 25 RETURN TRUE
		CASE 26 RETURN TRUE
		CASE 27 RETURN TRUE
		
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC
FUNC VECTOR GET_BONE_OFFSET_POSITION_TO_ATTACH_TO(PED_DATA &Data)
	SWITCH INT_TO_ENUM(PENTHOUSE_ACTIVITY, Data.iActivity)
		CASE PA_DANCING_WITH_BEER
		CASE PA_SIT_DRINKING
			RETURN <<0,0,0.01>>
		BREAK
		CASE PA_NIGHTCLUB_hi_dance_prop_17_v1_male
		CASE PA_NIGHTCLUB_hi_dance_prop_13_v2_male
		CASE PA_NIGHTCLUB_hi_dance_prop_09_v1_male
			 RETURN <<0.00, 0.0, 0.11>>
		BREAK
	ENDSWITCH
	RETURN <<0,0,0>>
ENDFUNC

FUNC MODEL_NAMES GET_PROP_FOR_ACTIVITY(PED_DATA &Data)
	
	SWITCH INT_TO_ENUM(PENTHOUSE_ACTIVITY, Data.iActivity)
		CASE PA_DANCING_WITH_BEER
		CASE PA_SIT_DRINKING
		CASE PA_HANGOUT_WITHDRINK_M_01A
		CASE PA_HANGOUT_WITHDRINK_M_01B
		CASE PA_HANGOUT_CONVO_WITHDRINK_M_01A
		CASE PA_HANGOUT_CONVO_WITHDRINK_M_01B
		CASE PA_HANGOUT_WITHDRINK_F_01A
		CASE PA_HANGOUT_WITHDRINK_F_01B
		CASE PA_HANGOUT_CONVO_WITHDRINK_F_01A
		CASE PA_HANGOUT_CONVO_WITHDRINK_F_01B	
		CASE PA_NIGHTCLUB_DRINKING_BEER_FEMALE
		CASE PA_NIGHTCLUB_hi_dance_prop_17_v1_male
		CASE PA_NIGHTCLUB_hi_dance_prop_13_v2_male
		CASE PA_NIGHTCLUB_hi_dance_prop_09_v1_male
			RETURN PROP_AMB_BEER_BOTTLE
	ENDSWITCH
	RETURN PROP_AMB_BEER_BOTTLE
ENDFUNC

FUNC BOOL DOES_ACTIVITY_HAVE_PROP(PED_DATA &Data)
	SWITCH INT_TO_ENUM(PENTHOUSE_ACTIVITY, Data.iActivity)
		CASE PA_DANCING_WITH_BEER
		CASE PA_SIT_DRINKING
		CASE PA_HANGOUT_WITHDRINK_M_01A
		CASE PA_HANGOUT_WITHDRINK_M_01B
		CASE PA_HANGOUT_CONVO_WITHDRINK_M_01A
		CASE PA_HANGOUT_CONVO_WITHDRINK_M_01B
		CASE PA_HANGOUT_WITHDRINK_F_01A
		CASE PA_HANGOUT_WITHDRINK_F_01B
		CASE PA_HANGOUT_CONVO_WITHDRINK_F_01A
		CASE PA_HANGOUT_CONVO_WITHDRINK_F_01B	
		CASE PA_NIGHTCLUB_DRINKING_BEER_FEMALE
		CASE PA_NIGHTCLUB_hi_dance_prop_17_v1_male
		CASE PA_NIGHTCLUB_hi_dance_prop_13_v2_male
		CASE PA_NIGHTCLUB_hi_dance_prop_09_v1_male
			RETURN TRUE
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC PED_BONETAG GET_BONE_FOR_ACTIVITY_PROP(PED_DATA &Data)
	SWITCH INT_TO_ENUM(PENTHOUSE_ACTIVITY, Data.iActivity)
		CASE PA_DANCING_WITH_BEER		
		CASE PA_HANGOUT_WITHDRINK_M_01B		
		CASE PA_HANGOUT_CONVO_WITHDRINK_M_01B
		CASE PA_HANGOUT_WITHDRINK_F_01B	
		CASE PA_HANGOUT_CONVO_WITHDRINK_F_01B	
		CASE PA_NIGHTCLUB_DRINKING_BEER_FEMALE
		CASE PA_NIGHTCLUB_hi_dance_prop_17_v1_male
		CASE PA_NIGHTCLUB_hi_dance_prop_13_v2_male
		CASE PA_NIGHTCLUB_hi_dance_prop_09_v1_male
			//IF (Data.bFemale)
				RETURN BONETAG_PH_R_HAND
			//ELSE
			//RETURN BONETAG_PH_L_HAND
			//ENDIF
		BREAK
		CASE PA_HANGOUT_CONVO_WITHDRINK_F_01A
		CASE PA_HANGOUT_WITHDRINK_F_01A
		CASE PA_HANGOUT_WITHDRINK_M_01A
		CASE PA_HANGOUT_CONVO_WITHDRINK_M_01A
			RETURN BONETAG_PH_L_HAND
		BREAK
		CASE PA_SIT_DRINKING
			RETURN BONETAG_PH_R_HAND
		BREAK
	ENDSWITCH
	RETURN BONETAG_PH_R_HAND
ENDFUNC


FUNC STRING GET_ANIM_DICTIONARY_FOR_ACTIVITY(PED_DATA &Data)

	SWITCH INT_TO_ENUM(PENTHOUSE_ACTIVITY, Data.iActivity)
		CASE PA_AMBIENT							RETURN "ANIM@AMB@CASINO@PEDS@"
		CASE PA_DANCING_WITH_BEER 				RETURN "ANIM@AMB@CASINO@PEDS@"
		CASE PA_SIT_DRINKING 					RETURN "ANIM@AMB@CASINO@PEDS@"
		CASE PA_HANGOUT_M_1A 					RETURN "ANIM@AMB@CASINO@HANGOUT@PED_MALE@STAND@01A@IDLES"
		CASE PA_HANGOUT_M_1B 					RETURN "ANIM@AMB@CASINO@HANGOUT@PED_MALE@STAND@01B@IDLES"
		CASE PA_HANGOUT_M_2A					RETURN "ANIM@AMB@CASINO@HANGOUT@PED_MALE@STAND@02A@IDLES"
		CASE PA_HANGOUT_M_2B					RETURN "ANIM@AMB@CASINO@HANGOUT@PED_MALE@STAND@02B@IDLES"
		CASE PA_HANGOUT_M_3A					RETURN "ANIM@AMB@CASINO@HANGOUT@PED_MALE@STAND@03A@IDLES"
		CASE PA_HANGOUT_M_3B					RETURN "ANIM@AMB@CASINO@HANGOUT@PED_MALE@STAND@03B@IDLES"
		CASE PA_HANGOUT_CONVO_M_1A				RETURN "ANIM@AMB@CASINO@HANGOUT@PED_MALE@STAND@01A@IDLES_CONVO"
		CASE PA_HANGOUT_CONVO_M_1B				RETURN "ANIM@AMB@CASINO@HANGOUT@PED_MALE@STAND@01B@IDLES_CONVO"
		CASE PA_HANGOUT_CONVO_M_2A				RETURN "ANIM@AMB@CASINO@HANGOUT@PED_MALE@STAND@02A@IDLES_CONVO"
		CASE PA_HANGOUT_CONVO_M_2B				RETURN "ANIM@AMB@CASINO@HANGOUT@PED_MALE@STAND@02B@IDLES_CONVO"
		CASE PA_HANGOUT_CONVO_M_3A				RETURN "ANIM@AMB@CASINO@HANGOUT@PED_MALE@STAND@03A@IDLES_CONVO"
		CASE PA_HANGOUT_CONVO_M_3B				RETURN "ANIM@AMB@CASINO@HANGOUT@PED_MALE@STAND@03B@IDLES_CONVO"
		CASE PA_HANGOUT_WITHDRINK_M_01A			RETURN "ANIM@AMB@CASINO@HANGOUT@PED_MALE@STAND_WITHDRINK@01A@IDLES"
		CASE PA_HANGOUT_WITHDRINK_M_01B			RETURN "ANIM@AMB@CASINO@HANGOUT@PED_MALE@STAND_WITHDRINK@01B@IDLES"
		CASE PA_HANGOUT_CONVO_WITHDRINK_M_01A	RETURN "ANIM@AMB@CASINO@HANGOUT@PED_MALE@STAND_WITHDRINK@01A@IDLES_CONVO"
		CASE PA_HANGOUT_CONVO_WITHDRINK_M_01B	RETURN "ANIM@AMB@CASINO@HANGOUT@PED_MALE@STAND_WITHDRINK@01B@IDLES_CONVO"
		CASE PA_HANGOUT_F_1A					RETURN "ANIM@AMB@CASINO@HANGOUT@PED_FEMALE@STAND@01A@IDLES"					
		CASE PA_HANGOUT_F_1B					RETURN "ANIM@AMB@CASINO@HANGOUT@PED_FEMALE@STAND@01B@IDLES"
		CASE PA_HANGOUT_F_2A					RETURN "ANIM@AMB@CASINO@HANGOUT@PED_FEMALE@STAND@02A@IDLES"
		CASE PA_HANGOUT_F_2B					RETURN "ANIM@AMB@CASINO@HANGOUT@PED_FEMALE@STAND@02B@IDLES"
		CASE PA_HANGOUT_F_3A					RETURN "ANIM@AMB@CASINO@HANGOUT@PED_FEMALE@STAND@03A@IDLES"
		CASE PA_HANGOUT_F_3B					RETURN "ANIM@AMB@CASINO@HANGOUT@PED_FEMALE@STAND@03B@IDLES"
		CASE PA_HANGOUT_CONVO_F_1A				RETURN "ANIM@AMB@CASINO@HANGOUT@PED_FEMALE@STAND@01A@IDLES_CONVO"
		CASE PA_HANGOUT_CONVO_F_1B				RETURN "ANIM@AMB@CASINO@HANGOUT@PED_FEMALE@STAND@01B@IDLES_CONVO"
		CASE PA_HANGOUT_CONVO_F_2A				RETURN "ANIM@AMB@CASINO@HANGOUT@PED_FEMALE@STAND@02A@IDLES_CONVO"		
		CASE PA_HANGOUT_CONVO_F_2B				RETURN "ANIM@AMB@CASINO@HANGOUT@PED_FEMALE@STAND@02B@IDLES_CONVO"	
		CASE PA_HANGOUT_CONVO_F_3A				RETURN "ANIM@AMB@CASINO@HANGOUT@PED_FEMALE@STAND@03A@IDLES_CONVO"
		CASE PA_HANGOUT_CONVO_F_3B				RETURN "ANIM@AMB@CASINO@HANGOUT@PED_FEMALE@STAND@03B@IDLES_CONVO"
		CASE PA_HANGOUT_WITHDRINK_F_01A			RETURN "ANIM@AMB@CASINO@HANGOUT@PED_FEMALE@STAND_WITHDRINK@01A@IDLES"
		CASE PA_HANGOUT_WITHDRINK_F_01B			RETURN "ANIM@AMB@CASINO@HANGOUT@PED_FEMALE@STAND_WITHDRINK@01B@IDLES"
		CASE PA_HANGOUT_CONVO_WITHDRINK_F_01A	RETURN "ANIM@AMB@CASINO@HANGOUT@PED_FEMALE@STAND_WITHDRINK@01A@IDLES_CONVO"	
		CASE PA_HANGOUT_CONVO_WITHDRINK_F_01B	RETURN "ANIM@AMB@CASINO@HANGOUT@PED_FEMALE@STAND_WITHDRINK@01B@IDLES_CONVO"
		//FIXER
		CASE PA_NIGHTCLUB_DRINKING_BEER_FEMALE						RETURN "anim@amb@nightclub@peds@"
		CASE PA_NIGHTCLUB_mi_loop_f02								RETURN "anim@amb@nightclub_island@dancers@beachdanceprop@"
		CASE PA_NIGHTCLUB_mi_idle_c_f02								RETURN "anim@amb@nightclub_island@dancers@beachdanceprop@"
		CASE PA_NIGHTCLUB_mi_to_li_f02								RETURN "anim@amb@nightclub_island@dancers@beachdanceprop@"
		CASE PA_NIGHTCLUB_hi_dance_prop_13_v2_male					RETURN "anim@amb@nightclub_island@dancers@crowddance_single_props@"
		CASE PA_NIGHTCLUB_hi_dance_prop_09_v1_male					RETURN "anim@amb@nightclub_island@dancers@crowddance_single_props@"
		CASE PA_NIGHTCLUB_hi_dance_prop_17_v1_male					RETURN "anim@amb@nightclub_island@dancers@crowddance_single_props@"
		CASE PA_NIGHTCLUB_hi_dance_crowd_15_v1_male					RETURN "anim@amb@nightclub_island@dancers@crowddance_groups@groupe@"
		CASE PA_NIGHTCLUB_hi_dance_crowd_09_v1_male					RETURN "anim@amb@nightclub_island@dancers@crowddance_groups@groupe@"
		CASE PA_NIGHTCLUB_hi_dance_crowd_13_v2_male					RETURN "anim@amb@nightclub_island@dancers@crowddance_groups@groupe@"
		CASE PA_NIGHTCLUB_trans_dance_crowd_li_to_mi_11_v1_male		RETURN "anim@amb@nightclub_island@dancers@crowddance_groups@groupe@"
		CASE PA_NIGHTCLUB_mi_dance_crowd_17_v2_female				RETURN "anim@amb@nightclub_island@dancers@crowddance_groups@groupe@"
		CASE PA_NIGHTCLUB_hi_dance_crowd_17_v1_female				RETURN "anim@amb@nightclub_island@dancers@crowddance_groups@groupe@"
		CASE PA_NIGHTCLUB_ti_loop_m01								RETURN "anim@amb@nightclub_island@dancers@club@"
		CASE PA_NIGHTCLUB_ti_loop_f02								RETURN "anim@amb@nightclub_island@dancers@club@"
		CASE PA_NIGHTCLUB_ti_idle_d_f02								RETURN "anim@amb@nightclub_island@dancers@club@"
		CASE PA_NIGHTCLUB_ti_idle_a_m03								RETURN "anim@amb@nightclub_island@dancers@club@"
		CASE PA_NIGHTCLUB_ti_idle_b_f02								RETURN "anim@amb@nightclub_island@dancers@club@"
		CASE PA_NIGHTCLUB_ti_idle_a_f02								RETURN "anim@amb@nightclub_island@dancers@club@"
		CASE PA_NIGHTCLUB_ti_loop_f01								RETURN "anim@amb@nightclub_island@dancers@club@"
		CASE PA_NIGHTCLUB_ti_idle_b_m02								RETURN "anim@amb@nightclub_island@dancers@club@"
		CASE PA_NIGHTCLUB_ti_idle_b_f01								RETURN "anim@amb@nightclub_island@dancers@club@"
		CASE PA_NIGHTCLUB_ti_idle_c_m03								RETURN "anim@amb@nightclub_island@dancers@club@"
		CASE PA_NIGHTCLUB_ti_idle_c_f03								RETURN "anim@amb@nightclub_island@dancers@club@"
		CASE PA_NIGHTCLUB_ti_loop_m02								RETURN "anim@amb@nightclub_island@dancers@club@"
		CASE PA_NIGHTCLUB_ti_idle_c_m02								RETURN "anim@amb@nightclub_island@dancers@club@"
		CASE PA_NIGHTCLUB_ti_idle_b_f03								RETURN "anim@amb@nightclub_island@dancers@club@"
		CASE PA_NIGHTCLUB_ti_idle_c_f02								RETURN "anim@amb@nightclub_island@dancers@club@"
		
	ENDSWITCH	
	
	RETURN "ANIM@AMB@CASINO@PEDS@"

ENDFUNC

FUNC STRING GET_ANIM_DICTIONARY_FOR_ACTIVITY_BASE(PED_DATA &Data)

	SWITCH INT_TO_ENUM(PENTHOUSE_ACTIVITY, Data.iActivity)
		CASE PA_HANGOUT_M_1A 					RETURN "ANIM@AMB@CASINO@HANGOUT@PED_MALE@STAND@01A@BASE"
		CASE PA_HANGOUT_M_1B 					RETURN "ANIM@AMB@CASINO@HANGOUT@PED_MALE@STAND@01B@BASE"
		CASE PA_HANGOUT_M_2A					RETURN "ANIM@AMB@CASINO@HANGOUT@PED_MALE@STAND@02A@BASE"
		CASE PA_HANGOUT_M_2B					RETURN "ANIM@AMB@CASINO@HANGOUT@PED_MALE@STAND@02B@BASE"
		CASE PA_HANGOUT_M_3A					RETURN "ANIM@AMB@CASINO@HANGOUT@PED_MALE@STAND@03A@BASE"
		CASE PA_HANGOUT_M_3B					RETURN "ANIM@AMB@CASINO@HANGOUT@PED_MALE@STAND@03B@BASE"
		CASE PA_HANGOUT_CONVO_M_1A				RETURN "ANIM@AMB@CASINO@HANGOUT@PED_MALE@STAND@01A@BASE"
		CASE PA_HANGOUT_CONVO_M_1B				RETURN "ANIM@AMB@CASINO@HANGOUT@PED_MALE@STAND@01B@BASE"
		CASE PA_HANGOUT_CONVO_M_2A				RETURN "ANIM@AMB@CASINO@HANGOUT@PED_MALE@STAND@02A@BASE"
		CASE PA_HANGOUT_CONVO_M_2B				RETURN "ANIM@AMB@CASINO@HANGOUT@PED_MALE@STAND@02B@BASE"
		CASE PA_HANGOUT_CONVO_M_3A				RETURN "ANIM@AMB@CASINO@HANGOUT@PED_MALE@STAND@03A@BASE"
		CASE PA_HANGOUT_CONVO_M_3B				RETURN "ANIM@AMB@CASINO@HANGOUT@PED_MALE@STAND@03B@BASE"
		CASE PA_HANGOUT_WITHDRINK_M_01A			RETURN "ANIM@AMB@CASINO@HANGOUT@PED_MALE@STAND_WITHDRINK@01A@BASE"
		CASE PA_HANGOUT_WITHDRINK_M_01B			RETURN "ANIM@AMB@CASINO@HANGOUT@PED_MALE@STAND_WITHDRINK@01B@BASE"
		CASE PA_HANGOUT_CONVO_WITHDRINK_M_01A	RETURN "ANIM@AMB@CASINO@HANGOUT@PED_MALE@STAND_WITHDRINK@01A@BASE"
		CASE PA_HANGOUT_CONVO_WITHDRINK_M_01B	RETURN "ANIM@AMB@CASINO@HANGOUT@PED_MALE@STAND_WITHDRINK@01B@BASE"
		CASE PA_HANGOUT_F_1A					RETURN "ANIM@AMB@CASINO@HANGOUT@PED_FEMALE@STAND@01A@BASE"					
		CASE PA_HANGOUT_F_1B					RETURN "ANIM@AMB@CASINO@HANGOUT@PED_FEMALE@STAND@01B@BASE"
		CASE PA_HANGOUT_F_2A					RETURN "ANIM@AMB@CASINO@HANGOUT@PED_FEMALE@STAND@02A@BASE"
		CASE PA_HANGOUT_F_2B					RETURN "ANIM@AMB@CASINO@HANGOUT@PED_FEMALE@STAND@02B@BASE"
		CASE PA_HANGOUT_F_3A					RETURN "ANIM@AMB@CASINO@HANGOUT@PED_FEMALE@STAND@03A@BASE"
		CASE PA_HANGOUT_F_3B					RETURN "ANIM@AMB@CASINO@HANGOUT@PED_FEMALE@STAND@03B@BASE"
		CASE PA_HANGOUT_CONVO_F_1A				RETURN "ANIM@AMB@CASINO@HANGOUT@PED_FEMALE@STAND@01A@BASE"
		CASE PA_HANGOUT_CONVO_F_1B				RETURN "ANIM@AMB@CASINO@HANGOUT@PED_FEMALE@STAND@01B@BASE"
		CASE PA_HANGOUT_CONVO_F_2A				RETURN "ANIM@AMB@CASINO@HANGOUT@PED_FEMALE@STAND@02A@BASE"		
		CASE PA_HANGOUT_CONVO_F_2B				RETURN "ANIM@AMB@CASINO@HANGOUT@PED_FEMALE@STAND@02B@BASE"	
		CASE PA_HANGOUT_CONVO_F_3A				RETURN "ANIM@AMB@CASINO@HANGOUT@PED_FEMALE@STAND@03A@BASE"
		CASE PA_HANGOUT_CONVO_F_3B				RETURN "ANIM@AMB@CASINO@HANGOUT@PED_FEMALE@STAND@03B@BASE"
		CASE PA_HANGOUT_WITHDRINK_F_01A			RETURN "ANIM@AMB@CASINO@HANGOUT@PED_FEMALE@STAND_WITHDRINK@01A@BASE"
		CASE PA_HANGOUT_WITHDRINK_F_01B			RETURN "ANIM@AMB@CASINO@HANGOUT@PED_FEMALE@STAND_WITHDRINK@01B@BASE"
		CASE PA_HANGOUT_CONVO_WITHDRINK_F_01A	RETURN "ANIM@AMB@CASINO@HANGOUT@PED_FEMALE@STAND_WITHDRINK@01A@BASE"	
		CASE PA_HANGOUT_CONVO_WITHDRINK_F_01B	RETURN "ANIM@AMB@CASINO@HANGOUT@PED_FEMALE@STAND_WITHDRINK@01B@BASE"
	ENDSWITCH	
	
	RETURN "ANIM@AMB@CASINO@PEDS@"

ENDFUNC

FUNC BOOL IsMultiStageAnim(PED_DATA &Data)
	SWITCH INT_TO_ENUM(PENTHOUSE_ACTIVITY, Data.iActivity)
		CASE PA_AMBIENT	
		CASE PA_DANCING_WITH_BEER 
		CASE PA_SIT_DRINKING 
		CASE PA_NIGHTCLUB_DRINKING_BEER_FEMALE
		CASE PA_NIGHTCLUB_mi_loop_f02
		CASE PA_NIGHTCLUB_mi_idle_c_f02
		CASE PA_NIGHTCLUB_mi_to_li_f02
		CASE PA_NIGHTCLUB_hi_dance_prop_13_v2_male
		CASE PA_NIGHTCLUB_hi_dance_prop_09_v1_male
		CASE PA_NIGHTCLUB_hi_dance_prop_17_v1_male
		CASE PA_NIGHTCLUB_hi_dance_crowd_15_v1_male
		CASE PA_NIGHTCLUB_hi_dance_crowd_09_v1_male
		CASE PA_NIGHTCLUB_hi_dance_crowd_13_v2_male
		CASE PA_NIGHTCLUB_trans_dance_crowd_li_to_mi_11_v1_male
		CASE PA_NIGHTCLUB_mi_dance_crowd_17_v2_female
		CASE PA_NIGHTCLUB_hi_dance_crowd_17_v1_female
		CASE PA_NIGHTCLUB_ti_loop_m01								
		CASE PA_NIGHTCLUB_ti_loop_f02								
		CASE PA_NIGHTCLUB_ti_idle_d_f02								
		CASE PA_NIGHTCLUB_ti_idle_a_m03								
		CASE PA_NIGHTCLUB_ti_idle_b_f02								
		CASE PA_NIGHTCLUB_ti_idle_a_f02								
		CASE PA_NIGHTCLUB_ti_loop_f01								
		CASE PA_NIGHTCLUB_ti_idle_b_m02								
		CASE PA_NIGHTCLUB_ti_idle_b_f01								
		CASE PA_NIGHTCLUB_ti_idle_c_m03								
		CASE PA_NIGHTCLUB_ti_idle_c_f03								
		CASE PA_NIGHTCLUB_ti_loop_m02								
		CASE PA_NIGHTCLUB_ti_idle_c_m02								
		CASE PA_NIGHTCLUB_ti_idle_b_f03								
		CASE PA_NIGHTCLUB_ti_idle_c_f02								
			RETURN FALSE
	ENDSWITCH	
	RETURN TRUE
ENDFUNC

FUNC STRING GET_ANIM_CLIP_FOR_ACTIVITY(PED_DATA &Data)
	
	IF IS_PED_MODEL_FEMALE(Data)
		SWITCH INT_TO_ENUM(PENTHOUSE_ACTIVITY, Data.iActivity)
			CASE PA_AMBIENT	RETURN "amb_world_human_hang_out_street_female_hold_arm_idle_b"
			CASE PA_DANCING_WITH_BEER RETURN "amb_world_human_partying_female_partying_beer_base"
			CASE PA_SIT_DRINKING RETURN "amb_prop_human_seat_chair_drink_beer_female_idle_a"
		ENDSWITCH		
	ELSE
		SWITCH INT_TO_ENUM(PENTHOUSE_ACTIVITY, Data.iActivity)
			CASE PA_AMBIENT	RETURN "amb_world_human_hang_out_street_male_c_base"
			CASE PA_DANCING_WITH_BEER RETURN "amb_world_human_partying_male_partying_beer_base"
			CASE PA_SIT_DRINKING RETURN "amb_prop_human_seat_chair_drink_beer_male_idle_a"
		ENDSWITCH		
	ENDIF
	
	SWITCH INT_TO_ENUM(PENTHOUSE_ACTIVITY, Data.iActivity)
		CASE PA_NIGHTCLUB_DRINKING_BEER_FEMALE								RETURN  "amb_world_human_drinking_beer_female_base"
		CASE PA_NIGHTCLUB_mi_loop_f02										RETURN  "mi_loop_f02"			
		CASE PA_NIGHTCLUB_mi_idle_c_f02										RETURN  "mi_idle_c_f02"
		CASE PA_NIGHTCLUB_mi_to_li_f02										RETURN  "mi_to_li_f02"
		CASE PA_NIGHTCLUB_hi_dance_prop_13_v2_male							RETURN  "hi_dance_prop_13_v2_male^1"
		CASE PA_NIGHTCLUB_hi_dance_prop_09_v1_male							RETURN  "hi_dance_prop_09_v1_male^1"
		CASE PA_NIGHTCLUB_hi_dance_prop_17_v1_male							RETURN  "hi_dance_prop_17_v1_male^1"
		CASE PA_NIGHTCLUB_hi_dance_crowd_15_v1_male							RETURN  "hi_dance_crowd_15_v1_male^1"
		CASE PA_NIGHTCLUB_hi_dance_crowd_09_v1_male							RETURN  "hi_dance_crowd_09_v1_male^1"
		CASE PA_NIGHTCLUB_hi_dance_crowd_13_v2_male							RETURN  "hi_dance_crowd_13_v2_male^1"
		CASE PA_NIGHTCLUB_trans_dance_crowd_li_to_mi_11_v1_male				RETURN  "trans_dance_crowd_li_to_mi_11_v1_male^1"
		CASE PA_NIGHTCLUB_mi_dance_crowd_17_v2_female						RETURN  "mi_dance_crowd_17_v2_female^1"
		CASE PA_NIGHTCLUB_hi_dance_crowd_17_v1_female						RETURN  "hi_dance_crowd_17_v1_female^1"
		CASE PA_NIGHTCLUB_ti_loop_m01										RETURN "ti_loop_m01"
		CASE PA_NIGHTCLUB_ti_loop_f02										RETURN "ti_loop_f02"
		CASE PA_NIGHTCLUB_ti_idle_d_f02										RETURN "ti_idle_d_f02"
		CASE PA_NIGHTCLUB_ti_idle_a_m03										RETURN "ti_idle_a_m03"
		CASE PA_NIGHTCLUB_ti_idle_b_f02										RETURN "ti_idle_b_f02"
		CASE PA_NIGHTCLUB_ti_idle_a_f02										RETURN "ti_idle_a_f02"
		CASE PA_NIGHTCLUB_ti_loop_f01										RETURN "ti_loop_f01"
		CASE PA_NIGHTCLUB_ti_idle_b_m02										RETURN "ti_idle_b_m02"
		CASE PA_NIGHTCLUB_ti_idle_b_f01										RETURN "ti_idle_b_f01"
		CASE PA_NIGHTCLUB_ti_idle_c_m03										RETURN "ti_idle_c_m03"
		CASE PA_NIGHTCLUB_ti_idle_c_f03										RETURN "ti_idle_c_f03"
		CASE PA_NIGHTCLUB_ti_loop_m02										RETURN "ti_loop_m02"
		CASE PA_NIGHTCLUB_ti_idle_c_m02										RETURN "ti_idle_c_m02"
		CASE PA_NIGHTCLUB_ti_idle_b_f03										RETURN "ti_idle_b_f03"
		CASE PA_NIGHTCLUB_ti_idle_c_f02										RETURN "ti_idle_c_f02"
	ENDSWITCH

	RETURN "amb_world_human_hang_out_street_female_hold_arm_idle_b"
ENDFUNC

FUNC BOOL IS_ACTIVITY_SEATED(PED_DATA &Data)
	SWITCH INT_TO_ENUM(PENTHOUSE_ACTIVITY, Data.iActivity)
		CASE PA_SIT_DRINKING	RETURN TRUE
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC INT GET_LEVEL()
	INT iLevel 
	iLevel = ServerBD.iLevel
	#IF IS_DEBUG_BUILD
	iLevel = iCurrentLevel
	#ENDIF
	RETURN iLevel
ENDFUNC

FUNC INT GET_LAYOUT()
	INT iLayout
	iLayout = ServerBD.iLayout
	#IF IS_DEBUG_BUILD
	iLayout = iCurrentLayout
	#ENDIF
	RETURN iLayout
ENDFUNC

FUNC INT GET_INITIAL_LAYOUT()	
	#IF FEATURE_FIXER
		IF g_bTurnOnMissionPenthousePartyPeds
			RETURN 1
		ENDIF
	#ENDIF	
	RETURN 0
ENDFUNC

FUNC BOOL SHOULD_CREATE_PENTHOUSE_PED(PED_DATA &Data)

	#IF FEATURE_FIXER
	IF IS_VECTOR_ZERO(Data.vPosition)	// MEANING DATA WAS NOT SET FOR THAT PED
		RETURN FALSE
	ENDIF
	#ENDIF
	
	IF (Data.iLevel <= GET_LEVEL())
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC REQUEST_PENTHOUSE_PED_ASSETS(PED_DATA &Data)
	IF SHOULD_CREATE_PENTHOUSE_PED(Data)
		REQUEST_MODEL(GET_MODEL_FOR_PED(Data))
		REQUEST_ANIM_DICT(GET_ANIM_DICTIONARY_FOR_ACTIVITY(Data))
		IF IsMultiStageAnim(Data)
			REQUEST_ANIM_DICT(GET_ANIM_DICTIONARY_FOR_ACTIVITY_BASE(Data))
		ENDIF
		IF DOES_ACTIVITY_HAVE_PROP(Data)
			REQUEST_MODEL(GET_PROP_FOR_ACTIVITY(Data))
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL HAVE_PENTHOUSE_PED_ASSETS_LOADED(PED_DATA &Data)
	IF SHOULD_CREATE_PENTHOUSE_PED(Data)
		IF HAS_MODEL_LOADED(GET_MODEL_FOR_PED(Data))
		AND HAS_ANIM_DICT_LOADED(GET_ANIM_DICTIONARY_FOR_ACTIVITY(Data))
			IF IsMultiStageAnim(Data)
				IF NOT HAS_ANIM_DICT_LOADED(GET_ANIM_DICTIONARY_FOR_ACTIVITY_BASE(Data))
					RETURN FALSE
				ENDIF
			ENDIF
		
			IF DOES_ACTIVITY_HAVE_PROP(Data)
				IF HAS_MODEL_LOADED(GET_PROP_FOR_ACTIVITY(Data))
					RETURN TRUE
				ELSE
					RETURN FALSE
				ENDIF
			ELSE
				RETURN TRUE
			ENDIF
		ELSE
			RETURN FALSE
		ENDIF
	ENDIF	
	RETURN TRUE
ENDFUNC

PROC CREATE_PENTHOUSE_PED(PED_DATA &Data #IF IS_DEBUG_BUILD ,INT index #ENDIF)
	
	IF (iPedsCreatedThisFrame < MAX_NUM_PENTHOUSE_PEDS_CREATED_PER_FRAME)	
		REQUEST_PENTHOUSE_PED_ASSETS(Data)
		IF HAVE_PENTHOUSE_PED_ASSETS_LOADED(Data)
		
			Data.PedID = CREATE_PED(PEDTYPE_MISSION,GET_MODEL_FOR_PED(Data),Data.vPosition,Data.fHeading,FALSE,FALSE)
			
			IF DOES_ENTITY_EXIST(Data.PedID)
				#IF IS_DEBUG_BUILD
				PRINTLN("[CREATE_PENTHOUSE_PED] APPLYING COMPONENTS TO PED ", index)
				#ENDIF
				
				//clothes
				INT iComp
				REPEAT NUM_PED_COMPONENTS iComp
					SET_PED_COMPONENT_VARIATION(Data.PedID, INT_TO_ENUM(PED_COMPONENT, iComp), Data.ComponentConfig.ComponentData[iComp].iDrawable, Data.ComponentConfig.ComponentData[iComp].iTexture)
				ENDREPEAT
				
				//ADD PROPS (hats glasses etc)						
				REPEAT PENTHOUSE_PED_MAX_PROPS iComp
					IF Data.bAttachProp[icomp]
						SET_PED_PROP_INDEX(Data.PedID, INT_TO_ENUM(PED_PROP_POSITION,Data.iPropPosition[iComp]), 0, Data.iPropVariant[iComp])				
					ENDIF
				ENDREPEAT
				
				SET_PENTHOUSE_PED_PROPERTIES(Data)
				
				IF DOES_ACTIVITY_HAVE_PROP(Data)
					IF NOT DOES_ENTITY_EXIST(Data.ObjectID)
						Data.ObjectID = CREATE_OBJECT(GET_PROP_FOR_ACTIVITY(Data), GET_PED_BONE_COORDS(Data.PedID, GET_BONE_FOR_ACTIVITY_PROP(Data), <<0,0,0>>), FALSE, FALSE)
						ATTACH_ENTITY_TO_ENTITY(Data.ObjectID, Data.PedID, GET_PED_BONE_INDEX(Data.PedID, GET_BONE_FOR_ACTIVITY_PROP(Data)), GET_BONE_OFFSET_POSITION_TO_ATTACH_TO(Data), <<0,0,0>>, TRUE, TRUE)								
					ENDIF
				ENDIF
				
				IF IsMultiStageAnim(Data)
					SET_PED_ALTERNATE_MOVEMENT_ANIM(Data.PedID, AAT_IDLE, GET_ANIM_DICTIONARY_FOR_ACTIVITY_BASE(Data), "BASE")
					PRINTLN("CREATE_PENTHOUSE_PED - setting alternate idle")
				ENDIF
				
				iPedsCreatedThisFrame++
				PRINTLN("CREATE_PENTHOUSE_PED - created ", iPedsCreatedThisFrame, " peds this frame")
				
			ENDIF

		ENDIF
	ELSE
		PRINTLN("CREATE_PENTHOUSE_PED - already created ", MAX_NUM_PENTHOUSE_PEDS_CREATED_PER_FRAME, " peds this frame")
	ENDIF

ENDPROC

PROC REQUEST_ALL_PENTHOUSE_ASSETS()
	INT i
	REPEAT MAX_NUM_PENTHOUSE_PEDS i
		REQUEST_PENTHOUSE_PED_ASSETS(PenthousePed[i])	
	ENDREPEAT
ENDPROC

PROC GET_PED_DATA_LAYOUT_0(PED_DATA &Data, INT iID)
	SWITCH iID	
		CASE 0
			Data.vPosition = <<949.1602, 0.9533, 115.6748>>
			Data.fHeading = 33.9501
			Data.iModel = 5
			Data.ComponentConfig.ComponentData[0].iDrawable = 5
			Data.ComponentConfig.ComponentData[0].iTexture = 0
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 4
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 3
			Data.ComponentConfig.ComponentData[3].iTexture = 2
			Data.ComponentConfig.ComponentData[4].iDrawable = 2
			Data.ComponentConfig.ComponentData[4].iTexture = 2
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 5
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 2
			Data.iLevel = 3
		BREAK

		CASE 1
			Data.vPosition = <<948.5114, 2.2070, 115.2448>>
			Data.fHeading = 203.6001
			Data.iModel = 0
			Data.ComponentConfig.ComponentData[0].iDrawable = 2
			Data.ComponentConfig.ComponentData[0].iTexture = 0
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 5
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 2
			Data.ComponentConfig.ComponentData[3].iTexture = 1
			Data.ComponentConfig.ComponentData[4].iDrawable = 0
			Data.ComponentConfig.ComponentData[4].iTexture = 1
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 2
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 32
			Data.iLevel = 3
		BREAK

		CASE 2
			Data.vPosition = <<943.7761, 2.3737, 115.2597>>
			Data.fHeading = 287.0000
			Data.iModel = 0
			Data.ComponentConfig.ComponentData[0].iDrawable = 0
			Data.ComponentConfig.ComponentData[0].iTexture = 1
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 4
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 1
			Data.ComponentConfig.ComponentData[3].iTexture = 0
			Data.ComponentConfig.ComponentData[4].iDrawable = 1
			Data.ComponentConfig.ComponentData[4].iTexture = 0
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 3
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 1
			Data.iLevel = 4
		BREAK

		CASE 3
			Data.vPosition = <<944.6894, 2.5794, 115.2448>>
			Data.fHeading = 105.1996
			Data.iModel = 5
			Data.ComponentConfig.ComponentData[0].iDrawable = 1
			Data.ComponentConfig.ComponentData[0].iTexture = 0
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 2
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 1
			Data.ComponentConfig.ComponentData[3].iTexture = 0
			Data.ComponentConfig.ComponentData[4].iDrawable = 0
			Data.ComponentConfig.ComponentData[4].iTexture = 0
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 2
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 1
			Data.iLevel = 4
		BREAK

		CASE 4
			Data.vPosition = <<946.3756, 5.3382, 115.2448>>
			Data.fHeading = 133.7994
			Data.iModel = 0
			Data.ComponentConfig.ComponentData[0].iDrawable = 3
			Data.ComponentConfig.ComponentData[0].iTexture = 0
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 7
			Data.ComponentConfig.ComponentData[2].iTexture = 2
			Data.ComponentConfig.ComponentData[3].iDrawable = 4
			Data.ComponentConfig.ComponentData[3].iTexture = 2
			Data.ComponentConfig.ComponentData[4].iDrawable = 3
			Data.ComponentConfig.ComponentData[4].iTexture = 0
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 5
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 1
			Data.iLevel = 2
		BREAK

		CASE 5
			Data.vPosition = <<945.3410, 5.1376, 115.2448>>
			Data.fHeading = 260.1493
			Data.iModel = 0
			Data.ComponentConfig.ComponentData[0].iDrawable = 3
			Data.ComponentConfig.ComponentData[0].iTexture = 1
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 6
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 4
			Data.ComponentConfig.ComponentData[3].iTexture = 1
			Data.ComponentConfig.ComponentData[4].iDrawable = 3
			Data.ComponentConfig.ComponentData[4].iTexture = 2
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 5
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 1
			Data.iLevel = 2
		BREAK

		CASE 6
			Data.vPosition = <<946.0930, 4.4610, 115.2448>>
			Data.fHeading = 1.1990
			Data.iModel = 0
			Data.ComponentConfig.ComponentData[0].iDrawable = 1
			Data.ComponentConfig.ComponentData[0].iTexture = 2
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 2
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 0
			Data.ComponentConfig.ComponentData[3].iTexture = 2
			Data.ComponentConfig.ComponentData[4].iDrawable = 1
			Data.ComponentConfig.ComponentData[4].iTexture = 2
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 4
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 1
			Data.iLevel = 2
		BREAK

		CASE 7
			Data.vPosition = <<941.0575, 4.1073, 115.2448>>
			Data.fHeading = 22.1992
			Data.iModel = 0
			Data.ComponentConfig.ComponentData[0].iDrawable = 0
			Data.ComponentConfig.ComponentData[0].iTexture = 0
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 0
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 0
			Data.ComponentConfig.ComponentData[3].iTexture = 0
			Data.ComponentConfig.ComponentData[4].iDrawable = 0
			Data.ComponentConfig.ComponentData[4].iTexture = 0
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 1
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 1
			Data.iLevel = 3
		BREAK

		CASE 8
			Data.vPosition = <<940.3315, 4.5308, 115.2448>>
			Data.fHeading = 234.5990
			Data.iModel = 5
			Data.ComponentConfig.ComponentData[0].iDrawable = 3
			Data.ComponentConfig.ComponentData[0].iTexture = 0
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 2
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 1
			Data.ComponentConfig.ComponentData[3].iTexture = 0
			Data.ComponentConfig.ComponentData[4].iDrawable = 1
			Data.ComponentConfig.ComponentData[4].iTexture = 0
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 2
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 1
			Data.iLevel = 3
		BREAK

		CASE 9
			Data.vPosition = <<945.2953, 9.2744, 115.2448>>
			Data.fHeading = 130.5238
			Data.iModel = 0
			Data.ComponentConfig.ComponentData[0].iDrawable = 0
			Data.ComponentConfig.ComponentData[0].iTexture = 1
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 4
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 1
			Data.ComponentConfig.ComponentData[3].iTexture = 0
			Data.ComponentConfig.ComponentData[4].iDrawable = 1
			Data.ComponentConfig.ComponentData[4].iTexture = 0
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 3
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 1
			Data.iLevel = 1
		BREAK

		CASE 10
			Data.vPosition = <<944.7067, 8.6226, 115.2448>>
			Data.fHeading = 328.3250
			Data.iModel = 5
			Data.ComponentConfig.ComponentData[0].iDrawable = 1
			Data.ComponentConfig.ComponentData[0].iTexture = 0
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 2
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 1
			Data.ComponentConfig.ComponentData[3].iTexture = 0
			Data.ComponentConfig.ComponentData[4].iDrawable = 0
			Data.ComponentConfig.ComponentData[4].iTexture = 0
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 2
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 1
			Data.iLevel = 1
		BREAK

		CASE 11
			Data.vPosition = <<936.2474, 7.5443, 115.2448>>
			Data.fHeading = 314.1984
			Data.iModel = 0
			Data.ComponentConfig.ComponentData[0].iDrawable = 2
			Data.ComponentConfig.ComponentData[0].iTexture = 2
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 0
			Data.ComponentConfig.ComponentData[2].iTexture = 1
			Data.ComponentConfig.ComponentData[3].iDrawable = 0
			Data.ComponentConfig.ComponentData[3].iTexture = 1
			Data.ComponentConfig.ComponentData[4].iDrawable = 1
			Data.ComponentConfig.ComponentData[4].iTexture = 1
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 24
			Data.iLevel = 2
		BREAK

		CASE 12
			Data.vPosition = <<936.5831, 8.3338, 115.2448>>
			Data.fHeading = 168.2732
			Data.iModel = 5
			Data.ComponentConfig.ComponentData[0].iDrawable = 7
			Data.ComponentConfig.ComponentData[0].iTexture = 1
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 3
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 7
			Data.ComponentConfig.ComponentData[3].iTexture = 1
			Data.ComponentConfig.ComponentData[4].iDrawable = 3
			Data.ComponentConfig.ComponentData[4].iTexture = 0
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 6
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 17
			Data.iLevel = 2
		BREAK

		CASE 13
			Data.vPosition = <<940.6352, 12.9253, 115.2448>>
			Data.fHeading = 167.7982
			Data.iModel = 0
			Data.ComponentConfig.ComponentData[0].iDrawable = 3
			Data.ComponentConfig.ComponentData[0].iTexture = 0
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 6
			Data.ComponentConfig.ComponentData[2].iTexture = 2
			Data.ComponentConfig.ComponentData[3].iDrawable = 4
			Data.ComponentConfig.ComponentData[3].iTexture = 1
			Data.ComponentConfig.ComponentData[4].iDrawable = 3
			Data.ComponentConfig.ComponentData[4].iTexture = 2
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 5
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 1
			Data.iLevel = 1
		BREAK

		CASE 14
			Data.vPosition = <<940.7306, 11.9791, 115.2448>>
			Data.fHeading = 79.7981
			Data.iModel = 0
			Data.ComponentConfig.ComponentData[0].iDrawable = 4
			Data.ComponentConfig.ComponentData[0].iTexture = 2
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 0
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 5
			Data.ComponentConfig.ComponentData[3].iTexture = 0
			Data.ComponentConfig.ComponentData[4].iDrawable = 4
			Data.ComponentConfig.ComponentData[4].iTexture = 0
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 6
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 1
			Data.iLevel = 5
		BREAK

		CASE 15
			Data.vPosition = <<939.7188, 11.7404, 115.2448>>
			Data.fHeading = 336.7230
			Data.iModel = 5
			Data.ComponentConfig.ComponentData[0].iDrawable = 3
			Data.ComponentConfig.ComponentData[0].iTexture = 0
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 2
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 1
			Data.ComponentConfig.ComponentData[3].iTexture = 0
			Data.ComponentConfig.ComponentData[4].iDrawable = 1
			Data.ComponentConfig.ComponentData[4].iTexture = 0
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 2
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 1
			Data.iLevel = 1
		BREAK

		CASE 16
			Data.vPosition = <<939.5512, 12.9063, 115.2448>>
			Data.fHeading = 259.3980
			Data.iModel = 5
			Data.ComponentConfig.ComponentData[0].iDrawable = 6
			Data.ComponentConfig.ComponentData[0].iTexture = 1
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 3
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 6
			Data.ComponentConfig.ComponentData[3].iTexture = 1
			Data.ComponentConfig.ComponentData[4].iDrawable = 4
			Data.ComponentConfig.ComponentData[4].iTexture = 2
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 6
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 1
			Data.iLevel = 5
		BREAK

		CASE 17
			Data.vPosition = <<942.5848, 13.0726, 115.2448>>
			Data.fHeading = 192.9481
			Data.iModel = 0
			Data.ComponentConfig.ComponentData[0].iDrawable = 3
			Data.ComponentConfig.ComponentData[0].iTexture = 2
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 6
			Data.ComponentConfig.ComponentData[2].iTexture = 1
			Data.ComponentConfig.ComponentData[3].iDrawable = 3
			Data.ComponentConfig.ComponentData[3].iTexture = 0
			Data.ComponentConfig.ComponentData[4].iDrawable = 2
			Data.ComponentConfig.ComponentData[4].iTexture = 2
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 5
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 21
			Data.iLevel = 5
		BREAK

		CASE 18
			Data.vPosition = <<945.7389, 18.4156, 115.2448>>
			Data.fHeading = 333.7200
			Data.iModel = 0
			Data.ComponentConfig.ComponentData[0].iDrawable = 2
			Data.ComponentConfig.ComponentData[0].iTexture = 1
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 5
			Data.ComponentConfig.ComponentData[2].iTexture = 1
			Data.ComponentConfig.ComponentData[3].iDrawable = 1
			Data.ComponentConfig.ComponentData[3].iTexture = 1
			Data.ComponentConfig.ComponentData[4].iDrawable = 0
			Data.ComponentConfig.ComponentData[4].iTexture = 1
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 2
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 21
			Data.iLevel = 3
		BREAK

		CASE 19
			Data.vPosition = <<941.1565, 15.6501, 115.6673>>
			Data.fHeading = 304.1979
			Data.iModel = 5
			Data.ComponentConfig.ComponentData[0].iDrawable = 1
			Data.ComponentConfig.ComponentData[0].iTexture = 2
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 2
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 0
			Data.ComponentConfig.ComponentData[3].iTexture = 0
			Data.ComponentConfig.ComponentData[4].iDrawable = 0
			Data.ComponentConfig.ComponentData[4].iTexture = 0
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 1
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 2
			Data.iLevel = 4
		BREAK

		CASE 20
			Data.vPosition = <<940.9312, 16.9540, 115.6373>>
			Data.fHeading = 233.9977
			Data.iModel = 5
			Data.ComponentConfig.ComponentData[0].iDrawable = 6
			Data.ComponentConfig.ComponentData[0].iTexture = 2
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 3
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 6
			Data.ComponentConfig.ComponentData[3].iTexture = 0
			Data.ComponentConfig.ComponentData[4].iDrawable = 4
			Data.ComponentConfig.ComponentData[4].iTexture = 0
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 6
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 2
			Data.iLevel = 4
		BREAK

		CASE 21
			Data.vPosition = <<942.7503, 18.7311, 115.6673>>
			Data.fHeading = 221.2227
			Data.iModel = 5
			Data.ComponentConfig.ComponentData[0].iDrawable = 6
			Data.ComponentConfig.ComponentData[0].iTexture = 1
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 3
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 6
			Data.ComponentConfig.ComponentData[3].iTexture = 1
			Data.ComponentConfig.ComponentData[4].iDrawable = 3
			Data.ComponentConfig.ComponentData[4].iTexture = 1
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 6
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 2
			Data.iLevel = 1
		BREAK

		CASE 22
			Data.vPosition = <<943.7043, 18.2618, 115.2448>>
			Data.fHeading = 86.8474
			Data.iModel = 0
			Data.ComponentConfig.ComponentData[0].iDrawable = 1
			Data.ComponentConfig.ComponentData[0].iTexture = 1
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 3
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 2
			Data.ComponentConfig.ComponentData[3].iTexture = 2
			Data.ComponentConfig.ComponentData[4].iDrawable = 0
			Data.ComponentConfig.ComponentData[4].iTexture = 2
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 1
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 34
			Data.iLevel = 2
		BREAK

		CASE 23
			Data.vPosition = <<944.0376, 3.3225, 115.2523>>
			Data.fHeading = 189.1973
			Data.iModel = 5
			Data.ComponentConfig.ComponentData[0].iDrawable = 7
			Data.ComponentConfig.ComponentData[0].iTexture = 2
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 3
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 7
			Data.ComponentConfig.ComponentData[3].iTexture = 0
			Data.ComponentConfig.ComponentData[4].iDrawable = 3
			Data.ComponentConfig.ComponentData[4].iTexture = 2
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 6
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 1
			Data.iLevel = 5
		BREAK

		CASE 24
			Data.vPosition = <<946.0226, 24.4982, 115.6473>>
			Data.fHeading = 318.9970
			Data.iModel = 5
			Data.ComponentConfig.ComponentData[0].iDrawable = 7
			Data.ComponentConfig.ComponentData[0].iTexture = 1
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 3
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 6
			Data.ComponentConfig.ComponentData[3].iTexture = 0
			Data.ComponentConfig.ComponentData[4].iDrawable = 3
			Data.ComponentConfig.ComponentData[4].iTexture = 0
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 6
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 2
			Data.iLevel = 1
		BREAK

		CASE 25
			Data.vPosition = <<946.7876, 24.0731, 115.6798>>
			Data.fHeading = 326.0470
			Data.iModel = 5
			Data.ComponentConfig.ComponentData[0].iDrawable = 6
			Data.ComponentConfig.ComponentData[0].iTexture = 2
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 3
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 7
			Data.ComponentConfig.ComponentData[3].iTexture = 1
			Data.ComponentConfig.ComponentData[4].iDrawable = 4
			Data.ComponentConfig.ComponentData[4].iTexture = 2
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 6
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 2
			Data.iLevel = 1
		BREAK

		CASE 26
			Data.vPosition = <<949.3113, 25.9423, 115.3098>>
			Data.fHeading = 109.2968
			Data.iModel = 0
			Data.ComponentConfig.ComponentData[0].iDrawable = 4
			Data.ComponentConfig.ComponentData[0].iTexture = 1
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 0
			Data.ComponentConfig.ComponentData[2].iTexture = 1
			Data.ComponentConfig.ComponentData[3].iDrawable = 5
			Data.ComponentConfig.ComponentData[3].iTexture = 1
			Data.ComponentConfig.ComponentData[4].iDrawable = 4
			Data.ComponentConfig.ComponentData[4].iTexture = 1
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 6
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 32
			Data.iLevel = 3
		BREAK

		CASE 27
			Data.vPosition = <<949.0017, 26.6102, 115.6823>>
			Data.fHeading = 123.1967
			Data.iModel = 5
			Data.ComponentConfig.ComponentData[0].iDrawable = 4
			Data.ComponentConfig.ComponentData[0].iTexture = 1
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 2
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 2
			Data.ComponentConfig.ComponentData[3].iTexture = 0
			Data.ComponentConfig.ComponentData[4].iDrawable = 1
			Data.ComponentConfig.ComponentData[4].iTexture = 0
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 2
			Data.iLevel = 3
		BREAK

		CASE 28
			Data.vPosition = <<947.7147, 22.5621, 115.2398>>
			Data.fHeading = 307.4400
			Data.iModel = 0
			Data.ComponentConfig.ComponentData[0].iDrawable = 4
			Data.ComponentConfig.ComponentData[0].iTexture = 0
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 0
			Data.ComponentConfig.ComponentData[2].iTexture = 1
			Data.ComponentConfig.ComponentData[3].iDrawable = 6
			Data.ComponentConfig.ComponentData[3].iTexture = 0
			Data.ComponentConfig.ComponentData[4].iDrawable = 5
			Data.ComponentConfig.ComponentData[4].iTexture = 1
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 6
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 33
			Data.iLevel = 5
		BREAK

		CASE 29
			Data.vPosition = <<948.3247, 23.0696, 115.2448>>
			Data.fHeading = 135.9463
			Data.iModel = 0
			Data.ComponentConfig.ComponentData[0].iDrawable = 0
			Data.ComponentConfig.ComponentData[0].iTexture = 2
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 2
			Data.ComponentConfig.ComponentData[2].iTexture = 1
			Data.ComponentConfig.ComponentData[3].iDrawable = 2
			Data.ComponentConfig.ComponentData[3].iTexture = 0
			Data.ComponentConfig.ComponentData[4].iDrawable = 0
			Data.ComponentConfig.ComponentData[4].iTexture = 0
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 1
			Data.iLevel = 5
		BREAK

		CASE 30
			Data.vPosition = <<957.8384, 22.2340, 115.2223>>
			Data.fHeading = 113.7962
			Data.iModel = 5
			Data.ComponentConfig.ComponentData[0].iDrawable = 1
			Data.ComponentConfig.ComponentData[0].iTexture = 1
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 2
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 0
			Data.ComponentConfig.ComponentData[3].iTexture = 0
			Data.ComponentConfig.ComponentData[4].iDrawable = 0
			Data.ComponentConfig.ComponentData[4].iTexture = 0
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 1
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 11
			Data.iLevel = 4
		BREAK

		CASE 31
			Data.vPosition = <<957.2652, 21.5127, 115.2448>>
			Data.fHeading = 337.3962
			Data.iModel = 0
			Data.ComponentConfig.ComponentData[0].iDrawable = 1
			Data.ComponentConfig.ComponentData[0].iTexture = 0
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 1
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 1
			Data.ComponentConfig.ComponentData[3].iTexture = 2
			Data.ComponentConfig.ComponentData[4].iDrawable = 1
			Data.ComponentConfig.ComponentData[4].iTexture = 2
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 4
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 1
			Data.iLevel = 4
		BREAK

		CASE 32
			Data.vPosition = <<957.1033, 22.4842, 115.2448>>
			Data.fHeading = 223.1959
			Data.iModel = 0
			Data.ComponentConfig.ComponentData[0].iDrawable = 0
			Data.ComponentConfig.ComponentData[0].iTexture = 0
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 0
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 0
			Data.ComponentConfig.ComponentData[3].iTexture = 0
			Data.ComponentConfig.ComponentData[4].iDrawable = 1
			Data.ComponentConfig.ComponentData[4].iTexture = 0
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 3
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 1
			Data.iLevel = 5
		BREAK

		CASE 33
			Data.vPosition = <<951.9340, 22.3747, 115.2448>>
			Data.fHeading = 223.1959
			Data.iModel = 0
			Data.ComponentConfig.ComponentData[0].iDrawable = 2
			Data.ComponentConfig.ComponentData[0].iTexture = 0
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 1
			Data.ComponentConfig.ComponentData[2].iTexture = 1
			Data.ComponentConfig.ComponentData[3].iDrawable = 2
			Data.ComponentConfig.ComponentData[3].iTexture = 1
			Data.ComponentConfig.ComponentData[4].iDrawable = 1
			Data.ComponentConfig.ComponentData[4].iTexture = 1
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 1
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 1
			Data.iLevel = 3
		BREAK

		CASE 34
			Data.vPosition = <<952.5277, 21.7400, 115.2448>>
			Data.fHeading = 63.3955
			Data.iModel = 0
			Data.ComponentConfig.ComponentData[0].iDrawable = 1
			Data.ComponentConfig.ComponentData[0].iTexture = 2
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 2
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 0
			Data.ComponentConfig.ComponentData[3].iTexture = 2
			Data.ComponentConfig.ComponentData[4].iDrawable = 0
			Data.ComponentConfig.ComponentData[4].iTexture = 2
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 2
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 1
			Data.iLevel = 3
		BREAK

		CASE 35
			Data.vPosition = <<949.3122, 19.3779, 115.2448>>
			Data.fHeading = 37.3705
			Data.iModel = 0
			Data.ComponentConfig.ComponentData[0].iDrawable = 3
			Data.ComponentConfig.ComponentData[0].iTexture = 1
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 6
			Data.ComponentConfig.ComponentData[2].iTexture = 1
			Data.ComponentConfig.ComponentData[3].iDrawable = 4
			Data.ComponentConfig.ComponentData[3].iTexture = 0
			Data.ComponentConfig.ComponentData[4].iDrawable = 3
			Data.ComponentConfig.ComponentData[4].iTexture = 1
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 5
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 32
			Data.iLevel = 1
		BREAK

		CASE 36
			Data.vPosition = <<949.5209, 20.1740, 115.2448>>
			Data.fHeading = 129.0955
			Data.iModel = 5
			Data.ComponentConfig.ComponentData[0].iDrawable = 7
			Data.ComponentConfig.ComponentData[0].iTexture = 2
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 3
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 7
			Data.ComponentConfig.ComponentData[3].iTexture = 0
			Data.ComponentConfig.ComponentData[4].iDrawable = 4
			Data.ComponentConfig.ComponentData[4].iTexture = 0
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 6
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 10
			Data.iLevel = 1
		BREAK

		CASE 37
			Data.vPosition = <<948.5744, 19.8094, 115.2448>>
			Data.fHeading = 261.7953
			Data.iModel = 0
			Data.ComponentConfig.ComponentData[0].iDrawable = 1
			Data.ComponentConfig.ComponentData[0].iTexture = 2
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 2
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 0
			Data.ComponentConfig.ComponentData[3].iTexture = 2
			Data.ComponentConfig.ComponentData[4].iDrawable = 1
			Data.ComponentConfig.ComponentData[4].iTexture = 2
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 4
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 24
			Data.iLevel = 1
		BREAK

		CASE 38
			Data.vPosition = <<942.6625, 12.3174, 115.2448>>
			Data.fHeading = 0.0000
			Data.iModel = 5
			Data.ComponentConfig.ComponentData[0].iDrawable = 4
			Data.ComponentConfig.ComponentData[0].iTexture = 2
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 2
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 2
			Data.ComponentConfig.ComponentData[3].iTexture = 0
			Data.ComponentConfig.ComponentData[4].iDrawable = 0
			Data.ComponentConfig.ComponentData[4].iTexture = 0
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 15
			Data.iLevel = 5
		BREAK

		CASE 39
			Data.vPosition = <<946.2049, 19.0668, 115.2448>>
			Data.fHeading = 156.0950
			Data.iModel = 5
			Data.ComponentConfig.ComponentData[0].iDrawable = 2
			Data.ComponentConfig.ComponentData[0].iTexture = 0
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 2
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 1
			Data.ComponentConfig.ComponentData[3].iTexture = 0
			Data.ComponentConfig.ComponentData[4].iDrawable = 1
			Data.ComponentConfig.ComponentData[4].iTexture = 0
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 2
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 17
			Data.iLevel = 3
		BREAK

		CASE 40
			Data.vPosition = <<935.8634, 3.8235, 115.2448>>
			Data.fHeading = 103.7946
			Data.iModel = 0
			Data.ComponentConfig.ComponentData[0].iDrawable = 1
			Data.ComponentConfig.ComponentData[0].iTexture = 0
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 1
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 1
			Data.ComponentConfig.ComponentData[3].iTexture = 2
			Data.ComponentConfig.ComponentData[4].iDrawable = 0
			Data.ComponentConfig.ComponentData[4].iTexture = 2
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 34
			Data.iLevel = 3
		BREAK

		CASE 41
			Data.vPosition = <<935.4420, 4.6561, 115.2448>>
			Data.fHeading = 129.7940
			Data.iModel = 5
			Data.ComponentConfig.ComponentData[0].iDrawable = 0
			Data.ComponentConfig.ComponentData[0].iTexture = 2
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 2
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 0
			Data.ComponentConfig.ComponentData[3].iTexture = 0
			Data.ComponentConfig.ComponentData[4].iDrawable = 0
			Data.ComponentConfig.ComponentData[4].iTexture = 0
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 1
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 17
			Data.iLevel = 4
		BREAK

		CASE 42
			Data.vPosition = <<954.5865, 25.1964, 115.2448>>
			Data.fHeading = 131.7939
			Data.iModel = 5
			Data.ComponentConfig.ComponentData[0].iDrawable = 3
			Data.ComponentConfig.ComponentData[0].iTexture = 2
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 2
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 1
			Data.ComponentConfig.ComponentData[3].iTexture = 0
			Data.ComponentConfig.ComponentData[4].iDrawable = 1
			Data.ComponentConfig.ComponentData[4].iTexture = 0
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 2
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 0
			Data.iLevel = 2
		BREAK

		CASE 43
			Data.vPosition = <<954.5208, 24.2893, 115.2448>>
			Data.fHeading = 16.7939
			Data.iModel = 0
			Data.ComponentConfig.ComponentData[0].iDrawable = 2
			Data.ComponentConfig.ComponentData[0].iTexture = 2
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 0
			Data.ComponentConfig.ComponentData[2].iTexture = 1
			Data.ComponentConfig.ComponentData[3].iDrawable = 0
			Data.ComponentConfig.ComponentData[3].iTexture = 1
			Data.ComponentConfig.ComponentData[4].iDrawable = 0
			Data.ComponentConfig.ComponentData[4].iTexture = 1
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 1
			Data.iLevel = 2
		BREAK

		CASE 44
			Data.vPosition = <<953.5839, 24.6154, 115.2448>>
			Data.fHeading = 286.5938
			Data.iModel = 5
			Data.ComponentConfig.ComponentData[0].iDrawable = 6
			Data.ComponentConfig.ComponentData[0].iTexture = 0
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 3
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 7
			Data.ComponentConfig.ComponentData[3].iTexture = 2
			Data.ComponentConfig.ComponentData[4].iDrawable = 4
			Data.ComponentConfig.ComponentData[4].iTexture = 1
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 6
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 1
			Data.iLevel = 2
		BREAK

		CASE 45
			Data.vPosition = <<961.0172, 28.1687, 115.2448>>
			Data.fHeading = 121.2183
			Data.iModel = 0
			Data.ComponentConfig.ComponentData[0].iDrawable = 4
			Data.ComponentConfig.ComponentData[0].iTexture = 2
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 0
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 5
			Data.ComponentConfig.ComponentData[3].iTexture = 2
			Data.ComponentConfig.ComponentData[4].iDrawable = 4
			Data.ComponentConfig.ComponentData[4].iTexture = 2
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 6
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 26
			Data.iLevel = 5
		BREAK

		CASE 46
			Data.vPosition = <<960.3821, 27.4420, 115.2448>>
			Data.fHeading = 328.0932
			Data.iModel = 0
			Data.ComponentConfig.ComponentData[0].iDrawable = 3
			Data.ComponentConfig.ComponentData[0].iTexture = 0
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 7
			Data.ComponentConfig.ComponentData[2].iTexture = 1
			Data.ComponentConfig.ComponentData[3].iDrawable = 3
			Data.ComponentConfig.ComponentData[3].iTexture = 2
			Data.ComponentConfig.ComponentData[4].iDrawable = 2
			Data.ComponentConfig.ComponentData[4].iTexture = 0
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 5
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 21
			Data.iLevel = 5
		BREAK

		CASE 47
			Data.vPosition = <<956.9514, 27.2328, 115.2448>>
			Data.fHeading = 301.0679
			Data.iModel = 0
			Data.ComponentConfig.ComponentData[0].iDrawable = 0
			Data.ComponentConfig.ComponentData[0].iTexture = 1
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 4
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 1
			Data.ComponentConfig.ComponentData[3].iTexture = 0
			Data.ComponentConfig.ComponentData[4].iDrawable = 0
			Data.ComponentConfig.ComponentData[4].iTexture = 0
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 2
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 19
			Data.iLevel = 4
		BREAK

		CASE 48
			Data.vPosition = <<957.5153, 28.0321, 115.2448>>
			Data.fHeading = 167.5676
			Data.iModel = 5
			Data.ComponentConfig.ComponentData[0].iDrawable = 0
			Data.ComponentConfig.ComponentData[0].iTexture = 0
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 2
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 2
			Data.ComponentConfig.ComponentData[3].iTexture = 0
			Data.ComponentConfig.ComponentData[4].iDrawable = 0
			Data.ComponentConfig.ComponentData[4].iTexture = 0
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 15
			Data.iLevel = 4
		BREAK

		CASE 49
			Data.vPosition = <<960.1348, 28.1884, 115.2448>>
			Data.fHeading = 242.4169
			Data.iModel = 5
			Data.ComponentConfig.ComponentData[0].iDrawable = 0
			Data.ComponentConfig.ComponentData[0].iTexture = 1
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 4
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 3
			Data.ComponentConfig.ComponentData[3].iTexture = 1
			Data.ComponentConfig.ComponentData[4].iDrawable = 6
			Data.ComponentConfig.ComponentData[4].iTexture = 2
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 3
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 17
			Data.iLevel = 5
		BREAK	

	ENDSWITCH
ENDPROC

PROC GET_PED_DATA_LAYOUT_1(PED_DATA &Data, INT iID)
	SWITCH iID		
		CASE 0//edited
			Data.vPosition = <<943.8861, 2.1137, 115.2597>>
			Data.fHeading = 315.1000
			Data.iModel = 22
			Data.ComponentConfig.ComponentData[0].iDrawable = 0
			Data.ComponentConfig.ComponentData[0].iTexture = 1
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 2
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 1
			Data.ComponentConfig.ComponentData[3].iTexture = 0
			Data.ComponentConfig.ComponentData[4].iDrawable = 0
			Data.ComponentConfig.ComponentData[4].iTexture = 2
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 39
			Data.iLevel = 4
		BREAK

		CASE 1//edited
			Data.vPosition = <<944.6894, 2.5794, 115.2448>>
			Data.fHeading = 105.1996
			Data.iModel = 26
			Data.ComponentConfig.ComponentData[0].iDrawable = 1
			Data.ComponentConfig.ComponentData[0].iTexture = 0
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 0
			Data.ComponentConfig.ComponentData[2].iTexture = 2
			Data.ComponentConfig.ComponentData[3].iDrawable = 0
			Data.ComponentConfig.ComponentData[3].iTexture = 2
			Data.ComponentConfig.ComponentData[4].iDrawable = 0
			Data.ComponentConfig.ComponentData[4].iTexture = 0
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 1
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.bAttachProp[0] = TRUE
			Data.iPropPosition[0] = 1
			Data.iPropVariant[0] = 0
			Data.iActivity = 1
			Data.iLevel = 4
		BREAK

		CASE 2//edited
			Data.vPosition = <<940.9675, 3.5673, 115.2448>>
			Data.fHeading = 22.1992
			Data.iModel = 6
			Data.ComponentConfig.ComponentData[0].iDrawable = 5
			Data.ComponentConfig.ComponentData[0].iTexture = 0
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 3
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 4
			Data.ComponentConfig.ComponentData[3].iTexture = 2
			Data.ComponentConfig.ComponentData[4].iDrawable = 1
			Data.ComponentConfig.ComponentData[4].iTexture = 2
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 3
			Data.ComponentConfig.ComponentData[6].iDrawable = 1
			Data.ComponentConfig.ComponentData[6].iTexture = 1
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 1
			Data.iLevel = 3
		BREAK

		CASE 3//edited
			Data.vPosition = <<940.5815, 4.3308, 115.1948>>
			Data.fHeading = 215.5990
			Data.iModel = 26
			Data.ComponentConfig.ComponentData[0].iDrawable = 1
			Data.ComponentConfig.ComponentData[0].iTexture = 1
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 1
			Data.ComponentConfig.ComponentData[2].iTexture = 2
			Data.ComponentConfig.ComponentData[3].iDrawable = 1
			Data.ComponentConfig.ComponentData[3].iTexture = 2
			Data.ComponentConfig.ComponentData[4].iDrawable = 1
			Data.ComponentConfig.ComponentData[4].iTexture = 2
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 1
			Data.ComponentConfig.ComponentData[11].iDrawable = 1
			Data.ComponentConfig.ComponentData[11].iTexture = 2
			Data.bAttachProp[0] = TRUE
			Data.iPropPosition[0] = 1
			Data.iPropVariant[0] = 1
			Data.iActivity = 36
			Data.iLevel = 3
		BREAK

		CASE 4//edited
			Data.vPosition = <<940.3752, 13.0853, 115.2448>>
			Data.fHeading = 167.7982
			Data.iModel = 4
			Data.ComponentConfig.ComponentData[0].iDrawable = 0
			Data.ComponentConfig.ComponentData[0].iTexture = 0
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 4
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 1
			Data.ComponentConfig.ComponentData[3].iTexture = 1
			Data.ComponentConfig.ComponentData[4].iDrawable = 1
			Data.ComponentConfig.ComponentData[4].iTexture = 0
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 1
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 1
			Data.iLevel = 1
		BREAK

		CASE 5//edited
			Data.vPosition = <<940.6506, 12.1791, 115.2448>>
			Data.fHeading = 76.1981
			Data.iModel = 3
			Data.ComponentConfig.ComponentData[0].iDrawable = 1
			Data.ComponentConfig.ComponentData[0].iTexture = 0
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 2
			Data.ComponentConfig.ComponentData[2].iTexture = 2
			Data.ComponentConfig.ComponentData[3].iDrawable = 1
			Data.ComponentConfig.ComponentData[3].iTexture = 0
			Data.ComponentConfig.ComponentData[4].iDrawable = 1
			Data.ComponentConfig.ComponentData[4].iTexture = 0
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 1
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 61
			Data.iLevel = 5
		BREAK

		CASE 6//edited
			Data.vPosition = <<939.6988, 11.7404, 115.2448>>
			Data.fHeading = 336.7230
			Data.iModel = 6
			Data.ComponentConfig.ComponentData[0].iDrawable = 3
			Data.ComponentConfig.ComponentData[0].iTexture = 0
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 1
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 4
			Data.ComponentConfig.ComponentData[3].iTexture = 1
			Data.ComponentConfig.ComponentData[4].iDrawable = 3
			Data.ComponentConfig.ComponentData[4].iTexture = 6
			Data.ComponentConfig.ComponentData[5].iDrawable = 3
			Data.ComponentConfig.ComponentData[5].iTexture = 2
			Data.ComponentConfig.ComponentData[6].iDrawable = 2
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.bAttachProp[0] = TRUE
			Data.iPropPosition[0] = 1
			Data.iPropVariant[0] = 1
			Data.bAttachProp[1] = TRUE
			Data.iPropPosition[1] = 0
			Data.iPropVariant[1] = 2
			Data.iActivity = 1
			Data.iLevel = 1
		BREAK

		CASE 7//edited
			Data.vPosition = <<939.3912, 12.8663, 115.2448>>
			Data.fHeading = 237.6980
			Data.iModel = 6
			Data.ComponentConfig.ComponentData[0].iDrawable = 4
			Data.ComponentConfig.ComponentData[0].iTexture = 0
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 4
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 1
			Data.ComponentConfig.ComponentData[3].iTexture = 1
			Data.ComponentConfig.ComponentData[4].iDrawable = 3
			Data.ComponentConfig.ComponentData[4].iTexture = 8
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 2
			Data.ComponentConfig.ComponentData[6].iDrawable = 1
			Data.ComponentConfig.ComponentData[6].iTexture = 2
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 42
			Data.iLevel = 5
		BREAK

		CASE 8//edited
			Data.vPosition = <<942.5348, 12.4426, 115.2448>>
			Data.fHeading = 221.9481
			Data.iModel = 26
			Data.ComponentConfig.ComponentData[0].iDrawable = 0
			Data.ComponentConfig.ComponentData[0].iTexture = 1
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 0
			Data.ComponentConfig.ComponentData[2].iTexture = 1
			Data.ComponentConfig.ComponentData[3].iDrawable = 0
			Data.ComponentConfig.ComponentData[3].iTexture = 1
			Data.ComponentConfig.ComponentData[4].iDrawable = 0
			Data.ComponentConfig.ComponentData[4].iTexture = 2
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 1
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 2
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 1
			Data.iLevel = 5
		BREAK

		CASE 9//edited
			Data.vPosition = <<945.7389, 18.4156, 115.2448>>
			Data.fHeading = 333.7200
			Data.iModel = 23
			Data.ComponentConfig.ComponentData[0].iDrawable = 2
			Data.ComponentConfig.ComponentData[0].iTexture = 1
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 4
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 5
			Data.ComponentConfig.ComponentData[3].iTexture = 2
			Data.ComponentConfig.ComponentData[4].iDrawable = 1
			Data.ComponentConfig.ComponentData[4].iTexture = 0
			Data.ComponentConfig.ComponentData[5].iDrawable = 5
			Data.ComponentConfig.ComponentData[5].iTexture = 3
			Data.ComponentConfig.ComponentData[6].iDrawable = 1
			Data.ComponentConfig.ComponentData[6].iTexture = 1
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.bAttachProp[1] = TRUE
			Data.iPropPosition[1] = 1
			Data.iPropVariant[1] = 0
			Data.iActivity = 61
			Data.iLevel = 3
		BREAK

		CASE 10//edited
			Data.vPosition = <<941.0665, 15.6101, 115.6673>>
			Data.fHeading = 304.1979
			Data.iModel = 3
			Data.ComponentConfig.ComponentData[0].iDrawable = 1
			Data.ComponentConfig.ComponentData[0].iTexture = 0
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 1
			Data.ComponentConfig.ComponentData[2].iTexture = 1
			Data.ComponentConfig.ComponentData[3].iDrawable = 0
			Data.ComponentConfig.ComponentData[3].iTexture = 1
			Data.ComponentConfig.ComponentData[4].iDrawable = 0
			Data.ComponentConfig.ComponentData[4].iTexture = 2
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 1
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 2
			Data.iLevel = 4
		BREAK

		CASE 11//edited
			Data.vPosition = <<940.8412, 16.9940, 115.6373>>
			Data.fHeading = 233.9977
			Data.iModel = 26
			Data.ComponentConfig.ComponentData[0].iDrawable = 0
			Data.ComponentConfig.ComponentData[0].iTexture = 0
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 0
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 0
			Data.ComponentConfig.ComponentData[3].iTexture = 0
			Data.ComponentConfig.ComponentData[4].iDrawable = 1
			Data.ComponentConfig.ComponentData[4].iTexture = 0
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 1
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 2
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 2
			Data.iLevel = 4
		BREAK

		CASE 12//edited
			Data.vPosition = <<943.2703, 19.0311, 115.2000>>
			Data.fHeading = 221.2227
			Data.iModel = 9
			Data.ComponentConfig.ComponentData[0].iDrawable = 0
			Data.ComponentConfig.ComponentData[0].iTexture = 1
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 0
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 0
			Data.ComponentConfig.ComponentData[3].iTexture = 2
			Data.ComponentConfig.ComponentData[4].iDrawable = 0
			Data.ComponentConfig.ComponentData[4].iTexture = 0
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.bAttachProp[0] = TRUE
			Data.iPropPosition[0] = 1
			Data.iPropVariant[0] = 0
			Data.iActivity = 55
			Data.iLevel = 1
		BREAK

		CASE 13//edited
			Data.vPosition = <<943.7043, 18.2618, 115.2448>>
			Data.fHeading = 7.9200
			Data.iModel = 23
			Data.ComponentConfig.ComponentData[0].iDrawable = 5
			Data.ComponentConfig.ComponentData[0].iTexture = 1
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 1
			Data.ComponentConfig.ComponentData[2].iTexture = 1
			Data.ComponentConfig.ComponentData[3].iDrawable = 1
			Data.ComponentConfig.ComponentData[3].iTexture = 0
			Data.ComponentConfig.ComponentData[4].iDrawable = 1
			Data.ComponentConfig.ComponentData[4].iTexture = 6
			Data.ComponentConfig.ComponentData[5].iDrawable = 1
			Data.ComponentConfig.ComponentData[5].iTexture = 2
			Data.ComponentConfig.ComponentData[6].iDrawable = 2
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 1
			Data.iLevel = 2
		BREAK

		CASE 14//edited
			Data.vPosition = <<946.0826, 24.4982, 115.6673>>
			Data.fHeading = 304.8970
			Data.iModel = 25
			Data.ComponentConfig.ComponentData[0].iDrawable = 0
			Data.ComponentConfig.ComponentData[0].iTexture = 1
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 1
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 1
			Data.ComponentConfig.ComponentData[3].iTexture = 2
			Data.ComponentConfig.ComponentData[4].iDrawable = 1
			Data.ComponentConfig.ComponentData[4].iTexture = 0
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 1
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 2
			Data.iLevel = 1
		BREAK

		CASE 15//edited
			Data.vPosition = <<939.1000, 3.1730, 115.2000>>
			Data.fHeading = 355.0470
			Data.iModel = 3
			Data.ComponentConfig.ComponentData[0].iDrawable = 0
			Data.ComponentConfig.ComponentData[0].iTexture = 0
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 2
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 1
			Data.ComponentConfig.ComponentData[3].iTexture = 3
			Data.ComponentConfig.ComponentData[4].iDrawable = 1
			Data.ComponentConfig.ComponentData[4].iTexture = 3
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 52
			Data.iLevel = 1
		BREAK

		CASE 16//edited
			Data.vPosition = <<949.3113, 25.9423, 115.3098>>
			Data.fHeading = 134.1968
			Data.iModel = 23
			Data.ComponentConfig.ComponentData[0].iDrawable = 1
			Data.ComponentConfig.ComponentData[0].iTexture = 0
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 3
			Data.ComponentConfig.ComponentData[2].iTexture = 2
			Data.ComponentConfig.ComponentData[3].iDrawable = 2
			Data.ComponentConfig.ComponentData[3].iTexture = 2
			Data.ComponentConfig.ComponentData[4].iDrawable = 1
			Data.ComponentConfig.ComponentData[4].iTexture = 9
			Data.ComponentConfig.ComponentData[5].iDrawable = 2
			Data.ComponentConfig.ComponentData[5].iTexture = 1
			Data.ComponentConfig.ComponentData[6].iDrawable = 1
			Data.ComponentConfig.ComponentData[6].iTexture = 2
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 57
			Data.iLevel = 3
		BREAK

		CASE 17//edited
			Data.vPosition = <<949.0017, 26.6102, 115.6823>>
			Data.fHeading = 123.1967
			Data.iModel = 19
			Data.ComponentConfig.ComponentData[0].iDrawable = 0
			Data.ComponentConfig.ComponentData[0].iTexture = 0
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 0
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 0
			Data.ComponentConfig.ComponentData[3].iTexture = 1
			Data.ComponentConfig.ComponentData[4].iDrawable = 0
			Data.ComponentConfig.ComponentData[4].iTexture = 0
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 1
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 2
			Data.iLevel = 3
		BREAK

		CASE 18//edited
			Data.vPosition = <<946.5150, 21.9020, 115.2398>>
			Data.fHeading = 307.4400
			Data.iModel = 26
			Data.ComponentConfig.ComponentData[0].iDrawable = 1
			Data.ComponentConfig.ComponentData[0].iTexture = 1
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 1
			Data.ComponentConfig.ComponentData[2].iTexture = 1
			Data.ComponentConfig.ComponentData[3].iDrawable = 1
			Data.ComponentConfig.ComponentData[3].iTexture = 1
			Data.ComponentConfig.ComponentData[4].iDrawable = 1
			Data.ComponentConfig.ComponentData[4].iTexture = 0
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 1
			Data.ComponentConfig.ComponentData[11].iDrawable = 1
			Data.ComponentConfig.ComponentData[11].iTexture = 2
			Data.iActivity = 61
			Data.iLevel = 5
		BREAK

		CASE 19//edited
			Data.vPosition = <<947.3250, 22.5700, 115.2448>>
			Data.fHeading = 135.9463
			Data.iModel = 23
			Data.ComponentConfig.ComponentData[0].iDrawable = 5
			Data.ComponentConfig.ComponentData[0].iTexture = 2
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 0
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 0
			Data.ComponentConfig.ComponentData[3].iTexture = 1
			Data.ComponentConfig.ComponentData[4].iDrawable = 3
			Data.ComponentConfig.ComponentData[4].iTexture = 6
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 2
			Data.ComponentConfig.ComponentData[6].iDrawable = 1
			Data.ComponentConfig.ComponentData[6].iTexture = 2
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 1
			Data.iLevel = 5
		BREAK

		CASE 20//edited
			Data.vPosition = <<938.4180, 3.5700, 115.2223>>
			Data.fHeading = 291.7000
			Data.iModel = 6
			Data.ComponentConfig.ComponentData[0].iDrawable = 2
			Data.ComponentConfig.ComponentData[0].iTexture = 1
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 0
			Data.ComponentConfig.ComponentData[2].iTexture = 1
			Data.ComponentConfig.ComponentData[3].iDrawable = 5
			Data.ComponentConfig.ComponentData[3].iTexture = 0
			Data.ComponentConfig.ComponentData[4].iDrawable = 3
			Data.ComponentConfig.ComponentData[4].iTexture = 0
			Data.ComponentConfig.ComponentData[5].iDrawable = 3
			Data.ComponentConfig.ComponentData[5].iTexture = 1
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 2
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 1
			Data.ComponentConfig.ComponentData[8].iTexture = 1
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 1
			Data.iLevel = 4
		BREAK

		CASE 21//edited
			Data.vPosition = <<956.7952, 21.6827, 115.2448>>
			Data.fHeading = 4.7000
			Data.iModel = 10
			Data.ComponentConfig.ComponentData[0].iDrawable = 2
			Data.ComponentConfig.ComponentData[0].iTexture = 2
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 5
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 4
			Data.ComponentConfig.ComponentData[3].iTexture = 1
			Data.ComponentConfig.ComponentData[4].iDrawable = 2
			Data.ComponentConfig.ComponentData[4].iTexture = 1
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 1
			Data.iLevel = 4
		BREAK

		CASE 22//edited
			Data.vPosition = <<957.3933, 22.5142, 115.2448>>
			Data.fHeading = 143.7959
			Data.iModel = 6
			Data.ComponentConfig.ComponentData[0].iDrawable = 6
			Data.ComponentConfig.ComponentData[0].iTexture = 0
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 3
			Data.ComponentConfig.ComponentData[2].iTexture = 1
			Data.ComponentConfig.ComponentData[3].iDrawable = 3
			Data.ComponentConfig.ComponentData[3].iTexture = 2
			Data.ComponentConfig.ComponentData[4].iDrawable = 1
			Data.ComponentConfig.ComponentData[4].iTexture = 1
			Data.ComponentConfig.ComponentData[5].iDrawable = 1
			Data.ComponentConfig.ComponentData[5].iTexture = 3
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 2
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.bAttachProp[0] = TRUE
			Data.iPropPosition[0] = 0
			Data.iPropVariant[0] = 0
			Data.iActivity = 42
			Data.iLevel = 5
		BREAK

		CASE 23//edited
			Data.vPosition = <<951.3540, 21.7047, 115.2448>>
			Data.fHeading = 223.1959
			Data.iModel = 6
			Data.ComponentConfig.ComponentData[0].iDrawable = 3
			Data.ComponentConfig.ComponentData[0].iTexture = 1
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 0
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 0
			Data.ComponentConfig.ComponentData[3].iTexture = 0
			Data.ComponentConfig.ComponentData[4].iDrawable = 2
			Data.ComponentConfig.ComponentData[4].iTexture = 0
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 1
			Data.ComponentConfig.ComponentData[6].iDrawable = 1
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.bAttachProp[0] = TRUE
			Data.iPropPosition[0] = 1
			Data.iPropVariant[0] = 0
			Data.iActivity = 1
			Data.iLevel = 3
		BREAK

		CASE 24//edited
			Data.vPosition = <<951.9577, 21.0700, 115.2248>>
			Data.fHeading = 38.7955
			Data.iModel = 11
			Data.ComponentConfig.ComponentData[0].iDrawable = 4
			Data.ComponentConfig.ComponentData[0].iTexture = 0
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 3
			Data.ComponentConfig.ComponentData[2].iTexture = 2
			Data.ComponentConfig.ComponentData[3].iDrawable = 6
			Data.ComponentConfig.ComponentData[3].iTexture = 0
			Data.ComponentConfig.ComponentData[4].iDrawable = 3
			Data.ComponentConfig.ComponentData[4].iTexture = 2
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 45
			Data.iLevel = 3
		BREAK

		CASE 25//edited
			Data.vPosition = <<949.3122, 19.3779, 115.2448>>
			Data.fHeading = 37.3705
			Data.iModel = 23
			Data.ComponentConfig.ComponentData[0].iDrawable = 1
			Data.ComponentConfig.ComponentData[0].iTexture = 0
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 3
			Data.ComponentConfig.ComponentData[2].iTexture = 2
			Data.ComponentConfig.ComponentData[3].iDrawable = 2
			Data.ComponentConfig.ComponentData[3].iTexture = 1
			Data.ComponentConfig.ComponentData[4].iDrawable = 2
			Data.ComponentConfig.ComponentData[4].iTexture = 5
			Data.ComponentConfig.ComponentData[5].iDrawable = 3
			Data.ComponentConfig.ComponentData[5].iTexture = 1
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 1
			Data.iLevel = 1
		BREAK

		CASE 26//edited
			Data.vPosition = <<949.5209, 20.1740, 115.2448>>
			Data.fHeading = 129.0955
			Data.iModel = 22
			Data.ComponentConfig.ComponentData[0].iDrawable = 0
			Data.ComponentConfig.ComponentData[0].iTexture = 0
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 0
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 0
			Data.ComponentConfig.ComponentData[3].iTexture = 2
			Data.ComponentConfig.ComponentData[4].iDrawable = 0
			Data.ComponentConfig.ComponentData[4].iTexture = 0
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 59
			Data.iLevel = 1
		BREAK

		CASE 27//edited
			Data.vPosition = <<942.5000, 0.0000, 115.2448>>
			Data.fHeading = 338.8953
			Data.iModel = 17
			Data.ComponentConfig.ComponentData[0].iDrawable = 1
			Data.ComponentConfig.ComponentData[0].iTexture = 2
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 0
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 1
			Data.ComponentConfig.ComponentData[3].iTexture = 1
			Data.ComponentConfig.ComponentData[4].iDrawable = 1
			Data.ComponentConfig.ComponentData[4].iTexture = 2
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 1
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 45
			Data.iLevel = 1
		BREAK

		CASE 28//edited
			Data.vPosition = <<943.1325, 13.1574, 115.9348>>
			Data.fHeading = 190.0000
			Data.iModel = 12
			Data.ComponentConfig.ComponentData[0].iDrawable = 1
			Data.ComponentConfig.ComponentData[0].iTexture = 2
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 0
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 0
			Data.ComponentConfig.ComponentData[3].iTexture = 3
			Data.ComponentConfig.ComponentData[4].iDrawable = 0
			Data.ComponentConfig.ComponentData[4].iTexture = 0
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 1
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.bAttachProp[0] = TRUE
			Data.iPropPosition[0] = 1
			Data.iPropVariant[0] = 0
			Data.bAttachProp[1] = TRUE
			Data.iPropPosition[1] = 0
			Data.iPropVariant[1] = 1
			Data.iActivity = 2
			Data.iLevel = 5
		BREAK

		CASE 29//edited
			Data.vPosition = <<946.2049, 19.0668, 115.2448>>
			Data.fHeading = 156.0950
			Data.iModel = 9
			Data.ComponentConfig.ComponentData[0].iDrawable = 1
			Data.ComponentConfig.ComponentData[0].iTexture = 2
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 0
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 1
			Data.ComponentConfig.ComponentData[3].iTexture = 1
			Data.ComponentConfig.ComponentData[4].iDrawable = 0
			Data.ComponentConfig.ComponentData[4].iTexture = 2
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 1
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 1
			Data.iLevel = 3
		BREAK

		CASE 30//edited
			Data.vPosition = <<935.6234, 4.0235, 115.2648>>
			Data.fHeading = 300.0000
			Data.iModel = 12
			Data.ComponentConfig.ComponentData[0].iDrawable = 0
			Data.ComponentConfig.ComponentData[0].iTexture = 1
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 0
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 1
			Data.ComponentConfig.ComponentData[3].iTexture = 3
			Data.ComponentConfig.ComponentData[4].iDrawable = 0
			Data.ComponentConfig.ComponentData[4].iTexture = 2
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 1
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 1
			Data.iLevel = 3
		BREAK

		CASE 31//edited
			Data.vPosition = <<935.2920, 4.9361, 115.4248>>
			Data.fHeading = 275.6000
			Data.iModel = 23
			Data.ComponentConfig.ComponentData[0].iDrawable = 0
			Data.ComponentConfig.ComponentData[0].iTexture = 2
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 2
			Data.ComponentConfig.ComponentData[2].iTexture = 2
			Data.ComponentConfig.ComponentData[3].iDrawable = 1
			Data.ComponentConfig.ComponentData[3].iTexture = 1
			Data.ComponentConfig.ComponentData[4].iDrawable = 0
			Data.ComponentConfig.ComponentData[4].iTexture = 8
			Data.ComponentConfig.ComponentData[5].iDrawable = 1
			Data.ComponentConfig.ComponentData[5].iTexture = 3
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 2
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 3
			Data.iLevel = 4
		BREAK

		CASE 32//edited
			Data.vPosition = <<937.6000, 9.5000, 115.2448>>
			Data.fHeading = 270.7200
			Data.iModel = 17
			Data.ComponentConfig.ComponentData[0].iDrawable = 0
			Data.ComponentConfig.ComponentData[0].iTexture = 1
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 0
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 0
			Data.ComponentConfig.ComponentData[3].iTexture = 2
			Data.ComponentConfig.ComponentData[4].iDrawable = 0
			Data.ComponentConfig.ComponentData[4].iTexture = 0
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 1
			Data.ComponentConfig.ComponentData[8].iTexture = 1
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.bAttachProp[1] = TRUE
			Data.iPropPosition[1] = 1
			Data.iPropVariant[1] = 1
			Data.iActivity = 30
			Data.iLevel = 2
		BREAK

		CASE 33//edited
			Data.vPosition = <<954.2008, 24.0593, 115.2448>>
			Data.fHeading = 29.9939
			Data.iModel = 15
			Data.ComponentConfig.ComponentData[0].iDrawable = 1
			Data.ComponentConfig.ComponentData[0].iTexture = 0
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 0
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 1
			Data.ComponentConfig.ComponentData[3].iTexture = 0
			Data.ComponentConfig.ComponentData[4].iDrawable = 1
			Data.ComponentConfig.ComponentData[4].iTexture = 1
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 1
			Data.iLevel = 1
		BREAK

		CASE 34//edited
			Data.vPosition = <<953.2839, 24.6954, 115.2448>>
			Data.fHeading = 263.3600
			Data.iModel = 10
			Data.ComponentConfig.ComponentData[0].iDrawable = 0
			Data.ComponentConfig.ComponentData[0].iTexture = 0
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 1
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 0
			Data.ComponentConfig.ComponentData[3].iTexture = 1
			Data.ComponentConfig.ComponentData[4].iDrawable = 1
			Data.ComponentConfig.ComponentData[4].iTexture = 0
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 3
			Data.iLevel = 2
		BREAK

		CASE 35//edited
			Data.vPosition = <<961.0172, 28.1687, 115.2448>>
			Data.fHeading = 121.2183
			Data.iModel = 3
			Data.ComponentConfig.ComponentData[0].iDrawable = 0
			Data.ComponentConfig.ComponentData[0].iTexture = 0
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 0
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 1
			Data.ComponentConfig.ComponentData[3].iTexture = 5
			Data.ComponentConfig.ComponentData[4].iDrawable = 0
			Data.ComponentConfig.ComponentData[4].iTexture = 3
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 0
			Data.iLevel = 5
		BREAK

		CASE 36//edited
			Data.vPosition = <<960.3821, 27.4420, 115.2448>>
			Data.fHeading = 328.0932
			Data.iModel = 4
			Data.ComponentConfig.ComponentData[0].iDrawable = 0
			Data.ComponentConfig.ComponentData[0].iTexture = 1
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 4
			Data.ComponentConfig.ComponentData[2].iTexture = 1
			Data.ComponentConfig.ComponentData[3].iDrawable = 0
			Data.ComponentConfig.ComponentData[3].iTexture = 1
			Data.ComponentConfig.ComponentData[4].iDrawable = 1
			Data.ComponentConfig.ComponentData[4].iTexture = 2
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 21
			Data.iLevel = 5
		BREAK

		CASE 37//edited
			Data.vPosition = <<946.9900, 23.9700, 115.6500>>
			Data.fHeading = 332.0000
			Data.iModel = 18
			Data.ComponentConfig.ComponentData[0].iDrawable = 0
			Data.ComponentConfig.ComponentData[0].iTexture = 0
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 0
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 1
			Data.ComponentConfig.ComponentData[3].iTexture = 0
			Data.ComponentConfig.ComponentData[4].iDrawable = 0
			Data.ComponentConfig.ComponentData[4].iTexture = 1
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 2
			Data.iLevel = 4
		BREAK

		CASE 38//edited
			Data.vPosition = <<937.6000, 10.8000, 115.2448>>
			Data.fHeading = 236.5000
			Data.iModel = 19
			Data.ComponentConfig.ComponentData[0].iDrawable = 0
			Data.ComponentConfig.ComponentData[0].iTexture = 0
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 0
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 1
			Data.ComponentConfig.ComponentData[3].iTexture = 0
			Data.ComponentConfig.ComponentData[4].iDrawable = 0
			Data.ComponentConfig.ComponentData[4].iTexture = 0
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 1
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 52
			Data.iLevel = 4
		BREAK

		CASE 39//edited
			Data.vPosition = <<943.3300, -0.1500, 115.2448>>
			Data.fHeading = 10.0000
			Data.iModel = 9
			Data.ComponentConfig.ComponentData[0].iDrawable = 1
			Data.ComponentConfig.ComponentData[0].iTexture = 1
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 0
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 0
			Data.ComponentConfig.ComponentData[3].iTexture = 0
			Data.ComponentConfig.ComponentData[4].iDrawable = 0
			Data.ComponentConfig.ComponentData[4].iTexture = 2
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 1
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 17
			Data.iLevel = 5
		BREAK

		CASE 40//edited
			Data.vPosition = <<958.5800, 45.0400, 115.6700>>
			Data.fHeading = 231.3000
			Data.iModel = 16
			Data.ComponentConfig.ComponentData[0].iDrawable = 1
			Data.ComponentConfig.ComponentData[0].iTexture = 0
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 0
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 1
			Data.ComponentConfig.ComponentData[3].iTexture = 0
			Data.ComponentConfig.ComponentData[4].iDrawable = 0
			Data.ComponentConfig.ComponentData[4].iTexture = 1
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 2
			Data.iLevel = 1
		BREAK

		CASE 41//edited
			Data.vPosition = <<958.7900, 43.3800, 115.6500>>
			Data.fHeading = 329.1000
			Data.iModel = 4
			Data.ComponentConfig.ComponentData[0].iDrawable = 0
			Data.ComponentConfig.ComponentData[0].iTexture = 0
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 0
			Data.ComponentConfig.ComponentData[2].iTexture = 1
			Data.ComponentConfig.ComponentData[3].iDrawable = 0
			Data.ComponentConfig.ComponentData[3].iTexture = 2
			Data.ComponentConfig.ComponentData[4].iDrawable = 1
			Data.ComponentConfig.ComponentData[4].iTexture = 2
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 1
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 2
			Data.iLevel = 1
		BREAK

		CASE 42//edited
			Data.vPosition = <<962.3000, 47.3000, 116.2000>>
			Data.fHeading = 0.3000
			Data.iModel = 6
			Data.ComponentConfig.ComponentData[0].iDrawable = 6
			Data.ComponentConfig.ComponentData[0].iTexture = 2
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 5
			Data.ComponentConfig.ComponentData[2].iTexture = 1
			Data.ComponentConfig.ComponentData[3].iDrawable = 0
			Data.ComponentConfig.ComponentData[3].iTexture = 1
			Data.ComponentConfig.ComponentData[4].iDrawable = 0
			Data.ComponentConfig.ComponentData[4].iTexture = 2
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 3
			Data.ComponentConfig.ComponentData[6].iDrawable = 2
			Data.ComponentConfig.ComponentData[6].iTexture = 1
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 14
			Data.iLevel = 1
		BREAK

		CASE 43//edited
			Data.vPosition = <<962.2600, 48.1900, 116.2000>>
			Data.fHeading = 182.0000
			Data.iModel = 27
			Data.ComponentConfig.ComponentData[0].iDrawable = 1
			Data.ComponentConfig.ComponentData[0].iTexture = 0
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 2
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 0
			Data.ComponentConfig.ComponentData[3].iTexture = 1
			Data.ComponentConfig.ComponentData[4].iDrawable = 0
			Data.ComponentConfig.ComponentData[4].iTexture = 1
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 28
			Data.iLevel = 1
		BREAK

		CASE 44//edited
			Data.vPosition = <<954.0800, 25.2100, 115.2000>>
			Data.fHeading = 154.8000
			Data.iModel = 3
			Data.ComponentConfig.ComponentData[0].iDrawable = 0
			Data.ComponentConfig.ComponentData[0].iTexture = 0
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 2
			Data.ComponentConfig.ComponentData[2].iTexture = 1
			Data.ComponentConfig.ComponentData[3].iDrawable = 1
			Data.ComponentConfig.ComponentData[3].iTexture = 4
			Data.ComponentConfig.ComponentData[4].iDrawable = 1
			Data.ComponentConfig.ComponentData[4].iTexture = 2
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 1
			Data.ComponentConfig.ComponentData[8].iTexture = 1
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 59
			Data.iLevel = 1
		BREAK

		CASE 45//edited
			Data.vPosition = <<957.7000, 21.6000, 115.2000>>
			Data.fHeading = 42.8000
			Data.iModel = 27
			Data.ComponentConfig.ComponentData[0].iDrawable = 0
			Data.ComponentConfig.ComponentData[0].iTexture = 1
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 1
			Data.ComponentConfig.ComponentData[2].iTexture = 2
			Data.ComponentConfig.ComponentData[3].iDrawable = 1
			Data.ComponentConfig.ComponentData[3].iTexture = 0
			Data.ComponentConfig.ComponentData[4].iDrawable = 0
			Data.ComponentConfig.ComponentData[4].iTexture = 2
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 1
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 2
			Data.ComponentConfig.ComponentData[11].iTexture = 1
			Data.iActivity = 56
			Data.iLevel = 1
		BREAK

		CASE 46//edited
			Data.vPosition = <<945.7000, 23.6000, 115.2000>>
			Data.fHeading = 157.4000
			Data.iModel = 25
			Data.ComponentConfig.ComponentData[0].iDrawable = 1
			Data.ComponentConfig.ComponentData[0].iTexture = 2
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 2
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 1
			Data.ComponentConfig.ComponentData[3].iTexture = 1
			Data.ComponentConfig.ComponentData[4].iDrawable = 0
			Data.ComponentConfig.ComponentData[4].iTexture = 1
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 2
			Data.ComponentConfig.ComponentData[11].iDrawable = 1
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 58
			Data.iLevel = 1
		BREAK

		CASE 47//edited
			Data.vPosition = <<944.9300, 22.7600, 115.2000>>
			Data.fHeading = 300.0000
			Data.iModel = 13
			Data.ComponentConfig.ComponentData[0].iDrawable = 1
			Data.ComponentConfig.ComponentData[0].iTexture = 2
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 0
			Data.ComponentConfig.ComponentData[2].iTexture = 1
			Data.ComponentConfig.ComponentData[3].iDrawable = 0
			Data.ComponentConfig.ComponentData[3].iTexture = 2
			Data.ComponentConfig.ComponentData[4].iDrawable = 0
			Data.ComponentConfig.ComponentData[4].iTexture = 2
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 2
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 7
			Data.iLevel = 1
		BREAK

		CASE 48//edited
			Data.vPosition = <<945.7100, 17.1000, 115.2000>>
			Data.fHeading = 150.9000
			Data.iModel = 15
			Data.ComponentConfig.ComponentData[0].iDrawable = 1
			Data.ComponentConfig.ComponentData[0].iTexture = 2
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 0
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 0
			Data.ComponentConfig.ComponentData[3].iTexture = 2
			Data.ComponentConfig.ComponentData[4].iDrawable = 0
			Data.ComponentConfig.ComponentData[4].iTexture = 1
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 17
			Data.iLevel = 1
		BREAK

		CASE 49//edited
			Data.vPosition = <<945.2700, 16.2700, 115.2000>>
			Data.fHeading = 333.3000
			Data.iModel = 11
			Data.ComponentConfig.ComponentData[0].iDrawable = 1
			Data.ComponentConfig.ComponentData[0].iTexture = 2
			Data.ComponentConfig.ComponentData[1].iDrawable = 0
			Data.ComponentConfig.ComponentData[1].iTexture = 0
			Data.ComponentConfig.ComponentData[2].iDrawable = 2
			Data.ComponentConfig.ComponentData[2].iTexture = 0
			Data.ComponentConfig.ComponentData[3].iDrawable = 0
			Data.ComponentConfig.ComponentData[3].iTexture = 1
			Data.ComponentConfig.ComponentData[4].iDrawable = 4
			Data.ComponentConfig.ComponentData[4].iTexture = 0
			Data.ComponentConfig.ComponentData[5].iDrawable = 0
			Data.ComponentConfig.ComponentData[5].iTexture = 0
			Data.ComponentConfig.ComponentData[6].iDrawable = 0
			Data.ComponentConfig.ComponentData[6].iTexture = 0
			Data.ComponentConfig.ComponentData[7].iDrawable = 0
			Data.ComponentConfig.ComponentData[7].iTexture = 0
			Data.ComponentConfig.ComponentData[8].iDrawable = 0
			Data.ComponentConfig.ComponentData[8].iTexture = 0
			Data.ComponentConfig.ComponentData[9].iDrawable = 0
			Data.ComponentConfig.ComponentData[9].iTexture = 0
			Data.ComponentConfig.ComponentData[10].iDrawable = 0
			Data.ComponentConfig.ComponentData[10].iTexture = 0
			Data.ComponentConfig.ComponentData[11].iDrawable = 0
			Data.ComponentConfig.ComponentData[11].iTexture = 0
			Data.iActivity = 18
			Data.iLevel = 1
		BREAK


	ENDSWITCH
ENDPROC

PROC GET_INITIAL_DATA_FOR_PED(PED_DATA &Data, INT iID)
	SWITCH GET_LAYOUT()
		CASE 0		GET_PED_DATA_LAYOUT_0(Data, iID)  BREAK
		CASE 1		GET_PED_DATA_LAYOUT_1(Data, iID)  BREAK		
	ENDSWITCH
ENDPROC

#IF IS_DEBUG_BUILD

FUNC STRING ACTIVITY_TO_STRING(PENTHOUSE_ACTIVITY eActivity)
	SWITCH eActivity
		CASE PA_AMBIENT							RETURN "PA_AMBIENT"
		CASE PA_DANCING_WITH_BEER 				RETURN "PA_DANCING_WITH_BEER"
		CASE PA_SIT_DRINKING 					RETURN "PA_SIT_DRINKING"
		CASE PA_HANGOUT_M_1A 					RETURN "PA_HANGOUT_M_1A"				
		CASE PA_HANGOUT_M_1B 					RETURN "PA_HANGOUT_M_1B" 				
		CASE PA_HANGOUT_M_2A					RETURN "PA_HANGOUT_M_2A"				
		CASE PA_HANGOUT_M_2B					RETURN "PA_HANGOUT_M_2B"				
		CASE PA_HANGOUT_M_3A					RETURN "PA_HANGOUT_M_3A"				
		CASE PA_HANGOUT_M_3B					RETURN "PA_HANGOUT_M_3B"				
		CASE PA_HANGOUT_CONVO_M_1A				RETURN "PA_HANGOUT_CONVO_M_1A"			
		CASE PA_HANGOUT_CONVO_M_1B				RETURN "PA_HANGOUT_CONVO_M_1B"			
		CASE PA_HANGOUT_CONVO_M_2A				RETURN "PA_HANGOUT_CONVO_M_2A"			
		CASE PA_HANGOUT_CONVO_M_2B				RETURN "PA_HANGOUT_CONVO_M_2B"			
		CASE PA_HANGOUT_CONVO_M_3A				RETURN "PA_HANGOUT_CONVO_M_3A"			
		CASE PA_HANGOUT_CONVO_M_3B				RETURN "PA_HANGOUT_CONVO_M_3B"			
		CASE PA_HANGOUT_WITHDRINK_M_01A			RETURN "PA_HANGOUT_WITHDRINK_M_01A"		
		CASE PA_HANGOUT_WITHDRINK_M_01B			RETURN "PA_HANGOUT_WITHDRINK_M_01B"		
		CASE PA_HANGOUT_CONVO_WITHDRINK_M_01A	RETURN "PA_HANGOUT_CONVO_WITHDRINK_M_01A"
		CASE PA_HANGOUT_CONVO_WITHDRINK_M_01B	RETURN "PA_HANGOUT_CONVO_WITHDRINK_M_01B"
		CASE PA_HANGOUT_F_1A					RETURN "PA_HANGOUT_F_1A"				
		CASE PA_HANGOUT_F_1B					RETURN "PA_HANGOUT_F_1B"			
		CASE PA_HANGOUT_F_2A					RETURN "PA_HANGOUT_F_2A"				
		CASE PA_HANGOUT_F_2B					RETURN "PA_HANGOUT_F_2B"				
		CASE PA_HANGOUT_F_3A					RETURN "PA_HANGOUT_F_3A"				
		CASE PA_HANGOUT_F_3B					RETURN "PA_HANGOUT_F_3B"				
		CASE PA_HANGOUT_CONVO_F_1A				RETURN "PA_HANGOUT_CONVO_F_1A"			
		CASE PA_HANGOUT_CONVO_F_1B				RETURN "PA_HANGOUT_CONVO_F_1B"			
		CASE PA_HANGOUT_CONVO_F_2A				RETURN "PA_HANGOUT_CONVO_F_2A"			
		CASE PA_HANGOUT_CONVO_F_2B				RETURN "PA_HANGOUT_CONVO_F_2B"			
		CASE PA_HANGOUT_CONVO_F_3A				RETURN "PA_HANGOUT_CONVO_F_3A"			
		CASE PA_HANGOUT_CONVO_F_3B				RETURN "PA_HANGOUT_CONVO_F_3B"			
		CASE PA_HANGOUT_WITHDRINK_F_01A			RETURN "PA_HANGOUT_WITHDRINK_F_01A"		
		CASE PA_HANGOUT_WITHDRINK_F_01B			RETURN "PA_HANGOUT_WITHDRINK_F_01B"		
		CASE PA_HANGOUT_CONVO_WITHDRINK_F_01A	RETURN "PA_HANGOUT_CONVO_WITHDRINK_F_01A"
		CASE PA_HANGOUT_CONVO_WITHDRINK_F_01B	RETURN "PA_HANGOUT_CONVO_WITHDRINK_F_01B"
		//NEWLY ADDED FOR FIXER
		CASE PA_NIGHTCLUB_DRINKING_BEER_FEMALE							RETURN "PA_NIGHTCLUB_DRINKING_BEER_FEMALE"
		CASE PA_NIGHTCLUB_hi_dance_crowd_09_v1_male						RETURN "PA_NIGHTCLUB_hi_dance_crowd_09_v1_male"
		CASE PA_NIGHTCLUB_hi_dance_crowd_17_v1_female					RETURN "PA_NIGHTCLUB_hi_dance_crowd_17_v1_female"
		CASE PA_NIGHTCLUB_hi_dance_crowd_13_v2_male						RETURN "PA_NIGHTCLUB_hi_dance_crowd_13_v2_male"
		CASE PA_NIGHTCLUB_hi_dance_crowd_15_v1_male						RETURN "PA_NIGHTCLUB_hi_dance_crowd_15_v1_male"
		CASE PA_NIGHTCLUB_hi_dance_prop_09_v1_male						RETURN "PA_NIGHTCLUB_hi_dance_prop_09_v1_male"
		CASE PA_NIGHTCLUB_hi_dance_prop_13_v2_male						RETURN "PA_NIGHTCLUB_hi_dance_prop_13_v2_male"
		CASE PA_NIGHTCLUB_hi_dance_prop_17_v1_male						RETURN "PA_NIGHTCLUB_hi_dance_prop_17_v1_male"
		CASE PA_NIGHTCLUB_mi_dance_crowd_17_v2_female					RETURN "PA_NIGHTCLUB_mi_dance_crowd_17_v2_female"
		CASE PA_NIGHTCLUB_mi_idle_c_f02									RETURN "PA_NIGHTCLUB_mi_idle_c_f02"
		CASE PA_NIGHTCLUB_mi_loop_f02									RETURN "PA_NIGHTCLUB_mi_loop_f02"
		CASE PA_NIGHTCLUB_mi_to_li_f02									RETURN "PA_NIGHTCLUB_mi_to_li_f02"
		CASE PA_NIGHTCLUB_trans_dance_crowd_li_to_mi_11_v1_male			RETURN "PA_NIGHTCLUB_trans_dance_crowd_li_to_mi_11_v1_male"
	ENDSWITCH
	RETURN ""
ENDFUNC

PROC RENDER_PED_INFO(PED_DATA &Data, INT iID, BOOL bDoHighlight=FALSE)

	HUD_COLOURS HudColour
	INT iRed, iGreen, iBlue
	INT iAlpha = 100
	VECTOR vActualPedPos, vDrawPos
	INT iRow = 0
	FLOAT fRowZ = 0.06
	
	TEXT_LABEL_63 str
	str = "PenthousePed["
	str += iID
	str += "]"

	IF (bDoHighlight)
		HudColour = HUD_COLOUR_ORANGE
	ELSE
		HudColour = HUD_COLOUR_PURE_WHITE
	ENDIF
	GET_HUD_COLOUR(HudColour, iRed, iGreen, iBlue, iAlpha)
	
	vActualPedPos = Data.vPosition
	IF DOES_ENTITY_EXIST(Data.PedID)
		vActualPedPos = GET_ENTITY_COORDS(Data.PedID, FALSE)
	ENDIF
	vDrawPos = vActualPedPos
	
	DRAW_DEBUG_SPAWN_POINT(Data.vPosition, Data.fHeading, HudColour, 0, 0.5)
	DRAW_DEBUG_TEXT(str, <<vDrawPos.x, vDrawPos.y, vDrawPos.z + (fRowZ* TO_FLOAT(iRow))>>, iRed, iGreen, iBlue, iAlpha)
	
	// iModel
	iRow++
	str = "iModel: "
	str += Data.iModel
	DRAW_DEBUG_TEXT(str, <<vDrawPos.x, vDrawPos.y, vDrawPos.z + (fRowZ* TO_FLOAT(iRow))>>, iRed, iGreen, iBlue, iAlpha)		
	
	// Activity
	iRow++
	str = "Activity: "
	str += ACTIVITY_TO_STRING(INT_TO_ENUM(PENTHOUSE_ACTIVITY, Data.iActivity))
	DRAW_DEBUG_TEXT(str, <<vDrawPos.x, vDrawPos.y, vDrawPos.z + (fRowZ* TO_FLOAT(iRow))>>, iRed, iGreen, iBlue, iAlpha)
				
	// iPedToHeadTrack
//	iRow++
//	str = "iPedToHeadTrack: "
//	str += Data.iPedToHeadTrack
//	DRAW_DEBUG_TEXT(str, <<vDrawPos.x, vDrawPos.y, vDrawPos.z + (fRowZ* TO_FLOAT(iRow))>>, iRed, iGreen, iBlue, iAlpha)	
	
	// iPedToHeadTrack
	iRow++
	str = "iLevel: "
	str += Data.iLevel
	DRAW_DEBUG_TEXT(str, <<vDrawPos.x, vDrawPos.y, vDrawPos.z + (fRowZ* TO_FLOAT(iRow))>>, iRed, iGreen, iBlue, iAlpha)	
	
ENDPROC

FUNC STRING DEBUG_GET_PED_MODEL_STRING(INT i)
	SWITCH i
		CASE 0 RETURN	"a_f_y_smartcaspat_01"
		CASE 1 RETURN	"a_f_y_clubcust_01"
		CASE 2 RETURN	"a_f_y_clubcust_03"
		CASE 3 RETURN	"A_F_Y_Genhot_01"
		CASE 4 RETURN	"A_F_Y_BevHills_02"
		CASE 5 RETURN	"a_m_y_smartcaspat_01"
		CASE 6 RETURN	"A_M_Y_CarClub_01"
		CASE 7 RETURN	"G_M_Y_BallaOrig_01"
		CASE 8 RETURN	"A_M_M_SouCent_01"
		CASE 9 RETURN	"G_M_Y_FamFOR_01"
		CASE 10 RETURN	"a_m_y_clubcust_01"
		CASE 11 RETURN	"a_m_y_clubcust_02"
		CASE 12 RETURN	"A_M_Y_StudioParty_01"
		CASE 13 RETURN	"G_M_Y_BallaSout_01"
		CASE 14 RETURN	"G_M_Y_FamCA_01"
		CASE 15 RETURN	"G_M_Y_FamDNF_01"
		CASE 16 RETURN	"G_M_Y_MexGoon_03"
		CASE 17 RETURN	"A_M_M_StudioParty_01"
		CASE 18 RETURN	"A_M_Y_SouCent_02"
		CASE 19 RETURN	"a_m_y_vinewood_01"
		CASE 20 RETURN	"a_m_y_vinewood_02"
		CASE 21 RETURN	"A_M_Y_EastSA_02"
		CASE 22 RETURN	"A_M_Y_indian_01"
		CASE 23 RETURN	"A_F_Y_CarClub_01"
		CASE 24 RETURN	"A_F_Y_StudioParty_01"
		CASE 25 RETURN	"G_F_Y_ballas_01"
		CASE 26 RETURN	"A_F_Y_StudioParty_02"
		CASE 27 RETURN	"G_F_Y_Vagos_01"
	ENDSWITCH
	RETURN "INVALID"
ENDFUNC

PROC CREATE_PENTHOUSE_PED_WIDGET(PED_DATA &Data, INT iID)
	TEXT_LABEL_63 str
	str = "PenthousePed["
	str += iID
	str += "]"
	START_WIDGET_GROUP(str)
		ADD_WIDGET_INT_SLIDER("iModel", Data.iModel, 0, 27, 1)
		Data.twModel = ADD_TEXT_WIDGET("Ped Model: ")	
		SET_CONTENTS_OF_TEXT_WIDGET(Data.twModel, DEBUG_GET_PED_MODEL_STRING(Data.iModel))
		
	
		ADD_WIDGET_VECTOR_SLIDER("Position", Data.vPosition, -9999.9, 9999.9, 0.01)
		ADD_WIDGET_FLOAT_SLIDER("Heading", Data.fHeading, 0.0, 360.0, 0.1)
		ADD_WIDGET_INT_SLIDER("iActivity", Data.iActivity, 0, ENUM_TO_INT(PA_TOTAL) - 1, 1)
		INT iComp
		REPEAT NUM_PED_COMPONENTS iComp
			START_WIDGET_GROUP(GET_PED_MODEL_COMP_STRING(iComp))
				ADD_WIDGET_INT_SLIDER("Drawable: ", Data.ComponentConfig.ComponentData[iComp].iDrawable, 0, MAX_NUM_PED_CLOTHING_COMPONENT_VALUE, 1)
				ADD_WIDGET_INT_SLIDER("Texture: ", 	Data.ComponentConfig.ComponentData[iComp].iTexture, 0, MAX_NUM_PED_CLOTHING_COMPONENT_VALUE, 1)
			STOP_WIDGET_GROUP()
		ENDREPEAT
		
		START_WIDGET_GROUP("PROPS")	
		REPEAT PENTHOUSE_PED_MAX_PROPS iComp
			START_WIDGET_GROUP(CONVERT_INT_TO_STRING(iComp))
				ADD_WIDGET_BOOL("bAttachProp[", Data.bAttachProp[iComp])
				ADD_WIDGET_INT_SLIDER("iPropPosition", Data.iPropPosition[iComp], -1, 255, 1)
				ADD_WIDGET_INT_SLIDER("iPropVariant",  Data.iPropVariant[iComp], -1, 255, 1)
			STOP_WIDGET_GROUP()	
		ENDREPEAT
		STOP_WIDGET_GROUP()
	
		//ADD_WIDGET_INT_SLIDER("iPedToHeadTrack", Data.iPedToHeadTrack, 0, HIGHEST_INT, 1)
		ADD_WIDGET_INT_SLIDER("iLevel", Data.iLevel, 0, 5, 1)
		ADD_WIDGET_BOOL("bApplyChanges", Data.bApplyChanges)
	STOP_WIDGET_GROUP()
	
ENDPROC	
PROC CREATE_WIDGET()
	INT i
	
	START_WIDGET_GROUP("PENTHOUSE PEDS")
		
		START_WIDGET_GROUP("ServerBD")
			ADD_WIDGET_BOOL("bServerDataInitialised", ServerBD.bServerDataInitialised)			
			ADD_WIDGET_INT_READ_ONLY("iLayout", ServerBD.iLayout)
			ADD_WIDGET_INT_READ_ONLY("iLevel", ServerBD.iLevel)
		STOP_WIDGET_GROUP()
	
		ADD_WIDGET_STRING("Changing the Layout will lose any changes to the ped data!")	
		ADD_WIDGET_INT_SLIDER("Layout", iCurrentLayout, -1, 1, 0)
		ADD_WIDGET_INT_SLIDER("Level", iCurrentLevel, -1, 5, 1)
		#IF FEATURE_FIXER
		ADD_WIDGET_BOOL("Set fixer mission layout", g_bTurnOnMissionPenthousePartyPeds)
		#ENDIF
		
		ADD_WIDGET_BOOL("Output Ped Data To File", bOutputToFile)
		//ADD_WIDGET_BOOL("Update Positions", bUpdatePositions)
		ADD_WIDGET_BOOL("bShowDebug", bShowDebug)
		ADD_WIDGET_BOOL("bRecreatePeds", bRecreatePeds)
		
		ADD_WIDGET_INT_READ_ONLY("Count Level 1", iCount_Level[0])
		ADD_WIDGET_INT_READ_ONLY("Count Level 2", iCount_Level[1])
		ADD_WIDGET_INT_READ_ONLY("Count Level 3", iCount_Level[2])
		ADD_WIDGET_INT_READ_ONLY("Count Level 4", iCount_Level[3])
		ADD_WIDGET_INT_READ_ONLY("Count Level 5", iCount_Level[4])
	
		REPEAT MAX_NUM_PENTHOUSE_PEDS i
			CREATE_PENTHOUSE_PED_WIDGET(PenthousePed[i], i)
		ENDREPEAT
	
	STOP_WIDGET_GROUP()
ENDPROC

FUNC BOOL IS_DATA_EQUAL(PED_DATA& Data1, PED_DATA &Data2)
	IF (VDIST(Data1.vPosition,Data2.vPosition) < 0.01)
	AND (Data1.fHeading = Data2.fHeading)
	AND (Data1.ComponentConfig.ComponentData[0].iDrawable = Data2.ComponentConfig.ComponentData[0].iDrawable)
	AND (Data1.ComponentConfig.ComponentData[0].iTexture = Data2.ComponentConfig.ComponentData[0].iTexture)
	AND (Data1.ComponentConfig.ComponentData[1].iDrawable = Data2.ComponentConfig.ComponentData[1].iDrawable)
	AND (Data1.ComponentConfig.ComponentData[1].iTexture = Data2.ComponentConfig.ComponentData[1].iTexture)
	AND (Data1.ComponentConfig.ComponentData[2].iDrawable = Data2.ComponentConfig.ComponentData[2].iDrawable)
	AND (Data1.ComponentConfig.ComponentData[2].iTexture = Data2.ComponentConfig.ComponentData[2].iTexture)
	AND (Data1.ComponentConfig.ComponentData[3].iDrawable = Data2.ComponentConfig.ComponentData[3].iDrawable)
	AND (Data1.ComponentConfig.ComponentData[3].iTexture = Data2.ComponentConfig.ComponentData[3].iTexture)
	AND (Data1.ComponentConfig.ComponentData[4].iDrawable = Data2.ComponentConfig.ComponentData[4].iDrawable)
	AND (Data1.ComponentConfig.ComponentData[4].iTexture = Data2.ComponentConfig.ComponentData[4].iTexture)
	AND (Data1.ComponentConfig.ComponentData[5].iDrawable = Data2.ComponentConfig.ComponentData[5].iDrawable)
	AND (Data1.ComponentConfig.ComponentData[5].iTexture = Data2.ComponentConfig.ComponentData[5].iTexture)
	AND (Data1.ComponentConfig.ComponentData[6].iDrawable = Data2.ComponentConfig.ComponentData[6].iDrawable)
	AND (Data1.ComponentConfig.ComponentData[6].iTexture = Data2.ComponentConfig.ComponentData[6].iTexture)
	AND (Data1.ComponentConfig.ComponentData[7].iDrawable = Data2.ComponentConfig.ComponentData[7].iDrawable)
	AND (Data1.ComponentConfig.ComponentData[7].iTexture = Data2.ComponentConfig.ComponentData[7].iTexture)
	AND (Data1.ComponentConfig.ComponentData[8].iDrawable = Data2.ComponentConfig.ComponentData[8].iDrawable)
	AND (Data1.ComponentConfig.ComponentData[8].iTexture = Data2.ComponentConfig.ComponentData[8].iTexture)
	AND (Data1.ComponentConfig.ComponentData[9].iDrawable = Data2.ComponentConfig.ComponentData[9].iDrawable)
	AND (Data1.ComponentConfig.ComponentData[9].iTexture = Data2.ComponentConfig.ComponentData[9].iTexture)
	AND (Data1.ComponentConfig.ComponentData[10].iDrawable = Data2.ComponentConfig.ComponentData[10].iDrawable)
	AND (Data1.ComponentConfig.ComponentData[10].iTexture = Data2.ComponentConfig.ComponentData[10].iTexture)
	AND (Data1.ComponentConfig.ComponentData[11].iDrawable = Data2.ComponentConfig.ComponentData[11].iDrawable)
	AND (Data1.ComponentConfig.ComponentData[11].iTexture = Data2.ComponentConfig.ComponentData[11].iTexture)
	AND (Data1.bAttachProp[0] = Data2.bAttachProp[0])
	AND (Data1.bAttachProp[1] = Data2.bAttachProp[1])
	AND (Data1.bAttachProp[2] = Data2.bAttachProp[2])
	AND (Data1.iPropPosition[0] = Data2.iPropPosition[0])
	AND (Data1.iPropPosition[1] = Data2.iPropPosition[1])
	AND (Data1.iPropPosition[2] = Data2.iPropPosition[2])
	AND (Data1.iPropVariant[0] = Data2.iPropVariant[0])
	AND (Data1.iPropVariant[1] = Data2.iPropVariant[1])
	AND (Data1.iPropVariant[2] = Data2.iPropVariant[2])
	AND (Data1.iModel = Data2.iModel)
	AND (Data1.iActivity = Data2.iActivity)
	AND (Data1.iLevel = Data2.iLevel)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC



PROC OUTPUT_PED_DATA(PED_DATA &Data, INT iID)
	
	TEXT_LABEL_63 str
	
	str = "CASE "
	str += iID
	
	IF (Data.bHasBeenEdited)
		str += "//edited"	
	ENDIF

	SAVE_STRING_TO_NAMED_DEBUG_FILE(str, sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	Data.vPosition = ", sPath, sFile)
	SAVE_VECTOR_TO_NAMED_DEBUG_FILE(Data.vPosition, sPath, sFile) 
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	Data.fHeading = ", sPath, sFile)
	SAVE_FLOAT_TO_NAMED_DEBUG_FILE(Data.fHeading, sPath, sFile) 
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	Data.iModel = ", sPath, sFile)	
	SAVE_INT_TO_NAMED_DEBUG_FILE(Data.iModel, sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
		
	INT iComp	
	TEXT_LABEL_63 Text
	REPEAT NUM_PED_COMPONENTS iComp
		Text = "	Data.ComponentConfig.ComponentData["
		Text+=iComp
		Text+="].iDrawable = "
		
		SAVE_STRING_TO_NAMED_DEBUG_FILE(Text, sPath, sFile)	
		SAVE_INT_TO_NAMED_DEBUG_FILE(GET_PED_DRAWABLE_VARIATION(Data.PedID, INT_TO_ENUM(PED_COMPONENT, iComp)), sPath, sFile) 
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)	
		
		Text = "	Data.ComponentConfig.ComponentData["
		Text+=iComp
		Text+="].iTexture = "
		SAVE_STRING_TO_NAMED_DEBUG_FILE(Text, sPath, sFile)	
		SAVE_INT_TO_NAMED_DEBUG_FILE(GET_PED_TEXTURE_VARIATION(Data.PedID, INT_TO_ENUM(PED_COMPONENT, iComp)), sPath, sFile) 
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)			
	ENDREPEAT
	

	REPEAT PENTHOUSE_PED_MAX_PROPS iComp
		IF Data.bAttachProp[iComp]
			Text = "	Data.bAttachProp["
			Text+=iComp
			Text+="] = TRUE"
			SAVE_STRING_TO_NAMED_DEBUG_FILE(Text, sPath, sFile)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)	
			
			Text = "	Data.iPropPosition["
			Text+=iComp
			Text+="] = "
			
			SAVE_STRING_TO_NAMED_DEBUG_FILE(Text, sPath, sFile)	
			SAVE_INT_TO_NAMED_DEBUG_FILE(Data.iPropPosition[iComp], sPath, sFile) 
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)	
			
			Text = "	Data.iPropVariant["
			Text+=iComp
			Text+="] = "
			SAVE_STRING_TO_NAMED_DEBUG_FILE(Text, sPath, sFile)	
			SAVE_INT_TO_NAMED_DEBUG_FILE(Data.iPropVariant[iComp], sPath, sFile) 
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)		
		ENDIF
	ENDREPEAT
	
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	Data.iActivity = ", sPath, sFile)
	SAVE_INT_TO_NAMED_DEBUG_FILE(Data.iActivity, sPath, sFile) 
	//SAVE_STRING_TO_NAMED_DEBUG_FILE(ACTIVITY_TO_STRING(Data.eActivity), sPath, sFile) 
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)	
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	Data.iLevel = ", sPath, sFile)
	SAVE_INT_TO_NAMED_DEBUG_FILE(Data.iLevel, sPath, sFile) 
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)	
	
//	SAVE_STRING_TO_NAMED_DEBUG_FILE("	Data.iPedToHeadTrack = ", sPath, sFile)
//	SAVE_INT_TO_NAMED_DEBUG_FILE(Data.iPedToHeadTrack, sPath, sFile) 
//	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)	
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)

ENDPROC

PROC MAINTAIN_WIDGET()
	
	PED_DATA data
	INT i
	REPEAT MAX_NUM_PENTHOUSE_PEDS i
		IF PenthousePed[i].bHasBeenEdited = FALSE
			GET_INITIAL_DATA_FOR_PED(data, i)
			IF NOT IS_DATA_EQUAL(data, PenthousePed[i])
				PenthousePed[i].bHasBeenEdited= TRUE
			ENDIF
		ENDIF	
		IF data.iModel != PenthousePed[i].iModel
			PenthousePed[i].bModelChanged = TRUE
			SET_CONTENTS_OF_TEXT_WIDGET(PenthousePed[i].twModel, DEBUG_GET_PED_MODEL_STRING(PenthousePed[i].iModel))
		ELIF PenthousePed[i].bModelChanged
			PenthousePed[i].bModelChanged = FALSE
			SET_CONTENTS_OF_TEXT_WIDGET(PenthousePed[i].twModel, DEBUG_GET_PED_MODEL_STRING(PenthousePed[i].iModel))
		ENDIF
	ENDREPEAT
	
//	IF (bUpdatePositions)
//		REPEAT MAX_NUM_PENTHOUSE_PEDS i
//			IF DOES_ENTITY_EXIST(PenthousePed[i].PedID)
//			AND NOT IS_ENTITY_DEAD(PenthousePed[i].PedID)
//				SET_ENTITY_COORDS(PenthousePed[i].PedID, PenthousePed[i].vPosition, FALSE, TRUE, TRUE, TRUE)
//				SET_ENTITY_HEADING(PenthousePed[i].PedID, PenthousePed[i].fHeading)
//			ENDIF
//		ENDREPEAT		
//	ENDIF
	
	IF (bOutputToFile)
		
		INT iYear, iMonth, iDay, iHour, iMins, iSec
		GET_LOCAL_TIME(iYear, iMonth, iDay, iHour, iMins, iSec)
		
		sFile = "penthouse_ped_data_"
		IF (iHour < 10)
			sFile += 0
		ENDIF
		sFile += iHour
		sFile += "."
		IF (iMins < 10)
			sFile += 0
		ENDIF
		sFile += iMins
		sFile += "."
		IF (iSec < 10)
			sFile += 0
		ENDIF
		sFile += iSec
		sFile += ".txt"

		CLEAR_NAMED_DEBUG_FILE(sPath, sFile)
		OPEN_NAMED_DEBUG_FILE(sPath, sFile)
	
		SAVE_STRING_TO_NAMED_DEBUG_FILE("iCurrentLayout = ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE(iCurrentLayout, sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)	
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)	
	
		REPEAT MAX_NUM_PENTHOUSE_PEDS i
			IF IS_ENTITY_ALIVE(PenthousePed[i].PedID)
				OUTPUT_PED_DATA(PenthousePed[i], i)
			ENDIF
		ENDREPEAT
	
		CLOSE_DEBUG_FILE()
		
		bOutputToFile= FALSE
	ENDIF
	
	IF (bShowDebug)
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
		REPEAT MAX_NUM_PENTHOUSE_PEDS i
			RENDER_PED_INFO(PenthousePed[i], i)
		ENDREPEAT
	ENDIF

	IF (bRecreatePeds)
		CLEANUP_ALL_PENTHOUSE_PEDS()
		bRecreatePeds = FALSE
	ENDIF

ENDPROC
#ENDIF

PROC PERFORM_SEQUENCE(PED_DATA &Data)
	
	STRING sDict = GET_ANIM_DICTIONARY_FOR_ACTIVITY(Data)
	STRING sClip = GET_ANIM_CLIP_FOR_ACTIVITY(Data)
	FLOAT fRand = GET_RANDOM_FLOAT_IN_RANGE(0, 0.7) 
	SCRIPTTASKSTATUS TaskStatus
	
	TaskStatus = GET_SCRIPT_TASK_STATUS(Data.PedID, SCRIPT_TASK_PERFORM_SEQUENCE)
	IF (TaskStatus != WAITING_TO_START_TASK)
	AND (TaskStatus != PERFORMING_TASK)		
		IF HAS_ANIM_DICT_LOADED(sDict)			
			SEQUENCE_INDEX seq
			OPEN_SEQUENCE_TASK(seq)
				IF NOT IsMultiStageAnim(Data)
					IF IS_ACTIVITY_SEATED(Data)
						TASK_PLAY_ANIM_ADVANCED(NULL, sDict, sClip, Data.vPosition, <<0,0,Data.fHeading>>, DEFAULT, DEFAULT, DEFAULT, AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION, fRand, DEFAULT, AIK_DISABLE_ARM_IK | AIK_DISABLE_LEG_IK)											
					ELSE
						TASK_PLAY_ANIM(NULL, sDict, sClip, DEFAULT, DEFAULT,DEFAULT, AF_LOOPING, fRand)
					ENDIF		
				ELSE
					
					INT iTask[4] 
					iTask[0] = 0
					iTask[1] = 1
					iTask[2] = 2
					iTask[3] = 3		
					
					// shuffle tasks so they play in a different order
					INT iRd1, iRd2
					INT i
					INT iStored
					REPEAT 10 i
						iRd1 = GET_RANDOM_INT_IN_RANGE(0, 4)	
						iRd2 = GET_RANDOM_INT_IN_RANGE(0, 4)
						iStored = iTask[iRd1]
						iTask[iRd1] = iTask[iRd2] 
						iTask[iRd2] = iStored
					ENDREPEAT					
						
					REPEAT 4 i
						SWITCH iTask[i]
							CASE 0
								TASK_PLAY_ANIM(NULL, sDict, "IDLE_A", DEFAULT, DEFAULT,DEFAULT, AF_DEFAULT, 0)
							BREAK
							CASE 1
								TASK_PLAY_ANIM(NULL, sDict, "IDLE_B", DEFAULT, DEFAULT,DEFAULT, AF_DEFAULT, 0)
							BREAK
							CASE 2
								TASK_PLAY_ANIM(NULL, sDict, "IDLE_C", DEFAULT, DEFAULT,DEFAULT, AF_DEFAULT, 0)
							BREAK
							CASE 3
								TASK_PLAY_ANIM(NULL, sDict, "IDLE_D", DEFAULT, DEFAULT,DEFAULT, AF_DEFAULT, 0)	
							BREAK
						ENDSWITCH
					ENDREPEAT
					
				ENDIF
				SET_SEQUENCE_TO_REPEAT(seq, REPEAT_FOREVER)
			CLOSE_SEQUENCE_TASK(seq)				
			TASK_PERFORM_SEQUENCE(Data.PedID, seq)	
			CLEAR_SEQUENCE_TASK(seq)
		ENDIF
	ENDIF

ENDPROC

PROC UPDATE_PED_ACTIVITY(PED_DATA &Data)
	
	IF DOES_ENTITY_EXIST(Data.PedID)
	AND NOT IS_ENTITY_DEAD(Data.PedID)
	
		PERFORM_SEQUENCE(Data)			

	ENDIF

ENDPROC

FUNC INT GetNearestPedToMiddle()
	
	FLOAT fNearestDist = 999999.9
	FLOAT fDist
	INT iNearest = -1
	INT i
	REPEAT MAX_NUM_PENTHOUSE_PEDS i 
		IF PenthousePed[i].iLevel <= ServerBD.iLevel
			fDist = VDIST(PenthousePed[i].vPosition, <<945,15,117>>)
			IF (fDist < fNearestDist)
				fNearestDist = fDist	
				iNearest = i
			ENDIF
		ENDIF
	ENDREPEAT
	
	PRINTLN("[CASINO_PEDS] GetNearestPedToMiddle returning ", iNearest)
	RETURN iNearest
	
ENDFUNC

PROC UPDATE_AMBIENT_SOUND()
	
	IF iAudioState = 0
		IF LOAD_STREAM("Party_Walla_Stream", "DLC_Vinewood_Casino_Penthouse_Party_Sounds")
			PRINTLN("[CASINO_PEDS] UPDATE_AMBIENT_SOUND loaded ")
			iAudioState++	
		ELSE
			PRINTLN("[CASINO_PEDS] UPDATE_AMBIENT_SOUND loading ")
		ENDIF
	ENDIF
	
	IF (iAudioState = 1)
		INT i = GetNearestPedToMiddle()
		IF (i > -1)
			IF DOES_ENTITY_EXIST(PenthousePed[i].PedID)
			AND NOT IS_ENTITY_DEAD(PenthousePed[i].PedID)
				PLAY_STREAM_FROM_PED(PenthousePed[i].PedID)	
				PRINTLN("[CASINO_PEDS] UPDATE_AMBIENT_SOUND PLAY_STREAM_FROM_PEDPLAY_STREAM_FROM_PED on ped ", i)
				iAudioState++
			ENDIF
		ENDIF
	ENDIF
	
	IF (iAudioState = 2)
		IF IS_STREAM_PLAYING()
			FLOAT fCrowdSize = TO_FLOAT(ServerBD.iLevel) / 5.0
			SET_VARIABLE_ON_STREAM("Crowd_Size",fCrowdSize)
			PRINTLN("[CASINO_PEDS] UPDATE_AMBIENT_SOUND SET_VARIABLE_ON_STREAM fCrowdSize = ", fCrowdSize)
			iAudioState++
		ENDIF
	ENDIF
	
ENDPROC

PROC UPDATE_PENTHOUSE_PEDS()


	INT i
	

	#IF IS_DEBUG_BUILD
	REPEAT 5 i
		iCount_Level[i] = 0	
	ENDREPEAT
	#ENDIF	
	
	REPEAT MAX_NUM_PENTHOUSE_PEDS i
		
		IF SHOULD_CREATE_PENTHOUSE_PED(PenthousePed[i])
	
			IF NOT DOES_ENTITY_EXIST(PenthousePed[i].PedID)
				CREATE_PENTHOUSE_PED(PenthousePed[i] #IF IS_DEBUG_BUILD ,i #ENDIF)
			ELSE
				IF IS_ENTITY_DEAD(PenthousePed[i].PedID)
					DELETE_PENTHOUSE_PED(PenthousePed[i])	
				ENDIF
			ENDIF
			
			UPDATE_PED_ACTIVITY(PenthousePed[i])
			
		ENDIF
		
		#IF IS_DEBUG_BUILD
		
		IF PenthousePed[i].iLevel > 0
			iCount_Level[PenthousePed[i].iLevel-1]++
		ENDIF
		
		IF (PenthousePed[i].bApplyChanges)
			DELETE_PENTHOUSE_PED(PenthousePed[i])	
			PenthousePed[i].bApplyChanges = FALSE
		ENDIF
		#ENDIF		
		
	ENDREPEAT


	iPedsCreatedThisFrame = 0 // reset for next frame

ENDPROC

PROC INITIALISE_PED_DATA()

	INT i
	REPEAT MAX_NUM_PENTHOUSE_PEDS i
		GET_INITIAL_DATA_FOR_PED(PenthousePed[i], i)
	ENDREPEAT
ENDPROC

PROC SCRIPT_CLEANUP()
	IF IS_STREAM_PLAYING()
		PRINTLN("[CASINO_PEDS] SCRIPT_CLEANUP STOP_STREAM ")
		STOP_STREAM()
	ENDIF
	CLEANUP_ALL_PENTHOUSE_PEDS()
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

FUNC INT GET_LEVEL_FROM_CHIPS(INT iChips)
	IF iChips >= g_sMPTunables.iVC_PENTHOUSE_CHIPS_MEMBERS_5
		RETURN 5
	ELIF iChips >= g_sMPTunables.iVC_PENTHOUSE_CHIPS_MEMBERS_4
		RETURN 4
	ELIF iChips >= g_sMPTunables.iVC_PENTHOUSE_CHIPS_MEMBERS_3
		RETURN 3
	ELIF iChips >= g_sMPTunables.iVC_PENTHOUSE_CHIPS_MEMBERS_2
		RETURN 2
	ENDIF
	RETURN 1 // default
ENDFUNC

PROC INIT_PENTHOUSE_PEDS(BOOL bInitData=TRUE)
	CLEANUP_ALL_PENTHOUSE_PEDS()
	IF (bInitData)
		INITIALISE_PED_DATA()	
	ENDIF
	REQUEST_ALL_PENTHOUSE_ASSETS()	
	
	#IF IS_DEBUG_BUILD
		iLastLayout = iCurrentLayout
		iLastLevel = iCurrentLevel
	#ENDIF
ENDPROC

SCRIPT(PENTHOUSE_DATA OwnerData)
	
	g_bTurnOnPenthousePeds = TRUE

	PROCESS_PRE_GAME()
	
	// Set the server data
	IF NETWORK_IS_GAME_IN_PROGRESS()
		WHILE (serverBD.bServerDataInitialised = FALSE)
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				serverBD.iLevel = GET_LEVEL_FROM_CHIPS(OwnerData.iChips)
				serverBD.iLayout = GET_INITIAL_LAYOUT()
				serverBD.bServerDataInitialised = TRUE
				PRINTLN("[PENTHOUSE_PEDS] iLevel set to ", serverBD.iLevel)
				PRINTLN("[PENTHOUSE_PEDS] iLayout set to ", serverBD.iLayout)
			ELSE
				PRINTLN("[PENTHOUSE_PEDS] waiting for bServerDataInitialised")
				WAIT(0)				
			ENDIF
		ENDWHILE
	ENDIF
	
	#IF IS_DEBUG_BUILD
	iCurrentLayout = ServerBD.iLayout
	iCurrentLevel = ServerBD.iLevel
	#ENDIF
	
	INIT_PENTHOUSE_PEDS()
	
	#IF IS_DEBUG_BUILD
	CREATE_WIDGET()
	#ENDIF
	
	WHILE TRUE

		MP_LOOP_WAIT_ZERO()

		IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		OR SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		OR (g_bTurnOnPenthousePeds = FALSE)
		OR SHOULD_CASINO_PENTHOUSE_MISSION_PEDS_CLEANUP()
			SCRIPT_CLEANUP()	
		ENDIF
		
		#IF IS_DEBUG_BUILD
		MAINTAIN_WIDGET()
		#ENDIF
		
		UPDATE_PENTHOUSE_PEDS()
		
		UPDATE_AMBIENT_SOUND()
		
		#IF IS_DEBUG_BUILD
		IF (iCurrentLayout != iLastLayout)
			INIT_PENTHOUSE_PEDS()
		ENDIF
		IF (iCurrentLevel != iLastLevel)
			INIT_PENTHOUSE_PEDS(FALSE)
		ENDIF
		#ENDIF
		
	ENDWHILE
ENDSCRIPT
#ENDIF // FEATURE_CASINO
