USING "globals.sch"
USING "net_simple_cutscene.sch"
USING "GB_DELIVERY_DEFINITIONS.sch"
USING "GB_DELIVERY_CHILD_SUPPORT.sch"
USING "gang_ops_defunct_base_entry_cutscenes.sch"
USING "net_simple_interior.sch"

//DEFINITIONS
CONST_INT DEFUNCT_BASE_DELIVERY_CUTSCENE_WALK_IN 					0
CONST_INT DEFUNCT_BASE_DELIVERY_CUTSCENE_LEAVE_VEHICLE_OUTSIDE		1
CONST_INT DEFUNCT_BASE_DELIVERY_CUTSCENE_BRING_VEHICLE_INSIDE		2
CONST_INT DEFUNCT_BASE_DELIVERY_CUTSCENE_AVENGER					3
CONST_INT DEFUNCT_BASE_DELIVERY_CUTSCENE_BRING_PERSONAL_VEH_INSIDE	4

//OBEJCTS
CONST_INT DB_DELIVERY_OBJECT_DEFUNCT_BASE_LIFT						0
//VECTORS

//INT ARRAY
CONST_INT DB_DELIVERY_INT_ARRAY_BITSET 								0
CONST_INT DB_DELIVERY_INT_ARRAY_DELIVERY_TYPE						1 //DEFUNCT_BASE_DELIVERY_CUTSCENE_TYPE
CONST_INT DB_DELIVERY_INT_ARRAY_SOUND_ID 							2
CONST_INT DB_DELIVERY_INT_ARRAY_SCENE_ID 							3

//BITSET //DB_DELIVERY_INT_ARRAY_BITSET
CONST_INT DB_DELIVERY_BITSET_TASK_MOVE_LIFT							0
CONST_INT DB_DELIVERY_BITSET_TASK_PEDS_LEAVE_VEHICLE				1
CONST_INT DB_DELIVERY_BITSET_TASK_MOVE_VEH_FORWARD					2
CONST_INT DB_DELIVERY_BITSET_TASK_FADED_SCREEN_IN					3
CONST_INT DB_DELIVERY_BITSET_TASK_PLAYED_DOOR_CLOSING_SOUND			4
CONST_INT DB_DELIVERY_BITSET_TASK_PLAYED_RELEASE_SOUND_ON_CLEANUP	5
CONST_INT DB_DELIVERY_BITSET_MAP_IPL_REQUESTED						6
CONST_INT DB_DELIVERY_BITSET_CUTSCENE_RELEASED_ASSETS				7
CONST_INT DB_DELIVERY_BITSET_STARTED_LOAD_SCENE						8
CONST_INT DB_DELIVERY_BITSET_CUTSCENE_STARTED						9
CONST_INT DB_DELIVERY_BITSET_TASK_MOVE_INSIDE						10

//audioBS
//CONST_INT DB_DELIVERY_BITSET_AUDIO_STREAM_STOPED		3
//CONST_INT DB_DELIVERY_BITSET_AUDIO_STREAM_STARTED		4
//CONST_INT DB_DELIVERY_BITSET_AUDIO_DOOR_OPENED_SOUND	5

//FLOAT ARRAY
CONST_INT DB_DELIVERY_FLOAT_LIFT_MOVE_RATIO		0

FUNC BOOL CUTSCENE_GANG_OPS_DEFUNCT_BASE_ASSETS_CREATE(DELIVERY_DATA &deliveryData)
	
	SIMPLE_INTERIORS eSimpleInteriorID = GET_SIMPLE_INTERIOR_FROM_FM_GANGOPS_DROPOFF(deliveryData.eDropoffID)
	INT iTintIndex = GET_DEFUNCT_BASE_STYLE_TINT_VALUE(deliveryData.deliveryOwner)
	
	//Create the fake map prop
	IF NOT IS_BIT_SET(deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_BITSET], DB_DELIVERY_BITSET_MAP_IPL_REQUESTED)
		REQUEST_DEFUNCT_BASE_FAKE_MAP_IPL(GET_SIMPLE_INTERIOR_FROM_FM_GANGOPS_DROPOFF(deliveryData.eDropoffID))
		SET_BIT(deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_BITSET], DB_DELIVERY_BITSET_MAP_IPL_REQUESTED)
	ENDIF
	
	REQUEST_MODEL(DEFUNCT_BASE_GET_MODEL_USED_IN_FAKE_MAP_IPL(eSimpleInteriorID))
	IF NOT HAS_MODEL_LOADED(DEFUNCT_BASE_GET_MODEL_USED_IN_FAKE_MAP_IPL(eSimpleInteriorID))
		PRINTLN("CUTSCENE_GANG_OPS_DEFUNCT_BASE_ASSETS_CREATE - ", DEFUNCT_BASE_GET_MODEL_USED_IN_FAKE_MAP_IPL(eSimpleInteriorID), " has not loaded")
		RETURN FALSE
	ENDIF
	
	TRANSFORM_STRUCT sObjInitTransform
	sObjInitTransform = GANG_OPS_DEFUNCT_BASE_GET_TRANSFORM(eSimpleInteriorID, DEFUNCT_BASE_EXT_ASSET_HATCH)
	sObjInitTransform.Position.z -= 30.0
	
	GANG_OPS_DEFUNCT_BASE_PROP_CREATE_ANIMATED_HATCH(deliveryData.cutsceneData.objectArray[DEFUNC_BASE_CS_ENTRY_OBJECT_OPEN_HATCH], sObjInitTransform.Position, sObjInitTransform.Rotation)
	//The light prop is made with the same offsets as the main hatch
	GANG_OPS_DEFUNCT_BASE_PROP_CREATE_LIGHT(deliveryData.cutsceneData.objectArray[DEFUNC_BASE_CS_ENTRY_LIGHTS], sObjInitTransform.Position, sObjInitTransform.Rotation)
	
	IF DOES_ENTITY_EXIST(deliveryData.cutsceneData.objectArray[DEFUNC_BASE_CS_ENTRY_OBJECT_OPEN_HATCH])
		SET_ENTITY_COLLISION(deliveryData.cutsceneData.objectArray[DEFUNC_BASE_CS_ENTRY_OBJECT_OPEN_HATCH], FALSE)
		SET_ENTITY_INVINCIBLE(deliveryData.cutsceneData.objectArray[DEFUNC_BASE_CS_ENTRY_OBJECT_OPEN_HATCH], TRUE)
		FREEZE_ENTITY_POSITION(deliveryData.cutsceneData.objectArray[DEFUNC_BASE_CS_ENTRY_OBJECT_OPEN_HATCH], TRUE)
		SET_OBJECT_TINT_INDEX(deliveryData.cutsceneData.objectArray[DEFUNC_BASE_CS_ENTRY_OBJECT_OPEN_HATCH], iTintIndex)
	ELSE
		RETURN FALSE
	ENDIF
	
	IF DOES_ENTITY_EXIST(deliveryData.cutsceneData.objectArray[DEFUNC_BASE_CS_ENTRY_LIGHTS])
		SET_ENTITY_COLLISION(deliveryData.cutsceneData.objectArray[DEFUNC_BASE_CS_ENTRY_LIGHTS], FALSE)
		SET_ENTITY_INVINCIBLE(deliveryData.cutsceneData.objectArray[DEFUNC_BASE_CS_ENTRY_LIGHTS], TRUE)
		FREEZE_ENTITY_POSITION(deliveryData.cutsceneData.objectArray[DEFUNC_BASE_CS_ENTRY_LIGHTS], TRUE)
	ELSE
		RETURN FALSE
	ENDIF
	
	GANG_OPS_DEFUNCT_BASE_PROP_CREATE_LIFT(deliveryData.cutsceneData.objectArray[DB_DELIVERY_OBJECT_DEFUNCT_BASE_LIFT], <<10.0, 1.0, -10.0>>, <<0.0, 0.0, 0.0>>, DEFAULT, TRUE)
	
	IF DOES_ENTITY_EXIST(deliveryData.cutsceneData.objectArray[DB_DELIVERY_OBJECT_DEFUNCT_BASE_LIFT])
		SET_ENTITY_VISIBLE(deliveryData.cutsceneData.objectArray[DB_DELIVERY_OBJECT_DEFUNCT_BASE_LIFT], FALSE)
		FREEZE_ENTITY_POSITION(deliveryData.cutsceneData.objectArray[DB_DELIVERY_OBJECT_DEFUNCT_BASE_LIFT], TRUE)
		SET_ENTITY_COLLISION(deliveryData.cutsceneData.objectArray[DB_DELIVERY_OBJECT_DEFUNCT_BASE_LIFT], FALSE)
		SET_OBJECT_TINT_INDEX(deliveryData.cutsceneData.objectArray[DB_DELIVERY_OBJECT_DEFUNCT_BASE_LIFT], iTintIndex)
	ELSE
		RETURN FALSE
	ENDIF
	
	//Create a hidden lift prop to assist vehicle entry to the base
	IF deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_DELIVERY_TYPE] = DEFUNCT_BASE_DELIVERY_CUTSCENE_BRING_PERSONAL_VEH_INSIDE
	AND DEFUNCT_BASE_ENTRY_SCENE_SHOULD_VEHICLE_DRIVE_IN(deliveryData.deliveryVehicle)
		sObjInitTransform = GANG_OPS_DEFUNCT_BASE_GET_TRANSFORM(eSimpleInteriorID, DEFUNCT_BASE_EXT_ASSET_HIDDEN_LIFT)
		GANG_OPS_DEFUNCT_BASE_PROP_CREATE_LIFT(deliveryData.cutsceneData.objectArray[DEFUNC_BASE_CS_ENTRY_HIDDEN_LIFT], sObjInitTransform.Position, sObjInitTransform.Rotation, FALSE)
		SET_ENTITY_VISIBLE(deliveryData.cutsceneData.objectArray[DEFUNC_BASE_CS_ENTRY_HIDDEN_LIFT], FALSE)
		SET_ENTITY_COLLISION(deliveryData.cutsceneData.objectArray[DEFUNC_BASE_CS_ENTRY_HIDDEN_LIFT], FALSE)		
	ENDIF
	
	IF deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_DELIVERY_TYPE] != DEFUNCT_BASE_DELIVERY_CUTSCENE_LEAVE_VEHICLE_OUTSIDE
		IF deliveryData.deliveryType = DT_FOOT
			deliveryData.fDoorOpenRatio = 0.4
		ELIF deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_DELIVERY_TYPE] = DEFUNCT_BASE_DELIVERY_CUTSCENE_AVENGER
			deliveryData.fDoorOpenRatio = 0.23
		ELSE
			deliveryData.fDoorOpenRatio = 0.4
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL CUTSCENE_GANG_OPS_DEFUNCT_BASE_CREATE_SCENE(DELIVERY_DATA &deliveryData)
	CDEBUG2LN(DEBUG_DELIVERY, "CUTSCENE_GANG_OPS_DEFUNCT_BASE_CREATE_SCENE - new scene added.")	
	MODEL_NAMES vehModel = DUMMY_MODEL_FOR_SCRIPT
	
	IF deliveryData.deliveryType = DT_VEHICLE
	AND DOES_ENTITY_EXIST(deliveryData.deliveryVehicle)
		vehModel = GET_ENTITY_MODEL(deliveryData.deliveryVehicle)
	ENDIF
	
	BOOL bPVScene = (deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_DELIVERY_TYPE] = DEFUNCT_BASE_DELIVERY_CUTSCENE_BRING_PERSONAL_VEH_INSIDE)
	BOOL bLeaveVehOutside = (deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_DELIVERY_TYPE] = DEFUNCT_BASE_DELIVERY_CUTSCENE_LEAVE_VEHICLE_OUTSIDE)
	
	GANG_OPS_DEFUNCT_BASE_ADD_DELIVERY_ENTRY_SCENES(GET_SIMPLE_INTERIOR_FROM_FM_GANGOPS_DROPOFF(deliveryData.eDropoffID), deliveryData.cutsceneData.cutscene, bLeaveVehOutside, vehModel, 0, bPVScene)
	
	IF bPVScene
		GANG_OPS_DEFUNCT_BASE_ADD_DELIVERY_ENTRY_SCENES(GET_SIMPLE_INTERIOR_FROM_FM_GANGOPS_DROPOFF(deliveryData.eDropoffID), deliveryData.cutsceneData.cutscene, bLeaveVehOutside, vehModel, 1, bPVScene)
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC CUTSCENE_GANG_OPS_DEFUNCT_BASE_POSITION_LOADED_ASSETS(DELIVERY_DATA &deliveryData)

	SIMPLE_INTERIORS eSimpleInteriorID = GET_SIMPLE_INTERIOR_FROM_FM_GANGOPS_DROPOFF(deliveryData.eDropoffID)
	
	TRANSFORM_STRUCT sObjInitTransform
	sObjInitTransform = GANG_OPS_DEFUNCT_BASE_GET_TRANSFORM(eSimpleInteriorID, DEFUNCT_BASE_EXT_ASSET_HATCH)
	
	IF DOES_ENTITY_EXIST(deliveryData.cutsceneData.objectArray[DEFUNC_BASE_CS_ENTRY_HIDDEN_LIFT])
		SET_ENTITY_COLLISION(deliveryData.cutsceneData.objectArray[DEFUNC_BASE_CS_ENTRY_HIDDEN_LIFT], TRUE)
		SET_ENTITY_VISIBLE(deliveryData.cutsceneData.objectArray[DEFUNC_BASE_CS_ENTRY_HIDDEN_LIFT], FALSE)
		FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(deliveryData.cutsceneData.objectArray[DEFUNC_BASE_CS_ENTRY_HIDDEN_LIFT])
	ENDIF
	
	IF DOES_ENTITY_EXIST(deliveryData.cutsceneData.objectArray[DEFUNC_BASE_CS_ENTRY_OBJECT_OPEN_HATCH])
		SET_ENTITY_COORDS_NO_OFFSET(deliveryData.cutsceneData.objectArray[DEFUNC_BASE_CS_ENTRY_OBJECT_OPEN_HATCH], sObjInitTransform.Position, TRUE, DEFAULT, FALSE)
	ENDIF
	
	IF DOES_ENTITY_EXIST(deliveryData.cutsceneData.objectArray[DEFUNC_BASE_CS_ENTRY_LIGHTS])
		SET_ENTITY_COORDS_NO_OFFSET(deliveryData.cutsceneData.objectArray[DEFUNC_BASE_CS_ENTRY_LIGHTS], sObjInitTransform.Position, TRUE, DEFAULT, FALSE)
	ENDIF
	
	IF DOES_ENTITY_EXIST(deliveryData.cutsceneData.objectArray[DB_DELIVERY_OBJECT_DEFUNCT_BASE_LIFT])
		SET_ENTITY_VISIBLE(deliveryData.cutsceneData.objectArray[DB_DELIVERY_OBJECT_DEFUNCT_BASE_LIFT], TRUE)
		SET_ENTITY_COLLISION(deliveryData.cutsceneData.objectArray[DB_DELIVERY_OBJECT_DEFUNCT_BASE_LIFT], TRUE)
	ENDIF
	
	deliveryData.cutsceneData.floatArray[DB_DELIVERY_FLOAT_LIFT_MOVE_RATIO] = 0.0
	GANG_OPS_DEFUNCT_BASE_EXTERIOR_SET_LIFT_RATIO(eSimpleInteriorID, deliveryData.cutsceneData.objectArray[DB_DELIVERY_OBJECT_DEFUNCT_BASE_LIFT], deliveryData.cutsceneData.floatArray[DB_DELIVERY_FLOAT_LIFT_MOVE_RATIO])
			
	//Adjust the start positions as required
	IF deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_DELIVERY_TYPE] = DEFUNCT_BASE_DELIVERY_CUTSCENE_LEAVE_VEHICLE_OUTSIDE
	
		deliveryData.fDoorOpenRatio = 0.3
		deliveryData.cutsceneData.floatArray[DB_DELIVERY_FLOAT_LIFT_MOVE_RATIO] = 0.3
		GANG_OPS_DEFUNCT_BASE_EXTERIOR_SET_LIFT_RATIO(eSimpleInteriorID, deliveryData.cutsceneData.objectArray[DB_DELIVERY_OBJECT_DEFUNCT_BASE_LIFT], deliveryData.cutsceneData.floatArray[DB_DELIVERY_FLOAT_LIFT_MOVE_RATIO], TRUE, 1.0)
		GANG_OPS_DEFUNCT_BASE_EXTERIOR_START_ANIMATED_DOOR_ANIM(deliveryData.cutsceneData.objectArray[DEFUNC_BASE_CS_ENTRY_OBJECT_OPEN_HATCH], TRUE, deliveryData.fDoorOpenRatio, 0.1)
		
	ELIF deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_DELIVERY_TYPE] = DEFUNCT_BASE_DELIVERY_CUTSCENE_BRING_PERSONAL_VEH_INSIDE
		
		//For a big vehicle use a lower lift starting point and the doors shut
		IF NOT DEFUNCT_BASE_ENTRY_SCENE_SHOULD_VEHICLE_DRIVE_IN(deliveryData.cutsceneData.vehiclePackArray[0].vehicle)
			deliveryData.cutsceneData.floatArray[DB_DELIVERY_FLOAT_LIFT_MOVE_RATIO] = 0.3
			deliveryData.fDoorOpenRatio = 0.0
			DO_SCREEN_FADE_OUT(0)
		ELSE
			deliveryData.fDoorOpenRatio = 0.25
			deliveryData.cutsceneData.floatArray[DB_DELIVERY_FLOAT_LIFT_MOVE_RATIO] = 0.45
		ENDIF
		
		GANG_OPS_DEFUNCT_BASE_EXTERIOR_SET_LIFT_RATIO(eSimpleInteriorID, deliveryData.cutsceneData.objectArray[DB_DELIVERY_OBJECT_DEFUNCT_BASE_LIFT], deliveryData.cutsceneData.floatArray[DB_DELIVERY_FLOAT_LIFT_MOVE_RATIO], TRUE, 1.0)
		GANG_OPS_DEFUNCT_BASE_EXTERIOR_START_ANIMATED_DOOR_ANIM(deliveryData.cutsceneData.objectArray[DEFUNC_BASE_CS_ENTRY_OBJECT_OPEN_HATCH], TRUE, deliveryData.fDoorOpenRatio, 0.15)
		
	ELSE
		GANG_OPS_DEFUNCT_BASE_EXTERIOR_START_ANIMATED_DOOR_ANIM(deliveryData.cutsceneData.objectArray[DEFUNC_BASE_CS_ENTRY_OBJECT_OPEN_HATCH], FALSE, deliveryData.fDoorOpenRatio, 0.0)
	ENDIF
	
	FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(deliveryData.cutsceneData.objectArray[DEFUNC_BASE_CS_ENTRY_OBJECT_OPEN_HATCH])
		
ENDPROC

FUNC BOOL CUTSCENE_GANG_OPS_DEFUNCT_BASE_SHOULD_ROTATE_DELIVERY_VEHILCE(SIMPLE_INTERIORS eSimpleInteriorID)
	//If we reverse in then face the opposite direction
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
	AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	AND IS_ENTITY_ALIVE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
		FLOAT 				fplayerEntryVehHeading 	= GET_ENTITY_HEADING(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
		TRANSFORM_STRUCT	HatchPosition			= GANG_OPS_DEFUNCT_BASE_REFERENCE_TRANSFORM(eSimpleInteriorID)
		FLOAT 				fHeadingDiff 			= (fplayerEntryVehHeading - HatchPosition.Rotation.z)
		
		IF fHeadingDiff < 0.0
			fHeadingDiff *= -1
		ENDIF
		 
		IF fHeadingDiff < 190.0
		 	RETURN TRUE
		ELSE
			IF (fplayerEntryVehHeading - 190.0) < 0.0
				fplayerEntryVehHeading = (fplayerEntryVehHeading - 190.0) + 360.0
			ELIF (fplayerEntryVehHeading + 190.0) > 360.0
				fplayerEntryVehHeading = (fplayerEntryVehHeading - 190.0) - 360.0
			ENDIF
			
			fHeadingDiff = (fplayerEntryVehHeading - HatchPosition.Rotation.z)			
			
			IF fHeadingDiff < 0.0
				fHeadingDiff *= -1
			ENDIF
			
			RETURN (fHeadingDiff < 190.0)
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CUTSCENE_GANG_OPS_BASE_SETUP_VEHICLE_ON_LIFT(DELIVERY_DATA &deliveryData, SIMPLE_INTERIORS eSimpleInteriorID)
	
	TRANSFORM_STRUCT assetPosition
	
	IF IS_ENTITY_ALIVE(deliveryData.cutsceneData.vehiclePackArray[0].vehicle)
		assetPosition 	= GANG_OPS_DEFUNCT_BASE_GET_TRANSFORM(eSimpleInteriorID, DEFUNCT_BASE_EXT_ASSET_VEH_POS_2)
		
//		IF CUTSCENE_GANG_OPS_DEFUNCT_BASE_SHOULD_ROTATE_DELIVERY_VEHILCE(eSimpleInteriorID)
//		 	IF assetPosition.Rotation.z > 180.0
//				assetPosition.Rotation.z -= 180.0
//			ELSE
//				assetPosition.Rotation.z += 180.0
//			ENDIF
//		ENDIF

		assetPosition.Position.z += 0.25
		CDEBUG1LN(DEBUG_DELIVERY, "CUTSCENE_GANG_OPS_DEFUNCT_BASE_ASSET_SCENE_SETUP - CUTSCENE_GANG_OPS_BASE_SETUP_VEHICLE_ON_LIFT set vehicle position: ", assetPosition.Position)	
								
		SET_ENTITY_COORDS_NO_OFFSET(deliveryData.cutsceneData.vehiclePackArray[0].vehicle, assetPosition.Position, FALSE, FALSE, FALSE)
		SET_ENTITY_HEADING(deliveryData.cutsceneData.vehiclePackArray[0].vehicle, assetPosition.Rotation.z)
		
		FREEZE_ENTITY_POSITION(deliveryData.cutsceneData.vehiclePackArray[0].vehicle, FALSE)
		SET_ENTITY_COLLISION(deliveryData.cutsceneData.vehiclePackArray[0].vehicle, TRUE)
		SET_ENTITY_VISIBLE(deliveryData.cutsceneData.vehiclePackArray[0].vehicle, TRUE)
		SET_ENTITY_DYNAMIC(deliveryData.cutsceneData.vehiclePackArray[0].vehicle,TRUE)
		SET_DISABLE_MAP_COLLISION(deliveryData.cutsceneData.vehiclePackArray[0].vehicle)
		
		MODEL_NAMES vehModel = GET_ENTITY_MODEL(deliveryData.cutsceneData.vehiclePackArray[0].vehicle)
				
		IF vehModel = AVENGER
		OR vehModel = GET_ARMOURY_AIRCRAFT_COLLAPSED_WING_MODEL()
			//Attach the avenger as it doesn't look good
			FLOAT fHeight = GET_MODEL_HEIGHT(vehModel)
			ATTACH_ENTITY_TO_ENTITY(deliveryData.cutsceneData.vehiclePackArray[0].vehicle, deliveryData.cutsceneData.objectArray[DB_DELIVERY_OBJECT_DEFUNCT_BASE_LIFT], -1, <<0.0, -0.5,(ABSF(fHeight) / 2.0) - 1.73>>, <<0.0,0.0,14.0>>)
		
		ELIF vehModel = THRUSTER
		
			ATTACH_ENTITY_TO_ENTITY(deliveryData.cutsceneData.vehiclePackArray[0].vehicle, deliveryData.cutsceneData.objectArray[DB_DELIVERY_OBJECT_DEFUNCT_BASE_LIFT], -1, <<-0.950, 3.925, 0.850>>, <<0.0, 0.0, 12.900>>)
			
		ELIF vehModel = AKULA
			//Special case for the Akula
			SET_VEHICLE_ENGINE_ON(deliveryData.cutsceneData.vehiclePackArray[0].vehicle, FALSE, TRUE, TRUE)
			ATTACH_ENTITY_TO_ENTITY(deliveryData.cutsceneData.vehiclePackArray[0].vehicle, deliveryData.cutsceneData.objectArray[DB_DELIVERY_OBJECT_DEFUNCT_BASE_LIFT], 0, <<0.0, 1.0, 2.113>>, <<1.113, 0.0, -165.960>>)
			
		ELIF vehModel = CHERNOBOG
		
			ATTACH_ENTITY_TO_ENTITY(deliveryData.cutsceneData.vehiclePackArray[0].vehicle, deliveryData.cutsceneData.objectArray[DB_DELIVERY_OBJECT_DEFUNCT_BASE_LIFT], 0, <<-0.650, 3.275, 0.775>>, <<0.0, 0.0, -164.760>>)
			
		ELIF vehModel = AMBULANCE
		
			ATTACH_ENTITY_TO_ENTITY(deliveryData.cutsceneData.vehiclePackArray[0].vehicle, deliveryData.cutsceneData.objectArray[DB_DELIVERY_OBJECT_DEFUNCT_BASE_LIFT], 0, <<-1.000, 3.100, 0.790>>, <<0.0, 0.0, -165.240>>)
			
		ELIF vehModel = STROMBERG
		
			ATTACH_ENTITY_TO_ENTITY(deliveryData.cutsceneData.vehiclePackArray[0].vehicle, deliveryData.cutsceneData.objectArray[DB_DELIVERY_OBJECT_DEFUNCT_BASE_LIFT], 0, <<-1.150, 4.850, 0.265>>, <<0.0, 0.0, -164.000>>)
			
		ELIF vehModel = DELUXO
		
			ATTACH_ENTITY_TO_ENTITY(deliveryData.cutsceneData.vehiclePackArray[0].vehicle, deliveryData.cutsceneData.objectArray[DB_DELIVERY_OBJECT_DEFUNCT_BASE_LIFT], 0, <<-1.150, 4.250, 0.475>>, <<0.0, 0.0, -164.000>>)
			
		ELIF vehModel = RIOT
		
			ATTACH_ENTITY_TO_ENTITY(deliveryData.cutsceneData.vehiclePackArray[0].vehicle, deliveryData.cutsceneData.objectArray[DB_DELIVERY_OBJECT_DEFUNCT_BASE_LIFT], 0, <<-1.125, 4.0, 0.675>>, <<0.0, 0.0, -164.000>>)
		
		ELIF vehModel = RIOT2
		
			ATTACH_ENTITY_TO_ENTITY(deliveryData.cutsceneData.vehiclePackArray[0].vehicle, deliveryData.cutsceneData.objectArray[DB_DELIVERY_OBJECT_DEFUNCT_BASE_LIFT], 0, <<-1.125, 4.0, 0.875>>, <<0.0, 0.0, -164.000>>)
		
		ELIF GB_GET_GANGOPS_VARIATION_PLAYER_IS_ON(PLAYER_ID()) = GOV_AUTO_SALVAGE
			deliveryData.cutsceneData.objectArray[DEFUNC_BASE_CS_ENTRY_STORMBERG] = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_auto_salvage_stromberg")), GET_ENTITY_COORDS(deliveryData.cutsceneData.vehiclePackArray[0].vehicle, FALSE), FALSE)
			SET_ENTITY_COLLISION(deliveryData.cutsceneData.objectArray[DEFUNC_BASE_CS_ENTRY_STORMBERG], TRUE)
			SET_ENTITY_VISIBLE(deliveryData.cutsceneData.objectArray[DEFUNC_BASE_CS_ENTRY_STORMBERG], TRUE)
			
			IF vehModel = WASTELANDER
			
				ATTACH_ENTITY_TO_ENTITY(deliveryData.cutsceneData.vehiclePackArray[0].vehicle, deliveryData.cutsceneData.objectArray[DB_DELIVERY_OBJECT_DEFUNCT_BASE_LIFT], 0, <<-1.225, 4.950, 0.890>>, <<0.0, 0.0, -164.850>>)
				ATTACH_ENTITY_TO_ENTITY(deliveryData.cutsceneData.objectArray[DEFUNC_BASE_CS_ENTRY_STORMBERG], deliveryData.cutsceneData.vehiclePackArray[0].vehicle, -1, <<0.0, -1.025, 0.890>>, <<0.0, 0.0, 0.0>>)
				
			ELIF vehModel = FLATBED
			
				ATTACH_ENTITY_TO_ENTITY(deliveryData.cutsceneData.vehiclePackArray[0].vehicle, deliveryData.cutsceneData.objectArray[DB_DELIVERY_OBJECT_DEFUNCT_BASE_LIFT], 0, <<-0.575, 3.075, 1.125>>, <<0.0, 0.0, -164.880>>)
				ATTACH_ENTITY_TO_ENTITY(deliveryData.cutsceneData.objectArray[DEFUNC_BASE_CS_ENTRY_STORMBERG], deliveryData.cutsceneData.vehiclePackArray[0].vehicle, -1, <<0.0, -2.675, 0.450>>, <<0.0, 0.0, 0.0>>)
				
			ENDIF
		ELSE
			//SET_VEHICLE_ON_GROUND_PROPERLY(deliveryData.cutsceneData.vehiclePackArray[0].vehicle)
		ENDIF
	ENDIF
	
	INT indexVehiclePack
	INT indexPassenger
	REPEAT CONST_DELIVERY_MAX_VEHICLES_PACK indexVehiclePack
		REPEAT MAX_NO_VEH_PASSENGERS indexPassenger
			IF DOES_ENTITY_EXIST(deliveryData.cutsceneData.vehiclePackArray[indexVehiclePack].passengers[indexPassenger])
			AND NOT IS_ENTITY_DEAD(deliveryData.cutsceneData.vehiclePackArray[indexVehiclePack].passengers[indexPassenger])
				
				SET_DISABLE_PED_MAP_COLLISION(deliveryData.cutsceneData.vehiclePackArray[indexVehiclePack].passengers[indexPassenger])
				FREEZE_ENTITY_POSITION(deliveryData.cutsceneData.vehiclePackArray[indexVehiclePack].passengers[indexPassenger], FALSE)
				CDEBUG1LN(DEBUG_DELIVERY, "CUTSCENE_GANG_OPS_DEFUNCT_BASE_ASSET_SCENE_SETUP - indexVehiclePack: ", indexVehiclePack, " indexPassenger: ", indexPassenger)	
			ENDIF
		ENDREPEAT
	ENDREPEAT
	
	CDEBUG1LN(DEBUG_DELIVERY, "CUTSCENE_GANG_OPS_DEFUNCT_BASE_ASSET_SCENE_SETUP - SET_DISABLE_MAP_COLLISION on vehicle 0")	
ENDPROC

/// PURPOSE:
///    Use this to grab a coord spcific to each base as the ground it 
///    too uneven around each base to allow an offset transform
/// PARAMS:
///    eSimpleInterior - The base to get a coord for
FUNC VECTOR CUTSCENE_GANG_OPS_BASE_GET_OUTSIDE_VEH_COORDS(SIMPLE_INTERIORS eSimpleInterior)
	SWITCH eSimpleInterior
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_1	RETURN 	<<1265.5416, 2828.8218, 48.1990>>
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_2 RETURN 	<<40.9421, 2625.3062, 83.9271>>
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_3	RETURN 	<<2751.8875, 3903.4551, 44.2191>>
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_4	RETURN 	<<3383.1821, 5510.5356, 23.8583>>
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_6	RETURN 	<<27.5968, 6822.7324, 14.9623>>
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_7	RETURN 	<<-2229.1023, 2392.6274, 12.86857>>
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_8	RETURN 	<<-0.9906, 3352.8455, 40.3208>>
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_9	RETURN	<<2091.6182, 1767.2856, 102.7415>>
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_10 RETURN	<<1861.6932, 263.5905, 163.1817>>
	ENDSWITCH
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

/// PURPOSE:
///    Use this to grab a heading spcific to each base as the ground is 
///    too uneven around each base to allow an good offset transform
/// PARAMS:
///    eSimpleInterior - The base to get a coord for
FUNC FLOAT CUTSCENE_GANG_OPS_BASE_GET_OUTSIDE_VEH_HEADING(SIMPLE_INTERIORS eSimpleInterior)
	SWITCH eSimpleInterior
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_1	RETURN 	309.1471
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_2 RETURN 	122.0095
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_3	RETURN 	312.9328
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_4	RETURN 	254.9535 
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_6	RETURN 	70.0241
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_7	RETURN 	1.5689
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_8	RETURN 	167.3613
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_9	RETURN	135.9090
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_10 RETURN	327.9435
	ENDSWITCH
	
	RETURN 0.0
ENDFUNC

PROC CUTSCENE_GANG_OPS_BASE_SETUP_VEHICLE_LEFT_OUTSIDE(DELIVERY_DATA &deliveryData, VECTOR vCoord, FLOAT vHeading)
	VEHICLE_INDEX veh = deliveryData.cutsceneData.vehiclePackArray[0].vehicle
	
	IF IS_ENTITY_ALIVE(veh)
		vCoord.z += 1.0
		SET_ENTITY_COORDS_NO_OFFSET(veh, vCoord)
		SET_ENTITY_HEADING(veh, vHeading)
		SET_ENTITY_INVINCIBLE(veh, TRUE)
		SET_ENTITY_CAN_BE_DAMAGED(veh, FALSE)
		SET_VEHICLE_ON_GROUND_PROPERLY(veh)
		
		//If we're in a helicopter stop the blades spinning
		IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(veh))
			SET_VEHICLE_ENGINE_ON(veh, FALSE, TRUE, TRUE)
			SET_HELI_BLADES_SPEED(veh, 0)
		ENDIF
	ENDIF
	
	CDEBUG1LN(DEBUG_DELIVERY, "CUTSCENE_GANG_OPS_DEFUNCT_BASE_ASSET_SCENE_SETUP - SET_DISABLE_MAP_COLLISION on vehicle 0")	
ENDPROC

//Will call once to position assets on the start of an x scene
PROC CUTSCENE_GANG_OPS_DEFUNCT_BASE_ASSET_SCENE_SETUP(DELIVERY_DATA &deliveryData, INT iScene)
	CDEBUG2LN(DEBUG_DELIVERY, "CUTSCENE_GANG_OPS_DEFUNCT_BASE_ASSET_SCENE_SETUP")
	FLOAT 				fVehHeading
	STRING 				sAnimDict	
	VECTOR 				vVehPosition	
	TRANSFORM_STRUCT 	sObjInitTransform
	SIMPLE_INTERIORS 	eSimpleInteriorID = GET_SIMPLE_INTERIOR_FROM_FM_GANGOPS_DROPOFF(deliveryData.eDropoffID)
	
	SWITCH iScene
		CASE 0					
			GANG_OPS_DEFUNCT_BASE_TOGGLE_IPL_VISIBILITY(GET_SIMPLE_INTERIOR_FROM_FM_GANGOPS_DROPOFF(deliveryData.eDropoffID), FALSE)
						
			CUTSCENE_GANG_OPS_DEFUNCT_BASE_POSITION_LOADED_ASSETS(deliveryData)
			
			THEFEED_HIDE()
			THEFEED_PAUSE()
			SET_BIT(g_SimpleInteriorData.iThirdBS, BS3_SIMPLE_INTERIOR_GLOBAL_DATA_RESUME_FEED_ON_IDLE)
				
			SWITCH deliveryData.deliveryType
				CASE DT_VEHICLE
					CUTSCENE_HELP_PREPARE_VEHICLE_FOR_SCENE(deliveryData.cutsceneData.vehiclePackArray[0].vehicle, TRUE)
					
					IF IS_ENTITY_ALIVE(deliveryData.deliveryVehicle)
					AND GET_ENTITY_MODEL(deliveryData.deliveryVehicle) = AMBULANCE
					AND NETWORK_HAS_CONTROL_OF_ENTITY(deliveryData.deliveryVehicle)
						SET_VEHICLE_SIREN(deliveryData.deliveryVehicle, FALSE)
					ENDIF
					
					EXT_CUTSCENE_DEFUNCT_BASE_CONCEAL_ENTRY_VEHICLE(FALSE, TRUE)
					
					IF deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_DELIVERY_TYPE] = DEFUNCT_BASE_DELIVERY_CUTSCENE_BRING_VEHICLE_INSIDE
					OR deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_DELIVERY_TYPE] = DEFUNCT_BASE_DELIVERY_CUTSCENE_AVENGER
					
						CUTSCENE_GANG_OPS_BASE_SETUP_VEHICLE_ON_LIFT(deliveryData, eSimpleInteriorID)
						
					ELIF deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_DELIVERY_TYPE] = DEFUNCT_BASE_DELIVERY_CUTSCENE_BRING_PERSONAL_VEH_INSIDE
					
						sObjInitTransform = GANG_OPS_DEFUNCT_BASE_GET_TRANSFORM(eSimpleInteriorID, DEFUNCT_BASE_EXT_ASSET_VEH_POS_1)
						
						IF NOT DEFUNCT_BASE_ENTRY_SCENE_SHOULD_VEHICLE_DRIVE_IN(deliveryData.cutsceneData.vehiclePackArray[0].vehicle)
						AND eSimpleInteriorID = SIMPLE_INTERIOR_DEFUNCT_BASE_4
							sObjInitTransform.Position = <<3383.6575, 5510.2881, 24.0167>>
							sObjInitTransform.Rotation.z = 256.6348
						ENDIF
						
						DEFUNCT_BASE_TRIGGER_START_DOOR_OPEN_SOUND(deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_SOUND_ID])
						SET_BIT(deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_BITSET], DB_DELIVERY_BITSET_TASK_PLAYED_RELEASE_SOUND_ON_CLEANUP)
						CUTSCENE_GANG_OPS_BASE_SETUP_VEHICLE_LEFT_OUTSIDE(deliveryData, sObjInitTransform.Position, sObjInitTransform.Rotation.z)
						
					ELSE
						vVehPosition 	= CUTSCENE_GANG_OPS_BASE_GET_OUTSIDE_VEH_COORDS(eSimpleInteriorID)
						fVehHeading 	= CUTSCENE_GANG_OPS_BASE_GET_OUTSIDE_VEH_HEADING(eSimpleInteriorID)
						CUTSCENE_GANG_OPS_BASE_SETUP_VEHICLE_LEFT_OUTSIDE(deliveryData, vVehPosition, fVehHeading)
						SET_BIT(deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_BITSET], DB_DELIVERY_BITSET_TASK_PLAYED_RELEASE_SOUND_ON_CLEANUP)
						DEFUNCT_BASE_TRIGGER_START_DOOR_OPEN_SOUND(deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_SOUND_ID])
					ENDIF
					
					ADD_ENTITY_TO_AUDIO_MIX_GROUP(deliveryData.cutsceneData.vehiclePackArray[0].vehicle, "dlc_xm_facility_enter/exit_fake_heli_group")
					START_AUDIO_SCENE("dlc_xm_facility_enter_in_vehicle_exterior_scene")
					
				BREAK
					
				CASE DT_FOOT
					IF DOES_ENTITY_EXIST(deliveryData.cutsceneData.pedArray[0])
					AND NOT IS_ENTITY_DEAD(deliveryData.cutsceneData.pedArray[0])
						
						FORCE_PED_AI_AND_ANIMATION_UPDATE(deliveryData.cutsceneData.pedArray[0])
						SET_DISABLE_PED_MAP_COLLISION(deliveryData.cutsceneData.pedArray[0])
						FREEZE_ENTITY_POSITION(deliveryData.cutsceneData.pedArray[0], FALSE)
						SET_ENTITY_DYNAMIC(deliveryData.cutsceneData.pedArray[0],TRUE)
						
						IF IS_PLAYER_FEMALE()
							sAnimDict = "anim@amb@facility@hangerdoors@base@enter_exit@female@"
						ELSE
							sAnimDict = "anim@amb@facility@hangerdoors@base@enter_exit@male@"
						ENDIF
						
						VECTOR vCoords 
						sObjInitTransform = GANG_OPS_DEFUNCT_BASE_GET_TRANSFORM(eSimpleInteriorID, DEFUNCT_BASE_EXT_ASSET_PED_POS_SYNC_SCENE)
						
						vCoords = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(deliveryData.cutsceneData.objectArray[DEFUNC_BASE_CS_ENTRY_OBJECT_LIFT], sObjInitTransform.Position)
						deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_SCENE_ID]	= CREATE_SYNCHRONIZED_SCENE(vCoords, <<0.0, 0.0, -18.720>>)
						
						ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_SCENE_ID], deliveryData.cutsceneData.objectArray[DEFUNC_BASE_CS_ENTRY_OBJECT_LIFT], -1)
						TASK_SYNCHRONIZED_SCENE(deliveryData.cutsceneData.pedArray[0], deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_SCENE_ID], sAnimDict, "control_operation", INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT)						
						START_AUDIO_SCENE("dlc_xm_facility_enter_on_foot_exterior_scene")
					ENDIF
				BREAK
			ENDSWITCH
						
			IF NOT IS_BIT_SET(deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_BITSET], DB_DELIVERY_BITSET_CUTSCENE_STARTED)
				SET_BIT(deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_BITSET], DB_DELIVERY_BITSET_CUTSCENE_STARTED)
				CDEBUG1LN(DEBUG_DELIVERY, "DB_DELIVERY_BITSET_CUTSCENE_STARTED - TRUE")	
			ENDIF
		BREAK
		CASE 1
			SWITCH deliveryData.deliveryType
				CASE DT_VEHICLE
					IF deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_DELIVERY_TYPE] = DEFUNCT_BASE_DELIVERY_CUTSCENE_BRING_PERSONAL_VEH_INSIDE
						//Reset the variables controling how far we've gone through the door closing and lift loweing movement
						deliveryData.cutsceneData.floatArray[DB_DELIVERY_FLOAT_LIFT_MOVE_RATIO] = 0
						deliveryData.fDoorOpenRatio = 0.46
						
						GANG_OPS_DEFUNCT_BASE_EXTERIOR_START_ANIMATED_DOOR_ANIM(deliveryData.cutsceneData.objectArray[DEFUNC_BASE_CS_ENTRY_OBJECT_OPEN_HATCH], FALSE, deliveryData.fDoorOpenRatio, 0.0)
						FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(deliveryData.cutsceneData.objectArray[DEFUNC_BASE_CS_ENTRY_OBJECT_OPEN_HATCH])
						
						//Update the lift position
						GANG_OPS_DEFUNCT_BASE_EXTERIOR_SET_LIFT_RATIO(eSimpleInteriorID, deliveryData.cutsceneData.objectArray[DB_DELIVERY_OBJECT_DEFUNCT_BASE_LIFT], deliveryData.cutsceneData.floatArray[DB_DELIVERY_FLOAT_LIFT_MOVE_RATIO], FALSE, 8.0)
						
						IF IS_ENTITY_ALIVE(deliveryData.cutsceneData.vehiclePackArray[0].vehicle)
							
							PED_INDEX pedDriver
							pedDriver = GET_PED_IN_VEHICLE_SEAT(deliveryData.cutsceneData.vehiclePackArray[0].vehicle)
							
							IF IS_ENTITY_ALIVE(pedDriver)			
							
								SET_ENTITY_DYNAMIC(deliveryData.cutsceneData.vehiclePackArray[0].vehicle,TRUE)
								CLEAR_PED_TASKS(pedDriver)
								CLEAR_PRIMARY_VEHICLE_TASK(deliveryData.cutsceneData.vehiclePackArray[0].vehicle)
								BRING_VEHICLE_TO_HALT(deliveryData.cutsceneData.vehiclePackArray[0].vehicle,0,8000)
								
								IF NOT DEFUNCT_BASE_ENTRY_SCENE_SHOULD_VEHICLE_DRIVE_IN(deliveryData.cutsceneData.vehiclePackArray[0].vehicle)
									sObjInitTransform 	= GANG_OPS_DEFUNCT_BASE_GET_TRANSFORM(eSimpleInteriorID, DEFUNCT_BASE_EXT_ASSET_BIG_VEH_EXT_SHOT_2)
								ELSE
									sObjInitTransform 	= GANG_OPS_DEFUNCT_BASE_GET_TRANSFORM(eSimpleInteriorID, DEFUNCT_BASE_EXT_ASSET_VEH_POS_2)
								ENDIF
								
								VECTOR vCurrentPos, vNewCoord
								
								vCurrentPos = GET_ENTITY_COORDS(deliveryData.cutsceneData.vehiclePackArray[0].vehicle)
								vNewCoord 	= GET_GROUND_COORD_ADJUSTED_FOR_MODEL_DIMENSIONS(GET_ENTITY_MODEL(deliveryData.cutsceneData.vehiclePackArray[0].vehicle),sObjInitTransform.Position)
								
								IF DEFUNCT_BASE_ENTRY_SCENE_SHOULD_VEHICLE_DRIVE_IN(deliveryData.cutsceneData.vehiclePackArray[0].vehicle)
									vNewCoord.z = vCurrentPos.z
								ENDIF
								
								SET_ENTITY_COORDS_NO_OFFSET(deliveryData.cutsceneData.vehiclePackArray[0].vehicle, vNewCoord)
								SET_ENTITY_HEADING(deliveryData.cutsceneData.vehiclePackArray[0].vehicle, sObjInitTransform.Rotation.z)							
								FREEZE_ENTITY_POSITION(deliveryData.cutsceneData.vehiclePackArray[0].vehicle, FALSE)
								SET_DISABLE_MAP_COLLISION(deliveryData.cutsceneData.vehiclePackArray[0].vehicle)
								SET_VEHICLE_ENGINE_ON(deliveryData.cutsceneData.vehiclePackArray[0].vehicle,FALSE,TRUE)
								
								IF NOT DEFUNCT_BASE_ENTRY_SCENE_SHOULD_VEHICLE_DRIVE_IN(deliveryData.cutsceneData.vehiclePackArray[0].vehicle)
									SET_VEHICLE_ON_GROUND_PROPERLY(deliveryData.cutsceneData.vehiclePackArray[0].vehicle)
								ENDIF
							ENDIF
						ENDIF
						
						IF DOES_ENTITY_EXIST(deliveryData.cutsceneData.objectArray[DEFUNC_BASE_CS_ENTRY_HIDDEN_LIFT])
							DELETE_OBJECT(deliveryData.cutsceneData.objectArray[DEFUNC_BASE_CS_ENTRY_HIDDEN_LIFT])
						ENDIF
						
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	GANG_OPS_BASE_ENTRY_SCENE_SETUP_MAP_AREA(eSimpleInteriorID)
ENDPROC

CONST_INT ciDeliveryVehExitStagger 	400
CONST_INT ciExitVehStartTime 		750
CONST_INT ciVehMovementStartTime	1000

PROC CUTSCENE_GANG_OPS_BASE_TASK_PEDS_TO_LEAVE_VEHICLE(DELIVERY_DATA &deliveryData)
	PED_INDEX ped
	INT indexVehiclePack
	INT indexPassenger
	SEQUENCE_INDEX mSequence
	
	GANG_OPS_DEFUNCT_BASE_EXT_ASSETS eAssetPosition = DEFUNCT_BASE_EXT_ASSET_PED_POS_2
	
	REPEAT CONST_DELIVERY_MAX_VEHICLES_PACK indexVehiclePack
		REPEAT MAX_NO_VEH_PASSENGERS indexPassenger
			ped = deliveryData.cutsceneData.vehiclePackArray[indexVehiclePack].passengers[indexPassenger]
			INT iExitTime = (ciDeliveryVehExitStagger * indexPassenger)
			iExitTime += ciExitVehStartTime
			
			IF IS_ENTITY_ALIVE(ped)
				IF deliveryData.cutsceneData.cutscene.iTotalElapsedTime > iExitTime
					IF GET_SCRIPT_TASK_STATUS(ped, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK			
						mSequence = NULL
						OPEN_SEQUENCE_TASK(mSequence)
							TASK_LEAVE_ANY_VEHICLE(NULL)
							
							TRANSFORM_STRUCT sFootTransform
							sFootTransform = GANG_OPS_DEFUNCT_BASE_GET_TRANSFORM(GET_SIMPLE_INTERIOR_FROM_FM_GANGOPS_DROPOFF(deliveryData.eDropoffID), eAssetPosition)
							TASK_GO_STRAIGHT_TO_COORD(NULL, sFootTransform.Position, PEDMOVEBLENDRATIO_RUN, DEFAULT_TIME_NEVER_WARP)
						CLOSE_SEQUENCE_TASK(mSequence)
						TASK_PERFORM_SEQUENCE(ped, mSequence)
						CLEAR_SEQUENCE_TASK(mSequence)
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDREPEAT
ENDPROC

PROC CUTSCENE_GANG_OPS_DEFUNCT_BASE_SETUP_VEHICLE_TASK(DELIVERY_DATA &deliveryData, SIMPLE_INTERIORS eSimpleInteriorID)
	
	TRANSFORM_STRUCT sObjInitTransform
	VEHICLE_INDEX veh = deliveryData.cutsceneData.vehiclePackArray[0].vehicle
	
	IF NOT IS_ENTITY_ALIVE(veh)
		SCRIPT_ASSERT("CUTSCENE_GANG_OPS_DEFUNCT_BASE_SETUP_VEHICLE_TASK - Vehicle is dead!")
		EXIT
	ENDIF
		
	FLOAT fSpeed = 3.0
	
	IF IS_VEHICLE_A_SUPERMOD_MODEL(GET_ENTITY_MODEL(veh), SUPERMOD_FLAG_HAS_HYDRAULICS)
	AND GET_ENTITY_MODEL(veh) != FACTION3
		DEFUNCT_BASE_ENTRY_SCENE_SETUP_VEH_HYDRAULICS(veh)
	ENDIF
	
	PED_INDEX pedDriver = GET_PED_IN_VEHICLE_SEAT(veh)
	
	IF IS_ENTITY_ALIVE(pedDriver)
		SET_ENTITY_DYNAMIC(veh,TRUE)
		
		//Get the drive to coord
		sObjInitTransform = GANG_OPS_DEFUNCT_BASE_GET_TRANSFORM(eSimpleInteriorID, DEFUNCT_BASE_EXT_ASSET_VEH_POS_2)
		
		TASK_VEHICLE_DRIVE_TO_COORD(pedDriver, veh, sObjInitTransform.Position, fSpeed, DRIVINGSTYLE_STRAIGHTLINE, GET_ENTITY_MODEL(veh), DRIVINGMODE_PLOUGHTHROUGH, 1.0, 100.0)
									
		CDEBUG2LN(DEBUG_DELIVERY, "CUTSCENE_GANG_OPS_DEFUNCT_BASE_SETUP_VEHICLE_TASK - task vehicle to drive inside.")
	ELSE
		SCRIPT_ASSERT("CUTSCENE_GANG_OPS_DEFUNCT_BASE_SETUP_VEHICLE_TASK - Vehicle driver is dead!")
	ENDIF
	
	SET_BIT(deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_BITSET], DB_DELIVERY_BITSET_TASK_MOVE_VEH_FORWARD)
ENDPROC

PROC CUTSCENE_GANG_OPS_DEFUNCT_BASE_TRIGGER_DOOR_CLOSE_SOUND(DELIVERY_DATA &deliveryData, BOOL bStopOpenAudio = FALSE)
	IF NOT IS_BIT_SET(deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_BITSET], DB_DELIVERY_BITSET_TASK_PLAYED_DOOR_CLOSING_SOUND)
		
		IF bStopOpenAudio
			STOP_SOUND(deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_SOUND_ID])
			RELEASE_SOUND_ID(deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_SOUND_ID])
		ENDIF
		
		PLAY_SOUND_FRONTEND(-1, "hangar_doors_limit", "dlc_xm_facility_entry_exit_sounds")
		deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_SOUND_ID] = GET_SOUND_ID()
		PLAY_SOUND_FRONTEND(deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_SOUND_ID], "hangar_doors_loop", "dlc_xm_facility_entry_exit_sounds")
		
		SET_BIT(deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_BITSET], DB_DELIVERY_BITSET_TASK_PLAYED_DOOR_CLOSING_SOUND)
		SET_BIT(deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_BITSET], DB_DELIVERY_BITSET_TASK_PLAYED_RELEASE_SOUND_ON_CLEANUP)
	ENDIF
ENDPROC

PROC CUTSCENE_GANG_OPS_DEFUNCT_BASE_UPDATE_ON_FOOT_ELEMENTS(DELIVERY_DATA &deliveryData, SIMPLE_INTERIORS eSimpleInteriorID)
	BOOL bAnimateProps = FALSE
			
	
	IF IS_ENTITY_ALIVE(deliveryData.cutsceneData.pedArray[0])
	AND IS_SYNCHRONIZED_SCENE_RUNNING(deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_SCENE_ID])
	AND GET_SYNCHRONIZED_SCENE_PHASE(deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_SCENE_ID]) >= 0.147
		bAnimateProps = TRUE
	ENDIF
	
	IF bAnimateProps
	
		FLOAT fLowerSpeed = 0.09
		
		IF eSimpleInteriorID = SIMPLE_INTERIOR_DEFUNCT_BASE_10
			fLowerSpeed = 0.045
		ENDIF				
		
		//Move the lift down
		deliveryData.cutsceneData.floatArray[DB_DELIVERY_FLOAT_LIFT_MOVE_RATIO] += GET_FRAME_TIME() * fLowerSpeed
		GANG_OPS_DEFUNCT_BASE_EXTERIOR_SET_LIFT_RATIO(eSimpleInteriorID, deliveryData.cutsceneData.objectArray[DB_DELIVERY_OBJECT_DEFUNCT_BASE_LIFT], deliveryData.cutsceneData.floatArray[DB_DELIVERY_FLOAT_LIFT_MOVE_RATIO])
						
		IF NOT IS_SCREEN_FADED_OUT()
			TOGGLE_DELIVERY_FLAG_EXTRA(deliveryData, DELIVERY_FLAG_EXTRA_SHOW_SHARD, TRUE)
		ENDIF
		
		CUTSCENE_GANG_OPS_DEFUNCT_BASE_TRIGGER_DOOR_CLOSE_SOUND(deliveryData)
		GANG_OPS_DEFUNCT_BASE_EXTERIOR_ANIMATED_DOOR_SET_SPEED(deliveryData.cutsceneData.objectArray[DEFUNC_BASE_CS_ENTRY_OBJECT_OPEN_HATCH], FALSE, 0.12)
	ENDIF
ENDPROC

PROC CUTSCENE_GANG_OPS_DEFUNCT_BASE_UPDATE_VEH_ON_RAMP_ELEMENTS(DELIVERY_DATA &deliveryData, SIMPLE_INTERIORS eSimpleInteriorID)
	FLOAT fDoorCloseSpeed = 0.15
	FLOAT fLiftLowerSpeed = 0.09
	
	//Set the animation speed for the avenger specific scene
	IF deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_DELIVERY_TYPE] = DEFUNCT_BASE_DELIVERY_CUTSCENE_AVENGER
		fDoorCloseSpeed = 0.06
		fLiftLowerSpeed = 0.045
	ENDIF
	
	//Only start the movement after 1.5 seconds
	IF deliveryData.cutsceneData.cutscene.iCurrentSceneElapsedTime >= 1500
		
		IF NOT IS_SCREEN_FADED_OUT()
			TOGGLE_DELIVERY_FLAG_EXTRA(deliveryData, DELIVERY_FLAG_EXTRA_SHOW_SHARD, TRUE)
		ENDIF
		
		//Move the lift down
		deliveryData.cutsceneData.floatArray[DB_DELIVERY_FLOAT_LIFT_MOVE_RATIO] += GET_FRAME_TIME() * fLiftLowerSpeed
		GANG_OPS_DEFUNCT_BASE_EXTERIOR_SET_LIFT_RATIO(eSimpleInteriorID, deliveryData.cutsceneData.objectArray[DB_DELIVERY_OBJECT_DEFUNCT_BASE_LIFT], deliveryData.cutsceneData.floatArray[DB_DELIVERY_FLOAT_LIFT_MOVE_RATIO])
		
		IF IS_ENTITY_ALIVE(deliveryData.cutsceneData.vehiclePackArray[0].vehicle)
		AND GET_ENTITY_MODEL(deliveryData.cutsceneData.vehiclePackArray[0].vehicle) = AKULA
			fDoorCloseSpeed = 0.09
		ENDIF
		
		CUTSCENE_GANG_OPS_DEFUNCT_BASE_TRIGGER_DOOR_CLOSE_SOUND(deliveryData)
		
		//Set the speed of the door close anim
		GANG_OPS_DEFUNCT_BASE_EXTERIOR_ANIMATED_DOOR_SET_SPEED(deliveryData.cutsceneData.objectArray[DEFUNC_BASE_CS_ENTRY_OBJECT_OPEN_HATCH], FALSE, fDoorCloseSpeed)
	ENDIF
ENDPROC

PROC CUTSCENE_GANG_OPS_DEFUNCT_BASE_UPDATE_VEH_OUTSIDE_ELEMENTS(DELIVERY_DATA &deliveryData, SIMPLE_INTERIORS eSimpleInteriorID)
	IF deliveryData.cutsceneData.cutscene.iCurrentSceneElapsedTime >= 1500
	AND NOT IS_SCREEN_FADED_OUT()
		TOGGLE_DELIVERY_FLAG_EXTRA(deliveryData, DELIVERY_FLAG_EXTRA_SHOW_SHARD, TRUE)
	ENDIF
	
	//Move the lift up
	deliveryData.cutsceneData.floatArray[DB_DELIVERY_FLOAT_LIFT_MOVE_RATIO] += GET_FRAME_TIME() * 0.1
	GANG_OPS_DEFUNCT_BASE_EXTERIOR_SET_LIFT_RATIO(eSimpleInteriorID, deliveryData.cutsceneData.objectArray[DB_DELIVERY_OBJECT_DEFUNCT_BASE_LIFT], deliveryData.cutsceneData.floatArray[DB_DELIVERY_FLOAT_LIFT_MOVE_RATIO], TRUE, 1.0)
ENDPROC

PROC CUTSCENE_GANG_OPS_DEFUNCT_BASE_UPDATE_PV_ELEMENTS(DELIVERY_DATA &deliveryData, SIMPLE_INTERIORS eSimpleInteriorID, INT iScene)
	IF iScene = 0
		
		//Move the lift up
		IF NOT IS_BIT_SET(deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_BITSET], DB_DELIVERY_BITSET_TASK_FADED_SCREEN_IN)
		AND NOT DEFUNCT_BASE_ENTRY_SCENE_SHOULD_VEHICLE_DRIVE_IN(deliveryData.cutsceneData.vehiclePackArray[0].vehicle)
			DO_SCREEN_FADE_IN(1000)
			SET_BIT(deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_BITSET], DB_DELIVERY_BITSET_TASK_FADED_SCREEN_IN)
		ENDIF
		
		IF deliveryData.cutsceneData.floatArray[DB_DELIVERY_FLOAT_LIFT_MOVE_RATIO] < 0.7
		OR DEFUNCT_BASE_ENTRY_SCENE_SHOULD_VEHICLE_DRIVE_IN(deliveryData.cutsceneData.vehiclePackArray[0].vehicle)
			deliveryData.cutsceneData.floatArray[DB_DELIVERY_FLOAT_LIFT_MOVE_RATIO] += GET_FRAME_TIME() * 0.1
			deliveryData.cutsceneData.floatArray[DB_DELIVERY_FLOAT_LIFT_MOVE_RATIO] = CLAMP(deliveryData.cutsceneData.floatArray[DB_DELIVERY_FLOAT_LIFT_MOVE_RATIO], 0.0, 1.0)
			GANG_OPS_DEFUNCT_BASE_EXTERIOR_SET_LIFT_RATIO(eSimpleInteriorID, deliveryData.cutsceneData.objectArray[DB_DELIVERY_OBJECT_DEFUNCT_BASE_LIFT], deliveryData.cutsceneData.floatArray[DB_DELIVERY_FLOAT_LIFT_MOVE_RATIO], TRUE, 1.0)
		ENDIF
	
	ELIF SIMPLE_CUTSCENE_IS_SCENE_RUNNING(deliveryData.cutsceneData.cutscene, 1)		
		FLOAT fLowerSpeed = 0.10
		FLOAT fCloseSpeed = 0.12
				
		IF eSimpleInteriorID = SIMPLE_INTERIOR_DEFUNCT_BASE_10
			fLowerSpeed = 0.04
		ENDIF
		
		CUTSCENE_GANG_OPS_DEFUNCT_BASE_TRIGGER_DOOR_CLOSE_SOUND(deliveryData, TRUE)
				
		//move lift down
		deliveryData.cutsceneData.floatArray[DB_DELIVERY_FLOAT_LIFT_MOVE_RATIO] += GET_FRAME_TIME() * fLowerSpeed
		deliveryData.cutsceneData.floatArray[DB_DELIVERY_FLOAT_LIFT_MOVE_RATIO] = CLAMP(deliveryData.cutsceneData.floatArray[DB_DELIVERY_FLOAT_LIFT_MOVE_RATIO], 0.0, 1.0)
		GANG_OPS_DEFUNCT_BASE_EXTERIOR_SET_LIFT_RATIO(eSimpleInteriorID, deliveryData.cutsceneData.objectArray[DB_DELIVERY_OBJECT_DEFUNCT_BASE_LIFT], deliveryData.cutsceneData.floatArray[DB_DELIVERY_FLOAT_LIFT_MOVE_RATIO], FALSE, 8.0)
		
		//Start to close the doors
		GANG_OPS_DEFUNCT_BASE_EXTERIOR_ANIMATED_DOOR_SET_SPEED(deliveryData.cutsceneData.objectArray[DEFUNC_BASE_CS_ENTRY_OBJECT_OPEN_HATCH], FALSE, fCloseSpeed)
		
		IF NOT IS_SCREEN_FADED_OUT()
			TOGGLE_DELIVERY_FLAG_EXTRA(deliveryData, DELIVERY_FLAG_EXTRA_SHOW_SHARD, TRUE)
		ENDIF
	ENDIF
ENDPROC

PROC CUTSCENE_GANG_OPS_DEFUNCT_BASE_MAINTAIN_SCENE(DELIVERY_DATA &deliveryData)			
	
	SIMPLE_INTERIORS eSimpleInteriorID = GET_SIMPLE_INTERIOR_FROM_FM_GANGOPS_DROPOFF(deliveryData.eDropoffID)
	
	IF deliveryData.cutsceneData.cutscene.bPlaying
		IF deliveryData.deliveryType = DT_FOOT
		
			CUTSCENE_GANG_OPS_DEFUNCT_BASE_UPDATE_ON_FOOT_ELEMENTS(deliveryData, eSimpleInteriorID)
			CULL_MODELS_FOR_CUTSCENE(eSimpleInteriorID)
			
		ELIF deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_DELIVERY_TYPE] = DEFUNCT_BASE_DELIVERY_CUTSCENE_BRING_VEHICLE_INSIDE
		OR deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_DELIVERY_TYPE] = DEFUNCT_BASE_DELIVERY_CUTSCENE_AVENGER
		
			CUTSCENE_GANG_OPS_DEFUNCT_BASE_UPDATE_VEH_ON_RAMP_ELEMENTS(deliveryData, eSimpleInteriorID)
			CULL_MODELS_FOR_CUTSCENE(eSimpleInteriorID)
			
		ELIF deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_DELIVERY_TYPE] = DEFUNCT_BASE_DELIVERY_CUTSCENE_LEAVE_VEHICLE_OUTSIDE
			
			CUTSCENE_GANG_OPS_DEFUNCT_BASE_UPDATE_VEH_OUTSIDE_ELEMENTS(deliveryData, eSimpleInteriorID)
		
		ELIF deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_DELIVERY_TYPE] = DEFUNCT_BASE_DELIVERY_CUTSCENE_BRING_PERSONAL_VEH_INSIDE
			
			CUTSCENE_GANG_OPS_DEFUNCT_BASE_UPDATE_PV_ELEMENTS(deliveryData, eSimpleInteriorID, deliveryData.cutsceneData.cutscene.iCurrentScene)
			
			IF deliveryData.cutsceneData.cutscene.iCurrentScene > 0
				CULL_MODELS_FOR_CUTSCENE(eSimpleInteriorID)
			ENDIF
			
		ENDIF
		
		IF NOT IS_BIT_SET(deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_BITSET], DB_DELIVERY_BITSET_CUTSCENE_RELEASED_ASSETS)
			BOOL bCleanupAvenger = (deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_DELIVERY_TYPE] = DEFUNCT_BASE_DELIVERY_CUTSCENE_AVENGER)
			
			//Release the default assets
			EXT_CUTSCENE_DEFUNCT_BASE_RELEASE_REQUESTED_ASSETS(eSimpleInteriorID, bCleanupAvenger)
			//Release the stormberg asset
			SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_auto_salvage_stromberg")))
			
			SET_BIT(deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_BITSET], DB_DELIVERY_BITSET_CUTSCENE_RELEASED_ASSETS)
		ENDIF

		IF NOT IS_BIT_SET(deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_BITSET], DB_DELIVERY_BITSET_STARTED_LOAD_SCENE)
			VECTOR vSpawn
			FLOAT fHeading
			GET_DEFUNCT_BASE_SPAWN_POINT(eSimpleInteriorID, PARTICIPANT_ID_TO_INT(), vSpawn, fHeading, FALSE)
			
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
			AND NOT IS_VECTOR_ZERO(vSpawn)
				NEW_LOAD_SCENE_START_SPHERE(vSpawn, 20.0)
				REQUEST_SCRIPT("am_mp_smpl_interior_int")
				REQUEST_SCRIPT("am_mp_defunct_base")
			ENDIF
			
			SET_BIT(deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_BITSET], DB_DELIVERY_BITSET_STARTED_LOAD_SCENE)
		ENDIF
	ENDIF
	
	IF IS_SCREEN_FADED_OUT()
		TOGGLE_DELIVERY_FLAG_EXTRA(deliveryData, DELIVERY_FLAG_EXTRA_SHOW_SHARD, FALSE)
	ENDIF
	
	IF SIMPLE_CUTSCENE_IS_SCENE_RUNNING(deliveryData.cutsceneData.cutscene, 0)
		
		DEFUNCT_BASE_MAINTAIN_HIDE_MAP_SHADOW(eSimpleInteriorID)
		
		SWITCH deliveryData.deliveryType
			CASE DT_VEHICLE
				IF deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_DELIVERY_TYPE] = DEFUNCT_BASE_DELIVERY_CUTSCENE_LEAVE_VEHICLE_OUTSIDE
				
					IF deliveryData.cutsceneData.cutscene.iCurrentSceneElapsedTime >= ciExitVehStartTime
						CUTSCENE_GANG_OPS_BASE_TASK_PEDS_TO_LEAVE_VEHICLE(deliveryData)
					ENDIF
				
				ELIF deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_DELIVERY_TYPE] = DEFUNCT_BASE_DELIVERY_CUTSCENE_BRING_PERSONAL_VEH_INSIDE
				
					IF NOT IS_BIT_SET(deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_BITSET], DB_DELIVERY_BITSET_TASK_MOVE_VEH_FORWARD)
					AND deliveryData.cutsceneData.cutscene.iCurrentSceneElapsedTime >= ciVehMovementStartTime
						
						IF NOT DEFUNCT_BASE_ENTRY_SCENE_SHOULD_VEHICLE_DRIVE_IN(deliveryData.cutsceneData.vehiclePackArray[0].vehicle)
							SET_BIT(deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_BITSET], DB_DELIVERY_BITSET_TASK_MOVE_VEH_FORWARD)
						ELSE
							CUTSCENE_GANG_OPS_DEFUNCT_BASE_SETUP_VEHICLE_TASK(deliveryData, eSimpleInteriorID)
						ENDIF
						
						IF DOES_ENTITY_EXIST(deliveryData.cutsceneData.objectArray[DEFUNC_BASE_CS_ENTRY_OBJECT_OPEN_HATCH])
							SET_ENTITY_COLLISION(deliveryData.cutsceneData.objectArray[DEFUNC_BASE_CS_ENTRY_OBJECT_OPEN_HATCH], TRUE)
						ENDIF
					ENDIF
					
				ELSE
					IF NOT IS_BIT_SET(deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_BITSET], DB_DELIVERY_BITSET_TASK_MOVE_LIFT)
					AND deliveryData.cutsceneData.cutscene.iCurrentSceneElapsedTime >= 1500

						SET_BIT(deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_BITSET], DB_DELIVERY_BITSET_TASK_MOVE_LIFT)
						CDEBUG2LN(DEBUG_DELIVERY, "CUTSCENE_GANG_OPS_DEFUNCT_BASE_MAINTAIN_SCENE - task vehicle to drive inside.")
						
					ENDIF
				ENDIF
			BREAK
			
			CASE DT_FOOT
				IF NOT IS_BIT_SET(deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_BITSET], DB_DELIVERY_BITSET_TASK_MOVE_INSIDE)
					SET_BIT(deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_BITSET], DB_DELIVERY_BITSET_TASK_MOVE_INSIDE)
				ENDIF
				
				IF IS_ENTITY_ALIVE(deliveryData.cutsceneData.pedArray[0])
					SET_ENTITY_DYNAMIC(deliveryData.cutsceneData.pedArray[0],TRUE)
					FREEZE_ENTITY_POSITION(deliveryData.cutsceneData.pedArray[0], FALSE)
				ENDIF
			BREAK
		ENDSWITCH
	ELIF SIMPLE_CUTSCENE_IS_SCENE_RUNNING(deliveryData.cutsceneData.cutscene, 1)
		DEFUNCT_BASE_MAINTAIN_HIDE_MAP_SHADOW(eSimpleInteriorID)
	ENDIF
ENDPROC

FUNC BOOL CUTSCENE_GANG_OPS_DEFUNCT_BASE_ASSETS_LOADED(DELIVERY_DATA &deliveryData)
	UNUSED_PARAMETER(deliveryData)
	BOOL bAllAssetsLoaded = TRUE
	
	bAllAssetsLoaded = GANG_OPS_DEFUNCT_BASE_HAVE_EXTERIOR_CUTSCENE_ASSETS_LOADED(TRUE)
	
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
	AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	AND IS_ENTITY_ALIVE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
	AND (GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) = WASTELANDER
	OR GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) = FLATBED)
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_auto_salvage_stromberg")))
	
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_auto_salvage_stromberg")))
			bAllAssetsLoaded = FALSE
		ENDIF
	ENDIF
	
	STRING sAnimDict = "anim@amb@facility@hangerdoors@base@enter_exit@male@"
	IF IS_PLAYER_FEMALE()
		sAnimDict = "anim@amb@facility@hangerdoors@base@enter_exit@female@"
	ENDIF
	
	REQUEST_ANIM_DICT(sAnimDict)
	IF NOT HAS_ANIM_DICT_LOADED(sAnimDict)
		bAllAssetsLoaded = FALSE
	ENDIF
	
	IF NOT REQUEST_SCRIPT_AUDIO_BANK("DLC_CHRISTMAS2017/XM_FACILITY_ENTRY_EXIT")
		CDEBUG1LN(DEBUG_DELIVERY, "[SHC]CUTSCENE_GANG_OPS_DEFUNCT_BASE_ASSETS_LOADED Audio - DLC_CHRISTMAS2017/XM_FACILITY_ENTRY_EXIT - not loaded yet.")
		bAllAssetsLoaded = FALSE
	ENDIF
	
	IF deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_DELIVERY_TYPE] = DEFUNCT_BASE_DELIVERY_CUTSCENE_AVENGER
		REQUEST_MODEL(GET_ARMOURY_AIRCRAFT_COLLAPSED_WING_MODEL())
		IF NOT HAS_MODEL_LOADED(GET_ARMOURY_AIRCRAFT_COLLAPSED_WING_MODEL())
			bAllAssetsLoaded = FALSE
		ENDIF
	ENDIF
	
	RETURN bAllAssetsLoaded
ENDFUNC

PROC CUTSCENE_GANG_OPS_DEFUNCT_BASE_CLEANUP(DELIVERY_DATA &deliveryData)
	CDEBUG1LN(DEBUG_DELIVERY, "CUTSCENE_GANG_OPS_DEFUNCT_BASE_CLEANUP")
	
	SIMPLE_INTERIORS eSimpleInteriorID = GET_SIMPLE_INTERIOR_FROM_FM_GANGOPS_DROPOFF(deliveryData.eDropoffID)
	
	IF IS_BIT_SET(deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_BITSET], DB_DELIVERY_BITSET_CUTSCENE_STARTED)
		
		GANG_OPS_DEFUNCT_BASE_TOGGLE_IPL_VISIBILITY(eSimpleInteriorID, TRUE)
		
		//Remove ambient vehicles in front of the facility
		VECTOR vClearAreaCoord	= FM_GANG_OPS_GET_IN_VEHICLE_DROPOFF_LICATION(deliveryData.eDropoffID)
		FLOAT fClearAreaRadius 	= 25.0
		
		CDEBUG1LN(DEBUG_DELIVERY, "CUTSCENE_GANG_OPS_DEFUNCT_BASE_CLEANUP - vClearAreaCoord: ", vClearAreaCoord, " fClearAreaRadius: ", fClearAreaRadius)
		CLEAR_AREA_OF_VEHICLES(vClearAreaCoord, fClearAreaRadius, TRUE, FALSE , FALSE , FALSE, TRUE, TRUE) //OnFire
		CLEAR_AREA_OF_VEHICLES(vClearAreaCoord, fClearAreaRadius, TRUE, FALSE , TRUE , FALSE, TRUE, FALSE) //Wrecked
		
		IF GET_DELIVERY_FLAG_EXTRA(deliveryData, DELIVERY_FLAG_EXTRA_RETURN_PV_TO_PROPERTY)
			CDEBUG1LN(DEBUG_DELIVERY, "CUTSCENE_GANG_OPS_DEFUNCT_BASE_CLEANUP - Calling CLEANUP_MP_SAVED_VEHICLE")
			CLEANUP_MP_SAVED_VEHICLE(FALSE, TRUE, TRUE, FALSE, TRUE, FALSE, FALSE)
		ENDIF
		
		IF deliveryData.deliveryType != DT_FOOT
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
				SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PreventAutoShuffleToDriversSeat, FALSE)
			ENDIF
		ENDIF
		
		IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		AND IS_PED_WEARING_A_HELMET(PLAYER_PED_ID())
			REMOVE_PED_HELMET(PLAYER_PED_ID(), TRUE)
		ENDIF
		
		IF deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_DELIVERY_TYPE] != DEFUNCT_BASE_DELIVERY_CUTSCENE_AVENGER
			SET_BIT(g_SimpleInteriorData.iThirdBS, BS3_SIMPLE_INTERIOR_GLOBAL_DATA_FORCE_SIMPLE_INTERIOR_ON_FOOT_QUICK_WARP)
			CDEBUG1LN(DEBUG_DELIVERY, "CUTSCENE_GANG_OPS_DEFUNCT_BASE_CLEANUP set BS3_SIMPLE_INTERIOR_GLOBAL_DATA_FORCE_SIMPLE_INTERIOR_ON_FOOT_QUICK_WARP")
		ENDIF
		
		IF IS_NEW_LOAD_SCENE_ACTIVE()
			NEW_LOAD_SCENE_STOP()
		ENDIF
	ENDIF
	
	IF deliveryData.deliveryType = DT_FOOT
		IF IS_PLAYER_FEMALE()
			REMOVE_ANIM_DICT("anim@amb@facility@hangerdoors@base@enter_exit@female@")
		ELSE
			REMOVE_ANIM_DICT("anim@amb@facility@hangerdoors@base@enter_exit@male@")
		ENDIF
		
		STOP_AUDIO_SCENE("dlc_xm_facility_enter_on_foot_exterior_scene")
	ELSE
		STOP_AUDIO_SCENE("dlc_xm_facility_enter_in_vehicle_exterior_scene")
	ENDIF
	
	IF NOT IS_BIT_SET(deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_BITSET], DB_DELIVERY_BITSET_CUTSCENE_RELEASED_ASSETS)
		SET_MODEL_AS_NO_LONGER_NEEDED(GANG_OPS_DEFUNCT_BASE_MODEL_ANIMATED_HATCH())
		SET_MODEL_AS_NO_LONGER_NEEDED(GANG_OPS_DEFUNCT_BASE_MODEL_LIGHT())
		SET_MODEL_AS_NO_LONGER_NEEDED(GANG_OPS_DEFUNCT_BASE_MODEL_LIFT(TRUE))
		SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_auto_salvage_stromberg")))
		
		IF deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_DELIVERY_TYPE] = DEFUNCT_BASE_DELIVERY_CUTSCENE_AVENGER
			SET_MODEL_AS_NO_LONGER_NEEDED(GET_ARMOURY_AIRCRAFT_COLLAPSED_WING_MODEL())
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_BITSET], DB_DELIVERY_BITSET_TASK_PLAYED_RELEASE_SOUND_ON_CLEANUP)
		STOP_SOUND(deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_SOUND_ID])
		RELEASE_SOUND_ID(deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_SOUND_ID])
	ENDIF
	
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_CHRISTMAS2017/XM_FACILITY_ENTRY_EXIT")
	REMOVE_ANIM_DICT("anim@amb@facility@hanger_doors")
	
	GANG_OPS_BASE_ENTRY_SCENE_CLEANUP_MAP_AREA(GET_SIMPLE_INTERIOR_FROM_FM_GANGOPS_DROPOFF(deliveryData.eDropoffID))
ENDPROC

FUNC BOOL IS_LOCAL_PLAYER_DELIVERING_TO_DEFUNCT_BASE_IN_OWNERS_AVENGER(PLAYER_INDEX pOwner)
	IF FM_GANGOPS_DROPOFF_IS_PLAYER_PROPERTY(g_FreemodeDeliveryData.eDropoffForDeliveryScript)
	AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
	AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		VEHICLE_INDEX veh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		IF IS_VEHICLE_A_PERSONAL_ARMORY_AIRCRAFT(veh)
		AND GET_OWNER_OF_PERSONAL_ARMORY_AIRCRAFT(veh) = pOwner
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//Should this vehicle drive into the hangar in the cutscene.
FUNC BOOL CUTSCENE_GANG_OPS_DEFUNCT_BASE_SHOULD_VEHICLE_ENTER(DELIVERY_DATA &deliveryData)
	
	IF NOT IS_ENTITY_ALIVE(deliveryData.deliveryVehicle)
		PRINTLN("CUTSCENE_GANG_OPS_DEFUNCT_BASE_SHOULD_VEHICLE_ENTER - vehicle does not exist")
		RETURN FALSE
	ENDIF
	
	CDEBUG1LN(DEBUG_DELIVERY, "CUTSCENE_GANG_OPS_DEFUNCT_BASE - Mission vehicle, bring in. Mission: ", IS_ENTITY_A_MISSION_ENTITY(deliveryData.deliveryVehicle)
	," Personal: ", IS_VEHICLE_A_PERSONAL_VEHICLE(deliveryData.deliveryVehicle), " Hangar PV: ", IS_THIS_MODEL_ALLOWED_IN_HANGAR(GET_ENTITY_MODEL(deliveryData.deliveryVehicle)), " SVM? ", IS_SVM_VEHICLE(deliveryData.deliveryVehicle))
	
	IF DECOR_EXIST_ON(deliveryData.deliveryVehicle,"FMDeliverableID")
		RETURN TRUE
	ENDIF
	
	PLAYER_INDEX pVehOwner = GET_OWNER_OF_PERSONAL_VEHICLE(deliveryData.deliveryVehicle)
						
	IF IS_VEHICLE_A_PERSONAL_VEHICLE(deliveryData.deliveryVehicle)
	AND pVehOwner = deliveryData.deliveryOwner
	AND NOT IS_THIS_MODEL_ALLOWED_IN_HANGAR(GET_ENTITY_MODEL(deliveryData.deliveryVehicle))
	AND NOT IS_VEHICLE_ARMORY_TRUCK(deliveryData.deliveryVehicle)
	AND NOT IS_SVM_VEHICLE(deliveryData.deliveryVehicle)	
	AND IS_PV_OWNER_IN_VEHICLE(deliveryData.deliveryVehicle, pVehOwner)
		IF deliveryData.deliveryOwner = PLAYER_ID()
		AND IS_VEHICLE_ASSIGNED_TO_SIMPLE_INTERIOR(GET_SIMPLE_INTERIOR_FROM_FM_GANGOPS_DROPOFF(deliveryData.eDropoffID))
			TOGGLE_DELIVERY_FLAG_EXTRA(deliveryData, DELIVERY_FLAG_EXTRA_RETURN_PV_TO_PROPERTY, TRUE)
			CDEBUG1LN(DEBUG_DELIVERY, "CUTSCENE_GANG_OPS_DEFUNCT_BASE - DELIVERY_FLAG_EXTRA_RETURN_PV_TO_PROPERTY - TRUE.")
		ELSE
			TOGGLE_DELIVERY_FLAG_EXTRA(deliveryData, DELIVERY_FLAG_EXTRA_RETURN_PV_TO_PROPERTY, FALSE)			
			CDEBUG1LN(DEBUG_DELIVERY, "CUTSCENE_GANG_OPS_DEFUNCT_BASE - DELIVERY_FLAG_EXTRA_RETURN_PV_TO_PROPERTY - FALSE.")
		ENDIF
		
		RETURN TRUE
	ELSE
		PRINTLN("CUTSCENE_GANG_OPS_DEFUNCT_BASE_SHOULD_VEHICLE_ENTER - IS PV? ", IS_VEHICLE_A_PERSONAL_VEHICLE(deliveryData.deliveryVehicle), 
				" Deliverable owners vehicle? ", (pVehOwner = deliveryData.deliveryOwner),
				" IS_THIS_MODEL_ALLOWED_IN_HANGAR? ", IS_THIS_MODEL_ALLOWED_IN_HANGAR(GET_ENTITY_MODEL(deliveryData.deliveryVehicle)),
				" IS_VEHICLE_ARMORY_TRUCK? ", IS_VEHICLE_ARMORY_TRUCK(deliveryData.deliveryVehicle),
				" IS_SVM_VEHICLE? ", IS_SVM_VEHICLE(deliveryData.deliveryVehicle),
				" IS_PV_OWNER_IN_VEHICLE? ", IS_PV_OWNER_IN_VEHICLE(deliveryData.deliveryVehicle, pVehOwner))
	ENDIF
	
	CDEBUG1LN(DEBUG_DELIVERY, "CUTSCENE_GANG_OPS_DEFUNCT_BASE - CUTSCENE_GANG_OPS_DEFUNCT_BASE_SHOULD_VEHICLE_ENTER - FALSE.")
	RETURN FALSE
ENDFUNC
//Vehicle on the side, player walks in.
//walk in 
//drop from air.
//Plane fly in , heli fly in.
PROC CUTSCENE_GANG_OPS_DEFUNCT_BASE_INITIALIZE(FREEMODE_DELIVERY_SCRIPT_LAUNCH_DATA &_launchData, SCRIPT_DATA& _scriptData)	
	UNUSED_PARAMETER(_launchData)
	
	g_FreemodeDeliveryData.bDeliveryScriptAllowPropertyEntryCollisionProp = FALSE
	
	//Work out type of delivery
	SWITCH _scriptData.deliveryData.deliveryType
		CASE DT_VEHICLE
			SWITCH _scriptData.deliveryData.eCutscene
				CASE GANG_OPS_CUTSCENE_DEFUNCT_BASE
					
					SET_BIT(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_GLOBAL_DATA_DONT_BRING_VEHICLE_INSIDE)
					
					IF GET_ENTITY_MODEL(_scriptData.deliveryData.deliveryVehicle) = AVENGER
						_scriptData.deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_DELIVERY_TYPE] = DEFUNCT_BASE_DELIVERY_CUTSCENE_AVENGER
						
						IF GET_OWNER_OF_PERSONAL_ARMORY_AIRCRAFT(_scriptData.deliveryData.deliveryVehicle) = _launchData.pOwner
							
							TOGGLE_DELIVERY_FLAG_DEFAULT(_scriptData.deliveryData,DELIVERY_FLAG_DEFAULT_CLONE_NORMAL_AVENGER, FALSE)
							
							IF IS_PV_OWNER_IN_VEHICLE(_scriptData.deliveryData.deliveryVehicle, _launchData.pOwner)
								IF (GET_PED_IN_VEHICLE_SEAT(_scriptData.deliveryData.deliveryVehicle) = GET_PLAYER_PED(_launchData.pOwner))
									CLEAR_BIT(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_GLOBAL_DATA_DONT_BRING_VEHICLE_INSIDE)
									CDEBUG1LN(DEBUG_DELIVERY, "CUTSCENE_GANG_OPS_DEFUNCT_BASE_INITIALIZE - Entering in owners personal avenger with them as the pilot")
								ELSE
									_scriptData.deliveryData.deliveryType = DT_FOOT
									CDEBUG1LN(DEBUG_DELIVERY, "CUTSCENE_GANG_OPS_DEFUNCT_BASE_INITIALIZE - Entering in owners personal avenger but they are in the passenger seat")
								ENDIF
							ELSE
								_scriptData.deliveryData.deliveryType = DT_FOOT
								CDEBUG1LN(DEBUG_DELIVERY, "CUTSCENE_GANG_OPS_DEFUNCT_BASE_INITIALIZE - Entering in owners personal avenger but they are not in the vehicle")
							ENDIF
						ELSE
							CDEBUG1LN(DEBUG_DELIVERY, "CUTSCENE_GANG_OPS_DEFUNCT_BASE_INITIALIZE - Entering in another players personal avenger")
						ENDIF
					ELIF CUTSCENE_GANG_OPS_DEFUNCT_BASE_SHOULD_VEHICLE_ENTER(_scriptData.deliveryData)
						IF DECOR_EXIST_ON(_scriptData.deliveryData.deliveryVehicle,"FMDeliverableID")
						OR GET_ENTITY_MODEL(_scriptData.deliveryData.deliveryVehicle) = THRUSTER
							CDEBUG1LN(DEBUG_DELIVERY, "CUTSCENE_GANG_OPS_DEFUNCT_BASE_INITIALIZE - Entering in mission vehicle")
							_scriptData.deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_DELIVERY_TYPE] = DEFUNCT_BASE_DELIVERY_CUTSCENE_BRING_VEHICLE_INSIDE
						ELSE
							CDEBUG1LN(DEBUG_DELIVERY, "CUTSCENE_GANG_OPS_DEFUNCT_BASE_INITIALIZE - Entering in owners PV")
							_scriptData.deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_DELIVERY_TYPE] = DEFUNCT_BASE_DELIVERY_CUTSCENE_BRING_PERSONAL_VEH_INSIDE
						ENDIF
					ELSE
						CDEBUG1LN(DEBUG_DELIVERY, "CUTSCENE_GANG_OPS_DEFUNCT_BASE_INITIALIZE - Leave vehicle outside")
						_scriptData.deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_DELIVERY_TYPE] = DEFUNCT_BASE_DELIVERY_CUTSCENE_LEAVE_VEHICLE_OUTSIDE						
						g_FreemodeDeliveryData.bDeliveryScriptAllowPropertyEntryCollisionProp = TRUE
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		
		CASE DT_FOOT
			//vehicle does not exist, jsut walk in.
			_scriptData.deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_DELIVERY_TYPE] = DEFUNCT_BASE_DELIVERY_CUTSCENE_WALK_IN
			CDEBUG1LN(DEBUG_DELIVERY, "TEST_INITIALIZE - DT_FOOT")
		BREAK
	ENDSWITCH
	
	SET_BIT(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_ENTERING_VIA_DELIVERY_MISSION)	
	
	#IF IS_DEBUG_BUILD
	CDEBUG1LN(DEBUG_DELIVERY, "TEST_INITIALIZE - DELIVERY TYPE: ", _scriptData.deliveryData.cutsceneData.intArray[DB_DELIVERY_INT_ARRAY_DELIVERY_TYPE])
	#ENDIF
	
	//Lookup
	_scriptData.functionPointers.coreFunctions.handlerAssetsLoaded = &CUTSCENE_GANG_OPS_DEFUNCT_BASE_ASSETS_LOADED
	_scriptData.functionPointers.coreFunctions.handlerAssetsCreate = &CUTSCENE_GANG_OPS_DEFUNCT_BASE_ASSETS_CREATE
	_scriptData.functionPointers.coreFunctions.handlerCreateScene = &CUTSCENE_GANG_OPS_DEFUNCT_BASE_CREATE_SCENE
	_scriptData.functionPointers.coreFunctions.handlerMaintainScene = &CUTSCENE_GANG_OPS_DEFUNCT_BASE_MAINTAIN_SCENE
	_scriptData.functionPointers.coreFunctions.handlerSceneSetup = &CUTSCENE_GANG_OPS_DEFUNCT_BASE_ASSET_SCENE_SETUP
	_scriptData.functionPointers.coreFunctions.handlerCleanup = &CUTSCENE_GANG_OPS_DEFUNCT_BASE_CLEANUP
			
	//Delivery Setup
	TOGGLE_DELIVERY_FLAG_EXTRA(_scriptData.deliveryData, DELIVERY_FLAG_EXTRA_REPOSITION_DELIVERY_VEHICLE, TRUE)
	TOGGLE_DELIVERY_FLAG_DEFAULT(_scriptData.deliveryData,DELIVERY_FLAG_DEFAULT_SHOW_SHARD_ON_FADDED_OUT_SCREEN, FALSE)
	TOGGLE_DELIVERY_FLAG_DEFAULT(_scriptData.deliveryData, DELIVERY_FLAG_DEFAULT_DO_VEH_HALT_LOCATE_CHECK, FALSE)
ENDPROC
