// FM_CONTENT_CUTSCENE_SPAWNS.sch
// Post mission spawn locations for FM Content missions
USING "globals.sch"
USING "net_spawn.sch"
USING "gb_delivery_definitions.sch"
USING "FM_CONTENT_CUTSCENE_HELPERS.sch"

FUNC BOOL GET_SOLOMON_OFFICE_POST_SCENE_SPAWN_POINT(INT iSpawnPoint, VECTOR &vSpawnPoint, FLOAT &fSpawnHeading)
	SWITCH iSpawnPoint
		CASE 0	vSpawnPoint 	= <<-1078.9417, -495.1308, 35.6103>>	fSpawnHeading 	= 300.3900	BREAK
		CASE 1	vSpawnPoint 	= <<-1078.1829, -496.4247, 35.5790>>	fSpawnHeading 	= 300.3900	BREAK
		CASE 2	vSpawnPoint 	= <<-1079.7004, -493.8369, 35.6141>>	fSpawnHeading 	= 300.3900	BREAK
		CASE 3	vSpawnPoint 	= <<-1077.4241, -497.7186, 35.5593>>	fSpawnHeading 	= 300.3900	BREAK
		CASE 4	vSpawnPoint 	= <<-1080.4592, -492.5430, 35.5865>>	fSpawnHeading 	= 300.3900	BREAK
		CASE 5	vSpawnPoint 	= <<-1076.6653, -499.0125, 35.5455>>	fSpawnHeading 	= 300.3900	BREAK
		CASE 6	vSpawnPoint 	= <<-1081.2180, -491.2491, 35.5320>>	fSpawnHeading 	= 300.3900	BREAK
		CASE 7	vSpawnPoint 	= <<-1077.6477, -494.3720, 35.6416>>	fSpawnHeading 	= 300.3900	BREAK
		CASE 8	vSpawnPoint 	= <<-1076.8889, -495.6659, 35.6084>>	fSpawnHeading 	= 300.3900	BREAK
		CASE 9	vSpawnPoint 	= <<-1078.4065, -493.0781, 35.6467>>	fSpawnHeading 	= 300.3900	BREAK
		CASE 10	vSpawnPoint 	= <<-1076.1301, -496.9598, 35.5548>>	fSpawnHeading 	= 300.3900	BREAK
		CASE 11	vSpawnPoint 	= <<-1079.1653, -491.7841, 35.6212>>	fSpawnHeading 	= 300.3900	BREAK
		CASE 12	vSpawnPoint 	= <<-1075.3713, -498.2537, 35.4912>>	fSpawnHeading 	= 300.3900	BREAK
		CASE 13	vSpawnPoint 	= <<-1079.9241, -490.4902, 35.5710>>	fSpawnHeading 	= 300.3900	BREAK
		CASE 14	vSpawnPoint 	= <<-1076.3538, -493.6132, 35.6503>>	fSpawnHeading 	= 300.3900	BREAK
		CASE 15	vSpawnPoint 	= <<-1075.5950, -494.9071, 35.6172>>	fSpawnHeading 	= 300.3900	BREAK
		CASE 16	vSpawnPoint 	= <<-1077.1125, -492.3192, 35.6520>>	fSpawnHeading 	= 300.3900	BREAK
		CASE 17	vSpawnPoint 	= <<-1074.8362, -496.2010, 35.5655>>	fSpawnHeading 	= 300.3900	BREAK
		CASE 18	vSpawnPoint 	= <<-1077.8713, -491.0253, 35.6251>>	fSpawnHeading 	= 300.3900	BREAK
		CASE 19	vSpawnPoint 	= <<-1074.0774, -497.4949, 35.5022>>	fSpawnHeading 	= 300.3900	BREAK
		CASE 20	vSpawnPoint 	= <<-1078.6301, -489.7314, 35.5718>>	fSpawnHeading 	= 300.3900	BREAK
		CASE 21	vSpawnPoint 	= <<-1075.0598, -492.8543, 35.6571>>	fSpawnHeading 	= 300.3900	BREAK
		CASE 22	vSpawnPoint 	= <<-1074.3010, -494.1483, 35.6260>>	fSpawnHeading 	= 300.3900	BREAK
		CASE 23	vSpawnPoint 	= <<-1075.8186, -491.5604, 35.6573>>	fSpawnHeading 	= 300.3900	BREAK
		CASE 24	vSpawnPoint 	= <<-1073.5422, -495.4422, 35.5749>>	fSpawnHeading 	= 300.3900	BREAK
		CASE 25	vSpawnPoint 	= <<-1076.5774, -490.2665, 35.6260>>	fSpawnHeading 	= 300.3900	BREAK
		CASE 26	vSpawnPoint 	= <<-1072.7834, -496.7361, 35.5119>>	fSpawnHeading 	= 300.3900	BREAK
		CASE 27	vSpawnPoint 	= <<-1077.3362, -488.9726, 35.5718>>	fSpawnHeading 	= 300.3900	BREAK
		CASE 28	vSpawnPoint 	= <<-1073.7659, -492.0955, 35.6640>>	fSpawnHeading 	= 300.3900	BREAK
		CASE 29	vSpawnPoint 	= <<-1073.0071, -493.3894, 35.6348>>	fSpawnHeading 	= 300.3900	BREAK
		CASE 30	vSpawnPoint 	= <<-1074.5247, -490.8016, 35.6614>>	fSpawnHeading 	= 300.3900	BREAK
		CASE 31	vSpawnPoint 	= <<-1072.2483, -494.6833, 35.5843>>	fSpawnHeading 	= 300.3900	BREAK
		CASE 32	vSpawnPoint 	= <<-1075.2834, -489.5077, 35.6269>>	fSpawnHeading 	= 300.3900	BREAK
		CASE 33	vSpawnPoint 	= <<-1071.4895, -495.9773, 35.5219>>	fSpawnHeading 	= 300.3900	BREAK
		CASE 34	vSpawnPoint 	= <<-1076.0422, -488.2138, 35.5684>>	fSpawnHeading 	= 300.3900	BREAK
		CASE 35	vSpawnPoint 	= <<-1072.4719, -491.3367, 35.6700>>	fSpawnHeading 	= 300.3900	BREAK
		CASE 36	vSpawnPoint 	= <<-1071.7131, -492.6306, 35.6403>>	fSpawnHeading 	= 300.3900	BREAK
		CASE 37	vSpawnPoint 	= <<-1073.2307, -490.0428, 35.6620>>	fSpawnHeading 	= 300.3900	BREAK
		CASE 38	vSpawnPoint 	= <<-1070.9543, -493.9245, 35.5909>>	fSpawnHeading 	= 300.3900	BREAK
		CASE 39	vSpawnPoint 	= <<-1073.9895, -488.7489, 35.6278>>	fSpawnHeading 	= 300.3900	BREAK
		CASE 40	vSpawnPoint 	= <<-1070.1956, -495.2184, 35.5276>>	fSpawnHeading 	= 300.3900	BREAK
		CASE 41	vSpawnPoint 	= <<-1074.7483, -487.4550, 35.5611>>	fSpawnHeading 	= 300.3900	BREAK
		CASE 42	vSpawnPoint 	= <<-1071.1780, -490.5779, 35.6691>>	fSpawnHeading 	= 300.3900	BREAK
		CASE 43	vSpawnPoint 	= <<-1070.4192, -491.8718, 35.6388>>	fSpawnHeading 	= 300.3900	BREAK
		CASE 44	vSpawnPoint 	= <<-1071.9368, -489.2840, 35.6571>>	fSpawnHeading 	= 300.3900	BREAK
		CASE 45	vSpawnPoint 	= <<-1069.6604, -493.1657, 35.5884>>	fSpawnHeading 	= 300.3900	BREAK
		CASE 46	vSpawnPoint 	= <<-1072.6956, -487.9901, 35.6235>>	fSpawnHeading 	= 300.3900	BREAK
		CASE 47	vSpawnPoint 	= <<-1068.9016, -494.4596, 35.5242>>	fSpawnHeading 	= 300.3900	BREAK
		CASE 48	vSpawnPoint 	= <<-1073.4543, -486.6961, 35.5535>>	fSpawnHeading 	= 300.3900	BREAK
		CASE 49	vSpawnPoint 	= <<-1069.8840, -489.8191, 35.6697>>	fSpawnHeading 	= 300.3900	BREAK
		CASE 50	vSpawnPoint 	= <<-1069.1252, -491.1130, 35.6373>>	fSpawnHeading 	= 300.3900	BREAK
		CASE 51	vSpawnPoint 	= <<-1070.6428, -488.5251, 35.6521>>	fSpawnHeading 	= 300.3900	BREAK
		CASE 52	vSpawnPoint 	= <<-1068.3665, -492.4069, 35.5889>>	fSpawnHeading 	= 300.3900	BREAK
		CASE 53	vSpawnPoint 	= <<-1071.4016, -487.2312, 35.6195>>	fSpawnHeading 	= 300.3900	BREAK
		CASE 54	vSpawnPoint 	= <<-1067.6077, -493.7008, 35.5208>>	fSpawnHeading 	= 300.3900	BREAK
		CASE 55	vSpawnPoint 	= <<-1072.1604, -485.9373, 35.5458>>	fSpawnHeading 	= 300.3900	BREAK
		CASE 56	vSpawnPoint 	= <<-1068.5901, -489.0602, 35.6703>>	fSpawnHeading 	= 300.3900	BREAK
		CASE 57	vSpawnPoint 	= <<-1067.8313, -490.3542, 35.6368>>	fSpawnHeading 	= 300.3900	BREAK
		CASE 58	vSpawnPoint 	= <<-1069.3489, -487.7663, 35.6480>>	fSpawnHeading 	= 300.3900	BREAK
		CASE 59	vSpawnPoint 	= <<-1067.0725, -491.6481, 35.5894>>	fSpawnHeading 	= 300.3900	BREAK
		DEFAULT RETURN FALSE
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL GET_OMEGA_POST_SCENE_SPAWN_POINT(INT iSpawnPoint, VECTOR &vSpawnPoint, FLOAT &fSpawnHeading)
	SWITCH iSpawnPoint
		CASE 0    vSpawnPoint 	= <<2309.6111, 2565.4014, 45.6680>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 1    vSpawnPoint 	= <<2311.3110, 2565.4014, 45.6677>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 2    vSpawnPoint 	= <<2313.0110, 2565.4014, 45.6677>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 3    vSpawnPoint 	= <<2314.7109, 2565.4014, 45.6677>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 4    vSpawnPoint 	= <<2316.4109, 2565.4014, 45.6677>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 5    vSpawnPoint 	= <<2318.1108, 2565.4014, 45.6677>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 6    vSpawnPoint 	= <<2319.8108, 2565.4014, 45.6677>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 7    vSpawnPoint 	= <<2309.6111, 2567.1013, 45.6680>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 8    vSpawnPoint 	= <<2311.3110, 2567.1013, 45.6680>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 9    vSpawnPoint 	= <<2313.0110, 2567.1013, 45.6677>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 10    vSpawnPoint 	= <<2314.7109, 2567.1013, 45.6677>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 11    vSpawnPoint 	= <<2316.4109, 2567.1013, 45.6677>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 12    vSpawnPoint 	= <<2318.1108, 2567.1013, 45.6677>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 13    vSpawnPoint 	= <<2319.8108, 2567.1013, 45.6677>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 14    vSpawnPoint 	= <<2309.6111, 2568.8013, 45.6680>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 15    vSpawnPoint 	= <<2311.3110, 2568.8013, 45.6680>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 16    vSpawnPoint 	= <<2313.0110, 2568.8013, 45.6677>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 17    vSpawnPoint 	= <<2314.7109, 2568.8013, 45.6677>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 18    vSpawnPoint 	= <<2316.4109, 2568.8013, 45.6677>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 19    vSpawnPoint 	= <<2318.1108, 2568.8013, 45.6677>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 20    vSpawnPoint 	= <<2319.8108, 2568.8013, 45.6677>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 21    vSpawnPoint 	= <<2309.6111, 2570.5012, 45.6680>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 22    vSpawnPoint 	= <<2311.3110, 2570.5012, 45.6677>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 23    vSpawnPoint 	= <<2313.0110, 2570.5012, 45.6677>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 24    vSpawnPoint 	= <<2314.7109, 2570.5012, 45.6677>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 25    vSpawnPoint 	= <<2316.4109, 2570.5012, 45.6677>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 26    vSpawnPoint 	= <<2318.1108, 2570.5012, 45.6677>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 27    vSpawnPoint 	= <<2319.8108, 2570.5012, 45.6677>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 28    vSpawnPoint 	= <<2309.6111, 2572.2012, 45.6680>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 29    vSpawnPoint 	= <<2311.3110, 2572.2012, 45.6677>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 30    vSpawnPoint 	= <<2313.0110, 2572.2012, 45.6677>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 31    vSpawnPoint 	= <<2314.7109, 2572.2012, 45.6677>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 32    vSpawnPoint 	= <<2316.4109, 2572.2012, 45.6677>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 33    vSpawnPoint 	= <<2318.1108, 2572.2012, 45.6677>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 34    vSpawnPoint 	= <<2319.8108, 2572.2012, 45.6677>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 35    vSpawnPoint 	= <<2309.6111, 2573.9011, 45.6680>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 36    vSpawnPoint 	= <<2311.3110, 2573.9011, 45.6677>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 37    vSpawnPoint 	= <<2313.0110, 2573.9011, 45.6677>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 38    vSpawnPoint 	= <<2314.7109, 2573.9011, 45.6677>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 39    vSpawnPoint 	= <<2316.4109, 2573.9011, 45.6677>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 40    vSpawnPoint 	= <<2318.1108, 2573.9011, 45.6677>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 41    vSpawnPoint 	= <<2319.8108, 2573.9011, 45.6677>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 42    vSpawnPoint 	= <<2309.6111, 2575.6011, 45.6680>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 43    vSpawnPoint 	= <<2311.3110, 2575.6011, 45.6677>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 44    vSpawnPoint 	= <<2313.0110, 2575.6011, 45.6677>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 45    vSpawnPoint 	= <<2314.7109, 2575.6011, 45.6677>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 46    vSpawnPoint 	= <<2316.4109, 2575.6011, 45.6677>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 47    vSpawnPoint 	= <<2318.1108, 2575.6011, 45.6677>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 48    vSpawnPoint 	= <<2319.8108, 2575.6011, 45.6677>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 49    vSpawnPoint 	= <<2309.6111, 2577.3010, 45.6696>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 50    vSpawnPoint 	= <<2311.3110, 2577.3010, 45.6677>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 51    vSpawnPoint 	= <<2313.0110, 2577.3010, 45.6677>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 52    vSpawnPoint 	= <<2314.7109, 2577.3010, 45.6677>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 53    vSpawnPoint 	= <<2316.4109, 2577.3010, 45.6677>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 54    vSpawnPoint 	= <<2318.1108, 2577.3010, 45.6677>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 55    vSpawnPoint 	= <<2319.8108, 2577.3010, 45.6677>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 56    vSpawnPoint 	= <<2309.6111, 2579.0010, 45.6652>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 57    vSpawnPoint 	= <<2311.3110, 2579.0010, 45.6636>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 58    vSpawnPoint 	= <<2313.0110, 2579.0010, 45.6636>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 59    vSpawnPoint 	= <<2314.7109, 2579.0010, 45.6636>>    fSpawnHeading 	= 0.0000    BREAK
		DEFAULT RETURN FALSE
	ENDSWITCH
	RETURN TRUE
ENDFUNC

FUNC BOOL GET_PALETO_SHERRIFF_POST_SCENE_SPAWN_POINT(INT iSpawnPoint, VECTOR &vSpawnPoint, FLOAT &fSpawnHeading)
	SWITCH iSpawnPoint
		CASE 0    vSpawnPoint 	= <<-451.0987, 6032.6616, 30.3405>>    fSpawnHeading 	= 305.0314    BREAK
		CASE 1    vSpawnPoint 	= <<-450.1229, 6031.2695, 30.3405>>    fSpawnHeading 	= 305.0314    BREAK
		CASE 2    vSpawnPoint 	= <<-449.1470, 6029.8774, 30.3405>>    fSpawnHeading 	= 305.0314    BREAK
		CASE 3    vSpawnPoint 	= <<-448.1712, 6028.4854, 30.4901>>    fSpawnHeading 	= 305.0314    BREAK
		CASE 4    vSpawnPoint 	= <<-447.1954, 6027.0933, 30.4901>>    fSpawnHeading 	= 305.0314    BREAK
		CASE 5    vSpawnPoint 	= <<-446.2195, 6025.7012, 30.4901>>    fSpawnHeading 	= 305.0314    BREAK
		CASE 6    vSpawnPoint 	= <<-445.2437, 6024.3091, 30.4901>>    fSpawnHeading 	= 305.0314    BREAK
		CASE 7    vSpawnPoint 	= <<-449.7067, 6033.6377, 30.3405>>    fSpawnHeading 	= 305.0314    BREAK
		CASE 8    vSpawnPoint 	= <<-448.7308, 6032.2456, 30.3405>>    fSpawnHeading 	= 305.0314    BREAK
		CASE 9    vSpawnPoint 	= <<-447.7550, 6030.8535, 30.3405>>    fSpawnHeading 	= 305.0314    BREAK
		CASE 10    vSpawnPoint 	= <<-446.7792, 6029.4614, 30.3405>>    fSpawnHeading 	= 305.0314    BREAK
		CASE 11    vSpawnPoint 	= <<-445.8033, 6028.0693, 30.3405>>    fSpawnHeading 	= 305.0314    BREAK
		CASE 12    vSpawnPoint 	= <<-444.8275, 6026.6772, 30.3405>>    fSpawnHeading 	= 305.0314    BREAK
		CASE 13    vSpawnPoint 	= <<-443.8517, 6025.2852, 30.3405>>    fSpawnHeading 	= 305.0314    BREAK
		CASE 14    vSpawnPoint 	= <<-448.3146, 6034.6138, 30.3405>>    fSpawnHeading 	= 305.0314    BREAK
		CASE 15    vSpawnPoint 	= <<-447.3388, 6033.2217, 30.3405>>    fSpawnHeading 	= 305.0314    BREAK
		CASE 16    vSpawnPoint 	= <<-446.3630, 6031.8296, 30.3405>>    fSpawnHeading 	= 305.0314    BREAK
		CASE 17    vSpawnPoint 	= <<-445.3871, 6030.4375, 30.3405>>    fSpawnHeading 	= 305.0314    BREAK
		CASE 18    vSpawnPoint 	= <<-444.4113, 6029.0454, 30.3405>>    fSpawnHeading 	= 305.0314    BREAK
		CASE 19    vSpawnPoint 	= <<-443.4355, 6027.6533, 30.3405>>    fSpawnHeading 	= 305.0314    BREAK
		CASE 20    vSpawnPoint 	= <<-442.4597, 6026.2612, 30.3405>>    fSpawnHeading 	= 305.0314    BREAK
		CASE 21    vSpawnPoint 	= <<-446.9226, 6035.5898, 30.3405>>    fSpawnHeading 	= 305.0314    BREAK
		CASE 22    vSpawnPoint 	= <<-445.9468, 6034.1978, 30.3405>>    fSpawnHeading 	= 305.0314    BREAK
		CASE 23    vSpawnPoint 	= <<-444.9709, 6032.8057, 30.3405>>    fSpawnHeading 	= 305.0314    BREAK
		CASE 24    vSpawnPoint 	= <<-443.9951, 6031.4136, 30.3405>>    fSpawnHeading 	= 305.0314    BREAK
		CASE 25    vSpawnPoint 	= <<-443.0193, 6030.0215, 30.3405>>    fSpawnHeading 	= 305.0314    BREAK
		CASE 26    vSpawnPoint 	= <<-442.0435, 6028.6294, 30.3405>>    fSpawnHeading 	= 305.0314    BREAK
		CASE 27    vSpawnPoint 	= <<-441.0676, 6027.2373, 30.3405>>    fSpawnHeading 	= 305.0314    BREAK
		CASE 28    vSpawnPoint 	= <<-445.5306, 6036.5659, 30.3405>>    fSpawnHeading 	= 305.0314    BREAK
		CASE 29    vSpawnPoint 	= <<-444.5547, 6035.1738, 30.3405>>    fSpawnHeading 	= 305.0314    BREAK
		CASE 30    vSpawnPoint 	= <<-443.5789, 6033.7817, 30.3405>>    fSpawnHeading 	= 305.0314    BREAK
		CASE 31    vSpawnPoint 	= <<-442.6031, 6032.3896, 30.3405>>    fSpawnHeading 	= 305.0314    BREAK
		CASE 32    vSpawnPoint 	= <<-441.6273, 6030.9976, 30.3405>>    fSpawnHeading 	= 305.0314    BREAK
		CASE 33    vSpawnPoint 	= <<-440.6514, 6029.6055, 30.3405>>    fSpawnHeading 	= 305.0314    BREAK
		CASE 34    vSpawnPoint 	= <<-439.6756, 6028.2134, 30.3405>>    fSpawnHeading 	= 305.0314    BREAK
		CASE 35    vSpawnPoint 	= <<-444.1385, 6037.5420, 30.3405>>    fSpawnHeading 	= 305.0314    BREAK
		CASE 36    vSpawnPoint 	= <<-443.1627, 6036.1499, 30.3405>>    fSpawnHeading 	= 305.0314    BREAK
		CASE 37    vSpawnPoint 	= <<-442.1869, 6034.7578, 30.3405>>    fSpawnHeading 	= 305.0314    BREAK
		CASE 38    vSpawnPoint 	= <<-441.2111, 6033.3657, 30.3405>>    fSpawnHeading 	= 305.0314    BREAK
		CASE 39    vSpawnPoint 	= <<-440.2352, 6031.9736, 30.3405>>    fSpawnHeading 	= 305.0314    BREAK
		CASE 40    vSpawnPoint 	= <<-439.2594, 6030.5815, 30.3405>>    fSpawnHeading 	= 305.0314    BREAK
		CASE 41    vSpawnPoint 	= <<-438.2836, 6029.1895, 30.3405>>    fSpawnHeading 	= 305.0314    BREAK
		CASE 42    vSpawnPoint 	= <<-442.7465, 6038.5181, 30.3405>>    fSpawnHeading 	= 305.0314    BREAK
		CASE 43    vSpawnPoint 	= <<-441.7707, 6037.1260, 30.3405>>    fSpawnHeading 	= 305.0314    BREAK
		CASE 44    vSpawnPoint 	= <<-440.7949, 6035.7339, 30.3405>>    fSpawnHeading 	= 305.0314    BREAK
		CASE 45    vSpawnPoint 	= <<-439.8190, 6034.3418, 30.3405>>    fSpawnHeading 	= 305.0314    BREAK
		CASE 46    vSpawnPoint 	= <<-438.8432, 6032.9497, 30.3405>>    fSpawnHeading 	= 305.0314    BREAK
		CASE 47    vSpawnPoint 	= <<-437.8674, 6031.5576, 30.3405>>    fSpawnHeading 	= 305.0314    BREAK
		CASE 48    vSpawnPoint 	= <<-436.8915, 6030.1655, 30.3405>>    fSpawnHeading 	= 305.0314    BREAK
		CASE 49    vSpawnPoint 	= <<-441.3545, 6039.4941, 30.3405>>    fSpawnHeading 	= 305.0314    BREAK
		CASE 50    vSpawnPoint 	= <<-440.3787, 6038.1021, 30.3405>>    fSpawnHeading 	= 305.0314    BREAK
		CASE 51    vSpawnPoint 	= <<-439.4028, 6036.7100, 30.3405>>    fSpawnHeading 	= 305.0314    BREAK
		CASE 52    vSpawnPoint 	= <<-438.4270, 6035.3179, 30.3405>>    fSpawnHeading 	= 305.0314    BREAK
		CASE 53    vSpawnPoint 	= <<-437.4512, 6033.9258, 30.3405>>    fSpawnHeading 	= 305.0314    BREAK
		CASE 54    vSpawnPoint 	= <<-436.4753, 6032.5337, 30.3405>>    fSpawnHeading 	= 305.0314    BREAK
		CASE 55    vSpawnPoint 	= <<-435.4995, 6031.1416, 30.3405>>    fSpawnHeading 	= 305.0314    BREAK
		CASE 56    vSpawnPoint 	= <<-439.9625, 6040.4702, 30.3405>>    fSpawnHeading 	= 305.0314    BREAK
		CASE 57    vSpawnPoint 	= <<-438.9866, 6039.0781, 30.3405>>    fSpawnHeading 	= 305.0314    BREAK
		CASE 58    vSpawnPoint 	= <<-438.0108, 6037.6860, 30.3405>>    fSpawnHeading 	= 305.0314    BREAK
		CASE 59    vSpawnPoint 	= <<-437.0350, 6036.2939, 30.3405>>    fSpawnHeading 	= 305.0314    BREAK
		DEFAULT RETURN FALSE
	ENDSWITCH
	RETURN TRUE
ENDFUNC

FUNC BOOL GET_VESPUCCI_POLICE_POST_SCENE_SPAWN_POINT(INT iSpawnPoint, VECTOR &vSpawnPoint, FLOAT &fSpawnHeading)
	SWITCH iSpawnPoint
		CASE 0    vSpawnPoint 	= <<-1065.9716, -853.5154, 3.8673>>    fSpawnHeading 	= 216.0783    BREAK
		CASE 1    vSpawnPoint 	= <<-1067.3455, -854.5165, 3.8673>>    fSpawnHeading 	= 216.0783    BREAK
		CASE 2    vSpawnPoint 	= <<-1068.7194, -855.5176, 3.8673>>    fSpawnHeading 	= 216.0783    BREAK
		CASE 3    vSpawnPoint 	= <<-1070.0933, -856.5187, 3.8673>>    fSpawnHeading 	= 216.0783    BREAK
		CASE 4    vSpawnPoint 	= <<-1071.4672, -857.5198, 3.8673>>    fSpawnHeading 	= 216.0783    BREAK
		CASE 5    vSpawnPoint 	= <<-1072.8411, -858.5209, 3.8673>>    fSpawnHeading 	= 216.0783    BREAK
		CASE 6    vSpawnPoint 	= <<-1074.2150, -859.5220, 3.8671>>    fSpawnHeading 	= 216.0783    BREAK
		CASE 7    vSpawnPoint 	= <<-1064.9705, -854.8893, 3.8674>>    fSpawnHeading 	= 216.0783    BREAK
		CASE 8    vSpawnPoint 	= <<-1066.3444, -855.8904, 3.8675>>    fSpawnHeading 	= 216.0783    BREAK
		CASE 9    vSpawnPoint 	= <<-1067.7183, -856.8915, 3.8675>>    fSpawnHeading 	= 216.0783    BREAK
		CASE 10    vSpawnPoint 	= <<-1069.0922, -857.8926, 3.8675>>    fSpawnHeading 	= 216.0783    BREAK
		CASE 11    vSpawnPoint 	= <<-1070.4661, -858.8937, 3.8675>>    fSpawnHeading 	= 216.0783    BREAK
		CASE 12    vSpawnPoint 	= <<-1071.8400, -859.8948, 3.8674>>    fSpawnHeading 	= 216.0783    BREAK
		CASE 13    vSpawnPoint 	= <<-1073.2139, -860.8959, 3.8671>>    fSpawnHeading 	= 216.0783    BREAK
		CASE 14    vSpawnPoint 	= <<-1063.9694, -856.2633, 3.8676>>    fSpawnHeading 	= 216.0783    BREAK
		CASE 15    vSpawnPoint 	= <<-1065.3433, -857.2644, 3.8676>>    fSpawnHeading 	= 216.0783    BREAK
		CASE 16    vSpawnPoint 	= <<-1066.7172, -858.2655, 3.8676>>    fSpawnHeading 	= 216.0783    BREAK
		CASE 17    vSpawnPoint 	= <<-1068.0911, -859.2666, 3.8676>>    fSpawnHeading 	= 216.0783    BREAK
		CASE 18    vSpawnPoint 	= <<-1069.4650, -860.2677, 3.8676>>    fSpawnHeading 	= 216.0783    BREAK
		CASE 19    vSpawnPoint 	= <<-1070.8389, -861.2688, 3.8674>>    fSpawnHeading 	= 216.0783    BREAK
		CASE 20    vSpawnPoint 	= <<-1072.2128, -862.2699, 3.8671>>    fSpawnHeading 	= 216.0783    BREAK
		CASE 21    vSpawnPoint 	= <<-1062.9683, -857.6373, 3.8677>>    fSpawnHeading 	= 216.0783    BREAK
		CASE 22    vSpawnPoint 	= <<-1064.3422, -858.6384, 3.8677>>    fSpawnHeading 	= 216.0783    BREAK
		CASE 23    vSpawnPoint 	= <<-1065.7161, -859.6395, 3.8678>>    fSpawnHeading 	= 216.0783    BREAK
		CASE 24    vSpawnPoint 	= <<-1067.0900, -860.6406, 3.8678>>    fSpawnHeading 	= 216.0783    BREAK
		CASE 25    vSpawnPoint 	= <<-1068.4639, -861.6417, 3.8677>>    fSpawnHeading 	= 216.0783    BREAK
		CASE 26    vSpawnPoint 	= <<-1069.8378, -862.6428, 3.8674>>    fSpawnHeading 	= 216.0783    BREAK
		CASE 27    vSpawnPoint 	= <<-1071.2117, -863.6439, 3.8672>>    fSpawnHeading 	= 216.0783    BREAK
		CASE 28    vSpawnPoint 	= <<-1061.9672, -859.0112, 3.8679>>    fSpawnHeading 	= 216.0783    BREAK
		CASE 29    vSpawnPoint 	= <<-1063.3411, -860.0123, 3.8679>>    fSpawnHeading 	= 216.0783    BREAK
		CASE 30    vSpawnPoint 	= <<-1064.7150, -861.0134, 3.8679>>    fSpawnHeading 	= 216.0783    BREAK
		CASE 31    vSpawnPoint 	= <<-1066.0889, -862.0145, 3.8679>>    fSpawnHeading 	= 216.0783    BREAK
		CASE 32    vSpawnPoint 	= <<-1067.4628, -863.0156, 3.8678>>    fSpawnHeading 	= 216.0783    BREAK
		CASE 33    vSpawnPoint 	= <<-1068.8367, -864.0167, 3.8676>>    fSpawnHeading 	= 216.0783    BREAK
		CASE 34    vSpawnPoint 	= <<-1070.2106, -865.0178, 3.8676>>    fSpawnHeading 	= 216.0783    BREAK
		CASE 35    vSpawnPoint 	= <<-1060.9661, -860.3852, 3.8681>>    fSpawnHeading 	= 216.0783    BREAK
		CASE 36    vSpawnPoint 	= <<-1062.3400, -861.3863, 3.8681>>    fSpawnHeading 	= 216.0783    BREAK
		CASE 37    vSpawnPoint 	= <<-1063.7139, -862.3874, 3.8681>>    fSpawnHeading 	= 216.0783    BREAK
		CASE 38    vSpawnPoint 	= <<-1065.0878, -863.3885, 3.8681>>    fSpawnHeading 	= 216.0783    BREAK
		CASE 39    vSpawnPoint 	= <<-1066.4617, -864.3896, 3.8679>>    fSpawnHeading 	= 216.0783    BREAK
		CASE 40    vSpawnPoint 	= <<-1067.8356, -865.3907, 3.8679>>    fSpawnHeading 	= 216.0783    BREAK
		CASE 41    vSpawnPoint 	= <<-1069.2095, -866.3918, 3.8679>>    fSpawnHeading 	= 216.0783    BREAK
		CASE 42    vSpawnPoint 	= <<-1059.9650, -861.7592, 3.8743>>    fSpawnHeading 	= 216.0783    BREAK
		CASE 43    vSpawnPoint 	= <<-1061.3389, -862.7603, 3.8742>>    fSpawnHeading 	= 216.0783    BREAK
		CASE 44    vSpawnPoint 	= <<-1062.7128, -863.7614, 3.8737>>    fSpawnHeading 	= 216.0783    BREAK
		CASE 45    vSpawnPoint 	= <<-1064.0867, -864.7625, 3.8724>>    fSpawnHeading 	= 216.0783    BREAK
		CASE 46    vSpawnPoint 	= <<-1065.4606, -865.7635, 3.8719>>    fSpawnHeading 	= 216.0783    BREAK
		CASE 47    vSpawnPoint 	= <<-1066.8345, -866.7646, 3.8715>>    fSpawnHeading 	= 216.0783    BREAK
		CASE 48    vSpawnPoint 	= <<-1068.2084, -867.7657, 3.8710>>    fSpawnHeading 	= 216.0783    BREAK
		CASE 49    vSpawnPoint 	= <<-1058.9639, -863.1331, 3.9307>>    fSpawnHeading 	= 216.0783    BREAK
		CASE 50    vSpawnPoint 	= <<-1060.3378, -864.1342, 3.9351>>    fSpawnHeading 	= 216.0783    BREAK
		CASE 51    vSpawnPoint 	= <<-1061.7117, -865.1353, 3.9345>>    fSpawnHeading 	= 216.0783    BREAK
		CASE 52    vSpawnPoint 	= <<-1063.0856, -866.1364, 3.9261>>    fSpawnHeading 	= 216.0783    BREAK
		CASE 53    vSpawnPoint 	= <<-1064.4595, -867.1375, 3.9239>>    fSpawnHeading 	= 216.0783    BREAK
		CASE 54    vSpawnPoint 	= <<-1065.8334, -868.1386, 3.9234>>    fSpawnHeading 	= 216.0783    BREAK
		CASE 55    vSpawnPoint 	= <<-1067.2073, -869.1397, 3.9177>>    fSpawnHeading 	= 216.0783    BREAK
		CASE 56    vSpawnPoint 	= <<-1057.9628, -864.5071, 3.9915>>    fSpawnHeading 	= 216.0783    BREAK
		CASE 57    vSpawnPoint 	= <<-1059.3367, -865.5082, 3.9959>>    fSpawnHeading 	= 216.0783    BREAK
		CASE 58    vSpawnPoint 	= <<-1060.7106, -866.5093, 3.9916>>    fSpawnHeading 	= 216.0783    BREAK
		CASE 59    vSpawnPoint 	= <<-1062.0845, -867.5104, 3.9821>>    fSpawnHeading 	= 216.0783    BREAK
		DEFAULT RETURN FALSE
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL GET_SANDY_SHORES_SHERRIFF_POST_SCENE_SPAWN_POINT(INT iSpawnPoint, VECTOR &vSpawnPoint, FLOAT &fSpawnHeading)
	SWITCH iSpawnPoint
		CASE 0    vSpawnPoint 	= <<1860.9390, 3681.7063, 32.7652>>    fSpawnHeading 	= 217.0651    BREAK
		CASE 1    vSpawnPoint 	= <<1859.5824, 3680.6816, 32.7468>>    fSpawnHeading 	= 217.0651    BREAK
		CASE 2    vSpawnPoint 	= <<1858.2258, 3679.6570, 32.7415>>    fSpawnHeading 	= 217.0651    BREAK
		CASE 3    vSpawnPoint 	= <<1856.8693, 3678.6323, 32.7573>>    fSpawnHeading 	= 217.0651    BREAK
		CASE 4    vSpawnPoint 	= <<1855.5127, 3677.6077, 32.7697>>    fSpawnHeading 	= 217.0651    BREAK
		CASE 5    vSpawnPoint 	= <<1854.1561, 3676.5830, 32.7701>>    fSpawnHeading 	= 217.0651    BREAK
		CASE 6    vSpawnPoint 	= <<1852.7996, 3675.5583, 32.7716>>    fSpawnHeading 	= 217.0651    BREAK
		CASE 7    vSpawnPoint 	= <<1851.4430, 3674.5337, 32.7759>>    fSpawnHeading 	= 217.0651    BREAK
		CASE 8    vSpawnPoint 	= <<1850.0864, 3673.5090, 32.7494>>    fSpawnHeading 	= 217.0651    BREAK
		CASE 9    vSpawnPoint 	= <<1848.7299, 3672.4844, 32.7188>>    fSpawnHeading 	= 217.0651    BREAK
		CASE 10    vSpawnPoint 	= <<1847.3733, 3671.4597, 32.6974>>    fSpawnHeading 	= 217.0651    BREAK
		CASE 11    vSpawnPoint 	= <<1846.0167, 3670.4351, 32.6798>>    fSpawnHeading 	= 217.0651    BREAK
		CASE 12    vSpawnPoint 	= <<1844.6602, 3669.4104, 32.6800>>    fSpawnHeading 	= 217.0651    BREAK
		CASE 13    vSpawnPoint 	= <<1843.3036, 3668.3857, 32.6800>>    fSpawnHeading 	= 217.0651    BREAK
		CASE 14    vSpawnPoint 	= <<1841.9470, 3667.3611, 32.6800>>    fSpawnHeading 	= 217.0651    BREAK
		CASE 15    vSpawnPoint 	= <<1861.9636, 3680.3499, 32.6875>>    fSpawnHeading 	= 217.0651    BREAK
		CASE 16    vSpawnPoint 	= <<1860.6071, 3679.3252, 32.6788>>    fSpawnHeading 	= 217.0651    BREAK
		CASE 17    vSpawnPoint 	= <<1859.2505, 3678.3005, 32.6840>>    fSpawnHeading 	= 217.0651    BREAK
		CASE 18    vSpawnPoint 	= <<1857.8939, 3677.2759, 32.6992>>    fSpawnHeading 	= 217.0651    BREAK
		CASE 19    vSpawnPoint 	= <<1856.5374, 3676.2512, 32.6916>>    fSpawnHeading 	= 217.0651    BREAK
		CASE 20    vSpawnPoint 	= <<1855.1808, 3675.2266, 32.6912>>    fSpawnHeading 	= 217.0651    BREAK
		CASE 21    vSpawnPoint 	= <<1853.8242, 3674.2019, 32.7036>>    fSpawnHeading 	= 217.0651    BREAK
		CASE 22    vSpawnPoint 	= <<1852.4677, 3673.1772, 32.7260>>    fSpawnHeading 	= 217.0651    BREAK
		CASE 23    vSpawnPoint 	= <<1851.1111, 3672.1526, 32.7209>>    fSpawnHeading 	= 217.0651    BREAK
		CASE 24    vSpawnPoint 	= <<1849.7545, 3671.1279, 32.7083>>    fSpawnHeading 	= 217.0651    BREAK
		CASE 25    vSpawnPoint 	= <<1848.3979, 3670.1033, 32.7027>>    fSpawnHeading 	= 217.0651    BREAK
		CASE 26    vSpawnPoint 	= <<1847.0414, 3669.0786, 32.7029>>    fSpawnHeading 	= 217.0651    BREAK
		CASE 27    vSpawnPoint 	= <<1845.6848, 3668.0540, 32.7191>>    fSpawnHeading 	= 217.0651    BREAK
		CASE 28    vSpawnPoint 	= <<1844.3282, 3667.0293, 32.7398>>    fSpawnHeading 	= 217.0651    BREAK
		CASE 29    vSpawnPoint 	= <<1842.9717, 3666.0046, 32.7605>>    fSpawnHeading 	= 217.0651    BREAK
		CASE 30    vSpawnPoint 	= <<1862.9883, 3678.9934, 32.6500>>    fSpawnHeading 	= 217.0651    BREAK
		CASE 31    vSpawnPoint 	= <<1861.6317, 3677.9688, 32.6466>>    fSpawnHeading 	= 217.0651    BREAK
		CASE 32    vSpawnPoint 	= <<1860.2751, 3676.9441, 32.6425>>    fSpawnHeading 	= 217.0651    BREAK
		CASE 33    vSpawnPoint 	= <<1858.9186, 3675.9194, 32.6238>>    fSpawnHeading 	= 217.0651    BREAK
		CASE 34    vSpawnPoint 	= <<1857.5620, 3674.8948, 32.6106>>    fSpawnHeading 	= 217.0651    BREAK
		CASE 35    vSpawnPoint 	= <<1856.2054, 3673.8701, 32.6095>>    fSpawnHeading 	= 217.0651    BREAK
		CASE 36    vSpawnPoint 	= <<1854.8489, 3672.8455, 32.6727>>    fSpawnHeading 	= 217.0651    BREAK
		CASE 37    vSpawnPoint 	= <<1853.4923, 3671.8208, 32.7438>>    fSpawnHeading 	= 217.0651    BREAK
		CASE 38    vSpawnPoint 	= <<1852.1357, 3670.7961, 32.8135>>    fSpawnHeading 	= 217.0651    BREAK
		CASE 39    vSpawnPoint 	= <<1850.7792, 3669.7715, 32.7995>>    fSpawnHeading 	= 217.0651    BREAK
		CASE 40    vSpawnPoint 	= <<1849.4226, 3668.7468, 32.8329>>    fSpawnHeading 	= 217.0651    BREAK
		CASE 41    vSpawnPoint 	= <<1848.0660, 3667.7222, 32.8663>>    fSpawnHeading 	= 217.0651    BREAK
		CASE 42    vSpawnPoint 	= <<1846.7095, 3666.6975, 32.9053>>    fSpawnHeading 	= 217.0651    BREAK
		CASE 43    vSpawnPoint 	= <<1845.3529, 3665.6729, 32.9440>>    fSpawnHeading 	= 217.0651    BREAK
		CASE 44    vSpawnPoint 	= <<1843.9963, 3664.6482, 32.9847>>    fSpawnHeading 	= 217.0651    BREAK
		CASE 45    vSpawnPoint 	= <<1864.0129, 3677.6370, 32.6292>>    fSpawnHeading 	= 217.0651    BREAK
		CASE 46    vSpawnPoint 	= <<1862.6564, 3676.6123, 32.6211>>    fSpawnHeading 	= 217.0651    BREAK
		CASE 47    vSpawnPoint 	= <<1861.2998, 3675.5876, 32.6257>>    fSpawnHeading 	= 217.0651    BREAK
		CASE 48    vSpawnPoint 	= <<1859.9432, 3674.5630, 32.6340>>    fSpawnHeading 	= 217.0651    BREAK
		CASE 49    vSpawnPoint 	= <<1858.5867, 3673.5383, 32.6658>>    fSpawnHeading 	= 217.0651    BREAK
		CASE 50    vSpawnPoint 	= <<1857.2301, 3672.5137, 32.7348>>    fSpawnHeading 	= 217.0651    BREAK
		CASE 51    vSpawnPoint 	= <<1855.8735, 3671.4890, 32.8360>>    fSpawnHeading 	= 217.0651    BREAK
		CASE 52    vSpawnPoint 	= <<1854.5170, 3670.4644, 32.9049>>    fSpawnHeading 	= 217.0651    BREAK
		CASE 53    vSpawnPoint 	= <<1853.1604, 3669.4397, 32.9425>>    fSpawnHeading 	= 217.0651    BREAK
		CASE 54    vSpawnPoint 	= <<1851.8038, 3668.4150, 32.9648>>    fSpawnHeading 	= 217.0651    BREAK
		CASE 55    vSpawnPoint 	= <<1850.4473, 3667.3904, 33.0169>>    fSpawnHeading 	= 217.0651    BREAK
		CASE 56    vSpawnPoint 	= <<1849.0907, 3666.3657, 33.0687>>    fSpawnHeading 	= 217.0651    BREAK
		CASE 57    vSpawnPoint 	= <<1847.7341, 3665.3411, 33.1134>>    fSpawnHeading 	= 217.0651    BREAK
		CASE 58    vSpawnPoint 	= <<1846.3776, 3664.3164, 33.1617>>    fSpawnHeading 	= 217.0651    BREAK
		CASE 59    vSpawnPoint 	= <<1845.0210, 3663.2917, 33.1852>>    fSpawnHeading 	= 217.0651    BREAK
			DEFAULT RETURN FALSE
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL GET_MISSION_ROW_POLICE_POST_SCENE_SPAWN_POINT(INT iSpawnPoint, VECTOR &vSpawnPoint, FLOAT &fSpawnHeading)
	SWITCH iSpawnPoint
		CASE 0    vSpawnPoint 	= <<474.9756, -977.8572, 26.9839>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 1    vSpawnPoint 	= <<475.9756, -977.8572, 26.9839>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 2    vSpawnPoint 	= <<476.9756, -977.8572, 26.9839>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 3    vSpawnPoint 	= <<477.9756, -977.8572, 26.9839>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 4    vSpawnPoint 	= <<478.9756, -977.8572, 26.9839>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 5    vSpawnPoint 	= <<479.9756, -977.8572, 26.9839>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 6    vSpawnPoint 	= <<480.9756, -977.8572, 26.9839>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 7    vSpawnPoint 	= <<481.9756, -977.8572, 26.9839>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 8    vSpawnPoint 	= <<474.9756, -976.8572, 26.9839>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 9    vSpawnPoint 	= <<475.9756, -976.8572, 26.9839>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 10    vSpawnPoint 	= <<476.9756, -976.8572, 26.9839>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 11    vSpawnPoint 	= <<477.9756, -976.8572, 26.9839>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 12    vSpawnPoint 	= <<478.9756, -976.8572, 26.9839>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 13    vSpawnPoint 	= <<479.9756, -976.8572, 26.9839>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 14    vSpawnPoint 	= <<480.9756, -976.8572, 26.9839>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 15    vSpawnPoint 	= <<481.9756, -976.8572, 26.9839>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 16    vSpawnPoint 	= <<474.9756, -975.8572, 26.9839>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 17    vSpawnPoint 	= <<475.9756, -975.8572, 26.9839>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 18    vSpawnPoint 	= <<476.9756, -975.8572, 26.9839>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 19    vSpawnPoint 	= <<477.9756, -975.8572, 26.9839>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 20    vSpawnPoint 	= <<478.9756, -975.8572, 26.9839>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 21    vSpawnPoint 	= <<479.9756, -975.8572, 26.9839>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 22    vSpawnPoint 	= <<480.9756, -975.8572, 26.9839>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 23    vSpawnPoint 	= <<481.9756, -975.8572, 26.9839>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 24    vSpawnPoint 	= <<474.9756, -974.8572, 26.9839>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 25    vSpawnPoint 	= <<475.9756, -974.8572, 26.9839>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 26    vSpawnPoint 	= <<476.9756, -974.8572, 26.9839>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 27    vSpawnPoint 	= <<477.9756, -974.8572, 26.9839>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 28    vSpawnPoint 	= <<478.9756, -974.8572, 26.9839>>    fSpawnHeading 	= 0.0000    BREAK
		CASE 29    vSpawnPoint 	= <<479.9756, -974.8572, 26.9839>>    fSpawnHeading 	= 0.0000    BREAK
		DEFAULT RETURN FALSE
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL GET_GREAT_CHAPARRL_POST_SCENE_SPAWN_POINT(INT iSpawnPoint, VECTOR &vSpawnPoint, FLOAT &fSpawnHeading)
	SWITCH iSpawnPoint
		CASE 0    vSpawnPoint 	= <<-106.0423, 2801.1509, 52.1794>>    fSpawnHeading 	= 288.5801    BREAK
		CASE 1    vSpawnPoint 	= <<-105.5006, 2799.5396, 52.2389>>    fSpawnHeading 	= 288.5801    BREAK
		CASE 2    vSpawnPoint 	= <<-104.9589, 2797.9282, 52.3285>>    fSpawnHeading 	= 288.5801    BREAK
		CASE 3    vSpawnPoint 	= <<-104.4172, 2796.3169, 52.3501>>    fSpawnHeading 	= 288.5801    BREAK
		CASE 4    vSpawnPoint 	= <<-103.8756, 2794.7056, 52.3480>>    fSpawnHeading 	= 288.5801    BREAK
		CASE 5    vSpawnPoint 	= <<-103.3339, 2793.0942, 52.3493>>    fSpawnHeading 	= 288.5801    BREAK
		CASE 6    vSpawnPoint 	= <<-102.7922, 2791.4829, 52.1554>>    fSpawnHeading 	= 288.5801    BREAK
		CASE 7    vSpawnPoint 	= <<-104.4309, 2801.6926, 52.1665>>    fSpawnHeading 	= 288.5801    BREAK
		CASE 8    vSpawnPoint 	= <<-103.8892, 2800.0813, 52.2666>>    fSpawnHeading 	= 288.5801    BREAK
		CASE 9    vSpawnPoint 	= <<-103.3475, 2798.4700, 52.3392>>    fSpawnHeading 	= 288.5801    BREAK
		CASE 10    vSpawnPoint 	= <<-102.8058, 2796.8586, 52.3633>>    fSpawnHeading 	= 288.5801    BREAK
		CASE 11    vSpawnPoint 	= <<-102.2642, 2795.2473, 52.3301>>    fSpawnHeading 	= 288.5801    BREAK
		CASE 12    vSpawnPoint 	= <<-101.7225, 2793.6360, 52.3164>>    fSpawnHeading 	= 288.5801    BREAK
		CASE 13    vSpawnPoint 	= <<-101.1808, 2792.0247, 52.1820>>    fSpawnHeading 	= 288.5801    BREAK
		CASE 14    vSpawnPoint 	= <<-102.8195, 2802.2344, 52.0884>>    fSpawnHeading 	= 288.5801    BREAK
		CASE 15    vSpawnPoint 	= <<-102.2778, 2800.6230, 52.1444>>    fSpawnHeading 	= 288.5801    BREAK
		CASE 16    vSpawnPoint 	= <<-101.7361, 2799.0117, 52.3376>>    fSpawnHeading 	= 288.5801    BREAK
		CASE 17    vSpawnPoint 	= <<-101.1944, 2797.4004, 52.3509>>    fSpawnHeading 	= 288.5801    BREAK
		CASE 18    vSpawnPoint 	= <<-100.6528, 2795.7891, 52.3206>>    fSpawnHeading 	= 288.5801    BREAK
		CASE 19    vSpawnPoint 	= <<-100.1111, 2794.1777, 52.2889>>    fSpawnHeading 	= 288.5801    BREAK
		CASE 20    vSpawnPoint 	= <<-99.5694, 2792.5664, 52.2285>>    fSpawnHeading 	= 288.5801    BREAK
		CASE 21    vSpawnPoint 	= <<-101.2081, 2802.7761, 52.0751>>    fSpawnHeading 	= 288.5801    BREAK
		CASE 22    vSpawnPoint 	= <<-100.6664, 2801.1648, 52.1282>>    fSpawnHeading 	= 288.5801    BREAK
		CASE 23    vSpawnPoint 	= <<-100.1247, 2799.5535, 52.3206>>    fSpawnHeading 	= 288.5801    BREAK
		CASE 24    vSpawnPoint 	= <<-99.5830, 2797.9421, 52.3284>>    fSpawnHeading 	= 288.5801    BREAK
		CASE 25    vSpawnPoint 	= <<-99.0414, 2796.3308, 52.3170>>    fSpawnHeading 	= 288.5801    BREAK
		CASE 26    vSpawnPoint 	= <<-98.4997, 2794.7195, 52.2795>>    fSpawnHeading 	= 288.5801    BREAK
		CASE 27    vSpawnPoint 	= <<-97.9580, 2793.1082, 52.2397>>    fSpawnHeading 	= 288.5801    BREAK
		CASE 28    vSpawnPoint 	= <<-99.5967, 2803.3179, 52.1114>>    fSpawnHeading 	= 288.5801    BREAK
		CASE 29    vSpawnPoint 	= <<-99.0550, 2801.7065, 52.1684>>    fSpawnHeading 	= 288.5801    BREAK
		CASE 30    vSpawnPoint 	= <<-98.5133, 2800.0952, 52.2655>>    fSpawnHeading 	= 288.5801    BREAK
		CASE 31    vSpawnPoint 	= <<-97.9716, 2798.4839, 52.3158>>    fSpawnHeading 	= 288.5801    BREAK
		CASE 32    vSpawnPoint 	= <<-97.4300, 2796.8726, 52.3135>>    fSpawnHeading 	= 288.5801    BREAK
		CASE 33    vSpawnPoint 	= <<-96.8883, 2795.2612, 52.3068>>    fSpawnHeading 	= 288.5801    BREAK
		CASE 34    vSpawnPoint 	= <<-96.3466, 2793.6499, 52.2715>>    fSpawnHeading 	= 288.5801    BREAK
		CASE 35    vSpawnPoint 	= <<-97.9853, 2803.8596, 52.1879>>    fSpawnHeading 	= 288.5801    BREAK
		CASE 36    vSpawnPoint 	= <<-97.4436, 2802.2483, 52.2187>>    fSpawnHeading 	= 288.5801    BREAK
		CASE 37    vSpawnPoint 	= <<-96.9019, 2800.6370, 52.2758>>    fSpawnHeading 	= 288.5801    BREAK
		CASE 38    vSpawnPoint 	= <<-96.3603, 2799.0256, 52.3276>>    fSpawnHeading 	= 288.5801    BREAK
		CASE 39    vSpawnPoint 	= <<-95.8186, 2797.4143, 52.3349>>    fSpawnHeading 	= 288.5801    BREAK
		CASE 40    vSpawnPoint 	= <<-95.2769, 2795.8030, 52.3275>>    fSpawnHeading 	= 288.5801    BREAK
		CASE 41    vSpawnPoint 	= <<-94.7352, 2794.1917, 52.2777>>    fSpawnHeading 	= 288.5801    BREAK
		CASE 42    vSpawnPoint 	= <<-96.3739, 2804.4014, 52.2684>>    fSpawnHeading 	= 288.5801    BREAK
		CASE 43    vSpawnPoint 	= <<-95.8322, 2802.7900, 52.3338>>    fSpawnHeading 	= 288.5801    BREAK
		CASE 44    vSpawnPoint 	= <<-95.2905, 2801.1787, 52.2906>>    fSpawnHeading 	= 288.5801    BREAK
		CASE 45    vSpawnPoint 	= <<-94.7489, 2799.5674, 52.3318>>    fSpawnHeading 	= 288.5801    BREAK
		CASE 46    vSpawnPoint 	= <<-94.2072, 2797.9561, 52.3576>>    fSpawnHeading 	= 288.5801    BREAK
		CASE 47    vSpawnPoint 	= <<-93.6655, 2796.3447, 52.3362>>    fSpawnHeading 	= 288.5801    BREAK
		CASE 48    vSpawnPoint 	= <<-93.1238, 2794.7334, 52.3041>>    fSpawnHeading 	= 288.5801    BREAK
		CASE 49    vSpawnPoint 	= <<-94.7625, 2804.9431, 52.3176>>    fSpawnHeading 	= 288.5801    BREAK
		CASE 50    vSpawnPoint 	= <<-94.2208, 2803.3318, 52.4103>>    fSpawnHeading 	= 288.5801    BREAK
		CASE 51    vSpawnPoint 	= <<-93.6791, 2801.7205, 52.3602>>    fSpawnHeading 	= 288.5801    BREAK
		CASE 52    vSpawnPoint 	= <<-93.1375, 2800.1091, 52.3403>>    fSpawnHeading 	= 288.5801    BREAK
		CASE 53    vSpawnPoint 	= <<-92.5958, 2798.4978, 52.3556>>    fSpawnHeading 	= 288.5801    BREAK
		CASE 54    vSpawnPoint 	= <<-92.0541, 2796.8865, 52.3403>>    fSpawnHeading 	= 288.5801    BREAK
		CASE 55    vSpawnPoint 	= <<-91.5124, 2795.2751, 52.3108>>    fSpawnHeading 	= 288.5801    BREAK
		CASE 56    vSpawnPoint 	= <<-93.1511, 2805.4849, 52.3523>>    fSpawnHeading 	= 288.5801    BREAK
		CASE 57    vSpawnPoint 	= <<-92.6094, 2803.8735, 52.3866>>    fSpawnHeading 	= 288.5801    BREAK
		CASE 58    vSpawnPoint 	= <<-92.0677, 2802.2622, 52.3602>>    fSpawnHeading 	= 288.5801    BREAK
		CASE 59    vSpawnPoint 	= <<-98.5020, 2788.7732, 53.2953>>    fSpawnHeading 	= 288.5801    BREAK
		DEFAULT RETURN FALSE
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL GET_LA_MESA_POST_SCENE_SPAWN_POINT(INT iSpawnPoint, VECTOR &vSpawnPoint, FLOAT &fSpawnHeading)
	SWITCH iSpawnPoint
		CASE 0    vSpawnPoint 	= <<934.5687, -1802.8246, 29.7523>>    fSpawnHeading 	= 264.6078    BREAK
		CASE 1    vSpawnPoint 	= <<934.4090, -1804.5171, 29.7618>>    fSpawnHeading 	= 264.6078    BREAK
		CASE 2    vSpawnPoint 	= <<934.2493, -1806.2096, 29.7755>>    fSpawnHeading 	= 264.6078    BREAK
		CASE 3    vSpawnPoint 	= <<934.0895, -1807.9021, 29.7890>>    fSpawnHeading 	= 264.6078    BREAK
		CASE 4    vSpawnPoint 	= <<933.9298, -1809.5946, 29.7895>>    fSpawnHeading 	= 264.6078    BREAK
		CASE 5    vSpawnPoint 	= <<933.7701, -1811.2871, 29.7900>>    fSpawnHeading 	= 264.6078    BREAK
		CASE 6    vSpawnPoint 	= <<933.6104, -1812.9796, 29.7905>>    fSpawnHeading 	= 264.6078    BREAK
		CASE 7    vSpawnPoint 	= <<933.4506, -1814.6721, 29.7989>>    fSpawnHeading 	= 264.6078    BREAK
		CASE 8    vSpawnPoint 	= <<933.2909, -1816.3646, 29.8077>>    fSpawnHeading 	= 264.6078    BREAK
		CASE 9    vSpawnPoint 	= <<933.1312, -1818.0571, 29.8060>>    fSpawnHeading 	= 264.6078    BREAK
		CASE 10    vSpawnPoint 	= <<932.9714, -1819.7496, 29.8033>>    fSpawnHeading 	= 264.6078    BREAK
		CASE 11    vSpawnPoint 	= <<936.2612, -1802.9844, 29.8018>>    fSpawnHeading 	= 264.6078    BREAK
		CASE 12    vSpawnPoint 	= <<936.1015, -1804.6769, 29.8115>>    fSpawnHeading 	= 264.6078    BREAK
		CASE 13    vSpawnPoint 	= <<935.9418, -1806.3694, 29.8253>>    fSpawnHeading 	= 264.6078    BREAK
		CASE 14    vSpawnPoint 	= <<935.7820, -1808.0619, 29.8390>>    fSpawnHeading 	= 264.6078    BREAK
		CASE 15    vSpawnPoint 	= <<935.6223, -1809.7544, 29.8524>>    fSpawnHeading 	= 264.6078    BREAK
		CASE 16    vSpawnPoint 	= <<935.4626, -1811.4469, 29.8529>>    fSpawnHeading 	= 264.6078    BREAK
		CASE 17    vSpawnPoint 	= <<935.3029, -1813.1394, 29.8618>>    fSpawnHeading 	= 264.6078    BREAK
		CASE 18    vSpawnPoint 	= <<935.1431, -1814.8319, 29.8706>>    fSpawnHeading 	= 264.6078    BREAK
		CASE 19    vSpawnPoint 	= <<934.9834, -1816.5244, 29.8794>>    fSpawnHeading 	= 264.6078    BREAK
		CASE 20    vSpawnPoint 	= <<934.8237, -1818.2169, 29.8776>>    fSpawnHeading 	= 264.6078    BREAK
		CASE 21    vSpawnPoint 	= <<934.6639, -1819.9094, 29.8756>>    fSpawnHeading 	= 264.6078    BREAK
		CASE 22    vSpawnPoint 	= <<937.9537, -1803.1442, 29.8940>>    fSpawnHeading 	= 264.6078    BREAK
		CASE 23    vSpawnPoint 	= <<937.7940, -1804.8367, 29.9028>>    fSpawnHeading 	= 264.6078    BREAK
		CASE 24    vSpawnPoint 	= <<937.6343, -1806.5292, 29.9146>>    fSpawnHeading 	= 264.6078    BREAK
		CASE 25    vSpawnPoint 	= <<937.4745, -1808.2217, 29.9263>>    fSpawnHeading 	= 264.6078    BREAK
		CASE 26    vSpawnPoint 	= <<937.3148, -1809.9142, 29.9372>>    fSpawnHeading 	= 264.6078    BREAK
		CASE 27    vSpawnPoint 	= <<937.1551, -1811.6067, 29.9402>>    fSpawnHeading 	= 264.6078    BREAK
		CASE 28    vSpawnPoint 	= <<936.9954, -1813.2992, 29.9481>>    fSpawnHeading 	= 264.6078    BREAK
		CASE 29    vSpawnPoint 	= <<936.8356, -1814.9917, 29.9560>>    fSpawnHeading 	= 264.6078    BREAK
		CASE 30    vSpawnPoint 	= <<936.6759, -1816.6842, 29.9597>>    fSpawnHeading 	= 264.6078    BREAK
		CASE 31    vSpawnPoint 	= <<936.5162, -1818.3767, 29.9573>>    fSpawnHeading 	= 264.6078    BREAK
		CASE 32    vSpawnPoint 	= <<936.3564, -1820.0692, 29.9548>>    fSpawnHeading 	= 264.6078    BREAK
		CASE 33    vSpawnPoint 	= <<939.6462, -1803.3040, 30.0127>>    fSpawnHeading 	= 264.6078    BREAK
		CASE 34    vSpawnPoint 	= <<939.4865, -1804.9965, 30.0216>>    fSpawnHeading 	= 264.6078    BREAK
		CASE 35    vSpawnPoint 	= <<939.3268, -1806.6890, 30.0333>>    fSpawnHeading 	= 264.6078    BREAK
		CASE 36    vSpawnPoint 	= <<939.1671, -1808.3815, 30.0437>>    fSpawnHeading 	= 264.6078    BREAK
		CASE 37    vSpawnPoint 	= <<939.0073, -1810.0740, 30.0411>>    fSpawnHeading 	= 264.6078    BREAK
		CASE 38    vSpawnPoint 	= <<938.8476, -1811.7665, 30.0441>>    fSpawnHeading 	= 264.6078    BREAK
		CASE 39    vSpawnPoint 	= <<938.6879, -1813.4590, 30.0520>>    fSpawnHeading 	= 264.6078    BREAK
		CASE 40    vSpawnPoint 	= <<938.5281, -1815.1515, 30.0552>>    fSpawnHeading 	= 264.6078    BREAK
		CASE 41    vSpawnPoint 	= <<938.3684, -1816.8440, 30.0529>>    fSpawnHeading 	= 264.6078    BREAK
		CASE 42    vSpawnPoint 	= <<938.2087, -1818.5365, 30.0505>>    fSpawnHeading 	= 264.6078    BREAK
		CASE 43    vSpawnPoint 	= <<938.0490, -1820.2290, 30.0481>>    fSpawnHeading 	= 264.6078    BREAK
		CASE 44    vSpawnPoint 	= <<941.3387, -1803.4637, 30.1314>>    fSpawnHeading 	= 264.6078    BREAK
		CASE 45    vSpawnPoint 	= <<941.1790, -1805.1563, 30.1403>>    fSpawnHeading 	= 264.6078    BREAK
		CASE 46    vSpawnPoint 	= <<941.0193, -1806.8488, 30.1501>>    fSpawnHeading 	= 264.6078    BREAK
		CASE 47    vSpawnPoint 	= <<940.8596, -1808.5413, 30.1475>>    fSpawnHeading 	= 264.6078    BREAK
		CASE 48    vSpawnPoint 	= <<940.6998, -1810.2338, 30.1449>>    fSpawnHeading 	= 264.6078    BREAK
		CASE 49    vSpawnPoint 	= <<940.5401, -1811.9263, 30.1480>>    fSpawnHeading 	= 264.6078    BREAK
		CASE 50    vSpawnPoint 	= <<940.3804, -1813.6188, 30.1507>>    fSpawnHeading 	= 264.6078    BREAK
		CASE 51    vSpawnPoint 	= <<940.2206, -1815.3113, 30.1484>>    fSpawnHeading 	= 264.6078    BREAK
		CASE 52    vSpawnPoint 	= <<940.0609, -1817.0038, 30.1461>>    fSpawnHeading 	= 264.6078    BREAK
		CASE 53    vSpawnPoint 	= <<939.9012, -1818.6963, 30.1437>>    fSpawnHeading 	= 264.6078    BREAK
		CASE 54    vSpawnPoint 	= <<939.7415, -1820.3888, 30.1413>>    fSpawnHeading 	= 264.6078    BREAK
		CASE 55    vSpawnPoint 	= <<943.0313, -1803.6235, 30.2239>>    fSpawnHeading 	= 264.6078    BREAK
		CASE 56    vSpawnPoint 	= <<942.8715, -1805.3160, 30.2354>>    fSpawnHeading 	= 264.6078    BREAK
		CASE 57    vSpawnPoint 	= <<942.7118, -1807.0085, 30.2359>>    fSpawnHeading 	= 264.6078    BREAK
		CASE 58    vSpawnPoint 	= <<942.5521, -1808.7010, 30.2364>>    fSpawnHeading 	= 264.6078    BREAK
		CASE 59    vSpawnPoint 	= <<942.3923, -1810.3936, 30.2370>>    fSpawnHeading 	= 264.6078    BREAK
		DEFAULT RETURN FALSE
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL GET_STRAWBERRY_POST_SCENE_SPAWN_POINT(INT iSpawnPoint, VECTOR &vSpawnPoint, FLOAT &fSpawnHeading)
	SWITCH iSpawnPoint
		CASE 0    vSpawnPoint 	= <<-149.4418, -1405.0439, 29.3949>>    fSpawnHeading 	= 34.1473    BREAK
		CASE 1    vSpawnPoint 	= <<-148.0349, -1404.0897, 29.3186>>    fSpawnHeading 	= 34.1473    BREAK
		CASE 2    vSpawnPoint 	= <<-146.6280, -1403.1355, 29.2385>>    fSpawnHeading 	= 34.1473    BREAK
		CASE 3    vSpawnPoint 	= <<-145.2211, -1402.1813, 29.1816>>    fSpawnHeading 	= 34.1473    BREAK
		CASE 4    vSpawnPoint 	= <<-143.8141, -1401.2271, 29.1198>>    fSpawnHeading 	= 34.1473    BREAK
		CASE 5    vSpawnPoint 	= <<-142.4072, -1400.2728, 29.0409>>    fSpawnHeading 	= 34.1473    BREAK
		CASE 6    vSpawnPoint 	= <<-141.0003, -1399.3186, 28.9616>>    fSpawnHeading 	= 34.1473    BREAK
		CASE 7    vSpawnPoint 	= <<-139.5934, -1398.3644, 28.8665>>    fSpawnHeading 	= 34.1473    BREAK
		CASE 8    vSpawnPoint 	= <<-138.1865, -1397.4102, 28.8192>>    fSpawnHeading 	= 34.1473    BREAK
		CASE 9    vSpawnPoint 	= <<-136.7795, -1396.4559, 28.7723>>    fSpawnHeading 	= 34.1473    BREAK
		CASE 10    vSpawnPoint 	= <<-135.3726, -1395.5017, 28.7677>>    fSpawnHeading 	= 34.1473    BREAK
		CASE 11    vSpawnPoint 	= <<-133.9657, -1394.5475, 28.7298>>    fSpawnHeading 	= 34.1473    BREAK
		CASE 12    vSpawnPoint 	= <<-132.5588, -1393.5933, 28.7161>>    fSpawnHeading 	= 34.1473    BREAK
		CASE 13    vSpawnPoint 	= <<-131.1519, -1392.6390, 28.6615>>    fSpawnHeading 	= 34.1473    BREAK
		CASE 14    vSpawnPoint 	= <<-129.7449, -1391.6848, 28.6046>>    fSpawnHeading 	= 34.1473    BREAK
		CASE 15    vSpawnPoint 	= <<-128.3380, -1390.7306, 28.5505>>    fSpawnHeading 	= 34.1473    BREAK
		CASE 16    vSpawnPoint 	= <<-150.3961, -1403.6371, 29.3788>>    fSpawnHeading 	= 34.1473    BREAK
		CASE 17    vSpawnPoint 	= <<-148.9892, -1402.6829, 29.3045>>    fSpawnHeading 	= 34.1473    BREAK
		CASE 18    vSpawnPoint 	= <<-147.5822, -1401.7286, 29.2425>>    fSpawnHeading 	= 34.1473    BREAK
		CASE 19    vSpawnPoint 	= <<-146.1753, -1400.7744, 29.1859>>    fSpawnHeading 	= 34.1473    BREAK
		CASE 20    vSpawnPoint 	= <<-144.7684, -1399.8202, 29.1241>>    fSpawnHeading 	= 34.1473    BREAK
		CASE 21    vSpawnPoint 	= <<-143.3615, -1398.8660, 29.0623>>    fSpawnHeading 	= 34.1473    BREAK
		CASE 22    vSpawnPoint 	= <<-141.9546, -1397.9117, 28.9860>>    fSpawnHeading 	= 34.1473    BREAK
		CASE 23    vSpawnPoint 	= <<-140.5476, -1396.9575, 28.8774>>    fSpawnHeading 	= 34.1473    BREAK
		CASE 24    vSpawnPoint 	= <<-139.1407, -1396.0033, 28.8129>>    fSpawnHeading 	= 34.1473    BREAK
		CASE 25    vSpawnPoint 	= <<-137.7338, -1395.0491, 28.7659>>    fSpawnHeading 	= 34.1473    BREAK
		CASE 26    vSpawnPoint 	= <<-136.3269, -1394.0948, 28.7900>>    fSpawnHeading 	= 34.1473    BREAK
		CASE 27    vSpawnPoint 	= <<-134.9200, -1393.1406, 28.7519>>    fSpawnHeading 	= 34.1473    BREAK
		CASE 28    vSpawnPoint 	= <<-133.5130, -1392.1864, 28.7127>>    fSpawnHeading 	= 34.1473    BREAK
		CASE 29    vSpawnPoint 	= <<-132.1061, -1391.2322, 28.6734>>    fSpawnHeading 	= 34.1473    BREAK
		CASE 30    vSpawnPoint 	= <<-130.6992, -1390.2780, 28.6264>>    fSpawnHeading 	= 34.1473    BREAK
		CASE 31    vSpawnPoint 	= <<-129.2923, -1389.3237, 28.5664>>    fSpawnHeading 	= 34.1473    BREAK
		CASE 32    vSpawnPoint 	= <<-151.3503, -1402.2302, 29.3627>>    fSpawnHeading 	= 34.1473    BREAK
		CASE 33    vSpawnPoint 	= <<-149.9434, -1401.2760, 29.3025>>    fSpawnHeading 	= 34.1473    BREAK
		CASE 34    vSpawnPoint 	= <<-148.5365, -1400.3218, 29.2465>>    fSpawnHeading 	= 34.1473    BREAK
		CASE 35    vSpawnPoint 	= <<-147.1296, -1399.3676, 29.1902>>    fSpawnHeading 	= 34.1473    BREAK
		CASE 36    vSpawnPoint 	= <<-145.7227, -1398.4133, 29.1284>>    fSpawnHeading 	= 34.1473    BREAK
		CASE 37    vSpawnPoint 	= <<-144.3157, -1397.4591, 29.0666>>    fSpawnHeading 	= 34.1473    BREAK
		CASE 38    vSpawnPoint 	= <<-142.9088, -1396.5049, 29.0048>>    fSpawnHeading 	= 34.1473    BREAK
		CASE 39    vSpawnPoint 	= <<-141.5019, -1395.5507, 28.9041>>    fSpawnHeading 	= 34.1473    BREAK
		CASE 40    vSpawnPoint 	= <<-140.0950, -1394.5964, 28.8066>>    fSpawnHeading 	= 34.1473    BREAK
		CASE 41    vSpawnPoint 	= <<-138.6880, -1393.6422, 28.7750>>    fSpawnHeading 	= 34.1473    BREAK
		CASE 42    vSpawnPoint 	= <<-137.2811, -1392.6880, 28.8113>>    fSpawnHeading 	= 34.1473    BREAK
		CASE 43    vSpawnPoint 	= <<-135.8742, -1391.7338, 28.7711>>    fSpawnHeading 	= 34.1473    BREAK
		CASE 44    vSpawnPoint 	= <<-134.4673, -1390.7795, 28.7308>>    fSpawnHeading 	= 34.1473    BREAK
		CASE 45    vSpawnPoint 	= <<-133.0604, -1389.8253, 28.5083>>    fSpawnHeading 	= 34.1473    BREAK
		CASE 46    vSpawnPoint 	= <<-131.6534, -1388.8711, 28.4930>>    fSpawnHeading 	= 34.1473    BREAK
		CASE 47    vSpawnPoint 	= <<-130.2465, -1387.9169, 28.4428>>    fSpawnHeading 	= 34.1473    BREAK
		CASE 48    vSpawnPoint 	= <<-152.3046, -1400.8234, 29.2086>>    fSpawnHeading 	= 34.1473    BREAK
		CASE 49    vSpawnPoint 	= <<-150.8977, -1399.8691, 29.1569>>    fSpawnHeading 	= 34.1473    BREAK
		CASE 50    vSpawnPoint 	= <<-149.4908, -1398.9149, 29.1051>>    fSpawnHeading 	= 34.1473    BREAK
		CASE 51    vSpawnPoint 	= <<-148.0838, -1397.9607, 29.0533>>    fSpawnHeading 	= 34.1473    BREAK
		CASE 52    vSpawnPoint 	= <<-146.6769, -1397.0065, 28.9959>>    fSpawnHeading 	= 34.1473    BREAK
		CASE 53    vSpawnPoint 	= <<-145.2700, -1396.0522, 28.9383>>    fSpawnHeading 	= 34.1473    BREAK
		CASE 54    vSpawnPoint 	= <<-143.8631, -1395.0980, 28.8807>>    fSpawnHeading 	= 34.1473    BREAK
		CASE 55    vSpawnPoint 	= <<-142.4561, -1394.1438, 28.8301>>    fSpawnHeading 	= 34.1473    BREAK
		CASE 56    vSpawnPoint 	= <<-141.0492, -1393.1896, 28.7678>>    fSpawnHeading 	= 34.1473    BREAK
		CASE 57    vSpawnPoint 	= <<-139.6423, -1392.2354, 28.7135>>    fSpawnHeading 	= 34.1473    BREAK
		CASE 58    vSpawnPoint 	= <<-138.2354, -1391.2811, 28.6676>>    fSpawnHeading 	= 34.1473    BREAK
		CASE 59    vSpawnPoint 	= <<-136.8285, -1390.3269, 28.6283>>    fSpawnHeading 	= 34.1473    BREAK
		DEFAULT RETURN FALSE
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL GET_PALETO_COVE_DOCK_POST_SCENE_SPAWN_POINT(INT iSpawnPoint, VECTOR &vSpawnPoint, FLOAT &fSpawnHeading)
	SWITCH iSpawnPoint
		CASE 0	vSpawnPoint 	= <<-1611.4316, 5259.5303, 2.9791>>	fSpawnHeading 	= 203.7600	RETURN TRUE	BREAK
		CASE 1	vSpawnPoint 	= <<-1613.1637, 5260.0693, 2.9791>>	fSpawnHeading 	= 203.7600	RETURN TRUE	BREAK
		CASE 2	vSpawnPoint 	= <<-1610.8409, 5261.0786, 2.9791>>	fSpawnHeading 	= 203.7600	RETURN TRUE	BREAK
		CASE 3	vSpawnPoint 	= <<-1614.0294, 5258.6001, 2.9791>>	fSpawnHeading 	= 203.7600	RETURN TRUE	BREAK
		CASE 4	vSpawnPoint 	= <<-1609.3048, 5260.6450, 2.9791>>	fSpawnHeading 	= 203.7600	RETURN TRUE	BREAK
		CASE 5	vSpawnPoint 	= <<-1607.9646, 5258.0723, 2.9791>>	fSpawnHeading 	= 203.7600	RETURN TRUE	BREAK
		CASE 6	vSpawnPoint 	= <<-1612.7306, 5255.7368, 2.9791>>	fSpawnHeading 	= 203.7600	RETURN TRUE	BREAK
		CASE 7	vSpawnPoint 	= <<-1612.3020, 5257.8081, 2.9791>>	fSpawnHeading 	= 203.7600	RETURN TRUE	BREAK
		CASE 8	vSpawnPoint 	= <<-1610.5160, 5257.2397, 2.9791>>	fSpawnHeading 	= 203.7600	RETURN TRUE	BREAK
		CASE 9	vSpawnPoint 	= <<-1609.6614, 5258.8765, 2.9791>>	fSpawnHeading 	= 203.7600	RETURN TRUE	BREAK
		CASE 10	vSpawnPoint 	= <<-1607.3303, 5256.8960, 2.9791>>	fSpawnHeading 	= 203.7600	RETURN TRUE	BREAK
		CASE 11	vSpawnPoint 	= <<-1609.4845, 5255.2720, 2.9791>>	fSpawnHeading 	= 203.7600	RETURN TRUE	BREAK
		CASE 12	vSpawnPoint 	= <<-1609.5530, 5252.9917, 2.9791>>	fSpawnHeading 	= 203.7600	RETURN TRUE	BREAK
		CASE 13	vSpawnPoint 	= <<-1611.5170, 5253.8359, 2.9791>>	fSpawnHeading 	= 203.7600	RETURN TRUE	BREAK
		CASE 14	vSpawnPoint 	= <<-1607.9380, 5255.3647, 2.9791>>	fSpawnHeading 	= 203.7600	RETURN TRUE	BREAK
		CASE 15	vSpawnPoint 	= <<-1606.3608, 5254.6016, 2.9791>>	fSpawnHeading 	= 203.7600	RETURN TRUE	BREAK
		CASE 16	vSpawnPoint 	= <<-1607.8608, 5253.3379, 2.9791>>	fSpawnHeading 	= 203.7600	RETURN TRUE	BREAK
		CASE 17	vSpawnPoint 	= <<-1609.2957, 5250.4922, 2.9791>>	fSpawnHeading 	= 203.7600	RETURN TRUE	BREAK
		CASE 18	vSpawnPoint 	= <<-1610.5538, 5251.9180, 2.9791>>	fSpawnHeading 	= 203.7600	RETURN TRUE	BREAK
		CASE 19	vSpawnPoint 	= <<-1608.0438, 5251.2905, 2.9791>>	fSpawnHeading 	= 203.7600	RETURN TRUE	BREAK
		CASE 20	vSpawnPoint 	= <<-1608.0750, 5249.6401, 2.9791>>	fSpawnHeading 	= 203.7600	RETURN TRUE	BREAK
		CASE 21	vSpawnPoint 	= <<-1609.1597, 5248.9282, 2.9791>>	fSpawnHeading 	= 203.7600	RETURN TRUE	BREAK
		CASE 22	vSpawnPoint 	= <<-1606.7708, 5250.2090, 2.9791>>	fSpawnHeading 	= 203.7600	RETURN TRUE	BREAK
		CASE 23	vSpawnPoint 	= <<-1606.3810, 5252.1631, 2.9791>>	fSpawnHeading 	= 203.7600	RETURN TRUE	BREAK
		CASE 24	vSpawnPoint 	= <<-1605.1121, 5251.1997, 2.9791>>	fSpawnHeading 	= 203.7600	RETURN TRUE	BREAK
		CASE 25	vSpawnPoint 	= <<-1603.7267, 5249.0122, 2.9791>>	fSpawnHeading 	= 203.7600	RETURN TRUE	BREAK
		CASE 26	vSpawnPoint 	= <<-1605.4659, 5248.4639, 2.9791>>	fSpawnHeading 	= 203.7600	RETURN TRUE	BREAK
		CASE 27	vSpawnPoint 	= <<-1607.8718, 5244.8252, 3.0033>>	fSpawnHeading 	= 203.7600	RETURN TRUE	BREAK
		CASE 28	vSpawnPoint 	= <<-1607.4706, 5247.3833, 2.9791>>	fSpawnHeading 	= 203.7600	RETURN TRUE	BREAK
		CASE 29	vSpawnPoint 	= <<-1605.9164, 5243.5068, 2.9791>>	fSpawnHeading 	= 203.7600	RETURN TRUE	BREAK
		CASE 30	vSpawnPoint 	= <<-1601.9214, 5242.0698, 2.9791>>	fSpawnHeading 	= 203.7600	RETURN TRUE	BREAK
		CASE 31	vSpawnPoint 	= <<-1603.9755, 5242.1646, 2.9791>>	fSpawnHeading 	= 203.7600	RETURN TRUE	BREAK
		CASE 32	vSpawnPoint 	= <<-1603.7052, 5244.4082, 2.9970>>	fSpawnHeading 	= 203.7600	RETURN TRUE	BREAK
		CASE 33	vSpawnPoint 	= <<-1603.1008, 5239.8848, 2.9791>>	fSpawnHeading 	= 203.7600	RETURN TRUE	BREAK
		CASE 34	vSpawnPoint 	= <<-1601.9927, 5244.6816, 2.9791>>	fSpawnHeading 	= 203.7600	RETURN TRUE	BREAK
		CASE 35	vSpawnPoint 	= <<-1601.7040, 5236.5903, 2.9791>>	fSpawnHeading 	= 203.7600	RETURN TRUE	BREAK
		CASE 36	vSpawnPoint 	= <<-1604.3221, 5238.7793, 2.9791>>	fSpawnHeading 	= 203.7600	RETURN TRUE	BREAK
		CASE 37	vSpawnPoint 	= <<-1600.8124, 5238.2559, 2.9791>>	fSpawnHeading 	= 203.7600	RETURN TRUE	BREAK
		CASE 38	vSpawnPoint 	= <<-1605.4824, 5240.9463, 2.9791>>	fSpawnHeading 	= 203.7600	RETURN TRUE	BREAK
		CASE 39	vSpawnPoint 	= <<-1600.2213, 5240.4351, 2.9791>>	fSpawnHeading 	= 203.7600	RETURN TRUE	BREAK
		CASE 40	vSpawnPoint 	= <<-1600.1974, 5232.8765, 2.9791>>	fSpawnHeading 	= 203.7600	RETURN TRUE	BREAK
		CASE 41	vSpawnPoint 	= <<-1601.4580, 5231.6074, 2.9791>>	fSpawnHeading 	= 203.7600	RETURN TRUE	BREAK
		CASE 42	vSpawnPoint 	= <<-1599.4509, 5235.2988, 2.9791>>	fSpawnHeading 	= 203.7600	RETURN TRUE	BREAK
		CASE 43	vSpawnPoint 	= <<-1603.7236, 5236.8809, 2.9791>>	fSpawnHeading 	= 203.7600	RETURN TRUE	BREAK
		CASE 44	vSpawnPoint 	= <<-1598.5618, 5237.7378, 2.9791>>	fSpawnHeading 	= 203.7600	RETURN TRUE	BREAK
		DEFAULT RETURN FALSE
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL GET_ELYSIAN_ISLAND_SHIPYARD_POST_SCENE_SPAWN_POINT(INT iSpawnPoint, VECTOR &vSpawnPoint, FLOAT &fSpawnHeading)
	SWITCH iSpawnPoint
		CASE 0	vSpawnPoint 	= <<-154.9186, -2676.3420, 5.0107>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 1	vSpawnPoint 	= <<-152.9835, -2677.6506, 5.0104>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 2	vSpawnPoint 	= <<-157.0320, -2677.2280, 5.0115>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 3	vSpawnPoint 	= <<-151.2652, -2675.8914, 5.0069>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 4	vSpawnPoint 	= <<-158.7026, -2675.8220, 5.0102>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 5	vSpawnPoint 	= <<-147.3867, -2677.8394, 5.0017>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 6	vSpawnPoint 	= <<-162.1549, -2677.1282, 5.0134>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 7	vSpawnPoint 	= <<-152.2628, -2673.9934, 5.0061>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 8	vSpawnPoint 	= <<-150.1857, -2673.5503, 5.0042>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 9	vSpawnPoint 	= <<-160.7722, -2675.1362, 5.0095>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 10	vSpawnPoint 	= <<-148.0512, -2674.8789, 5.0013>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 11	vSpawnPoint 	= <<-160.6559, -2673.3628, 5.0078>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 12	vSpawnPoint 	= <<-146.4116, -2673.7852, 5.0006>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 13	vSpawnPoint 	= <<-163.2567, -2673.4478, 5.0079>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 14	vSpawnPoint 	= <<-154.9186, -2672.3420, 5.0060>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 15	vSpawnPoint 	= <<-152.9186, -2672.3420, 5.0041>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 16	vSpawnPoint 	= <<-156.9186, -2672.3420, 5.0438>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 17	vSpawnPoint 	= <<-150.9186, -2672.3420, 5.0038>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 18	vSpawnPoint 	= <<-158.9186, -2672.3420, 5.0068>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 19	vSpawnPoint 	= <<-148.9186, -2672.3420, 5.0017>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 20	vSpawnPoint 	= <<-162.7836, -2671.2747, 5.0057>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 21	vSpawnPoint 	= <<-154.9186, -2670.3420, 5.0032>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 22	vSpawnPoint 	= <<-152.9186, -2670.3420, 5.0014>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 23	vSpawnPoint 	= <<-156.9186, -2670.3420, 5.0438>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 24	vSpawnPoint 	= <<-150.9186, -2670.3420, 5.0012>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 25	vSpawnPoint 	= <<-158.9186, -2670.3420, 5.0048>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 26	vSpawnPoint 	= <<-148.9186, -2670.3420, 5.0009>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 27	vSpawnPoint 	= <<-160.9186, -2670.3420, 5.0048>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 28	vSpawnPoint 	= <<-154.9186, -2668.3420, 5.0019>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 29	vSpawnPoint 	= <<-152.9186, -2668.3420, 5.0016>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 30	vSpawnPoint 	= <<-156.9186, -2668.3420, 5.0018>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 31	vSpawnPoint 	= <<-150.9186, -2668.3420, 5.0013>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 32	vSpawnPoint 	= <<-158.9186, -2668.3420, 5.0029>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 33	vSpawnPoint 	= <<-148.9186, -2668.3420, 5.0011>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 34	vSpawnPoint 	= <<-160.9186, -2668.3420, 5.0029>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 35	vSpawnPoint 	= <<-154.9186, -2666.3420, 5.0010>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 36	vSpawnPoint 	= <<-152.9186, -2666.3420, 5.0002>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 37	vSpawnPoint 	= <<-156.9186, -2666.3420, 5.0010>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 38	vSpawnPoint 	= <<-150.9186, -2666.3420, 5.0002>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 39	vSpawnPoint 	= <<-158.9186, -2666.3420, 5.0010>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 40	vSpawnPoint 	= <<-148.9186, -2666.3420, 5.0002>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 41	vSpawnPoint 	= <<-160.9186, -2666.3420, 5.0010>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 42	vSpawnPoint 	= <<-154.9186, -2664.3420, 5.0010>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 43	vSpawnPoint 	= <<-152.9186, -2664.3420, 5.0002>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 44	vSpawnPoint 	= <<-156.9186, -2664.3420, 5.0010>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 45	vSpawnPoint 	= <<-150.9186, -2664.3420, 5.0002>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 46	vSpawnPoint 	= <<-158.9186, -2664.3420, 5.0010>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 47	vSpawnPoint 	= <<-148.9186, -2664.3420, 5.0002>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 48	vSpawnPoint 	= <<-160.9186, -2664.3420, 5.0010>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 49	vSpawnPoint 	= <<-154.9186, -2662.3420, 5.0002>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 50	vSpawnPoint 	= <<-152.9186, -2662.3420, 5.0002>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 51	vSpawnPoint 	= <<-156.9186, -2662.3420, 5.0010>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 52	vSpawnPoint 	= <<-150.9186, -2662.3420, 5.0002>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 53	vSpawnPoint 	= <<-158.9186, -2662.3420, 5.0010>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 54	vSpawnPoint 	= <<-148.9186, -2662.3420, 5.0002>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 55	vSpawnPoint 	= <<-160.9186, -2662.3420, 5.0010>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 56	vSpawnPoint 	= <<-154.9186, -2660.3420, 5.0002>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 57	vSpawnPoint 	= <<-152.9186, -2660.3420, 5.0002>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 58	vSpawnPoint 	= <<-156.9186, -2660.3420, 5.0002>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 59	vSpawnPoint 	= <<-150.9186, -2660.3420, 5.0002>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 60	vSpawnPoint 	= <<-158.9186, -2660.3420, 5.0010>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 61	vSpawnPoint 	= <<-148.9186, -2660.3420, 5.0002>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 62	vSpawnPoint 	= <<-160.9186, -2660.3420, 5.0010>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 63	vSpawnPoint 	= <<-154.9186, -2658.3420, 5.0002>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 64	vSpawnPoint 	= <<-152.9186, -2658.3420, 5.0002>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 65	vSpawnPoint 	= <<-156.9186, -2658.3420, 5.0002>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 66	vSpawnPoint 	= <<-150.9186, -2658.3420, 5.0002>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 67	vSpawnPoint 	= <<-158.9186, -2658.3420, 5.0002>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 68	vSpawnPoint 	= <<-148.9186, -2658.3420, 5.0002>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 69	vSpawnPoint 	= <<-160.9186, -2658.3420, 5.0010>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 70	vSpawnPoint 	= <<-154.9186, -2656.3420, 5.0059>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		CASE 71	vSpawnPoint 	= <<-152.9186, -2656.3420, 5.0010>>	fSpawnHeading 	= 0.0000	RETURN TRUE	BREAK
		DEFAULT RETURN FALSE
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL GET_FMC_DOCKS_POST_SCENE_SPAWN_POINT(INT iSpawnPoint, VECTOR &vSpawnPoint, FLOAT &fSpawnHeading)
	SWITCH iSpawnPoint
		CASE 0	vSpawnPoint 	= <<1179.8842, -2969.9683, 4.9021>>	fSpawnHeading 	= 273.9600	RETURN TRUE	BREAK
		CASE 1	vSpawnPoint 	= <<1179.9878, -2971.4646, 4.9021>>	fSpawnHeading 	= 273.9600	RETURN TRUE	BREAK
		CASE 2	vSpawnPoint 	= <<1179.7805, -2968.4719, 4.9021>>	fSpawnHeading 	= 273.9600	RETURN TRUE	BREAK
		CASE 3	vSpawnPoint 	= <<1180.9005, -2972.9500, 4.9021>>	fSpawnHeading 	= 273.9600	RETURN TRUE	BREAK
		CASE 4	vSpawnPoint 	= <<1179.6769, -2966.9756, 4.9021>>	fSpawnHeading 	= 273.9600	RETURN TRUE	BREAK
		CASE 5	vSpawnPoint 	= <<1180.5901, -2974.4109, 4.9021>>	fSpawnHeading 	= 273.9600	RETURN TRUE	BREAK
		CASE 6	vSpawnPoint 	= <<1179.5732, -2965.4792, 4.9021>>	fSpawnHeading 	= 273.9600	RETURN TRUE	BREAK
		CASE 7	vSpawnPoint 	= <<1180.2987, -2975.9536, 4.9021>>	fSpawnHeading 	= 273.9600	RETURN TRUE	BREAK
		CASE 8	vSpawnPoint 	= <<1179.5195, -2963.6221, 4.9021>>	fSpawnHeading 	= 273.9600	RETURN TRUE	BREAK
		CASE 9	vSpawnPoint 	= <<1180.4023, -2977.4500, 4.9021>>	fSpawnHeading 	= 273.9600	RETURN TRUE	BREAK
		CASE 10	vSpawnPoint 	= <<1181.3806, -2969.8647, 4.9021>>	fSpawnHeading 	= 273.9600	RETURN TRUE	BREAK
		CASE 11	vSpawnPoint 	= <<1181.4843, -2971.3611, 4.9021>>	fSpawnHeading 	= 273.9600	RETURN TRUE	BREAK
		CASE 12	vSpawnPoint 	= <<1181.2770, -2968.3684, 4.9021>>	fSpawnHeading 	= 273.9600	RETURN TRUE	BREAK
		CASE 13	vSpawnPoint 	= <<1182.4806, -2972.9109, 4.9021>>	fSpawnHeading 	= 273.9600	RETURN TRUE	BREAK
		CASE 14	vSpawnPoint 	= <<1181.1733, -2966.8721, 4.9021>>	fSpawnHeading 	= 273.9600	RETURN TRUE	BREAK
		CASE 15	vSpawnPoint 	= <<1182.3357, -2974.4348, 4.9021>>	fSpawnHeading 	= 273.9600	RETURN TRUE	BREAK
		CASE 16	vSpawnPoint 	= <<1181.0697, -2965.3757, 4.9021>>	fSpawnHeading 	= 273.9600	RETURN TRUE	BREAK
		CASE 17	vSpawnPoint 	= <<1181.7952, -2975.8501, 4.9021>>	fSpawnHeading 	= 273.9600	RETURN TRUE	BREAK
		CASE 18	vSpawnPoint 	= <<1183.3728, -2978.9336, 4.9021>>	fSpawnHeading 	= 273.9600	RETURN TRUE	BREAK
		CASE 19	vSpawnPoint 	= <<1181.8988, -2977.3464, 4.9021>>	fSpawnHeading 	= 273.9600	RETURN TRUE	BREAK
		CASE 20	vSpawnPoint 	= <<1182.8771, -2969.7612, 4.9021>>	fSpawnHeading 	= 273.9600	RETURN TRUE	BREAK
		CASE 21	vSpawnPoint 	= <<1182.9807, -2971.2576, 4.9021>>	fSpawnHeading 	= 273.9600	RETURN TRUE	BREAK
		CASE 22	vSpawnPoint 	= <<1182.7734, -2968.2649, 4.9021>>	fSpawnHeading 	= 273.9600	RETURN TRUE	BREAK
		CASE 23	vSpawnPoint 	= <<1183.5938, -2972.7708, 4.9021>>	fSpawnHeading 	= 273.9600	RETURN TRUE	BREAK
		CASE 24	vSpawnPoint 	= <<1182.6698, -2966.7686, 4.9021>>	fSpawnHeading 	= 273.9600	RETURN TRUE	BREAK
		CASE 25	vSpawnPoint 	= <<1183.5519, -2974.1750, 4.9021>>	fSpawnHeading 	= 273.9600	RETURN TRUE	BREAK
		CASE 26	vSpawnPoint 	= <<1182.5662, -2965.2722, 4.9021>>	fSpawnHeading 	= 273.9600	RETURN TRUE	BREAK
		CASE 27	vSpawnPoint 	= <<1183.2916, -2975.7466, 4.9021>>	fSpawnHeading 	= 273.9600	RETURN TRUE	BREAK
		CASE 28	vSpawnPoint 	= <<1182.5313, -2979.7944, 4.9021>>	fSpawnHeading 	= 273.9600	RETURN TRUE	BREAK
		CASE 29	vSpawnPoint 	= <<1183.3953, -2977.2429, 4.9021>>	fSpawnHeading 	= 273.9600	RETURN TRUE	BREAK
		CASE 30	vSpawnPoint 	= <<1184.3735, -2969.6577, 4.9021>>	fSpawnHeading 	= 273.9600	RETURN TRUE	BREAK
		CASE 31	vSpawnPoint 	= <<1184.4772, -2971.1541, 4.9021>>	fSpawnHeading 	= 273.9600	RETURN TRUE	BREAK
		CASE 32	vSpawnPoint 	= <<1184.2699, -2968.1614, 4.9021>>	fSpawnHeading 	= 273.9600	RETURN TRUE	BREAK
		CASE 33	vSpawnPoint 	= <<1185.1837, -2972.6433, 4.9021>>	fSpawnHeading 	= 273.9600	RETURN TRUE	BREAK
		CASE 34	vSpawnPoint 	= <<1184.1663, -2966.6650, 4.9021>>	fSpawnHeading 	= 273.9600	RETURN TRUE	BREAK
		CASE 35	vSpawnPoint 	= <<1185.2205, -2974.1069, 4.9021>>	fSpawnHeading 	= 273.9600	RETURN TRUE	BREAK
		CASE 36	vSpawnPoint 	= <<1184.0626, -2965.1687, 4.9021>>	fSpawnHeading 	= 273.9600	RETURN TRUE	BREAK
		CASE 37	vSpawnPoint 	= <<1184.7881, -2975.6431, 4.9021>>	fSpawnHeading 	= 273.9600	RETURN TRUE	BREAK
		CASE 38	vSpawnPoint 	= <<1181.0248, -2979.6233, 4.9021>>	fSpawnHeading 	= 273.9600	RETURN TRUE	BREAK
		CASE 39	vSpawnPoint 	= <<1184.8917, -2977.1394, 4.9021>>	fSpawnHeading 	= 273.9600	RETURN TRUE	BREAK
		CASE 40	vSpawnPoint 	= <<1185.8700, -2969.5542, 4.9021>>	fSpawnHeading 	= 273.9600	RETURN TRUE	BREAK
		CASE 41	vSpawnPoint 	= <<1185.9736, -2971.0505, 4.9021>>	fSpawnHeading 	= 273.9600	RETURN TRUE	BREAK
		CASE 42	vSpawnPoint 	= <<1185.7664, -2968.0579, 4.9021>>	fSpawnHeading 	= 273.9600	RETURN TRUE	BREAK
		CASE 43	vSpawnPoint 	= <<1186.6943, -2972.5459, 4.9021>>	fSpawnHeading 	= 273.9600	RETURN TRUE	BREAK
		CASE 44	vSpawnPoint 	= <<1185.6627, -2966.5615, 4.9021>>	fSpawnHeading 	= 273.9600	RETURN TRUE	BREAK
		CASE 45	vSpawnPoint 	= <<1186.8273, -2974.0664, 4.9021>>	fSpawnHeading 	= 273.9600	RETURN TRUE	BREAK
		CASE 46	vSpawnPoint 	= <<1185.5591, -2965.0652, 4.9021>>	fSpawnHeading 	= 273.9600	RETURN TRUE	BREAK
		CASE 47	vSpawnPoint 	= <<1186.2845, -2975.5396, 4.9021>>	fSpawnHeading 	= 273.9600	RETURN TRUE	BREAK
		CASE 48	vSpawnPoint 	= <<1182.7483, -2982.2891, 4.9021>>	fSpawnHeading 	= 273.9600	RETURN TRUE	BREAK
		CASE 49	vSpawnPoint 	= <<1186.3882, -2977.0359, 4.9021>>	fSpawnHeading 	= 273.9600	RETURN TRUE	BREAK
		CASE 50	vSpawnPoint 	= <<1187.3665, -2969.4507, 4.9021>>	fSpawnHeading 	= 273.9600	RETURN TRUE	BREAK
		CASE 51	vSpawnPoint 	= <<1187.4701, -2970.9470, 4.9021>>	fSpawnHeading 	= 273.9600	RETURN TRUE	BREAK
		CASE 52	vSpawnPoint 	= <<1187.2628, -2967.9543, 4.9021>>	fSpawnHeading 	= 273.9600	RETURN TRUE	BREAK
		CASE 53	vSpawnPoint 	= <<1181.6018, -2981.4167, 4.9021>>	fSpawnHeading 	= 273.9600	RETURN TRUE	BREAK
		CASE 54	vSpawnPoint 	= <<1187.1592, -2966.4580, 4.9021>>	fSpawnHeading 	= 273.9600	RETURN TRUE	BREAK
		CASE 55	vSpawnPoint 	= <<1185.5358, -2982.2544, 4.9021>>	fSpawnHeading 	= 273.9600	RETURN TRUE	BREAK
		CASE 56	vSpawnPoint 	= <<1185.7189, -2978.9602, 4.9021>>	fSpawnHeading 	= 273.9600	RETURN TRUE	BREAK
		CASE 57	vSpawnPoint 	= <<1187.7810, -2975.4360, 4.9021>>	fSpawnHeading 	= 273.9600	RETURN TRUE	BREAK
		CASE 58	vSpawnPoint 	= <<1184.3988, -2980.2434, 4.9021>>	fSpawnHeading 	= 273.9600	RETURN TRUE	BREAK
		CASE 59	vSpawnPoint 	= <<1186.7546, -2980.7603, 4.9021>>	fSpawnHeading 	= 273.9600	RETURN TRUE	BREAK
		DEFAULT RETURN FALSE
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL GET_FMC_AUTO_SHOP_CUSTOMER_POST_SCENE_SPAWN_POINT(FREEMODE_DELIVERY_DROPOFFS eDropoffID, INT iSpawnPoint, VECTOR &vSpawnPoint, FLOAT &fSpawnHeading)
	SWITCH eDropoffID
		CASE FMC_DROPOFF_AUTO_SHOP_CUSTOMER_H1
			SWITCH iSpawnPoint
				CASE 0    vSpawnPoint 	= <<-546.2589, 494.0297, 103.0195>>    fSpawnHeading 	= 77.7600    BREAK
				CASE 1    vSpawnPoint 	= <<-545.9476, 495.5926, 103.0948>>    fSpawnHeading 	= 77.7600    BREAK
				CASE 2    vSpawnPoint 	= <<-546.7669, 492.5821, 102.9336>>    fSpawnHeading 	= 77.7600    BREAK
				CASE 3    vSpawnPoint 	= <<-546.4385, 496.8913, 103.1388>>    fSpawnHeading 	= 77.7600    BREAK
				CASE 4    vSpawnPoint 	= <<-547.0834, 490.7594, 102.8078>>    fSpawnHeading 	= 77.7600    BREAK
				CASE 5    vSpawnPoint 	= <<-548.1310, 494.4309, 102.9336>>    fSpawnHeading 	= 77.7600    BREAK
				CASE 6    vSpawnPoint 	= <<-547.7528, 497.0690, 103.1192>>    fSpawnHeading 	= 77.7600    BREAK
				CASE 7    vSpawnPoint 	= <<-548.2810, 493.1382, 102.8551>>    fSpawnHeading 	= 77.7693    BREAK
				CASE 8    vSpawnPoint 	= <<-547.4830, 495.2458, 103.0487>>    fSpawnHeading 	= 77.7600    BREAK
				CASE 9    vSpawnPoint 	= <<-548.4042, 491.4344, 102.7764>>    fSpawnHeading 	= 48.9005    BREAK
				CASE 10    vSpawnPoint 	= <<-549.2100, 494.8819, 102.8701>>    fSpawnHeading 	= 102.8456    BREAK
				CASE 11    vSpawnPoint 	= <<-548.4417, 496.2144, 103.0330>>    fSpawnHeading 	= 102.3215    BREAK
				CASE 12    vSpawnPoint 	= <<-549.4996, 492.3578, 102.6911>>    fSpawnHeading 	= 64.2193    BREAK
				CASE 13    vSpawnPoint 	= <<-548.7397, 497.9646, 103.1311>>    fSpawnHeading 	= 85.9124    BREAK
				CASE 14    vSpawnPoint 	= <<-549.9380, 493.9531, 102.7040>>    fSpawnHeading 	= 106.1207    BREAK
				CASE 15    vSpawnPoint 	= <<-550.4314, 497.5323, 102.9610>>    fSpawnHeading 	= 97.9393    BREAK
				CASE 16    vSpawnPoint 	= <<-550.0804, 495.7797, 102.8365>>    fSpawnHeading 	= 102.6010    BREAK
				CASE 17    vSpawnPoint 	= <<-550.9204, 494.7628, 102.6284>>    fSpawnHeading 	= 102.1379    BREAK
				CASE 18    vSpawnPoint 	= <<-549.6957, 496.7508, 102.9660>>    fSpawnHeading 	= 102.3298    BREAK
				CASE 19    vSpawnPoint 	= <<-550.7745, 493.1568, 102.5332>>    fSpawnHeading 	= 107.1717    BREAK
				CASE 20    vSpawnPoint 	= <<-552.2946, 495.6505, 102.6702>>    fSpawnHeading 	= 97.1730    BREAK
				CASE 21    vSpawnPoint 	= <<-551.1799, 496.5644, 102.7781>>    fSpawnHeading 	= 94.2641    BREAK
				CASE 22    vSpawnPoint 	= <<-552.0463, 494.0060, 102.5441>>    fSpawnHeading 	= 102.9851    BREAK
				CASE 23    vSpawnPoint 	= <<-551.3241, 498.3574, 102.9321>>    fSpawnHeading 	= 99.0227    BREAK
				CASE 24    vSpawnPoint 	= <<-551.6782, 492.2589, 102.4477>>    fSpawnHeading 	= 102.5654    BREAK
				CASE 25    vSpawnPoint 	= <<-564.1227, 500.4875, 103.1528>>    fSpawnHeading 	= 294.1949    BREAK
				CASE 26    vSpawnPoint 	= <<-552.4658, 497.4384, 102.8227>>    fSpawnHeading 	= 102.3968    BREAK
				CASE 27    vSpawnPoint 	= <<-561.2325, 520.3223, 105.1957>>    fSpawnHeading 	= 212.8374    BREAK
				CASE 28    vSpawnPoint 	= <<-552.6113, 499.5201, 103.0065>>    fSpawnHeading 	= 98.3544    BREAK
				CASE 29    vSpawnPoint 	= <<-552.7922, 501.9469, 103.2373>>    fSpawnHeading 	= 94.0115    BREAK
				CASE 30    vSpawnPoint 	= <<-570.1929, 518.1334, 105.2342>>    fSpawnHeading 	= 219.7544    BREAK
				CASE 31    vSpawnPoint 	= <<-567.4070, 518.9605, 105.2446>>    fSpawnHeading 	= 220.4688    BREAK
				DEFAULT RETURN FALSE
			ENDSWITCH
		BREAK
		CASE FMC_DROPOFF_AUTO_SHOP_CUSTOMER_H2
			SWITCH iSpawnPoint
				CASE 0    vSpawnPoint 	= <<-991.0334, 789.1226, 171.6771>>    fSpawnHeading 	= 25.2000    BREAK
				CASE 1    vSpawnPoint 	= <<-989.5262, 789.8405, 171.8269>>    fSpawnHeading 	= 37.4955    BREAK
				CASE 2    vSpawnPoint 	= <<-992.5261, 788.8121, 171.5047>>    fSpawnHeading 	= 25.2000    BREAK
				CASE 3    vSpawnPoint 	= <<-988.2609, 790.5891, 171.9529>>    fSpawnHeading 	= 45.7236    BREAK
				CASE 4    vSpawnPoint 	= <<-995.0258, 787.3626, 171.2301>>    fSpawnHeading 	= 25.2000    BREAK
				CASE 5    vSpawnPoint 	= <<-991.7439, 791.0509, 171.5688>>    fSpawnHeading 	= 25.2000    BREAK
				CASE 6    vSpawnPoint 	= <<-990.3627, 790.9854, 171.7341>>    fSpawnHeading 	= 25.2000    BREAK
				CASE 7    vSpawnPoint 	= <<-993.4758, 789.4677, 171.3788>>    fSpawnHeading 	= 25.2000    BREAK
				CASE 8    vSpawnPoint 	= <<-988.8314, 791.7583, 171.9054>>    fSpawnHeading 	= 45.6591    BREAK
				CASE 9    vSpawnPoint 	= <<-995.0121, 788.9412, 171.2029>>    fSpawnHeading 	= 25.2000    BREAK
				CASE 10    vSpawnPoint 	= <<-993.1004, 790.8963, 171.3992>>    fSpawnHeading 	= 25.2000    BREAK
				CASE 11    vSpawnPoint 	= <<-990.9588, 792.2522, 171.6625>>    fSpawnHeading 	= 25.2000    BREAK
				CASE 12    vSpawnPoint 	= <<-994.3532, 790.4034, 171.2563>>    fSpawnHeading 	= 25.2000    BREAK
				CASE 13    vSpawnPoint 	= <<-989.4777, 793.1752, 171.8442>>    fSpawnHeading 	= 25.2000    BREAK
				CASE 14    vSpawnPoint 	= <<-996.4968, 790.0168, 170.9994>>    fSpawnHeading 	= 25.2000    BREAK
				CASE 15    vSpawnPoint 	= <<-993.9147, 792.3286, 171.2731>>    fSpawnHeading 	= 25.2000    BREAK
				CASE 16    vSpawnPoint 	= <<-991.1392, 793.6536, 171.6034>>    fSpawnHeading 	= 25.2000    BREAK
				CASE 17    vSpawnPoint 	= <<-995.0162, 791.4996, 171.1638>>    fSpawnHeading 	= 25.2000    BREAK
				CASE 18    vSpawnPoint 	= <<-990.5740, 794.6957, 171.6802>>    fSpawnHeading 	= 25.2000    BREAK
				CASE 19    vSpawnPoint 	= <<-996.8610, 791.3173, 170.9655>>    fSpawnHeading 	= 25.2000    BREAK
				CASE 20    vSpawnPoint 	= <<-992.6888, 792.5503, 171.4226>>    fSpawnHeading 	= 25.2000    BREAK
				CASE 21    vSpawnPoint 	= <<-994.3065, 793.6458, 171.1261>>    fSpawnHeading 	= 25.2000    BREAK
				CASE 22    vSpawnPoint 	= <<-995.5972, 792.7064, 171.0487>>    fSpawnHeading 	= 25.2000    BREAK
				CASE 23    vSpawnPoint 	= <<-992.8757, 794.1784, 171.3129>>    fSpawnHeading 	= 25.2000    BREAK
				CASE 24    vSpawnPoint 	= <<-997.3876, 792.4154, 170.8890>>    fSpawnHeading 	= 25.2000    BREAK
				CASE 25    vSpawnPoint 	= <<-994.0138, 795.4565, 171.1124>>    fSpawnHeading 	= 25.2000    BREAK
				CASE 26    vSpawnPoint 	= <<-992.5684, 795.5598, 171.3187>>    fSpawnHeading 	= 25.2000    BREAK
				CASE 27    vSpawnPoint 	= <<-995.2806, 794.8604, 171.0096>>    fSpawnHeading 	= 25.2000    BREAK
				CASE 28    vSpawnPoint 	= <<-991.3306, 795.8200, 171.5231>>    fSpawnHeading 	= 25.2000    BREAK
				CASE 29    vSpawnPoint 	= <<-996.7284, 793.9251, 170.9163>>    fSpawnHeading 	= 25.2000    BREAK
				CASE 30    vSpawnPoint 	= <<-996.0389, 805.9272, 171.3559>>    fSpawnHeading 	= 196.0887   BREAK
				CASE 31    vSpawnPoint 	= <<-992.7300, 806.4244, 171.6365>>    fSpawnHeading 	= 201.5534   BREAK
				DEFAULT RETURN FALSE
			ENDSWITCH
		BREAK
		CASE FMC_DROPOFF_AUTO_SHOP_CUSTOMER_H3
			SWITCH iSpawnPoint
				CASE 0    vSpawnPoint 	= <<-1941.4670, 203.8431, 84.2692>>    fSpawnHeading 	= 296.6400    BREAK
				CASE 1    vSpawnPoint 	= <<-1940.1409, 203.1303, 84.2182>>    fSpawnHeading 	= 296.6400    BREAK
				CASE 2    vSpawnPoint 	= <<-1941.1228, 205.4688, 84.1875>>    fSpawnHeading 	= 296.6400    BREAK
				CASE 3    vSpawnPoint 	= <<-1939.4652, 201.9581, 84.2244>>    fSpawnHeading 	= 296.6400    BREAK
				CASE 4    vSpawnPoint 	= <<-1941.8772, 207.1007, 84.1889>>    fSpawnHeading 	= 296.6400    BREAK
				CASE 5    vSpawnPoint 	= <<-1938.8335, 204.8528, 84.0546>>    fSpawnHeading 	= 296.6400    BREAK
				CASE 6    vSpawnPoint 	= <<-1938.3174, 203.5131, 84.0785>>    fSpawnHeading 	= 296.6400    BREAK
				CASE 7    vSpawnPoint 	= <<-1939.2949, 205.8668, 84.0420>>    fSpawnHeading 	= 296.6400    BREAK
				CASE 8    vSpawnPoint 	= <<-1937.5735, 202.5955, 84.0655>>    fSpawnHeading 	= 296.6400    BREAK
				CASE 9    vSpawnPoint 	= <<-1940.0157, 207.2788, 84.0251>>    fSpawnHeading 	= 296.6400    BREAK
				CASE 10    vSpawnPoint 	= <<-1937.9152, 205.7189, 83.9419>>    fSpawnHeading 	= 296.6400    BREAK
				CASE 11    vSpawnPoint 	= <<-1937.3223, 204.4490, 83.9565>>    fSpawnHeading 	= 296.6400    BREAK
				CASE 12    vSpawnPoint 	= <<-1938.5459, 206.9898, 83.9282>>    fSpawnHeading 	= 296.6400    BREAK
				CASE 13    vSpawnPoint 	= <<-1936.5020, 203.1828, 83.9613>>    fSpawnHeading 	= 296.6400    BREAK
				CASE 14    vSpawnPoint 	= <<-1939.0819, 208.4451, 83.8908>>    fSpawnHeading 	= 296.6400    BREAK
				CASE 15    vSpawnPoint 	= <<-1936.5760, 206.1501, 83.8173>>    fSpawnHeading 	= 296.6400    BREAK
				CASE 16    vSpawnPoint 	= <<-1936.1293, 204.9543, 83.8454>>    fSpawnHeading 	= 296.6400    BREAK
				CASE 17    vSpawnPoint 	= <<-1937.2067, 207.3005, 83.8113>>    fSpawnHeading 	= 296.6400    BREAK
				CASE 18    vSpawnPoint 	= <<-1935.0770, 204.0031, 83.8206>>    fSpawnHeading 	= 296.6400    BREAK
				CASE 19    vSpawnPoint 	= <<-1937.7385, 208.7276, 83.7721>>    fSpawnHeading 	= 296.6400    BREAK
				CASE 20    vSpawnPoint 	= <<-1935.1554, 207.2425, 83.6627>>    fSpawnHeading 	= 296.6400    BREAK
				CASE 21    vSpawnPoint 	= <<-1934.7577, 205.9819, 83.6980>>    fSpawnHeading 	= 296.6400    BREAK
				CASE 22    vSpawnPoint 	= <<-1936.0398, 208.3412, 83.6674>>    fSpawnHeading 	= 296.6400    BREAK
				CASE 23    vSpawnPoint 	= <<-1933.8612, 204.7598, 83.6977>>    fSpawnHeading 	= 296.6400    BREAK
				CASE 24    vSpawnPoint 	= <<-1935.9138, 209.4861, 83.6152>>    fSpawnHeading 	= 296.6400    BREAK
				CASE 25    vSpawnPoint 	= <<-1933.3269, 207.2670, 83.5602>>    fSpawnHeading 	= 308.3019    BREAK
				CASE 26    vSpawnPoint 	= <<-1931.9933, 205.0440, 83.5662>>    fSpawnHeading 	= 300.7469    BREAK
				CASE 27    vSpawnPoint 	= <<-1934.4924, 208.8109, 83.5630>>    fSpawnHeading 	= 304.8318    BREAK
				CASE 28    vSpawnPoint 	= <<-1930.8280, 203.0186, 83.6574>>    fSpawnHeading 	= 304.8515    BREAK
				CASE 29    vSpawnPoint 	= <<-1935.1555, 210.6674, 83.5348>>    fSpawnHeading 	= 296.6400    BREAK
				CASE 30    vSpawnPoint 	= <<-1937.3210, 213.4392, 83.6272>>    fSpawnHeading 	= 296.6400    BREAK
				CASE 31    vSpawnPoint 	= <<-1929.5333, 201.3114, 83.6241>>    fSpawnHeading 	= 296.6400    BREAK
				DEFAULT RETURN FALSE
			ENDSWITCH
		BREAK
		CASE FMC_DROPOFF_AUTO_SHOP_CUSTOMER_H4
			SWITCH iSpawnPoint
				CASE 0    vSpawnPoint 	= <<50.0805, 564.3147, 179.2538>>    fSpawnHeading 	= 0.1815     BREAK
				CASE 1    vSpawnPoint 	= <<53.5146, 564.9898, 179.4616>>    fSpawnHeading 	= 16.3438    BREAK
				CASE 2    vSpawnPoint 	= <<48.2267, 564.1354, 179.1367>>    fSpawnHeading 	= 28.3793    BREAK
				CASE 3    vSpawnPoint 	= <<46.5639, 563.5975, 179.0647>>    fSpawnHeading 	= 19.4065    BREAK
				CASE 4    vSpawnPoint 	= <<51.7924, 564.6124, 179.3518>>    fSpawnHeading 	= 13.0292    BREAK
				CASE 5    vSpawnPoint 	= <<48.0380, 574.3699, 179.3920>>    fSpawnHeading 	= 189.0000   BREAK
				CASE 6    vSpawnPoint 	= <<44.7696, 563.7823, 178.9068>>    fSpawnHeading 	= 12.7698    BREAK
				CASE 7    vSpawnPoint 	= <<49.9318, 574.5540, 179.5633>>    fSpawnHeading 	= 189.0000   BREAK
				CASE 8    vSpawnPoint 	= <<45.8622, 573.9634, 179.2391>>    fSpawnHeading 	= 189.0000   BREAK
				CASE 9    vSpawnPoint 	= <<52.2549, 574.9553, 179.8537>>    fSpawnHeading 	= 189.0000   BREAK
				CASE 10    vSpawnPoint 	= <<48.9202, 576.1768, 179.8839>>    fSpawnHeading 	= 189.0000   BREAK
				CASE 11    vSpawnPoint 	= <<47.2358, 575.9270, 179.7228>>    fSpawnHeading 	= 189.0000   BREAK
				CASE 12    vSpawnPoint 	= <<50.9050, 576.2969, 180.2189>>    fSpawnHeading 	= 189.0000   BREAK
				CASE 13    vSpawnPoint 	= <<44.0661, 574.9827, 179.4906>>    fSpawnHeading 	= 189.0000   BREAK
				CASE 14    vSpawnPoint 	= <<52.8614, 576.2047, 180.4150>>    fSpawnHeading 	= 189.0000   BREAK
				CASE 15    vSpawnPoint 	= <<49.5679, 577.7722, 180.5606>>    fSpawnHeading 	= 189.0000   BREAK
				CASE 16    vSpawnPoint 	= <<46.1428, 577.1478, 180.0610>>    fSpawnHeading 	= 189.0000   BREAK
				CASE 17    vSpawnPoint 	= <<43.7991, 576.8724, 180.0236>>    fSpawnHeading 	= 189.0000   BREAK
				CASE 18    vSpawnPoint 	= <<45.7126, 575.4717, 179.5733>>    fSpawnHeading 	= 189.0000   BREAK
				CASE 19    vSpawnPoint 	= <<51.6467, 578.0911, 181.0217>>    fSpawnHeading 	= 189.0000   BREAK
				CASE 20    vSpawnPoint 	= <<47.3777, 579.1622, 180.7496>>    fSpawnHeading 	= 189.0000   BREAK
				CASE 21    vSpawnPoint 	= <<45.2600, 578.5686, 180.4545>>    fSpawnHeading 	= 189.0000   BREAK
				CASE 22    vSpawnPoint 	= <<48.8184, 579.1835, 180.9303>>    fSpawnHeading 	= 189.0000   BREAK
				CASE 23    vSpawnPoint 	= <<43.7762, 578.4512, 180.4685>>    fSpawnHeading 	= 189.0000   BREAK
				CASE 24    vSpawnPoint 	= <<50.8106, 579.6572, 181.4429>>    fSpawnHeading 	= 189.0000   BREAK
				CASE 25    vSpawnPoint 	= <<46.1346, 579.6292, 180.7722>>    fSpawnHeading 	= 189.0000   BREAK
				CASE 26    vSpawnPoint 	= <<51.9335, 580.4414, 182.0276>>    fSpawnHeading 	= 189.0000   BREAK
				CASE 27    vSpawnPoint 	= <<44.6520, 580.1509, 180.8625>>    fSpawnHeading 	= 189.0000   BREAK
				CASE 28    vSpawnPoint 	= <<43.4118, 579.3017, 180.7159>>    fSpawnHeading 	= 189.0000   BREAK
				CASE 29    vSpawnPoint 	= <<46.6904, 580.6099, 181.1366>>    fSpawnHeading 	= 189.0000   BREAK
				CASE 30    vSpawnPoint 	= <<50.2763, 580.7300, 181.7714>>    fSpawnHeading 	= 189.0000   BREAK
				CASE 31    vSpawnPoint 	= <<48.6572, 580.1755, 181.2269>>    fSpawnHeading 	= 189.0000   BREAK
				DEFAULT RETURN FALSE
			ENDSWITCH		
		BREAK
		CASE FMC_DROPOFF_AUTO_SHOP_CUSTOMER_H5
			SWITCH iSpawnPoint
				CASE 0    vSpawnPoint 	= <<-3081.3066, 227.5810, 13.0432>>    fSpawnHeading 	= 322.6117    BREAK
				CASE 1    vSpawnPoint 	= <<-3079.7029, 226.4243, 13.1319>>    fSpawnHeading 	= 331.2000    BREAK
				CASE 2    vSpawnPoint 	= <<-3083.3909, 228.5822, 12.9177>>    fSpawnHeading 	= 331.2000    BREAK
				CASE 3    vSpawnPoint 	= <<-3078.5813, 224.9830, 13.1168>>    fSpawnHeading 	= 331.2000    BREAK
				CASE 4    vSpawnPoint 	= <<-3084.7593, 230.1487, 12.8419>>    fSpawnHeading 	= 322.8897    BREAK
				CASE 5    vSpawnPoint 	= <<-3080.0012, 227.8177, 13.1279>>    fSpawnHeading 	= 331.2000    BREAK
				CASE 6    vSpawnPoint 	= <<-3078.3240, 226.6861, 13.2535>>    fSpawnHeading 	= 331.2000    BREAK
				CASE 7    vSpawnPoint 	= <<-3081.5784, 228.6715, 13.0337>>    fSpawnHeading 	= 331.2000    BREAK
				CASE 8    vSpawnPoint 	= <<-3076.9219, 225.7447, 13.3959>>    fSpawnHeading 	= 331.2000    BREAK
				CASE 9    vSpawnPoint 	= <<-3083.5449, 230.2317, 12.8721>>    fSpawnHeading 	= 331.2000    BREAK
				CASE 10    vSpawnPoint 	= <<-3078.6389, 227.9544, 13.3024>>    fSpawnHeading 	= 331.2000    BREAK
				CASE 11    vSpawnPoint 	= <<-3076.8604, 227.0989, 13.5063>>    fSpawnHeading 	= 331.2000    BREAK
				CASE 12    vSpawnPoint 	= <<-3080.4580, 228.9869, 13.1373>>    fSpawnHeading 	= 331.2000    BREAK
				CASE 13    vSpawnPoint 	= <<-3075.6204, 226.3181, 13.6302>>    fSpawnHeading 	= 327.2150    BREAK
				CASE 14    vSpawnPoint 	= <<-3082.2783, 229.9154, 12.9889>>    fSpawnHeading 	= 331.2000    BREAK
				CASE 15    vSpawnPoint 	= <<-3069.4990, 221.6724, 14.3555>>    fSpawnHeading 	= 351.4616    BREAK
				CASE 16    vSpawnPoint 	= <<-3066.7300, 223.8882, 14.4631>>    fSpawnHeading 	= 355.6886    BREAK
				CASE 17    vSpawnPoint 	= <<-3065.8103, 222.9574, 14.6571>>    fSpawnHeading 	= 347.5893    BREAK
				CASE 18    vSpawnPoint 	= <<-3068.4546, 224.2052, 14.3202>>    fSpawnHeading 	= 351.9212    BREAK
				CASE 19    vSpawnPoint 	= <<-3084.0444, 231.4189, 12.8182>>    fSpawnHeading 	= 331.2000    BREAK
				CASE 20    vSpawnPoint 	= <<-3070.0403, 224.8121, 14.2218>>    fSpawnHeading 	= 351.9415    BREAK
				CASE 21    vSpawnPoint 	= <<-3071.4390, 225.1889, 14.1182>>    fSpawnHeading 	= 343.3133    BREAK
				CASE 22    vSpawnPoint 	= <<-3066.2952, 222.0831, 14.6235>>    fSpawnHeading 	= 347.9753    BREAK
				CASE 23    vSpawnPoint 	= <<-3071.1509, 222.1575, 14.2215>>    fSpawnHeading 	= 347.7016    BREAK
				CASE 24    vSpawnPoint 	= <<-3068.2373, 222.6944, 14.4766>>    fSpawnHeading 	= 355.7686    BREAK
				CASE 25    vSpawnPoint 	= <<-3066.9482, 235.6128, 14.1496>>    fSpawnHeading 	= 167.8173    BREAK
				CASE 26    vSpawnPoint 	= <<-3071.6604, 223.8272, 14.2122>>    fSpawnHeading 	= 7.088152    BREAK
				CASE 27    vSpawnPoint 	= <<-3068.9456, 236.2520, 13.9333>>    fSpawnHeading 	= 170.6788    BREAK
				CASE 28    vSpawnPoint 	= <<-3069.8069, 223.2945, 14.3564>>    fSpawnHeading 	= 347.4962    BREAK
				CASE 29    vSpawnPoint 	= <<-3070.9512, 237.2878, 13.6355>>    fSpawnHeading 	= 167.3761    BREAK
				CASE 30    vSpawnPoint 	= <<-3074.9250, 239.2939, 13.0769>>    fSpawnHeading 	= 154.2718    BREAK
				CASE 31    vSpawnPoint 	= <<-3073.0376, 238.1643, 13.3654>>    fSpawnHeading 	= 163.2216    BREAK
				DEFAULT RETURN FALSE
			ENDSWITCH		
		BREAK
		CASE FMC_DROPOFF_AUTO_SHOP_CUSTOMER_H6
			SWITCH iSpawnPoint
				CASE 0    vSpawnPoint 	= <<-816.6753, -1334.7438, 4.1503>>    fSpawnHeading 	= 112.3200    BREAK
				CASE 1    vSpawnPoint 	= <<-817.2449, -1333.3562, 4.1503>>    fSpawnHeading 	= 112.3200    BREAK
				CASE 2    vSpawnPoint 	= <<-816.1057, -1336.1313, 4.1503>>    fSpawnHeading 	= 112.3200    BREAK
				CASE 3    vSpawnPoint 	= <<-819.2330, -1332.7690, 4.1503>>    fSpawnHeading 	= 95.8179    BREAK
				CASE 4    vSpawnPoint 	= <<-817.1620, -1337.3357, 4.1503>>    fSpawnHeading 	= 124.5398    BREAK
				CASE 5    vSpawnPoint 	= <<-818.9573, -1339.0817, 4.1504>>    fSpawnHeading 	= 112.3200    BREAK
				CASE 6    vSpawnPoint 	= <<-819.0541, -1336.8997, 4.1505>>    fSpawnHeading 	= 112.3200    BREAK
				CASE 7    vSpawnPoint 	= <<-821.7123, -1336.7789, 4.1042>>    fSpawnHeading 	= 112.3200    BREAK
				CASE 8    vSpawnPoint 	= <<-821.7495, -1332.9912, 4.1503>>    fSpawnHeading 	= 99.9029    BREAK
				CASE 9    vSpawnPoint 	= <<-821.5684, -1339.5052, 4.1505>>    fSpawnHeading 	= 112.3200    BREAK
				CASE 10    vSpawnPoint 	= <<-827.1141, -1332.2723, 4.1545>>    fSpawnHeading 	= 112.3200    BREAK
				CASE 11    vSpawnPoint 	= <<-823.6188, -1334.8158, 4.1531>>    fSpawnHeading 	= 95.8631    BREAK
				CASE 12    vSpawnPoint 	= <<-826.6886, -1334.4131, 4.1561>>    fSpawnHeading 	= 112.3200    BREAK
				CASE 13    vSpawnPoint 	= <<-824.6989, -1332.2798, 4.1530>>    fSpawnHeading 	= 99.9932    BREAK
				CASE 14    vSpawnPoint 	= <<-821.1755, -1341.9774, 4.1504>>    fSpawnHeading 	= 120.4887    BREAK
				CASE 15    vSpawnPoint 	= <<-829.0634, -1330.8085, 4.1518>>    fSpawnHeading 	= 112.3200    BREAK
				CASE 16    vSpawnPoint 	= <<-824.1336, -1344.6696, 4.1504>>    fSpawnHeading 	= 112.3200    BREAK
				CASE 17    vSpawnPoint 	= <<-823.7188, -1342.4453, 4.1509>>    fSpawnHeading 	= 112.3200    BREAK
				CASE 18    vSpawnPoint 	= <<-829.7715, -1333.1138, 4.1576>>    fSpawnHeading 	= 112.3200    BREAK
				CASE 19    vSpawnPoint 	= <<-824.4214, -1339.8979, 4.1057>>    fSpawnHeading 	= 112.3200    BREAK
				CASE 20    vSpawnPoint 	= <<-826.5271, -1341.5017, 4.1065>>    fSpawnHeading 	= 112.3200    BREAK
				CASE 21    vSpawnPoint 	= <<-828.6345, -1336.2181, 4.1039>>    fSpawnHeading 	= 112.3200    BREAK
				CASE 22    vSpawnPoint 	= <<-825.8774, -1344.1019, 4.1529>>    fSpawnHeading 	= 112.3200    BREAK
				CASE 23    vSpawnPoint 	= <<-833.5002, -1330.1530, 4.1521>>    fSpawnHeading 	= 112.3200    BREAK
				CASE 24    vSpawnPoint 	= <<-825.4216, -1346.4998, 4.1504>>    fSpawnHeading 	= 112.3200    BREAK
				CASE 25    vSpawnPoint 	= <<-830.3430, -1338.8472, 4.1555>>    fSpawnHeading 	= 112.3200    BREAK
				CASE 26    vSpawnPoint 	= <<-831.3143, -1336.3252, 4.1568>>    fSpawnHeading 	= 112.3200    BREAK
				CASE 27    vSpawnPoint 	= <<-830.0225, -1341.1393, 4.1532>>    fSpawnHeading 	= 112.3200    BREAK
				CASE 28    vSpawnPoint 	= <<-832.4841, -1332.5046, 4.1570>>    fSpawnHeading 	= 112.3200    BREAK
				CASE 29    vSpawnPoint 	= <<-829.1557, -1343.6802, 4.1527>>    fSpawnHeading 	= 112.3200    BREAK
				CASE 30    vSpawnPoint 	= <<-827.5688, -1348.5446, 4.1519>>    fSpawnHeading 	= 112.3200    BREAK
				CASE 31    vSpawnPoint 	= <<-828.6310, -1345.6602, 4.1525>>    fSpawnHeading 	= 112.3200    BREAK
				DEFAULT RETURN FALSE
			ENDSWITCH		
		BREAK
		CASE FMC_DROPOFF_AUTO_SHOP_CUSTOMER_H7
			SWITCH iSpawnPoint
				CASE 0    vSpawnPoint 	= <<-1854.3684, -380.5186, 47.4461>>    fSpawnHeading 	= 142.5600    BREAK
				CASE 1    vSpawnPoint 	= <<-1855.5594, -379.6067, 47.4530>>    fSpawnHeading 	= 142.5600    BREAK
				CASE 2    vSpawnPoint 	= <<-1853.1774, -381.4305, 47.4391>>    fSpawnHeading 	= 142.5600    BREAK
				CASE 3    vSpawnPoint 	= <<-1856.7505, -378.6948, 47.4556>>    fSpawnHeading 	= 142.5600    BREAK
				CASE 4    vSpawnPoint 	= <<-1851.9863, -382.3424, 47.4322>>    fSpawnHeading 	= 142.5600    BREAK
				CASE 5    vSpawnPoint 	= <<-1855.2803, -381.7096, 47.2298>>    fSpawnHeading 	= 142.5600    BREAK
				CASE 6    vSpawnPoint 	= <<-1856.4713, -380.7977, 47.2464>>    fSpawnHeading 	= 142.5600    BREAK
				CASE 7    vSpawnPoint 	= <<-1854.0892, -382.6215, 47.2131>>    fSpawnHeading 	= 142.5600    BREAK
				CASE 8    vSpawnPoint 	= <<-1857.6624, -379.8858, 47.2630>>    fSpawnHeading 	= 142.5600    BREAK
				CASE 9    vSpawnPoint 	= <<-1852.8982, -383.5334, 47.1965>>    fSpawnHeading 	= 142.5600    BREAK
				CASE 10    vSpawnPoint 	= <<-1856.1921, -382.9005, 46.9580>>    fSpawnHeading 	= 142.5600    BREAK
				CASE 11    vSpawnPoint 	= <<-1857.3832, -381.9886, 46.9887>>    fSpawnHeading 	= 142.5600    BREAK
				CASE 12    vSpawnPoint 	= <<-1855.0011, -383.8124, 46.9402>>    fSpawnHeading 	= 142.5600    BREAK
				CASE 13    vSpawnPoint 	= <<-1858.5742, -381.0768, 47.0303>>    fSpawnHeading 	= 142.5600    BREAK
				CASE 14    vSpawnPoint 	= <<-1853.8101, -384.7243, 46.9224>>    fSpawnHeading 	= 142.5600    BREAK
				CASE 15    vSpawnPoint 	= <<-1857.1040, -384.0915, 46.7413>>    fSpawnHeading 	= 142.5600    BREAK
				CASE 16    vSpawnPoint 	= <<-1858.2950, -383.1796, 46.7835>>    fSpawnHeading 	= 142.5600    BREAK
				CASE 17    vSpawnPoint 	= <<-1855.9130, -385.0034, 46.6992>>    fSpawnHeading 	= 142.5600    BREAK
				CASE 18    vSpawnPoint 	= <<-1859.4861, -382.2677, 46.8257>>    fSpawnHeading 	= 142.5600    BREAK
				CASE 19    vSpawnPoint 	= <<-1854.7219, -385.9153, 46.6576>>    fSpawnHeading 	= 142.5600    BREAK
				CASE 20    vSpawnPoint 	= <<-1858.0159, -385.2825, 46.4984>>    fSpawnHeading 	= 142.5600    BREAK
				CASE 21    vSpawnPoint 	= <<-1859.2069, -384.3706, 46.5539>>    fSpawnHeading 	= 142.5600    BREAK
				CASE 22    vSpawnPoint 	= <<-1856.8248, -386.1944, 46.4429>>    fSpawnHeading 	= 142.5600    BREAK
				CASE 23    vSpawnPoint 	= <<-1860.3979, -383.4587, 46.6094>>    fSpawnHeading 	= 142.5600    BREAK
				CASE 24    vSpawnPoint 	= <<-1855.6338, -387.1063, 46.3874>>    fSpawnHeading 	= 142.5600    BREAK
				CASE 25    vSpawnPoint 	= <<-1858.9277, -386.4735, 46.2574>>    fSpawnHeading 	= 142.5600    BREAK
				CASE 26    vSpawnPoint 	= <<-1860.1188, -385.5616, 46.3284>>    fSpawnHeading 	= 142.5600    BREAK
				CASE 27    vSpawnPoint 	= <<-1857.7367, -387.3854, 46.1871>>    fSpawnHeading 	= 142.5600    BREAK
				CASE 28    vSpawnPoint 	= <<-1861.3098, -384.6497, 46.4014>>    fSpawnHeading 	= 142.5600    BREAK
				CASE 29    vSpawnPoint 	= <<-1856.5457, -388.2973, 46.1169>>    fSpawnHeading 	= 142.5600    BREAK
				CASE 30    vSpawnPoint 	= <<-1859.8396, -387.6645, 46.0972>>    fSpawnHeading 	= 142.5600    BREAK
				CASE 31    vSpawnPoint 	= <<-1861.0306, -386.7526, 46.1750>>    fSpawnHeading 	= 142.5600    BREAK
				DEFAULT RETURN FALSE
			ENDSWITCH		
		BREAK
		CASE FMC_DROPOFF_AUTO_SHOP_CUSTOMER_H8
			SWITCH iSpawnPoint
				CASE 0    vSpawnPoint 	= <<-3235.1824, 955.5072, 12.2111>>    fSpawnHeading 	= 10.4400    BREAK
				CASE 1    vSpawnPoint 	= <<-3234.2405, 956.8399, 12.2220>>    fSpawnHeading 	= 10.4400    BREAK
				CASE 2    vSpawnPoint 	= <<-3238.1592, 955.4146, 11.7019>>    fSpawnHeading 	= 10.4400    BREAK
				CASE 3    vSpawnPoint 	= <<-3236.3596, 957.3724, 12.1756>>    fSpawnHeading 	= 10.4400    BREAK
				CASE 4    vSpawnPoint 	= <<-3236.6680, 959.5195, 12.1559>>    fSpawnHeading 	= 10.4400    BREAK
				CASE 5    vSpawnPoint 	= <<-3234.9050, 959.7594, 12.1687>>    fSpawnHeading 	= 10.4400    BREAK
				CASE 6    vSpawnPoint 	= <<-3238.6331, 957.1829, 11.4096>>    fSpawnHeading 	= 10.4400    BREAK
				CASE 7    vSpawnPoint 	= <<-3234.0046, 958.4041, 12.1990>>    fSpawnHeading 	= 10.4400    BREAK
				CASE 8    vSpawnPoint 	= <<-3235.4548, 962.3365, 12.1181>>    fSpawnHeading 	= 10.4400    BREAK
				CASE 9    vSpawnPoint 	= <<-3236.2856, 961.1478, 12.1364>>    fSpawnHeading 	= 10.4400    BREAK
				CASE 10    vSpawnPoint 	= <<-3239.2439, 961.5840, 11.7019>>    fSpawnHeading 	= 2.45850    BREAK
				CASE 11    vSpawnPoint 	= <<-3238.9136, 959.3049, 11.7019>>    fSpawnHeading 	= 2.10610    BREAK
				CASE 12    vSpawnPoint 	= <<-3237.1814, 962.4586, 12.1052>>    fSpawnHeading 	= 10.4400    BREAK
				CASE 13    vSpawnPoint 	= <<-3237.4854, 964.6573, 12.0596>>    fSpawnHeading 	= 10.4400    BREAK
				CASE 14    vSpawnPoint 	= <<-3239.6099, 964.0398, 11.7019>>    fSpawnHeading 	= 358.085    BREAK
				CASE 15    vSpawnPoint 	= <<-3235.6401, 964.5833, 12.0709>>    fSpawnHeading 	= 10.4400    BREAK
				CASE 16    vSpawnPoint 	= <<-3237.7952, 967.2527, 11.9987>>    fSpawnHeading 	= 10.4400    BREAK
				CASE 17    vSpawnPoint 	= <<-3237.7881, 968.9925, 11.9572>>    fSpawnHeading 	= 10.4400    BREAK
				CASE 18    vSpawnPoint 	= <<-3239.8308, 967.0061, 11.7019>>    fSpawnHeading 	= 6.20430    BREAK
				CASE 19    vSpawnPoint 	= <<-3235.9529, 967.0388, 12.0166>>    fSpawnHeading 	= 10.1911    BREAK
				CASE 20    vSpawnPoint 	= <<-3237.9014, 971.0781, 11.9124>>    fSpawnHeading 	= 10.4400    BREAK
				CASE 21    vSpawnPoint 	= <<-3236.1606, 969.4367, 11.9608>>    fSpawnHeading 	= 10.4400    BREAK
				CASE 22    vSpawnPoint 	= <<-3240.0366, 970.9451, 11.7019>>    fSpawnHeading 	= 6.36310    BREAK
				CASE 23    vSpawnPoint 	= <<-3236.5088, 972.7813, 11.8925>>    fSpawnHeading 	= 10.4400    BREAK
				CASE 24    vSpawnPoint 	= <<-3240.6108, 980.0478, 11.7019>>    fSpawnHeading 	= 268.844    BREAK
				CASE 25    vSpawnPoint 	= <<-3240.3401, 977.0618, 11.7019>>    fSpawnHeading 	= 268.263    BREAK
				CASE 26    vSpawnPoint 	= <<-3238.4834, 976.7290, 11.7945>>    fSpawnHeading 	= 272.506    BREAK
				CASE 27    vSpawnPoint 	= <<-3239.9473, 974.5887, 11.7019>>    fSpawnHeading 	= 284.470    BREAK
				CASE 28    vSpawnPoint 	= <<-3236.9509, 977.9911, 11.7833>>    fSpawnHeading 	= 268.342    BREAK
				CASE 29    vSpawnPoint 	= <<-3236.7488, 975.7599, 11.8314>>    fSpawnHeading 	= 276.560    BREAK
				CASE 30    vSpawnPoint 	= <<-3236.8013, 980.5408, 11.7307>>    fSpawnHeading 	= 268.228    BREAK
				CASE 31    vSpawnPoint 	= <<-3238.5286, 979.3701, 11.7406>>    fSpawnHeading 	= 272.861    BREAK
				DEFAULT RETURN FALSE
			ENDSWITCH		
		BREAK
		CASE FMC_DROPOFF_AUTO_SHOP_CUSTOMER_L1
			SWITCH iSpawnPoint
				CASE 0    vSpawnPoint 	= <<1709.6405, 4668.4995, 42.1264>>    fSpawnHeading 	= 90.0000    BREAK
				CASE 1    vSpawnPoint 	= <<1709.6405, 4669.9995, 42.1154>>    fSpawnHeading 	= 90.0000    BREAK
				CASE 2    vSpawnPoint 	= <<1709.6405, 4666.9995, 42.1911>>    fSpawnHeading 	= 90.0000    BREAK
				CASE 3    vSpawnPoint 	= <<1709.6405, 4671.4995, 42.1254>>    fSpawnHeading 	= 90.0000    BREAK
				CASE 4    vSpawnPoint 	= <<1708.2087, 4663.7305, 42.3311>>    fSpawnHeading 	= 90.0000    BREAK
				CASE 5    vSpawnPoint 	= <<1708.1405, 4668.4995, 42.1710>>    fSpawnHeading 	= 90.0000    BREAK
				CASE 6    vSpawnPoint 	= <<1708.0319, 4670.5679, 42.1589>>    fSpawnHeading 	= 90.0000    BREAK
				CASE 7    vSpawnPoint 	= <<1708.3628, 4665.9873, 42.2747>>    fSpawnHeading 	= 90.0000    BREAK
				CASE 8    vSpawnPoint 	= <<1708.3540, 4672.3706, 42.1642>>    fSpawnHeading 	= 90.0000    BREAK
				CASE 9    vSpawnPoint 	= <<1707.4166, 4665.2207, 42.3068>>    fSpawnHeading 	= 90.0000    BREAK
				CASE 10    vSpawnPoint 	= <<1706.6405, 4668.4995, 42.2226>>    fSpawnHeading 	= 90.0000    BREAK
				CASE 11    vSpawnPoint 	= <<1706.6405, 4669.9995, 42.2029>>    fSpawnHeading 	= 90.0000    BREAK
				CASE 12    vSpawnPoint 	= <<1706.6405, 4666.9995, 42.2617>>    fSpawnHeading 	= 90.0000    BREAK
				CASE 13    vSpawnPoint 	= <<1706.6405, 4671.4995, 42.1877>>    fSpawnHeading 	= 90.0000    BREAK
				CASE 14    vSpawnPoint 	= <<1706.4004, 4663.6162, 42.3476>>    fSpawnHeading 	= 90.0000    BREAK
				CASE 15    vSpawnPoint 	= <<1705.1405, 4668.4995, 42.2584>>    fSpawnHeading 	= 90.0000    BREAK
				CASE 16    vSpawnPoint 	= <<1705.1405, 4669.9995, 42.2270>>    fSpawnHeading 	= 90.0000    BREAK
				CASE 17    vSpawnPoint 	= <<1705.1405, 4666.9995, 42.2934>>    fSpawnHeading 	= 90.0000    BREAK
				CASE 18    vSpawnPoint 	= <<1705.1405, 4671.4995, 42.1956>>    fSpawnHeading 	= 90.0000    BREAK
				CASE 19    vSpawnPoint 	= <<1705.1405, 4665.4995, 42.3203>>    fSpawnHeading 	= 90.0000    BREAK
				CASE 20    vSpawnPoint 	= <<1703.6405, 4668.4995, 42.2062>>    fSpawnHeading 	= 90.0000    BREAK
				CASE 21    vSpawnPoint 	= <<1703.6405, 4669.9995, 42.1749>>    fSpawnHeading 	= 90.0000    BREAK
				CASE 22    vSpawnPoint 	= <<1703.6405, 4666.9995, 42.2375>>    fSpawnHeading 	= 90.0000    BREAK
				CASE 23    vSpawnPoint 	= <<1703.6405, 4671.4995, 42.1436>>    fSpawnHeading 	= 90.0000    BREAK
				CASE 24    vSpawnPoint 	= <<1703.6405, 4665.4995, 42.2683>>    fSpawnHeading 	= 90.0000    BREAK
				CASE 25    vSpawnPoint 	= <<1702.1405, 4668.4995, 42.2770>>    fSpawnHeading 	= 90.0000    BREAK
				CASE 26    vSpawnPoint 	= <<1702.1405, 4669.9995, 42.2594>>    fSpawnHeading 	= 90.0000    BREAK
				CASE 27    vSpawnPoint 	= <<1702.1405, 4666.9995, 42.2946>>    fSpawnHeading 	= 90.0000    BREAK
				CASE 28    vSpawnPoint 	= <<1702.1405, 4671.4995, 42.2418>>    fSpawnHeading 	= 90.0000    BREAK
				CASE 29    vSpawnPoint 	= <<1702.1405, 4665.4995, 42.3124>>    fSpawnHeading 	= 90.0000    BREAK
				CASE 30    vSpawnPoint 	= <<1702.6085, 4663.4038, 42.2944>>    fSpawnHeading 	= 90.0000    BREAK
				CASE 31    vSpawnPoint 	= <<1704.3370, 4663.5933, 42.3297>>    fSpawnHeading 	= 90.0000    BREAK
				DEFAULT RETURN FALSE
			ENDSWITCH		
		BREAK
		CASE FMC_DROPOFF_AUTO_SHOP_CUSTOMER_L2
			SWITCH iSpawnPoint
				CASE 0    vSpawnPoint 	= <<-1097.8652, -1050.9996, 1.0761>>    fSpawnHeading 	= 301.0250    BREAK
				CASE 1    vSpawnPoint 	= <<-1098.3640, -1052.7095, 1.1153>>    fSpawnHeading 	= 301.0250    BREAK
				CASE 2    vSpawnPoint 	= <<-1099.4827, -1050.6945, 1.1117>>    fSpawnHeading 	= 301.0250    BREAK
				CASE 3    vSpawnPoint 	= <<-1097.4425, -1053.5938, 1.1315>>    fSpawnHeading 	= 301.0250    BREAK
				CASE 4    vSpawnPoint 	= <<-1100.1227, -1049.1377, 1.1494>>    fSpawnHeading 	= 301.0250    BREAK
				CASE 5    vSpawnPoint 	= <<-1097.0292, -1049.5172, 1.0784>>    fSpawnHeading 	= 301.0250    BREAK
				CASE 6    vSpawnPoint 	= <<-1096.5078, -1052.3997, 1.1043>>    fSpawnHeading 	= 301.0250    BREAK
				CASE 7    vSpawnPoint 	= <<-1098.8899, -1048.2864, 1.1601>>    fSpawnHeading 	= 301.0250    BREAK
				CASE 8    vSpawnPoint 	= <<-1095.4105, -1052.6210, 1.1309>>    fSpawnHeading 	= 301.0250    BREAK
				CASE 9    vSpawnPoint 	= <<-1097.7102, -1048.0486, 1.1658>>    fSpawnHeading 	= 301.0250    BREAK
				CASE 10    vSpawnPoint 	= <<-1095.8695, -1049.7808, 1.0632>>    fSpawnHeading 	= 301.0250    BREAK
				CASE 11    vSpawnPoint 	= <<-1095.5646, -1051.2397, 1.0774>>    fSpawnHeading 	= 301.0250    BREAK
				CASE 12    vSpawnPoint 	= <<-1095.9315, -1048.1453, 1.1335>>    fSpawnHeading 	= 301.0250    BREAK
				CASE 13    vSpawnPoint 	= <<-1094.2913, -1051.8041, 1.1338>>    fSpawnHeading 	= 301.0250    BREAK
				CASE 14    vSpawnPoint 	= <<-1097.3524, -1046.9767, 1.1522>>    fSpawnHeading 	= 301.0250    BREAK
				CASE 15    vSpawnPoint 	= <<-1093.1818, -1047.9080, 1.1458>>    fSpawnHeading 	= 301.0250    BREAK
				CASE 16    vSpawnPoint 	= <<-1094.1855, -1049.5115, 1.1135>>    fSpawnHeading 	= 301.0250    BREAK
				CASE 17    vSpawnPoint 	= <<-1094.3573, -1047.1344, 1.1760>>    fSpawnHeading 	= 301.0250    BREAK
				CASE 18    vSpawnPoint 	= <<-1092.8051, -1051.1125, 1.1383>>    fSpawnHeading 	= 301.0250    BREAK
				CASE 19    vSpawnPoint 	= <<-1095.6619, -1046.2489, 1.1562>>    fSpawnHeading 	= 301.0250    BREAK
				CASE 20    vSpawnPoint 	= <<-1090.7706, -1047.3285, 1.1316>>    fSpawnHeading 	= 301.0250    BREAK
				CASE 21    vSpawnPoint 	= <<-1089.8132, -1049.0569, 1.1292>>    fSpawnHeading 	= 301.0250    BREAK
				CASE 22    vSpawnPoint 	= <<-1092.4120, -1045.8668, 1.1727>>    fSpawnHeading 	= 301.0250    BREAK
				CASE 23    vSpawnPoint 	= <<-1092.3124, -1049.6691, 1.1207>>    fSpawnHeading 	= 301.0250    BREAK
				CASE 24    vSpawnPoint 	= <<-1094.7927, -1044.9845, 1.1634>>    fSpawnHeading 	= 301.0250    BREAK
				CASE 25    vSpawnPoint 	= <<-1106.8362, -1056.5886, 1.0990>>    fSpawnHeading 	= 128.5829    BREAK
				CASE 26    vSpawnPoint 	= <<-1108.0955, -1054.7871, 1.1075>>    fSpawnHeading 	= 125.0408    BREAK
				CASE 27    vSpawnPoint 	= <<-1112.5511, -1055.7065, 1.1448>>    fSpawnHeading 	= 125.4430    BREAK
				CASE 28    vSpawnPoint 	= <<-1108.4098, -1056.5454, 1.0814>>    fSpawnHeading 	= 124.0992    BREAK
				CASE 29    vSpawnPoint 	= <<-1110.6415, -1056.1661, 1.1227>>    fSpawnHeading 	= 116.7658    BREAK
				CASE 30    vSpawnPoint 	= <<-1108.5884, -1058.9952, 1.1289>>    fSpawnHeading 	= 121.5317    BREAK
				CASE 31    vSpawnPoint 	= <<-1109.9740, -1058.1569, 1.0911>>    fSpawnHeading 	= 123.2820    BREAK
				DEFAULT RETURN FALSE
			ENDSWITCH		
		BREAK
		CASE FMC_DROPOFF_AUTO_SHOP_CUSTOMER_L3
			SWITCH iSpawnPoint
				CASE 0    vSpawnPoint 	= <<-1158.9094, -1464.5691, 3.3685>>    fSpawnHeading 	= 128.8800    BREAK
				CASE 1    vSpawnPoint 	= <<-1160.1112, -1462.7881, 3.3668>>    fSpawnHeading 	= 128.8800    BREAK
				CASE 2    vSpawnPoint 	= <<-1162.1830, -1459.5699, 3.2471>>    fSpawnHeading 	= 128.8800    BREAK
				CASE 3    vSpawnPoint 	= <<-1164.2059, -1456.6108, 3.2448>>    fSpawnHeading 	= 128.8800    BREAK
				CASE 4    vSpawnPoint 	= <<-1161.1614, -1461.1858, 3.3668>>    fSpawnHeading 	= 128.8800    BREAK
				CASE 5    vSpawnPoint 	= <<-1160.8530, -1459.3939, 3.3744>>    fSpawnHeading 	= 128.8800    BREAK
				CASE 6    vSpawnPoint 	= <<-1161.6029, -1458.3464, 3.2927>>    fSpawnHeading 	= 128.8800    BREAK
				CASE 7    vSpawnPoint 	= <<-1161.0774, -1456.9819, 3.3286>>    fSpawnHeading 	= 128.8800    BREAK
				CASE 8    vSpawnPoint 	= <<-1162.3715, -1457.2288, 3.2750>>    fSpawnHeading 	= 128.8800    BREAK
				CASE 9    vSpawnPoint 	= <<-1160.2064, -1458.4506, 3.3805>>    fSpawnHeading 	= 128.8800    BREAK
				CASE 10    vSpawnPoint 	= <<-1159.4325, -1457.0896, 3.3607>>    fSpawnHeading 	= 128.8800    BREAK
				CASE 11    vSpawnPoint 	= <<-1159.6980, -1455.8184, 3.3701>>    fSpawnHeading 	= 128.8800    BREAK
				CASE 12    vSpawnPoint 	= <<-1157.9730, -1457.1554, 3.3828>>    fSpawnHeading 	= 128.8800    BREAK
				CASE 13    vSpawnPoint 	= <<-1160.0647, -1454.5939, 3.3825>>    fSpawnHeading 	= 120.4252    BREAK
				CASE 14    vSpawnPoint 	= <<-1158.9486, -1458.5092, 3.4084>>    fSpawnHeading 	= 128.8800    BREAK
				CASE 15    vSpawnPoint 	= <<-1157.9027, -1455.7953, 3.3285>>    fSpawnHeading 	= 128.8800    BREAK
				CASE 16    vSpawnPoint 	= <<-1158.6371, -1453.8516, 3.3694>>    fSpawnHeading 	= 128.8800    BREAK
				CASE 17    vSpawnPoint 	= <<-1156.5524, -1456.5765, 3.3393>>    fSpawnHeading 	= 128.8800    BREAK
				CASE 18    vSpawnPoint 	= <<-1156.9132, -1452.4323, 3.4217>>    fSpawnHeading 	= 128.8800    BREAK
				CASE 19    vSpawnPoint 	= <<-1157.6954, -1459.0452, 3.4150>>    fSpawnHeading 	= 128.8800    BREAK
				CASE 20    vSpawnPoint 	= <<-1155.5934, -1451.9590, 3.4298>>    fSpawnHeading 	= 128.8800    BREAK
				CASE 21    vSpawnPoint 	= <<-1156.7667, -1453.8401, 3.3017>>    fSpawnHeading 	= 128.8800    BREAK
				CASE 22    vSpawnPoint 	= <<-1154.3361, -1453.1263, 3.2730>>    fSpawnHeading 	= 128.8800    BREAK
				CASE 23    vSpawnPoint 	= <<-1155.1494, -1455.0836, 3.3271>>    fSpawnHeading 	= 128.8800    BREAK
				CASE 24    vSpawnPoint 	= <<-1153.9175, -1455.0684, 3.4026>>    fSpawnHeading 	= 128.3990    BREAK
				CASE 25    vSpawnPoint 	= <<-1173.6736, -1469.0973, 3.3857>>    fSpawnHeading 	= 300.2633    BREAK
				CASE 26    vSpawnPoint 	= <<-1175.6411, -1464.7915, 3.3958>>    fSpawnHeading 	= 295.6910    BREAK
				CASE 27    vSpawnPoint 	= <<-1172.4896, -1470.0378, 3.3842>>    fSpawnHeading 	= 300.9921    BREAK
				CASE 28    vSpawnPoint 	= <<-1174.2744, -1470.8082, 3.3831>>    fSpawnHeading 	= 278.8469    BREAK
				CASE 29    vSpawnPoint 	= <<-1159.7194, -1460.6772, 3.3764>>    fSpawnHeading 	= 128.8800    BREAK
				CASE 30    vSpawnPoint 	= <<-1175.3644, -1466.6715, 3.3847>>    fSpawnHeading 	= 295.5116    BREAK
				CASE 31    vSpawnPoint 	= <<-1177.1135, -1466.2538, 3.3819>>    fSpawnHeading 	= 295.9021    BREAK
				DEFAULT RETURN FALSE
			ENDSWITCH		
		BREAK
		CASE FMC_DROPOFF_AUTO_SHOP_CUSTOMER_L4
			SWITCH iSpawnPoint
				CASE 0    vSpawnPoint 	= <<-1824.1879, -663.9872, 9.4996>>    fSpawnHeading 	= 138.9550    BREAK
				CASE 1    vSpawnPoint 	= <<-1825.5454, -662.8053, 9.5014>>    fSpawnHeading 	= 138.9550    BREAK
				CASE 2    vSpawnPoint 	= <<-1822.8303, -665.1692, 9.4978>>    fSpawnHeading 	= 138.9550    BREAK
				CASE 3    vSpawnPoint 	= <<-1826.9030, -661.6234, 9.5032>>    fSpawnHeading 	= 138.9550    BREAK
				CASE 4    vSpawnPoint 	= <<-1821.4728, -666.3511, 9.4960>>    fSpawnHeading 	= 138.9550    BREAK
				CASE 5    vSpawnPoint 	= <<-1828.2605, -660.4414, 9.5050>>    fSpawnHeading 	= 138.9550    BREAK
				CASE 6    vSpawnPoint 	= <<-1820.1152, -667.5331, 9.4850>>    fSpawnHeading 	= 138.9550    BREAK
				CASE 7    vSpawnPoint 	= <<-1829.6180, -659.2595, 9.5068>>    fSpawnHeading 	= 138.9550    BREAK
				CASE 8    vSpawnPoint 	= <<-1825.3699, -665.3448, 9.3867>>    fSpawnHeading 	= 138.9550    BREAK
				CASE 9    vSpawnPoint 	= <<-1826.7274, -664.1628, 9.3885>>    fSpawnHeading 	= 138.9550    BREAK
				CASE 10    vSpawnPoint 	= <<-1824.0123, -666.5267, 9.3849>>    fSpawnHeading 	= 138.9550    BREAK
				CASE 11    vSpawnPoint 	= <<-1828.0850, -662.9809, 9.3904>>    fSpawnHeading 	= 138.9550    BREAK
				CASE 12    vSpawnPoint 	= <<-1822.6548, -667.7087, 9.3831>>    fSpawnHeading 	= 138.9550    BREAK
				CASE 13    vSpawnPoint 	= <<-1829.4425, -661.7990, 9.3922>>    fSpawnHeading 	= 138.9550    BREAK
				CASE 14    vSpawnPoint 	= <<-1821.2972, -668.8906, 9.3672>>    fSpawnHeading 	= 138.9550    BREAK
				CASE 15    vSpawnPoint 	= <<-1830.8000, -660.6170, 9.3938>>    fSpawnHeading 	= 138.9550    BREAK
				CASE 16    vSpawnPoint 	= <<-1826.5519, -666.7023, 9.3607>>    fSpawnHeading 	= 138.9550    BREAK
				CASE 17    vSpawnPoint 	= <<-1827.9094, -665.5204, 9.3600>>    fSpawnHeading 	= 138.9550    BREAK
				CASE 18    vSpawnPoint 	= <<-1825.1943, -667.8843, 9.3614>>    fSpawnHeading 	= 138.9550    BREAK
				CASE 19    vSpawnPoint 	= <<-1829.2670, -664.3384, 9.3592>>    fSpawnHeading 	= 138.9550    BREAK
				CASE 20    vSpawnPoint 	= <<-1823.8368, -669.0662, 9.3621>>    fSpawnHeading 	= 138.9550    BREAK
				CASE 21    vSpawnPoint 	= <<-1830.6245, -663.1565, 9.3585>>    fSpawnHeading 	= 138.9550    BREAK
				CASE 22    vSpawnPoint 	= <<-1822.4792, -670.2482, 9.3628>>    fSpawnHeading 	= 138.9550    BREAK
				CASE 23    vSpawnPoint 	= <<-1831.9821, -661.9745, 9.3578>>    fSpawnHeading 	= 138.9550    BREAK
				CASE 24    vSpawnPoint 	= <<-1827.7339, -668.0599, 9.3993>>    fSpawnHeading 	= 138.9550    BREAK
				CASE 25    vSpawnPoint 	= <<-1829.0914, -666.8779, 9.3987>>    fSpawnHeading 	= 138.9550    BREAK
				CASE 26    vSpawnPoint 	= <<-1826.3763, -669.2418, 9.3998>>    fSpawnHeading 	= 138.9550    BREAK
				CASE 27    vSpawnPoint 	= <<-1830.5699, -666.3531, 9.4086>>    fSpawnHeading 	= 138.9550    BREAK
				CASE 28    vSpawnPoint 	= <<-1825.0188, -670.4238, 9.4004>>    fSpawnHeading 	= 138.9550    BREAK
				CASE 29    vSpawnPoint 	= <<-1832.3477, -664.7587, 9.4072>>    fSpawnHeading 	= 138.9550    BREAK
				CASE 30    vSpawnPoint 	= <<-1823.6613, -671.6057, 9.4009>>    fSpawnHeading 	= 138.9550    BREAK
				CASE 31    vSpawnPoint 	= <<-1833.1641, -663.3321, 9.3975>>    fSpawnHeading 	= 138.9550    BREAK
				DEFAULT RETURN FALSE
			ENDSWITCH		
		BREAK
		CASE FMC_DROPOFF_AUTO_SHOP_CUSTOMER_L5
			SWITCH iSpawnPoint
				CASE 0    vSpawnPoint 	= <<876.8036, -509.6296, 56.3301>>    fSpawnHeading 	= 217.4400    BREAK
				CASE 1    vSpawnPoint 	= <<875.4600, -511.3341, 56.3352>>    fSpawnHeading 	= 217.4400    BREAK
				CASE 2    vSpawnPoint 	= <<878.2328, -508.5353, 56.3237>>    fSpawnHeading 	= 217.4400    BREAK
				CASE 3    vSpawnPoint 	= <<873.6586, -512.3547, 56.3281>>    fSpawnHeading 	= 217.4400    BREAK
				CASE 4    vSpawnPoint 	= <<880.6370, -507.4767, 56.3321>>    fSpawnHeading 	= 209.2742    BREAK
				CASE 5    vSpawnPoint 	= <<872.3223, -513.4522, 56.3299>>    fSpawnHeading 	= 217.4400    BREAK
				CASE 6    vSpawnPoint 	= <<882.5317, -506.3899, 56.3307>>    fSpawnHeading 	= 205.0466    BREAK
				CASE 7    vSpawnPoint 	= <<870.8762, -514.4914, 56.3300>>    fSpawnHeading 	= 234.3541    BREAK
				CASE 8    vSpawnPoint 	= <<877.8979, -511.0588, 56.3266>>    fSpawnHeading 	= 217.4400    BREAK
				CASE 9    vSpawnPoint 	= <<876.4687, -512.1531, 56.3289>>    fSpawnHeading 	= 217.4400    BREAK
				CASE 10    vSpawnPoint 	= <<879.3271, -509.9645, 56.3333>>    fSpawnHeading 	= 217.4400    BREAK
				CASE 11    vSpawnPoint 	= <<875.0395, -513.2473, 56.3283>>    fSpawnHeading 	= 217.4400    BREAK
				CASE 12    vSpawnPoint 	= <<880.7563, -508.8702, 56.3403>>    fSpawnHeading 	= 217.4400    BREAK
				CASE 13    vSpawnPoint 	= <<873.6103, -514.3416, 56.3319>>    fSpawnHeading 	= 217.4400    BREAK
				CASE 14    vSpawnPoint 	= <<885.7950, -507.1485, 56.3319>>    fSpawnHeading 	= 209.1990    BREAK
				CASE 15    vSpawnPoint 	= <<872.1811, -515.4359, 56.3318>>    fSpawnHeading 	= 217.4400    BREAK
				CASE 16    vSpawnPoint 	= <<878.9922, -512.4879, 56.2814>>    fSpawnHeading 	= 217.4400    BREAK
				CASE 17    vSpawnPoint 	= <<877.5630, -513.5822, 56.2792>>    fSpawnHeading 	= 217.4400    BREAK
				CASE 18    vSpawnPoint 	= <<880.4214, -511.3937, 56.2980>>    fSpawnHeading 	= 217.4400    BREAK
				CASE 19    vSpawnPoint 	= <<876.1338, -514.6765, 56.3305>>    fSpawnHeading 	= 217.4400    BREAK
				CASE 20    vSpawnPoint 	= <<881.8506, -510.2994, 56.3399>>    fSpawnHeading 	= 217.4400    BREAK
				CASE 21    vSpawnPoint 	= <<874.7046, -515.7708, 56.3730>>    fSpawnHeading 	= 217.4400    BREAK
				CASE 22    vSpawnPoint 	= <<884.4017, -508.8406, 56.3431>>    fSpawnHeading 	= 205.2478    BREAK
				CASE 23    vSpawnPoint 	= <<873.2754, -516.8651, 56.3714>>    fSpawnHeading 	= 217.4400    BREAK
				CASE 24    vSpawnPoint 	= <<879.5620, -513.8776, 56.2068>>    fSpawnHeading 	= 217.4400    BREAK
				CASE 25    vSpawnPoint 	= <<877.9250, -514.9755, 56.2518>>    fSpawnHeading 	= 217.4400    BREAK
				CASE 26    vSpawnPoint 	= <<881.9274, -512.1732, 56.2348>>    fSpawnHeading 	= 213.6399    BREAK
				CASE 27    vSpawnPoint 	= <<876.2750, -515.9240, 56.3727>>    fSpawnHeading 	= 217.4400    BREAK
				CASE 28    vSpawnPoint 	= <<883.8260, -510.9079, 56.4015>>    fSpawnHeading 	= 213.3066    BREAK
				CASE 29    vSpawnPoint 	= <<874.7527, -517.3654, 56.3741>>    fSpawnHeading 	= 217.4400    BREAK
				CASE 30    vSpawnPoint 	= <<887.1552, -509.0055, 56.3889>>    fSpawnHeading 	= 192.6829    BREAK
				CASE 31    vSpawnPoint 	= <<872.7526, -519.2885, 56.3730>>    fSpawnHeading 	= 217.4400    BREAK
				DEFAULT RETURN FALSE
			ENDSWITCH		
		BREAK
		CASE FMC_DROPOFF_AUTO_SHOP_CUSTOMER_L6
			SWITCH iSpawnPoint
				CASE 0    vSpawnPoint 	= <<-1555.8230, -298.3855, 47.1645>>    fSpawnHeading 	= 227.8400    BREAK
				CASE 1    vSpawnPoint 	= <<-1557.0311, -299.7198, 47.1464>>    fSpawnHeading 	= 227.8400    BREAK
				CASE 2    vSpawnPoint 	= <<-1554.6149, -297.0512, 47.1831>>    fSpawnHeading 	= 227.8400    BREAK
				CASE 3    vSpawnPoint 	= <<-1558.2393, -301.0541, 47.1301>>    fSpawnHeading 	= 227.8400    BREAK
				CASE 4    vSpawnPoint 	= <<-1553.4067, -295.7169, 47.2000>>    fSpawnHeading 	= 227.8400    BREAK
				CASE 5    vSpawnPoint 	= <<-1554.4886, -299.5937, 47.1281>>    fSpawnHeading 	= 227.8400    BREAK
				CASE 6    vSpawnPoint 	= <<-1555.6968, -300.9279, 47.1050>>    fSpawnHeading 	= 227.8400    BREAK
				CASE 7    vSpawnPoint 	= <<-1553.2805, -298.2594, 47.1541>>    fSpawnHeading 	= 227.8400    BREAK
				CASE 8    vSpawnPoint 	= <<-1556.9049, -302.2622, 47.0892>>    fSpawnHeading 	= 227.8400    BREAK
				CASE 9    vSpawnPoint 	= <<-1552.0724, -296.9251, 47.1800>>    fSpawnHeading 	= 227.8400    BREAK
				CASE 10    vSpawnPoint 	= <<-1553.1543, -300.8018, 47.0807>>    fSpawnHeading 	= 227.8400    BREAK
				CASE 11    vSpawnPoint 	= <<-1554.3624, -302.1361, 47.0389>>    fSpawnHeading 	= 227.8400    BREAK
				CASE 12    vSpawnPoint 	= <<-1551.9462, -299.4675, 47.1226>>    fSpawnHeading 	= 227.8400    BREAK
				CASE 13    vSpawnPoint 	= <<-1555.5706, -303.4704, 47.0156>>    fSpawnHeading 	= 227.8400    BREAK
				CASE 14    vSpawnPoint 	= <<-1550.7380, -298.1332, 47.1647>>    fSpawnHeading 	= 227.8400    BREAK
				CASE 15    vSpawnPoint 	= <<-1551.8199, -302.0100, 46.9895>>    fSpawnHeading 	= 227.8400    BREAK
				CASE 16    vSpawnPoint 	= <<-1553.0281, -303.3443, 46.9420>>    fSpawnHeading 	= 227.8400    BREAK
				CASE 17    vSpawnPoint 	= <<-1550.6118, -300.6757, 47.0437>>    fSpawnHeading 	= 227.8400    BREAK
				CASE 18    vSpawnPoint 	= <<-1554.2362, -304.6786, 46.9172>>    fSpawnHeading 	= 227.8400    BREAK
				CASE 19    vSpawnPoint 	= <<-1549.4037, -299.3414, 47.0979>>    fSpawnHeading 	= 227.8400    BREAK
				CASE 20    vSpawnPoint 	= <<-1550.4856, -303.2181, 46.8713>>    fSpawnHeading 	= 227.8400    BREAK
				CASE 21    vSpawnPoint 	= <<-1551.6937, -304.5524, 46.8178>>    fSpawnHeading 	= 227.8400    BREAK
				CASE 22    vSpawnPoint 	= <<-1549.2775, -301.8839, 46.9232>>    fSpawnHeading 	= 227.8400    BREAK
				CASE 23    vSpawnPoint 	= <<-1552.9019, -305.8867, 46.7790>>    fSpawnHeading 	= 227.8400    BREAK
				CASE 24    vSpawnPoint 	= <<-1548.0693, -300.5496, 46.9730>>    fSpawnHeading 	= 227.8400    BREAK
				CASE 25    vSpawnPoint 	= <<-1549.1512, -304.4263, 46.6990>>    fSpawnHeading 	= 227.8400    BREAK
				CASE 26    vSpawnPoint 	= <<-1550.3594, -305.7606, 46.6059>>    fSpawnHeading 	= 227.8400    BREAK
				CASE 27    vSpawnPoint 	= <<-1547.9431, -303.0920, 46.7920>>    fSpawnHeading 	= 227.8400    BREAK
				CASE 28    vSpawnPoint 	= <<-1551.5675, -307.0949, 46.5102>>    fSpawnHeading 	= 227.8400    BREAK
				CASE 29    vSpawnPoint 	= <<-1546.7350, -301.7577, 46.8862>>    fSpawnHeading 	= 227.8400    BREAK
				CASE 30    vSpawnPoint 	= <<-1547.8169, -305.6345, 46.7032>>    fSpawnHeading 	= 227.8400    BREAK
				CASE 31    vSpawnPoint 	= <<-1549.0250, -306.9688, 46.6102>>    fSpawnHeading 	= 227.8400    BREAK
				DEFAULT RETURN FALSE
			ENDSWITCH		
		BREAK
		CASE FMC_DROPOFF_AUTO_SHOP_CUSTOMER_L7
			SWITCH iSpawnPoint
				CASE 0    vSpawnPoint 	= <<-1011.9918, -2742.2380, 12.7585>>    fSpawnHeading 	= 241.9650    BREAK
				CASE 1    vSpawnPoint 	= <<-1013.1393, -2742.8228, 12.7590>>    fSpawnHeading 	= 241.9650    BREAK
				CASE 2    vSpawnPoint 	= <<-1011.9830, -2740.7456, 12.7578>>    fSpawnHeading 	= 241.9650    BREAK
				CASE 3    vSpawnPoint 	= <<-1014.2985, -2743.6865, 12.7597>>    fSpawnHeading 	= 241.9650    BREAK
				CASE 4    vSpawnPoint 	= <<-1012.0072, -2739.5200, 12.7573>>    fSpawnHeading 	= 241.9650    BREAK
				CASE 5    vSpawnPoint 	= <<-1010.5309, -2742.9514, 12.7585>>    fSpawnHeading 	= 241.9650    BREAK
				CASE 6    vSpawnPoint 	= <<-1011.1372, -2743.7832, 12.7590>>    fSpawnHeading 	= 241.9650    BREAK
				CASE 7    vSpawnPoint 	= <<-1009.9110, -2741.8403, 12.7578>>    fSpawnHeading 	= 241.9650    BREAK
				CASE 8    vSpawnPoint 	= <<-1012.2308, -2744.9143, 12.7598>>    fSpawnHeading 	= 241.9650    BREAK
				CASE 9    vSpawnPoint 	= <<-1009.2958, -2740.7056, 12.7571>>    fSpawnHeading 	= 241.9650    BREAK
				CASE 10    vSpawnPoint 	= <<-1009.4648, -2744.1672, 12.7588>>    fSpawnHeading 	= 241.9650    BREAK
				CASE 11    vSpawnPoint 	= <<-1010.6228, -2745.0256, 12.7594>>    fSpawnHeading 	= 241.9650    BREAK
				CASE 12    vSpawnPoint 	= <<-1008.9236, -2742.9993, 12.7581>>    fSpawnHeading 	= 241.9650    BREAK
				CASE 13    vSpawnPoint 	= <<-1009.4998, -2745.7505, 12.7587>>    fSpawnHeading 	= 241.9650    BREAK
				CASE 14    vSpawnPoint 	= <<-1008.1514, -2741.8369, 12.7574>>    fSpawnHeading 	= 241.9650    BREAK
				CASE 15    vSpawnPoint 	= <<-1007.5979, -2744.4058, 12.7579>>    fSpawnHeading 	= 241.9650    BREAK
				CASE 16    vSpawnPoint 	= <<-1008.1600, -2745.6467, 12.7579>>    fSpawnHeading 	= 241.9650    BREAK
				CASE 17    vSpawnPoint 	= <<-1007.0906, -2743.2717, 12.7578>>    fSpawnHeading 	= 241.9650    BREAK
				CASE 18    vSpawnPoint 	= <<-1008.9395, -2746.8708, 12.7580>>    fSpawnHeading 	= 241.9650    BREAK
				CASE 19    vSpawnPoint 	= <<-1006.4342, -2742.1924, 12.7571>>    fSpawnHeading 	= 241.9650    BREAK
				CASE 20    vSpawnPoint 	= <<-1005.9960, -2745.8906, 12.7566>>    fSpawnHeading 	= 241.9650    BREAK
				CASE 21    vSpawnPoint 	= <<-1006.7097, -2747.2175, 12.7566>>    fSpawnHeading 	= 241.9650    BREAK
				CASE 22    vSpawnPoint 	= <<-1005.4406, -2744.4392, 12.7566>>    fSpawnHeading 	= 241.9650    BREAK
				CASE 23    vSpawnPoint 	= <<-1007.2489, -2748.2522, 12.7566>>    fSpawnHeading 	= 241.9650    BREAK
				CASE 24    vSpawnPoint 	= <<-1004.2490, -2743.1145, 12.7566>>    fSpawnHeading 	= 241.9650    BREAK
				CASE 25    vSpawnPoint 	= <<-1001.5872, -2747.4709, 12.7566>>    fSpawnHeading 	= 241.9650    BREAK
				CASE 26    vSpawnPoint 	= <<-1003.9101, -2748.1199, 12.7566>>    fSpawnHeading 	= 241.9650    BREAK
				CASE 27    vSpawnPoint 	= <<-1002.5576, -2745.7031, 12.7566>>    fSpawnHeading 	= 241.9650    BREAK
				CASE 28    vSpawnPoint 	= <<-1005.1085, -2750.1084, 12.7566>>    fSpawnHeading 	= 241.9650    BREAK
				CASE 29    vSpawnPoint 	= <<-1001.3039, -2744.6006, 12.7566>>    fSpawnHeading 	= 241.9650    BREAK
				CASE 30    vSpawnPoint 	= <<-996.9316, -2747.0542, 12.7566>>    fSpawnHeading 	= 241.9650    BREAK
				CASE 31    vSpawnPoint 	= <<-1001.0746, -2750.8396, 12.7566>>    fSpawnHeading 	= 241.9650    BREAK
				DEFAULT RETURN FALSE
			ENDSWITCH		
		BREAK
		CASE FMC_DROPOFF_AUTO_SHOP_CUSTOMER_L8
			SWITCH iSpawnPoint
				CASE 0    vSpawnPoint 	= <<-627.6099, 188.1829, 67.2672>>    fSpawnHeading 	= 91.2900    BREAK
				CASE 1    vSpawnPoint 	= <<-626.7203, 190.3506, 67.8756>>    fSpawnHeading 	= 91.2900    BREAK
				CASE 2    vSpawnPoint 	= <<-626.9240, 186.4211, 66.8774>>    fSpawnHeading 	= 91.2900    BREAK
				CASE 3    vSpawnPoint 	= <<-626.7796, 192.2087, 68.3053>>    fSpawnHeading 	= 91.2900    BREAK
				CASE 4    vSpawnPoint 	= <<-627.6679, 182.8091, 65.8183>>    fSpawnHeading 	= 91.2900    BREAK
				CASE 5    vSpawnPoint 	= <<-628.1556, 192.9405, 68.3380>>    fSpawnHeading 	= 91.2900    BREAK
				CASE 6    vSpawnPoint 	= <<-629.3879, 187.0788, 66.6684>>    fSpawnHeading 	= 91.2900    BREAK
				CASE 7    vSpawnPoint 	= <<-629.9278, 189.8741, 67.4163>>    fSpawnHeading 	= 91.2900    BREAK
				CASE 8    vSpawnPoint 	= <<-628.8543, 184.8547, 66.1415>>    fSpawnHeading 	= 91.2900    BREAK
				CASE 9    vSpawnPoint 	= <<-628.4518, 191.1634, 67.8835>>    fSpawnHeading 	= 91.2900    BREAK
				CASE 10    vSpawnPoint 	= <<-630.5839, 182.5464, 65.2600>>    fSpawnHeading 	= 91.2900    BREAK
				CASE 11    vSpawnPoint 	= <<-629.4736, 193.6239, 68.4221>>    fSpawnHeading 	= 91.2900    BREAK
				CASE 12    vSpawnPoint 	= <<-631.3083, 187.7198, 66.6261>>    fSpawnHeading 	= 91.2900    BREAK
				CASE 13    vSpawnPoint 	= <<-631.3297, 189.9269, 67.2712>>    fSpawnHeading 	= 91.2900    BREAK
				CASE 14    vSpawnPoint 	= <<-630.6173, 186.0896, 66.1844>>    fSpawnHeading 	= 91.2900    BREAK
				CASE 15    vSpawnPoint 	= <<-630.8812, 191.3100, 67.6629>>    fSpawnHeading 	= 91.2900    BREAK
				CASE 16    vSpawnPoint 	= <<-632.1068, 184.0549, 65.3961>>    fSpawnHeading 	= 91.2900    BREAK
				CASE 17    vSpawnPoint 	= <<-631.0272, 193.6348, 68.3015>>    fSpawnHeading 	= 91.2900    BREAK
				CASE 18    vSpawnPoint 	= <<-633.0806, 187.7252, 66.3944>>    fSpawnHeading 	= 91.2900    BREAK
				CASE 19    vSpawnPoint 	= <<-632.8481, 189.8396, 67.0935>>    fSpawnHeading 	= 91.2900    BREAK
				CASE 20    vSpawnPoint 	= <<-633.1104, 185.8789, 65.7820>>    fSpawnHeading 	= 91.2900    BREAK
				CASE 21    vSpawnPoint 	= <<-632.4808, 191.2740, 67.5243>>    fSpawnHeading 	= 91.2900    BREAK
				CASE 22    vSpawnPoint 	= <<-633.2818, 182.4401, 64.7758>>    fSpawnHeading 	= 91.2900    BREAK
				CASE 23    vSpawnPoint 	= <<-633.0287, 192.9955, 67.9619>>    fSpawnHeading 	= 91.2900    BREAK
				CASE 24    vSpawnPoint 	= <<-634.6408, 188.0616, 66.3085>>    fSpawnHeading 	= 91.2900    BREAK
				CASE 25    vSpawnPoint 	= <<-634.5385, 190.4171, 67.0652>>    fSpawnHeading 	= 91.2900    BREAK
				CASE 26    vSpawnPoint 	= <<-635.0911, 184.6751, 65.1279>>    fSpawnHeading 	= 91.2900    BREAK
				CASE 27    vSpawnPoint 	= <<-636.1261, 191.4432, 67.1933>>    fSpawnHeading 	= 91.2900    BREAK
				CASE 28    vSpawnPoint 	= <<-635.3190, 182.2772, 64.4273>>    fSpawnHeading 	= 91.2900    BREAK
				CASE 29    vSpawnPoint 	= <<-634.9794, 192.8967, 67.7654>>    fSpawnHeading 	= 91.2900    BREAK
				CASE 30    vSpawnPoint 	= <<-636.1522, 186.7233, 65.6708>>    fSpawnHeading 	= 91.2900    BREAK
				CASE 31    vSpawnPoint 	= <<-636.1997, 189.2993, 66.5199>>    fSpawnHeading 	= 91.2900    BREAK
				DEFAULT RETURN FALSE
			ENDSWITCH		
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL GET_FMC_HARMONY_POST_SCENE_SPAWN_POINT(INT iSpawnPoint, VECTOR &vSpawnPoint, FLOAT &fSpawnHeading)
	SWITCH iSpawnPoint
		CASE 0    vSpawnPoint 	= <<188.7012, 2806.3174, 44.5673>>    fSpawnHeading 	= 9.6646    BREAK
		CASE 1    vSpawnPoint 	= <<190.5684, 2807.8066, 44.4249>>    fSpawnHeading 	= 9.8425    BREAK
		CASE 2    vSpawnPoint 	= <<186.6343, 2807.2373, 44.5016>>    fSpawnHeading 	= 13.985    BREAK
		CASE 3    vSpawnPoint 	= <<192.6192, 2807.0679, 44.5379>>    fSpawnHeading 	= 5.6979    BREAK
		CASE 4    vSpawnPoint 	= <<185.4271, 2805.4729, 44.6061>>    fSpawnHeading 	= 9.5306    BREAK
		CASE 5    vSpawnPoint 	= <<194.4886, 2808.8447, 44.3457>>    fSpawnHeading 	= 9.8807    BREAK
		CASE 6    vSpawnPoint 	= <<189.9418, 2811.6282, 44.0882>>    fSpawnHeading 	= 357.22    BREAK
		CASE 7    vSpawnPoint 	= <<187.3170, 2811.0010, 44.1460>>    fSpawnHeading 	= 9.6074    BREAK
		CASE 8    vSpawnPoint 	= <<192.5020, 2810.8914, 44.1468>>    fSpawnHeading 	= 1.6163    BREAK
		CASE 9    vSpawnPoint 	= <<184.5147, 2811.1936, 44.1365>>    fSpawnHeading 	= 1.2995    BREAK
		CASE 10   vSpawnPoint 	= <<194.3472, 2816.4626, 43.5240>>    fSpawnHeading 	= 357.48    BREAK
		CASE 11   vSpawnPoint 	= <<191.3824, 2815.4758, 43.6203>>    fSpawnHeading 	= 357.48    BREAK
		CASE 12   vSpawnPoint 	= <<188.9817, 2816.6978, 43.5210>>    fSpawnHeading 	= 357.48    BREAK
		CASE 13   vSpawnPoint 	= <<192.9230, 2813.6362, 43.7733>>    fSpawnHeading 	= 357.48    BREAK
		CASE 14   vSpawnPoint 	= <<185.4713, 2817.2227, 43.4774>>    fSpawnHeading 	= 357.48    BREAK
		CASE 15   vSpawnPoint 	= <<191.2197, 2822.1211, 43.3248>>    fSpawnHeading 	= 357.48    BREAK
		CASE 16   vSpawnPoint 	= <<193.1866, 2820.5259, 43.3378>>    fSpawnHeading 	= 357.48    BREAK
		CASE 17   vSpawnPoint 	= <<188.1022, 2821.0708, 43.3548>>    fSpawnHeading 	= 357.48    BREAK
		CASE 18   vSpawnPoint 	= <<191.5564, 2818.5098, 43.3749>>    fSpawnHeading 	= 357.48    BREAK
		CASE 19   vSpawnPoint 	= <<185.2663, 2821.9172, 43.3555>>    fSpawnHeading 	= 357.48    BREAK
		CASE 20   vSpawnPoint 	= <<188.2330, 2826.0591, 43.3144>>    fSpawnHeading 	= 349.44    BREAK
		CASE 21   vSpawnPoint 	= <<191.7887, 2826.7805, 43.2992>>    fSpawnHeading 	= 357.48    BREAK
		CASE 22   vSpawnPoint 	= <<185.0997, 2826.8174, 43.3322>>    fSpawnHeading 	= 336.85    BREAK
		CASE 23   vSpawnPoint 	= <<194.8165, 2824.1521, 43.2802>>    fSpawnHeading 	= 9.8275    BREAK
		CASE 24   vSpawnPoint 	= <<189.9915, 2828.5500, 43.3144>>    fSpawnHeading 	= 357.48    BREAK
		CASE 25   vSpawnPoint 	= <<192.3548, 2831.3064, 43.3212>>    fSpawnHeading 	= 357.48    BREAK
		CASE 26   vSpawnPoint 	= <<194.3779, 2828.4077, 43.2905>>    fSpawnHeading 	= 357.48    BREAK
		CASE 27   vSpawnPoint 	= <<189.9015, 2833.1394, 43.3654>>    fSpawnHeading 	= 357.48    BREAK
		CASE 28   vSpawnPoint 	= <<187.3686, 2829.9346, 43.3334>>    fSpawnHeading 	= 357.48    BREAK
		CASE 29   vSpawnPoint 	= <<186.3003, 2833.5801, 43.4055>>    fSpawnHeading 	= 357.48    BREAK
		CASE 30   vSpawnPoint 	= <<188.6639, 2836.4138, 43.4356>>    fSpawnHeading 	= 357.48    BREAK
		CASE 31   vSpawnPoint 	= <<192.6770, 2835.8774, 43.3780>>    fSpawnHeading 	= 357.48    BREAK
		CASE 32   vSpawnPoint 	= <<194.4776, 2838.4080, 43.4074>>    fSpawnHeading 	= 357.48    BREAK
		CASE 33   vSpawnPoint 	= <<194.7422, 2833.0796, 43.3229>>    fSpawnHeading 	= 357.48    BREAK
		CASE 34   vSpawnPoint 	= <<191.4016, 2839.0315, 43.4686>>    fSpawnHeading 	= 357.48    BREAK
		CASE 35   vSpawnPoint 	= <<193.5422, 2843.2654, 43.5364>>    fSpawnHeading 	= 336.79    BREAK
		CASE 36   vSpawnPoint 	= <<196.7697, 2842.3560, 43.4360>>    fSpawnHeading 	= 349.34    BREAK
		CASE 37   vSpawnPoint 	= <<191.0427, 2843.9036, 43.6154>>    fSpawnHeading 	= 340.97    BREAK
		CASE 38   vSpawnPoint 	= <<188.3177, 2840.1753, 43.5577>>    fSpawnHeading 	= 345.10    BREAK
		CASE 39   vSpawnPoint 	= <<188.0195, 2844.1689, 43.6935>>    fSpawnHeading 	= 332.95    BREAK
		DEFAULT RETURN FALSE
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL GET_ELYSIAN_ISLAND_WHOUSE_DEFENCES_POST_SCENE_SPAWN_POINT(INT iSpawnPoint, VECTOR &vSpawnPoint, FLOAT &fSpawnHeading)
	SWITCH iSpawnPoint
		CASE 0    vSpawnPoint 	= <<-155.3064, -2651.4983, 5.0010>>    fSpawnHeading 	= 0.3600    BREAK
		CASE 1    vSpawnPoint 	= <<-153.7906, -2650.4429, 5.0010>>    fSpawnHeading 	= 0.3600    BREAK
		CASE 2    vSpawnPoint 	= <<-156.8175, -2650.6233, 5.0472>>    fSpawnHeading 	= 0.3600    BREAK
		CASE 3    vSpawnPoint 	= <<-152.0706, -2651.2205, 5.0007>>    fSpawnHeading 	= 0.3600    BREAK
		CASE 4    vSpawnPoint 	= <<-159.0528, -2650.6843, 5.0010>>    fSpawnHeading 	= 0.3600    BREAK
		CASE 5    vSpawnPoint 	= <<-150.4169, -2650.4114, 5.0007>>    fSpawnHeading 	= 0.3600    BREAK
		CASE 6    vSpawnPoint 	= <<-154.6948, -2648.5413, 5.0027>>    fSpawnHeading 	= 0.3600    BREAK
		CASE 7    vSpawnPoint 	= <<-153.2624, -2648.8674, 5.0015>>    fSpawnHeading 	= 0.3600    BREAK
		CASE 8    vSpawnPoint 	= <<-156.8710, -2648.2957, 5.0472>>    fSpawnHeading 	= 0.3600    BREAK
		CASE 9    vSpawnPoint 	= <<-151.2217, -2648.5618, 5.0026>>    fSpawnHeading 	= 0.3600    BREAK
		CASE 10    vSpawnPoint 	= <<-159.5098, -2648.6431, 5.0023>>    fSpawnHeading 	= 0.3600    BREAK
		CASE 11    vSpawnPoint 	= <<-148.6107, -2649.4563, 5.0010>>    fSpawnHeading 	= 0.3600    BREAK
		CASE 12    vSpawnPoint 	= <<-155.1601, -2646.9941, 5.0084>>    fSpawnHeading 	= 0.3600    BREAK
		CASE 13    vSpawnPoint 	= <<-153.7620, -2646.5754, 5.0093>>    fSpawnHeading 	= 0.3600    BREAK
		CASE 14    vSpawnPoint 	= <<-157.7816, -2646.5447, 5.0100>>    fSpawnHeading 	= 0.3600    BREAK
		CASE 15    vSpawnPoint 	= <<-152.3393, -2647.3979, 5.0064>>    fSpawnHeading 	= 0.3600    BREAK
		CASE 16    vSpawnPoint 	= <<-159.7165, -2646.5059, 5.0101>>    fSpawnHeading 	= 0.3600    BREAK
		CASE 17    vSpawnPoint 	= <<-149.5632, -2647.5527, 5.0059>>    fSpawnHeading 	= 0.3600    BREAK
		CASE 18    vSpawnPoint 	= <<-154.5556, -2644.0776, 5.0187>>    fSpawnHeading 	= 0.3600    BREAK
		CASE 19    vSpawnPoint 	= <<-152.4772, -2644.4172, 5.0175>>    fSpawnHeading 	= 0.3600    BREAK
		CASE 20    vSpawnPoint 	= <<-156.2637, -2645.0859, 5.0152>>    fSpawnHeading 	= 0.3600    BREAK
		CASE 21    vSpawnPoint 	= <<-150.4486, -2644.7556, 5.0164>>    fSpawnHeading 	= 0.3600    BREAK
		CASE 22    vSpawnPoint 	= <<-159.9252, -2644.4666, 5.0173>>    fSpawnHeading 	= 0.3600    BREAK
		CASE 23    vSpawnPoint 	= <<-151.3197, -2646.0786, 5.0111>>    fSpawnHeading 	= 0.3600    BREAK
		CASE 24    vSpawnPoint 	= <<-153.4367, -2642.7590, 5.0234>>    fSpawnHeading 	= 0.3600    BREAK
		CASE 25    vSpawnPoint 	= <<-151.7793, -2641.8484, 5.0267>>    fSpawnHeading 	= 0.3600    BREAK
		CASE 26    vSpawnPoint 	= <<-156.2066, -2642.2068, 5.0254>>    fSpawnHeading 	= 0.3600    BREAK
		CASE 27    vSpawnPoint 	= <<-149.7055, -2642.4363, 5.0246>>    fSpawnHeading 	= 0.3600    BREAK
		CASE 28    vSpawnPoint 	= <<-158.6255, -2641.4626, 5.0281>>    fSpawnHeading 	= 0.3600    BREAK
		CASE 29    vSpawnPoint 	= <<-157.9236, -2643.6626, 5.0201>>    fSpawnHeading 	= 0.3600    BREAK
		CASE 30    vSpawnPoint 	= <<-154.4362, -2640.6023, 5.0312>>    fSpawnHeading 	= 0.3600    BREAK
		CASE 31    vSpawnPoint 	= <<-151.9839, -2639.7070, 5.0343>>    fSpawnHeading 	= 0.3600    BREAK
		CASE 32    vSpawnPoint 	= <<-157.0520, -2639.8540, 5.0338>>    fSpawnHeading 	= 0.3600    BREAK
		CASE 33    vSpawnPoint 	= <<-150.1383, -2639.3462, 5.0356>>    fSpawnHeading 	= 0.3600    BREAK
		CASE 34    vSpawnPoint 	= <<-159.3374, -2639.7585, 5.0339>>    fSpawnHeading 	= 0.3600    BREAK
		CASE 35    vSpawnPoint 	= <<-154.5900, -2638.3621, 5.0391>>    fSpawnHeading 	= 0.3600    BREAK
		CASE 36    vSpawnPoint 	= <<-157.1000, -2631.9780, 5.0263>>    fSpawnHeading 	= 0.3600    BREAK
		CASE 37    vSpawnPoint 	= <<-154.6393, -2633.3784, 5.0378>>    fSpawnHeading 	= 0.3600    BREAK
		CASE 38    vSpawnPoint 	= <<-160.3246, -2631.3625, 5.0215>>    fSpawnHeading 	= 0.3600    BREAK
		CASE 39    vSpawnPoint 	= <<-151.1673, -2631.5464, 5.0227>>    fSpawnHeading 	= 0.3600    BREAK
		DEFAULT RETURN FALSE
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL FMC_GET_SPAWN_POINT_FOR_MORNINGWOOD_LOCKUP(INT iSpawnPoint, VECTOR &vSpawnPoint, FLOAT &fSpawnHeading)
	SWITCH iSpawnPoint
		CASE 0 vSpawnPoint 		= <<-1421.8895, -240.8550, 45.3791>> 	fSpawnHeading 	= 159.8400 	RETURN TRUE	BREAK
		CASE 1 vSpawnPoint 		= <<-1422.6659, -239.4049, 45.3791>> 	fSpawnHeading 	= 140.6785 	RETURN TRUE	BREAK
		CASE 2 vSpawnPoint 		= <<-1419.9780, -241.1484, 45.3791>> 	fSpawnHeading 	= 159.8400 	RETURN TRUE	BREAK
		CASE 3 vSpawnPoint 		= <<-1423.1282, -237.9081, 45.3791>> 	fSpawnHeading 	= 145.7831 	RETURN TRUE	BREAK
		CASE 4 vSpawnPoint 		= <<-1418.1815, -242.2163, 45.3791>> 	fSpawnHeading 	= 159.8400 	RETURN TRUE	BREAK
		CASE 5 vSpawnPoint 		= <<-1422.5702, -242.7090, 45.3791>> 	fSpawnHeading 	= 159.8400 	RETURN TRUE	BREAK
		CASE 6 vSpawnPoint 		= <<-1424.4242, -242.0283, 45.3791>> 	fSpawnHeading 	= 159.8400 	RETURN TRUE	BREAK
		CASE 7 vSpawnPoint 		= <<-1420.7162, -243.3897, 45.3791>> 	fSpawnHeading 	= 159.8400 	RETURN TRUE	BREAK
		CASE 8 vSpawnPoint 		= <<-1426.2782, -241.3477, 45.3791>> 	fSpawnHeading 	= 159.8400 	RETURN TRUE	BREAK
		CASE 9 vSpawnPoint 		= <<-1418.4403, -244.5638, 45.3791>> 	fSpawnHeading 	= 198.0757 	RETURN TRUE	BREAK
		CASE 10 vSpawnPoint 	= <<-1423.2509, -244.5630, 45.3791>> 	fSpawnHeading 	= 159.8400 	RETURN TRUE	BREAK
		CASE 11 vSpawnPoint 	= <<-1425.1049, -243.8823, 45.3791>> 	fSpawnHeading 	= 159.8400 	RETURN TRUE	BREAK
		CASE 12 vSpawnPoint 	= <<-1421.7312, -245.8801, 45.3791>> 	fSpawnHeading 	= 202.1228 	RETURN TRUE	BREAK
		CASE 13 vSpawnPoint 	= <<-1426.9589, -243.2017, 45.3791>> 	fSpawnHeading 	= 159.8400 	RETURN TRUE	BREAK
		CASE 14 vSpawnPoint 	= <<-1419.7930, -246.6821, 45.3584>> 	fSpawnHeading 	= 193.7357 	RETURN TRUE	BREAK
		CASE 15 vSpawnPoint 	= <<-1427.5627, -245.5330, 45.3791>>	fSpawnHeading 	= 134.0556 	RETURN TRUE	BREAK
		CASE 16	vSpawnPoint 	= <<-1409.9904, -257.3475, 45.3494>>	fSpawnHeading 	= 204.2961	RETURN TRUE	BREAK
		CASE 17	vSpawnPoint 	= <<-1436.5670, -253.3840, 45.2926>>	fSpawnHeading 	= 138.9874	RETURN TRUE	BREAK
		CASE 18	vSpawnPoint 	= <<-1439.5281, -254.8211, 45.2324>>	fSpawnHeading 	= 126.6357	RETURN TRUE	BREAK
		CASE 19	vSpawnPoint 	= <<-1413.7655, -256.3324, 45.3791>>	fSpawnHeading 	= 221.5997	RETURN TRUE	BREAK
		CASE 20	vSpawnPoint 	= <<-1437.9916, -256.3392, 45.2363>>	fSpawnHeading 	= 138.5611	RETURN TRUE	BREAK
		CASE 21	vSpawnPoint 	= <<-1408.3815, -261.3486, 45.3791>>	fSpawnHeading 	= 212.6250	RETURN TRUE	BREAK
		CASE 22	vSpawnPoint 	= <<-1407.0198, -258.6854, 45.3791>>	fSpawnHeading 	= 191.9588	RETURN TRUE	BREAK
		CASE 23	vSpawnPoint 	= <<-1406.0466, -261.9364, 45.3431>>	fSpawnHeading 	= 203.9846	RETURN TRUE	BREAK
		CASE 24	vSpawnPoint 	= <<-1410.9063, -259.5887, 45.3791>>	fSpawnHeading 	= 203.9389	RETURN TRUE	BREAK
		CASE 25	vSpawnPoint 	= <<-1409.1400, -263.4220, 45.3791>>	fSpawnHeading 	= 199.6243	RETURN TRUE	BREAK
		CASE 26	vSpawnPoint 	= <<-1439.1086, -262.6098, 45.2077>>	fSpawnHeading 	= 130.3991	RETURN TRUE	BREAK
		CASE 27	vSpawnPoint 	= <<-1440.4532, -260.7457, 45.2077>>	fSpawnHeading 	= 125.8554	RETURN TRUE	BREAK
		CASE 28	vSpawnPoint 	= <<-1444.2335, -259.2365, 45.2077>>	fSpawnHeading 	= 133.9451	RETURN TRUE	BREAK
		CASE 29	vSpawnPoint 	= <<-1441.4536, -258.3626, 45.2078>>	fSpawnHeading 	= 130.5721	RETURN TRUE	BREAK
		CASE 30	vSpawnPoint 	= <<-1443.5177, -261.4785, 45.2077>>	fSpawnHeading 	= 138.7202	RETURN TRUE	BREAK
		CASE 31	vSpawnPoint 	= <<-1442.3074, -263.7374, 45.2077>>	fSpawnHeading 	= 134.5693	RETURN TRUE	BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL FMC_GET_SPAWN_POINT_FOR_FRANKLINS_HOUSE(INT iSpawnPoint, VECTOR &vSpawnPoint, FLOAT &fSpawnHeading)
	SWITCH iSpawnPoint
		CASE 0    vSpawnPoint 	= <<219.4214, 1.5931, 73.8582>>    fSpawnHeading 	= 337.4561    RETURN TRUE	BREAK
		CASE 1    vSpawnPoint 	= <<221.6229, 1.5499, 74.1173>>    fSpawnHeading 	= 337.3287    RETURN TRUE	BREAK
		CASE 2    vSpawnPoint 	= <<217.6575, 3.1070, 74.1775>>    fSpawnHeading 	= 341.3682    RETURN TRUE	BREAK
		CASE 3    vSpawnPoint 	= <<221.6332, 3.7341, 74.8140>>    fSpawnHeading 	= 341.4460    RETURN TRUE	BREAK
		CASE 4    vSpawnPoint 	= <<219.4165, 4.6941, 74.8770>>    fSpawnHeading 	= 337.4311    RETURN TRUE	BREAK
		CASE 5    vSpawnPoint 	= <<223.1590, 4.9270, 75.4618>>    fSpawnHeading 	= 341.4144    RETURN TRUE	BREAK
		CASE 6    vSpawnPoint 	= <<220.2513, 8.4665, 76.2456>>    fSpawnHeading 	= 341.5436    RETURN TRUE	BREAK
		CASE 7    vSpawnPoint 	= <<221.0089, 6.1881, 75.6005>>    fSpawnHeading 	= 341.5718    RETURN TRUE	BREAK
		CASE 8    vSpawnPoint 	= <<222.8559, 7.0797, 76.1173>>    fSpawnHeading 	= 341.4365    RETURN TRUE	BREAK
		CASE 9    vSpawnPoint 	= <<218.7437, 6.5531, 75.4854>>    fSpawnHeading 	= 341.0323    RETURN TRUE	BREAK
		CASE 10    vSpawnPoint 	= <<223.1760, -4.9500, 72.6668>>    fSpawnHeading 	= 250.8200    RETURN TRUE	BREAK
		CASE 11    vSpawnPoint 	= <<222.7258, -7.0385, 72.7397>>    fSpawnHeading 	= 250.8200    RETURN TRUE	BREAK
		CASE 12    vSpawnPoint 	= <<224.9717, -4.3152, 72.8108>>    fSpawnHeading 	= 250.8200    RETURN TRUE	BREAK
		CASE 13    vSpawnPoint 	= <<225.8801, -8.3838, 72.7429>>    fSpawnHeading 	= 250.8200    RETURN TRUE	BREAK
		CASE 14    vSpawnPoint 	= <<224.9446, 8.0969, 76.6408>>    fSpawnHeading 	= 337.4257    RETURN TRUE	BREAK
		CASE 15    vSpawnPoint 	= <<225.7694, -6.0723, 72.6328>>    fSpawnHeading 	= 250.8200    RETURN TRUE	BREAK
		CASE 16    vSpawnPoint 	= <<227.1693, -7.4375, 72.6696>>    fSpawnHeading 	= 250.8200    RETURN TRUE	BREAK
		CASE 17    vSpawnPoint 	= <<227.4994, -5.2762, 72.7498>>    fSpawnHeading 	= 250.8200    RETURN TRUE	BREAK
		CASE 18    vSpawnPoint 	= <<228.9252, -8.6134, 72.6969>>    fSpawnHeading 	= 250.8200    RETURN TRUE	BREAK
		CASE 19    vSpawnPoint 	= <<232.1034, -7.0522, 72.7111>>    fSpawnHeading 	= 250.8200    RETURN TRUE	BREAK
		CASE 20    vSpawnPoint 	= <<230.4831, -7.8249, 72.6283>>    fSpawnHeading 	= 250.8200    RETURN TRUE	BREAK
		CASE 21    vSpawnPoint 	= <<231.2063, -9.6596, 72.6281>>    fSpawnHeading 	= 250.8200    RETURN TRUE	BREAK
		CASE 22    vSpawnPoint 	= <<232.8303, -8.2076, 72.6227>>    fSpawnHeading 	= 250.8200    RETURN TRUE	BREAK
		CASE 23    vSpawnPoint 	= <<233.6082, -10.0650, 72.5900>>    fSpawnHeading 	= 250.8200    RETURN TRUE	BREAK
		CASE 24    vSpawnPoint 	= <<228.8863, -6.3465, 72.7066>>    fSpawnHeading 	= 250.8200    RETURN TRUE	BREAK
		CASE 25    vSpawnPoint 	= <<234.9592, -9.1090, 72.6085>>    fSpawnHeading 	= 250.8200    RETURN TRUE	BREAK
		CASE 26    vSpawnPoint 	= <<235.7239, -11.0547, 72.5962>>    fSpawnHeading 	= 250.8200    RETURN TRUE	BREAK
		CASE 27    vSpawnPoint 	= <<236.4721, -8.6577, 72.6957>>    fSpawnHeading 	= 250.8200    RETURN TRUE	BREAK
		CASE 28    vSpawnPoint 	= <<236.9771, -10.2139, 72.5924>>    fSpawnHeading 	= 250.8200    RETURN TRUE	BREAK
		CASE 29    vSpawnPoint 	= <<238.1813, -12.2572, 72.6254>>    fSpawnHeading 	= 250.8200    RETURN TRUE	BREAK
		CASE 30    vSpawnPoint 	= <<238.8847, -9.7027, 72.6798>>    fSpawnHeading 	= 250.8200    RETURN TRUE	BREAK
		CASE 31    vSpawnPoint 	= <<239.7619, -11.4821, 72.6040>>    fSpawnHeading 	= 250.8200    RETURN TRUE	BREAK
		CASE 32    vSpawnPoint 	= <<241.2168, -10.5020, 72.6846>>    fSpawnHeading 	= 250.8200    RETURN TRUE	BREAK
		CASE 33    vSpawnPoint 	= <<241.0755, -13.2541, 72.6593>>    fSpawnHeading 	= 250.8200    RETURN TRUE	BREAK
		CASE 34    vSpawnPoint 	= <<248.1483, -13.6944, 72.6145>>    fSpawnHeading 	= 250.8200    RETURN TRUE	BREAK
		CASE 35    vSpawnPoint 	= <<243.5738, -11.3233, 72.6654>>    fSpawnHeading 	= 250.8200    RETURN TRUE	BREAK
		CASE 36    vSpawnPoint 	= <<243.6026, -13.8289, 72.6482>>    fSpawnHeading 	= 250.8200    RETURN TRUE	BREAK
		CASE 37    vSpawnPoint 	= <<246.4070, -11.5305, 72.6985>>    fSpawnHeading 	= 250.8200    RETURN TRUE	BREAK
		CASE 38    vSpawnPoint 	= <<242.3971, -12.3230, 72.6000>>    fSpawnHeading 	= 250.8200    RETURN TRUE	BREAK
		CASE 39    vSpawnPoint 	= <<247.9962, -15.5816, 72.6603>>    fSpawnHeading 	= 250.8200    RETURN TRUE	BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

#IF FEATURE_DLC_2_2022
FUNC BOOL GET_FMC_TREVOR_METH_LAB_POST_SCENE_SPAWN_POINT(INT iSpawnPoint, VECTOR &vSpawnPoint, FLOAT &fSpawnHeading)
	SWITCH iSpawnPoint
		CASE 0    vSpawnPoint 	= <<1370.9554, 3603.9360, 33.8941>>    fSpawnHeading 	= 198.7200    BREAK
		CASE 1    vSpawnPoint 	= <<1368.8369, 3601.8074, 33.9008>>    fSpawnHeading 	= 198.7200    BREAK
		CASE 2    vSpawnPoint 	= <<1372.3011, 3604.4292, 33.8941>>    fSpawnHeading 	= 198.7200    BREAK
		CASE 3    vSpawnPoint 	= <<1365.7052, 3601.1353, 33.8966>>    fSpawnHeading 	= 198.7200    BREAK
		CASE 4    vSpawnPoint 	= <<1373.6307, 3604.9536, 33.8941>>    fSpawnHeading 	= 198.7200    BREAK
		CASE 5    vSpawnPoint 	= <<1361.9014, 3598.5037, 33.8959>>    fSpawnHeading 	= 110.9628    BREAK
		CASE 6    vSpawnPoint 	= <<1376.7465, 3606.4238, 33.8938>>    fSpawnHeading 	= 198.7200    BREAK
		CASE 7    vSpawnPoint 	= <<1355.9413, 3599.1777, 33.8795>>    fSpawnHeading 	= 112.0807    BREAK
		CASE 8    vSpawnPoint 	= <<1374.3809, 3600.4785, 33.8945>>    fSpawnHeading 	= 198.7200    BREAK
		CASE 9    vSpawnPoint 	= <<1370.5137, 3598.1682, 33.8947>>    fSpawnHeading 	= 198.7200    BREAK
		CASE 10    vSpawnPoint 	= <<1377.2761, 3603.6504, 33.8789>>    fSpawnHeading 	= 198.7200    BREAK
		CASE 11    vSpawnPoint 	= <<1366.3660, 3598.4150, 33.8944>>    fSpawnHeading 	= 198.7200    BREAK
		CASE 12    vSpawnPoint 	= <<1380.2339, 3605.1331, 33.8905>>    fSpawnHeading 	= 198.7200    BREAK
		CASE 13    vSpawnPoint 	= <<1360.1331, 3596.9868, 33.8998>>    fSpawnHeading 	= 109.8184    BREAK
		CASE 14    vSpawnPoint 	= <<1383.3828, 3604.7012, 33.8945>>    fSpawnHeading 	= 198.7200    BREAK
		CASE 15    vSpawnPoint 	= <<1357.2091, 3595.1218, 33.9094>>    fSpawnHeading 	= 118.5978    BREAK
		CASE 16    vSpawnPoint 	= <<1374.2178, 3596.9131, 33.8950>>    fSpawnHeading 	= 198.7200    BREAK
		CASE 17    vSpawnPoint 	= <<1369.6903, 3595.7380, 33.8947>>    fSpawnHeading 	= 198.7200    BREAK
		CASE 18    vSpawnPoint 	= <<1377.2043, 3600.1091, 33.8594>>    fSpawnHeading 	= 198.7200    BREAK
		CASE 19    vSpawnPoint 	= <<1367.2889, 3592.9617, 33.8952>>    fSpawnHeading 	= 198.7200    BREAK
		CASE 20    vSpawnPoint 	= <<1381.3345, 3599.7534, 33.8578>>    fSpawnHeading 	= 198.7200    BREAK
		CASE 21    vSpawnPoint 	= <<1362.3718, 3593.0044, 33.9158>>    fSpawnHeading 	= 124.5975    BREAK
		CASE 22    vSpawnPoint 	= <<1383.3702, 3602.4290, 33.8947>>    fSpawnHeading 	= 198.7200    BREAK
		CASE 23    vSpawnPoint 	= <<1358.7028, 3591.1597, 33.9427>>    fSpawnHeading 	= 115.0141    BREAK
		CASE 24    vSpawnPoint 	= <<1375.4119, 3592.7976, 33.8523>>    fSpawnHeading 	= 198.7200    BREAK
		CASE 25    vSpawnPoint 	= <<1371.3323, 3593.0479, 33.8627>>    fSpawnHeading 	= 198.7200    BREAK
		CASE 26    vSpawnPoint 	= <<1378.1688, 3595.9392, 33.8885>>    fSpawnHeading 	= 198.7200    BREAK
		CASE 27    vSpawnPoint 	= <<1369.0614, 3589.3933, 33.8974>>    fSpawnHeading 	= 193.0456    BREAK
		CASE 28    vSpawnPoint 	= <<1382.0778, 3595.5117, 33.8695>>    fSpawnHeading 	= 198.7200    BREAK
		CASE 29    vSpawnPoint 	= <<1362.4373, 3589.5908, 33.9391>>    fSpawnHeading 	= 145.3148    BREAK
		CASE 30    vSpawnPoint 	= <<1385.2677, 3597.4690, 33.8952>>    fSpawnHeading 	= 198.7200    BREAK
		CASE 31    vSpawnPoint 	= <<1359.9093, 3587.9158, 33.9729>>    fSpawnHeading 	= 122.5986    BREAK
		CASE 32    vSpawnPoint 	= <<1379.7642, 3589.2783, 33.9364>>    fSpawnHeading 	= 198.7200    BREAK
		CASE 33    vSpawnPoint 	= <<1375.4707, 3588.0364, 33.9427>>    fSpawnHeading 	= 198.7200    BREAK
		CASE 34    vSpawnPoint 	= <<1384.4520, 3590.7693, 33.9261>>    fSpawnHeading 	= 206.3399    BREAK
		CASE 35    vSpawnPoint 	= <<1370.8074, 3586.4915, 34.0227>>    fSpawnHeading 	= 198.7200    BREAK
		CASE 36    vSpawnPoint 	= <<1389.6332, 3593.6523, 33.8925>>    fSpawnHeading 	= 239.8458    BREAK
		CASE 37    vSpawnPoint 	= <<1365.6980, 3585.2039, 34.0403>>    fSpawnHeading 	= 198.7200    BREAK
		CASE 38    vSpawnPoint 	= <<1391.0935, 3596.7842, 34.0564>>    fSpawnHeading 	= 288.7843    BREAK
		CASE 39    vSpawnPoint 	= <<1361.0254, 3583.6135, 34.0359>>    fSpawnHeading 	= 152.5844    BREAK
		DEFAULT RETURN FALSE
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC
#ENDIF

#IF FEATURE_DLC_1_2022
FUNC BOOL GET_FMC_PASSED_OUT_VIP_POST_SCENE_SPAWN_POINT(FREEMODE_DELIVERY_DROPOFFS eDropoffID, INT iSpawnPoint, VECTOR &vSpawnPoint, FLOAT &fSpawnHeading)
	SWITCH eDropoffID
		CASE FMC_DROPOFF_LS_MEDICAL_CENTER
			SWITCH iSpawnPoint
				CASE 0    vSpawnPoint 	= <<286.3735, -1437.9675, 28.9666>>    fSpawnHeading 	= 49.3200    BREAK
				CASE 1    vSpawnPoint 	= <<285.7016, -1435.6552, 28.9686>>    fSpawnHeading 	= 49.3200    BREAK
				CASE 2    vSpawnPoint 	= <<284.3025, -1437.4977, 28.9666>>    fSpawnHeading 	= 49.3200    BREAK
				CASE 3    vSpawnPoint 	= <<283.8267, -1435.5190, 28.9660>>    fSpawnHeading 	= 49.3200    BREAK
				CASE 4    vSpawnPoint 	= <<281.6780, -1436.1010, 28.9666>>    fSpawnHeading 	= 49.3200    BREAK
				CASE 5    vSpawnPoint 	= <<280.7120, -1433.6099, 28.9656>>    fSpawnHeading 	= 49.3200    BREAK
				CASE 6    vSpawnPoint 	= <<282.0267, -1434.8879, 28.9662>>    fSpawnHeading 	= 49.3200    BREAK
				CASE 7    vSpawnPoint 	= <<281.5742, -1432.1265, 28.9642>>    fSpawnHeading 	= 49.3200    BREAK
				CASE 8    vSpawnPoint 	= <<282.9532, -1433.3025, 28.9645>>    fSpawnHeading 	= 49.3200    BREAK
				CASE 9    vSpawnPoint 	= <<279.1084, -1433.7432, 28.9663>>    fSpawnHeading 	= 49.3200    BREAK
				CASE 10    vSpawnPoint 	= <<276.7782, -1431.2588, 28.8148>>    fSpawnHeading 	= 49.3200    BREAK
				CASE 11    vSpawnPoint 	= <<280.0139, -1430.9874, 28.9625>>    fSpawnHeading 	= 49.3200    BREAK
				CASE 12    vSpawnPoint 	= <<277.9944, -1429.4901, 28.8017>>    fSpawnHeading 	= 49.3200    BREAK
				CASE 13    vSpawnPoint 	= <<278.4812, -1432.2078, 28.9375>>    fSpawnHeading 	= 49.3200    BREAK
				CASE 14    vSpawnPoint 	= <<274.9487, -1429.9485, 28.6553>>    fSpawnHeading 	= 49.3200    BREAK
				CASE 15    vSpawnPoint 	= <<275.8202, -1427.8757, 28.6284>>    fSpawnHeading 	= 49.3200    BREAK
				CASE 16    vSpawnPoint 	= <<287.4216, -1413.4053, 28.8810>>    fSpawnHeading 	= 49.3200    BREAK
				CASE 17    vSpawnPoint 	= <<272.9955, -1428.4067, 28.4739>>    fSpawnHeading 	= 49.3200    BREAK
				CASE 18    vSpawnPoint 	= <<284.7186, -1414.6750, 28.7640>>    fSpawnHeading 	= 49.3200    BREAK
				CASE 19    vSpawnPoint 	= <<274.2914, -1426.4601, 28.5007>>    fSpawnHeading 	= 49.3200    BREAK
				CASE 20    vSpawnPoint 	= <<286.6792, -1407.0681, 28.9815>>    fSpawnHeading 	= 49.3200    BREAK
				CASE 21    vSpawnPoint 	= <<286.6040, -1404.8550, 29.0862>>    fSpawnHeading 	= 49.3200    BREAK
				CASE 22    vSpawnPoint 	= <<287.0877, -1409.3759, 28.8924>>    fSpawnHeading 	= 49.3200    BREAK
				CASE 23    vSpawnPoint 	= <<289.4111, -1404.5083, 29.1630>>    fSpawnHeading 	= 49.3200    BREAK
				CASE 24    vSpawnPoint 	= <<284.4871, -1411.3625, 28.7732>>    fSpawnHeading 	= 49.3200    BREAK
				CASE 25    vSpawnPoint 	= <<271.4800, -1426.6425, 28.3410>>    fSpawnHeading 	= 49.3200    BREAK
				CASE 26    vSpawnPoint 	= <<282.3035, -1410.1163, 28.7529>>    fSpawnHeading 	= 49.3200    BREAK
				CASE 27    vSpawnPoint 	= <<267.2432, -1427.3411, 28.3231>>    fSpawnHeading 	= 49.3200    BREAK
				CASE 28    vSpawnPoint 	= <<283.6289, -1407.5281, 28.9004>>    fSpawnHeading 	= 49.3200    BREAK
				CASE 29    vSpawnPoint 	= <<267.6462, -1430.0575, 28.3214>>    fSpawnHeading 	= 49.3200    BREAK
				CASE 30    vSpawnPoint 	= <<262.4311, -1432.1667, 28.3433>>    fSpawnHeading 	= 49.3200    BREAK
				CASE 31    vSpawnPoint 	= <<264.8017, -1432.1056, 28.3433>>    fSpawnHeading 	= 49.3200    BREAK
				CASE 32    vSpawnPoint 	= <<262.4645, -1434.4142, 28.3018>>    fSpawnHeading 	= 49.3200    BREAK
				CASE 33    vSpawnPoint 	= <<264.7932, -1429.8831, 28.3423>>    fSpawnHeading 	= 49.3200    BREAK
				CASE 34    vSpawnPoint 	= <<260.4811, -1435.2578, 28.2676>>    fSpawnHeading 	= 49.3200    BREAK
				CASE 35    vSpawnPoint 	= <<263.6295, -1406.4933, 28.6398>>    fSpawnHeading 	= 232.4847    BREAK
				CASE 36    vSpawnPoint 	= <<264.2902, -1404.3943, 28.8100>>    fSpawnHeading 	= 232.5443    BREAK
				CASE 37    vSpawnPoint 	= <<262.1300, -1407.7896, 28.5487>>    fSpawnHeading 	= 227.8757    BREAK
				CASE 38    vSpawnPoint 	= <<266.2852, -1403.3625, 28.8096>>    fSpawnHeading 	= 232.3330    BREAK
				CASE 39    vSpawnPoint 	= <<260.8799, -1410.1255, 28.4914>>    fSpawnHeading 	= 228.2236    BREAK
				DEFAULT RETURN FALSE
			ENDSWITCH
		BREAK
		CASE FMC_DROPOFF_ECLIPSE_MEDICAL_TOWER
			SWITCH iSpawnPoint
				CASE 0    vSpawnPoint 	= <<-663.2943, 296.5263, 80.5848>>    fSpawnHeading 	= 155.8800    BREAK
				CASE 1    vSpawnPoint 	= <<-665.5712, 295.5526, 80.6195>>    fSpawnHeading 	= 155.8800    BREAK
				CASE 2    vSpawnPoint 	= <<-662.3047, 294.1768, 80.5254>>    fSpawnHeading 	= 147.6273    BREAK
				CASE 3    vSpawnPoint 	= <<-666.6068, 297.7033, 80.7014>>    fSpawnHeading 	= 155.8800    BREAK
				CASE 4    vSpawnPoint 	= <<-667.8422, 296.0309, 80.6972>>    fSpawnHeading 	= 155.8800    BREAK
				CASE 5    vSpawnPoint 	= <<-667.4559, 299.6032, 80.7522>>    fSpawnHeading 	= 172.3102    BREAK
				CASE 6    vSpawnPoint 	= <<-669.2222, 299.8488, 80.8097>>    fSpawnHeading 	= 176.4823    BREAK
				CASE 7    vSpawnPoint 	= <<-664.6328, 299.1336, 80.7380>>    fSpawnHeading 	= 176.5485    BREAK
				CASE 8    vSpawnPoint 	= <<-671.5597, 300.1847, 80.8384>>    fSpawnHeading 	= 172.3829    BREAK
				CASE 9    vSpawnPoint 	= <<-662.6137, 299.2596, 80.7155>>    fSpawnHeading 	= 180.4433    BREAK
				CASE 10    vSpawnPoint 	= <<-675.3583, 296.6006, 80.9942>>    fSpawnHeading 	= 172.3740    BREAK
				CASE 11    vSpawnPoint 	= <<-676.4544, 298.3513, 81.0559>>    fSpawnHeading 	= 176.4288    BREAK
				CASE 12    vSpawnPoint 	= <<-674.1078, 298.0598, 80.9469>>    fSpawnHeading 	= 176.5930    BREAK
				CASE 13    vSpawnPoint 	= <<-678.3787, 296.6599, 81.1324>>    fSpawnHeading 	= 176.5021    BREAK
				CASE 14    vSpawnPoint 	= <<-672.8151, 296.0570, 80.8788>>    fSpawnHeading 	= 168.2462    BREAK
				CASE 15    vSpawnPoint 	= <<-682.9033, 298.9626, 81.3946>>    fSpawnHeading 	= 172.2874    BREAK
				CASE 16    vSpawnPoint 	= <<-681.8257, 296.9314, 81.3232>>    fSpawnHeading 	= 184.5914    BREAK
				CASE 17    vSpawnPoint 	= <<-686.4478, 299.4647, 81.5787>>    fSpawnHeading 	= 180.5804    BREAK
				CASE 18    vSpawnPoint 	= <<-679.9510, 298.3681, 81.2320>>    fSpawnHeading 	= 176.3125    BREAK
				CASE 19    vSpawnPoint 	= <<-685.0098, 296.8471, 81.4923>>    fSpawnHeading 	= 180.6100    BREAK
				CASE 20    vSpawnPoint 	= <<-688.7194, 297.1294, 81.6772>>    fSpawnHeading 	= 184.7377    BREAK
				CASE 21    vSpawnPoint 	= <<-692.6191, 300.0577, 81.8509>>    fSpawnHeading 	= 180.3588    BREAK
				CASE 22    vSpawnPoint 	= <<-693.6849, 297.2432, 81.9119>>    fSpawnHeading 	= 176.4219    BREAK
				CASE 23    vSpawnPoint 	= <<-689.1094, 299.6349, 81.7096>>    fSpawnHeading 	= 184.7274    BREAK
				CASE 24    vSpawnPoint 	= <<-691.0069, 297.4652, 81.7907>>    fSpawnHeading 	= 176.4852    BREAK
				CASE 25    vSpawnPoint 	= <<-651.6002, 289.0671, 80.3609>>    fSpawnHeading 	= 155.8800    BREAK
				CASE 26    vSpawnPoint 	= <<-653.8889, 289.7580, 80.3640>>    fSpawnHeading 	= 155.8800    BREAK
				CASE 27    vSpawnPoint 	= <<-649.9471, 288.2961, 80.3569>>    fSpawnHeading 	= 155.8800    BREAK
				CASE 28    vSpawnPoint 	= <<-655.4840, 289.0682, 80.3651>>    fSpawnHeading 	= 155.8800    BREAK
				CASE 29    vSpawnPoint 	= <<-647.5913, 287.6148, 80.3511>>    fSpawnHeading 	= 155.8800    BREAK
				CASE 30    vSpawnPoint 	= <<-649.8922, 285.9492, 80.3505>>    fSpawnHeading 	= 155.8800    BREAK
				CASE 31    vSpawnPoint 	= <<-651.4832, 285.2641, 80.3482>>    fSpawnHeading 	= 155.8800    BREAK
				CASE 32    vSpawnPoint 	= <<-648.3654, 284.8181, 80.3467>>    fSpawnHeading 	= 155.8800    BREAK
				CASE 33    vSpawnPoint 	= <<-652.4952, 287.0024, 80.3559>>    fSpawnHeading 	= 155.8800    BREAK
				CASE 34    vSpawnPoint 	= <<-646.9313, 285.5026, 80.3525>>    fSpawnHeading 	= 155.8800    BREAK
				CASE 35    vSpawnPoint 	= <<-676.5344, 277.8224, 80.7062>>    fSpawnHeading 	= 326.7368    BREAK
				CASE 36    vSpawnPoint 	= <<-675.4332, 273.5662, 80.3625>>    fSpawnHeading 	= 326.3095    BREAK
				CASE 37    vSpawnPoint 	= <<-675.8624, 275.4890, 80.4726>>    fSpawnHeading 	= 334.7144    BREAK
				CASE 38    vSpawnPoint 	= <<-674.2730, 277.5827, 80.5656>>    fSpawnHeading 	= 327.3049    BREAK
				CASE 39    vSpawnPoint 	= <<-673.5389, 275.2970, 80.3963>>    fSpawnHeading 	= 326.2880    BREAK
				DEFAULT RETURN FALSE
			ENDSWITCH
		BREAK
		CASE FMC_DROPOFF_ZONAH_MEDICAL_CENTER
			SWITCH iSpawnPoint
				CASE 0    vSpawnPoint 	= <<-465.0737, -351.1617, 33.5016>>    fSpawnHeading 	= 171.7200    BREAK
				CASE 1    vSpawnPoint 	= <<-466.1570, -352.7312, 33.4790>>    fSpawnHeading 	= 171.7200    BREAK
				CASE 2    vSpawnPoint 	= <<-464.6781, -353.7815, 33.3895>>    fSpawnHeading 	= 171.7200    BREAK
				CASE 3    vSpawnPoint 	= <<-464.9526, -356.5770, 33.0928>>    fSpawnHeading 	= 171.7200    BREAK
				CASE 4    vSpawnPoint 	= <<-466.2035, -354.9295, 33.2481>>    fSpawnHeading 	= 171.7200    BREAK
				CASE 5    vSpawnPoint 	= <<-465.0555, -358.9078, 33.0303>>    fSpawnHeading 	= 171.7200    BREAK
				CASE 6    vSpawnPoint 	= <<-467.0046, -359.9734, 33.0303>>    fSpawnHeading 	= 171.7200    BREAK
				CASE 7    vSpawnPoint 	= <<-465.1328, -361.3194, 33.0306>>    fSpawnHeading 	= 171.7200    BREAK
				CASE 8    vSpawnPoint 	= <<-466.9306, -356.9790, 33.0303>>    fSpawnHeading 	= 171.7200    BREAK
				CASE 9    vSpawnPoint 	= <<-462.3326, -359.1949, 33.0024>>    fSpawnHeading 	= 171.7200    BREAK
				CASE 10    vSpawnPoint 	= <<-461.8100, -361.1225, 32.9885>>    fSpawnHeading 	= 171.7200    BREAK
				CASE 11    vSpawnPoint 	= <<-459.4226, -361.2205, 32.9320>>    fSpawnHeading 	= 171.7200    BREAK
				CASE 12    vSpawnPoint 	= <<-460.0856, -359.3363, 32.9533>>    fSpawnHeading 	= 171.7200    BREAK
				CASE 13    vSpawnPoint 	= <<-457.0731, -361.8073, 32.8684>>    fSpawnHeading 	= 171.7200    BREAK
				CASE 14    vSpawnPoint 	= <<-457.2988, -359.8282, 32.8856>>    fSpawnHeading 	= 171.7200    BREAK
				CASE 15    vSpawnPoint 	= <<-475.2379, -356.6218, 33.0814>>    fSpawnHeading 	= 171.7200    BREAK
				CASE 16    vSpawnPoint 	= <<-477.9145, -356.4024, 33.1541>>    fSpawnHeading 	= 171.7200    BREAK
				CASE 17    vSpawnPoint 	= <<-472.5703, -357.0853, 33.0163>>    fSpawnHeading 	= 171.7200    BREAK
				CASE 18    vSpawnPoint 	= <<-481.2621, -355.8771, 33.2517>>    fSpawnHeading 	= 171.7200    BREAK
				CASE 19    vSpawnPoint 	= <<-469.6055, -357.2527, 32.9424>>    fSpawnHeading 	= 171.7200    BREAK
				CASE 20    vSpawnPoint 	= <<-476.9234, -358.9542, 33.0577>>    fSpawnHeading 	= 171.7200    BREAK
				CASE 21    vSpawnPoint 	= <<-480.1507, -358.6633, 33.1493>>    fSpawnHeading 	= 171.7200    BREAK
				CASE 22    vSpawnPoint 	= <<-474.2419, -359.0529, 32.9842>>    fSpawnHeading 	= 171.7200    BREAK
				CASE 23    vSpawnPoint 	= <<-482.7782, -358.6072, 33.2735>>    fSpawnHeading 	= 171.7200    BREAK
				CASE 24    vSpawnPoint 	= <<-471.0877, -359.6485, 32.9389>>    fSpawnHeading 	= 171.7200    BREAK
				CASE 25    vSpawnPoint 	= <<-485.6264, -358.1671, 33.4945>>    fSpawnHeading 	= 171.7200    BREAK
				CASE 26    vSpawnPoint 	= <<-488.4773, -355.5750, 33.5377>>    fSpawnHeading 	= 171.7200    BREAK
				CASE 27    vSpawnPoint 	= <<-484.3761, -356.9303, 33.4956>>    fSpawnHeading 	= 171.7200    BREAK
				CASE 28    vSpawnPoint 	= <<-491.3415, -355.2912, 33.6291>>    fSpawnHeading 	= 171.7200    BREAK
				CASE 29    vSpawnPoint 	= <<-486.1566, -356.0440, 33.4952>>    fSpawnHeading 	= 171.7200    BREAK
				CASE 30    vSpawnPoint 	= <<-483.9967, -352.2376, 33.5000>>    fSpawnHeading 	= 171.7200    BREAK
				CASE 31    vSpawnPoint 	= <<-485.6615, -353.1250, 33.5084>>    fSpawnHeading 	= 171.7200    BREAK
				CASE 32    vSpawnPoint 	= <<-485.4901, -351.0468, 33.5009>>    fSpawnHeading 	= 171.7200    BREAK
				CASE 33    vSpawnPoint 	= <<-484.2515, -354.6956, 33.4976>>    fSpawnHeading 	= 171.7200    BREAK
				CASE 34    vSpawnPoint 	= <<-483.5518, -350.0593, 33.5020>>    fSpawnHeading 	= 171.7200    BREAK
				CASE 35    vSpawnPoint 	= <<-479.5969, -391.3542, 33.0746>>    fSpawnHeading 	= 358.6760    BREAK
				CASE 36    vSpawnPoint 	= <<-482.6427, -390.8444, 33.1674>>    fSpawnHeading 	= 354.5457    BREAK
				CASE 37    vSpawnPoint 	= <<-476.9572, -391.9380, 33.0283>>    fSpawnHeading 	= 350.7342    BREAK
				CASE 38    vSpawnPoint 	= <<-486.7601, -390.3233, 33.2854>>    fSpawnHeading 	= 354.4479    BREAK
				CASE 39    vSpawnPoint 	= <<-474.3470, -392.3773, 33.0188>>    fSpawnHeading 	= 350.4203    BREAK
				DEFAULT RETURN FALSE
			ENDSWITCH
		BREAK
		CASE FMC_DROPOFF_PILLBOX_MEDICAL_CENTER
			SWITCH iSpawnPoint
				CASE 0    vSpawnPoint 	= <<293.9754, -603.2953, 42.3088>>    fSpawnHeading 	= 160.8301    BREAK
				CASE 1    vSpawnPoint 	= <<291.4648, -603.4787, 42.3159>>    fSpawnHeading 	= 152.6853    BREAK
				CASE 2    vSpawnPoint 	= <<295.0297, -605.3739, 42.3207>>    fSpawnHeading 	= 156.7591    BREAK
				CASE 3    vSpawnPoint 	= <<290.5026, -605.9206, 42.3304>>    fSpawnHeading 	= 127.7154    BREAK
				CASE 4    vSpawnPoint 	= <<293.1640, -608.0484, 42.3419>>    fSpawnHeading 	= 140.3281    BREAK
				CASE 5    vSpawnPoint 	= <<281.5250, -610.3979, 42.2574>>    fSpawnHeading 	= 103.2102    BREAK
				CASE 6    vSpawnPoint 	= <<284.3842, -609.3220, 42.3032>>    fSpawnHeading 	= 123.7736    BREAK
				CASE 7    vSpawnPoint 	= <<278.4064, -610.9698, 42.1856>>    fSpawnHeading 	= 107.3440    BREAK
				CASE 8    vSpawnPoint 	= <<287.0749, -608.3032, 42.3297>>    fSpawnHeading 	= 119.6838    BREAK
				CASE 9    vSpawnPoint 	= <<275.1675, -611.3729, 42.0716>>    fSpawnHeading 	= 94.9960    BREAK
				CASE 10    vSpawnPoint 	= <<285.0446, -611.7308, 42.3433>>    fSpawnHeading 	= 66.1032    BREAK
				CASE 11    vSpawnPoint 	= <<288.9455, -607.2522, 42.3348>>    fSpawnHeading 	= 127.9296    BREAK
				CASE 12    vSpawnPoint 	= <<288.3977, -611.2729, 42.3855>>    fSpawnHeading 	= 78.4800    BREAK
				CASE 13    vSpawnPoint 	= <<282.2920, -612.3489, 42.3015>>    fSpawnHeading 	= 78.4578    BREAK
				CASE 14    vSpawnPoint 	= <<285.9055, -614.3506, 42.4093>>    fSpawnHeading 	= 66.2230    BREAK
				CASE 15    vSpawnPoint 	= <<291.4648, -611.7970, 42.3887>>    fSpawnHeading 	= 70.2456    BREAK
				CASE 16    vSpawnPoint 	= <<292.5860, -610.2173, 42.3719>>    fSpawnHeading 	= 61.9813    BREAK
				CASE 17    vSpawnPoint 	= <<289.5612, -613.6606, 42.4131>>    fSpawnHeading 	= 66.0997    BREAK
				CASE 18    vSpawnPoint 	= <<280.6413, -614.1336, 42.2982>>    fSpawnHeading 	= 70.2302    BREAK
				CASE 19    vSpawnPoint 	= <<288.2893, -615.7963, 42.4442>>    fSpawnHeading 	= 62.1299    BREAK
				CASE 20    vSpawnPoint 	= <<267.0577, -619.0096, 41.2995>>    fSpawnHeading 	= 66.0779    BREAK
				CASE 21    vSpawnPoint 	= <<268.2202, -617.1796, 41.4128>>    fSpawnHeading 	= 53.6624    BREAK
				CASE 22    vSpawnPoint 	= <<268.0562, -613.3243, 41.5967>>    fSpawnHeading 	= 62.1004    BREAK
				CASE 23    vSpawnPoint 	= <<269.1331, -615.1758, 41.5248>>    fSpawnHeading 	= 61.9146    BREAK
				CASE 24    vSpawnPoint 	= <<266.4995, -616.3620, 41.4067>>    fSpawnHeading 	= 70.2257    BREAK
				CASE 25    vSpawnPoint 	= <<276.4736, -595.9255, 42.1921>>    fSpawnHeading 	= 66.1085    BREAK
				CASE 26    vSpawnPoint 	= <<277.7086, -594.2880, 42.2281>>    fSpawnHeading 	= 66.0335    BREAK
				CASE 27    vSpawnPoint 	= <<275.7695, -598.5045, 42.1421>>    fSpawnHeading 	= 74.3431    BREAK
				CASE 28    vSpawnPoint 	= <<277.8267, -591.7498, 42.2758>>    fSpawnHeading 	= 61.8670    BREAK
				CASE 29    vSpawnPoint 	= <<270.0036, -613.0704, 41.6962>>    fSpawnHeading 	= 70.3052    BREAK
				CASE 30    vSpawnPoint 	= <<280.8892, -585.0767, 42.2972>>    fSpawnHeading 	= 66.1029    BREAK
				CASE 31    vSpawnPoint 	= <<281.8792, -582.9568, 42.2770>>    fSpawnHeading 	= 62.0097    BREAK
				CASE 32    vSpawnPoint 	= <<280.0732, -587.0531, 42.3088>>    fSpawnHeading 	= 66.0651    BREAK
				CASE 33    vSpawnPoint 	= <<283.2179, -580.1431, 42.2637>>    fSpawnHeading 	= 70.2265    BREAK
				CASE 34    vSpawnPoint 	= <<279.2820, -590.4817, 42.2852>>    fSpawnHeading 	= 78.4800    BREAK
				CASE 35    vSpawnPoint 	= <<244.6278, -582.1455, 42.2187>>    fSpawnHeading 	= 248.9666    BREAK
				CASE 36    vSpawnPoint 	= <<244.8692, -579.4848, 42.2671>>    fSpawnHeading 	= 245.0118    BREAK
				CASE 37    vSpawnPoint 	= <<243.0031, -584.0549, 42.1722>>    fSpawnHeading 	= 249.1838    BREAK
				CASE 38    vSpawnPoint 	= <<246.3890, -577.4266, 42.3138>>    fSpawnHeading 	= 253.0114    BREAK
				CASE 39    vSpawnPoint 	= <<242.1805, -587.1180, 42.1046>>    fSpawnHeading 	= 244.7926    BREAK
				DEFAULT RETURN FALSE
			ENDSWITCH
		BREAK
		CASE FMC_DROPOFF_SANDY_SH_MEDICAL_CENTER
			SWITCH iSpawnPoint
				CASE 0    vSpawnPoint 	= <<1843.5984, 3665.7212, 32.8242>>    fSpawnHeading 	= 210.6600    BREAK
				CASE 1    vSpawnPoint 	= <<1843.1992, 3664.1938, 32.9957>>    fSpawnHeading 	= 210.6600    BREAK
				CASE 2    vSpawnPoint 	= <<1845.1631, 3665.4709, 32.9558>>    fSpawnHeading 	= 210.6600    BREAK
				CASE 3    vSpawnPoint 	= <<1840.7999, 3663.5408, 32.9482>>    fSpawnHeading 	= 210.6600    BREAK
				CASE 4    vSpawnPoint 	= <<1846.5907, 3666.9375, 32.8716>>    fSpawnHeading 	= 210.6600    BREAK
				CASE 5    vSpawnPoint 	= <<1842.7721, 3662.5186, 33.1851>>    fSpawnHeading 	= 210.6600    BREAK
				CASE 6    vSpawnPoint 	= <<1840.5051, 3661.7937, 33.1523>>    fSpawnHeading 	= 210.6600    BREAK
				CASE 7    vSpawnPoint 	= <<1847.6887, 3665.1731, 33.1319>>    fSpawnHeading 	= 210.6600    BREAK
				CASE 8    vSpawnPoint 	= <<1837.6879, 3660.1638, 33.1709>>    fSpawnHeading 	= 210.6600    BREAK
				CASE 9    vSpawnPoint 	= <<1851.6057, 3668.6270, 32.9405>>    fSpawnHeading 	= 210.6600    BREAK
				CASE 10    vSpawnPoint 	= <<1835.3607, 3663.1636, 32.7988>>    fSpawnHeading 	= 210.6600    BREAK
				CASE 11    vSpawnPoint 	= <<1837.9459, 3662.9204, 32.8640>>    fSpawnHeading 	= 210.6600    BREAK
				CASE 12    vSpawnPoint 	= <<1836.2693, 3661.0203, 33.0266>>    fSpawnHeading 	= 210.6600    BREAK
				CASE 13    vSpawnPoint 	= <<1851.3574, 3672.8428, 32.7334>>    fSpawnHeading 	= 210.6600    BREAK
				CASE 14    vSpawnPoint 	= <<1853.3640, 3671.1184, 32.8073>>    fSpawnHeading 	= 210.6600    BREAK
				CASE 15    vSpawnPoint 	= <<1833.9532, 3659.7493, 33.0393>>    fSpawnHeading 	= 210.6600    BREAK
				CASE 16    vSpawnPoint 	= <<1828.7070, 3659.4590, 32.9231>>    fSpawnHeading 	= 210.6600    BREAK
				CASE 17    vSpawnPoint 	= <<1831.2439, 3659.4243, 32.9450>>    fSpawnHeading 	= 210.6600    BREAK
				CASE 18    vSpawnPoint 	= <<1832.3699, 3661.8157, 32.8882>>    fSpawnHeading 	= 210.6600    BREAK
				CASE 19    vSpawnPoint 	= <<1830.4290, 3657.7380, 33.0665>>    fSpawnHeading 	= 210.6600    BREAK
				CASE 20    vSpawnPoint 	= <<1856.8346, 3675.3638, 32.6468>>    fSpawnHeading 	= 210.6600    BREAK
				CASE 21    vSpawnPoint 	= <<1855.8821, 3673.0361, 32.6530>>    fSpawnHeading 	= 210.6600    BREAK
				CASE 22    vSpawnPoint 	= <<1858.4602, 3676.8445, 32.6711>>    fSpawnHeading 	= 210.6600    BREAK
				CASE 23    vSpawnPoint 	= <<1853.9297, 3673.3081, 32.6809>>    fSpawnHeading 	= 210.6600    BREAK
				CASE 24    vSpawnPoint 	= <<1859.1561, 3674.8386, 32.6104>>    fSpawnHeading 	= 210.6600    BREAK
				CASE 25    vSpawnPoint 	= <<1826.6630, 3657.9629, 33.0412>>    fSpawnHeading 	= 120.2769    BREAK
				CASE 26    vSpawnPoint 	= <<1824.5505, 3657.9919, 33.0649>>    fSpawnHeading 	= 115.7242    BREAK
				CASE 27    vSpawnPoint 	= <<1825.8225, 3655.8396, 33.1220>>    fSpawnHeading 	= 115.8879    BREAK
				CASE 28    vSpawnPoint 	= <<1823.7059, 3656.3040, 33.0950>>    fSpawnHeading 	= 124.0682    BREAK
				CASE 29    vSpawnPoint 	= <<1827.8838, 3655.9856, 33.1304>>    fSpawnHeading 	= 120.1334    BREAK
				CASE 30    vSpawnPoint 	= <<1863.8660, 3677.8447, 32.6335>>    fSpawnHeading 	= 210.6600    BREAK
				CASE 31    vSpawnPoint 	= <<1861.3621, 3676.5459, 32.6255>>    fSpawnHeading 	= 210.6600    BREAK
				CASE 32    vSpawnPoint 	= <<1866.2109, 3677.9456, 32.6059>>    fSpawnHeading 	= 210.6600    BREAK
				CASE 33    vSpawnPoint 	= <<1859.8347, 3658.1714, 32.9302>>    fSpawnHeading 	= 41.6511    BREAK
				CASE 34    vSpawnPoint 	= <<1843.0792, 3648.5869, 33.1553>>    fSpawnHeading 	= 25.1863    BREAK
				CASE 35    vSpawnPoint 	= <<1850.9730, 3652.7598, 33.0765>>    fSpawnHeading 	= 33.3391    BREAK
				CASE 36    vSpawnPoint 	= <<1848.5164, 3651.4668, 33.0979>>    fSpawnHeading 	= 25.6058    BREAK
				CASE 37    vSpawnPoint 	= <<1855.2065, 3655.3867, 33.0169>>    fSpawnHeading 	= 33.8363    BREAK
				CASE 38    vSpawnPoint 	= <<1845.6661, 3649.7842, 33.1238>>    fSpawnHeading 	= 33.7014    BREAK
				CASE 39    vSpawnPoint 	= <<1856.9698, 3656.7241, 32.9985>>    fSpawnHeading 	= 29.6801    BREAK
				DEFAULT RETURN FALSE
			ENDSWITCH
		BREAK
		CASE FMC_DROPOFF_ST_FIACRE_HOSPITAL
			SWITCH iSpawnPoint
				CASE 0    vSpawnPoint 	= <<1153.3873, -1518.9758, 33.8434>>    fSpawnHeading 	= 360.0000    BREAK
				CASE 1    vSpawnPoint 	= <<1156.5734, -1519.0901, 33.8434>>    fSpawnHeading 	= 360.0000    BREAK
				CASE 2    vSpawnPoint 	= <<1150.9865, -1520.1340, 33.8434>>    fSpawnHeading 	= 360.0000    BREAK
				CASE 3    vSpawnPoint 	= <<1160.3373, -1520.0675, 33.8434>>    fSpawnHeading 	= 360.0000    BREAK
				CASE 4    vSpawnPoint 	= <<1155.0294, -1520.4072, 33.8434>>    fSpawnHeading 	= 360.0000    BREAK
				CASE 5    vSpawnPoint 	= <<1149.8523, -1507.5195, 33.6926>>    fSpawnHeading 	= 360.0000    BREAK
				CASE 6    vSpawnPoint 	= <<1152.8167, -1507.1365, 33.6926>>    fSpawnHeading 	= 360.0000    BREAK
				CASE 7    vSpawnPoint 	= <<1147.5703, -1506.5516, 33.6926>>    fSpawnHeading 	= 360.0000    BREAK
				CASE 8    vSpawnPoint 	= <<1155.3431, -1507.2389, 33.6926>>    fSpawnHeading 	= 360.0000    BREAK
				CASE 9    vSpawnPoint 	= <<1144.0665, -1506.0282, 33.6926>>    fSpawnHeading 	= 360.0000    BREAK
				CASE 10    vSpawnPoint 	= <<1150.9044, -1504.1021, 33.6926>>    fSpawnHeading 	= 360.0000    BREAK
				CASE 11    vSpawnPoint 	= <<1152.8320, -1503.8447, 33.6926>>    fSpawnHeading 	= 360.0000    BREAK
				CASE 12    vSpawnPoint 	= <<1148.7285, -1504.0544, 33.6926>>    fSpawnHeading 	= 360.0000    BREAK
				CASE 13    vSpawnPoint 	= <<1155.3021, -1503.4799, 33.6926>>    fSpawnHeading 	= 360.0000    BREAK
				CASE 14    vSpawnPoint 	= <<1144.5679, -1502.5056, 33.6926>>    fSpawnHeading 	= 360.0000    BREAK
				CASE 15    vSpawnPoint 	= <<1150.4951, -1500.4816, 33.6926>>    fSpawnHeading 	= 360.0000    BREAK
				CASE 16    vSpawnPoint 	= <<1151.7258, -1501.7357, 33.6926>>    fSpawnHeading 	= 360.0000    BREAK
				CASE 17    vSpawnPoint 	= <<1149.3035, -1501.8596, 33.6926>>    fSpawnHeading 	= 360.0000    BREAK
				CASE 18    vSpawnPoint 	= <<1153.4951, -1500.4816, 33.6926>>    fSpawnHeading 	= 360.0000    BREAK
				CASE 19    vSpawnPoint 	= <<1147.4951, -1500.4816, 33.6926>>    fSpawnHeading 	= 360.0000    BREAK
				CASE 20    vSpawnPoint 	= <<1150.4951, -1498.9816, 33.6926>>    fSpawnHeading 	= 360.0000    BREAK
				CASE 21    vSpawnPoint 	= <<1151.9951, -1498.9816, 33.6926>>    fSpawnHeading 	= 360.0000    BREAK
				CASE 22    vSpawnPoint 	= <<1148.9951, -1498.9816, 33.6926>>    fSpawnHeading 	= 360.0000    BREAK
				CASE 23    vSpawnPoint 	= <<1152.2373, -1497.2566, 33.6926>>    fSpawnHeading 	= 360.0000    BREAK
				CASE 24    vSpawnPoint 	= <<1145.6305, -1499.8416, 33.6926>>    fSpawnHeading 	= 360.0000    BREAK
				CASE 25    vSpawnPoint 	= <<1151.2264, -1496.1263, 33.6926>>    fSpawnHeading 	= 360.0000    BREAK
				CASE 26    vSpawnPoint 	= <<1142.1674, -1500.1010, 33.6926>>    fSpawnHeading 	= 360.0000    BREAK
				CASE 27    vSpawnPoint 	= <<1148.5361, -1496.7717, 33.6926>>    fSpawnHeading 	= 360.0000    BREAK
				CASE 28    vSpawnPoint 	= <<1154.0773, -1497.4143, 33.6926>>    fSpawnHeading 	= 360.0000    BREAK
				CASE 29    vSpawnPoint 	= <<1144.6495, -1497.3079, 33.6926>>    fSpawnHeading 	= 360.0000    BREAK
				CASE 30    vSpawnPoint 	= <<1150.0634, -1493.7867, 33.6926>>    fSpawnHeading 	= 360.0000    BREAK
				CASE 31    vSpawnPoint 	= <<1152.2483, -1493.7616, 33.6926>>    fSpawnHeading 	= 360.0000    BREAK
				CASE 32    vSpawnPoint 	= <<1147.1747, -1494.2677, 33.6608>>    fSpawnHeading 	= 360.0000    BREAK
				CASE 33    vSpawnPoint 	= <<1154.2222, -1494.6938, 33.6926>>    fSpawnHeading 	= 360.0000    BREAK
				CASE 34    vSpawnPoint 	= <<1144.0110, -1494.1826, 33.6926>>    fSpawnHeading 	= 360.0000    BREAK
				CASE 35    vSpawnPoint 	= <<1149.4454, -1472.9208, 33.6926>>    fSpawnHeading 	= 360.0000    BREAK
				CASE 36    vSpawnPoint 	= <<1152.7017, -1474.2998, 33.6926>>    fSpawnHeading 	= 360.0000    BREAK
				CASE 37    vSpawnPoint 	= <<1146.2203, -1474.1921, 33.6926>>    fSpawnHeading 	= 360.0000    BREAK
				CASE 38    vSpawnPoint 	= <<1143.9307, -1477.5515, 33.6926>>    fSpawnHeading 	= 360.0000    BREAK
				CASE 39    vSpawnPoint 	= <<1143.2510, -1472.5671, 33.6926>>    fSpawnHeading 	= 360.0000    BREAK
				DEFAULT RETURN FALSE
			ENDSWITCH
		BREAK
		CASE FMC_DROPOFF_PARSONS_REHAB_CENTER
			SWITCH iSpawnPoint
				CASE 0    vSpawnPoint 	= <<-1476.1829, 884.5821, 181.9113>>    fSpawnHeading 	= 261.0000    BREAK
				CASE 1    vSpawnPoint 	= <<-1475.9456, 882.9977, 181.9475>>    fSpawnHeading 	= 261.0000    BREAK
				CASE 2    vSpawnPoint 	= <<-1475.3057, 885.9031, 181.8834>>    fSpawnHeading 	= 261.0000    BREAK
				CASE 3    vSpawnPoint 	= <<-1476.6296, 881.8365, 181.9524>>    fSpawnHeading 	= 261.0000    BREAK
				CASE 4    vSpawnPoint 	= <<-1475.9222, 887.2035, 181.8362>>    fSpawnHeading 	= 261.0000    BREAK
				CASE 5    vSpawnPoint 	= <<-1473.6261, 884.1042, 181.9714>>    fSpawnHeading 	= 281.3690    BREAK
				CASE 6    vSpawnPoint 	= <<-1474.4229, 882.8116, 181.9923>>    fSpawnHeading 	= 261.0000    BREAK
				CASE 7    vSpawnPoint 	= <<-1473.9808, 886.1394, 181.8818>>    fSpawnHeading 	= 293.8318    BREAK
				CASE 8    vSpawnPoint 	= <<-1474.3018, 881.3308, 182.0390>>    fSpawnHeading 	= 269.1091    BREAK
				CASE 9    vSpawnPoint 	= <<-1474.3821, 887.7734, 181.8046>>    fSpawnHeading 	= 285.6817    BREAK
				CASE 10    vSpawnPoint 	= <<-1471.7113, 883.6147, 182.0444>>    fSpawnHeading 	= 285.6302    BREAK
				CASE 11    vSpawnPoint 	= <<-1471.3069, 881.2050, 182.1540>>    fSpawnHeading 	= 273.3870    BREAK
				CASE 12    vSpawnPoint 	= <<-1472.3947, 885.5709, 181.9331>>    fSpawnHeading 	= 281.4587    BREAK
				CASE 13    vSpawnPoint 	= <<-1472.6195, 881.8232, 182.0877>>    fSpawnHeading 	= 281.6400    BREAK
				CASE 14    vSpawnPoint 	= <<-1473.1418, 887.5906, 181.8090>>    fSpawnHeading 	= 289.7600    BREAK
				CASE 15    vSpawnPoint 	= <<-1470.5951, 884.9200, 182.0013>>    fSpawnHeading 	= 285.7412    BREAK
				CASE 16    vSpawnPoint 	= <<-1470.2604, 882.8334, 182.1148>>    fSpawnHeading 	= 281.2556    BREAK
				CASE 17    vSpawnPoint 	= <<-1471.2537, 887.1733, 181.8500>>    fSpawnHeading 	= 285.6702    BREAK
				CASE 18    vSpawnPoint 	= <<-1469.5569, 879.9545, 182.2339>>    fSpawnHeading 	= 273.3761    BREAK
				CASE 19    vSpawnPoint 	= <<-1471.9270, 889.3697, 181.6514>>    fSpawnHeading 	= 294.0400    BREAK
				CASE 20    vSpawnPoint 	= <<-1476.8336, 897.1030, 180.8276>>    fSpawnHeading 	= 297.8252    BREAK
				CASE 21    vSpawnPoint 	= <<-1475.5933, 894.7125, 181.2204>>    fSpawnHeading 	= 289.2554    BREAK
				CASE 22    vSpawnPoint 	= <<-1478.2159, 899.1392, 180.5579>>    fSpawnHeading 	= 298.1003    BREAK
				CASE 23    vSpawnPoint 	= <<-1475.2278, 892.1971, 181.7751>>    fSpawnHeading 	= 289.0910    BREAK
				CASE 24    vSpawnPoint 	= <<-1479.4822, 901.6791, 180.0299>>    fSpawnHeading 	= 297.2701    BREAK
				CASE 25    vSpawnPoint 	= <<-1467.3531, 865.8702, 182.5485>>    fSpawnHeading 	= 277.4530    BREAK
				CASE 26    vSpawnPoint 	= <<-1466.6000, 862.3205, 182.6048>>    fSpawnHeading 	= 277.4706    BREAK
				CASE 27    vSpawnPoint 	= <<-1468.2480, 868.9019, 182.5194>>    fSpawnHeading 	= 281.7503    BREAK
				CASE 28    vSpawnPoint 	= <<-1467.9847, 863.8915, 182.6463>>    fSpawnHeading 	= 277.4024    BREAK
				CASE 29    vSpawnPoint 	= <<-1467.8239, 871.3609, 182.4756>>    fSpawnHeading 	= 285.6835    BREAK
				DEFAULT RETURN FALSE
			ENDSWITCH
		BREAK
		CASE FMC_DROPOFF_VINEWOOD_VIP_HOUSE
			SWITCH iSpawnPoint
				CASE 0    vSpawnPoint 	= <<-1971.3877, 458.0360, 100.8146>>    fSpawnHeading 	= 105.8329    BREAK
				CASE 1    vSpawnPoint 	= <<-1972.5297, 459.1351, 100.9025>>    fSpawnHeading 	= 105.9037    BREAK
				CASE 2    vSpawnPoint 	= <<-1971.3807, 456.4164, 100.6358>>    fSpawnHeading 	= 97.5600    BREAK
				CASE 3    vSpawnPoint 	= <<-1973.1437, 461.2972, 101.1384>>    fSpawnHeading 	= 97.5600    BREAK
				CASE 4    vSpawnPoint 	= <<-1970.7369, 454.8231, 100.4763>>    fSpawnHeading 	= 105.8172    BREAK
				CASE 5    vSpawnPoint 	= <<-1969.6725, 458.6161, 100.7900>>    fSpawnHeading 	= 97.5600    BREAK
				CASE 6    vSpawnPoint 	= <<-1969.5667, 459.8681, 100.9109>>    fSpawnHeading 	= 97.5600    BREAK
				CASE 7    vSpawnPoint 	= <<-1968.9355, 457.3513, 100.6508>>    fSpawnHeading 	= 97.5600    BREAK
				CASE 8    vSpawnPoint 	= <<-1970.0145, 461.1935, 101.0597>>    fSpawnHeading 	= 97.5600    BREAK
				CASE 9    vSpawnPoint 	= <<-1969.3304, 456.0387, 100.5221>>    fSpawnHeading 	= 97.5600    BREAK
				CASE 10    vSpawnPoint 	= <<-1967.0657, 459.5885, 100.8366>>    fSpawnHeading 	= 97.5600    BREAK
				CASE 11    vSpawnPoint 	= <<-1966.8535, 461.0537, 100.9611>>    fSpawnHeading 	= 97.5600    BREAK
				CASE 12    vSpawnPoint 	= <<-1967.0938, 458.0587, 100.7051>>    fSpawnHeading 	= 97.5600    BREAK
				CASE 13    vSpawnPoint 	= <<-1971.4011, 460.2481, 101.0557>>    fSpawnHeading 	= 97.5600    BREAK
				CASE 14    vSpawnPoint 	= <<-1967.3374, 456.4558, 100.5620>>    fSpawnHeading 	= 97.5600    BREAK
				CASE 15    vSpawnPoint 	= <<-1975.1228, 474.0853, 102.5512>>    fSpawnHeading 	= 97.5600    BREAK
				CASE 16    vSpawnPoint 	= <<-1974.0575, 469.7769, 102.0574>>    fSpawnHeading 	= 97.5600    BREAK
				CASE 17    vSpawnPoint 	= <<-1973.8778, 467.7186, 101.8499>>    fSpawnHeading 	= 97.5600    BREAK
				CASE 18    vSpawnPoint 	= <<-1974.6913, 472.2366, 102.3443>>    fSpawnHeading 	= 97.5600    BREAK
				CASE 19    vSpawnPoint 	= <<-1973.5842, 464.7101, 101.5398>>    fSpawnHeading 	= 97.5600    BREAK
				CASE 20    vSpawnPoint 	= <<-1970.2407, 447.7622, 99.8263>>    fSpawnHeading 	= 97.5600    BREAK
				CASE 21    vSpawnPoint 	= <<-1971.0837, 450.3822, 100.1060>>    fSpawnHeading 	= 97.5600    BREAK
				CASE 22    vSpawnPoint 	= <<-1969.5118, 445.1949, 99.5474>>    fSpawnHeading 	= 97.5600    BREAK
				CASE 23    vSpawnPoint 	= <<-1971.5417, 452.3288, 100.3089>>    fSpawnHeading 	= 97.5600    BREAK
				CASE 24    vSpawnPoint 	= <<-1968.5656, 442.4102, 99.2392>>    fSpawnHeading 	= 97.5600    BREAK
				CASE 25    vSpawnPoint 	= <<-1963.8423, 433.4307, 98.1449>>    fSpawnHeading 	= 97.5600    BREAK
				CASE 26    vSpawnPoint 	= <<-1964.9458, 436.4358, 98.4660>>    fSpawnHeading 	= 97.5600    BREAK
				CASE 27    vSpawnPoint 	= <<-1965.5795, 433.0000, 98.0556>>    fSpawnHeading 	= 97.5600    BREAK
				CASE 28    vSpawnPoint 	= <<-1967.4139, 439.0467, 98.8411>>    fSpawnHeading 	= 97.5600    BREAK
				CASE 29    vSpawnPoint 	= <<-1967.0205, 436.3662, 98.4191>>    fSpawnHeading 	= 109.7929    BREAK
				DEFAULT RETURN FALSE
			ENDSWITCH
		BREAK
		CASE FMC_DROPOFF_BANHAM_CANYON_VIP_HOUSE
			SWITCH iSpawnPoint
				CASE 0    vSpawnPoint 	= <<-3033.9875, 739.4830, 22.0967>>    fSpawnHeading 	= 91.8000    BREAK
				CASE 1    vSpawnPoint 	= <<-3033.2578, 740.9106, 22.3239>>    fSpawnHeading 	= 91.8000    BREAK
				CASE 2    vSpawnPoint 	= <<-3033.2205, 738.2315, 22.4546>>    fSpawnHeading 	= 91.8000    BREAK
				CASE 3    vSpawnPoint 	= <<-3034.0691, 742.0818, 22.0469>>    fSpawnHeading 	= 91.8000    BREAK
				CASE 4    vSpawnPoint 	= <<-3033.9060, 736.8843, 22.3195>>    fSpawnHeading 	= 91.8000    BREAK
				CASE 5    vSpawnPoint 	= <<-3036.0161, 739.6304, 21.5596>>    fSpawnHeading 	= 91.8000    BREAK
				CASE 6    vSpawnPoint 	= <<-3035.3276, 740.7416, 21.6962>>    fSpawnHeading 	= 91.8000    BREAK
				CASE 7    vSpawnPoint 	= <<-3034.8540, 737.8594, 21.9464>>    fSpawnHeading 	= 91.8000    BREAK
				CASE 8    vSpawnPoint 	= <<-3035.3684, 742.0410, 21.6437>>    fSpawnHeading 	= 91.8000    BREAK
				CASE 9    vSpawnPoint 	= <<-3036.0222, 736.7314, 21.7483>>    fSpawnHeading 	= 108.3095    BREAK
				CASE 10    vSpawnPoint 	= <<-3037.2720, 738.6791, 21.3857>>    fSpawnHeading 	= 120.6482    BREAK
				CASE 11    vSpawnPoint 	= <<-3037.6868, 740.6949, 21.3503>>    fSpawnHeading 	= 112.4433    BREAK
				CASE 12    vSpawnPoint 	= <<-3038.5234, 742.6531, 21.2743>>    fSpawnHeading 	= 120.5351    BREAK
				CASE 13    vSpawnPoint 	= <<-3036.6677, 742.0001, 21.4646>>    fSpawnHeading 	= 91.8000    BREAK
				CASE 14    vSpawnPoint 	= <<-3039.8523, 744.1208, 21.2107>>    fSpawnHeading 	= 132.9082    BREAK
				CASE 15    vSpawnPoint 	= <<-3038.8743, 738.9898, 21.2660>>    fSpawnHeading 	= 128.7978    BREAK
				CASE 16    vSpawnPoint 	= <<-3039.7815, 740.4703, 21.2171>>    fSpawnHeading 	= 128.8130    BREAK
				CASE 17    vSpawnPoint 	= <<-3037.8149, 736.8942, 21.3264>>    fSpawnHeading 	= 124.8304    BREAK
				CASE 18    vSpawnPoint 	= <<-3040.1792, 741.8270, 21.2047>>    fSpawnHeading 	= 137.1475    BREAK
				CASE 19    vSpawnPoint 	= <<-3036.3655, 734.9150, 21.3979>>    fSpawnHeading 	= 133.0709    BREAK
				CASE 20    vSpawnPoint 	= <<-3042.9976, 746.6920, 21.0176>>    fSpawnHeading 	= 128.8633    BREAK
				CASE 21    vSpawnPoint 	= <<-3043.6555, 745.3318, 20.9988>>    fSpawnHeading 	= 124.7849    BREAK
				CASE 22    vSpawnPoint 	= <<-3041.5569, 745.1882, 21.1079>>    fSpawnHeading 	= 124.6630    BREAK
				CASE 23    vSpawnPoint 	= <<-3045.2344, 746.4199, 20.9191>>    fSpawnHeading 	= 128.9849    BREAK
				CASE 24    vSpawnPoint 	= <<-3041.7234, 743.0615, 21.1213>>    fSpawnHeading 	= 120.6568    BREAK
				CASE 25    vSpawnPoint 	= <<-3068.2400, 779.6105, 20.3265>>    fSpawnHeading 	= 124.7822    BREAK
				CASE 26    vSpawnPoint 	= <<-3066.4001, 778.6747, 20.3268>>    fSpawnHeading 	= 120.4272    BREAK
				CASE 27    vSpawnPoint 	= <<-3068.0205, 781.9905, 20.3565>>    fSpawnHeading 	= 128.6988    BREAK
				CASE 28    vSpawnPoint 	= <<-3065.3970, 777.3942, 20.3263>>    fSpawnHeading 	= 120.8039    BREAK
				CASE 29    vSpawnPoint 	= <<-3070.4761, 781.3628, 20.3577>>    fSpawnHeading 	= 120.6533    BREAK
				DEFAULT RETURN FALSE
			ENDSWITCH
		BREAK
		CASE FMC_DROPOFF_MIRROR_PARK_VIP_HOUSE
			SWITCH iSpawnPoint
				CASE 0    vSpawnPoint 	= <<926.8788, -615.5621, 56.4020>>    fSpawnHeading 	= 316.6295    BREAK
				CASE 1    vSpawnPoint 	= <<928.6769, -616.4485, 56.4201>>    fSpawnHeading 	= 320.5664    BREAK
				CASE 2    vSpawnPoint 	= <<925.6805, -614.1047, 56.3829>>    fSpawnHeading 	= 324.6497    BREAK
				CASE 3    vSpawnPoint 	= <<929.6690, -618.3582, 56.4621>>    fSpawnHeading 	= 316.4400    BREAK
				CASE 4    vSpawnPoint 	= <<924.1888, -612.4882, 56.4197>>    fSpawnHeading 	= 324.7185    BREAK
				CASE 5    vSpawnPoint 	= <<924.3882, -617.8088, 56.4594>>    fSpawnHeading 	= 316.6185    BREAK
				CASE 6    vSpawnPoint 	= <<925.2465, -619.0570, 56.4770>>    fSpawnHeading 	= 324.7291    BREAK
				CASE 7    vSpawnPoint 	= <<923.5509, -616.5280, 56.4206>>    fSpawnHeading 	= 324.4661    BREAK
				CASE 8    vSpawnPoint 	= <<926.7546, -620.2199, 56.4787>>    fSpawnHeading 	= 320.5403    BREAK
				CASE 9    vSpawnPoint 	= <<921.9481, -616.4625, 56.4701>>    fSpawnHeading 	= 328.8508    BREAK
				CASE 10    vSpawnPoint 	= <<926.0529, -617.2644, 56.4343>>    fSpawnHeading 	= 316.4400    BREAK
				CASE 11    vSpawnPoint 	= <<928.0107, -618.4504, 56.4537>>    fSpawnHeading 	= 324.6775    BREAK
				CASE 12    vSpawnPoint 	= <<924.0385, -614.7364, 56.4240>>    fSpawnHeading 	= 324.6976    BREAK
				CASE 13    vSpawnPoint 	= <<930.2900, -620.7388, 56.4660>>    fSpawnHeading 	= 316.4738    BREAK
				CASE 14    vSpawnPoint 	= <<922.4411, -613.5939, 56.4454>>    fSpawnHeading 	= 328.7970    BREAK
				CASE 15    vSpawnPoint 	= <<932.5286, -622.3072, 56.4397>>    fSpawnHeading 	= 316.4400    BREAK
				CASE 16    vSpawnPoint 	= <<935.2368, -625.0612, 56.4439>>    fSpawnHeading 	= 316.4400    BREAK
				CASE 17    vSpawnPoint 	= <<935.1364, -622.6348, 56.4149>>    fSpawnHeading 	= 316.4400    BREAK
				CASE 18    vSpawnPoint 	= <<937.1377, -626.9271, 56.4566>>    fSpawnHeading 	= 316.4400    BREAK
				CASE 19    vSpawnPoint 	= <<920.8773, -612.5145, 56.4353>>    fSpawnHeading 	= 316.4400    BREAK
				CASE 20    vSpawnPoint 	= <<915.4108, -608.8056, 56.4219>>    fSpawnHeading 	= 316.4400    BREAK
				CASE 21    vSpawnPoint 	= <<917.3391, -610.1438, 56.4271>>    fSpawnHeading 	= 316.4400    BREAK
				CASE 22    vSpawnPoint 	= <<913.4052, -607.5221, 56.4165>>    fSpawnHeading 	= 316.4400    BREAK
				CASE 23    vSpawnPoint 	= <<919.1994, -611.1960, 56.4323>>    fSpawnHeading 	= 316.4400    BREAK
				CASE 24    vSpawnPoint 	= <<911.7528, -606.1351, 56.4121>>    fSpawnHeading 	= 316.4400    BREAK
				CASE 25    vSpawnPoint 	= <<938.9791, -601.7088, 56.4664>>    fSpawnHeading 	= 147.5044    BREAK
				CASE 26    vSpawnPoint 	= <<940.7039, -602.8283, 56.4669>>    fSpawnHeading 	= 147.5144    BREAK
				CASE 27    vSpawnPoint 	= <<937.4488, -600.4926, 56.4664>>    fSpawnHeading 	= 147.5135    BREAK
				CASE 28    vSpawnPoint 	= <<942.1786, -604.0784, 56.4660>>    fSpawnHeading 	= 143.6996    BREAK
				CASE 29    vSpawnPoint 	= <<935.6398, -599.0029, 56.4670>>    fSpawnHeading 	= 143.4739    BREAK
				DEFAULT RETURN FALSE
			ENDSWITCH
		BREAK
		CASE FMC_DROPOFF_LAKE_VINEWOOD_VIP_HOUSE
			SWITCH iSpawnPoint
				CASE 0    vSpawnPoint 	= <<-160.8568, 978.7885, 234.4669>>    fSpawnHeading 	= 307.4032    BREAK
				CASE 1    vSpawnPoint 	= <<-160.5741, 977.2686, 234.5379>>    fSpawnHeading 	= 307.2894    BREAK
				CASE 2    vSpawnPoint 	= <<-162.1961, 979.9774, 234.4394>>    fSpawnHeading 	= 315.5430    BREAK
				CASE 3    vSpawnPoint 	= <<-159.5970, 976.3505, 234.4759>>    fSpawnHeading 	= 299.1600    BREAK
				CASE 4    vSpawnPoint 	= <<-162.8317, 981.8328, 234.2591>>    fSpawnHeading 	= 315.5830    BREAK
				CASE 5    vSpawnPoint 	= <<-160.4211, 980.9630, 234.2745>>    fSpawnHeading 	= 299.1600    BREAK
				CASE 6    vSpawnPoint 	= <<-159.3679, 978.9667, 234.3113>>    fSpawnHeading 	= 299.1600    BREAK
				CASE 7    vSpawnPoint 	= <<-161.1212, 982.6496, 234.1728>>    fSpawnHeading 	= 299.1600    BREAK
				CASE 8    vSpawnPoint 	= <<-158.4618, 976.9839, 234.3325>>    fSpawnHeading 	= 299.1600    BREAK
				CASE 9    vSpawnPoint 	= <<-161.6981, 984.3149, 234.0303>>    fSpawnHeading 	= 327.9322    BREAK
				CASE 10    vSpawnPoint 	= <<-158.8003, 980.4389, 234.1666>>    fSpawnHeading 	= 290.9133    BREAK
				CASE 11    vSpawnPoint 	= <<-157.9600, 978.7526, 234.2268>>    fSpawnHeading 	= 299.1600    BREAK
				CASE 12    vSpawnPoint 	= <<-159.5664, 982.5093, 234.1041>>    fSpawnHeading 	= 290.9076    BREAK
				CASE 13    vSpawnPoint 	= <<-157.1176, 976.4905, 234.2786>>    fSpawnHeading 	= 299.1600    BREAK
				CASE 14    vSpawnPoint 	= <<-160.3252, 984.8020, 234.0375>>    fSpawnHeading 	= 286.7725    BREAK
				CASE 15    vSpawnPoint 	= <<-154.2853, 994.4424, 233.9484>>    fSpawnHeading 	= 95.4588    BREAK
				CASE 16    vSpawnPoint 	= <<-154.0873, 993.1260, 234.0083>>    fSpawnHeading 	= 103.5946    BREAK
				CASE 17    vSpawnPoint 	= <<-154.5128, 995.7450, 233.9059>>    fSpawnHeading 	= 112.0470    BREAK
				CASE 18    vSpawnPoint 	= <<-153.9735, 991.9250, 234.0619>>    fSpawnHeading 	= 95.6728    BREAK
				CASE 19    vSpawnPoint 	= <<-160.7715, 986.7759, 233.9845>>    fSpawnHeading 	= 299.1600    BREAK
				CASE 20    vSpawnPoint 	= <<-151.1836, 992.6777, 234.9954>>    fSpawnHeading 	= 103.7305    BREAK
				CASE 21    vSpawnPoint 	= <<-149.3246, 995.6367, 235.8559>>    fSpawnHeading 	= 91.4378    BREAK
				CASE 22    vSpawnPoint 	= <<-151.5797, 994.1172, 234.9954>>    fSpawnHeading 	= 95.2229    BREAK
				CASE 23    vSpawnPoint 	= <<-148.9752, 993.1416, 235.8477>>    fSpawnHeading 	= 99.4731    BREAK
				CASE 24    vSpawnPoint 	= <<-151.8848, 995.9695, 234.9954>>    fSpawnHeading 	= 95.6092    BREAK
				CASE 25    vSpawnPoint 	= <<-144.8845, 952.1608, 234.9156>>    fSpawnHeading 	= 307.3969    BREAK
				CASE 26    vSpawnPoint 	= <<-143.6193, 950.1420, 234.9322>>    fSpawnHeading 	= 311.6201    BREAK
				CASE 27    vSpawnPoint 	= <<-145.7918, 954.0358, 234.8881>>    fSpawnHeading 	= 307.3818    BREAK
				CASE 28    vSpawnPoint 	= <<-141.8501, 949.0985, 234.9265>>    fSpawnHeading 	= 311.5306    BREAK
				CASE 29    vSpawnPoint 	= <<-147.1397, 954.9672, 234.8880>>    fSpawnHeading 	= 307.4137    BREAK
				DEFAULT RETURN FALSE
			ENDSWITCH
		BREAK
		CASE FMC_DROPOFF_RICHMAN_VIP_HOUSE
			SWITCH iSpawnPoint
				CASE 0    vSpawnPoint 	= <<-1549.4742, 38.7567, 56.6467>>    fSpawnHeading 	= 349.9200    BREAK
				CASE 1    vSpawnPoint 	= <<-1547.5768, 38.7195, 56.5391>>    fSpawnHeading 	= 349.9200    BREAK
				CASE 2    vSpawnPoint 	= <<-1551.6165, 39.1405, 56.7620>>    fSpawnHeading 	= 349.9200    BREAK
				CASE 3    vSpawnPoint 	= <<-1545.7759, 38.4984, 56.4600>>    fSpawnHeading 	= 349.9200    BREAK
				CASE 4    vSpawnPoint 	= <<-1553.5554, 39.2373, 56.9084>>    fSpawnHeading 	= 349.9200    BREAK
				CASE 5    vSpawnPoint 	= <<-1549.7881, 36.4128, 56.9519>>    fSpawnHeading 	= 349.9200    BREAK
				CASE 6    vSpawnPoint 	= <<-1546.4994, 35.6804, 56.8563>>    fSpawnHeading 	= 349.9200    BREAK
				CASE 7    vSpawnPoint 	= <<-1551.0399, 36.3819, 57.0150>>    fSpawnHeading 	= 349.9200    BREAK
				CASE 8    vSpawnPoint 	= <<-1543.7821, 38.8374, 56.3311>>    fSpawnHeading 	= 2.2916    BREAK
				CASE 9    vSpawnPoint 	= <<-1552.7362, 36.2258, 57.0794>>    fSpawnHeading 	= 354.1165    BREAK
				CASE 10    vSpawnPoint 	= <<-1549.2257, 37.5602, 56.7766>>    fSpawnHeading 	= 358.0320    BREAK
				CASE 11    vSpawnPoint 	= <<-1548.5206, 36.1898, 56.9072>>    fSpawnHeading 	= 349.9200    BREAK
				CASE 12    vSpawnPoint 	= <<-1550.8765, 37.5049, 56.8931>>    fSpawnHeading 	= 2.2903    BREAK
				CASE 13    vSpawnPoint 	= <<-1545.9413, 36.7006, 56.6905>>    fSpawnHeading 	= 349.9200    BREAK
				CASE 14    vSpawnPoint 	= <<-1552.6091, 37.5929, 56.9815>>    fSpawnHeading 	= 354.0822    BREAK
				CASE 15    vSpawnPoint 	= <<-1549.0732, 34.4392, 57.0933>>    fSpawnHeading 	= 358.1035    BREAK
				CASE 16    vSpawnPoint 	= <<-1547.5233, 34.7379, 57.0073>>    fSpawnHeading 	= 349.9200    BREAK
				CASE 17    vSpawnPoint 	= <<-1550.3669, 34.7341, 57.1019>>    fSpawnHeading 	= 354.1771    BREAK
				CASE 18    vSpawnPoint 	= <<-1547.5057, 37.3810, 56.6972>>    fSpawnHeading 	= 349.9200    BREAK
				CASE 19    vSpawnPoint 	= <<-1551.7932, 34.9838, 57.1202>>    fSpawnHeading 	= 345.7301    BREAK
				CASE 20    vSpawnPoint 	= <<-1582.6144, 42.9188, 58.9300>>    fSpawnHeading 	= 349.9511    BREAK
				CASE 21    vSpawnPoint 	= <<-1587.4097, 42.5641, 59.0918>>    fSpawnHeading 	= 349.8995    BREAK
				CASE 22    vSpawnPoint 	= <<-1585.3981, 43.5161, 58.9919>>    fSpawnHeading 	= 349.9296    BREAK
				CASE 23    vSpawnPoint 	= <<-1584.3672, 41.7517, 59.0374>>    fSpawnHeading 	= 349.9200    BREAK
				CASE 24    vSpawnPoint 	= <<-1587.8568, 44.3654, 59.0697>>    fSpawnHeading 	= 341.6682    BREAK
				CASE 25    vSpawnPoint 	= <<-1564.5498, 41.6610, 57.7700>>    fSpawnHeading 	= 349.9200    BREAK
				CASE 26    vSpawnPoint 	= <<-1586.1588, 45.8683, 58.9397>>    fSpawnHeading 	= 345.8252    BREAK
				CASE 27    vSpawnPoint 	= <<-1565.9097, 42.6282, 57.8290>>    fSpawnHeading 	= 349.9200    BREAK
				CASE 28    vSpawnPoint 	= <<-1583.7190, 45.3924, 58.8331>>    fSpawnHeading 	= 349.9200    BREAK
				CASE 29    vSpawnPoint 	= <<-1588.1775, 46.7949, 58.9798>>    fSpawnHeading 	= 341.6573    BREAK
				DEFAULT RETURN FALSE
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL GET_FMC_AMMU_NATION_POST_SCENE_SPAWN_POINT(FREEMODE_DELIVERY_DROPOFFS eDropoffID, INT iSpawnPoint, VECTOR &vSpawnPoint, FLOAT &fSpawnHeading)
	SWITCH eDropoffID
		CASE FMC_DROPOFF_AMMU_NATION_PALETO_BAY
			SWITCH iSpawnPoint
				CASE 0    vSpawnPoint 	= <<-322.0029, 6072.9712, 30.2782>>    fSpawnHeading 	= 224.3450    BREAK
				CASE 1    vSpawnPoint 	= <<-322.5772, 6071.6396, 30.2891>>    fSpawnHeading 	= 224.3450    BREAK
				CASE 2    vSpawnPoint 	= <<-320.5433, 6073.9551, 30.2941>>    fSpawnHeading 	= 224.3450    BREAK
				CASE 3    vSpawnPoint 	= <<-324.5127, 6071.2783, 30.2510>>    fSpawnHeading 	= 224.3450    BREAK
				CASE 4    vSpawnPoint 	= <<-320.1155, 6075.6909, 30.2604>>    fSpawnHeading 	= 224.3450    BREAK
				CASE 5    vSpawnPoint 	= <<-320.6357, 6071.2168, 30.3361>>    fSpawnHeading 	= 224.3450    BREAK
				CASE 6    vSpawnPoint 	= <<-322.2735, 6069.8442, 30.3274>>    fSpawnHeading 	= 224.3450    BREAK
				CASE 7    vSpawnPoint 	= <<-319.3096, 6072.7207, 30.3448>>    fSpawnHeading 	= 224.3450    BREAK
				CASE 8    vSpawnPoint 	= <<-323.4005, 6068.2148, 30.3195>>    fSpawnHeading 	= 224.3450    BREAK
				CASE 9    vSpawnPoint 	= <<-318.0130, 6074.4854, 30.3474>>    fSpawnHeading 	= 224.3450    BREAK
				CASE 10    vSpawnPoint 	= <<-325.3520, 6068.7563, 30.2749>>    fSpawnHeading 	= 224.3450    BREAK
				CASE 11    vSpawnPoint 	= <<-325.8732, 6066.8818, 30.2768>>    fSpawnHeading 	= 224.3450    BREAK
				CASE 12    vSpawnPoint 	= <<-318.0419, 6076.5625, 30.3006>>    fSpawnHeading 	= 224.3450    BREAK
				CASE 13    vSpawnPoint 	= <<-327.4277, 6068.6929, 30.2361>>    fSpawnHeading 	= 224.3450    BREAK
				CASE 14    vSpawnPoint 	= <<-317.6207, 6078.3330, 30.3365>>    fSpawnHeading 	= 224.3450    BREAK
				CASE 15    vSpawnPoint 	= <<-314.7169, 6079.3315, 30.2281>>    fSpawnHeading 	= 224.3450    BREAK
				CASE 16    vSpawnPoint 	= <<-316.6760, 6080.1973, 30.2898>>    fSpawnHeading 	= 224.3450    BREAK
				CASE 17    vSpawnPoint 	= <<-314.1652, 6082.0005, 30.2331>>    fSpawnHeading 	= 224.3450    BREAK
				CASE 18    vSpawnPoint 	= <<-315.9208, 6076.2646, 30.3557>>    fSpawnHeading 	= 224.3450    BREAK
				CASE 19    vSpawnPoint 	= <<-312.1714, 6081.8960, 30.2207>>    fSpawnHeading 	= 224.3450    BREAK
				CASE 20    vSpawnPoint 	= <<-310.5283, 6085.8628, 30.2209>>    fSpawnHeading 	= 224.3450    BREAK
				CASE 21    vSpawnPoint 	= <<-309.9379, 6083.9165, 30.2123>>    fSpawnHeading 	= 224.3450    BREAK
				CASE 22    vSpawnPoint 	= <<-308.4214, 6085.9541, 30.2129>>    fSpawnHeading 	= 224.3450    BREAK
				CASE 23    vSpawnPoint 	= <<-312.2133, 6083.9888, 30.2263>>    fSpawnHeading 	= 224.3450    BREAK
				CASE 24    vSpawnPoint 	= <<-307.7972, 6088.5186, 30.2138>>    fSpawnHeading 	= 224.3450    BREAK
				CASE 25    vSpawnPoint 	= <<-301.0461, 6092.6538, 30.4028>>    fSpawnHeading 	= 224.3450    BREAK
				CASE 26    vSpawnPoint 	= <<-303.1133, 6092.5977, 30.3961>>    fSpawnHeading 	= 224.3450    BREAK
				CASE 27    vSpawnPoint 	= <<-300.7160, 6094.5166, 30.3922>>    fSpawnHeading 	= 224.3450    BREAK
				CASE 28    vSpawnPoint 	= <<-303.7354, 6089.9351, 30.4143>>    fSpawnHeading 	= 224.3450    BREAK
				CASE 29    vSpawnPoint 	= <<-298.8057, 6095.2788, 30.3947>>    fSpawnHeading 	= 224.3450    BREAK
				DEFAULT RETURN FALSE
			ENDSWITCH
		BREAK
		CASE FMC_DROPOFF_AMMU_NATION_SANDY_SHORES
			SWITCH iSpawnPoint
				CASE 0    vSpawnPoint 	= <<1700.5457, 3751.1345, 33.3671>>    fSpawnHeading 	= 227.5200    BREAK
				CASE 1    vSpawnPoint 	= <<1699.8938, 3749.8257, 33.3753>>    fSpawnHeading 	= 227.5200    BREAK
				CASE 2    vSpawnPoint 	= <<1701.6666, 3752.1213, 33.3585>>    fSpawnHeading 	= 227.5200    BREAK
				CASE 3    vSpawnPoint 	= <<1698.4076, 3749.2185, 33.3835>>    fSpawnHeading 	= 227.5200    BREAK
				CASE 4    vSpawnPoint 	= <<1702.2734, 3753.2747, 33.3512>>    fSpawnHeading 	= 227.5200    BREAK
				CASE 5    vSpawnPoint 	= <<1703.5275, 3750.8545, 33.1112>>    fSpawnHeading 	= 227.5200    BREAK
				CASE 6    vSpawnPoint 	= <<1701.4282, 3748.6323, 33.0841>>    fSpawnHeading 	= 227.5200    BREAK
				CASE 7    vSpawnPoint 	= <<1703.7546, 3754.3286, 33.3410>>    fSpawnHeading 	= 227.5200    BREAK
				CASE 8    vSpawnPoint 	= <<1699.7997, 3747.0989, 33.0971>>    fSpawnHeading 	= 227.5200    BREAK
				CASE 9    vSpawnPoint 	= <<1704.6110, 3755.7517, 33.3366>>    fSpawnHeading 	= 227.5200    BREAK
				CASE 10    vSpawnPoint 	= <<1704.0955, 3747.8691, 32.9469>>    fSpawnHeading 	= 227.5200    BREAK
				CASE 11    vSpawnPoint 	= <<1702.5209, 3746.2205, 32.9101>>    fSpawnHeading 	= 227.5200    BREAK
				CASE 12    vSpawnPoint 	= <<1705.7271, 3749.3455, 32.9978>>    fSpawnHeading 	= 227.5200    BREAK
				CASE 13    vSpawnPoint 	= <<1700.5385, 3744.9197, 32.9308>>    fSpawnHeading 	= 227.5200    BREAK
				CASE 14    vSpawnPoint 	= <<1707.8478, 3751.6836, 33.0600>>    fSpawnHeading 	= 227.5200    BREAK
				CASE 15    vSpawnPoint 	= <<1709.7104, 3747.7029, 32.7639>>    fSpawnHeading 	= 198.6705    BREAK
				CASE 16    vSpawnPoint 	= <<1707.6914, 3746.3474, 32.7514>>    fSpawnHeading 	= 202.7894    BREAK
				CASE 17    vSpawnPoint 	= <<1712.1510, 3748.8320, 32.7734>>    fSpawnHeading 	= 206.9045    BREAK
				CASE 18    vSpawnPoint 	= <<1705.3036, 3744.9272, 32.7502>>    fSpawnHeading 	= 211.0060    BREAK
				CASE 19    vSpawnPoint 	= <<1696.5636, 3741.5393, 32.8492>>    fSpawnHeading 	= 206.9693    BREAK
				CASE 20    vSpawnPoint 	= <<1697.4580, 3739.4878, 32.7604>>    fSpawnHeading 	= 215.0863    BREAK
				CASE 21    vSpawnPoint 	= <<1695.4792, 3739.4048, 32.8041>>    fSpawnHeading 	= 210.9627    BREAK
				CASE 22    vSpawnPoint 	= <<1699.3105, 3740.5027, 32.7470>>    fSpawnHeading 	= 211.1369    BREAK
				CASE 23    vSpawnPoint 	= <<1698.7856, 3742.3528, 32.8181>>    fSpawnHeading 	= 215.0465    BREAK
				CASE 24    vSpawnPoint 	= <<1701.1082, 3741.6228, 32.7392>>    fSpawnHeading 	= 219.2820    BREAK
				CASE 25    vSpawnPoint 	= <<1715.1659, 3751.0906, 32.8139>>    fSpawnHeading 	= 198.7812    BREAK
				CASE 26    vSpawnPoint 	= <<1718.3259, 3754.6204, 32.9136>>    fSpawnHeading 	= 198.6306    BREAK
				CASE 27    vSpawnPoint 	= <<1717.6498, 3752.1895, 32.8211>>    fSpawnHeading 	= 211.0287    BREAK
				CASE 28    vSpawnPoint 	= <<1715.6227, 3754.0486, 32.9397>>    fSpawnHeading 	= 198.6681    BREAK
				CASE 29    vSpawnPoint 	= <<1720.0333, 3753.2317, 32.8239>>    fSpawnHeading 	= 202.7993    BREAK
				DEFAULT RETURN FALSE
			ENDSWITCH
		BREAK
		CASE FMC_DROPOFF_AMMU_NATION_ROUTE_68
			SWITCH iSpawnPoint
				CASE 0    vSpawnPoint 	= <<-1102.9465, 2690.5532, 18.0951>>    fSpawnHeading 	= 220.6800    BREAK
				CASE 1    vSpawnPoint 	= <<-1104.6576, 2690.4990, 17.9021>>    fSpawnHeading 	= 220.6800    BREAK
				CASE 2    vSpawnPoint 	= <<-1102.1005, 2692.5552, 18.0298>>    fSpawnHeading 	= 220.6800    BREAK
				CASE 3    vSpawnPoint 	= <<-1104.9440, 2687.9434, 18.1299>>    fSpawnHeading 	= 220.6800    BREAK
				CASE 4    vSpawnPoint 	= <<-1100.0021, 2693.0637, 18.2363>>    fSpawnHeading 	= 220.6800    BREAK
				CASE 5    vSpawnPoint 	= <<-1109.7981, 2683.7744, 17.8964>>    fSpawnHeading 	= 220.6800    BREAK
				CASE 6    vSpawnPoint 	= <<-1111.4312, 2682.3706, 17.8206>>    fSpawnHeading 	= 220.6800    BREAK
				CASE 7    vSpawnPoint 	= <<-1108.3405, 2684.9236, 17.9752>>    fSpawnHeading 	= 220.6800    BREAK
				CASE 8    vSpawnPoint 	= <<-1113.5754, 2680.4368, 17.6774>>    fSpawnHeading 	= 220.6800    BREAK
				CASE 9    vSpawnPoint 	= <<-1106.6268, 2686.3860, 18.0825>>    fSpawnHeading 	= 220.6800    BREAK
				CASE 10    vSpawnPoint 	= <<-1103.9685, 2693.8267, 17.6469>>    fSpawnHeading 	= 220.6800    BREAK
				CASE 11    vSpawnPoint 	= <<-1105.7806, 2691.8152, 17.6767>>    fSpawnHeading 	= 220.6800    BREAK
				CASE 12    vSpawnPoint 	= <<-1102.4474, 2695.2444, 17.6688>>    fSpawnHeading 	= 220.6800    BREAK
				CASE 13    vSpawnPoint 	= <<-1101.3600, 2696.7952, 17.7285>>    fSpawnHeading 	= 220.6800    BREAK
				CASE 14    vSpawnPoint 	= <<-1093.7354, 2702.5134, 18.0738>>    fSpawnHeading 	= 220.6800    BREAK
				CASE 15    vSpawnPoint 	= <<-1097.6127, 2696.1672, 18.2932>>    fSpawnHeading 	= 220.6800    BREAK
				CASE 16    vSpawnPoint 	= <<-1099.5665, 2696.9385, 17.9221>>    fSpawnHeading 	= 220.6800    BREAK
				CASE 17    vSpawnPoint 	= <<-1095.6296, 2697.8762, 18.4057>>    fSpawnHeading 	= 220.6800    BREAK
				CASE 18    vSpawnPoint 	= <<-1096.1113, 2700.1738, 18.0860>>    fSpawnHeading 	= 220.6800    BREAK
				CASE 19    vSpawnPoint 	= <<-1093.4395, 2700.0000, 18.4506>>    fSpawnHeading 	= 220.6800    BREAK
				CASE 20    vSpawnPoint 	= <<-1093.8243, 2696.9592, 18.8545>>    fSpawnHeading 	= 220.6800    BREAK
				CASE 21    vSpawnPoint 	= <<-1095.7892, 2695.2568, 18.7423>>    fSpawnHeading 	= 220.6800    BREAK
				CASE 22    vSpawnPoint 	= <<-1091.9193, 2698.5154, 18.9757>>    fSpawnHeading 	= 220.6800    BREAK
				CASE 23    vSpawnPoint 	= <<-1091.3625, 2701.0435, 18.6431>>    fSpawnHeading 	= 220.6800    BREAK
				CASE 24    vSpawnPoint 	= <<-1089.5544, 2700.2947, 19.1743>>    fSpawnHeading 	= 220.6800    BREAK
				CASE 25    vSpawnPoint 	= <<-1120.1378, 2676.0989, 17.3340>>    fSpawnHeading 	= 220.6800    BREAK
				CASE 26    vSpawnPoint 	= <<-1122.3108, 2674.0596, 17.1780>>    fSpawnHeading 	= 220.6800    BREAK
				CASE 27    vSpawnPoint 	= <<-1117.8738, 2678.3354, 17.4586>>    fSpawnHeading 	= 220.6800    BREAK
				CASE 28    vSpawnPoint 	= <<-1124.5297, 2672.2927, 17.1059>>    fSpawnHeading 	= 220.6800    BREAK
				CASE 29    vSpawnPoint 	= <<-1115.9313, 2680.1924, 17.5589>>    fSpawnHeading 	= 220.6800    BREAK
				DEFAULT RETURN FALSE
			ENDSWITCH
		BREAK
		CASE FMC_DROPOFF_AMMU_NATION_CHUMASH
			SWITCH iSpawnPoint
				CASE 0    vSpawnPoint 	= <<-3162.3865, 1080.0240, 19.8480>>    fSpawnHeading 	= 247.0950    BREAK
				CASE 1    vSpawnPoint 	= <<-3163.1724, 1078.3125, 19.8480>>    fSpawnHeading 	= 247.0950    BREAK
				CASE 2    vSpawnPoint 	= <<-3161.6731, 1081.6497, 19.8480>>    fSpawnHeading 	= 247.0950    BREAK
				CASE 3    vSpawnPoint 	= <<-3162.2197, 1076.2795, 19.6882>>    fSpawnHeading 	= 247.0950    BREAK
				CASE 4    vSpawnPoint 	= <<-3159.8132, 1082.3281, 19.6977>>    fSpawnHeading 	= 247.0950    BREAK
				CASE 5    vSpawnPoint 	= <<-3160.0977, 1080.0867, 19.6943>>    fSpawnHeading 	= 247.0950    BREAK
				CASE 6    vSpawnPoint 	= <<-3160.1292, 1078.4240, 19.6913>>    fSpawnHeading 	= 247.0950    BREAK
				CASE 7    vSpawnPoint 	= <<-3158.4934, 1080.6821, 19.6948>>    fSpawnHeading 	= 247.0950    BREAK
				CASE 8    vSpawnPoint 	= <<-3161.0356, 1075.6486, 19.6861>>    fSpawnHeading 	= 247.0950    BREAK
				CASE 9    vSpawnPoint 	= <<-3157.5154, 1082.2813, 19.6969>>    fSpawnHeading 	= 247.0950    BREAK
				CASE 10    vSpawnPoint 	= <<-3157.6956, 1078.8291, 19.6905>>    fSpawnHeading 	= 247.0950    BREAK
				CASE 11    vSpawnPoint 	= <<-3158.7312, 1077.4034, 19.6882>>    fSpawnHeading 	= 247.0950    BREAK
				CASE 12    vSpawnPoint 	= <<-3156.4431, 1080.4810, 19.6931>>    fSpawnHeading 	= 247.0950    BREAK
				CASE 13    vSpawnPoint 	= <<-3159.6750, 1075.2522, 19.6843>>    fSpawnHeading 	= 247.0950    BREAK
				CASE 14    vSpawnPoint 	= <<-3155.4373, 1081.9526, 19.6956>>    fSpawnHeading 	= 247.0950    BREAK
				CASE 15    vSpawnPoint 	= <<-3155.8323, 1078.9495, 19.6896>>    fSpawnHeading 	= 247.0950    BREAK
				CASE 16    vSpawnPoint 	= <<-3157.1914, 1076.7194, 19.6858>>    fSpawnHeading 	= 247.0950    BREAK
				CASE 17    vSpawnPoint 	= <<-3154.0671, 1080.2350, 19.6912>>    fSpawnHeading 	= 247.0950    BREAK
				CASE 18    vSpawnPoint 	= <<-3157.4517, 1074.9761, 19.6823>>    fSpawnHeading 	= 247.0950    BREAK
				CASE 19    vSpawnPoint 	= <<-3153.4800, 1082.0508, 19.6946>>    fSpawnHeading 	= 247.0950    BREAK
				CASE 20    vSpawnPoint 	= <<-3156.4661, 1092.0243, 19.8563>>    fSpawnHeading 	= 247.0950    BREAK
				CASE 21    vSpawnPoint 	= <<-3157.3145, 1090.5308, 19.8560>>    fSpawnHeading 	= 247.0950    BREAK
				CASE 22    vSpawnPoint 	= <<-3155.4641, 1094.2990, 19.8563>>    fSpawnHeading 	= 247.0950    BREAK
				CASE 23    vSpawnPoint 	= <<-3157.9163, 1088.7961, 19.8563>>    fSpawnHeading 	= 247.0950    BREAK
				CASE 24    vSpawnPoint 	= <<-3154.2827, 1096.3651, 19.8568>>    fSpawnHeading 	= 247.0950    BREAK
				CASE 25    vSpawnPoint 	= <<-3166.7307, 1071.1515, 19.8480>>    fSpawnHeading 	= 247.0950    BREAK
				CASE 26    vSpawnPoint 	= <<-3167.2729, 1069.4581, 19.8480>>    fSpawnHeading 	= 247.0950    BREAK
				CASE 27    vSpawnPoint 	= <<-3166.0203, 1072.6639, 19.8480>>    fSpawnHeading 	= 247.0950    BREAK
				CASE 28    vSpawnPoint 	= <<-3167.7061, 1067.9435, 19.8480>>    fSpawnHeading 	= 247.0950    BREAK
				CASE 29    vSpawnPoint 	= <<-3165.4297, 1073.8694, 19.8480>>    fSpawnHeading 	= 247.0950    BREAK
				DEFAULT RETURN FALSE
			ENDSWITCH
		BREAK
		CASE FMC_DROPOFF_AMMU_NATION_TATAVIUM
			SWITCH iSpawnPoint
				CASE 0    vSpawnPoint 	= <<2568.8318, 307.0247, 107.6095>>    fSpawnHeading 	= 359.8100    BREAK
				CASE 1    vSpawnPoint 	= <<2570.8904, 307.2356, 107.6093>>    fSpawnHeading 	= 359.8100    BREAK
				CASE 2    vSpawnPoint 	= <<2566.4246, 307.2950, 107.6093>>    fSpawnHeading 	= 359.8100    BREAK
				CASE 3    vSpawnPoint 	= <<2572.6304, 308.5737, 107.6082>>    fSpawnHeading 	= 359.8100    BREAK
				CASE 4    vSpawnPoint 	= <<2564.4412, 308.6856, 107.6082>>    fSpawnHeading 	= 359.8100    BREAK
				CASE 5    vSpawnPoint 	= <<2570.4509, 308.9537, 107.6034>>    fSpawnHeading 	= 359.8100    BREAK
				CASE 6    vSpawnPoint 	= <<2567.4958, 308.9432, 107.6037>>    fSpawnHeading 	= 359.8100    BREAK
				CASE 7    vSpawnPoint 	= <<2562.9360, 307.9292, 107.6089>>    fSpawnHeading 	= 359.8100    BREAK
				CASE 8    vSpawnPoint 	= <<2573.7659, 307.8503, 107.6087>>    fSpawnHeading 	= 359.8100    BREAK
				CASE 9    vSpawnPoint 	= <<2560.9795, 308.3070, 107.6086>>    fSpawnHeading 	= 359.8100    BREAK
				CASE 10    vSpawnPoint 	= <<2563.5293, 306.1505, 107.6094>>    fSpawnHeading 	= 359.8100    BREAK
				CASE 11    vSpawnPoint 	= <<2575.0588, 306.4052, 107.6093>>    fSpawnHeading 	= 359.8100    BREAK
				CASE 12    vSpawnPoint 	= <<2561.3413, 306.4847, 107.6100>>    fSpawnHeading 	= 359.8100    BREAK
				CASE 13    vSpawnPoint 	= <<2575.1316, 308.7528, 107.6070>>    fSpawnHeading 	= 359.8100    BREAK
				CASE 14    vSpawnPoint 	= <<2576.6199, 307.8288, 107.6086>>    fSpawnHeading 	= 359.8100    BREAK
				CASE 15    vSpawnPoint 	= <<2578.9075, 311.3672, 107.6068>>    fSpawnHeading 	= 359.8100    BREAK
				CASE 16    vSpawnPoint 	= <<2577.8145, 310.6273, 107.6029>>    fSpawnHeading 	= 359.8100    BREAK
				CASE 17    vSpawnPoint 	= <<2560.6208, 316.6651, 107.4581>>    fSpawnHeading 	= 359.8100    BREAK
				CASE 18    vSpawnPoint 	= <<2578.4653, 308.8459, 107.6080>>    fSpawnHeading 	= 359.8100    BREAK
				CASE 19    vSpawnPoint 	= <<2577.4329, 316.5629, 107.4569>>    fSpawnHeading 	= 359.8100    BREAK
				CASE 20    vSpawnPoint 	= <<2570.0229, 316.5410, 107.4605>>    fSpawnHeading 	= 359.8100    BREAK
				CASE 21    vSpawnPoint 	= <<2572.6726, 316.7272, 107.4600>>    fSpawnHeading 	= 359.8100    BREAK
				CASE 22    vSpawnPoint 	= <<2566.9780, 316.8920, 107.4601>>    fSpawnHeading 	= 359.8100    BREAK
				CASE 23    vSpawnPoint 	= <<2574.9287, 316.5563, 107.4585>>    fSpawnHeading 	= 359.8100    BREAK
				CASE 24    vSpawnPoint 	= <<2563.7622, 316.8810, 107.4597>>    fSpawnHeading 	= 359.8100    BREAK
				CASE 25    vSpawnPoint 	= <<2570.0242, 305.7814, 107.6087>>    fSpawnHeading 	= 359.8100    BREAK
				CASE 26    vSpawnPoint 	= <<2576.8799, 305.3611, 107.6086>>    fSpawnHeading 	= 359.8100    BREAK
				CASE 27    vSpawnPoint 	= <<2567.9128, 305.6312, 107.6086>>    fSpawnHeading 	= 359.8100    BREAK
				CASE 28    vSpawnPoint 	= <<2578.6416, 306.5076, 107.6080>>    fSpawnHeading 	= 359.8100    BREAK
				CASE 29    vSpawnPoint 	= <<2558.8804, 305.7215, 107.6095>>    fSpawnHeading 	= 359.8100    BREAK
				DEFAULT RETURN FALSE
			ENDSWITCH
		BREAK
		CASE FMC_DROPOFF_AMMU_NATION_HAWICK
			SWITCH iSpawnPoint
				CASE 0    vSpawnPoint 	= <<232.4300, -48.6822, 68.3917>>    fSpawnHeading 	= 159.6050    BREAK
				CASE 1    vSpawnPoint 	= <<231.1519, -47.3551, 68.4192>>    fSpawnHeading 	= 159.6050    BREAK
				CASE 2    vSpawnPoint 	= <<234.5263, -48.6097, 68.4754>>    fSpawnHeading 	= 159.6050    BREAK
				CASE 3    vSpawnPoint 	= <<229.4648, -46.7279, 68.3824>>    fSpawnHeading 	= 159.6050    BREAK
				CASE 4    vSpawnPoint 	= <<236.2098, -49.3660, 68.4902>>    fSpawnHeading 	= 159.6050    BREAK
				CASE 5    vSpawnPoint 	= <<230.7034, -51.2641, 68.3190>>    fSpawnHeading 	= 159.6050    BREAK
				CASE 6    vSpawnPoint 	= <<228.6847, -50.7076, 68.2720>>    fSpawnHeading 	= 159.6050    BREAK
				CASE 7    vSpawnPoint 	= <<232.7003, -51.9310, 68.4171>>    fSpawnHeading 	= 159.6050    BREAK
				CASE 8    vSpawnPoint 	= <<226.0667, -49.2337, 68.2676>>    fSpawnHeading 	= 159.6050    BREAK
				CASE 9    vSpawnPoint 	= <<234.6485, -52.2535, 68.4568>>    fSpawnHeading 	= 159.6050    BREAK
				CASE 10    vSpawnPoint 	= <<231.4508, -53.3934, 68.3636>>    fSpawnHeading 	= 159.6050    BREAK
				CASE 11    vSpawnPoint 	= <<226.8295, -51.6338, 68.2139>>    fSpawnHeading 	= 159.6050    BREAK
				CASE 12    vSpawnPoint 	= <<233.6879, -53.8123, 68.4494>>    fSpawnHeading 	= 159.6050    BREAK
				CASE 13    vSpawnPoint 	= <<224.7081, -50.5435, 68.2498>>    fSpawnHeading 	= 159.6050    BREAK
				CASE 14    vSpawnPoint 	= <<235.6418, -53.6106, 68.4844>>    fSpawnHeading 	= 159.6050    BREAK
				CASE 15    vSpawnPoint 	= <<230.8475, -55.1402, 68.3298>>    fSpawnHeading 	= 159.6050    BREAK
				CASE 16    vSpawnPoint 	= <<228.7500, -53.7980, 68.2412>>    fSpawnHeading 	= 159.6050    BREAK
				CASE 17    vSpawnPoint 	= <<232.6504, -55.4010, 68.4405>>    fSpawnHeading 	= 159.6050    BREAK
				CASE 18    vSpawnPoint 	= <<226.1319, -53.5325, 68.1762>>    fSpawnHeading 	= 159.6050    BREAK
				CASE 19    vSpawnPoint 	= <<235.1079, -56.6180, 68.4950>>    fSpawnHeading 	= 159.6050    BREAK
				CASE 20    vSpawnPoint 	= <<221.5541, -51.5822, 68.1938>>    fSpawnHeading 	= 159.6050    BREAK
				CASE 21    vSpawnPoint 	= <<218.2915, -50.1217, 68.1155>>    fSpawnHeading 	= 159.6050    BREAK
				CASE 22    vSpawnPoint 	= <<245.6234, -59.6973, 68.7062>>    fSpawnHeading 	= 159.6050    BREAK
				CASE 23    vSpawnPoint 	= <<215.4164, -48.8410, 68.0488>>    fSpawnHeading 	= 159.6050    BREAK
				CASE 24    vSpawnPoint 	= <<237.5714, -53.6311, 68.5191>>    fSpawnHeading 	= 159.6050    BREAK
				CASE 25    vSpawnPoint 	= <<220.7713, -48.2251, 68.1507>>    fSpawnHeading 	= 159.6050    BREAK
				CASE 26    vSpawnPoint 	= <<217.8558, -46.5937, 68.0796>>    fSpawnHeading 	= 159.6050    BREAK
				CASE 27    vSpawnPoint 	= <<239.5148, -55.4376, 68.5678>>    fSpawnHeading 	= 159.6050    BREAK
				CASE 28    vSpawnPoint 	= <<215.3300, -45.1429, 68.0165>>    fSpawnHeading 	= 159.6050    BREAK
				CASE 29    vSpawnPoint 	= <<244.2508, -57.1811, 68.6652>>    fSpawnHeading 	= 159.6050    BREAK
				DEFAULT RETURN FALSE
			ENDSWITCH
		BREAK
		CASE FMC_DROPOFF_AMMU_NATION_MORNINGWOOD
			SWITCH iSpawnPoint
				CASE 0    vSpawnPoint 	= <<-1316.5652, -390.2798, 35.4480>>    fSpawnHeading 	= 77.4000    BREAK
				CASE 1    vSpawnPoint 	= <<-1317.8551, -388.5131, 35.4158>>    fSpawnHeading 	= 77.4000    BREAK
				CASE 2    vSpawnPoint 	= <<-1318.7216, -391.5426, 35.4246>>    fSpawnHeading 	= 77.4000    BREAK
				CASE 3    vSpawnPoint 	= <<-1317.5576, -386.9129, 35.4325>>    fSpawnHeading 	= 114.5241   BREAK
				CASE 4    vSpawnPoint 	= <<-1319.1892, -393.4562, 35.4206>>    fSpawnHeading 	= 40.2514    BREAK
				CASE 5    vSpawnPoint 	= <<-1320.5089, -388.1588, 35.4899>>    fSpawnHeading 	= 77.4000    BREAK
				CASE 6    vSpawnPoint 	= <<-1324.4219, -380.8290, 35.7904>>    fSpawnHeading 	= 31.9899    BREAK
				CASE 7    vSpawnPoint 	= <<-1322.4030, -389.2605, 35.5096>>    fSpawnHeading 	= 77.4000    BREAK
				CASE 8    vSpawnPoint 	= <<-1322.1188, -379.7348, 35.7856>>    fSpawnHeading 	= 19.6888    BREAK
				CASE 9    vSpawnPoint 	= <<-1321.1410, -391.0076, 35.4676>>    fSpawnHeading 	= 77.4000    BREAK
				CASE 10    vSpawnPoint 	= <<-1332.3710, -385.4161, 35.6902>>    fSpawnHeading 	= 23.8198    BREAK
				CASE 11    vSpawnPoint 	= <<-1330.4553, -385.4838, 35.6582>>    fSpawnHeading 	= 23.8489    BREAK
				CASE 12    vSpawnPoint 	= <<-1332.1066, -387.5398, 35.6677>>    fSpawnHeading 	= 19.6243    BREAK
				CASE 13    vSpawnPoint 	= <<-1329.4863, -383.7914, 35.6938>>    fSpawnHeading 	= 19.6585    BREAK
				CASE 14    vSpawnPoint 	= <<-1328.2159, -385.6252, 35.6420>>    fSpawnHeading 	= 23.8574    BREAK
				CASE 15    vSpawnPoint 	= <<-1321.1851, -396.3780, 35.6008>>    fSpawnHeading 	= 31.9994    BREAK
				CASE 16    vSpawnPoint 	= <<-1322.0603, -377.3590, 35.7216>>    fSpawnHeading 	= 19.6516    BREAK
				CASE 17    vSpawnPoint 	= <<-1323.4441, -397.8695, 35.6002>>    fSpawnHeading 	= 32.0563    BREAK
				CASE 18    vSpawnPoint 	= <<-1339.7990, -382.5143, 35.7283>>    fSpawnHeading 	= 28.0044    BREAK
				CASE 19    vSpawnPoint 	= <<-1325.8567, -399.3744, 35.5999>>    fSpawnHeading 	= 32.0359    BREAK
				CASE 20    vSpawnPoint 	= <<-1342.2450, -383.6217, 35.7324>>    fSpawnHeading 	= 27.9124    BREAK
				CASE 21    vSpawnPoint 	= <<-1340.9695, -385.5087, 35.7377>>    fSpawnHeading 	= 23.8343    BREAK
				CASE 22    vSpawnPoint 	= <<-1338.7041, -384.5499, 35.7340>>    fSpawnHeading 	= 27.9594    BREAK
				CASE 23    vSpawnPoint 	= <<-1336.6925, -383.7042, 35.7227>>    fSpawnHeading 	= 23.7053    BREAK
				CASE 24    vSpawnPoint 	= <<-1343.1522, -386.7083, 35.7399>>    fSpawnHeading 	= 27.9705    BREAK
				CASE 25    vSpawnPoint 	= <<-1328.7867, -379.9848, 35.7250>>    fSpawnHeading 	= 23.7928    BREAK
				CASE 26    vSpawnPoint 	= <<-1326.5370, -378.9269, 35.7164>>    fSpawnHeading 	= 19.7326    BREAK
				CASE 27    vSpawnPoint 	= <<-1326.1643, -376.6758, 35.7203>>    fSpawnHeading 	= 19.6690    BREAK
				CASE 28    vSpawnPoint 	= <<-1324.4904, -378.2164, 35.7234>>    fSpawnHeading 	= 15.4682    BREAK
				CASE 29    vSpawnPoint 	= <<-1328.7235, -378.1813, 35.7169>>    fSpawnHeading 	= 15.5816    BREAK
				DEFAULT RETURN FALSE
			ENDSWITCH
		BREAK
		CASE FMC_DROPOFF_AMMU_NATION_STRAWBERRY
			SWITCH iSpawnPoint
				CASE 0    vSpawnPoint 	= <<15.4271, -1119.7815, 27.8146>>    fSpawnHeading 	= 160.1394    BREAK
				CASE 1    vSpawnPoint 	= <<13.3756, -1120.2426, 27.7484>>    fSpawnHeading 	= 155.9431    BREAK
				CASE 2    vSpawnPoint 	= <<17.5728, -1120.7864, 27.8945>>    fSpawnHeading 	= 168.2881    BREAK
				CASE 3    vSpawnPoint 	= <<11.9103, -1121.6935, 27.6836>>    fSpawnHeading 	= 151.9152    BREAK
				CASE 4    vSpawnPoint 	= <<18.8848, -1121.6447, 27.9501>>    fSpawnHeading 	= 180.7200    BREAK
				CASE 5    vSpawnPoint 	= <<16.2572, -1122.0933, 27.8504>>    fSpawnHeading 	= 168.3930    BREAK
				CASE 6    vSpawnPoint 	= <<14.4769, -1121.7700, 27.7876>>    fSpawnHeading 	= 160.0869    BREAK
				CASE 7    vSpawnPoint 	= <<17.3249, -1123.1434, 27.8975>>    fSpawnHeading 	= 180.7200    BREAK
				CASE 8    vSpawnPoint 	= <<13.1470, -1123.1462, 27.7412>>    fSpawnHeading 	= 180.7200    BREAK
				CASE 9    vSpawnPoint 	= <<19.5467, -1123.2012, 27.9841>>    fSpawnHeading 	= 180.7200    BREAK
				CASE 10    vSpawnPoint 	= <<10.4830, -1122.8845, 27.6161>>    fSpawnHeading 	= 180.7200    BREAK
				CASE 11    vSpawnPoint 	= <<7.9200, -1121.3350, 27.4775>>    fSpawnHeading 	= 180.7200    BREAK
				CASE 12    vSpawnPoint 	= <<21.3399, -1122.2017, 28.0629>>    fSpawnHeading 	= 180.7200    BREAK
				CASE 13    vSpawnPoint 	= <<7.9983, -1123.7327, 27.4922>>    fSpawnHeading 	= 180.7200    BREAK
				CASE 14    vSpawnPoint 	= <<21.9719, -1123.2863, 28.0914>>    fSpawnHeading 	= 180.7200    BREAK
				CASE 15    vSpawnPoint 	= <<26.6812, -1119.7706, 28.2782>>    fSpawnHeading 	= 197.2065    BREAK
				CASE 16    vSpawnPoint 	= <<3.3403, -1123.8324, 27.2601>>    fSpawnHeading 	= 180.7200    BREAK
				CASE 17    vSpawnPoint 	= <<22.8186, -1120.5051, 28.1291>>    fSpawnHeading 	= 180.7200    BREAK
				CASE 18    vSpawnPoint 	= <<-1.1221, -1123.8776, 27.0299>>    fSpawnHeading 	= 180.7200    BREAK
				CASE 19    vSpawnPoint 	= <<24.8219, -1120.1854, 28.2258>>    fSpawnHeading 	= 180.7200    BREAK
				CASE 20    vSpawnPoint 	= <<2.7176, -1121.0519, 27.2278>>    fSpawnHeading 	= 180.7200    BREAK
				CASE 21    vSpawnPoint 	= <<0.7502, -1122.3278, 27.1213>>    fSpawnHeading 	= 180.7200    BREAK
				CASE 22    vSpawnPoint 	= <<4.6500, -1122.6279, 27.3199>>    fSpawnHeading 	= 193.0488    BREAK
				CASE 23    vSpawnPoint 	= <<-1.7624, -1120.9384, 26.9819>>    fSpawnHeading 	= 180.7200    BREAK
				CASE 24    vSpawnPoint 	= <<-3.1378, -1122.9878, 26.9191>>    fSpawnHeading 	= 180.7200    BREAK
				CASE 25    vSpawnPoint 	= <<32.6293, -1117.6625, 28.2453>>    fSpawnHeading 	= 238.3397    BREAK
				CASE 26    vSpawnPoint 	= <<30.8825, -1120.0906, 28.2421>>    fSpawnHeading 	= 234.3568    BREAK
				CASE 27    vSpawnPoint 	= <<28.6314, -1118.7411, 28.2795>>    fSpawnHeading 	= 221.9171    BREAK
				CASE 28    vSpawnPoint 	= <<28.0488, -1121.2803, 28.2490>>    fSpawnHeading 	= 201.2956    BREAK
				CASE 29    vSpawnPoint 	= <<30.0120, -1116.9698, 28.2856>>    fSpawnHeading 	= 226.0854    BREAK
				DEFAULT RETURN FALSE
			ENDSWITCH
		BREAK
		CASE FMC_DROPOFF_AMMU_NATION_LA_MESA
			SWITCH iSpawnPoint
				CASE 0    vSpawnPoint 	= <<843.9434, -1021.3018, 26.5354>>    fSpawnHeading 	= 0.0000    BREAK
				CASE 1    vSpawnPoint 	= <<845.0759, -1020.5635, 26.5324>>    fSpawnHeading 	= 0.0000    BREAK
				CASE 2    vSpawnPoint 	= <<841.9396, -1020.7837, 26.5333>>    fSpawnHeading 	= 0.0000    BREAK
				CASE 3    vSpawnPoint 	= <<846.8491, -1020.3857, 26.5316>>    fSpawnHeading 	= 0.0000    BREAK
				CASE 4    vSpawnPoint 	= <<839.9845, -1021.4153, 26.5318>>    fSpawnHeading 	= 0.0000    BREAK
				CASE 5    vSpawnPoint 	= <<843.8986, -1018.7431, 26.5403>>    fSpawnHeading 	= 0.0000    BREAK
				CASE 6    vSpawnPoint 	= <<845.6260, -1018.7468, 26.5403>>    fSpawnHeading 	= 0.0000    BREAK
				CASE 7    vSpawnPoint 	= <<841.7612, -1019.1021, 26.5380>>    fSpawnHeading 	= 0.0000    BREAK
				CASE 8    vSpawnPoint 	= <<837.4459, -1020.3794, 26.2589>>    fSpawnHeading 	= 0.0000    BREAK
				CASE 9    vSpawnPoint 	= <<839.3450, -1019.0980, 26.4635>>    fSpawnHeading 	= 0.0000    BREAK
				CASE 10    vSpawnPoint 	= <<842.8562, -1017.2861, 26.5497>>    fSpawnHeading 	= 0.0000    BREAK
				CASE 11    vSpawnPoint 	= <<845.0236, -1016.9378, 26.7836>>    fSpawnHeading 	= 0.0000    BREAK
				CASE 12    vSpawnPoint 	= <<840.4464, -1017.4172, 26.5488>>    fSpawnHeading 	= 0.0000    BREAK
				CASE 13    vSpawnPoint 	= <<847.1328, -1017.7123, 26.8685>>    fSpawnHeading 	= 0.0000    BREAK
				CASE 14    vSpawnPoint 	= <<837.7717, -1017.6318, 26.2765>>    fSpawnHeading 	= 0.0000    BREAK
				CASE 15    vSpawnPoint 	= <<841.7704, -1015.7899, 26.6437>>    fSpawnHeading 	= 0.0000    BREAK
				CASE 16    vSpawnPoint 	= <<843.8491, -1015.7678, 26.8780>>    fSpawnHeading 	= 0.0000    BREAK
				CASE 17    vSpawnPoint 	= <<839.4379, -1015.9055, 26.4360>>    fSpawnHeading 	= 0.0000    BREAK
				CASE 18    vSpawnPoint 	= <<847.0569, -1016.1992, 27.1520>>    fSpawnHeading 	= 0.0000    BREAK
				CASE 19    vSpawnPoint 	= <<836.9308, -1015.6498, 26.1668>>    fSpawnHeading 	= 0.0000    BREAK
				CASE 20    vSpawnPoint 	= <<852.2017, -1012.9470, 27.9794>>    fSpawnHeading 	= 0.0000    BREAK
				CASE 21    vSpawnPoint 	= <<853.5396, -1013.7178, 28.1508>>    fSpawnHeading 	= 0.0000    BREAK
				CASE 22    vSpawnPoint 	= <<850.6238, -1013.5739, 27.8080>>    fSpawnHeading 	= 0.0000    BREAK
				CASE 23    vSpawnPoint 	= <<855.1817, -1012.8339, 28.3011>>    fSpawnHeading 	= 0.0000    BREAK
				CASE 24    vSpawnPoint 	= <<848.9149, -1012.8273, 27.5709>>    fSpawnHeading 	= 0.0000    BREAK
				CASE 25    vSpawnPoint 	= <<833.9338, -1012.1498, 25.9434>>    fSpawnHeading 	= 0.0000    BREAK
				CASE 26    vSpawnPoint 	= <<828.7020, -1014.0722, 25.4595>>    fSpawnHeading 	= 0.0000    BREAK
				CASE 27    vSpawnPoint 	= <<831.2758, -1011.9677, 25.7395>>    fSpawnHeading 	= 0.0000    BREAK
				CASE 28    vSpawnPoint 	= <<831.6010, -1014.1122, 25.7333>>    fSpawnHeading 	= 0.0000    BREAK
				CASE 29    vSpawnPoint 	= <<828.6831, -1011.7847, 25.5390>>    fSpawnHeading 	= 0.0000    BREAK
				DEFAULT RETURN FALSE
			ENDSWITCH
		BREAK
		CASE FMC_DROPOFF_AMMU_NATION_CYPRESS
			SWITCH iSpawnPoint
				CASE 0    vSpawnPoint 	= <<813.0250, -2145.9922, 28.3737>>    fSpawnHeading 	= 358.5600    BREAK
				CASE 1    vSpawnPoint 	= <<814.2990, -2145.1790, 28.3023>>    fSpawnHeading 	= 358.5600    BREAK
				CASE 2    vSpawnPoint 	= <<811.8075, -2144.8972, 28.2891>>    fSpawnHeading 	= 358.5600    BREAK
				CASE 3    vSpawnPoint 	= <<816.1917, -2145.9722, 28.3376>>    fSpawnHeading 	= 358.5600    BREAK
				CASE 4    vSpawnPoint 	= <<810.5428, -2146.0015, 28.3230>>    fSpawnHeading 	= 358.5600    BREAK
				CASE 5    vSpawnPoint 	= <<813.3445, -2143.1265, 28.2896>>    fSpawnHeading 	= 358.5600    BREAK
				CASE 6    vSpawnPoint 	= <<814.9783, -2143.3347, 28.2907>>    fSpawnHeading 	= 358.5600    BREAK
				CASE 7    vSpawnPoint 	= <<815.7664, -2141.5242, 28.2918>>    fSpawnHeading 	= 358.5600    BREAK
				CASE 8    vSpawnPoint 	= <<816.6390, -2143.8809, 28.2923>>    fSpawnHeading 	= 358.5600    BREAK
				CASE 9    vSpawnPoint 	= <<810.9135, -2142.9360, 28.3171>>    fSpawnHeading 	= 358.5600    BREAK
				CASE 10    vSpawnPoint 	= <<813.3223, -2140.8818, 28.2907>>    fSpawnHeading 	= 358.5600    BREAK
				CASE 11    vSpawnPoint 	= <<815.3239, -2139.4338, 28.2915>>    fSpawnHeading 	= 358.5600    BREAK
				CASE 12    vSpawnPoint 	= <<811.7744, -2139.6670, 28.3016>>    fSpawnHeading 	= 358.5600    BREAK
				CASE 13    vSpawnPoint 	= <<817.1050, -2139.5642, 28.2930>>    fSpawnHeading 	= 358.5600    BREAK
				CASE 14    vSpawnPoint 	= <<811.0197, -2141.1506, 28.3119>>    fSpawnHeading 	= 358.5600    BREAK
				CASE 15    vSpawnPoint 	= <<815.1187, -2136.3445, 28.2962>>    fSpawnHeading 	= 358.5600    BREAK
				CASE 16    vSpawnPoint 	= <<812.0568, -2134.6794, 28.3005>>    fSpawnHeading 	= 358.5600    BREAK
				CASE 17    vSpawnPoint 	= <<813.0230, -2136.5571, 28.2977>>    fSpawnHeading 	= 358.5600    BREAK
				CASE 18    vSpawnPoint 	= <<817.5277, -2135.7327, 28.2937>>    fSpawnHeading 	= 358.5600    BREAK
				CASE 19    vSpawnPoint 	= <<810.8301, -2136.6436, 28.2987>>    fSpawnHeading 	= 358.5600    BREAK
				CASE 20    vSpawnPoint 	= <<801.4501, -2127.9155, 28.3252>>    fSpawnHeading 	= 88.6733    BREAK
				CASE 21    vSpawnPoint 	= <<802.6937, -2123.1216, 28.3491>>    fSpawnHeading 	= 84.2288    BREAK
				CASE 22    vSpawnPoint 	= <<799.9276, -2125.7920, 28.3930>>    fSpawnHeading 	= 80.9760    BREAK
				CASE 23    vSpawnPoint 	= <<803.4741, -2125.4084, 28.3595>>    fSpawnHeading 	= 92.4655    BREAK
				CASE 24    vSpawnPoint 	= <<799.1015, -2123.2512, 28.4295>>    fSpawnHeading 	= 97.4219    BREAK
				CASE 25    vSpawnPoint 	= <<796.7129, -2123.8413, 28.4681>>    fSpawnHeading 	= 88.5643    BREAK
				CASE 26    vSpawnPoint 	= <<797.1804, -2121.4241, 28.4513>>    fSpawnHeading 	= 89.1153    BREAK
				CASE 27    vSpawnPoint 	= <<796.3558, -2125.8550, 28.4711>>    fSpawnHeading 	= 96.8685    BREAK
				CASE 28    vSpawnPoint 	= <<798.2308, -2127.2371, 28.3967>>    fSpawnHeading 	= 84.2091    BREAK
				CASE 29    vSpawnPoint 	= <<796.0472, -2127.9749, 28.4178>>    fSpawnHeading 	= 96.7001    BREAK
				DEFAULT RETURN FALSE
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL GET_FMC_CUSTOM_BIKE_DELIVERY_POST_SCENE_SPAWN_POINT(FREEMODE_DELIVERY_DROPOFFS eDropoffID, INT iSpawnPoint, VECTOR &vSpawnPoint, FLOAT &fSpawnHeading)
	SWITCH eDropoffID
		CASE FMC_DROPOFF_BIKE_SHOP_CUSTOMER_1
			SWITCH iSpawnPoint
				CASE 0    vSpawnPoint 	= <<300.6062, -1978.8716, 21.1119>>    fSpawnHeading 	= 229.3200    BREAK
				CASE 1    vSpawnPoint 	= <<300.4942, -1980.7212, 20.9761>>    fSpawnHeading 	= 229.3200    BREAK
				CASE 2    vSpawnPoint 	= <<302.4556, -1978.6598, 21.2352>>    fSpawnHeading 	= 229.3200    BREAK
				CASE 3    vSpawnPoint 	= <<299.8182, -1982.4626, 20.8028>>    fSpawnHeading 	= 229.3200    BREAK
				CASE 4    vSpawnPoint 	= <<302.5442, -1980.5479, 21.1118>>    fSpawnHeading 	= 229.3200    BREAK
				CASE 5    vSpawnPoint 	= <<302.2559, -1984.5244, 20.8482>>    fSpawnHeading 	= 229.3200    BREAK
				CASE 6    vSpawnPoint 	= <<300.9729, -1984.8132, 20.7357>>    fSpawnHeading 	= 229.3200    BREAK
				CASE 7    vSpawnPoint 	= <<304.2935, -1979.2266, 21.2899>>    fSpawnHeading 	= 229.3200    BREAK
				CASE 8    vSpawnPoint 	= <<299.0645, -1987.4083, 20.3779>>    fSpawnHeading 	= 229.3200    BREAK
				CASE 9    vSpawnPoint 	= <<300.7263, -1986.5614, 20.5476>>    fSpawnHeading 	= 229.3200    BREAK
				CASE 10    vSpawnPoint 	= <<309.1514, -1975.8994, 21.7108>>    fSpawnHeading 	= 229.3200    BREAK
				CASE 11    vSpawnPoint 	= <<298.9060, -1988.9249, 20.2840>>    fSpawnHeading 	= 229.3200    BREAK
				CASE 12    vSpawnPoint 	= <<307.4952, -1977.8303, 21.4982>>    fSpawnHeading 	= 229.3200    BREAK
				CASE 13    vSpawnPoint 	= <<297.0498, -1989.5029, 20.1440>>    fSpawnHeading 	= 229.3200    BREAK
				CASE 14    vSpawnPoint 	= <<306.7154, -1979.3617, 21.3247>>    fSpawnHeading 	= 229.3200    BREAK
				CASE 15    vSpawnPoint 	= <<305.6027, -1980.9275, 21.1474>>    fSpawnHeading 	= 229.3200    BREAK
				DEFAULT RETURN FALSE
			ENDSWITCH
		BREAK
		CASE FMC_DROPOFF_BIKE_SHOP_CUSTOMER_2
			SWITCH iSpawnPoint
				CASE 0    vSpawnPoint 	= <<480.9947, -1777.2462, 27.5537>>    fSpawnHeading 	= 283.2200    BREAK
				CASE 1    vSpawnPoint 	= <<482.0106, -1778.6466, 27.5409>>    fSpawnHeading 	= 283.2200    BREAK
				CASE 2    vSpawnPoint 	= <<481.2838, -1775.8481, 27.5130>>    fSpawnHeading 	= 283.2200    BREAK
				CASE 3    vSpawnPoint 	= <<482.2216, -1780.7357, 27.5641>>    fSpawnHeading 	= 283.2200    BREAK
				CASE 4    vSpawnPoint 	= <<480.6660, -1774.3494, 27.4673>>    fSpawnHeading 	= 283.2200    BREAK
				CASE 5    vSpawnPoint 	= <<484.0490, -1777.5240, 27.5364>>    fSpawnHeading 	= 291.2112    BREAK
				CASE 6    vSpawnPoint 	= <<484.5346, -1779.4493, 27.5383>>    fSpawnHeading 	= 291.2448    BREAK
				CASE 7    vSpawnPoint 	= <<483.2788, -1774.6102, 27.5372>>    fSpawnHeading 	= 283.2200    BREAK
				CASE 8    vSpawnPoint 	= <<485.0942, -1781.2805, 27.5396>>    fSpawnHeading 	= 299.2265    BREAK
				CASE 9    vSpawnPoint 	= <<482.8514, -1772.4104, 27.5323>>    fSpawnHeading 	= 283.2200    BREAK
				CASE 10    vSpawnPoint 	= <<485.0965, -1773.3945, 27.4467>>    fSpawnHeading 	= 283.2200    BREAK
				CASE 11    vSpawnPoint 	= <<485.5443, -1775.6084, 27.4549>>    fSpawnHeading 	= 283.2200    BREAK
				CASE 12    vSpawnPoint 	= <<484.5072, -1769.6987, 27.5732>>    fSpawnHeading 	= 283.2200    BREAK
				CASE 13    vSpawnPoint 	= <<486.4534, -1778.3494, 27.4736>>    fSpawnHeading 	= 291.2299    BREAK
				CASE 14    vSpawnPoint 	= <<488.2630, -1781.6333, 27.5494>>    fSpawnHeading 	= 299.2273    BREAK
				CASE 15    vSpawnPoint 	= <<484.2397, -1767.6708, 27.5796>>    fSpawnHeading 	= 275.2268    BREAK
				DEFAULT RETURN FALSE
			ENDSWITCH
		BREAK
		CASE FMC_DROPOFF_BIKE_SHOP_CUSTOMER_3
			SWITCH iSpawnPoint
				CASE 0    vSpawnPoint 	= <<-59.5249, -1787.5508, 26.8248>>    fSpawnHeading 	= 139.5050    BREAK
				CASE 1    vSpawnPoint 	= <<-61.2169, -1787.3331, 26.8347>>    fSpawnHeading 	= 139.5050    BREAK
				CASE 2    vSpawnPoint 	= <<-59.1889, -1789.0797, 26.7257>>    fSpawnHeading 	= 139.5050    BREAK
				CASE 3    vSpawnPoint 	= <<-61.8512, -1785.7693, 26.9364>>    fSpawnHeading 	= 139.5050    BREAK
				CASE 4    vSpawnPoint 	= <<-56.8806, -1789.7841, 26.6848>>    fSpawnHeading 	= 139.5050    BREAK
				CASE 5    vSpawnPoint 	= <<-62.5349, -1788.9460, 26.7468>>    fSpawnHeading 	= 139.5050    BREAK
				CASE 6    vSpawnPoint 	= <<-63.3586, -1787.7305, 26.8196>>    fSpawnHeading 	= 139.5050    BREAK
				CASE 7    vSpawnPoint 	= <<-61.3205, -1790.3142, 26.6635>>    fSpawnHeading 	= 139.5050    BREAK
				CASE 8    vSpawnPoint 	= <<-64.6257, -1787.9185, 26.8524>>    fSpawnHeading 	= 139.5050    BREAK
				CASE 9    vSpawnPoint 	= <<-60.4785, -1791.4777, 26.5935>>    fSpawnHeading 	= 139.5050    BREAK
				CASE 10    vSpawnPoint 	= <<-67.3155, -1785.4554, 27.1235>>    fSpawnHeading 	= 139.5050    BREAK
				CASE 11    vSpawnPoint 	= <<-54.5188, -1791.8643, 26.5583>>    fSpawnHeading 	= 139.5050    BREAK
				CASE 12    vSpawnPoint 	= <<-55.8636, -1795.0536, 26.4928>>    fSpawnHeading 	= 139.5050    BREAK
				CASE 13    vSpawnPoint 	= <<-65.7040, -1782.6760, 27.1644>>    fSpawnHeading 	= 139.5050    BREAK
				CASE 14    vSpawnPoint 	= <<-54.4565, -1796.3589, 26.4153>>    fSpawnHeading 	= 139.5050    BREAK
				CASE 15    vSpawnPoint 	= <<-53.2019, -1797.7345, 26.2982>>    fSpawnHeading 	= 139.5050    BREAK
				DEFAULT RETURN FALSE
			ENDSWITCH
		BREAK
		CASE FMC_DROPOFF_BIKE_SHOP_CUSTOMER_4
			SWITCH iSpawnPoint
				CASE 0    vSpawnPoint 	= <<92.3042, -286.4365, 45.4773>>    fSpawnHeading 	= 160.5600    BREAK
				CASE 1    vSpawnPoint 	= <<90.1923, -286.7465, 45.5287>>    fSpawnHeading 	= 160.5600    BREAK
				CASE 2    vSpawnPoint 	= <<93.6568, -287.9684, 45.4291>>    fSpawnHeading 	= 160.5600    BREAK
				CASE 3    vSpawnPoint 	= <<88.6653, -285.4317, 45.5794>>    fSpawnHeading 	= 160.5600    BREAK
				CASE 4    vSpawnPoint 	= <<95.4091, -287.6762, 45.3875>>    fSpawnHeading 	= 160.5600    BREAK
				CASE 5    vSpawnPoint 	= <<91.2135, -290.3836, 45.4624>>    fSpawnHeading 	= 160.5600    BREAK
				CASE 6    vSpawnPoint 	= <<88.4979, -289.1112, 45.5480>>    fSpawnHeading 	= 160.5600    BREAK
				CASE 7    vSpawnPoint 	= <<93.5141, -291.0777, 45.3947>>    fSpawnHeading 	= 160.5600    BREAK
				CASE 8    vSpawnPoint 	= <<85.5219, -287.5064, 45.6416>>    fSpawnHeading 	= 160.5600    BREAK
				CASE 9    vSpawnPoint 	= <<95.5738, -290.4192, 45.3497>>    fSpawnHeading 	= 160.5600    BREAK
				CASE 10    vSpawnPoint 	= <<82.2315, -286.8285, 45.7313>>    fSpawnHeading 	= 160.5600    BREAK
				CASE 11    vSpawnPoint 	= <<79.9879, -286.6301, 45.7943>>    fSpawnHeading 	= 160.5600    BREAK
				CASE 12    vSpawnPoint 	= <<100.1939, -291.9995, 45.2016>>   fSpawnHeading 	= 160.5600    BREAK
				CASE 13    vSpawnPoint 	= <<77.5080, -285.9527, 45.8649>>    fSpawnHeading 	= 160.5600    BREAK
				CASE 14    vSpawnPoint 	= <<98.4171, -293.0549, 45.2415>>    fSpawnHeading 	= 160.5600    BREAK
				CASE 15    vSpawnPoint 	= <<100.3141, -294.1104, 45.1786>>   fSpawnHeading 	= 160.5600    BREAK
				DEFAULT RETURN FALSE
			ENDSWITCH
		BREAK
		CASE FMC_DROPOFF_BIKE_SHOP_CUSTOMER_5
			SWITCH iSpawnPoint
				CASE 0    vSpawnPoint 	= <<317.2592, -231.8367, 52.9641>>    fSpawnHeading 	= 160.2000    BREAK
				CASE 1    vSpawnPoint 	= <<315.3350, -232.3794, 52.9770>>    fSpawnHeading 	= 160.2000    BREAK
				CASE 2    vSpawnPoint 	= <<317.9468, -233.4886, 52.9567>>    fSpawnHeading 	= 160.2000    BREAK
				CASE 3    vSpawnPoint 	= <<313.4267, -231.0258, 52.9924>>    fSpawnHeading 	= 160.2000    BREAK
				CASE 4    vSpawnPoint 	= <<320.4322, -233.7855, 53.0222>>    fSpawnHeading 	= 160.2000    BREAK
				CASE 5    vSpawnPoint 	= <<315.1367, -235.8213, 52.9720>>    fSpawnHeading 	= 160.2000    BREAK
				CASE 6    vSpawnPoint 	= <<312.3393, -234.2323, 52.9917>>    fSpawnHeading 	= 160.2000    BREAK
				CASE 7    vSpawnPoint 	= <<317.5215, -236.4466, 52.9722>>    fSpawnHeading 	= 160.2000    BREAK
				CASE 8    vSpawnPoint 	= <<309.6986, -233.3682, 53.0392>>    fSpawnHeading 	= 160.2000    BREAK
				CASE 9    vSpawnPoint 	= <<319.8015, -237.3996, 53.0208>>    fSpawnHeading 	= 160.2000    BREAK
				CASE 10    vSpawnPoint 	= <<307.9164, -234.9477, 53.0763>>    fSpawnHeading 	= 160.2000    BREAK
				CASE 11    vSpawnPoint 	= <<305.2618, -234.7247, 53.0913>>    fSpawnHeading 	= 160.2000    BREAK
				CASE 12    vSpawnPoint 	= <<319.8857, -240.6826, 52.9876>>    fSpawnHeading 	= 160.2000    BREAK
				CASE 13    vSpawnPoint 	= <<306.8459, -236.8460, 53.1378>>    fSpawnHeading 	= 160.2000    BREAK
				CASE 14    vSpawnPoint 	= <<317.5546, -241.7937, 53.0142>>    fSpawnHeading 	= 160.2000    BREAK
				CASE 15    vSpawnPoint 	= <<319.3887, -242.9980, 52.9568>>    fSpawnHeading 	= 160.2000    BREAK
				DEFAULT RETURN FALSE
			ENDSWITCH
		BREAK
		CASE FMC_DROPOFF_BIKE_SHOP_CUSTOMER_6
			SWITCH iSpawnPoint
				CASE 0    vSpawnPoint 	= <<-3.0225, 6.1529, 69.9647>>    fSpawnHeading 	= 339.4800    BREAK
				CASE 1    vSpawnPoint 	= <<-1.1343, 6.0814, 69.9022>>    fSpawnHeading 	= 339.4800    BREAK
				CASE 2    vSpawnPoint 	= <<-4.4954, 7.1953, 70.0047>>    fSpawnHeading 	= 339.4800    BREAK
				CASE 3    vSpawnPoint 	= <<0.5438, 4.6157, 69.8627>>     fSpawnHeading 	= 339.4800    BREAK
				CASE 4    vSpawnPoint 	= <<-7.2074, 8.0501, 70.0843>>    fSpawnHeading 	= 339.4800    BREAK
				CASE 5    vSpawnPoint 	= <<-3.8519, 9.1500, 69.9657>>    fSpawnHeading 	= 339.4800    BREAK
				CASE 6    vSpawnPoint 	= <<-1.8503, 8.4582, 69.9044>>    fSpawnHeading 	= 339.4800    BREAK
				CASE 7    vSpawnPoint 	= <<-5.9728, 10.2857, 70.0238>>   fSpawnHeading 	= 339.4800    BREAK
				CASE 8    vSpawnPoint 	= <<2.0413, 7.1268, 69.7947>>     fSpawnHeading 	= 339.4800    BREAK
				CASE 9    vSpawnPoint 	= <<-8.0682, 10.2583, 70.0910>>   fSpawnHeading 	= 339.4800    BREAK
				CASE 10    vSpawnPoint 	= <<10.9385, 3.8025, 69.6148>>    fSpawnHeading 	= 339.4800    BREAK
				CASE 11    vSpawnPoint 	= <<4.1623, 6.8436, 69.8300>>     fSpawnHeading 	= 339.4800    BREAK
				CASE 12    vSpawnPoint 	= <<9.1738, 2.8482, 69.6649>>     fSpawnHeading 	= 339.4800    BREAK
				CASE 13    vSpawnPoint 	= <<6.6270, 6.0308, 69.7463>>     fSpawnHeading 	= 339.4800    BREAK
				CASE 14    vSpawnPoint 	= <<-10.3915, 12.0792, 70.2219>>  fSpawnHeading 	= 339.4800    BREAK
				CASE 15    vSpawnPoint 	= <<-12.0415, 12.9378, 70.3544>>  fSpawnHeading 	= 339.4800    BREAK
				DEFAULT RETURN FALSE
			ENDSWITCH
		BREAK
		CASE FMC_DROPOFF_BIKE_SHOP_CUSTOMER_7
			SWITCH iSpawnPoint
				CASE 0    vSpawnPoint 	= <<-966.8731, -1096.9984, 1.0661>>    fSpawnHeading 	= 118.4700    BREAK
				CASE 1    vSpawnPoint 	= <<-966.8069, -1095.5145, 1.0908>>    fSpawnHeading 	= 118.4700    BREAK
				CASE 2    vSpawnPoint 	= <<-965.6844, -1097.6598, 1.1102>>    fSpawnHeading 	= 118.4700    BREAK
				CASE 3    vSpawnPoint 	= <<-968.6230, -1095.6478, 1.1072>>    fSpawnHeading 	= 118.4700    BREAK
				CASE 4    vSpawnPoint 	= <<-966.4496, -1099.2361, 1.1863>>    fSpawnHeading 	= 118.4700    BREAK
				CASE 5    vSpawnPoint 	= <<-971.0955, -1098.0636, 1.1228>>    fSpawnHeading 	= 118.4700    BREAK
				CASE 6    vSpawnPoint 	= <<-968.7744, -1099.1240, 1.1008>>    fSpawnHeading 	= 118.4700    BREAK
				CASE 7    vSpawnPoint 	= <<-970.6662, -1100.2196, 1.1503>>    fSpawnHeading 	= 118.4700    BREAK
				CASE 8    vSpawnPoint 	= <<-970.7925, -1096.5378, 1.1053>>    fSpawnHeading 	= 118.4700    BREAK
				CASE 9    vSpawnPoint 	= <<-969.0001, -1101.4077, 1.1503>>    fSpawnHeading 	= 118.4700    BREAK
				CASE 10    vSpawnPoint 	= <<-974.4060, -1099.0560, 1.1248>>    fSpawnHeading 	= 118.4700    BREAK
				CASE 11    vSpawnPoint 	= <<-971.5845, -1102.4801, 1.1479>>    fSpawnHeading 	= 118.4700    BREAK
				CASE 12    vSpawnPoint 	= <<-975.1439, -1101.2616, 1.0974>>    fSpawnHeading 	= 118.4700    BREAK
				CASE 13    vSpawnPoint 	= <<-972.8051, -1099.8740, 1.1213>>    fSpawnHeading 	= 118.4700    BREAK
				CASE 14    vSpawnPoint 	= <<-973.5844, -1102.1614, 1.1241>>    fSpawnHeading 	= 118.4700    BREAK
				CASE 15    vSpawnPoint 	= <<-973.9477, -1104.0319, 1.1503>>    fSpawnHeading 	= 118.4700    BREAK
				DEFAULT RETURN FALSE
			ENDSWITCH
		BREAK
		CASE FMC_DROPOFF_BIKE_SHOP_CUSTOMER_8
			SWITCH iSpawnPoint
				CASE 0    vSpawnPoint 	= <<-1139.4696, -1184.0885, 1.3282>>    fSpawnHeading 	= 209.8800    BREAK
				CASE 1    vSpawnPoint 	= <<-1140.2664, -1185.6703, 1.2734>>    fSpawnHeading 	= 209.8800    BREAK
				CASE 2    vSpawnPoint 	= <<-1137.3188, -1183.9081, 1.2819>>    fSpawnHeading 	= 209.8800    BREAK
				CASE 3    vSpawnPoint 	= <<-1142.1636, -1186.3445, 1.3359>>    fSpawnHeading 	= 209.8800    BREAK
				CASE 4    vSpawnPoint 	= <<-1135.6644, -1182.4659, 1.2775>>    fSpawnHeading 	= 209.8800    BREAK
				CASE 5    vSpawnPoint 	= <<-1136.3031, -1186.9132, 1.1886>>    fSpawnHeading 	= 209.8800    BREAK
				CASE 6    vSpawnPoint 	= <<-1138.1936, -1189.0997, 1.1718>>    fSpawnHeading 	= 209.8800    BREAK
				CASE 7    vSpawnPoint 	= <<-1133.9114, -1186.4707, 1.1820>>    fSpawnHeading 	= 209.8800    BREAK
				CASE 8    vSpawnPoint 	= <<-1140.9430, -1190.1595, 1.2975>>    fSpawnHeading 	= 209.8800    BREAK
				CASE 9    vSpawnPoint 	= <<-1132.7092, -1185.0823, 1.2000>>    fSpawnHeading 	= 209.8800    BREAK
				CASE 10    vSpawnPoint 	= <<-1144.9736, -1190.0385, 1.5371>>    fSpawnHeading 	= 209.8800    BREAK
				CASE 11    vSpawnPoint 	= <<-1142.8453, -1191.4398, 1.4540>>    fSpawnHeading 	= 209.8800    BREAK
				CASE 12    vSpawnPoint 	= <<-1129.3333, -1181.7225, 1.2873>>    fSpawnHeading 	= 209.8800    BREAK
				CASE 13    vSpawnPoint 	= <<-1144.9158, -1192.3132, 1.5605>>    fSpawnHeading 	= 209.8800    BREAK
				CASE 14    vSpawnPoint 	= <<-1129.5873, -1184.2489, 1.2885>>    fSpawnHeading 	= 209.8800    BREAK
				CASE 15    vSpawnPoint 	= <<-1127.1061, -1182.9778, 1.2873>>    fSpawnHeading 	= 209.8800    BREAK
				DEFAULT RETURN FALSE
			ENDSWITCH
		BREAK
		CASE FMC_DROPOFF_BIKE_SHOP_CUSTOMER_9
			SWITCH iSpawnPoint
				CASE 0    vSpawnPoint 	= <<-1637.0779, -351.0717, 49.0757>>    fSpawnHeading 	= 41.7600    BREAK
				CASE 1    vSpawnPoint 	= <<-1635.5764, -350.0691, 49.0724>>    fSpawnHeading 	= 41.7600    BREAK
				CASE 2    vSpawnPoint 	= <<-1638.3634, -352.2859, 49.0109>>    fSpawnHeading 	= 41.7600    BREAK
				CASE 3    vSpawnPoint 	= <<-1635.0309, -348.2437, 49.1821>>    fSpawnHeading 	= 41.7600    BREAK
				CASE 4    vSpawnPoint 	= <<-1640.8241, -352.5830, 48.9800>>    fSpawnHeading 	= 41.7600    BREAK
				CASE 5    vSpawnPoint 	= <<-1635.7872, -346.4776, 49.1863>>    fSpawnHeading 	= 41.7600    BREAK
				CASE 6    vSpawnPoint 	= <<-1632.3519, -342.2065, 49.3405>>    fSpawnHeading 	= 41.7600    BREAK
				CASE 7    vSpawnPoint 	= <<-1641.5229, -350.7123, 49.0050>>    fSpawnHeading 	= 41.7600    BREAK
				CASE 8    vSpawnPoint 	= <<-1633.6896, -344.6217, 49.2689>>    fSpawnHeading 	= 41.7600    BREAK
				CASE 9    vSpawnPoint 	= <<-1643.8156, -352.8540, 48.9007>>    fSpawnHeading 	= 41.7600    BREAK
				CASE 10    vSpawnPoint 	= <<-1632.9469, -339.7177, 49.3646>>    fSpawnHeading 	= 49.7255    BREAK
				CASE 11    vSpawnPoint 	= <<-1634.5056, -341.7250, 49.3001>>    fSpawnHeading 	= 41.7600    BREAK
				CASE 12    vSpawnPoint 	= <<-1644.1384, -350.4545, 48.9220>>    fSpawnHeading 	= 41.7605    BREAK
				CASE 13    vSpawnPoint 	= <<-1636.0004, -343.3769, 49.2326>>    fSpawnHeading 	= 41.7600    BREAK
				CASE 14    vSpawnPoint 	= <<-1646.1245, -352.0166, 48.8493>>    fSpawnHeading 	= 33.7610    BREAK
				CASE 15    vSpawnPoint 	= <<-1648.4927, -353.1526, 48.7689>>    fSpawnHeading 	= 33.7668    BREAK
				DEFAULT RETURN FALSE
			ENDSWITCH
		BREAK
		CASE FMC_DROPOFF_BIKE_SHOP_CUSTOMER_10
			SWITCH iSpawnPoint
				CASE 0    vSpawnPoint 	= <<-1266.1199, 458.0824, 93.8581>>    fSpawnHeading 	= 7.9200    BREAK
				CASE 1    vSpawnPoint 	= <<-1264.4247, 457.8297, 93.7471>>    fSpawnHeading 	= 7.9200    BREAK
				CASE 2    vSpawnPoint 	= <<-1267.2239, 457.3708, 93.9507>>    fSpawnHeading 	= 7.9200    BREAK
				CASE 3    vSpawnPoint 	= <<-1262.8226, 458.6671, 93.6318>>    fSpawnHeading 	= 7.9200    BREAK
				CASE 4    vSpawnPoint 	= <<-1268.8721, 457.9288, 94.0859>>    fSpawnHeading 	= 7.9200    BREAK
				CASE 5    vSpawnPoint 	= <<-1270.1536, 456.9672, 93.9855>>    fSpawnHeading 	= 7.9200    BREAK
				CASE 6    vSpawnPoint 	= <<-1261.4332, 458.9881, 93.5309>>    fSpawnHeading 	= 7.9200    BREAK
				CASE 7    vSpawnPoint 	= <<-1272.5791, 456.5330, 94.2243>>    fSpawnHeading 	= 7.9200    BREAK
				CASE 8    vSpawnPoint 	= <<-1259.8744, 458.2836, 93.4779>>    fSpawnHeading 	= 7.9200    BREAK
				CASE 9    vSpawnPoint 	= <<-1274.8204, 456.6397, 94.4202>>    fSpawnHeading 	= 7.9200    BREAK
				CASE 10    vSpawnPoint 	= <<-1258.8152, 459.4575, 93.3050>>    fSpawnHeading 	= 7.9200    BREAK
				CASE 11    vSpawnPoint 	= <<-1255.2389, 460.0731, 92.9908>>    fSpawnHeading 	= 7.9200    BREAK
				CASE 12    vSpawnPoint 	= <<-1271.4852, 457.7068, 94.1534>>    fSpawnHeading 	= 7.9200    BREAK
				CASE 13    vSpawnPoint 	= <<-1256.9475, 459.7319, 93.1458>>    fSpawnHeading 	= 7.9200    BREAK
				CASE 14    vSpawnPoint 	= <<-1273.8260, 457.4357, 94.3622>>    fSpawnHeading 	= 7.9200    BREAK
				CASE 15    vSpawnPoint 	= <<-1275.8108, 457.2439, 94.5302>>    fSpawnHeading 	= 7.9200    BREAK
				DEFAULT RETURN FALSE
			ENDSWITCH
		BREAK
		CASE FMC_DROPOFF_BIKE_SHOP_CUSTOMER_11
			SWITCH iSpawnPoint
				CASE 0    vSpawnPoint 	= <<1374.1147, -1525.4778, 55.7746>>    fSpawnHeading 	= 203.7600    BREAK
				CASE 1    vSpawnPoint 	= <<1372.9116, -1526.7694, 55.6108>>    fSpawnHeading 	= 203.7600    BREAK
				CASE 2    vSpawnPoint 	= <<1375.8689, -1525.5016, 55.8327>>    fSpawnHeading 	= 203.7600    BREAK
				CASE 3    vSpawnPoint 	= <<1371.0883, -1527.0416, 55.6310>>    fSpawnHeading 	= 203.7600    BREAK
				CASE 4    vSpawnPoint 	= <<1377.4355, -1524.3645, 55.9978>>    fSpawnHeading 	= 203.7600    BREAK
				CASE 5    vSpawnPoint 	= <<1375.2325, -1527.4508, 55.5874>>    fSpawnHeading 	= 203.7600    BREAK
				CASE 6    vSpawnPoint 	= <<1372.4197, -1529.1847, 55.3540>>    fSpawnHeading 	= 203.7600    BREAK
				CASE 7    vSpawnPoint 	= <<1377.4817, -1526.8622, 55.6803>>    fSpawnHeading 	= 203.7600    BREAK
				CASE 8    vSpawnPoint 	= <<1370.2648, -1530.1738, 55.2509>>    fSpawnHeading 	= 203.7600    BREAK
				CASE 9    vSpawnPoint 	= <<1379.1115, -1525.9229, 55.8088>>    fSpawnHeading 	= 203.7600    BREAK
				CASE 10    vSpawnPoint 	= <<1380.6868, -1524.6873, 56.0087>>    fSpawnHeading 	= 203.7600    BREAK
				CASE 11    vSpawnPoint 	= <<1359.3622, -1533.8665, 54.0455>>    fSpawnHeading 	= 203.7600    BREAK
				CASE 12    vSpawnPoint 	= <<1382.5922, -1523.8125, 56.2006>>    fSpawnHeading 	= 203.7600    BREAK
				CASE 13    vSpawnPoint 	= <<1362.8143, -1533.1029, 54.4513>>    fSpawnHeading 	= 203.7600    BREAK
				CASE 14    vSpawnPoint 	= <<1384.6206, -1522.8240, 56.2064>>    fSpawnHeading 	= 203.7600    BREAK
				CASE 15    vSpawnPoint 	= <<1365.7805, -1531.8082, 54.8607>>    fSpawnHeading 	= 203.7600    BREAK
				DEFAULT RETURN FALSE
			ENDSWITCH
		BREAK
		CASE FMC_DROPOFF_BIKE_SHOP_CUSTOMER_12
			SWITCH iSpawnPoint
				CASE 0    vSpawnPoint 	= <<1250.8236, -578.5029, 68.0934>>    fSpawnHeading 	= 272.8800    BREAK
				CASE 1    vSpawnPoint 	= <<1251.6576, -579.9032, 68.0735>>    fSpawnHeading 	= 272.8800    BREAK
				CASE 2    vSpawnPoint 	= <<1251.4878, -576.9954, 68.0840>>    fSpawnHeading 	= 272.8800    BREAK
				CASE 3    vSpawnPoint 	= <<1252.2393, -581.7086, 68.0770>>    fSpawnHeading 	= 272.8800    BREAK
				CASE 4    vSpawnPoint 	= <<1251.5381, -574.8220, 68.0656>>    fSpawnHeading 	= 272.8800    BREAK
				CASE 5    vSpawnPoint 	= <<1254.0311, -580.0795, 68.1076>>    fSpawnHeading 	= 272.8800    BREAK
				CASE 6    vSpawnPoint 	= <<1254.6373, -584.7056, 68.1047>>    fSpawnHeading 	= 272.8800    BREAK
				CASE 7    vSpawnPoint 	= <<1253.7059, -577.7534, 68.0638>>    fSpawnHeading 	= 272.8800    BREAK
				CASE 8    vSpawnPoint 	= <<1254.1517, -582.5006, 68.1053>>    fSpawnHeading 	= 272.8800    BREAK
				CASE 9    vSpawnPoint 	= <<1253.2836, -574.2394, 68.0523>>    fSpawnHeading 	= 272.8800    BREAK
				CASE 10    vSpawnPoint 	= <<1255.7623, -581.1500, 67.9949>>    fSpawnHeading 	= 272.8800    BREAK
				CASE 11    vSpawnPoint 	= <<1255.9707, -583.3247, 67.9961>>    fSpawnHeading 	= 272.8800    BREAK
				CASE 12    vSpawnPoint 	= <<1255.5151, -578.9110, 67.9722>>    fSpawnHeading 	= 272.8800    BREAK
				CASE 13    vSpawnPoint 	= <<1256.3136, -585.9127, 67.9988>>    fSpawnHeading 	= 272.8800    BREAK
				CASE 14    vSpawnPoint 	= <<1255.2596, -576.6329, 67.9579>>    fSpawnHeading 	= 272.8800    BREAK
				CASE 15    vSpawnPoint 	= <<1255.3163, -574.4727, 68.0250>>    fSpawnHeading 	= 272.8800    BREAK
				DEFAULT RETURN FALSE
			ENDSWITCH
		BREAK
		CASE FMC_DROPOFF_BIKE_SHOP_CUSTOMER_13
			SWITCH iSpawnPoint
				CASE 0    vSpawnPoint 	= <<462.2386, 2611.7944, 42.2654>>    fSpawnHeading 	= 11.1600    BREAK
				CASE 1    vSpawnPoint 	= <<463.4591, 2612.7661, 42.2154>>    fSpawnHeading 	= 11.1600    BREAK
				CASE 2    vSpawnPoint 	= <<460.7997, 2612.3345, 42.2580>>    fSpawnHeading 	= 11.1600    BREAK
				CASE 3    vSpawnPoint 	= <<465.0406, 2613.7073, 42.1823>>    fSpawnHeading 	= 11.1600    BREAK
				CASE 4    vSpawnPoint 	= <<458.9385, 2611.8496, 42.2955>>    fSpawnHeading 	= 11.1600    BREAK
				CASE 5    vSpawnPoint 	= <<462.0022, 2614.7407, 42.1301>>    fSpawnHeading 	= 11.1600    BREAK
				CASE 6    vSpawnPoint 	= <<463.7515, 2614.9978, 42.1287>>    fSpawnHeading 	= 11.1600    BREAK
				CASE 7    vSpawnPoint 	= <<459.9230, 2614.2761, 42.1846>>    fSpawnHeading 	= 11.1600    BREAK
				CASE 8    vSpawnPoint 	= <<465.8802, 2615.5984, 42.1448>>    fSpawnHeading 	= 11.1600    BREAK
				CASE 9    vSpawnPoint 	= <<458.2535, 2613.9939, 42.2178>>    fSpawnHeading 	= 11.1600    BREAK
				CASE 10    vSpawnPoint 	= <<456.7284, 2613.5317, 42.2721>>    fSpawnHeading 	= 11.1600    BREAK
				CASE 11    vSpawnPoint 	= <<467.3104, 2615.9333, 42.1456>>    fSpawnHeading 	= 11.1600    BREAK
				CASE 12    vSpawnPoint 	= <<471.4044, 2616.9963, 42.1888>>    fSpawnHeading 	= 11.1600    BREAK
				CASE 13    vSpawnPoint 	= <<469.4947, 2616.6323, 42.2323>>    fSpawnHeading 	= 11.1600    BREAK
				CASE 14    vSpawnPoint 	= <<473.2236, 2617.3452, 42.1344>>    fSpawnHeading 	= 11.1600    BREAK
				CASE 15    vSpawnPoint 	= <<454.6343, 2613.6370, 42.3378>>    fSpawnHeading 	= 11.1600    BREAK
				DEFAULT RETURN FALSE
			ENDSWITCH
		BREAK
		CASE FMC_DROPOFF_BIKE_SHOP_CUSTOMER_14
			SWITCH iSpawnPoint
				CASE 0    vSpawnPoint 	= <<1386.0780, 3672.2673, 32.6116>>    fSpawnHeading 	= 23.0400    BREAK
				CASE 1    vSpawnPoint 	= <<1386.8401, 3673.2979, 32.5895>>    fSpawnHeading 	= 23.0400    BREAK
				CASE 2    vSpawnPoint 	= <<1384.3794, 3672.1492, 32.5747>>    fSpawnHeading 	= 23.0400    BREAK
				CASE 3    vSpawnPoint 	= <<1388.7865, 3673.3384, 32.6356>>    fSpawnHeading 	= 23.0400    BREAK
				CASE 4    vSpawnPoint 	= <<1382.5299, 3671.1821, 32.5676>>    fSpawnHeading 	= 23.0400    BREAK
				CASE 5    vSpawnPoint 	= <<1383.0425, 3672.5742, 32.5251>>    fSpawnHeading 	= 23.0400    BREAK
				CASE 6    vSpawnPoint 	= <<1388.3352, 3674.7512, 32.5686>>    fSpawnHeading 	= 23.0400    BREAK
				CASE 7    vSpawnPoint 	= <<1381.2587, 3671.8162, 32.5114>>    fSpawnHeading 	= 23.0400    BREAK
				CASE 8    vSpawnPoint 	= <<1392.0410, 3675.0085, 32.6489>>    fSpawnHeading 	= 23.0400    BREAK
				CASE 9    vSpawnPoint 	= <<1379.3224, 3671.2273, 32.4873>>    fSpawnHeading 	= 23.0400    BREAK
				CASE 10    vSpawnPoint 	= <<1390.5592, 3674.9629, 32.6145>>    fSpawnHeading 	= 23.0400    BREAK
				CASE 11    vSpawnPoint 	= <<1392.0118, 3676.0730, 32.6060>>    fSpawnHeading 	= 23.0400    BREAK
				CASE 12    vSpawnPoint 	= <<1376.7220, 3670.8557, 32.4516>>    fSpawnHeading 	= 23.0400    BREAK
				CASE 13    vSpawnPoint 	= <<1393.8416, 3677.1670, 32.6163>>    fSpawnHeading 	= 23.0400    BREAK
				CASE 14    vSpawnPoint 	= <<1374.5627, 3669.8411, 32.4263>>    fSpawnHeading 	= 23.0400    BREAK
				CASE 15    vSpawnPoint 	= <<1372.7241, 3669.6294, 32.4318>>    fSpawnHeading 	= 23.0400    BREAK
				DEFAULT RETURN FALSE
			ENDSWITCH
		BREAK
		CASE FMC_DROPOFF_BIKE_SHOP_CUSTOMER_15
			SWITCH iSpawnPoint
				CASE 0    vSpawnPoint 	= <<1855.9563, 3919.3247, 31.9301>>    fSpawnHeading 	= 284.0400    BREAK
				CASE 1    vSpawnPoint 	= <<1855.3885, 3917.9663, 31.9313>>    fSpawnHeading 	= 284.0400    BREAK
				CASE 2    vSpawnPoint 	= <<1854.7308, 3920.3035, 31.9318>>    fSpawnHeading 	= 284.0400    BREAK
				CASE 3    vSpawnPoint 	= <<1856.3613, 3917.0442, 31.9174>>    fSpawnHeading 	= 284.0400    BREAK
				CASE 4    vSpawnPoint 	= <<1855.0817, 3922.2854, 31.9174>>    fSpawnHeading 	= 284.0400    BREAK
				CASE 5    vSpawnPoint 	= <<1853.8339, 3926.3147, 31.8967>>    fSpawnHeading 	= 284.0400    BREAK
				CASE 6    vSpawnPoint 	= <<1854.9819, 3923.9482, 31.9375>>    fSpawnHeading 	= 284.0400    BREAK
				CASE 7    vSpawnPoint 	= <<1855.3456, 3925.3789, 32.0082>>    fSpawnHeading 	= 284.0400    BREAK
				CASE 8    vSpawnPoint 	= <<1857.7423, 3915.7505, 32.0044>>    fSpawnHeading 	= 284.0400    BREAK
				CASE 9    vSpawnPoint 	= <<1854.6917, 3927.5974, 32.0070>>    fSpawnHeading 	= 284.0400    BREAK
				CASE 10    vSpawnPoint 	= <<1844.3572, 3898.0891, 32.2260>>    fSpawnHeading 	= 196.0914    BREAK
				CASE 11    vSpawnPoint 	= <<1842.4280, 3897.8010, 32.2849>>    fSpawnHeading 	= 204.0258    BREAK
				CASE 12    vSpawnPoint 	= <<1854.0963, 3930.9209, 32.0102>>    fSpawnHeading 	= 284.0400    BREAK
				CASE 13    vSpawnPoint 	= <<1858.2833, 3914.0608, 32.0034>>    fSpawnHeading 	= 284.0400    BREAK
				CASE 14    vSpawnPoint 	= <<1853.6910, 3933.0315, 32.0119>>    fSpawnHeading 	= 284.0400    BREAK
				CASE 15    vSpawnPoint 	= <<1846.1432, 3898.2546, 32.1618>>    fSpawnHeading 	= 204.0063    BREAK
				DEFAULT RETURN FALSE
			ENDSWITCH
		BREAK
		CASE FMC_DROPOFF_BIKE_SHOP_CUSTOMER_16
			SWITCH iSpawnPoint
				CASE 0    vSpawnPoint 	= <<1686.8639, 4680.9102, 42.0554>>    fSpawnHeading 	= 266.0400    BREAK
				CASE 1    vSpawnPoint 	= <<1687.5359, 4679.6704, 42.0429>>    fSpawnHeading 	= 266.0400    BREAK
				CASE 2    vSpawnPoint 	= <<1687.6023, 4681.9038, 42.0489>>    fSpawnHeading 	= 266.0400    BREAK
				CASE 3    vSpawnPoint 	= <<1687.2236, 4678.0884, 42.0677>>    fSpawnHeading 	= 266.0400    BREAK
				CASE 4    vSpawnPoint 	= <<1687.9482, 4683.5283, 42.0306>>    fSpawnHeading 	= 266.0400    BREAK
				CASE 5    vSpawnPoint 	= <<1688.7631, 4680.7847, 41.9762>>    fSpawnHeading 	= 266.0400    BREAK
				CASE 6    vSpawnPoint 	= <<1688.7717, 4678.2104, 42.0246>>    fSpawnHeading 	= 266.0400    BREAK
				CASE 7    vSpawnPoint 	= <<1689.1948, 4682.6475, 41.9316>>    fSpawnHeading 	= 266.0400    BREAK
				CASE 8    vSpawnPoint 	= <<1688.6383, 4676.3252, 42.0658>>    fSpawnHeading 	= 266.0400    BREAK
				CASE 9    vSpawnPoint 	= <<1689.1558, 4684.6196, 41.9279>>    fSpawnHeading 	= 266.0400    BREAK
				CASE 10    vSpawnPoint 	= <<1688.8541, 4674.0088, 42.1049>>    fSpawnHeading 	= 266.0400    BREAK
				CASE 11    vSpawnPoint 	= <<1687.6631, 4672.8862, 42.1600>>    fSpawnHeading 	= 266.0400    BREAK
				CASE 12    vSpawnPoint 	= <<1688.8800, 4689.1274, 41.8574>>    fSpawnHeading 	= 266.0400    BREAK
				CASE 13    vSpawnPoint 	= <<1688.6974, 4671.3687, 42.1470>>    fSpawnHeading 	= 266.0400    BREAK
				CASE 14    vSpawnPoint 	= <<1689.7555, 4688.1719, 41.8303>>    fSpawnHeading 	= 266.0400    BREAK
				CASE 15    vSpawnPoint 	= <<1690.2902, 4690.0117, 41.7960>>    fSpawnHeading 	= 266.0400    BREAK	
				DEFAULT RETURN FALSE
			ENDSWITCH
		BREAK
		CASE FMC_DROPOFF_BIKE_SHOP_CUSTOMER_17
			SWITCH iSpawnPoint
				CASE 0    vSpawnPoint 	= <<-351.5226, 6331.5063, 28.9435>>    fSpawnHeading 	= 220.3200    BREAK
				CASE 1    vSpawnPoint 	= <<-352.2832, 6329.9878, 28.9220>>    fSpawnHeading 	= 220.3200    BREAK
				CASE 2    vSpawnPoint 	= <<-350.1364, 6332.0342, 28.9566>>    fSpawnHeading 	= 220.3200    BREAK
				CASE 3    vSpawnPoint 	= <<-353.6886, 6329.1685, 28.9063>>    fSpawnHeading 	= 220.3200    BREAK
				CASE 4    vSpawnPoint 	= <<-349.5961, 6333.1445, 28.9693>>    fSpawnHeading 	= 220.3200    BREAK
				CASE 5    vSpawnPoint 	= <<-350.8535, 6329.8647, 28.9262>>    fSpawnHeading 	= 220.3200    BREAK
				CASE 6    vSpawnPoint 	= <<-352.1539, 6328.7651, 28.9049>>    fSpawnHeading 	= 220.3200    BREAK
				CASE 7    vSpawnPoint 	= <<-349.5111, 6331.0806, 28.9488>>    fSpawnHeading 	= 220.3200    BREAK
				CASE 8    vSpawnPoint 	= <<-353.4104, 6327.5200, 28.8813>>    fSpawnHeading 	= 220.3200    BREAK
				CASE 9    vSpawnPoint 	= <<-348.2887, 6332.1689, 28.9660>>    fSpawnHeading 	= 220.3200    BREAK
				CASE 10    vSpawnPoint 	= <<-357.4413, 6325.1675, 28.8291>>    fSpawnHeading 	= 220.3200    BREAK
				CASE 11    vSpawnPoint 	= <<-347.0991, 6333.2129, 28.9867>>    fSpawnHeading 	= 220.3200    BREAK
				CASE 12    vSpawnPoint 	= <<-358.6964, 6324.0479, 28.8051>>    fSpawnHeading 	= 220.3200    BREAK
				CASE 13    vSpawnPoint 	= <<-348.4512, 6333.4248, 28.9742>>    fSpawnHeading 	= 220.3200    BREAK
				CASE 14    vSpawnPoint 	= <<-359.7550, 6323.0098, 28.7847>>    fSpawnHeading 	= 220.3200    BREAK
				CASE 15    vSpawnPoint 	= <<-355.4493, 6326.3267, 28.8551>>    fSpawnHeading 	= 220.3200    BREAK
				DEFAULT RETURN FALSE
			ENDSWITCH
		BREAK
		CASE FMC_DROPOFF_BIKE_SHOP_CUSTOMER_18
			SWITCH iSpawnPoint
				CASE 0    vSpawnPoint 	= <<46.3010, 6601.3628, 30.9240>>    fSpawnHeading 	= 224.4250    BREAK
				CASE 1    vSpawnPoint 	= <<44.8210, 6601.0400, 31.0132>>    fSpawnHeading 	= 224.4250    BREAK
				CASE 2    vSpawnPoint 	= <<46.5257, 6602.7993, 30.9849>>    fSpawnHeading 	= 224.4250    BREAK
				CASE 3    vSpawnPoint 	= <<43.5481, 6600.2964, 31.0727>>    fSpawnHeading 	= 224.4250    BREAK
				CASE 4    vSpawnPoint 	= <<47.7527, 6603.9336, 30.9809>>    fSpawnHeading 	= 224.4250    BREAK
				CASE 5    vSpawnPoint 	= <<47.3175, 6598.8252, 30.7513>>    fSpawnHeading 	= 224.4250    BREAK
				CASE 6    vSpawnPoint 	= <<48.3586, 6601.8442, 30.8203>>    fSpawnHeading 	= 224.4250    BREAK
				CASE 7    vSpawnPoint 	= <<48.7146, 6599.7935, 30.7044>>    fSpawnHeading 	= 224.4250    BREAK
				CASE 8    vSpawnPoint 	= <<45.6111, 6599.0347, 30.8690>>    fSpawnHeading 	= 224.4250    BREAK
				CASE 9    vSpawnPoint 	= <<50.1991, 6601.2539, 30.6802>>    fSpawnHeading 	= 224.4250    BREAK
				CASE 10    vSpawnPoint 	= <<47.8790, 6597.3765, 30.6477>>    fSpawnHeading 	= 224.4250    BREAK
				CASE 11    vSpawnPoint 	= <<46.3619, 6595.6899, 30.6764>>    fSpawnHeading 	= 224.4250    BREAK
				CASE 12    vSpawnPoint 	= <<49.1134, 6598.2495, 30.6044>>    fSpawnHeading 	= 224.4250    BREAK
				CASE 13    vSpawnPoint 	= <<45.8975, 6597.3745, 30.7961>>    fSpawnHeading 	= 224.4250    BREAK
				CASE 14    vSpawnPoint 	= <<50.1862, 6599.7852, 30.6113>>    fSpawnHeading 	= 224.4250    BREAK
				CASE 15    vSpawnPoint 	= <<51.6598, 6600.7744, 30.5944>>    fSpawnHeading 	= 224.4250    BREAK
				DEFAULT RETURN FALSE
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC
#ENDIF

#IF FEATURE_DLC_2_2022
FUNC BOOL GET_FMC_GANG_CONVOY_POST_SCENE_SPAWN_POINT(FREEMODE_DELIVERY_DROPOFFS eDropoffID, INT iSpawnPoint, VECTOR &vSpawnPoint, FLOAT &fSpawnHeading)
	SWITCH eDropoffID
		CASE FMC_DROPOFF_GANG_CONVOY_GARAGE_CYPRESS
			SWITCH iSpawnPoint
				CASE 0    vSpawnPoint 	= <<909.4280, -2192.0598, 29.5118>>    fSpawnHeading 	= 267.8200    BREAK
				CASE 1    vSpawnPoint 	= <<909.7670, -2193.8801, 29.5064>>    fSpawnHeading 	= 263.6063    BREAK
				CASE 2    vSpawnPoint 	= <<910.1407, -2190.3958, 29.5076>>    fSpawnHeading 	= 267.8200    BREAK
				CASE 3    vSpawnPoint 	= <<912.0805, -2194.8306, 29.4972>>    fSpawnHeading 	= 267.8200    BREAK
				CASE 4    vSpawnPoint 	= <<912.3045, -2189.5439, 29.4965>>    fSpawnHeading 	= 267.8200    BREAK
				CASE 5    vSpawnPoint 	= <<910.9002, -2196.3687, 29.5033>>    fSpawnHeading 	= 267.8200    BREAK
				CASE 6    vSpawnPoint 	= <<911.2757, -2187.9470, 29.4948>>    fSpawnHeading 	= 267.8200    BREAK
				CASE 7    vSpawnPoint 	= <<904.0286, -2185.4331, 29.5154>>    fSpawnHeading 	= 266.9822    BREAK
				CASE 8    vSpawnPoint 	= <<913.1080, -2197.1355, 29.4585>>    fSpawnHeading 	= 267.8200    BREAK
				CASE 9    vSpawnPoint 	= <<909.8329, -2184.8320, 29.5089>>    fSpawnHeading 	= 267.8200    BREAK
				CASE 10    vSpawnPoint 	= <<911.6063, -2198.6895, 29.5042>>    fSpawnHeading 	= 267.8200    BREAK
				CASE 11    vSpawnPoint 	= <<907.4467, -2186.0005, 29.5180>>    fSpawnHeading 	= 267.8200    BREAK
				CASE 12    vSpawnPoint 	= <<911.4179, -2201.0618, 29.5050>>    fSpawnHeading 	= 174.6076    BREAK
				CASE 13    vSpawnPoint 	= <<905.6915, -2184.3269, 29.5191>>    fSpawnHeading 	= 267.8200    BREAK
				CASE 14    vSpawnPoint 	= <<912.9086, -2179.6326, 29.4947>>    fSpawnHeading 	= 354.8300    BREAK
				CASE 15    vSpawnPoint 	= <<914.5630, -2181.2671, 29.4930>>    fSpawnHeading 	= 1.7876    BREAK
				CASE 16    vSpawnPoint 	= <<914.7285, -2177.4526, 29.4934>>    fSpawnHeading 	= 0.3716    BREAK
				CASE 17    vSpawnPoint 	= <<912.7906, -2182.9285, 29.4972>>    fSpawnHeading 	= 5.4651    BREAK
				CASE 18    vSpawnPoint 	= <<913.2404, -2175.9260, 29.4947>>    fSpawnHeading 	= 352.4827    BREAK
				CASE 19    vSpawnPoint 	= <<912.8995, -2202.1438, 29.4676>>    fSpawnHeading 	= 153.8028    BREAK
				CASE 20    vSpawnPoint 	= <<915.0444, -2174.0129, 29.4933>>    fSpawnHeading 	= 351.6779    BREAK
				CASE 21    vSpawnPoint 	= <<912.3397, -2209.6160, 29.4941>>    fSpawnHeading 	= 179.8538    BREAK
				CASE 22    vSpawnPoint 	= <<910.7260, -2211.7476, 29.4947>>    fSpawnHeading 	= 172.0186    BREAK
				CASE 23    vSpawnPoint 	= <<911.3255, -2207.5649, 29.4947>>    fSpawnHeading 	= 169.4786    BREAK
				CASE 24    vSpawnPoint 	= <<911.7578, -2214.3384, 29.4947>>    fSpawnHeading 	= 173.1991    BREAK
				CASE 25    vSpawnPoint 	= <<912.7862, -2205.3528, 29.4939>>    fSpawnHeading 	= 166.5493    BREAK
				CASE 26    vSpawnPoint 	= <<910.2437, -2216.1323, 29.4939>>    fSpawnHeading 	= 180.7054    BREAK
				CASE 27    vSpawnPoint 	= <<911.4098, -2203.8206, 29.4996>>    fSpawnHeading 	= 171.5740    BREAK
				CASE 28    vSpawnPoint 	= <<929.9306, -2194.1572, 29.5179>>    fSpawnHeading 	= 92.0054    BREAK
				CASE 29    vSpawnPoint 	= <<931.0103, -2196.3579, 29.5058>>    fSpawnHeading 	= 80.6982    BREAK
				CASE 30    vSpawnPoint 	= <<931.1668, -2191.9167, 29.4985>>    fSpawnHeading 	= 80.3439    BREAK
				CASE 31    vSpawnPoint 	= <<929.3533, -2197.4729, 29.5186>>    fSpawnHeading 	= 80.8081    BREAK
				CASE 32    vSpawnPoint 	= <<929.8067, -2190.1921, 29.4715>>    fSpawnHeading 	= 88.6860    BREAK
				CASE 33    vSpawnPoint 	= <<930.5162, -2199.3398, 29.5036>>    fSpawnHeading 	= 93.5643    BREAK
				CASE 34    vSpawnPoint 	= <<931.7136, -2188.3035, 29.4999>>    fSpawnHeading 	= 88.4550    BREAK
				CASE 35    vSpawnPoint 	= <<913.3904, -2171.8662, 29.4947>>    fSpawnHeading 	= 347.3104    BREAK
				CASE 36    vSpawnPoint 	= <<931.1605, -2175.1384, 29.4092>>    fSpawnHeading 	= 351.9238    BREAK
				CASE 37    vSpawnPoint 	= <<933.1494, -2173.5339, 29.4736>>    fSpawnHeading 	= 356.7183    BREAK
				CASE 38    vSpawnPoint 	= <<932.1880, -2186.2788, 29.4933>>    fSpawnHeading 	= 87.4690    BREAK
				CASE 39    vSpawnPoint 	= <<914.7026, -2169.0828, 29.4947>>    fSpawnHeading 	= 357.0593    BREAK
				DEFAULT RETURN FALSE
			ENDSWITCH
		BREAK
		CASE FMC_DROPOFF_GANG_CONVOY_GARAGE_SAN_CHIANSKI
			SWITCH iSpawnPoint
				CASE 0    vSpawnPoint 	= <<2703.5393, 3456.9868, 54.5640>>    fSpawnHeading 	= 156.2400    BREAK
				CASE 1    vSpawnPoint 	= <<2702.0845, 3457.3367, 54.5735>>    fSpawnHeading 	= 156.2400    BREAK
				CASE 2    vSpawnPoint 	= <<2704.8994, 3456.2974, 54.5686>>    fSpawnHeading 	= 156.2400    BREAK
				CASE 3    vSpawnPoint 	= <<2699.7959, 3455.5620, 54.6744>>    fSpawnHeading 	= 128.7000    BREAK
				CASE 4    vSpawnPoint 	= <<2705.1111, 3453.8491, 54.6575>>    fSpawnHeading 	= 156.2400    BREAK
				CASE 5    vSpawnPoint 	= <<2701.0361, 3452.9744, 54.7520>>    fSpawnHeading 	= 158.1473    BREAK
				CASE 6    vSpawnPoint 	= <<2708.7649, 3451.9734, 54.5662>>    fSpawnHeading 	= 161.5857    BREAK
				CASE 7    vSpawnPoint 	= <<2703.2842, 3451.6731, 54.7672>>    fSpawnHeading 	= 156.2400    BREAK
				CASE 8    vSpawnPoint 	= <<2700.8342, 3449.3982, 54.7985>>    fSpawnHeading 	= 156.2400    BREAK
				CASE 9    vSpawnPoint 	= <<2705.3447, 3450.8943, 54.7653>>    fSpawnHeading 	= 159.4415    BREAK
				CASE 10    vSpawnPoint 	= <<2698.6001, 3452.7717, 54.7958>>    fSpawnHeading 	= 106.0627    BREAK
				CASE 11    vSpawnPoint 	= <<2707.6306, 3449.4233, 54.6835>>    fSpawnHeading 	= 162.9370    BREAK
				CASE 12    vSpawnPoint 	= <<2703.7083, 3448.5420, 54.8006>>    fSpawnHeading 	= 178.3187    BREAK
				CASE 13    vSpawnPoint 	= <<2706.9102, 3446.8809, 54.7077>>    fSpawnHeading 	= 156.2400    BREAK
				CASE 14    vSpawnPoint 	= <<2706.9089, 3444.2080, 54.7346>>    fSpawnHeading 	= 203.2746    BREAK
				CASE 15    vSpawnPoint 	= <<2702.3345, 3445.5127, 54.8007>>    fSpawnHeading 	= 169.5984    BREAK
				CASE 16    vSpawnPoint 	= <<2699.8164, 3446.4346, 54.7986>>    fSpawnHeading 	= 156.2400    BREAK
				CASE 17    vSpawnPoint 	= <<2698.1699, 3449.7852, 54.7966>>    fSpawnHeading 	= 140.1354    BREAK
				CASE 18    vSpawnPoint 	= <<2696.4827, 3448.7161, 54.7958>>    fSpawnHeading 	= 118.2178    BREAK
				CASE 19    vSpawnPoint 	= <<2696.2766, 3451.5649, 54.7947>>    fSpawnHeading 	= 99.1736    BREAK
				CASE 20    vSpawnPoint 	= <<2693.5044, 3450.4646, 54.7932>>    fSpawnHeading 	= 105.0682    BREAK
				CASE 21    vSpawnPoint 	= <<2693.4397, 3453.7263, 54.7915>>    fSpawnHeading 	= 96.3867    BREAK
				CASE 22    vSpawnPoint 	= <<2696.8745, 3457.5068, 55.2291>>    fSpawnHeading 	= 62.7118    BREAK
				CASE 23    vSpawnPoint 	= <<2690.3574, 3451.4714, 54.7866>>    fSpawnHeading 	= 62.9293    BREAK
				CASE 24    vSpawnPoint 	= <<2694.6082, 3458.4988, 55.2291>>    fSpawnHeading 	= 64.8459    BREAK
				CASE 25    vSpawnPoint 	= <<2689.9741, 3454.8579, 54.7823>>    fSpawnHeading 	= 65.9823    BREAK
				CASE 26    vSpawnPoint 	= <<2691.8577, 3459.4468, 55.2291>>    fSpawnHeading 	= 65.8829    BREAK
				CASE 27    vSpawnPoint 	= <<2686.0986, 3452.9827, 54.7744>>    fSpawnHeading 	= 64.2022    BREAK
				CASE 28    vSpawnPoint 	= <<2689.2063, 3460.3872, 55.2291>>    fSpawnHeading 	= 63.5091    BREAK
				CASE 29    vSpawnPoint 	= <<2686.2161, 3457.3364, 54.7686>>    fSpawnHeading 	= 61.7771    BREAK
				CASE 30    vSpawnPoint 	= <<2686.5327, 3461.3999, 55.2291>>    fSpawnHeading 	= 66.4147    BREAK
				CASE 31    vSpawnPoint 	= <<2682.4985, 3454.6406, 54.7606>>    fSpawnHeading 	= 67.2618    BREAK
				CASE 32    vSpawnPoint 	= <<2684.4688, 3462.7625, 55.2291>>    fSpawnHeading 	= 75.4005    BREAK
				CASE 33    vSpawnPoint 	= <<2681.9973, 3459.5610, 54.7505>>    fSpawnHeading 	= 61.2934    BREAK
				CASE 34    vSpawnPoint 	= <<2682.1235, 3464.0950, 55.2291>>    fSpawnHeading 	= 68.2975    BREAK
				CASE 35    vSpawnPoint 	= <<2678.7649, 3459.1741, 54.7369>>    fSpawnHeading 	= 68.6372    BREAK
				CASE 36    vSpawnPoint 	= <<2711.1650, 3444.5969, 54.7047>>    fSpawnHeading 	= 242.2665    BREAK
				CASE 37    vSpawnPoint 	= <<2715.0332, 3443.1130, 54.7687>>    fSpawnHeading 	= 244.1313    BREAK
				CASE 38    vSpawnPoint 	= <<2718.9302, 3441.1746, 54.9153>>    fSpawnHeading 	= 244.2298    BREAK
				CASE 39    vSpawnPoint 	= <<2722.1882, 3439.0776, 55.0607>>    fSpawnHeading 	= 243.3840    BREAK
				DEFAULT RETURN FALSE
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC
#ENDIF

FUNC BOOL FMC_GET_SPAWN_POINT_FOR_DROPOFF(FREEMODE_DELIVERY_DROPOFFS eDropoffID, INT iScene, INT iSpawnPoint, VECTOR &vSpawnPoint, FLOAT &fSpawnHeading, DELIVERY_TYPE deliveryType)
	UNUSED_PARAMETER(iScene)

	SWITCH eDropoffID
		CASE FMC_DROPOFF_SOLOMONS_OFFICE	 		RETURN GET_SOLOMON_OFFICE_POST_SCENE_SPAWN_POINT(iSpawnPoint, vSpawnPoint, fSpawnHeading)					BREAK
		CASE FMC_DROPOFF_OMEGA 						RETURN GET_OMEGA_POST_SCENE_SPAWN_POINT(iSpawnPoint, vSpawnPoint, fSpawnHeading)							BREAK
		CASE FMC_DROPOFF_PALETO_SHERRIFF	 		RETURN GET_PALETO_SHERRIFF_POST_SCENE_SPAWN_POINT(iSpawnPoint, vSpawnPoint, fSpawnHeading)					BREAK
		CASE FMC_DROPOFF_VESPUCCI_POLICE			RETURN GET_VESPUCCI_POLICE_POST_SCENE_SPAWN_POINT(iSpawnPoint, vSpawnPoint, fSpawnHeading)					BREAK
		CASE FMC_DROPOFF_SANDY_SHORES_SHERRIFF		RETURN GET_SANDY_SHORES_SHERRIFF_POST_SCENE_SPAWN_POINT(iSpawnPoint, vSpawnPoint, fSpawnHeading)			BREAK
		CASE FMC_DROPOFF_MISSION_ROW_POLICE			RETURN GET_MISSION_ROW_POLICE_POST_SCENE_SPAWN_POINT(iSpawnPoint, vSpawnPoint, fSpawnHeading)				BREAK
		CASE FMC_DROPOFF_GREAT_CHAPARRL				RETURN GET_GREAT_CHAPARRL_POST_SCENE_SPAWN_POINT(iSpawnPoint, vSpawnPoint, fSpawnHeading)					BREAK
		CASE FMC_DROPOFF_LA_MESA					RETURN GET_LA_MESA_POST_SCENE_SPAWN_POINT(iSpawnPoint, vSpawnPoint, fSpawnHeading)							BREAK
		CASE FMC_DROPOFF_STRAWBERRY					RETURN GET_STRAWBERRY_POST_SCENE_SPAWN_POINT(iSpawnPoint, vSpawnPoint, fSpawnHeading)						BREAK
		CASE FMC_DROPOFF_PALETO_COVE				RETURN GET_PALETO_COVE_DOCK_POST_SCENE_SPAWN_POINT(iSpawnPoint, vSpawnPoint, fSpawnHeading)					BREAK
		CASE FMC_DROPOFF_ELYSIAN_ISLAND				RETURN GET_ELYSIAN_ISLAND_SHIPYARD_POST_SCENE_SPAWN_POINT(iSpawnPoint, vSpawnPoint, fSpawnHeading)			BREAK
		CASE FMC_SANDY_SHORES_AIRFIELD				RETURN GET_SANDY_AIRFIELD_POST_DELIVERY_SCENE_SPAWN_POINT(iSpawnPoint, vSpawnPoint, fSpawnHeading)			BREAK
		CASE FMC_DROPOFF_WHOUSE_DEFENCES_ELYSIAN 	RETURN GET_ELYSIAN_ISLAND_WHOUSE_DEFENCES_POST_SCENE_SPAWN_POINT(iSpawnPoint, vSpawnPoint, fSpawnHeading)	BREAK
		CASE FMC_DROPOFF_DOCKS						RETURN GET_FMC_DOCKS_POST_SCENE_SPAWN_POINT(iSpawnPoint, vSpawnPoint, fSpawnHeading)						BREAK
		CASE FMC_DROPOFF_METH_TANKER_HARMONY		RETURN GET_FMC_HARMONY_POST_SCENE_SPAWN_POINT(iSpawnPoint, vSpawnPoint, fSpawnHeading)						BREAK
		CASE FMC_DROPOFF_MORNINGWOOD_LOCKUP			RETURN FMC_GET_SPAWN_POINT_FOR_MORNINGWOOD_LOCKUP(iSpawnPoint, vSpawnPoint, fSpawnHeading)					BREAK
		CASE FMC_DROPOFF_FRANKLINS_HOUSE			RETURN FMC_GET_SPAWN_POINT_FOR_FRANKLINS_HOUSE(iSpawnPoint, vSpawnPoint, fSpawnHeading)						BREAK
		#IF FEATURE_DLC_2_2022
		CASE FMC_DROPOFF_TREVOR_METH_LAB			RETURN GET_FMC_TREVOR_METH_LAB_POST_SCENE_SPAWN_POINT(iSpawnPoint, vSpawnPoint, fSpawnHeading)				BREAK
		#ENDIF
		CASE FMC_DROPOFF_PLYR_AUTO_SHOP_LA_MESA
		CASE FMC_DROPOFF_PLYR_AUTO_SHOP_STRAWBERRY
		CASE FMC_DROPOFF_PLYR_AUTO_SHOP_BURTON
		CASE FMC_DROPOFF_PLYR_AUTO_SHOP_RANCHO
		CASE FMC_DROPOFF_PLYR_AUTO_SHOP_MISSION_ROW
			RETURN GET_AUTO_SHOP_EXTERIOR_SPAWN_POINT_ON_FOOT(FMC_GET_DROPOFF_PROPERTY_ID(eDropoffID), iSpawnPoint, vSpawnPoint, fSpawnHeading)		BREAK
		CASE FMC_DROPOFF_AUTO_SHOP_CUSTOMER_H1
		CASE FMC_DROPOFF_AUTO_SHOP_CUSTOMER_H2
		CASE FMC_DROPOFF_AUTO_SHOP_CUSTOMER_H3
		CASE FMC_DROPOFF_AUTO_SHOP_CUSTOMER_H4
		CASE FMC_DROPOFF_AUTO_SHOP_CUSTOMER_H5
		CASE FMC_DROPOFF_AUTO_SHOP_CUSTOMER_H6
		CASE FMC_DROPOFF_AUTO_SHOP_CUSTOMER_H7
		CASE FMC_DROPOFF_AUTO_SHOP_CUSTOMER_H8
		CASE FMC_DROPOFF_AUTO_SHOP_CUSTOMER_L1
		CASE FMC_DROPOFF_AUTO_SHOP_CUSTOMER_L2
		CASE FMC_DROPOFF_AUTO_SHOP_CUSTOMER_L3
		CASE FMC_DROPOFF_AUTO_SHOP_CUSTOMER_L4
		CASE FMC_DROPOFF_AUTO_SHOP_CUSTOMER_L5
		CASE FMC_DROPOFF_AUTO_SHOP_CUSTOMER_L6
		CASE FMC_DROPOFF_AUTO_SHOP_CUSTOMER_L7
		CASE FMC_DROPOFF_AUTO_SHOP_CUSTOMER_L8
			RETURN GET_FMC_AUTO_SHOP_CUSTOMER_POST_SCENE_SPAWN_POINT(eDropoffID, iSpawnPoint, vSpawnPoint, fSpawnHeading)							BREAK
		BREAK
		CASE FMC_DROPOFF_DJ_LSIA
		CASE FMC_DROPOFF_LSIA_SEA
			RETURN GET_LSIA_GENERAL_DELIVERY_SCENE_SPAWN_COORD(iSpawnPoint, vSpawnPoint, fSpawnHeading)
		BREAK
		CASE FMC_DROPOFF_DJ_MUSIC_LOCKER
			IF deliveryType = DT_VEHICLE
				RETURN GET_CASINO_GARAGE_DOOR_EXTERIOR_SPAWN_POINT_ON_FOOT(SIMPLE_INTERIOR_CASINO, iSpawnPoint, vSpawnPoint, fSpawnHeading)			BREAK
			ELSE
				RETURN GET_CASINO_NIGHTCLUB_DOOR_SPAWN_POINT_ON_FOOT(SIMPLE_INTERIOR_CASINO_NIGHTCLUB, iSpawnPoint, vSpawnPoint, fSpawnHeading)		BREAK
			ENDIF
		BREAK
		#IF FEATURE_DLC_2_2022
		CASE FMC_DROPOFF_GANG_CONVOY_GARAGE_CYPRESS
		CASE FMC_DROPOFF_GANG_CONVOY_GARAGE_SAN_CHIANSKI
			RETURN GET_FMC_GANG_CONVOY_POST_SCENE_SPAWN_POINT(eDropoffID, iSpawnPoint, vSpawnPoint, fSpawnHeading)	
		BREAK
		#ENDIF
		#IF FEATURE_DLC_1_2022		
		CASE FMC_DROPOFF_LS_MEDICAL_CENTER
		CASE FMC_DROPOFF_ECLIPSE_MEDICAL_TOWER
		CASE FMC_DROPOFF_ZONAH_MEDICAL_CENTER
		CASE FMC_DROPOFF_PILLBOX_MEDICAL_CENTER
		CASE FMC_DROPOFF_SANDY_SH_MEDICAL_CENTER
		CASE FMC_DROPOFF_ST_FIACRE_HOSPITAL
		CASE FMC_DROPOFF_PARSONS_REHAB_CENTER
		CASE FMC_DROPOFF_VINEWOOD_VIP_HOUSE
		CASE FMC_DROPOFF_BANHAM_CANYON_VIP_HOUSE
		CASE FMC_DROPOFF_MIRROR_PARK_VIP_HOUSE
		CASE FMC_DROPOFF_LAKE_VINEWOOD_VIP_HOUSE
		CASE FMC_DROPOFF_RICHMAN_VIP_HOUSE
			RETURN GET_FMC_PASSED_OUT_VIP_POST_SCENE_SPAWN_POINT(eDropoffID, iSpawnPoint, vSpawnPoint, fSpawnHeading)
		BREAK
		CASE FMC_DROPOFF_AMMU_NATION_PALETO_BAY
		CASE FMC_DROPOFF_AMMU_NATION_SANDY_SHORES
		CASE FMC_DROPOFF_AMMU_NATION_ROUTE_68
		CASE FMC_DROPOFF_AMMU_NATION_CHUMASH
		CASE FMC_DROPOFF_AMMU_NATION_TATAVIUM
		CASE FMC_DROPOFF_AMMU_NATION_HAWICK
		CASE FMC_DROPOFF_AMMU_NATION_MORNINGWOOD
		CASE FMC_DROPOFF_AMMU_NATION_STRAWBERRY
		CASE FMC_DROPOFF_AMMU_NATION_LA_MESA
		CASE FMC_DROPOFF_AMMU_NATION_CYPRESS
			RETURN GET_FMC_AMMU_NATION_POST_SCENE_SPAWN_POINT(eDropoffID, iSpawnPoint, vSpawnPoint, fSpawnHeading)
		BREAK
		CASE FMC_DROPOFF_BIKE_SHOP_CUSTOMER_1
		CASE FMC_DROPOFF_BIKE_SHOP_CUSTOMER_2
		CASE FMC_DROPOFF_BIKE_SHOP_CUSTOMER_3
		CASE FMC_DROPOFF_BIKE_SHOP_CUSTOMER_4
		CASE FMC_DROPOFF_BIKE_SHOP_CUSTOMER_5
		CASE FMC_DROPOFF_BIKE_SHOP_CUSTOMER_6
		CASE FMC_DROPOFF_BIKE_SHOP_CUSTOMER_7
		CASE FMC_DROPOFF_BIKE_SHOP_CUSTOMER_8
		CASE FMC_DROPOFF_BIKE_SHOP_CUSTOMER_9
		CASE FMC_DROPOFF_BIKE_SHOP_CUSTOMER_10
		CASE FMC_DROPOFF_BIKE_SHOP_CUSTOMER_11
		CASE FMC_DROPOFF_BIKE_SHOP_CUSTOMER_12
		CASE FMC_DROPOFF_BIKE_SHOP_CUSTOMER_13
		CASE FMC_DROPOFF_BIKE_SHOP_CUSTOMER_14
		CASE FMC_DROPOFF_BIKE_SHOP_CUSTOMER_15
		CASE FMC_DROPOFF_BIKE_SHOP_CUSTOMER_16
		CASE FMC_DROPOFF_BIKE_SHOP_CUSTOMER_17
		CASE FMC_DROPOFF_BIKE_SHOP_CUSTOMER_18
			RETURN GET_FMC_CUSTOM_BIKE_DELIVERY_POST_SCENE_SPAWN_POINT(eDropoffID, iSpawnPoint, vSpawnPoint, fSpawnHeading)
		BREAK
		#ENDIF
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC CUTSCENE_FMC_SPAWN_POINT_SETUP(DELIVERY_DATA &deliveryData)
	CDEBUG2LN(DEBUG_DELIVERY, "CUTSCENE_FMC_SPAWN_POINT_SETUP")
	
	VECTOR vSpawnPoint
	FLOAT fSpawnHeading
	FLOAT fweight = 1.0
	INT i
	
	WHILE FMC_GET_SPAWN_POINT_FOR_DROPOFF(deliveryData.eDropoffID, deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_SCENE_VARIATION], i, vSpawnPoint, fSpawnHeading, deliveryData.deliveryType)
		ADD_CUSTOM_SPAWN_POINT(vSpawnPoint, fSpawnHeading, fweight)
		fweight -= 0.025
		i++
	ENDWHILE
ENDPROC
