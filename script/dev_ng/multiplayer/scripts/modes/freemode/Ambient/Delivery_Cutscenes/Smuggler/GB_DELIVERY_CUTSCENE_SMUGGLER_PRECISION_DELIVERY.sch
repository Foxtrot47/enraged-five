USING "globals.sch"
USING "net_simple_cutscene.sch"
USING "GB_DELIVERY_DEFINITIONS.sch"
USING "GB_DELIVERY_CHILD_SUPPORT.sch"

USING "gb_smuggler_coords.sch"
USING "GB_SMUGGLER_DROPOFFS.sch"
USING "Cutscene_help.sch"

//VEHICLE EXTRA
CONST_INT SMG_PD_DELIVERY_VEHICLE_EXTRA_FLATBED 	0
//VEHICLE
CONST_INT SMG_PD_DELIVERY_VEHICLE_CARGOBOB			0
//OBJECTS
CONST_INT SMG_PD_DELIVERY_CAMERA_PLACE_HOLDER		0

//VECTOR
CONST_INT SMG_PD_DELIVERY_VECTOR_CARGOBOB_START		0
CONST_INT SMG_PD_DELIVERY_VECTOR_CARGOBOB_FINISH	1
//FLOAT
CONST_INT SMG_PD_DELIVERY_FLOAT_CARGOBOB_LOWER_BIAS 0
//BITSET
CONST_INT SMG_PD_DELIVERY_BITSET_TASK_HELI_AWAY				0
CONST_INT SMG_PD_DELIVERY_BITSET_MOVE_CAM					1
CONST_INT SMG_PD_DELIVERY_BITSET_TASK_FLATBED_DRIVE_AWAY 	2
CONST_INT SMG_PD_DELIVERY_BITSET_DONE_CARGOBOB_REPOSITION	3
CONST_INT SMG_PD_DELIVERY_BITSET_IMPACT_SOUND_PLAYED		4
//EXTRA PED
CONST_INT SMG_PD_DELIVERY_PED_FLATBED_DRIVER	0
//INT 
CONST_INT SMG_PD_DELIVERY_INTEGER_BITSET	0
CONST_INT SMG_PD_DELIVERY_INTEGER_SOUND_ID	1

FUNC BOOL CUTSCENE_SMUGGLER_PRECISION_DELIVERY_ASSETS_CREATE(DELIVERY_DATA &deliveryData)
	
	//FLATBED
	IF NOT DOES_ENTITY_EXIST(deliveryData.cutsceneData.vehicleArray[SMG_PD_DELIVERY_VEHICLE_EXTRA_FLATBED])
		deliveryData.cutsceneData.vehicleArray[SMG_PD_DELIVERY_VEHICLE_EXTRA_FLATBED] = CREATE_VEHICLE(FLATBED, <<10.0, 10.0, 10.0>>, 0.0, FALSE, FALSE)
	ENDIF
	
	//Make sure cargobob and cargo exists.
	IF NOT DOES_ENTITY_EXIST(deliveryData.cutsceneData.vehiclePackArray[SMG_PD_DELIVERY_VEHICLE_CARGOBOB].vehicle)
		ASSERTLN("CUTSCENE_SMUGGLER_PRECISION_DELIVERY_ASSETS_CREATE - cargobob does nto exist")
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(deliveryData.deliveryVehicleObjectAttached)
		ASSERTLN("CUTSCENE_SMUGGLER_PRECISION_DELIVERY_ASSETS_CREATE - container does nto exist")
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(deliveryData.cutsceneData.pedArray[SMG_PD_DELIVERY_PED_FLATBED_DRIVER])
		deliveryData.cutsceneData.pedArray[SMG_PD_DELIVERY_PED_FLATBED_DRIVER] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_DEALER_01, <<10.0, 10.0, 10.0>>, 0.0, FALSE)
		IF DOES_ENTITY_EXIST(deliveryData.cutsceneData.pedArray[SMG_PD_DELIVERY_PED_FLATBED_DRIVER])
		AND NOT IS_ENTITY_DEAD(deliveryData.cutsceneData.pedArray[SMG_PD_DELIVERY_PED_FLATBED_DRIVER])
			SET_PED_AS_ENEMY(deliveryData.cutsceneData.pedArray[SMG_PD_DELIVERY_PED_FLATBED_DRIVER], FALSE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(deliveryData.cutsceneData.pedArray[SMG_PD_DELIVERY_PED_FLATBED_DRIVER], TRUE)
			//Ped variations
			SET_PED_COMPONENT_VARIATION(deliveryData.cutsceneData.pedArray[SMG_PD_DELIVERY_PED_FLATBED_DRIVER], PED_COMP_HEAD, 1, 0)
			SET_PED_COMPONENT_VARIATION(deliveryData.cutsceneData.pedArray[SMG_PD_DELIVERY_PED_FLATBED_DRIVER], PED_COMP_TORSO, 0, 2)
			SET_PED_COMPONENT_VARIATION(deliveryData.cutsceneData.pedArray[SMG_PD_DELIVERY_PED_FLATBED_DRIVER], PED_COMP_LEG, 0, 1)
		ENDIF
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(deliveryData.cutsceneData.objectArray[SMG_PD_DELIVERY_CAMERA_PLACE_HOLDER])
		deliveryData.cutsceneData.objectArray[SMG_PD_DELIVERY_CAMERA_PLACE_HOLDER] = CREATE_OBJECT_NO_OFFSET(prop_golf_ball, <<0.0, 10.0, 10.0>>, FALSE)
		IF DOES_ENTITY_EXIST(deliveryData.cutsceneData.objectArray[SMG_PD_DELIVERY_CAMERA_PLACE_HOLDER])
		AND NOT IS_ENTITY_DEAD(deliveryData.cutsceneData.objectArray[SMG_PD_DELIVERY_CAMERA_PLACE_HOLDER])
			SET_ENTITY_VISIBLE(deliveryData.cutsceneData.objectArray[SMG_PD_DELIVERY_CAMERA_PLACE_HOLDER], FALSE)
			SET_ENTITY_INVINCIBLE(deliveryData.cutsceneData.objectArray[SMG_PD_DELIVERY_CAMERA_PLACE_HOLDER], TRUE)
			SET_ENTITY_COLLISION(deliveryData.cutsceneData.objectArray[SMG_PD_DELIVERY_CAMERA_PLACE_HOLDER], FALSE)
			FREEZE_ENTITY_POSITION(deliveryData.cutsceneData.objectArray[SMG_PD_DELIVERY_CAMERA_PLACE_HOLDER], TRUE)
		ENDIF
	ENDIF
	
	IF (DOES_ENTITY_EXIST(deliveryData.cutsceneData.vehicleArray[SMG_PD_DELIVERY_VEHICLE_EXTRA_FLATBED])
	AND NOT IS_ENTITY_DEAD(deliveryData.cutsceneData.vehicleArray[SMG_PD_DELIVERY_VEHICLE_EXTRA_FLATBED]))
	AND (DOES_ENTITY_EXIST(deliveryData.cutsceneData.objectArray[SMG_PD_DELIVERY_CAMERA_PLACE_HOLDER])
	AND NOT IS_ENTITY_DEAD(deliveryData.cutsceneData.objectArray[SMG_PD_DELIVERY_CAMERA_PLACE_HOLDER]))	
		IF NOT IS_BIT_SET(deliveryData.cutsceneData.intArray[SMG_PD_DELIVERY_INTEGER_BITSET], SMG_PD_DELIVERY_BITSET_MOVE_CAM)
				
			VECTOR vSceneRotation, vScenePosition
			vScenePosition = <<135.284, 4447.876, 80.214>>
			//vSceneRotation = <<0.0, 0.0, -130.0>>
			vSceneRotation = <<-6.202, -0.754, -131.545>>
			
			VECTOR vObjectRotation, vObjectPosition
			vObjectPosition = <<136.8627, 4445.8608, 81.7023>>
			vObjectRotation = <<-0.8312, -0.0120, 22.2966>>
			
			SET_ENTITY_COORDS_NO_OFFSET(deliveryData.cutsceneData.vehicleArray[SMG_PD_DELIVERY_VEHICLE_EXTRA_FLATBED], vScenePosition)
			SET_ENTITY_HEADING(deliveryData.cutsceneData.vehicleArray[SMG_PD_DELIVERY_VEHICLE_EXTRA_FLATBED], vSceneRotation.z)
			FREEZE_ENTITY_POSITION(deliveryData.cutsceneData.vehicleArray[SMG_PD_DELIVERY_VEHICLE_EXTRA_FLATBED], TRUE)
			//flatbed variations
			SET_VEHICLE_COLOURS(deliveryData.cutsceneData.vehicleArray[SMG_PD_DELIVERY_VEHICLE_EXTRA_FLATBED], 45, 40)
			SET_VEHICLE_EXTRA_COLOURS(deliveryData.cutsceneData.vehicleArray[SMG_PD_DELIVERY_VEHICLE_EXTRA_FLATBED], 35, 156)
			SET_VEHICLE_DIRT_LEVEL(deliveryData.cutsceneData.vehicleArray[SMG_PD_DELIVERY_VEHICLE_EXTRA_FLATBED], 0.0)

			SET_ENTITY_COORDS_NO_OFFSET(deliveryData.cutsceneData.objectArray[SMG_PD_DELIVERY_CAMERA_PLACE_HOLDER], vObjectPosition)
			SET_ENTITY_ROTATION(deliveryData.cutsceneData.objectArray[SMG_PD_DELIVERY_CAMERA_PLACE_HOLDER], vObjectRotation)
			FREEZE_ENTITY_POSITION(deliveryData.cutsceneData.objectArray[SMG_PD_DELIVERY_CAMERA_PLACE_HOLDER], TRUE)
			
			CDEBUG1LN(DEBUG_DELIVERY, "CUTSCENE_SMUGGLER_PRECISION_DELIVERY_ASSETS_CREATE - POSITION")
			SET_BIT(deliveryData.cutsceneData.intArray[SMG_PD_DELIVERY_INTEGER_BITSET], SMG_PD_DELIVERY_BITSET_MOVE_CAM)
			
			RETURN FALSE
		ELSE
			CDEBUG1LN(DEBUG_DELIVERY, "CUTSCENE_SMUGGLER_PRECISION_DELIVERY_ASSETS_CREATE - ATTACH")
				VECTOR vSceneRotation, vScenePosition
				vScenePosition = GET_ENTITY_COORDS(deliveryData.cutsceneData.vehicleArray[SMG_PD_DELIVERY_VEHICLE_EXTRA_FLATBED])
				vSceneRotation = GET_ENTITY_ROTATION(deliveryData.cutsceneData.vehicleArray[SMG_PD_DELIVERY_VEHICLE_EXTRA_FLATBED])
				
				VECTOR vObjectRotation, vObjectPosition
				vObjectPosition = GET_ENTITY_COORDS(deliveryData.cutsceneData.objectArray[SMG_PD_DELIVERY_CAMERA_PLACE_HOLDER])
				vObjectRotation = GET_ENTITY_ROTATION(deliveryData.cutsceneData.objectArray[SMG_PD_DELIVERY_CAMERA_PLACE_HOLDER])
			
			VECTOR vOffsetRot, vOffsetPos
			vOffsetPos = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(deliveryData.cutsceneData.vehicleArray[SMG_PD_DELIVERY_VEHICLE_EXTRA_FLATBED], vObjectPosition)
			vOffsetRot.x = GET_ANGULAR_DIFFERENCE(vSceneRotation.x, vObjectRotation.x)
			vOffsetRot.y = GET_ANGULAR_DIFFERENCE(vSceneRotation.y, vObjectRotation.y)
			vOffsetRot.z = GET_ANGULAR_DIFFERENCE(vSceneRotation.z, vObjectRotation.z)
				
			deliveryData.cutsceneData.vectorArray[0] = vOffsetPos
			deliveryData.cutsceneData.vectorArray[1] = vOffsetRot
			
			CDEBUG1LN(DEBUG_DELIVERY, "CUTSCENE_SMUGGLER_PRECISION_DELIVERY_ASSETS_CREATE - vObjectPosition: ", vObjectPosition, " Scene: ", vScenePosition)
			CDEBUG1LN(DEBUG_DELIVERY, "CUTSCENE_SMUGGLER_PRECISION_DELIVERY_ASSETS_CREATE - vOffsetPos: ", vOffsetPos, " vOffsetRot: ", vOffsetRot)
		ENDIF
	ENDIF
	
	SET_VEHICLE_LOD_MULTIPLIER(deliveryData.cutsceneData.vehicleArray[SMG_PD_DELIVERY_VEHICLE_EXTRA_FLATBED], 2.0)
	SET_ENTITY_LOD_DIST(deliveryData.cutsceneData.vehicleArray[SMG_PD_DELIVERY_VEHICLE_EXTRA_FLATBED], 2000)
	
	RETURN TRUE
ENDFUNC

FUNC BOOL CUTSCENE_SMUGGLER_PRECISION_DELIVERY_CREATE_SCENE(DELIVERY_DATA &deliveryData)
	VECTOR vCenter
	vCenter = FREEMODE_DELIVERY_DROPOFF_GET_IN_CAR_COORDS(deliveryData.eDropoffID)
	CDEBUG1LN(DEBUG_DELIVERY, "CUTSCENE_SMUGGLER_PRECISION_DELIVERY_CREATE_SCENE - vCenter: ", vCenter)
	
	//VECTOR vCamCoord
	//vCamCoord = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(deliveryData.cutsceneData.vehiclePackArray[SMG_PD_DELIVERY_VEHICLE_CARGOBOB].vehicle, <<-4.969, 10.660, 2.495>>)
	//VECTOR vCamRotation
	
	
	
	//SIMPLE_CUTSCENE_ADD_SCENE(deliveryData.cutsceneData.cutscene, 5000, "Scene", <<1438.5291, 4481.5063, 51.3310>>, <<16.1140, -10.3717, -174.5028>>, 44.5980, <<1438.5291, 4481.5063, 51.3310>>, <<16.1140, -10.3717, -174.5028>>, 44.5980, 1.0, 50, 0, GRAPH_TYPE_LINEAR)
	//SIMPLE_CUTSCENE_ADD_SCENE(deliveryData.cutsceneData.cutscene, 3000, "Scene", <<1434.2938, 4468.4077, 74.1007>>, <<-76.6351, -10.3717, -79.7143>>, 44.5980, <<1434.2938, 4468.4077, 74.1007>>, <<-76.6351, -10.3717, -79.7143>>, 44.5980, 1.0, 50, 0, GRAPH_TYPE_LINEAR)
	//SIMPLE_CUTSCENE_TRANSFORM(<<1440.952, 4470.092, 49.624>>, <<0.0, 0.0, -24.34>>, deliveryData.cutsceneData.cutscene, vFlatBedCoords, <<0.0, 0.0, fFlatBedHeading>>)
	
	VECTOR vFlatBedCoords
	vFlatBedCoords = GET_NON_SMUGGLER_VEHICLE_SPAWN_COORDS(SMUGGLER_GET_MISSION_VARIATION_FROM_DROPOFF(deliveryData.eDropoffID), SmugglerLocation_BeaconGrab_Location1, 0, deliveryData.eDropoffID)
	
	FLOAT fFlatBedHeading
	fFlatBedHeading = GET_NON_SMUGGLER_VEHICLE_SPAWN_HEADING(SMUGGLER_GET_MISSION_VARIATION_FROM_DROPOFF(deliveryData.eDropoffID), SmugglerLocation_BeaconGrab_Location1, 0, deliveryData.eDropoffID)
			
	
	SIMPLE_CUTSCENE_ADD_SCENE(deliveryData.cutsceneData.cutscene, 4000, "Scene", <<2182.6028, 3401.7007, 47.2852>>, <<-3.7169, -1.1441, 177.3787>>, 52.2750, <<2182.6028, 3401.7007, 47.2852>>, <<-3.7169, -1.1441, 177.3787>>, 52.2750, 1.0, 50, 0, GRAPH_TYPE_LINEAR)
	//SIMPLE_CUTSCENE_ADD_SCENE(deliveryData.cutsceneData.cutscene, 5000, "Scene", <<2188.9829, 3401.0339, 80.8119>>, <<-79.1713, -1.1441, 129.1281>>, 31.7713, <<2189.3989, 3401.3708, 83.6054>>, <<-79.1713, -1.1441, 129.1281>>, 31.7713, 1.0, 50, 0, GRAPH_TYPE_LINEAR)
	SIMPLE_CUTSCENE_TRANSFORM(<<2182.993, 3399.328, 45.608>>, <<0.0, 0.0, 45.6077>>, deliveryData.cutsceneData.cutscene, vFlatBedCoords, <<0.0, 0.0, fFlatBedHeading>>)
	
	
	RETURN TRUE
ENDFUNC

//Will call once to position assets on the start of an x scene
PROC CUTSCENE_SMUGGLER_PRECISION_DELIVERY_ASSET_SCENE_SETUP(DELIVERY_DATA &deliveryData, INT iScene)
	UNUSED_PARAMETER(deliveryData)
	SWITCH iScene
		CASE 0	
			//TEMP
			IF DOES_ENTITY_EXIST(deliveryData.deliveryVehicle)
			AND IS_ENTITY_ALIVE(deliveryData.deliveryVehicle)
				SET_ENTITY_NO_COLLISION_ENTITY(deliveryData.cutsceneData.vehiclePackArray[SMG_PD_DELIVERY_VEHICLE_CARGOBOB].vehicle, deliveryData.deliveryVehicle, FALSE)
				SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
				IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
					SET_ENTITY_VISIBLE_IN_CUTSCENE(deliveryData.deliveryVehicle, TRUE, TRUE)
					SET_ENTITY_INVINCIBLE(deliveryData.deliveryVehicle, TRUE)
					PRINTLN("CUTSCENE_SMUGGLER_PRECISION_DELIVERY_MAINTAIN_SCENE - Moved cargobob to: ")
				ENDIF
			ENDIF
				
			IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
				SET_FOCUS_POS_AND_VEL(GET_CAM_COORD(deliveryData.cutsceneData.cutscene.camera), <<0.0, 0.0, 0.0>>)
				SET_VEHICLE_LOD_MULTIPLIER(deliveryData.cutsceneData.vehicleArray[SMG_PD_DELIVERY_VEHICLE_EXTRA_FLATBED], 2.0)
				SET_ENTITY_LOD_DIST(deliveryData.cutsceneData.vehicleArray[SMG_PD_DELIVERY_VEHICLE_EXTRA_FLATBED], 2000)
			ENDIF

			//position flatbed.
			VECTOR vFlatBedCoords
			vFlatBedCoords = GET_NON_SMUGGLER_VEHICLE_SPAWN_COORDS(SMUGGLER_GET_MISSION_VARIATION_FROM_DROPOFF(deliveryData.eDropoffID), SmugglerLocation_BeaconGrab_Location1, 0, deliveryData.eDropoffID)
			
			FLOAT fFlatBedHeading
			fFlatBedHeading = GET_NON_SMUGGLER_VEHICLE_SPAWN_HEADING(SMUGGLER_GET_MISSION_VARIATION_FROM_DROPOFF(deliveryData.eDropoffID), SmugglerLocation_BeaconGrab_Location1, 0, deliveryData.eDropoffID)
			CDEBUG1LN(DEBUG_DELIVERY, "CUTSCENE_SMUGGLER_PRECISION_DELIVERY_ASSET_SCENE_SETUP - vFlatBedCoords: ", vFlatBedCoords, " fFlatBedHeading: ", fFlatBedHeading)	
			
			IF DOES_ENTITY_EXIST(deliveryData.cutsceneData.pedArray[SMG_PD_DELIVERY_PED_FLATBED_DRIVER])
				IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
					DELIVERY_CHILD_SET_FOCUS(deliveryData, GET_ENTITY_COORDS(deliveryData.cutsceneData.pedArray[SMG_PD_DELIVERY_PED_FLATBED_DRIVER]))
				ENDIF
			ENDIF
			
			//Flatbed
			IF DOES_ENTITY_EXIST(deliveryData.cutsceneData.vehicleArray[SMG_PD_DELIVERY_VEHICLE_EXTRA_FLATBED])
			AND NOT IS_ENTITY_DEAD(deliveryData.cutsceneData.vehicleArray[SMG_PD_DELIVERY_VEHICLE_EXTRA_FLATBED])
				//Play audio from flatbed.
				PLAY_SOUND_FROM_ENTITY(deliveryData.cutsceneData.intArray[SMG_PD_DELIVERY_INTEGER_SOUND_ID], "flatbed_delivery", deliveryData.cutsceneData.vehicleArray[SMG_PD_DELIVERY_VEHICLE_EXTRA_FLATBED], "DLC_SM_Precision_Delivery_Sounds")
				
				CUTSCENE_HELP_PREPARE_VEHICLE_FOR_SCENE(deliveryData.cutsceneData.vehicleArray[SMG_PD_DELIVERY_VEHICLE_EXTRA_FLATBED], TRUE)
				SET_ENTITY_COORDS(deliveryData.cutsceneData.vehicleArray[SMG_PD_DELIVERY_VEHICLE_EXTRA_FLATBED], vFlatBedCoords)
				SET_ENTITY_HEADING(deliveryData.cutsceneData.vehicleArray[SMG_PD_DELIVERY_VEHICLE_EXTRA_FLATBED], fFlatBedHeading)
				FREEZE_ENTITY_POSITION(deliveryData.cutsceneData.vehicleArray[SMG_PD_DELIVERY_VEHICLE_EXTRA_FLATBED], TRUE)
				SET_ENTITY_COLLISION(deliveryData.cutsceneData.vehicleArray[SMG_PD_DELIVERY_VEHICLE_EXTRA_FLATBED], FALSE)
				IF DOES_ENTITY_EXIST(deliveryData.cutsceneData.pedArray[SMG_PD_DELIVERY_PED_FLATBED_DRIVER])
				AND NOT IS_ENTITY_DEAD(deliveryData.cutsceneData.pedArray[SMG_PD_DELIVERY_PED_FLATBED_DRIVER])
					SET_PED_INTO_VEHICLE(deliveryData.cutsceneData.pedArray[SMG_PD_DELIVERY_PED_FLATBED_DRIVER], deliveryData.cutsceneData.vehicleArray[SMG_PD_DELIVERY_VEHICLE_EXTRA_FLATBED])
				ENDIF
			ENDIF
			
			ATTACH_ENTITY_TO_ENTITY(deliveryData.cutsceneData.objectArray[SMG_PD_DELIVERY_CAMERA_PLACE_HOLDER], deliveryData.cutsceneData.vehicleArray[SMG_PD_DELIVERY_VEHICLE_EXTRA_FLATBED], -1, 
			deliveryData.cutsceneData.vectorArray[0], deliveryData.cutsceneData.vectorArray[1])

			//Cargobob
			IF DOES_ENTITY_EXIST(deliveryData.cutsceneData.vehiclePackArray[SMG_PD_DELIVERY_VEHICLE_CARGOBOB].vehicle)
			AND NOT IS_ENTITY_DEAD(deliveryData.cutsceneData.vehiclePackArray[SMG_PD_DELIVERY_VEHICLE_CARGOBOB].vehicle)
				CUTSCENE_HELP_PREPARE_VEHICLE_FOR_SCENE(deliveryData.cutsceneData.vehiclePackArray[SMG_PD_DELIVERY_VEHICLE_CARGOBOB].vehicle, TRUE)
				
				SET_ENTITY_HEADING(deliveryData.cutsceneData.vehiclePackArray[SMG_PD_DELIVERY_VEHICLE_CARGOBOB].vehicle, fFlatBedHeading)
				VECTOR cargobobCoord
				cargobobCoord = vFlatBedCoords + <<0.0, 0.0, 9.5>>
				cargobobCoord = cargobobCoord + (GET_ENTITY_FORWARD_VECTOR(deliveryData.cutsceneData.vehiclePackArray[SMG_PD_DELIVERY_VEHICLE_CARGOBOB].vehicle) * -4.5)
				SET_ENTITY_COORDS(deliveryData.cutsceneData.vehiclePackArray[SMG_PD_DELIVERY_VEHICLE_CARGOBOB].vehicle, cargobobCoord)
				
				FREEZE_ENTITY_POSITION(deliveryData.cutsceneData.vehiclePackArray[SMG_PD_DELIVERY_VEHICLE_CARGOBOB].vehicle, TRUE)
				SET_ENTITY_COLLISION(deliveryData.cutsceneData.vehiclePackArray[SMG_PD_DELIVERY_VEHICLE_CARGOBOB].vehicle, FALSE)
				//Hook
				//SET_VEHICLE_DISABLE_TOWING(deliveryData.cutsceneData.vehiclePackArray[SMG_PD_DELIVERY_VEHICLE_CARGOBOB].vehicle), TRUE)
				CREATE_PICK_UP_ROPE_FOR_CARGOBOB(deliveryData.cutsceneData.vehiclePackArray[SMG_PD_DELIVERY_VEHICLE_CARGOBOB].vehicle)
				SET_PICKUP_ROPE_LENGTH_FOR_CARGOBOB(deliveryData.cutsceneData.vehiclePackArray[SMG_PD_DELIVERY_VEHICLE_CARGOBOB].vehicle, 3.0, 3.0, TRUE)
				SET_CARGOBOB_FORCE_DONT_DETACH_VEHICLE(deliveryData.cutsceneData.vehiclePackArray[SMG_PD_DELIVERY_VEHICLE_CARGOBOB].vehicle, TRUE)
				//Attach container to cargobob.
				IF DOES_ENTITY_EXIST(deliveryData.deliveryVehicleObjectAttached)
				AND NOT IS_ENTITY_DEAD(deliveryData.deliveryVehicleObjectAttached)
					VECTOR vCoord
					vCoord = GET_ENTITY_COORDS(deliveryData.cutsceneData.vehiclePackArray[SMG_PD_DELIVERY_VEHICLE_CARGOBOB].vehicle)
					vCoord = vCoord + GET_ENTITY_FORWARD_VECTOR(deliveryData.cutsceneData.vehiclePackArray[SMG_PD_DELIVERY_VEHICLE_CARGOBOB].vehicle) * 1.5
					vCoord = vCoord + <<0.0, 0.0, -6.0>>
					
					SET_ENTITY_COLLISION(deliveryData.deliveryVehicleObjectAttached, FALSE)
					SET_ENTITY_COORDS(deliveryData.deliveryVehicleObjectAttached, vCoord)
					SET_ENTITY_HEADING(deliveryData.deliveryVehicleObjectAttached, fFlatBedHeading)
					ATTACH_ENTITY_TO_CARGOBOB(deliveryData.cutsceneData.vehiclePackArray[SMG_PD_DELIVERY_VEHICLE_CARGOBOB].vehicle, deliveryData.deliveryVehicleObjectAttached, 0, <<0.0, 0.0, -3.0>>)
				ENDIF
					
				//lowering end cargobob position.
				deliveryData.cutsceneData.vectorArray[SMG_PD_DELIVERY_VECTOR_CARGOBOB_START] = GET_ENTITY_COORDS(deliveryData.cutsceneData.vehiclePackArray[SMG_PD_DELIVERY_VEHICLE_CARGOBOB].vehicle)
				deliveryData.cutsceneData.vectorArray[SMG_PD_DELIVERY_VECTOR_CARGOBOB_FINISH] = deliveryData.cutsceneData.vectorArray[0] - <<0.0, 0.0, 3.3>>
			ENDIF

		BREAK
	ENDSWITCH
ENDPROC

PROC CUTSCENE_SMUGGLER_PRECISION_DELIVERY_MAINTAIN_SCENE(DELIVERY_DATA &deliveryData)			
	//Hide networked cargobob
	IF DOES_ENTITY_EXIST(deliveryData.deliveryVehicle)
	AND IS_ENTITY_ALIVE(deliveryData.deliveryVehicle)
		SET_ENTITY_ALPHA(deliveryData.deliveryVehicle, 0 , FALSE)
	ENDIF
	//task cargobob to fly up when cargo was detached.
	IF NOT IS_BIT_SET(deliveryData.cutsceneData.intArray[SMG_PD_DELIVERY_INTEGER_BITSET], SMG_PD_DELIVERY_BITSET_DONE_CARGOBOB_REPOSITION)
		IF DOES_ENTITY_EXIST(deliveryData.deliveryVehicle)
		AND NOT IS_ENTITY_DEAD(deliveryData.deliveryVehicle)
			ENTITY_INDEX entityAttachedCargobob
			entityAttachedCargobob = GET_ENTITY_ATTACHED_TO_CARGOBOB(deliveryData.deliveryVehicle)
		
			IF NOT DOES_ENTITY_EXIST(entityAttachedCargobob)
				IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
					TASK_HELI_MISSION(PLAYER_PED_ID(), deliveryData.deliveryVehicle, NULL, NULL, GET_ENTITY_COORDS(deliveryData.deliveryVehicle) + <<0.0, 0.0, 100.0>>, MISSION_GOTO, 10.0, 1.0, GET_ENTITY_HEADING(deliveryData.deliveryVehicle), 10, 10, -1.0, HF_DontDoAvoidance)
					CDEBUG1LN(DEBUG_DELIVERY,"[FAT]CUTSCENE_SMUGGLER_PRECISION_DELIVERY_MAINTAIN_SCENE - tasked to fly away: ")
					SET_BIT(deliveryData.cutsceneData.intArray[SMG_PD_DELIVERY_INTEGER_BITSET], SMG_PD_DELIVERY_BITSET_DONE_CARGOBOB_REPOSITION)
				ELSE
					CDEBUG1LN(DEBUG_DELIVERY,"[FAT]CUTSCENE_SMUGGLER_PRECISION_DELIVERY_MAINTAIN_SCENE - NOT A DRIVER: ")
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_DELIVERY,"[FAT]CUTSCENE_SMUGGLER_PRECISION_DELIVERY_MAINTAIN_SCENE - still attached.")
			ENDIF
		ELSE
			CDEBUG1LN(DEBUG_DELIVERY,"[FAT]CUTSCENE_SMUGGLER_PRECISION_DELIVERY_MAINTAIN_SCENE - delivery vehicle does not exist.")
		ENDIF
	ENDIF
	
	IF SIMPLE_CUTSCENE_IS_SCENE_RUNNING(deliveryData.cutsceneData.cutscene, 0)
		//59.942 // start old not used/
		//new start 58.942
		//56.542/ end cargobob
		IF DOES_CAM_EXIST(deliveryData.cutsceneData.cutscene.camera)
			SET_CAM_COORD(deliveryData.cutsceneData.cutscene.camera, GET_ENTITY_COORDS(deliveryData.cutsceneData.objectArray[SMG_PD_DELIVERY_CAMERA_PLACE_HOLDER]))
			SET_CAM_ROT(deliveryData.cutsceneData.cutscene.camera, GET_ENTITY_ROTATION(deliveryData.cutsceneData.objectArray[SMG_PD_DELIVERY_CAMERA_PLACE_HOLDER]))
		ENDIF	
		
		//LOWER CARGOBOB
		IF deliveryData.cutsceneData.cutscene.iCurrentSceneElapsedTime >= 300
			deliveryData.cutsceneData.floatArray[SMG_PD_DELIVERY_FLOAT_CARGOBOB_LOWER_BIAS] += 0.30 * GET_FRAME_TIME()
			deliveryData.cutsceneData.floatArray[SMG_PD_DELIVERY_FLOAT_CARGOBOB_LOWER_BIAS] = CLAMP(deliveryData.cutsceneData.floatArray[SMG_PD_DELIVERY_FLOAT_CARGOBOB_LOWER_BIAS], 0.0, 1.0)
			
			VECTOR vCargobobNewCoord
			vCargobobNewCoord = LERP_VECTOR(deliveryData.cutsceneData.vectorArray[SMG_PD_DELIVERY_VECTOR_CARGOBOB_START], deliveryData.cutsceneData.vectorArray[SMG_PD_DELIVERY_VECTOR_CARGOBOB_FINISH], deliveryData.cutsceneData.floatArray[SMG_PD_DELIVERY_FLOAT_CARGOBOB_LOWER_BIAS])
			IF DOES_ENTITY_EXIST(deliveryData.cutsceneData.vehiclePackArray[SMG_PD_DELIVERY_VEHICLE_CARGOBOB].vehicle)
			AND NOT IS_ENTITY_DEAD(deliveryData.cutsceneData.vehiclePackArray[SMG_PD_DELIVERY_VEHICLE_CARGOBOB].vehicle)
				SET_ENTITY_COORDS_NO_OFFSET(deliveryData.cutsceneData.vehiclePackArray[SMG_PD_DELIVERY_VEHICLE_CARGOBOB].vehicle, vCargobobNewCoord)
			ENDIF
			
			IF deliveryData.cutsceneData.floatArray[SMG_PD_DELIVERY_FLOAT_CARGOBOB_LOWER_BIAS] >= 1.0
			AND NOT IS_BIT_SET(deliveryData.cutsceneData.intArray[SMG_PD_DELIVERY_INTEGER_BITSET], SMG_PD_DELIVERY_BITSET_IMPACT_SOUND_PLAYED)
				CDEBUG1LN(DEBUG_DELIVERY, "CUTSCENE_SMUGGLER_SHOULD_CUTSCENE_START - STOP SOUND PLAY IMPACT")
				STOP_SOUND(deliveryData.cutsceneData.intArray[SMG_PD_DELIVERY_INTEGER_SOUND_ID])
				PLAY_SOUND_FROM_ENTITY(deliveryData.cutsceneData.intArray[SMG_PD_DELIVERY_INTEGER_SOUND_ID], "flatbed_delivery_impact", deliveryData.cutsceneData.vehicleArray[SMG_PD_DELIVERY_VEHICLE_EXTRA_FLATBED], "DLC_SM_Precision_Delivery_Sounds")
				SET_BIT(deliveryData.cutsceneData.intArray[SMG_PD_DELIVERY_INTEGER_BITSET], SMG_PD_DELIVERY_BITSET_IMPACT_SOUND_PLAYED)
			ELSE
				CDEBUG1LN(DEBUG_DELIVERY, "CUTSCENE_SMUGGLER_SHOULD_CUTSCENE_START - B: ", deliveryData.cutsceneData.floatArray[SMG_PD_DELIVERY_FLOAT_CARGOBOB_LOWER_BIAS])
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL CUTSCENE_SMUGGLER_SHOULD_CUTSCENE_START(DELIVERY_DATA &deliveryData)	
	IF _FREEMODE_DELIVERY_IS_DELIVERY_WANTED_LEVEL_RESTRICTED(scriptData.deliveryData.eDropoffID)
		CDEBUG1LN(DEBUG_DELIVERY, "CUTSCENE_SMUGGLER_SHOULD_CUTSCENE_START - WANTED LEVEL")
		RETURN FALSE
	ENDIF
	
	_FREEMODE_DELIVERY_DO_AREA_DELIVERY_CHECKS_FOR_DROPOFF(scriptData.deliveryData.eDropoffID, scriptData.deliveryData.eDeliverableID, FALSE, FALSE)
	IF FREEMODE_DELIVERY_IS_PLAYER_IN_POSITION_TO_AIR_DROP_DELIVERABLE()
		IF DOES_ENTITY_EXIST(deliveryData.deliveryVehicle)
		AND NOT IS_ENTITY_DEAD(deliveryData.deliveryVehicle)
			IF VMAG(GET_ENTITY_VELOCITY(deliveryData.deliveryVehicle)) >= 10.0
				CDEBUG1LN(DEBUG_DELIVERY, "CUTSCENE_SMUGGLER_SHOULD_CUTSCENE_START - SPEED")
				RETURN FALSE
			ENDIF
		ENDIF
		
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
		OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
			FREEMODE_DELIVERY_SET_PLAYER_IN_POSITION_TO_DELIVER_DELIVERABLE(FALSE)
			FREEMODE_DELIVERY_SET_PLAYER_IN_POSITION_TO_AIR_DROP_DELIVERABLE(FALSE) 
			CDEBUG1LN(DEBUG_DELIVERY, "CUTSCENE_SMUGGLER_SHOULD_CUTSCENE_START - TRUE")
			RETURN TRUE
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF GET_FRAME_COUNT() % 150 = 0
			CDEBUG1LN(DEBUG_DELIVERY, "CUTSCENE_SMUGGLER_SHOULD_CUTSCENE_START - FALSE end.")
		ENDIF
	#ENDIF
	RETURN FALSE
ENDFUNC

PROC CUTSCENE_SMUGGLER_PRECISION_DELIVERY_CLEANUP(DELIVERY_DATA &deliveryData)
	CDEBUG1LN(DEBUG_DELIVERY, "[FAT]CUTSCENE_SMUGGLER_HANGAR_CLEANUP")
	IF DOES_ENTITY_EXIST(deliveryData.deliveryVehicle)
	AND IS_ENTITY_ALIVE(deliveryData.deliveryVehicle)
		RESET_ENTITY_ALPHA(deliveryData.deliveryVehicle)
		SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
		IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
			SET_ENTITY_INVINCIBLE(deliveryData.deliveryVehicle, FALSE)
			CDEBUG1LN(DEBUG_DELIVERY,"[FAT]CUTSCENE_SMUGGLER_HANGAR_CLEANUP - VS_DRIVER")
		ENDIF
	ENDIF
	IF IS_BIT_SET(deliveryData.cutsceneData.intArray[SMG_PD_DELIVERY_INTEGER_BITSET], SMG_PD_DELIVERY_BITSET_DONE_CARGOBOB_REPOSITION)
		CDEBUG1LN(DEBUG_DELIVERY,"[FAT]CUTSCENE_SMUGGLER_HANGAR_CLEANUP - Cleared tasks")
		CLEAR_PED_TASKS(PLAYER_PED_ID())
	ENDIF
ENDPROC

PROC CUTSCENE_SMUGGLER_PRECISION_DELIVERY_INITIALIZE(FREEMODE_DELIVERY_SCRIPT_LAUNCH_DATA &_launchData, SCRIPT_DATA& _scriptData)	
	UNUSED_PARAMETER(_launchData)
	//Lookup
	_scriptData.functionPointers.coreFunctions.handlerAssetsCreate = &CUTSCENE_SMUGGLER_PRECISION_DELIVERY_ASSETS_CREATE
	_scriptData.functionPointers.coreFunctions.handlerCreateScene = &CUTSCENE_SMUGGLER_PRECISION_DELIVERY_CREATE_SCENE
	_scriptData.functionPointers.coreFunctions.handlerMaintainScene = &CUTSCENE_SMUGGLER_PRECISION_DELIVERY_MAINTAIN_SCENE
	_scriptData.functionPointers.coreFunctions.handlerSceneSetup = &CUTSCENE_SMUGGLER_PRECISION_DELIVERY_ASSET_SCENE_SETUP
	_scriptData.functionPointers.coreFunctions.handlerCleanup= &CUTSCENE_SMUGGLER_PRECISION_DELIVERY_CLEANUP
			
	DELIVERY_CHILD_SET_AUDIO_SCENE_CUTSCENE(_scriptData.deliveryData, "DLC_SM_PD_Flatbed_Container_Delivery_Scene")
	
	TOGGLE_DELIVERY_FLAG_DEFAULT(_scriptData.deliveryData, DELIVERY_FLAG_DEFAULT_SCREEN_FADE_AFTER_DELIVERY, FALSE)
	TOGGLE_DELIVERY_FLAG_DEFAULT(_scriptData.deliveryData, DELIVERY_FLAG_DEFAULT_BRING_VEHICLE_TO_HALT, FALSE)
	TOGGLE_DELIVERY_FLAG_DEFAULT(_scriptData.deliveryData, DELIVERY_FLAG_DEFAULT_DO_CUTSCENE_START_DELAY, FALSE)
	//Script Logic override
	//Add asset requests
	ADD_ASSET_REQUEST_FOR_MODEL(_scriptData.deliveryData.cutsceneData.assetRequester, 0, INT_TO_ENUM(MODEL_NAMES, HASH("sm_prop_smug_rsply_crate02a")))
	ADD_ASSET_REQUEST_FOR_MODEL(_scriptData.deliveryData.cutsceneData.assetRequester, 1, FLATBED)
	ADD_ASSET_REQUEST_FOR_MODEL(_scriptData.deliveryData.cutsceneData.assetRequester, 2, S_M_Y_DEALER_01)
	ADD_ASSET_REQUEST_FOR_MODEL(_scriptData.deliveryData.cutsceneData.assetRequester, 3, prop_golf_ball)
	
	_scriptData.deliveryData.cutsceneData.intArray[SMG_PD_DELIVERY_INTEGER_SOUND_ID] = GET_SOUND_ID()
	CDEBUG1LN(DEBUG_DELIVERY,"[FAT]CUTSCENE_SMUGGLER_PRECISION_DELIVERY_INITIALIZE - SOUND_ID: ", _scriptData.deliveryData.cutsceneData.intArray[SMG_PD_DELIVERY_INTEGER_SOUND_ID])
	
	scriptData.functionPointers.specificFunctions.shouldDeliveryCutsceneStart = &CUTSCENE_SMUGGLER_SHOULD_CUTSCENE_START
ENDPROC
