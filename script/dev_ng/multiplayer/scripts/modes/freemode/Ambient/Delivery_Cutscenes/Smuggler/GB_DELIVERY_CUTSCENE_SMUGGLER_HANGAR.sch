USING "globals.sch"
USING "net_simple_cutscene.sch"
USING "GB_DELIVERY_DEFINITIONS.sch"
USING "GB_DELIVERY_CHILD_SUPPORT.sch"
USING "smuggler_hangar_entry_cutscenes.sch"
USING "net_simple_interior_hangar.sch"

//DEFINITIONS
CONST_INT HANGAR_DELIVERY_CUTSCENE_WALK_IN 					0
CONST_INT HANGAR_DELIVERY_CUTSCENE_LEAVE_VEHICLE_OUTSIDE	1
CONST_INT HANGAR_DELIVERY_CUTSCENE_BRING_VEHICLE_INSIDE		2

//OBEJCTS
CONST_INT DELIVERY_OBJECT_HANGAR_GROUND 	0
CONST_INT DELIVERY_OBJECT_HANGAR_DOOR_LEFT 	1
CONST_INT DELIVERY_OBJECT_HANGAR_DOOR_RIGHT	2
CONST_INT DELIVERY_OBJECT_HANGAR_SHADOW		3
CONST_INT DELIVERY_OBJECT_HANGAR_LIGHT		4
//VECTORS

//INT ARRAY
CONST_INT SMG_H_DELIVERY_INT_ARRAY_BITSET 			0
CONST_INT SMG_H_DELIVERY_INT_ARRAY_DELIVERY_TYPE	1 //HANGAR_DELIVERY_CUTSCENE_TYPE

//BITSET //SMG_H_DELIVERY_INT_ARRAY_BITSET
CONST_INT SMG_H_DELIVERY_BITSET_TASK_MOVE_INSIDE			0
CONST_INT SMG_H_DELIVERY_BITSET_VEHICLE_REACHED_DOOR		1
CONST_INT SMG_H_DELIVERY_BITSET_VEHICLE_RECORDING_USED		2
//audioBS
CONST_INT SMG_H_DELIVERY_BITSET_AUDIO_STREAM_STOPED			3
CONST_INT SMG_H_DELIVERY_BITSET_AUDIO_STREAM_STARTED		4
CONST_INT SMG_H_DELIVERY_BITSET_AUDIO_DOOR_OPENED_SOUND		5
CONST_INT SMG_H_DELIVERY_BITSET_CUTSCENE_STARTED			6
//CONST_INT SMG_H_DELIVERY_BITSET_STOP_VEHICLE_ENGINE		6

//FLOAT ARRAY
CONST_INT SMG_H_DELIVERY_FLOAT_DOOR_OPEN_RATIO		0
CONST_INT SMG_H_DELIVERY_FLOAT_VEHICLE_SPEED		1

FUNC BOOL CUTSCENE_SMUGGLER_HANGAR_ASSETS_CREATE(DELIVERY_DATA &deliveryData)
	UNUSED_PARAMETER(deliveryData)
	
	#IF FEATURE_HEIST_ISLAND
	BOOL bMissionDropoff = (deliveryData.eDropoffID = FMC_DROPOFF_LSIA_HANGAR_1 OR deliveryData.eDropoffID = FMC_DROPOFF_LSIA_HANGAR_2)
	#ENDIF
	
	CDEBUG2LN(DEBUG_DELIVERY, "CUTSCENE_SMUGGLER_HANGAR_ASSETS_CREATE - light grab from, scriptData.deliveryData.deliveryOwner: ", GET_PLAYER_NAME(scriptData.deliveryData.deliveryOwner))	
	SMUGGLER_HANGAR_SPAWN_ALL_CUTSCENE_PROPS(GET_SIMPLE_INTERIOR_FROM_SMUGGLER_DROPOFF(deliveryData.eDropoffID), scriptData.deliveryData.deliveryOwner,
	deliveryData.cutsceneData.objectArray[DELIVERY_OBJECT_HANGAR_SHADOW],
	deliveryData.cutsceneData.objectArray[DELIVERY_OBJECT_HANGAR_GROUND],
	deliveryData.cutsceneData.objectArray[DELIVERY_OBJECT_HANGAR_DOOR_LEFT],
	deliveryData.cutsceneData.objectArray[DELIVERY_OBJECT_HANGAR_DOOR_RIGHT],
	deliveryData.cutsceneData.objectArray[DELIVERY_OBJECT_HANGAR_LIGHT] #IF FEATURE_HEIST_ISLAND , bMissionDropoff #ENDIF)
	
	RETURN TRUE
ENDFUNC

FUNC BOOL CUTSCENE_SMUGGLER_HANGAR_CREATE_SCENE(DELIVERY_DATA &deliveryData)
	CDEBUG2LN(DEBUG_DELIVERY, "CUTSCENE_SMUGGLER_HANGAR_CREATE_SCENE - new scene added.")	
	
	
	SMUGGLER_HANGAR_ADD_ENTRY_SCENES(GET_SIMPLE_INTERIOR_FROM_SMUGGLER_DROPOFF(deliveryData.eDropoffID), deliveryData.cutsceneData.cutscene, TRUE)
//	
//	SWITCH GET_SIMPLE_INTERIOR_FROM_SMUGGLER_DROPOFF(deliveryData.eDropoffID)
//		CASE SIMPLE_INTERIOR_HANGAR_1
//			//SWITCH deliveryData.deliveryType
//				SIMPLE_CUTSCENE_ADD_SCENE(deliveryData.cutsceneData.cutscene, 7000, "DoorOpen", <<-1153.9325, -3417.0769, 14.8057>>, <<-1.3850, 0.0139, -20.1005>>, 47.6242, <<-1153.9325, -3417.0769, 14.8057>>, <<-1.3850, 0.0139, -20.1005>>, 47.6242, 1.0, 50, 0, GRAPH_TYPE_LINEAR)
//				
////				CASE DT_VEHICLE
////					SIMPLE_CUTSCENE_ADD_SCENE(deliveryData.cutsceneData.cutscene, 8000, "DoorOpen", <<-1130.5835, -3422.6221, 47.0637>>, <<-50.6684, 0.2072, 44.9889>>, 31.3883, <<-1129.2828, -3423.4968, 48.8805>>, <<-49.4953, 0.2072, 44.9889>>, 31.3883, 1.0, 50, 0, GRAPH_TYPE_LINEAR)
////				BREAK
////				CASE DT_FOOT
////					SIMPLE_CUTSCENE_ADD_SCENE(deliveryData.cutsceneData.cutscene, 4000, "DoorOpen", <<-1130.5835, -3422.6221, 47.0637>>, <<-50.6684, 0.2072, 44.9889>>, 31.3883, <<-1129.2828, -3423.4968, 48.8805>>, <<-49.4953, 0.2072, 44.9889>>, 31.3883, 1.0, 50, 0, GRAPH_TYPE_LINEAR)
////				BREAK
//			//ENDSWITCH
//		BREAK
//		
//		CASE SIMPLE_INTERIOR_HANGAR_2
//			SIMPLE_CUTSCENE_ADD_SCENE(deliveryData.cutsceneData.cutscene, 7000, "DoorOpen", <<-1397.8757, -3270.8530, 13.7599>>, <<0.0522, 0.1906, -32.1037>>, 44.3700, <<-1399.2123, -3270.8989, 14.2000>>, <<0.0522, 0.1906, -35.6240>>, 44.3700, 1.0, 50, 0, GRAPH_TYPE_LINEAR)
////			SWITCH deliveryData.deliveryType
////				CASE DT_VEHICLE
////					SIMPLE_CUTSCENE_ADD_SCENE(deliveryData.cutsceneData.cutscene, 8000, "DoorOpen", <<-1383.6124, -3277.2749, 36.2584>>, <<-51.1004, 0.0893, 34.8471>>, 31.3883, <<-1384.1860, -3276.9551, 35.5679>>, <<-51.1004, 0.0893, 34.8471>>, 31.3883, 1.0, 50, 0, GRAPH_TYPE_LINEAR)
////				BREAK
////				CASE DT_FOOT
////					SIMPLE_CUTSCENE_ADD_SCENE(deliveryData.cutsceneData.cutscene, 4000, "DoorOpen", <<-1383.6124, -3277.2749, 36.2584>>, <<-51.1004, 0.0893, 34.8471>>, 31.3883, <<-1384.1860, -3276.9551, 35.5679>>, <<-51.1004, 0.0893, 34.8471>>, 31.3883, 1.0, 50, 0, GRAPH_TYPE_LINEAR)
////				BREAK
////			ENDSWITCH
//		BREAK
//
//		CASE SIMPLE_INTERIOR_HANGAR_3
//			SIMPLE_CUTSCENE_ADD_SCENE(deliveryData.cutsceneData.cutscene, 7000, "DoorOpen", <<-2017.9141, 3163.3787, 32.5853>>, <<-0.3057, -0.0000, 150.4240>>, 47.4749, <<-2017.9141, 3163.3787, 32.5853>>, <<-0.3057, -0.0000, 150.4240>>, 47.4749, 1.0, 50, 0, GRAPH_TYPE_LINEAR)
////			SWITCH deliveryData.deliveryType
////				CASE DT_VEHICLE
////					SIMPLE_CUTSCENE_ADD_SCENE(deliveryData.cutsceneData.cutscene, 8000, "DoorOpen", <<-2039.9498, 3167.9131, 62.0952>>, <<-51.3307, -0.0000, -136.4024>>, 50, <<-2039.0171, 3167.3860, 60.8159>>, <<-51.3307, -0.0000, -136.4024>>, 50, 1.0, 50, 0, GRAPH_TYPE_LINEAR)
////				BREAK
////				CASE DT_FOOT
////					SIMPLE_CUTSCENE_ADD_SCENE(deliveryData.cutsceneData.cutscene, 4000, "DoorOpen", <<-2039.9498, 3167.9131, 62.0952>>, <<-51.3307, -0.0000, -136.4024>>, 50, <<-2039.0171, 3167.3860, 60.8159>>, <<-51.3307, -0.0000, -136.4024>>, 50, 1.0, 50, 0, GRAPH_TYPE_LINEAR)
////				BREAK
////			ENDSWITCH
//		BREAK
//		
//		CASE SIMPLE_INTERIOR_HANGAR_4
//			SIMPLE_CUTSCENE_ADD_SCENE(deliveryData.cutsceneData.cutscene, 7000, "DoorOpen", <<-1873.3253, 3114.9600, 32.8099>>, <<-3.9302, 0.0091, 151.7198>>, 50.000, <<-1873.3253, 3114.9600, 32.8099>>, <<-3.9302, 0.0091, 151.7198>>, 50.000, 1.0, 50, 0, GRAPH_TYPE_LINEAR)
////			SWITCH deliveryData.deliveryType
////				CASE DT_VEHICLE
////					SIMPLE_CUTSCENE_ADD_SCENE(deliveryData.cutsceneData.cutscene, 8000, "DoorOpen", <<-1860.0061, 3101.6501, 39.4789>>, <<-20.7258, -0.0000, 77.9390>>, 50, <<-1859.8038, 3101.8452, 39.5379>>, <<-20.3245, 0.1083, 78.1531>>, 50, 1.0, 50, 0, GRAPH_TYPE_LINEAR)
////				BREAK
////				CASE DT_FOOT
////					SIMPLE_CUTSCENE_ADD_SCENE(deliveryData.cutsceneData.cutscene, 4000, "DoorOpen", <<-1860.0061, 3101.6501, 39.4789>>, <<-20.7258, -0.0000, 77.9390>>, 50, <<-1859.8038, 3101.8452, 39.5379>>, <<-20.3245, 0.1083, 78.1531>>, 50, 1.0, 50, 0, GRAPH_TYPE_LINEAR)
////				BREAK
////			ENDSWITCH
//		BREAK
//		
//		CASE SIMPLE_INTERIOR_HANGAR_5
//			SIMPLE_CUTSCENE_ADD_SCENE(deliveryData.cutsceneData.cutscene, 7000, "DoorOpen", <<-2466.7385, 3286.1919, 34.1198>>, <<-5.1581, 0.0368, 157.8272>>, 28.8674, <<-2466.7385, 3286.1919, 34.1198>>, <<-5.1581, 0.0368, 157.8272>>, 28.8674, 1.0, 50, 0, GRAPH_TYPE_LINEAR)
////			SWITCH deliveryData.deliveryType
////				CASE DT_VEHICLE
////					SIMPLE_CUTSCENE_ADD_SCENE(deliveryData.cutsceneData.cutscene, 8000, "DoorOpen", <<-2486.1104, 3288.4473, 38.8931>>, <<-16.4325, 0.0000, -134.0506>>, 50, <<-2485.4998, 3288.6599, 38.8093>>, <<-15.8486, 0.1470, -134.7430>>, 50, 1.0, 50, 0, GRAPH_TYPE_LINEAR)
////				BREAK
////				CASE DT_FOOT
////					SIMPLE_CUTSCENE_ADD_SCENE(deliveryData.cutsceneData.cutscene, 4000, "DoorOpen", <<-2486.1104, 3288.4473, 38.8931>>, <<-16.4325, 0.0000, -134.0506>>, 50, <<-2485.4998, 3288.6599, 38.8093>>, <<-15.8486, 0.1470, -134.7430>>, 50, 1.0, 50, 0, GRAPH_TYPE_LINEAR)
////				BREAK
////			ENDSWITCH
//		BREAK
//	ENDSWITCH

	RETURN TRUE
ENDFUNC

//Will call once to position assets on the start of an x scene
PROC CUTSCENE_SMUGGLER_HANGAR_ASSET_SCENE_SETUP(DELIVERY_DATA &deliveryData, INT iScene)
	CDEBUG2LN(DEBUG_DELIVERY, "CUTSCENE_SMUGGLER_HANGAR_ASSET_SCENE_SETUP")	
	SWITCH iScene
		CASE 0
			SWITCH deliveryData.deliveryType
				CASE DT_VEHICLE
					CUTSCENE_HELP_PREPARE_VEHICLE_FOR_SCENE(deliveryData.cutsceneData.vehiclePackArray[0].vehicle, TRUE)
					SMUGGLER_HANGAR_POSITION_PED_AT_ENTRY(deliveryData.cutsceneData.vehiclePackArray[0].passengers[0], GET_SIMPLE_INTERIOR_FROM_SMUGGLER_DROPOFF(deliveryData.eDropoffID))
					SMUGGLER_HANGAR_VEHICLE_DISABLE_ENTRY_PROP_COLLISION(GET_SIMPLE_INTERIOR_FROM_SMUGGLER_DROPOFF(deliveryData.eDropoffID), deliveryData.cutsceneData.vehiclePackArray[0].vehicle)
					INT indexVehiclePack
					INT indexPassenger
					REPEAT CONST_DELIVERY_MAX_VEHICLES_PACK indexVehiclePack
						REPEAT MAX_NO_VEH_PASSENGERS indexPassenger
							IF DOES_ENTITY_EXIST(deliveryData.cutsceneData.vehiclePackArray[indexVehiclePack].passengers[indexPassenger])
							AND NOT IS_ENTITY_DEAD(deliveryData.cutsceneData.vehiclePackArray[indexVehiclePack].passengers[indexPassenger])
								SET_DISABLE_PED_MAP_COLLISION(deliveryData.cutsceneData.vehiclePackArray[indexVehiclePack].passengers[indexPassenger])
								CDEBUG2LN(DEBUG_DELIVERY, "CUTSCENE_SMUGGLER_HANGAR_ASSET_SCENE_SETUP - indexVehiclePack: ", indexVehiclePack, " indexPassenger: ", indexPassenger)	
							ENDIF
						ENDREPEAT
					ENDREPEAT
				BREAK
					
				CASE DT_FOOT
					SMUGGLER_HANGAR_POSITION_PED_AT_ENTRY(deliveryData.cutsceneData.pedArray[0], GET_SIMPLE_INTERIOR_FROM_SMUGGLER_DROPOFF(deliveryData.eDropoffID))
					IF DOES_ENTITY_EXIST(deliveryData.cutsceneData.pedArray[0])
					AND NOT IS_ENTITY_DEAD(deliveryData.cutsceneData.pedArray[0])
						SET_DISABLE_PED_MAP_COLLISION(deliveryData.cutsceneData.pedArray[0])
						CDEBUG2LN(DEBUG_DELIVERY, "CUTSCENE_SMUGGLER_HANGAR_ASSET_SCENE_SETUP - pedArray[0]")	
					ENDIF
				BREAK
			ENDSWITCH
			
			SMUGGLER_HANGAR_TOGGLE_HANGAR_MODEL_HIDE(GET_SIMPLE_INTERIOR_FROM_SMUGGLER_DROPOFF(deliveryData.eDropoffID), TRUE)
			HANGAR_CUTSCENE_PREPARE_ASSETS(deliveryData.cutsceneData.objectArray[DELIVERY_OBJECT_HANGAR_SHADOW],
			deliveryData.cutsceneData.objectArray[DELIVERY_OBJECT_HANGAR_GROUND], 
			deliveryData.cutsceneData.objectArray[DELIVERY_OBJECT_HANGAR_DOOR_LEFT], 
			deliveryData.cutsceneData.objectArray[DELIVERY_OBJECT_HANGAR_DOOR_RIGHT])
			
			deliveryData.cutsceneData.floatArray[SMG_H_DELIVERY_FLOAT_DOOR_OPEN_RATIO] = 0.0
			
			IF NOT IS_BIT_SET(deliveryData.cutsceneData.intArray[SMG_H_DELIVERY_INT_ARRAY_BITSET], SMG_H_DELIVERY_BITSET_CUTSCENE_STARTED)
				SET_BIT(deliveryData.cutsceneData.intArray[SMG_H_DELIVERY_INT_ARRAY_BITSET], SMG_H_DELIVERY_BITSET_CUTSCENE_STARTED)
				CDEBUG1LN(DEBUG_DELIVERY, "SMG_H_DELIVERY_BITSET_CUTSCENE_STARTED - TRUE")	
			ENDIF
		BREAK
		CASE 1
			SMUGGLER_HANGAR_HIDE_DOORS(deliveryData.cutsceneData.objectArray[DELIVERY_OBJECT_HANGAR_DOOR_LEFT], deliveryData.cutsceneData.objectArray[DELIVERY_OBJECT_HANGAR_DOOR_RIGHT])
		BREAK
	ENDSWITCH
ENDPROC

PROC CUTSCENE_SMUGGLER_HANGAR_TASK_PASSENGERS_MOVE_TO_HANGAR(VEHICLE_INDEX &vehPed, VECTOR vMoveTo, BOOL bFirstPassenger = FALSE)
	IF DOES_ENTITY_EXIST(vehPed)
		//MS
		INT iPedExitDelay
		INT iPedExitDelayDefault
		iPedExitDelayDefault = GET_RANDOM_INT_IN_RANGE(1300, 1500)
		
		INT iVehicleSeat
		VEHICLE_SEAT eVehSeat

		SEQUENCE_INDEX sequencePed					
		FOR iVehicleSeat = VS_DRIVER TO VS_EXTRA_RIGHT_3
		eVehSeat = INT_TO_ENUM(VEHICLE_SEAT, iVehicleSeat)
			IF NOT IS_VEHICLE_SEAT_FREE(vehPed, eVehSeat)
				PED_INDEX pedInSeat
				pedInSeat = GET_PED_IN_VEHICLE_SEAT(vehPed, eVehSeat)
				IF DOES_ENTITY_EXIST(pedInSeat)
				AND NOT IS_ENTITY_DEAD(pedInSeat)
					iPedExitDelay = 0
					IF eVehSeat <> VS_DRIVER
						iPedExitDelay = iPedExitDelayDefault
					ENDIF
					CDEBUG1LN(DEBUG_DELIVERY, "CUTSCENE_SMUGGLER_HANGAR_TASK_PASSENGERS_MOVE_TO_HANGAR - iVehicleSeat: ", iVehicleSeat, ", iPedExitDelayDefault: ", iPedExitDelay)
					//Task players to go to the hangar entry gate					
					OPEN_SEQUENCE_TASK(sequencePed)
						TASK_LEAVE_ANY_VEHICLE(NULL, iPedExitDelay, ECF_DONT_DEFAULT_WARP_IF_DOOR_BLOCKED)
						//TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vMoveTo, PEDMOVEBLENDRATIO_SPRINT, DEFAULT_TIME_NEVER_WARP, 1.0, ENAV_DONT_AVOID_PEDS | ENAV_DONT_AVOID_OBJECTS)
						TASK_GO_STRAIGHT_TO_COORD(NULL, vMoveTo, PEDMOVEBLENDRATIO_SPRINT, DEFAULT_TIME_NEVER_WARP)
					CLOSE_SEQUENCE_TASK(sequencePed)
					
					TASK_PERFORM_SEQUENCE(pedInSeat, sequencePed)
					CLEAR_SEQUENCE_TASK(sequencePed)
					
					IF bFirstPassenger
						EXIT
					ENDIF
				ELSE
					//ASSERTLN("[SHC]TEST_TASK_PASSENGERS_MOVE_TO_HANGAR - seat is not free but, DOES_ENTITY_EXIST: ", DOES_ENTITY_EXIST(pedInSeat), " IS_ENTITY_DEAD: ", IS_ENTITY_DEAD(pedInSeat))
				ENDIF
			ENDIF
		ENDFOR
	ELSE
		ASSERTLN("[SHC]TEST_TASK_PASSENGERS_MOVE_TO_HANGAR - ped is not inside any vehicle, cant task passangers")
	ENDIF
ENDPROC

PROC CUTSCENE_SMUGGLER_HANGAR_MAINTAIN_SCENE(DELIVERY_DATA &deliveryData)	
	//PLAY DOOR OPEN SOUND
	IF deliveryData.cutsceneData.cutscene.iTotalElapsedTime >= 4500
	AND deliveryData.cutsceneData.floatArray[SMG_H_DELIVERY_FLOAT_DOOR_OPEN_RATIO] < 1.0
		deliveryData.cutsceneData.floatArray[SMG_H_DELIVERY_FLOAT_DOOR_OPEN_RATIO] = 1.0
	ENDIF
	
		IF DOES_ENTITY_EXIST(deliveryData.cutsceneData.vehiclePackArray[0].vehicle)
		AND NOT IS_ENTITY_DEAD(deliveryData.cutsceneData.vehiclePackArray[0].vehicle)
		AND GET_ENTITY_MODEL(deliveryData.cutsceneData.vehiclePackArray[0].vehicle) = GET_ARMORY_AIRCRAFT_MODEL()
			SET_VEHICLE_ENGINE_ON(deliveryData.cutsceneData.vehiclePackArray[0].vehicle, FALSE, TRUE)
		ENDIF
	
	//UPDATE		
	IF SIMPLE_CUTSCENE_IS_SCENE_RUNNING(deliveryData.cutsceneData.cutscene, 0)
		IF deliveryData.cutsceneData.cutscene.iCurrentSceneElapsedTime >= 0
			SMUGGLER_HANGAR_CUTSCENE_DOOR_MAINTAIN_AUDIO(deliveryData.cutsceneData.floatArray[SMG_H_DELIVERY_FLOAT_DOOR_OPEN_RATIO], deliveryData.cutsceneData.intArray[SMG_H_DELIVERY_INT_ARRAY_BITSET], SMG_H_DELIVERY_BITSET_AUDIO_STREAM_STARTED, SMG_H_DELIVERY_BITSET_AUDIO_DOOR_OPENED_SOUND)
			HANGAR_CUTSCENE_MAINTAIN_DOOR_SLIDE_OPENING(GET_SIMPLE_INTERIOR_FROM_SMUGGLER_DROPOFF(deliveryData.eDropoffID), deliveryData.cutsceneData.objectArray[DELIVERY_OBJECT_HANGAR_DOOR_LEFT], deliveryData.cutsceneData.objectArray[DELIVERY_OBJECT_HANGAR_DOOR_RIGHT], deliveryData.cutsceneData.floatArray[SMG_H_DELIVERY_FLOAT_DOOR_OPEN_RATIO])
		ENDIF

		SWITCH deliveryData.deliveryType
			CASE DT_VEHICLE
				IF deliveryData.cutsceneData.intArray[SMG_H_DELIVERY_INT_ARRAY_DELIVERY_TYPE] = HANGAR_DELIVERY_CUTSCENE_LEAVE_VEHICLE_OUTSIDE
					IF NOT IS_BIT_SET(deliveryData.cutsceneData.intArray[SMG_H_DELIVERY_INT_ARRAY_BITSET], SMG_H_DELIVERY_BITSET_TASK_MOVE_INSIDE)
						INT iTaskDelayMS
						IF GET_ENTITY_MODEL(deliveryData.cutsceneData.vehiclePackArray[0].vehicle) = TULA
							iTaskDelayMS = 1500
						ELIF GET_ENTITY_MODEL(deliveryData.cutsceneData.vehiclePackArray[0].vehicle) = MOGUL
							iTaskDelayMS = 4500
						ELSE
							iTaskDelayMS = 2500
						ENDIF
						
						IF deliveryData.cutsceneData.cutscene.iCurrentSceneElapsedTime >= iTaskDelayMS
							IF GET_ENTITY_MODEL(deliveryData.cutsceneData.vehiclePackArray[0].vehicle) = TULA
								CUTSCENE_SMUGGLER_HANGAR_TASK_PASSENGERS_MOVE_TO_HANGAR(deliveryData.cutsceneData.vehiclePackArray[0].vehicle, SMUGGLER_HANGAR_GET_ENTRY_END_COORD(GET_SIMPLE_INTERIOR_FROM_SMUGGLER_DROPOFF(deliveryData.eDropoffID)), TRUE)
							ELSE
								CUTSCENE_SMUGGLER_HANGAR_TASK_PASSENGERS_MOVE_TO_HANGAR(deliveryData.cutsceneData.vehiclePackArray[0].vehicle, SMUGGLER_HANGAR_GET_ENTRY_END_COORD(GET_SIMPLE_INTERIOR_FROM_SMUGGLER_DROPOFF(deliveryData.eDropoffID)))
							ENDIF
							SET_BIT(deliveryData.cutsceneData.intArray[SMG_H_DELIVERY_INT_ARRAY_BITSET], SMG_H_DELIVERY_BITSET_TASK_MOVE_INSIDE)
							CDEBUG2LN(DEBUG_DELIVERY, "CUTSCENE_SMUGGLER_HANGAR_MAINTAIN_SCENE - task ped to run inside away from the vehicle")	
						ENDIF
					ENDIF
					
//					IF NOT IS_BIT_SET(deliveryData.cutsceneData.intArray[SMG_H_DELIVERY_INT_ARRAY_BITSET], SMG_H_DELIVERY_BITSET_STOP_VEHICLE_ENGINE)
//						IF deliveryData.cutsceneData.cutscene.iCurrentSceneElapsedTime >= 1000
//							IF DOES_ENTITY_EXIST(deliveryData.cutsceneData.vehiclePackArray[0].vehicle)
//							AND NOT IS_ENTITY_DEAD(deliveryData.cutsceneData.vehiclePackArray[0].vehicle)
//								SET_VEHICLE_ENGINE_ON(deliveryData.cutsceneData.vehiclePackArray[0].vehicle, FALSE, TRUE)
//								CDEBUG2LN(DEBUG_DELIVERY, "CUTSCENE_SMUGGLER_HANGAR_MAINTAIN_SCENE - disabling engine")	
//							ENDIF
//							SET_BIT(deliveryData.cutsceneData.intArray[SMG_H_DELIVERY_INT_ARRAY_BITSET], SMG_H_DELIVERY_BITSET_STOP_VEHICLE_ENGINE)
//						ENDIF
//					ENDIF
				ELSE
					IF NOT IS_BIT_SET(deliveryData.cutsceneData.intArray[SMG_H_DELIVERY_INT_ARRAY_BITSET], SMG_H_DELIVERY_BITSET_TASK_MOVE_INSIDE)
						IF deliveryData.cutsceneData.cutscene.iCurrentSceneElapsedTime >= 1500
							SMUGGLER_HANGAR_TASK_PED_MOVE_INSIDE(GET_SIMPLE_INTERIOR_FROM_SMUGGLER_DROPOFF(deliveryData.eDropoffID), deliveryData.cutsceneData.vehiclePackArray[0].passengers[0], HANGAR_ENTRY_SPEED_QUICK)
							IF DOES_ENTITY_EXIST(deliveryData.cutsceneData.vehiclePackArray[0].vehicle)
							AND NOT IS_ENTITY_DEAD(deliveryData.cutsceneData.vehiclePackArray[0].vehicle)
								SET_VEHICLE_MAX_SPEED(deliveryData.cutsceneData.vehiclePackArray[0].vehicle, 0.7)
							ENDIF
							SET_BIT(deliveryData.cutsceneData.intArray[SMG_H_DELIVERY_INT_ARRAY_BITSET], SMG_H_DELIVERY_BITSET_TASK_MOVE_INSIDE)
							CDEBUG2LN(DEBUG_DELIVERY, "CUTSCENE_SMUGGLER_HANGAR_MAINTAIN_SCENE - task vehicle to drive inside.")	
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE DT_FOOT
				IF NOT IS_BIT_SET(deliveryData.cutsceneData.intArray[SMG_H_DELIVERY_INT_ARRAY_BITSET], SMG_H_DELIVERY_BITSET_TASK_MOVE_INSIDE)
					SMUGGLER_HANGAR_TASK_PED_MOVE_INSIDE(GET_SIMPLE_INTERIOR_FROM_SMUGGLER_DROPOFF(deliveryData.eDropoffID), deliveryData.cutsceneData.pedArray[0], HANGAR_ENTRY_SPEED_QUICK)
					SET_BIT(deliveryData.cutsceneData.intArray[SMG_H_DELIVERY_INT_ARRAY_BITSET], SMG_H_DELIVERY_BITSET_TASK_MOVE_INSIDE)
				ENDIF
			BREAK
		ENDSWITCH
		
		IF IS_BIT_SET(deliveryData.cutsceneData.intArray[SMG_H_DELIVERY_INT_ARRAY_BITSET], SMG_H_DELIVERY_BITSET_TASK_MOVE_INSIDE)
			IF deliveryData.deliveryType = DT_VEHICLE
			AND deliveryData.cutsceneData.intArray[SMG_H_DELIVERY_INT_ARRAY_DELIVERY_TYPE] = HANGAR_DELIVERY_CUTSCENE_LEAVE_VEHICLE_OUTSIDE
				INT indexVehiclePack
				INT indexPassenger
				REPEAT CONST_DELIVERY_MAX_VEHICLES_PACK indexVehiclePack
					REPEAT MAX_NO_VEH_PASSENGERS indexPassenger
						IF DOES_ENTITY_EXIST(deliveryData.cutsceneData.vehiclePackArray[indexVehiclePack].passengers[indexPassenger])
						AND NOT IS_ENTITY_DEAD(deliveryData.cutsceneData.vehiclePackArray[indexVehiclePack].passengers[indexPassenger])
							SET_DISABLE_PED_MAP_COLLISION(deliveryData.cutsceneData.vehiclePackArray[indexVehiclePack].passengers[indexPassenger])
						ENDIF
					ENDREPEAT
				ENDREPEAT
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL CUTSCENE_SMUGGLER_HANGAR_ASSETS_LOADED(DELIVERY_DATA &deliveryData)
	BOOL bAllAssetsLoaded = TRUE
	
	IF NOT SMUGGLER_HANGAR_HAVE_CUTSCENE_ASSETS_LOADED(GET_SIMPLE_INTERIOR_FROM_SMUGGLER_DROPOFF(deliveryData.eDropoffID))
		CDEBUG1LN(DEBUG_DELIVERY, "TEST_ASSETS_LOADED - SMUGGLER_HANGAR_HAVE_CUTSCENE_ASSETS_LOADED - FALSE.")
		bAllAssetsLoaded = FALSE
	ENDIF

	IF NOT DELIVERY_DEFAULT_ASSTS_LOADED(deliveryData)
		CDEBUG1LN(DEBUG_DELIVERY, "TEST_ASSETS_LOADED - DELIVERY_DEFAULT_ASSTS_LOADED - FALSE.")
		bAllAssetsLoaded = FALSE
	ENDIF
	
	IF NOT SMUGGLER_HANGAR_CUTSCENE_DOOR_STREAM_LOADED(deliveryData.cutsceneData.intArray[SMG_H_DELIVERY_INT_ARRAY_BITSET], SMG_H_DELIVERY_BITSET_AUDIO_STREAM_STOPED)
		CDEBUG1LN(DEBUG_DELIVERY, "TEST_ASSETS_LOADED - SMUGGLER_HANGAR_CUTSCENE_DOOR_STREAM_LOADED - FALSE.")
		bAllAssetsLoaded = FALSE
	ENDIF
	
	RETURN bAllAssetsLoaded
ENDFUNC

PROC CUTSCENE_SMUGGLER_HANGAR_CLEANUP(DELIVERY_DATA &deliveryData)
	CDEBUG1LN(DEBUG_DELIVERY, "CUTSCENE_SMUGGLER_HANGAR_CLEANUP")
	IF IS_BIT_SET(deliveryData.cutsceneData.intArray[SMG_H_DELIVERY_INT_ARRAY_BITSET], SMG_H_DELIVERY_BITSET_CUTSCENE_STARTED)
		SMUGGLER_HANGAR_TOGGLE_HANGAR_MODEL_HIDE(GET_SIMPLE_INTERIOR_FROM_SMUGGLER_DROPOFF(deliveryData.eDropoffID), FALSE)
		//Remove ambient vehicles in front of the hangar
		VECTOR vClearAreaCoord
		FLOAT fClearAreaRadius
		
		SWITCH GET_SIMPLE_INTERIOR_FROM_SMUGGLER_DROPOFF(deliveryData.eDropoffID)
			CASE SIMPLE_INTERIOR_HANGAR_1
				vClearAreaCoord = <<-1144.650, -3390.700, 13.000>>
				fClearAreaRadius = 70.700
			BREAK
			CASE SIMPLE_INTERIOR_HANGAR_2
				vClearAreaCoord = <<-1386.700, -3252.475, 13.000>>
				fClearAreaRadius =  50.150
			BREAK
			CASE SIMPLE_INTERIOR_HANGAR_3
				vClearAreaCoord = <<-2028.525, 3144.300, 32.800>>
				fClearAreaRadius = 55.950
			BREAK
			CASE SIMPLE_INTERIOR_HANGAR_4
				vClearAreaCoord = <<-1891.200, 3086.700, 32.800>>
				fClearAreaRadius = 50.100
			BREAK
			CASE SIMPLE_INTERIOR_HANGAR_5
				vClearAreaCoord = <<-2479.800, 3255.200, 32.800>> 
				fClearAreaRadius = 50.100
			BREAK
		ENDSWITCH
		CDEBUG1LN(DEBUG_DELIVERY, "CUTSCENE_SMUGGLER_HANGAR_CLEANUP - vClearAreaCoord: ", vClearAreaCoord, " fClearAreaRadius: ", fClearAreaRadius)
		CLEAR_AREA_OF_VEHICLES(vClearAreaCoord, fClearAreaRadius, TRUE, FALSE , FALSE , FALSE, TRUE, TRUE) //OnFire
		CLEAR_AREA_OF_VEHICLES(vClearAreaCoord, fClearAreaRadius, TRUE, FALSE , TRUE , FALSE, TRUE, FALSE) //Wrecked
	ENDIF
ENDPROC

//Should this vehicle drive into the hangar in the cutscene.
FUNC BOOL CUTSCENE_SMUGGLER_HANGAR_SHOULD_VEHICLE_ENTER(VEHICLE_INDEX &vehicle, FREEMODE_DELIVERY_DROPOFFS eDropoff)
	CDEBUG1LN(DEBUG_DELIVERY, "CUTSCENE_SMUGGLER_HANGAR - Mission vehicle, bring in. Mission: ", IS_ENTITY_A_MISSION_ENTITY(vehicle)
	," Personal: ", IS_VEHICLE_A_PERSONAL_VEHICLE(vehicle)
	," Plane: ", IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(vehicle)))
	
	IF NOT DOES_ENTITY_EXIST(vehicle)
		ASSERTLN("CUTSCENE_SMUGGLER_HANGAR_SHOULD_VEHICLE_ENTER - vehicle does not exist")
		RETURN FALSE
	ENDIF
	
	#IF FEATURE_HEIST_ISLAND
	IF eDropoff = FMC_DROPOFF_LSIA_HANGAR_1
	OR eDropoff = FMC_DROPOFF_LSIA_HANGAR_2
		RETURN TRUE
	ENDIF
	#ENDIF
						
	IF (IS_VEHICLE_A_PERSONAL_VEHICLE(vehicle) AND IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(vehicle)))
	//allow setup aircraft to go in in the cutscene.
	OR (IS_ENTITY_A_MISSION_ENTITY(vehicle) AND IS_SMUGGLER_VARIATION_A_SETUP() AND IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(vehicle)))
		RETURN TRUE
	ENDIF
	
	CDEBUG1LN(DEBUG_DELIVERY, "CUTSCENE_SMUGGLER_HANGAR - CUTSCENE_SMUGGLER_HANGAR_SHOULD_VEHICLE_ENTER - FALSE.")
	RETURN FALSE
ENDFUNC

PROC CUTSCENE_SMUGGLER_SPAWN_POINT_SETUP(DELIVERY_DATA &deliveryData)
	UNUSED_PARAMETER(deliveryData)
	CDEBUG2LN(DEBUG_DELIVERY, "CUTSCENE_GBC_CASINO_SPAWN_POINT_SETUP")
	
	VECTOR vSpawnPoint
	FLOAT fSpawnHeading
	INT i
	FLOAT fWeight = 1.0
	
	WHILE GET_LSIA_GENERAL_DELIVERY_SCENE_SPAWN_COORD(i, vSpawnPoint, fSpawnHeading)
		ADD_CUSTOM_SPAWN_POINT(vSpawnPoint, fSpawnHeading, fWeight)
		fWeight -= 0.01
		i++
	ENDWHILE
ENDPROC

//Vehicle on the side, player walks in.
//walk in 
//drop from air.
//Plane fly in , heli fly in.
PROC CUTSCENE_SMUGGLER_HANGAR_INITIALIZE(FREEMODE_DELIVERY_SCRIPT_LAUNCH_DATA &_launchData, SCRIPT_DATA& _scriptData)	
	UNUSED_PARAMETER(_launchData)
	//Work out type of delivery
	SWITCH _scriptData.deliveryData.deliveryType
		CASE DT_VEHICLE
			SWITCH _scriptData.deliveryData.eCutscene
				CASE SMUGGLER_CUTSCENE_HANGAR_LEAVE_VEHICLE
					_scriptData.deliveryData.cutsceneData.intArray[SMG_H_DELIVERY_INT_ARRAY_DELIVERY_TYPE] = HANGAR_DELIVERY_CUTSCENE_LEAVE_VEHICLE_OUTSIDE
					CDEBUG1LN(DEBUG_DELIVERY, "TEST_INITIALIZE - Cutscene dictates, leave vehicle outside.")
				BREAK
				
				CASE SMUGGLER_CUTSCENE_HANGAR_BRING_EVERYTHING
					IF CUTSCENE_SMUGGLER_HANGAR_SHOULD_VEHICLE_ENTER(_scriptData.deliveryData.deliveryVehicle, _scriptData.deliveryData.eDropoffID)
						_scriptData.deliveryData.cutsceneData.intArray[SMG_H_DELIVERY_INT_ARRAY_DELIVERY_TYPE] = HANGAR_DELIVERY_CUTSCENE_BRING_VEHICLE_INSIDE
					ELSE
						_scriptData.deliveryData.cutsceneData.intArray[SMG_H_DELIVERY_INT_ARRAY_DELIVERY_TYPE] = HANGAR_DELIVERY_CUTSCENE_LEAVE_VEHICLE_OUTSIDE
					ENDIF
				BREAK
			ENDSWITCH
			
			IF IS_ENTITY_ALIVE(_scriptData.deliveryData.deliveryVehicle)
			AND IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(_scriptData.deliveryData.deliveryVehicle))
				TOGGLE_DELIVERY_FLAG_DEFAULT(scriptData.deliveryData, DELIVERY_FLAG_DEFAULT_DO_CUTSCENE_START_DELAY, FALSE)
			ENDIF
		BREAK
		
		CASE DT_FOOT
			//vehicle does not exist, jsut walk in.
			_scriptData.deliveryData.cutsceneData.intArray[SMG_H_DELIVERY_INT_ARRAY_DELIVERY_TYPE] = HANGAR_DELIVERY_CUTSCENE_WALK_IN
			CDEBUG1LN(DEBUG_DELIVERY, "TEST_INITIALIZE - DT_FOOT")
		BREAK
	ENDSWITCH
	
	CDEBUG1LN(DEBUG_DELIVERY, "TEST_INITIALIZE - DELIVERY TYPE: ", _scriptData.deliveryData.cutsceneData.intArray[SMG_H_DELIVERY_INT_ARRAY_DELIVERY_TYPE])
	
	//Lookup
	_scriptData.functionPointers.coreFunctions.handlerAssetsLoaded = &CUTSCENE_SMUGGLER_HANGAR_ASSETS_LOADED
	_scriptData.functionPointers.coreFunctions.handlerAssetsCreate = &CUTSCENE_SMUGGLER_HANGAR_ASSETS_CREATE
	_scriptData.functionPointers.coreFunctions.handlerCreateScene = &CUTSCENE_SMUGGLER_HANGAR_CREATE_SCENE
	_scriptData.functionPointers.coreFunctions.handlerMaintainScene = &CUTSCENE_SMUGGLER_HANGAR_MAINTAIN_SCENE
	_scriptData.functionPointers.coreFunctions.handlerSceneSetup = &CUTSCENE_SMUGGLER_HANGAR_ASSET_SCENE_SETUP
	_scriptData.functionPointers.coreFunctions.handlerCleanup = &CUTSCENE_SMUGGLER_HANGAR_CLEANUP
	_scriptData.functionPointers.coreFunctions.handlerSetupAfterSceneSpawnPoints = &CUTSCENE_SMUGGLER_SPAWN_POINT_SETUP
		
	//Delivery Setup
	TOGGLE_DELIVERY_FLAG_EXTRA(_scriptData.deliveryData, DELIVERY_FLAG_EXTRA_REPOSITION_DELIVERY_VEHICLE, TRUE)
	#IF FEATURE_HEIST_ISLAND
	IF _scriptData.deliveryData.eDropoffID = FMC_DROPOFF_LSIA_HANGAR_1
	OR _scriptData.deliveryData.eDropoffID = FMC_DROPOFF_LSIA_HANGAR_2
		TOGGLE_DELIVERY_FLAG_EXTRA(_scriptData.deliveryData, DELIVERY_FLAG_EXTRA_REPOSITION_PED_AFTER_SCENE, TRUE)
		TOGGLE_DELIVERY_FLAG_EXTRA(_scriptData.deliveryData, DELIVERY_FLAG_EXTRA_DONT_SIMULATE_INPUT_GAIT_ON_WARP, TRUE)
	ENDIF
	#ENDIF	
	
	//Script Logic override
	//Add asset requests
	ADD_ASSET_REQUEST_FOR_MODEL(_scriptData.deliveryData.cutsceneData.assetRequester, 0, p_parachute1_mp_dec)
	ADD_ASSET_REQUEST_FOR_MODEL(_scriptData.deliveryData.cutsceneData.assetRequester, 1, prop_golf_ball)
ENDPROC
