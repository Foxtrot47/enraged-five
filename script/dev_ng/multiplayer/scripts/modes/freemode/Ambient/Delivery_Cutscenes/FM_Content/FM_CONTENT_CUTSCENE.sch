USING "globals.sch"
USING "net_simple_cutscene.sch"
USING "GB_DELIVERY_DEFINITIONS.sch"
USING "GB_DELIVERY_CHILD_SUPPORT.sch"
USING "net_simple_interior.sch"
USING "FM_CONTENT_CUTSCENE_SPAWNS.sch"

//Consts & Data defined in 	x:\gta5\script\dev_ng\multiplayer\scripts\modes\freemode\ambient\delivery_cutscenes\fm_content\fm_content_cutscene_data.sch
//Helpers in 				X:\gta5\script\dev_ng\multiplayer\scripts\modes\freemode\Ambient\Delivery_Cutscenes\FM_Content\FM_CONTENT_CUTSCENE_HELPERS.sch
//Spawn points in 			X:\gta5\script\dev_ng\multiplayer\scripts\modes\freemode\Ambient\Delivery_Cutscenes\FM_Content\FM_CONTENT_CUTSCENE_SPAWNS.sch

FUNC BOOL CUTSCENE_FMC_ASSETS_CREATE_DOCKS_SCENE(DELIVERY_DATA &deliveryData)
	
	deliveryData.cutsceneData.objectArray[FMC_DELIVERY_BOJECT_DOCKS_SCENE_CRANE_GRIP] 	= CREATE_OBJECT(P_DOCK_CRANE_SLD_S, << 984.455, -2883.53, 29.8977 >>, FALSE)
	deliveryData.cutsceneData.objectArray[FMC_DELIVERY_BOJECT_DOCKS_SCENE_ROPES] 		= CREATE_OBJECT(p_dock_crane_cabl_s, << 984.455, -2883.53, 29.8977 >>, FALSE)
	deliveryData.cutsceneData.objectArray[FMC_DELIVERY_BOJECT_DOCKS_SCENE_CONTAINER] 	= CREATE_OBJECT(PROP_CONTAINER_LD_D, << 984.455, -2883.53, 29.8977 >>, FALSE)
	
	IF IS_ENTITY_ALIVE(deliveryData.cutsceneData.objectArray[FMC_DELIVERY_BOJECT_DOCKS_SCENE_CRANE_GRIP])
		ATTACH_ENTITY_TO_ENTITY(deliveryData.cutsceneData.objectArray[FMC_DELIVERY_BOJECT_DOCKS_SCENE_ROPES], deliveryData.cutsceneData.objectArray[FMC_DELIVERY_BOJECT_DOCKS_SCENE_CRANE_GRIP], -1, <<0.0, 0.0, 7.41>>, <<0.0, 0.0, 0.0>>)
		ATTACH_ENTITY_TO_ENTITY(deliveryData.cutsceneData.objectArray[FMC_DELIVERY_BOJECT_DOCKS_SCENE_CONTAINER], deliveryData.cutsceneData.objectArray[FMC_DELIVERY_BOJECT_DOCKS_SCENE_CRANE_GRIP], -1, <<0.0, 0.0, -3.39>> , <<0.0, 0.0, 0.0>>)
		
		FREEZE_ENTITY_POSITION(deliveryData.cutsceneData.objectArray[FMC_DELIVERY_BOJECT_DOCKS_SCENE_CRANE_GRIP], TRUE)
		SET_ENTITY_VISIBLE(deliveryData.cutsceneData.objectArray[FMC_DELIVERY_BOJECT_DOCKS_SCENE_CRANE_GRIP], TRUE)
		SET_ENTITY_HEADING(deliveryData.cutsceneData.objectArray[FMC_DELIVERY_BOJECT_DOCKS_SCENE_CRANE_GRIP], 270.0)
		
		PLAY_ENTITY_ANIM(deliveryData.cutsceneData.objectArray[FMC_DELIVERY_BOJECT_DOCKS_SCENE_CRANE_GRIP], "Dock_crane_SLD_load", "map_objects", INSTANT_BLEND_IN, FALSE, TRUE)
	ELSE
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL CUTSCENE_FMC_ASSETS_CREATE(DELIVERY_DATA &deliveryData)
	UNUSED_PARAMETER(deliveryData)
	
	IF SHOULD_DO_FMC_DOCKS_DELIVERY_SCENE(deliveryData)
		RETURN CUTSCENE_FMC_ASSETS_CREATE_DOCKS_SCENE(deliveryData)
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC _FMC_DELIVERY_PLAY_AUDIO_STREAM(STRING sStreamName)
	IF NOT IS_STRING_NULL_OR_EMPTY(sStreamName)
		IF NOT IS_STREAM_PLAYING()
		AND LOAD_STREAM(sStreamName)
			PLAY_STREAM_FRONTEND()
			CDEBUG1LN(DEBUG_DELIVERY, "_FMC_DELIVERY_PLAY_AUDIO_STREAM - Called...")
		ENDIF
	ENDIF
ENDPROC

PROC _FMC_DELIVERY_STOP_AUDIO_STREAM(STRING sStreamName)
	IF NOT IS_STRING_NULL_OR_EMPTY(sStreamName)
		IF IS_STREAM_PLAYING()
		AND LOAD_STREAM(sStreamName)
			STOP_STREAM()
			CDEBUG1LN(DEBUG_DELIVERY, "_FMC_DELIVERY_STOP_AUDIO_STREAM - Called...")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL _FMC_DELIVERY_LOAD_AUDIO_STREAM(STRING sStreamName, INT iStartOffset = -1)
	IF NOT IS_STRING_NULL_OR_EMPTY(sStreamName)	
		IF iStartOffset <= 0
			CDEBUG1LN(DEBUG_DELIVERY, "_FMC_DELIVERY_LOAD_AUDIO_STREAM - Called... Loaded? ", LOAD_STREAM(sStreamName))
		
			RETURN LOAD_STREAM(sStreamName)
		ENDIF
		
		CDEBUG1LN(DEBUG_DELIVERY, "_FMC_DELIVERY_LOAD_AUDIO_STREAM - Called... Loaded? ", LOAD_STREAM_WITH_START_OFFSET(sStreamName, iStartOffset), " offset: ", iStartOffset)
		
		RETURN LOAD_STREAM_WITH_START_OFFSET(sStreamName, iStartOffset)
	ELSE
		CDEBUG1LN(DEBUG_DELIVERY, "_FMC_DELIVERY_LOAD_AUDIO_STREAM - Called... ")
		RETURN TRUE
	ENDIF
ENDFUNC

PROC CUTSCENE_FMC_CREATE_SCENE_DRIVE_IN(DELIVERY_DATA &deliveryData)
	VECTOR vCamPos, vCamPosEnd, vCamRot, vCamEndRot
	FLOAT fShotFOV, fShotEndFOV
	INT i, iShotDuration
	TEXT_LABEL_15 tlSceneName
	
	REPEAT GET_FMC_VEH_SCENE_COUNT(deliveryData.eDropoffID) i
		tlSceneName = "FMC Veh scene "
		tlSceneName += i
		
		GET_FMC_VEH_SCENE_SHOT(deliveryData.eDropoffID, vCamPos, vCamRot, fShotFOV, i, TRUE)
		GET_FMC_VEH_SCENE_SHOT(deliveryData.eDropoffID, vCamPosEnd, vCamEndRot, fShotEndFOV, i, FALSE)
		iShotDuration = GET_FMC_VEH_DELIVERY_SCENE_SHOT_DURATION(deliveryData, i)
	
		SIMPLE_CUTSCENE_ADD_SCENE(deliveryData.cutsceneData.cutscene, iShotDuration, tlSceneName, vCamPos, vCamRot, fShotFOV, 
									vCamPosEnd, vCamEndRot, fShotEndFOV, 0.2, 0, 0)	
	ENDREPEAT
ENDPROC

PROC CUTSCENE_FMC_CREATE_SCENE_DRIVE_IN_WITH_CAM_ZOOM(DELIVERY_DATA &deliveryData)
	VECTOR vEstablishingShot
	VECTOR vEstablishingRot
	FLOAT fEstablishingFOV
		
	VECTOR vTDShot
	VECTOR vTDRot
	FLOAT fTDFOV
	
	GET_FMC_VEH_SCENE_SHOT(deliveryData.eDropoffID, vEstablishingShot, vEstablishingRot, fEstablishingFOV, 0, TRUE)
	GET_FMC_VEH_SCENE_SHOT(deliveryData.eDropoffID, vTDShot, vTDRot, fTDFOV, 1, TRUE)
	
	SIMPLE_INTERIOR_VEH_ENTRY_SCENE_CREATE_SCENE(deliveryData.cutsceneData.cutscene, vEstablishingShot, vEstablishingRot, fEstablishingFOV, (fEstablishingFOV - 5.0), FALSE, vTDShot, vTDRot, fTDFOV)
ENDPROC

PROC CUTSCENE_FMC_CREATE_SCENE_EXIT_LIFT_GARAGE(DELIVERY_DATA &deliveryData)
	VECTOR vTDShot, vTDRot
	FLOAT fTDFOV
	FLOAT fCamShake = 0.2
	INT i, iShotDuration, iFadeOutTime
	TEXT_LABEL_15 tlSceneName
	
	REPEAT GET_FMC_VEH_SCENE_COUNT(deliveryData.eDropoffID) i
		tlSceneName = "FMC Exit Veh "
		tlSceneName += i
		
		iShotDuration = GET_FMC_VEH_EXIT_LIFT_GARAGE_SCENE_SHOT_DURATION(deliveryData.eDropoffID, i)
		iFadeOutTime = GET_FMC_VEH_EXIT_LIFT_GARAGE_SCENE_SHOT_FADE_OUT_TIME(deliveryData.eDropoffID, i)
			
		GET_FMC_VEH_EXIT_LIFT_GARAGE_SCENE_SHOT(deliveryData.eDropoffID, vTDShot, vTDRot, fTDFOV, i)
		
		SIMPLE_CUTSCENE_ADD_SCENE(deliveryData.cutsceneData.cutscene, iShotDuration, tlSceneName, vTDShot, vTDRot, fTDFOV, vTDShot, vTDRot, fTDFOV, fCamShake, 0, iFadeOutTime)
	ENDREPEAT
ENDPROC

PROC CUTSCENE_FMC_CREATE_SCENE_ANIMATED_CAM_WALK_IN(DELIVERY_DATA &deliveryData)
	INT iShotDuration = GET_FMC_ON_FOOT_DELIVERY_SCENE_SHOT_DURATION(deliveryData.eDropoffID, 0)
	FMC_ON_FOOT_SCENE_DETAILS sDetails = FMC_GET_ON_FOOT_SCENE_DETAILS(deliveryData.eDropoffID)
	
	SIMPLE_CUTSCENE_ADD_ANIMATED_CAM_SCENE(deliveryData.cutsceneData.cutscene, iShotDuration, "FMC AC ft scn", sDetails.vScenePosition, sDetails.vSceneRotation, sDetails.sAnimDict, sDetails.sAnimCam, TRUE)
ENDPROC

PROC CUTSCENE_FMC_CREATE_SCENE_WALK_IN(DELIVERY_DATA &deliveryData)
	VECTOR vCamPos, vCamPosEnd, vCamRot, vCamEndRot
	FLOAT fShotFOV, fShotEndFOV
	INT i, iShotDuration
	TEXT_LABEL_15 tlSceneName
	
	REPEAT GET_FMC_ON_FOOT_SCENE_COUNT(deliveryData.eDropoffID) i
		tlSceneName = "FMC ft scene "
		tlSceneName += i
		
		GET_FMC_ON_FOOT_SCENE_SHOT(deliveryData.eDropoffID, vCamPos, vCamRot, fShotFOV, i, TRUE)
		GET_FMC_ON_FOOT_SCENE_SHOT(deliveryData.eDropoffID, vCamPosEnd, vCamEndRot, fShotEndFOV, i, FALSE)
		iShotDuration = GET_FMC_ON_FOOT_DELIVERY_SCENE_SHOT_DURATION(deliveryData.eDropoffID, i)
	
		SIMPLE_CUTSCENE_ADD_SCENE(deliveryData.cutsceneData.cutscene, iShotDuration, tlSceneName, vCamPos, vCamRot, fShotFOV, 
									vCamPosEnd, vCamEndRot, fShotEndFOV, 0.2, 0, 1000)	
	ENDREPEAT
ENDPROC

PROC CUTSCENE_FMC_CREATE_BOAT_SCENE(DELIVERY_DATA &deliveryData)
	VECTOR vCamPos, vCamPosEnd, vCamRot, vCamEndRot
	FLOAT fShotFOV, fShotEndFOV
	INT iShotDuration
	TEXT_LABEL_15 tlSceneName = "FMC Boat scene"
	
	GET_FMC_VEH_SCENE_SHOT(deliveryData.eDropoffID, vCamPos, vCamRot, fShotFOV, 0, TRUE)
	GET_FMC_VEH_SCENE_SHOT(deliveryData.eDropoffID, vCamPosEnd, vCamEndRot, fShotEndFOV, 0, FALSE)
	iShotDuration = GET_FMC_VEH_DELIVERY_SCENE_SHOT_DURATION(deliveryData, 0)

	SIMPLE_CUTSCENE_ADD_SCENE(deliveryData.cutsceneData.cutscene, iShotDuration, tlSceneName, vCamPos, vCamRot, fShotFOV, 
									vCamPosEnd, vCamEndRot, fShotEndFOV, 0.2, 0, 1000)	
ENDPROC

PROC CUTSCENE_FMC_CREATE_DOCKS_SCENE(DELIVERY_DATA &deliveryData)
	VECTOR vCamPos, vCamPosEnd, vCamRot, vCamEndRot
	FLOAT fShotFOV, fShotEndFOV
	INT iShotDuration
	TEXT_LABEL_15 tlSceneName = "FMC Dock scene"
	
	GET_FMC_VEH_SCENE_SHOT(deliveryData.eDropoffID, vCamPos, vCamRot, fShotFOV, 0, TRUE)	
	GET_FMC_VEH_SCENE_SHOT(deliveryData.eDropoffID, vCamPosEnd, vCamEndRot, fShotEndFOV, 0, FALSE)
	
	iShotDuration = GET_FMC_VEH_DELIVERY_SCENE_SHOT_DURATION(deliveryData, 0)

	SIMPLE_CUTSCENE_ADD_SCENE(deliveryData.cutsceneData.cutscene, iShotDuration, tlSceneName, vCamPos, vCamRot, fShotFOV, 
									vCamPosEnd, vCamEndRot, fShotEndFOV, 0.1, 0, 1000, GRAPH_TYPE_DECEL)
ENDPROC

PROC CUTSCENE_FMC_CREATE_VEH_HANDOVER_TO_CUSTOMER_SCENE(DELIVERY_DATA &deliveryData)
	VECTOR vCamPos, vCamPosEnd, vCamRot, vCamEndRot
	FLOAT fShotFOV, fShotEndFOV
	INT iShotDuration
	TEXT_LABEL_15 tlSceneName = "FMC handover "
	
	GET_FMC_VEH_SCENE_SHOT(deliveryData.eDropoffID, vCamPos, vCamRot, fShotFOV, 0, TRUE)	
	GET_FMC_VEH_SCENE_SHOT(deliveryData.eDropoffID, vCamPosEnd, vCamEndRot, fShotEndFOV, 0, FALSE)
	
	iShotDuration = GET_FMC_VEH_DELIVERY_SCENE_SHOT_DURATION(deliveryData, 0)

	SIMPLE_CUTSCENE_ADD_SCENE(deliveryData.cutsceneData.cutscene, iShotDuration, tlSceneName, vCamPos, vCamRot, fShotFOV, 
									vCamPosEnd, vCamEndRot, fShotEndFOV, 0.1, 0, 1000, GRAPH_TYPE_DECEL)
ENDPROC

PROC CUTSCENE_FMC_CREATE_DROPOFF_PASSENGER_SCENE(DELIVERY_DATA &deliveryData)
	VECTOR vCamPos, vCamPosEnd, vCamRot, vCamEndRot
	FLOAT fShotFOV, fShotEndFOV
	INT iShotDuration, i
	TEXT_LABEL_15 tlSceneName
	
	REPEAT GET_FMC_VEH_SCENE_COUNT(deliveryData.eDropoffID) i
		tlSceneName = "FMC psngr DO "
		tlSceneName += i
		
		GET_FMC_VEH_SCENE_SHOT(deliveryData.eDropoffID, vCamPos, vCamRot, fShotFOV, i, TRUE)
		GET_FMC_VEH_SCENE_SHOT(deliveryData.eDropoffID, vCamPosEnd, vCamEndRot, fShotEndFOV, i, FALSE)
		
		iShotDuration = GET_FMC_VEH_DELIVERY_SCENE_SHOT_DURATION(deliveryData, i)
	
		SIMPLE_CUTSCENE_ADD_SCENE(deliveryData.cutsceneData.cutscene, iShotDuration, tlSceneName, vCamPos, vCamRot, fShotFOV, 
									vCamPosEnd, vCamEndRot, fShotEndFOV, 0.1)	
	ENDREPEAT
ENDPROC

PROC CUTSCENE_FMC_CREATE_VEHICLE_BACKUP_SCENE(DELIVERY_DATA &deliveryData)
	VECTOR vCamPos, vCamPosEnd, vCamRot, vCamEndRot
	FLOAT fShotFOV, fShotEndFOV
	INT iShotDuration, i, iFadeOutTime
	TEXT_LABEL_15 tlSceneName
	
	REPEAT GET_FMC_VEH_SCENE_COUNT(deliveryData.eDropoffID) i
		tlSceneName = "FMC BUP scene "
		tlSceneName += i
		
		GET_FMC_VEH_SCENE_SHOT(deliveryData.eDropoffID, vCamPos, vCamRot, fShotFOV, i, TRUE)
		GET_FMC_VEH_SCENE_SHOT(deliveryData.eDropoffID, vCamPosEnd, vCamEndRot, fShotEndFOV, i, FALSE)
		
		iShotDuration = GET_FMC_VEH_DELIVERY_SCENE_SHOT_DURATION(deliveryData, i)
		
		IF i + 1 = GET_FMC_VEH_SCENE_COUNT(deliveryData.eDropoffID)
			iFadeOutTime = 1000
		ENDIF
	
		SIMPLE_CUTSCENE_ADD_SCENE(deliveryData.cutsceneData.cutscene, iShotDuration, tlSceneName, vCamPos, vCamRot, fShotFOV, 
									vCamPosEnd, vCamEndRot, fShotEndFOV, 0.2, 0, iFadeOutTime)	
	ENDREPEAT
ENDPROC

FUNC BOOL CUTSCENE_FMC_CREATE_SCENE(DELIVERY_DATA &deliveryData)
	
	IF FMC_SHOULD_SCENE_CHECK_FOR_NEARBY_DELIVERABLES(deliveryData.eDropoffID)
	AND DELIVERY_SCENE_DO_DELIVERABLES_CHECK(deliveryData, FMC_DROPOFF_IS_PROPERTY(deliveryData.eDropoffID), FMC_GET_DROPOFF_PROPERTY_ID(deliveryData.eDropoffID), IS_PLAYER_DELIVERING_IN_A_FMC_MISSION_VEHICLE(deliveryData), TRUE)
		//Set flags here if required
	ENDIF
	
	IF SHOULD_DO_DRIVE_IN_SCENE(deliveryData)
		CDEBUG2LN(DEBUG_DELIVERY, "CUTSCENE_FMC_CREATE_SCENE - Setup scenes for veh drive in.")
		IF FMC_DOES_VEHICLE_SCENE_USE_ES_ZOOM(deliveryData.eDropoffID)
			CUTSCENE_FMC_CREATE_SCENE_DRIVE_IN_WITH_CAM_ZOOM(deliveryData)
		ELSE
			CUTSCENE_FMC_CREATE_SCENE_DRIVE_IN(deliveryData)
		ENDIF
	ELIF SHOULD_DO_BOAT_DELIVERY_SCENE(deliveryData)
		CUTSCENE_FMC_CREATE_BOAT_SCENE(deliveryData)
	ELIF SHOULD_DO_FMC_DOCKS_DELIVERY_SCENE(deliveryData)
		CUTSCENE_FMC_CREATE_DOCKS_SCENE(deliveryData)
	ELIF SHOULD_DO_FMC_VEHICLE_DROPOFF_DELIVERY_SCENE(deliveryData)
		CUTSCENE_FMC_CREATE_VEH_HANDOVER_TO_CUSTOMER_SCENE(deliveryData)
	ELIF SHOULD_DO_FMC_VEHICLE_BACKUP_INTO_SPACE_SCENE(deliveryData)
		CUTSCENE_FMC_CREATE_VEHICLE_BACKUP_SCENE(deliveryData)
	ELIF SHOULD_DO_FMC_DROPOFF_PASSENGER_SCENE(deliveryData)
		CUTSCENE_FMC_CREATE_DROPOFF_PASSENGER_SCENE(deliveryData)
	ELIF SHOULD_DO_IN_VEHICLE_EXIT_WALK_SCENE(deliveryData)
		VECTOR vTDShot, vTDEndShot, vTDRot, vTDEndRot
		FLOAT fTDFOV, fTDEndFOV
		
		GET_FMC_VEH_EXIT_SCENE_SHOT(deliveryData.eDropoffID, vTDShot, vTDRot, fTDFOV, TRUE)
		GET_FMC_VEH_EXIT_SCENE_SHOT(deliveryData.eDropoffID, vTDEndShot, vTDEndRot, fTDEndFOV, FALSE)
		
		SIMPLE_CUTSCENE_ADD_SCENE(deliveryData.cutsceneData.cutscene, 4200, "TD Veh Exit", vTDShot, vTDRot, fTDFOV, vTDEndShot, vTDEndRot, fTDEndFOV, 0.1, 0, 1000)
	ELIF SHOULD_DO_EXIT_LIFT_GARAGE_SCENE(deliveryData)
		CUTSCENE_FMC_CREATE_SCENE_EXIT_LIFT_GARAGE(deliveryData)
	ELSE
		IF FMC_DOES_ON_FOOT_SCENE_USE_ANIMATED_CAMERA(deliveryData.eDropoffID)
			CUTSCENE_FMC_CREATE_SCENE_ANIMATED_CAM_WALK_IN(deliveryData)
			CDEBUG2LN(DEBUG_DELIVERY, "CUTSCENE_FMC_CREATE_SCENE - Setup scene for walk in with animated camera.")
		ELSE
			CUTSCENE_FMC_CREATE_SCENE_WALK_IN(deliveryData)
			CDEBUG2LN(DEBUG_DELIVERY, "CUTSCENE_FMC_CREATE_SCENE - Setup scene for walk in.")
		ENDIF
	ENDIF
			
	RETURN TRUE
ENDFUNC

FUNC VECTOR FMC_SCENE_VEHICLE_START_POSITION(DELIVERY_DATA &deliveryData, INT iBS)
	VECTOR vReturn
	UNUSED_PARAMETER(iBS)
	
	IF SHOULD_DO_DRIVE_IN_SCENE(deliveryData)
	OR SHOULD_DO_BOAT_DELIVERY_SCENE(deliveryData)
	OR SHOULD_DO_FMC_VEHICLE_DROPOFF_DELIVERY_SCENE(deliveryData)
	OR SHOULD_DO_FMC_VEHICLE_BACKUP_INTO_SPACE_SCENE(deliveryData)
	OR SHOULD_DO_FMC_DROPOFF_PASSENGER_SCENE(deliveryData)
		vReturn = GET_FMC_VEHICLE_DELIVERY_SCENE_VEH_START_POS(deliveryData.eDropoffID)
	ENDIF
	
	RETURN vReturn
ENDFUNC

FUNC VECTOR FMC_SCENE_VEHICLE_END_POSITION(DELIVERY_DATA &deliveryData)
	VECTOR vReturn
	
	IF SHOULD_DO_DRIVE_IN_SCENE(deliveryData)
	OR SHOULD_DO_FMC_VEHICLE_BACKUP_INTO_SPACE_SCENE(deliveryData)
		vReturn = GET_FMC_VEHICLE_DELIVERY_SCENE_VEH_END_POS(deliveryData.eDropoffID)
	ENDIF
		
	RETURN vReturn
ENDFUNC

FUNC FLOAT FMC_SCENE_VEHICLE_START_HEADING(DELIVERY_DATA &deliveryData)
	FLOAT fReturn
	
	IF SHOULD_DO_DRIVE_IN_SCENE(deliveryData)
	OR SHOULD_DO_BOAT_DELIVERY_SCENE(deliveryData)
	OR SHOULD_DO_FMC_VEHICLE_DROPOFF_DELIVERY_SCENE(deliveryData)
	OR SHOULD_DO_FMC_VEHICLE_BACKUP_INTO_SPACE_SCENE(deliveryData)
	OR SHOULD_DO_FMC_DROPOFF_PASSENGER_SCENE(deliveryData)
		fReturn = GET_FMC_VEH_DELIVERY_SCENE_VEHICLE_START_HEADING(deliveryData.eDropoffID)
	ENDIF
	
	RETURN fReturn
ENDFUNC

FUNC INT GET_COUNT_OF_PROPS_ATTACHED_TO_DELIVERY_VEHICLE(DELIVERY_DATA &deliveryData)
	
	IF IS_BIT_SET(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET], FMC_DELIVERY_BITSET_ATTACHED_PROP_TO_CLONE)
	
		INT i
		
		REPEAT FMC_MAX_ATTACHED_PROPS i
			INT iPropArrayIndex = (FMC_DELIVERY_INT_ATTACH_VEH_MODEL + i)	
			
			IF deliveryData.cutsceneData.intArray[iPropArrayIndex] = ENUM_TO_INT(DUMMY_MODEL_FOR_SCRIPT)
				RETURN i
			ENDIF
		ENDREPEAT
		
		RETURN i
	ENDIF
	
	RETURN 0
ENDFUNC

PROC COPPY_ATTACHED_VEHICLE_SETUP(DELIVERY_DATA &deliveryData, VEHICLE_INDEX &vehToAttach)
	IF DOES_ENTITY_EXIST(vehToAttach)
		MODEL_NAMES eAttachModel = GET_ENTITY_MODEL(vehToAttach)
		ENTITY_INDEX entity = GET_ENTITY_OF_TYPE_ATTACHED_TO_ENTITY(deliveryData.deliveryVehicle, eAttachModel)
			
		IF DOES_ENTITY_EXIST(entity)
			VEHICLE_INDEX veh = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(entity)
			
			IF IS_VEHICLE_DRIVEABLE(veh)
				RESET_VEHICLE_SETUP_STRUCT_MP(g_sVehicleSetup)
				GET_VEHICLE_SETUP_MP(veh, g_sVehicleSetup)
				SET_VEHICLE_SETUP_MP(vehToAttach, g_sVehicleSetup)
				
				COPY_VEHICLE_DAMAGES(veh, vehToAttach)
				
				VEHICLE_COPY_EXTRAS(veh, vehToAttach)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CUTSCENE_FMC_POSITION_LOADED_ASSETS(DELIVERY_DATA &deliveryData)
	CDEBUG3LN(DEBUG_DELIVERY, "CUTSCENE_FMC_POSITION_LOADED_ASSETS - Called")
	
	IF IS_BIT_SET(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET], FMC_DELIVERY_BITSET_ATTACHED_PROP_TO_CLONE)
		IF IS_ENTITY_ALIVE(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle)
			
			INT i
			
			REPEAT GET_COUNT_OF_PROPS_ATTACHED_TO_DELIVERY_VEHICLE(deliveryData) i
				INT iPropArrayIndex = (FMC_DELIVERY_INT_ATTACH_VEH_MODEL + i)	
				FLOAT fZOffset = 10.0 + (i + 1)
				
				MODEL_NAMES eModel 		= INT_TO_ENUM(MODEL_NAMES, deliveryData.cutsceneData.intArray[iPropArrayIndex])
				VECTOR propCoords 		= (GET_ENTITY_COORDS(PLAYER_PED_ID()) - <<0.0, 0.0, fZOffset>>)
				VECTOR vAttachOffset	= FMC_GET_VEH_PROP_ATTACH_OFFSET(ENUM_TO_INT(eModel), i)
				VECTOR vRotationOffset	= FMC_GET_VEH_PROP_ATTACH_ROT_OFFSET(ENUM_TO_INT(eModel), i)
				VEHICLE_INDEX veh		= deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle
				
				IF IS_BIT_SET(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET], FMC_DELIVERY_BITSET_ATTACHED_PROP_TO_TRAILER_CLONE)
					IF NOT GET_VEHICLE_TRAILER_VEHICLE(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle, veh)
						SCRIPT_ASSERT("CUTSCENE_FMC_POSITION_LOADED_ASSETS - Failed to grab vehicle to attach prop to")
						PRINTLN("CUTSCENE_FMC_POSITION_LOADED_ASSETS - Failed to grab vehicle to attach prop to")
						EXIT
					ENDIF
				ENDIF
								
				INT iBoneIndex = FMC_GET_VEH_PROP_ATTACH_BONE_INDEX(veh, ENUM_TO_INT(eModel))
				INT iObjectIndex = (FMC_OBJECT_ARRAY_VEH_ATTACH_OBJ + i)
				
				IF IS_MODEL_A_VEHICLE(eModel)
					deliveryData.cutsceneData.vehicleArray[FMC_DELIVERY_VEH_ARRAY_DUMMY_VEH] = CREATE_VEHICLE(eModel, propCoords, DEFAULT, FALSE, FALSE, TRUE)
					
					IF IS_ENTITY_ALIVE(deliveryData.cutsceneData.vehicleArray[FMC_DELIVERY_VEH_ARRAY_DUMMY_VEH])
						//Coppy the mods and damage
						COPPY_ATTACHED_VEHICLE_SETUP(deliveryData, deliveryData.cutsceneData.vehicleArray[FMC_DELIVERY_VEH_ARRAY_DUMMY_VEH])
						
						//Attach the vehicle
						ATTACH_ENTITY_TO_ENTITY(deliveryData.cutsceneData.vehicleArray[FMC_DELIVERY_VEH_ARRAY_DUMMY_VEH], veh, 
													iBoneIndex, vAttachOffset, vRotationOffset, FALSE, FALSE, FALSE)
						CDEBUG2LN(DEBUG_DELIVERY, "CUTSCENE_FMC_POSITION_LOADED_ASSETS - attached vehicle: ", eModel, " to vehicle")
					ENDIF					
				ELSE
					deliveryData.cutsceneData.objectArray[iObjectIndex] = CREATE_OBJECT(eModel, propCoords, FALSE, FALSE)
					
					IF IS_ENTITY_ALIVE(deliveryData.cutsceneData.objectArray[iObjectIndex])
						ATTACH_ENTITY_TO_ENTITY(deliveryData.cutsceneData.objectArray[iObjectIndex], veh, 
													iBoneIndex, vAttachOffset, vRotationOffset, FALSE, FALSE, FALSE)
						CDEBUG2LN(DEBUG_DELIVERY, "CUTSCENE_FMC_POSITION_LOADED_ASSETS - attached prop: ", iObjectIndex, " to vehicle")
					ENDIF
				ENDIF
				
				CDEBUG2LN(DEBUG_DELIVERY, "CUTSCENE_FMC_POSITION_LOADED_ASSETS - Creating setup mission prop: ", eModel, " iPropArrayIndex: ", iPropArrayIndex)
			ENDREPEAT
		ELSE
			SCRIPT_ASSERT("CUTSCENE_FMC_POSITION_LOADED_ASSETS - Failed - Delivery vehicle does not exist")
		ENDIF
	ENDIF
ENDPROC

FUNC FLOAT GET_VEHICLE_HEIGHT_LIMIT_FOR_DROPOFF(FREEMODE_DELIVERY_DROPOFFS eDropOff)
	SWITCH eDropOff
		CASE FMC_DROPOFF_FIXER_OFFICE_LITTLE_SEOUL_GARAGE
		CASE FMC_DROPOFF_FIXER_OFFICE_VESPUCCI_GARAGE
			RETURN 2.5
		BREAK
	ENDSWITCH
	
	RETURN 2.75
ENDFUNC

PROC CHECK_VEHICLE_HEIGHT(DELIVERY_DATA &deliveryData, VECTOR &vModelMin, VEHICLE_INDEX vehicleToCheck)
	IF IS_ENTITY_ALIVE(vehicleToCheck)
		GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(vehicleToCheck), vModelMin, deliveryData.cutsceneData.vectorArray[FMC_DELIVERY_VEC_ARRAY_PRIMARY_VEH_MODEL_MAX])
		
		//Check the height of the vehicle
		IF (deliveryData.cutsceneData.vectorArray[FMC_DELIVERY_VEC_ARRAY_PRIMARY_VEH_MODEL_MAX].z - vModelMin.z) > GET_VEHICLE_HEIGHT_LIMIT_FOR_DROPOFF(deliveryData.eDropoffID)					
			SET_BIT(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET], FMC_DELIVERY_BITSET_VEH_ABOVE_HEIGHT_LIMIT)
			CDEBUG3LN(DEBUG_DELIVERY, "CUTSCENE_FMC_ASSET_SCENE_SETUP - Vehicle above height limit. Slow it down! Height: ", (deliveryData.cutsceneData.vectorArray[FMC_DELIVERY_VEC_ARRAY_PRIMARY_VEH_MODEL_MAX].z - vModelMin.z))
		ENDIF
	ENDIF
ENDPROC

PROC CUTSCENE_FMC_BASIC_VEH_SCENE(DELIVERY_DATA &deliveryData)
	//Check if we've attached any trailers since the script started
	VEHICLE_INDEX trailer	
	VECTOR vSceneVehStartPosition = FMC_SCENE_VEHICLE_START_POSITION(deliveryData, deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET])
	
//	IF IS_VECTOR_ZERO(vSceneVehStartPosition)
//		IF IS_ENTITY_ALIVE(deliverydata.deliveryVehicle)
//			vSceneVehStartPosition = GET_ENTITY_COORDS(deliverydata.deliveryVehicle)
//			CDEBUG1LN(DEBUG_DELIVERY, "CUTSCENE_FMC_ASSET_SCENE_SETUP - No efined start position. Selecting the delivery vehicle coord")
//		ENDIF
//	ENDIF
	
	#IF IS_DEBUG_BUILD
	VECTOR vehicleCoords
	#ENDIF

	IF GET_VEHICLE_TRAILER_VEHICLE(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle, trailer)
		IF DOES_ENTITY_EXIST(trailer)
		AND GET_ENTITY_MODEL(trailer) = TRAILERSMALL2
			CDEBUG1LN(DEBUG_DELIVERY, "CUTSCENE_FMC_ASSET_SCENE_SETUP - we have an AA trailer attached")
			SET_BIT(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET], FMC_DELIVERY_BITSET_AA_TRAILER_ATTACHED)
		ENDIF
	ELSE
		CDEBUG1LN(DEBUG_DELIVERY, "CUTSCENE_FMC_ASSET_SCENE_SETUP No attached trailer.")
	ENDIF
	
	//Setup any extra assets
	CUTSCENE_FMC_POSITION_LOADED_ASSETS(deliveryData)
	
	//Grab the model max for use later
	VECTOR vModelMin
	CHECK_VEHICLE_HEIGHT(deliveryData, vModelMin, deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle)
	
	//Check the length of the vehicle
	IF (deliveryData.cutsceneData.vectorArray[FMC_DELIVERY_VEC_ARRAY_PRIMARY_VEH_MODEL_MAX].y - vModelMin.y) > FMC_VEHICLE_SCENE_VEH_MAX_LENGTH						
		//bExtraLongVehicle = TRUE
		IF SHOULD_FMC_DELIVERY_SCENE_SKIP_SECOND_CUT(deliveryData.eDropoffID)
			SET_BIT(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET], FMC_DELIVERY_BITSET_VEH_ABOVE_LENGTH_LIMIT)
		ENDIF
		CDEBUG3LN(DEBUG_DELIVERY, "CUTSCENE_FMC_ASSET_SCENE_SETUP - Vehicle above Length limit: ", (deliveryData.cutsceneData.vectorArray[FMC_DELIVERY_VEC_ARRAY_PRIMARY_VEH_MODEL_MAX].y - vModelMin.y))
	ENDIF
	
	IF NOT IS_VECTOR_ZERO(vSceneVehStartPosition)
		#IF IS_DEBUG_BUILD
			vehicleCoords = GET_ENTITY_COORDS(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle)
			CDEBUG1LN(DEBUG_DELIVERY, "CUTSCENE_FMC_ASSET_SCENE_SETUP - Vehicle coords before warp: ", vehicleCoords)
		#ENDIF
		
		SET_ENTITY_COORDS(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle, vSceneVehStartPosition)
		SET_ENTITY_HEADING(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle, FMC_SCENE_VEHICLE_START_HEADING(deliveryData))
		SET_VEHICLE_ON_GROUND_PROPERLY(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle)
		
		#IF IS_DEBUG_BUILD
			vehicleCoords = GET_ENTITY_COORDS(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle)
			CDEBUG1LN(DEBUG_DELIVERY, "CUTSCENE_FMC_ASSET_SCENE_SETUP - Vehicle coords after warp: ", vehicleCoords)
		#ENDIF
	ELSE
		CDEBUG1LN(DEBUG_DELIVERY, "CUTSCENE_FMC_ASSET_SCENE_SETUP - Skip warp - No start position defined")
	ENDIF
	
	SET_VEHICLE_ENGINE_ON(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle, TRUE, TRUE)
	
	deliveryData.cutsceneData.vectorArray[FMC_DELIVERY_VEC_ARRAY_PRIMARY_VEH_MODEL_MAX].x = 0.0
	deliveryData.cutsceneData.vectorArray[FMC_DELIVERY_VEC_ARRAY_PRIMARY_VEH_MODEL_MAX].z = 0.0
	
	deliveryData.cutsceneData.floatArray[FMC_DELIVERY_FLOAT_MOVE_VEH_DISTANCE] = (deliveryData.cutsceneData.vectorArray[FMC_DELIVERY_VEC_ARRAY_PRIMARY_VEH_MODEL_MAX].y - vModelMin.y)
ENDPROC

PROC CUTSCENE_FMC_ARCADE_SETUP_LEAVE_VEH_SCENE(DELIVERY_DATA &deliveryData)

	IF IS_ENTITY_ALIVE(deliveryData.deliveryVehicle)
		VECTOR vehicleCoords = GET_ENTITY_COORDS(deliveryData.deliveryVehicle)
		FLOAT vehicleHeading = GET_ENTITY_HEADING(deliveryData.deliveryVehicle)
		CDEBUG1LN(DEBUG_DELIVERY, "CUTSCENE_FMC_ARCADE_SETUP_LEAVE_VEH_SCENE - Vehicle coords to warp to: ", vehicleCoords)
		
		SET_ENTITY_COORDS(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle, vehicleCoords)
		SET_ENTITY_HEADING(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle, vehicleHeading)
		SET_VEHICLE_ON_GROUND_PROPERLY(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle)
	ELSE
		SCRIPT_ASSERT("CUTSCENE_FMC_ARCADE_SETUP_LEAVE_VEH_SCENE - Dead delivery vehicle!")
	ENDIF
	
ENDPROC

PROC CUTSCENE_FMC_PREPARE_TRAILER_VEHICLE(VEHICLE_INDEX vPrimaryVehicle)
	IF NOT IS_ENTITY_ALIVE(vPrimaryVehicle)
		CDEBUG1LN(DEBUG_DELIVERY, "CUTSCENE_FMC_PREPARE_TRAILER_VEHICLE - No delivery vehicle is dead")
	ELIF IS_VEHICLE_ATTACHED_TO_TRAILER(vPrimaryVehicle)
		VEHICLE_INDEX trailer
		
		IF GET_VEHICLE_TRAILER_VEHICLE(vPrimaryVehicle, trailer)
		AND DOES_ENTITY_EXIST(trailer)
		AND IS_ENTITY_ALIVE(trailer)
			FREEZE_ENTITY_POSITION(trailer, FALSE)
			SET_ENTITY_COLLISION(trailer, TRUE)
			CDEBUG1LN(DEBUG_DELIVERY, "CUTSCENE_FMC_PREPARE_TRAILER_VEHICLE - Trailer vehicle successfully prepared for scene")
		ENDIF
	ELSE
		CDEBUG1LN(DEBUG_DELIVERY, "CUTSCENE_FMC_PREPARE_TRAILER_VEHICLE - No vehicle attached to the primary vehicle")
	ENDIF
ENDPROC

PROC CREATE_ON_FOOT_ANIMATED_SCENE(DELIVERY_DATA &deliveryData, PED_INDEX ped, BOOL bForceAI = FALSE)
	FMC_ON_FOOT_SCENE_DETAILS details
	details = FMC_GET_ON_FOOT_SCENE_DETAILS(deliveryData.eDropoffID)
	
	IF DOES_ENTITY_EXIST(ped)
	AND IS_ENTITY_ALIVE(ped)
		IF NOT FMC_DOES_ON_FOOT_SCENE_USE_ANIMATED_CAMERA(deliveryData.eDropoffID)
			//Using an animated camera means the scene will be created by the simple scene system
			deliveryData.cutsceneData.cutscene.iSyncSceneID = CREATE_SYNCHRONIZED_SCENE(details.vScenePosition, details.vSceneRotation)
		ENDIF
		TASK_SYNCHRONIZED_SCENE(ped, deliveryData.cutsceneData.cutscene.iSyncSceneID, details.sAnimDict, details.sAnimName, INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT)
		SET_SYNCHRONIZED_SCENE_PHASE(deliveryData.cutsceneData.cutscene.iSyncSceneID, details.fStartPhase)
		
		IF (bForceAI)
			FORCE_PED_AI_AND_ANIMATION_UPDATE(ped)
		ENDIF
	ELSE	
		SCRIPT_ASSERT("CREATE_ON_FOOT_ANIMATED_SCENE - Can't task player ped clone.")
	ENDIF
ENDPROC

//Will call once to position assets on the start of an x scene
PROC CUTSCENE_FMC_ASSET_SCENE_SETUP(DELIVERY_DATA &deliveryData, INT iScene)
	CDEBUG2LN(DEBUG_DELIVERY, "CUTSCENE_FMC_ASSET_SCENE_SETUP")
	
	SWITCH iScene
		CASE 0
			
			THEFEED_HIDE()
			THEFEED_PAUSE()
				
			SWITCH deliveryData.deliveryType
				CASE DT_VEHICLE
					SWITCH deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_SCENE_VARIATION]
						CASE FMC_DOCKS_SCENE
							IF IS_ENTITY_PLAYING_ANIM(deliveryData.cutsceneData.objectArray[FMC_DELIVERY_BOJECT_DOCKS_SCENE_CRANE_GRIP], "map_objects", "Dock_crane_SLD_load")
								//Because the request for this scene was only the third of three shots we need to have the crane ready to play its unload animation
								SET_ENTITY_ANIM_CURRENT_TIME(deliveryData.cutsceneData.objectArray[FMC_DELIVERY_BOJECT_DOCKS_SCENE_CRANE_GRIP], "map_objects", "Dock_crane_SLD_load", 0.99)
							ENDIF
						BREAK
						CASE FMC_VEH_BACKUP_INTO_SPACE_SCN
							CUTSCENE_HELP_PREPARE_VEHICLE_FOR_SCENE(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle, TRUE)					
							CUTSCENE_FMC_BASIC_VEH_SCENE(deliveryData)
							CUTSCENE_FMC_PREPARE_TRAILER_VEHICLE(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle)
						BREAK
						DEFAULT
							IF IS_ENTITY_ALIVE(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle)
								
								CUTSCENE_HELP_PREPARE_VEHICLE_FOR_SCENE(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle, TRUE)					
								CUTSCENE_FMC_BASIC_VEH_SCENE(deliveryData)
								
								IF FMC_IS_IN_VEHICLE_SCENE_EXIT_WALK(deliveryData.eDropoffID)
								OR FMC_SHOULD_DO_EXIT_LIFT_GARAGE_SCENE(deliveryData.eDropoffID)
									CUTSCENE_FMC_ARCADE_SETUP_LEAVE_VEH_SCENE(deliveryData)
								ENDIF	
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				
				CASE DT_FOOT
					deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_AUDIO_SOUND_ID] = -1
					
					IF SHOULD_DO_ON_FOOT_ANIMATED_SCENE(deliveryData)
						CREATE_ON_FOOT_ANIMATED_SCENE(deliveryData, deliveryData.cutsceneData.pedArray[0])
					ELSE
						IF IS_ENTITY_ALIVE(deliveryData.cutsceneData.pedArray[0])
							SET_ENTITY_COORDS(deliveryData.cutsceneData.pedArray[0], FMC_SCENE_GET_PED_WALK_TO_POSITION_INITIAL_COORD(deliveryData))
							SET_ENTITY_HEADING(deliveryData.cutsceneData.pedArray[0], FMC_SCENE_GET_PED_WALK_TO_POSITION_INITIAL_HEADING(deliveryData))
							TASK_FOLLOW_NAV_MESH_TO_COORD(deliveryData.cutsceneData.pedArray[0], FMC_SCENE_GET_PED_WALK_TO_POSITION(deliveryData), PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_NEVER_WARP)	
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		
	ENDSWITCH
	
	IF NOT IS_BIT_SET(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET], FMC_DELIVERY_BITSET_CUTSCENE_STARTED)
		SET_BIT(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET], FMC_DELIVERY_BITSET_CUTSCENE_STARTED)
		CDEBUG1LN(DEBUG_DELIVERY, "FMC_DELIVERY_BITSET_CUTSCENE_STARTED - TRUE")
	ENDIF
ENDPROC

PROC FMC_DELIVERY_CONTROL_VEH_CLONE(DELIVERY_DATA &deliveryData)
	IF NOT IS_ENTITY_ALIVE(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle)
		EXIT
	ENDIF

	FLOAT fTimeFactor, fCurrentSpeed
	
	INT iTotalSceneTime 			= deliveryData.cutsceneData.cutscene.sScenes[deliveryData.cutsceneData.cutscene.iCurrentScene].iDuration
	INT iElapsedTime 				= deliveryData.cutsceneData.cutscene.iCurrentSceneElapsedTime
	INT iMaxSceneToMoveForward 		= deliveryData.cutsceneData.cutscene.iScenesCount - 1
	INT iMaxTotalElapsedMoveForward = -1
		
	FLOAT fInitialSpeed 	= 1.75
	FLOAT fMaxForwardSpeed 	= 3.75
	
	IF IS_BIT_SET(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET], FMC_DELIVERY_BITSET_VEH_ABOVE_LENGTH_LIMIT)
		fInitialSpeed 		= 0.8
		fMaxForwardSpeed 	= 2.0
		iMaxSceneToMoveForward = deliveryData.cutsceneData.cutscene.iScenesCount	
	ENDIF
	
	VECTOR vCarFront = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle, deliveryData.cutsceneData.vectorArray[FMC_DELIVERY_VEC_ARRAY_PRIMARY_VEH_MODEL_MAX])
		
	IF deliveryData.cutsceneData.cutscene.iCurrentScene < iMaxSceneToMoveForward
		
		IF (((deliveryData.cutsceneData.cutscene.iCurrentScene) < iMaxSceneToMoveForward)		
		AND IS_BIT_SET(deliveryData.cutsceneData.cutscene.iBSScenesInit, deliveryData.cutsceneData.cutscene.iCurrentScene))
			INT i
			
			iTotalSceneTime = 0
			REPEAT iMaxSceneToMoveForward i
				iTotalSceneTime += deliveryData.cutsceneData.cutscene.sScenes[i].iDuration
				IF i < deliveryData.cutsceneData.cutscene.iCurrentScene
					iElapsedTime += deliveryData.cutsceneData.cutscene.sScenes[i].iDuration
				ENDIF
			ENDREPEAT
			
			IF iMaxTotalElapsedMoveForward > -1
			AND iTotalSceneTime > iMaxTotalElapsedMoveForward
				iTotalSceneTime = iMaxTotalElapsedMoveForward
			ENDIF
		ENDIF
		
		IF IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle))
		
			PED_INDEX pedDriver = GET_PED_IN_VEHICLE_SEAT(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle)
			
			IF pedDriver = NULL
				CDEBUG1LN(DEBUG_DELIVERY, "FMC_DELIVERY_CONTROL_VEH_CLONE - TASK_GO_TO_COORD_ANY_MEANS ped is null")
			ELIF NOT DOES_ENTITY_EXIST(pedDriver)
				CDEBUG1LN(DEBUG_DELIVERY, "FMC_DELIVERY_CONTROL_VEH_CLONE - TASK_GO_TO_COORD_ANY_MEANS ped is non-existent")
			ENDIF
			
			IF NOT IS_BIT_SET(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET], FMC_DELIVERY_BITSET_TASKED_MOTORBIKE)
				VECTOR vGoto = FMC_SCENE_VEHICLE_END_POSITION(deliveryData)
				
				SET_VEHICLE_MAX_SPEED(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle, 2.0)				
				TASK_GO_TO_COORD_ANY_MEANS(pedDriver, vGoto, 1.0, deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle, FALSE, DRIVINGMODE_PLOUGHTHROUGH)
				SET_BIT(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET], FMC_DELIVERY_BITSET_TASKED_MOTORBIKE) 
			ENDIF
			
			IF FMC_SHOULD_SCENE_DELETE_PED_ON_BIKE_IF_VEH_EXIT(deliveryData.eDropoffID)
				SET_BIT(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET], FMC_DELIVERY_BITSET_DELETE_PED_ON_VEHICLE_EXIT)
			ENDIF
			
			fCurrentSpeed = GET_ENTITY_SPEED(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle)
		ELSE
			//Move the car forward
			fTimeFactor = FMIN(TO_FLOAT(iElapsedTime) / TO_FLOAT(iTotalSceneTime), 1.0)
			fCurrentSpeed = fInitialSpeed + GET_GRAPH_TYPE_ACCEL(fTimeFactor) * (fMaxForwardSpeed - fInitialSpeed)
			SET_VEHICLE_HANDBRAKE(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle, FALSE)
			SET_VEHICLE_BRAKE(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle, 0.0)
			SET_VEHICLE_FORWARD_SPEED(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle, fCurrentSpeed)
		ENDIF
		
		// Monitor if front of the car is about to hit the garage, if so move to the last scene, save the speed and position so we can maintain it
		VECTOR AreaA, AreaB
		FLOAT fWidth
		FMC_SCENE_GET_VEHICLE_DRIVE_FORWARD_LIMIT_AREA(deliveryData.eDropoffID, AreaA, AreaB, fWidth)
		IF IS_POINT_IN_ANGLED_AREA(vCarFront, AreaA, AreaB, fWidth)
			IF IS_BIT_SET(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET], FMC_DELIVERY_BITSET_VEH_ABOVE_LENGTH_LIMIT)
				DO_SCREEN_FADE_OUT(CONST_DELIVERY_END_SCREN_FADE_DURATION * 2)
			ELSE
				PRINTLN("[CUTSCENE] FMC_DELIVERY_CONTROL_VEH_CLONE - Front of the car was about to hit the door, moving to last scene with ID: ", (deliveryData.cutsceneData.cutscene.iScenesCount - 1))		
				
				deliveryData.cutsceneData.cutscene.iCurrentScene 				= deliveryData.cutsceneData.cutscene.iScenesCount - 1
				deliveryData.cutsceneData.cutscene.iCurrentSceneElapsedTime 	= 0
				
				deliveryData.cutsceneData.floatArray[FMC_DELIVERY_FLOAT_ARRAY_VEH_SPEED] 			= fCurrentSpeed
				deliveryData.cutsceneData.vectorArray[FMC_DELIVERY_VEC_ARRAY_SAVED_VEH_COORD] 		= GET_ENTITY_COORDS(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle)
			ENDIF
		ENDIF
			
	ELSE
		VECTOR vStartPos, vEndPos
		VECTOR vDir = GET_HEADING_AS_VECTOR(GET_ENTITY_HEADING(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle))
		FLOAT fLastRecordedSpeed = fMaxForwardSpeed
		
		IF deliveryData.cutsceneData.floatArray[FMC_DELIVERY_FLOAT_ARRAY_VEH_SPEED] > 0.0
			vStartPos = deliveryData.cutsceneData.vectorArray[FMC_DELIVERY_VEC_ARRAY_SAVED_VEH_COORD]
			fLastRecordedSpeed = deliveryData.cutsceneData.floatArray[FMC_DELIVERY_FLOAT_ARRAY_VEH_SPEED]
			FLOAT fDistToMoveCar = (deliveryData.cutsceneData.floatArray[FMC_DELIVERY_FLOAT_MOVE_VEH_DISTANCE] * 1.48)
			vEndPos = vStartPos + vDir * fDistToMoveCar
			FLOAT fTimeToMove = (fDistToMoveCar / fLastRecordedSpeed) * 1000.0
			fTimeFactor = FMIN(TO_FLOAT(deliveryData.cutsceneData.cutscene.iCurrentSceneElapsedTime) / fTimeToMove, 1.0)
		ELSE
			fTimeFactor = FMIN(TO_FLOAT(deliveryData.cutsceneData.cutscene.iCurrentSceneElapsedTime) / TO_FLOAT(iTotalSceneTime), 1.0)
			vStartPos	= FMC_SCENE_VEHICLE_START_POSITION(deliveryData, deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET])
			vEndPos		= FMC_SCENE_VEHICLE_END_POSITION(deliveryData)
			// Calculate endpos to maintain last speed
			vEndPos = vStartPos + vDir * (TO_FLOAT(iTotalSceneTime) / 1000.0) * fLastRecordedSpeed
		ENDIF
		
		VECTOR vTotalOffset = vEndPos - vStartPos
		VECTOR vCurrentPos = vStartPos + vTotalOffset * GET_GRAPH_TYPE_DECEL(fTimeFactor)
		FLOAT fCloneHeading = FMC_SCENE_VEHICLE_START_HEADING(deliveryData)
		
		IF NOT IS_BIT_SET(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET], FMC_DELIVERY_BITSET_FROZEN_CLONE)
			SET_ENTITY_COLLISION(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle, FALSE)
			SET_ENTITY_COORDS(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle, vCurrentPos) // This will set it on ground properly
			SET_ENTITY_ROTATION(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle, <<0.0, 0.0, fCloneHeading>>)
			FREEZE_ENTITY_POSITION(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle, TRUE)
			
			VECTOR vCurrentCoords = GET_ENTITY_COORDS(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle)
			deliveryData.cutsceneData.floatArray[FMC_DELIVERY_FLOAT_ARRAY_VEH_Z_COORD] = vCurrentCoords.Z
			SET_BIT(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET], FMC_DELIVERY_BITSET_FROZEN_CLONE)
		ENDIF
		
		SET_ENTITY_COORDS_NO_OFFSET(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle, <<vCurrentPos.X, vCurrentPos.Y, deliveryData.cutsceneData.floatArray[FMC_DELIVERY_FLOAT_ARRAY_VEH_Z_COORD]>>)
		
		IF IS_BIT_SET(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET], FMC_DELIVERY_BITSET_DELETE_PED_ON_VEHICLE_EXIT)
			IF IS_ENTITY_ALIVE(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].passengers[FMC_DELIVERY_PED_ARRAY_PRIMARY_PED])
				IF GET_IS_TASK_ACTIVE(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].passengers[FMC_DELIVERY_PED_ARRAY_PRIMARY_PED], CODE_TASK_EXIT_VEHICLE)
					DELETE_PED(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].passengers[FMC_DELIVERY_PED_ARRAY_PRIMARY_PED])
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC FMC_DELIVERY_CONTROL_SINGLE_SHOT_VEH_CLONE_SCENE(DELIVERY_DATA &deliveryData)
	IF NOT IS_ENTITY_ALIVE(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle)
		EXIT
	ENDIF

	FLOAT fTimeFactor, fCurrentSpeed
	
	INT iTotalSceneTime 			= deliveryData.cutsceneData.cutscene.sScenes[deliveryData.cutsceneData.cutscene.iCurrentScene].iDuration
	INT iElapsedTime 				= deliveryData.cutsceneData.cutscene.iCurrentSceneElapsedTime
	INT iMaxSceneToMoveForward 		= deliveryData.cutsceneData.cutscene.iScenesCount - 1
	INT iMaxTotalElapsedMoveForward = -1
		
	FLOAT fInitialSpeed 	= 1.75
	FLOAT fMaxForwardSpeed 	= 3.75
	
	IF IS_BIT_SET(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET], FMC_DELIVERY_BITSET_VEH_ABOVE_LENGTH_LIMIT)
	OR (IS_BIT_SET(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET], FMC_DELIVERY_BITSET_VEH_ABOVE_HEIGHT_LIMIT) AND SHOULD_SLOW_DOWN_VEHICLE_FOR_THIS_DROPOFF(deliveryData.eDropoffID))
		fInitialSpeed 		= 0.8
		fMaxForwardSpeed 	= 2.0
		iMaxSceneToMoveForward = deliveryData.cutsceneData.cutscene.iScenesCount	
	ENDIF
	
	VECTOR vCarFront = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle, deliveryData.cutsceneData.vectorArray[FMC_DELIVERY_VEC_ARRAY_PRIMARY_VEH_MODEL_MAX])
		
	IF NOT IS_BIT_SET(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET], FMC_DELIVERY_BITSET_DONE_DRIVE_IN_FIRST_STAGE)
		
		IF (((deliveryData.cutsceneData.cutscene.iCurrentScene) < iMaxSceneToMoveForward)		
		AND IS_BIT_SET(deliveryData.cutsceneData.cutscene.iBSScenesInit, deliveryData.cutsceneData.cutscene.iCurrentScene))
			INT i
			
			iTotalSceneTime = 0
			REPEAT iMaxSceneToMoveForward i
				iTotalSceneTime += deliveryData.cutsceneData.cutscene.sScenes[i].iDuration
				IF i < deliveryData.cutsceneData.cutscene.iCurrentScene
					iElapsedTime += deliveryData.cutsceneData.cutscene.sScenes[i].iDuration
				ENDIF
			ENDREPEAT
			
			IF iMaxTotalElapsedMoveForward > -1
			AND iTotalSceneTime > iMaxTotalElapsedMoveForward
				iTotalSceneTime = iMaxTotalElapsedMoveForward
			ENDIF
		ENDIF
		
		IF IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle))
		
			PED_INDEX pedDriver = GET_PED_IN_VEHICLE_SEAT(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle)
			
			IF pedDriver = NULL
				CDEBUG1LN(DEBUG_DELIVERY, "FMC_DELIVERY_CONTROL_VEH_CLONE - TASK_GO_TO_COORD_ANY_MEANS ped is null")
			ELIF NOT DOES_ENTITY_EXIST(pedDriver)
				CDEBUG1LN(DEBUG_DELIVERY, "FMC_DELIVERY_CONTROL_VEH_CLONE - TASK_GO_TO_COORD_ANY_MEANS ped is non-existent")
			ENDIF
			
			IF NOT IS_BIT_SET(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET], FMC_DELIVERY_BITSET_TASKED_MOTORBIKE)
				VECTOR vGoto = FMC_SCENE_VEHICLE_END_POSITION(deliveryData)
				
				SET_VEHICLE_MAX_SPEED(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle, 2.0)				
				TASK_GO_TO_COORD_ANY_MEANS(pedDriver, vGoto, 1.0, deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle, FALSE, DRIVINGMODE_PLOUGHTHROUGH)
				SET_BIT(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET], FMC_DELIVERY_BITSET_TASKED_MOTORBIKE) 
			ENDIF
			
			IF FMC_SHOULD_SCENE_DELETE_PED_ON_BIKE_IF_VEH_EXIT(deliveryData.eDropoffID)
				SET_BIT(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET], FMC_DELIVERY_BITSET_DELETE_PED_ON_VEHICLE_EXIT)
			ENDIF
			
			fCurrentSpeed = GET_ENTITY_SPEED(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle)
		ELSE
			//Move the car forward
			fTimeFactor = FMIN(TO_FLOAT(iElapsedTime) / TO_FLOAT(iTotalSceneTime), 1.0)
			fCurrentSpeed = fInitialSpeed + GET_GRAPH_TYPE_ACCEL(fTimeFactor) * (fMaxForwardSpeed - fInitialSpeed)
			SET_VEHICLE_HANDBRAKE(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle, FALSE)
			SET_VEHICLE_BRAKE(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle, 0.0)
			SET_VEHICLE_FORWARD_SPEED(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle, fCurrentSpeed)
		ENDIF
		
		// Monitor if front of the car is about to hit the garage, if so move to the last scene, save the speed and position so we can maintain it
		VECTOR AreaA, AreaB
		FLOAT fWidth
		FMC_SCENE_GET_VEHICLE_DRIVE_FORWARD_LIMIT_AREA(deliveryData.eDropoffID, AreaA, AreaB, fWidth)
		
		IF IS_POINT_IN_ANGLED_AREA(vCarFront, AreaA, AreaB, fWidth)
			PRINTLN("[CUTSCENE] FMC_DELIVERY_CONTROL_VEH_CLONE - Front of the car was about to hit the door, moving to last scene with ID: ", (deliveryData.cutsceneData.cutscene.iScenesCount - 1), " elapsed time: ", deliveryData.cutsceneData.cutscene.iCurrentSceneElapsedTime)		
			
			SET_BIT(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET], FMC_DELIVERY_BITSET_DONE_DRIVE_IN_FIRST_STAGE)
			
			deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_DRIVE_IN_SCENE_FIRST_STAGE_SCENE_END]	= deliveryData.cutsceneData.cutscene.iCurrentSceneElapsedTime
			deliveryData.cutsceneData.floatArray[FMC_DELIVERY_FLOAT_ARRAY_VEH_SPEED] 					= fCurrentSpeed
			deliveryData.cutsceneData.vectorArray[FMC_DELIVERY_VEC_ARRAY_SAVED_VEH_COORD] 				= GET_ENTITY_COORDS(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle)
		ENDIF
			
	ELSE
		VECTOR vStartPos, vEndPos
		VECTOR vDir = GET_HEADING_AS_VECTOR(GET_ENTITY_HEADING(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle))
		FLOAT fLastRecordedSpeed = fMaxForwardSpeed
		
		IF deliveryData.cutsceneData.floatArray[FMC_DELIVERY_FLOAT_ARRAY_VEH_SPEED] > 0.0
			vStartPos = deliveryData.cutsceneData.vectorArray[FMC_DELIVERY_VEC_ARRAY_SAVED_VEH_COORD]
			fLastRecordedSpeed = deliveryData.cutsceneData.floatArray[FMC_DELIVERY_FLOAT_ARRAY_VEH_SPEED]
			
			FLOAT fDistToMoveCar = (deliveryData.cutsceneData.floatArray[FMC_DELIVERY_FLOAT_MOVE_VEH_DISTANCE] * 1.48)
			vEndPos = vStartPos + vDir * fDistToMoveCar
			
			FLOAT fTimeToMove = (fDistToMoveCar / fLastRecordedSpeed) * 1000.0
			INT iSecondStageElapsedTime = (deliveryData.cutsceneData.cutscene.iCurrentSceneElapsedTime - deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_DRIVE_IN_SCENE_FIRST_STAGE_SCENE_END])
			fTimeFactor = FMIN(TO_FLOAT(iSecondStageElapsedTime) / fTimeToMove, 1.0)
		ELSE
			fTimeFactor = FMIN(TO_FLOAT(deliveryData.cutsceneData.cutscene.iCurrentSceneElapsedTime) / TO_FLOAT(iTotalSceneTime), 1.0)
			vStartPos	= FMC_SCENE_VEHICLE_START_POSITION(deliveryData, deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET])
			vEndPos		= FMC_SCENE_VEHICLE_END_POSITION(deliveryData)
			// Calculate endpos to maintain last speed
			vEndPos = vStartPos + vDir * (TO_FLOAT(iTotalSceneTime) / 1000.0) * fLastRecordedSpeed
		ENDIF
		
		VECTOR vTotalOffset = vEndPos - vStartPos
		VECTOR vCurrentPos = vStartPos + vTotalOffset * GET_GRAPH_TYPE_DECEL(fTimeFactor)
		FLOAT fCloneHeading = FMC_SCENE_VEHICLE_START_HEADING(deliveryData)
		
		IF NOT IS_BIT_SET(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET], FMC_DELIVERY_BITSET_FROZEN_CLONE)
			SET_ENTITY_COLLISION(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle, FALSE)
			SET_ENTITY_COORDS(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle, vCurrentPos) // This will set it on ground properly
			SET_ENTITY_ROTATION(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle, <<0.0, 0.0, fCloneHeading>>)
			FREEZE_ENTITY_POSITION(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle, TRUE)
			
			VECTOR vCurrentCoords = GET_ENTITY_COORDS(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle)
			deliveryData.cutsceneData.floatArray[FMC_DELIVERY_FLOAT_ARRAY_VEH_Z_COORD] = vCurrentCoords.Z
			SET_BIT(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET], FMC_DELIVERY_BITSET_FROZEN_CLONE)
		ENDIF
		
		SET_ENTITY_COORDS_NO_OFFSET(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle, <<vCurrentPos.X, vCurrentPos.Y, deliveryData.cutsceneData.floatArray[FMC_DELIVERY_FLOAT_ARRAY_VEH_Z_COORD]>>)
		
		IF IS_BIT_SET(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET], FMC_DELIVERY_BITSET_DELETE_PED_ON_VEHICLE_EXIT)
			IF IS_ENTITY_ALIVE(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].passengers[FMC_DELIVERY_PED_ARRAY_PRIMARY_PED])
				IF GET_IS_TASK_ACTIVE(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].passengers[FMC_DELIVERY_PED_ARRAY_PRIMARY_PED], CODE_TASK_EXIT_VEHICLE)
					DELETE_PED(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].passengers[FMC_DELIVERY_PED_ARRAY_PRIMARY_PED])
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CUTSCENE_FMC_TASK_PEDS_TO_LEAVE_VEHICLE(DELIVERY_DATA &deliveryData, BOOL bOnlyNonPlayers = FALSE)
	PED_INDEX ped
	INT indexVehiclePack
	INT indexPassenger
	VECTOR vWalkToPlayer = FMC_SCENE_GET_PED_WALK_TO_POSITION(deliveryData)
	
	IF deliveryData.cutsceneData.cutscene.iCurrentSceneElapsedTime < 500
		IF IS_ENTITY_ALIVE(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle)
			CDEBUG3LN(DEBUG_DELIVERY, "CUTSCENE_FMC_TASK_PEDS_TO_LEAVE_VEHICLE - Freezing vehicle temporarily")
			FREEZE_ENTITY_POSITION(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle, TRUE)
			EXIT
		ELSE
			FREEZE_ENTITY_POSITION(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle, FALSE)
		ENDIF
	ENDIF
		
	REPEAT CONST_DELIVERY_MAX_VEHICLES_PACK indexVehiclePack
		REPEAT MAX_NO_VEH_PASSENGERS indexPassenger
			ped = deliveryData.cutsceneData.vehiclePackArray[indexVehiclePack].passengers[indexPassenger]
			INT iExitTime = (FMC_TIME_MULTI_FOR_LEAVE_VEHICLE * indexPassenger) + FMC_TIME_FOR_LEAVE_VEHICLE
			
			IF IS_ENTITY_ALIVE(ped)
				
				IF bOnlyNonPlayers
					MODEL_NAMES pedModel = GET_ENTITY_MODEL(ped)
					IF pedModel = MP_M_FREEMODE_01
					OR pedModel = MP_F_FREEMODE_01
						RELOOP
					ENDIF
				ENDIF
				
				IF deliveryData.cutsceneData.cutscene.iCurrentScene >= GET_FMC_SCENE_FOR_LEAVE_VEHICLE(deliveryData)
					IF deliveryData.cutsceneData.cutscene.iTotalElapsedTime > iExitTime
						IF GET_SCRIPT_TASK_STATUS(ped, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK	
							TASK_PED_TO_LEAVE_VEH_AND_FOLLOW_NAVMESH(ped, vWalkToPlayer)
						ENDIF
						
						IF NOT IS_PED_IN_ANY_VEHICLE(ped)
							SET_BIT(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET], FMC_DELIVERY_BITSET_EXITED_VEHICLE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDREPEAT
ENDPROC

PROC CUTSCENE_FMC_MAINTAIN_EXIT_LIFT_GARAGE_SCENE(DELIVERY_DATA &deliveryData)
	PED_INDEX ped
	INT indexVehiclePack
	INT indexPassenger
	INT iCurrentScene = deliveryData.cutsceneData.cutscene.iCurrentScene
		
	REPEAT CONST_DELIVERY_MAX_VEHICLES_PACK indexVehiclePack
		REPEAT MAX_NO_VEH_PASSENGERS indexPassenger
			ped = deliveryData.cutsceneData.vehiclePackArray[indexVehiclePack].passengers[indexPassenger]
			
			IF IS_ENTITY_ALIVE(ped)
				MODEL_NAMES pedModel = GET_ENTITY_MODEL(ped)
				IF pedModel = MP_M_FREEMODE_01
				OR pedModel = MP_F_FREEMODE_01
					IF NOT IS_PED_IN_ANY_VEHICLE(ped)
					AND iCurrentScene = 1
					AND IS_BIT_SET(deliveryData.cutsceneData.cutscene.iBSScenesInit, iCurrentScene)
						IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(deliveryData.cutsceneData.cutscene.iSyncSceneID)
							
							// Hide vehicle so ped can perform garage anim (in case vehicle is blocking position)
							IF IS_ENTITY_ALIVE(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle)
								SET_ENTITY_VISIBLE(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle, FALSE)
								SET_ENTITY_COLLISION(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle, FALSE)
								FREEZE_ENTITY_POSITION(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle, TRUE)
							ENDIF
							
							CLEAR_PED_TASKS_IMMEDIATELY(ped)
							CREATE_ON_FOOT_ANIMATED_SCENE(deliveryData, ped, TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDREPEAT
ENDPROC

PROC FMC_SCENE_CONCEAL_ENTRY_VEHICLE(DELIVERY_DATA &deliveryData, BOOL bExcludeDriver = FALSE, BOOL bPreventAutoShuffle = FALSE, BOOL bConceal = TRUE)
	IF DOES_ENTITY_EXIST(deliveryData.deliveryVehicle)
	AND NOT IS_ENTITY_DEAD(deliveryData.deliveryVehicle)
		IF (IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND GET_PED_VEHICLE_SEAT(PLAYER_PED_ID(), deliveryData.deliveryVehicle) != VS_DRIVER)
		OR NOT bExcludeDriver
			IF bConceal 
				IF NOT NETWORK_IS_ENTITY_CONCEALED(deliveryData.deliveryVehicle)
					NETWORK_CONCEAL_ENTITY(deliveryData.deliveryVehicle, bConceal)
					SET_BIT(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET], FMC_DELIVERY_BITSET_CONCEALED_VEHICLE)
					TOGGLE_DELIVERY_FLAG_EXTRA(deliveryData, DELIVERY_FLAG_EXTRA_LET_CHILD_SCRIPT_UNCONCEAL_VEHICLE, TRUE)
				ENDIF
			ELSE
				IF NETWORK_IS_ENTITY_CONCEALED(deliveryData.deliveryVehicle)
					NETWORK_CONCEAL_ENTITY(deliveryData.deliveryVehicle, bConceal)
				ENDIF
			ENDIF
		ENDIF
		IF bPreventAutoShuffle
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PreventAutoShuffleToDriversSeat, TRUE)
		ENDIF
	ELSE
		PRINTLN("FMC_SCENE_CONCEAL_ENTRY_VEHICLE - Failed to conceal delivery vehicle DOES_ENTITY_EXIST = ", DOES_ENTITY_EXIST(deliveryData.deliveryVehicle))
	ENDIF
ENDPROC

PROC CUTSCENE_FMC_ADD_AMBIENT_PEDS_TO_SCENE(DELIVERY_DATA &deliveryData)
	IF NOT IS_BIT_SET(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET], FMC_DELIVERY_BITSET_ADDED_AMBIENT_PEDS)
	AND FMC_SHOULD_SCENE_INCLUDE_NEARBY_AMBIENT_PEDS(deliveryData.eDropoffID)
		PED_INDEX tmpArray[50]
				
		INT npeds = GET_PED_NEARBY_PEDS(PLAYER_PED_ID(), tmpArray)
		
		INT i
		REPEAT npeds i
			IF DOES_ENTITY_EXIST(tmpArray[i])
			AND IS_ENTITY_ALIVE(tmpArray[i])
			AND IS_PED_ACTIVE_IN_SCENARIO(tmpArray[i])
				SET_ENTITY_VISIBLE_IN_CUTSCENE(tmpArray[i], TRUE, TRUE)
			ENDIF
		ENDREPEAT
		
		SET_BIT(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET], FMC_DELIVERY_BITSET_ADDED_AMBIENT_PEDS)
	ENDIF
ENDPROC

PROC CUTSCENE_FMC_MAINTAIN_VEHICLE_COLLISIONS(DELIVERY_DATA &deliveryData)
		
	IF IS_ENTITY_ALIVE(deliveryData.deliveryVehicle)
	AND IS_ENTITY_ALIVE(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle)
		//Primary vehicle
		SET_ENTITY_NO_COLLISION_ENTITY(deliveryData.deliveryVehicle, deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle, FALSE)
		SET_ENTITY_LOCALLY_INVISIBLE(deliveryData.deliveryVehicle)
		
		//Trailer vehicle
		IF IS_ENTITY_ALIVE(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_DUMMY_VEH].vehicle)
			//Clone
			SET_ENTITY_NO_COLLISION_ENTITY(deliveryData.deliveryVehicle, deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_DUMMY_VEH].vehicle, FALSE)
			FREEZE_ENTITY_POSITION(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_DUMMY_VEH].vehicle, FALSE)
			SET_ENTITY_COLLISION(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_DUMMY_VEH].vehicle, TRUE)
			
			//Mission Entity
			IF IS_ENTITY_ALIVE(deliveryData.deliveryVehicleAttached)
				SET_ENTITY_NO_COLLISION_ENTITY(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle, deliveryData.deliveryVehicleAttached, FALSE)
				SET_ENTITY_NO_COLLISION_ENTITY(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_DUMMY_VEH].vehicle, deliveryData.deliveryVehicleAttached, FALSE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_FMC_DOCKS_SCENE(DELIVERY_DATA &deliveryData)
	VECTOR vObjectStartPos = << 984.455, -2883.53, 29.8977 >> 
	VECTOR vObjectEndPos = << 984.455, -2883.53, 24.002 >>
	INT iDescentDuration = 4200
	
	IF NOT IS_BIT_SET(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET], FMC_DELIVERY_BITSET_STARTED_AUDIO_STREAM)
		_FMC_DELIVERY_PLAY_AUDIO_STREAM("DLC_IE_Delivery_Docks_Stream")
		SET_BIT(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET], FMC_DELIVERY_BITSET_STARTED_AUDIO_STREAM)
	ENDIF
	
	IF deliveryData.cutsceneData.cutscene.iTotalElapsedTime <= iDescentDuration
		INT iTransitionTime = deliveryData.cutsceneData.cutscene.iTotalElapsedTime
		FLOAT fTimeFactor = FMIN(TO_FLOAT(iTransitionTime) / TO_FLOAT(iDescentDuration), 1.0)
		
		VECTOR vPropOffset = vObjectEndPos - vObjectStartPos
		VECTOR vPropPos = vObjectStartPos + (vPropOffset * fTimeFactor)
		SET_ENTITY_COORDS(deliveryData.cutsceneData.objectArray[FMC_DELIVERY_BOJECT_DOCKS_SCENE_CRANE_GRIP], vPropPos)
	ELSE
		IF NOT IS_BIT_SET(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET], FMC_DELIVERY_BITSET_PLAYED_DOCKS_CRANE_RELEASE_ANIM)
			PLAY_ENTITY_ANIM(deliveryData.cutsceneData.objectArray[FMC_DELIVERY_BOJECT_DOCKS_SCENE_CRANE_GRIP], "Dock_crane_SLD_unload", "map_objects", INSTANT_BLEND_IN, FALSE, TRUE)
			SET_BIT(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET], FMC_DELIVERY_BITSET_PLAYED_DOCKS_CRANE_RELEASE_ANIM)
		ENDIF
	ENDIF
ENDPROC

DEBUGONLY FUNC BOOL SHOULD_FMC_SCENE_DISPLAY_PLACEHOLDER_MESSAGE(FREEMODE_DELIVERY_DROPOFFS eDropoffID)
	SWITCH eDropoffID
		CASE FMC_DROPOFF_SOLOMONS_OFFICE	RETURN FALSE	BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

DEBUGONLY PROC DISPLAY_PLACEHOLDER_MESSAGE(FREEMODE_DELIVERY_DROPOFFS eDropoffID)
	IF SHOULD_FMC_SCENE_DISPLAY_PLACEHOLDER_MESSAGE(eDropoffID)
		SET_TEXT_SCALE(0.6, 0.6)
		SET_TEXT_COLOUR(255, 255, 255, 255)
		SET_TEXT_OUTLINE()
		#IF IS_DEBUG_BUILD
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.1, 0.1, "STRING", "Placeholder delivery scene")
		#ENDIF
	ENDIF
ENDPROC

PROC CUTSCENE_FMC_TASK_BACK_VEH_UP(DELIVERY_DATA &deliveryData)

	IF IS_ENTITY_ALIVE(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle)
		
		VECTOR vGoto 			= GET_FMC_VEHICLE_DELIVERY_SCENE_VEH_END_POS(deliveryData.eDropoffID)
		VECTOR vCurrentPosition = GET_ENTITY_COORDS(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle)
		FLOAT fDist				= VDIST(vGoto, vCurrentPosition)
		FLOAT fMaxSpeed			= 2.5
		
		IF deliveryData.cutsceneData.floatArray[FMC_DELIVERY_FLOAT_MOVE_VEH_DISTANCE] = 0.0
			//Set the max distance so we can check this against where the vehicle has moved to
			deliveryData.cutsceneData.floatArray[FMC_DELIVERY_FLOAT_MOVE_VEH_DISTANCE] = fDist
		ENDIF
		
		FLOAT fTargetSpeed = (fMaxSpeed * (fDist/deliveryData.cutsceneData.floatArray[FMC_DELIVERY_FLOAT_MOVE_VEH_DISTANCE]))
		
		//Set the value negative so we reverse
		fTargetSpeed *= -1
		
		SET_VEHICLE_BRAKE_LIGHTS(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle, FALSE)
		SET_VEHICLE_BRAKE(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle, 0.0)
		SET_VEHICLE_HANDBRAKE(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle, FALSE)
		SET_VEHICLE_FORWARD_SPEED(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle, fTargetSpeed)
		SET_VEHICLE_FORCE_REVERSE_WARNING(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle, TRUE)
	ENDIF
		
//			Implement the below for reversing without an attached traier
//			PED_INDEX ped = GET_PED_IN_VEHICLE_SEAT(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle)
//			IF IS_ENTITY_ALIVE(ped)
//				TASK_VEHICLE_DRIVE_TO_COORD(ped, deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle, vGoto, 1.4, DRIVINGSTYLE_REVERSING, DUMMY_MODEL_FOR_SCRIPT, DF_DriveInReverse, fGotoTargetRadius, -1)
//				SET_BIT(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET], FMC_DELIVERY_BITSET_TASKED_VEH_REVERSE)
//			ENDIF
ENDPROC

PROC DO_CUSTOM_PED_SETUP(PED_INDEX pedId)
	IF IS_ENTITY_ALIVE(pedId)
	AND GET_ENTITY_MODEL(pedId) = INT_TO_ENUM(MODEL_NAMES, HASH("IG_LILDEE"))
		SET_PED_PROP_INDEX(pedId, ANCHOR_HEAD, 0)
		SET_PED_PROP_INDEX(pedId, ANCHOR_EYES, 0)
		
		SET_PED_COMPONENT_VARIATION(pedId, PED_COMP_HEAD, 0, 0)
		SET_PED_COMPONENT_VARIATION(pedId, PED_COMP_TORSO, 0, 0)
		SET_PED_COMPONENT_VARIATION(pedId, PED_COMP_LEG, 0, 0)
		SET_PED_COMPONENT_VARIATION(pedId, PED_COMP_SPECIAL, 0, 0)
	ENDIF
ENDPROC

PROC CUTSCENE_FMC_MAINTAIN_PASSENGER_DROPOFF_SCENE(DELIVERY_DATA &deliveryData)
	CUTSCENE_FMC_TASK_PEDS_TO_LEAVE_VEHICLE(deliveryData, TRUE)
	
	IF NOT IS_BIT_SET(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET], FMC_DELIVERY_BITSET_SETUP_CUSTOM_PED)
		DO_CUSTOM_PED_SETUP(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].passengers[FMC_DELIVERY_PED_ARRAY_PRIMARY_PASSENGER])
		SET_BIT(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET], FMC_DELIVERY_BITSET_SETUP_CUSTOM_PED)
	ENDIF
	
	IF NOT IS_BIT_SET(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET], FMC_DELIVERY_BITSET_PLAYED_CUSTOM_SPEECH)
	AND deliveryData.cutsceneData.cutscene.iCurrentScene = 1
	AND IS_ENTITY_ALIVE(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].passengers[FMC_DELIVERY_PED_ARRAY_PRIMARY_PASSENGER])
		PLAY_PED_AMBIENT_SPEECH(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].passengers[FMC_DELIVERY_PED_ARRAY_PRIMARY_PASSENGER], "GENERIC_BYE", SPEECH_PARAMS_FORCE_FRONTEND)
		CDEBUG3LN(DEBUG_DELIVERY, "CUTSCENE_FMC_MAINTAIN_PASSENGER_DROPOFF_SCENE - Play speech - SPEECH_PARAMS_FORCE_FRONTEND")
		SET_BIT(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET], FMC_DELIVERY_BITSET_PLAYED_CUSTOM_SPEECH)
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET], FMC_DELIVERY_BITSET_TASKED_LOOK_AT)
		IF IS_ENTITY_ALIVE(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].passengers[FMC_DELIVERY_PED_ARRAY_PRIMARY_PED])
		AND IS_ENTITY_ALIVE(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].passengers[FMC_DELIVERY_PED_ARRAY_PRIMARY_PASSENGER])			
			IF GET_SCRIPT_TASK_STATUS(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].passengers[FMC_DELIVERY_PED_ARRAY_PRIMARY_PASSENGER], SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK	
				TASK_LOOK_AT_COORD(deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].passengers[FMC_DELIVERY_PED_ARRAY_PRIMARY_PED],
									<<404.0702, -989.6832, 29.9709>>, 3000, SLF_WIDEST_YAW_LIMIT | SLF_FAST_TURN_RATE, SLF_LOOKAT_VERY_HIGH)
				
				SET_BIT(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET], FMC_DELIVERY_BITSET_TASKED_LOOK_AT)
				CDEBUG2LN(DEBUG_DELIVERY, "CUTSCENE_FMC_MAINTAIN_PASSENGER_DROPOFF_SCENE - Tasked look at")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CUTSCENE_FMC_MAINTAIN_SCENE(DELIVERY_DATA &deliveryData)
			
	IF IS_SCREEN_FADED_OUT()
		TOGGLE_DELIVERY_FLAG_EXTRA(deliveryData, DELIVERY_FLAG_EXTRA_SHOW_SHARD, FALSE)
	ELSE
		TOGGLE_DELIVERY_FLAG_EXTRA(deliveryData, DELIVERY_FLAG_EXTRA_SHOW_SHARD, TRUE)
	ENDIF
	
	DISPLAY_PLACEHOLDER_MESSAGE(deliveryData.eDropoffID)
	
	IF NOT IS_BIT_SET(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET], FMC_DELIVERY_BITSET_DONE_MISSISON_SPECIFIC_CHECK)
		FMC_SCENE_DO_MISSION_SPECIFIC_DELIVERY_REQUIREMENT_CHECK(deliveryData)
		
		SET_BIT(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET], FMC_DELIVERY_BITSET_DONE_MISSISON_SPECIFIC_CHECK)
	ENDIF
	
	CUTSCENE_FMC_ADD_AMBIENT_PEDS_TO_SCENE(deliveryData)
	
	IF deliveryData.deliveryType = DT_FOOT
		
		IF SHOULD_DO_ON_FOOT_ANIMATED_SCENE(deliveryData)
			IF IS_SYNCHRONIZED_SCENE_RUNNING(deliveryData.cutsceneData.cutscene.iSyncSceneID)
				
				FMC_ON_FOOT_SCENE_DETAILS details = FMC_GET_ON_FOOT_SCENE_DETAILS(deliveryData.eDropoffID)
				FLOAT fCurrentPhase = GET_SYNCHRONIZED_SCENE_PHASE(deliveryData.cutsceneData.cutscene.iSyncSceneID)
			
				// Play sound of opening door
				IF NOT IS_STRING_NULL_OR_EMPTY(details.sSoundSet)
					INT i
					
					REPEAT FMC_MAX_AUDIO_ELEMENTS_PER_SCENE i
						IF NOT IS_BIT_SET(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_AUDIO_PLAYED_BS], i)
						AND IS_ENTITY_ALIVE(deliveryData.cutsceneData.pedArray[0])
						AND NOT IS_STRING_NULL_OR_EMPTY(details.sSoundNames[i])
						AND fCurrentPhase >= details.fSoundPhases[i]
							VECTOR vSoundPos = GET_ENTITY_COORDS(deliveryData.cutsceneData.pedArray[0])
							
							PLAY_SOUND_FROM_COORD(-1, details.sSoundNames[i], vSoundPos, details.sSoundSet)							
							SET_BIT(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_AUDIO_PLAYED_BS], i)
							
							CDEBUG1LN(DEBUG_DELIVERY, "CUTSCENE_FMC_MAINTAIN_SCENE - Playing sound ", details.sSoundNames[i])
						ENDIF
					ENDREPEAT
				ENDIF
			ENDIF
		ELSE
			IF IS_SCREEN_FADED_OUT()
			AND deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_AUDIO_SOUND_ID] = -1
				deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_AUDIO_SOUND_ID] = GET_SOUND_ID()
				PLAY_SOUND_FRONTEND(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_AUDIO_SOUND_ID], "Garage_Door_Close", "GTAO_Script_Doors_Faded_Screen_Sounds")
			ELIF IS_SCREEN_FADING_IN()
				IF deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_AUDIO_SOUND_ID] != -1
					STOP_SOUND(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_AUDIO_SOUND_ID])
					RELEASE_SOUND_ID(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_AUDIO_SOUND_ID])
				ENDIF
			ENDIF
		ENDIF
	ELSE
		
		SWITCH deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_SCENE_VARIATION]
			CASE FMC_DOCKS_SCENE
				MAINTAIN_FMC_DOCKS_SCENE(deliveryData)
			BREAK
			CASE FMC_VEHICLE_DROPOFF
				//Do nothing for now
			BREAK
			CASE FMC_DROPOFF_PASSENGER_SCENE
				CUTSCENE_FMC_MAINTAIN_PASSENGER_DROPOFF_SCENE(deliveryData)
			BREAK
			CASE FMC_VEH_BACKUP_INTO_SPACE_SCN
				CUTSCENE_FMC_TASK_BACK_VEH_UP(deliveryData)
			BREAK
			DEFAULT
	
				CUTSCENE_FMC_MAINTAIN_VEHICLE_COLLISIONS(deliveryData)
				
				IF SHOULD_DO_IN_VEHICLE_EXIT_WALK_SCENE(deliveryData)
				OR (SHOULD_DO_BOAT_DELIVERY_SCENE(deliveryData) AND FMC_SHOULD_BOAT_SCENE_INCLUDE_VEH_EXIT(deliveryData.eDropoffID))
					CUTSCENE_FMC_TASK_PEDS_TO_LEAVE_VEHICLE(deliveryData)
				ELIF SHOULD_DO_EXIT_LIFT_GARAGE_SCENE(deliveryData)
					IF NOT IS_BIT_SET(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET], FMC_DELIVERY_BITSET_EXITED_VEHICLE)
						CUTSCENE_FMC_TASK_PEDS_TO_LEAVE_VEHICLE(deliveryData)
					ELSE
						CUTSCENE_FMC_MAINTAIN_EXIT_LIFT_GARAGE_SCENE(deliveryData)
					ENDIF
				ELIF SHOULD_DO_DRIVE_IN_SCENE(deliveryData)
					IF deliveryData.cutsceneData.cutscene.iScenesCount = 1
						FMC_DELIVERY_CONTROL_SINGLE_SHOT_VEH_CLONE_SCENE(deliveryData)
					ELSE
						FMC_DELIVERY_CONTROL_VEH_CLONE(deliveryData)
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	
	IF g_bEndScreenSuppressFadeIn = FALSE
		g_bEndScreenSuppressFadeIn = TRUE
		SET_BIT(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET], FMC_DELIVERY_BITSET_SET_SHARD_FADE_BOOL)
		CDEBUG1LN(DEBUG_DELIVERY, "CUTSCENE_FMC_MAINTAIN_SCENE sets g_bEndScreenSuppressFadeIn = TRUE")
	ENDIF
ENDPROC

FUNC BOOL CUTSCENE_FMC_ASSETS_LOADED(DELIVERY_DATA &deliveryData)
	UNUSED_PARAMETER(deliveryData)
	
	BOOL bAllAssetsLoaded = TRUE
	
	//Load any audio streams
	STRING sItemToLoad 	= GET_AUDIO_STREAM_FOR_SCENE(deliveryData.eDropoffID)
	INT iStartOffset 	= GET_AUDIO_STREAM_OFFSET_TIME_FOR_SCENE(deliveryData.eDropoffID)
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sItemToLoad)
		IF NOT _FMC_DELIVERY_LOAD_AUDIO_STREAM(sItemToLoad, iStartOffset)	
			bAllAssetsLoaded = FALSE
		ENDIF
		
		SET_BIT(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET], FMC_DELIVERY_BITSET_REQUESTED_AUDIO_STREAM)	
	ENDIF
	
	//Load any assets here
	IF deliveryData.deliveryType = DT_FOOT
		IF SHOULD_DO_ON_FOOT_ANIMATED_SCENE(deliveryData)
			FMC_ON_FOOT_SCENE_DETAILS details = FMC_GET_ON_FOOT_SCENE_DETAILS(deliveryData.eDropoffID)
			
			IF NOT IS_BIT_SET(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET], FMC_DELIVERY_BITSET_REQUESTED_ANIM_DICT)				
				REQUEST_ANIM_DICT(details.sAnimDict)
				SET_BIT(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET], FMC_DELIVERY_BITSET_REQUESTED_ANIM_DICT)				
			ENDIF
			
			IF NOT HAS_ANIM_DICT_LOADED(details.sAnimDict)
				bAllAssetsLoaded = FALSE
			ENDIF
		ENDIF
	ELIF deliveryData.deliveryType = DT_VEHICLE		
		//Load any animation dictionaries
		sItemToLoad = GET_ANIM_DICT_FOR_VEHICLE_SCENE(deliveryData.eDropoffID)		
		
		IF NOT IS_STRING_NULL_OR_EMPTY(sItemToLoad)
			REQUEST_ANIM_DICT(sItemToLoad)
			SET_BIT(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET], FMC_DELIVERY_BITSET_REQUESTED_ANIM_DICT)	
			
			IF NOT HAS_ANIM_DICT_LOADED(sItemToLoad)
				bAllAssetsLoaded  = FALSE
			ENDIF
		ENDIF
		
		//Load general props needed for a scene at a given location
		INT iNumProps = GET_NUM_PROPS_FOR_FMC_SCENE(deliveryData.eDropoffID)
				
		IF iNumProps > 0
			INT i
			
			REPEAT iNumProps i
				MODEL_NAMES eModel = GET_PROP_FOR_FMC_SCENE(deliveryData.eDropoffID, i)
				
				IF eModel != DUMMY_MODEL_FOR_SCRIPT
					REQUEST_MODEL(eModel)
					
					IF NOT HAS_MODEL_LOADED(eModel)
						bAllAssetsLoaded = FALSE
					ENDIF
					
					SET_BIT(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET], FMC_DELIVERY_BITSET_REQUESTED_SCENE_MODELS)
				ENDIF
			ENDREPEAT
		ENDIF
		
		IF IS_BIT_SET(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET], FMC_DELIVERY_BITSET_ATTACHED_PROP_TO_CLONE)
			INT i
			
			REPEAT GET_COUNT_OF_PROPS_ATTACHED_TO_DELIVERY_VEHICLE(deliveryData) i
				INT iModelIndex = (FMC_DELIVERY_INT_ATTACH_VEH_MODEL + i)
				//Load props where the delivery vehicle includes an attached prop			
				IF deliveryData.cutsceneData.intArray[iModelIndex] != ENUM_TO_INT(DUMMY_MODEL_FOR_SCRIPT)
					
					REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, deliveryData.cutsceneData.intArray[iModelIndex]))
					SET_BIT(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET], FMC_DELIVERY_BITSET_REQUESTED_SCENE_MODELS)
					
					IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, deliveryData.cutsceneData.intArray[iModelIndex]))
						bAllAssetsLoaded = FALSE
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
	
	RETURN bAllAssetsLoaded
ENDFUNC

PROC CUTSCENE_FMC_CLEANUP(DELIVERY_DATA &deliveryData)
	CDEBUG1LN(DEBUG_DELIVERY, "CUTSCENE_FMC_CLEANUP")
		
	IF IS_BIT_SET(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET], FMC_DELIVERY_BITSET_CUTSCENE_STARTED)
				
		//Remove ambient vehicles in front of the delivery location
		VECTOR vClearAreaCoord	= FMC_GET_IN_VEHICLE_DROPOFF_LOCATION(deliveryData.eDropoffID)
		FLOAT fClearAreaRadius 	= 25.0
		
		CDEBUG1LN(DEBUG_DELIVERY, "CUTSCENE_FMC_CLEANUP - vClearAreaCoord: ", vClearAreaCoord, " fClearAreaRadius: ", fClearAreaRadius)
		CLEAR_AREA_OF_VEHICLES(vClearAreaCoord, fClearAreaRadius, TRUE, FALSE , FALSE , FALSE, TRUE, TRUE) //OnFire
		CLEAR_AREA_OF_VEHICLES(vClearAreaCoord, fClearAreaRadius, TRUE, FALSE , TRUE , FALSE, TRUE, FALSE) //Wrecked
		
		THEFEED_RESUME()
		THEFEED_SHOW()
		
		IF IS_BIT_SET(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET], FMC_DELIVERY_BITSET_SET_SHARD_FADE_BOOL)
			g_bEndScreenSuppressFadeIn = FALSE
			CDEBUG1LN(DEBUG_DELIVERY, "CUTSCENE_FMC_CLEANUP sets g_bEndScreenSuppressFadeIn = FALSE")
		ENDIF
		
		IF GET_DELIVERY_FLAG_EXTRA(deliveryData, DELIVERY_FLAG_EXTRA_RETURN_PV_TO_PROPERTY)
			CDEBUG1LN(DEBUG_DELIVERY, "CUTSCENE_FMC_CLEANUP - Calling CLEANUP_MP_SAVED_VEHICLE")
			CLEANUP_MP_SAVED_VEHICLE(FALSE, TRUE, TRUE, FALSE, TRUE, FALSE, FALSE)
		ENDIF
		
		//Deal with audio here
		
	ENDIF
	
	IF IS_BIT_SET(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET], FMC_DELIVERY_BITSET_REQUESTED_SCENE_MODELS)
		IF deliveryData.deliveryType = DT_VEHICLE
			INT i
			
			REPEAT GET_COUNT_OF_PROPS_ATTACHED_TO_DELIVERY_VEHICLE(deliveryData) i
				INT iModelIndex = (FMC_DELIVERY_INT_ATTACH_VEH_MODEL + 1)
				
				IF deliveryData.cutsceneData.intArray[iModelIndex] != ENUM_TO_INT(DUMMY_MODEL_FOR_SCRIPT)
					
					SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, deliveryData.cutsceneData.intArray[iModelIndex]))
					
				ENDIF
			ENDREPEAT
		ENDIF
		
		INT iNumProps = GET_NUM_PROPS_FOR_FMC_SCENE(deliveryData.eDropoffID)
		
		//Unload general props needed for a scene at a given location
		IF iNumProps > 0
			INT i
			
			REPEAT iNumProps i
				MODEL_NAMES eModel = GET_PROP_FOR_FMC_SCENE(deliveryData.eDropoffID, i)
				
				IF eModel != DUMMY_MODEL_FOR_SCRIPT
					SET_MODEL_AS_NO_LONGER_NEEDED(eModel)
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
		
	IF IS_BIT_SET(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET], FMC_DELIVERY_BITSET_REQUESTED_ANIM_DICT)
		IF deliveryData.deliveryType = DT_FOOT
			FMC_ON_FOOT_SCENE_DETAILS details = FMC_GET_ON_FOOT_SCENE_DETAILS(deliveryData.eDropoffID)
			REMOVE_ANIM_DICT(details.sAnimDict)
			CDEBUG1LN(DEBUG_DELIVERY, "CUTSCENE_FMC_CLEANUP release anim dict  ", details.sAnimDict)
		ELSE
			STRING sAnimDict = GET_ANIM_DICT_FOR_VEHICLE_SCENE(deliveryData.eDropoffID)		
		
			IF NOT IS_STRING_NULL_OR_EMPTY(sAnimDict)
				REMOVE_ANIM_DICT(sAnimDict)
				CDEBUG1LN(DEBUG_DELIVERY, "CUTSCENE_FMC_CLEANUP release anim dict  ", sAnimDict)
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET], FMC_DELIVERY_BITSET_REQUESTED_AUDIO_STREAM)
		STRING sAudioStream	= GET_AUDIO_STREAM_FOR_SCENE(deliveryData.eDropoffID)
		
		IF NOT IS_STRING_NULL_OR_EMPTY(sAudioStream)
			_FMC_DELIVERY_STOP_AUDIO_STREAM(sAudioStream)
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL IS_PV_ASSIGNED_TO_PROPERTY(FREEMODE_DELIVERY_DROPOFFS eDropoffID)
	IF IS_VEHICLE_ASSIGNED_TO_SIMPLE_INTERIOR(FMC_GET_DROPOFF_PROPERTY_ID(eDropoffID))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//Should this vehicle drive into the hangar in the cutscene.
FUNC BOOL CUTSCENE_FMC_SHOULD_VEHICLE_ENTER(DELIVERY_DATA &deliveryData)
	
	IF NOT IS_ENTITY_ALIVE(deliveryData.deliveryVehicle)
		PRINTLN("CUTSCENE_FMC_SHOULD_VEHICLE_ENTER - vehicle does not exist")
		RETURN FALSE
	ENDIF
	
	CDEBUG1LN(DEBUG_DELIVERY, "CUTSCENE_FMC_SHOULD_VEHICLE_ENTER - Mission vehicle, bring in. Mission: ", IS_ENTITY_A_MISSION_ENTITY(deliveryData.deliveryVehicle)
	," Personal: ", IS_VEHICLE_A_PERSONAL_VEHICLE(deliveryData.deliveryVehicle), " Hangar PV: ", IS_THIS_MODEL_ALLOWED_IN_HANGAR(GET_ENTITY_MODEL(deliveryData.deliveryVehicle)), " SVM? ", IS_SVM_VEHICLE(deliveryData.deliveryVehicle), "Terrorbyte? ", IS_VEHICLE_A_PERSONAL_HACKER_TRUCK(deliveryData.deliveryVehicle))
	
	IF DECOR_EXIST_ON(deliveryData.deliveryVehicle,"FMCVehicle")
		CDEBUG1LN(DEBUG_DELIVERY, "CUTSCENE_FMC_SHOULD_VEHICLE_ENTER - TRUE - Vehicle is a mission entity")
		SET_DELIVERING_IN_A_FMC_MISSION_VEHICLE(deliveryData, TRUE)
		RETURN TRUE
	ENDIF
	
	//Reinstate the below for personal vehicle entry settings
//	PLAYER_INDEX pVehOwner = GET_OWNER_OF_PERSONAL_VEHICLE(deliveryData.deliveryVehicle)
						
//	IF IS_VEHICLE_A_PERSONAL_VEHICLE(deliveryData.deliveryVehicle)
//	AND pVehOwner = deliveryData.deliveryOwner
//	AND NOT IS_VEHICLE_ARMORY_TRUCK(deliveryData.deliveryVehicle)
//	AND CAN_PLAYER_ENTER_ARCADE_IN_VEHICLE(GET_SIMPLE_INTERIOR_FROM_FMC_DROPOFF(deliveryData.eDropoffID), GET_ENTITY_MODEL(deliveryData.deliveryVehicle))
//	AND IS_PV_OWNER_IN_VEHICLE(deliveryData.deliveryVehicle, pVehOwner)
//	AND NOT IS_LOCAL_PLAYER_RIVAL_OR_RIVAL_PASSENGER(GET_SIMPLE_INTERIOR_FROM_FMC_DROPOFF(deliveryData.eDropoffID))
//	
//		IF deliveryData.deliveryOwner = PLAYER_ID()
//		AND (IS_PV_ASSIGNED_TO_ARCADE(deliveryData.eDropoffID))
//			TOGGLE_DELIVERY_FLAG_EXTRA(deliveryData, DELIVERY_FLAG_EXTRA_RETURN_PV_TO_PROPERTY, TRUE)
//			CDEBUG1LN(DEBUG_DELIVERY, "CUTSCENE_FMC_SHOULD_VEHICLE_ENTER - DELIVERY_FLAG_EXTRA_RETURN_PV_TO_PROPERTY - TRUE.")
//		ELSE
			TOGGLE_DELIVERY_FLAG_EXTRA(deliveryData, DELIVERY_FLAG_EXTRA_RETURN_PV_TO_PROPERTY, FALSE)			
			CDEBUG1LN(DEBUG_DELIVERY, "CUTSCENE_FMC_SHOULD_VEHICLE_ENTER - DELIVERY_FLAG_EXTRA_RETURN_PV_TO_PROPERTY - FALSE.")
//		ENDIF
//		
//		RETURN TRUE
//	ELSE
//		PRINTLN("CUTSCENE_FMC_SHOULD_VEHICLE_ENTER - IS PV? ", IS_VEHICLE_A_PERSONAL_VEHICLE(deliveryData.deliveryVehicle), 
//				" Deliverable owners vehicle? ", (pVehOwner = deliveryData.deliveryOwner),
//				" IS_THIS_MODEL_ALLOWED_IN_HANGAR? ", IS_THIS_MODEL_ALLOWED_IN_HANGAR(GET_ENTITY_MODEL(deliveryData.deliveryVehicle)),
//				" IS_VEHICLE_ARMORY_TRUCK? ", IS_VEHICLE_ARMORY_TRUCK(deliveryData.deliveryVehicle),
//				" IS_SVM_VEHICLE? ", IS_SVM_VEHICLE(deliveryData.deliveryVehicle),
//				" IS_PV_OWNER_IN_VEHICLE? ", IS_PV_OWNER_IN_VEHICLE(deliveryData.deliveryVehicle, pVehOwner),
//				" Delivery owner = ", NATIVE_TO_INT(deliveryData.deliveryOwner), " - Player ID: ", NATIVE_TO_INT(PLAYER_ID()))
//				
//		PRINTLN("CUTSCENE_FMC_SHOULD_VEHICLE_ENTER - Rival: ", IS_LOCAL_PLAYER_RIVAL_OR_RIVAL_PASSENGER(GET_SIMPLE_INTERIOR_FROM_FMC_DROPOFF(deliveryData.eDropoffID)))
//	ENDIF
	
	CDEBUG1LN(DEBUG_DELIVERY, "CUTSCENE_FMC_SHOULD_VEHICLE_ENTER - FALSE.")
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Select the scene to use for this delivery
PROC CUTSCENE_FMC_SETUP_SCENE_VARIATION(DELIVERY_DATA &deliveryData)
	IF deliveryData.deliveryType = DT_VEHICLE
		IF DOES_ENTITY_EXIST(deliveryData.deliveryVehicle)
		AND IS_THIS_MODEL_A_BOAT(GET_ENTITY_MODEL(deliveryData.deliveryVehicle))
			SET_FMC_SCENE_VARIATION(deliveryData, FMC_IN_BOAT_DELIVERY)
		ELIF deliveryData.eDropoffID = FMC_DROPOFF_DOCKS
			SET_FMC_SCENE_VARIATION(deliveryData, FMC_DOCKS_SCENE)
		ELIF deliveryData.eDropoffID = FMC_DROPOFF_METH_TANKER_HARMONY
			SET_FMC_SCENE_VARIATION(deliveryData, FMC_VEH_BACKUP_INTO_SPACE_SCN)
		ELIF FMC_IS_VEHICLE_DELIVERY_TO_CUSTOMER(deliveryData.eDropoffID)
			SET_FMC_SCENE_VARIATION(deliveryData, FMC_VEHICLE_DROPOFF)
		ELIF FMC_SHOULD_DO_PASSENGER_DROPOFF_SCENE(deliveryData.eDropoffID)
			SET_FMC_SCENE_VARIATION(deliveryData, FMC_DROPOFF_PASSENGER_SCENE)
			TOGGLE_DELIVERY_FLAG_EXTRA(deliveryData, DELIVERY_FLAG_EXTRA_PERFORM_EARLY_ASSET_DELETION, TRUE)
		ELIF FMC_IS_IN_VEHICLE_SCENE_EXIT_WALK(deliveryData.eDropoffID)
			IF FMC_SHOULD_WARP_PLAYER_AFTER_SCENE(deliveryData.eDropoffID)
				TOGGLE_DELIVERY_FLAG_EXTRA(deliveryData, DELIVERY_FLAG_EXTRA_REPOSITION_PED_AFTER_SCENE, TRUE)
			ENDIF
			TOGGLE_DELIVERY_FLAG_EXTRA(deliveryData, DELIVERY_FLAG_EXTRA_RETURN_PV_TO_PROPERTY, FALSE)
			SET_FMC_SCENE_VARIATION(deliveryData, FMC_IN_VEHICLE_EXIT_SCENE)
		ELIF FMC_SHOULD_DO_EXIT_LIFT_GARAGE_SCENE(deliveryData.eDropoffID)
			SET_FMC_SCENE_VARIATION(deliveryData, FMC_EXIT_LIFT_GARAGE_SCENE)
		ELSE
			SET_FMC_SCENE_VARIATION(deliveryData, FMC_DRIVE_IN_SCENE)
		ENDIF
	ELSE
		IF FMC_IS_ON_FOOT_SCENE_ANIMATED(deliveryData.eDropoffID)
			SET_FMC_SCENE_VARIATION(deliveryData, FMC_ON_FOOT_ANIMATED_SCENE)
		ELSE
			SET_FMC_SCENE_VARIATION(deliveryData, FMC_ON_FOOT_TASK_WALK_SCENE)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_FMC_DELIVERY_CHECK_VEH_PARTICIPANTS()
	IF FM_EVENT_IS_PLAYER_PERMANENT_PARTICIPANT_IN_THIS_FM_EVENT(PLAYER_ID(), FMMC_TYPE_SUPER_BUSINESS_BATTLES)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC CHECK_DELIVERY_VEH_FOR_ATTACHED_PROPS(DELIVERY_DATA &deliveryData)
	
	INT i, iPropCount
	MODEL_NAMES eAttachModel
	
	IF IS_ENTITY_ALIVE(deliveryData.deliveryVehicle)
		//Determine if we need to attach anything to the delivery vehicle	
		WHILE FMC_GET_VEH_PROP_ATTACH_MODEL(i) != DUMMY_MODEL_FOR_SCRIPT			
			eAttachModel = FMC_GET_VEH_PROP_ATTACH_MODEL(i)

			ENTITY_INDEX entity = GET_ENTITY_OF_TYPE_ATTACHED_TO_ENTITY(deliveryData.deliveryVehicle, eAttachModel)
			
			INT iPropArrayIndex = (FMC_DELIVERY_INT_ATTACH_VEH_MODEL + iPropCount)
			
			IF DOES_ENTITY_EXIST(entity)
				SET_BIT(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET], FMC_DELIVERY_BITSET_ATTACHED_PROP_TO_CLONE)
				deliveryData.cutsceneData.intArray[iPropArrayIndex] = ENUM_TO_INT(eAttachModel)
				CDEBUG1LN(DEBUG_DELIVERY, "CHECK_DELIVERY_VEH_FOR_ATTACHED_PROPS - Model: ", eAttachModel, " is attached to vehicle at index: ", iPropArrayIndex)
				
				iPropCount ++
				
				IF iPropCount = FMC_MAX_ATTACHED_PROPS
					BREAKLOOP
				ENDIF
			ELSE
				VEHICLE_INDEX vehTrailer = NULL
				IF IS_VEHICLE_ATTACHED_TO_TRAILER(deliveryData.deliveryVehicle)
				AND GET_VEHICLE_TRAILER_VEHICLE(deliveryData.deliveryVehicle, vehTrailer)
					IF IS_ENTITY_ALIVE(vehTrailer)
						
						entity = GET_ENTITY_OF_TYPE_ATTACHED_TO_ENTITY(vehTrailer, eAttachModel)
							
						IF DOES_ENTITY_EXIST(entity)							
							SET_BIT(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET], FMC_DELIVERY_BITSET_ATTACHED_PROP_TO_CLONE)
							SET_BIT(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET], FMC_DELIVERY_BITSET_ATTACHED_PROP_TO_TRAILER_CLONE)
							deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ATTACH_VEH_MODEL] = ENUM_TO_INT(eAttachModel)
							CDEBUG1LN(DEBUG_DELIVERY, "CHECK_DELIVERY_VEH_FOR_ATTACHED_PROPS - Model: ", eAttachModel, " is attached to trailer at index: ", iPropArrayIndex)
							
							iPropCount ++
				
							IF iPropCount = FMC_MAX_ATTACHED_PROPS
								BREAKLOOP
							ENDIF
							
						ELSE
							CDEBUG1LN(DEBUG_DELIVERY, "CHECK_DELIVERY_VEH_FOR_ATTACHED_PROPS - Model: ", eAttachModel, " is not attached to trailer that exists")
						ENDIF
					ELSE
						CDEBUG1LN(DEBUG_DELIVERY, "CHECK_DELIVERY_VEH_FOR_ATTACHED_PROPS - Model: ", eAttachModel, " can't be found. Delivery trailer does not exist")
					ENDIF
				ELSE
					CDEBUG1LN(DEBUG_DELIVERY, "CHECK_DELIVERY_VEH_FOR_ATTACHED_PROPS - Model: ", eAttachModel, " does not exist")
				ENDIF
			ENDIF
			
			i++
		ENDWHILE
	ELSE
		CDEBUG1LN(DEBUG_DELIVERY, "CHECK_DELIVERY_VEH_FOR_ATTACHED_PROPS - Vehicle is not alive")
	ENDIF
ENDPROC

//Vehicle parked in front of garage door, drive in
PROC CUTSCENE_FMC_INITIALIZE(FREEMODE_DELIVERY_SCRIPT_LAUNCH_DATA &_launchData, SCRIPT_DATA& _scriptData)	
	UNUSED_PARAMETER(_launchData)
		
	CDEBUG1LN(DEBUG_DELIVERY, "CUTSCENE_FMC_INITIALIZE - DELIVERY TYPE: ", _scriptData.deliveryData.deliveryType)
	
	//Lookup
	_scriptData.functionPointers.coreFunctions.handlerAssetsLoaded 					= &CUTSCENE_FMC_ASSETS_LOADED
	_scriptData.functionPointers.coreFunctions.handlerAssetsCreate 					= &CUTSCENE_FMC_ASSETS_CREATE
	_scriptData.functionPointers.coreFunctions.handlerCreateScene 					= &CUTSCENE_FMC_CREATE_SCENE
	_scriptData.functionPointers.coreFunctions.handlerMaintainScene 				= &CUTSCENE_FMC_MAINTAIN_SCENE
	_scriptData.functionPointers.coreFunctions.handlerSceneSetup 					= &CUTSCENE_FMC_ASSET_SCENE_SETUP
	_scriptData.functionPointers.coreFunctions.handlerCleanup 						= &CUTSCENE_FMC_CLEANUP
	_scriptData.functionPointers.coreFunctions.handlerSetupAfterSceneSpawnPoints 	= &CUTSCENE_FMC_SPAWN_POINT_SETUP
		
	//Should we return this PV to the garage slot:
	IF _scriptData.deliveryData.deliveryType = DT_VEHICLE
	AND FMC_SHOULD_SCENE_CHECK_FOR_VEH_ALLOWED_ENTRY(_scriptData.deliveryData.eDropoffID)
		CUTSCENE_FMC_SHOULD_VEHICLE_ENTER(_scriptData.deliveryData)
	ENDIF
	
	//How many deliverables are being delivered
	IF _scriptData.deliveryData.deliveryType = DT_VEHICLE
		IF FREEMODE_DELIVERY_IS_DELIVERABLE_VALID(_launchData.deliverableID)
			FREEMODE_DELIVERABLE_TYPE eDeliverableType = FREEMODE_DELIVERY_GET_DELIVERABLE_TYPE(_launchData.deliverableID)
			_scriptData.deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_NUM_DELIVERABLES_IN_DELIVERY] = GET_NUM_FREEMODE_DELIVERABLES_IN_VEH(eDeliverableType)
			
			TOGGLE_DELIVERY_FLAG_EXTRA(_scriptData.deliveryData, DELIVERY_FLAG_EXTRA_ENABLE_AGGRESSIVE_BRING_VEH_TO_HALT, TRUE)
			
			IF IS_ENTITY_ALIVE(_scriptData.deliveryData.deliveryVehicle)
				_scriptData.deliveryData.cutsceneData.floatArray[FMC_DELIVERY_FLOAT_VEH_START_HEADING] = GET_ENTITY_HEADING(_scriptData.deliveryData.deliveryVehicle)
				CDEBUG1LN(DEBUG_DELIVERY, "CUTSCENE_FMC_INITIALIZE - Start heading: ", _scriptData.deliveryData.cutsceneData.floatArray[FMC_DELIVERY_FLOAT_VEH_START_HEADING])
			ENDIF
		ENDIF
		
		IF FMC_SHOULD_VEHICLE_SCENE_INCLUDE_NON_PLAYER_PEDS(_scriptData.deliveryData.eDropoffID)
			TOGGLE_DELIVERY_FLAG_EXTRA(_scriptData.deliveryData, DELIVERY_FLAG_EXTRA_CLONE_NON_PLAYER_PEDS, TRUE)
		ENDIF
		
		CHECK_DELIVERY_VEH_FOR_ATTACHED_PROPS(_scriptData.deliveryData)
	ELSE
		_scriptData.deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_NUM_DELIVERABLES_IN_DELIVERY] = FREEMODE_DELIVERY_GET_COUNT_OF_DELIVERABLES_HELD_BY_PLAYER(PLAYER_ID())
	ENDIF
	
	CDEBUG1LN(DEBUG_DELIVERY, "CUTSCENE_FMC_INITIALIZE - Deliverables in delivery: ", _scriptData.deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_NUM_DELIVERABLES_IN_DELIVERY])
	
	//Pick a scene variation based on the mission
	CUTSCENE_FMC_SETUP_SCENE_VARIATION(_scriptData.deliveryData)
	
	VECTOR vModelMin
	CHECK_VEHICLE_HEIGHT(_scriptData.deliveryData, vModelMin, _scriptData.deliveryData.deliveryVehicle)
	
	IF FMC_SHOULD_WARP_REAL_VEH_DURING_SCENE(_scriptData.deliveryData.eDropoffID)
		//Delivery Setup - Fade and reposition after scene
		TOGGLE_DELIVERY_FLAG_EXTRA(_scriptData.deliveryData, DELIVERY_FLAG_EXTRA_FADE_OUT_AND_MOVE_PED, TRUE)
		TOGGLE_DELIVERY_FLAG_EXTRA(_scriptData.deliveryData, DELIVERY_FLAG_EXTRA_REPOSITION_DELIVERY_VEHICLE, TRUE)
	ELIF FMC_SHOULD_WARP_PED_OUT_OF_VEHICLE_DURING_SCENE(_scriptData.deliveryData.eDropoffID)
		TOGGLE_DELIVERY_FLAG_EXTRA(_scriptData.deliveryData, DELIVERY_FLAG_EXTRA_FADE_OUT_AND_MOVE_PED, TRUE)
	ENDIF
	
	IF _scriptData.deliveryData.deliveryType = DT_VEHICLE
	AND FMC_SHOULD_DELETE_AMBIENT_VEH_DURING_SCENE(_scriptData.deliveryData.eDropoffID)
		IF DOES_ENTITY_EXIST(_scriptData.deliveryData.deliveryVehicle) 
		AND NOT IS_ENTITY_A_MISSION_ENTITY(_scriptData.deliveryData.deliveryVehicle)
			TOGGLE_DELIVERY_FLAG_EXTRA(_scriptData.deliveryData, DELIVERY_FLAG_EXTRA_DELETE_AMBIENT_VEHICLE, TRUE)
		ELSE
			CDEBUG1LN(DEBUG_DELIVERY, "CUTSCENE_FMC_INITIALIZE - Dock delivery but entity is a mission entity. Do not delete.")
		ENDIF
	ENDIF
	
	IF NOT FMC_SHOULD_WARP_PLAYER_INTO_PROPERTY(_scriptData.deliveryData)
	AND FMC_SHOULD_WARP_PLAYER_AFTER_SCENE(_scriptData.deliveryData.eDropoffID)
		IF FMC_DOES_DROPOFF_REQUIRE_POST_SCENE_EXT_SCRIPT_WARP(_scriptData.deliveryData.eDropoffID)
			TOGGLE_DELIVERY_FLAG_EXTRA(_scriptData.deliveryData, DELIVERY_FLAG_EXTRA_EXTERNAL_SCRIPT_PERFORMS_WARP_AFTER_SCENE, TRUE)
		ELSE
			TOGGLE_DELIVERY_FLAG_EXTRA(_scriptData.deliveryData, DELIVERY_FLAG_EXTRA_REPOSITION_PED_AFTER_SCENE, TRUE)
		ENDIF
		
		IF NOT FMC_DOES_DROPOFF_REQUIRE_INPUT_GAIT_AFTER_POST_SCENE_WARP(_scriptData.deliveryData.eDropoffID)
			TOGGLE_DELIVERY_FLAG_EXTRA(_scriptData.deliveryData, DELIVERY_FLAG_EXTRA_DONT_SIMULATE_INPUT_GAIT_ON_WARP, TRUE)
		ENDIF
	ENDIF
	
	IF FMC_DOES_DROPOFF_REQUIRE_CALL_OVER_SCENE(_scriptData.deliveryData.eDropoffID)
		TOGGLE_DELIVERY_FLAG_EXTRA(_scriptData.deliveryData, DELIVERY_FLAG_EXTRA_ALLOW_CALL_DURING_SCENE, TRUE)
	ENDIF
	
	IF FMC_SHOULD_CLONE_NON_PLAYER_PEDS(_scriptData.deliveryData.eDropoffID)
		TOGGLE_DELIVERY_FLAG_EXTRA(_scriptData.deliveryData, DELIVERY_FLAG_EXTRA_CLONE_NON_PLAYER_PEDS, TRUE)
	ENDIF
	
	IF SHOULD_DO_FMC_DROPOFF_PASSENGER_SCENE(_scriptData.deliveryData)
		TOGGLE_DELIVERY_FLAG_DEFAULT(_scriptData.deliveryData, DELIVERY_FLAG_DEFAULT_SCREEN_FADE_AFTER_DELIVERY, FALSE)
	ENDIF
	
	//Setup when the shard should display
	_scriptData.deliveryData.iSceneToDisplayShard = 3
	
	IF SHOULD_FMC_DELIVERY_CHECK_VEH_PARTICIPANTS()
		TOGGLE_DELIVERY_FLAG_EXTRA(_scriptData.deliveryData, DELIVERY_FLAG_EXTRA_CHECK_VEH_INSTANCE, TRUE)
	ENDIF
	
ENDPROC
