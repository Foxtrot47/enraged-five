USING "globals.sch"
USING "net_include.sch"
USING "GB_DELIVERY_GLOBAL.sch"
USING "rc_asset_public.sch"
USING "gb_delivery_definitions.sch"
USING "net_simple_interior_cs_header_include.sch"
USING "net_freemode_delivery_private.sch"

/// PURPOSE:
///    VFX will be removed per frame
PROC DELIVERY_CHILD_SET_VFX_REMOVE(DELIVERY_DATA &deliveryData, VECTOR vCoord, FLOAT fRange)
	CDEBUG1LN(DEBUG_DELIVERY, "DELIVERY_CHILD_SET_VFX_REMOVE - vfxRemoveCoord: ", vCoord, " vfxRemoveRange: ", fRange)
	deliveryData.vfxRemoveCoord = vCoord
	deliveryData.vfxRemoveRange = fRange
ENDPROC

/// PURPOSE:
///    Set audio scene to start with the cutscene.
PROC DELIVERY_CHILD_SET_AUDIO_SCENE_CUTSCENE(DELIVERY_DATA &deliveryData, STRING sNewAudioScene)
	CDEBUG1LN(DEBUG_DELIVERY, "DELIVERY_CHILD_SET_AUDIO_SCENE_CUTSCENE - new audio scene is set: ", sNewAudioScene)
	deliveryData.cutsceneData.audioSceneCutscene = sNewAudioScene
ENDPROC

//################################################# GENERIC LOCATE LOGIC ################################################# 
PROC DELIVERY_CHILD_SET_LOCATE_AAB(DELIVERY_DATA &deliveryData, VECTOR vNewCoord1, VECTOR vNewCoord2)
	CDEBUG1LN(DEBUG_DELIVERY, "DELIVERY_CHILD_SET_LOCATE_COORDS - vNewCoord1: ", vNewCoord1, " vNewCoord2: ", vNewCoord2)
	deliveryData.locateCoord1 = vNewCoord1
	deliveryData.locateCoord2 = vNewCoord2
ENDPROC

PROC DELIVERY_CHILD_SET_FOCUS(DELIVERY_DATA &deliveryData, VECTOR vNewFocus)
	CDEBUG1LN(DEBUG_DELIVERY, "DELIVERY_CHILD_SET_FOCUS - vNewFocus: ", vNewFocus)
	deliveryData.focusCoord = vNewFocus
ENDPROC

//PROC DELIVRY_CHILD_ST_LOCATE_CYLINDER(DELIVERY_DATA &deliveryData, VECTOR vNewCoord, FLOAT fRadius, FLOAT fHeight)
//
//ENDPROC

PROC DELIVERY_CHILD_SET_LOCATE_MULTIPLIER(DELIVERY_DATA &deliveryData, VECTOR vNewMultiplier)
	#IF IS_DEBUG_BUILD
	IF ARE_VECTORS_EQUAL(vNewMultiplier, deliveryData.locateSizeMultiplier)
		CDEBUG1LN(DEBUG_DELIVERY, "DELIVERY_CHILD_SET_LOCATE_MULTIPLIER - vNewMultiplier: ", vNewMultiplier)
	ENDIF
	#ENDIF
	deliveryData.locateSizeMultiplier = vNewMultiplier
ENDPROC

//################################################# BITSET DELIVERY SPECIFIC FLAGS ################################################# 
//DEFAULT
FUNC BOOL GET_DELIVERY_FLAG_DEFAULT(DELIVERY_DATA &deliveryData, DELIVERY_CUTSCENE_DEFAULT_FLAGS bit)
	RETURN IS_BIT_SET_ENUM(deliveryData.iDefaultBS, bit)
ENDFUNC

PROC TOGGLE_DELIVERY_FLAG_DEFAULT(DELIVERY_DATA &deliveryData, DELIVERY_CUTSCENE_DEFAULT_FLAGS bitIndex, BOOL bNewState)
	CDEBUG1LN(DEBUG_DELIVERY, "TOGGLE_DELIVERY_FLAG_DEFAULT - Switching: ", GET_DELIVERY_FLAG_DEFAULT_NAME(bitIndex), " From: ", IS_BIT_SET_ENUM(deliveryData.iDefaultBS, bitIndex), " To: ", bNewState)
	TOGGLE_BIT_ENUM(deliveryData.iDefaultBS, bitIndex, bNewState)
ENDPROC
//EXTRA
FUNC BOOL GET_DELIVERY_FLAG_EXTRA(DELIVERY_DATA &deliveryData, DELIVERY_CUTSCENE_EXTRA_FLAGS bit)
	RETURN IS_BIT_SET_ENUM(deliveryData.iExtraBS, bit)
ENDFUNC

PROC TOGGLE_DELIVERY_FLAG_EXTRA(DELIVERY_DATA &deliveryData, DELIVERY_CUTSCENE_EXTRA_FLAGS bitIndex, BOOL bNewState)
	#IF IS_DEBUG_BUILD
		IF bNewState != GET_DELIVERY_FLAG_EXTRA(deliveryData, bitIndex)
			CDEBUG1LN(DEBUG_DELIVERY, "TOGGLE_DELIVERY_FLAG_DEFAULT - Switching: ", GET_DELIVERY_FLAG_EXTRA_NAME(bitIndex), " From: ", IS_BIT_SET_ENUM(deliveryData.iExtraBS, bitIndex), " To: ", bNewState)
		ENDIF
	#ENDIF
	TOGGLE_BIT_ENUM(deliveryData.iExtraBS, bitIndex, bNewState)
ENDPROC

//################################################# ASSETS SUPPORT ################################################# 
//Simplified object Creation
PROC DELIVERY_CREATE_OBJECT(DELIVERY_CUTSCENE_DATA &cutsceneData, MODEL_NAMES model, VECTOR vPosition, INT iObjectArrayIndex)
	IF iObjectArrayIndex > COUNT_OF(cutsceneData.objectArray)
		ASSERTLN("DELIVERY_CREATE_OBJECT - can not create a new object, reched objectArray limit, iObjectArrayIndex: ", iObjectArrayIndex, " vPosition: ", vPosition, " model: ", GET_MODEL_NAME_FOR_DEBUG(model))
		EXIT
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(cutsceneData.objectArray[iObjectArrayIndex])
		cutsceneData.objectArray[iObjectArrayIndex] = CREATE_OBJECT_NO_OFFSET(model, vPosition, FALSE)
	ELSE
		ASSERTLN("DELIVERY_CREATE_OBJECT - index is already used up, iObjectArrayIndex: ", iObjectArrayIndex, " vPosition: ", vPosition, " model: ", GET_MODEL_NAME_FOR_DEBUG(model))
	ENDIF
ENDPROC

//**************************************** OVERRIDES
//######################################## DELIVERY CHILD HANDLERS BASE ########################################
/// PURPOSE:
///    Default asset loader can be overriden.
FUNC BOOL DELIVERY_DEFAULT_ASSTS_LOADED(DELIVERY_DATA &deliveryData)
	CDEBUG1LN(DEBUG_DELIVERY, "DELIVERY_DEFAULT_ASSTS_LOADED - default asset loader used, loading...") 
	RETURN HAVE_ASSET_REQUESTS_LOADED(deliveryData.cutsceneData.assetRequester)
ENDFUNC

/// PURPOSE:
///    Extra clenup can be hooked up 
PROC DELIVERY_DEFAULT_CLEANUP(DELIVERY_DATA &deliveryData)
	CDEBUG1LN(DEBUG_DELIVERY, "DELIVERY_DEFAULT_CLEANUP - default cutscene cleanup is used, nothing extra cleaned-up.") 
	UNUSED_PARAMETER(deliveryData)
ENDPROC

/// PURPOSE:
///    All assets created at once before cutscene starts.
FUNC BOOL DELIVERY_DEFAULT_ASSETS_CREATE(DELIVERY_DATA &deliveryData)
	CDEBUG1LN(DEBUG_DELIVERY, "DELIVERY_DEFAULT_ASSETS_CREATE - no extra assets will be created.") 
	UNUSED_PARAMETER(deliveryData)
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Called once when a scene switch happened, used to initialize/position assets.
/// PARAMS:
///    iScene - new active scene
PROC DELIVERY_DEFAULT_SCENE_SETUP(DELIVERY_DATA &deliveryData, INT iScene)
	UNUSED_PARAMETER(deliveryData)
	CDEBUG1LN(DEBUG_DELIVERY, "DELIVERY_DEFAULT_SCENE_SETUP - scene setup pointer not initialized, scene: ", iScene) 
ENDPROC

FUNC BOOL IS_PV_OWNER_IN_VEHICLE(VEHICLE_INDEX veh, PLAYER_INDEX pOwner)
	
	IF pOwner = INVALID_PLAYER_INDEX()
		RETURN FALSE
	ENDIF
	
	INT iPlayersInVeh = ALL_PLAYERS_IN_VEHICLE(veh, TRUE, FALSE, TRUE)	
	RETURN IS_BIT_SET(iPlayersInVeh, NATIVE_TO_INT(pOwner))
ENDFUNC

PROC TASK_PED_TO_LEAVE_VEH_AND_FOLLOW_NAVMESH(PED_INDEX ped, VECTOR vWalkTo)
	SEQUENCE_INDEX mSequence = NULL
	
	OPEN_SEQUENCE_TASK(mSequence)
		TASK_LEAVE_ANY_VEHICLE(NULL)
		TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vWalkTo, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_NEVER_WARP)		
	CLOSE_SEQUENCE_TASK(mSequence)
	
	TASK_PERFORM_SEQUENCE(ped, mSequence)
	CLEAR_SEQUENCE_TASK(mSequence)
ENDPROC

FUNC BOOL ARE_GOONS_NEARBY_WITH_REMAINING_MISSION_DELIVERABLES(FREEMODE_DELIVERY_DROPOFFS eDropoffID, INT iTotaldeliverables, INT iNumDeliverablesRemaining)
	
	INT iNearbyGoons = 0
	
	IF iNumDeliverablesRemaining >= 1
	AND GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG(TRUE)
	AND GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(PLAYER_ID())
	AND GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID())
		PLAYER_INDEX pBoss = GB_GET_LOCAL_PLAYER_GANG_BOSS()
		VECTOR vDropoffCoord = FREEMODE_DELIVERY_DROPOFF_GET_ON_FOOT_COORDS(eDropoffID)
		INT i
		
		IF pBoss != PLAYER_ID()
			IF FREEMODE_DELIVERY_IS_PLAYER_IN_POSSESSION_OF_ANY_DELIVERABLE(pBoss)
				VECTOR vPlayerPos = GET_PLAYER_COORDS(pBoss)
				
				IF VDIST2(vPlayerPos, vDropoffCoord) <= 400
					CDEBUG1LN(DEBUG_DELIVERY, "ARE_GOONS_NEARBY_WITH_REMAINING_MISSION_DELIVERABLES - TRUE - I'm not boss. They are nearby ", FREEMODE_DELIVERY_GET_COUNT_OF_DELIVERABLES_HELD_BY_PLAYER(pBoss), " package(s)")
					iNearbyGoons += FREEMODE_DELIVERY_GET_COUNT_OF_DELIVERABLES_HELD_BY_PLAYER(pBoss)
				ELSE
					CDEBUG1LN(DEBUG_DELIVERY, "ARE_GOONS_NEARBY_WITH_REMAINING_MISSION_DELIVERABLES - Boss is not in possesion.")
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_DELIVERY, "ARE_GOONS_NEARBY_WITH_REMAINING_MISSION_DELIVERABLES - Boss is not in possesion.")
			ENDIF
		ELSE
			CDEBUG1LN(DEBUG_DELIVERY, "ARE_GOONS_NEARBY_WITH_REMAINING_MISSION_DELIVERABLES - I am boss.")
		ENDIF
		
		REPEAT GB_MAX_GANG_GOONS i
			IF GlobalplayerBD_FM_3[NATIVE_TO_INT(pBoss)].sMagnateGangBossData.GangMembers[i] != INVALID_PLAYER_INDEX()
			AND GlobalplayerBD_FM_3[NATIVE_TO_INT(pBoss)].sMagnateGangBossData.GangMembers[i] != PLAYER_ID()
				IF FREEMODE_DELIVERY_IS_PLAYER_IN_POSSESSION_OF_ANY_DELIVERABLE(GlobalplayerBD_FM_3[NATIVE_TO_INT(pBoss)].sMagnateGangBossData.GangMembers[i])
					VECTOR vPlayerPos = GET_PLAYER_COORDS(GlobalplayerBD_FM_3[NATIVE_TO_INT(pBoss)].sMagnateGangBossData.GangMembers[i])
					
					IF VDIST2(vPlayerPos, vDropoffCoord) <= 225
						CDEBUG1LN(DEBUG_DELIVERY, "ARE_GOONS_NEARBY_WITH_REMAINING_MISSION_DELIVERABLES - Player in goon slot: ", i, " ID: ", NATIVE_TO_INT(GlobalplayerBD_FM_3[NATIVE_TO_INT(pBoss)].sMagnateGangBossData.GangMembers[i]), " holds ", FREEMODE_DELIVERY_GET_COUNT_OF_DELIVERABLES_HELD_BY_PLAYER(GlobalplayerBD_FM_3[NATIVE_TO_INT(pBoss)].sMagnateGangBossData.GangMembers[i]), " packages and is in range!")
						iNearbyGoons += FREEMODE_DELIVERY_GET_COUNT_OF_DELIVERABLES_HELD_BY_PLAYER(GlobalplayerBD_FM_3[NATIVE_TO_INT(pBoss)].sMagnateGangBossData.GangMembers[i])
					ELSE
						CDEBUG1LN(DEBUG_DELIVERY, "ARE_GOONS_NEARBY_WITH_REMAINING_MISSION_DELIVERABLES - Player in goon slot: ", i, " ID: ", NATIVE_TO_INT(GlobalplayerBD_FM_3[NATIVE_TO_INT(pBoss)].sMagnateGangBossData.GangMembers[i]), " holds a package and is NOT in range!")
					ENDIF
				ELSE
					CDEBUG1LN(DEBUG_DELIVERY, "ARE_GOONS_NEARBY_WITH_REMAINING_MISSION_DELIVERABLES - Player in goon slot: ", i, " ID: ", NATIVE_TO_INT(GlobalplayerBD_FM_3[NATIVE_TO_INT(pBoss)].sMagnateGangBossData.GangMembers[i]), " does NOT hold a package")
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_DELIVERY, "ARE_GOONS_NEARBY_WITH_REMAINING_MISSION_DELIVERABLES - Ignoring player in goon slot: ", i, " ID: ", NATIVE_TO_INT(GlobalplayerBD_FM_3[NATIVE_TO_INT(pBoss)].sMagnateGangBossData.GangMembers[i]))
			ENDIF
		ENDREPEAT
	ELSE
		CDEBUG1LN(DEBUG_DELIVERY, "ARE_GOONS_NEARBY_WITH_REMAINING_MISSION_DELIVERABLES - Ignoring checks. Deliverables remaining = ", iNumDeliverablesRemaining, " #Deliverables = ", iTotaldeliverables)
	ENDIF
	
	CDEBUG1LN(DEBUG_DELIVERY, "ARE_GOONS_NEARBY_WITH_REMAINING_MISSION_DELIVERABLES - iNumDeliverablesRemaining: ", iNumDeliverablesRemaining, " iNearbyGoons: ", iNearbyGoons)
	RETURN (iNumDeliverablesRemaining = iNearbyGoons)
ENDFUNC

/// PURPOSE:
///    Checks the number of delivered items vs the number remaining also taking into account nearby allies who may also be holdiong a package
/// PARAMS:
///    deliveryData 		- delivery script struct
///    bIsProperty 			- Is the delivery location a player property
///    eDropoffInteriorID 	- the simple interior ID of the dropoff used if bIsProperty = TRUE
///    bIsMissionVeh 		- Is the delivery vehicle used a mission vehicle
///    bUseDeliverableOwner - Use the owner of the deliverable instead of the launch data delivery owner
/// RETURNS:
///    TRUE if the exit vehicle to property scene is required
FUNC BOOL DELIVERY_SCENE_DO_DELIVERABLES_CHECK(DELIVERY_DATA &deliveryData, BOOL bIsProperty, SIMPLE_INTERIORS eDropoffInteriorID, BOOL bIsMissionVeh, BOOL bUseDeliverableOwner)
	IF bIsProperty
	AND NOT IS_LOCAL_PLAYER_RIVAL_OR_RIVAL_PASSENGER(eDropoffInteriorID)
		//Run a check on if we should spawn near the dropoff to continue the mission		
		INT i, iNumDeliverablesLeft, iNumDeliverables
		INT iNumDeliverablesHeld = FREEMODE_DELIVERY_GET_COUNT_OF_DELIVERABLES_HELD_BY_PLAYER(PLAYER_ID())
		FREEMODE_DELIVERABLE_ID eDeliverables[CSH_MAX_MISSION_DELIVERABLES]
		FREEMODE_DELIVERABLE_TYPE eType = FREEMODE_DELIVERY_GET_DELIVERABLE_TYPE(deliveryData.eDeliverableID)
		
		PLAYER_INDEX pOwner = deliveryData.deliveryOwner
		
		IF bUseDeliverableOwner
		AND deliveryData.eDeliverableID.pOwner != INVALID_PLAYER_INDEX()
			pOwner = deliveryData.eDeliverableID.pOwner
		ENDIF
		
		_FREEMODE_DELIVERY_GET_DELIVERABLES_OF_TYPE(eType, pOwner, eDeliverables)
		
		REPEAT CSH_MAX_MISSION_DELIVERABLES i
			IF FREEMODE_DELIVERY_IS_DELIVERABLE_VALID(eDeliverables[i])
				FREEMODE_DELIVERABLE_STATUS eDeliverableStatus = FREEMODE_DELIVERY_GET_DELIVERABLE_STATUS(eDeliverables[i])
				
				iNumDeliverables ++
				IF eDeliverableStatus = DELIVERABLE_STATUS_INITIAL
					//One of the other deliverables is available to collect to warp the player outside
					iNumDeliverablesLeft ++
				ENDIF
				
				CDEBUG1LN(DEBUG_DELIVERY, "DELIVERY_SCENE_DO_DELIVERABLES_CHECK - Deliverable ", i, " ID: ", eDeliverables[i].iIndex, " status: ", eDeliverableStatus, " script = ", deliveryData.eDeliverableID.iIndex)
			ENDIF
		ENDREPEAT
		
		CDEBUG1LN(DEBUG_DELIVERY, "DELIVERY_SCENE_DO_DELIVERABLES_CHECK - iNumDeliverables: ", iNumDeliverables, " iNumDeliverablesLeft: ", iNumDeliverablesLeft, " iNumDeliverablesHeld: ", iNumDeliverablesHeld)
		
		iNumDeliverablesLeft -= iNumDeliverablesHeld
		
		IF deliveryData.deliveryType = DT_FOOT			
			IF iNumDeliverablesLeft >= 1
				IF ARE_GOONS_NEARBY_WITH_REMAINING_MISSION_DELIVERABLES(deliveryData.eDropoffID, iNumDeliverables, iNumDeliverablesLeft)
					TOGGLE_DELIVERY_FLAG_EXTRA(deliveryData, DELIVERY_FLAG_EXTRA_REPOSITION_PED_AFTER_SCENE, FALSE)
				ELSE
					TOGGLE_DELIVERY_FLAG_EXTRA(deliveryData, DELIVERY_FLAG_EXTRA_REPOSITION_PED_AFTER_SCENE, TRUE)
				ENDIF
			ELSE
				TOGGLE_DELIVERY_FLAG_EXTRA(deliveryData, DELIVERY_FLAG_EXTRA_REPOSITION_PED_AFTER_SCENE, FALSE)
			ENDIF
		ELSE			
			IF iNumDeliverables = iNumDeliverablesHeld
			AND ARE_MULTIPLE_FREEMODE_DELIVERABLES_IN_VEH(deliveryData.eDeliverableID, INVALID_PLAYER_INDEX())
				TOGGLE_DELIVERY_FLAG_EXTRA(deliveryData, DELIVERY_FLAG_EXTRA_REPOSITION_PED_AFTER_SCENE, FALSE)
				CDEBUG1LN(DEBUG_DELIVERY, "DELIVERY_SCENE_DO_DELIVERABLES_CHECK - Doing a mission vehicle delivery. ARE_MULTIPLE_FREEMODE_DELIVERABLES_IN_VEH = TRUE.")
			ELIF bIsMissionVeh
				IF iNumDeliverablesLeft <= 1
				OR ARE_GOONS_NEARBY_WITH_REMAINING_MISSION_DELIVERABLES(deliveryData.eDropoffID, iNumDeliverables, iNumDeliverablesLeft)
					TOGGLE_DELIVERY_FLAG_EXTRA(deliveryData, DELIVERY_FLAG_EXTRA_REPOSITION_PED_AFTER_SCENE, FALSE)
					TOGGLE_DELIVERY_FLAG_EXTRA(deliveryData, DELIVERY_FLAG_EXTRA_RETURN_PV_TO_PROPERTY, FALSE)
					CDEBUG1LN(DEBUG_DELIVERY, "DELIVERY_SCENE_DO_DELIVERABLES_CHECK - Doing a mission vehicle delivery. Last veh to deliver.")
				ELSE
					TOGGLE_DELIVERY_FLAG_EXTRA(deliveryData, DELIVERY_FLAG_EXTRA_REPOSITION_PED_AFTER_SCENE, TRUE)
					TOGGLE_DELIVERY_FLAG_EXTRA(deliveryData, DELIVERY_FLAG_EXTRA_RETURN_PV_TO_PROPERTY, FALSE)
					CDEBUG1LN(DEBUG_DELIVERY, "DELIVERY_SCENE_DO_DELIVERABLES_CHECK - Doing a mission vehicle delivery. More to deliver after this.")
				ENDIF
			ELIF ARE_GOONS_NEARBY_WITH_REMAINING_MISSION_DELIVERABLES(deliveryData.eDropoffID, iNumDeliverables, iNumDeliverablesLeft)
				TOGGLE_DELIVERY_FLAG_EXTRA(deliveryData, DELIVERY_FLAG_EXTRA_REPOSITION_PED_AFTER_SCENE, FALSE)
				TOGGLE_DELIVERY_FLAG_EXTRA(deliveryData, DELIVERY_FLAG_EXTRA_RETURN_PV_TO_PROPERTY, FALSE)
				CDEBUG1LN(DEBUG_DELIVERY, "DELIVERY_SCENE_DO_DELIVERABLES_CHECK - Doing a package delivery in veh. another gang member is nearby with the other package.")
			ELSE
				TOGGLE_DELIVERY_FLAG_EXTRA(deliveryData, DELIVERY_FLAG_EXTRA_REPOSITION_PED_AFTER_SCENE, TRUE)
				TOGGLE_DELIVERY_FLAG_EXTRA(deliveryData, DELIVERY_FLAG_EXTRA_RETURN_PV_TO_PROPERTY, FALSE)
				CDEBUG1LN(DEBUG_DELIVERY, "DELIVERY_SCENE_DO_DELIVERABLES_CHECK - DELIVERY_FLAG_EXTRA_RETURN_PV_TO_PROPERTY - FALSE due to DELIVERY_FLAG_EXTRA_REPOSITION_PED_AFTER_SCENE. Returning true")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC
