//ADD GENERIC LOGIC

USING "globals.sch"
USING "net_simple_cutscene.sch"
USING "Cutscene_help.sch"

USING "GB_DELIVERY_DEFINITIONS.sch"
USING "GB_DELIVERY_GLOBAL.sch"
USING "GB_DELIVERY_CHILD_SUPPORT.sch"
USING "net_freemode_delivery_private.sch"
USING "gang_ops_defunct_base_entry_cutscenes.sch"
USING "FM_CONTENT_CUTSCENE.sch"

/// PURPOSE: // PURPOSE: // PURPOSE: // PURPOSE: // PURPOSE: 
STRUCT GUNRUN_MOVE_AFTER_BUNKER_DELIVERY_DATA
	PLAYER_INDEX 	piBossVehOwner
	MODEL_NAMES 	eVehModel 				= DUMMY_MODEL_FOR_SCRIPT
	SCRIPT_TIMER 	stFadeInBossVehTimer
	INT iPegasusVehOwner
	VEHICLE_INDEX vehVehicleToTmove
ENDSTRUCT

ENUM DELIVERY_FADE_AND_MOVE_ENTITIES_STATE
	MES_FADE_OUT_ENTITIES,
	MES_WARP_VEHICLE,
	MES_WARP_PV,
	MES_WARP_PEGASUS_VEH,
	MES_WARP_PLAYER,
	MES_DELETE_VEHICLE,
	MES_FADE_IN_VEHICLE,
	MES_WARP_BOSS_VEH,
	MES_AWAIT_BOSS_VEH_FADE_IN,
	MES_DONE
ENDENUM

ENUM DELIVERY_DELETE_DELIVERY_VEH_STATE
	DV_STATE_FADE_OUT_VEH,
	DV_STATE_MOVE_OUT_OF_VEH,
	DV_STATE_REGISTER_ENTITY,
	DV_STATE_DELETE,
	DV_STATE_DONE
ENDENUM

GUNRUN_MOVE_AFTER_BUNKER_DELIVERY_DATA thisSceneMoveEntityStruct
DELIVERY_FADE_AND_MOVE_ENTITIES_STATE eFadeAndMoveState
DELIVERY_DELETE_DELIVERY_VEH_STATE eDeleteVehState
////////////////////////////////


ServerBroadcastData serverBD
PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]
SCRIPT_DATA scriptData

FUNC DELIVERY_STATE PRIVATE_GET_DELIVERY_STATE()
	RETURN playerBD[PARTICIPANT_ID_TO_INT()].eState
ENDFUNC

PROC PRIVATE_SET_DELIVERY_STATE(DELIVERY_STATE newState)
	CDEBUG3LN(DEBUG_DELIVERY, "")
	CDEBUG3LN(DEBUG_DELIVERY, "!!! DELIVERY_SET_STATE - Request to switch state from: ", GET_DELIVERY_STATE_NAME(playerBD[PARTICIPANT_ID_TO_INT()].eState), " TO: ", GET_DELIVERY_STATE_NAME(newState))
	
	IF playerBD[PARTICIPANT_ID_TO_INT()].eState <> newState
		playerBD[PARTICIPANT_ID_TO_INT()].eState = newState
	ENDIF
	
	RESET_NET_TIMER(scriptData.coreData.stState)
ENDPROC

FUNC BOOL IS_CARGOBOB_DELIVERY_DROP_HELP_DISPLAYING()
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SMDEL_DROPHELP")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("GODEL_DROPHELP")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_CARGOBOB_DELIVERY_DETATCH_HELP_DISPLAYING()
	IF  IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SMDEL_DETACHHELP")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("GODEL_DETACHHELP")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_CARGOBOB_DELIVERY_HELP_DISPLAYING()
	IF IS_CARGOBOB_DELIVERY_DROP_HELP_DISPLAYING()
	OR IS_CARGOBOB_DELIVERY_DETATCH_HELP_DISPLAYING()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PRINT_CARGOBOB_DETATCH_HELP(FREEMODE_DELIVERY_CUTSCENES eScene)
		IF eScene = GANG_OPS_CUTSCENE_DEFUNCT_BASE
			PRINT_HELP_FOREVER("GODEL_DETACHHELP")
			EXIT
		ENDIF
	
	PRINT_HELP_FOREVER("SMDEL_DETACHHELP")
ENDPROC

PROC PRINT_CARGOBOB_DROP_HELP(FREEMODE_DELIVERY_CUTSCENES eScene)
		IF eScene = GANG_OPS_CUTSCENE_DEFUNCT_BASE
			PRINT_HELP_FOREVER("GODEL_DROPHELP")
			EXIT
		ENDIF
	
	PRINT_HELP_FOREVER("SMDEL_DROPHELP")
ENDPROC

/////////////////////////////////////////////////////////////// BITSETS ///////////////////////////////////////////////////////////////      

/////////// LOCAL BITSET.
FUNC BOOL GET_CORE_BITSET(DELIVERY_CORE_FLAGS bit)
	RETURN IS_BIT_SET_ENUM(scriptData.coreData.iBS, bit)
ENDFUNC

PROC TOGGLE_CORE_BITSET(DELIVERY_CORE_FLAGS bitIndex, BOOL bNewState)
	#IF IS_DEBUG_BUILD
		IF bNewState != GET_CORE_BITSET(bitIndex)
			CDEBUG3LN(DEBUG_DELIVERY, "TOGGLE_CORE_FLAG - Switching: ", GET_CORE_BITSET_NAME(bitIndex), " From: ", IS_BIT_SET_ENUM(scriptData.coreData.iBS, bitIndex), " To: ", bNewState)
		ENDIF
	#ENDIF
	TOGGLE_BIT_ENUM(scriptData.coreData.iBS, bitIndex, bNewState)
ENDPROC

/////////// SCRIPT PARTICIAPNT BITSET - broadcasted bitset
FUNC BOOL GET_PARTICIPANT_BROADCAST_BITSET(PLAYER_BROADCAST_BITSET bit, INT participantInt)
	RETURN IS_BIT_SET_ENUM(playerBD[participantInt].iBS, bit)
ENDFUNC

PROC TOGGLE_PARTICIPANT_BROADCAST_BITSET(PLAYER_BROADCAST_BITSET bitIndex, BOOL bNewState)
	CDEBUG3LN(DEBUG_DELIVERY, "TOGGLE_PBBS - Switching: ", GET_PLAYER_BROADCAST_BITSET_NAME(bitIndex), " From: ", IS_BIT_SET_ENUM(playerBD[PARTICIPANT_ID_TO_INT()].iBS, bitIndex), " To: ", bNewState, " participantId: ", PARTICIPANT_ID_TO_INT())
	TOGGLE_BIT_ENUM(playerBD[PARTICIPANT_ID_TO_INT()].iBS, bitIndex, bNewState)
ENDPROC

//FUNCTIONS
FUNC DELIVERY_TYPE DETERMINE_DELIVERY_TYPE()
	IF IS_ENTITY_DEAD(PLAYER_PED_ID())
		CDEBUG3LN(DEBUG_DELIVERY, "DETERMINE_DELIVERY_TYPE - IS_PLAYER_DEAD returned true, DT_INVALID.")
		RETURN DT_INVALID
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		CDEBUG3LN(DEBUG_DELIVERY, "DETERMINE_DELIVERY_TYPE - IS_PED_IN_ANY_VEHICLE local player is inside of a vehicle.")
		scriptData.deliveryData.deliveryType = DT_VEHICLE
	ELIF IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
		scriptData.deliveryData.deliveryType = DT_VEHICLE
		CDEBUG3LN(DEBUG_DELIVERY, "DETERMINE_DELIVERY_TYPE - IS_PED_IN_ANY_VEHICLE local player is inside of a heli.")
	ELIF IS_PED_IN_ANY_BOAT(PLAYER_PED_ID())
		scriptData.deliveryData.deliveryType = DT_VEHICLE
		CDEBUG3LN(DEBUG_DELIVERY, "DETERMINE_DELIVERY_TYPE - IS_PED_IN_ANY_VEHICLE local player is inside of a boat.")
	ELIF IS_PED_IN_ANY_PLANE(PLAYER_PED_ID())
		scriptData.deliveryData.deliveryType = DT_VEHICLE
		CDEBUG3LN(DEBUG_DELIVERY, "DETERMINE_DELIVERY_TYPE - IS_PED_IN_ANY_VEHICLE local player is inside of a plane.")
	ELSE
		scriptData.deliveryData.deliveryType = DT_FOOT
		scriptData.deliveryData.deliveryVehicle = NULL
		CDEBUG3LN(DEBUG_DELIVERY, "DETERMINE_DELIVERY_TYPE - IS_PED_IN_ANY_VEHICLE local player is not inside of a vehicle.")
	ENDIF
		
	IF scriptData.deliveryData.deliveryType = DT_VEHICLE
		CDEBUG3LN(DEBUG_DELIVERY, "DETERMINE_DELIVERY_TYPE - DT_VEHICLE, trying to grab vehicle reference.")
		scriptData.deliveryData.deliveryVehicle = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
		IF DOES_ENTITY_EXIST(scriptData.deliveryData.deliveryVehicle)
			CDEBUG3LN(DEBUG_DELIVERY, "DETERMINE_DELIVERY_TYPE - DT_VEHICLE, vehicle reference grabbed, player is in: ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(scriptData.deliveryData.deliveryVehicle)))
		ELSE
			CDEBUG3LN(DEBUG_DELIVERY, "DETERMINE_DELIVERY_TYPE - DT_VEHICLE, was unable to grab vehicle reference.")
		ENDIF
	ENDIF
	
	IF scriptData.deliveryData.deliveryType = DT_INVALID
		ASSERTLN("AM_DELIVERY DETERMINE_DELIVERY_TYPE - was unable to determine player delivery type.")
	ENDIF
	
	RETURN scriptData.deliveryData.deliveryType
ENDFUNC

FUNC ENTITY_INDEX GET_DELIVERY_ENTITY()
	ENTITY_INDEX returnEntity
	
	returnEntity = PLAYER_PED_ID()
	
	IF scriptData.deliveryData.deliveryType = DT_VEHICLE
		IF DOES_ENTITY_EXIST(scriptData.deliveryData.deliveryVehicle)
			returnEntity = scriptData.deliveryData.deliveryVehicle
		ENDIF
	ENDIF

	RETURN returnEntity
ENDFUNC

PROC MAINTAIN_PLAYER_FADE_OUT()
//	NETWORK_FADE_OUT_ENTITY(entryStruct.vehiclePackArray[PIT_VEHICLE_PLAYER_IS_IN], TRUE, TRUE)
//	FREEZE_ENTITY_POSITION(entryStruct.vehiclePackArray[PIT_VEHICLE_PLAYER_IS_IN], TRUE)
//	SET_ENTITY_COLLISION(entryStruct.vehiclePackArray[PIT_VEHICLE_PLAYER_IS_IN], FALSE)
//	SET_ENTITY_CAN_BE_DAMAGED(entryStruct.vehiclePackArray[PIT_VEHICLE_ATTACHED], FALSE)
ENDPROC

FUNC BOOL DELIVERY_HAS_STREAM_ASSETS_FINISHED()
	INT indexGeneral
	
	REPEAT CONST_DELIVERY_MAX_VEHICLES_PACK indexGeneral
		//Ped Streaming.
		IF NOT CUTSCENE_HELP_HAS_PED_ARRAY_LOADING_FINISHED(scriptData.deliveryData.cutsceneData.vehiclePackArray[indexGeneral].passengers)
			CDEBUG3LN(DEBUG_DELIVERY, "DELIVERY_HAS_STREAM_ASSETS_FINISHED - Passenger peds have not finished loading, indexGeneral: ", indexGeneral)  
			RETURN FALSE
		ENDIF

		//Vehicle streaming
		IF DOES_ENTITY_EXIST(scriptData.deliveryData.cutsceneData.vehiclePackArray[indexGeneral].vehicle) AND NOT IS_ENTITY_DEAD(scriptData.deliveryData.cutsceneData.vehiclePackArray[indexGeneral].vehicle)
			IF NOT HAVE_VEHICLE_MODS_STREAMED_IN(scriptData.deliveryData.cutsceneData.vehiclePackArray[indexGeneral].vehicle)
				CDEBUG3LN(DEBUG_DELIVERY, "DELIVERY_HAS_STREAM_ASSETS_FINISHED - Straming vehiclePackArray, waiting for indexGeneral: ", indexGeneral)  
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	
	//Extra vehicles
	REPEAT CONST_DELIVERY_MAX_VEHICLES_EXTRA indexGeneral
		IF DOES_ENTITY_EXIST(scriptData.deliveryData.cutsceneData.vehicleArray[indexGeneral]) AND NOT IS_ENTITY_DEAD(scriptData.deliveryData.cutsceneData.vehicleArray[indexGeneral])
			IF NOT HAVE_VEHICLE_MODS_STREAMED_IN(scriptData.deliveryData.cutsceneData.vehicleArray[indexGeneral])
				CDEBUG3LN(DEBUG_DELIVERY, "DELIVERY_HAS_STREAM_ASSETS_FINISHED - Straming vehicleArray, waiting for indexGeneral: ", indexGeneral)  
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	//Extra peds
	IF NOT CUTSCENE_HELP_HAS_PED_ARRAY_LOADING_FINISHED(scriptData.deliveryData.cutsceneData.pedArray)
		CDEBUG3LN(DEBUG_DELIVERY, "DELIVERY_HAS_STREAM_ASSETS_FINISHED - Extra peds have not finished loading.")  
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL DELIVERY_CREATE_CLONES()
	SWITCH scriptData.deliveryData.deliveryType
		CASE DT_VEHICLE
			IF DOES_ENTITY_EXIST(scriptData.deliveryData.cutsceneData.vehiclePackArray[CONST_DELIVERY_VEH_PED_IS_IN].vehicle)
				CDEBUG3LN(DEBUG_DELIVERY, "DELIVERY_CREATE_CLONES - Cloning peds for player clone vehicle..")
				IF DOES_ENTITY_EXIST(scriptData.deliveryData.deliveryVehicleAttached)
					CUTSCENE_HELP_VEH_PASSENGERS_CLONE(scriptData.deliveryData.deliveryVehicle, scriptData.deliveryData.cutsceneData.vehiclePackArray[CONST_DELIVERY_VEH_PED_IS_IN].vehicle, scriptData.deliveryData.cutsceneData.vehiclePackArray[CONST_DELIVERY_VEH_PED_IS_IN].passengers, !GET_DELIVERY_FLAG_EXTRA(scriptData.deliveryData, DELIVERY_FLAG_EXTRA_CLONE_NON_PLAYER_PEDS))
				ELSE
					RETURN CUTSCENE_HELP_VEH_PASSENGERS_CLONE(scriptData.deliveryData.deliveryVehicle, scriptData.deliveryData.cutsceneData.vehiclePackArray[CONST_DELIVERY_VEH_PED_IS_IN].vehicle, scriptData.deliveryData.cutsceneData.vehiclePackArray[CONST_DELIVERY_VEH_PED_IS_IN].passengers, !GET_DELIVERY_FLAG_EXTRA(scriptData.deliveryData, DELIVERY_FLAG_EXTRA_CLONE_NON_PLAYER_PEDS), TRUE, GET_DELIVERY_FLAG_EXTRA(scriptData.deliveryData, DELIVERY_FLAG_EXTRA_CLONE_DEAD_PEDS), TRUE, GET_DELIVERY_FLAG_EXTRA(scriptData.deliveryData, DELIVERY_FLAG_EXTRA_CLONE_ONLY_DRIVER))
				ENDIF
				
				IF DOES_ENTITY_EXIST(scriptData.deliveryData.cutsceneData.vehiclePackArray[CONST_DELIVERY_VEH_ATTACHED].vehicle)
					CDEBUG3LN(DEBUG_DELIVERY, "DELIVERY_CREATE_CLONES - Attached vehicle exists, try clone peds for it.")
					RETURN CUTSCENE_HELP_VEH_PASSENGERS_CLONE(scriptData.deliveryData.deliveryVehicleAttached, scriptData.deliveryData.cutsceneData.vehiclePackArray[CONST_DELIVERY_VEH_ATTACHED].vehicle, scriptData.deliveryData.cutsceneData.vehiclePackArray[CONST_DELIVERY_VEH_ATTACHED].passengers, TRUE, TRUE)
				ENDIF
				
				RETURN TRUE
			ELSE
				IF DOES_ENTITY_EXIST(scriptData.deliveryData.deliveryVehicle)
				AND NOT IS_ENTITY_DEAD(scriptData.deliveryData.deliveryVehicle)
					CDEBUG3LN(DEBUG_DELIVERY, "DELIVERY_CREATE_CLONES - Delivery vehicle is: ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(scriptData.deliveryData.deliveryVehicle)))
					VEHICLE_INDEX VehicleTrailerAttached
					
					CDEBUG3LN(DEBUG_DELIVERY, "DELIVERY_CREATE_CLONES::: IS_VEHICLE_CARGOBOB: ", IS_VEHICLE_CARGOBOB(scriptData.deliveryData.deliveryVehicle),
					" GET_VEHICLE_TRAILER_VEHICLE: ", GET_VEHICLE_TRAILER_VEHICLE(scriptData.deliveryData.deliveryVehicle, VehicleTrailerAttached),
					" IS_VEHICLE_TOW_TRUCK: ", IS_VEHICLE_TOW_TRUCK(scriptData.deliveryData.deliveryVehicle),
					" Attached_to_any_vehicle: ", IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(scriptData.deliveryData.deliveryVehicle))
					
					IF IS_VEHICLE_CARGOBOB(scriptData.deliveryData.deliveryVehicle)
						CDEBUG3LN(DEBUG_DELIVERY, "DELIVERY_CREATE_CLONES,IS_VEHICLE_CARGOBOB - localData.deliveryVehicle - is a cargobob")
						

						ENTITY_INDEX CargobobAttached
						CargobobAttached = GET_ENTITY_ATTACHED_TO_CARGOBOB(scriptData.deliveryData.deliveryVehicle)
						
						IF DOES_ENTITY_EXIST(CargobobAttached)
							IF IS_ENTITY_AN_OBJECT(CargobobAttached)
								CDEBUG3LN(DEBUG_DELIVERY, "DELIVERY_CREATE_CLONES,IS_VEHICLE_CARGOBOB - cargobob has a entity attached: ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(CargobobAttached)))
								scriptData.deliveryData.deliveryVehicleObjectAttached = CREATE_OBJECT_NO_OFFSET(GET_ENTITY_MODEL(CargobobAttached), <<0.0, -30.0, -20.0>>, FALSE, FALSE)
								IF DOES_ENTITY_EXIST(scriptData.deliveryData.deliveryVehicleObjectAttached)
									CDEBUG3LN(DEBUG_DELIVERY, "DELIVERY_CREATE_CLONES,IS_VEHICLE_CARGOBOB - cargobob attached entity clone created successfully")
									INT tint
									tint = GET_OBJECT_TINT_INDEX(GET_OBJECT_INDEX_FROM_ENTITY_INDEX(CargobobAttached))
									CDEBUG3LN(DEBUG_DELIVERY, "DELIVERY_CREATE_CLONES,IS_VEHICLE_CARGOBOB - cargobob attached entity clone, tint: ", tint)
									SET_OBJECT_TINT_INDEX(scriptData.deliveryData.deliveryVehicleObjectAttached, tint)
								ELSE
									CDEBUG3LN(DEBUG_DELIVERY, "DELIVERY_CREATE_CLONES,IS_VEHICLE_CARGOBOB - cargobob attached entity clone clone error.")
								ENDIF
							ELIF IS_ENTITY_A_VEHICLE(CargobobAttached)
								CDEBUG3LN(DEBUG_DELIVERY, "DELIVERY_CREATE_CLONES,IS_VEHICLE_CARGOBOB - cargobob has a vehicle attached: ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(CargobobAttached)))
								scriptData.deliveryData.deliveryVehicleAttached = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(CargobobAttached)
								scriptData.deliveryData.deliveryVehicleAttachedType = VA_CARGOBOB_HAS_CARGO_ATTACHED
							ELSE
								CDEBUG3LN(DEBUG_DELIVERY, "DELIVERY_CREATE_CLONES,IS_VEHICLE_CARGOBOB - cargobob has a error attached, model ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(CargobobAttached)))
							ENDIF
						ENDIF

					ELIF GET_VEHICLE_TRAILER_VEHICLE(scriptData.deliveryData.deliveryVehicle, VehicleTrailerAttached)
						CDEBUG3LN(DEBUG_DELIVERY, "DELIVERY_CREATE_CLONES,GET_VEHICLE_TRAILER_VEHICLE - Truck has a trailer attached: ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(VehicleTrailerAttached)))
						scriptData.deliveryData.deliveryVehicleAttached = VehicleTrailerAttached
						scriptData.deliveryData.deliveryVehicleAttachedType = VA_TRUCK_HAS_TRAILER_ATTACHED
						
					ELIF IS_VEHICLE_TOW_TRUCK(scriptData.deliveryData.deliveryVehicle)
						CDEBUG3LN(DEBUG_DELIVERY, "DELIVERY_CREATE_CLONES,IS_VEHICLE_TOW_TRUCK - localData.deliveryVehicle - is a cargobob")
						ENTITY_INDEX entityAttachedTo
						entityAttachedTo =  GET_ENTITY_ATTACHED_TO_TOW_TRUCK(scriptData.deliveryData.deliveryVehicle)
						scriptData.deliveryData.deliveryVehicleAttached =	GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(entityAttachedTo)
						
						IF DOES_ENTITY_EXIST(scriptData.deliveryData.deliveryVehicleAttached)
							CDEBUG3LN(DEBUG_DELIVERY, "DELIVERY_CREATE_CLONES,IS_VEHICLE_TOW_TRUCK - cargobob has a vehicle attached: ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(scriptData.deliveryData.deliveryVehicleAttached)))
							scriptData.deliveryData.deliveryVehicleAttachedType = VA_TOWTRUCK_HAS_CARGO_ATTACHED
						ELSE
							CDEBUG3LN(DEBUG_DELIVERY, "DELIVERY_CREATE_CLONES,IS_VEHICLE_TOW_TRUCK - was unable to grab reference to anything attached to cargobob")
						ENDIF
						
					ELSE
						IF IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(scriptData.deliveryData.deliveryVehicle)
							ENTITY_INDEX entityAttachedTo
							entityAttachedTo = GET_ENTITY_ATTACHED_TO(scriptData.deliveryData.deliveryVehicle)
							IF DOES_ENTITY_EXIST(entityAttachedTo)
								CDEBUG3LN(DEBUG_DELIVERY, "DELIVERY_CREATE_CLONES - entityAttachedTo exists: ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(entityAttachedTo)))
								
								VEHICLE_INDEX vehAttachedTo
								vehAttachedTo = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(entityAttachedTo)
								IF DOES_ENTITY_EXIST(vehAttachedTo)
								AND NOT IS_ENTITY_DEAD(vehAttachedTo)
									CDEBUG3LN(DEBUG_DELIVERY, "DELIVERY_CREATE_CLONES - vehAttachedTo exists, converted ok: ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(vehAttachedTo)))

									IF IS_VEHICLE_ATTACHED_TO_CARGOBOB(vehAttachedTo, scriptData.deliveryData.deliveryVehicle)
										scriptData.deliveryData.deliveryVehicleAttached = vehAttachedTo
										scriptData.deliveryData.deliveryVehicleAttachedType = VA_ATTACHED_TO_CARGOBOB
										CDEBUG3LN(DEBUG_DELIVERY, "DELIVERY_CREATE_CLONES - IS_VEHICLE_ATTACHED_TO_CARGOBOB")
									ELIF IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(vehAttachedTo, scriptData.deliveryData.deliveryVehicle)
										scriptData.deliveryData.deliveryVehicleAttached = vehAttachedTo
										scriptData.deliveryData.deliveryVehicleAttachedType = VA_ATTACHED_TO_TOWTRUCK
										CDEBUG3LN(DEBUG_DELIVERY, "DELIVERY_CREATE_CLONES - IS_VEHICLE_ATTACHED_TO_TOW_TRUCK")
									ELIF IS_VEHICLE_ATTACHED_TO_TRAILER(scriptData.deliveryData.deliveryVehicle)
										scriptData.deliveryData.deliveryVehicleAttached = vehAttachedTo
										scriptData.deliveryData.deliveryVehicleAttachedType = VA_ATTACHED_TO_TRAILER
										CDEBUG3LN(DEBUG_DELIVERY, "DELIVERY_CREATE_CLONES - IS_VEHICLE_ATTACHED_TO_TRAILER")
									ENDIF
								ELSE
									CDEBUG3LN(DEBUG_DELIVERY, "DELIVERY_CREATE_CLONES - DOES_ENTITY_EXIST(vehAttachedTo) - unable to grab reference.")
								ENDIF
							ELSE
								CDEBUG3LN(DEBUG_DELIVERY, "DELIVERY_CREATE_CLONES - DOES_ENTITY_EXIST(entityAttachedTo) - unable to grab reference.")
							ENDIF
						ELSE
							CDEBUG3LN(DEBUG_DELIVERY, "DELIVERY_CREATE_CLONES - IS_ENTITY_ATTACHED_TO_ANY_VEHICLE, not attached to anything")
						ENDIF
					ENDIF
					
					IF NOT GET_DELIVERY_FLAG_DEFAULT(scriptData.deliveryData, DELIVERY_FLAG_DEFAULT_CLONE_NORMAL_AVENGER)
						CREATE_AVENGER2_CLONE(scriptData.deliveryData.deliveryVehicle, scriptData.deliveryData.cutsceneData.vehiclePackArray[CONST_DELIVERY_VEH_PED_IS_IN].vehicle, chVEHICLE_CLONE_ONLY_PLAYER_PASSENGERS, <<0.0, 0.0, -50.0>>)
					ELSE
						CREATE_VEHICLE_CLONE(scriptData.deliveryData.cutsceneData.vehiclePackArray[CONST_DELIVERY_VEH_PED_IS_IN].vehicle, scriptData.deliveryData.deliveryVehicle, <<0.0, 0.0, -50.0>>, GET_ENTITY_HEADING(scriptData.deliveryData.deliveryVehicle), FALSE, FALSE)
						VEHICLE_COPY_EXTRAS(scriptData.deliveryData.deliveryVehicle, scriptData.deliveryData.cutsceneData.vehiclePackArray[CONST_DELIVERY_VEH_PED_IS_IN].vehicle)
					ENDIF
					
					IF DOES_ENTITY_EXIST(scriptData.deliveryData.cutsceneData.vehiclePackArray[CONST_DELIVERY_VEH_PED_IS_IN].vehicle)
						SET_ENTITY_COLLISION(scriptData.deliveryData.cutsceneData.vehiclePackArray[CONST_DELIVERY_VEH_PED_IS_IN].vehicle, FALSE)
						FREEZE_ENTITY_POSITION(scriptData.deliveryData.cutsceneData.vehiclePackArray[CONST_DELIVERY_VEH_PED_IS_IN].vehicle, TRUE)
						SET_ENTITY_INVINCIBLE(scriptData.deliveryData.cutsceneData.vehiclePackArray[CONST_DELIVERY_VEH_PED_IS_IN].vehicle, TRUE)
						CUTSCENE_HELP_DISABLE_VEHICLE_AUTOMATIC_CRASH(scriptData.deliveryData.cutsceneData.vehiclePackArray[CONST_DELIVERY_VEH_PED_IS_IN].vehicle)
						CDEBUG3LN(DEBUG_DELIVERY, "DELIVERY_CREATE_CLONES - scriptData.deliveryData.cutsceneData.vehiclePackArray[0].vehicle, clone created sucesfully")
					ELSE
						ASSERTLN("DELIVERY_CREATE_CLONES - was unable to create main vehicle clone.")
					ENDIF

					IF DOES_ENTITY_EXIST(scriptData.deliveryData.deliveryVehicleAttached)
					AND IS_ENTITY_ALIVE(scriptData.deliveryData.deliveryVehicleAttached)
						CDEBUG3LN(DEBUG_DELIVERY, "DELIVERY_CREATE_CLONES - delivery vehicle is attached to: ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(scriptData.deliveryData.deliveryVehicleAttached)))
						IF IS_ENTITY_A_VEHICLE(scriptData.deliveryData.deliveryVehicleAttached)
							CDEBUG3LN(DEBUG_DELIVERY, "DELIVERY_CREATE_CLONES - attached vehicle create clone")
							CREATE_VEHICLE_CLONE(scriptData.deliveryData.cutsceneData.vehiclePackArray[CONST_DELIVERY_VEH_ATTACHED].vehicle, scriptData.deliveryData.deliveryVehicleAttached, <<0.0, 0.0, -100.0>>, GET_ENTITY_HEADING(scriptData.deliveryData.deliveryVehicleAttached), FALSE, FALSE)
							
						ELIF IS_ENTITY_AN_OBJECT(scriptData.deliveryData.deliveryVehicleAttached)
							CDEBUG3LN(DEBUG_DELIVERY, "DELIVERY_CREATE_CLONES - attached entity create clone")
							CREATE_OBJECT(GET_ENTITY_MODEL(scriptData.deliveryData.deliveryVehicleAttached), <<10.0, 10.0, 10.0>>, FALSE, FALSE)
						ENDIF
						
						IF DOES_ENTITY_EXIST(scriptData.deliveryData.cutsceneData.vehiclePackArray[CONST_DELIVERY_VEH_ATTACHED].vehicle)
							SET_ENTITY_COLLISION(scriptData.deliveryData.cutsceneData.vehiclePackArray[CONST_DELIVERY_VEH_ATTACHED].vehicle, FALSE)
							FREEZE_ENTITY_POSITION(scriptData.deliveryData.cutsceneData.vehiclePackArray[CONST_DELIVERY_VEH_ATTACHED].vehicle, TRUE)
							SET_ENTITY_INVINCIBLE(scriptData.deliveryData.cutsceneData.vehiclePackArray[CONST_DELIVERY_VEH_ATTACHED].vehicle, TRUE)
							CUTSCENE_HELP_DISABLE_VEHICLE_AUTOMATIC_CRASH(scriptData.deliveryData.cutsceneData.vehiclePackArray[CONST_DELIVERY_VEH_PED_IS_IN].vehicle)
						ENDIF
					ELSE
						CDEBUG3LN(DEBUG_DELIVERY, "DELIVERY_CREATE_CLONES - localData.deliveryVehicleAttached reference does not exist")
					ENDIF

					IF DOES_ENTITY_EXIST(scriptData.deliveryData.cutsceneData.vehiclePackArray[CONST_DELIVERY_VEH_PED_IS_IN].vehicle)
					AND DOES_ENTITY_EXIST(scriptData.deliveryData.cutsceneData.vehiclePackArray[CONST_DELIVERY_VEH_ATTACHED].vehicle)
						CDEBUG3LN(DEBUG_DELIVERY, "DELIVERY_CREATE_CLONES - both vehicles exist, attachment type: ", GET_VEHICLE_ATTACH_NAME(scriptData.deliveryData.deliveryVehicleAttachedType))
						SWITCH scriptData.deliveryData.deliveryVehicleAttachedType
							CASE VA_ATTACHED_TO_CARGOBOB
								//ATTACH_VEHICLE_TO_CARGOBOB(scriptData.deliveryData.cutsceneData.vehiclePackArray[1].vehicle, scriptData.deliveryData.cutsceneData.vehiclePackArray[0].vehicle, -1, <<0.0,0.0,0.0>>)
								CDEBUG3LN(DEBUG_DELIVERY, "DELIVERY_CREATE_CLONES - ATTACH_VEHICLE_TO_CARGOBOB")
							BREAK
							
							CASE VA_ATTACHED_TO_TOWTRUCK
								ATTACH_VEHICLE_TO_TOW_TRUCK(scriptData.deliveryData.cutsceneData.vehiclePackArray[1].vehicle, scriptData.deliveryData.cutsceneData.vehiclePackArray[0].vehicle, -1, <<0.0, 0.0, 0.0>>)
								CDEBUG3LN(DEBUG_DELIVERY, "DELIVERY_CREATE_CLONES - ATTACH_VEHICLE_TO_TOW_TRUCK")
							BREAK
							
							CASE VA_ATTACHED_TO_TRAILER
								ATTACH_VEHICLE_TO_TRAILER(scriptData.deliveryData.cutsceneData.vehiclePackArray[1].vehicle, scriptData.deliveryData.cutsceneData.vehiclePackArray[0].vehicle)
								CDEBUG3LN(DEBUG_DELIVERY, "DELIVERY_CREATE_CLONES - VA_ATTACHED_TO_TRAILER")
							BREAK
							
							CASE VA_CARGOBOB_HAS_CARGO_ATTACHED
								//ATTACH_VEHICLE_TO_TRAILER(scriptData.deliveryData.cutsceneData.vehiclePackArray[0].vehicle, scriptData.deliveryData.cutsceneData.vehiclePackArray[1].vehicle)
								CDEBUG3LN(DEBUG_DELIVERY, "DELIVERY_CREATE_CLONES - VA_CARGOBOB_HAS_CARGO_ATTACHED")
							BREAK
							
							CASE VA_TOWTRUCK_HAS_CARGO_ATTACHED
								ATTACH_VEHICLE_TO_TOW_TRUCK(scriptData.deliveryData.cutsceneData.vehiclePackArray[0].vehicle, scriptData.deliveryData.cutsceneData.vehiclePackArray[1].vehicle, -1, <<0.0, 0.0, 0.0>>)
								CDEBUG3LN(DEBUG_DELIVERY, "DELIVERY_CREATE_CLONES - VA_TOWTRUCK_HAS_CARGO_ATTACHED")
							BREAK
							CASE VA_TRUCK_HAS_TRAILER_ATTACHED
								ATTACH_VEHICLE_TO_TRAILER(scriptData.deliveryData.cutsceneData.vehiclePackArray[0].vehicle, scriptData.deliveryData.cutsceneData.vehiclePackArray[1].vehicle)
								CDEBUG3LN(DEBUG_DELIVERY, "DELIVERY_CREATE_CLONES - VA_TRUCK_HAS_TRAILER_ATTACHED")
							BREAK
						ENDSWITCH
					ELSE
						CDEBUG3LN(DEBUG_DELIVERY, "DELIVERY_CREATE_CLONES - 0: ", DOES_ENTITY_EXIST(scriptData.deliveryData.cutsceneData.vehiclePackArray[0].vehicle), " 1: ", DOES_ENTITY_EXIST(scriptData.deliveryData.cutsceneData.vehiclePackArray[1].vehicle))
					ENDIF
				ELSE
					ASSERTLN("DELIVERY_CREATE_CLONES - delivery type is vehicle, but no vehicle reference")
				ENDIF
				
				CDEBUG3LN(DEBUG_DELIVERY, "##DELIVERY_CREATE_CLONES: ", GET_VEHICLE_ATTACH_NAME(scriptData.deliveryData.deliveryVehicleAttachedType))
			ENDIF
		BREAK
		
		CASE DT_FOOT
			IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
			AND NOT DOES_ENTITY_EXIST(scriptData.deliveryData.cutsceneData.pedArray[0])
				CUTSCENE_HELP_CLONE_PLAYER(scriptData.deliveryData.cutsceneData.pedArray[0], PLAYER_ID())
				IF DOES_ENTITY_EXIST(scriptData.deliveryData.cutsceneData.pedArray[0])
				AND NOT IS_ENTITY_DEAD(scriptData.deliveryData.cutsceneData.pedArray[0])
					FREEZE_ENTITY_POSITION(scriptData.deliveryData.cutsceneData.pedArray[0], TRUE)
					SET_ENTITY_COLLISION(scriptData.deliveryData.cutsceneData.pedArray[0], FALSE)
					SET_ENTITY_COORDS_NO_OFFSET(scriptData.deliveryData.cutsceneData.pedArray[0], GET_ENTITY_COORDS(scriptData.deliveryData.cutsceneData.pedArray[0]) - <<0.0, 0.0, 10.0>>)
				ENDIF
			ELSE
				ASSERTLN("DELIVERY_CREATE_CLONES - was unable to clone player, DOES_ENTITY_EXIST-Player: ", DOES_ENTITY_EXIST(PLAYER_PED_ID()), " DOES_ENTITY_EXIST-Clone: ", DOES_ENTITY_EXIST(scriptData.deliveryData.cutsceneData.pedArray[0]))
			ENDIF
			
		RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

//HIDE ASSETS BEFORE CUTSCEN IS READY
PROC DELIVERY_TOGGLE_ASSETS_READY(BOOL bReady)
	CDEBUG3LN(DEBUG_DELIVERY, "DELIVERY_TOGGLE_ASSETS_READY - new state: ", bReady)
	INT index, sizeArray
		
	//Vehicle struct
	REPEAT CONST_DELIVERY_MAX_VEHICLES_PACK index
		//Vehicle
		IF DOES_ENTITY_EXIST(scriptData.deliveryData.cutsceneData.vehiclePackArray[index].vehicle)
		AND NOT IS_ENTITY_DEAD(scriptData.deliveryData.cutsceneData.vehiclePackArray[index].vehicle)
			//Force vehicle to be off and silent while not in a cutscene.
			IF NOT bReady
				CUTSCENE_HELP_PREPARE_VEHICLE_FOR_SCENE(scriptData.deliveryData.cutsceneData.vehiclePackArray[index].vehicle, FALSE)
			ENDIF
			SET_ENTITY_VISIBLE(scriptData.deliveryData.cutsceneData.vehiclePackArray[index].vehicle, bReady)
			IF NOT IS_ENTITY_ATTACHED(scriptData.deliveryData.cutsceneData.vehiclePackArray[index].vehicle)
				SET_ENTITY_COLLISION(scriptData.deliveryData.cutsceneData.vehiclePackArray[index].vehicle, bReady)
				FREEZE_ENTITY_POSITION(scriptData.deliveryData.cutsceneData.vehiclePackArray[index].vehicle, NOT bReady)
			ENDIF
		ENDIF
		
		//Passengers
		INT pedIndex
		REPEAT MAX_NO_VEH_PASSENGERS pedIndex
			IF DOES_ENTITY_EXIST(scriptData.deliveryData.cutsceneData.vehiclePackArray[index].passengers[pedIndex])
			AND NOT IS_ENTITY_DEAD(scriptData.deliveryData.cutsceneData.vehiclePackArray[index].passengers[pedIndex])
				SET_ENTITY_VISIBLE(scriptData.deliveryData.cutsceneData.vehiclePackArray[index].passengers[pedIndex], bReady)
				IF NOT IS_PED_IN_ANY_VEHICLE(scriptData.deliveryData.cutsceneData.vehiclePackArray[index].passengers[pedIndex])
					SET_ENTITY_COLLISION(scriptData.deliveryData.cutsceneData.vehiclePackArray[index].passengers[pedIndex], bReady)
					FREEZE_ENTITY_POSITION(scriptData.deliveryData.cutsceneData.vehiclePackArray[index].passengers[pedIndex], NOT bReady)
				ENDIF
			ENDIF
		ENDREPEAT
	ENDREPEAT
	
	//Objects
	sizeArray = COUNT_OF(scriptData.deliveryData.cutsceneData.objectArray)
	REPEAT sizeArray index
		IF DOES_ENTITY_EXIST(scriptData.deliveryData.cutsceneData.objectArray[index])
		AND NOT IS_ENTITY_DEAD(scriptData.deliveryData.cutsceneData.objectArray[index])
			SET_ENTITY_VISIBLE(scriptData.deliveryData.cutsceneData.objectArray[index], bReady)
			IF NOT IS_ENTITY_STATIC(scriptData.deliveryData.cutsceneData.objectArray[index])
				SET_ENTITY_COLLISION(scriptData.deliveryData.cutsceneData.objectArray[index], bReady)
				FREEZE_ENTITY_POSITION(scriptData.deliveryData.cutsceneData.objectArray[index], NOT bReady)
			ENDIF
		ENDIF
	ENDREPEAT
	
	//Peds
	sizeArray = COUNT_OF(scriptData.deliveryData.cutsceneData.pedArray)
	REPEAT sizeArray index
		IF DOES_ENTITY_EXIST(scriptData.deliveryData.cutsceneData.pedArray[index])
		AND NOT IS_ENTITY_DEAD(scriptData.deliveryData.cutsceneData.pedArray[index])	
			SET_ENTITY_VISIBLE(scriptData.deliveryData.cutsceneData.pedArray[index], bReady)
			IF NOT IS_PED_IN_ANY_VEHICLE(scriptData.deliveryData.cutsceneData.pedArray[index])
				SET_ENTITY_COLLISION(scriptData.deliveryData.cutsceneData.pedArray[index], bReady)
				FREEZE_ENTITY_POSITION(scriptData.deliveryData.cutsceneData.pedArray[index], NOT bReady)
			ENDIF
		ENDIF
	ENDREPEAT
	
	REPEAT CONST_DELIVERY_MAX_VEHICLES_EXTRA index
		//Vehicle
		IF DOES_ENTITY_EXIST(scriptData.deliveryData.cutsceneData.vehicleArray[index])
		AND NOT IS_ENTITY_DEAD(scriptData.deliveryData.cutsceneData.vehicleArray[index])
			//Force vehicle to be off and silent while not in a cutscene.
			IF NOT bReady
				CUTSCENE_HELP_PREPARE_VEHICLE_FOR_SCENE(scriptData.deliveryData.cutsceneData.vehicleArray[index], FALSE)
			ENDIF
			SET_ENTITY_VISIBLE(scriptData.deliveryData.cutsceneData.vehicleArray[index], bReady)
			IF NOT IS_ENTITY_ATTACHED(scriptData.deliveryData.cutsceneData.vehicleArray[index])
				SET_ENTITY_COLLISION(scriptData.deliveryData.cutsceneData.vehicleArray[index], bReady)
				FREEZE_ENTITY_POSITION(scriptData.deliveryData.cutsceneData.vehicleArray[index], NOT bReady)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC DELIVERY_DELETE_ASSETS()
	INT indexGeneral
	INT index
	
	REPEAT CONST_DELIVERY_MAX_VEHICLES_PACK indexGeneral
		//Ped
		REPEAT MAX_NO_VEH_PASSENGERS index
			IF DOES_ENTITY_EXIST(scriptData.deliveryData.cutsceneData.vehiclePackArray[indexGeneral].passengers[index])
				DELETE_PED(scriptData.deliveryData.cutsceneData.vehiclePackArray[indexGeneral].passengers[index])
			ENDIF
		ENDREPEAT
		
		//Vehicle
		IF DOES_ENTITY_EXIST(scriptData.deliveryData.cutsceneData.vehiclePackArray[indexGeneral].vehicle)
			DELETE_VEHICLE(scriptData.deliveryData.cutsceneData.vehiclePackArray[indexGeneral].vehicle)
		ENDIF
	ENDREPEAT
	
	//Objects
	REPEAT CONST_DELIVERY_MAX_OBJECTS indexGeneral
		IF DOES_ENTITY_EXIST(scriptData.deliveryData.cutsceneData.objectArray[indexGeneral])
			DELETE_OBJECT(scriptData.deliveryData.cutsceneData.objectArray[indexGeneral])
		ENDIF
	ENDREPEAT
	
	//Additional peds
	REPEAT CONST_DELIVERY_MAX_PEDS_EXTRA indexGeneral
		IF DOES_ENTITY_EXIST(scriptData.deliveryData.cutsceneData.pedArray[indexGeneral])
			DELETE_PED(scriptData.deliveryData.cutsceneData.pedArray[indexGeneral])
		ENDIF
	ENDREPEAT
	
	//Aditional vehicles
	REPEAT CONST_DELIVERY_MAX_VEHICLES_EXTRA indexGeneral
		IF DOES_ENTITY_EXIST(scriptData.deliveryData.cutsceneData.vehicleArray[indexGeneral])
			DELETE_VEHICLE(scriptData.deliveryData.cutsceneData.vehicleArray[indexGeneral])
		ENDIF
	ENDREPEAT
	
	IF DOES_ENTITY_EXIST(scriptData.deliveryData.deliveryVehicleObjectAttached)
		DELETE_OBJECT(scriptData.deliveryData.deliveryVehicleObjectAttached)
	ENDIF
	
	CDEBUG3LN(DEBUG_DELIVERY, "DELIVERY_DELETE_ASSETS - finished.") 
ENDPROC

PROC DELIVERY_MAINTAIN_FORCE_HD_VEHICLE()
	INT indexGeneral
	
	REPEAT CONST_DELIVERY_MAX_VEHICLES_PACK indexGeneral
		IF DOES_ENTITY_EXIST(scriptData.deliveryData.cutsceneData.vehiclePackArray[indexGeneral].vehicle)
		AND NOT IS_ENTITY_DEAD(scriptData.deliveryData.cutsceneData.vehiclePackArray[indexGeneral].vehicle)
			SET_FORCE_HD_VEHICLE(scriptData.deliveryData.cutsceneData.vehiclePackArray[indexGeneral].vehicle, TRUE)
		ENDIF
	ENDREPEAT
	
	//EXTRA
	REPEAT CONST_DELIVERY_MAX_VEHICLES_EXTRA indexGeneral
		IF DOES_ENTITY_EXIST(scriptData.deliveryData.cutsceneData.vehicleArray[indexGeneral])
		AND NOT IS_ENTITY_DEAD(scriptData.deliveryData.cutsceneData.vehicleArray[indexGeneral])
			SET_FORCE_HD_VEHICLE(scriptData.deliveryData.cutsceneData.vehicleArray[indexGeneral], TRUE)
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Toggle current night vision state
/// PARAMS:
///    bEnable - 
PROC DELIVERY_TOGGLE_NIGHT_VISION(BOOL bEnable)
	IF bEnable
		IF GET_REQUESTINGNIGHTVISION()
			CDEBUG3LN(DEBUG_DELIVERY, "DELIVERY_TOGGLE_NIGHT_VISION - can not enable NV, already enabled.") 
			EXIT
		ENDIF
		
		CDEBUG3LN(DEBUG_DELIVERY, "DELIVERY_TOGGLE_NIGHT_VISION - ENABLE_NIGHTVISION(VISUALAID_NIGHT)") 
		ENABLE_NIGHTVISION(VISUALAID_NIGHT)
	ELSE
		IF NOT GET_REQUESTINGNIGHTVISION()
			CDEBUG3LN(DEBUG_DELIVERY, "DELIVERY_TOGGLE_NIGHT_VISION - can not disable NV, already disabled.") 
			EXIT
		ENDIF
		
		CDEBUG3LN(DEBUG_DELIVERY, "DELIVERY_TOGGLE_NIGHT_VISION - ENABLE_NIGHTVISION(VISUALAID_OFF)") 
		ENABLE_NIGHTVISION(VISUALAID_OFF)
	ENDIF

ENDPROC

/// PURPOSE:
///    Maintain nightvision state for the delivery, outcome will be stored and restored after the script is terminated.
PROC DELIVERY_MAINTAIN_NIGHT_VISION()
	IF NOT GET_CORE_BITSET(DELIVERY_FLAG_CORE_NIGHT_VISION_WAS_TURNED_OFF)
	AND GET_REQUESTINGNIGHTVISION()
		DELIVERY_TOGGLE_NIGHT_VISION(FALSE)
		TOGGLE_CORE_BITSET(DELIVERY_FLAG_CORE_NIGHT_VISION_WAS_TURNED_OFF, TRUE)
		CDEBUG3LN(DEBUG_DELIVERY, "DELIVERY_MAINTAIN_NIGHT_VISION - turn off night vision.") 
	ENDIF
ENDPROC

//**************************************** OVERRIDES
//######################################## DELIVERY CHILD HANDLERS OVERRIDES ########################################
//Function pointers that can be overriden via child delivery scripts.
PROC PRIVATE_DELIVERY_LINK_DEFAULT_CORE_HANDLERS()
	scriptData.functionPointers.coreFunctions.handlerAssetsLoaded = &DELIVERY_DEFAULT_ASSTS_LOADED
	scriptData.functionPointers.coreFunctions.handlerCleanup = &DELIVERY_DEFAULT_CLEANUP
	scriptData.functionPointers.coreFunctions.handlerAssetsCreate = &DELIVERY_DEFAULT_ASSETS_CREATE
	scriptData.functionPointers.coreFunctions.handlerSceneSetup = &DELIVERY_DEFAULT_SCENE_SETUP
ENDPROC

FUNC BOOL SHOULD_BLOCK_HELI_DELIVERY()
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
	AND (IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
	OR GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(), TRUE)) = GET_ARMORY_AIRCRAFT_MODEL() 
	)
		VEHICLE_INDEX vehHeli = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(), TRUE)
		
		IF NOT IS_ENTITY_ALIVE(vehHeli)
			PRINTLN("[GUNRUNNING_CUTSCENE] SHOULD_BLOCK_HELI_DELIVERY heli is not alive!")
			RETURN TRUE
		ENDIF
		
		VECTOR vVelocity = GET_ENTITY_VELOCITY(PLAYER_PED_ID())
		
		IF DOES_ENTITY_EXIST(GET_ENTITY_ATTACHED_TO_CARGOBOB(vehHeli))
			PRINTLN("[GUNRUNNING_CUTSCENE] SHOULD_BLOCK_HELI_DELIVERY - VMAG(vVelocity): ", VMAG(vVelocity))
			IF VMAG(vVelocity) >= 8.0
				RETURN TRUE
			ENDIF
		ELSE
			IF (vVelocity.x > 3.0 OR vVelocity.x < -3.0)
			OR (vVelocity.y > 3.0 OR vVelocity.y < -3.0)
			OR (vVelocity.z > 0.5 OR vVelocity.z < -0.5)
				#IF IS_DEBUG_BUILD
					IF (GET_FRAME_COUNT() % 10) = 0
						PRINTLN("[GUNRUNNING_CUTSCENE] SHOULD_BLOCK_HELI_DELIVERY - velocity not acceptable for delivery via heli: ", vVelocity)
					ENDIF
				#ENDIF
				
				RETURN TRUE
			ELIF IS_ENTITY_IN_AIR(vehHeli)
				#IF IS_DEBUG_BUILD
					IF (GET_FRAME_COUNT() % 10) = 0
						PRINTLN("[GUNRUNNING_CUTSCENE] SHOULD_BLOCK_HELI_DELIVERY - heli is airborne")
					ENDIF
				#ENDIF
				
				
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

CONST_FLOAT MIN_CONTEXT_PRESS_DELIVERY_DISTANCE 625.0

FUNC BOOL DO_HELI_SPECIFIC_DELIVERY_CHECKS()
	
	IF FM_GANGOPS_DROPOFF_IS_PLAYER_PROPERTY(scriptData.deliveryData.eDropoffID)
	AND (GB_GET_GANGOPS_VARIATION_PLAYER_IS_ON(PLAYER_ID()) = GOV_UNDER_CONTROL
	OR g_FreemodeDeliveryData.sLocalDropoff.eDropoff = scriptData.deliveryData.eDropoffID)
		IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		AND IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
			VEHICLE_INDEX vehHeli 	= GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			VECTOR vPlayerPos 		= GET_ENTITY_COORDS(vehHeli)
			VECTOR vTarget			= FM_GANG_OPS_GET_HELI_DROP_LOICATION(scriptData.deliveryData.eDropoffID)
			FLOAT fDistToGround
			
			GET_GROUND_Z_FOR_3D_COORD(GET_ENTITY_COORDS(vehHeli), fDistToGround, TRUE)
			fDistToGround = (vPlayerPos.z - fDistToGround)
						
			IF IS_ENTITY_IN_AIR(vehHeli)
			AND IS_ENTITY_UPRIGHT(vehHeli)
			AND fDistToGround > 3.0
			AND VDIST2(vPlayerPos, vTarget) < MIN_CONTEXT_PRESS_DELIVERY_DISTANCE
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("GO_HELI_DELIV")
						CLEAR_BIT(g_FreemodeDeliveryData.iBS, BS_FREEMODE_DELIVERY_GLOBAL_DATA_BLOCK_BUTTON_CONFLICT)
						CLEAR_HELP()
					ENDIF
					
					RETURN TRUE
				ELSE
					IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("GO_HELI_DELIV")
					AND GET_PED_VEHICLE_SEAT(PLAYER_PED_ID(), vehHeli) = VS_DRIVER
						PRINT_HELP_FOREVER("GO_HELI_DELIV")
						SET_BIT(g_FreemodeDeliveryData.iBS, BS_FREEMODE_DELIVERY_GLOBAL_DATA_BLOCK_BUTTON_CONFLICT)
					ENDIF
				ENDIF
			ELSE
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("GO_HELI_DELIV")
					CLEAR_BIT(g_FreemodeDeliveryData.iBS, BS_FREEMODE_DELIVERY_GLOBAL_DATA_BLOCK_BUTTON_CONFLICT)
					CLEAR_HELP()
				ENDIF
			ENDIF
			
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_BLOCK_VEHICLE_FOR_DELIVERY(VEHICLE_INDEX veh, FREEMODE_DELIVERY_DROPOFFS eDropoffID, PLAYER_INDEX pDeliverableOwner)
	IF IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(veh))
		IF GET_ENTITY_MODEL(veh) = AVENGER
			IF GET_OWNER_OF_PERSONAL_ARMORY_AIRCRAFT(veh) != pDeliverableOwner
				RETURN TRUE
			ENDIF
			
			RETURN FALSE
		ENDIF
		
		IF FM_GANGOPS_DROPOFF_IS_VALID(eDropoffID)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_REMOTE_PLAYER_ON_SCRIPT_WANTED(PLAYER_INDEX player)
	IF player = INVALID_PLAYER_INDEX()
		RETURN FALSE
	ENDIF
	
	IF NOT NETWORK_IS_PLAYER_A_PARTICIPANT(player)
		RETURN FALSE
	ENDIF
	
	PARTICIPANT_INDEX participantID = NETWORK_GET_PARTICIPANT_INDEX(player)
	
	RETURN GET_PARTICIPANT_BROADCAST_BITSET(PBBS_I_AM_WANTED, NATIVE_TO_INT(participantID))
ENDFUNC

FUNC BOOL SHOULD_BLOCK_DELIVERY_DUE_TOANOTHER_PLAYER_IN_VEH(VEHICLE_INDEX vehicle, PLAYER_INDEX &PlayerReturn, BOOL &bReturnIsAnotherPlayerCritical, BOOL &bReturnIsAnotherPlayerWanted, BOOL bInSameGang = TRUE, BOOL bRivalBitSet = TRUE, BOOL bCheckDifferentGangs = FALSE)
	INT iSeat
	VEHICLE_SEAT eSeat
	PED_INDEX pedInSeat
	PLAYER_INDEX playerInSeat
	
	bReturnIsAnotherPlayerCritical 	= FALSE
	bReturnIsAnotherPlayerWanted	= FALSE
	
	FOR iSeat = VS_DRIVER TO VS_EXTRA_RIGHT_3
		eSeat = INT_TO_ENUM(VEHICLE_SEAT, iSeat)
		IF NOT IS_VEHICLE_SEAT_FREE(vehicle, eSeat)
			pedInSeat = GET_PED_IN_VEHICLE_SEAT(vehicle, eSeat)
			IF IS_ENTITY_ALIVE(pedInSeat)
				playerInSeat = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedInSeat)
				IF playerInSeat <> INVALID_PLAYER_INDEX()
				AND playerInSeat <> PLAYER_ID()
					
					IF NOT bReturnIsAnotherPlayerCritical
						IF GB_IS_PLAYER_CRITICAL_TO_JOB(playerInSeat)
							bReturnIsAnotherPlayerCritical = TRUE
						ENDIF
					ENDIF
					
					IF NOT bReturnIsAnotherPlayerWanted
						IF (GET_PLAYER_WANTED_LEVEL(playerInSeat) > 0)
						OR IS_REMOTE_PLAYER_ON_SCRIPT_WANTED(playerInSeat)
							bReturnIsAnotherPlayerWanted = TRUE
						ENDIF
					ENDIF
					
					IF IS_NET_PLAYER_OK(playerInSeat)
					AND FREEMODE_DELIVERY_IS_PLAYER_IN_POSSESSION_OF_ANY_DELIVERABLE(playerInSeat)
						IF bInSameGang 
							IF bRivalBitSet
								PlayerReturn = playerInSeat
								RETURN GB_IS_GLOBAL_CLIENT_BIT1_SET(playerInSeat, eGB_GLOBAL_CLIENT_BITSET_1_RIVAL_BUSINESS_PLAYER)
							ELSE
								PlayerReturn = playerInSeat
								RETURN GB_ARE_PLAYERS_IN_SAME_GANG(playerInSeat, PLAYER_ID())
							ENDIF
						ELIF bCheckDifferentGangs
							IF GB_IS_PLAYER_MEMBER_OF_A_GANG(playerInSeat)
							AND GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID())
							AND NOT GB_ARE_PLAYERS_IN_SAME_GANG(playerInSeat, PLAYER_ID())
							OR NOT GB_IS_PLAYER_MEMBER_OF_A_GANG(playerInSeat)
								PlayerReturn = playerInSeat
								RETURN TRUE
							ENDIF
						ELSE
							PlayerReturn = playerInSeat
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

//######################################## DELIVERY SCRIPT LOGIC OVERRIDES ########################################
//Check if the cutscene should start, by default is not based on anything, can enable locate check and set it up.
FUNC BOOL PRIVATE_DELIVERY_DEFAULT_SHOULD_CUTSCENE_START(DELIVERY_DATA &deliveryData)
	
	BOOL bBlockDeliveryForOtherPlayerInVeh 		= FALSE
	BOOL bAnotherPlayerInVehIsWanted 			= FALSE
	BOOL bCheckAnotherPlayerIsCriticalReturn 	= FALSE
	PLAYER_INDEX playerInSeatReturn				= INVALID_PLAYER_INDEX()
	STRING sUnsuitableDeliveryVehHelp			= _FREEMODE_DELIVERY_GET_UNSUITABLE_DELIVERY_HELP(deliveryData.eDropoffID, TRUE)
	
	IF IS_BIT_SET(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_AcceptedInvite)
		#IF IS_DEBUG_BUILD
			IF GET_FRAME_COUNT() % 60 = 0
				CDEBUG3LN(DEBUG_DELIVERY, "PRIVATE_DELIVERY_DEFAULT_SHOULD_CUTSCENE_START - accepted property invite.")
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF g_SimpleInteriorData.bAcceptedInviteIntoSimpleInterior
		#IF IS_DEBUG_BUILD
			IF GET_FRAME_COUNT() % 60 = 0
				CDEBUG3LN(DEBUG_DELIVERY, "PRIVATE_DELIVERY_DEFAULT_SHOULD_CUTSCENE_START - accepted simple interior invite.")
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_ENTITY_ALIVE(deliveryData.deliveryVehicle)
	AND IS_THIS_MODEL_ONLY_ALLOWED_IN_ARENA_GARAGE(GET_ENTITY_MODEL(deliveryData.deliveryVehicle))		
		#IF IS_DEBUG_BUILD
			IF GET_FRAME_COUNT() % 60 = 0
				CDEBUG3LN(DEBUG_DELIVERY, "PRIVATE_DELIVERY_DEFAULT_SHOULD_CUTSCENE_START - IS_THIS_MODEL_ONLY_ALLOWED_IN_ARENA_GARAGE = TRUE.")
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_INITIALISING_RCBANDITO(PLAYER_ID())
		CDEBUG3LN(DEBUG_DELIVERY, "PRIVATE_DELIVERY_DEFAULT_SHOULD_CUTSCENE_START - IS_PLAYER_INITIALISING_RCBANDITO = TRUE.")
		RETURN FALSE
	ENDIF
	
	#IF FEATURE_CASINO_HEIST
	IF IS_PLAYER_INITIALISING_RC_TANK(PLAYER_ID())
		CDEBUG3LN(DEBUG_DELIVERY, "PRIVATE_DELIVERY_DEFAULT_SHOULD_CUTSCENE_START - IS_PLAYER_INITIALISING_RC_TANK = TRUE.")
		RETURN FALSE
	ENDIF
	#ENDIF
	
	INT iBS = FREEMODE_DELIVERY_GET_DROPOFF_DETAILS_BS(deliveryData.eDropoffID)
	BOOL bVehicleExists = DOES_ENTITY_EXIST(deliveryData.deliveryVehicle) AND NOT IS_ENTITY_DEAD(deliveryData.deliveryVehicle)
	
	IF _FREEMODE_DELIVERY_IS_DELIVERY_WANTED_LEVEL_RESTRICTED(scriptData.deliveryData.eDropoffID)
		#IF IS_DEBUG_BUILD
			IF GET_FRAME_COUNT() % 150 = 0
				CDEBUG3LN(DEBUG_DELIVERY, "PRIVATE_DELIVERY_DEFAULT_SHOULD_CUTSCENE_START - wanted level.")
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_LOCAL_PLAYER_DELIVERING_BOUNTY()
		#IF IS_DEBUG_BUILD
			IF GET_FRAME_COUNT() % 150 = 0
				CDEBUG3LN(DEBUG_DELIVERY, "PRIVATE_DELIVERY_DEFAULT_SHOULD_CUTSCENE_START - IS_LOCAL_PLAYER_DELIVERING_BOUNTY.")
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF DO_HELI_SPECIFIC_DELIVERY_CHECKS()
		_FREEMODE_DELIVERY_START_BLOCK_CONFLICTING_INPUT_TIMER()
		RETURN TRUE
	ENDIF
	
	IF bVehicleExists
		IF SHOULD_BLOCK_VEHICLE_FOR_DELIVERY(deliveryData.deliveryVehicle, deliveryData.eDropoffID, deliveryData.deliveryOwner)
			RETURN FALSE
		ENDIF
		
		bBlockDeliveryForOtherPlayerInVeh = SHOULD_BLOCK_DELIVERY_DUE_TOANOTHER_PLAYER_IN_VEH(deliveryData.deliveryVehicle, playerInSeatReturn, bCheckAnotherPlayerIsCriticalReturn, bAnotherPlayerInVehIsWanted, FALSE, FALSE, TRUE)
		
		IF bAnotherPlayerInVehIsWanted
		AND g_FreemodeDeliveryData.bAllowDeliveryToBeBlockedByWantedPassenger
		AND _FREEMODE_DELIVERY_IS_DELIVERY_WANTED_LEVEL_RESTRICTED(scriptData.deliveryData.eDropoffID, TRUE)
			#IF IS_DEBUG_BUILD
				IF GET_FRAME_COUNT() % 30 = 0
					CDEBUG3LN(DEBUG_DELIVERY, "PRIVATE_DELIVERY_DEFAULT_SHOULD_CUTSCENE_START - Another player in the vehicle is wanted.")
				ENDIF
			#ENDIF
			
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iBS, FREEMODE_DELIVERY_DROPOFF_DETAILS_BS_PRIMARY_DROPOFF_USES_AA)
	AND bVehicleExists
	AND (NOT IS_VEHICLE_A_CARGOBOB(deliveryData.deliveryVehicle) OR GET_DELIVERY_FLAG_EXTRA(deliveryData, DELIVERY_FLAG_EXTRA_ALLOW_AIR_DELIVERY_VIA_CARGOBOB))
		BOOL bUseAlternateAA = ((GET_FRAME_COUNT() % 2) = 0)
		_FREEMODE_DELIVERY_DO_AREA_DELIVERY_CHECKS_FOR_DROPOFF(scriptData.deliveryData.eDropoffID, scriptData.deliveryData.eDeliverableID, TRUE, FALSE, bUseAlternateAA, FALSE)
		
		FLOAT fMaxVelocity = 30.0
		
		IF bUseAlternateAA
			fMaxVelocity = 7.5
		ENDIF
		
		IF GET_DELIVERY_FLAG_EXTRA(deliveryData, DELIVERY_FLAG_EXTRA_ALLOW_AIR_DELIVERY_VIA_CARGOBOB)
			fMaxVelocity = 25.0
		ENDIF
		
		IF FREEMODE_DELIVERY_IS_PLAYER_IN_POSITION_TO_DELIVER_DELIVERABLE()
			IF bVehicleExists
				IF VMAG(GET_ENTITY_VELOCITY(deliveryData.deliveryVehicle)) >= fMaxVelocity
				OR (SHOULD_BLOCK_HELI_DELIVERY() AND NOT IS_BIT_SET(iBS, FREEMODE_DELIVERY_DROPOFF_DETAILS_BS_LESS_RESTRICTIVE_AIRBORNE_HELI_DELIVERY))
					RETURN FALSE
				ENDIF
			ENDIF
			
			RETURN TRUE
		ENDIF
		
		RETURN FALSE
	ENDIF
	
	_FREEMODE_DELIVERY_DO_AREA_DELIVERY_CHECKS_FOR_DROPOFF(scriptData.deliveryData.eDropoffID, scriptData.deliveryData.eDeliverableID, FALSE, FALSE, FALSE, FALSE)
	
	IF NOT FREEMODE_DELIVERY_IS_PLAYER_IN_POSITION_TO_AIR_DROP_DELIVERABLE()
		IF bVehicleExists					
			IF VMAG(GET_ENTITY_VELOCITY(deliveryData.deliveryVehicle)) >= 30.0
			OR SHOULD_BLOCK_HELI_DELIVERY()
				#IF IS_DEBUG_BUILD
					IF GET_FRAME_COUNT() % 150 = 0
						CDEBUG3LN(DEBUG_DELIVERY, "PRIVATE_DELIVERY_DEFAULT_SHOULD_CUTSCENE_START - not in air position")
					ENDIF
				#ENDIF
				
				IF IS_CARGOBOB_DELIVERY_HELP_DISPLAYING()
					CDEBUG3LN(DEBUG_DELIVERY, "PRIVATE_DELIVERY_DEFAULT_SHOULD_CUTSCENE_START - clear_help out of locate")
					CLEAR_HELP()
				ENDIF
				
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF GET_DELIVERY_FLAG_DEFAULT(deliveryData, DELIVERY_FLAG_DEFAULT_PERFORM_LOCATE_CHECK)
		IF IS_ENTITY_IN_AREA(GET_DELIVERY_ENTITY(), deliveryData.locateCoord1, deliveryData.locateCoord2)
		AND NOT IS_PED_TRYING_TO_ENTER_OR_EXIT_VEHICLE(PLAYER_PED_ID())
			//Cargobob delivery must be without attachment.
			IF deliveryData.deliveryType = DT_VEHICLE
				IF DOES_ENTITY_EXIST(deliveryData.deliveryVehicle)
					IF IS_VEHICLE_A_CARGOBOB(deliveryData.deliveryVehicle)
						//cargobob with attachment?
						ENTITY_INDEX entityAttachedToCargobob
						entityAttachedToCargobob = GET_ENTITY_ATTACHED_TO_CARGOBOB(deliveryData.deliveryVehicle)
						
						IF DOES_ENTITY_EXIST(entityAttachedToCargobob)
							IF NOT IS_CARGOBOB_DELIVERY_DROP_HELP_DISPLAYING()
							AND GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
								CDEBUG3LN(DEBUG_DELIVERY, "PRIVATE_DELIVERY_DEFAULT_SHOULD_CUTSCENE_START - player in a cargobob with cargo attached, entity: ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(entityAttachedToCargobob)))
								PRINT_CARGOBOB_DROP_HELP(deliveryData.eCutscene)
							ENDIF
							RETURN FALSE
						ENDIF
					ELSE						
						//Attached?
						IF IS_VEHICLE_ATTACHED_TO_ANY_CARGOBOB(deliveryData.deliveryVehicle)
							IF NOT IS_CARGOBOB_DELIVERY_DETATCH_HELP_DISPLAYING()
							AND GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
								CDEBUG3LN(DEBUG_DELIVERY, "PRIVATE_DELIVERY_DEFAULT_SHOULD_CUTSCENE_START - player is in a vehicle that is attached to a cargobob")
								PRINT_CARGOBOB_DETATCH_HELP(deliveryData.eCutscene)
							ENDIF
							RETURN FALSE
						ELIF bBlockDeliveryForOtherPlayerInVeh
						AND playerInSeatReturn != INVALID_PLAYER_INDEX()
						AND GlobalPlayerBD_FM_3[NATIVE_TO_INT(playerInSeatReturn)].freemodeDelivery.eCurrentActiveDropoff != GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].freemodeDelivery.eCurrentActiveDropoff
							CDEBUG3LN(DEBUG_DELIVERY, "PRIVATE_DELIVERY_DEFAULT_SHOULD_CUTSCENE_START - can't deliver another player is in the vehicle with another package")
							IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sUnsuitableDeliveryVehHelp)
								PRINT_HELP_FOREVER(sUnsuitableDeliveryVehHelp)
							ENDIF
							RETURN FALSE
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_CARGOBOB_DELIVERY_HELP_DISPLAYING()
				OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sUnsuitableDeliveryVehHelp)
					CDEBUG3LN(DEBUG_DELIVERY, "PRIVATE_DELIVERY_DEFAULT_SHOULD_CUTSCENE_START - clear_help in locate")
					CLEAR_HELP()
				ENDIF
			ENDIF
			
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("GO_HELI_DELIV")
				CLEAR_HELP()
			ENDIF
			
			RETURN TRUE
		ELSE
			IF IS_CARGOBOB_DELIVERY_HELP_DISPLAYING()
			OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sUnsuitableDeliveryVehHelp)
				CDEBUG3LN(DEBUG_DELIVERY, "PRIVATE_DELIVERY_DEFAULT_SHOULD_CUTSCENE_START - clear_help out locate")
				CLEAR_HELP()
			ENDIF
				
			IF FREEMODE_DELIVERY_IS_PLAYER_IN_POSITION_TO_AIR_DROP_DELIVERABLE()
				//Sends event to start the delivery.
				_FREEMODE_DELIVERY_DO_AREA_DELIVERY_CHECKS_FOR_DROPOFF(scriptData.deliveryData.eDropoffID, scriptData.deliveryData.eDeliverableID, FALSE, FALSE, FALSE, FALSE)
				IF  GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].freemodeDelivery.eDropoffUsedForDelivery = scriptData.deliveryData.eDropoffID
					TOGGLE_CORE_BITSET(DELIVERY_FLAG_CORE_FORCE_TERMIANTE, TRUE)
				ENDIF
			ENDIF
		ENDIF
	ELSE
		
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("GO_HELI_DELIV")
			CLEAR_HELP()
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF GET_FRAME_COUNT() % 150 = 0
		CDEBUG3LN(DEBUG_DELIVERY, "PRIVATE_DELIVERY_DEFAULT_SHOULD_CUTSCENE_START - FALSE end.")
	ENDIF
	#ENDIF
	RETURN FALSE
ENDFUNC

//Perform action on vehicle before cutscene starts.
PROC PRIVATE_DELIVERY_DEFAULT_PERFORM_ACTION_ON_DELIVERY_VEHICLE(DELIVERY_DATA &deliveryData)
	//Moved to the function below
	UNUSED_PARAMETER(deliveryData)
ENDPROC

//Perform action on vehicle before cutscene starts.
PROC PRIVATE_DELIVERY_DEFAULT_MAINTAIN_VEHICLE_HALT_ON_DELIVERY_VEHICLE(DELIVERY_DATA &deliveryData, BOOL bRemoveControls = FALSE)
	//Should stop vehicle in place
	VECTOR vCoordA, vCoordB
	
	IF deliverydata.deliveryType = DT_FOOT
		EXIT
	ENDIF
		
	//Grab in vehicle locate from delivery data.
	FREEMODE_DELIVERY_GET_IN_CAR_ACTIVATION_LOCATE(scriptData.deliveryData.eDropoffID, vCoordA, vCoordB, <<1.3, 1.3, 2.0>>)
	
	IF GET_DELIVERY_FLAG_DEFAULT(deliveryData, DELIVERY_FLAG_DEFAULT_BRING_VEHICLE_TO_HALT)
		IF DOES_ENTITY_EXIST(deliveryData.deliveryVehicle)
		AND NOT IS_ENTITY_DEAD(deliveryData.deliveryVehicle)
			IF NETWORK_HAS_CONTROL_OF_ENTITY(deliveryData.deliveryVehicle)
				IF IS_ENTITY_IN_AREA(GET_DELIVERY_ENTITY(), vCoordA, vCoordB)
				OR NOT GET_DELIVERY_FLAG_DEFAULT(deliveryData, DELIVERY_FLAG_DEFAULT_DO_VEH_HALT_LOCATE_CHECK)
					
					IF NOT IS_VEHICLE_BEING_BROUGHT_TO_HALT(deliveryData.deliveryVehicle)
						IF GET_DELIVERY_FLAG_EXTRA(deliveryData, DELIVERY_FLAG_EXTRA_ENABLE_AGGRESSIVE_BRING_VEH_TO_HALT)
							BRING_VEHICLE_TO_HALT(deliveryData.deliveryVehicle, 7.5, 2)
						ELSE
							BRING_VEHICLE_TO_HALT(deliveryData.deliveryVehicle, 4.0, 2)
						ENDIF
					ENDIF
					
					IF bRemoveControls
					AND IS_PLAYER_CONTROL_ON(PLAYER_ID())
						NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_LEAVE_CAMERA_CONTROL_ON)
						TOGGLE_CORE_BITSET(DELIVERY_FLAG_CORE_REMOVED_PLAYER_CONTROL, TRUE)
					ENDIF
					
					CDEBUG3LN(DEBUG_DELIVERY, "PRIVATE_MAINTAIN_VEHICLE_HALT_ON_DELIVERY_VEHICLE - BRING_VEHICLE_TO_HALT.")	
				ENDIF
			ELSE
				CDEBUG3LN(DEBUG_DELIVERY, "PRIVATE_MAINTAIN_VEHICLE_HALT_ON_DELIVERY_VEHICLE - NOT DRIVER. DO NOT BRING_VEHICLE_TO_HALT.")	
				TOGGLE_DELIVERY_FLAG_DEFAULT(deliveryData, DELIVERY_FLAG_DEFAULT_BRING_VEHICLE_TO_HALT, FALSE)
			ENDIF
		ELSE
			CDEBUG3LN(DEBUG_DELIVERY, "PRIVATE_MAINTAIN_VEHICLE_HALT_ON_DELIVERY_VEHICLE - BRING_VEHICLE_TO_HALT - unable, no vehicle exists.")	
		ENDIF
	ELSE
		CDEBUG3LN(DEBUG_DELIVERY, "PRIVATE_MAINTAIN_VEHICLE_HALT_ON_DELIVERY_VEHICLE - BRING_VEHICLE_TO_HALT - disabled.")	
	ENDIF
ENDPROC

/// PURPOSE:
///    Return true to start disaplying delviery shard message.
FUNC BOOL PRIVATE_DELIVERY_DEFAULT_SHOULD_DELIVERY_MESSAGE_DISPLAY(DELIVERY_DATA &deliveryData)
	IF deliveryData.cutsceneData.cutscene.bPlaying
		IF deliveryData.cutsceneData.cutscene.iScenesCount > 0
			IF deliveryData.cutsceneData.cutscene.iCurrentScene = deliveryData.cutsceneData.cutscene.iScenesCount - deliveryData.iSceneToDisplayShard
				IF (deliveryData.cutsceneData.cutscene.sScenes[deliveryData.cutsceneData.cutscene.iCurrentScene].iDuration - deliveryData.cutsceneData.cutscene.iCurrentSceneElapsedTime) <= 1700
				OR GET_DELIVERY_FLAG_EXTRA(deliveryData, DELIVERY_FLAG_EXTRA_SHOW_SHARD)
					
					IF GET_DELIVERY_FLAG_DEFAULT(deliveryData, DELIVERY_FLAG_DEFAULT_SHOW_SHARD_ON_FADDED_OUT_SCREEN)
					OR NOT IS_SCREEN_FADED_OUT()
						CDEBUG3LN(DEBUG_DELIVERY, "PRIVATE_SHOULD_DELIVERY_MESSAGE_DISPLAY - default true, 1700ms to finish the cutscene, SceneID: ", deliveryData.cutsceneData.cutscene.iCurrentScene, 
						" Duration: ", deliveryData.cutsceneData.cutscene.sScenes[deliveryData.cutsceneData.cutscene.iCurrentScene].iDuration,
						" Elapsed: ", deliveryData.cutsceneData.cutscene.iCurrentSceneElapsedTime,
						" DELIVERY_FLAG_EXTRA_SHOW_SHARD? ", GET_DELIVERY_FLAG_EXTRA(deliveryData, DELIVERY_FLAG_EXTRA_SHOW_SHARD))
						RETURN TRUE
					ELSE
						CDEBUG3LN(DEBUG_DELIVERY, "PRIVATE_SHOULD_DELIVERY_MESSAGE_DISPLAY - default false due to DELIVERY_FLAG_DEFAULT_SHOW_SHARD_ON_FADDED_OUT_SCREEN = FALSE")
					ENDIF
					
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Check if cutscene is about to finish, start fading out.
FUNC BOOL PRIVATE_DELIVERY_DEFAULT_CHECK_CUTSCENE_FADE_OUT_START(DELIVERY_DATA &deliveryData)
	IF deliveryData.cutsceneData.cutscene.bPlaying
		IF deliveryData.cutsceneData.cutscene.iScenesCount > 0
			IF deliveryData.cutsceneData.cutscene.iCurrentScene = scriptData.deliveryData.cutsceneData.cutscene.iScenesCount - 1
				//Before cutscene finishes start fade out.
				IF deliveryData.cutsceneData.cutscene.sScenes[deliveryData.cutsceneData.cutscene.iCurrentScene].iDuration - deliveryData.cutsceneData.cutscene.iCurrentSceneElapsedTime <= (CONST_DELIVERY_END_SCREN_FADE_DURATION + 10)
					IF NOT IS_SCREEN_FADING_OUT()
					AND NOT IS_SCREEN_FADED_OUT()
						CDEBUG3LN(DEBUG_DELIVERY, "DELIVERY_CHECK_CUTSCENE_FADE_OUT_START - fade out screen, cutscene near end") 
						DO_SCREEN_FADE_OUT(CONST_DELIVERY_END_SCREN_FADE_DURATION)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Check if mission vehicle should be cleanedup/destroyed.  
FUNC BOOL PRIVATE_DELIVERY_DEFAULT_SHOULD_CLEANUP_DELIVERABLE_ENTITY(DELIVERY_DATA &deliveryData)
	UNUSED_PARAMETER(deliveryData)
	
	IF GET_CORE_BITSET(DELIVERY_FLAG_CORE_WILL_PERFORM_INTERIOR_WARP)
		//CDEBUG3LN(DEBUG_DELIVERY, "PRIVATE_DELIVERY_SHOULD_CLEANUP_DELIVERABLE_ENTITY - warping into an interior, deliverable will cleanup.") 
		RETURN TRUE
	ENDIF
	
	//CDEBUG3LN(DEBUG_DELIVERY, "PRIVATE_DELIVERY_SHOULD_CLEANUP_DELIVERABLE_ENTITY - FALSE.") 
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Check if the scritp should termiante, can be overriden by specific deliveries.
/// RETURNS: enum value to print the reason in leanup for easy debugging..
///    
FUNC STRING PRIVATE_DELIVERY_DEFAULT_MAINTAIN_TERMIANTE_CHECK(DELIVERY_DATA &deliveryData)
	IF IS_ENTITY_DEAD(PLAYER_PED_ID())
		CDEBUG3LN(DEBUG_DELIVERY, "TTTTT - PRIVATE_MAINTAIN_TERMIANTE_CHECK - Player is dead.")
		RETURN "Player is dead"
	ENDIF
	
	IF GET_CORE_BITSET(DELIVERY_FLAG_CORE_FORCE_TERMIANTE)
		RETURN "DELIVERY_FLAG_CORE_FORCE_TERMIANTE"
	ENDIF
	
	IF (PRIVATE_GET_DELIVERY_STATE() < DS_WAIT_FOR_PLAYERS)
		IF GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].freemodeDelivery.eCurrentActiveDropoff != g_FreemodeDeliveryData.eDropoffForDeliveryScript
			CDEBUG3LN(DEBUG_DELIVERY, "TTTTT - PRIVATE_MAINTAIN_TERMIANTE_CHECK - Current active dropoff has changed.")
			RETURN "activeDropOff is not the same as sctiptDropOff"
		ENDIF
	ENDIF
	
	IF FREEMODE_DELIVERY_IS_DROPOFF_COORD_OVERRIDDEN(deliveryData.eDropoffID)
		RETURN "Dropoff is overridden"
	ENDIF
	
	//Checks before cutscene.
	IF (PRIVATE_GET_DELIVERY_STATE() >= DS_WAIT_FOR_START) AND (PRIVATE_GET_DELIVERY_STATE() < DS_MAINTAIN_DELIVERY)
		IF IS_PED_TRYING_TO_ENTER_OR_EXIT_VEHICLE(PLAYER_PED_ID(), FALSE)
			CDEBUG3LN(DEBUG_DELIVERY, "TTTTT - PRIVATE_MAINTAIN_TERMIANTE_CHECK - Entering or exiting a vehicle.")
			RETURN "player enter/exit vehicle"
		ENDIF

		IF VDIST(GET_PLAYER_COORDS(PLAYER_ID()), deliveryData.locateCoord1) > 1000.0
			#IF IS_DEBUG_BUILD
				VECTOR vPlayerPos = GET_PLAYER_COORDS(PLAYER_ID())
				VECTOR vbasePos = deliveryData.locateCoord1
				CDEBUG3LN(DEBUG_DELIVERY, "TTTTT - PRIVATE_MAINTAIN_TERMIANTE_CHECK - Local player moved too far. vPlayerPos: ", vPlayerPos, " vbasePos: ", vbasePos)
			#ENDIF
			
			RETURN "palyer is out of delivery distance >1000"
		ENDIF
		
		IF scriptData.deliveryData.deliveryType = DT_VEHICLE
			IF DOES_ENTITY_EXIST(deliveryData.deliveryVehicle)
				IF IS_VEHICLE_SEAT_FREE(deliveryData.deliveryVehicle, DEFAULT)
					RETURN " no driver in the delivery vehicle"
				ENDIF
				
				IF NOT IS_VEHICLE_DRIVEABLE(deliveryData.deliveryVehicle)
					RETURN "delivery vehicle is not driveable"
				ENDIF
				
				IF GET_ENTITY_MODEL(deliveryData.deliveryVehicle) = STROMBERG
				AND IS_VEHICLE_IN_SUBMARINE_MODE(deliveryData.deliveryVehicle)
					RETURN "delivery vehicle is in submarine mode"
				ENDIF
				
				IF FMBB_DROPOFF_IS_VALID(deliveryData.eDropoffID)
				AND IS_ENTITY_A_MISSION_ENTITY(deliveryData.deliveryVehicle)
					INT iInstance
					INT iScriptHash = GET_HASH_KEY(GET_ENTITY_SCRIPT(deliveryData.deliveryVehicle, iInstance))
					
					//Was this vehicle created by the business battles script
					IF iScriptHash = HASH("GB_DATA_HACK")
						RETURN "Data hack delivery vehicle is not valid"
					ENDIF
				ENDIF
			ENDIF
			
			PED_INDEX pedDriver = GET_PED_IN_VEHICLE_SEAT(deliveryData.deliveryVehicle)
			IF IS_PED_TRYING_TO_ENTER_OR_EXIT_VEHICLE(pedDriver, TRUE)
				CDEBUG3LN(DEBUG_DELIVERY, "TTTTT - PRIVATE_MAINTAIN_TERMIANTE_CHECK - Driver is trying to enter or exit the vehicle.")
				RETURN "Driver is trying to enter or exit the vehicle"
			ENDIF
			
			IF deliveryData.iPedsInVehBS = 0
				SCRIPT_ASSERT("PRIVATE_MAINTAIN_TERMIANTE_CHECK - iPedsInVehBS = 0")
				deliveryData.iPedsInVehBS = ALL_PLAYERS_IN_VEHICLE(deliveryData.deliveryVehicle, TRUE, FALSE, TRUE)
			ELSE
				IF deliveryData.iPedsInVehBS != ALL_PLAYERS_IN_VEHICLE(deliveryData.deliveryVehicle, TRUE, FALSE, TRUE)
					CDEBUG3LN(DEBUG_DELIVERY, "TTTTT - PRIVATE_MAINTAIN_TERMIANTE_CHECK - Players in vehicle have changed!")
					RETURN "Players in vehicle have changed"
				ENDIF
			ENDIF
			
			IF g_FreemodeDeliveryData.bDeliveringViaProxy != deliveryData.bDeliveringViaProxy
				CDEBUG3LN(DEBUG_DELIVERY, "TTTTT - PRIVATE_MAINTAIN_TERMIANTE_CHECK - bDeliveringViaProxy has changed!")
				RETURN "bDeliveringViaProxy has changed"
			ENDIF
			
			IF GET_DELIVERY_FLAG_DEFAULT(deliveryData, DELIVERY_FLAG_DEFAULT_DO_GANG_VALIDITY_CHECKS)
				IF _FREEMODE_DELIVERY_ARE_WE_DELIVERING_WITH_A_PLAYER_FROM_ANOTHER_GANG(scriptData.deliveryData.deliveryVehicle, scriptData.deliveryData.deliveryOwner)
					RETURN "other gang member in the vehicle"
				ENDIF

				IF GB_GET_THIS_PLAYER_GANG_BOSS(PLAYER_ID()) <> GB_GET_THIS_PLAYER_GANG_BOSS(scriptData.deliveryData.deliveryOwner)
				AND (GB_GET_THIS_PLAYER_GANG_BOSS(PLAYER_ID()) <> INVALID_PLAYER_INDEX() OR GB_GET_THIS_PLAYER_GANG_BOSS(scriptData.deliveryData.deliveryOwner) <> INVALID_PLAYER_INDEX())
				AND scriptData.deliveryData.deliveryOwner != scriptdata.deliveryData.eDeliverableID.pOwner
				AND (NOT FMBB_DROPOFF_IS_PLAYER_PROPERTY(scriptdata.deliveryData.eDropoffID) OR (GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG() AND GB_IS_PLAYER_MEMBER_OF_A_GANG(scriptData.deliveryData.deliveryOwner)))
				AND FREEMODE_DELIVERY_GET_DELIVERABLE_TYPE(scriptData.deliveryData.eDeliverableID) != FMC_FFA_DELIVERABLE
					RETURN "deliveryOwner is not in a gang with the localPlayer."
				ENDIF
				
				//If any player in the vehicle is not in a gang, and holds a delivery package.
				PLAYER_INDEX playerInSeat
				BOOL bCheckAnotherPlayerIsCriticalReturn = FALSE
				BOOL bAnotherPlayerInVehIsWanted = FALSE
				IF SHOULD_BLOCK_DELIVERY_DUE_TOANOTHER_PLAYER_IN_VEH(deliveryData.deliveryVehicle, playerInSeat, bCheckAnotherPlayerIsCriticalReturn, bAnotherPlayerInVehIsWanted, FALSE)
					IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
					AND NOT GB_ARE_PLAYERS_IN_SAME_GANG(PLAYER_ID(), playerInSeat)
						RETURN "Player is in a vehicle with a player who is not in the same gang/not in a gang, and has a package"
					ENDIF
				ENDIF
			
				IF bCheckAnotherPlayerIsCriticalReturn
				OR GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID())
					IF FMBB_DROPOFF_IS_VALID(deliveryData.eDropoffID)
					AND g_FreemodeDeliveryData.bAllowDeliveryToBeBlockedByCriticalPassenger
						RETURN "Another player in the vehicle is critical to a job"
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT FREEMODE_DELIVERY_IS_DELIVERABLE_VALID(deliveryData.eDeliverableID)
			#IF IS_DEBUG_BUILD
			IF NOT g_FreemodeDeliveryData.d_bAllDropoffsAcceptEverything
			#ENDIF
				CDEBUG3LN(DEBUG_DELIVERY, "TTTTT - PRIVATE_MAINTAIN_TERMIANTE_CHECK -  Deliverable is invalid deliverableID: ", scriptData.deliveryData.eDeliverableID.iIndex)
				RETURN "invalid deliverable scriptData.deliveryData.eDeliverableID: "
			#IF IS_DEBUG_BUILD
			ENDIF
			#ENDIF
		ELIF NOT FREEMODE_DELIVERY_DOES_DELIVERABLE_ID_EXIST_ON_SERVER(deliveryData.eDeliverableID)
			#IF IS_DEBUG_BUILD
			IF NOT g_FreemodeDeliveryData.d_bAllDropoffsAcceptEverything
			#ENDIF
				CDEBUG3LN(DEBUG_DELIVERY, "TTTTT - PRIVATE_MAINTAIN_TERMIANTE_CHECK -  Deliverable does not exist on the server deliverableID: ", scriptData.deliveryData.eDeliverableID.iIndex)
				RETURN "non existant deliverable"
			#IF IS_DEBUG_BUILD
			ENDIF
			#ENDIF
		ENDIF
	ENDIF

	RETURN ""
ENDFUNC

PROC PERFORM_DELVIERY_DEFAULT_LOCATE_CHECK(DELIVERY_DATA &deliveryData)
	VECTOR vCoordA, vCoordB
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		//default size
		DELIVERY_CHILD_SET_LOCATE_MULTIPLIER(deliveryData, <<2.0, 2.0, 2.0>>)

		//override size
		IF DOES_ENTITY_EXIST(deliveryData.deliveryVehicle)
		AND NOT IS_ENTITY_DEAD(deliveryData.deliveryVehicle)
			IF IS_VEHICLE_A_CARGOBOB(deliveryData.deliveryVehicle)
				PRINTLN("PRIVATE_DELIVERY_INITIALIZE - Setting a locate size multiplier - cargobob")
				ENTITY_INDEX EntityAttachedToCargobob
				EntityAttachedToCargobob = GET_ENTITY_ATTACHED_TO_CARGOBOB(deliveryData.deliveryVehicle)
				IF DOES_ENTITY_EXIST(EntityAttachedToCargobob)
					DELIVERY_CHILD_SET_LOCATE_MULTIPLIER(deliveryData, <<2.0, 2.0, 9.0>>)
				ELIF FM_GANGOPS_DROPOFF_IS_VALID(deliveryData.eDropoffID)
					DELIVERY_CHILD_SET_LOCATE_MULTIPLIER(deliveryData, <<2.0, 2.0, 5.0>>)
				ELSE
					PRINTLN("PRIVATE_DELIVERY_INITIALIZE - Setting a locate size multiplier - cargobob, nothing attached")
				ENDIF			
			ELIF GET_ENTITY_MODEL(deliveryData.deliveryVehicle) = GET_ARMORY_AIRCRAFT_MODEL()
				DELIVERY_CHILD_SET_LOCATE_MULTIPLIER(deliveryData, <<3.0, 3.0, 9.0>>)
			ELIF GET_ENTITY_MODEL(deliveryData.deliveryVehicle) = ALKONOST
				DELIVERY_CHILD_SET_LOCATE_MULTIPLIER(deliveryData, <<10.0, 10.0, 10.0>>)
			ELIF FM_GANGOPS_DROPOFF_IS_VALID(deliveryData.eDropoffID)
				DELIVERY_CHILD_SET_LOCATE_MULTIPLIER(deliveryData, <<2.0, 2.0, 5.0>>)
			ELIF FMBB_DROPOFF_IS_VALID(deliveryData.eDropoffID)
				IF GET_ENTITY_MODEL(deliveryData.deliveryVehicle) = PHANTOM2
				OR GET_ENTITY_MODEL(deliveryData.deliveryVehicle) = TERBYTE
					DELIVERY_CHILD_SET_LOCATE_MULTIPLIER(deliveryData, <<1.2, 1.2, 3.0>>)
				ELSE
					DELIVERY_CHILD_SET_LOCATE_MULTIPLIER(deliveryData, <<1.0, 1.0, 3.0>>)
				ENDIF
			ELIF CSH_DROPOFF_IS_VALID(deliveryData.eDropoffID)
				DELIVERY_CHILD_SET_LOCATE_MULTIPLIER(deliveryData, <<1.0, 1.0, 2.0>>)
			#IF FEATURE_TUNER
			ELIF deliveryData.eDropoffID = FMC_DROPOFF_INSIDE_MAN_MISS_ROW
				DELIVERY_CHILD_SET_LOCATE_AAB(deliveryData, <<410.7167, -986.8162, 28.4711>>, <<404.9944, -981.6803, 30.6828>>)
				EXIT
			#ENDIF
			ELIF deliveryData.eDropoffID != FMC_DROPOFF_LSIA_HANGAR_1
			OR deliveryData.eDropoffID != FMC_DROPOFF_LSIA_HANGAR_2
				VECTOR vMultiplier = FMC_DROPOFF_GET_LOCATE_SIZE_MULTIPLIER(deliveryData.eDropoffID)
				DELIVERY_CHILD_SET_LOCATE_MULTIPLIER(deliveryData, vMultiplier)
			ELIF FMC_DROPOFF_IS_VALID(deliveryData.eDropoffID)			
				DELIVERY_CHILD_SET_LOCATE_MULTIPLIER(deliveryData, <<1.0, 1.0, 2.0>>)
			ENDIF
		ENDIF
		
		//apply size
		FREEMODE_DELIVERY_GET_IN_CAR_ACTIVATION_LOCATE(deliveryData.eDropoffID, vCoordA, vCoordB, deliveryData.locateSizeMultiplier)
	ELSE
		DELIVERY_CHILD_SET_LOCATE_MULTIPLIER(deliveryData, <<1.35, 1.35, 1.35>>)
		
		FREEMODE_DELIVERY_GET_ON_FOOT_ACTIVATION_LOCATE(deliveryData.eDropoffID, vCoordA, vCoordB, deliveryData.locateSizeMultiplier)
	ENDIF
	
	DELIVERY_CHILD_SET_LOCATE_AAB(deliveryData, vCoordA, vCoordB)	
ENDPROC

#IF FEATURE_TUNER
PROC CINEMATIC_SCENE_REGISTER_SCENE_ENTITIES(DELIVERY_DATA &deliveryData)
	UNUSED_PARAMETER(deliveryData)
ENDPROC
#ENDIF

//Set default logic fp, can be overriden in child scripts.
PROC PRIVATE_DELIVERY_LINK_DEFAULT_SPECIFIC_HANDLERS()
	scriptData.functionPointers.specificFunctions.shouldDeliveryCutsceneStart =&PRIVATE_DELIVERY_DEFAULT_SHOULD_CUTSCENE_START
	scriptData.functionPointers.specificFunctions.performActionOnDeliveryVehicle =&PRIVATE_DELIVERY_DEFAULT_PERFORM_ACTION_ON_DELIVERY_VEHICLE
	scriptData.functionPointers.specificFunctions.shouldStartDeliveryMessageDisplay =&PRIVATE_DELIVERY_DEFAULT_SHOULD_DELIVERY_MESSAGE_DISPLAY
	scriptData.functionPointers.specificFunctions.shouldCutsceneStartFadeOut = &PRIVATE_DELIVERY_DEFAULT_CHECK_CUTSCENE_FADE_OUT_START
	scriptData.functionPointers.specificFunctions.shouldCleanupDeliverable = &PRIVATE_DELIVERY_DEFAULT_SHOULD_CLEANUP_DELIVERABLE_ENTITY
	scriptData.functionPointers.specificFunctions.shouldTerminateScript = &PRIVATE_DELIVERY_DEFAULT_MAINTAIN_TERMIANTE_CHECK
	scriptData.functionPointers.specificFunctions.performLocateCheck = &PERFORM_DELVIERY_DEFAULT_LOCATE_CHECK
	#IF FEATURE_TUNER
	scriptData.functionPointers.specificFunctions.registerSceneEntities = &CINEMATIC_SCENE_REGISTER_SCENE_ENTITIES
	#ENDIF
ENDPROC

/// PURPOSE:
///    By default we want all the functionality enabled, disable via child if required.
PROC PRIVATE_FLIP_ALL_BITS()
	INT bit
	REPEAT 32 bit
		SET_BIT(scriptData.deliveryData.iDefaultBS, bit)
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Check if player should be warped into an interior after cutscene ends.
/// RETURNS:
///    
FUNC BOOL PRIVATE_DELIVERY_SHOULD_INTERIOR_WARP()
	IF GUNNRUNNING_DROPOFF_IS_PLAYER_BUNKER(scriptData.deliveryData.eDropoffID)
	OR SMUGGLER_DROPOFF_IS_PLAYER_HANGAR(scriptData.deliveryData.eDropoffID)
	OR FM_GANGOPS_DROPOFF_IS_PLAYER_PROPERTY(scriptData.deliveryData.eDropoffID)
	OR FMBB_DROPOFF_IS_PLAYER_PROPERTY(scriptData.deliveryData.eDropoffID)
	OR GBC_DROPOFF_IS_CASINO(scriptData.deliveryData.eDropoffID)	
	OR CSH_DROPOFF_IS_ARCADE(scriptData.deliveryData.eDropoffID, FALSE)
	OR FMC_SHOULD_WARP_PLAYER_INTO_PROPERTY(scriptData.deliveryData)
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

PROC PLAYER_TRANSITION_FINISHED()
	IF NOT GET_CORE_BITSET(DELIVERY_FLAG_CORE_DELIVERY_VEHICLE_TRANSITION_FINISHED)
		CDEBUG3LN(DEBUG_DELIVERY, "[TFADE]SCRIPT - DELIVERY_FLAG_CORE_DELIVERY_VEHICLE_TRANSITION_FINISHED")
		TOGGLE_CORE_BITSET(DELIVERY_FLAG_CORE_DELIVERY_VEHICLE_TRANSITION_FINISHED, TRUE)
	ENDIF
ENDPROC

/// PURPOSE:
///    Initialize default functionality
PROC PRIVATE_DELIVERY_INITIALIZE()
	CDEBUG3LN(DEBUG_DELIVERY, "~~~~~~~~~~~~~~~~~~~~~")
	CDEBUG3LN(DEBUG_DELIVERY, "PRIVATE_DELIVERY_INITIALIZE - PRIVATE_DELIVERY_INITIALIZE")
	
	g_FreemodeDeliveryData.bDeliveryScriptAllowDeliveryShard = FALSE
	//scriptData.coreData.sTransition.transitionFinished = &PLAYER_TRANSITION_FINISHED
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		VEHICLE_INDEX vehiclePedIsIn
		vehiclePedIsIn = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
		CDEBUG3LN(DEBUG_DELIVERY, "PRIVATE_DELIVERY_INITIALIZE - ped is in a vehicle Model: ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(vehiclePedIsIn)), " MissionEntity: ", IS_ENTITY_A_MISSION_ENTITY(vehiclePedIsIn))
		IF IS_ENTITY_A_MISSION_ENTITY(vehiclePedIsIn)	
			TOGGLE_CORE_BITSET(DELIVERY_FLAG_CORE_DELIVERY_VEHICLE_IS_MISSION, TRUE)
		ENDIF
	ELSE
		CDEBUG3LN(DEBUG_DELIVERY, "PRIVATE_DELIVERY_INITIALIZE - ped is not in a avehicle")
	ENDIF
		
	//Set the locate data for use later
	PERFORM_DELVIERY_DEFAULT_LOCATE_CHECK(scriptData.deliveryData)
	DELIVERY_CHILD_SET_VFX_REMOVE(scriptData.deliveryData, scriptData.deliveryData.locateCoord1, 5000.0)
	DELIVERY_CHILD_SET_FOCUS(scriptData.deliveryData, scriptData.deliveryData.locateCoord1)
	
	CDEBUG3LN(DEBUG_DELIVERY, "PRIVATE_DELIVERY_INITIALIZE - This delivery should perform an interior warp? ", PRIVATE_DELIVERY_SHOULD_INTERIOR_WARP()) 
	IF PRIVATE_DELIVERY_SHOULD_INTERIOR_WARP()
		TOGGLE_CORE_BITSET(DELIVERY_FLAG_CORE_WILL_PERFORM_INTERIOR_WARP, TRUE)
	ENDIF
	
	CDEBUG3LN(DEBUG_DELIVERY, "PRIVATE_DELIVERY_INITIALIZE - This player holds a delivry package? ", _FREEMODE_DELIVERY_DOES_PLAYER_POSSESS_VALID_DELIVERABLE(GET_PLAYER_INDEX())) 
	IF _FREEMODE_DELIVERY_DOES_PLAYER_POSSESS_VALID_DELIVERABLE(GET_PLAYER_INDEX())
		TOGGLE_PARTICIPANT_BROADCAST_BITSET(PBBS_HOLDING_DELIVERY_PACKAGE, TRUE)
	ENDIF
	CDEBUG3LN(DEBUG_DELIVERY, "~~~~~~~~~~~~~~~~~~~~~")
ENDPROC

FUNC BOOL PRIVATE_DELIVERY_EVERYONE_READY_FOR_CUTSCENE(DELIVERY_DATA &deliveryData)
	PARTICIPANT_INDEX participantIndex
	INT iParticipantsActive, iParticipantsReady
	INT iParticipantInt, iNumPlayersInvehicle
	VEHICLE_INDEX playerVeh
	
	REPEAT NUM_NETWORK_PLAYERS iParticipantInt
		participantIndex = INT_TO_PARTICIPANTINDEX(iParticipantInt)
		PLAYER_INDEX playerId = INT_TO_PLAYERINDEX(iParticipantInt)
		
		IF DOES_ENTITY_EXIST(deliveryData.deliveryVehicle)
		AND IS_NET_PLAYER_OK(playerId)
			IF IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(playerID), TRUE)
				IF NOT IS_REMOTE_PLAYER_IN_NON_CLONED_VEHICLE(playerID)
					playerVeh = GET_VEHICLE_PED_IS_IN(GET_PLAYER_PED(playerID), TRUE)
					IF playerVeh = deliveryData.deliveryVehicle
						iNumPlayersInvehicle ++
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NETWORK_IS_PARTICIPANT_ACTIVE(participantIndex)
			iParticipantsActive++
			
			#IF IS_DEBUG_BUILD
			PLAYER_INDEX participantPlayer
			participantPlayer = NETWORK_GET_PLAYER_INDEX(participantIndex)
			#ENDIF
			
			IF GET_PARTICIPANT_BROADCAST_BITSET(PBBS_READY_TO_START_CUTSCENE, iParticipantInt)
				iParticipantsReady++
			#IF IS_DEBUG_BUILD
				CDEBUG3LN(DEBUG_DELIVERY, "PRIVATE_DELIVERY_EVERYONE_READY_FOR_CUTSCENE - Player: ", GET_PLAYER_NAME(participantPlayer), " iParticipantInt: ", iParticipantInt, " - READY")  
			ELSE
				CDEBUG3LN(DEBUG_DELIVERY, "PRIVATE_DELIVERY_EVERYONE_READY_FOR_CUTSCENE - Player: ", GET_PLAYER_NAME(participantPlayer), " iParticipantInt: ", iParticipantInt, " - NOT READY")  
			#ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	BOOL bShouldCheckVehParticipants = GET_DELIVERY_FLAG_EXTRA(deliveryData, DELIVERY_FLAG_EXTRA_CHECK_VEH_INSTANCE)
	
	IF iParticipantsActive = iParticipantsReady
	AND (iParticipantsActive = iNumPlayersInvehicle OR deliveryData.deliveryType != DT_VEHICLE OR NOT bShouldCheckVehParticipants)
		CDEBUG3LN(DEBUG_DELIVERY, "PRIVATE_DELIVERY_EVERYONE_READY_FOR_CUTSCENE - all passengers area ready.") 	
		RETURN TRUE
	ELIF iNumPlayersInvehicle != iParticipantsActive
	AND deliveryData.deliveryType = DT_VEHICLE
		CDEBUG3LN(DEBUG_DELIVERY, "PRIVATE_DELIVERY_EVERYONE_READY_FOR_CUTSCENE - not everyone in the vehicle is a participant. iNumPlayersInvehicle: ", iNumPlayersInvehicle, " iParticipantsActive: ", iParticipantsActive)
	ELSE
		CDEBUG3LN(DEBUG_DELIVERY, "PRIVATE_DELIVERY_EVERYONE_READY_FOR_CUTSCENE - not everyone ready to start the cutscene, iParticipantsActive: ", iParticipantsActive, " iParticipantsReady: ", iParticipantsReady) 
	ENDIF
	
	RETURN FALSE
ENDFUNC

//BROADCAST CHECKS
FUNC BOOL HAS_EVERYONE_STARTED_DELIVERY_FADE_SUPPRESS()
	INT i
	PARTICIPANT_INDEX participant
	
	INT iParticipantCount, iParticipantsReady
	REPEAT NUM_NETWORK_PLAYERS i
		participant = INT_TO_PARTICIPANTINDEX(i)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(participant)
			iParticipantCount++
			IF GET_PARTICIPANT_BROADCAST_BITSET(PBBS_SUPRESSING_PLAYER_FADE_OUT, i)
				iParticipantsReady++
			ENDIF
		ENDIF
	ENDREPEAT
	
	//Fix to cehck if in vehicle
	IF iParticipantCount = iParticipantsReady
		CDEBUG3LN(DEBUG_DELIVERY, "HAS_EVERYONE_STARTED_DELIVERY_FADE_SUPPRESS, all players are supressing fade out.")
		RETURN TRUE
	#IF IS_DEBUG_BUILD
	ELSE
		IF GET_FRAME_COUNT() % 90 = 0
			CDEBUG3LN(DEBUG_DELIVERY, "HAS_EVERYONE_STARTED_DELIVERY_FADE_SUPPRESS, not all supressing yet, iParticipantCount: ", iParticipantCount, " iParticipantsReady: ", iParticipantsReady)
		ENDIF
	#ENDIF
		
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_EVERYONE_STARTED_DELIVERY_CUTSCENE()
	INT i
	PARTICIPANT_INDEX participant
	INT iParticipantCount, iParticipantsReady
	REPEAT NUM_NETWORK_PLAYERS i
		participant = INT_TO_PARTICIPANTINDEX(i)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(participant)
			iParticipantCount++
			IF GET_PARTICIPANT_BROADCAST_BITSET(PBBS_CUTSCENE_STARTED, i)
				iParticipantsReady++
			ENDIF
		ENDIF
	ENDREPEAT
	
	//Fix to cehck if in vehicle
	IF iParticipantCount = iParticipantsReady
		#IF IS_DEBUG_BUILD
		IF GET_FRAME_TIME() % 120 = 0
			CDEBUG3LN(DEBUG_DELIVERY, "HAS_EVERYONE_STARTED_DELIVERY_CUTSCENE, all players are playing the cutscene.")
		ENDIF
		#ENDIF
		RETURN TRUE
	ELSE
		#IF IS_DEBUG_BUILD
		IF GET_FRAME_TIME() % 120 = 0
			CDEBUG3LN(DEBUG_DELIVERY, "HAS_EVERYONE_STARTED_DELIVERY_CUTSCENE, not all playing yet, iParticipantCount: ", iParticipantCount, " iParticipantsReady: ", iParticipantsReady)
		ENDIF
		#ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//######################################## DEBUG ########################################
#IF IS_DEBUG_BUILD

PROC DELIVERY_FLAG_DEFAULT_PRINT_ALL(SCRIPT_DATA &_scriptData)
	CDEBUG3LN(DEBUG_DELIVERY, "**") 
	CDEBUG3LN(DEBUG_DELIVERY, "** DELIVERY_FLAG_DEFAULT_PRINT_ALL - printing generic setup") 
	INT index
	DELIVERY_CUTSCENE_DEFAULT_FLAGS deliveryCutsceneBit
	REPEAT DELIVERY_FLAG_DEFAULT_MAX index
		deliveryCutsceneBit = INT_TO_ENUM(DELIVERY_CUTSCENE_DEFAULT_FLAGS, index)
		CDEBUG3LN(DEBUG_DELIVERY, "** DELIVERY_FLAG_DEFAULT_PRINT_ALL BIT: ", GET_DELIVERY_FLAG_DEFAULT_NAME(deliveryCutsceneBit), " Value: ", GET_DELIVERY_FLAG_DEFAULT(_scriptData.deliveryData, deliveryCutsceneBit))
	ENDREPEAT
	CDEBUG3LN(DEBUG_DELIVERY, "**") 
ENDPROC

PROC DELIVERY_FLAG_EXTRA_PRINT_ALL(SCRIPT_DATA &_scriptData)
	CDEBUG3LN(DEBUG_DELIVERY, "**") 
	CDEBUG3LN(DEBUG_DELIVERY, "** DELIVERY_FLAG_EXTRA_PRINT_ALL - printing generic setup") 
	INT index
	DELIVERY_CUTSCENE_EXTRA_FLAGS deliveryCutsceneBit
	REPEAT DELIVERY_FLAG_EXTRA_MAX index
		deliveryCutsceneBit = INT_TO_ENUM(DELIVERY_CUTSCENE_EXTRA_FLAGS, index)
		CDEBUG3LN(DEBUG_DELIVERY, "** DELIVERY_FLAG_EXTRA_PRINT_ALL BIT: ", GET_DELIVERY_FLAG_EXTRA_NAME(deliveryCutsceneBit), " Value: ", GET_DELIVERY_FLAG_EXTRA(_scriptData.deliveryData, deliveryCutsceneBit))
	ENDREPEAT
	CDEBUG3LN(DEBUG_DELIVERY, "**") 
ENDPROC

PROC DELIVERY_DEBUG_CREATE_WIDGETS(SCRIPT_DATA &_scriptData)
	START_WIDGET_GROUP("GB_DELIVERY")
		ADD_WIDGET_BOOL("bPlayCutscene", _scriptData.debugData.bPlayCutscene)
		ADD_WIDGET_BOOL("bDontBringIntoProperty", _scriptData.debugData.bDontBringIntoProperty)
		ADD_WIDGET_BOOL("bTerminateScript", _scriptData.debugData.bTerminateScript)
		ADD_WIDGET_STRING("------------------------------")
		ADD_WIDGET_BOOL("bCreateAssets", _scriptData.debugData.bCreateAssets)
		ADD_WIDGET_BOOL("bPlaceObjects", _scriptData.debugData.bPlaceObjects)
		ADD_WIDGET_INT_SLIDER("Cinematic scene var", _scriptData.debugData.iCinematicsceneVar, 0, 4, 1)
		
		START_NEW_WIDGET_COMBO()
			ADD_TO_WIDGET_COMBO("OBJECT")
			ADD_TO_WIDGET_COMBO("VEHICLE_PACK")
			ADD_TO_WIDGET_COMBO("VEHICLE_EXTRA")
			ADD_TO_WIDGET_COMBO("PEDS_EXTRA")
		STOP_WIDGET_COMBO("Entity", _scriptData.debugData.entityType)
		
		ADD_WIDGET_INT_SLIDER("iEntityIndex", _scriptData.debugData.iEntityIndex, 0, 9999, 1)
		ADD_WIDGET_VECTOR_SLIDER("Position", _scriptData.debugData.vPosition, -999999, 999999, 0.1)
		ADD_WIDGET_VECTOR_SLIDER("Rotation", _scriptData.debugData.vRotation, -999999, 999999, 0.1)
		
		
		//ADD_WIDGET_BOOL("localData.bDebugCreatePlayerClone", _localData.bDebugCreatePlayerClone)
		//ADD_WIDGET_BOOL("localData.bDebugDestroyPlayerClone", _localData.bDebugDestroyPlayerClone)
	STOP_WIDGET_GROUP()
ENDPROC

PROC DELIVERY_DEBUG_UPDATE(SCRIPT_DATA &_scriptData)	
	IF _scriptData.debugData.bPlaceObjects
		IF _scriptData.debugData.entityType <> _scriptData.debugData.entityTypePrev	
		OR _scriptData.debugData.iEntityIndex <> _scriptData.debugData.iEntityIndexPrev
			SWITCH _scriptData.debugData.entityType
				CASE DELIVERY_ENTITY_OBJECT
					IF _scriptData.debugData.iEntityIndex >= DELIVERY_ENTITY_OBJECT
						_scriptData.debugData.iEntityIndex = CONST_DELIVERY_MAX_OBJECTS-1
					ENDIF
					IF DOES_ENTITY_EXIST(_scriptData.deliveryData.cutsceneData.objectArray[_scriptData.debugData.iEntityIndex])
						_scriptData.debugData.vPosition = GET_ENTITY_COORDS(_scriptData.deliveryData.cutsceneData.objectArray[_scriptData.debugData.iEntityIndex])
						_scriptData.debugData.vRotation = GET_ENTITY_ROTATION(_scriptData.deliveryData.cutsceneData.objectArray[_scriptData.debugData.iEntityIndex])
					ENDIF
				BREAK
				
				CASE DELIVERY_ENTITY_VEHICLE_PACK
					IF _scriptData.debugData.iEntityIndex >= DELIVERY_ENTITY_VEHICLE_PACK
						_scriptData.debugData.iEntityIndex = CONST_DELIVERY_MAX_VEHICLES_PACK-1
					ENDIF
					IF DOES_ENTITY_EXIST(_scriptData.deliveryData.cutsceneData.vehiclePackArray[_scriptData.debugData.iEntityIndex].vehicle)
						_scriptData.debugData.vPosition = GET_ENTITY_COORDS(_scriptData.deliveryData.cutsceneData.vehiclePackArray[_scriptData.debugData.iEntityIndex].vehicle)
						_scriptData.debugData.vRotation = GET_ENTITY_ROTATION(_scriptData.deliveryData.cutsceneData.vehiclePackArray[_scriptData.debugData.iEntityIndex].vehicle)
					ENDIF
				BREAK
				
				CASE DELIVERY_ENTITY_VEHICLE_EXTRA
					IF _scriptData.debugData.iEntityIndex >= DELIVERY_ENTITY_VEHICLE_EXTRA
						_scriptData.debugData.iEntityIndex = CONST_DELIVERY_MAX_VEHICLES_EXTRA-1
					ENDIF
					IF DOES_ENTITY_EXIST(_scriptData.deliveryData.cutsceneData.vehicleArray[_scriptData.debugData.iEntityIndex])
						_scriptData.debugData.vPosition = GET_ENTITY_COORDS(_scriptData.deliveryData.cutsceneData.vehicleArray[_scriptData.debugData.iEntityIndex])
						_scriptData.debugData.vRotation = GET_ENTITY_ROTATION(_scriptData.deliveryData.cutsceneData.vehicleArray[_scriptData.debugData.iEntityIndex])
					ENDIF
				BREAK
				
				CASE DELIVERY_ENTITY_PEDS_EXTRA
					IF _scriptData.debugData.iEntityIndex >= DELIVERY_ENTITY_PEDS_EXTRA
						_scriptData.debugData.iEntityIndex = CONST_DELIVERY_MAX_PEDS_EXTRA-1
					ENDIF
					IF DOES_ENTITY_EXIST(_scriptData.deliveryData.cutsceneData.pedArray[_scriptData.debugData.iEntityIndex])
						_scriptData.debugData.vPosition = GET_ENTITY_COORDS(_scriptData.deliveryData.cutsceneData.pedArray[_scriptData.debugData.iEntityIndex])
						_scriptData.debugData.vRotation = GET_ENTITY_ROTATION(_scriptData.deliveryData.cutsceneData.pedArray[_scriptData.debugData.iEntityIndex])
					ENDIF
				BREAK
				
			ENDSWITCH
		
			CALL _scriptData.functionPointers.coreFunctions.handlerSceneSetup(_scriptData.deliveryData, _scriptData.deliveryData.cutsceneData.cutscene.iCurrentScene)
			CDEBUG3LN(DEBUG_DELIVERY, "*DEBUG* DELIVERY_DEBUG_UPDATE - switched entity")
			_scriptData.debugData.iEntityIndexPrev = _scriptData.debugData.iEntityIndex
			_scriptData.debugData.entityTypePrev = _scriptData.debugData.entityType
		ENDIF

	
	
		//CALL _scriptData.functionPointers.coreFunctions.handlerSceneSetup(_scriptData.deliveryData, _scriptData.deliveryData.cutsceneData.cutscene.iCurrentScene)
		//debugData.entityTypePrev = -1
		
		SWITCH _scriptData.debugData.entityType
			CASE DELIVERY_ENTITY_OBJECT
				IF DOES_ENTITY_EXIST(_scriptData.deliveryData.cutsceneData.objectArray[_scriptData.debugData.iEntityIndex])
				AND NOT IS_ENTITY_DEAD(_scriptData.deliveryData.cutsceneData.objectArray[_scriptData.debugData.iEntityIndex])
				AND NOT IS_ENTITY_ATTACHED(_scriptData.deliveryData.cutsceneData.objectArray[_scriptData.debugData.iEntityIndex])
					SET_ENTITY_COORDS_NO_OFFSET(_scriptData.deliveryData.cutsceneData.objectArray[_scriptData.debugData.iEntityIndex], _scriptData.debugData.vPosition)
					SET_ENTITY_ROTATION(_scriptData.deliveryData.cutsceneData.objectArray[_scriptData.debugData.iEntityIndex], _scriptData.debugData.vRotation)
				ENDIF
			BREAK
			CASE DELIVERY_ENTITY_VEHICLE_PACK
				IF DOES_ENTITY_EXIST(_scriptData.deliveryData.cutsceneData.vehiclePackArray[_scriptData.debugData.iEntityIndex].vehicle)
				AND NOT IS_ENTITY_DEAD(_scriptData.deliveryData.cutsceneData.vehiclePackArray[_scriptData.debugData.iEntityIndex].vehicle)
				AND NOT IS_ENTITY_ATTACHED(_scriptData.deliveryData.cutsceneData.vehiclePackArray[_scriptData.debugData.iEntityIndex].vehicle)
					SET_ENTITY_COORDS_NO_OFFSET(_scriptData.deliveryData.cutsceneData.vehiclePackArray[_scriptData.debugData.iEntityIndex].vehicle, _scriptData.debugData.vPosition)
					SET_ENTITY_ROTATION(_scriptData.deliveryData.cutsceneData.vehiclePackArray[_scriptData.debugData.iEntityIndex].vehicle, _scriptData.debugData.vRotation)
				ENDIF
			BREAK
			CASE DELIVERY_ENTITY_VEHICLE_EXTRA
				IF DOES_ENTITY_EXIST(_scriptData.deliveryData.cutsceneData.vehicleArray[_scriptData.debugData.iEntityIndex])
				AND NOT IS_ENTITY_DEAD(_scriptData.deliveryData.cutsceneData.vehicleArray[_scriptData.debugData.iEntityIndex])
				AND NOT IS_ENTITY_ATTACHED(_scriptData.deliveryData.cutsceneData.vehicleArray[_scriptData.debugData.iEntityIndex])
					SET_ENTITY_COORDS_NO_OFFSET(_scriptData.deliveryData.cutsceneData.vehicleArray[_scriptData.debugData.iEntityIndex], _scriptData.debugData.vPosition)
					SET_ENTITY_ROTATION(_scriptData.deliveryData.cutsceneData.vehicleArray[_scriptData.debugData.iEntityIndex], _scriptData.debugData.vRotation)
				ENDIF
			BREAK
			CASE DELIVERY_ENTITY_PEDS_EXTRA
				IF DOES_ENTITY_EXIST(_scriptData.deliveryData.cutsceneData.pedArray[_scriptData.debugData.iEntityIndex])
				AND NOT IS_PED_IN_ANY_VEHICLE(_scriptData.deliveryData.cutsceneData.pedArray[_scriptData.debugData.iEntityIndex])
					SET_ENTITY_COORDS_NO_OFFSET(_scriptData.deliveryData.cutsceneData.pedArray[_scriptData.debugData.iEntityIndex], _scriptData.debugData.vPosition)
					SET_ENTITY_ROTATION(_scriptData.deliveryData.cutsceneData.pedArray[_scriptData.debugData.iEntityIndex], _scriptData.debugData.vRotation)
				ENDIF
			BREAK
		ENDSWITCH
	ELSE
		//NETWORK_SET_IN_MP_CUTSCENE(FALSE, FALSE)
	ENDIF
	
	IF _scriptData.debugData.bCreateAssets
		NETWORK_SET_IN_MP_CUTSCENE(TRUE, FALSE)
		DETERMINE_DELIVERY_TYPE()
		DELIVERY_CREATE_CLONES()
		CALL _scriptData.functionPointers.coreFunctions.handlerAssetsCreate(_scriptData.deliveryData)
		_scriptData.debugData.bCreateAssets = FALSE
	ENDIF
//	
//	IF localData.bDebugDestroyPlayerClone
//		DELIVERY_DELETE_ASSETS(_localData)
//		localData.bDebugDestroyPlayerClone = FALSE
//	ENDIF
ENDPROC
#ENDIF


///////////// TEMPORARY OLD !
///    
///    
///    

#IF IS_DEBUG_BUILD
FUNC STRING DEBUG_GET_FADE_AND_MOVE_ENTITIES_STATE_NAME(DELIVERY_FADE_AND_MOVE_ENTITIES_STATE eState)
	SWITCH eState		
		CASE MES_FADE_OUT_ENTITIES		RETURN "MES_FADE_OUT_ENTITIES"
		CASE MES_WARP_VEHICLE			RETURN "MES_WARP_VEHICLE"
		CASE MES_WARP_PLAYER			RETURN "MES_WARP_PLAYER"
		CASE MES_DELETE_VEHICLE			RETURN "MES_DELETE_VEHICLE"
		CASE MES_FADE_IN_VEHICLE		RETURN "MES_FADE_IN_VEHICLE"
		CASE MES_DONE					RETURN "MES_DONE"
		CASE MES_WARP_PV				RETURN "MES_WARP_PV"
		CASE MES_WARP_PEGASUS_VEH		RETURN "MES_WARP_PEGASUS_VEH"
		CASE MES_WARP_BOSS_VEH 			RETURN "MES_WARP_BOSS_VEH"
		CASE MES_AWAIT_BOSS_VEH_FADE_IN RETURN "MES_AWAIT_BOSS_VEH_FADE_IN"
	ENDSWITCH
	
	RETURN "***unknown***"
ENDFUNC

FUNC STRING DEBUG_GET_DELIVERY_DELETE_DELIVERY_VEH_STATE_NAME(DELIVERY_DELETE_DELIVERY_VEH_STATE eState)
	SWITCH eState
		CASE DV_STATE_FADE_OUT_VEH		RETURN "DV_STATE_FADE_OUT_VEH"
		CASE DV_STATE_MOVE_OUT_OF_VEH	RETURN "DV_STATE_MOVE_OUT_OF_VEH"
		CASE DV_STATE_DELETE			RETURN "DV_STATE_DELETE"
		CASE DV_STATE_DONE				RETURN "DV_STATE_DONE"
		CASE DV_STATE_REGISTER_ENTITY	RETURN "DV_STATE_REGISTER_ENTITY"
	ENDSWITCH
	
	RETURN "***unknown***"
ENDFUNC
#ENDIF

#IF FEATURE_TUNER
PROC SET_DELIVERY_DELETE_DELIVERY_VEH_STATE(DELIVERY_DELETE_DELIVERY_VEH_STATE eNewState)
	IF eDeleteVehState != eNewState
		CDEBUG1LN(DEBUG_DELIVERY, "SET_DELIVERY_DELETE_DELIVERY_VEH_STATE new state = ",DEBUG_GET_DELIVERY_DELETE_DELIVERY_VEH_STATE_NAME(eNewState), " from: ", DEBUG_GET_DELIVERY_DELETE_DELIVERY_VEH_STATE_NAME(eDeleteVehState))
		eDeleteVehState = eNewState
	ENDIF
ENDPROC
#ENDIF

#IF FEATURE_TUNER
PROC REGISTER_AMBIENT_VEHICLE_WITH_THREAD(BOOL &bVehEntityGrabbed)
	IF NETWORK_HAS_ENTITY_BEEN_REGISTERED_WITH_THIS_THREAD(scriptData.deliveryData.deliveryVehicle)
		CDEBUG3LN(DEBUG_DELIVERY, " MAINTAIN_CLEANUP_AMBIENT_VEHICLE_DURING_SCENE - MES_FADE_OUT_ENTITIES - Registered vehicle with this script")
		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(VEH_TO_NET(scriptData.deliveryData.deliveryVehicle))
			SET_NETWORK_ID_CAN_MIGRATE(VEH_TO_NET(scriptData.deliveryData.deliveryVehicle), FALSE)
			NETWORK_FADE_OUT_ENTITY(scriptData.deliveryData.deliveryVehicle, TRUE, TRUE)
			RESET_ENTITY_ALPHA(scriptData.deliveryData.deliveryVehicle)
			bVehEntityGrabbed = TRUE
			CDEBUG3LN(DEBUG_DELIVERY, " MAINTAIN_CLEANUP_AMBIENT_VEHICLE_DURING_SCENE - REGISTER_AMBIENT_VEHICLE_WITH_THREAD - Fading out the vehicle.")
		ELSE
			CDEBUG3LN(DEBUG_DELIVERY, " MAINTAIN_CLEANUP_AMBIENT_VEHICLE_DURING_SCENE - REGISTER_AMBIENT_VEHICLE_WITH_THREAD - Requesting control of netID")
			NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(VEH_TO_NET(scriptData.deliveryData.deliveryVehicle))
		ENDIF
	ELSE
		CDEBUG3LN(DEBUG_DELIVERY, "MAINTAIN_CLEANUP_AMBIENT_VEHICLE_DURING_SCENE - REGISTER_AMBIENT_VEHICLE_WITH_THREAD - mission entity: ", IS_ENTITY_A_MISSION_ENTITY(scriptData.deliveryData.deliveryVehicle)
		, " ControlGrab: ", IS_BIT_SET_ENUM(scriptData.coreData.iBS, BS_GRABBING_CONTROL_OF_AMBIENT_VEH)
		, " MissionVehOnDelivery: ", GET_CORE_BITSET(DELIVERY_FLAG_CORE_DELIVERY_VEHICLE_IS_MISSION))
		
		SET_BIT_ENUM(scriptData.coreData.iBS, BS_GRABBING_CONTROL_OF_AMBIENT_VEH)
		BOOL bScriptHostObject
		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			bScriptHostObject = TRUE
		ELSE
			bScriptHostObject = FALSE
		ENDIF
		
		CDEBUG3LN(DEBUG_DELIVERY, "MAINTAIN_CLEANUP_AMBIENT_VEHICLE_DURING_SCENE - non mission forcing to be mission, ", IS_ENTITY_A_MISSION_ENTITY(scriptData.deliveryData.deliveryVehicle))
		SET_ENTITY_AS_MISSION_ENTITY(scriptData.deliveryData.deliveryVehicle,bScriptHostObject,TRUE)
	ENDIF
ENDPROC

PROC MAINTAIN_CLEANUP_AMBIENT_VEHICLE_DURING_SCENE()
	
	IF NOT IS_ENTITY_ALIVE(PLAYER_PED_ID())
		CDEBUG3LN(DEBUG_DELIVERY, "MAINTAIN_CLEANUP_AMBIENT_VEHICLE_DURING_SCENE - Player is dead")
		EXIT
	ENDIF
	
	//Set our vehicle alpha to maintain local visibility
	IF DOES_ENTITY_EXIST(scriptData.deliveryData.deliveryVehicle)
	AND NOT scriptData.deliveryData.cutsceneData.cutscene.bPlaying		
		RESET_ENTITY_ALPHA(scriptData.deliveryData.deliveryVehicle)
		RESET_ENTITY_ALPHA(PLAYER_PED_ID())
	ENDIF
	
	//Only delete the vehicle once the scene has started
	IF NOT HAS_EVERYONE_STARTED_DELIVERY_CUTSCENE()
		//CDEBUG3LN(DEBUG_DELIVERY, "MAINTAIN_CLEANUP_AMBIENT_VEHICLE_DURING_SCENE - Waiting on cutscene to start")
		EXIT
	ENDIF
	
	SWITCH eDeleteVehState
		
		CASE DV_STATE_FADE_OUT_VEH
			
			BOOL bVehEntityGrabbed
			
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())				
				NETWORK_FADE_OUT_ENTITY(PLAYER_PED_ID(), TRUE, TRUE)
				FORCE_PLAYER_LOCALLY_VISIBLE_FOR_FADE()
			ENDIF
			
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF IS_ENTITY_ALIVE(scriptData.deliveryData.deliveryVehicle)
					
					BOOL bDriver, bMissionEntity, bPV
					
					bDriver 		= GET_PED_IN_VEHICLE_SEAT(scriptData.deliveryData.deliveryVehicle, VS_DRIVER) = PLAYER_PED_ID()
					bMissionEntity 	= (IS_ENTITY_A_MISSION_ENTITY(scriptData.deliveryData.deliveryVehicle) AND NOT IS_BIT_SET_ENUM(scriptData.coreData.iBS, BS_GRABBING_CONTROL_OF_AMBIENT_VEH))
					bPV 			= IS_VEHICLE_A_PERSONAL_VEHICLE(scriptData.deliveryData.deliveryVehicle)
					
					IF NOT bDriver
					OR bMissionEntity
					OR bPV
						SET_DELIVERY_DELETE_DELIVERY_VEH_STATE(DV_STATE_MOVE_OUT_OF_VEH)
						CDEBUG3LN(DEBUG_DELIVERY, " MAINTAIN_CLEANUP_AMBIENT_VEHICLE_DURING_SCENE - not deleting vehicle. Driver: ", bDriver, " mission: ", bMissionEntity, " PV: ", bPV)
						EXIT						
					ELSE
						REGISTER_AMBIENT_VEHICLE_WITH_THREAD(bVehEntityGrabbed)
					ENDIF
				ENDIF
				
				IF bVehEntityGrabbed
					SET_DELIVERY_DELETE_DELIVERY_VEH_STATE(DV_STATE_MOVE_OUT_OF_VEH)
				ENDIF
			ELSE
				CDEBUG3LN(DEBUG_DELIVERY, "MAINTAIN_CLEANUP_AMBIENT_VEHICLE_DURING_SCENE - Not in a vehicle so move on")
				SET_DELIVERY_DELETE_DELIVERY_VEH_STATE(DV_STATE_DELETE)
			ENDIF
		BREAK
		
		CASE DV_STATE_MOVE_OUT_OF_VEH
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF IS_ENTITY_ALIVE(scriptData.deliveryData.deliveryVehicle)
					IF NOT NETWORK_IS_ENTITY_FADING(scriptData.deliveryData.deliveryVehicle)
						TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID(), 0, ECF_WARP_PED)
					ENDIF
				ENDIF
			ELSE
				VECTOR vWarpCoords
				
				vWarpCoords = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
				vWarpCoords.z -= 10.0
				
				SET_ENTITY_COORDS(PLAYER_PED_ID(), vWarpCoords, FALSE, FALSE, FALSE, FALSE)
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
				SET_DELIVERY_DELETE_DELIVERY_VEH_STATE(DV_STATE_DELETE)
			ENDIF
		BREAK
		
		CASE DV_STATE_REGISTER_ENTITY
			IF IS_ENTITY_ALIVE(scriptData.deliveryData.deliveryVehicle)
			AND NETWORK_HAS_CONTROL_OF_ENTITY(scriptData.deliveryData.deliveryVehicle)		
				BOOL bVehEntityRegistered
					
				REGISTER_AMBIENT_VEHICLE_WITH_THREAD(bVehEntityRegistered)
				
				IF bVehEntityRegistered
					SET_DELIVERY_DELETE_DELIVERY_VEH_STATE(DV_STATE_DELETE)
				ENDIF
			ELIF NOT IS_ENTITY_ALIVE(scriptData.deliveryData.deliveryVehicle) 
			OR IS_ENTITY_A_MISSION_ENTITY(scriptData.deliveryData.deliveryVehicle)
				SET_DELIVERY_DELETE_DELIVERY_VEH_STATE(DV_STATE_DELETE)
			ENDIF
		BREAK
		
		CASE DV_STATE_DELETE
			
			IF IS_ENTITY_ALIVE(scriptData.deliveryData.deliveryVehicle)
				IF NOT IS_ENTITY_A_MISSION_ENTITY(scriptData.deliveryData.deliveryVehicle)
					CDEBUG3LN(DEBUG_DELIVERY, "MAINTAIN_CLEANUP_AMBIENT_VEHICLE_DURING_SCENE - DV_STATE_DELETE - vhicle not a mission entity.")
					SET_DELIVERY_DELETE_DELIVERY_VEH_STATE(DV_STATE_REGISTER_ENTITY)
					EXIT
				ENDIF
			
				IF NOT NETWORK_IS_ENTITY_FADING(scriptData.deliveryData.deliveryVehicle)
					IF NETWORK_HAS_CONTROL_OF_ENTITY(scriptData.deliveryData.deliveryVehicle)					
						
						IF IS_VEHICLE_EMPTY(scriptData.deliveryData.deliveryVehicle, TRUE, TRUE, FALSE, TRUE, FALSE, TRUE)
							DELETE_VEHICLE(scriptData.deliveryData.deliveryVehicle)
						ELSE
							CDEBUG3LN(DEBUG_DELIVERY, "MAINTAIN_CLEANUP_AMBIENT_VEHICLE_DURING_SCENE - DV_STATE_DELETE - doing nothing - Waiting for the vehicle to be empty")
						ENDIF
						
					ELSE
						CDEBUG3LN(DEBUG_DELIVERY, "MAINTAIN_CLEANUP_AMBIENT_VEHICLE_DURING_SCENE - DV_STATE_DELETE - doing nothing - We do not have control")
					ENDIF
				ENDIF
			ELSE
				CDEBUG3LN(DEBUG_DELIVERY, "MAINTAIN_CLEANUP_AMBIENT_VEHICLE_DURING_SCENE - DV_STATE_DELETE - vhicle no longer exists")
				SET_DELIVERY_DELETE_DELIVERY_VEH_STATE(DV_STATE_DONE)
			ENDIF
		BREAK
		
		CASE DV_STATE_DONE
			//Do nothing
		BREAK
		
	ENDSWITCH
ENDPROC
#ENDIF

PROC SET_DELIVERY_FADE_AND_MOVE_ENTITIES_STATE(DELIVERY_FADE_AND_MOVE_ENTITIES_STATE eNewState)
	IF eFadeAndMoveState != eNewState
		CDEBUG1LN(DEBUG_DELIVERY, "MAINTAIN_FADE_AND_MOVE_ON_DELIVERY new state = ",DEBUG_GET_FADE_AND_MOVE_ENTITIES_STATE_NAME(eNewState), " from: ", DEBUG_GET_FADE_AND_MOVE_ENTITIES_STATE_NAME(eFadeAndMoveState))
		eFadeAndMoveState = eNewState
	ENDIF
ENDPROC

  FUNC BOOL IS_VEHICLE_TO_MOVE_TOWING_PERSONAL_AA_TRAILER(VEHICLE_INDEX theVeh, VEHICLE_INDEX &towedVehicle)
	towedVehicle = NULL
	IF IS_VEHICLE_ATTACHED_TO_TRAILER(theVeh)
		GET_VEHICLE_TRAILER_VEHICLE(theVeh,towedVehicle)
		IF IS_VEHICLE_A_PERSONAL_VEHICLE(towedVehicle)
		AND GET_ENTITY_MODEL(towedVehicle) = TRAILERSMALL2
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL GB_DELIVERY_SHOULD_REQUEST_PV_WARP()	
	IF FMBB_DROPOFF_IS_PLAYER_PROPERTY(scriptData.deliveryData.eDropoffID)
	AND IS_VEHICLE_A_PERSONAL_HACKER_TRUCK(thisSceneMoveEntityStruct.vehVehicleToTmove)
		RETURN NOT ARE_MULTIPLE_FREEMODE_DELIVERABLES_IN_VEH(scriptData.deliveryData.eDeliverableID, INVALID_PLAYER_INDEX())
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC MAINTAIN_FADE_AND_HIDE_DURING_DELIVERY()
	IF NOT IS_ENTITY_ALIVE(PLAYER_PED_ID())
		CDEBUG3LN(DEBUG_DELIVERY, "MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - Player is dead")
		EXIT
	ENDIF
	
	//Set our vehicle alpha to maintain local visibility
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	AND NOT scriptData.deliveryData.cutsceneData.cutscene.bPlaying
		thisSceneMoveEntityStruct.vehVehicleToTmove = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())		
		RESET_ENTITY_ALPHA(thisSceneMoveEntityStruct.vehVehicleToTmove)
		RESET_ENTITY_ALPHA(PLAYER_PED_ID())
	ENDIF
	
	//Only hide the player once the scene has started
	IF NOT HAS_EVERYONE_STARTED_DELIVERY_CUTSCENE()
		EXIT
	ENDIF
	
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())				
		NETWORK_FADE_OUT_ENTITY(PLAYER_PED_ID(), TRUE, TRUE)
		FORCE_PLAYER_LOCALLY_VISIBLE_FOR_FADE()
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		CDEBUG3LN(DEBUG_DELIVERY, "MFAM -MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - IS_PED_IN_ANY_VEHICLE..")
		IF IS_ENTITY_ALIVE(thisSceneMoveEntityStruct.vehVehicleToTmove)
			IF NETWORK_HAS_CONTROL_OF_ENTITY(thisSceneMoveEntityStruct.vehVehicleToTmove)
				NETWORK_FADE_OUT_ENTITY(thisSceneMoveEntityStruct.vehVehicleToTmove, TRUE, TRUE)
				RESET_ENTITY_ALPHA(thisSceneMoveEntityStruct.vehVehicleToTmove)
				FREEZE_ENTITY_POSITION(thisSceneMoveEntityStruct.vehVehicleToTmove, TRUE)
				
				SET_ENTITY_COLLISION(thisSceneMoveEntityStruct.vehVehicleToTmove, FALSE)
				SET_ENTITY_INVINCIBLE(thisSceneMoveEntityStruct.vehVehicleToTmove, TRUE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_FADE_AND_MOVE_ON_DELIVERY()

	VECTOR vPlayerPos
	BOOL bVehEntityGrabbed = FALSE
	
	IF NOT IS_ENTITY_ALIVE(PLAYER_PED_ID())
		CDEBUG3LN(DEBUG_DELIVERY, "MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - Player is dead")
		EXIT
	ENDIF
	
	//Set our vehicle alpha to maintain local visibility
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	AND NOT scriptData.deliveryData.cutsceneData.cutscene.bPlaying
		thisSceneMoveEntityStruct.vehVehicleToTmove = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())		
		RESET_ENTITY_ALPHA(thisSceneMoveEntityStruct.vehVehicleToTmove)
		RESET_ENTITY_ALPHA(PLAYER_PED_ID())
		//CDEBUG3LN(DEBUG_DELIVERY, "RESET_ENTITY_ALPHA")
	ENDIF
	
	
	//Only move the player once the scene has started
	IF NOT HAS_EVERYONE_STARTED_DELIVERY_CUTSCENE()
		//CDEBUG3LN(DEBUG_DELIVERY, "MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - entities are fading out. Waiting on cutscene to start")
		EXIT
	ENDIF
	
	//Only start the fade once we are in a position to stream assets before starting the scene
	//IF NOT IS_BIT_SET_ENUM(scriptData.coreData.iBS, BS_DELIVERY_FADE_AND_HIDE_ENTITIES)
	//OR IS_BIT_SET_ENUM(scriptData.coreData.iBS, BS_PREVENT_FADE_FOR_MISSION_VEHICLES)
	//OR (scriptData.bSpecialOnFootScene AND (IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND NOT IS_VEHICLE_EMPTY(thisSceneMoveEntityStruct.vehVehicleToTmove, FALSE, TRUE, TRUE)))
		//Nothing to do just yet
		//CDEBUG3LN(DEBUG_DELIVERY, "MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - NOT BS_DELIVERY_FADE_AND_HIDE_ENTITIES / BS_PREVENT_FADE_FOR_MISSION_VEHICLES")
		//EXIT
	//ENDIF
	
	SWITCH eFadeAndMoveState
		
		CASE MES_FADE_OUT_ENTITIES
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())				
				NETWORK_FADE_OUT_ENTITY(PLAYER_PED_ID(), TRUE, TRUE)
				FORCE_PLAYER_LOCALLY_VISIBLE_FOR_FADE()
			ENDIF
			
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				CDEBUG3LN(DEBUG_DELIVERY, "MFAM -MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - IS_PED_IN_ANY_VEHICLE..")
				IF IS_ENTITY_ALIVE(thisSceneMoveEntityStruct.vehVehicleToTmove)
					CDEBUG3LN(DEBUG_DELIVERY, "MFAM -MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - ALIVE VEH")
					//Save out the model for layer use
					IF thisSceneMoveEntityStruct.eVehModel = DUMMY_MODEL_FOR_SCRIPT
						thisSceneMoveEntityStruct.eVehModel = GET_ENTITY_MODEL(thisSceneMoveEntityStruct.vehVehicleToTmove)
					ENDIF
					
					VEHICLE_INDEX towedVehicle
					IF IS_VEHICLE_TO_MOVE_TOWING_PERSONAL_AA_TRAILER(thisSceneMoveEntityStruct.vehVehicleToTmove, towedVehicle)	
						VECTOR vSpawnLocation
						FLOAT fSpawnHeading
						VEHICLE_SPAWN_LOCATION_PARAMS vehicleSpawnLocationParams
						
						vehicleSpawnLocationParams.fMinDistFromCoords 		= 20
						vehicleSpawnLocationParams.fMaxDistance 			= 150
						vehicleSpawnLocationParams.bConsiderHighways 		= TRUE
						vehicleSpawnLocationParams.bCheckEntityArea 		= TRUE
						vehicleSpawnLocationParams.bCheckOwnVisibility 		= FALSE
						vehicleSpawnLocationParams.bConsiderOnlyActiveNodes = TRUE
						
						IF NOT HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(GET_ENTITY_COORDS(towedVehicle), <<0.0, 0.0, 0.0>>, GET_ENTITY_MODEL(towedVehicle), TRUE, vSpawnLocation, fSpawnHeading, vehicleSpawnLocationParams)
							CDEBUG3LN(DEBUG_DELIVERY, "MFAM - MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - towing AA, not warped...")
							EXIT
						ELSE
							DETACH_VEHICLE_FROM_TRAILER(thisSceneMoveEntityStruct.vehVehicleToTmove)
							
							SET_ENTITY_COORDS_NO_OFFSET(towedVehicle, vSpawnLocation)
							SET_ENTITY_HEADING(towedVehicle, fSpawnHeading)
							
							CDEBUG3LN(DEBUG_DELIVERY, "MFAM - MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - towing AA, warped to ", vSpawnLocation, "!")
						ENDIF
					ENDIF
					
//					IF IS_VEHICLE_CARGOBOB(thisSceneMoveEntityStruct.vehVehicleToTmove)
//						ENTITY_INDEX entityAttachedCargobob
//
//						entityAttachedCargobob = GET_VEHICLE_ATTACHED_TO_CARGOBOB(thisSceneMoveEntityStruct.vehVehicleToTmove)
//						IF DOES_ENTITY_EXIST(entityAttachedCargobob)
//							VEHICLE_INDEX vehicleAttachedCargobob
//							vehicleAttachedCargobob = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(entityAttachedCargobob)
//							IF DOES_ENTITY_EXIST(vehicleAttachedCargobob)
//								CDEBUG3LN(DEBUG_DELIVERY, "MFAM - MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - Cargobob and has an vehicle attached, model: ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(vehicleAttachedCargobob)), " detaching event sent.")
//								
//								PED_INDEX pedDriver
//								pedDriver = GET_PED_IN_VEHICLE_SEAT(vehicleAttachedCargobob)
//								IF IS_ENTITY_ALIVE(pedDriver)
//									IF IS_PED_A_PLAYER(pedDriver)
//										PLAYER_INDEX piPlayerDriver
//										piPlayerDriver = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedDriver)
//										IF  piPlayerDriver <> INVALID_PLAYER_INDEX()
//											BROADCAST_DETACH_VEHICLE(piPlayerDriver, vehicleAttachedCargobob)
//											CDEBUG3LN(DEBUG_DELIVERY, "MFAM - MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - detach event sent.")
//										ELSE
//											CDEBUG3LN(DEBUG_DELIVERY, "MFAM - MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - invalid_player_index in driver seat.")
//										ENDIF
//									ELSE
//										CDEBUG3LN(DEBUG_DELIVERY, "MFAM - MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - driver ped is not a player.")
//									ENDIF
//								ELSE
//									CDEBUG3LN(DEBUG_DELIVERY, "MFAM - MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - no noe in vehicle seat/ not alive.")
//								ENDIF
//
//							ELSE
//								CDEBUG3LN(DEBUG_DELIVERY, "MFAM - MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - Cargobob but has no vehicle attached.")
//							ENDIF
//						ELSE
//							CDEBUG3LN(DEBUG_DELIVERY, "MFAM - MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - Cargobob but has no Entityvehicle attached.")
//						ENDIF
//						
//					ENDIF
					
//					IF IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(thisSceneMoveEntityStruct.vehVehicleToTmove)
//						CDEBUG3LN(DEBUG_DELIVERY, "MFAM - MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - Vehicle attached to something, model: ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(thisSceneMoveEntityStruct.vehVehicleToTmove)), " detaching.")
//						ENTITY_INDEX entityAttachedTo
//						entityAttachedTo = GET_ENTITY_ATTACHED_TO(thisSceneMoveEntityStruct.vehVehicleToTmove)
//						IF DOES_ENTITY_EXIST(entityAttachedTo)
//						AND NOT IS_ENTITY_DEAD(entityAttachedTo)
//							CDEBUG3LN(DEBUG_DELIVERY, "MFAM - MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - vehicle is attached to an entity, model: ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(entityAttachedTo)), " detaching.")
//							VEHICLE_INDEX vehicleAttachedTo
//							vehicleAttachedTo = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(entityAttachedTo)
//							
//							IF IS_VEHICLE_CARGOBOB(vehicleAttachedTo)
//							AND NOT IS_ENTITY_DEAD(vehicleAttachedTo)
//								CDEBUG3LN(DEBUG_DELIVERY, "MFAM - MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - vehicle attached to entity that is a cargobob vehicle, detaching.")
//								DETACH_VEHICLE_FROM_ANY_CARGOBOB(thisSceneMoveEntityStruct.vehVehicleToTmove)
//							ENDIF
//						ELSE
//							CDEBUG3LN(DEBUG_DELIVERY, "MFAM - MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - vehicle atatched to an entity but unable to grab reference.")
//						ENDIF
//						
//						DETACH_ENTITY(thisSceneMoveEntityStruct.vehVehicleToTmove)
//					ENDIF
					
					IF IS_VEHICLE_ARMORY_TRUCK(thisSceneMoveEntityStruct.vehVehicleToTmove)
					
						SET_DELIVERY_FADE_AND_MOVE_ENTITIES_STATE(MES_DONE)
						CDEBUG3LN(DEBUG_DELIVERY, "MFAM - MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - we are in an MOC")
						EXIT
						
					ELIF IS_VEHICLE_A_PERSONAL_ARMORY_AIRCRAFT(thisSceneMoveEntityStruct.vehVehicleToTmove, FALSE)
						
						IF IS_BIT_SET(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_GLOBAL_DATA_DONT_BRING_VEHICLE_INSIDE)
							SET_DELIVERY_FADE_AND_MOVE_ENTITIES_STATE(MES_WARP_PLAYER)
							CDEBUG3LN(DEBUG_DELIVERY, "MFAM - MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - we are in an Avenger that should not be brought inside")
						ELSE
							SET_DELIVERY_FADE_AND_MOVE_ENTITIES_STATE(MES_DONE)
							CDEBUG3LN(DEBUG_DELIVERY, "MFAM - MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - we are in an Avenger")
						ENDIF
						
						EXIT
							
					ELIF NOT IS_PED_SITTING_IN_VEHICLE_SEAT(PLAYER_PED_ID(), thisSceneMoveEntityStruct.vehVehicleToTmove, VS_DRIVER)
						
						IF NOT IS_VEHICLE_A_PERSONAL_VEHICLE(thisSceneMoveEntityStruct.vehVehicleToTmove)
						OR GET_OWNER_OF_PERSONAL_VEHICLE(thisSceneMoveEntityStruct.vehVehicleToTmove) != scriptData.deliveryData.deliveryOwner
							CDEBUG3LN(DEBUG_DELIVERY, "MFAM - MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - Setting BS2_SIMPLE_INTERIOR_GLOBAL_DATA_DONT_BRING_VEHICLE_INSIDE.")
							SET_BIT(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_GLOBAL_DATA_DONT_BRING_VEHICLE_INSIDE)
						ENDIF
						
						SET_DELIVERY_FADE_AND_MOVE_ENTITIES_STATE(MES_WARP_PLAYER)
						CDEBUG3LN(DEBUG_DELIVERY, "MFAM - MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - in a vehicle but not the driver.")
						EXIT
						
					ELIF GB_IS_PLAYER_DRIVING_BOSS_LIMO(PLAYER_ID())
						
						GB_SET_GLOBAL_CLIENT_BIT1(eGB_GLOBAL_CLIENT_BITSET_1_LOCAL_REQUEST_BOSS_LIMO_FADE_OUT)						
						SET_BIT(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_GLOBAL_DATA_DONT_BRING_VEHICLE_INSIDE)
						SET_DELIVERY_FADE_AND_MOVE_ENTITIES_STATE(MES_WARP_BOSS_VEH)
						thisSceneMoveEntityStruct.piBossVehOwner = GB_GET_LOCAL_PLAYER_GANG_BOSS()
						CDEBUG3LN(DEBUG_DELIVERY, "MFAM - MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - Requesting boss limo be moved elsewhere. Owner is: ", GET_PLAYER_NAME(thisSceneMoveEntityStruct.piBossVehOwner))
						EXIT
						
					ELIF IS_THIS_VEHICLE_A_PEGASUS_VEHICLE(thisSceneMoveEntityStruct.vehVehicleToTmove)
						thisSceneMoveEntityStruct.iPegasusVehOwner = GET_OWNER_OF_PEGASUS_VEHICLE(thisSceneMoveEntityStruct.vehVehicleToTmove)
						SET_BIT(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_GLOBAL_DATA_DONT_BRING_VEHICLE_INSIDE)
						SET_DELIVERY_FADE_AND_MOVE_ENTITIES_STATE(MES_WARP_PEGASUS_VEH)
						CDEBUG3LN(DEBUG_DELIVERY, "MFAM - MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - Moving to stage Requesting pegasus vehicle warp")
						EXIT
					ELIF NOT IS_VEHICLE_A_PERSONAL_VEHICLE(thisSceneMoveEntityStruct.vehVehicleToTmove)
					AND NOT IS_VEHICLE_A_PERSONAL_HACKER_TRUCK(thisSceneMoveEntityStruct.vehVehicleToTmove)
						CDEBUG3LN(DEBUG_DELIVERY, "MFAM - MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - NOT IS_VEHICLE_A_PERSONAL_VEHICLE")
						
						IF NETWORK_HAS_ENTITY_BEEN_REGISTERED_WITH_THIS_THREAD(thisSceneMoveEntityStruct.vehVehicleToTmove)
							CDEBUG3LN(DEBUG_DELIVERY, "MFAM - MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - MES_FADE_OUT_ENTITIES - Registered vehicle with this script")
							IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(VEH_TO_NET(thisSceneMoveEntityStruct.vehVehicleToTmove))
								NETWORK_FADE_OUT_ENTITY(thisSceneMoveEntityStruct.vehVehicleToTmove, TRUE, TRUE)
								RESET_ENTITY_ALPHA(thisSceneMoveEntityStruct.vehVehicleToTmove)
								bVehEntityGrabbed = TRUE
								CDEBUG3LN(DEBUG_DELIVERY, "MFAM - MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - MES_FADE_OUT_ENTITIES - Fading out the vehicle.")
							ELSE
								CDEBUG3LN(DEBUG_DELIVERY, "MFAM - MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - MES_FADE_OUT_ENTITIES - Requesting control of netID")
								NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(VEH_TO_NET(thisSceneMoveEntityStruct.vehVehicleToTmove))
							ENDIF
						ELSE
							CDEBUG3LN(DEBUG_DELIVERY, "MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - mission entity: ", IS_ENTITY_A_MISSION_ENTITY(thisSceneMoveEntityStruct.vehVehicleToTmove)
							, " ControlGrab: ", IS_BIT_SET_ENUM(scriptData.coreData.iBS, BS_GRABBING_CONTROL_OF_AMBIENT_VEH)
							, " MissionVehOnDelivery: ", GET_CORE_BITSET(DELIVERY_FLAG_CORE_DELIVERY_VEHICLE_IS_MISSION))
							
							FREEMODE_DELIVERABLE_TYPE eType
							eType = FREEMODE_DELIVERY_GET_DELIVERABLE_TYPE(scriptData.deliveryData.eDeliverableID)
							
							IF (IS_ENTITY_A_MISSION_ENTITY(thisSceneMoveEntityStruct.vehVehicleToTmove) AND NOT IS_BIT_SET_ENUM(scriptData.coreData.iBS, BS_GRABBING_CONTROL_OF_AMBIENT_VEH))
							OR GET_CORE_BITSET(DELIVERY_FLAG_CORE_DELIVERY_VEHICLE_IS_MISSION)
								SET_BIT_ENUM(scriptData.coreData.iBS, BS_PREVENT_FADE_FOR_MISSION_VEHICLES)
								SET_BIT(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_GLOBAL_DATA_DONT_BRING_VEHICLE_INSIDE)
								CDEBUG3LN(DEBUG_DELIVERY, "MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - mission entity, move player out")
								SET_DELIVERY_FADE_AND_MOVE_ENTITIES_STATE(MES_WARP_PLAYER)								
								
								IF (DECOR_EXIST_ON(scriptData.deliveryData.deliveryVehicle,"FMDeliverableID") 
								AND eType = FM_GANGOPS_DELIVERABLE_VEHICLE)
								OR (FREEMODE_DELIVERY_IS_TYPE_FMBB(eType)
								AND NOT DECOR_EXIST_ON(scriptData.deliveryData.deliveryVehicle,"FMCVehicle"))
									CDEBUG3LN(DEBUG_DELIVERY, "MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - NETWORK_FADE_OUT_ENTITY(scriptData.deliveryData.deliveryVehicle, TRUE, TRUE)")
									NETWORK_FADE_OUT_ENTITY(scriptData.deliveryData.deliveryVehicle, TRUE, TRUE)
								ENDIF
							ELSE
								SET_BIT_ENUM(scriptData.coreData.iBS, BS_GRABBING_CONTROL_OF_AMBIENT_VEH)
								BOOL bScriptHostObject
								
								IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
									bScriptHostObject = TRUE
								ELSE
									bScriptHostObject = FALSE
								ENDIF
								CDEBUG3LN(DEBUG_DELIVERY, "MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - non mission forcing to be mission, ", IS_ENTITY_A_MISSION_ENTITY(thisSceneMoveEntityStruct.vehVehicleToTmove))
								SET_ENTITY_AS_MISSION_ENTITY(thisSceneMoveEntityStruct.vehVehicleToTmove,bScriptHostObject,TRUE)
								
								IF FREEMODE_DELIVERY_IS_TYPE_FMBB(eType)
								AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(VEH_TO_NET(thisSceneMoveEntityStruct.vehVehicleToTmove))
									NETWORK_FADE_OUT_ENTITY(thisSceneMoveEntityStruct.vehVehicleToTmove, TRUE, TRUE)
									RESET_ENTITY_ALPHA(thisSceneMoveEntityStruct.vehVehicleToTmove)
									bVehEntityGrabbed = TRUE
									CDEBUG3LN(DEBUG_DELIVERY, "MFAM - MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - MES_FADE_OUT_ENTITIES - Fading out the vehicle for FMBB.")
								ENDIF
							ENDIF
						ENDIF

						SET_BIT(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_GLOBAL_DATA_DONT_BRING_VEHICLE_INSIDE)
					ELSE
						
						IF IS_VEHICLE_A_PERSONAL_VEHICLE(thisSceneMoveEntityStruct.vehVehicleToTmove)
						AND GET_OWNER_OF_PERSONAL_VEHICLE(thisSceneMoveEntityStruct.vehVehicleToTmove) != scriptData.deliveryData.deliveryOwner
						OR IS_VEHICLE_A_PERSONAL_HACKER_TRUCK(thisSceneMoveEntityStruct.vehVehicleToTmove)
						AND GET_OWNER_OF_PERSONAL_HACKER_TRUCK(thisSceneMoveEntityStruct.vehVehicleToTmove) != scriptData.deliveryData.deliveryOwner
							CDEBUG3LN(DEBUG_DELIVERY, "MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - Requesting personal vehicle be moved elsewhere")
							bVehEntityGrabbed = TRUE
						ELSE
							CDEBUG3LN(DEBUG_DELIVERY, "MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - owner of personal vehicle. ELSE, model: ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(thisSceneMoveEntityStruct.vehVehicleToTmove)))
							
							FREEMODE_DELIVERABLE_TYPE eType
							eType = FREEMODE_DELIVERY_GET_DELIVERABLE_TYPE(scriptData.deliveryData.eDeliverableID)
							
							IF IS_VEHICLE_A_PERSONAL_VEHICLE(thisSceneMoveEntityStruct.vehVehicleToTmove)
							OR IS_VEHICLE_A_PERSONAL_HACKER_TRUCK(thisSceneMoveEntityStruct.vehVehicleToTmove)
								IF IS_THIS_MODEL_ALLOWED_IN_HANGAR(GET_ENTITY_MODEL(thisSceneMoveEntityStruct.vehVehicleToTmove))
									CDEBUG3LN(DEBUG_DELIVERY, "MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - sitting in our own personal vehicle, and is allowed into the hangar")								
									SET_DELIVERY_FADE_AND_MOVE_ENTITIES_STATE(MES_DONE)
								ELSE
									CDEBUG3LN(DEBUG_DELIVERY, "MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - sitting in our own personal vehicle, personal vehicel but not allowed into the hangar")
									IF GET_DELIVERY_FLAG_EXTRA(scriptData.deliveryData, DELIVERY_FLAG_EXTRA_RETURN_PV_TO_PROPERTY)
									AND NOT GB_DELIVERY_SHOULD_REQUEST_PV_WARP()
										CDEBUG3LN(DEBUG_DELIVERY, "MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - Calling NETWORK_FADE_OUT_ENTITY and moving to warp player")
										NETWORK_FADE_OUT_ENTITY(thisSceneMoveEntityStruct.vehVehicleToTmove, TRUE, TRUE)
										SET_BIT(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_GLOBAL_DATA_DONT_BRING_VEHICLE_INSIDE)
										SET_DELIVERY_FADE_AND_MOVE_ENTITIES_STATE(MES_WARP_PLAYER)
									ELSE
										IF FREEMODE_DELIVERY_GET_DELIVERABLE_TYPE(scriptData.deliveryData.eDeliverableID) = FM_GANGOPS_DELIVERABLE_CRATE
										OR FREEMODE_DELIVERY_IS_TYPE_FMBB(eType)
											NETWORK_FADE_OUT_ENTITY(thisSceneMoveEntityStruct.vehVehicleToTmove, TRUE, TRUE)
											CDEBUG3LN(DEBUG_DELIVERY, "MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - Calling NETWORK_FADE_OUT_ENTITY for gang ops/FMBB entity")
										ELSE
											CDEBUG3LN(DEBUG_DELIVERY, "MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - Skipping NETWORK_FADE_OUT_ENTITY for non gang ops and non FMBB entity")
										ENDIF
										SET_DELIVERY_FADE_AND_MOVE_ENTITIES_STATE(MES_WARP_PV)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF bVehEntityGrabbed
					IF IS_THIS_VEHICLE_A_PEGASUS_VEHICLE(thisSceneMoveEntityStruct.vehVehicleToTmove)
						SET_DELIVERY_FADE_AND_MOVE_ENTITIES_STATE(MES_WARP_PEGASUS_VEH)
					ELIF IS_VEHICLE_A_PERSONAL_VEHICLE(thisSceneMoveEntityStruct.vehVehicleToTmove)
						SET_DELIVERY_FADE_AND_MOVE_ENTITIES_STATE(MES_WARP_PV)
					ELSE
						SET_DELIVERY_FADE_AND_MOVE_ENTITIES_STATE(MES_WARP_VEHICLE)
					ENDIF
				ENDIF
			ELSE
				SET_DELIVERY_FADE_AND_MOVE_ENTITIES_STATE(MES_WARP_PLAYER) 
			ENDIF
		BREAK
		
		CASE MES_WARP_VEHICLE
						
			IF IS_ENTITY_ALIVE(thisSceneMoveEntityStruct.vehVehicleToTmove)
				IF NOT NETWORK_IS_ENTITY_FADING(thisSceneMoveEntityStruct.vehVehicleToTmove)
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(VEH_TO_NET(thisSceneMoveEntityStruct.vehVehicleToTmove))
					
						VECTOR vSpawnLocation
						FLOAT fSpawnHeading
						VEHICLE_SPAWN_LOCATION_PARAMS vehicleSpawnLocationParams
						
						vehicleSpawnLocationParams.fMinDistFromCoords 		= 20
						vehicleSpawnLocationParams.fMaxDistance 			= 150
						vehicleSpawnLocationParams.bConsiderHighways 		= TRUE
						vehicleSpawnLocationParams.bCheckEntityArea 		= TRUE
						vehicleSpawnLocationParams.bCheckOwnVisibility 		= FALSE
						vehicleSpawnLocationParams.bConsiderOnlyActiveNodes = TRUE
						
						IF HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(GET_ENTITY_COORDS(thisSceneMoveEntityStruct.vehVehicleToTmove), <<0.0, 0.0, 0.0>>, thisSceneMoveEntityStruct.eVehModel, TRUE, vSpawnLocation, fSpawnHeading, vehicleSpawnLocationParams)									  
							
							NETWORK_FADE_IN_ENTITY(thisSceneMoveEntityStruct.vehVehicleToTmove, TRUE)
							SET_ENTITY_COORDS(thisSceneMoveEntityStruct.vehVehicleToTmove, vSpawnLocation)
							SET_ENTITY_HEADING(thisSceneMoveEntityStruct.vehVehicleToTmove, fSpawnHeading)
							SET_VEHICLE_ON_GROUND_PROPERLY(thisSceneMoveEntityStruct.vehVehicleToTmove)
							SET_VEHICLE_ENGINE_ON(thisSceneMoveEntityStruct.vehVehicleToTmove, FALSE, TRUE)
							
							IF IS_THIS_MODEL_A_HELI(thisSceneMoveEntityStruct.eVehModel)
								SET_HELI_BLADES_SPEED(thisSceneMoveEntityStruct.vehVehicleToTmove, 0.0)
							ENDIF
							
							CDEBUG3LN(DEBUG_DELIVERY, "MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - Moved ambient vehicle!")
							SET_DELIVERY_FADE_AND_MOVE_ENTITIES_STATE(MES_DONE)
							
						ENDIF
					ELSE
						CDEBUG3LN(DEBUG_DELIVERY, "MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - MES_WARP_VEHICLE - requesting control of amient vehicle")
						NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(VEH_TO_NET(thisSceneMoveEntityStruct.vehVehicleToTmove))					
					ENDIF
				ENDIF
			ELSE
				CDEBUG3LN(DEBUG_DELIVERY, "MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - MES_WARP_VEHICLE - vhicle no longer exists")
			ENDIF
		BREAK
		
		CASE MES_WARP_BOSS_VEH
			IF NOT GB_IS_GLOBAL_CLIENT_BIT1_SET(PLAYER_ID(), eGB_GLOBAL_CLIENT_BITSET_1_LOCAL_REQUEST_BOSS_LIMO_FADE_OUT)
				IF thisSceneMoveEntityStruct.piBossVehOwner != INVALID_PLAYER_INDEX()
					IF DOES_ENTITY_EXIST(MPGlobalsAmbience.sMagnateGangBossData.bossLimo[NATIVE_TO_INT(thisSceneMoveEntityStruct.piBossVehOwner)])
					AND NOT IS_ENTITY_DEAD(MPGlobalsAmbience.sMagnateGangBossData.bossLimo[NATIVE_TO_INT(thisSceneMoveEntityStruct.piBossVehOwner)])
						IF NOT NETWORK_IS_ENTITY_FADING(MPGlobalsAmbience.sMagnateGangBossData.bossLimo[NATIVE_TO_INT(thisSceneMoveEntityStruct.piBossVehOwner)])
							PRINTLN("[GUNRUNNING_CUTSCENE][MAGNATE_GANG_BOSS][BOSS_LIMO] GB_REQUEST_CREATE_GANG_BOSS_LIMO - Sending request event.")
							IF IS_NET_PLAYER_OK(GB_GET_LOCAL_PLAYER_GANG_BOSS(),FALSE)
								GB_REQUEST_CREATE_GANG_BOSS_LIMO(thisSceneMoveEntityStruct.eVehModel, DEFAULT, TRUE)
								PRINTLN("[GUNRUNNING_CUTSCENE][MAGNATE_GANG_BOSS][BOSS_LIMO] GB_REQUEST_CREATE_GANG_BOSS_LIMO - Sending event to boss - ",GET_PLAYER_NAME(thisSceneMoveEntityStruct.piBossVehOwner))
							ENDIF
							
							SET_DELIVERY_FADE_AND_MOVE_ENTITIES_STATE(MES_AWAIT_BOSS_VEH_FADE_IN)
						ENDIF
					ELSE
						PRINTLN("[GUNRUNNING_CUTSCENE][MAGNATE_GANG_BOSS][BOSS_LIMO] GB_REQUEST_CREATE_GANG_BOSS_LIMO - bosslimo does not exist.")
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE MES_AWAIT_BOSS_VEH_FADE_IN
			IF NOT HAS_NET_TIMER_STARTED(thisSceneMoveEntityStruct.stFadeInBossVehTimer)
				CDEBUG3LN(DEBUG_DELIVERY, "MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - Broadcasting fade in request!")
				BROADCAST_REQUEST_BOSS_LIMO_FADE_IN(thisSceneMoveEntityStruct.piBossVehOwner)
				START_NET_TIMER(thisSceneMoveEntityStruct.stFadeInBossVehTimer)
			ELIF HAS_NET_TIMER_EXPIRED(thisSceneMoveEntityStruct.stFadeInBossVehTimer, 3000)
				CDEBUG3LN(DEBUG_DELIVERY, "MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - Resending fade in event")
				RESET_NET_TIMER(thisSceneMoveEntityStruct.stFadeInBossVehTimer)
			ENDIF	
			
			IF DOES_ENTITY_EXIST(MPGlobalsAmbience.sMagnateGangBossData.bossLimo[NATIVE_TO_INT(thisSceneMoveEntityStruct.piBossVehOwner)])
			AND NOT IS_ENTITY_DEAD(MPGlobalsAmbience.sMagnateGangBossData.bossLimo[NATIVE_TO_INT(thisSceneMoveEntityStruct.piBossVehOwner)])
				IF NETWORK_IS_ENTITY_FADING(MPGlobalsAmbience.sMagnateGangBossData.bossLimo[NATIVE_TO_INT(thisSceneMoveEntityStruct.piBossVehOwner)])
					CDEBUG3LN(DEBUG_DELIVERY, "MAINTAIN_FADE_AND_MOVE_ON_DELIVERY vehicle is now fading in!")
					RESET_NET_TIMER(thisSceneMoveEntityStruct.stFadeInBossVehTimer)
					SET_DELIVERY_FADE_AND_MOVE_ENTITIES_STATE(MES_DONE)
				ENDIF
			ELSE
				CDEBUG3LN(DEBUG_DELIVERY, "MAINTAIN_FADE_AND_MOVE_ON_DELIVERY ,MES_AWAIT_BOSS_VEH_FADE_IN - dead: ", IS_ENTITY_DEAD(MPGlobalsAmbience.sMagnateGangBossData.bossLimo[NATIVE_TO_INT(thisSceneMoveEntityStruct.piBossVehOwner)]))
			ENDIF
		BREAK
		
		CASE MES_WARP_PLAYER
			IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
			AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				IF NOT NETWORK_IS_ENTITY_FADING(PLAYER_PED_ID())
					vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
					vPlayerPos.z -= 10.0					
					SET_ENTITY_COORDS_NO_OFFSET(PLAYER_PED_ID(), vPlayerPos)
					FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
					#IF IS_DEBUG_BUILD
					VECTOR vCoordsPlayers
					vCoordsPlayers = GET_ENTITY_COORDS(PLAYER_PED_ID())
					CDEBUG3LN(DEBUG_DELIVERY, "MAINTAIN_FADE_AND_MOVE_ON_DELIVERY, MES_WARP_PLAYER - moving palyer underground, Coords: ", vCoordsPlayers)
					#ENDIF
					SET_DELIVERY_FADE_AND_MOVE_ENTITIES_STATE(MES_DONE)
				ELSE
					CDEBUG3LN(DEBUG_DELIVERY, "MAINTAIN_FADE_AND_MOVE_ON_DELIVERY, MES_WARP_PLAYER - NOT NETWORK_IS_ENTITY_FADING")
				ENDIF
			ELSE
				CDEBUG3LN(DEBUG_DELIVERY, "MAINTAIN_FADE_AND_MOVE_ON_DELIVERY, MES_WARP_PLAYER - LAYER_PED_ID(), alive: ", IS_ENTITY_DEAD(PLAYER_PED_ID()))
			ENDIF
		BREAK
		
		CASE MES_WARP_PEGASUS_VEH
			MPGlobalsAmbience.iRequestFadeOutPegasusVeh = thisSceneMoveEntityStruct.iPegasusVehOwner
			SET_DELIVERY_FADE_AND_MOVE_ENTITIES_STATE(MES_DONE)
			CDEBUG3LN(DEBUG_DELIVERY, "MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - Requesting pegasus vehicle warp with player ID: ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(MPGlobalsAmbience.iRequestFadeOutPegasusVeh)))
		BREAK
		
		CASE MES_WARP_PV
			
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
				vPlayerPos.z -= 10.0
				SET_ENTITY_COORDS_NO_OFFSET(PLAYER_PED_ID(), vPlayerPos)
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
				
				CDEBUG2LN(DEBUG_DELIVERY, "MAINTAIN_FADE_AND_MOVE_ON_DELIVERY Moving player ped to ", vPlayerPos)
			ENDIF
				
			IF IS_VEHICLE_EMPTY(thisSceneMoveEntityStruct.vehVehicleToTmove)	
				BOOL bFadeIn
				VECTOR vAdjustedCoords
				
				bFadeIn = FREEMODE_DELIVERY_GET_DELIVERABLE_TYPE(scriptData.deliveryData.eDeliverableID) = FM_GANGOPS_DELIVERABLE_CRATE
				
				IF NOT bFadeIn
					bFadeIn = FREEMODE_DELIVERY_IS_TYPE_FMBB(FREEMODE_DELIVERY_GET_DELIVERABLE_TYPE(scriptData.deliveryData.eDeliverableID))
				ENDIF
				
				vAdjustedCoords = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
				vAdjustedCoords.z += 10.0
				
				IF IS_VEHICLE_A_PERSONAL_HACKER_TRUCK(thisSceneMoveEntityStruct.vehVehicleToTmove)
					BROADCAST_REQUEST_PLAYER_WARP_PV_NEAR_COORDS(vAdjustedCoords, GET_OWNER_OF_PERSONAL_HACKER_TRUCK(thisSceneMoveEntityStruct.vehVehicleToTmove), bFadeIn, TRUE)
				ELSE
					BROADCAST_REQUEST_PLAYER_WARP_PV_NEAR_COORDS(vAdjustedCoords, GET_OWNER_OF_PERSONAL_VEHICLE(thisSceneMoveEntityStruct.vehVehicleToTmove), bFadeIn)
				ENDIF
				CDEBUG3LN(DEBUG_DELIVERY, "MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - Requesting Move other players PV")
				SET_DELIVERY_FADE_AND_MOVE_ENTITIES_STATE(MES_DONE)
			ELSE
				CDEBUG2LN(DEBUG_DELIVERY, "MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - Waiting to do move PV request. PV not empty")
			ENDIF
		BREAK
		
		CASE MES_DONE
			IF NOT GET_CORE_BITSET(DELIVERY_FLAG_CORE_PLAYER_MOVE_COMPLETED)
				TOGGLE_CORE_BITSET(DELIVERY_FLAG_CORE_PLAYER_MOVE_COMPLETED, TRUE)
				
				IF NOT GET_DELIVERY_FLAG_EXTRA(scriptData.deliveryData, DELIVERY_FLAG_EXTRA_LET_CHILD_SCRIPT_UNCONCEAL_VEHICLE)
				AND IS_ENTITY_ALIVE(thisSceneMoveEntityStruct.vehVehicleToTmove)
				AND NETWORK_IS_ENTITY_CONCEALED(thisSceneMoveEntityStruct.vehVehicleToTmove)
					NETWORK_CONCEAL_ENTITY(thisSceneMoveEntityStruct.vehVehicleToTmove, FALSE)					
				ENDIF
			ENDIF
			//Do nothing
		BREAK
		
	ENDSWITCH
ENDPROC

PROC DS_MAINTAIN_SCENE_START_DELAY_TIMER()
	IF HAS_NET_TIMER_STARTED(scriptData.coreData.stSceneStartDelay)
		IF HAS_NET_TIMER_EXPIRED(scriptData.coreData.stSceneStartDelay, 1250)
			RESET_NET_TIMER(scriptData.coreData.stSceneStartDelay)
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_PREVENT_VISIBILITY_CHANGES)
			TOGGLE_CORE_BITSET(DELIVERY_FLAG_CORE_REMOVED_PLAYER_CONTROL, TRUE)
			CDEBUG3LN(DEBUG_DELIVERY, "MAINTAIN_FADE_AND_MOVE_ON_DELIVERY - control delay timer reached, disabling controls.")
			TOGGLE_CORE_BITSET(DELIVERY_FLAG_CORE_START_DELAY_DONE, TRUE)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Check if anyone on this script isntance started a request to start the delviery and ahs the package.
///    Allows vehicle deliveries to be triggered by the package owner.
FUNC BOOL DELIVERY_IS_DELIVERY_VIA_PACKAGE_OWNER()
	PARTICIPANT_INDEX participant
	INT i
	
	REPEAT NUM_NETWORK_PLAYERS i
		participant = INT_TO_PARTICIPANTINDEX(i)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(participant)
			IF GET_PARTICIPANT_BROADCAST_BITSET(PBBS_READY_TO_START_DELIVERY, i)
			AND GET_PARTICIPANT_BROADCAST_BITSET(PBBS_HOLDING_DELIVERY_PACKAGE, i)
				#IF IS_DEBUG_BUILD
				PLAYER_INDEX playerIndex
				playerIndex = NETWORK_GET_PLAYER_INDEX(participant)
				CDEBUG3LN(DEBUG_DELIVERY, "DELIVERY_IS_DELIVERY_VIA_PACKAGE_OWNER - ParticipantID: ", i, " PlayerName: ", GET_PLAYER_NAME(playerIndex))
				#ENDIF
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT

	RETURN FALSE
ENDFUNC


PROC TOGGLE_AUDIO_SCENE_CUTSCENE(BOOL bOn)
	CDEBUG3LN(DEBUG_DELIVERY, "TOGGLE_AUDIO_SCENE_CUTSCENE - request bOn: ", bOn)
	IF IS_STRING_NULL_OR_EMPTY(scriptData.deliveryData.cutsceneData.audioSceneCutscene)
		CDEBUG3LN(DEBUG_DELIVERY, "TOGGLE_AUDIO_SCENE_CUTSCENE - unable to manipulate audio scene, null or empty.")
		EXIT 
	ENDIF
	
	IF bOn
		IF NOT IS_AUDIO_SCENE_ACTIVE(scriptData.deliveryData.cutsceneData.audioSceneCutscene)
			CDEBUG3LN(DEBUG_DELIVERY, "TOGGLE_AUDIO_SCENE_CUTSCENE - starting audio scene.")
			START_AUDIO_SCENE(scriptData.deliveryData.cutsceneData.audioSceneCutscene)
		ELSE
			CDEBUG3LN(DEBUG_DELIVERY, "TOGGLE_AUDIO_SCENE_CUTSCENE - unable to start, audio scene is already running.")
		ENDIF
	ELSE
		IF IS_AUDIO_SCENE_ACTIVE(scriptData.deliveryData.cutsceneData.audioSceneCutscene)
			CDEBUG3LN(DEBUG_DELIVERY, "TOGGLE_AUDIO_SCENE_CUTSCENE - stopping audio scene.")
			STOP_AUDIO_SCENE(scriptData.deliveryData.cutsceneData.audioSceneCutscene)
		ELSE
			CDEBUG3LN(DEBUG_DELIVERY, "TOGGLE_AUDIO_SCENE_CUTSCENE - unable to stop, audio scene is already stopped.")
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Prevent black screen from supressing screen messages.
PROC DELIVERY_SET_NO_LOADING_SCREEN()
	IF GET_CORE_BITSET(DELIVERY_FLAG_CORE_WILL_PERFORM_INTERIOR_WARP)
		CDEBUG3LN(DEBUG_DELIVERY, "TOGGLE_NO_LOADING_SCREEN - Prevent black loading screen.")
		SET_NO_LOADING_SCREEN(TRUE)
		//Interior cleanup will set SET_NO_LOADING_SCREEN back to default, cleaning it up via this scrip will cleanup too early.
		SET_BIT(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_NO_LOADING_SCREEN_SET)
	ELSE
		CDEBUG3LN(DEBUG_DELIVERY, "TOGGLE_NO_LOADING_SCREEN - Will not be performing an interior warp, no need SET_NO_LOADING_SCREEN(TRUE).")
	ENDIF
ENDPROC

PROC MAINTAIN_LOCAL_PLAYER_REPORTING_WANTED_LEVEL()
	IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
		IF NOT GET_PARTICIPANT_BROADCAST_BITSET(PBBS_I_AM_WANTED, NATIVE_TO_INT(PARTICIPANT_ID()))
			TOGGLE_PARTICIPANT_BROADCAST_BITSET(PBBS_I_AM_WANTED, TRUE)
		ENDIF
	ELSE
		IF GET_PARTICIPANT_BROADCAST_BITSET(PBBS_I_AM_WANTED, NATIVE_TO_INT(PARTICIPANT_ID()))
			TOGGLE_PARTICIPANT_BROADCAST_BITSET(PBBS_I_AM_WANTED, FALSE)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_DELIVERY_CUTSCENE_TYPE_SCRIPTED(FREEMODE_DELIVERY_CUTSCENES eScene)
	UNUSED_PARAMETER(eScene)
		
	#IF FEATURE_TUNER
	IF eScene = FMC_CINEMATIC_CUTSCENE
		RETURN FALSE
	ENDIF
	#ENDIF
	
	RETURN TRUE
ENDFUNC

///
PROC SCRIPT_CLEANUP(STRING sTermianteReason)
	#IF NOT IS_DEBUG_BUILD
	UNUSED_PARAMETER(sTermianteReason)
	#ENDIF
	CDEBUG3LN(DEBUG_DELIVERY, "SCRIPT_CLEANUP - cleanup start, reason: ", sTermianteReason)
	
	TOGGLE_AUDIO_SCENE_CUTSCENE(FALSE)	
	
	IF IS_DELIVERY_CUTSCENE_TYPE_SCRIPTED(scriptData.deliveryData.eCutscene)
		SIMPLE_CUTSCENE_STOP(scriptData.deliveryData.cutsceneData.cutscene, TRUE)
	#IF FEATURE_TUNER
	ELSE
		CLEANUP_MP_CUTSCENE(TRUE, FALSE, FALSE, TRUE)
	#ENDIF
	ENDIF
	
	IF g_FreemodeDeliveryData.bDeliveryScriptTriggeredCutScene
		CLEAR_FOCUS()
		VEHICLE_SET_JET_WASH_FORCE_ENABLED(TRUE)
	ENDIF
	
	//Prevent resetting control if warp into interior was performed, interior script handles player control.
	IF NOT GET_CORE_BITSET(DELIVERY_FLAG_CORE_PERFORMING_INTERIOR_WARP)
	AND GET_CORE_BITSET(DELIVERY_FLAG_CORE_REMOVED_PLAYER_CONTROL)
	AND IS_SKYSWOOP_AT_GROUND()
		CDEBUG3LN(DEBUG_DELIVERY, "SCRIPT_CLEANUP - reseting player control.")
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE, NSPC_DONT_CLEAR_TASKS_ON_RESUME_CONTROL)
	ENDIF
	
	IF GET_CORE_BITSET(DELIVERY_FLAG_CORE_NIGHT_VISION_WAS_TURNED_OFF)
		CDEBUG3LN(DEBUG_DELIVERY, "SCRIPT_CLEANUP - restoring night vision.")
		DELIVERY_TOGGLE_NIGHT_VISION(TRUE)
	ENDIF
	
	//Destroy all created assets.
	DELIVERY_DELETE_ASSETS()
	//Assets
	UNLOAD_REQUESTED_ASSETS(scriptData.deliveryData.cutsceneData.assetRequester)
	CLEAR_ASSET_REQUESTER(scriptData.deliveryData.cutsceneData.assetRequester)
	//Custom cleanup from cutscene.
	IF GET_CORE_BITSET(DELIVERY_FLAG_CORE_POINTERS_INITIALIZED)
		CALL scriptData.functionPointers.coreFunctions.handlerCleanup(scriptData.deliveryData)
	ELSE
		CDEBUG3LN(DEBUG_DELIVERY, "SCRIPT_CLEANUP - function pointers not initialized yet, dont call cleanup.")
	ENDIF
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("GO_HELI_DELIV")
		CLEAR_HELP()
	ENDIF
	
	//Reset the critical passenger check
	g_FreemodeDeliveryData.bAllowDeliveryToBeBlockedByCriticalPassenger = TRUE
	
	TERMINATE_THIS_FREEMODE_DELIVERY_SCRIPT()
ENDPROC

PROC MAINTAIN_DELIVERABLE_CLEANUP(BOOL bForce = FALSE)
	#IF IS_DEBUG_BUILD
	IF NOT scriptData.debugData.bPlayCutscene
	#ENDIF
	IF NOT GET_CORE_BITSET(DELIVERY_FLAG_CORE_DELIVERY_CLEANUP_SENT)
	AND GET_PARTICIPANT_BROADCAST_BITSET(PBBS_CUTSCENE_STARTED, PARTICIPANT_ID_TO_INT())
		IF HAS_EVERYONE_STARTED_DELIVERY_CUTSCENE()
		OR bForce
			IF CALL scriptData.functionPointers.specificFunctions.shouldCleanupDeliverable(scriptData.deliveryData)
				CDEBUG1LN(DEBUG_DELIVERY, "DC - MAINTAIN_DELIVERABLE_CLEANUP - All Stared Cutscene: ", HAS_EVERYONE_STARTED_DELIVERY_CUTSCENE(), " Force: ", bForce)
				IF scriptData.deliveryData.eDeliverableID.iIndex != -1
					CDEBUG1LN(DEBUG_DELIVERY, "DC - MAINTAIN_DELIVERABLE_CLEANUP - BROADCAST_OK_TO_CLEANUP_DELIVERABLE_ENTITY - sent")
					BROADCAST_OK_TO_CLEANUP_DELIVERABLE_ENTITY(scriptData.deliveryData.eDeliverableID)
					TOGGLE_CORE_BITSET(DELIVERY_FLAG_CORE_DELIVERY_CLEANUP_SENT, TRUE)
				ELSE
					CDEBUG1LN(DEBUG_DELIVERY, "DC - MAINTAIN_DELIVERABLE_CLEANUP - FREEMODE_DELIVERY_IS_DELIVERABLE_VALID - NOT VALID")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	#IF IS_DEBUG_BUILD
	ENDIF
	#ENDIF
ENDPROC
