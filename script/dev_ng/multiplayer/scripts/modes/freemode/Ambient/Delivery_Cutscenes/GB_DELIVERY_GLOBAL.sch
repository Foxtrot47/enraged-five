USING "globals.sch"

FUNC BOOL TOGGLE_BIT_ENUM(INT &iBitSet, ENUM_TO_INT eBitIndex, BOOL bNewState)
	IF bNewState
		IF NOT IS_BIT_SET_ENUM(iBitSet, eBitIndex)
			SET_BIT_ENUM(iBitSet, eBitIndex)
			RETURN TRUE
		ENDIF
	ELSE
		IF IS_BIT_SET_ENUM(iBitSet, eBitIndex)
			CLEAR_BIT_ENUM(iBitSet, eBitIndex)
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC
