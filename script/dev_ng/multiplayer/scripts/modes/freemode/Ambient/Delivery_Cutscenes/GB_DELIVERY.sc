//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name: AM_DELIVERY.sc       																				//
// Description: Main script handling delivery cutscene.														//
// Written by:  Aleksej Leskin																				//
// Date:  		01/06/2017																					//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "net_include.sch"
USING "net_wait_zero.sch"

USING "GB_DELIVERY.sch"
USING "GB_DELIVERY_LOOKUP.sch"
USING "GB_delivery_definitions.sch"
USING "gb_delivery_global.sch"

USING "net_freemode_delivery_private.sch"
USING "net_freemode_delivery_helpers.sch"

PROC SCRIPT_INITIALISE(FREEMODE_DELIVERY_SCRIPT_LAUNCH_DATA &launchData)
	
	CDEBUG3LN(DEBUG_DELIVERY, "SCRIPT_INITIALISE - HANDLE_NET_SCRIPT_INITIALISATION - launchData.iInstance: ", launchData.iInstance)
	CDEBUG3LN(DEBUG_DELIVERY, "SCRIPT_INITIALISE - HANDLE_NET_SCRIPT_INITIALISATION - launchData.eDropOffId: ", FREEMODE_DELIVERY_GET_DROPOFF_DEBUG_NAME(launchData.eDropoffID), " (", ENUM_TO_INT(launchData.eDropoffID), ")")
	CDEBUG3LN(DEBUG_DELIVERY, "SCRIPT_INITIALISE - HANDLE_NET_SCRIPT_INITIALISATION - launchData.eCutscene: ", ENUM_TO_INT(launchData.eCutscene))
	IF launchData.pDeliverer <> INVALID_PLAYER_INDEX()
		CDEBUG3LN(DEBUG_DELIVERY, "SCRIPT_INITIALISE - HANDLE_NET_SCRIPT_INITIALISATION - Player deliverer: ", GET_PLAYER_NAME(launchData.pDeliverer))
	ELSE
		CDEBUG3LN(DEBUG_DELIVERY, "SCRIPT_INITIALISE - HANDLE_NET_SCRIPT_INITIALISATION - Player deliverer: - INVALID_PLAYER_INDEX")
	ENDIF
	IF launchData.pOwner <> INVALID_PLAYER_INDEX()
		CDEBUG3LN(DEBUG_DELIVERY, "SCRIPT_INITIALISE - HANDLE_NET_SCRIPT_INITIALISATION - pOwner: ", GET_PLAYER_NAME(launchData.pOwner))
	ELSE
		CDEBUG3LN(DEBUG_DELIVERY, "SCRIPT_INITIALISE - HANDLE_NET_SCRIPT_INITIALISATION - pOwner: - INVALID_PLAYER_INDEX")
	ENDIF
	CDEBUG3LN(DEBUG_DELIVERY, "SCRIPT_INITIALISE - HANDLE_NET_SCRIPT_INITIALISATION - deliverable ID index: ", launchData.deliverableID.iIndex)
	
	NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE, launchData.iInstance)
	
	IF NOT HANDLE_NET_SCRIPT_INITIALISATION(DEFAULT, DEFAULT, TRUE)
		SCRIPT_CLEANUP("Net Script Initialize failed")
	ENDIF

	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
	
	IF NOT Wait_For_First_Network_Broadcast()
		SCRIPT_CLEANUP("first network broadcast failed")
	ENDIF
	
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		SCRIPT_CLEANUP("network game is not in progress")
	ENDIF
	
	PRIVATE_SET_DELIVERY_STATE(DS_INITIALIZE)
	
	CDEBUG3LN(DEBUG_DELIVERY, "")
ENDPROC

//###################################### STATE PROCS ######################################

PROC PROC_DS_INITIALIZE(FREEMODE_DELIVERY_SCRIPT_LAUNCH_DATA &launchData)
	scriptData.deliveryData.deliveryOwner = launchData.pOwner
	scriptData.deliveryData.eDropoffID = launchData.eDropoffID
	scriptData.deliveryData.eDeliverableID = launchData.deliverableID
	scriptData.deliveryData.eCutscene = launchData.eCutscene
	scriptData.deliveryData.bDeliveringViaProxy = launchData.bDeliveryByProxy
	
	DETERMINE_DELIVERY_TYPE()
	
	//bits for generic setup are all enabled by default
	PRIVATE_FLIP_ALL_BITS()
	//.
	PRIVATE_DELIVERY_INITIALIZE()
	
	//Generic handlers for cutscne children
	PRIVATE_DELIVERY_LINK_DEFAULT_CORE_HANDLERS()
	//Generic handlers for script logic, can be overriden by children.
	PRIVATE_DELIVERY_LINK_DEFAULT_SPECIFIC_HANDLERS()
	
	//Override default core logic with delivery specific.
	IF NOT DELIVERY_LINK_DELIVERY_CORE_HANDLERS(launchData, scriptData)
		SCRIPT_CLEANUP("Linking handlers failed")
	ENDIF
		
	#IF IS_DEBUG_BUILD
	DELIVERY_FLAG_DEFAULT_PRINT_ALL(scriptData)
	DELIVERY_FLAG_EXTRA_PRINT_ALL(scriptData)
	#ENDIF
	
	IF scriptData.deliveryData.deliveryType = DT_VEHICLE
		IF IS_ENTITY_ALIVE(scriptData.deliveryData.deliveryVehicle)
			
			PED_INDEX pedDriver = GET_PED_IN_VEHICLE_SEAT(scriptData.deliveryData.deliveryVehicle, VS_DRIVER)
			
			IF NOT IS_ENTITY_ALIVE(pedDriver)
				SCRIPT_CLEANUP("No Driver")
				EXIT
			ENDIF
			
			scriptData.deliveryData.iPedsInVehBS = ALL_PLAYERS_IN_VEHICLE(scriptData.deliveryData.deliveryVehicle, TRUE, FALSE, TRUE)
			scriptData.deliveryData.driverWhoDelivered = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedDriver)
		ELSE
			SCRIPT_ASSERT("PROC_DS_INITIALIZE - DT = VEHICLE but the vehicle cannot be used!")
		ENDIF
	ELIF scriptData.deliveryData.deliveryType = DT_FOOT
		scriptData.deliveryData.driverWhoDelivered = PLAYER_ID()
	ENDIF
	
	TOGGLE_CORE_BITSET(DELIVERY_FLAG_CORE_POINTERS_INITIALIZED, TRUE)
	//Request assets/preload
	CALL scriptData.functionPointers.coreFunctions.handlerAssetsLoaded(scriptData.deliveryData)
	
	PRIVATE_SET_DELIVERY_STATE(DS_WAIT_FOR_START)
ENDPROC

PROC SET_VEHICLE_ENTRY_DATA()
	IF SMUGGLER_DROPOFF_IS_PLAYER_HANGAR(scriptData.deliveryData.eDropoffID)
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			VEHICLE_INDEX vVehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			
			IF IS_VEHICLE_A_PERSONAL_VEHICLE(vVehicle)
			AND IS_THIS_MODEL_ALLOWED_IN_HANGAR(GET_ENTITY_MODEL(vVehicle))
			AND GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
			AND GET_OWNER_OF_PERSONAL_VEHICLE(vVehicle) = scriptData.deliveryData.deliveryOwner
			AND IS_NET_PLAYER_OK(scriptData.deliveryData.deliveryOwner)
				INT iTempDisplaySlot
				MPSV_GET_DISPLAY_SLOT_FROM_SAVE_SLOT(CURRENT_SAVED_VEHICLE_SLOT(), iTempDisplaySlot)
				
				IF IS_HANGAR_DISPLAY_SLOT_IN_STORAGE(iTempDisplaySlot)
					SET_BIT(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].freemodeDelivery.iBS, BS_FREEMODE_DELIVERY_GLOBAL_PLAYER_BD_ENTERING_VEHICLE_STORAGE)
				ELSE
					IF NOT IS_HANGAR_DISPLAY_SLOT_IN_STORAGE(iTempDisplaySlot)
						SIMPLE_INTERIORS eInteriorID = GET_SIMPLE_INTERIOR_FROM_SMUGGLER_DROPOFF(scriptData.deliveryData.eDropoffID)
						SIMPLE_INTERIOR_SET_ENTERING_WITH_VEHICLE(eInteriorID)
						PRINTLN("SET_VEHICLE_ENTRY_DATA SIMPLE_INTERIOR_SET_ENTERING_WITH_VEHICLE(", eInteriorID, ") - 1")
						SIMPLE_INTERIOR_CLEAR_ENTERING_WITH_VEH_AS_PASSENGER()
						SIMPLE_INTERIOR_SET_ENTRY_VEH_SEAT()
					ENDIF
				ENDIF
				
				IF IS_VEHICLE_A_PERSONAL_VEHICLE(vVehicle)
					g_bEnteredInPV = TRUE
					g_bEnteredInPegasus = FALSE
				ELIF IS_THIS_VEHICLE_A_PEGASUS_VEHICLE(vVehicle)
					g_bEnteredInPV = FALSE
					g_bEnteredInPegasus = TRUE
				ELSE
					g_bEnteredInPV = FALSE
					g_bEnteredInPegasus = FALSE
				ENDIF
				
				g_iVehicleEntered = CURRENT_SAVED_VEHICLE_SLOT()
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROC_DS_WAIT_FOR_START()
	
	g_FreemodeDeliveryData.bDeliveryScriptTriggeredSceneLoad = FALSE
	
	IF GET_DELIVERY_FLAG_EXTRA(scriptData.deliveryData, DELIVERY_FLAG_EXTRA_ENABLE_AGGRESSIVE_BRING_VEH_TO_HALT)
		IF DOES_ENTITY_EXIST(scriptData.deliveryData.deliveryVehicle)
		AND NOT IS_ENTITY_DEAD(scriptData.deliveryData.deliveryVehicle)
			IF GET_ENTITY_SPEED(scriptData.deliveryData.deliveryVehicle) > 15.0
				PRIVATE_DELIVERY_DEFAULT_MAINTAIN_VEHICLE_HALT_ON_DELIVERY_VEHICLE(scriptData.deliveryData)
			ELIF IS_VEHICLE_BEING_BROUGHT_TO_HALT(scriptData.deliveryData.deliveryVehicle)
			AND NETWORK_HAS_CONTROL_OF_ENTITY(scriptData.deliveryData.deliveryVehicle)
				STOP_BRINGING_VEHICLE_TO_HALT(scriptData.deliveryData.deliveryVehicle)
			ENDIF
		ENDIF
	ENDIF
	
	//Locate
	IF NOT HAS_NET_TIMER_STARTED(scriptData.coreData.stLocateCheck)
		START_NET_TIMER(scriptData.coreData.stLocateCheck)
		CDEBUG3LN(DEBUG_DELIVERY, "scriptData.coreData.stLocateCheck - started")
	ENDIF
	
	IF HAS_NET_TIMER_EXPIRED(scriptData.coreData.stLocateCheck, CONST_DELIVERY_LOCATE_CHECK_INTERVAL)
		REINIT_NET_TIMER(scriptData.coreData.stLocateCheck)
		CALL scriptData.functionPointers.specificFunctions.performLocateCheck(scriptData.deliveryData)
	ENDIF
			
	IF scriptData.deliveryData.deliveryOwner != INVALID_PLAYER_INDEX()
	AND GlobalplayerBD[NATIVE_TO_INT(scriptData.deliveryData.deliveryOwner)].bJustKilledSelf
		CDEBUG1LN(DEBUG_DELIVERY, "DS_WAIT_FOR_START - delivery cutscene should not start delivery owner has killed themselves.")	
	//By default will start, and bypass locate check
	ELIF CALL scriptData.functionPointers.specificFunctions.shouldDeliveryCutsceneStart(scriptData.deliveryData)
	OR DELIVERY_IS_DELIVERY_VIA_PACKAGE_OWNER()
	#IF IS_DEBUG_BUILD
	OR scriptData.debugData.bPlayCutscene
	#ENDIF
	
		CDEBUG3LN(DEBUG_DELIVERY, "DS_WAIT_FOR_START - shouldDeliveryCutsceneStart: ", CALL scriptData.functionPointers.specificFunctions.shouldDeliveryCutsceneStart(scriptData.deliveryData), " ViaPackage: ", DELIVERY_IS_DELIVERY_VIA_PACKAGE_OWNER())	
		TOGGLE_PARTICIPANT_BROADCAST_BITSET(PBBS_READY_TO_START_DELIVERY, TRUE)	
		TOGGLE_PARTICIPANT_BROADCAST_BITSET(PBBS_SUPRESSING_PLAYER_FADE_OUT, TRUE)
		TOGGLE_CORE_BITSET(DELIVERY_FLAG_CORE_SUPRESS_PLAYER_FADE_OUT, TRUE)

		//Bring to halt by default
		CALL scriptData.functionPointers.specificFunctions.performActionOnDeliveryVehicle(scriptData.deliveryData)
		
		IF GET_DELIVERY_FLAG_DEFAULT(scriptData.deliveryData, DELIVERY_FLAG_DEFAULT_DO_CUTSCENE_START_DELAY)
		AND scriptData.deliveryData.deliveryType != DT_FOOT
			CDEBUG3LN(DEBUG_DELIVERY, "DS_WAIT_FOR_START - delivery started stSceneStartDelay.")
			RESET_NET_TIMER(scriptData.coreData.stSceneStartDelay)
			START_NET_TIMER(scriptData.coreData.stSceneStartDelay)
		ELSE
			CDEBUG3LN(DEBUG_DELIVERY, "DS_WAIT_FOR_START - delivery not starting stSceneStartDelay.")
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_PREVENT_VISIBILITY_CHANGES)
			TOGGLE_CORE_BITSET(DELIVERY_FLAG_CORE_START_DELAY_DONE, TRUE)
			TOGGLE_CORE_BITSET(DELIVERY_FLAG_CORE_REMOVED_PLAYER_CONTROL, TRUE)
		ENDIF
		
		IF scriptData.deliveryData.deliveryOwner = PLAYER_ID()
			SET_VEHICLE_ENTRY_DATA()
		ENDIF
		
		g_FreemodeDeliveryData.bDeliveryScriptTriggeredSceneLoad = TRUE
		IF NOT GET_DELIVERY_FLAG_EXTRA(scriptData.deliveryData, DELIVERY_FLAG_EXTRA_ALLOW_CALL_DURING_SCENE)
			HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
		ENDIF
		PRIVATE_SET_DELIVERY_STATE(DS_LOAD_ASSETS)
	#IF IS_DEBUG_BUILD
	ELSE
		IF GET_FRAME_COUNT() % 150 = 0
			CDEBUG3LN(DEBUG_DELIVERY, "DS_WAIT_FOR_START - delivery cutscene should not start yet.")	
		ENDIF
	#ENDIF
	ENDIF
ENDPROC

PROC PROC_DS_LOAD_ASSETS()

	//Add wait for all players to start cutscene at the same time?
	PRIVATE_DELIVERY_DEFAULT_MAINTAIN_VEHICLE_HALT_ON_DELIVERY_VEHICLE(ScriptData.deliveryData)
	
	IF NOT HAS_NET_TIMER_STARTED(scriptData.coreData.stState)
		CDEBUG3LN(DEBUG_DELIVERY, "PROC_DS_LOAD_ASSETS - stState started, streaming failsafe timer started, waiting for: ", CONST_TIME_MS_STREAMING_FAILSAFE, " ms.")	
		START_NET_TIMER(scriptData.coreData.stState)
	ENDIF
	
	BOOL bStreamingExpired
	bStreamingExpired = HAS_NET_TIMER_EXPIRED_READ_ONLY(scriptData.coreData.stState, CONST_TIME_MS_STREAMING_FAILSAFE)
	
	IF (CALL scriptData.functionPointers.coreFunctions.handlerAssetsLoaded(scriptData.deliveryData))
	AND DELIVERY_DEFAULT_ASSTS_LOADED(scriptData.deliveryData)
		RESET_NET_TIMER(scriptData.coreData.stState)
		PRIVATE_SET_DELIVERY_STATE(DS_PREPARE_CUTSCENE)
	
	ELIF bStreamingExpired
		
		CDEBUG1LN(DEBUG_DELIVERY, "PROC_DS_LOAD_ASSETS - Load assets timer expired: ", CONST_TIME_MS_STREAMING_FAILSAFE, " ms.")	
		PRIVATE_SET_DELIVERY_STATE(DS_AFTER_DELIVERY)
		FREEMODE_DELIVERY_START_DELIVERY(g_FreemodeDeliveryData.eDropoffForDeliveryScript, ScriptData.deliveryData.deliveryOwner, FALSE, TRUE, FALSE, IS_PLAYER_HOLDING_MULTIPLE_DELIVERABLES(PLAYER_ID()))	
	
	#IF IS_DEBUG_BUILD
	ELSE
		IF GET_FRAME_COUNT() % 30 = 0
			CDEBUG3LN(DEBUG_DELIVERY, "DS_LOAD_ASSETS - handlerAssetsLoaded: ", CALL scriptData.functionPointers.coreFunctions.handlerAssetsLoaded(scriptData.deliveryData)
			," DELIVERY_DEFAULT_ASSTS_LOADED(scriptData.deliveryData): ", DELIVERY_DEFAULT_ASSTS_LOADED(scriptData.deliveryData))
		ENDIF
	#ENDIF
	
	ENDIF
ENDPROC

PROC PROC_DS_PREPARE_CUTSCENE()
	
	//Add wait for all players to start cutscene at the same time?
	PRIVATE_DELIVERY_DEFAULT_MAINTAIN_VEHICLE_HALT_ON_DELIVERY_VEHICLE(ScriptData.deliveryData)
	
	IF NOT HAS_NET_TIMER_STARTED(scriptData.coreData.stState)
		CDEBUG3LN(DEBUG_DELIVERY, "PROC_DS_PREPARE_CUTSCENE - stState started, streaming failsafe timer started, waiting for: ", CONST_TIME_MS_STREAMING_FAILSAFE, " ms.")	
		START_NET_TIMER(scriptData.coreData.stState)
	ENDIF
	
	IF HAS_NET_TIMER_EXPIRED_READ_ONLY(scriptData.coreData.stState, CONST_TIME_MS_STREAMING_FAILSAFE)
		
		CDEBUG1LN(DEBUG_DELIVERY, "PROC_DS_PREPARE_CUTSCENE - Load assets timer expired: ", CONST_TIME_MS_STREAMING_FAILSAFE, " ms.")	
		PRIVATE_SET_DELIVERY_STATE(DS_AFTER_DELIVERY)
		FREEMODE_DELIVERY_START_DELIVERY(g_FreemodeDeliveryData.eDropoffForDeliveryScript, ScriptData.deliveryData.deliveryOwner, TRUE, FALSE, IS_PLAYER_HOLDING_MULTIPLE_DELIVERABLES(PLAYER_ID()))	
	ENDIF
	
	IF GET_DELIVERY_FLAG_DEFAULT(scriptData.deliveryData, DELIVERY_FLAG_DEFAULT_CREATE_CLONE)
		IF NOT GET_CORE_BITSET(DELIVERY_FLAG_CORE_DEFAULT_CLONES_CREATED)
			IF DELIVERY_CREATE_CLONES()
				CDEBUG3LN(DEBUG_DELIVERY, "DS_CREATE_ASSETS - clones created, peds put inside.")	
				TOGGLE_CORE_BITSET(DELIVERY_FLAG_CORE_DEFAULT_CLONES_CREATED, TRUE)
			ELSE
				CDEBUG3LN(DEBUG_DELIVERY, "DS_CREATE_ASSETS - vehicle clone frame skipped for - url:bugstar:3857012.")	
				//Skip a frame //url:bugstar:3857012 - Hangar Property - Remote player's free cuban 800 is appearing damaged to the local player whilst in the hangar
				EXIT
			ENDIF
		ELSE
			CDEBUG3LN(DEBUG_DELIVERY, "DS_CREATE_ASSETS - DELIVERY_FLAG_CORE_DEFAULT_CLONES_CREATED is set, assets already created.")	
		ENDIF
	ELSE
		CDEBUG3LN(DEBUG_DELIVERY, "DS_CREATE_ASSETS - DELIVERY_FLAG_DEFAULT_DO_NOT_CREATE_CLONE is set, skipping default asset creation.")	
	ENDIF
	
	//Create additional assets
	IF (CALL scriptData.functionPointers.coreFunctions.handlerAssetsCreate(scriptData.deliveryData))
		CDEBUG3LN(DEBUG_DELIVERY, "DS_CREATE_ASSETS - all assets have been created! continue.")	
		
		//Add scenes 
		CALL scriptData.functionPointers.coreFunctions.handlerCreateScene(scriptData.deliveryData)
		RESET_NET_TIMER(scriptData.coreData.stState)
		PRIVATE_SET_DELIVERY_STATE(DS_STREAM_ASSETS)
	#IF IS_DEBUG_BUILD
	ELSE
		IF GET_FRAME_COUNT() % 30 = 0
			CDEBUG3LN(DEBUG_DELIVERY, "DS_CREATE_ASSETS - not all assets have been created.")	
		ENDIF
	#ENDIF
	ENDIF

	//Called per frame in case there is need to frame skip/opening insurget turet door example.
	DELIVERY_TOGGLE_ASSETS_READY(FALSE)
ENDPROC

PROC PROC_DS_STREAM_ASSETS()
	
	//Add wait for all players to start cutscene at the same time?
	PRIVATE_DELIVERY_DEFAULT_MAINTAIN_VEHICLE_HALT_ON_DELIVERY_VEHICLE(ScriptData.deliveryData)
	
	IF NOT HAS_NET_TIMER_STARTED(scriptData.coreData.stState)
		CDEBUG3LN(DEBUG_DELIVERY, "PROC_DS_STREAM_ASSETS - stState started, streaming failsafe timer started, waiting for: ", CONST_TIME_MS_STREAMING_FAILSAFE, " ms.")	
		START_NET_TIMER(scriptData.coreData.stState)
	ENDIF
	
	BOOL bStreamingExpired
	bStreamingExpired = HAS_NET_TIMER_EXPIRED_READ_ONLY(scriptData.coreData.stState, CONST_TIME_MS_STREAMING_FAILSAFE)	
	
	IF DELIVERY_HAS_STREAM_ASSETS_FINISHED()
	OR bStreamingExpired
		IF bStreamingExpired
			ASSERTLN(" DS_LOAD_ASSETS - not all assets loaded, timer expired after CONST_TIME_MS_CUTSCENE_START: ", CONST_TIME_MS_STREAMING_FAILSAFE)
		ELSE
			CDEBUG3LN(DEBUG_DELIVERY, "DS_STREAM_ASSETS - all assets have streamed in! continue.")	
		ENDIF
		
		IF IS_DELIVERY_CUTSCENE_TYPE_SCRIPTED(scriptData.deliveryData.eCutscene)
			TOGGLE_PARTICIPANT_BROADCAST_BITSET(PBBS_READY_TO_START_CUTSCENE, TRUE)
			PRIVATE_SET_DELIVERY_STATE(DS_WAIT_FOR_PLAYERS)
		#IF FEATURE_TUNER
		ELSE
			PRIVATE_SET_DELIVERY_STATE(DS_STREAM_CINEMATIC_SCENE)
		#ENDIF
		ENDIF
	#IF IS_DEBUG_BUILD
	ELSE
		IF GET_FRAME_COUNT() % 30 = 0
			CDEBUG3LN(DEBUG_DELIVERY, "DS_STREAM_ASSETS - not all assets have streamed in, streamedIn: ", DELIVERY_HAS_STREAM_ASSETS_FINISHED(),
			" Expired: ", bStreamingExpired)		
		ENDIF
	#ENDIF
	ENDIF
ENDPROC

PROC PROC_DS_STREAM_CINEMATIC_SCENE()
	
	IF NOT HAS_NET_TIMER_STARTED(scriptData.coreData.stState)
		CDEBUG3LN(DEBUG_DELIVERY, "PROC_DS_STREAM_ASSETS - stState started, streaming failsafe timer started, waiting for: ", CONST_TIME_MS_STREAMING_FAILSAFE, " ms.")	
		START_NET_TIMER(scriptData.coreData.stState)
	ENDIF
	
	BOOL bStreamingExpired
	bStreamingExpired = HAS_NET_TIMER_EXPIRED_READ_ONLY(scriptData.coreData.stState, CONST_TIME_MS_STREAMING_FAILSAFE)	
	
	IF CALL scriptData.functionPointers.coreFunctions.handlerCinematicStream(ScriptData.deliveryData #IF IS_DEBUG_BUILD , ScriptData.debugData #ENDIF)
	OR bStreamingExpired
	
		IF bStreamingExpired
			ASSERTLN(" DS_LOAD_ASSETS - not all assets loaded, timer expired after CONST_TIME_MS_CUTSCENE_START: ", CONST_TIME_MS_STREAMING_FAILSAFE)
		ELSE
			CDEBUG3LN(DEBUG_DELIVERY, "PROC_DS_STREAM_CINEMATIC_SCENE - all assets have streamed in! continue.")	
		ENDIF
		
		TOGGLE_PARTICIPANT_BROADCAST_BITSET(PBBS_READY_TO_START_CUTSCENE, TRUE)
		PRIVATE_SET_DELIVERY_STATE(DS_WAIT_FOR_PLAYERS)
	ENDIF
	
ENDPROC

PROC COMMON_DELIVERY_SCENE_START_CALLS()
	VEHICLE_SET_JET_WASH_FORCE_ENABLED(FALSE)
	g_FreemodeDeliveryData.bDeliveryScriptTriggeredCutScene = TRUE
	GB_SET_GLOBAL_CLIENT_BIT1(eGB_GLOBAL_CLIENT_BITSET_1_DOING_DELIVERY_CUTSCENE)
	TOGGLE_PARTICIPANT_BROADCAST_BITSET(PBBS_CUTSCENE_STARTED, TRUE)
	DELIVERY_SET_NO_LOADING_SCREEN()
	PRIVATE_SET_DELIVERY_STATE(DS_MAINTAIN_DELIVERY)
ENDPROC

PROC PRIVATE_START_CINEMATIC_CUTSCENE()
	
	CALL scriptData.functionPointers.specificFunctions.registerSceneEntities(scriptData.deliveryData)
	
	START_CUTSCENE()
	
	IF NOT IS_VECTOR_ZERO(scriptData.deliveryData.vCinematicSceneCoords)
		SET_CUTSCENE_ORIGIN(scriptData.deliveryData.vCinematicSceneCoords, scriptData.deliveryData.fCinematicSceneHeading, -1)
	ENDIF
	
	COMMON_DELIVERY_SCENE_START_CALLS()
	
ENDPROC

PROC PRIVATE_START_SCRIPTED_CUTSCENE()
	SIMPLE_CUTSCENE_SETUP eSceneFlags
	
	IF GET_DELIVERY_FLAG_EXTRA(scriptData.deliveryData, DELIVERY_FLAG_EXTRA_ALLOW_CALL_DURING_SCENE)
		eSceneFlags = SCS_DO_NOT_RETURN_PLAYER_CONTROL | SCS_HIDE_PROJECTILES | SCS_ALLOW_CALLS_OVER_SCENE
	ELSE
		eSceneFlags = SCS_DO_NOT_RETURN_PLAYER_CONTROL | SCS_HIDE_PROJECTILES
	ENDIF
		
	SIMPLE_CUTSCENE_START(scriptData.deliveryData.cutsceneData.cutscene, eSceneFlags)
	
	COMMON_DELIVERY_SCENE_START_CALLS()
	
ENDPROC

PROC PRIVATE_START_CUTSCENE()
	IF IS_DELIVERY_CUTSCENE_TYPE_SCRIPTED(scriptData.deliveryData.eCutscene)
		PRIVATE_START_SCRIPTED_CUTSCENE()
	#IF FEATURE_TUNER
	ELSE
		PRIVATE_START_CINEMATIC_CUTSCENE()
	#ENDIF
	ENDIF
ENDPROC

PROC PROC_DS_WAIT_FOR_PLAYERS()
	//Add wait for all players to start cutscene at the same time?
	PRIVATE_DELIVERY_DEFAULT_MAINTAIN_VEHICLE_HALT_ON_DELIVERY_VEHICLE(ScriptData.deliveryData, TRUE)
	
	IF GET_CORE_BITSET(DELIVERY_FLAG_CORE_START_DELAY_DONE)
		IF GET_DELIVERY_FLAG_DEFAULT(scriptData.deliveryData, DELIVERY_FLAG_DEFAULT_WAIT_FOR_PLAYERS_FOR_CUTSCENE)
			IF PRIVATE_DELIVERY_EVERYONE_READY_FOR_CUTSCENE(ScriptData.deliveryData)
				PRIVATE_START_CUTSCENE()
			#IF IS_DEBUG_BUILD
			ELSE
				IF GET_FRAME_COUNT() % 30 = 0
					CDEBUG3LN(DEBUG_DELIVERY, "PROC_DS_WAIT_FOR_PLAYERS - not all players are ready.")	
				ENDIF
			#ENDIF
			ENDIF
		ELSE
			PRIVATE_START_CUTSCENE()
		ENDIF
	#IF IS_DEBUG_BUILD
	ELSE
		IF GET_FRAME_COUNT() % 30 = 0
			CDEBUG3LN(DEBUG_DELIVERY, "PROC_DS_WAIT_FOR_PLAYERS - wait on DELIVERY_FLAG_CORE_START_DELAY_DONE.")	
		ENDIF
	#ENDIF
	ENDIF
ENDPROC

PROC DISABLE_COLLISIONS_OF_CLONE_VEHICLE()
	IF NOT IS_ENTITY_ALIVE(scriptData.deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle)
		EXIT
	ENDIF
	
	CDEBUG3LN(DEBUG_DELIVERY, "PROC_DS_WAIT_FOR_PLAYERS - Call DISABLE_COLLISIONS_OF_CLONE_VEHICLE.")
	
	VEHICLE_INDEX veh = scriptData.deliveryData.cutsceneData.vehiclePackArray[FMC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle
	
	//Disable collision for the vehicle
	FREEZE_ENTITY_POSITION(veh, TRUE)
	SET_ENTITY_COLLISION(veh, FALSE)
ENDPROC

PROC PROC_DS_MAINTAIN_SCRIPTED_DELIVERY_SCENE()
	
	IF NOT IS_SKYSWOOP_AT_GROUND()
		SCRIPT_CLEANUP("Sky swoop not at ground")
	ENDIF
	
	SET_BITMASK_AS_ENUM(scriptData.deliveryData.cutsceneData.cutscene.iSetupBM, SCS_PREVENT_VISIBLITY_CHANGES_START)
	SET_BITMASK_AS_ENUM(scriptData.deliveryData.cutsceneData.cutscene.iSetupBM, SCS_LEAVE_PLAYER_REMOTELY_VISIBLE)
	SIMPLE_CUTSCENE_MAINTAIN(scriptData.deliveryData.cutsceneData.cutscene)

	//Single call, before main update.
	IF scriptData.deliveryData.cutsceneData.cutscene.bPlaying
		//On first frame of the cutscene, call once all these
		IF NOT GET_CORE_BITSET(DELIVERY_FLAG_CORE_FIRST_CUTSCENE_FRAME_INITIALIZE)
						
			DELIVERY_TOGGLE_ASSETS_READY(TRUE)
			SET_FOCUS_POS_AND_VEL(scriptData.deliveryData.focusCoord, <<0.0, 0.0, 0.0>>)
			CLEAR_AREA_OF_OBJECTS(scriptData.deliveryData.clearAreaCoord, scriptData.deliveryData.clearAreaRange)
			TOGGLE_AUDIO_SCENE_CUTSCENE(TRUE)
			#IF IS_DEBUG_BUILD
			IF NOT scriptData.debugData.bPlayCutscene
			#ENDIF
			FREEMODE_DELIVERY_START_DELIVERY(g_FreemodeDeliveryData.eDropoffForDeliveryScript, scriptData.deliveryData.deliveryOwner, FALSE, FALSE, FALSE, IS_PLAYER_HOLDING_MULTIPLE_DELIVERABLES(PLAYER_ID()))	
			#IF IS_DEBUG_BUILD
			ENDIF
			#ENDIF
			
			IF IS_HELP_MESSAGE_BEING_DISPLAYED()
				CDEBUG3LN(DEBUG_DELIVERY, "DS_MAINTAIN_DELIVERY - CLEAR_HELP")
				CLEAR_HELP(TRUE)	
			ENDIF
			
			TOGGLE_CORE_BITSET(DELIVERY_FLAG_CORE_FIRST_CUTSCENE_FRAME_INITIALIZE, TRUE)
		ENDIF

		//Scene prepare event, called once a scene switched.(overrides assets ready where needed, only positions everything when needed)
		IF scriptData.deliveryData.cutsceneData.cutscene.iCurrentScene <> scriptData.coreData.iPreviousScene
			IF SIMPLE_CUTSCENE_IS_SCENE_RUNNING(scriptData.deliveryData.cutsceneData.cutscene, scriptData.deliveryData.cutsceneData.cutscene.iCurrentScene)
				CDEBUG3LN(DEBUG_DELIVERY, "DS_MAINTAIN_DELIVERY - handlerSceneSetup called, scene: ", scriptData.deliveryData.cutsceneData.cutscene.iCurrentScene)	
				CALL scriptData.functionPointers.coreFunctions.handlerSceneSetup(scriptData.deliveryData, scriptData.deliveryData.cutsceneData.cutscene.iCurrentScene)
				scriptData.coreData.iPreviousScene = scriptData.deliveryData.cutsceneData.cutscene.iCurrentScene
			ENDIF
		ENDIF
				
		//start showing delivery message on screen.
		IF NOT g_FreemodeDeliveryData.bDeliveryScriptAllowDeliveryShard
			IF CALL scriptData.functionPointers.specificFunctions.shouldStartDeliveryMessageDisplay(scriptData.deliveryData)
				CDEBUG3LN(DEBUG_DELIVERY, "DS_MAINTAIN_DELIVERY - g_FreemodeDeliveryData.bDeliveryScriptAllowDeliveryShard.")
				g_FreemodeDeliveryData.bDeliveryScriptAllowDeliveryShard = TRUE
			ENDIF
		ENDIF
	ENDIF

	//Main Cutscene update
	CALL scriptData.functionPointers.coreFunctions.handlerMaintainScene(scriptData.deliveryData)
	
	//Per frame update
	IF scriptData.deliveryData.cutsceneData.cutscene.bPlaying				
		IF GET_DELIVERY_FLAG_DEFAULT(scriptData.deliveryData, DELIVERY_FLAG_DEFAULT_REMOVE_PARTICLE_FX_IN_RANGE)
			REMOVE_PARTICLE_FX_IN_RANGE(scriptData.deliveryData.vfxRemoveCoord, scriptData.deliveryData.vfxRemoveRange)
		ENDIF
				
		IF GET_DELIVERY_FLAG_DEFAULT(scriptData.deliveryData, DELIVERY_FLAG_DEFAULT_DISABLE_NIGHT_VISION)
			DELIVERY_MAINTAIN_NIGHT_VISION()
		ENDIF
		
		//Force peds to get out of vehicles properly if networked vehicles are blocking doors.
		CUTSCE_HELP_MAINTAIN_PED_ARRAY_FREEMODE_CUTSCENE(scriptData.deliveryData.cutsceneData.pedArray)
		INT index
		REPEAT CONST_DELIVERY_MAX_VEHICLES_PACK index
			CUTSCE_HELP_MAINTAIN_PED_ARRAY_FREEMODE_CUTSCENE(scriptData.deliveryData.cutsceneData.vehiclePackArray[index].passengers)
		ENDREPEAT
	ENDIF
	
	//Should transition to next stage?
	BOOL bTransition
	bTransition = FALSE
	
	IF GET_DELIVERY_FLAG_DEFAULT(scriptData.deliveryData, DELIVERY_FLAG_DEFAULT_SCREEN_FADE_AFTER_DELIVERY)
		IF NOT GET_CORE_BITSET(DELIVERY_FLAG_CORE_START_CUTSCENE_FADE_OUT)
			IF CALL scriptData.functionPointers.specificFunctions.shouldCutsceneStartFadeOut(scriptData.deliveryData)
				//fade out failsafe just in case something breaks the fades/externally.
				IF NOT HAS_NET_TIMER_STARTED(scriptData.coreData.stState)
					CDEBUG3LN(DEBUG_DELIVERY, "DS_MAINTAIN_DELIVERY - stState started, fadeout failsafe timer started, waiting for: ", CONST_TIME_MS_CUTSCENE_START, " ms.")	
					START_NET_TIMER(scriptData.coreData.stState)
				ENDIF
				CDEBUG3LN(DEBUG_DELIVERY, "DS_MAINTAIN_DELIVERY - DELIVERY_CHECK_CUTSCENE_FADE_OUT_START - TRUE")	
				TOGGLE_CORE_BITSET(DELIVERY_FLAG_CORE_START_CUTSCENE_FADE_OUT, TRUE)
				DISABLE_COLLISIONS_OF_CLONE_VEHICLE()
			ENDIF
		ELSE

			IF (NOT IS_SCREEN_FADING_OUT() AND IS_SCREEN_FADED_OUT())
				CDEBUG3LN(DEBUG_DELIVERY, "DS_MAINTAIN_DELIVERY - Screen is faded out, transition")
				//Stop timer
				IF HAS_NET_TIMER_STARTED(scriptData.coreData.stState)
					RESET_NET_TIMER(scriptData.coreData.stState)
				ENDIF
				bTransition = TRUE
			ENDIF
			
			IF HAS_NET_TIMER_STARTED(scriptData.coreData.stState)
			AND HAS_NET_TIMER_EXPIRED_READ_ONLY(scriptData.coreData.stState, CONST_TIME_MS_CUTSCENE_START)
				ASSERTLN("DS_MAINTAIN_DELIVERY - forcing screen to black. IS_SCREEN_FADING_OUT: ", IS_SCREEN_FADING_OUT(), " IS_SCREEN_FADED_OUT: ", IS_SCREEN_FADED_OUT())
				DO_SCREEN_FADE_OUT(0)
				bTransition = TRUE
			ENDIF
		ENDIF
	ELSE
		IF NOT scriptData.deliveryData.cutsceneData.cutscene.bPlaying
			CDEBUG3LN(DEBUG_DELIVERY, "DS_MAINTAIN_DELIVERY - Not fading screen after delivery.")
			IF NETWORK_IS_GAME_IN_PROGRESS()
				IF NETWORK_IS_IN_MP_CUTSCENE()
						CDEBUG3LN(DEBUG_DELIVERY,"NETWORK_SET_IN_MP_CUTSCENE(FALSE,FALSE)")
					NETWORK_SET_IN_MP_CUTSCENE(FALSE, FALSE)
				ENDIF
			ENDIF
			bTransition = TRUE
		ENDIF
	ENDIF
	
	IF bTransition
		#IF IS_DEBUG_BUILD
		IF scriptData.debugData.bPlayCutscene
			DO_SCREEN_FADE_IN(0)
			PRIVATE_SET_DELIVERY_STATE(DS_INITIALIZE)
		ENDIF
		#ENDIF
		
			IF GET_DELIVERY_FLAG_EXTRA(scriptData.deliveryData, DELIVERY_FLAG_EXTRA_EXTERNAL_SCRIPT_PERFORMS_WARP_AFTER_SCENE)
				PRIVATE_SET_DELIVERY_STATE(DS_WAIT_FOR_WARP)
				EXIT
			ELIF GET_DELIVERY_FLAG_EXTRA(scriptData.deliveryData, DELIVERY_FLAG_EXTRA_REPOSITION_PED_AFTER_SCENE)
				BOOL bInVehicle = IS_ENTITY_ALIVE(PLAYER_PED_ID()) AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				BOOL bDriver 	=  bInVehicle AND (GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER)
				
				IF bInVehicle
				AND NOT bDriver
					PRIVATE_SET_DELIVERY_STATE(DS_WAIT_FOR_WARP)
				ELSE
					PRIVATE_SET_DELIVERY_STATE(DS_WARP_AFTER_SCENE)
				ENDIF
				
				EXIT
			ENDIF
		
		IF GET_DELIVERY_FLAG_EXTRA(scriptData.deliveryData, DELIVERY_FLAG_EXTRA_PERFORM_EARLY_ASSET_DELETION)
			DELIVERY_DELETE_ASSETS()
		ENDIF
		
		PRIVATE_SET_DELIVERY_STATE(DS_AFTER_DELIVERY)
	ENDIF
ENDPROC

PROC PROC_DS_MAINTAIN_CINEMATIC_DELIVERY_SCENE()
	IF NOT IS_SKYSWOOP_AT_GROUND()
		SCRIPT_CLEANUP("Sky swoop not at ground")
	ENDIF
	
	SET_BITMASK_AS_ENUM(scriptData.deliveryData.cutsceneData.cutscene.iSetupBM, SCS_PREVENT_VISIBLITY_CHANGES_START)
	SET_BITMASK_AS_ENUM(scriptData.deliveryData.cutsceneData.cutscene.iSetupBM, SCS_LEAVE_PLAYER_REMOTELY_VISIBLE)
	
	//Single call, before main update.
	IF IS_CUTSCENE_PLAYING()
		//On first frame of the cutscene, call once all these
		IF NOT GET_CORE_BITSET(DELIVERY_FLAG_CORE_FIRST_CUTSCENE_FRAME_INITIALIZE)
			
			// Very first init of cutscene
			NETWORK_HIDE_PROJECTILE_IN_CUTSCENE()	

			SET_LOCAL_PLAYER_VISIBLE_IN_CUTSCENE(FALSE, FALSE)
			
			IF NOT MPGlobals.bStartedMPCutscene
				START_MP_CUTSCENE(TRUE)
			ENDIF
			
			IF IS_SKYSWOOP_AT_GROUND()
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			ENDIF
			
			DELIVERY_TOGGLE_ASSETS_READY(TRUE)
			SET_FOCUS_POS_AND_VEL(scriptData.deliveryData.focusCoord, <<0.0, 0.0, 0.0>>)
			CLEAR_AREA_OF_OBJECTS(scriptData.deliveryData.clearAreaCoord, scriptData.deliveryData.clearAreaRange)
			TOGGLE_AUDIO_SCENE_CUTSCENE(TRUE)
			#IF IS_DEBUG_BUILD
			IF NOT scriptData.debugData.bPlayCutscene
			#ENDIF
			FREEMODE_DELIVERY_START_DELIVERY(g_FreemodeDeliveryData.eDropoffForDeliveryScript, scriptData.deliveryData.deliveryOwner, FALSE, FALSE, FALSE, IS_PLAYER_HOLDING_MULTIPLE_DELIVERABLES(PLAYER_ID()))	
			#IF IS_DEBUG_BUILD
			ENDIF
			#ENDIF
			
			IF IS_HELP_MESSAGE_BEING_DISPLAYED()
				CDEBUG3LN(DEBUG_DELIVERY, "DS_MAINTAIN_DELIVERY - CLEAR_HELP")
				CLEAR_HELP(TRUE)	
			ENDIF
			
			TOGGLE_CORE_BITSET(DELIVERY_FLAG_CORE_FIRST_CUTSCENE_FRAME_INITIALIZE, TRUE)
		ENDIF

		//Scene prepare event, called once a scene switched.(overrides assets ready where needed, only positions everything when needed)
		IF scriptData.deliveryData.cutsceneData.cutscene.iCurrentScene <> scriptData.coreData.iPreviousScene
			IF SIMPLE_CUTSCENE_IS_SCENE_RUNNING(scriptData.deliveryData.cutsceneData.cutscene, scriptData.deliveryData.cutsceneData.cutscene.iCurrentScene)
				CDEBUG3LN(DEBUG_DELIVERY, "DS_MAINTAIN_DELIVERY - handlerSceneSetup called, scene: ", scriptData.deliveryData.cutsceneData.cutscene.iCurrentScene)	
				CALL scriptData.functionPointers.coreFunctions.handlerSceneSetup(scriptData.deliveryData, scriptData.deliveryData.cutsceneData.cutscene.iCurrentScene)
				scriptData.coreData.iPreviousScene = scriptData.deliveryData.cutsceneData.cutscene.iCurrentScene
			ENDIF
		ENDIF
				
		//start showing delivery message on screen.
		IF NOT g_FreemodeDeliveryData.bDeliveryScriptAllowDeliveryShard
			IF CALL scriptData.functionPointers.specificFunctions.shouldStartDeliveryMessageDisplay(scriptData.deliveryData)
				CDEBUG3LN(DEBUG_DELIVERY, "DS_MAINTAIN_DELIVERY - g_FreemodeDeliveryData.bDeliveryScriptAllowDeliveryShard.")
				g_FreemodeDeliveryData.bDeliveryScriptAllowDeliveryShard = TRUE
			ENDIF
		ENDIF
		
		//Per frame update
		IF GET_DELIVERY_FLAG_DEFAULT(scriptData.deliveryData, DELIVERY_FLAG_DEFAULT_REMOVE_PARTICLE_FX_IN_RANGE)
			REMOVE_PARTICLE_FX_IN_RANGE(scriptData.deliveryData.vfxRemoveCoord, scriptData.deliveryData.vfxRemoveRange)
		ENDIF
				
		IF GET_DELIVERY_FLAG_DEFAULT(scriptData.deliveryData, DELIVERY_FLAG_DEFAULT_DISABLE_NIGHT_VISION)
			DELIVERY_MAINTAIN_NIGHT_VISION()
		ENDIF
		
		//Force peds to get out of vehicles properly if networked vehicles are blocking doors.
		CUTSCE_HELP_MAINTAIN_PED_ARRAY_FREEMODE_CUTSCENE(scriptData.deliveryData.cutsceneData.pedArray)
		INT index
		REPEAT CONST_DELIVERY_MAX_VEHICLES_PACK index
			CUTSCE_HELP_MAINTAIN_PED_ARRAY_FREEMODE_CUTSCENE(scriptData.deliveryData.cutsceneData.vehiclePackArray[index].passengers)
		ENDREPEAT
	ENDIF

	//Main Cutscene update
	CALL scriptData.functionPointers.coreFunctions.handlerMaintainScene(scriptData.deliveryData)
	
	//Should transition to next stage?
	BOOL bTransition
	bTransition = FALSE
	
	IF GET_DELIVERY_FLAG_DEFAULT(scriptData.deliveryData, DELIVERY_FLAG_DEFAULT_SCREEN_FADE_AFTER_DELIVERY)
		IF NOT GET_CORE_BITSET(DELIVERY_FLAG_CORE_START_CUTSCENE_FADE_OUT)
			IF CALL scriptData.functionPointers.specificFunctions.shouldCutsceneStartFadeOut(scriptData.deliveryData)
				//fade out failsafe just in case something breaks the fades/externally.
				IF NOT HAS_NET_TIMER_STARTED(scriptData.coreData.stState)
					CDEBUG3LN(DEBUG_DELIVERY, "DS_MAINTAIN_DELIVERY - stState started, fadeout failsafe timer started, waiting for: ", CONST_TIME_MS_CUTSCENE_START, " ms.")	
					START_NET_TIMER(scriptData.coreData.stState)
				ENDIF
				CDEBUG3LN(DEBUG_DELIVERY, "DS_MAINTAIN_DELIVERY - DELIVERY_CHECK_CUTSCENE_FADE_OUT_START - TRUE")	
				TOGGLE_CORE_BITSET(DELIVERY_FLAG_CORE_START_CUTSCENE_FADE_OUT, TRUE)
				DISABLE_COLLISIONS_OF_CLONE_VEHICLE()
			ENDIF
		ELSE

			IF (NOT IS_SCREEN_FADING_OUT() AND IS_SCREEN_FADED_OUT())
				CDEBUG3LN(DEBUG_DELIVERY, "DS_MAINTAIN_DELIVERY - Screen is faded out, transition")
				//Stop timer
				IF HAS_NET_TIMER_STARTED(scriptData.coreData.stState)
					RESET_NET_TIMER(scriptData.coreData.stState)
				ENDIF
				bTransition = TRUE
			ENDIF
			
			IF HAS_NET_TIMER_STARTED(scriptData.coreData.stState)
			AND HAS_NET_TIMER_EXPIRED_READ_ONLY(scriptData.coreData.stState, CONST_TIME_MS_CUTSCENE_START)
				ASSERTLN("DS_MAINTAIN_DELIVERY - forcing screen to black. IS_SCREEN_FADING_OUT: ", IS_SCREEN_FADING_OUT(), " IS_SCREEN_FADED_OUT: ", IS_SCREEN_FADED_OUT())
				DO_SCREEN_FADE_OUT(0)
				bTransition = TRUE
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_CUTSCENE_PLAYING()
			CDEBUG3LN(DEBUG_DELIVERY, "DS_MAINTAIN_DELIVERY - Not fading screen after delivery.")
			IF NETWORK_IS_GAME_IN_PROGRESS()
				IF NETWORK_IS_IN_MP_CUTSCENE()
					CDEBUG3LN(DEBUG_DELIVERY,"NETWORK_SET_IN_MP_CUTSCENE(FALSE,FALSE)")
					NETWORK_SET_IN_MP_CUTSCENE(FALSE, FALSE)
				ENDIF
			ENDIF
			bTransition = TRUE
		ENDIF
	ENDIF
	
	IF bTransition
		#IF IS_DEBUG_BUILD
		IF scriptData.debugData.bPlayCutscene
			DO_SCREEN_FADE_IN(0)
			PRIVATE_SET_DELIVERY_STATE(DS_INITIALIZE)
		ENDIF
		#ENDIF
		
			IF GET_DELIVERY_FLAG_EXTRA(scriptData.deliveryData, DELIVERY_FLAG_EXTRA_EXTERNAL_SCRIPT_PERFORMS_WARP_AFTER_SCENE)
				PRIVATE_SET_DELIVERY_STATE(DS_WAIT_FOR_WARP)
				EXIT
			ELIF GET_DELIVERY_FLAG_EXTRA(scriptData.deliveryData, DELIVERY_FLAG_EXTRA_REPOSITION_PED_AFTER_SCENE)
				BOOL bInVehicle = IS_ENTITY_ALIVE(PLAYER_PED_ID()) AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				BOOL bDriver 	=  bInVehicle AND (GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER)
				
				IF bInVehicle
				AND NOT bDriver
					PRIVATE_SET_DELIVERY_STATE(DS_WAIT_FOR_WARP)
				ELSE
					PRIVATE_SET_DELIVERY_STATE(DS_WARP_AFTER_SCENE)
				ENDIF
				
				EXIT
			ENDIF
		
		PRIVATE_SET_DELIVERY_STATE(DS_AFTER_DELIVERY)
	ENDIF
ENDPROC

PROC PROC_DS_MAINTAIN_DELIVERY()
	IF IS_DELIVERY_CUTSCENE_TYPE_SCRIPTED(scriptData.deliveryData.eCutscene)
		PROC_DS_MAINTAIN_SCRIPTED_DELIVERY_SCENE()
	#IF FEATURE_TUNER
	ELSE
		PROC_DS_MAINTAIN_CINEMATIC_DELIVERY_SCENE()
	#ENDIF
	ENDIF
ENDPROC

PROC DS_WAIT_FOR_DEFAULT_WARP()
	BOOL bInVehicle = IS_ENTITY_ALIVE(PLAYER_PED_ID()) AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	
	IF NOT bInVehicle
		SCRIPT_ASSERT("DS_WAIT_FOR_DEFAULT_WARP - No longer in vehicle!")
		CDEBUG1LN(DEBUG_DELIVERY,"DS_WAIT_FOR_DEFAULT_WARP No longer in a vehicle!")
		PRIVATE_SET_DELIVERY_STATE(DS_WARP_AFTER_SCENE)
		EXIT
	ENDIF
	
	VEHICLE_INDEX veh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		
	IF NOT IS_ENTITY_ALIVE(veh)
		SCRIPT_ASSERT("DS_WAIT_FOR_DEFAULT_WARP - Vehicle is dead!")
		CDEBUG1LN(DEBUG_DELIVERY,"DS_WAIT_FOR_DEFAULT_WARP - Vehicle is dead!")
		PRIVATE_SET_DELIVERY_STATE(DS_WARP_AFTER_SCENE)
		EXIT
	ENDIF
	
	PED_INDEX pedDriver = GET_PED_IN_VEHICLE_SEAT(veh, VS_DRIVER)	
	
	IF NOT IS_ENTITY_ALIVE(pedDriver)
		//No need for an assert here the driver will have just warped out before us
		CDEBUG1LN(DEBUG_DELIVERY,"DS_WAIT_FOR_DEFAULT_WARP - Driver is not alive!")
		PRIVATE_SET_DELIVERY_STATE(DS_WARP_AFTER_SCENE)
		EXIT
	ENDIF
	
	IF NOT IS_PED_A_PLAYER(pedDriver)
		SCRIPT_ASSERT("DS_WAIT_FOR_DEFAULT_WARP - Driver is not a player!")
		CDEBUG1LN(DEBUG_DELIVERY,"DS_WAIT_FOR_DEFAULT_WARP - Driver is not a player!")
		PRIVATE_SET_DELIVERY_STATE(DS_WARP_AFTER_SCENE)
		EXIT
	ENDIF
		
	PLAYER_INDEX playerDriver = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedDriver)
		
	IF NOT IS_NET_PLAYER_OK(playerDriver, FALSE)
		SCRIPT_ASSERT("DS_WAIT_FOR_DEFAULT_WARP - Driver is not okay!")
		CDEBUG1LN(DEBUG_DELIVERY,"DS_WAIT_FOR_DEFAULT_WARP - Driver is not okay!")
		PRIVATE_SET_DELIVERY_STATE(DS_WARP_AFTER_SCENE)
		EXIT
	ENDIF
			
	PARTICIPANT_INDEX piDriverParticipantID = NETWORK_GET_PARTICIPANT_INDEX(playerDriver)
	
	IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(piDriverParticipantID)		
		SCRIPT_ASSERT("DS_WAIT_FOR_DEFAULT_WARP - Driver is not a participant!")
		CDEBUG1LN(DEBUG_DELIVERY,"DS_WAIT_FOR_DEFAULT_WARP - Driver is not a participant!")
		PRIVATE_SET_DELIVERY_STATE(DS_WARP_AFTER_SCENE)
		EXIT
	ENDIF
	
	IF GET_PARTICIPANT_BROADCAST_BITSET(PBBS_WARP_AFTER_SCENE_FINISHED, NATIVE_TO_INT(piDriverParticipantID))
		CDEBUG1LN(DEBUG_DELIVERY,"DS_WAIT_FOR_DEFAULT_WARP - Driver finished warp.")
		PRIVATE_SET_DELIVERY_STATE(DS_AFTER_DELIVERY)
		
		IF IS_SCREEN_FADED_OUT()
		OR IS_SCREEN_FADED_OUT()
			DO_SCREEN_FADE_IN(CONST_TIME_MS_POST_SCENE_FADE_TIME)
		ENDIF
	ELSE
		IF HAS_NET_TIMER_STARTED(scriptData.coreData.stState)
		AND HAS_NET_TIMER_EXPIRED_READ_ONLY(scriptData.coreData.stState, CONST_TIME_MS_POST_SCENE_WARP)
			SCRIPT_ASSERT("DS_WAIT_FOR_DEFAULT_WARP - Warp Timeout!")
			CDEBUG1LN(DEBUG_DELIVERY,"DS_WAIT_FOR_DEFAULT_WARP - Warp Timeout!")
			PRIVATE_SET_DELIVERY_STATE(DS_WARP_AFTER_SCENE)
		ELSE
			CDEBUG3LN(DEBUG_DELIVERY,"DS_WAIT_FOR_DEFAULT_WARP - Waiting for driver to warp vehicle")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL DS_WAIT_FOR_PLAYERS_TO_ENTER_WARP()
	IF IS_LOCAL_PLAYER_IN_WARP_TRANSITION()
		CDEBUG1LN(DEBUG_DELIVERY,"DS_WAIT_FOR_NON_DRIVER_PLAYERS_TO_ENTER_WARP PLayer is now in warp transition, cleaning up script")
		FREEMODE_DELIVERY_SCRIPT_PREVENT_DELAY(TRUE)		
		FREEMODE_DELIVERY_FINISH_DELIVERY(scriptData.deliveryData.eDeliverableID)
		g_FreemodeDeliveryData.sDeliveryMissionID = INVALID_FM_DELIVERY_MISSION_ID()
		SCRIPT_CLEANUP("External script warp")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC DS_WAIT_FOR_EXTERNAL_WARP()
	IF DS_WAIT_FOR_PLAYERS_TO_ENTER_WARP()
		CDEBUG1LN(DEBUG_DELIVERY,"DS_WAIT_FOR_EXTERNAL_WARP - Recieved Warp Event")
		EXIT
	ENDIF
	
	// Only delivery drivers should send the event
	IF scriptData.deliveryData.driverWhoDelivered != PLAYER_ID()
		EXIT
	ENDIF
	
	// The driver needs to broadcast the warp to the other players
	IF FMC_TRIGGER_EXTERNAL_SCRIPT_WARP(scriptData.deliveryData)
		CDEBUG1LN(DEBUG_DELIVERY,"DS_WAIT_FOR_EXTERNAL_WARP - Launched external script warp")
		EXIT
	ENDIF
ENDPROC

PROC PROC_DS_WAIT_FOR_WARP()
	IF GET_DELIVERY_FLAG_EXTRA(scriptData.deliveryData, DELIVERY_FLAG_EXTRA_EXTERNAL_SCRIPT_PERFORMS_WARP_AFTER_SCENE)
		DS_WAIT_FOR_EXTERNAL_WARP()
	ELSE
		DS_WAIT_FOR_DEFAULT_WARP()
	ENDIF
ENDPROC

PROC PROC_DS_WARP_AFTER_SCENE()
		
	IF NOT GET_CORE_BITSET(DELIVERY_FLAG_CORE_SETUP_AFTER_SCENE_SPAWN_POINTS)
	
		TOGGLE_CORE_BITSET(DELIVERY_FLAG_CORE_SETUP_AFTER_SCENE_SPAWN_POINTS, TRUE)
		
		USE_CUSTOM_SPAWN_POINTS(TRUE, FALSE, FALSE, 0.2, DEFAULT, DEFAULT, DEFAULT, 0.1, DEFAULT, DEFAULT, TRUE)
		
		CALL scriptData.functionPointers.coreFunctions.handlerSetupAfterSceneSpawnPoints(scriptData.deliveryData)
		
		IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
			SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
		ENDIF
		
		CDEBUG1LN(DEBUG_DELIVERY,"PROC_DS_WARP_AFTER_SCENE Spawn points setup")
		
	ELIF NOT GET_CORE_BITSET(DELIVERY_FLAG_CORE_DONE_AFTER_SCENE_WARP)
		
		BOOL bKeepVeh = GET_DELIVERY_FLAG_EXTRA(scriptData.deliveryData, DELIVERY_FLAG_EXTRA_REPOSITION_PED_IN_VEH_AFTER_SCENE)
		
		IF WARP_TO_SPAWN_LOCATION(SPAWN_LOCATION_CUSTOM_SPAWN_POINTS, FALSE, FALSE, FALSE, bKeepVeh, FALSE, FALSE, bKeepVeh, FALSE, DEFAULT, TRUE, TRUE, FALSE, TRUE, FALSE, TRUE)
			CLEAR_CUSTOM_SPAWN_POINTS()
			
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())				
				
				NETWORK_FADE_IN_ENTITY(PLAYER_PED_ID(), TRUE, TRUE)
				FORCE_PLAYER_LOCALLY_VISIBLE_FOR_FADE()
				
				IF GET_DELIVERY_FLAG_EXTRA(scriptData.deliveryData, DELIVERY_FLAG_EXTRA_REMOVE_CARGOBOB_HOOK_AFTER_SCENE)
				OR GET_DELIVERY_FLAG_EXTRA(scriptData.deliveryData, DELIVERY_FLAG_EXTRA_REPOSITION_PED_IN_VEH_AFTER_SCENE)
				OR GET_DELIVERY_FLAG_EXTRA(scriptData.deliveryData, DELIVERY_FLAG_EXTRA_HIDE_DELIVERY_VEHICLE)
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						VEHICLE_INDEX veh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
						IF IS_ENTITY_ALIVE(veh)
							IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
								IF GET_DELIVERY_FLAG_EXTRA(scriptData.deliveryData, DELIVERY_FLAG_EXTRA_REMOVE_CARGOBOB_HOOK_AFTER_SCENE)
									REMOVE_PICK_UP_ROPE_FOR_CARGOBOB(veh)
								ENDIF
								IF GET_DELIVERY_FLAG_EXTRA(scriptData.deliveryData, DELIVERY_FLAG_EXTRA_REPOSITION_PED_IN_VEH_AFTER_SCENE)
									SET_ENTITY_INVINCIBLE(veh, FALSE)
								ENDIF
								IF GET_DELIVERY_FLAG_EXTRA(scriptData.deliveryData, DELIVERY_FLAG_EXTRA_HIDE_DELIVERY_VEHICLE)
									FREEZE_ENTITY_POSITION(veh, FALSE)
									NETWORK_FADE_IN_ENTITY(veh, TRUE, TRUE)
									SET_ENTITY_COLLISION(veh, TRUE)
									RESET_ENTITY_ALPHA(veh)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			RESET_NET_TIMER(scriptData.coreData.stState)
			TOGGLE_CORE_BITSET(DELIVERY_FLAG_CORE_DONE_AFTER_SCENE_WARP, TRUE)
			
			CDEBUG1LN(DEBUG_DELIVERY,"PROC_DS_WARP_AFTER_SCENE - Warp process finished")
		ENDIF
		
	ELSE
		IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
			//Wait one frame to start the input gait as it sometimes gets stuck if done immediately ater the warp
			IF NOT GET_DELIVERY_FLAG_EXTRA(scriptData.deliveryData, DELIVERY_FLAG_EXTRA_DONT_SIMULATE_INPUT_GAIT_ON_WARP)
			AND NOT GET_DELIVERY_FLAG_EXTRA(scriptData.deliveryData, DELIVERY_FLAG_EXTRA_REPOSITION_PED_IN_VEH_AFTER_SCENE)
			AND NOT GET_DELIVERY_FLAG_EXTRA(scriptData.deliveryData, DELIVERY_FLAG_EXTRA_REQUESTED_SIM_INPUT_GAIT)
				SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 1200)
				TOGGLE_DELIVERY_FLAG_EXTRA(scriptData.deliveryData, DELIVERY_FLAG_EXTRA_REQUESTED_SIM_INPUT_GAIT, TRUE)
			ENDIF
		ENDIF
		
		IF NOT HAS_NET_TIMER_STARTED(scriptData.coreData.stState)
			CDEBUG3LN(DEBUG_DELIVERY, "PROC_DS_WARP_AFTER_SCENE - end failsafe timer started, waiting for: ", CONST_TIME_MS_POST_WARP_FAILSAFE, " ms.")	
			START_NET_TIMER(scriptData.coreData.stState)
		ENDIF
		
		BOOL bFailsafeExpired
		bFailsafeExpired = HAS_NET_TIMER_EXPIRED_READ_ONLY(scriptData.coreData.stState, CONST_TIME_MS_POST_WARP_FAILSAFE)	
		
		IF GET_DELIVERY_FLAG_EXTRA(scriptData.deliveryData, DELIVERY_FLAG_EXTRA_HIDE_DELIVERY_VEHICLE)
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
			AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())			
				VEHICLE_INDEX veh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				
				IF IS_ENTITY_ALIVE(veh)
				AND NETWORK_IS_ENTITY_FADING(veh)
					RESET_ENTITY_ALPHA(veh)
					
					IF IS_SCREEN_FADED_OUT()
						DELIVERY_DELETE_ASSETS()
						DO_SCREEN_FADE_IN(CONST_TIME_MS_POST_SCENE_FADE_TIME)
					ELIF IS_SCREEN_FADED_IN()
						IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
							NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						ENDIF
					ENDIF
					
					EXIT
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_SCREEN_FADED_OUT()
		AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
			IF (GET_ENTITY_SPEED(PLAYER_PED_ID()) > 0.8 OR GET_DELIVERY_FLAG_EXTRA(scriptData.deliveryData, DELIVERY_FLAG_EXTRA_DONT_SIMULATE_INPUT_GAIT_ON_WARP))
			OR GET_DELIVERY_FLAG_EXTRA(scriptData.deliveryData, DELIVERY_FLAG_EXTRA_REPOSITION_PED_IN_VEH_AFTER_SCENE)
			OR bFailsafeExpired
				DO_SCREEN_FADE_IN(CONST_TIME_MS_POST_SCENE_FADE_TIME)
				TOGGLE_PARTICIPANT_BROADCAST_BITSET(PBBS_WARP_AFTER_SCENE_FINISHED, TRUE)
				PRIVATE_SET_DELIVERY_STATE(DS_AFTER_DELIVERY)
			ENDIF
		ENDIF
		
		IF IS_SCREEN_FADED_IN()
			TOGGLE_PARTICIPANT_BROADCAST_BITSET(PBBS_WARP_AFTER_SCENE_FINISHED, TRUE)
			PRIVATE_SET_DELIVERY_STATE(DS_AFTER_DELIVERY)
		ENDIF
	ENDIF
ENDPROC

PROC PROC_DS_AFTER_DELIVERY()
	#IF IS_DEBUG_BUILD
	IF NOT scriptData.debugData.bDontBringIntoProperty
	#ENDIF
		
	IF NOT GET_CORE_BITSET(DELIVERY_FLAG_CORE_DELIVERY_VEHICLE_TRANSITION_FINISHED)
		#IF IS_DEBUG_BUILD
		IF GET_FRAME_COUNT() % 90 = 0
			CDEBUG3LN(DEBUG_DELIVERY, "PROC_DS_AFTER_DELIVERY - DELIVERY_FLAG_CORE_DELIVERY_VEHICLE_TRANSITION_FINISHED - FALSE.")	
		ENDIF
		#ENDIF
		
		//EXIT
	ENDIF
	
	IF GET_CORE_BITSET(DELIVERY_FLAG_CORE_WILL_PERFORM_INTERIOR_WARP)
	AND NOT GET_DELIVERY_FLAG_EXTRA(scriptData.deliveryData, DELIVERY_FLAG_EXTRA_REPOSITION_PED_AFTER_SCENE)
		CDEBUG3LN(DEBUG_DELIVERY, "££ PROC_DS_AFTER_DELIVERY- will warp into interior at the end of cutscene, eDropOffID: ", ENUM_TO_INT(scriptData.deliveryData.eDropoffID))
		DELIVERY_TRIGGER_ENTRY_TO_SIMPLE_INTERIOR(scriptData.deliveryData.eDropoffID, scriptData.deliveryData.deliveryOwner)
		SET_PLAYER_DATA_DOING_INTERIOR_WARP_FROM_CUTSCENE(TRUE)
		SET_BIT(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_PREVENT_BIG_MESSAGE_DISABLE)
		
		IF FM_GANGOPS_DROPOFF_IS_PLAYER_PROPERTY(scriptData.deliveryData.eDropoffID)
		AND IS_LOCAL_PLAYER_DELIVERING_TO_DEFUNCT_BASE_IN_OWNERS_AVENGER(scriptData.deliveryData.deliveryOwner)
			PRINTLN("££ PROC_DS_AFTER_DELIVERY - Cutscene finished - Starting facility entry in avenger.")
			SET_BIT(g_SimpleInteriorData.iThirdBS, BS3_SIMPLE_INTERIOR_DEFUNCT_BASE_FINISHED_DELIVERY_MISSION_USING_AVENGER)
		ENDIF
		
		TOGGLE_CORE_BITSET(DELIVERY_FLAG_CORE_PERFORMING_INTERIOR_WARP, TRUE)
	ELIF GET_DELIVERY_FLAG_EXTRA(scriptData.deliveryData, DELIVERY_FLAG_EXTRA_REPOSITION_PED_AFTER_SCENE)
		//Don't do anything for now
	ELSE
		//reposition player//spawn in a vehicle...
		DO_SCREEN_FADE_IN(0)
		CDEBUG3LN(DEBUG_DELIVERY, "PROC_DS_AFTER_DELIVERY - DO_SCREEN_FADE_IN")	
	ENDIF
	
	//Cleanup on Warp
	//IF FREEMODE_DELIVERY_IS_DELIVERABLE_VALID(scriptData.deliveryData.eDeliverableID)
	//AND NOT GET_CORE_BITSET(DELIVERY_FLAG_CORE_WILL_PERFORM_INTERIOR_WARP)
	//	CDEBUG3LN(DEBUG_DELIVERY, "££ DS_AFTER_DELIVERY - Scene failed int vehicle, cleanup deliverable.")
	//	BROADCAST_OK_TO_CLEANUP_DELIVERABLE_ENTITY(scriptData.deliveryData.eDeliverableID)
	//ENDIF
	
	//prevent delivery script from being able to restart for x time.
	FREEMODE_DELIVERY_SCRIPT_PREVENT_DELAY(TRUE)
	
	#IF IS_DEBUG_BUILD
	ENDIF
	#ENDIF
	
	FREEMODE_DELIVERY_FINISH_DELIVERY(scriptData.deliveryData.eDeliverableID)
	g_FreemodeDeliveryData.sDeliveryMissionID = INVALID_FM_DELIVERY_MISSION_ID()
	
	SCRIPT_CLEANUP("After Delivery")
ENDPROC

PROC MAINTAIN_DELIVERY_VEH_EXTRA_STATES()
	IF scriptData.deliveryData.deliveryType = DT_VEHICLE
	AND DOES_ENTITY_EXIST(scriptData.deliveryData.cutsceneData.vehiclePackArray[CSH_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle)
		IF GET_DELIVERY_FLAG_EXTRA(scriptData.deliveryData, DELIVERY_FLAG_EXTRA_MAINTAIN_TULA_STATE_WINGS_UP)
			SET_VEHICLE_ENGINE_ON(scriptData.deliveryData.cutsceneData.vehiclePackArray[CSH_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle, FALSE, TRUE, TRUE)
			SET_VEHICLE_FLIGHT_NOZZLE_POSITION_IMMEDIATE(scriptData.deliveryData.cutsceneData.vehiclePackArray[CSH_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle, 1.0)
		ELIF GET_DELIVERY_FLAG_EXTRA(scriptData.deliveryData, DELIVERY_FLAG_EXTRA_MAINTAIN_TULA_STATE_WINGS_DOWN)
			SET_VEHICLE_ENGINE_ON(scriptData.deliveryData.cutsceneData.vehiclePackArray[CSH_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle, FALSE, TRUE, TRUE)
			SET_VEHICLE_FLIGHT_NOZZLE_POSITION_IMMEDIATE(scriptData.deliveryData.cutsceneData.vehiclePackArray[CSH_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle, 0.0)
		ENDIF		
	ENDIF
ENDPROC

PROC _BLOCK_VEHICLE_EXIT_DURING_DELIVERY()
	IF g_FreemodeDeliveryData.bAllowDeliveryScriptToBlockVehExitOnLoad
	AND g_FreemodeDeliveryData.bDeliveryScriptTriggeredSceneLoad
		IF scriptData.deliveryData.deliveryType = DT_VEHICLE
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_VEH_EXIT)
					DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_VEH_EXIT, TRUE)
					CDEBUG3LN(DEBUG_DELIVERY, "_BLOCK_VEHICLE_EXIT_DURING_DELIVERY - Blocking vehicle exit after triggering scene")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//###################################### SCRIPT ######################################
SCRIPT(FREEMODE_DELIVERY_SCRIPT_LAUNCH_DATA launchData)
	
	IF NETWORK_IS_GAME_IN_PROGRESS() 
		CDEBUG3LN(DEBUG_DELIVERY, "SCRIPT - START SCRIPT_INITIALISE")
		SCRIPT_INITIALISE(launchData)
	ELSE
		SCRIPT_CLEANUP("Game is not in progress")
	ENDIF
	
	#IF IS_DEBUG_BUILD
		DELIVERY_DEBUG_CREATE_WIDGETS(scriptData)
	#ENDIF
		
	WHILE TRUE
		MP_LOOP_WAIT_ZERO()
		
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			SCRIPT_CLEANUP("SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE")
		ENDIF
		
		//Script termiante checks.
		IF GET_CORE_BITSET(DELIVERY_FLAG_CORE_POINTERS_INITIALIZED)		
			//Terminate
			scriptData.sTermianteReason = CALL scriptData.functionPointers.specificFunctions.shouldTerminateScript(scriptData.deliveryData)
			IF NOT IS_STRING_NULL_OR_EMPTY(scriptData.sTermianteReason)
				SCRIPT_CLEANUP(scriptData.sTermianteReason)
			ENDIF
		ENDIF
		
		MAINTAIN_DELIVERABLE_CLEANUP()
		MAINTAIN_LOCAL_PLAYER_REPORTING_WANTED_LEVEL()		
		_BLOCK_VEHICLE_EXIT_DURING_DELIVERY()
		
		//Fade and moving of the delivery vehicle
		IF GET_DELIVERY_FLAG_EXTRA(scriptData.deliveryData, DELIVERY_FLAG_EXTRA_REPOSITION_DELIVERY_VEHICLE)
		OR GET_DELIVERY_FLAG_EXTRA(scriptData.deliveryData, DELIVERY_FLAG_EXTRA_FADE_OUT_AND_MOVE_PED)
			#IF IS_DEBUG_BUILD
			IF NOT scriptData.debugData.bDontBringIntoProperty					
			#ENDIF
				MAINTAIN_FADE_AND_MOVE_ON_DELIVERY()
			#IF IS_DEBUG_BUILD
			ELSE
				TOGGLE_CORE_BITSET(DELIVERY_FLAG_CORE_PLAYER_MOVE_COMPLETED, TRUE)
			ENDIF
			#ENDIF
		ELIF GET_DELIVERY_FLAG_EXTRA(scriptData.deliveryData, DELIVERY_FLAG_EXTRA_HIDE_DELIVERY_VEHICLE)
			IF PRIVATE_GET_DELIVERY_STATE() != DS_WARP_AFTER_SCENE
			AND PRIVATE_GET_DELIVERY_STATE() != DS_AFTER_DELIVERY
			AND PRIVATE_GET_DELIVERY_STATE() != DS_CLEANUP
				MAINTAIN_FADE_AND_HIDE_DURING_DELIVERY()
			ENDIF
		#IF FEATURE_TUNER
		ELIF GET_DELIVERY_FLAG_EXTRA(scriptData.deliveryData, DELIVERY_FLAG_EXTRA_DELETE_AMBIENT_VEHICLE)
			MAINTAIN_CLEANUP_AMBIENT_VEHICLE_DURING_SCENE()
		#ENDIF
		ENDIF
		
		//Maintain as soon as clones are created, since sometimes it requires time to load the hd models, there are no checks for this..
		IF GET_DELIVERY_FLAG_DEFAULT(scriptData.deliveryData, DELIVERY_FLAG_DEFAULT_FORCE_HD_VEHICLES)
			DELIVERY_MAINTAIN_FORCE_HD_VEHICLE()
		ENDIF
		//delay the control being takes
		DS_MAINTAIN_SCENE_START_DELAY_TIMER()
		
		IF NOT GET_CORE_BITSET(DELIVERY_FLAG_CORE_DELIVERY_PLAYER_MODE_UNDERGROUND)
		AND HAS_EVERYONE_STARTED_DELIVERY_CUTSCENE()
			CDEBUG3LN(DEBUG_DELIVERY, "[TFADE]SCRIPT - START TRANSITION_SET_CAN_MOVE_UNDERGROUND")
			TOGGLE_CORE_BITSET(BS_DELIVERY_FADE_AND_HIDE_ENTITIES, TRUE)
			TOGGLE_CORE_BITSET(DELIVERY_FLAG_CORE_DELIVERY_PLAYER_MODE_UNDERGROUND, TRUE)
		ENDIF
		
		SWITCH PRIVATE_GET_DELIVERY_STATE()
			CASE DS_INITIALIZE
				PROC_DS_INITIALIZE(launchData)
			BREAK
			
			CASE DS_WAIT_FOR_START
				PROC_DS_WAIT_FOR_START()
			BREAK

			CASE DS_LOAD_ASSETS
				PROC_DS_LOAD_ASSETS()
			BREAK
			
			CASE DS_PREPARE_CUTSCENE
				PROC_DS_PREPARE_CUTSCENE()
			BREAK
			
			CASE DS_STREAM_ASSETS
				PROC_DS_STREAM_ASSETS()
			BREAK
			
			#IF FEATURE_TUNER
			CASE DS_STREAM_CINEMATIC_SCENE
				PROC_DS_STREAM_CINEMATIC_SCENE()
			BREAK
			#ENDIF
			
			CASE DS_WAIT_FOR_PLAYERS
				PROC_DS_WAIT_FOR_PLAYERS()
			BREAK
			
			CASE DS_MAINTAIN_DELIVERY
				PROC_DS_MAINTAIN_DELIVERY()
			BREAK
			
			CASE DS_AFTER_DELIVERY
				PROC_DS_AFTER_DELIVERY()
			BREAK
			
			CASE DS_WARP_AFTER_SCENE
				PROC_DS_WARP_AFTER_SCENE()
			BREAK
			
			CASE DS_WAIT_FOR_WARP
				PROC_DS_WAIT_FOR_WARP()
			BREAK
		ENDSWITCH
		
		MAINTAIN_DELIVERY_VEH_EXTRA_STATES()
		
		#IF IS_DEBUG_BUILD
		IF IS_DEBUG_KEY_JUST_RELEASED(KEY_F, KEYBOARD_MODIFIER_NONE, "F fail")
			SCRIPT_CLEANUP("Debug Key Pressed - F")
		ENDIF
			
			DELIVERY_DEBUG_UPDATE(scriptData)
		
			IF scriptData.debugData.bTerminateScript
				SCRIPT_CLEANUP("Terminate Force")
			ENDIF
		#ENDIF
		
	ENDWHILE
ENDSCRIPT
		
