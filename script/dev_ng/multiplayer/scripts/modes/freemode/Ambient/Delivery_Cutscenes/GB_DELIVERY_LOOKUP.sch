USING "globals.sch"
USING "GB_DELIVERY_DEFINITIONS.sch"
USING "GB_DELIVERY_GLOBAL.sch"
//Cutscenes
USING "GB_DELIVERY_CUTSCENE_SMUGGLER_HANGAR.sch"
USING "GB_DELIVERY_CUTSCENE_SMUGGLER_PRECISION_DELIVERY.sch"
USING "GB_DELIVERY_CUTSCENE_GANG_OPS_DEFUNCT_BASE.sch"
USING "business_battle_secondary_dropoff_delivery.sch"
USING "GB_DELIVERY_CUTSCENE_GBC_CASINO.sch"
USING "GB_DELIVERY_CUTSCENE_CSH_ARCADE.sch"
USING "FM_CONTENT_CUTSCENE.sch"
#IF FEATURE_TUNER
USING "FM_CONTENT_CINEMATIC_CUTSCENE.sch"
#ENDIF

FUNC BOOL DELIVERY_LINK_DELIVERY_CORE_HANDLERS(FREEMODE_DELIVERY_SCRIPT_LAUNCH_DATA &_launchData, SCRIPT_DATA &_scriptData)
	SWITCH _launchData.eCutscene
		CASE SMUGGLER_CUTSCENE_HANGAR_BRING_EVERYTHING
		CASE SMUGGLER_CUTSCENE_HANGAR_LEAVE_VEHICLE
			CUTSCENE_SMUGGLER_HANGAR_INITIALIZE(_launchData, _scriptData)
		RETURN TRUE
		
		CASE SMUGGLER_CUTSCENE_PRECISION_DELIVERY
			CUTSCENE_SMUGGLER_PRECISION_DELIVERY_INITIALIZE(_launchData, _scriptData)
		RETURN TRUE
		
		CASE GANG_OPS_CUTSCENE_DEFUNCT_BASE
			CUTSCENE_GANG_OPS_DEFUNCT_BASE_INITIALIZE(_launchData, _scriptData)
		RETURN TRUE
		
		CASE BBT_SECONDARY_DROP_DELIVERY
			CUTSCENE_FMBB_INITIALIZE(_launchData, _scriptData)
		RETURN TRUE
		
		CASE GBC_CUTSCENE_CASINO
			CUTSCENE_GBC_CASINO_INITIALIZE(_launchData, _scriptData)
		RETURN TRUE		
		
		CASE CSH_CUTSCENE
			CUTSCENE_CSH_ARCADE_INITIALIZE(_launchData, _scriptData)
		RETURN TRUE
		
		CASE FMC_CUTSCENE
			CUTSCENE_FMC_INITIALIZE(_launchData, _scriptData)
		RETURN TRUE
		
		#IF FEATURE_TUNER
		CASE FMC_CINEMATIC_CUTSCENE
			FMC_CINEMATIC_SCENE_INITIALIZE(_launchData, _scriptData)
		RETURN TRUE
		#ENDIF
	ENDSWITCH
	
	ASSERTLN("DELIVERY_LINK_HANDLERS -  was unable to LINK specific handlers")
	RETURN FALSE
ENDFUNC
