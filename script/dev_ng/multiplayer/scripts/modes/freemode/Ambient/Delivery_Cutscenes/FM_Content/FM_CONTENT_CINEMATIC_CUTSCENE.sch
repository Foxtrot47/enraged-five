USING "globals.sch"
USING "mp_globals_common.sch"
USING "net_simple_cutscene.sch"
USING "GB_DELIVERY_DEFINITIONS.sch"
USING "GB_DELIVERY_CHILD_SUPPORT.sch"
USING "net_simple_interior.sch"
USING "FM_CONTENT_CUTSCENE_SPAWNS.sch"

//USE THE CONSTS DEFINED HERE. NOT FROM fm_content_cutscene_data.sch

//INT array
CONST_INT FMC_CINEMATIC_DELIVERY_INT_ARRAY_BITSET		0
CONST_INT FMC_CINEMATIC_DELIVERY_INT_ARRAY_SELECTD_PED	1

//Vehicle array
CONST_INT FMC_CINEMATIC_DELIVERY_VEH_ARRAY_PRIMARY_VEH	0

//Vehicle Passengers array
CONST_INT FMC_CINEMATIC_DELIVERY_PED_ARRAY_PRIMARY_PED	0

//Extra Ped array
CONST_INT FMC_CINEMATIC_DELIVERY_PED_ARRAY_EXTRA_PED	0

//BS
CONST_INT FMC_CINEMATIC_DELIVERY_BS_SET_STREAMING_FLAGS		0
CONST_INT FMC_CINEMATIC_DELIVERY_BS_REQUESTED_SCENE			1
CONST_INT FMC_CINEMATIC_DELIVERY_BS_SET_VEH_POS				2
CONST_INT FMC_CINEMATIC_DELIVERY_BS_SELECTED_PED_FOR_SCENE	3

FUNC BOOL FMC_CINEMATIC_SCENE_ASSETS_LOADED(DELIVERY_DATA &deliveryData)
	
	BOOL bAllAssetsLoaded = TRUE
	
	IF NOT IS_BIT_SET(deliveryData.cutsceneData.intArray[FMC_CINEMATIC_DELIVERY_INT_ARRAY_BITSET], FMC_CINEMATIC_DELIVERY_BS_SELECTED_PED_FOR_SCENE)
		
		CDEBUG2LN(DEBUG_DELIVERY, "FMC_CINEMATIC_SCENE_ASSETS_LOADED - Selecting ped model. Gender enum: ", g_CustomDeliverySceneGenderPed)
		
		IF FMC_DROPOFF_IS_AUTO_SHOP_CUSTOMER(deliveryData.eDropoffID)
			IF FMC_DROPOFF_IS_AUTO_SHOP_CUSTOMER_HIGH_CLASS(deliveryData.eDropoffID)
				deliveryData.cutsceneData.intArray[FMC_CINEMATIC_DELIVERY_INT_ARRAY_SELECTD_PED] = GET_RANDOM_INT_IN_RANGE(4, 8)
			ELSE
				deliveryData.cutsceneData.intArray[FMC_CINEMATIC_DELIVERY_INT_ARRAY_SELECTD_PED] = GET_RANDOM_INT_IN_RANGE(0, 4)
			ENDIF
		#IF FEATURE_DLC_1_2022
		ELIF g_CustomDeliverySceneGenderPed = eCUSTOMDELIVERYSCENE_PEDGENDER_NO_PREFERENCE
			deliveryData.cutsceneData.intArray[FMC_CINEMATIC_DELIVERY_INT_ARRAY_SELECTD_PED] = GET_RANDOM_INT_IN_RANGE(0, 4)
		ELIF g_CustomDeliverySceneGenderPed = eCUSTOMDELIVERYSCENE_PEDGENDER_MALE
			deliveryData.cutsceneData.intArray[FMC_CINEMATIC_DELIVERY_INT_ARRAY_SELECTD_PED] = GET_RANDOM_INT_IN_RANGE(0, 2)
		ELSE
			deliveryData.cutsceneData.intArray[FMC_CINEMATIC_DELIVERY_INT_ARRAY_SELECTD_PED] = GET_RANDOM_INT_IN_RANGE(2, 4)
		#ENDIF
		ENDIF
		
		SET_BIT(deliveryData.cutsceneData.intArray[FMC_CINEMATIC_DELIVERY_INT_ARRAY_BITSET], FMC_CINEMATIC_DELIVERY_BS_SELECTED_PED_FOR_SCENE)
	ENDIF
	
	//We don't stream the mocap in here as this needs to happen after we have created and streamed all other assets
	//Load general props needed for a scene at a given location
	INT iNumProps = GET_NUM_PROPS_FOR_FMC_SCENE(deliveryData.eDropoffID)
			
	IF iNumProps > 0
		INT i
		
		REPEAT iNumProps i
			MODEL_NAMES eModel = GET_PROP_FOR_FMC_SCENE(deliveryData.eDropoffID, i)
			
			IF FMC_DROPOFF_IS_AUTO_SHOP_CUSTOMER(deliveryData.eDropoffID)
				eModel = GET_PROP_FOR_FMC_SCENE(deliveryData.eDropoffID, deliveryData.cutsceneData.intArray[FMC_CINEMATIC_DELIVERY_INT_ARRAY_SELECTD_PED])
			ENDIF
			
			IF eModel != DUMMY_MODEL_FOR_SCRIPT
				REQUEST_MODEL(eModel)
				
				IF NOT HAS_MODEL_LOADED(eModel)
					bAllAssetsLoaded = FALSE
				ENDIF
				
				SET_BIT(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET], FMC_DELIVERY_BITSET_REQUESTED_SCENE_MODELS)
			ENDIF
		ENDREPEAT
	ENDIF
	
	RETURN bAllAssetsLoaded
ENDFUNC

FUNC PED_TYPE FMC_CINEMATIC_GET_PED_TYPE_FOR_SCENE(DELIVERY_DATA &deliveryData)
	MODEL_NAMES eModel = GET_PROP_FOR_FMC_SCENE(deliveryData.eDropoffID, deliveryData.cutsceneData.intArray[FMC_CINEMATIC_DELIVERY_INT_ARRAY_SELECTD_PED])
	
	PED_TYPE pedType = PEDTYPE_CIVFEMALE
	
	IF eModel = A_M_Y_HIPSTER_01
	OR eModel = A_M_Y_STWHI_02
	OR eModel = A_M_M_BEVHILLS_02
	OR eModel = A_M_Y_BUSICAS_01
		pedType = PEDTYPE_CIVMALE
	ENDIF
	
	RETURN pedType
ENDFUNC

FUNC BOOL FMC_CINEMATIC_SCENE_ASSETS_CREATE(DELIVERY_DATA &deliveryData)
	
	IF IS_BIT_SET(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET], FMC_DELIVERY_BITSET_REQUESTED_SCENE_MODELS)
		IF IS_BIT_SET(deliveryData.cutsceneData.intArray[FMC_CINEMATIC_DELIVERY_INT_ARRAY_BITSET], FMC_CINEMATIC_DELIVERY_BS_SELECTED_PED_FOR_SCENE)
			MODEL_NAMES eModel = GET_PROP_FOR_FMC_SCENE(deliveryData.eDropoffID, deliveryData.cutsceneData.intArray[FMC_CINEMATIC_DELIVERY_INT_ARRAY_SELECTD_PED])
			PED_TYPE pedType = FMC_CINEMATIC_GET_PED_TYPE_FOR_SCENE(deliveryData)
			
			VECTOR vCoords
			
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
				vCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
				vCoords -= <<0.0, 0.0, 10.0>>
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(deliveryData.cutsceneData.pedArray[FMC_CINEMATIC_DELIVERY_PED_ARRAY_EXTRA_PED])
				deliveryData.cutsceneData.pedArray[FMC_CINEMATIC_DELIVERY_PED_ARRAY_EXTRA_PED] = CREATE_PED(pedType, eModel, vCoords, DEFAULT, FALSE, FALSE)
			ENDIF
			
			IF IS_ENTITY_ALIVE(deliveryData.cutsceneData.pedArray[FMC_CINEMATIC_DELIVERY_PED_ARRAY_EXTRA_PED])
				SET_ENTITY_INVINCIBLE(deliveryData.cutsceneData.pedArray[FMC_CINEMATIC_DELIVERY_PED_ARRAY_EXTRA_PED], TRUE)
				SET_ENTITY_PROOFS(deliveryData.cutsceneData.pedArray[FMC_CINEMATIC_DELIVERY_PED_ARRAY_EXTRA_PED], TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, FALSE, TRUE)
				SET_PED_CONFIG_FLAG(deliveryData.cutsceneData.pedArray[FMC_CINEMATIC_DELIVERY_PED_ARRAY_EXTRA_PED], PCF_CanBeAgitated, FALSE)
				SET_PED_CONFIG_FLAG(deliveryData.cutsceneData.pedArray[FMC_CINEMATIC_DELIVERY_PED_ARRAY_EXTRA_PED], PCF_PedIgnoresAnimInterruptEvents, TRUE)
				SET_PED_CONFIG_FLAG(deliveryData.cutsceneData.pedArray[FMC_CINEMATIC_DELIVERY_PED_ARRAY_EXTRA_PED], PCF_DisableExplosionReactions, TRUE)
				SET_PED_CONFIG_FLAG(deliveryData.cutsceneData.pedArray[FMC_CINEMATIC_DELIVERY_PED_ARRAY_EXTRA_PED], PCF_DisableShockingEvents, TRUE)
				SET_PED_CONFIG_FLAG(deliveryData.cutsceneData.pedArray[FMC_CINEMATIC_DELIVERY_PED_ARRAY_EXTRA_PED], PCF_IgnoreBeingOnFire, TRUE)
			ELSE
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL FMC_CINEMATIC_SCENE_STREAM_CUTSCENE(DELIVERY_DATA &deliveryData #IF IS_DEBUG_BUILD , DELIVERY_DEBUG &sDebugData #ENDIF)
	BOOL bAllAssetsLoaded = TRUE
	
	IF NOT IS_BIT_SET(deliveryData.cutsceneData.intArray[FMC_CINEMATIC_DELIVERY_INT_ARRAY_BITSET], FMC_CINEMATIC_DELIVERY_BS_REQUESTED_SCENE)
		STRING sSceneName = FMC_CINEMATIC_SCENE_GET_SCENE_NAME(deliveryData.eDropoffID)
		CUTSCENE_SECTION eSections
		
		IF IS_STRING_NULL_OR_EMPTY(sSceneName)
			RETURN FALSE
		ELIF FMC_CINEMATIC_SCENE_SHOULD_REQUEST_SPECIFIC_SECTIONS(deliveryData, eSections #IF IS_DEBUG_BUILD , sDebugData.iCinematicsceneVar #ENDIF)
			CDEBUG1LN(DEBUG_DELIVERY, "FMC_CINEMATIC_SCENE_ASSETS_LOADED - Request scene: ", sSceneName, " with sections: ", eSections)
			REQUEST_CUTSCENE_WITH_PLAYBACK_LIST(sSceneName, eSections)
		ELSE
			CDEBUG1LN(DEBUG_DELIVERY, "FMC_CINEMATIC_SCENE_ASSETS_LOADED - Request scene: ", sSceneName)
			REQUEST_CUTSCENE(sSceneName)
		ENDIF
		
		SET_BIT(deliveryData.cutsceneData.intArray[FMC_CINEMATIC_DELIVERY_INT_ARRAY_BITSET], FMC_CINEMATIC_DELIVERY_BS_REQUESTED_SCENE)
		
		FMC_CINEMATIC_CUTSCENE_SET_LOCATION(deliveryData, eSections)
		
		//Give a one frame gap before we try to register any assets for the scene
		RETURN FALSE
	ENDIF
	
	IF NOT IS_BIT_SET(deliveryData.cutsceneData.intArray[FMC_CINEMATIC_DELIVERY_INT_ARRAY_BITSET], FMC_CINEMATIC_DELIVERY_BS_SET_STREAMING_FLAGS)
		IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
			CDEBUG1LN(DEBUG_DELIVERY, "FMC_CINEMATIC_SCENE_ASSETS_LOADED - Streaming flags set for main entity")
			SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_1", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
			SET_CUTSCENE_ENTITY_STREAMING_FLAGS("TUN_CUSTO1", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
			SET_BIT(deliveryData.cutsceneData.intArray[FMC_CINEMATIC_DELIVERY_INT_ARRAY_BITSET], FMC_CINEMATIC_DELIVERY_BS_SET_STREAMING_FLAGS)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF NOT HAS_CUTSCENE_LOADED()
		bAllAssetsLoaded = FALSE
	ENDIF
	
	RETURN bAllAssetsLoaded
ENDFUNC

PROC FMC_CINEMATIC_SCENE_REGISTER_SCENE_ENTITIES(DELIVERY_DATA &deliveryData)
	//Register the primary player
	IF IS_ENTITY_ALIVE(deliveryData.cutsceneData.vehiclePackArray[FMC_CINEMATIC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].passengers[FMC_CINEMATIC_DELIVERY_PED_ARRAY_PRIMARY_PED])
		CDEBUG1LN(DEBUG_DELIVERY, "FMC_CINEMATIC_SCENE_ASSETS_CREATE - register player ped clone for scene")
		REGISTER_ENTITY_FOR_CUTSCENE(deliveryData.cutsceneData.vehiclePackArray[FMC_CINEMATIC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].passengers[FMC_CINEMATIC_DELIVERY_PED_ARRAY_PRIMARY_PED], "MP_1", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
	ENDIF
	
	IF IS_ENTITY_ALIVE(deliveryData.cutsceneData.pedArray[FMC_CINEMATIC_DELIVERY_PED_ARRAY_EXTRA_PED])
		PED_TYPE pedType = FMC_CINEMATIC_GET_PED_TYPE_FOR_SCENE(deliveryData)
		
		CDEBUG1LN(DEBUG_DELIVERY, "FMC_CINEMATIC_SCENE_ASSETS_CREATE - register player ped clone for scene.pedType = ", pedType)
		
		IF pedType = PEDTYPE_CIVMALE
			REGISTER_ENTITY_FOR_CUTSCENE(deliveryData.cutsceneData.pedArray[FMC_CINEMATIC_DELIVERY_PED_ARRAY_EXTRA_PED], "TUN_CUSTO1", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
			REGISTER_ENTITY_FOR_CUTSCENE(NULL, "TUN_CUSTO2", CU_DONT_ANIMATE_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
		ELSE
			REGISTER_ENTITY_FOR_CUTSCENE(NULL, "TUN_CUSTO1", CU_DONT_ANIMATE_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
			REGISTER_ENTITY_FOR_CUTSCENE(deliveryData.cutsceneData.pedArray[FMC_CINEMATIC_DELIVERY_PED_ARRAY_EXTRA_PED], "TUN_CUSTO2", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
		ENDIF
	ENDIF
	
	//Register the delivery vehicle if required
//	IF IS_ENTITY_ALIVE(deliveryData.cutsceneData.vehiclePackArray[FMC_CINEMATIC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle)
//		CDEBUG1LN(DEBUG_DELIVERY, "FMC_CINEMATIC_SCENE_ASSETS_CREATE - register delivery vehicle clone for scene")
//		REGISTER_ENTITY_FOR_CUTSCENE(deliveryData.cutsceneData.vehiclePackArray[FMC_CINEMATIC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle, "Delivery_vehicle", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
//	ENDIF	
ENDPROC

FUNC BOOL FMC_CINEMATIC_SCENE_CREATE_SCENE(DELIVERY_DATA &deliveryData)
	UNUSED_PARAMETER(deliveryData)
	//Dummy function not used for cinematic scenes, but required by scripted scenes
	RETURN TRUE
ENDFUNC

PROC MAINTAIN_REAL_VEHICLE_VISIBILITY_FOR_SCENE(DELIVERY_DATA &deliveryData)
	
	INT indexPassenger
	PED_INDEX ped
	
	IF IS_ENTITY_ALIVE(deliveryData.deliveryVehicle)
	AND NETWORK_HAS_CONTROL_OF_ENTITY(deliveryData.deliveryVehicle)
	
		FOR indexPassenger = VS_DRIVER TO VS_EXTRA_RIGHT_3
			VEHICLE_SEAT vehSeat = INT_TO_ENUM(VEHICLE_SEAT, indexPassenger)
			
			ped = GET_PED_IN_VEHICLE_SEAT(deliveryData.deliveryVehicle, vehSeat)
			
			IF IS_ENTITY_ALIVE(ped)			
				SET_ENTITY_LOCALLY_INVISIBLE(ped)
			ENDIF
		ENDFOR
		
		SET_ENTITY_LOCALLY_INVISIBLE(deliveryData.deliveryVehicle)
		SET_VEHICLE_ENGINE_ON(deliveryData.deliveryVehicle, FALSE, TRUE)
		SET_VEHICLE_RADIO_ENABLED(deliveryData.deliveryVehicle, FALSE)
	ENDIF
ENDPROC

PROC FMC_CINEMATIC_SCENE_MAINTAIN(DELIVERY_DATA &deliveryData)	
	//Add functions here that need called during the cinematic scene
	MAINTAIN_REAL_VEHICLE_VISIBILITY_FOR_SCENE(deliveryData)
	
	ENTITY_INDEX scenVeh = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("Delivery_vehicle", HERMES)
	
	//Hacky fix owing to a lack of a beter option
	FLOAT fTargetVehHeading
	VECTOR vTargetPosition
	
	IF IS_ENTITY_ALIVE(scenVeh)
		SET_ENTITY_VISIBLE(scenVeh, FALSE)
		SET_ENTITY_COLLISION(scenVeh, FALSE)
		FREEZE_ENTITY_POSITION(scenVeh, TRUE)
		
		vTargetPosition		= GET_ENTITY_COORDS(scenVeh)
		fTargetVehHeading 	= GET_ENTITY_HEADING(scenVeh)
	ENDIF
	
	IF NOT IS_BIT_SET(deliveryData.cutsceneData.intArray[FMC_CINEMATIC_DELIVERY_INT_ARRAY_BITSET], FMC_CINEMATIC_DELIVERY_BS_SET_VEH_POS)
		IF IS_ENTITY_ALIVE(deliveryData.cutsceneData.vehiclePackArray[FMC_CINEMATIC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle)
			SET_ENTITY_COORDS(deliveryData.cutsceneData.vehiclePackArray[FMC_CINEMATIC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle, vTargetPosition, FALSE, FALSE, FALSE, FALSE)
			SET_ENTITY_HEADING(deliveryData.cutsceneData.vehiclePackArray[FMC_CINEMATIC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle, fTargetVehHeading)
			SET_VEHICLE_ON_GROUND_PROPERLY(deliveryData.cutsceneData.vehiclePackArray[FMC_CINEMATIC_DELIVERY_VEH_ARRAY_PRIMARY_VEH].vehicle)
			
			SET_BIT(deliveryData.cutsceneData.intArray[FMC_CINEMATIC_DELIVERY_INT_ARRAY_BITSET], FMC_CINEMATIC_DELIVERY_BS_SET_VEH_POS)
		ENDIF
	ENDIF
ENDPROC

PROC FMC_CINEMATIC_SCENE_ASSET_SCENE_SETUP(DELIVERY_DATA &deliveryData, INT iScene)
	UNUSED_PARAMETER(deliveryData)
	UNUSED_PARAMETER(iScene)
ENDPROC

PROC FMC_CINEMATIC_SCENE_CLEANUP(DELIVERY_DATA &deliveryData)
	UNUSED_PARAMETER(deliveryData)
	
	IF IS_CUTSCENE_PLAYING()
		STOP_CUTSCENE_IMMEDIATELY()				
	ENDIF
	
	REMOVE_CUTSCENE()
	
	IF IS_BIT_SET(deliveryData.cutsceneData.intArray[FMC_DELIVERY_INT_ARRAY_BITSET], FMC_DELIVERY_BITSET_REQUESTED_SCENE_MODELS)
		INT i
		INT iNumProps = GET_NUM_PROPS_FOR_FMC_SCENE(deliveryData.eDropoffID)
		
		REPEAT iNumProps i
			MODEL_NAMES eModel = GET_PROP_FOR_FMC_SCENE(deliveryData.eDropoffID, i)
			
			IF FMC_DROPOFF_IS_AUTO_SHOP_CUSTOMER(deliveryData.eDropoffID)
				eModel = GET_PROP_FOR_FMC_SCENE(deliveryData.eDropoffID, deliveryData.cutsceneData.intArray[FMC_CINEMATIC_DELIVERY_INT_ARRAY_SELECTD_PED])
			ENDIF
			
			IF eModel != DUMMY_MODEL_FOR_SCRIPT
				SET_MODEL_AS_NO_LONGER_NEEDED(eModel)
			ENDIF
		ENDREPEAT
	ENDIF
	
ENDPROC

CONST_INT FMC_CINEMATIC_SCENE_FADE_TIME	500

FUNC BOOL SHOULD_FMC_CINEMATIC_CUTSCENE_FADE_OUT_START(DELIVERY_DATA &deliveryData)
	UNUSED_PARAMETER(deliveryData)
	INT iTotalPlaybackTime 	= GET_CUTSCENE_END_TIME()
	INT iCurrentRunTime 	= GET_CUTSCENE_TIME()
	
	IF ((iTotalPlaybackTime - iCurrentRunTime) <= FMC_CINEMATIC_SCENE_FADE_TIME)
		IF IS_SCREEN_FADED_IN()
			DO_SCREEN_FADE_OUT(FMC_CINEMATIC_SCENE_FADE_TIME)
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC FMC_CINEMATIC_SCENE_INITIALIZE(FREEMODE_DELIVERY_SCRIPT_LAUNCH_DATA &_launchData, SCRIPT_DATA& _scriptData)	
	UNUSED_PARAMETER(_launchData)
		
	CDEBUG1LN(DEBUG_DELIVERY, "FMC_CINEMATIC_SCENE_INITIALIZE - DELIVERY TYPE: ", _scriptData.deliveryData.deliveryType)
	
	//Lookup
	_scriptData.functionPointers.coreFunctions.handlerAssetsLoaded 					= &FMC_CINEMATIC_SCENE_ASSETS_LOADED
	_scriptData.functionPointers.coreFunctions.handlerAssetsCreate 					= &FMC_CINEMATIC_SCENE_ASSETS_CREATE
	_scriptData.functionPointers.coreFunctions.handlerCreateScene 					= &FMC_CINEMATIC_SCENE_CREATE_SCENE
	_scriptData.functionPointers.coreFunctions.handlerCinematicStream 				= &FMC_CINEMATIC_SCENE_STREAM_CUTSCENE
	_scriptData.functionPointers.coreFunctions.handlerMaintainScene 				= &FMC_CINEMATIC_SCENE_MAINTAIN
	_scriptData.functionPointers.coreFunctions.handlerSceneSetup 					= &FMC_CINEMATIC_SCENE_ASSET_SCENE_SETUP
	_scriptData.functionPointers.coreFunctions.handlerCleanup 						= &FMC_CINEMATIC_SCENE_CLEANUP
	_scriptData.functionPointers.coreFunctions.handlerSetupAfterSceneSpawnPoints 	= &CUTSCENE_FMC_SPAWN_POINT_SETUP
	_scriptData.functionPointers.specificFunctions.shouldCutsceneStartFadeOut		= &SHOULD_FMC_CINEMATIC_CUTSCENE_FADE_OUT_START
	_scriptData.functionPointers.specificFunctions.registerSceneEntities			= &FMC_CINEMATIC_SCENE_REGISTER_SCENE_ENTITIES
		
	TOGGLE_DELIVERY_FLAG_EXTRA(_scriptData.deliveryData, DELIVERY_FLAG_EXTRA_REPOSITION_PED_AFTER_SCENE, TRUE)
	TOGGLE_DELIVERY_FLAG_EXTRA(_scriptData.deliveryData, DELIVERY_FLAG_EXTRA_CLONE_ONLY_DRIVER, TRUE)
	TOGGLE_DELIVERY_FLAG_EXTRA(_scriptData.deliveryData, DELIVERY_FLAG_EXTRA_DONT_SIMULATE_INPUT_GAIT_ON_WARP, TRUE)
	TOGGLE_DELIVERY_FLAG_EXTRA(_scriptData.deliveryData, DELIVERY_FLAG_EXTRA_ENABLE_AGGRESSIVE_BRING_VEH_TO_HALT, TRUE)	
	
ENDPROC
