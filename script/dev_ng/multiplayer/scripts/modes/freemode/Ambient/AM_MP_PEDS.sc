//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        AM_MP_PEDS.sch																							//
// Description: Script for maintaining peds. 																			//
// Written by:  Online Technical Team: Scott Ranken																		//
// Date:  		26/02/20																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "net_include.sch"
USING "net_wait_zero.sch"

#IF FEATURE_HEIST_ISLAND
USING "net_peds_animation.sch"
USING "net_peds_head_tracking.sch"
USING "net_peds_dancing.sch"
USING "net_peds_dj_switch_scene.sch"
USING "net_peds_dr_dre_switch_scene.sch"

#IF IS_DEBUG_BUILD
USING "net_peds_debug.sch"
USING "profiler.sch"
#ENDIF

//╒══════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ VARIABLES ╞═══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

SCRIPT_PED_DATA LocalData
SERVER_PED_DATA ServerBD
PED_ANIM_DATA PedAnimData

#IF IS_DEBUG_BUILD
PED_DEBUG_DATA DebugData
#ENDIF

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ CLEANUP ╞════════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

PROC CLEANUP_PED_GLOBALS()
	DEBUG_PRINTCALLSTACK()
	g_clubMusicData.eNextDJ = CLUB_DJ_NULL
	g_clubMusicData.bHasDJSwitchOverSceneStarted = FALSE
	g_clubMusicData.bHasNextDJDataFileLoaded = FALSE
	g_bTurnOnPeds = FALSE
	g_bInitPedsCreated = FALSE
	g_iPedLayout = -1
	CLEAR_PEDS_BITSET(g_iPedScriptGlobalBS)
	#IF IS_DEBUG_BUILD
	g_clubMusicData.eDebugNextDJ = CLUB_DJ_NULL
	g_bDebugHideCutsceneAssets = FALSE
	#ENDIF
	
	INT iDJ
	GLOBAL_PED_DJ_ANIM_DATA blankPedDJAnimData
	REPEAT MAX_PED_ON_STAGE_DJS iDJ
		g_PedDJAnimData[iDJ] = blankPedDJAnimData
	ENDREPEAT
ENDPROC

PROC SET_PED_ACTIVE_STATE(PED_ACTIVE_STATES eActiveState)
	PRINTLN("[AM_MP_PEDS] SET_PED_ACTIVE_STATE - Setting Active State: ", DEBUG_GET_PED_ACTIVE_STATE_STRING(eActiveState))
	LocalData.eActiveState = eActiveState
ENDPROC

PROC SCRIPT_CLEANUP(PED_LOCATIONS ePedLocation)
	CLEANUP_ALL_PED_SYNCED_SCENES(ServerBD.NetworkPedData, LocalData.PedData, ServerBD.iMaxLocalPeds, ServerBD.iMaxNetworkPeds)
	DELETE_ALL_SCRIPT_PEDS(ServerBD, LocalData.PedData)
	UNLOAD_ALL_PED_ASSETS(ePedLocation, ServerBD, LocalData)
	CLEANUP_PED_GLOBALS()
	SET_PED_ACTIVE_STATE(PED_ACTIVE_STATE_INIT)
	PRINTLN("[AM_MP_PEDS] MAINTAIN_PED_SCRIPT_LAUNCHING - Terminating AM_MP_PEDS script")
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

PROC MAINTAIN_PED_RESETTING(PED_LOCATIONS ePedLocation)
	IF g_bResetSelectedPeds		
		IF DELETE_LOCAL_RESETTABLE_SCRIPT_PEDS(LocalData, ServerBD, LocalData.PedData)	
			g_bResetSelectedPeds = FALSE
			INT iPed
			REPEAT ServerBD.iMaxLocalPeds iPed
				RESET_PED_DATA(LocalData.PedData[iPed])
				SET_PED_DATA(ePedLocation, ServerBD, LocalData.PedData[iPed], iPed, ServerBD.iLayout)
			ENDREPEAT
			REPEAT ServerBD.iMaxNetworkPeds iPed
				RESET_PED_DATA(ServerBD.NetworkPedData[iPed].Data)
				SET_PED_DATA(ePedLocation, ServerBD, ServerBD.NetworkPedData[iPed].Data, GET_PED_NETWORK_ID(iPed), ServerBD.iLayout)
			ENDREPEAT
			
			SET_ALL_PED_PATROL_DATA(ePedLocation, ServerBD, LocalData)
			SET_ALL_PED_HEAD_TRACKING_DATA(ePedLocation, ServerBD, LocalData)
			SET_PED_CONTROLLER_SPEECH_DATA(ePedLocation, ServerBD, LocalData)
			SET_ALL_PED_SPEECH_DATA(ePedLocation, ServerBD, LocalData)
			SET_ALL_PED_PARENT_DATA(ePedLocation, ServerBD, LocalData)
			SET_ALL_PED_VFX_DATA(ServerBD, LocalData)
			
			LocalData.iLocalSpeechPed 			= 0
			LocalData.iLocalSpeechTimeOffsetMS 	= 0
			RESET_NET_TIMER(LocalData.stLocalSpeechTimer)
		ENDIF
	ENDIF
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ INITIALISE ╞══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

PROC INITIALISE_PED_GLOBALS()
	g_bTurnOnPeds = TRUE
	g_bInitPedsCreated = FALSE
	g_bResetSelectedPeds = FALSE
ENDPROC

PROC INITIALISE(PED_LOCATIONS ePedLocation)
	INITIALISE_PED_GLOBALS()
	
	IF DOES_ACTIVE_PED_TOTAL_CHANGE(ePedLocation)
		SET_PEDS_BIT(LocalData.iBS, BS_PED_LOCAL_DATA_ACTIVE_PED_TOTAL_CAN_CHANGE)
	ENDIF
ENDPROC

PROC SCRIPT_INITIALISE(SCRIPT_PARENT_DATA ParentData)
	PRINTLN("[AM_MP_PEDS] SCRIPT_INITIALISE - Initialising")
	
	IF GET_CURRENT_GAMEMODE() = GAMEMODE_CREATOR
		NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
		EXIT
	ENDIF
	
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		PRINTLN("[AM_MP_PEDS] SCRIPT_INITIALISE - Not in MP. Terminating script.")
		SCRIPT_CLEANUP(ParentData.ePedLocation)
	ENDIF
	
	WHILE NOT NETWORK_TRY_TO_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE, ParentData.iScriptInstance)
		PRINTLN("[AM_MP_PEDS] SCRIPT_INITIALISE - Waiting to be able to launch")
		WAIT(0)
	ENDWHILE
	
	HANDLE_NET_SCRIPT_INITIALISATION()
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(ServerBD, SIZE_OF(ServerBD))
	
	IF NOT Wait_For_First_Network_Broadcast()
		PRINTLN("[AM_MP_PEDS] SCRIPT_INITIALISE - Failed to receive initial network broadcast. Terminating script.")
		SCRIPT_CLEANUP(ParentData.ePedLocation)
	ENDIF
	
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	PRINTLN("[AM_MP_PEDS] SCRIPT_INITIALISE - Complete")
ENDPROC

PROC INITIALISE_PED_DATA(PEDS_DATA &Data)
	Data.vRotation = <<0.0, 0.0, -1.0>>
	RESET_PED_PACKED_CLOTHING_COMPONENT_VARS(Data.iPackedDrawable, Data.iPackedTexture)
ENDPROC

PROC SET_ALL_PED_DATA(PED_LOCATIONS ePedLocation)
	
	INT iPed
	REPEAT ServerBD.iMaxLocalPeds iPed
		INITIALISE_PED_DATA(LocalData.PedData[iPed])
		SET_PED_DATA(ePedLocation, ServerBD, LocalData.PedData[iPed], iPed, ServerBD.iLayout)
	ENDREPEAT
	
	REPEAT ServerBD.iMaxNetworkPeds iPed
		INITIALISE_PED_DATA(ServerBD.NetworkPedData[iPed].Data)
		SET_PED_DATA(ePedLocation, ServerBD, ServerBD.NetworkPedData[iPed].Data, GET_PED_NETWORK_ID(iPed), ServerBD.iLayout)
	ENDREPEAT
	
ENDPROC

PROC RESERVE_NETWORK_PEDS(PED_LOCATIONS ePedLocation)
	
	IF GET_CURRENT_GAMEMODE() = GAMEMODE_CREATOR
		EXIT
	ENDIF
	
	INT iPed
	INT iProp
	INT iNetworkPropTotal = 0
	PED_PROP_DATA pedPropData
	
	REPEAT ServerBD.iMaxNetworkPeds iPed
		REPEAT MAX_NUM_PED_PROPS iProp
			GET_PED_PROP_DATA(ePedLocation, INT_TO_ENUM(PED_ACTIVITIES, ServerBD.NetworkPedData[iPed].Data.iActivity), iProp, pedPropData)
			IF (IS_MODEL_VALID(pedPropData.ePropModel) AND pedPropData.ePropModel != DUMMY_MODEL_FOR_SCRIPT)
				iNetworkPropTotal++
			ELSE
				BREAKLOOP
			ENDIF
		ENDREPEAT
	ENDREPEAT
	
	IF (ServerBD.iMaxNetworkPeds > 0)
		RESERVE_LOCAL_NETWORK_MISSION_PEDS(GET_NUM_RESERVED_MISSION_PEDS(FALSE, RESERVATION_LOCAL_ONLY) + ServerBD.iMaxNetworkPeds)
	ENDIF
	IF (iNetworkPropTotal > 0)
		RESERVE_LOCAL_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE, RESERVATION_LOCAL_ONLY) + iNetworkPropTotal)
	ENDIF
	PRINTLN("[AM_MP_PEDS] RESERVE_NETWORK_PEDS - Reserving Network Objects - Peds: ", ServerBD.iMaxNetworkPeds, " Props: ", iNetworkPropTotal)
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════╡ PROCESS EVENTS ╞════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

PROC PROCESS_PED_EVENTS(PED_LOCATIONS ePedLocation)
	
	INT iEventID
	EVENT_NAMES ThisScriptEvent
	STRUCT_EVENT_COMMON_DETAILS Details
	
	REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iEventID
	    ThisScriptEvent = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iEventID)
	   	
	    SWITCH ThisScriptEvent
			CASE EVENT_NETWORK_SCRIPT_EVENT
				GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Details, SIZE_OF(Details))
				SWITCH Details.Type
					CASE SCRIPT_EVENT_UPDATE_PED_SPEECH
						IF g_sBlockedEvents.bSCRIPT_EVENT_UPDATE_PED_SPEECH
							EXIT
						ENDIF
						PROCESS_PED_SPEECH_EVENTS(ePedLocation, ServerBD, LocalData, Details.Type, iEventID)
					BREAK
					CASE SCRIPT_EVENT_CLUB_MUSIC_UPDATE_DJ_PED_AFTER_SWITCH_SCENE
						IF g_sBlockedEvents.bSCRIPT_EVENT_CLUB_MUSIC_UPDATE_DJ_PED_AFTER_SWITCH_SCENE
							EXIT
						ENDIF
						IF ePedLocation = PED_LOCATION_MUSIC_STUDIO
							PROCESS_MUSIC_STUDIO_UPDATE_PEDS_AFTER_SWITCH_SCENE(LocalData, iEventID)
						ELSE
							PROCESS_DJ_PED_UPDATE_AFTER_SWITCH_SCENE(LocalData, iEventID)
						ENDIF
					BREAK
					CASE SCRIPT_EVENT_MUSIC_STUDIO_UPDATE_PEDS_AFTER_SWITCH_SCENE
						IF g_sBlockedEvents.bSCRIPT_EVENT_MUSIC_STUDIO_UPDATE_PEDS_AFTER_SWITCH_SCENE
							EXIT
						ENDIF
						PRINTLN("PROCESS_MUSIC_STUDIO_UPDATE_PEDS_AFTER_SWITCH_SCENE")
						PROCESS_MUSIC_STUDIO_UPDATE_PEDS_AFTER_SWITCH_SCENE(LocalData, iEventID)
					BREAK
					CASE SCRIPT_EVENT_SET_PED_FADE_OUT
						IF g_sBlockedEvents.bSCRIPT_EVENT_SET_PED_FADE_OUT
							EXIT
						ENDIF
						PROCESS_PED_FADE_EVENTS(LocalData, Details.Type, iEventID)
					BREAK
					CASE SCRIPT_EVENT_SET_PED_FADE_OUT_FOR_OTHER_PLAYERS
						IF g_sBlockedEvents.bSCRIPT_EVENT_SET_PED_FADE_OUT_FOR_OTHER_PLAYERS
							EXIT
						ENDIF
						PROCESS_PED_FADE_EVENTS(LocalData, Details.Type, iEventID)
					BREAK
					CASE SCRIPT_EVENT_LAUNCH_CUSTOM_PED_UPDATE_ACTION
						IF g_sBlockedEvents.bSCRIPT_EVENT_LAUNCH_CUSTOM_PED_UPDATE_ACTION
							EXIT
						ENDIF
						PROCESS_LAUNCH_CUSTOM_PED_UPDATE_ACTION_EVENTS(LocalData, Details.Type, iEventID)
					BREAK
					#IF IS_DEBUG_BUILD
					CASE SCRIPT_EVENT_SET_MAX_LOCAL_PEDS
						IF g_sBlockedEvents.bSCRIPT_EVENT_SET_MAX_LOCAL_PEDS
							EXIT
						ENDIF
						PROCESS_PED_DEBUG_EVENTS(ePedLocation, DebugData, ServerBD, LocalData, Details.Type, iEventID)
					BREAK
					CASE SCRIPT_EVENT_DEBUG_CLUB_MUSIC_RESET_TRACK
						IF g_sBlockedEvents.bSCRIPT_EVENT_DEBUG_CLUB_MUSIC_RESET_TRACK
							EXIT
						ENDIF
						PROCESS_PED_DEBUG_EVENTS(ePedLocation, DebugData, ServerBD, LocalData, Details.Type, iEventID)
					BREAK
					#ENDIF
				ENDSWITCH
			BREAK
		ENDSWITCH
	ENDREPEAT
	
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ MAINTAIN ╞═══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

FUNC INT GET_NETWORK_PED_PROCESS_TOTAL()
	IF (ServerBD.iMaxNetworkPeds < g_iMaxNetworkPedsProcessedPerFrame)
		RETURN ServerBD.iMaxNetworkPeds
	ENDIF
	RETURN g_iMaxNetworkPedsProcessedPerFrame
ENDFUNC

FUNC INT GET_PED_PROCESS_TOTAL()
	IF (ServerBD.iMaxLocalPeds < g_iMaxPedsProcessedPerFrame)
		RETURN ServerBD.iMaxLocalPeds
	ENDIF
	RETURN g_iMaxPedsProcessedPerFrame
ENDFUNC

PROC RESET_PED_PROCESS_ARRAY()
	// Reset all elements in array to -1 except the inital high priority peds
	INT iSlot
	FOR iSlot = LocalData.iHighPriorityPeds TO GET_PED_PROCESS_TOTAL()-1
		LocalData.iPedsProcessArray[iSlot] = -1
	ENDFOR
ENDPROC

FUNC BOOL IS_SAFE_TO_PROCESS_PED(INT iPedID, BOOL bNetworkedPed = FALSE)
	
	// Don't process ped if debug testing with
	#IF IS_DEBUG_BUILD
	IF (DebugData.AnimData.bEnableAnimDebug AND iPedID = DebugData.AnimData.iAnimPed)
		RETURN FALSE
	ENDIF
	
	INT iChildPed
	REPEAT MAX_NUM_TOTAL_CHILD_PEDS iChildPed
		IF (DebugData.AnimData.bEnableAnimDebug AND iPedID = DebugData.AnimData.ChildPedData[iChildPed].iChildPedID)
			RETURN FALSE
		ENDIF
	ENDREPEAT
	#ENDIF
	
	// Don't process again if high priority ped (already in the array - added on initialise)
	IF (bNetworkedPed)
		IF IS_PEDS_BIT_SET(ServerBD.NetworkPedData[GET_PED_NETWORK_ARRAY_ID(iPedID)].Data.iBS, BS_PED_DATA_HIGH_PRIORITY_PED)
			RETURN FALSE
		ENDIF
	ELSE
		IF IS_PEDS_BIT_SET(LocalData.PedData[iPedID].iBS, BS_PED_DATA_HIGH_PRIORITY_PED)
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC POPULATE_PED_PROCESS_ARRAY()
	LocalData.iPedsProcessArrayID = LocalData.iHighPriorityPeds
	
	INT iCount
	FOR iCount = LocalData.iHighPriorityPeds TO GET_PED_PROCESS_TOTAL()-1
		
		// This loop grabs the next g_iMaxPedsProcessedPerFrame amount of peds and puts them in an array to process.
		// Any high priority peds are added to the array first in INITIALISE_PED_PROCESS_ARRAY().
		
		// iPedsProcessID keeps track of the current ped being processed.
		IF (LocalData.iPedsProcessID >= ServerBD.iMaxLocalPeds)
			LocalData.iPedsProcessID = 0
		ENDIF
		
		IF IS_SAFE_TO_PROCESS_PED(LocalData.iPedsProcessID)
			LocalData.iPedsProcessArray[LocalData.iPedsProcessArrayID] = LocalData.iPedsProcessID
			LocalData.iPedsProcessArrayID++
		ELSE
			// Reuse this slot to grab next safe ped
			iCount--
		ENDIF
		
		LocalData.iPedsProcessID++
	ENDFOR
	
ENDPROC

FUNC INT GET_PLAYER_COUNT()
	
	INT iPlayerCount
	INT iParticipant
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
			iPlayerCount++
		ENDIF
	ENDREPEAT
	
	RETURN iPlayerCount
ENDFUNC

PROC MAINTAIN_LOCAL_PED_ACTIVE_TOTAL(PED_LOCATIONS ePedLocation)
	
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
	OR NOT IS_PEDS_BIT_SET(LocalData.iBS, BS_PED_LOCAL_DATA_ACTIVE_PED_TOTAL_CAN_CHANGE)
	OR NOT PROCESS_FUNCTION_THIS_FRAME(MAX_NUM_ACTIVE_TOTAL_PEDS_PROCESSING_FRAMES)
		EXIT
	ENDIF
	
	INT iPlayerCount = GET_PLAYER_COUNT()
	
	INT iLevel
	INT iThreshold
	
	
	REPEAT MAX_NUM_PED_LEVELS iLevel
		iThreshold = GET_ACTIVE_PED_LEVEL_THRESHOLD(ePedLocation, iLevel)
		IF (iThreshold = -1)
			BREAKLOOP
		ELSE
			IF (iPlayerCount > iThreshold)
				IF (ServerBD.iLevel != iLevel)
					ServerBD.iLevel = iLevel
					PRINTLN("[AM_MP_PEDS] MAINTAIN_LOCAL_PED_ACTIVE_TOTAL - Updating Level: ", iLevel)
				ENDIF
				BREAKLOOP
			ENDIF
		ENDIF
	ENDREPEAT
		
ENDPROC

FUNC BOOL IS_SAFE_TO_CREATE_LOCAL_PED(INT iPedID)
	IF IS_PEDS_BIT_SET(LocalData.iBS, BS_PED_LOCAL_DATA_DJ_SWITCH_ACTIVE)
	AND NOT g_clubMusicData.bHasNextDJDataFileLoaded
		RETURN FALSE
	ENDIF
	
	IF NOT g_bInitPedsCreated																			// Initial peds have not been created
	OR NOT IS_SPHERE_VISIBLE(LocalData.PedData[iPedID].vPosition, g_fMAX_RECREATE_PED_SPHERE_RADIUS)	// Ped is not visible on gameplay cam
	#IF FEATURE_FIXER
	OR IS_SCREEN_FADED_OUT()
	#ENDIF
	#IF IS_DEBUG_BUILD
	OR DebugData.GeneralData.bEnableDebug
	OR DebugData.PerformanceData.bRunningDebugPedLocation
	#ENDIF
	OR g_clubMusicData.bHasNextDJDataFileLoaded
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_CONTROL_OF_NETWORK_CHILD_PEDS(INT iPedID)
	
	IF NOT IS_PEDS_BIT_SET(ServerBD.NetworkPedData[iPedID].Data.iBS, BS_PED_DATA_PARENT_PED)
	OR IS_PEDS_BIT_SET(ServerBD.NetworkPedData[iPedID].Data.iBS, BS_PED_DATA_PARTNER_PED)
	OR (ServerBD.iParentPedID[iPedID] = -1)
		RETURN TRUE
	ENDIF
	
	INT iChild
	INT iChildPedID
	INT iMaxChildPeds = 0
	INT iControlChildPeds = 0
	
	REPEAT MAX_NUM_TOTAL_CHILD_PEDS iChild
		iChildPedID = ServerBD.ParentPedData[ServerBD.iParentPedID[iPedID]].iChildID[iChild]
		IF (iChildPedID != -1)
			IF DOES_ENTITY_EXIST(ServerBD.NetworkPedData[iChildPedID].Data.PedID)
				IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(ServerBD.NetworkPedData[iChildPedID].Data.PedID)
					NETWORK_REQUEST_CONTROL_OF_ENTITY(ServerBD.NetworkPedData[iChildPedID].Data.PedID)
				ELSE
					SET_NETWORK_ID_CAN_MIGRATE(ServerBD.NetworkPedData[iChildPedID].niPedID, FALSE)
					iControlChildPeds++
				ENDIF
				iMaxChildPeds++
			ELSE
				BREAKLOOP
			ENDIF
		ENDIF
	ENDREPEAT	
			
	RETURN (iControlChildPeds = iMaxChildPeds)
ENDFUNC

FUNC BOOL HAS_CONTROL_OF_NETWORK_PED_PROPS(INT iPedID)
	
	INT iProp
	INT iMaxProps = 0
	INT iPropControl = 0
	
	REPEAT MAX_NUM_PED_PROPS iProp
		IF DOES_ENTITY_EXIST(ServerBD.NetworkPedData[iPedID].Data.PropID[iProp])
			IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(ServerBD.NetworkPedData[iPedID].Data.PropID[iProp])
				NETWORK_REQUEST_CONTROL_OF_ENTITY(ServerBD.NetworkPedData[iPedID].Data.PropID[iProp])
			ELSE
				SET_NETWORK_ID_CAN_MIGRATE(ServerBD.NetworkPedData[iPedID].niPropID[iProp], FALSE)
				iPropControl++
			ENDIF
			iMaxProps++
		ELSE
			BREAKLOOP
		ENDIF
	ENDREPEAT
	
	RETURN (iPropControl = iMaxProps)
ENDFUNC

FUNC BOOL HAS_CONTROL_OF_NETWORK_PED(INT iPedID)
	
	IF IS_ENTITY_ALIVE(ServerBD.NetworkPedData[iPedID].Data.PedID)
		IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(ServerBD.NetworkPedData[iPedID].Data.PedID)
			NETWORK_REQUEST_CONTROL_OF_ENTITY(ServerBD.NetworkPedData[iPedID].Data.PedID)
		ELSE
			SET_NETWORK_ID_CAN_MIGRATE(ServerBD.NetworkPedData[iPedID].niPedID, FALSE) 
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL REQUEST_CONTROL_OF_NETWORK_PED_WRAPPER(INT iPedID)
	
	BOOL bPedControl 			= FALSE
	BOOL bPedPropControl 		= FALSE
	BOOL bChildPedsControl		= FALSE
	
	IF HAS_CONTROL_OF_NETWORK_PED(iPedID)
		bPedControl = TRUE
	ENDIF
	
	IF HAS_CONTROL_OF_NETWORK_PED_PROPS(iPedID)
		bPedPropControl = TRUE
	ENDIF
	
	IF HAS_CONTROL_OF_NETWORK_CHILD_PEDS(iPedID)
		bChildPedsControl = TRUE
	ENDIF

	RETURN (bPedControl AND bPedPropControl AND bChildPedsControl)
ENDFUNC

FUNC BOOL REQUEST_CONTROL_OF_NETWORK_PROPS_WRAPPER(INT iPedID)

	BOOL bPedPropControl 		= FALSE
		
	IF HAS_CONTROL_OF_NETWORK_PED_PROPS(iPedID)
		bPedPropControl = TRUE
	ENDIF

	RETURN bPedPropControl
ENDFUNC

PROC UPDATE_NETWORK_PEDS(SCRIPT_PARENT_DATA ParentData)
	
	IF g_bForceMigrateWhenClientControlMusicStudio
	AND IS_PLAYER_IN_MUSIC_STUDIO(PLAYER_ID())
		INT iPed
		INT iProp
		IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT() 
		AND ServerBD.iMaxNetworkPeds > 0
			REPEAT ServerBD.iMaxNetworkPeds iPed
				// Let network ped migrate
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(ServerBD.NetworkPedData[iPed].niPedID)
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(ServerBD.NetworkPedData[iPed].niPedID)
						SET_NETWORK_ID_CAN_MIGRATE(ServerBD.NetworkPedData[iPed].niPedID, TRUE)
						//PRINTLN("[AM_MP_PEDS] UPDATE_NETWORK_PEDS non host with control - Setting network ped: ", iPed, " as migratable")
					ENDIF
				ENDIF
				
				// Let network props migrate
				REPEAT MAX_NUM_PED_PROPS iProp
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(ServerBD.NetworkPedData[iPed].niPropID[iProp])
						IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(ServerBD.NetworkPedData[iPed].niPropID[iProp])
							SET_NETWORK_ID_CAN_MIGRATE(ServerBD.NetworkPedData[iPed].niPropID[iProp], TRUE)
							//PRINTLN("[AM_MP_PEDS] UPDATE_NETWORK_PEDS non host with control Setting network ped: ", iPed, " Prop: ", iProp, " as migratable")
						ENDIF
					ENDIF
				ENDREPEAT
			ENDREPEAT
		ENDIF
	ENDIF
	
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT() 
	OR (ServerBD.iMaxNetworkPeds = 0)
		EXIT
	ENDIF
	
	// Description:
	// Process GET_NETWORK_PED_PROCESS_TOTAL networked ped per frame. Networked peds are staggered using LocalData.iStaggeredNetworkPedID
	//
	// A large number of functions use ped IDs to populate the correct data. This is problematic for network peds as
	// they are stored in an array of MAX_NUM_TOTAL_NETWORK_PEDS, slots 0-x. Local peds use an array of 125, slots 0-124.
	// Therefore we need a way to determine which 0-x peds are being queried, local or networked. To avoid duplicated
	// functions with different ped ID lookups, the networked peds use hard coded CONST INT IDs, index 125-134. 
	//
	// The function GET_PED_NETWORK_ID() converts the networked peds array ID (0-9) into their ped ID (125-134).
	// The function GET_PED_NETWORK_ARRAY_ID() converts the ped ID (125-134) to the networked ped array ID (0-9).
	
	INT iCount
	INT iPedID
	INT iPropID
	REPEAT GET_NETWORK_PED_PROCESS_TOTAL() iCount
		iPedID = GET_PED_NETWORK_ID(LocalData.iStaggeredNetworkPedID)
		
		IF SHOULD_PED_BE_CREATED(ServerBD.NetworkPedData[LocalData.iStaggeredNetworkPedID].Data.iBS, ServerBD.NetworkPedData[LocalData.iStaggeredNetworkPedID].Data.iLevel, ServerBD.iLevel)
			IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(ServerBD.NetworkPedData[LocalData.iStaggeredNetworkPedID].niPedID)
				CREATE_NETWORK_SCRIPT_PED(ParentData.ePedLocation, ServerBD.NetworkPedData[LocalData.iStaggeredNetworkPedID], iPedID, ServerBD.iLayout, LocalData.DancingData.eMusicIntensity, ServerBD.NetworkPedData[LocalData.iStaggeredNetworkPedID].Data.dancingData.ePedMusicIntensity, IS_PEDS_BIT_SET(ServerBD.NetworkPedData[LocalData.iStaggeredNetworkPedID].Data.iBS, BS_PED_DATA_USE_DANCING_TRANSITION))
			ENDIF
		ELIF IS_PEDS_BIT_SET(ServerBD.NetworkPedData[LocalData.iStaggeredNetworkPedID].Data.iBS, BS_PED_DATA_PROPS_ONLY_NO_PED)	
		AND NOT IS_PEDS_BIT_SET(ServerBD.NetworkPedData[LocalData.iStaggeredNetworkPedID].Data.iBS, BS_PED_DATA_SKIP_PED)
			IF NOT ARE_NETWORK_SCRIPT_PED_PROPS_CREATED(ParentData.ePedLocation, ServerBD.NetworkPedData[LocalData.iStaggeredNetworkPedID])
				CREATE_NETWORK_SCRIPT_NO_PED_ONLY_PROPS(ParentData.ePedLocation, ServerBD.NetworkPedData[LocalData.iStaggeredNetworkPedID], iPedID, LocalData.DancingData.eMusicIntensity, ServerBD.NetworkPedData[LocalData.iStaggeredNetworkPedID].Data.dancingData.ePedMusicIntensity, IS_PEDS_BIT_SET(ServerBD.NetworkPedData[LocalData.iStaggeredNetworkPedID].Data.iBS, BS_PED_DATA_USE_DANCING_TRANSITION))
			ENDIF
		ENDIF
		
		// Regrab ped and prop indexes in case host migrates
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(ServerBD.NetworkPedData[LocalData.iStaggeredNetworkPedID].niPedID)
			ServerBD.NetworkPedData[LocalData.iStaggeredNetworkPedID].Data.PedID = GET_PED_INDEX_FROM_ENTITY_INDEX(NETWORK_GET_ENTITY_FROM_NETWORK_ID(ServerBD.NetworkPedData[LocalData.iStaggeredNetworkPedID].niPedID))
		ENDIF
		
		REPEAT MAX_NUM_NETWORKED_SYNCED_SCENE_PED_PROPS iPropID
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(ServerBD.NetworkPedData[LocalData.iStaggeredNetworkPedID].niPropID[iPropID])
				ServerBD.NetworkPedData[LocalData.iStaggeredNetworkPedID].Data.PropID[iPropID] = GET_OBJECT_INDEX_FROM_ENTITY_INDEX(NETWORK_GET_ENTITY_FROM_NETWORK_ID(ServerBD.NetworkPedData[LocalData.iStaggeredNetworkPedID].niPropID[iPropID]))
			ELSE
				BREAKLOOP
			ENDIF
		ENDREPEAT
		
		IF (GET_CURRENT_GAMEMODE() != GAMEMODE_CREATOR)
		AND REQUEST_CONTROL_OF_NETWORK_PED_WRAPPER(LocalData.iStaggeredNetworkPedID)
			UPDATE_PED_DANCING_TRANSITION(ParentData.ePedLocation, ServerBD.NetworkPedData[LocalData.iStaggeredNetworkPedID].Data, LocalData.DancingData.eMusicIntensity)
			UPDATE_PED_ANIMATION(ParentData.ePedLocation, ServerBD, LocalData, ServerBD.NetworkPedData[LocalData.iStaggeredNetworkPedID].Data, PedAnimData, LocalData.iStaggeredNetworkPedID, LocalData.DancingData.eNextMusicIntensity, LocalData.DancingData.eMusicIntensity, ServerBD.NetworkPedData[LocalData.iStaggeredNetworkPedID].Data.dancingData.ePedMusicIntensity, IS_PEDS_BIT_SET(ServerBD.NetworkPedData[LocalData.iStaggeredNetworkPedID].Data.iBS, BS_PED_DATA_USE_DANCING_TRANSITION), TRUE, IS_PEDS_BIT_SET(ServerBD.NetworkPedData[LocalData.iStaggeredNetworkPedID].Data.iBS, BS_PED_DATA_USE_PED_TRANSITION))
			UPDATE_NETWORK_PED_HEAD_TRACKING(ServerBD, LocalData.iStaggeredNetworkPedID)
			UPDATE_NETWORK_PED_PATROL(ParentData.ePedLocation, ServerBD, LocalData, LocalData.iStaggeredNetworkPedID)
			
		//MAINTAIN VFX ON PROPS WITHOUT PED
		ELIF IS_PEDS_BIT_SET(ServerBD.NetworkPedData[LocalData.iStaggeredNetworkPedID].Data.iBS, BS_PED_DATA_PROPS_ONLY_NO_PED)	
		AND	REQUEST_CONTROL_OF_NETWORK_PROPS_WRAPPER(LocalData.iStaggeredNetworkPedID)	
			MAINTAIN_NETWORK_PED_VFX(ParentData.ePedLocation, ServerBD, PedAnimData, LocalData.iStaggeredNetworkPedID, ServerBD.NetworkPedData[LocalData.iStaggeredNetworkPedID].Data.animData.iCurrentClip, LocalData.DancingData.eNextMusicIntensity, LocalData.DancingData.eMusicIntensity, IS_PEDS_BIT_SET(ServerBD.NetworkPedData[LocalData.iStaggeredNetworkPedID].Data.iBS, BS_PED_DATA_USE_DANCING_TRANSITION))		
		ENDIF
		
		// Reset ped ID stagger
		LocalData.iStaggeredNetworkPedID++
		IF (LocalData.iStaggeredNetworkPedID >= ServerBD.iMaxNetworkPeds)
			LocalData.iStaggeredNetworkPedID = 0
		ENDIF
	ENDREPEAT
	
	SET_PEDS_CREATED_FLAG(ParentData.ePedLocation, ServerBD, LocalData.PedData)
	
ENDPROC
PROC UPDATE_PEDS_CAPSULE_SIZES()
	INT iPed
	REPEAT MAX_NUM_LOCAL_CHANGED_CAPSULE_SIZE_PEDS iPed
	
		IF LocalData.ChangedCapsuleData[iPed].iPedID = -1
			BREAKLOOP
		ENDIF
		IF IS_ENTITY_ALIVE(LocalData.PedData[LocalData.ChangedCapsuleData[iPed].iPedID].PedID)
			UPDATE_LOCAL_PED_CAPSULE_SIZE(LocalData, LocalData.ChangedCapsuleData[iPed].iPedID)
		ENDIF
	ENDREPEAT
ENDPROC

PROC UPDATE_LOCAL_PEDS(SCRIPT_PARENT_DATA ParentData)
	
	IF (ServerBD.iMaxLocalPeds = 0)
		EXIT
	ENDIF
	
	// Host maintains the active number of local peds based on player count
	MAINTAIN_LOCAL_PED_ACTIVE_TOTAL(ParentData.ePedLocation)
	
	// Reset elements in the peds processing array to -1
	RESET_PED_PROCESS_ARRAY()
	
	// Populate the array with the next load of peds
	POPULATE_PED_PROCESS_ARRAY()
	
	// Used to check if a forced animation update has been done this frame
	CLEAR_PEDS_BIT(LocalData.iBS, BS_PED_LOCAL_DATA_FORCED_ANIM_UPDATE_THIS_FRAME)
	
	INT iPed
	REPEAT GET_PED_PROCESS_TOTAL() iPed
		// Now update all the ped IDs inside the array that are valid
		IF (LocalData.iPedsProcessArray[iPed] != -1)
			
			IF SHOULD_PED_BE_CREATED(LocalData.PedData[LocalData.iPedsProcessArray[iPed]].iBS, LocalData.PedData[LocalData.iPedsProcessArray[iPed]].iLevel, ServerBD.iLevel)
				IF NOT DOES_ENTITY_EXIST(LocalData.PedData[LocalData.iPedsProcessArray[iPed]].PedID)
					IF IS_SAFE_TO_CREATE_LOCAL_PED(LocalData.iPedsProcessArray[iPed])
					OR IS_PEDS_BIT_SET(LocalData.PedData[LocalData.iPedsProcessArray[iPed]].iBS, BS_PED_DATA_FADE_IN_PED)
						CREATE_LOCAL_SCRIPT_PED(ParentData.ePedLocation, LocalData, LocalData.PedData[LocalData.iPedsProcessArray[iPed]], ServerBD.iLayout, LocalData.iPedsProcessArray[iPed], LocalData.iPedsCreatedPerFrame, LocalData.DancingData.eMusicIntensity, LocalData.PedData[LocalData.iPedsProcessArray[iPed]].dancingData.ePedMusicIntensity, IS_PEDS_BIT_SET(LocalData.PedData[LocalData.iPedsProcessArray[iPed]].iBS, BS_PED_DATA_USE_DANCING_TRANSITION))
					ENDIF
				ENDIF
			ELSE
				IF DOES_ENTITY_EXIST(LocalData.PedData[LocalData.iPedsProcessArray[iPed]].PedID)
					IF IS_PEDS_BIT_SET(LocalData.PedData[LocalData.iPedsProcessArray[iPed]].iBS, BS_PED_DATA_FADE_PED)
						PERFORM_LOCAL_PED_FADE(LocalData.PedData[LocalData.iPedsProcessArray[iPed]], FALSE)
						IF NOT IS_ENTITY_ON_SCREEN(LocalData.PedData[LocalData.iPedsProcessArray[iPed]].PedID)
						#IF FEATURE_FIXER
						OR g_bIsMocapPlaying
						#ENDIF
						OR IS_PEDS_BIT_SET(LocalData.PedData[LocalData.iPedsProcessArray[iPed]].iBS, BS_PED_DATA_FADE_PED_COMPLETE)
							DELETE_LOCAL_SCRIPT_PED_UPDATE(LocalData.PedData[LocalData.iPedsProcessArray[iPed]], LocalData.iPedsDeletedPerFrame, LocalData.iPedsProcessArray[iPed])
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_PEDS_BIT_SET(LocalData.PedData[LocalData.iPedsProcessArray[iPed]].iBS, BS_PED_DATA_FADE_IN_PED)
			AND HAS_PED_BEEN_CREATED(ParentData.ePedLocation, LocalData.PedData[LocalData.iPedsProcessArray[iPed]], 2)
				PERFORM_LOCAL_PED_FADE(LocalData.PedData[LocalData.iPedsProcessArray[iPed]], TRUE)
			ENDIF
			
			//UPDATE_PED_CULLING(ParentData.ePedLocation, LocalData.PedData[LocalData.iPedsProcessArray[iPed]], LocalData.eCullingArea, LocalData.LoiterData, LocalData.iPedsProcessArray[iPed], ServerBD.iLayout #IF IS_DEBUG_BUILD, DebugData #ENDIF)
			//IF NOT IS_PED_CULLING_ACTIVE(LocalData.PedData[LocalData.iPedsProcessArray[iPed]].iBS)
			
			IF (GET_CURRENT_GAMEMODE() != GAMEMODE_CREATOR)
			AND IS_ENTITY_ALIVE(LocalData.PedData[LocalData.iPedsProcessArray[iPed]].PedID)
			AND NOT IS_PEDS_BIT_SET(LocalData.PedData[LocalData.iPedsProcessArray[iPed]].iBS, BS_PED_DATA_PERFORMING_CUSTOM_TASK)
				UPDATE_PED_DANCING_TRANSITION(ParentData.ePedLocation, LocalData.PedData[LocalData.iPedsProcessArray[iPed]], LocalData.DancingData.eMusicIntensity)
				UPDATE_PED_ANIMATION(ParentData.ePedLocation, ServerBD, LocalData, LocalData.PedData[LocalData.iPedsProcessArray[iPed]], PedAnimData, LocalData.iPedsProcessArray[iPed], LocalData.DancingData.eNextMusicIntensity, LocalData.DancingData.eMusicIntensity, LocalData.PedData[LocalData.iPedsProcessArray[iPed]].dancingData.ePedMusicIntensity, IS_PEDS_BIT_SET(LocalData.PedData[LocalData.iPedsProcessArray[iPed]].iBS, BS_PED_DATA_USE_DANCING_TRANSITION), FALSE, IS_PEDS_BIT_SET(LocalData.PedData[LocalData.iPedsProcessArray[iPed]].iBS, BS_PED_DATA_USE_PED_TRANSITION))
				UPDATE_LOCAL_PED_HEAD_TRACKING(ParentData.ePedLocation, LocalData, LocalData.iPedsProcessArray[iPed])
				UPDATE_LOCAL_PED_PATROL(ParentData.ePedLocation, LocalData, LocalData.iPedsProcessArray[iPed])
			ENDIF
			
		ELSE
			BREAKLOOP
		ENDIF
		
	ENDREPEAT
	
	SET_PEDS_CREATED_FLAG(ParentData.ePedLocation, ServerBD, LocalData.PedData)
	LocalData.iPedsCreatedPerFrame = 0	// Reset for next frame
	LocalData.iPedsDeletedPerFrame = 0	// Reset for next frame
	
	//UPDATE_PED_CULLING_LOCATES(ParentData.ePedLocation, LocalData.eCullingArea)
	
ENDPROC

PROC SET_PLAYER_PED_SPEECH_STAGGER_INDEX(INT &iPlayerSpeechStagger, INT iTotalPlayerSpeechPeds, BOOL bNetworkPlayerSpeech = FALSE)
	
	iPlayerSpeechStagger++
	IF (iPlayerSpeechStagger >= iTotalPlayerSpeechPeds)
		iPlayerSpeechStagger = 0
	ELSE
		IF (bNetworkPlayerSpeech)
			IF (LocalData.NetworkSpeechData[iPlayerSpeechStagger].iPedID = -1)
				iPlayerSpeechStagger = 0
			ENDIF
		ELSE
			IF (LocalData.LocalSpeechData[iPlayerSpeechStagger].iPedID = -1)
				iPlayerSpeechStagger = 0
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC UPDATE_PLAYER_PED_SPEECH(SCRIPT_PARENT_DATA ParentData)
	
	IF NOT PROCESS_FUNCTION_THIS_FRAME(MAX_NUM_PLAYER_SPEECH_PROCESSING_FRAMES)
		EXIT
	ENDIF
	
	IF (LocalData.NetworkSpeechData[LocalData.iNetworkPlayerSpeechPedID].iPedID != -1)
		UPDATE_NETWORK_PED_PLAYER_SPEECH(ParentData.ePedLocation, ServerBD, LocalData, LocalData.NetworkSpeechData[LocalData.iNetworkPlayerSpeechPedID].iPedID, LocalData.LoiterData #IF IS_DEBUG_BUILD, DebugData #ENDIF)
		SET_PLAYER_PED_SPEECH_STAGGER_INDEX(LocalData.iNetworkPlayerSpeechPedID, MAX_NUM_NETWORK_PLAYER_SPEECH_PEDS, TRUE)
	ELSE
		LocalData.iNetworkPlayerSpeechPedID = 0
	ENDIF
	
	IF (LocalData.LocalSpeechData[LocalData.iLocalPlayerSpeechPedID].iPedID != -1)
		UPDATE_LOCAL_PED_PLAYER_SPEECH(ParentData.ePedLocation, LocalData, LocalData.LocalSpeechData[LocalData.iLocalPlayerSpeechPedID].iPedID, LocalData.LoiterData #IF IS_DEBUG_BUILD, DebugData #ENDIF)
		SET_PLAYER_PED_SPEECH_STAGGER_INDEX(LocalData.iLocalPlayerSpeechPedID, MAX_NUM_LOCAL_PLAYER_SPEECH_PEDS)
	ELSE
		LocalData.iLocalPlayerSpeechPedID = 0
	ENDIF
	
ENDPROC

PROC UPDATE_CONTROLLER_PED_SPEECH(SCRIPT_PARENT_DATA ParentData)
	IF NOT HAVE_ALL_PEDS_BEEN_CREATED()
		EXIT
	ENDIF
	
	IF DO_ANY_LOCAL_PEDS_PLAY_CONTROLLER_TRIGGERED_SPEECH(LocalData)
		UPDATE_LOCAL_PED_CONTROLLER_SPEECH(ParentData.ePedLocation, LocalData #IF IS_DEBUG_BUILD, DebugData #ENDIF)
	ENDIF
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
	AND DO_ANY_NETWORK_PEDS_PLAY_CONTROLLER_TRIGGERED_SPEECH(LocalData)
		UPDATE_NETWORK_PED_CONTROLLER_SPEECH(ParentData.ePedLocation, ServerBD, LocalData #IF IS_DEBUG_BUILD, DebugData #ENDIF)
	ENDIF
ENDPROC

PROC UPDATE_CUTSCENE_PEDS(SCRIPT_PARENT_DATA ParentData)
	IF NETWORK_IS_IN_MP_CUTSCENE()
	OR IS_CUTSCENE_RUNNING_FOR_PED_LOCATION(ParentData.ePedLocation)
	#IF IS_DEBUG_BUILD
	OR g_bDebugHideCutsceneAssets
	#ENDIF
		IF NOT IS_PEDS_BIT_SET(LocalData.iBS, BS_PED_LOCAL_DATA_HIDE_ALL_PEDS)
			IF SET_ALL_SCRIPT_PEDS_VISIBILITY(ParentData.ePedLocation, ServerBD, LocalData, FALSE, TRUE)
				SET_PEDS_BIT(LocalData.iBS, BS_PED_LOCAL_DATA_HIDE_ALL_PEDS)
			ENDIF
		ENDIF
		HIDE_NETWORK_PEDS_DURING_CUTSCENE(ServerBD)
	ELSE
		IF IS_PEDS_BIT_SET(LocalData.iBS, BS_PED_LOCAL_DATA_HIDE_ALL_PEDS)
			IF SET_ALL_SCRIPT_PEDS_VISIBILITY(ParentData.ePedLocation, ServerBD, LocalData, TRUE, TRUE)
				CLEAR_PEDS_BIT(LocalData.iBS, BS_PED_LOCAL_DATA_HIDE_ALL_PEDS)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════════╡ DEBUG ╞═════════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

#IF IS_DEBUG_BUILD
PROC CREATE_WIDGETS(SCRIPT_PARENT_DATA ParentData)
	INITIALISE_PED_DEBUG_DATA(DebugData, ServerBD, LocalData)
	START_WIDGET_GROUP("AM_MP_PEDS")
		CREATE_PED_DEBUG_WIDGETS(ParentData.ePedLocation, DebugData, ServerBD, LocalData)
	STOP_WIDGET_GROUP()
	
	#IF SCRIPT_PROFILER_ACTIVE
	CREATE_SCRIPT_PROFILER_WIDGET() 
	#ENDIF
ENDPROC
#ENDIF

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════╡ SCRIPT MAINTAIN ╞════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

FUNC BOOL SHOULD_PED_SCRIPT_TERMINATE(SCRIPT_PARENT_DATA ParentData)
	BOOL bTerminate = FALSE
	
	IF GET_CURRENT_GAMEMODE() != GAMEMODE_CREATOR
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			PRINTLN("[AM_MP_PEDS] SHOULD_PED_SCRIPT_TERMINATE - Func SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE is returning TRUE")
			bTerminate = TRUE
		ENDIF
		
		IF IS_PARENT_A_SIMPLE_INTERIOR(ParentData.ePedLocation)
			IF NOT IS_PLAYER_IN_PARENT_PROPERTY(ParentData.ePedLocation, PLAYER_ID())
			AND NOT IS_PLAYER_WALKING_INTO_SIMPLE_INTERIOR(PLAYER_ID())
			AND NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBS, BS_SIMPLE_INTERIOR_GLOBAL_PLAYER_BD_SPAWNING_INTO_SIMPLE_INTERIOR)
				PRINTLN("[AM_MP_PEDS] SHOULD_PED_SCRIPT_TERMINATE - Player is no longer inside the simple interior parent property")
				bTerminate = TRUE
			ENDIF
		ELSE
			IF NOT IS_PLAYER_IN_PARENT_PROPERTY(ParentData.ePedLocation, PLAYER_ID())
				PRINTLN("[AM_MP_PEDS] SHOULD_PED_SCRIPT_TERMINATE - Player is no longer inside the parent property")
				bTerminate = TRUE
			ENDIF
		ENDIF
	ENDIF
		
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(ParentData.iParentScriptNameHash) < 1
		PRINTLN("[AM_MP_PEDS] SHOULD_PED_SCRIPT_TERMINATE - Parent script has terminated")
		bTerminate = TRUE
	ENDIF
	
	IF NETWORK_IS_ACTIVITY_SESSION()
		PRINTLN("[AM_MP_PEDS] SHOULD_PED_SCRIPT_TERMINATE - Native NETWORK_IS_ACTIVITY_SESSION is returning TRUE")
		bTerminate = TRUE
	ENDIF
	
	IF (g_bTurnOnPeds = FALSE)
		PRINTLN("[AM_MP_PEDS] SHOULD_PED_SCRIPT_TERMINATE - Global g_bTurnOnPeds is set to FALSE")
		bTerminate = TRUE
	ENDIF
	
	RETURN bTerminate
ENDFUNC

SCRIPT(SCRIPT_PARENT_DATA ParentData)
	
	SCRIPT_INITIALISE(ParentData)
	
	INITIALISE(ParentData.ePedLocation)
	SET_PED_SERVER_DATA(ParentData.ePedLocation, ServerBD)
	SET_ALL_PED_DATA(ParentData.ePedLocation)
	SET_PED_CONTROLLER_SPEECH_DATA(ParentData.ePedLocation, ServerBD, LocalData)
	SET_ALL_PED_SPEECH_DATA(ParentData.ePedLocation, ServerBD, LocalData)
	SET_ALL_PED_PATROL_DATA(ParentData.ePedLocation, ServerBD, LocalData)
	SET_ALL_PED_HEAD_TRACKING_DATA(ParentData.ePedLocation, ServerBD, LocalData)
	SET_ALL_PED_CHANGED_CAPSULE_SIZE_DATA(ParentData.ePedLocation, ServerBD, LocalData)
	SET_ALL_PED_PARENT_DATA(ParentData.ePedLocation, ServerBD, LocalData)
	SET_ALL_PED_VFX_DATA(ServerBD, LocalData)
	SET_PED_DANCING_DATA(ParentData.ePedLocation, LocalData.DancingData)
	RESERVE_NETWORK_PEDS(ParentData.ePedLocation)
	INITIALISE_PED_PROCESS_ARRAY(LocalData)
	
	#IF IS_DEBUG_BUILD
	CREATE_WIDGETS(ParentData)
	#ENDIF
	
	WHILE TRUE
		MP_LOOP_WAIT_ZERO()
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		SCRIPT_PROFILER_START_OF_FRAME() 
		#ENDIF
		#ENDIF
		
		IF SHOULD_PED_SCRIPT_TERMINATE(ParentData)
			SCRIPT_CLEANUP(ParentData.ePedLocation)
		ELSE
			PROCESS_PED_EVENTS(ParentData.ePedLocation)
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("PROCESS_PED_EVENTS")
			#ENDIF	
			#ENDIF
			
			SWITCH LocalData.eActiveState
				CASE PED_ACTIVE_STATE_INIT
					
					IF HAS_PED_DANCING_DATA_BEEN_INITIALISED(ServerBD, LocalData)
						SET_PED_ACTIVE_STATE(PED_ACTIVE_STATE_UPDATE)
					ENDIF
					
				BREAK
				CASE PED_ACTIVE_STATE_UPDATE
					
					MAINTAIN_PED_DANCING_INTENSITY(LocalData.DancingData)
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE
					ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_PED_DANCING_INTENSITY")
					#ENDIF
					#ENDIF
					
					UPDATE_PLAYER_PED_SPEECH(ParentData)
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE
					ADD_SCRIPT_PROFILE_MARKER("UPDATE_PLAYER_PED_SPEECH")
					#ENDIF	
					#ENDIF
					
					UPDATE_CONTROLLER_PED_SPEECH(ParentData)
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE
					ADD_SCRIPT_PROFILE_MARKER("UPDATE_CONTROLLER_PED_SPEECH")
					#ENDIF	
					#ENDIF
					
					UPDATE_LOCAL_PEDS(ParentData)
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE
					ADD_SCRIPT_PROFILE_MARKER("UPDATE_LOCAL_PEDS")
					#ENDIF	
					#ENDIF
									
					UPDATE_NETWORK_PEDS(ParentData)
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE
					ADD_SCRIPT_PROFILE_MARKER("UPDATE_NETWORK_PEDS")
					#ENDIF	
					#ENDIF
					
					UPDATE_PEDS_CAPSULE_SIZES()
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE
					ADD_SCRIPT_PROFILE_MARKER("UPDATE_PEDS_CAPSULE_SIZES")
					#ENDIF	
					#ENDIF
					
					UPDATE_CUTSCENE_PEDS(ParentData)
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE
					ADD_SCRIPT_PROFILE_MARKER("UPDATE_CUTSCENE_PEDS")
					#ENDIF
					#ENDIF
					
					MAINTAIN_DJ_SWITCH_CUTSCENE(ParentData.ePedLocation, LocalData, ServerBD #IF IS_DEBUG_BUILD , DebugData #ENDIF )
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE
					ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_DJ_SWITCH_CUTSCENE")
					#ENDIF
					#ENDIF
					
					MAINTAIN_MUSIC_STUDIO_DRE_STATE_SWITCH_CUTSCENE(ParentData.ePedLocation, LocalData, ServerBD)
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE
					ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_MUSIC_STUDIO_DRE_STATE_SWITCH_CUTSCENE")
					#ENDIF
					#ENDIF
					
					MAINTAIN_PED_RESETTING(ParentData.ePedLocation)
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE
					ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_PED_RESETTING")
					#ENDIF
					#ENDIF
					
					MAINTAIN_NETWORK_PED_PATROL_PROPS_VISIBLE_STATE(ParentData.ePedLocation, LocalData, ServerBD)
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE
					ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_NETWORK_PED_PATROL_PROPS_VISIBLE_STATE")
					#ENDIF
					#ENDIF
					
					
					MAINTAIN_NETWORK_PED_PATROL_ALTERNATIVE_WALKING_ANIMATIONS(ParentData.ePedLocation, LocalData, ServerBD)
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE
					ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_NETWORK_PED_PATROL_ALTERNATIVE_WALKING_ANIMATIONS")
					#ENDIF
					#ENDIF
					
					RUN_LOCATION_SPECIFIC_UPDATE(ParentData.ePedLocation, LocalData, ServerBD)
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE
					ADD_SCRIPT_PROFILE_MARKER("RUN_LOCATION_SPECIFIC_UPDATE")
					#ENDIF
					#ENDIF
					
					
					#IF IS_DEBUG_BUILD
					UPDATE_PED_DEBUG_WIDGETS(ParentData.ePedLocation, DebugData, ServerBD, LocalData)
					#IF SCRIPT_PROFILER_ACTIVE
					ADD_SCRIPT_PROFILE_MARKER("UPDATE_PED_DEBUG_WIDGETS")
					#ENDIF
					#ENDIF
										
				BREAK
			ENDSWITCH
		ENDIF
			
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		SCRIPT_PROFILER_END_OF_FRAME() 
		#ENDIF
		#ENDIF
	ENDWHILE
	
#ENDIF	// FEATURE_HEIST_ISLAND

#IF NOT FEATURE_HEIST_ISLAND
SCRIPT
#ENDIF

ENDSCRIPT
