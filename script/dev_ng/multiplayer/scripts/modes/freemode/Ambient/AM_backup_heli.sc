
// ___________________________________________
//
//	Game: 
//	------
//	GTAV.
//
//
//  Script: 
//	-------
// 	Backup heli.
// 
//
//  Author: 
//	-------
//	William.Kennedy@RockstarNorth.com 
//
//
// 	Description: 
//	------------
//	Player can call contact and
// 	pay for a helicopter to fly
// 	in and back them up.
//
// ___________________________________________ 

 

USING "globals.sch"

// Game Headers
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_camera.sch"
USING "commands_hud.sch"
USING "commands_weapon.sch"
USING "commands_clock.sch"
USING "commands_fire.sch"
USING "lineactivation.sch"
USING "fm_relationships.sch"
USING "net_mission.sch"
USING "net_gang_angry.sch"

// Network Headers
USING "net_include.sch"
USING "freemode_header.sch"

USING "net_wait_zero.sch"

#IF IS_DEBUG_BUILD 
USING "net_debug.sch"
USING "profiler.sch"
USING "net_debug_log.sch"
#ENDIF



// ***********
// VARIABLES.
// ***********

//MP_MISSION_DATA thisScript
//MP_MISSION thisMission = eAM_CR_SecurityVan

// Game states.
CONST_INT GAME_STATE_INI						0
CONST_INT GAME_STATE_INI_SPAWN					1
CONST_INT GAME_STATE_RUNNING					2
CONST_INT GAME_STATE_END						3

CONST_INT NUM_HELI_PEDS							3
CONST_INT HELI_PILOT							0
CONST_INT HELI_GUNNER_A							1
CONST_INT HELI_GUNNER_B							2

CONST_INT HELI_LIFESPAN							1000*60*2 // 2 minutes.
CONST_INT HELI_LEAVETIME						20000 // 20 seconds.

BLIP_INDEX biHeliBlip


BOOL bOnMissionOnLaunch
BOOL bAddedPilotForDialogue
BOOL bPlayedArrivedDialogue
BOOL bPlayedLeaveDialogue
BOOL bHeliAngryWithMe
BOOL bHeliPed0
BOOL bHeliPed1
BOOL bHeliPed2

INT iNumVehiclesToReserve
INT iNumPedsToReserve

FLOAT fHeightMapHeight = -1
SCRIPT_TIMER stHeightMapTimer
FLOAT fCurrentFlightHeight[NUM_HELI_PEDS]

ENUM eHELI_STATE
    eHELISTATE_NOT_EXIST = 0,
    eHELISTATE_SPAWNING,
    eHELISTATE_GIVING_BACKUP,
	eHELISTATE_LEAVE,
	eHELISTATE_FAR_AWAY_FROM_EVERYONE,
    eHELISTATE_DEAD
ENDENUM

STRUCT STRUCT_SURVIVAL_HELI_PED
    NETWORK_INDEX netId
    MODEL_NAMES eModel = MP_S_M_Armoured_01
ENDSTRUCT

STRUCT STRUCT_SURVIVAL_HELI_DATA
    NETWORK_INDEX netId
    MODEL_NAMES eModel = BUZZARD2
    eHELI_STATE eState
    STRUCT_SURVIVAL_HELI_PED sPed[NUM_HELI_PEDS]
    VECTOR vSpawnCoords
    FLOAT fHeading
    BOOL bActive
    INT iCircCount
    INT iActivatingStage
	SCRIPT_TIMER stLifeSpanTimer
	SCRIPT_TIMER stLeaveTimer
ENDSTRUCT

structPedsForConversation sSpeech

// ****************
// BROADCAST DATA.
// ****************

// The server broadcast data.
// Everyone can read this data, only the server can update it. 
STRUCT ServerBroadcastData

	INT iServerGameState
	STRUCT_SURVIVAL_HELI_DATA sHeli
	
ENDSTRUCT
ServerBroadcastData serverBD

// The client broadcast data.
// Everyone can read this data, only the local player only the server can update it. 
STRUCT PlayerBroadcastData

	INT iGameState
	
ENDSTRUCT
PlayerBroadcastData playerBD[2]

// ****************
// Widgets & Debug.
// ****************
#IF IS_DEBUG_BUILD

	BOOL bHostEndMissionNow
	
	FUNC STRING GET_HELI_STATE_NAME(eHELI_STATE eNewState)
	
		SWITCH eNewState
			CASE eHELISTATE_NOT_EXIST 				RETURN "NOT_EXIST"
		    CASE eHELISTATE_SPAWNING 				RETURN "SPAWNING"
		    CASE eHELISTATE_GIVING_BACKUP 			RETURN "GIVING_BACKUP "
			CASE eHELISTATE_LEAVE					RETURN "LEAVE"
			CASE eHELISTATE_FAR_AWAY_FROM_EVERYONE 	RETURN "FAR_AWAY_FROM_EVERYONE"
		    CASE eHELISTATE_DEAD 					RETURN "DEAD"
		ENDSWITCH
		
		RETURN "NOT_IN_SWITCH"
	ENDFUNC
	
#ENDIF

PROC GOTO_HELI_STATE(eHELI_STATE eNewState)
	
	#IF IS_DEBUG_BUILD
	IF serverBD.sHeli.eState != eNewState
		NET_PRINT("[WJK] - Backup Heli - setting state to ")NET_PRINT(GET_HELI_STATE_NAME(eNewState))NET_NL()
	ENDIF
	#ENDIF
	
	serverBD.sHeli.eState = eNewState
	
ENDPROC


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  MAIN PROCS                            ////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

PROC MAINTAIN_START_OF_FRAME_FLAGS()
	
	
	
ENDPROC

FUNC BOOL CONTROL_CHECKS(NETWORK_INDEX netId)
	
	IF IS_NETWORK_ID_OWNED_BY_PARTICIPANT(netId)
		RETURN TRUE
	ENDIF
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(netId)
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Adds driver for dialogue once he exists.
PROC ADD_PILOT_FOR_DIALOGUE()
	IF NOT bAddedPilotForDialogue
		IF NOT IS_NET_PED_INJURED(serverBD.sHeli.sPed[0].netId)
			ADD_PED_FOR_DIALOGUE(sSpeech, 3, NET_TO_PED(serverBD.sHeli.sPed[0].netId), "FM_HBACK", TRUE)
			bAddedPilotForDialogue = TRUE
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Gets if the time of day means the enemy helis should have their searchlights on.
/// RETURNS:
///    BOOL - TRUE if should have searchlights on, FALSE if not.
FUNC BOOL DOES_TOD_NEED_HELI_SEARCHLIGHTS()
	
	IF GET_CLOCK_HOURS() >= 22
		RETURN TRUE
	ENDIF
	
	IF GET_CLOCK_HOURS() <= 6
	AND GET_CLOCK_HOURS() >= 0
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC REMOVE_HELI_BLIP()
    
    IF DOES_BLIP_EXIST(biHeliBlip)
        REMOVE_BLIP(biHeliBlip)
    ENDIF
    
ENDPROC

PROC MAINTAIN_HELI_MINIMUM_FLIGHT_HEIGHT()
	
	BOOL bCalculateHeight
	VECTOR vPlayerCoords
	
	IF fHeightMapHeight = (-1)
		bCalculateHeight = TRUE
	ELSE
		IF NOT HAS_NET_TIMER_STARTED(stHeightMapTimer)
			START_NET_TIMER(stHeightMapTimer)
		ELSE
			IF HAS_NET_TIMER_EXPIRED(stHeightMapTimer, 5000)
				bCalculateHeight = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF bCalculateHeight
		vPlayerCoords = GET_PLAYER_COORDS(PLAYER_ID())
		fHeightMapHeight = GET_APPROX_HEIGHT_FOR_POINT(vPlayerCoords.x, vPlayerCoords.y)
		IF fHeightMapHeight < 85.0
			fHeightMapHeight = 85.0
		ENDIF
		RESET_NET_TIMER(stHeightMapTimer)
	ENDIF
	
ENDPROC

FUNC BOOL FIND_SURVIVAL_HELI_SPAWN_COORDS()
    
    VECTOR vtemp
    
    IF IS_VECTOR_ZERO(serverBD.sHeli.vSpawnCoords)
        
		IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				
		        vtemp = GET_OFFSET_FROM_COORD_IN_WORLD_COORDS(  GET_PLAYER_COORDS(PLAYER_ID()),
		                                                        0.0,
		                                                        << (-1*200 * SIN(TO_FLOAT(serverBD.sHeli.iCircCount)*30)), (200 * COS(TO_FLOAT(serverBD.sHeli.iCircCount)*30)), 0 >>      )
		        
		        vtemp.z = GET_APPROX_HEIGHT_FOR_POINT(Vtemp.x, Vtemp.y)
		        
		        vtemp.z += 30.0
		        
		        IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(vtemp, 20.0, 1, 1, 15, TRUE, TRUE, TRUE, 180)
		                
		            serverBD.sHeli.vSpawnCoords = vtemp
		           	RETURN TRUE
					
		        ELSE
		        
		            serverBD.sHeli.iCircCount++
		            
		            IF serverBD.sHeli.iCircCount >= 12
		                serverBD.sHeli.iCircCount = 0
		            ENDIF
		            
		        ENDIF
            
			ENDIF
		ENDIF
		
    ELSE
    
        RETURN TRUE
    
    ENDIF
    
    RETURN FALSE
    
ENDFUNC

PROC SETUP_HELI_RELATIONSHIPS(NETWORK_INDEX pedNetId)
	
	REL_GROUP_HASH relGroup
	
	IF NOT IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID(), FALSE)
	AND NOT IS_FM_MISSION_LAUNCH_IN_PROGRESS()
		
		// If in free mode main session, want heli to only like me.
		relGroup = rgFM_AiAmbientGangMerc[AmbientBackupHeliMerc]
		
	ELSE
		
		// If on a mission, set the heli to do whatever my rel group tels them to do.
		relGroup = GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID())
		
	ENDIF
	
	SET_PED_RELATIONSHIP_GROUP_HASH(NET_TO_PED(pedNetId), relGroup)
	
ENDPROC

PROC SET_HELI_PED_COMBAT_AI(PED_INDEX pedId, INT iPed, BOOL bInvincible = FALSE, BOOL bSetHealth = TRUE)
	
	GIVE_DELAYED_WEAPON_TO_PED(pedId, WEAPONTYPE_ASSAULTRIFLE, 300, TRUE)
    SET_PED_COMBAT_ATTRIBUTES(pedId, CA_ALWAYS_FIGHT, TRUE)
	SET_PED_COMBAT_ATTRIBUTES(pedId, CA_LEAVE_VEHICLES, FALSE)
	
	SET_PED_COMBAT_MOVEMENT(pedId, CM_WILLADVANCE)
	SET_PED_COMBAT_ABILITY(pedId, CAL_AVERAGE)
    SET_PED_COMBAT_RANGE(pedId, CR_FAR)
	SET_PED_TARGET_LOSS_RESPONSE(pedId, TLR_NEVER_LOSE_TARGET)
	SET_PED_HIGHLY_PERCEPTIVE(pedId, TRUE)
	SET_PED_CAN_BE_TARGETTED(pedId, TRUE)
	SET_PED_SEEING_RANGE(pedId, fCurrentFlightHeight[iPed]+100.0)
	
//	SET_PED_VISUAL_FIELD_MIN_ANGLE(pedId, -90.0)
//	SET_PED_VISUAL_FIELD_MAX_ANGLE(pedId, 90.0)
//	SET_PED_VISUAL_FIELD_MIN_ELEVATION_ANGLE(pedId, -90.0)
//	SET_PED_VISUAL_FIELD_MAX_ELEVATION_ANGLE(pedId, 90.0)
	SET_PED_VISUAL_FIELD_PERIPHERAL_RANGE(pedId, 400.0)
	
	SET_COMBAT_FLOAT(pedId, CCF_HELI_SENSES_RANGE, 400.0)
	
	// Make peds a bit tougher.
	IF NOT bInvincible
		IF bSetHealth
			SET_ENTITY_MAX_HEALTH(pedId, 250)
			SET_ENTITY_HEALTH(pedId, 250)
			SET_PED_ARMOUR(pedId, 250)
		ENDIF
	ELSE
		SET_ENTITY_INVINCIBLE(pedId, TRUE)
	ENDIF
	
ENDPROC

FUNC BOOL SPAWN_SURVIVAL_HELI()
    
    IF REQUEST_LOAD_MODEL(serverBD.sHeli.eModel)
        IF REQUEST_LOAD_MODEL(serverBD.sHeli.sPed[0].eModel)
            
            IF NOT IS_NET_VEHICLE_DRIVEABLE(serverBD.sHeli.netId)
        
                IF CREATE_NET_VEHICLE(serverBD.sHeli.netId, serverBD.sHeli.eModel, serverBD.sHeli.vSpawnCoords, 0.0, TRUE,TRUE, TRUE, FALSE, FALSE)
                    
					SET_HELI_BLADES_FULL_SPEED(NET_TO_VEH(serverBD.sHeli.netId))
					SET_VEHICLE_ENGINE_ON(NET_TO_VEH(serverBD.sHeli.netId), TRUE, TRUE)
					ACTIVATE_PHYSICS(NET_TO_VEH(serverBD.sHeli.netId))
					SET_ENTITY_DYNAMIC(NET_TO_VEH(serverBD.sHeli.netId), TRUE)
					SET_ATTRIBUTE_DAMAGE_DECORATOR_BIT(NET_TO_VEH(serverBD.sHeli.netId), ATTRIBUTE_DAMAGE_BS_BACKUP_HELI)
					mpglobals.viBackupHeli = NET_TO_VEH(serverBD.sHeli.netId)
					
                   	NET_PRINT("[WJK] - Backup Heli - SPAWN_SURVIVAL_HELI - created heli.")NET_NL()
					 
                    IF CREATE_NET_PED_IN_VEHICLE(serverBD.sHeli.sPed[0].netId, serverBD.sHeli.netId, PEDTYPE_CIVMALE, serverBD.sHeli.sPed[0].eModel, VS_DRIVER, TRUE)
                        
                        // Set driver properties.
                        SETUP_HELI_RELATIONSHIPS(serverBD.sHeli.sPed[0].netId)
						SET_HELI_PED_COMBAT_AI(NET_TO_PED(serverBD.sHeli.sPed[0].netId), 0)
                        NETWORK_SET_ATTRIBUTE_DAMAGE_TO_PLAYER(NET_TO_PED(serverBD.sHeli.sPed[0].netId), PLAYER_ID())
						SET_ATTRIBUTE_DAMAGE_DECORATOR_BIT(NET_TO_PED(serverBD.sHeli.sPed[0].netId), ATTRIBUTE_DAMAGE_BS_BACKUP_HELI)
						mpglobals.viBackUpHeliPed[0] = NET_TO_PED(serverBD.sHeli.sPed[0].netId)
						
						NET_PRINT("[WJK] - Backup Heli - SPAWN_SURVIVAL_HELI - created pilot.")NET_NL()
						
                        IF CREATE_NET_PED_IN_VEHICLE(serverBD.sHeli.sPed[1].netId, serverBD.sHeli.netId, PEDTYPE_CIVMALE, serverBD.sHeli.sPed[1].eModel, VS_BACK_LEFT, TRUE)
                            
                            // Set driver properties.
							SETUP_HELI_RELATIONSHIPS(serverBD.sHeli.sPed[1].netId)
                            SET_HELI_PED_COMBAT_AI(NET_TO_PED(serverBD.sHeli.sPed[1].netId), 1, TRUE)
							NETWORK_SET_ATTRIBUTE_DAMAGE_TO_PLAYER(NET_TO_PED(serverBD.sHeli.sPed[1].netId), PLAYER_ID())
							SET_ATTRIBUTE_DAMAGE_DECORATOR_BIT(NET_TO_PED(serverBD.sHeli.sPed[1].netId), ATTRIBUTE_DAMAGE_BS_BACKUP_HELI)
							mpglobals.viBackUpHeliPed[1] = NET_TO_PED(serverBD.sHeli.sPed[1].netId)
							
							NET_PRINT("[WJK] - Backup Heli - SPAWN_SURVIVAL_HELI - created sniper 1.")NET_NL()
							
                            IF CREATE_NET_PED_IN_VEHICLE(serverBD.sHeli.sPed[2].netId, serverBD.sHeli.netId, PEDTYPE_CIVMALE, serverBD.sHeli.sPed[2].eModel, VS_BACK_RIGHT, TRUE)
	                            
	                            // Set driver properties.
								SETUP_HELI_RELATIONSHIPS(serverBD.sHeli.sPed[2].netId)
	                            SET_HELI_PED_COMBAT_AI(NET_TO_PED(serverBD.sHeli.sPed[2].netId), 2, TRUE)
								NETWORK_SET_ATTRIBUTE_DAMAGE_TO_PLAYER(NET_TO_PED(serverBD.sHeli.sPed[2].netId), PLAYER_ID())
								SET_ATTRIBUTE_DAMAGE_DECORATOR_BIT(NET_TO_PED(serverBD.sHeli.sPed[2].netId), ATTRIBUTE_DAMAGE_BS_BACKUP_HELI)
								mpglobals.viBackUpHeliPed[2] = NET_TO_PED(serverBD.sHeli.sPed[2].netId)
								
								NET_PRINT("[WJK] - Backup Heli - SPAWN_SURVIVAL_HELI - created sniper 2, returning true.")NET_NL()
								
	                            RETURN TRUE
	                            
                            ENDIF
                            
                        ENDIF
                        
                    ENDIF
                    
                ENDIF
                
            ENDIF
    
        ENDIF
    ENDIF
    
    RETURN FALSE
    
ENDFUNC

PROC MAINTAIN_RESERVATION()
	
	iNumVehiclesToReserve = 0
	iNumPedsToReserve = 0

	IF IS_NET_VEHICLE_DRIVEABLE(serverBD.sHeli.netId)
		iNumVehiclesToReserve++
	ELSE
		IF DOES_ENTITY_EXIST(NET_TO_VEH(serverBD.sHeli.netId))
			CLEANUP_NET_ID(serverBD.sHeli.netId)
			iNumVehiclesToReserve++
		ENDIF
	ENDIF
	
	IF IS_NET_PED_INJURED(serverBD.sHeli.sPed[0].netId)
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sHeli.sPed[0].netId)
			CLEANUP_NET_ID(serverBD.sHeli.sPed[0].netId)
			iNumPedsToReserve++
		ENDIF
	ELSE
		iNumPedsToReserve++
	ENDIF
	
	IF IS_NET_PED_INJURED(serverBD.sHeli.sPed[1].netId)
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sHeli.sPed[1].netId)
			CLEANUP_NET_ID(serverBD.sHeli.sPed[1].netId)
			iNumPedsToReserve++
		ENDIF
	ELSE
		iNumPedsToReserve++
	ENDIF
	
	IF IS_NET_PED_INJURED(serverBD.sHeli.sPed[2].netId)
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sHeli.sPed[2].netId)
			CLEANUP_NET_ID(serverBD.sHeli.sPed[2].netId)
			iNumPedsToReserve++
		ENDIF
	ELSE
		iNumPedsToReserve++
	ENDIF
	
	IF GET_NUM_RESERVED_MISSION_VEHICLES() != iNumVehiclesToReserve
		IF CAN_RESERVE_NETWORK_VEHICLES_FOR_THIS_SCRIPT(iNumVehiclesToReserve, FALSE, TRUE)
			RESERVE_NETWORK_MISSION_VEHICLES(iNumVehiclesToReserve)
		ENDIF
	ENDIF
	
	IF GET_NUM_RESERVED_MISSION_PEDS() != iNumPedsToReserve
		IF CAN_RESERVE_NETWORK_PEDS_FOR_THIS_SCRIPT(iNumPedsToReserve, FALSE, TRUE)
			RESERVE_NETWORK_MISSION_PEDS(iNumPedsToReserve)
		ENDIF
	ENDIF
	
ENDPROC

PROC MAINTAIN_ANGRY_TRACKING()
		
	IF mpglobals.bIKilledABackupHeliEntity
		SET_GANG_ANGRY_AT_PLAYER(GANG_CALL_TYPE_SPECIAL_MERRYWEATHER, GANG_ANGRY_TIME_LONG)
		bHeliAngryWithMe = TRUE
	ENDIF
	
	IF mpglobals.fBackUpHeliDamageByMe >= 250.0
		SET_GANG_ANGRY_AT_PLAYER(GANG_CALL_TYPE_SPECIAL_MERRYWEATHER, GANG_ANGRY_TIME_LONG)
		bHeliAngryWithMe = TRUE
	ENDIF
	
ENDPROC

PROC PROCESS_HELI_BRAIN()
    
    SWITCH serverBD.sHeli.eState
        
        CASE eHELISTATE_NOT_EXIST
            
         	GOTO_HELI_STATE(eHELISTATE_SPAWNING)
            
        BREAK
        
        CASE eHELISTATE_SPAWNING
            
            IF FIND_SURVIVAL_HELI_SPAWN_COORDS()
                IF SPAWN_SURVIVAL_HELI()
					START_NET_TIMER(serverBD.sHeli.stLifeSpanTimer)
                    GOTO_HELI_STATE(eHELISTATE_GIVING_BACKUP)
                ELSE
                    NET_PRINT("[WJK] - Backup Heli - found suitable spawn coords, spawning heli.")NET_NL()
                ENDIF
            ELSE
                NET_PRINT("[WJK] - Backup Heli - finding heli spawn coords.")NET_NL()
            ENDIF
               
        BREAK
        
        CASE eHELISTATE_GIVING_BACKUP
            
			MAINTAIN_RESERVATION()
			MAINTAIN_ANGRY_TRACKING()
			
            IF NOT IS_NET_VEHICLE_DRIVEABLE(serverBD.sHeli.netId)
				IF IS_NET_PED_INJURED(serverBD.sHeli.sPed[0].netId)
				AND IS_NET_PED_INJURED(serverBD.sHeli.sPed[1].netId)
				AND IS_NET_PED_INJURED(serverBD.sHeli.sPed[2].netId)
					NET_PRINT("[WJK] - Backup Heli - heli wrecked.")NET_NL()
	                GOTO_HELI_STATE(eHELISTATE_DEAD)
				ENDIF
            ELIF IS_NET_PED_INJURED(serverBD.sHeli.sPed[0].netId)
				NET_PRINT("[WJK] - Backup Heli - pilot dead.")NET_NL()
				IF IS_NET_PED_INJURED(serverBD.sHeli.sPed[1].netId)
				AND IS_NET_PED_INJURED(serverBD.sHeli.sPed[2].netId)
	                GOTO_HELI_STATE(eHELISTATE_DEAD)
				ENDIF
			ELIF (IS_NET_PED_INJURED(serverBD.sHeli.sPed[1].netId) ANd IS_NET_PED_INJURED(serverBD.sHeli.sPed[2].netId))
				NET_PRINT("[WJK] - Backup Heli - both snipers dead.")NET_NL()
				GOTO_HELI_STATE(eHELISTATE_LEAVE)
			ELSE
				IF HAS_NET_TIMER_STARTED(serverBD.sHeli.stLifeSpanTimer)
					IF HAS_NET_TIMER_EXPIRED(serverBD.sHeli.stLifeSpanTimer, HELI_LIFESPAN)
						NET_PRINT("[WJK] - Backup Heli - heli been providing backup for ")NET_PRINT_INT(HELI_LIFESPAN)NET_PRINT(" seconds.")NET_NL()
						GOTO_HELI_STATE(eHELISTATE_LEAVE)
					ENDIF
				ENDIF
				IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(),MPAM_TYPE_CINEMA)
					NET_PRINT("[WJK] - Backup Heli - player is on cinema script.")NET_NL()
					GOTO_HELI_STATE(eHELISTATE_LEAVE)
				ENDIF
				IF bHeliAngryWithMe
					NET_PRINT("[WJK] - Backup Heli - heli is angry with me, leaving.")NET_NL()
					GOTO_HELI_STATE(eHELISTATE_LEAVE)
				ENDIF
				IF IS_FM_PLAYER_CRITICAL_TO_THIS_FM_EVENT(PLAYER_ID(), FMMC_TYPE_PENNED_IN)
					NET_PRINT("[WJK] - Backup Heli - player is on penned in.")NET_NL()
					GOTO_HELI_STATE(eHELISTATE_LEAVE)
				ENDIF
            ENDIF
                
        BREAK
		
        CASE eHELISTATE_LEAVE
			
			MAINTAIN_RESERVATION()
			
			IF NOT IS_NET_VEHICLE_DRIVEABLE(serverBD.sHeli.netId)
				IF IS_NET_PED_INJURED(serverBD.sHeli.sPed[0].netId)
				AND IS_NET_PED_INJURED(serverBD.sHeli.sPed[1].netId)
				AND IS_NET_PED_INJURED(serverBD.sHeli.sPed[2].netId)
	                GOTO_HELI_STATE(eHELISTATE_DEAD)
				ENDIF
            ELIF IS_NET_PED_INJURED(serverBD.sHeli.sPed[0].netId)
				IF IS_NET_PED_INJURED(serverBD.sHeli.sPed[1].netId)
				AND IS_NET_PED_INJURED(serverBD.sHeli.sPed[2].netId)
	                GOTO_HELI_STATE(eHELISTATE_DEAD)
				ENDIF
			ENDIF
			
		BREAK
		
        CASE eHELISTATE_DEAD
            
			
			
        BREAK
        
    ENDSWITCH
    
ENDPROC

PROC KILL_HELI_CREW()
	
	PED_INDEX pedId
	
	IF NOT IS_NET_PED_INJURED(serverBD.sHeli.sPed[0].netId)
		IF CONTROL_CHECKS(serverBD.sHeli.sPed[0].netId)
            IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sHeli.sPed[0].netId)
                pedId = NET_TO_PED(serverBD.sHeli.sPed[0].netId)
                SET_ENTITY_HEALTH(pedId, 0)
            ENDIF
		ENDIF
    ENDIF
    
    IF NOT IS_NET_PED_INJURED(serverBD.sHeli.sPed[1].netId)
		IF CONTROL_CHECKS(serverBD.sHeli.sPed[1].netId)
            IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sHeli.sPed[1].netId)
            	pedId = NET_TO_PED(serverBD.sHeli.sPed[1].netId)
               	SET_ENTITY_HEALTH(pedId, 0)
            ENDIF
		ENDIF
    ENDIF
    
    IF NOT IS_NET_PED_INJURED(serverBD.sHeli.sPed[2].netId)
		IF CONTROL_CHECKS(serverBD.sHeli.sPed[2].netId)
            IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sHeli.sPed[2].netId)
                pedId = NET_TO_PED(serverBD.sHeli.sPed[2].netId)
                SET_ENTITY_HEALTH(pedId, 0)
            ENDIF
		ENDIF
    ENDIF
	
ENDPROC

PROC PROCESS_HELI_BODY()
    
    PED_INDEX pedId
    VEHICLE_INDEX niHeli
	VEHICLE_MISSION eVehicleMission
	
    SWITCH serverBD.sHeli.eState
            
            CASE eHELISTATE_NOT_EXIST
                    
                REMOVE_HELI_BLIP()
                    
            BREAK
            
            CASE eHELISTATE_SPAWNING
                    
                REMOVE_HELI_BLIP()
                    
            BREAK
            
            CASE eHELISTATE_GIVING_BACKUP
                
				// If heli is stil flying.
                IF IS_NET_VEHICLE_DRIVEABLE(serverBD.sHeli.netId)
                    
					niHeli = NET_TO_VEH(serverBD.sHeli.netId)
					
					IF NOT DOES_BLIP_EXIST(biHeliBlip)
                        biHeliBlip = ADD_BLIP_FOR_ENTITY(niHeli)
						SET_BLIP_SPRITE(biHeliBlip, RADAR_TRACE_PLAYER_HELI)
						SHOW_HEIGHT_ON_BLIP(biHeliBlip, FALSE)
						SET_BLIP_NAME_FROM_TEXT_FILE(biHeliBlip, "MPCT_MERRY3")
						//IF IS_NET_PLAYER_OK(PLAYER_ID(), FALSE)
							//SET_BLIP_COLOUR(biHeliBlip, GET_INT_FROM_HUD_COLOUR(GET_PLAYER_HUD_COLOUR(PLAYER_ID())))
							//SET_BLIP_COLOUR_FROM_HUD_COLOUR(biHeliBlip, GET_PLAYER_HUD_COLOUR(PLAYER_ID()))
						//ENDIF
						SET_BLIP_COLOUR_FROM_HUD_COLOUR(biHeliBlip, HUD_COLOUR_WHITE)
                    ENDIF
					
					IF CONTROL_CHECKS(serverBD.sHeli.netId)
						IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sHeli.netId)
							IF DOES_TOD_NEED_HELI_SEARCHLIGHTS()
								IF NOT IS_VEHICLE_SEARCHLIGHT_ON(niHeli)
									SET_VEHICLE_SEARCHLIGHT(niHeli, TRUE)
								ENDIF
							ELSE
								IF IS_VEHICLE_SEARCHLIGHT_ON(niHeli)
									SET_VEHICLE_SEARCHLIGHT(niHeli, FALSE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.sHeli.sPed[0].netId)
						pedId = NET_TO_PED(serverBD.sHeli.sPed[0].netId)
						IF NOT IS_PED_INJURED(pedId)
							
							eVehicleMission = GET_ACTIVE_VEHICLE_MISSION_TYPE(niHeli)
							
							IF eVehicleMission != MISSION_PROTECT
							OR (fCurrentFlightHeight[0] != fHeightMapHeight) // Sorted in MAINTAIN_HELI_MINIMUM_FLIGHT_HEIGHT().
								#IF IS_DEBUG_BUILD
									IF eVehicleMission != MISSION_PROTECT
										NET_PRINT("[WJK] - Backup Heli - eVehicleMission != MISSION_PROTECT.")NET_NL()
									ENDIF
									IF (fCurrentFlightHeight[0] != fHeightMapHeight)
										NET_PRINT("[WJK] - Backup Heli - (fCurrentFlightHeight[0] != fHeightMapHeight).")NET_NL()
									ENDIF
								#ENDIF
								IF CONTROL_CHECKS(serverBD.sHeli.netId)
			                        IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sHeli.sPed[0].netId)
										fCurrentFlightHeight[0] = fHeightMapHeight
		                                SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedId, TRUE)
										SET_HELI_PED_COMBAT_AI(pedId, 0, FALSE, FALSE)
										SET_HELI_BLADES_FULL_SPEED(niHeli)
										SET_VEHICLE_ENGINE_ON(niHeli, TRUE, TRUE)
										IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
											IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
												TASK_HELI_MISSION(pedId, niHeli, NULL, PLAYER_PED_ID(), <<0.0,0.0,0.0>>, MISSION_PROTECT, 20.0, 40.0, -1.0, CEIL(fCurrentFlightHeight[0]), 10)
												NET_PRINT("[WJK] - Backup Heli - given TASK_HELI_MISSION(pedId, niHeli, NULL, PLAYER_PED_ID(), <<0.0,0.0,0.0>>, MISSION_PROTECT, 20.0, 40.0, -1.0, CEIL(fCurrentFlightHeight[0]), 10)")NET_NL()
												NET_PRINT("[WJK] - Backup Heli - fCurrentFlightHeight[0] = ")NET_PRINT_FLOAT(fCurrentFlightHeight[0])NET_NL()
											ENDIF
										ENDIF
									ENDIF
								ENDIF
	                        ENDIF
							
							IF NOT bPlayedArrivedDialogue
								IF VDIST(GET_PLAYER_COORDS(PLAYER_ID()), GET_ENTITY_COORDS(NET_TO_VEH(serverBD.sHeli.netId))) <= 50.0 
									CREATE_CONVERSATION_USING_V_CONTENT_IN_DLC(sSpeech, "CT_AUD", "MPCT_HBarr", CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT)
									bPlayedArrivedDialogue = TRUE
								ENDIF
							ENDIF
							
						ELSE
							NET_PRINT("[WJK] - Backup Heli - pilot is dead.")NET_NL()
	                    ENDIF
                    ELSE
						NET_PRINT("[WJK] - Backup Heli - pilot does not exist.")NET_NL()
					ENDIF
					
					IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.sHeli.sPed[1].netId)
						pedId = NET_TO_PED(serverBD.sHeli.sPed[1].netId)
						IF NOT IS_PED_INJURED(pedId)
							IF NOT IS_PED_IN_COMBAT(pedId)
							OR (fCurrentFlightHeight[1] != fHeightMapHeight) // Sorted in MAINTAIN_HELI_MINIMUM_FLIGHT_HEIGHT().
								IF CONTROL_CHECKS(serverBD.sHeli.netId)
			                        IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sHeli.sPed[1].netId)
										fCurrentFlightHeight[1] = fHeightMapHeight
		                                SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedId, FALSE)
										SET_HELI_PED_COMBAT_AI(pedId, 1, FALSE, FALSE)
//		                                TASK_COMBAT_HATED_TARGETS_IN_AREA(pedId, GET_ENTITY_COORDS(pedId), 299.0)
		                            ENDIF
								ENDIF
	                        ENDIF
						ELSE
							NET_PRINT("[WJK] - Backup Heli - sniper 1 is dead.")NET_NL()
	                    ENDIF
					ELSE
						NET_PRINT("[WJK] - Backup Heli - sniper 1 does not exist.")NET_NL()
					ENDIF
					
					IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.sHeli.sPed[2].netId)
						pedId = NET_TO_PED(serverBD.sHeli.sPed[2].netId)
						IF NOT IS_PED_INJURED(pedId)
							IF NOT IS_PED_IN_COMBAT(pedId)
							OR (fCurrentFlightHeight[2] != fHeightMapHeight) // Sorted in MAINTAIN_HELI_MINIMUM_FLIGHT_HEIGHT().
								IF CONTROL_CHECKS(serverBD.sHeli.netId)
			                        IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sHeli.sPed[2].netId)
										fCurrentFlightHeight[2] = fHeightMapHeight
		                                SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedId, FALSE)
										SET_HELI_PED_COMBAT_AI(pedId, 2, FALSE, FALSE)
//		                                TASK_COMBAT_HATED_TARGETS_IN_AREA(pedId, GET_ENTITY_COORDS(pedId), 299.0)
		                            ENDIF
								ENDIF
	                        ENDIF
						ELSE
							NET_PRINT("[WJK] - Backup Heli - sniper 2 is dead.")NET_NL()
	                    ENDIF
					ELSE
						NET_PRINT("[WJK] - Backup Heli - sniper 2 does not exist.")NET_NL()
					ENDIF
					
                ENDIF
                
				// If heli or pilot are dead, kill crew.
				IF NOT IS_NET_VEHICLE_DRIVEABLE(serverBD.sHeli.netId)
				OR IS_NET_PED_INJURED(serverBD.sHeli.sPed[0].netId)
					KILL_HELI_CREW()
				ENDIF
				
            BREAK
            
			CASE eHELISTATE_LEAVE
				
				REMOVE_HELI_BLIP()
				
				IF NOT HAS_NET_TIMER_STARTED(serverBD.sHeli.stLeaveTimer)
					PRINTSTRING("Starting stLeaveTimer now") PRINTNL()
					START_NET_TIMER(serverBD.sHeli.stLeaveTimer)
				ENDIF
				
				IF IS_NET_VEHICLE_DRIVEABLE(serverBD.sHeli.netId)
                    
					niHeli = NET_TO_VEH(serverBD.sHeli.netId)
					
                    IF NOT IS_NET_PED_INJURED(serverBD.sHeli.sPed[0].netId)
						pedId = NET_TO_PED(serverBD.sHeli.sPed[0].netId)
//						eVehicleMission = GET_ACTIVE_VEHICLE_MISSION_TYPE(niHeli)
//						IF eVehicleMission != MISSION_FLEE
						IF bHeliPed0 = FALSE
							IF CONTROL_CHECKS(serverBD.sHeli.netId)
		                        IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sHeli.sPed[0].netId)
	                                SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedId, FALSE)
									CLEAR_PED_TASKS(pedId)
									SET_PED_RELATIONSHIP_GROUP_HASH(pedId, rgFM_AiLike)
									IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
										IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
											TASK_HELI_MISSION(pedId, niHeli, NULL, PLAYER_PED_ID(), <<0.0,0.0,0.0>>, MISSION_FLEE, 20.0, 500.0, -1.0, 40, 10)
											SET_PED_KEEP_TASK(pedId, TRUE)
											bHeliPed0 = TRUE
											PRINTSTRING("TASK_HELI_MISSION MISSION_FLEE Being given to heli pilot") PRINTNL()
										ENDIF
									ENDIF
	                            ENDIF
							ENDIF
                        ENDIF
                    ENDIF
                    
					IF NOT IS_NET_PED_INJURED(serverBD.sHeli.sPed[1].netId)
						IF bHeliPed1 = FALSE
							pedId = NET_TO_PED(serverBD.sHeli.sPed[1].netId)
							IF CONTROL_CHECKS(serverBD.sHeli.netId)
		                        IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sHeli.sPed[1].netId)
									CLEAR_PED_TASKS(pedId)
	                                SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedId, TRUE)
									SET_PED_KEEP_TASK(pedId, TRUE)
//									SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iBSHeliPeds, HeliPed1)
									bHeliPed1 = TRUE
									PRINTSTRING("bHeliPed1 = TRUE")
	                            ENDIF
							ENDIF
						ENDIF
//						IF IS_PED_IN_COMBAT(pedId)
//							IF CONTROL_CHECKS(serverBD.sHeli.netId)
//		                        IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sHeli.sPed[1].netId)
//									CLEAR_PED_TASKS(pedId)
//	                                SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedId, TRUE)
//	                            ENDIF
//							ENDIF
//                        ENDIF
						IF NOT bPlayedLeaveDialogue
							IF VDIST(GET_PLAYER_COORDS(PLAYER_ID()), GET_ENTITY_COORDS(NET_TO_VEH(serverBD.sHeli.netId))) <= 50.0 
								CREATE_CONVERSATION_USING_V_CONTENT_IN_DLC(sSpeech, "CT_AUD", "MPCT_HBleav", CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT)
								bPlayedLeaveDialogue = TRUE
							ENDIF
						ENDIF
                    ENDIF
					
					IF NOT IS_NET_PED_INJURED(serverBD.sHeli.sPed[2].netId)						
						IF bHeliPed2 = FALSE
							pedId = NET_TO_PED(serverBD.sHeli.sPed[2].netId)
							IF CONTROL_CHECKS(serverBD.sHeli.netId)
		                        IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sHeli.sPed[2].netId)
									CLEAR_PED_TASKS(pedId)
	                                SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedId, TRUE)
									SET_PED_KEEP_TASK(pedId, TRUE)
//									SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iBSHeliPeds, HeliPed2)
									bHeliPed2 = TRUE
									PRINTSTRING("bHeliPed2 = TRUE")
	                            ENDIF
							ENDIF
						ENDIF
//						pedId = NET_TO_PED(serverBD.sHeli.sPed[2].netId)
//						IF IS_PED_IN_COMBAT(pedId)
//							IF CONTROL_CHECKS(serverBD.sHeli.netId)
//		                        IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sHeli.sPed[2].netId)
//	                                CLEAR_PED_TASKS(pedId)
//									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedId, TRUE)
//	                            ENDIF
//							ENDIF
//                        ENDIF
                    ENDIF
					
				ENDIF
				
				// If heli or pilot are dead, kill crew.
				IF NOT IS_NET_VEHICLE_DRIVEABLE(serverBD.sHeli.netId)
				OR IS_NET_PED_INJURED(serverBD.sHeli.sPed[0].netId)
					KILL_HELI_CREW()
				ENDIF
				
			BREAK
			
			CASE eHELISTATE_DEAD
                    
                REMOVE_HELI_BLIP()
                    
            BREAK
            
    ENDSWITCH
    
ENDPROC

/// PURPOSE:
///    Server only function that checks to see if the game mode should now end. For this test script, it if time has passed.
/// RETURNS:
///    True if the the mode has been active for ROUND_DURATION 
FUNC BOOL HAVE_MISSION_END_CONDITIONS_BEEN_MET()
	
	#IF IS_DEBUG_BUILD
		IF bHostEndMissionNow
			RETURN TRUE
		ENDIF
	#ENDIF	
	
	RETURN FALSE
	
ENDFUNC
 
/// PURPOSE:
///    Helper function to get a clients game/mission state
/// PARAMS:
///    iPlayer - The player's game state you want.
/// RETURNS:
///    The game state.
FUNC INT GET_CLIENT_MISSION_STATE(INT iPlayer)
	RETURN playerBD[iPlayer].iGameState
ENDFUNC


/// PURPOSE:
///    Helper function to get the servers game/mission state
/// RETURNS:
///    The server game state.
FUNC INT GET_SERVER_MISSION_STATE()
	RETURN serverBD.iServerGameState
ENDFUNC

/// PURPOSE:
///    Processes security van script server logic.
PROC PROCESS_SERVER()
	
	PROCESS_HELI_BRAIN()
	
	IF serverBD.sHeli.eState = eHELISTATE_DEAD
		NET_PRINT("[WJK] - Backup Heli - heli is dead, initiating script termination.")NET_NL()
		serverBD.iServerGameState = GAME_STATE_END
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(serverBD.sHeli.stLeaveTimer)
		IF HAS_NET_TIMER_EXPIRED(serverBD.sHeli.stLeaveTimer, HELI_LEAVETIME)
			NET_PRINT("[WJK] - Backup Heli - heli has been leaving for 20 seconds, should be far enough away now for cleanup, initiating script termination.")NET_NL()
			serverBD.iServerGameState = GAME_STATE_END
		ENDIF
	ENDIF
	
	//Set to end state if player enters passive mode whilst airstrike is active.
	IF IS_MP_PASSIVE_MODE_ENABLED()
		PRINTLN("[BACKUP_HELI] - GAME_STATE END 1")
		serverBD.iServerGameState = GAME_STATE_END
	ENDIF	
	
	IF bOnMissionOnLaunch
		IF NOT NETWORK_IS_IN_TUTORIAL_SESSION()
			NET_PRINT("[WJK] - Backup Heli - was on NETWORK_IS_IN_TUTORIAL_SESSION() when heli was called, now not on mission, initiating script termination.")NET_NL()
			serverBD.iServerGameState = GAME_STATE_END
		ENDIF
	ELSE
		IF NETWORK_IS_IN_TUTORIAL_SESSION()
			NET_PRINT("[WJK] - Backup Heli - was not on NETWORK_IS_IN_TUTORIAL_SESSION() when heli was called, now on mission, initiating script termination.")NET_NL()
			serverBD.iServerGameState = GAME_STATE_END
		ENDIF
	ENDIF
	
	//2043884
	IF IS_CORONA_INITIALISING_A_QUICK_RESTART()
		NET_PRINT("[MJM] - Backup Heli - Doing a quick restart, kill script.")NET_NL()
		serverBD.iServerGameState = GAME_STATE_END
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Processes security van client logic.
PROC PROCESS_CLIENT()
	
	ADD_PILOT_FOR_DIALOGUE()
	PROCESS_HELI_BODY()
	
ENDPROC

/// PURPOSE:
///    Widgets and debug.
#IF IS_DEBUG_BUILD
	
	BOOL bKillHeli
	
	/// PURPOSE:
	///    Creates widgets.
	PROC CREATE_WIDGETS()
		
		START_WIDGET_GROUP("Backup Heli")
			ADD_WIDGET_BOOL("Kill Heli", bKillHeli)
		STOP_WIDGET_GROUP()
		
	ENDPROC		
	
	/// PURPOSE:
	///    Updates widgets.
	PROC UPDATE_WIDGETS()		
		
		IF bKillHeli
			IF IS_NET_VEHICLE_DRIVEABLE(serverBD.sHeli.netId)
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sHeli.netId)
					SET_ENTITY_HEALTH(NET_TO_VEH(serverBD.sHeli.netId), 0)
				ELSE
					NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.sHeli.netId)
				ENDIF
			ELSE
				bKillHeli = FALSE
			ENDIF
		ENDIF
		
	ENDPROC

#ENDIF


/// PURPOSE:
///    Script cleanup.
PROC SCRIPT_CLEANUP()
	VEHICLE_INDEX viTemp
	PED_INDEX piTemp
	mpglobals.viBackupHeli = viTemp
	mpglobals.viBackUpHeliPed[0] = piTemp
	mpglobals.viBackUpHeliPed[1] = piTemp
	mpglobals.viBackUpHeliPed[2] = piTemp
	mpGlobals.fBackUpHeliDamageByMe = 0.0
	mpGlobals.bIKilledABackupHeliEntity = FALSE
	BROADCAST_TELL_SERVER_CONTACT_REQUEST_COMPLETE(ALL_PLAYERS(),REQUEST_BACKUP_HELI,INVALID_PLAYER_INDEX())
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()  // call this to terminate your script.
ENDPROC	


/// PURPOSE:
///    Do necessary pre game start ini.
PROC PROCESS_PRE_GAME(MP_MISSION_DATA missionScriptArgs)
	
	// This marks the script as a net script, and handles any instancing setup. 
	SETUP_MP_MISSION_FOR_NETWORK(GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(missionScriptArgs.mdID.idMission) ,  missionScriptArgs )
	
//	thisScript = missionScriptArgs
	
	RESERVE_NETWORK_MISSION_PEDS(3)
	RESERVE_NETWORK_MISSION_VEHICLES(1)
	
	// This makes sure the net script is active, waits until it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
	
	// This script will not be paused if another script calls PAUSE_GAME
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	
	// KGM: Wait for the first network broadcast before moving on - call this as the last instruction in pre-game
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		NET_PRINT("[WJK] - Backup Heli - Failed to receive initial network broadcast. Cleaning up.")NET_NL()
		SCRIPT_CLEANUP()
	ENDIF
	
	IF NETWORK_IS_IN_TUTORIAL_SESSION()
		bOnMissionOnLaunch = TRUE
	ENDIF
	
	RESET_REMINDER_TIMER_FOR_ACTIVITY(FMMC_TYPE_HELI_STRIKE)
	
	playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_INI
	
ENDPROC


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                      Main Loop                             ////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///    

SCRIPT(MP_MISSION_DATA missionScriptArgs)

	#IF IS_DEBUG_BUILD	
		NET_PRINT_TIME() NET_PRINT_THREAD_ID() NET_PRINT("CnC: Starting AM_backup_heli \n") NET_NL()
	#ENDIF
	
	// Carry out all the initial game starting duties. 
	IF NETWORK_IS_GAME_IN_PROGRESS()
		
		PROCESS_PRE_GAME(missionScriptArgs)	

		// Setup widgets. Make sure this is done after PROCESS_PRE_GAME()
		#IF IS_DEBUG_BUILD
			CREATE_WIDGETS()
		#ENDIF
		
	ENDIF
	
	// Main loop.
	WHILE TRUE

		// One wait to rule them all. This can be the ONLY wait from here on in.
		MP_LOOP_WAIT_ZERO() 
		
		// Initiate script profiler.
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		SCRIPT_PROFILER_START_OF_FRAME()
		#ENDIF
		#ENDIF
		
//		// Get rid of any cop players who shouldn't be running this script.
//		IF IS_PLAYER_COP(PLAYER_ID())
//			NET_PRINT("SECURITY VAN: I am a sneaky sneaky cop that managed to run the security van script, time to go now.") NET_NL()
//			SCRIPT_CLEANUP()
//		ENDIF
		
		// If we have a match end event, bail.
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
     		PRINTLN("[BACKUP_HELI] - Script Cleanup 1)")
			SCRIPT_CLEANUP()
		ENDIF	
		
		IF NETWORK_IS_GAME_IN_PROGRESS()
			
			// Flags for one call per frame of expensive commands.
			MAINTAIN_START_OF_FRAME_FLAGS()
			
			// Maintain height heli should fly at.
			MAINTAIN_HELI_MINIMUM_FLIGHT_HEIGHT()
			
			// Deal with the debug.
			#IF IS_DEBUG_BUILD
				UPDATE_WIDGETS()
			#ENDIF		
		
			// -----------------------------------
			// Process your game logic.....
			SWITCH GET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT())
							
				// Wait until the server gives the all go before moving on.
				CASE GAME_STATE_INI		
					IF GET_SERVER_MISSION_STATE() > GAME_STATE_INI	
						playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_RUNNING					
					ENDIF				
				BREAK
				
				// Main gameplay state.
				CASE GAME_STATE_RUNNING			
					
					PROCESS_CLIENT()
					
					// Look for the server say the mission has ended.
					IF GET_SERVER_MISSION_STATE() = GAME_STATE_END
						PRINTLN("[BACKUP_HELI] - GAME_STATE END 2")
						playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END			
					ENDIF	
					
				BREAK	
	
				CASE GAME_STATE_END	
					PRINTLN("[BACKUP_HELI] - Script Cleanup 2)")
					SCRIPT_CLEANUP()
				BREAK
				
			ENDSWITCH
			
			// -----------------------------------
			// Process server game logic		
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				
				SWITCH GET_SERVER_MISSION_STATE()
					
					CASE GAME_STATE_INI	
						serverBD.iServerGameState = GAME_STATE_RUNNING
					BREAK
					
					// Look for game end conditions.
					CASE GAME_STATE_RUNNING		
						
						// Process security van script server logic.
						PROCESS_SERVER()
						
						#IF IS_DEBUG_BUILD
							IF bHostEndMissionNow	
								PRINTLN("[BACKUP_HELI] - GAME_STATE END 3")
								serverBD.iServerGameState = GAME_STATE_END
							ENDIF
						#ENDIF	
						
					BREAK
					
					CASE GAME_STATE_END			
										
						// We'd want to upload scores here, show results, do whatever.
						
					BREAK	
					
				ENDSWITCH

			ENDIF
		
		ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
			SCRIPT_PROFILER_END_OF_FRAME()
		#ENDIF
		#ENDIF
		
		
	ENDWHILE
	
// End of Mission
ENDSCRIPT
