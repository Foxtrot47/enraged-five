
// ********************   For 1738980 - uncomment SEND_NPC_CLIENT and PROCESS_MISSION_SERVER
















/// Player's will invites to jobs from NPCs if they haven't done anything for a while 
///    Dave W

/// 


USING "globals.sch"

// Game Headers
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_camera.sch"
USING "commands_hud.sch"

// Network Headers

USING "net_events.sch"

// 

USING "net_mission.sch"
USING "net_scoring_common.sch"
USING "commands_path.sch"
USING "net_blips.sch"
USING "commands_zone.sch"

USING "hud_drawing.sch"
USING "net_ambience.sch"
USING "FM_Unlocks_header.sch"


USING "shop_public.sch"
USING "net_garages.sch"
USING "net_mission_joblist.sch"
USING "net_activity_selector_groups.sch"

USING "net_wait_zero.sch"

USING "net_cash_transactions.sch"


USING "net_mission_details_overlay.sch"
USING "net_hud_displays.sch"
#IF IS_DEBUG_BUILD
	USING "profiler.sch"
#ENDIF



TWEAK_INT MIN_PLAYERS_FOR_INVITE	1


TWEAK_INT MIN_TIME_FOR_NPC_INVITE	20000
TWEAK_INT MIN_TIME_NOT_DONE_ANYTHING	570000
TWEAK_INT TIME_DELAY_REQUEST_NPC_INVITE	10000

CONST_INT MAX_VALID_JOBS		200

// Game States
CONST_INT GAME_STATE_INI 		0
CONST_INT GAME_STATE_INI_SPAWN	1
CONST_INT GAME_STATE_RUNNING	2
CONST_INT GAME_STATE_LEAVE		3
CONST_INT GAME_STATE_FAILED		4
CONST_INT GAME_STATE_TERMINATE_DELAY 5
CONST_INT GAME_STATE_END		6


CONST_INT biS_AllPlayersFinished				0
CONST_INT biS_SomeoneDidDeathmatch				1
CONST_INT biS_SomeoneDidRace					2
CONST_INT biS_SomeoneDidParachute				3
CONST_INT biS_SomeoneDidSurvival				4
CONST_INT biS_SendNpcInvite						5
CONST_INT biS_SetupNextMissionType				6
CONST_INT biS_PlayerNotUnlockedParachuting		7
CONST_INT biS_PlayerNotUnlockedSurvival			8
CONST_INT biS_SHouldComeFromLester				9




// Everyone can read this data, only the server can update it. 
STRUCT ServerBroadcastData

	INT iServerBitSet
	INT iServerRecentlyPlayedBitset
	INT iServerGameState
	INT iServerMissionProg
	INT iMissionTypeForInvite
	//INT iNextPlayerForInvite = -1
	INT iNextPartForInvite = -1
	enumCharacterList charInvite
	
	INT iJobToUse
	
	SCRIPT_TIMER timeSinceLastNpcInvite
	SCRIPT_TIMER timePlayerRequestInvite
	SCRIPT_TIMER timeTerminate
ENDSTRUCT
ServerBroadcastData serverBD
INT iServerStaggeredCount
//INT iStaggeredNextPlayerForInvite = -1
INT iStaggeredNextPartForInvite = -1
INT iValidJobs[MAX_VALID_JOBS]
INT iValidLesterJobs[MAX_VALID_JOBS]
INT iSendInviteProg
INT iNumValidJobs
INT iNumLesterValidJobs
INT iLocalJobToUse


SCRIPT_TIMER timeUpdatePlayedList
SCRIPT_TIMER timeLesterJob

// Player bits



STRUCT PlayerBroadcastData
	INT iPlayerBitset
	INT iGameState
	INT iMissionProg
	
ENDSTRUCT
PlayerBroadcastData PlayerBD[NUM_NETWORK_PLAYERS]

CONST_INT biP_UnlockedParachuting			0
CONST_INT biP_UnlockedSurvival				1
CONST_INT biP_NotDoneAnything				2
CONST_INT biP_CustomMenuOnScreen			3
//INT iPlayerStaggeredCount

SCRIPT_TIMER timeNotDoneAnything
SCRIPT_TIMER timeMyUnlockedJobs

INT iBoolsBitSet
CONST_INT biL_InitSendInvite			0

INT iServerStaggeredBitset

CONST_INT biSt_ServerLoopedThroughEveryone			0
CONST_INT biSt_FoundSOmeoneNotDoneAnything			1
CONST_INT biSt_UpdateRecentlyPlayed					2
CONST_INT biSt_FoundPlayerNotUnlockedParachute		3
CONST_INT biSt_FoundPlayerNotUnlockedSurvival		4
CONST_INT biSt_SomeoneRequestedNpcInvite			5


INT iNumberOfPlayersReady

INT iLamarRequestJobProg
INT iLamarDiagProg
SCRIPT_TIMER timeLamarJob

INT iSimeonRequestJobProg
INT iSimeonDiagProg
SCRIPT_TIMER timeSimeonJob

INT iMartinRequestJobProg
INT iMartinDiagProg
SCRIPT_TIMER timeMartinJob

INT iRonRequestJobProg
INT iRonDiagProg
SCRIPT_TIMER timeRonJob


INT iLesterRequestHeistProg
INT iLesterHeistDiagProg
SCRIPT_TIMER timeLesterHeist
INT iMyHeistStrand


#IF IS_DEBUG_BUILD
	BOOL bWdDoSendInviteDebug
	BOOL bWdNewInvite
	BOOL bWdRequestAnInvite
	BOOL bDisableAllDebug = FALSE
	BOOL bWdDoStaggeredDebug = FALSE
	BOOL bAllowSamePlayerInvite
	PROC NET_DW_PRINT(STRING sText)
		IF NOT bDisableAllDebug
			NET_PRINT_TIME() NET_PRINT("[amNpcInvite] [DSW] ") NET_PRINT_STRINGS(" ", sText) NET_NL()
		ENDIF
	ENDPROC 

	PROC NET_DW_PRINT_STRING_INT(STRING sText1, INT i)
		IF NOT bDisableAllDebug
			NET_PRINT_TIME() NET_PRINT("[amNpcInvite] [DSW] ") NET_PRINT_STRING_INT(sText1, i) NET_NL()
		ENDIF
	ENDPROC

	PROC NET_DW_PRINT_STRING_FLOAT(STRING sText1, FLOAT f)
		IF NOT bDisableAllDebug
			NET_PRINT_TIME() NET_PRINT("[amNpcInvite] [DSW] ") NET_PRINT_STRING_FLOAT(sText1, f) NET_NL()
		ENDIF
	ENDPROC

	PROC NET_DW_PRINT_STRING_VECTOR(STRING sText1, VECTOR v)
		IF NOT bDisableAllDebug
			NET_PRINT_TIME() NET_PRINT("[amNpcInvite] [DSW] ") NET_PRINT_STRING_VECTOR(sText1, v) NET_NL()
		ENDIF
	ENDPROC
	
	PROC NET_DW_PRINT_STRINGS(STRING sText1, STRING sText2)
		IF NOT bDisableAllDebug
			NET_PRINT_TIME() NET_PRINT("[amNpcInvite] [DSW] ") NET_PRINT_STRINGS(sText1, sText2) NET_NL()
		ENDIF
	ENDPROC
	
	PROC NET_DW_PRINT_STRING_WITH_PLAYER_NAME(STRING sText, PLAYER_INDEX player)
		IF NOT bDisableAllDebug
			NET_PRINT_TIME() NET_PRINT("[amNpcInvite] [DSW] ") NET_PRINT_STRINGS(" ", sText) NET_PRINT_STRINGS(" ", GET_PLAYER_NAME(player)) NET_NL()
		ENDIF
	ENDPROC
	
	PROC NET_DW_PRINT_HOST_NAME()
		NET_PRINT_TIME() NET_PRINT("[amNpcInvite] [DSW] Current host... ") NET_PRINT(GET_PLAYER_NAME(NETWORK_GET_HOST_OF_SCRIPT("am_npc_invites"))) NET_NL() 
	ENDPROC
	
	PROC CREATE_WIDGETS()
		START_WIDGET_GROUP("Npc Invites")
			//Profiling widgets
			#IF SCRIPT_PROFILER_ACTIVE
			CREATE_SCRIPT_PROFILER_WIDGET()
			#ENDIF		
		
			START_WIDGET_GROUP("Player")
				ADD_WIDGET_BOOL("Request Npc Invite", bWdRequestAnInvite)
				ADD_WIDGET_INT_SLIDER("Delay after request", TIME_DELAY_REQUEST_NPC_INVITE, 1000, 10000000, 1)
				ADD_WIDGET_INT_SLIDER("MIN_TIME_NOT_DONE_ANYTHING ", MIN_TIME_NOT_DONE_ANYTHING, 1000, 10000000, 1)
				ADD_WIDGET_INT_READ_ONLY("Mission Prog", PlayerBD[PARTICIPANT_ID_TO_INT()].iMissionProg)
			//	ADD_BIT_FIELD_WIDGET("Local", iBoolsBitSet)
				ADD_BIT_FIELD_WIDGET("Player", PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet)
				
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Server")
				ADD_WIDGET_BOOL("Staggered debug ", bWdDoStaggeredDebug)
				ADD_WIDGET_BOOL("Allow same player invite", bAllowSamePlayerInvite)
				ADD_WIDGET_BOOL("Do invite debug ", bWdDoSendInviteDebug) 
				ADD_WIDGET_BOOL("New Invite ", bWdNewInvite)
				ADD_WIDGET_INT_SLIDER("Min time between invites", MIN_TIME_FOR_NPC_INVITE, 0, 10000000, 1)
				ADD_WIDGET_INT_SLIDER("Min players", MIN_PLAYERS_FOR_INVITE, 1, NUM_NETWORK_PLAYERS, 1)
				ADD_BIT_FIELD_WIDGET("Server", serverBD.iServerBitSet)
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP() 
	ENDPROC
	
	PROC UPDATE_WIDGETS()
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			IF bWdNewInvite
				bWdNewInvite = FALSE
				IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_SendNpcInvite)
					SET_BIT(serverBD.iServerBitSet, biS_SendNpcInvite)
					CLEAR_BIT(iBoolsBitSet, biL_InitSendInvite)
					NET_DW_PRINT("biS_SendNpcInvite set via widget")
				ENDIF
			ENDIF
		ENDIF
		
		IF bWdRequestAnInvite
			bWdRequestAnInvite = FALSE
			SET_BIT(MPGlobalsAmbience.iNpcInviteBitset, biNPC_WantNpcInvite)
			NET_DW_PRINT("Widget setting biNPC_WantNpcInvite")
		ENDIF
	ENDPROC
#ENDIF

PROC UPDATE_MY_UNLOCKED_JOBS()
	#IF IS_DEBUG_BUILD
		NET_DW_PRINT("UPDATE_MY_UNLOCKED_JOBS called...")
	#ENDIF
	IF NOT IS_BIT_SET(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitset, biP_UnlockedParachuting)
		IF IS_FM_TYPE_UNLOCKED(FMMC_TYPE_BASE_JUMP)
			SET_BIT(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitset, biP_UnlockedParachuting)
			#IF IS_DEBUG_BUILD NET_DW_PRINT("I've unlocked parachuting") #ENDIF
		ELSE
			#IF IS_DEBUG_BUILD NET_DW_PRINT("I've NOT unlocked parachuting") #ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitset, biP_UnlockedSurvival)
		IF IS_FM_TYPE_UNLOCKED(FMMC_TYPE_SURVIVAL)
			SET_BIT(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitset, biP_UnlockedSurvival)
			#IF IS_DEBUG_BUILD NET_DW_PRINT("I've unlocked survival") #ENDIF
		ELSE	
			#IF IS_DEBUG_BUILD NET_DW_PRINT("I've NOT unlocked survival") #ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC BROADCAST_PLAYER_REQUEST_NPC_INVITE()
	#IF IS_DEBUG_BUILD 
		PRINTLN("[amNpcInvite] [dsw] [BROADCAST_PLAYER_REQUEST_NPC_INVITE] I'd like an npc invite... ", NATIVE_TO_INT(PLAYER_ID()))
	#ENDIF
	SCRIPT_EVENT_FM_PLAYER_REQUEST_NPC_INVITE Event
	event.Details.Type = SCRIPT_EVENT_REQUEST_NPC_INVITE
	Event.details.FromPlayerIndex = PLAYER_ID()
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), ALL_PLAYERS())
ENDPROC

INT iLesterRequestJobProg
INT iLesterDiagProg
INT iMyNextLesterJobType
INT iMyLastLesterJobType
INT iMyNextLesterJob
FUNC INT GET_MY_NEXT_LESTER_JOB_TYPE()
	INT iNextJob = -1

	IF iNextJob = -1
		IF NOT IS_BIT_SET(serverBD.iServerRecentlyPlayedBitset , biS_SomeoneDidRace)
			IF iMyLastLesterJobType <> FMMC_TYPE_RACE
				iNextJob = FMMC_TYPE_RACE
				#IF IS_DEBUG_BUILD NET_DW_PRINT("[GET_MY_NEXT_LESTER_JOB_TYPE] iNextJobType = FMMC_TYPE_RACE") #ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF iNextJob = -1
		IF NOT IS_BIT_SET(serverBD.iServerRecentlyPlayedBitset , biS_SomeoneDidDeathmatch)
			IF iMyLastLesterJobType <> FMMC_TYPE_DEATHMATCH
				iNextJob = FMMC_TYPE_DEATHMATCH
				#IF IS_DEBUG_BUILD NET_DW_PRINT("[GET_MY_NEXT_LESTER_JOB_TYPE] iNextJobType = FMMC_TYPE_DEATHMATCH") #ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF iNextJob = -1
		IF NOT IS_BIT_SET(serverBD.iServerRecentlyPlayedBitset , biS_SomeoneDidSurvival)
			IF IS_FM_TYPE_UNLOCKED(FMMC_TYPE_SURVIVAL)
				IF iMyLastLesterJobType <> FMMC_TYPE_SURVIVAL
					iNextJob = FMMC_TYPE_SURVIVAL
					#IF IS_DEBUG_BUILD NET_DW_PRINT("[GET_MY_NEXT_LESTER_JOB_TYPE] iNextJobType = FMMC_TYPE_SURVIVAL") #ENDIF
				ENDIF
			ELSE	
				#IF IS_DEBUG_BUILD NET_DW_PRINT("[GET_MY_NEXT_LESTER_JOB_TYPE] Not picking FMMC_TYPE_SURVIVAL as not unlocked") #ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF iNextJob = -1
		IF NOT IS_BIT_SET(serverBD.iServerRecentlyPlayedBitset , biS_SomeoneDidParachute)
			IF IS_FM_TYPE_UNLOCKED(FMMC_TYPE_BASE_JUMP)
				IF iMyLastLesterJobType <> FMMC_TYPE_BASE_JUMP
					iNextJob = FMMC_TYPE_BASE_JUMP
					#IF IS_DEBUG_BUILD NET_DW_PRINT("[GET_MY_NEXT_LESTER_JOB_TYPE] iNextJobType = FMMC_TYPE_BASE_JUMP") #ENDIF
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD NET_DW_PRINT("[GET_MY_NEXT_LESTER_JOB_TYPE] Not picking FMMC_TYPE_BASE_JUMP as not unlocked") #ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	
	IF iNextJob = -1
		#IF IS_DEBUG_BUILD NET_DW_PRINT("[GET_MY_NEXT_LESTER_JOB_TYPE] Didn't find a job type, will pick one at random") #ENDIF
		
		// Rank requirements:
		// FMMC_TYPE_RACE = FMMC_TYPE_DEATHMATCH < FMMC_TYPE_BASE_JUMP < FMMC_TYPE_SURVIVAL
		
		//-- Pick a job at random, but don't pick something that a Player that needs a job hasn't unlocked
		INT iMaxRand
		IF NOT IS_FM_TYPE_UNLOCKED(FMMC_TYPE_BASE_JUMP)
			iMaxRand = 2
		ELIF NOT IS_FM_TYPE_UNLOCKED(FMMC_TYPE_SURVIVAL)
			iMaxRand = 3
		ELSE
			iMaxRand = 4
		ENDIF
		
		INT iRand = GET_RANDOM_INT_IN_RANGE(0, iMaxRand)
		
		IF iRand = 0 iNextJob = FMMC_TYPE_RACE
		ELIF iRand = 1 iNextJob = FMMC_TYPE_DEATHMATCH
		ELIF iRand = 2 iNextJob = FMMC_TYPE_BASE_JUMP
		ELIF iRand = 3 iNextJob = FMMC_TYPE_SURVIVAL
		ENDIF
		
	ENDIF
	
	RETURN iNextJob
ENDFUNC	
FUNC BOOL REQUEST_A_JOB_FROM_LESTER()
	INT i
	INT index
	INT iNewJob
	SWITCH iLesterRequestJobProg
		CASE 0
			IF NOT HAS_NET_TIMER_STARTED(timeLesterJob)
				START_NET_TIMER(timeLesterJob)
				#IF IS_DEBUG_BUILD NET_DW_PRINT("[REQUEST_A_JOB_FROM_LESTER] Started timeLesterJob") #ENDIF
			ELSE
				IF HAS_NET_TIMER_EXPIRED(timeLesterJob, TIME_DELAY_REQUEST_NPC_INVITE)
					iMyNextLesterJobType = GET_MY_NEXT_LESTER_JOB_TYPE()
					IF iMyNextLesterJobType >= 0
						iMyLastLesterJobType = iMyNextLesterJobType
						REPEAT MAX_VALID_JOBS i
							iValidLesterJobs[i] = -1
						ENDREPEAT
					
						iNumLesterValidJobs = 0
						iLesterRequestJobProg++
						#IF IS_DEBUG_BUILD
							NET_DW_PRINT_STRINGS("[REQUEST_A_JOB_FROM_LESTER] My next Lester job = ", GET_FMMC_BIT_SET_NAME_FOR_LOGS(iMyNextLesterJobType))
							NET_DW_PRINT("[REQUEST_A_JOB_FROM_LESTER] iLesterRequestJobProg = 1")
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
			FOR i = 0 TO FMMC_MAX_AT_ONCE_ROCKSTAR_CREATED - 1
				IF iNumLesterValidJobs < (MAX_VALID_JOBS - 1)	
					IF IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iBitSet, ciROCKSTAR_CREATED_MISSION_IN_USE)
						IF g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iType = iMyNextLesterJobType
							iValidLesterJobs[iNumLesterValidJobs] = i
							iNumLesterValidJobs++
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
			
			iLesterRequestJobProg++
			#IF IS_DEBUG_BUILD
				NET_DW_PRINT("[REQUEST_A_JOB_FROM_LESTER] iLesterRequestJobProg = 2")
			#ENDIF
		BREAK
		
		CASE 2
			IF iNumLesterValidJobs > 0
				index = GET_RANDOM_INT_IN_RANGE(0, iNumLesterValidJobs - 1)
				#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[REQUEST_A_JOB_FROM_LESTER] Random int... ", index) #ENDIF
				
				iNewJob = iValidLesterJobs[index] 
				#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[REQUEST_A_JOB_FROM_LESTER] using job ", iNewJob) #ENDIF
				
				//IF g_FMMC_ROCKSTAR_CREATED.bInUse[iJobToUse]
				IF IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iNewJob].iBitSet, ciROCKSTAR_CREATED_MISSION_IN_USE)
					IF g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iNewJob].iType = iMyNextLesterJobType
						#IF IS_DEBUG_BUILD 
							NET_DW_PRINT("[REQUEST_A_JOB_FROM_LESTER] Job is valid!")  
							NET_DW_PRINT_STRINGS("[REQUEST_A_JOB_FROM_LESTER] Job name... ", g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iNewJob].tlMissionName)
						#ENDIF
						
						
						iMyNextLesterJob = iNewJob
	
						
						iLesterRequestJobProg++
						
						#IF IS_DEBUG_BUILD
							NET_DW_PRINT("[REQUEST_A_JOB_FROM_LESTER] iLesterRequestJobProg = 3")
						#ENDIF
					ELSE
						#IF IS_DEBUG_BUILD 
							NET_DW_PRINT_STRING_INT("[REQUEST_A_JOB_FROM_LESTER] g_FMMC_ROCKSTAR_CREATED.iType <> iMissionType! g_FMMC_ROCKSTAR_CREATED.iType = ", g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iNewJob].iType) 
							NET_DW_PRINT_STRING_INT("[REQUEST_A_JOB_FROM_LESTER] iMissionType = ", iMyNextLesterJobType)
						#ENDIF
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD NET_DW_PRINT("[REQUEST_A_JOB_FROM_LESTER] bInUse not set! ") #ENDIF
				ENDIF
				
			ELSE
				
				iMyNextLesterJob = 99
				#IF IS_DEBUG_BUILD NET_DW_PRINT("[REQUEST_A_JOB_FROM_LESTER] iNumValidJobs = 0!") #ENDIF
				#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[REQUEST_A_JOB_FROM_LESTER] iLesterRequestJobProg = ", iLesterRequestJobProg) #ENDIF
			ENDIF
		BREAK
		
		CASE 3
			#IF IS_DEBUG_BUILD
				NET_DW_PRINT("[REQUEST_A_JOB_FROM_LESTER] Sending invite...")
			#ENDIF
						
			Store_Invite_Details_For_Joblist_From_NPC(iMyNextLesterJobType,
				g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iMyNextLesterJob].iSubType,
	        	g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iMyNextLesterJob].vStartPos,
	       		FMMC_ROCKSTAR_CREATOR_ID,
	        	CHAR_LESTER,
	        	g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iMyNextLesterJob].tlMissionName,
	        	"",
				g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iMyNextLesterJob].tlName
				)
				
			iLesterRequestJobProg = 99
			#IF IS_DEBUG_BUILD
				NET_DW_PRINT("[REQUEST_A_JOB_FROM_LESTER] iLesterRequestJobProg = 99")
			#ENDIF
		BREAK
		
		CASE 99
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC



INT thisCMArrayPos
MP_MISSION_ID_DATA mpMissionIdData
/*

FUNC BOOL REQUEST_A_JOB_FROM_LESTER_V2()

	INT iRank
	SWITCH iLesterRequestJobProg
		CASE 0
			IF NOT HAS_NET_TIMER_STARTED(timeLesterJob)
				START_NET_TIMER(timeLesterJob)
				#IF IS_DEBUG_BUILD NET_DW_PRINT("[REQUEST_A_JOB_FROM_LESTER] Started timeLesterJob") #ENDIF
			ELSE
				IF HAS_NET_TIMER_EXPIRED(timeLesterJob, TIME_DELAY_REQUEST_NPC_INVITE)
			        iLesterRequestJobProg++
					#IF IS_DEBUG_BUILD
						NET_DW_PRINT("[REQUEST_A_JOB_FROM_LESTER] iLesterRequestJobProg = 1")
					#ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
			iRank = (GET_PLAYER_RANK(PLAYER_ID()) - 1)
			#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[REQUEST_A_JOB_FROM_LESTER] iRank = ", iRank) #ENDIF
			
			thisCMArrayPos = Get_Random_Contact_Mission_Matching_These_Details(CHAR_LESTER, MIN_PENDING_ACTIVITIES_RANK, iRank)
			
			IF (thisCMArrayPos <> ILLEGAL_CONTACT_MISSION_SLOT)
				iLesterRequestJobProg++
				#IF IS_DEBUG_BUILD
					NET_DW_PRINT("[REQUEST_A_JOB_FROM_LESTER] iLesterRequestJobProg = 2")
				#ENDIF
			ELSE
				#IF IS_DEBUG_BUILD	
					NET_DW_PRINT("[REQUEST_A_JOB_FROM_LESTER] Case 1 Set BI_FM_NMH4_LEST_JOB_FAILED")
					NET_DW_PRINT("[REQUEST_A_JOB_FROM_LESTER] Case 1 thisCMArrayPos = ILLEGAL_CONTACT_MISSION_SLOT setting iLesterRequestJobProg = 99")
				#ENDIF
				SET_BIT(MPGlobalsAmbience.iFmNmhBitSet4, BI_FM_NMH4_LEST_JOB_FAILED)
				iLesterRequestJobProg = 99
			ENDIF
		BREAK
		
		CASE 2
			mpMissionIdData = Gather_And_Get_MissionID_Data_For_Contact_Mission(thisCMArrayPos)
			
			//DOES_ANY_CONTACT_MISSION_EXIST_FOR_CONTACT(CHAR_LESTER)
			
			IF (Are_These_FM_Cloud_Loaded_Activity_Details_Available_Locally(mpMissionIdData))
			    iLesterRequestJobProg++
				#IF IS_DEBUG_BUILD
					NET_DW_PRINT("[REQUEST_A_JOB_FROM_LESTER] iLesterRequestJobProg = 3")
				#ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
					NET_DW_PRINT("[REQUEST_A_JOB_FROM_LESTER] Case 2 Set BI_FM_NMH4_LEST_JOB_FAILED")
					NET_DW_PRINT("[REQUEST_A_JOB_FROM_LESTER] Case 2 Are_These_FM_Cloud_Loaded_Activity_Details_Available_Locally returned false setting iLesterRequestJobProg = 99")
				#ENDIF
				SET_BIT(MPGlobalsAmbience.iFmNmhBitSet4, BI_FM_NMH4_LEST_JOB_FAILED)
				iLesterRequestJobProg = 99
			ENDIF

		BREAK
		
		CASE 3
			#IF IS_DEBUG_BUILD
				VECTOR vCoords
				TEXT_LABEL_31 tl31MissionName
				TEXT_LABEL_63 tl63MissionDesc
				
				vCoords 		= Get_Coordinates_For_FM_Cloud_Loaded_Activity(mpMissionIdData)
				tl31MissionName = Get_Mission_Name_For_FM_Cloud_Loaded_Activity(mpMissionIdData)
				tl63MissionDesc = Get_Mission_Description_For_FM_Cloud_Loaded_Activity(mpMissionIdData)
			
				NET_DW_PRINT("[REQUEST_A_JOB_FROM_LESTER] Sending invite...")
				PRINTLN("[amNpcInvite] [DSW] [REQUEST_A_JOB_FROM_LESTER] Get_Coordinates_For_FM_Cloud_Loaded_Activity(mpMissionIdData) = ", 
				vCoords)
				PRINTLN("[amNpcInvite] [DSW] [REQUEST_A_JOB_FROM_LESTER] mpMissionIdData.idCreator = ",
					mpMissionIdData.idCreator)
				PRINTLN("[amNpcInvite] [DSW] [REQUEST_A_JOB_FROM_LESTER] Get_Mission_Name_For_FM_Cloud_Loaded_Activity(mpMissionIdData) = ", 
					tl31MissionName)
				PRINTLN("[amNpcInvite] [DSW] [REQUEST_A_JOB_FROM_LESTER] 	Get_Mission_Description_For_FM_Cloud_Loaded_Activity(mpMissionIdData) = ", 
					tl63MissionDesc)
			#ENDIF
			
			Setup_Contact_Mission_From_CM_Data_And_Send_Invite(mpMissionIdData)
				
			iLesterRequestJobProg = 99
			#IF IS_DEBUG_BUILD
				NET_DW_PRINT("[REQUEST_A_JOB_FROM_LESTER] iLesterRequestJobProg = 99")
			#ENDIF
		BREAK
		
		CASE 99
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC
*/
structPedsForConversation convReqJob
PROC ADD_PED_FOR_REQUEST_JOB_DIALOGUE(enumCharacterList contactJob)
	SWITCH contactJob
		CASE CHAR_LESTER
			ADD_PED_FOR_DIALOGUE(convReqJob, 3, NULL, "Lester")
		BREAK
		CASE CHAR_MP_GERALD
			ADD_PED_FOR_DIALOGUE(convReqJob, 3, NULL, "Gerald")
		BREAK
		CASE CHAR_SIMEON
			ADD_PED_FOR_DIALOGUE(convReqJob, 3, NULL, "Simeon")
		BREAK
		CASE CHAR_MARTIN
			ADD_PED_FOR_DIALOGUE(convReqJob, 3, NULL, "Martin")
		BREAK
		CASE CHAR_RON
			ADD_PED_FOR_DIALOGUE(convReqJob, 2, NULL, "NervousRon")
		BREAK
	ENDSWITCH
	
	
ENDPROC

FUNC TEXT_LABEL GET_JOB_AVAILABLE_LABEL_FOR_CONTACT(enumCharacterList contactJob, BOOL bIsHeist = FALSE)
	TEXT_LABEL tlAvail
	
	SWITCH contactJob
		CASE CHAR_LESTER
			IF NOT bIsHeist
				tlAvail = "MPCT_Job"
			ELSE
				tlAvail = "MPCT_Lconf"
			ENDIF
		BREAK
		CASE CHAR_MP_GERALD
			tlAvail = "MPCT_GJob"
		BREAK
		CASE CHAR_SIMEON
			tlAvail = "MPCT_SJob"
		BREAK
		CASE CHAR_MARTIN
			tlAvail = "MPCT_MJob"
		BREAK
		CASE CHAR_RON
			tlAvail = "MPCT_RJob"
		BREAK
	ENDSWITCH
	
	RETURN tlAvail
ENDFUNC

FUNC TEXT_LABEL GET_NO_JOB_AVAILABLE_LABEL_FOR_CONTACT(enumCharacterList contactJob)
	TEXT_LABEL tlNotAvail
	
	SWITCH contactJob
		CASE CHAR_LESTER
			tlNotAvail = "MPCT_Jobno"
		BREAK
		CASE CHAR_MP_GERALD
			tlNotAvail = "MPCT_GJobno"
		BREAK
		CASE CHAR_SIMEON
			tlNotAvail = "MPCT_SJobno"
		BREAK
		CASE CHAR_MARTIN
			tlNotAvail = "MPCT_MJobno"
		BREAK
		CASE CHAR_RON
			tlNotAvail = "MPCT_RJobno"
		BREAK
	ENDSWITCH
	
	RETURN tlNotAvail
ENDFUNC
FUNC BOOL MAINTAIN_DIALOGUE_FOR_REQUEST_JOB(enumCharacterList contactJob, INT &iDiagJobProg, BOOL bIsHeist = FALSE)
	TEXT_LABEL tl
	INT iCurrentRank
	SWITCH iDiagJobProg
		CASE 0
			ADD_PED_FOR_REQUEST_JOB_DIALOGUE(contactJob)
			iDiagJobProg++
			#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[MAINTAIN_DIALOGUE_FOR_REQUEST_JOB] Added contact, iDiagJobProg = ", iDiagJobProg) #ENDIF
		BREAK
		
		CASE 1
			IF NOT bIsHeist
				// KGM 26/10/14: Swap to use the same function as 'get Contact Mission' routine below to ensure consistency - the checks are more complex now
				PRINTLN(".KGM [ActSelect][ContactMission]: AM_NPC_INVITES checking if a Contact Mission exists for contact")
				iCurrentRank = GET_PLAYER_RANK(PLAYER_ID())
				#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[MAINTAIN_DIALOGUE_FOR_REQUEST_JOB] iCurrentRank = ", iCurrentRank) #ENDIF
			
				thisCMArrayPos = Get_Random_Contact_Mission_For_Contact(contactJob, iCurrentRank)
			
				IF (thisCMArrayPos <> ILLEGAL_CONTACT_MISSION_SLOT)
					// Job available
					iDiagJobProg = 2
					#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[MAINTAIN_DIALOGUE_FOR_REQUEST_JOB] Job exists, iDiagJobProg = ", iDiagJobProg) #ENDIF
				ELSE
					// Job not available
					iDiagJobProg = 50
					#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[MAINTAIN_DIALOGUE_FOR_REQUEST_JOB] No job exists, iDiagJobProg = ", iDiagJobProg) #ENDIF
				ENDIF
			ELSE
				
				IF DOES_ANY_VALID_HEIST_STRAND_EXIST_FOR_CONTACT(contactJob)	
					iDiagJobProg = 2
					#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[MAINTAIN_DIALOGUE_FOR_REQUEST_JOB] Heist exists, iDiagJobProg = ", iDiagJobProg) #ENDIF
				ELSE
					iDiagJobProg = 50
					#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[MAINTAIN_DIALOGUE_FOR_REQUEST_JOB] No Heist exists, iDiagJobProg = ", iDiagJobProg) #ENDIF
				ENDIF
				
			ENDIF
		BREAK
		
		CASE 2
			tl = GET_JOB_AVAILABLE_LABEL_FOR_CONTACT(contactJob, bIsHeist)
			IF CREATE_CONVERSATION(convReqJob,"CT_AUD",tl,CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT)
				iDiagJobProg = 99
				#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[MAINTAIN_DIALOGUE_FOR_REQUEST_JOB] Started job exists dialogue, iDiagJobProg = ", iDiagJobProg) #ENDIF
			
			ENDIF
		BREAK
		
		CASE 50
			tl = GET_NO_JOB_AVAILABLE_LABEL_FOR_CONTACT(contactJob)
			IF CREATE_CONVERSATION(convReqJob,"CT_AUD",tl,CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT)
				iDiagJobProg = 99
				#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[MAINTAIN_DIALOGUE_FOR_REQUEST_JOB] Started NO job exists dialogue, iDiagJobProg = ", iDiagJobProg) #ENDIF
			
			ENDIF
		BREAK
		
		CASE 99
			RETURN TRUE
		BREAK
	ENDSWITCH	
	RETURN FALSE
ENDFUNC
FUNC BOOL REQUEST_A_JOB_FROM_CONTACT(enumCharacterList contactJob, INT &iReqJobProg, SCRIPT_TIMER &timeBetweenJobs, INT &iBitset, INT iFailedBit)

	INT iCurrentRank
	SWITCH iReqJobProg
		CASE 0
			IF NOT HAS_NET_TIMER_STARTED(timeBetweenJobs)
				START_NET_TIMER(timeBetweenJobs)
				#IF IS_DEBUG_BUILD 
					IF contactJob = CHAR_MP_GERALD
						NET_DW_PRINT("[REQUEST_A_JOB_FROM_CONTACT] Started timeBetweenJobs. Contact is Gerald") 
					ELIF contactJob = CHAR_LESTER
						NET_DW_PRINT("[REQUEST_A_JOB_FROM_CONTACT] Started timeBetweenJobs. Contact is Lester")
					ELIF contactJob = CHAR_SIMEON	
						NET_DW_PRINT("[REQUEST_A_JOB_FROM_CONTACT] Started timeBetweenJobs. Contact is Simeon")
					ELIF contactJob = CHAR_LESTER
						NET_DW_PRINT("[REQUEST_A_JOB_FROM_CONTACT] Started timeBetweenJobs. Contact is lester")
					ELIF contactJob = CHAR_RON
						NET_DW_PRINT("[REQUEST_A_JOB_FROM_CONTACT] Started timeBetweenJobs. Contact is Ron")
					ELSE
						NET_DW_PRINT("[REQUEST_A_JOB_FROM_CONTACT] Started timeBetweenJobs. Don't know who contact is!")
					ENDIF
				#ENDIF
			ELSE
				IF HAS_NET_TIMER_EXPIRED(timeBetweenJobs, TIME_DELAY_REQUEST_NPC_INVITE)
			        iReqJobProg++
					#IF IS_DEBUG_BUILD
						NET_DW_PRINT("[REQUEST_A_JOB_FROM_CONTACT] iReqJobProg = 1")
					#ENDIF
					
					CLEAR_BIT(iBitset, iFailedBit)
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
			iCurrentRank = GET_PLAYER_RANK(PLAYER_ID())
			#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[REQUEST_A_JOB_FROM_CONTACT] iCurrentRank = ", iCurrentRank) #ENDIF
			
			// KEITH 10/7/13: Now need to use the Player's Contact Mission Rank instead of Game Rank. Changed function to accept
			//			'Game Rank' (to avoid cyclic headers in deeper function) and 'Use of Rank' as parameters - I can then calculate everything else
			//thisCMArrayPos = Get_Random_Contact_Mission_Matching_These_Details(contactJob, iCurrentRank, CM_RANK_USE_PREVIOUSLY_PLAYED_BELOW_CURRENT_RANK)
			
			PRINTLN(".KGM [ActSelect][ContactMission]: AM_NPC_INVITES choosing a Contact Mission for Contact")
			thisCMArrayPos = Get_Random_Contact_Mission_For_Contact(contactJob, iCurrentRank)
			
			IF (thisCMArrayPos <> ILLEGAL_CONTACT_MISSION_SLOT)
				/*// Removed for 2002194 - This was previously causing an assert and was only useful for debugging on the shift+F11 screen
				// Found a mission, so store the details as a Contact Mission
				Store_Contact_Mission_Based_On_Contact_Mission_Slot(thisCMArrayPos, FALSE)
				
				// Ask the server if it is okay to send an invite for this mission
				Set_My_Contact_Mission_Stage(CONTACT_MISSION_CLIENT_STAGE_INVITE_SENT)
				*/
				iReqJobProg++
				#IF IS_DEBUG_BUILD
					NET_DW_PRINT("[REQUEST_A_JOB_FROM_CONTACT] iReqJobProg = 2")
				#ENDIF
			ELSE
				#IF IS_DEBUG_BUILD	
					NET_DW_PRINT("[REQUEST_A_JOB_FROM_CONTACT] Case 1 Set Failed bit")
					NET_DW_PRINT("[REQUEST_A_JOB_FROM_CONTACT] Case 1 thisCMArrayPos = ILLEGAL_CONTACT_MISSION_SLOT setting iLesterRequestJobProg = 99")
				#ENDIF
				SET_BIT(iBitset, iFailedBit)
				iReqJobProg = 99
			ENDIF
		BREAK
		
		CASE 2
			mpMissionIdData = Gather_And_Get_MissionID_Data_For_Contact_Mission(thisCMArrayPos)
			
			//DOES_ANY_CONTACT_MISSION_EXIST_FOR_CONTACT(CHAR_LESTER)
			
			IF (Are_These_FM_Cloud_Loaded_Activity_Details_Available_Locally(mpMissionIdData))
			    iReqJobProg++
				#IF IS_DEBUG_BUILD
					NET_DW_PRINT("[REQUEST_A_JOB_FROM_CONTACT] iReqJobProg = 3")
				#ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
					NET_DW_PRINT("[REQUEST_A_JOB_FROM_CONTACT] Case 2 Set Failed bit")
					NET_DW_PRINT("[REQUEST_A_JOB_FROM_CONTACT] Case 2 Are_These_FM_Cloud_Loaded_Activity_Details_Available_Locally returned false setting iReqJobProg = 99")
				#ENDIF
				SET_BIT(iBitset, iFailedBit)
				iReqJobProg = 99
			ENDIF

		BREAK
		
		CASE 3
			#IF IS_DEBUG_BUILD
				VECTOR vCoords
				TEXT_LABEL_31 tl31MissionName
				TEXT_LABEL_63 tl63MissionDesc
				
				vCoords 		= Get_Coordinates_For_FM_Cloud_Loaded_Activity(mpMissionIdData)
				tl31MissionName = Get_Mission_Name_For_FM_Cloud_Loaded_Activity(mpMissionIdData)
				tl63MissionDesc = Get_Mission_Description_For_FM_Cloud_Loaded_Activity(mpMissionIdData)
			
				NET_DW_PRINT("[REQUEST_A_JOB_FROM_CONTACT] Sending invite...")
				PRINTLN("[amNpcInvite] [DSW] [REQUEST_A_JOB_FROM_CONTACT] Get_Coordinates_For_FM_Cloud_Loaded_Activity(mpMissionIdData) = ", 
				vCoords)
				PRINTLN("[amNpcInvite] [DSW] [REQUEST_A_JOB_FROM_CONTACT] mpMissionIdData.idCreator = ",
					mpMissionIdData.idCreator)
				PRINTLN("[amNpcInvite] [DSW] [REQUEST_A_JOB_FROM_CONTACT] Get_Mission_Name_For_FM_Cloud_Loaded_Activity(mpMissionIdData) = ", 
					tl31MissionName)
				PRINTLN("[amNpcInvite] [DSW] [REQUEST_A_JOB_FROM_CONTACT] 	Get_Mission_Description_For_FM_Cloud_Loaded_Activity(mpMissionIdData) = ", 
					tl63MissionDesc)
			#ENDIF
			
			Setup_Contact_Mission_From_CM_Data_And_Send_Invite(mpMissionIdData)
				
			iReqJobProg = 99
			#IF IS_DEBUG_BUILD
				NET_DW_PRINT("[REQUEST_A_JOB_FROM_CONTACT] iReqJobProg = 99")
			#ENDIF
		BREAK
		
		CASE 99
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

//FUNC BOOL REQUEST_A_HEIST_FROM_CONTACT(enumCharacterList contactHeist, INT &iReqHeistProg, SCRIPT_TIMER &timeBetweenJobs, INT &iBitset, INT iFailedBit)
//
//	INT iCurrentRank
//	SWITCH iReqHeistProg
//		CASE 0
//			IF NOT HAS_NET_TIMER_STARTED(timeBetweenJobs)
//				START_NET_TIMER(timeBetweenJobs)
//				#IF IS_DEBUG_BUILD 
//					IF contactHeist = CHAR_MP_GERALD
//						NET_DW_PRINT("[REQUEST_A_HEIST_FROM_CONTACT] Started timeBetweenJobs. Contact is Gerald") 
//					ELIF contactHeist = CHAR_LESTER
//						NET_DW_PRINT("[REQUEST_A_HEIST_FROM_CONTACT] Started timeBetweenJobs. Contact is Lester")
//					ELIF contactHeist = CHAR_SIMEON	
//						NET_DW_PRINT("[REQUEST_A_HEIST_FROM_CONTACT] Started timeBetweenJobs. Contact is Simeon")
//					ELIF contactHeist = CHAR_LESTER
//						NET_DW_PRINT("[REQUEST_A_HEIST_FROM_CONTACT] Started timeBetweenJobs. Contact is lester")
//					ELIF contactHeist = CHAR_RON
//						NET_DW_PRINT("[REQUEST_A_HEIST_FROM_CONTACT] Started timeBetweenJobs. Contact is Ron")
//					ELSE
//						NET_DW_PRINT("[REQUEST_A_HEIST_FROM_CONTACT] Started timeBetweenJobs. Don't know who contact is!")
//					ENDIF
//				#ENDIF
//			ELSE
//				IF HAS_NET_TIMER_EXPIRED(timeBetweenJobs, TIME_DELAY_REQUEST_NPC_INVITE)
//					iMyHeistStrand = ILLEGAL_HEIST_STRAND_SLOT
//					
//			        iReqHeistProg++
//					#IF IS_DEBUG_BUILD
//						NET_DW_PRINT("[REQUEST_A_HEIST_FROM_CONTACT] iReqHeistProg = 1")
//					#ENDIF
//					
//					CLEAR_BIT(iBitset, iFailedBit)
//				ENDIF
//			ENDIF
//		BREAK
//		
//		CASE 1
//			iCurrentRank = GET_PLAYER_RANK(PLAYER_ID())
//			#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[REQUEST_A_HEIST_FROM_CONTACT] iCurrentRank = ", iCurrentRank) #ENDIF
//			
//			iMyHeistStrand = Get_Random_Heist_Strand_Matching_These_Details(contactHeist, iCurrentRank) 
//			
//			IF (iMyHeistStrand <> ILLEGAL_HEIST_STRAND_SLOT)
//				#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[REQUEST_A_HEIST_FROM_CONTACT] Heist strand valid. Strand = ", iMyHeistStrand) #ENDIF
//				Setup_Specific_Heist_Strand(iMyHeistStrand)
//				iReqHeistProg++
//				#IF IS_DEBUG_BUILD
//					NET_DW_PRINT("[REQUEST_A_HEIST_FROM_CONTACT] iReqHeistProg = 2")
//				#ENDIF
//			ELSE
//				#IF IS_DEBUG_BUILD	
//					NET_DW_PRINT("[REQUEST_A_HEIST_FROM_CONTACT] Case 1 Set Failed bit")
//					NET_DW_PRINT("[REQUEST_A_HEIST_FROM_CONTACT] Case 1 thisCMArrayPos = ILLEGAL_HEIST_STRAND_SLOT setting iMyHeistStrand = 99")
//				#ENDIF
//				SET_BIT(iBitset, iFailedBit)
//				iReqHeistProg = 99
//			ENDIF
//		BREAK
//		
//		CASE 99
//			RETURN TRUE
//		BREAK
//	ENDSWITCH
//	
//	RETURN FALSE
//ENDFUNC

FUNC BOOL REQUEST_A_HEIST_FROM_CONTACT_V2(enumCharacterList contactHeist, INT &iReqHeistProg, SCRIPT_TIMER &timeBetweenJobs, INT &iBitset, INT iFailedBit)

	INT iCurrentRank
	IF iCurrentRank = iCurrentRank
	ENDIF
	IF contactHeist = contactHeist
	ENDIF
	SWITCH iReqHeistProg
		CASE 0
			IF NOT HAS_NET_TIMER_STARTED(timeBetweenJobs)
				START_NET_TIMER(timeBetweenJobs)
				#IF IS_DEBUG_BUILD 
					IF contactHeist = CHAR_MP_GERALD
						NET_DW_PRINT("[REQUEST_A_HEIST_FROM_CONTACT] Started timeBetweenJobs. Contact is Gerald") 
					ELIF contactHeist = CHAR_LESTER
						NET_DW_PRINT("[REQUEST_A_HEIST_FROM_CONTACT] Started timeBetweenJobs. Contact is Lester")
					ELIF contactHeist = CHAR_SIMEON	
						NET_DW_PRINT("[REQUEST_A_HEIST_FROM_CONTACT] Started timeBetweenJobs. Contact is Simeon")
					ELIF contactHeist = CHAR_LESTER
						NET_DW_PRINT("[REQUEST_A_HEIST_FROM_CONTACT] Started timeBetweenJobs. Contact is lester")
					ELIF contactHeist = CHAR_RON
						NET_DW_PRINT("[REQUEST_A_HEIST_FROM_CONTACT] Started timeBetweenJobs. Contact is Ron")
					ELSE
						NET_DW_PRINT("[REQUEST_A_HEIST_FROM_CONTACT] Started timeBetweenJobs. Don't know who contact is!")
					ENDIF
				#ENDIF
			ELSE
				IF HAS_NET_TIMER_EXPIRED(timeBetweenJobs, TIME_DELAY_REQUEST_NPC_INVITE)
					iMyHeistStrand = ILLEGAL_HEIST_STRAND_SLOT
					
			        iReqHeistProg++
					#IF IS_DEBUG_BUILD
						NET_DW_PRINT("[REQUEST_A_HEIST_FROM_CONTACT] iReqHeistProg = 1")
					#ENDIF
					
					CLEAR_BIT(iBitset, iFailedBit)
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
			iCurrentRank = GET_PLAYER_RANK(PLAYER_ID())
			#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[REQUEST_A_HEIST_FROM_CONTACT] iCurrentRank = ", iCurrentRank) #ENDIF
			
			iMyHeistStrand = MPGlobalsAmbience.iReqHeistStrand
			
			IF (iMyHeistStrand <> ILLEGAL_HEIST_STRAND_SLOT)
			
				#IF IS_DEBUG_BUILD 
					MP_MISSION_ID_DATA theMissionIdData 
					TEXT_LABEL_63 tl63HeistName
					theMissionIdData = g_sLocalMPHeists[MPGlobalsAmbience.iReqHeistStrand].lhsMissionIdData
					tl63HeistName = Get_Mission_Name_For_FM_Cloud_Loaded_Activity(theMissionIdData)
					NET_DW_PRINT_STRING_INT("[REQUEST_A_HEIST_FROM_CONTACT] Heist strand valid. Strand = ", iMyHeistStrand) 
					NET_DW_PRINT_STRINGS("[REQUEST_A_HEIST_FROM_CONTACT] Heist strand valid. Heist = ", tl63HeistName)
				#ENDIF
				
				Setup_Specific_Heist_Strand(iMyHeistStrand)
				iReqHeistProg++
				#IF IS_DEBUG_BUILD
					NET_DW_PRINT("[REQUEST_A_HEIST_FROM_CONTACT] iReqHeistProg = 2")
				#ENDIF
			ELSE
				#IF IS_DEBUG_BUILD	
					NET_DW_PRINT("[REQUEST_A_HEIST_FROM_CONTACT] Case 1 Set Failed bit")
					NET_DW_PRINT("[REQUEST_A_HEIST_FROM_CONTACT] Case 1 thisCMArrayPos = ILLEGAL_HEIST_STRAND_SLOT setting iMyHeistStrand = 99")
				#ENDIF
				SET_BIT(iBitset, iFailedBit)
				iReqHeistProg = 99
			ENDIF
		BREAK
		
		CASE 99
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

CONST_INT ciYACHT_JOB_INVITE_STAGE_DELAY 			0
CONST_INT ciYACHT_JOB_INVITE_STAGE_FIND_JOB 		1
CONST_INT ciYACHT_JOB_INVITE_STAGE_GATHER_JOB_INFO	2
CONST_INT ciYACHT_JOB_INVITE_STAGE_SETUP_JOB 		3
CONST_INT ciYACHT_JOB_INVITE_STAGE_INVITE_SENT		4

INT iYachtJobInviteStage = 0
INT iYachtMissionArrayPos = ILLEGAL_CONTACT_MISSION_SLOT
MP_MISSION_ID_DATA mpYachtMissionIdData
SCRIPT_TIMER timerYachtJob

FUNC BOOL SEND_INVITE_TO_CURRENT_YACHT_MISSION(BOOL& bFailedToSendInvite)
	SWITCH iYachtJobInviteStage
		CASE ciYACHT_JOB_INVITE_STAGE_DELAY
			IF NOT HAS_NET_TIMER_STARTED(timerYachtJob)
				START_NET_TIMER(timerYachtJob)
				PRINTLN("[amNpcInvite] [SEND_INVITE_TO_CURRENT_YACHT_MISSION] - ciYACHT_JOB_INVITE_STAGE_DELAY - Started timer.")
			ELSE
				IF HAS_NET_TIMER_EXPIRED(timerYachtJob, TIME_DELAY_REQUEST_NPC_INVITE)
					RESET_NET_TIMER(timerYachtJob)
					iYachtJobInviteStage++
					PRINTLN("[amNpcInvite] [SEND_INVITE_TO_CURRENT_YACHT_MISSION] - ciYACHT_JOB_INVITE_STAGE_DELAY - Timer has expired.")
				ENDIF
			ENDIF
		BREAK
		
		CASE ciYACHT_JOB_INVITE_STAGE_FIND_JOB
			iYachtMissionArrayPos = GET_CONTACT_MISSION_ARRAY_POS_FROM_ROOT_CONTENT_ID_HASH(GET_YACHT_MISSION_ROOT_CONTENT_ID(GET_LOCAL_PLAYER_CURRENT_YACHT_MISSION()))
			IF iYachtMissionArrayPos != ILLEGAL_CONTACT_MISSION_SLOT
				iYachtJobInviteStage++
				PRINTLN("[amNpcInvite] [SEND_INVITE_TO_CURRENT_YACHT_MISSION] - ciYACHT_JOB_INVITE_STAGE_FIND_JOB - Found array pos of job = ", iYachtMissionArrayPos)
			ELSE
				PRINTLN("[amNpcInvite] [SEND_INVITE_TO_CURRENT_YACHT_MISSION] - ciYACHT_JOB_INVITE_STAGE_FIND_JOB - Failed to find array pos of current yacht mission, return true, bFailedToSendInvite = TRUE")
				bFailedToSendInvite = TRUE
				iYachtJobInviteStage = ciYACHT_JOB_INVITE_STAGE_DELAY
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE ciYACHT_JOB_INVITE_STAGE_GATHER_JOB_INFO
		
			mpYachtMissionIdData = GATHER_AND_GET_MISSIONID_DATA_FOR_CONTACT_MISSION(iYachtMissionArrayPos)
			
			IF (ARE_THESE_FM_CLOUD_LOADED_ACTIVITY_DETAILS_AVAILABLE_LOCALLY(mpYachtMissionIdData))
			    iYachtJobInviteStage++
				PRINTLN("[amNpcInvite] [SEND_INVITE_TO_CURRENT_YACHT_MISSION] - ciYACHT_JOB_INVITE_STAGE_GATHER_JOB_INFO - Job info available locally, progress")
			ELSE
				PRINTLN("[amNpcInvite] [SEND_INVITE_TO_CURRENT_YACHT_MISSION] - ciYACHT_JOB_INVITE_STAGE_GATHER_JOB_INFO - Are_These_FM_Cloud_Loaded_Activity_Details_Available_Locally returned false, return true, bFailedToSendInvite = TRUE")
				bFailedToSendInvite = TRUE
				iYachtJobInviteStage = ciYACHT_JOB_INVITE_STAGE_DELAY
				RETURN TRUE
			ENDIF
		
		BREAK
		
		CASE ciYACHT_JOB_INVITE_STAGE_SETUP_JOB
			#IF IS_DEBUG_BUILD
			VECTOR vCoords
			TEXT_LABEL_31 tl31MissionName
			TEXT_LABEL_63 tl63MissionDesc
			
			vCoords 		= GET_COORDINATES_FOR_FM_CLOUD_LOADED_ACTIVITY(mpYachtMissionIdData)
			tl31MissionName = GET_MISSION_NAME_FOR_FM_CLOUD_LOADED_ACTIVITY(mpYachtMissionIdData)
			tl63MissionDesc = GET_MISSION_DESCRIPTION_FOR_FM_CLOUD_LOADED_ACTIVITY(mpYachtMissionIdData)
		
			PRINTLN("[amNpcInvite] [SEND_INVITE_TO_CURRENT_YACHT_MISSION] - ciYACHT_JOB_INVITE_STAGE_SETUP_JOB - GET_COORDINATES_FOR_FM_CLOUD_LOADED_ACTIVITY(mpYachtMissionIdData) = ", 
				vCoords)
			PRINTLN("[amNpcInvite] [SEND_INVITE_TO_CURRENT_YACHT_MISSION] - ciYACHT_JOB_INVITE_STAGE_SETUP_JOB - mpYachtMissionIdData.idCreator = ",
				mpMissionIdData.idCreator)
			PRINTLN("[amNpcInvite] [SEND_INVITE_TO_CURRENT_YACHT_MISSION] - ciYACHT_JOB_INVITE_STAGE_SETUP_JOB - GET_MISSION_NAME_FOR_FM_CLOUD_LOADED_ACTIVITY(mpYachtMissionIdData) = ", 
				tl31MissionName)
			PRINTLN("[amNpcInvite] [SEND_INVITE_TO_CURRENT_YACHT_MISSION] - ciYACHT_JOB_INVITE_STAGE_SETUP_JOB - GET_MISSION_DESCRIPTION_FOR_FM_CLOUD_LOADED_ACTIVITY(mpYachtMissionIdData) = ", 
				tl63MissionDesc)
			#ENDIF
			
			SETUP_CONTACT_MISSION_FROM_CM_DATA_AND_SEND_INVITE(mpYachtMissionIdData)
				
			iYachtJobInviteStage = ciYACHT_JOB_INVITE_STAGE_INVITE_SENT
			PRINTLN("[amNpcInvite] [SEND_INVITE_TO_CURRENT_YACHT_MISSION] - ciYACHT_JOB_INVITE_STAGE_SETUP_JOB - SETUP_CONTACT_MISSION_FROM_CM_DATA_AND_SEND_INVITE called, invite should be sent. iYachtJobInviteStage = ciYACHT_JOB_INVITE_STAGE_INVITE_SENT.")
		BREAK
		
		CASE ciYACHT_JOB_INVITE_STAGE_INVITE_SENT
			MP_MISSION_ID_DATA tempData
			mpYachtMissionIdData = tempData
			
			bFailedToSendInvite = FALSE
			iYachtJobInviteStage = ciYACHT_JOB_INVITE_STAGE_DELAY
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

		
PROC MAINTAIN_LOCAL_PLAYER_CHECKS()
	INT iPart = PARTICIPANT_ID_TO_INT()
	INT iCost = 0
	
	IF IS_NET_PLAYER_OK(PLAYER_ID(), FALSE)
		IF NOT IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
		AND GET_LOCAL_PLAYER_CORONA_POS_STATE() = 0
			IF NOT IS_BIT_SET(PlayerBD[iPart].iPlayerBitset, biP_NotDoneAnything)
				IF NOT HAS_NET_TIMER_STARTED(timeNotDoneAnything)
					START_NET_TIMER(timeNotDoneAnything)
					#IF IS_DEBUG_BUILD NET_DW_PRINT("Started timeNotDoneAnything timer") #ENDIF
				ELSE
					IF HAS_NET_TIMER_EXPIRED(timeNotDoneAnything, MIN_TIME_NOT_DONE_ANYTHING)
						#IF IS_DEBUG_BUILD NET_DW_PRINT("Setting biP_NotDoneAnything as I haven't done anything for a while") #ENDIF
						SET_BIT(PlayerBD[iPart].iPlayerBitset, biP_NotDoneAnything)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF IS_BIT_SET(PlayerBD[iPart].iPlayerBitset, biP_NotDoneAnything)
				#IF IS_DEBUG_BUILD NET_DW_PRINT("Clearing biP_NotDoneAnything as I'm on a mission") #ENDIF
				RESET_NET_TIMER(timeNotDoneAnything)
				CLEAR_BIT(PlayerBD[iPart].iPlayerBitset, biP_NotDoneAnything)
			ENDIF
			IF IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet4, BI_FM_NMH4_LEST_JOB_FAILED)
				#IF IS_DEBUG_BUILD NET_DW_PRINT("Clearing BI_FM_NMH4_LEST_JOB_FAILED as I'm on a mission") #ENDIF
				CLEAR_BIT(MPGlobalsAmbience.iFmNmhBitSet4, BI_FM_NMH4_LEST_JOB_FAILED)
			ENDIF
			
		ENDIF
		
		IF NOT HAS_NET_TIMER_STARTED(timeMyUnlockedJobs)
		OR HAS_NET_TIMER_EXPIRED(timeMyUnlockedJobs, 600000)
			UPDATE_MY_UNLOCKED_JOBS()
			RESET_NET_TIMER(timeMyUnlockedJobs)
			START_NET_TIMER(timeMyUnlockedJobs)
		ENDIF
		
		IF NOT IS_BIT_SET(PlayerBD[iPart].iPlayerBitset, biP_CustomMenuOnScreen)
			IF IS_CUSTOM_MENU_ON_SCREEN()
				SET_BIT(PlayerBD[iPart].iPlayerBitset, biP_CustomMenuOnScreen)
				#IF IS_DEBUG_BUILD NET_DW_PRINT("Setting biP_CustomMenuOnScreen") #ENDIF
			ENDIF
		ELSE
			IF NOT IS_CUSTOM_MENU_ON_SCREEN()
				CLEAR_BIT(PlayerBD[iPart].iPlayerBitset, biP_CustomMenuOnScreen)
				#IF IS_DEBUG_BUILD NET_DW_PRINT("Clearing biP_CustomMenuOnScreen") #ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//-- Request a job from lamar
	IF IS_BIT_SET(MPGlobalsAmbience.iNpcInviteBitset, biNPC_WantGeraldJobInvite)
		MAINTAIN_DIALOGUE_FOR_REQUEST_JOB(CHAR_MP_GERALD, iLamarDiagProg)
		IF REQUEST_A_JOB_FROM_CONTACT(CHAR_MP_GERALD, iLamarRequestJobProg,  timeLamarJob, MPGlobalsAmbience.iFmNmhBitSet5, BI_FM_NMH5_LAM_JOB_FAILED)
			IF NOT IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet5, BI_FM_NMH5_LAM_JOB_FAILED)
				iCost = GET_CONTACT_REQUEST_COST(REQUEST_GERALD_JOB)
				IF iCost > 0
					IF NETWORK_CAN_SPEND_MONEY(iCost, FALSE, TRUE, FALSE)
						#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[REQUEST_A_JOB_FROM_CONTACT] Removing cash for Gerald req job, cost = ", iCost) #ENDIF
						IF USE_SERVER_TRANSACTIONS()
							INT iTransactionID
							TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_SPEND_REQUEST_JOB, iCost, iTransactionID, FALSE, TRUE)
							g_cashTransactionData[iTransactionID].cashInfo.iItemHash = HASH("CHAR_MP_GERALD")
						ELSE
							GIVE_LOCAL_PLAYER_FM_CASH(-iCost, 1, TRUE, 0)
							NETWORK_SPENT_REQUEST_JOB(iCost, FALSE, TRUE, HASH("CHAR_MP_GERALD"))
						ENDIF
						
					ELSE
						#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[REQUEST_A_JOB_FROM_CONTACT] NETWORK_CAN_SPEND_MONEY - Failed Removing cash for Gerald req job, cost = ", iCost) #ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			
			RESET_REMINDER_TIMER_FOR_ACTIVITY(FMMC_TYPE_GERALD_REQUEST_JOB)
			iLamarRequestJobProg = 0
			RESET_NET_TIMER(timeLamarJob)
			CLEAR_BIT(MPGlobalsAmbience.iNpcInviteBitset, biNPC_WantGeraldJobInvite)
		ENDIF
	ELSE
		IF iLamarDiagProg <> 0
			iLamarDiagProg = 0
		ENDIF
		
	ENDIF
	
	//-- Request a job from Simeon
	IF IS_BIT_SET(MPGlobalsAmbience.iNpcInviteBitset, biNPC_WantSimeonJobInvite)
		MAINTAIN_DIALOGUE_FOR_REQUEST_JOB(CHAR_SIMEON, iSimeonDiagProg)
		IF REQUEST_A_JOB_FROM_CONTACT(CHAR_SIMEON, iSimeonRequestJobProg,  timeSimeonJob, MPGlobalsAmbience.iFmNmhBitSet5, BI_FM_NMH5_SIM_REQ_FAILED)
			
			IF NOT IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet5, BI_FM_NMH5_SIM_REQ_FAILED)
				iCost = GET_CONTACT_REQUEST_COST(REQUEST_SIMEON_JOB)
				IF iCost > 0
					IF NETWORK_CAN_SPEND_MONEY(iCost, FALSE, TRUE, FALSE)
						#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[REQUEST_A_JOB_FROM_CONTACT] Removing cash for SImeon req job. iCost = ", iCost) #ENDIF
						IF USE_SERVER_TRANSACTIONS()
							INT iTransactionID
							TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_SPEND_REQUEST_JOB, iCost, iTransactionID, FALSE, TRUE)
							g_cashTransactionData[iTransactionID].cashInfo.iItemHash = HASH("CHAR_SIMEON")
						ELSE
							GIVE_LOCAL_PLAYER_FM_CASH(-iCost, 1, TRUE, 0)
							NETWORK_SPENT_REQUEST_JOB(iCost, FALSE, TRUE, HASH("CHAR_SIMEON"))
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[REQUEST_A_JOB_FROM_CONTACT] NETWORK_CAN_SPEND_MONEY - Failed Removing cash for Simeon req job, cost = ", iCost) #ENDIF
					
					ENDIF
				ENDIF
			ENDIF
			
			RESET_REMINDER_TIMER_FOR_ACTIVITY(FMMC_TYPE_SIMEON_REQUEST_JOB)
			iSimeonRequestJobProg = 0
			RESET_NET_TIMER(timeSimeonJob)
			CLEAR_BIT(MPGlobalsAmbience.iNpcInviteBitset, biNPC_WantSimeonJobInvite)
		ENDIF
	ELSE
		IF iSimeonDiagProg <> 0
			iSimeonDiagProg = 0
		ENDIF
	ENDIF
	
	//-- Request a job from Lester
	IF IS_BIT_SET(MPGlobalsAmbience.iNpcInviteBitset, biNPC_WantNpcInvite)
		MAINTAIN_DIALOGUE_FOR_REQUEST_JOB(CHAR_LESTER, iLesterDiagProg)

		IF REQUEST_A_JOB_FROM_CONTACT(CHAR_LESTER, iLesterRequestJobProg, timeLesterJob, MPGlobalsAmbience.iFmNmhBitSet4, BI_FM_NMH4_LEST_JOB_FAILED)
			IF NOT IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet4, BI_FM_NMH4_LEST_JOB_FAILED)
				iCost = GET_CONTACT_REQUEST_COST(REQUEST_LESTER_JOB)
				IF iCost > 0
					IF NETWORK_CAN_SPEND_MONEY(iCost, FALSE, TRUE, FALSE)
						#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[REQUEST_A_JOB_FROM_CONTACT] Removing cash for Lester req job. iCost = ", iCost) #ENDIF
						IF USE_SERVER_TRANSACTIONS()
							INT iTransactionID
							TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_SPEND_REQUEST_JOB, iCost, iTransactionID, FALSE, TRUE)
							g_cashTransactionData[iTransactionID].cashInfo.iItemHash = HASH("CHAR_LESTER")
						ELSE
							GIVE_LOCAL_PLAYER_FM_CASH(-iCost, 1, TRUE, 0)
							NETWORK_SPENT_REQUEST_JOB(iCost, FALSE, TRUE, HASH("CHAR_LESTER"))
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[REQUEST_A_JOB_FROM_CONTACT] NETWORK_CAN_SPEND_MONEY - Failed Removing cash for Simeon req job, cost = ", iCost) #ENDIF
					
					ENDIF
				ENDIF
			ENDIF
								
			RESET_REMINDER_TIMER_FOR_ACTIVITY(FMMC_TYPE_LESTER_REQUEST_JOB)
			iLesterRequestJobProg = 0
			RESET_NET_TIMER(timeLesterJob)
			CLEAR_BIT(MPGlobalsAmbience.iNpcInviteBitset, biNPC_WantNpcInvite)
		ENDIF
	ELSE
		IF iLesterDiagProg <> 0
			iLesterDiagProg = 0
		ENDIF
	ENDIF
	
	
	//-- Request a Heist from Lester
	IF IS_BIT_SET(MPGlobalsAmbience.iNpcInviteBitset, biNPC_WantHstInvite)
		MAINTAIN_DIALOGUE_FOR_REQUEST_JOB(CHAR_LESTER, iLesterHeistDiagProg, TRUE)
		
	//	IF REQUEST_A_HEIST_FROM_CONTACT(CHAR_LESTER, iLesterRequestHeistProg, timeLesterHeist, g_iFmNmhBitSet8, BI_FM_NMH8_REQ_HST_FAIL)
		IF REQUEST_A_HEIST_FROM_CONTACT_V2(CHAR_LESTER, iLesterRequestHeistProg, timeLesterHeist, g_iFmNmhBitSet8, BI_FM_NMH8_REQ_HST_FAIL)
			IF NOT IS_BIT_SET(g_iFmNmhBitSet8, BI_FM_NMH8_REQ_HST_FAIL)
				iCost = GET_CONTACT_REQUEST_COST(REQUEST_LESTER_HST)
				IF NETWORK_CAN_SPEND_MONEY(iCost, FALSE, TRUE, FALSE)
					IF iCost > 0
						#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[REQUEST_A_HEIST_FROM_CONTACT] Removing cash for Lester req Heist. iCost = ", iCost) #ENDIF
						IF USE_SERVER_TRANSACTIONS()
							INT iTransactionID
							TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_SPEND_REQUEST_JOB, iCost, iTransactionID, FALSE, TRUE)
							g_cashTransactionData[iTransactionID].cashInfo.iItemHash = HASH("CHAR_LESTER")
						ELSE
							GIVE_LOCAL_PLAYER_FM_CASH(-iCost, 1, TRUE, 0)
							NETWORK_SPENT_REQUEST_JOB(iCost, FALSE, TRUE, HASH("CHAR_LESTER"))
						ENDIF
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[REQUEST_A_JOB_FROM_CONTACT] NETWORK_CAN_SPEND_MONEY - Failed Removing cash for Lester Req Heist, cost = ", iCost) #ENDIF
					
				ENDIF
			ENDIF
			
			iLesterRequestHeistProg = 0
			RESET_NET_TIMER(timeLesterHeist)
			CLEAR_BIT(MPGlobalsAmbience.iNpcInviteBitset, biNPC_WantHstInvite)
		ENDIF
	ENDIF
	
	
	//-- Request a job from Martin
	IF IS_BIT_SET(MPGlobalsAmbience.iNpcInviteBitset, biNPC_WantMartinInvite)
		MAINTAIN_DIALOGUE_FOR_REQUEST_JOB(CHAR_MARTIN, iMartinDiagProg)
		IF REQUEST_A_JOB_FROM_CONTACT(CHAR_MARTIN, iMartinRequestJobProg,  timeMartinJob, MPGlobalsAmbience.iFmNmhBitSet5, BI_FM_NMH5_MART_REQ_FAILED)
			IF NOT IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet5, BI_FM_NMH5_MART_REQ_FAILED)
				iCost = GET_CONTACT_REQUEST_COST(REQUEST_MARTIN_JOB)
				IF iCost > 0
					IF NETWORK_CAN_SPEND_MONEY(iCost, FALSE, TRUE, FALSE)
						#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[REQUEST_A_JOB_FROM_CONTACT] Removing cash for Martin req job. iCost = ", iCost) #ENDIF
						IF USE_SERVER_TRANSACTIONS()
							INT iTransactionID
							TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_SPEND_REQUEST_JOB, iCost, iTransactionID, FALSE, TRUE)
							g_cashTransactionData[iTransactionID].cashInfo.iItemHash = HASH("CHAR_MARTIN")
						ELSE
							GIVE_LOCAL_PLAYER_FM_CASH(-iCost, 1, TRUE, 0)
							NETWORK_SPENT_REQUEST_JOB(iCost, FALSE, TRUE, HASH("CHAR_MARTIN"))
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[REQUEST_A_JOB_FROM_CONTACT] NETWORK_CAN_SPEND_MONEY - Failed Removing cash for Martin req job, cost = ", iCost) #ENDIF
					
					ENDIF
				ENDIF
			ENDIF
			
			RESET_REMINDER_TIMER_FOR_ACTIVITY(FMMC_TYPE_MARTIN_REQUEST_JOB)
			iMartinRequestJobProg = 0
			RESET_NET_TIMER(timeMartinJob)
			CLEAR_BIT(MPGlobalsAmbience.iNpcInviteBitset, biNPC_WantMartinInvite)
		ENDIF
	ELSE
		IF iMartinDiagProg <> 0
			iMartinDiagProg = 0
		ENDIF
	ENDIF
	
	//-- Request a job from Ron
	IF IS_BIT_SET(MPGlobalsAmbience.iNpcInviteBitset, biNPC_WantRonInvite)
		MAINTAIN_DIALOGUE_FOR_REQUEST_JOB(CHAR_RON, iRonDiagProg)
		IF REQUEST_A_JOB_FROM_CONTACT(CHAR_RON, iRonRequestJobProg,  timeRonJob, MPGlobalsAmbience.iFmNmhBitSet6, BI_FM_NMH6_RON_REQ_FAILED)
			IF NOT IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet6, BI_FM_NMH6_RON_REQ_FAILED)
				iCost = GET_CONTACT_REQUEST_COST(REQUEST_RON_JOB)
				IF iCost > 0
					IF NETWORK_CAN_SPEND_MONEY(iCost, FALSE, TRUE, FALSE)
						#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[REQUEST_A_JOB_FROM_CONTACT] Removing cash for Ron req job. iCost = ", iCost) #ENDIF
						IF USE_SERVER_TRANSACTIONS()
							INT iTransactionID
							TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_SPEND_REQUEST_JOB, iCost, iTransactionID, FALSE, TRUE)
							g_cashTransactionData[iTransactionID].cashInfo.iItemHash = HASH("CHAR_RON")
						ELSE
							GIVE_LOCAL_PLAYER_FM_CASH(-iCost, 1, TRUE, 0)
							NETWORK_SPENT_REQUEST_JOB(iCost, FALSE, TRUE, HASH("CHAR_RON"))
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[REQUEST_A_JOB_FROM_CONTACT] NETWORK_CAN_SPEND_MONEY - Failed Removing cash for Ron req job, cost = ", iCost) #ENDIF
					
					ENDIF
				ENDIF
			ENDIF
			
			RESET_REMINDER_TIMER_FOR_ACTIVITY(FMMC_TYPE_RON_REQUEST_JOB)
			iRonRequestJobProg = 0
			RESET_NET_TIMER(timeRonJob)
			CLEAR_BIT(MPGlobalsAmbience.iNpcInviteBitset, biNPC_WantRonInvite)
		ENDIF
	ELSE
		IF iRonDiagProg <> 0
			iRonDiagProg = 0
		ENDIF
	ENDIF
	
	//-- Request a job from Ron
	IF IS_BIT_SET(MPGlobalsAmbience.iNpcInviteBitset, biNPC_WantYachtJobInvite)
//		MAINTAIN_DIALOGUE_FOR_REQUEST_JOB(CHAR_RON, iRonDiagProg)
//		IF REQUEST_A_JOB_FROM_CONTACT(CHAR_RON, iRonRequestJobProg,  timeRonJob, MPGlobalsAmbience.iFmNmhBitSet6, BI_FM_NMH6_RON_REQ_FAILED)
		BOOL bFailed
		IF SEND_INVITE_TO_CURRENT_YACHT_MISSION(bFailed)
			IF NOT bFailed
				iCost = GET_CONTACT_REQUEST_COST(REQUEST_YACHT_CAPTAIN_JOB)
				IF iCost > 0
					IF NETWORK_CAN_SPEND_MONEY(iCost, FALSE, TRUE, FALSE)
						#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[REQUEST_A_JOB_FROM_CONTACT] Removing cash for Yacht Captain req job. iCost = ", iCost) #ENDIF
						IF USE_SERVER_TRANSACTIONS()
							INT iTransactionID
							TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_SPEND_REQUEST_JOB, iCost, iTransactionID, FALSE, TRUE)
							g_cashTransactionData[iTransactionID].cashInfo.iItemHash = HASH("CHAR_YACHT_CAPTAIN")
						ELSE
							GIVE_LOCAL_PLAYER_FM_CASH(-iCost, 1, TRUE, 0)
							NETWORK_SPENT_REQUEST_JOB(iCost, FALSE, TRUE, HASH("CHAR_YACHT_CAPTAIN"))
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[REQUEST_A_JOB_FROM_CONTACT] NETWORK_CAN_SPEND_MONEY - Failed Removing cash for Yacht Captain req job, cost = ", iCost) #ENDIF
					
					ENDIF
				ENDIF
			ENDIF
			
			CLEAR_BIT(MPGlobalsAmbience.iNpcInviteBitset, biNPC_WantYachtJobInvite)
		ENDIF
	ELSE
		IF iRonDiagProg <> 0
			iRonDiagProg = 0
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(MPGlobalsAmbience.iNpcInviteBitset, biNPC_PlayerWantsNpcInvite)
		IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
			#IF IS_DEBUG_BUILD NET_DW_PRINT("biNPC_PlayerWantsNpcInvite is set, but I'm not host....") #ENDIF
			CLEAR_BIT(MPGlobalsAmbience.iNpcInviteBitset, biNPC_PlayerWantsNpcInvite)
		ENDIF
	ENDIF
ENDPROC

FUNC INT GET_CLIENT_MISSION_STATE(INT iPlayer)
	RETURN playerBD[iPlayer].iGameState
ENDFUNC

FUNC INT GET_SERVER_MISSION_STATE()
	RETURN serverBD.iServerGameState

ENDFUNC


PROC SEND_NPC_INVITE_CLIENT()
	IF iLocalJobToUse <> serverBD.iJobToUse
		IF serverBD.iJobToUse >= 0
		//	IF serverBD.iNextPartForInvite = PARTICIPANT_ID_TO_INT() // 
				IF IS_BIT_SET(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitset, biP_NotDoneAnything)
					IF NOT IS_BIT_SET(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitset, biP_CustomMenuOnScreen)
						#IF IS_DEBUG_BUILD 
							NET_DW_PRINT("[SEND_NPC_INVITE_CLIENT] (biP_NotDoneAnything is set) New NPC invite:")
							NET_DW_PRINT_STRINGS("[SEND_NPC_INVITE_CLIENT] Job Type...", GET_FMMC_BIT_SET_NAME_FOR_LOGS(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[serverbd.iJobToUse].iType))
							NET_DW_PRINT_STRINGS("[SEND_NPC_INVITE_CLIENT] Job name... ", g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[serverbd.iJobToUse].tlMissionName)
							NET_DW_PRINT_HOST_NAME()
							
						#ENDIF
						
						IF IS_BIT_SET(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitset, biP_NotDoneAnything)
							#IF IS_DEBUG_BUILD NET_DW_PRINT("Clearing biP_NotDoneAnything as it's my turn for an npc invite") #ENDIF
							RESET_NET_TIMER(timeNotDoneAnything)
							CLEAR_BIT(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitset, biP_NotDoneAnything)
						ENDIF
					
						IF IS_FM_TYPE_UNLOCKED(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[serverbd.iJobToUse].iType)
						
							Store_Invite_Details_For_Joblist_From_NPC(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[serverbd.iJobToUse].iType,
								g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[serverbd.iJobToUse].iSubType,
					        	g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[serverbd.iJobToUse].vStartPos,
					       		FMMC_ROCKSTAR_CREATOR_ID,
					        	serverBD.charInvite,
					        	g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[serverbd.iJobToUse].tlMissionName,
					        	"",
								g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[serverbd.iJobToUse].tlName)
							
						ELSE
							#IF IS_DEBUG_BUILD 
								NET_DW_PRINT("[SEND_NPC_INVITE_CLIENT] not doing NPC invite as I haven't unlocked job type")
							#ENDIF
						ENDIF
						
						RESET_NET_TIMER(timeNotDoneAnything)
					ELSE
						#IF IS_DEBUG_BUILD	
							NET_DW_PRINT("serverBD.iNextPartForInvite = PARTICIPANT_ID_TO_INT() , but biP_CustomMenuOnScreen is set")
						#ENDIF
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD	
						NET_DW_PRINT("serverBD.iNextPartForInvite = PARTICIPANT_ID_TO_INT() , but biP_NotDoneAnything not set")
					#ENDIF
				ENDIF
//			ELSE
//				#IF IS_DEBUG_BUILD 
//					NET_DW_PRINT("[SEND_NPC_INVITE_CLIENT] not doing NPC invite as serverBD.iNextPartForInvite <> PARTICIPANT_ID_TO_INT()")
//					
//					IF serverBD.iNextPartForInvite >= 0// serverBD.iNextPlayerForInvite >= 0
//						IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(serverBD.iNextPartForInvite))
//							NET_DW_PRINT_STRING_WITH_PLAYER_NAME("This player is receiving the invite", NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(serverBD.iNextPartForInvite)))
//						ENDIF
//					ENDIF
//					NET_DW_PRINT_HOST_NAME()
//				#ENDIF
//			ENDIF
		ENDIF
		
		UPDATE_MY_UNLOCKED_JOBS()
		iLocalJobToUse = serverBD.iJobToUse
	ENDIF
ENDPROC


PROC PROCESS_CLIENT_STAGGERED_LOOP()
	/*INT i
	
	
	PLAYER_INDEX player
	
	IF IS_BIT_SET(PlayerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitset, biP_PlayerOnMission)
		IF iPlayerStaggeredCount >= COUNT_OF(PlayerBD) 
			iPlayerStaggeredCount = 0
			CLEAR_BIT(iBoolsBitSet, biL_LocalLoopedThroughEveryone)
			
		ENDIF
		
		i = iPlayerStaggeredCount
		
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
			player = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
			IF IS_NET_PLAYER_OK(player, FALSE)
			ENDIF
		ENDIF
		
		iPlayerStaggeredCount++
		
		IF iServerStaggeredCount >= COUNT_OF(PlayerBD) 
			SET_BIT(iBoolsBitSet, biL_LocalLoopedThroughEveryone)
			
			
		ENDIF
	ENDIF */
ENDPROC
PROC PROCESS_MISSION_CLIENT()



	MAINTAIN_LOCAL_PLAYER_CHECKS()
	
	
	SWITCH PlayerBD[PARTICIPANT_ID_TO_INT()].iMissionProg
		CASE 0
		//	SEND_NPC_INVITE_CLIENT()
		BREAK
		
	ENDSWITCH
	
	
ENDPROC	

PROC UPDATE_RECENTLY_PLAYED(PLAYER_INDEX player, INT &iBitset)
	IF NOT IS_BIT_SET(iBitset, biS_SomeoneDidRace)
		IF IS_PLAYER_ON_ANY_RACE(player)	
			SET_BIT(iBitset, biS_SomeoneDidRace)
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(iBitset, biS_SomeoneDidDeathmatch)
		IF IS_PLAYER_ON_ANY_DEATHMATCH(player)	
			SET_BIT(iBitset, biS_SomeoneDidDeathmatch)
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(iBitset, biS_SomeoneDidParachute)
		IF IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(player, FMMC_TYPE_BASE_JUMP)	
			SET_BIT(iBitset, biS_SomeoneDidParachute)
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(iBitset, biS_SomeoneDidSurvival)
		IF IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(player, FMMC_TYPE_SURVIVAL)	
			SET_BIT(iBitset, biS_SomeoneDidParachute)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SERVER_STAGGERED_LOOP()
	INT i
	INT iPlayerInt
	
	PLAYER_INDEX player
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF iServerStaggeredCount >= COUNT_OF(PlayerBD) 
			iServerStaggeredCount = 0
			iNumberOfPlayersReady = 0
		//	iStaggeredNextPlayerForInvite = -1
			iStaggeredNextPartForInvite = -1
			CLEAR_BIT(iServerStaggeredBitset, biSt_ServerLoopedThroughEveryone)
			CLEAR_BIT(iServerStaggeredBitset, biSt_FoundSOmeoneNotDoneAnything)
			CLEAR_BIT(iServerStaggeredBitset, biSt_FoundPlayerNotUnlockedParachute)
			CLEAR_BIT(iServerStaggeredBitset, biSt_FoundPlayerNotUnlockedSurvival)
			CLEAR_BIT(iServerStaggeredBitset, biSt_SomeoneRequestedNpcInvite)
			IF NOT HAS_NET_TIMER_STARTED(timeUpdatePlayedList)
			OR HAS_NET_TIMER_EXPIRED(timeUpdatePlayedList, 5000)
				SET_BIT(iServerStaggeredBitset, biSt_UpdateRecentlyPlayed)
			ENDIF
		ENDIF
		
		i = iServerStaggeredCount
		
		IF HAS_NET_TIMER_EXPIRED(serverBD.timeSinceLastNpcInvite, MIN_TIME_FOR_NPC_INVITE)
			#IF IS_DEBUG_BUILD
				IF bWdDoStaggeredDebug
					NET_DW_PRINT("[PROCESS_SERVER_STAGGERED_LOOP] MIN_TIME_FOR_NPC_INVITE expired... ")
				ENDIF
			#ENDIF
			
			//-- DOn't do this if someone has requested one
		//	IF NOT IS_BIT_SET(MPGlobalsAmbience.iNpcInviteBitset, biNPC_PlayerWantsNpcInvite)
				IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
					player = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
					IF IS_NET_PLAYER_OK(player, FALSE)
						IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_SendNpcInvite)
							IF NOT IS_PLAYER_ON_ANY_FM_MISSION(player)
								//-- Check for players who haven't done anything for a while
								IF IS_BIT_SET(PlayerBD[i].iPlayerBitset, biP_NotDoneAnything)
									IF NOT IS_BIT_SET(PlayerBD[i].iPlayerBitset, biP_CustomMenuOnScreen)
										iNumberOfPlayersReady++
										#IF IS_DEBUG_BUILD
											IF bWdDoStaggeredDebug
											//	NET_DW_PRINT("[PROCESS_SERVER_STAGGERED_LOOP] MIN_TIME_FOR_NPC_INVITE NOT expired... ")
												NET_DW_PRINT_STRING_WITH_PLAYER_NAME("[PROCESS_SERVER_STAGGERED_LOOP] Found a player who hasn't done anything... ", player)
											ENDIF
										#ENDIF
										
										//#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_WITH_PLAYER_NAME("Found a player ready... ", player) #ENDIF
									//	IF serverBD.iNextPlayerForInvite <> NATIVE_TO_INT(player)
										IF serverBD.iNextPartForInvite <> i
										#IF IS_DEBUG_BUILD OR bAllowSamePlayerInvite #ENDIF
										//	IF iStaggeredNextPlayerForInvite = -1
											IF iStaggeredNextPartForInvite = -1
											
												IF NOT IS_BIT_SET(iServerStaggeredBitset, biSt_FoundSOmeoneNotDoneAnything)
													SET_BIT(iServerStaggeredBitset, biSt_FoundSOmeoneNotDoneAnything)
												ENDIF
										
												//-- Check if player has unlocked parachuting
												IF NOT IS_BIT_SET(iServerStaggeredBitset, biSt_FoundPlayerNotUnlockedParachute)
													IF NOT IS_BIT_SET(PlayerBD[i].iPlayerBitset, biP_UnlockedParachuting)
														SET_BIT(iServerStaggeredBitset, biSt_FoundPlayerNotUnlockedParachute)
													ENDIF
												ENDIF
												
												//-- Check if player has unlocked Survival
												IF NOT IS_BIT_SET(iServerStaggeredBitset, biSt_FoundPlayerNotUnlockedSurvival)
													IF NOT IS_BIT_SET(PlayerBD[i].iPlayerBitset, biP_UnlockedSurvival)
														SET_BIT(iServerStaggeredBitset, biSt_FoundPlayerNotUnlockedSurvival)
													ENDIF
												ENDIF
										//		iStaggeredNextPlayerForInvite = NATIVE_TO_INT(player)
												iStaggeredNextPartForInvite = i
											ENDIF
										ELSE
											#IF IS_DEBUG_BUILD
												IF bWdDoStaggeredDebug
												
													NET_DW_PRINT_STRING_WITH_PLAYER_NAME("[PROCESS_SERVER_STAGGERED_LOOP] Not doing invite as serverBD.iNextPartForInvite = i ", player)
												ENDIF
											#ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
					ENDIF
					
				ENDIF
		/*	ELSE
				#IF IS_DEBUG_BUILD NET_DW_PRINT("timeSinceLastNpcInvite expired, but someone requested an invite") #ENDIF
				RESET_NET_TIMER(serverBD.timeSinceLastNpcInvite)
				START_NET_TIMER(serverBD.timeSinceLastNpcInvite)
			ENDIF */
		ELSE	
			#IF IS_DEBUG_BUILD
				IF bWdDoStaggeredDebug
					NET_DW_PRINT("[PROCESS_SERVER_STAGGERED_LOOP] MIN_TIME_FOR_NPC_INVITE NOT expired... ")
				ENDIF
			#ENDIF
		ENDIF
		
		//-- Check for player being on a race/dm/parachute/survuval
		IF IS_BIT_SET(iServerStaggeredBitset, biSt_UpdateRecentlyPlayed)
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
				player = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
				IF IS_NET_PLAYER_OK(player, FALSE)
				//	UPDATE_RECENTLY_PLAYED(player, serverBD.iServerRecentlyPlayedBitset)
				ENDIF
			ENDIF
		ENDIF
		
		//-- Check for someone who requested an npc invite
		IF IS_BIT_SET(MPGlobalsAmbience.iNpcInviteBitset, biNPC_PlayerWantsNpcInvite)
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
				player = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
				IF IS_NET_PLAYER_OK(player, FALSE)
					iPlayerInt = NATIVE_TO_INT(player)
					IF IS_BIT_SET(MPGlobalsAmbience.iPlayerNpcInviteBitset, iPlayerInt)
						IF NOT HAS_NET_TIMER_STARTED(serverBD.timePlayerRequestInvite)
							#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_WITH_PLAYER_NAME("Server thinks this player requested an npc invite, starting delay timer... ", player) #ENDIF
							START_NET_TIMER(serverBD.timePlayerRequestInvite)
						ELSE	
							IF HAS_NET_TIMER_EXPIRED(serverBD.timePlayerRequestInvite, TIME_DELAY_REQUEST_NPC_INVITE)
								#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_WITH_PLAYER_NAME("Server thinks this player requested an npc invite... ", player) #ENDIF
								
								IF NOT IS_BIT_SET(PlayerBD[i].iPlayerBitset, biP_UnlockedParachuting)
									#IF IS_DEBUG_BUILD NET_DW_PRINT("They haven't unlocked parachuting") #ENDIF
									SET_BIT(iServerStaggeredBitset, biSt_FoundPlayerNotUnlockedParachute)
								ENDIF
								
								IF NOT IS_BIT_SET(PlayerBD[i].iPlayerBitset, biP_UnlockedSurvival)
									#IF IS_DEBUG_BUILD NET_DW_PRINT("They haven't unlocked survival") #ENDIF
									SET_BIT(iServerStaggeredBitset, biSt_FoundPlayerNotUnlockedSurvival)
								ENDIF
								CLEAR_BIT(MPGlobalsAmbience.iNpcInviteBitset, biNPC_PlayerWantsNpcInvite)
								SET_BIT(iServerStaggeredBitset, biSt_SomeoneRequestedNpcInvite)
								RESET_NET_TIMER(serverBD.timePlayerRequestInvite)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF		
		ENDIF
		
		iServerStaggeredCount++
		
		IF iServerStaggeredCount >= COUNT_OF(PlayerBD) 
			SET_BIT(iServerStaggeredBitset, biSt_ServerLoopedThroughEveryone)
			
			IF IS_BIT_SET(iServerStaggeredBitset, biSt_UpdateRecentlyPlayed)
				CLEAR_BIT(iServerStaggeredBitset, biSt_UpdateRecentlyPlayed)
				RESET_NET_TIMER(timeUpdatePlayedList)
				START_NET_TIMER(timeUpdatePlayedList)
			ENDIF
			
			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_SendNpcInvite)
				IF IS_BIT_SET(iServerStaggeredBitset, biSt_FoundSOmeoneNotDoneAnything)
				OR IS_BIT_SET(iServerStaggeredBitset, biSt_SomeoneRequestedNpcInvite)
					IF iNumberOfPlayersReady >= MIN_PLAYERS_FOR_INVITE
					OR IS_BIT_SET(iServerStaggeredBitset, biSt_SomeoneRequestedNpcInvite)
						#IF IS_DEBUG_BUILD NET_DW_PRINT("Setting biS_SendNpcInvite") #ENDIF
						SET_BIT(serverBD.iServerBitSet, biS_SendNpcInvite)
						
						IF IS_BIT_SET(iServerStaggeredBitset, biSt_FoundPlayerNotUnlockedParachute)
							SET_BIT(serverBD.iServerBitSet, biS_PlayerNotUnlockedParachuting)
							#IF IS_DEBUG_BUILD NET_DW_PRINT("A player hasn't unlocked parachuting - won't pick job ") #ENDIF
						ENDIF
						
						IF IS_BIT_SET(iServerStaggeredBitset, biSt_FoundPlayerNotUnlockedSurvival)
							SET_BIT(serverBD.iServerBitSet, biS_PlayerNotUnlockedSurvival)
							#IF IS_DEBUG_BUILD NET_DW_PRINT("A player hasn't unlocked survival - won't pick job ") #ENDIF
						ENDIF
						
//						IF iStaggeredNextPlayerForInvite > -1
//							serverBD.iNextPlayerForInvite = iStaggeredNextPlayerForInvite
//							#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_WITH_PLAYER_NAME("Next player for invite...", INT_TO_PLAYERINDEX(iStaggeredNextPlayerForInvite)) #ENDIF
//						ENDIF

						IF iStaggeredNextPartForInvite > -1
							
							serverBD.iNextPartForInvite = iStaggeredNextPartForInvite
							IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(serverBD.iNextPartForInvite))
								#IF IS_DEBUG_BUILD 
									NET_DW_PRINT_STRING_INT("Next part for invite = ", serverBD.iNextPartForInvite)
									NET_DW_PRINT_STRING_WITH_PLAYER_NAME("Next player for invite...", NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iStaggeredNextPartForInvite))) 
								#ENDIF
							ENDIF
						ENDIF
						
						IF IS_BIT_SET(iServerStaggeredBitset, biSt_SomeoneRequestedNpcInvite)
							SET_BIT(serverBD.iServerBitSet, biS_SHouldComeFromLester)
						ENDIF
						CLEAR_BIT(iBoolsBitSet, biL_InitSendInvite)
						
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC enumCharacterList GET_NPC_CHAR_FOR_INVITE(INT iJobType)
	enumCharacterList charNpc = CHAR_LAMAR
	SWITCH iJobType
		CASE FMMC_TYPE_RACE
			charNpc = CHAR_LAMAR
		BREAK
		
		CASE FMMC_TYPE_DEATHMATCH
			charNpc = CHAR_MP_GERALD
		BREAK
		
		CASE FMMC_TYPE_BASE_JUMP
			charNpc = CHAR_DOM
		BREAK
		
		CASE FMMC_TYPE_SURVIVAL
			charNpc = CHAR_MARTIN
		BREAK
	ENDSWITCH
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_SHouldComeFromLester)
		charNpc = CHAR_LESTER
		#IF IS_DEBUG_BUILD NET_DW_PRINT("[GET_NPC_CHAR_FOR_INVITE] Using Lester...") #ENDIF
	ENDIF
	
	RETURN charNpc
ENDFUNC

FUNC BOOL IS_TUTORIAL_RACE(INT iMissionType, VECTOR vLoc)
	IF iMissionType <> FMMC_TYPE_RACE
		RETURN FALSE
	ENDIF
	
	IF NOT ARE_VECTORS_ALMOST_EQUAL(vLoc, <<-706.2501,-974.4005,19.3900>>, 1.0)
		RETURN FALSE
	ENDIF
	
	#IF IS_DEBUG_BUILD NET_DW_PRINT("[IS_TUTORIAL_RACE] Picked tutorial race!...") #ENDIF
	RETURN TRUE
ENDFUNC
FUNC BOOL SEND_NPC_INVITE_SERVER(INT iMissionType)
	INT i
	INT iNewJob
	INT index
	IF NOT IS_BIT_SET(iBoolsBitSet, biL_InitSendInvite)
		REPEAT MAX_VALID_JOBS i
			iValidJobs[i] = -1
		ENDREPEAT
		iSendInviteProg = 0
		iNumValidJobs = 0
		i = 0
		SET_BIT(iBoolsBitSet, biL_InitSendInvite)
		#IF IS_DEBUG_BUILD 
			NET_DW_PRINT("[SEND_NPC_INVITE_SERVER] Set biL_InitSendInvite") 
			NET_DW_PRINT_STRINGS("[SEND_NPC_INVITE_SERVER] Will try to send NPC invite for activity ", GET_FMMC_BIT_SET_NAME_FOR_LOGS(iMissionType))
		#ENDIF
	ENDIF
	
	SWITCH iSendInviteProg
		CASE 0
			IF IS_BIT_SET(iBoolsBitSet, biL_InitSendInvite)
				FOR i = 0 TO FMMC_MAX_AT_ONCE_ROCKSTAR_CREATED - 1
					IF iNumValidJobs < (MAX_VALID_JOBS - 1)
						//IF g_FMMC_ROCKSTAR_CREATED.bInUse[i]
						IF IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iBitSet, ciROCKSTAR_CREATED_MISSION_IN_USE)
						
							#IF IS_DEBUG_BUILD
								IF bWdDoSendInviteDebug
									NET_DW_PRINT_STRING_INT("This job in use...", i)
									NET_DW_PRINT_STRINGS("Job Type...", GET_FMMC_BIT_SET_NAME_FOR_LOGS(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iType))
								ENDIF
							#ENDIF
							IF g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].iType = iMissionType
								IF NOT IS_TUTORIAL_RACE(iMissionType, g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[i].vStartPos) 
									#IF IS_DEBUG_BUILD
										IF bWdDoSendInviteDebug
											NET_DW_PRINT("Job is valid")
										ENDIF
									#ENDIF
									
									iValidJobs[iNumValidJobs] = i
									iNumValidJobs++
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDFOR
				
				iSendInviteProg++
				#IF IS_DEBUG_BUILD 
					NET_DW_PRINT_STRING_INT("[SEND_NPC_INVITE_SERVER] iSendInviteProg = ", iSendInviteProg) 
					NET_DW_PRINT_STRING_INT("[SEND_NPC_INVITE_SERVER] Found this many valid jobs ", iNumValidJobs)
				#ENDIF
			ENDIF
		BREAK
		
		CASE 1
			IF iNumValidJobs > 0
				index = GET_RANDOM_INT_IN_RANGE(0, iNumValidJobs - 1)
				#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[SEND_NPC_INVITE_SERVER] Random int... ", index) #ENDIF
				
				iNewJob = iValidJobs[index] 
				#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[SEND_NPC_INVITE_SERVER] using job ", iNewJob) #ENDIF
				
				//IF g_FMMC_ROCKSTAR_CREATED.bInUse[iJobToUse]
				IF IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iNewJob].iBitSet, ciROCKSTAR_CREATED_MISSION_IN_USE)
					IF g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iNewJob].iType = iMissionType
						#IF IS_DEBUG_BUILD 
							NET_DW_PRINT("[SEND_NPC_INVITE_SERVER] Job is valid! Broadcasting...")  
							NET_DW_PRINT_STRINGS("Job name... ", g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iNewJob].tlMissionName)
						#ENDIF
						
						
						serverBD.charInvite = GET_NPC_CHAR_FOR_INVITE(iMissionType)
						serverbd.iJobToUse = iNewJob
                    	serverBD.iServerRecentlyPlayedBitset = 0	
						
						iSendInviteProg = 99
					ELSE
						#IF IS_DEBUG_BUILD 
							NET_DW_PRINT_STRING_INT("[SEND_NPC_INVITE_SERVER] g_FMMC_ROCKSTAR_CREATED.iType <> iMissionType! g_FMMC_ROCKSTAR_CREATED.iType = ", g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iNewJob].iType) 
							NET_DW_PRINT_STRING_INT("[SEND_NPC_INVITE_SERVER] iMissionType = ", iMissionType)
						#ENDIF
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD NET_DW_PRINT("[SEND_NPC_INVITE_SERVER] bInUse not set! ") #ENDIF
				ENDIF
				
			ELSE
				
				iSendInviteProg = 99
				#IF IS_DEBUG_BUILD NET_DW_PRINT("[SEND_NPC_INVITE_SERVER] iNumValidJobs = 0!") #ENDIF
				#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[SEND_NPC_INVITE_SERVER] iSendInviteProg = ", iSendInviteProg) #ENDIF
			ENDIF
		BREAK
		
		CASE 99
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Figure out the job type for the next npc invite
/// RETURNS:
///    The FMMC_TYPE_ for the job
FUNC INT GET_NEXT_NPC_INVITE_JOB_TYPE(BOOL bAllowRaces = TRUE, BOOL bAllowDM = TRUE, BOOL bAllowPara = TRUE, BOOL bAllowSurvival = TRUE)
	INT iNextJobType = -1
	INT iLastJobType = serverBD.iMissionTypeForInvite // Try to at least pick a different job from last npc invite
	INT iValid[4]
	INT iTotalValid = 0
	INT i
	
	FOR i = 0 TO 3
		iValid[i] = -1
	ENDFOR
	
	//-- COmpile array of valid job types
	IF iNextJobType = -1
		IF bAllowRaces
			IF NOT IS_BIT_SET(serverBD.iServerRecentlyPlayedBitset , biS_SomeoneDidRace)
				IF iLastJobType <> FMMC_TYPE_RACE
					#IF IS_DEBUG_BUILD NET_DW_PRINT("[GET_NEXT_NPC_INVITE_JOB_TYPE] Races valid") #ENDIF
					iValid[iTotalValid] = FMMC_TYPE_RACE
					iTotalValid++
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF iNextJobType = -1
		IF bAllowDM
			IF NOT IS_BIT_SET(serverBD.iServerRecentlyPlayedBitset , biS_SomeoneDidDeathmatch)
				IF iLastJobType <> FMMC_TYPE_DEATHMATCH
					#IF IS_DEBUG_BUILD NET_DW_PRINT("[GET_NEXT_NPC_INVITE_JOB_TYPE] Deathmatch valid") #ENDIF
					iValid[iTotalValid] = FMMC_TYPE_DEATHMATCH
					iTotalValid++
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF iNextJobType = -1
		IF bAllowSurvival
			IF NOT IS_BIT_SET(serverBD.iServerRecentlyPlayedBitset , biS_SomeoneDidSurvival)
				IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_PlayerNotUnlockedSurvival)
					IF iLastJobType <> FMMC_TYPE_SURVIVAL
						#IF IS_DEBUG_BUILD NET_DW_PRINT("[GET_NEXT_NPC_INVITE_JOB_TYPE] Survival valid") #ENDIF
						iValid[iTotalValid] = FMMC_TYPE_SURVIVAL
						iTotalValid++
					ENDIF
				ELSE	
					#IF IS_DEBUG_BUILD NET_DW_PRINT("[GET_NEXT_NPC_INVITE_JOB_TYPE] Not picking FMMC_TYPE_SURVIVAL as biS_PlayerNotUnlockedSurvival is set") #ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF iNextJobType = -1
		IF bAllowPara
			IF NOT IS_BIT_SET(serverBD.iServerRecentlyPlayedBitset , biS_SomeoneDidParachute)
				IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_PlayerNotUnlockedParachuting)
					IF iLastJobType <> FMMC_TYPE_BASE_JUMP
						#IF IS_DEBUG_BUILD NET_DW_PRINT("[GET_NEXT_NPC_INVITE_JOB_TYPE] Parachuting valid") #ENDIF
						iValid[iTotalValid] = FMMC_TYPE_BASE_JUMP
						iTotalValid++
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD NET_DW_PRINT("[GET_NEXT_NPC_INVITE_JOB_TYPE] Not picking FMMC_TYPE_BASE_JUMP as biS_PlayerNotUnlockedParachuting is set") #ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//-- Pick at random one of the valid job types
	IF iNextJobType = -1
		IF iTotalValid > 1 
			i = GET_RANDOM_INT_IN_RANGE(0, iTotalValid)
			IF iValid[i] <> -1
				iNextJobType = iValid[i]
				
				#IF IS_DEBUG_BUILD 
					NET_DW_PRINT_STRING_INT("[GET_NEXT_NPC_INVITE_JOB_TYPE] Total valid jobs  = ", iTotalValid)
					NET_DW_PRINT_STRING_INT("[GET_NEXT_NPC_INVITE_JOB_TYPE] random int  = ", i)
					NET_DW_PRINT_STRINGS("[GET_NEXT_NPC_INVITE_JOB_TYPE] Using job type ", 
											GET_FMMC_BIT_SET_NAME_FOR_LOGS(iValid[i])) 
				#ENDIF
			ENDIF
			
		ELIF iTotalValid = 1
			IF iValid[0] <> -1
				iNextJobType = iValid[0]
				#IF IS_DEBUG_BUILD 
					NET_DW_PRINT_STRINGS("[GET_NEXT_NPC_INVITE_JOB_TYPE] Only found one valid job type, using ", 
										GET_FMMC_BIT_SET_NAME_FOR_LOGS(iValid[0])) 
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF iNextJobType = -1
		#IF IS_DEBUG_BUILD NET_DW_PRINT("[GET_NEXT_NPC_INVITE_JOB_TYPE] Didn't find a job type, will pick one at random") #ENDIF
		
		// Rank requirements:
		// FMMC_TYPE_RACE = FMMC_TYPE_DEATHMATCH < FMMC_TYPE_BASE_JUMP < FMMC_TYPE_SURVIVAL
		
		//-- Pick a job at random, but don't pick something that a Player that needs a job hasn't unlocked
		INT iMaxRand
		IF (IS_BIT_SET(serverBD.iServerBitSet, biS_PlayerNotUnlockedParachuting) OR NOT bAllowPara)
			iMaxRand = 2
		ELIF (IS_BIT_SET(serverBD.iServerBitSet, biS_PlayerNotUnlockedSurvival) OR NOT bAllowSurvival)
			iMaxRand = 3
		ELSE
			iMaxRand = 4
		ENDIF
		
		INT iRand = GET_RANDOM_INT_IN_RANGE(0, iMaxRand)
		
		IF iRand = 0 iNextJobType = FMMC_TYPE_RACE
		ELIF iRand = 1 iNextJobType = FMMC_TYPE_DEATHMATCH
		ELIF iRand = 2 iNextJobType = FMMC_TYPE_BASE_JUMP
		ELIF iRand = 3 iNextJobType = FMMC_TYPE_SURVIVAL
		ENDIF
		
	ENDIF
	
	RETURN iNextJobType
ENDFUNC

PROC PROCESS_MISSION_SERVER()
	INT iNextJob
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF IS_BIT_SET(serverBD.iServerBitSet, biS_SendNpcInvite)
			
			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_SetupNextMissionType)
				iNextJob = GET_NEXT_NPC_INVITE_JOB_TYPE(FALSE, TRUE, FALSE, FALSE)
				IF iNextJob > -1
					serverBD.iMissionTypeForInvite = iNextJob
					SET_BIT(serverBD.iServerBitSet, biS_SetupNextMissionType)
					#IF IS_DEBUG_BUILD NET_DW_PRINT_STRINGS("Got next job type... ", GET_FMMC_BIT_SET_NAME_FOR_LOGS(iNextJob)) #ENDIF
				ENDIF
			ELSE
				IF SEND_NPC_INVITE_SERVER(serverBD.iMissionTypeForInvite)
					CLEAR_BIT(serverBD.iServerBitSet, biS_SendNpcInvite)
					CLEAR_BIT(serverBD.iServerBitSet, biS_SetupNextMissionType)
					CLEAR_BIT(serverBD.iServerBitSet, biS_PlayerNotUnlockedParachuting)
					CLEAR_BIT(serverBD.iServerBitSet, biS_PlayerNotUnlockedSurvival)
					CLEAR_BIT(serverBD.iServerBitSet, biS_SHouldComeFromLester)
					RESET_NET_TIMER(serverBD.timeSinceLastNpcInvite)
				ENDIF
			ENDIF
	
		ENDIF
		
		
		
	ENDIF
ENDPROC	

PROC CLEANUP_SCRIPT()
	#IF IS_DEBUG_BUILD
		NET_DW_PRINT("Running cleanup")
	#ENDIF
	Clear_Any_Objective_Text_From_This_Script()
	
	
	
	//TERMINATE_THIS_THREAD()
	//RESET_FMMC_MISSION_VARIABLES(FALSE, FALSE, ciFMMC_END_OF_MISSION_STATUS_PASSED)
	
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

/// PURPOSE:
///    Do necessary pre game start ini.
PROC PROCESS_PRE_GAME(MP_MISSION_DATA missionScriptArgs)
	// This marks the script as a net script, and handles any instancing setup. 
	SETUP_MP_MISSION_FOR_NETWORK(GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(missionScriptArgs.mdID.idMission) , missionScriptArgs)
	
	// This makes sure the net script is active, waits until it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
	
	// This script will not be paused if another script calls PAUSE_GAME
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	

	// KGM: Wait for the first network broadcast before moving on - call this as the last instruction in pre-game
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		#IF IS_DEBUG_BUILD
			NET_DW_PRINT("Failed to receive an initial network broadcast. Cleaning up.")
		#ENDIF
		CLEANUP_SCRIPT()
	ENDIF
	
	
	#IF IS_DEBUG_BUILD
		
		NET_DW_PRINT("Launching....V1")
	#ENDIF
ENDPROC

FUNC BOOL ARE_ALL_PLAYERS_FINISHED()
	
	RETURN IS_BIT_SET(serverBD.iServerBitSet, biS_AllPlayersFinished)
ENDFUNC

FUNC BOOL HAVE_MISSION_END_CONDITIONS_BEEN_MET()
	IF ARE_ALL_PLAYERS_FINISHED()
		#IF IS_DEBUG_BUILD NET_DW_PRINT("[HAVE_MISSION_END_CONDITIONS_BEEN_MET] True because ARE_ALL_PLAYERS_FINISHED") #ENDIF
		RETURN TRUE	
	ENDIF
	RETURN FALSE
ENDFUNC
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                      Main Loop                          ///////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

SCRIPT(MP_MISSION_DATA missionScriptArgs)
	
	
	PROCESS_PRE_GAME(missionScriptArgs)	
	
	#IF IS_DEBUG_BUILD
		CREATE_WIDGETS()	
	#ENDIF
	
	
	
	WHILE TRUE
	
		MP_LOOP_WAIT_ZERO()
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
			SCRIPT_PROFILER_START_OF_FRAME()
		#ENDIF
		#ENDIF		
		
		

		// If we have a match end event, bail.
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		
			CLEANUP_SCRIPT()
		ENDIF	
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER( "Cleanup checks")
		#ENDIF
		#ENDIF 
		
		#IF IS_DEBUG_BUILD
			UPDATE_WIDGETS()
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER( "UPDATE_WIDGETS")
			#ENDIF
		#ENDIF
		
		
// *****************************************************************************************************************************************************************	
// *****************************************************************************************************************************************************************	
//												LOCAL PROCESSING
// *****************************************************************************************************************************************************************	
// *****************************************************************************************************************************************************************
		
		
		// -----------------------------------
		// Process your game logic.....
		SWITCH GET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT())
			
			// Wait until the server gives the all go before moving on
			CASE GAME_STATE_INI
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					UPDATE_MY_UNLOCKED_JOBS()
					iLocalJobToUse = serverBD.iJobToUse
					
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_RUNNING
					
					#IF IS_DEBUG_BUILD NET_DW_PRINT("playerBD[PARTICIPANT_ID_TO_INT()].iGameState GAME_STATE_INI >  GAME_STATE_RUNNING") #ENDIF
					
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY
					
					#IF IS_DEBUG_BUILD NET_DW_PRINT("playerBD[PARTICIPANT_ID_TO_INT()].iGameState GAME_STATE_INI >  GAME_STATE_TERMINATE_DELAY") #ENDIF
					
				ENDIF
			BREAK
			
			// Main gameplay state.
			CASE GAME_STATE_RUNNING
				
				
				
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING	
					
					PROCESS_MISSION_CLIENT()
				
				
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY
					#IF IS_DEBUG_BUILD NET_DW_PRINT("playerBD[PARTICIPANT_ID_TO_INT()].iGameState GAME_STATE_RUNNING >  GAME_STATE_TERMINATE_DELAY") #ENDIF
				ENDIF
				
				
				//-- Leave if we start a mission
			/*	IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
					#IF IS_DEBUG_BUILD NET_DW_PRINT("playerBD[PARTICIPANT_ID_TO_INT()].iGameState GAME_STATE_RUNNING > GAME_STATE_END as on mission ") #ENDIF
					
				ENDIF
				*/			
			BREAK
			
			CASE GAME_STATE_TERMINATE_DELAY 
				SET_MISSION_FINISHED(serverBD.timeTerminate)
		      	IF IS_MISSION_READY_TO_CLEANUP(serverBD.timeTerminate)
		            playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
					#IF IS_DEBUG_BUILD NET_DW_PRINT("playerBD[PARTICIPANT_ID_TO_INT()].iGameState GAME_STATE_TERMINATE_DELAY >  GAME_STATE_END") #ENDIF
		      	ENDIF
			BREAK
			
			// The game stage the local player is placed when we want him to leave a mission
			CASE GAME_STATE_LEAVE

				playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
				#IF IS_DEBUG_BUILD NET_DW_PRINT("playerBD[PARTICIPANT_ID_TO_INT()].iGameState GAME_STATE_LEAVE >  GAME_STATE_END") #ENDIF

			FALLTHRU
			
			//Cleans up then terminates the mission
			CASE GAME_STATE_END
				#IF IS_DEBUG_BUILD NET_DW_PRINT("About to run cleanup from GAME_STATE_END") #ENDIF
				CLEANUP_SCRIPT()
			BREAK

		ENDSWITCH
		
	 	

		
// *****************************************************************************************************************************************************************	
// *****************************************************************************************************************************************************************	
//												SERVER PROCESSING
// *****************************************************************************************************************************************************************	
// *****************************************************************************************************************************************************************	
		
		
		
		// -----------------------------------
		// Process server game logic		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			
			SWITCH GET_SERVER_MISSION_STATE()
				
				// For CnC group missions, we may want to wait for all players to have launched the script
				CASE GAME_STATE_INI
					//START_NET_TIMER(serverBD.timeSinceLastNpcInvite)
					serverBD.iServerGameState = GAME_STATE_RUNNING
					#IF IS_DEBUG_BUILD NET_DW_PRINT("serverBD.iServerGameState = GAME_STATE_RUNNING") #ENDIF
						
				BREAK
				
				// Look for game end conditions
				CASE GAME_STATE_RUNNING

					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE
						ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_HOLD_UP_TUTORIAL_SERVER")
					#ENDIF
					#ENDIF		
					
					PROCESS_SERVER_STAGGERED_LOOP()
					
				//	PROCESS_MISSION_SERVER()
					
					IF HAVE_MISSION_END_CONDITIONS_BEEN_MET()
						serverBD.iServerGameState = GAME_STATE_END
						#IF IS_DEBUG_BUILD NET_DW_PRINT ("serverBD.iServerGameState GAME_STATE_RUNNING > GAME_STATE_END ")#ENDIF
						
					ENDIF
					
				BREAK
				
				CASE GAME_STATE_END
				BREAK
				
			ENDSWITCH
			
		ENDIF
		
		
		
		#IF IS_DEBUG_BUILD 
			#IF SCRIPT_PROFILER_ACTIVE
				SCRIPT_PROFILER_END_OF_FRAME()
			#ENDIF
		#ENDIF			
	
	ENDWHILE

ENDSCRIPT
