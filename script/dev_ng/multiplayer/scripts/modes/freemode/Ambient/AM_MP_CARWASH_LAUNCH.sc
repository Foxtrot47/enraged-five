// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	am_mp_carwash_launch.sc
//		AUTHOR			:	Aaron Gandaa
//		DESCRIPTION		:	Multiplayer Carwash Launcher
//
// *****************************************************************************************
// *****************************************************************************************

//----------------------
//	INCLUDES
//----------------------
USING "brains.sch"
USING "rgeneral_include.sch"
USING "freemode_header.sch"
USING "net_mission.sch"

//----------------------
//	MAIN SCRIPT
//----------------------
SCRIPT(coords_struct in_coords)
	
	INT iTimer 
	INT scriptHash = HASH("carwash1")
	STRING scriptName = "carwash1" // "AM_MP_CARWASH_1"
	VECTOR vInCoords = in_coords.vec_coord[0] 	// Update world point
	
    IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH (HASH("am_mp_carwash_launch")) > 1
    	CPRINTLN(DEBUG_AMBIENT, "MP Launcher Carwash: is attempting to launch with an instance already active.")
    	TERMINATE_THIS_THREAD()
    ENDIF

	// Setup callback when player is killed, arrested or goes to multiplayer
	IF NETWORK_IS_GAME_IN_PROGRESS()
		NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
		CPRINTLN(DEBUG_AMBIENT, "MP Launcher Carwash: PROCESS_PRE_GAME started in MP")
		SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	ELSE
		CPRINTLN(DEBUG_AMBIENT, "MP Launcher Carwash: we aren't in a network game or the player is not ok - terminate")
		TERMINATE_THIS_THREAD()
	ENDIF

	IF (g_bLoadedClifford)
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_MP_CARWASH_LAUNCH_CLF")) = 0
			REQUEST_SCRIPT_WITH_NAME_HASH(HASH("AM_MP_CARWASH_LAUNCH_CLF"))
			WHILE NOT HAS_SCRIPT_WITH_NAME_HASH_LOADED(HASH("AM_MP_CARWASH_LAUNCH_CLF"))
				WAIT(0)
			ENDWHILE
			START_NEW_SCRIPT_WITH_NAME_HASH_AND_ARGS(HASH("AM_MP_CARWASH_LAUNCH_CLF"),in_coords, SIZE_OF(in_coords),DEFAULT_STACK_SIZE)
		ENDIF
		
		//Shutdown bed save script since we're switching to AGT
		TERMINATE_THIS_THREAD()
	ENDIF
	
	CPRINTLN(DEBUG_AMBIENT, "MP Launcher Carwash - Initializing - World Point:", vInCoords)
	IF IS_ENTITY_OK(PLAYER_PED_ID()) 
		CPRINTLN(DEBUG_AMBIENT, "Distance From Player To World Point: ", GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vInCoords, FALSE))
	ENDIF
	
	BOOL bOk = FALSE
	WHILE (bOk = FALSE)
		IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
		OR HIDE_BLIP_BECAUSE_PLAYER_IS_STARTING_RACE()
		OR NOT HAS_PLAYER_COMPLETED_RACE_AND_DM_TUTORIAL(PLAYER_ID())
		OR IS_FM_MISSION_LAUNCH_IN_PROGRESS()
		OR IS_PLAYER_USING_RCBANDITO(PLAYER_ID())
		#IF FEATURE_CASINO_HEIST
		OR IS_PLAYER_USING_RC_TANK(PLAYER_ID())
		#ENDIF
		#IF FEATURE_DLC_1_2022
		OR IS_PLAYER_TEST_DRIVING_A_VEHICLE(PLAYER_ID())
		#ENDIF // #IF FEATURE_DLC_1_2022
			bOk = FALSE
		ELSE
			bOK = TRUE
		ENDIF
		
		WAIT(0)
	ENDWHILE
	
	// get the closest carwash
	IF VDIST2(vInCoords, GET_STATIC_BLIP_COORDS(STATIC_BLIP_AMBIENT_CARWASH_SHORT)) < VDIST2(vInCoords, GET_STATIC_BLIP_COORDS(STATIC_BLIP_AMBIENT_CARWASH_LONG))
		scriptName = "carwash2"
	ENDIF
	
	// load it
	iTimer = GET_GAME_TIMER() 
	//scriptName = "rtestlauncher"
	scriptHash = GET_HASH_KEY(scriptName)
			
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(scriptHash) = 0
		REQUEST_SCRIPT(scriptName)
		WHILE NOT HAS_SCRIPT_LOADED(scriptName)
			REQUEST_SCRIPT(scriptName)
			WAIT(0)
			
			IF GET_GAME_TIMER() > (iTimer + 20000)
				CPRINTLN(DEBUG_AMBIENT, "MP Launcher Carwash - Script:", scriptName, " has taken too long to load - terminate")
				TERMINATE_THIS_THREAD()
			ENDIF
		ENDWHILE
		
		IF HAS_SCRIPT_LOADED(scriptName)
			START_NEW_SCRIPT(scriptName, DEFAULT_STACK_SIZE)
			SET_SCRIPT_AS_NO_LONGER_NEEDED(scriptName)
		ENDIF
	ENDIF
	
	TERMINATE_THIS_THREAD()
ENDSCRIPT



