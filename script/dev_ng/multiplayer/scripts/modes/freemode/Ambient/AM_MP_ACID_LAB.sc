USING "globals.sch"

USING "commands_network.sch"
USING "script_player.sch"
USING "commands_path.sch"
USING "commands_event.sch"

USING "net_include.sch"
USING "net_events.sch"
USING "net_spawn.sch"
USING "net_ambience.sch"

USING "net_wait_zero.sch"
USING "freemode_events_header.sch"

USING "net_MP_Radio.sch"
USING "net_gun_locker.sch"
USING "safehouse_sitting_activities.sch"
	
USING "net_sliding_doors_lib.sch"
USING "net_property_sections_lib.sch"
USING "net_activity_creator_activities.sch"

#IF FEATURE_DLC_2_2022
USING "net_realty_ACID_LAB.sch"


//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════════╡ CONSTS  ╞═════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

CONST_INT BS_ACID_LAB_PREVENT_JACKING						0


//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════════╡ VARIABLES  ╞══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

STRUCT ServerBroadcastData
	INT iBS
ENDSTRUCT
ServerBroadcastData serverBD

STRUCT PlayerBroadcastData
	INT iBS
ENDSTRUCT
PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]

ENUM ACID_LAB_SCRIPT_STATE
	ACID_LAB_STATE_LOADING,
	ACID_LAB_STATE_IDLE
ENDENUM

STRUCT ACID_LAB_DATA
	ACID_LAB_SCRIPT_STATE eState
	
	SIMPLE_INTERIORS eSimpleInteriorID
	
	INT iScriptInstance, iLocalBS, iInvitingPlayer
	
	PLAYER_INDEX pOwner
	
	THREADID CarModThread
	
	SCRIPT_TIMER sCarModScriptRunTimer
	
ENDSTRUCT
ACID_LAB_DATA thisAcidLab

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════════╡ CLEANUP  ╞═══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC SCRIPT_CLEANUP()

	#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_ACID_LAB - SCRIPT_CLEANUP")
	#ENDIF
	
	IF IS_NET_PLAYER_OK(PLAYER_ID())
	AND IS_BIT_SET(thisAcidLab.iLocalBS, BS_ACID_LAB_PREVENT_JACKING)
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_NotAllowedToJackAnyPlayers, FALSE)
	ENDIF
	
	IF IS_GAMEPLAY_CAM_SHAKING()
		STOP_GAMEPLAY_CAM_SHAKING(TRUE)
	ENDIF
	
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ INITIALISATION  ╞════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC INITIALISE()
	//waiting until sections are loaded.
	SET_SIMPLE_INTERIOR_CHILD_SCRIPT_BLOCKING_FADE_IN(TRUE)
	SET_SIMPLE_INTERIOR_CHILD_SCRIPT_ALLOWS_FOR_WARP(TRUE)
	
	g_SimpleInteriorData.bDontSimulateInputGaitOnEntrance = TRUE
ENDPROC

/// PURPOSE:
///    Usees the iPlayerIJoinedBS to find the owner of the hacker truck we have entered
/// RETURNS:
///    TRUE when know who the owner is
FUNC BOOL DETERMINE_ACID_LAB_OWNER()
	//We have to search for the owner as we can't rely on gang membership
	IF IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_ACID_LAB_ACCESS_BS_OWNER)
		thisAcidLab.pOwner = PLAYER_ID()
		g_ownerOfHackerTruckPropertyIAmIn = thisAcidLab.pOwner
		PRINTLN("AM_MP_ACID_LAB - DETERMINE_ACID_LAB_OWNER - I ", GET_PLAYER_NAME(thisAcidLab.pOwner), " am the owner. access BS =  ", g_SimpleInteriorData.iAccessBS)
		RETURN TRUE
	ELIF g_SimpleInteriorData.iAccessBS > 0
		IF globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner != INVALID_PLAYER_INDEX()
			thisAcidLab.pOwner = globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner
			g_ownerOfHackerTruckPropertyIAmIn = thisAcidLab.pOwner
			PRINTLN("AM_MP_ACID_LAB - DETERMINE_ACID_LAB_OWNER - I am not the owner. The owner is: ", GET_PLAYER_NAME(thisAcidLab.pOwner))
			RETURN TRUE
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	PRINTLN("AM_MP_ACID_LAB - DETERMINE_ACID_LAB_OWNER - g_SimpleInteriorData.iAccessBS = ", g_SimpleInteriorData.iAccessBS)
	#ENDIF
	
	IF (GET_FRAME_COUNT() % 60) = 0
		PRINTLN("AM_MP_ACID_LAB - DETERMINE_ACID_LAB_OWNER - Trying to find the owner")
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SCRIPT_INITIALISE(SIMPLE_INTERIOR_CHILD_SCRIPT_DATA& scriptData)
	
	WHILE NOT DETERMINE_ACID_LAB_OWNER()
		
		IF NOT NETWORK_IS_GAME_IN_PROGRESS()
			PRINTLN("AM_MP_ACID_LAB - NETWORK_IS_GAME_IN_PROGRESS = FALSE")
			SCRIPT_CLEANUP()
		ENDIF
		
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			PRINTLN("AM_MP_ACID_LAB - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE = TRUE")
			SCRIPT_CLEANUP()
		ENDIF
		
		IF SHOULD_THIS_SIMPLE_INTERIOR_CHILD_THREAD_TERMINATE(scriptData.iScriptInstance, scriptData.eSimpleInteriorID)
			PRINTLN("AM_MP_ACID_LAB - SHOULD_THIS_SIMPLE_INTERIOR_CHILD_THREAD_TERMINATE = TRUE")
			SCRIPT_CLEANUP()
		ENDIF
		
		IF IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_ACID_LAB_ACCESS_BS_OWNER_LEFT_GAME)
			PRINTLN("AM_MP_ACID_LAB - SIMPLE_INTERIOR_BUNKER_ACCESS_BS_OWNER_LEFT_GAME = TRUE")
			SCRIPT_CLEANUP()
		ENDIF
		
		WAIT(0)
	ENDWHILE
	
	thisAcidLab.eSimpleInteriorID = scriptData.eSimpleInteriorID
	thisAcidLab.iScriptInstance = scriptData.iScriptInstance
	thisAcidLab.iInvitingPlayer = scriptData.iInvitingPlayer

	NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE, thisAcidLab.iScriptInstance)
	
	// This makes sure the net script is active, waits untill it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	RESERVE_NETWORK_MISSION_VEHICLES(1) //for Personal vehicle
//	RESERVE_NETWORK_MISSION_PEDS(1) // For fake MOC wanted ped

	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))

	// KGM: Wait for the first network broadcast before moving on
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		PRINTLN("AM_MP_ACID_LAB - FAILED TO RECEIVE INITIAL NETWORK BROADCAST - SCRIPT CLEANUP")
		SCRIPT_CLEANUP()
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		// This script will not be paused if another script calls PAUSE_GAME
		PRINTLN("AM_MP_ACID_LAB - INITIALISED")
	ELSE
		PRINTLN("AM_MP_ACID_LAB - NETWORK_IS_GAME_IN_PROGRESS = FALSE - SCRIPT CLEANUP")
		SCRIPT_CLEANUP()
	ENDIF
	
	INITIALISE()
	
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ PROCEDURES  ╞═══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

FUNC BOOL IS_ACID_LAB_STATE(ACID_LAB_SCRIPT_STATE eState)
	RETURN thisAcidLab.eState = eState
ENDFUNC

PROC SET_ACID_LAB_STATE(ACID_LAB_SCRIPT_STATE eState)
	CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] AM_MP_ACID_LAB - SET_ACID_LAB_STATE - New state: ", ENUM_TO_INT(eState))
	thisAcidLab.eState = eState
	IF eState = ACID_LAB_STATE_IDLE
		CLEAR_GLOBAL_ENTRY_DATA()
		SET_DRIVER_ENTERING_SIMPLE_INTERIOR(FALSE)
	ENDIF
ENDPROC

PROC MANAGE_PLAYER_JACK_FLAGS()
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF !GET_PED_CONFIG_FLAG(PLAYER_PED_ID(),PCF_NotAllowedToJackAnyPlayers)
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_NotAllowedToJackAnyPlayers, TRUE)
			SET_BIT(thisAcidLab.iLocalBS, BS_ACID_LAB_PREVENT_JACKING)
			PRINTLN("MANAGE_PLAYER_JACK_FLAGS - SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_NotAllowedToJackAnyPlayers, TRUE)")
		ENDIF
	ENDIF
ENDPROC

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                 MAIN LOGIC PROC                  /////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

PROC RUN_MAIN_CLIENT_LOGIC()
	IF IS_ACID_LAB_STATE(ACID_LAB_STATE_LOADING)
		IF IS_PLAYER_IN_ACID_LAB(PLAYER_ID())

			//IF GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()) = GET_HASH_KEY("GtaMloRoom001") //Model swap for sections should wait until inside the interior
				// This is important, let internal script know that its child script is ready
				SET_SIMPLE_INTERIOR_CHILD_SCRIPT_READY(TRUE)
				
				// Move on to the next stage
				SET_TRANS_DONT_CLEAR_ENTERING_FLAG(FALSE)
				//Ready for warp into interior, will allow fade in.
				SET_SIMPLE_INTERIOR_CHILD_SCRIPT_BLOCKING_FADE_IN(FALSE)
				
				IF IS_INTERACTION_MENU_DISABLED()
					ENABLE_INTERACTION_MENU()
				ENDIF
				
				SET_ACID_LAB_STATE(ACID_LAB_STATE_IDLE)
//			ELSE
//				#IF IS_DEBUG_BUILD
//				IF GET_FRAME_COUNT() % 30 = 0
//					PRINTLN("RUN_MAIN_CLIENT_LOGIC- waiting for RoomKey, Current: ", GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()))
//				ENDIF
//				#ENDIF
//			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			IF GET_FRAME_COUNT() % 30 = 0
				PRINTLN("RUN_MAIN_CLIENT_LOGIC- waiting for IS_PLAYER_IN_ACID_LAB")
			ENDIF
			#ENDIF
		ENDIF
	ELIF IS_ACID_LAB_STATE(ACID_LAB_STATE_IDLE)
		MANAGE_PLAYER_JACK_FLAGS()
	ENDIF
ENDPROC

PROC RUN_MAIN_SERVER_LOGIC()
	
ENDPROC

#IF IS_DEBUG_BUILD
PROC UPDATE_DEBUG_WIDGETS()

ENDPROC
#ENDIF

#ENDIF
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                      Main Loop                      //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
SCRIPT #IF FEATURE_DLC_2_2022	(SIMPLE_INTERIOR_CHILD_SCRIPT_DATA scriptData) #ENDIF
	
	#IF FEATURE_DLC_2_2022
	
	IF NETWORK_IS_GAME_IN_PROGRESS() 
		SCRIPT_INITIALISE(scriptData)	
	ELSE
		SCRIPT_CLEANUP()
	ENDIF

	// Main loop
	WHILE TRUE
		
		// One wait to rule them all. This can be the ONLY wait from here on in.....
		MP_LOOP_WAIT_ZERO()
				
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			PRINTLN("AM_MP_ACID_LAB - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE = TRUE")
			SCRIPT_CLEANUP()
		ENDIF
		
		IF SHOULD_THIS_SIMPLE_INTERIOR_CHILD_THREAD_TERMINATE(scriptData.iScriptInstance, thisAcidLab.eSimpleInteriorID)
			PRINTLN("AM_MP_ACID_LAB - SHOULD_THIS_SIMPLE_INTERIOR_CHILD_THREAD_TERMINATE = TRUE")
			SCRIPT_CLEANUP()
		ENDIF
		
		RUN_MAIN_CLIENT_LOGIC()
		
		IF NETWORK_GET_HOST_OF_THIS_SCRIPT() != INT_TO_NATIVE(PARTICIPANT_INDEX , -1)	
			g_iHostOfam_mp_acid_lab = NATIVE_TO_INT(NETWORK_GET_PLAYER_INDEX(NETWORK_GET_HOST_OF_THIS_SCRIPT()))
		ENDIF
		
		// -----------------------------------
		// Process server game logic		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			RUN_MAIN_SERVER_LOGIC()
		ENDIF
		
		#IF IS_DEBUG_BUILD
			UPDATE_DEBUG_WIDGETS()
		#ENDIF
		
	ENDWHILE
	
	#ENDIF
ENDSCRIPT

