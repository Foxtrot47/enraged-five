//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        AM_MP_HANGAR.sch																			//
// Description: Script for managing the interior of a AM_MP_HANGAR. AM_MP_HANGAR access, spawning etc. 		//
//				is managed by AM_MP_SMPL_INTERIOR_* script while this script is launched by simple interior	//
//				script to handle anything specific to AM_MP_HANGAR.											//
// Written by:  Online Technical Team: Scott Ranken															//
// Date:  		07/06/2017																					//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"

USING "commands_network.sch"
USING "script_player.sch"
USING "commands_path.sch"
USING "commands_event.sch"

USING "net_include.sch"
USING "net_events.sch"
USING "net_spawn.sch"
USING "net_ambience.sch"

USING "net_wait_zero.sch"
USING "freemode_events_header.sch"

USING "net_MP_Radio.sch"

USING "net_MP_CCTV.sch"
USING "net_MP_TV.sch"

USING "safehouse_sitting_activities.sch"

USING "net_cash_transactions.sch"
USING "mp_bed_high.sch"
USING "net_MP_Radio.sch"

USING "net_simple_interior.sch"
USING "net_simple_cutscene.sch"
USING "net_simple_cutscene_interior.sch"
USING "net_realty_hangar.sch"

#IF IS_DEBUG_BUILD
USING "net_debug_log.sch"
#ENDIF


//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ VARIABLES ╞═════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

CONST_INT BS_HANGAR_PERSONAL_QUARTERS_TRADITIONAL_PURCHASED	0
CONST_INT BS_HANGAR_PERSONAL_QUARTERS_MODERN_PURCHASED		1
CONST_INT BS_HANGAR_MODSHOP_PURCHASED						2
CONST_INT BS_HANGAR_FURNITURE_STANDARD_PURCHASED			3
CONST_INT BS_HANGAR_FURNITURE_TRADITIONAL_PURCHASED			4
CONST_INT BS_HANGAR_FURNITURE_MODERN_PURCHASED				5
CONST_INT BS_HANGAR_STYLE_0_PURCHASED						6
CONST_INT BS_HANGAR_STYLE_1_PURCHASED						7
CONST_INT BS_HANGAR_STYLE_2_PURCHASED						8
CONST_INT BS_HANGAR_STYLE_3_PURCHASED						9
CONST_INT BS_HANGAR_STYLE_4_PURCHASED						10
CONST_INT BS_HANGAR_STYLE_5_PURCHASED						11
CONST_INT BS_HANGAR_STYLE_6_PURCHASED						12
CONST_INT BS_HANGAR_STYLE_7_PURCHASED						13
CONST_INT BS_HANGAR_STYLE_8_PURCHASED						14
CONST_INT BS_HANGAR_FLOOR_DECAL_0_PURCHASED					15
CONST_INT BS_HANGAR_FLOOR_DECAL_1_PURCHASED					16
CONST_INT BS_HANGAR_FLOOR_DECAL_2_PURCHASED					17
CONST_INT BS_HANGAR_FLOOR_DECAL_3_PURCHASED					18
CONST_INT BS_HANGAR_FLOOR_DECAL_4_PURCHASED					19
CONST_INT BS_HANGAR_FLOOR_DECAL_5_PURCHASED					20
CONST_INT BS_HANGAR_FLOOR_DECAL_6_PURCHASED					21
CONST_INT BS_HANGAR_FLOOR_DECAL_7_PURCHASED					22
CONST_INT BS_HANGAR_FLOOR_DECAL_8_PURCHASED					23
CONST_INT BS_HANGAR_LIGHTING_0_PURCHASED					24
CONST_INT BS_HANGAR_LIGHTING_1_PURCHASED					25
CONST_INT BS_HANGAR_LIGHTING_2_PURCHASED					26
CONST_INT BS_HANGAR_LIGHTING_3_PURCHASED					27
CONST_INT BS_HANGAR_WATCHING_INTRO_MOCAP					28
CONST_INT BS_HANGAR_CHRISTMAS_DECORATIONS_CREATED			29

/// BS2 boradcast data consts
CONST_INT BS2_HANGAR_DATA_READY_TO_WARP_OUT					0

//CONST_INT MAX_SAVED_VEHS_IN_HANGAR						20

//!!Need to be careful what you use these bits with!!
//The below const ints are for thisHangar.ibs not player BD

CONST_INT BS_HANGAR_DATA_INTERIOR_REFRESHED_ON_INIT				0
CONST_INT BS_HANGAR_DATA_LOAD_SCENE_STARTED						1
CONST_INT BS_HANGAR_KILL_LOAD_SCENE_WHEN_INSIDE					2
CONST_INT BS_HANGAR_DATA_BOSS_WAS_WATCHING_CUTSCENE_LAST_FRAME	3
CONST_INT BS_HANGAR_DATA_PLAYING_SETUP_CUTSCENE					4
CONST_INT BS_HANGAR_DATA_DID_INTERIOR_REFRESH_FOR_CUTSCENE		5
CONST_INT BS_HANGAR_DATA_LOAD_SCENE_FOR_CUTSCENE_STARTED		6				
CONST_INT BS_HANGAR_DATA_DID_VEH_WARP							7
CONST_INT BS_HANGAR_DATA_GARAGE_ENTRY_HELP_TEXT					8
CONST_INT BS_HANGAR_CALLED_CLEAR_HELP							9
CONST_INT BS_HANGAR_FORCED_CHAIR_TO_ROOM						10
CONST_INT BS_HANGAR_SPAWN_CUTSCENE_PRODUCT						11
CONST_INT BS_HANGAR_CLEAN_CUTSCENE_PRODUCT						12
CONST_INT BS_HANGAR_DATA_ENTERED_IN_PEGASUS						13
CONST_INT BS_HANGAR_DATA_ENTERED_IN_STORAGE						14
CONST_INT BS_HANGAR_PRODUCT_DELIVERED							15	//Used to deliver any cargo we hold after entering the hangar
CONST_INT BS_HANGAR_INIT_HANGAR_TIMECYCLE_MODS					16
CONST_INT BS_HANGAR_CLEARED_TIMECYCLE_MODS						17
CONST_INT BS_HANGAR_HAS_SCRIPT_MOD_BEEN_PUSHED					18
CONST_INT BS_HANGAR_DISABLE_ACTIVITIES_FOR_WARP					19
CONST_INT BS_HANGAR_SUPRESS_CONTROL_SHAKE						20
CONST_INT BS_HANGAR_CLEARED_TIMECYCLE_MODIFIERS					21
CONST_INT BS_HANGAR_SET_VEH_ON_GROUND_PROPERLY					22

CONST_INT MAX_INTRO_CUTSCENE_FALLBACK_TIME				300000 // Duration of intro cutscene, 5 minutes.

TWEAK_FLOAT fHUGE_HANGAR_VEHICLE_Pitch_Limit_Min		-17.5
TWEAK_FLOAT fHUGE_HANGAR_VEHICLE_Pitch_Limit_Max		65.0

TWEAK_FLOAT fHUGE_HANGAR_VEHICLE_Orbit_Limit_Min		20.0
TWEAK_FLOAT fHUGE_HANGAR_VEHICLE_Orbit_Limit_Max		30.0

TWEAK_FLOAT fHUGE_HANGAR_VEHICLE_Heading_Limit_Min	-75.0
TWEAK_FLOAT fHUGE_HANGAR_VEHICLE_Heading_Limit_Max	75.0

//END thisHangar.ibs CONSTS

ENUM HANGAR_SERVER_STATE
	HANGAR_SERVER_STATE_LOADING,
	HANGAR_SERVER_STATE_IDLE
ENDENUM

STRUCT GET_OUT_OF_THE_WAY_HANGAR_SERVER_VARS
	INT iPlayerBitSet
	INT iCreationBitSet
	INT iClientRequestBS
ENDSTRUCT

STRUCT ServerBroadcastData
	HANGAR_SERVER_STATE eState = HANGAR_SERVER_STATE_LOADING
	
	INT iBS
	INT iProductData[ciMAX_HANGAR_PRODUCT_UNITS]
	BOOL bUpdateHangarProductData
	BOOL bHangarVehiclesCreated = FALSE
	
	MP_RADIO_SERVER_DATA_STRUCT MPRadioServer
	MP_PROP_ACT_SERVER_CONTROL_STRUCT activityControl
	
	NETWORK_INDEX	customVehicleNetIDs[MAX_SAVED_VEHS_IN_HANGAR]
	INT iVehicleFadeBS
	INT ivehicleSaveSlot[MAX_SAVED_VEHS_IN_HANGAR]
	INT iCarSpawnBS[MAX_SAVED_VEHS_IN_HANGAR]
	INT iReserved
	INT iVehicleResBS
	INT iVehicleCreated
	INT iCreatedBS
	BOOL bOwnerCompletedVehicleCreation
	
	SERVER_BROADCAST_DATA_FOR_SEATS serverSeatBD
	
	GET_OUT_OF_THE_WAY_HANGAR_SERVER_VARS sGetOutOfTheWay
	
	INT iEnteringVeh = -1
	MP_TV_SERVER_DATA_STRUCT MPTVServer
	SERVER_BROADCAST_DATA_FOR_SIMPLE_SEATS serverTVSeatBD
ENDSTRUCT
ServerBroadcastData serverBD

STRUCT GET_OUT_OF_THE_WAY_HANGAR_PLAYER_VARS
	INT iPlayerBitSet
	VECTOR vGetOutOfWayLoc
ENDSTRUCT

STRUCT PlayerBroadcastData
	INT iBS
	INT iBS2
	MP_RADIO_CLIENT_DATA_STRUCT MPRadioClient
	MP_CCTV_CLIENT_DATA_STRUCT MPCCTVClient
	INT iActivityRequested
	GET_OUT_OF_THE_WAY_HANGAR_PLAYER_VARS sGetOutOfTheWay
	MP_TV_CLIENT_DATA_STRUCT MPTVClient
	ACTIVITY_SIMPLE_SEAT_BROADCASTDATA playerTVSeatBD
ENDSTRUCT
PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]

STRUCT GET_OUT_OF_THE_WAY_HANGAR_LOCAL_VARS
	INT iProgressBitSet
	VECTOR vSafePos
	SCRIPT_TIMER controlTimer
ENDSTRUCT
GET_OUT_OF_THE_WAY_HANGAR_LOCAL_VARS sGetOutOfWayVars

ENUM HANGAR_SCRIPT_STATE
	HANGAR_STATE_LOADING,
	HANGAR_STATE_IDLE,
	HANGAR_STATE_PLAYING_INTRO_CUTSCENE,
	HANGAR_STATE_PLAYING_SETUP_CUTSCENE,
	HANGAR_STATE_LOADING_GARAGE_EXIT,
	HANGAR_STATE_PLAYING_GARAGE_EXIT,
	HANGAR_STATE_FINISHING_GARAGE_EXIT
ENDENUM

ENUM HANGAR_UPGRADE_STATE
	HANGAR_UPGRADE_STATE_IDLE,
	HANGAR_UPGRADE_STATE_FADE_OUT,
	HANGAR_UPGRADE_STATE_WARP_PLAYER, 
	HANGAR_UPGRADE_STATE_ACTIVATE_ENTITY_SETS,
	HANGAR_UPGRADE_STATE_REFRESH_INTERIOR,
	HANGAR_UPGRADE_STATE_REFRESH_LIGHTING,
	HANGAR_UPGRADE_STATE_WAIT_FOR_VEHICLES,
	HANGAR_UPGRADE_STATE_FADE_IN
ENDENUM

ENUM HANGAR_CUTSCENE_STATE
	HANGAR_CUTSCENE_STATE_INITIALISE,
	HANGAR_CUTSCENE_STATE_ASSET_REQUEST,
	HANGAR_CUTSCENE_STATE_LOAD_INTERIOR,
	HANGAR_CUTSCENE_STATE_IDLING,
	HANGAR_CUTSCENE_STATE_PLAY_INTERIOR,
	HANGAR_CUTSCENE_STATE_LOAD_EXTERIOR,
	HANGAR_CUTSCENE_STATE_PLAY_EXTERIOR,
	HANGAR_CUTSCENE_STATE_CLEANUP
ENDENUM

STRUCT PRODUCT_DATA
	INT iSetProductFadeTimer[ciMAX_HANGAR_PRODUCT_UNITS]
	INT iProductFadeComplete[ciMAX_HANGAR_PRODUCT_UNITS]
	OBJECT_INDEX oProductObject[ciMAX_HANGAR_PRODUCT_UNITS]
	TIME_DATATYPE tdProductFadeTimer[ciMAX_HANGAR_PRODUCT_UNITS]
ENDSTRUCT

CONST_INT MANAGE_VEH_MENU_DISPLAYING												1
CONST_INT MANAGE_VEH_MENU_SETUP														2
CONST_INT MANAGE_VEH_MENU_DISABLE_CONTROL											3
CONST_INT MANAGE_VEH_MENU_DRAWING													4
CONST_INT MANAGE_VEH_MENU_DRAWING_LAST												5
CONST_INT MANAGE_VEH_MENU_AREA_STORAGE												6
CONST_INT MANAGE_VEH_MENU_PROCESS_REPLACE_TRANSACTION								7
CONST_INT MANAGE_VEH_MENU_CREATED_TRANSACTION_VEHICLE								8
CONST_INT MANAGE_VEH_MENU_APPLIED_DETAILS_TRANSACTION_VEHICLE						9

CONST_INT MANAGE_VEH_CREATE_SET_CUTSCENE_ENTITIES_TO_BE_NETWORKED					1
CONST_INT MANAGE_VEH_CREATE_VEHICLES_AVAILABLE_CHECKED								2
CONST_INT MANAGE_VEH_CREATE_PENDING_FADE											3

CONST_INT MANAGE_CAMERAS															4

ENUM VEHICLE_MANAGE_MENU_STAGE
	VEHICLE_MANAGE_MENU_STAGE_SELECT = 0,
	VEHICLE_MANAGE_MENU_STAGE_MOVE,
	VEHICLE_MANAGE_MENU_STAGE_REPLACE,
	VEHICLE_MANAGE_MENU_STAGE_AREA
ENDENUM

STRUCT VEHICLE_MANAGE_DATA
	//menu
	INT iManageVehMenuBS
	INT iManageVehicleContext = NEW_CONTEXT_INTENTION
	SCRIPT_TIMER menuNavigationDelay
	INT iMaxVertSel
	INT iCurVertSel
	INT iButtonBS
	INT iManageCamera
	CAMERA_INDEX camManage
	INT iSelectedVehSlot = -1
	INT iVehicleSlotIDs[MAX_SAVED_VEHS_IN_HANGAR]
	INT iSelectedVehDisplaySlot = -1
	INT iVehicleDisplaySlotIDs[MAX_SAVED_VEHS_IN_HANGAR]
	VEHICLE_MANAGE_MENU_STAGE eMenuStage = VEHICLE_MANAGE_MENU_STAGE_SELECT
	VEHICLE_INDEX vehTransaction 
	INT iNewCarTransactionResult
	INT iReplacedVehicle = -1
	INT iBlockAlphaBS = 0
	OBJECT_INDEX objProp
	
	//creation
	INT iManageVehCreateBS
	INT iVehSpaceAvailableForGameBS
	SCRIPT_TIMER failSafeClearVehicleDelay[MAX_SAVED_VEHS_IN_HANGAR]
	SCRIPT_TIMER st_HostRequestTimer
ENDSTRUCT

ENUM HANGAR_LAPTOP_SCREEN_STATE
	LAPTOP_SCREEN_STATE_INIT,
	LAPTOP_SCREEN_STATE_LINK_RT,
	LAPTOP_SCREEN_STATE_UPDATE_SCALEFORM
ENDENUM

STRUCT HANGAR_LAPTOP_SCREEN_STRUCT
	HANGAR_LAPTOP_SCREEN_STATE eLaptopState
	INT iRenderTargetID = -1
ENDSTRUCT

STRUCT HANGAR_DATA
	INT iBS
	INT iBS2
	HANGAR_ID eID

	INT iScriptInstance
	INT iInvitingPlayer
	INT iHangarSaveSlot
	INT iCurrentProductSlot = 0
	
	BOOL bScriptRelaunched
	BOOL bApplyUpgrade = FALSE
	BOOL bWarpDuringUpgrade = FALSE
	BOOL bAddProduct
	BOOL bRemoveProduct
	
	SCRIPT_TIMER stPinInMemTimer
	SCRIPT_TIMER tHelpTimer
	
	INT iHelpTextID = 0

	HANGAR_UPGRADE_STATE eUpgradeState
	INTERIOR_INSTANCE_INDEX HangarInteriorID
	SCRIPT_TIMER upgradeTimer
	HANGAR_UPGRADE_ID eUpgradeID
	
	VEHICLE_MANAGE_DATA sVehManageData
	
	INT iHangarPickupFlags[MAX_NUM_HANGAR_WEAPON_PICKUPS]
	
	VECTOR vGarageExitMin
	VECTOR vGarageExitMax
	
	PRODUCT_DATA sProductData
	HANGAR_SCRIPT_STATE eState
	SIMPLE_INTERIORS eSimpleInteriorID
	SIMPLE_INTERIOR_DETAILS interiorDetails
	SCRIPT_TIMER sCarModScriptRunTimer
	THREADID CarModThread
	STRING sChildOfChildScript
	SIMPLE_INTERIOR_ENTRY_ANIM_DATA garageExitAnim
	HANGAR_LAPTOP_SCREEN_STRUCT hangarLaptopData
	
	PLAYER_INDEX pOwner
	
	HANGAR_CUTSCENE_STATE eCutsceneState
	SIMPLE_CUTSCENE setupCutscene
	SCRIPT_TIMER timeFailSafe
	SCRIPT_TIMER tIdleFallback
	SCRIPT_TIMER tRefreshTimer
	BOOL bSceneTimeout = FALSE
	BOOL bIdleTimeout = FALSE
	FLOAT fMPTVVolumeBeforeCutscene
	BOOL bMPTVVolumeHasBeenModified
	
	SCRIPT_TIMER stOwnerNotOKTimer	//Timer to check if the owner is not ok before continuing
	
	INT iSpawnActivityStage, iSpawnActivitySceneID
	STRING strSpawnActivityDict
	SCRIPT_TIMER timerSpawnActivity
	
	BOOL bShouldPlayIntroCut
	BOOL bInitLocalPlayerForIntroMocap
	PED_INDEX scenePed
	PED_INDEX sceneDeadPed
	BOOL bHasCutsceneLoad = FALSE
	
	VECTOR vSceneSpawnPosition
	FLOAT fSceneSpawnHeading
	
	MP_TV_LOCAL_DATA_STRUCT MPTVLocal
	ACTIVITY_SIMPLE_SEAT_STRUCT activityTVSeatStruct
	
	BOOL bDrivingOut = FALSE
	
	OBJECT_INDEX objChristmasTree
ENDSTRUCT
HANGAR_DATA thisHangar

//Walk out server bitset const
CONST_INT HANGAR_WLK_OUT_SERVER_RUN_PLAYER_CLEAN_UP_CHECKS		0
//Walk out player bitset const
CONST_INT HANGAR_WLK_OUT_PLAYER_IM_OUT_OF_THE_WAY				0
//Walk out local bitset const
CONST_INT HANGAR_WLK_OUT_LOCAL_SET_UP_WALK_OUT					0

CONST_INT HANGAR_WALK_OUT_SAFETY_TIME 							7000
CONST_FLOAT HANGAR_SAFE_DISTANCE 								15.0

BedStructData bedStructLeft
BedStructData bedStructRight

//For now just let them go in from one side
//BedStructData bedStructMechanicLeft
BedStructData bedStructMechanicRight


MP_PROP_LOCAL_CONTROL serverLocalActControl
MP_RADIO_LOCAL_DATA_STRUCT MPRadioLocal

BOOL bGetOutOfWayFlagClearedThisFrame

INT iHangarInteriorHash
INT iServerGetOutOfWayPartCheck

INT iDoorCloseSlowLoop

MP_CCTV_LOCAL_DATA_STRUCT MPCCTVLocal

SCRIPT_TIMER failSafeClearVehicleDelay[MAX_SAVED_VEHS_IN_HANGAR]
SCRIPT_TIMER st_ServerWalkOutTimeout
SCRIPT_TIMER getOutOfWayTimer
SCRIPT_TIMER st_HostRequestTimer
SCRIPT_TIMER st_SyncBDTimer
SCRIPT_TIMER sMoveOutOfWayTimer
SCRIPT_TIMER sExitDelayTimer

VECTOR vServerWalkOutLocation

BOOL bStartCarModScript

ACTIVITY_SEAT_STRUCT activitySeatStruct

VEHICLE_INDEX currentVeh
VEHICLE_INDEX lastVeh

BLIP_INDEX bComputerBlip
BLIP_INDEX bStorageBlip

SCRIPT_TIMER st_ExitingVehicleTimer

#IF IS_DEBUG_BUILD
STRUCT HANGAR_DEBUG_DATA
	STRING sWidgetName
	
	TEXT_LABEL_63 tlWidgetName
	
	BOOL bSpawnProduct
	BOOL bSpawnMedicalLarge
	BOOL bSpawnMedicalMedium
	BOOL bSpawnTobaccoLarge
	BOOL bSpawnTobaccoMedium
	BOOL bSpawnAntiquesLarge
	BOOL bSpawnAntiquesMedium
	BOOL bSpawnNartoicsLarge
	BOOL bSpawnNartoicsMedium
	BOOL bSpawnJewelleryLarge
	BOOL bSpawnJewelleryMedium
	BOOL bSpawnAnimalMaterialsLarge
	BOOL bSpawnAnimalMaterialsMedium
	BOOL bSpawnCounterfeitGoodsLarge
	BOOL bSpawnCounterfeitGoodsMedium
	BOOL bSpawnChemicalsLarge
	BOOL bSpawnChemicalsMedium
	BOOL bAddProductToHangar
	BOOL bRemoveProductFromHangarOfType
	BOOL bRemoveAllProductFromHangar
	BOOL bFadeProductFlag
	BOOL bProductVisualDebug
	BOOL bSendDeliveredEventSingle
	BOOL bSendDeliveredEventMulti
	BOOL bSpawnTutorialProducts
	BOOL bClearTutorialProducts
	BOOL bDropoff_LSIA_1 = TRUE
	BOOL bDropoff_LSIA_A17
	BOOL bDropoff_Zancudo_A2
	BOOL bDropoff_Zancudo_3497
	BOOL bDropoff_Zancudo_3499
	BOOL bGoodsTypeMedical
	BOOL bGoodsTypeTobacco
	BOOL bGoodsTypeAntiques
	BOOL bGoodsTypeNartoics
	BOOL bGoodsTypeJewellery
	BOOL bGoodsTypeAnimalMaterial
	BOOL bGoodsTypeCounterfeitGoods
	BOOL bGoodsTypeChemicals
	
	VECTOR vCrateCoords
	VECTOR vCrateRotation
	
	INT iTutorialProductAmountToAdd = 1
	INT iProductAmountToAdd = 1
	INT iProductStatsSlot
	INT iProductStatsSlot2
	INT iProductGlobalSlot
	INT iProductServerBDSlot
	INT iProductSlotNextAvailable
	INT iProductStatsData[ciMAX_HANGAR_PRODUCT_UNITS]
	
	OBJECT_INDEX oProductObj
	
	INT iCreateAircraftModelID
	INT iCreateAircraftDisplaySlot
	INT iCreateAircraftSlot
	INT iCreateAircraftTransaction
	BOOL bCreateTestAircraft
	BOOL bCreateInStorage
	BOOL bClearAircraft
	VEHICLE_INDEX vehTemp
	
	CONTRABAND_TRANSACTION_STATE eTransactionState = CONTRABAND_TRANSACTION_STATE_DEFAULT
	BOOL bTransactionAddedMission 	= FALSE
	INT iTotalOfTypeToRemove		= -1

	BOOL bStartSpawnActivity
	
	BOOL bStartIntroCutscene
	BOOL bClearHaveSeenIntroCutscene
ENDSTRUCT
HANGAR_DEBUG_DATA debugData
#ENDIF

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ PROCEDURES  ╞═══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

// thisHanger iBS
CONST_INT BS_HANGAR_DATA_UPDATE_HANGAR_PRODUCT		0
CONST_INT BS_HANGAR_DATA_FADE_HANGAR_PRODUCT		1

FUNC BOOL IS_HANGAR_STATE(HANGAR_SCRIPT_STATE eState)
	RETURN thisHangar.eState = eState
ENDFUNC

FUNC STRING GET_HANGAR_SERVER_STATE_NAME(HANGAR_SERVER_STATE eState)
	SWITCH eState
		CASE HANGAR_SERVER_STATE_LOADING	RETURN "HANGAR_SERVER_STATE_LOADING"
		CASE HANGAR_SERVER_STATE_IDLE		RETURN "HANGAR_SERVER_STATE_IDLE"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC STRING GET_HANGAR_STATE_NAME(HANGAR_SCRIPT_STATE eState) 
	SWITCH eState
		CASE HANGAR_STATE_LOADING					RETURN "HANGAR_STATE_LOADING"
		CASE HANGAR_STATE_IDLE						RETURN "HANGAR_STATE_IDLE"
		CASE HANGAR_STATE_PLAYING_INTRO_CUTSCENE	RETURN "HANGAR_STATE_PLAYING_INTRO_CUTSCENE"
		CASE HANGAR_STATE_PLAYING_SETUP_CUTSCENE	RETURN "HANGAR_STATE_PLAYING_SETUP_CUTSCENE"
		CASE HANGAR_STATE_LOADING_GARAGE_EXIT		RETURN "HANGAR_STATE_LOADING_GARAGE_EXIT"
		CASE HANGAR_STATE_PLAYING_GARAGE_EXIT		RETURN "HANGAR_STATE_PLAYING_GARAGE_EXIT"
		CASE HANGAR_STATE_FINISHING_GARAGE_EXIT		RETURN "HANGAR_STATE_FINISHING_GARAGE_EXIT"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC STRING GET_HANGAR_UPGRADE_STATE_NAME(HANGAR_UPGRADE_STATE eUpgradeState)
	SWITCH eUpgradeState
		CASE HANGAR_UPGRADE_STATE_FADE_OUT				RETURN "HANGAR_UPGRADE_STATE_FADE_OUT"
		CASE HANGAR_UPGRADE_STATE_WARP_PLAYER			RETURN "HANGAR_UPGRADE_STATE_WARP_PLAYER"
		CASE HANGAR_UPGRADE_STATE_ACTIVATE_ENTITY_SETS	RETURN "HANGAR_UPGRADE_STATE_ACTIVATE_ENTITY_SETS"
		CASE HANGAR_UPGRADE_STATE_REFRESH_INTERIOR		RETURN "HANGAR_UPGRADE_STATE_REFRESH_INTERIOR"
		CASE HANGAR_UPGRADE_STATE_REFRESH_LIGHTING		RETURN "HANGAR_UPGRADE_STATE_REFRESH_LIGHTING"
		CASE HANGAR_UPGRADE_STATE_WAIT_FOR_VEHICLES		RETURN "HANGAR_UPGRADE_STATE_WAIT_FOR_VEHICLES"
		CASE HANGAR_UPGRADE_STATE_FADE_IN				RETURN "HANGAR_UPGRADE_STATE_FADE_IN"
		CASE HANGAR_UPGRADE_STATE_IDLE					RETURN "HANGAR_UPGRADE_STATE_IDLE"
	ENDSWITCH

	RETURN ""
ENDFUNC

FUNC STRING GET_HANGAR_CUTSCENE_STATE_NAME(HANGAR_CUTSCENE_STATE eCutsceneState)
	SWITCH eCutsceneState
		CASE HANGAR_CUTSCENE_STATE_INITIALISE		RETURN "HANGAR_CUTSCENE_STATE_INITIALISE"
		CASE HANGAR_CUTSCENE_STATE_LOAD_INTERIOR	RETURN "HANGAR_CUTSCENE_STATE_LOAD_INTERIOR"
		CASE HANGAR_CUTSCENE_STATE_ASSET_REQUEST	RETURN "HANGAR_CUTSCENE_STATE_ASSET_REQUEST"
		CASE HANGAR_CUTSCENE_STATE_IDLING			RETURN "HANGAR_CUTSCENE_STATE_IDLING"
		CASE HANGAR_CUTSCENE_STATE_PLAY_INTERIOR	RETURN "HANGAR_CUTSCENE_STATE_PLAY_INTERIOR"
		CASE HANGAR_CUTSCENE_STATE_LOAD_EXTERIOR	RETURN "HANGAR_CUTSCENE_STATE_LOAD_EXTERIOR"
		CASE HANGAR_CUTSCENE_STATE_PLAY_EXTERIOR	RETURN "HANGAR_CUTSCENE_STATE_PLAY_EXTERIOR"
		CASE HANGAR_CUTSCENE_STATE_CLEANUP			RETURN "HANGAR_CUTSCENE_STATE_CLEANUP"
	ENDSWITCH

	RETURN ""
ENDFUNC

FUNC BOOL IS_HANGAR_SERVER_STATE(HANGAR_SERVER_STATE eState)
	RETURN serverBD.eState = eState
ENDFUNC

PROC SET_HANGAR_SERVER_STATE(HANGAR_SERVER_STATE eState)
	CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - SET_HANGAR_SERVER_STATE - New state: ", GET_HANGAR_SERVER_STATE_NAME(eState))
	serverBD.eState = eState
ENDPROC

PROC SET_HANGAR_STATE(HANGAR_SCRIPT_STATE eState)
	CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_HANGAR - SET_HANGAR_STATE - New state: ", GET_HANGAR_STATE_NAME(eState))
	thisHangar.eState = eState
ENDPROC

PROC SET_HANGAR_UPGRADE_STATE(HANGAR_UPGRADE_STATE eUpgradeState)
	CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_HANGAR - SET_HANGAR_UPGRADE_STATE - New state: ", GET_HANGAR_UPGRADE_STATE_NAME(eUpgradeState))
	thisHangar.eUpgradeState = eUpgradeState
ENDPROC

PROC SET_HANGAR_CUTSCENE_STATE(HANGAR_CUTSCENE_STATE eCutsceneState)
	CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_HANGAR - SET_HANGAR_CUTSCENE_STATE - New state: ", GET_HANGAR_CUTSCENE_STATE_NAME(eCutsceneState))
	thisHangar.eCutsceneState = eCutsceneState
ENDPROC

PROC CLEAN_UP_HANGAR_PERSONAL_CAR_MOD()
	PRINTLN("CLEAN_UP_HANGAR_PERSONAL_CAR_MOD (Hangar) - called")
	g_bCleanUpCarmodShop = TRUE
	g_sShopSettings.bShopScriptLaunched[ENUM_TO_INT(CARMOD_SHOP_PERSONALMOD)] = FALSE
	bStartCarModScript = FALSE
	RESET_NET_TIMER(thisHangar.sCarModScriptRunTimer)
	IF NOT IS_STRING_NULL_OR_EMPTY(thisHangar.sChildOfChildScript)
		SET_SCRIPT_AS_NO_LONGER_NEEDED(thisHangar.sChildOfChildScript)
	ENDIF
ENDPROC 

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════╡ HANGAR PRODUCT SPAWNING ╞══════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

FUNC BOOL IS_HANGAR_PRODUCT_SLOT_AVALIABLE(INT iProductSlot)
	RETURN (thisHangar.sProductData.oProductObject[iProductSlot] = NULL)
ENDFUNC

PROC SET_HANGAR_PRODUCT_UPDATE_FLAGS(BOOL bSet)
	IF bSet
		IF NOT IS_BIT_SET(thisHangar.iBS, BS_HANGAR_DATA_UPDATE_HANGAR_PRODUCT)
			SET_BIT(thisHangar.iBS, BS_HANGAR_DATA_UPDATE_HANGAR_PRODUCT)
		ENDIF
	ELSE
		IF IS_BIT_SET(thisHangar.iBS, BS_HANGAR_DATA_UPDATE_HANGAR_PRODUCT)
			CLEAR_BIT(thisHangar.iBS, BS_HANGAR_DATA_UPDATE_HANGAR_PRODUCT)
		ENDIF
	ENDIF
ENDPROC

PROC SET_HANGAR_PRODUCT_FADE_FLAGS(BOOL bSet)
	IF bSet
		IF NOT IS_BIT_SET(thisHangar.iBS, BS_HANGAR_DATA_FADE_HANGAR_PRODUCT)
			SET_BIT(thisHangar.iBS, BS_HANGAR_DATA_FADE_HANGAR_PRODUCT)
		ENDIF
	ELSE
		IF IS_BIT_SET(thisHangar.iBS, BS_HANGAR_DATA_FADE_HANGAR_PRODUCT)
			CLEAR_BIT(thisHangar.iBS, BS_HANGAR_DATA_FADE_HANGAR_PRODUCT)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Flags to be called after product creation
PROC SET_HANGAR_PRODUCT_SPAWNING_FLAGS()
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		serverBD.bUpdateHangarProductData = FALSE
	ENDIF
	
	// Clear update flags
	SET_HANGAR_PRODUCT_UPDATE_FLAGS(FALSE)
	
	// Init spawning complete - from here on fade product in & out
	SET_HANGAR_PRODUCT_FADE_FLAGS(TRUE)
	g_bHangarProductSpawning = FALSE
	
ENDPROC

#IF IS_DEBUG_BUILD
PROC DEBUG_PRINT_PRODUCT_DATA()
	INT i
	REPEAT ciMAX_HANGAR_PRODUCT_UNITS i
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			PRINTLN("[AM_MP_HANGAR] DEBUG_PRINT_PRODUCT_DATA - Product Slot: ", i, " Packed Stats Product: 	", GET_PACKED_STAT_INT(GET_STAT_ENUM_FOR_HANGAR_PRODUCT_SLOT(i)), " Packed Stats Product Model:	", DEBUG_GET_PRODUCT_MODEL_NAME(GET_HANGAR_PRODUCT_MODEL_FROM_INDEX(GET_PACKED_STAT_INT(GET_STAT_ENUM_FOR_HANGAR_PRODUCT_SLOT(i)))))
			PRINTLN("[AM_MP_HANGAR] DEBUG_PRINT_PRODUCT_DATA - Product Slot: ", i, " Global Product: 		", g_iHangarProductData[i],  " Global Product Model:		", DEBUG_GET_PRODUCT_MODEL_NAME(GET_HANGAR_PRODUCT_MODEL_FROM_INDEX(g_iHangarProductData[i])))
		ENDIF
		PRINTLN("[AM_MP_HANGAR] DEBUG_PRINT_PRODUCT_DATA - Product Slot: ", i, " Server BD Product: 		", serverBD.iProductData[i], " Server BD Product Model:		", DEBUG_GET_PRODUCT_MODEL_NAME(GET_HANGAR_PRODUCT_MODEL_FROM_INDEX(serverBD.iProductData[i])))
	ENDREPEAT
ENDPROC
#ENDIF

PROC ITERATE_HANGAR_PRODUCT_SLOT()
	
	IF thisHangar.iCurrentProductSlot >= (ciMAX_HANGAR_PRODUCT_UNITS-1)
		SET_HANGAR_PRODUCT_SPAWNING_FLAGS()
		thisHangar.iCurrentProductSlot = 0
		
		#IF IS_DEBUG_BUILD
		DEBUG_PRINT_PRODUCT_DATA()
		#ENDIF
	ELSE
		thisHangar.iCurrentProductSlot++
	ENDIF
	
ENDPROC

FUNC BOOL IS_ANY_PLAYER_BLOCKING_HANGAR_PRODUCT_COORDS(INT iProductSlot)
	VECTOR vProductCoords = GET_HANGAR_PRODUCT_COORDS(iProductSlot)
	
	INT iParticipant
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
		    PLAYER_INDEX playerID = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
		    IF IS_NET_PLAYER_OK(playerID)
				IF ARE_VECTORS_ALMOST_EQUAL(GET_PLAYER_COORDS(playerID), vProductCoords, 1.25)
					RETURN TRUE
				ENDIF
			ENDIF
	    ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

FUNC BOOL CLEANUP_HANGAR_PRODUCT(INT iProductSlot)
	
	OBJECT_INDEX oProductObj
	oProductObj = thisHangar.sProductData.oProductObject[iProductSlot]
	
	IF DOES_ENTITY_EXIST(oProductObj)
		DELETE_OBJECT(oProductObj)
		
		thisHangar.sProductData.oProductObject[iProductSlot] = NULL
		
		#IF IS_DEBUG_BUILD
		PRINTLN("[AM_MP_HANGAR] CLEANUP_HANGAR_PRODUCT - iProductSlot: ", iProductSlot)
		#ENDIF
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CLEANUP_ALL_HANGAR_PRODUCT()
	
	INT iProductSlot
	REPEAT ciMAX_HANGAR_PRODUCT_UNITS iProductSlot
		CLEANUP_HANGAR_PRODUCT(iProductSlot)
	ENDREPEAT
	
ENDPROC

FUNC BOOL CREATE_HANGAR_PRODUCT(INT iProductSlot, BOOL bProductFadeIn, BOOL bTutorialProducts = FALSE)
	
	MODEL_NAMES eProductModel	= GET_HANGAR_PRODUCT_MODEL_FROM_INDEX(serverBD.iProductData[iProductSlot])
	
	IF bTutorialProducts
		eProductModel = GET_HANGAR_PRODUCT_MODEL_FROM_INDEX(GET_RANDOM_INT_IN_RANGE(1, 17))
	ENDIF
	
	VECTOR vProductCoords 		= GET_HANGAR_PRODUCT_COORDS(iProductSlot)
	VECTOR vProductRotation 	= GET_HANGAR_PRODUCT_ROTATION(iProductSlot, eProductModel)
	
	IF IS_MODEL_VALID(eProductModel) AND (eProductModel != DUMMY_MODEL_FOR_SCRIPT)
		OBJECT_INDEX oProductObj = CREATE_OBJECT(eProductModel, vProductCoords, FALSE, FALSE)
		
		IF DOES_ENTITY_EXIST(oProductObj)
			SET_ENTITY_ROTATION(oProductObj, vProductRotation)
			FREEZE_ENTITY_POSITION(oProductObj, TRUE)
			
			thisHangar.sProductData.oProductObject[iProductSlot] = oProductObj
			
			IF bProductFadeIn
				SET_ENTITY_ALPHA(oProductObj, 0, FALSE)
			ELSE
				IF IS_MODEL_VALID(eProductModel)
					SET_MODEL_AS_NO_LONGER_NEEDED(eProductModel)
				ENDIF
			ENDIF
			#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_PROPERTY, "[AM_MP_HANGAR] - CREATE_HANGAR_PRODUCT - Creating Product in Slot: ", iProductSlot, " Model: ", DEBUG_GET_PRODUCT_MODEL_NAME(GET_HANGAR_PRODUCT_MODEL_FROM_INDEX(serverBD.iProductData[iProductSlot])))
			#ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_HANGAR_PRODUCT_FADE_TIMER(INT iProductSlot)
	IF thisHangar.sProductData.iSetProductFadeTimer[iProductSlot] = 0
		thisHangar.sProductData.tdProductFadeTimer[iProductSlot] = GET_TIME_OFFSET(GET_NETWORK_TIME(), ciMAX_HANGAR_PRODUCT_FADE_TIME)
		thisHangar.sProductData.iSetProductFadeTimer[iProductSlot] = 1
	ENDIF
ENDPROC

PROC UPDATE_HANGAR_PRODUCT_FADE(OBJECT_INDEX &oProductObject, INT iProductSlot, BOOL bFadeIn)

	IF DOES_ENTITY_EXIST(oProductObject)
		INT iTimeDifference = ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), thisHangar.sProductData.tdProductFadeTimer[iProductSlot]))
		INT iAlpha = ROUND((TO_FLOAT(iTimeDifference) / ciMAX_HANGAR_PRODUCT_FADE_TIME) * 255)
		
		IF bFadeIn
			iAlpha = (255 - (ROUND((TO_FLOAT(iTimeDifference) / ciMAX_HANGAR_PRODUCT_FADE_TIME) * 255)))
		ENDIF
		
		SET_ENTITY_ALPHA(oProductObject, iAlpha, FALSE)
	ENDIF
	
ENDPROC

PROC FINALISE_HANGAR_PRODUCT_FADE(OBJECT_INDEX &oProductObject, INT iProductSlot, BOOL bFadeIn)
	
	IF DOES_ENTITY_EXIST(oProductObject)
		IF bFadeIn
			SET_ENTITY_ALPHA(oProductObject, 255, FALSE)
			IF IS_MODEL_VALID(GET_ENTITY_MODEL(oProductObject))
				SET_MODEL_AS_NO_LONGER_NEEDED(GET_ENTITY_MODEL(oProductObject))
			ENDIF
			
			thisHangar.sProductData.iProductFadeComplete[iProductSlot] = 1
		ELSE
			SET_ENTITY_ALPHA(oProductObject, 0, FALSE)
			IF NETWORK_HAS_CONTROL_OF_ENTITY(oProductObject)
				CLEANUP_HANGAR_PRODUCT(iProductSlot)
			ENDIF
			
			thisHangar.sProductData.iProductFadeComplete[iProductSlot] = 0
		ENDIF
	ENDIF
	
ENDPROC

PROC PERFORM_HANGAR_PRODUCT_FADE(INT iProductSlot, BOOL bFadeIn)
	SET_HANGAR_PRODUCT_FADE_TIMER(iProductSlot)
	
	IF IS_TIME_LESS_THAN(GET_NETWORK_TIME(), thisHangar.sProductData.tdProductFadeTimer[iProductSlot])
		OBJECT_INDEX oProductObject = thisHangar.sProductData.oProductObject[iProductSlot]
		UPDATE_HANGAR_PRODUCT_FADE(oProductObject, iProductSlot, bFadeIn)
	ELSE
		OBJECT_INDEX oProductObject = thisHangar.sProductData.oProductObject[iProductSlot]
		FINALISE_HANGAR_PRODUCT_FADE(oProductObject, iProductSlot, bFadeIn)
		thisHangar.sProductData.iSetProductFadeTimer[iProductSlot] = 0
	ENDIF
	
ENDPROC

FUNC BOOL DOES_PRODUCT_SLOT_NEED_UPDATING(INT iProductSlot, BOOL &bProductAdded)
	
	MODEL_NAMES eServerProduct  = GET_HANGAR_PRODUCT_MODEL_FROM_INDEX(serverBD.iProductData[iProductSlot])
	MODEL_NAMES eCurrentProduct = DUMMY_MODEL_FOR_SCRIPT
	
	IF DOES_ENTITY_EXIST(thisHangar.sProductData.oProductObject[iProductSlot])
		eCurrentProduct = GET_ENTITY_MODEL(thisHangar.sProductData.oProductObject[iProductSlot])
	ENDIF
	
	#IF IS_DEBUG_BUILD
	//PRINTLN("[AM_MP_HANGAR] DOES_PRODUCT_SLOT_NEED_UPDATING - Slot: ", iProductSlot, " eServerProduct: ", DEBUG_GET_PRODUCT_MODEL_NAME(eServerProduct))
	//PRINTLN("[AM_MP_HANGAR] DOES_PRODUCT_SLOT_NEED_UPDATING - Slot: ", iProductSlot, " eCurrentProduct: ", DEBUG_GET_PRODUCT_MODEL_NAME(eCurrentProduct))
	#ENDIF
	
	IF (eCurrentProduct != eServerProduct)
		// Product has been added - continue to spawn
		IF eCurrentProduct = DUMMY_MODEL_FOR_SCRIPT
			PRINTLN("[AM_MP_HANGAR] DOES_PRODUCT_SLOT_NEED_UPDATING - [0] Slot: ", iProductSlot, " bProductAdded = TRUE")
			bProductAdded = TRUE
			RETURN TRUE
		
		// Product has been removed - continue to clear
		ELIF eServerProduct = DUMMY_MODEL_FOR_SCRIPT
			PRINTLN("[AM_MP_HANGAR] DOES_PRODUCT_SLOT_NEED_UPDATING - [0] Slot: ", iProductSlot, " bProductAdded = FALSE")
			bProductAdded = FALSE
			RETURN TRUE
		ENDIF
	ELSE
		// Is product currently fading?
		IF thisHangar.bAddProduct
			PRINTLN("[AM_MP_HANGAR] DOES_PRODUCT_SLOT_NEED_UPDATING - [1] Slot: ", iProductSlot, " bProductAdded = TRUE")
			bProductAdded = TRUE
			RETURN TRUE
		
		ELIF thisHangar.bRemoveProduct
			PRINTLN("[AM_MP_HANGAR] DOES_PRODUCT_SLOT_NEED_UPDATING - [1] Slot: ", iProductSlot, " bProductAdded = FALSE")
			bProductAdded = FALSE
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SPAWN_HANGAR_PRODUCT()
	
	BOOL bAddProduct
	BOOL bFadeProduct = IS_BIT_SET(thisHangar.iBS, BS_HANGAR_DATA_FADE_HANGAR_PRODUCT)
	
	IF NOT IS_ANY_PLAYER_BLOCKING_HANGAR_PRODUCT_COORDS(thisHangar.iCurrentProductSlot)
		IF DOES_PRODUCT_SLOT_NEED_UPDATING(thisHangar.iCurrentProductSlot, bAddProduct)
			
			IF bAddProduct
				thisHangar.bAddProduct = TRUE
				IF IS_HANGAR_PRODUCT_SLOT_AVALIABLE(thisHangar.iCurrentProductSlot)
					CREATE_HANGAR_PRODUCT(thisHangar.iCurrentProductSlot, bFadeProduct)
					
					IF NOT bFadeProduct
						thisHangar.sProductData.iProductFadeComplete[thisHangar.iCurrentProductSlot] = 1
					ENDIF
				ENDIF
				
				IF bFadeProduct AND NOT IS_HANGAR_PRODUCT_SLOT_AVALIABLE(thisHangar.iCurrentProductSlot)
					PERFORM_HANGAR_PRODUCT_FADE(thisHangar.iCurrentProductSlot, TRUE)
					IF thisHangar.sProductData.iProductFadeComplete[thisHangar.iCurrentProductSlot] = 1
						thisHangar.bAddProduct = FALSE
						ITERATE_HANGAR_PRODUCT_SLOT()
					ENDIF
				ELSE
					thisHangar.bAddProduct = FALSE
					ITERATE_HANGAR_PRODUCT_SLOT()
				ENDIF
			ELSE
				thisHangar.bRemoveProduct = TRUE
				PERFORM_HANGAR_PRODUCT_FADE(thisHangar.iCurrentProductSlot, FALSE)
				IF thisHangar.sProductData.iProductFadeComplete[thisHangar.iCurrentProductSlot] = 0
					thisHangar.bRemoveProduct = FALSE
					ITERATE_HANGAR_PRODUCT_SLOT()
				ENDIF
			ENDIF
			
		ELSE
			ITERATE_HANGAR_PRODUCT_SLOT()
		ENDIF
	ENDIF
	
ENDPROC

PROC SPAWN_HANGAR_TUTORIAL_PRODUCT(INT iProductsToSpawn, BOOL bFadeIn = FALSE)
	
	INT i
	REPEAT iProductsToSpawn i
		IF IS_HANGAR_PRODUCT_SLOT_AVALIABLE(i)
			CREATE_HANGAR_PRODUCT(i, bFadeIn, TRUE)
		ENDIF
	ENDREPEAT
	
ENDPROC

FUNC BOOL MAINTAIN_LAUNCHING_HANGER_CARMOD_SCRIPT()
	

	IF thisHangar.pOwner != INVALID_PLAYER_INDEX()
		//Make sure the car mod script is not started too early after being killed
		//Make sure the car mod script cleanup flag is false
		IF g_bCleanUpCarmodShop = TRUE
			IF NOT HAS_NET_TIMER_STARTED(thisHangar.sCarModScriptRunTimer)
				START_NET_TIMER(thisHangar.sCarModScriptRunTimer)
			ELSE
				IF HAS_NET_TIMER_EXPIRED(thisHangar.sCarModScriptRunTimer, 8000)
					g_bCleanUpCarmodShop = FALSE
					RESET_NET_TIMER(thisHangar.sCarModScriptRunTimer)
					#IF IS_DEBUG_BUILD
					PRINTLN("MAINTAIN_LAUNCHING_HANGER_CARMOD_SCRIPT - Setting g_bCleanUpCarmodShop To FALSE")
					#ENDIF
				ENDIF	
			ENDIF
		ENDIF
		
		IF NOT bStartCarModScript
			thisHangar.sChildOfChildScript = "hangar_carmod"
			REQUEST_SCRIPT(thisHangar.sChildOfChildScript)
			IF HAS_SCRIPT_LOADED(thisHangar.sChildOfChildScript)
			AND NOT IS_THREAD_ACTIVE(thisHangar.CarModThread)
			AND !g_bCleanUpCarmodShop
				
				g_iCarModInstance = thisHangar.iScriptInstance
				
				IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(thisHangar.sChildOfChildScript)) < 1
					IF NOT NETWORK_IS_SCRIPT_ACTIVE(thisHangar.sChildOfChildScript, g_iCarModInstance, TRUE)
						SHOP_LAUNCHER_STRUCT sShopLauncherData
						sShopLauncherData.bLinkedShop = FALSE
						sShopLauncherData.eShop = CARMOD_SHOP_PERSONALMOD
						sShopLauncherData.iNetInstanceID = g_iCarModInstance
						sShopLauncherData.ePersonalCarModVariation = PERSONAL_CAR_MOD_VARIATION_HANGAR
						
						g_iPersonalCarModVariation = ENUM_TO_INT(sShopLauncherData.ePersonalCarModVariation)
						thisHangar.CarModThread = START_NEW_SCRIPT_WITH_ARGS(thisHangar.sChildOfChildScript, sShopLauncherData, SIZE_OF(sShopLauncherData), CAR_MOD_SHOP_STACK_SIZE)
						SET_SCRIPT_AS_NO_LONGER_NEEDED(thisHangar.sChildOfChildScript)
						bStartCarModScript = TRUE
						g_sShopSettings.bShopScriptLaunched[ENUM_TO_INT(CARMOD_SHOP_PERSONALMOD)] = TRUE
						g_sShopSettings.bShopScriptLaunchedInMP[ENUM_TO_INT(CARMOD_SHOP_PERSONALMOD)] = NETWORK_IS_GAME_IN_PROGRESS()
						PRINTLN("MAINTAIN_LAUNCHING_HANGER_CARMOD_SCRIPT - TRUE sShopLauncherData.ePersonalCarModVariation: ", sShopLauncherData.ePersonalCarModVariation)
						RETURN TRUE
					ELSE
						#IF IS_DEBUG_BUILD
							PRINTLN("MAINTAIN_LAUNCHING_HANGER_CARMOD_SCRIPT: Unable to start shop script for hangar_carmod - last instance still active")
						#ENDIF					
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
					IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
						CLEAN_UP_HANGAR_PERSONAL_CAR_MOD()
						TERMINATE_THREAD(thisHangar.CarModThread)
					ENDIF
					#ENDIF
					
					PRINTLN("MAINTAIN_LAUNCHING_HANGER_CARMOD_SCRIPT - carmod is already running")
					RETURN TRUE 
				ENDIF
			ELSE
				PRINTLN("MAINTAIN_LAUNCHING_HANGER_CARMOD_SCRIPT - is car mod thread active: " , IS_THREAD_ACTIVE(thisHangar.CarModThread))
				PRINTLN("MAINTAIN_LAUNCHING_HANGER_CARMOD_SCRIPT - is car mod loaded: " , HAS_SCRIPT_LOADED(thisHangar.sChildOfChildScript))
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
				CLEAN_UP_HANGAR_PERSONAL_CAR_MOD()
				TERMINATE_THREAD(thisHangar.CarModThread)
			ENDIF
			#ENDIF
			
			RETURN TRUE
		ENDIF
	ELSE
		PRINTLN("MAINTAIN_LAUNCHING_HANGER_CARMOD_SCRIPT - thisHangar.pOwner is invalid")
	ENDIF
	
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(thisHangar.sChildOfChildScript)) > 0
	AND IS_THREAD_ACTIVE(thisHangar.CarModThread)
		#IF IS_DEBUG_BUILD
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
				CLEAN_UP_HANGAR_PERSONAL_CAR_MOD()
				TERMINATE_THREAD(thisHangar.CarModThread)
			ENDIF
		#ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTLN("MAINTAIN_LAUNCHING_HANGER_CARMOD_SCRIPT - return false")
	#ENDIF
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_WANTED_LEVEL()
	IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - MAINTAIN_WANTED_LEVEL - Resetting wanted level")
		SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
	ENDIF
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════╡ HANGAR PRODUCT DATA ╞═════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

/// PURPOSE: Populates owners global data. 
///    Allows the global data to be used when adding product mid mission.
PROC POPULATE_SERVER_BD_HANGAR_PRODUCT_DATA_OWNER()
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		
		IF thisHangar.pOwner = PLAYER_ID()
			INT iProductSlot
			
			// Clear any existing data
			REPEAT ciMAX_HANGAR_PRODUCT_UNITS iProductSlot
				g_iHangarProductData[iProductSlot] = 0
			ENDREPEAT
			
			// Populate global data
			REPEAT ciMAX_HANGAR_PRODUCT_UNITS iProductSlot
				g_iHangarProductData[iProductSlot] = GET_PACKED_STAT_INT(GET_STAT_ENUM_FOR_HANGAR_PRODUCT_SLOT(iProductSlot))
			ENDREPEAT
		ENDIF
		
	ENDIF
ENDPROC

PROC POPULATE_SERVER_BD_HANGAR_PRODUCT_DATA()
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		
		// Fill server BD with populated product data
		INT iProductSlot
		REPEAT ciMAX_HANGAR_PRODUCT_UNITS iProductSlot
			serverBD.iProductData[iProductSlot] = g_iHangarProductData[iProductSlot]
		ENDREPEAT
		
	ENDIF
ENDPROC

/// PURPOSE: Populate server BD product array
PROC INITIALISE_HANGAR_PRODUCT_HOST()
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF thisHangar.pOwner = PLAYER_ID()
			// Read stats directly - No need for events
			POPULATE_SERVER_BD_HANGAR_PRODUCT_DATA_OWNER()
			g_bHangarProductDataPopulated = TRUE
		ELSE
			// Request hangar product data via events
			IF IS_NET_PLAYER_OK(thisHangar.pOwner, FALSE)
				BROADCAST_REQUEST_HANGAR_PRODUCT_DATA(thisHangar.pOwner)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL IS_HANGAR_PRODUCT_VALIDATION_NEEDED()
	
	// Dont validate during cutscene
	IF thisHangar.setupCutscene.bPlaying
		RETURN FALSE
	ENDIF
	
	BOOL bAddProduct
	INT iProductSlot
	REPEAT ciMAX_HANGAR_PRODUCT_UNITS iProductSlot
		IF DOES_PRODUCT_SLOT_NEED_UPDATING(iProductSlot, bAddProduct)
			RETURN TRUE
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

PROC DOES_HANGAR_PRODUCT_DATA_NEED_UPDATING_LOCAL()
	
	// Tell local player to update data
	IF serverBD.bUpdateHangarProductData
	OR IS_HANGAR_PRODUCT_VALIDATION_NEEDED()
		SET_HANGAR_PRODUCT_UPDATE_FLAGS(TRUE)
	ENDIF
	
ENDPROC

FUNC BOOL DOES_HANGAR_PRODUCT_DATA_NEED_UPDATING()
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF g_bHangarProductDataPopulated
			POPULATE_SERVER_BD_HANGAR_PRODUCT_DATA()
			
			serverBD.bUpdateHangarProductData	= TRUE
			g_bHangarProductDataPopulated 		= FALSE
		ENDIF
	ENDIF
	
	DOES_HANGAR_PRODUCT_DATA_NEED_UPDATING_LOCAL()
	RETURN (IS_BIT_SET(thisHangar.iBS, BS_HANGAR_DATA_UPDATE_HANGAR_PRODUCT))
ENDFUNC

PROC MAINTAIN_HANGAR_PRODUCT_DATA()
	
	IF DOES_HANGAR_PRODUCT_DATA_NEED_UPDATING()
		SPAWN_HANGAR_PRODUCT()
	ENDIF
	
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════╡ VEHICLES IN HANGAR ╞════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

FUNC BOOL IS_IT_SAFE_TO_CREATE_HANGAR_VEHICLE()
	RETURN NOT IS_BIT_SET(serverBD.sGetOutOfTheWay.iCreationBitSet, HANGAR_WLK_OUT_SERVER_RUN_PLAYER_CLEAN_UP_CHECKS)
ENDFUNC

PROC SERVER_CHECK_ANY_PLAYERS_ARE_IN_THE_WAY_OF_HANGAR_VEHICLES(VECTOR vCreationPos)
	PLAYER_INDEX playerID
	INT iLoop
	VECTOR vPlayerPos
	FLOAT fdistance
	FLOAT fDistanceToCheck = HANGAR_SAFE_DISTANCE
	
	IF NOT bGetOutOfWayFlagClearedThisFrame
		serverBD.sGetOutOfTheWay.iCreationBitSet = 0
		serverBD.sGetOutOfTheWay.iPlayerBitSet = 0
		bGetOutOfWayFlagClearedThisFrame = TRUE
	ENDIF
	
	IF ARE_VECTORS_ALMOST_EQUAL(vServerWalkOutLocation, vCreationPos, 0.1)
		IF NOT HAS_NET_TIMER_STARTED(st_ServerWalkOutTimeout)
			START_NET_TIMER(st_ServerWalkOutTimeout, TRUE)
		ELSE
			IF HAS_NET_TIMER_EXPIRED(st_ServerWalkOutTimeout, 15000, TRUE)
				EXIT
			ENDIF
		ENDIF
	ELSE
		RESET_NET_TIMER(st_ServerWalkOutTimeout)
	ENDIF
	
	vServerWalkOutLocation = vCreationPos
	
	REPEAT NUM_NETWORK_PLAYERS iLoop
		playerID = INT_TO_PLAYERINDEX(iLoop)
		
		IF IS_NET_PLAYER_OK(playerID, FALSE)
			IF NETWORK_IS_PLAYER_A_PARTICIPANT(playerID)
				IF NOT IS_BIT_SET(serverBD.sGetOutOfTheWay.iPlayerBitSet, iLoop)
					IF IS_NET_PLAYER_OK(playerID, TRUE, TRUE)
						IF NOT IS_THIS_PLAYER_ACTIVE_IN_CORONA(playerID)
						AND NOT IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(playerID))
						AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(playerID)
							vPlayerPos = GET_ENTITY_COORDS(GET_PLAYER_PED(playerID))
							fdistance = GET_DISTANCE_BETWEEN_COORDS(vPlayerPos, vCreationPos, FALSE)
							
							IF fDistance < fDistanceToCheck
							AND vPlayerPos.Z < -45.0
								SET_BIT(serverBD.sGetOutOfTheWay.iPlayerBitSet, iLoop)
								SET_BIT(serverBD.sGetOutOfTheWay.iCreationBitSet, HANGAR_WLK_OUT_SERVER_RUN_PLAYER_CLEAN_UP_CHECKS)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC GET_HANGAR_VEH_CREATION_POSITION(MP_PROP_OFFSET_STRUCT &creationLocation, INT iPosition, MODEL_NAMES eModel = DUMMY_MODEL_FOR_SCRIPT)
	SWITCH iPosition
		CASE 0
			creationLocation.vLoc = <<-1266.9, -3035.0, -48.0>>
			creationLocation.vRot = <<0.0,0.0, 10>>
		BREAK
		CASE 1
			creationLocation.vLoc = <<-1252.6, -3028.0, -48.0>>
			creationLocation.vRot = <<0.0,0.0,36>>
		BREAK
		CASE 2
			creationLocation.vLoc = <<-1258.6, -3032.0, -48.0>>
			creationLocation.vRot = <<0.0,0.0,26>>
		BREAK
		CASE 3
			creationLocation.vLoc = <<-1266.9, -3022.0, -48.0>>
			creationLocation.vRot = <<0.0,0.0,-8>>
		BREAK
		CASE 4
			creationLocation.vLoc = <<-1275.2, -3032.0, -48.0>>
			creationLocation.vRot = <<0.0,0.0,-20>>
		BREAK
		CASE 5
			creationLocation.vLoc = <<-1281.2, -3028.0, -48.0>>
			creationLocation.vRot = <<0.0,0.0,-34>>
		BREAK
		CASE 6
			creationLocation.vLoc = <<-1266.9, -3019.0, -48.0>>
			creationLocation.vRot = <<0.0,0.0,2>>
		BREAK
		CASE 7
			creationLocation.vLoc = <<-1252.6, -3011.0, -48.0>>
			creationLocation.vRot = <<0.0,0.0,20>>
		BREAK
		CASE 8
			creationLocation.vLoc = <<-1281.2, -3011.0, -48.0>>
			creationLocation.vRot = <<0.0,0.0,-22>>
		BREAK
		CASE 9
			creationLocation.vLoc = <<-1258.6, -3004.0, -48.0>>
			creationLocation.vRot = <<0.0,0.0,16>>
		BREAK
		CASE 10
			creationLocation.vLoc = <<-1266.9, -3002.0, -48.0>>
			creationLocation.vRot = <<0.0,0.0, 10>>
		BREAK
		CASE 11
			creationLocation.vLoc = <<-1277.4, -3004.0, -48.0>>
			creationLocation.vRot = <<0.0,0.0,-25>>
		BREAK
		CASE 12
			creationLocation.vLoc = <<-1252.6, -2991.0, -48.0>>
			creationLocation.vRot = <<0.0,0.0,26>>
		BREAK
		CASE 13
			IF eModel = VOLATOL
				creationLocation.vLoc = <<-1266.9, -2984.0, -48.0>>
				creationLocation.vRot = <<0.0,0.0,6>>
			ELSE
				creationLocation.vLoc = <<-1264.9, -2984.0, -48.0>>
				creationLocation.vRot = <<0.0,0.0,6>>
			ENDIF
		BREAK
		CASE 14
			creationLocation.vLoc = <<-1278.7, -2990.0, -48.0>>
			creationLocation.vRot = <<0.0,0.0,-40>>
		BREAK
		CASE 15
			creationLocation.vLoc = <<-1266.9, -2985.0, -48.0>>
			creationLocation.vRot = <<0.0,0.0, -6>>
		BREAK
		CASE 16
			creationLocation.vLoc = <<-1265.4, -2981.5, -48.0>>
			creationLocation.vRot = <<0.0,0.0, -2>>
		BREAK
		CASE 17
			creationLocation.vLoc = <<-1255.6, -2974.0, -48.0>>
			creationLocation.vRot = <<0.0,0.0,38>>
		BREAK
		CASE 18
			creationLocation.vLoc = <<-1279.2, -2974.0, -48.0>>
			creationLocation.vRot = <<0.0,0.0,-30>>
		BREAK
		CASE 19
			creationLocation.vLoc = <<-1272.478, -3005.846, -46.79>>
			creationLocation.vRot = <<0.0,0.0,17.375>>
		BREAK
	ENDSWITCH
	
ENDPROC

FUNC BOOL FADING_VEHICLES_BLOCK_CREATE(INT iDisplayIndex)

	IF IS_BIT_SET(thisHangar.sVehManageData.iManageVehCreateBS,MANAGE_VEH_CREATE_PENDING_FADE)
		PRINTLN("FADING_VEHICLES_BLOCK_CREATE: blocked by pending fade.")
		RETURN TRUE
	ENDIF

	INT iCreateConflicts = GET_HANGAR_SLOT_CONFLICTS(iDisplayIndex)
	INT i
	PRINTLN("FADING_VEHICLES_BLOCK_CREATE: conflicts are ", iCreateConflicts)
	REPEAT MAX_DISPLAYED_VEHS_IN_HANGAR i
		IF IS_BIT_SET(serverBD.iVehicleFadeBS, i)
		AND IS_BIT_SET(iCreateConflicts, i)
			PRINTLN("FADING_VEHICLES_BLOCK_CREATE: blocked by slot ", i)
			RETURN TRUE
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

FUNC BOOL CREATE_MODDED_CAR_FOR_HANGAR(VEHICLE_SETUP_STRUCT vehicleSetup, INT i,INT iSaveSlot, BOOL bHasEmblem)
	MP_PROP_OFFSET_STRUCT vehCreationLocation
	VEHICLE_INDEX tempVehicle
	
	//url:bugstar:3872025 - causes problems during cutscene when vehicles are set to create in this time
	IF NETWORK_IS_IN_MP_CUTSCENE()
		RETURN TRUE
	ENDIF
	
//	IF IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, PLAYER_BD_BS_WARPING_OUT_OF_GARAGE)
//		PRINTLN("CREATE_MODDED_CAR_FOR_GARAGE: bypassing as player is in process of leaving")
//		RETURN FALSE
//	ENDIF
	
	PRINTLN("CREATE_MODDED_CAR_FOR_GARAGE for model ", GET_MODEL_NAME_FOR_DEBUG(vehicleSetup.eModel), ", index ", i,", save slot ",iSaveSlot,".")
	
	GET_HANGAR_VEH_CREATION_POSITION(vehCreationLocation, i, vehicleSetup.eModel)
	
	
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.customVehicleNetIDs[i])
		IF IS_MODEL_IN_CDIMAGE(vehicleSetup.eModel)
			IF REQUEST_LOAD_MODEL(vehicleSetup.eModel)
				IF CAN_RESERVE_NETWORK_VEHICLES_FOR_THIS_SCRIPT(GET_NUM_RESERVED_MISSION_VEHICLES(FALSE,RESERVATION_LOCAL_ONLY)+ 1,false,true)
					IF NOT IS_BIT_SET(serverBD.iVehicleResBS,i)
						RESERVE_LOCAL_NETWORK_MISSION_VEHICLES(GET_NUM_RESERVED_MISSION_VEHICLES(FALSE,RESERVATION_LOCAL_ONLY)+ 1)
						PRINTLN("CREATE_MODDED_CAR_FOR_GARAGE: reserving ",GET_NUM_RESERVED_MISSION_VEHICLES(FALSE,RESERVATION_LOCAL_ONLY)+ 1 ," vehicles")
						serverBD.iReserved++
						SET_BIT(serverBD.iVehicleResBS,i)
					ENDIF
					
					SERVER_CHECK_ANY_PLAYERS_ARE_IN_THE_WAY_OF_HANGAR_VEHICLES(vehCreationLocation.vLoc)
					
					IF NOT IS_IT_SAFE_TO_CREATE_HANGAR_VEHICLE()
						IF NOT HAS_NET_TIMER_STARTED(sMoveOutOfWayTimer)
							START_NET_TIMER(sMoveOutOfWayTimer)
						ENDIF
					ENDIF
					
					IF HAS_NET_TIMER_STARTED(sMoveOutOfWayTimer)
						IF HAS_NET_TIMER_EXPIRED(sMoveOutOfWayTimer, 500)
							RESET_NET_TIMER(sMoveOutOfWayTimer)
						ELSE
							PRINTLN("CREATE_MODDED_CAR_FOR_GARAGE: waiting for players to be out of the way: Vector = ",vehCreationLocation.vLoc)
							
							RETURN FALSE
						ENDIF
					ENDIF
					
					IF IS_BIT_SET(serverBD.iVehicleResBS,i)
						IF CAN_REGISTER_MISSION_ENTITIES(0,1,0,0)
							IF NETWORK_IS_IN_MP_CUTSCENE() 
								SET_NETWORK_CUTSCENE_ENTITIES(TRUE)	
								CDEBUG1LN(DEBUG_SAFEHOUSE, "CREATE_MODDED_CAR_FOR_GARAGE, in cutscene therefore setting SET_NETWORK_CUTSCENE_ENTITIES")
							ENDIF
						
							tempVehicle = CREATE_VEHICLE(vehicleSetup.eModel,vehCreationLocation.vLoc,vehCreationLocation.vRot.z,TRUE,TRUE)	
							serverbD.customVehicleNetIDs[i] = VEH_TO_NET(tempVehicle)
							
							IF i = HANGAR_DISPLAY_SLOT_MASSIVE
								SET_ENTITY_COORDS_NO_OFFSET(tempVehicle, vehCreationLocation.vLoc)
								SET_ENTITY_ROTATION(tempVehicle, vehCreationLocation.vRot)
							ENDIF
							
							SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(tempVehicle,TRUE)	
							
							#IF IS_DEBUG_BUILD
							PRINTLN("CREATE_MODDED_CAR_FOR_GARAGE: HOST Creating vehicle #",i, " save slot #",iSaveSlot,"at ", vehCreationLocation.vLoc, " in garage index# ",mpPropMaintain.iMostRecentPropertyID )
							#ENDIF
							
							serverBD.ivehicleSaveSlot[i] = iSaveSlot
							MPGlobalsAmbience.GarageVehicleID[i] = tempVehicle
							//MPGlobalsAmbience.bHasThisGarageVehicleBeenCreatedThisSession[iPropertySlot][i] = TRUE
							
							serverBD.iCarSpawnBS[i] = 0	
							IF bHasEmblem
								MP_SAVE_VEHICLE_APPLY_SAVED_DETAILS_TO_VEHICLE_INC_EMBLEM(tempVehicle,iSaveSlot)//,bLoadingEmblem[i])
							ELSE
								MP_SAVE_VEHICLE_APPLY_SAVED_DETAILS_TO_VEHICLE(tempVehicle,iSaveSlot)
								SET_BIT(serverBD.iCarSpawnBS[i],MP_PROP_CREATE_CARS_BS_APPLIED_DETAILS)
							ENDIF
							
							NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(tempVehicle,TRUE)
							
							IF GET_VEHICLE_HAS_LANDING_GEAR(tempVehicle)
								CONTROL_LANDING_GEAR(tempVehicle, LGC_DEPLOY_INSTANT)
							ENDIF
							
							IF IS_VEHICLE_MODEL(tempVehicle, VALKYRIE)
								SET_VEHICLE_EXTRA(tempVehicle, 12, TRUE)
								SET_VEHICLE_EXTRA(tempVehicle, 13, TRUE)
								SET_VEHICLE_EXTRA(tempVehicle, 14, TRUE)
							ENDIF
							
							SET_VEHICLE_NOT_STEALABLE_AMBIENTLY(tempVehicle,FALSE)
							SET_VEHICLE_FULLBEAM(tempVehicle, FALSE)
							SET_VEHICLE_LIGHTS(tempVehicle,FORCE_VEHICLE_LIGHTS_OFF)	
							SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(tempVehicle, TRUE)  
							
							SET_DISABLE_AUTOMATIC_CRASH_TASK(tempVehicle, TRUE)
							
							SET_VEHICLE_ENGINE_ON(tempVehicle, FALSE, TRUE)
							IF IS_THIS_MODEL_A_HELI(vehicleSetup.eModel)
								SET_HELI_BLADES_SPEED(tempVehicle, 0)
							ENDIF	
							
							//bRadioSet[i] = FALSE
							
							IF thisHangar.pOwner != INVALID_PLAYER_INDEX()
								IF IS_NET_PLAYER_OK(thisHangar.pOwner,FALSE,FALSE)
									SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(tempVehicle, thisHangar.pOwner, FALSE)
									PRINTLN("Setting vehicle",i," doors unlocked for ", GET_PLAYER_NAME(thisHangar.pOwner))
								ENDIF
							ENDIF
//							ENDIF
							
							SET_VEHICLE_FIXED(tempVehicle) 
					        SET_ENTITY_HEALTH(tempVehicle, 1000) 
					        SET_VEHICLE_ENGINE_HEALTH(tempVehicle, 1000) 
					        SET_VEHICLE_PETROL_TANK_HEALTH(tempVehicle, 1000) 
							
							IF IS_THIS_MODEL_A_HELI(vehicleSetup.eModel)
								SET_HELI_MAIN_ROTOR_HEALTH(tempVehicle, 1000)
								SET_HELI_TAIL_ROTOR_HEALTH(tempVehicle, 1000)
							ENDIF
							IF vehicleSetup.eModel = TULA //wings down for tula on spawn
								SET_VEHICLE_FLIGHT_NOZZLE_POSITION(tempVehicle, 0.0)
							ENDIF
							
							SET_VEHICLE_DIRT_LEVEL(tempVehicle,0.0)
			                SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(tempVehicle,TRUE)
							SET_VEHICLE_RESPECTS_LOCKS_WHEN_HAS_DRIVER(tempVehicle,TRUE)
							CLEAR_BIT(serverBD.iCreatedBS,i)

							//bLoadingEmblem[i] = FALSE
							SET_ENTITY_CAN_BE_DAMAGED(tempVehicle,FALSE)
							SET_ENTITY_INVINCIBLE(tempVehicle, TRUE)
							
							SET_ENTITY_VISIBLE(tempVehicle,FALSE) //hidden for test
							
							SET_VEHICLE_RADIO_ENABLED(tempVehicle, FALSE)
							
							// set this vehicle as a player_vehicle so no on else can steal it
							IF DECOR_IS_REGISTERED_AS_TYPE("Player_Vehicle", DECOR_TYPE_INT)
								IF NOT DECOR_EXIST_ON(tempVehicle, "Player_Vehicle")
									DECOR_SET_INT(tempVehicle, "Player_Vehicle", NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID()))
								ENDIF
							ENDIF
							
							SET_NETWORK_ID_ALWAYS_EXISTS_FOR_PLAYER(serverbD.customVehicleNetIDs[i],PLAYER_ID(),FALSE)
							PRINTLN("CREATE_MODDED_CAR_FOR_GARAGE: created vehicle ID ", i)
							IF NETWORK_IS_IN_MP_CUTSCENE() 
								SET_NETWORK_CUTSCENE_ENTITIES(FALSE)	
							ENDIF
							
							RESET_NET_TIMER(st_ServerWalkOutTimeout)
						ENDIF
					ENDIF
				ELSE
					PRINTLN("CREATE_MODDED_CAR_FOR_GARAGE: Waiting for CAN_RESERVE_NETWORK_ENTITIES_FOR_THIS_SCRIPT(",serverBD.iVehicleCreated+ 1 ,")") 
				ENDIF
			ELSE
				PRINTLN("CREATE_MODDED_CAR_FOR_GARAGE: Waiting for model to load")
				RETURN FALSE
			ENDIF
		ENDIF
	ELIF IS_NET_VEHICLE_DRIVEABLE(serverBD.customVehicleNetIDs[i])
		
		MPGlobalsAmbience.GarageVehicleID[i] = NET_TO_VEH(serverBD.customVehicleNetIDs[i])
	
		IF NOT IS_BIT_SET(serverBD.iCreatedBS,i)
			// Now set the specifics
			// Bug #437236 - Set all license plates for all profiles in SP and MP to have the same text when reading from the cloud data.
			IF NOT IS_BIT_SET(serverBD.iCarSpawnBS[i],MP_PROP_CREATE_CARS_BS_MODS_LOADED)
				tempVehicle = NET_TO_VEH(serverBD.customVehicleNetIDs[i])
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.customVehicleNetIDs[i])
					IF NOT IS_BIT_SET(serverBD.iCarSpawnBS[i],MP_PROP_CREATE_CARS_BS_APPLIED_DETAILS)
						IF bHasEmblem
							IF MP_SAVE_VEHICLE_APPLY_SAVED_DETAILS_TO_VEHICLE_INC_EMBLEM(tempVehicle,iSaveSlot)//,bLoadingEmblem[i])
							
								SET_BIT(serverBD.iCarSpawnBS[i],MP_PROP_CREATE_CARS_BS_APPLIED_DETAILS)
								PRINTLN("CREATE_MODDED_CAR_FOR_GARAGE: added all mods for car in slot # ", iSaveSlot)
							#IF IS_DEBUG_BUILD
							ELSE
								PRINTLN("CREATE_MODDED_CAR_FOR_GARAGE: waiting for applying details to car # ", i)
							#ENDIF
							ENDIF
						ELSE
							MP_SAVE_VEHICLE_APPLY_SAVED_DETAILS_TO_VEHICLE(tempVehicle,iSaveSlot)
							SET_BIT(serverBD.iCarSpawnBS[i],MP_PROP_CREATE_CARS_BS_APPLIED_DETAILS)
							PRINTLN("CREATE_MODDED_CAR_FOR_GARAGE: added all mods for car in slot # ", iSaveSlot)
						ENDIF
					ELSE
						IF HAVE_VEHICLE_MODS_STREAMED_IN(tempVehicle)
							IF i != HANGAR_DISPLAY_SLOT_MASSIVE
							AND NOT SET_VEHICLE_ON_GROUND_PROPERLY(tempVehicle)
								PRINTLN("CREATE_MODDED_CAR_FOR_GARAGE: SET_VEHICLE_ON_GROUND_PROPERLY failed, reseting position and trying again")
								SET_ENTITY_COORDS(tempVehicle, vehCreationLocation.vLoc)
								SET_ENTITY_ROTATION(tempVehicle, vehCreationLocation.vRot)
								SET_VEHICLE_ON_GROUND_PROPERLY(tempVehicle)
							ELSE
								PRINTLN("CREATE_MODDED_CAR_FOR_GARAGE: SET_VEHICLE_ON_GROUND_PROPERLY done")
							ENDIF
							SET_BIT(serverBD.iCarSpawnBS[i],MP_PROP_CREATE_CARS_BS_MODS_LOADED)
							serverBD.iVehicleCreated++
							SET_BIT(serverBD.iCreatedBS,i)
							
							NETWORK_FADE_IN_ENTITY(tempVehicle, true, false)
							
							SET_VEHICLE_DOORS_LOCKED(tempVehicle,VEHICLELOCK_UNLOCKED)

							SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(tempVehicle, TRUE)  
							IF thisHangar.pOwner != INVALID_PLAYER_INDEX()
								IF IS_NET_PLAYER_OK(thisHangar.pOwner,FALSE,FALSE)
									SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(tempVehicle, thisHangar.pOwner, FALSE)
									PRINTLN("Setting vehicle",i," doors unlocked for ", GET_PLAYER_NAME(thisHangar.pOwner))
								ENDIF
							ENDIF
			                SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(tempVehicle,TRUE)
							SET_VEHICLE_RESPECTS_LOCKS_WHEN_HAS_DRIVER(tempVehicle,TRUE)
							CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR: adding a new vehicle to garage incrementing serverBD.iVehicleCreated to :", serverBD.iVehicleCreated)
							thisHangar.sVehManageData.iReplacedVehicle = -1
							PRINTLN("CREATE_MODDED_CAR_FOR_GARAGE: setting server net ID for vehicle # ", i)
						#IF IS_DEBUG_BUILD
						ELSE
							PRINTLN("CREATE_MODDED_CAR_FOR_GARAGE: waiting for HAVE_VEHICLE_MODS_STREAMED_IN to car # ", i)
						#ENDIF
						ENDIF
					ENDIF
				ELSE
					NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.customVehicleNetIDs[i])
					PRINTLN("CREATE_MODDED_CAR_FOR_GARAGE: waiting for ownership for mods# ", i)
				ENDIF
			ELSE
				PRINTLN("CREATE_MODDED_CAR_FOR_GARAGE: MP_PROP_CREATE_CARS_BS_MODS_LOADED for vehicle ", i)
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC DELETE_CAR_FOR_HANGAR(INT i, INT iSaveSlot)
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.customVehicleNetIDs[i])
		IF IS_NET_VEHICLE_DRIVEABLE(serverBD.customVehicleNetIDs[i])
			IF GET_VEHICLE_DOOR_LOCK_STATUS(NET_TO_VEH(serverBD.customVehicleNetIDs[i])) != VEHICLELOCK_LOCKED
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.customVehicleNetIDs[i])
					PRINTLN("CDM: Locking doors for delete vehicle A# ",i)
					SET_VEHICLE_DOORS_LOCKED(NET_TO_VEH(serverBD.customVehicleNetIDs[i]),VEHICLELOCK_LOCKED)
				ELSE
					NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.customVehicleNetIDs[i])
				ENDIF
			ENDIF
			IF NOT HAS_NET_TIMER_STARTED(thisHangar.sVehManageData.failSafeClearVehicleDelay[i])
				START_NET_TIMER(thisHangar.sVehManageData.failSafeClearVehicleDelay[i],TRUE)
			ENDIF
			IF IS_VEHICLE_EMPTY(NET_TO_VEH(serverBD.customVehicleNetIDs[i]),TRUE, TRUE)
			OR HAS_NET_TIMER_EXPIRED(thisHangar.sVehManageData.failSafeClearVehicleDelay[i],3000,TRUE)
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.customVehicleNetIDs[i])
					
					DELETE_NET_ID(serverBD.customVehicleNetIDs[i])
					IF GET_NUM_RESERVED_MISSION_VEHICLES(FALSE,RESERVATION_LOCAL_ONLY) >= 1
					AND IS_BIT_SET(serverBD.iVehicleResBS,i)
						RESERVE_LOCAL_NETWORK_MISSION_VEHICLES(GET_NUM_RESERVED_MISSION_VEHICLES(FALSE,RESERVATION_LOCAL_ONLY) - 1)
					ENDIF
					serverBD.iReserved--
					CLEAR_BIT(serverBD.iVehicleResBS,i)
					IF iSaveSlot >= 0
						PRINTLN("MAINTAIN_CUSTOM_CARS: clearing MP_SAVED_VEHICLE_UPDATE_VEHICLE for vehicle #",i ," A saveSlot# ", iSaveSlot)
						CLEAR_BIT(g_MpSavedVehicles[iSaveSlot].iVehicleBS,MP_SAVED_VEHICLE_UPDATE_VEHICLE)
					ENDIF
					PRINTLN("MAINTAIN_CUSTOM_CARS: deleting vehicle #",i ," vehicle is no longer valid! A saveSlot# ", iSaveSlot)
					iSaveSlot = -1
				ELSE
					IF iSaveSlot >= 0
						IF NOT IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS,MP_SAVED_VEHICLE_UPDATE_VEHICLE)
							SET_BIT(g_MpSavedVehicles[iSaveSlot].iVehicleBS,MP_SAVED_VEHICLE_UPDATE_VEHICLE)
							PRINTLN("MAINTAIN_CUSTOM_CARS: setting update vehicle as player is trying to delete but does not have control")
						ENDIF
					ENDIF
					NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.customVehicleNetIDs[i])
					PRINTLN("MAINTAIN_CUSTOM_CARS: deleting vehicle trying to get control #",i)
				ENDIF
			ELSE
				IF IS_NET_PLAYER_OK(PLAYER_ID())
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),NET_TO_VEH(serverBD.customVehicleNetIDs[i]))
						TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID(),0,ECF_WARP_IF_DOOR_IS_BLOCKED)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.customVehicleNetIDs[i])
				DELETE_NET_ID(serverBD.customVehicleNetIDs[i])
				IF GET_NUM_RESERVED_MISSION_VEHICLES(FALSE,RESERVATION_LOCAL_ONLY) >= 1
				AND IS_BIT_SET(serverBD.iVehicleResBS,i)
					RESERVE_LOCAL_NETWORK_MISSION_VEHICLES(GET_NUM_RESERVED_MISSION_VEHICLES(FALSE,RESERVATION_LOCAL_ONLY) - 1)
				ENDIF
				serverBD.iReserved--
				CLEAR_BIT(serverBD.iVehicleResBS,i)
				PRINTLN("MAINTAIN_CUSTOM_CARS: deleting (NOT DRIVEABLE)vehicle #",i ," vehicle is no longer valid! saveSlot# ", iSaveSlot)
				
				IF iSaveSlot >= 0
					CLEAR_BIT(g_MpSavedVehicles[iSaveSlot].iVehicleBS,MP_SAVED_VEHICLE_UPDATE_VEHICLE)
				ENDIF
			ELSE
				NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.customVehicleNetIDs[i])
			ENDIF
		ENDIF
	ELSE
		IF iSaveSlot >= 0
			CLEAR_BIT(g_MpSavedVehicles[iSaveSlot].iVehicleBS,MP_SAVED_VEHICLE_UPDATE_VEHICLE)
		ENDIF
		RESET_NET_TIMER(thisHangar.sVehManageData.failSafeClearVehicleDelay[i])
	ENDIF
ENDPROC

PROC RESET_CHECK_ALL_VEHICLES_AVAILABLE_FOR_GAME()
	CLEAR_BIT(thisHangar.sVehManageData.iManageVehCreateBS,MANAGE_VEH_CREATE_VEHICLES_AVAILABLE_CHECKED)
	thisHangar.sVehManageData.iVehSpaceAvailableForGameBS = 0
ENDPROC

PROC CHECK_ALL_VEHICLES_AVAILABLE_FOR_GAME()
	BOOL bSkip
	INT i, iDisplaySlot, iSaveSlot
	IF NOT IS_BIT_SET(thisHangar.sVehManageData.iManageVehCreateBS,MANAGE_VEH_CREATE_VEHICLES_AVAILABLE_CHECKED)
		REPEAT MAX_DISPLAYED_VEHS_IN_HANGAR i
			iDisplaySlot = i + DISPLAY_SLOT_START_HANGAR
			MPSV_GET_SAVE_SLOT_FROM_DISPLAY_SLOT(iDisplaySlot,iSaveSlot)	
			bSkip = FALSE
			IF iSaveSlot < 0
				PRINTLN("CHECK_ALL_VEHICLES_AVAILABLE_FOR_GAME: skipping no save for vehSlot# ",i)
				bSkip= TRUE
			ENDIF
			IF NOT bSkip	
				IF g_MpSavedVehicles[iSaveSlot].vehicleSetupMP.VehicleSetup.eModel != DUMMY_MODEL_FOR_SCRIPT
					IF IS_VEHICLE_AVAILABLE_FOR_GAME(g_MpSavedVehicles[iSaveSlot].vehicleSetupMP.VehicleSetup.eModel, TRUE)
						SET_BIT(thisHangar.sVehManageData.iVehSpaceAvailableForGameBS,i)
						PRINTLN("CHECK_ALL_VEHICLES_AVAILABLE_FOR_GAME: set true for  vehslot: ",i," displayslot: ",iDisplaySlot," saveSlot:",iSaveSlot)
					ELSE
						CLEAR_BIT(thisHangar.sVehManageData.iVehSpaceAvailableForGameBS,i)
						PRINTLN("CHECK_ALL_VEHICLES_AVAILABLE_FOR_GAME: set false for  vehslot: ",i," displayslot: ",iDisplaySlot," saveSlot:",iSaveSlot)
					ENDIF
				ELSE
					PRINTLN("CHECK_ALL_VEHICLES_AVAILABLE_FOR_GAME: set false DUMMY_MODEL for  vehslot: ",i," displayslot: ",iDisplaySlot," saveSlot:",iSaveSlot)
				ENDIF
			ENDIF
		ENDREPEAT
		SET_BIT(thisHangar.sVehManageData.iManageVehCreateBS,MANAGE_VEH_CREATE_VEHICLES_AVAILABLE_CHECKED)
		PRINTLN("CHECK_ALL_VEHICLES_AVAILABLE_FOR_GAME: run this frame")
		g_bResetMaintainCarImprovements = FALSE
	ENDIF
ENDPROC

FUNC BOOL IS_VEHICLE_AVAILABLE_FOR_GAME_IN_HANGAR(INT iVehSpace,INT iSaveSlot)
	IF NOT g_bRunMaintainCarImprovements
		RETURN IS_VEHICLE_AVAILABLE_FOR_GAME(g_MpSavedVehicles[iSaveSlot].vehicleSetupMP.VehicleSetup.eModel, TRUE)
	ENDIF
	IF g_bResetMaintainCarImprovements
		RESET_CHECK_ALL_VEHICLES_AVAILABLE_FOR_GAME()
	ENDIF
	IF IS_BIT_SET(thisHangar.sVehManageData.iManageVehCreateBS,MANAGE_VEH_CREATE_VEHICLES_AVAILABLE_CHECKED)
		RETURN IS_BIT_SET(thisHangar.sVehManageData.iVehSpaceAvailableForGameBS,iVehSpace)
	ELSE
		CHECK_ALL_VEHICLES_AVAILABLE_FOR_GAME()
		IF IS_BIT_SET(thisHangar.sVehManageData.iVehSpaceAvailableForGameBS,iVehSpace)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC 

//PROC MAINTAIN_REGISTRATION()
//	INT iCurrentlyReserved = GET_NUM_RESERVED_MISSION_VEHICLES(FALSE,RESERVATION_LOCAL_ONLY)
//	IF serverBD.iReserved != iCurrentlyReserved
//		IF serverBD.iReserved < iCurrentlyReserved
//		OR CAN_RESERVE_NETWORK_VEHICLES_FOR_THIS_SCRIPT(serverBD.iReserved, FALSE, TRUE)
//			CDEBUG2LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR: - MAINTAIN_REGISTRATION() - L = ",iCurrentlyReserved, " S = ",serverBD.iReserved)
//			RESERVE_LOCAL_NETWORK_MISSION_VEHICLES(serverBD.iReserved)
//		ENDIF
//	ENDIF
//ENDPROC

PROC MAINTAIN_CURRENT_PV_SLOT_GLOBAL()

	#IF IS_DEBUG_BUILD
	INT iLastPVSlot = g_iCurrentPVSlotInProperty
	#ENDIF
	
	IF IS_NET_PLAYER_OK(PLAYER_ID())
	AND thisHangar.pOwner = PLAYER_ID()
		
		IF g_iCurrentPVSlotInProperty != -1
			IF NOT IS_PLAYER_BROWSING_ITEMS_IN_SHOP_OF_TYPE(SHOP_TYPE_CARMOD)	
			AND NOT IS_PLAYER_BROWSING_ITEMS_IN_SHOP_OF_TYPE(SHOP_TYPE_PERSONAL_CARMOD)
				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
					g_iCurrentPVSlotInProperty = -1
					PRINTLN("MAINTAIN_CURRENT_PV_SLOT_GLOBAL - setting g_iCurrentPVSlotInProperty to -1 ")
				ENDIF
			ENDIF
		ENDIF
		
		INT i
		INT iSaveSlot
		VEHICLE_INDEX vehicleID
		REPEAT MAX_DISPLAYED_VEHS_IN_HANGAR i
			IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.customVehicleNetIDs[i])
				vehicleID = NET_TO_VEH(serverBD.customVehicleNetIDs[i])
				IF IS_VEHICLE_DRIVEABLE(vehicleID)
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehicleID,TRUE)
						MPSV_GET_SAVE_SLOT_FROM_DISPLAY_SLOT(DISPLAY_SLOT_START_HANGAR+i, iSaveSlot)
						g_iCurrentPVSlotInProperty = iSaveSlot
						g_iCurrentPViSaveSlot = i
//						g_vehicleTakingToOfficeModGarage = NET_TO_VEH(serverBD.customVehicleNetIDs[g_iCurrentPViSaveSlot])
						#IF IS_DEBUG_BUILD
						IF iLastPVSlot != g_iCurrentPVSlotInProperty
							PRINTLN("MAINTAIN_CURRENT_PV_SLOT_GLOBAL - Changing g_iCurrentPVSlotInProperty to ", g_iCurrentPVSlotInProperty)
						ENDIF
						#ENDIF
						
						EXIT
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF iLastPVSlot != g_iCurrentPVSlotInProperty
		PRINTLN("MAINTAIN_CURRENT_PV_SLOT_GLOBAL - Changed g_iCurrentPVSlotInProperty to ", g_iCurrentPVSlotInProperty)
	ENDIF
	#ENDIF

ENDPROC


//FUNC BOOL MAINTAIN_CUSTOM_CAR_CREATION_HANGAR()
//	
//	MAINTAIN_REGISTRATION()
//	MAINTAIN_CURRENT_PV_SLOT_GLOBAL()
//	
//	INT iDisplaySlot, iSaveSlot
//	
//	//BOOL bSkip
//	BOOL bHasEmblem
//
//
//	IF thisHangar.pOwner != PLAYER_ID()
//		IF NOT serverBD.bOwnerCompletedVehicleCreation
//		AND (NETWORK_IS_PLAYER_A_PARTICIPANT(thisHangar.pOwner)
//		OR IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBS, BS_SIMPLE_INTERIOR_GLOBAL_PLAYER_BD_DRIVER_ENTERING))
//		
//		
//			PRINTLN("MAINTAIN_CUSTOM_CAR_CREATION_ARMORY_TRUCK: waiting for owner to be host/create personal vehicle if nessecary")
//			RETURN FALSE
//		ENDIF
//	ELSE
//		IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
//			IF NOT HAS_NET_TIMER_STARTED(thisHangar.sVehManageData.st_HostRequestTimer)
//				PRINTLN("MAINTAIN_CUSTOM_CAR_CREATION_ARMORY_TRUCK: local player (OWNER) requesting to be host of script ")
//				NETWORK_REQUEST_TO_BE_HOST_OF_THIS_SCRIPT()
//				START_NET_TIMER(thisHangar.sVehManageData.st_HostRequestTimer,TRUE)
//			ELSE
//				IF HAS_NET_TIMER_EXPIRED(thisHangar.sVehManageData.st_HostRequestTimer,1000,TRUE)
//					RESET_NET_TIMER(thisHangar.sVehManageData.st_HostRequestTimer)
//				ENDIF
//			ENDIF
//			RETURN FALSE
//		ENDIF
//		
//		IF g_bPlayerLeavingCurrentInteriorInVeh
//			PRINTLN("MAINTAIN_CUSTOM_CAR_CREATION_ARMORY_TRUCK: g_bPlayerLeavingCurrentInteriorInVeh = TRUE exiting creation")
//			RETURN FALSE
//		ENDIF
//	ENDIF
//	
//
//	IF thisHangar.pOwner  = PLAYER_ID()	
//		IF NETWORK_IS_IN_MP_CUTSCENE()
//			IF NOT IS_BIT_SET(thisHangar.sVehManageData.iManageVehCreateBS,MANAGE_VEH_CREATE_SET_CUTSCENE_ENTITIES_TO_BE_NETWORKED)
//				SET_NETWORK_CUTSCENE_ENTITIES(TRUE)
//				SET_BIT(thisHangar.sVehManageData.iManageVehCreateBS,MANAGE_VEH_CREATE_SET_CUTSCENE_ENTITIES_TO_BE_NETWORKED)
//				PRINTLN("AM_MP_ARMORY_TRUCK: Setting entities created to be networked in CS")
//			ENDIF
//		ELSE
//			CLEAR_BIT(thisHangar.sVehManageData.iManageVehCreateBS,MANAGE_VEH_CREATE_SET_CUTSCENE_ENTITIES_TO_BE_NETWORKED)
//		ENDIF
//	ENDIF
//	
//	INT i = 0
//	REPEAT MAX_DISPLAYED_VEHS_IN_HANGAR i
//		iDisplaySlot = DISPLAY_SLOT_START_HANGAR + i
//		MPSV_GET_SAVE_SLOT_FROM_DISPLAY_SLOT(iDisplaySlot,iSaveSlot)
//		IF thisHangar.pOwner = PLAYER_ID()
//			
//			IF iSaveSlot >= 0
//			AND g_MpSavedVehicles[iSaveSlot].vehicleSetupMP.VehicleSetup.eModel != DUMMY_MODEL_FOR_SCRIPT
//			AND NOT IS_HANGAR_DISPLAY_SLOT_IN_STORAGE(iDisplaySlot)
//			AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(g_MpSavedVehicles[iSaveSlot].vehicleSetupMP.VehicleSetup.eModel,PROPERTY_OWNED_SLOT_HANGAR)
//			AND IS_VEHICLE_AVAILABLE_FOR_GAME_IN_HANGAR(i, iSaveSlot)
//				IF NOT IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS,MP_SAVED_VEHICLE_OUT_GARAGE)
//				AND NOT IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS,MP_SAVED_VEHICLE_DESTROYED) 
//				AND NOT IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS,MP_SAVED_VEHICLE_IMPOUNDED)
//				AND MPGlobalsAmbience.iVDPersonalVehicleSlot != iSaveSlot
//					IF IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS,MP_SAVED_VEHICLE_UPDATE_VEHICLE)
//						PRINTLN("MAINTAIN_CUSTOM_CARS:MP_SAVED_VEHICLE_UPDATE_VEHICLE set on vehicle #",i," saveSlot# ", iSaveSlot)
//						DELETE_CAR_FOR_HANGAR(i, iSaveSlot)
//					ELIF NOT IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS,MP_SAVED_VEHICLE_JUST_PURCHASED)
//						IF IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS,MP_SAVED_VEHICLE_HAS_CREW_EMBLEM)
//							bHasEmblem = TRUE
//						ELSE
//							bHasEmblem = FALSE
//						ENDIF
//						
//						IF NOT CREATE_MODDED_CAR_FOR_HANGAR(g_MpSavedVehicles[iSaveSlot].vehicleSetupMP.VehicleSetup, i,iSaveSlot,bHasEmblem)
//							RETURN FALSE
//						ENDIF
//						RESET_NET_TIMER(thisHangar.sVehManageData.failSafeClearVehicleDelay[i])
//						
//					ELSE
//						IF IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS,MP_SAVED_VEHICLE_JUST_PURCHASED)
//							PRINTLN("MAINTAIN_CUSTOM_CARS: waiting for MP_SAVED_VEHICLE_JUST_PURCHASED to be cleared on vehicle in save slot # ",iSaveSlot)
//						ENDIF
//						DELETE_CAR_FOR_HANGAR(i, iSaveSlot)
//					ENDIF
//				ELSE
//					DELETE_CAR_FOR_HANGAR(i, iSaveSlot)
//				ENDIF
//			ELSE
//				DELETE_CAR_FOR_HANGAR(i,iSaveSlot)
//			ENDIF
//		ELSE
//			IF thisHangar.pOwner != INVALID_PLAYER_INDEX()
//				IF IS_NET_PLAYER_OK(thisHangar.pOwner,FALSE,TRUE)
//					IF NOT NETWORK_IS_PLAYER_A_PARTICIPANT(thisHangar.pOwner)
//						IF GlobalplayerBD_FM[NATIVE_TO_INT(thisHangar.pOwner)].iCurrentsActivePersonalVehicle = iSaveSlot
//						AND iSaveSlot >= 0
//							DELETE_CAR_FOR_HANGAR(i, iSaveSlot)
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.customVehicleNetIDs[i])
//			IF IS_BIT_SET(serverBD.iCreatedBS,i)
//				serverBD.iVehicleCreated--
//				CLEAR_BIT(serverBD.iVehicleResBS,i)
//				CLEAR_BIT(serverBD.iCreatedBS,i)
//				serverBD.iCarSpawnBS[i] = 0	
//				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR: CREATE_MODDED_CAR_FOR_GARAGE: a vehicle is now not driveable reducing serverBD.iVehicleCreated to :", serverBD.iVehicleCreated)
//			ENDIF
//			RESET_NET_TIMER(thisHangar.sVehManageData.failSafeClearVehicleDelay[i])
//		ENDIF
//	ENDREPEAT
//	
//	RETURN TRUE
//ENDFUNC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════╡ VEHICLE MANAGEMENT MENU ╞══════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

FUNC BOOL IS_HANGAR_STORAGE_OPTION_SELECTED()
	RETURN thisHangar.sVehManageData.iCurVertSel = thisHangar.sVehManageData.iMaxVertSel-1
	AND NOT IS_BIT_SET(thisHangar.sVehManageData.iManageVehMenuBS, MANAGE_VEH_MENU_AREA_STORAGE)
ENDFUNC


PROC DRAW_MARKER_FOR_VEHICLE(INT iSelectedIndex, HUD_COLOURS markerColour)
	VECTOR vFrontTopLeft, vFrontTopRight, vBackTopLeft, vBackTopRight
	
	VECTOR vPos, vRot
	INT iR, iG, iB,iA
	MP_PROP_OFFSET_STRUCT vehCreationLocation
	
	IF iSelectedIndex >= 0
		IF IS_NET_VEHICLE_DRIVEABLE(serverBD.customVehicleNetIDs[iSelectedIndex])
			DETERMINE_4_TOP_VECTORS_OF_MODEL_BOX_IN_WORLD_COORDS(NET_TO_VEH(serverBD.customVehicleNetIDs[iSelectedIndex]),
																GET_ENTITY_MODEL(NET_TO_VEH(serverBD.customVehicleNetIDs[iSelectedIndex])),
																vFrontTopLeft,vFrontTopRight,vBackTopLeft,vBackTopRight)	
			vPos = GET_ENTITY_COORDS(NET_TO_VEH(serverBD.customVehicleNetIDs[iSelectedIndex]))

			vPos.z = vFrontTopRight.z + 2 
			PRINTLN("Vehicle POS with model Z 1= ", vPos)		
			
		ELSE
			GET_HANGAR_VEH_CREATION_POSITION(vehCreationLocation, iSelectedIndex)
			vPos = vehCreationLocation.vLoc+<<0,0,2>>
		ENDIF
		GET_HUD_COLOUR(markerColour,iR, iG, iB,iA)
		vRot = <<0,0,0>>
		DRAW_MARKER(MARKER_ARROW, vPos, <<0,0,0>>,<<0,180,vRot.z>> , <<1,1,1>>,iR, iG, iB, 255)
	ENDIF
ENDPROC

PROC DRAW_MARKER_FOR_VEHICLE_SPACE(INT iSelectedIndex, FLOAT fRadius)
	
	VECTOR vPos
	INT iR, iG, iB,iA
	MP_PROP_OFFSET_STRUCT vehCreationLocation
	
	IF iSelectedIndex >= 0
		IF IS_NET_VEHICLE_DRIVEABLE(serverBD.customVehicleNetIDs[iSelectedIndex])
			vPos = GET_ENTITY_COORDS(NET_TO_VEH(serverBD.customVehicleNetIDs[iSelectedIndex]))

			vPos.z = -49.5
			PRINTLN("Vehicle POS with model Z 1= ", vPos)		
			
		ELSE
			GET_HANGAR_VEH_CREATION_POSITION(vehCreationLocation, iSelectedIndex)
			vPos = vehCreationLocation.vLoc
			vPos.z = -49.5
		ENDIF
		GET_HUD_COLOUR(HUD_COLOUR_BLUELIGHT,iR, iG, iB,iA)
		DRAW_MARKER(MARKER_CYLINDER, vPos, <<0,0,0>>,<<0,0,0>> , <<fRadius,fRadius,fRadius/3.0>>,iR, iG, iB, 255)
	ENDIF
ENDPROC

PROC DRAW_MARKERS_ABOVE_SELECTED_VEHICLES()
	
	INT iSelectedIndex = -1
	
	
	//MARKERS
	IF thisHangar.sVehManageData.eMenuStage = VEHICLE_MANAGE_MENU_STAGE_MOVE
		MPSV_GET_DISPLAY_SLOT_FROM_SAVE_SLOT(thisHangar.sVehManageData.iSelectedVehSlot,iSelectedIndex)
		IF NOT IS_HANGAR_DISPLAY_SLOT_IN_STORAGE(iSelectedIndex)
			DRAW_MARKER_FOR_VEHICLE(iSelectedIndex - DISPLAY_SLOT_START_HANGAR, HUD_COLOUR_BLUEDARK)
		ENDIF
	ENDIF
	
	iSelectedIndex = thisHangar.sVehManageData.iVehicleDisplaySlotIDs[thisHangar.sVehManageData.iCurVertSel]
	
	IF (thisHangar.sVehManageData.eMenuStage = VEHICLE_MANAGE_MENU_STAGE_SELECT
	AND NOT IS_BIT_SET(thisHangar.sVehManageData.iManageVehMenuBS, MANAGE_VEH_MENU_AREA_STORAGE))
	OR thisHangar.sVehManageData.eMenuStage = VEHICLE_MANAGE_MENU_STAGE_REPLACE
		IF NOT IS_HANGAR_DISPLAY_SLOT_IN_STORAGE(iSelectedIndex)
			DRAW_MARKER_FOR_VEHICLE(iSelectedIndex - DISPLAY_SLOT_START_HANGAR, HUD_COLOUR_BLUELIGHT)
		ENDIF
	ELIF thisHangar.sVehManageData.eMenuStage = VEHICLE_MANAGE_MENU_STAGE_MOVE
	AND NOT IS_HANGAR_STORAGE_OPTION_SELECTED()
		FLOAT fRadius = 5.0
		SWITCH GET_DISPLAY_HANGAR_SLOT_SIZE(iSelectedIndex)
			CASE HANGAR_SLOT_SIZE_SMALL
				fRadius = 12.0
			BREAK
			CASE HANGAR_SLOT_SIZE_LARGE
				fRadius = 16.0
			BREAK
			CASE HANGAR_SLOT_SIZE_HUGE
				fRadius = 30.0
			BREAK
			CASE HANGAR_SLOT_SIZE_MASSIVE
				fRadius = 35.0
			BREAK
		ENDSWITCH
		DRAW_MARKER_FOR_VEHICLE_SPACE(iSelectedIndex - DISPLAY_SLOT_START_HANGAR, fRadius)
	ENDIF
	
	IF thisHangar.sVehManageData.eMenuStage = VEHICLE_MANAGE_MENU_STAGE_MOVE
	AND NOT IS_HANGAR_STORAGE_OPTION_SELECTED()
		INT iBlockers = GET_HANGAR_SLOT_BLOCKERS(iSelectedIndex)
		IF thisHangar.sVehManageData.iBlockAlphaBS != iBlockers
			thisHangar.sVehManageData.iBlockAlphaBS = iBlockers
			REPEAT MAX_DISPLAYED_VEHS_IN_HANGAR iSelectedIndex
				IF IS_NET_VEHICLE_DRIVEABLE(serverBD.customVehicleNetIDs[iSelectedIndex])
					IF IS_BIT_SET(iBlockers, iSelectedIndex)
						SET_ENTITY_ALPHA(NET_TO_VEH(serverBD.customVehicleNetIDs[iSelectedIndex]), 127, FALSE)
						SET_ENTITY_FLAG_SUPPRESS_SHADOW(NET_TO_ENT(serverBD.customVehicleNetIDs[iSelectedIndex]), TRUE) 
						SET_ENTITY_FLAG_RENDER_SMALL_SHADOW(NET_TO_ENT(serverBD.customVehicleNetIDs[iSelectedIndex]), FALSE) 
					ELSE
						RESET_ENTITY_ALPHA(NET_TO_VEH(serverBD.customVehicleNetIDs[iSelectedIndex]))
						SET_ENTITY_FLAG_SUPPRESS_SHADOW(NET_TO_ENT(serverBD.customVehicleNetIDs[iSelectedIndex]), FALSE) 
						SET_ENTITY_FLAG_RENDER_SMALL_SHADOW(NET_TO_ENT(serverBD.customVehicleNetIDs[iSelectedIndex]), TRUE) 
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ELIF thisHangar.sVehManageData.iBlockAlphaBS > 0
		REPEAT MAX_DISPLAYED_VEHS_IN_HANGAR iSelectedIndex
			IF IS_NET_VEHICLE_DRIVEABLE(serverBD.customVehicleNetIDs[iSelectedIndex])
			AND IS_BIT_SET(thisHangar.sVehManageData.iBlockAlphaBS, iSelectedIndex)
				RESET_ENTITY_ALPHA(NET_TO_VEH(serverBD.customVehicleNetIDs[iSelectedIndex]))
				SET_ENTITY_FLAG_SUPPRESS_SHADOW(NET_TO_ENT(serverBD.customVehicleNetIDs[iSelectedIndex]), FALSE) 
				SET_ENTITY_FLAG_RENDER_SMALL_SHADOW(NET_TO_ENT(serverBD.customVehicleNetIDs[iSelectedIndex]), TRUE) 
			ENDIF
		ENDREPEAT
		thisHangar.sVehManageData.iBlockAlphaBS = 0
	ENDIF
ENDPROC

FUNC BOOL HANDLE_VEHICLE_MANAGEMENT_MENU_NAVIGATION_DELAY(CONTROL_ACTION theInput)
	IF (NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL,theInput)
	AND NOT (IS_PHONE_ONSCREEN() AND IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,theInput)))
	OR HAS_NET_TIMER_EXPIRED(thisHangar.sVehManageData.menuNavigationDelay,250,TRUE)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC GET_CAMERA_LOCATION(INT iManageCamera, MP_CAM_OFFSET_STRUCT_FLOAT &camLocation)
	SWITCH iManageCamera
		CASE 0
			camLocation.vLoc = <<-1266.9, -2961.0, -32.0>>
			camLocation.vRot = <<-25.0, 0.0, 180.0>>
			camLocation.fFov = 70.0
		BREAK
		CASE 1
			camLocation.vLoc = <<-1283.0, -2968.0, -43.0>>
			camLocation.vRot = <<-10.0, 0.0, -150.0>>
			camLocation.fFov = 60.0
		BREAK
		CASE 2
			camLocation.vLoc = <<-1266.9, -3046.0, -34.5>>
			camLocation.vRot = <<-25.0, 0.0, -0.>>
			camLocation.fFov = 60.0
		BREAK
		CASE 3
			camLocation.vLoc = <<-1250.8, -2968.0, -43.0>>
			camLocation.vRot = <<-10.0, 0.0, 150.0>>
			camLocation.fFov = 60.0
		BREAK
	ENDSWITCH
ENDPROC

PROC UPDATE_CAMERA()
	IF NOT DOES_CAM_EXIST(thisHangar.sVehManageData.camManage)
		thisHangar.sVehManageData.camManage = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE) 
		SET_CAM_ACTIVE(thisHangar.sVehManageData.camManage, TRUE)
		RENDER_SCRIPT_CAMS(TRUE,FALSE)
	ENDIF
	
	MP_CAM_OFFSET_STRUCT_FLOAT offset
	GET_CAMERA_LOCATION(thisHangar.sVehManageData.iManageCamera, offset)
	
	SET_CAM_PARAMS(thisHangar.sVehManageData.camManage, offset.vLoc, offset.vRot, offset.fFov, 0)
ENDPROC

PROC HANDLE_MENU_DESCRIPTION()
	IF NOT IS_BIT_SET(thisHangar.sVehManageData.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
		SWITCH thisHangar.sVehManageData.eMenuStage
			CASE VEHICLE_MANAGE_MENU_STAGE_SELECT
				IF thisHangar.sVehManageData.iVehicleSlotIDs[0] = -1
					SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_MAN_VEH7ac")
				ELSE
					IF thisHangar.sVehManageData.iVehicleSlotIDs[thisHangar.sVehManageData.iCurVertSel] >= 0 AND IS_BIT_SET(g_MpSavedVehicles[thisHangar.sVehManageData.iVehicleSlotIDs[thisHangar.sVehManageData.iCurVertSel]].iVehicleBS, MP_SAVED_VEHICLE_DESTROYED)
						SET_CURRENT_MENU_ITEM_DESCRIPTION("MAN_VEH_DESac")
					ELSE
						SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_MAN_VEH3ac")
					ENDIF
				ENDIF
			BREAK
			CASE VEHICLE_MANAGE_MENU_STAGE_MOVE
				SET_CURRENT_MENU_ITEM_DESCRIPTION("CAR_MAN_1ac")
			BREAK
			CASE VEHICLE_MANAGE_MENU_STAGE_REPLACE
				SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_PROP_SC_3ac")
			BREAK
			CASE VEHICLE_MANAGE_MENU_STAGE_AREA
				SET_CURRENT_MENU_ITEM_DESCRIPTION("MAN_VEH_AREA_D")
			BREAK
		ENDSWITCH
	ELSE
		IF thisHangar.sVehManageData.eMenuStage = VEHICLE_MANAGE_MENU_STAGE_REPLACE
			SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_MAN_VEH4aac")
		ELIF thisHangar.sVehManageData.eMenuStage = VEHICLE_MANAGE_MENU_STAGE_MOVE 
		AND IS_HANGAR_STORAGE_OPTION_SELECTED()
			SET_CURRENT_MENU_ITEM_DESCRIPTION("VEH_STOR_WARN2")
		ELIF GET_HANGAR_SLOT_BLOCKERS(thisHangar.sVehManageData.iVehicleDisplaySlotIDs[thisHangar.sVehManageData.iCurVertSel]) > 0
			SET_CURRENT_MENU_ITEM_DESCRIPTION("VEH_STOR_WARN")
		ELSE
			SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_MAN_VEH5aac")
		ENDIF
	ENDIF
ENDPROC

PROC DRAW_MENU_THIS_FRAME()
	HANDLE_MENU_DESCRIPTION()
	DRAW_MENU()
	SET_BIT(thisHangar.sVehManageData.iManageVehMenuBS, MANAGE_VEH_MENU_DRAWING_LAST)
ENDPROC

FUNC BOOL SHOULD_DISABLE_VEHICLE_MANAGEMENT()
	IF (g_BGScript_PreventCarManagement)
	OR (g_sMPTunables.bDISABLE_VEHICLE_MANAGEMENT_HANGAR)
	OR IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
	OR thisHangar.pOwner != PLAYER_ID()
	OR IS_BROWSER_OPEN()
	OR SHOULD_LAUNCH_CRIMINAL_STARTER_PACK_BROWSER()
		RETURN TRUE
	ENDIF
	#IF IS_DEBUG_BUILD
		PRINTLN("MAINTAIN_VEHICLE_MANAGEMENT: ShouldVehicleManagementBeDisabled returning FALSE, g_BGScript_PreventCarManagement = ", g_BGScript_PreventCarManagement, ", g_sMPTunables.bDISABLE_VEHICLE_MANAGEMENT = ", g_sMPTunables.bDISABLE_VEHICLE_MANAGEMENT)
	#ENDIF
	RETURN FALSE
ENDFUNC

PROC SETUP_VEHICLE_MANAGEMENT_MENU()
	thisHangar.sVehManageData.iMaxVertSel = 0
	
	DISPLAY_RADAR(FALSE)
	
	CLEAR_MENU_DATA()
	
	SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT,MENU_ITEM_TEXT)
	SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT, FONT_RIGHT)
	SET_MENU_ITEM_TOGGLEABLE(FALSE, FALSE)	
	
	SWITCH thisHangar.sVehManageData.eMenuStage
		CASE VEHICLE_MANAGE_MENU_STAGE_SELECT
			SET_MENU_TITLE("MAN_VEH_AC_T")
		BREAK
		CASE VEHICLE_MANAGE_MENU_STAGE_MOVE
			SET_MENU_TITLE("MAN_VEH_SLOT_T")
		BREAK
		CASE VEHICLE_MANAGE_MENU_STAGE_REPLACE
			SET_MENU_TITLE("MP_MAN_VEH4ac")
		BREAK
		CASE VEHICLE_MANAGE_MENU_STAGE_AREA
			SET_MENU_TITLE("MAN_VEH_AREA_T")
		BREAK
	ENDSWITCH
	
	INT i
	INT iVehSlot, iSaveSlot
	
	TEXT_LABEL_23 textLabel
	
	INT iMenuCounter
	
	BOOL bSkip = FALSE
	
	IF thisHangar.sVehManageData.eMenuStage = VEHICLE_MANAGE_MENU_STAGE_AREA
		ADD_MENU_ITEM_TEXT(0,"MAN_VEH_FLOR_T",0)
		ADD_MENU_ITEM_TEXT(1,"MAN_VEH_STOR_T",0)
		
		IF IS_BIT_SET(thisHangar.sVehManageData.iManageVehMenuBS, MANAGE_VEH_MENU_AREA_STORAGE)
			CLEAR_BIT(thisHangar.sVehManageData.iManageVehMenuBS, MANAGE_VEH_MENU_AREA_STORAGE)
			thisHangar.sVehManageData.iCurVertSel = 1
		ENDIF
		
		iMenuCounter = 2
	ELSE
		REPEAT MAX_SAVED_VEHS_IN_HANGAR i
			iVehSlot = DISPLAY_SLOT_START_HANGAR+i
			
			MPSV_GET_SAVE_SLOT_FROM_DISPLAY_SLOT(iVehSlot,iSaveSlot)
			
			IF (thisHangar.sVehManageData.eMenuStage = VEHICLE_MANAGE_MENU_STAGE_SELECT 
				AND (IS_HANGAR_DISPLAY_SLOT_IN_STORAGE(iVehSlot) OR GET_HANGAR_SLOT_BLOCKERS(iVehSlot) > 0 OR (iSaveSlot > -1 AND IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS, MP_SAVED_VEHICLE_DESTROYED))) = (IS_BIT_SET(thisHangar.sVehManageData.iManageVehMenuBS, MANAGE_VEH_MENU_AREA_STORAGE)))
			OR thisHangar.sVehManageData.eMenuStage = VEHICLE_MANAGE_MENU_STAGE_MOVE
			OR thisHangar.sVehManageData.eMenuStage = VEHICLE_MANAGE_MENU_STAGE_REPLACE
			
				bSkip = FALSE
				
				
				IF thisHangar.sVehManageData.eMenuStage = VEHICLE_MANAGE_MENU_STAGE_MOVE
				AND (GET_DISPLAY_HANGAR_SLOT_SIZE(iVehSlot) != GET_MODEL_HANGAR_SLOT_SIZE(g_MpSavedVehicles[thisHangar.sVehManageData.iSelectedVehSlot].VehicleSetupMP.VehicleSetup.eModel)
				OR i >= MAX_DISPLAYED_VEHS_IN_HANGAR)
					bSkip = TRUE
				ENDIF
				
				//don't display vehicles that haven't arrived yet
				IF thisHangar.sVehManageData.eMenuStage = VEHICLE_MANAGE_MENU_STAGE_SELECT
				AND iSaveSlot > -1
				AND IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS, MP_SAVED_VEHICLE_JUST_PURCHASED)
					bSkip = TRUE
				ENDIF
				
				IF NOT bSkip
					IF thisHangar.sVehManageData.eMenuStage = VEHICLE_MANAGE_MENU_STAGE_MOVE
						SWITCH GET_MODEL_HANGAR_SLOT_SIZE(g_MpSavedVehicles[thisHangar.sVehManageData.iSelectedVehSlot].VehicleSetupMP.VehicleSetup.eModel)
							CASE HANGAR_SLOT_SIZE_SMALL
								textLabel = "MAN_SLOT_S"
							BREAK
							CASE HANGAR_SLOT_SIZE_LARGE
								textLabel = "MAN_SLOT_M"
							BREAK
							CASE HANGAR_SLOT_SIZE_HUGE
								textLabel = "MAN_SLOT_L"
							BREAK
							CASE HANGAR_SLOT_SIZE_MASSIVE
								textLabel = "MAN_SLOT_MASS"
							BREAK
						ENDSWITCH
						
						ADD_MENU_ITEM_TEXT(iMenuCounter,textLabel,1)
						ADD_MENU_ITEM_TEXT_COMPONENT_INT(iMenuCounter + 1)
					ELSE
					
						IF iSaveSlot >= 0
						AND g_MpSavedVehicles[iSaveSlot].VehicleSetupMP.VehicleSetup.eModel != DUMMY_MODEL_FOR_SCRIPT
							IF IS_VEHICLE_AVAILABLE_FOR_GAME(g_MpSavedVehicles[iSaveSlot].vehicleSetupMP.VehicleSetup.eModel, TRUE)
								IF IS_MODEL_IN_CDIMAGE(g_MpSavedVehicles[iSaveSlot].VehicleSetupMP.VehicleSetup.eModel)
								
									textLabel = "PIL_MP_VEH_01"
									
									IF thisHangar.sVehManageData.eMenuStage = VEHICLE_MANAGE_MENU_STAGE_SELECT
										SWITCH GET_MODEL_HANGAR_SLOT_SIZE(g_MpSavedVehicles[iSaveSlot].VehicleSetupMP.VehicleSetup.eModel)
											CASE HANGAR_SLOT_SIZE_SMALL
												textLabel = "MAN_VEH_S"
											BREAK
											CASE HANGAR_SLOT_SIZE_LARGE
												textLabel = "MAN_VEH_M"
											BREAK
											CASE HANGAR_SLOT_SIZE_HUGE
												textLabel = "MAN_VEH_L"
											BREAK
											CASE HANGAR_SLOT_SIZE_MASSIVE
												textLabel = "MAN_VEH_MASS"
											BREAK
										ENDSWITCH
									ENDIF
									
									BOOL bSelectable = TRUE
									
									IF IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS, MP_SAVED_VEHICLE_DESTROYED)
										textLabel = "MP_MAN_VEH_S01"
										bSelectable = FALSE
									ENDIF
								
									ADD_MENU_ITEM_TEXT(iMenuCounter,textLabel, 1, bSelectable)
									ADD_MENU_ITEM_TEXT_COMPONENT_STRING(GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(g_MpSavedVehicles[iSaveSlot].VehicleSetupMP.VehicleSetup.eModel))
						
								ENDIF
							ELSE
								ADD_MENU_ITEM_TEXT(iMenuCounter,"MP_MAN_VEH_S2",0)
							ENDIF
						ELSE
							bSkip = TRUE
						ENDIF
					ENDIF
				ENDIF
				IF NOT bSkip
					
					thisHangar.sVehManageData.iVehicleSlotIDs[iMenuCounter] = iSaveSlot
					thisHangar.sVehManageData.iVehicleDisplaySlotIDs[iMenuCounter] = iVehSlot
					
					PRINTLN("thisHangar.sVehManageData.iVehicleSlotIDs[",iMenuCounter,"] = ", iSaveSlot)
					PRINTLN("thisHangar.sVehManageData.iVehicleDisplaySlotIDs[",iMenuCounter,"] = ", iVehSlot)
					
					iMenuCounter++
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF thisHangar.sVehManageData.eMenuStage = VEHICLE_MANAGE_MENU_STAGE_MOVE
		AND NOT IS_BIT_SET(thisHangar.sVehManageData.iManageVehMenuBS, MANAGE_VEH_MENU_AREA_STORAGE)
		AND iMenuCounter > 0
			ADD_MENU_ITEM_TEXT(iMenuCounter,"MAN_VEH_STOR_T",0)
			iMenuCounter++
		ENDIF
	ENDIF
	
	IF iMenuCounter = 0 AND thisHangar.sVehManageData.eMenuStage = VEHICLE_MANAGE_MENU_STAGE_SELECT
		ADD_MENU_ITEM_TEXT(iMenuCounter,"MP_MAN_VEH_S",0)
		thisHangar.sVehManageData.iVehicleSlotIDs[iMenuCounter] = -1
		thisHangar.sVehManageData.iVehicleDisplaySlotIDs[iMenuCounter] = -1
		iMenuCounter++
	ENDIF
	
	
	thisHangar.sVehManageData.iMaxVertSel	= iMenuCounter
	
	SET_CURRENT_MENU_ITEM(thisHangar.sVehManageData.iCurVertSel)
	
	REMOVE_MENU_HELP_KEYS()
	
	IF thisHangar.sVehManageData.eMenuStage = VEHICLE_MANAGE_MENU_STAGE_SELECT AND thisHangar.sVehManageData.iVehicleSlotIDs[0] = -1 //no vehicles to select
		ADD_MENU_HELP_KEY_CLICKABLE( INPUT_FRONTEND_CANCEL, "BB_BACK")
	ELSE
		ADD_MENU_HELP_KEY_CLICKABLE( INPUT_FRONTEND_ACCEPT, "BB_SELECT")
		ADD_MENU_HELP_KEY_CLICKABLE( INPUT_FRONTEND_CANCEL, "BB_BACK")
	ENDIF
	ADD_MENU_HELP_KEY_CLICKABLE( INPUT_NEXT_CAMERA, "REZ_HELP_3")
	
ENDPROC

PROC CLEANUP_VEHICLE_MANAGEMENT_MENU()
	CLEANUP_MENU_ASSETS()
	
	RESET_NET_TIMER(thisHangar.sVehManageData.menuNavigationDelay)
	thisHangar.sVehManageData.iMaxVertSel = 0
	thisHangar.sVehManageData.iCurVertSel = 0
	thisHangar.sVehManageData.iButtonBS = 0
	thisHangar.sVehManageData.iManageCamera = 0
	thisHangar.sVehManageData.iSelectedVehSlot = -1
	thisHangar.sVehManageData.iSelectedVehDisplaySlot = -1
	thisHangar.sVehManageData.eMenuStage = VEHICLE_MANAGE_MENU_STAGE_AREA
	
	IF IS_BIT_SET(thisHangar.sVehManageData.iManageVehMenuBS, MANAGE_VEH_MENU_DISABLE_CONTROL)
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(),PCF_UseKinematicModeWhenStationary,FALSE)
		CLEAR_BIT(thisHangar.sVehManageData.iManageVehMenuBS, MANAGE_VEH_MENU_DISABLE_CONTROL)
	ENDIF
	
	CLEAR_BIT(thisHangar.sVehManageData.iManageVehMenuBS, MANAGE_VEH_MENU_DISPLAYING)
	CLEAR_BIT(thisHangar.sVehManageData.iManageVehMenuBS, MANAGE_VEH_MENU_SETUP)
	
	CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSFive, BS_SIMPLE_INTERIOR_GLOBAL_BS_FIVE_USING_VEHICLE_MANAGEMENT)
	
	IF DOES_CAM_EXIST(thisHangar.sVehManageData.camManage)
		RENDER_SCRIPT_CAMS(FALSE,FALSE)
		DESTROY_CAM(thisHangar.sVehManageData.camManage)
		PRINTLN("CLEANUP CAMERA")
	ENDIF
	
	DISPLAY_RADAR(TRUE)
ENDPROC

FUNC BOOL IS_PC_MENU_SCROLL_UP_PRESSED()

	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)

		IF IS_DISABLED_CONTROL_JUST_PRESSED( FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_UP )
			RETURN TRUE
		ENDIF

		IF g_iMenuCursorItem = MENU_CURSOR_SCROLL_UP
			IF IS_DISABLED_CONTROL_JUST_PRESSED( FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT) 
				RETURN TRUE
			ENDIF
		ENDIF
		
	ENDIF

	RETURN FALSE

ENDFUNC

FUNC BOOL IS_PC_MENU_SCROLL_DOWN_PRESSED()

	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)

		IF IS_DISABLED_CONTROL_JUST_PRESSED( FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_DOWN )
			RETURN TRUE
		ENDIF

		IF g_iMenuCursorItem = MENU_CURSOR_SCROLL_DOWN
			IF IS_DISABLED_CONTROL_JUST_PRESSED( FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT) 
				RETURN TRUE
			ENDIF
		ENDIF
		
	ENDIF

	RETURN FALSE

ENDFUNC

PROC HANDLE_VEHICLE_MANAGEMENT_MENU()
	BOOL bCursorAccept = FALSE
	BOOL bCursorSelect = FALSE
	BOOL bInputUsedThisFrame = FALSE
	
	HIDE_HELP_TEXT_THIS_FRAME()
	DISABLE_DPADDOWN_THIS_FRAME()
	DISABLE_SELECTOR_THIS_FRAME()
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_FRONTEND_CANCEL)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_FRONTEND_LEFT)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_FRONTEND_RIGHT)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_FRONTEND_UP)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_FRONTEND_DOWN)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_FRONTEND_X)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_NEXT_CAMERA)
	
	// ----------------------------------------------------------------------------------
	// PC mouse support

	HANDLE_CURSOR_HIGHLIGHTED_MENU_ITEMS()
	
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		
		IF IS_USING_CURSOR(FRONTEND_CONTROL)
			SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CURSOR_X)
			SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CURSOR_Y)
			SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
			SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CURSOR_CANCEL)
		ENDIF
		
		SET_MOUSE_CURSOR_THIS_FRAME()
	
		IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		
			IF IS_MENU_CURSOR_ACCEPT_PRESSED()
			
				IF g_iMenuCursorItem != thisHangar.sVehManageData.iCurVertSel
					thisHangar.sVehManageData.iCurVertSel = g_iMenuCursorItem
					SET_CURRENT_MENU_ITEM(thisHangar.sVehManageData.iCurVertSel)
				ELSE
					bCursorAccept = TRUE
				ENDIF
			
			ENDIF
			
		ENDIF
	
	ENDIF
	
	// -------------------------------------------------------------------------
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
	IF NOT IS_BIT_SET(thisHangar.sVehManageData.iManageVehMenuBS, MANAGE_VEH_MENU_DISABLE_CONTROL)
		CLEAR_PED_TASKS(PLAYER_PED_ID())
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		SET_BIT(thisHangar.sVehManageData.iManageVehMenuBS, MANAGE_VEH_MENU_DISABLE_CONTROL)
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(),PCF_UseKinematicModeWhenStationary,TRUE)
	ELIF IS_PLAYER_CONTROL_ON(PLAYER_ID())
	AND g_bForceControlOffForVehManagement
		CLEAR_BIT(thisHangar.sVehManageData.iManageVehMenuBS, MANAGE_VEH_MENU_DISABLE_CONTROL)
	ENDIF
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(),SCRIPT_TASK_ENTER_VEHICLE) = WAITING_TO_START_TASK
		OR GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(),SCRIPT_TASK_ENTER_VEHICLE) = PERFORMING_TASK
			CLEAR_PED_TASKS(PLAYER_PED_ID())
			PRINTLN("CDM: Clearing tasks trying to enter vehicle while on management menu")
		ELIF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID(),0,ECF_WARP_PED)
			PRINTLN("CDM: telling player to leave vehicle while on vehicle management menu")
			EXIT
		ENDIF
	ENDIF
	
	IF SHOULD_DISABLE_VEHICLE_MANAGEMENT()
		CLEANUP_VEHICLE_MANAGEMENT_MENU()
		EXIT
	ENDIF
	
	IF LOAD_MENU_ASSETS()
		
		DRAW_MARKERS_ABOVE_SELECTED_VEHICLES()
		IF NOT IS_BIT_SET(thisHangar.sVehManageData.iManageVehMenuBS, MANAGE_VEH_MENU_SETUP)
			CLEAR_HELP()
			SETUP_VEHICLE_MANAGEMENT_MENU()

			SET_BIT(thisHangar.sVehManageData.iManageVehMenuBS, MANAGE_VEH_MENU_SETUP)
			IF NOT IS_BIT_SET(thisHangar.sVehManageData.iManageVehMenuBS, MANAGE_VEH_MENU_DRAWING)
				
				PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FREEMODE_SOUNDSET")
				//PRINTLN("HANDLE_VEHICLE_MANAGEMENT_MENU: playing audio when menu first appears.")
			ENDIF
			DRAW_MENU_THIS_FRAME()
		ELSE
			IF IS_BIT_SET(thisHangar.sVehManageData.iManageVehMenuBS,MANAGE_VEH_MENU_PROCESS_REPLACE_TRANSACTION)
				IF NOT IS_BIT_SET(thisHangar.sVehManageData.iManageVehMenuBS,MANAGE_VEH_MENU_CREATED_TRANSACTION_VEHICLE)
					IF NOT DOES_ENTITY_EXIST(thisHangar.sVehManageData.vehTransaction)
						IF REQUEST_LOAD_MODEL(globalPropertyEntryData.replacingVehicle.vehicleSetupMP.VehicleSetup.eModel)
							thisHangar.sVehManageData.vehTransaction = CREATE_VEHICLE(globalPropertyEntryData.replacingVehicle.vehicleSetupMP.VehicleSetup.eModel,GET_ENTITY_COORDS(PLAYER_PED_ID())+<<0,0,-20>>,0,FALSE,FALSE)
							FREEZE_ENTITY_POSITION(thisHangar.sVehManageData.vehTransaction,TRUE)
							SET_VEHICLE_DOORS_LOCKED(thisHangar.sVehManageData.vehTransaction,VEHICLELOCK_CANNOT_ENTER)
							SET_VEHICLE_FULLBEAM(thisHangar.sVehManageData.vehTransaction, FALSE)
							SET_VEHICLE_LIGHTS(thisHangar.sVehManageData.vehTransaction,FORCE_VEHICLE_LIGHTS_OFF)	
							SET_VEHICLE_DIRT_LEVEL(thisHangar.sVehManageData.vehTransaction,0.0)
							SET_ENTITY_CAN_BE_DAMAGED(thisHangar.sVehManageData.vehTransaction,FALSE)
							IF IS_THIS_MODEL_A_HELI(globalPropertyEntryData.replacingVehicle.vehicleSetupMP.VehicleSetup.eModel)
								SET_HELI_BLADES_SPEED(thisHangar.sVehManageData.vehTransaction, 0)
							ENDIF	
							SET_VEHICLE_ENGINE_ON(thisHangar.sVehManageData.vehTransaction, FALSE, TRUE)
							SET_ENTITY_VISIBLE(thisHangar.sVehManageData.vehTransaction,FALSE) //hidden for test
						ENDIF
					ELSE
						IF IS_VEHICLE_DRIVEABLE(thisHangar.sVehManageData.vehTransaction)
							IF NOT IS_BIT_SET(thisHangar.sVehManageData.iManageVehMenuBS,MANAGE_VEH_MENU_APPLIED_DETAILS_TRANSACTION_VEHICLE)
								IF MP_SAVE_VEHICLE_APPLY_SAVED_DETAILS_TO_VEHICLE_INC_EMBLEM_USING_STRUCT(thisHangar.sVehManageData.vehTransaction,globalPropertyEntryData.replacingVehicle)//,bLoadingEmblem[i])
									SET_BIT(thisHangar.sVehManageData.iManageVehMenuBS,MANAGE_VEH_MENU_APPLIED_DETAILS_TRANSACTION_VEHICLE)								PRINTLN("TRANSACTION_VEHICLE: added all mods for car")
								#IF IS_DEBUG_BUILD
								ELSE
									PRINTLN("TRANSACTION_VEHICLE: waiting for applying details to car")
								#ENDIF
								ENDIF
							ELSE
								IF HAVE_VEHICLE_MODS_STREAMED_IN(thisHangar.sVehManageData.vehTransaction)
									SET_BIT(thisHangar.sVehManageData.iManageVehMenuBS,MANAGE_VEH_MENU_CREATED_TRANSACTION_VEHICLE)
									SET_MODEL_AS_NO_LONGER_NEEDED(globalPropertyEntryData.replacingVehicle.vehicleSetupMP.VehicleSetup.eModel)
									SET_CAN_USE_HYDRAULICS(thisHangar.sVehManageData.vehTransaction, FALSE)
									PRINTLN("TRANSACTION_VEHICLE: setting transaction vehicle can't use hydraulics")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					
					IF PROCESS_TRANSACTION_FOR_NEW_GARAGE_VEHICLE(thisHangar.sVehManageData.vehTransaction, thisHangar.sVehManageData.iSelectedVehSlot, thisHangar.sVehManageData.iNewCarTransactionResult)
						//SET_BIT(thisHangar.sVehManageData.iManageVehMenuBS,LOCAL_BS2_FINISHED_VEHICLE_TRANSACTION)
						//CLEAR_BIT(thisHangar.sVehManageData.iManageVehMenuBS,LOCAL_BS2_ENTERED_IN_NEW_VEHICLE)
						IF thisHangar.sVehManageData.iNewCarTransactionResult = GARAGE_VEHICLE_TRANSACTION_STATE_SUCCESS
							CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR: PROCESS_TRANSACTION_FOR_NEW_GARAGE_VEHICLE 2 - SUCCESS!")
						ELIF thisHangar.sVehManageData.iNewCarTransactionResult = GARAGE_VEHICLE_TRANSACTION_STATE_FAILED
							CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR: PROCESS_TRANSACTION_FOR_NEW_GARAGE_VEHICLE 2 - FAILED! Aborting")
							
							thisHangar.sVehManageData.iSelectedVehSlot = -1
							thisHangar.sVehManageData.iSelectedVehDisplaySlot = -1
							CLEAR_BIT(thisHangar.sVehManageData.iManageVehMenuBS, MANAGE_VEH_MENU_SETUP)
							CLEANUP_VEHICLE_MANAGEMENT_MENU()
							DELETE_VEHICLE(thisHangar.sVehManageData.vehTransaction)
							CLEAR_BIT(thisHangar.sVehManageData.iManageVehMenuBS,MANAGE_VEH_MENU_PROCESS_REPLACE_TRANSACTION)
							CLEAR_BIT(thisHangar.sVehManageData.iManageVehMenuBS,MANAGE_VEH_MENU_CREATED_TRANSACTION_VEHICLE)
							CLEAR_BIT(thisHangar.sVehManageData.iManageVehMenuBS,MANAGE_VEH_MENU_APPLIED_DETAILS_TRANSACTION_VEHICLE)
							
							EXIT
						ENDIF
						thisHangar.sVehManageData.iNewCarTransactionResult = 0
					ELSE
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR: waiting for PROCESS_TRANSACTION_FOR_NEW_GARAGE_VEHICLE 2")
						DRAW_MENU_THIS_FRAME()
						EXIT
					ENDIF

					COPY_MP_SAVE_VEHICLE_STUCT(globalPropertyEntryData.replacingVehicle, g_MpSavedVehicles[thisHangar.sVehManageData.iSelectedVehSlot])
					SET_BIT(g_MpSavedVehicles[thisHangar.sVehManageData.iSelectedVehSlot].iVehicleBS,MP_SAVED_VEHICLE_UPDATE_VEHICLE)	
					CLEAR_MP_VEHICLE_TEXT_MESSAGE_SLOT(thisHangar.sVehManageData.iSelectedVehSlot,TRUE)
					CV2_SET_VEHICLE_COUNT_NEEDS_REFRESHED()
					g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iCarsModifiedTimeStamp = GET_CLOUD_TIME_AS_INT() //1679280
					PRINTLN("CLEAR_MP_SAVED_VEHICLE_SLOT: updating save game time to be = ", g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iCarsModifiedTimeStamp)
					MP_SAVE_VEHICLE_SLOT_STATS_FROM_SAVEGAME(thisHangar.sVehManageData.iReplacedVehicle,g_MpSavedVehicles[thisHangar.sVehManageData.iReplacedVehicle],FALSE)
					
					
					// Kenneth R. - We need to make sure the app data gets updated too!
					INT iCarAppSlot
					IF globalPropertyEntryData.bCachedModInfo_Setup
					AND GET_CAR_APP_SLOT_FOR_SAVE_VEHICLE(thisHangar.sVehManageData.iSelectedVehSlot, iCarAppSlot)
						
						// Make sure this gets added to the app in DO_MAINTAIN_LAST_USED_SAVED_VEHICLES()
						g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedCarApp.iNewSavedVehToProcess = thisHangar.sVehManageData.iSelectedVehSlot+1
						
						// Game data
						SET_BIT(g_MpSavedVehicles[thisHangar.sVehManageData.iSelectedVehSlot].iVehicleBS, MP_SAVED_VEHICLE_CARAPP_SEND)
						CLEAR_BIT(g_MpSavedVehicles[thisHangar.sVehManageData.iSelectedVehSlot].iVehicleBS, MP_SAVED_VEHICLE_CARAPP_DELETE)
					ENDIF

					IF NOT IS_HANGAR_DISPLAY_SLOT_IN_STORAGE(thisHangar.sVehManageData.iSelectedVehDisplaySlot)
					AND GET_MODEL_HANGAR_SLOT_SIZE(g_MpSavedVehicles[thisHangar.sVehManageData.iSelectedVehSlot].vehicleSetupMP.VehicleSetup.eModel) != GET_DISPLAY_HANGAR_SLOT_SIZE(thisHangar.sVehManageData.iSelectedVehDisplaySlot)
						SET_HANGAR_DISPLAY_SLOT_IN_STORAGE(thisHangar.sVehManageData.iSelectedVehDisplaySlot, TRUE)
					ENDIF
					
					SET_BIT(thisHangar.sVehManageData.iManageVehCreateBS,MANAGE_VEH_CREATE_PENDING_FADE)
					
					//bShowReplaceVehicleOption = FALSE
					thisHangar.sVehManageData.iSelectedVehSlot = -1
					thisHangar.sVehManageData.iSelectedVehDisplaySlot = -1
					CLEAR_BIT(thisHangar.sVehManageData.iManageVehMenuBS, MANAGE_VEH_MENU_SETUP)
					CLEANUP_VEHICLE_MANAGEMENT_MENU()
					REQUEST_SAVE(SSR_REASON_VEH_STORAGE_UPDATE, STAT_SAVETYPE_IMMEDIATE_FLUSH)
					DELETE_VEHICLE(thisHangar.sVehManageData.vehTransaction)
					CLEAR_BIT(thisHangar.sVehManageData.iManageVehMenuBS,MANAGE_VEH_MENU_PROCESS_REPLACE_TRANSACTION)
					CLEAR_BIT(thisHangar.sVehManageData.iManageVehMenuBS,MANAGE_VEH_MENU_CREATED_TRANSACTION_VEHICLE)
					CLEAR_BIT(thisHangar.sVehManageData.iManageVehMenuBS,MANAGE_VEH_MENU_APPLIED_DETAILS_TRANSACTION_VEHICLE)
					EXIT
				ENDIF
			ELIF NOT IS_PAUSE_MENU_ACTIVE_EX()
			AND NOT IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
							
				//ACCEPT PRESSED		
								
				IF NOT IS_BIT_SET(thisHangar.sVehManageData.iButtonBS,PROP_BUTTON_BS_PRESSED_ACCEPT)
					IF (IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT) OR bCursorAccept)
					AND NOT bInputUsedThisFrame
						bInputUsedThisFrame = TRUE
						SET_BIT(thisHangar.sVehManageData.iButtonBS,PROP_BUTTON_BS_PRESSED_ACCEPT)
						
						IF IS_BIT_SET(thisHangar.sVehManageData.iButtonBS,PROP_BUTTON_BS_REPLACE_CAR_WARNING)
							NET_PRINT("HANDLE_VEHICLE_MANAGEMENT_MENU player exited menu") NET_NL()
							PLAY_SOUND_FRONTEND(-1, "CANCEL","HUD_FREEMODE_SOUNDSET")
							CLEAR_BIT(thisHangar.sVehManageData.iManageVehMenuBS, MANAGE_VEH_MENU_SETUP)
							CLEANUP_VEHICLE_MANAGEMENT_MENU()
							
							CLEAR_BIT(thisHangar.iBS, BS_HANGAR_DATA_ENTERED_IN_STORAGE)
							CLEAR_BIT(thisHangar.iBS, BS_HANGAR_DATA_ENTERED_IN_PEGASUS)
							
							EXIT
						ELIF NOT (thisHangar.sVehManageData.eMenuStage = VEHICLE_MANAGE_MENU_STAGE_MOVE AND thisHangar.sVehManageData.iSelectedVehSlot = thisHangar.sVehManageData.iVehicleSlotIDs[thisHangar.sVehManageData.iCurVertSel] AND NOT IS_HANGAR_DISPLAY_SLOT_IN_STORAGE(thisHangar.sVehManageData.iSelectedVehDisplaySlot) AND NOT IS_HANGAR_STORAGE_OPTION_SELECTED()) //same slot, do nothing
						AND NOT (thisHangar.sVehManageData.eMenuStage = VEHICLE_MANAGE_MENU_STAGE_SELECT AND thisHangar.sVehManageData.iVehicleSlotIDs[0] = -1) //no options, do nothing
						AND NOT (thisHangar.sVehManageData.eMenuStage = VEHICLE_MANAGE_MENU_STAGE_SELECT AND thisHangar.sVehManageData.iVehicleSlotIDs[thisHangar.sVehManageData.iCurVertSel] >= 0 AND IS_BIT_SET(g_MpSavedVehicles[thisHangar.sVehManageData.iVehicleSlotIDs[thisHangar.sVehManageData.iCurVertSel]].iVehicleBS, MP_SAVED_VEHICLE_DESTROYED)) //destroyed vehicle, do nothing
							PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET")
							
							INT iSlotBlockers, i
							
							SWITCH thisHangar.sVehManageData.eMenuStage
								CASE VEHICLE_MANAGE_MENU_STAGE_AREA
									IF thisHangar.sVehManageData.iCurVertSel = 1
										SET_BIT(thisHangar.sVehManageData.iManageVehMenuBS, MANAGE_VEH_MENU_AREA_STORAGE)
									ELSE
										CLEAR_BIT(thisHangar.sVehManageData.iManageVehMenuBS, MANAGE_VEH_MENU_AREA_STORAGE)
									ENDIF
									thisHangar.sVehManageData.eMenuStage = VEHICLE_MANAGE_MENU_STAGE_SELECT
									thisHangar.sVehManageData.iCurVertSel = 0
									CLEAR_BIT(thisHangar.sVehManageData.iManageVehMenuBS, MANAGE_VEH_MENU_SETUP)
								BREAK
								CASE VEHICLE_MANAGE_MENU_STAGE_SELECT
									thisHangar.sVehManageData.iSelectedVehSlot = thisHangar.sVehManageData.iVehicleSlotIDs[thisHangar.sVehManageData.iCurVertSel]
									thisHangar.sVehManageData.iSelectedVehDisplaySlot  = thisHangar.sVehManageData.iVehicleDisplaySlotIDs[thisHangar.sVehManageData.iCurVertSel]
									thisHangar.sVehManageData.eMenuStage = VEHICLE_MANAGE_MENU_STAGE_MOVE
									PRINTLN("HANDLE_VEHICLE_MANAGEMENT_MENU: player has selected slot #", thisHangar.sVehManageData.iSelectedVehSlot, " display slot #",thisHangar.sVehManageData.iSelectedVehDisplaySlot," A")
									REMOVE_MENU_HELP_KEYS()
									thisHangar.sVehManageData.iCurVertSel = 0
									CLEAR_BIT(thisHangar.sVehManageData.iManageVehMenuBS, MANAGE_VEH_MENU_SETUP)
								BREAK
								CASE VEHICLE_MANAGE_MENU_STAGE_MOVE
									IF NOT IS_BIT_SET(thisHangar.sVehManageData.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
										REMOVE_MENU_HELP_KEYS()
										ADD_MENU_HELP_KEY_CLICKABLE( INPUT_FRONTEND_ACCEPT, "BB_YES")
										ADD_MENU_HELP_KEY_CLICKABLE( INPUT_FRONTEND_CANCEL, "BB_NO")
										SET_BIT(thisHangar.sVehManageData.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
										PRINTLN("HANDLE_VEHICLE_MANAGEMENT_MENU: player has selected save slot #", thisHangar.sVehManageData.iSelectedVehSlot, " display slot #",thisHangar.sVehManageData.iSelectedVehDisplaySlot," B")
									ELSE
										IF IS_HANGAR_STORAGE_OPTION_SELECTED()
											SET_HANGAR_DISPLAY_SLOT_IN_STORAGE(thisHangar.sVehManageData.iSelectedVehDisplaySlot, TRUE)
											PRINTLN("HANDLE_VEHICLE_MANAGEMENT_MENU: player moving back to vehicle management options menu")
										ELSE
											MOVE_VEHICLES_BETWEEN_SLOTS(thisHangar.sVehManageData.iSelectedVehSlot,thisHangar.sVehManageData.iVehicleSlotIDs[thisHangar.sVehManageData.iCurVertSel],thisHangar.sVehManageData.iSelectedVehDisplaySlot,thisHangar.sVehManageData.iVehicleDisplaySlotIDs[thisHangar.sVehManageData.iCurVertSel])
											
										
											PRINTLN("HANDLE_VEHICLE_MANAGEMENT_MENU: player has choosen to move display slot #", thisHangar.sVehManageData.iSelectedVehDisplaySlot, " to display slot #", thisHangar.sVehManageData.iVehicleDisplaySlotIDs[thisHangar.sVehManageData.iCurVertSel])
											PRINTLN("HANDLE_VEHICLE_MANAGEMENT_MENU: player has choosen to move slot #", thisHangar.sVehManageData.iSelectedVehSlot, " to slot #", thisHangar.sVehManageData.iVehicleSlotIDs[thisHangar.sVehManageData.iCurVertSel])
											
											IF IS_HANGAR_DISPLAY_SLOT_IN_STORAGE(thisHangar.sVehManageData.iVehicleDisplaySlotIDs[thisHangar.sVehManageData.iCurVertSel])
											OR (thisHangar.sVehManageData.iVehicleSlotIDs[thisHangar.sVehManageData.iCurVertSel] > -1 AND IS_BIT_SET(g_MpSavedVehicles[thisHangar.sVehManageData.iVehicleSlotIDs[thisHangar.sVehManageData.iCurVertSel]].iVehicleBS, MP_SAVED_VEHICLE_DESTROYED))
												PRINTLN("HANDLE_VEHICLE_MANAGEMENT_MENU: Set old slot to storage as we're not swapping")
												SET_HANGAR_DISPLAY_SLOT_IN_STORAGE(thisHangar.sVehManageData.iSelectedVehDisplaySlot, TRUE)
											ENDIF
											
											SET_HANGAR_DISPLAY_SLOT_IN_STORAGE(thisHangar.sVehManageData.iVehicleDisplaySlotIDs[thisHangar.sVehManageData.iCurVertSel], FALSE)
											
											iSlotBlockers = GET_HANGAR_SLOT_BLOCKERS(thisHangar.sVehManageData.iVehicleDisplaySlotIDs[thisHangar.sVehManageData.iCurVertSel])
											IF iSlotBlockers > 0
												i = 0
												REPEAT MAX_DISPLAYED_VEHS_IN_HANGAR i
													IF IS_BIT_SET(iSlotBlockers, i)
														SET_HANGAR_DISPLAY_SLOT_IN_STORAGE(i + DISPLAY_SLOT_START_HANGAR, TRUE)
													ENDIF
												ENDREPEAT
											ENDIF
										ENDIF
										
										SET_BIT(thisHangar.sVehManageData.iManageVehCreateBS,MANAGE_VEH_CREATE_PENDING_FADE)
										
										thisHangar.sVehManageData.eMenuStage = VEHICLE_MANAGE_MENU_STAGE_AREA
										
										thisHangar.sVehManageData.iSelectedVehSlot = -1
										thisHangar.sVehManageData.iSelectedVehDisplaySlot = -1
										thisHangar.sVehManageData.iCurVertSel = 0
										
										CLEAR_BIT(thisHangar.sVehManageData.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
										CLEAR_BIT(thisHangar.sVehManageData.iManageVehMenuBS, MANAGE_VEH_MENU_SETUP)
									ENDIF
								BREAK
								CASE VEHICLE_MANAGE_MENU_STAGE_REPLACE
									IF NOT IS_BIT_SET(thisHangar.sVehManageData.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
										REMOVE_MENU_HELP_KEYS()
										ADD_MENU_HELP_KEY_CLICKABLE( INPUT_FRONTEND_ACCEPT, "BB_YES")
										ADD_MENU_HELP_KEY_CLICKABLE( INPUT_FRONTEND_CANCEL, "BB_NO")
										SET_BIT(thisHangar.sVehManageData.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
										thisHangar.sVehManageData.iSelectedVehSlot = thisHangar.sVehManageData.iVehicleSlotIDs[thisHangar.sVehManageData.iCurVertSel]
										thisHangar.sVehManageData.iSelectedVehDisplaySlot  = thisHangar.sVehManageData.iVehicleDisplaySlotIDs[thisHangar.sVehManageData.iCurVertSel]
			
										PRINTLN("HANDLE_VEHICLE_MANAGEMENT_MENU: player has selected save slot #", thisHangar.sVehManageData.iSelectedVehSlot, " display slot #",thisHangar.sVehManageData.iSelectedVehDisplaySlot," B")
									ELSE
										thisHangar.sVehManageData.iReplacedVehicle = thisHangar.sVehManageData.iSelectedVehSlot
										PRINTLN("HANDLE_VEHICLE_MANAGEMENT_MENU player trying to replace vehicle slot #",thisHangar.sVehManageData.iSelectedVehSlot)
										#IF IS_DEBUG_BUILD
										IF thisHangar.sVehManageData.iSelectedVehSlot >= 0
											IF IS_MODEL_IN_CDIMAGE(g_MpSavedVehicles[thisHangar.sVehManageData.iSelectedVehSlot].VehicleSetupMP.VehicleSetup.eModel)
												PRINTLN("HANDLE_VEHICLE_MANAGEMENT_MENU player just replaced a stored vehicle: Slot #",thisHangar.sVehManageData.iReplacedVehicle, " Vehicle Model = ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(g_MpSavedVehicles[thisHangar.sVehManageData.iSelectedVehSlot].vehicleSetupMP.VehicleSetup.eModel))
											ELSE
												PRINTLN("HANDLE_VEHICLE_MANAGEMENT_MENU player just replaced a stored vehicle: Slot #",thisHangar.sVehManageData.iReplacedVehicle)
											ENDIF
										ENDIF
										#ENDIF
										IF IS_PERSONAL_VEHICLE_DRIVEABLE_IN_FREEMODE()
											IF (CURRENT_SAVED_VEHICLE_SLOT() = thisHangar.sVehManageData.iSelectedVehSlot)
												NET_PRINT("HANDLE_VEHICLE_MANAGEMENT_MENU: player replaced out personal vehicle") NET_NL()
												CLEANUP_MP_SAVED_VEHICLE()	
											ENDIF
										ENDIF
										
										IF IS_BIT_SET(thisHangar.iBS, BS_HANGAR_DATA_ENTERED_IN_PEGASUS)
											PLAYSTATS_PEGASUS_AS_PERSONAL_AIRCRAFT(ENUM_TO_INT(g_MpSavedVehicles[thisHangar.sVehManageData.iSelectedVehSlot].vehicleSetupMP.VehicleSetup.eModel))
										ENDIF
										
										//not checking if new arrival is an already saved vehicle, as so far only the hangar can store aircraft so this never happens.
										
										IF USE_SERVER_TRANSACTIONS()
											SET_BIT(thisHangar.sVehManageData.iManageVehMenuBS,MANAGE_VEH_MENU_PROCESS_REPLACE_TRANSACTION)
											DRAW_MENU_THIS_FRAME()
											CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR: Player doing a replace transaction")
											EXIT
										ENDIF
										COPY_MP_SAVE_VEHICLE_STUCT(globalPropertyEntryData.replacingVehicle, g_MpSavedVehicles[thisHangar.sVehManageData.iSelectedVehSlot])
										SET_BIT(g_MpSavedVehicles[thisHangar.sVehManageData.iSelectedVehSlot].iVehicleBS,MP_SAVED_VEHICLE_UPDATE_VEHICLE)	
										CV2_SET_VEHICLE_COUNT_NEEDS_REFRESHED()
										CLEAR_MP_VEHICLE_TEXT_MESSAGE_SLOT(thisHangar.sVehManageData.iSelectedVehSlot,TRUE)
										g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iCarsModifiedTimeStamp = GET_CLOUD_TIME_AS_INT() //1679280
										PRINTLN("CLEAR_MP_SAVED_VEHICLE_SLOT: updating save game time to be = ", g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iCarsModifiedTimeStamp)
										MP_SAVE_VEHICLE_SLOT_STATS_FROM_SAVEGAME(thisHangar.sVehManageData.iReplacedVehicle,g_MpSavedVehicles[thisHangar.sVehManageData.iReplacedVehicle],FALSE)
										
										
										// Kenneth R. - We need to make sure the app data gets updated too!
										INT iCarAppSlot
										IF globalPropertyEntryData.bCachedModInfo_Setup
										AND GET_CAR_APP_SLOT_FOR_SAVE_VEHICLE(thisHangar.sVehManageData.iSelectedVehSlot, iCarAppSlot)
											
											// Make sure this gets added to the app in DO_MAINTAIN_LAST_USED_SAVED_VEHICLES()
											g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedCarApp.iNewSavedVehToProcess = thisHangar.sVehManageData.iSelectedVehSlot+1
											
											// Game data
											SET_BIT(g_MpSavedVehicles[thisHangar.sVehManageData.iSelectedVehSlot].iVehicleBS, MP_SAVED_VEHICLE_CARAPP_SEND)
											CLEAR_BIT(g_MpSavedVehicles[thisHangar.sVehManageData.iSelectedVehSlot].iVehicleBS, MP_SAVED_VEHICLE_CARAPP_DELETE)
										ENDIF

										IF NOT IS_HANGAR_DISPLAY_SLOT_IN_STORAGE(thisHangar.sVehManageData.iSelectedVehDisplaySlot)
										AND GET_MODEL_HANGAR_SLOT_SIZE(g_MpSavedVehicles[thisHangar.sVehManageData.iSelectedVehSlot].vehicleSetupMP.VehicleSetup.eModel) != GET_DISPLAY_HANGAR_SLOT_SIZE(thisHangar.sVehManageData.iSelectedVehDisplaySlot)
											SET_HANGAR_DISPLAY_SLOT_IN_STORAGE(thisHangar.sVehManageData.iSelectedVehDisplaySlot, TRUE)
										ENDIF
										
										SET_BIT(thisHangar.sVehManageData.iManageVehCreateBS,MANAGE_VEH_CREATE_PENDING_FADE)
										
										//bShowReplaceVehicleOption = FALSE
										thisHangar.sVehManageData.iSelectedVehSlot = -1
										thisHangar.sVehManageData.iSelectedVehDisplaySlot = -1
										CLEAR_BIT(thisHangar.sVehManageData.iManageVehMenuBS, MANAGE_VEH_MENU_SETUP)
										CLEANUP_VEHICLE_MANAGEMENT_MENU()
										REQUEST_SAVE(SSR_REASON_VEH_STORAGE_UPDATE, STAT_SAVETYPE_IMMEDIATE_FLUSH)
										EXIT
									ENDIF
								BREAK
							ENDSWITCH
						
						ENDIF
					ENDIF
				ELSE
					IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT)
						CLEAR_BIT(thisHangar.sVehManageData.iButtonBS,PROP_BUTTON_BS_PRESSED_ACCEPT)
					ENDIF
				ENDIF
				
				//CANCEL PRESSED
				
				IF NOT IS_BIT_SET(thisHangar.sVehManageData.iButtonBS,PROP_BUTTON_BS_PRESSED_CANCEL)
					IF IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_CANCEL)
					OR IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_CURSOR_CANCEL)
					AND NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT)
					AND NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_CURSOR_ACCEPT)
					AND NOT bInputUsedThisFrame
						bInputUsedThisFrame = TRUE
						IF thisHangar.sVehManageData.eMenuStage = VEHICLE_MANAGE_MENU_STAGE_MOVE
							thisHangar.sVehManageData.eMenuStage = VEHICLE_MANAGE_MENU_STAGE_SELECT
							thisHangar.sVehManageData.iSelectedVehSlot = -1
							thisHangar.sVehManageData.iCurVertSel = 0
							CLEAR_BIT(thisHangar.sVehManageData.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
							CLEAR_BIT(thisHangar.sVehManageData.iManageVehMenuBS, MANAGE_VEH_MENU_SETUP)
							PRINTLN("HANDLE_VEHICLE_MANAGEMENT_MENU: player moving back to vehicle management options menu")
							SET_BIT(thisHangar.sVehManageData.iButtonBS,PROP_BUTTON_BS_PRESSED_CANCEL)
						ELIF thisHangar.sVehManageData.eMenuStage = VEHICLE_MANAGE_MENU_STAGE_REPLACE
							IF IS_BIT_SET(thisHangar.sVehManageData.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
								thisHangar.sVehManageData.iSelectedVehSlot = -1
								CLEAR_BIT(thisHangar.sVehManageData.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
							ELIF IS_BIT_SET(thisHangar.sVehManageData.iButtonBS,PROP_BUTTON_BS_REPLACE_CAR_WARNING)
								CLEAR_BIT(thisHangar.sVehManageData.iButtonBS,PROP_BUTTON_BS_REPLACE_CAR_WARNING)
							ELSE
								SET_BIT(thisHangar.sVehManageData.iButtonBS,PROP_BUTTON_BS_REPLACE_CAR_WARNING)
							ENDIF
							SET_BIT(thisHangar.sVehManageData.iButtonBS,PROP_BUTTON_BS_PRESSED_CANCEL)
						ELIF NOT IS_BIT_SET(thisHangar.sVehManageData.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
							SET_BIT(thisHangar.sVehManageData.iButtonBS,PROP_BUTTON_BS_PRESSED_CANCEL)
							IF thisHangar.sVehManageData.eMenuStage = VEHICLE_MANAGE_MENU_STAGE_SELECT
								thisHangar.sVehManageData.eMenuStage = VEHICLE_MANAGE_MENU_STAGE_AREA
								thisHangar.sVehManageData.iCurVertSel = 0
								CLEAR_BIT(thisHangar.sVehManageData.iManageVehMenuBS, MANAGE_VEH_MENU_SETUP)
								PRINTLN("HANDLE_VEHICLE_MANAGEMENT_MENU: player moving back to vehicle management options menu")
							ELSE
								NET_PRINT("HANDLE_VEHICLE_MANAGEMENT_MENU player exited menu") NET_NL()
								PLAY_SOUND_FRONTEND(-1, "CANCEL","HUD_FREEMODE_SOUNDSET")
								CLEAR_BIT(thisHangar.sVehManageData.iManageVehMenuBS, MANAGE_VEH_MENU_SETUP)
								CLEANUP_VEHICLE_MANAGEMENT_MENU()
							EXIT
							ENDIF
						ENDIF
						
						//PROP_BUTTON_BS_REPLACE_CAR_WARNING
					ENDIF
				ELSE
					IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_CANCEL)
					AND NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_CURSOR_CANCEL)
						CLEAR_BIT(thisHangar.sVehManageData.iButtonBS,PROP_BUTTON_BS_PRESSED_CANCEL)
					ENDIF
				ENDIF
				
				//NEXT CAM PRESSED
				
				IF NOT IS_BIT_SET(thisHangar.sVehManageData.iButtonBS,PROP_BUTTON_BS_PRESSED_NEXT_CAM)
					IF IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_NEXT_CAMERA)
					AND NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_CANCEL)
					AND NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_CURSOR_CANCEL)
					AND NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT)
					AND NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_CURSOR_ACCEPT)
					AND NOT bInputUsedThisFrame
						bInputUsedThisFrame = TRUE
						
						thisHangar.sVehManageData.iManageCamera++
						IF thisHangar.sVehManageData.iManageCamera >= MANAGE_CAMERAS
							thisHangar.sVehManageData.iManageCamera -= MANAGE_CAMERAS
						ENDIF
						
						UPDATE_CAMERA()
						SET_BIT(thisHangar.sVehManageData.iButtonBS,PROP_BUTTON_BS_PRESSED_NEXT_CAM)
					ENDIF
				ELSE
					IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_NEXT_CAMERA)
					//AND NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_CURSOR_CANCEL)
						CLEAR_BIT(thisHangar.sVehManageData.iButtonBS,PROP_BUTTON_BS_PRESSED_NEXT_CAM)
					ENDIF
				ENDIF
				
				//UP/DOWN PRESSED
				
				INT iTempSelectionVert = thisHangar.sVehManageData.iCurVertSel
				
				IF thisHangar.sVehManageData.iMaxVertSel > 0
					IF NOT IS_BIT_SET(thisHangar.sVehManageData.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
						IF NOT IS_BIT_SET(thisHangar.sVehManageData.iButtonBS,PROP_BUTTON_BS_PRESSED_UP)
							IF IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_UP)
							OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_UP)
							OR IS_PC_MENU_SCROLL_UP_PRESSED()
							AND NOT bInputUsedThisFrame
								bInputUsedThisFrame = TRUE
								thisHangar.sVehManageData.iCurVertSel--
								PRINTLN("Pressed up")
								PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN","HUD_FREEMODE_SOUNDSET")
								SET_BIT(thisHangar.sVehManageData.iButtonBS,PROP_BUTTON_BS_PRESSED_UP)
								RESET_NET_TIMER(thisHangar.sVehManageData.menuNavigationDelay)
							ENDIF
						ELSE
							IF HANDLE_VEHICLE_MANAGEMENT_MENU_NAVIGATION_DELAY(INPUT_FRONTEND_UP)
							AND HANDLE_VEHICLE_MANAGEMENT_MENU_NAVIGATION_DELAY(INPUT_CURSOR_SCROLL_UP)
							AND NOT bCursorSelect

								CLEAR_BIT(thisHangar.sVehManageData.iButtonBS,PROP_BUTTON_BS_PRESSED_UP)	
							ENDIF
						ENDIF
						IF NOT IS_BIT_SET(thisHangar.sVehManageData.iButtonBS,PROP_BUTTON_BS_PRESSED_DOWN)
							IF IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_DOWN)
							OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_DOWN)
							OR IS_PC_MENU_SCROLL_DOWN_PRESSED()
							AND NOT bInputUsedThisFrame
								bInputUsedThisFrame = TRUE
								thisHangar.sVehManageData.iCurVertSel++
								PRINTLN("Pressed down")
								PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN","HUD_FREEMODE_SOUNDSET")
								SET_BIT(thisHangar.sVehManageData.iButtonBS,PROP_BUTTON_BS_PRESSED_DOWN)
								RESET_NET_TIMER(thisHangar.sVehManageData.menuNavigationDelay)
							ENDIF
						ELSE
							IF HANDLE_VEHICLE_MANAGEMENT_MENU_NAVIGATION_DELAY(INPUT_FRONTEND_DOWN)
							AND HANDLE_VEHICLE_MANAGEMENT_MENU_NAVIGATION_DELAY(INPUT_CURSOR_SCROLL_DOWN)
							AND NOT bCursorSelect
								CLEAR_BIT(thisHangar.sVehManageData.iButtonBS,PROP_BUTTON_BS_PRESSED_DOWN)	
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				//looping
				IF thisHangar.sVehManageData.iCurVertSel > thisHangar.sVehManageData.iMaxVertSel-1
					thisHangar.sVehManageData.iCurVertSel = 0
				ENDIF
				IF thisHangar.sVehManageData.iCurVertSel < 0
					thisHangar.sVehManageData.iCurVertSel = thisHangar.sVehManageData.iMaxVertSel -1
				ENDIF
			
				IF thisHangar.sVehManageData.iCurVertSel != iTempSelectionVert
					PRINTLN("Updating menu vertical selection changed to ", thisHangar.sVehManageData.iCurVertSel)
					CLEAR_BIT(thisHangar.sVehManageData.iManageVehMenuBS, MANAGE_VEH_MENU_SETUP)
				ENDIF
				
				//display car warning leave message
				IF IS_BIT_SET(thisHangar.sVehManageData.iButtonBS,PROP_BUTTON_BS_REPLACE_CAR_WARNING)
					SET_WARNING_MESSAGE_WITH_HEADER("VEH_REP_WARN","MP_MAN_VEH6ac",FE_WARNING_OKCANCEL)
				ELSE
					DRAW_MENU_THIS_FRAME()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC VECTOR GET_HANGAR_MANAGEMENT_LOCATE()
	RETURN <<-1252.86, -3049.3758, -49.4897>>
ENDFUNC

PROC MAINTAIN_VEHICLE_MANAGEMENT()
	
	//PRINTLN("MAINTAIN_VEHICLE_MANAGEMENT: called this frame")

	IF IS_BIT_SET(thisHangar.sVehManageData.iManageVehMenuBS, MANAGE_VEH_MENU_DRAWING_LAST)
		SET_BIT(thisHangar.sVehManageData.iManageVehMenuBS, MANAGE_VEH_MENU_DRAWING)
	ELSE
		CLEAR_BIT(thisHangar.sVehManageData.iManageVehMenuBS, MANAGE_VEH_MENU_DRAWING)
	ENDIF
	CLEAR_BIT(thisHangar.sVehManageData.iManageVehMenuBS, MANAGE_VEH_MENU_DRAWING_LAST)
	
	IF IS_BIT_SET(thisHangar.sVehManageData.iManageVehMenuBS, MANAGE_VEH_MENU_DISPLAYING)
		HANDLE_VEHICLE_MANAGEMENT_MENU()
	ELSE
		IF IS_NET_PLAYER_OK(PLAYER_ID())
			FLOAT fSize = 0.7
		
			//IF NOT IS_POSITION_OCCUPIED(GET_HANGAR_MANAGEMENT_LOCATE(),fSize,FALSE,TRUE,FALSE,FALSE,FALSE) - titan can block this even though there's plenty of space
				IF NOT SHOULD_DISABLE_VEHICLE_MANAGEMENT()
					INT iR, iG, iB, iA
					GET_HUD_COLOUR(HUD_COLOUR_BLUE,iR, iG, iB, iA)
					DRAW_MARKER(MARKER_CYLINDER, GET_HANGAR_MANAGEMENT_LOCATE() + <<0,0,-0.25>>, <<0,0,0>>, <<0, 0, 0>>, <<0.75, 0.75, 0.75>>, iR, iG, iB, 255)
					
					IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),GET_HANGAR_MANAGEMENT_LOCATE(),<<fSize, fSize, 2>>,FALSE,TRUE,TM_ON_FOOT)
					AND NOT IS_PLAYER_IN_CORONA()
					AND NOT IS_ANY_TRANSITION_SESSION_INVITE_BEING_ACTIONED()
					
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
						IF thisHangar.sVehManageData.iManageVehicleContext = NEW_CONTEXT_INTENTION
							REGISTER_CONTEXT_INTENTION(thisHangar.sVehManageData.iManageVehicleContext,CP_MEDIUM_PRIORITY,"MP_MAN_VEHac")
							PRINTLN("MAINTAIN_VEHICLE_MANAGEMENT: registering context intention")
						ELSE
						
							IF HAS_CONTEXT_BUTTON_TRIGGERED(thisHangar.sVehManageData.iManageVehicleContext)
								SET_BIT(thisHangar.sVehManageData.iButtonBS,PROP_BUTTON_BS_PRESSED_RIGHT)
								SET_BIT(thisHangar.sVehManageData.iButtonBS,PROP_BUTTON_BS_PRESSED_LEFT)
								SET_BIT(thisHangar.sVehManageData.iButtonBS,PROP_BUTTON_BS_PRESSED_UP)
								SET_BIT(thisHangar.sVehManageData.iButtonBS,PROP_BUTTON_BS_PRESSED_DOWN)
								SET_BIT(thisHangar.sVehManageData.iButtonBS,PROP_BUTTON_BS_PRESSED_ACCEPT)
								SET_BIT(thisHangar.sVehManageData.iButtonBS,PROP_BUTTON_BS_PRESSED_CANCEL)
								
								SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSFive, BS_SIMPLE_INTERIOR_GLOBAL_BS_FIVE_USING_VEHICLE_MANAGEMENT)
								
								RESET_NET_TIMER(thisHangar.sVehManageData.menuNavigationDelay)
								SET_BIT(thisHangar.sVehManageData.iManageVehMenuBS, MANAGE_VEH_MENU_DISPLAYING)
								thisHangar.sVehManageData.eMenuStage = VEHICLE_MANAGE_MENU_STAGE_AREA
								RELEASE_CONTEXT_INTENTION(thisHangar.sVehManageData.iManageVehicleContext)
								UPDATE_CAMERA()
								PRINTLN("MAINTAIN_VEHICLE_MANAGEMENT: releasing context intention. Player selected to use management menu.")
							ENDIF
						ENDIF
					ELSE
						IF thisHangar.sVehManageData.iManageVehicleContext != NEW_CONTEXT_INTENTION
							RELEASE_CONTEXT_INTENTION(thisHangar.sVehManageData.iManageVehicleContext)
							PRINTLN("MAINTAIN_VEHICLE_MANAGEMENT: releasing context intention-1")
						ENDIF
					ENDIF
				ELSE
					IF thisHangar.sVehManageData.iManageVehicleContext != NEW_CONTEXT_INTENTION
						RELEASE_CONTEXT_INTENTION(thisHangar.sVehManageData.iManageVehicleContext)
						PRINTLN("MAINTAIN_VEHICLE_MANAGEMENT: releasing context intention ShouldVehicleManagementBeDisabled")
					ENDIF
				ENDIF
//			ELSE
//				PRINTLN("MAINTAIN_VEHICLE_MANAGEMENT: releasing context intention-3")
//				IF thisHangar.sVehManageData.iManageVehicleContext != NEW_CONTEXT_INTENTION
//					RELEASE_CONTEXT_INTENTION(thisHangar.sVehManageData.iManageVehicleContext)
//					PRINTLN("MAINTAIN_VEHICLE_MANAGEMENT: releasing context intention-3")
//				ENDIF
//			ENDIF
		ELSE
			IF thisHangar.sVehManageData.iManageVehicleContext != NEW_CONTEXT_INTENTION
				RELEASE_CONTEXT_INTENTION(thisHangar.sVehManageData.iManageVehicleContext)
				PRINTLN("MAINTAIN_VEHICLE_MANAGEMENT: releasing context intention-2")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC INITIALISE_HANGAR_VEHICLE_MANAGEMENT_ENTRY()
	IF globalPropertyEntryData.bChoosenToReplaceVehicle
	AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
		SET_REPLACING_VEHICLE_ON_ENTRY(FALSE)
		thisHangar.sVehManageData.eMenuStage = VEHICLE_MANAGE_MENU_STAGE_REPLACE
		SET_BIT(thisHangar.sVehManageData.iButtonBS,PROP_BUTTON_BS_PRESSED_RIGHT)
		SET_BIT(thisHangar.sVehManageData.iButtonBS,PROP_BUTTON_BS_PRESSED_LEFT)
		SET_BIT(thisHangar.sVehManageData.iButtonBS,PROP_BUTTON_BS_PRESSED_UP)
		SET_BIT(thisHangar.sVehManageData.iButtonBS,PROP_BUTTON_BS_PRESSED_DOWN)
		SET_BIT(thisHangar.sVehManageData.iButtonBS,PROP_BUTTON_BS_PRESSED_ACCEPT)
		SET_BIT(thisHangar.sVehManageData.iButtonBS,PROP_BUTTON_BS_PRESSED_CANCEL)
		RESET_NET_TIMER(thisHangar.sVehManageData.menuNavigationDelay)
		SET_BIT(thisHangar.sVehManageData.iManageVehMenuBS, MANAGE_VEH_MENU_DISPLAYING)
		UPDATE_CAMERA()
		
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR: Choosen to replace a vehicle from exterior")
	ENDIF
ENDPROC

FUNC BOOL CREATE_VEHICLE_MANAGEMENT_PROP()
	IF DOES_ENTITY_EXIST(thisHangar.sVehManageData.objProp)
		RETURN TRUE
	ENDIF
	
	REQUEST_MODEL(HEI_PROP_HEI_SECURITYPANEL)
	IF HAS_MODEL_LOADED(HEI_PROP_HEI_SECURITYPANEL)
		thisHangar.sVehManageData.objProp = CREATE_OBJECT(HEI_PROP_HEI_SECURITYPANEL, <<-1252.86, -3049.79, -48.32>>, FALSE, FALSE)
		SET_ENTITY_ROTATION(thisHangar.sVehManageData.objProp, <<0.00, -0.00, -180.00>>)
		SET_ENTITY_LOD_DIST(thisHangar.sVehManageData.objProp, 300)
		SET_MODEL_AS_NO_LONGER_NEEDED(HEI_PROP_HEI_SECURITYPANEL)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC CLEANUP_VEHICLE_MANAGEMENT_PROP()
	IF DOES_ENTITY_EXIST(thisHangar.sVehManageData.objProp)
		DELETE_OBJECT(thisHangar.sVehManageData.objProp)
	ENDIF
ENDPROC

//DEBUG
#IF IS_DEBUG_BUILD

CONST_INT AIRCRAFT_MODEL_NUM		45

FUNC MODEL_NAMES GET_AIRCRAFT_MODEL_FROM_DEBUG_ID(INT id)
	SWITCH id
		CASE 0 RETURN PYRO
		CASE 1 RETURN TULA
		CASE 2 RETURN TITAN
		CASE 3 RETURN MICROLIGHT
		CASE 4 RETURN HAVOK
		CASE 5 RETURN SEABREEZE
		CASE 6 RETURN ROGUE
		CASE 7 RETURN BUZZARD
		CASE 8 RETURN FROGGER
		CASE 9 RETURN MAVERICK
		CASE 10 RETURN SUPERVOLITO
		CASE 11 RETURN SUPERVOLITO2
		CASE 12 RETURN VOLATUS
		CASE 13 RETURN CUBAN800
		CASE 14 RETURN BESRA
		CASE 15 RETURN DUSTER
		CASE 16 RETURN STUNT
		CASE 17 RETURN VELUM
		CASE 18 RETURN VELUM2
		CASE 19 RETURN VESTRA
		CASE 20 RETURN LAZER
		CASE 21 RETURN ALPHAZ1
		CASE 22 RETURN HOWARD
		CASE 23 RETURN STARLING
		CASE 24 RETURN HYDRA
		CASE 25 RETURN DODO
		CASE 26 RETURN MAMMATUS
		CASE 27 RETURN ANNIHILATOR
		CASE 28 RETURN HUNTER
		CASE 29 RETURN MOGUL
		CASE 30 RETURN CARGOBOB
		CASE 31 RETURN CARGOBOB2
		CASE 32 RETURN SHAMAL
		CASE 33 RETURN SAVAGE
		CASE 34 RETURN LUXOR
		CASE 35 RETURN LUXOR2
		CASE 36 RETURN NIMBUS
		CASE 37 RETURN SWIFT
		CASE 38 RETURN SWIFT2
		CASE 39 RETURN VALKYRIE
		CASE 40 RETURN SKYLIFT
		CASE 41 RETURN MILJET
		CASE 42 RETURN BOMBUSHKA
		CASE 43 RETURN NOKOTA
		CASE 44 RETURN MOLOTOK
	ENDSWITCH
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

PROC VEHICLE_MANAGEMENT_CREATE_DEBUG_WIDGETS()
	START_WIDGET_GROUP("Spawn Aircraft")
		START_NEW_WIDGET_COMBO()
			INT i
			REPEAT AIRCRAFT_MODEL_NUM i
				ADD_TO_WIDGET_COMBO(GET_MODEL_NAME_FOR_DEBUG(GET_AIRCRAFT_MODEL_FROM_DEBUG_ID(i)))
			ENDREPEAT
		STOP_WIDGET_COMBO("Model to create: ", debugData.iCreateAircraftModelID)
		ADD_WIDGET_BOOL("Create Test Aircraft", debugData.bCreateTestAircraft)
		ADD_WIDGET_BOOL("Clear Aircraft", debugData.bClearAircraft)
	STOP_WIDGET_GROUP()
ENDPROC


PROC VEHICLE_MANAGEMENT_MAINTAIN_DEBUG_WIDGETS()
	IF thisHangar.pOwner = PLAYER_ID()
		IF debugData.bCreateTestAircraft
			//get slot
			IF debugData.iCreateAircraftSlot < 0
				INT i, iDisplaySlot, iSaveSlot, iPass
				REPEAT 2 iPass
					REPEAT MAX_SAVED_VEHS_IN_HANGAR i
						iDisplaySlot = i + DISPLAY_SLOT_START_HANGAR
						IF (GET_DISPLAY_HANGAR_SLOT_SIZE(iDisplaySlot) = GET_MODEL_HANGAR_SLOT_SIZE(GET_AIRCRAFT_MODEL_FROM_DEBUG_ID(debugData.iCreateAircraftModelID))
						AND GET_HANGAR_SLOT_BLOCKERS(iDisplaySlot) = 0)
						OR iPass = 1
							MPSV_GET_SAVE_SLOT_FROM_DISPLAY_SLOT(iDisplaySlot, iSaveSlot)
							IF iSaveSlot < 0
							OR g_MpSavedVehicles[iSaveSlot].vehicleSetupMP.VehicleSetup.eModel = DUMMY_MODEL_FOR_SCRIPT
								debugData.iCreateAircraftDisplaySlot = iDisplaySlot
								IF iSaveSlot < 0
									debugData.iCreateAircraftSlot = MP_SAVE_VEHICLE_GET_EMPTY_SAVE_SLOT(MP_SAVE_VEH_SEARCH_EMPTY_FIRST, FALSE, GET_AIRCRAFT_MODEL_FROM_DEBUG_ID(debugData.iCreateAircraftModelID))
								ELSE
									debugData.iCreateAircraftSlot = iSaveSlot
								ENDIF
								debugData.bCreateInStorage = (iPass = 1)
								BREAKLOOP
							ENDIF
						ENDIF
					ENDREPEAT
					IF debugData.iCreateAircraftSlot >= 0
						BREAKLOOP
					ENDIF
				ENDREPEAT
			ENDIF
			IF debugData.iCreateAircraftSlot >= 0
				//create vehicle
				IF NOT DOES_ENTITY_EXIST(debugData.vehTemp)
					REQUEST_MODEL(GET_AIRCRAFT_MODEL_FROM_DEBUG_ID(debugData.iCreateAircraftModelID))
				ENDIF
				IF DOES_ENTITY_EXIST(debugData.vehTemp)
				OR HAS_MODEL_LOADED(GET_AIRCRAFT_MODEL_FROM_DEBUG_ID(debugData.iCreateAircraftModelID))	
					IF NOT DOES_ENTITY_EXIST(debugData.vehTemp)
						debugData.vehTemp = CREATE_VEHICLE(GET_AIRCRAFT_MODEL_FROM_DEBUG_ID(debugData.iCreateAircraftModelID),  <<-1035.6709, 4206.5659, 117.5565>>,0.0, FALSE, FALSE)
						SET_MODEL_AS_NO_LONGER_NEEDED(GET_AIRCRAFT_MODEL_FROM_DEBUG_ID(debugData.iCreateAircraftModelID))
					ENDIF
					
					PRINTLN("DEBUG_SPAWN_AIRCRAFT: Creating ", GET_MODEL_NAME_FOR_DEBUG(GET_AIRCRAFT_MODEL_FROM_DEBUG_ID(debugData.iCreateAircraftModelID)), " in display slot #", debugData.iCreateAircraftDisplaySlot, ", save slot #", debugData.iCreateAircraftSlot, ".")
					
					IF NOT DEBUG_ADD_VEHICLE_TO_GARAGE(debugData.vehTemp, debugData.iCreateAircraftSlot, debugData.iCreateAircraftTransaction, DEFAULT, debugData.iCreateAircraftDisplaySlot)
						EXIT
					ENDIF
									
					INSURE_MP_SAVE_VEHICLE_AND_SET_FLAG(debugData.vehTemp, debugData.iCreateAircraftSlot)
					
					DELETE_VEHICLE(debugData.vehTemp)
					
					SET_HANGAR_DISPLAY_SLOT_IN_STORAGE(debugData.iCreateAircraftDisplaySlot, debugData.bCreateInStorage)
					
					debugData.bCreateTestAircraft = FALSE
				ENDIF
			ELSE
				debugData.bCreateTestAircraft = FALSE
			ENDIF
		ELIF debugData.bClearAircraft
			IF debugData.iCreateAircraftSlot < 0
				INT i, iDisplaySlot, iSaveSlot
				REPEAT MAX_SAVED_VEHS_IN_HANGAR i
					iDisplaySlot = i + DISPLAY_SLOT_START_HANGAR
					MPSV_GET_SAVE_SLOT_FROM_DISPLAY_SLOT(iDisplaySlot, iSaveSlot)
					IF iSaveSlot >= 0
					AND g_MpSavedVehicles[iSaveSlot].vehicleSetupMP.VehicleSetup.eModel != DUMMY_MODEL_FOR_SCRIPT
						debugData.iCreateAircraftSlot = iSaveSlot
						debugData.iCreateAircraftDisplaySlot = iDisplaySlot
					ENDIF
				ENDREPEAT
			ENDIF
			IF debugData.iCreateAircraftSlot >= 0
				PRINTLN("DEBUG_SPAWN_AIRCRAFT: Removing ", GET_MODEL_NAME_FOR_DEBUG(g_MpSavedVehicles[debugData.iCreateAircraftSlot].vehicleSetupMP.VehicleSetup.eModel), " in display slot #", debugData.iCreateAircraftDisplaySlot, ", save slot #", debugData.iCreateAircraftSlot, ".")
				IF NOT DEBUG_REMOVE_VEHICLE_FROM_GARAGE(debugData.iCreateAircraftSlot, debugData.iCreateAircraftTransaction, TRUE)
					EXIT
				ENDIF
				debugData.iCreateAircraftSlot = -1
			ELSE
				debugData.bClearAircraft = FALSE
			ENDIF
		ELSE
			debugData.iCreateAircraftSlot = -1
		ENDIF
	ENDIF
ENDPROC

#ENDIF


//╒═════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════╡ HANGAR PICKUPS ╞═══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC GET_HANGAR_WEAPON_PICKUP_DATA(INT iHangarPickup, PICKUP_TYPE &ePickupType, VECTOR &vPos, VECTOR &vRot, INT &iCount)
	SWITCH iHangarPickup
		CASE 0
			vPos = <<-1243.9270, -3018.9832, -43.7918 + 0.3>>
			vRot = << 0.0, 0.0, 0.0 >>
			ePickupType = PICKUP_PARACHUTE
		BREAK
		CASE 1
			vPos = <<-1259.7783, -2961.7976, -30.6420>>
			vRot = << 0.0, 0.0, 0.0 >>
			ePickupType = PICKUP_WEAPON_DLC_HOMINGLAUNCHER
			iCount = (GET_MAX_AMMO_IN_CLIP(PLAYER_PED_ID(), WEAPONTYPE_DLC_HOMINGLAUNCHER, FALSE) * 2)
		BREAK
		CASE 2
			vPos = <<-1266.6228, -3047.5740, -38.1578>>
			vRot = << 0.0, 0.0, 0.0 >>
			ePickupType = PICKUP_WEAPON_HEAVYSNIPER
			iCount = (GET_MAX_AMMO_IN_CLIP(PLAYER_PED_ID(), WEAPONTYPE_HEAVYSNIPER, FALSE) * 2)
		BREAK
	ENDSWITCH
ENDPROC

PROC MAINTAIN_HANGAR_PICKUPS()

	INT iHangarPickup
	PICKUP_TYPE ePickupType
	VECTOR vPos, vRot
	INT iCount
	
	REPEAT MAX_NUM_HANGAR_WEAPON_PICKUPS iHangarPickup
		IF DOES_PICKUP_EXIST(g_pickupHangarWeapon[iHangarPickup])
			IF HAS_PICKUP_BEEN_COLLECTED(g_pickupHangarWeapon[iHangarPickup])
				g_iLastHangarWeaponCreateTime[iHangarPickup] = GET_CLOUD_TIME_AS_INT()
				REMOVE_PICKUP(g_pickupHangarWeapon[iHangarPickup])
				g_pickupHangarWeapon[iHangarPickup] = NULL
				g_bLastHangarWeaponPickedUp[iHangarPickup] = TRUE
			ELSE
			
				IF NOT IS_BIT_SET(thisHangar.iHangarPickupFlags[iHangarPickup], 1) // Safe to collect?
					// remove so we can re-create
					REMOVE_PICKUP(g_pickupHangarWeapon[iHangarPickup])
					g_iLastHangarWeaponCreateTime[iHangarPickup] = 0
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(thisHangar.iHangarPickupFlags[iHangarPickup], 0) // First weapon processed
			IF NOT g_bLastHangarWeaponPickedUp[iHangarPickup]
				g_iLastHangarWeaponCreateTime[iHangarPickup] = 0
			ENDIF
			SET_BIT(thisHangar.iHangarPickupFlags[iHangarPickup], 0) // First weapon processed
		ENDIF
		
		IF (GET_CLOUD_TIME_AS_INT() - g_iLastHangarWeaponCreateTime[iHangarPickup]) > 6 // 10 minutes
			IF DOES_PICKUP_EXIST(g_pickupHangarWeapon[iHangarPickup])
				g_iLastHangarWeaponCreateTime[iHangarPickup] = GET_CLOUD_TIME_AS_INT()-3 // Check again in 5 minutes
				
			ELSE
				g_iLastHangarWeaponCreateTime[iHangarPickup] = GET_CLOUD_TIME_AS_INT()
				
				INT iPlacementFlags = 0
				
				SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ROTATE))
				SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_LOCAL_ONLY))
		  		SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ORIENT_TO_GROUND))
				
				GET_HANGAR_WEAPON_PICKUP_DATA(iHangarPickup, ePickupType, vPos, vRot, iCount)
				g_pickupHangarWeapon[iHangarPickup] = CREATE_PICKUP_ROTATE(ePickupType, vPos+<<0.0,0.0,0.3>>, vRot, iPlacementFlags, iCount, DEFAULT, FALSE)
				PRINTLN("MAINTAIN_HANGAR_PICKUPS - Creating pickup ", iHangarPickup)
				
				g_bLastHangarWeaponPickedUp[iHangarPickup] = FALSE
				
				SET_BIT(thisHangar.iHangarPickupFlags[iHangarPickup], 1) // Safe to collect?
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

FUNC BOOL IS_HANGAR_OWNER_WATCHING_INTRO_MOCAP()
	IF thisHangar.pOwner != INVALID_PLAYER_INDEX()
	AND NETWORK_IS_PLAYER_ACTIVE(thisHangar.pOwner)
		IF IS_BIT_SET(playerBD[NATIVE_TO_INT(thisHangar.pOwner)].iBS, BS_HANGAR_WATCHING_INTRO_MOCAP)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL AM_I_WALKING_INTO_HANGAR_WITH_OWNER()
	IF thisHangar.pOwner != INVALID_PLAYER_INDEX()
		IF thisHangar.pOwner = PLAYER_ID()
			
			//Are we walking into this bunker?
			RETURN IS_LOCAL_PLAYER_WALKING_INTO_THIS_SIMPLE_INTERIOR(thisHangar.eSimpleInteriorID, TRUE)
			
		ELSE
			
			//Are we both entering the simple interior
			IF IS_PLAYER_WALKING_INTO_SIMPLE_INTERIOR(PLAYER_ID())
			AND IS_PLAYER_WALKING_INTO_SIMPLE_INTERIOR(thisHangar.pOwner)
				
				//Are we entering the same simple interior
				RETURN GET_SIMPLE_INTERIOR_PLAYER_IS_WALKING_IN_OR_OUT_FROM(PLAYER_ID()) = GET_SIMPLE_INTERIOR_PLAYER_IS_WALKING_IN_OR_OUT_FROM(thisHangar.pOwner)
				
			ENDIF
			
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ HANGAR TV ╞═════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC MAINTAIN_HANGAR_MP_TV_CLIENT()
	INT iPlayer = NATIVE_TO_INT(PLAYER_ID())
	
	IF NOT IS_PLAYER_HANGAR_FURNITURE_TRADITIONAL_PURCHASED(thisHangar.pOwner)
	AND NOT IS_PLAYER_HANGAR_FURNITURE_MODERN_PURCHASED(thisHangar.pOwner)
		EXIT
	ENDIF
	
	IF NOT IS_HANGAR_STATE(HANGAR_STATE_IDLE)
		IF thisHangar.pOwner = PLAYER_ID()
		AND NOT HAS_LOCAL_PLAYER_VIEWED_INTERIOR_CUTSCENE(biFmCut_Hangar_Intro_Cutscene)
			PRINTLN("MAINTAIN_HANGAR_MP_TV_CLIENT: aborting until intro is done")
			EXIT
		ELIF AM_I_WALKING_INTO_HANGAR_WITH_OWNER()
		AND IS_HANGAR_OWNER_WATCHING_INTRO_MOCAP()
			PRINTLN("MAINTAIN_HANGAR_MP_TV_CLIENT: aborting until Owner intro is done")
			EXIT
		ENDIF
	ENDIF
	
	IF thisHangar.activityTVSeatStruct.officeSeatActState = OFFICE_SEAT_CHECK_BASE_ANIM
	OR thisHangar.activityTVSeatStruct.officeSeatActState = OFFICE_SEAT_CHECK_ENTER_OR_IDLE_ANIM
		//enter seat - start seat logic for MP_TV
		IF NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].MPTVClient.iBitset, MPTV_CLIENT_BS_OCCUPYING_SEAT)
			SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].MPTVClient.iBitset, MPTV_CLIENT_BS_OCCUPYING_SEAT)
			CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].MPTVClient.iBitset, MPTV_CLIENT_BS_LEAVING_SEAT)
			playerBD[NATIVE_TO_INT(PLAYER_ID())].MPTVClient.iSeat = thisHangar.activityTVSeatStruct.iPlayerChosenSeatSlot
			SET_MP_TV_CLIENT_STAGE(playerBD[NATIVE_TO_INT(PLAYER_ID())].MPTVClient, MP_TV_CLIENT_STAGE_SITTING)
		ELSE
			//activate TV - let office seat activity know so player can't leave seat
			IF playerBD[NATIVE_TO_INT(PLAYER_ID())].MPTVClient.eStage = MP_TV_CLIENT_STAGE_ACTIVATED
			AND NOT IS_BIT_SET(thisHangar.activityTVSeatStruct.iOfficeSeatiBitSet , OFFICE_SEAT_BS_ACTIVE_TV) 
				SET_BIT(thisHangar.activityTVSeatStruct.iOfficeSeatiBitSet , OFFICE_SEAT_BS_ACTIVE_TV)
			ELIF playerBD[NATIVE_TO_INT(PLAYER_ID())].MPTVClient.eStage != MP_TV_CLIENT_STAGE_ACTIVATED
			AND IS_BIT_SET(thisHangar.activityTVSeatStruct.iOfficeSeatiBitSet , OFFICE_SEAT_BS_ACTIVE_TV) 
				CLEAR_BIT(thisHangar.activityTVSeatStruct.iOfficeSeatiBitSet , OFFICE_SEAT_BS_ACTIVE_TV)
			ENDIF
		ENDIF
	ELSE
		//exit seat - stop seat logic for MP_TV
		IF IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].MPTVClient.iBitset, MPTV_CLIENT_BS_OCCUPYING_SEAT)
			CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].MPTVClient.iBitset, MPTV_CLIENT_BS_OCCUPYING_SEAT)
			CLEAR_BIT(thisHangar.activityTVSeatStruct.iOfficeSeatiBitSet , OFFICE_SEAT_BS_ACTIVE_TV)
			playerBD[NATIVE_TO_INT(PLAYER_ID())].MPTVClient.iSeat = -1
			SET_MP_TV_CLIENT_STAGE(playerBD[NATIVE_TO_INT(PLAYER_ID())].MPTVClient, MP_TV_CLIENT_STAGE_WALKING)
		ENDIF
	ENDIF

	IF iPlayer <> -1			
	AND IS_PLAYER_IN_HANGAR(PLAYER_ID())
		CLIENT_MAINTAIN_MP_TV(serverBD.MPTVServer, playerBD[iPlayer].MPTVClient, thisHangar.MPTVLocal, InteriorPropStruct)	
	ENDIF
		
ENDPROC

PROC MAINTAIN_HANGAR_MP_TV_SERVER()
	
	IF NOT IS_PLAYER_HANGAR_FURNITURE_TRADITIONAL_PURCHASED(thisHangar.pOwner)
	AND NOT IS_PLAYER_HANGAR_FURNITURE_MODERN_PURCHASED(thisHangar.pOwner)
		EXIT
	ENDIF
	
	IF IS_PLAYER_IN_HANGAR(PLAYER_ID())
		SERVER_MAINTAIN_MP_TV(serverBD.MPTVServer, playerBD[thisHangar.MPTVLocal.iServerPlayerStagger].MPTVClient, thisHangar.MPTVLocal, InteriorPropStruct)
	ENDIF
ENDPROC

PROC STORE_HANGAR_TV_SEATS()
	thisHangar.activityTVSeatStruct.mpSeatData.seatAnimPositions[0].vLoc = <<26.597, 2.421, 5.634>>
	thisHangar.activityTVSeatStruct.mpSeatData.seatAnimPositions[0].vRot = <<0.0, 0.0, 45.0>>
	thisHangar.activityTVSeatStruct.mpSeatData.seatAnimPositions[1].vLoc = <<25.792, 0.737, 5.634>>
	thisHangar.activityTVSeatStruct.mpSeatData.seatAnimPositions[1].vRot = <<0.0, 0.0, 90.0>>
	thisHangar.activityTVSeatStruct.mpSeatData.seatAnimPositions[2].vLoc = <<28.859, -0.519, 5.634>>
	thisHangar.activityTVSeatStruct.mpSeatData.seatAnimPositions[2].vRot = <<0.0, 0.0, 180.0>>
	thisHangar.activityTVSeatStruct.mpSeatData.seatAnimPositions[3].vLoc = <<28.012, -0.519, 5.634>>
	thisHangar.activityTVSeatStruct.mpSeatData.seatAnimPositions[3].vRot = <<0.0, 0.0, 180.0>>
	thisHangar.activityTVSeatStruct.mpSeatData.seatAnimPositions[4].vLoc = <<27.154, -0.519, 5.634>>
	thisHangar.activityTVSeatStruct.mpSeatData.seatAnimPositions[4].vRot = <<0.0, 0.0, 180.0>>
	
	INT i
	REPEAT 5 i 
		thisHangar.activityTVSeatStruct.mpSeatData.seatAnimPositions[i].vLoc += <<-1266.8020, -3014.8364, -49.4895>>
		thisHangar.activityTVSeatStruct.mpSeatData.seatLocates0[i].vLoc = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(thisHangar.activityTVSeatStruct.mpSeatData.seatAnimPositions[i].vLoc,  thisHangar.activityTVSeatStruct.mpSeatData.seatAnimPositions[i].vRot.Z, -GET_OFFICE_SIMPLE_SEAT_LOCATE_OFFSET_FROM_OFFICE_TYPE(TRUE))
		thisHangar.activityTVSeatStruct.mpSeatData.seatLocates1[i].vLoc = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(thisHangar.activityTVSeatStruct.mpSeatData.seatAnimPositions[i].vLoc,  thisHangar.activityTVSeatStruct.mpSeatData.seatAnimPositions[i].vRot.Z, -GET_OFFICE_SIMPLE_SEAT_LOCATE_OFFSET_FROM_OFFICE_TYPE(FALSE))
	ENDREPEAT
ENDPROC

PROC TV_SEAT_ACTIVITY_CLEAN_UP_HANGAR()
	CDEBUG1LN(DEBUG_SAFEHOUSE, "OFFICE_SEAT_ACTIVITY_CLEAN_UP_HANGAR")
	g_bSecuroQuickExitOfficeChair = FALSE
	
	IF IS_BROWSER_OPEN()
		CDEBUG1LN(DEBUG_SAFEHOUSE, "OFFICE_SEAT_ACTIVITY_CLEAN_UP_HANGAR: Closing web browser")
		CLOSE_WEB_BROWSER()
	ELSE
		CDEBUG1LN(DEBUG_SAFEHOUSE, "OFFICE_SEAT_ACTIVITY_CLEAN_UP_HANGAR: web browser not open")
	ENDIF
	
	IF thisHangar.activityTVSeatStruct.iOfficeSeatContextIntention != NEW_CONTEXT_INTENTION
		CDEBUG1LN(DEBUG_SAFEHOUSE, "OFFICE_SEAT_ACTIVITY_CLEAN_UP_HANGAR: clearing context intention")
		RELEASE_CONTEXT_INTENTION(thisHangar.activityTVSeatStruct.iOfficeSeatContextIntention)
	ENDIF
	
	IF IS_OFFICE_SEAT_EXIT_PROMPT_SHOWN()
	OR IS_OFFICE_SEAT_COMPUTER_EXIT_PROMPT_SHOWN()
	OR IS_OFFICE_ARMCHAIR_EXIT_PROMPT_SHOWN()
		CDEBUG1LN(DEBUG_SAFEHOUSE, "OFFICE_SEAT_ACTIVITY_CLEAN_UP_HANGAR: Clearing help text")
		CLEAR_HELP()
	ENDIF
	
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
	CLEAR_ALL_OFFICE_SIMPLE_SEAT_PROMPTS_GARAGE(thisHangar.activityTVSeatStruct)
	CLEAR_BIT(thisHangar.activityTVSeatStruct.iOfficeSeatiBitSet, OFFICE_SEAT_BS_EXIT_HELP_TEXT_DISPLAYED)
	thisHangar.activityTVSeatStruct.sOfficeSeatAnimDict = ""
	SET_PLAYER_HEALTH_RECHARGE_MAX_PERCENT(GET_PLAYER_INDEX(), 0.5)
	IS_PLAYER_USING_OFFICE_SEATID_VALID()
	g_bSecuroQuickExitOfficeChair = FALSE
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════╡ GARAGE DOOR EXIT ANIM ╞═══════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC INITIALISE_HANGAR_GARAGE_EXIT()
	thisHangar.garageExitAnim.dictionary = "anim@apt_trans@garage"
	thisHangar.garageExitAnim.clip = "gar_open_1_left"
	thisHangar.garageExitAnim.startingPhase = 0.0
	thisHangar.garageExitAnim.endingPhase = 0.45
	
	thisHangar.garageExitAnim.strOnEnterFadeOutFinishSoundSet = "GTAO_EXEC_WH_GARAGE_DOOR_SOUNDS"
	thisHangar.garageExitAnim.strOnEnterFadeOutFinishSoundName = "Door_Open"
	
	thisHangar.vGarageExitMin = <<1104.275, -3101.6143, -40.0000>>
	thisHangar.vGarageExitMax = <<1105.4827, -3097.0706, -36.9999>>
	
	thisHangar.garageExitAnim.syncScenePos = <<1105.095, -3099.446, -40.000>>
	thisHangar.garageExitAnim.syncSceneRot = <<0.0, 0.0, 26.64>>
ENDPROC

FUNC BOOL IS_PLAYER_IN_HANGAR_EXIT_LOCATE()
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND IS_ENTITY_IN_AREA(PLAYER_PED_ID(), thisHangar.vGarageExitMin, thisHangar.vGarageExitMax)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC START_EXIT_VIA_GARAGE_DOOR()
	g_SimpleInteriorData.bShouldExitMenuBeVisible = FALSE
	START_SIMPLE_INTERIOR_ENTRY_ANIM(thisHangar.garageExitAnim)
	SET_HANGAR_STATE(HANGAR_STATE_LOADING_GARAGE_EXIT)
ENDPROC

PROC MAINTAIN_EXIT_VIA_GARAGE_DOOR()
	MAINTAIN_SIMPLE_INTERIOR_ENTRY_ANIM(thisHangar.garageExitAnim, thisHangar.eSimpleInteriorID)
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════╡ DEBUG STUFF ╞═════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

#IF IS_DEBUG_BUILD
PROC CREATE_DEBUG_HANGAR_PRODUCT_WIDGETS()

	START_WIDGET_GROUP("Crate Widgets")
		START_WIDGET_GROUP("Crate Positioning")
			START_WIDGET_GROUP("Product Type")
				START_WIDGET_GROUP("Medical")
					ADD_WIDGET_BOOL("Medical Large", debugData.bSpawnMedicalLarge)
					ADD_WIDGET_BOOL("Medical Medium", debugData.bSpawnMedicalMedium)
				STOP_WIDGET_GROUP()
				START_WIDGET_GROUP("Tobacco")
					ADD_WIDGET_BOOL("Tobacco Large", debugData.bSpawnTobaccoLarge)
					ADD_WIDGET_BOOL("Tobacco Medium", debugData.bSpawnTobaccoMedium)
				STOP_WIDGET_GROUP()
				START_WIDGET_GROUP("Antiques")
					ADD_WIDGET_BOOL("Antiques Large", debugData.bSpawnAntiquesLarge)
					ADD_WIDGET_BOOL("Antiques Medium", debugData.bSpawnAntiquesMedium)
				STOP_WIDGET_GROUP()
				START_WIDGET_GROUP("Nartoics")
					ADD_WIDGET_BOOL("Nartoics Large", debugData.bSpawnNartoicsLarge)
					ADD_WIDGET_BOOL("Nartoics Medium", debugData.bSpawnNartoicsMedium)
				STOP_WIDGET_GROUP()
				START_WIDGET_GROUP("Jewellery")
					ADD_WIDGET_BOOL("Jewellery Large", debugData.bSpawnJewelleryLarge)
					ADD_WIDGET_BOOL("Jewellery Medium", debugData.bSpawnJewelleryMedium)
				STOP_WIDGET_GROUP()	
				START_WIDGET_GROUP("Animal Materials")
					ADD_WIDGET_BOOL("Animal Materials Large", debugData.bSpawnAnimalMaterialsLarge)
					ADD_WIDGET_BOOL("Animal Materials Medium", debugData.bSpawnAnimalMaterialsMedium)
				STOP_WIDGET_GROUP()
				START_WIDGET_GROUP("Counterfeit Goods")
					ADD_WIDGET_BOOL("Counterfeit Goods Large", debugData.bSpawnCounterfeitGoodsLarge)
					ADD_WIDGET_BOOL("Counterfeit Goods Medium", debugData.bSpawnCounterfeitGoodsMedium)
				STOP_WIDGET_GROUP()
				START_WIDGET_GROUP("Chemicals")
					ADD_WIDGET_BOOL("Chemicals Large", debugData.bSpawnChemicalsLarge)
					ADD_WIDGET_BOOL("Chemicals Medium", debugData.bSpawnChemicalsMedium)
				STOP_WIDGET_GROUP()
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Dropoff ID")
				ADD_WIDGET_BOOL("LSIA 1", debugData.bDropoff_LSIA_1)
				ADD_WIDGET_BOOL("LSIA A2", debugData.bDropoff_LSIA_A17)
				ADD_WIDGET_BOOL("Zancudo A2", debugData.bDropoff_Zancudo_A2)
				ADD_WIDGET_BOOL("Zancudo 3497", debugData.bDropoff_Zancudo_3497)
				ADD_WIDGET_BOOL("Zancudo 3499", debugData.bDropoff_Zancudo_3499)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("zPositioning")
				START_WIDGET_GROUP("Crate Coords")
					ADD_WIDGET_VECTOR_SLIDER("Coords", debugData.vCrateCoords, -10000.0, 10000.0, 0.01)
				STOP_WIDGET_GROUP()
				START_WIDGET_GROUP("Crate Rotation")
					ADD_WIDGET_VECTOR_SLIDER("Rotation", debugData.vCrateRotation, -10000.0, 10000.0, 0.01)
				STOP_WIDGET_GROUP()
			STOP_WIDGET_GROUP()
			ADD_WIDGET_BOOL("Spawn Product", debugData.bSpawnProduct)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Spawn Crates")
			START_WIDGET_GROUP("Product Type")
				ADD_WIDGET_BOOL("Medical", debugData.bGoodsTypeMedical)
				ADD_WIDGET_BOOL("Tobacco", debugData.bGoodsTypeTobacco)	
				ADD_WIDGET_BOOL("Antiques", debugData.bGoodsTypeAntiques)	
				ADD_WIDGET_BOOL("Nartoics", debugData.bGoodsTypeNartoics)	
				ADD_WIDGET_BOOL("Jewellery", debugData.bGoodsTypeJewellery)	
				ADD_WIDGET_BOOL("Animal Materials", debugData.bGoodsTypeAnimalMaterial)	
				ADD_WIDGET_BOOL("Counterfeit Goods", debugData.bGoodsTypeCounterfeitGoods)	
				ADD_WIDGET_BOOL("Chemicals", debugData.bGoodsTypeChemicals)
			STOP_WIDGET_GROUP()
			ADD_WIDGET_INT_SLIDER("Product Amount", debugData.iProductAmountToAdd, 1, FREEMODE_DELIVERY_MAX_HELD_DELIVERABLES, 1)
			ADD_WIDGET_BOOL("Add To Hangar", debugData.bAddProductToHangar)
			ADD_WIDGET_BOOL("Remove From Hangar Of Type", debugData.bRemoveProductFromHangarOfType)
			ADD_WIDGET_BOOL("Remove All From Hangar", debugData.bRemoveAllProductFromHangar)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Fade Product Flag")
			ADD_WIDGET_BOOL("Flag State", debugData.bFadeProductFlag)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Product Slot")
			ADD_WIDGET_INT_READ_ONLY("Current Product Slot", thisHangar.iCurrentProductSlot)
			ADD_WIDGET_INT_READ_ONLY("Next Available Slot", debugData.iProductSlotNextAvailable)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Mission Product")
			START_WIDGET_GROUP("Product Type")
				START_WIDGET_GROUP("Medical")
					ADD_WIDGET_BOOL("Medical Large", debugData.bSpawnMedicalLarge)
					ADD_WIDGET_BOOL("Medical Medium", debugData.bSpawnMedicalMedium)
				STOP_WIDGET_GROUP()
				START_WIDGET_GROUP("Tobacco")
					ADD_WIDGET_BOOL("Tobacco Large", debugData.bSpawnTobaccoLarge)
					ADD_WIDGET_BOOL("Tobacco Medium", debugData.bSpawnTobaccoMedium)
				STOP_WIDGET_GROUP()
				START_WIDGET_GROUP("Antiques")
					ADD_WIDGET_BOOL("Antiques Large", debugData.bSpawnAntiquesLarge)
					ADD_WIDGET_BOOL("Antiques Medium", debugData.bSpawnAntiquesMedium)
				STOP_WIDGET_GROUP()
				START_WIDGET_GROUP("Nartoics")
					ADD_WIDGET_BOOL("Nartoics Large", debugData.bSpawnNartoicsLarge)
					ADD_WIDGET_BOOL("Nartoics Medium", debugData.bSpawnNartoicsMedium)
				STOP_WIDGET_GROUP()
				START_WIDGET_GROUP("Jewellery")
					ADD_WIDGET_BOOL("Jewellery Large", debugData.bSpawnJewelleryLarge)
					ADD_WIDGET_BOOL("Jewellery Medium", debugData.bSpawnJewelleryMedium)
				STOP_WIDGET_GROUP()	
				START_WIDGET_GROUP("Animal Materials")
					ADD_WIDGET_BOOL("Animal Materials Large", debugData.bSpawnAnimalMaterialsLarge)
					ADD_WIDGET_BOOL("Animal Materials Medium", debugData.bSpawnAnimalMaterialsMedium)
				STOP_WIDGET_GROUP()
				START_WIDGET_GROUP("Counterfeit Goods")
					ADD_WIDGET_BOOL("Counterfeit Goods Large", debugData.bSpawnCounterfeitGoodsLarge)
					ADD_WIDGET_BOOL("Counterfeit Goods Medium", debugData.bSpawnCounterfeitGoodsMedium)
				STOP_WIDGET_GROUP()
				START_WIDGET_GROUP("Chemicals")
					ADD_WIDGET_BOOL("Chemicals Large", debugData.bSpawnChemicalsLarge)
					ADD_WIDGET_BOOL("Chemicals Medium", debugData.bSpawnChemicalsMedium)
				STOP_WIDGET_GROUP()
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Dropoff ID")
				ADD_WIDGET_BOOL("LSIA 1", debugData.bDropoff_LSIA_1)
				ADD_WIDGET_BOOL("LSIA A2", debugData.bDropoff_LSIA_A17)
				ADD_WIDGET_BOOL("Zancudo A2", debugData.bDropoff_Zancudo_A2)
				ADD_WIDGET_BOOL("Zancudo 3497", debugData.bDropoff_Zancudo_3497)
				ADD_WIDGET_BOOL("Zancudo 3499", debugData.bDropoff_Zancudo_3499)
			STOP_WIDGET_GROUP()
			ADD_WIDGET_BOOL("Send Delivered Event Single", debugData.bSendDeliveredEventSingle)
			ADD_WIDGET_BOOL("Send Delivered Event Multi", debugData.bSendDeliveredEventMulti)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Saved Data")
			START_WIDGET_GROUP("Stats Data")
				REPEAT ciMAX_HANGAR_PRODUCT_UNITS debugData.iProductStatsSlot
					debugData.tlWidgetName = "PACKED_MP_INT_HANGAR_PRODUCT_"
					debugData.tlWidgetName += debugData.iProductStatsSlot
					ADD_WIDGET_INT_READ_ONLY(debugData.tlWidgetName, debugData.iProductStatsData[debugData.iProductStatsSlot])
				ENDREPEAT
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Global Data")
				REPEAT ciMAX_HANGAR_PRODUCT_UNITS debugData.iProductGlobalSlot
					debugData.tlWidgetName = "g_iHangarProductData["
					debugData.tlWidgetName += debugData.iProductGlobalSlot
					debugData.tlWidgetName += "]"
					ADD_WIDGET_INT_READ_ONLY(debugData.tlWidgetName, g_iHangarProductData[debugData.iProductGlobalSlot])
				ENDREPEAT
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Server BD Data")
				REPEAT ciMAX_HANGAR_PRODUCT_UNITS debugData.iProductServerBDSlot
					debugData.tlWidgetName = "serverBD.iProductData["
					debugData.tlWidgetName += debugData.iProductServerBDSlot
					debugData.tlWidgetName += "]"
					ADD_WIDGET_INT_READ_ONLY(debugData.tlWidgetName, serverBD.iProductData[debugData.iProductServerBDSlot])
				ENDREPEAT
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Product Visual Debug")
			ADD_WIDGET_BOOL("Enable", debugData.bProductVisualDebug)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Tutorial Product Slots")
			ADD_WIDGET_INT_SLIDER("Slots To Populate", debugData.iTutorialProductAmountToAdd, 1, ciMAX_HANGAR_PRODUCT_UNITS, 1)
			ADD_WIDGET_BOOL("Spawn Products", debugData.bSpawnTutorialProducts)
			ADD_WIDGET_BOOL("Clear Products", debugData.bClearTutorialProducts)
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
	
ENDPROC			

PROC CREATE_DEBUG_WIDGETS()
	debugData.sWidgetName = "AM_MP_HANGAR"
	
	START_WIDGET_GROUP(debugData.sWidgetName)
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			VEHICLE_MANAGEMENT_CREATE_DEBUG_WIDGETS()

			SIMPLE_CUTSCENE_CREATE_DEBUG_WIDGETS(thisHangar.setupCutscene)
			
			ADD_WIDGET_BOOL("Start spawn activity", debugData.bStartSpawnActivity)
			
			START_WIDGET_GROUP("Huge Vehicle Cam Limits")
				ADD_WIDGET_FLOAT_SLIDER("fPitch_Limit_Min", fHUGE_HANGAR_VEHICLE_Pitch_Limit_Min, -90.0, 90.0, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("fPitch_Limit_Max", fHUGE_HANGAR_VEHICLE_Pitch_Limit_Max, -90.0, 90.0, 0.1)
				
				ADD_WIDGET_FLOAT_SLIDER("fOrbit_Limit_Min", fHUGE_HANGAR_VEHICLE_Orbit_Limit_Min, 5.0, 100.0, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("fOrbit_Limit_Max", fHUGE_HANGAR_VEHICLE_Orbit_Limit_Max, 5.0, 100.0, 0.1)
				
				ADD_WIDGET_FLOAT_SLIDER("fHeading_Limit_Min", fHUGE_HANGAR_VEHICLE_Heading_Limit_Min, -360.0, 360.0, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("fHeading_Limit_Max", fHUGE_HANGAR_VEHICLE_Heading_Limit_Max, -360.0, 360.0, 0.1)
			STOP_WIDGET_GROUP()
		ENDIF
		CREATE_DEBUG_HANGAR_PRODUCT_WIDGETS()
		
		ADD_WIDGET_BOOL("Start hangar cutscene", debugData.bStartIntroCutscene)
		ADD_WIDGET_BOOL("Clear have seen hangar cutscene", debugData.bClearHaveSeenIntroCutscene)
	STOP_WIDGET_GROUP()
	
ENDPROC

PROC INITIALISE_DEBUG_WIDGET_VALUES()
	debugData.vCrateCoords	 	= <<-1246.6, -3014.1, -46.7>>
	debugData.vCrateRotation 	= <<0.0, 0.0, -90.0>>
ENDPROC

FUNC MODEL_NAMES GET_HANGAR_PRODUCT_MODEL_DEBUG()
	
	MODEL_NAMES eCrateModel
	IF debugData.bSpawnMedicalLarge
		eCrateModel = SM_PROP_SMUG_CRATE_L_MEDICAL
	ELIF debugData.bSpawnMedicalMedium
		eCrateModel = SM_PROP_SMUG_CRATE_M_MEDICAL
	ELIF debugData.bSpawnTobaccoLarge
		eCrateModel = SM_PROP_SMUG_CRATE_L_TOBACCO
	ELIF debugData.bSpawnTobaccoMedium
		eCrateModel = SM_PROP_SMUG_CRATE_M_TOBACCO
	ELIF debugData.bSpawnAntiquesLarge
		eCrateModel = SM_PROP_SMUG_CRATE_L_ANTIQUES
	ELIF debugData.bSpawnAntiquesMedium
		eCrateModel = SM_PROP_SMUG_CRATE_M_ANTIQUES
	ELIF debugData.bSpawnNartoicsLarge
		eCrateModel = SM_PROP_SMUG_CRATE_L_NARC
	ELIF debugData.bSpawnNartoicsMedium
		eCrateModel = SM_PROP_SMUG_CRATE_M_NARC
	ELIF debugData.bSpawnJewelleryLarge
		eCrateModel = SM_PROP_SMUG_CRATE_L_JEWELLERY
	ELIF debugData.bSpawnJewelleryMedium
		eCrateModel = SM_PROP_SMUG_CRATE_M_JEWELLERY
	ELIF debugData.bSpawnAnimalMaterialsLarge
		eCrateModel = SM_PROP_SMUG_CRATE_L_BONES
	ELIF debugData.bSpawnAnimalMaterialsMedium
		eCrateModel = SM_PROP_SMUG_CRATE_M_BONES
	ELIF debugData.bSpawnCounterfeitGoodsLarge
		eCrateModel = SM_PROP_SMUG_CRATE_L_FAKE
	ELIF debugData.bSpawnCounterfeitGoodsMedium
		eCrateModel = SM_PROP_SMUG_CRATE_M_FAKE
	ELIF debugData.bSpawnChemicalsLarge
		eCrateModel = SM_PROP_SMUG_CRATE_L_HAZARD
	ELIF debugData.bSpawnChemicalsMedium
		eCrateModel = SM_PROP_SMUG_CRATE_M_HAZARD
	ENDIF
	
	RETURN eCrateModel
ENDFUNC

FUNC eSMUGGLED_GOODS_TYPE GET_HANGAR_SMUGGLED_GOODS_TYPE_DEBUG()
	
	eSMUGGLED_GOODS_TYPE eGoodsType
	IF debugData.bGoodsTypeMedical
		eGoodsType = eSGT_MEDICAL_SUPPLIES
	ELIF debugData.bGoodsTypeTobacco
		eGoodsType = eSGT_TOBACCO_AND_ALCOHOL
	ELIF debugData.bGoodsTypeAntiques
		eGoodsType = eSGT_ART_AND_ANTIQUES
	ELIF debugData.bGoodsTypeNartoics
		eGoodsType = eSGT_NARCOTICS
	ELIF debugData.bGoodsTypeJewellery
		eGoodsType = eSGT_JEWELRY_AND_GEMSTONES
	ELIF debugData.bGoodsTypeAnimalMaterial
		eGoodsType = eSGT_ANIMAL_MATERIALS
	ELIF debugData.bGoodsTypeCounterfeitGoods
		eGoodsType = eSGT_COUNTERFEIT_GOODS
	ELIF debugData.bGoodsTypeChemicals
		eGoodsType = eSGT_CHEMICALS
	ENDIF
	
	RETURN eGoodsType
ENDFUNC

FUNC FREEMODE_DELIVERY_DROPOFFS GET_HANGAR_DROPOFF_DEBUG()
	
	FREEMODE_DELIVERY_DROPOFFS eDropoff
	IF debugData.bDropoff_LSIA_1
		eDropoff = SMUGGLER_DROPOFF_LSIA_HANGAR_1
	ELIF debugData.bDropoff_LSIA_A17
		eDropoff = SMUGGLER_DROPOFF_LSIA_HANGAR_A17
	ELIF debugData.bDropoff_Zancudo_A2
		eDropoff = SMUGGLER_DROPOFF_ZANCUDO_HANGAR_A2
	ELIF debugData.bDropoff_Zancudo_3497
		eDropoff = SMUGGLER_DROPOFF_ZANCUDO_HANGAR_3497
	ELIF debugData.bDropoff_Zancudo_3499
		eDropoff = SMUGGLER_DROPOFF_ZANCUDO_HANGAR_3499
	ENDIF
	
	RETURN eDropoff
ENDFUNC

PROC DISPLAY_HANGAR_PRODUCT_INFO()
	
	FLOAT fSpacingX			= 0.01
	FLOAT fSpacingY			= 0.05
	FLOAT fPS4Spacing 		= 0.0
	FLOAT fSpacingXModel	= 0.02
	
	IF NOT USE_SERVER_TRANSACTIONS()
		fPS4Spacing = 0.013
	ENDIF
	
	// Background Rectangle
    DRAW_RECT((0.02 + fPS4Spacing), (0.033 + fPS4Spacing), 1.68, 0.51, 0, 0, 0, 200)
	SET_TEXT_SCALE((0.375 - fPS4Spacing), (0.375 - fPS4Spacing))
	SET_TEXT_COLOUR(137, 217, 208, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING((0.01 + fPS4Spacing), (0.012 + fPS4Spacing), "STRING", "Hangar Product Slot Info")
    
	// Total No. of Products
	SET_TEXT_SCALE((0.375 - fPS4Spacing), (0.375 - fPS4Spacing))
	SET_TEXT_COLOUR(137, 217, 208, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING((0.689 + fPS4Spacing), (0.012 + fPS4Spacing), "STRING", "Total No. of Products: ")
	
	// Total No. of Products Value
	TEXT_LABEL_15 tlProductTotal = GET_PLAYERS_CONTRABAND_UNITS_TOTAL_FOR_HANGAR(thisHangar.pOwner)
	SET_TEXT_SCALE((0.375 - fPS4Spacing), (0.375 - fPS4Spacing))
	SET_TEXT_COLOUR(137, 217, 208, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING((0.8075 + fPS4Spacing), (0.012 + fPS4Spacing), "STRING", tlProductTotal)
	
	TEXT_LABEL_15 tlProductSlot
	
	INT i
	REPEAT ciMAX_HANGAR_PRODUCT_UNITS i
		
		tlProductSlot += i
		tlProductSlot += ") "
		
		TEXT_LABEL_63 tlProductModel
		tlProductModel = DEBUG_GET_PRODUCT_MODEL_NAME(GET_HANGAR_PRODUCT_MODEL_FROM_INDEX(serverBD.iProductData[i]))
		
		SET_TEXT_SCALE((0.25 - fPS4Spacing), (0.25 - fPS4Spacing))
		SET_TEXT_COLOUR(81, 163, 157, 255)
		DISPLAY_TEXT_WITH_LITERAL_STRING((fSpacingX + fPS4Spacing), (fSpacingY + fPS4Spacing), "STRING", tlProductSlot)
		
		SET_TEXT_SCALE((0.22 - fPS4Spacing), (0.22 - fPS4Spacing))
		SET_TEXT_COLOUR(211, 211, 211, 255)
		DISPLAY_TEXT_WITH_LITERAL_STRING((fSpacingXModel + fPS4Spacing), (fSpacingY + fPS4Spacing), "STRING", tlProductModel)
		
		tlProductModel 	= ""
		tlProductSlot 	= ""
		fSpacingY 		+= 0.0225
		
		// X Spacing
		IF (i >= 0 AND i < 9)
			fSpacingX 		= 0.01
			fSpacingXModel 	= 0.02
			
		ELIF (i >= 9 AND i < 19)
			fSpacingX 		= 0.182
			fSpacingXModel 	= 0.194
			
		ELIF (i >= 19 AND i < 29)
			fSpacingX 		= 0.349
			fSpacingXModel	= 0.364
			
		ELIF (i >= 29 AND i < 39)
			fSpacingX 		= 0.519
			fSpacingXModel	= 0.534
			
		ELIF (i >= 39)
			fSpacingX 		= 0.689
			fSpacingXModel	= 0.704
		ENDIF
		
		// Y Spacing
		IF (i = 9)
		OR (i = 19)
		OR (i = 29)
		OR (i = 39)
			fSpacingY = 0.05
		ENDIF
	ENDREPEAT
ENDPROC

PROC UPDATE_DEBUG_WIDGETS()
	
	// Next Available Product Slot
	debugData.iProductSlotNextAvailable = GET_NEXT_AVAILABLE_HANGAR_PRODUCT_SLOT()
	
	// Fade Product Flag State
	debugData.bFadeProductFlag = IS_BIT_SET(thisHangar.iBS, BS_HANGAR_DATA_FADE_HANGAR_PRODUCT)
	
	// Add/Remove Product To Hangar
	IF debugData.bAddProductToHangar
		
		IF GET_LOCAL_PLAYERS_CONTRABAND_UNITS_TOTAL_FOR_HANGAR() >= ciMAX_HANGAR_PRODUCT_UNITS
			CASSERTLN(DEBUG_SAFEHOUSE, "Don't use this widget when you already have max contraband!")
			debugData.bAddProductToHangar = FALSE
			EXIT
		ENDIF
		
		IF NOT debugData.bTransactionAddedMission
			
			IF NOT USE_SERVER_TRANSACTIONS()
				debugData.bTransactionAddedMission 	= TRUE
				EXIT
			ENDIF
			
			PROCESS_TRANSACTION_FOR_HANGAR_CONTRABAND_MISSION(debugData.eTransactionState, FALSE)
			
			IF debugData.eTransactionState = CONTRABAND_TRANSACTION_STATE_SUCCESS
			
				debugData.bTransactionAddedMission 	= TRUE
				debugData.eTransactionState 		= CONTRABAND_TRANSACTION_STATE_DEFAULT
				
			ELIF debugData.eTransactionState = CONTRABAND_TRANSACTION_STATE_FAILED
			
				debugData.bAddProductToHangar 	= FALSE
				debugData.eTransactionState		= CONTRABAND_TRANSACTION_STATE_DEFAULT
				
			ENDIF
		ELSE
			PRINTLN("[SMUGGLER][HANGAR] ADD_CONTRABAND_TO_HANGAR - Calling via hangar script debug")
			ADD_CONTRABAND_TO_HANGAR(debugData.iProductAmountToAdd, debugData.eTransactionState)
		ENDIF
		
		IF debugData.eTransactionState = CONTRABAND_TRANSACTION_STATE_SUCCESS
		
		
			INT iQuantityRemaining[FREEMODE_DELIVERY_MAX_HELD_DELIVERABLES]
			FREEMODE_DELIVERABLE_TYPE eType[FREEMODE_DELIVERY_MAX_HELD_DELIVERABLES]
			FREEMODE_DELIVERABLE_ID eDeliverableID[FREEMODE_DELIVERY_MAX_HELD_DELIVERABLES]
			
			INT i
			REPEAT debugData.iProductAmountToAdd i
				eDeliverableID[i].pOwner = thisHangar.pOwner
				eDeliverableID[i].iIndex = 1
				eType[i] = GET_HANGAR_FREEMODE_DELIVERABLE_TYPE_FROM_SMUGGLED_GOODS(GET_HANGAR_SMUGGLED_GOODS_TYPE_DEBUG())
			ENDREPEAT
			
			FM_DELIVERY_MISSION_ID sMissionID = _GET_DEBUG_MISSION_ID()
			
			BROADCAST_FREEMODE_MULTIPLE_DELIVERABLE_DELIVERY(eDeliverableID, iQuantityRemaining, GET_HANGAR_DROPOFF_DEBUG(), eType, sMissionID, FM_DELIVERY_ACTION_DEFAULT)
			
			debugData.bAddProductToHangar 		= FALSE
			debugData.bTransactionAddedMission 	= FALSE
			debugData.eTransactionState 		= CONTRABAND_TRANSACTION_STATE_DEFAULT
			
		ELIF debugData.eTransactionState = CONTRABAND_TRANSACTION_STATE_FAILED
			debugData.bAddProductToHangar 		= FALSE
			debugData.bTransactionAddedMission 	= FALSE
			debugData.eTransactionState 		= CONTRABAND_TRANSACTION_STATE_DEFAULT
		ENDIF
	ENDIF
	
	IF NOT debugData.bAddProductToHangar
	
		IF GET_LOCAL_PLAYERS_CONTRABAND_UNITS_TOTAL_FOR_HANGAR() <= 0
		AND (debugData.bRemoveProductFromHangarOfType OR debugData.bRemoveAllProductFromHangar)
			CASSERTLN(DEBUG_SAFEHOUSE, "Don't use this widget when you have no contraband!")
			debugData.bRemoveAllProductFromHangar 		= FALSE
			debugData.bRemoveProductFromHangarOfType 	= FALSE
			EXIT
		ENDIF
				
		IF debugData.bRemoveProductFromHangarOfType
			IF debugData.iTotalOfTypeToRemove = -1
				debugData.iTotalOfTypeToRemove = GET_COUNT_OF_CONTRABAND_TYPE_IN_HANGAR(GET_HANGAR_SMUGGLED_GOODS_TYPE_DEBUG())
				
				IF debugData.iTotalOfTypeToRemove <=0
					debugData.bRemoveProductFromHangarOfType = FALSE
					debugData.iTotalOfTypeToRemove = -1
					EXIT
				ENDIF
			ELSE
				REMOVE_CONTRABAND_FROM_HANGAR(debugData.iTotalOfTypeToRemove, debugData.eTransactionState, GET_HANGAR_SMUGGLED_GOODS_TYPE_DEBUG(), 0, REMOVE_CONTRA_ATTACKED)
				
				IF debugData.eTransactionState = CONTRABAND_TRANSACTION_STATE_SUCCESS
				OR debugData.eTransactionState = CONTRABAND_TRANSACTION_STATE_FAILED
					debugData.iTotalOfTypeToRemove 				= -1
					debugData.bRemoveProductFromHangarOfType 	= FALSE
					debugData.eTransactionState 				= CONTRABAND_TRANSACTION_STATE_DEFAULT
				ENDIF
			ENDIF
		ENDIF
		
		IF debugData.bRemoveAllProductFromHangar
			REMOVE_CONTRABAND_FROM_HANGAR(GET_LOCAL_PLAYERS_CONTRABAND_UNITS_TOTAL_FOR_HANGAR(), debugData.eTransactionState, eSGT_MIXED, 0, REMOVE_CONTRA_ATTACKED)
				
			IF debugData.eTransactionState = CONTRABAND_TRANSACTION_STATE_SUCCESS
			OR debugData.eTransactionState = CONTRABAND_TRANSACTION_STATE_FAILED
				SEND_PRODUCT_DATA_TO_HANGAR_HOST()
				debugData.bRemoveAllProductFromHangar = FALSE
				debugData.eTransactionState = CONTRABAND_TRANSACTION_STATE_DEFAULT
			ENDIF
		ENDIF
	ENDIF
	
	// Crate Placement
	IF debugData.bSpawnProduct
		IF NOT DOES_ENTITY_EXIST(debugData.oProductObj)
			debugData.oProductObj = CREATE_OBJECT(GET_HANGAR_PRODUCT_MODEL_DEBUG(), debugData.vCrateCoords, FALSE, FALSE)
		ENDIF
		SET_ENTITY_ROTATION(debugData.oProductObj, debugData.vCrateRotation)
		SET_ENTITY_COORDS(debugData.oProductObj, debugData.vCrateCoords, FALSE)
	ELSE
		SAFE_DELETE_OBJECT(debugData.oProductObj)
	ENDIF
	
	// Spawn Tutorial Products
	IF debugData.bSpawnTutorialProducts
		SPAWN_HANGAR_TUTORIAL_PRODUCT(debugData.iTutorialProductAmountToAdd)
		debugData.bSpawnTutorialProducts = FALSE
	ENDIF
	
	// Clear Tutorial Products
	IF debugData.bClearTutorialProducts
		CLEANUP_ALL_HANGAR_PRODUCT()
		debugData.bClearTutorialProducts = FALSE
	ENDIF
		
	
	// Product Stats Data
	REPEAT ciMAX_HANGAR_PRODUCT_UNITS debugData.iProductStatsSlot2
		debugData.iProductStatsData[debugData.iProductStatsSlot2] = GET_PACKED_STAT_INT(GET_STAT_ENUM_FOR_HANGAR_PRODUCT_SLOT(debugData.iProductStatsSlot2))
	ENDREPEAT
	
	// Delivered Mission Product Single Event
	IF debugData.bSendDeliveredEventSingle
		FREEMODE_DELIVERABLE_ID eDeliverableID
		eDeliverableID.pOwner = thisHangar.pOwner
		eDeliverableID.iIndex = 1
		
		FREEMODE_DELIVERABLE_TYPE eType = GET_HANGAR_FREEMODE_DELIVERABLE_TYPE_FROM_SMUGGLED_GOODS(GET_HANGAR_PRODUCT_MODEL_PROPERTY_TYPE(GET_HANGAR_PRODUCT_MODEL_DEBUG()))
		
		BROADCAST_FREEMODE_DELIVERABLE_DELIVERY(eDeliverableID, 1, GET_HANGAR_DROPOFF_DEBUG(), eType, FM_DELIVERY_ACTION_DEFAULT)
		debugData.bSendDeliveredEventSingle = FALSE
	ENDIF
	
	// Delivered Mission Product Multi Event
	IF debugData.bSendDeliveredEventMulti
		FREEMODE_DELIVERABLE_ID eDeliverableID[FREEMODE_DELIVERY_MAX_HELD_DELIVERABLES]
		eDeliverableID[0].pOwner = thisHangar.pOwner
		eDeliverableID[0].iIndex = 1
		eDeliverableID[1].pOwner = thisHangar.pOwner
		eDeliverableID[1].iIndex = 1
		
		INT iQuantityRemaining[FREEMODE_DELIVERY_MAX_HELD_DELIVERABLES]
		
		FREEMODE_DELIVERABLE_TYPE eType[FREEMODE_DELIVERY_MAX_HELD_DELIVERABLES]
		eType[0] = GET_HANGAR_FREEMODE_DELIVERABLE_TYPE_FROM_SMUGGLED_GOODS(GET_HANGAR_PRODUCT_MODEL_PROPERTY_TYPE(GET_HANGAR_PRODUCT_MODEL_DEBUG()))
		eType[1] = GET_HANGAR_FREEMODE_DELIVERABLE_TYPE_FROM_SMUGGLED_GOODS(GET_HANGAR_PRODUCT_MODEL_PROPERTY_TYPE(GET_HANGAR_PRODUCT_MODEL_DEBUG()))
		
		FM_DELIVERY_MISSION_ID sMissionID = _GET_DEBUG_MISSION_ID()
		
		BROADCAST_FREEMODE_MULTIPLE_DELIVERABLE_DELIVERY(eDeliverableID, iQuantityRemaining, GET_HANGAR_DROPOFF_DEBUG(), eType, sMissionID, FM_DELIVERY_ACTION_DEFAULT)
		debugData.bSendDeliveredEventMulti = FALSE
	ENDIF
	
	// Product Visual Debug
	IF debugData.bProductVisualDebug
		DISPLAY_HANGAR_PRODUCT_INFO()
	ENDIF
		
	VEHICLE_MANAGEMENT_MAINTAIN_DEBUG_WIDGETS()

	SIMPLE_CUTSCENE_MAINTAIN_DEBUG_WIDGETS(thisHangar.setupCutscene)
	
	IF debugData.bClearHaveSeenIntroCutscene
		debugData.bClearHaveSeenIntroCutscene = FALSE
		INT iStatInt = g_MP_STAT_FM_CUT_DONE[GET_SLOT_NUMBER(-1)]
		CLEAR_BIT(iStatInt, biFmCut_Hangar_Intro_Cutscene)
		SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_FM_CUT_DONE, iStatInt)
		CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdHangarData.iBSHangar, PROPERTY_BROADCAST_BS_OWNS_HANGAR_VIEWED_INTRO)
		PRINTLN("AM_MP_HANGAR DEBUG: Clearing MP_STAT_FM_CUT_DONE - Clearing broadcast bit PROPERTY_BROADCAST_BS_OWNS_HANGAR_VIEWED_INTRO")
	ENDIF
	
ENDPROC
#ENDIF

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════╡ SCRIPT CLEANUP ╞═══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC SET_LOCAL_PLAYER_HAS_PUSHED_TIMECYCLE_MODIFIER(BOOL bSet)
	IF bSet
		IF NOT IS_BIT_SET(thisHangar.iBS2, BS_HANGAR_HAS_SCRIPT_MOD_BEEN_PUSHED)
			SET_BIT(thisHangar.iBS2, BS_HANGAR_HAS_SCRIPT_MOD_BEEN_PUSHED)
			PRINTLN("SET_LOCAL_PLAYER_HAS_PUSHED_TIMECYCLE_MODIFIER - Setting BS_HANGAR_HAS_SCRIPT_MOD_BEEN_PUSHED")
		ENDIF
	ELSE
		IF IS_BIT_SET(thisHangar.iBS2, BS_HANGAR_HAS_SCRIPT_MOD_BEEN_PUSHED)
			CLEAR_BIT(thisHangar.iBS2, BS_HANGAR_HAS_SCRIPT_MOD_BEEN_PUSHED)
			PRINTLN("SET_LOCAL_PLAYER_HAS_PUSHED_TIMECYCLE_MODIFIER - Clearing BS_HANGAR_HAS_SCRIPT_MOD_BEEN_PUSHED")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL HAS_LOCAL_PLAYER_PUSHED_TIMECYCLE_MODIFIER() 
	RETURN IS_BIT_SET(thisHangar.iBS2, BS_HANGAR_HAS_SCRIPT_MOD_BEEN_PUSHED)
ENDFUNC

PROC SET_HANGAR_VEHICLE_FLAGS()
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	AND g_iSimpleInteriorState != SIMPLE_INT_STATE_IDLE
	AND NOT HAS_PLAYER_ACCEPTED_SIMPLE_INTERIOR_INVITE()
	AND thisHangar.bDrivingOut
		VEHICLE_INDEX vTemp = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
		
		IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
		AND IS_THIS_MODEL_ALLOWED_IN_HANGAR(GET_ENTITY_MODEL(vTemp))
			SET_VEHICLE_FIXED(vTemp)
	        SET_ENTITY_HEALTH(vTemp, 1000)
	        SET_VEHICLE_ENGINE_HEALTH(vTemp, 1000)
	        SET_VEHICLE_PETROL_TANK_HEALTH(vTemp, 1000)
			SET_VEHICLE_DIRT_LEVEL(vTemp, 0.0)
			SET_ENTITY_COLLISION(vTemp, TRUE)
			SET_ENTITY_VISIBLE(vTemp, TRUE)
			FREEZE_ENTITY_POSITION(vTemp, FALSE)
			SET_VEHICLE_LIGHTS(vTemp, NO_VEHICLE_LIGHT_OVERRIDE)
			SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(vTemp, FALSE)
			SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(vTemp, FALSE)
			SET_VEHICLE_IS_STOLEN(vTemp, FALSE)
			SET_VEHICLE_NOT_STEALABLE_AMBIENTLY(vTemp, FALSE)
			SET_VEHICLE_RESPECTS_LOCKS_WHEN_HAS_DRIVER(vTemp, FALSE)
			SET_ENTITY_CAN_BE_DAMAGED(vTemp, TRUE)
			SET_ENTITY_INVINCIBLE(vTemp, FALSE)
			SET_CAN_USE_HYDRAULICS(vTemp, TRUE)
			SET_DISABLE_AUTOMATIC_CRASH_TASK(vTemp, FALSE)
			NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(vTemp, FALSE)
			
			IF IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
				SET_VEHICLE_ENGINE_ON(vTemp, TRUE, TRUE)
				SET_HELI_BLADES_FULL_SPEED(vTemp)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - SET_HANGAR_VEHICLE_FLAGS - Setting helicopter engine on and blades to full speed.")
			ENDIF
			
			IF IS_VEHICLE_MODEL(vTemp, TULA)
				SET_VEHICLE_FLIGHT_NOZZLE_POSITION(vTemp, 0.0)
			ENDIF
			
			IF IS_VEHICLE_A_PERSONAL_VEHICLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
			AND GET_OWNER_OF_PERSONAL_VEHICLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) = PLAYER_ID()
				MPGlobalsAmbience.iStoredAllowIntoPV = -1
			ENDIF
		ENDIF
		
		thisHangar.bDrivingOut = FALSE
	ENDIF
ENDPROC

PROC RESET_HANGAR_EXITING_FLAG()
	INT i
	INT iSaveSlot
	INT iDisplaySlot
	
	REPEAT MAX_SAVED_VEHS_IN_HANGAR i
		iDisplaySlot = i + GET_PROPERTY_SLOT_DISPLAY_START_INDEX(PROPERTY_OWNED_SLOT_HANGAR)
		MPSV_GET_SAVE_SLOT_FROM_DISPLAY_SLOT(iDisplaySlot, iSaveSlot, FALSE)
		
		IF iSaveSlot >= 0
		AND IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS, MP_SAVED_VEHICLE_EXITING_SIMPLE_INTERIOR)
			CLEAR_BIT(g_MpSavedVehicles[iSaveSlot].iVehicleBS, MP_SAVED_VEHICLE_EXITING_SIMPLE_INTERIOR)
		ENDIF
	ENDREPEAT
ENDPROC

PROC CLEANUP_CCTV(BOOL bReturnControl)
	CLEANUP_MP_CCTV_CLIENT(playerBD[NATIVE_TO_INT(PLAYER_ID())].MPCCTVClient, MPCCTVLocal, bReturnControl)
ENDPROC

PROC CLEANUP_HANGAR_ENTITY_SETS()
	IF IS_VALID_INTERIOR(thisHangar.HangarInteriorID)
		TEXT_LABEL_63 tlEntitySetName
		INT i
		
		REPEAT ENUM_TO_INT(HANGAR_SET_COUNT) i
			tlEntitySetName = GET_HANGAR_ENTITY_SET(INT_TO_ENUM(HANGAR_ENTITY_SET_ID, i))
			
			IF IS_INTERIOR_ENTITY_SET_ACTIVE(thisHangar.HangarInteriorID, tlEntitySetName)
				DEACTIVATE_INTERIOR_ENTITY_SET(thisHangar.HangarInteriorID, tlEntitySetName)
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

PROC OFFICE_SEAT_ACTIVITY_CLEAN_UP_HANGAR()
	
	OFFICE_SEAT_ACTIVITY_CLEAN_UP(activitySeatStruct, TRUE)
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - OFFICE_SEAT_ACTIVITY_CLEAN_UP_HANGAR")

ENDPROC

PROC CLEANUP_HANGAR_HELP()
 	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HANGAR_LPTP_HLP")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HANGAR_PAM_HLP")
		CLEAR_HELP()
	ENDIF
	
	g_SimpleInteriorData.bDisplayHelpTextInHangar = FALSE
ENDPROC

PROC MAINTAIN_HANGAR_LAPTOP_SCREEN()
	
	SWITCH thisHangar.hangarLaptopData.eLaptopState
		CASE LAPTOP_SCREEN_STATE_INIT
			
			REQUEST_STREAMED_TEXTURE_DICT("prop_screen_sm_free_trade_shipping")
			IF HAS_STREAMED_TEXTURE_DICT_LOADED("prop_screen_sm_free_trade_shipping")
				PRINTLN("MAINTAIN_HANGAR_LAPTOP_SCREEN - Changing to state: LAPTOP_SCREEN_STATE_LINK_RT")
				thisHangar.hangarLaptopData.eLaptopState = LAPTOP_SCREEN_STATE_LINK_RT
			ENDIF
			
		BREAK
		CASE LAPTOP_SCREEN_STATE_LINK_RT
			IF NOT IS_NAMED_RENDERTARGET_REGISTERED("Smug_Monitor_01")
				IF REGISTER_NAMED_RENDERTARGET("Smug_Monitor_01")
					IF NOT IS_NAMED_RENDERTARGET_LINKED(INT_TO_ENUM(MODEL_NAMES, HASH("sm_prop_smug_monitor_01")))
						LINK_NAMED_RENDERTARGET(INT_TO_ENUM(MODEL_NAMES, HASH("sm_prop_smug_monitor_01")))
						IF thisHangar.hangarLaptopData.iRenderTargetID = -1
							thisHangar.hangarLaptopData.iRenderTargetID = GET_NAMED_RENDERTARGET_RENDER_ID("Smug_Monitor_01")
						ENDIF
					ENDIF
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - MAINTAIN_HANGAR_LAPTOP_SCREEN - unable to register Smug_Monitor_01")
					EXIT
				ENDIF
			ENDIF
			
			PRINTLN("MAINTAIN_HANGAR_LAPTOP_SCREEN - Changing to state: LAPTOP_SCREEN_STATE_UPDATE_SCALEFORM - iRenderTargetID: ", thisHangar.hangarLaptopData.iRenderTargetID)
			thisHangar.hangarLaptopData.eLaptopState = LAPTOP_SCREEN_STATE_UPDATE_SCALEFORM
		BREAK
		CASE LAPTOP_SCREEN_STATE_UPDATE_SCALEFORM
			SET_TEXT_RENDER_ID(thisHangar.hangarLaptopData.iRenderTargetID)
			SET_SCRIPT_GFX_ALIGN(UI_ALIGN_IGNORE, UI_ALIGN_IGNORE)
			SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
			
			DRAW_SPRITE_NAMED_RENDERTARGET("prop_screen_sm_free_trade_shipping", "prop_screen_sm_free_trade_shipping", 0.5, 0.5, 1.0, 1.0, 0.0, 255, 255, 255, 255)
			
			RESET_SCRIPT_GFX_ALIGN()
			SET_TEXT_RENDER_ID(GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID())
		BREAK
	ENDSWITCH
	
ENDPROC

PROC CLEANUP_HANGAR_LAPTOP_SCREEN()
	IF IS_NAMED_RENDERTARGET_REGISTERED("Smug_Monitor_01")   
		RELEASE_NAMED_RENDERTARGET("Smug_Monitor_01")
	ENDIF
	thisHangar.hangarLaptopData.iRenderTargetID = -1
	thisHangar.hangarLaptopData.eLaptopState = LAPTOP_SCREEN_STATE_INIT
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("prop_screen_sm_free_trade_shipping")
ENDPROC

PROC CLEANUP_HANGAR_TIMECYCLE_MODIFIERS()
	IF IS_BIT_SET(thisHangar.iBS2, BS_HANGAR_CLEARED_TIMECYCLE_MODIFIERS)
		EXIT
	ENDIF
	
	IF HAS_LOCAL_PLAYER_PUSHED_TIMECYCLE_MODIFIER()
		REMOVE_HANGAR_TIMECYCLE_MODIFIERS(TRUE)
		SET_LOCAL_PLAYER_HAS_PUSHED_TIMECYCLE_MODIFIER(FALSE)
		SET_BIT(thisHangar.iBS2, BS_HANGAR_CLEARED_TIMECYCLE_MODIFIERS)
	ELSE
		REMOVE_HANGAR_TIMECYCLE_MODIFIERS()
		SET_BIT(thisHangar.iBS2, BS_HANGAR_CLEARED_TIMECYCLE_MODIFIERS)
	ENDIF
ENDPROC

PROC SCRIPT_CLEANUP()
	#IF IS_DEBUG_BUILD
	CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_HANGAR - SCRIPT_CLEANUP")
	#ENDIF
	
	g_bCleanupHangarTVRenderTarget = TRUE
	CLEANUP_MP_RADIO(playerBD[NATIVE_TO_INT(PLAYER_ID())].MPRadioClient, MPRadioLocal)
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF NETWORK_GET_THIS_SCRIPT_IS_NETWORK_SCRIPT()
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				CLEANUP_SERVER_RADIO(serverBD.MPRadioServer, InteriorPropStruct)
			ENDIF
		ENDIF
	ENDIF

	CLEANUP_HANGAR_TIMECYCLE_MODIFIERS()
	
	IF NOT g_sMPTunables.bDisable_Christmas_Tree_Apartment
		DELETE_CHRISTMAS_DECORATIONS_FOR_SIMPLE_INTERIOR(thisHangar.objChristmasTree)
	ENDIF
	
	CLEANUP_CCTV(TRUE)
	CLEANUP_HANGAR_ENTITY_SETS()
	CLEANUP_ALL_HANGAR_PRODUCT()
	CLEANUP_SIMPLE_INTERIOR_ENTRY_ANIM(thisHangar.garageExitAnim)
	CLEANUP_HANGAR_LAPTOP_SCREEN()
	IF NOT IS_PLAYER_SPECTATING(PLAYER_ID())
		g_bKeepHelpTextWhenCleaningUpSpecHUD = TRUE
	ENDIF
	CLEANUP_MP_TV(serverBD.MPTVServer, playerBD[thisHangar.MPTVLocal.iServerPlayerStagger].MPTVClient, thisHangar.MPTVLocal, InteriorPropStruct)
	
	TV_SEAT_ACTIVITY_CLEAN_UP_HANGAR()
	
	IF thisHangar.sVehManageData.iManageVehicleContext != NEW_CONTEXT_INTENTION
		RELEASE_CONTEXT_INTENTION(thisHangar.sVehManageData.iManageVehicleContext)
	ENDIF
	CLEANUP_VEHICLE_MANAGEMENT_MENU()
	CLEANUP_VEHICLE_MANAGEMENT_PROP()
	CLEAN_UP_HANGAR_PERSONAL_CAR_MOD()
	
	SET_HANGAR_VEHICLE_FLAGS()
	
	g_bKillTheBed = FALSE
	
	SIMPLE_INTERIOR_CLEAR_ENTERING_WITH_VEHICLE()
	g_SimpleInteriorData.driverPlayer = INVALID_PLAYER_INDEX()
	
	SIMPLE_INTERIOR_CLEAR_ENTERING_WITH_VEH_AS_PASSENGER()
	g_iVehicleEntered = -1
	
	IF (g_iHangarDisplaySlotPlayerIsIn != 0)
		PRINTLN("MAINTAIN_HANGAR_DISPLAY_SLOT_PLAYER_IS_IN cleanup g_iHangarDisplaySlotPlayerIsIn, currently ", g_iHangarDisplaySlotPlayerIsIn)
		g_iHangarDisplaySlotPlayerIsIn = 0
	ENDIF
	
	OFFICE_SEAT_ACTIVITY_CLEAN_UP_HANGAR()
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
		SET_ENTITY_CAN_BE_DAMAGED(PLAYER_PED_ID(), TRUE)
		SET_ENTITY_PROOFS(PLAYER_PED_ID(), FALSE, FALSE, FALSE, FALSE, FALSE)
		SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(PLAYER_PED_ID(), TRUE)
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DontActivateRagdollFromVehicleImpact, FALSE)
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_NotAllowedToJackAnyPlayers, FALSE)
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableStartEngine, FALSE)
	ENDIF
	
	IF thisHangar.pOwner = PLAYER_ID()
		RESET_HANGAR_EXITING_FLAG()
		
		IF thisHangar.bDrivingOut
			IF g_iCurrentPVSlotInProperty >= 0
			AND g_iCurrentPVSlotInProperty < MAX_MP_SAVED_VEHICLES
				PRINTLN("AM_MP_HANGAR - SCRIPT_CLEANUP - Updating iVehicleBS for save slot: ", g_iCurrentPVSlotInProperty)
				
				SET_MP_INT_CHARACTER_STAT(GET_INT_STAT_FOR_VEHICLE_SLOT(MPSV_STAT_ELEMENT_VEHICLE_BS, g_iCurrentPVSlotInProperty), g_MpSavedVehicles[g_iCurrentPVSlotInProperty].iVehicleBS)
			ENDIF
		ENDIF
	ENDIF
	
	CLEANUP_HANGAR_HELP()
	
	IF DOES_BLIP_EXIST(bComputerBlip)
		REMOVE_BLIP(bComputerBlip)
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF NETWORK_IS_SESSION_ACTIVE() AND NETWORK_IS_HOST_OF_THIS_SCRIPT()
			serverBD.iEnteringVeh = -1
		ENDIF
	ENDIF
	SET_CONTROL_SHAKE_SUPPRESSED_ID(PLAYER_CONTROL,-1)
	
	// Fix for B*3915240
	IF g_bScriptsSetSafeForCutscene
		g_bScriptsSetSafeForCutscene = FALSE
	ENDIF
	
	IF NOT IS_BIT_SET(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_CleanupApartScript)
		CLEAR_GLOBAL_ENTRY_DATA()
	ENDIF

	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════╡ INITIALISATION ╞═══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

#IF IS_DEBUG_BUILD
FUNC STRING DEBUG_GET_PLAYER_BROADCAST_BIT_NAME(INT iBit)
	SWITCH iBit
		CASE BS_HANGAR_PERSONAL_QUARTERS_TRADITIONAL_PURCHASED	RETURN "BS_HANGAR_PERSONAL_QUARTERS_TRADITIONAL_PURCHASED"	
		CASE BS_HANGAR_PERSONAL_QUARTERS_MODERN_PURCHASED		RETURN "BS_HANGAR_PERSONAL_QUARTERS_MODERN_PURCHASED"	
		CASE BS_HANGAR_MODSHOP_PURCHASED						RETURN "BS_HANGAR_MODSHOP_PURCHASED"
		CASE BS_HANGAR_FURNITURE_STANDARD_PURCHASED				RETURN "BS_HANGAR_FURNITURE_STANDARD_PURCHASED"
		CASE BS_HANGAR_FURNITURE_TRADITIONAL_PURCHASED			RETURN "BS_HANGAR_FURNITURE_TRADITIONAL_PURCHASED"
		CASE BS_HANGAR_FURNITURE_MODERN_PURCHASED				RETURN "BS_HANGAR_FURNITURE_MODERN_PURCHASED"
		CASE BS_HANGAR_STYLE_0_PURCHASED						RETURN "BS_HANGAR_STYLE_0_PURCHASED"
		CASE BS_HANGAR_STYLE_1_PURCHASED						RETURN "BS_HANGAR_STYLE_1_PURCHASED"
		CASE BS_HANGAR_STYLE_2_PURCHASED						RETURN "BS_HANGAR_STYLE_2_PURCHASED"
		CASE BS_HANGAR_STYLE_3_PURCHASED						RETURN "BS_HANGAR_STYLE_3_PURCHASED"
		CASE BS_HANGAR_STYLE_4_PURCHASED						RETURN "BS_HANGAR_STYLE_4_PURCHASED"
		CASE BS_HANGAR_STYLE_5_PURCHASED						RETURN "BS_HANGAR_STYLE_5_PURCHASED"
		CASE BS_HANGAR_STYLE_6_PURCHASED						RETURN "BS_HANGAR_STYLE_6_PURCHASED"
		CASE BS_HANGAR_STYLE_7_PURCHASED						RETURN "BS_HANGAR_STYLE_7_PURCHASED"
		CASE BS_HANGAR_STYLE_8_PURCHASED						RETURN "BS_HANGAR_STYLE_8_PURCHASED"
		CASE BS_HANGAR_FLOOR_DECAL_0_PURCHASED					RETURN "BS_HANGAR_FLOOR_DECAL_0_PURCHASED"
		CASE BS_HANGAR_FLOOR_DECAL_1_PURCHASED					RETURN "BS_HANGAR_FLOOR_DECAL_1_PURCHASED"
		CASE BS_HANGAR_FLOOR_DECAL_2_PURCHASED					RETURN "BS_HANGAR_FLOOR_DECAL_2_PURCHASED"
		CASE BS_HANGAR_FLOOR_DECAL_3_PURCHASED					RETURN "BS_HANGAR_FLOOR_DECAL_3_PURCHASED"
		CASE BS_HANGAR_FLOOR_DECAL_4_PURCHASED					RETURN "BS_HANGAR_FLOOR_DECAL_4_PURCHASED"
		CASE BS_HANGAR_FLOOR_DECAL_5_PURCHASED					RETURN "BS_HANGAR_FLOOR_DECAL_5_PURCHASED"
		CASE BS_HANGAR_FLOOR_DECAL_6_PURCHASED					RETURN "BS_HANGAR_FLOOR_DECAL_6_PURCHASED"
		CASE BS_HANGAR_FLOOR_DECAL_7_PURCHASED					RETURN "BS_HANGAR_FLOOR_DECAL_7_PURCHASED"
		CASE BS_HANGAR_FLOOR_DECAL_8_PURCHASED					RETURN "BS_HANGAR_FLOOR_DECAL_8_PURCHASED"
		CASE BS_HANGAR_LIGHTING_0_PURCHASED						RETURN "BS_HANGAR_LIGHTING_0_PURCHASED"
		CASE BS_HANGAR_LIGHTING_1_PURCHASED						RETURN "BS_HANGAR_LIGHTING_1_PURCHASED"
		CASE BS_HANGAR_LIGHTING_2_PURCHASED						RETURN "BS_HANGAR_LIGHTING_2_PURCHASED"
		CASE BS_HANGAR_LIGHTING_3_PURCHASED						RETURN "BS_HANGAR_LIGHTING_3_PURCHASED"
	ENDSWITCH

	RETURN "***unknown***"
ENDFUNC
#ENDIF

PROC SET_LOCAL_PLAYER_BROADCAST_BIT(INT iBit, BOOL bSet)
	IF bSet
		#IF IS_DEBUG_BUILD
		IF NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, iBit)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - SET_LOCAL_PLAYER_BROADCAST_BIT - Setting bit ", DEBUG_GET_PLAYER_BROADCAST_BIT_NAME(iBit))
		#ENDIF
		
			SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, iBit)
		
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		IF IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, iBit)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - SET_LOCAL_PLAYER_BROADCAST_BIT - Clearing bit ", DEBUG_GET_PLAYER_BROADCAST_BIT_NAME(iBit))
		#ENDIF
			
			CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, iBit)
		
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
	ENDIF
ENDPROC

PROC INITIALISE_HANGAR_ENTITY_SETS()
	thisHangar.HangarInteriorID = GET_INTERIOR_AT_COORDS_WITH_TYPE(GET_HANGAR_INTERIOR_COORDS(thisHangar.eID), GET_HANGAR_INTERIOR_NAME(thisHangar.eID))

	IF IS_VALID_INTERIOR(thisHangar.HangarInteriorID)
		SET_DEFAULT_HANGAR_ENTITY_SETS(thisHangar.eID, thisHangar.pOwner)
	ENDIF

	SET_LOCAL_PLAYER_BROADCAST_BIT(BS_HANGAR_PERSONAL_QUARTERS_TRADITIONAL_PURCHASED, IS_PLAYER_HANGAR_SAVEBED_TRADITIONAL_PURCHASED(thisHangar.pOwner))
	SET_LOCAL_PLAYER_BROADCAST_BIT(BS_HANGAR_PERSONAL_QUARTERS_MODERN_PURCHASED, IS_PLAYER_HANGAR_SAVEBED_MODERN_PURCHASED(thisHangar.pOwner))
	SET_LOCAL_PLAYER_BROADCAST_BIT(BS_HANGAR_MODSHOP_PURCHASED, IS_PLAYER_HANGAR_MODSHOP_PURCHASED(thisHangar.pOwner))
	SET_LOCAL_PLAYER_BROADCAST_BIT(BS_HANGAR_FURNITURE_STANDARD_PURCHASED, IS_PLAYER_HANGAR_FURNITURE_STANDARD_PURCHASED(thisHangar.pOwner))
	SET_LOCAL_PLAYER_BROADCAST_BIT(BS_HANGAR_FURNITURE_TRADITIONAL_PURCHASED, IS_PLAYER_HANGAR_FURNITURE_TRADITIONAL_PURCHASED(thisHangar.pOwner))
	SET_LOCAL_PLAYER_BROADCAST_BIT(BS_HANGAR_FURNITURE_MODERN_PURCHASED, IS_PLAYER_HANGAR_FURNITURE_MODERN_PURCHASED(thisHangar.pOwner))
	SET_LOCAL_PLAYER_BROADCAST_BIT(BS_HANGAR_FLOOR_DECAL_0_PURCHASED, IS_PLAYER_HANGAR_FLOOR_DECAL_0_PURCHASED(thisHangar.pOwner))
	SET_LOCAL_PLAYER_BROADCAST_BIT(BS_HANGAR_FLOOR_DECAL_1_PURCHASED, IS_PLAYER_HANGAR_FLOOR_DECAL_1_PURCHASED(thisHangar.pOwner))
	SET_LOCAL_PLAYER_BROADCAST_BIT(BS_HANGAR_FLOOR_DECAL_2_PURCHASED, IS_PLAYER_HANGAR_FLOOR_DECAL_2_PURCHASED(thisHangar.pOwner))
	SET_LOCAL_PLAYER_BROADCAST_BIT(BS_HANGAR_FLOOR_DECAL_3_PURCHASED, IS_PLAYER_HANGAR_FLOOR_DECAL_3_PURCHASED(thisHangar.pOwner))
	SET_LOCAL_PLAYER_BROADCAST_BIT(BS_HANGAR_FLOOR_DECAL_4_PURCHASED, IS_PLAYER_HANGAR_FLOOR_DECAL_4_PURCHASED(thisHangar.pOwner))
	SET_LOCAL_PLAYER_BROADCAST_BIT(BS_HANGAR_FLOOR_DECAL_5_PURCHASED, IS_PLAYER_HANGAR_FLOOR_DECAL_5_PURCHASED(thisHangar.pOwner))
	SET_LOCAL_PLAYER_BROADCAST_BIT(BS_HANGAR_FLOOR_DECAL_6_PURCHASED, IS_PLAYER_HANGAR_FLOOR_DECAL_6_PURCHASED(thisHangar.pOwner))
	SET_LOCAL_PLAYER_BROADCAST_BIT(BS_HANGAR_FLOOR_DECAL_7_PURCHASED, IS_PLAYER_HANGAR_FLOOR_DECAL_7_PURCHASED(thisHangar.pOwner))
	SET_LOCAL_PLAYER_BROADCAST_BIT(BS_HANGAR_FLOOR_DECAL_8_PURCHASED, IS_PLAYER_HANGAR_FLOOR_DECAL_8_PURCHASED(thisHangar.pOwner))
	SET_LOCAL_PLAYER_BROADCAST_BIT(BS_HANGAR_STYLE_0_PURCHASED, IS_PLAYER_HANGAR_STYLE_0_PURCHASED(thisHangar.pOwner))
	SET_LOCAL_PLAYER_BROADCAST_BIT(BS_HANGAR_STYLE_1_PURCHASED, IS_PLAYER_HANGAR_STYLE_1_PURCHASED(thisHangar.pOwner))
	SET_LOCAL_PLAYER_BROADCAST_BIT(BS_HANGAR_STYLE_2_PURCHASED, IS_PLAYER_HANGAR_STYLE_2_PURCHASED(thisHangar.pOwner))
	SET_LOCAL_PLAYER_BROADCAST_BIT(BS_HANGAR_STYLE_3_PURCHASED, IS_PLAYER_HANGAR_STYLE_3_PURCHASED(thisHangar.pOwner))
	SET_LOCAL_PLAYER_BROADCAST_BIT(BS_HANGAR_STYLE_4_PURCHASED, IS_PLAYER_HANGAR_STYLE_4_PURCHASED(thisHangar.pOwner))
	SET_LOCAL_PLAYER_BROADCAST_BIT(BS_HANGAR_STYLE_5_PURCHASED, IS_PLAYER_HANGAR_STYLE_5_PURCHASED(thisHangar.pOwner))
	SET_LOCAL_PLAYER_BROADCAST_BIT(BS_HANGAR_STYLE_6_PURCHASED, IS_PLAYER_HANGAR_STYLE_6_PURCHASED(thisHangar.pOwner))
	SET_LOCAL_PLAYER_BROADCAST_BIT(BS_HANGAR_STYLE_7_PURCHASED, IS_PLAYER_HANGAR_STYLE_7_PURCHASED(thisHangar.pOwner))
	SET_LOCAL_PLAYER_BROADCAST_BIT(BS_HANGAR_STYLE_8_PURCHASED, IS_PLAYER_HANGAR_STYLE_8_PURCHASED(thisHangar.pOwner))
	SET_LOCAL_PLAYER_BROADCAST_BIT(BS_HANGAR_LIGHTING_0_PURCHASED, IS_PLAYER_HANGAR_LIGHTING_0_PURCHASED(thisHangar.pOwner))
	SET_LOCAL_PLAYER_BROADCAST_BIT(BS_HANGAR_LIGHTING_1_PURCHASED, IS_PLAYER_HANGAR_LIGHTING_1_PURCHASED(thisHangar.pOwner))
	SET_LOCAL_PLAYER_BROADCAST_BIT(BS_HANGAR_LIGHTING_2_PURCHASED, IS_PLAYER_HANGAR_LIGHTING_2_PURCHASED(thisHangar.pOwner))
	SET_LOCAL_PLAYER_BROADCAST_BIT(BS_HANGAR_LIGHTING_3_PURCHASED, IS_PLAYER_HANGAR_LIGHTING_3_PURCHASED(thisHangar.pOwner))
ENDPROC

PROC INITIALISE()
	g_bPlayerLeavingCurrentInteriorInVeh = FALSE
	
	g_bCleanupHangarTVRenderTarget = FALSE
	g_bPlayerLeavingHangarInAircraft = FALSE
	activitySeatStruct.propertyOwner = GET_PLAYER_PED(thisHangar.pOwner)
	
	// Get simple interior details
	GET_SIMPLE_INTERIOR_DETAILS(thisHangar.eSimpleInteriorID, thisHangar.interiorDetails)
	
	INITIALISE_HANGAR_ENTITY_SETS()
	
	INITIALISE_HANGAR_PRODUCT_HOST()
	INITIALISE_HANGAR_GARAGE_EXIT()
	STORE_HANGAR_TV_SEATS()
	STORE_HANGAR_SEAT_POSITIONS(activitySeatStruct)
	
	INITIALISE_INTERIOR_BED_SPAWN_ACTIVITIES(GET_SIMPLE_INTERIOR_TYPE(thisHangar.eSimpleInteriorID))
	
	#IF IS_DEBUG_BUILD
	CREATE_DEBUG_WIDGETS()
	INITIALISE_DEBUG_WIDGET_VALUES()
	CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_HANGAR - INITIALISE - Done.")
	#ENDIF
	
	HOLSTER_WEAPON_IN_SIMPLE_INTERIOR()
	SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
	SET_ENTITY_CAN_BE_DAMAGED(PLAYER_PED_ID(), FALSE)
	SET_ENTITY_PROOFS(PLAYER_PED_ID(), TRUE, TRUE, TRUE, TRUE, TRUE)
	SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(PLAYER_PED_ID(), FALSE)
	SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DontActivateRagdollFromBulletImpact, TRUE)
	SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_NotAllowedToJackAnyPlayers, TRUE)
	SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableStartEngine, TRUE)
	
	IF thisHangar.pOwner = PLAYER_ID()
		RESET_HANGAR_EXITING_FLAG()
	ENDIF
	
	// Reset the exit menu option as we initialise the hangar.
	IF g_SimpleInteriorData.iExitMenuOption > -1
		g_SimpleInteriorData.iExitMenuOption = -1
	ENDIF

	START_NET_TIMER(getOutOfWayTimer)
ENDPROC

FUNC BOOL DETERMINE_HANGAR_OWNER()	
	// We have to search for the owner as we can't rely on gang membership.
	IF IS_LOCAL_PLAYER_IN_HANGAR_THEY_OWN()
		thisHangar.pOwner = PLAYER_ID()
		MPGlobals.iHangarOwner = NATIVE_TO_INT(thisHangar.pOwner)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - DETERMINE_HANGAR_OWNER - I ", GET_PLAYER_NAME(thisHangar.pOwner), " am the owner. Access BS = ", g_SimpleInteriorData.iAccessBS) 
		RETURN TRUE
	ENDIF
	
	IF g_SimpleInteriorData.iAccessBS > 0
		IF globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner != INVALID_PLAYER_INDEX()
			
			IF IS_NET_PLAYER_OK(globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner, FALSE)
				thisHangar.pOwner = globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner
				MPGlobals.iHangarOwner = NATIVE_TO_INT(thisHangar.pOwner)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - DETERMINE_HANGAR_OWNER - I am not the owner. It is: ", GET_PLAYER_NAME(thisHangar.pOwner))
				RETURN TRUE
			ELSE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - DETERMINE_HANGAR_OWNER - I am not the owner. It is: ", GET_PLAYER_NAME(thisHangar.pOwner), " holding up the script due to IS_NET_PLAYER_OK")
				
				IF NOT HAS_NET_TIMER_STARTED(thisHangar.stOwnerNotOKTimer)
					START_NET_TIMER(thisHangar.stOwnerNotOKTimer)
				ENDIF
				
				IF HAS_NET_TIMER_EXPIRED(thisHangar.stOwnerNotOKTimer, 5000)
					// If the owner is not ok after 5 seconds just move on. The interior script will run a check on this and kick us once we're in anyway.
					thisHangar.pOwner = globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner
					MPGlobals.iHangarOwner = NATIVE_TO_INT(thisHangar.pOwner)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - DETERMINE_HANGAR_OWNER - I am not the owner. It is: ", GET_PLAYER_NAME(thisHangar.pOwner), " timer expired for IS_NET_PLAYER_OK. Continuing regardless")
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF g_SimpleInteriorData.iAccessBS = 0
		IF (GET_FRAME_COUNT() % 60) = 0
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - DETERMINE_HANGAR_OWNER - Unable to determine hangar owner, g_SimpleInteriorData.iAccessBS = ", g_SimpleInteriorData.iAccessBS)
		ENDIF
		
		IF NOT HAS_NET_TIMER_STARTED(g_SimpleInteriorData.tIntAccessFallback)
			START_NET_TIMER(g_SimpleInteriorData.tIntAccessFallback)
		ENDIF
		
		// NOTE: If we still can't determine the hangar owner, return true so we 
		// can force the hangar script to launch and kick any players who are 
		// stuck inside. 
		IF HAS_NET_TIMER_EXPIRED(g_SimpleInteriorData.tIntAccessFallback, g_SimpleInteriorData.iIntAccessFallbackTime)
			thisHangar.pOwner = INVALID_PLAYER_INDEX()
			RESET_NET_TIMER(g_SimpleInteriorData.tIntAccessFallback)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - DETERMINE_HANGAR_OWNER - Unable to determine hangar owner, returning TRUE")
			RETURN TRUE
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF (GET_FRAME_COUNT() % 5) = 0 	
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - DETERMINE_HANGAR_OWNER - g_SimpleInteriorData.iAccessBS = ", g_SimpleInteriorData.iAccessBS, " Trying to find the owner.")
	ENDIF
	#ENDIF
	
	RETURN FALSE
ENDFUNC

PROC INIT_BED_DATA()
	CDEBUG1LN(DEBUG_SAFEHOUSE, "INIT_BED_DATA")
	bedStructLeft.vScenePos = << -1237.536, -2985.021, -41.231 >>
	bedStructLeft.vSceneRot = << 0.000, 0.000, 28.800 >>
	bedStructLeft.vBoundingBoxA =  <<-1238.251343,-2985.629395,-42.261150>>
	bedStructLeft.vBoundingBoxB = <<-1238.248047,-2982.878174,-40.424896>> 
	bedStructLeft.fWidth = 2.5
	bedStructLeft.iScriptInstanceID = 1
	bedStructLeft.bEnterRightBedSide = TRUE
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "INIT_BED_DATA")
	bedStructRight.vScenePos = << -1239.666, -2983.651, -41.822 >>
	bedStructRight.vSceneRot = << 0.000, 0.000, -75.240 >>
	bedStructRight.vBoundingBoxA = <<-1240.747559,-2982.645508,-42.261150>>  
	bedStructRight.vBoundingBoxB = <<-1240.711182,-2985.602295,-40.511150>> 
	bedStructRight.fWidth = 1.500
	bedStructRight.iScriptInstanceID = 2
	bedStructRight.bEnterRightBedSide = FALSE
	
//For now just let them go in from one side
//	bedStructMechanicLeft.vScenePos = << -1296.860, -3024.750, -44.045 >>
//	bedStructMechanicLeft.vSceneRot = << 0.000, 0.000, -56.160 >>
//	bedStructMechanicLeft.vBoundingBoxA = <<-1296.764160,-3023.875488,-45.086365>>
//	bedStructMechanicLeft.vBoundingBoxB = <<-1294.218018,-3023.815186,-42.601391>> 
//	bedStructMechanicLeft.fWidth = 1.75
//	bedStructMechanicLeft.iScriptInstanceID = 2
//	bedStructMechanicLeft.bEnterRightBedSide = TRUE
	
	bedStructMechanicRight.vScenePos = << -1295.698, -3023.544, -44.616 >>
	bedStructMechanicRight.vSceneRot = << 0.000, -0.000, -180.000 >>
	bedStructMechanicRight.vBoundingBoxA = <<-1296.697021,-3022.834717,-45.086365>>
	bedStructMechanicRight.vBoundingBoxB = <<-1294.218018,-3022.856201,-42.482265>> 
	bedStructMechanicRight.fWidth = 1.75
	bedStructMechanicRight.iScriptInstanceID = 3
	bedStructMechanicRight.bEnterRightBedSide = FALSE
ENDPROC

PROC SCRIPT_INITIALISE(SIMPLE_INTERIOR_CHILD_SCRIPT_DATA& scriptData)
	
	#IF IS_DEBUG_BUILD
	CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_HANGAR - SCRIPT_INITIALISE - Launching instance ", thisHangar.iScriptInstance)
	#ENDIF
	
	IF DID_I_JOIN_MISSION_AS_SPECTATOR() AND IS_BIT_SET(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_SPAWNING_IN_SIMPLE_INTERIOR)
	OR (g_sImpromptuVars.iLaunchStage != -1) // Check if a deathmatch is launching.
		SET_SIMPLE_INTERIOR_CHILD_SCRIPT_ALLOWS_FOR_WARP(TRUE)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - SCRIPT_INITIALISE - We are launching a mission so no need to launch child script.")
		SCRIPT_CLEANUP()
	ENDIF
	
	// Fix for B*3787142
	IF IS_PHONE_ONSCREEN()
		HANG_UP_AND_PUT_AWAY_PHONE()
	ENDIF
	
	InteriorPropStruct.iIndex = -1 // reseting property struct so the radio doesn't think the player is still in the last apartment they were in.
	InteriorPropStruct.iGarageSize = 0
	InteriorPropStruct.iType = -1
	InteriorPropStruct.iBuildingID = -1
	
	MPGlobals.iHangarOwner = -1
	
	WHILE NOT DETERMINE_HANGAR_OWNER()
		IF NOT NETWORK_IS_GAME_IN_PROGRESS()
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - SCRIPT_INITIALISE - NETWORK_IS_GAME_IN_PROGRESS = FALSE")
			SCRIPT_CLEANUP()
		ENDIF
		
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - SCRIPT_INITIALISE - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE = TRUE")
			SCRIPT_CLEANUP()
		ENDIF
		
		IF SHOULD_THIS_SIMPLE_INTERIOR_CHILD_THREAD_TERMINATE(scriptData.iScriptInstance, scriptData.eSimpleInteriorID)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - SCRIPT_INITIALISE - SHOULD_THIS_SIMPLE_INTERIOR_CHILD_THREAD_TERMINATE = TRUE")
			SCRIPT_CLEANUP()
		ENDIF
		
		IF IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_HANGAR_ACCESS_BS_OWNER_LEFT_GAME)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - SCRIPT_INITIALISE - SIMPLE_INTERIOR_HANGAR_ACCESS_BS_OWNER_LEFT_GAME = TRUE")
			SCRIPT_CLEANUP()
		ENDIF

		WAIT(0)
	ENDWHILE
	
	MPRadioLocal.piApartmentOwner = thisHangar.pOwner
	
	thisHangar.eSimpleInteriorID 	= scriptData.eSimpleInteriorID
	thisHangar.iScriptInstance 		= scriptData.iScriptInstance
	thisHangar.iInvitingPlayer 		= scriptData.iInvitingPlayer
	thisHangar.eID					= GET_HANGAR_ID_FROM_SIMPLE_INTERIOR_ID(scriptData.eSimpleInteriorID)

	IF thisHangar.pOwner = INVALID_PLAYER_INDEX()
		#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_HANGAR - SCRIPT_INITIALISE - Owner of this HANGAR is invalid, exiting...")
		#ENDIF
		SET_SIMPLE_INTERIOR_CHILD_SCRIPT_READY(TRUE)
		SCRIPT_CLEANUP()
	ENDIF
	
	IF IS_BIT_SET(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_SPAWNING_IN_SIMPLE_INTERIOR)
		IF thisHangar.pOwner = INVALID_PLAYER_INDEX()
			HANGAR_ID eHangarID = GET_PLAYERS_OWNED_HANGAR(PLAYER_ID())
			
			PRINTLN("AM_MP_HANGAR - SCRIPT_INITIALISE - Spawning inside hangar but owner is invalid... Stat says we own hangar ", eHangarID, ", the script is starting for ", GET_HANGAR_ID_FROM_SIMPLE_INTERIOR_ID(thisHangar.eSimpleInteriorID))
			
			IF eHangarID = GET_HANGAR_ID_FROM_SIMPLE_INTERIOR_ID(thisHangar.eSimpleInteriorID)
				PRINTLN("AM_MP_HANGAR - SCRIPT_INITIALISE - We are spawning in IE warehouse that we own so we are the owner.")
				thisHangar.pOwner = PLAYER_ID()
			ENDIF
		ENDIF
	ENDIF
	
	NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE, thisHangar.iScriptInstance)
	
	// This makes sure the net script is active, waits untill it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
	
	// KGM: Wait for the first network broadcast before moving on
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - SCRIPT_INITIALISE - FAILED TO RECEIVE INITIAL NETWORK BROADCAST - SCRIPT CLEANUP")
		SCRIPT_CLEANUP()
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		// This script will not be paused if another script calls PAUSE_GAME
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - SCRIPT_INITIALISE - INITIALISED")
	ELSE
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - SCRIPT_INITIALISE - NETWORK_IS_GAME_IN_PROGRESS = FALSE - SCRIPT CLEANUP")
		SCRIPT_CLEANUP()
	ENDIF
	
	g_bLaunchedMissionFrmLaptop = FALSE
	INIT_BED_DATA()
	INITIALISE()
	
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════╡ HANGAR UPGRADES ╞══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

FUNC BOOL DID_PLAYER_CHOOSE_TO_EXIT_HANGAR()
	
	IF g_SimpleInteriorData.iExitMenuOption != 0
		RETURN FALSE
	ENDIF
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - DID_PLAYER_CHOOSE_TO_EXIT_HANGAR - Player is exiting hangar, returning TRUE")
	RETURN TRUE
ENDFUNC

FUNC BOOL PERFORM_HANGAR_MAZEBANK_WEBSITE_UPGRADE()
	
	//IF IS_LOCAL_PLAYER_WALKING_OUT_OF_SIMPLE_INTERIOR()
	IF DID_PLAYER_CHOOSE_TO_EXIT_HANGAR()
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - PERFORM_HANGAR_MAZEBANK_WEBSITE_UPGRADE - Player is walking in or out of the interior, returning FALSE")
		RETURN FALSE
	ENDIF

	IF IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_HANGAR_PERSONAL_QUARTERS_TRADITIONAL_PURCHASED) != IS_PLAYER_HANGAR_SAVEBED_TRADITIONAL_PURCHASED(thisHangar.pOwner)
	OR IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_HANGAR_PERSONAL_QUARTERS_MODERN_PURCHASED) != IS_PLAYER_HANGAR_SAVEBED_MODERN_PURCHASED(thisHangar.pOwner)
	OR IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_HANGAR_MODSHOP_PURCHASED) != IS_PLAYER_HANGAR_MODSHOP_PURCHASED(thisHangar.pOwner)
	OR IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_HANGAR_FURNITURE_STANDARD_PURCHASED) != IS_PLAYER_HANGAR_FURNITURE_STANDARD_PURCHASED(thisHangar.pOwner)
	OR IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_HANGAR_FURNITURE_TRADITIONAL_PURCHASED) != IS_PLAYER_HANGAR_FURNITURE_TRADITIONAL_PURCHASED(thisHangar.pOwner)
	OR IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_HANGAR_FURNITURE_MODERN_PURCHASED) != IS_PLAYER_HANGAR_FURNITURE_MODERN_PURCHASED(thisHangar.pOwner)
	OR IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_HANGAR_FLOOR_DECAL_0_PURCHASED) != IS_PLAYER_HANGAR_FLOOR_DECAL_0_PURCHASED(thisHangar.pOwner)
	OR IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_HANGAR_FLOOR_DECAL_1_PURCHASED) != IS_PLAYER_HANGAR_FLOOR_DECAL_1_PURCHASED(thisHangar.pOwner)
	OR IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_HANGAR_FLOOR_DECAL_2_PURCHASED) != IS_PLAYER_HANGAR_FLOOR_DECAL_2_PURCHASED(thisHangar.pOwner)
	OR IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_HANGAR_FLOOR_DECAL_3_PURCHASED) != IS_PLAYER_HANGAR_FLOOR_DECAL_3_PURCHASED(thisHangar.pOwner)
	OR IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_HANGAR_FLOOR_DECAL_4_PURCHASED) != IS_PLAYER_HANGAR_FLOOR_DECAL_4_PURCHASED(thisHangar.pOwner)
	OR IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_HANGAR_FLOOR_DECAL_5_PURCHASED) != IS_PLAYER_HANGAR_FLOOR_DECAL_5_PURCHASED(thisHangar.pOwner)
	OR IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_HANGAR_FLOOR_DECAL_6_PURCHASED) != IS_PLAYER_HANGAR_FLOOR_DECAL_6_PURCHASED(thisHangar.pOwner)
	OR IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_HANGAR_FLOOR_DECAL_7_PURCHASED) != IS_PLAYER_HANGAR_FLOOR_DECAL_7_PURCHASED(thisHangar.pOwner)
	OR IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_HANGAR_FLOOR_DECAL_8_PURCHASED) != IS_PLAYER_HANGAR_FLOOR_DECAL_8_PURCHASED(thisHangar.pOwner)
	OR IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_HANGAR_STYLE_0_PURCHASED) != IS_PLAYER_HANGAR_STYLE_0_PURCHASED(thisHangar.pOwner)
	OR IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_HANGAR_STYLE_1_PURCHASED) != IS_PLAYER_HANGAR_STYLE_1_PURCHASED(thisHangar.pOwner)
	OR IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_HANGAR_STYLE_2_PURCHASED) != IS_PLAYER_HANGAR_STYLE_2_PURCHASED(thisHangar.pOwner)
	OR IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_HANGAR_STYLE_3_PURCHASED) != IS_PLAYER_HANGAR_STYLE_3_PURCHASED(thisHangar.pOwner)
	OR IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_HANGAR_STYLE_4_PURCHASED) != IS_PLAYER_HANGAR_STYLE_4_PURCHASED(thisHangar.pOwner)
	OR IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_HANGAR_STYLE_5_PURCHASED) != IS_PLAYER_HANGAR_STYLE_5_PURCHASED(thisHangar.pOwner)
	OR IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_HANGAR_STYLE_6_PURCHASED) != IS_PLAYER_HANGAR_STYLE_6_PURCHASED(thisHangar.pOwner)
	OR IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_HANGAR_STYLE_7_PURCHASED) != IS_PLAYER_HANGAR_STYLE_7_PURCHASED(thisHangar.pOwner)
	OR IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_HANGAR_STYLE_8_PURCHASED) != IS_PLAYER_HANGAR_STYLE_8_PURCHASED(thisHangar.pOwner)
	OR IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_HANGAR_LIGHTING_0_PURCHASED) != IS_PLAYER_HANGAR_LIGHTING_0_PURCHASED(thisHangar.pOwner)
	OR IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_HANGAR_LIGHTING_1_PURCHASED) != IS_PLAYER_HANGAR_LIGHTING_1_PURCHASED(thisHangar.pOwner)
	OR IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_HANGAR_LIGHTING_2_PURCHASED) != IS_PLAYER_HANGAR_LIGHTING_2_PURCHASED(thisHangar.pOwner)
	OR IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_HANGAR_LIGHTING_3_PURCHASED) != IS_PLAYER_HANGAR_LIGHTING_3_PURCHASED(thisHangar.pOwner)
		#IF IS_DEBUG_BUILD
		IF (GET_FRAME_COUNT() % 60) = 0
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - PERFORM_HANGAR_MAZEBANK_WEBSITE_UPGRADE - Returning TRUE.")
		ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF (GET_FRAME_COUNT() % 120) = 0
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - PERFORM_HANGAR_MAZEBANK_WEBSITE_UPGRADE - Returning FALSE.")
	ENDIF
	#ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_PLAYER_WARP_DURING_HANGAR_UPGRADE(PLAYER_INDEX playerID)
	
	//IF IS_LOCAL_PLAYER_WALKING_OUT_OF_SIMPLE_INTERIOR()
	IF DID_PLAYER_CHOOSE_TO_EXIT_HANGAR()
		PRINTLN("AM_MP_HANGAR - SHOULD_PLAYER_WARP_DURING_HANGAR_UPGRADE - Player is walking out of the hangar, returning FALSE.")
		RETURN FALSE
	ENDIF
	
	IF thisHangar.bWarpDuringUpgrade
		RETURN TRUE
	ENDIF
	
	IF playerID != INVALID_PLAYER_INDEX()
		IF playerID = thisHangar.pOwner
			IF PERFORM_HANGAR_MAZEBANK_WEBSITE_UPGRADE()
				RETURN TRUE
			ENDIF
			RETURN FALSE
		ENDIF
	ENDIF

	RETURN TRUE
ENDFUNC

FUNC BOOL GET_SAFE_HANGAR_SPAWN_POINT(INT iIndex, VECTOR &vSpawnPoint, FLOAT &fSpawnHeading)
	SWITCH thisHangar.eID
		CASE LSIA_HANGAR_1
		CASE LSIA_HANGAR_A17
		CASE ZANCUDO_HANGAR_A2
		CASE ZANCUDO_HANGAR_3497
		CASE ZANCUDO_HANGAR_3499
			SWITCH iIndex
				CASE 0
				    vSpawnPoint 	= <<-1267.5425, -2967.7156, -49.4895>>
				    fSpawnHeading 	= 180.0000
				    RETURN TRUE
				BREAK
				CASE 1
				    vSpawnPoint 	= <<-1269.0425, -2967.7156, -49.4895>>
				    fSpawnHeading 	= 180.0000
				    RETURN TRUE
				BREAK
				CASE 2
				    vSpawnPoint 	= <<-1266.0425, -2967.7156, -49.4895>>
				    fSpawnHeading 	= 180.0000
				    RETURN TRUE
				BREAK
				CASE 3
				    vSpawnPoint 	= <<-1270.5425, -2967.7156, -49.4895>>
				    fSpawnHeading 	= 180.0000
				    RETURN TRUE
				BREAK
				CASE 4
				    vSpawnPoint 	= <<-1264.5425, -2967.7156, -49.4895>>
				    fSpawnHeading 	= 180.0000
				    RETURN TRUE
				BREAK
				CASE 5
				    vSpawnPoint 	= <<-1272.0425, -2967.7156, -49.4895>>
				    fSpawnHeading 	= 180.0000
				    RETURN TRUE
				BREAK
				CASE 6
				    vSpawnPoint 	= <<-1263.0425, -2967.7156, -49.4895>>
				    fSpawnHeading 	= 180.0000
				    RETURN TRUE
				BREAK
				CASE 7
				    vSpawnPoint 	= <<-1273.5425, -2967.7156, -49.4895>>
				    fSpawnHeading 	= 180.0000
				    RETURN TRUE
				BREAK
				CASE 8
				    vSpawnPoint 	= <<-1267.5425, -2969.2156, -49.4895>>
				    fSpawnHeading 	= 180.0000
				    RETURN TRUE
				BREAK
				CASE 9
				    vSpawnPoint 	= <<-1269.0425, -2969.2156, -49.4895>>
				    fSpawnHeading 	= 180.0000
				    RETURN TRUE
				BREAK
				CASE 10
				    vSpawnPoint 	= <<-1266.0425, -2969.2156, -49.4895>>
				    fSpawnHeading 	= 180.0000
				    RETURN TRUE
				BREAK
				CASE 11
				    vSpawnPoint 	= <<-1270.5425, -2969.2156, -49.4895>>
				    fSpawnHeading 	= 180.0000
				    RETURN TRUE
				BREAK
				CASE 12
				    vSpawnPoint 	= <<-1264.5425, -2969.2156, -49.4895>>
				    fSpawnHeading 	= 180.0000
				    RETURN TRUE
				BREAK
				CASE 13
				    vSpawnPoint 	= <<-1272.0425, -2969.2156, -49.4895>>
				    fSpawnHeading 	= 180.0000
				    RETURN TRUE
				BREAK
				CASE 14
				    vSpawnPoint 	= <<-1263.0425, -2969.2156, -49.4895>>
				    fSpawnHeading 	= 180.0000
				    RETURN TRUE
				BREAK
				CASE 15
				    vSpawnPoint 	= <<-1273.5425, -2969.2156, -49.4895>>
				    fSpawnHeading 	= 180.0000
				    RETURN TRUE
				BREAK
				CASE 16
				    vSpawnPoint 	= <<-1267.5425, -2970.7156, -49.4895>>
				    fSpawnHeading 	= 180.0000
				    RETURN TRUE
				BREAK
				CASE 17
				    vSpawnPoint 	= <<-1269.0425, -2970.7156, -49.4895>>
				    fSpawnHeading 	= 180.0000
				    RETURN TRUE
				BREAK
				CASE 18
				    vSpawnPoint 	= <<-1266.0425, -2970.7156, -49.4895>>
				    fSpawnHeading 	= 180.0000
				    RETURN TRUE
				BREAK
				CASE 19
				    vSpawnPoint 	= <<-1270.5425, -2970.7156, -49.4895>>
				    fSpawnHeading 	= 180.0000
				    RETURN TRUE
				BREAK
				CASE 20
				    vSpawnPoint 	= <<-1264.5425, -2970.7156, -49.4895>>
				    fSpawnHeading 	= 180.0000
				    RETURN TRUE
				BREAK
				CASE 21
				    vSpawnPoint 	= <<-1272.0425, -2970.7156, -49.4895>>
				    fSpawnHeading 	= 180.0000
				    RETURN TRUE
				BREAK
				CASE 22
				    vSpawnPoint 	= <<-1263.0425, -2970.7156, -49.4895>>
				    fSpawnHeading 	= 180.0000
				    RETURN TRUE
				BREAK
				CASE 23
				    vSpawnPoint 	= <<-1273.5425, -2970.7156, -49.4895>>
				    fSpawnHeading 	= 180.0000
				    RETURN TRUE
				BREAK
				CASE 24
				    vSpawnPoint 	= <<-1267.5425, -2972.2156, -49.4895>>
				    fSpawnHeading 	= 180.0000
				    RETURN TRUE
				BREAK
				CASE 25
				    vSpawnPoint 	= <<-1269.0425, -2972.2156, -49.4895>>
				    fSpawnHeading 	= 180.0000
				    RETURN TRUE
				BREAK
				CASE 26
				    vSpawnPoint 	= <<-1266.0425, -2972.2156, -49.4895>>
				    fSpawnHeading 	= 180.0000
				    RETURN TRUE
				BREAK
				CASE 27
				    vSpawnPoint 	= <<-1270.5425, -2972.2156, -49.4895>>
				    fSpawnHeading 	= 180.0000
				    RETURN TRUE
				BREAK
				CASE 28
				    vSpawnPoint 	= <<-1264.5425, -2972.2156, -49.4895>>
				    fSpawnHeading 	= 180.0000
				    RETURN TRUE
				BREAK
				CASE 29
				    vSpawnPoint 	= <<-1272.0425, -2972.2156, -49.4895>>
				    fSpawnHeading 	= 180.0000
				    RETURN TRUE
				BREAK
				CASE 30
				    vSpawnPoint 	= <<-1263.0425, -2972.2156, -49.4895>>
				    fSpawnHeading 	= 180.0000
				    RETURN TRUE
				BREAK
				CASE 31
				    vSpawnPoint 	= <<-1273.5425, -2972.2156, -49.4895>>
				    fSpawnHeading 	= 180.0000
				    RETURN TRUE
				BREAK
			ENDSWITCH	
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC SET_UP_CUSTOM_SPAWN_POINTS_AFTER_HANGAR_UPGRADE()
	IF NOT g_SpawnData.bUseCustomSpawnPoints
		VECTOR vSpawnPoint
		FLOAT fSpawnHeading, fWeight, fMaxDist

		USE_CUSTOM_SPAWN_POINTS(TRUE, FALSE, TRUE, 0.01, 2, DEFAULT, DEFAULT, 0.01, DEFAULT, DEFAULT, TRUE, TRUE)
		
		SIMPLE_INTERIOR_DETAILS details		
		GET_SIMPLE_INTERIOR_DETAILS(thisHangar.eSimpleInteriorID, details)
		
		INT i
		
		WHILE GET_SAFE_HANGAR_SPAWN_POINT(i, vSpawnPoint, fSpawnHeading)
			IF VDIST(vSpawnPoint, GET_PLAYER_COORDS(PLAYER_ID())) > fMaxDist
				fMaxDist = VDIST(vSpawnPoint, GET_PLAYER_COORDS(PLAYER_ID()))
			ENDIF	
			
			i += 1
		ENDWHILE
		
		i = 0
		
		WHILE GET_SAFE_HANGAR_SPAWN_POINT(i, vSpawnPoint, fSpawnHeading)
			fWeight = 1.0 - (VDIST(GET_PLAYER_COORDS(PLAYER_ID()), vSpawnPoint) / fMaxDist)
			ADD_CUSTOM_SPAWN_POINT(vSpawnPoint, fSpawnHeading, fWeight)

			i += 1
		ENDWHILE
	ENDIF
ENDPROC

// Clean up TV 
PROC CLEANUP_TV_PROP()
	CDEBUG1LN(DEBUG_SAFEHOUSE, "CLEANUP_TV_PROP: Trying to delete MPTVLocal.objOverlay(The TV)")
	CLEANUP_MP_TV(serverBD.MPTVServer, playerBD[NATIVE_TO_INT(PLAYER_ID())].MPTVClient, thisHangar.MPTVLocal, InteriorPropStruct)
ENDPROC

PROC PERFORM_HANGAR_UPGRADE()

	//IF IS_LOCAL_PLAYER_WALKING_OUT_OF_SIMPLE_INTERIOR()
	IF DID_PLAYER_CHOOSE_TO_EXIT_HANGAR()
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - PERFORM_HANGAR_UPGRADE - Exiting procedure, player is walking in or out of the hangar.")	
		
		thisHangar.bWarpDuringUpgrade = FALSE
		thisHangar.bApplyUpgrade = FALSE
		
		EXIT
	ENDIF

	INT iTime = GET_CLOCK_HOURS()
	HANGAR_ID eHangarID = GET_PLAYERS_OWNED_HANGAR(thisHangar.pOwner)
	
	SWITCH thisHangar.eUpgradeState
		CASE HANGAR_UPGRADE_STATE_IDLE
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - PERFORM_HANGAR_UPGRADE - HANGAR_UPGRADE_STATE_IDLE")
			
			IF thisHangar.bApplyUpgrade
				IF (eHangarID != thisHangar.eID)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - PERFORM_HANGAR_UPGRADE - Player does not own ", GET_HANGAR_NAME(GET_SIMPLE_INTERIOR_ID_FROM_HANGAR_ID(eHangarID)), ", exiting PERFORM_HANGAR_UPGRADE")
					thisHangar.bApplyUpgrade = FALSE
					EXIT
				ELSE
					SET_HANGAR_UPGRADE_STATE(HANGAR_UPGRADE_STATE_FADE_OUT)
				ENDIF
			ENDIF
		BREAK
		CASE HANGAR_UPGRADE_STATE_FADE_OUT
			
			CDEBUG1LN(DEBUG_MP_TV, "MAINTAIN_HANGAR_UPGRADES: HANGAR_UPGRADE_STATE_FADE_OUT: CLEANUP_MP_TV_CLIENT")
			IF NOT IS_SCREEN_FADED_OUT() 
			AND NOT IS_SCREEN_FADING_OUT()
				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - PERFORM_HANGAR_UPGRADE - Fading out and disabling player control.")
				DO_SCREEN_FADE_OUT(500)
				
				IF NOT IS_BIT_SET(thisHangar.iBS2, BS_HANGAR_DISABLE_ACTIVITIES_FOR_WARP)	
					SET_BIT(thisHangar.iBS2, BS_HANGAR_DISABLE_ACTIVITIES_FOR_WARP)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - PERFORM_HANGAR_UPGRADE - Setting BS_HANGAR_DISABLE_ACTIVITIES_FOR_WARP")
				ENDIF
				
				
				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - PERFORM_HANGAR_UPGRADE - Setting player control to false.")
			ENDIF
			
			IF IS_SCREEN_FADED_OUT()
				g_SimpleInteriorData.bForceCCTVCleanup = TRUE
				g_bKillTheBed = TRUE
				
				IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - PERFORM_HANGAR_UPGRADE - Clearing ped tasks immediately.")
				ENDIF
				
				SET_HANGAR_UPGRADE_STATE(HANGAR_UPGRADE_STATE_WARP_PLAYER)
			ENDIF
		BREAK
		CASE HANGAR_UPGRADE_STATE_WARP_PLAYER
			
			IF NOT IS_SCREEN_FADED_OUT()
				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - PERFORM_HANGAR_UPGRADE - HANGAR_UPGRADE_STATE_WARP_PLAYER force fade out")
				DO_SCREEN_FADE_OUT(0)
			ENDIF
			
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - PERFORM_HANGAR_UPGRADE - HANGAR_UPGRADE_STATE_FADE_OUT")
			CLEANUP_TV_PROP()
			CLEANUP_MP_TV_CLIENT(playerBD[NATIVE_TO_INT(PLAYER_ID())].MPTVClient, thisHangar.MPTVLocal, InteriorPropStruct)
			TV_SEAT_ACTIVITY_CLEAN_UP_HANGAR()
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - PERFORM_HANGAR_UPGRADE - HANGAR_UPGRADE_STATE_WARP_PLAYER")
			
			IF IS_BROWSER_OPEN()
				CLOSE_WEB_BROWSER()
			ENDIF
			
			IF SHOULD_PLAYER_WARP_DURING_HANGAR_UPGRADE(PLAYER_ID())
				SET_UP_CUSTOM_SPAWN_POINTS_AFTER_HANGAR_UPGRADE()
				
				IF WARP_TO_SPAWN_LOCATION(SPAWN_LOCATION_CUSTOM_SPAWN_POINTS, FALSE, FALSE, FALSE, FALSE, DEFAULT, DEFAULT, FALSE)
					CLEAR_CUSTOM_SPAWN_POINTS()
					USE_CUSTOM_SPAWN_POINTS(FALSE)
					SET_HANGAR_UPGRADE_STATE(HANGAR_UPGRADE_STATE_ACTIVATE_ENTITY_SETS)		
				ENDIF
			ELSE
				SET_HANGAR_UPGRADE_STATE(HANGAR_UPGRADE_STATE_ACTIVATE_ENTITY_SETS)
			ENDIF
		BREAK
		CASE HANGAR_UPGRADE_STATE_ACTIVATE_ENTITY_SETS
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - PERFORM_HANGAR_UPGRADE - HANGAR_UPGRADE_STATE_ACTIVATE_ENTITY_SETS")
			
			IF IS_VALID_INTERIOR(thisHangar.HangarInteriorID)
			
				// Personal Quarters
				IF IS_PLAYER_HANGAR_SAVEBED_TRADITIONAL_PURCHASED(thisHangar.pOwner)
					REMOVE_HANGAR_ENTITY_SETS_BY_TYPE(thisHangar.eID, HANGAR_SET_TYPE_PERSONAL_QUARTERS)
					ADD_HANGAR_ENTITY_SET(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_PERSONAL_QUARTERS_TINT))
					ADD_HANGAR_ENTITY_SET(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_PERSONAL_QUARTERS_TRADITIONAL))
				ENDIF
				
				IF IS_PLAYER_HANGAR_SAVEBED_MODERN_PURCHASED(thisHangar.pOwner)
					REMOVE_HANGAR_ENTITY_SETS_BY_TYPE(thisHangar.eID, HANGAR_SET_TYPE_PERSONAL_QUARTERS)
					ADD_HANGAR_ENTITY_SET(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_PERSONAL_QUARTERS_TINT))
					ADD_HANGAR_ENTITY_SET(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_PERSONAL_QUARTERS_MODERN))
				ENDIF
				
				IF IS_PLAYER_HANGAR_SAVEBED_TRADITIONAL_PURCHASED(thisHangar.pOwner)
				OR IS_PLAYER_HANGAR_SAVEBED_MODERN_PURCHASED(thisHangar.pOwner)
					IF (iTime > 20 OR iTime < 8)
						REMOVE_HANGAR_ENTITY_SET(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_WINDOW_BLINDS_OPEN))
						ADD_HANGAR_ENTITY_SET(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_WINDOW_BLINDS_CLOSED))
					ELSE
						REMOVE_HANGAR_ENTITY_SET(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_WINDOW_BLINDS_CLOSED))
						ADD_HANGAR_ENTITY_SET(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_WINDOW_BLINDS_OPEN))
					ENDIF
				ENDIF
				
				// Mod Area
				IF IS_PLAYER_HANGAR_MODSHOP_PURCHASED(thisHangar.pOwner)
					ADD_HANGAR_ENTITY_SET(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_MOD_SHOP))
				ENDIF
				
				// Office Style
				IF IS_PLAYER_HANGAR_FURNITURE_STANDARD_PURCHASED(thisHangar.pOwner) 
					REMOVE_HANGAR_ENTITY_SETS_BY_TYPE(thisHangar.eID, HANGAR_SET_TYPE_OFFICE_STYLE)
					ADD_HANGAR_ENTITY_SET(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_OFFICE_BASIC))
				ENDIF
				
				IF IS_PLAYER_HANGAR_FURNITURE_TRADITIONAL_PURCHASED(thisHangar.pOwner)
					REMOVE_HANGAR_ENTITY_SETS_BY_TYPE(thisHangar.eID, HANGAR_SET_TYPE_OFFICE_STYLE)
					ADD_HANGAR_ENTITY_SET(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_OFFICE_TRADITIONAL))
				ENDIF
				
				IF IS_PLAYER_HANGAR_FURNITURE_MODERN_PURCHASED(thisHangar.pOwner)
					REMOVE_HANGAR_ENTITY_SETS_BY_TYPE(thisHangar.eID, HANGAR_SET_TYPE_OFFICE_STYLE)
					ADD_HANGAR_ENTITY_SET(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_OFFICE_MODERN))
				ENDIF
				
				// Floor Decals
				IF IS_PLAYER_HANGAR_FLOOR_DECAL_0_PURCHASED(thisHangar.pOwner)
					REMOVE_HANGAR_ENTITY_SETS_BY_TYPE(thisHangar.eID, HANGAR_SET_TYPE_FLOOR_DECAL)
					ADD_HANGAR_ENTITY_SET(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_FLOOR_DECAL_1))
					ADD_HANGAR_ENTITY_SET_TINT(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_FLOOR_DECAL_1), GET_HANGAR_TINT_VALUE(thisHangar.pOwner), TRUE)
				ENDIF
				
				IF IS_PLAYER_HANGAR_FLOOR_DECAL_1_PURCHASED(thisHangar.pOwner)
					REMOVE_HANGAR_ENTITY_SETS_BY_TYPE(thisHangar.eID, HANGAR_SET_TYPE_FLOOR_DECAL)
					ADD_HANGAR_ENTITY_SET(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_FLOOR_DECAL_2))
					ADD_HANGAR_ENTITY_SET_TINT(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_FLOOR_DECAL_2), GET_HANGAR_TINT_VALUE(thisHangar.pOwner), TRUE)
				ENDIF
				
				IF IS_PLAYER_HANGAR_FLOOR_DECAL_2_PURCHASED(thisHangar.pOwner)
					REMOVE_HANGAR_ENTITY_SETS_BY_TYPE(thisHangar.eID, HANGAR_SET_TYPE_FLOOR_DECAL)
					ADD_HANGAR_ENTITY_SET(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_FLOOR_DECAL_3))
					ADD_HANGAR_ENTITY_SET_TINT(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_FLOOR_DECAL_3), GET_HANGAR_TINT_VALUE(thisHangar.pOwner), TRUE)
				ENDIF
				
				IF IS_PLAYER_HANGAR_FLOOR_DECAL_3_PURCHASED(thisHangar.pOwner)
					REMOVE_HANGAR_ENTITY_SETS_BY_TYPE(thisHangar.eID, HANGAR_SET_TYPE_FLOOR_DECAL)
					ADD_HANGAR_ENTITY_SET(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_FLOOR_DECAL_4))
					ADD_HANGAR_ENTITY_SET_TINT(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_FLOOR_DECAL_4), GET_HANGAR_TINT_VALUE(thisHangar.pOwner), TRUE)
				ENDIF
				
				IF IS_PLAYER_HANGAR_FLOOR_DECAL_4_PURCHASED(thisHangar.pOwner)
					REMOVE_HANGAR_ENTITY_SETS_BY_TYPE(thisHangar.eID, HANGAR_SET_TYPE_FLOOR_DECAL)
					ADD_HANGAR_ENTITY_SET(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_FLOOR_DECAL_5))
					ADD_HANGAR_ENTITY_SET_TINT(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_FLOOR_DECAL_5), GET_HANGAR_TINT_VALUE(thisHangar.pOwner), TRUE)
				ENDIF
				
				IF IS_PLAYER_HANGAR_FLOOR_DECAL_5_PURCHASED(thisHangar.pOwner)
					REMOVE_HANGAR_ENTITY_SETS_BY_TYPE(thisHangar.eID, HANGAR_SET_TYPE_FLOOR_DECAL)
					ADD_HANGAR_ENTITY_SET(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_FLOOR_DECAL_6))
					ADD_HANGAR_ENTITY_SET_TINT(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_FLOOR_DECAL_6), GET_HANGAR_TINT_VALUE(thisHangar.pOwner), TRUE)
				ENDIF
				
				IF IS_PLAYER_HANGAR_FLOOR_DECAL_6_PURCHASED(thisHangar.pOwner)
					REMOVE_HANGAR_ENTITY_SETS_BY_TYPE(thisHangar.eID, HANGAR_SET_TYPE_FLOOR_DECAL)
					ADD_HANGAR_ENTITY_SET(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_FLOOR_DECAL_7))
					ADD_HANGAR_ENTITY_SET_TINT(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_FLOOR_DECAL_7), GET_HANGAR_TINT_VALUE(thisHangar.pOwner), TRUE)
				ENDIF
				
				IF IS_PLAYER_HANGAR_FLOOR_DECAL_7_PURCHASED(thisHangar.pOwner)
					REMOVE_HANGAR_ENTITY_SETS_BY_TYPE(thisHangar.eID, HANGAR_SET_TYPE_FLOOR_DECAL)
					ADD_HANGAR_ENTITY_SET(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_FLOOR_DECAL_8))
					ADD_HANGAR_ENTITY_SET_TINT(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_FLOOR_DECAL_8), GET_HANGAR_TINT_VALUE(thisHangar.pOwner), TRUE)
				ENDIF
				
				IF IS_PLAYER_HANGAR_FLOOR_DECAL_8_PURCHASED(thisHangar.pOwner)
					REMOVE_HANGAR_ENTITY_SETS_BY_TYPE(thisHangar.eID, HANGAR_SET_TYPE_FLOOR_DECAL)
					ADD_HANGAR_ENTITY_SET(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_FLOOR_DECAL_9))
					ADD_HANGAR_ENTITY_SET_TINT(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_FLOOR_DECAL_9), GET_HANGAR_TINT_VALUE(thisHangar.pOwner), TRUE)
				ENDIF
				
				// Hangar Style
				IF IS_PLAYER_HANGAR_STYLE_0_PURCHASED(thisHangar.pOwner)
					
					REMOVE_HANGAR_ENTITY_SETS_BY_TYPE(thisHangar.eID, HANGAR_SET_TYPE_FLOOR)
					ADD_HANGAR_ENTITY_SET(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_FLOOR_CONCRETE))
					
					IF IS_PLAYER_HANGAR_LIGHTING_0_PURCHASED(thisHangar.pOwner)
						REMOVE_HANGAR_ENTITY_SETS_BY_TYPE(thisHangar.eID, HANGAR_SET_TYPE_LIGHTING)
						ADD_HANGAR_ENTITY_SET(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_LIGHTING_A))
						ADD_HANGAR_ENTITY_SET(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_WALL_LIGHT_NEUTRAL))
						ADD_HANGAR_ENTITY_SET_TINT(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_LIGHT_TINT_PROPS), 10, TRUE)
					ENDIF
					
					IF IS_PLAYER_HANGAR_LIGHTING_1_PURCHASED(thisHangar.pOwner)
						REMOVE_HANGAR_ENTITY_SETS_BY_TYPE(thisHangar.eID, HANGAR_SET_TYPE_LIGHTING)
						ADD_HANGAR_ENTITY_SET(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_LIGHTING_C))
						ADD_HANGAR_ENTITY_SET(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_WALL_LIGHT_TINT_1))
						ADD_HANGAR_ENTITY_SET_TINT(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_LIGHT_TINT_PROPS), 1, TRUE)
					ENDIF
				ENDIF
				
				IF IS_PLAYER_HANGAR_STYLE_1_PURCHASED(thisHangar.pOwner)
					
					REMOVE_HANGAR_ENTITY_SETS_BY_TYPE(thisHangar.eID, HANGAR_SET_TYPE_FLOOR)
					ADD_HANGAR_ENTITY_SET(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_FLOOR_CONCRETE))
					
					IF IS_PLAYER_HANGAR_LIGHTING_0_PURCHASED(thisHangar.pOwner)
						REMOVE_HANGAR_ENTITY_SETS_BY_TYPE(thisHangar.eID, HANGAR_SET_TYPE_LIGHTING)
						ADD_HANGAR_ENTITY_SET(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_LIGHTING_C))
						ADD_HANGAR_ENTITY_SET(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_WALL_LIGHT_NEUTRAL))
						ADD_HANGAR_ENTITY_SET_TINT(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_LIGHT_TINT_PROPS), 10, TRUE)
					ENDIF
					
					IF IS_PLAYER_HANGAR_LIGHTING_1_PURCHASED(thisHangar.pOwner)
						REMOVE_HANGAR_ENTITY_SETS_BY_TYPE(thisHangar.eID, HANGAR_SET_TYPE_LIGHTING)
						ADD_HANGAR_ENTITY_SET(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_LIGHTING_A))
						ADD_HANGAR_ENTITY_SET(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_WALL_LIGHT_TINT_2))
						ADD_HANGAR_ENTITY_SET_TINT(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_LIGHT_TINT_PROPS), 2, TRUE)
					ENDIF
				ENDIF
				
				IF IS_PLAYER_HANGAR_STYLE_2_PURCHASED(thisHangar.pOwner)
					
					REMOVE_HANGAR_ENTITY_SETS_BY_TYPE(thisHangar.eID, HANGAR_SET_TYPE_FLOOR)
					ADD_HANGAR_ENTITY_SET(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_FLOOR_CONCRETE))
					
					IF IS_PLAYER_HANGAR_LIGHTING_0_PURCHASED(thisHangar.pOwner)
						REMOVE_HANGAR_ENTITY_SETS_BY_TYPE(thisHangar.eID, HANGAR_SET_TYPE_LIGHTING)
						ADD_HANGAR_ENTITY_SET(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_LIGHTING_B))
						ADD_HANGAR_ENTITY_SET(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_WALL_LIGHT_NEUTRAL))
						ADD_HANGAR_ENTITY_SET_TINT(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_LIGHT_TINT_PROPS), 10, TRUE)
					ENDIF
					
					IF IS_PLAYER_HANGAR_LIGHTING_1_PURCHASED(thisHangar.pOwner)
						REMOVE_HANGAR_ENTITY_SETS_BY_TYPE(thisHangar.eID, HANGAR_SET_TYPE_LIGHTING)
						ADD_HANGAR_ENTITY_SET(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_LIGHTING_A))
						ADD_HANGAR_ENTITY_SET(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_WALL_LIGHT_TINT_3))
						ADD_HANGAR_ENTITY_SET_TINT(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_LIGHT_TINT_PROPS), 3, TRUE)
					ENDIF
				ENDIF
				
				IF IS_PLAYER_HANGAR_STYLE_3_PURCHASED(thisHangar.pOwner)
					
					REMOVE_HANGAR_ENTITY_SETS_BY_TYPE(thisHangar.eID, HANGAR_SET_TYPE_FLOOR)
					ADD_HANGAR_ENTITY_SET(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_FLOOR_EPOXY))
					
					IF IS_PLAYER_HANGAR_LIGHTING_0_PURCHASED(thisHangar.pOwner)
						REMOVE_HANGAR_ENTITY_SETS_BY_TYPE(thisHangar.eID, HANGAR_SET_TYPE_LIGHTING)
						ADD_HANGAR_ENTITY_SET(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_LIGHTING_C))
						ADD_HANGAR_ENTITY_SET(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_WALL_LIGHT_NEUTRAL))
						ADD_HANGAR_ENTITY_SET_TINT(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_LIGHT_TINT_PROPS), 10, TRUE)
					ENDIF
					
					IF IS_PLAYER_HANGAR_LIGHTING_1_PURCHASED(thisHangar.pOwner)
						REMOVE_HANGAR_ENTITY_SETS_BY_TYPE(thisHangar.eID, HANGAR_SET_TYPE_LIGHTING)
						ADD_HANGAR_ENTITY_SET(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_LIGHTING_A))
						ADD_HANGAR_ENTITY_SET(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_WALL_LIGHT_TINT_4))
						ADD_HANGAR_ENTITY_SET_TINT(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_LIGHT_TINT_PROPS), 4, TRUE)
					ENDIF
				ENDIF
				
				IF IS_PLAYER_HANGAR_STYLE_4_PURCHASED(thisHangar.pOwner)
					
					REMOVE_HANGAR_ENTITY_SETS_BY_TYPE(thisHangar.eID, HANGAR_SET_TYPE_FLOOR)
					ADD_HANGAR_ENTITY_SET(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_FLOOR_EPOXY))

					IF IS_PLAYER_HANGAR_LIGHTING_0_PURCHASED(thisHangar.pOwner)
						REMOVE_HANGAR_ENTITY_SETS_BY_TYPE(thisHangar.eID, HANGAR_SET_TYPE_LIGHTING)
						ADD_HANGAR_ENTITY_SET(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_LIGHTING_C))
						ADD_HANGAR_ENTITY_SET(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_WALL_LIGHT_NEUTRAL))
						ADD_HANGAR_ENTITY_SET_TINT(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_LIGHT_TINT_PROPS), 10, TRUE)
					ENDIF
					
					IF IS_PLAYER_HANGAR_LIGHTING_1_PURCHASED(thisHangar.pOwner)
						REMOVE_HANGAR_ENTITY_SETS_BY_TYPE(thisHangar.eID, HANGAR_SET_TYPE_LIGHTING)
						ADD_HANGAR_ENTITY_SET(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_LIGHTING_A))
						ADD_HANGAR_ENTITY_SET(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_WALL_LIGHT_TINT_5))
						ADD_HANGAR_ENTITY_SET_TINT(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_LIGHT_TINT_PROPS), 5, TRUE)
					ENDIF
				ENDIF
				
				IF IS_PLAYER_HANGAR_STYLE_5_PURCHASED(thisHangar.pOwner)
					
					REMOVE_HANGAR_ENTITY_SETS_BY_TYPE(thisHangar.eID, HANGAR_SET_TYPE_FLOOR)
					ADD_HANGAR_ENTITY_SET(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_FLOOR_EPOXY))

					IF IS_PLAYER_HANGAR_LIGHTING_0_PURCHASED(thisHangar.pOwner)
						REMOVE_HANGAR_ENTITY_SETS_BY_TYPE(thisHangar.eID, HANGAR_SET_TYPE_LIGHTING)
						ADD_HANGAR_ENTITY_SET(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_LIGHTING_C))
						ADD_HANGAR_ENTITY_SET(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_WALL_LIGHT_NEUTRAL))
						ADD_HANGAR_ENTITY_SET_TINT(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_LIGHT_TINT_PROPS), 10, TRUE)
					ENDIF
					
					IF IS_PLAYER_HANGAR_LIGHTING_1_PURCHASED(thisHangar.pOwner)
						REMOVE_HANGAR_ENTITY_SETS_BY_TYPE(thisHangar.eID, HANGAR_SET_TYPE_LIGHTING)
						ADD_HANGAR_ENTITY_SET(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_LIGHTING_B))
						ADD_HANGAR_ENTITY_SET(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_WALL_LIGHT_TINT_6))
						ADD_HANGAR_ENTITY_SET_TINT(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_LIGHT_TINT_PROPS), 6, TRUE)
					ENDIF
				ENDIF
				
				IF IS_PLAYER_HANGAR_STYLE_6_PURCHASED(thisHangar.pOwner)
				OR IS_PLAYER_HANGAR_STYLE_7_PURCHASED(thisHangar.pOwner)
				OR IS_PLAYER_HANGAR_STYLE_8_PURCHASED(thisHangar.pOwner)
					
					REMOVE_HANGAR_ENTITY_SETS_BY_TYPE(thisHangar.eID, HANGAR_SET_TYPE_FLOOR)
					
					IF IS_PLAYER_HANGAR_STYLE_6_PURCHASED(thisHangar.pOwner)
						ADD_HANGAR_ENTITY_SET(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_FLOOR_CONCRETE))
					ELSE
						ADD_HANGAR_ENTITY_SET(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_FLOOR_EPOXY))
					ENDIF

					IF IS_PLAYER_HANGAR_LIGHTING_0_PURCHASED(thisHangar.pOwner)
						REMOVE_HANGAR_ENTITY_SETS_BY_TYPE(thisHangar.eID, HANGAR_SET_TYPE_LIGHTING)
						ADD_HANGAR_ENTITY_SET(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_LIGHTING_C))
						ADD_HANGAR_ENTITY_SET(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_WALL_LIGHT_NEUTRAL))
						ADD_HANGAR_ENTITY_SET_TINT(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_LIGHT_TINT_PROPS), 10, TRUE)
					ENDIF
					
					IF IS_PLAYER_HANGAR_LIGHTING_1_PURCHASED(thisHangar.pOwner)
						REMOVE_HANGAR_ENTITY_SETS_BY_TYPE(thisHangar.eID, HANGAR_SET_TYPE_LIGHTING)
						ADD_HANGAR_ENTITY_SET(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_LIGHTING_C))
						ADD_HANGAR_ENTITY_SET(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_WALL_LIGHT_TINT_7))
						ADD_HANGAR_ENTITY_SET_TINT(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_LIGHT_TINT_PROPS), 7, TRUE)
					ENDIF
					
					IF IS_PLAYER_HANGAR_LIGHTING_2_PURCHASED(thisHangar.pOwner)
						REMOVE_HANGAR_ENTITY_SETS_BY_TYPE(thisHangar.eID, HANGAR_SET_TYPE_LIGHTING)
						ADD_HANGAR_ENTITY_SET(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_LIGHTING_C))
						ADD_HANGAR_ENTITY_SET(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_WALL_LIGHT_TINT_8))
						ADD_HANGAR_ENTITY_SET_TINT(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_LIGHT_TINT_PROPS), 8, TRUE)
					ENDIF
					
					IF IS_PLAYER_HANGAR_LIGHTING_3_PURCHASED(thisHangar.pOwner)
						REMOVE_HANGAR_ENTITY_SETS_BY_TYPE(thisHangar.eID, HANGAR_SET_TYPE_LIGHTING)
						ADD_HANGAR_ENTITY_SET(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_LIGHTING_C))
						ADD_HANGAR_ENTITY_SET(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_WALL_LIGHT_TINT_9))
						ADD_HANGAR_ENTITY_SET_TINT(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_LIGHT_TINT_PROPS), 9, TRUE)
					ENDIF
				ENDIF
				
				// Core Tints
				ADD_HANGAR_ENTITY_SET_TINT(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_SHELL), GET_HANGAR_TINT_VALUE(thisHangar.pOwner), TRUE)
				ADD_HANGAR_ENTITY_SET_TINT(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_CRANE_TINT), GET_HANGAR_TINT_VALUE(thisHangar.pOwner), TRUE)
				ADD_HANGAR_ENTITY_SET_TINT(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_FLOOR_EPOXY), GET_HANGAR_TINT_VALUE(thisHangar.pOwner), TRUE)
				ADD_HANGAR_ENTITY_SET_TINT(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_PERSONAL_QUARTERS_TINT), GET_HANGAR_TINT_VALUE(thisHangar.pOwner), TRUE)
				ADD_HANGAR_ENTITY_SET_TINT(thisHangar.eID, GET_HANGAR_ENTITY_SET(HANGAR_SET_MOD_SHOP), GET_HANGAR_TINT_VALUE(thisHangar.pOwner), TRUE)
			ENDIF
			
			SET_HANGAR_UPGRADE_STATE(HANGAR_UPGRADE_STATE_REFRESH_INTERIOR)
		BREAK
		CASE HANGAR_UPGRADE_STATE_REFRESH_INTERIOR
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - PERFORM_HANGAR_UPGRADE - HANGAR_UPGRADE_STATE_REFRESH_INTERIOR")
			
			REFRESH_INTERIOR(thisHangar.HangarInteriorID)
			
			SET_HANGAR_UPGRADE_STATE(HANGAR_UPGRADE_STATE_REFRESH_LIGHTING)
		BREAK
		CASE HANGAR_UPGRADE_STATE_REFRESH_LIGHTING
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - PERFORM_HANGAR_UPGRADE - HANGAR_UPGRADE_STATE_REFRESH_LIGHTING")

			CLEAR_TIMECYCLE_MODIFIER()

			IF IS_BIT_SET(thisHangar.iBS2, BS_HANGAR_INIT_HANGAR_TIMECYCLE_MODS)
				CLEAR_BIT(thisHangar.iBS2, BS_HANGAR_INIT_HANGAR_TIMECYCLE_MODS)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "M_MP_HANGAR - PERFORM_HANGAR_UPGRADE - Clearing BS_HANGAR_INIT_HANGAR_TIMECYCLE_MODS")
			ENDIF
			
			SET_HANGAR_UPGRADE_STATE(HANGAR_UPGRADE_STATE_WAIT_FOR_VEHICLES)
		BREAK
		CASE HANGAR_UPGRADE_STATE_WAIT_FOR_VEHICLES
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - PERFORM_HANGAR_UPGRADE - HANGAR_UPGRADE_STATE_WAIT_FOR_VEHICLES")
			
			IF serverBD.bHangarVehiclesCreated
				SET_HANGAR_UPGRADE_STATE(HANGAR_UPGRADE_STATE_FADE_IN)
			ENDIF
		BREAK
		CASE HANGAR_UPGRADE_STATE_FADE_IN
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - PERFORM_HANGAR_UPGRADE - HANGAR_UPGRADE_STATE_FADE_IN")
			
			// TV Reset stuff to reset the render target for the new tv model
			CLEANUP_TV_PROP()
			CLEANUP_MP_TV_CLIENT(playerBD[NATIVE_TO_INT(PLAYER_ID())].MPTVClient, thisHangar.MPTVLocal, InteriorPropStruct)
			thisHangar.activityTVSeatStruct.officeSeatActState = OFFICE_SEAT_LOCATECHECK
			
			CDEBUG1LN(DEBUG_MP_TV, "MAINTAIN_HANGAR_UPGRADES: CLEANUP_MP_TV_CLIENT")
			
			IF IS_INTERIOR_READY(thisHangar.HangarInteriorID)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - PERFORM_HANGAR_UPGRADE - Interior ready.")
				
				IF IS_SCREEN_FADED_OUT()
					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - PERFORM_HANGAR_UPGRADE - Screen has faded out.")
					
					IF HAS_NET_TIMER_EXPIRED(thisHangar.upgradeTimer, 2000)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - PERFORM_HANGAR_UPGRADE - Net timer has expired.")
						
						SET_LOCAL_PLAYER_BROADCAST_BIT(BS_HANGAR_PERSONAL_QUARTERS_TRADITIONAL_PURCHASED, IS_PLAYER_HANGAR_SAVEBED_TRADITIONAL_PURCHASED(thisHangar.pOwner))
						SET_LOCAL_PLAYER_BROADCAST_BIT(BS_HANGAR_PERSONAL_QUARTERS_MODERN_PURCHASED, IS_PLAYER_HANGAR_SAVEBED_MODERN_PURCHASED(thisHangar.pOwner))
						SET_LOCAL_PLAYER_BROADCAST_BIT(BS_HANGAR_MODSHOP_PURCHASED, IS_PLAYER_HANGAR_MODSHOP_PURCHASED(thisHangar.pOwner))
						SET_LOCAL_PLAYER_BROADCAST_BIT(BS_HANGAR_FURNITURE_STANDARD_PURCHASED, IS_PLAYER_HANGAR_FURNITURE_STANDARD_PURCHASED(thisHangar.pOwner))
						SET_LOCAL_PLAYER_BROADCAST_BIT(BS_HANGAR_FURNITURE_TRADITIONAL_PURCHASED, IS_PLAYER_HANGAR_FURNITURE_TRADITIONAL_PURCHASED(thisHangar.pOwner))
						SET_LOCAL_PLAYER_BROADCAST_BIT(BS_HANGAR_FURNITURE_MODERN_PURCHASED, IS_PLAYER_HANGAR_FURNITURE_MODERN_PURCHASED(thisHangar.pOwner))
						SET_LOCAL_PLAYER_BROADCAST_BIT(BS_HANGAR_FLOOR_DECAL_0_PURCHASED, IS_PLAYER_HANGAR_FLOOR_DECAL_0_PURCHASED(thisHangar.pOwner))
						SET_LOCAL_PLAYER_BROADCAST_BIT(BS_HANGAR_FLOOR_DECAL_1_PURCHASED, IS_PLAYER_HANGAR_FLOOR_DECAL_1_PURCHASED(thisHangar.pOwner))
						SET_LOCAL_PLAYER_BROADCAST_BIT(BS_HANGAR_FLOOR_DECAL_2_PURCHASED, IS_PLAYER_HANGAR_FLOOR_DECAL_2_PURCHASED(thisHangar.pOwner))
						SET_LOCAL_PLAYER_BROADCAST_BIT(BS_HANGAR_FLOOR_DECAL_3_PURCHASED, IS_PLAYER_HANGAR_FLOOR_DECAL_3_PURCHASED(thisHangar.pOwner))
						SET_LOCAL_PLAYER_BROADCAST_BIT(BS_HANGAR_FLOOR_DECAL_4_PURCHASED, IS_PLAYER_HANGAR_FLOOR_DECAL_4_PURCHASED(thisHangar.pOwner))
						SET_LOCAL_PLAYER_BROADCAST_BIT(BS_HANGAR_FLOOR_DECAL_5_PURCHASED, IS_PLAYER_HANGAR_FLOOR_DECAL_5_PURCHASED(thisHangar.pOwner))
						SET_LOCAL_PLAYER_BROADCAST_BIT(BS_HANGAR_FLOOR_DECAL_6_PURCHASED, IS_PLAYER_HANGAR_FLOOR_DECAL_6_PURCHASED(thisHangar.pOwner))
						SET_LOCAL_PLAYER_BROADCAST_BIT(BS_HANGAR_FLOOR_DECAL_7_PURCHASED, IS_PLAYER_HANGAR_FLOOR_DECAL_7_PURCHASED(thisHangar.pOwner))
						SET_LOCAL_PLAYER_BROADCAST_BIT(BS_HANGAR_FLOOR_DECAL_8_PURCHASED, IS_PLAYER_HANGAR_FLOOR_DECAL_8_PURCHASED(thisHangar.pOwner))
						SET_LOCAL_PLAYER_BROADCAST_BIT(BS_HANGAR_STYLE_0_PURCHASED, IS_PLAYER_HANGAR_STYLE_0_PURCHASED(thisHangar.pOwner))
						SET_LOCAL_PLAYER_BROADCAST_BIT(BS_HANGAR_STYLE_1_PURCHASED, IS_PLAYER_HANGAR_STYLE_1_PURCHASED(thisHangar.pOwner))
						SET_LOCAL_PLAYER_BROADCAST_BIT(BS_HANGAR_STYLE_2_PURCHASED, IS_PLAYER_HANGAR_STYLE_2_PURCHASED(thisHangar.pOwner))
						SET_LOCAL_PLAYER_BROADCAST_BIT(BS_HANGAR_STYLE_3_PURCHASED, IS_PLAYER_HANGAR_STYLE_3_PURCHASED(thisHangar.pOwner))
						SET_LOCAL_PLAYER_BROADCAST_BIT(BS_HANGAR_STYLE_4_PURCHASED, IS_PLAYER_HANGAR_STYLE_4_PURCHASED(thisHangar.pOwner))
						SET_LOCAL_PLAYER_BROADCAST_BIT(BS_HANGAR_STYLE_5_PURCHASED, IS_PLAYER_HANGAR_STYLE_5_PURCHASED(thisHangar.pOwner))
						SET_LOCAL_PLAYER_BROADCAST_BIT(BS_HANGAR_STYLE_6_PURCHASED, IS_PLAYER_HANGAR_STYLE_6_PURCHASED(thisHangar.pOwner))
						SET_LOCAL_PLAYER_BROADCAST_BIT(BS_HANGAR_STYLE_7_PURCHASED, IS_PLAYER_HANGAR_STYLE_7_PURCHASED(thisHangar.pOwner))
						SET_LOCAL_PLAYER_BROADCAST_BIT(BS_HANGAR_STYLE_8_PURCHASED, IS_PLAYER_HANGAR_STYLE_8_PURCHASED(thisHangar.pOwner))
						SET_LOCAL_PLAYER_BROADCAST_BIT(BS_HANGAR_LIGHTING_0_PURCHASED, IS_PLAYER_HANGAR_LIGHTING_0_PURCHASED(thisHangar.pOwner))
						SET_LOCAL_PLAYER_BROADCAST_BIT(BS_HANGAR_LIGHTING_1_PURCHASED, IS_PLAYER_HANGAR_LIGHTING_1_PURCHASED(thisHangar.pOwner))
						SET_LOCAL_PLAYER_BROADCAST_BIT(BS_HANGAR_LIGHTING_2_PURCHASED, IS_PLAYER_HANGAR_LIGHTING_2_PURCHASED(thisHangar.pOwner))
						SET_LOCAL_PLAYER_BROADCAST_BIT(BS_HANGAR_LIGHTING_3_PURCHASED, IS_PLAYER_HANGAR_LIGHTING_3_PURCHASED(thisHangar.pOwner))
						
						IF IS_BIT_SET(thisHangar.iBS2, BS_HANGAR_DISABLE_ACTIVITIES_FOR_WARP)	
							CLEAR_BIT(thisHangar.iBS2, BS_HANGAR_DISABLE_ACTIVITIES_FOR_WARP)
							CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - PERFORM_HANGAR_UPGRADE - Clearing BS_HANGAR_DISABLE_ACTIVITIES_FOR_WARP")
						ENDIF
						
						DO_SCREEN_FADE_IN(500)
						NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						
						IF thisHangar.pOwner = PLAYER_ID()
							SET_PLAYER_HAS_JUST_PURCHASED_HANGAR_UPGRADE(thisHangar.pOwner, thisHangar.eID, thisHangar.eUpgradeID, FALSE)
						ENDIF
						
						g_bKillTheBed = FALSE
						thisHangar.bApplyUpgrade = FALSE
						thisHangar.bWarpDuringUpgrade = FALSE
						CLEAR_BIT(thisHangar.iBS, BS_HANGAR_CALLED_CLEAR_HELP)
						SET_HANGAR_UPGRADE_STATE(HANGAR_UPGRADE_STATE_IDLE)
						RESET_NET_TIMER(thisHangar.upgradeTimer)
					ENDIF
				ELSE
					IF NOT IS_SCREEN_FADING_OUT()
						PRINTLN("AM_MP_HANGAR - PERFORM_HANGAR_UPGRADE - Forcing fade out screen is unexpectedly faded in.")
						DO_SCREEN_FADE_OUT(500)
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC CHECK_ENTITY_SET_IS_ACTIVE(HANGAR_ENTITY_SET_ID eHangarSetID)
	STRING sEntitySetName = GET_HANGAR_ENTITY_SET(eHangarSetID)
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sEntitySetName)
		IF NOT IS_INTERIOR_ENTITY_SET_ACTIVE(thisHangar.HangarInteriorID, sEntitySetName)
			thisHangar.bApplyUpgrade = TRUE
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - CHECK_ENTITY_SET_IS_ACTIVE - Mismatch for entity set: ", sEntitySetName)
		ENDIF
	ELSE
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - CHECK_ENTITY_SET_IS_ACTIVE - sEntitySetName is null or empty.")
	ENDIF
ENDPROC

PROC MAINTAIN_ENTITY_SET_MISMATCH_CHECKS()

	//IF IS_LOCAL_PLAYER_WALKING_OUT_OF_SIMPLE_INTERIOR()
	IF DID_PLAYER_CHOOSE_TO_EXIT_HANGAR()
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - PERFORM_HANGAR_MAZEBANK_WEBSITE_UPGRADE - Terminating procedure as g_SimpleInteriorData.iExitMenuOption = ", g_SimpleInteriorData.iExitMenuOption)
		EXIT
	ENDIF

	IF thisHangar.pOwner != INVALID_PLAYER_INDEX()
	AND thisHangar.pOwner != PLAYER_ID()
		IF (GET_FRAME_COUNT() % 10) = 0
					
			// Personal Quarters
			IF IS_PLAYER_HANGAR_SAVEBED_TRADITIONAL_PURCHASED(thisHangar.pOwner)
			OR IS_PLAYER_HANGAR_SAVEBED_MODERN_PURCHASED(thisHangar.pOwner)
				CHECK_ENTITY_SET_IS_ACTIVE(HANGAR_SET_PERSONAL_QUARTERS_TINT)		
			ENDIF
			
			IF IS_PLAYER_HANGAR_SAVEBED_TRADITIONAL_PURCHASED(thisHangar.pOwner)				
				CHECK_ENTITY_SET_IS_ACTIVE(HANGAR_SET_PERSONAL_QUARTERS_TRADITIONAL)
			ENDIF
			
			IF IS_PLAYER_HANGAR_SAVEBED_MODERN_PURCHASED(thisHangar.pOwner)
				CHECK_ENTITY_SET_IS_ACTIVE(HANGAR_SET_PERSONAL_QUARTERS_MODERN)
			ENDIF
			
			// Mod Area
			IF IS_PLAYER_HANGAR_MODSHOP_PURCHASED(thisHangar.pOwner)
				CHECK_ENTITY_SET_IS_ACTIVE(HANGAR_SET_MOD_SHOP)
			ENDIF
			
			// Office Style
			IF IS_PLAYER_HANGAR_FURNITURE_STANDARD_PURCHASED(thisHangar.pOwner)
				CHECK_ENTITY_SET_IS_ACTIVE(HANGAR_SET_OFFICE_BASIC)
			ENDIF
			
			IF IS_PLAYER_HANGAR_FURNITURE_TRADITIONAL_PURCHASED(thisHangar.pOwner)
				CHECK_ENTITY_SET_IS_ACTIVE(HANGAR_SET_OFFICE_TRADITIONAL)
			ENDIF
			
			IF IS_PLAYER_HANGAR_FURNITURE_MODERN_PURCHASED(thisHangar.pOwner)
				CHECK_ENTITY_SET_IS_ACTIVE(HANGAR_SET_OFFICE_MODERN)
			ENDIF
			
			// Floor Decals
			IF IS_PLAYER_HANGAR_FLOOR_DECAL_0_PURCHASED(thisHangar.pOwner) 
				CHECK_ENTITY_SET_IS_ACTIVE(HANGAR_SET_FLOOR_DECAL_1)
			ENDIF
			
			IF IS_PLAYER_HANGAR_FLOOR_DECAL_1_PURCHASED(thisHangar.pOwner)
				CHECK_ENTITY_SET_IS_ACTIVE(HANGAR_SET_FLOOR_DECAL_2)
			ENDIF
			
			IF IS_PLAYER_HANGAR_FLOOR_DECAL_2_PURCHASED(thisHangar.pOwner)
				CHECK_ENTITY_SET_IS_ACTIVE(HANGAR_SET_FLOOR_DECAL_3)
			ENDIF
			
			IF IS_PLAYER_HANGAR_FLOOR_DECAL_3_PURCHASED(thisHangar.pOwner)
				CHECK_ENTITY_SET_IS_ACTIVE(HANGAR_SET_FLOOR_DECAL_4)
			ENDIF
			
			IF IS_PLAYER_HANGAR_FLOOR_DECAL_4_PURCHASED(thisHangar.pOwner)
				CHECK_ENTITY_SET_IS_ACTIVE(HANGAR_SET_FLOOR_DECAL_5)
			ENDIF
			
			IF IS_PLAYER_HANGAR_FLOOR_DECAL_5_PURCHASED(thisHangar.pOwner)
				CHECK_ENTITY_SET_IS_ACTIVE(HANGAR_SET_FLOOR_DECAL_6)
			ENDIF

			IF IS_PLAYER_HANGAR_FLOOR_DECAL_6_PURCHASED(thisHangar.pOwner)
				CHECK_ENTITY_SET_IS_ACTIVE(HANGAR_SET_FLOOR_DECAL_7)
			ENDIF
			
			IF IS_PLAYER_HANGAR_FLOOR_DECAL_7_PURCHASED(thisHangar.pOwner)
				CHECK_ENTITY_SET_IS_ACTIVE(HANGAR_SET_FLOOR_DECAL_8)
			ENDIF
			
			IF IS_PLAYER_HANGAR_FLOOR_DECAL_8_PURCHASED(thisHangar.pOwner)
				CHECK_ENTITY_SET_IS_ACTIVE(HANGAR_SET_FLOOR_DECAL_9)
			ENDIF
			
			// Hangar Style
			IF IS_PLAYER_HANGAR_STYLE_0_PURCHASED(thisHangar.pOwner)
			
				CHECK_ENTITY_SET_IS_ACTIVE(HANGAR_SET_FLOOR_CONCRETE)

				IF IS_PLAYER_HANGAR_LIGHTING_0_PURCHASED(thisHangar.pOwner)
					CHECK_ENTITY_SET_IS_ACTIVE(HANGAR_SET_WALL_LIGHT_NEUTRAL)
					CHECK_ENTITY_SET_IS_ACTIVE(HANGAR_SET_LIGHTING_A)
				ENDIF
				
				IF IS_PLAYER_HANGAR_LIGHTING_1_PURCHASED(thisHangar.pOwner)
					CHECK_ENTITY_SET_IS_ACTIVE(HANGAR_SET_WALL_LIGHT_TINT_1)
					CHECK_ENTITY_SET_IS_ACTIVE(HANGAR_SET_LIGHTING_C)
				ENDIF
			ENDIF
			
			IF IS_PLAYER_HANGAR_STYLE_1_PURCHASED(thisHangar.pOwner)
				
				CHECK_ENTITY_SET_IS_ACTIVE(HANGAR_SET_FLOOR_CONCRETE)

				IF IS_PLAYER_HANGAR_LIGHTING_0_PURCHASED(thisHangar.pOwner)
					CHECK_ENTITY_SET_IS_ACTIVE(HANGAR_SET_WALL_LIGHT_NEUTRAL)
					CHECK_ENTITY_SET_IS_ACTIVE(HANGAR_SET_LIGHTING_C)
				ENDIF
				
				IF IS_PLAYER_HANGAR_LIGHTING_1_PURCHASED(thisHangar.pOwner)
					CHECK_ENTITY_SET_IS_ACTIVE(HANGAR_SET_WALL_LIGHT_TINT_2)
					CHECK_ENTITY_SET_IS_ACTIVE(HANGAR_SET_LIGHTING_A)
				ENDIF
			ENDIF
			
			IF IS_PLAYER_HANGAR_STYLE_2_PURCHASED(thisHangar.pOwner)

				CHECK_ENTITY_SET_IS_ACTIVE(HANGAR_SET_FLOOR_CONCRETE)

				IF IS_PLAYER_HANGAR_LIGHTING_0_PURCHASED(thisHangar.pOwner)					
					CHECK_ENTITY_SET_IS_ACTIVE(HANGAR_SET_WALL_LIGHT_NEUTRAL)
					CHECK_ENTITY_SET_IS_ACTIVE(HANGAR_SET_LIGHTING_B)
				ENDIF
				
				IF IS_PLAYER_HANGAR_LIGHTING_1_PURCHASED(thisHangar.pOwner)
					CHECK_ENTITY_SET_IS_ACTIVE(HANGAR_SET_WALL_LIGHT_TINT_3)
					CHECK_ENTITY_SET_IS_ACTIVE(HANGAR_SET_LIGHTING_A)
				ENDIF
			ENDIF
			
			IF IS_PLAYER_HANGAR_STYLE_3_PURCHASED(thisHangar.pOwner)

				CHECK_ENTITY_SET_IS_ACTIVE(HANGAR_SET_FLOOR_EPOXY)
				
				IF IS_PLAYER_HANGAR_LIGHTING_0_PURCHASED(thisHangar.pOwner)
					CHECK_ENTITY_SET_IS_ACTIVE(HANGAR_SET_LIGHTING_C)
					CHECK_ENTITY_SET_IS_ACTIVE(HANGAR_SET_WALL_LIGHT_NEUTRAL)
				ENDIF
					
				IF IS_PLAYER_HANGAR_LIGHTING_1_PURCHASED(thisHangar.pOwner)
					CHECK_ENTITY_SET_IS_ACTIVE(HANGAR_SET_LIGHTING_A)
					CHECK_ENTITY_SET_IS_ACTIVE(HANGAR_SET_WALL_LIGHT_TINT_4)
				ENDIF
			ENDIF
			
			IF IS_PLAYER_HANGAR_STYLE_4_PURCHASED(thisHangar.pOwner)
				
				CHECK_ENTITY_SET_IS_ACTIVE(HANGAR_SET_FLOOR_EPOXY)
				
				IF IS_PLAYER_HANGAR_LIGHTING_0_PURCHASED(thisHangar.pOwner)
					CHECK_ENTITY_SET_IS_ACTIVE(HANGAR_SET_LIGHTING_C)
					CHECK_ENTITY_SET_IS_ACTIVE(HANGAR_SET_WALL_LIGHT_NEUTRAL)
				ENDIF
				
				IF IS_PLAYER_HANGAR_LIGHTING_1_PURCHASED(thisHangar.pOwner)
					CHECK_ENTITY_SET_IS_ACTIVE(HANGAR_SET_LIGHTING_A)
					CHECK_ENTITY_SET_IS_ACTIVE(HANGAR_SET_WALL_LIGHT_TINT_5)
				ENDIF
			ENDIF
			
			IF IS_PLAYER_HANGAR_STYLE_5_PURCHASED(thisHangar.pOwner)
				
				CHECK_ENTITY_SET_IS_ACTIVE(HANGAR_SET_FLOOR_EPOXY)
				
				IF IS_PLAYER_HANGAR_LIGHTING_0_PURCHASED(thisHangar.pOwner)
					CHECK_ENTITY_SET_IS_ACTIVE(HANGAR_SET_LIGHTING_C)
					CHECK_ENTITY_SET_IS_ACTIVE(HANGAR_SET_WALL_LIGHT_NEUTRAL)
				ENDIF	
				
				IF IS_PLAYER_HANGAR_LIGHTING_1_PURCHASED(thisHangar.pOwner)
					CHECK_ENTITY_SET_IS_ACTIVE(HANGAR_SET_LIGHTING_B)					
					CHECK_ENTITY_SET_IS_ACTIVE(HANGAR_SET_WALL_LIGHT_TINT_6)
				ENDIF
			ENDIF
			
			IF IS_PLAYER_HANGAR_STYLE_6_PURCHASED(thisHangar.pOwner)
			OR IS_PLAYER_HANGAR_STYLE_7_PURCHASED(thisHangar.pOwner)
			OR IS_PLAYER_HANGAR_STYLE_8_PURCHASED(thisHangar.pOwner)
			
				IF IS_PLAYER_HANGAR_STYLE_6_PURCHASED(thisHangar.pOwner)
					CHECK_ENTITY_SET_IS_ACTIVE(HANGAR_SET_FLOOR_CONCRETE)
				ELSE
					CHECK_ENTITY_SET_IS_ACTIVE(HANGAR_SET_FLOOR_EPOXY)
				ENDIF
				
				CHECK_ENTITY_SET_IS_ACTIVE(HANGAR_SET_LIGHTING_C)
				
				IF IS_PLAYER_HANGAR_LIGHTING_0_PURCHASED(thisHangar.pOwner)
					CHECK_ENTITY_SET_IS_ACTIVE(HANGAR_SET_WALL_LIGHT_NEUTRAL)
				ENDIF
				
				IF IS_PLAYER_HANGAR_LIGHTING_1_PURCHASED(thisHangar.pOwner)
					CHECK_ENTITY_SET_IS_ACTIVE(HANGAR_SET_WALL_LIGHT_TINT_7)
				ENDIF
				
				IF IS_PLAYER_HANGAR_LIGHTING_2_PURCHASED(thisHangar.pOwner)
					CHECK_ENTITY_SET_IS_ACTIVE(HANGAR_SET_WALL_LIGHT_TINT_8)
				ENDIF
				
				IF IS_PLAYER_HANGAR_LIGHTING_3_PURCHASED(thisHangar.pOwner)
					CHECK_ENTITY_SET_IS_ACTIVE(HANGAR_SET_WALL_LIGHT_TINT_9)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_SPAWNING_IN_SIMPLE_INTERIOR()
	RETURN IS_BIT_SET(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_SPAWNING_IN_SIMPLE_INTERIOR)
ENDFUNC

PROC MAINTAIN_HANGAR_UPGRADES()
	IF IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
		CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_HANGAR - MAINTAIN_HANGAR_UPGRADES - Exiting procedure, player is walking in or out of the hangar.")
		EXIT
	ENDIF

	IF (IS_SKYSWOOP_AT_GROUND() OR NOT IS_PLAYER_SPAWNING_IN_SIMPLE_INTERIOR())
	AND !g_TransitionData.bEnteringPropertyCam
		
		IF NOT IS_BIT_SET(thisHangar.iBS2, BS_HANGAR_INIT_HANGAR_TIMECYCLE_MODS)
		AND !thisHangar.bApplyUpgrade
			INITIALISE_HANGAR_TIMECYCLE_MODIFIER(thisHangar.pOwner)
			SET_BIT(thisHangar.iBS2, BS_HANGAR_INIT_HANGAR_TIMECYCLE_MODS)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - MAINTAIN_HANGAR_UPGRADES - Setting BS_HANGAR_INIT_HANGAR_TIMECYCLE_MODS")
		ENDIF		
	ELSE
		IF (GET_FRAME_COUNT() % 60) = 0
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - MAINTAIN_HANGAR_UPGRADES - Unable to apply timecycle modifiers.")
		ENDIF
	ENDIF

	IF NOT thisHangar.bApplyUpgrade
		#IF IS_DEBUG_BUILD
		IF (GET_FRAME_COUNT() % 120) = 0
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - MAINTAIN_HANGAR_UPGRADES - Running checks for entity set mismatches.")
		ENDIF
		#ENDIF
		MAINTAIN_ENTITY_SET_MISMATCH_CHECKS()
	ENDIF
	
	IF PERFORM_HANGAR_MAZEBANK_WEBSITE_UPGRADE()
		IF NOT thisHangar.bApplyUpgrade
			thisHangar.bApplyUpgrade = TRUE	
			
			#IF IS_DEBUG_BUILD
			IF (GET_FRAME_COUNT() % 120) = 0
				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - MAINTAIN_HANGAR_UPGRADES - thisHangar.bApplyUpgrade = ", BOOL_TO_INT(thisHangar.bApplyUpgrade))
			ENDIF
			#ENDIF
		ENDIF
	ENDIF

	IF thisHangar.bApplyUpgrade
		#IF IS_DEBUG_BUILD
		IF (GET_FRAME_COUNT() % 120) = 0
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - MAINTAIN_HANGAR_UPGRADES - Performing Hangar upgrade.")
		ENDIF
		#ENDIF
		PERFORM_HANGAR_UPGRADE()
	ENDIF
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ CUTSCENE SETUP  ╞════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC INITIALISE_POST_SETUP_CUTSCENE_DATA(SIMPLE_CUTSCENE &cutscene, INT iCutsceneHash)
	TEXT_LABEL_23 helpPrefix = "HANGAR_STP_"
	TEXT_LABEL_23 txtHelps[SIMPLE_CUTSCENE_MAX_HELP]
	
	SWITCH iCutsceneHash 
		CASE HANGAR_SETUP_CUTSCENE
			SIMPLE_CUTSCENE_CLEAR_ALL_DATA(cutscene)
			SIMPLE_CUTSCENE_CREATE(cutscene, "hangar_setup_cutscene", thisHangar.eSimpleInteriorID)
			
			SIMPLE_CUTSCENE_ADD_SCENE(cutscene, 5000, "hangar_storage", <<30.7106, 28.5083, 2.7875>>, <<-4.1343, 0.0000, 171.0142>>, 50.0, <<30.7106, 28.5083, 2.7875>>, <<-4.1343, -0.0000, 170.8239>>, 50.0, 0.0500, 500)
			SIMPLE_CUTSCENE_ADD_SCENE(cutscene, 7500, "hangar_laptop", <<27.3036, 14.2312, 7.1156>>, <<-16.5747, -0.0000, 160.0718>>, 50.0, <<26.8041, 13.2793, 6.8951>>, <<-16.8431, -0.0000, 179.6463>>, 50.0, 0.0500, 0, 500)
		BREAK
	ENDSWITCH
	
	INT iStartTime = 0
	INT i
	
	REPEAT cutscene.iScenesCount i
		txtHelps[i] = helpPrefix
		txtHelps[i] += i
		
		IF (i = 0)
			SIMPLE_CUTSCENE_ADD_HELP(cutscene, cutscene.sScenes[i].iFadeInTime, cutscene.sScenes[i].iDuration - cutscene.sScenes[i].iFadeInTime, txtHelps[i])
		ELSE
			iStartTime += cutscene.sScenes[i - 1].iDuration
			SIMPLE_CUTSCENE_ADD_HELP(cutscene, iStartTime, cutscene.sScenes[i].iDuration, txtHelps[i])
		ENDIF
	ENDREPEAT
ENDPROC

FUNC INT GET_HANGAR_CUTSCENE(HANGAR_ID eHangarID)
	UNUSED_PARAMETER(eHangarID)
	
	IF PLAYER_ID() != INVALID_PLAYER_INDEX()
		IF PLAYER_ID() = thisHangar.pOwner
			IF NOT HAS_LOCAL_PLAYER_VIEWED_INTERIOR_CUTSCENE(biFmCut_Hangar_Setup_Cutscene)
				#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_CUTSCENE, "AM_MP_HANGAR - GET_HANGAR_CUTSCENE - Player hasn't viewed setup cutscene, returning cutscene hash.")
				#ENDIF
				RETURN HANGAR_SETUP_CUTSCENE
			ENDIF
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	CDEBUG1LN(DEBUG_CUTSCENE, "AM_MP_HANGAR - GET_HANGAR_CUTSCENE - Returning 0")
	#ENDIF
	
	RETURN 0
ENDFUNC

PROC SUPPRESS_SCREEN_DECORATIONS()

	IF IS_HANGAR_STATE(HANGAR_STATE_LOADING)
		
		IF NOT IS_BIT_SET(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_PREVENT_BIG_MESSAGE_DISABLE)
			IF IS_LOCAL_PLAYER_WALKING_INTO_SIMPLE_INTERIOR()
				SET_DISABLE_RANK_UP_MESSAGE(TRUE)	
			ENDIF
		ENDIF
	
	ELIF IS_HANGAR_STATE(HANGAR_STATE_IDLE)
	
		// Unpause the ticker feed.
		IF THEFEED_IS_PAUSED()
			THEFEED_RESUME()
		ENDIF
	
		SET_DISABLE_RANK_UP_MESSAGE(FALSE)
		
	ELIF IS_HANGAR_STATE(HANGAR_STATE_PLAYING_INTRO_CUTSCENE)
	
		// Pause the ticker feed.
		IF NOT THEFEED_IS_PAUSED()
			THEFEED_PAUSE()
		ENDIF
	
	ELIF IS_HANGAR_STATE(HANGAR_STATE_PLAYING_SETUP_CUTSCENE) 
		
		// Pause the ticker feed.
		IF NOT THEFEED_IS_PAUSED()
			THEFEED_PAUSE()
		ENDIF
		
		HIDE_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_MP_RANK_BAR)
		SET_DISABLE_RANK_UP_MESSAGE(TRUE)
		
	ENDIF	
ENDPROC

PROC REQUEST_HANGAR_INTRO_CUTSCENE_INTERIOR()
	// NOTE: Scene play order:- //gta5_dlc/art/animation/Cutscene/Renders/Smuggler/HANG_INT.mov)
	
	CUTSCENE_SECTION cutsceneSections = CS_SECTION_1 | CS_SECTION_2 | CS_SECTION_3 | CS_SECTION_4 | CS_SECTION_5 | CS_SECTION_6 | CS_SECTION_9 | CS_SECTION_10 | CS_SECTION_11 | CS_SECTION_13 | CS_SECTION_14 | CS_SECTION_19

	IF IS_PLAYER_HANGAR_MODSHOP_PURCHASED(thisHangar.pOwner)
		cutsceneSections = cutsceneSections | CS_SECTION_15 | CS_SECTION_16
	ELSE
		cutsceneSections = cutsceneSections | CS_SECTION_17 | CS_SECTION_18
	ENDIF

	IF IS_PLAYER_HANGAR_PERSONAL_QUARTERS_PURCHASED(thisHangar.pOwner)
		cutsceneSections = cutsceneSections | CS_SECTION_12
	ENDIF

	IF IS_PLAYER_PED_FEMALE(thisHangar.pOwner)
		cutsceneSections = cutsceneSections | CS_SECTION_8
	ELSE
		cutsceneSections = cutsceneSections | CS_SECTION_7
	ENDIF
	
	// Request cutscene for the hangar interior.
	REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("HANG_INT", cutsceneSections)
ENDPROC

PROC REQUEST_HANGAR_INTRO_CUTSCENE_EXTERIOR()
	REQUEST_CUTSCENE("HANG_INT_PLANE")
ENDPROC 

PROC CREATE_HANGAR_INTRO_CUTSCENE_MODEL_HIDES()
	CREATE_MODEL_HIDE(<<-1235.25, -3000.18, -42.72>>, 5.0, INT_TO_ENUM(MODEL_NAMES, HASH("v_ilev_rc_door2")), TRUE)
	CREATE_MODEL_HIDE(<<-1234.43, -2986.97, -41.12>>, 5.0, INT_TO_ENUM(MODEL_NAMES, HASH("v_ilev_rc_door2")), TRUE)
	CREATE_MODEL_HIDE(<<-1239.92, -3001.34, -43.26>>, 5.0, INT_TO_ENUM(MODEL_NAMES, HASH("sm_prop_smug_offchair_01a")), TRUE)
	CREATE_MODEL_HIDE(<<-1294.71, -3005.12, -49.49>>, 5.0, INT_TO_ENUM(MODEL_NAMES, HASH("sm_prop_smug_havok")), TRUE)
ENDPROC

PROC REMOVE_HANGAR_INTRO_CUTSCENE_MODEL_HIDES()
	REMOVE_MODEL_HIDE(<<-1235.25, -3000.18, -42.72>>, 5.0, INT_TO_ENUM(MODEL_NAMES, HASH("v_ilev_rc_door2")))
	REMOVE_MODEL_HIDE(<<-1234.43, -2986.97, -41.12>>, 5.0, INT_TO_ENUM(MODEL_NAMES, HASH("v_ilev_rc_door2")))
	REMOVE_MODEL_HIDE(<<-1239.92, -3001.34, -43.26>>, 5.0, INT_TO_ENUM(MODEL_NAMES, HASH("sm_prop_smug_offchair_01a")))
	REMOVE_MODEL_HIDE(<<-1294.71, -3005.12, -49.49>>, 5.0, INT_TO_ENUM(MODEL_NAMES, HASH("sm_prop_smug_havok")))
ENDPROC

FUNC BOOL CREATE_PLAYER_PED_COPY_FOR_HANGAR_INTRO_CUT()

	IF NOT DOES_ENTITY_EXIST(thisHangar.scenePed)
		IF NETWORK_IS_PLAYER_ACTIVE(thisHangar.pOwner)
			IF NOT IS_PED_INJURED(GET_PLAYER_PED(thisHangar.pOwner))
				MODEL_NAMES mPlayer = GET_ENTITY_MODEL(GET_PLAYER_PED(thisHangar.pOwner))
				PED_TYPE pedType = PEDTYPE_CIVMALE
				
				IF IS_PLAYER_PED_FEMALE(thisHangar.pOwner)
					pedType = PEDTYPE_CIVFEMALE
				ENDIF
				
				thisHangar.scenePed = CREATE_PED(pedType, mPlayer, <<-1266.8011, -2970.3367, -49.4897>>, 180, FALSE, FALSE)
				SET_ENTITY_INVINCIBLE(thisHangar.scenePed, TRUE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(thisHangar.scenePed, TRUE)
				
				CLONE_PED_TO_TARGET(GET_PLAYER_PED(thisHangar.pOwner), thisHangar.scenePed)
				
				//remove helmet
				IF IS_ITEM_A_HELMET(GET_ENTITY_MODEL(thisHangar.scenePed), COMP_TYPE_PROPS, GET_PED_COMP_ITEM_CURRENT_MP(thisHangar.scenePed, COMP_TYPE_PROPS, ENUM_TO_INT(ANCHOR_HEAD)))
					CLEAR_PED_PROP(thisHangar.scenePed, ANCHOR_HEAD)
				ENDIF
				
				//remove oxy tube
				IF IS_PED_WEARING_PILOT_SUIT(thisHangar.scenePed, PED_COMP_TEETH)
					SET_PED_COMPONENT_VARIATION(thisHangar.scenePed, PED_COMP_TEETH, 0, 0)
				ENDIF
			ELSE
				PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [MAINTAIN_HANGAR_INTRO_CUTSCENE_STATE] [CREATE_PLAYER_PED_COPY_FOR_HANGAR_INTRO_CUT] Owner injured!")
			ENDIF
		ELSE
			PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [MAINTAIN_HANGAR_INTRO_CUTSCENE_STATE] [CREATE_PLAYER_PED_COPY_FOR_HANGAR_INTRO_CUT] Owner not active!")
		ENDIF
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(thisHangar.scenePed)
		RETURN FALSE
	ENDIF
	
	IF NOT IS_PED_INJURED(thisHangar.scenePed)
		IF NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(thisHangar.scenePed)
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL CREATE_DEAD_PED_FOR_HANGAR_INTRO_CUT()

	IF NOT DOES_ENTITY_EXIST(thisHangar.sceneDeadPed)
		IF REQUEST_LOAD_MODEL(S_M_M_FIBOFFICE_02)
			thisHangar.sceneDeadPed = CREATE_PED(PEDTYPE_CIVMALE, S_M_M_FIBOFFICE_02, <<-1266.8011, -2970.3367, -49.4897>>, 180, FALSE, FALSE)
			SET_MODEL_AS_NO_LONGER_NEEDED(S_M_M_FIBOFFICE_02)
			SET_ENTITY_INVINCIBLE(thisHangar.sceneDeadPed, TRUE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(thisHangar.sceneDeadPed, TRUE)
			SET_PED_COMPONENT_VARIATION(thisHangar.sceneDeadPed, PED_COMP_HEAD, 1, 1)
			SET_PED_COMPONENT_VARIATION(thisHangar.sceneDeadPed, PED_COMP_TORSO, 0, 1)
			SET_PED_COMPONENT_VARIATION(thisHangar.sceneDeadPed, PED_COMP_LEG, 0, 1)
			APPLY_PED_BLOOD_SPECIFIC(thisHangar.sceneDeadPed, ENUM_TO_INT(PDZ_HEAD), 0.550, 0.720, 307.546, 1.000, 3, 0.00, "BulletLarge") 
		ENDIF
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(thisHangar.sceneDeadPed)
		RETURN FALSE
	ENDIF
	
	IF NOT IS_PED_INJURED(thisHangar.sceneDeadPed)
		IF NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(thisHangar.sceneDeadPed)
			PRINTLN("CREATE_DEAD_PED_FOR_HANGAR_INTRO_CUT - streaming...")
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC


PROC RESET_ENTITY_SETS_FOR_INTRO_CUTSCENE(BOOL &bShouldRefresh)

	CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR [RESET_ENTITY_SETS_FOR_INTRO_CUTSCENE] Called....") 
	IF NOT IS_PLAYER_HANGAR_MODSHOP_PURCHASED(thisHangar.pOwner)
	OR NOT IS_PLAYER_HANGAR_PERSONAL_QUARTERS_PURCHASED(thisHangar.pOwner)
		SET_DEFAULT_HANGAR_ENTITY_SETS(thisHangar.eID, thisHangar.pOwner)
		bShouldRefresh = TRUE
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR [RESET_ENTITY_SETS_FOR_INTRO_CUTSCENE] Added blocker")
	ENDIF
	
	IF bShouldRefresh
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR [RESET_ENTITY_SETS_FOR_INTRO_CUTSCENE] REFRESH_INTERIOR")
		REFRESH_INTERIOR(thisHangar.HangarInteriorID)
	ENDIF
ENDPROC

PROC MAINTAIN_POST_SETUP_CUTSCENE()
	IF IS_HANGAR_STATE(HANGAR_STATE_PLAYING_SETUP_CUTSCENE)
	//OR HAS_LOCAL_PLAYER_VIEWED_INTERIOR_CUTSCENE(biFmCut_Hangar_Setup_Cutscene)
		EXIT
	ENDIF
	
	IF NOT thisHangar.setupCutscene.bPlaying
		BOOL bShouldCheck
		INT iCutsceneHash
		
		IF IS_LOCAL_PLAYER_WALKING_INTO_SIMPLE_INTERIOR()
			IF NOT HAS_SIMPLE_INTERIOR_CHILD_SCRIPT_TIMED_OUT()
			AND IS_HANGAR_STATE(HANGAR_STATE_LOADING)
				bShouldCheck = TRUE
			ENDIF
		ELSE
			IF IS_HANGAR_STATE(HANGAR_STATE_IDLE)
				IF NOT IS_LOCAL_PLAYER_WALKING_OUT_OF_SIMPLE_INTERIOR()
				AND NOT IS_BROWSER_OPEN()
				AND NOT IS_SCREEN_FADED_OUT()
				AND NOT IS_MOBILE_PHONE_CALL_ONGOING()
					bShouldCheck = (GET_FRAME_COUNT() % 60 = 0)
				ENDIF
			ENDIF
		ENDIF
		
		IF bShouldCheck
			IF PLAYER_ID() != INVALID_PLAYER_INDEX()
				IF NOT HAS_LOCAL_PLAYER_VIEWED_INTERIOR_CUTSCENE(biFmCut_Hangar_Setup_Cutscene)
				AND HAS_LOCAL_PLAYER_VIEWED_INTERIOR_CUTSCENE(biFmCut_Hangar_Intro_Cutscene)
				AND (IS_HANGAR_SETUP_MISSION_COMPLETE() #IF IS_DEBUG_BUILD OR GET_COMMANDLINE_PARAM_EXISTS("sc_PostSetupCutsceneAlwaysOn") #ENDIF)
					IF PLAYER_ID() = thisHangar.pOwner
						IF NOT IS_PLAYER_IN_SIMPLE_CUTSCENE(PLAYER_ID())
							IF NOT IS_BIT_SET(thisHangar.iBS, BS_HANGAR_DATA_BOSS_WAS_WATCHING_CUTSCENE_LAST_FRAME)
								iCutsceneHash = GET_HANGAR_CUTSCENE(thisHangar.eID)
								#IF IS_DEBUG_BUILD
								CDEBUG1LN(DEBUG_CUTSCENE, "AM_MP_HANGAR - MAINTAIN_POST_SETUP_CUTSCENE - Player was assigned cutscene ", iCutsceneHash)
								#ENDIF
				
								SET_BIT(thisHangar.iBS, BS_HANGAR_DATA_BOSS_WAS_WATCHING_CUTSCENE_LAST_FRAME)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF iCutsceneHash != 0
		AND NOT HAS_LOCAL_PLAYER_VIEWED_INTERIOR_CUTSCENE(biFmCut_Hangar_Setup_Cutscene)
		AND (PLAYER_ID() != INVALID_PLAYER_INDEX() AND PLAYER_ID() = thisHangar.pOwner)
				
			INITIALISE_POST_SETUP_CUTSCENE_DATA(thisHangar.setupCutscene, iCutsceneHash)
			SIMPLE_CUTSCENE_START(thisHangar.setupCutscene)
			
			IF iCutsceneHash = HANGAR_SETUP_CUTSCENE
				SET_BIT(thisHangar.iBS, BS_HANGAR_DATA_PLAYING_SETUP_CUTSCENE)
			ENDIF
			
			IF NOT HAS_SIMPLE_INTERIOR_CHILD_SCRIPT_TIMED_OUT()
			AND IS_HANGAR_STATE(HANGAR_STATE_LOADING)
				SET_SIMPLE_INTERIOR_CHILD_SCRIPT_IS_IN_CONTROL(TRUE)
				SET_SIMPLE_INTERIOR_CHILD_SCRIPT_READY(FALSE)
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_HANGAR_STATE(HANGAR_STATE_IDLE)
	AND thisHangar.setupCutscene.bPlaying
		IF IS_SCREEN_FADED_OUT()
			
			IF NOT IS_BIT_SET(thisHangar.iBS, BS_HANGAR_DATA_DID_INTERIOR_REFRESH_FOR_CUTSCENE)
				SET_HANGAR_STATE(HANGAR_STATE_PLAYING_SETUP_CUTSCENE)
				EXIT
			ELSE
				IF HAS_SIMPLE_INTERIOR_BEEN_REFRESHED()
					IF NOT IS_BIT_SET(thisHangar.iBS, BS_HANGAR_DATA_LOAD_SCENE_FOR_CUTSCENE_STARTED)
						VECTOR vHangarCoords
						FLOAT fHangarHeading, fHangarRadius
						TEXT_LABEL_63 strInteriorType
						
						SIMPLE_INTERIOR_INSIDE_BOUNDING_BOX bounds
						GET_SIMPLE_INTERIOR_INSIDE_BOUNDING_BOX(thisHangar.eSimpleInteriorID, bounds)
						
						fHangarRadius = GET_SIMPLE_INTERIOR_RADIUS_FROM_BOUNDING_BOX(bounds)
						GET_SIMPLE_INTERIOR_INTERIOR_TYPE_AND_POSITION(thisHangar.eSimpleInteriorID, strInteriorType, vHangarCoords, fhangarHEading)
						
						NEW_LOAD_SCENE_START_SPHERE(vHangarCoords, fHangarRadius)
						SET_BIT(thisHangar.iBS, BS_HANGAR_DATA_LOAD_SCENE_FOR_CUTSCENE_STARTED)
						
						#IF IS_DEBUG_BUILD
						CDEBUG1LN(DEBUG_CUTSCENE, "AM_MP_HANGAR - MAINTAIN_POST_SETUP_CUTSCENE - Starting new load scene.")
						#ENDIF
					ELSE
						IF IS_NEW_LOAD_SCENE_ACTIVE()
							IF IS_NEW_LOAD_SCENE_LOADED()
								NEW_LOAD_SCENE_STOP()
								
								#IF IS_DEBUG_BUILD
								CDEBUG1LN(DEBUG_CUTSCENE, "AM_MP_HANGAR - MAINTAIN_POST_SETUP_CUTSCENE - Load scene active and has loaded. Stopping load scene.")
								#ENDIF
								
								SET_HANGAR_STATE(HANGAR_STATE_PLAYING_SETUP_CUTSCENE)
								
								CLEAR_BIT(thisHangar.iBS, BS_HANGAR_DATA_LOAD_SCENE_FOR_CUTSCENE_STARTED)
								CLEAR_BIT(thisHangar.iBS, BS_HANGAR_DATA_DID_INTERIOR_REFRESH_FOR_CUTSCENE)
							ENDIF
						ELSE
							SET_HANGAR_STATE(HANGAR_STATE_PLAYING_SETUP_CUTSCENE)
							
							CLEAR_BIT(thisHangar.iBS, BS_HANGAR_DATA_LOAD_SCENE_FOR_CUTSCENE_STARTED)
							CLEAR_BIT(thisHangar.iBS, BS_HANGAR_DATA_DID_INTERIOR_REFRESH_FOR_CUTSCENE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_HANGAR_POST_SETUP_CUTSCENE_STATE()

	IF SHOULD_LOCAL_PLAYER_BE_AUTOWARPED_INSIDE_THIS_SIMPLE_INTERIOR(thisHangar.eSimpleInteriorID)
		IF NOT IS_SIMPLE_INTERIOR_GLOBAL_BIT_SET(SI_BS_CanFinishAutoWarp, thisHangar.eSimpleInteriorID)
			#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_CUTSCENE, "AM_MP_HANGAR - MAINTAIN_HANGAR_POST_SETUP_CUTSCENE_STATE - Waiting until autowarp has been completed before beginning cutscene.")
			#ENDIF
			EXIT
		ENDIF
	ENDIF
	
	IF NOT IS_LOCAL_PLAYER_WALKING_OUT_OF_SIMPLE_INTERIOR()
		SIMPLE_CUTSCENE_MAINTAIN(thisHangar.setupCutscene)
	
		IF thisHangar.setupCutscene.bPlaying
		
			IF thisHangar.setupCutscene.iCurrentScene > 0
				IF NOT IS_BIT_SET(thisHangar.iBS, BS_HANGAR_CLEAN_CUTSCENE_PRODUCT)
					#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_CUTSCENE, "AM_MP_HANGAR - MAINTAIN_HANGAR_POST_SETUP_CUTSCENE_STATE - Cleaning hangar product.")
					#ENDIF
					CLEANUP_ALL_HANGAR_PRODUCT()
					SET_BIT(thisHangar.iBS, BS_HANGAR_CLEAN_CUTSCENE_PRODUCT)
				ENDIF
			ENDIF
		
			IF thisHangar.setupCutscene.iCurrentScene = 0
				IF NOT IS_BIT_SET(thisHangar.iBS, BS_HANGAR_SPAWN_CUTSCENE_PRODUCT)
					#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_CUTSCENE, "AM_MP_HANGAR - MAINTAIN_HANGAR_POST_SETUP_CUTSCENE_STATE - Spawning hangar product.")
					#ENDIF
					SPAWN_HANGAR_TUTORIAL_PRODUCT(8, TRUE)
					SET_BIT(thisHangar.iBS, BS_HANGAR_SPAWN_CUTSCENE_PRODUCT)
				ENDIF
			ENDIF

		ELSE
			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(SIMPLE_INTERIOR_SCREEN_FADE_TIME)
			ENDIF
			
			IF NOT IS_SIMPLE_INTERIOR_CHILD_SCRIPT_IN_CONTROL()
				NETWORK_FADE_IN_ENTITY(PLAYER_PED_ID(), TRUE)
				FORCE_PLAYER_LOCALLY_VISIBLE_FOR_FADE()
			ELSE
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					IF IS_ENTITY_VISIBLE(PLAYER_PED_ID())
						#IF IS_DEBUG_BUILD 
						CDEBUG1LN(DEBUG_CUTSCENE, "AM_MP_HANGAR - MAINTAIN_HANGAR_POST_SETUP_CUTSCENE_STATE - Player is visible after cutscene. Visibility reset, setting player invisible.")
						#ENDIF
						SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT IS_SIMPLE_INTERIOR_CHILD_SCRIPT_READY()
				SET_SIMPLE_INTERIOR_CHILD_SCRIPT_READY(TRUE)
				SET_SIMPLE_INTERIOR_CHILD_SCRIPT_IS_IN_CONTROL(FALSE)
				g_SimpleInteriorData.bDontSimulateInputGaitOnEntrance = TRUE
			ENDIF
			
			IF thisHangar.pOwner != INVALID_PLAYER_INDEX() AND thisHangar.pOwner = PLAYER_ID()
				IF IS_BIT_SET(thisHangar.iBS, BS_HANGAR_DATA_PLAYING_SETUP_CUTSCENE)
					SET_LOCAL_PLAYER_COMPLETED_HANGAR_SETUP_CUTSCENE(TRUE)
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(thisHangar.iBS, BS_HANGAR_DATA_PLAYING_SETUP_CUTSCENE)
				CLEAR_BIT(thisHangar.iBS, BS_HANGAR_DATA_PLAYING_SETUP_CUTSCENE)
			ENDIF
			
			SET_HANGAR_STATE(HANGAR_STATE_IDLE)
		ENDIF
	ELSE
		IF IS_SCREEN_FADED_OUT()
			IF IS_NEW_LOAD_SCENE_ACTIVE() AND IS_NEW_LOAD_SCENE_LOADED()
				NEW_LOAD_SCENE_STOP()
				#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_CUTSCENE, "AM_MP_HANGAR - MAINTAIN_HANGAR_POST_SETUP_CUTSCENE_STATE - Load scene active, stopping load scene.")
				#ENDIF
			ENDIF
		
			SIMPLE_CUTSCENE_STOP(thisHangar.setupCutscene)
		ENDIF
	ENDIF
ENDPROC

PROC CLEANUP_INTRO_CUTSCENE()

	IF IS_NEW_LOAD_SCENE_ACTIVE()
		NEW_LOAD_SCENE_STOP()	
	ENDIF
	
	IF IS_CUTSCENE_PLAYING()
		STOP_CUTSCENE_IMMEDIATELY()
		REMOVE_CUTSCENE()
	ENDIF
	
	REMOVE_HANGAR_INTRO_CUTSCENE_MODEL_HIDES()
	
	BOOL bClearHelp = FALSE
	
	IF IS_ANY_FLOATING_HELP_BEING_DISPLAYED()
		bClearHelp = TRUE
	ENDIF
	
	CLEANUP_MP_CUTSCENE(bClearHelp, TRUE)
	
	NETWORK_SET_VOICE_ACTIVE(TRUE)
	
	IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBS, BS_HANGAR_WATCHING_INTRO_MOCAP)
		CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iBS, BS_HANGAR_WATCHING_INTRO_MOCAP)
	ENDIF
	
	ENTITY_INDEX entity = thisHangar.scenePed
	
	IF DOES_ENTITY_EXIST(entity)
		DELETE_ENTITY(entity)
	ENDIF
	
	entity = thisHangar.sceneDeadPed
	
	IF DOES_ENTITY_EXIST(entity)
		DELETE_ENTITY(entity)
	ENDIF	

	IF PLAYER_PED_ID() = GET_PLAYER_PED(thisHangar.pOwner)
		SET_ENTITY_VISIBLE(GET_PLAYER_PED(thisHangar.pOwner), TRUE)
	ENDIF

	//Fix for 3877788
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
	AND thisHangar.bMPTVVolumeHasBeenModified
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR [CLEANUP_INTRO_CUTSCENE] Set TV volume to ", thisHangar.fMPTVVolumeBeforeCutscene, " from ", serverBD.MPTVServer.fVolume)
		serverBD.MPTVServer.fVolume = thisHangar.fMPTVVolumeBeforeCutscene
	ENDIF
	
	
	// TV Reset stuff to reset the render target for the new tv model (3887344)
	CLEANUP_TV_PROP()
	CLEANUP_MP_TV_CLIENT(playerBD[NATIVE_TO_INT(PLAYER_ID())].MPTVClient, thisHangar.MPTVLocal, InteriorPropStruct)
	
	CDEBUG1LN(DEBUG_MP_TV, "AM_MP_HANGAR [CLEANUP_INTRO_CUTSCENE] CLEANUP_MP_TV_CLIENT")
	
	thisHangar.bShouldPlayIntroCut = FALSE
	thisHangar.bSceneTimeout = FALSE
	thisHangar.bIdleTimeout = FALSE
ENDPROC

PROC INIT_LOCAL_PLAYER_FOR_INTRO_MOCAP()
	
	IF NOT thisHangar.bInitLocalPlayerForIntroMocap
	
		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBS, BS_HANGAR_WATCHING_INTRO_MOCAP)
			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iBS, BS_HANGAR_WATCHING_INTRO_MOCAP)
		ENDIF
		
		MAKE_PLAYER_SAFE_FOR_MP_CUTSCENE(FALSE, TRUE, TRUE, FALSE)
		
		NETWORK_SET_VOICE_ACTIVE(FALSE)
		CDEBUG1LN(DEBUG_CUTSCENE, "AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE] [INIT_LOCAL_PLAYER_FOR_INTRO_MOCAP] Set")
		thisHangar.bInitLocalPlayerForIntroMocap = TRUE
	ENDIF
ENDPROC

PROC MAINTAIN_HANGAR_INTRO_CUTSCENE_STATE()

	IF HAS_SIMPLE_INTERIOR_CHILD_SCRIPT_TIMED_OUT()
	OR ((IS_SKYSWOOP_MOVING() OR IS_SKYSWOOP_IN_SKY()) AND NOT IS_SKYSWOOP_AT_GROUND())
	OR IS_ANY_TRANSITION_SESSION_INVITE_BEING_ACTIONED()  
	#IF IS_DEBUG_BUILD
	OR IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED()
	#ENDIF
		CLEANUP_INTRO_CUTSCENE()
		#IF IS_DEBUG_BUILD
		IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED()
			IF thisHangar.pOwner = PLAYER_ID()
				SET_HANGAR_INTRO_CUTSCENE_VIEWED()
			ENDIF
		ELSE
		#ENDIF	
			SET_HANGAR_STATE(HANGAR_STATE_LOADING)
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
		
		CDEBUG1LN(DEBUG_CUTSCENE, "AM_MP_HANGAR - MAINTAIN_HANGAR_INTRO_CUTSCENE_STATE - Child script is ready, mocap has not played yet, cleaning up mocap.")
		EXIT
	ENDIF
	
	ENTITY_INDEX sceneEntity
		
	SWITCH thisHangar.eCutsceneState
		CASE HANGAR_CUTSCENE_STATE_INITIALISE
			IF CREATE_PLAYER_PED_COPY_FOR_HANGAR_INTRO_CUT()
			AND CREATE_DEAD_PED_FOR_HANGAR_INTRO_CUT()
				CDEBUG1LN(DEBUG_CUTSCENE, "AM_MP_HANGAR - MAINTAIN_HANGAR_INTRO_CUTSCENE_STATE - HANGAR_CUTSCENE_STATE_INITIALISE")
				
				CREATE_HANGAR_INTRO_CUTSCENE_MODEL_HIDES()
				REQUEST_HANGAR_INTRO_CUTSCENE_INTERIOR()
				INITIALISE_HANGAR_ENTITY_SETS()
				
				//Fix for 3877788
				IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				AND NOT thisHangar.bMPTVVolumeHasBeenModified
					thisHangar.fMPTVVolumeBeforeCutscene = serverBD.MPTVServer.fVolume
					serverBD.MPTVServer.fVolume = 0
					thisHangar.bMPTVVolumeHasBeenModified = TRUE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR [INITIALISE_HANGAR_ENTITY_SETS] Set TV volume to 0 from ", thisHangar.fMPTVVolumeBeforeCutscene)
				ENDIF
				
				SET_HANGAR_CUTSCENE_STATE(HANGAR_CUTSCENE_STATE_ASSET_REQUEST)
			ENDIF
		BREAK
		CASE HANGAR_CUTSCENE_STATE_ASSET_REQUEST
			IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
				SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_1", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
				
				SET_HANGAR_CUTSCENE_STATE(HANGAR_CUTSCENE_STATE_LOAD_INTERIOR)
			ENDIF
			BREAK
		CASE HANGAR_CUTSCENE_STATE_LOAD_INTERIOR
			CDEBUG1LN(DEBUG_CUTSCENE, "AM_MP_HANGAR - MAINTAIN_HANGAR_INTRO_CUTSCENE_STATE - HANGAR_CUTSCENE_STATE_LOAD_INTERIOR")

			IF HAS_CUTSCENE_LOADED()
			AND CAN_REGISTER_MISSION_ENTITIES(0,1,0,0)
				
				IF NOT IS_PED_INJURED(thisHangar.scenePed)
				AND NOT IS_PED_INJURED(thisHangar.sceneDeadPed)
					CDEBUG1LN(DEBUG_CUTSCENE, "AM_MP_HANGAR - MAINTAIN_HANGAR_INTRO_CUTSCENE_STATE - Registering scene ped for cut")
					REGISTER_ENTITY_FOR_CUTSCENE(thisHangar.scenePed, "MP_1", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
					REGISTER_ENTITY_FOR_CUTSCENE(thisHangar.sceneDeadPed, "FIB_corpse1", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME | CEO_PRESERVE_FACE_BLOOD_DAMAGE | CEO_PRESERVE_BODY_BLOOD_DAMAGE | CEO_CLONE_DAMAGE_TO_CS_MODEL)
				ENDIF
				
				// Fix for B*3872025
				RESERVE_NETWORK_MISSION_VEHICLES(GET_NUM_RESERVED_MISSION_VEHICLES(FALSE)+1)
				
				START_CUTSCENE()
				START_MP_CUTSCENE()
				CDEBUG1LN(DEBUG_CUTSCENE, "AM_MP_HANGAR - MAINTAIN_HANGAR_INTRO_CUTSCENE_STATE - Starting interior cutscene.")
				
				SET_HANGAR_CUTSCENE_STATE(HANGAR_CUTSCENE_STATE_IDLING)
			ELSE
				// Wait for cutscene to load.
			ENDIF
		BREAK
		CASE HANGAR_CUTSCENE_STATE_IDLING
			CDEBUG1LN(DEBUG_CUTSCENE, "AM_MP_HANGAR - MAINTAIN_HANGAR_INTRO_CUTSCENE_STATE - HANGAR_CUTSCENE_STATE_IDLING")
		
			IF IS_CUTSCENE_PLAYING()
				
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(500)
					PRINTLN("AM_MP_HANGAR - MAINTAIN_HANGAR_INTRO_CUTSCENE_STATE - Interior scene is playing, fading screen in.")
				ENDIF
				
				IF HAS_NET_TIMER_STARTED(thisHangar.tIdleFallback)
					RESET_NET_TIMER(thisHangar.tIdleFallback)
					CDEBUG1LN(DEBUG_CUTSCENE, "AM_MP_HANGAR - MAINTAIN_HANGAR_INTRO_CUTSCENE_STATE - Resetting idle fallback timer.")
				ENDIF
				
				SET_HANGAR_CUTSCENE_STATE(HANGAR_CUTSCENE_STATE_PLAY_INTERIOR)
			ELSE
				CDEBUG1LN(DEBUG_CUTSCENE, "AM_MP_HANGAR - MAINTAIN_HANGAR_INTRO_CUTSCENE_STATE - Waiting for cutscene playback.")
				
				IF NOT HAS_NET_TIMER_STARTED(thisHangar.tIdleFallback)
					START_NET_TIMER(thisHangar.tIdleFallback)
					CDEBUG1LN(DEBUG_CUTSCENE, "AM_MP_HANGAR - MAINTAIN_HANGAR_INTRO_CUTSCENE_STATE - Starting idle fallback timer.")
				ENDIF
				
				// Idle for 7.5 seconds before killing the cutscene.
				IF HAS_NET_TIMER_EXPIRED(thisHangar.tIdleFallback, 7500) 
					CDEBUG1LN(DEBUG_CUTSCENE, "AM_MP_HANGAR - MAINTAIN_HANGAR_INTRO_CUTSCENE_STATE - Idle fallback timer has expired, bailing on cutscene.")				
					SET_HANGAR_CUTSCENE_STATE(HANGAR_CUTSCENE_STATE_CLEANUP)
				ENDIF
			ENDIF
		BREAK
		CASE HANGAR_CUTSCENE_STATE_PLAY_INTERIOR
			IF (GET_FRAME_COUNT() % 120) = 0
				CDEBUG1LN(DEBUG_CUTSCENE, "AM_MP_HANGAR - MAINTAIN_HANGAR_INTRO_CUTSCENE_STATE - HANGAR_CUTSCENE_STATE_PLAY_INTERIOR")
			ENDIF
			
			IF NOT HAS_NET_TIMER_STARTED(thisHangar.timeFailSafe)
				START_NET_TIMER(thisHangar.timeFailSafe) 
			ENDIF
			
			IF HAS_NET_TIMER_EXPIRED(thisHangar.timeFailSafe, MAX_INTRO_CUTSCENE_FALLBACK_TIME)
				CDEBUG1LN(DEBUG_CUTSCENE, "AM_MP_HANGAR - MAINTAIN_HANGAR_INTRO_CUTSCENE_STATE - Net timer has expired, cleaning cutscene.")
				
				IF NOT IS_SCREEN_FADED_OUT() AND NOT IS_SCREEN_FADING_OUT()
					DO_SCREEN_FADE_OUT(0)
					SET_HANGAR_INTRO_CUTSCENE_VIEWED()
				ENDIF
			
				IF IS_SCREEN_FADED_OUT()
					RESET_NET_TIMER(thisHangar.timeFailSafe)
					
					BOOL bInteriorWasRefreshed
					RESET_ENTITY_SETS_FOR_INTRO_CUTSCENE(bInteriorWasRefreshed)
					
					IF bInteriorWasRefreshed
						// Wait until interior has been refreshed.
						START_NET_TIMER(thisHangar.timeFailSafe)
						CDEBUG1LN(DEBUG_CUTSCENE, "AM_MP_HANGAR - MAINTAIN_HANGAR_INTRO_CUTSCENE_STATE - Reset timer as bInteriorWasRefreshed = ", BOOL_TO_INT(bInteriorWasRefreshed))
					ENDIF

					SET_HANGAR_CUTSCENE_STATE(HANGAR_CUTSCENE_STATE_CLEANUP)
				ENDIF
			ENDIF
			
			/* Disabled for B*3883772
			REQUEST_HANGAR_INTRO_CUTSCENE_EXTERIOR()
			*/
			
			IF IS_THIS_HANGAR_IN_FORT_ZANCUDO(thisHangar.eID)
				NEW_LOAD_SCENE_START_SPHERE(<<-2210.719, 2964.25, 31.8199>>, 300.0)
			ELIF IS_THIS_HANGAR_IN_LSIA(thisHangar.eID)
				NEW_LOAD_SCENE_START_SPHERE(<<-1169.94, -3250.649, 12.930>>, 300.0)
			ENDIF
			
			IF HAS_CUTSCENE_FINISHED()
				
				IF IS_CUTSCENE_PLAYING()
					STOP_CUTSCENE()
					REMOVE_CUTSCENE()
					CDEBUG1LN(DEBUG_CUTSCENE, "AM_MP_HANGAR - MAINTAIN_HANGAR_INTRO_CUTSCENE_STATE - Stopping cutscene.")
				ENDIF
				
				IF IS_SCREEN_FADED_IN() OR IS_SCREEN_FADING_IN()
					// Keep the screen faded out as we transition between scenes.
					DO_SCREEN_FADE_OUT(0)
				ENDIF

				SET_HANGAR_CUTSCENE_STATE(HANGAR_CUTSCENE_STATE_LOAD_EXTERIOR)	
			ELSE
				IF NOT IS_SCREEN_FADED_OUT() AND NOT IS_SCREEN_FADING_OUT()
					sceneEntity = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("MP_1")
					
					IF DOES_ENTITY_EXIST(sceneEntity)
						IF NOT IS_ENTITY_DEAD(sceneEntity)	
							IF HAS_ANIM_EVENT_FIRED(sceneEntity, GET_HASH_KEY("fade")) 
								DO_SCREEN_FADE_OUT(1000)
								CDEBUG1LN(DEBUG_CUTSCENE, "AM_MP_HANGAR - MAINTAIN_HANGAR_INTRO_CUTSCENE_STATE - Fading out.")
							ENDIF
						ELSE
							CDEBUG1LN(DEBUG_CUTSCENE, "AM_MP_HANGAR - MAINTAIN_HANGAR_INTRO_CUTSCENE_STATE - scene entity is dead.")
						ENDIF
					ELSE
						CDEBUG1LN(DEBUG_CUTSCENE, "AM_MP_HANGAR - MAINTAIN_HANGAR_INTRO_CUTSCENE_STATE - scene entity doesn't exist.")
					ENDIF
				#IF IS_DEBUG_BUILD
				ELSE
					IF GET_FRAME_COUNT() % 30 = 0
						CDEBUG1LN(DEBUG_CUTSCENE, "AM_MP_HANGAR - MAINTAIN_HANGAR_INTRO_CUTSCENE_STATE - HAS_CUTSCENE_FINISHED ELSE: ", HAS_CUTSCENE_FINISHED(), " IS_CUTSCENE_PLAYING: ", IS_CUTSCENE_PLAYING(), " Timer Exp: ", HAS_NET_TIMER_EXPIRED(thisHangar.timeFailSafe, MAX_INTRO_CUTSCENE_FALLBACK_TIME))
						CDEBUG1LN(DEBUG_CUTSCENE, "AM_MP_HANGAR - MAINTAIN_HANGAR_INTRO_CUTSCENE_STATE - Screen fade state, IS_SCREEN_FADED_OUT: ", IS_SCREEN_FADED_OUT(), " IS_SCREEN_FADING_OUT: ", IS_SCREEN_FADING_OUT())
					ENDIF
				#ENDIF
				ENDIF
			ENDIF
		BREAK
				
		CASE HANGAR_CUTSCENE_STATE_LOAD_EXTERIOR
			IF (GET_FRAME_COUNT() % 120) = 0
				CDEBUG1LN(DEBUG_CUTSCENE, "AM_MP_HANGAR - MAINTAIN_HANGAR_INTRO_CUTSCENE_STATE - HANGAR_CUTSCENE_STATE_LOAD_EXTERIOR")
			ENDIF
			
			IF !thisHangar.bHasCutsceneLoad
				IF HAS_CUTSCENE_LOADED()
					IF IS_THIS_HANGAR_IN_FORT_ZANCUDO(thisHangar.eID)
						START_CUTSCENE_AT_COORDS(<<-2210.719, 2964.25, 31.8199>>)
					ELIF IS_THIS_HANGAR_IN_LSIA(thisHangar.eID)
						START_CUTSCENE_AT_COORDS(<<-1169.94, -3250.649, 12.930>>)
					ENDIF
					thisHangar.bHasCutsceneLoad = TRUE
				ELSE					
					REQUEST_HANGAR_INTRO_CUTSCENE_EXTERIOR()
					CDEBUG1LN(DEBUG_CUTSCENE, "AM_MP_HANGAR - MAINTAIN_HANGAR_INTRO_CUTSCENE_STATE - Requesting exterior cutscene.")
				ENDIF
			ELSE	
				IF IS_CUTSCENE_PLAYING()					
					SET_HANGAR_CUTSCENE_STATE(HANGAR_CUTSCENE_STATE_PLAY_EXTERIOR)
				ENDIF
			ENDIF
		BREAK
		CASE HANGAR_CUTSCENE_STATE_PLAY_EXTERIOR
			IF (GET_FRAME_COUNT() % 120) = 0	
				CDEBUG1LN(DEBUG_CUTSCENE, "AM_MP_HANGAR - MAINTAIN_HANGAR_INTRO_CUTSCENE_STATE - HANGAR_CUTSCENE_STATE_PLAY_EXTERIOR")
			ENDIF
			
			IF IS_CUTSCENE_PLAYING()
				IF IS_SCREEN_FADED_OUT() OR IS_SCREEN_FADING_OUT()
					DO_SCREEN_FADE_IN(0)
				ENDIF
			ENDIF

			IF HAS_NET_TIMER_EXPIRED(thisHangar.timeFailSafe, MAX_INTRO_CUTSCENE_FALLBACK_TIME)
				thisHangar.bSceneTimeout = TRUE
				CDEBUG1LN(DEBUG_CUTSCENE, "AM_MP_HANGAR - MAINTAIN_HANGAR_INTRO_CUTSCENE_STATE - Scene time out during exterior scene playback.")
			ENDIF

			IF HAS_CUTSCENE_FINISHED() //OR thisHangar.bSceneTimeout
				
				IF IS_NEW_LOAD_SCENE_ACTIVE() AND IS_NEW_LOAD_SCENE_LOADED()
					NEW_LOAD_SCENE_STOP()
					CDEBUG1LN(DEBUG_CUTSCENE, "AM_MP_HANGAR - MAINTAIN_HANGAR_INTRO_CUTSCENE_STATE - Stopping load scene.")
				ENDIF
				
				IF NOT IS_SCREEN_FADED_OUT() AND NOT IS_SCREEN_FADING_OUT()
					DO_SCREEN_FADE_OUT(0)
					SET_HANGAR_INTRO_CUTSCENE_VIEWED()
					CDEBUG1LN(DEBUG_CUTSCENE, "AM_MP_HANGAR - MAINTAIN_HANGAR_INTRO_CUTSCENE_STATE - Exterior scene finished, fading screen out.")
				ENDIF
				
				IF IS_SCREEN_FADED_OUT()
				
					BOOL bInteriorWasRefreshed
					RESET_ENTITY_SETS_FOR_INTRO_CUTSCENE(bInteriorWasRefreshed)
					
					IF bInteriorWasRefreshed
						// Wait until interior has been refreshed.
						START_NET_TIMER(thisHangar.timeFailSafe)
						CDEBUG1LN(DEBUG_CUTSCENE, "AM_MP_HANGAR - MAINTAIN_HANGAR_INTRO_CUTSCENE_STATE - Resetting timer as bInteriorWasRefreshed = ", BOOL_TO_INT(bInteriorWasRefreshed))
					ENDIF
				ENDIF
				
				SET_HANGAR_CUTSCENE_STATE(HANGAR_CUTSCENE_STATE_CLEANUP)
			ENDIF
		BREAK
		CASE HANGAR_CUTSCENE_STATE_CLEANUP
			CDEBUG1LN(DEBUG_CUTSCENE, "AM_MP_HANGAR - MAINTAIN_HANGAR_INTRO_CUTSCENE_STATE - HANGAR_CUTSCENE_STATE_CLEANUP")
			
			IF NOT HAS_NET_TIMER_STARTED(thisHangar.tIdleFallback)
				START_NET_TIMER(thisHangar.tIdleFallback)
			ENDIF
			
			// If we idle in the cleanup for more than 5 seconds then bail and force cleanup.
			IF HAS_NET_TIMER_EXPIRED(thisHangar.tIdleFallback, 5000)
				thisHangar.bIdleTimeout = TRUE
				CDEBUG1LN(DEBUG_CUTSCENE, "AM_MP_HANGAR - MAINTAIN_HANGAR_INTRO_CUTSCENE_STATE - Idle fallback timer has expired, forcing cleanup.")
			ENDIF
			
			IF NOT HAS_NET_TIMER_STARTED(thisHangar.timeFailSafe)
			OR (HAS_NET_TIMER_EXPIRED(thisHangar.timeFailSafe, 1000) AND IS_INTERIOR_READY(thisHangar.HangarInteriorID))
			OR thisHangar.bIdleTimeout
			
				IF IS_SCREEN_FADED_OUT()
					CLEANUP_INTRO_CUTSCENE()
					
					// Update all viewers stats to say they have watched the cutscene.
					SET_MP_SPAWN_POINT_SETTING(MP_SETTING_SPAWN_HANGAR)
					SET_HANGAR_INTRO_CUTSCENE_VIEWED()

					IF NOT HAS_NET_TIMER_STARTED(thisHangar.tRefreshTimer)
					
						// Update visible hangar entity sets.
						SET_DEFAULT_HANGAR_ENTITY_SETS(thisHangar.eID, thisHangar.pOwner)
						REFRESH_INTERIOR(thisHangar.HangarInteriorID)
					
						START_NET_TIMER(thisHangar.tRefreshTimer)
					ENDIF
					
					IF HAS_NET_TIMER_EXPIRED(thisHangar.tRefreshTimer, 2000)// AND IS_INTERIOR_READY(thisHangar.HangarInteriorID)
						SET_HANGAR_STATE(HANGAR_STATE_LOADING)
						CDEBUG1LN(DEBUG_CUTSCENE, "AM_MP_HANGAR - MAINTAIN_HANGAR_INTRO_CUTSCENE_STATE - ******* FINISHED ******")
					ELSE
						CDEBUG1LN(DEBUG_CUTSCENE, "AM_MP_HANGAR - MAINTAIN_HANGAR_INTRO_CUTSCENE_STATE - thisHangar.tRefreshTimer NOT expired.")
					ENDIF
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_CUTSCENE, "AM_MP_HANGAR - MAINTAIN_HANGAR_INTRO_CUTSCENE_STATE - WAITING FOR REFRESH")
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC MAINTAIN_SHOULD_START_INTRO_CUSTCENE()
	IF IS_HANGAR_STATE(HANGAR_STATE_PLAYING_INTRO_CUTSCENE)
		EXIT
	ENDIF
	
	IF IS_TRANSITION_ACTIVE()
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_NoSmIntroCutscene")
		EXIT
	ENDIF
	#ENDIF
	
	BOOL bShouldCheck
	BOOL bDoneIntroCut
	
	IF IS_LOCAL_PLAYER_WALKING_INTO_SIMPLE_INTERIOR()
		IF NOT HAS_SIMPLE_INTERIOR_CHILD_SCRIPT_TIMED_OUT()
		AND IS_HANGAR_STATE(HANGAR_STATE_LOADING)
			bShouldCheck = TRUE
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF debugData.bStartIntroCutscene
		bShouldCheck = TRUE
	ENDIF
	#ENDIF

	IF bShouldCheck
		IF thisHangar.eCutsceneState = HANGAR_CUTSCENE_STATE_INITIALISE
			IF thisHangar.pOwner = PLAYER_ID()
				//-- I'm the owner of the hangar - play if I haven't seen it already.
				bDoneIntroCut = HAS_LOCAL_PLAYER_VIEWED_INTERIOR_CUTSCENE(biFmCut_Hangar_Intro_Cutscene)
				
				IF NOT bDoneIntroCut
				#IF IS_DEBUG_BUILD
				OR debugData.bStartIntroCutscene
				#ENDIF
					INIT_LOCAL_PLAYER_FOR_INTRO_MOCAP()
					thisHangar.bShouldPlayIntroCut = TRUE
				ELSE
					#IF IS_DEBUG_BUILD
					IF GET_FRAME_COUNT() % 60 = 0
						CDEBUG1LN(DEBUG_CUTSCENE, "AM_MP_HANGAR - MAINTAIN_SHOULD_START_INTRO_CUSTCENE Not playing - already done")
					ENDIF
					#ENDIF
				ENDIF
				
			ELIF AM_I_WALKING_INTO_HANGAR_WITH_OWNER()
				
				IF IS_HANGAR_OWNER_WATCHING_INTRO_MOCAP()
					//- I'm not the owner, but I'm entering the bunker with the owner - play if I haven't seen it already 
					bDoneIntroCut = HAS_LOCAL_PLAYER_VIEWED_INTERIOR_CUTSCENE(biFmCut_Hangar_Intro_Cutscene)
					IF NOT bDoneIntroCut
					#IF IS_DEBUG_BUILD
					OR debugData.bStartIntroCutscene
					#ENDIF
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - MAINTAIN_SHOULD_START_INTRO_CUSTCENE SHould play - owner entering")
						INIT_LOCAL_PLAYER_FOR_INTRO_MOCAP()
						thisHangar.bShouldPlayIntroCut = TRUE
					ENDIF
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - MAINTAIN_SHOULD_START_INTRO_CUSTCENE - owner is not watching?")
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - MAINTAIN_SHOULD_START_INTRO_CUSTCENE - not the owner or entering with owner.")
			ENDIF
		ELSE
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - MAINTAIN_SHOULD_START_INTRO_CUSTCENE - Cutscene already running?")
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF debugData.bStartIntroCutscene
	AND thisHangar.bShouldPlayIntroCut = TRUE
		SET_HANGAR_STATE(HANGAR_STATE_PLAYING_INTRO_CUTSCENE)
		debugData.bStartIntroCutscene = FALSE
	ENDIF
	#ENDIF
ENDPROC

FUNC BOOL IS_LOCAL_PLAYER_WITHIN_HANGAR_BOUNDS()
	SIMPLE_INTERIOR_INSIDE_BOUNDING_BOX bounds
	GET_SIMPLE_INTERIOR_INSIDE_BOUNDING_BOX(thisHangar.eSimpleInteriorID, bounds)
		
	BOOL bInBounds = IS_ENTITY_IN_AREA(PLAYER_PED_ID(), bounds.vInsideBBoxMin, bounds.vInsideBBoxMax)
	
	#IF IS_DEBUG_BUILD
		VECTOR vPlayerCoords = GET_PLAYER_COORDS(PLAYER_ID())
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - IS_LOCAL_PLAYER_WITHIN_HANGAR_BOUNDS - Player pos: ", vPlayerCoords)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - IS_LOCAL_PLAYER_WITHIN_HANGAR_BOUNDS - Bounds: ", bounds.vInsideBBoxMin, bounds.vInsideBBoxMax)
	#ENDIF
	
	RETURN bInBounds
ENDFUNC

PROC CLEANUP_ENTRY_VEHICLES()
	IF NOT IS_BIT_SET(thisHangar.iBS, BS_HANGAR_DATA_DID_VEH_WARP)
		IF thisHangar.pOwner = PLAYER_ID()
			IF g_bEnteredInPV
				CLEANUP_MP_SAVED_VEHICLE(DEFAULT, DEFAULT, TRUE, DEFAULT, TRUE)
			ELIF g_bEnteredInPegasus
				SET_BIT(thisHangar.iBS, BS_HANGAR_DATA_ENTERED_IN_PEGASUS)
				SET_CLEARNUP_PEGASUS_BEFORE_SUPERMOD(TRUE)
			ENDIF
			
			INT iDisplaySlot
			
			IF g_iVehicleEntered > -1
				MPSV_GET_DISPLAY_SLOT_FROM_SAVE_SLOT(g_iVehicleEntered, iDisplaySlot)
				
				IF IS_HANGAR_DISPLAY_SLOT_IN_STORAGE(iDisplaySlot)
					SET_BIT(thisHangar.iBS, BS_HANGAR_DATA_ENTERED_IN_STORAGE)
				ENDIF
			ENDIF
			
			PRINTLN("g_bEnteredInPegasus = FALSE")
			g_bEnteredInPV = FALSE
			g_bEnteredInPegasus = FALSE
		ENDIF
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR: CLEANUP_ENTRY_VEHICLES")
		SET_BIT(thisHangar.iBS, BS_HANGAR_DATA_DID_VEH_WARP)
	ENDIF
ENDPROC

FUNC BOOL ARE_PLAYERS_IN_ENTRANCE_VEHICLE()
	IF NOT CAN_SIMPLE_INTERIOR_CHILD_SCRIPT_TAKE_CONTROL()
		RETURN FALSE
	ENDIF
	
	CLEANUP_ENTRY_VEHICLES()
	
	IF IS_BIT_SET(thisHangar.iBS, BS_HANGAR_DATA_DID_VEH_WARP)
	AND serverBD.iEnteringVeh != -1
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.customVehicleNetIDs[serverBD.iEnteringVeh])
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			AND IS_VEHICLE_DRIVEABLE(NET_TO_VEH(serverBD.customVehicleNetIDs[serverBD.iEnteringVeh]))
			AND NOT NETWORK_IS_ENTITY_FADING(NET_TO_VEH(serverBD.customVehicleNetIDs[serverBD.iEnteringVeh]))
			AND (IS_VEHICLE_ON_ALL_WHEELS(NET_TO_VEH(serverBD.customVehicleNetIDs[serverBD.iEnteringVeh])) OR GET_ENTITY_MODEL(NET_TO_VEH(serverBD.customVehicleNetIDs[serverBD.iEnteringVeh])) = SEASPARROW)
				IF IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.customVehicleNetIDs[serverBD.iEnteringVeh]))
					NETWORK_FADE_IN_ENTITY(PLAYER_PED_ID(), TRUE, FALSE)
					
					RETURN TRUE
				ELSE
					IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_ENTER_VEHICLE) != PERFORMING_TASK
					AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_ENTER_VEHICLE) != WAITING_TO_START_TASK
						TASK_ENTER_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.customVehicleNetIDs[serverBD.iEnteringVeh]), DEFAULT, SIMPLE_INTERIOR_GET_ENTRY_VEH_SEAT(), DEFAULT, ECF_WARP_PED)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_HANGAR_LOAD()
	INTERIOR_INSTANCE_INDEX interiorIndex
	VECTOR vInteriorPosition
	FLOAT fInteriorHeading
	TEXT_LABEL_63 strInteriorType
	
	IF NOT IS_BIT_SET(thisHangar.iBS2, BS_HANGAR_DATA_INTERIOR_REFRESHED_ON_INIT)
		IF NOT thisHangar.bScriptRelaunched
			GET_SIMPLE_INTERIOR_INTERIOR_TYPE_AND_POSITION(thisHangar.eSimpleInteriorID, strInteriorType, vInteriorPosition, fInteriorHeading)
			interiorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(vInteriorPosition, strInteriorType)
			
			IF IS_VALID_INTERIOR(interiorIndex) AND IS_INTERIOR_READY(interiorIndex)
				BOOL bReady = TRUE

//				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//					IF (IS_LOCAL_PLAYER_IN_PERSONAL_VEHICLE() 
//					OR IS_VEHICLE_UPGRADABLE_PEGASUS_VEHICLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())))
//					AND thisHangar.pOwner = PLAYER_ID()
//						bReady = FALSE
//						PRINTLN("[SIMPLE_INTERIOR] AM_MP_HANGAR - MAINTAIN_HANGAR_LOAD - Player ped is in a vehicle, waiting until they exit before refreshing interior.")
//					ENDIF
//				ENDIF

				IF bReady
					IF IS_PLAYER_IN_HANGAR(PLAYER_ID())
						FORCE_ROOM_FOR_ENTITY(PLAYER_PED_ID(), interiorIndex, HANGAR_ROOM_KEY)
						PRINTLN("[SIMPLE_INTERIOR] AM_MP_HANGAR - MAINTAIN_HANGAR_LOAD - Forcing room for player, we are inside hangar.")
					ELSE
						PRINTLN("[SIMPLE_INTERIOR] AM_MP_HANGAR - MAINTAIN_HANGAR_LOAD - Preventing room force, not inside hangar yet.")
					ENDIF
					
					IF IS_BIT_SET(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_SPAWNING_IN_SIMPLE_INTERIOR)
					AND NOT IS_SKYSWOOP_AT_GROUND()
					
						REFRESH_INTERIOR(interiorIndex)
						PRINTLN("[SIMPLE_INTERIOR] AM_MP_HANGAR - MAINTAIN_HANGAR_LOAD - Refreshing interior.")
					
					ENDIF
					
					REPIN_AND_REFRESH_SIMPLE_INTERIOR()
					
					SET_SIMPLE_INTERIOR_CHILD_SCRIPT_ALLOWS_FOR_WARP(TRUE)
					SET_BIT(thisHangar.iBS2, BS_HANGAR_DATA_INTERIOR_REFRESHED_ON_INIT)
					CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] AM_MP_HANGAR - MAINTAIN_HANGAR_LOAD - Setting BS_HANGAR_DATA_INTERIOR_REFRESHED_ON_INIT")
					
					RESET_NET_TIMER(thisHangar.stPinInMemTimer)
				ENDIF
			ELSE
				IF NOT HAS_NET_TIMER_STARTED(thisHangar.stPinInMemTimer)
					PRINTLN("[SIMPLE_INTERIOR] AM_MP_HANGAR - MAINTAIN_HANGAR_LOAD - Pinning interior with ID: ", NATIVE_TO_INT(interiorIndex))
					PIN_INTERIOR_IN_MEMORY_VALIDATE(interiorIndex)
					START_NET_TIMER(thisHangar.stPinInMemTimer)
				ENDIF
				
				IF HAS_NET_TIMER_EXPIRED(thisHangar.stPinInMemTimer, 12500)
					RESET_NET_TIMER(thisHangar.stPinInMemTimer)
				ENDIF
				
				PRINTLN("[SIMPLE_INTERIOR] AM_MP_HANGAR - MAINTAIN_HANGAR_LOAD - Waiting for a valid and ready interior with ID: ", 
							NATIVE_TO_INT(interiorIndex), " valid? ", IS_VALID_INTERIOR(interiorIndex), " ready? ", IS_INTERIOR_READY(interiorIndex), " disabled? ", IS_INTERIOR_DISABLED(interiorIndex))
			ENDIF
		ELSE
			SET_SIMPLE_INTERIOR_CHILD_SCRIPT_ALLOWS_FOR_WARP(TRUE)
			SET_BIT(thisHangar.iBS2, BS_HANGAR_DATA_INTERIOR_REFRESHED_ON_INIT)
			CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] AM_MP_HANGAR - MAINTAIN_HANGAR_LOAD - Setting BS_HANGAR_DATA_INTERIOR_REFRESHED_ON_INIT")
		ENDIF
	ENDIF
	
	IF CAN_SIMPLE_INTERIOR_CHILD_SCRIPT_TAKE_CONTROL()
	AND IS_BIT_SET(thisHangar.iBS2, BS_HANGAR_DATA_INTERIOR_REFRESHED_ON_INIT)
		BOOL bReady = TRUE
		
		IF NOT IS_BIT_SET(thisHangar.iBS, BS_HANGAR_DATA_LOAD_SCENE_STARTED)
			VECTOR vHangarCoords
			FLOAT fHangarHeading
			
			SIMPLE_INTERIOR_INSIDE_BOUNDING_BOX bounds
			GET_SIMPLE_INTERIOR_INSIDE_BOUNDING_BOX(thisHangar.eSimpleInteriorID, bounds)
			GET_SIMPLE_INTERIOR_INTERIOR_TYPE_AND_POSITION(thisHangar.eSimpleInteriorID, strInteriorType, vHangarCoords, fHangarHeading)			
			vHangarCoords.z = vHangarCoords.z + 1
			
			SET_BIT(thisHangar.iBS, BS_HANGAR_DATA_LOAD_SCENE_STARTED)
			PRINTLN("[SIMPLE_INTERIOR] AM_MP_HANGAR - MAINTAIN_HANGAR_LOAD - BS_HANGAR_DATA_LOAD_SCENE_STARTED")
			
			bReady = FALSE
		ELSE
			IF IS_NEW_LOAD_SCENE_ACTIVE()
				IF NOT IS_NEW_LOAD_SCENE_LOADED()
					PRINTLN("[SIMPLE_INTERIOR] AM_MP_HANGAR - MAINTAIN_HANGAR_LOAD - BS_HANGAR_DATA_LOAD_SCENE_STARTED - IS_NEW_LOAD_SCENE_LOADED = FALSE")
					bReady = FALSE
				ELSE
					GET_SIMPLE_INTERIOR_INTERIOR_TYPE_AND_POSITION(thisHangar.eSimpleInteriorID, strInteriorType, vInteriorPosition, fInteriorHeading)
					interiorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(vInteriorPosition, strInteriorType)
					
					IF IS_VALID_INTERIOR(interiorIndex)
					AND IS_INTERIOR_READY(interiorIndex)
					AND IS_LOCAL_PLAYER_WITHIN_HANGAR_BOUNDS()
						PRINTLN("[SIMPLE_INTERIOR] AM_MP_HANGAR - MAINTAIN_HANGAR_LOAD - Inside hangar bounds, load scene is ready.")
						NEW_LOAD_SCENE_STOP()
					ELSE
						PRINTLN("[SIMPLE_INTERIOR] AM_MP_HANGAR - MAINTAIN_HANGAR_LOAD - Setting BS_HANGAR_KILL_LOAD_SCENE_WHEN_INSIDE")
						SET_BIT(thisHangar.iBS, BS_HANGAR_KILL_LOAD_SCENE_WHEN_INSIDE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF bReady
			IF thisHangar.bShouldPlayIntroCut
				//-- Need to play intro mocap. Moved here from RUN_MAIN_CLIENT_LOGIC() so the above load scene can finish
				IF CAN_SIMPLE_INTERIOR_CHILD_SCRIPT_TAKE_CONTROL()
					SET_SIMPLE_INTERIOR_CHILD_SCRIPT_IS_IN_CONTROL(TRUE)
					SET_HANGAR_STATE(HANGAR_STATE_PLAYING_INTRO_CUTSCENE)
				ELSE
					PRINTLN("[SIMPLE_INTERIOR] AM_MP_HANGAR - MAINTAIN_HANGAR_LOAD - Should play intro cut - waiting for CAN_SIMPLE_INTERIOR_CHILD_SCRIPT_TAKE_CONTROL")
				ENDIF
			ELIF MAINTAIN_LAUNCHING_HANGER_CARMOD_SCRIPT()
			AND CREATE_VEHICLE_MANAGEMENT_PROP()
				IF SIMPLE_INTERIOR_IS_LOCAL_PLAYER_ENTERING_WITH_VEHICLE()
					IF thisHangar.pOwner != PLAYER_ID() 
					AND IS_HANGAR_OWNER_WATCHING_INTRO_MOCAP()
					AND HAS_LOCAL_PLAYER_VIEWED_INTERIOR_CUTSCENE(biFmCut_Hangar_Intro_Cutscene)
						SIMPLE_INTERIOR_CLEAR_ENTERING_WITH_VEHICLE()
					ELSE
					
						SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PreventAutoShuffleToDriversSeat, TRUE)
						SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PreventAutoShuffleToTurretSeat, TRUE)
						
						IF ARE_PLAYERS_IN_ENTRANCE_VEHICLE()
							SET_SIMPLE_INTERIOR_CHILD_SCRIPT_READY(TRUE) // This is important, let internal script know that its child script is ready
							SET_SIMPLE_INTERIOR_CHILD_SCRIPT_ALLOWS_FOR_WARP(TRUE)
							SET_HANGAR_STATE(HANGAR_STATE_IDLE)
							
							START_NET_TIMER(sExitDelayTimer)
							
							PRINTLN("[SIMPLE_INTERIOR] AM_MP_HANGAR - MAINTAIN_HANGAR_LOAD - Everyone in vehicle warped, bReady set to TRUE")
						ENDIF
					ENDIF
				ELSE
					CLEANUP_ENTRY_VEHICLES()
					SET_SIMPLE_INTERIOR_CHILD_SCRIPT_READY(TRUE) // This is important, let internal script know that its child script is ready
					SET_SIMPLE_INTERIOR_CHILD_SCRIPT_ALLOWS_FOR_WARP(TRUE)
					SET_HANGAR_STATE(HANGAR_STATE_IDLE)
					
					PRINTLN("[SIMPLE_INTERIOR] AM_MP_HANGAR - MAINTAIN_HANGAR_LOAD - Not in vehicle, bReady set to TRUE")
				ENDIF
			ELSE
				PRINTLN("[SIMPLE_INTERIOR] AM_MP_HANGAR - MAINTAIN_HANGAR_LOAD - Waiting for MAINTAIN_LAUNCHING_HANGER_CARMOD_SCRIPT")
			ENDIF
		ENDIF
	ELSE
		BOOL bResult = IS_BIT_SET(thisHangar.iBS2, BS_HANGAR_DATA_INTERIOR_REFRESHED_ON_INIT)
		PRINTLN("[SIMPLE_INTERIOR] AM_MP_HANGAR - MAINTAIN_HANGAR_LOAD - CAN_SIMPLE_INTERIOR_CHILD_SCRIPT_TAKE_CONTROL = FALSE & BS_HANGAR_DATA_INTERIOR_REFRESHED_ON_INIT = ", bResult)
	ENDIF
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════╡ HANGAR VEHICLES ╞══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC SERVER_CLEANUP_PLAYERS_ARE_IN_THE_WAY_HANGAR_FLAGS()
	IF IS_BIT_SET(serverBD.sGetOutOfTheWay.iCreationBitSet, HANGAR_WLK_OUT_SERVER_RUN_PLAYER_CLEAN_UP_CHECKS)
		BOOL bActive
		PLAYER_INDEX playerID
		
		bActive = FALSE
		playerID = INT_TO_PLAYERINDEX(iServerGetOutOfWayPartCheck)
		
		IF IS_NET_PLAYER_OK(playerID, FALSE)
			IF NETWORK_IS_PLAYER_A_PARTICIPANT(playerID)
				bActive = TRUE
				
				IF IS_BIT_SET(serverBD.sGetOutOfTheWay.iPlayerBitSet, iServerGetOutOfWayPartCheck)
					IF IS_BIT_SET(playerBD[NATIVE_TO_INT(playerID)].sGetOutOfTheWay.iPlayerBitSet, HANGAR_WLK_OUT_PLAYER_IM_OUT_OF_THE_WAY)
						CLEAR_BIT(serverBD.sGetOutOfTheWay.iPlayerBitSet, iServerGetOutOfWayPartCheck)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF bActive = FALSE
			IF IS_BIT_SET(serverBD.sGetOutOfTheWay.iPlayerBitSet, iServerGetOutOfWayPartCheck)
				CLEAR_BIT(serverBD.sGetOutOfTheWay.iPlayerBitSet, iServerGetOutOfWayPartCheck)
			ENDIF
		ENDIF
		
		iServerGetOutOfWayPartCheck++
		
		IF iServerGetOutOfWayPartCheck >= NUM_NETWORK_PLAYERS
			iServerGetOutOfWayPartCheck = 0
			
			IF serverBD.sGetOutOfTheWay.iPlayerBitSet = 0
				CLEAR_BIT(serverBD.sGetOutOfTheWay.iCreationBitSet, HANGAR_WLK_OUT_SERVER_RUN_PLAYER_CLEAN_UP_CHECKS)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL MAINTAIN_GET_OUT_OF_THE_WAY_HANGAR()
	INT iMyPart = PARTICIPANT_ID_TO_INT()
	
	IF iMyPart = -1
		RETURN FALSE
	ENDIF
	
	INT iMyGBD = NATIVE_TO_INT(PLAYER_ID())
	
	IF (IS_BIT_SET(serverBD.sGetOutOfTheWay.iPlayerBitSet, iMyGBD)
	OR IS_BIT_SET(sGetOutOfWayVars.iProgressBitSet, HANGAR_WLK_OUT_LOCAL_SET_UP_WALK_OUT))
		IF HAS_NET_TIMER_STARTED(getOutOfWayTimer)
		AND NOT HAS_NET_TIMER_EXPIRED(getOutOfWayTimer, 5000)
			SET_BIT(playerBD[iMyGBD].sGetOutOfTheWay.iPlayerBitSet, HANGAR_WLK_OUT_PLAYER_IM_OUT_OF_THE_WAY)
			CLEAR_BIT(sGetOutOfWayVars.iProgressBitSet, HANGAR_WLK_OUT_LOCAL_SET_UP_WALK_OUT)
			RESET_NET_TIMER(sGetOutOfWayVars.controlTimer)
		ELSE
			IF NOT IS_BIT_SET(playerBD[iMyGBD].sGetOutOfTheWay.iPlayerBitSet, HANGAR_WLK_OUT_PLAYER_IM_OUT_OF_THE_WAY)
				IF NOT IS_BIT_SET(sGetOutOfWayVars.iProgressBitSet, HANGAR_WLK_OUT_LOCAL_SET_UP_WALK_OUT)
					IF IS_NET_PLAYER_OK(PLAYER_ID())
						thisHangar.bApplyUpgrade = TRUE
						thisHangar.bWarpDuringUpgrade = TRUE
						
						PRINTLN("[SIMPLE_INTERIOR] AM_MP_HANGAR - MAINTAIN_GET_OUT_OF_THE_WAY_HANGAR - starting upgrade")
						
						REINIT_NET_TIMER(sGetOutOfWayVars.controlTimer, TRUE)
						SET_BIT(sGetOutOfWayVars.iProgressBitSet, HANGAR_WLK_OUT_LOCAL_SET_UP_WALK_OUT)		
					ENDIF
				ELSE
					IF HAS_NET_TIMER_EXPIRED(sGetOutOfWayVars.controlTimer, HANGAR_WALK_OUT_SAFETY_TIME, TRUE)
					OR thisHangar.eUpgradeState > HANGAR_UPGRADE_STATE_FADE_OUT
						SET_BIT(playerBD[iMyGBD].sGetOutOfTheWay.iPlayerBitSet, HANGAR_WLK_OUT_PLAYER_IM_OUT_OF_THE_WAY)
						CLEAR_BIT(sGetOutOfWayVars.iProgressBitSet, HANGAR_WLK_OUT_LOCAL_SET_UP_WALK_OUT)
						RESET_NET_TIMER(sGetOutOfWayVars.controlTimer)
					ENDIF
					
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ELSE
		CLEAR_BIT(playerBD[iMyGBD].sGetOutOfTheWay.iPlayerBitSet, HANGAR_WLK_OUT_PLAYER_IM_OUT_OF_THE_WAY)
		
		IF playerBD[iMyGBD].sGetOutOfTheWay.iPlayerBitSet != 0
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_GET_OUT_OF_OWNERS_HANGAR_VEHICLE()
	VEHICLE_INDEX vehIndex
	PED_INDEX DriverPedID
	
	IF thisHangar.pOwner != PLAYER_ID()
		IF IS_NET_PLAYER_OK(PLAYER_ID())
		AND thisHangar.eState = HANGAR_STATE_IDLE
		AND IS_SCREEN_FADED_IN()
		AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(thisHangar.pOwner)
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				vehIndex = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				
				IF IS_THIS_MODEL_ALLOWED_IN_HANGAR(GET_ENTITY_MODEL(vehIndex))
					IF thisHangar.pOwner != INVALID_PLAYER_INDEX()
						IF IS_NET_PLAYER_OK(thisHangar.pOwner, FALSE, TRUE)
							IF NOT IS_VEHICLE_SEAT_FREE(vehIndex)
								DriverPedID = GET_PED_IN_VEHICLE_SEAT(vehIndex, VS_DRIVER)	
								
								IF DOES_ENTITY_EXIST(DriverPedID)
								AND IS_PED_A_PLAYER(DriverPedID)
									IF NETWORK_GET_PLAYER_INDEX_FROM_PED(DriverPedID) != thisHangar.pOwner
										IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != PERFORMING_TASK
										AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != WAITING_TO_START_TASK
											TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != PERFORMING_TASK
								AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != WAITING_TO_START_TASK
									TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
								ENDIF
							ENDIF
						ELSE
							IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != PERFORMING_TASK
							AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != WAITING_TO_START_TASK
								TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF thisHangar.pOwner != INVALID_PLAYER_INDEX()
			IF IS_NET_PLAYER_OK(thisHangar.pOwner)
				IF NOT IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(thisHangar.pOwner), FALSE)
				OR NOT IS_VEHICLE_SEAT_FREE(GET_VEHICLE_PED_IS_IN(GET_PLAYER_PED(thisHangar.pOwner)), VS_ANY_PASSENGER)
				OR GET_SEAT_PED_IS_IN(GET_PLAYER_PED(thisHangar.pOwner)) != VS_DRIVER
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL ARE_ALL_PEDS_IN_HANGAR_VEHICLE_READY(VEHICLE_INDEX theVeh #IF IS_DEBUG_BUILD , BOOL bOutputDebug = FALSE #ENDIF)
	INT i
	PLAYER_INDEX thePlayer
	REPEAT NUM_NETWORK_PLAYERS i
		thePlayer = INT_TO_PLAYERINDEX(i)
		
		IF thePlayer != PLAYER_ID()
		AND thePlayer != INVALID_PLAYER_INDEX()
		AND NETWORK_IS_PLAYER_A_PARTICIPANT(thePlayer)
			IF IS_NET_PLAYER_OK(thePlayer)
				IF IS_PED_IN_VEHICLE(GET_PLAYER_PED(thePlayer), theVeh, FALSE)
					IF NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(thePlayer)].iBS2, BS2_HANGAR_DATA_READY_TO_WARP_OUT)
					OR GET_SCRIPT_TASK_STATUS(GET_PLAYER_PED(thePlayer), SCRIPT_TASK_LEAVE_VEHICLE) = PERFORMING_TASK
					OR GET_SCRIPT_TASK_STATUS(GET_PLAYER_PED(thePlayer), SCRIPT_TASK_LEAVE_ANY_VEHICLE) = PERFORMING_TASK
					OR IS_PLAYER_WALKING_INTO_SIMPLE_INTERIOR(thePlayer)
						#IF IS_DEBUG_BUILD
						IF bOutputDebug
							PRINTLN("ARE_ALL_PEDS_IN_HANGAR_VEHICLE_READY: ", GET_PLAYER_NAME(thePlayer)," not ready not set flag")
						ENDIF
						#ENDIF
						
						RETURN FALSE
					ENDIF
				ELSE
					IF IS_PED_IN_VEHICLE(GET_PLAYER_PED(thePlayer), theVeh, TRUE)
					OR IS_ENTITY_ATTACHED_TO_ENTITY(GET_PLAYER_PED(thePlayer), theVeh)
						#IF IS_DEBUG_BUILD
						IF bOutputDebug
							PRINTLN("ARE_ALL_PEDS_IN_HANGAR_VEHICLE_READY: ", GET_PLAYER_NAME(thePlayer)," entering or attached")
						ENDIF
						#ENDIF
						
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

FUNC BOOL CAN_CURRENT_VEHICLE_DRIVE_OUT()
	IF g_iCurrentPVSlotInProperty >= 0
	AND g_iCurrentPViSaveSlot >= 0
		IF NOT IS_BIT_SET(g_MpSavedVehicles[g_iCurrentPVSlotInProperty].iVehicleBS, MP_SAVED_VEHICLE_OUT_GARAGE)
		AND NOT IS_BIT_SET(g_MpSavedVehicles[g_iCurrentPVSlotInProperty].iVehicleBS, MP_SAVED_VEHICLE_DESTROYED)
		AND NOT IS_BIT_SET(g_MpSavedVehicles[g_iCurrentPVSlotInProperty].iVehicleBS, MP_SAVED_VEHICLE_IMPOUNDED)
		AND NOT IS_BIT_SET(g_MpSavedVehicles[g_iCurrentPVSlotInProperty].iVehicleBS, MP_SAVED_VEHICLE_JUST_PURCHASED)
		AND NOT IS_BIT_SET(serverBD.iVehicleFadeBS, g_iCurrentPViSaveSlot)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_PLAYER_DRIVE_OUT_OF_HANGAR()
	IF NOT CAN_CURRENT_VEHICLE_DRIVE_OUT()
		PRINTLN("CAN_PLAYER_DRIVE_OUT_OF_HANGAR - False NOT CAN_CURRENT_VEHICLE_DRIVE_OUT")
		RETURN FALSE
	ENDIF
	IF NOT ARE_ALL_PEDS_IN_HANGAR_VEHICLE_READY(currentVeh #IF IS_DEBUG_BUILD , TRUE #ENDIF)
		PRINTLN("CAN_PLAYER_DRIVE_OUT_OF_HANGAR - False NOT ARE_ALL_PEDS_IN_HANGAR_VEHICLE_READY")
		RETURN FALSE
	ENDIF
	IF IS_PLAYER_STARTED_TO_USE_PERSONAL_CAR_MOD(PLAYER_ID())
		PRINTLN("CAN_PLAYER_DRIVE_OUT_OF_HANGAR - False IS_PLAYER_STARTED_TO_USE_PERSONAL_CAR_MOD true")
		RETURN FALSE
	ENDIF
	IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) = PERFORMING_TASK
	OR GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) = WAITING_TO_START_TASK
		PRINTLN("CAN_PLAYER_DRIVE_OUT_OF_HANGAR - False leaving a vehicle")
		RETURN FALSE
	ENDIF
	IF g_bDisableVehicleWarehouseVehExit
		PRINTLN("CAN_PLAYER_DRIVE_OUT_OF_HANGAR - g_bDisableVehicleWarehouseVehExit is TRUE")
		RETURN FALSE
	ENDIF
	IF IS_PLAYER_SWITCH_IN_PROGRESS()
		PRINTLN("CAN_PLAYER_DRIVE_OUT_OF_HANGAR- False IS_PLAYER_SWITCH_IN_PROGRESS")
		RETURN FALSE
	ENDIF
	IF IS_SELECTOR_ONSCREEN()
		PRINTLN("CAN_PLAYER_DRIVE_OUT_OF_HANGAR- False IS_SELECTOR_ONSCREEN")
		RETURN FALSE
	ENDIF
	IF PROPERTY_HAS_JUST_ACCEPTED_A_MISSION() // Neilf - added to fix 1764551
		PRINTLN("CAN_PLAYER_DRIVE_OUT_OF_HANGAR- False accepted mission")
		RETURN FALSE
	ENDIF											
	IF DOES_SCRIPT_EXIST("appMPJobListNEW")
	AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appMPJobListNEW")) > 0
		PRINTLN("CAN_PLAYER_DRIVE_OUT_OF_HANGAR- False appMPJobListNew")
		RETURN FALSE
	ENDIF
	IF DOES_SCRIPT_EXIST("appJIPMP")
	AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appJIPMP")) > 0
		PRINTLN("CAN_PLAYER_DRIVE_OUT_OF_HANGAR- False appJIPMP")
		RETURN FALSE
	ENDIF
	IF IS_PLAYER_SCTV(PLAYER_ID())
		PRINTLN("CAN_PLAYER_DRIVE_OUT_OF_HANGAR- False SCTV")
		RETURN FALSE
	ENDIF
	IF IS_PAUSE_MENU_ACTIVE_EX()
		PRINTLN("CAN_PLAYER_DRIVE_OUT_OF_HANGAR- False pause menu active")
		RETURN FALSE
	ENDIF
	IF IS_COMMERCE_STORE_OPEN()
		PRINTLN("CAN_PLAYER_DRIVE_OUT_OF_HANGAR- False commerce store open")
		RETURN FALSE
	ENDIF
	IF IS_STORE_PENDING_NETWORK_SHUTDOWN_TO_OPEN()
		PRINTLN("CAN_PLAYER_DRIVE_OUT_OF_HANGAR- False IS_STORE_PENDING_NETWORK_SHUTDOWN_TO_OPEN")
		RETURN FALSE
	ENDIF
	IF IS_CELLPHONE_CAMERA_IN_USE()
		PRINTLN("CAN_PLAYER_DRIVE_OUT_OF_HANGAR- False IS_CELLPHONE_CAMERA_IN_USE")
		RETURN FALSE
	ENDIF
	IF IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP() 
	AND NOT IS_PLAYER_LEAVING_HANGAR_FROM_MOD_SHOP(PLAYER_ID())
		PRINTLN("CAN_PLAYER_DRIVE_OUT_OF_HANGAR- False CAN_PLAYER_USE_PROPERTY")
		RETURN FALSE
	ENDIF
	IF GET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_IsSeatShuffling)
		PRINTLN("CAN_PLAYER_DRIVE_OUT_OF_HANGAR False player shuffling")
		RETURN FALSE
	ENDIF
	IF IS_BROWSER_OPEN()
		PRINTLN("CAN_PLAYER_DRIVE_OUT_OF_HANGAR - IS_BROWSER_OPEN is TRUE")
		RETURN FALSE
	ENDIF
	IF IS_PLAYER_WALKING_INTO_SIMPLE_INTERIOR(PLAYER_ID())
		PRINTLN("CAN_PLAYER_DRIVE_OUT_OF_HANGAR - IS_PLAYER_WALKING_INTO_SIMPLE_INTERIOR is TRUE")
		RETURN FALSE
	ENDIF
	IF IS_SCREEN_FADED_OUT()
		PRINTLN("CAN_PLAYER_DRIVE_OUT_OF_HANGAR - IS_SCREEN_FADED_OUT is TRUE")
		RETURN FALSE
	ENDIF
	IF IS_SCREEN_FADING_IN()
		PRINTLN("CAN_PLAYER_DRIVE_OUT_OF_HANGAR - IS_SCREEN_FADING_IN is TRUE")
		RETURN FALSE
	ENDIF
	IF HAS_NET_TIMER_STARTED(sExitDelayTimer)
		PRINTLN("CAN_PLAYER_DRIVE_OUT_OF_HANGAR - sExitDelayTimer is running")
		RETURN FALSE
	ENDIF
	IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
		PRINTLN("CAN_PLAYER_DRIVE_OUT_OF_HANGAR - IS_PLAYER_CONTROL_ON FALSE")
		RETURN FALSE
	ENDIF
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
		PRINTLN("CAN_PLAYER_DRIVE_OUT_OF_HANGAR - IS_CONTROL_JUST_PRESSED INPUT_CONTEXT TRUE")
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

PROC MAINTAIN_CAN_DRIVE_OUT_OF_HANGAR()
	IF IS_NET_PLAYER_OK(PLAYER_ID())
	AND IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
	AND NOT IS_PLAYER_WALKING_INTO_SIMPLE_INTERIOR(PLAYER_ID())
	AND NOT IS_SCREEN_FADED_OUT()
	AND NOT IS_SCREEN_FADING_IN()
		DISABLE_SELECTOR_THIS_FRAME()
		
		IF NOT g_MultiplayerSettings.g_bSuicide
			SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS2, BS2_HANGAR_DATA_READY_TO_WARP_OUT)
		ELSE
			CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS2, BS2_HANGAR_DATA_READY_TO_WARP_OUT)
		ENDIF
	ELSE
		CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS2, BS2_HANGAR_DATA_READY_TO_WARP_OUT)
	ENDIF
ENDPROC

PROC MAINTAIN_HANGAR_VEHICLE_LOCKS()
	g_bPlayerLeavingCurrentInteriorInVeh = FALSE
	
	IF HAS_NET_TIMER_STARTED(sExitDelayTimer)
	AND HAS_NET_TIMER_EXPIRED(sExitDelayTimer, 3000)
		RESET_NET_TIMER(sExitDelayTimer)
	ENDIF
	
	IF thisHangar.pOwner = PLAYER_ID()
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), FALSE)
			currentVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(), TRUE)
			
			IF currentVeh != lastVeh
				IF NETWORK_HAS_CONTROL_OF_ENTITY(currentVeh)
					IF NOT IS_PLAYER_STARTED_TO_LEAVE_PERSONAL_CAR_MOD_MODING_BAY(PLAYER_ID())
						lastVeh = currentVeh
						SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(currentVeh, FALSE)
						SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(currentVeh, FALSE)
					ENDIF	
				ELSE
					NETWORK_REQUEST_CONTROL_OF_ENTITY(currentVeh)
				ENDIF
			ENDIF
			
			IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
				//IF GET_IS_VEHICLE_ENGINE_RUNNING(currentVeh)
				IF PLAYER_PED_ID() = GET_PED_IN_VEHICLE_SEAT(currentVeh)
					IF NOT g_bPlayerLeavingCurrentInteriorInVeh
					AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
					AND IS_PLAYER_IN_ANY_SIMPLE_INTERIOR(PLAYER_ID())
						IF NETWORK_HAS_CONTROL_OF_ENTITY(currentVeh)
							IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(currentVeh))
								SET_HELI_BLADES_SPEED(currentVeh, 0)
								SET_VEHICLE_ENGINE_ON(currentVeh, FALSE, TRUE)
							ENDIF
						ENDIF
					ENDIF
					IF CAN_PLAYER_DRIVE_OUT_OF_HANGAR()
						IF ((GET_DISABLED_CONTROL_UNBOUND_NORMAL(PLAYER_CONTROL, INPUT_VEH_ACCELERATE) != 0
						OR GET_DISABLED_CONTROL_UNBOUND_NORMAL(PLAYER_CONTROL, INPUT_VEH_BRAKE) != 0
						OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
						OR IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
						OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
						OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_BRAKE)
						OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_BRAKE)
						OR IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_BRAKE)
						OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_BRAKE))
						AND NOT g_MultiplayerSettings.g_bSuicide	
						AND NOT IS_SELECTOR_ONSCREEN())
						OR IS_PLAYER_LEAVING_HANGAR_FROM_MOD_SHOP(PLAYER_ID())
							g_bPlayerLeavingCurrentInteriorInVeh = TRUE
							
							thisHangar.bDrivingOut = TRUE
							
							IF NETWORK_HAS_CONTROL_OF_ENTITY(currentVeh)
								SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(currentVeh, TRUE)
								SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(currentVeh, TRUE)
								DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
							ELSE
								NETWORK_REQUEST_CONTROL_OF_ENTITY(currentVeh)
							ENDIF
						ENDIF
					ELSE
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BRAKE)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF IS_VEHICLE_DRIVEABLE(lastVeh)
				IF NETWORK_HAS_CONTROL_OF_ENTITY(lastVeh)
					SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(lastVeh, TRUE)
		        	SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(lastVeh, PLAYER_ID(), FALSE)
					lastVeh = NULL
				ELSE
					NETWORK_REQUEST_CONTROL_OF_ENTITY(lastVeh)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_DELETE_FADING_VEHICLES()
	INT i
	REPEAT MAX_DISPLAYED_VEHS_IN_HANGAR i
		IF IS_BIT_SET(serverBD.iVehicleFadeBS, i)
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.customVehicleNetIDs[i])
			AND DOES_ENTITY_EXIST(NET_TO_ENT(serverBD.customVehicleNetIDs[i])) 
			AND IS_ENTITY_ALIVE(NET_TO_ENT(serverBD.customVehicleNetIDs[i]))
				IF NOT NETWORK_IS_ENTITY_FADING(NET_TO_ENT(serverBD.customVehicleNetIDs[i]))
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.customVehicleNetIDs[i])
						DELETE_NET_ID(serverBD.customVehicleNetIDs[i])
						serverBD.iReserved--
						IF GET_NUM_RESERVED_MISSION_VEHICLES(FALSE,RESERVATION_LOCAL_ONLY) >= 1
						AND IS_BIT_SET(serverBD.iVehicleResBS,i)
							RESERVE_LOCAL_NETWORK_MISSION_VEHICLES(GET_NUM_RESERVED_MISSION_VEHICLES(FALSE,RESERVATION_LOCAL_ONLY) - 1)
						ENDIF
						CLEAR_BIT(serverBD.iVehicleResBS, i)
						CLEAR_BIT(serverBD.iVehicleFadeBS, i)
					ELSE
						NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.customVehicleNetIDs[i])
					ENDIF
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - MAINTAIN_DELETE_FADING_VEHICLES - Hangar vehicle ", i, " does not exist and is not alive, skipping.")
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

FUNC BOOL ADD_FADING_VEHICLE(INT i)
	IF IS_BIT_SET(serverBD.iVehicleFadeBS, i)
		RETURN TRUE
	ENDIF
	IF DOES_ENTITY_EXIST(NET_TO_VEH(serverBD.customVehicleNetIDs[i]))
		IF IS_VEHICLE_EMPTY(NET_TO_VEH(serverBD.customVehicleNetIDs[i]))
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - ADDING FADING VEHICLE: ", i)
			SET_BIT(serverBD.iVehicleFadeBS, i)
			NETWORK_FADE_OUT_ENTITY(NET_TO_ENT(serverBD.customVehicleNetIDs[i]), FALSE, TRUE)
			RETURN TRUE
		ELSE
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - ADD_FADING_VEHICLE - Kicking players out of vehicle: ", i)
			
			INT j
			PLAYER_INDEX tempPlayer
			
			REPEAT NUM_NETWORK_PLAYERS j
				tempPlayer = INT_TO_PLAYERINDEX(j)
				
				IF IS_NET_PLAYER_OK(tempPlayer, FALSE)
					IF IS_PED_IN_VEHICLE(GET_PLAYER_PED(tempPlayer), NET_TO_VEH(serverBD.customVehicleNetIDs[i]))
						BROADCAST_LEAVE_VEHICLE(SPECIFIC_PLAYER(tempPlayer), TRUE, 0, 0)
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL MAINTAIN_HANGAR_VEHICLES()
	BOOL bHasEmblem
	BOOL bDeleteVeh = FALSE
	
	INT i = 0
	INT iSaveSlot
	INT iDisplaySlot
	
	MAINTAIN_CURRENT_PV_SLOT_GLOBAL()
	MAINTAIN_DELETE_FADING_VEHICLES()
	
	REPEAT MAX_DISPLAYED_VEHS_IN_HANGAR i
		bDeleteVeh = FALSE
		iDisplaySlot = i + GET_PROPERTY_SLOT_DISPLAY_START_INDEX(PROPERTY_OWNED_SLOT_HANGAR)
		
		MPSV_GET_SAVE_SLOT_FROM_DISPLAY_SLOT(iDisplaySlot, iSaveSlot,FALSE)
		
		IF thisHangar.pOwner = PLAYER_ID()
			IF iSaveSlot >= 0
			AND g_MpSavedVehicles[iSaveSlot].vehicleSetupMP.VehicleSetup.eModel != DUMMY_MODEL_FOR_SCRIPT
			AND NOT IS_HANGAR_DISPLAY_SLOT_IN_STORAGE(iDisplaySlot)
			AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(g_MpSavedVehicles[iSaveSlot].vehicleSetupMP.VehicleSetup.eModel,PROPERTY_OWNED_SLOT_HANGAR)
			AND IS_VEHICLE_AVAILABLE_FOR_GAME_IN_HANGAR(i, iSaveSlot)
				IF (g_iSimpleInteriorState = SIMPLE_INT_STATE_INITIALISE
				OR g_iSimpleInteriorState = SIMPLE_INT_STATE_LOADING
				OR g_iSimpleInteriorState = SIMPLE_INT_STATE_START_TELEPORT
				OR g_iSimpleInteriorState = SIMPLE_INT_STATE_TELEPORTING
				OR g_iSimpleInteriorState = SIMPLE_INT_STATE_TELEPORTING_IN_SVM)
				AND SIMPLE_INTERIOR_IS_LOCAL_PLAYER_ENTERING_WITH_VEHICLE()
				AND IS_HANGAR_SERVER_STATE(HANGAR_SERVER_STATE_IDLE)
					PRINTLN("MAINTAIN_HANGAR_VEHICLES: not creating vehicle due to script state.")
					RETURN FALSE
				ENDIF
				
				IF NOT SIMPLE_INTERIOR_IS_LOCAL_PLAYER_ENTERING_WITH_VEHICLE()
				OR g_bEnteredInPegasus
					IF IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS, MP_SAVED_VEHICLE_ENTERING_SIMPLE_INTERIOR)
						CLEAR_BIT(g_MpSavedVehicles[iSaveSlot].iVehicleBS, MP_SAVED_VEHICLE_ENTERING_SIMPLE_INTERIOR)
					ENDIF
				ENDIF
				
				// Allow vehicle storage help text to be displayed on entry to hangar.
				g_SimpleInteriorData.bDisplayHelpTextInHangar = TRUE
				
				// Check for vehicles that are incorrectly not in storage
				IF (GET_DISPLAY_HANGAR_SLOT_SIZE(iDisplaySlot) != GET_MODEL_HANGAR_SLOT_SIZE(g_MpSavedVehicles[iSaveSlot].vehicleSetupMP.VehicleSetup.eModel)
				OR GET_HANGAR_SLOT_BLOCKERS(iDisplaySlot) != 0)
					ASSERTLN("AM_MP_HANGAR - MAINTAIN_HANGAR_VEHICLES - Found ", GET_MODEL_NAME_FOR_DEBUG(g_MpSavedVehicles[iSaveSlot].vehicleSetupMP.VehicleSetup.eModel), " out of storage in unsuitable slot ", iDisplaySlot, ". Returning to storage.")
					PRINTLN("AM_MP_HANGAR - MAINTAIN_HANGAR_VEHICLES - Found ", GET_MODEL_NAME_FOR_DEBUG(g_MpSavedVehicles[iSaveSlot].vehicleSetupMP.VehicleSetup.eModel), " out of storage in unsuitable slot ", iDisplaySlot, ". Returning to storage.")
					SET_HANGAR_DISPLAY_SLOT_IN_STORAGE(iDisplaySlot, TRUE)
				ENDIF
				
				IF NOT IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS, MP_SAVED_VEHICLE_OUT_GARAGE)
				AND NOT IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS, MP_SAVED_VEHICLE_DESTROYED)
				AND NOT IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS, MP_SAVED_VEHICLE_IMPOUNDED)
				AND NOT IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS, MP_SAVED_VEHICLE_EXITING_SIMPLE_INTERIOR)
				AND NOT IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS, MP_SAVED_VEHICLE_ENTERING_SIMPLE_INTERIOR)
				AND MPGlobalsAmbience.iVDPersonalVehicleSlot != iSaveSlot
				AND (thisHangar.eCutsceneState <= HANGAR_CUTSCENE_STATE_INITIALISE OR thisHangar.eCutsceneState = HANGAR_CUTSCENE_STATE_CLEANUP)
					//PRINTLN("found a valid vehicle: ", iDisplaySlot)
					IF IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS, MP_SAVED_VEHICLE_UPDATE_VEHICLE)
						//PRINTLN("updating: ", iDisplaySlot)
						IF NOT HAS_NET_TIMER_STARTED(failSafeClearVehicleDelay[i])
							START_NET_TIMER(failSafeClearVehicleDelay[i], TRUE)
						ENDIF
						
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.customVehicleNetIDs[i])
							IF IS_NET_VEHICLE_DRIVEABLE(serverBD.customVehicleNetIDs[i])
								IF GET_VEHICLE_DOOR_LOCK_STATUS(NET_TO_VEH(serverBD.customVehicleNetIDs[i])) != VEHICLELOCK_LOCKED
									IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.customVehicleNetIDs[i])
										SET_VEHICLE_DOORS_LOCKED(NET_TO_VEH(serverBD.customVehicleNetIDs[i]), VEHICLELOCK_LOCKED)
									ELSE
										NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.customVehicleNetIDs[i])
									ENDIF
								ENDIF
								
								IF IS_VEHICLE_EMPTY(NET_TO_VEH(serverBD.customVehicleNetIDs[i]), TRUE, TRUE)
								OR HAS_NET_TIMER_EXPIRED(failSafeClearVehicleDelay[i], 3000, TRUE)
									IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.customVehicleNetIDs[i])
										IF ADD_FADING_VEHICLE(i)
											serverBD.ivehicleSaveSlot[i] = -1
											CLEAR_BIT(g_MpSavedVehicles[iSaveSlot].iVehicleBS, MP_SAVED_VEHICLE_UPDATE_VEHICLE)
										ENDIF
									ELSE
										NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.customVehicleNetIDs[i])
									ENDIF
								ENDIF
							ELSE
								IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.customVehicleNetIDs[i])
									IF ADD_FADING_VEHICLE(i)
										serverBD.ivehicleSaveSlot[i] = -1
										CLEAR_BIT(g_MpSavedVehicles[iSaveSlot].iVehicleBS, MP_SAVED_VEHICLE_UPDATE_VEHICLE)
									ENDIF
								ELSE
									NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.customVehicleNetIDs[i])
								ENDIF
							ENDIF
						ELSE
							CLEAR_BIT(g_MpSavedVehicles[iSaveSlot].iVehicleBS, MP_SAVED_VEHICLE_UPDATE_VEHICLE)
						ENDIF
					ELIF NOT IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS, MP_SAVED_VEHICLE_JUST_PURCHASED)
						IF NOT FADING_VEHICLES_BLOCK_CREATE(iDisplaySlot)
							IF IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS, MP_SAVED_VEHICLE_HAS_CREW_EMBLEM)
								bHasEmblem = TRUE
							ELSE
								bHasEmblem = FALSE
							ENDIF
							
							RUN_COMMERCIAL_VEHICLE_IN_SLOT_CHECK(iSaveSlot)
							
							IF NOT IS_MODEL_VALID_FOR_PERSONAL_VEHICLE(g_MpSavedVehicles[iSaveSlot].vehicleSetupMP.VehicleSetup.eModel)
								//PRINTLN("not valid: ", iDisplaySlot)
								CLEAR_MP_SAVED_VEHICLE_SLOT(iSaveSlot)
							ELSE
								//PRINTLN("Valid: ", iDisplaySlot)
								IF NOT CREATE_MODDED_CAR_FOR_HANGAR(g_MpSavedVehicles[iSaveSlot].vehicleSetupMP.VehicleSetup, i, iSaveSlot, bHasEmblem)
									RETURN FALSE
								ENDIF
							ENDIF
							
							RESET_NET_TIMER(failSafeClearVehicleDelay[i])
						ENDIF
					ELSE
						bDeleteVeh = TRUE
					ENDIF
				ELSE
					IF IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS, MP_SAVED_VEHICLE_OUT_GARAGE)
					OR IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS, MP_SAVED_VEHICLE_DESTROYED)
					OR IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS, MP_SAVED_VEHICLE_IMPOUNDED)
					OR IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS, MP_SAVED_VEHICLE_EXITING_SIMPLE_INTERIOR)
					OR IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS, MP_SAVED_VEHICLE_ENTERING_SIMPLE_INTERIOR)
					OR MPGlobalsAmbience.iVDPersonalVehicleSlot = iSaveSlot
					OR thisHangar.eCutsceneState > HANGAR_CUTSCENE_STATE_INITIALISE 
					OR thisHangar.eCutsceneState != HANGAR_CUTSCENE_STATE_CLEANUP
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - MAINTAIN_HANGAR_VEHICLES - Deleting vehicle: ", i)
						
						#IF IS_DEBUG_BUILD
						PRINTLN("thisHangar.eCutsceneState = ",thisHangar.eCutsceneState)
						IF IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS, MP_SAVED_VEHICLE_OUT_GARAGE)
							PRINTLN("AM_MP_HANGAR - MAINTAIN_HANGAR_VEHICLES - MP_SAVED_VEHICLE_OUT_GARAGE")
						ENDIF
						IF IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS, MP_SAVED_VEHICLE_DESTROYED)
							PRINTLN("AM_MP_HANGAR - MAINTAIN_HANGAR_VEHICLES - MP_SAVED_VEHICLE_DESTROYED")
						ENDIF
						IF IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS, MP_SAVED_VEHICLE_IMPOUNDED)
							PRINTLN("AM_MP_HANGAR - MAINTAIN_HANGAR_VEHICLES - MP_SAVED_VEHICLE_IMPOUNDED")
						ENDIF
						IF IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS, MP_SAVED_VEHICLE_EXITING_SIMPLE_INTERIOR)
							PRINTLN("AM_MP_HANGAR - MAINTAIN_HANGAR_VEHICLES - MP_SAVED_VEHICLE_EXITING_SIMPLE_INTERIOR")
						ENDIF
						IF IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS, MP_SAVED_VEHICLE_ENTERING_SIMPLE_INTERIOR)
							PRINTLN("AM_MP_HANGAR - MAINTAIN_HANGAR_VEHICLES - MP_SAVED_VEHICLE_ENTERING_SIMPLE_INTERIOR")
						ENDIF
						IF MPGlobalsAmbience.iVDPersonalVehicleSlot = iSaveSlot
							PRINTLN("AM_MP_HANGAR - MAINTAIN_HANGAR_VEHICLES - MPGlobalsAmbience.iVDPersonalVehicleSlot = iSaveSlot")
						ENDIF
						#ENDIF
						
						bDeleteVeh = TRUE
					ENDIF
				ENDIF
			ELSE
				PRINTLN("AM_MP_HANGAR - MAINTAIN_HANGAR_VEHICLES - save slot invalid getting rid of vehicle: ", iDisplaySlot)
				PRINTLN("IS_HANGAR_DISPLAY_SLOT_IN_STORAGE = ",IS_HANGAR_DISPLAY_SLOT_IN_STORAGE(iDisplaySlot))
				IF iSaveSlot >= 0
					PRINTLN("IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT = ",IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(g_MpSavedVehicles[iSaveSlot].vehicleSetupMP.VehicleSetup.eModel,PROPERTY_OWNED_SLOT_HANGAR))
					PRINTLN("IS_VEHICLE_AVAILABLE_FOR_GAME_IN_HANGAR = ",IS_VEHICLE_AVAILABLE_FOR_GAME_IN_HANGAR(i, iSaveSlot))
				ENDIF
				bDeleteVeh = TRUE
			ENDIF
			
			IF iSaveSlot >= 0
			AND IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS, MP_SAVED_VEHICLE_EXITING_SIMPLE_INTERIOR)
				PRINTLN("AM_MP_HANGAR - MAINTAIN_HANGAR_VEHICLES - MP_SAVED_VEHICLE_EXITING_SIMPLE_INTERIOR is set, not deleting")
				
				bDeleteVeh = FALSE
			ENDIF
		ELSE
			IF thisHangar.pOwner != INVALID_PLAYER_INDEX()
				IF IS_NET_PLAYER_OK(thisHangar.pOwner, FALSE, TRUE)
					IF NOT NETWORK_IS_PLAYER_A_PARTICIPANT(thisHangar.pOwner)
						IF GlobalplayerBD_FM[NATIVE_TO_INT(thisHangar.pOwner)].iCurrentsActivePersonalVehicle = serverBD.ivehicleSaveSlot[i]
						AND serverBD.ivehicleSaveSlot[i] >= 0
							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.customVehicleNetIDs[i])
								IF IS_NET_VEHICLE_DRIVEABLE(serverBD.customVehicleNetIDs[i])
									IF NOT HAS_NET_TIMER_STARTED(failSafeClearVehicleDelay[i])
										START_NET_TIMER(failSafeClearVehicleDelay[i], TRUE)
									ENDIF
									
									IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.customVehicleNetIDs[i])
										IF GET_VEHICLE_DOOR_LOCK_STATUS(NET_TO_VEH(serverBD.customVehicleNetIDs[i])) != VEHICLELOCK_LOCKED
											SET_VEHICLE_DOORS_LOCKED(NET_TO_VEH(serverBD.customVehicleNetIDs[i]), VEHICLELOCK_LOCKED)
										ENDIF
									ENDIF
									
									IF IS_VEHICLE_EMPTY(NET_TO_VEH(serverBD.customVehicleNetIDs[i]), TRUE, TRUE)
									OR HAS_NET_TIMER_EXPIRED(failSafeClearVehicleDelay[i], 3000, TRUE)
										IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.customVehicleNetIDs[i])
											IF ADD_FADING_VEHICLE(i)
												serverBD.ivehicleSaveSlot[i] = -1
												
												IF iSaveSlot >= 0
													CLEAR_BIT(g_MpSavedVehicles[iSaveSlot].iVehicleBS, MP_SAVED_VEHICLE_UPDATE_VEHICLE)
												ENDIF
											ENDIF
										ELSE
											NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.customVehicleNetIDs[i])
										ENDIF
									ELSE
										IF IS_NET_PLAYER_OK(PLAYER_ID())
											IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.customVehicleNetIDs[i]))
												TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID(), 0, ECF_WARP_IF_DOOR_IS_BLOCKED)
											ENDIF
										ENDIF
									ENDIF
								ELSE
									IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.customVehicleNetIDs[i])
										IF ADD_FADING_VEHICLE(i)
											serverBD.ivehicleSaveSlot[i] = -1
											
											IF iSaveSlot >= 0
												CLEAR_BIT(g_MpSavedVehicles[iSaveSlot].iVehicleBS, MP_SAVED_VEHICLE_UPDATE_VEHICLE)
											ENDIF
										ENDIF
									ELSE
										NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.customVehicleNetIDs[i])
									ENDIF
								ENDIF
							ELSE
								RESET_NET_TIMER(failSafeClearVehicleDelay[i])
							ENDIF
						ENDIF
					ENDIF
				ELSE
					bDeleteVeh = TRUE
				ENDIF
			ELSE
				bDeleteVeh = TRUE
			ENDIF
		ENDIF
		
		IF bDeleteVeh
			IF thisHangar.pOwner = PLAYER_ID()
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.customVehicleNetIDs[i])
					IF IS_NET_VEHICLE_DRIVEABLE(serverBD.customVehicleNetIDs[i])
						IF GET_VEHICLE_DOOR_LOCK_STATUS(NET_TO_VEH(serverBD.customVehicleNetIDs[i])) != VEHICLELOCK_LOCKED
							IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.customVehicleNetIDs[i])
								SET_VEHICLE_DOORS_LOCKED(NET_TO_VEH(serverBD.customVehicleNetIDs[i]), VEHICLELOCK_LOCKED)
							ELSE
								NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.customVehicleNetIDs[i])
							ENDIF
						ENDIF
						
						IF NOT HAS_NET_TIMER_STARTED(failSafeClearVehicleDelay[i])
							START_NET_TIMER(failSafeClearVehicleDelay[i], TRUE)
						ENDIF
						
						IF IS_VEHICLE_EMPTY(NET_TO_VEH(serverBD.customVehicleNetIDs[i]), TRUE, TRUE)
						OR HAS_NET_TIMER_EXPIRED(failSafeClearVehicleDelay[i], 3000, TRUE)
							IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.customVehicleNetIDs[i])
								IF ADD_FADING_VEHICLE(i)
								
									IF iSaveSlot >= 0
										CLEAR_BIT(g_MpSavedVehicles[iSaveSlot].iVehicleBS, MP_SAVED_VEHICLE_UPDATE_VEHICLE)
									ENDIF
									
									serverBD.ivehicleSaveSlot[i] = -1
								ENDIF
							ELSE
								IF iSaveSlot >= 0
									IF NOT IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS, MP_SAVED_VEHICLE_UPDATE_VEHICLE)
										SET_BIT(g_MpSavedVehicles[iSaveSlot].iVehicleBS, MP_SAVED_VEHICLE_UPDATE_VEHICLE)
									ENDIF
								ENDIF
								
								NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.customVehicleNetIDs[i])
							ENDIF
						ELSE
							IF IS_NET_PLAYER_OK(PLAYER_ID())
								IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.customVehicleNetIDs[i]))
									TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID(), 0, ECF_WARP_IF_DOOR_IS_BLOCKED)
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.customVehicleNetIDs[i])
							IF ADD_FADING_VEHICLE(i)
								serverBD.ivehicleSaveSlot[i] = -1
							ENDIF
						ELSE
							NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.customVehicleNetIDs[i])
						ENDIF
					ENDIF
				ELSE
					RESET_NET_TIMER(failSafeClearVehicleDelay[i])
				ENDIF
			ELSE
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.customVehicleNetIDs[i])
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.customVehicleNetIDs[i])
						DELETE_NET_ID(serverBD.customVehicleNetIDs[i])
					ELSE
						NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.customVehicleNetIDs[i])
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.customVehicleNetIDs[i])
			IF IS_BIT_SET(serverBD.iCreatedBS, i)
				serverBD.iVehicleCreated--
				CLEAR_BIT(serverBD.iVehicleResBS, i)
				CLEAR_BIT(serverBD.iCreatedBS, i)
				serverBD.iCarSpawnBS[i] = 0
				
				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - MAINTAIN_HANGAR_GARAGE_VEHICLES - a vehicle is now not driveable reducing serverBD.iVehicleCreated to :", serverBD.iVehicleCreated)
			ENDIF
			
			RESET_NET_TIMER(failSafeClearVehicleDelay[i])
		ELSE
			IF !IS_BIT_SET(thisHangar.iBS2, BS_HANGAR_SET_VEH_ON_GROUND_PROPERLY)
			AND i < HANGAR_DISPLAY_SLOT_MASSIVE
				IF IS_VEHICLE_MODEL(NET_TO_VEH(serverBD.customVehicleNetIDs[i]), STRIKEFORCE)
					IF SET_VEHICLE_ON_GROUND_PROPERLY(NET_TO_VEH(serverBD.customVehicleNetIDs[i]))
						SET_BIT(thisHangar.iBS2, BS_HANGAR_SET_VEH_ON_GROUND_PROPERLY)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - MAINTAIN_HANGAR_GARAGE_VEHICLES - BS_HANGAR_SET_VEH_ON_GROUND_PROPERLY")
					ENDIF
				ENDIF
			ENDIF	
		ENDIF
	ENDREPEAT
	
	CLEAR_BIT(thisHangar.sVehManageData.iManageVehCreateBS,MANAGE_VEH_CREATE_PENDING_FADE)
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_ANYONE_EXITING_A_VEHICLE()
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		IF NETWORK_IS_PLAYER_A_PARTICIPANT(INT_TO_PLAYERINDEX(i))
			PED_INDEX tempPed = GET_PLAYER_PED(INT_TO_PLAYERINDEX(i))
			
			IF NOT IS_PED_INJURED(tempPed)
				IF GET_IS_TASK_ACTIVE(tempPed,CODE_TASK_EXIT_VEHICLE)  
				OR GET_SCRIPT_TASK_STATUS(tempPed, SCRIPT_TASK_LEAVE_ANY_VEHICLE) = PERFORMING_TASK
				OR GET_SCRIPT_TASK_STATUS(tempPed, SCRIPT_TASK_LEAVE_ANY_VEHICLE) = WAITING_TO_START_TASK
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_CLOSE_PLANE_DOORS()
	BOOL bReinitTimer
	IF IS_NET_VEHICLE_DRIVEABLE(serverBD.customVehicleNetIDs[iDoorCloseSlowLoop])
		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.customVehicleNetIDs[iDoorCloseSlowLoop])	
			INT iNumDoor
			VEHICLE_INDEX vehIndex = NET_TO_VEH(serverBD.customVehicleNetIDs[iDoorCloseSlowLoop])
			FOR iNumDoor = 0 TO 5
				IF GET_VEHICLE_DOOR_ANGLE_RATIO(vehIndex , INT_TO_ENUM(SC_DOOR_LIST, iNumDoor)) >  0
					IF IS_VEHICLE_EMPTY(vehIndex,TRUE,TRUE,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
						IF NOT IS_ANYONE_EXITING_A_VEHICLE()
							IF HAS_NET_TIMER_STARTED(st_ExitingVehicleTimer)
								IF HAS_NET_TIMER_EXPIRED(st_ExitingVehicleTimer,1000,TRUE)
									SET_VEHICLE_DOORS_SHUT(vehIndex, FALSE)
									PRINTLN("MAINTAIN_CLOSE_PLANE_DOORS: closing doors on vehicle #",iDoorCloseSlowLoop)
								ELSE
									PRINTLN("MAINTAIN_CLOSE_PLANE_DOORS: NOT closing doors waiting for timer vehicle #",iDoorCloseSlowLoop)
								ENDIF
							ELSE
								REINIT_NET_TIMER(st_ExitingVehicleTimer,TRUE)
							ENDIF
						ELSE
							bReinitTimer = TRUE
							PRINTLN("MAINTAIN_CLOSE_PLANE_DOORS: NOT closing doors players exiting vehicle #",iDoorCloseSlowLoop)
						ENDIF
					ELSE
						bReinitTimer = TRUE
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
	ENDIF
	IF bReinitTimer 
		REINIT_NET_TIMER(st_ExitingVehicleTimer,TRUE)
	ENDIF

	iDoorCloseSlowLoop++
	IF iDoorCloseSlowLoop >= MAX_DISPLAYED_VEHS_IN_HANGAR
		iDoorCloseSlowLoop = 0
	ENDIF
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════╡ MAIN LOGIC PROC ╞══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC MAINTAIN_HANGAR_BLIPS()
	IF thisHangar.pOwner = PLAYER_ID()
		IF NOT DOES_BLIP_EXIST(bComputerBlip)
		AND NOT IS_PLAYER_IN_ZOOMED_OUT_RADAR_OR_MAP(PLAYER_ID())
			bComputerBlip = CREATE_BLIP_FOR_COORD(<<-1239.8896, -3002.5691, -42.7662>>)
			SET_BLIP_SPRITE(bComputerBlip, RADAR_TRACE_LAPTOP)
			SET_BLIP_NAME_FROM_TEXT_FILE(bComputerBlip, "OR_PC_BLIP")
			SET_BLIP_DISPLAY(bComputerBlip, DISPLAY_BOTH)
		ELSE
			IF IS_PLAYER_IN_ZOOMED_OUT_RADAR_OR_MAP(PLAYER_ID())
				REMOVE_BLIP(bComputerBlip)
			ENDIF
		ENDIF
		
		IF NOT DOES_BLIP_EXIST(bStorageBlip)
		AND NOT g_sMPTunables.bDISABLE_VEHICLE_MANAGEMENT_HANGAR
		AND NOT IS_PLAYER_IN_ZOOMED_OUT_RADAR_OR_MAP(PLAYER_ID())
			bStorageBlip = CREATE_BLIP_FOR_COORD(GET_HANGAR_MANAGEMENT_LOCATE())
			SET_BLIP_SPRITE(bStorageBlip, RADAR_TRACE_CRIM_CARSTEAL)
			SET_BLIP_NAME_FROM_TEXT_FILE(bStorageBlip, "HANGAR_PAM_BLP")
			SET_BLIP_DISPLAY(bStorageBlip, DISPLAY_BOTH)
		ELIF DOES_BLIP_EXIST(bStorageBlip)
			IF IS_PLAYER_IN_ZOOMED_OUT_RADAR_OR_MAP(PLAYER_ID())
				REMOVE_BLIP(bStorageBlip)
			ENDIF
		ENDIF
	ELSE
		IF DOES_BLIP_EXIST(bComputerBlip)
			REMOVE_BLIP(bComputerBlip)
		ENDIF
		IF DOES_BLIP_EXIST(bStorageBlip)
			REMOVE_BLIP(bStorageBlip)
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_HANGAR_DESK_SEAT()
	IF NOT IS_BIT_SET(thisHangar.iBS, BS_HANGAR_FORCED_CHAIR_TO_ROOM)
	AND IS_SCREEN_FADED_IN()
	
		INTERIOR_INSTANCE_INDEX interiorIndex
		VECTOR vInteriorPosition
		FLOAT fInteriorHeading
		TEXT_LABEL_63 strInteriorType
		INT iRoomKey = HASH("GtaMloRoom001")
	
		GET_SIMPLE_INTERIOR_INTERIOR_TYPE_AND_POSITION(thisHangar.eSimpleInteriorID, strInteriorType, vInteriorPosition, fInteriorHeading)
		interiorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(vInteriorPosition, strInteriorType)

		IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
			IF IS_VALID_INTERIOR(interiorIndex) AND IS_INTERIOR_READY(interiorIndex)
				IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.serverSeatBD.niOfficeSeats[0])
				AND IS_ENTITY_ALIVE(NET_TO_OBJ(serverBD.serverSeatBD.niOfficeSeats[0]))
					FORCE_ROOM_FOR_ENTITY(NET_TO_OBJ(serverBD.serverSeatBD.niOfficeSeats[0]), interiorIndex, iRoomKey)
					SET_BIT(thisHangar.iBS, BS_HANGAR_FORCED_CHAIR_TO_ROOM)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - MAINTAIN_HANGAR_DESK_SEAT Forced Chair to GtaMloRoom001")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_HANGAR_BEHAVIOURS()
	// Allow player to sprint in hangar interior.
	SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_IgnoreInteriorCheckForSprinting, TRUE)
	SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DontUseSprintEnergy, TRUE)
ENDPROC

PROC MAINTAIN_HANGAR_HELP()

	IF IS_SCREEN_FADED_IN()
	AND NOT IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
	AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
	AND NOT IS_BROWSER_OPEN()
	AND NOT IS_PHONE_ONSCREEN(FALSE)
	AND NOT GB_IS_LOCAL_HELP_BIT_SET(eGB_LOCAL_HELP_BITSET_NOTIFICATION_FAIL_CALL_SET_UP)
	AND (PLAYER_ID() != INVALID_PLAYER_INDEX() AND thisHangar.pOwner = PLAYER_ID())

		INT iStatValue 

		SWITCH thisHangar.iHelpTextID
			CASE 0
				IF NOT IS_HANGAR_SETUP_MISSION_COMPLETE()
				AND NOT IS_SMUGGLER_VARIATION_A_SETUP()
					IF NOT HAS_NET_TIMER_STARTED(thisHangar.tHelpTimer)
						START_NET_TIMER(thisHangar.tHelpTimer)
					ENDIF

					IF NOT HAS_NET_TIMER_EXPIRED(thisHangar.tHelpTimer, 7500)
						PRINT_HELP("HANGAR_LPTP_HLP")
					ELSE
						RESET_NET_TIMER(thisHangar.tHelpTimer)
						thisHangar.iHelpTextID = 1
					ENDIF
				ELSE
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HANGAR_LPTP_HLP")
						CLEAR_HELP()
					ENDIF
					
					thisHangar.iHelpTextID = 1
				ENDIF
			BREAK
			CASE 1
				IF g_SimpleInteriorData.bDisplayHelpTextInHangar
				AND GET_PACKED_STAT_INT(PACKED_MP_PAM_HELP_COUNTER) < 3
				AND NOT g_sMPTunables.bDISABLE_VEHICLE_MANAGEMENT_HANGAR
					IF NOT HAS_NET_TIMER_STARTED(thisHangar.tHelpTimer)
						START_NET_TIMER(thisHangar.tHelpTimer)	
					ENDIF
					
					IF NOT HAS_NET_TIMER_EXPIRED(thisHangar.tHelpTimer, 7500)
						PRINT_HELP("HANGAR_PAM_HLP")
					ELSE
						iStatValue = GET_PACKED_STAT_INT(PACKED_MP_PAM_HELP_COUNTER)
						SET_PACKED_STAT_INT(PACKED_MP_PAM_HELP_COUNTER, iStatValue + 1)

						CDEBUG1LN(DEBUG_PROPERTY, "MAINTAIN_HANGAR_HELP - Updating PACKED_MP_PAM_HELP_COUNTER = ", GET_PACKED_STAT_INT(PACKED_MP_PAM_HELP_COUNTER))
						
						// Ensure no help text is displayed after this.
						thisHangar.iHelpTextID = -1
					ENDIF
				ELSE
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HANGAR_PAM_HLP")
						CLEAR_HELP()
					ENDIF
					
					thisHangar.iHelpTextID = -1
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_ON_HANGAR_FLOOR(PLAYER_INDEX playerID)
	IF IS_ENTITY_IN_ANGLED_AREA(GET_PLAYER_PED(playerID), <<-1266.965088,-3044.419189,-49.489574>>, <<-1266.986328,-2959.695557,-43.489571>>, 40.375)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// Ugly but needs to set flag on each player each frame 
PROC FORCE_ZERO_MASS_FOR_ALL_PLAYERS_IN_HANGAR()
	INT i
	PLAYER_INDEX playerID
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
			playerID = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
			IF IS_NET_PLAYER_OK(playerID)
				IF IS_PLAYER_ON_HANGAR_FLOOR(playerID)
					FORCE_ZERO_MASS_IN_COLLISIONS(GET_PLAYER_PED(playerID))
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC MAINTAIN_HELI_BLADES_SPEED()
	IF IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
	AND GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
	AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
	AND g_iSimpleInteriorState = SIMPLE_INT_STATE_IDLE
		SET_HELI_BLADES_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), 0.0)
	ENDIF
ENDPROC

PROC MAINTAIN_HANGAR_INTERIOR_MAP()
	IF IS_PED_INJURED(PLAYER_PED_ID())
		EXIT
	ENDIF
	
	INT iLevel
	VECTOR vPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	STRING sHangarMap = "sm_SmugDLC_Int_01"
	IF (vPos.z < -45.5)
		iLevel = 0
		IF IS_PLAYER_HANGAR_MODSHOP_PURCHASED(thisHangar.pOwner)
			sHangarMap = "sm_SmugDLC_Int_01_level_0_ModArea"
		ELSE
			sHangarMap = "sm_SmugDLC_Int_01_level_0_NoModArea"
		ENDIF
	ELIF (vPos.z < -39.25)
		iLevel = 1
		IF IS_PLAYER_HANGAR_SAVEBED_TRADITIONAL_PURCHASED(thisHangar.pOwner)
		OR IS_PLAYER_HANGAR_SAVEBED_MODERN_PURCHASED(thisHangar.pOwner)
			IF IS_PLAYER_HANGAR_MODSHOP_PURCHASED(thisHangar.pOwner)
				sHangarMap = "sm_SmugDLC_Int_01_level_1_Bed_ModArea"
			ELSE
				sHangarMap = "sm_SmugDLC_Int_01_level_1_Bed_NoModArea"
			ENDIF
		ELSE
			IF IS_PLAYER_HANGAR_MODSHOP_PURCHASED(thisHangar.pOwner)
				sHangarMap = "sm_SmugDLC_Int_01_level_1_NoBed_ModArea"
			ELSE
				sHangarMap = "sm_SmugDLC_Int_01_level_1_NoBed_NoModArea"
			ENDIF
		ENDIF
	ELIF (vPos.z < -34.75)
		iLevel = 2
		sHangarMap = "sm_SmugDLC_Int_01_lvl_2"
	ELIF (vPos.z < -23)
		iLevel = 3
		sHangarMap = "sm_SmugDLC_Int_01_lvl_3"
	ENDIF
	
	IF (iHangarInteriorHash != GET_HASH_KEY(sHangarMap))
		PRINTLN("MAINTAIN_HANGAR_INTERIOR_MAP ", sHangarMap, ", change hash ", iHangarInteriorHash, " -> ", GET_HASH_KEY(sHangarMap))
		iHangarInteriorHash = GET_HASH_KEY(sHangarMap)
	ENDIF
	IF iHangarInteriorHash != 0
	AND iHangarInteriorHash != HASH("")
		SET_RADAR_AS_INTERIOR_THIS_FRAME(iHangarInteriorHash, -1266.8020, -3014.8364, DEFAULT, iLevel)
	ENDIF
ENDPROC

PROC MAINTAIN_DRIVING_IN_FLAG()
	IF IS_SCREEN_FADED_IN()
		IF IS_DRIVER_ENTERING_SIMPLE_INTERIOR(PLAYER_ID())
			SET_DRIVER_ENTERING_SIMPLE_INTERIOR(FALSE)
		ENDIF
		
		IF IS_PASSENGER_ENTERING_WITH_DRIVER(PLAYER_ID())
			SET_PASSENGER_ENTERING_WITH_DRIVER_TO_SIMPLE_INTERIOR(FALSE)
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_HANGAR_ENTRANCE_HELP_TEXT()
	IF thisHangar.pOwner = PLAYER_ID()
	AND IS_NET_PLAYER_OK(PLAYER_ID())
	AND IS_SCREEN_FADED_IN()
	AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
	AND IS_BIT_SET(thisHangar.iBS, BS_HANGAR_CALLED_CLEAR_HELP)
	AND NOT IS_BIT_SET(thisHangar.sVehManageData.iManageVehMenuBS, MANAGE_VEH_MENU_DISPLAYING)
	AND NOT globalPropertyEntryData.bChoosenToReplaceVehicle
		IF IS_BIT_SET(thisHangar.iBS, BS_HANGAR_DATA_ENTERED_IN_PEGASUS)
			PRINT_HELP("HANGAR_PEG")
			CLEAR_BIT(thisHangar.iBS, BS_HANGAR_DATA_ENTERED_IN_PEGASUS)
			EXIT
		ENDIF
		
		IF IS_BIT_SET(thisHangar.iBS, BS_HANGAR_DATA_ENTERED_IN_STORAGE)
			PRINT_HELP("HANGAR_STORAGE")
			CLEAR_BIT(thisHangar.iBS, BS_HANGAR_DATA_ENTERED_IN_STORAGE)
			EXIT
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_HANGAR_DISPLAY_SLOT_PLAYER_IS_IN()
	IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), FALSE)
		IF (g_iHangarDisplaySlotPlayerIsIn != 0)
			PRINTLN("MAINTAIN_HANGAR_DISPLAY_SLOT_PLAYER_IS_IN reset g_iHangarDisplaySlotPlayerIsIn, currently ", g_iHangarDisplaySlotPlayerIsIn)
			g_iHangarDisplaySlotPlayerIsIn = 0
		ENDIF
	ELSE
		INT iVeh, iVehicleSaveSlot = -1
		REPEAT MAX_SAVED_VEHS_IN_HANGAR iVeh
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.customVehicleNetIDs[iVeh])
				IF IS_NET_VEHICLE_DRIVEABLE(serverBD.customVehicleNetIDs[iVeh])
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.customVehicleNetIDs[iVeh]), FALSE)
						//
						iVehicleSaveSlot = serverBD.ivehicleSaveSlot[iVeh]
						iVeh = MAX_SAVED_VEHS_IN_HANGAR+1	//bail
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		IF (iVehicleSaveSlot != -1)
			INT iDisplaySlotToUse = 0
			MPSV_GET_DISPLAY_SLOT_FROM_SAVE_SLOT(iVehicleSaveSlot, iDisplaySlotToUse)
				
			IF (g_iHangarDisplaySlotPlayerIsIn != iDisplaySlotToUse)
				PRINTLN("MAINTAIN_HANGAR_DISPLAY_SLOT_PLAYER_IS_IN set g_iHangarDisplaySlotPlayerIsIn ", iDisplaySlotToUse, ", currently ", g_iHangarDisplaySlotPlayerIsIn)
				g_iHangarDisplaySlotPlayerIsIn = iDisplaySlotToUse
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PRINT_HANGAR_AIRCRAFT_WORKSHOP_HELP()
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF PLAYER_ID() = thisHangar.pOwner
			IF IS_PLAYER_IN_HANGAR(PLAYER_ID())
				IF IS_PLAYER_HANGAR_MODSHOP_PURCHASED(PLAYER_ID())
					IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iHangarBS,  PROPERTY_BROADCAST_HANGAR_BS_HANGAR_CAR_MOD_HELP)
						IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
						AND NOT IS_HELP_MESSAGE_ON_SCREEN()
							INT i
							FOR i = 0 TO MAX_SAVED_VEHS_IN_HANGAR - 1
								IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.customVehicleNetIDs[i])
									IF DOES_HANGAR_AIRCRAFT_HAVE_WEAPON_GROUP(NET_TO_VEH(serverBD.customVehicleNetIDs[i]))
										IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSThree,  BS_SIMPLE_INTERIOR_GLOBAL_BS_THREE_AIRCRAFT_EXIST_IN_HANGAR)
											SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSThree,  BS_SIMPLE_INTERIOR_GLOBAL_BS_THREE_AIRCRAFT_EXIST_IN_HANGAR)
											PRINTLN("PRINT_HANGAR_AIRCRAFT_WORKSHOP_HELP BS_SIMPLE_INTERIOR_GLOBAL_BS_THREE_AIRCRAFT_EXIST_IN_HANGAR TRUE")
										ENDIF
									ENDIF
								ENDIF
							ENDFOR 
						ENDIF
					ENDIF
				ENDIF
			ENDIF	
		ENDIF	
	ENDIF	
ENDPROC 

FUNC BOOL DOES_PLAYER_OWN_HANGAR_LIVING_QUARTERS(PLAYER_INDEX pPlayer)
	IF pPlayer != INVALID_PLAYER_INDEX()
	AND IS_NET_PLAYER_OK(pPlayer)
		IF IS_BIT_SET(playerBD[NATIVE_TO_INT(pPlayer)].iBS, BS_HANGAR_PERSONAL_QUARTERS_TRADITIONAL_PURCHASED)
		OR IS_BIT_SET(playerBD[NATIVE_TO_INT(pPlayer)].iBS, BS_HANGAR_PERSONAL_QUARTERS_MODERN_PURCHASED)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_IN_VEHICLE_CAMERA()
	IF IS_PED_IN_ANY_PLANE(PLAYER_PED_ID())
		IF GET_MODEL_HANGAR_SLOT_SIZE(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))) = HANGAR_SLOT_SIZE_HUGE
		OR GET_MODEL_HANGAR_SLOT_SIZE(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))) = HANGAR_SLOT_SIZE_MASSIVE
			SET_THIRD_PERSON_CAM_RELATIVE_PITCH_LIMITS_THIS_UPDATE(fHUGE_HANGAR_VEHICLE_Pitch_Limit_Min, fHUGE_HANGAR_VEHICLE_Pitch_Limit_Max)
			SET_THIRD_PERSON_CAM_ORBIT_DISTANCE_LIMITS_THIS_UPDATE(fHUGE_HANGAR_VEHICLE_Orbit_Limit_Min, fHUGE_HANGAR_VEHICLE_Orbit_Limit_Max)
			
			IF (g_iHangarDisplaySlotPlayerIsIn-DISPLAY_SLOT_START_HANGAR) = 13
				SET_THIRD_PERSON_CAM_RELATIVE_HEADING_LIMITS_THIS_UPDATE(fHUGE_HANGAR_VEHICLE_Heading_Limit_Min, fHUGE_HANGAR_VEHICLE_Heading_Limit_Max)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_VEHICLE_ZOOM()
	IF NOT IS_PLAYER_IN_ZOOMED_OUT_RADAR_OR_MAP(PLAYER_ID())
	AND IS_PLAYER_IN_HANGAR(PLAYER_ID())
		SET_RADAR_ZOOM_PRECISE(1)
	ELSE
		SET_RADAR_ZOOM_PRECISE(0)	
	ENDIF
ENDPROC

PROC MAINTAIN_INCOMPATIBLE_CLOTHING_RESTRICTIONS()
	IF IS_PLAYER_WALKING_INTO_SIMPLE_INTERIOR(PLAYER_ID())
		IF IS_PED_WEARING_PARACHUTE_MP(PLAYER_PED_ID())
		AND !MPGlobalsAmbience.bDisableTakeOffChute
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - MAINTAIN_INCOMPATIBLE_CLOTHING_RESTRICTIONS: Player is wearing a parachute, removing.")
			SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HAND, 0, 0)
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_MP_RADIO_LOCAL()
	IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_IN_GARAGE)
		CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_IN_GARAGE)
	ENDIF
	IF NATIVE_TO_INT(PLAYER_ID()) != -1
	AND IS_PLAYER_IN_HANGAR(PLAYER_ID())
	AND NOT IS_SCREEN_FADED_OUT()
		CLIENT_MAINTAIN_MP_RADIO(serverBD.MPRadioServer, playerBD[NATIVE_TO_INT(PLAYER_ID())].MPRadioClient, MPRadioLocal, InteriorPropStruct, playerBD[PARTICIPANT_ID_TO_INT()].iActivityRequested, serverBD.activityControl)	
	ELSE
		IF NOT IS_PLAYER_IN_HANGAR(PLAYER_ID())	
			CDEBUG1LN(DEBUG_RADIO, "MAINTAIN_MP_RADIO_LOCAL: player is not in the hangar")
		ENDIF
	ENDIF
	IF IS_SCREEN_FADED_OUT()
		CLEANUP_MP_RADIO(playerBD[NATIVE_TO_INT(PLAYER_ID())].MPRadioClient, MPRadioLocal)
	ENDIF
ENDPROC

PROC MAINTAIN_CAMERA_CONSTRAINTS()
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())	
	AND IS_PLAYER_IN_ANY_AIRCRAFT(PLAYER_PED_ID())
		IF (GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) = BOMBUSHKA)
			SET_THIRD_PERSON_CAM_RELATIVE_PITCH_LIMITS_THIS_UPDATE(-13.1251, 16.3896)
		ENDIF
	ENDIF
ENDPROC

PROC STOP_PLAYER_CONTROL_SHAKE()
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF IS_VEHICLE_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VALKYRIE)
			IF NOT IS_BIT_SET(thisHangar.iBS2, BS_HANGAR_SUPRESS_CONTROL_SHAKE)
				STOP_CONTROL_SHAKE(PLAYER_CONTROL)
				SET_CONTROL_SHAKE_SUPPRESSED_ID(PLAYER_CONTROL,1)
				SET_BIT(thisHangar.iBS2, BS_HANGAR_SUPRESS_CONTROL_SHAKE)
				PRINTLN("STOP_PLAYER_CONTROL_SHAKE - BS_HANGAR_SUPRESS_CONTROL_SHAKE TRUE")
			ENDIF	
		ENDIF
	ELSE
		IF IS_BIT_SET(thisHangar.iBS2, BS_HANGAR_SUPRESS_CONTROL_SHAKE)
			SET_CONTROL_SHAKE_SUPPRESSED_ID(PLAYER_CONTROL,-1)
			CLEAR_BIT(thisHangar.iBS2, BS_HANGAR_SUPRESS_CONTROL_SHAKE)
			PRINTLN("STOP_PLAYER_CONTROL_SHAKE - BS_HANGAR_SUPRESS_CONTROL_SHAKE FALSE")
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_CLEANUP_HANGAR_TIMECYCLE_MODIFIERS()
	IF NOT IS_BIT_SET(thisHangar.iBS2, BS_HANGAR_CLEARED_TIMECYCLE_MODIFIERS)
		IF IS_LOCAL_PLAYER_WALKING_OUT_OF_SIMPLE_INTERIOR()
			IF IS_SCREEN_FADED_OUT()
				CLEANUP_HANGAR_TIMECYCLE_MODIFIERS()
				IF NOT IS_BIT_SET(thisHangar.iBS2, BS_HANGAR_CLEARED_TIMECYCLE_MODIFIERS)
					SET_BIT(thisHangar.iBS2, BS_HANGAR_CLEARED_TIMECYCLE_MODIFIERS)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_HANGAR_BED()
	IF NOT IS_BIT_SET(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_RUNNING_SPAWN_ACTIVITY)
	
		// Stop any load scenes prior to running bed activities.
//		IF IS_NEW_LOAD_SCENE_ACTIVE()
//			NEW_LOAD_SCENE_STOP()
//			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - MAINTAIN_HANGAR_BED - Stopping load scene.")
//		ENDIF
	
		IF NOT (IS_BIT_SET(thisHangar.iBS2, BS_HANGAR_DISABLE_ACTIVITIES_FOR_WARP)	
		AND NOT g_bKillTheBed)
			IF IS_PLAYER_HANGAR_PERSONAL_QUARTERS_PURCHASED(thisHangar.pOwner)
				BED_SCRIPT_LAUNCH_LOGIC(bedStructLeft)
				BED_SCRIPT_LAUNCH_LOGIC(bedStructRight)
			ENDIF
			IF IS_PLAYER_HANGAR_MODSHOP_PURCHASED(thisHangar.pOwner)
				//For now just let them go in from one side
				//BED_SCRIPT_LAUNCH_LOGIC(bedStructMechanicLeft)
				BED_SCRIPT_LAUNCH_LOGIC(bedStructMechanicRight)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

INT iVehicleStagger = 0
PROC MAINTAIN_AIRCRAFT_POSITIONS()
	IF thisHangar.pOwner != INVALID_PLAYER_INDEX()
	AND thisHangar.pOwner != PLAYER_ID()
		IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(thisHangar.pOwner)].SimpleInteriorBD.iBSFive, BS_SIMPLE_INTERIOR_GLOBAL_BS_FIVE_USING_VEHICLE_MANAGEMENT)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
		ENDIF
	ENDIF
	
	FLOAT fDistance
	MP_PROP_OFFSET_STRUCT vehCreationLocation
	
	IF iVehicleStagger = 0
	AND thisHangar.pOwner != INVALID_PLAYER_INDEX()
	AND IS_NET_PLAYER_OK(thisHangar.pOwner)
	AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(thisHangar.pOwner)
		INT i
		REPEAT MAX_DISPLAYED_VEHS_IN_HANGAR i
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.customVehicleNetIDs[i])
			AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.customVehicleNetIDs[i])
			AND IS_ENTITY_ALIVE(NET_TO_VEH(serverBD.customVehicleNetIDs[i]))
				GET_HANGAR_VEH_CREATION_POSITION(vehCreationLocation, i, GET_ENTITY_MODEL(NET_TO_VEH(serverBD.customVehicleNetIDs[i])))
				
				fDistance = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(NET_TO_VEH(serverBD.customVehicleNetIDs[i])), vehCreationLocation.vLoc, FALSE)
				
				IF fDistance > 2.0
				AND fDistance < 15.0
					SET_ENTITY_COORDS(NET_TO_VEH(serverBD.customVehicleNetIDs[i]), vehCreationLocation.vLoc)
					SET_ENTITY_ROTATION(NET_TO_VEH(serverBD.customVehicleNetIDs[i]), vehCreationLocation.vRot)
					IF i < HANGAR_DISPLAY_SLOT_MASSIVE
						SET_VEHICLE_ON_GROUND_PROPERLY(NET_TO_VEH(serverBD.customVehicleNetIDs[i]))
					ENDIF
					PRINTLN("[SIMPLE_INTERIOR] AM_MP_HANGAR - MAINTAIN_AIRCRAFT_POSITIONS - resetting vehicle position: ", i)
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	iVehicleStagger++
	
	IF iVehicleStagger >= 60
		iVehicleStagger = 0
	ENDIF
ENDPROC

PROC MAINTAIN_HANGAR_CHRISTMAS_DECORATIONS()
	IF NOT IS_BIT_SET(thisHangar.iBS, BS_HANGAR_CHRISTMAS_DECORATIONS_CREATED)
		IF NOT g_sMPTunables.bDisable_Christmas_Tree_Apartment
			IF CREATE_CHRISTMAS_DECORATIONS_FOR_SIMPLE_INTERIOR(thisHangar.eSimpleInteriorID, thisHangar.objChristmasTree)
				#IF IS_DEBUG_BUILD
				PRINTLN("MAINTAIN_HANGAR_CHRISTMAS_DECORATIONS - Christmas decorations created.")
				#ENDIF
				SET_BIT(thisHangar.iBS, BS_HANGAR_CHRISTMAS_DECORATIONS_CREATED)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC RUN_MAIN_CLIENT_LOGIC()
	
	MAINTAIN_HANGAR_INTERIOR_MAP()
	MAINTAIN_HANGAR_PRODUCT_DATA()
	MAINTAIN_EXIT_VIA_GARAGE_DOOR()
	MAINTAIN_LAUNCHING_HANGER_CARMOD_SCRIPT()
	MAINTAIN_WANTED_LEVEL()
	MAINTAIN_INCOMPATIBLE_CLOTHING_RESTRICTIONS()
	MAINTAIN_IN_VEHICLE_CAMERA()
	SUPPRESS_SCREEN_DECORATIONS()
	
	/* Disabled for B*3873106
	MAINTAIN_POST_SETUP_CUTSCENE()
	*/
	
	MAINTAIN_SHOULD_START_INTRO_CUSTCENE()
	MAINTAIN_MP_RADIO_LOCAL()
	MAINTAIN_CLOSE_PLANE_DOORS()
	MAINTAIN_HANGAR_MP_TV_CLIENT()
	MAINTAIN_HANGAR_BED()
	MAINTAIN_CLEANUP_HANGAR_TIMECYCLE_MODIFIERS()
	
	MAINTAIN_AIRCRAFT_POSITIONS()
	
	MAINTAIN_HANGAR_CHRISTMAS_DECORATIONS()
	
	IF IS_HANGAR_STATE(HANGAR_STATE_LOADING)
	
		// Fix for B*3915240
		IF g_bScriptsSetSafeForCutscene
			g_bScriptsSetSafeForCutscene = FALSE
		ENDIF
		
		MAINTAIN_HANGAR_LOAD()

	ELIF IS_HANGAR_STATE(HANGAR_STATE_IDLE)
	
		IF SHOULD_LOCAL_PLAYER_BE_AUTOWARPED_INSIDE_THIS_SIMPLE_INTERIOR(thisHangar.eSimpleInteriorID)
			
			IF IS_BIT_SET(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_SPAWNING_IN_SIMPLE_INTERIOR)
				SET_BIT(g_SimpleInteriorData.iThirdBS, BS3_SIMPLE_INTERIOR_GLOBAL_DATA_RUN_SPAWN_ACTIVITY_AFTER_AUTOWARP)
			ENDIF
			
			IF NOT IS_SIMPLE_INTERIOR_GLOBAL_BIT_SET(SI_BS_CanFinishAutoWarp, thisHangar.eSimpleInteriorID)
				// Wait until we're done autowarping.
				EXIT
			ENDIF
		ENDIF

		FORCE_ZERO_MASS_FOR_ALL_PLAYERS_IN_HANGAR()
		INITIALISE_HANGAR_VEHICLE_MANAGEMENT_ENTRY()
		MAINTAIN_HANGAR_LAPTOP_SCREEN()
		MAINTAIN_DRIVING_IN_FLAG()
		CLEANUP_ENTRY_VEHICLES()
		MAINTAIN_HANGAR_ENTRANCE_HELP_TEXT()
		
		IF NOT g_bLaunchedMissionFrmLaptop
		AND (IS_PLAYER_HANGAR_FURNITURE_TRADITIONAL_PURCHASED(thisHangar.pOwner)
		OR IS_PLAYER_HANGAR_FURNITURE_MODERN_PURCHASED(thisHangar.pOwner))
			RUN_OFFICE_SIMPLE_SEAT_ARMCHAIR_ACTIVITY(thisHangar.activityTVSeatStruct, serverBD.serverTVSeatBD, playerBD[NATIVE_TO_INT(PLAYER_ID())].playerTVSeatBD)
		ENDIF
		
		IF SIMPLE_INTERIOR_IS_LOCAL_PLAYER_ENTERING_WITH_VEHICLE()
		AND serverBD.iEnteringVeh != -1
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PreventAutoShuffleToDriversSeat, FALSE)
				SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PreventAutoShuffleToTurretSeat, FALSE)
			ENDIF
			
			SIMPLE_INTERIOR_CLEAR_ENTERING_WITH_VEHICLE()
			SIMPLE_INTERIOR_CLEAR_ENTERING_WITH_VEH_AS_PASSENGER()
			g_iVehicleEntered = -1
		ENDIF
		
		IF IS_BIT_SET(thisHangar.iBS, BS_HANGAR_KILL_LOAD_SCENE_WHEN_INSIDE)
			CLEAR_BIT(thisHangar.iBS, BS_HANGAR_KILL_LOAD_SCENE_WHEN_INSIDE)
		ENDIF
		
		IF NOT g_bLaunchedMissionFrmLaptop
		AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
			RUN_OFFICE_SEAT_ACTIVITY(activitySeatStruct, serverBD.serverSeatBD)
		ENDIF
		
		MAINTAIN_VEHICLE_ZOOM()
		MAINTAIN_HANGAR_BEHAVIOURS()
//		MAINTAIN_CUSTOM_CAR_CREATION_HANGAR()
		MAINTAIN_VEHICLE_MANAGEMENT()
		MAINTAIN_HANGAR_PICKUPS()
		MAINTAIN_HANGAR_VEHICLE_LOCKS()
		MAINTAIN_CAN_DRIVE_OUT_OF_HANGAR()
		MAINTAIN_GET_OUT_OF_OWNERS_HANGAR_VEHICLE()
		MAINTAIN_HANGAR_BLIPS()
		MAINTAIN_HANGAR_DESK_SEAT()
		MAINTAIN_HANGAR_HELP()
		MAINTAIN_HANGAR_UPGRADES() 
		MAINTAIN_HELI_BLADES_SPEED()
		MAINTAIN_HANGAR_DISPLAY_SLOT_PLAYER_IS_IN()
		PRINT_HANGAR_AIRCRAFT_WORKSHOP_HELP()
		STOP_PLAYER_CONTROL_SHAKE()
		
		INT iBlockerBitSet = 0
		
		IF NOT DOES_PLAYER_OWN_HANGAR_LIVING_QUARTERS(thisHangar.pOwner)
			SET_BIT(iBlockerBitSet, 0)
		ENDIF
		
		CLIENT_MAINTAIN_MP_CCTV(playerBD[NATIVE_TO_INT(PLAYER_ID())].MPCCTVClient, MPCCTVLocal, thisHangar.eSimpleInteriorID, DEFAULT, DEFAULT, iBlockerBitSet)
		
		IF g_SimpleInteriorData.bForceCCTVCleanup
			CLEANUP_CCTV(FALSE)
			
			g_SimpleInteriorData.bForceCCTVCleanup = FALSE
		ENDIF
				
		IF IS_SCREEN_FADED_IN()
		AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
			IF NOT IS_BIT_SET(thisHangar.iBS, BS_HANGAR_CALLED_CLEAR_HELP)
				CLEAR_HELP()
				SET_BIT(thisHangar.iBS, BS_HANGAR_CALLED_CLEAR_HELP)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - Calling CLEAR_HELP")
			ENDIF
			
			IF NOT IS_BIT_SET(thisHangar.iBS, BS_HANGAR_PRODUCT_DELIVERED)
				IF FREEMODE_DELIVERY_IS_PLAYER_IN_POSSESSION_OF_ANY_DELIVERABLE(PLAYER_ID())
					IF thisHangar.pOwner = PLAYER_ID()
					OR thisHangar.pOwner = GB_GET_LOCAL_PLAYER_GANG_BOSS()
						FREEMODE_DELIVERY_START_DELIVERY(GET_SMUGGLER_DROPOFF_FROM_HANGAR_ID(thisHangar.eID), GB_GET_LOCAL_PLAYER_GANG_BOSS(), FALSE, FALSE, FALSE, TRUE)
						SET_BIT(thisHangar.iBS, BS_HANGAR_PRODUCT_DELIVERED)
					ENDIF
				ELSE
					SET_BIT(thisHangar.iBS, BS_HANGAR_PRODUCT_DELIVERED)
				ENDIF
			ENDIF
		ENDIF
		
		MAINTAIN_CAMERA_CONSTRAINTS()
		
	ELIF IS_HANGAR_STATE(HANGAR_STATE_PLAYING_GARAGE_EXIT)
		
		IF IS_SIMPLE_INTERIOR_ENTRY_ANIM_STAGE(SIMPLE_INTERIOR_ANIM_STAGE_FINISHED, thisHangar.garageExitAnim)
			DO_SCREEN_FADE_OUT(SIMPLE_INTERIOR_SCREEN_FADE_TIME)
			SET_HANGAR_STATE(HANGAR_STATE_FINISHING_GARAGE_EXIT)
		ENDIF
		
	ELIF IS_HANGAR_STATE(HANGAR_STATE_FINISHING_GARAGE_EXIT)
		
		IF IS_SCREEN_FADED_OUT()
			IF NOT IS_BIT_SET(thisHangar.garageExitAnim.iBS, BS_SIMPLE_INTERIOR_ANIM_ON_ENTER_FADE_OUT_FINISH_SOUND_PLAYED)
				IF NOT IS_STRING_NULL_OR_EMPTY(thisHangar.garageExitAnim.strOnEnterFadeOutFinishSoundName)
				AND NOT IS_STRING_NULL_OR_EMPTY(thisHangar.garageExitAnim.strOnEnterFadeOutFinishSoundSet)
					PLAY_SOUND_FRONTEND(-1, thisHangar.garageExitAnim.strOnEnterFadeOutFinishSoundName, thisHangar.garageExitAnim.strOnEnterFadeOutFinishSoundSet)
				ENDIF
				SET_BIT(thisHangar.garageExitAnim.iBS, BS_SIMPLE_INTERIOR_ANIM_ON_ENTER_FADE_OUT_FINISH_SOUND_PLAYED)
			ENDIF
			STOP_SIMPLE_INTERIOR_ENTRY_ANIM(thisHangar.garageExitAnim)
		ENDIF
	
	ELIF IS_HANGAR_STATE(HANGAR_STATE_PLAYING_INTRO_CUTSCENE)
		// Cutsene played the first time player views their hangar.
		MAINTAIN_HANGAR_LAPTOP_SCREEN()
		MAINTAIN_HANGAR_INTRO_CUTSCENE_STATE()
		
	ELIF IS_HANGAR_STATE(HANGAR_STATE_PLAYING_SETUP_CUTSCENE)
		// Cutscene played after completing hangar set up mission.
		MAINTAIN_HANGAR_LAPTOP_SCREEN()
	
		/* Disabled for B*3873106
		MAINTAIN_HANGAR_POST_SETUP_CUTSCENE_STATE()
		*/
	ENDIF
ENDPROC

PROC RUN_MAIN_SERVER_LOGIC()

	IF IS_HANGAR_SERVER_STATE(HANGAR_SERVER_STATE_LOADING)
		IF (GET_FRAME_COUNT() % 60) = 0
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - RUN_MAIN_SERVER_LOGIC - HANGAR_SERVER_STATE_LOADING")
		ENDIF
		
		IF MAINTAIN_HANGAR_VEHICLES()
			serverBD.bHangarVehiclesCreated = TRUE
			
			SET_HANGAR_SERVER_STATE(HANGAR_SERVER_STATE_IDLE)
		ELSE
			IF (GET_FRAME_COUNT() % 60) = 0
				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - RUN_MAIN_SERVER_LOGIC - Waiting for MAINTAIN_HANGAR_VEHICLES to return TRUE")
			ENDIF
		ENDIF
		
	ELIF IS_HANGAR_SERVER_STATE(HANGAR_SERVER_STATE_IDLE)
		IF (GET_FRAME_COUNT() % 120) = 0
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - RUN_MAIN_SERVER_LOGIC - HANGAR_SERVER_STATE_IDLE")
		ENDIF
		
		IF MAINTAIN_HANGAR_VEHICLES()
			serverBD.bHangarVehiclesCreated = TRUE
		ELSE
			serverBD.bHangarVehiclesCreated = FALSE
		ENDIF
		
		MAINTAIN_HANGAR_MP_TV_SERVER()
		SERVER_MAINTAIN_PROPERTY_ACTIVITIES(playerBD[serverLocalActControl.iSlowPlayerLoop].iActivityRequested, serverBD.activityControl, serverLocalActControl)
		SERVER_MAINTAIN_MP_RADIO(serverBD.MPRadioServer, playerBD[MPRadioLocal.iServerPlayerStagger].MPRadioClient, MPRadioLocal , InteriorPropStruct )
		
		IF IS_HANGAR_STATE(HANGAR_STATE_IDLE)		
			CREATE_BOARDROOM_CHAIRS(serverBD.serverSeatBD, activitySeatStruct,  thisHangar.eSimpleInteriorID)
		ENDIF
	ELSE
		IF (GET_FRAME_COUNT() % 120) = 0
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - RUN_MAIN_SERVER_LOGIC - Invalid servre state, how did we get here?")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL CHECK_HANGAR_VEHICLES_EXIST()
	INT i
	INT iCount
	
	REPEAT MAX_DISPLAYED_VEHS_IN_HANGAR i
		IF IS_NET_VEHICLE_DRIVEABLE(serverBD.customVehicleNetIDs[i])
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - CHECK_HANGAR_VEHICLES_EXIST - vehicle exists #", i)
			iCount++
		ELSE
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - CHECK_HANGAR_VEHICLES_EXIST - vehicle does not exist #", i)
		ENDIF
	ENDREPEAT
	
	IF iCount = serverBD.iVehicleCreated
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_OWNER_AS_HOST()
	IF thisHangar.pOwner = PLAYER_ID()
		IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
			IF NOT HAS_NET_TIMER_STARTED(st_SyncBDTimer)
				START_NET_TIMER(st_SyncBDTimer)
			ENDIF
			
			IF CHECK_HANGAR_VEHICLES_EXIST()
			OR (HAS_NET_TIMER_STARTED(st_SyncBDTimer)
			AND HAS_NET_TIMER_EXPIRED(st_SyncBDTimer, 3000))
				IF NOT HAS_NET_TIMER_STARTED(st_HostRequestTimer)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR: local player (OWNER) requesting to be host of script ")
					NETWORK_REQUEST_TO_BE_HOST_OF_THIS_SCRIPT()
					START_NET_TIMER(st_HostRequestTimer,TRUE)
				ELSE
					IF HAS_NET_TIMER_EXPIRED(st_HostRequestTimer,1000,TRUE)
						RESET_NET_TIMER(st_HostRequestTimer)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF SIMPLE_INTERIOR_IS_LOCAL_PLAYER_ENTERING_WITH_VEHICLE()
			AND serverBD.iEnteringVeh = -1
				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR: trying to assign serverBD.iEnteringVeh g_iVehicleEntered = ",g_iVehicleEntered)
				IF g_iVehicleEntered >= 0
					INT iDisplaySlot
					MPSV_GET_DISPLAY_SLOT_FROM_SAVE_SLOT(g_iVehicleEntered, iDisplaySlot)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR: iDisplaySlot = ",iDisplaySlot)
					IF iDisplaySlot > 0
						serverBD.iEnteringVeh = iDisplaySlot - DISPLAY_SLOT_START_HANGAR
						IF serverBD.iEnteringVeh > 0 
							CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR: setting serverBD.iEnteringVeh = ",serverBD.iEnteringVeh)
						ELSE
							serverBD.iEnteringVeh = 0 
							CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR: INVALID enter setting serverBD.iEnteringVeh = ",serverBD.iEnteringVeh)
						ENDIF
					ENDIF
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR: player is entering in a vehicle but g_iVehicleEntered = ",g_iVehicleEntered)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC




//╒═════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════════╡ MAIN LOOP ╞════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

SCRIPT  (SIMPLE_INTERIOR_CHILD_SCRIPT_DATA scriptData) 
	
	
	IF NETWORK_IS_GAME_IN_PROGRESS() 
		SCRIPT_INITIALISE(scriptData)	
	ELSE
		SCRIPT_CLEANUP()
	ENDIF
	
	// Main loop
	WHILE TRUE

		// One wait to rule them all. This can be the ONLY wait from here on in.....
		MP_LOOP_WAIT_ZERO()
		
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE = TRUE")
			SCRIPT_CLEANUP()
		ENDIF
		
		IF SHOULD_THIS_SIMPLE_INTERIOR_CHILD_THREAD_TERMINATE(scriptData.iScriptInstance, scriptData.eSimpleInteriorID)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_HANGAR - SHOULD_THIS_SIMPLE_INTERIOR_CHILD_THREAD_TERMINATE = TRUE")
			SCRIPT_CLEANUP()
		ENDIF
		
		bGetOutOfWayFlagClearedThisFrame = FALSE
		
		IF MAINTAIN_GET_OUT_OF_THE_WAY_HANGAR()
			playerBD[NATIVE_TO_INT(PLAYER_ID())].sGetOutOfTheWay.iPlayerBitSet = 0
			sGetOutOfWayVars.iProgressBitSet = 0
		ENDIF
		
		SERVER_CLEANUP_PLAYERS_ARE_IN_THE_WAY_HANGAR_FLAGS()
		
		MAINTAIN_OWNER_AS_HOST()
		
		IF IS_HANGAR_SERVER_STATE(HANGAR_SERVER_STATE_IDLE)
			RUN_MAIN_CLIENT_LOGIC()
		ENDIF

		MAINTAIN_INTERIOR_BED_SPAWN_ACTIVITIES()
		
		// -----------------------------------
		// Process server game logic		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			RUN_MAIN_SERVER_LOGIC()
		ENDIF
		
		#IF IS_DEBUG_BUILD
		UPDATE_DEBUG_WIDGETS()
		#ENDIF
		
	ENDWHILE
	

ENDSCRIPT
