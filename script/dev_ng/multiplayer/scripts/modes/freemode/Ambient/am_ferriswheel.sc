// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	am_ferriswheel.sc
//		AUTHOR			:	Martin McMillan
//		DESCRIPTION		:   Ferris Wheel ride for multiplayer
//
// *****************************************************************************************
// *****************************************************************************************


//----------------------
//	INCLUDES
//----------------------
USING "globals.sch"
USING "rage_builtins.sch"
USING "rgeneral_include.sch"
USING "RC_Helper_Functions.sch"
USING "RC_Asset_Public.sch"
USING "RC_Camera_Public.sch"
USING "commands_brains.sch"
USING "net_fps_cam.sch"
USING "net_include.sch"
USING "net_wait_zero.sch"
USING "net_mission.sch"
USING "mp_scaleform_functions.sch"
USING "net_common_functions.sch"
USING "net_transition_sessions.sch"
USING "freemode_header.sch"
#IF IS_DEBUG_BUILD
USING "shared_debug.sch"
#ENDIF

//----------------------
//	ENUM
//----------------------

//----------------------
//	CONSTANTS
//----------------------
CONST_INT 	MAX_OCCUPIED_WHEEL_CARS 8
CONST_INT 	MAX_WHEEL_CARS 			16
CONST_INT 	MAX_WHEEL_CAR_OCCUPANTS 2
CONST_FLOAT WHEEL_CAR_RADIUS 		15.3
CONST_INT 	WHEEL_CAR_LOD_DIST		1000
CONST_FLOAT	WHEEL_CAR_DISTANCE		22.5
CONST_INT	RIDE_ID					2
CONST_INT	PLAYER_WHEEL_CAR		1
CONST_INT 	BOARDING_TIME			30000 //30 Seconds

//----------------------
//	Local Bitset
//----------------------

//INT iLocalBitSet


//----------------------
//	STRUCT
//----------------------
STRUCT WHEEL_CAR
	BOOL bOccupied[MAX_WHEEL_CAR_OCCUPANTS] 
	INT iNumPlayers
	INT iSyncSceneID
	INT iIndex
	FLOAT fPosition
	
	PED_INDEX pedID[MAX_WHEEL_CAR_OCCUPANTS]
ENDSTRUCT

STRUCT CAR_REQUEST
	INT iSelectedCarIndex = -1
	PLAYER_INDEX requestingPlayer
ENDSTRUCT

//----------------------
//	VARIABLES
//----------------------
ASSET_REQUESTER asRequest1, asRequest2

MODEL_NAMES wheelModel = PROP_LD_FERRIS_WHEEL
MODEL_NAMES wheelCarModel = PROP_FERRIS_CAR_01
STRING sWheelCarAnimDict = "MISSFINALE_C2IG_5"

OBJECT_INDEX wheelCarObject[MAX_WHEEL_CARS]

OBJECT_INDEX wheelObjectID

FLOAT fWheelRotationRate = 2.0

VECTOR vWheelCenter = <<-1663.970, -1126.700, 30.700>>
VECTOR vPedWheelCarAttachOffset = <<0.5, 0.5, -1.94>> // radius = 0.5, height below = -1.94
VECTOR vCarPosition
FLOAT fWheelGroundZ = 13.0
#IF IS_DEBUG_BUILD
INT iRevolutions = 0
FLOAT fPlayerWheelCarAngle = 260.
INT iPlayerWheelCarAnim = 0
#ENDIF


INT iGeneratorSoundID = -1
INT iRattlingSoundID = -1
INT iSlowSqueakSoundID = -1
INT iSqueakSoundID = -1
//INT iCameraTimer
BOOL bStandardCamera = TRUE

#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID widgetGroup
	BOOL bQuit = FALSE
	BOOL bShowDebug = FALSE
	BOOL bRespotPlayer = FALSE
#ENDIF

CAMERA_INDEX initialCamera

FIRST_PERSON_CAM_STRUCT fpsCam 
CAMERA_STRUCT outerCamera
BASIC_CAMERA fpsTestCam
CAMERA_INDEX cameraID
BOOL bFPSMode

//BOOL bSkipped = FALSE
//INT iOccupiedCount = 0

INT iStaggeredParticipant
//BOOL bDoEndStaggeredLoopChecks

TIME_DATATYPE previousNetworkTime

STRING sFW_AnimDict = "anim@mp_ferris_wheel"
//STRING sFW_AnimEnter = "enter_player_"
//STRING sFW_AnimIdle = "idle_a_player_"
//STRING sFW_AnimExit = "exit_player_"

TEXT_LABEL_23 sFW_AnimPlayer
TEXT_LABEL_23 sFW_AnimPlayer2
TEXT_LABEL_23 sFW_AnimPlayer3

INT iLocalBitset 
CONST_INT HELP_SHOWN			0
CONST_INT WAIT_SHOWN			1
CONST_INT FULL_SHOWN			2
CONST_INT STORE_ACTIVE			3
CONST_INT BUTTONS_REQUESTED		4
CONST_INT START_MOVE_CHECK		5
CONST_INT ACTIVATED_PASSIVE		6
CONST_INT SKIP_SOUND_REQUEST	7
CONST_INT SOUNDS_LOADED     	8

SCRIPT_TIMER iBoardingTimer
CONST_INT ANIMATION_CHECK_TIME	1000
INT iBoardingTime

SCRIPT_TIMER timeNotMoved
SCRIPT_TIMER soundRequestTimer

ENUM FERRIS_WHEEL_ENTER_ANIM_STAGE
	ENTER_WALK_TO_POINT	= 0,
	ENTER_PLAY_ANIM,
	ENTER_ANIM_PLAYING,
	ENTER_IN_CAR
ENDENUM

//ENUM FERRIS_WHEEL_EXIT_ANIM_STAGE
//	EXIT_PLAY_ANIM	= 0,
//	EXIT_OUT_OF_CAR
//ENDENUM

INT iEnterScene
INT iExitScene

//SEQUENCE_INDEX SequenceIndex

FERRIS_WHEEL_ENTER_ANIM_STAGE eEnterAnimStage = ENTER_WALK_TO_POINT
//FERRIS_WHEEL_EXIT_ANIM_STAGE eExitAnimStage = EXIT_PLAY_ANIM

BOOL bRefreshButtons = TRUE

//Instructional button variables
SCALEFORM_INDEX buttonMovie
SCALEFORM_INSTRUCTIONAL_BUTTONS instructionalButtons
SPRITE_PLACEMENT aSprite

SCRIPT_TIMER iCleanupDelay

//----------------------
//	GAME STATE
//----------------------

CONST_INT	GAME_STATE_AWAIT_ACTIVATION		0 
CONST_INT	GAME_STATE_INIT					1
CONST_INT	GAME_STATE_READY				2
CONST_INT	GAME_STATE_CLEANUP				3

ENUM FERRIS_WHEEL_RUN_STAGE
	FERRIS_WHEEL_RUNNING = 0,
	FERRIS_WHEEL_BOARDING
ENDENUM



//----------------------
//	BROADCAST VARIABLES
//----------------------

// Everyone can read this data, only the server can update it
STRUCT ServerBroadcastData

	FERRIS_WHEEL_RUN_STAGE eFerrisWheelStage = FERRIS_WHEEL_RUNNING

	INT iServerGameState = GAME_STATE_AWAIT_ACTIVATION
	
	INT iServerBitSet
	
	INT iPlayerBitSet[NUM_NETWORK_PLAYERS]
	
	WHEEL_CAR wheelCar[MAX_WHEEL_CARS] 
	
	CAR_REQUEST requests[NUM_NETWORK_PLAYERS]
	INT iCurrentRequest
	
	BOOL bReadyToRun
ENDSTRUCT

ServerBroadcastData serverBD

//iServerBitSet
CONST_INT MOVING_TO_CAR					0
CONST_INT BLOCK_NON_REQUESTING_PLAYERS	1
CONST_INT WAITING_FOR_REQUESTED			2
CONST_INT BLOCK_ENTRY					3
CONST_INT DONT_SHOW_EXIT				4

//iPlayerBitSet
CONST_INT STOP_REQUEST_PROCESSED		0
CONST_INT WHEEL_STOP_PROCESSED			1

// The client broadcast data.
// Everyone can read this data, only the local player only the server can update it
STRUCT PlayerBroadcastData
	
	INT iGameState = GAME_STATE_AWAIT_ACTIVATION
	
	FERRIS_WHEEL_RUN_STAGE eFerrisWheelStage = FERRIS_WHEEL_RUNNING
	
	int iClientBitSet
	
	INT iCarIndex = -1
	
	INT iCarOrder
	
ENDSTRUCT

PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]

CONST_INT PLAYER_IN_CAR				0
CONST_INT PLAYER_JUST_JOINED		1
CONST_INT CLIENT_DO_SYNC			2
CONST_INT STOP_REQUESTED			3
CONST_INT WHEEL_STOPPED				4
CONST_INT PLAYER_JUST_ENTERED_WHEEL	5
CONST_INT PLAYER_JUST_LEFT_WHEEL	6
CONST_INT SETUP_BOTTOM_CAR			7
CONST_INT PLAYER_PAID				8
CONST_INT PUTTING_PLAYER_IN_CAR		9
CONST_INT REMOVING_PLAYER_FROM_CAR	10
CONST_INT INTERACT_REQUEST_SENT		11

//----------------------
//	FUNCTIONS
//----------------------


#IF IS_DEBUG_BUILD
//----------------------
//	DBG FUNCTIONS
//----------------------
		

/// PURPOSE:
/// 	Initializes Debug Widgets
PROC SETUP_DEBUG_WIDGETS()
	IF NOT GET_COMMANDLINE_PARAM_EXISTS("sc_AutoProfileMissionController")
		SET_PROFILING_OF_THIS_SCRIPT(TRUE)
	ENDIF
	widgetGroup = START_WIDGET_GROUP("Fairground - Bigwheel")	
		ADD_WIDGET_BOOL("Quit Script", bQuit)
		ADD_WIDGET_BOOL("Show Debug", bShowDebug)
		ADD_WIDGET_BOOL("Use Standard Cam", bStandardCamera)
		ADD_WIDGET_VECTOR_SLIDER("Wheel Pos", vWheelCenter, -2500.0, 2500, 0.1)
		ADD_WIDGET_FLOAT_SLIDER("Rotate Rate", fWheelRotationRate, 0, 180, 1)
		ADD_WIDGET_FLOAT_SLIDER("Rotation", g_fWheelRotation, 0, 360, 1)
		ADD_WIDGET_INT_READ_ONLY("Revs", iRevolutions)
		
		ADD_WIDGET_BOOL("Respot Player", bRespotPlayer)
		ADD_WIDGET_FLOAT_SLIDER("Player Wheel Angle", fPlayerWheelCarAngle, -360, 360, 1)
		ADD_WIDGET_INT_SLIDER("Player Wheel Anim", iPlayerWheelCarAnim, 0, 3, 1)
		
		START_WIDGET_GROUP("FPS Camera")	
			ADD_WIDGET_INT_SLIDER("Look X Limit", fpsCam.iLookXLimit, -360, 360, 1)
			ADD_WIDGET_INT_SLIDER("Look Y Limit", fpsCam.iLookYLimit, -360, 360, 1)
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
ENDPROC

/// PURPOSE:
///		Removes Debug Widgets
PROC CLEANUP_DEBUG_WIDGETS()
	IF DOES_WIDGET_GROUP_EXIST(widgetGroup)
		DELETE_WIDGET_GROUP(widgetGroup)
	ENDIF
ENDPROC 


#ENDIF

PROC CLEANUP_AUDIO()
	IF IS_BIT_SET(iLocalBitset, SOUNDS_LOADED)
		SAFE_STOP_AND_RELEASE_SOUND_ID(iGeneratorSoundID)
		SAFE_STOP_AND_RELEASE_SOUND_ID(iSlowSqueakSoundID)
		SAFE_STOP_AND_RELEASE_SOUND_ID(iSqueakSoundID)
		SAFE_STOP_AND_RELEASE_SOUND_ID(iRattlingSoundID)
		
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("SCRIPT\\FERRIS_WHALE_01")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("SCRIPT\\FERRIS_WHALE_02")
		
		CLEAR_BIT(iLocalBitset, SOUNDS_LOADED)
		
		CDEBUG1LN(DEBUG_MINIGAME, "am_ferriswheel - CLEANUP_AUDIO - audio banks released")
	ENDIF
	CLEAR_BIT(iLocalBitset, SKIP_SOUND_REQUEST)
ENDPROC

/// PURPOSE:
///    Switch to First Person
PROC SWITCH_TO_FIRST_PERSON_MODE()
	VECTOR v
	IF IS_ENTITY_OK(PLAYER_PED_ID())
		SHUTDOWN_CAMERA(cameraID)
		//
		
		fpsCam.iLookXLimit = 160
		fpsCam.iLookYLimit = 20
		
		IF (bStandardCamera)
			v = GET_PED_BONE_COORDS(PLAYER_PED_ID(), BONETAG_HEAD, <<0, -0.25, 0>>)
			INIT_FIRST_PERSON_CAMERA(fpsCam, v, GET_ENTITY_ROTATION(PLAYER_PED_ID()), 50.0, fpsCam.iLookXLimit, fpsCam.iLookYLimit)
			ATTACH_CAM_TO_PED_BONE(fpsCam.theCam, PLAYER_PED_ID(), BONETAG_HEAD, <<0, -0.25, 0>>)		
		ELSE
			SETUP_BASIC_FPS_CAMERA(fpsTestCam, TRUE)
			ENABLE_BASIC_CAMERA(fpsTestCam)
		ENDIF
		
		IF IS_AUDIO_SCENE_ACTIVE("FAIRGROUND_RIDES_FERRIS_WHALE_ALTERNATIVE_VIEW")
			STOP_AUDIO_SCENE("FAIRGROUND_RIDES_FERRIS_WHALE_ALTERNATIVE_VIEW")
		ENDIF
		START_AUDIO_SCENE("FAIRGROUND_RIDES_FERRIS_WHALE")
	ENDIF
	
	bFPSMode = TRUE
ENDPROC

/// PURPOSE:
///    Switch to External View
PROC SWITCH_TO_EXTERNAL_MODE()
	IF IS_ENTITY_OK(PLAYER_PED_ID())
		DESTROY_BASIC_CAMERA(fpsTestCam)
		CLEAR_FIRST_PERSON_CAMERA(fpsCam)
		ENABLE_CAMERA_STRUCT(outerCamera, cameraID)
		bFPSMode = FALSE
		
		
			println("am_ferriswheel - SWITCHED TO EXTERNAL MODE")
		
		
		IF IS_AUDIO_SCENE_ACTIVE("FAIRGROUND_RIDES_FERRIS_WHALE")
			STOP_AUDIO_SCENE("FAIRGROUND_RIDES_FERRIS_WHALE")
		ENDIF
		
		START_AUDIO_SCENE("FAIRGROUND_RIDES_FERRIS_WHALE_ALTERNATIVE_VIEW")
	ENDIF
ENDPROC

//----------------------
//	WHEEL
//----------------------

/// PURPOSE:
///    Calculate Position Of Wheel Car
/// PARAMS:
///    iGondola - Gondola Index
/// RETURNS:
///    Position of car
FUNC VECTOR CALCULATE_WHEEL_CAR_POSITION(INT iGondola)
	FLOAT fAngle = (360.0 / TO_FLOAT(MAX_WHEEL_CARS)) * TO_FLOAT(iGondola)
	RETURN GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(wheelObjectID, <<0, WHEEL_CAR_RADIUS * SIN(fAngle), -WHEEL_CAR_RADIUS * COS(fAngle)>>)
ENDFUNC

/// PURPOSE:
///    Gets the offset from a wheel car that a ped needs to be standing on the end
/// PARAMS:
///    angle - angle around the wheel car
/// RETURNS:
///    Vector
FUNC VECTOR CALCULATE_PED_WHEEL_CAR_OFFSET(FLOAT angle)
	VECTOR v 
	
	v.x = SIN(angle) * vPedWheelCarAttachOffset.x 
	v.y = COS(angle) * vPedWheelCarAttachOffset.y 
	v.z = vPedWheelCarAttachOffset.z
	
	RETURN v 
ENDFUNC


// get bystander variant
FUNC INT GET_WHEEL_BYSTANDER_VARIANT(INT iGondola, INT iIndex)
	SWITCH iGondola
		CASE 0
			IF iIndex=0
				RETURN 0
			ELSE
				RETURN 1
			ENDIF
		BREAK
		CASE 1
			IF iIndex=0
				RETURN 0
			ELSE
				RETURN 1
			ENDIF
		BREAK
		CASE 2
			IF iIndex=0
				RETURN 2
			ELSE
				RETURN 3
			ENDIF
		BREAK
		CASE 3
			IF iIndex=0
				RETURN 2
			ELSE
				RETURN 3
			ENDIF
		BREAK
		CASE 4
			IF iIndex=0
				RETURN 2
			ELSE
				RETURN 3
			ENDIF
		BREAK
		CASE 5
			IF iIndex=0
				RETURN 0
			ELSE
				RETURN 1
			ENDIF
		BREAK
		CASE 6
			IF iIndex=0
				RETURN 0
			ELSE
				RETURN 1
			ENDIF
		BREAK
		CASE 7
			IF iIndex=0
				RETURN 0
			ELSE
				RETURN 1
			ENDIF
		BREAK
		CASE 8
			IF iIndex=0
				RETURN 2
			ELSE
				RETURN 3
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Gets a random wheel car anim
/// RETURNS:
///    String Name
FUNC STRING GET_WHEEL_CAR_PED_ANIM(INT iVariant)
	SWITCH iVariant
		CASE 0
			RETURN "Stand_Idle_1_PEDA"
		BREAK
		CASE 1
			RETURN "Stand_Idle_1_PEDB"
		BREAK
		CASE 2
			RETURN "Stand_Idle_2_PEDA"
		BREAK
		CASE 3
			RETURN "Stand_Idle_2_PEDB"
		BREAK
	ENDSWITCH
	
	RETURN NULL_STRING()
ENDFUNC

FUNC FLOAT GET_ANIM_ROT_FOR_OCCUPIED_GONDOLA(INT iGondola)
	SWITCH iGondola
		CASE 0
			RETURN 180.0
		BREAK
		CASE 1
			RETURN 340.0
		BREAK
		CASE 2
			RETURN 90.0
		BREAK
		CASE 3
			RETURN 250.0
		BREAK
		CASE 4
			RETURN 300.0
		BREAK
		CASE 5
			RETURN 160.0
		BREAK
		CASE 6
			RETURN 180.0
		BREAK
		CASE 7
			RETURN 340.0
		BREAK
		CASE 8
			RETURN 90.0
		BREAK
	ENDSWITCH
	
	RETURN 0.0
ENDFUNC

/// PURPOSE:
///    Updates the wheel car
/// PARAMS:
///    w - wheel car reference
PROC UPDATE_WHEEL_CAR(WHEEL_CAR &w)
	VECTOR v = CALCULATE_WHEEL_CAR_POSITION(w.iIndex)
	
	IF IS_ENTITY_ALIVE(wheelCarObject[w.iIndex])
		SET_ENTITY_COORDS(wheelCarObject[w.iIndex], v)
		//SET_ENTITY_HEADING(w.objectID, g_fWheelRotation + ((w.iIndex % 2) * 180))
		SET_ENTITY_LOD_DIST(wheelCarObject[w.iIndex], WHEEL_CAR_LOD_DIST)
		FREEZE_ENTITY_POSITION(wheelCarObject[w.iIndex], TRUE)
	ENDIF
	
		#IF IS_DEBUG_BUILD
		FLOAT sx, sy
		IF (bShowDebug)
			IF GET_SCREEN_COORD_FROM_WORLD_COORD(v, sx, sy)
				SET_TEXT_SCALE(1, 1)
			
				
				DISPLAY_TEXT_WITH_NUMBER(sx, sy, "NUMBER", w.iIndex)
			ENDIF
		ENDIF
		#ENDIF
ENDPROC

/// PURPOSE:
///    Cleanups up the wheel car
/// PARAMS:
///    w - car refernece
PROC CLEANUP_WHEEL_CAR(WHEEL_CAR &w)

	IF (wheelCarObject[w.iIndex] = NULL)
		EXIT 
	ENDIF
	
	SAFE_DELETE_OBJECT(wheelCarObject[w.iIndex])
	
	wheelCarObject[w.iIndex] = NULL
ENDPROC

PROC ADJUST_FOR_BLOCKING_OBJECTS()
	IF IS_ANY_VEHICLE_NEAR_POINT(<<-1666.7106, -1127.5184, 12.6973>>,3.0)
	AND (IS_ENTITY_AT_COORD(PLAYER_PED_ID(),<<-1666.7106, -1127.5184, 12.6973>>,<<0.5,0.5,1.0>>)
	OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(),<<-1666.3831, -1126.2861, 12.6973>>,<<0.5,0.5,1.0>>))
		IF NOT IS_BIT_SET(iLocalBitset,START_MOVE_CHECK)
			IF (IS_PED_WALKING(PLAYER_PED_ID())
			OR IS_PED_RUNNING(PLAYER_PED_ID())
			OR IS_PED_SPRINTING(PLAYER_PED_ID())
			OR IS_PED_STRAFING(PLAYER_PED_ID()))
				RESET_NET_TIMER(timeNotMoved)
				SET_BIT(iLocalBitset,START_MOVE_CHECK)
			ENDIF
		ELSE
			IF HAS_NET_TIMER_EXPIRED(timeNotMoved,2000)
				IF (IS_PED_WALKING(PLAYER_PED_ID())
				OR IS_PED_RUNNING(PLAYER_PED_ID())
				OR IS_PED_SPRINTING(PLAYER_PED_ID())
				OR IS_PED_STRAFING(PLAYER_PED_ID()))
					VECTOR vCurrentPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
					SET_ENTITY_COORDS(PLAYER_PED_ID(),<<vCurrentPosition.x,vCurrentPosition.y,vCurrentPosition.z + 2.0>>)
					CLEAR_BIT(iLocalBitset,START_MOVE_CHECK)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SET_FERRISWHEEL_PASSIVE_MODE(BOOL bOn)

	IF bOn
		IF NOT IS_BIT_SET(iLocalBitset,ACTIVATED_PASSIVE)
			SET_TEMP_PASSIVE_MODE()
			SET_BIT(iLocalBitset,ACTIVATED_PASSIVE)
			println("am_ferriswheel - SET_FERRISWHEEL_PASSIVE_MODE - TRUE")
		ENDIF
	ELSE
		IF IS_BIT_SET(iLocalBitset,ACTIVATED_PASSIVE)
			CLEANUP_TEMP_PASSIVE_MODE()
			CLEAR_BIT(iLocalBitset,ACTIVATED_PASSIVE)
			println("am_ferriswheel - SET_FERRISWHEEL_PASSIVE_MODE - FALSE")
		ELSE
			println("am_ferriswheel - SET_FERRISWHEEL_PASSIVE_MODE - not cleaning up temp passive mode")
		ENDIF
	ENDIF

ENDPROC

PROC PUT_PLAYER_IN_WHEEL_CAR()
	
	println("am_ferriswheel - PUT_PLAYER_IN_WHEEL_CAR")
	//VECTOR vOffset
	VECTOR vAnimPosition =  <<-1661.9143, -1126.8416, 12.6973>>

	VECTOR vCarOffset
	INT iLocalSceneID
	//VECTOR vInCarPosition
	
	IF NOT IS_BIT_SET(iLocalBitset, SKIP_SOUND_REQUEST)
		IF NOT REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\FERRIS_WHALE_01")
		OR NOT REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\FERRIS_WHALE_02")
			CDEBUG1LN(DEBUG_MINIGAME, "am_ferriswheel - PUT_PLAYER_IN_WHEEL_CAR - Loading audio banks...")
			
			IF NOT HAS_NET_TIMER_STARTED(soundRequestTimer)
				START_NET_TIMER(soundRequestTimer,TRUE)
				CDEBUG1LN(DEBUG_MINIGAME, "am_ferriswheel - soundRequestTimer timer started")
			ELSE
				IF HAS_NET_TIMER_EXPIRED_READ_ONLY(soundRequestTimer, 4000, TRUE)
					CDEBUG1LN(DEBUG_MINIGAME, "am_ferriswheel - PUT_PLAYER_IN_WHEEL_CAR - soundRequestTimer expired, ferris wheel won't play sounds")
					SET_BIT(iLocalBitset, SKIP_SOUND_REQUEST)
				ENDIF
			ENDIF
			
			EXIT
		ELSE
			SET_BIT(iLocalBitset, SOUNDS_LOADED)
			SET_BIT(iLocalBitset, SKIP_SOUND_REQUEST)
			CDEBUG1LN(DEBUG_MINIGAME, "am_ferriswheel - PUT_PLAYER_IN_WHEEL_CAR - sounds loaded")
		ENDIF
	ENDIF
	
	SWITCH (eEnterAnimStage)
	
		CASE ENTER_WALK_TO_POINT
			IF IS_NET_PLAYER_OK(PLAYER_ID(),TRUE,TRUE)
			AND NOT g_MultiplayerSettings.g_bSuicide
				println("am_ferriswheel - ENTER_WALK_TO_POINT")
				
				playerBD[PARTICIPANT_ID_TO_INT()].iCarIndex = serverBD.requests[serverBD.iCurrentRequest].iSelectedCarIndex
				vCarPosition = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(wheelCarObject[playerBD[PARTICIPANT_ID_TO_INT()].iCarIndex],<<0.0,0.0,0.0>>)
				
				IF NOT(serverBD.wheelCar[serverBD.requests[serverBD.iCurrentRequest].iSelectedCarIndex].bOccupied[0])
					playerBD[PARTICIPANT_ID_TO_INT()].iCarOrder = 0
				ELIF NOT(serverBD.wheelCar[serverBD.requests[serverBD.iCurrentRequest].iSelectedCarIndex].bOccupied[1])
					playerBD[PARTICIPANT_ID_TO_INT()].iCarOrder = 1
				ELSE
					println("am_ferriswheel - ERROR - BOTH CARS FULL")
				ENDIF
				
				sFW_AnimPlayer = "enter_player_"
				sFW_AnimPlayer2 = "idle_a_player_"
				IF playerBD[PARTICIPANT_ID_TO_INT()].iCarOrder = 0
					sFW_AnimPlayer += "one"
					sFW_AnimPlayer2+= "one"
				ELSE
					sFW_AnimPlayer += "two"
					sFW_AnimPlayer2+= "two"
				ENDIF
				
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_CLEAR_TASKS  | NSPC_LEAVE_CAMERA_CONTROL_ON )
				SET_FERRISWHEEL_PASSIVE_MODE(TRUE)
				
				TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(),vAnimPosition, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP, 98.6981 , 0.2)
				
				//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].g_bUsingFerrisWheel = TRUE
				CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bUsingFerrisWheel)
				PRINTLN("am_ferriswheel - g_bUsingFerrisWheel = TRUE")
				NETWORK_DISABLE_LEAVE_REMOTE_PED_BEHIND(TRUE)
				
				SET_TRANSITION_SESSIONS_CLEAN_UP_ON_CALL_NOW_IF_NEEDED()
				
				eEnterAnimStage = ENTER_PLAY_ANIM
			ELSE
				CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PUTTING_PLAYER_IN_CAR)
				BROADCAST_CLEAR_INTERACT_CAR(ALL_PLAYERS_ON_SCRIPT(TRUE),playerBD[PARTICIPANT_ID_TO_INT()].iCarOrder,TRUE)
			ENDIF
		BREAK
		CASE ENTER_PLAY_ANIM
			IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(),SCRIPT_TASK_GO_STRAIGHT_TO_COORD) != WAITING_TO_START_TASK
			AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(),SCRIPT_TASK_GO_STRAIGHT_TO_COORD) != PERFORMING_TASK
				println("am_ferriswheel - ENTER_PLAY_ANIM")
				iEnterScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vCarPosition,<<0.0,0.0,0.0>>,DEFAULT,TRUE)
				NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(),iEnterScene,sFW_AnimDict,sFW_AnimPlayer,NORMAL_BLEND_IN,NORMAL_BLEND_OUT,SYNCED_SCENE_NET_DISREGARD_ATTACHMENT_CHECKS)
				
				NETWORK_START_SYNCHRONISED_SCENE(iEnterScene)
				
				eEnterAnimStage = ENTER_ANIM_PLAYING
			ENDIF
		BREAK
		CASE ENTER_ANIM_PLAYING
			println("am_ferriswheel - ENTER_ANIM_PLAYING")
			
			iLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iEnterScene)
			IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalSceneID) > 0.99
				iEnterScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vCarPosition,<<0.0,0.0,0.0>>,DEFAULT,DEFAULT,TRUE)
				NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(),iEnterScene,sFW_AnimDict,sFW_AnimPlayer2,NORMAL_BLEND_IN,NORMAL_BLEND_OUT,SYNCED_SCENE_NET_DISREGARD_ATTACHMENT_CHECKS)
				NETWORK_START_SYNCHRONISED_SCENE(iEnterScene)
				
				eEnterAnimStage = ENTER_IN_CAR
			ENDIF
			
			//TASK_PLAY_ANIM(PLAYER_PED_ID(),sFW_AnimDict,sFW_AnimEnter,NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_TURN_OFF_COLLISION | AF_LOOPING,0,FALSE,AIK_DISABLE_LEG_IK )
		BREAK
		CASE ENTER_IN_CAR	
			IF IS_ENTITY_OK(PLAYER_PED_ID())		
				SET_ENTITY_LOD_DIST(PLAYER_PED_ID(), WHEEL_CAR_LOD_DIST)
			ENDIF
			vCarOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(wheelCarObject[playerBD[PARTICIPANT_ID_TO_INT()].iCarIndex],GET_ENTITY_COORDS(PLAYER_PED_ID()))
			ATTACH_ENTITY_TO_ENTITY(PLAYER_PED_ID(), wheelCarObject[playerBD[PARTICIPANT_ID_TO_INT()].iCarIndex], 0, vCarOffset, <<0.0, 0.0, GET_ENTITY_HEADING(PLAYER_PED_ID())>>)
			NETWORK_ALLOW_REMOTE_ATTACHMENT_MODIFICATION(PLAYER_PED_ID(),TRUE)
			//vOffset = CALCULATE_PED_WHEEL_CAR_OFFSET(angle)
			BROADCAST_ATTACH_REMOTE_PLAYER_TO_CAR(ALL_PLAYERS_ON_SCRIPT(FALSE),playerBD[PARTICIPANT_ID_TO_INT()].iCarIndex,vCarOffset,GET_ENTITY_HEADING(PLAYER_PED_ID()))
			
			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PLAYER_JUST_ENTERED_WHEEL)
			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PLAYER_IN_CAR)
			
			CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PUTTING_PLAYER_IN_CAR)
			
			
			eEnterAnimStage = ENTER_WALK_TO_POINT
			println("am_ferriswheel - SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PLAYER_IN_CAR)")
					
			BROADCAST_CLEAR_INTERACT_CAR(ALL_PLAYERS_ON_SCRIPT(TRUE),playerBD[PARTICIPANT_ID_TO_INT()].iCarOrder,TRUE)
		BREAK
	ENDSWITCH
	
ENDPROC

PROC REMOVE_PLAYER_FROM_WHEEL_CAR()
	//INT iLocalSceneID
	

		sFW_AnimPlayer3 = "exit_player_"

		IF playerBD[PARTICIPANT_ID_TO_INT()].iCarOrder = 0
			sFW_AnimPlayer3 += "one"
		ELSE
			sFW_AnimPlayer3 += "two"
		ENDIF

		iExitScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vCarPosition,<<0.0,0.0,0.0>>)
		println("am_ferriswheel - PLAY ANIM 5: ",sFW_AnimPlayer3)
		NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(),iExitScene,sFW_AnimDict,sFW_AnimPlayer3,NORMAL_BLEND_IN,NORMAL_BLEND_OUT,SYNCED_SCENE_NET_DISREGARD_ATTACHMENT_CHECKS)
		
		NETWORK_START_SYNCHRONISED_SCENE(iExitScene)
		//TASK_PLAY_ANIM(PLAYER_PED_ID(),sFW_AnimDict,sFW_AnimPlayer3,NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_TURN_OFF_COLLISION,0,DEFAULT,AIK_DISABLE_LEG_IK )
		CLEAR_HELP()
		
		//eExitAnimStage = EXIT_OUT_OF_CAR

		println("am_ferriswheel - CHECK ANIM: ",sFW_AnimPlayer3)

		NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		SET_FERRISWHEEL_PASSIVE_MODE(FALSE)	
			
		IF IS_ENTITY_OK(PLAYER_PED_ID())
			IF IS_ENTITY_ATTACHED(PLAYER_PED_ID())
				DETACH_ENTITY(PLAYER_PED_ID())
			ENDIF
		ENDIF	
			
		BROADCAST_DETACH_REMOTE_PLAYER_FROM_CAR(ALL_PLAYERS_ON_SCRIPT(FALSE))
		//NETWORK_ALLOW_REMOTE_ATTACHMENT_MODIFICATION(PLAYER_PED_ID(),FALSE)
		
		IF IS_AUDIO_SCENE_ACTIVE("FAIRGROUND_RIDES_FERRIS_WHALE")
			STOP_AUDIO_SCENE("FAIRGROUND_RIDES_FERRIS_WHALE")
		ENDIF
		
		IF IS_AUDIO_SCENE_ACTIVE("FAIRGROUND_RIDES_FERRIS_WHALE_ALTERNATIVE_VIEW")
			STOP_AUDIO_SCENE("FAIRGROUND_RIDES_FERRIS_WHALE_ALTERNATIVE_VIEW")
		ENDIF
		
		CLEANUP_AUDIO()
		
		SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PLAYER_JUST_LEFT_WHEEL)
		CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,SETUP_BOTTOM_CAR)
		CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PLAYER_IN_CAR)
		CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PLAYER_JUST_ENTERED_WHEEL)
		CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,REMOVING_PLAYER_FROM_CAR)
		
		println("am_ferriswheel - CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PLAYER_IN_CAR)")
		
		//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].g_bUsingFerrisWheel = FALSE
		CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bUsingFerrisWheel)
		PRINTLN("am_ferriswheel - g_bUsingFerrisWheel = FALSE")
			
		BROADCAST_CLEAR_INTERACT_CAR(ALL_PLAYERS_ON_SCRIPT(TRUE),playerBD[PARTICIPANT_ID_TO_INT()].iCarOrder,FALSE)
		playerBD[PARTICIPANT_ID_TO_INT()].iCarOrder = -1
		//eExitAnimStage = EXIT_PLAY_ANIM
					

		NETWORK_DISABLE_LEAVE_REMOTE_PED_BEHIND(FALSE)
		
		

ENDPROC

#IF IS_DEBUG_BUILD

/// PURPOSE:
///    Updates the Debug Widgers
PROC UPDATE_DEBUG_WIDGETS()
	IF (bRespotPlayer)
		//PUT_PLAYER_IN_WHEEL_CAR(serverBD.wheelCar[PLAYER_WHEEL_CAR])
		bRespotPlayer = FALSE
	ENDIF
ENDPROC

#ENDIF

/// PURPOSE:
///    Creates a wheel car
/// PARAMS:
///    w - wheel car reference
///    ind - number of car 0 to 11
PROC CREATE_WHEEL_CAR(WHEEL_CAR &w, INT ind)
	
	//CLEANUP_WHEEL_CAR(w)

	w.iIndex = ind
	wheelCarObject[w.iIndex] = CREATE_OBJECT(wheelCarModel, <<0, 1, 2>>,FALSE,FALSE)
	SET_ENTITY_INVINCIBLE(wheelCarObject[w.iIndex],TRUE)
	UPDATE_WHEEL_CAR(w)
	

		
//	// create synced scene index
//	FLOAT fAnimRot = GET_ANIM_ROT_FOR_OCCUPIED_GONDOLA(0)
//	w.iSyncSceneID = CREATE_SYNCHRONIZED_SCENE(<<0, 0, 0>>, <<0, 0, fAnimRot>>)
//	SET_SYNCHRONIZED_SCENE_ORIGIN(w.iSyncSceneID, <<0, 0, 0>>, <<0, 0, fAnimRot>>)
//	ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(w.iSyncSceneID, wheelCarObject[w.iIndex], -1)
//	SET_SYNCHRONIZED_SCENE_LOOPED(w.iSyncSceneID, TRUE)
		
ENDPROC



/// PURPOSE:
///  	Create ferris wheel
PROC CREATE_FERRIS_WHEEL()
	INT i = 0
	
	g_fWheelRotation = 0.0
	
	wheelObjectID = CREATE_OBJECT(wheelModel, <<0,1,2>>,FALSE,FALSE)
	SET_ENTITY_COORDS(wheelObjectID, vWheelCenter)
	SET_ENTITY_ROTATION(wheelObjectID, <<g_fWheelRotation, 0, 0>>)
	FREEZE_ENTITY_POSITION(wheelObjectID, TRUE)
	SET_ENTITY_LOD_DIST(wheelObjectID, WHEEL_CAR_LOD_DIST)
	SET_ENTITY_INVINCIBLE(wheelObjectID,TRUE)
	SET_ENTITY_USE_MAX_DISTANCE_FOR_WATER_REFLECTION(wheelObjectID,FALSE)
	
	IF NOT IS_AUDIO_SCENE_ACTIVE("FAIRGROUND_RIDES_FERRIS_WHALE")
		START_AUDIO_SCENE("FAIRGROUND_RIDES_FERRIS_WHALE")
	ENDIF
	
	//iOccupiedCount = 0
	REPEAT MAX_WHEEL_CARS i 
		CREATE_WHEEL_CAR(serverBD.wheelCar[i], i)
		//UPDATE_WHEEL_CAR(serverBD.wheelCar[i])
	ENDREPEAT
	
	IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
		SET_BUILDING_STATE(BUILDINGNAME_IPL_FERRIS_WHEEL, BUILDINGSTATE_DESTROYED)
		PRINTLN("am_ferriswheel - CREATE_FERRIS_WHEEL - BUILDINGSTATE_DESTROYED")
	ENDIF

ENDPROC

/// PURPOSE:
///  	Create ferris wheel
PROC UPDATE_FERRIS_WHEEL()
	INT i
	VECTOR v
	FLOAT fTimeStep

	IF previousNetworkTime != null		
		fTimeStep = GET_TIME_DIFFERENCE(GET_NETWORK_TIME_ACCURATE(),previousNetworkTime) /1000.0
	ENDIF
	
	previousNetworkTime = GET_NETWORK_TIME_ACCURATE()
	
	IF serverBD.iCurrentRequest != -1
	AND IS_BIT_SET(serverBD.iServerBitSet,MOVING_TO_CAR) 
	AND g_fWheelRotation < serverBD.wheelCar[serverBD.requests[serverBD.iCurrentRequest].iSelectedCarIndex].fPosition
	AND (g_fWheelRotation + fWheelRotationRate * fTimeStep) > serverBD.wheelCar[serverBD.requests[serverBD.iCurrentRequest].iSelectedCarIndex].fPosition
		g_fWheelRotation = serverBD.wheelCar[serverBD.requests[serverBD.iCurrentRequest].iSelectedCarIndex].fPosition
	ELSE
		g_fWheelRotation += fWheelRotationRate * fTimeStep
	ENDIF
	
	// rotate the wheel
	
	IF (g_fWheelRotation >= 360.0)
		g_fWheelRotation -= 360.0
		#IF IS_DEBUG_BUILD
			iRevolutions ++
		#ENDIF
	ENDIF
	
	
	IF IS_ENTITY_OK(wheelObjectID)
		SET_ENTITY_COORDS(wheelObjectID, vWheelCenter)
		SET_ENTITY_ROTATION(wheelObjectID, <<-g_fWheelRotation - (360.0 / 16.0), 0, 0>>)
		FREEZE_ENTITY_POSITION(wheelObjectID, TRUE)
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("FAIRGROUND_RIDES_FERRIS_WHALE")
		IF IS_ENTITY_OK(PLAYER_PED_ID())
			v = GET_ENTITY_COORDS(PLAYER_PED_ID())
		ENDIF
		SET_AUDIO_SCENE_VARIABLE("FAIRGROUND_RIDES_FERRIS_WHALE", "HEIGHT", v.z - fWheelGroundZ)
	ENDIF
	
	REPEAT MAX_WHEEL_CARS i 
		UPDATE_WHEEL_CAR(serverBD.wheelCar[i])
	ENDREPEAT
ENDPROC

/// PURPOSE:
///  	Cleanup ferris wheel
PROC CLEANUP_FERRIS_WHEEL()
	IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
		SET_BUILDING_STATE(BUILDINGNAME_IPL_FERRIS_WHEEL, BUILDINGSTATE_NORMAL) 
		PRINTLN("am_ferriswheel - CREATE_FERRIS_WHEEL - BUILDINGSTATE_NORMAL")
	ENDIF
	
	INT i 

	REPEAT COUNT_OF(serverBD.wheelCar) i
		CLEANUP_WHEEL_CAR(serverBD.wheelCar[i])
	ENDREPEAT
		
	IF IS_AUDIO_SCENE_ACTIVE("FAIRGROUND_RIDES_FERRIS_WHALE")
		STOP_AUDIO_SCENE("FAIRGROUND_RIDES_FERRIS_WHALE")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("FAIRGROUND_RIDES_FERRIS_WHALE_ALTERNATIVE_VIEW")
		STOP_AUDIO_SCENE("FAIRGROUND_RIDES_FERRIS_WHALE_ALTERNATIVE_VIEW")
	ENDIF
	
	SAFE_STOP_AND_RELEASE_SOUND_ID(iSqueakSoundID)
	SAFE_STOP_AND_RELEASE_SOUND_ID(iSqueakSoundID)
	SAFE_STOP_AND_RELEASE_SOUND_ID(iGeneratorSoundID)
	SAFE_STOP_AND_RELEASE_SOUND_ID(iRattlingSoundID)
	
	SAFE_DELETE_OBJECT(wheelObjectID)
ENDPROC



PROC PAY_FOR_RIDE
	INT iScriptTransactionIndex
	IF GET_LOCAL_PLAYER_VC_AMOUNT(TRUE) >= g_sMptunables.iferriswheelcost
	OR NETWORK_CAN_SPEND_MONEY(g_sMptunables.iferriswheelcost,FALSE,FALSE,TRUE) //Use OR to catch cash values over SCRIPT_MAX_INT32.
			
			IF USE_SERVER_TRANSACTIONS()
			// Trigger a cash transaction for PC Build (2150728)
				TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_SPEND_FAIRGROUND, g_sMPTunables.iferriswheelcost, iScriptTransactionIndex, FALSE, TRUE)
				g_cashTransactionData[iScriptTransactionIndex].cashInfo.iLocation = RIDE_ID
			ELSE
				NETWORK_BUY_FAIRGROUND_RIDE(g_sMptunables.iferriswheelcost,RIDE_ID,FALSE,TRUE)
			ENDIF
			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PLAYER_PAID)
	ELSE
		IF NOT IS_BIT_SET(iLocalBitSet, STORE_ACTIVE)
			LAUNCH_STORE_CASH_ALERT()
			SET_BIT(iLocalBitSet, STORE_ACTIVE)
			//NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, TRUE, TRUE, TRUE, TRUE, TRUE, FALSE, TRUE, FALSE)
			PRINTLN("am_ferriswheel - LAUNCH_STORE_CASH_ALERT - CALLED")
		ENDIF
	ENDIF
ENDPROC
//----------------------
//	MAIN
//----------------------

/// PURPOSE:
///    Setups up the script
PROC SCRIPT_SETUP()
	
	#IF IS_DEBUG_BUILD
	SETUP_DEBUG_WIDGETS()
	#ENDIF
	
	//SET_BUILDING_STATE(BUILDINGNAME_IPL_FERRIS_WHEEL, BUILDINGSTATE_DESTROYED)
	
	//ADD_ASSET_REQUEST_FOR_MODEL(asRequest, 0, wheelModel)
	//ADD_ASSET_REQUEST_FOR_MODEL(asRequest, 1, wheelCarModel)
	//ADD_ASSET_REQUEST_FROM_STRING(asRequest, 2, ASSET_ANIMDICT, sWheelCarAnimDict)
//	ADD_ASSET_REQUEST_FROM_STRING(asRequest1, 3, ASSET_AUDIOBANK, "SCRIPT\\FERRIS_WHALE_01")
//	ADD_ASSET_REQUEST_FROM_STRING(asRequest2, 4, ASSET_AUDIOBANK, "SCRIPT\\FERRIS_WHALE_02")
	
	// 15/12/2015 - removing hinting altogether as it causes asserts on certain freemode events
	// Only hint the audio banks if we are not in veh, stops asserst about not enough free banks if we are in a luxury veh that launches scripts requesting audio banks
	// (url:bugstar:2628439)
	IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		//HINT_SCRIPT_AUDIO_BANK("SCRIPT\\FERRIS_WHALE_01")
		//HINT_SCRIPT_AUDIO_BANK("SCRIPT\\FERRIS_WHALE_02")
	ENDIF
	
	//CREATE_FERRIS_WHEEL()

	
ENDPROC

/// PURPOSE: 
///  	Cleanups Script and terminates thread - this should be the last function called 
PROC SCRIPT_CLEANUP()
	
	#IF IS_DEBUG_BUILD
	CLEANUP_DEBUG_WIDGETS()
	#ENDIF

	CLEANUP_FERRIS_WHEEL()
	DISABLE_CELLPHONE(FALSE)
	SHUTDOWN_CAMERA(cameraID)
	SHUTDOWN_CAMERA(initialCamera)
	CLEAR_FIRST_PERSON_CAMERA(fpsCam)
	
	IF IS_AUDIO_SCENE_ACTIVE("FAIRGROUND_RIDES_FERRIS_WHALE")
		STOP_AUDIO_SCENE("FAIRGROUND_RIDES_FERRIS_WHALE")
	ENDIF	
	
	//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].g_bUsingFerrisWheel = FALSE
	CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bUsingFerrisWheel)
	PRINTLN("am_ferriswheel - g_bUsingFerrisWheel = FALSE")	
		
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PLAYER_IN_CAR)
		OR IS_PLAYER_IN_CORONA()																// Make sure we clean up if we've triggered a corona as we enter the car
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
			AND IS_ENTITY_ATTACHED(PLAYER_PED_ID()) 
				DETACH_ENTITY(PLAYER_PED_ID()) 
			ENDIF
			
			IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PLAYER_IN_CAR)
				CLEAR_PED_TASKS(PLAYER_PED_ID())
			ENDIF
		ENDIF
		SET_FERRISWHEEL_PASSIVE_MODE(FALSE)
	ENDIF
	
	CLEANUP_AUDIO()
	
	UNLOAD_REQUESTED_ASSETS(asRequest1)
	UNLOAD_REQUESTED_ASSETS(asRequest2)
	REACTIVATE_NAMED_WORLD_BRAINS_WAITING_TILL_OUT_OF_RANGE("fairgroundHub")
	TERMINATE_THIS_THREAD()
ENDPROC

//Helper function to get a clients game/mission state
FUNC INT GET_CLIENT_MISSION_STATE(INT iPlayer)
	RETURN playerBD[iPlayer].iGameState
ENDFUNC

//Helper function to get the servers game/mission state
FUNC INT GET_SERVER_MISSION_STATE()
	RETURN serverBD.iServerGameState
ENDFUNC

FUNC BOOL INIT_WHEEL()
	INT iTemp

	REPEAT MAX_WHEEL_CARS iTemp
		serverBD.wheelCar[iTemp].fPosition = (360.0 / MAX_WHEEL_CARS) * iTemp - 21.5
		println("Car ",iTemp," at rotation ",serverBD.wheelCar[iTemp].fPosition)
	ENDREPEAT
	
	serverBD.wheelCar[0].fPosition += 360
	println("Car 0 at rotation ",serverBD.wheelCar[0].fPosition)
	
	RETURN TRUE
ENDFUNC

FUNC BOOL INIT_WHEEL_CLIENT()
	
	REQUEST_ANIM_DICT(sFW_AnimDict)
	REQUEST_ANIM_DICT(sWheelCarAnimDict)
	REQUEST_MODEL(wheelModel)
	REQUEST_MODEL(wheelCarModel)
	//REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\FERRIS_WHALE_01")
	//REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\FERRIS_WHALE_02")
	
	IF NOT HAS_ANIM_DICT_LOADED(sFW_AnimDict)
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_ANIM_DICT_LOADED(sWheelCarAnimDict)
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_MODEL_LOADED(wheelModel)
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_MODEL_LOADED(wheelCarModel)
		RETURN FALSE
	ENDIF
	
	CREATE_FERRIS_WHEEL()
	
	BROADCAST_REQUEST_ATTACH_REMOTE_PLAYER_TO_CAR(ALL_PLAYERS_ON_SCRIPT(FALSE))
	BROADCAST_REQUEST_WHEEL_ROTATION(ALL_PLAYERS_ON_SCRIPT(FALSE))
	
	eEnterAnimStage = ENTER_WALK_TO_POINT
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_WHEEL_SCRIPT_RUN()
	INT iTemp
	REPEAT NUM_NETWORK_PLAYERS iTemp
		PLAYER_INDEX playerID = INT_TO_PLAYERINDEX(iTemp)
		IF IS_NET_PLAYER_OK(playerID, FALSE, TRUE)
			IF IS_ENTITY_AT_COORD(GET_PLAYER_PED(playerID), <<-1662.0095, -1126.8689, 12.6973>>, <<500, 500, 500>>)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

PROC SETUP_PLAYER_IN_CAR
	SETUP_CAMERA_STRUCT(outerCamera, <<-1703.8540, -1082.2223, 42.0060>>, <<-8.3096, 0.0000, -111.8213>>)
	
	ENABLE_CAMERA_STRUCT(outerCamera, cameraID)
	
	//PRINT_HELP("AMFW_PROMPTS")
	
	IF IS_BIT_SET(iLocalBitset, SOUNDS_LOADED)
		iGeneratorSoundID = GET_SOUND_ID()
		PLAY_SOUND_FROM_ENTITY(iGeneratorSoundID, "GENERATOR", wheelObjectID, "THE_FERRIS_WHALE_SOUNDSET")
		
		iSlowSqueakSoundID = GET_SOUND_ID()
		PLAY_SOUND_FROM_ENTITY(iSlowSqueakSoundID, "SLOW_SQUEAK", wheelObjectID, "THE_FERRIS_WHALE_SOUNDSET")
		
		iSqueakSoundID = GET_SOUND_ID()
		PLAY_SOUND_FROM_ENTITY(iSqueakSoundID, "SLOW_SQUEAK", wheelCarObject[PLAYER_WHEEL_CAR], "THE_FERRIS_WHALE_SOUNDSET")

		iRattlingSoundID = GET_SOUND_ID()
		PLAY_SOUND_FROM_ENTITY(iRattlingSoundID, "CARRIAGE", wheelCarObject[PLAYER_WHEEL_CAR], "THE_FERRIS_WHALE_SOUNDSET")
	ENDIF
	
	SWITCH_TO_EXTERNAL_MODE()
	
	CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PLAYER_JUST_ENTERED_WHEEL)
ENDPROC

//PURPOSE: Sets up the Control Icons to be displayed in the bottom right of the screen
PROC PROCESS_INSTRUCTIONAL_BUTTONS()	
	IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PLAYER_IN_CAR)
		IF IS_BIT_SET(iLocalBitSet, BUTTONS_REQUESTED)
			IF HAS_SCALEFORM_MOVIE_LOADED(buttonMovie)
				IF bRefreshButtons
					aSprite = GET_SCALEFORM_INSTRUCTIONAL_BUTTON_POSITION()
					REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(instructionalButtons)
				
					ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_SCRIPT_SELECT), "AMFW_BTNCV", instructionalButtons)  //Change View
					IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,STOP_REQUESTED)
						ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_CONTEXT), "AMFW_BTNEX", instructionalButtons)		//Exit
					ENDIF
					
					bRefreshButtons = FALSE
				ENDIF	
				SET_TEXT_RENDER_ID(GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID())
				
				SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(FALSE)
				
				SET_INSTRUCTIONAL_BUTTONS_UNDER_HUD_THIS_FRAME()
				SET_LOADING_ICON_SUBTITLES_OFFSET_SHIFT_THIS_FRAME()
				
				RUN_SCALEFORM_INSTRUCTIONAL_BUTTONS(buttonMovie, aSprite, instructionalButtons, SHOULD_REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(instructionalButtons))
			ENDIF
		ELSE
			buttonMovie = REQUEST_SCALEFORM_MOVIE("instructional_buttons")
			SET_BIT(iLocalBitSet,BUTTONS_REQUESTED)
		ENDIF
	ELSE
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(buttonMovie)
		CLEAR_BIT(iLocalBitSet, BUTTONS_REQUESTED)
		bRefreshButtons = TRUE
	ENDIF
ENDPROC


PROC PROCESS_WHEEL_CONTROLS
	
	IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PLAYER_IN_CAR)
	
		HIDE_HUD_AND_RADAR_THIS_FRAME()
		DISABLE_CONTROL_ACTION(CAMERA_CONTROL,INPUT_NEXT_CAMERA)

		// Force instructional buttons to update if player switches control types.
		IF HAVE_CONTROLS_CHANGED(FRONTEND_CONTROL)
			bRefreshButtons = TRUE
		ENDIF
	
		IF NOT IS_PAUSE_MENU_ACTIVE()
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_SELECT)
				IF (bFPSMode = FALSE)
					SWITCH_TO_FIRST_PERSON_MODE()
				ELSE
					SWITCH_TO_EXTERNAL_MODE()
				ENDIF
			ENDIF
			
			IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,WHEEL_STOPPED)
			AND NOT IS_BIT_SET(serverBD.iPlayerBitSet[PARTICIPANT_ID_TO_INT()],STOP_REQUEST_PROCESSED)
			AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,STOP_REQUESTED)
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
					println("am_ferriswheel - PLAYER REQUESTS EXIT")
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,STOP_REQUESTED)
					bRefreshButtons = TRUE
				ENDIF
			ENDIF
		ENDIF	
		
			IF (bFPSMode = TRUE)
			
				IF (bStandardCamera = FALSE)
					UPDATE_BASIC_CAMERA(fpsTestCam)
				ELSE
					UPDATE_FIRST_PERSON_CAMERA(fpsCam, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 0.1)
					SET_CAM_ROT(fpsCam.theCam, GET_ENTITY_ROTATION(PLAYER_PED_ID()) + fpsCam.vCamRotOffsetCurrent)
					SET_LOCAL_PLAYER_INVISIBLE_LOCALLY()
				ENDIF
			ENDIF

	ENDIF
	
	PROCESS_INSTRUCTIONAL_BUTTONS()	
ENDPROC

//PURPOSE: Finds the next car in sequence
FUNC INT GET_NEXT_CAR()
	INT iTemp

	REPEAT MAX_WHEEL_CARS iTemp	
		IF iTemp = 0
			iTemp = 1
		ENDIF
		IF serverBD.wheelCar[iTemp].fPosition >= g_fWheelRotation		//Next car in rotation
			RETURN iTemp
		ENDIF
	ENDREPEAT
		
	IF serverBD.wheelCar[0].fPosition >= g_fWheelRotation						//Special case
		RETURN 0
	ELSE
		RETURN 1
	ENDIF
	
	RETURN -1															//Keep the compiler happy, shouldn't ever be returned
	
ENDFUNC

//PURPOSE: Gets whether a stop request exists
FUNC BOOL DOES_REQUEST_EXIST()
	INT iParticipant

	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		IF serverBD.requests[iParticipant].iSelectedCarIndex != -1
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Gets the next stop request to be processed
FUNC INT GET_NEXT_REQUEST()
	INT iNextCar,iParticipant,iStartCar
	BOOL bStarted
		
	iStartCar = GET_NEXT_CAR()
	
	iNextCar = iStartCar
	WHILE(iNextCar!=iStartCar OR NOT bStarted)
		IF NOT bStarted
			bStarted = TRUE
		ENDIF
		REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant					//For all requests
			IF serverBD.requests[iParticipant].iSelectedCarIndex = iNextCar		//Is there a request for this car?
				RETURN iParticipant
			ENDIF
		ENDREPEAT
		iNextCar++																//Try next car
		IF iNextCar = MAX_WHEEL_CARS
			iNextCar = 0
		ENDIF
	ENDWHILE
	
	RETURN -1

ENDFUNC

//PURPOSE: Finds a car for the player. If player is already in a car, it will be returned
FUNC INT FIND_CAR(INT iPlayerIndex)
	INT iTemp,iTemp2
	BOOL bAcceptPopulatedCars
	
	REPEAT 2 iTemp2	
		IF IS_BIT_SET(playerBD[iPlayerIndex].iClientBitSet,PLAYER_IN_CAR)
			
				println("am_ferriswheel - PLAYER ALREADY IN CAR")
			
			RETURN playerBD[iPlayerIndex].iCarIndex
		ELSE
			REPEAT MAX_WHEEL_CARS iTemp	
				IF iTemp = 0
					iTemp = 1
				ENDIF
				IF NOT(serverBD.wheelCar[iTemp].iNumPlayers >= MAX_WHEEL_CAR_OCCUPANTS)
					
						println("am_ferriswheel - CHECKING CAR ",iTemp," at position ",serverBD.wheelCar[iTemp].fPosition,". Current rotation: ",g_fWheelRotation)
					
					IF serverBD.wheelCar[iTemp].fPosition >= g_fWheelRotation		//Next free car in rotation
						IF NOT(serverBD.wheelCar[iTemp].iNumPlayers > 0)
						OR bAcceptPopulatedCars
							RETURN iTemp
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
			
			IF NOT(serverBD.wheelCar[0].iNumPlayers >= MAX_WHEEL_CAR_OCCUPANTS)
				println("am_ferriswheel - CHECKING CAR 0 at position ",serverBD.wheelCar[0].fPosition,". Current rotation: ",g_fWheelRotation)
				IF serverBD.wheelCar[0].fPosition >= g_fWheelRotation						//Special case
					IF NOT(serverBD.wheelCar[0].iNumPlayers > 0)
					OR bAcceptPopulatedCars
						RETURN 0
					ENDIF
				ELSE
					RETURN 1
				ENDIF
			ENDIF
			
			
			
			IF NOT bAcceptPopulatedCars				//No empty cars, try populated (but not full) ones
				bAcceptPopulatedCars = TRUE
			ELSE
				RETURN -1
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN -1
	
ENDFUNC

FUNC BOOL ARE_PLAYERS_READY()
	INT iTemp
	
	FOR iTemp = NUM_NETWORK_REAL_PLAYERS() TO 0 STEP -1
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iTemp))
			IF IS_BIT_SET(playerBD[iTemp].iClientBitSet,PUTTING_PLAYER_IN_CAR)
			OR IS_BIT_SET(playerBD[iTemp].iClientBitSet,REMOVING_PLAYER_FROM_CAR)
				RETURN FALSE
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN TRUE
	
ENDFUNC

//----------------------
//	EVENT PROCESSING
//----------------------

//Purpose: Syncs the current rotation of the Ferris wheel with the server
PROC PROCESS_SYNC_WHEEL_ROTATION(INT iEventID)
	NET_NL() NET_PRINT("     ----->    PROCESS_SYNC_WHEEL_ROTATION( - called...") NET_NL()

	SCRIPT_EVENT_DATA_SYNC_WHEEL_ROTATION Event	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
		g_fWheelRotation = Event.fRotation
		PRINTLN("     ----->    PROCESS_SYNC_WHEEL_ROTATION ",Event.fRotation,"( - SENT FROM ", GET_PLAYER_NAME(Event.Details.FromPlayerIndex)," at ",GET_CLOUD_TIME_AS_INT())
	ELSE
		SCRIPT_ASSERT("PROCESS_SYNC_WHEEL_ROTATION( - could not retreive data.")		
	ENDIF
ENDPROC

//Purpose: Syncs the current rotation of the Ferris wheel with the server
PROC PROCESS_REQUEST_WHEEL_ROTATION(INT iEventID)
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()

		NET_NL() NET_PRINT("     ----->    PROCESS_REQUEST_WHEEL_ROTATION( - called...") NET_NL()

		SCRIPT_EVENT_DATA_REQUEST_WHEEL_ROTATION Event	
		IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
			BROADCAST_SYNC_WHEEL_ROTATION(SPECIFIC_PLAYER(Event.Details.FromPlayerIndex))
			PRINTLN("     ----->    PROCESS_REQUEST_WHEEL_ROTATION ( - SENT FROM ", GET_PLAYER_NAME(Event.Details.FromPlayerIndex)," at ",GET_CLOUD_TIME_AS_INT())
		ELSE
			SCRIPT_ASSERT("PROCESS_REQUEST_WHEEL_ROTATION( - could not retreive data.")		
		ENDIF
	ENDIF
	
ENDPROC

//Purpose: Request all players send an attach event
PROC PROCESS_REQUEST_ATTACH_REMOTE_PLAYER_TO_CAR(INT iEventID)
	NET_NL() NET_PRINT("     ----->    PROCESS_REQUEST_ATTACH_REMOTE_PLAYER_TO_CAR( - called...") NET_NL()

	SCRIPT_EVENT_DATA_REQUEST_ATTACH_REMOTE_PLAYER_TO_CAR Event	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
		IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PLAYER_IN_CAR)
			NETWORK_ALLOW_REMOTE_ATTACHMENT_MODIFICATION(PLAYER_PED_ID(),TRUE)
			VECTOR vCarOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(wheelCarObject[playerBD[PARTICIPANT_ID_TO_INT()].iCarIndex],GET_ENTITY_COORDS(PLAYER_PED_ID()))
			BROADCAST_ATTACH_REMOTE_PLAYER_TO_CAR(SPECIFIC_PLAYER(Event.Details.FromPlayerIndex),playerBD[PARTICIPANT_ID_TO_INT()].iCarIndex,vCarOffset, GET_ENTITY_HEADING(PLAYER_PED_ID()))
			PRINTLN("     ----->    PROCESS_REQUEST_ATTACH_REMOTE_PLAYER_TO_CAR ( - SENT FROM ", GET_PLAYER_NAME(Event.Details.FromPlayerIndex)," at ",GET_CLOUD_TIME_AS_INT())
		ENDIF
	ELSE
		SCRIPT_ASSERT("PROCESS_REQUEST_ATTACH_REMOTE_PLAYER_TO_CAR( - could not retreive data.")		
	ENDIF
ENDPROC

//Purpose: Attach a remote player to the local Ferris wheel car
PROC PROCESS_ATTACH_REMOTE_PLAYER_TO_CAR(INT iEventID)
	NET_NL() NET_PRINT("     ----->    PROCESS_ATTACH_REMOTE_PLAYER_TO_CAR( - called...") NET_NL()

	SCRIPT_EVENT_DATA_ATTACH_REMOTE_PLAYER_TO_CAR Event	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
		IF Event.iCarIndex != -1
			PED_INDEX playerPed = GET_PLAYER_PED(Event.Details.FromPlayerIndex)
			IF IS_NET_PLAYER_OK(Event.Details.FromPlayerIndex)
				NETWORK_ALLOW_REMOTE_ATTACHMENT_MODIFICATION(playerPed,TRUE)
				ATTACH_ENTITY_TO_ENTITY(playerPed, wheelCarObject[Event.iCarIndex], 0, Event.vOffset, <<0, 0, Event.fAngle>>)
				PRINTLN("     ----->    PROCESS_ATTACH_REMOTE_PLAYER_TO_CAR ( - SENT FROM ", GET_PLAYER_NAME(Event.Details.FromPlayerIndex)," at ",GET_CLOUD_TIME_AS_INT())
			ENDIF
		ELSE
			PRINTLN("     ----->    PROCESS_ATTACH_REMOTE_PLAYER_TO_CAR - CANCELLED - iCarIndex = -1   ( - SENT FROM ", GET_PLAYER_NAME(Event.Details.FromPlayerIndex)," at ",GET_CLOUD_TIME_AS_INT())
		ENDIF
	ELSE
		SCRIPT_ASSERT("PROCESS_ATTACH_REMOTE_PLAYER_TO_CAR( - could not retreive data.")		
	ENDIF
ENDPROC

//Purpose: Detach a remote player from the local Ferris wheel car
PROC PROCESS_DETACH_REMOTE_PLAYER_FROM_CAR(INT iEventID)
	NET_NL() NET_PRINT("     ----->    PROCESS_DETACH_REMOTE_PLAYER_FROM_CAR( - called...") NET_NL()

	SCRIPT_EVENT_DATA_DETACH_REMOTE_PLAYER_FROM_CAR Event	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
		PED_INDEX playerPed = GET_PLAYER_PED(Event.Details.FromPlayerIndex)
		IF IS_NET_PLAYER_OK(Event.Details.FromPlayerIndex)
			NETWORK_ALLOW_REMOTE_ATTACHMENT_MODIFICATION(playerPed,TRUE)
			DETACH_ENTITY(playerPed,FALSE)
			PRINTLN("     ----->    PROCESS_DETACH_REMOTE_PLAYER_FROM_CAR ( - SENT FROM ", GET_PLAYER_NAME(Event.Details.FromPlayerIndex)," at ",GET_CLOUD_TIME_AS_INT())
		ENDIF
	ELSE
		SCRIPT_ASSERT("PROCESS_DETACH_REMOTE_PLAYER_FROM_CAR( - could not retreive data.")		
	ENDIF
ENDPROC

//Purpose: Syncs the current rotation of the Ferris wheel with the server
PROC PROCESS_REQUEST_INTERACT_CAR(INT iEventID)
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()

		NET_NL() NET_PRINT("     ----->    PROCESS_REQUEST_INTERACT_CAR( - called...") NET_NL()

		SCRIPT_EVENT_DATA_REQUEST_INTERACT_CAR Event	
		IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
			Bool b = IS_BIT_SET(serverBD.iServerBitSet,BLOCK_ENTRY)
			BROADCAST_INTERACT_CAR(SPECIFIC_PLAYER(Event.Details.FromPlayerIndex),b)
			IF NOT b
				SET_BIT(serverBD.iServerBitSet,BLOCK_ENTRY)

			ENDIF
			PRINTLN("     ----->    PROCESS_REQUEST_INTERACT_CAR ( ",IS_BIT_SET(serverBD.iServerBitSet,BLOCK_ENTRY),"- SENT FROM ", GET_PLAYER_NAME(Event.Details.FromPlayerIndex)," at ",GET_CLOUD_TIME_AS_INT())
		ELSE
			SCRIPT_ASSERT("PROCESS_REQUEST_INTERACT_CAR( - could not retreive data.")		
		ENDIF
	ENDIF
ENDPROC

//Purpose: Attach a remote player to the local Ferris wheel car
PROC PROCESS_INTERACT_CAR(INT iEventID)
	NET_NL() NET_PRINT("     ----->    PROCESS_INTERACT_CAR( - called...") NET_NL()

	SCRIPT_EVENT_DATA_INTERACT_CAR Event	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
		IF NOT Event.bBlocked
			IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PLAYER_IN_CAR)
				SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,REMOVING_PLAYER_FROM_CAR)
			ELSE
				SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PUTTING_PLAYER_IN_CAR)
			ENDIF
		ENDIF
		PRINTLN("     ----->    PROCESS_INTERACT_CAR ( - SENT FROM ", GET_PLAYER_NAME(Event.Details.FromPlayerIndex)," at ",GET_CLOUD_TIME_AS_INT())

	ELSE
		SCRIPT_ASSERT("PROCESS_INTERACT_CAR( - could not retreive data.")		
	ENDIF
ENDPROC

//Purpose: Syncs the current rotation of the Ferris wheel with the server
PROC PROCESS_CLEAR_INTERACT_CAR(INT iEventID)

	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()

		NET_NL() NET_PRINT("     ----->    PROCESS_CLEAR_INTERACT_CAR( - called...") NET_NL()

		SCRIPT_EVENT_DATA_CLEAR_INTERACT_CAR Event	
		IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
			CLEAR_BIT(serverBD.iServerBitSet,BLOCK_ENTRY)
			CLEAR_BIT(serverBD.iServerBitSet,WAITING_FOR_REQUESTED)
			IF serverBD.iCurrentRequest !=  -1
			AND serverBD.requests[serverBD.iCurrentRequest].iSelectedCarIndex != -1
			AND event.iSeatIndex != -1
				IF NOT Event.bEntering
					println("am_ferriswheel - iNumPlayers --",serverBD.wheelCar[serverBD.requests[serverBD.iCurrentRequest].iSelectedCarIndex].iNumPlayers)
					serverBD.wheelCar[serverBD.requests[serverBD.iCurrentRequest].iSelectedCarIndex].iNumPlayers --
					serverBD.wheelCar[serverBD.requests[serverBD.iCurrentRequest].iSelectedCarIndex].bOccupied[event.iSeatIndex] = FALSE
					println("am_ferriswheel - Seat ",event.iSeatIndex," occupied = ",serverBD.wheelCar[serverBD.requests[serverBD.iCurrentRequest].iSelectedCarIndex].bOccupied[event.iSeatIndex])
				ELSE	
					println("am_ferriswheel - iNumPlayers ++",serverBD.wheelCar[serverBD.requests[serverBD.iCurrentRequest].iSelectedCarIndex].iNumPlayers)
					serverBD.wheelCar[serverBD.requests[serverBD.iCurrentRequest].iSelectedCarIndex].iNumPlayers++
					serverBD.wheelCar[serverBD.requests[serverBD.iCurrentRequest].iSelectedCarIndex].bOccupied[event.iSeatIndex] = TRUE
					println("am_ferriswheel - Seat ",event.iSeatIndex," occupied = ",serverBD.wheelCar[serverBD.requests[serverBD.iCurrentRequest].iSelectedCarIndex].bOccupied[event.iSeatIndex])
				ENDIF
			ENDIF
			PRINTLN("     ----->    PROCESS_CLEAR_INTERACT_CAR ( - SENT FROM ", GET_PLAYER_NAME(Event.Details.FromPlayerIndex)," at ",GET_CLOUD_TIME_AS_INT())
		ELSE
			SCRIPT_ASSERT("PROCESS_CLEAR_INTERACT_CAR( - could not retreive data.")		
		ENDIF
		PRINTLN("     ----->    PROCESS_CLEAR_INTERACT_CAR ( - SENT FROM ", GET_PLAYER_NAME(Event.Details.FromPlayerIndex)," at ",GET_CLOUD_TIME_AS_INT())
	ELSE
		SCRIPT_ASSERT("PROCESS_CLEAR_INTERACT_CAR( - could not retreive data.")		
	ENDIF
		
ENDPROC

//Purpose: Clears any server data for that player
PROC PROCESS_CLEAR_WHEEL_DATA(INT iEventID)

	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()

		NET_NL() NET_PRINT("     ----->    PROCESS_CLEAR_WHEEL_DATA( - called...") NET_NL()

		SCRIPT_EVENT_DATA_CLEAR_WHEEL_DATA Event	
		IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
			println("am_ferriswheel - CLEARING DATA - SelectedCarIndex: ",serverBD.requests[event.iParticipant].iSelectedCarIndex)
			println("am_ferriswheel - CLEARING DATA - RequestingPlayer: ",GET_PLAYER_NAME(serverBD.requests[event.iParticipant].requestingPlayer))
			serverBD.iPlayerBitSet[event.iParticipant] = 0
			serverBD.requests[event.iParticipant].iSelectedCarIndex = -1
			serverBD.requests[event.iParticipant].requestingPlayer = null
			println("am_ferriswheel - CLEARED DATA - SelectedCarIndex: ",serverBD.requests[event.iParticipant].iSelectedCarIndex)
			println("am_ferriswheel - CLEARED DATA - RequestingPlayer null: ",serverBD.requests[event.iParticipant].requestingPlayer=null)
			serverBD.iCurrentRequest = GET_NEXT_REQUEST()
			PRINTLN("     ----->    PROCESS_CLEAR_WHEEL_DATA ( - SENT FROM ", GET_PLAYER_NAME(Event.Details.FromPlayerIndex)," at ",GET_CLOUD_TIME_AS_INT())
		ELSE
			SCRIPT_ASSERT("PROCESS_CLEAR_WHEEL_DATA( - could not retreive data.")		
		ENDIF
		
	ENDIF	
ENDPROC

PROC PROCESS_FERRIS_WHEEL_EVENTS()

	INT iEventID
	EVENT_NAMES ThisScriptEvent
	
	STRUCT_EVENT_COMMON_DETAILS Details


		REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iEventID
		
			ThisScriptEvent = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iEventID)
						
			SWITCH ThisScriptEvent			
			
				CASE EVENT_NETWORK_SCRIPT_EVENT	
					GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Details, SIZE_OF(Details))	
					IF IS_NET_PLAYER_OK(Details.FromPlayerIndex, FALSE)
						IF NETWORK_IS_PLAYER_A_PARTICIPANT(Details.FromPlayerIndex)
							SWITCH Details.Type
								CASE SCRIPT_EVENT_REQUEST_ATTACH_REMOTE_PLAYER_TO_CAR
									NET_PRINT("PROCESS_SCRIPT_EVENT - received SCRIPT_EVENT_REQUEST_ATTACH_REMOTE_PLAYER_TO_CAR") NET_NL()
									PROCESS_REQUEST_ATTACH_REMOTE_PLAYER_TO_CAR(iEventID)				
								BREAK							
								CASE SCRIPT_EVENT_ATTACH_REMOTE_PLAYER_TO_CAR
									NET_PRINT("PROCESS_SCRIPT_EVENT - received SCRIPT_EVENT_ATTACH_REMOTE_PLAYER_TO_CAR") NET_NL()
									PROCESS_ATTACH_REMOTE_PLAYER_TO_CAR(iEventID)				
								BREAK	
								CASE SCRIPT_EVENT_DETACH_REMOTE_PLAYER_FROM_CAR
									NET_PRINT("PROCESS_SCRIPT_EVENT - received SCRIPT_EVENT_DETACH_REMOTE_PLAYER_TO_CAR") NET_NL()
									PROCESS_DETACH_REMOTE_PLAYER_FROM_CAR(iEventID)				
								BREAK									
								CASE SCRIPT_EVENT_SYNC_WHEEL_ROTATION
									NET_PRINT("PROCESS_SCRIPT_EVENT - received SCRIPT_EVENT_SYNC_WHEEL_ROTATION") NET_NL()
									PROCESS_SYNC_WHEEL_ROTATION(iEventID)				
								BREAK
								CASE SCRIPT_EVENT_REQUEST_WHEEL_ROTATION
									NET_PRINT("PROCESS_SCRIPT_EVENT - received SCRIPT_EVENT_REQUEST_WHEEL_ROTATION") NET_NL()
									PROCESS_REQUEST_WHEEL_ROTATION(iEventID)				
								BREAK
								CASE SCRIPT_EVENT_REQUEST_INTERACT_CAR
									NET_PRINT("PROCESS_SCRIPT_EVENT - received SCRIPT_EVENT_REQUEST_INTERACT_CAR") NET_NL()
									PROCESS_REQUEST_INTERACT_CAR(iEventID)	
								BREAK
								CASE SCRIPT_EVENT_INTERACT_CAR
									NET_PRINT("PROCESS_SCRIPT_EVENT - received SCRIPT_EVENT_INTERACT_CAR") NET_NL()
									PROCESS_INTERACT_CAR(iEventID)	
								BREAk
								CASE SCRIPT_EVENT_CLEAR_INTERACT_CAR
									NET_PRINT("PROCESS_SCRIPT_EVENT - received SCRIPT_EVENT_CLEAR_INTERACT_CAR") NET_NL()
									PROCESS_CLEAR_INTERACT_CAR(iEventID)	
								BREAK
								CASE SCRIPT_EVENT_CLEAR_WHEEL_DATA
									NET_PRINT("PROCESS_SCRIPT_EVENT - received SCRIPT_EVENT_CLEAR_WHEEL_DATA") NET_NL()
									PROCESS_CLEAR_WHEEL_DATA(iEventID)	
								BREAK
							ENDSWITCH
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		ENDREPEAT
ENDPROC


//PURPOSE: Runs through the max num participants and does relevant checks
PROC MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER()
	INT iCar
	//INT iParticipant
	
	//Initialise Staggered Loop
	IF iStaggeredParticipant = 0
		//bDoEndStaggeredLoopChecks = FALSE
	ENDIF
		
	//REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iStaggeredParticipant))
			PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iStaggeredParticipant))
			#IF IS_DEBUG_BUILD
			STRING PlayerName = GET_PLAYER_NAME(PlayerId)
			#ENDIF
			//PED_INDEX PlayerPedId = GET_PLAYER_PED(PlayerId)
					
			SWITCH serverBD.eFerrisWheelStage
			
				CASE FERRIS_WHEEL_RUNNING
				
					IF IS_BIT_SET(playerBD[iStaggeredParticipant].iClientBitSet,WHEEL_STOPPED)
					AND NOT IS_BIT_SET(serverBD.iPlayerBitSet[iStaggeredParticipant],WHEEL_STOP_PROCESSED)
						CLEAR_BIT(serverBD.iServerBitSet,MOVING_TO_CAR)
						println("am_ferriswheel - CLEAR :MOVING TO CAR")
						CLEAR_BIT(serverBD.iPlayerBitSet[iStaggeredParticipant],STOP_REQUEST_PROCESSED)
						SET_BIT(serverBD.iServerBitSet,WAITING_FOR_REQUESTED)
						iBoardingTime = BOARDING_TIME
						serverBD.eFerrisWheelStage = FERRIS_WHEEL_BOARDING
						RESET_NET_TIMER(iBoardingTimer)
						SET_BIT(serverBD.iPlayerBitSet[iStaggeredParticipant],WHEEL_STOP_PROCESSED)
						println("am_ferriswheel - SET_BIT(serverBD.iPlayerBitSet[iStaggeredParticipant],WHEEL_STOP_PROCESSED)")	
					ENDIF
					
					IF IS_BIT_SET(playerBD[iStaggeredParticipant].iClientBitSet,STOP_REQUESTED)
					AND NOT IS_BIT_SET(serverBD.iPlayerBitSet[iStaggeredParticipant],STOP_REQUEST_PROCESSED)	
						println("am_ferriswheel - STOP REQUESTED BY ",PlayerName)
						iCar = FIND_CAR(iStaggeredParticipant)
						IF iCar =  -1
							println("am_ferriswheel - NO CAR FOUND")
						ELSE
							println("am_ferriswheel - CAR FOUND: ",iCar)
							serverBD.requests[iStaggeredParticipant].requestingPlayer = PlayerId
							serverBD.requests[iStaggeredParticipant].iSelectedCarIndex = iCar
							serverBD.iCurrentRequest = GET_NEXT_REQUEST()
							SET_BIT(serverBD.iServerBitSet,MOVING_TO_CAR)
							println("am_ferriswheel - SET :MOVING TO CAR ",serverBD.requests[iStaggeredParticipant].iSelectedCarIndex)	
							SET_BIT(serverBD.iPlayerBitSet[iStaggeredParticipant],STOP_REQUEST_PROCESSED)
							println("am_ferriswheel - SET_BIT(serverBD.iPlayerBitSet[iStaggeredParticipant],STOP_REQUEST_PROCESSED) - Player: ",PlayerName)	
						ENDIF
					ENDIF	
				BREAK
				
				CASE FERRIS_WHEEL_BOARDING
					//IF serverBD.requests[iStaggeredParticipant].iSelectedCarIndex = serverBD.requests[serverBD.iCurrentRequest].iSelectedCarIndex
						IF IS_BIT_SET(serverBD.iPlayerBitSet[iStaggeredParticipant],WHEEL_STOP_PROCESSED)
							CLEAR_BIT(serverBD.iPlayerBitSet[iStaggeredParticipant],WHEEL_STOP_PROCESSED)
							println("am_ferriswheel - CLEAR_BIT(serverBD.iPlayerBitSet[iStaggeredParticipant],WHEEL_STOP_PROCESSED)")	
						ENDIF
						

					//ENDIF
								
				BREAK
			
			ENDSWITCH
		ELSE
			IF serverBD.iPlayerBitSet[iStaggeredParticipant] != 0
			OR serverBD.requests[iStaggeredParticipant].iSelectedCarIndex != -1
			OR serverBD.requests[iStaggeredParticipant].requestingPlayer != null
				serverBD.iPlayerBitSet[iStaggeredParticipant] = 0
				serverBD.requests[iStaggeredParticipant].iSelectedCarIndex = -1
				serverBD.requests[iStaggeredParticipant].requestingPlayer = null
				serverBD.iCurrentRequest = GET_NEXT_REQUEST()
			ENDIF
		ENDIF
	//ENDREPEAT
	
	iStaggeredParticipant++
	
	//Reset Staggered Loop
	IF iStaggeredParticipant = NETWORK_GET_MAX_NUM_PARTICIPANTS()
		iStaggeredParticipant = 0		
		//bDoEndStaggeredLoopChecks = TRUE
	ENDIF
ENDPROC

FUNC BOOL PROCESS_PRE_GAME(MP_MISSION_DATA missionScriptArgs)

	println("[MJM] - am_ferriswheel START")
	
	// This marks the script as a net script, and handles any instancing setup. 
	SETUP_MP_MISSION_FOR_NETWORK(GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(missionScriptArgs.mdID.idMission), missionScriptArgs)
	
	//Reserve Entities
	RESERVE_NETWORK_MISSION_OBJECTS(0)
	
	// This makes sure the net script is active, waits until it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD,SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD,SIZE_OF(playerBD))
	
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	
	playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_AWAIT_ACTIVATION
	
	BROADCAST_CLEAR_WHEEL_DATA(ALL_PLAYERS_ON_SCRIPT(TRUE))
	
	RETURN TRUE
	
ENDFUNC

PROC PROCESS_FERRIS_WHEEL_CLIENT

	SWITCH playerBD[PARTICIPANT_ID_TO_INT()].eFerrisWheelStage	
	
		CASE FERRIS_WHEEL_RUNNING
			IF serverBD.eFerrisWheelStage = FERRIS_WHEEL_BOARDING
				CLEAR_HELP()
				CLEAR_BIT(iLocalBitset,WAIT_SHOWN) 
				CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,WHEEL_STOPPED)
				previousNetworkTime = null 																		//to prevent jump when restarting wheel
				playerBD[PARTICIPANT_ID_TO_INT()].eFerrisWheelStage	= FERRIS_WHEEL_BOARDING
				println("am_ferriswheel - playerBD[PARTICIPANT_ID_TO_INT()].eFerrisWheelStage = FERRIS_WHEEL_BOARDING")
			ELSE
			
				IF IS_BIT_SET(serverBD.iServerBitSet,MOVING_TO_CAR)													//Wheel is stopping	
					println("am_ferriswheel - SERVER ABOUT TO START MOVING TO CAR")
					IF serverBD.iCurrentRequest != -1
						IF g_fWheelRotation != serverBD.wheelCar[serverBD.requests[serverBD.iCurrentRequest].iSelectedCarIndex].fPosition
							println("am_ferriswheel - SERVER MOVING TO CAR")
							UPDATE_FERRIS_WHEEL()
						ENDIF
					ELSE
						println("am_ferriswheel - PROCESS_FERRIS_WHEEL_CLIENT - iCurrentRequest = -1")
					ENDIF
				ELSE
					UPDATE_FERRIS_WHEEL()
				ENDIF
				
				IF IS_BIT_SET(serverBD.iPlayerBitSet[PARTICIPANT_ID_TO_INT()],STOP_REQUEST_PROCESSED)				//Wheel is stopping	
					println("am_ferriswheel - SETTING INDEX")
					playerBD[PARTICIPANT_ID_TO_INT()].iCarIndex = serverBD.requests[PARTICIPANT_ID_TO_INT()].iSelectedCarIndex
					println("am_ferriswheel - CLIENT MOVING TO CAR")
					IF playerBD[PARTICIPANT_ID_TO_INT()].iCarIndex != -1
						IF g_fWheelRotation != serverBD.wheelCar[playerBD[PARTICIPANT_ID_TO_INT()].iCarIndex].fPosition
							println("am_ferriswheel - iSelectedCarIndex = ",serverBD.requests[PARTICIPANT_ID_TO_INT()].iSelectedCarIndex," fPosition = ",serverBD.wheelCar[serverBD.requests[PARTICIPANT_ID_TO_INT()].iSelectedCarIndex].fPosition," g_fWheelRotation = ",g_fWheelRotation)
							IF IS_BIT_SET(iLocalBitset,HELP_SHOWN) 
								CLEAR_HELP()
								CLEAR_BIT(iLocalBitset,HELP_SHOWN) 
							ENDIF
							IF NOT IS_BIT_SET(iLocalBitset,WAIT_SHOWN)
								IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PLAYER_IN_CAR)
									PRINT_HELP("AMFW_WAIT_OFF")
								ELSE
									PRINT_HELP("AMFW_WAIT")
								ENDIF
								SET_BIT(iLocalBitset,WAIT_SHOWN) 
							ENDIF
						ELSE
							IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,WHEEL_STOPPED)
								SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,WHEEL_STOPPED)
								CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,STOP_REQUESTED)
								bRefreshButtons = TRUE
								println("am_ferriswheel - SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,WHEEL_STOPPED)")	
							ENDIF
						ENDIF
					ELSE
						CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,STOP_REQUESTED)
						bRefreshButtons = TRUE
						println("am_ferriswheel - iCarIndex = -1 - CLEAR STOP_REQUESTED")	
					ENDIF
				ELSE
					IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PLAYER_PAID)
						IF NOT IS_PHONE_ONSCREEN()
						AND NOT IS_PLAYER_CRITICAL_TO_ANY_EVENT(PLAYER_ID())
						AND NOT IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_TIME_TRIAL)
						AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(),TRUE)
						AND NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
						AND GET_CLOSEST_VEHICLE(<<-1661.9504, -1127.0112, 12.6973>>,1.0,DUMMY_MODEL_FOR_SCRIPT,VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES | VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES)= null
						AND NOT IS_PED_WEARING_JUGGERNAUT_SUIT(PLAYER_PED_ID())
						AND NOT IS_PLAYER_AN_ANIMAL(PLAYER_ID())
							IF NOT IS_PLAYER_ACTIONING_ANY_CORONA_INVITE()
								IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PLAYER_IN_CAR)						//Player not in wheel
									IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),<<-1661.9504, -1127.0112, 12.6973>>,<<1.0,1.0,2.0>>)		//Player outside wheel
										PRINT_HELP_WITH_NUMBER("AMFW_ENTER",g_sMptunables.iferriswheelcost)
										SET_BIT(iLocalBitset,HELP_SHOWN) 
										IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)								//Player requests a car to enter
											println("am_ferriswheel - PLAYER REQUESTS ENTER ",GET_PLAYER_NAME(PLAYER_ID()))
											PAY_FOR_RIDE()
											SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,STOP_REQUESTED)
										ENDIF
									ELSE
										IF IS_BIT_SET(iLocalBitset,HELP_SHOWN) 
											CLEAR_HELP()
											CLEAR_BIT(iLocalBitset,HELP_SHOWN) 
										ENDIF
									ENDIF	
								ENDIF
							ELSE
								IF (GET_GAME_TIMER() % 1000) < 50
									println("am_ferriswheel - IS_PLAYER_ACTIONING_ANY_CORONA_INVITE - Request Blocked.")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				
				#IF IS_DEBUG_BUILD
				UPDATE_DEBUG_WIDGETS()
				#ENDIF
			ENDIF
			PROCESS_WHEEL_CONTROLS()
		BREAK
		
		CASE FERRIS_WHEEL_BOARDING
			IF serverBD.eFerrisWheelStage = FERRIS_WHEEL_RUNNING
				IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PLAYER_JUST_ENTERED_WHEEL)
				OR IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,SETUP_BOTTOM_CAR)
					SETUP_PLAYER_IN_CAR()
					SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_AM_RIDE_FAIRGROUND_RIDE)
				ENDIF
				
				IF NOT IS_BIT_SET(serverBD.iPlayerBitSet[PARTICIPANT_ID_TO_INT()],STOP_REQUEST_PROCESSED)		//If not still waiting on car
					CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PLAYER_PAID)						//Play must pay again
				ENDIF
				
				CLEAR_HELP()
				CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,SETUP_BOTTOM_CAR)
				CLEAR_BIT(iLocalBitset,FULL_SHOWN)
				CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,INTERACT_REQUEST_SENT)
				CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PLAYER_JUST_LEFT_WHEEL)
				playerBD[PARTICIPANT_ID_TO_INT()].eFerrisWheelStage	= FERRIS_WHEEL_RUNNING
				println("am_ferriswheel - playerBD[PARTICIPANT_ID_TO_INT()].eFerrisWheelStage = FERRIS_WHEEL_RUNNING")
				
				IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PLAYER_IN_CAR)
					PROCESS_WHEEL_CONTROLS()
				ENDIF
			ELSE				
				IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PLAYER_IN_CAR)									
					IF serverBD.iCurrentRequest != -1
					AND playerBD[PARTICIPANT_ID_TO_INT()].iCarIndex != -1
					AND playerBD[PARTICIPANT_ID_TO_INT()].iCarIndex = serverBD.requests[serverBD.iCurrentRequest].iSelectedCarIndex		//Player may exit car
						IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,SETUP_BOTTOM_CAR)
							IF NOT IS_BIT_SET(serverBD.iServerBitSet,DONT_SHOW_EXIT)
								PRINT_HELP("AMFW_GET_OUT")
							ENDIF
							SHUTDOWN_CAMERA(cameraID)
							SHUTDOWN_CAMERA(initialCamera)
							CLEAR_FIRST_PERSON_CAMERA(fpsCam)
							SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,SETUP_BOTTOM_CAR)
							println("am_ferriswheel - SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,SETUP_BOTTOM_CAR)")
						ENDIF
						IF NOT IS_BIT_SET(serverBD.iServerBitSet,BLOCK_ENTRY)
							IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_ENTER)
								BROADCAST_REQUEST_INTERACT_CAR(ALL_PLAYERS_ON_SCRIPT(TRUE))
							ENDIF
						ENDIF
					ELSE
						PROCESS_WHEEL_CONTROLS()
					ENDIF
				ELSE
					IF NOT IS_PHONE_ONSCREEN()
					AND NOT IS_PLAYER_CRITICAL_TO_ANY_EVENT(PLAYER_ID())
					AND NOT IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_TIME_TRIAL)
					AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(),TRUE)
					AND NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
					AND GET_CLOSEST_VEHICLE(<<-1661.9504, -1127.0112, 12.6973>>,1.0,DUMMY_MODEL_FOR_SCRIPT,VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES | VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES)= null
					AND NOT IS_INTERACTION_MENU_OPEN()	
					AND NOT g_MultiplayerSettings.g_bSuicide
						IF NOT IS_PLAYER_ACTIONING_ANY_CORONA_INVITE()
							IF IS_BIT_SET(serverBD.iServerBitSet,BLOCK_NON_REQUESTING_PLAYERS)
								println("am_ferriswheel - CLIENT: Requesting Player = ",GET_PLAYER_NAME(serverBD.requests[serverBD.iCurrentRequest].requestingPlayer))
								IF serverBD.requests[serverBD.iCurrentRequest].requestingPlayer = PLAYER_ID()
									IF NOT IS_BIT_SET(serverBD.iServerBitSet,BLOCK_ENTRY)
										IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,INTERACT_REQUEST_SENT)
											println("serverBD.requests[serverBD.iCurrentRequest].requestingPlayer = PLAYER_ID()")
											BROADCAST_REQUEST_INTERACT_CAR(ALL_PLAYERS_ON_SCRIPT(TRUE))
											SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,INTERACT_REQUEST_SENT)
										ENDIF
									ENDIF
								ENDIF
							ELSE																												//Player may enter car
								IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),<<-1661.9504, -1127.0112, 12.6973>>,<<1.0,1.0,2.0>>)
									IF NOT IS_BIT_SET(serverBD.iServerBitSet,BLOCK_ENTRY)
									AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PUTTING_PLAYER_IN_CAR)
									AND serverBD.iCurrentRequest != -1
										println("am_ferriswheel - car occupants = ",serverBD.wheelCar[serverBD.requests[serverBD.iCurrentRequest].iSelectedCarIndex].iNumPlayers)
										IF NOT(serverBD.wheelCar[serverBD.requests[serverBD.iCurrentRequest].iSelectedCarIndex].iNumPlayers >= MAX_WHEEL_CAR_OCCUPANTS)
											IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PLAYER_PAID)
												PRINT_HELP("AMFW_GET_IN")
											ELSE
												PRINT_HELP_WITH_NUMBER("AMFW_GET_IN_P",g_sMptunables.iferriswheelcost)
											ENDIF
											IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL,INPUT_ENTER)
												IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PLAYER_PAID)
													PAY_FOR_RIDE()
												ENDIF
												println("IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_ENTER)")
												BROADCAST_REQUEST_INTERACT_CAR(ALL_PLAYERS_ON_SCRIPT(TRUE))
											ENDIF
										ELSE
											IF NOT IS_BIT_SET(iLocalBitset,FULL_SHOWN)
												PRINT_HELP("AMFW_CAR_FULL")
												SET_BIT(iLocalBitset,FULL_SHOWN)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF (GET_GAME_TIMER() % 1000) < 50
								println("am_ferriswheel - IS_PLAYER_ACTIONING_ANY_CORONA_INVITE - Access Blocked.")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				IF serverBD.iCurrentRequest != -1
					IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PUTTING_PLAYER_IN_CAR)
						PUT_PLAYER_IN_WHEEL_CAR()
					ENDIF
					IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,REMOVING_PLAYER_FROM_CAR)
						REMOVE_PLAYER_FROM_WHEEL_CAR()
					ENDIF
				ENDIF
			ENDIF
		BREAK
	
	ENDSWITCH
	
	IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet,PLAYER_IN_CAR)
		DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	ENDIF
		
	//Make sure Ferris Wheel IPL has been destroyed
	//IF GET_BUILDING_STATE(BUILDINGNAME_IPL_FERRIS_WHEEL) = BUILDINGSTATE_NORMAL
	//	SET_BUILDING_STATE(BUILDINGNAME_IPL_FERRIS_WHEEL, BUILDINGSTATE_DESTROYED)
	//IF IS_IPL_ACTIVE("ferris_finale_Anim")
	IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
		REMOVE_IPL("ferris_finale_Anim")
	ENDIF
	//	SET_BUILDING_STATE(BUILDINGNAME_IPL_FERRIS_WHEEL, BUILDINGSTATE_DESTROYED)
	//	PRINTLN("am_ferriswheel - CREATE_FERRIS_WHEEL - BUILDINGSTATE_DESTROYED - BACKUP")
	//ENDIF
	ADJUST_FOR_BLOCKING_OBJECTS()
ENDPROC

PROC PROCESS_FERRIS_WHEEL_SERVER

	SWITCH serverBD.eFerrisWheelStage
	
		CASE FERRIS_WHEEL_RUNNING

		BREAK
		
		CASE FERRIS_WHEEL_BOARDING
			IF HAS_NET_TIMER_EXPIRED(iBoardingTimer,iBoardingTime)
				IF ARE_PLAYERS_READY()
					IF serverBD.iCurrentRequest != -1
						INT iTemp, iCurrent
						iCurrent = serverBD.requests[serverBD.iCurrentRequest].iSelectedCarIndex
						REPEAT NUM_NETWORK_PLAYERS iTemp
							IF serverBD.requests[iTemp].iSelectedCarIndex = iCurrent
								println("am_ferriswheel - CLEARING REQUEST FROM ",GET_PLAYER_NAME(serverBD.requests[iTemp].requestingPlayer)," FOR ",serverBD.requests[iTemp].iSelectedCarIndex)
								CLEAR_BIT(serverBD.iPlayerBitSet[iTemp],STOP_REQUEST_PROCESSED)
								CLEAR_BIT(serverBD.iPlayerBitSet[iTemp],WHEEL_STOP_PROCESSED)
								serverBD.requests[iTemp].iSelectedCarIndex = -1
								serverBD.requests[iTemp].requestingPlayer = null		
							ENDIF
						ENDREPEAT
						println("am_ferriswheel - CLEARING CURRENT REQUEST FROM ",GET_PLAYER_NAME(serverBD.requests[serverBD.iCurrentRequest].requestingPlayer)," FOR ",serverBD.requests[serverBD.iCurrentRequest].iSelectedCarIndex)
						serverBD.requests[serverBD.iCurrentRequest].iSelectedCarIndex = -1
						serverBD.requests[serverBD.iCurrentRequest].requestingPlayer = null
					ENDIF
					IF DOES_REQUEST_EXIST()
						serverBD.iCurrentRequest = GET_NEXT_REQUEST()
					ELSE
						serverBD.iCurrentRequest = -1
					ENDIF
					IF serverBD.iCurrentRequest != -1
						SET_BIT(serverBD.iServerBitSet,MOVING_TO_CAR)
					ENDIF
					CLEAR_BIT(serverBD.iServerBitSet,BLOCK_ENTRY)
					CLEAR_BIT(serverBD.iServerBitSet,DONT_SHOW_EXIT)
					serverBD.eFerrisWheelStage = FERRIS_WHEEL_RUNNING
				ELSE
					iBoardingTime = ANIMATION_CHECK_TIME
					RESET_NET_TIMER(iBoardingTimer)
				ENDIF
			ELSE
				IF ((BOARDING_TIME-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(iBoardingTimer)) < 4000)
					SET_BIT(serverBD.iServerBitSet,DONT_SHOW_EXIT)
				ENDIF
				IF (BOARDING_TIME-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(iBoardingTimer)) < 3000
					SET_BIT(serverBD.iServerBitSet,BLOCK_ENTRY)
				ENDIF
								
				IF serverBD.iCurrentRequest != -1
				AND NOT IS_BIT_SET(playerBD[serverBD.iCurrentRequest].iClientBitSet,PLAYER_JUST_LEFT_WHEEL)
				AND IS_ENTITY_AT_COORD(GET_PLAYER_PED(serverBD.requests[serverBD.iCurrentRequest].requestingPlayer),<<-1661.9504, -1127.0112, 12.6973>>,<<1.0,1.0,2.0>>)
				AND IS_BIT_SET(serverBD.iServerBitSet,WAITING_FOR_REQUESTED)
					IF NOT IS_BIT_SET(serverBD.iServerBitSet,BLOCK_NON_REQUESTING_PLAYERS)
						SET_BIT(serverBD.iServerBitSet,BLOCK_NON_REQUESTING_PLAYERS)
					ENDIF
				ELSE
					IF IS_BIT_SET(serverBD.iServerBitSet,BLOCK_NON_REQUESTING_PLAYERS)
						CLEAR_BIT(serverBD.iServerBitSet,BLOCK_NON_REQUESTING_PLAYERS)
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH

ENDPROC

//----------------------
//	MAIN SCRIPT
//----------------------
SCRIPT(MP_MISSION_DATA missionScriptArgs)

	BOOL isInMultiplayer = NETWORK_IS_GAME_IN_PROGRESS()
	
	IF(isInMultiplayer)
	
		// Carry out all the initial game starting duties. 
		IF NOT PROCESS_PRE_GAME(missionScriptArgs)
			SCRIPT_CLEANUP()
		ENDIF	

		SCRIPT_SETUP()
		SET_BIT(g_iRepeatPlayBits, ENUM_TO_INT(RPB_FROZEN_ON_FERRIS_WHEEL))

	ENDIF
	
	WHILE (TRUE)
	
		MP_LOOP_WAIT_ZERO()
		
		// If we have a match end event, bail
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			SCRIPT_CLEANUP()
			PRINTLN("am_ferriswheel - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE = TRUE")
		ENDIF
		IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(),<<-1645.5549, -1123.8719, 17.3436>>,<<ciFAIRGROUND_END_DISTANCE, ciFAIRGROUND_END_DISTANCE, ciFAIRGROUND_END_DISTANCE>>,FALSE)
		OR IS_PLAYER_IN_CORONA()
		OR NETWORK_IS_ACTIVITY_SESSION()
		OR (IS_ANY_TRANSITION_SESSION_INVITE_BEING_ACTIONED() AND NOT IS_SKYSWOOP_AT_GROUND())
			playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_CLEANUP
			PRINTLN("am_ferriswheel - iGameState - GAME_STATE_CLEANUP")
		ENDIF
		
		PROCESS_FERRIS_WHEEL_EVENTS()
		
		IF IS_BIT_SET(iLocalBitSet, STORE_ACTIVE)
			IF NOT g_sShopSettings.bProcessStoreAlert
				CLEAR_BIT(iLocalBitSet, STORE_ACTIVE)
				//NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				PRINTLN("am_ferriswheel - LAUNCH_STORE_CASH_ALERT - CLEARED")
			ENDIF
		ENDIF
	
		SWITCH GET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT())	
		
			CASE GAME_STATE_AWAIT_ACTIVATION
				IF GET_SERVER_MISSION_STATE() > GAME_STATE_AWAIT_ACTIVATION
						playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_INIT
				ENDIF
			BREAK
			
			CASE GAME_STATE_INIT
				IF GET_SERVER_MISSION_STATE() > GAME_STATE_INIT
					IF INIT_WHEEL_CLIENT()
						playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_READY
					ENDIF
				ENDIF
			BREAK
			
			CASE GAME_STATE_READY
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_READY
					PROCESS_FERRIS_WHEEL_CLIENT()
				ENDIF
			BREAK
			
			CASE GAME_STATE_CLEANUP
				IF NOT HAS_NET_TIMER_STARTED(iCleanupDelay)
					IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
						SET_BUILDING_STATE(BUILDINGNAME_IPL_FERRIS_WHEEL, BUILDINGSTATE_NORMAL) 
						PRINTLN("am_ferriswheel - CREATE_FERRIS_WHEEL - BUILDINGSTATE_NORMAL")
					ENDIF
				ENDIF
				
				IF HAS_NET_TIMER_EXPIRED(iCleanupDelay, 3000)
				OR IS_TRANSITION_ACTIVE()
				OR IS_FM_MISSION_LAUNCH_IN_PROGRESS()
				OR IS_PLAYER_ON_ANY_FM_MISSION_THAT_IS_NOT_GANG_ATTACK(PLAYER_ID())
					SCRIPT_CLEANUP()
				ENDIF
			BREAK
		ENDSWITCH		
		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			SWITCH GET_SERVER_MISSION_STATE()
			
				CASE GAME_STATE_AWAIT_ACTIVATION
					IF SHOULD_WHEEL_SCRIPT_RUN()
						serverBD.iServerGameState = GAME_STATE_INIT
						println("am_ferriswheel- serverBD.iServerGameState = GAME_STATE_INIT ",GET_PLAYER_NAME(PLAYER_ID()))
					ENDIF
				BREAK
				
				CASE GAME_STATE_INIT
					IF INIT_WHEEL()
						serverBD.iServerGameState = GAME_STATE_READY
						println("am_ferriswheel - serverBD.iServerGameState = GAME_STATE_READY ",GET_PLAYER_NAME(PLAYER_ID()))
					ENDIF
				BREAK
				
				CASE GAME_STATE_READY
					PROCESS_FERRIS_WHEEL_SERVER()
					MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER()
				BREAK
				
			ENDSWITCH
		ENDIF
		
		#IF IS_DEBUG_BUILD
		IF (bQuit)
			SCRIPT_CLEANUP()
		ENDIF	
		#ENDIF
		
	ENDWHILE
ENDSCRIPT



