USING "globals.sch"
USING "net_events.sch"
USING "net_wait_zero.sch"
USING "net_mission.sch"
USING "invade_persuade_main.sch"

#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID widgetMinigame
#ENDIF

PROC CLEANUP_SCRIPT()

	IAP_CLEANUP_AND_EXIT_CLIENT()
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
	
ENDPROC

/// PURPOSE:
///    Do necessary pre game start initialization.
PROC PROCESS_PRE_GAME(MP_MISSION_DATA missionScriptArgs)
	
	missionScriptArgs = missionScriptArgs
	NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
	
ENDPROC

SCRIPT(MP_MISSION_DATA missionScriptArgs)
	
	missionScriptArgs.mdID.idMission = eAM_SCROLLING_ARCADE_CABINET
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		PROCESS_PRE_GAME(missionScriptArgs)
	ENDIF
		
	WHILE TRUE
		MP_LOOP_WAIT_ZERO()
		
		IF NETWORK_IS_GAME_IN_PROGRESS()
		
#IF IS_DEBUG_BUILD
			IF NOT DOES_WIDGET_GROUP_EXIST(widgetMinigame)
				widgetMinigame = START_WIDGET_GROUP("Scrolling Arcade Cabinet Minigame")
				STOP_WIDGET_GROUP()
				IAP_DEBUG_SET_PARENT_WIDGET_GROUP(widgetMinigame)
			ENDIF
#ENDIF
		
			PROCESS_INVADE_AND_PERSUADE()
		ELSE
			CLEANUP_SCRIPT()
		ENDIF
		
		#IF IS_DEBUG_BUILD
			IF IS_KEYBOARD_KEY_PRESSED(KEY_F)
				CLEANUP_SCRIPT()
			ENDIF
		#ENDIF
	ENDWHILE

ENDSCRIPT