//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        AM_MP_BIKER_WAREHOUSE.sch																	//
// Description: Script for managing the interior of a biker warehouse. Warehouse access, spawning etc. 		//
//				is managed by AM_MP_SMPL_INTERIOR_* script while this script is launched by simple interior	//
//				script to handle anything specific to biker warehouses - crates, production line etc.		//
// Written by:  Online Technical Team: Scott Ranken															//
// Date:  		12/07/2016																					//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"

USING "commands_network.sch"
USING "script_player.sch"
USING "commands_path.sch"
USING "commands_event.sch"

USING "net_include.sch"
USING "net_events.sch"
USING "net_spawn.sch"
USING "net_ambience.sch"

USING "net_wait_zero.sch"
USING "freemode_events_header.sch"

USING "net_activity_creator_activities.sch"
//USING "net_MP_Radio.sch"

USING "net_simple_interior.sch"
USING "net_realty_biker_warehouse.sch"
USING "net_MP_CCTV.sch"
USING "net_simple_cutscene.sch"
USING "net_simple_cutscene_interior.sch"
#IF IS_DEBUG_BUILD
USING "net_debug_log.sch"
#ENDIF


//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════════╡ VARIABLES ╞═══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

STRUCT ServerBroadcastData
	INT iBS
	INT iTUTProductSlot = 0
	int ITUTProductTotal = 0
	int ITUTProductFadeTime
	INT iMaterialsTotal
	INT iProductionStage
	BOOL bActiveState
	//MP_RADIO_SERVER_DATA_STRUCT MPRadioServer
	//MP_PROP_ACT_SERVER_CONTROL_STRUCT activityControl
	
	SERVER_CREATOR_ACTIVITY_PEDS activityPeds[iMaxCreatorActivities]
	SERVER_CREATOR_ACTIVITY_PROPS activityProps[iMaxCreatorActivities]
	PED_VAR_ID_STRUCT pedVariationStruct[iMaxCreatorActivities]
ENDSTRUCT
ServerBroadcastData serverBD

ACTIVITY_INTERIOR_STRUCT interiorStruct

CHECK_CREATOR_ACTIVITIES_STRUCT activityCheck[iMaxCreatorActivities]
ACTIVITY_CONTROLLER_STRUCT activityControllerStruct
PED_VAR_ID_STRUCT pedLocalVariationsStruct[iMaxCreatorActivities]


STRUCT PlayerBroadcastData
	INT iBS
	INT iActivityRequested
	MP_CCTV_CLIENT_DATA_STRUCT MPCCTVClient
ENDSTRUCT
PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]

STRUCT PRODUCT_DATA
	OBJECT_INDEX oPalletObject[ciMAX_FACTORY_PALLETS]
	OBJECT_INDEX oProductObject[ciMAX_FACTORY_PRODUCT_UNITS]
	INT iSetProductFadeTimer[ciMAX_FACTORY_PRODUCT_UNITS]
	INT iProductFadeComplete[ciMAX_FACTORY_PRODUCT_UNITS]
	TIME_DATATYPE tdProductFadeTimer[ciMAX_FACTORY_PRODUCT_UNITS]
ENDSTRUCT

STRUCT TUTORIAL_PRODUCT_DATA
	OBJECT_INDEX oTUTProductObject[ciMAX_FACTORY_PRODUCT_UNITS]
	//INT iTUTSetProductFadeTimer[ciMAX_FACTORY_PRODUCT_UNITS]
	//TIME_DATATYPE tdTUTProductFadeTimer[ciMAX_FACTORY_PRODUCT_UNITS]
	//INT iTUTIndividualProductFadeTime = 0
	
	INT iDesiredAlpha[ciMAX_FACTORY_PRODUCT_UNITS]
	INT iTimeOfDesiredAlpha[ciMAX_FACTORY_PRODUCT_UNITS]
	INT iStartAlpha[ciMAX_FACTORY_PRODUCT_UNITS]
	INT iTimeOfStartAlpha[ciMAX_FACTORY_PRODUCT_UNITS]
ENDSTRUCT

STRUCT PRODUCTION_PROPS
	INT iPropID
	INT iPropIter
	OBJECT_INDEX oProductionProp[ciMAX_FACTORY_PRODUCTION_PROPS]
	INT iSetPropFadeTimer[ciMAX_FACTORY_PRODUCTION_PROPS]
	INT iPropFadeComplete[ciMAX_FACTORY_PRODUCTION_PROPS]
	TIME_DATATYPE tdPropFadeTimer[ciMAX_FACTORY_PRODUCTION_PROPS]
ENDSTRUCT

STRUCT HUD_DATA
	INT iSetHUDBlendTimer
	INT iHUDBlendComplete
	INT iHUDBlendOffset
	TIME_DATATYPE tdHUDBlendTimer
ENDSTRUCT

ENUM FACTORY_LAPTOP_SCREEN_STATE
	FACTORY_LAPTOP_SCREEN_STATE_INIT,
	FACTORY_LAPTOP_SCREEN_STATE_LINK_RT,
	FACTORY_LAPTOP_SCREEN_STATE_UPDATE_SCALEFORM
ENDENUM

STRUCT FACTORY_LAPTOP_SCREEN_STRUCT
	FACTORY_LAPTOP_SCREEN_STATE eLaptopState
	INT iRenderTargetID = -1
ENDSTRUCT

ENUM FACTORY_SCRIPT_STATE
	FACTORY_STATE_LOADING,
	FACTORY_STATE_IDLE,
	FACTORY_STATE_PLAYING_INTRO,
	FACTORY_STATE_LOADING_GARAGE_EXIT,
	FACTORY_STATE_PLAYING_GARAGE_EXIT,
	FACTORY_STATE_FINISHING_GARAGE_EXIT
ENDENUM

ENUM FACTORY_UPGRADE_STATE
	FACTORY_UPGRADE_STATE_FADE_OUT,
	FACTORY_UPGRADE_STATE_WARP_PLAYER,
	FACTORY_UPGRADE_STATE_ACTIVATE_ENTITY_SETS,
	FACTORY_UPGRADE_STATE_REFRESH_INTERIOR,
	FACTORY_UPGRADE_STATE_PRODUCTS,
	FACTORY_UPGRADE_STATE_TIMECYCLE_MODIFIERS,
	FACTORY_UPGRADE_STATE_UPDATE_AUDIO,
	FACTORY_UPGRADE_STATE_FADE_IN
ENDENUM

CONST_INT BS_FACTORY_DATA_WAS_ON_EXIT_TRIGGERING_MISSION_LAST_FRAME		0
CONST_INT BS_FACTORY_DATA_DOOR_LOCKED									1
CONST_INT BS_FACTORY_DATA_INIT_PRODUCT									2
CONST_INT BS_FACTORY_DATA_INTERIOR_REFRESHED_ON_INIT					3
CONST_INT BS_FACTORY_DATA_AUDIO_SCENE_STARTED							4
CONST_INT BS_FACTORY_DATA_MOVED_VEHICLES_TO_SAFE_SPOTS					5
CONST_INT BS_FACTORY_DATA_CLEANED_UP_TIMECYCLE_MODIFIERS				6
CONST_INT BS_FACTORY_DATA_REQUESTED_AUDIO_BANKS							7
CONST_INT BS_FACTORY_DATA_AMBIENT_ZONE_SET								8
CONST_INT BS_FACTORY_DATA_WAS_PRODUCTION_ACTIVE_LAST_FRAME				9
CONST_INT BS_FACTORY_DATA_PUSHED_TIMECYCLE_MODIFIER						10
CONST_INT BS_FACTORY_DATA_LOAD_SCENE_STARTED							11 // After entity sets have been activated we have to do a load scene to make sure it's all loaded
CONST_INT BS_FACTORY_DATA_BOSS_WAS_WATCHING_CUTSCENE_LAST_FRAME			12
CONST_INT BS_FACTORY_DATA_PLAYING_FIRST_CUTSCENE						13
CONST_INT BS_FACTORY_DATA_PLAYING_SECOND_CUTSCENE						14
CONST_INT BS_FACTORY_DATA_DELIVERY_SOUND_DONE							15
CONST_INT BS_FACTORY_DATA_DID_INTERIOR_REFRESH_FOR_CUTSCENE				16
CONST_INT BS_FACTORY_DATA_LOAD_SCENE_FOR_CUTSCENE_STARTED				17
#IF FEATURE_GEN9_EXCLUSIVE
CONST_INT BS_FACTORY_DATA_DISPLAYED_MP_INTRO_HELP						18
#ENDIF

STRUCT FACTORY_DATA
	INT iBS
	INT iScriptInstance
	INT iInvitingPlayer
	INT iMaxNumGoons = GB_MAX_GANG_GOONS
	INT iSaveSlot
	
	FACTORY_SCRIPT_STATE eState
	FACTORY_ID eID // ID of this Biker Warehouse
	SIMPLE_INTERIORS eSimpleInteriorID
	SCRIPT_TIMER timerMissionKickOut
	SIMPLE_INTERIOR_ENTRY_ANIM_DATA garageExitAnim
	PRODUCT_DATA productData
	FACTORY_UPGRADE_STATE iUpgradeState
	FACTORY_UPGRADE_ID eUpgradeID
	FACTORY_TYPE eFactoryType
	SCRIPT_TIMER upgradeTimer
	FACTORY_LAPTOP_SCREEN_STRUCT factoryLaptopData
	PRODUCTION_PROPS propData
	HUD_DATA hudData
	TUTORIAL_PRODUCT_DATA tutPropData
	
	PLAYER_INDEX pOwner
	PLAYER_INDEX piGoon
	INTERIOR_INSTANCE_INDEX FactoryInteriorID

	VECTOR vGarageExitMin
	VECTOR vGarageExitMax
	VECTOR vUpgradeSpawnPoint
	
	FLOAT vUpgradeSpawnHeading
	
	STRING strAudioScene // audioscene to use in this factory
	STRING strAmbientZone // ambient zone to usein this factory
	
	INT iSoundIDs[ciMAX_FACTORY_SOUND_IDS]
	INT iProductSlot
	
	INT iPlayersAroundEntranceStagger
	INT iTempNumOfPlayersAroundEntrance
	BOOL bThereArePlayersAroundTheEntrance = TRUE
	
	BOOL bApplyUpgrade = FALSE
	
	BOOL bScriptWasRelaunched
	
	SIMPLE_CUTSCENE introCutscene
	BOOL bFadeScreenInAfterCutsceneIsDone
	INT iProductAnimationStage = -1
	SCRIPT_TIMER timerCutsceneProductSounds
	//INT iDemoProductCount
	INT iDemoProductUpSoundID, iDemoProductDownSoundID
	
	STRING strDeliveryStreamToPlay
	BOOL bPlayingDeliveryStream
	SCRIPT_TIMER timerDeliverySound
	
	INT iProductionStageWhenEntering
	
	BOOL bPlayerLeftTheFactory = FALSE
	
	OBJECT_INDEX oRadio
	BLIP_INDEX bLaptopBlip
ENDSTRUCT
FACTORY_DATA thisFactory

// Cutscene tweaking
//TWEAK_INT CUTSCENE_DEMO_PRODUCT_INIT_TIME			4000 // how long the first few producvts take to fill
//TWEAK_INT CUTSCENE_DEMO_PRODUCT_INIT_DELAY			2500 // how long after the storage area shot appears before we start showing product
//TWEAK_INT CUTSCENE_DEMO_PRODUCT_FULL_FILL_TIME		2000 // how long should it take for whole storage area to get filled
TWEAK_INT CUTSCENE_DEMO_PRODUCT_FULL_FILL_DELAY 	5000 // time before we fill the whole thing
TWEAK_INT CUTSCENE_DEMO_PRODUCT_FULL_FILL_SUSTAIN	8500 // how long the full storage should remain
//TWEAK_INT CUTSCENE_DEMO_PRODUCT_FULL_FILL_RELEASE	1500 // how long it takes for all product to despawn

TWEAK_INT CUTSCENE_DEMO_PRODUCT_FULL_FILL_TIME_WEED 	2000
TWEAK_INT CUTSCENE_DEMO_PRODUCT_FULL_FILL_TIME_METH  	1500
TWEAK_INT CUTSCENE_DEMO_PRODUCT_FULL_FILL_TIME_CRACK  	1200
TWEAK_INT CUTSCENE_DEMO_PRODUCT_FULL_FILL_TIME_FAKE_ID  2000
TWEAK_INT CUTSCENE_DEMO_PRODUCT_FULL_FILL_TIME_MONEY	1500

#IF IS_DEBUG_BUILD
STRUCT FACTORY_DEBUG_DATA
	TEXT_LABEL_63 tlWidgetName[2]
	
	VECTOR vPlacement[2]
	VECTOR vRotation[2]
	
	BOOL bSpawnPlacementObject[2]
	BOOL bToggleBuySpawnPoints
	BOOL bToggleEnterWithGangSpawnPoints

	OBJECT_INDEX oObj[2]
	
	INT iTUTProduct
	INT iTUTProductTotalFadeTime
	BOOL bTUTSpawnProduct
	
	BOOL bHideAllProduct
	BOOL bShowAllProduct

	// For testing of simple interior child script timeout
	BOOL bSimulateTimeout = FALSE
	SCRIPT_TIMER timerSimulateTimeout
	
	BOOL bActivateDemoProduct, bDeactivateDemoProduct
	INT iDemoProductAmount
	INT iDemoProductTime
	BOOL bDoDemoProduct
ENDSTRUCT
FACTORY_DEBUG_DATA debugData
#ENDIF

MP_CCTV_LOCAL_DATA_STRUCT MPCCTVLocal
//MP_RADIO_LOCAL_DATA_STRUCT MPRadioLocal
//MP_PROP_LOCAL_CONTROL serverLocalActControl

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ PROCEDURES  ╞═══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

FUNC BOOL IS_FACTORY_STATE(FACTORY_SCRIPT_STATE eState)
	RETURN thisFactory.eState = eState
ENDFUNC

PROC SET_FACTORY_STATE(FACTORY_SCRIPT_STATE eState)
	CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] AM_MP_BIKER_WAREHOUSE - SET_FACTORY_STATE - New state: ", ENUM_TO_INT(eState))
	thisFactory.eState = eState
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════╡ GARAGE DOOR EXIT ANIM  ╞══════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC INITIALISE_FACTORY_GARAGE_EXIT()
	thisFactory.garageExitAnim.dictionary = "anim@apt_trans@garage"
	thisFactory.garageExitAnim.clip = "gar_open_1_left"
	thisFactory.garageExitAnim.startingPhase = 0.0
	thisFactory.garageExitAnim.endingPhase = 0.45
	
	thisFactory.garageExitAnim.strOnEnterFadeOutFinishSoundSet = "GTAO_EXEC_WH_GARAGE_DOOR_SOUNDS"
	thisFactory.garageExitAnim.strOnEnterFadeOutFinishSoundName = "Door_Open"
	
	thisFactory.vGarageExitMin = <<1104.275, -3101.6143, -40.0000>>
	thisFactory.vGarageExitMax = <<1105.4827, -3097.0706, -36.9999>>
	
	thisFactory.garageExitAnim.syncScenePos = <<1105.095, -3099.446, -40.000>>
	thisFactory.garageExitAnim.syncSceneRot = <<0.0, 0.0, 26.64>>
ENDPROC

FUNC BOOL IS_PLAYER_IN_FACTORY_EXIT_LOCATE()
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND IS_ENTITY_IN_AREA(PLAYER_PED_ID(), thisFactory.vGarageExitMin, thisFactory.vGarageExitMax)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC START_EXIT_VIA_GARAGE_DOOR()
	g_SimpleInteriorData.bShouldExitMenuBeVisible = FALSE
	START_SIMPLE_INTERIOR_ENTRY_ANIM(thisFactory.garageExitAnim)
	SET_FACTORY_STATE(FACTORY_STATE_LOADING_GARAGE_EXIT)
ENDPROC

PROC MAINTAIN_EXIT_VIA_GARAGE_DOOR()
	MAINTAIN_SIMPLE_INTERIOR_ENTRY_ANIM(thisFactory.garageExitAnim, thisFactory.eSimpleInteriorID)
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════╡ FACTORY PRODUCT ╞══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

FUNC BOOL IS_FACTORY_SLOT_AVALIABLE(INT iProductSlot, BOOL bTutorial = FALSE)
	BOOL bAvaliableSlot = FALSE
	IF bTutorial
		IF thisFactory.tutPropData.oTUTProductObject[iProductSlot] = NULL
			bAvaliableSlot = TRUE
		ENDIF
	ELSE
		IF thisFactory.productData.oProductObject[iProductSlot] = NULL
			bAvaliableSlot = TRUE
		ENDIF
	ENDIF
	RETURN bAvaliableSlot
ENDFUNC

FUNC BOOL IS_FACTORY_PRODUCT_SLOT_VALID(FACTORY_ID eFactoryID, INT iProductSlot)
	RETURN (iProductSlot > 0 AND iProductSlot <= GET_FACTORY_PRODUCT_CAPACITY(eFactoryID))
ENDFUNC

FUNC BOOL IS_FACTORY_PALLET_SLOT_AVALIABLE(INT iPalletSlot)
	IF iPalletSlot > -1
	AND iPalletSlot < ciMAX_FACTORY_PALLETS
		RETURN (thisFactory.productData.oPalletObject[iPalletSlot] = NULL)
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_FACTORY_PRODUCTION_PROP_SLOT_AVALIABLE(INT iPropSlot)
	RETURN (thisFactory.propData.oProductionProp[iPropSlot] = NULL)
ENDFUNC

FUNC BOOL CREATE_FACTORY_PRODUCTION_PROP(FACTORY_ID eFactoryID, INT iPropID, BOOL bFadeIn = TRUE)
	
	VECTOR vProductCoords = GET_FACTORY_PRODUCTION_PROP_COORDS(eFactoryID, iPropID)
	VECTOR vProductRotation = GET_FACTORY_PRODUCTION_PROP_ROTATION(eFactoryID, iPropID)
	
	MODEL_NAMES eProductionModel = GET_FACTORY_PRODUCTION_PROP_MODEL(eFactoryID, iPropID)
	
	IF IS_MODEL_VALID(eProductionModel)
	AND IS_FACTORY_PRODUCTION_PROP_SLOT_AVALIABLE(iPropID)
		
		OBJECT_INDEX oProductObj = CREATE_OBJECT(eProductionModel, vProductCoords, FALSE, FALSE)
		
		IF DOES_ENTITY_EXIST(oProductObj)
			SET_ENTITY_ROTATION(oProductObj, vProductRotation)
			FREEZE_ENTITY_POSITION(oProductObj, TRUE)
			thisFactory.propData.oProductionProp[iPropID] = oProductObj
			
			IF bFadeIn
				SET_ENTITY_ALPHA(oProductObj, 0, FALSE)
			ELSE
				IF IS_MODEL_VALID(eProductionModel)
					SET_MODEL_AS_NO_LONGER_NEEDED(eProductionModel)
				ENDIF
			ENDIF
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CREATE_FACTORY_PRODUCT(FACTORY_ID eFactoryID, INT iProductSlot, BOOL bProductFadeIn = TRUE, BOOL bTutorial = FALSE)
	
	VECTOR vProductCoords = GET_FACTORY_PRODUCT_COORDS(thisFactory.pOwner, eFactoryID, iProductSlot)
	VECTOR vProductRotation = GET_FACTORY_PRODUCT_ROTATION(eFactoryID, iProductSlot)
	
	MODEL_NAMES eProductModel = GET_FACTORY_PRODUCT_MODEL(thisFactory.pOwner, eFactoryID)
	
	IF IS_MODEL_VALID(eProductModel)
	AND IS_FACTORY_SLOT_AVALIABLE(iProductSlot)
		
		OBJECT_INDEX oProductObj = CREATE_OBJECT(eProductModel, vProductCoords, FALSE, FALSE)
		
		IF DOES_ENTITY_EXIST(oProductObj)
			SET_ENTITY_ROTATION(oProductObj, vProductRotation)
			FREEZE_ENTITY_POSITION(oProductObj, TRUE)
			IF bTutorial
				thisFactory.tutPropData.oTUTProductObject[iProductSlot] = oProductObj
			ELSE
				thisFactory.productData.oProductObject[iProductSlot] = oProductObj
			ENDIF
			
			IF bProductFadeIn
				SET_ENTITY_ALPHA(oProductObj, 0, FALSE)
			ELSE
				IF IS_MODEL_VALID(eProductModel)
					SET_MODEL_AS_NO_LONGER_NEEDED(eProductModel)
				ENDIF
			ENDIF
			
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_PROPERTY, "CREATE_FACTORY_PRODUCT - Creating Product in Slot: ", iProductSlot, " Model: ", ENUM_TO_INT(eProductModel), " Factory ID: ", eFactoryID)
			#ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CREATE_ALL_FACTORY_PRODUCTS(FACTORY_ID iFactoryID, BOOL bProductFadeIn = FALSE)
	
	INT iProductSlot
	FOR iProductSlot = 0 TO GET_FACTORY_PRODUCT_CAPACITY(iFactoryID)
		IF IS_FACTORY_PRODUCT_SLOT_VALID(iFactoryID, iProductSlot)
			CREATE_FACTORY_PRODUCT(iFactoryID, iProductSlot, bProductFadeIn)
		ENDIF
	ENDFOR
	
ENDPROC

PROC CREATE_FACTORY_PALLETS(FACTORY_ID eFactoryID)
	
	IF CAN_FACTORY_HAVE_PALLET(eFactoryID)
	
		INT iPalletSlot
		FOR iPalletSlot = 0 TO GET_FACTORY_PALLET_TOTAL(eFactoryID)-1
			
			IF IS_FACTORY_PALLET_SLOT_AVALIABLE(iPalletSlot)
			
				VECTOR vPalletCoords = GET_FACTORY_PALLET_COORDS(eFactoryID, iPalletSlot)
				VECTOR vPalletRotation = GET_FACTORY_PALLET_ROTATION(eFactoryID, iPalletSlot)
				
				MODEL_NAMES ePalletModel = GET_FACTORY_PALLET_MODEL(eFactoryID)
				
				IF IS_MODEL_VALID(ePalletModel)
				AND IS_FACTORY_PALLET_SLOT_AVALIABLE(iPalletSlot)
					
					OBJECT_INDEX oPalletObj = CREATE_OBJECT(ePalletModel, vPalletCoords, FALSE, FALSE)
						
					IF DOES_ENTITY_EXIST(oPalletObj)
						SET_ENTITY_ROTATION(oPalletObj, vPalletRotation)
						FREEZE_ENTITY_POSITION(oPalletObj, TRUE)
						thisFactory.productData.oPalletObject[iPalletSlot] = oPalletObj
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
ENDPROC

FUNC BOOL CLEANUP_FACTORY_PALLET(INT iPalletSlot)
	
	OBJECT_INDEX oPalletObj = thisFactory.productData.oPalletObject[iPalletSlot]
	
	IF DOES_ENTITY_EXIST(oPalletObj)
	AND NETWORK_HAS_CONTROL_OF_ENTITY(oPalletObj)
		DELETE_OBJECT(oPalletObj)
		thisFactory.productData.oPalletObject[iPalletSlot] = NULL
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CLEANUP_ALL_FACTORY_PALLETS(FACTORY_ID eFactoryID)
	
	INT iPalletSlot
	FOR iPalletSlot = 0 TO GET_FACTORY_PALLET_TOTAL(eFactoryID)-1
		CLEANUP_FACTORY_PALLET(iPalletSlot)
	ENDFOR
	
ENDPROC

FUNC BOOL CLEANUP_FACTORY_PRODUCTION_PROP(INT iPropID)
	
	OBJECT_INDEX oPropObj = thisFactory.propData.oProductionProp[iPropID]
	
	IF DOES_ENTITY_EXIST(oPropObj)
	AND NETWORK_HAS_CONTROL_OF_ENTITY(oPropObj)
		DELETE_OBJECT(oPropObj)
		thisFactory.propData.oProductionProp[iPropID] = NULL
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CLEANUP_FACTORY_PRODUCT(INT iProductSlot, BOOL bUpdateProductSlot = TRUE, BOOL bTutorial = FALSE)
	
	OBJECT_INDEX oProductObj
	IF bTutorial
		oProductObj = thisFactory.tutPropData.oTUTProductObject[iProductSlot]
	ELSE
		oProductObj = thisFactory.productData.oProductObject[iProductSlot]
	ENDIF
	
	IF DOES_ENTITY_EXIST(oProductObj)
	AND NETWORK_HAS_CONTROL_OF_ENTITY(oProductObj)
		DELETE_OBJECT(oProductObj)
		IF bTutorial
			thisFactory.tutPropData.oTUTProductObject[iProductSlot] = NULL
		ELSE
			thisFactory.productData.oProductObject[iProductSlot] = NULL
		ENDIF
		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			IF bTutorial
				IF bUpdateProductSlot
					serverBD.iTUTProductSlot--
				ENDIF
			ENDIF
		ENDIF
		
		IF bUpdateProductSlot
			thisFactory.iProductSlot--
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CLEANUP_ALL_FACTORY_PRODUCTS(FACTORY_ID iBikerWarehouseID)
	
	INT iProductSlot
	FOR iProductSlot = 0 TO GET_FACTORY_PRODUCT_CAPACITY(iBikerWarehouseID)
		CLEANUP_FACTORY_PRODUCT(iProductSlot)
	ENDFOR
	
ENDPROC

PROC CLEANUP_ALL_TUTORIAL_FACTORY_PRODUCTS(FACTORY_ID iBikerWarehouseID)
	
	INT iProductSlot
	FOR iProductSlot = 0 TO GET_FACTORY_PRODUCT_CAPACITY(iBikerWarehouseID)
		CLEANUP_FACTORY_PRODUCT(iProductSlot, DEFAULT, TRUE)
	ENDFOR
	
ENDPROC

FUNC BOOL HAS_FACTORY_PRODUCT_FADED_IN(INT iProductSlot)
	IF thisFactory.productData.iProductFadeComplete[iProductSlot] = 1
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_FACTORY_PRODUCT_FADED_OUT(INT iProductSlot)
	IF thisFactory.productData.iProductFadeComplete[iProductSlot] = 0
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_FACTORY_PRODUCT_CURRENTLY_FADING(INT iProductSlot, BOOL bTutorial = FALSE)
	
	IF IS_FACTORY_PRODUCT_SLOT_VALID(thisFactory.eID, iProductSlot)
		IF NOT IS_FACTORY_SLOT_AVALIABLE(iProductSlot, bTutorial)
			INT iAlpha
			IF bTutorial
				iAlpha = GET_ENTITY_ALPHA(thisFactory.tutPropData.oTUTProductObject[iProductSlot])
			ELSE
				iAlpha = GET_ENTITY_ALPHA(thisFactory.productData.oProductObject[iProductSlot])
			ENDIF
			IF iAlpha > 0 AND iAlpha < 255
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
		
	RETURN FALSE
ENDFUNC

PROC HIDE_ALL_FACTORY_PRODUCT()
	
	INT iProductSlot
	FOR iProductSlot = 0 TO GET_FACTORY_PRODUCT_CAPACITY(thisFactory.eID)
		IF DOES_ENTITY_EXIST(thisFactory.productData.oProductObject[iProductSlot])
			SET_ENTITY_COORDS(thisFactory.productData.oProductObject[iProductSlot], GET_ENTITY_COORDS(thisFactory.productData.oProductObject[iProductSlot]) - <<0,0,-30.0>>)
		ENDIF
	ENDFOR
	
ENDPROC

PROC SHOW_ALL_FACTORY_PRODUCT()
	
	INT iProductSlot
	FOR iProductSlot = 0 TO GET_FACTORY_PRODUCT_CAPACITY(thisFactory.eID)
		IF DOES_ENTITY_EXIST(thisFactory.productData.oProductObject[iProductSlot])
			SET_ENTITY_COORDS(thisFactory.productData.oProductObject[iProductSlot], GET_FACTORY_PRODUCT_COORDS(thisFactory.pOwner, thisFactory.eID, iProductSlot))
		ENDIF
	ENDFOR
	
ENDPROC

PROC SET_FACTORY_PRODUCT_FADE_TIMER(INT iProductSlot)
	IF thisFactory.productData.iSetProductFadeTimer[iProductSlot] = 0
		thisFactory.productData.tdProductFadeTimer[iProductSlot] = GET_TIME_OFFSET(GET_NETWORK_TIME(), ciBIKER_PRODUCT_FADE_TIME)
		thisFactory.productData.iSetProductFadeTimer[iProductSlot] = 1
	ENDIF
ENDPROC

PROC PERFORM_FACTORY_PRODUCT_FADE(INT iProductSlot, BOOL bFadeIn, BOOL bUpdateProductSlot = TRUE)
	
	SET_FACTORY_PRODUCT_FADE_TIMER(iProductSlot)
	
	IF IS_TIME_LESS_THAN(GET_NETWORK_TIME(), thisFactory.productData.tdProductFadeTimer[iProductSlot])
		OBJECT_INDEX oProductObject = thisFactory.productData.oProductObject[iProductSlot]
		IF DOES_ENTITY_EXIST(oProductObject)
			
			INT iTimeDifference = ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), thisFactory.productData.tdProductFadeTimer[iProductSlot]))
			INT iAlpha = ROUND((TO_FLOAT(iTimeDifference) / ciBIKER_PRODUCT_FADE_TIME) * 255)
			
			IF bFadeIn
				iAlpha = (255 - (ROUND((TO_FLOAT(iTimeDifference) / ciBIKER_PRODUCT_FADE_TIME) * 255)))
			ENDIF
			
			SET_ENTITY_ALPHA(oProductObject, iAlpha, FALSE)
			
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_PROPERTY, "PERFORM_FACTORY_PRODUCT_FADE - Product Slot: ", iProductSlot, " Alpha: ", iAlpha, " Time Difference: ", iTimeDifference)
			#ENDIF
		ENDIF
		
	ELSE
		
		OBJECT_INDEX oProductObject = thisFactory.productData.oProductObject[iProductSlot]
		IF DOES_ENTITY_EXIST(oProductObject)
			IF bFadeIn
				
				SET_ENTITY_ALPHA(oProductObject, 255, FALSE)
				IF IS_MODEL_VALID(GET_ENTITY_MODEL(oProductObject))
					SET_MODEL_AS_NO_LONGER_NEEDED(GET_ENTITY_MODEL(oProductObject))
				ENDIF
				
				thisFactory.productData.iProductFadeComplete[iProductSlot] = 1
				
				#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_PROPERTY, "PERFORM_FACTORY_PRODUCT_FADE - Product Slot: ", iProductSlot, " Has Fully Faded In")
				#ENDIF
				
				IF bUpdateProductSlot
					thisFactory.iProductSlot++
				ENDIF
				
			ELSE
				
				SET_ENTITY_ALPHA(oProductObject, 0, FALSE)
				IF NETWORK_HAS_CONTROL_OF_ENTITY(oProductObject)
					CLEANUP_FACTORY_PRODUCT(iProductSlot, bUpdateProductSlot)
				ENDIF
				
				thisFactory.productData.iProductFadeComplete[iProductSlot] = 0
				
				#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_PROPERTY, "PERFORM_FACTORY_PRODUCT_FADE - Product Slot: ", iProductSlot, " Has Fully Faded Out & Been Deleted")
				#ENDIF
			ENDIF
		ENDIF
		
		thisFactory.productData.iSetProductFadeTimer[iProductSlot] = 0
		
	ENDIF
ENDPROC

FUNC BOOL DOES_FACTORY_PRODUCT_TOTAL_NEED_UPDATING(BOOL bTutorial = FALSE)
	BOOL bUpdate = FALSE
	IF bTutorial
		IF serverBD.iTUTProductSlot != serverBD.ITUTProductTotal
			bUpdate = TRUE
		ENDIF
	ELSE
		IF thisFactory.iProductSlot != GET_PRODUCT_TOTAL_FOR_FACTORY(thisFactory.pOwner, thisFactory.eID)
			bUpdate = TRUE
		ENDIF
	ENDIF
	RETURN bUpdate
ENDFUNC

FUNC BOOL HAS_FACTORY_PRODUCT_TOTAL_INCREASED(BOOL bTutorial = FALSE)
	BOOL bIncreased = FALSE
	IF bTutorial
		IF serverBD.iTUTProductSlot < serverBD.ITUTProductTotal
			bIncreased = TRUE
		ENDIF
	ELSE
		IF (thisFactory.iProductSlot < GET_PRODUCT_TOTAL_FOR_FACTORY(thisFactory.pOwner, thisFactory.eID))
			bIncreased = TRUE
		ENDIF
	ENDIF
	RETURN bIncreased
ENDFUNC

FUNC BOOL HAS_FACTORY_PRODUCT_TOTAL_DECREASED(BOOL bTutorial = FALSE)
	BOOL bDecreased = FALSE
	IF bTutorial
		IF serverBD.iTUTProductSlot > serverBD.ITUTProductTotal
			bDecreased = TRUE
		ENDIF
	ELSE
		IF (thisFactory.iProductSlot > GET_PRODUCT_TOTAL_FOR_FACTORY(thisFactory.pOwner, thisFactory.eID))
			bDecreased = TRUE
		ENDIF
	ENDIF
	RETURN bDecreased
ENDFUNC

/*
PROC SET_FACTORY_TUTORIAL_PRODUCT_FADE_TIMER(INT iCurrentProductSlot, INT iTotalProductToChange, INT iTotalFadeTime, BOOL bFadeIn)
	//IF thisFactory.tutPropData.iTUTSetProductFadeTimer[iCurrentProductSlot] = 0
		IF bFadeIn
			thisFactory.tutPropData.iTUTIndividualProductFadeTime = iTotalFadeTime/iTotalProductToChange
		ELSE
			IF thisFactory.tutPropData.iTUTIndividualProductFadeTime = 0
				INT iAmountToReduce = serverBD.iTUTProductSlot - serverBD.ITUTProductTotal
				thisFactory.tutPropData.iTUTIndividualProductFadeTime = iTotalFadeTime/iAmountToReduce
			ENDIF
		ENDIF
		thisFactory.tutPropData.tdTUTProductFadeTimer[iCurrentProductSlot] = GET_TIME_OFFSET(GET_NETWORK_TIME(), thisFactory.tutPropData.iTUTIndividualProductFadeTime)
		//thisFactory.tutPropData.iTUTSetProductFadeTimer[iCurrentProductSlot] = 1
	//ENDIF
ENDPROC

PROC PERFORM_FACTORY_TUTORIAL_PRODUCT_SPAWNING(INT iProductAmount, INT iFadeTimeMS)
	serverBD.ITUTProductTotal 		= iProductAmount
	serverBD.ITUTProductFadeTime 	= iFadeTimeMS
	
	BOOL bFadeIn = TRUE
	IF DOES_FACTORY_PRODUCT_TOTAL_NEED_UPDATING(TRUE)
		IF HAS_FACTORY_PRODUCT_TOTAL_INCREASED(TRUE)
			bFadeIn = TRUE
		ELIF HAS_FACTORY_PRODUCT_TOTAL_DECREASED(TRUE)
			bFadeIn = FALSE
		ENDIF
	ENDIF
	
	SET_FACTORY_TUTORIAL_PRODUCT_FADE_TIMER(serverBD.iTUTProductSlot+1, serverBD.ITUTProductTotal, serverBD.ITUTProductFadeTime, bFadeIn)
ENDPROC

PROC PERFORM_FACTORY_TUTORIAL_PRODUCT_FADE(INT iProductSlot, BOOL bFadeIn)
	
	IF IS_TIME_LESS_THAN(GET_NETWORK_TIME(), thisFactory.tutPropData.tdTUTProductFadeTimer[iProductSlot])
		OBJECT_INDEX oProductObject = thisFactory.tutPropData.oTUTProductObject[iProductSlot]
		IF DOES_ENTITY_EXIST(oProductObject)
			
			INT iTimeDifference = ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), thisFactory.tutPropData.tdTUTProductFadeTimer[iProductSlot]))
			INT iAlpha = ROUND((TO_FLOAT(iTimeDifference) / thisFactory.tutPropData.iTUTIndividualProductFadeTime) * 255)
			
			IF bFadeIn
				iAlpha = (255 - (ROUND((TO_FLOAT(iTimeDifference) / thisFactory.tutPropData.iTUTIndividualProductFadeTime) * 255)))
			ENDIF
			
			SET_ENTITY_ALPHA(oProductObject, iAlpha, FALSE)
			
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_PROPERTY, "PERFORM_FACTORY_TUTORIAL_PRODUCT_FADE - Product Slot: ", iProductSlot, " Alpha: ", iAlpha, " Time Difference: ", iTimeDifference)
			#ENDIF
		ENDIF
		
	ELSE
		
		OBJECT_INDEX oProductObject = thisFactory.tutPropData.oTUTProductObject[iProductSlot]
		IF DOES_ENTITY_EXIST(oProductObject)
			IF bFadeIn
				
				SET_ENTITY_ALPHA(oProductObject, 255, FALSE)
				IF IS_MODEL_VALID(GET_ENTITY_MODEL(oProductObject))
					SET_MODEL_AS_NO_LONGER_NEEDED(GET_ENTITY_MODEL(oProductObject))
				ENDIF
				
				#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_PROPERTY, "PERFORM_FACTORY_TUTORIAL_PRODUCT_FADE - Product Slot: ", iProductSlot, " Has Fully Faded In")
				#ENDIF
				
				IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
					serverBD.iTUTProductSlot++
				ENDIF
				
			ELSE
				
				SET_ENTITY_ALPHA(oProductObject, 0, FALSE)
				IF NETWORK_HAS_CONTROL_OF_ENTITY(oProductObject)
					CLEANUP_FACTORY_PRODUCT(iProductSlot, TRUE, TRUE)
				ENDIF
				
				#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_PROPERTY, "PERFORM_FACTORY_TUTORIAL_PRODUCT_FADE - Product Slot: ", iProductSlot, " Has Fully Faded Out & Been Deleted")
				#ENDIF
			ENDIF
		ENDIF
		
		IF serverBD.iTUTProductSlot != serverBD.ITUTProductTotal
			PERFORM_FACTORY_TUTORIAL_PRODUCT_SPAWNING(serverBD.ITUTProductTotal, serverBD.ITUTProductFadeTime)
		ELSE
			thisFactory.tutPropData.iTUTIndividualProductFadeTime = 0
		ENDIF
	ENDIF
ENDPROC
*/

FUNC BOOL IS_ANY_PLAYER_BLOCKING_FACTORY_PRODUCT_COORDS(FACTORY_ID eFactoryID, INT iProductSlot)

	VECTOR vProductCoords = GET_FACTORY_PRODUCT_COORDS(thisFactory.pOwner, eFactoryID, iProductSlot)
	
	INT iParticipant
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
		    PLAYER_INDEX playerID = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
		    IF IS_NET_PLAYER_OK(playerID)
				IF ARE_VECTORS_ALMOST_EQUAL(GET_PLAYER_COORDS(playerID), vProductCoords, 1.25)
					RETURN TRUE
				ENDIF
			ENDIF
	    ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

PROC SET_FACTORY_PRODUCTION_PROP_FADE_TIMER(INT iPropID)
	IF thisFactory.propData.iSetPropFadeTimer[iPropID] = 0
		thisFactory.propData.tdPropFadeTimer[iPropID] = GET_TIME_OFFSET(GET_NETWORK_TIME(), ciBIKER_PRODUCT_FADE_TIME)
		thisFactory.propData.iSetPropFadeTimer[iPropID] = 1
	ENDIF
ENDPROC

PROC PERFORM_FACTORY_PRODUCTION_PROP_FADE(INT iPropID, BOOL bFadeIn)
	
	SET_FACTORY_PRODUCTION_PROP_FADE_TIMER(iPropID)
	
	IF IS_TIME_LESS_THAN(GET_NETWORK_TIME(), thisFactory.propData.tdPropFadeTimer[iPropID])
		OBJECT_INDEX oPropObject = thisFactory.propData.oProductionProp[iPropID]
		IF DOES_ENTITY_EXIST(oPropObject)
			
			INT iTimeDifference = ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), thisFactory.propData.tdPropFadeTimer[iPropID]))
			INT iAlpha = ROUND((TO_FLOAT(iTimeDifference) / ciBIKER_PRODUCT_FADE_TIME) * 255)
			
			IF bFadeIn
				iAlpha = (255 - (ROUND((TO_FLOAT(iTimeDifference) / ciBIKER_PRODUCT_FADE_TIME) * 255)))
			ENDIF
			
			SET_ENTITY_ALPHA(oPropObject, iAlpha, FALSE)
			
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_PROPERTY, "PERFORM_FACTORY_PRODUCTION_PROP_FADE - Prop ID: ", iPropID, " Alpha: ", iAlpha, " Time Difference: ", iTimeDifference)
			#ENDIF
		ENDIF
		
	ELSE
		
		OBJECT_INDEX oPropObject = thisFactory.propData.oProductionProp[iPropID]
		IF DOES_ENTITY_EXIST(oPropObject)
			IF bFadeIn
				
				SET_ENTITY_ALPHA(oPropObject, 255, FALSE)
				IF IS_MODEL_VALID(GET_ENTITY_MODEL(oPropObject))
					SET_MODEL_AS_NO_LONGER_NEEDED(GET_ENTITY_MODEL(oPropObject))
				ENDIF
				
				thisFactory.propData.iPropFadeComplete[iPropID] = 1
				
				#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_PROPERTY, "PERFORM_FACTORY_PRODUCTION_PROP_FADE - Prop ID: ", iPropID, " Has Fully Faded In")
				#ENDIF
				
			ELSE
				
				SET_ENTITY_ALPHA(oPropObject, 0, FALSE)
				IF NETWORK_HAS_CONTROL_OF_ENTITY(oPropObject)
					CLEANUP_FACTORY_PRODUCTION_PROP(iPropID)
				ENDIF
				
				thisFactory.propData.iPropFadeComplete[iPropID] = 0
				
				#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_PROPERTY, "PERFORM_FACTORY_PRODUCTION_PROP_FADE - Prop ID: ", iPropID, " Has Fully Faded Out & Been Deleted")
				#ENDIF
			ENDIF
		ENDIF
		
		thisFactory.propData.iSetPropFadeTimer[iPropID] = 0
		
	ENDIF
ENDPROC

FUNC BOOL DOES_FACTORY_MATERIALS_TOTAL_NEED_UPDATING()
	IF serverBD.iMaterialsTotal != GET_MATERIALS_TOTAL_FOR_FACTORY(thisFactory.pOwner, thisFactory.eID)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_FACTORY_MATERIALS_TOTAL_INCREASED()
	RETURN (serverBD.iMaterialsTotal < GET_MATERIALS_TOTAL_FOR_FACTORY(thisFactory.pOwner, thisFactory.eID))
ENDFUNC

FUNC BOOL HAS_FACTORY_MATERIALS_TOTAL_DECREASED()
	RETURN (serverBD.iMaterialsTotal > GET_MATERIALS_TOTAL_FOR_FACTORY(thisFactory.pOwner, thisFactory.eID))
ENDFUNC

/// PURPOSE: If we need to remove previous products. E.g weed packets into a weed bale
PROC PERFORM_FACTORY_PRODUCT_SORTING(FACTORY_ID eFactoryID, INT iProductSlot, BOOL bAddingProduct = TRUE, BOOL bPerformFade = TRUE)
	
	INT iProductIter
	INT iProductCounter
	
	SWITCH thisFactory.eFactoryType
		CASE FACTORY_TYPE_WEED
			IF iProductSlot = 5  OR iProductSlot = 10
			OR iProductSlot = 15 OR iProductSlot = 20
			OR iProductSlot = 25 OR iProductSlot = 30
			OR iProductSlot = 35 OR iProductSlot = 40
				iProductCounter = (iProductSlot-GET_FACTORY_PALLET_CAPACITY(eFactoryID))
				FOR iProductIter = iProductCounter TO iProductSlot-1
					IF IS_FACTORY_PRODUCT_SLOT_VALID(thisFactory.eID, iProductIter)
						IF bAddingProduct
							// FADE PREVIOUS PACKETS OUT
							IF bPerformFade
								PERFORM_FACTORY_PRODUCT_FADE(iProductIter, FALSE, FALSE)
							ELSE
								CLEANUP_FACTORY_PRODUCT(iProductIter, FALSE)
							ENDIF
						ELSE
							// FADE PREVIOUS PACKETS IN
							IF NOT IS_ANY_PLAYER_BLOCKING_FACTORY_PRODUCT_COORDS(thisFactory.eID, iProductSlot)
								CREATE_FACTORY_PRODUCT(thisFactory.eID, iProductIter)
								IF NOT IS_FACTORY_SLOT_AVALIABLE(iProductIter)
									PERFORM_FACTORY_PRODUCT_FADE(iProductIter, TRUE, FALSE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDFOR
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC

PROC ADD_FACTORY_PRODUCT()
	
	IF NOT IS_FACTORY_PRODUCT_CURRENTLY_FADING(thisFactory.iProductSlot)
		IF IS_FACTORY_PRODUCT_SLOT_VALID(thisFactory.eID, thisFactory.iProductSlot+1)
			IF NOT IS_ANY_PLAYER_BLOCKING_FACTORY_PRODUCT_COORDS(thisFactory.eID, thisFactory.iProductSlot+1)
				CREATE_FACTORY_PRODUCT(thisFactory.eID, thisFactory.iProductSlot+1)
				
				IF NOT IS_FACTORY_SLOT_AVALIABLE(thisFactory.iProductSlot+1)
					//PERFORM_FACTORY_PRODUCT_SORTING(thisFactory.eID, thisFactory.iProductSlot+1)
					PERFORM_FACTORY_PRODUCT_FADE(thisFactory.iProductSlot+1, TRUE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC REMOVE_FACTORY_PRODUCT()
	
	IF NOT IS_FACTORY_PRODUCT_CURRENTLY_FADING(thisFactory.iProductSlot+1)
		IF IS_FACTORY_PRODUCT_SLOT_VALID(thisFactory.eID, thisFactory.iProductSlot)
			//PERFORM_FACTORY_PRODUCT_SORTING(thisFactory.eID, thisFactory.iProductSlot, FALSE)
			PERFORM_FACTORY_PRODUCT_FADE(thisFactory.iProductSlot, FALSE)
		ENDIF
	ENDIF
	
ENDPROC
/*
PROC ADD_FACTORY_TUTORIAL_PRODUCT()
	
	IF NOT IS_FACTORY_PRODUCT_CURRENTLY_FADING(serverBD.iTUTProductSlot, TRUE)
		IF IS_FACTORY_PRODUCT_SLOT_VALID(thisFactory.eID, serverBD.iTUTProductSlot+1)
			IF NOT IS_ANY_PLAYER_BLOCKING_FACTORY_PRODUCT_COORDS(thisFactory.eID, serverBD.iTUTProductSlot+1)
				IF IS_FACTORY_SLOT_AVALIABLE(serverBD.iTUTProductSlot+1, TRUE)
					CREATE_FACTORY_PRODUCT(thisFactory.eID, serverBD.iTUTProductSlot+1, DEFAULT, TRUE)
				ENDIF
				
				IF NOT IS_FACTORY_SLOT_AVALIABLE(serverBD.iTUTProductSlot+1, TRUE)
					//PERFORM_FACTORY_PRODUCT_SORTING(thisFactory.eID, serverBD.iTUTProductSlot+1)
					PERFORM_FACTORY_TUTORIAL_PRODUCT_FADE(serverBD.iTUTProductSlot+1, TRUE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC REMOVE_FACTORY_TUTORIAL_PRODUCT()
	
	IF NOT IS_FACTORY_PRODUCT_CURRENTLY_FADING(serverBD.iTUTProductSlot+1, TRUE)
		IF IS_FACTORY_PRODUCT_SLOT_VALID(thisFactory.eID, serverBD.iTUTProductSlot)
			//PERFORM_FACTORY_PRODUCT_SORTING(thisFactory.eID, serverBD.iTUTProductSlot, FALSE)
			PERFORM_FACTORY_TUTORIAL_PRODUCT_FADE(serverBD.iTUTProductSlot, FALSE)
		ENDIF
	ENDIF
	
ENDPROC
*/
PROC CREATE_ALL_OWNED_FACTORY_PRODUCT(FACTORY_ID eFactoryID, INT iProductTotal)
	
	INT iProductSlot
	FOR iProductSlot = 0 TO iProductTotal
		IF IS_FACTORY_PRODUCT_SLOT_VALID(eFactoryID, iProductSlot)
			CREATE_FACTORY_PRODUCT(eFactoryID, iProductSlot, FALSE)
			
			IF NOT IS_FACTORY_SLOT_AVALIABLE(iProductSlot)
				//PERFORM_FACTORY_PRODUCT_SORTING(eFactoryID, iProductSlot, TRUE, FALSE)
			ENDIF
		ENDIF
	ENDFOR
	
ENDPROC

/// PURPOSE: Each factory has scripted props placed inside. These are not the final product.
///          E.g Painkiller, baking soda etc. placed inside the Coke factory.
PROC INITIALISE_FACTORY_PRODUCTION_PROPS()
	
	SWITCH thisFactory.eFactoryType
		CASE FACTORY_TYPE_WEED
			IF serverBD.bActiveState
				INT iIter
				REPEAT (ciMAX_FACTORY_PRODUCTION_PROPS-1) iIter
					
					INT iProductionTime
					INT iUIBarPercent
					INT iRemainingUIBarPercent
					INT iFinalTime
					
					iProductionTime = (GET_FACTORY_TOTAL_PRODUCTION_TIME(thisFactory.eID, FALSE) / (GET_FACTORY_TIME_STAGE(PLAYER_ID(), thisFactory.eID)+1))
					iUIBarPercent = (iProductionTime/ciMAX_FACTORY_UI_BARS)*(iIter+1)
					
					iRemainingUIBarPercent = iProductionTime-iUIBarPercent
					iFinalTime = GET_FACTORY_PRODUCTION_TIME(thisFactory.pOwner, thisFactory.eID)-iRemainingUIBarPercent
					
					IF GET_GAME_TIMER() >= iFinalTime
						IF iIter <= 3
							CREATE_FACTORY_PRODUCTION_PROP(thisFactory.eID, iIter, FALSE)
						ENDIF
					ENDIF
					
				ENDREPEAT
			ENDIF
		BREAK
		CASE FACTORY_TYPE_METH
			IF GET_FACTORY_PRODUCTION_STAGE(thisFactory.pOwner, thisFactory.eID) > 1
				INT iTrayIter
				REPEAT (ciMAX_FACTORY_METH_TRAY_SLOTS-1) iTrayIter
					INT iTraySlot
					iTraySlot = GET_RANDOM_INT_IN_RANGE(0, ciMAX_FACTORY_METH_TRAY_SLOTS)
					IF IS_FACTORY_PRODUCTION_PROP_SLOT_AVALIABLE(iTraySlot)
						#IF IS_DEBUG_BUILD
							PRINTLN("AM_MP_BIKER_WAREHOUSE - INITIALISE_FACTORY_PRODUCTION_PROPS - Creating a meth tray in slot: ", iTraySlot)
						#ENDIF
						CREATE_FACTORY_PRODUCTION_PROP(thisFactory.eID, iTraySlot, FALSE)
					ENDIF
				ENDREPEAT
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC

PROC MAINTAIN_FACTORY_PRODUCTION_PROPS()
	
	SWITCH thisFactory.eFactoryType
		CASE FACTORY_TYPE_WEED
			
			INT iFinalTime
			INT iUIBarPercent
			INT iProductionTime
			INT iRemainingUIBarPercent
			
			REPEAT (ciMAX_FACTORY_PRODUCTION_PROPS-1) thisFactory.propData.iPropIter
			
				iProductionTime = (GET_FACTORY_TOTAL_PRODUCTION_TIME(thisFactory.eID, FALSE) / (GET_FACTORY_TIME_STAGE(PLAYER_ID(), thisFactory.eID)+1))
				iUIBarPercent = (iProductionTime/ciMAX_FACTORY_UI_BARS)*(thisFactory.propData.iPropIter+1)
			
				iRemainingUIBarPercent = iProductionTime-iUIBarPercent
				iFinalTime = GET_FACTORY_PRODUCTION_TIME(thisFactory.pOwner, thisFactory.eID)-iRemainingUIBarPercent
				
				IF GET_GAME_TIMER() >= iFinalTime
					IF CREATE_FACTORY_PRODUCTION_PROP(thisFactory.eID, thisFactory.propData.iPropIter)
						PERFORM_FACTORY_PRODUCTION_PROP_FADE(thisFactory.propData.iPropIter, TRUE)
					ENDIF
				ELIF GET_GAME_TIMER() >= GET_FACTORY_PRODUCTION_TIME(thisFactory.pOwner, thisFactory.eID)
					
					INT iIter
					REPEAT (ciMAX_FACTORY_PRODUCTION_PROPS-1) iIter
						PERFORM_FACTORY_PRODUCTION_PROP_FADE(iIter, FALSE)
					ENDREPEAT
				ENDIF
			ENDREPEAT
			
		BREAK
		
		CASE FACTORY_TYPE_CRACK
			
			IF DOES_FACTORY_MATERIALS_TOTAL_NEED_UPDATING()
			
//				iTotalProps = (GET_MATERIALS_TOTAL_FOR_FACTORY(thisFactory.pOwner, thisFactory.eID)/100)*ciMAX_FACTORY_UI_BARS  // Spawn coke blocks based on materials percentage
//				iTotalProps = ROUND_DOWN_TO_NEAREST(iTotalProps, 1)+3 															// Three existing props before coke blocks
//				
//				IF HAS_FACTORY_MATERIALS_TOTAL_INCREASED()
//					
//					INT iIter
//					REPEAT iTotalProps iIter
//						IF iIter > 2
//							CREATE_FACTORY_PRODUCTION_PROP(thisFactory.eID, iIter)
//						ELSE
//							CREATE_FACTORY_PRODUCTION_PROP(thisFactory.eID, iIter, FALSE)
//						ENDIF
//					ENDREPEAT
//				
//				ELIF HAS_FACTORY_MATERIALS_TOTAL_DECREASED()
//					
//					INT iIter
//					FOR iIter = iTotalProps TO ciMAX_FACTORY_PRODUCTION_PROPS-1
//						PERFORM_FACTORY_PRODUCTION_PROP_FADE(thisFactory.iProductSlot, FALSE)
//					ENDFOR
//					
//				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC

#IF IS_DEBUG_BUILD
PROC DEBUG_ACTIVATE_WEED_STAGE()

	IF thisFactory.pOwner != INVALID_PLAYER_INDEX()
	AND thisFactory.eFactoryType = FACTORY_TYPE_WEED
		SWITCH GlobalplayerBD_FM[NATIVE_TO_INT(thisFactory.pOwner)].iDebugWeedFactoryEntitySetStage
			CASE 1
				IF DOES_PLAYER_OWN_FACTORY_UPGRADE(thisFactory.pOwner, thisFactory.eID, UPGRADE_ID_EQUIPMENT)
					ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "weed_upgrade_equip")
					DEACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_standard_equip")
				ELSE
					ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_standard_equip")
					DEACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "weed_upgrade_equip")
				ENDIF
				IF DOES_PLAYER_OWN_FACTORY_UPGRADE(thisFactory.pOwner, thisFactory.eID, UPGRADE_ID_SECURITY)
					ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "weed_security_upgrade")
				ELSE
					DEACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "weed_security_upgrade")
				ENDIF
				
				ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_set_up")
				ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_production")
				ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthA_stage1")
				ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthB_stage1")
				ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthC_stage1")
				ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthD_stage1")
				ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthE_stage1")
				ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthF_stage1")
				ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthG_stage1")
				ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthH_stage1")
				ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthI_stage1")
				ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_hosea")
				ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_hoseb")
				ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_hosec")
				ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_hosed")
				ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_hosee")
				ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_hosef")
				ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_hoseg")
				ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_hoseh")
				ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_hosei")
				
				DEACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthA_stage2")
				DEACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthB_stage2")
				DEACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthC_stage2")
				DEACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthD_stage2")
				DEACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthE_stage2")
				DEACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthF_stage2")
				DEACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthG_stage2")
				DEACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthH_stage2")
				DEACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthI_stage2")
				
				DEACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthA_stage3")
				DEACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthB_stage3")
				DEACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthC_stage3")
				DEACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthD_stage3")
				DEACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthE_stage3")
				DEACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthF_stage3")
				DEACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthG_stage3")
				DEACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthH_stage3")
				DEACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthI_stage3")
				
				PRINTLN("DEBUG_ACTIVATE_WEED_STAGE() - Overriding to stage 1")
				
				EXIT
			BREAK
			CASE 2
				IF DOES_PLAYER_OWN_FACTORY_UPGRADE(thisFactory.pOwner, thisFactory.eID, UPGRADE_ID_EQUIPMENT)
					ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "weed_upgrade_equip")
					DEACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_standard_equip")
				ELSE
					ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_standard_equip")
					DEACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "weed_upgrade_equip")
				ENDIF
				IF DOES_PLAYER_OWN_FACTORY_UPGRADE(thisFactory.pOwner, thisFactory.eID, UPGRADE_ID_SECURITY)
					ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "weed_security_upgrade")
				ELSE
					DEACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "weed_security_upgrade")
				ENDIF
				ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_set_up")
				ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_production")
				ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthA_stage2")
				ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthB_stage2")
				ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthC_stage2")
				ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthD_stage2")
				ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthE_stage2")
				ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthF_stage2")
				ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthG_stage2")
				ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthH_stage2")
				ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthI_stage2")
				ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_hosea")
				ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_hoseb")
				ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_hosec")
				ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_hosed")
				ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_hosee")
				ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_hosef")
				ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_hoseg")
				ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_hoseh")
				ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_hosei")
				ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_drying")
				
				DEACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthA_stage1")
				DEACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthB_stage1")
				DEACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthC_stage1")
				DEACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthD_stage1")
				DEACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthE_stage1")
				DEACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthF_stage1")
				DEACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthG_stage1")
				DEACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthH_stage1")
				DEACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthI_stage1")
				
				DEACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthA_stage3")
				DEACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthB_stage3")
				DEACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthC_stage3")
				DEACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthD_stage3")
				DEACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthE_stage3")
				DEACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthF_stage3")
				DEACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthG_stage3")
				DEACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthH_stage3")
				DEACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthI_stage3")
				
				PRINTLN("DEBUG_ACTIVATE_WEED_STAGE() - Overriding to stage 2")
				EXIT
			BREAK
			CASE 3
				IF DOES_PLAYER_OWN_FACTORY_UPGRADE(thisFactory.pOwner, thisFactory.eID, UPGRADE_ID_EQUIPMENT)
					ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "weed_upgrade_equip")
					DEACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_standard_equip")
				ELSE
					ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_standard_equip")
					DEACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "weed_upgrade_equip")
				ENDIF
				IF DOES_PLAYER_OWN_FACTORY_UPGRADE(thisFactory.pOwner, thisFactory.eID, UPGRADE_ID_SECURITY)
					ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "weed_security_upgrade")
				ELSE
					DEACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "weed_security_upgrade")
				ENDIF
				ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_set_up")
				ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_production")
				ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_hosea")
				ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_hoseb")
				ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_hosec")
				ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_hosed")
				ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_hosee")
				ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_hosef")
				ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_hoseg")
				ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_hoseh")
				ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_hosei")
				ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_drying")
				ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthA_stage3")
				ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthB_stage3")
				ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthC_stage3")
				ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthD_stage3")
				ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthE_stage3")
				ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthF_stage3")
				ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthG_stage3")
				ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthH_stage3")
				ACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthI_stage3")
				
				DEACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthA_stage1")
				DEACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthB_stage1")
				DEACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthC_stage1")
				DEACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthD_stage1")
				DEACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthE_stage1")
				DEACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthF_stage1")
				DEACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthG_stage1")
				DEACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthH_stage1")
				DEACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthI_stage1")
				
				DEACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthA_stage2")
				DEACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthB_stage2")
				DEACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthC_stage2")
				DEACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthD_stage2")
				DEACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthE_stage2")
				DEACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthF_stage2")
				DEACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthG_stage2")
				DEACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthH_stage2")
				DEACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, "Weed_growthI_stage2")
				
				PRINTLN("DEBUG_ACTIVATE_WEED_STAGE() - Overriding to stage 3")
				EXIT
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC
#ENDIF

PROC INITIALISE_FACTORY_ENTITY_SETS(INT iForceStage = -1)
	
	thisFactory.FactoryInteriorID = GET_INTERIOR_AT_COORDS_WITH_TYPE(GET_FACTORY_COORDS(thisFactory.eID), GET_FACTORY_INTERIOR_NAME(thisFactory.eID))
	IF IS_VALID_INTERIOR(thisFactory.FactoryInteriorID)
		
		INT iStage = serverBD.iProductionStage
		
		IF iForceStage > -1
			iStage = iForceStage
		ENDIF
		
		SWITCH thisFactory.eFactoryType
			CASE FACTORY_TYPE_WEED
				ACTIVATE_WEED_STAGE(thisFactory.pOwner, thisFactory.iSaveSlot, iStage)
				
				#IF IS_DEBUG_BUILD
					DEBUG_ACTIVATE_WEED_STAGE()
				#ENDIF
			BREAK
			CASE FACTORY_TYPE_METH
				ACTIVATE_METH_STAGE(thisFactory.pOwner, thisFactory.iSaveSlot, iStage)
			BREAK
			CASE FACTORY_TYPE_CRACK
				ACTIVATE_CRACK_STAGE(thisFactory.pOwner, thisFactory.iSaveSlot, iStage)
			BREAK
			CASE FACTORY_TYPE_FAKE_IDS
				ACTIVATE_FAKEID_STAGE(thisFactory.pOwner, thisFactory.iSaveSlot, iStage)
			BREAK
			CASE FACTORY_TYPE_FAKE_MONEY
				ACTIVATE_COUNTERFEIT_CASH_STAGE(thisFactory.pOwner, thisFactory.iSaveSlot, iStage, serverBD.bActiveState)
			BREAK
		ENDSWITCH
	ENDIF
	
ENDPROC

PROC CLEANUP_FACTORY_ENTITY_SETS()
	
	INT iESIndex
	TEXT_LABEL_63 tlESName
	
	REPEAT ciMAX_FACTORY_ENTITY_SETS iESIndex
		IF GET_ALL_FACTORY_ENTITY_SETS(thisFactory.eFactoryType, iESIndex, tlESName)
			IF IS_INTERIOR_ENTITY_SET_ACTIVE(thisFactory.FactoryInteriorID, tlESName)
				DEACTIVATE_INTERIOR_ENTITY_SET(thisFactory.FactoryInteriorID, tlESName)
			ENDIF
		ENDIF
	ENDREPEAT
	
ENDPROC

PROC INITIALISE_FACTORY_TIMECYCLE_MODIFIERS()
	
	IF HAS_PLAYER_COMPLETED_FACTORY_SETUP_MISSION(thisFactory.pOwner, thisFactory.eID)
		SWITCH thisFactory.eFactoryType
			CASE FACTORY_TYPE_WEED
				IF DOES_PLAYER_OWN_FACTORY_UPGRADE(thisFactory.pOwner, thisFactory.eID, UPGRADE_ID_EQUIPMENT)
					SET_TIMECYCLE_MODIFIER("mp_bkr_ware02_upgrade")
				ELSE
					SET_TIMECYCLE_MODIFIER("mp_bkr_ware02_standard")
				ENDIF
				IF GET_TIMECYCLE_MODIFIER_INDEX() != -1
					PUSH_TIMECYCLE_MODIFIER()
				ENDIF
				SET_BIT(thisFactory.iBS, BS_FACTORY_DATA_PUSHED_TIMECYCLE_MODIFIER)
			BREAK
			CASE FACTORY_TYPE_CRACK
				IF DOES_PLAYER_OWN_FACTORY_UPGRADE(thisFactory.pOwner, thisFactory.eID, UPGRADE_ID_EQUIPMENT)
					SET_TIMECYCLE_MODIFIER("mp_bkr_ware03_upgrade")
				ELSE
					SET_TIMECYCLE_MODIFIER("mp_bkr_ware03_basic")
				ENDIF
				IF GET_TIMECYCLE_MODIFIER_INDEX() != -1
					PUSH_TIMECYCLE_MODIFIER()
				ENDIF
				SET_BIT(thisFactory.iBS, BS_FACTORY_DATA_PUSHED_TIMECYCLE_MODIFIER)
			BREAK
		ENDSWITCH
	ENDIF
	
ENDPROC

PROC CLEANUP_FACTORY_TIMECYCLE_MODIFIERS()

	IF IS_BIT_SET(thisFactory.iBS, BS_FACTORY_DATA_CLEANED_UP_TIMECYCLE_MODIFIERS)
		EXIT
	ENDIF
	
	IF IS_BIT_SET(thisFactory.iBS, BS_FACTORY_DATA_PUSHED_TIMECYCLE_MODIFIER)
		POP_TIMECYCLE_MODIFIER()
		CLEAR_TIMECYCLE_MODIFIER()
		#IF IS_DEBUG_BUILD
			PRINTLN("AM_MP_BIKER_WAREHOUSE - CLEANUP_FACTORY_TIMECYCLE_MODIFIERS - Done.")
		#ENDIF
		CLEAR_BIT(thisFactory.iBS, BS_FACTORY_DATA_PUSHED_TIMECYCLE_MODIFIER)
		SET_BIT(thisFactory.iBS, BS_FACTORY_DATA_CLEANED_UP_TIMECYCLE_MODIFIERS)
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Timcycles have to be cleaned up before we are warped outside - as soon as screen fades out on exiting, to avoid outside looking like the inside of the factory
PROC MAINTAIN_CLEANUP_FACTORY_TIMECYCLE_MODIFIERS()
	IF NOT IS_BIT_SET(thisFactory.iBS, BS_FACTORY_DATA_CLEANED_UP_TIMECYCLE_MODIFIERS)
		IF IS_LOCAL_PLAYER_WALKING_OUT_OF_SIMPLE_INTERIOR()
			IF IS_SCREEN_FADED_OUT()
				CLEANUP_FACTORY_TIMECYCLE_MODIFIERS()
				SET_BIT(thisFactory.iBS, BS_FACTORY_DATA_CLEANED_UP_TIMECYCLE_MODIFIERS)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC INITIALISE_FACTORY_TOTALS()
	thisFactory.iProductSlot = GET_PRODUCT_TOTAL_FOR_FACTORY(thisFactory.pOwner, thisFactory.eID)
	CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_BIKER_WAREHOUSE - INITIALISE_FACTORY_TOTALS =- - - - - - - - - =")
	CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_BIKER_WAREHOUSE - INITIALISE_FACTORY_TOTALS - Factory: ", thisFactory.eID, " Product Total: ", thisFactory.iProductSlot)
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		serverBD.iMaterialsTotal = GET_MATERIALS_TOTAL_FOR_FACTORY(thisFactory.pOwner, thisFactory.eID)
		serverBD.iProductionStage = GET_PRODUCTION_STATE_FOR_FACTORY(thisFactory.pOwner, thisFactory.eID)
		serverBD.bActiveState = IS_FACTORY_PRODUCTION_ACTIVE(thisFactory.pOwner, thisFactory.iSaveSlot)
		thisFactory.iUpgradeState = FACTORY_UPGRADE_STATE_FADE_OUT
	ENDIF
	
	CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_BIKER_WAREHOUSE - INITIALISE_FACTORY_TOTALS - Factory: ", thisFactory.eID, " Materials Total: ", serverBD.iMaterialsTotal)
	CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_BIKER_WAREHOUSE - INITIALISE_FACTORY_TOTALS - Factory: ", thisFactory.eID, " Production Stage: ", serverBD.iProductionStage)
	CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_BIKER_WAREHOUSE - INITIALISE_FACTORY_TOTALS - Factory: ", thisFactory.eID, " Type: ", GET_FACTORY_TYPE_NAME(thisFactory.eFactoryType))
	CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_BIKER_WAREHOUSE - INITIALISE_FACTORY_TOTALS - Factory: ", thisFactory.eID, " Slot: ", thisFactory.iSaveSlot)
	CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_BIKER_WAREHOUSE - INITIALISE_FACTORY_TOTALS - Factory: ", thisFactory.eID, " Active State: ", serverBD.bActiveState)
	
	IF HAS_PLAYER_COMPLETED_FACTORY_SETUP_MISSION(thisFactory.pOwner, GET_FACTORY_ID_FROM_FACTORY_SLOT(thisFactory.pOwner, thisFactory.iSaveSlot))
		CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_BIKER_WAREHOUSE - INITIALISE_FACTORY_TOTALS - Factory: ", thisFactory.eID, " has completed its setup mission")
	ELSE
		CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_BIKER_WAREHOUSE - INITIALISE_FACTORY_TOTALS - Factory: ", thisFactory.eID, " has NOT completed its setup mission")
	ENDIF
	IF GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(thisFactory.pOwner, TRUE)
		CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_BIKER_WAREHOUSE - INITIALISE_FACTORY_TOTALS - Player currently on a gang boss mission")
	ELSE
		CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_BIKER_WAREHOUSE - INITIALISE_FACTORY_TOTALS - Player currently NOT on a gang boss mission")
	ENDIF
	IF g_sMPTunables.bBIKER_STOP_PRODUCTION_WHEN_NOT_MC_PRESIDENT
	AND NOT GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(thisFactory.pOwner)
		CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_BIKER_WAREHOUSE - INITIALISE_FACTORY_TOTALS - Factory: ", thisFactory.eID, " Inactive because player is not MC and bBIKER_STOP_PRODUCTION_WHEN_NOT_MC_PRESIDENT tunable on")
	ENDIF
ENDPROC

FUNC STRING GET_AMBIENT_ZONE_FOR_THIS_FACTORY()
	IF ARE_WORKERS_WORKING_IN_THIS_FACTORY(thisFactory.pOwner, thisFactory.eID, TRUE)
		BOOL bDoesOwnUpgrade = DOES_PLAYER_OWN_FACTORY_UPGRADE(thisFactory.pOwner, thisFactory.eID, UPGRADE_ID_EQUIPMENT)
		SWITCH thisFactory.eFactoryType
			CASE FACTORY_TYPE_WEED
				IF bDoesOwnUpgrade
					RETURN "AZ_DLC_BIKER_Weed_Warehouse_Upgraded"
				ELSE
					RETURN "AZ_DLC_BIKER_Weed_Warehouse_Normal"
				ENDIF
			BREAK
			CASE FACTORY_TYPE_METH
				IF bDoesOwnUpgrade
					RETURN "AZ_DLC_Biker_Meth_Warehouse_Upgraded"
				ELSE
					RETURN "AZ_DLC_Biker_Meth_Warehouse_Normal"
				ENDIF
			BREAK
			CASE FACTORY_TYPE_CRACK
				IF bDoesOwnUpgrade
					RETURN "AZ_DLC_Biker_Crack_Warehouse_Upgraded"
				ELSE
					RETURN "AZ_DLC_Biker_Crack_Warehouse_Normal"
				ENDIF
			BREAK
			CASE FACTORY_TYPE_FAKE_IDS
				IF bDoesOwnUpgrade
					RETURN "AZ_DLC_Biker_FakeID_Warehouse_Upgraded"
				ELSE
					RETURN "AZ_DLC_Biker_FakeID_Warehouse_Normal"
				ENDIF
			BREAK
			CASE FACTORY_TYPE_FAKE_MONEY
				IF bDoesOwnUpgrade
					RETURN "AZ_DLC_Biker_Cash_Warehouse_Upgraded"
				ELSE
					RETURN "AZ_DLC_Biker_Cash_Warehouse_Normal"
				ENDIF
			BREAK
		ENDSWITCH
	ELSE
		#IF IS_DEBUG_BUILD
			PRINTLN("AM_MP_BIKER_WAREHOUSE - GET_AMBIENT_ZONE_FOR_THIS_FACTORY - Nobody works in this factory.")
		#ENDIF
	ENDIF
	
	RETURN ""
ENDFUNC

FUNC STRING GET_FACTORY_AUDIO_SOUND_NAME(INT iSoundSlot)
	
	STRING sSoundName
	
	SWITCH thisFactory.eFactoryType
		CASE FACTORY_TYPE_FAKE_MONEY
			IF DOES_PLAYER_OWN_FACTORY_UPGRADE(thisFactory.pOwner, thisFactory.eID, UPGRADE_ID_EQUIPMENT)
				SWITCH iSoundSlot
					CASE 0	sSoundName = "Printer_Unit_01_Loop"	BREAK
					CASE 1	sSoundName = "Printer_Unit_02_Loop"	BREAK
					CASE 2	sSoundName = "Printer_Unit_03_Loop"	BREAK
					CASE 3	sSoundName = "Printer_Unit_04_Loop"	BREAK
					CASE 4	sSoundName = "Printer_Unit_05_Loop"	BREAK
					CASE 5	sSoundName = "Printer_Unit_06_Loop"	BREAK
					CASE 6	sSoundName = "Printer_Cutter_Loop"	BREAK
					CASE 7	sSoundName = "Printer_Cutter_Loop"	BREAK
					CASE 8	sSoundName = "Printer_Cutter_Loop"	BREAK
					CASE 9	sSoundName = "Printer_Cutter_Loop"	BREAK
					CASE 10	sSoundName = "Printer_Cutter_Loop"	BREAK
					CASE 11	sSoundName = "Printer_Cutter_Loop"	BREAK
					CASE 12	sSoundName = "Printer_Cutter_Loop"	BREAK
				ENDSWITCH
			ELSE
				SWITCH iSoundSlot
					CASE 0	sSoundName = "Printer_Unit_01_Loop"	BREAK
					CASE 1	sSoundName = "Printer_Unit_02_Loop"	BREAK
					CASE 2	sSoundName = "Printer_Unit_03_Loop"	BREAK
					CASE 3	sSoundName = "Printer_Unit_04_Loop"	BREAK
					CASE 4	sSoundName = "Printer_Cutter_Loop"	BREAK
					CASE 5	sSoundName = "Printer_Cutter_Loop"	BREAK
					CASE 6	sSoundName = "Printer_Cutter_Loop"	BREAK
					CASE 7	sSoundName = "Printer_Cutter_Loop"	BREAK
					CASE 8	sSoundName = "Printer_Cutter_Loop"	BREAK
				ENDSWITCH
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN sSoundName
ENDFUNC

FUNC STRING GET_FACTORY_AUDIO_SOUND_SET_NAME()
	
	STRING sSoundSetName
	
	SWITCH thisFactory.eFactoryType
		CASE FACTORY_TYPE_FAKE_MONEY
			IF DOES_PLAYER_OWN_FACTORY_UPGRADE(thisFactory.pOwner, thisFactory.eID, UPGRADE_ID_EQUIPMENT)
				sSoundSetName = "DLC_Biker_Money_Printer_Upgraded_Sounds"
			ELSE
				sSoundSetName = "DLC_Biker_Money_Printer_Basic_Sounds"
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN sSoundSetName
ENDFUNC

FUNC VECTOR GET_FACTORY_AUDIO_SOUND_COORDS(INT iSoundSlot)
	
	VECTOR vSoundCoords
	
	SWITCH thisFactory.eFactoryType
		CASE FACTORY_TYPE_FAKE_MONEY
			IF DOES_PLAYER_OWN_FACTORY_UPGRADE(thisFactory.pOwner, thisFactory.eID, UPGRADE_ID_EQUIPMENT)
				SWITCH iSoundSlot
					CASE 0	vSoundCoords = <<1135.2, -3198.5, -39.5>>	BREAK
					CASE 1	vSoundCoords = <<1133.6, -3198.2, -39.4>>	BREAK
					CASE 2	vSoundCoords = <<1132.2, -3198.1, -39.5>>	BREAK
					CASE 3	vSoundCoords = <<1130.7, -3198.1, -39.6>>	BREAK
					CASE 4	vSoundCoords = <<1129.3, -3197.7, -39.6>>	BREAK
					CASE 5	vSoundCoords = <<1127.8, -3198.0, -39.5>>	BREAK
					CASE 6	vSoundCoords = <<1126.9, -3198.5, -40.0>>	BREAK
					CASE 7	vSoundCoords = <<1128.5, -3198.5, -40.0>>	BREAK
					CASE 8	vSoundCoords = <<1130.0, -3198.5, -40.0>>	BREAK
					CASE 9	vSoundCoords = <<1131.5, -3198.5, -40.0>>	BREAK
					CASE 10	vSoundCoords = <<1133.0, -3198.5, -40.0>>	BREAK
					CASE 11	vSoundCoords = <<1134.4, -3198.5, -40.0>>	BREAK
					CASE 12	vSoundCoords = <<1135.9, -3198.5, -40.0>>	BREAK
				ENDSWITCH
			ELSE
				SWITCH iSoundSlot
					CASE 0	vSoundCoords = <<1132.2, -3198.2, -39.6>>	BREAK
					CASE 1	vSoundCoords = <<1130.7, -3198.2, -39.6>>	BREAK
					CASE 2	vSoundCoords = <<1129.2, -3198.2, -39.6>>	BREAK
					CASE 3	vSoundCoords = <<1127.8, -3198.2, -39.6>>	BREAK
					CASE 4	vSoundCoords = <<1126.9, -3198.2, -40.0>>	BREAK
					CASE 5	vSoundCoords = <<1128.5, -3198.4, -39.8>>	BREAK
					CASE 6	vSoundCoords = <<1130.0, -3198.4, -39.8>>	BREAK
					CASE 7	vSoundCoords = <<1131.5, -3198.4, -39.8>>	BREAK
					CASE 8	vSoundCoords = <<1132.9, -3198.5, -39.8>>	BREAK
				ENDSWITCH
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN vSoundCoords
ENDFUNC

PROC INITALISE_FACTORY_AUDIO_SOUNDS()
	INT iIter
	REPEAT ciMAX_FACTORY_SOUND_IDS iIter
		thisFactory.iSoundIDs[iIter] = -1
	ENDREPEAT
ENDPROC

PROC CLEANUP_FACTORY_AUDIO_SOUNDS()
	
	SWITCH thisFactory.eFactoryType
		CASE FACTORY_TYPE_FAKE_MONEY
			INT iIter
			REPEAT ciMAX_FACTORY_SOUND_IDS iIter
				IF thisFactory.iSoundIDs[iIter] != -1
					STOP_SOUND(thisFactory.iSoundIDs[iIter])
					RELEASE_SOUND_ID(thisFactory.iSoundIDs[iIter])
					thisFactory.iSoundIDs[iIter] = -1
					PRINTLN("CLEANUP_FACTORY_AUDIO_SOUNDS - Cleaning up sound ID: ", iIter)
				ENDIF
			ENDREPEAT
		BREAK
	ENDSWITCH
	
ENDPROC

PROC INITIALISE_FACTORY_AUDIO()
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
	#ENDIF

	STRING strAmbientZone = GET_AMBIENT_ZONE_FOR_THIS_FACTORY()
	
	#IF IS_DEBUG_BUILD
		PRINTLN("AM_MP_BIKER_WAREHOUSE - INITIALISE_FACTORY_AUDIO - Got this zone: ", strAmbientZone)
	#ENDIF
	
	IF IS_BIT_SET(thisFactory.iBS, BS_FACTORY_DATA_AMBIENT_ZONE_SET)
	AND NOT IS_STRING_NULL_OR_EMPTY(thisFactory.strAmbientZone)
	AND NOT ARE_STRINGS_EQUAL(strAmbientZone, thisFactory.strAmbientZone)
		// Stop previous ambient zone
		#IF IS_DEBUG_BUILD
			PRINTLN("AM_MP_BIKER_WAREHOUSE - INITIALISE_FACTORY_AUDIO - Stopping previous ambient zone ", thisFactory.strAmbientZone, " and starting new one ", strAmbientZone)
		#ENDIF
		
		SET_AMBIENT_ZONE_STATE(thisFactory.strAmbientZone, FALSE, TRUE)
		CLEAR_BIT(thisFactory.iBS, BS_FACTORY_DATA_AMBIENT_ZONE_SET)
		thisFactory.strAmbientZone = strAmbientZone
	ENDIF
	
	IF NOT IS_STRING_NULL_OR_EMPTY(strAmbientZone)
	AND NOT IS_BIT_SET(thisFactory.iBS, BS_FACTORY_DATA_AMBIENT_ZONE_SET)
	AND ARE_WORKERS_WORKING_IN_THIS_FACTORY(thisFactory.pOwner, thisFactory.eID, TRUE)
	AND IS_PLAYERS_FACTORY_PRODUCTION_ACTIVATED(thisFactory.pOwner, thisFactory.iSaveSlot) 
	AND NOT IS_FACTORY_PRODUCTION_HALTED(thisFactory.pOwner, thisFactory.iSaveSlot, FALSE) 
		// Start ambient zone
		thisFactory.strAmbientZone = strAmbientZone
		#IF IS_DEBUG_BUILD
			PRINTLN("AM_MP_BIKER_WAREHOUSE - INITIALISE_FACTORY_AUDIO - Starting ambient zone ", thisFactory.strAmbientZone)
		#ENDIF
		SET_AMBIENT_ZONE_STATE(thisFactory.strAmbientZone, TRUE, TRUE)
		SET_BIT(thisFactory.iBS, BS_FACTORY_DATA_AMBIENT_ZONE_SET)
	ENDIF
	
	SWITCH thisFactory.eFactoryType
		CASE FACTORY_TYPE_FAKE_MONEY
			
			// Play new sounds
			INT iIter
			REPEAT ciMAX_FACTORY_SOUND_IDS iIter
				IF serverBD.bActiveState
					thisFactory.iSoundIDs[iIter] = GET_SOUND_ID()
					PLAY_SOUND_FROM_COORD(thisFactory.iSoundIDs[iIter], GET_FACTORY_AUDIO_SOUND_NAME(iIter), GET_FACTORY_AUDIO_SOUND_COORDS(iIter), GET_FACTORY_AUDIO_SOUND_SET_NAME())
					PRINTLN("INITIALISE_FACTORY_AUDIO - Playing Sound: ", GET_FACTORY_AUDIO_SOUND_NAME(iIter), " from Sound Set: ", GET_FACTORY_AUDIO_SOUND_SET_NAME(), " in Slot: ", iIter)
				ELSE
					thisFactory.iSoundIDs[iIter] = -1
				ENDIF
			ENDREPEAT
		BREAK
	ENDSWITCH
	
	
ENDPROC

PROC MAINTAIN_FACTORY_AUDIO()
	
	// Peds wont actually spawn while we're inside so this functionality has no purpose, leaving if it becomes relevant in the future
	
	IF NOT IS_BIT_SET(thisFactory.iBS, BS_FACTORY_DATA_WAS_PRODUCTION_ACTIVE_LAST_FRAME)
	AND NOT IS_FACTORY_PRODUCTION_HALTED(thisFactory.pOwner, thisFactory.iSaveSlot)
		/*
		#IF IS_DEBUG_BUILD
			PRINTLN("AM_MP_BIKER_WAREHOUSE - MAINTAIN_FACTORY_AUDIO - Factory became active, running INITIALISE_FACTORY_AUDIO")
		#ENDIF
		INITIALISE_FACTORY_AUDIO()
		SET_BIT(thisFactory.iBS, BS_FACTORY_DATA_WAS_PRODUCTION_ACTIVE_LAST_FRAME)
		*/
	ENDIF
	
	IF IS_BIT_SET(thisFactory.iBS, BS_FACTORY_DATA_WAS_PRODUCTION_ACTIVE_LAST_FRAME)
	AND IS_FACTORY_PRODUCTION_HALTED(thisFactory.pOwner, thisFactory.iSaveSlot)
		/*
		#IF IS_DEBUG_BUILD
			PRINTLN("AM_MP_BIKER_WAREHOUSE - MAINTAIN_FACTORY_AUDIO - Factory became inactive, running INITIALISE_FACTORY_AUDIO")
		#ENDIF
		INITIALISE_FACTORY_AUDIO()
		CLEAR_BIT(thisFactory.iBS, BS_FACTORY_DATA_WAS_PRODUCTION_ACTIVE_LAST_FRAME)
		*/
	ENDIF
	
	/*
	#IF IS_DEBUG_BUILD
		IF GET_FRAME_COUNT() % 100 = 0
			PRINTLN("AM_MP_BIKER_WAREHOUSE - MAINTAIN_FACTORY_AUDIO - ===========================================================")
			PRINTLN("AM_MP_BIKER_WAREHOUSE - MAINTAIN_FACTORY_AUDIO - The owner: ", GET_PLAYER_NAME(thisFactory.pOwner))
			PRINTLN("AM_MP_BIKER_WAREHOUSE - MAINTAIN_FACTORY_AUDIO - The factory: ", ENUM_TO_INT(thisFactory.eID))
			PRINTLN("AM_MP_BIKER_WAREHOUSE - MAINTAIN_FACTORY_AUDIO - IS_FACTORY_PRODUCTION_HALTED: ", IS_FACTORY_PRODUCTION_HALTED(GET_PLAYER_SAVE_SLOT_FOR_FACTORY(PLAYER_ID(), thisFactory.eID)))
			PRINTLN("AM_MP_BIKER_WAREHOUSE - MAINTAIN_FACTORY_AUDIO - bProductionActive = ", GlobalplayerBD_FM[NATIVE_TO_INT(thisFactory.pOwner)].propertyDetails.bdFactoryData[GET_PLAYER_SAVE_SLOT_FOR_FACTORY(thisFactory.pOwner, thisFactory.eID)].bProductionActive)
			PRINTLN("AM_MP_BIKER_WAREHOUSE - MAINTAIN_FACTORY_AUDIO - IS_PLAYERS_FACTORY_PRODUCTION_ACTIVATED = ", IS_PLAYERS_FACTORY_PRODUCTION_ACTIVATED(thisFactory.pOwner, thisFactory.eID))
			PRINTLN("AM_MP_BIKER_WAREHOUSE - MAINTAIN_FACTORY_AUDIO - IS_PLAYERS_FACTORY_PRODUCTION_HALTED = ", IS_PLAYERS_FACTORY_PRODUCTION_HALTED(thisFactory.pOwner, thisFactory.eID))
			PRINTLN("AM_MP_BIKER_WAREHOUSE - MAINTAIN_FACTORY_AUDIO - BS_FACTORY_DATA_WAS_PRODUCTION_ACTIVE_LAST_FRAME = ", IS_BIT_SET(thisFactory.iBS, BS_FACTORY_DATA_WAS_PRODUCTION_ACTIVE_LAST_FRAME))
		ENDIF
	#ENDIF
	*/
ENDPROC

PROC MAINTAIN_FACTORY_AUDIO_BANKS()
	
	IF NOT IS_BIT_SET(thisFactory.iBS, BS_FACTORY_DATA_REQUESTED_AUDIO_BANKS)
		SWITCH thisFactory.eFactoryType
			CASE FACTORY_TYPE_WEED
				IF REQUEST_SCRIPT_AUDIO_BANK("DLC_BIKER/Interior_Weed")
					CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_BIKER_WAREHOUSE - MAINTAIN_FACTORY_AUDIO_BANKS - Audio Bank Requested: DLC_BIKER/Interior_Weed")
					SET_BIT(thisFactory.iBS, BS_FACTORY_DATA_REQUESTED_AUDIO_BANKS)
				ENDIF
			BREAK
			CASE FACTORY_TYPE_METH
				IF REQUEST_SCRIPT_AUDIO_BANK("DLC_BIKER/Interior_Meth")
					CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_BIKER_WAREHOUSE - MAINTAIN_FACTORY_AUDIO_BANKS - Audio Bank Requested: DLC_BIKER/Interior_Meth")
					SET_BIT(thisFactory.iBS, BS_FACTORY_DATA_REQUESTED_AUDIO_BANKS)
				ENDIF
			BREAK
			CASE FACTORY_TYPE_CRACK
				IF REQUEST_SCRIPT_AUDIO_BANK("DLC_BIKER/Interior_Cocaine")
					CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_BIKER_WAREHOUSE - MAINTAIN_FACTORY_AUDIO_BANKS - Audio Bank Requested: DLC_BIKER/Interior_Cocaine")
					SET_BIT(thisFactory.iBS, BS_FACTORY_DATA_REQUESTED_AUDIO_BANKS)
				ENDIF
			BREAK
			CASE FACTORY_TYPE_FAKE_IDS
				IF REQUEST_SCRIPT_AUDIO_BANK("DLC_BIKER/Interior_FakeID")
					CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_BIKER_WAREHOUSE - MAINTAIN_FACTORY_AUDIO_BANKS - Audio Bank Requested: DLC_BIKER/Interior_FakeID")
					SET_BIT(thisFactory.iBS, BS_FACTORY_DATA_REQUESTED_AUDIO_BANKS)
				ENDIF
			BREAK
			CASE FACTORY_TYPE_FAKE_MONEY
				IF REQUEST_SCRIPT_AUDIO_BANK("DLC_BIKER/Interior_MoneyPrinting")
					CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_BIKER_WAREHOUSE - MAINTAIN_FACTORY_AUDIO_BANKS - Audio Bank Requested: DLC_BIKER/Interior_MoneyPrinting")
					SET_BIT(thisFactory.iBS, BS_FACTORY_DATA_REQUESTED_AUDIO_BANKS)
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	
ENDPROC

PROC CLEANUP_FACTORY_AUDIO()

	SWITCH thisFactory.eFactoryType
		CASE FACTORY_TYPE_WEED
			RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_BIKER/Interior_Weed")
		BREAK
		CASE FACTORY_TYPE_METH
			RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_BIKER/Interior_Meth")
		BREAK
		CASE FACTORY_TYPE_CRACK
			RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_BIKER/Interior_Cocaine")
		BREAK
		CASE FACTORY_TYPE_FAKE_IDS
			RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_BIKER/Interior_FakeID")
		BREAK
		CASE FACTORY_TYPE_FAKE_MONEY
			RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_BIKER/Interior_MoneyPrinting")
		BREAK
	ENDSWITCH
	
	IF IS_BIT_SET(thisFactory.iBS, BS_FACTORY_DATA_AMBIENT_ZONE_SET)
	AND NOT IS_STRING_NULL_OR_EMPTY(thisFactory.strAmbientZone)
		#IF IS_DEBUG_BUILD
			PRINTLN("AM_MP_BIKER_WAREHOUSE - CLEANUP_FACTORY_AUDIO - setting ambient zone to false ", thisFactory.strAmbientZone)
		#ENDIF
		
		SET_AMBIENT_ZONE_STATE(thisFactory.strAmbientZone, FALSE, TRUE)
		CLEAR_BIT(thisFactory.iBS, BS_FACTORY_DATA_AMBIENT_ZONE_SET)
	ENDIF
	
ENDPROC

PROC INITIALISE_FACTORY_PRODUCTION()
	
	IF IS_FACTORY_OWNER_ID_VALID(NATIVE_TO_INT(thisFactory.pOwner))
		INITIALISE_FACTORY_TOTALS()
		INITIALISE_FACTORY_ENTITY_SETS()
		INITIALISE_FACTORY_PRODUCTION_PROPS()
	ENDIF
	
ENDPROC

PROC INITIALISE_FACTORY_PRODUCT()
	
	IF IS_FACTORY_OWNER_ID_VALID(NATIVE_TO_INT(thisFactory.pOwner))
		CREATE_FACTORY_PALLETS(thisFactory.eID)
		CREATE_ALL_OWNED_FACTORY_PRODUCT(thisFactory.eID, thisFactory.iProductSlot)
		INITIALISE_FACTORY_TIMECYCLE_MODIFIERS()
		INITALISE_FACTORY_AUDIO_SOUNDS()
		INITIALISE_FACTORY_AUDIO()
	ENDIF
	
ENDPROC

PROC MAINTAIN_FACTORY_PRODUCTION()
	
	IF DOES_FACTORY_PRODUCT_TOTAL_NEED_UPDATING()
		IF HAS_FACTORY_PRODUCT_TOTAL_INCREASED()
			ADD_FACTORY_PRODUCT()
		ELIF HAS_FACTORY_PRODUCT_TOTAL_DECREASED()
			REMOVE_FACTORY_PRODUCT()
		ENDIF
	ENDIF
	
ENDPROC

/*
PROC MAINTAIN_FACTORY_TUTORIAL_PRODUCTION()
	
	IF DOES_FACTORY_PRODUCT_TOTAL_NEED_UPDATING(TRUE)
		IF HAS_FACTORY_PRODUCT_TOTAL_INCREASED(TRUE)
			ADD_FACTORY_TUTORIAL_PRODUCT()
		ELIF HAS_FACTORY_PRODUCT_TOTAL_DECREASED(TRUE)
			REMOVE_FACTORY_TUTORIAL_PRODUCT()
		ENDIF
	ENDIF
	
ENDPROC
*/

PROC SET_FACTORY_HUD_BLEND_TIMER()
	IF thisFactory.hudData.iSetHUDBlendTimer  = 0
		thisFactory.hudData.iHUDBlendOffset   = GET_FACTORY_PRODUCTION_TIME(thisFactory.pOwner, thisFactory.eID) - GET_GAME_TIMER()
		thisFactory.hudData.tdHUDBlendTimer   = GET_TIME_OFFSET(GET_NETWORK_TIME(), thisFactory.hudData.iHUDBlendOffset)
		thisFactory.hudData.iSetHUDBlendTimer = 1
	ENDIF
ENDPROC

FUNC BOOL SHOULD_FACTORY_SUPPLY_BAR_BLEND()
	BOOL bBlend = FALSE
	
	IF IS_PLAYERS_FACTORY_PRODUCTION_ACTIVATED(thisFactory.pOwner, thisFactory.iSaveSlot) 
	AND NOT IS_FACTORY_PRODUCTION_HALTED(thisFactory.pOwner, thisFactory.iSaveSlot, FALSE)
	#IF IS_DEBUG_BUILD
	OR (g_bFactoryWidgetEnabled AND g_bFactoryForceActiveProduction)
	#ENDIF
		bBlend = TRUE
	ENDIF
	
	RETURN bBlend
ENDFUNC

PROC MAINTAIN_FACTORY_HUD()
	
	IF IS_PLAYER_IN_FACTORY(PLAYER_ID())
	AND NOT IS_ANY_TYPE_OF_CUTSCENE_PLAYING()
	AND GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) != FMMC_TYPE_BIKER_CONTRABAND_SELL
	AND playerBD[PARTICIPANT_ID_TO_INT()].MPCCTVClient.eStage != MP_CCTV_CLIENT_STAGE_ACTIVATED
	AND NOT AM_I_TRANSITION_SESSIONS_STARTING_ON_CALL()
	#IF IS_DEBUG_BUILD
	AND NOT g_bFactoryHideUI
	#ENDIF
		INT iProductQuantity   	= GET_PRODUCT_TOTAL_FOR_FACTORY(thisFactory.pOwner, thisFactory.eID)
		INT iProductCapacity   	= GET_FACTORY_PRODUCT_CAPACITY(thisFactory.eID)
		INT iTotalProductValue 	= GET_FACTORY_TOTAL_PRODUCT_VALUE(thisFactory.pOwner, thisFactory.eID, iProductQuantity)
		FLOAT fModifiedProdValue = (TO_FLOAT(iTotalProductValue) * g_sMPTunables.fBiker_Sell_Product_Local_Reward_Modifier)
		iTotalProductValue = ROUND(fModifiedProdValue)
		INT iMaterialTotalBlended = 0
		IF thisFactory.pOwner != INVALID_PLAYER_INDEX()
			iMaterialTotalBlended = GlobalplayerBD_FM[NATIVE_TO_INT(thisFactory.pOwner)].propertyDetails.bdFactoryData[thisFactory.iSaveSlot].iMaterialsTotalBlended
		ENDIF
		
		DRAW_GENERIC_SCORE(iTotalProductValue, "HUD_STOCK", DEFAULT, DEFAULT, HUDORDER_THIRDBOTTOM, DEFAULT, "HUD_CASH")
		
		// Calculate the Capacity and Supply meters based on percentages
		FLOAT fProductQuantityPercent = (TO_FLOAT(iProductQuantity)/TO_FLOAT(iProductCapacity))*100
		INT iProductQuantityPercent = ROUND(fProductQuantityPercent)
		
		DRAW_GENERIC_METER(iProductQuantityPercent, 100, "HUD_CAPACITY", HUD_COLOUR_BLUE, DEFAULT, HUDORDER_SECONDBOTTOM,DEFAULT,DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, PERCENTAGE_METER_LINE_ALL_20, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
		DRAW_GENERIC_METER(iMaterialTotalBlended, 100, "HUD_SUPPLIES", HUD_COLOUR_GREEN, DEFAULT, HUDORDER_BOTTOM,DEFAULT,DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, PERCENTAGE_METER_LINE_ALL_20, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE, 20)
	ENDIF
	
ENDPROC

FUNC BOOL IS_PLAYER_PLAYING_LAPTOP_ANIM(PLAYER_INDEX playerID)
	IF DOES_ENTITY_EXIST(GET_PLAYER_PED(playerID))
	AND NOT IS_PED_INJURED(GET_PLAYER_PED(playerID))
	//AND IS_ENTITY_ALIVE(GET_PLAYER_PED(playerID))
		IF IS_ENTITY_PLAYING_ANIM(GET_PLAYER_PED(playerID), "anim@amb@warehouse@laptop@", "enter")
		OR IS_ENTITY_PLAYING_ANIM(GET_PLAYER_PED(playerID), "anim@amb@warehouse@laptop@", "idle_a")
		OR IS_ENTITY_PLAYING_ANIM(GET_PLAYER_PED(playerID), "anim@amb@warehouse@laptop@", "idle_b")
		OR IS_ENTITY_PLAYING_ANIM(GET_PLAYER_PED(playerID), "anim@amb@warehouse@laptop@", "idle_c")
		OR IS_ENTITY_PLAYING_ANIM(GET_PLAYER_PED(playerID), "anim@amb@warehouse@laptop@", "idle_d")
		OR IS_ENTITY_PLAYING_ANIM(GET_PLAYER_PED(playerID), "anim@amb@warehouse@laptop@", "exit")
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL _GET_SAFE_FACTORY_SPAWN_POINT(INT iIndex, VECTOR &vSpawnPoint, FLOAT &fSpawnHeading)
	SWITCH thisFactory.eID
		CASE FACTORY_ID_METH_1
		CASE FACTORY_ID_METH_2
		CASE FACTORY_ID_METH_3
		CASE FACTORY_ID_METH_4
			SWITCH iIndex
				CASE 0
				    vSpawnPoint = <<-8.0651, 0.8882, -1.4932>>
				    fSpawnHeading = 261.0805
				    RETURN TRUE
				BREAK
				CASE 1
				    vSpawnPoint = <<-7.0479, 0.8728, -1.4932>>
				    fSpawnHeading = 272.6658
				    RETURN TRUE
				BREAK
				CASE 2
				    vSpawnPoint = <<-5.2506, 0.7437, -1.4932>>
				    fSpawnHeading = 266.0114
				    RETURN TRUE
				BREAK
				CASE 3
				    vSpawnPoint = <<-3.5274, 0.3464, -1.4932>>
				    fSpawnHeading = 267.6772
				    RETURN TRUE
				BREAK
				CASE 4
				    vSpawnPoint = <<-1.4504, 0.4937, -1.4932>>
				    fSpawnHeading = 287.7808
				    RETURN TRUE
				BREAK
				CASE 5
				    vSpawnPoint = <<-0.0495, 0.5037, -1.4932>>
				    fSpawnHeading = 271.5429
				    RETURN TRUE
				BREAK
				CASE 6
				    vSpawnPoint = <<1.9395, 0.5813, -1.4932>>
				    fSpawnHeading = 272.2227
				    RETURN TRUE
				BREAK
				CASE 7
				    vSpawnPoint = <<3.3624, 0.6377, -1.4932>>
				    fSpawnHeading = 272.2618
				    RETURN TRUE
				BREAK
				CASE 8
				    vSpawnPoint = <<5.3270, 0.7144, -1.4932>>
				    fSpawnHeading = 272.2172
				    RETURN TRUE
				BREAK
				CASE 9
				    vSpawnPoint = <<6.3069, -0.1621, -1.4932>>
				    fSpawnHeading = 173.7993
				    RETURN TRUE
				BREAK
				CASE 10
				    vSpawnPoint = <<6.4319, -1.6653, -1.4932>>
				    fSpawnHeading = 174.7092
				    RETURN TRUE
				BREAK
				CASE 11
				    vSpawnPoint = <<6.2258, -3.6362, -1.4932>>
				    fSpawnHeading = 210.9242
				    RETURN TRUE
				BREAK
				CASE 12
				    vSpawnPoint = <<4.0538, -4.4775, -1.4932>>
				    fSpawnHeading = 80.2866
				    RETURN TRUE
				BREAK
				CASE 13
				    vSpawnPoint = <<2.2449, -4.0894, -1.4932>>
				    fSpawnHeading = 84.3287
				    RETURN TRUE
				BREAK
				CASE 14
				    vSpawnPoint = <<0.3893, -3.9419, -1.4932>>
				    fSpawnHeading = 104.3800
				    RETURN TRUE
				BREAK
				CASE 15
				    vSpawnPoint = <<-1.3326, -3.7742, -1.4932>>
				    fSpawnHeading = 80.2858
				    RETURN TRUE
				BREAK
				CASE 16
				    vSpawnPoint = <<-3.6606, -1.5364, -1.4932>>
				    fSpawnHeading = 96.8562
				    RETURN TRUE
				BREAK
				CASE 17
				    vSpawnPoint = <<-5.4593, -1.6995, -1.4932>>
				    fSpawnHeading = 94.0108
				    RETURN TRUE
				BREAK
				CASE 18
				    vSpawnPoint = <<-5.7396, -2.8513, -1.4932>>
				    fSpawnHeading = 163.1431
				    RETURN TRUE
				BREAK
				CASE 19
				    vSpawnPoint = <<-6.9738, -0.6362, -1.4932>>
				    fSpawnHeading = 19.8501
				    RETURN TRUE
				BREAK
				CASE 20
				    vSpawnPoint = <<-11.7814, 0.7563, 0.1068>>
				    fSpawnHeading = 290.6634
				    RETURN TRUE
				BREAK
				CASE 21
				    vSpawnPoint = <<-10.8303, -2.7856, 1.1069>>
				    fSpawnHeading = 268.2964
				    RETURN TRUE
				BREAK
				CASE 22
				    vSpawnPoint = <<-11.3101, -4.2405, 1.1069>>
				    fSpawnHeading = 246.1742
				    RETURN TRUE
				BREAK
				CASE 23
				    vSpawnPoint = <<-11.7900, -2.7625, 1.1069>>
				    fSpawnHeading = 24.5168
				    RETURN TRUE
				BREAK
				CASE 24
				    vSpawnPoint = <<-2.3275, -3.4011, -1.4932>>
				    fSpawnHeading = 233.0836
				    RETURN TRUE
				BREAK
				CASE 25
				    vSpawnPoint = <<3.0890, -5.4397, -1.4932>>
				    fSpawnHeading = 345.8568
				    RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FACTORY_ID_WEED_1
		CASE FACTORY_ID_WEED_2
		CASE FACTORY_ID_WEED_3
		CASE FACTORY_ID_WEED_4
			SWITCH iIndex
				CASE 0
				    vSpawnPoint = <<15.4935, 14.4722, -1.6627>>
				    fSpawnHeading = 190.2072
				    RETURN TRUE
				BREAK
				CASE 1
				    vSpawnPoint = <<15.2948, 12.1663, -1.6641>>
				    fSpawnHeading = 153.0879
				    RETURN TRUE
				BREAK
				CASE 2
				    vSpawnPoint = <<13.5016, 11.1245, -1.6491>>
				    fSpawnHeading = 6.5562
				    RETURN TRUE
				BREAK
				CASE 3
				    vSpawnPoint = <<13.4386, 13.4099, -1.6634>>
				    fSpawnHeading = 43.2638
				    RETURN TRUE
				BREAK
				CASE 4
				    vSpawnPoint = <<11.5516, 13.5752, -1.6643>>
				    fSpawnHeading = 95.8077
				    RETURN TRUE
				BREAK
				CASE 5
				    vSpawnPoint = <<11.7296, 11.0786, -1.6647>>
				    fSpawnHeading = 264.5948
				    RETURN TRUE
				BREAK
				CASE 6
				    vSpawnPoint = <<13.1332, 9.2014, -1.5981>>
				    fSpawnHeading = 187.5681
				    RETURN TRUE
				BREAK
				CASE 7
				    vSpawnPoint = <<13.4993, 7.4380, -1.6343>>
				    fSpawnHeading = 235.1340
				    RETURN TRUE
				BREAK
				CASE 8
				    vSpawnPoint = <<12.0526, 7.2146, -1.6440>>
				    fSpawnHeading = 132.9082
				    RETURN TRUE
				BREAK
				CASE 9
				    vSpawnPoint = <<10.4784, 7.2537, -1.6614>>
				    fSpawnHeading = 93.4745
				    RETURN TRUE
				BREAK
				CASE 10
				    vSpawnPoint = <<9.9497, 5.3606, -1.6614>>
				    fSpawnHeading = 182.1266
				    RETURN TRUE
				BREAK
				CASE 11
				    vSpawnPoint = <<9.7693, 3.8811, -1.6614>>
				    fSpawnHeading = 165.1594
				    RETURN TRUE
				BREAK
				CASE 12
				    vSpawnPoint = <<8.9268, 1.5435, -1.6613>>
				    fSpawnHeading = 181.7826
				    RETURN TRUE
				BREAK
				CASE 13
				    vSpawnPoint = <<6.9039, 1.2212, -1.6613>>
				    fSpawnHeading = 231.9531
				    RETURN TRUE
				BREAK
				CASE 14
				    vSpawnPoint = <<8.2637, 2.9502, -1.6613>>
				    fSpawnHeading = 331.5128
				    RETURN TRUE
				BREAK
				CASE 15
				    vSpawnPoint = <<5.2361, 0.2166, -1.6613>>
				    fSpawnHeading = 191.7016
				    RETURN TRUE
				BREAK
				CASE 16
				    vSpawnPoint = <<5.0042, -1.5713, -1.6613>>
				    fSpawnHeading = 177.4317
				    RETURN TRUE
				BREAK
				CASE 17
				    vSpawnPoint = <<4.6091, -3.9709, -1.6612>>
				    fSpawnHeading = 169.1113
				    RETURN TRUE
				BREAK
				CASE 18
				    vSpawnPoint = <<4.0208, -6.4746, -1.6612>>
				    fSpawnHeading = 145.6640
				    RETURN TRUE
				BREAK
				CASE 19
				    vSpawnPoint = <<2.4635, -6.6208, -1.6612>>
				    fSpawnHeading = 79.7766
				    RETURN TRUE
				BREAK
				CASE 20
				    vSpawnPoint = <<0.6039, -6.8201, -1.6478>>
				    fSpawnHeading = 95.7839
				    RETURN TRUE
				BREAK
				CASE 21
				    vSpawnPoint = <<-2.1455, -6.7568, -1.3997>>
				    fSpawnHeading = 94.1804
				    RETURN TRUE
				BREAK
				CASE 22
				    vSpawnPoint = <<-3.6716, -6.8521, -1.0077>>
				    fSpawnHeading = 96.6808
				    RETURN TRUE
				BREAK
				CASE 23
				    vSpawnPoint = <<-3.6742, -8.6653, -1.0066>>
				    fSpawnHeading = 192.8282
				    RETURN TRUE
				BREAK
				CASE 24
				    vSpawnPoint = <<-1.3040, -8.8503, -1.6063>>
				    fSpawnHeading = 240.6599
				    RETURN TRUE
				BREAK
				CASE 25
				    vSpawnPoint = <<-6.6735, -6.5791, -0.6590>>
				    fSpawnHeading = 71.2122
				    RETURN TRUE
				BREAK
				CASE 26
				    vSpawnPoint = <<-8.7126, -7.4272, -0.6632>>
				    fSpawnHeading = 95.6252
				    RETURN TRUE
				BREAK
				CASE 27
				    vSpawnPoint = <<-11.2070, -7.5120, -0.6687>>
				    fSpawnHeading = 96.3023
				    RETURN TRUE
				BREAK
				CASE 28
				    vSpawnPoint = <<-14.0040, -7.4170, -0.6750>>
				    fSpawnHeading = 105.6386
				    RETURN TRUE
				BREAK
				CASE 29
				    vSpawnPoint = <<-16.4396, -7.4263, -0.6804>>
				    fSpawnHeading = 89.3830
				    RETURN TRUE
				BREAK
				CASE 30
				    vSpawnPoint = <<-13.7642, -9.5327, -0.6738>>
				    fSpawnHeading = 195.4684
				    RETURN TRUE
				BREAK
				CASE 31
				    vSpawnPoint = <<-8.2518, -4.8899, -0.6630>>
				    fSpawnHeading = 333.5356
				    RETURN TRUE
				BREAK
				CASE 32
				    vSpawnPoint = <<-8.0204, -2.3789, -0.6632>>
				    fSpawnHeading = 23.9753
				    RETURN TRUE
				BREAK
				CASE 33
				    vSpawnPoint = <<-7.7484, 0.1997, -0.6635>>
				    fSpawnHeading = 354.4371
				    RETURN TRUE
				BREAK
				CASE 34
				    vSpawnPoint = <<-6.3077, 0.2598, -0.6603>>
				    fSpawnHeading = 248.8716
				    RETURN TRUE
				BREAK
				CASE 35
				    vSpawnPoint = <<10.9467, -6.3420, -1.6612>>
				    fSpawnHeading = 115.3785
				    RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FACTORY_ID_CRACK_1
		CASE FACTORY_ID_CRACK_2
		CASE FACTORY_ID_CRACK_3
		CASE FACTORY_ID_CRACK_4
			SWITCH iIndex
				CASE 0
				    vSpawnPoint = <<-5.2800, 7.8618, -1.4935>>
				    fSpawnHeading = 75.5755
				    RETURN TRUE
				BREAK
				CASE 1
				    vSpawnPoint = <<-3.4958, 6.4199, -1.4935>>
				    fSpawnHeading = 176.3378
				    RETURN TRUE
				BREAK
				CASE 2
				    vSpawnPoint = <<-5.0426, 6.1199, -1.4935>>
				    fSpawnHeading = 89.9021
				    RETURN TRUE
				BREAK
				CASE 3
				    vSpawnPoint = <<-4.8940, 3.9675, -1.4935>>
				    fSpawnHeading = 194.0774
				    RETURN TRUE
				BREAK
				CASE 4
				    vSpawnPoint = <<-5.6848, 0.2993, -1.4935>>
				    fSpawnHeading = 214.1578
				    RETURN TRUE
				BREAK
				CASE 5
				    vSpawnPoint = <<-5.6473, -1.6326, -1.4935>>
				    fSpawnHeading = 281.0292
				    RETURN TRUE
				BREAK
				CASE 6
				    vSpawnPoint = <<-4.1578, -1.3774, -1.4935>>
				    fSpawnHeading = 263.3574
				    RETURN TRUE
				BREAK
				CASE 7
				    vSpawnPoint = <<-2.7953, -1.4641, -1.4935>>
				    fSpawnHeading = 268.1769
				    RETURN TRUE
				BREAK
				CASE 8
				    vSpawnPoint = <<-1.1670, -1.3782, -1.4935>>
				    fSpawnHeading = 267.5936
				    RETURN TRUE
				BREAK
				CASE 9
				    vSpawnPoint = <<0.4619, -1.2634, -1.4935>>
				    fSpawnHeading = 284.8116
				    RETURN TRUE
				BREAK
				CASE 10
				    vSpawnPoint = <<2.3597, -1.3296, -1.4935>>
				    fSpawnHeading = 274.2172
				    RETURN TRUE
				BREAK
				CASE 11
				    vSpawnPoint = <<5.0250, -1.0359, -1.4935>>
				    fSpawnHeading = 276.3821
				    RETURN TRUE
				BREAK
				CASE 12
				    vSpawnPoint = <<7.1592, 2.0425, -1.4935>>
				    fSpawnHeading = 172.8774
				    RETURN TRUE
				BREAK
				CASE 13
				    vSpawnPoint = <<7.1210, -0.3628, -1.4935>>
				    fSpawnHeading = 265.1676
				    RETURN TRUE
				BREAK
				CASE 14
				    vSpawnPoint = <<-3.1549, 3.1899, -1.4935>>
				    fSpawnHeading = 252.8759
				    RETURN TRUE
				BREAK
				CASE 15
				    vSpawnPoint = <<-1.6194, 3.1726, -1.4935>>
				    fSpawnHeading = 256.5730
				    RETURN TRUE
				BREAK
				CASE 16
				    vSpawnPoint = <<0.5004, 2.9946, -1.4935>>
				    fSpawnHeading = 214.1534
				    RETURN TRUE
				BREAK
				CASE 17
				    vSpawnPoint = <<2.3693, 2.6523, -1.4935>>
				    fSpawnHeading = 262.9874
				    RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FACTORY_ID_FAKE_CASH_1
		CASE FACTORY_ID_FAKE_CASH_2
		CASE FACTORY_ID_FAKE_CASH_3
		CASE FACTORY_ID_FAKE_CASH_4
			SWITCH iIndex
				CASE 0
				    vSpawnPoint = <<13.3914, -1.1318, -2.1607>>
				    fSpawnHeading = 67.1420
				    RETURN TRUE
				BREAK
				CASE 1
				    vSpawnPoint = <<11.6571, -0.1790, -2.1607>>
				    fSpawnHeading = 348.9611
				    RETURN TRUE
				BREAK
				CASE 2
				    vSpawnPoint = <<9.2828, -0.1216, -2.1607>>
				    fSpawnHeading = 103.6845
				    RETURN TRUE
				BREAK
				CASE 3
				    vSpawnPoint = <<6.8961, -0.0735, -2.1607>>
				    fSpawnHeading = 56.9763
				    RETURN TRUE
				BREAK
				CASE 4
				    vSpawnPoint = <<4.3380, -0.0527, -2.1607>>
				    fSpawnHeading = 84.5957
				    RETURN TRUE
				BREAK
				CASE 5
				    vSpawnPoint = <<2.0148, -0.0662, -2.1607>>
				    fSpawnHeading = 95.6365
				    RETURN TRUE
				BREAK
				CASE 6
				    vSpawnPoint = <<-0.7841, -0.1589, -2.8972>>
				    fSpawnHeading = 94.6691
				    RETURN TRUE
				BREAK
				CASE 7
				    vSpawnPoint = <<-2.7266, 0.0317, -2.8979>>
				    fSpawnHeading = 106.0158
				    RETURN TRUE
				BREAK
				CASE 8
				    vSpawnPoint = <<-5.1082, 0.2373, -2.8987>>
				    fSpawnHeading = 267.1570
				    RETURN TRUE
				BREAK
				CASE 9
				    vSpawnPoint = <<-6.2844, 1.9412, -2.8996>>
				    fSpawnHeading = 32.7783
				    RETURN TRUE
				BREAK
				CASE 10
				    vSpawnPoint = <<-4.8251, 1.8984, -2.8999>>
				    fSpawnHeading = 264.3560
				    RETURN TRUE
				BREAK
				CASE 11
				    vSpawnPoint = <<-0.1385, 1.7961, -2.9005>>
				    fSpawnHeading = 175.7735
				    RETURN TRUE
				BREAK
				CASE 12
				    vSpawnPoint = <<2.2612, 1.5024, -2.9023>>
				    fSpawnHeading = 279.3143
				    RETURN TRUE
				BREAK
				CASE 13
				    vSpawnPoint = <<4.1428, 1.6858, -2.9012>>
				    fSpawnHeading = 269.9267
				    RETURN TRUE
				BREAK
				CASE 14
				    vSpawnPoint = <<7.7526, 1.7688, -2.9006>>
				    fSpawnHeading = 237.2515
				    RETURN TRUE
				BREAK
				CASE 15
				    vSpawnPoint = <<9.3647, 1.5320, -2.9021>>
				    fSpawnHeading = 340.2497
				    RETURN TRUE
				BREAK
				CASE 16
				    vSpawnPoint = <<12.5358, 2.1553, -2.8983>>
				    fSpawnHeading = 171.1550
				    RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FACTORY_ID_FAKE_ID_1
		CASE FACTORY_ID_FAKE_ID_2
		CASE FACTORY_ID_FAKE_ID_3
		CASE FACTORY_ID_FAKE_ID_4
			SWITCH iIndex
				CASE 0
				    vSpawnPoint = <<7.8514, 1.0417, -1.8080>>
				    fSpawnHeading = 320.9874
				    RETURN TRUE
				BREAK
				CASE 1
				    vSpawnPoint = <<6.8098, 1.1497, -1.8080>>
				    fSpawnHeading = 83.4751
				    RETURN TRUE
				BREAK
				CASE 2
				    vSpawnPoint = <<6.2847, 0.3074, -1.8080>>
				    fSpawnHeading = 179.5315
				    RETURN TRUE
				BREAK
				CASE 3
				    vSpawnPoint = <<6.4712, -1.1572, -1.8080>>
				    fSpawnHeading = 186.8054
				    RETURN TRUE
				BREAK
				CASE 4
				    vSpawnPoint = <<7.8724, -0.3706, -1.8080>>
				    fSpawnHeading = 170.5044
				    RETURN TRUE
				BREAK
				CASE 5
				    vSpawnPoint = <<5.5302, 1.4609, -1.8080>>
				    fSpawnHeading = 56.2245
				    RETURN TRUE
				BREAK
				CASE 6
				    vSpawnPoint = <<3.9001, 1.8862, -1.8080>>
				    fSpawnHeading = 343.5474
				    RETURN TRUE
				BREAK
				CASE 7
				    vSpawnPoint = <<2.3759, 1.9517, -1.8080>>
				    fSpawnHeading = 150.4720
				    RETURN TRUE
				BREAK
				CASE 8
				    vSpawnPoint = <<0.7621, 1.9512, -1.8080>>
				    fSpawnHeading = 172.6307
				    RETURN TRUE
				BREAK
				CASE 9
				    vSpawnPoint = <<-2.0548, 2.2393, -1.8080>>
				    fSpawnHeading = 39.2363
				    RETURN TRUE
				BREAK
				CASE 10
				    vSpawnPoint = <<-2.9337, 4.6160, -1.8080>>
				    fSpawnHeading = 11.1992
				    RETURN TRUE
				BREAK
				CASE 11
				    vSpawnPoint = <<-5.8589, 3.4314, -1.8080>>
				    fSpawnHeading = 120.3226
				    RETURN TRUE
				BREAK
				CASE 12
				    vSpawnPoint = <<-7.2682, 1.9580, -1.8080>>
				    fSpawnHeading = 250.0415
				    RETURN TRUE
				BREAK
				CASE 13
				    vSpawnPoint = <<-7.8790, -0.3760, -1.8080>>
				    fSpawnHeading = 190.5884
				    RETURN TRUE
				BREAK
				CASE 14
				    vSpawnPoint = <<-5.7352, 1.7732, -1.8080>>
				    fSpawnHeading = 275.3224
				    RETURN TRUE
				BREAK
				CASE 15
				    vSpawnPoint = <<-3.8862, 2.1445, -1.8080>>
				    fSpawnHeading = 184.7904
				    RETURN TRUE
				BREAK
				CASE 16
				    vSpawnPoint = <<-2.9065, 1.0234, -1.8080>>
				    fSpawnHeading = 265.3000
				    RETURN TRUE
				BREAK
				CASE 17
				    vSpawnPoint = <<-3.1045, -1.0984, -1.8080>>
				    fSpawnHeading = 192.7319
				    RETURN TRUE
				BREAK
				CASE 18
				    vSpawnPoint = <<4.1752, -1.6794, -1.8080>>
				    fSpawnHeading = 127.5510
				    RETURN TRUE
				BREAK
				CASE 19
				    vSpawnPoint = <<2.6870, -1.5823, -1.8080>>
				    fSpawnHeading = 16.3232
				    RETURN TRUE
				BREAK
				CASE 20
				    vSpawnPoint = <<1.6663, -0.7551, -1.8080>>
				    fSpawnHeading = 309.5113
				    RETURN TRUE
				BREAK
				CASE 21
				    vSpawnPoint = <<-0.4951, -2.1733, -1.8080>>
				    fSpawnHeading = 87.8314
				    RETURN TRUE
				BREAK
				CASE 22
				    vSpawnPoint = <<-2.2415, -2.2161, -1.8080>>
				    fSpawnHeading = 345.3737
				    RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC _SET_UP_CUSTOM_SPAWN_POINTS_AFTER_FACTORY_UPGRADE()
	IF NOT g_SpawnData.bUseCustomSpawnPoints
		USE_CUSTOM_SPAWN_POINTS(TRUE, FALSE, TRUE, 0.01, 2, DEFAULT, DEFAULT, 0.01, DEFAULT, DEFAULT, TRUE, TRUE)
		
		INT i
		VECTOR vSpawnPoint
		VECTOR vPlayerPos = GET_PLAYER_COORDS(PLAYER_ID())
		FLOAT fSpawnHeading, fWeight, fPlayerHeading
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			fPlayerHeading  = GET_ENTITY_HEADING(PLAYER_PED_ID())
		ENDIF
		
		FLOAT fMaxDist
		#IF IS_DEBUG_BUILD
			INT db_iMaxDistPoint
		#ENDIF
		
		// Get simple interior details
		SIMPLE_INTERIOR_DETAILS details
		GET_SIMPLE_INTERIOR_DETAILS(thisFactory.eSimpleInteriorID, details)
		
		WHILE _GET_SAFE_FACTORY_SPAWN_POINT(i, vSpawnPoint, fSpawnHeading)
			vSpawnPoint = TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS(vSpawnPoint, details)
			IF VDIST(vSpawnPoint, vPlayerPos) > fMaxDist
				fMaxDist = VDIST(vSpawnPoint, vPlayerPos)
				#IF IS_DEBUG_BUILD
					db_iMaxDistPoint = i
				#ENDIF
			ENDIF
			i += 1
		ENDWHILE
		
		#IF IS_DEBUG_BUILD
			PRINTLN("AM_MP_BIKER_WAREHOUSE - _SET_UP_CUSTOM_SPAWN_POINTS_AFTER_FACTORY_UPGRADE - Max distance of any point to player: ", fMaxDist, " (point ", db_iMaxDistPoint, ")")
		#ENDIF
		
		i = 0
		
		WHILE _GET_SAFE_FACTORY_SPAWN_POINT(i, vSpawnPoint, fSpawnHeading)
			vSpawnPoint = TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS(vSpawnPoint, details)
			fWeight = 1.0 - (VDIST(vPlayerPos, vSpawnPoint) / fMaxDist)
			
			ADD_CUSTOM_SPAWN_POINT(vSpawnPoint, fPlayerHeading, fWeight #IF IS_DEBUG_BUILD,0.2 #ENDIF)
			i += 1
		ENDWHILE
		
		#IF IS_DEBUG_BUILD
			PRINTLN("AM_MP_BIKER_WAREHOUSE - _SET_UP_CUSTOM_SPAWN_POINTS_AFTER_FACTORY_UPGRADE - Done.")
		#ENDIF
	ENDIF
ENDPROC

PROC PERFORM_FACTORY_UPGRADE(FACTORY_TYPE eFactoryType, FACTORY_UPGRADE_ID eUpgradeID)
	
	SWITCH thisFactory.iUpgradeState
		CASE FACTORY_UPGRADE_STATE_FADE_OUT
		
			PRINTLN("AM_MP_BIKER_WAREHOUSE - FACTORY_UPGRADE_STATE_FADE_OUT")
			
			IF NOT IS_SCREEN_FADED_OUT()
			AND NOT IS_SCREEN_FADING_OUT()
			AND NOT IS_PLAYER_PLAYING_LAPTOP_ANIM(thisFactory.pOwner)
				PRINTLN("AM_MP_BIKER_WAREHOUSE - fade out and remove control")
				DO_SCREEN_FADE_OUT(500)
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			ENDIF
			
			IF IS_SCREEN_FADED_OUT()
				RESET_PED_ACTIVITIES(activityControllerStruct, activityCheck, pedLocalVariationsStruct)
				g_SimpleInteriorData.bForceCCTVCleanup = TRUE
				PRINTLN("AM_MP_BIKER_WAREHOUSE - Switching state to: FACTORY_UPGRADE_STATE_WARP_PLAYER")
				thisFactory.iUpgradeState = FACTORY_UPGRADE_STATE_WARP_PLAYER
			ENDIF
			
		BREAK
		CASE FACTORY_UPGRADE_STATE_WARP_PLAYER
		
			PRINTLN("AM_MP_BIKER_WAREHOUSE - FACTORY_UPGRADE_STATE_WARP_PLAYER")
			IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID(), FALSE)
			
				_SET_UP_CUSTOM_SPAWN_POINTS_AFTER_FACTORY_UPGRADE()
				IF WARP_TO_SPAWN_LOCATION(SPAWN_LOCATION_CUSTOM_SPAWN_POINTS, FALSE, FALSE, FALSE, FALSE, DEFAULT, DEFAULT, FALSE)
					CLEAR_CUSTOM_SPAWN_POINTS()
					USE_CUSTOM_SPAWN_POINTS(FALSE)
					thisFactory.iUpgradeState = FACTORY_UPGRADE_STATE_ACTIVATE_ENTITY_SETS
					#IF IS_DEBUG_BUILD
						PRINTLN("AM_MP_BIKER_WAREHOUSE - Warp done, switching state to: FACTORY_UPGRADE_STATE_REFRESH_INTERIOR")
					#ENDIF
				ENDIF
			ELSE
				// Don't warp the owner on the laptop
				thisFactory.iUpgradeState = FACTORY_UPGRADE_STATE_ACTIVATE_ENTITY_SETS
			ENDIF
			
		BREAK
		CASE FACTORY_UPGRADE_STATE_ACTIVATE_ENTITY_SETS
			
			PRINTLN("AM_MP_BIKER_WAREHOUSE - FACTORY_UPGRADE_STATE_ACTIVATE_ENTITY_SETS")
			IF IS_VALID_INTERIOR(thisFactory.FactoryInteriorID)
				SWITCH eFactoryType
					CASE FACTORY_TYPE_WEED
						IF eUpgradeID = UPGRADE_ID_EQUIPMENT
							REMOVE_ENTITY_SET(thisFactory.eID, "weed_standard_equip")
							ADD_ENTITY_SET(thisFactory.eID, "weed_upgrade_equip")
							
							// Deactive standard lighting
							REMOVE_ENTITY_SET(thisFactory.eID, "light_growtha_stage23_standard")
							REMOVE_ENTITY_SET(thisFactory.eID, "light_growthb_stage23_standard")
							REMOVE_ENTITY_SET(thisFactory.eID, "light_growthc_stage23_standard")
							REMOVE_ENTITY_SET(thisFactory.eID, "light_growthd_stage23_standard")
							REMOVE_ENTITY_SET(thisFactory.eID, "light_growthe_stage23_standard")
							REMOVE_ENTITY_SET(thisFactory.eID, "light_growthf_stage23_standard")
							REMOVE_ENTITY_SET(thisFactory.eID, "light_growthg_stage23_standard")
							REMOVE_ENTITY_SET(thisFactory.eID, "light_growthh_stage23_standard")
							REMOVE_ENTITY_SET(thisFactory.eID, "light_growthi_stage23_standard")
							
							// Activate upgraded lighting
							ADD_ENTITY_SET(thisFactory.eID, "light_growtha_stage23_upgrade")
							ADD_ENTITY_SET(thisFactory.eID, "light_growthb_stage23_upgrade")
							ADD_ENTITY_SET(thisFactory.eID, "light_growthc_stage23_upgrade")
							ADD_ENTITY_SET(thisFactory.eID, "light_growthd_stage23_upgrade")
							ADD_ENTITY_SET(thisFactory.eID, "light_growthe_stage23_upgrade")
							ADD_ENTITY_SET(thisFactory.eID, "light_growthf_stage23_upgrade")
							ADD_ENTITY_SET(thisFactory.eID, "light_growthg_stage23_upgrade")
							ADD_ENTITY_SET(thisFactory.eID, "light_growthh_stage23_upgrade")
							ADD_ENTITY_SET(thisFactory.eID, "light_growthi_stage23_upgrade")
							
						ELIF eUpgradeID = UPGRADE_ID_SECURITY
							REMOVE_ENTITY_SET(thisFactory.eID, "weed_low_security")
							ADD_ENTITY_SET(thisFactory.eID, "weed_security_upgrade")
						ENDIF
					BREAK
					CASE FACTORY_TYPE_FAKE_IDS
						IF eUpgradeID = UPGRADE_ID_EQUIPMENT
							REMOVE_ENTITY_SET(thisFactory.eID, "equipment_basic")
							REMOVE_ENTITY_SET(thisFactory.eID, "equipment_downgrade")
							REMOVE_ENTITY_SET(thisFactory.eID, "interior_basic")
							REMOVE_ENTITY_SET(thisFactory.eID, "clutter")
							ADD_ENTITY_SET(thisFactory.eID, "equipment_upgrade")
							ADD_ENTITY_SET(thisFactory.eID, "interior_upgrade")
							
						ELIF eUpgradeID = UPGRADE_ID_SECURITY
							REMOVE_ENTITY_SET(thisFactory.eID, "security_low")
							ADD_ENTITY_SET(thisFactory.eID, "security_high")
						ELIF eUpgradeID = UPGRADE_ID_STAFF
							REMOVE_ENTITY_SET(thisFactory.eID, "Chair05")
							REMOVE_ENTITY_SET(thisFactory.eID, "Chair04")
							REMOVE_ENTITY_SET(thisFactory.eID, "Chair07")
						ENDIF
					BREAK
					CASE FACTORY_TYPE_CRACK
						IF eUpgradeID = UPGRADE_ID_EQUIPMENT
							REMOVE_ENTITY_SET(thisFactory.eID, "production_basic")
							REMOVE_ENTITY_SET(thisFactory.eID, "equipment_basic")
							ADD_ENTITY_SET(thisFactory.eID, "equipment_upgrade")
							
							IF GET_MATERIALS_TOTAL_FOR_FACTORY(thisFactory.pOwner, thisFactory.eID) <= 0
							OR GET_PRODUCT_TOTAL_FOR_FACTORY(thisFactory.pOwner, thisFactory.eID) = GET_FACTORY_PRODUCT_CAPACITY(thisFactory.eID)
							OR GET_FACTORY_PRODUCTION_STAGE(thisFactory.pOwner, thisFactory.eID) < 2
								// Only change if peds idle or production in an inactive stage
								REMOVE_ENTITY_SET(thisFactory.eID, "coke_press_basic")
								ADD_ENTITY_SET(thisFactory.eID, "coke_press_upgrade")
							ENDIF
							
							IF DOES_PLAYER_OWN_FACTORY_UPGRADE(thisFactory.pOwner, thisFactory.eID, UPGRADE_ID_STAFF)
								ADD_ENTITY_SET(thisFactory.eID, "table_equipment_upgrade")
								ADD_ENTITY_SET(thisFactory.eID, "coke_cut_04")
								ADD_ENTITY_SET(thisFactory.eID, "coke_cut_05")
							ENDIF
							
						ELIF eUpgradeID = UPGRADE_ID_SECURITY
							REMOVE_ENTITY_SET(thisFactory.eID, "security_low")
							ADD_ENTITY_SET(thisFactory.eID, "security_high")
						
						ELIF eUpgradeID = UPGRADE_ID_STAFF
							IF DOES_PLAYER_OWN_FACTORY_UPGRADE(thisFactory.pOwner, thisFactory.eID, UPGRADE_ID_EQUIPMENT)
								ADD_ENTITY_SET(thisFactory.eID, "coke_cut_04")
								ADD_ENTITY_SET(thisFactory.eID, "coke_cut_05")
							ENDIF
						ENDIF
					BREAK
					CASE FACTORY_TYPE_METH
						IF eUpgradeID = UPGRADE_ID_EQUIPMENT
							REMOVE_ENTITY_SET(thisFactory.eID, "meth_lab_basic")
							ADD_ENTITY_SET(thisFactory.eID, "meth_lab_upgrade")
							
						ELIF eUpgradeID = UPGRADE_ID_SECURITY
							ADD_ENTITY_SET(thisFactory.eID, "meth_lab_security_high")
						ENDIF
					BREAK
					CASE FACTORY_TYPE_FAKE_MONEY
						IF eUpgradeID = UPGRADE_ID_EQUIPMENT
							REMOVE_ENTITY_SET(thisFactory.eID, "counterfeit_standard_equip")
							REMOVE_ENTITY_SET(thisFactory.eID, "counterfeit_standard_equip_no_prod")
							
							IF IS_FACTORY_PRODUCTION_ACTIVE(thisFactory.pOwner, thisFactory.iSaveSlot)
								REMOVE_ENTITY_SET(thisFactory.eID, "dryerc_off")
								REMOVE_ENTITY_SET(thisFactory.eID, "dryerd_off")
								ADD_ENTITY_SET(thisFactory.eID, "dryerc_on")
								ADD_ENTITY_SET(thisFactory.eID, "dryerd_on")
								ADD_ENTITY_SET(thisFactory.eID, "counterfeit_upgrade_equip")
							ELSE
								ADD_ENTITY_SET(thisFactory.eID, "counterfeit_upgrade_equip_no_prod")
							ENDIF
							
						ELIF eUpgradeID = UPGRADE_ID_SECURITY
							REMOVE_ENTITY_SET(thisFactory.eID, "counterfeit_low_security")
							ADD_ENTITY_SET(thisFactory.eID, "counterfeit_security")
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
			
			PRINTLN("AM_MP_BIKER_WAREHOUSE - Switching state to: FACTORY_UPGRADE_STATE_REFRESH_INTERIOR")
			thisFactory.iUpgradeState = FACTORY_UPGRADE_STATE_REFRESH_INTERIOR
			
		BREAK
		CASE FACTORY_UPGRADE_STATE_REFRESH_INTERIOR
			
			PRINTLN("AM_MP_BIKER_WAREHOUSE - FACTORY_UPGRADE_STATE_REFRESH_INTERIOR")
			REFRESH_INTERIOR(thisFactory.FactoryInteriorID)
			
			PRINTLN("AM_MP_BIKER_WAREHOUSE - Switching state to: FACTORY_UPGRADE_STATE_PRODUCTS")
			thisFactory.iUpgradeState = FACTORY_UPGRADE_STATE_PRODUCTS
			
		BREAK
		CASE FACTORY_UPGRADE_STATE_PRODUCTS
			
			PRINTLN("AM_MP_BIKER_WAREHOUSE - FACTORY_UPGRADE_STATE_PRODUCTS")
			
			SWITCH eFactoryType
				CASE FACTORY_TYPE_CRACK
					CLEANUP_ALL_FACTORY_PRODUCTS(thisFactory.eID)
					CREATE_ALL_OWNED_FACTORY_PRODUCT(thisFactory.eID, thisFactory.iProductSlot)
				BREAK
			ENDSWITCH
			
			PRINTLN("AM_MP_BIKER_WAREHOUSE - Switching state to: FACTORY_UPGRADE_STATE_TIMECYCLE_MODIFIERS")
			thisFactory.iUpgradeState = FACTORY_UPGRADE_STATE_TIMECYCLE_MODIFIERS
			
		BREAK
		CASE FACTORY_UPGRADE_STATE_TIMECYCLE_MODIFIERS
			
			PRINTLN("AM_MP_BIKER_WAREHOUSE - FACTORY_UPGRADE_STATE_TIMECYCLE_MODIFIERS")
			
			SWITCH eFactoryType
				CASE FACTORY_TYPE_WEED
					IF GET_TIMECYCLE_MODIFIER_INDEX() != -1
						//CLEAR_TIMECYCLE_MODIFIER()
						POP_TIMECYCLE_MODIFIER()
					ENDIF
					
					IF DOES_PLAYER_OWN_FACTORY_UPGRADE(thisFactory.pOwner, thisFactory.eID, UPGRADE_ID_EQUIPMENT)
						PRINTLN("AM_MP_BIKER_WAREHOUSE - activiating setting timecycle: mp_bkr_ware02_upgrade")
						SET_TIMECYCLE_MODIFIER("mp_bkr_ware02_upgrade")
					ELSE
						PRINTLN("AM_MP_BIKER_WAREHOUSE - activiating setting timecycle: mp_bkr_ware02_standard")
						SET_TIMECYCLE_MODIFIER("mp_bkr_ware02_standard")
					ENDIF
					
					IF GET_TIMECYCLE_MODIFIER_INDEX() != -1
						PUSH_TIMECYCLE_MODIFIER()
					ENDIF
					SET_BIT(thisFactory.iBS, BS_FACTORY_DATA_PUSHED_TIMECYCLE_MODIFIER)
				BREAK
				CASE FACTORY_TYPE_CRACK
					IF GET_TIMECYCLE_MODIFIER_INDEX() != -1
						//CLEAR_TIMECYCLE_MODIFIER()
						POP_TIMECYCLE_MODIFIER()
					ENDIF
					
					IF DOES_PLAYER_OWN_FACTORY_UPGRADE(thisFactory.pOwner, thisFactory.eID, UPGRADE_ID_EQUIPMENT)
						PRINTLN("AM_MP_BIKER_WAREHOUSE - activiating setting timecycle: mp_bkr_ware03_upgrade")
						SET_TIMECYCLE_MODIFIER("mp_bkr_ware03_upgrade")
					ELSE
						PRINTLN("AM_MP_BIKER_WAREHOUSE - activiating setting timecycle: mp_bkr_ware03_basic")
						SET_TIMECYCLE_MODIFIER("mp_bkr_ware03_basic")
					ENDIF
					
					IF GET_TIMECYCLE_MODIFIER_INDEX() != -1
						PUSH_TIMECYCLE_MODIFIER()
					ENDIF
					SET_BIT(thisFactory.iBS, BS_FACTORY_DATA_PUSHED_TIMECYCLE_MODIFIER)
				BREAK
			ENDSWITCH
			
			PRINTLN("AM_MP_BIKER_WAREHOUSE - Switching state to: FACTORY_UPGRADE_STATE_UPDATE_AUDIO")
			thisFactory.iUpgradeState = FACTORY_UPGRADE_STATE_UPDATE_AUDIO
			
		BREAK
		CASE FACTORY_UPGRADE_STATE_UPDATE_AUDIO
			
			PRINTLN("AM_MP_BIKER_WAREHOUSE - FACTORY_UPGRADE_STATE_UPDATE_AUDIO")
			
			CLEANUP_FACTORY_AUDIO_SOUNDS()
			INITIALISE_FACTORY_AUDIO()
			
			PRINTLN("AM_MP_BIKER_WAREHOUSE - Switching state to: FACTORY_UPGRADE_STATE_FADE_IN")
			thisFactory.iUpgradeState = FACTORY_UPGRADE_STATE_FADE_IN
			
		BREAK
		CASE FACTORY_UPGRADE_STATE_FADE_IN
			
			PRINTLN("AM_MP_BIKER_WAREHOUSE - FACTORY_UPGRADE_STATE_FADE_IN")
			IF IS_INTERIOR_READY(thisFactory.FactoryInteriorID)
				PRINTLN("AM_MP_BIKER_WAREHOUSE - interior is ready")
				
				//IF PLAYER_ID() != thisFactory.pOwner
					//FORCE_ROOM_FOR_ENTITY(PLAYER_PED_ID(), thisFactory.FactoryInteriorID, GET_FACTORY_INTERIOR_ROOM_KEY(thisFactory.eFactoryType))
				//ENDIF
				
				IF IS_SCREEN_FADED_OUT()
					PRINTLN("AM_MP_BIKER_WAREHOUSE - screen has faded out")
					IF HAS_NET_TIMER_EXPIRED(thisFactory.upgradeTimer, ciBIKER_UPGRADE_DELAY)
						PRINTLN("AM_MP_BIKER_WAREHOUSE - net timer has expired")
						
						DO_SCREEN_FADE_IN(500)
						NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						
						IF thisFactory.pOwner = PLAYER_ID()
							SET_PLAYER_HAS_JUST_PURCHASED_FACTORY_UPGRADE(thisFactory.pOwner, thisFactory.eID, thisFactory.eUpgradeID, FALSE)
						ENDIF
						
						thisFactory.bApplyUpgrade = FALSE
						thisFactory.iUpgradeState = FACTORY_UPGRADE_STATE_FADE_OUT
						RESET_NET_TIMER(thisFactory.upgradeTimer)
						
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC

/// PURPOSE:
///    Periodically runs a check to see if our upgrade entity sets do not match those of the owener: url:bugstar:3042140
PROC RUN_CHECKS_FOR_ENTITY_SET_MISMATCH(PLAYER_INDEX pOwner, FACTORY_ID eFactory)
	IF pOwner != INVALID_PLAYER_INDEX()
	AND pOwner != PLAYER_ID()
		IF (GET_FRAME_COUNT() % 10 = 0)
			FACTORY_TYPE eFactoryType = GET_GOODS_CATEGORY_FROM_FACTORY_ID(eFactory)
			
			SWITCH eFactoryType
				CASE FACTORY_TYPE_WEED
					IF DOES_PLAYER_OWN_FACTORY_UPGRADE(pOwner, eFactory, UPGRADE_ID_EQUIPMENT)
					AND IS_INTERIOR_ENTITY_SET_ACTIVE(thisFactory.FactoryInteriorID, "weed_standard_equip")
						thisFactory.eUpgradeID 		= UPGRADE_ID_EQUIPMENT
						thisFactory.bApplyUpgrade 	= TRUE
						PRINTLN("AM_MP_BIKER_WAREHOUSE - RUN_CHECKS_FOR_ENTITY_SET_MISMATCH begin weed equipment upgrade")
					ENDIF
					
					IF DOES_PLAYER_OWN_FACTORY_UPGRADE(pOwner, eFactory, UPGRADE_ID_SECURITY)
					AND NOT IS_INTERIOR_ENTITY_SET_ACTIVE(thisFactory.FactoryInteriorID, "weed_security_upgrade")
						thisFactory.eUpgradeID 		= UPGRADE_ID_SECURITY
						thisFactory.bApplyUpgrade 	= TRUE
						PRINTLN("AM_MP_BIKER_WAREHOUSE - RUN_CHECKS_FOR_ENTITY_SET_MISMATCH begin weed security upgrade")
					ENDIF
				BREAK
				
				CASE FACTORY_TYPE_FAKE_MONEY
					IF DOES_PLAYER_OWN_FACTORY_UPGRADE(pOwner, eFactory, UPGRADE_ID_EQUIPMENT)
					AND IS_INTERIOR_ENTITY_SET_ACTIVE(thisFactory.FactoryInteriorID, "counterfeit_standard_equip")
						thisFactory.eUpgradeID 		= UPGRADE_ID_EQUIPMENT
						thisFactory.bApplyUpgrade 	= TRUE
						PRINTLN("AM_MP_BIKER_WAREHOUSE - RUN_CHECKS_FOR_ENTITY_SET_MISMATCH begin cash equipment upgrade")
					ENDIF
					
					IF DOES_PLAYER_OWN_FACTORY_UPGRADE(pOwner, eFactory, UPGRADE_ID_SECURITY)
					AND IS_INTERIOR_ENTITY_SET_ACTIVE(thisFactory.FactoryInteriorID, "counterfeit_low_security")
						thisFactory.eUpgradeID 		= UPGRADE_ID_SECURITY
						thisFactory.bApplyUpgrade 	= TRUE
						PRINTLN("AM_MP_BIKER_WAREHOUSE - RUN_CHECKS_FOR_ENTITY_SET_MISMATCH begin cash security upgrade")
					ENDIF
				BREAK
				
				CASE FACTORY_TYPE_METH
					IF DOES_PLAYER_OWN_FACTORY_UPGRADE(pOwner, eFactory, UPGRADE_ID_EQUIPMENT)
					AND IS_INTERIOR_ENTITY_SET_ACTIVE(thisFactory.FactoryInteriorID, "meth_lab_basic")
						thisFactory.eUpgradeID 		= UPGRADE_ID_EQUIPMENT
						thisFactory.bApplyUpgrade 	= TRUE
						PRINTLN("AM_MP_BIKER_WAREHOUSE - RUN_CHECKS_FOR_ENTITY_SET_MISMATCH begin meth equipment upgrade")
					ENDIF
					
					IF DOES_PLAYER_OWN_FACTORY_UPGRADE(pOwner, eFactory, UPGRADE_ID_SECURITY)
					AND NOT IS_INTERIOR_ENTITY_SET_ACTIVE(thisFactory.FactoryInteriorID, "meth_lab_security_high")
						thisFactory.eUpgradeID 		= UPGRADE_ID_SECURITY
						thisFactory.bApplyUpgrade 	= TRUE
						PRINTLN("AM_MP_BIKER_WAREHOUSE - RUN_CHECKS_FOR_ENTITY_SET_MISMATCH begin meth security upgrade")
					ENDIF
				BREAK
				
				CASE FACTORY_TYPE_CRACK
				CASE FACTORY_TYPE_FAKE_IDS
					IF DOES_PLAYER_OWN_FACTORY_UPGRADE(pOwner, eFactory, UPGRADE_ID_EQUIPMENT)
					AND IS_INTERIOR_ENTITY_SET_ACTIVE(thisFactory.FactoryInteriorID, "equipment_basic")
						thisFactory.eUpgradeID 		= UPGRADE_ID_EQUIPMENT
						thisFactory.bApplyUpgrade 	= TRUE
						PRINTLN("AM_MP_BIKER_WAREHOUSE - RUN_CHECKS_FOR_ENTITY_SET_MISMATCH begin crack/FID equipment upgrade")
					ENDIF
					
					IF DOES_PLAYER_OWN_FACTORY_UPGRADE(pOwner, eFactory, UPGRADE_ID_SECURITY)
					AND IS_INTERIOR_ENTITY_SET_ACTIVE(thisFactory.FactoryInteriorID, "security_low")
						thisFactory.eUpgradeID 		= UPGRADE_ID_SECURITY
						thisFactory.bApplyUpgrade 	= TRUE
						PRINTLN("AM_MP_BIKER_WAREHOUSE - RUN_CHECKS_FOR_ENTITY_SET_MISMATCH begin crack/FID securty upgrade")
					ENDIF
				BREAK
			ENDSWITCH
			
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_FACTORY_UPGRADES()
	
	IF IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
	OR thisFactory.bPlayerLeftTheFactory
		#IF IS_DEBUG_BUILD
			PRINTLN("AM_MP_BIKER_WAREHOUSE - MAINTAIN_FACTORY_UPGRADES Not running because player is transitioning in/out the factory WALKING_IN_OR_OUT: ", PICK_STRING(IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(), "TRUE", "FALSE"), ", bPlayerLeftTheFactory: ", PICK_STRING(thisFactory.bPlayerLeftTheFactory, "TRUE", "FALSE"))
		#ENDIF
		EXIT
	ENDIF
	
	IF thisFactory.bApplyUpgrade != TRUE
		RUN_CHECKS_FOR_ENTITY_SET_MISMATCH(thisFactory.pOwner, thisFactory.eID)
	ENDIF
	
	IF HAS_PLAYER_JUST_PURCHASED_FACTORY_UPGRADE(thisFactory.pOwner, thisFactory.eID, thisFactory.eUpgradeID)
		IF thisFactory.bApplyUpgrade != TRUE
			thisFactory.bApplyUpgrade = TRUE
		ENDIF
	ENDIF
	
	IF thisFactory.bApplyUpgrade
		PERFORM_FACTORY_UPGRADE(thisFactory.eFactoryType, thisFactory.eUpgradeID)
	ENDIF
	
ENDPROC

PROC MAINTAIN_FACTORY_LAPTOP_SCREEN()
	
	SWITCH thisFactory.factoryLaptopData.eLaptopState
		CASE FACTORY_LAPTOP_SCREEN_STATE_INIT
			
			REQUEST_STREAMED_TEXTURE_DICT("prop_screen_biker_laptop")
			IF HAS_STREAMED_TEXTURE_DICT_LOADED("prop_screen_biker_laptop")
				PRINTLN("MAINTAIN_FACTORY_LAPTOP_MONITOR - Changing to state: FACTORY_LAPTOP_SCREEN_STATE_LINK_RT")
				thisFactory.factoryLaptopData.eLaptopState = FACTORY_LAPTOP_SCREEN_STATE_LINK_RT
			ENDIF
			
		BREAK
		CASE FACTORY_LAPTOP_SCREEN_STATE_LINK_RT
			IF NOT IS_NAMED_RENDERTARGET_REGISTERED("prop_clubhouse_laptop_01a")
				REGISTER_NAMED_RENDERTARGET("prop_clubhouse_laptop_01a")
				IF NOT IS_NAMED_RENDERTARGET_LINKED(BKR_PROP_CLUBHOUSE_LAPTOP_01A)
					LINK_NAMED_RENDERTARGET(BKR_PROP_CLUBHOUSE_LAPTOP_01A)
					IF thisFactory.factoryLaptopData.iRenderTargetID = -1
						thisFactory.factoryLaptopData.iRenderTargetID = GET_NAMED_RENDERTARGET_RENDER_ID("prop_clubhouse_laptop_01a")
					ENDIF
				ENDIF
			ENDIF
			
			PRINTLN("MAINTAIN_FACTORY_LAPTOP_MONITOR - Changing to state: FACTORY_LAPTOP_SCREEN_STATE_UPDATE_SCALEFORM - iRenderTargetID: ", thisFactory.factoryLaptopData.iRenderTargetID)
			thisFactory.factoryLaptopData.eLaptopState = FACTORY_LAPTOP_SCREEN_STATE_UPDATE_SCALEFORM
		BREAK
		CASE FACTORY_LAPTOP_SCREEN_STATE_UPDATE_SCALEFORM
			SET_TEXT_RENDER_ID(thisFactory.factoryLaptopData.iRenderTargetID)
			SET_SCRIPT_GFX_ALIGN(UI_ALIGN_IGNORE, UI_ALIGN_IGNORE)
			SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
			SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
			
			DRAW_SPRITE_NAMED_RENDERTARGET("prop_screen_biker_laptop", "prop_screen_biker_laptop_2", 0.5, 0.5, 1.0, 1.0, 0.0, 255, 255, 255, 255)
			
			RESET_SCRIPT_GFX_ALIGN()
			SET_TEXT_RENDER_ID(GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID())
		BREAK
	ENDSWITCH
	
ENDPROC

PROC CLEANUP_CLUBHOUSE_LAPTOP_SCREEN()
	IF IS_NAMED_RENDERTARGET_REGISTERED("prop_clubhouse_laptop_01a")   
		RELEASE_NAMED_RENDERTARGET("prop_clubhouse_laptop_01a")
	ENDIF
	thisFactory.factoryLaptopData.iRenderTargetID = -1
	thisFactory.factoryLaptopData.eLaptopState = FACTORY_LAPTOP_SCREEN_STATE_INIT
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("prop_screen_biker_laptop")
ENDPROC

PROC MAINTAIN_FACTORY_EXIT_TRIGGER(BOOL bDontTrigger = FALSE)
	IF NOT IS_BIT_SET(thisFactory.iBS, BS_FACTORY_DATA_WAS_ON_EXIT_TRIGGERING_MISSION_LAST_FRAME)
		IF GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(PLAYER_ID())
			INT iMission = GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID())
			IF (thisFactory.pOwner != PLAYER_ID() AND iMission = FMMC_TYPE_BIKER_CONTRABAND_SELL AND IS_BIT_SET(g_iSellBitSet, ciGB_BIKER_SELL_SERV_INIT_DONE)) // sell kick out for boss happens via the laptop
			OR iMission = FMMC_TYPE_BIKER_BUY
				IF NOT bDontTrigger
					START_NET_TIMER(thisFactory.timerMissionKickOut)
					#IF IS_DEBUG_BUILD
						PRINTLN("AM_MP_BIKER_WAREHOUSE - MAINTAIN_FACTORY_EXIT_TRIGGER - Starting exit timer as player started biker buy/sell mission")
					#ENDIF
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("AM_MP_BIKER_WAREHOUSE - MAINTAIN_FACTORY_EXIT_TRIGGER - We are on a buy/sell mission but re-entering the warehouse.")
				#ENDIF
				ENDIF
				SET_BIT(thisFactory.iBS, BS_FACTORY_DATA_WAS_ON_EXIT_TRIGGERING_MISSION_LAST_FRAME)
			ENDIF
		ENDIF
	ELSE
		IF NOT GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(PLAYER_ID())
			CLEAR_BIT(thisFactory.iBS, BS_FACTORY_DATA_WAS_ON_EXIT_TRIGGERING_MISSION_LAST_FRAME)
		ENDIF
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(thisFactory.timerMissionKickOut)
		IF HAS_NET_TIMER_EXPIRED(thisFactory.timerMissionKickOut, 1500)
			TRIGGER_EXIT_FROM_SIMPLE_INTERIOR_NOW()
			#IF IS_DEBUG_BUILD
				PRINTLN("AM_MP_BIKER_WAREHOUSE - MAINTAIN_FACTORY_EXIT_TRIGGER - Triggering exit!")
			#ENDIF
			RESET_NET_TIMER(thisFactory.timerMissionKickOut)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Lock the door in document forgery business as they sometimes open on their own...
PROC MAINTAIN_INSIDE_DOOR()
	IF IS_BIT_SET(thisFactory.iBS, BS_FACTORY_DATA_DOOR_LOCKED)
		EXIT
	ENDIF
	
	BOOL bLockDoor
	INT iDoorHash
	VECTOR vDoorPos
	TEXT_LABEL_63 txtDoorHash
	MODEL_NAMES modelDoor
	
	IF thisFactory.eID = FACTORY_ID_FAKE_ID_1
	OR thisFactory.eID = FACTORY_ID_FAKE_ID_2
	OR thisFactory.eID = FACTORY_ID_FAKE_ID_3
	OR thisFactory.eID = FACTORY_ID_FAKE_ID_4
		bLockDoor = TRUE
		vDoorPos = <<1174.1702, -3197.2778, -38.8676>>
		txtDoorHash = "BWH_FAKE_ID_EXIT_DOOR"
		modelDoor = V_ILEV_FIB_DOOR1
	ELIF thisFactory.eID = FACTORY_ID_METH_1
	OR thisFactory.eID = FACTORY_ID_METH_2
	OR thisFactory.eID = FACTORY_ID_METH_3
	OR thisFactory.eID = FACTORY_ID_METH_4
		bLockDoor = TRUE
		vDoorPos = <<996.5116, -3201.3459, -36.1669>>
		txtDoorHash = "BWH_METH_EXIT_DOOR"
		modelDoor = V_ILEV_TORT_DOOR
	ENDIF
	
	IF NOT bLockDoor
		SET_BIT(thisFactory.iBS, BS_FACTORY_DATA_DOOR_LOCKED)
		EXIT
	ENDIF
	
	IF NOT DOOR_SYSTEM_FIND_EXISTING_DOOR(vDoorPos, modelDoor, iDoorHash)
		iDoorHash = GET_HASH_KEY(txtDoorHash)
		IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(iDoorHash)
			ADD_DOOR_TO_SYSTEM(iDoorHash, modelDoor, vDoorPos, FALSE, FALSE)
			#IF IS_DEBUG_BUILD
				PRINTLN("AM_MP_BIKER_WAREHOUSE - MAINTAIN_INSIDE_DOOR - ADD_DOOR_TO_SYSTEM - Added ", GET_MODEL_NAME_FOR_DEBUG(modelDoor), " door to system!")
			#ENDIF
		ENDIF
	ENDIF
	
	DOOR_SYSTEM_SET_DOOR_STATE(iDoorHash, DOORSTATE_LOCKED, FALSE, FALSE)
	DOOR_SYSTEM_SET_OPEN_RATIO(iDoorHash, 0.0, FALSE, FALSE)
	
	#IF IS_DEBUG_BUILD
		PRINTLN("AM_MP_BIKER_WAREHOUSE - MAINTAIN_INSIDE_DOOR - DOOR_SYSTEM_SET_DOOR_STATE - Done!")
	#ENDIF
	
	SET_BIT(thisFactory.iBS, BS_FACTORY_DATA_DOOR_LOCKED)
ENDPROC

FUNC STRING GET_FACTORY_AUDIO_SCENE()
	SWITCH thisFactory.eID
		CASE FACTORY_ID_CRACK_1
		CASE FACTORY_ID_CRACK_2
		CASE FACTORY_ID_CRACK_3
		CASE FACTORY_ID_CRACK_4
			RETURN "Biker_Warehouses_Coke_Scene"
		BREAK
		CASE FACTORY_ID_FAKE_CASH_1
		CASE FACTORY_ID_FAKE_CASH_2
		CASE FACTORY_ID_FAKE_CASH_3
		CASE FACTORY_ID_FAKE_CASH_4
			RETURN "Biker_Warehouses_Printers_Scene"
		BREAK
		CASE FACTORY_ID_FAKE_ID_1
		CASE FACTORY_ID_FAKE_ID_2
		CASE FACTORY_ID_FAKE_ID_3
		CASE FACTORY_ID_FAKE_ID_4
			RETURN "Biker_Warehouses_FakeID_Scene"
		BREAK
		CASE FACTORY_ID_METH_1
		CASE FACTORY_ID_METH_2
		CASE FACTORY_ID_METH_3
		CASE FACTORY_ID_METH_4
			RETURN "Biker_Warehouses_Meth_Scene"
		BREAK
		CASE FACTORY_ID_WEED_1
		CASE FACTORY_ID_WEED_2
		CASE FACTORY_ID_WEED_3
		CASE FACTORY_ID_WEED_4
			RETURN "Biker_Warehouses_Weed_Scene"
		BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

PROC MAINTAIN_CHECKING_FOR_PLAYERS_NEARBY_THIS_FACTORY()
	VECTOR vEntranceCoords, vPlayerCoords
	FLOAT fDist
	
	GET_FACTORY_ENTRY_LOCATE(GET_SIMPLE_INTERIOR_ID_FROM_FACTORY_ID(thisFactory.eID), vEntranceCoords)
	
	PLAYER_INDEX playerID = INT_TO_PLAYERINDEX(thisFactory.iPlayersAroundEntranceStagger)
	
	IF IS_NET_PLAYER_OK(playerID)
		vPlayerCoords = GET_PLAYER_COORDS(playerID)
		fDist = VDIST(vPlayerCoords, vEntranceCoords)
		IF fDist < 80.0
			#IF IS_DEBUG_BUILD
				PRINTLN("AM_MP_BIKER_WAREHOUSE - MAINTAIN_CHECKING_FOR_PLAYERS_NEARBY_THIS_FACTORY - Player ", GET_PLAYER_NAME(playerID), " is nearby (distance: ", fDist, ").")
			#ENDIF
			thisFactory.iTempNumOfPlayersAroundEntrance += 1
		ENDIF
	ENDIF
	
	thisFactory.iPlayersAroundEntranceStagger = (thisFactory.iPlayersAroundEntranceStagger + 1) % NUM_NETWORK_PLAYERS
	
	IF thisFactory.iPlayersAroundEntranceStagger = 0
		IF thisFactory.iTempNumOfPlayersAroundEntrance > 0
			thisFactory.bThereArePlayersAroundTheEntrance = TRUE
		ELSE
			thisFactory.bThereArePlayersAroundTheEntrance = FALSE
		ENDIF
		thisFactory.iTempNumOfPlayersAroundEntrance = 0
	ENDIF
ENDPROC

FUNC BOOL _IS_THIS_ENTITY_IN_A_SAFE_PARKING_SPOT(ENTITY_INDEX entity)
	IF thisFactory.eID = FACTORY_ID_WEED_2
		// Spots on the fences around weed city business
		IF IS_ENTITY_IN_ANGLED_AREA(entity, <<97.487762,166.323532,103.550850>>, <<91.494141,149.176987,109.371674>>, 2.500000)
		OR IS_ENTITY_IN_ANGLED_AREA(entity, <<105.475143,162.094910,103.542030>>, <<96.373642,165.429398,109.069382>>, 2.500000)
		OR IS_ENTITY_IN_ANGLED_AREA(entity, <<109.635330,160.695450,103.800812>>, <<106.899506,161.686081,109.031769>>, 2.500000)
			#IF IS_DEBUG_BUILD
				PRINTLN("AM_MP_BIKER_WAREHOUSE - _IS_THIS_ENTITY_IN_A_SAFE_PARKING_SPOT - Entity ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(entity)), " - nope, unsafe.")
			#ENDIF
			RETURN FALSE
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTLN("AM_MP_BIKER_WAREHOUSE - _IS_THIS_ENTITY_IN_A_SAFE_PARKING_SPOT - Entity ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(entity)), " - yeah, all good.")
	#ENDIF
	RETURN TRUE
ENDFUNC

FUNC BOOL MOVED_VEHICLES_TO_SAFE_SPOT()
	IF IS_BIT_SET(thisFactory.iBS, BS_FACTORY_DATA_MOVED_VEHICLES_TO_SAFE_SPOTS)
		RETURN TRUE
	ENDIF
	
	IF thisFactory.eID != FACTORY_ID_WEED_2
		SET_BIT(thisFactory.iBS, BS_FACTORY_DATA_MOVED_VEHICLES_TO_SAFE_SPOTS)
		#IF IS_DEBUG_BUILD
			PRINTLN("AM_MP_BIKER_WAREHOUSE - MOVED_VEHICLES_TO_SAFE_SPOT - Not an affected business, nothing to do here.")
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF NOT thisFactory.bThereArePlayersAroundTheEntrance
		VECTOR vEntranceCoords
		GET_FACTORY_ENTRY_LOCATE(GET_SIMPLE_INTERIOR_ID_FROM_FACTORY_ID(thisFactory.eID), vEntranceCoords)
		
		VEHICLE_INDEX vehPersonal = PERSONAL_VEHICLE_ID() // GET_PLAYERS_LAST_VEHICLE() // 
		
		IF vehPersonal = NULL
		OR NOT DOES_ENTITY_EXIST(vehPersonal)
		OR IS_ENTITY_DEAD(vehPersonal)
		OR _IS_THIS_ENTITY_IN_A_SAFE_PARKING_SPOT(vehPersonal)
			vehPersonal = GET_PLAYERS_LAST_VEHICLE()
		ENDIF
		
		IF vehPersonal != NULL AND DOES_ENTITY_EXIST(vehPersonal) AND NOT IS_ENTITY_DEAD(vehPersonal)
		
			#IF IS_DEBUG_BUILD
				PRINTLN("AM_MP_BIKER_WAREHOUSE - MOVED_VEHICLES_TO_SAFE_SPOT - Ok got last players vehicle to check: ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(vehPersonal)))
			#ENDIF
	
			IF NOT _IS_THIS_ENTITY_IN_A_SAFE_PARKING_SPOT(vehPersonal)
			
				#IF IS_DEBUG_BUILD
					PRINTLN("AM_MP_BIKER_WAREHOUSE - MOVED_VEHICLES_TO_SAFE_SPOT - It needs to be moved!")
				#ENDIF
			
				IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(vehPersonal)
					NETWORK_REQUEST_CONTROL_OF_ENTITY(vehPersonal)
				ENDIF
				
				IF NETWORK_HAS_CONTROL_OF_ENTITY(vehPersonal)
					// Move it
					VECTOR vSpawnCoords
					FLOAT fSpawnHeading
					
					VEHICLE_SPAWN_LOCATION_PARAMS spawnParams
					spawnParams.fMinDistFromCoords = 0.0
					spawnParams.fMaxDistance = 35.0
					
					#IF IS_DEBUG_BUILD
						PRINTLN("AM_MP_BIKER_WAREHOUSE - MOVED_VEHICLES_TO_SAFE_SPOT - OK gonna try to move it!")
					#ENDIF
					
					IF HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(vEntranceCoords, <<0.0, 0.0, 0.0>>, GET_ENTITY_MODEL(vehPersonal), FALSE, vSpawnCoords, fSpawnHeading, spawnParams)
						SET_ENTITY_COORDS(vehPersonal, vSpawnCoords)
						SET_ENTITY_HEADING(vehPersonal, fSpawnHeading)
						SET_VEHICLE_ON_GROUND_PROPERLY(vehPersonal)
						
						#IF IS_DEBUG_BUILD
							PRINTLN("AM_MP_BIKER_WAREHOUSE - MOVED_VEHICLES_TO_SAFE_SPOT - Boom done! Returning TRUE.")
						#ENDIF
						SET_BIT(thisFactory.iBS, BS_FACTORY_DATA_MOVED_VEHICLES_TO_SAFE_SPOTS)
						RETURN TRUE
					ENDIF
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
					PRINTLN("AM_MP_BIKER_WAREHOUSE - MOVED_VEHICLES_TO_SAFE_SPOT - Vehicle is in a safe spot already, returning TRUE.")
				#ENDIF
				SET_BIT(thisFactory.iBS, BS_FACTORY_DATA_MOVED_VEHICLES_TO_SAFE_SPOTS)
				RETURN TRUE
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
				PRINTLN("AM_MP_BIKER_WAREHOUSE - MOVED_VEHICLES_TO_SAFE_SPOT - Couldn't find last used vehicle or it's dead, returning TRUE.")
			#ENDIF
			SET_BIT(thisFactory.iBS, BS_FACTORY_DATA_MOVED_VEHICLES_TO_SAFE_SPOTS)
			RETURN TRUE
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
			PRINTLN("AM_MP_BIKER_WAREHOUSE - MOVED_VEHICLES_TO_SAFE_SPOT - There's some players around the factory, returning FALSE.")
		#ENDIF
		RETURN FALSE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_MOVING_PLAYER_VEHICLE_TO_SAFE_SPOT()
	IF NOT IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
		IF NOT IS_BIT_SET(thisFactory.iBS, BS_FACTORY_DATA_MOVED_VEHICLES_TO_SAFE_SPOTS)
			IF MOVED_VEHICLES_TO_SAFE_SPOT()
				#IF IS_DEBUG_BUILD
					PRINTLN("AM_MP_BIKER_WAREHOUSE - MAINTAIN_MOVING_PLAYER_VEHICLE_TO_SAFE_SPOT - Done.")
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CLEANUP_CCTV(BOOL bReturnControl)
	CLEANUP_MP_CCTV_CLIENT(playerBD[NATIVE_TO_INT(PLAYER_ID())].MPCCTVClient, MPCCTVLocal, bReturnControl)
ENDPROC


PROC CREATE_FACTORY_RADIO()
	
	IF NOT DOES_ENTITY_EXIST(thisFactory.oRadio)
		IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("prop_radio_01")))
			VECTOR vPosition
			FLOAT fHeading
			SWITCH thisFactory.eFactoryType
				CASE FACTORY_TYPE_METH
					vPosition = <<1001.9400, -3193.7649, -39.1060>>
					fHeading = 0.00
				BREAK	
				CASE FACTORY_TYPE_WEED 
					vPosition = <<1030.6600, -3204.5200, -38.2192>>
					fHeading = 75.5200
				BREAK	
				CASE FACTORY_TYPE_CRACK 
					vPosition = <<1086.0601, -3195.5601, -39.1100>>
					fHeading =  90.0000
				BREAK	
				CASE FACTORY_TYPE_FAKE_MONEY 
					vPosition = <<1130.0930, -3193.2620, -40.4976>>
					fHeading = 0.0
				BREAK	
				CASE FACTORY_TYPE_FAKE_IDS
					vPosition = <<1156.1801, -3196.3330, -38.0976>>
					fHeading = 90.000 
				BREAK
			ENDSWITCH
			
			thisFactory.oRadio = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, HASH("prop_radio_01")), vPosition, FALSE , FALSE)
			SET_ENTITY_HEADING(thisFactory.oRadio,fHeading)
			SET_ENTITY_INVINCIBLE(thisFactory.oRadio, TRUE)
			FREEZE_ENTITY_POSITION(thisFactory.oRadio, TRUE)
			SET_ENTITY_VISIBLE(thisFactory.oRadio, TRUE)
		ENDIF	
	ENDIF
	
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ CUTSCENE STUFF  ╞════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

FUNC TEXT_LABEL_23 GET_DEMO_PRODUCT_SOUND(STRING suffix)
	TEXT_LABEL_23 base
	SWITCH GET_GOODS_CATEGORY_FROM_FACTORY_ID(thisFactory.eID)
		CASE FACTORY_TYPE_WEED base = "Weed_Tick_" BREAK
		CASE FACTORY_TYPE_METH base = "Meth_Tick_" BREAK
		CASE FACTORY_TYPE_CRACK base = "Cocaine_Tick_" BREAK
		CASE FACTORY_TYPE_FAKE_IDS base = "ID_Tick_" BREAK
		CASE FACTORY_TYPE_FAKE_MONEY base = "Cash_Tick_" BREAK
	ENDSWITCH
	
	base += suffix
	
	RETURN base
ENDFUNC

FUNC INT GET_DEMO_PRODUCT_COUNT()
	SWITCH GET_GOODS_CATEGORY_FROM_FACTORY_ID(thisFactory.eID)
		CASE FACTORY_TYPE_WEED RETURN 8
		CASE FACTORY_TYPE_METH RETURN 4
		CASE FACTORY_TYPE_CRACK RETURN 3
		CASE FACTORY_TYPE_FAKE_IDS RETURN 8
		CASE FACTORY_TYPE_FAKE_MONEY RETURN 6
	ENDSWITCH
	
	RETURN 0
ENDFUNC

FUNC INT GET_DEMO_PRODUCT_TIME(BOOL bDespawn = FALSE)
	UNUSED_PARAMETER(bDespawn)
	INT iTime
	SWITCH GET_GOODS_CATEGORY_FROM_FACTORY_ID(thisFactory.eID)
		CASE FACTORY_TYPE_WEED iTime = CUTSCENE_DEMO_PRODUCT_FULL_FILL_TIME_WEED BREAK
		CASE FACTORY_TYPE_METH iTime = CUTSCENE_DEMO_PRODUCT_FULL_FILL_TIME_METH BREAK
		CASE FACTORY_TYPE_CRACK iTime = CUTSCENE_DEMO_PRODUCT_FULL_FILL_TIME_CRACK BREAK
		CASE FACTORY_TYPE_FAKE_IDS iTime = CUTSCENE_DEMO_PRODUCT_FULL_FILL_TIME_FAKE_ID BREAK
		CASE FACTORY_TYPE_FAKE_MONEY iTime = CUTSCENE_DEMO_PRODUCT_FULL_FILL_TIME_MONEY BREAK
	ENDSWITCH
	
	RETURN iTime
ENDFUNC

PROC CREATE_DEMO_PRODUCT()
	IF NOT DOES_ENTITY_EXIST(thisFactory.tutPropData.oTUTProductObject[0])
		MODEL_NAMES eProductModel = GET_FACTORY_PRODUCT_MODEL(thisFactory.pOwner, thisFactory.eID)
		REQUEST_MODEL(eProductModel)
		
		IF IS_MODEL_VALID(eProductModel)
		AND HAS_MODEL_LOADED(eProductModel)
			INT i
			VECTOR vCoords, vRot
			REPEAT GET_FACTORY_PRODUCT_CAPACITY(thisFactory.eID) i
				vCoords = GET_FACTORY_PRODUCT_COORDS(thisFactory.pOwner, thisFactory.eID, i + 1)
				vRot = GET_FACTORY_PRODUCT_ROTATION(thisFactory.eID, i + 1)
				
				thisFactory.tutPropData.oTUTProductObject[i] = CREATE_OBJECT(eProductModel, vCoords, FALSE, FALSE)
				SET_ENTITY_ROTATION(thisFactory.tutPropData.oTUTProductObject[i], vRot)
				FREEZE_ENTITY_POSITION(thisFactory.tutPropData.oTUTProductObject[i], TRUE)
				SET_ENTITY_ALPHA(thisFactory.tutPropData.oTUTProductObject[i], 0, FALSE)
			ENDREPEAT
			
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_BIKER_WAREHOUSE - CREATE_DEMO_PRODUCT - Created.")
			#ENDIF
			
			SET_MODEL_AS_NO_LONGER_NEEDED(eProductModel)
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_DEMO_PRODUCT()
	INT iCurrentTime = GET_GAME_TIMER()
	
	INT i, iCurrentAlpha
	INT iStartAlpha, iEndAlpha, iStartTime, iEndTime
	REPEAT GET_FACTORY_PRODUCT_CAPACITY(thisFactory.eID) i
		IF DOES_ENTITY_EXIST(thisFactory.tutPropData.oTUTProductObject[i])
			IF thisFactory.tutPropData.iTimeOfDesiredAlpha[i] > iCurrentTime
				iStartAlpha = thisFactory.tutPropData.iStartAlpha[i]
				iEndAlpha = thisFactory.tutPropData.iDesiredAlpha[i]
				iStartTime = thisFactory.tutPropData.iTimeOfStartAlpha[i]
				iEndTime = thisFactory.tutPropData.iTimeOfDesiredAlpha[i]
				
				// This is a linear equation
				FLOAT fSlope = TO_FLOAT(iEndAlpha - iStartAlpha) / TO_FLOAT(iEndTime - iStartTime)
				FLOAT fB = iStartAlpha - (fSlope * TO_FLOAT(iStartTime))
				
				iCurrentAlpha = ROUND(fSlope * TO_FLOAT(iCurrentTime) + fB)
				
				SET_ENTITY_ALPHA(thisFactory.tutPropData.oTUTProductObject[i], iMIN(255, IMAX(0, iCurrentAlpha)), FALSE)
				#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_BIKER_WAREHOUSE - MAINTAIN_DEMO_PRODUCT - Updating alpha of entity ", i, " to ", iCurrentAlpha, "(iCurrentTime = ", iCurrentTime, ", iStarTime = ", iStartTime, ", iEndTime = ", iEndTime, ")")
				#ENDIF
			ELSE
				SET_ENTITY_ALPHA(thisFactory.tutPropData.oTUTProductObject[i], thisFactory.tutPropData.iDesiredAlpha[i], FALSE)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC CLEANUP_DEMO_PRODUCT()
	INT i
	REPEAT GET_FACTORY_PRODUCT_CAPACITY(thisFactory.eID) i
		IF DOES_ENTITY_EXIST(thisFactory.tutPropData.oTUTProductObject[i])
			DELETE_OBJECT(thisFactory.tutPropData.oTUTProductObject[i])
		ENDIF
	ENDREPEAT
	#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_BIKER_WAREHOUSE - CLEANUP_DEMO_PRODUCT - Done.")
	#ENDIF
ENDPROC

PROC SHOW_DEMO_PRODUCT(INT iAmount, INT iTime)
	#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_BIKER_WAREHOUSE - SHOW_DEMO_PRODUCT - Called with iAmount = ", iAmount, " and iTime = ", iTime, ", current time = ", GET_GAME_TIMER())
	#ENDIF
	
	iAmount = IMIN(iAmount, GET_FACTORY_PRODUCT_CAPACITY(thisFactory.eID))
	
	// First find the current index that's animating
	INT iCurrentIndex = -1
	INT i
	REPEAT GET_FACTORY_PRODUCT_CAPACITY(thisFactory.eID) i
		IF DOES_ENTITY_EXIST(thisFactory.tutPropData.oTUTProductObject[i])
		AND GET_ENTITY_ALPHA(thisFactory.tutPropData.oTUTProductObject[i]) = 0
			iCurrentIndex = i
			BREAKLOOP
		ENDIF
	ENDREPEAT
	
	IF iCurrentIndex = -1
		iCurrentIndex = GET_FACTORY_PRODUCT_CAPACITY(thisFactory.eID)
		#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_BIKER_WAREHOUSE - SHOW_DEMO_PRODUCT - iCurrentIndex = -1 so using max capacity")
		#ENDIF
	ENDIF
	
	REPEAT GET_FACTORY_PRODUCT_CAPACITY(thisFactory.eID) i
		IF DOES_ENTITY_EXIST(thisFactory.tutPropData.oTUTProductObject[i])
			IF i < iAmount
				thisFactory.tutPropData.iDesiredAlpha[i] = 255
				thisFactory.tutPropData.iStartAlpha[i] = GET_ENTITY_ALPHA(thisFactory.tutPropData.oTUTProductObject[i])
				#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_BIKER_WAREHOUSE - SHOW_DEMO_PRODUCT - Setting product ", i, " to iDesiredAlpha 255")
				#ENDIF
			ELSE
				thisFactory.tutPropData.iDesiredAlpha[i] = 0
				thisFactory.tutPropData.iStartAlpha[i] = GET_ENTITY_ALPHA(thisFactory.tutPropData.oTUTProductObject[i])
				#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_BIKER_WAREHOUSE - SHOW_DEMO_PRODUCT - Setting product ", i, " to iDesiredAlpha 0")
				#ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	INT iAmountToAnimate = ABSI(iAmount - iCurrentIndex)
	INT iTimePerObject = FLOOR(TO_FLOAT(iTime) / TO_FLOAT(iAmountToAnimate))
	INT iStartOffset = FLOOR(TO_FLOAT(iTimePerObject) / TO_FLOAT(iAmountToAnimate)) * 2
	INT iTotalTime = GET_GAME_TIMER()
	INT iStep
	BOOL bRunLoop
	
	INT iStartIndex, iEndIndex
	
	IF iCurrentIndex > iAmount
		iStep = -1
		iStartIndex = IMAX(0, iCurrentIndex - 1)
		iEndIndex = iCurrentIndex - iAmountToAnimate
		bRunLoop = TRUE
	ELIF iCurrentIndex < iAmount
		iStep = 1
		iStartIndex = iCurrentIndex
		iEndIndex = iCurrentIndex + iAmount - 1
		bRunLoop = TRUE
	ENDIF
	
	#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_BIKER_WAREHOUSE - SHOW_DEMO_PRODUCT - iCurrentIndex is ", iCurrentIndex, ", iAmountToAnimate is ", iAmountToAnimate, ", iTimePerObject is ", iTimePerObject)
		CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_BIKER_WAREHOUSE - SHOW_DEMO_PRODUCT - This loop will run from iStartIndex = ", iStartIndex, " to iEndIndex = ", iEndIndex)
	#ENDIF
	
	i = iStartIndex // 
	
	WHILE bRunLoop
		thisFactory.tutPropData.iTimeOfStartAlpha[i] = iTotalTime
		
		IF i != iStartIndex
			// Offset start times for all but first so they overlap at start
			thisFactory.tutPropData.iTimeOfStartAlpha[i] -= iStartOffset
		ENDIF
		
		iTotalTime += iTimePerObject
		thisFactory.tutPropData.iTimeOfDesiredAlpha[i] = iTotalTime
		
		IF i != iEndIndex
			// Offset end time for all but last so they overlap a bit
			thisFactory.tutPropData.iTimeOfDesiredAlpha[i] += iStartOffset
		ENDIF
		
		#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_BIKER_WAREHOUSE - SHOW_DEMO_PRODUCT - New start time for product ", i, " is ", thisFactory.tutPropData.iTimeOfStartAlpha[i], " and end time is ", thisFactory.tutPropData.iTimeOfDesiredAlpha[i])
		#ENDIF
		
		IF i = iEndIndex
			bRunLoop = FALSE
			BREAKLOOP
		ELSE
			i = i + iStep
		ENDIF
	ENDWHILE
	
	#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_BIKER_WAREHOUSE - SHOW_DEMO_PRODUCT - Done.")
	#ENDIF
ENDPROC

PROC _INIT_INTRO_CUTSCENE_DATA(SIMPLE_CUTSCENE &cutscene, INT iCutsceneHash, BOOL bForBoss)
	TEXT_LABEL_23 helpPrefix
	BOOL bSecondCutscene
	
	SWITCH iCutsceneHash
		//════════════════════════════════════════════════════════════════════
		//════════════════════════════════════════════════════════════════════
		//════════════════════════════════════════════════════════════════════
		CASE CI_BIKER_BUSINESS_CUTSCENE_WEED_1
			helpPrefix = "BWH_WEED_TUT"
			
			SIMPLE_CUTSCENE_CLEAR_ALL_DATA(cutscene)
			SIMPLE_CUTSCENE_CREATE(cutscene, "bwh_intro_weed_1", thisFactory.eSimpleInteriorID)
			
			SIMPLE_CUTSCENE_ADD_SCENE(cutscene, 8000, "establishing", <<14.1904, 14.5803, -0.0213>>, <<0.6801, -0.0000, 163.3713>>, 42.3385, <<13.1978, 11.3318, 0.0190>>, <<0.6801, -0.0000, 157.1486>>, 42.3385, 0.0500, 500, 0)
			SIMPLE_CUTSCENE_ADD_SCENE(cutscene, 8100, "drying area", <<-6.5289, -10.1863, 1.0270>>, <<0.1531, -0.0000, 5.8025>>, 40.8104, <<-7.9395, -7.9827, 1.0257>>, <<-1.1665, -0.0000, 358.1084>>, 40.8104, 0.0500, 0, 0)
			SIMPLE_CUTSCENE_ADD_SCENE(cutscene, 12500, "laptop", <<-7.1649, 0.9514, 0.4623>>, <<-3.3398, -0.0000, 286.5220>>, 13.9449, <<-6.2316, 1.3892, 0.4043>>, <<-3.0982, 0.0000, 281.0035>>, 13.9449, 0.0500, 0, 500)
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE CI_BIKER_BUSINESS_CUTSCENE_WEED_2
			bSecondCutscene = TRUE
			helpPrefix = "BWH_WEED_TUT"
			
			SIMPLE_CUTSCENE_CLEAR_ALL_DATA(cutscene)
			SIMPLE_CUTSCENE_CREATE(cutscene, "bwh_intro_weed_2", thisFactory.eSimpleInteriorID)
			
			SIMPLE_CUTSCENE_ADD_SCENE(cutscene, 8500, "est", <<14.2351, 12.8220, 0.4894>>, <<-3.3065, -0.0000, 153.9617>>, 33.7108, <<12.8866, 10.8025, 0.3424>>, <<-5.9659, -0.0000, 154.6708>>, 33.7108, 0.0500, 500, 0)
			SIMPLE_CUTSCENE_ADD_SCENE(cutscene, 17000, "shelves", <<-8.2140, -8.3713, 0.3006>>, <<3.2210, -0.0000, 351.9489>>, 35.6007, <<-8.2634, -7.0686, 0.3700>>, <<1.3174, -0.0000, 351.9489>>, 35.6007, 0.0500, 0, 0)
			SIMPLE_CUTSCENE_ADD_SCENE(cutscene, 12500, "laptop", <<-6.1687, 1.0110, 0.8188>>, <<-13.6495, -0.0000, 289.4386>>, 28.1969, <<-5.3717, 1.4932, 0.4699>>, <<-10.7223, -0.0000, 283.3191>>, 28.1969, 0.0500, 0, 500)
		BREAK
		//════════════════════════════════════════════════════════════════════
		//════════════════════════════════════════════════════════════════════
		//════════════════════════════════════════════════════════════════════
		CASE CI_BIKER_BUSINESS_CUTSCENE_METH_1
			helpPrefix = "BWH_METH_TUT"
			
			SIMPLE_CUTSCENE_CLEAR_ALL_DATA(cutscene)
			SIMPLE_CUTSCENE_CREATE(cutscene, "bwh_intro_meth_1", thisFactory.eSimpleInteriorID)
			
			SIMPLE_CUTSCENE_ADD_SCENE(cutscene, 8000, "establishing", <<-9.4316, -2.6089, 3.1750>>, <<-29.1965, 0.0010, 270.3851>>, 65.5699, <<-9.3555, -2.6125, 1.8984>>, <<-20.4069, 0.0010, 270.6035>>, 65.5699, 0.0500, 500, 0)
			SIMPLE_CUTSCENE_ADD_SCENE(cutscene, 7000, "detail", <<4.7844, -2.4282, -0.3100>>, <<1.8859, -0.0000, 21.7141>>, 35.8379, <<0.8005, -2.5591, -0.2650>>, <<1.8859, 0.0000, 32.6818>>, 35.8379, 0.0500, 0, 0)
			SIMPLE_CUTSCENE_ADD_SCENE(cutscene, 12500, "laptop", <<-5.8754, 0.3940, -0.5673>>, <<2.2618, -0.0000, 35.1024>>, 27.7762, <<-7.2169, 0.9314, -0.5268>>, <<-0.3108, -0.0000, 13.4374>>, 27.7762, 0.0500, 0, 500)
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE CI_BIKER_BUSINESS_CUTSCENE_METH_2
			bSecondCutscene = TRUE
			helpPrefix = "BWH_METH_TUT"
			
			SIMPLE_CUTSCENE_CLEAR_ALL_DATA(cutscene)
			SIMPLE_CUTSCENE_CREATE(cutscene, "bwh_intro_meth_2", thisFactory.eSimpleInteriorID)
			
			SIMPLE_CUTSCENE_ADD_SCENE(cutscene, 8000, "est", <<-9.3190, -4.3784, 0.4376>>, <<-1.2768, 0.4294, 293.8416>>, 66.4588, <<-8.0355, -3.8220, 0.4063>>, <<-1.2768, 0.4294, 293.8416>>, 66.4588, 0.0500, 500, 0)
			SIMPLE_CUTSCENE_ADD_SCENE(cutscene, 17000, "shelves", <<4.7519, -0.3984, 0.9535>>, <<-15.3915, 0.0543, 245.9866>>, 50.1051, <<4.3245, -2.4495, 0.9497>>, <<-14.6135, 0.0543, 270.1556>>, 50.1051, 0.0500, 0, 0)
			SIMPLE_CUTSCENE_ADD_SCENE(cutscene, 12500, "laptop", <<-7.2652, 0.8423, -0.4054>>, <<-4.6072, -0.0000, 10.9585>>, 26.6837, <<-7.3683, 1.2405, -0.4383>>, <<-4.6072, -0.0000, 8.4177>>, 26.6837, 0.0500, 0, 500)
		BREAK
		//════════════════════════════════════════════════════════════════════
		//════════════════════════════════════════════════════════════════════
		//════════════════════════════════════════════════════════════════════
		CASE CI_BIKER_BUSINESS_CUTSCENE_CRACK_1
			helpPrefix = "BWH_CRCK_TUT"
			
			SIMPLE_CUTSCENE_CLEAR_ALL_DATA(cutscene)
			SIMPLE_CUTSCENE_CREATE(cutscene, "bwh_intro_crack_1", thisFactory.eSimpleInteriorID)
			
			SIMPLE_CUTSCENE_ADD_SCENE(cutscene, 8500, "establishing", <<-5.7667, 7.2690, 0.0545>>, <<-0.5591, 0.3254, 203.6759>>, 52.6236, <<-6.6780, 3.8992, 0.0204>>, <<-0.5591, 0.3254, 242.5347>>, 52.6236, 0.0500, 500, 0)
			SIMPLE_CUTSCENE_ADD_SCENE(cutscene, 7000, "detail", <<0.3754, 0.8586, -0.4766>>, <<0.0295, -0.0000, 89.0737>>, 26.0765, <<4.5068, 0.8357, -0.4235>>, <<-0.9607, -0.0000, 89.7334>>, 26.0765, 0.0500, 0, 0)
			SIMPLE_CUTSCENE_ADD_SCENE(cutscene, 12500, "laptop", <<-5.3696, 3.0608, -0.3635>>, <<-4.9026, -0.0000, 109.8245>>, 26.0765, <<-6.0858, 2.5864, -0.4347>>, <<-6.2331, -0.0000, 102.8364>>, 26.0765, 0.0500, 0, 500)
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE CI_BIKER_BUSINESS_CUTSCENE_CRACK_2
			bSecondCutscene = TRUE
			helpPrefix = "BWH_CRCK_TUT"
			
			SIMPLE_CUTSCENE_CLEAR_ALL_DATA(cutscene)
			SIMPLE_CUTSCENE_CREATE(cutscene, "bwh_intro_crack_2", thisFactory.eSimpleInteriorID)
			
			SIMPLE_CUTSCENE_ADD_SCENE(cutscene, 8000, "est", <<-3.8098, 3.7849, -0.1060>>, <<-4.4690, -0.0000, 185.0120>>, 29.5812, <<-0.0228, 3.4885, -0.2085>>, <<-4.4690, 0.0000, 180.1065>>, 29.5812, 0.0500, 500, 0)
			SIMPLE_CUTSCENE_ADD_SCENE(cutscene, 17000, "storage", <<5.6705, -1.2598, 0.0797>>, <<-15.1557, 0.0543, 293.2990>>, 45.4907, <<5.6539, 1.0364, 0.0085>>, <<-14.9861, 0.0543, 260.6721>>, 45.4907, 0.0500, 0, 0)
			SIMPLE_CUTSCENE_ADD_SCENE(cutscene, 12500, "laptop", <<-5.3322, 2.3506, 0.0023>>, <<-18.0995, 0.0000, 88.0398>>, 19.2378, <<-5.7130, 2.3574, -0.3709>>, <<-8.9529, -0.0000, 90.6181>>, 19.2378, 0.0500, 0, 500)
		BREAK
		//════════════════════════════════════════════════════════════════════
		//════════════════════════════════════════════════════════════════════
		//════════════════════════════════════════════════════════════════════
		CASE CI_BIKER_BUSINESS_CUTSCENE_FAKE_MONEY_1
			helpPrefix = "BWH_CASH_TUT"
			
			SIMPLE_CUTSCENE_CLEAR_ALL_DATA(cutscene)
			SIMPLE_CUTSCENE_CREATE(cutscene, "bwh_intro_fake_money_1", thisFactory.eSimpleInteriorID)
			
			SIMPLE_CUTSCENE_ADD_SCENE(cutscene, 8500, "establishing", <<12.8396, 0.5125, -0.1067>>, <<-5.5372, 0.0000, 89.1305>>, 55.0581, <<9.0612, 0.5969, -0.4730>>, <<-5.5372, 0.0000, 89.1305>>, 55.0581, 0.0500, 500, 0)
			SIMPLE_CUTSCENE_ADD_SCENE(cutscene, 7500, "detail", <<-7.7939, 2.6033, -1.5720>>, <<-2.2365, 0.0000, 185.8322>>, 51.2877, <<-7.0431, 2.3779, -1.5857>>, <<-2.2365, 0.0000, 214.1964>>, 51.2877, 0.0500, 0, 0)
			SIMPLE_CUTSCENE_ADD_SCENE(cutscene, 12500, "laptop", <<4.7688, -0.4536, -1.2073>>, <<-11.5703, -0.0000, 355.8767>>, 19.5980, <<4.8339, 1.1013, -1.5829>>, <<-9.4752, -0.0000, 355.7222>>, 19.5980, 0.0500, 0, 500)
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE CI_BIKER_BUSINESS_CUTSCENE_FAKE_MONEY_2
			bSecondCutscene = TRUE
			helpPrefix = "BWH_CASH_TUT"
			
			SIMPLE_CUTSCENE_CLEAR_ALL_DATA(cutscene)
			SIMPLE_CUTSCENE_CREATE(cutscene, "bwh_intro_fake_money_2", thisFactory.eSimpleInteriorID)
			
			SIMPLE_CUTSCENE_ADD_SCENE(cutscene, 8500, "establishing", <<11.7859, 2.7458, -1.0244>>, <<-2.6769, 0.3045, 117.8105>>, 51.0818, <<8.4652, 2.7581, -1.1699>>, <<-2.6769, 0.3045, 129.9328>>, 51.0818, 0.0500, 500, 0)
			SIMPLE_CUTSCENE_ADD_SCENE(cutscene, 17000, "detail", <<-7.6213, -0.7544, -0.8840>>, <<-16.0873, 0.0543, 341.0746>>, 45.6083, <<-4.7753, -0.9043, -0.9608>>, <<-16.7817, 0.0543, 13.5590>>, 45.6083, 0.0500, 0, 0)
			SIMPLE_CUTSCENE_ADD_SCENE(cutscene, 12500, "laptop", <<4.7172, 1.9983, -1.8117>>, <<-7.2639, -0.0000, 347.7141>>, 22.0221, <<4.7413, 1.4888, -1.7486>>, <<-7.2639, -0.0000, 350.8928>>, 22.0221, 0.0500, 0, 500)
		BREAK
		//════════════════════════════════════════════════════════════════════
		//════════════════════════════════════════════════════════════════════
		//════════════════════════════════════════════════════════════════════
		CASE CI_BIKER_BUSINESS_CUTSCENE_FAKE_ID_1
			helpPrefix = "BWH_ID_TUT"
			
			SIMPLE_CUTSCENE_CLEAR_ALL_DATA(cutscene)
			SIMPLE_CUTSCENE_CREATE(cutscene, "bwh_intro_fake_id_1", thisFactory.eSimpleInteriorID)
			
			SIMPLE_CUTSCENE_ADD_SCENE(cutscene, 8500, "establishing", <<8.8148, -2.3389, 0.0597>>, <<-7.5462, -0.0000, 67.0342>>, 24.2499, <<7.7472, -1.8657, -0.0737>>, <<-3.5846, 0.0000, 75.1344>>, 24.2499, 0.0500, 500, 0)
			SIMPLE_CUTSCENE_ADD_SCENE(cutscene, 7500, "detail", <<-8.7659, -1.6423, -0.6221>>, <<-0.0930, -0.0000, 304.6277>>, 27.0230, <<-8.5006, -1.0615, -0.6220>>, <<0.2242, -0.0000, 321.9795>>, 27.0230, 0.0500, 0, 0)
			SIMPLE_CUTSCENE_ADD_SCENE(cutscene, 12500, "laptop", <<-5.1025, 4.7981, -0.7234>>, <<-7.0501, -0.0000, 198.3635>>, 27.0230, <<-4.7155, 4.9072, -0.7169>>, <<-7.3223, -0.0000, 181.6511>>, 27.0230, 0.0500, 0, 500)
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE CI_BIKER_BUSINESS_CUTSCENE_FAKE_ID_2
			bSecondCutscene = TRUE
			helpPrefix = "BWH_ID_TUT"
			
			SIMPLE_CUTSCENE_CLEAR_ALL_DATA(cutscene)
			SIMPLE_CUTSCENE_CREATE(cutscene, "bwh_intro_fake_id_2", thisFactory.eSimpleInteriorID)
			
			SIMPLE_CUTSCENE_ADD_SCENE(cutscene, 8000, "establishing", <<6.1974, 0.1077, 1.5128>>, <<-42.2418, 0.0357, 90.1732>>, 50.0000, <<3.4540, 0.1077, 0.5973>>, <<-11.2852, 0.0357, 90.1732>>, 50.0000, 0.0500, 500, 0)
			SIMPLE_CUTSCENE_ADD_SCENE(cutscene, 17000, "detail", <<0.4869, 4.3911, -0.5739>>, <<-1.3259, -0.0000, 64.5097>>, 43.1212, <<-0.7479, 4.7292, -0.6036>>, <<-1.3259, 0.0000, 73.1108>>, 43.1212, 0.0500, 0, 0)
			SIMPLE_CUTSCENE_ADD_SCENE(cutscene, 12500, "laptop", <<-4.5405, 5.3601, -0.6719>>, <<-7.0140, -0.0000, 171.0523>>, 31.6359, <<-4.6979, 4.7576, -0.7478>>, <<-7.0140, -0.0000, 176.5675>>, 31.6359, 0.0500, 0, 500)
		BREAK
		//════════════════════════════════════════════════════════════════════
		//════════════════════════════════════════════════════════════════════
		//════════════════════════════════════════════════════════════════════
	ENDSWITCH
	
	// Create all help texts based on which cutscene and if we're boss and if production started
	
	TEXT_LABEL_23 txtHelps[4]
	
	IF NOT bSecondCutscene
		txtHelps[0] = helpPrefix IF (NOT bForBoss) txtHelps[0] += "P" ENDIF txtHelps[0] += "_A1"
		txtHelps[1] = helpPrefix txtHelps[1] += "_A2"
		txtHelps[2] = helpPrefix IF (NOT bForBoss) txtHelps[2] += "P" ENDIF txtHelps[2] += "_A3"
		
		SIMPLE_CUTSCENE_ADD_HELP(cutscene, 500, cutscene.sScenes[0].iDuration - 500, txtHelps[0])
		SIMPLE_CUTSCENE_ADD_HELP(cutscene, cutscene.sScenes[0].iDuration, cutscene.sScenes[1].iDuration, txtHelps[1])
		SIMPLE_CUTSCENE_ADD_HELP(cutscene, cutscene.sScenes[0].iDuration + cutscene.sScenes[1].iDuration, cutscene.sScenes[2].iDuration, txtHelps[2])
	ELSE
		txtHelps[0] = helpPrefix txtHelps[0] += "_B1"
		txtHelps[1] = helpPrefix txtHelps[1] += "_B2"
		txtHelps[2] = helpPrefix IF (NOT bForBoss) txtHelps[2] += "P" ENDIF txtHelps[2] += "_B3"
		txtHelps[3] = helpPrefix IF (NOT bForBoss) txtHelps[3] += "P" ENDIF txtHelps[3] += "_B4"
		
		IF ARE_ANY_WORKERS_IN_THIS_FACTORY(thisFactory.pOwner, thisFactory.eID)
			// Alternative texts for when production already started
			txtHelps[0] += "a"
			txtHelps[1] += "a"
		ENDIF
		
		SIMPLE_CUTSCENE_ADD_HELP(cutscene, 500, cutscene.sScenes[0].iDuration - 500, txtHelps[0])
		SIMPLE_CUTSCENE_ADD_HELP(cutscene, cutscene.sScenes[0].iDuration, 7000, txtHelps[1])
		SIMPLE_CUTSCENE_ADD_HELP(cutscene, cutscene.sScenes[0].iDuration + 7000, 10000, txtHelps[2])
		SIMPLE_CUTSCENE_ADD_HELP(cutscene, cutscene.sScenes[0].iDuration + cutscene.sScenes[1].iDuration, cutscene.sScenes[2].iDuration, txtHelps[3])
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTLN("AM_MP_BIKER_WAREHOUSE - _INIT_INTRO_CUTSCENE_DATA - Created scenes...")
		INT i
		REPEAT 4 i
			PRINTLN("AM_MP_BIKER_WAREHOUSE - _INIT_INTRO_CUTSCENE_DATA - txtHelps[", i, "] = ", txtHelps[i])
		ENDREPEAT
	#ENDIF
ENDPROC

PROC MAINTAIN_STARTING_INTRO_CUTSCENE()
	IF IS_FACTORY_STATE(FACTORY_STATE_PLAYING_INTRO)
		EXIT
	ENDIF
	
	IF NOT thisFactory.introCutscene.bPlaying
		BOOL bShouldCheck
		INT iCutsceneHash
		
		IF IS_LOCAL_PLAYER_WALKING_INTO_SIMPLE_INTERIOR()
			IF NOT HAS_SIMPLE_INTERIOR_CHILD_SCRIPT_TIMED_OUT()
			AND IS_FACTORY_STATE(FACTORY_STATE_LOADING)
				bShouldCheck = TRUE
			ENDIF
		ELSE
			IF IS_FACTORY_STATE(FACTORY_STATE_IDLE)
				IF NOT IS_LOCAL_PLAYER_WALKING_OUT_OF_SIMPLE_INTERIOR()
				AND NOT IS_BROWSER_OPEN()
				AND NOT IS_SCREEN_FADED_OUT()
				AND NOT IS_MOBILE_PHONE_CALL_ONGOING()
					bShouldCheck = (GET_FRAME_COUNT() % 60 = 0)
				ENDIF
			ENDIF
		ENDIF
		
		IF bShouldCheck
			IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID(), FALSE)
				// If we're not boss just check if our boss plays the cutscene
				IF IS_PLAYER_IN_SIMPLE_CUTSCENE(GB_GET_THIS_PLAYER_GANG_BOSS(PLAYER_ID()))
					IF NOT IS_BIT_SET(thisFactory.iBS, BS_FACTORY_DATA_BOSS_WAS_WATCHING_CUTSCENE_LAST_FRAME)
						iCutsceneHash = WHICH_FACTORY_INTRO_CUTSCENE_SHOULD_BE_PLAYED(thisFactory.eID)
						SET_BIT(thisFactory.iBS, BS_FACTORY_DATA_BOSS_WAS_WATCHING_CUTSCENE_LAST_FRAME)
						#IF IS_DEBUG_BUILD
							PRINTLN("AM_MP_BIKER_WAREHOUSE - MAINTAIN_STARTING_INTRO_CUTSCENE - Boss wasn't playing the cutscene last frame but is now!")
						#ENDIF
					ENDIF
				ELSE
					IF IS_BIT_SET(thisFactory.iBS, BS_FACTORY_DATA_BOSS_WAS_WATCHING_CUTSCENE_LAST_FRAME)
						CLEAR_BIT(thisFactory.iBS, BS_FACTORY_DATA_BOSS_WAS_WATCHING_CUTSCENE_LAST_FRAME)
						#IF IS_DEBUG_BUILD
							PRINTLN("AM_MP_BIKER_WAREHOUSE - MAINTAIN_STARTING_INTRO_CUTSCENE - Boss stopped playing cutscene!")
						#ENDIF
					ENDIF
				ENDIF
			ELSE
				// If we're boss - directly check which cutscene which should play, if any
				iCutsceneHash = WHICH_FACTORY_INTRO_CUTSCENE_SHOULD_BE_PLAYED(thisFactory.eID)
			ENDIF
		ENDIF
		
		IF iCutsceneHash != 0
			#IF IS_DEBUG_BUILD
				PRINTLN("AM_MP_BIKER_WAREHOUSE - MAINTAIN_STARTING_INTRO_CUTSCENE - Gonna start cutscene with hash ", iCutsceneHash)
			#ENDIF
			
			_INIT_INTRO_CUTSCENE_DATA(thisFactory.introCutscene, iCutsceneHash, (thisFactory.pOwner = PLAYER_ID()))
			SIMPLE_CUTSCENE_START(thisFactory.introCutscene)
			
			IF iCutsceneHash = CI_BIKER_BUSINESS_CUTSCENE_WEED_1
			OR iCutsceneHash = CI_BIKER_BUSINESS_CUTSCENE_CRACK_1
			OR iCutsceneHash = CI_BIKER_BUSINESS_CUTSCENE_FAKE_ID_1
			OR iCutsceneHash = CI_BIKER_BUSINESS_CUTSCENE_METH_1
			OR iCutsceneHash = CI_BIKER_BUSINESS_CUTSCENE_FAKE_MONEY_1
				SET_BIT(thisFactory.iBS, BS_FACTORY_DATA_PLAYING_FIRST_CUTSCENE)
			ELSE
				SET_BIT(thisFactory.iBS, BS_FACTORY_DATA_PLAYING_SECOND_CUTSCENE)
			ENDIF
			
			IF NOT HAS_SIMPLE_INTERIOR_CHILD_SCRIPT_TIMED_OUT()
			AND IS_FACTORY_STATE(FACTORY_STATE_LOADING)
				SET_SIMPLE_INTERIOR_CHILD_SCRIPT_IS_IN_CONTROL(TRUE)
				SET_SIMPLE_INTERIOR_CHILD_SCRIPT_READY(FALSE)
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_FACTORY_STATE(FACTORY_STATE_IDLE)
	AND thisFactory.introCutscene.bPlaying
		IF NOT IS_SCREEN_FADED_OUT()
			IF NOT IS_SCREEN_FADING_OUT()
				DO_SCREEN_FADE_OUT(SIMPLE_INTERIOR_SCREEN_FADE_TIME)
			ENDIF
		ELSE
			INT iProductionStage = GlobalplayerBD_FM[NATIVE_TO_INT(thisFactory.pOwner)].propertyDetails.bdFactoryData[thisFactory.iSaveSlot].iProductionStage
			
			#IF IS_DEBUG_BUILD
				PRINTLN("AM_MP_BIKER_WAREHOUSE - MAINTAIN_STARTING_INTRO_CUTSCENE - Factory production stage now: ", iProductionStage, ", when we entered: ", thisFactory.iProductionStageWhenEntering)
			#ENDIF
			
			IF thisFactory.iProductionStageWhenEntering != iProductionStage
				INITIALISE_FACTORY_ENTITY_SETS(iProductionStage)
				REPIN_AND_REFRESH_SIMPLE_INTERIOR()
				#IF IS_DEBUG_BUILD
					PRINTLN("AM_MP_BIKER_WAREHOUSE - MAINTAIN_STARTING_INTRO_CUTSCENE - Production stage has changed in the meantime, activating new entity sets and refreshing the interior.")
				#ENDIF
				SET_BIT(thisFactory.iBS, BS_FACTORY_DATA_DID_INTERIOR_REFRESH_FOR_CUTSCENE)
				thisFactory.iProductionStageWhenEntering = iProductionStage
			ELSE
				IF NOT IS_BIT_SET(thisFactory.iBS, BS_FACTORY_DATA_DID_INTERIOR_REFRESH_FOR_CUTSCENE)
					SET_FACTORY_STATE(FACTORY_STATE_PLAYING_INTRO)
					#IF IS_DEBUG_BUILD
						PRINTLN("AM_MP_BIKER_WAREHOUSE - MAINTAIN_STARTING_INTRO_CUTSCENE - Production stage is the same, launching cutscene.")
					#ENDIF
					EXIT
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(thisFactory.iBS, BS_FACTORY_DATA_DID_INTERIOR_REFRESH_FOR_CUTSCENE)
				IF HAS_SIMPLE_INTERIOR_BEEN_REFRESHED()
					IF NOT IS_BIT_SET(thisFactory.iBS, BS_FACTORY_DATA_LOAD_SCENE_FOR_CUTSCENE_STARTED)
						VECTOR vFactoryCoords
						TEXT_LABEL_63 strInteriorType
						FLOAT fFactoryHeading, fFactoryRadius
						SIMPLE_INTERIOR_INSIDE_BOUNDING_BOX bbox
						GET_SIMPLE_INTERIOR_INSIDE_BOUNDING_BOX(thisFactory.eSimpleInteriorID, bbox)
				
						fFactoryRadius = GET_SIMPLE_INTERIOR_RADIUS_FROM_BOUNDING_BOX(bbox)
						GET_SIMPLE_INTERIOR_INTERIOR_TYPE_AND_POSITION(thisFactory.eSimpleInteriorID, strInteriorType, vFactoryCoords, fFactoryHeading)
				
						NEW_LOAD_SCENE_START_SPHERE(vFactoryCoords, fFactoryRadius)
						SET_BIT(thisFactory.iBS, BS_FACTORY_DATA_LOAD_SCENE_FOR_CUTSCENE_STARTED)
						
						#IF IS_DEBUG_BUILD
							PRINTLN("AM_MP_BIKER_WAREHOUSE - MAINTAIN_STARTING_INTRO_CUTSCENE - Starting load scene for newly activated entity sets.")
						#ENDIF
					ELSE
						IF IS_NEW_LOAD_SCENE_ACTIVE()
							IF IS_NEW_LOAD_SCENE_LOADED()
								NEW_LOAD_SCENE_STOP()
								SET_FACTORY_STATE(FACTORY_STATE_PLAYING_INTRO)
								CLEAR_BIT(thisFactory.iBS, BS_FACTORY_DATA_LOAD_SCENE_FOR_CUTSCENE_STARTED)
								CLEAR_BIT(thisFactory.iBS, BS_FACTORY_DATA_DID_INTERIOR_REFRESH_FOR_CUTSCENE)
								#IF IS_DEBUG_BUILD
									PRINTLN("AM_MP_BIKER_WAREHOUSE - MAINTAIN_STARTING_INTRO_CUTSCENE - OK load scene loaded!")
								#ENDIF
							ENDIF
						ELSE
							SET_FACTORY_STATE(FACTORY_STATE_PLAYING_INTRO)
							CLEAR_BIT(thisFactory.iBS, BS_FACTORY_DATA_LOAD_SCENE_FOR_CUTSCENE_STARTED)
							CLEAR_BIT(thisFactory.iBS, BS_FACTORY_DATA_DID_INTERIOR_REFRESH_FOR_CUTSCENE)
							#IF IS_DEBUG_BUILD
								PRINTLN("AM_MP_BIKER_WAREHOUSE - MAINTAIN_STARTING_INTRO_CUTSCENE - Something happened to our load scene, launching cutscene anyway.")
							#ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_DELIVERY_SOUND()
	IF IS_BIT_SET(thisFactory.iBS, BS_FACTORY_DATA_DELIVERY_SOUND_DONE)
		EXIT
	ENDIF
	
	IF g_FactoryDeliveryType != DELIVERY_TYPE_NONE
		thisFactory.strDeliveryStreamToPlay = GET_STREAM_FOR_DELIVERY_TYPE(g_FactoryDeliveryType)
		
		#IF IS_DEBUG_BUILD
			PRINTLN("AM_MP_BIKER_WAREHOUSE - MAINTAIN_DELIVERY_SOUND - Got delivery type: ", GET_FACTORY_DELIVERY_TYPE_DEBUG_NAME(g_FactoryDeliveryType), ", will load and play stream: ", thisFactory.strDeliveryStreamToPlay)
		#ENDIF
		
		g_FactoryDeliveryType = DELIVERY_TYPE_NONE
	ELSE
		IF IS_STRING_NULL_OR_EMPTY(thisFactory.strDeliveryStreamToPlay)
			SET_BIT(thisFactory.iBS, BS_FACTORY_DATA_DELIVERY_SOUND_DONE)
			#IF IS_DEBUG_BUILD
				PRINTLN("AM_MP_BIKER_WAREHOUSE - MAINTAIN_DELIVERY_SOUND - No delivery is made right now.")
			#ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_STRING_NULL_OR_EMPTY(thisFactory.strDeliveryStreamToPlay)
	AND NOT thisFactory.bPlayingDeliveryStream
		#IF IS_DEBUG_BUILD
			PRINTLN("AM_MP_BIKER_WAREHOUSE - MAINTAIN_DELIVERY_SOUND - Loading stream...")
		#ENDIF
		
		SET_SIMPLE_INTERIOR_CHILD_SCRIPT_IS_IN_CONTROL(TRUE)
		
		IF LOAD_STREAM(thisFactory.strDeliveryStreamToPlay, "DLC_Biker_Delivery_Sounds")
			#IF IS_DEBUG_BUILD
				PRINTLN("AM_MP_BIKER_WAREHOUSE - MAINTAIN_DELIVERY_SOUND - Stream loaded!")
			#ENDIF
			
			IF IS_LOCAL_PLAYER_WALKING_INTO_SIMPLE_INTERIOR()
			AND IS_SCREEN_FADED_OUT()
				#IF IS_DEBUG_BUILD
					PRINTLN("AM_MP_BIKER_WAREHOUSE - MAINTAIN_DELIVERY_SOUND - Taking control and playing the sound - PLAY_STREAM_FRONTEND")
				#ENDIF
				
				START_NET_TIMER(thisFactory.timerDeliverySound)
				PLAY_STREAM_FRONTEND()
				thisFactory.bPlayingDeliveryStream = TRUE
			ELSE
				#IF IS_DEBUG_BUILD
					PRINTLN("AM_MP_BIKER_WAREHOUSE - MAINTAIN_DELIVERY_SOUND - Player is not walkin in or screen is not faded out.")
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF thisFactory.bPlayingDeliveryStream
		#IF IS_DEBUG_BUILD
			PRINTLN("AM_MP_BIKER_WAREHOUSE - MAINTAIN_DELIVERY_SOUND - Playing stream..")
		#ENDIF
		
		IF HAS_NET_TIMER_EXPIRED(thisFactory.timerDeliverySound, 4000)
			IF NOT thisFactory.introCutscene.bPlaying
				SET_SIMPLE_INTERIOR_CHILD_SCRIPT_IS_IN_CONTROL(FALSE, FALSE)
			ENDIF
			
			thisFactory.bPlayingDeliveryStream = FALSE
			
			SET_BIT(thisFactory.iBS, BS_FACTORY_DATA_DELIVERY_SOUND_DONE)
			#IF IS_DEBUG_BUILD
				PRINTLN("AM_MP_BIKER_WAREHOUSE - MAINTAIN_DELIVERY_SOUND - OK DONE.")
			#ENDIF
		ENDIF
		
		/*
		IF NOT IS_STREAM_PLAYING()
			
		ENDIF
		*/
	ENDIF
ENDPROC

// CLEANUP PROC

/// PURPOSE:
///    
/// PARAMS:
///    bFullCLeanup - cleanup all the resources, might be used when script_cleanup proc happens before any of that is initialised
PROC SCRIPT_CLEANUP(BOOL bFullCleanup = TRUE)

	#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_BIKER_WAREHOUSE - SCRIPT_CLEANUP")
	#ENDIF
	
	IF bFullCleanup
		CLEANUP_CCTV(TRUE)
		CLEANUP_ALL_FACTORY_PRODUCTS(thisFactory.eID)
		CLEANUP_ALL_FACTORY_PALLETS(thisFactory.eID)
		CLEANUP_FACTORY_AUDIO_SOUNDS()
		CLEANUP_FACTORY_AUDIO()
		CLEANUP_FACTORY_ENTITY_SETS()
		CLEANUP_FACTORY_TIMECYCLE_MODIFIERS()
		CLEANUP_CLUBHOUSE_LAPTOP_SCREEN()
		CLEANUP_SIMPLE_INTERIOR_ENTRY_ANIM(thisFactory.garageExitAnim)
		CLEANUP_DEMO_PRODUCT()
	ENDIF
	
	IF IS_BIT_SET(thisFactory.iBS, BS_FACTORY_DATA_AUDIO_SCENE_STARTED)
	AND NOT IS_STRING_NULL_OR_EMPTY(thisFactory.strAudioScene)
		STOP_AUDIO_SCENE(thisFactory.strAudioScene)
		CLEAR_BIT(thisFactory.iBS, BS_FACTORY_DATA_AUDIO_SCENE_STARTED)
		#IF IS_DEBUG_BUILD
			PRINTLN("AM_MP_BIKER_WAREHOUSE - SCRIPT_CLEANUP - STOP_AUDIO_SCENE - Stopping audio scene ", thisFactory.strAudioScene)
		#ENDIF
	ENDIF
	
	GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].simpleCutscene.iBS = 0
	GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].simpleCutscene.iCutsceneHash = 0
	
	IF DOES_ENTITY_EXIST(thisFactory.oRadio)
		DELETE_OBJECT(thisFactory.oRadio)
	ENDIF
	
	IF DOES_BLIP_EXIST(thisFactory.bLaptopBlip)
		REMOVE_BLIP(thisFactory.bLaptopBlip)
	ENDIF
	
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════════╡ DEBUG STUFF  ╞═════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

#IF IS_DEBUG_BUILD
PROC CREATE_DEBUG_WIDGETS()

	INT iIter
	START_WIDGET_GROUP("AM_MP_BIKER_WAREHOUSE")
		START_WIDGET_GROUP("Object Placement")
			ADD_WIDGET_BOOL("Toggle visibility of post buy mission spawn points", debugData.bToggleBuySpawnPoints)
			ADD_WIDGET_BOOL("Toggle visibility of enter with gang spawn points", debugData.bToggleEnterWithGangSpawnPoints)
			REPEAT 2 iIter
			
				debugData.tlWidgetName[iIter] = "Spawn Object" 
				debugData.tlWidgetName[iIter] += iIter
				
				ADD_WIDGET_BOOL(debugData.tlWidgetName[iIter], debugData.bSpawnPlacementObject[iIter])
				ADD_WIDGET_VECTOR_SLIDER("Placement Coords", debugData.vPlacement[iIter], -10000.0, 10000.0, 0.01)
				ADD_WIDGET_VECTOR_SLIDER("Rotation Coords", debugData.vRotation[iIter], -10000.0, 10000.0, 0.01)
				
			ENDREPEAT
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Tutorial Product")
			/*
			ADD_WIDGET_INT_SLIDER("Product Amount", debugData.iTUTProduct, 0, GET_FACTORY_PRODUCT_CAPACITY(thisFactory.eID), 1)
			ADD_WIDGET_INT_SLIDER("Total Fade Time (MS)", debugData.iTUTProductTotalFadeTime, 0, 300000, 1)
			ADD_WIDGET_BOOL("Spawn Product", debugData.bTUTSpawnProduct)
			
			ADD_WIDGET_INT_READ_ONLY("Products Spawned", serverBD.iTUTProductSlot)
			
			ADD_WIDGET_INT_READ_ONLY("Fade Time per Product", thisFactory.tutPropData.iTUTIndividualProductFadeTime)
			*/
			
			ADD_WIDGET_BOOL("Hide All Product", debugData.bHideAllProduct)
			ADD_WIDGET_BOOL("Show All Product", debugData.bShowAllProduct)
			
			ADD_WIDGET_BOOL("bActivateDemoProduct", debugData.bActivateDemoProduct)
			ADD_WIDGET_INT_SLIDER("iDemoProductAmount", debugData.iDemoProductAmount, 0, GET_FACTORY_PRODUCT_CAPACITY(thisFactory.eID), 1)
			ADD_WIDGET_INT_SLIDER("iDemoProductTime", debugData.iDemoProductTime, 0, 99999, 1)
			ADD_WIDGET_BOOL("bDoDemoProduct", debugData.bDoDemoProduct)
			ADD_WIDGET_BOOL("bDeactivateDemoProduct", debugData.bDeactivateDemoProduct)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Tutorial product timing")
			//ADD_WIDGET_INT_SLIDER("CUTSCENE_DEMO_PRODUCT_FULL_FILL_TIME", CUTSCENE_DEMO_PRODUCT_FULL_FILL_TIME, 0, 99999, 1)
			ADD_WIDGET_INT_SLIDER("CUTSCENE_DEMO_PRODUCT_FULL_FILL_DELAY", CUTSCENE_DEMO_PRODUCT_FULL_FILL_DELAY, 0, 99999, 1)
			ADD_WIDGET_INT_SLIDER("CUTSCENE_DEMO_PRODUCT_FULL_FILL_SUSTAIN", CUTSCENE_DEMO_PRODUCT_FULL_FILL_SUSTAIN, 0, 99999, 1)
			
			ADD_WIDGET_INT_SLIDER("CUTSCENE_DEMO_PRODUCT_FULL_FILL_TIME_WEED", CUTSCENE_DEMO_PRODUCT_FULL_FILL_TIME_WEED, 0, 99999, 1)
			ADD_WIDGET_INT_SLIDER("CUTSCENE_DEMO_PRODUCT_FULL_FILL_TIME_METH", CUTSCENE_DEMO_PRODUCT_FULL_FILL_TIME_METH, 0, 99999, 1)
			ADD_WIDGET_INT_SLIDER("CUTSCENE_DEMO_PRODUCT_FULL_FILL_TIME_CRACK", CUTSCENE_DEMO_PRODUCT_FULL_FILL_TIME_CRACK, 0, 99999, 1)
			ADD_WIDGET_INT_SLIDER("CUTSCENE_DEMO_PRODUCT_FULL_FILL_TIME_FAKE_ID", CUTSCENE_DEMO_PRODUCT_FULL_FILL_TIME_FAKE_ID, 0, 99999, 1)
			ADD_WIDGET_INT_SLIDER("CUTSCENE_DEMO_PRODUCT_FULL_FILL_TIME_MONEY", CUTSCENE_DEMO_PRODUCT_FULL_FILL_TIME_MONEY, 0, 99999, 1)
			
			
		STOP_WIDGET_GROUP()
		
		SIMPLE_CUTSCENE_CREATE_DEBUG_WIDGETS(thisFactory.introCutscene)
	STOP_WIDGET_GROUP()
	
ENDPROC

PROC UPDATE_DEBUG_WIDGETS()
	
	INT iIter
	REPEAT 2 iIter
		IF debugData.bSpawnPlacementObject[iIter]
			IF NOT DOES_ENTITY_EXIST(debugData.oObj[iIter])
				debugData.vPlacement[iIter] = GET_FACTORY_PRODUCT_COORDS(thisFactory.pOwner, thisFactory.eID, 4)
				debugData.oObj[iIter] = CREATE_OBJECT(BKR_PROP_METH_TRAY_01A, debugData.vPlacement[iIter], FALSE, FALSE)
			ENDIF
			SET_ENTITY_ROTATION(debugData.oObj[iIter], debugData.vRotation[iIter])
			SET_ENTITY_COORDS(debugData.oObj[iIter], debugData.vPlacement[iIter], FALSE)
		ELSE
			//SAFE_DELETE_OBJECT(debugData.oObj[iIter])
		ENDIF
	ENDREPEAT
	
	IF debugData.bToggleBuySpawnPoints
		IF NOT (g_SpawnData.bUseCustomSpawnPoints)
			USE_CUSTOM_SPAWN_POINTS(TRUE, FALSE, TRUE, 0.01, 2, DEFAULT, DEFAULT, 0.01)
			
			INT i
			VECTOR vSpawnPoint
			FLOAT fSpawnHeading
			
			// Get simple interior details
			SIMPLE_INTERIOR_DETAILS details
			GET_SIMPLE_INTERIOR_DETAILS(thisFactory.eSimpleInteriorID, details)

			WHILE GET_FACTORY_AUTOWARP_OVERRIDDEN_SPAWN_POINT(thisFactory.eID, i, vSpawnPoint, fSpawnHeading)
				vSpawnPoint = TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS(vSpawnPoint, details)
				fSpawnHeading = TRANSFORM_SIMPLE_INTERIOR_HEADING_TO_WORLD_HEADING(fSpawnHeading, details)
				
				ADD_CUSTOM_SPAWN_POINT(vSpawnPoint, fSpawnHeading, 1.0 #IF IS_DEBUG_BUILD,0.2 #ENDIF)
				
				i = i + 1
			ENDWHILE
			g_SpawnData.bShowCustomSpawnPoints = TRUE
		ELSE
			g_SpawnData.bShowCustomSpawnPoints = FALSE
			CLEAR_CUSTOM_SPAWN_POINTS()
			USE_CUSTOM_SPAWN_POINTS(FALSE)
		ENDIF
		
		debugData.bToggleBuySpawnPoints = FALSE
	ENDIF
	
	IF debugData.bToggleEnterWithGangSpawnPoints
		IF NOT (g_SpawnData.bUseCustomSpawnPoints)
			USE_CUSTOM_SPAWN_POINTS(TRUE, FALSE, TRUE, 0.01, 2, DEFAULT, DEFAULT, 0.01)
			
			INT i
			VECTOR vSpawnPoint
			FLOAT fSpawnHeading
			
			// Get simple interior details
			SIMPLE_INTERIOR_DETAILS details
			GET_SIMPLE_INTERIOR_DETAILS(thisFactory.eSimpleInteriorID, details)

			WHILE _GET_FACTORY_ENTER_ALL_SPAWN_COORDS(thisFactory.eSimpleInteriorID, i, vSpawnPoint, fSpawnHeading)
				vSpawnPoint = TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS(vSpawnPoint, details)
				fSpawnHeading = TRANSFORM_SIMPLE_INTERIOR_HEADING_TO_WORLD_HEADING(fSpawnHeading, details)
				
				ADD_CUSTOM_SPAWN_POINT(vSpawnPoint, fSpawnHeading, 1.0 #IF IS_DEBUG_BUILD,0.2 #ENDIF)
				
				i = i + 1
			ENDWHILE
			g_SpawnData.bShowCustomSpawnPoints = TRUE
		ELSE
			g_SpawnData.bShowCustomSpawnPoints = FALSE
			CLEAR_CUSTOM_SPAWN_POINTS()
			USE_CUSTOM_SPAWN_POINTS(FALSE)
		ENDIF
		
		debugData.bToggleEnterWithGangSpawnPoints = FALSE
	ENDIF
	
	/*
	IF debugData.bTUTSpawnProduct
		PERFORM_FACTORY_TUTORIAL_PRODUCT_SPAWNING(debugData.iTUTProduct, debugData.iTUTProductTotalFadeTime)
		debugData.bTUTSpawnProduct = FALSE
	ENDIF
	*/
	
	IF debugData.bHideAllProduct
		HIDE_ALL_FACTORY_PRODUCT()
		debugData.bHideAllProduct = FALSE
	ENDIF
	
	IF debugData.bShowAllProduct
		SHOW_ALL_FACTORY_PRODUCT()
		debugData.bShowAllProduct = FALSE
	ENDIF
	
	IF debugData.bActivateDemoProduct
		CREATE_DEMO_PRODUCT()
		MAINTAIN_DEMO_PRODUCT()
	ENDIF
	
	IF debugData.bDeactivateDemoProduct
		CLEANUP_DEMO_PRODUCT()
		debugData.bDeactivateDemoProduct = FALSE
	ENDIF
	
	IF debugData.bDoDemoProduct
		SHOW_DEMO_PRODUCT(debugData.iDemoProductAmount, debugData.iDemoProductTime)
		debugData.bDoDemoProduct = FALSE
	ENDIF
	
	SIMPLE_CUTSCENE_MAINTAIN_DEBUG_WIDGETS(thisFactory.introCutscene)
	
ENDPROC
#ENDIF

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ INITIALISATION  ╞════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC CACHE_FACTORY_STATE()

	interiorStruct.cachedFactoryState.iProductionState = serverBD.iProductionStage 
	CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CACHE_FACTORY_STATE: interiorStruct.cachedFactoryState.iProductionState  = ", interiorStruct.cachedFactoryState.iProductionState  )
	interiorStruct.cachedFactoryState.iProductionTotal = thisFactory.iProductSlot
	CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CACHE_FACTORY_STATE: interiorStruct.cachedFactoryState.iProductionTotal = ", interiorStruct.cachedFactoryState.iProductionTotal )
	interiorStruct.cachedFactoryState.iMaterialsTotalForFactory = GlobalplayerBD_FM[NATIVE_TO_INT(thisFactory.pOwner)].propertyDetails.bdFactoryData[thisFactory.iSaveSlot].iMaterialsTotal
	CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CACHE_FACTORY_STATE: interiorStruct.cachedFactoryState.iMaterialsTotalForFactory = ", interiorStruct.cachedFactoryState.iMaterialsTotalForFactory)
	
ENDPROC

PROC INITIALISE()
	
	INITIALISE_FACTORY_PRODUCTION()
	
	CACHE_FACTORY_STATE()
	
	INITIALISE_FACTORY_GARAGE_EXIT()
	
	MAINTAIN_FACTORY_EXIT_TRIGGER(TRUE) // Init bitset that says if we're on a biker buy/sell mission (so we dont get kicked out if we decide to come back)
	
	SIMPLE_CUTSCENE_CREATE(thisFactory.introCutscene, "factory", thisFactory.eSimpleInteriorID)
	
	CREATE_FACTORY_RADIO()
	
	#IF IS_DEBUG_BUILD
		CREATE_DEBUG_WIDGETS()
		CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_BIKER_WAREHOUSE - INITIALISE - Done.")
	#ENDIF
	
	
ENDPROC



PROC SCRIPT_INITIALISE(SIMPLE_INTERIOR_CHILD_SCRIPT_DATA& scriptData)
	
	thisFactory.eSimpleInteriorID = scriptData.eSimpleInteriorID
	thisFactory.iScriptInstance = scriptData.iScriptInstance
	thisFactory.iInvitingPlayer = scriptData.iInvitingPlayer
	thisFactory.eID = GET_FACTORY_ID_FROM_SIMPLE_INTERIOR_ID(scriptData.eSimpleInteriorID)
	thisFactory.pOwner = GET_FACTORY_OWNER(thisFactory.eID #IF IS_DEBUG_BUILD , TRUE #ENDIF)
	thisFactory.eFactoryType = GET_GOODS_CATEGORY_FROM_FACTORY_ID(thisFactory.eID)
	thisFactory.bScriptWasRelaunched = scriptData.bWasRelaunched
	
	
	
	#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][FACTORY] AM_MP_BIKER_WAREHOUSE - SCRIPT_INITIALISE - Launching instance ", thisFactory.iScriptInstance, " was relaunched: ", scriptData.bWasRelaunched)
	#ENDIF
	
	IF thisFactory.pOwner = INVALID_PLAYER_INDEX()
		#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][FACTORY] AM_MP_BIKER_WAREHOUSE - SCRIPT_INITIALISE - Owner of this FACTORY is invalid, exiting...")
			IF NOT scriptData.bWasRelaunched
			AND NOT IS_PLAYER_SCTV(PLAYER_ID())
			AND NOT SHOULD_LOCAL_PLAYER_BE_KICKED_OUT_FROM_SIMPLE_INTERIOR(thisFactory.eSimpleInteriorID)
				ASSERTLN("[SIMPLE_INTERIOR][FACTORY] AM_MP_BIKER_WAREHOUSE - SCRIPT_INITIALISE - Child script is not relaunched and the owner is invalid! This shouldn't happen!")
			ENDIF
		#ENDIF
		SET_SIMPLE_INTERIOR_CHILD_SCRIPT_READY(TRUE)
		SET_SIMPLE_INTERIOR_CHILD_SCRIPT_ALLOWS_FOR_WARP(TRUE)
		SCRIPT_CLEANUP(FALSE)
	ENDIF
	
	thisFactory.iSaveSlot = GET_PLAYER_SAVE_SLOT_FOR_FACTORY(thisFactory.pOwner, thisFactory.eID)
	thisFactory.strAudioScene = GET_FACTORY_AUDIO_SCENE()
	thisFactory.iProductionStageWhenEntering =  GlobalplayerBD_FM[NATIVE_TO_INT(thisFactory.pOwner)].propertyDetails.bdFactoryData[thisFactory.iSaveSlot].iProductionStage
	
	NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE, thisFactory.iScriptInstance)
	
	// This makes sure the net script is active, waits untill it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
	
	// KGM: Wait for the first network broadcast before moving on
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		PRINTLN("AM_MP_BIKER_WAREHOUSE - FAILED TO RECEIVE INITIAL NETWORK BROADCAST - SCRIPT CLEANUP")
		SCRIPT_CLEANUP()
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		// This script will not be paused if another script calls PAUSE_GAME
		PRINTLN("AM_MP_BIKER_WAREHOUSE - INITIALISED")
	ELSE
		PRINTLN("AM_MP_BIKER_WAREHOUSE - NETWORK_IS_GAME_IN_PROGRESS = FALSE - SCRIPT CLEANUP")
		SCRIPT_CLEANUP()
	ENDIF
	
	INITIALISE()
	
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ MAIN LOGIC PROC ╞════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC RUN_MAIN_CLIENT_LOGIC()
	
	MAINTAIN_EXIT_VIA_GARAGE_DOOR()
	MAINTAIN_FACTORY_PRODUCTION()
	//MAINTAIN_FACTORY_TUTORIAL_PRODUCTION()
	//MAINTAIN_FACTORY_PRODUCTION_PROPS()
	MAINTAIN_FACTORY_HUD()
	MAINTAIN_INSIDE_DOOR()
	MAINTAIN_CLEANUP_FACTORY_TIMECYCLE_MODIFIERS()
	MAINTAIN_FACTORY_LAPTOP_SCREEN()
	MAINTAIN_FACTORY_AUDIO_BANKS()
	MAINTAIN_CHECKING_FOR_PLAYERS_NEARBY_THIS_FACTORY()
	MAINTAIN_FACTORY_AUDIO()
	CREATE_FACTORY_RADIO()
	MAINTAIN_STARTING_INTRO_CUTSCENE()
	MAINTAIN_DELIVERY_SOUND()
	
	IF NOT thisFactory.bPlayerLeftTheFactory
		IF IS_LOCAL_PLAYER_WALKING_OUT_OF_THIS_FACTORY(thisFactory.eID)
			thisFactory.bPlayerLeftTheFactory = TRUE
		ENDIF
	ENDIF
	
	IF IS_FACTORY_STATE(FACTORY_STATE_LOADING)

		// TODO: add all the checks for stuff being loaded & ready here.
		// Until SET_SIMPLE_INTERIOR_CHILD_SCRIPT_READY is called the game is left on the black screen.
		// As soon as it's called the parent script takes care of fading in and finishing the warp.
		
		IF CAN_FACTORY_HAVE_PALLET(thisFactory.eID)
		AND IS_FACTORY_PALLET_SLOT_AVALIABLE(GET_FACTORY_PALLET_TOTAL(thisFactory.eID)-1)
			CREATE_FACTORY_PALLETS(thisFactory.eID)
			EXIT
		ENDIF
		
		IF NOT IS_BIT_SET(thisFactory.iBS, BS_FACTORY_DATA_INTERIOR_REFRESHED_ON_INIT)
			IF NOT thisFactory.bScriptWasRelaunched
				// Get simple interior details
				VECTOR vInteriorPosition
				FLOAT fInteriorHeading
				TEXT_LABEL_63 txtInteriorType
				GET_SIMPLE_INTERIOR_INTERIOR_TYPE_AND_POSITION(thisFactory.eSimpleInteriorID, txtInteriorType, vInteriorPosition, fInteriorHeading)
				INTERIOR_INSTANCE_INDEX interirorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(vInteriorPosition, txtInteriorType)
				
				IF IS_VALID_INTERIOR(interirorIndex)
				AND IS_INTERIOR_READY(interirorIndex)
					
					#IF IS_DEBUG_BUILD
						PRINTLN("AM_MP_BIKER_WAREHOUSE - Interior ready! Refresing with activated entity sets.")
					#ENDIF
					
					//Make sure we're rendering to the interior
					FORCE_ROOM_FOR_ENTITY(PLAYER_PED_ID(), interirorIndex, GET_FACTORY_INTERIOR_ROOM_KEY(thisFactory.eFactoryType))
					//Refesh the interior with the new entity sets active
					REPIN_AND_REFRESH_SIMPLE_INTERIOR()
					
					SET_SIMPLE_INTERIOR_CHILD_SCRIPT_ALLOWS_FOR_WARP(TRUE)
					
					SET_BIT(thisFactory.iBS, BS_FACTORY_DATA_INTERIOR_REFRESHED_ON_INIT)
				ELSE
					CDEBUG3LN(DEBUG_PROPERTY, "AM_MP_BIKER_WAREHOUSE - waiting for the interior to be valid and ready")
				ENDIF
			ELSE
				SET_SIMPLE_INTERIOR_CHILD_SCRIPT_ALLOWS_FOR_WARP(TRUE)
				SET_BIT(thisFactory.iBS, BS_FACTORY_DATA_INTERIOR_REFRESHED_ON_INIT)
				#IF IS_DEBUG_BUILD
					PRINTLN("AM_MP_BIKER_WAREHOUSE - RUN_MAIN_CLIENT_LOGIC - Script was relaunched, skipping refresh.")
				#ENDIF
			ENDIF
		ENDIF
		
		IF CAN_SIMPLE_INTERIOR_CHILD_SCRIPT_TAKE_CONTROL()
		
			IF NOT IS_STRING_NULL_OR_EMPTY(thisFactory.strAudioScene)
			AND NOT IS_BIT_SET(thisFactory.iBS, BS_FACTORY_DATA_AUDIO_SCENE_STARTED)
				START_AUDIO_SCENE(thisFactory.strAudioScene)
				SET_BIT(thisFactory.iBS, BS_FACTORY_DATA_AUDIO_SCENE_STARTED)
				#IF IS_DEBUG_BUILD
					PRINTLN("AM_MP_BIKER_WAREHOUSE - RUN_MAIN_CLIENT_LOGIC - START_AUDIO_SCENE - Started audio scene ", thisFactory.strAudioScene)
				#ENDIF
			ENDIF
			
			BOOL bReady = TRUE
			
			IF NOT IS_BIT_SET(thisFactory.iBS, BS_FACTORY_DATA_LOAD_SCENE_STARTED)
				VECTOR vFactoryCoords
				TEXT_LABEL_63 strInteriorType
				FLOAT fFactoryHeading, fFactoryRadius
				SIMPLE_INTERIOR_INSIDE_BOUNDING_BOX bbox
				GET_SIMPLE_INTERIOR_INSIDE_BOUNDING_BOX(thisFactory.eSimpleInteriorID, bbox)
				
				fFactoryRadius = GET_SIMPLE_INTERIOR_RADIUS_FROM_BOUNDING_BOX(bbox)
				GET_SIMPLE_INTERIOR_INTERIOR_TYPE_AND_POSITION(thisFactory.eSimpleInteriorID, strInteriorType, vFactoryCoords, fFactoryHeading)
				
				NEW_LOAD_SCENE_START_SPHERE(vFactoryCoords, fFactoryRadius)
				SET_BIT(thisFactory.iBS, BS_FACTORY_DATA_LOAD_SCENE_STARTED)
				
				#IF IS_DEBUG_BUILD
					PRINTLN("AM_MP_BIKER_WAREHOUSE - RUN_MAIN_CLIENT_LOGIC - NEW_LOAD_SCENE_START_SPHERE - Started at pos ", vFactoryCoords, " with radius ", fFactoryRadius)
				#ENDIF
				
				bReady = FALSE
			ELSE
				IF IS_NEW_LOAD_SCENE_ACTIVE()
					IF NOT IS_NEW_LOAD_SCENE_LOADED()
						bReady = FALSE
						#IF IS_DEBUG_BUILD
							PRINTLN("AM_MP_BIKER_WAREHOUSE - RUN_MAIN_CLIENT_LOGIC - NEW_LOAD_SCENE_START_SPHERE - Waiting for load scene to finish.")
						#ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
							PRINTLN("AM_MP_BIKER_WAREHOUSE - RUN_MAIN_CLIENT_LOGIC - NEW_LOAD_SCENE_START_SPHERE - Load scene finished!")
						#ENDIF
						NEW_LOAD_SCENE_STOP()
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
						PRINTLN("AM_MP_BIKER_WAREHOUSE - RUN_MAIN_CLIENT_LOGIC - NEW_LOAD_SCENE_START_SPHERE - Load scene is not active.")
					#ENDIF
				ENDIF
			ENDIF
			
			IF thisFactory.bPlayingDeliveryStream
				bReady = FALSE
				#IF IS_DEBUG_BUILD
					PRINTLN("AM_MP_BIKER_WAREHOUSE - RUN_MAIN_CLIENT_LOGIC - bReady = FALSE because playing delivery sound.")
				#ENDIF
			ENDIF
			
			#IF IS_DEBUG_BUILD
			IF debugData.bSimulateTimeout
				IF NOT HAS_NET_TIMER_EXPIRED(debugData.timerSimulateTimeout, 10000)
					bReady = FALSE
					PRINTLN("AM_MP_BIKER_WAREHOUSE - RUN_MAIN_CLIENT_LOGIC - bReady = FALSE because simulating timeout.")
				ENDIF
			ENDIF
			#ENDIF
			
			IF bReady
				IF thisFactory.introCutscene.bPlaying
					SET_FACTORY_STATE(FACTORY_STATE_PLAYING_INTRO)
				ELSE
					// This is important, let internal script know that its child script is ready
					SET_SIMPLE_INTERIOR_CHILD_SCRIPT_READY(TRUE)
					// Move on to the next stage
					SET_FACTORY_STATE(FACTORY_STATE_IDLE)
				ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
				PRINTLN("AM_MP_BIKER_WAREHOUSE - RUN_MAIN_CLIENT_LOGIC - Waiting for CAN_SIMPLE_INTERIOR_CHILD_SCRIPT_TAKE_CONTROL: ", g_SimpleInteriorData.bSafeForChildScriptToTakeControl)
			#ENDIF
		ENDIF
		
	ELIF IS_FACTORY_STATE(FACTORY_STATE_IDLE)
		IF DOES_PLAYER_OWN_FACTORY_UPGRADE(thisFactory.pOwner, thisFactory.eID, UPGRADE_ID_SECURITY)
			BOOL bResetTimecycleModifier = FALSE
			BOOL bWeed = FALSE
			
			SWITCH thisFactory.eFactoryType
				CASE FACTORY_TYPE_WEED
					IF DOES_PLAYER_OWN_FACTORY_UPGRADE(thisFactory.pOwner, thisFactory.eID, UPGRADE_ID_EQUIPMENT)
						bResetTimecycleModifier = TRUE
						bWeed = TRUE
					ENDIF
				BREAK
				CASE FACTORY_TYPE_CRACK
					IF DOES_PLAYER_OWN_FACTORY_UPGRADE(thisFactory.pOwner, thisFactory.eID, UPGRADE_ID_EQUIPMENT)
						bResetTimecycleModifier = TRUE
						bWeed = FALSE
					ENDIF
				BREAK
			ENDSWITCH
			
			CLIENT_MAINTAIN_MP_CCTV(playerBD[NATIVE_TO_INT(PLAYER_ID())].MPCCTVClient, MPCCTVLocal, thisFactory.eSimpleInteriorID, bResetTimecycleModifier, bWeed)
		ENDIF
		
		IF g_SimpleInteriorData.bForceCCTVCleanup
			IF DOES_PLAYER_OWN_FACTORY_UPGRADE(thisFactory.pOwner, thisFactory.eID, UPGRADE_ID_SECURITY)
				CLEANUP_CCTV(FALSE)
			ENDIF
			
			g_SimpleInteriorData.bForceCCTVCleanup = FALSE
		ENDIF
		
		IF NOT IS_BIT_SET(thisFactory.iBS, BS_FACTORY_DATA_INIT_PRODUCT)
			INITIALISE_FACTORY_PRODUCT()
			SET_BIT(thisFactory.iBS, BS_FACTORY_DATA_INIT_PRODUCT)
		ENDIF
			
		MAINTAIN_FACTORY_UPGRADES()
		MAINTAIN_FACTORY_EXIT_TRIGGER()
		MAINTAIN_MOVING_PLAYER_VEHICLE_TO_SAFE_SPOT()
		
		IF DOES_BLIP_EXIST(thisFactory.bLaptopBlip)
			#IF FEATURE_GEN9_EXCLUSIVE
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("G9_BWH_TUT")
				SET_BLIP_FLASHES(thisFactory.bLaptopBlip, TRUE)
			ELSE
				SET_BLIP_FLASHES(thisFactory.bLaptopBlip, FALSE)
			ENDIF
			#ENDIF
		ELIF thisFactory.pOwner = PLAYER_ID()
			thisFactory.bLaptopBlip = CREATE_BLIP_FOR_COORD(GET_BLIP_COORDS_FOR_FACTORY_LAPTOP(thisFactory.eFactoryType))
			SET_BLIP_SPRITE(thisFactory.bLaptopBlip, RADAR_TRACE_LAPTOP)
			SET_BLIP_NAME_FROM_TEXT_FILE(thisFactory.bLaptopBlip, "OR_PC_BLIP")
			SET_BLIP_HIDDEN_ON_LEGEND(thisFactory.bLaptopBlip, TRUE)
			SET_BLIP_DISPLAY(thisFactory.bLaptopBlip, DISPLAY_RADAR_ONLY)
		ENDIF
		
		#IF FEATURE_GEN9_EXCLUSIVE
		IF NOT IS_BIT_SET(thisFactory.iBS, BS_FACTORY_DATA_DISPLAYED_MP_INTRO_HELP)
		AND IS_PLAYER_ON_MP_INTRO()
		AND IS_SCREEN_FADED_IN()
		AND g_iSimpleInteriorState = SIMPLE_INT_STATE_IDLE
		AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
		AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
			PRINT_HELP("G9_BWH_TUT")
			SET_BIT(thisFactory.iBS, BS_FACTORY_DATA_DISPLAYED_MP_INTRO_HELP)
		ENDIF
		#ENDIF
	
	ELIF IS_FACTORY_STATE(FACTORY_STATE_PLAYING_INTRO)
		
		IF SHOULD_LOCAL_PLAYER_BE_AUTOWARPED_INSIDE_THIS_SIMPLE_INTERIOR(thisFactory.eSimpleInteriorID)
			IF NOT IS_SIMPLE_INTERIOR_GLOBAL_BIT_SET(SI_BS_CanFinishAutoWarp, thisFactory.eSimpleInteriorID)
				// Wait till we're done autowarping before can do the cutscene
				EXIT
			ENDIF
		ENDIF
	
		IF NOT IS_LOCAL_PLAYER_WALKING_OUT_OF_SIMPLE_INTERIOR()
	
			SIMPLE_CUTSCENE_MAINTAIN(thisFactory.introCutscene)
			
			IF thisFactory.introCutscene.bPlaying
				// Animate product
				IF thisFactory.iProductAnimationStage = -1
					HIDE_ALL_FACTORY_PRODUCT()
					IF IS_BIT_SET(thisFactory.iBS, BS_FACTORY_DATA_PLAYING_SECOND_CUTSCENE)
						thisFactory.iProductAnimationStage = 0
						//thisFactory.iDemoProductCount = 0
						thisFactory.iDemoProductUpSoundID = GET_SOUND_ID()
						thisFactory.iDemoProductDownSoundID = GET_SOUND_ID()
					ENDIF
				ELSE
					CREATE_DEMO_PRODUCT()
					MAINTAIN_DEMO_PRODUCT()
				ENDIF
				
				// 17000, first help is 7000 second is 10000
				
				IF thisFactory.introCutscene.iCurrentScene = 1
				AND IS_BIT_SET(thisFactory.introCutscene.iBSScenesInit, 1)
				AND thisFactory.iProductAnimationStage > -1
					TEXT_LABEL_23 soundName
					SWITCH thisFactory.iProductAnimationStage
						CASE 0
							IF GET_GAME_TIMER() >= thisFactory.introCutscene.iCurrentSceneTimer + CUTSCENE_DEMO_PRODUCT_FULL_FILL_DELAY
								thisFactory.iProductAnimationStage += 1
								SHOW_DEMO_PRODUCT(GET_FACTORY_PRODUCT_CAPACITY(thisFactory.eID), GET_DEMO_PRODUCT_TIME())
								soundName = GET_DEMO_PRODUCT_SOUND("Up")
								PLAY_SOUND_FRONTEND(thisFactory.iDemoProductUpSoundID, soundName, "DLC_Biker_Warehouse_Intro_Inventory_Sounds")
								SET_VARIABLE_ON_SOUND(thisFactory.iDemoProductUpSoundID, "Time", TO_FLOAT(GET_DEMO_PRODUCT_TIME()) / 1000.0)
								//REINIT_NET_TIMER(thisFactory.timerCutsceneProductSounds)
							ENDIF
						BREAK
						CASE 1
							IF GET_GAME_TIMER() >= thisFactory.introCutscene.iCurrentSceneTimer + CUTSCENE_DEMO_PRODUCT_FULL_FILL_DELAY + CUTSCENE_DEMO_PRODUCT_FULL_FILL_SUSTAIN
								thisFactory.iProductAnimationStage += 1
								SHOW_DEMO_PRODUCT(0, GET_DEMO_PRODUCT_TIME())
								soundName = GET_DEMO_PRODUCT_SOUND("Down")
								PLAY_SOUND_FRONTEND(thisFactory.iDemoProductDownSoundID, soundName, "DLC_Biker_Warehouse_Intro_Inventory_Sounds")
								//PLAY_SOUND_FRONTEND(thisFactory.iDemoProductDownSoundID, "DLC_Biker_Warehouse_Intro_Product_Ticker_Down_Master")
								SET_VARIABLE_ON_SOUND(thisFactory.iDemoProductDownSoundID, "Time", TO_FLOAT(GET_DEMO_PRODUCT_TIME()) / 1000.0)
							ENDIF
						BREAK
						/*
						CASE 1
							// Play a tick sound every time one package appears
							IF HAS_NET_TIMER_EXPIRED(thisFactory.timerCutsceneProductSounds, FLOOR(TO_FLOAT(CUTSCENE_DEMO_PRODUCT_INIT_TIME) / TO_FLOAT(GET_DEMO_PRODUCT_COUNT())))
							AND thisFactory.iDemoProductCount < GET_DEMO_PRODUCT_COUNT()
								soundName = GET_DEMO_PRODUCT_SOUND("Single")
								PLAY_SOUND_FRONTEND(-1, soundName, "DLC_Biker_Warehouse_Intro_Inventory_Sounds")
								IF thisFactory.iDemoProductCount < GET_DEMO_PRODUCT_COUNT()
									REINIT_NET_TIMER(thisFactory.timerCutsceneProductSounds)
									thisFactory.iDemoProductCount += 1
								ENDIF
							ENDIF
							
							IF GET_GAME_TIMER() >= thisFactory.introCutscene.iCurrentSceneTimer + CUTSCENE_DEMO_PRODUCT_INIT_DELAY + CUTSCENE_DEMO_PRODUCT_INIT_TIME + CUTSCENE_DEMO_PRODUCT_FULL_FILL_DELAY
								thisFactory.iProductAnimationStage += 1
								PERFORM_FACTORY_TUTORIAL_PRODUCT_SPAWNING(GET_FACTORY_PRODUCT_CAPACITY(thisFactory.eID), CUTSCENE_DEMO_PRODUCT_FULL_FILL_TIME)
								soundName = GET_DEMO_PRODUCT_SOUND("Up")
								PLAY_SOUND_FRONTEND(thisFactory.iDemoProductUpSoundID, soundName, "DLC_Biker_Warehouse_Intro_Inventory_Sounds")
								SET_VARIABLE_ON_SOUND(thisFactory.iDemoProductUpSoundID, "Time", TO_FLOAT(CUTSCENE_DEMO_PRODUCT_FULL_FILL_TIME) / 1000.0)
							ENDIF
						BREAK
						*/
						
					ENDSWITCH
				ENDIF
			ELSE
				thisFactory.iProductAnimationStage = -1
				SHOW_ALL_FACTORY_PRODUCT()
				CLEANUP_DEMO_PRODUCT()
				
				IF NOT IS_SIMPLE_INTERIOR_CHILD_SCRIPT_IN_CONTROL() // There's nothing to handle the fade, we have to do it
					DO_SCREEN_FADE_IN(SIMPLE_INTERIOR_SCREEN_FADE_TIME)
					NETWORK_FADE_IN_ENTITY(PLAYER_PED_ID(), TRUE)
					FORCE_PLAYER_LOCALLY_VISIBLE_FOR_FADE()
					//thisFactory.bFadeScreenInAfterCutsceneIsDone = FALSE
				ELSE
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
						IF IS_ENTITY_VISIBLE(PLAYER_PED_ID())
							SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
							#IF IS_DEBUG_BUILD
								PRINTLN("AM_MP_BIKER_FACTORY - Player is visible after the cutscene, cutscene must have reset our visibility. Making us invisible again.")
							#ENDIF
						ENDIF
					ENDIF
				ENDIF
			
				IF NOT IS_SIMPLE_INTERIOR_CHILD_SCRIPT_READY()
					SET_SIMPLE_INTERIOR_CHILD_SCRIPT_READY(TRUE)
					SET_SIMPLE_INTERIOR_CHILD_SCRIPT_IS_IN_CONTROL(FALSE)
					g_SimpleInteriorData.bDontSimulateInputGaitOnEntrance = TRUE
				ENDIF
				
				// Cutscene finished, set that we've seen it
				IF thisFactory.pOwner = PLAYER_ID()
					IF IS_BIT_SET(thisFActory.iBS, BS_FACTORY_DATA_PLAYING_FIRST_CUTSCENE)
						SET_LOCAL_PLAYER_COMPLETED_FIRST_BIKER_BUSINESS_INTRO_CUTSCENE(thisFactory.eFactoryType, TRUE)
					ENDIF
					
					IF IS_BIT_SET(thisFActory.iBS, BS_FACTORY_DATA_PLAYING_SECOND_CUTSCENE)
						SET_LOCAL_PLAYER_COMPLETED_SECOND_BIKER_BUSINESS_INTRO_CUTSCENE(thisFactory.eFactoryType, TRUE)
						
						RELEASE_SOUND_ID(thisFactory.iDemoProductUpSoundID)
						RELEASE_SOUND_ID(thisFactory.iDemoProductDownSoundID)
					ENDIF
					
					//commented out for bug 3095664 as gb_illicit_goods_resupply HANDLE_REWARDS will normally be called straight after. Except the first time but another save should catch that. 
					//REQUEST_SAVE(STAT_SAVETYPE_PROLOGUE)
				ENDIF
				
				CLEAR_BIT(thisFActory.iBS, BS_FACTORY_DATA_PLAYING_FIRST_CUTSCENE)
				CLEAR_BIT(thisFActory.iBS, BS_FACTORY_DATA_PLAYING_SECOND_CUTSCENE)
				
				SET_FACTORY_STATE(FACTORY_STATE_IDLE)
			ENDIF
		ELSE
			IF IS_SCREEN_FADED_OUT()
				SIMPLE_CUTSCENE_STOP(thisFactory.introCutscene)
			ENDIF
		ENDIF
		
	ELIF IS_FACTORY_STATE(FACTORY_STATE_PLAYING_GARAGE_EXIT)
		IF IS_SIMPLE_INTERIOR_ENTRY_ANIM_STAGE(SIMPLE_INTERIOR_ANIM_STAGE_FINISHED, thisFactory.garageExitAnim)
			DO_SCREEN_FADE_OUT(SIMPLE_INTERIOR_SCREEN_FADE_TIME)
			SET_FACTORY_STATE(FACTORY_STATE_FINISHING_GARAGE_EXIT)
		ENDIF
		
	ELIF IS_FACTORY_STATE(FACTORY_STATE_FINISHING_GARAGE_EXIT)
		IF IS_SCREEN_FADED_OUT()
			IF NOT IS_BIT_SET(thisFactory.garageExitAnim.iBS, BS_SIMPLE_INTERIOR_ANIM_ON_ENTER_FADE_OUT_FINISH_SOUND_PLAYED)
				IF NOT IS_STRING_NULL_OR_EMPTY(thisFactory.garageExitAnim.strOnEnterFadeOutFinishSoundName)
				AND NOT IS_STRING_NULL_OR_EMPTY(thisFactory.garageExitAnim.strOnEnterFadeOutFinishSoundSet)
					PLAY_SOUND_FRONTEND(-1, thisFactory.garageExitAnim.strOnEnterFadeOutFinishSoundName, thisFactory.garageExitAnim.strOnEnterFadeOutFinishSoundSet)
				ENDIF
				SET_BIT(thisFactory.garageExitAnim.iBS, BS_SIMPLE_INTERIOR_ANIM_ON_ENTER_FADE_OUT_FINISH_SOUND_PLAYED)
			ENDIF
			STOP_SIMPLE_INTERIOR_ENTRY_ANIM(thisFactory.garageExitAnim)
		ENDIF
	ENDIF
	
ENDPROC

PROC RUN_MAIN_SERVER_LOGIC()

	//SERVER_MAINTAIN_MP_RADIO(serverBD.MPRadioServer, playerBD[MPRadioLocal.iServerPlayerStagger].MPRadioClient, MPRadioLocal #IF FEATURE_INDEPENDANT_RADIO, InteriorPropStruct #ENDIF)
	
	//SERVER_MAINTAIN_PROPERTY_ACTIVITIES(playerBD[serverLocalActControl.iSlowPlayerLoop].iActivityRequested,serverBD.activityControl,serverLocalActControl)
	
ENDPROC

PROC RUN_ACTIVITY_LOGIC()
	interiorStruct.pFactoryOwner 	= thisFactory.pOwner
	interiorStruct.eFactoryID	 	= thisFactory.eID
	interiorStruct.vInteriorPos 	= GET_FACTORY_COORDS(thisFactory.eID)
	interiorStruct.iInterior 		= GET_HASH_KEY(GET_FACTORY_INTERIOR_NAME(interiorStruct.eFactoryID))
	
	
	
	// Runs activity creator logic
	IF interiorStruct.iInterior != 0
		
		INIT_ACTIVITY_LAUNCH_SCRIPT_CHECK_WITH_AI_PEDS(activityControllerStruct, activityCheck, serverBD.activityProps, serverBD.activityPeds, serverBD.pedVariationStruct, pedLocalVariationsStruct, interiorStruct)
		
		MAINTAIN_APARTMENT_ACTIVITIES_CREATOR(activityControllerStruct, activityCheck, serverBD.activityProps, serverBD.activityPeds)
	ENDIF
	#IF IS_DEBUG_BUILD
	CHECK_TO_LAUNCH_ACTIVITY_CREATOR_UI_SCRIPT(interiorStruct)
	#ENDIF
ENDPROC




//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ MAIN LOOP ╞══════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛
SCRIPT  (SIMPLE_INTERIOR_CHILD_SCRIPT_DATA scriptData) 
	
	
	IF NETWORK_IS_GAME_IN_PROGRESS() 
		SCRIPT_INITIALISE(scriptData)	
	ELSE
		SCRIPT_CLEANUP()
	ENDIF
	
	// Main loop
	WHILE TRUE

		// One wait to rule them all. This can be the ONLY wait from here on in.....
		MP_LOOP_WAIT_ZERO()
				
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			PRINTLN("AM_MP_BIKER_WAREHOUSE - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE = TRUE")
			SET_SIMPLE_INTERIOR_CHILD_SCRIPT_READY(TRUE)
			SET_SIMPLE_INTERIOR_CHILD_SCRIPT_ALLOWS_FOR_WARP(TRUE)
			SCRIPT_CLEANUP()
		ENDIF
		
		IF SHOULD_THIS_SIMPLE_INTERIOR_CHILD_THREAD_TERMINATE(scriptData.iScriptInstance, thisFactory.eSimpleInteriorID)
			PRINTLN("AM_MP_BIKER_WAREHOUSE - SHOULD_THIS_SIMPLE_INTERIOR_CHILD_THREAD_TERMINATE = TRUE")
			SET_SIMPLE_INTERIOR_CHILD_SCRIPT_READY(TRUE)
			SET_SIMPLE_INTERIOR_CHILD_SCRIPT_ALLOWS_FOR_WARP(TRUE)
			SCRIPT_CLEANUP()
		ENDIF
		
		RUN_ACTIVITY_LOGIC()
		RUN_MAIN_CLIENT_LOGIC()
		
		// -----------------------------------
		// Process server game logic		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			RUN_MAIN_SERVER_LOGIC()
		ENDIF
		
		#IF IS_DEBUG_BUILD
			UPDATE_DEBUG_WIDGETS()
		#ENDIF
		
	ENDWHILE
	

ENDSCRIPT
