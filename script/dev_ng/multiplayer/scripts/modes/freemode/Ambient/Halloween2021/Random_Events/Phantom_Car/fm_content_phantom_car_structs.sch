
USING "globals.sch"
USING "rage_builtins.sch"
USING "fm_content_includes.sch"

//----------------------
//	CONSTANTS
//----------------------

CONST_INT MAX_NUM_SOUND_TRIGGERS	5	
CONST_INT MAX_NUM_MUSIC_TRIGGERS	2

//----------------------
//	SERVER VARIABLES
//----------------------

STRUCT MISSION_SPECIFIC_SERVER_DATA
	
	PLAYER_INDEX targetPlayer
	SCRIPT_TIMER stTargetDead
	SCRIPT_TIMER stFollowTimer
	
ENDSTRUCT

//----------------------
//	LOCAL VARIABLES
//----------------------


//----------------------
//	BITSET WRAPPERS
//----------------------

