//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        AM_MP_ISLAND.sc																				//
// Description: Script for managing the Heist Island														//
// Written by:  Online Technical Team																		//																			//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_path.sch"
USING "commands_event.sch"
USING "net_include.sch"
USING "net_events.sch"
USING "net_spawn.sch"
USING "net_ambience.sch"
USING "net_wait_zero.sch"
USING "casino_bar_activity.sch"

#IF FEATURE_HEIST_ISLAND
USING "net_peds_script_management.sch"
USING "heist_island_dancing.sch"
USING "heist_island_dance_cinematic_cam.sch"
USING "net_club_music.sch"
USING "heist_island_travel.sch"

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ VARIABLES ╞═════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

//m_IslandData.iBS
CONST_INT BS_ISLAND_DANCE_FLOOR_WALK_SLOW	0
CONST_INT BS_ISLAND_DANCE_FLOOR_WALK_FAST	1
CONST_INT BS_ISLAND_DANCE_FLOOR_HALF_LOCO	2
CONST_INT BS_ISLAND_SEATING_LAUNCHED		3

ENUM ISLAND_SERVER_STATE
	ISLAND_SERVER_STATE_LOADING,
	ISLAND_SERVER_STATE_IDLE
ENDENUM

ENUM ISLAND_CLIENT_STATE
	ISLAND_STATE_LOADING,
	ISLAND_STATE_IDLE
ENDENUM

ENUM CASINO_ISLAND_DJ_SWITCH_OVER_STATE
	CASINO_ISLAND_DJ_SWITCH_OVER_STATE_IDLE = 0,
	CASINO_ISLAND_DJ_SWITCH_OVER_STATE_FADE_OUT,
	CASINO_ISLAND_DJ_SWITCH_OVER_STATE_WARP_PLAYER,
	CASINO_ISLAND_DJ_SWITCH_OVER_STATE_FADE_IN
ENDENUM

STRUCT ServerBroadcastData
	DRINKING_ACTIVITY_SERVER_DATA sDrinkingActivityData
	DJ_SERVER_DATA DJServerData
	INT iTod = -1
	ISLAND_SERVER_STATE eState = ISLAND_SERVER_STATE_LOADING
ENDSTRUCT
ServerBroadcastData m_ServerBD

STRUCT PlayerBroadcastData
	INT iBS
ENDSTRUCT
PlayerBroadcastData m_PlayerBD[NUM_NETWORK_PLAYERS]

STRUCT ISLAND_DATA
	INT iBS
	
	DRINKING_ACTIVITY_DATA sDrinkingActivityData
	PLAYER_DANCE_LOCAL_STRUCT sDanceActivityData
	DANCE_CINEMATIC_CAM sDanceCam
	DJ_LOCAL_DATA DJLocalData
	HEIST_ISLAND_TRAVEL_BEACH_PARTY sBeachPartyTravel
	
	ISLAND_CLIENT_STATE eState = ISLAND_STATE_LOADING
	CASINO_ISLAND_DJ_SWITCH_OVER_STATE eDJSwitchOverState
		
	BOOL bPedScriptLaunched = FALSE
	
//	INT iCalmQuadID = -1
	BOOL bPartyDayStarted
	INT iDayLengthInMS
	INT iLocalBS
	
	SCRIPT_TIMER stDJSwitchWarpDelay
	
	#IF IS_DEBUG_BUILD
	BOOL bHideAlcoholProps = FALSE
	#ENDIF
ENDSTRUCT
ISLAND_DATA m_IslandData

DRINKING_ACTIVITY_PLAYER_DATA sDrinkingActivityPlayerBD[NUM_NETWORK_PLAYERS]

STRUCT TIME_FOR_LERP
	INT iHour
	INT iMinute
	INT iSecond
	TIME_DATATYPE stTime
	INT iBitSet
ENDSTRUCT

TIME_FOR_LERP sLerpTime

CONST_INT ciTOD_PARTY 16
CONST_INT ciMS_PER_GAME_SECOND 20
CONST_INT ciTIME_CYCLE_TRANSITION_TIME 144 


CONST_INT ciTODBS_DONE_SETUP 			0
CONST_INT ciTODBS_MAINTAIN_TOD_PARTY	1
CONST_INT ciTODBS_DONE_TOD_LERP			2
CONST_INT ciTODBS_MAINTAIN_TOD_SCOPING	3
CONST_INT ciTODBS_SET_TC_MODIFIER		4
CONST_INT ciTODBS_SETUP_TOD_LERP		5

INT iBeatIndex = -1
INT iUpcomingBeatTime

SCRIPT_TIMER stBeatTimer

FLOAT fSpeakerDistance = 5.0

INT iBeatOffset = 0

INT iLowFreq = 100
INT iHighFreq = 100
INT iBeatDuration = 65
INT iLowFreqMedium = 160
INT iHighFreqMedium = 160
INT iBeatDurationMedium = 65
INT iLowFreqIntense = 180
INT iHighFreqIntense = 180
INT iBeatDurationIntense = 65

INT iLowFreqDancing = 100
INT iHighFreqDancing = 100
INT iBeatDurationDancing = 65
INT iLowFreqDancingMedium = 160
INT iHighFreqDancingMedium = 160
INT iBeatDurationDancingMedium = 65
INT iLowFreqDancingIntense = 180
INT iHighFreqDancingIntense = 180
INT iBeatDurationDancingIntense = 65

INT iLowFreqUpstairs = 80
INT iHighFreqUpstairs = 80
INT iBeatDurationUpstairs = 65
INT iLowFreqUpstairsMedium = 100
INT iHighFreqUpstairsMedium = 100
INT iBeatDurationUpstairsMedium = 65
INT iLowFreqUpstairsIntense = 120
INT iHighFreqUpstairsIntense = 120
INT iBeatDurationUpstairsIntense = 65

INT iLowFreqUpstairsDancing = 80
INT iHighFreqUpstairsDancing = 80
INT iBeatDurationUpstairsDancing = 65
INT iLowFreqUpstairsDancingMedium = 100
INT iHighFreqUpstairsDancingMedium = 100
INT iBeatDurationUpstairsDancingMedium = 65
INT iLowFreqUpstairsDancingIntense = 120
INT iHighFreqUpstairsDancingIntense = 120
INT iBeatDurationUpstairsDancingIntense = 65

INT iLowFreqOutside = 50
INT iHighFreqOutside = 50
INT iBeatDurationOutside = 33
INT iLowFreqOutsideMedium = 50
INT iHighFreqOutsideMedium = 50
INT iBeatDurationOutsideMedium = 33
INT iLowFreqOutsideIntense = 50
INT iHighFreqOutsideIntense = 50
INT iBeatDurationOutsideIntense = 33

INT iLowFreqSpeakers = 180
INT iHighFreqSpeakers = 180
INT iBeatDurationSpeakers = 133
INT iLowFreqSpeakersMedium = 200
INT iHighFreqSpeakersMedium = 200
INT iBeatDurationSpeakersMedium = 180
INT iLowFreqSpeakersIntense = 240
INT iHighFreqSpeakersIntense = 240
INT iBeatDurationSpeakersIntense = 180

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════════╡ DEBUG ╞══════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

#IF IS_DEBUG_BUILD
//BOOL db_bDisableWaterCalming
BOOL bForceWeatherUpdate

STRUCT ISLAND_DEBUG_DATA
	DJ_DEBUG_DATA DJDebugData
ENDSTRUCT
ISLAND_DEBUG_DATA debugData

FUNC STRING DEBUG_GET_ISLAND_STATE_NAME(ISLAND_CLIENT_STATE eState)
	SWITCH eState
		CASE ISLAND_STATE_LOADING	RETURN "ISLAND_STATE_LOADING"
		CASE ISLAND_STATE_IDLE		RETURN "ISLAND_STATE_IDLE"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC STRING DEBUG_GET_ISLAND_SERVER_STATE_NAME(ISLAND_SERVER_STATE eState)
	SWITCH eState
		CASE ISLAND_SERVER_STATE_LOADING	RETURN "ISLAND_SERVER_STATE_LOADING"
		CASE ISLAND_SERVER_STATE_IDLE		RETURN "ISLAND_SERVER_STATE_IDLE"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC STRING DEBUG_GET_ISLAND_LOCAL_BS_NAME(INT iBit)
	SWITCH iBit
		CASE 0	RETURN "0"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC STRING DEBUG_GET_ISLAND_DJS_SWITCH_OVER_STATE_NAME(CASINO_ISLAND_DJ_SWITCH_OVER_STATE eState)
	SWITCH eState
		CASE CASINO_ISLAND_DJ_SWITCH_OVER_STATE_IDLE						RETURN "CASINO_ISLAND_DJ_SWITCH_OVER_STATE_IDLE"
		CASE CASINO_ISLAND_DJ_SWITCH_OVER_STATE_FADE_OUT					RETURN "CASINO_ISLAND_DJ_SWITCH_OVER_STATE_FADE_OUT"
		CASE CASINO_ISLAND_DJ_SWITCH_OVER_STATE_WARP_PLAYER				RETURN "CASINO_ISLAND_DJ_SWITCH_OVER_STATE_WARP_PLAYER"
		CASE CASINO_ISLAND_DJ_SWITCH_OVER_STATE_FADE_IN					RETURN "CASINO_ISLAND_DJ_SWITCH_OVER_STATE_FADE_IN"
	ENDSWITCH
	RETURN ""
ENDFUNC
#ENDIF // IS_DEBUG_BUILD

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════════╡ FUNCTIONS ╞════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

FUNC BOOL IS_ISLAND_STATE(ISLAND_CLIENT_STATE eState)
	RETURN m_IslandData.eState = eState
ENDFUNC

FUNC BOOL IS_ISLAND_SERVER_STATE(ISLAND_SERVER_STATE eState)
	RETURN m_ServerBD.eState = eState
ENDFUNC

PROC SET_ISLAND_STATE(ISLAND_CLIENT_STATE eState)
	PRINTLN("AM_MP_ISLAND - SET_ISLAND_STATE - New state: ", DEBUG_GET_ISLAND_STATE_NAME(eState))
	
	m_IslandData.eState = eState
ENDPROC

PROC SET_ISLAND_SERVER_STATE(ISLAND_SERVER_STATE eState)
	PRINTLN("AM_MP_ISLAND - SET_ISLAND_SERVER_STATE - New state: ", DEBUG_GET_ISLAND_SERVER_STATE_NAME(eState))
	
	m_ServerBD.eState = eState
ENDPROC

FUNC BOOL IS_ISLAND_LOCAL_BIT_SET(INT iBitToCheck)
	RETURN IS_BIT_SET(m_IslandData.iLocalBS, iBitToCheck)
ENDFUNC

PROC SET_ISLAND_DJ_SWITCH_OVER_STATE(CASINO_ISLAND_DJ_SWITCH_OVER_STATE eState)
	#IF IS_DEBUG_BUILD
	PRINTLN("AM_MP_ISLAND - SET_ISLAND_DJ_SWITCH_OVER_STATE - New state: ", DEBUG_GET_ISLAND_DJS_SWITCH_OVER_STATE_NAME(eState))
	#ENDIF
	m_IslandData.eDJSwitchOverState = eState
ENDPROC

PROC SET_ISLAND_LOCAL_BIT_SET(INT iBit, BOOL bSet)
	IF bSet
		IF NOT IS_ISLAND_LOCAL_BIT_SET(iBit)
			SET_BIT(m_IslandData.iLocalBS, iBit)
			PRINTLN("AM_MP_ISLAND - SET_ISLAND_LOCAL_BIT_SET: ", DEBUG_GET_ISLAND_LOCAL_BS_NAME(iBit), " TRUE")
		ENDIF
	ELSE
		IF IS_ISLAND_LOCAL_BIT_SET(iBit)
			CLEAR_BIT(m_IslandData.iLocalBS, iBit)
			PRINTLN("AM_MP_ISLAND - SET_ISLAND_LOCAL_BIT_SET: ", DEBUG_GET_ISLAND_LOCAL_BS_NAME(iBit), " FALSE")
		ENDIF
	ENDIF
ENDPROC

PROC INIT_TIME_WEATHER_FOR_ISLAND()
//	IF NETWORK_IS_ACTIVITY_SESSION()
////		NETWORK_OVERRIDE_CLOCK_TIME(12, 0, 0)
////		PRINTLN("INIT_TIME_WEATHER_FOR_ISLAND: fixed time for mission ")
//	ELSE
//		NETWORK_OVERRIDE_CLOCK_TIME(7, 0, 0)  ///7 to 1900
//		PRINTLN("INIT_TIME_WEATHER_FOR_ISLAND: set start time for island party ")									
//	ENDIF
//	SET_OVERRIDE_WEATHER("EXTRASUNNY")
	IF NOT NETWORK_IS_ACTIVITY_SESSION()
	AND NOT GB_IS_PLAYER_ON_ISLAND_HEIST_SCOPING_MISSION(PLAYER_ID())
		NETWORK_OVERRIDE_CLOCK_TIME(7, 0, 0)  ///7 to 1900
		PRINTLN("INIT_TIME_WEATHER_FOR_ISLAND: set start time for island party ")	
		SET_OVERRIDE_WEATHEREX("EXTRASUNNY",TRUE)
	ENDIF
	PRINTLN("INIT_TIME_WEATHER_FOR_ISLAND: called this frame")
ENDPROC

PROC START_PARTY_TIME_OF_DAY()
	NETWORK_OVERRIDE_CLOCK_TIME(7, 0, 0)  ///7 to 1900
	m_IslandData.iDayLengthInMS = 0
	INT i
	REPEAT GET_CLUB_RADIO_STATION_MAX_TRACKS(CLUB_LOCATION_ISLAND,CLUB_RADIO_STATION_KEINEMUSIK_BEACH_PARTY) i
		m_IslandData.iDayLengthInMS += GET_CLUB_RADIO_STATION_TRACK_DURATION_MS(CLUB_LOCATION_ISLAND,CLUB_RADIO_STATION_KEINEMUSIK_BEACH_PARTY,i+1)
	ENDREPEAT
	IF m_IslandData.iDayLengthInMS > 0
		PRINTLN("START_PARTY_TIME_OF_DAY: total DJ set time in MS: ",m_IslandData.iDayLengthInMS)
		//m_IslandData.iDayLengthInMS += 10*60*1000 //10 extra minutes for end transition back to game
		PRINTLN("START_PARTY_TIME_OF_DAY: total day time in MS: ",m_IslandData.iDayLengthInMS)
		PRINTLN("START_PARTY_TIME_OF_DAY: time per min in MS: ",m_IslandData.iDayLengthInMS/(60*12))
		INT iCurrentSetTime = GET_CLUB_MUSIC_CURRENT_SET_TIME(CLUB_LOCATION_ISLAND,m_ServerBD.DJServerData,m_IslandData.DJLocalData)
		IF iCurrentSetTime > 0
			FLOAT fTimeOffset = (TO_FLOAT(iCurrentSetTime)/TO_FLOAT(m_IslandData.iDayLengthInMs))*12
			FLOAT fCurrentGameHrs= 7+ fTimeOffset 
			INT iCurrentGameSecs = FLOOR(fCurrentGameHrs*60*60)
			INT iHrs = FLOOR(fCurrentGameHrs)
			INT iMins = FLOOR((TO_FLOAT(iCurrentGameSecs-iHrs*60*60))/60)
			INT iSecs = iCurrentGameSecs- (iMins*60) - (iHrs*60*60)
			PRINTLN("START_PARTY_TIME_OF_DAY: iCurrentSetTime = ", iCurrentSetTime)
			PRINTLN("START_PARTY_TIME_OF_DAY: m_IslandData.iDayLengthInMS = ", m_IslandData.iDayLengthInMS)
			PRINTLN("START_PARTY_TIME_OF_DAY: fTimeOffset = ", fTimeOffset) 
			PRINTLN("START_PARTY_TIME_OF_DAY: iCurrentGameSecs =  ", iCurrentGameSecs)
			PRINTLN("START_PARTY_TIME_OF_DAY: fCurrentGameHrs =  ", fCurrentGameHrs)
			PRINTLN("START_PARTY_TIME_OF_DAY: iHrs = ",iHrs)
			PRINTLN("START_PARTY_TIME_OF_DAY: iMins = ",iMins)
			PRINTLN("START_PARTY_TIME_OF_DAY: iSecs = ",iSecs)
			IF iHrs < 0
				iHrs = 7
			ENDIF
			IF iMins < 0
				iMins = 0
			ENDIF
			IF iSecs < 0
				iSecs = 0
			ENDIF
			NETWORK_OVERRIDE_CLOCK_TIME(iHrs, iMins, iSecs) 
			m_IslandData.bPartyDayStarted = TRUE
			g_clubMusicData.bStartIslandBeachPartyDay = FALSE
			NETWORK_OVERRIDE_CLOCK_RATE(m_IslandData.iDayLengthInMS/(60*12))
		ENDIF
	ELSE
		PRINTLN("START_PARTY_TIME_OF_DAY: NOT m_IslandData.iDayLengthInMS > 0")
	ENDIF
ENDPROC


PROC DO_TOD_LERP()
	
	//Start
	IF NOT IS_BIT_SET(sLerpTime.iBitSet, ciTODBS_SETUP_TOD_LERP)
		sLerpTime.iHour  = ciTOD_PARTY
		PRINTLN("[AM_MP_ISLAND] DO_TOD_LERP - START - sLerpTime.iHour = ", sLerpTime.iHour, " sLerpTime.iMinute = ", sLerpTime.iMinute, " sLerpTime.iSecond = ", sLerpTime.iSecond)
		sLerpTime.stTime = GET_NETWORK_TIME()
		//REset up the TC modifier
		CLEAR_BIT(sLerpTime.iBitSet, ciTODBS_SET_TC_MODIFIER)
		SET_BIT(sLerpTime.iBitSet, ciTODBS_SETUP_TOD_LERP)
	ENDIF
	
	//Incrament
	INT iDiffrence = ABSI(GET_TIME_DIFFERENCE(sLerpTime.stTime, GET_NETWORK_TIME()))
	IF  iDiffrence >= ciMS_PER_GAME_SECOND
		PRINTLN("[AM_MP_ISLAND] DO_TOD_LERP - sLerpTime.iSecond += iDiffrence % ciFRAME_MS = ",  sLerpTime.iSecond, " += ", iDiffrence / ciMS_PER_GAME_SECOND)
		iDiffrence = iDiffrence / ciMS_PER_GAME_SECOND
		IF iDiffrence < 1
			iDiffrence = 1
		ENDIF
		sLerpTime.iSecond += iDiffrence
		sLerpTime.stTime = GET_NETWORK_TIME()
	ENDIF
	
	//Move minute
	IF sLerpTime.iSecond >= 60
		sLerpTime.iSecond = 0
		sLerpTime.iMinute++
	ENDIF
	
	//Move hour
	IF sLerpTime.iMinute >= 60
		sLerpTime.iMinute = 0
		sLerpTime.iHour++
		PRINTLN("[AM_MP_ISLAND] DO_TOD_LERP - sLerpTime.iHour = ", sLerpTime.iHour, " sLerpTime.iMinute = ", sLerpTime.iMinute, " sLerpTime.iSecond = ", sLerpTime.iSecond)
	ENDIF
	
	//End
	IF sLerpTime.iHour >= GET_ISLAND_HEIST_SCOPING_EVENING_TOD()
		sLerpTime.iMinute = 0
		sLerpTime.iHour = 0
		PRINTLN("[AM_MP_ISLAND] DO_TOD_LERP - Done")
		SET_BIT(sLerpTime.iBitSet, ciTODBS_DONE_TOD_LERP)
	ENDIF 
	
	NETWORK_OVERRIDE_CLOCK_TIME(sLerpTime.iHour, sLerpTime.iMinute, sLerpTime.iSecond)
	
ENDPROC


FUNC INT GET_RANDOM_ISLAND_TOD()
	SWITCH GET_RANDOM_INT_IN_RANGE(0, 4)
		CASE 0	RETURN 0
		CASE 1	RETURN 7
		CASE 2	RETURN 19
		DEFAULT	RETURN 12
	ENDSWITCH
	RETURN 12	
ENDFUNC

FUNC BOOL NEED_TO_DO_TOD_LERP()
	IF NOT IS_BIT_SET(sLerpTime.iBitSet, ciTODBS_DONE_TOD_LERP)
	AND GB_GET_ISLAND_HEIST_PREP_PLAYER_IS_ON(PLAYER_ID()) = IHV_SCOPING_ISLAND
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_TIMECYCLE_MODIFIER()
	IF NOT IS_BIT_SET(sLerpTime.iBitSet, ciTODBS_SET_TC_MODIFIER)
		IF IS_BIT_SET(sLerpTime.iBitSet, ciTODBS_MAINTAIN_TOD_PARTY)
			IF GB_GET_ISLAND_HEIST_PREP_PLAYER_IS_ON(PLAYER_ID()) = IHV_SCOPING_ISLAND 
				CLEAR_TIMECYCLE_MODIFIER()
				PRINTLN("[AM_MP_ISLAND] MAINTAIN_TIMECYCLE_MODIFIER - START - CLEAR_TIMECYCLE_MODIFIER")		
			ELSE
				SET_TIMECYCLE_MODIFIER("h4_beachparty_day")
				PRINTLN("[AM_MP_ISLAND] MAINTAIN_TIMECYCLE_MODIFIER - START - SET_TIMECYCLE_MODIFIER(\"h4_beachparty_day\")")		
			ENDIF
		ENDIF
		SET_BIT(sLerpTime.iBitSet, ciTODBS_SET_TC_MODIFIER)
	ENDIF
ENDPROC

PROC MAINTAIN_TIME_FOR_ISLAND()

	IF NOT g_bDisableForceTimeOnIsland
	AND NOT NETWORK_IS_ACTIVITY_SESSION()
		
		//Do set up
		IF NOT IS_BIT_SET(sLerpTime.iBitSet, ciTODBS_DONE_SETUP)
			IF GB_IS_PLAYER_ON_ISLAND_HEIST_SCOPING_MISSION(PLAYER_ID())
				IF HAS_PLAYER_COMPLETED_ISLAND_HEIST_AS_LEADER(GB_GET_LOCAL_PLAYER_GANG_BOSS())
					SET_BIT(sLerpTime.iBitSet, ciTODBS_MAINTAIN_TOD_SCOPING)				
					PRINTLN("[AM_MP_ISLAND] MAINTAIN_TIME_FOR_ISLAND - SET_BIT(sLerpTime.iBitSet, ciTODBS_MAINTAIN_TOD_SCOPING)	")
				ELSE
					SET_BIT(sLerpTime.iBitSet, ciTODBS_MAINTAIN_TOD_PARTY)
					PRINTLN("[AM_MP_ISLAND] MAINTAIN_TIME_FOR_ISLAND - SET_BIT(sLerpTime.iBitSet, ciTODBS_MAINTAIN_TOD_PARTY)	")
				ENDIF
				SET_BIT(sLerpTime.iBitSet, ciTODBS_DONE_SETUP)
			ENDIF
		ENDIF
		
		//Maintain party
		IF IS_BIT_SET(sLerpTime.iBitSet, ciTODBS_MAINTAIN_TOD_PARTY)
			IF NEED_TO_DO_TOD_LERP()
				DO_TOD_LERP()
			ELIF IS_BIT_SET(sLerpTime.iBitSet, ciTODBS_DONE_TOD_LERP)
				NETWORK_OVERRIDE_CLOCK_TIME(GET_ISLAND_HEIST_SCOPING_EVENING_TOD(), 0, 0)
			ELSE
				NETWORK_OVERRIDE_CLOCK_TIME(ciTOD_PARTY, 0, 0)
			ENDIF
			MAINTAIN_TIMECYCLE_MODIFIER()
			
		//Maintain scoping
		ELIF IS_BIT_SET(sLerpTime.iBitSet, ciTODBS_MAINTAIN_TOD_SCOPING)	
			IF m_ServerBD.iTod != -1
				NETWORK_OVERRIDE_CLOCK_TIME(m_ServerBD.iTod, 0, 0)
			ENDIF
		
		//Maintain everything else
		ELSE
			IF NOT m_IslandData.bPartyDayStarted
			OR g_clubMusicData.bStartIslandBeachPartyDay
				NETWORK_OVERRIDE_CLOCK_TIME(7, 0, 0)
				IF NOT IS_LOCAL_PLAYER_IN_WARP_TRANSITION()
					START_PARTY_TIME_OF_DAY()
				ELSE
					PRINTLN("MAINTAIN_TIME_FOR_ISLAND:IS_LOCAL_PLAYER_IN_WARP_TRANSITION ")
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF
	
ENDPROC

PROC CLEANUP_TIME_WEATHER_FOR_ISLAND()
	IF m_IslandData.bPartyDayStarted
	OR IS_BIT_SET(sLerpTime.iBitSet, ciTODBS_MAINTAIN_TOD_PARTY)
	OR IS_BIT_SET(sLerpTime.iBitSet, ciTODBS_MAINTAIN_TOD_SCOPING)	
		NETWORK_CLEAR_CLOCK_TIME_OVERRIDE()
		CLEAR_OVERRIDE_WEATHER()
		PRINTLN("CLEANUP_TIME_WEATHER_FOR_ISLAND: overrides cleared")
	ENDIF
	PRINTLN("CLEANUP_TIME_WEATHER_FOR_ISLAND: called this frame")
ENDPROC

//PROC CLEANUP_WAVE_CALMING()
//	IF m_IslandData.iCalmQuadID != -1
//		REMOVE_EXTRA_CALMING_QUAD(m_IslandData.iCalmQuadID)
//		SET_CALMED_WAVE_HEIGHT_SCALER(1)
//		PRINTLN("AM_MP_ISLAND: CLEANUP_WAVE_CALMING REMOVE_EXTRA_CALMING_QUAD cleanup ID #",m_IslandData.iCalmQuadID)
//		m_IslandData.iCalmQuadID = -1
//	ENDIF
//ENDPROC

//PROC INIT_WAVE_CALMING()
//	//VECTOR vTemp = <<4892.7173, -4936.3262, 2.4291>>
//	//FLOAT fCalmingQuadSize = 200.0
//	IF m_IslandData.iCalmQuadID = -1
//		m_IslandData.iCalmQuadID = ADD_EXTRA_CALMING_QUAD(3700.0000,-6200.0000, 5750.0000, -4150.0000, 0.02)
//		SET_CALMED_WAVE_HEIGHT_SCALER(0.0)
//		PRINTLN("AM_MP_ISLAND: INIT_WAVE_CALMING ADD_EXTRA_CALMING_QUAD at xMin:", 3700.0000," xMax:", 5750.0000," yMin:", -6200.0000, " yMax:", -4150.0000, "ID #", m_IslandData.iCalmQuadID)
//	ENDIF
//ENDPROC

PROC CLEANUP_WALKING_STYLE()
	IF g_bOverrideWalkingStyle
		PRINTLN("AM_MP_ISLAND - MAINTAIN_WALKING_STYLE - Resetting walking style - 2")
		
		REMOVE_CLIP_SET(PICK_STRING((GET_PACKED_STAT_INT(PACKED_CHAR_PICTURE) = 0), "MOVE_M@DANCING", "MOVE_F@DANCING"))
		REMOVE_CLIP_SET(PICK_STRING((GET_PACKED_STAT_INT(PACKED_CHAR_PICTURE) = 0), "MOVE_M@DANCING@SLOW", "MOVE_F@DANCING@SLOW"))
		REMOVE_CLIP_SET(PICK_STRING((GET_PACKED_STAT_INT(PACKED_CHAR_PICTURE) = 0), "MOVE_M@DANCING@VERY_SLOW", "MOVE_F@DANCING@VERY_SLOW"))
		
		IF Get_Peds_Drunk_Alcohol_Hit_Count(PLAYER_PED_ID()) < 3
		AND Get_Peds_Drunk_Weed_Hit_Count(PLAYER_PED_ID()) < 3
			MPGlobalsAmbience.bUpdatePlayerWalk = TRUE
		ENDIF
		
		g_bOverrideWalkingStyle = FALSE
		
		CLEAR_BIT(m_IslandData.iBS, BS_ISLAND_DANCE_FLOOR_WALK_FAST)
		CLEAR_BIT(m_IslandData.iBS, BS_ISLAND_DANCE_FLOOR_WALK_SLOW)
		CLEAR_BIT(m_IslandData.iBS, BS_ISLAND_DANCE_FLOOR_HALF_LOCO)
	ENDIF
ENDPROC

PROC SCRIPT_CLEANUP()
	PRINTLN("AM_MP_ISLAND - SCRIPT_CLEANUP")
	
	SEND_SOCIAL_HUB_EXIT_TELEMETRY_DATA()	
	SET_AUDIO_FLAG("UseQuietSceneSoftVersion", FALSE)
	CLEANUP_DRINKING_ACTIVITY(m_serverBD.sDrinkingActivityData, sDrinkingActivityPlayerBD, m_IslandData.sDrinkingActivityData, FALSE, TRUE)
	DANCE_CLEANUP(m_IslandData.sDanceActivityData, NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()))
	DANCE_CINEMATIC_CAM__CLEANUP(m_IslandData.sDanceCam)
	CLEANUP_CLUB_MUSIC(CLUB_LOCATION_ISLAND, m_IslandData.DJLocalData, m_ServerBD.DJServerData)
	HEIST_ISLAND_TRAVEL_BEACH_PARTY__CLEANUP(m_IslandData.sBeachPartyTravel)
	CLEANUP_WALKING_STYLE()
	
	IF IS_BIT_SET(sLerpTime.iBitSet, ciTODBS_SET_TC_MODIFIER) 
		CLEAR_TIMECYCLE_MODIFIER()
		PRINTLN("AM_MP_ISLAND - SCRIPT_CLEANUP - Clearing time cycle modifier")
	ENDIF
	
	PRINTLN("AM_MP_ISLAND - SCRIPT_CLEANUP - g_bCleanupIslandScript = FALSE")
	
	g_clubMusicData.bWarpAfterDJSwitchOver = FALSE
	g_bCleanupIslandScript = FALSE
	Block_All_MissionsAtCoords_Missions(FALSE)
	CLEANUP_TIME_WEATHER_FOR_ISLAND()
//	CLEANUP_WAVE_CALMING()
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

FUNC NIGHTCLUB_AUDIO_TAGS GET_NIGHTCLUB_AUDIO_TAG_FROM_CLUB_MUSIC_INTENSITY(CLUB_MUSIC_INTENSITY eIntensity)
	SWITCH eIntensity
		CASE CLUB_MUSIC_INTENSITY_NULL 			RETURN AUDIO_TAG_NULL
		CASE CLUB_MUSIC_INTENSITY_LOW 			RETURN AUDIO_TAG_LOW
		CASE CLUB_MUSIC_INTENSITY_MEDIUM 		RETURN AUDIO_TAG_MEDIUM
		CASE CLUB_MUSIC_INTENSITY_HIGH			RETURN AUDIO_TAG_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS 	RETURN AUDIO_TAG_HIGH_HANDS
		CASE CLUB_MUSIC_INTENSITY_TRANCE	 	RETURN AUDIO_TAG_LOW
	ENDSWITCH
	
	ASSERTLN("[CASINO_NIGHTCLUB] GET_NIGHTCLUB_AUDIO_TAG_FROM_CLUB_MUSIC_INTENSITY - No conversion for: ", eIntensity, " make sure the lookup is up to date")
	RETURN AUDIO_TAG_NULL
ENDFUNC

#IF IS_DEBUG_BUILD
PROC CREATE_DEBUG_WIDGETS()
	START_WIDGET_GROUP("AM_MP_ISLAND")
		ADD_WIDGET_BOOL("Hide Alcohol Props", m_IslandData.bHideAlcoholProps)
		ADD_WIDGET_INT_SLIDER("Drunk Spawn Location", g_iBeachPartyDrunkSpawnLocation, -1, 16, 1)
		ADD_WIDGET_BOOL("bForceWeatherUpdate", bForceWeatherUpdate)
		DANCE_MAINTAIN_WIDGETS(m_IslandData.sDanceActivityData, GET_NIGHTCLUB_AUDIO_TAG_FROM_CLUB_MUSIC_INTENSITY(m_IslandData.DJLocalData.eMusicIntensity))
		DANCE_CINEMATIC_CAM__ADD_WIDGETS(m_IslandData.sDanceCam, "Beach Party")
		CREATE_CLUB_MUSIC_WIDGETS(debugData.DJDebugData)
		
		START_WIDGET_GROUP("Controller Vibration")
			ADD_WIDGET_INT_SLIDER("Beat Offset", iBeatOffset, -1000, 1000, 1)
			ADD_WIDGET_STRING("")
			ADD_WIDGET_STRING("Outside Large Area")
			ADD_WIDGET_INT_SLIDER("Low Freq", iLowFreqOutside, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("High Freq", iHighFreqOutside, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("Beat Duration", iBeatDurationOutside, 0, 1000, 1)
			ADD_WIDGET_INT_SLIDER("Low Freq Medium", iLowFreqOutsideMedium, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("High Freq Medium", iHighFreqOutsideMedium, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("Beat Duration Medium", iBeatDurationOutsideMedium, 0, 1000, 1)
			ADD_WIDGET_INT_SLIDER("Low Freq Intense", iLowFreqOutsideIntense, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("High Freq Intense", iHighFreqOutsideIntense, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("Beat Duration Intense", iBeatDurationOutsideIntense, 0, 1000, 1)
			ADD_WIDGET_STRING("")
			ADD_WIDGET_STRING("Off Dance Floor")
			ADD_WIDGET_INT_SLIDER("Low Freq", iLowFreqUpstairs, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("High Freq", iHighFreqUpstairs, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("Beat Duration", iBeatDurationUpstairs, 0, 1000, 1)
			ADD_WIDGET_INT_SLIDER("Low Freq Medium", iLowFreqUpstairsMedium, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("High Freq Medium", iHighFreqUpstairsMedium, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("Beat Duration Medium", iBeatDurationUpstairsMedium, 0, 1000, 1)
			ADD_WIDGET_INT_SLIDER("Low Freq Intense", iLowFreqUpstairsIntense, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("High Freq Intense", iHighFreqUpstairsIntense, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("Beat Duration Intense", iBeatDurationUpstairsIntense, 0, 1000, 1)
			ADD_WIDGET_STRING("")
			ADD_WIDGET_STRING("Off Dance Floor - Dancing")
			ADD_WIDGET_INT_SLIDER("Low Freq", iLowFreqUpstairsDancing, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("High Freq", iHighFreqUpstairsDancing, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("Beat Duration", iBeatDurationUpstairsDancing, 0, 1000, 1)
			ADD_WIDGET_INT_SLIDER("Low Freq Medium", iLowFreqUpstairsDancingMedium, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("High Freq Medium", iHighFreqUpstairsDancingMedium, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("Beat Duration Medium", iBeatDurationUpstairsDancingMedium, 0, 1000, 1)
			ADD_WIDGET_INT_SLIDER("Low Freq Intense", iLowFreqUpstairsDancingIntense, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("High Freq Intense", iHighFreqUpstairsDancingIntense, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("Beat Duration Intense", iBeatDurationUpstairsDancingIntense, 0, 1000, 1)
			ADD_WIDGET_STRING("")
			ADD_WIDGET_STRING("Dance Floor")
			ADD_WIDGET_INT_SLIDER("Low Freq", iLowFreq, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("High Freq", iHighFreq, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("Beat Duration", iBeatDuration, 0, 1000, 1)
			ADD_WIDGET_INT_SLIDER("Low Freq Medium", iLowFreqMedium, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("High Freq Medium", iHighFreqMedium, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("Beat Duration Medium", iBeatDurationMedium, 0, 1000, 1)
			ADD_WIDGET_INT_SLIDER("Low Freq Intense", iLowFreqIntense, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("High Freq Intense", iHighFreqIntense, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("Beat Duration Intense", iBeatDurationIntense, 0, 1000, 1)
			ADD_WIDGET_STRING("")
			ADD_WIDGET_STRING("Dance Floor - Dancing")
			ADD_WIDGET_INT_SLIDER("Low Freq", iLowFreqDancing, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("High Freq", iHighFreqDancing, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("Beat Duration", iBeatDurationDancing, 0, 1000, 1)
			ADD_WIDGET_INT_SLIDER("Low Freq Medium", iLowFreqDancingMedium, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("High Freq Medium", iHighFreqDancingMedium, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("Beat Duration Medium", iBeatDurationDancingMedium, 0, 1000, 1)
			ADD_WIDGET_INT_SLIDER("Low Freq Intense", iLowFreqDancingIntense, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("High Freq Intense", iHighFreqDancingIntense, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("Beat Duration Intense", iBeatDurationDancingIntense, 0, 1000, 1)
			ADD_WIDGET_STRING("")
			ADD_WIDGET_STRING("Speakers")
			ADD_WIDGET_INT_SLIDER("Low Freq", iLowFreqSpeakers, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("High Freq", iHighFreqSpeakers, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("Beat Duration", iBeatDurationSpeakers, 0, 1000, 1)
			ADD_WIDGET_INT_SLIDER("Low Freq Medium", iLowFreqSpeakersMedium, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("High Freq Medium", iHighFreqSpeakersMedium, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("Beat Duration Medium", iBeatDurationSpeakersMedium, 0, 1000, 1)
			ADD_WIDGET_INT_SLIDER("Low Freq Intense", iLowFreqSpeakersIntense, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("High Freq Intense", iHighFreqSpeakersIntense, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("Beat Duration Intense", iBeatDurationSpeakersIntense, 0, 1000, 1)
			ADD_WIDGET_FLOAT_SLIDER("Distance from Speakers", fSpeakerDistance, 0.0, 25.0, 0.1)
		STOP_WIDGET_GROUP()
		
		#IF SCRIPT_PROFILER_ACTIVE
		CREATE_SCRIPT_PROFILER_WIDGET() 
		#ENDIF
		
	STOP_WIDGET_GROUP()

ENDPROC

PROC UPDATE_DEBUG_WIDGETS()
//	IF db_bDisableWaterCalming
//		CLEANUP_WAVE_CALMING()
//	ELSE
//		INIT_WAVE_CALMING()
//	ENDIF
	IF bForceWeatherUpdate
		SET_OVERRIDE_WEATHEREX("EXTRASUNNY",TRUE)
		bForceWeatherUpdate = FALSE
	ENDIF
	UPDATE_CLUB_MUSIC_WIDGETS(CLUB_LOCATION_ISLAND, debugData.DJDebugData, m_ServerBD.DJServerData, m_IslandData.DJLocalData)
ENDPROC

BOOL bAlcoholPropsHidden = FALSE

PROC UPDATE_PROP_HIDING()
	IF m_IslandData.bHideAlcoholProps
		IF NOT bAlcoholPropsHidden
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_Drink_WhtWine, TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_Tumbler_01, TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_Bottle_cognac, TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_glass_stack_01, TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_Rum_Bottle, TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_Plonk_Red, TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_Champ_Cool, TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_BikerSet, TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_Vodka_Bottle, TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_Drink_RedWine, TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_Cocktail_Glass, TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_Shot_Glass, TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_Wine_White, TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_Wine_Glass, TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_BeerDusche, TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_Tall_Glass, TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_Drink_Whisky, TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_Pint_Glass_02, TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_Beer_Stz, TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_Beer_Bottle, TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_Beer_AMopen, TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_Pint_Glass_01, TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_ClubSet, TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_Bar_CoastDusc, TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_Bar_CoastBarr, TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_Bar_CoastMount, TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_Drink_Champ, TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_Whiskey_01, TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_Beer_Patriot, TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_Beer_StzOpen, TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_Beer_Pride, TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_Beer_Pissh, TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_Beer_LogOpen, TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, BEERROW_WORLD, TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, WINEROW, TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, SPIRITSROW, TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, INT_TO_ENUM(MODEL_NAMES, HASH("h4_int_club_bar_fridge_content")), TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, INT_TO_ENUM(MODEL_NAMES, HASH("h4_prop_battle_champ_closed")), TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, PROP_AMB_BEER_BOTTLE, TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, PROP_CS_BEER_BOT_01, TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, PROP_OPTIC_JD, TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, PROP_OPTIC_RUM, TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, PROP_OPTIC_VODKA, TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, PROP_CS_PAPER_CUP, TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, INT_TO_ENUM(MODEL_NAMES, HASH("h4_prop_battle_waterbottle_01a")), TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, INT_TO_ENUM(MODEL_NAMES, HASH("ba_prop_battle_champ_open_03")), TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, INT_TO_ENUM(MODEL_NAMES, HASH("h4_prop_h4_t_bottle_01a")), TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, INT_TO_ENUM(MODEL_NAMES, HASH("ba_prop_battle_decanter_03_s")), TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, INT_TO_ENUM(MODEL_NAMES, HASH("ba_prop_battle_decanter_02_s")), TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, INT_TO_ENUM(MODEL_NAMES, HASH("ex_p_ex_tumbler_02_empty")), TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, INT_TO_ENUM(MODEL_NAMES, HASH("ex_p_ex_tumbler_03_empty")), TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, PROP_BOTTLE_BRANDY, TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, PROP_TEQUILA_BOTTLE, TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, PROP_WHISKEY_BOTTLE, TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, P_TUMBLER_01_S, TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, P_CS_SHOT_GLASS_2_S, TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, INT_TO_ENUM(MODEL_NAMES, HASH("apa_prop_cs_plastic_cup_01")), TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, INT_TO_ENUM(MODEL_NAMES, HASH("h4_prop_club_water_bottle")), TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, INT_TO_ENUM(MODEL_NAMES, HASH("h4_p_cs_shot_glass_2_s")), TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, INT_TO_ENUM(MODEL_NAMES, HASH("h4_prop_battle_coconutdrink_01a")), TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, INT_TO_ENUM(MODEL_NAMES, HASH("h4_prop_h4_t_bottle_02b")), TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, INT_TO_ENUM(MODEL_NAMES, HASH("h4_prop_h4_can_beer_01a")), TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, PROP_SHOTS_GLASS_CS, TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, VODKAROW, TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, PROP_BAR_COCKSHAKER, TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, PROP_KEG_01, TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, PROP_CHERENKOV_03, TRUE)
			CREATE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, PROP_CAVA, TRUE)
			
			bAlcoholPropsHidden = TRUE
		ENDIF
	ELSE
		IF bAlcoholPropsHidden
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_Drink_WhtWine)
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_Tumbler_01)
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_Bottle_cognac)
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_glass_stack_01)
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_Rum_Bottle)
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_Plonk_Red)
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_Champ_Cool)
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_BikerSet)
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_Vodka_Bottle)
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_Drink_RedWine)
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_Cocktail_Glass)
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_Shot_Glass)
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_Wine_White)
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_Wine_Glass)
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_BeerDusche)
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_Tall_Glass)
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_Drink_Whisky)
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_Pint_Glass_02)
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_Beer_Stz)
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_Beer_Bottle)
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_Beer_AMopen)
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_Pint_Glass_01)
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_ClubSet)
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_Bar_CoastDusc)
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_Bar_CoastBarr)
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_Bar_CoastMount)
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_Drink_Champ)
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_Whiskey_01)
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_Beer_Patriot)
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_Beer_StzOpen)
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_Beer_Pride)
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_Beer_Pissh)
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, Prop_Beer_LogOpen)
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, BEERROW_WORLD)
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, WINEROW)
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, SPIRITSROW)
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, INT_TO_ENUM(MODEL_NAMES, HASH("h4_int_club_bar_fridge_content")))
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, INT_TO_ENUM(MODEL_NAMES, HASH("h4_prop_battle_champ_closed")))
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, PROP_AMB_BEER_BOTTLE)
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, PROP_CS_BEER_BOT_01)
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, PROP_OPTIC_JD)
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, PROP_OPTIC_RUM)
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, PROP_OPTIC_VODKA)
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, PROP_CS_PAPER_CUP)
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, INT_TO_ENUM(MODEL_NAMES, HASH("h4_prop_battle_waterbottle_01a")))
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, INT_TO_ENUM(MODEL_NAMES, HASH("ba_prop_battle_champ_open_03")))
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, INT_TO_ENUM(MODEL_NAMES, HASH("h4_prop_h4_t_bottle_01a")))
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, INT_TO_ENUM(MODEL_NAMES, HASH("ba_prop_battle_decanter_03_s")))
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, INT_TO_ENUM(MODEL_NAMES, HASH("ba_prop_battle_decanter_02_s")))
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, INT_TO_ENUM(MODEL_NAMES, HASH("ex_p_ex_tumbler_02_empty")))
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, INT_TO_ENUM(MODEL_NAMES, HASH("ex_p_ex_tumbler_03_empty")))
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, PROP_BOTTLE_BRANDY)
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, PROP_TEQUILA_BOTTLE)
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, PROP_WHISKEY_BOTTLE)
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, P_TUMBLER_01_S)
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, P_CS_SHOT_GLASS_2_S)
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, INT_TO_ENUM(MODEL_NAMES, HASH("apa_prop_cs_plastic_cup_01")))
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, INT_TO_ENUM(MODEL_NAMES, HASH("h4_prop_club_water_bottle")))
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, INT_TO_ENUM(MODEL_NAMES, HASH("h4_p_cs_shot_glass_2_s")))
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, INT_TO_ENUM(MODEL_NAMES, HASH("h4_prop_battle_coconutdrink_01a")))
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, INT_TO_ENUM(MODEL_NAMES, HASH("h4_prop_h4_t_bottle_02b")))
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, INT_TO_ENUM(MODEL_NAMES, HASH("h4_prop_h4_can_beer_01a")))
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, PROP_SHOTS_GLASS_CS)
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, VODKAROW)
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, PROP_BAR_COCKSHAKER)
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, PROP_KEG_01)
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, PROP_CHERENKOV_03)
			REMOVE_MODEL_HIDE(<<4890.2168, -4923.8716, 2.3683>>, 200.0, PROP_CAVA)
			
			bAlcoholPropsHidden = FALSE
		ENDIF
	ENDIF
ENDPROC
#ENDIF

PROC INITIALISE()
	PRINTLN("AM_MP_ISLAND - INITIALISE")
	INIT_TIME_WEATHER_FOR_ISLAND()
//	INIT_WAVE_CALMING()
	INITIALISE_DRINK_MENU_DATA(m_IslandData.sDrinkingActivityData)
	HEIST_ISLAND_DANCING_INITIALIZE(m_IslandData.sDanceActivityData)
	HEIST_ISLAND_DANCE_CINEMATIC_CAM__INIT_DANCE_CAM_CONFIGURATION(m_IslandData.sDanceCam)
	SET_AUDIO_FLAG("UseQuietSceneSoftVersion", TRUE)
	
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE)
	ENDIF

	#IF IS_DEBUG_BUILD
	CREATE_DEBUG_WIDGETS()
	#ENDIF
ENDPROC

/// PURPOSE:
///    Get island script instance. We need to add players to diffrent instance of script to get seprate host while active in mission.
///    Unique host is needed to manage island audio while in mission.
FUNC INT GET_ISLAND_SCRIPT_INSTANCE()
	IF GB_GET_ISLAND_HEIST_PREP_PLAYER_IS_ON(PLAYER_ID()) = IHV_SCOPING_INTRO
		IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
			PLAYER_INDEX bossIndex = GB_GET_LOCAL_PLAYER_GANG_BOSS()
			IF bossIndex != INVALID_PLAYER_INDEX()
				PRINTLN("AM_MP_ISLAND - SCRIPT_INITIALISE - instance: ", NATIVE_TO_INT(bossIndex))
				RETURN NATIVE_TO_INT(bossIndex)
			ELSE
				PRINTLN("AM_MP_ISLAND - SCRIPT_INITIALISE - we are in IHV_SCOPING_INTRO but not in gang boss is invalid!")
			ENDIF
		ELSE
			PRINTLN("AM_MP_ISLAND - SCRIPT_INITIALISE - we are in IHV_SCOPING_INTRO but not in gang WTF!")
		ENDIF
	ENDIF
	
	PRINTLN("AM_MP_ISLAND - SCRIPT_INITIALISE - instance: -1")
	RETURN -1
ENDFUNC

PROC SCRIPT_INITIALISE()
	PRINTLN("AM_MP_ISLAND - SCRIPT_INITIALISE - Launching")
	
	g_bCleanupIslandScript = FALSE
	
	NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE, GET_ISLAND_SCRIPT_INSTANCE())
	
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(m_ServerBD, SIZE_OF(m_ServerBD))
	
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(m_PlayerBD, SIZE_OF(m_PlayerBD))
	
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(sDrinkingActivityPlayerBD, SIZE_OF(sDrinkingActivityPlayerBD))
	
	DANCING_REGISTER_BROADCAST_DATA(m_IslandData.sDanceActivityData)
	
	RESERVE_LOCAL_NETWORK_MISSION_PEDS(MAX_NUM_BARTENDERS)
	
	IF NOT Wait_For_First_Network_Broadcast()
		PRINTLN("AM_MP_ISLAND - SCRIPT_INITIALISE - Failed to receive initial network broadcast")
		
		SCRIPT_CLEANUP()
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		PRINTLN("AM_MP_ISLAND - SCRIPT_INITIALISE - Initialised")
	ELSE
		PRINTLN("AM_MP_ISLAND - SCRIPT_INITIALISE - NETWORK_IS_GAME_IN_PROGRESS = FALSE")
		
		SCRIPT_CLEANUP()
	ENDIF
	
	INITIALISE()
ENDPROC

PROC LOAD_ISLAND_CLIENT()
	PRINTLN("AM_MP_ISLAND - LOAD_ISLAND_CLIENT - Loading")
	
	BOOL bReady = TRUE
	
	// Dancing mini-game
	IF NOT DANCE_LOAD_MINIGAME(m_IslandData.sDanceActivityData)
		PRINTLN("AM_MP_ISLAND - LOAD_ISLAND_CLIENT - Not ready yet, DANCE_LOAD_MINIGAME returning false")
		bReady = FALSE
	ENDIF
	
	// Commenting out as peds will spawn at bounds check when close to beach party.
//	IF NOT HAVE_ALL_PEDS_BEEN_CREATED()
//		PRINTLN("AM_MP_ISLAND - LOAD_ISLAND_CLIENT - Waiting for the peds to be created.")
//		bReady = FALSE
//	ENDIF
	
	IF bReady
		SET_ISLAND_STATE(ISLAND_STATE_IDLE)
		PRINTLN("AM_MP_ISLAND - LOAD_ISLAND_CLIENT - Ready.")
	ENDIF
ENDPROC

// Add any conditions here for blocking the island dancing
FUNC BOOL SHOULD_BLOCK_ISLAND_DANCING()
	IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
		RETURN TRUE
	ENDIF
	
	IF m_IslandData.sBeachPartyTravel.bPlayerGettingKickedFromArea
		#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_printDanceBusyReason")
			PRINTLN("[SHOULD_BLOCK_ISLAND_DANCING] bPlayerGettingKickedFromArea = TRUE")
		ENDIF
		#ENDIF
		
		RETURN TRUE
	ENDIF
	
	IF IS_SCREEN_FADING_OUT()
	OR IS_SCREEN_FADED_OUT()
	OR IS_PED_RUNNING(PLAYER_PED_ID())
		#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_printDanceBusyReason")
			PRINTLN("[SHOULD_BLOCK_ISLAND_DANCING] Screen fading out, or faded out")
		ENDIF
		#ENDIF
		
		RETURN TRUE
	ENDIF
	
	IF IS_PED_RUNNING(PLAYER_PED_ID())
	OR IS_ENTITY_IN_AIR(PLAYER_PED_ID())
	OR IS_PED_GETTING_UP(PLAYER_PED_ID())
	OR IS_PLAYER_CLIMBING(PLAYER_ID())
	OR IS_PED_RAGDOLL(PLAYER_PED_ID())
		#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_printDanceBusyReason")
			PRINTLN("[SHOULD_BLOCK_ISLAND_DANCING] Player moving around")
		ENDIF
		#ENDIF
		
		RETURN TRUE
	ENDIF
	
	IF IS_SCREEN_FADING_OUT()
	OR IS_SCREEN_FADED_OUT()
	OR IS_PED_RUNNING(PLAYER_PED_ID())
		#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_printDanceBusyReason")
			PRINTLN("[SHOULD_BLOCK_ISLAND_DANCING] Screen fading out, faded out, or ped running")
		ENDIF
		#ENDIF
		
		RETURN TRUE
	ENDIF
	
	IF m_IslandData.sDrinkingActivityData.eCurrentState > DRINKING_ACTIVITY_STATE_PROMPT
	OR m_IslandData.sDrinkingActivityData.iBarContextIntention != NEW_CONTEXT_INTENTION
		#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_printDanceBusyReason")
			PRINTLN("[IS_PLAYER_TOO_BUSY_TO_DANCE_IN_CASINO_NIGHTCLUB] Player is at bar")
		ENDIF
		#ENDIF
		
		RETURN TRUE
	ENDIF
	
	IF g_SpawnData.bPassedOutDrunk
	OR Get_Peds_Drunk_Alcohol_Hit_Count(PLAYER_PED_ID()) >= 10
		#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_printDanceBusyReason")
			PRINTLN("[SHOULD_BLOCK_ISLAND_DANCING] Player has passed out")
		ENDIF
		#ENDIF
		
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(g_simpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_INSTANT_PASS_OUT)
		#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_printDanceBusyReason")
			PRINTLN("[SHOULD_BLOCK_ISLAND_DANCING] Player drank Macbeth")
		ENDIF
		#ENDIF
		
		RETURN TRUE
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
		#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_printDanceBusyReason")
			PRINTLN("[SHOULD_BLOCK_ISLAND_DANCING] Player is in vehicle")
		ENDIF
		#ENDIF
		
		RETURN TRUE
	ENDIF
	
	IF g_bInNightclubSeatLocate
	OR g_bPlayerInActivityArea
		#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_printDanceBusyReason")
			PRINTLN("[SHOULD_BLOCK_ISLAND_DANCING] Player is at activity")
		ENDIF
		#ENDIF
		
		RETURN TRUE
	ENDIF
	
	IF IS_PI_MENU_OPEN()
	OR IS_CUSTOM_MENU_ON_SCREEN()
	OR SHOULD_LAUNCH_CRIMINAL_STARTER_PACK_BROWSER()
	OR IS_BROWSER_OPEN()
		#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_printDanceBusyReason")
			PRINTLN("[SHOULD_BLOCK_ISLAND_DANCING] UI active")
		ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF IS_PHONE_ONSCREEN()
	OR IS_CELLPHONE_CAMERA_IN_USE()
		#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_printDanceBusyReason")
			PRINTLN("[SHOULD_BLOCK_ISLAND_DANCING] Cellphone in use")
		ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF GET_LOCAL_PLAYER_USING_OFFICE_SEATID() != -1
		#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_printDanceBusyReason")
			PRINTLN("[SHOULD_BLOCK_ISLAND_DANCING] Player using seat")
		ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_ISLAND_CONTROLS()
	IF NOT NETWORK_IS_ACTIVITY_SESSION()
	AND (IS_LOCAL_PLAYER_AT_HEIST_ISLAND_BEACH_PARTY() // Weapons are only blocked if at the party
	AND IS_BEACH_PARTY_ACTIVE()
	OR IS_BIT_SET_ENUM(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iHeistIslandTravelBS, HITBS__IN_BEACH_PARTY_TO_SCOPING_TRANSITION))
	
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
	    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_UNARMED)
	    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_HANDGUN)
	    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SHOTGUN)
	    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SMG)
	    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_AUTO_RIFLE)
	    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SNIPER)
	    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_HEAVY)
	    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SPECIAL)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_MELEE)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DUCK)
	    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL)
	    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL_TWO)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DETONATE)
	    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
	    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK1)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK2)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_ALTERNATE)
	    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
		
		HOLSTER_WEAPON_IN_SIMPLE_INTERIOR()
		
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF NOT GB_IS_PLAYER_ON_ISLAND_HEIST_SCOPING_MISSION(PLAYER_ID())
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HYDRAULICS_CONTROL_TOGGLE)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT)
			ENDIF
			
		    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_GUN_LEFT)
		    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_GUN_RIGHT)
		    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_GUN_UP)
		    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_GUN_DOWN)
		    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
		    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_ATTACK)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_PREV_WEAPON)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_JUMP)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ROCKET_BOOST)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BIKE_WINGS)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MELEE_HOLD)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MELEE_LEFT)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MELEE_RIGHT)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PASSENGER_AIM)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PASSENGER_ATTACK)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ROOF)
		ELSE
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
		ENDIF
		
		SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisablePlayerAutoVaulting, TRUE)
	ENDIF
	
	// Always disable radio wheel on the island even when not at the party
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_NEXT_RADIO)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PREV_RADIO)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_NEXT_RADIO_TRACK)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PREV_RADIO_TRACK)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_RADIO_WHEEL)
	ENDIF
	
ENDPROC

FUNC BOOL MAINTAIN_AT_BEACH_PARTY()	
	// Brute force the flags when at the post finale beach party as they cannot leave the area like they can on missions and it should always be active
	IF NETWORK_IS_IN_TUTORIAL_SESSION()
		IF NETWORK_GET_PLAYER_TUTORIAL_SESSION_INSTANCE(PLAYER_ID()) = ciHEIST_ISLAND_BEACH_PARTY_TUTORIAL_INSTANCE_ID
			IF NOT IS_BEACH_PARTY_ACTIVE()
				SET_BEACH_PARTY_ACTIVE(TRUE)
				PRINTLN("AM_MP_ISLAND - MAINTAIN_AT_BEACH_PARTY - in post finale beach party but party wasn't active, activating party")
			ENDIF
			
			IF _HEIST_ISLAND_TRAVEL__IS_LOCAL_PLAYER_WITHIN_BEACH_PARTY_RADIUS(ciBEACH_PARTY_RESTRICTION_RADIUS + 10.0)		
				IF NOT IS_LOCAL_PLAYER_AT_HEIST_ISLAND_BEACH_PARTY()
					SET_LOCAL_PLAYER_AT_HEIST_ISLAND_BEACH_PARTY(TRUE)
					PRINTLN("AM_MP_ISLAND - MAINTAIN_AT_BEACH_PARTY - in post finale beach party instance but not classed as at party, setting at party")
				ENDIF
				
				IF NOT ARE_ALL_BEACH_PARTY_RESTRICTED_AREAS_ACTIVE_FOR_LOCAL_PLAYER()
					ENABLE_ALL_BEACH_PARTY_RESTRICTED_AREAS_FOR_LOCAL_PLAYER(TRUE)
					PRINTLN("AM_MP_ISLAND - MAINTAIN_AT_BEACH_PARTY - in post finale beach party but some restricted areas were not active, enabling all restriction areas")
				ENDIF
			ENDIF	
			
			RETURN TRUE			
		ENDIF
	ENDIF
	
	// If the party is complety disabled don't load on distance
	IF NOT IS_BEACH_PARTY_ACTIVE()
		
		// Don't class the player as at the beach party if its disabled
		IF IS_LOCAL_PLAYER_AT_HEIST_ISLAND_BEACH_PARTY()
			SET_LOCAL_PLAYER_AT_HEIST_ISLAND_BEACH_PARTY(FALSE)
		ENDIF
		
		RETURN FALSE
	ENDIF
	
	/// If in an activity session we let missions control the party loading
	/// as they may not want the party to load in on approach
	IF NETWORK_IS_ACTIVITY_SESSION()
		RETURN IS_LOCAL_PLAYER_AT_HEIST_ISLAND_BEACH_PARTY()
	ENDIF
	
	// If the party is active load based on distance to the party
	IF _HEIST_ISLAND_TRAVEL__IS_LOCAL_PLAYER_WITHIN_BEACH_PARTY_RADIUS(ciBEACH_PARTY_RESTRICTION_RADIUS + 10.0)
		IF NOT IS_LOCAL_PLAYER_AT_HEIST_ISLAND_BEACH_PARTY()
			SET_LOCAL_PLAYER_AT_HEIST_ISLAND_BEACH_PARTY(TRUE)
			PRINTLN("AM_MP_ISLAND - MAINTAIN_AT_BEACH_PARTY - left party radius")
		ENDIF
	ELSE
		IF IS_LOCAL_PLAYER_AT_HEIST_ISLAND_BEACH_PARTY()
			SET_LOCAL_PLAYER_AT_HEIST_ISLAND_BEACH_PARTY(FALSE)
			PRINTLN("AM_MP_ISLAND - MAINTAIN_AT_BEACH_PARTY - in party radius")
		ENDIF
	ENDIF
	
	RETURN IS_LOCAL_PLAYER_AT_HEIST_ISLAND_BEACH_PARTY()
ENDFUNC

FUNC BOOL SHOULD_BEACH_PARTY_RUN()
	RETURN IS_LOCAL_PLAYER_AT_HEIST_ISLAND_BEACH_PARTY()
ENDFUNC

FUNC BOOL SHOULD_RUN_BEACH_MUSIC()
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF m_ServerBD.DJServerData.eDJ = CLUB_DJ_NULL
		OR m_IslandData.DJLocalData.eDJstate = CLUB_DJ_INIT_DATAFILE_STATE
		OR m_IslandData.DJLocalData.eDJstate = CLUB_DJ_INIT_STATE
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN SHOULD_BEACH_PARTY_RUN()
ENDFUNC

PROC MAINTAIN_STOPPING_BEACH_PARTY()
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("am_mp_peds")) > 0
		TERMINATE_ALL_SCRIPTS_WITH_THIS_NAME("am_mp_peds")
		m_IslandData.bPedScriptLaunched = FALSE
		PRINTLN("AM_MP_ISLAND - MAINTAIN_STOPPING_BEACH_PARTY - Killing ped thread")
	ENDIF
	
	IF m_IslandData.sDanceActivityData.eStage != DLS_CLEANUP_COMPLETE
		DANCE_CLEANUP(m_IslandData.sDanceActivityData)
		PRINTLN("AM_MP_ISLAND - MAINTAIN_STOPPING_BEACH_PARTY - Cleaning up dancing")
	ENDIF
	
	IF NOT SHOULD_RUN_BEACH_MUSIC()
		IF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_MISSION_FINALE()
			IF m_IslandData.DJLocalData.eDJ != CLUB_DJ_NULL
				CLEANUP_CLUB_MUSIC(CLUB_LOCATION_ISLAND, m_IslandData.DJLocalData, m_ServerBD.DJServerData)
			ENDIF	
		ENDIF	
	ENDIF	
ENDPROC

PROC MAINTAIN_WALKING_STYLE()
	IF NOT g_bOverrideWalkingStyle
		CLEAR_BIT(m_IslandData.iBS, BS_ISLAND_DANCE_FLOOR_WALK_FAST)
		CLEAR_BIT(m_IslandData.iBS, BS_ISLAND_DANCE_FLOOR_WALK_SLOW)
		CLEAR_BIT(m_IslandData.iBS, BS_ISLAND_DANCE_FLOOR_HALF_LOCO)
	ENDIF
	
	IF Get_Peds_Drunk_Alcohol_Hit_Count(PLAYER_PED_ID()) < 3
	AND Get_Peds_Drunk_Weed_Hit_Count(PLAYER_PED_ID()) < 3
		IF (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<4894.003906, -4890.653809, 2.674105>>, <<4889.937988, -4949.889160, 9.369576>>, 37.75)
		OR (GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_FRONTEND_LT) > 0.0))
		AND NOT SHOULD_BLOCK_ISLAND_DANCING()
			SET_PED_CAN_PLAY_AMBIENT_IDLES(PLAYER_PED_ID(), TRUE)
			
			IF GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_FRONTEND_LT) > 0.0
				IF NOT IS_BIT_SET(m_IslandData.iBS, BS_ISLAND_DANCE_FLOOR_HALF_LOCO)
					REQUEST_CLIP_SET(PICK_STRING((GET_PACKED_STAT_INT(PACKED_CHAR_PICTURE) = 0), "MOVE_M@DANCING@VERY_SLOW", "MOVE_F@DANCING@VERY_SLOW"))
					
					IF HAS_CLIP_SET_LOADED(PICK_STRING((GET_PACKED_STAT_INT(PACKED_CHAR_PICTURE) = 0), "MOVE_M@DANCING@VERY_SLOW", "MOVE_F@DANCING@VERY_SLOW"))
						PRINTLN("AM_MP_ISLAND - MAINTAIN_WALKING_STYLE - Overriding walking style to very slow dancing")
						
						SET_PED_MOVEMENT_CLIPSET(PLAYER_PED_ID(), PICK_STRING((GET_PACKED_STAT_INT(PACKED_CHAR_PICTURE) = 0), "MOVE_M@DANCING@VERY_SLOW", "MOVE_F@DANCING@VERY_SLOW"), 0.5)
						
						g_bOverrideWalkingStyle = TRUE
						
						SET_BIT(m_IslandData.iBS, BS_ISLAND_DANCE_FLOOR_HALF_LOCO)
						CLEAR_BIT(m_IslandData.iBS, BS_ISLAND_DANCE_FLOOR_WALK_FAST)
						CLEAR_BIT(m_IslandData.iBS, BS_ISLAND_DANCE_FLOOR_WALK_SLOW)
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<4893.691406, -4899.992188, 2.378432>>, <<4894.234863, -4917.535645, 5.367703>>, 15.0)
					IF NOT IS_BIT_SET(m_IslandData.iBS, BS_ISLAND_DANCE_FLOOR_WALK_FAST)
						REQUEST_CLIP_SET(PICK_STRING((GET_PACKED_STAT_INT(PACKED_CHAR_PICTURE) = 0), "MOVE_M@DANCING", "MOVE_F@DANCING"))
						
						IF HAS_CLIP_SET_LOADED(PICK_STRING((GET_PACKED_STAT_INT(PACKED_CHAR_PICTURE) = 0), "MOVE_M@DANCING", "MOVE_F@DANCING"))
							PRINTLN("AM_MP_ISLAND - MAINTAIN_WALKING_STYLE - Overring walking style")
							
							SET_PED_MOVEMENT_CLIPSET(PLAYER_PED_ID(), PICK_STRING((GET_PACKED_STAT_INT(PACKED_CHAR_PICTURE) = 0), "MOVE_M@DANCING", "MOVE_F@DANCING"))
							
							g_bOverrideWalkingStyle = TRUE
							
							SET_BIT(m_IslandData.iBS, BS_ISLAND_DANCE_FLOOR_WALK_FAST)
							CLEAR_BIT(m_IslandData.iBS, BS_ISLAND_DANCE_FLOOR_WALK_SLOW)
							CLEAR_BIT(m_IslandData.iBS, BS_ISLAND_DANCE_FLOOR_HALF_LOCO)
						ENDIF
					ENDIF
				ELSE
					IF NOT IS_BIT_SET(m_IslandData.iBS, BS_ISLAND_DANCE_FLOOR_WALK_SLOW)
						REQUEST_CLIP_SET(PICK_STRING((GET_PACKED_STAT_INT(PACKED_CHAR_PICTURE) = 0), "MOVE_M@DANCING@SLOW", "MOVE_F@DANCING@SLOW"))
						
						IF HAS_CLIP_SET_LOADED(PICK_STRING((GET_PACKED_STAT_INT(PACKED_CHAR_PICTURE) = 0), "MOVE_M@DANCING@SLOW", "MOVE_F@DANCING@SLOW"))
							PRINTLN("AM_MP_ISLAND - MAINTAIN_WALKING_STYLE - Overring walking style")
							
							SET_PED_MOVEMENT_CLIPSET(PLAYER_PED_ID(), PICK_STRING((GET_PACKED_STAT_INT(PACKED_CHAR_PICTURE) = 0), "MOVE_M@DANCING@SLOW", "MOVE_F@DANCING@SLOW"))
							
							g_bOverrideWalkingStyle = TRUE
							
							SET_BIT(m_IslandData.iBS, BS_ISLAND_DANCE_FLOOR_WALK_SLOW)
							CLEAR_BIT(m_IslandData.iBS, BS_ISLAND_DANCE_FLOOR_WALK_FAST)
							CLEAR_BIT(m_IslandData.iBS, BS_ISLAND_DANCE_FLOOR_HALF_LOCO)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			CLEANUP_WALKING_STYLE()
		ENDIF
	ELSE
		CLEANUP_WALKING_STYLE()
	ENDIF
ENDPROC

PROC MAINTAIN_SEATING_ACTIVITY_LAUNCHING()
	IF NOT IS_BIT_SET(m_IslandData.iBS, BS_ISLAND_SEATING_LAUNCHED)
	AND IS_PLAYER_ON_HEIST_ISLAND(PLAYER_ID())
	AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY("beach_exterior_seating")) = 0
	AND NOT NETWORK_IS_SCRIPT_ACTIVE("beach_exterior_seating", DEFAULT, TRUE)
		REQUEST_SCRIPT("beach_exterior_seating")
		
		IF HAS_SCRIPT_LOADED("beach_exterior_seating")
			START_NEW_SCRIPT("beach_exterior_seating", MULTIPLAYER_MISSION_STACK_SIZE)
			
			SET_SCRIPT_AS_NO_LONGER_NEEDED("beach_exterior_seating")
			
			SET_BIT(m_IslandData.iBS, BS_ISLAND_SEATING_LAUNCHED)
		ENDIF
	ELIF IS_BIT_SET(m_IslandData.iBS, BS_ISLAND_SEATING_LAUNCHED)
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY("beach_exterior_seating")) = 0
		AND NOT NETWORK_IS_SCRIPT_ACTIVE("beach_exterior_seating", DEFAULT, TRUE)
			CLEAR_BIT(m_IslandData.iBS, BS_ISLAND_SEATING_LAUNCHED)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: 
///    Spawn points for Island DJ switch over
PROC SET_UP_CUSTOM_SPAWN_POINTS_FOR_DJ_SWITCH_WARP(BOOL bWeightByProximity = TRUE)
	IF NOT g_SpawnData.bUseCustomSpawnPoints
		
		VECTOR vSpawnPoint
		FLOAT fSpawnHeading, fWeight, fMaxDist
		IF NOT bWeightByProximity
			fWeight = 1.0
		ENDIF
		
		USE_CUSTOM_SPAWN_POINTS(TRUE, FALSE, TRUE, 0.01, 2, DEFAULT, DEFAULT, 0.01, DEFAULT, DEFAULT, TRUE, TRUE)
		INT i
		WHILE GET_ISLAND_BEACH_PARTY_SPAWN_POINT(i, vSpawnPoint, fSpawnHeading)

			IF bWeightByProximity
				IF VDIST2(vSpawnPoint, GET_PLAYER_COORDS(PLAYER_ID())) > fMaxDist
					fMaxDist = VDIST2(vSpawnPoint, GET_PLAYER_COORDS(PLAYER_ID()))
				ENDIF
			ELSE
				ADD_CUSTOM_SPAWN_POINT(vSpawnPoint, fSpawnHeading, fWeight)
				fWeight -= 0.025 
			ENDIF
			
			i += 1
		ENDWHILE
		
		IF bWeightByProximity
			i = 0
			WHILE GET_ISLAND_BEACH_PARTY_SPAWN_POINT(i, vSpawnPoint, fSpawnHeading)

				fWeight = 1.0 - (VDIST2(GET_PLAYER_COORDS(PLAYER_ID()), vSpawnPoint) / fMaxDist)
				ADD_CUSTOM_SPAWN_POINT(vSpawnPoint, fSpawnHeading, fWeight)
				
				i += 1
			ENDWHILE
		ENDIF
		
	ENDIF
ENDPROC

PROC PERFORM_ISLAND_DJ_SWITCH_OVER_WARP()
	
	SWITCH m_IslandData.eDJSwitchOverState
		CASE CASINO_ISLAND_DJ_SWITCH_OVER_STATE_IDLE
			IF g_clubMusicData.bWarpAfterDJSwitchOver
				// Need to cancel MacBeth Whiskey if we're renovating
				CLEAR_BIT(g_simpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_INSTANT_PASS_OUT)
				
				SET_ISLAND_DJ_SWITCH_OVER_STATE(CASINO_ISLAND_DJ_SWITCH_OVER_STATE_FADE_OUT)
			ENDIF
		BREAK
		CASE CASINO_ISLAND_DJ_SWITCH_OVER_STATE_FADE_OUT
			PRINTLN("AM_MP_ISLAND - PERFORM_ISLAND_DJ_SWITCH_OVER_WARP - Inside: CASINO_ISLAND_DJ_SWITCH_OVER_STATE_FADE_OUT")

			g_bKillTheBed = TRUE
			g_bKickPlayerOutOfShop = TRUE
			PRINTLN("AM_MP_ISLAND - PERFORM_ISLAND_DJ_SWITCH_OVER_WARP - CLEANUP 5")
			CLEANUP_DRINKING_ACTIVITY(m_serverBD.sDrinkingActivityData, sDrinkingActivityPlayerBD, m_IslandData.sDrinkingActivityData, DEFAULT, TRUE, TRUE)
			//CLEANUP_CHAMPAGNE_ACTIVITY(sChampagneActivityPlayerBD, m_InteriorData.sChampagneActivityData, TRUE)
			SET_ISLAND_DJ_SWITCH_OVER_STATE(CASINO_ISLAND_DJ_SWITCH_OVER_STATE_WARP_PLAYER)
			
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
			AND NOT IS_BIT_SET(g_SimpleInteriorData.iSeventhBS, BS7_SIMPLE_INTERIOR_USING_SEATED_ACTIVITY)
				CLEAR_PED_TASKS(PLAYER_PED_ID())
			ENDIF
	
		BREAK
		CASE CASINO_ISLAND_DJ_SWITCH_OVER_STATE_WARP_PLAYER
			PRINTLN("AM_MP_ISLAND - PERFORM_ISLAND_DJ_SWITCH_OVER_WARP - Inside: CASINO_ISLAND_DJ_SWITCH_OVER_STATE_WARP_PLAYER")
			
			IF IS_BROWSER_OPEN()
				CLOSE_WEB_BROWSER()
			ENDIF
			
			SET_UP_CUSTOM_SPAWN_POINTS_FOR_DJ_SWITCH_WARP()
			
			IF WARP_TO_SPAWN_LOCATION(SPAWN_LOCATION_CUSTOM_SPAWN_POINTS, FALSE, FALSE, FALSE, FALSE, DEFAULT, DEFAULT, FALSE)
				CLEAR_CUSTOM_SPAWN_POINTS()
				USE_CUSTOM_SPAWN_POINTS(FALSE)
				SET_ISLAND_DJ_SWITCH_OVER_STATE(CASINO_ISLAND_DJ_SWITCH_OVER_STATE_FADE_IN)
			ENDIF
		BREAK
		CASE CASINO_ISLAND_DJ_SWITCH_OVER_STATE_FADE_IN
			PRINTLN("AM_MP_ISLAND - PERFORM_ISLAND_DJ_SWITCH_OVER_WARP - Inside: CASINO_ISLAND_DJ_SWITCH_OVER_STATE_FADE_IN")

			IF HAS_NET_TIMER_EXPIRED(m_IslandData.stDJSwitchWarpDelay, CASINO_NIGHTCLUB_RENOVATION_DELAY_TIME_MS)
				RESET_NET_TIMER(m_IslandData.stDJSwitchWarpDelay)
				
				RESET_KICK_PLAYER_OUT_OF_SHOP_GLOBAL()
				g_bKillTheBed = FALSE
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				g_clubMusicData.bWarpAfterDJSwitchOver = FALSE
				
				SET_ISLAND_DJ_SWITCH_OVER_STATE(CASINO_ISLAND_DJ_SWITCH_OVER_STATE_IDLE)
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC

PROC MAINTAIN_DANCING()
	IF m_IslandData.sDanceActivityData.eStage = DLS_CLEANUP_COMPLETE
		DANCE_INIT(m_IslandData.sDanceActivityData)
		PRINTLN("AM_MP_ISLAND - MAINTAIN_DANCING - Reinitialising dancing")
	ENDIF

	BOOL bDancingPrevFrame = IS_PLAYER_DANCING(PLAYER_ID())
	BOOL bIsPlayerBusy = SHOULD_BLOCK_ISLAND_DANCING()
	NIGHTCLUB_AUDIO_TAGS eMusicIntensity = GET_NIGHTCLUB_AUDIO_TAG_FROM_CLUB_MUSIC_INTENSITY(m_IslandData.DJLocalData.eMusicIntensity)
	DANCING_ENABLE_DANCE_CAM_HELP(m_IslandData.sDanceActivityData, DANCE_CINEMATIC_CAM__IS_ACTIVE(m_IslandData.sDanceCam))
	DANCE_MAINTAIN_UPDATE(m_IslandData.sDanceActivityData, bIsPlayerBusy, eMusicIntensity)
	DANCE_CINEMATIC_CAM__MAINTAIN(m_IslandData.sDanceCam)
		
	// If player just started dancing?
	IF NOT bDancingPrevFrame AND IS_PLAYER_DANCING(PLAYER_ID()) 
		SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_AM_DANCE_BEACH_PARTY)
	ENDIF	
ENDPROC

PROC MAINTAIN_MUSIC_RUMBLE()
	#IF FEATURE_GEN9_RELEASE
	IF IS_PROSPERO_VERSION()
		EXIT
	ENDIF
	#ENDIF
	
	FLOAT timeS, bpm
	
	INT beatNum
	
	IF NOT HAS_NET_TIMER_STARTED(stBeatTimer)
		IF GET_NEXT_AUDIBLE_BEAT(timeS, bpm, beatNum)
		AND iBeatIndex != beatNum
			iBeatIndex = beatNum
			iUpcomingBeatTime = FLOOR(timeS * 1000)
			
			iUpcomingBeatTime += iBeatOffset
			
			IF iUpcomingBeatTime < 0
				iUpcomingBeatTime = 0
			ENDIF
			
			START_NET_TIMER(stBeatTimer)
		ENDIF
	ELIF HAS_NET_TIMER_EXPIRED(stBeatTimer, iUpcomingBeatTime)
		RESET_NET_TIMER(stBeatTimer)
		
		IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
			BOOL bDanceFloor, bMidArea, bOuterArea
			
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<4893.868652, -4900.226563, 1.378058>>, <<4893.095703, -4917.580566, 7.367704>>, 17.5)
				bDanceFloor = TRUE
			ELIF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<4893.781738, -4894.097168, 1.545530>>, <<4893.601563, -4926.185059, 7.361947>>, 25.0)
				bMidArea = TRUE
			ELIF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<4894.066895, -4878.833984, -1.466522>>, <<4891.250488, -4953.730957, 8.417141>>, 75.0)
				bOuterArea = TRUE
			ENDIF
			
			INT iFloorLowFreq = iLowFreq
			INT iFloorHighFreq = iHighFreq
			INT iFloorDuration = iBeatDuration
			INT iFloorLowFreqMedium = iLowFreqMedium
			INT iFloorHighFreqMedium = iHighFreqMedium
			INT iFloorDurationMedium = iBeatDurationMedium
			INT iFloorLowFreqIntense = iLowFreqIntense
			INT iFloorHighFreqIntense = iHighFreqIntense
			INT iFloorDurationIntense = iBeatDurationIntense
			
			FLOAT fDistance1 = GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()), <<4890.1611, -4906.7163, 2.3619>>)
			FLOAT fDistance2 = GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()), <<4896.8047, -4907.1597, 2.3619>>)
			FLOAT fDistance3 = GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()), <<4892.7871, -4906.8247, 2.6299>>)
			FLOAT fDistance4 = GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()), <<4891.9053, -4905.0830, 3.2695>>)
			FLOAT fDistance5 = GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()), <<4894.9395, -4905.1846, 3.2695>>)
			
			FLOAT fShortestDistance = PICK_FLOAT((fDistance1 <= fDistance2), fDistance1, fDistance2)
			fShortestDistance = PICK_FLOAT((fShortestDistance <= fDistance3), fShortestDistance, fDistance3)
			fShortestDistance = PICK_FLOAT((fShortestDistance <= fDistance3), fShortestDistance, fDistance4)
			fShortestDistance = PICK_FLOAT((fShortestDistance <= fDistance3), fShortestDistance, fDistance5)
			
			IF IS_PLAYER_DANCING(PLAYER_ID())
				iFloorLowFreq = iLowFreqDancing
				iFloorHighFreq = iHighFreqDancing
				iFloorDuration = iBeatDurationDancing
				iFloorLowFreqMedium = iLowFreqDancingMedium
				iFloorHighFreqMedium = iHighFreqDancingMedium
				iFloorDurationMedium = iBeatDurationDancingMedium
				iFloorLowFreqIntense = iLowFreqDancingIntense
				iFloorHighFreqIntense = iHighFreqDancingIntense
				iFloorDurationIntense = iBeatDurationDancingIntense
				
				IF fShortestDistance < fSpeakerDistance
					INT fPercentage = 100 - ROUND((fShortestDistance / fSpeakerDistance) * 100)
					
					iFloorLowFreq = iLowFreqDancing + ROUND((fPercentage / 100.0) * (iLowFreqSpeakers - iLowFreqDancing))
					iFloorHighFreq = iHighFreqDancing + ROUND((fPercentage / 100.0) * (iHighFreqSpeakers - iHighFreqDancing))
					iFloorDuration = iBeatDurationDancing + ROUND((fPercentage / 100.0) * (iBeatDurationSpeakers - iBeatDurationDancing))
					
					iFloorLowFreqMedium = iLowFreqDancingMedium + ROUND((fPercentage / 100.0) * (iLowFreqSpeakersMedium - iLowFreqDancingMedium))
					iFloorHighFreqMedium = iHighFreqDancingMedium + ROUND((fPercentage / 100.0) * (iHighFreqSpeakersMedium - iHighFreqDancingMedium))
					iFloorDurationMedium = iBeatDurationDancingMedium + ROUND((fPercentage / 100.0) * (iBeatDurationSpeakersMedium - iBeatDurationDancingMedium))
					
					iFloorLowFreqIntense = iLowFreqDancingIntense + ROUND((fPercentage / 100.0) * (iLowFreqSpeakersIntense - iLowFreqDancingIntense))
					iFloorHighFreqIntense = iHighFreqDancingIntense + ROUND((fPercentage / 100.0) * (iHighFreqSpeakersIntense - iHighFreqDancingIntense))
					iFloorDurationIntense = iBeatDurationDancingIntense + ROUND((fPercentage / 100.0) * (iBeatDurationSpeakersIntense - iBeatDurationDancingIntense))
				ENDIF
				
				IF m_IslandData.DJLocalData.eMusicIntensity = CLUB_MUSIC_INTENSITY_HIGH_HANDS
				OR m_IslandData.DJLocalData.eMusicIntensity = CLUB_MUSIC_INTENSITY_HIGH
					IF bOuterArea
						IF iBeatIndex = 1
							SET_CONTROL_SHAKE(PLAYER_CONTROL, iBeatDurationOutsideIntense, iHighFreqOutsideIntense)
						ELSE
							SET_CONTROL_SHAKE(PLAYER_CONTROL, iBeatDurationOutsideIntense, iLowFreqOutsideIntense)
						ENDIF
					ELIF bMidArea
						IF iBeatIndex = 1
							SET_CONTROL_SHAKE(PLAYER_CONTROL, iBeatDurationUpstairsDancingIntense, iHighFreqUpstairsDancingIntense)
						ELSE
							SET_CONTROL_SHAKE(PLAYER_CONTROL, iBeatDurationUpstairsDancingIntense, iLowFreqUpstairsDancingIntense)
						ENDIF
					ELIF bDanceFloor
						IF iBeatIndex = 1
							SET_CONTROL_SHAKE(PLAYER_CONTROL, iFloorDurationIntense, iFloorHighFreqIntense)
						ELSE
							SET_CONTROL_SHAKE(PLAYER_CONTROL, iFloorDurationIntense, iFloorLowFreqIntense)
						ENDIF
					ENDIF
				ELIF m_IslandData.DJLocalData.eMusicIntensity = CLUB_MUSIC_INTENSITY_MEDIUM
					IF bOuterArea
						IF iBeatIndex = 1
							SET_CONTROL_SHAKE(PLAYER_CONTROL, iBeatDurationOutsideMedium, iHighFreqOutsideMedium)
						ELSE
							SET_CONTROL_SHAKE(PLAYER_CONTROL, iBeatDurationOutsideMedium, iLowFreqOutsideMedium)
						ENDIF
					ELIF bMidArea
						IF iBeatIndex = 1
							SET_CONTROL_SHAKE(PLAYER_CONTROL, iBeatDurationUpstairsDancingMedium, iHighFreqUpstairsDancingMedium)
						ELSE
							SET_CONTROL_SHAKE(PLAYER_CONTROL, iBeatDurationUpstairsDancingMedium, iLowFreqUpstairsDancingMedium)
						ENDIF
					ELIF bDanceFloor
						IF iBeatIndex = 1
							SET_CONTROL_SHAKE(PLAYER_CONTROL, iFloorDurationMedium, iFloorHighFreqMedium)
						ELSE
							SET_CONTROL_SHAKE(PLAYER_CONTROL, iFloorDurationMedium, iFloorLowFreqMedium)
						ENDIF
					ENDIF
				ELIF m_IslandData.DJLocalData.eMusicIntensity = CLUB_MUSIC_INTENSITY_LOW
					IF bOuterArea
						IF iBeatIndex = 1
							SET_CONTROL_SHAKE(PLAYER_CONTROL, iBeatDurationOutside, iHighFreqOutside)
						ELSE
							SET_CONTROL_SHAKE(PLAYER_CONTROL, iBeatDurationOutside, iLowFreqOutside)
						ENDIF
					ELIF bMidArea
						IF iBeatIndex = 1
							SET_CONTROL_SHAKE(PLAYER_CONTROL, iBeatDurationUpstairsDancing, iHighFreqUpstairsDancing)
						ELSE
							SET_CONTROL_SHAKE(PLAYER_CONTROL, iBeatDurationUpstairsDancing, iLowFreqUpstairsDancing)
						ENDIF
					ELIF bDanceFloor
						IF iBeatIndex = 1
							SET_CONTROL_SHAKE(PLAYER_CONTROL, iFloorDuration, iFloorHighFreq)
						ELSE
							SET_CONTROL_SHAKE(PLAYER_CONTROL, iFloorDuration, iFloorLowFreq)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF fShortestDistance < fSpeakerDistance
					INT fPercentage = 100 - ROUND((fShortestDistance / fSpeakerDistance) * 100)
					
					iFloorLowFreq = iLowFreq + ROUND((fPercentage / 100.0) * (iLowFreqSpeakers - iLowFreq))
					iFloorHighFreq = iHighFreq + ROUND((fPercentage / 100.0) * (iHighFreqSpeakers - iHighFreq))
					iFloorDuration = iBeatDuration + ROUND((fPercentage / 100.0) * (iBeatDurationSpeakers - iBeatDuration))
					
					iFloorLowFreqMedium = iLowFreqMedium + ROUND((fPercentage / 100.0) * (iLowFreqSpeakersMedium - iLowFreqMedium))
					iFloorHighFreqMedium = iHighFreqMedium + ROUND((fPercentage / 100.0) * (iHighFreqSpeakersMedium - iHighFreqMedium))
					iFloorDurationMedium = iBeatDurationMedium + ROUND((fPercentage / 100.0) * (iBeatDurationSpeakersMedium - iBeatDurationMedium))
					
					iFloorLowFreqIntense = iLowFreqIntense + ROUND((fPercentage / 100.0) * (iLowFreqSpeakersIntense - iLowFreqIntense))
					iFloorHighFreqIntense = iHighFreqIntense + ROUND((fPercentage / 100.0) * (iHighFreqSpeakersIntense - iHighFreqIntense))
					iFloorDurationIntense = iBeatDurationIntense + ROUND((fPercentage / 100.0) * (iBeatDurationSpeakersIntense - iBeatDurationIntense))
				ENDIF
				
				IF m_IslandData.DJLocalData.eMusicIntensity = CLUB_MUSIC_INTENSITY_HIGH_HANDS
				OR m_IslandData.DJLocalData.eMusicIntensity = CLUB_MUSIC_INTENSITY_HIGH
					IF bOuterArea
						IF iBeatIndex = 1
							SET_CONTROL_SHAKE(PLAYER_CONTROL, iBeatDurationOutsideIntense, iHighFreqOutsideIntense)
						ELSE
							SET_CONTROL_SHAKE(PLAYER_CONTROL, iBeatDurationOutsideIntense, iLowFreqOutsideIntense)
						ENDIF
					ELIF bMidArea
						IF iBeatIndex = 1
							SET_CONTROL_SHAKE(PLAYER_CONTROL, iBeatDurationUpstairsIntense, iHighFreqUpstairsIntense)
						ELSE
							SET_CONTROL_SHAKE(PLAYER_CONTROL, iBeatDurationUpstairsIntense, iLowFreqUpstairsIntense)
						ENDIF
					ELIF bDanceFloor
						IF iBeatIndex = 1
							SET_CONTROL_SHAKE(PLAYER_CONTROL, iFloorDurationIntense, iFloorHighFreqIntense)
						ELSE
							SET_CONTROL_SHAKE(PLAYER_CONTROL, iFloorDurationIntense, iFloorLowFreqIntense)
						ENDIF
					ENDIF
				ELIF m_IslandData.DJLocalData.eMusicIntensity = CLUB_MUSIC_INTENSITY_MEDIUM
					IF bOuterArea
						IF iBeatIndex = 1
							SET_CONTROL_SHAKE(PLAYER_CONTROL, iBeatDurationOutsideMedium, iHighFreqOutsideMedium)
						ELSE
							SET_CONTROL_SHAKE(PLAYER_CONTROL, iBeatDurationOutsideMedium, iLowFreqOutsideMedium)
						ENDIF
					ELIF bMidArea
						IF iBeatIndex = 1
							SET_CONTROL_SHAKE(PLAYER_CONTROL, iBeatDurationUpstairsMedium, iHighFreqUpstairsMedium)
						ELSE
							SET_CONTROL_SHAKE(PLAYER_CONTROL, iBeatDurationUpstairsMedium, iLowFreqUpstairsMedium)
						ENDIF
					ELIF bDanceFloor
						IF iBeatIndex = 1
							SET_CONTROL_SHAKE(PLAYER_CONTROL, iFloorDurationMedium, iFloorHighFreqMedium)
						ELSE
							SET_CONTROL_SHAKE(PLAYER_CONTROL, iFloorDurationMedium, iFloorLowFreqMedium)
						ENDIF
					ENDIF
				ELIF m_IslandData.DJLocalData.eMusicIntensity = CLUB_MUSIC_INTENSITY_LOW
					IF bOuterArea
						IF iBeatIndex = 1
							SET_CONTROL_SHAKE(PLAYER_CONTROL, iBeatDurationOutside, iHighFreqOutside)
						ELSE
							SET_CONTROL_SHAKE(PLAYER_CONTROL, iBeatDurationOutside, iLowFreqOutside)
						ENDIF
					ELIF bMidArea
						IF iBeatIndex = 1
							SET_CONTROL_SHAKE(PLAYER_CONTROL, iBeatDurationUpstairs, iHighFreqUpstairs)
						ELSE
							SET_CONTROL_SHAKE(PLAYER_CONTROL, iBeatDurationUpstairs, iLowFreqUpstairs)
						ENDIF
					ELIF bDanceFloor
						IF iBeatIndex = 1
							SET_CONTROL_SHAKE(PLAYER_CONTROL, iFloorDuration, iFloorHighFreq)
						ELSE
							SET_CONTROL_SHAKE(PLAYER_CONTROL, iFloorDuration, iFloorLowFreq)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_MINIMAP_MISSION_BLIP_HIDE()
	IF NOT g_matcBlockAndHideAllMissions
	   Block_All_MissionsAtCoords_Missions(TRUE)
	ENDIF
ENDPROC

PROC RUN_MAIN_CLIENT_LOGIC()
	MAINTAIN_AT_BEACH_PARTY()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_AT_BEACH_PARTY")
	#ENDIF
	#ENDIF
	
	MAINTAIN_MINIMAP_MISSION_BLIP_HIDE()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_MINIMAP_MISSION_BLIP_HIDE")
	#ENDIF
	#ENDIF
	
	MAINTAIN_ISLAND_CONTROLS()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_ISLAND_CONTROLS")
	#ENDIF
	#ENDIF

	MAINTAIN_TIME_FOR_ISLAND()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_TIME_FOR_ISLAND")
	#ENDIF
	#ENDIF
	
	IF SHOULD_RUN_BEACH_MUSIC()
		MAINTAIN_CLUB_MUSIC(CLUB_LOCATION_ISLAND, m_ServerBD.DJServerData, m_IslandData.DJLocalData)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_CLUB_MUSIC")
		#ENDIF
		#ENDIF
	ENDIF
	
	IF SHOULD_BEACH_PARTY_RUN()
		MAINTAIN_PED_SCRIPT_LAUNCHING(PED_LOCATION_ISLAND, m_IslandData.bPedScriptLaunched, PED_SCRIPT_INSTANCE_ISLAND, FALSE)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_PED_SCRIPT_LAUNCHING")
		#ENDIF
		#ENDIF
		
		MAINTAIN_WALKING_STYLE()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_WALKING_STYLE")
		#ENDIF
		#ENDIF
		
		MAINTAIN_SEATING_ACTIVITY_LAUNCHING()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_SEATING_ACTIVITY_LAUNCHING")
		#ENDIF
		#ENDIF
	ENDIF
	
	IF IS_ISLAND_STATE(ISLAND_STATE_LOADING)
		LOAD_ISLAND_CLIENT()
	ELIF IS_ISLAND_STATE(ISLAND_STATE_IDLE)
		IF SHOULD_BEACH_PARTY_RUN()			
			MAINTAIN_DRINKING_ACTIVITY(m_ServerBD.sDrinkingActivityData, sDrinkingActivityPlayerBD, m_IslandData.sDrinkingActivityData)
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_DRINKING_ACTIVITY")
			#ENDIF
			#ENDIF
				
			MAINTAIN_DANCING()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_DANCING")
			#ENDIF
			#ENDIF
			
			HEIST_ISLAND_TRAVEL_BEACH_PARTY__MAINTAIN(m_IslandData.sBeachPartyTravel)
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("HEIST_ISLAND_TRAVEL_BEACH_PARTY__MAINTAIN")
			#ENDIF
			#ENDIF
			
			PERFORM_ISLAND_DJ_SWITCH_OVER_WARP()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_ISLAND_WARP_AFTER_DJ_SWITCH")
			#ENDIF
			#ENDIF
			
			MAINTAIN_MUSIC_RUMBLE()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_MUSIC_RUMBLE")
			#ENDIF
			#ENDIF
		ELSE
			MAINTAIN_STOPPING_BEACH_PARTY()
		ENDIF
	ENDIF
ENDPROC

PROC RUN_MAIN_SERVER_LOGIC()
	IF IS_ISLAND_SERVER_STATE(ISLAND_SERVER_STATE_LOADING)
		SET_ISLAND_SERVER_STATE(ISLAND_SERVER_STATE_IDLE)
		m_ServerBD.iTod	= GET_RANDOM_ISLAND_TOD()	
		PRINTLN("AM_MP_ISLAND - m_ServerBD.iTod	= GET_RANDOM_ISLAND_TOD() = ", m_ServerBD.iTod)
	ELIF IS_ISLAND_SERVER_STATE(ISLAND_SERVER_STATE_IDLE)
		BOOL bForceDeletePeds = FALSE
		
		IF NOT SHOULD_BEACH_PARTY_RUN()
			bForceDeletePeds = TRUE
		ENDIF
		
		MAINTAIN_BAR_SERVER(m_ServerBD.sDrinkingActivityData, m_IslandData.sDrinkingActivityData, bForceDeletePeds)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_BAR_SERVER")
		#ENDIF
		#ENDIF
	ENDIF
ENDPROC


#ENDIF // FEATURE_HEIST_ISLAND

//╒════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════════╡ SCRIPT ╞═════════════════════════════════╡
//╘════════════════════════════════════════════════════════════════════════════╛

SCRIPT
	#IF FEATURE_HEIST_ISLAND
	IF NETWORK_IS_GAME_IN_PROGRESS()
		SCRIPT_INITIALISE()
	ELSE
		SCRIPT_CLEANUP()
	ENDIF
	
	WHILE TRUE
		MP_LOOP_WAIT_ZERO()
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		SCRIPT_PROFILER_START_OF_FRAME()
		#ENDIF
		#ENDIF
		
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			PRINTLN("AM_MP_ISLAND - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE = TRUE")
			
			SCRIPT_CLEANUP()
		ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE")
		#ENDIF
		#ENDIF
		
		IF g_bCleanupIslandScript
			PRINTLN("AM_MP_ISLAND - g_bCleanupIslandScript = TRUE")
			
			SCRIPT_CLEANUP()
		ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("g_bCleanupIslandScript")
		#ENDIF
		#ENDIF
		
		IF IS_ISLAND_SERVER_STATE(ISLAND_SERVER_STATE_IDLE)
			RUN_MAIN_CLIENT_LOGIC()
		ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("RUN_MAIN_CLIENT_LOGIC")
		#ENDIF
		#ENDIF
		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			RUN_MAIN_SERVER_LOGIC()
		ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("RUN_MAIN_SERVER_LOGIC")
		#ENDIF
		#ENDIF
		
		#IF IS_DEBUG_BUILD
		UPDATE_DEBUG_WIDGETS()
		UPDATE_PROP_HIDING()
		
		#IF SCRIPT_PROFILER_ACTIVE
		SCRIPT_PROFILER_END_OF_FRAME()
		#ENDIF
		#ENDIF
	ENDWHILE
	#ENDIF // FEATURE_HEIST_ISLAND
ENDSCRIPT
