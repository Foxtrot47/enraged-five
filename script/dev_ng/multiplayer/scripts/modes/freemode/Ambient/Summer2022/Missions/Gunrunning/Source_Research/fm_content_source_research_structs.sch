USING "globals.sch"
USING "rage_builtins.sch"
USING "fm_content_includes.sch"

//----------------------
//	CONSTANTS
//----------------------

CONST_INT MAX_NUM_STATES 					6
CONST_INT MAX_NUM_CLIENT_STATES				7
CONST_INT MAX_NUM_GOTO_LOCATIONS			1
CONST_INT MAX_NUM_DROPOFFS					1
CONST_INT MAX_NUM_TARGETS					1
CONST_INT MAX_NUM_PEDS						30
CONST_INT MAX_NUM_PROPS						30
CONST_INT MAX_NUM_VEHICLES					30
CONST_INT MAX_NUM_INTERACT_LOCATIONS		1
CONST_INT MAX_NUM_SEARCH_AREAS				1
CONST_INT MAX_NUM_POCKET_ITEMS				1
CONST_INT MAX_NUM_TAGS						8
CONST_INT MAX_NUM_PED_BEHAVIOURS			3
CONST_INT MAX_NUM_PED_GROUPS				2
CONST_INT MAX_NUM_CUSTOM_SPAWNS				9
CONST_INT MAX_NUM_TRIGGER_AREAS				2
CONST_INT MAX_NUM_HELP_TEXTS				1
CONST_INT MAX_NUM_POP_BLOCKERS				1
CONST_INT MAX_NUM_DIALOGUE					1
CONST_INT MAX_NUM_MUSIC_TRIGGERS			5
CONST_INT MAX_NUM_AMBIENT_SPEECH_PEDS		1
CONST_INT MAX_NUM_BOTTOM_RIGHT_HUD			1

ENUM AMBIENT_SPEECH_COLLECTION
	eAMBIENTSPEECHCOLLECTION_HELI_PILOT_SPOTTED,
	eAMBIENTSPEECHCOLLECTION_END	// Keep this at the end
ENDENUM

//----------------------
//	SERVER VARIABLES
//----------------------

STRUCT MISSION_SPECIFIC_SERVER_DATA
	SOURCE_RESEARCH_VARIATION eMissionVariation = SRV_INVALID
	SOURCE_RESEARCH_SUBVARIATION eMissionSubvariation = SRS_INVALID
ENDSTRUCT

//----------------------
//	LOCAL VARIABLES
//----------------------


//----------------------
//	BITSET WRAPPERS
//----------------------

ENUM eMISSION_CLIENT_BITSET
	eMISSIONCLIENTBITSET_SENT_KILL_TICKER,
	eMISSIONCLIENTBITSET_SETUP_SEARCHLIGHT_TARGET,
	eMISSIONCLIENTBITSET_SENT_TRIGGER_AREA_WANTED,
	eMISSIONCLIENTBITSET_PLAYER_GOT_WANTED_IN_RANGE,
	eMISSIONCLIENTBITSET_LOCATED_TARGET,
	
	eMISSIONCLIENTBITSET_END
ENDENUM

ENUM eMISSION_SERVER_BITSET
	eMISSIONSERVERBITSET_SENT_KILL_TICKER,
	eMISSIONSERVERBITSET_SENT_TRIGGER_AREA_WANTED,
	eMISSIONSERVERBITSET_ANY_PLAYER_GOT_WANTED_IN_RANGE,
	eMISSIONSERVERBITSET_LOCATED_TARGET,
	
	eMISSIONSERVERBITSET_END
ENDENUM

ENUM eMISSION_LOCAL_BITSET
	eMISSIONLOCALBITSET_GAINED_WANTED_LEVEL,
	eMISSIONLOCALBITSET_STARTED_AUDIO_SCENE,
	eMISSIONLOCALBITSET_STOPPED_AUDIO_STREAM,
	
	eMISSIONLOCALBITSET_END
ENDENUM

FUNC eMISSION_SERVER_BITSET GET_MISSION_SERVER_BIT_FOR_CLIENT_BIT(eMISSION_CLIENT_BITSET eClientBit)
	SWITCH eClientBit
		CASE eMISSIONCLIENTBITSET_SENT_KILL_TICKER					RETURN eMISSIONSERVERBITSET_SENT_KILL_TICKER
		CASE eMISSIONCLIENTBITSET_SENT_TRIGGER_AREA_WANTED			RETURN eMISSIONSERVERBITSET_SENT_TRIGGER_AREA_WANTED
		CASE eMISSIONCLIENTBITSET_PLAYER_GOT_WANTED_IN_RANGE		RETURN eMISSIONSERVERBITSET_ANY_PLAYER_GOT_WANTED_IN_RANGE
		CASE eMISSIONCLIENTBITSET_LOCATED_TARGET					RETURN eMISSIONSERVERBITSET_LOCATED_TARGET
		
		CASE eMISSIONCLIENTBITSET_END								RETURN eMISSIONSERVERBITSET_END
	ENDSWITCH
	
	RETURN eMISSIONSERVERBITSET_END
ENDFUNC

#IF IS_DEBUG_BUILD
FUNC STRING GET_MISSION_SERVER_BIT_NAME(eMISSION_SERVER_BITSET eBit)
	SWITCH eBit
		CASE eMISSIONSERVERBITSET_SENT_KILL_TICKER					RETURN "eMISSIONSERVERBITSET_SENT_KILL_TICKER"
		CASE eMISSIONSERVERBITSET_SENT_TRIGGER_AREA_WANTED			RETURN "eMISSIONSERVERBITSET_SENT_TRIGGER_AREA_WANTED"
		CASE eMISSIONSERVERBITSET_ANY_PLAYER_GOT_WANTED_IN_RANGE	RETURN "eMISSIONSERVERBITSET_ANY_PLAYER_GOT_WANTED_IN_RANGE"
		CASE eMISSIONSERVERBITSET_LOCATED_TARGET					RETURN "eMISSIONSERVERBITSET_LOCATED_TARGET"
		
		CASE eMISSIONSERVERBITSET_END								RETURN "eMISSIONSERVERBITSET_END"
	ENDSWITCH
	
	RETURN "***INVALID***"
ENDFUNC

FUNC STRING GET_MISSION_CLIENT_BIT_NAME(eMISSION_CLIENT_BITSET eBit)
	SWITCH eBit
		CASE eMISSIONCLIENTBITSET_SENT_KILL_TICKER				RETURN "eMISSIONCLIENTBITSET_SENT_KILL_TICKER"
		CASE eMISSIONCLIENTBITSET_SETUP_SEARCHLIGHT_TARGET		RETURN "eMISSIONCLIENTBITSET_SETUP_SEARCHLIGHT_TARGET"
		CASE eMISSIONCLIENTBITSET_SENT_TRIGGER_AREA_WANTED		RETURN "eMISSIONCLIENTBITSET_SENT_TRIGGER_AREA_WANTED"
		CASE eMISSIONCLIENTBITSET_PLAYER_GOT_WANTED_IN_RANGE	RETURN "eMISSIONCLIENTBITSET_PLAYER_GOT_WANTED_IN_RANGE"
		CASE eMISSIONCLIENTBITSET_LOCATED_TARGET				RETURN "eMISSIONCLIENTBITSET_LOCATED_TARGET"
		
		CASE eMISSIONCLIENTBITSET_END							RETURN "eMISSIONCLIENTBITSET_END"
	ENDSWITCH
	
	RETURN "***INVALID***"
ENDFUNC

FUNC STRING GET_MISSION_LOCAL_BIT_NAME(eMISSION_LOCAL_BITSET eBit)
	SWITCH eBit
		CASE eMISSIONLOCALBITSET_GAINED_WANTED_LEVEL					RETURN "eMISSIONLOCALBITSET_GAINED_WANTED_LEVEL"
		CASE eMISSIONLOCALBITSET_STARTED_AUDIO_SCENE					RETURN "eMISSIONLOCALBITSET_STARTED_AUDIO_SCENE"
		CASE eMISSIONLOCALBITSET_STOPPED_AUDIO_STREAM					RETURN "eMISSIONLOCALBITSET_STOPPED_AUDIO_STREAM"
		
		CASE eMISSIONLOCALBITSET_END									RETURN "eMISSIONLOCALBITSET_END"
	ENDSWITCH
	
	RETURN "***INVALID***"
ENDFUNC
#ENDIF

