USING "globals.sch"
USING "rage_builtins.sch"

CONST_INT ENABLE_CONTENT	FEATURE_DLC_1_2022

#IF ENABLE_CONTENT
USING "Variations/fm_content_gunrunning_hack_plane.sch"
USING "Variations/fm_content_gunrunning_ocean_supply_drop.sch"

/// PURPOSE: Use to set up the logic pointers for your mission
///    
PROC SETUP_MISSION_LOGIC()
	SWITCH GET_VARIATION()
		CASE GRRV_OCEAN_SUPPLY_DROP 	OSD_SETUP_MISSION_LOGIC()		BREAK
		CASE GRRV_HACK_PLANE		 	HP_SETUP_MISSION_LOGIC()		BREAK
		
		#IF IS_DEBUG_BUILD
		DEFAULT
			SCRIPT_ASSERT("Please populate SETUP_MISSION_LOGIC in fm_content_gunrunning_variations.sch")
		BREAK
		#ENDIF
	ENDSWITCH
ENDPROC
#ENDIF
