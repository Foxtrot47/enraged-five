USING "globals.sch"
USING "rage_builtins.sch"

CONST_INT ENABLE_CONTENT	FEATURE_DLC_1_2022

#IF ENABLE_CONTENT
USING "Variations/fm_content_cargo_auto_shop_robbery.sch"
USING "Variations/fm_content_cargo_underwater_cargo.sch"

/// PURPOSE: Use to set up the logic pointers for your mission
///    
PROC SETUP_MISSION_LOGIC()
	SWITCH GET_VARIATION()
		CASE SCGV_AUTO_SHOP_ROBBERY		ASR_SETUP_LOGIC()	BREAK
		CASE SCGV_UNDERWATER_CARGO	 	UC_SETUP_LOGIC()	BREAK
		#IF IS_DEBUG_BUILD
		DEFAULT
			SCRIPT_ASSERT("Please populate SETUP_MISSION_LOGIC in fm_content_payphone_hit_variations.sch")
		BREAK
		#ENDIF
	ENDSWITCH
ENDPROC
#ENDIF
