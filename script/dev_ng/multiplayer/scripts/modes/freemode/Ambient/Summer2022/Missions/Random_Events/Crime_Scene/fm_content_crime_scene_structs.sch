USING "globals.sch"
USING "rage_builtins.sch"
USING "fm_content_includes.sch"

CONST_INT MAX_NUM_PED_GROUPS 			2
CONST_INT MAX_NUM_PEDS 					2	
CONST_INT MAX_NUM_VEHICLES 				1
CONST_INT MAX_NUM_PROPS 				8
CONST_INT MAX_NUM_TRIGGER_AREAS			3
CONST_INT MAX_NUM_INTERACT_LOCATIONS	1
CONST_INT MAX_NUM_POP_BLOCKERS			1
CONST_INT MAX_NUM_SCENARIOS				2
CONST_INT MAX_NUM_HELP_TEXTS			1
CONST_INT MAX_NUM_PED_TRIGGERS			7

CONST_INT INVALID_RIFLE_COMPONENT		-1
CONST_INT NUM_TACTICAL_RIFLE_PARTS		5
CONST_INT CRIME_SCENE_INTERACTION		0
CONST_INT CRIME_SCENE_CLEANUP_TIME		120000

CONST_INT MAX_NUM_DECALS				4

CONST_INT CRIME_SCENE_PED_BEHAVIOUR_COP	0

CONST_FLOAT CRIME_SCENE_CENTRE_BLIP_RANGE	10.0

ENUM CRIME_SCENE_PROP
	CSP_INVALID = -1,
	
	CSP_CASH_CASE,
	CSP_RIFLE_BARREL,
	CSP_RIFLE_MAGAZINE,
	CSP_RIFLE_TRIGGER,
	CSP_RIFLE_SIGHTS,
	CSP_RIFLE_STOCK,
	
	CSP_MAX
ENDENUM

STRUCT DECALS_STRUCT
	INT iDecalBitSet
	INT iDecalCreationStagger
	DECAL_ID decalId[MAX_NUM_DECALS]
ENDSTRUCT

STRUCT MISSION_SPECIFIC_SERVER_DATA
	CRIME_SCENE_VARIATION eMissionVariation = CRSV_INVALID
	CRIME_SCENE_SUBVARIATION eMissionSubvariation = CRSS_INVALID
	
	SCRIPT_TIMER stEndTimer
ENDSTRUCT

STRUCT MISSION_SPECIFIC_LOCAL_VARIABLES
	DECALS_STRUCT sDecals
	
	CRIME_SCENE_PROP eCrimeSceneProp = CSP_INVALID
	
	OBJECT_INDEX propID
	
	FLOAT fInteractSceneHeading
	
	INT iWantedTriggerArea = -1
ENDSTRUCT

MISSION_SPECIFIC_LOCAL_VARIABLES sLocalVariables

ENUM eMISSION_SERVER_BITSET
	eMISSIONSERVERBITSET_SOMEONE_HAS_COLLECTED_PROP,
	eMISSIONSERVERBITSET_MODE_SHOULD_END,
	eMISSIONSERVERBITSET_SOMEONE_ATTEMPTED_TO_STEAL_COP_CAR,
	
	eMISSIONSERVERBITSET_END
ENDENUM

ENUM eMISSION_CLIENT_BITSET
	eMISSIONCLIENTBITSET_I_HAVE_COLLECTED_PROP,
	eMISSIONCLIENTBITSET_I_ATTEMPTED_TO_STEAL_COP_CAR,
	
	eMISSIONCLIENTBITSET_END
ENDENUM

ENUM eMISSION_LOCAL_BITSET
	eMISSIONLOCALBITSET_FOUND_PROP,
	eMISSIONLOCALBITSET_ENTERED_AREA,
	eMISSIONLOCALBITSET_GIVEN_WANTED,
	eMISSIONLOCALBITSET_GIVE_WEAPON,
	
	eMISSIONLOCALBITSET_END
ENDENUM

#IF IS_DEBUG_BUILD
FUNC STRING GET_MISSION_SERVER_BIT_NAME(eMISSION_SERVER_BITSET eBit)
	SWITCH eBit
		CASE eMISSIONSERVERBITSET_SOMEONE_HAS_COLLECTED_PROP				RETURN "eMISSIONSERVERBITSET_SOMEONE_HAS_COLLECTED_PROP"
		CASE eMISSIONSERVERBITSET_MODE_SHOULD_END							RETURN "eMISSIONSERVERBITSET_MODE_SHOULD_END"
		CASE eMISSIONSERVERBITSET_SOMEONE_ATTEMPTED_TO_STEAL_COP_CAR		RETURN "eMISSIONSERVERBITSET_SOMEONE_ATTEMPTED_TO_STEAL_COP_CAR"
	
		CASE eMISSIONSERVERBITSET_END										RETURN "eMISSIONSERVERBITSET_END"
	ENDSWITCH
	
	RETURN "***INVALID***"
ENDFUNC

FUNC STRING GET_MISSION_CLIENT_BIT_NAME(eMISSION_CLIENT_BITSET eBit)
	SWITCH eBit
		CASE eMISSIONCLIENTBITSET_I_HAVE_COLLECTED_PROP						RETURN "eMISSIONCLIENTBITSET_I_HAVE_COLLECTED_PROP"
		CASE eMISSIONCLIENTBITSET_I_ATTEMPTED_TO_STEAL_COP_CAR				RETURN "eMISSIONCLIENTBITSET_I_ATTEMPTED_TO_STEAL_COP_CAR"
	
		CASE eMISSIONCLIENTBITSET_END										RETURN "eMISSIONCLIENTBITSET_END"
	ENDSWITCH
	
	RETURN "***INVALID***"
ENDFUNC

FUNC STRING GET_MISSION_LOCAL_BIT_NAME(eMISSION_LOCAL_BITSET eBit)
	SWITCH eBit
		CASE eMISSIONLOCALBITSET_FOUND_PROP									RETURN "eMISSIONLOCALBITSET_FOUND_PROP"
		CASE eMISSIONLOCALBITSET_ENTERED_AREA								RETURN "eMISSIONLOCALBITSET_ENTERED_AREA"
		CASE eMISSIONLOCALBITSET_GIVEN_WANTED								RETURN "eMISSIONLOCALBITSET_GIVEN_WANTED"
		CASE eMISSIONLOCALBITSET_GIVE_WEAPON								RETURN "eMISSIONLOCALBITSET_GIVE_WEAPON"
		
		CASE eMISSIONLOCALBITSET_END										RETURN "eMISSIONLOCALBITSET_END"
	ENDSWITCH
	
	RETURN "***INVALID***"
ENDFUNC
#ENDIF

FUNC eMISSION_SERVER_BITSET GET_MISSION_SERVER_BIT_FOR_CLIENT_BIT(eMISSION_CLIENT_BITSET eClientBit)
	SWITCH eClientBit
		CASE eMISSIONCLIENTBITSET_I_HAVE_COLLECTED_PROP					RETURN eMISSIONSERVERBITSET_SOMEONE_HAS_COLLECTED_PROP
		CASE eMISSIONCLIENTBITSET_I_ATTEMPTED_TO_STEAL_COP_CAR			RETURN eMISSIONSERVERBITSET_SOMEONE_ATTEMPTED_TO_STEAL_COP_CAR
	ENDSWITCH
	
	RETURN eMISSIONSERVERBITSET_END
ENDFUNC





