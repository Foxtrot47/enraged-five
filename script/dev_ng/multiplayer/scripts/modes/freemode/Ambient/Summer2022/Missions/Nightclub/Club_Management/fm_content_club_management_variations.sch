USING "globals.sch"
USING "rage_builtins.sch"

CONST_INT ENABLE_CONTENT	FEATURE_DLC_1_2022

#IF ENABLE_CONTENT
USING "Variations/fm_content_club_management_drug_problem.sch"
USING "Variations/fm_content_club_management_paparazzi.sch"

/// PURPOSE: Use to set up the logic pointers for your mission
///    
PROC SETUP_MISSION_LOGIC()
	SWITCH GET_VARIATION()
		CASE CMV_PAPARAZZI		PAPARAZZI_SETUP_LOGIC()	BREAK
		CASE CMV_DRUG_PROBLEM	DP_SETUP_LOGIC()		BREAK
	ENDSWITCH
ENDPROC
#ENDIF
