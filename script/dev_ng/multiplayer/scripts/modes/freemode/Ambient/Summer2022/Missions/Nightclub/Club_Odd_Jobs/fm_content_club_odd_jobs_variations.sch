USING "globals.sch"
USING "rage_builtins.sch"

CONST_INT ENABLE_CONTENT	FEATURE_DLC_1_2022

#IF ENABLE_CONTENT
USING "Variations/fm_content_club_odd_jobs_passed_out_vip.sch"

/// PURPOSE: Use to set up the logic pointers for your mission
///    
PROC SETUP_MISSION_LOGIC()
	SWITCH GET_VARIATION()
		CASE COJV_PASSED_OUT_VIP		POVIP_SETUP_MISSION_LOGIC()		BREAK
	ENDSWITCH
ENDPROC
#ENDIF
