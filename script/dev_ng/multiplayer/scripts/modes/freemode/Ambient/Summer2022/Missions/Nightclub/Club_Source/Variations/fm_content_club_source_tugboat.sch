USING "globals.sch"
USING "rage_builtins.sch"
USING "Nightclub/Club_Source/fm_content_club_source_core.sch"

//////////////////////////
///    CONSTS
//////////////////////////

CONST_INT cTUG_PICKUP_ARMOUR		0
CONST_INT cTUG_PICKUP_AMMO			1
CONST_INT cTUG_PICKUP_HEALTH		2

CONST_INT cTUG_ATTACH_BOXES		0
CONST_INT cTUG_ATTACH_AMMO		1
CONST_INT cTUG_ATTACH_HEALTH	2
CONST_INT cTUG_ATTACH_GOODS		3

CONST_INT cTUG_TIMER_FOOT_AMBUSH_DELAY		10000

//////////////////////////
///    ENUMS
//////////////////////////

ENUM TUG_STATE
	eTB_STATE_GO_TO,
	eTB_STATE_STEAL,
	eTB_STATE_DELIVER,
	
	eTB_STATE_END
ENDENUM

ENUM TUG_CLIENT_STATE
	eTB_CLIENTSTATE_GO_TO,
	eTB_CLIENTSTATE_STEAL,
	eTB_CLIENTSTATE_DELIVER,
	eTB_CLIENTSTATE_HELP_DELIVER,
	eTB_CLIENTSTATE_RECOVER,
	
	eTB_CLIENTSTATE_END
ENDENUM

ENUM TUG_TAGS
	eTB_TAG_TUGBOAT,
	eTB_TAG_GOODS,
	eTB_TAG_BOXES,
	eTB_TAG_AMMO,
	eTB_TAG_HEALTH,
	eTB_TAG_ARMOUR,
	eTB_TAG_RESPAWN_1,
	eTB_TAG_RESPAWN_2,
	eTB_TAG_RESPAWN_3
ENDENUM

ENUM TUG_TICKER
	eTB_TICKER_COLLECT,
	eTB_TICKER_DELIVER
ENDENUM

ENUM TUG_DIALOGUE
	eTB_DIALOGUE_OPENING
ENDENUM

ENUM TUG_MUSIC
	eTB_MUSIC_INVALID = -1,
	eTB_MUSIC_NCLUB_DELIVERING_START,
	eTB_MUSIC_NCLUB_SUSPENSE,
	eTB_MUSIC_NCLUB_GUNFIGHT,
	eTB_MUSIC_NCLUB_MED_INTENSITY
ENDENUM


//////////////////////////
///    HELPERS
//////////////////////////

FUNC TUG_STATE TUG_GET_STATE()
	RETURN INT_TO_ENUM(TUG_STATE, GET_MODE_STATE_ID())
ENDFUNC

FUNC TUG_CLIENT_STATE TUG_GET_CLIENT_STATE()
	RETURN INT_TO_ENUM(TUG_CLIENT_STATE, GET_CLIENT_MODE_STATE_ID())
ENDFUNC

FUNC BOOL TUG_DO_DIALOGUE_VARIATION()
	RETURN serverBD.iRandomModeInt1 = 1
ENDFUNC

FUNC BOOL TUG_SHOULD_ACTIVATE_AMBUSH()
	IF IS_MISSION_SERVER_BIT_SET(eMISSIONSERVERBITSET_KILLED_PED) 
	AND HAS_NET_TIMER_EXPIRED(serverBD.sMissionData.stFootAmbushDelay, cTUG_TIMER_FOOT_AMBUSH_DELAY)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

//////////////////////////
///    OBJECTIVE TEXT
//////////////////////////

PROC TUG_OBJ_GOTO()
	SWITCH GET_SUBVARIATION()
		CASE CSCS_TB_LOCATION_1
			Print_Objective_Text("NCS_OT_GOTO1")
		BREAK
		CASE CSCS_TB_LOCATION_2
			Print_Objective_Text("NCS_OT_GOTO2")
		BREAK
		DEFAULT
			Print_Objective_Text("NCS_OT_GOTO3")
		BREAK
	ENDSWITCH
ENDPROC

//////////////////////////
///    MODE STATES
//////////////////////////

PROC TUG_STATE_SETUP()
	ADD_MODE_STATE(eTB_STATE_GO_TO, eMODESTATE_GO_TO_POINT)
		ADD_MODE_STATE_TRANSITION_DEFAULT(eTB_STATE_GO_TO, eTB_STATE_STEAL)
		
	ADD_MODE_STATE(eTB_STATE_STEAL, eMODESTATE_COLLECT_MISSION_ENTITY)
		ADD_MODE_STATE_TRANSITION_DEFAULT(eTB_STATE_STEAL, eTB_STATE_DELIVER)
		
	ADD_MODE_STATE(eTB_STATE_DELIVER, eMODESTATE_DELIVER_MISSION_ENTITY)
		ADD_MODE_STATE_TRANSITION_DEFAULT(eTB_STATE_DELIVER, eTB_STATE_END)
	
	ADD_MODE_STATE(eTB_STATE_END, eMODESTATE_END)
ENDPROC

PROC TUG_STATE_SETUP_CLIENT()
	ADD_CLIENT_MODE_STATE(eTB_CLIENTSTATE_GO_TO, eMODESTATE_GO_TO_POINT, &TUG_OBJ_GOTO)
		ADD_CLIENT_MODE_STATE_TRANSITION_LINKED(eTB_CLIENTSTATE_GO_TO, eTB_CLIENTSTATE_STEAL, eTB_STATE_STEAL)
	
	ADD_CLIENT_MODE_STATES_DELIVERY(eTB_CLIENTSTATE_STEAL, eTB_CLIENTSTATE_DELIVER, eTB_CLIENTSTATE_HELP_DELIVER, eTB_CLIENTSTATE_RECOVER, eTB_CLIENTSTATE_END,
									&SRC_OBJ_STEAL,   &SRC_OBJ_DELIVER,   &SRC_OBJ_HELP_DELIVER,   &EMPTY)
	
	ADD_CLIENT_MODE_STATE(eTB_CLIENTSTATE_END, eMODESTATE_END, &EMPTY)
ENDPROC

//////////////////////////
///    MISSION ENTITY
//////////////////////////

FUNC BOOL TUG_MISSION_ENTITY_SHOULD_SPAWN(INT iMissionEntity)
	UNUSED_PARAMETER(iMissionEntity)
	IF IS_PROP_BIT_SET(GET_TAG_ID(eTB_TAG_GOODS), ePROPBITSET_PLAYER_HAS_BEEN_NEAR)
		RETURN TRUE
	ENDIF
	
//	IF GET_VEHICLE_STATE(eTB_TAG_TUGBOAT) > eVEHICLESTATE_DRIVEABLE
//		RETURN TRUE
//	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC TUG_MISSION_ENTITY_ONCOLLECT(INT iMissionEntity)
	UNUSED_PARAMETER(iMissionEntity)
	
	MISSION_TICKER_DATA sTickerData
	sTickerData.iInt1 = ENUM_TO_INT(eTB_TICKER_COLLECT)
	SEND_FM_CONTENT_TICKER_EVENT_WITH_DATA(eFMCTICKER_CUSTOM, sTickerData) 
	
	IF NOT IS_MISSION_SERVER_BIT_SET(eMISSIONSERVERBITSET_DONE_FIRST_PICKUP_AUDIO)
		PLAY_SOUND_FRONTEND(-1, "PICKUP_DEFAULT", "HUD_FRONTEND_STANDARD_PICKUPS_SOUNDSET")
		SET_MISSION_CLIENT_BIT(eMISSIONCLIENTBITSET_DONE_FIRST_PICKUP_AUDIO)
	ENDIF
ENDPROC

PROC TUG_MISSION_ENTITY_ONDELIVERY(INT iMissionEntity, BOOL bDeliverer)
	UNUSED_PARAMETER(iMissionEntity)
	
	IF bDeliverer
		MISSION_TICKER_DATA sTickerData
		sTickerData.iInt1 = ENUM_TO_INT(eTB_TICKER_DELIVER)
		SEND_FM_CONTENT_TICKER_EVENT_WITH_DATA(eFMCTICKER_CUSTOM, sTickerData)
	ENDIF
ENDPROC

FUNC PICKUP_TYPE TUG_MISSION_ENTITY_PICKUP_TYPE(INT iMissionEntity)
	UNUSED_PARAMETER(iMissionEntity)
	RETURN PICKUP_PORTABLE_CRATE_UNFIXED_INCAR_SMALL
ENDFUNC

FUNC BOOL TUG_MISSION_ENTITY_ATTACH_TO_PART(INT iMissionEntity, PARTICIPANT_INDEX partId)
	UNUSED_PARAMETER(iMissionEntity)
	RETURN IS_PROP_CLIENT_BIT_SET(GET_TAG_ID(eTB_TAG_GOODS), partId, ePROPCLIENTBITSET_PLAYER_HAS_BEEN_NEAR)
ENDFUNC

//////////////////////////
///   PED
//////////////////////////

FUNC BOOL TUG_PED_ACTIVATE(INT iPed)
	IF iPed = GET_TAG_ID(eTB_TAG_RESPAWN_1)
	OR iPed = GET_TAG_ID(eTB_TAG_RESPAWN_2)
	OR iPed = GET_TAG_ID(eTB_TAG_RESPAWN_3)
		RETURN TUG_SHOULD_ACTIVATE_AMBUSH()
	ENDIF
	RETURN TRUE
ENDFUNC

PROC TUG_PED_ONDAMAGED(INT iPed, STRUCT_ENTITY_DAMAGE_EVENT sEvent, PLAYER_INDEX damagerId)
	UNUSED_PARAMETER(iPed)
	IF sEvent.VictimDestroyed
	AND damagerId = LOCAL_PLAYER_INDEX
		SET_MISSION_CLIENT_BIT(eMISSIONCLIENTBITSET_KILLED_PED)
	ENDIF
ENDPROC

//////////////////////////
///   PED
//////////////////////////

FUNC BOOL TUG_AMBUSH_ACTIVATE()
	RETURN HAS_ANY_MISSION_ENTITY_BEEN_COLLECTED_FOR_THE_FIRST_TIME()
ENDFUNC

//////////////////////////
///   TICKER
//////////////////////////

FUNC STRING TUG_GET_TICKER_STRING_LOCAL(MISSION_TICKER_DATA &sTickerData)
	UNUSED_PARAMETER(sTickerData)
	SWITCH INT_TO_ENUM(TUG_TICKER, sTickerData.iInt1)
		CASE eTB_TICKER_COLLECT			RETURN "NCS_TIC_LCOL"
		CASE eTB_TICKER_DELIVER			RETURN "NCS_TIC_LDEL"
	ENDSWITCH

	RETURN ""
ENDFUNC

FUNC STRING TUG_GET_TICKER_STRING_REMOTE(MISSION_TICKER_DATA &sTickerData)
	UNUSED_PARAMETER(sTickerData)
	SWITCH INT_TO_ENUM(TUG_TICKER, sTickerData.iInt1)
		CASE eTB_TICKER_COLLECT			RETURN "NCS_TIC_RCOL"
		CASE eTB_TICKER_DELIVER			RETURN "NCS_TIC_RDEL"
	ENDSWITCH
	RETURN ""
ENDFUNC

//////////////////////////
///    VEHICLES
//////////////////////////

PROC TUG_VEHICLE_ATTRIBUTES(INT iVehicle, VEHICLE_INDEX vehId)
	IF iVehicle = GET_TAG_ID(eTB_TAG_TUGBOAT)
		SET_FORCE_LOW_LOD_ANCHOR_MODE(vehID, FALSE)
		SET_BOAT_LOW_LOD_ANCHOR_DISTANCE(vehID, 100.0)
		
		SET_VEHICLE_STRONG(vehId, TRUE)
		SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE(vehId, FALSE)
	ENDIF
ENDPROC

FUNC BOOL TUG_VEHICLE_MODS(INT iVehicle, VEHICLE_SETUP_STRUCT_MP& sData)
	SWITCH data.Vehicle.Vehicles[iVehicle].model
		CASE VIRGO2
			sData.VehicleSetup.eModel = VIRGO2
			sData.VehicleSetup.iColour1 = 88
			sData.VehicleSetup.iColour2 = 0
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 3
			sData.VehicleSetup.iModIndex[MOD_ENGINE] = 2
			sData.VehicleSetup.iModIndex[MOD_BRAKES] = 2
			sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 2
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 30
			sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 4
			sData.VehicleSetup.iModIndex[MOD_PLAQUE] = 18
			sData.VehicleSetup.iModIndex[MOD_TRUNK] = 1
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 4
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 4
			sData.VehicleSetup.iModIndex[MOD_CHASSIS3] = 1
			sData.VehicleSetup.iModIndex[MOD_CHASSIS4] = 1
			sData.VehicleSetup.iModIndex[MOD_CHASSIS5] = 1
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 3
			RETURN TRUE
		BREAK
		CASE SABREGT2
			sData.VehicleSetup.eModel = SABREGT2
			sData.VehicleSetup.iColour1 = 88
			sData.VehicleSetup.iColour2 = 88
			sData.VehicleSetup.iColourExtra1 = 88
			sData.VehicleSetup.iColourExtra2 = 88
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 2
			sData.VehicleSetup.iWheelType = 1
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 3
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 2
			sData.VehicleSetup.iModIndex[MOD_ENGINE] = 2
			sData.VehicleSetup.iModIndex[MOD_BRAKES] = 2
			sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 2
			sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 1
			sData.VehicleSetup.iModIndex[MOD_TRUNK] = 1
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 4
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 3
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 3
			RETURN TRUE
		BREAK
		CASE FACTION2
			sData.VehicleSetup.eModel = FACTION2
			sData.VehicleSetup.iColour1 = 0
			sData.VehicleSetup.iColour2 = 0
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 89
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 3
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_1)
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 1
			sData.VehicleSetup.iModIndex[MOD_ENGINE] = 2
			sData.VehicleSetup.iModIndex[MOD_BRAKES] = 2
			sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 2
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 2
			sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 4
			sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 6
			sData.VehicleSetup.iModIndex[MOD_STEERING] = 1
			sData.VehicleSetup.iModIndex[MOD_ICE] = 1
			sData.VehicleSetup.iModIndex[MOD_TRUNK] = 1
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 4
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 1
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 2
			RETURN TRUE
		BREAK
		CASE RUMPO
			sData.VehicleSetup.eModel = RUMPO // RUMPO
			sData.VehicleSetup.tlPlateText = "40ILF800"
			sData.VehicleSetup.iColour1 = 19
			sData.VehicleSetup.iColour2 = 19
			sData.VehicleSetup.iColourExtra1 = 19
			sData.VehicleSetup.iColourExtra2 = 19
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iLivery = 1
			sData.VehicleSetup.iWheelType = 1
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			RETURN TRUE
		BREAK
		CASE WARRENER2
			sData.VehicleSetup.eModel = WARRENER2
			sData.VehicleSetup.iColour1 = 74
			sData.VehicleSetup.iColour2 = 112
			sData.VehicleSetup.iColourExtra1 = 67
			sData.VehicleSetup.iColourExtra2 = 122
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 1
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 5
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 5
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
			sData.VehicleSetup.iModIndex[MOD_ENGINE] = 3
			sData.VehicleSetup.iModIndex[MOD_BRAKES] = 3
			sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 2
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 18
			sData.VehicleSetup.iModIndex[MOD_DOOR_R] = 1
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 2
			RETURN TRUE
		BREAK
		CASE PHOENIX
			sData.VehicleSetup.eModel = PHOENIX
			sData.VehicleSetup.iPlateIndex = 2
			sData.VehicleSetup.iColour1 = 112
			sData.VehicleSetup.iColour2 = 74
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_10)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 1
			sData.VehicleSetup.iModIndex[MOD_ENGINE] = 2
			sData.VehicleSetup.iModIndex[MOD_BRAKES] = 2
			sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 2
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 22
			RETURN TRUE
		BREAK
		CASE PRIMO2
			sData.VehicleSetup.eModel = PRIMO2
			sData.VehicleSetup.iColour1 = 0
			sData.VehicleSetup.iColour2 = 50
			sData.VehicleSetup.iColourExtra1 = 53
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 3
			sData.VehicleSetup.iWheelType = 8
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 2
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 2
			sData.VehicleSetup.iModIndex[MOD_ENGINE] = 2
			sData.VehicleSetup.iModIndex[MOD_BRAKES] = 2
			sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 2
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 1
			sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 1
			sData.VehicleSetup.iModIndex[MOD_TRUNK] = 2
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 5
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 5
			sData.VehicleSetup.iModIndex[MOD_CHASSIS3] = 2
			sData.VehicleSetup.iModIndex[MOD_CHASSIS4] = 2
			RETURN TRUE
		BREAK
		CASE MANANA2
			sData.VehicleSetup.eModel = MANANA2
			sData.VehicleSetup.iPlateIndex = 2
			sData.VehicleSetup.iColour1 = 50
			sData.VehicleSetup.iColour2 = 0
			sData.VehicleSetup.iColourExtra1 = 53
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 8
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_0)
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 3
			sData.VehicleSetup.iModIndex[MOD_ENGINE] = 2
			sData.VehicleSetup.iModIndex[MOD_BRAKES] = 2
			sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 2
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 38
			sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 4
			sData.VehicleSetup.iModIndex[MOD_STEERING] = 8
			sData.VehicleSetup.iModIndex[MOD_TRUNK] = 1
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 4
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 3
			sData.VehicleSetup.iModIndex[MOD_CHASSIS5] = 1
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 1
			RETURN TRUE
		BREAK
		CASE VOODOO2
			sData.VehicleSetup.eModel = VOODOO2
			sData.VehicleSetup.iColour1 = 50
			sData.VehicleSetup.iColour2 = 89
			sData.VehicleSetup.iColourExtra1 = 53
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 9
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 2
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 3
			sData.VehicleSetup.iModIndex[MOD_ENGINE] = 2
			sData.VehicleSetup.iModIndex[MOD_BRAKES] = 2
			sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 2
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 13
			sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 5
			sData.VehicleSetup.iModIndex[MOD_INTERIOR2] = 6
			sData.VehicleSetup.iModIndex[MOD_TRUNK] = 1
			sData.VehicleSetup.iModIndex[MOD_HYDRO] = 5
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 2
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 3
			sData.VehicleSetup.iModIndex[MOD_CHASSIS3] = 1
			sData.VehicleSetup.iModIndex[MOD_CHASSIS4] = 8
			sData.VehicleSetup.iModIndex[MOD_CHASSIS5] = 2
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 1
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

//////////////////////////
///    INIT
//////////////////////////

FUNC BOOL TUG_INIT_SERVER()
	serverBD.iRandomModeInt1 = GET_RANDOM_INT_IN_RANGE(0, 2)
	RETURN TRUE
ENDFUNC

//////////////////////////
///    MAINTAIN
//////////////////////////

PROC TUG_PICKUP_PLACEMENT_DATA()
	data.WeaponPickup[cTUG_PICKUP_ARMOUR].eType = PICKUP_ARMOUR_STANDARD
	data.WeaponPickup[cTUG_PICKUP_ARMOUR].vCoord = GET_ENTITY_COORDS(NET_TO_ENT(serverBD.sProp[GET_TAG_ID(eTB_TAG_ARMOUR)].netID), FALSE)
	SET_BIT(data.WeaponPickup[cTUG_PICKUP_ARMOUR].iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_FIXED))
	SET_BIT(data.WeaponPickup[cTUG_PICKUP_ARMOUR].iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ON_OBJECT))
	SET_BIT(data.WeaponPickup[cTUG_PICKUP_ARMOUR].iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_LOCAL_ONLY))
	 
	data.WeaponPickup[cTUG_PICKUP_AMMO].eType = PICKUP_AMMO_BULLET_MP
	data.WeaponPickup[cTUG_PICKUP_AMMO].vCoord = GET_ENTITY_COORDS(NET_TO_ENT(serverBD.sProp[GET_TAG_ID(eTB_TAG_AMMO)].netID), FALSE) + <<0.0, 0.0, 0.5>>
	SET_BIT(data.WeaponPickup[cTUG_PICKUP_AMMO].iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_UPRIGHT))
	SET_BIT(data.WeaponPickup[cTUG_PICKUP_AMMO].iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ON_OBJECT))
	SET_BIT(data.WeaponPickup[cTUG_PICKUP_AMMO].iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_LOCAL_ONLY))

	data.WeaponPickup[cTUG_PICKUP_HEALTH].eType = PICKUP_HEALTH_STANDARD
	data.WeaponPickup[cTUG_PICKUP_HEALTH].vCoord = GET_ENTITY_COORDS(NET_TO_ENT(serverBD.sProp[GET_TAG_ID(eTB_TAG_HEALTH)].netID), FALSE) + <<0.0, 0.0, 0.5>>
	SET_BIT(data.WeaponPickup[cTUG_PICKUP_HEALTH].iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_UPRIGHT))
	SET_BIT(data.WeaponPickup[cTUG_PICKUP_HEALTH].iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ON_OBJECT))
	SET_BIT(data.WeaponPickup[cTUG_PICKUP_HEALTH].iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_LOCAL_ONLY))
ENDPROC

FUNC BOOL TUG_SHOULD_ATTACH_MISSION_ENTITY()
	IF NOT IS_MISSION_ENTITY_CLIENT_BIT_SET(0, LOCAL_PARTICIPANT_INDEX, eMISSIONENTITYCLIENTBITSET_ATTACHED_TO_PLAYER)
		IF IS_PROP_CLIENT_BIT_SET(GET_TAG_ID(eTB_TAG_GOODS), LOCAL_PARTICIPANT_INDEX, ePROPCLIENTBITSET_PLAYER_HAS_BEEN_NEAR)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC TUG_MAINTAIN_CLIENT()
	IF NOT IS_MISSION_LOCAL_BIT_SET(eMISSIONLOCALBITSET_UPDATED_MISSION_ENTITY_COORD)
	AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sProp[GET_TAG_ID(eTB_TAG_GOODS)].netID)
		data.MissionEntity.MissionEntities[0].vCoords = GET_ENTITY_COORDS(NET_TO_ENT(serverBD.sProp[GET_TAG_ID(eTB_TAG_GOODS)].netID), FALSE)
		SET_MISSION_LOCAL_BIT(eMISSIONLOCALBITSET_UPDATED_MISSION_ENTITY_COORD)
	ENDIF
	
	IF NOT IS_MISSION_LOCAL_BIT_SET(eMISSIONLOCALBITSET_UPDATED_WEAPON_PICKUP_COORDS)
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sProp[GET_TAG_ID(eTB_TAG_AMMO)].netID)
		AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sProp[GET_TAG_ID(eTB_TAG_HEALTH)].netID)
		AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sProp[GET_TAG_ID(eTB_TAG_ARMOUR)].netID)
			TUG_PICKUP_PLACEMENT_DATA()
			SET_MISSION_LOCAL_BIT(eMISSIONLOCALBITSET_UPDATED_WEAPON_PICKUP_COORDS)
		ENDIF
	ENDIF
ENDPROC

//////////////////////////
///    CUSTOM SPAWNS
//////////////////////////

FUNC BOOL TUG_CUSTOM_SPAWNS_ENABLE()
	RETURN IS_LOCAL_PLAYER_IN_RANGE_OF_MISSION()
ENDFUNC

//////////////////////////
///    MISSION RANGE
//////////////////////////

FUNC VECTOR TUG_MISSION_COORD()
	RETURN data.Vehicle.Vehicles[GET_TAG_ID(eTB_TAG_TUGBOAT)].vCoords
ENDFUNC

FUNC FLOAT TUG_MISSION_RANGE()
	RETURN 200.0
ENDFUNC

//////////////////////////
///   WEAPON PICKUPS
//////////////////////////

FUNC INT TUG_GET_PROP_FOR_PICKUP(INT iPickup)
	SWITCH iPickup
		CASE cTUG_PICKUP_ARMOUR			RETURN GET_TAG_ID(eTB_TAG_ARMOUR)
		CASE cTUG_PICKUP_AMMO			RETURN GET_TAG_ID(eTB_TAG_AMMO)
		CASE cTUG_PICKUP_HEALTH			RETURN GET_TAG_ID(eTB_TAG_HEALTH)
	ENDSWITCH
	RETURN -1
ENDFUNC

FUNC BOOL TUG_SHOULD_CREATE_PICKUP(INT iPickup)
	IF IS_MISSION_LOCAL_BIT_SET(eMISSIONLOCALBITSET_UPDATED_WEAPON_PICKUP_COORDS)
		INT iProp = TUG_GET_PROP_FOR_PICKUP(iPickup)
		IF iProp != (-1)
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sProp[iProp].netID)
			AND HAS_OBJECT_BEEN_BROKEN(NET_TO_OBJ(serverBD.sProp[iProp].netID))
				data.WeaponPickup[iPickup].vCoord = GET_ENTITY_COORDS(NET_TO_ENT(serverBD.sProp[iProp].netID), FALSE) + <<0.0, 0.0, 0.3>>
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

//////////////////////////
///    PLACEMENT DATA
//////////////////////////

PROC TUG_DATA_SETUP()
	data.Prop.AttachedProp[cTUG_ATTACH_BOXES].eParentType = ET_VEHICLE
	data.Prop.AttachedProp[cTUG_ATTACH_BOXES].iParentIndex = GET_TAG_ID(eTB_TAG_TUGBOAT)
	data.Prop.AttachedProp[cTUG_ATTACH_BOXES].iPropIndex = GET_TAG_ID(eTB_TAG_BOXES)
	data.Prop.AttachedProp[cTUG_ATTACH_BOXES].vOffset = <<0.0, -12.5, 0.89>>
	data.Prop.Props[GET_TAG_ID(eTB_TAG_BOXES)].vRotation = <<-9.0, 0.0, 0.0>>
	
	// FRONT
	data.Prop.AttachedProp[cTUG_ATTACH_AMMO].eParentType = ET_VEHICLE
	data.Prop.AttachedProp[cTUG_ATTACH_AMMO].iParentIndex = GET_TAG_ID(eTB_TAG_TUGBOAT)
	data.Prop.AttachedProp[cTUG_ATTACH_AMMO].iPropIndex = GET_TAG_ID(eTB_TAG_AMMO)
	data.Prop.AttachedProp[cTUG_ATTACH_AMMO].vOffset = <<0.0, 11.5, 1.25>>
	data.Prop.Props[GET_TAG_ID(eTB_TAG_AMMO)].vRotation = <<8.5, 0.0, 0.0>>
	
	// BACK
	data.Prop.AttachedProp[cTUG_ATTACH_HEALTH].eParentType = ET_VEHICLE
	data.Prop.AttachedProp[cTUG_ATTACH_HEALTH].iParentIndex = GET_TAG_ID(eTB_TAG_TUGBOAT)
	data.Prop.AttachedProp[cTUG_ATTACH_HEALTH].iPropIndex = GET_TAG_ID(eTB_TAG_HEALTH)
	data.Prop.AttachedProp[cTUG_ATTACH_HEALTH].vOffset = <<0.0, -9.3, 0.50>>
	data.Prop.Props[GET_TAG_ID(eTB_TAG_HEALTH)].vRotation = <<-5.0, 0.0, 0.0>>
	
	// WHEEL ROOM (?)
	data.Prop.AttachedProp[cTUG_ATTACH_GOODS].eParentType = ET_VEHICLE
	data.Prop.AttachedProp[cTUG_ATTACH_GOODS].iParentIndex = GET_TAG_ID(eTB_TAG_TUGBOAT)
	data.Prop.AttachedProp[cTUG_ATTACH_GOODS].iPropIndex = GET_TAG_ID(eTB_TAG_GOODS)
	data.Prop.AttachedProp[cTUG_ATTACH_GOODS].vOffset = <<0.0, 1.850, 3.38>>
	data.Prop.Props[GET_TAG_ID(eTB_TAG_GOODS)].vRotation = <<0.0, 0.0, 0.0>>
	
	data.Vehicle.Vehicles[GET_TAG_ID(eTB_TAG_TUGBOAT)].vCoords.z += 6.0 
	
	data.Reward.bShowCashWithShard = TRUE
	
	data.Ambush.iSpawnDelay = 0
	
	data.MissionEntity.iCount = 1
	data.MissionEntity.MissionEntities[0].model = data.Prop.Props[GET_TAG_ID(eTB_TAG_GOODS)].model
	SET_MISSION_ENTITY_DATA_BIT(0, eMISSIONENTITYDATABITSET_ATTACH_TO_PLAYER_ON_SPAWN)
	
	SET_PROP_DATA_BIT(GET_TAG_ID(eTB_TAG_GOODS), ePROPDATABITSET_CHECK_PLAYER_NEAR)
	SET_PROP_DATA_BIT(GET_TAG_ID(eTB_TAG_GOODS), ePROPDATABITSET_SPAWN_INVINCIBLE)
	
	SET_PROP_DATA_BIT(GET_TAG_ID(eTB_TAG_HEALTH), ePROPDATABITSET_DISABLE_CLIMBING_ON_PROP)
	SET_PROP_DATA_BIT(GET_TAG_ID(eTB_TAG_BOXES), ePROPDATABITSET_DISABLE_CLIMBING_ON_PROP)
	SET_PROP_DATA_BIT(GET_TAG_ID(eTB_TAG_AMMO), ePROPDATABITSET_DISABLE_CLIMBING_ON_PROP)
ENDPROC

//////////////////////////
///    PROPS
//////////////////////////

FUNC BOOL TUG_PROP_ACTIVATE(INT iProp)
	IF iProp = GET_TAG_ID(eTB_TAG_BOXES)
	OR iProp = GET_TAG_ID(eTB_TAG_AMMO)
	OR iProp = GET_TAG_ID(eTB_TAG_HEALTH)
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[GET_TAG_ID(eTB_TAG_TUGBOAT)].netId)
		AND NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBD.sVehicle[GET_TAG_ID(eTB_TAG_TUGBOAT)].netId))
		AND IS_BOAT_ANCHORED(NET_TO_VEH(serverBD.sVehicle[GET_TAG_ID(eTB_TAG_TUGBOAT)].netId))
			RETURN TRUE
		ENDIF
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

FUNC BOOL TUG_PROP_SHOULD_DELETE(INT iProp)
	IF iProp = GET_TAG_ID(eTB_TAG_GOODS)
		RETURN IS_PROP_BIT_SET(iProp, ePROPBITSET_PLAYER_HAS_BEEN_NEAR)
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL TUG_PROP_ATTACH_COLLISIONS(INT iProp)
	IF iProp = GET_TAG_ID(eTB_TAG_BOXES)
	OR iProp = GET_TAG_ID(eTB_TAG_AMMO)
	OR iProp = GET_TAG_ID(eTB_TAG_HEALTH)
	OR iProp = GET_TAG_ID(eTB_TAG_GOODS)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL TUG_PROP_DETACH_ON_DEATH(INT iProp)
	IF iProp = GET_TAG_ID(eTB_TAG_GOODS)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC FLOAT TUG_PROP_NEAR_RANGE(INT iProp)
	IF iProp = GET_TAG_ID(eTB_TAG_GOODS)
		RETURN 2.0
	ENDIF
	RETURN 10.0
ENDFUNC

FUNC BOOL TUG_PROP_NEAR_LOS(INT iProp)
	IF iProp = GET_TAG_ID(eTB_TAG_GOODS)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL TUG_PROP_BLIP_ENABLE(INT iProp)
	IF iProp = GET_TAG_ID(eTB_TAG_GOODS)
		RETURN TUG_GET_STATE() = eTB_STATE_STEAL
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC HUD_COLOURS TUG_PROP_BLIP_COLOUR(INT iProp)
	IF iProp = GET_TAG_ID(eTB_TAG_GOODS)
		RETURN HUD_COLOUR_GREEN
	ENDIF
	RETURN HUD_COLOUR_WHITE
ENDFUNC

FUNC BLIP_SPRITE TUG_PROP_BLIP_SPRITE(INT iProp)
	IF iProp = GET_TAG_ID(eTB_TAG_GOODS)
		RETURN RADAR_TRACE_CONTRABAND
	ENDIF
	RETURN RADAR_TRACE_INVALID
ENDFUNC

FUNC STRING TUG_PROP_BLIP_NAME(INT iProp)
	IF iProp = GET_TAG_ID(eTB_TAG_GOODS)
		RETURN GET_MISSION_ENTITY_OVERRIDE_BLIP_NAME()
	ENDIF
	RETURN ""
ENDFUNC

FUNC BOOL TUG_PROP_MARKER_ENABLE(INT iProp, ENTITY_INDEX propId)
	UNUSED_PARAMETER(propId)
	IF iProp = GET_TAG_ID(eTB_TAG_GOODS)
		RETURN TUG_GET_STATE() = eTB_STATE_STEAL
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL TUG_PROP_BLIP_FLASH_ON_CREATE(INT iProp)
	IF iProp = GET_TAG_ID(eTB_TAG_GOODS)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

//////////////////////////
///   DIALOGUE
//////////////////////////
    
FUNC STRING TUG_DIALOGUE_ROOT(INT iDialogue)
	IF TUG_DO_DIALOGUE_VARIATION()
		SWITCH INT_TO_ENUM(TUG_DIALOGUE, iDialogue)
			CASE eTB_DIALOGUE_OPENING				
				SWITCH GET_GOODS_TYPE()
					CASE BUSINESS_TYPE_CARGO				RETURN "SM2YO_STB_2A"
					CASE BUSINESS_TYPE_WEAPONS				RETURN "SM2YO_STB_3A"
					CASE BUSINESS_TYPE_COKE					RETURN "SM2YO_STB_4A"
					CASE BUSINESS_TYPE_METH					RETURN "SM2YO_STB_5A"
					CASE BUSINESS_TYPE_WEED					RETURN "SM2YO_STB_6A"
					CASE BUSINESS_TYPE_COUNTERFEIT_CASH		RETURN "SM2YO_STB_7A"
					CASE BUSINESS_TYPE_FORGED_DOCS			RETURN "SM2YO_STB_8A"
					DEFAULT									RETURN "SM2YO_STB_1A"
				ENDSWITCH
			BREAK
		ENDSWITCH
	ELSE
		SWITCH INT_TO_ENUM(TUG_DIALOGUE, iDialogue)
			CASE eTB_DIALOGUE_OPENING				
				SWITCH GET_GOODS_TYPE()
					CASE BUSINESS_TYPE_CARGO				RETURN "SM2YO_STB_2B"
					CASE BUSINESS_TYPE_WEAPONS				RETURN "SM2YO_STB_3B"
					CASE BUSINESS_TYPE_COKE					RETURN "SM2YO_STB_4B"
					CASE BUSINESS_TYPE_METH					RETURN "SM2YO_STB_5B"
					CASE BUSINESS_TYPE_WEED					RETURN "SM2YO_STB_6B"
					CASE BUSINESS_TYPE_COUNTERFEIT_CASH		RETURN "SM2YO_STB_7B"
					CASE BUSINESS_TYPE_FORGED_DOCS			RETURN "SM2YO_STB_8B"
					DEFAULT									RETURN "SM2YO_STB_1B"
				ENDSWITCH
			BREAK
		ENDSWITCH
	ENDIF
	
	RETURN ""
ENDFUNC

FUNC BOOL TUG_DIALOGUE_TRIGGER(INT iDialogue)
	SWITCH INT_TO_ENUM(TUG_DIALOGUE, iDialogue)
		CASE eTB_DIALOGUE_OPENING				RETURN TRUE
	ENDSWITCH
	RETURN FALSE
ENDFUNC

//////////////////////////
///   MUSIC EVENTS
//////////////////////////

FUNC STRING TUG_MUSIC_EVENT_STRINGS(INT iMusicEvent)
	SWITCH INT_TO_ENUM(TUG_MUSIC, iMusicEvent)
		CASE eTB_MUSIC_NCLUB_DELIVERING_START		RETURN "MPSUM2_NCLUB_DELIVERING_START"
		CASE eTB_MUSIC_NCLUB_SUSPENSE				RETURN "MPSUM2_NCLUB_SUSPENSE"	
		CASE eTB_MUSIC_NCLUB_GUNFIGHT				RETURN "MPSUM2_NCLUB_GUNFIGHT"	
		CASE eTB_MUSIC_NCLUB_MED_INTENSITY			RETURN "MPSUM2_NCLUB_MED_INTENSITY"	
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC INT TUG_MUSIC_EVENT(INT iCurrentEvent)
	SWITCH INT_TO_ENUM(TUG_MUSIC, iCurrentEvent)
		CASE eTB_MUSIC_INVALID
			IF IS_GENERIC_BIT_SET(eGENERICBITSET_OPENING_DIALOGUE_DONE)
				RETURN ENUM_TO_INT(eTB_MUSIC_NCLUB_DELIVERING_START)
			ENDIF
		BREAK
		
		CASE eTB_MUSIC_NCLUB_DELIVERING_START
			IF HAS_ANY_PED_GROUP_BEEN_TRIGGERED()
				RETURN ENUM_TO_INT(eTB_MUSIC_NCLUB_GUNFIGHT)
			ENDIF
			
			IF TUG_GET_CLIENT_STATE() > eTB_CLIENTSTATE_GO_TO
				RETURN ENUM_TO_INT(eTB_MUSIC_NCLUB_SUSPENSE)
			ENDIF
		BREAK
		
		CASE eTB_MUSIC_NCLUB_SUSPENSE
			IF HAS_ANY_PED_GROUP_BEEN_TRIGGERED()
				RETURN ENUM_TO_INT(eTB_MUSIC_NCLUB_GUNFIGHT)
			ENDIF
		BREAK
		
		CASE eTB_MUSIC_NCLUB_GUNFIGHT
			IF TUG_GET_STATE() >= eTB_STATE_DELIVER
				RETURN ENUM_TO_INT(eTB_MUSIC_NCLUB_MED_INTENSITY)
			ENDIF
		BREAK
		
		CASE eTB_MUSIC_NCLUB_MED_INTENSITY
			
		BREAK
	ENDSWITCH
	
	RETURN -1
ENDFUNC

FUNC FLOAT TUG_REWARD_MULTIPLIER(eREWARD_TYPE eType)
	RETURN GET_SUMMER_2022_FIRST_PAYOUT_BOOST_MULTIPLIER(sMission.iType, ENUM_TO_INT(GET_VARIATION()), eType)
ENDFUNC

PROC TUG_SCRIPT_CLEANUP()

	SET_SUMMER_2022_FIRST_PAYOUT_BOOST_COMPLETE(sMission.iType, ENUM_TO_INT(GET_VARIATION()))
	
ENDPROC

//////////////////////////
///   DEBUG
//////////////////////////

#IF IS_DEBUG_BUILD
PROC TUG_DEBUG_ON_JSKIP()
	SWITCH TUG_GET_STATE()
		CASE eTB_STATE_STEAL
			IF NOT IS_PROP_BIT_SET(GET_TAG_ID(eTB_TAG_GOODS), ePROPBITSET_PLAYER_HAS_BEEN_NEAR)
			AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sProp[GET_TAG_ID(eTB_TAG_GOODS)].netID)
				SET_ENTITY_COORDS(LOCAL_PED_INDEX, GET_ENTITY_COORDS(NET_TO_ENT(serverBD.sProp[GET_TAG_ID(eTB_TAG_GOODS)].netID), FALSE))
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC
#ENDIF

//////////////////////////
///    LOGIC
//////////////////////////

PROC TUG_SETUP_LOGIC()
	#IF IS_DEBUG_BUILD
	logic.Debug.OnJSkip						= &TUG_DEBUG_ON_JSKIP
	#ENDIF
	
	// Init
	logic.Init.Server						= &TUG_INIT_SERVER
	
	// Data
	logic.Data.Setup						= &TUG_DATA_SETUP
	
	// Maintain
	logic.Maintain.Client					= &TUG_MAINTAIN_CLIENT
	
	// State Machines
	logic.StateMachine.SetupStates 			= &TUG_STATE_SETUP
	logic.StateMachine.SetupClientStates 	= &TUG_STATE_SETUP_CLIENT
	
	// Vehicles
	logic.Vehicle.Mods						= &TUG_VEHICLE_MODS
	logic.Vehicle.Attributes				= &TUG_VEHICLE_ATTRIBUTES
	
	// Tickers
	logic.Hud.Tickers.CustomStringLocal 	= &TUG_GET_TICKER_STRING_LOCAL
	logic.Hud.Tickers.CustomStringRemote 	= &TUG_GET_TICKER_STRING_REMOTE
	
	// Mission Entity
	logic.MissionEntity.ShouldSpawn			= &TUG_MISSION_ENTITY_SHOULD_SPAWN
	logic.MissionEntity.OnCollect			= &TUG_MISSION_ENTITY_ONCOLLECT
	logic.MissionEntity.OnDelivery			= &TUG_MISSION_ENTITY_ONDELIVERY
	logic.MissionEntity.GetPickupType		= &TUG_MISSION_ENTITY_PICKUP_TYPE
	logic.MissionEntity.ShouldAttachToParticipant = &TUG_MISSION_ENTITY_ATTACH_TO_PART
	
	// Peds
	logic.Ped.Activate						= &TUG_PED_ACTIVATE
	logic.Ped.OnDamaged						= &TUG_PED_ONDAMAGED
	
	// Ambush
	logic.Ambush.ShouldActivate				= &TUG_AMBUSH_ACTIVATE
	
	// Props
	logic.Prop.Activate						= &TUG_PROP_ACTIVATE
	logic.Prop.ShouldDelete					= &TUG_PROP_SHOULD_DELETE
	logic.Prop.ShouldCleanup				= &TUG_PROP_SHOULD_DELETE
	logic.Prop.AttachCollisionsActive		= &TUG_PROP_ATTACH_COLLISIONS
	logic.Prop.DetachOnDeath				= &TUG_PROP_DETACH_ON_DEATH
	logic.Prop.NearRange					= &TUG_PROP_NEAR_RANGE
	logic.Prop.CheckLOSNear					= &TUG_PROP_NEAR_LOS
	logic.Prop.Blip.Enable					= &TUG_PROP_BLIP_ENABLE
	logic.Prop.Blip.Colour					= &TUG_PROP_BLIP_COLOUR
	logic.Prop.Blip.Sprite					= &TUG_PROP_BLIP_SPRITE
	logic.Prop.Blip.Name					= &TUG_PROP_BLIP_NAME
	logic.Prop.Blip.FlashOnCreation			= &TUG_PROP_BLIP_FLASH_ON_CREATE
	logic.Prop.Blip.DrawMarker					= &TUG_PROP_MARKER_ENABLE
		
	// Custom Spawns
	logic.Player.Spawning.Custom.Enable		= &TUG_CUSTOM_SPAWNS_ENABLE
	
	// Mission Range
	logic.Mission.Coord						= &TUG_MISSION_COORD
	logic.Mission.WithinRange				= &TUG_MISSION_RANGE
	
	// Weapon Pickups
	logic.WeaponPickup.ShouldCreate 		= &TUG_SHOULD_CREATE_PICKUP
	
	// Dialogue
	logic.Dialogue.CellphoneCharacter		= &SOURCE_DIALOGUE_PHONE_CHARACTER
	logic.Dialogue.Character 				= &SOURCE_DIALOGUE_CHARACTER
	logic.Dialogue.ShouldTrigger			= &TUG_DIALOGUE_TRIGGER
	logic.Dialogue.Root						= &TUG_DIALOGUE_ROOT
	logic.Dialogue.SpeakerID 				= &SOURCE_DIALOGUE_SPEAKER_ID
	logic.Dialogue.SubtitleGroup 			= &SOURCE_DIALOGUE_SUBTITLE_GROUP
	logic.Dialogue.UseHeadsetAudio			= &SOURCE_DIALOGUE_USE_HEADSET
	
	// Music
	logic.Audio.MusicTrigger.EventString 	= &TUG_MUSIC_EVENT_STRINGS
	logic.Audio.MusicTrigger.Event 			= &TUG_MUSIC_EVENT
	
	// Dropoff
	logic.Delivery.EnableDropOffBlipRoute	= &SOURCE_SHOULD_ENABLE_DROPOFF_GPS
	
	// Rewards
	logic.Reward.Multiplier					= &TUG_REWARD_MULTIPLIER
	
	// Cleanup
	logic.Cleanup.ScriptEnd					= &TUG_SCRIPT_CLEANUP
	
ENDPROC

