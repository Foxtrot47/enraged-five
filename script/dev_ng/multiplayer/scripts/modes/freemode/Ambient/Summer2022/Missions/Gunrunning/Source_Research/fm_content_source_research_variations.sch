USING "globals.sch"
USING "rage_builtins.sch"

CONST_INT ENABLE_CONTENT	FEATURE_DLC_1_2022

#IF ENABLE_CONTENT
USING "Variations/fm_content_source_research_juggernaut.sch"

/// PURPOSE: Use to set up the logic pointers for your mission
///    
PROC SETUP_MISSION_LOGIC()
	SWITCH GET_VARIATION()
		CASE SRV_JUGGERNAUT 		
			JUG_SETUP_MISSION_LOGIC()		
		BREAK

		DEFAULT
			#IF IS_DEBUG_BUILD
			SCRIPT_ASSERT("Please populate SETUP_MISSION_LOGIC in fm_content_source_research_variations.sch")
			#ENDIF
		BREAK
	ENDSWITCH
ENDPROC
#ENDIF
