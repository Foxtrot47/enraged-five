USING "globals.sch"
USING "rage_builtins.sch"

CONST_INT ENABLE_CONTENT	FEATURE_DLC_1_2022

#IF ENABLE_CONTENT
USING "Variations/fm_content_clubhouse_contracts_motherboard_robbery.sch"
USING "Variations/fm_content_clubhouse_contracts_rooftop_container.sch"

/// PURPOSE: Use to set up the logic pointers for your mission
///    
PROC SETUP_MISSION_LOGIC()
	SWITCH GET_VARIATION()
		CASE CCV_ROOFTOP_CONTAINER 		
			ROOFTC_SETUP_LOGIC()		
		BREAK

		CASE CCV_MOTHERBOARD_ROBBERY
			MR_SETUP_LOGIC()
		BREAK
		
		DEFAULT
			#IF IS_DEBUG_BUILD
			SCRIPT_ASSERT("Please populate SETUP_MISSION_LOGIC in fm_content_clubhouse_contracts_variations.sch")
			#ENDIF
		BREAK
	ENDSWITCH
ENDPROC
#ENDIF
