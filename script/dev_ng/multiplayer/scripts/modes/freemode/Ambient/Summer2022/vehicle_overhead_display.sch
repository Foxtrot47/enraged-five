USING "globals.sch"
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_path.sch"
USING "commands_event.sch"
USING "net_include.sch"
USING "net_events.sch"
USING "net_spawn.sch"
USING "net_ambience.sch"
USING "net_wait_zero.sch"
USING "net_collectables_pickups.sch"
USING "net_render_targets.sch"
USING "net_MP_Radio.sch"

STRUCT ABOVE_CAR_STATS_DISPLAY
	VECTOR vScale
	VECTOR vWorldSize
	VECTOR vRot
	INT iRotOrder
	INT iBitSet
	#IF IS_DEBUG_BUILD
	BOOL bDisplayStats
	#ENDIF
ENDSTRUCT

STRUCT PLAYER_NAMES_SCALEFORM_DATA
	INT iDrawNamesStage = 1
	VEHICLE_INDEX viVehicles[ciCAR_MEET_MAX_NUMBER_OF_PLAYER_NAMES_DISPLAYED]
	VEHICLE_INDEX viPreviousVehicles[ciCAR_MEET_MAX_NUMBER_OF_PLAYER_NAMES_DISPLAYED]
	VEHICLE_STATS_CURRENT_VEHICLE sVehStats[ciCAR_MEET_MAX_NUMBER_OF_PLAYER_NAMES_DISPLAYED]
	VECTOR vPositions[ciCAR_MEET_MAX_NUMBER_OF_PLAYER_NAMES_DISPLAYED]
	FLOAT fDisplayHeights[ciCAR_MEET_MAX_NUMBER_OF_PLAYER_NAMES_DISPLAYED]
	BOOL bShouldMovieBeUpdated[ciCAR_MEET_MAX_NUMBER_OF_PLAYER_NAMES_DISPLAYED]
	INT iNumNamesToDisplay = -1
	FLOAT fRefreshTimer = 0.0
	BOOL bNeedsToFreeResources = TRUE
	INT iNumberOfMoviesLoaded = 0
	SCALEFORM_INDEX playerNameMovies[ciCAR_MEET_MAX_NUMBER_OF_PLAYER_NAMES_DISPLAYED]	
ENDSTRUCT 

PROC GET_VEHICLE_STATS_FOR_CURRENT_VEHICLE_INDEX(VEHICLE_INDEX vehID,VEHICLE_STATS_CURRENT_VEHICLE& currentDetails)
	
	IF NOT IS_ENTITY_DEAD(vehID)
		INT iLoop
		
		//get the current users vehicle stats
		CONST_FLOAT VOLTIC_ACCELERATION_MULTIPLIER 2.0
		CONST_FLOAT TEZERACT_ACCELERATION_MULTIPLIER 2.6753	//(0.9198/0.3438)
		CONST_FLOAT CYCLE_MULTIPLIER 0.50			
		FLOAT fMultiplier = 1.0
		MODEL_NAMES vehModel =GET_ENTITY_MODEL(vehID)
		
		IF IS_VEHICLE_A_CYCLE(vehModel)
			 fMultiplier = cfCYCLE_MULTIPLIER
		ELSE
			 fMultiplier = 1.0
		ENDIF		
		
		currentDetails.fValues[VEHICLE_STAT_TOP_SPEED_INDEX] 		= (GET_VEHICLE_ESTIMATED_MAX_SPEED(vehID))
		currentDetails.fValues[VEHICLE_STAT_BRAKING_INDEX] 			= (GET_VEHICLE_MAX_BRAKING(vehID) * fMultiplier)
	
		
		FLOAT fTemp
				
		currentDetails.fValues[VEHICLE_STAT_ACCELERATION_INDEX] 	= (GET_VEHICLE_ACCELERATION(vehID) * fMultiplier)
		IF vehModel = VOLTIC
			currentDetails.fValues[VEHICLE_STAT_ACCELERATION_INDEX] = (GET_VEHICLE_ACCELERATION(vehID) * VOLTIC_ACCELERATION_MULTIPLIER)
		ENDIF
		IF vehModel = TEZERACT
			currentDetails.fValues[VEHICLE_STAT_ACCELERATION_INDEX] = (GET_VEHICLE_ACCELERATION(vehID) * TEZERACT_ACCELERATION_MULTIPLIER)
		ENDIF
		
		CONST_FLOAT cfJESTER3_TOP_SPEED_MULTIPLIER		0.989008363	//(0.8278/0.8370)
		CONST_FLOAT cfFREECRAWLER_TOP_SPEED_MULTIPLIER	0.978876156	//(0.7090/0.7243)
		CONST_FLOAT cfSWINGER_TOP_SPEED_MULTIPLIER		0.965055329	//(0.8285/0.8585)
		CONST_FLOAT cfMENACER_TOP_SPEED_MULTIPLIER		0.973046595	//(0.6787/0.6975)
		CONST_FLOAT cfSPEEDO4_TOP_SPEED_MULTIPLIER		0.942652330	//(0.6575/0.6975)
		
		IF vehModel = JESTER3
			currentDetails.fValues[VEHICLE_STAT_TOP_SPEED_INDEX] 	= (GET_VEHICLE_ESTIMATED_MAX_SPEED(vehID) * cfJESTER3_TOP_SPEED_MULTIPLIER)
		ENDIF
		IF vehModel = FREECRAWLER
			currentDetails.fValues[VEHICLE_STAT_TOP_SPEED_INDEX] 	= (GET_VEHICLE_ESTIMATED_MAX_SPEED(vehID) * cfFREECRAWLER_TOP_SPEED_MULTIPLIER)
		ENDIF
		IF vehModel = SWINGER
			currentDetails.fValues[VEHICLE_STAT_TOP_SPEED_INDEX] 	= (GET_VEHICLE_ESTIMATED_MAX_SPEED(vehID) * cfSWINGER_TOP_SPEED_MULTIPLIER)
		ENDIF
		IF vehModel = MENACER
			currentDetails.fValues[VEHICLE_STAT_TOP_SPEED_INDEX] 	= (GET_VEHICLE_ESTIMATED_MAX_SPEED(vehID) * cfMENACER_TOP_SPEED_MULTIPLIER)
		ENDIF
		IF vehModel = SPEEDO4
			currentDetails.fValues[VEHICLE_STAT_TOP_SPEED_INDEX] 	= (GET_VEHICLE_ESTIMATED_MAX_SPEED(vehID) * cfSPEEDO4_TOP_SPEED_MULTIPLIER)
		ENDIF
		
		IF IS_THIS_MODEL_A_HELI(vehModel)
		OR IS_THIS_MODEL_A_PLANE(vehModel)
			fTemp = (GET_FLYING_VEHICLE_MODEL_AGILITY(vehModel) * fMultiplier)
		ELIF IS_THIS_MODEL_A_BOAT(vehModel)
			fTemp = (GET_BOAT_VEHICLE_MODEL_AGILITY(vehModel) * fMultiplier)
		ELSE
			fTemp = (GET_VEHICLE_MAX_TRACTION(vehID) * fMultiplier)
		ENDIF
		currentDetails.fValues[VEHICLE_STAT_TRACTION_INDEX]		= fTemp
		
		PRINTLN("CURRENT VEHICLE: ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(vehModel))
		PRINTLN("currentDetails.fValues[ciVEHICLE_STAT_BOX_TOP_SPEED] = ", currentDetails.fValues[VEHICLE_STAT_TOP_SPEED_INDEX])
		PRINTLN("currentDetails.fValues[ciVEHICLE_STAT_BOX_ACCELERATION] = ", currentDetails.fValues[VEHICLE_STAT_ACCELERATION_INDEX])
		PRINTLN("currentDetails.fValues[ciVEHICLE_STAT_BOX_BREAKING] = ", currentDetails.fValues[VEHICLE_STAT_BRAKING_INDEX])
		PRINTLN("currentDetails.fValues[ciVEHICLE_STAT_BOX_TRACTION] = ", currentDetails.fValues[VEHICLE_STAT_TRACTION_INDEX])
		//apply reduction for mod space  url:bugstar:2331128 - Level 2 Engine modification for the T20 maximises the acceleration stat, making the proceeding levels unnecessary.
		IF vehModel = T20
			currentDetails.fValues[VEHICLE_STAT_ACCELERATION_INDEX] = currentDetails.fValues[VEHICLE_STAT_ACCELERATION_INDEX] - 0.05
		ELIF vehModel = VINDICATOR
			currentDetails.fValues[VEHICLE_STAT_ACCELERATION_INDEX] = currentDetails.fValues[VEHICLE_STAT_ACCELERATION_INDEX] - 0.02
		ENDIF
		#IF IS_DEBUG_BUILD
		REPEAT VEHICLE_STAT_NUM_STATS iLoop
			IF fVehicleStatReduction[iLoop] != 0
				currentDetails.fValues[iLoop] = currentDetails.fValues[iLoop] - fVehicleStatReduction[iLoop]
			ENDIF
		ENDREPEAT
		PRINTLN("REDUCED currentDetails.fValues[ciVEHICLE_STAT_BOX_TOP_SPEED] = ", currentDetails.fValues[VEHICLE_STAT_TOP_SPEED_INDEX])
		PRINTLN("REDUCEDcurrentDetails.fValues[ciVEHICLE_STAT_BOX_ACCELERATION] = ", currentDetails.fValues[VEHICLE_STAT_ACCELERATION_INDEX])
		PRINTLN("REDUCEDcurrentDetails.fValues[ciVEHICLE_STAT_BOX_BREAKING] = ", currentDetails.fValues[VEHICLE_STAT_BRAKING_INDEX])
		PRINTLN("REDUCEDcurrentDetails.fValues[ciVEHICLE_STAT_BOX_TRACTION] = ", currentDetails.fValues[VEHICLE_STAT_TRACTION_INDEX])
		#ENDIF
		
		INT iClass = GET_VEHICLE_STAT_GROUP_FOR_CLASS(GET_VEHICLE_CLASS(vehID))
		
		PRINTLN("Max values: ")
		PRINTLN("iClass = ", iClass)
		PRINTLN("vehicleStats.fMaxValues[ciVEHICLE_STAT_BOX_TOP_SPEED] = ", vehicleStats.fMaxValues[iclass][VEHICLE_STAT_TOP_SPEED_INDEX])
		PRINTLN("vehicleStats.fMaxValues[ciVEHICLE_STAT_BOX_ACCELERATION] = ", vehicleStats.fMaxValues[iclass][VEHICLE_STAT_ACCELERATION_INDEX])
		PRINTLN("vehicleStats.fMaxValues[ciVEHICLE_STAT_BOX_BREAKING] = ", vehicleStats.fMaxValues[iclass][VEHICLE_STAT_BRAKING_INDEX])
		PRINTLN("vehicleStats.fMaxValues[ciVEHICLE_STAT_BOX_TRACTION] = ", vehicleStats.fMaxValues[iclass][VEHICLE_STAT_TRACTION_INDEX])
		
		iLoop = 0
		FOR iLoop = 0 TO (VEHICLE_STAT_NUM_STATS- 1)
			currentDetails.fValues[iLoop] = ((currentDetails.fValues[iLoop]/vehicleStats.fMaxValues[iclass][iLoop])*100)
			PRINTLN("currentDetails.fValues[", iLoop, "] = ", currentDetails.fValues[iLoop])
			IF currentDetails.fValues[iLoop] > 100
				currentDetails.fValues[iLoop] = 100
				PRINTLN("Capping value at 100: currentDetails.fValues[", iLoop, "] = ", currentDetails.fValues[iLoop])
			ENDIF
		ENDFOR				
	ENDIF	
ENDPROC
PROC MAINTAIN_OLD_VEHICLE_LIST_ORDER(VEHICLE_INDEX &prevList[], VEHICLE_INDEX &newList[])
	INT i,j
	VEHICLE_INDEX viSwap
	REPEAT ciCAR_MEET_MAX_NUMBER_OF_PLAYER_NAMES_DISPLAYED i
		IF prevList[i] = newList[i]
			RELOOP
		ENDIF
		FOR j=i+1 TO (ciCAR_MEET_MAX_NUMBER_OF_PLAYER_NAMES_DISPLAYED-1)
			IF prevList[j] = newList[i]
				viSwap = newList[j]
				newList[j] = newList[i]
				newList[i] = viSwap
			ENDIF
		ENDFOR
	ENDREPEAT
ENDPROC

FUNC BOOL REQUEST_AND_LOAD_SCALEFORM_MOVIE(SCALEFORM_INDEX &movie, STRING name)
	
	IF NATIVE_TO_INT(movie) = 0
	PRINTLN("[REQUEST_LOAD_SCALEFORM_MOVIE] REQUESTING MOVIE: ", name)
	movie = REQUEST_SCALEFORM_MOVIE(name)	
	ENDIF
	
	RETURN HAS_SCALEFORM_MOVIE_LOADED(movie) 
	
ENDFUNC

PROC SET_PROMO_VEHICLE_WEBSITE(MODEL_NAMES model)
	
	//seems this model is doesn't have the website set
	IF model = OMNISEGT
	OR model = TORERO2
	OR model = CORSITA
	OR model = LM87
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("LEGEND_WEB")
		EXIT
	ELIF model = RUINER4
	ELIF model = VIGERO2
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("SOUTHERN_WEB")
		EXIT
	ELIF model = NERO2
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BENNY_WEB")
		EXIT
	ELIF model = BALLER	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")
		EXIT
	ENDIF
	
	IF NOT IS_MODEL_VALID(model)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")			
	ENDIF
		
	SITE_BUYABLE_VEHICLE eVeh =  GET_WEBSITE_BUYABLE_VEHICLE_FROM_MODEL(model)
	WEBSITE_INDEX_ENUM eWebsite =  GET_WEBSITE_INDEX_FROM_VEHICLE_ENUM(eVeh)
	
	SWITCH eWebsite
		CASE WWW_LEGENDARYMOTORSPORT_NET 	 			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("LEGEND_WEB")          BREAK
		CASE WWW_SOUTHERNSANANDREASSUPERAUTOS_COM		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("SOUTHERN_WEB")        BREAK
		CASE WWW_WARSTOCK_D_CACHE_D_AND_D_CARRY_COM		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("WARSTOCK_WEB")        BREAK
		CASE WWW_BENNYSORIGINALMOTORWORKS_COM			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BENNY_WEB")    	   BREAK
		DEFAULT
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")
		BREAK
	ENDSWITCH
	
ENDPROC

PROC ADJUST_Z_POSITION_FOR_MODEL(MODEL_NAMES eModel, FLOAT &PosZ)
	SWITCH eModel
		
		CASE BUCCANEER2		PosZ += 0.350			BREAK		
		CASE WARRENER2		PosZ += 0.300			BREAK
	#IF FEATURE_DLC_1_2022
		CASE JESTER			PosZ += 0.095			BREAK						
		CASE ITALIGTO		PosZ -= 0.100			BREAK
		CASE NOVAK			PosZ -= 0.450			BREAK
		CASE RUINER4		PosZ -= 0.150			BREAK
		CASE Casco			PosZ += 0.250			BREAK
		CASE BALLER 		PosZ -= 0.600			BREAK
		CASE BALLER2 		PosZ -= 0.600			BREAK
		CASE BALLER3 		PosZ -= 0.600			BREAK
		CASE BALLER4 		PosZ -= 0.600			BREAK
		CASE BALLER5 		PosZ -= 0.600			BREAK
		CASE BALLER6		PosZ -= 0.600			BREAK
		CASE BALLER7 		PosZ -= 0.600			BREAK
		CASE BANSHEE		PosZ -= 0.200			BREAK
		CASE BRIOSO2		PosZ -= 0.200			BREAK
		CASE BUFFALO4		PosZ -= 0.100			BREAK
		CASE COGNOSCENTI	PosZ -= 0.200			BREAK
		CASE COGNOSCENTI2	PosZ -= 0.150			BREAK
		CASE DUBSTA			PosZ -= 0.150			BREAK
		CASE DUBSTA2		PosZ -= 0.100			BREAK
		CASE DUKES3			PosZ -= 0.100			BREAK
		CASE DYNASTY		PosZ -= 0.300			BREAK	
		CASE FAGALOA		PosZ -= 0.100			BREAK
		CASE FELTZER3		PosZ += 0.250			BREAK	
		CASE GREENWOOD		PosZ -= 0.150			BREAK	
		CASE HELLION		PosZ -= 0.350			BREAK	
		CASE HERMES			PosZ += 0.150			BREAK
		CASE IMPALER		PosZ += 0.100			BREAK
		CASE KANJO			PosZ -= 0.450			BREAK
		CASE LANDSTALKER2	PosZ -= 0.550			BREAK
		CASE MESA			PosZ -= 0.050			BREAK
		CASE MICHELLI		PosZ -= 0.300			BREAK
		CASE NEBULA			PosZ -= 0.200			BREAK
		CASE OMNIS			PosZ -= 0.450			BREAK
		CASE PENUMBRA2		PosZ -= 0.350			BREAK
		CASE PEYOTE2		PosZ -= 0.100			BREAK
		CASE PICADOR		PosZ -= 0.250			BREAK
		CASE REMUS			PosZ -= 0.350			BREAK
		CASE SCHWARZER		PosZ += 0.200			BREAK
		CASE SEMINOLE2		PosZ -= 0.150			BREAK
		CASE SENTINEL		PosZ += 0.100			BREAK
		CASE SPEEDO			PosZ -= 0.450			BREAK
		CASE SULTAN2		PosZ -= 0.150			BREAK
		CASE TORNADO2		PosZ -= 0.250			BREAK
		CASE VETO2			PosZ += 0.150			BREAK
		CASE CORSITA		PosZ -= 0.300			BREAK
		CASE IMORGON		PosZ -= 0.300			BREAK
		CASE JESTER4		PosZ += 0.150			BREAK
		CASE LM87			PosZ += 0.150			BREAK
		CASE NERO			PosZ -= 0.100			BREAK
		CASE OMNISEGT		PosZ -= 0.450			BREAK
		CASE OPENWHEEL1		PosZ += 0.250			BREAK
		CASE PARIAH			PosZ -= 0.050			BREAK
		CASE PFISTER811		PosZ += 0.150			BREAK
		CASE S80			PosZ += 0.250			BREAK
		CASE TEZERACT		PosZ += 0.100			BREAK
		CASE TURISMOR		PosZ += 0.250			BREAK
		CASE ZENTORNO		PosZ -= 0.150			BREAK
		CASE GAUNTLET4		PosZ += 0.100			BREAK
		CASE TURISMO2		PosZ -= 0.200			BREAK
		CASE COMET2			PosZ -= 0.100			BREAK
		CASE TAILGATER2		PosZ += 0.100			BREAK
		CASE PATRIOT3		PosZ -= 0.600			BREAK
	#ENDIF
						
	ENDSWITCH
ENDPROC

PROC SHOW_VEHICLE_OWNERS_AND_CREW_TAGS(PLAYER_NAMES_SCALEFORM_DATA &namesDisplayData,VEHICLE_INDEX &viVehicles[], VEHICLE_INDEX &viPreviousVehicles[], VEHICLE_STATS_CURRENT_VEHICLE &sVehStats[], VECTOR &vPositions[], FLOAT &fDisplayHeights[], SCALEFORM_INDEX &playerNameMovies[], ABOVE_CAR_STATS_DISPLAY& aboveCarStatsDisplay, BOOL alternativeCondition)
	IF IS_PLAYER_SWITCH_IN_PROGRESS()
		EXIT
	ENDIF

	IF GET_DPADDOWN_ACTIVATION_STATE() != DPADDOWN_NONE		
	OR alternativeCondition
		VECTOR vPosition
		INT iIndex
		
		
		REQUEST_STREAMED_TEXTURE_DICT("MPCarHUD")
		REQUEST_STREAMED_TEXTURE_DICT("MPCarHUD2")
		REQUEST_STREAMED_TEXTURE_DICT("MPCarHUD3")
		REQUEST_STREAMED_TEXTURE_DICT("MPCarHUD4")
		IF HAS_STREAMED_TEXTURE_DICT_LOADED("MPCarHUD")
		AND HAS_STREAMED_TEXTURE_DICT_LOADED("MPCarHUD2")
		AND HAS_STREAMED_TEXTURE_DICT_LOADED("MPCarHUD3")
		AND HAS_STREAMED_TEXTURE_DICT_LOADED("MPCarHUD4")		
		AND namesDisplayData.fRefreshTimer<=0
			namesDisplayData.fRefreshTimer = ciCAR_MEET_MILISECONDS_BETWEEN_VEHICLE_LIST_REFRESH/1000.0
			
			GET_PED_NEARBY_VEHICLES(PLAYER_PED_ID(), viVehicles)	
			MAINTAIN_OLD_VEHICLE_LIST_ORDER(viPreviousVehicles, viVehicles)
			TEXT_LABEL_31 sName	
			TEXT_LABEL_23 makeTxt, txtDict	
			PLAYER_INDEX piPlayer
			
			REPEAT ciCAR_MEET_MAX_NUMBER_OF_PLAYER_NAMES_DISPLAYED iIndex	
				IF viPreviousVehicles[iIndex] != viVehicles[iIndex]	
					namesDisplayData.bShouldMovieBeUpdated[iIndex] = TRUE														
					viPreviousVehicles[iIndex] = viVehicles[iIndex]
				ENDIF
				IF IS_ENTITY_ALIVE(viVehicles[iIndex])
				
					IF IS_VEHICLE_A_PERSONAL_VEHICLE(viVehicles[iIndex])					
						piPlayer = GET_OWNER_OF_PERSONAL_VEHICLE(viVehicles[iIndex])
						IF IS_NET_PLAYER_OK(piPlayer, TRUE, FALSE)
							IF IS_PLAYER_STARTED_TO_USE_PERSONAL_CAR_MOD(piPlayer)
								RELOOP
							ENDIF
						ENDIF
					ENDIF
					MODEL_NAMES eModel
					eModel = GET_ENTITY_MODEL(viVehicles[iIndex])	
					
					IF eModel = SLAMTRUCK
					AND NOT IS_VEHICLE_A_PERSONAL_VEHICLE(viVehicles[iIndex])
						RELOOP
					ENDIF

					vPosition = GET_ENTITY_COORDS(viVehicles[iIndex])
					IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(viVehicles[iIndex], FALSE)
					AND (VDIST(GET_PLAYER_COORDS(PLAYER_ID()), vPosition) <= cfCAR_MEET_MAX_VEHICLE_DATA_RENDER_DISTANCE )	
					
						TEXT_LABEL_15 tl63Temp
						tl63Temp = "MP_CAR_STATS_"
						IF iIndex < 9
							tl63Temp +="0"
						ENDIF
						tl63Temp += (iIndex + 1)				
						ADJUST_Z_POSITION_FOR_MODEL(eModel, vPosition.z)
						
						IF REQUEST_AND_LOAD_SCALEFORM_MOVIE(playerNameMovies[iIndex], tl63Temp)
						AND namesDisplayData.bShouldMovieBeUpdated[iIndex]
						
							vPositions[iIndex] = vPosition														
							fDisplayHeights[iIndex] = GET_MODEL_HEIGHT(eModel)
							
							PRINTLN("[SHOW_VEHICLE_OWNERS_AND_CREW_TAGS]  MOVIE: ", tl63Temp, " IS LOADED AND ABOUT TO BE UPDATED!")
							
							sName= ""
							makeTxt = "" 
							txtDict	= ""
							GET_VEHICLE_TEXTURE_DICTIONARY_AND_MAKE_TEXTURE(viVehicles[iIndex], makeTxt, txtDict)
							
							
													
							sName = GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(eModel)						
							
							GET_VEHICLE_STATS_FOR_CURRENT_VEHICLE_INDEX(viVehicles[iIndex], sVehStats[iIndex])
							BEGIN_SCALEFORM_MOVIE_METHOD(playerNameMovies[iIndex], "SET_VEHICLE_INFOR_AND_STATS")															
								BEGIN_TEXT_COMMAND_SCALEFORM_STRING("FM_TWO_STRINGS")
									ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(GET_MAKE_FROM_MODEL(eModel, TRUE))
									ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(sName)
								END_TEXT_COMMAND_SCALEFORM_STRING()
								
								IF NOT IS_VEHICLE_A_PERSONAL_VEHICLE(viVehicles[iIndex])									
									IF IS_VEHICLE_AVAILABLE_PAST_CURRENT_TIME(eModel)
										SET_PROMO_VEHICLE_WEBSITE(eModel)
									ELSE
										SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CMRC_COMINGSOON")
									ENDIF	
								ELSE
									SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")			
								ENDIF
								
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME_STRING(txtDict)
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME_STRING(makeTxt)
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("FMMC_VEHST_0")
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("FMMC_VEHST_1")
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("FMMC_VEHST_2")
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("FMMC_VEHST_3")
								
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ROUND(sVehStats[iIndex].fValues[VEHICLE_STAT_TOP_SPEED_INDEX]))
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ROUND(sVehStats[iIndex].fValues[VEHICLE_STAT_ACCELERATION_INDEX]))
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ROUND(sVehStats[iIndex].fValues[VEHICLE_STAT_BRAKING_INDEX]))
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ROUND(sVehStats[iIndex].fValues[VEHICLE_STAT_TRACTION_INDEX]))
							END_SCALEFORM_MOVIE_METHOD()
							
							IF IS_VEHICLE_A_PERSONAL_VEHICLE(viVehicles[iIndex])							
								IF IS_NET_PLAYER_OK(piPlayer, TRUE, FALSE)									
									TEXT_LABEL_15 sCrewTag																						
									sCrewTag = ""								
									sName = GET_PLAYER_NAME(piPlayer)
									GET_PLAYER_CREW_TAG_FOR_SCALEFORM(piPlayer,sCrewTag)																					
																
									
									BEGIN_SCALEFORM_MOVIE_METHOD(playerNameMovies[iIndex],"SET_PLAYER_NAME")
										SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(sName)
									END_SCALEFORM_MOVIE_METHOD()
									
									IF NOT IS_STRING_EMPTY(sCrewTag)
										BEGIN_SCALEFORM_MOVIE_METHOD(playerNameMovies[iIndex],"SET_CREW_NAME")
											SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(sCrewTag)
										END_SCALEFORM_MOVIE_METHOD()
									ENDIF	
								ENDIF
							ENDIF
							namesDisplayData.bShouldMovieBeUpdated[iIndex] = FALSE
									
						ENDIF	
					ENDIF
					viPreviousVehicles[iIndex] = viVehicles[iIndex]
				ENDIF							
		
			ENDREPEAT	
			
			
			 
		ENDIF		
		
		//DRAW EVERY FRAME							
		REPEAT ciCAR_MEET_MAX_NUMBER_OF_PLAYER_NAMES_DISPLAYED iIndex
			IF HAS_SCALEFORM_MOVIE_LOADED(playerNameMovies[iIndex])
				vPosition = vPositions[iIndex]
				vPosition.z += fDisplayHeights[iIndex] + 1.3
				aboveCarStatsDisplay.vRot = GET_MISSION_LOCATE_ROTATION_VECTOR(vPosition, GET_FINAL_RENDERED_CAM_COORD())
				PRINTLN("[AM_MP_PROPERTY] VEHICLE STATS SCALEFORM SHOULD BE DISPLAYED")
				IF VDIST(GET_PLAYER_COORDS(PLAYER_ID()), vPosition) <= cfCAR_MEET_MAX_VEHICLE_DATA_RENDER_DISTANCE
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE) AND VDIST(GET_PLAYER_COORDS(PLAYER_ID()), vPosition) < cfCAR_MEET_MIN_VEHICLE_DATA_RENDER_DISTANCE
						RELOOP
					ENDIF		
					
				
				DRAW_SCALEFORM_MOVIE_3D_SOLID(playerNameMovies[iIndex],
											  vPosition, 
											  aboveCarStatsDisplay.vRot, 
											  aboveCarStatsDisplay.vScale, 
											  aboveCarStatsDisplay.vWorldSize, 
											  INT_TO_ENUM(EULER_ROT_ORDER,aboveCarStatsDisplay.iRotOrder))			
				ENDIF
			ENDIF
		ENDREPEAT
		
		//countdown until next vehicle list refresh
		namesDisplayData.fRefreshTimer -= GET_FRAME_TIME()					
		
	ELSE 
		//DPAD RELEASED
		
			INT iIndex
			namesDisplayData.fRefreshTimer = 0 // TO AVOID DELAYS IF IT'S NOT THE FIRST TIME CHECKING THE NAMES
			REPEAT ciCAR_MEET_MAX_NUMBER_OF_PLAYER_NAMES_DISPLAYED iIndex	
				namesDisplayData.bShouldMovieBeUpdated[iIndex] = TRUE
				IF HAS_SCALEFORM_MOVIE_LOADED(playerNameMovies[iIndex])
					SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(playerNameMovies[iIndex])
				ENDIF
			ENDREPEAT
			namesDisplayData.iNumberOfMoviesLoaded = 0
			namesDisplayData.bNeedsToFreeResources=FALSE
		
	ENDIF
ENDPROC

PROC INIT_ABOVE_CAR_DISPLAY_STATS(ABOVE_CAR_STATS_DISPLAY& aboveCarStatsDisplay)
	FLOAT iScale = 0.70
	aboveCarStatsDisplay.vScale 	= <<1, 1, 1>>
	aboveCarStatsDisplay.vWorldSize = <<(10.0 * 0.8) * iScale, (6.0 * 0.8) * iScale, 1.0 * iScale>>
	aboveCarStatsDisplay.vRot 		= <<0, 0, 0>>
	aboveCarStatsDisplay.iRotOrder 	= ENUM_TO_INT(EULER_XYZ)
ENDPROC

PROC MAINTAIN_DISPLAY_NAMES_AND_CREWS(PLAYER_NAMES_SCALEFORM_DATA &namesDisplayData,ABOVE_CAR_STATS_DISPLAY& aboveCarStatsDisplay, BOOL alternativeCondition = FALSE)
	SHOW_VEHICLE_OWNERS_AND_CREW_TAGS(namesDisplayData,
									  namesDisplayData.viVehicles,
									  namesDisplayData.viPreviousVehicles,
									  namesDisplayData.sVehStats,
									  namesDisplayData.vPositions,
									  namesDisplayData.fDisplayHeights,
									  namesDisplayData.playerNameMovies,
									  aboveCarStatsDisplay,
									  alternativeCondition)																			
ENDPROC
