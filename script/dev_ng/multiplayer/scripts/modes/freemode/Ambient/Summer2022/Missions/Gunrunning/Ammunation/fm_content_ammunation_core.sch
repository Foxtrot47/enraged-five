USING "globals.sch"
USING "rage_builtins.sch"

CONST_INT ENABLE_CONTENT	FEATURE_DLC_1_2022

#IF ENABLE_CONTENT
USING "fm_content_ammunation_structs.sch"
USING "fm_content_gb_mission_structs.sch"
USING "fm_content_generic_structs.sch"

//////////////////////////
///    CONST
//////////////////////////

CONST_INT cAD_TOTAL_CALL_VARIATIONS		3

CONST_INT cAD_TIMER_DUNE_WARP_DELAY		2000

//////////////////////////
///    ENUMS
//////////////////////////

ENUM eAMMUDEL_STATE
	eAD_STATE_COLLECT,
	eAD_STATE_DELIVER,
	
	eAD_STATE_END
ENDENUM

ENUM eAMMUDEL_CLIENT_STATE
	eAD_CLIENTSTATE_COLLECT,
	eAD_CLIENTSTATE_DELIVER,
	eAD_CLIENTSTATE_HELP_DELIVER,
	eAD_CLIENTSTATE_RECOVER,
	
	eAD_CLIENTSTATE_END
ENDENUM

ENUM eAMMUDEL_TAG
	eAMMUDEL_TAG_DUNELOADER,
	eAMMUDEL_TAG_SUPPLIES
ENDENUM

ENUM eAMMUDEL_AMBUSH_TYPE
	eAMMUDEL_AMBUSHTYPE_NONE = -2,
	eAMMUDEL_AMBUSHTYPE_INVALID = -1,
	
	eAMMUDEL_AMBUSHTYPE_LOST = 0,
	eAMMUDEL_AMBUSHTYPE_TRIADS,
	eAMMUDEL_AMBUSHTYPE_MERRYWEATHER,
	
	eAMMUDEL_AMBUSHTYPE_MAX
ENDENUM

ENUM AMMUDEL_TICKER
	eAD_TICKER_COLLECT,
	eAD_TICKER_DELIVER
ENDENUM

ENUM AMMUDEL_HELP_TEXT
	eAMMUDEL_HELP_START
ENDENUM

ENUM AMMUDEL_DIALOGUE
	eAMMUDEL_DIALOGUE_START
ENDENUM

ENUM eAMMUDEL_MUSIC
	eAMMUDEL_MUSIC_NONE = -1,
	
	eAMMUDEL_MUSIC_DELIVER_START,
	eAMMUDEL_MUSIC_VEHICLE_ACTION,
	eAMMUDEL_MUSIC_DELIVERING
ENDENUM

//----------------------
//	LOCAL VARIABLES
//----------------------

SCRIPT_TIMER stPlayerDuneWarp
eAMMUDEL_AMBUSH_TYPE eAmbushType = eAMMUDEL_AMBUSHTYPE_INVALID

//////////////////////////
///    HELPERS
//////////////////////////

FUNC GUNRUNNING_AMMUNATION_VARIATION GET_VARIATION()
	RETURN serverBD.sMissionData.eMissionVariation
ENDFUNC

FUNC GUNRUNNING_AMMUNATION_SUBVARIATION GET_SUBVARIATION()
	RETURN serverBD.sMissionData.eMissionSubvariation
ENDFUNC

FUNC BOOL AMMUDEL_HAS_AMBUSH()
	RETURN (eAmbushType > eAMMUDEL_AMBUSHTYPE_INVALID)
ENDFUNC

FUNC eAMMUDEL_AMBUSH_TYPE AMMUDEL_GET_AMBUSH_TYPE()
	RETURN eAmbushType
ENDFUNC

FUNC eAMMUDEL_STATE AMMUDEL_GET_MODE_STATE()
	RETURN INT_TO_ENUM(eAMMUDEL_STATE, GET_MODE_STATE_ID())
ENDFUNC

FUNC eAMMUDEL_CLIENT_STATE AMMUDEL_GET_CLIENT_MODE_STATE()
	RETURN INT_TO_ENUM(eAMMUDEL_CLIENT_STATE, GET_CLIENT_MODE_STATE_ID())
ENDFUNC

//////////////////////////
///    OVERRIDES
//////////////////////////

FUNC XPCATEGORY GET_DEFAULT_RP_CATEGORY()
	RETURN XPCATEGORY_FM_CONTENT_SUM22_AMMUNATION_DELIVERY
ENDFUNC

#IF IS_DEBUG_BUILD
FUNC STRING GET_VARIATION_NAME_FOR_DEBUG()
	RETURN GUNRUNNING_AMMUNATION_GET_VARIATION_NAME_FOR_DEBUG(GET_VARIATION())
ENDFUNC

FUNC STRING GET_SUBVARIATION_NAME_FOR_DEBUG()
	RETURN GUNRUNNING_AMMUNATION_GET_SUBVARIATION_NAME_FOR_DEBUG(GET_SUBVARIATION())
ENDFUNC
#ENDIF

FUNC BOOL SETUP_VARIATION(INT iVariation, INT iSubvariation)
	serverBD.sMissionData.eMissionVariation = INT_TO_ENUM(GUNRUNNING_AMMUNATION_VARIATION, iVariation)
	serverBD.sMissionData.eMissionSubvariation = INT_TO_ENUM(GUNRUNNING_AMMUNATION_SUBVARIATION, iSubvariation)
	
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] SETUP_VARIATION - Variation: ", GET_VARIATION_NAME_FOR_DEBUG())
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] SETUP_VARIATION - Subvariation: ", GET_SUBVARIATION_NAME_FOR_DEBUG())
	
	RETURN TRUE
ENDFUNC

PROC PROCESS_GB_COMMON_END_TELEMETRY(BOOL bIWon, BOOL bForcedEndOfMode)
	
	INT iEndReason = GET_END_REASON_AS_TELEMETRY_TYPE(GET_END_REASON(), bIWon, bForcedEndOfMode)
	sTelemetry.iGbTelemetryEndReason = iEndReason
	g_sGUNRUN_Telemetry_data.m_Location = ENUM_TO_INT(serverBD.sMissionData.eMissionSubvariation)
	
	GB_SET_COMMON_TELEMETRY_DATA_ON_END(bIWon, iEndReason)
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [TELEMETRY] - PROCESS_GB_COMMON_END_TELEMETRY - Called GB_SET_COMMON_TELEMETRY_DATA_ON_END(", GET_STRING_FROM_BOOL(bIWon),", ", iEndReason, ")")
ENDPROC

FUNC STRING GET_MISSION_PLACEMENT_DATA_FILENAME()
	RETURN GUNRUNNING_AMMUNATION_GET_MISSION_PLACEMENT_DATA_FILENAME(GET_VARIATION(), GET_SUBVARIATION())
ENDFUNC

FUNC BLIP_SPRITE GET_MISSION_ENTITY_OVERRIDE_SPRITE()
	RETURN GET_FM_CONTENT_ENTITY_BLIP_SPRITE(LOCAL_PLAYER_INDEX)
ENDFUNC

PROC COMMON_ASSET_LOAD(BOOL bLoad)
	IF bLoad
		REQUEST_SCRIPT_AUDIO_BANK("DLC_MPSUM2/MPSUM2_Generic")
	ELSE
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_MPSUM2/MPSUM2_Generic")
	ENDIF
ENDPROC

PROC MISSION_CLEANUP()
	IF GB_GET_LOCAL_PLAYER_MISSION_HOST() = LOCAL_PLAYER_INDEX
		IF GET_END_REASON() = eENDREASON_MISSION_PASSED
			CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBunkerDuneloaderBS, BUNKER_DUNELOADER_BS_SPAWN_CRATE)
			
			SET_MP_INT_CHARACTER_STAT(MP_STAT_BUNKER_CRATE_COOLDOWN, GET_CLOUD_TIME_AS_INT())
		ENDIF
		
		CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBunkerDuneloaderBS, BUNKER_DUNELOADER_BS_RUNNING_MISSION)
	ENDIF
ENDPROC

//////////////////////////
///    BIG MESSAGE  
//////////////////////////

FUNC STRING GET_START_BIG_MESSAGE_TITLE()
	RETURN "AND_BM_S_TITLE"	// DELIVER SUPPLIES
ENDFUNC

FUNC STRING GET_START_BIG_MESSAGE_STRAPLINE()
	RETURN "AND_BM_S_STRAP"	// Deliver the supplies to Ammu-Nation
ENDFUNC

PROC DISPLAY_START_BIG_MESSAGE()
	SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_START_OF_JOB, GET_START_BIG_MESSAGE_TITLE(), GET_START_BIG_MESSAGE_STRAPLINE())
ENDPROC

FUNC STRING GET_END_BIG_MESSAGE_TITLE(BOOL bPass)
	IF bPass
		RETURN "AND_BM_P_TITLE"	// DELIVERED
	ENDIF

	RETURN "AND_BM_F_TITLE"		// MISSION OVER
ENDFUNC

FUNC STRING GET_END_BIG_MESSAGE_STRAPLINE(BOOL bPass)
	IF bPass
		RETURN "AND_BM_P_STRAP"		// The supplies were delivered
	ENDIF
	
	RETURN "AND_BM_F_STRAP"		// ~a~ were not delivered
ENDFUNC

PROC DISPLAY_END_BIG_MESSAGE()
	IF HAS_PLAYER_GANG_DELIVERED_ANY_MISSION_ENTITIES(LOCAL_PLAYER_INDEX)		// The supplies were delivered
		SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_BUY_END_OF_JOB_SUCCESS, GET_END_BIG_MESSAGE_TITLE(TRUE), GET_END_BIG_MESSAGE_STRAPLINE(TRUE))
	ELSE																		// The supplies were not delivered
		SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_BUY_END_OF_JOB_SUCCESS, GET_END_BIG_MESSAGE_TITLE(FALSE), GET_END_BIG_MESSAGE_STRAPLINE(FALSE))
	ENDIF
ENDPROC

//////////////////////////
///    REWARDS  
//////////////////////////

PROC PROCESS_REWARD_EARN(INT iCashToGive)
	IF USE_SERVER_TRANSACTIONS()
		INT iScriptTransactionIndex
		TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_AMBIENT_JOB_AMMUNATION_DELIVERY, iCashToGive, iScriptTransactionIndex, FALSE, TRUE)
		g_cashTransactionData[iScriptTransactionIndex].eExtraData.tl31ContenID = "fm_content_ammunation"
		PRINTLN("[",scriptName,"][GIVE_REWARD] - TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_AMBIENT_JOB_AMMUNATION_DELIVERY, ", iCashToGive, ", iScriptTransactionIndex, FALSE, TRUE)")
	ELSE	
		AMBIENT_JOB_DATA tempData
		NETWORK_EARN_FROM_AMBIENT_JOB(iCashToGive, "fm_content_ammunation", tempData)
		PRINTLN("[",scriptName,"][GIVE_REWARD] - NETWORK_EARN_FROM_AMBIENT_JOB(", iCashToGive, ", \"fm_content_ammunation\", tempData)")
	ENDIF
ENDPROC

FUNC INT GET_DEFAULT_WINNER_CASH_REWARD()
	IF GB_GET_LOCAL_PLAYER_MISSION_HOST() = LOCAL_PLAYER_INDEX
		RETURN g_sMPTunables.iAMMUNATION_DELIVERY_CASH_REWARD
	ENDIF
	
	RETURN 0
ENDFUNC

FUNC INT GET_DEFAULT_WINNER_RP_REWARD()
	IF GB_GET_LOCAL_PLAYER_MISSION_HOST() = LOCAL_PLAYER_INDEX
		RETURN g_sMPTunables.iAMMUNATION_DELIVERY_RP_REWARD
	ENDIF
	
	RETURN 0
ENDFUNC

FUNC INT GET_DEFAULT_MIN_PARTICIPATION_CASH()
	IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG(FALSE)
		RETURN g_sMPTunables.iAMMUNATION_DELIVERY_PARTICIPATION_CASH
	ENDIF
	RETURN 0
ENDFUNC

FUNC INT GET_DEFAULT_MIN_PARTICIPATION_RP()
	RETURN g_sMPTunables.iAMMUNATION_DELIVERY_PARTICIPATION_RP
ENDFUNC

FUNC INT GET_DEFAULT_MIN_PARTICIPATION_TIME_CAP()
	RETURN g_sMPTunables.iAMMUNATION_DELIVERY_PARTICIPATION_TIME_CAP
ENDFUNC

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CORE FILE STUFF ABOVE - MISSION LOGIC BELOW
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////
///    MISSION ENTITY
//////////////////////////

FUNC STRING AMMUDEL_MISSION_ENTITY_BLIP_NAME(INT iMissionEntity)
	UNUSED_PARAMETER(iMissionEntity)
	RETURN "AND_BLP_DUNE"
ENDFUNC

PROC AMMUDEL_MISSION_ENTITY_ONCOLLECT(INT iMissionEntity)
	IF NOT IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_DELIVERED)
	AND NOT IS_MISSION_ENTITY_CLIENT_BIT_SET(iMissionEntity, LOCAL_PARTICIPANT_INDEX, eMISSIONENTITYCLIENTBITSET_DELIVERED)
		MISSION_TICKER_DATA sTickerData
		sTickerData.iInt1 = ENUM_TO_INT(eAD_TICKER_COLLECT)
		SEND_FM_CONTENT_TICKER_EVENT_WITH_DATA(eFMCTICKER_CUSTOM, sTickerData)
	ENDIF
ENDPROC

PROC AMMUDEL_MISSION_ENTITY_ONDELIVERY(INT iMissionEntity, BOOL bDeliverer)
	UNUSED_PARAMETER(iMissionEntity)
	
	IF IS_LOCAL_HOST
		SET_PROP_DATA_BIT(GET_TAG_ID(eAMMUDEL_TAG_SUPPLIES), ePROPDATABITSET_DELETE_ON_END)
	ENDIF
	
	IF bDeliverer
		MISSION_TICKER_DATA sTickerData
		sTickerData.iInt1 = ENUM_TO_INT(eAD_TICKER_DELIVER)
		SEND_FM_CONTENT_TICKER_EVENT_WITH_DATA(eFMCTICKER_CUSTOM, sTickerData)
	ENDIF
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sProp[GET_TAG_ID(eAMMUDEL_TAG_SUPPLIES)].netID)
		IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sProp[GET_TAG_ID(eAMMUDEL_TAG_SUPPLIES)].netID)
			DETACH_ENTITY(NET_TO_ENT(serverBD.sProp[GET_TAG_ID(eAMMUDEL_TAG_SUPPLIES)].netID))
			DELETE_NET_ID(serverBD.sProp[GET_TAG_ID(eAMMUDEL_TAG_SUPPLIES)].netID)
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] AMMUDEL_ON_DELIVERY - Deleted attached prop.")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL AMMUDEL_MISSION_ENTITY_SHOULD_SPAWN(INT iMissionEntity)
	RETURN GET_VEHICLE_STATE(GET_MISSION_ENTITY_CARRIER(iMissionEntity)) = eVEHICLESTATE_DRIVEABLE
ENDFUNC

FUNC BOOL AMMUDEL_MISSION_ENTITY_SHOULD_BE_COLLECTABLE(INT iMissionEntity)
	RETURN IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_ATTACHED_IF_REQUIRED)
ENDFUNC

//////////////////////////
///   TICKER
//////////////////////////

FUNC STRING AMMUDEL_TICKER_STRING_LOCAL(MISSION_TICKER_DATA &sTickerData)
	UNUSED_PARAMETER(sTickerData)
	SWITCH INT_TO_ENUM(AMMUDEL_TICKER, sTickerData.iInt1)
		CASE eAD_TICKER_COLLECT			RETURN "AND_TIC_LCOL"
		CASE eAD_TICKER_DELIVER			RETURN "AND_TIC_LDEL"
	ENDSWITCH

	RETURN ""
ENDFUNC

FUNC STRING AMMUDEL_TICKER_STRING_REMOTE(MISSION_TICKER_DATA &sTickerData)
	UNUSED_PARAMETER(sTickerData)
	SWITCH INT_TO_ENUM(AMMUDEL_TICKER, sTickerData.iInt1)
		CASE eAD_TICKER_COLLECT			RETURN "AND_TIC_RCOL"
		CASE eAD_TICKER_DELIVER			RETURN "AND_TIC_RDEL"
	ENDSWITCH
	RETURN ""
ENDFUNC

//////////////////////////
///   VEHICLES
//////////////////////////

FUNC BOOL AMMUDEL_VEHICLE_MODS(INT iVehicle, VEHICLE_SETUP_STRUCT_MP& sData)
	IF iVehicle = GET_TAG_ID(eAMMUDEL_TAG_DUNELOADER)
		sData.VehicleSetup.eModel = DLOADER
		sData.VehicleSetup.tlPlateText = "29KXR232"
		sData.VehicleSetup.iPlateIndex = 1
		sData.VehicleSetup.iColour1 = 14
		sData.VehicleSetup.iColour2 = 0
		sData.VehicleSetup.iColourExtra1 = 4
		sData.VehicleSetup.iColourExtra2 = 156
		sData.iColour5 = 1
		sData.iColour6 = 132
		sData.iLivery2 = 0
		sData.VehicleSetup.iWheelType = 4
		sData.VehicleSetup.iTyreR = 255
		sData.VehicleSetup.iTyreG = 255
		sData.VehicleSetup.iTyreB = 255
		sData.VehicleSetup.iNeonR = 255
		sData.VehicleSetup.iNeonB = 255
		SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_9)
		SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_BULLETPROOF_TYRES)
		RETURN TRUE
	ENDIF
	
	SWITCH data.Vehicle.Vehicles[iVehicle].model
		CASE GBURRITO
			sData.VehicleSetup.eModel = GBURRITO // GBURRITO
			sData.VehicleSetup.tlPlateText = "06SAD065"
			sData.VehicleSetup.iColour1 = 13
			sData.VehicleSetup.iColour2 = 3
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 1
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_9)
			RETURN TRUE
		BREAK	
		
		CASE DAEMON
			sData.VehicleSetup.eModel = DAEMON // DAEMON
			sData.VehicleSetup.tlPlateText = "40WPD594"
			sData.VehicleSetup.iPlateIndex = 3
			sData.VehicleSetup.iColour1 = 111
			sData.VehicleSetup.iColour2 = 111
			sData.VehicleSetup.iColourExtra1 = 3
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 6
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			RETURN TRUE
		BREAK
		
		CASE HEXER
			sData.VehicleSetup.eModel = HEXER // HEXER
			sData.VehicleSetup.tlPlateText = "43OCW417"
			sData.VehicleSetup.iColour1 = 1
			sData.VehicleSetup.iColour2 = 1
			sData.VehicleSetup.iColourExtra1 = 2
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 6
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			RETURN TRUE
		BREAK
		
		CASE DUBSTA2
			IF iVehicle = data.Ambush.Vehicle[0].iIndex
				sData.VehicleSetup.eModel = Dubsta2 // DUBSTA
				sData.VehicleSetup.tlPlateText = "40KMH734"
				sData.VehicleSetup.iColour1 = 27
				sData.VehicleSetup.iColour2 = 12
				sData.VehicleSetup.iColourExtra1 = 36
				sData.VehicleSetup.iColourExtra2 = 156
				sData.iColour5 = 1
				sData.iColour6 = 132
				sData.iLivery2 = 0
				sData.VehicleSetup.iWheelType = 3
				sData.VehicleSetup.iTyreR = 255
				sData.VehicleSetup.iTyreG = 255
				sData.VehicleSetup.iTyreB = 255
				sData.VehicleSetup.iNeonR = 255
				sData.VehicleSetup.iNeonB = 255
				SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_10)
				RETURN TRUE
			ELSE
				sData.VehicleSetup.eModel = Dubsta2 // DUBSTA
				sData.VehicleSetup.tlPlateText = "12MMC876"
				sData.VehicleSetup.iColour1 = 112
				sData.VehicleSetup.iColour2 = 12
				sData.VehicleSetup.iColourExtra1 = 0
				sData.VehicleSetup.iColourExtra2 = 156
				sData.iColour5 = 1
				sData.iColour6 = 132
				sData.iLivery2 = 0
				sData.VehicleSetup.iWheelType = 3
				sData.VehicleSetup.iTyreR = 255
				sData.VehicleSetup.iTyreG = 255
				sData.VehicleSetup.iTyreB = 255
				sData.VehicleSetup.iNeonR = 255
				sData.VehicleSetup.iNeonB = 255
				SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_10)
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE MESA3
			sData.VehicleSetup.eModel = MESA3 // MESA
			sData.VehicleSetup.tlPlateText = "06WDS796"
			sData.VehicleSetup.iColour1 = 12
			sData.VehicleSetup.iColour2 = 12
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 12
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 4
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_1)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_9)
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

/////////////////////////
///   HELP TEXT
//////////////////////////

FUNC BOOL AMMUDEL_HELP_TEXT_TRIGGER(INT iHelp)
	SWITCH INT_TO_ENUM(AMMUDEL_HELP_TEXT, iHelp)
		CASE eAMMUDEL_HELP_START			RETURN TRUE
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC STRING AMMUDEL_HELP_TEXT_LABEL(INT iHelp)
	SWITCH INT_TO_ENUM(AMMUDEL_HELP_TEXT, iHelp)
		CASE eAMMUDEL_HELP_START		RETURN "AND_HT_START"
	ENDSWITCH
	RETURN ""
ENDFUNC

//////////////////////////
///    INIT
//////////////////////////

FUNC BOOL AMMUDEL_INIT_CLIENT()
	
	IF GB_GET_LOCAL_PLAYER_MISSION_HOST() != LOCAL_PLAYER_INDEX
		RETURN TRUE
	ENDIF
	
	IF NOT IS_MISSION_LOCAL_BIT_SET(eMISSIONLOCALBITSET_WARPED_INTO_VEHICLE)
		
		VEHICLE_INDEX vehID = GET_VEHICLE_INDEX_FROM_TAG(eAMMUDEL_TAG_DUNELOADER)
	
		IF DOES_ENTITY_EXIST(vehID)
		AND IS_VEHICLE_DRIVEABLE(vehID)
		AND NOT IS_PED_IN_VEHICLE(LOCAL_PED_INDEX, vehID)
			TASK_ENTER_VEHICLE(LOCAL_PED_INDEX, vehId, DEFAULT, VS_DRIVER, DEFAULT, ECF_WARP_PED)
			SET_MISSION_LOCAL_BIT(eMISSIONLOCALBITSET_WARPED_INTO_VEHICLE)
		ENDIF
		
	ELSE
		
		IF NOT IS_LOCAL_PLAYER_IN_ANY_SIMPLE_INTERIOR()
		AND NOT IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
			IF IS_SCREEN_FADED_OUT()
				IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(stPlayerDuneWarp, cAD_TIMER_DUNE_WARP_DELAY) 
					FADE_SCREEN_IN()
				ELSE
					RETURN FALSE
				ENDIF
			ENDIF
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			RETURN TRUE
		ENDIF
		
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

//////////////////////////
///    PROPS
//////////////////////////



//////////////////////////
///    AMBUSH
//////////////////////////

FUNC BOOL AMMUDEL_AMBUSH_ACTIVATE()
	RETURN AMMUDEL_HAS_AMBUSH() AND (sMissionEntityLocal.sMissionEntity[0].fDistanceFromSpawn > 500.0)
ENDFUNC

//////////////////////////
///    DIALOGUE  
//////////////////////////

FUNC enumCharacterList AMMUDEL_DIALOGUE_PHONE_CHARACTER(INT iDialogue)
	UNUSED_PARAMETER(iDialogue)
	RETURN CHAR_MP_AGENT_14
ENDFUNC

FUNC STRING AMMUDEL_DIALOGUE_CHARACTER(INT iDialogue, INT iSpeaker)
	UNUSED_PARAMETER(iDialogue)
	UNUSED_PARAMETER(iSpeaker)
	RETURN "AGENT14"
ENDFUNC

FUNC INT AMMUDEL_DIALOGUE_SPEAKER_ID(INT iDialogue, INT iSpeaker)
	UNUSED_PARAMETER(iDialogue)
	UNUSED_PARAMETER(iSpeaker)
	RETURN 2
ENDFUNC

FUNC STRING AMMUDEL_DIALOGUE_SUBTITLE_GROUP(INT iDialogue)
	UNUSED_PARAMETER(iDialogue)
	RETURN "SM2AGAU"
ENDFUNC

FUNC BOOL AMMUDEL_DIALOGUE_USE_HEADSET(INT iDialogue)
	UNUSED_PARAMETER(iDialogue)
	RETURN FALSE
ENDFUNC

FUNC STRING AMMUDEL_DIALOGUE_ROOT(INT iDialogue)
	SWITCH INT_TO_ENUM(AMMUDEL_DIALOGUE, iDialogue)
		CASE eAMMUDEL_DIALOGUE_START				
			SWITCH GET_SEEDED_INT_IN_RANGE(cAD_TOTAL_CALL_VARIATIONS, 1)
				CASE 0				RETURN "SM2AG_ANC_1A"
				CASE 1				RETURN "SM2AG_ANC_1B"
				CASE 2				RETURN "SM2AG_ANC_1C"
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC BOOL AMMUDEL_DIALOGUE_TRIGGER(INT iDialogue)
	SWITCH INT_TO_ENUM(AMMUDEL_DIALOGUE, iDialogue)
		CASE eAMMUDEL_DIALOGUE_START			RETURN TRUE	
	ENDSWITCH
	RETURN FALSE
ENDFUNC

//////////////////////////
///    MUSIC  
//////////////////////////

FUNC STRING GET_MISSION_MUSIC_STOP_STRING()
	RETURN "MPSUM2_MUSIC_STOP"
ENDFUNC

FUNC STRING GET_MISSION_MUSIC_FAIL_STRING()
	RETURN "MPSUM2_FAIL"
ENDFUNC

FUNC STRING AMMUDEL_MUSIC_EVENT_STRINGS(INT iMusicEvent)
	SWITCH INT_TO_ENUM(eAMMUDEL_MUSIC, iMusicEvent)
		CASE eAMMUDEL_MUSIC_DELIVER_START			RETURN "MPSUM2_GUNRUN_SUSPENSE_START"
		CASE eAMMUDEL_MUSIC_VEHICLE_ACTION			RETURN "MPSUM2_GUNRUN_MED_INTENSITY"
		CASE eAMMUDEL_MUSIC_DELIVERING				RETURN "MPSUM2_GUNRUN_DELIVERING"
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC INT AMMUDEL_MUSIC_EVENT(INT iCurrentEvent)
	SWITCH INT_TO_ENUM(eAMMUDEL_MUSIC, iCurrentEvent)
		CASE eAMMUDEL_MUSIC_NONE
			RETURN ENUM_TO_INT(eAMMUDEL_MUSIC_DELIVER_START)
		BREAK
		
		CASE eAMMUDEL_MUSIC_DELIVER_START
			IF AMMUDEL_HAS_AMBUSH()
				IF HAS_AMBUSH_ACTIVATED()
					RETURN ENUM_TO_INT(eAMMUDEL_MUSIC_VEHICLE_ACTION)
				ENDIF
			ELSE
				IF sMissionEntityLocal.sMissionEntity[0].fDistanceFromSpawn > 500.0
					RETURN ENUM_TO_INT(eAMMUDEL_MUSIC_DELIVERING)
				ENDIF
			ENDIF
		BREAK
		
		CASE eAMMUDEL_MUSIC_VEHICLE_ACTION
			IF IS_AMBUSH_OVER()
				RETURN ENUM_TO_INT(eAMMUDEL_MUSIC_DELIVERING)
			ENDIF
		BREAK
		
	ENDSWITCH
	
	RETURN -1
ENDFUNC

FUNC FM_CONTENT_PM_CALL_BITSET AMMUDEL_POST_MISSION_TEXT()

	IF NOT IS_GENERIC_BIT_SET(eGENERICBITSET_I_WON)
	OR GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG(FALSE)
		RETURN eFM_CONTENT_PM_CALL_INVALID
	ENDIF
	
	IF GET_SEEDED_INT_IN_RANGE(2) = 0
		RETURN eFM_CONTENT_PM_TXTMSG_GUNRUNNING_AMMUNATION_DELIVERY_0
	ENDIF
	
	RETURN eFM_CONTENT_PM_TXTMSG_GUNRUNNING_AMMUNATION_DELIVERY_1
ENDFUNC

FUNC FLOAT AMMUDEL_REWARD_MULTIPLIER(eREWARD_TYPE eType)
	RETURN GET_SUMMER_2022_FIRST_PAYOUT_BOOST_MULTIPLIER(sMission.iType, ENUM_TO_INT(GET_VARIATION()), eType)
ENDFUNC

PROC AMMUDEL_SCRIPT_CLEANUP()

	SET_SUMMER_2022_FIRST_PAYOUT_BOOST_COMPLETE(sMission.iType, ENUM_TO_INT(GET_VARIATION()))
	
ENDPROC

//////////////////////////
///    OBJECTIVE TEXT
//////////////////////////

PROC AMMUDEL_OBJ_COLLECT()
	Print_Objective_Text("AND_OBJ_ENTER")
ENDPROC

PROC AMMUDEL_OBJ_DELIVER()
	Print_Objective_Text("AND_OBJ_DEL")
ENDPROC

PROC AMMUDEL_OBJ_HELP_DELIVER()
	Print_Objective_Text("AND_OBJ_HDEL")
ENDPROC

//////////////////////////
///    MODE STATES
//////////////////////////

PROC AMMUDEL_SETUP_MODE_STATES()
	ADD_MODE_STATE(eAD_STATE_COLLECT, eMODESTATE_COLLECT_MISSION_ENTITY)
		ADD_MODE_STATE_TRANSITION_DEFAULT(eAD_STATE_COLLECT, eAD_STATE_DELIVER)
		
	ADD_MODE_STATE(eAD_STATE_DELIVER, eMODESTATE_DELIVER_MISSION_ENTITY)
		ADD_MODE_STATE_TRANSITION_DEFAULT(eAD_STATE_DELIVER, eAD_STATE_END)
		
	ADD_MODE_STATE(eAD_STATE_END, eMODESTATE_END)
ENDPROC

PROC AMMUDEL_SETUP_CLIENT_MODE_STATES()
	ADD_CLIENT_MODE_STATES_DELIVERY(eAD_CLIENTSTATE_COLLECT, eAD_CLIENTSTATE_DELIVER, eAD_CLIENTSTATE_HELP_DELIVER, eAD_CLIENTSTATE_RECOVER, eAD_CLIENTSTATE_END,
									&AMMUDEL_OBJ_COLLECT,  &AMMUDEL_OBJ_DELIVER,  &AMMUDEL_OBJ_HELP_DELIVER,  &EMPTY)
									
	ADD_CLIENT_MODE_STATE(eAD_CLIENTSTATE_END, eMODESTATE_END, &EMPTY)
ENDPROC

//////////////////////////
///    PLACEMENT DATA
//////////////////////////

FUNC BOOL AMMUDEL_SHOULD_SELECT_AMBUSH()

	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_ForceChanceAmbushOn")
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] AMMUDEL_SETUP_AMBUSH_DATA - sc_ForceChanceAmbushOn in on, forcing ambush")
		RETURN TRUE
	ENDIF
	#ENDIF
	
	INT iSeeded = GET_SEEDED_INT_IN_RANGE(100)
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] AMMUDEL_SETUP_AMBUSH_DATA - iSeeded = (",iSeeded ,"/", g_sMPTunables.iAMMUNATION_DELIVERY_AMBUSH_CHANCE, ")")
	
	RETURN iSeeded <= g_sMPTunables.iAMMUNATION_DELIVERY_AMBUSH_CHANCE
	
ENDFUNC

PROC AMMUDEL_SETUP_AMBUSH_DATA()

	IF AMMUDEL_SHOULD_SELECT_AMBUSH()
		eAmbushType = INT_TO_ENUM(eAMMUDEL_AMBUSH_TYPE, GET_SEEDED_INT_IN_RANGE(ENUM_TO_INT(eAMMUDEL_AMBUSHTYPE_MAX)))
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] AMMUDEL_SETUP_AMBUSH_DATA - Ambush is on. Ambush type: ", eAmbushType)
	ELSE
		eAmbushType = eAMMUDEL_AMBUSHTYPE_NONE
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] AMMUDEL_SETUP_AMBUSH_DATA - Ambush is off.")
	ENDIF

	SWITCH AMMUDEL_GET_AMBUSH_TYPE()
		CASE eAMMUDEL_AMBUSHTYPE_LOST
			data.Ped.Peds[0].model = G_M_Y_Lost_01
			data.Ped.Peds[0].weapon = WEAPONTYPE_PISTOL
			
			data.Ped.Peds[1].model = G_F_Y_Lost_01
			data.Ped.Peds[1].weapon = WEAPONTYPE_MICROSMG
			
			data.Ped.Peds[2].model = G_M_Y_Lost_01
			data.Ped.Peds[2].weapon = WEAPONTYPE_SAWNOFFSHOTGUN
			
			data.Ped.Peds[3].model = G_F_Y_Lost_01
			data.Ped.Peds[3].weapon = WEAPONTYPE_MICROSMG
			
			data.Vehicle.Vehicles[data.Ambush.Vehicle[0].iIndex].model = GBURRITO
			data.Vehicle.Vehicles[data.Ambush.Vehicle[1].iIndex].model = GBURRITO
		BREAK
		
		CASE eAMMUDEL_AMBUSHTYPE_TRIADS
			data.Ped.Peds[0].model = G_M_M_ChiGoon_02
			data.Ped.Peds[0].weapon = WEAPONTYPE_PISTOL
			
			data.Ped.Peds[1].model = G_M_M_ChiGoon_01
			data.Ped.Peds[1].weapon = WEAPONTYPE_MICROSMG
			
			data.Ped.Peds[2].model = G_M_M_ChiGoon_02
			data.Ped.Peds[2].weapon = WEAPONTYPE_PISTOL
			
			data.Ped.Peds[3].model = G_M_M_ChiGoon_01
			data.Ped.Peds[3].weapon = WEAPONTYPE_MICROSMG
			
			data.Vehicle.Vehicles[data.Ambush.Vehicle[0].iIndex].model = DUBSTA2
			data.Vehicle.Vehicles[data.Ambush.Vehicle[1].iIndex].model = DUBSTA2
		BREAK
		
		CASE eAMMUDEL_AMBUSHTYPE_MERRYWEATHER
			data.Ped.Peds[0].model = S_M_Y_BLACKOPS_01
			data.Ped.Peds[0].weapon = WEAPONTYPE_PISTOL
			
			data.Ped.Peds[1].model = S_M_Y_BLACKOPS_01
			data.Ped.Peds[1].weapon = WEAPONTYPE_MICROSMG
			
			data.Ped.Peds[2].model = S_M_Y_BLACKOPS_01
			data.Ped.Peds[2].weapon = WEAPONTYPE_PISTOL
			
			data.Ped.Peds[3].model = S_M_Y_BLACKOPS_01
			data.Ped.Peds[3].weapon = WEAPONTYPE_MICROSMG
			
			data.Vehicle.Vehicles[data.Ambush.Vehicle[0].iIndex].model = MESA3
			data.Vehicle.Vehicles[data.Ambush.Vehicle[1].iIndex].model = MESA3
		BREAK
	ENDSWITCH
ENDPROC

PROC AMMUDEL_SETUP_PLACEMENT_DATA()

	// Ambush
	AMMUDEL_SETUP_AMBUSH_DATA()

	// Vehicle spawn
	GET_BUNKER_OUTSIDE_SPAWN_POINT(
		GET_OWNED_BUNKER_SIMPLE_INTERIOR(GB_GET_LOCAL_PLAYER_MISSION_HOST()), 
		ENUM_TO_INT(GET_SUBVARIATION()), 
		data.Vehicle.Vehicles[GET_TAG_ID(eAMMUDEL_TAG_DUNELOADER)].vCoords, 
		data.Vehicle.Vehicles[GET_TAG_ID(eAMMUDEL_TAG_DUNELOADER)].fHeading, 
		TRUE)
		
	SET_VEHICLE_DATA_BIT(GET_TAG_ID(eAMMUDEL_TAG_DUNELOADER), eVEHICLEDATABITSET_FIND_STATIC_SPAWN_COORD_AS_BACKUP)
		
	// Mission Entity
	data.MissionEntity.MissionEntities[0].vCoords = data.Vehicle.Vehicles[GET_TAG_ID(eAMMUDEL_TAG_DUNELOADER)].vCoords

	// Props
	data.Prop.Props[GET_TAG_ID(eAMMUDEL_TAG_SUPPLIES)].vCoords = data.Vehicle.Vehicles[GET_TAG_ID(eAMMUDEL_TAG_DUNELOADER)].vCoords
	data.Prop.Props[GET_TAG_ID(eAMMUDEL_TAG_SUPPLIES)].vRotation = <<0.0, 00.0, 90.0>>
	SET_PROP_DATA_BIT(GET_TAG_ID(eAMMUDEL_TAG_SUPPLIES), ePROPDATABITSET_COLLISION_PROOF)
	SET_PROP_DATA_BIT(GET_TAG_ID(eAMMUDEL_TAG_SUPPLIES), ePROPDATABITSET_SPAWN_INVINCIBLE)
	
	data.Prop.AttachedProp[0].eParentType = ET_VEHICLE
	data.Prop.AttachedProp[0].iParentIndex = GET_TAG_ID(eAMMUDEL_TAG_DUNELOADER)
	data.Prop.AttachedProp[0].iPropIndex = GET_TAG_ID(eAMMUDEL_TAG_SUPPLIES)
	data.Prop.AttachedProp[0].vOffset = <<0.0, -1.4, 0.375>>
	
	// Timer
	data.ModeTimer.iTimeLimit = 15
	data.ModeTimer.eDisplay = eMODETIMERDISPLAYTYPE_ALWAYS
	
	// Rewards
	data.Reward.bShowCashWithShard = TRUE
	
ENDPROC

//////////////////////////
///    LOGIC
//////////////////////////

/// PURPOSE: Use to set up the logic pointers for your mission
PROC SETUP_MISSION_LOGIC()
 	logic.Data.Setup						=	&AMMUDEL_SETUP_PLACEMENT_DATA
	
	logic.Init.Client						=	&AMMUDEL_INIT_CLIENT
	
	logic.StateMachine.SetupClientStates 	= 	&AMMUDEL_SETUP_CLIENT_MODE_STATES
	logic.StateMachine.SetupStates 			= 	&AMMUDEL_SETUP_MODE_STATES
	
	logic.Ambush.ShouldActivate				= 	&AMMUDEL_AMBUSH_ACTIVATE
	
	logic.MissionEntity.OnCollect			= 	&AMMUDEL_MISSION_ENTITY_ONCOLLECT
	logic.MissionEntity.OnDelivery 			= 	&AMMUDEL_MISSION_ENTITY_ONDELIVERY
	logic.MissionEntity.Blip.Name 			= 	&AMMUDEL_MISSION_ENTITY_BLIP_NAME
	logic.MissionEntity.ShouldSpawn			= 	&AMMUDEL_MISSION_ENTITY_SHOULD_SPAWN
	logic.MissionEntity.ShouldBeCollectable	=	&AMMUDEL_MISSION_ENTITY_SHOULD_BE_COLLECTABLE
	
	logic.Hud.Tickers.CustomStringLocal 	= 	&AMMUDEL_TICKER_STRING_LOCAL
	logic.Hud.Tickers.CustomStringRemote 	= 	&AMMUDEL_TICKER_STRING_REMOTE
	
	logic.Vehicle.Mods 						= 	&AMMUDEL_VEHICLE_MODS
	
	logic.HelpText.Trigger					= 	&AMMUDEL_HELP_TEXT_TRIGGER
	logic.HelpText.Text						= 	&AMMUDEL_HELP_TEXT_LABEL

	logic.Dialogue.CellphoneCharacter		= 	&AMMUDEL_DIALOGUE_PHONE_CHARACTER
	logic.Dialogue.Character 				= 	&AMMUDEL_DIALOGUE_CHARACTER
	logic.Dialogue.ShouldTrigger			= 	&AMMUDEL_DIALOGUE_TRIGGER
	logic.Dialogue.Root						= 	&AMMUDEL_DIALOGUE_ROOT
	logic.Dialogue.SpeakerID 				= 	&AMMUDEL_DIALOGUE_SPEAKER_ID
	logic.Dialogue.SubtitleGroup 			= 	&AMMUDEL_DIALOGUE_SUBTITLE_GROUP
	logic.Dialogue.UseHeadsetAudio			= 	&AMMUDEL_DIALOGUE_USE_HEADSET
	
	// Music
	logic.Audio.MusicTrigger.Event			=	&AMMUDEL_MUSIC_EVENT
	logic.Audio.MusicTrigger.EventString	=	&AMMUDEL_MUSIC_EVENT_STRINGS

	// Post Mission Text
	logic.Phonecall.PostMissionCall			=	&AMMUDEL_POST_MISSION_TEXT
	
	logic.Reward.Multiplier					= 	&AMMUDEL_REWARD_MULTIPLIER
	
	logic.Cleanup.ScriptEnd					= 	&AMMUDEL_SCRIPT_CLEANUP
	
ENDPROC
#ENDIF
