USING "globals.sch"
USING "rage_builtins.sch"
USING "Nightclub/Club_Source/fm_content_club_source_core.sch"

//////////////////////////
///    CONST 
//////////////////////////

CONST_INT cSRCTRUCK_VEHICLE_TRUCK			0
CONST_INT cSRCTRUCK_MAX_TRUCK_VARIATIONS	3

CONST_FLOAT cSRCTRUCK_SPEED_ALERT		100.0

//////////////////////////
///    ENUMS
//////////////////////////

ENUM SRCTRUCK_STATE
	eST_STATE_STEAL,
	eST_STATE_DELIVER,
	
	eST_STATE_END
ENDENUM

ENUM SRCTRUCK_CLIENT_STATE
	eST_CLIENTSTATE_STEAL,
	eST_CLIENTSTATE_DELIVER,
	eST_CLIENTSTATE_HELP_DELIVER,
	eST_CLIENTSTATE_RECOVER,
	
	eST_CLIENTSTATE_END
ENDENUM

ENUM SRCTRUCK_PED_TASKS
	eST_TASK_IDLE,
	eST_TASK_DRIVE,
	eST_TASK_WALK_TO,
	eST_TASK_SCENARIO,
	eST_TASK_ATTACK_HATED
ENDENUM

ENUM SRCTRUCK_TAGS
	eST_TAG_TRUCK,
	eST_TAG_DRIVER,
	eST_TAG_PASSENGER,
	eST_TAG_SUPPLIES
ENDENUM

ENUM SRCTRUCK_TICKER
	eST_TICKER_COLLECT,
	eST_TICKER_DELIVER
ENDENUM

ENUM SRCTRUCK_DIALOGUE
	eST_DIALOGUE_OPENING
ENDENUM

ENUM SRCTRUCK_MUSIC
	eST_MUSIC_INVALID = -1,
	eST_MUSIC_NCLUB_SUSPENSE_START,
	eST_MUSIC_NCLUB_MED_INTENSITY,
	eST_MUSIC_NCLUB_SUSPENSE
ENDENUM

ENUM ST_HELP_TEXT
	eST_HELP_TRUCK_DAMAGE
ENDENUM

//////////////////////////
///    HELPERS
//////////////////////////

FUNC SRCTRUCK_STATE SRCTRUCK_GET_STATE()
	RETURN INT_TO_ENUM(SRCTRUCK_STATE, GET_MODE_STATE_ID())
ENDFUNC

FUNC SRCTRUCK_CLIENT_STATE SRCTRUCK_GET_CLIENT_STATE()
	RETURN INT_TO_ENUM(SRCTRUCK_CLIENT_STATE, GET_CLIENT_MODE_STATE_ID())
ENDFUNC

FUNC VECTOR SRCTRUCK_PED_TASK_WALK_TO_COORD(INT iPed)
	IF iPed = GET_TAG_ID(eST_TAG_DRIVER)
		SWITCH GET_SUBVARIATION()
			CASE CSCS_TK_LOCATION_1			RETURN <<232.7152, -1775.7064, 27.7063>>
			CASE CSCS_TK_LOCATION_2			RETURN <<198.7997, -1455.1260, 28.1398>> 
			CASE CSCS_TK_LOCATION_3			RETURN <<1407.8442, -2049.7563, 50.9985>>
			CASE CSCS_TK_LOCATION_4			RETURN <<-702.4203, -1137.7993, 9.6126>>
			CASE CSCS_TK_LOCATION_5			RETURN <<-113.3193, 37.0858, 70.3594>>
		ENDSWITCH
	ELIF iPed = GET_TAG_ID(eST_TAG_PASSENGER)
		SWITCH GET_SUBVARIATION()
			CASE CSCS_TK_LOCATION_1			RETURN <<242.7378, -1780.3271, 27.6845>>
			CASE CSCS_TK_LOCATION_2			RETURN <<192.1237, -1463.4458, 28.1416>>
			CASE CSCS_TK_LOCATION_3			RETURN <<1402.6692, -2062.8713, 50.9985>>
			CASE CSCS_TK_LOCATION_4			RETURN <<-720.6202, -1139.4132, 9.6126>>
			CASE CSCS_TK_LOCATION_5			RETURN <<-103.6889, 37.2976, 70.4785>>
		ENDSWITCH
	ENDIF
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC FLOAT SRCTRUCK_PED_TASK_WALK_TO_HEADING(INT iPed)
	IF iPed = GET_TAG_ID(eST_TAG_DRIVER)
		SWITCH GET_SUBVARIATION()
			CASE CSCS_TK_LOCATION_1			RETURN 49.7479
			CASE CSCS_TK_LOCATION_2			RETURN 322.2037
			CASE CSCS_TK_LOCATION_3			RETURN 105.4370
			CASE CSCS_TK_LOCATION_4			RETURN 126.2606
			CASE CSCS_TK_LOCATION_5			RETURN 323.6700
		ENDSWITCH
	ELIF iPed = GET_TAG_ID(eST_TAG_PASSENGER)
		SWITCH GET_SUBVARIATION()
			CASE CSCS_TK_LOCATION_1			RETURN 145.0530
			CASE CSCS_TK_LOCATION_2			RETURN 9.1694
			CASE CSCS_TK_LOCATION_3			RETURN 55.1834
			CASE CSCS_TK_LOCATION_4			RETURN 121.2633
			CASE CSCS_TK_LOCATION_5			RETURN 78.7883
		ENDSWITCH
	ENDIF
	RETURN 0.0
ENDFUNC

FUNC BOOL SRCTRUCK_PED_TASK_WALK_TO_USE_NAVMESH(INT iPed)
	IF iPed = GET_TAG_ID(eST_TAG_DRIVER)
	OR iPed = GET_TAG_ID(eST_TAG_PASSENGER)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC INT SRCTRUCK_GET_TRUCK_MOD_VARIATION()
	RETURN serverBD.iRandomModeInt0
ENDFUNC

FUNC BOOL SRCTRUCK_DO_DIALOGUE_VARIATION()
	RETURN serverBD.iRandomModeInt1 = 1
ENDFUNC

//////////////////////////
///    MODE STATES
//////////////////////////

PROC SRCTRUCK_STATE_SETUP()
	ADD_MODE_STATE(eST_STATE_STEAL, eMODESTATE_COLLECT_MISSION_ENTITY)
		ADD_MODE_STATE_TRANSITION_DEFAULT(eST_STATE_STEAL, eST_STATE_DELIVER)
		
	ADD_MODE_STATE(eST_STATE_DELIVER, eMODESTATE_DELIVER_MISSION_ENTITY)
		ADD_MODE_STATE_TRANSITION_DEFAULT(eST_STATE_DELIVER, eST_STATE_END)
	
	ADD_MODE_STATE(eST_STATE_END, eMODESTATE_END)
ENDPROC

PROC SRCTRUCK_STATE_SETUP_CLIENT()
	ADD_CLIENT_MODE_STATES_DELIVERY(eST_CLIENTSTATE_STEAL, eST_CLIENTSTATE_DELIVER, eST_CLIENTSTATE_HELP_DELIVER, eST_CLIENTSTATE_RECOVER, eST_CLIENTSTATE_END,
									&SRC_OBJ_STEAL,   &SRC_OBJ_DELIVER,   &SRC_OBJ_HELP_DELIVER,   &EMPTY)
	
	ADD_CLIENT_MODE_STATE(eST_CLIENTSTATE_END, eMODESTATE_END, &EMPTY)
ENDPROC

//////////////////////////
///    PEDS
//////////////////////////

FUNC BOOL SRCTRUCK_PED_TO_ATTACK(INT iPed, PED_INDEX pedId)
	UNUSED_PARAMETER(pedId)
	IF IS_MISSION_SERVER_BIT_SET(eMISSIONSERVERBITSET_TRUCK_HEALTH_LOW)
		RETURN TRUE
	ENDIF
	
	IF data.Ped.Peds[iPed].eSeat = VS_DRIVER
		RETURN HAS_PED_BEEN_TRIGGERED(iPed) AND IS_GENERIC_SERVER_BIT_SET(eGENERICSERVERBITSET_ENTITY_REACHED_FINAL_CHECKPOINT)
	ENDIF
	
	RETURN HAS_PED_BEEN_TRIGGERED(iPed)
ENDFUNC

FUNC BOOL SRCTRUCK_PED_TO_WALK(INT iPed, PED_INDEX pedId)
	UNUSED_PARAMETER(pedId)
	RETURN IS_GENERIC_SERVER_BIT_SET(eGENERICSERVERBITSET_ENTITY_REACHED_FINAL_CHECKPOINT) AND (NOT HAS_PED_BEEN_TRIGGERED(iPed))
ENDFUNC

FUNC BOOL SRCTRUCK_PED_TO_DRIVE(INT iPed, PED_INDEX pedId)
	UNUSED_PARAMETER(pedId)
	IF data.Ped.Peds[iPed].eSeat = VS_DRIVER
		IF HAS_PED_BEEN_TRIGGERED(iPed)
		OR IS_ANY_PLAYER_IN_RANGE_OF_MISSION()
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL SRCTRUCK_PED_TO_SCENARIO(INT iPed, PED_INDEX pedId)
	UNUSED_PARAMETER(pedId)
	IF NOT HAS_PED_BEEN_TRIGGERED(iPed)
	AND IS_ENTITY_IN_RANGE_COORDS(pedId, SRCTRUCK_PED_TASK_WALK_TO_COORD(iPed), 1.5)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SRCTRUCK_TASK_WALK_ONENTRY(INT iPed, PED_INDEX pedId)
	UNUSED_PARAMETER(iPed)
	TASK_LEAVE_ANY_VEHICLE(pedId)
ENDPROC

PROC SRCTRUCK_TASK_ATTACK_ONENTRY(INT iPed, PED_INDEX pedId)
	UNUSED_PARAMETER(iPed)
	IF IS_MISSION_SERVER_BIT_SET(eMISSIONSERVERBITSET_TRUCK_HEALTH_LOW)
		TASK_LEAVE_ANY_VEHICLE(pedId)
	ENDIF
ENDPROC

PROC SRCTRUCK_PED_SETUP_BEHAVIOUR(INT iBehaviour)
	
	SWITCH iBehaviour
		CASE 0
			ADD_PED_TASK(iBehaviour, eST_TASK_IDLE, ePEDTASK_IDLE)
				ADD_PED_TASK_TRANSITION(iBehaviour, eST_TASK_IDLE, eST_TASK_ATTACK_HATED, &SRCTRUCK_PED_TO_ATTACK)
				ADD_PED_TASK_TRANSITION(iBehaviour, eST_TASK_IDLE, eST_TASK_WALK_TO, &SRCTRUCK_PED_TO_WALK)
				ADD_PED_TASK_TRANSITION(iBehaviour, eST_TASK_IDLE, eST_TASK_DRIVE, &SRCTRUCK_PED_TO_DRIVE)
				
			ADD_PED_TASK(iBehaviour, eST_TASK_DRIVE, ePEDTASK_GO_TO_COORD_ANY_MEANS)
				ADD_PED_TASK_TRANSITION(iBehaviour, eST_TASK_DRIVE, eST_TASK_ATTACK_HATED, &SRCTRUCK_PED_TO_ATTACK)
				ADD_PED_TASK_TRANSITION(iBehaviour, eST_TASK_DRIVE, eST_TASK_WALK_TO, &SRCTRUCK_PED_TO_WALK)
			
			ADD_PED_TASK(iBehaviour, eST_TASK_WALK_TO, ePEDTASK_WALK_TO_POINT)
				ADD_PED_TASK_ON_ENTRY(iBehaviour, eST_TASK_WALK_TO, &SRCTRUCK_TASK_WALK_ONENTRY)
				ADD_PED_TASK_TRANSITION(iBehaviour, eST_TASK_WALK_TO, eST_TASK_ATTACK_HATED, &SRCTRUCK_PED_TO_ATTACK)
				ADD_PED_TASK_TRANSITION(iBehaviour, eST_TASK_WALK_TO, eST_TASK_SCENARIO, &SRCTRUCK_PED_TO_SCENARIO)
			
			ADD_PED_TASK(iBehaviour, eST_TASK_SCENARIO, ePEDTASK_DO_SCENARIOS)
				ADD_PED_TASK_TRANSITION(iBehaviour, eST_TASK_SCENARIO, eST_TASK_ATTACK_HATED, &SRCTRUCK_PED_TO_ATTACK)
				
			ADD_PED_TASK(iBehaviour, eST_TASK_ATTACK_HATED, ePEDTASK_ATTACK_HATED_TARGETS)
				ADD_PED_TASK_ON_ENTRY(iBehaviour, eST_TASK_ATTACK_HATED, &SRCTRUCK_TASK_ATTACK_ONENTRY)
		BREAK
		
		CASE 1
			SETUP_DEFAULT_PED_BEHAVIOUR(iBehaviour)
		BREAK
	ENDSWITCH
	
ENDPROC

FUNC INT SRCTRUCK_PED_GET_BEHAVIOUR(INT iPed)
	UNUSED_PARAMETER(iPed)
	IF GET_TAG_ID(eST_TAG_DRIVER) = iPed
	OR GET_TAG_ID(eST_TAG_PASSENGER) = iPed
		RETURN 0
	ENDIF
	
	RETURN 1
ENDFUNC

FUNC INT SRCTRUCK_PED_TASK_GO_TO_NUM_COORDS(INT iPed)
	#IF MAX_NUM_AMBUSH_VEHICLES
	IF IS_PED_BIT_SET(iPed, ePEDBITSET_AMBUSH_PED)
		RETURN 0
	ENDIF
	#ENDIF

	SWITCH GET_SUBVARIATION()
		CASE CSCS_TK_LOCATION_1			RETURN 3
		CASE CSCS_TK_LOCATION_2			RETURN 3
		CASE CSCS_TK_LOCATION_3			RETURN 3
		CASE CSCS_TK_LOCATION_4			RETURN 2
		CASE CSCS_TK_LOCATION_5			RETURN 2
	ENDSWITCH
	
	RETURN 1
ENDFUNC

FUNC VECTOR SRCTRUCK_PED_TASK_GO_TO_COORD(INT iPed, INT iCoord)
	UNUSED_PARAMETER(iPed)
	UNUSED_PARAMETER(iCoord)
	SWITCH GET_SUBVARIATION()
		CASE CSCS_TK_LOCATION_1				
			SWITCH iCoord
				CASE 0		RETURN <<232.0606, -1805.8054, 26.5629>>
				CASE 1		RETURN <<243.9207, -1786.3989, 27.2378>>
				CASE 2		RETURN <<237.3630, -1775.8911, 27.6685>>
			ENDSWITCH
		BREAK
		CASE CSCS_TK_LOCATION_2			
			SWITCH iCoord
				CASE 0		RETURN <<216.5904, -1436.9453, 28.3406>>
				CASE 1		RETURN <<212.2525, -1453.1455, 28.2527>>
				CASE 2		RETURN <<195.9936, -1458.1152, 28.1398>>
			ENDSWITCH
		BREAK
		CASE CSCS_TK_LOCATION_3			
			SWITCH iCoord
				CASE 0		RETURN <<1359.7090, -2017.2985, 50.2170>>
				CASE 1		RETURN <<1381.6421, -2051.7136, 50.9985>>
				CASE 2		RETURN <<1401.4156, -2058.1919, 50.9985>>
			ENDSWITCH
		BREAK
		CASE CSCS_TK_LOCATION_4			
			SWITCH iCoord
				CASE 0		RETURN <<-736.9662, -1160.2881, 9.6681>>
				CASE 1		RETURN <<-722.1434, -1144.2787, 9.6117>>
			ENDSWITCH
		BREAK
		CASE CSCS_TK_LOCATION_5			
			SWITCH iCoord
				CASE 0		RETURN <<-88.9450, 61.1375, 70.6618>>
				CASE 1		RETURN <<-112.3183, 40.5090, 70.3862>>
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN data.Prop.Props[0].vCoords
ENDFUNC

FUNC DRIVINGMODE SRCTRUCK_PED_TASK_GO_TO_FLAGS(INT iPed)
	UNUSED_PARAMETER(iPed)
	IF serverBD.sPed[iPed].iCheckpointStage > 0
		RETURN DF_UseShortCutLinks | DF_UseSwitchedOffNodes | DF_PreferNavmeshRoute
	ENDIF
	
	IF HAS_PED_BEEN_TRIGGERED(iPed)
		RETURN DF_SwerveAroundAllCars | DF_SteerAroundStationaryCars | DF_SteerAroundObjects | DF_SteerAroundPeds | DF_UseShortCutLinks | DF_ChangeLanesAroundObstructions | DF_UseSwitchedOffNodes | DF_ForceJoinInRoadDirection
	ENDIF
	
	RETURN DF_SwerveAroundAllCars | DF_SteerAroundStationaryCars | DF_SteerAroundObjects | DF_SteerAroundPeds | DF_UseShortCutLinks | DF_ChangeLanesAroundObstructions | DF_UseSwitchedOffNodes | DF_AdjustCruiseSpeedBasedOnRoadSpeed | DF_ForceJoinInRoadDirection
ENDFUNC

FUNC FLOAT SRCTRUCK_PED_TASK_GO_TO_SPEED(INT iPed)
	IF IS_MISSION_SERVER_BIT_SET(eMISSIONSERVERBITSET_TRUCK_NEAR_HIDEOUT)
		RETURN 10.0
	ENDIF
	
	IF HAS_PED_BEEN_TRIGGERED(iPed)
		RETURN 100.0
	ENDIF
	
	RETURN 30.0
ENDFUNC

FUNC FLOAT SRCTRUCK_PED_TASK_GO_TO_RADIUS(INT iPed)
	IF serverBD.sPed[iPed].iCheckpointStage > 0
		RETURN 4.0
	ENDIF
	
	RETURN 10.0
ENDFUNC

FUNC FLOAT SRCTRUCK_PED_TASK_GO_TO_RADIUS_SERVER(INT iPed)
	IF serverBD.sPed[iPed].iCheckpointStage > 0
		RETURN 8.0
	ENDIF
	
	RETURN 10.0
ENDFUNC

FUNC STRING SRCTRUCK_PED_TASK_SCENARIO_NAME(INT iPed)
	IF GET_TAG_ID(eST_TAG_DRIVER) = iPed
		IF GET_SUBVARIATION() = CSCS_TK_LOCATION_5
			RETURN "WORLD_HUMAN_STAND_MOBILE"
		ENDIF
		RETURN "WORLD_HUMAN_CLIPBOARD"
	ELIF GET_TAG_ID(eST_TAG_PASSENGER) = iPed
		RETURN "WORLD_HUMAN_GUARD_STAND"
	ENDIF
	
	#IF MAX_NUM_SCENARIOS
	IF data.ped.Peds[iPed].iScenarioIndex != -1
		RETURN CONVERT_TEXT_LABEL_TO_STRING(data.Scenario[data.ped.Peds[iPed].iScenarioIndex].sName)
	ENDIF
	#ENDIF
	
	RETURN ""
ENDFUNC

PROC SRCTRUCK_PED_CLIENT(INT iPed, PED_INDEX pedId, BOOL bDead)
	
	IF NOT bDead
	AND (GET_TAG_ID(eST_TAG_DRIVER) = iPed
	OR GET_TAG_ID(eST_TAG_PASSENGER) = iPed)
	AND IS_PED_IN_ANY_VEHICLE(pedId)
	
		IF IS_MISSION_SERVER_BIT_SET(eMISSIONSERVERBITSET_TRUCK_HEALTH_LOW)
		AND NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedId, SCRIPT_TASK_LEAVE_ANY_VEHICLE)
		AND MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netId)
			SET_PED_COMBAT_ATTRIBUTES(pedId, CA_USE_VEHICLE, FALSE)
			SET_PED_COMBAT_ATTRIBUTES(pedId, CA_DO_DRIVEBYS, FALSE)
			TASK_LEAVE_ANY_VEHICLE(pedId)
			PRINTLN("[",scriptName,"] [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed,"] SRCTRUCK_PED_CLIENT - Told ped to leave vehicle for attack hated on low truck health.")
		ENDIF
		
		IF GET_TAG_ID(eST_TAG_DRIVER) = iPed
			IF NOT IS_MISSION_SERVER_BIT_SET(eMISSIONSERVERBITSET_TRUCK_NEAR_HIDEOUT)
			AND IS_ENTITY_IN_RANGE_COORDS(pedId, SRCTRUCK_PED_TASK_GO_TO_COORD(iPed, 0), 50.0)
				IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netId)
					SET_DRIVE_TASK_CRUISE_SPEED(NET_TO_PED(serverBD.sPed[iPed].netId), 10.0)
					SET_DRIVE_TASK_MAX_CRUISE_SPEED(NET_TO_PED(serverBD.sPed[iPed].netId), 10.0)
					
					SET_DRIVER_AGGRESSIVENESS(pedId, 0.0)
					SET_DRIVER_RACING_MODIFIER(pedId, 0.0)
					SET_COMBAT_FLOAT(pedId, CCF_AUTOMOBILE_SPEED_MODIFIER, 1.0)
					
					CLEAR_PED_TASKS(NET_TO_PED(serverBD.sPed[iPed].netId))
					SET_MISSION_CLIENT_BIT(eMISSIONCLIENTBITSET_TRUCK_NEAR_HIDEOUT)
					
					PRINTLN("[",scriptName,"] [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed,"] SRCTRUCK_PED_CLIENT - Slowed down driver for entering hideout.")
				ENDIF
			ELIF NOT IS_MISSION_SERVER_BIT_SET(eMISSIONSERVERBITSET_SPED_UP_DRIVER)
			AND HAS_PED_BEEN_TRIGGERED(iPed)
			AND MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netId)
				SET_DRIVE_TASK_CRUISE_SPEED(NET_TO_PED(serverBD.sPed[iPed].netId), cSRCTRUCK_SPEED_ALERT)
				SET_DRIVE_TASK_MAX_CRUISE_SPEED(NET_TO_PED(serverBD.sPed[iPed].netId), cSRCTRUCK_SPEED_ALERT)
				CLEAR_PED_TASKS(NET_TO_PED(serverBD.sPed[iPed].netId))
				
				SET_DRIVER_AGGRESSIVENESS(pedId, 1.0)
				SET_DRIVER_RACING_MODIFIER(pedId, 1.0)
				SET_COMBAT_FLOAT(pedId, CCF_AUTOMOBILE_SPEED_MODIFIER, 5.0)
				SET_MISSION_CLIENT_BIT(eMISSIONCLIENTBITSET_SPED_UP_DRIVER)
				
				PRINTLN("[",scriptName,"] [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed,"] SRCTRUCK_PED_CLIENT - Sped up driver on trigger.")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SRCTRUCK_PED_ATTRIBUTES(INT iPed, PED_INDEX pedId, BOOL bAmbush)
	IF NOT bAmbush
		IF GET_TAG_ID(eST_TAG_DRIVER) = iPed
			SET_DRIVER_ABILITY(pedId, 1.0)
		ENDIF
		
		SET_PED_COMBAT_ATTRIBUTES(pedId, CA_DO_DRIVEBYS, TRUE)
	ENDIF
	
	IF GET_TAG_ID(eST_TAG_DRIVER) = iPed
		SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_HEAD, 	1, 1) 
		SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_TORSO, 	0, 0) 
		SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_LEG, 	0, 0) 
	ELIF GET_TAG_ID(eST_TAG_PASSENGER) = iPed
		SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_HEAD, 	0, 2) 
		SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_TORSO, 	0, 1) 
		SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_LEG, 	0, 1) 
	ELSE
		SWITCH GET_SUBVARIATION()
			CASE CSCS_TK_LOCATION_5
				SET_CARJACKER_PED_ATTRIBUTES(iPed, pedId)
			BREAK
		ENDSWITCH
	ENDIF
	
ENDPROC

//////////////////////////
///    VEHICLES
//////////////////////////

//PROC SRCTRUCK_VEHICLE_ATTRIBUTES(INT iVehicle, VEHICLE_INDEX vehId)
//	
//ENDPROC

FUNC BOOL SRCTRUCK_VEHICLE_MODS(INT iVehicle, VEHICLE_SETUP_STRUCT_MP& sData)
	IF iVehicle = GET_TAG_ID(eST_TAG_TRUCK)
		SWITCH SRCTRUCK_GET_TRUCK_MOD_VARIATION()
			CASE 0
				sData.VehicleSetup.eModel = benson // BENSON
				sData.VehicleSetup.tlPlateText = "61XOP051"
				sData.VehicleSetup.iPlateIndex = 3
				sData.VehicleSetup.iColour1 = 111
				sData.VehicleSetup.iColour2 = 1
				sData.VehicleSetup.iColourExtra1 = 114
				sData.VehicleSetup.iColourExtra2 = 156
				sData.iColour5 = 1
				sData.iColour6 = 132
				sData.iLivery2 = 0
				sData.VehicleSetup.iTyreR = 255
				sData.VehicleSetup.iTyreG = 255
				sData.VehicleSetup.iTyreB = 255
				sData.VehicleSetup.iNeonR = 255
				sData.VehicleSetup.iNeonB = 255
				SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_6)
				RETURN TRUE
			BREAK

			CASE 1
				sData.VehicleSetup.eModel = benson // BENSON
				sData.VehicleSetup.tlPlateText = "29MIL116"
				sData.VehicleSetup.iColour1 = 18
				sData.VehicleSetup.iColour2 = 122
				sData.VehicleSetup.iColourExtra1 = 111
				sData.VehicleSetup.iColourExtra2 = 156
				sData.iColour5 = 1
				sData.iColour6 = 132
				sData.iLivery2 = 0
				sData.VehicleSetup.iTyreR = 255
				sData.VehicleSetup.iTyreG = 255
				sData.VehicleSetup.iTyreB = 255
				sData.VehicleSetup.iNeonR = 255
				sData.VehicleSetup.iNeonB = 255
				SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_6)
				RETURN TRUE
			BREAK

			CASE 2
				sData.VehicleSetup.eModel = benson // BENSON
				sData.VehicleSetup.tlPlateText = "08LZJ155"
				sData.VehicleSetup.iColour1 = 77
				sData.VehicleSetup.iColour2 = 122
				sData.VehicleSetup.iColourExtra1 = 0
				sData.VehicleSetup.iColourExtra2 = 156
				sData.iColour5 = 1
				sData.iColour6 = 132
				sData.iLivery2 = 0
				sData.VehicleSetup.iTyreR = 255
				sData.VehicleSetup.iTyreG = 255
				sData.VehicleSetup.iTyreB = 255
				sData.VehicleSetup.iNeonR = 255
				sData.VehicleSetup.iNeonB = 255
				SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_6)
				RETURN TRUE
			BREAK
		ENDSWITCH
	ENDIF

	SWITCH GET_SUBVARIATION()
		CASE CSCS_TK_LOCATION_1
			SWITCH data.Vehicle.Vehicles[iVehicle].model
				CASE PEYOTE3
					sData.VehicleSetup.eModel = PEYOTE3
					sData.VehicleSetup.iPlateIndex = 3
					sData.VehicleSetup.iColour1 = 145
					sData.VehicleSetup.iColour2 = 0
					sData.VehicleSetup.iColourExtra1 = 74
					sData.VehicleSetup.iColourExtra2 = 141
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWheelType = 8
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
					sData.VehicleSetup.iModIndex[MOD_GRILL] = 3
					sData.VehicleSetup.iModIndex[MOD_BONNET] = 1
					sData.VehicleSetup.iModIndex[MOD_ENGINE] = 2
					sData.VehicleSetup.iModIndex[MOD_BRAKES] = 2
					sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 2
					sData.VehicleSetup.iModIndex[MOD_WHEELS] = 36
					sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 4
					sData.VehicleSetup.iModIndex[MOD_STEERING] = 16
					sData.VehicleSetup.iModIndex[MOD_TRUNK] = 3
					sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 5
					sData.VehicleSetup.iModIndex[MOD_CHASSIS3] = 1
					sData.VehicleSetup.iModIndex[MOD_CHASSIS5] = 2
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 2
					RETURN TRUE
				BREAK
				
				CASE BUCCANEER2
					sData.VehicleSetup.eModel = BUCCANEER2
					sData.VehicleSetup.iColour1 = 146
					sData.VehicleSetup.iColour2 = 111
					sData.VehicleSetup.iColourExtra1 = 145
					sData.VehicleSetup.iColourExtra2 = 90
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWheelType = 8
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_3)
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
					sData.VehicleSetup.iModIndex[MOD_BONNET] = 2
					sData.VehicleSetup.iModIndex[MOD_ENGINE] = 2
					sData.VehicleSetup.iModIndex[MOD_BRAKES] = 2
					sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 2
					sData.VehicleSetup.iModIndex[MOD_WHEELS] = 3
					sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 4
					sData.VehicleSetup.iModIndex[MOD_PLAQUE] = 6
					sData.VehicleSetup.iModIndex[MOD_TRUNK] = 2
					sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 4
					sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 1
					sData.VehicleSetup.iModIndex[MOD_CHASSIS5] = 1
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 3
					RETURN TRUE
				BREAK
				
				CASE EMPEROR
					sData.VehicleSetup.eModel = EMPEROR
					sData.VehicleSetup.iColour1 = 0
					sData.VehicleSetup.iColour2 = 0
					sData.VehicleSetup.iColourExtra1 = 145
					sData.VehicleSetup.iColourExtra2 = 156
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWindowTintColour = 3
					sData.VehicleSetup.iWheelType = 2
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_10)
					sData.VehicleSetup.iModIndex[MOD_ENGINE] = 3
					sData.VehicleSetup.iModIndex[MOD_BRAKES] = 2
					sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 2
					sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
					sData.VehicleSetup.iModIndex[MOD_WHEELS] = 18
					sData.VehicleSetup.iModVariation[0] = 1
					RETURN TRUE
				BREAK
				
				CASE CHINO2
					sData.VehicleSetup.eModel = CHINO2
					sData.VehicleSetup.iColour1 = 146
					sData.VehicleSetup.iColour2 = 25
					sData.VehicleSetup.iColourExtra1 = 145
					sData.VehicleSetup.iColourExtra2 = 156
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWindowTintColour = 3
					sData.VehicleSetup.iWheelType = 2
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_1)
					sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
					sData.VehicleSetup.iModIndex[MOD_ENGINE] = 2
					sData.VehicleSetup.iModIndex[MOD_BRAKES] = 2
					sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 2
					sData.VehicleSetup.iModIndex[MOD_WHEELS] = 16
					sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 4
					sData.VehicleSetup.iModIndex[MOD_TRUNK] = 6
					sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 2
					sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 2
					sData.VehicleSetup.iModIndex[MOD_CHASSIS5] = 2
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 9
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		
		CASE CSCS_TK_LOCATION_2
			SWITCH data.Vehicle.Vehicles[iVehicle].model
				CASE CAVALCADE2
					sData.VehicleSetup.eModel = CAVALCADE2
					sData.VehicleSetup.iColour1 = 50
					sData.VehicleSetup.iColour2 = 6
					sData.VehicleSetup.iColourExtra1 = 53
					sData.VehicleSetup.iColourExtra2 = 0
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWheelType = 3
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_11)
					sData.VehicleSetup.iModIndex[MOD_ENGINE] = 2
					sData.VehicleSetup.iModIndex[MOD_BRAKES] = 2
					sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 2
					sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 2
					sData.VehicleSetup.iModIndex[MOD_WHEELS] = 4
					RETURN TRUE
				BREAK
				
				CASE VOODOO
					sData.VehicleSetup.eModel = VOODOO2
					sData.VehicleSetup.iColour1 = 50
					sData.VehicleSetup.iColour2 = 89
					sData.VehicleSetup.iColourExtra1 = 53
					sData.VehicleSetup.iColourExtra2 = 156
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWheelType = 9
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 2
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
					sData.VehicleSetup.iModIndex[MOD_GRILL] = 3
					sData.VehicleSetup.iModIndex[MOD_ENGINE] = 2
					sData.VehicleSetup.iModIndex[MOD_BRAKES] = 2
					sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 2
					sData.VehicleSetup.iModIndex[MOD_WHEELS] = 13
					sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 5
					sData.VehicleSetup.iModIndex[MOD_INTERIOR2] = 6
					sData.VehicleSetup.iModIndex[MOD_TRUNK] = 1
					sData.VehicleSetup.iModIndex[MOD_HYDRO] = 5
					sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 2
					sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 3
					sData.VehicleSetup.iModIndex[MOD_CHASSIS3] = 1
					sData.VehicleSetup.iModIndex[MOD_CHASSIS4] = 8
					sData.VehicleSetup.iModIndex[MOD_CHASSIS5] = 2
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 1
					RETURN TRUE
				BREAK
				
				CASE MANANA2
					sData.VehicleSetup.eModel = MANANA2
					sData.VehicleSetup.iPlateIndex = 2
					sData.VehicleSetup.iColour1 = 50
					sData.VehicleSetup.iColour2 = 0
					sData.VehicleSetup.iColourExtra1 = 53
					sData.VehicleSetup.iColourExtra2 = 156
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWheelType = 8
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_0)
					sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 3
					sData.VehicleSetup.iModIndex[MOD_ENGINE] = 2
					sData.VehicleSetup.iModIndex[MOD_BRAKES] = 2
					sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 2
					sData.VehicleSetup.iModIndex[MOD_WHEELS] = 38
					sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 4
					sData.VehicleSetup.iModIndex[MOD_STEERING] = 8
					sData.VehicleSetup.iModIndex[MOD_TRUNK] = 1
					sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 4
					sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 3
					sData.VehicleSetup.iModIndex[MOD_CHASSIS5] = 1
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 1
					RETURN TRUE
				BREAK
				
				CASE PRIMO2
					sData.VehicleSetup.eModel = PRIMO2
					sData.VehicleSetup.iColour1 = 0
					sData.VehicleSetup.iColour2 = 50
					sData.VehicleSetup.iColourExtra1 = 53
					sData.VehicleSetup.iColourExtra2 = 156
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWindowTintColour = 3
					sData.VehicleSetup.iWheelType = 8
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
					sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 2
					sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
					sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
					sData.VehicleSetup.iModIndex[MOD_GRILL] = 2
					sData.VehicleSetup.iModIndex[MOD_ENGINE] = 2
					sData.VehicleSetup.iModIndex[MOD_BRAKES] = 2
					sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 2
					sData.VehicleSetup.iModIndex[MOD_WHEELS] = 1
					sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 1
					sData.VehicleSetup.iModIndex[MOD_TRUNK] = 2
					sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 5
					sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 5
					sData.VehicleSetup.iModIndex[MOD_CHASSIS3] = 2
					sData.VehicleSetup.iModIndex[MOD_CHASSIS4] = 2
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		
		CASE CSCS_TK_LOCATION_3
			SWITCH data.Vehicle.Vehicles[iVehicle].model
				CASE LANDSTALKER2
					sData.VehicleSetup.eModel = LANDSTALKER2
					sData.VehicleSetup.iColour1 = 88
					sData.VehicleSetup.iColour2 = 0
					sData.VehicleSetup.iColourExtra1 = 107
					sData.VehicleSetup.iColourExtra2 = 156
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWindowTintColour = 3
					sData.VehicleSetup.iWheelType = 3
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 3
					sData.VehicleSetup.iModIndex[MOD_SKIRT] = 5
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
					sData.VehicleSetup.iModIndex[MOD_GRILL] = 3
					sData.VehicleSetup.iModIndex[MOD_BONNET] = 2
					sData.VehicleSetup.iModIndex[MOD_WING_R] = 2
					sData.VehicleSetup.iModIndex[MOD_ENGINE] = 2
					sData.VehicleSetup.iModIndex[MOD_BRAKES] = 2
					sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 2
					sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 2
					RETURN TRUE
				BREAK
				
				CASE FACTION2
					sData.VehicleSetup.eModel = FACTION2
					sData.VehicleSetup.iColour1 = 0
					sData.VehicleSetup.iColour2 = 0
					sData.VehicleSetup.iColourExtra1 = 0
					sData.VehicleSetup.iColourExtra2 = 89
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWindowTintColour = 3
					sData.VehicleSetup.iWheelType = 2
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_1)
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
					sData.VehicleSetup.iModIndex[MOD_BONNET] = 1
					sData.VehicleSetup.iModIndex[MOD_ENGINE] = 2
					sData.VehicleSetup.iModIndex[MOD_BRAKES] = 2
					sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 2
					sData.VehicleSetup.iModIndex[MOD_WHEELS] = 2
					sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 4
					sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 6
					sData.VehicleSetup.iModIndex[MOD_STEERING] = 1
					sData.VehicleSetup.iModIndex[MOD_ICE] = 1
					sData.VehicleSetup.iModIndex[MOD_TRUNK] = 1
					sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 4
					sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 1
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 2
					RETURN TRUE
				BREAK
				
				CASE SABREGT2
					sData.VehicleSetup.eModel = SABREGT2
					sData.VehicleSetup.iColour1 = 88
					sData.VehicleSetup.iColour2 = 88
					sData.VehicleSetup.iColourExtra1 = 88
					sData.VehicleSetup.iColourExtra2 = 88
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWindowTintColour = 2
					sData.VehicleSetup.iWheelType = 1
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
					sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 3
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
					sData.VehicleSetup.iModIndex[MOD_BONNET] = 2
					sData.VehicleSetup.iModIndex[MOD_ENGINE] = 2
					sData.VehicleSetup.iModIndex[MOD_BRAKES] = 2
					sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 2
					sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 1
					sData.VehicleSetup.iModIndex[MOD_TRUNK] = 1
					sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 4
					sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 3
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 3
					RETURN TRUE
				BREAK
				
				CASE VIRGO2
					sData.VehicleSetup.eModel = VIRGO2
					sData.VehicleSetup.iColour1 = 88
					sData.VehicleSetup.iColour2 = 0
					sData.VehicleSetup.iColourExtra1 = 0
					sData.VehicleSetup.iColourExtra2 = 156
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWheelType = 2
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
					sData.VehicleSetup.iModIndex[MOD_GRILL] = 3
					sData.VehicleSetup.iModIndex[MOD_ENGINE] = 2
					sData.VehicleSetup.iModIndex[MOD_BRAKES] = 2
					sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 2
					sData.VehicleSetup.iModIndex[MOD_WHEELS] = 30
					sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 4
					sData.VehicleSetup.iModIndex[MOD_PLAQUE] = 18
					sData.VehicleSetup.iModIndex[MOD_TRUNK] = 1
					sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 4
					sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 4
					sData.VehicleSetup.iModIndex[MOD_CHASSIS3] = 1
					sData.VehicleSetup.iModIndex[MOD_CHASSIS4] = 1
					sData.VehicleSetup.iModIndex[MOD_CHASSIS5] = 1
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 3
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		
		CASE CSCS_TK_LOCATION_5
			SWITCH data.Vehicle.Vehicles[iVehicle].model
				CASE BANSHEE2
					IF IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_AMBUSH_VEHICLE)
						sData.VehicleSetup.eModel = BANSHEE2
						sData.VehicleSetup.tlPlateText = "06NZQ185"
						sData.VehicleSetup.iColour1 = 38
						sData.VehicleSetup.iColour2 = 38
						sData.VehicleSetup.iColourExtra1 = 0
						sData.VehicleSetup.iColourExtra2 = 156
						sData.iColour5 = 1
						sData.iColour6 = 132
						sData.iLivery2 = 0
						sData.VehicleSetup.iWindowTintColour = 3
						sData.VehicleSetup.iTyreR = 255
						sData.VehicleSetup.iTyreG = 255
						sData.VehicleSetup.iTyreB = 255
						SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_1)
						sData.VehicleSetup.iModIndex[MOD_SPOILER] = 2
						sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 3
						sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
						sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
						sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
						sData.VehicleSetup.iModIndex[MOD_BONNET] = 4
						sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
						sData.VehicleSetup.iModIndex[MOD_ENGINE] = 3
						sData.VehicleSetup.iModIndex[MOD_BRAKES] = 2
						sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 2
						sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 2
						sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
						sData.VehicleSetup.iModIndex[MOD_WHEELS] = 26
						sData.VehicleSetup.iModIndex[MOD_INTERIOR3] = 1
						sData.VehicleSetup.iModIndex[MOD_INTERIOR5] = 3
						sData.VehicleSetup.iModIndex[MOD_SEATS] = 5
						sData.VehicleSetup.iModIndex[MOD_STEERING] = 13
						sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 2
						sData.VehicleSetup.iModIndex[MOD_ENGINEBAY3] = 6
						sData.VehicleSetup.iModIndex[MOD_LIVERY] = 6
						RETURN TRUE
					ELSE
						sData.VehicleSetup.eModel = BANSHEE2
						sData.VehicleSetup.tlPlateText = "65GRZ071"
						sData.VehicleSetup.iColour1 = 27
						sData.VehicleSetup.iColour2 = 27
						sData.VehicleSetup.iColourExtra1 = 27
						sData.VehicleSetup.iColourExtra2 = 2
						sData.iColour5 = 1
						sData.iColour6 = 132
						sData.iLivery2 = 0
						sData.VehicleSetup.iWindowTintColour = 3
						sData.VehicleSetup.iTyreR = 255
						sData.VehicleSetup.iTyreG = 255
						sData.VehicleSetup.iTyreB = 255
						SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_1)
						sData.VehicleSetup.iModIndex[MOD_SPOILER] = 3
						sData.VehicleSetup.iModIndex[MOD_SKIRT] = 2
						sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 4
						sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
						sData.VehicleSetup.iModIndex[MOD_BONNET] = 3
						sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
						sData.VehicleSetup.iModIndex[MOD_ENGINE] = 3
						sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 2
						sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 2
						sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
						sData.VehicleSetup.iModIndex[MOD_WHEELS] = 7
						sData.VehicleSetup.iModIndex[MOD_INTERIOR3] = 1
						sData.VehicleSetup.iModIndex[MOD_INTERIOR5] = 3
						sData.VehicleSetup.iModIndex[MOD_SEATS] = 5
						sData.VehicleSetup.iModIndex[MOD_STEERING] = 13
						sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 4
						sData.VehicleSetup.iModIndex[MOD_ENGINEBAY3] = 1
						sData.VehicleSetup.iModIndex[MOD_LIVERY] = 3
						RETURN TRUE
					ENDIF
				BREAK
				
				CASE SULTANRS
					IF IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_AMBUSH_VEHICLE)
						sData.VehicleSetup.eModel = SULTANRS
						sData.VehicleSetup.tlPlateText = "86CVG072"
						sData.VehicleSetup.iColour1 = 28
						sData.VehicleSetup.iColour2 = 28
						sData.VehicleSetup.iColourExtra1 = 28
						sData.VehicleSetup.iColourExtra2 = 0
						sData.iColour5 = 1
						sData.iColour6 = 132
						sData.iLivery2 = 0
						sData.VehicleSetup.iWindowTintColour = 2
						sData.VehicleSetup.iTyreR = 255
						sData.VehicleSetup.iTyreG = 255
						sData.VehicleSetup.iTyreB = 255
						sData.VehicleSetup.iNeonR = 255
						sData.VehicleSetup.iNeonB = 255
						sData.VehicleSetup.iModIndex[MOD_SPOILER] = 3
						sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 11
						sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 2
						sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
						sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
						sData.VehicleSetup.iModIndex[MOD_GRILL] = 4
						sData.VehicleSetup.iModIndex[MOD_BONNET] = 8
						sData.VehicleSetup.iModIndex[MOD_ENGINE] = 3
						sData.VehicleSetup.iModIndex[MOD_BRAKES] = 2
						sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 2
						sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 2
						sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
						sData.VehicleSetup.iModIndex[MOD_WHEELS] = 2
						sData.VehicleSetup.iModIndex[MOD_INTERIOR3] = 2
						sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 1
						sData.VehicleSetup.iModIndex[MOD_INTERIOR5] = 6
						sData.VehicleSetup.iModIndex[MOD_SEATS] = 3
						sData.VehicleSetup.iModIndex[MOD_STEERING] = 12
						sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 3
						sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 3
						sData.VehicleSetup.iModIndex[MOD_ENGINEBAY3] = 1
						sData.VehicleSetup.iModIndex[MOD_CHASSIS4] = 1
						sData.VehicleSetup.iModIndex[MOD_LIVERY] = 5
						RETURN TRUE
					ELSE
						sData.VehicleSetup.eModel = SULTANRS
						sData.VehicleSetup.tlPlateText = "15HBW365"
						sData.VehicleSetup.iColour1 = 92
						sData.VehicleSetup.iColour2 = 92
						sData.VehicleSetup.iColourExtra1 = 92
						sData.VehicleSetup.iColourExtra2 = 156
						sData.iColour5 = 1
						sData.iColour6 = 132
						sData.iLivery2 = 0
						sData.VehicleSetup.iWindowTintColour = 2
						sData.VehicleSetup.iTyreR = 255
						sData.VehicleSetup.iTyreG = 255
						sData.VehicleSetup.iTyreB = 255
						sData.VehicleSetup.iNeonR = 255
						sData.VehicleSetup.iNeonB = 255
						sData.VehicleSetup.iModIndex[MOD_SPOILER] = 11
						sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 9
						sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 6
						sData.VehicleSetup.iModIndex[MOD_SKIRT] = 2
						sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 4
						sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
						sData.VehicleSetup.iModIndex[MOD_BONNET] = 1
						sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
						sData.VehicleSetup.iModIndex[MOD_ENGINE] = 3
						sData.VehicleSetup.iModIndex[MOD_BRAKES] = 2
						sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 2
						sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 2
						sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
						sData.VehicleSetup.iModIndex[MOD_WHEELS] = 49
						sData.VehicleSetup.iModIndex[MOD_INTERIOR3] = 3
						sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 5
						sData.VehicleSetup.iModIndex[MOD_INTERIOR5] = 4
						sData.VehicleSetup.iModIndex[MOD_SEATS] = 5
						sData.VehicleSetup.iModIndex[MOD_STEERING] = 6
						sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 1
						sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 4
						sData.VehicleSetup.iModIndex[MOD_ENGINEBAY3] = 6
						sData.VehicleSetup.iModIndex[MOD_CHASSIS4] = 1
						sData.VehicleSetup.iModIndex[MOD_LIVERY] = 4
						RETURN TRUE
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC SRCTRUCK_VEHICLE_CLIENT(INT iVehicle, VEHICLE_INDEX vehID, BOOL bDriveable, BOOL bInVehicle, INT iMissionEntityForCarrier)
	UNUSED_PARAMETER(iMissionEntityForCarrier)
	UNUSED_PARAMETER(bInVehicle)
	UNUSED_PARAMETER(bDriveable)
	
	IF iVehicle = GET_TAG_ID(eST_TAG_TRUCK)
		IF NOT IS_MISSION_SERVER_BIT_SET(eMISSIONSERVERBITSET_TRUCK_HEALTH_LOW)
		AND GET_VEHICLE_HEALTH_PERCENTAGE(vehId) < 60.0
			SET_MISSION_CLIENT_BIT(eMISSIONCLIENTBITSET_TRUCK_HEALTH_LOW)
		ENDIF
	ENDIF
ENDPROC

//////////////////////////
///    MISSION
//////////////////////////

FUNC VECTOR SRCTRUCK_MISSION_COORD()
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[GET_TAG_ID(eST_TAG_TRUCK)].netId)
		RETURN GET_ENTITY_COORDS(NET_TO_ENT(serverBD.sVehicle[GET_TAG_ID(eST_TAG_TRUCK)].netId), FALSE)
	ENDIF
	RETURN data.Vehicle.Vehicles[GET_TAG_ID(eST_TAG_TRUCK)].vCoords
ENDFUNC

FUNC FLOAT SRCTRUCK_MISSION_RANGE()
	IF GET_PED_TASK_ID(GET_TAG_ID(eST_TAG_DRIVER)) = ENUM_TO_INT(eST_TASK_IDLE)
		RETURN 2000.0
	ENDIF
	RETURN 100.0
ENDFUNC

//////////////////////////
///    MISSION ENTITY
//////////////////////////

PROC SRCTRUCK_MISSION_ENTITY_ONCOLLECT(INT iMissionEntity)
	UNUSED_PARAMETER(iMissionEntity)
	
	MISSION_TICKER_DATA sTickerData
	sTickerData.iInt1 = ENUM_TO_INT(eST_TICKER_COLLECT)
	SEND_FM_CONTENT_TICKER_EVENT_WITH_DATA(eFMCTICKER_CUSTOM, sTickerData) 
ENDPROC

PROC SRCTRUCK_MISSION_ENTITY_ONDELIVERY(INT iMissionEntity, BOOL bDeliverer)
	UNUSED_PARAMETER(iMissionEntity)
	
	IF bDeliverer
		MISSION_TICKER_DATA sTickerData
		sTickerData.iInt1 = ENUM_TO_INT(eST_TICKER_DELIVER)
		SEND_FM_CONTENT_TICKER_EVENT_WITH_DATA(eFMCTICKER_CUSTOM, sTickerData)
	ENDIF
ENDPROC

//////////////////////////
///   TICKER
//////////////////////////

FUNC STRING SRCTRUCK_GET_TICKER_STRING_LOCAL(MISSION_TICKER_DATA &sTickerData)
	UNUSED_PARAMETER(sTickerData)
	SWITCH INT_TO_ENUM(SRCTRUCK_TICKER, sTickerData.iInt1)
		CASE eST_TICKER_COLLECT			RETURN "NCS_TIC_LCOL"
		CASE eST_TICKER_DELIVER			RETURN "NCS_TIC_LDEL"
	ENDSWITCH

	RETURN ""
ENDFUNC

FUNC STRING SRCTRUCK_GET_TICKER_STRING_REMOTE(MISSION_TICKER_DATA &sTickerData)
	UNUSED_PARAMETER(sTickerData)
	SWITCH INT_TO_ENUM(SRCTRUCK_TICKER, sTickerData.iInt1)
		CASE eST_TICKER_COLLECT			RETURN "NCS_TIC_RCOL"
		CASE eST_TICKER_DELIVER			RETURN "NCS_TIC_RDEL"
	ENDSWITCH
	RETURN ""
ENDFUNC

//////////////////////////
///   DIALOGUE
//////////////////////////
    
FUNC STRING SRCTRUCK_DIALOGUE_ROOT(INT iDialogue)
	IF SRCTRUCK_DO_DIALOGUE_VARIATION()
		SWITCH INT_TO_ENUM(SRCTRUCK_DIALOGUE, iDialogue)
			CASE eST_DIALOGUE_OPENING				
				SWITCH GET_GOODS_TYPE()
					CASE BUSINESS_TYPE_CARGO				RETURN "SM2YO_SST_2A"
					CASE BUSINESS_TYPE_WEAPONS				RETURN "SM2YO_SST_3A"
					CASE BUSINESS_TYPE_COKE					RETURN "SM2YO_SST_4A"
					CASE BUSINESS_TYPE_METH					RETURN "SM2YO_SST_5A"
					CASE BUSINESS_TYPE_WEED					RETURN "SM2YO_SST_6A"
					CASE BUSINESS_TYPE_COUNTERFEIT_CASH		RETURN "SM2YO_SST_7A"
					CASE BUSINESS_TYPE_FORGED_DOCS			RETURN "SM2YO_SST_8A"
					DEFAULT									RETURN "SM2YO_SST_1A"
				ENDSWITCH
			BREAK
		ENDSWITCH
	ELSE
		SWITCH INT_TO_ENUM(SRCTRUCK_DIALOGUE, iDialogue)
			CASE eST_DIALOGUE_OPENING				
				SWITCH GET_GOODS_TYPE()
					CASE BUSINESS_TYPE_CARGO				RETURN "SM2YO_SST_2B"
					CASE BUSINESS_TYPE_WEAPONS				RETURN "SM2YO_SST_3B"
					CASE BUSINESS_TYPE_COKE					RETURN "SM2YO_SST_4B"
					CASE BUSINESS_TYPE_METH					RETURN "SM2YO_SST_5B"
					CASE BUSINESS_TYPE_WEED					RETURN "SM2YO_SST_6B"
					CASE BUSINESS_TYPE_COUNTERFEIT_CASH		RETURN "SM2YO_SST_7B"
					CASE BUSINESS_TYPE_FORGED_DOCS			RETURN "SM2YO_SST_8B"
					DEFAULT									RETURN "SM2YO_SST_1B"
				ENDSWITCH
			BREAK
		ENDSWITCH
	ENDIF
	
	RETURN ""
ENDFUNC

FUNC BOOL SRCTRUCK_DIALOGUE_TRIGGER(INT iDialogue)
	SWITCH INT_TO_ENUM(SRCTRUCK_DIALOGUE, iDialogue)
		CASE eST_DIALOGUE_OPENING				RETURN TRUE
	ENDSWITCH
	RETURN FALSE
ENDFUNC

//////////////////////////
///    INIT
//////////////////////////

FUNC BOOL SRCTRUCK_INIT_SERVER()
	serverBD.iRandomModeInt0 = GET_RANDOM_INT_IN_RANGE(0, cSRCTRUCK_MAX_TRUCK_VARIATIONS)
	serverBD.iRandomModeInt1 = GET_RANDOM_INT_IN_RANGE(0, 2)
	RETURN TRUE
ENDFUNC

//////////////////////////
///   MUSIC EVENTS
//////////////////////////

FUNC STRING SRCTRUCK_MUSIC_EVENT_STRINGS(INT iMusicEvent)
	SWITCH INT_TO_ENUM(SRCTRUCK_MUSIC, iMusicEvent)
		CASE eST_MUSIC_NCLUB_SUSPENSE_START			RETURN "MPSUM2_NCLUB_SUSPENSE_START"
		CASE eST_MUSIC_NCLUB_SUSPENSE				RETURN "MPSUM2_NCLUB_SUSPENSE"	
		CASE eST_MUSIC_NCLUB_MED_INTENSITY			RETURN "MPSUM2_NCLUB_MED_INTENSITY"	
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC INT SRCTRUCK_MUSIC_EVENT(INT iCurrentEvent)
	SWITCH INT_TO_ENUM(SRCTRUCK_MUSIC, iCurrentEvent)
		CASE eST_MUSIC_INVALID
			RETURN ENUM_TO_INT(eST_MUSIC_NCLUB_SUSPENSE_START)
		BREAK
		
		CASE eST_MUSIC_NCLUB_SUSPENSE_START
			IF (GET_PED_TASK_ID(GET_TAG_ID(eST_TAG_DRIVER)) > ENUM_TO_INT(eST_TASK_IDLE)
			AND IS_LOCAL_PLAYER_IN_RANGE_OF_MISSION())
			OR HAS_ANY_PED_GROUP_BEEN_TRIGGERED()
				RETURN ENUM_TO_INT(eST_MUSIC_NCLUB_MED_INTENSITY)
			ENDIF
		BREAK
		
		CASE eST_MUSIC_NCLUB_MED_INTENSITY
			IF SRCTRUCK_GET_STATE() >= eST_STATE_DELIVER
				IF NOT HAS_AMBUSH_ACTIVATED()
					RETURN ENUM_TO_INT(eST_MUSIC_NCLUB_SUSPENSE)
				ELIF IS_AMBUSH_WAVE_DEAD()
					RETURN ENUM_TO_INT(eST_MUSIC_NCLUB_SUSPENSE)
				ENDIF
			ENDIF
		BREAK
		
		CASE eST_MUSIC_NCLUB_SUSPENSE
			IF HAS_AMBUSH_ACTIVATED()
			AND ARE_ANY_AMBUSH_ALIVE()
				RETURN ENUM_TO_INT(eST_MUSIC_NCLUB_MED_INTENSITY)
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN -1
ENDFUNC

//////////////////////////
///    AMBUSH
//////////////////////////

FUNC BOOL SRCTRUCK_AMBUSH_ACTIVATE()
	IF HAS_ANY_MISSION_ENTITY_BEEN_COLLECTED_FOR_THE_FIRST_TIME()
		RETURN TRUE
	ENDIF
	
	IF GET_PED_STATE(GET_TAG_ID(eST_TAG_DRIVER)) > ePEDSTATE_ACTIVE
		RETURN TRUE
	ENDIF
	
	IF GET_PED_STATE(GET_TAG_ID(eST_TAG_DRIVER)) = ePEDSTATE_ACTIVE
	AND NOT IS_PED_LOCAL_BIT_SET(GET_TAG_ID(eST_TAG_DRIVER), ePEDLOCALBITSET_IN_ANY_VEHICLE)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/////////////////////////
///   HELP TEXT
//////////////////////////

FUNC BOOL SRCTRUCK_HELP_TEXT_TRIGGER(INT iHelp)
	SWITCH INT_TO_ENUM(ST_HELP_TEXT, iHelp)
		CASE eST_HELP_TRUCK_DAMAGE			
			IF GET_PED_TASK_ID(GET_TAG_ID(eST_TAG_DRIVER)) = ENUM_TO_INT(eST_TASK_DRIVE)
			AND NOT IS_MISSION_SERVER_BIT_SET(eMISSIONSERVERBITSET_TRUCK_HEALTH_LOW)
			AND IS_LOCAL_PLAYER_IN_RANGE_OF_MISSION()
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC STRING SRCTRUCK_HELP_TEXT_LABEL(INT iHelp)
	SWITCH INT_TO_ENUM(ST_HELP_TEXT, iHelp)
		CASE eST_HELP_TRUCK_DAMAGE		RETURN "NCS_HT_DMGTRK"
	ENDSWITCH
	RETURN ""
ENDFUNC

/////////////////////////
///   FOCUS CAM
//////////////////////////

FUNC BOOL SRCTRUCK_FOCUS_CAM_ENABLE(INT iCam)
	UNUSED_PARAMETER(iCam)
	RETURN SRCTRUCK_GET_STATE() = eST_STATE_STEAL
ENDFUNC

FUNC STRING SRCTRUCK_FOCUS_CAM_PROMPT(INT iCam)
	UNUSED_PARAMETER(iCam)
	RETURN "NCS_HT_FOCUS"
ENDFUNC

FUNC FLOAT SRCTRUCK_REWARD_MULTIPLIER(eREWARD_TYPE eType)
	RETURN GET_SUMMER_2022_FIRST_PAYOUT_BOOST_MULTIPLIER(sMission.iType, ENUM_TO_INT(GET_VARIATION()), eType)
ENDFUNC

PROC SRCTRUCK_SCRIPT_CLEANUP()
	
	SET_SUMMER_2022_FIRST_PAYOUT_BOOST_COMPLETE(sMission.iType, ENUM_TO_INT(GET_VARIATION()))
	
ENDPROC

//////////////////////////
///    PLACEMENT DATA
//////////////////////////

PROC SRCTRUCK_DATA_SETUP()
	
	data.Prop.AttachedProp[0].eParentType = ET_VEHICLE
	data.Prop.AttachedProp[0].iParentIndex = GET_TAG_ID(eST_TAG_TRUCK)
	data.Prop.AttachedProp[0].iPropIndex = GET_TAG_ID(eST_TAG_SUPPLIES)
	data.Prop.AttachedProp[0].vOffset = <<0.0, -3.5, 0.08>>
	data.Prop.Props[GET_TAG_ID(eST_TAG_SUPPLIES)].vRotation = <<0.0, 0.0, 0.0>>
	
	data.Ambush.iSpawnDelay = 2000
	
	data.Reward.bShowCashWithShard = TRUE
ENDPROC

//////////////////////////
///    LOGIC
//////////////////////////

PROC SRCTRUCK_SETUP_LOGIC()
	
	// Data
	logic.Data.Setup 						= &SRCTRUCK_DATA_SETUP
	
	// Init
	logic.Init.Server						= &SRCTRUCK_INIT_SERVER
	
	// State Machines
	logic.StateMachine.SetupStates 			= &SRCTRUCK_STATE_SETUP
	logic.StateMachine.SetupClientStates 	= &SRCTRUCK_STATE_SETUP_CLIENT
	
	// Vehicles
	logic.Vehicle.Mods						= &SRCTRUCK_VEHICLE_MODS
	logic.Vehicle.Client					= &SRCTRUCK_VEHICLE_CLIENT
	//logic.Vehicle.Attributes				= &SRCTRUCK_VEHICLE_ATTRIBUTES
	
	// Peds
	logic.Ped.Behaviour.Setup 				= &SRCTRUCK_PED_SETUP_BEHAVIOUR
	logic.Ped.Behaviour.Get 				= &SRCTRUCK_PED_GET_BEHAVIOUR
	logic.Ped.Attributes					= &SRCTRUCK_PED_ATTRIBUTES
	logic.Ped.Client						= &SRCTRUCK_PED_CLIENT
	
	// Mission
	logic.Mission.Coord						= &SRCTRUCK_MISSION_COORD
	logic.Mission.WithinRange				= &SRCTRUCK_MISSION_RANGE
	
	// Ambush
	logic.Ambush.ShouldActivate				= &SRCTRUCK_AMBUSH_ACTIVATE
	
	// Tasks
	logic.Ped.Task.GoToCoordAnyMeans.NumCoords		= &SRCTRUCK_PED_TASK_GO_TO_NUM_COORDS
	logic.Ped.Task.GoToCoordAnyMeans.Coords			= &SRCTRUCK_PED_TASK_GO_TO_COORD
	logic.Ped.Task.GoToCoordAnyMeans.DrivingFlags	= &SRCTRUCK_PED_TASK_GO_TO_FLAGS
	logic.Ped.Task.GoToCoordAnyMeans.Speed			= &SRCTRUCK_PED_TASK_GO_TO_SPEED
	logic.Ped.Task.GoToCoordAnyMeans.Radius			= &SRCTRUCK_PED_TASK_GO_TO_RADIUS
	logic.Ped.Task.GoToCoordAnyMeans.RadiusServer	= &SRCTRUCK_PED_TASK_GO_TO_RADIUS_SERVER
	
	logic.Ped.Task.WalkToPoint.Coords		= &SRCTRUCK_PED_TASK_WALK_TO_COORD
	logic.Ped.Task.WalkToPoint.FinalHeading	= &SRCTRUCK_PED_TASK_WALK_TO_HEADING
	logic.Ped.Task.WalkToPoint.UseNavMesh	= &SRCTRUCK_PED_TASK_WALK_TO_USE_NAVMESH
	logic.Ped.Task.DoScenario.ScenarioName	= &SRCTRUCK_PED_TASK_SCENARIO_NAME
	
	// Tickers
	logic.Hud.Tickers.CustomStringLocal 	= &SRCTRUCK_GET_TICKER_STRING_LOCAL
	logic.Hud.Tickers.CustomStringRemote 	= &SRCTRUCK_GET_TICKER_STRING_REMOTE
	
	// Mission Entity
	logic.MissionEntity.OnCollect			= &SRCTRUCK_MISSION_ENTITY_ONCOLLECT
	logic.MissionEntity.OnDelivery			= &SRCTRUCK_MISSION_ENTITY_ONDELIVERY
	
	// Dialogue
	logic.Dialogue.CellphoneCharacter		= &SOURCE_DIALOGUE_PHONE_CHARACTER
	logic.Dialogue.Character 				= &SOURCE_DIALOGUE_CHARACTER
	logic.Dialogue.ShouldTrigger			= &SRCTRUCK_DIALOGUE_TRIGGER
	logic.Dialogue.Root						= &SRCTRUCK_DIALOGUE_ROOT
	logic.Dialogue.SpeakerID 				= &SOURCE_DIALOGUE_SPEAKER_ID
	logic.Dialogue.SubtitleGroup 			= &SOURCE_DIALOGUE_SUBTITLE_GROUP
	logic.Dialogue.UseHeadsetAudio			= &SOURCE_DIALOGUE_USE_HEADSET
	
	// Music
	logic.Audio.MusicTrigger.EventString 	= &SRCTRUCK_MUSIC_EVENT_STRINGS
	logic.Audio.MusicTrigger.Event 			= &SRCTRUCK_MUSIC_EVENT
	
	// Dropoff
	logic.Delivery.EnableDropOffBlipRoute	= &SOURCE_SHOULD_ENABLE_DROPOFF_GPS
	
	// Help Text
	logic.HelpText.Trigger					= &SRCTRUCK_HELP_TEXT_TRIGGER
	logic.HelpText.Text						= &SRCTRUCK_HELP_TEXT_LABEL
	
	// Focus Cam
	logic.FocusCam.Enable					= &SRCTRUCK_FOCUS_CAM_ENABLE
	logic.FocusCam.Prompt					= &SRCTRUCK_FOCUS_CAM_PROMPT
	
	// Rewards
	logic.Reward.Multiplier					= &SRCTRUCK_REWARD_MULTIPLIER
	
	// Cleanup
	logic.Cleanup.ScriptEnd					= &SRCTRUCK_SCRIPT_CLEANUP
	
ENDPROC
