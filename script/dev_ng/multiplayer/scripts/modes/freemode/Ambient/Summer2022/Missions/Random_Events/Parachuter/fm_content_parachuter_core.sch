USING "globals.sch"
USING "rage_builtins.sch"

CONST_INT ENABLE_CONTENT	FEATURE_DLC_1_2022

#IF ENABLE_CONTENT
USING "fm_content_parachuter_structs.sch"
USING "fm_content_random_events_structs.sch"
USING "fm_content_generic_structs.sch"

//----------------------
//	VARIATIONS
//----------------------

FUNC BOOL SETUP_VARIATION(INT iVariation, INT iSubvariation)

	UNUSED_PARAMETER(iVariation)
	UNUSED_PARAMETER(iSubvariation)
	
	
	RETURN TRUE
ENDFUNC

//----------------------
//	MISSION LOGIC
//----------------------

/// PURPOSE: Use to set up the logic pointers for your mission
PROC SETUP_MISSION_LOGIC()
 	
	
ENDPROC

#ENDIF
