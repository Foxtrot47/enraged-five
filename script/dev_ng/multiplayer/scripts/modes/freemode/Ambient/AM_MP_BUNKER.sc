USING "globals.sch"

USING "commands_network.sch"
USING "script_player.sch"
USING "commands_path.sch"
USING "commands_event.sch"

USING "net_include.sch"
USING "net_events.sch"
USING "net_spawn.sch"
USING "net_ambience.sch"

USING "net_wait_zero.sch"
USING "freemode_events_header.sch"

USING "net_MP_Radio.sch"
USING "net_activity_creator_activities.sch"
USING "net_simple_cutscene.sch"
USING "net_gun_locker.sch"
USING "safehouse_sitting_activities.sch"
USING "net_MP_CCTV.sch"
USING "mp_bed_high.sch"
using "net_realty_bunker.sch"
USING "net_simple_cutscene_interior.sch"
USING "net_freemode_delivery_private.sch"
//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════════╡ VARIABLES  ╞══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

CONST_INT BS_BUNKER_DATA_WAS_ON_EXIT_TRIGGERING_MISSION_LAST_FRAME		0
CONST_INT BS_BUNKER_DATA_DOOR_LOCKED									1
CONST_INT BS_BUNKER_DATA_INIT_PRODUCT									2
CONST_INT BS_BUNKER_DATA_INTERIOR_REFRESHED_ON_INIT						3
CONST_INT BS_BUNKER_DATA_AUDIO_SCENE_STARTED							4
CONST_INT BS_BUNKER_DATA_MOVED_VEHICLES_TO_SAFE_SPOTS					5
CONST_INT BS_BUNKER_DATA_CLEANED_UP_TIMECYCLE_MODIFIERS					6
CONST_INT BS_BUNKER_DATA_REQUESTED_AUDIO_BANKS							7
CONST_INT BS_BUNKER_DATA_AMBIENT_ZONE_SET								8
CONST_INT BS_BUNKER_DATA_WAS_PRODUCTION_ACTIVE_LAST_FRAME				9
CONST_INT BS_BUNKER_DATA_PUSHED_TIMECYCLE_MODIFIER						10
CONST_INT BS_BUNKER_DATA_LOAD_SCENE_STARTED								11 // After entity sets have been activated we have to do a load scene to make sure it's all loaded
CONST_INT BS_BUNKER_DATA_BOSS_WAS_WATCHING_CUTSCENE_LAST_FRAME			12
CONST_INT BS_BUNKER_DATA_PLAYING_SETUP_CUTSCENE							13
CONST_INT BS_BUNKER_DATA_DELIVERY_SOUND_DONE							14
CONST_INT BS_BUNKER_DATA_DID_INTERIOR_REFRESH_FOR_CUTSCENE				15
CONST_INT BS_BUNKER_DATA_LOAD_SCENE_FOR_CUTSCENE_STARTED				16
CONST_INT BS_BUNKER_DATA_PRODUCTS_SPAWNED								17
CONST_INT BS_BUNKER_READY_TO_WARP_OUT_W_OWNER							18
CONST_INT BS_BUNKER_DATA_TRIGGERED_ENTER_TRUCK_WITH_VEHICLE				19
CONST_INT BS_BUNKER_DATA_FORCE_TRUCK_TO_ROOM							20
CONST_INT BS_BUNKER_KILL_LOAD_SCENE_WHEN_INSIDE							21
CONST_INT BS_BUNKER_WAIT_FOR_PLAYER_TO_EXIT_VEH							22
CONST_INT BS_BUNKER_DATA_MATERIAL_PROPS_SPAWNED							23
CONST_INT BS_BUNKER_IS_CAR_MOD_SCRIPT_READY								24
CONST_INT BS_BUNKER_CALLED_CLEAR_HELP									25
CONST_INT BS_BUNKER_DATA_RAGDOLLING										26
CONST_INT BS_BUNKER_PERS_VEH_CHECKED_AVAILABLILITY						27
CONST_INT BS_BUNKER_PERS_VEH_AVAILABLE									28
CONST_INT BS_BUNKER_SET_CUTSCENE_ENTITIES_TO_BE_NETWORKED				29
CONST_INT BS_BUNKER_APPLIED_DETAILS_TO_TRANSACTION_VEHICLE				30
CONST_INT BS_BUNKER_DATA_MACHINE_AMBIENT_ZONE_SET						31

CONST_INT BS_BUNKER_CHRISTMAS_DECORATIONS_CREATED						0

CONST_INT BS_2_BUNKER_DATA_REQUESTED_AUDIO_BANK_1						0
CONST_INT BS_2_BUNKER_DATA_REQUESTED_AUDIO_BANK_2						1
CONST_INT BS_2_BUNKER_DATA_REQUESTED_AUDIO_BANK_3						2
CONST_INT BS_2_BUNKER_DATA_HID_PV										3
CONST_INT BS_2_BUNKER_DATA_BLOCKED_WHEELIES								4
CONST_INT BS_2_BUNKER_DATA_FORCE_TRUCK_TO_ROOM							5
CONST_INT BS_2_BUNKER_DATA_DEMO_PRODUCT_CREATED							6
CONST_INT BS_2_BUNKER_DATA_INITIAL_PRODUCTS_SPAWNED						7
CONST_INT BS_2_BUNKER_DATA_TAKE_CONTROL									8
CONST_INT BS_2_BUNKER_TERMINATE_UPGRADE_AUDIO_SCENE						9
CONST_INT BS_2_BUNKER_DONE_RESEARCH_HELP								10
CONST_INT BS_2_BUNKER_REENABLED_DRIVING									11
CONST_INT BS_2_BUNKER_BLOCK_MOC_ENTERY									12
CONST_INT BS_2_BUNKER_FLASHING_LAPTOP_BLIP								13
#IF FEATURE_DLC_1_2022
CONST_INT BS_2_BUNKER_PRINTED_AMMU_NATION_CRATE_HELP					15
#ENDIF

// server broadcast BS
CONST_INT BS_BUNKER_DATA_RESERVE_VEHICLE_INDEX_FOR_TRUCK					0
CONST_INT BS_BUNKER_SERVER_DATA_SET_SERVER_RESEARCH_STATE					1	//We have set the server data for production mode
CONST_INT BS_BUNKER_SERVER_DATA_ASSIGN_TRUCK_TO_INTERIOR					2
CONST_INT BS_BUNKER_SERVER_DATA_FADE_OUT_TRUCK								3
CONST_INT BS_BUNKER_SERVER_DATA_REQUEST_TO_MOVE_OUT_OF_WAY_FOR_TRUCK		4
CONST_INT BS_BUNKER_SERVER_DATA_AREA_IS_SAFE_TO_SPAWN_TRUCK					5
CONST_INT BS_BUNKER_SERVER_DATA_UPDATE_CLONE_TRUCK_TURRETS_AFTER_RENOVATE	6
CONST_INT BS_BUNKER_SERVER_DATA_UPDATE_FREEZE_TRUCK_POS						7
CONST_INT BS_BUNKER_SERVER_DATA_CLEAR_PV_SPACE								8
CONST_INT BS_BUNKER_SERVER_DATA_UNFREEZE_TRUCK								9

// player broadcast BS
CONST_INT BS_BUNKER_DATA_FADE_OUT_TO_ENTER_TRUCK_WITH_VEH				0
CONST_INT BS_BUNKER_MOVE_OUT_OF_WAY										1
CONST_INT BS_BUNKER_WATCHING_INTRO_MOCAP								2
CONST_INT BS_BUNKER_PERSONAL_QUATRS_PURCHASED							3
CONST_INT BS_BUNKER_CADDY_STANDARD_PURCHASED							4
CONST_INT BS_BUNKER_CADDY_UPGRADED_PURCHASED							5
CONST_INT BS_BUNKER_SHOOTING_RANGE_BLACK_PURCHASED						6
CONST_INT BS_BUNKER_SHOOTING_RANGE_WHITE_PURCHASED						7
CONST_INT BS_BUNKER_GUN_LOCKER_PURCHASED								8			
CONST_INT BS_BUNKER_BUNKER_STYLE_A_PURCHASED							9
CONST_INT BS_BUNKER_BUNKER_STYLE_B_PURCHASED							10
CONST_INT BS_BUNKER_BUNKER_STYLE_C_PURCHASED							11
CONST_INT BS_BUNKER_OWNER_IS_UNFREEZE_TRUCK_AFTER_ENTERING_BUNKER		12
CONST_INT BS_BUNKER_OWNER_IS_LEAVING_BUNKER_IN_TRUCK					13

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════════╡ VARIABLES  ╞══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛


CONST_INT NUMBER_OF_BUNKER_VEHICLES		4

CONST_INT CI_BUNKER_ROOM_KEY	-1116396409

CONST_INT BUNKER_CUTSCENE_DEMO_PRODUCT_FULL_FILL_DELAY		6000 // Time takent to fill bunker storage area.
CONST_INT BUNKER_CUTSCENE_DEMO_PRODUCT_FULL_FILL_SUSTAIN	2500 // How long storage should be full for.
CONST_INT BUNKER_CUTSCENE_DEMO_PRODUCT_FULL_FILL_TIME		2000 // Time taken to remove all products from storage.
CONST_INT BUNKER_CUTSCENE_DEMO_PRODUCT_FULL_EMPTY_TIME		6000 // Time taken to remove all products from storage.

ENUM BUNKER_SCRIPT_STATE
	BUNKER_STATE_LOADING,
	BUNKER_STATE_IDLE,
	BUNKER_STATE_PLAYING_SETUP_CUTSCENE,
	BUNKER_STATE_PLAYING_INTRO_CUTSCENE
ENDENUM

ENUM BUNKER_UPGRADE_STATE
	BUNKER_UPGRADE_STATE_FADE_OUT,
	BUNKER_UPGRADE_STATE_WARP_PLAYER,
	BUNKER_UPGRADE_STATE_ACTIVATE_ENTITY_SETS,
	BUNKER_UPGRADE_STATE_REQUEST_VEHICLE_MODELS,
	BUNKER_UPGRADE_STATE_CREATE_VEHICLES,
	BUNKER_UPGRADE_STATE_REFRESH_INTERIOR,
	BUNKER_UPGRADE_STATE_PRODUCTS,
	BUNKER_UPGRADE_STATE_TIMECYCLE_MODIFIERS,
	BUNKER_UPGRADE_STATE_UPDATE_AUDIO,
	BUNKER_UPGRADE_STATE_FADE_IN
ENDENUM

ENUM BUNKER_LAPTOP_SCREEN_STATE
	LAPTOP_SCREEN_STATE_INIT,
	LAPTOP_SCREEN_STATE_LINK_RT,
	LAPTOP_SCREEN_STATE_UPDATE_SCALEFORM
ENDENUM

STRUCT BUNKER_LAPTOP_SCREEN_STRUCT
	BUNKER_LAPTOP_SCREEN_STATE eLaptopState
	INT iRenderTargetID = -1
ENDSTRUCT

//FACTORY_DATA thisBunker

SCRIPT_TIMER st_HostRequestTimer

VAULT_WEAPON_LOADOUT_CUSTOMIZATION sBunkerGunLocker
// Factory Ped Data

CHECK_CREATOR_ACTIVITIES_STRUCT activityCheck[iMaxCreatorActivities]
ACTIVITY_CONTROLLER_STRUCT activityControllerStruct
PED_VAR_ID_STRUCT pedLocalVariationsStruct[iMaxCreatorActivities]
BUNKER_FACTORY_ACTIVITY_STRUCT bunkerFactoryActivityStruct

ACTIVITY_SEAT_STRUCT activitySeatStruct

SCRIPT_TIMER vehSyncDelayTimer

STRUCT ServerBroadcastData
	BUNKER_SCRIPT_STATE eState
	
	NETWORK_INDEX niBunkerVehicles[NUMBER_OF_BUNKER_VEHICLES]
	NETWORK_INDEX niAATrailer
	
	NETWORK_INDEX niCloneTruck
	NETWORK_INDEX niCloneTrailer
	
	VEHICLE_INDEX niTruck
	VEHICLE_INDEX niTrailer
	
	INT AATrailerSaveSlot = -1
	INT iAATrailerVehCreationBS = 0 //uses MP_PROP_CREATE_CARS_BS_APPLIED_DETAILS
	BOOL bOwnerCompletedAATrailerCreation = FALSE
	
	
	INT iBS
	INT iTUTProductSlot = 0
	INT ITUTProductTotal = 0
	INT ITUTProductFadeTime
	INT iTUTMaterialSlot = 0
	INT ITUTMaterialTotal = 0
	INT ITUTMaterialFadeTime
	INT iProductionStage
	
	BOOL bActiveState
	BOOL bReadyToWarp
	
	// Factory Ped serverBD Data
	SERVER_CREATOR_ACTIVITY_PEDS activityPeds[iMaxCreatorActivities]
	SERVER_CREATOR_ACTIVITY_PROPS activityProps[iMaxCreatorActivities]
	SERVER_BROADCAST_DATA_FOR_SEATS serverSeatBD
	FACTORY_PRODUCTION_MODE eprodMode = PRODUCTION_MODE_GOODS
	
	ACTIVITY_INTERIOR_STRUCT serverInteriorStruct
	SCRIPT_TIMER sTruckFreezeTimer
ENDSTRUCT
ServerBroadcastData serverBD

STRUCT PlayerBroadcastData
	INT iBS
	MP_CCTV_CLIENT_DATA_STRUCT MPCCTVClient
	INT iShootingRangePickupUnlocks
	BOOL bDataReset
ENDSTRUCT

ACTIVITY_INTERIOR_STRUCT interiorStruct
PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]

STRUCT PRODUCT_DATA
	OBJECT_INDEX oProductObject[ciMAX_FACTORY_PRODUCT_UNITS]
	OBJECT_INDEX oTUTProductObject[ciMAX_FACTORY_PRODUCT_UNITS]
	INT iSetProductFadeTimer[ciMAX_FACTORY_PRODUCT_UNITS]
	INT iProductFadeComplete[ciMAX_FACTORY_PRODUCT_UNITS]
	INT iDesiredAlpha[ciMAX_FACTORY_PRODUCT_UNITS]
	INT iTimeOfDesiredAlpha[ciMAX_FACTORY_PRODUCT_UNITS]
	INT iStartAlpha[ciMAX_FACTORY_PRODUCT_UNITS]
	INT iTimeOfStartAlpha[ciMAX_FACTORY_PRODUCT_UNITS]
	TIME_DATATYPE tdProductFadeTimer[ciMAX_FACTORY_PRODUCT_UNITS]
ENDSTRUCT

STRUCT MATERIAL_DATA
	OBJECT_INDEX oMaterialObject[ciMAX_FACTORY_MATERIAL_UNITS]
	OBJECT_INDEX oTUTMaterialObject[ciMAX_FACTORY_MATERIAL_UNITS]
	INT iSetMaterialFadeTimer[ciMAX_FACTORY_MATERIAL_UNITS]
	INT iMaterialFadeComplete[ciMAX_FACTORY_MATERIAL_UNITS]
	INT iDesiredAlpha[ciMAX_FACTORY_MATERIAL_UNITS]
	INT iTimeOfDesiredAlpha[ciMAX_FACTORY_MATERIAL_UNITS]
	INT iStartAlpha[ciMAX_FACTORY_MATERIAL_UNITS]
	INT iTimeOfStartAlpha[ciMAX_FACTORY_MATERIAL_UNITS]
	TIME_DATATYPE tdMaterialFadeTimer[ciMAX_FACTORY_MATERIAL_UNITS]
ENDSTRUCT

STRUCT BUNKER_DATA
	INT iBS
	INT iBS2
	INT iBSXmas
	BUNKER_SCRIPT_STATE eState
	SIMPLE_INTERIORS eSimpleInteriorID
	PRODUCT_DATA sProductData
	MATERIAL_DATA sMaterialData
	INT iScriptInstance
	INT iInvitingPlayer
	INT iProductSlot
	INT iProductLayout
	INT iMaterialsSlot
	INT iMaterialPercentageSlot
	INT iMaterialInitPercentageSlot
	INT iProductionStageWhenEntering
	INT iDemoProductUpSoundID
	INT iDemoProductDownSoundID
	INT iTotalProductCreated = 0
	INT iFallOverAttempts = 0
	
	STRING sAmbientZone
	BOOL bScriptWasRelaunched
	BOOL bShootingRangeLaunched = FALSE
	
	FACTORY_ID eID
	FACTORY_TYPE eFactoryType
	
	INT iSaveSlot
	
	PLAYER_INDEX pOwner
	
	SIMPLE_CUTSCENE introCutscene
	BOOL bFadeScreenInAfterCutsceneIsDone
	INT iContextTruckEntryID = -1
	INT iProductAnimationStage = -1
	
	BOOL bApplyUpgrade = FALSE
	BUNKER_UPGRADE_STATE iUpgradeState
	INTERIOR_INSTANCE_INDEX BunkerInteriorID
	SCRIPT_TIMER upgradeTimer
	FACTORY_UPGRADE_ID eUpgradeID
	
	BOOL bPlayerLeftTheFactory = FALSE

	SCRIPT_TIMER stPinInMemTimer
	
	INT iSpawnActivityStage, iSpawnActivitySceneID
	STRING strSpawnActivityDict
	SCRIPT_TIMER timerSpawnActivity
		
	STRING sChildOfChildScript
	SCRIPT_TIMER sCarModScriptRunTimer	
	THREADID CarModThread
	
	SCRIPT_TIMER stBunkerSuppliesBarFlash
	SCRIPT_TIMER stBunkerStockBarFlash
	SCRIPT_TIMER stBunkerResearchBarFlash
	BUNKER_LAPTOP_SCREEN_STRUCT bunkerLaptopData
	
	SCRIPT_TIMER failSafeClearVehicleDelay
	
	INT iIntroCutsceneProg
	BOOL bShouldPlayIntroCut
	BOOL bInitLocalPlayerForIntroMocap
	BOOL bForceRoomForIntroCut
	SCRIPT_TIMER timeFailSafe
	SCRIPT_TIMER stCutsceneFinishedFailSafe
	VEHICLE_INDEX vehIntroCutMocTruck
	VEHICLE_INDEX vehIntroCutMocTrailer
	
	SCRIPT_TIMER tOwnerNotOk
	
	OBJECT_INDEX objChristmasTree
	
	BOOL bStrombergChanged = FALSE
ENDSTRUCT

BUNKER_DATA thisBunker

STRUCT BUNKER_TRUCK_DATA
	INT iStage
ENDSTRUCT
BUNKER_TRUCK_DATA thisTruckBunker

BedStructData bedStruct

VEHICLE_INDEX currentVeh
VEHICLE_INDEX lastVeh
BLIP_INDEX bLaptopBlip
BLIP_INDEX bShootingRangeBlip
BLIP_INDEX bModShopBlip
BLIP_INDEX bAATrailerBlip
OBJECT_INDEX obChairBlocker

SCRIPT_TIMER pauseMenuInteractionsDelay

MP_CCTV_LOCAL_DATA_STRUCT MPCCTVLocal

INT iBunkerPickupFlags[MAX_NUM_BUNKER_WEAPON_PICKUPS]

#IF FEATURE_DLC_1_2022
BLIP_INDEX blipDuneloaderCrate

CAMERA_INDEX camDuneloader

FLASH_FADE_STRUCT sDuneloaderFadeData
FLASH_FADE_STRUCT sDuneloaderCrateFadeData

INT iDuneloaderBS = 0
INT iDuneloaderState = 0
INT iDuneloaderContext = NEW_CONTEXT_INTENTION
INT iDuneloaderCutsceneState = 0

OBJECT_INDEX objDuneloaderCrate

VEHICLE_INDEX vehDuneloader
#ENDIF

//VECTOR vTruckCoord = <<848.867, -3236.171, -98.961>>	//<<833.466, -3241.446, -99.103>>
//FLOAT fTruckHeading = 288.000							//103.754
//FLOAT fTrailerHeadingOffset = 30.0						//300.0

#IF IS_DEBUG_BUILD
STRUCT BUNKER_DEBUG_DATA
	BOOL bSpawnRifles01a
	BOOL bSpawnRifles02a
	BOOL bSpawnRifles03a
	BOOL bSpawnRifles04a
	BOOL bSpawnMixed01a
	BOOL bSpawnPistols01a
	BOOL bSpawnGunsmithSupply01a
	BOOL bSpawnGunsmithSupply02a
	BOOL bSpawnGunsmithSupply03a
	BOOL bAddProduct
	BOOL bRemoveProduct
	BOOL bSpawnProduct
	BOOL bClearProduct
	BOOL bHideAllProduct
	BOOL bShowAllProduct
	BOOL bCreateDemoProduct
	BOOL bActivateDemoProduct
	BOOL bDeactivateDemoProduct
	BOOL bDoDemoProduct
	BOOL bSetLayout
	BOOL bGenerateLayout
	BOOL bGenerateRandomLayout
	BOOL bClearLayout
	BOOL bSpawnMaterials
	BOOL bClearMaterials
	BOOL bGiveMaterials
	BOOL bSetResearchCompletedTotal
	BOOL bUnlockResearchItem
	BOOL bUnlockBallistic
	BOOL bUnlockSAMBattery
	
	INT iLayout 				= 1
	INT iProductAmountToAdd		= 1
	INT iProductAmountToRemove  = 1
	INT iProductAmountToSpawn	= 1
	INT iProductAmountToClear  	= 1
	INT iMaterialAmountToSpawn	= 1
	INT iMaterialAmountToClear  = 1
	INT iDemoProductAmount
	INT iDemoProductTime
	INT iNewMaterials
	INT iResearchCompletedTotal
	INT iCurrentResearchTotal
	INT iShootingRangeBS
	INT iCratesSpawnedBS
	
	VECTOR vCrateCoords
	VECTOR vCrateRotation
	
	BOOL bStartSpawnActivity
	
	OBJECT_INDEX oProductObj
	BOOL bTestPlayerFade
ENDSTRUCT
BUNKER_DEBUG_DATA debugData

BOOL bResetPeds

FUNC STRING DEBUG_GET_PLAYER_BROADCAST_BIT_NAME(INT iBIT)
	SWITCH iBit
		CASE BS_BUNKER_DATA_FADE_OUT_TO_ENTER_TRUCK_WITH_VEH	RETURN "BS_BUNKER_DATA_FADE_OUT_TO_ENTER_TRUCK_WITH_VEH"
		CASE BS_BUNKER_MOVE_OUT_OF_WAY							RETURN "BS_BUNKER_MOVE_OUT_OF_WAY"
		CASE BS_BUNKER_WATCHING_INTRO_MOCAP						RETURN "BS_BUNKER_WATCHING_INTRO_MOCAP"
		CASE BS_BUNKER_PERSONAL_QUATRS_PURCHASED				RETURN "BS_BUNKER_PERSONAL_QUATRS_PURCHASED"
		CASE BS_BUNKER_CADDY_STANDARD_PURCHASED					RETURN "BS_BUNKER_CADDY_STANDARD_PURCHASED"
		CASE BS_BUNKER_CADDY_UPGRADED_PURCHASED					RETURN "BS_BUNKER_CADDY_UPGRADED_PURCHASED"
		CASE BS_BUNKER_SHOOTING_RANGE_BLACK_PURCHASED			RETURN "BS_BUNKER_SHOOTING_RANGE_BLACK_PURCHASED"
		CASE BS_BUNKER_SHOOTING_RANGE_WHITE_PURCHASED			RETURN "BS_BUNKER_SHOOTING_RANGE_WHITE_PURCHASED"
		CASE BS_BUNKER_GUN_LOCKER_PURCHASED						RETURN "BS_BUNKER_GUN_LOCKER_PURCHASED"
		CASE BS_BUNKER_BUNKER_STYLE_A_PURCHASED					RETURN "BS_BUNKER_BUNKER_STYLE_A_PURCHASED"
		CASE BS_BUNKER_BUNKER_STYLE_B_PURCHASED					RETURN "BS_BUNKER_BUNKER_STYLE_B_PURCHASED"
		CASE BS_BUNKER_BUNKER_STYLE_C_PURCHASED					RETURN "BS_BUNKER_BUNKER_STYLE_C_PURCHASED"
	ENDSWITCH

	RETURN "***unknown***"
ENDFUNC
#ENDIF

PROC SET_LOCAL_PLAYER_BROADCAST_BIT(INT iBit, BOOL bSet)
	IF bSet
		#IF IS_DEBUG_BUILD
		IF NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, iBit)
			PRINTLN("AM_MP_BUNKER - SET_LOCAL_PLAYER_BROADCAST_BIT setting bit ", DEBUG_GET_PLAYER_BROADCAST_BIT_NAME(iBit))
		#ENDIF
		
			SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, iBit)
			
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		IF IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, iBit)
			PRINTLN("AM_MP_BUNKER - SET_LOCAL_PLAYER_BROADCAST_BIT clearing bit ", DEBUG_GET_PLAYER_BROADCAST_BIT_NAME(iBit))
		#ENDIF
		
			CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, iBit)
			
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
	ENDIF
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════╡ FACTORY PRODUCT ╞══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

FUNC BOOL IS_PLAYER_IN_OR_ENTERING_THIS_BUNKER(PLAYER_INDEX pPlayer)
	
	IF pPlayer = INVALID_PLAYER_INDEX()
		RETURN FALSE
	ENDIF
	
	//Are they entering or in the bunker
	IF GET_SIMPLE_INTERIOR_PLAYER_IS_IN(pPlayer) = thisBunker.eSimpleInteriorID
	OR GET_SIMPLE_INTERIOR_PLAYER_IS_WALKING_IN_OR_OUT_FROM(pPlayer) = thisBunker.eSimpleInteriorID
		
		//Are they entering the bunker owned by this bunkers owner
		IF thisBunker.pOwner != INVALID_PLAYER_INDEX()
		AND globalPlayerBD[NATIVE_TO_INT(pPlayer)].SimpleInteriorBD.propertyOwner = thisBunker.pOwner
			
			//Do they have the same simple interior instance as us
			IF globalPlayerBD[NATIVE_TO_INT(pPlayer)].SimpleInteriorBD.iInstance = globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iInstance
			AND globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iInstance != -1
				RETURN TRUE
			ENDIF
			
		ENDIF
		
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL AM_I_WALKING_INTO_BUNKER_WITH_OWNER()
	IF thisBunker.pOwner != INVALID_PLAYER_INDEX()
		IF thisBunker.pOwner = PLAYER_ID()
			
			//Are we walking into this bunker?
			RETURN IS_LOCAL_PLAYER_WALKING_INTO_THIS_SIMPLE_INTERIOR(thisBunker.eSimpleInteriorID, TRUE)
			
		ELSE
			
			//Are we both entering the simple interior
			IF IS_PLAYER_WALKING_INTO_SIMPLE_INTERIOR(PLAYER_ID())
			AND IS_PLAYER_WALKING_INTO_SIMPLE_INTERIOR(thisBunker.pOwner)
				
				//Are we entering the same simple interior
				RETURN GET_SIMPLE_INTERIOR_PLAYER_IS_WALKING_IN_OR_OUT_FROM(PLAYER_ID()) = GET_SIMPLE_INTERIOR_PLAYER_IS_WALKING_IN_OR_OUT_FROM(thisBunker.pOwner)
				
			ENDIF
			
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_HOST_ASSIGNED_OWNERS_PRODUCTION_MODE()
	RETURN IS_BIT_SET(serverBD.iBS, BS_BUNKER_SERVER_DATA_SET_SERVER_RESEARCH_STATE)
ENDFUNC

FUNC FACTORY_PRODUCTION_MODE GET_HOST_ASSIGNED_PRODUCTION_MODE()
	RETURN serverBD.eprodMode
ENDFUNC

PROC SET_HOST_ASSIGNED_OWNERS_PRODUCTION_MODE(FACTORY_PRODUCTION_MODE eMode)
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		PRINTLN("AM_MP_BUNKER SET_HOST_ASSIGNED_OWNERS_PRODUCTION_MODE - setting mode to: ", eMode)
		SET_BIT(serverBD.iBS, BS_BUNKER_SERVER_DATA_SET_SERVER_RESEARCH_STATE)
		serverBD.eprodMode = eMode
	ELSE
		PRINTLN("AM_MP_BUNKER SET_HOST_ASSIGNED_OWNERS_PRODUCTION_MODE - tried to assign the production mode to: ", eMode, " but we're not the host. The host is: ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(NETWORK_GET_HOST_OF_THIS_SCRIPT())))
	ENDIF
ENDPROC

PROC UPDATE_MATERIAL_PERCENTAGE_SLOT()
	INT iMaterialsTotal = GlobalplayerBD_FM[NATIVE_TO_INT(thisBunker.pOwner)].propertyDetails.bdFactoryData[BUNKER_SAVE_SLOT].iMaterialsTotalBlended
	
	IF iMaterialsTotal > 0 AND iMaterialsTotal <= 20
		thisBunker.iMaterialPercentageSlot = 1
	ELIF iMaterialsTotal > 20 AND iMaterialsTotal <= 40
		thisBunker.iMaterialPercentageSlot = 2
	ELIF iMaterialsTotal > 40 AND iMaterialsTotal <= 60
		thisBunker.iMaterialPercentageSlot = 3
	ELIF iMaterialsTotal > 60 AND iMaterialsTotal <= 80
		thisBunker.iMaterialPercentageSlot = 4
	ELIF iMaterialsTotal > 80 AND iMaterialsTotal <= 100
		thisBunker.iMaterialPercentageSlot = 5
	ELSE
		thisBunker.iMaterialPercentageSlot = 0
	ENDIF
ENDPROC

FUNC BOOL IS_FACTORY_PRODUCT_SLOT_VALID(FACTORY_ID eFactoryID, INT iProductSlot)
	RETURN (iProductSlot > 0 AND iProductSlot <= GET_FACTORY_PRODUCT_CAPACITY(eFactoryID))
ENDFUNC

FUNC BOOL IS_FACTORY_MATERIAL_SLOT_VALID(FACTORY_ID eFactoryID, INT iMaterialSlot)
	RETURN (iMaterialSlot > 0 AND iMaterialSlot <= GET_FACTORY_MATERIAL_PROPS_CAPACITY(eFactoryID))
ENDFUNC

FUNC BOOL IS_FACTORY_PRODUCT_SLOT_AVALIABLE(INT iProductSlot, BOOL bTutorial = FALSE)
	BOOL bAvaliableSlot = FALSE
	IF bTutorial
		IF thisBunker.sProductData.oTUTProductObject[iProductSlot] = NULL
			bAvaliableSlot = TRUE
		ENDIF
	ELSE
		IF thisBunker.sProductData.oProductObject[iProductSlot] = NULL
			bAvaliableSlot = TRUE
		ENDIF
	ENDIF
	RETURN bAvaliableSlot
ENDFUNC

FUNC BOOL IS_FACTORY_MATERIAL_SLOT_AVALIABLE(INT iProductSlot, BOOL bTutorial = FALSE)
	BOOL bAvaliableSlot = FALSE
	IF bTutorial
		IF thisBunker.sMaterialData.oTUTMaterialObject[iProductSlot] = NULL
			bAvaliableSlot = TRUE
		ENDIF
	ELSE
		IF thisBunker.sMaterialData.oMaterialObject[iProductSlot] = NULL
			bAvaliableSlot = TRUE
		ENDIF
	ENDIF
	RETURN bAvaliableSlot
ENDFUNC

FUNC BOOL IS_FACTORY_PRODUCT_READY_TO_FADE(INT iProductSlot)
	IF thisBunker.sProductData.oProductObject[iProductSlot] != NULL
		RETURN (GET_ENTITY_ALPHA(thisBunker.sProductData.oProductObject[iProductSlot]) = 0)
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_FACTORY_MATERIAL_READY_TO_FADE(INT iMaterialSlot)
	IF thisBunker.sMaterialData.oMaterialObject[iMaterialSlot] != NULL
		RETURN (GET_ENTITY_ALPHA(thisBunker.sMaterialData.oMaterialObject[iMaterialSlot]) = 0)
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ANY_PLAYER_BLOCKING_FACTORY_PRODUCT_COORDS(FACTORY_ID eFactoryID, INT iProductSlot)
	
	FLOAT fTolerance = 1.25
	VECTOR vProductCoords = GET_FACTORY_PRODUCT_COORDS(thisBunker.pOwner, eFactoryID, iProductSlot)
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		fTolerance = 3.0
	ENDIF
	
	INT iParticipant
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
		    PLAYER_INDEX playerID = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
		    IF IS_NET_PLAYER_OK(playerID)
				IF ARE_VECTORS_ALMOST_EQUAL(GET_PLAYER_COORDS(playerID), vProductCoords, fTolerance)
					RETURN TRUE
				ENDIF
			ENDIF
	    ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ANY_BUNKER_VEHICLE_BLOCKING_FACTORY_PRODUCT_COORDS(FACTORY_ID eFactoryID, INT iProductSlot)
	VECTOR vProductCoords = GET_FACTORY_PRODUCT_COORDS(thisBunker.pOwner, eFactoryID, iProductSlot)
	
	// Caddies
	INT i
	REPEAT NUMBER_OF_BUNKER_VEHICLES i
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niBunkerVehicles[i])
		AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niBunkerVehicles[i])
			IF DOES_ENTITY_EXIST(NET_TO_VEH(serverBD.niBunkerVehicles[i]))
			AND NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niBunkerVehicles[i]))
				IF ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niBunkerVehicles[i])), vProductCoords, 3.0)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Personal Vehicle
	IF DOES_ENTITY_EXIST(MPGlobals.RemotePV[NATIVE_TO_INT(thisBunker.pOwner)])
	AND NOT IS_ENTITY_DEAD(MPGlobals.RemotePV[NATIVE_TO_INT(thisBunker.pOwner)])
		IF ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(MPGlobals.RemotePV[NATIVE_TO_INT(thisBunker.pOwner)]), vProductCoords, 3.0)
			RETURN TRUE
		ENDIF
	ENDIF
	
	// Pegasus Vehicle
	IF DOES_ENTITY_EXIST(MPGlobals.RemotePegV[NATIVE_TO_INT(thisBunker.pOwner)])
	AND NOT IS_ENTITY_DEAD(MPGlobals.RemotePegV[NATIVE_TO_INT(thisBunker.pOwner)])
		IF ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(MPGlobals.RemotePegV[NATIVE_TO_INT(thisBunker.pOwner)]), vProductCoords, 3.0)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ANY_PLAYER_BLOCKING_FACTORY_MATERIAL_COORDS(FACTORY_ID eFactoryID, INT iMaterialSlot)
	
	FLOAT fTolerance = 1.25
	VECTOR vMaterialCoords = GET_FACTORY_MATERIAL_COORDS(eFactoryID, iMaterialSlot)
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		fTolerance = 3.0
	ENDIF
	
	INT iParticipant
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
		    PLAYER_INDEX playerID = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
		    IF IS_NET_PLAYER_OK(playerID)
				IF ARE_VECTORS_ALMOST_EQUAL(GET_PLAYER_COORDS(playerID), vMaterialCoords, fTolerance)
					RETURN TRUE
				ENDIF
			ENDIF
	    ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ANY_BUNKER_VEHICLE_BLOCKING_FACTORY_MATERIAL_COORDS(FACTORY_ID eFactoryID, INT iMaterialSlot)
	VECTOR vMaterialCoords = GET_FACTORY_MATERIAL_COORDS(eFactoryID, iMaterialSlot)
	
	// Caddies
	INT i
	REPEAT NUMBER_OF_BUNKER_VEHICLES i
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niBunkerVehicles[i])
		AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niBunkerVehicles[i])
			IF DOES_ENTITY_EXIST(NET_TO_VEH(serverBD.niBunkerVehicles[i]))
			AND NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niBunkerVehicles[i]))
				IF ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niBunkerVehicles[i])), vMaterialCoords, 3.0)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Personal Vehicle
	IF DOES_ENTITY_EXIST(MPGlobals.RemotePV[NATIVE_TO_INT(thisBunker.pOwner)])
	AND NOT IS_ENTITY_DEAD(MPGlobals.RemotePV[NATIVE_TO_INT(thisBunker.pOwner)])
		IF ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(MPGlobals.RemotePV[NATIVE_TO_INT(thisBunker.pOwner)]), vMaterialCoords, 3.0)
			RETURN TRUE
		ENDIF
	ENDIF
	
	// Pegasus Vehicle
	IF DOES_ENTITY_EXIST(MPGlobals.RemotePegV[NATIVE_TO_INT(thisBunker.pOwner)])
	AND NOT IS_ENTITY_DEAD(MPGlobals.RemotePegV[NATIVE_TO_INT(thisBunker.pOwner)])
		IF ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(MPGlobals.RemotePegV[NATIVE_TO_INT(thisBunker.pOwner)]), vMaterialCoords, 3.0)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CREATE_FACTORY_PRODUCT(FACTORY_ID eFactoryID, INT iProductSlot, BOOL bProductFadeIn = TRUE, BOOL bTutorial = FALSE #IF IS_DEBUG_BUILD, BOOL bLayoutGeneration = FALSE #ENDIF)
	
	IF (IS_VALID_INTERIOR(GET_INTERIOR_FROM_ENTITY(GET_PLAYER_PED(PLAYER_ID()))))
	AND (IS_INTERIOR_READY(GET_INTERIOR_FROM_ENTITY(GET_PLAYER_PED(PLAYER_ID()))) OR thisBunker.bShouldPlayIntroCut)
	
		VECTOR vProductCoords 		= GET_FACTORY_PRODUCT_COORDS(thisBunker.pOwner, eFactoryID, iProductSlot)
		VECTOR vProductRotation 	= GET_FACTORY_PRODUCT_ROTATION(eFactoryID, iProductSlot)
		MODEL_NAMES eProductModel 	= GET_FACTORY_PRODUCT_MODEL(thisBunker.pOwner, eFactoryID, thisBunker.iProductLayout, iProductSlot #IF IS_DEBUG_BUILD, bLayoutGeneration #ENDIF)
		
		IF IS_MODEL_VALID(eProductModel) AND IS_FACTORY_PRODUCT_SLOT_AVALIABLE(iProductSlot)
			OBJECT_INDEX oProductObj = CREATE_OBJECT(eProductModel, vProductCoords, FALSE, FALSE)
			
			IF DOES_ENTITY_EXIST(oProductObj)
				SET_ENTITY_ROTATION(oProductObj, vProductRotation)
				FREEZE_ENTITY_POSITION(oProductObj, TRUE)
				
				IF bTutorial
					thisBunker.sProductData.oTUTProductObject[iProductSlot] = oProductObj
				ELSE
					thisBunker.sProductData.oProductObject[iProductSlot] = oProductObj
				ENDIF
				
				IF bProductFadeIn
					SET_ENTITY_ALPHA(oProductObj, 0, FALSE)
				ELSE
					IF IS_MODEL_VALID(eProductModel)
						SET_MODEL_AS_NO_LONGER_NEEDED(eProductModel)
					ENDIF
				ENDIF
				
				thisBunker.iTotalProductCreated++
				
				#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_PROPERTY, "[AM_MP_BUNKER] - CREATE_FACTORY_PRODUCT - Creating Product in Slot: ", iProductSlot, " Model: ", ENUM_TO_INT(eProductModel), " Bunker ID: ", eFactoryID)
				#ENDIF
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	thisBunker.iFallOverAttempts++
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CREATE_FACTORY_MATERIAL_PROP(FACTORY_ID eFactoryID, INT iMaterialSlot, BOOL bMaterialFadeIn = TRUE, BOOL bTutorial = FALSE)
	
	IF IS_VALID_INTERIOR(GET_INTERIOR_FROM_ENTITY(GET_PLAYER_PED(PLAYER_ID())))
	AND IS_INTERIOR_READY(GET_INTERIOR_FROM_ENTITY(GET_PLAYER_PED(PLAYER_ID())))
		
		VECTOR vMaterialCoords 		= GET_FACTORY_MATERIAL_COORDS(eFactoryID, iMaterialSlot, thisBunker.iProductLayout)
		VECTOR vMaterialRotation 	= GET_FACTORY_MATERIAL_ROTATION(eFactoryID, iMaterialSlot, thisBunker.iProductLayout)
		MODEL_NAMES eMaterialModel 	= GET_FACTORY_MATERIAL_MODEL(eFactoryID, thisBunker.iProductLayout, iMaterialSlot)
		
		IF IS_MODEL_VALID(eMaterialModel) AND IS_FACTORY_MATERIAL_SLOT_AVALIABLE(iMaterialSlot)
			OBJECT_INDEX oMaterialObj = CREATE_OBJECT(eMaterialModel, vMaterialCoords, FALSE, FALSE)
			
			IF DOES_ENTITY_EXIST(oMaterialObj)
				SET_ENTITY_ROTATION(oMaterialObj, vMaterialRotation)
				FREEZE_ENTITY_POSITION(oMaterialObj, TRUE)
				
				IF bTutorial
					thisBunker.sMaterialData.oTUTMaterialObject[iMaterialSlot] = oMaterialObj
				ELSE
					thisBunker.sMaterialData.oMaterialObject[iMaterialSlot] = oMaterialObj
				ENDIF
				
				IF bMaterialFadeIn
					SET_ENTITY_ALPHA(oMaterialObj, 0, FALSE)
				ELSE
					IF IS_MODEL_VALID(eMaterialModel)
						SET_MODEL_AS_NO_LONGER_NEEDED(eMaterialModel)
					ENDIF
				ENDIF
				
				#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_PROPERTY, "[AM_MP_BUNKER] - CREATE_FACTORY_MATERIAL_PROP - Creating Material in Slot: ", iMaterialSlot, " Model: ", ENUM_TO_INT(eMaterialModel), " Bunker ID: ", eFactoryID)
				#ENDIF
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CREATE_ALL_FACTORY_PRODUCTS(FACTORY_ID iFactoryID, BOOL bProductFadeIn = FALSE #IF IS_DEBUG_BUILD, BOOL bLayoutGeneration = FALSE #ENDIF)
	
	INT iProductSlot
	REPEAT GET_FACTORY_PRODUCT_CAPACITY(iFactoryID) iProductSlot
		IF IS_FACTORY_PRODUCT_SLOT_VALID(iFactoryID, iProductSlot)
			CREATE_FACTORY_PRODUCT(iFactoryID, iProductSlot, bProductFadeIn, DEFAULT #IF IS_DEBUG_BUILD, bLayoutGeneration #ENDIF)
		ENDIF
	ENDREPEAT
	
ENDPROC

PROC CREATE_ALL_FACTORY_MATERIAL_PROPS(FACTORY_ID iFactoryID, BOOL bMaterialFadeIn = FALSE)
	
	INT iMaterialSlot
	REPEAT GET_FACTORY_MATERIAL_PROPS_CAPACITY(iFactoryID) iMaterialSlot
		IF IS_FACTORY_PRODUCT_SLOT_VALID(iFactoryID, iMaterialSlot)
			CREATE_FACTORY_MATERIAL_PROP(iFactoryID, iMaterialSlot, bMaterialFadeIn)
		ENDIF
	ENDREPEAT
	
ENDPROC

PROC CREATE_ALL_OWNED_FACTORY_PRODUCTS(FACTORY_ID eFactoryID, INT iProductTotal, BOOL bCreateInitialProducts = FALSE)
	
	INT iProductSlot
	REPEAT iProductTotal iProductSlot
		IF bCreateInitialProducts
			IF iProductSlot <= ciMAX_BUNKER_INITIAL_PRODUCTS
				IF IS_FACTORY_PRODUCT_SLOT_VALID(eFactoryID, iProductSlot)
					IF NOT IS_ANY_PLAYER_BLOCKING_FACTORY_PRODUCT_COORDS(thisBunker.eID, iProductSlot)
					AND NOT IS_ANY_BUNKER_VEHICLE_BLOCKING_FACTORY_PRODUCT_COORDS(thisBunker.eID, iProductSlot)
						CREATE_FACTORY_PRODUCT(eFactoryID, iProductSlot, FALSE)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF IS_FACTORY_PRODUCT_SLOT_VALID(eFactoryID, iProductSlot)
				IF NOT IS_ANY_PLAYER_BLOCKING_FACTORY_PRODUCT_COORDS(thisBunker.eID, iProductSlot)
				AND NOT IS_ANY_BUNKER_VEHICLE_BLOCKING_FACTORY_PRODUCT_COORDS(thisBunker.eID, iProductSlot)
					CREATE_FACTORY_PRODUCT(eFactoryID, iProductSlot, FALSE)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
ENDPROC

PROC CREATE_ALL_OWNED_FACTORY_MATERIAL_PROPS(FACTORY_ID eFactoryID, INT iMaterialTotal)
	
	INT iMaterialSlot
	FOR iMaterialSlot = 1 TO iMaterialTotal
		IF IS_FACTORY_MATERIAL_SLOT_VALID(eFactoryID, iMaterialSlot)
			IF NOT IS_ANY_PLAYER_BLOCKING_FACTORY_MATERIAL_COORDS(thisBunker.eID, iMaterialSlot)
			AND NOT IS_ANY_BUNKER_VEHICLE_BLOCKING_FACTORY_MATERIAL_COORDS(thisBunker.eID, iMaterialSlot)
				CREATE_FACTORY_MATERIAL_PROP(eFactoryID, iMaterialSlot, FALSE)
			ENDIF
		ENDIF
	ENDFOR
	
ENDPROC

FUNC BOOL CLEANUP_FACTORY_PRODUCT(INT iProductSlot, BOOL bUpdateProductSlot = TRUE, BOOL bTutorial = FALSE)
	OBJECT_INDEX oProductObj
	
	IF bTutorial
		oProductObj = thisBunker.sProductData.oTUTProductObject[iProductSlot]
	ELSE
		oProductObj = thisBunker.sProductData.oProductObject[iProductSlot]
	ENDIF
	
	IF DOES_ENTITY_EXIST(oProductObj)
		DELETE_OBJECT(oProductObj)
		
		IF bTutorial
			thisBunker.sProductData.oTUTProductObject[iProductSlot] = NULL
		ELSE
			thisBunker.sProductData.oProductObject[iProductSlot] = NULL
		ENDIF
		
		IF NETWORK_IS_GAME_IN_PROGRESS() // B* 3512422
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT() AND bTutorial AND bUpdateProductSlot
				serverBD.iTUTProductSlot--
			ENDIF
		ENDIF
		
		IF bUpdateProductSlot
			IF thisBunker.iProductSlot > 0
				thisBunker.iProductSlot--
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
		PRINTLN("[AM_MP_BUNKER] CLEANUP_FACTORY_PRODUCT - thisBunker.iProductSlot: ", thisBunker.iProductSlot)
		#ENDIF
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CLEANUP_FACTORY_MATERIAL_PROPS(INT iMaterialSlot, BOOL bUpdateMaterialSlot = TRUE, BOOL bTutorial = FALSE)
	OBJECT_INDEX oMaterialObj
	
	IF bTutorial
		oMaterialObj = thisBunker.sMaterialData.oTUTMaterialObject[iMaterialSlot]
	ELSE
		oMaterialObj = thisBunker.sMaterialData.oMaterialObject[iMaterialSlot]
	ENDIF
	
	IF DOES_ENTITY_EXIST(oMaterialObj) AND NETWORK_HAS_CONTROL_OF_ENTITY(oMaterialObj)
		DELETE_OBJECT(oMaterialObj)
		
		IF bTutorial
			thisBunker.sMaterialData.oTUTMaterialObject[iMaterialSlot] = NULL
		ELSE
			thisBunker.sMaterialData.oMaterialObject[iMaterialSlot] = NULL
		ENDIF
		
		IF NETWORK_IS_GAME_IN_PROGRESS() // B* 3512422
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT() AND bTutorial AND bUpdateMaterialSlot
				serverBD.iTUTMaterialSlot--
			ENDIF
		ENDIF
		
		IF bUpdateMaterialSlot
			IF thisBunker.iMaterialsSlot > 0
				thisBunker.iMaterialsSlot--
			ENDIF
			IF thisBunker.iMaterialInitPercentageSlot > 0
				thisBunker.iMaterialInitPercentageSlot--
			ENDIF
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CLEANUP_ALL_FACTORY_PRODUCTS(FACTORY_ID iFactoryID, BOOL bKeepInitialProducts = FALSE)
	
	INT iProductSlot
	REPEAT GET_FACTORY_PRODUCT_CAPACITY(iFactoryID) iProductSlot
		IF bKeepInitialProducts
			IF iProductSlot > ciMAX_BUNKER_INITIAL_PRODUCTS
				CLEANUP_FACTORY_PRODUCT(iProductSlot)
			ENDIF
		ELSE
			CLEANUP_FACTORY_PRODUCT(iProductSlot)
		ENDIF
	ENDREPEAT
	
ENDPROC

PROC CLEANUP_ALL_FACTORY_MATERIAL_PROPS(FACTORY_ID iFactoryID)
	
	INT iMaterialSlot
	REPEAT GET_FACTORY_MATERIAL_PROPS_CAPACITY(iFactoryID) iMaterialSlot
		CLEANUP_FACTORY_MATERIAL_PROPS(iMaterialSlot)
	ENDREPEAT
	
ENDPROC

PROC CLEANUP_ALL_OWNED_FACTORY_PRODUCTS(INT iProductTotal)
	
	INT iProductSlot
	REPEAT iProductTotal iProductSlot
		CLEANUP_FACTORY_PRODUCT(iProductSlot)
	ENDREPEAT
	
ENDPROC

PROC CLEANUP_ALL_OWNED_FACTORY_MATERIAL_PROPS(INT iMaterialTotal)
	
	INT iMaterialSlot
	REPEAT iMaterialTotal iMaterialSlot
		CLEANUP_FACTORY_MATERIAL_PROPS(iMaterialSlot)
	ENDREPEAT
	
ENDPROC

FUNC BOOL IS_FACTORY_PRODUCT_CURRENTLY_FADING(INT iProductSlot, BOOL bTutorial = FALSE)
	
	IF IS_FACTORY_PRODUCT_SLOT_VALID(thisBunker.eID, iProductSlot)
		IF NOT IS_FACTORY_PRODUCT_SLOT_AVALIABLE(iProductSlot, bTutorial)
			INT iAlpha
			
			IF bTutorial
				iAlpha = GET_ENTITY_ALPHA(thisBunker.sProductData.oTUTProductObject[iProductSlot])
			ELSE
				iAlpha = GET_ENTITY_ALPHA(thisBunker.sProductData.oProductObject[iProductSlot])
			ENDIF
			
			IF iAlpha > 0 AND iAlpha < 255
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_FACTORY_MATERIAL_CURRENTLY_FADING(INT iMaterialSlot, BOOL bTutorial = FALSE)
	
	IF IS_FACTORY_MATERIAL_SLOT_VALID(thisBunker.eID, iMaterialSlot)
		IF NOT IS_FACTORY_MATERIAL_SLOT_AVALIABLE(iMaterialSlot, bTutorial)
			INT iAlpha
			
			IF bTutorial
				iAlpha = GET_ENTITY_ALPHA(thisBunker.sMaterialData.oTUTMaterialObject[iMaterialSlot])
			ELSE
				iAlpha = GET_ENTITY_ALPHA(thisBunker.sMaterialData.oMaterialObject[iMaterialSlot])
			ENDIF
			
			IF iAlpha > 0 AND iAlpha < 255
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_FACTORY_PRODUCT_FADE_TIMER(INT iProductSlot)
	IF thisBunker.sProductData.iSetProductFadeTimer[iProductSlot] = 0
		thisBunker.sProductData.tdProductFadeTimer[iProductSlot] = GET_TIME_OFFSET(GET_NETWORK_TIME(), ciBIKER_PRODUCT_FADE_TIME)
		thisBunker.sProductData.iSetProductFadeTimer[iProductSlot] = 1
	ENDIF
ENDPROC

PROC SET_FACTORY_MATERIAL_FADE_TIMER(INT iMaterialSlot)
	IF thisBunker.sMaterialData.iSetMaterialFadeTimer[iMaterialSlot] = 0
		thisBunker.sMaterialData.tdMaterialFadeTimer[iMaterialSlot] = GET_TIME_OFFSET(GET_NETWORK_TIME(), ciBIKER_PRODUCT_FADE_TIME)
		thisBunker.sMaterialData.iSetMaterialFadeTimer[iMaterialSlot] = 1
	ENDIF
ENDPROC

PROC UPDATE_FACTORY_PRODUCT_FADE(OBJECT_INDEX &oProductObject, INT iProductSlot, BOOL bFadeIn)

	IF DOES_ENTITY_EXIST(oProductObject)
		INT iTimeDifference = ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), thisBunker.sProductData.tdProductFadeTimer[iProductSlot]))
		INT iAlpha = ROUND((TO_FLOAT(iTimeDifference) / ciBIKER_PRODUCT_FADE_TIME) * 255)
		
		IF bFadeIn
			iAlpha = (255 - (ROUND((TO_FLOAT(iTimeDifference) / ciBIKER_PRODUCT_FADE_TIME) * 255)))
		ENDIF
		
		SET_ENTITY_ALPHA(oProductObject, iAlpha, FALSE)
	ENDIF
	
ENDPROC

PROC UPDATE_FACTORY_MATERIAL_FADE(OBJECT_INDEX &oMaterialObject, INT iMaterialSlot, BOOL bFadeIn)

	IF DOES_ENTITY_EXIST(oMaterialObject)
		INT iTimeDifference = ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), thisBunker.sMaterialData.tdMaterialFadeTimer[iMaterialSlot]))
		INT iAlpha = ROUND((TO_FLOAT(iTimeDifference) / ciBIKER_PRODUCT_FADE_TIME) * 255)
		
		IF bFadeIn
			iAlpha = (255 - (ROUND((TO_FLOAT(iTimeDifference) / ciBIKER_PRODUCT_FADE_TIME) * 255)))
		ENDIF
		
		SET_ENTITY_ALPHA(oMaterialObject, iAlpha, FALSE)
	ENDIF
	
ENDPROC

PROC FINALISE_FACTORY_PRODUCT_FADE(OBJECT_INDEX &oProductObject, INT iProductSlot, BOOL bFadeIn, BOOL bUpdateProductSlot)
	
	IF DOES_ENTITY_EXIST(oProductObject)
		IF bFadeIn
			SET_ENTITY_ALPHA(oProductObject, 255, FALSE)
			IF IS_MODEL_VALID(GET_ENTITY_MODEL(oProductObject))
				SET_MODEL_AS_NO_LONGER_NEEDED(GET_ENTITY_MODEL(oProductObject))
			ENDIF
			
			thisBunker.sProductData.iProductFadeComplete[iProductSlot] = 1
			IF bUpdateProductSlot
				thisBunker.iProductSlot++
			ENDIF
		ELSE
			SET_ENTITY_ALPHA(oProductObject, 0, FALSE)
			IF NETWORK_HAS_CONTROL_OF_ENTITY(oProductObject)
				CLEANUP_FACTORY_PRODUCT(iProductSlot, bUpdateProductSlot)
			ENDIF
			
			thisBunker.sProductData.iProductFadeComplete[iProductSlot] = 0
		ENDIF
	ENDIF
	
ENDPROC

PROC FINALISE_FACTORY_MATERIAL_FADE(OBJECT_INDEX &oMaterialObject, INT iMaterialSlot, BOOL bFadeIn, BOOL bUpdateMaterialSlot)
	
	IF DOES_ENTITY_EXIST(oMaterialObject)
		IF bFadeIn
			SET_ENTITY_ALPHA(oMaterialObject, 255, FALSE)
			IF IS_MODEL_VALID(GET_ENTITY_MODEL(oMaterialObject))
				SET_MODEL_AS_NO_LONGER_NEEDED(GET_ENTITY_MODEL(oMaterialObject))
			ENDIF
			
			thisBunker.sMaterialData.iMaterialFadeComplete[iMaterialSlot] = 1
			IF bUpdateMaterialSlot
				thisBunker.iMaterialsSlot++
				thisBunker.iMaterialInitPercentageSlot++
			ENDIF
		ELSE
			SET_ENTITY_ALPHA(oMaterialObject, 0, FALSE)
			IF NETWORK_HAS_CONTROL_OF_ENTITY(oMaterialObject)
				CLEANUP_FACTORY_MATERIAL_PROPS(iMaterialSlot, bUpdateMaterialSlot)
			ENDIF
			
			thisBunker.sMaterialData.iMaterialFadeComplete[iMaterialSlot] = 0
		ENDIF
	ENDIF
	
ENDPROC

PROC PERFORM_FACTORY_PRODUCT_FADE(INT iProductSlot, BOOL bFadeIn, BOOL bUpdateProductSlot = TRUE)
	SET_FACTORY_PRODUCT_FADE_TIMER(iProductSlot)
	
	IF IS_TIME_LESS_THAN(GET_NETWORK_TIME(), thisBunker.sProductData.tdProductFadeTimer[iProductSlot])
		OBJECT_INDEX oProductObject = thisBunker.sProductData.oProductObject[iProductSlot]
		UPDATE_FACTORY_PRODUCT_FADE(oProductObject, iProductSlot, bFadeIn)
	ELSE
		OBJECT_INDEX oProductObject = thisBunker.sProductData.oProductObject[iProductSlot]
		FINALISE_FACTORY_PRODUCT_FADE(oProductObject, iProductSlot, bFadeIn, bUpdateProductSlot)
		thisBunker.sProductData.iSetProductFadeTimer[iProductSlot] = 0
	ENDIF
	
ENDPROC

PROC PERFORM_FACTORY_MATERIAL_FADE(INT iMaterialSlot, BOOL bFadeIn, BOOL bUpdateMaterialSlot = TRUE)
	SET_FACTORY_MATERIAL_FADE_TIMER(iMaterialSlot)
	
	IF IS_TIME_LESS_THAN(GET_NETWORK_TIME(), thisBunker.sMaterialData.tdMaterialFadeTimer[iMaterialSlot])
		OBJECT_INDEX oMaterialObject = thisBunker.sMaterialData.oMaterialObject[iMaterialSlot]
		UPDATE_FACTORY_MATERIAL_FADE(oMaterialObject, iMaterialSlot, bFadeIn)
	ELSE
		OBJECT_INDEX oMaterialObject = thisBunker.sMaterialData.oMaterialObject[iMaterialSlot]
		FINALISE_FACTORY_MATERIAL_FADE(oMaterialObject, iMaterialSlot, bFadeIn, bUpdateMaterialSlot)
		thisBunker.sMaterialData.iSetMaterialFadeTimer[iMaterialSlot] = 0
	ENDIF
	
ENDPROC

FUNC BOOL DOES_FACTORY_PRODUCT_TOTAL_NEED_UPDATING(BOOL bTutorial = FALSE)
	BOOL bUpdate = FALSE
	IF bTutorial
		IF serverBD.iTUTProductSlot != serverBD.ITUTProductTotal
			bUpdate = TRUE
		ENDIF
	ELSE
		IF thisBunker.iProductSlot != GET_PRODUCT_TOTAL_FOR_FACTORY(thisBunker.pOwner, thisBunker.eID)
			bUpdate = TRUE
		ENDIF
	ENDIF
	RETURN bUpdate
ENDFUNC

FUNC BOOL HAS_FACTORY_PRODUCT_TOTAL_INCREASED(BOOL bTutorial = FALSE)
	BOOL bIncreased = FALSE
	IF bTutorial
		IF serverBD.iTUTProductSlot < serverBD.ITUTProductTotal
			bIncreased = TRUE
		ENDIF
	ELSE
		IF (thisBunker.iProductSlot < GET_PRODUCT_TOTAL_FOR_FACTORY(thisBunker.pOwner, thisBunker.eID))
			bIncreased = TRUE
		ENDIF
	ENDIF
	RETURN bIncreased
ENDFUNC

FUNC BOOL HAS_FACTORY_PRODUCT_TOTAL_DECREASED(BOOL bTutorial = FALSE)
	BOOL bDecreased = FALSE
	IF bTutorial
		IF serverBD.iTUTProductSlot > serverBD.ITUTProductTotal
			bDecreased = TRUE
		ENDIF
	ELSE
		IF (thisBunker.iProductSlot > GET_PRODUCT_TOTAL_FOR_FACTORY(thisBunker.pOwner, thisBunker.eID))
			bDecreased = TRUE
		ENDIF
	ENDIF
	RETURN bDecreased
ENDFUNC

FUNC BOOL DOES_FACTORY_MATERIALS_TOTAL_NEED_UPDATING(BOOL bTutorial = FALSE)
	IF thisBunker.pOwner = INVALID_PLAYER_INDEX()
		RETURN FALSE
	ENDIF
	
	BOOL bUpdate = FALSE
	IF bTutorial
		IF serverBD.iTUTMaterialSlot != serverBD.ITUTMaterialTotal
			bUpdate = TRUE
		ENDIF
	ELSE
		IF thisBunker.iMaterialsSlot != GlobalplayerBD_FM[NATIVE_TO_INT(thisBunker.pOwner)].propertyDetails.bdFactoryData[BUNKER_SAVE_SLOT].iMaterialsTotalBlended
			bUpdate = TRUE
		ENDIF
	ENDIF
	RETURN bUpdate
ENDFUNC

FUNC BOOL DOES_FACTORY_MATERIALS_PERCENTAGE_SLOT_NEED_UPDATING(BOOL bTutorial = FALSE)
	BOOL bUpdate = FALSE
	IF bTutorial
		IF serverBD.iTUTMaterialSlot != serverBD.ITUTMaterialTotal
			bUpdate = TRUE
		ENDIF
	ELSE
		IF thisBunker.iMaterialInitPercentageSlot != thisBunker.iMaterialPercentageSlot
			bUpdate = TRUE
		ENDIF
	ENDIF
	RETURN bUpdate
ENDFUNC

FUNC BOOL HAS_FACTORY_MATERIAL_TOTAL_INCREASED(BOOL bTutorial = FALSE)
	BOOL bIncreased = FALSE
	IF bTutorial
		IF serverBD.iTUTMaterialSlot < serverBD.ITUTMaterialTotal
			bIncreased = TRUE
		ENDIF
	ELSE
		IF (thisBunker.iMaterialPercentageSlot > thisBunker.iMaterialInitPercentageSlot)
			bIncreased = TRUE
		ENDIF
	ENDIF
	RETURN bIncreased
ENDFUNC

FUNC BOOL HAS_FACTORY_MATERIAL_TOTAL_DECREASED(BOOL bTutorial = FALSE)
	BOOL bDecreased = FALSE
	IF bTutorial
		IF serverBD.iTUTMaterialSlot > serverBD.ITUTMaterialTotal
			bDecreased = TRUE
		ENDIF
	ELSE
		IF (thisBunker.iMaterialPercentageSlot < thisBunker.iMaterialInitPercentageSlot)
			bDecreased = TRUE
		ENDIF
	ENDIF
	RETURN bDecreased
ENDFUNC

PROC ADD_FACTORY_PRODUCT()
	
	IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<836.0,-3238.5,-94.382935>>, <<905.5,-3238.5,-100.1>>, 27.0) // Init product
	AND (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<905.0,-3238.5,-94.382935>>, <<953.5,-3238.5,-100.1>>, 27.0) // Entrance corridor
	OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<916.1,-3238.5,-94.382935>>, <<948.1,-3189.0,-100.1>>, 50.0) // Main storage bay
	OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<887.3,-3226.2,-94.382935>>, <<918.4,-3213.2,-100.1>>, 20.0) // First product creation room
	OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<897.5,-3197.0,-94.382935>>, <<911.7,-3228.2,-100.1>>, 10.0)) // First product creation room
	AND NOT IS_BIT_SET(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_SHOOTING_RANGE_TARGETS_SPAWNED)
		
		IF NOT IS_FACTORY_PRODUCT_CURRENTLY_FADING(thisBunker.iProductSlot)
			IF IS_FACTORY_PRODUCT_SLOT_VALID(thisBunker.eID, thisBunker.iProductSlot+1)
				IF NOT IS_ANY_PLAYER_BLOCKING_FACTORY_PRODUCT_COORDS(thisBunker.eID, thisBunker.iProductSlot+1)
				AND NOT IS_ANY_BUNKER_VEHICLE_BLOCKING_FACTORY_PRODUCT_COORDS(thisBunker.eID, thisBunker.iProductSlot+1)
					CREATE_FACTORY_PRODUCT(thisBunker.eID, thisBunker.iProductSlot+1)
					
					IF NOT IS_FACTORY_PRODUCT_SLOT_AVALIABLE(thisBunker.iProductSlot+1)
//					AND IS_FACTORY_PRODUCT_READY_TO_FADE(thisBunker.iProductSlot+1)
						PERFORM_FACTORY_PRODUCT_FADE(thisBunker.iProductSlot+1, TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC REMOVE_FACTORY_PRODUCT()
	
	IF NOT IS_FACTORY_PRODUCT_CURRENTLY_FADING(thisBunker.iProductSlot+1)
		IF IS_FACTORY_PRODUCT_SLOT_VALID(thisBunker.eID, thisBunker.iProductSlot)
			PERFORM_FACTORY_PRODUCT_FADE(thisBunker.iProductSlot, FALSE)
		ENDIF
	ENDIF
	
ENDPROC

PROC ADD_FACTORY_MATERIALS()
	
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<836.0,-3238.5,-94.382935>>, <<953.5,-3238.5,-100.1>>, 25.0) // Entrance corridor
	OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<900.466553,-3226.168213,-99.278641>>, <<908.3105, -3218.3540, -96.5334>>, 8.0) // Research corridor
	
		IF NOT IS_FACTORY_MATERIAL_CURRENTLY_FADING(thisBunker.iMaterialInitPercentageSlot)
			IF IS_FACTORY_MATERIAL_SLOT_VALID(thisBunker.eID, thisBunker.iMaterialInitPercentageSlot+1)
				IF NOT IS_ANY_PLAYER_BLOCKING_FACTORY_MATERIAL_COORDS(thisBunker.eID, thisBunker.iMaterialInitPercentageSlot+1)
				AND NOT IS_ANY_BUNKER_VEHICLE_BLOCKING_FACTORY_MATERIAL_COORDS(thisBunker.eID, thisBunker.iMaterialInitPercentageSlot+1)
					CREATE_FACTORY_MATERIAL_PROP(thisBunker.eID, thisBunker.iMaterialInitPercentageSlot+1)
					
					IF NOT IS_FACTORY_MATERIAL_SLOT_AVALIABLE(thisBunker.iMaterialInitPercentageSlot+1)
//					AND IS_FACTORY_MATERIAL_READY_TO_FADE(thisBunker.iMaterialInitPercentageSlot+1)
						PERFORM_FACTORY_MATERIAL_FADE(thisBunker.iMaterialInitPercentageSlot+1, TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	
	ENDIF
	
ENDPROC

PROC REMOVE_FACTORY_MATERIALS()
	
	IF NOT IS_FACTORY_MATERIAL_CURRENTLY_FADING(thisBunker.iMaterialInitPercentageSlot+1)
		IF IS_FACTORY_MATERIAL_SLOT_VALID(thisBunker.eID, thisBunker.iMaterialInitPercentageSlot)
			PERFORM_FACTORY_MATERIAL_FADE(thisBunker.iMaterialInitPercentageSlot, FALSE)
		ENDIF
	ENDIF
	
ENDPROC

FUNC STRING GET_AMBIENT_ZONE_FOR_THIS_FACTORY()
	STRING sAmbientZone  = "AZ_DLC_GR_Bunker_Zone_00"
	
	IF ARE_WORKERS_WORKING_IN_THIS_FACTORY(thisBunker.pOwner, thisBunker.eID, TRUE)
		BOOL bOwnsEquipmentUpgrade = DOES_PLAYER_OWN_FACTORY_UPGRADE(thisBunker.pOwner, thisBunker.eID, UPGRADE_ID_EQUIPMENT)
		
		IF GET_GOODS_CATEGORY_FROM_FACTORY_ID(thisBunker.eID) = FACTORY_TYPE_WEAPONS
			IF bOwnsEquipmentUpgrade
				sAmbientZone = "AZ_DLC_GR_Bunker_Zone_01"
			ENDIF
			
			IF IS_FACTORY_PRODUCTION_ACTIVE(thisBunker.pOwner, thisBunker.iSaveSlot)
				sAmbientZone = "AZ_DLC_GR_Bunker_Zone_02"
			ENDIF
			
			IF IS_FACTORY_PRODUCTION_ACTIVE(thisBunker.pOwner, thisBunker.iSaveSlot)
			AND bOwnsEquipmentUpgrade
				sAmbientZone = "AZ_DLC_GR_Bunker_Zone_03"
			ENDIF
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	PRINTLN("[AM_MP_BUNKER] - GET_AMBIENT_ZONE_FOR_THIS_FACTORY - sAmbientZone: ", sAmbientZone)
	#ENDIF
	
	RETURN sAmbientZone
ENDFUNC

PROC INITIALISE_FACTORY_AUDIO()

	STRING sAmbientZone = GET_AMBIENT_ZONE_FOR_THIS_FACTORY()
	
	IF IS_BIT_SET(thisBunker.iBS, BS_BUNKER_DATA_AMBIENT_ZONE_SET)
	AND NOT IS_STRING_NULL_OR_EMPTY(thisBunker.sAmbientZone)
	AND NOT ARE_STRINGS_EQUAL(sAmbientZone, thisBunker.sAmbientZone)
		SET_AMBIENT_ZONE_STATE(thisBunker.sAmbientZone, FALSE, TRUE)
		CLEAR_BIT(thisBunker.iBS, BS_BUNKER_DATA_AMBIENT_ZONE_SET)
		thisBunker.sAmbientZone = sAmbientZone
		
		#IF IS_DEBUG_BUILD
		PRINTLN("[AM_MP_BUNKER] - INITIALISE_FACTORY_AUDIO - Stopping Prev Ambient Zone: ", thisBunker.sAmbientZone, " & Starting: ", sAmbientZone)
		#ENDIF
	ENDIF
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sAmbientZone)
	AND NOT IS_BIT_SET(thisBunker.iBS, BS_BUNKER_DATA_AMBIENT_ZONE_SET)
		thisBunker.sAmbientZone = sAmbientZone
		SET_AMBIENT_ZONE_STATE(thisBunker.sAmbientZone, TRUE, TRUE)
		SET_BIT(thisBunker.iBS, BS_BUNKER_DATA_AMBIENT_ZONE_SET)
		
		#IF IS_DEBUG_BUILD
		PRINTLN("[AM_MP_BUNKER] - INITIALISE_FACTORY_AUDIO - Starting: ", thisBunker.sAmbientZone)
		#ENDIF
	ENDIF
	
ENDPROC

PROC MAINTAIN_BUNKER_AMBIENT_ZONES()
	IF NOT IS_BIT_SET(thisBunker.iBS, BS_BUNKER_DATA_MACHINE_AMBIENT_ZONE_SET)
		IF ARE_WORKERS_WORKING_IN_THIS_FACTORY(thisBunker.pOwner, thisBunker.eID, TRUE)
		AND IS_FACTORY_PRODUCTION_ACTIVE(thisBunker.pOwner, thisBunker.iSaveSlot)
			SET_AMBIENT_ZONE_STATE("AZ_DLC_GR_Bunker_Machinery_Zone", TRUE, TRUE)
			SET_BIT(thisBunker.iBS, BS_BUNKER_DATA_MACHINE_AMBIENT_ZONE_SET)
			PRINTLN("[AM_MP_BUNKER] - MAINTAIN_BUNKER_AMBIENT_ZONES - Starting")
		ENDIF
	ELSE
		IF NOT ARE_WORKERS_WORKING_IN_THIS_FACTORY(thisBunker.pOwner, thisBunker.eID, TRUE)
		AND NOT IS_FACTORY_PRODUCTION_ACTIVE(thisBunker.pOwner, thisBunker.iSaveSlot)
			SET_AMBIENT_ZONE_STATE("AZ_DLC_GR_Bunker_Machinery_Zone", FALSE, TRUE)
			CLEAR_BIT(thisBunker.iBS, BS_BUNKER_DATA_MACHINE_AMBIENT_ZONE_SET)
			PRINTLN("[AM_MP_BUNKER] - MAINTAIN_BUNKER_AMBIENT_ZONES - Stopping")
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_BUNKER_AUDIO_BANKS()
	IF NOT IS_BIT_SET(thisBunker.iBS2, BS_2_BUNKER_DATA_REQUESTED_AUDIO_BANK_1)
		IF REQUEST_SCRIPT_AUDIO_BANK("DLC_GUNRUNNING/Interior_Bunker_01")
			CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_BUNKER - MAINTAIN_BUNKER_AUDIO_BANKS - Audio Bank Requested: DLC_GUNRUNNING/Interior_Bunker_01")
			SET_BIT(thisBunker.iBS2, BS_2_BUNKER_DATA_REQUESTED_AUDIO_BANK_1)
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(thisBunker.iBS2, BS_2_BUNKER_DATA_REQUESTED_AUDIO_BANK_2)
		IF REQUEST_SCRIPT_AUDIO_BANK("DLC_GUNRUNNING/Interior_Bunker_02")
			CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_BUNKER - MAINTAIN_BUNKER_AUDIO_BANKS - Audio Bank Requested: DLC_GUNRUNNING/Interior_Bunker_02")
			SET_BIT(thisBunker.iBS2, BS_2_BUNKER_DATA_REQUESTED_AUDIO_BANK_2)
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(thisBunker.iBS2, BS_2_BUNKER_DATA_REQUESTED_AUDIO_BANK_3)
		IF REQUEST_SCRIPT_AUDIO_BANK("DLC_GUNRUNNING/Interior_Bunker_03")
			CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_BUNKER - MAINTAIN_BUNKER_AUDIO_BANKS - Audio Bank Requested: DLC_GUNRUNNING/Interior_Bunker_03")
			SET_BIT(thisBunker.iBS2, BS_2_BUNKER_DATA_REQUESTED_AUDIO_BANK_3)
		ENDIF
	ENDIF
ENDPROC

PROC CLEANUP_FACTORY_AUDIO()
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_GUNRUNNING/Interior_Bunker_01")
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_GUNRUNNING/Interior_Bunker_02")
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_GUNRUNNING/Interior_Bunker_03")
	
	CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_BUNKER - CLEANUP_FACTORY_AUDIO - Audio Banks Released")
	
	IF IS_BIT_SET(thisBunker.iBS, BS_BUNKER_DATA_AMBIENT_ZONE_SET)
	AND NOT IS_STRING_NULL_OR_EMPTY(thisBunker.sAmbientZone)
		SET_AMBIENT_ZONE_STATE(thisBunker.sAmbientZone, FALSE, TRUE)
		CLEAR_BIT(thisBunker.iBS, BS_BUNKER_DATA_AMBIENT_ZONE_SET)
		
		#IF IS_DEBUG_BUILD
		PRINTLN("[AM_MP_BUNKER] - CLEANUP_FACTORY_AUDIO - Disabling Ambient Zone: ", thisBunker.sAmbientZone)
		#ENDIF
	ENDIF
	
	IF IS_BIT_SET(thisBunker.iBS, BS_BUNKER_DATA_MACHINE_AMBIENT_ZONE_SET)
		SET_AMBIENT_ZONE_STATE("AZ_DLC_GR_Bunker_Machinery_Zone", FALSE, TRUE)
		CLEAR_BIT(thisBunker.iBS, BS_BUNKER_DATA_MACHINE_AMBIENT_ZONE_SET)
		PRINTLN("[AM_MP_BUNKER] - CLEANUP_FACTORY_AUDIO - Stopping AZ_DLC_GR_Bunker_Machinery_Zone")
	ENDIF
ENDPROC

FUNC BOOL IS_BUNKER_PICKUP_SAFE_TO_COLLECT(INT iBunkerPickup)
	SWITCH iBunkerPickup
		CASE 0
			RETURN TRUE
		BREAK
		DEFAULT
			IF thisBunker.pOwner != INVALID_PLAYER_INDEX()
				RETURN IS_BIT_SET(PlayerBD[NATIVE_TO_INT(thisBunker.pOwner)].iShootingRangePickupUnlocks, iBunkerPickup)
			ENDIF
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC GET_BUNKER_WEAPON_PICKUP_DATA(INT iBunkerPickup, PICKUP_TYPE &ePickupType, VECTOR &vPos, VECTOR &vRot, INT &iCount)
	SWITCH iBunkerPickup
		CASE 0
			vPos = << 868.30, -3189.70, -97.24 >>
			vRot = << 0.0, 0.0, 0.0 >>
			ePickupType = PICKUP_WEAPON_MOLOTOV
			iCount = (GET_MAX_AMMO_IN_CLIP(PLAYER_PED_ID(), WEAPONTYPE_DLC_HOMINGLAUNCHER, FALSE) * 2)
		BREAK
		CASE 1
			vPos = << 865.80, -3190.48, -97.16 >>
			vRot = << 0.0, 0.0, 0.0 >>
			ePickupType = PICKUP_WEAPON_GRENADE
			iCount = (GET_MAX_AMMO_IN_CLIP(PLAYER_PED_ID(), WEAPONTYPE_DLC_PROXMINE, FALSE) * 2)
		BREAK
		CASE 2
			vPos = << 864.89, -3188.46, -97.15 >>
			vRot = << 0.0, 0.0, 0.0 >>
			ePickupType = PICKUP_WEAPON_GRENADELAUNCHER
			iCount = (GET_MAX_AMMO_IN_CLIP(PLAYER_PED_ID(), WEAPONTYPE_GRENADELAUNCHER, FALSE) * 2)
		BREAK
		CASE 3
			vPos = << 864.39, -3186.14, -97.16 >>
			vRot = << 0.0, 0.0, 0.0 >>
			ePickupType = PICKUP_WEAPON_RPG
			iCount = (GET_MAX_AMMO_IN_CLIP(PLAYER_PED_ID(), WEAPONTYPE_RPG, FALSE) * 2)
		BREAK
	ENDSWITCH
ENDPROC

PROC MAINTAIN_BUNKER_PICKUPS()

	PlayerBD[NATIVE_TO_INT(PLAYER_ID())].iShootingRangePickupUnlocks = 0
	IF thisBunker.pOwner != INVALID_PLAYER_INDEX()
	AND PLAYER_ID() = thisBunker.pOwner
		IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_SR_TIER_1_REWARD)
			SET_BIT(PlayerBD[NATIVE_TO_INT(PLAYER_ID())].iShootingRangePickupUnlocks, 1)
		ENDIF
		IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_SR_INCREASE_THROW_CAP)
			SET_BIT(PlayerBD[NATIVE_TO_INT(PLAYER_ID())].iShootingRangePickupUnlocks, 2)
		ENDIF
		IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_SR_TIER_3_REWARD)
			SET_BIT(PlayerBD[NATIVE_TO_INT(PLAYER_ID())].iShootingRangePickupUnlocks, 3)
		ENDIF
	ENDIF
	
	INT iBunkerPickup
	PICKUP_TYPE ePickupType
	VECTOR vPos, vRot
	INT iCount
	
	VECTOR vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
	
	BOOL bDisplayHelp = FALSE
	
	REPEAT MAX_NUM_BUNKER_WEAPON_PICKUPS iBunkerPickup
		IF DOES_PICKUP_EXIST(g_pickupBunkerWeapon[iBunkerPickup])
			IF HAS_PICKUP_BEEN_COLLECTED(g_pickupBunkerWeapon[iBunkerPickup])
				g_iLastBunkerWeaponCreateTime[iBunkerPickup] = GET_CLOUD_TIME_AS_INT()
				REMOVE_PICKUP(g_pickupBunkerWeapon[iBunkerPickup])
				g_pickupBunkerWeapon[iBunkerPickup] = NULL
				g_bLastBunkerWeaponPickedUp[iBunkerPickup] = TRUE
			ELSE
			
				IF IS_BUNKER_PICKUP_SAFE_TO_COLLECT(iBunkerPickup)
				AND NOT IS_BIT_SET(iBunkerPickupFlags[iBunkerPickup], 1) // Safe to collect?
					// remove so we can re-create
					REMOVE_PICKUP(g_pickupBunkerWeapon[iBunkerPickup])
					g_iLastBunkerWeaponCreateTime[iBunkerPickup] = 0
				ENDIF
				
				// Unlock help text for owner
				GET_BUNKER_WEAPON_PICKUP_DATA(iBunkerPickup, ePickupType, vPos, vRot, iCount)
				IF PLAYER_ID() = thisBunker.pOwner
				AND GET_DISTANCE_BETWEEN_COORDS(vPos, vPlayerCoords, FALSE) < 1.0
				AND NOT IS_BUNKER_PICKUP_SAFE_TO_COLLECT(iBunkerPickup)
					IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("BUNKER_PICK_1")
					OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("BUNKER_PICK_2")
					OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("BUNKER_PICK_3")
						IF iBunkerPickup = 1
							PRINT_HELP("BUNKER_PICK_1")
							bDisplayHelp = TRUE
						ELIF iBunkerPickup = 2
							PRINT_HELP("BUNKER_PICK_2")
							bDisplayHelp = TRUE
						ELIF iBunkerPickup = 3
							PRINT_HELP("BUNKER_PICK_3")
							bDisplayHelp = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(iBunkerPickupFlags[iBunkerPickup], 0) // First weapon processed
			IF NOT g_bLastBunkerWeaponPickedUp[iBunkerPickup]
				g_iLastBunkerWeaponCreateTime[iBunkerPickup] = 0
			ENDIF
			SET_BIT(iBunkerPickupFlags[iBunkerPickup], 0) // First weapon processed
		ENDIF
		
		IF (GET_CLOUD_TIME_AS_INT() - g_iLastBunkerWeaponCreateTime[iBunkerPickup]) > 600 // 10 minutes
			IF DOES_PICKUP_EXIST(g_pickupBunkerWeapon[iBunkerPickup])
				g_iLastBunkerWeaponCreateTime[iBunkerPickup] = GET_CLOUD_TIME_AS_INT()-300 // Check again in 5 minutes
				
			ELIF PLAYER_ID() != thisBunker.pOwner
			AND NOT IS_BUNKER_PICKUP_SAFE_TO_COLLECT(iBunkerPickup)
				// Don't bother creating if owner says no.
			ELSE
				g_iLastBunkerWeaponCreateTime[iBunkerPickup] = GET_CLOUD_TIME_AS_INT()
				
				INT iPlacementFlags = 0
				
				// If it's not available set as a map pickup so we can make it uncollectable and transparent
				IF NOT IS_BUNKER_PICKUP_SAFE_TO_COLLECT(iBunkerPickup)
					SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_MAP))
				ENDIF
				SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ROTATE))
				SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_LOCAL_ONLY))
		  		SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ORIENT_TO_GROUND))
				
				GET_BUNKER_WEAPON_PICKUP_DATA(iBunkerPickup, ePickupType, vPos, vRot, iCount)
				g_pickupBunkerWeapon[iBunkerPickup] = CREATE_PICKUP_ROTATE(ePickupType, vPos+<<0.0,0.0,0.3>>, vRot, iPlacementFlags, iCount, DEFAULT, FALSE)
				g_bLastBunkerWeaponPickedUp[iBunkerPickup] = FALSE
				
				IF IS_BUNKER_PICKUP_SAFE_TO_COLLECT(iBunkerPickup)
					SET_BIT(iBunkerPickupFlags[iBunkerPickup], 1) // Safe to collect?
				ELSE
					CLEAR_BIT(iBunkerPickupFlags[iBunkerPickup], 1) // Safe to collect?
					SET_PICKUP_UNCOLLECTABLE(g_pickupBunkerWeapon[iBunkerPickup], TRUE)
					SET_PICKUP_TRANSPARENT_WHEN_UNCOLLECTABLE(g_pickupBunkerWeapon[iBunkerPickup], TRUE)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF NOT bDisplayHelp
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("BUNKER_PICK_1")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("BUNKER_PICK_2")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("BUNKER_PICK_3")
			CLEAR_HELP()
		ENDIF
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
PROC INITIALISE_FACTORY_TOTALS_DEBUG()
	
	CDEBUG1LN(DEBUG_PROPERTY, "[AM_MP_BUNKER] - INITIALISE_FACTORY_TOTALS_DEBUG --=")
	CDEBUG1LN(DEBUG_PROPERTY, "[AM_MP_BUNKER] - INITIALISE_FACTORY_TOTALS_DEBUG - Factory: 				 ", thisBunker.eID							)
	CDEBUG1LN(DEBUG_PROPERTY, "[AM_MP_BUNKER] - INITIALISE_FACTORY_TOTALS_DEBUG - Slot: 				 ", thisBunker.iSaveSlot					)
	CDEBUG1LN(DEBUG_PROPERTY, "[AM_MP_BUNKER] - INITIALISE_FACTORY_TOTALS_DEBUG - Product Layout: 		 ", thisBunker.iProductLayout				)
	CDEBUG1LN(DEBUG_PROPERTY, "[AM_MP_BUNKER] - INITIALISE_FACTORY_TOTALS_DEBUG - Product Total: 		 ", thisBunker.iProductSlot					)
	CDEBUG1LN(DEBUG_PROPERTY, "[AM_MP_BUNKER] - INITIALISE_FACTORY_TOTALS_DEBUG - Materials Total: 		 ", thisBunker.iMaterialsSlot				)
	CDEBUG1LN(DEBUG_PROPERTY, "[AM_MP_BUNKER] - INITIALISE_FACTORY_TOTALS_DEBUG - Materials % Slot: 	 ", thisBunker.iMaterialPercentageSlot		)
	CDEBUG1LN(DEBUG_PROPERTY, "[AM_MP_BUNKER] - INITIALISE_FACTORY_TOTALS_DEBUG - Materials Init % Slot: ", thisBunker.iMaterialInitPercentageSlot	)
	CDEBUG1LN(DEBUG_PROPERTY, "[AM_MP_BUNKER] - INITIALISE_FACTORY_TOTALS_DEBUG - Production Stage: 	 ", serverBD.iProductionStage				)
	CDEBUG1LN(DEBUG_PROPERTY, "[AM_MP_BUNKER] - INITIALISE_FACTORY_TOTALS_DEBUG - Active State: 		 ", serverBD.bActiveState					)
	CDEBUG1LN(DEBUG_PROPERTY, "[AM_MP_BUNKER] - INITIALISE_FACTORY_TOTALS_DEBUG - Pers Quarters:		 ", IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_BUNKER_PERSONAL_QUATRS_PURCHASED)		)
	CDEBUG1LN(DEBUG_PROPERTY, "[AM_MP_BUNKER] - INITIALISE_FACTORY_TOTALS_DEBUG - Standard Caddy:		 ", IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_BUNKER_CADDY_STANDARD_PURCHASED)		)
	CDEBUG1LN(DEBUG_PROPERTY, "[AM_MP_BUNKER] - INITIALISE_FACTORY_TOTALS_DEBUG - Upgraded Caddy:		 ", IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_BUNKER_CADDY_UPGRADED_PURCHASED)		)
	CDEBUG1LN(DEBUG_PROPERTY, "[AM_MP_BUNKER] - INITIALISE_FACTORY_TOTALS_DEBUG - Shooting Range 0:		 ", IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_BUNKER_SHOOTING_RANGE_BLACK_PURCHASED)	)
	CDEBUG1LN(DEBUG_PROPERTY, "[AM_MP_BUNKER] - INITIALISE_FACTORY_TOTALS_DEBUG - Shooting Range 1:		 ", IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_BUNKER_SHOOTING_RANGE_WHITE_PURCHASED)	)
	CDEBUG1LN(DEBUG_PROPERTY, "[AM_MP_BUNKER] - INITIALISE_FACTORY_TOTALS_DEBUG - Gun Locker:			 ", IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_BUNKER_GUN_LOCKER_PURCHASED)			)
	CDEBUG1LN(DEBUG_PROPERTY, "[AM_MP_BUNKER] - INITIALISE_FACTORY_TOTALS_DEBUG - Bunker Style A:		 ", IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_BUNKER_BUNKER_STYLE_A_PURCHASED)		)
	CDEBUG1LN(DEBUG_PROPERTY, "[AM_MP_BUNKER] - INITIALISE_FACTORY_TOTALS_DEBUG - Bunker Style B:		 ", IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_BUNKER_BUNKER_STYLE_B_PURCHASED)		)
	CDEBUG1LN(DEBUG_PROPERTY, "[AM_MP_BUNKER] - INITIALISE_FACTORY_TOTALS_DEBUG - Bunker Style C:		 ", IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_BUNKER_BUNKER_STYLE_C_PURCHASED)		)
	CDEBUG1LN(DEBUG_PROPERTY, "[AM_MP_BUNKER] - INITIALISE_FACTORY_TOTALS_DEBUG - ")
	
	IF thisBunker.pOwner != INVALID_PLAYER_INDEX()
		IF HAS_PLAYER_COMPLETED_FACTORY_SETUP_MISSION(thisBunker.pOwner, GET_FACTORY_ID_FROM_FACTORY_SLOT(thisBunker.pOwner, thisBunker.iSaveSlot))
			CDEBUG1LN(DEBUG_PROPERTY, "[AM_MP_BUNKER] - INITIALISE_FACTORY_TOTALS_DEBUG - Factory: ", thisBunker.eID, " has completed its setup mission")
		ELSE
			CDEBUG1LN(DEBUG_PROPERTY, "[AM_MP_BUNKER] - INITIALISE_FACTORY_TOTALS_DEBUG - Factory: ", thisBunker.eID, " has *NOT completed its setup mission")
		ENDIF
		
		IF GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(thisBunker.pOwner, TRUE)
			CDEBUG1LN(DEBUG_PROPERTY, "[AM_MP_BUNKER] - INITIALISE_FACTORY_TOTALS_DEBUG - Factory owner is currently on a gang boss mission")
		ELSE
			CDEBUG1LN(DEBUG_PROPERTY, "[AM_MP_BUNKER] - INITIALISE_FACTORY_TOTALS_DEBUG - Factory owner is currently *NOT on a gang boss mission")
		ENDIF
		
		IF g_sMPTunables.bBIKER_STOP_PRODUCTION_WHEN_NOT_MC_PRESIDENT
		AND NOT GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(thisBunker.pOwner)
			CDEBUG1LN(DEBUG_PROPERTY, "[AM_MP_BUNKER] - INITIALISE_FACTORY_TOTALS_DEBUG - Factory: ", thisBunker.eID, " inactive because player is not MC and bBIKER_STOP_PRODUCTION_WHEN_NOT_MC_PRESIDENT tunable on")
		ENDIF
	ENDIF
	
ENDPROC
#ENDIF

PROC INITIALISE_FACTORY_TOTALS()
	
	IF thisBunker.pOwner = INVALID_PLAYER_INDEX()
		EXIT
	ENDIF
	
	thisBunker.iProductSlot = GET_PRODUCT_TOTAL_FOR_FACTORY(thisBunker.pOwner, thisBunker.eID)
	thisBunker.iProductLayout = GET_BUNKER_PRODUCT_LAYOUT(thisBunker.pOwner, thisBunker.eID)
	thisBunker.iMaterialsSlot = GlobalplayerBD_FM[NATIVE_TO_INT(thisBunker.pOwner)].propertyDetails.bdFactoryData[BUNKER_SAVE_SLOT].iMaterialsTotalBlended
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		serverBD.iProductionStage = GET_PRODUCTION_STATE_FOR_FACTORY(thisBunker.pOwner, thisBunker.eID)
		serverBD.bActiveState = IS_FACTORY_PRODUCTION_ACTIVE(thisBunker.pOwner, thisBunker.iSaveSlot)
	ENDIF
	
	SET_LOCAL_PLAYER_BROADCAST_BIT(BS_BUNKER_PERSONAL_QUATRS_PURCHASED, IS_PLAYER_BUNKER_SAVEBED_PURCHASED(thisBunker.pOwner))
	SET_LOCAL_PLAYER_BROADCAST_BIT(BS_BUNKER_CADDY_STANDARD_PURCHASED, IS_PLAYER_BUNKER_TRANSPORTATION_STANDARD_PURCHASED(thisBunker.pOwner))
	SET_LOCAL_PLAYER_BROADCAST_BIT(BS_BUNKER_CADDY_UPGRADED_PURCHASED, IS_PLAYER_BUNKER_TRANSPORTATION_UPDATED_PURCHASED(thisBunker.pOwner))
	SET_LOCAL_PLAYER_BROADCAST_BIT(BS_BUNKER_SHOOTING_RANGE_BLACK_PURCHASED, IS_PLAYER_BUNKER_FIRING_RANGE_BLACK_PURCHASED(thisBunker.pOwner))
	SET_LOCAL_PLAYER_BROADCAST_BIT(BS_BUNKER_SHOOTING_RANGE_WHITE_PURCHASED, IS_PLAYER_BUNKER_FIRING_RANGE_WHITE_PURCHASED(thisBunker.pOwner))
	SET_LOCAL_PLAYER_BROADCAST_BIT(BS_BUNKER_GUN_LOCKER_PURCHASED, IS_PLAYER_BUNKER_GUNLOCKER_PURCHASED(thisBunker.pOwner))
	SET_LOCAL_PLAYER_BROADCAST_BIT(BS_BUNKER_BUNKER_STYLE_A_PURCHASED, IS_PLAYER_BUNKER_STYLE_OPTION_A_PURCHASED(thisBunker.pOwner))
	SET_LOCAL_PLAYER_BROADCAST_BIT(BS_BUNKER_BUNKER_STYLE_B_PURCHASED, IS_PLAYER_BUNKER_STYLE_OPTION_B_PURCHASED(thisBunker.pOwner))
	SET_LOCAL_PLAYER_BROADCAST_BIT(BS_BUNKER_BUNKER_STYLE_C_PURCHASED, IS_PLAYER_BUNKER_STYLE_OPTION_C_PURCHASED(thisBunker.pOwner))
	
	UPDATE_MATERIAL_PERCENTAGE_SLOT()
	thisBunker.iMaterialInitPercentageSlot = thisBunker.iMaterialPercentageSlot
	
	#IF IS_DEBUG_BUILD
	INITIALISE_FACTORY_TOTALS_DEBUG()
	#ENDIF
ENDPROC

PROC INITIALISE_FACTORY_PRODUCT()
	
	IF IS_FACTORY_OWNER_ID_VALID(NATIVE_TO_INT(thisBunker.pOwner))
	AND NOT IS_BIT_SET(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_SHOOTING_RANGE_TARGETS_SPAWNED)
		CREATE_ALL_OWNED_FACTORY_PRODUCTS(thisBunker.eID, thisBunker.iProductSlot, TRUE)
		CREATE_ALL_OWNED_FACTORY_MATERIAL_PROPS(thisBunker.eID, thisBunker.iMaterialInitPercentageSlot)
		INITIALISE_FACTORY_AUDIO()
	ENDIF
	
ENDPROC

PROC MAINTAIN_FACTORY_PRODUCTION()
	
	IF DOES_FACTORY_PRODUCT_TOTAL_NEED_UPDATING()
		IF HAS_FACTORY_PRODUCT_TOTAL_INCREASED()
			ADD_FACTORY_PRODUCT()
		ELIF HAS_FACTORY_PRODUCT_TOTAL_DECREASED()
			REMOVE_FACTORY_PRODUCT()
		ENDIF
	ENDIF
	
	IF DOES_FACTORY_MATERIALS_TOTAL_NEED_UPDATING()
		UPDATE_MATERIAL_PERCENTAGE_SLOT()
		IF DOES_FACTORY_MATERIALS_PERCENTAGE_SLOT_NEED_UPDATING()
			IF HAS_FACTORY_MATERIAL_TOTAL_INCREASED()
				ADD_FACTORY_MATERIALS()
			ELIF HAS_FACTORY_MATERIAL_TOTAL_DECREASED()
				REMOVE_FACTORY_MATERIALS()
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Only spawn bunker products if the player is near the storage bay or in shot of any products
PROC MAINTAIN_PRODUCT_SPAWNING()
	
	IF NOT IS_BIT_SET(thisBunker.iBS2, BS_2_BUNKER_DATA_INITIAL_PRODUCTS_SPAWNED)
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<836.0,-3238.5,-94.382935>>, <<905.5,-3238.5,-100.1>>, 27.0) // Init product
			IF NOT IS_BIT_SET(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_SHOOTING_RANGE_TARGETS_SPAWNED)
				CREATE_ALL_OWNED_FACTORY_PRODUCTS(thisBunker.eID, GET_PRODUCT_TOTAL_FOR_FACTORY(thisBunker.pOwner, thisBunker.eID), TRUE)
				SET_BIT(thisBunker.iBS2, BS_2_BUNKER_DATA_INITIAL_PRODUCTS_SPAWNED)
				SET_BIT(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_BUNKER_CRATES_SPAWNED)
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<836.0,-3238.5,-94.382935>>, <<905.5,-3238.5,-100.1>>, 27.0) // Init product
		AND NOT IS_BIT_SET(thisBunker.iBS, BS_BUNKER_DATA_PRODUCTS_SPAWNED)
			CLEANUP_ALL_FACTORY_PRODUCTS(thisBunker.eID)
			CLEAR_BIT(thisBunker.iBS2, BS_2_BUNKER_DATA_INITIAL_PRODUCTS_SPAWNED)
			CLEAR_BIT(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_BUNKER_CRATES_SPAWNED)
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(thisBunker.iBS, BS_BUNKER_DATA_PRODUCTS_SPAWNED)
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<905.0,-3238.5,-94.382935>>, <<953.5,-3238.5,-100.1>>, 27.0) // Entrance corridor
		OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<916.1,-3238.5,-94.382935>>, <<948.1,-3189.0,-100.1>>, 50.0) // Main storage bay
		OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<887.3,-3226.2,-94.382935>>, <<918.4,-3213.2,-100.1>>, 20.0) // First product creation room
		OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<897.5,-3197.0,-94.382935>>, <<911.7,-3228.2,-100.1>>, 10.0) // First product creation room
			IF NOT IS_BIT_SET(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_SHOOTING_RANGE_TARGETS_SPAWNED)
				CREATE_ALL_OWNED_FACTORY_PRODUCTS(thisBunker.eID, GET_PRODUCT_TOTAL_FOR_FACTORY(thisBunker.pOwner, thisBunker.eID))
				SET_BIT(thisBunker.iBS, BS_BUNKER_DATA_PRODUCTS_SPAWNED)
				SET_BIT(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_BUNKER_CRATES_SPAWNED)
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<905.0,-3238.5,-94.382935>>, <<953.5,-3238.5,-100.1>>, 27.0) // Entrance corridor
		AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<916.1,-3238.5,-94.382935>>, <<948.1,-3189.0,-100.1>>, 50.0) // Main storage bay
		AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<887.3,-3226.2,-94.382935>>, <<918.4,-3213.2,-100.1>>, 20.0) // First product creation room
		AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<897.5,-3197.0,-94.382935>>, <<911.7,-3228.2,-100.1>>, 10.0) // First product creation room
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<836.0,-3238.5,-94.382935>>, <<905.5,-3238.5,-100.1>>, 27.0) // Init product
				CLEANUP_ALL_FACTORY_PRODUCTS(thisBunker.eID, TRUE)
			ELSE
				CLEANUP_ALL_FACTORY_PRODUCTS(thisBunker.eID)
			ENDIF
			CLEAR_BIT(thisBunker.iBS, BS_BUNKER_DATA_PRODUCTS_SPAWNED)
			CLEAR_BIT(thisBunker.iBS2, BS_2_BUNKER_DATA_INITIAL_PRODUCTS_SPAWNED)
			CLEAR_BIT(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_BUNKER_CRATES_SPAWNED)
		ENDIF
	ENDIF
	
ENDPROC

PROC MAINTAIN_MATERIAL_PROP_SPAWNING()
	
	IF NOT IS_BIT_SET(thisBunker.iBS, BS_BUNKER_DATA_MATERIAL_PROPS_SPAWNED)
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<836.0,-3238.5,-94.382935>>, <<953.5,-3238.5,-100.1>>, 25.0) // Entrance corridor
		OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<900.466553,-3226.168213,-99.278641>>, <<908.3105, -3218.3540, -96.5334>>, 8.0) // Research corridor
			CREATE_ALL_OWNED_FACTORY_MATERIAL_PROPS(thisBunker.eID, thisBunker.iMaterialsSlot)
			SET_BIT(thisBunker.iBS, BS_BUNKER_DATA_MATERIAL_PROPS_SPAWNED)
		ENDIF
	ELSE
		IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<836.0,-3238.5,-94.382935>>, <<953.5,-3238.5,-100.1>>, 25.0) // Entrance corridor
		AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<900.466553,-3226.168213,-99.278641>>, <<908.3105, -3218.3540, -96.5334>>, 8.0) // Research corridor
			CLEANUP_ALL_FACTORY_MATERIAL_PROPS(thisBunker.eID)
			CLEAR_BIT(thisBunker.iBS, BS_BUNKER_DATA_MATERIAL_PROPS_SPAWNED)
		ENDIF
	ENDIF
	
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════╡ TUTORIAL PRODUCT ╞═════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC HIDE_ALL_FACTORY_PRODUCT()
	
	INT iProductSlot
	FOR iProductSlot = 0 TO GET_FACTORY_PRODUCT_CAPACITY(thisBunker.eID)
		IF DOES_ENTITY_EXIST(thisBunker.sProductData.oProductObject[iProductSlot])
			SET_ENTITY_COORDS(thisBunker.sProductData.oProductObject[iProductSlot], GET_ENTITY_COORDS(thisBunker.sProductData.oProductObject[iProductSlot]) - <<0,0,-50.0>>)
		ENDIF
	ENDFOR
	
ENDPROC

PROC HIDE_ALL_FACTORY_MATERIAL_PROPS()
	
	INT iMaterialSlot
	FOR iMaterialSlot = 0 TO GET_FACTORY_MATERIAL_PROPS_CAPACITY(thisBunker.eID)
		IF DOES_ENTITY_EXIST(thisBunker.sMaterialData.oMaterialObject[iMaterialSlot])
			SET_ENTITY_COORDS(thisBunker.sMaterialData.oMaterialObject[iMaterialSlot], GET_ENTITY_COORDS(thisBunker.sMaterialData.oMaterialObject[iMaterialSlot]) - <<0,0,-50.0>>)
		ENDIF
	ENDFOR
	
ENDPROC

PROC SHOW_ALL_FACTORY_PRODUCT()
	
	INT iProductSlot
	FOR iProductSlot = 0 TO GET_FACTORY_PRODUCT_CAPACITY(thisBunker.eID)
		IF DOES_ENTITY_EXIST(thisBunker.sProductData.oProductObject[iProductSlot])
			SET_ENTITY_COORDS(thisBunker.sProductData.oProductObject[iProductSlot], GET_FACTORY_PRODUCT_COORDS(thisBunker.pOwner, thisBunker.eID, iProductSlot))
		ENDIF
	ENDFOR
	
ENDPROC

PROC SHOW_ALL_FACTORY_MATERIAL_PROPS()
	
	INT iMaterialSlot
	FOR iMaterialSlot = 0 TO GET_FACTORY_PRODUCT_CAPACITY(thisBunker.eID)
		IF DOES_ENTITY_EXIST(thisBunker.sMaterialData.oMaterialObject[iMaterialSlot])
			SET_ENTITY_COORDS(thisBunker.sMaterialData.oMaterialObject[iMaterialSlot], GET_FACTORY_PRODUCT_COORDS(thisBunker.pOwner, thisBunker.eID, iMaterialSlot))
		ENDIF
	ENDFOR
	
ENDPROC

PROC SET_ALL_BUNKER_PRODUCT_MODELS_AS_NO_LONGER_NEEDED()
	MODEL_NAMES eProductModel
	
	INT iModel
	REPEAT ciMAX_BUNKER_PRODUCT_MODELS iModel
		SWITCH iModel
			CASE 0	eProductModel = GR_PROP_GR_CRATES_PISTOLS_01A		BREAK
			CASE 1	eProductModel = GR_PROP_GR_CRATES_RIFLES_01A		BREAK
			CASE 2	eProductModel = GR_PROP_GR_CRATES_RIFLES_02A		BREAK
			CASE 3	eProductModel = GR_PROP_GR_CRATES_RIFLES_03A		BREAK
			CASE 4	eProductModel = GR_PROP_GR_CRATES_RIFLES_04A		BREAK
			CASE 5	eProductModel = GR_PROP_GR_CRATES_SAM_01A			BREAK
			CASE 6	eProductModel = GR_PROP_GR_CRATES_WEAPON_MIX_01A	BREAK
		ENDSWITCH
		
		IF IS_MODEL_VALID(eProductModel)
		AND HAS_MODEL_LOADED(eProductModel)
			SET_MODEL_AS_NO_LONGER_NEEDED(eProductModel)
		ENDIF
	ENDREPEAT
	
ENDPROC

PROC SET_ALL_BUNKER_MATERIAL_MODELS_AS_NO_LONGER_NEEDED()
	MODEL_NAMES eMaterialModel
	
	INT iModel
	REPEAT ciMAX_BUNKER_MATERIAL_MODELS iModel
		SWITCH iModel
			CASE 0	eMaterialModel = GR_PROP_GR_GUNSMITHSUPL_01A	BREAK
			CASE 1	eMaterialModel = GR_PROP_GR_GUNSMITHSUPL_02A	BREAK
			CASE 2	eMaterialModel = GR_PROP_GR_GUNSMITHSUPL_03A	BREAK
		ENDSWITCH
		
		IF IS_MODEL_VALID(eMaterialModel)
		AND HAS_MODEL_LOADED(eMaterialModel)
			SET_MODEL_AS_NO_LONGER_NEEDED(eMaterialModel)
		ENDIF
	ENDREPEAT
	
ENDPROC

PROC CREATE_DEMO_PRODUCT()
	
	IF NOT IS_BIT_SET(thisBunker.iBS2, BS_2_BUNKER_DATA_DEMO_PRODUCT_CREATED)
		INT i, iCount
		VECTOR vCoords, vRot
		REPEAT GET_FACTORY_PRODUCT_CAPACITY(thisBunker.eID) i
			
			IF NOT DOES_ENTITY_EXIST(thisBunker.sProductData.oTUTProductObject[i])
				MODEL_NAMES eProductModel = GET_FACTORY_PRODUCT_MODEL(thisBunker.pOwner, thisBunker.eID, thisBunker.iProductLayout, (i + 1))			
				
				IF IS_MODEL_VALID(eProductModel)
					REQUEST_MODEL(eProductModel)
					
					IF HAS_MODEL_LOADED(eProductModel)
						vCoords = GET_FACTORY_PRODUCT_COORDS(thisBunker.pOwner, thisBunker.eID, i + 1)
						vRot = GET_FACTORY_PRODUCT_ROTATION(thisBunker.eID, i + 1)
						
						thisBunker.sProductData.oTUTProductObject[i] = CREATE_OBJECT(eProductModel, vCoords, FALSE, FALSE)
						SET_ENTITY_ROTATION(thisBunker.sProductData.oTUTProductObject[i], vRot)
						FREEZE_ENTITY_POSITION(thisBunker.sProductData.oTUTProductObject[i], TRUE)
						SET_ENTITY_ALPHA(thisBunker.sProductData.oTUTProductObject[i], 0, FALSE)
					ENDIF
				ENDIF
			ELSE
				iCount ++
			ENDIF
		ENDREPEAT
		
		IF iCount = GET_FACTORY_PRODUCT_CAPACITY(thisBunker.eID)
			
			#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_PROPERTY, "[AM_MP_BUNKER] - CREATE_DEMO_PRODUCT - Created.")
			#ENDIF
			
			SET_ALL_BUNKER_PRODUCT_MODELS_AS_NO_LONGER_NEEDED()
			
			SET_BIT(thisBunker.iBS2, BS_2_BUNKER_DATA_DEMO_PRODUCT_CREATED)
		ENDIF
	ENDIF
	
ENDPROC

FUNC INT GET_DEMO_PRODUCT_TIME(BOOL bDespawn = FALSE) 
	UNUSED_PARAMETER(bDespawn)
	RETURN 1000//BUNKER_CUTSCENE_DEMO_PRODUCT_FULL_FILL_TIME
ENDFUNC

PROC CREATE_DEMO_MATERIAL_PROPS()

	IF NOT DOES_ENTITY_EXIST(thisBunker.sMaterialData.oTUTMaterialObject[0])
	
		INT i
		VECTOR vCoords, vRot
		REPEAT GET_FACTORY_MATERIAL_PROPS_CAPACITY(thisBunker.eID) i
			MODEL_NAMES eMaterialModel = GET_FACTORY_PRODUCT_MODEL(thisBunker.pOwner, thisBunker.eID, thisBunker.iProductLayout, i)
			REQUEST_MODEL(eMaterialModel)
			
			IF IS_MODEL_VALID(eMaterialModel)
			AND HAS_MODEL_LOADED(eMaterialModel)
				vCoords = GET_FACTORY_MATERIAL_COORDS(thisBunker.eID, i + 1)
				vRot = GET_FACTORY_MATERIAL_ROTATION(thisBunker.eID, i + 1)
				
				thisBunker.sMaterialData.oTUTMaterialObject[i] = CREATE_OBJECT(eMaterialModel, vCoords, FALSE, FALSE)
				SET_ENTITY_ROTATION(thisBunker.sMaterialData.oTUTMaterialObject[i], vRot)
				FREEZE_ENTITY_POSITION(thisBunker.sMaterialData.oTUTMaterialObject[i], TRUE)
				SET_ENTITY_ALPHA(thisBunker.sMaterialData.oTUTMaterialObject[i], 0, FALSE)
			ENDIF
		ENDREPEAT
		
		#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_PROPERTY, "[AM_MP_BUNKER] - CREATE_DEMO_MATERIAL_PROPS - Created.")
		#ENDIF
		
		SET_ALL_BUNKER_MATERIAL_MODELS_AS_NO_LONGER_NEEDED()
	ENDIF
	
ENDPROC

PROC MAINTAIN_DEMO_PRODUCT()
	INT iCurrentTime = GET_GAME_TIMER()
	
	INT i, iCurrentAlpha
	INT iStartAlpha, iEndAlpha, iStartTime, iEndTime
	REPEAT GET_FACTORY_PRODUCT_CAPACITY(thisBunker.eID) i
		IF DOES_ENTITY_EXIST(thisBunker.sProductData.oTUTProductObject[i])
			IF thisBunker.sProductData.iTimeOfDesiredAlpha[i] > iCurrentTime
				iStartAlpha = thisBunker.sProductData.iStartAlpha[i]
				iEndAlpha = thisBunker.sProductData.iDesiredAlpha[i]
				iStartTime = thisBunker.sProductData.iTimeOfStartAlpha[i]
				iEndTime = thisBunker.sProductData.iTimeOfDesiredAlpha[i]
				
				// This is a linear equation
				FLOAT fSlope = TO_FLOAT(iEndAlpha - iStartAlpha) / TO_FLOAT(iEndTime - iStartTime)
				FLOAT fB = iStartAlpha - (fSlope * TO_FLOAT(iStartTime))
				
				iCurrentAlpha = ROUND(fSlope * TO_FLOAT(iCurrentTime) + fB)
				
				SET_ENTITY_ALPHA(thisBunker.sProductData.oTUTProductObject[i], iMIN(255, IMAX(0, iCurrentAlpha)), FALSE)
				
				#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_PROPERTY, "[AM_MP_BUNKER] - MAINTAIN_DEMO_PRODUCT - Updating alpha of entity ", i, " to ", iCurrentAlpha, "(iCurrentTime = ", iCurrentTime, ", iStarTime = ", iStartTime, ", iEndTime = ", iEndTime, ")")
				#ENDIF
			ELSE
				SET_ENTITY_ALPHA(thisBunker.sProductData.oTUTProductObject[i], thisBunker.sProductData.iDesiredAlpha[i], FALSE)
			ENDIF
		ENDIF
	ENDREPEAT
	
ENDPROC

PROC MAINTAIN_DEMO_MATERIAL_PROPS()
	INT iCurrentTime = GET_GAME_TIMER()
	
	INT i, iCurrentAlpha
	INT iStartAlpha, iEndAlpha, iStartTime, iEndTime
	REPEAT GET_FACTORY_MATERIAL_PROPS_CAPACITY(thisBunker.eID) i
		IF DOES_ENTITY_EXIST(thisBunker.sMaterialData.oTUTMaterialObject[i])
			IF thisBunker.sMaterialData.iTimeOfDesiredAlpha[i] > iCurrentTime
				iStartAlpha = thisBunker.sMaterialData.iStartAlpha[i]
				iEndAlpha = thisBunker.sMaterialData.iDesiredAlpha[i]
				iStartTime = thisBunker.sMaterialData.iTimeOfStartAlpha[i]
				iEndTime = thisBunker.sMaterialData.iTimeOfDesiredAlpha[i]
				
				// This is a linear equation
				FLOAT fSlope = TO_FLOAT(iEndAlpha - iStartAlpha) / TO_FLOAT(iEndTime - iStartTime)
				FLOAT fB = iStartAlpha - (fSlope * TO_FLOAT(iStartTime))
				
				iCurrentAlpha = ROUND(fSlope * TO_FLOAT(iCurrentTime) + fB)
				
				SET_ENTITY_ALPHA(thisBunker.sMaterialData.oTUTMaterialObject[i], iMIN(255, IMAX(0, iCurrentAlpha)), FALSE)
				
				#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_PROPERTY, "[AM_MP_BUNKER] - MAINTAIN_DEMO_MATERIAL_PROPS - Updating alpha of entity ", i, " to ", iCurrentAlpha, "(iCurrentTime = ", iCurrentTime, ", iStarTime = ", iStartTime, ", iEndTime = ", iEndTime, ")")
				#ENDIF
			ELSE
				SET_ENTITY_ALPHA(thisBunker.sMaterialData.oTUTMaterialObject[i], thisBunker.sMaterialData.iDesiredAlpha[i], FALSE)
			ENDIF
		ENDIF
	ENDREPEAT
	
ENDPROC

PROC CLEANUP_DEMO_PRODUCT()
	INT i
	REPEAT GET_FACTORY_PRODUCT_CAPACITY(thisBunker.eID) i
		IF DOES_ENTITY_EXIST(thisBunker.sProductData.oTUTProductObject[i])
			DELETE_OBJECT(thisBunker.sProductData.oTUTProductObject[i])
		ENDIF
	ENDREPEAT
	#IF IS_DEBUG_BUILD
	CDEBUG1LN(DEBUG_PROPERTY, "[AM_MP_BUNKER] - CLEANUP_DEMO_PRODUCT - Done.")
	#ENDIF
ENDPROC

PROC CLEANUP_DEMO_MATERIAL_PROPS()
	INT i
	REPEAT GET_FACTORY_MATERIAL_PROPS_CAPACITY(thisBunker.eID) i
		IF DOES_ENTITY_EXIST(thisBunker.sMaterialData.oTUTMaterialObject[i])
			DELETE_OBJECT(thisBunker.sMaterialData.oTUTMaterialObject[i])
		ENDIF
	ENDREPEAT
	#IF IS_DEBUG_BUILD
	CDEBUG1LN(DEBUG_PROPERTY, "[AM_MP_BUNKER] - CLEANUP_DEMO_MATERIAL_PROPS - Done.")
	#ENDIF
ENDPROC

PROC SHOW_DEMO_PRODUCT(INT iAmount, INT iTime)
	#IF IS_DEBUG_BUILD
	CDEBUG1LN(DEBUG_PROPERTY, "[AM_MP_BUNKER] - SHOW_DEMO_PRODUCT - Called with iAmount = ", iAmount, " and iTime = ", iTime, ", current time = ", GET_GAME_TIMER())
	#ENDIF
	
	iAmount = IMIN(iAmount, GET_FACTORY_PRODUCT_CAPACITY(thisBunker.eID))
	
	// First find the current index that's animating
	INT iCurrentIndex = -1
	INT i
	REPEAT GET_FACTORY_PRODUCT_CAPACITY(thisBunker.eID) i
		IF DOES_ENTITY_EXIST(thisBunker.sProductData.oTUTProductObject[i])
		AND GET_ENTITY_ALPHA(thisBunker.sProductData.oTUTProductObject[i]) = 0
			iCurrentIndex = i
			BREAKLOOP
		ENDIF
	ENDREPEAT
	
	IF iCurrentIndex = -1
		iCurrentIndex = GET_FACTORY_PRODUCT_CAPACITY(thisBunker.eID)
		#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_PROPERTY, "[AM_MP_BUNKER] - SHOW_DEMO_PRODUCT - iCurrentIndex = -1 so using max capacity")
		#ENDIF
	ENDIF
	
	REPEAT GET_FACTORY_PRODUCT_CAPACITY(thisBunker.eID) i
		IF DOES_ENTITY_EXIST(thisBunker.sProductData.oTUTProductObject[i])
			IF i < iAmount
				thisBunker.sProductData.iDesiredAlpha[i] = 255
				thisBunker.sProductData.iStartAlpha[i] = GET_ENTITY_ALPHA(thisBunker.sProductData.oTUTProductObject[i])
				#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_PROPERTY, "[AM_MP_BUNKER] - SHOW_DEMO_PRODUCT - Setting product ", i, " to iDesiredAlpha 255")
				#ENDIF
			ELSE
				thisBunker.sProductData.iDesiredAlpha[i] = 0
				thisBunker.sProductData.iStartAlpha[i] = GET_ENTITY_ALPHA(thisBunker.sProductData.oTUTProductObject[i])
				#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_PROPERTY, "[AM_MP_BUNKER] - SHOW_DEMO_PRODUCT - Setting product ", i, " to iDesiredAlpha 0")
				#ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	INT iAmountToAnimate = ABSI(iAmount - iCurrentIndex)
	INT iTimePerObject = FLOOR(TO_FLOAT(iTime) / TO_FLOAT(iAmountToAnimate))
	INT iStartOffset = FLOOR(TO_FLOAT(iTimePerObject) / TO_FLOAT(iAmountToAnimate)) * 2
	INT iTotalTime = GET_GAME_TIMER()
	INT iStep
	BOOL bRunLoop
	
	INT iStartIndex, iEndIndex
	
	IF iCurrentIndex > iAmount
		iStep = -1
		iStartIndex = IMAX(0, iCurrentIndex - 1)
		iEndIndex = iCurrentIndex - iAmountToAnimate
		bRunLoop = TRUE
	ELIF iCurrentIndex < iAmount
		iStep = 1
		iStartIndex = iCurrentIndex
		iEndIndex = iCurrentIndex + iAmount - 1
		bRunLoop = TRUE
	ENDIF
	
	IF iEndIndex > GET_FACTORY_PRODUCT_CAPACITY(thisBunker.eID)
		iEndIndex = GET_FACTORY_PRODUCT_CAPACITY(thisBunker.eID)
	ENDIF
	
	#IF IS_DEBUG_BUILD
	CDEBUG1LN(DEBUG_PROPERTY, "[AM_MP_BUNKER] - SHOW_DEMO_PRODUCT - iCurrentIndex is ", iCurrentIndex, ", iAmountToAnimate is ", iAmountToAnimate, ", iTimePerObject is ", iTimePerObject)
	CDEBUG1LN(DEBUG_PROPERTY, "[AM_MP_BUNKER] - SHOW_DEMO_PRODUCT - This loop will run from iStartIndex = ", iStartIndex, " to iEndIndex = ", iEndIndex)
	#ENDIF
	
	i = iStartIndex // 
	
	WHILE bRunLoop
		thisBunker.sProductData.iTimeOfStartAlpha[i] = iTotalTime
		
		IF i != iStartIndex
			// Offset start times for all but first so they overlap at start
			thisBunker.sProductData.iTimeOfStartAlpha[i] -= iStartOffset
		ENDIF
		
		iTotalTime += iTimePerObject
		thisBunker.sProductData.iTimeOfDesiredAlpha[i] = iTotalTime
		
		IF i != iEndIndex
			// Offset end time for all but last so they overlap a bit
			thisBunker.sProductData.iTimeOfDesiredAlpha[i] += iStartOffset
		ENDIF
		
		#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_PROPERTY, "[AM_MP_BUNKER] - SHOW_DEMO_PRODUCT - New start time for product ", i, " is ", thisBunker.sProductData.iTimeOfStartAlpha[i], " and end time is ", thisBunker.sProductData.iTimeOfDesiredAlpha[i])
		#ENDIF
		
		IF i = iEndIndex
			bRunLoop = FALSE
			BREAKLOOP
		ELSE
			i = i + iStep
		ENDIF
	ENDWHILE
	
	#IF IS_DEBUG_BUILD
	CDEBUG1LN(DEBUG_PROPERTY, "[AM_MP_BUNKER] - SHOW_DEMO_PRODUCT - Done.")
	#ENDIF
ENDPROC

PROC SHOW_DEMO_MATERIAL_PROPS(INT iAmount, INT iTime)
	#IF IS_DEBUG_BUILD
	CDEBUG1LN(DEBUG_PROPERTY, "[AM_MP_BUNKER] - SHOW_DEMO_MATERIAL_PROPS - Called with iAmount = ", iAmount, " and iTime = ", iTime, ", current time = ", GET_GAME_TIMER())
	#ENDIF
	
	iAmount = IMIN(iAmount, GET_FACTORY_MATERIAL_PROPS_CAPACITY(thisBunker.eID))
	
	// First find the current index that's animating
	INT iCurrentIndex = -1
	INT i
	REPEAT GET_FACTORY_MATERIAL_PROPS_CAPACITY(thisBunker.eID) i
		IF DOES_ENTITY_EXIST(thisBunker.sMaterialData.oTUTMaterialObject[i])
		AND GET_ENTITY_ALPHA(thisBunker.sMaterialData.oTUTMaterialObject[i]) = 0
			iCurrentIndex = i
			BREAKLOOP
		ENDIF
	ENDREPEAT
	
	IF iCurrentIndex = -1
		iCurrentIndex = GET_FACTORY_MATERIAL_PROPS_CAPACITY(thisBunker.eID)
		#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_PROPERTY, "[AM_MP_BUNKER] - SHOW_DEMO_MATERIAL_PROPS - iCurrentIndex = -1 so using max capacity")
		#ENDIF
	ENDIF
	
	REPEAT GET_FACTORY_MATERIAL_PROPS_CAPACITY(thisBunker.eID) i
		IF DOES_ENTITY_EXIST(thisBunker.sMaterialData.oTUTMaterialObject[i])
			IF i < iAmount
				thisBunker.sMaterialData.iDesiredAlpha[i] = 255
				thisBunker.sMaterialData.iStartAlpha[i] = GET_ENTITY_ALPHA(thisBunker.sMaterialData.oTUTMaterialObject[i])
				#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_PROPERTY, "[AM_MP_BUNKER] - SHOW_DEMO_MATERIAL_PROPS - Setting product ", i, " to iDesiredAlpha 255")
				#ENDIF
			ELSE
				thisBunker.sMaterialData.iDesiredAlpha[i] = 0
				thisBunker.sMaterialData.iStartAlpha[i] = GET_ENTITY_ALPHA(thisBunker.sMaterialData.oTUTMaterialObject[i])
				#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_PROPERTY, "[AM_MP_BUNKER] - SHOW_DEMO_MATERIAL_PROPS - Setting product ", i, " to iDesiredAlpha 0")
				#ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	INT iAmountToAnimate = ABSI(iAmount - iCurrentIndex)
	INT iTimePerObject = FLOOR(TO_FLOAT(iTime) / TO_FLOAT(iAmountToAnimate))
	INT iStartOffset = FLOOR(TO_FLOAT(iTimePerObject) / TO_FLOAT(iAmountToAnimate)) * 2
	INT iTotalTime = GET_GAME_TIMER()
	INT iStep
	BOOL bRunLoop
	
	INT iStartIndex, iEndIndex
	
	IF iCurrentIndex > iAmount
		iStep = -1
		iStartIndex = IMAX(0, iCurrentIndex - 1)
		iEndIndex = iCurrentIndex - iAmountToAnimate
		bRunLoop = TRUE
	ELIF iCurrentIndex < iAmount
		iStep = 1
		iStartIndex = iCurrentIndex
		iEndIndex = iCurrentIndex + iAmount - 1
		bRunLoop = TRUE
	ENDIF
	
	IF iEndIndex > GET_FACTORY_MATERIAL_PROPS_CAPACITY(thisBunker.eID)
		iEndIndex = GET_FACTORY_MATERIAL_PROPS_CAPACITY(thisBunker.eID)
	ENDIF
	
	#IF IS_DEBUG_BUILD
	CDEBUG1LN(DEBUG_PROPERTY, "[AM_MP_BUNKER] - SHOW_DEMO_MATERIAL_PROPS - iCurrentIndex is ", iCurrentIndex, ", iAmountToAnimate is ", iAmountToAnimate, ", iTimePerObject is ", iTimePerObject)
	CDEBUG1LN(DEBUG_PROPERTY, "[AM_MP_BUNKER] - SHOW_DEMO_MATERIAL_PROPS - This loop will run from iStartIndex = ", iStartIndex, " to iEndIndex = ", iEndIndex)
	#ENDIF
	
	i = iStartIndex // 
	
	WHILE bRunLoop
		thisBunker.sMaterialData.iTimeOfStartAlpha[i] = iTotalTime
		
		IF i != iStartIndex
			// Offset start times for all but first so they overlap at start
			thisBunker.sMaterialData.iTimeOfStartAlpha[i] -= iStartOffset
		ENDIF
		
		iTotalTime += iTimePerObject
		thisBunker.sMaterialData.iTimeOfDesiredAlpha[i] = iTotalTime
		
		IF i != iEndIndex
			// Offset end time for all but last so they overlap a bit
			thisBunker.sMaterialData.iTimeOfDesiredAlpha[i] += iStartOffset
		ENDIF
		
		#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_PROPERTY, "[AM_MP_BUNKER] - SHOW_DEMO_MATERIAL_PROPS - New start time for product ", i, " is ", thisBunker.sMaterialData.iTimeOfStartAlpha[i], " and end time is ", thisBunker.sMaterialData.iTimeOfDesiredAlpha[i])
		#ENDIF
		
		IF i = iEndIndex
			bRunLoop = FALSE
			BREAKLOOP
		ELSE
			i = i + iStep
		ENDIF
	ENDWHILE
	
	#IF IS_DEBUG_BUILD
	CDEBUG1LN(DEBUG_PROPERTY, "[AM_MP_BUNKER] - SHOW_DEMO_MATERIAL_PROPS - Done.")
	#ENDIF
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════╡ BUNKER VEHICLES ╞═════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

FUNC VECTOR GET_BUNKER_VEHICLE_POSITION(INT iIndex)
	SWITCH iIndex
		CASE 0 		RETURN	<<877.0, -3247.7, -98.893>>		BREAK
		CASE 1 		RETURN	<<874.0, -3247.7, -98.893>>		BREAK
		CASE 2 		RETURN	<<871.0, -3247.7, -98.893>>		BREAK
		CASE 3 		RETURN	<<868.0, -3247.7, -98.893>>		BREAK
	ENDSWITCH
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

PROC SET_CADDY_EXTRAS(VEHICLE_INDEX &vehTemp, INT iIndex)
	SWITCH iIndex
		CASE 0
			SET_VEHICLE_EXTRA(vehTemp, 1, TRUE)
			SET_VEHICLE_EXTRA(vehTemp, 2, FALSE)
			SET_VEHICLE_EXTRA(vehTemp, 3, TRUE)
		BREAK
		
		CASE 1
			SET_VEHICLE_EXTRA(vehTemp, 1, TRUE)
			SET_VEHICLE_EXTRA(vehTemp, 2, TRUE)
			SET_VEHICLE_EXTRA(vehTemp, 3, TRUE)
		BREAK
		
		CASE 2
			SET_VEHICLE_EXTRA(vehTemp, 1, TRUE)
			SET_VEHICLE_EXTRA(vehTemp, 2, TRUE)
			SET_VEHICLE_EXTRA(vehTemp, 3, FALSE)
		BREAK
		
		CASE 3
			SET_VEHICLE_EXTRA(vehTemp, 1, FALSE)
			SET_VEHICLE_EXTRA(vehTemp, 2, TRUE)
			SET_VEHICLE_EXTRA(vehTemp, 3, TRUE)
		BREAK
	ENDSWITCH
ENDPROC

PROC CREATE_BUNKER_VEHICLE(INT iIndex, MODEL_NAMES eVehicleModel)
	IF NOT HAS_MODEL_LOADED(eVehicleModel)
		CWARNINGLN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] CREATE_BUNKER_VEHICLE - Model has not loaded")
		EXIT
	ENDIF
	
	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niBunkerVehicles[iIndex])
		CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] CREATE_BUNKER_VEHICLE - Model has loaded")
		
		IF CAN_REGISTER_MISSION_VEHICLES(1)
			CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] CREATE_BUNKER_VEHICLE - Creating vehicle: ", iIndex)
			
			IF NETWORK_IS_IN_MP_CUTSCENE()
				SET_NETWORK_CUTSCENE_ENTITIES(TRUE)
			ENDIF
			
			VEHICLE_INDEX vehTemp
			
			vehTemp = CREATE_VEHICLE(eVehicleModel, GET_BUNKER_VEHICLE_POSITION(iIndex), 30.0)
			
			SET_MODEL_AS_NO_LONGER_NEEDED(eVehicleModel)
			SET_ENTITY_CAN_BE_DAMAGED(vehTemp, FALSE)
			SET_VEHICLE_COLOURS(vehTemp, 113, 113)
			SET_VEHICLE_EXTRA_COLOURS(vehTemp, 113, 113)
			
			CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] CREATE_BUNKER_VEHICLE - Setting vehicle colours to 113")
			
			SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(vehTemp, FALSE)
			SET_VEHICLE_CAN_BREAK(vehTemp, FALSE)
			SET_VEHICLE_DIRT_LEVEL(vehTemp, GET_RANDOM_FLOAT_IN_RANGE(0.1, 4.9))
			SET_VEHICLE_HAS_UNBREAKABLE_LIGHTS(vehTemp, TRUE)
			SET_VEHICLE_LIGHT_MULTIPLIER(vehTemp, 0.0)
			SET_VEHICLE_LIGHTS(vehTemp, FORCE_VEHICLE_LIGHTS_OFF)
			SET_VEH_RADIO_STATION(vehTemp, "OFF")
			SET_VEHICLE_RADIO_ENABLED(vehTemp, FALSE)
			SET_VEHICLE_TYRES_CAN_BURST(vehTemp, FALSE)
			SET_VEHICLE_WHEELS_CAN_BREAK(vehTemp, FALSE)
			NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(vehTemp, TRUE)
			SET_ENTITY_CAN_ONLY_BE_DAMAGED_BY_SCRIPT_PARTICIPANTS(vehTemp, TRUE)
			
			IF DECOR_IS_REGISTERED_AS_TYPE("Not_Allow_As_Saved_Veh", DECOR_TYPE_INT)
				IF NOT DECOR_EXIST_ON(vehTemp, "Not_Allow_As_Saved_Veh")
					DECOR_SET_INT(vehTemp, "Not_Allow_As_Saved_Veh", 1)
				ENDIF
			ENDIF
			
			IF eVehicleModel = CADDY2
				SET_VEHICLE_EXTRA(vehTemp, 2, TRUE)
			ELSE
				SET_CADDY_EXTRAS(vehTemp, iIndex)
			ENDIF
			
			serverBD.niBunkerVehicles[iIndex] = VEH_TO_NET(vehTemp)
			
			IF NETWORK_IS_IN_MP_CUTSCENE()
				SET_NETWORK_CUTSCENE_ENTITIES(FALSE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL CREATE_BUNKER_CADDIES()
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF IS_PLAYER_BUNKER_TRANSPORTATION_STANDARD_PURCHASED(thisBunker.pOwner)
		OR (IS_PLAYER_BUNKER_TRANSPORTATION_UPDATED_PURCHASED(thisBunker.pOwner)
		AND NOT g_sMPTunables.BGR_DISABLE_CADDY_2)
			MODEL_NAMES eCaddyModel = CADDY2
			IF IS_PLAYER_BUNKER_TRANSPORTATION_UPDATED_PURCHASED(thisBunker.pOwner)
				eCaddyModel = CADDY3
			ENDIF
			
			INT i
			REPEAT NUMBER_OF_BUNKER_VEHICLES i
				IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niBunkerVehicles[i])
					REQUEST_MODEL(eCaddyModel)
					IF HAS_MODEL_LOADED(eCaddyModel)
						CREATE_BUNKER_VEHICLE(i, eCaddyModel)
					ENDIF
					
					RETURN FALSE
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL CREATE_BUNKER_VEHICLES()

	IF IS_VALID_INTERIOR(thisBunker.BunkerInteriorID)
	AND IS_INTERIOR_READY(thisBunker.BunkerInteriorID) 
		IF NOT CREATE_BUNKER_CADDIES()
			RETURN FALSE
		ENDIF
		
		SWITCH thisTruckBunker.iStage
			CASE 0
				IF thisBunker.pOwner != INVALID_PLAYER_INDEX()
				AND thisBunker.pOwner = PLAYER_ID()
					IF IS_PLAYER_GUNRUNNING_TRUCK_PURCHASED(thisBunker.pOwner)
						IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0])
						AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1])

	//						IF NOT IS_BIT_SET(serverBD.iBS, BS_BUNKER_DATA_RESERVE_VEHICLE_INDEX_FOR_TRUCK)
	//							IF CAN_RESERVE_NETWORK_VEHICLES_FOR_THIS_SCRIPT(GET_NUM_RESERVED_MISSION_VEHICLES() + 2, FALSE, TRUE)
	//								RESERVE_NETWORK_MISSION_VEHICLES(GET_NUM_RESERVED_MISSION_VEHICLES() + 2)
	//								IF CAN_REGISTER_MISSION_VEHICLES(2)
	//									SET_BIT(serverBD.iBS, BS_BUNKER_DATA_RESERVE_VEHICLE_INDEX_FOR_TRUCK)
	//								ENDIF
	//							ENDIF
	//						ELSE	
	//							IF SPAWN_BOOKED_TRUCK_VEHICLE(vTruckCoord, fTruckHeading, TRUE)
	//								CPRINTLN(DEBUG_PROPERTY, "RUN_ACTIVITY_LOGIC - created player ", NATIVE_TO_INT(thisBunker.pOwner), " truck")
	//								
	//								ar_ATTACH_VEHICLE_TO_TRAILER(NET_TO_VEH(serverBD.niBunkerArmoryTruck), NET_TO_VEH(serverBD.niBunkerArmoryTruckTrailer), fTrailerHeadingOffset)
	//								
	//								FORCE_ROOM_FOR_ENTITY(NET_TO_VEH(serverBD.niBunkerArmoryTruckTrailer), GET_INTERIOR_FROM_ENTITY(GET_PLAYER_PED(PLAYER_ID())), GET_HASH_KEY("bunker_ModRoom"))
	//								FORCE_ROOM_FOR_ENTITY(NET_TO_VEH(serverBD.niBunkerArmoryTruck), GET_INTERIOR_FROM_ENTITY(GET_PLAYER_PED(PLAYER_ID())), GET_HASH_KEY("bunker_ModRoom"))
	//								CPRINTLN(DEBUG_PROPERTY, "RUN_ACTIVITY_LOGIC - Forcing truck to room ")
	//								thisTruckBunker.iStage = 1
	//							ENDIF
	//						ENDIF	
						ENDIF	
					ELSE
						CPRINTLN(DEBUG_PROPERTY, "RUN_ACTIVITY_LOGIC - player ", NATIVE_TO_INT(thisBunker.pOwner), " doesnt own truck")
						thisTruckBunker.iStage = -1
					ENDIF
				ELSE
					CPRINTLN(DEBUG_PROPERTY, "RUN_ACTIVITY_LOGIC - player ", NATIVE_TO_INT(thisBunker.pOwner), " doesnt own truck")
					thisTruckBunker.iStage = -1
				ENDIF
			BREAK
			CASE 1
				
			BREAK
			
			CASE -1		//player doesn't own truck (do we need to monitor/reset?)
			BREAK
		ENDSWITCH
	ELSE
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL HAVE_ALL_BUNKER_CADDIES_BEEN_CREATED()
	INT i
	REPEAT NUMBER_OF_BUNKER_VEHICLES i
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niBunkerVehicles[i])
			RETURN FALSE
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

FUNC BOOL ARE_CADDIES_CLEANED_UP()
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		INT i
		REPEAT NUMBER_OF_BUNKER_VEHICLES i
			IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niBunkerVehicles[i])
			AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niBunkerVehicles[i])
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niBunkerVehicles[i])
					VEHICLE_INDEX vTemp = NET_TO_VEH(serverBD.niBunkerVehicles[i])
					
					DELETE_VEHICLE(vTemp)
				ELSE
					NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.niBunkerVehicles[i])
				ENDIF
				
				RETURN FALSE
			ENDIF
		ENDREPEAT
	ENDIF
	
	#IF IS_DEBUG_BUILD
	CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_BUNKER - ARE_CADDIES_CLEANED_UP - Returning TRUE.")	
	#ENDIF	
	
	RETURN TRUE
ENDFUNC

PROC MAINTAIN_BUNKER_CADDIES()
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		VEHICLE_INDEX vTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		
		IF (GET_ENTITY_MODEL(vTemp) = CADDY2 OR GET_ENTITY_MODEL(vTemp) = CADDY3)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_NEXT_RADIO)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PREV_RADIO)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_NEXT_RADIO_TRACK)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PREV_RADIO_TRACK)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_RADIO_WHEEL)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PASSENGER_AIM)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PASSENGER_ATTACK)
			
			IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
				IF NETWORK_HAS_CONTROL_OF_ENTITY(vTemp)
					SET_ENTITY_MAX_SPEED(vTemp, 5.0)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	

	IF NOT IS_PLAYER_IN_ZOOMED_OUT_RADAR_OR_MAP(PLAYER_ID()) // added by neilf, only zoom right in if the expanded rader is not on screen. bug 3581411
		SET_RADAR_ZOOM_PRECISE(1)
	ELSE
		SET_RADAR_ZOOM_PRECISE(0)	
	ENDIF
ENDPROC

PROC MAINTAIN_BUNKER_RAGDOLL()
	IF NOT IS_BIT_SET(thisBunker.iBS, BS_BUNKER_DATA_RAGDOLLING)
		INT i
		REPEAT NUMBER_OF_BUNKER_VEHICLES i
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niBunkerVehicles[i])
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				AND IS_ENTITY_A_VEHICLE(NET_TO_VEH(serverBD.niBunkerVehicles[i]))
				AND IS_VEHICLE_DRIVEABLE(NET_TO_VEH(serverBD.niBunkerVehicles[i]))
					IF IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niBunkerVehicles[i]))
					AND GET_ENTITY_SPEED(NET_TO_VEH(serverBD.niBunkerVehicles[i])) >= 1.0
					AND NOT IS_PED_RAGDOLL(PLAYER_PED_ID())
					AND NOT IS_PED_GETTING_UP(PLAYER_PED_ID())
					AND IS_PED_ON_FOOT(PLAYER_PED_ID())
					AND NOT IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())
					AND NOT IS_PLAYER_STARTED_TO_USE_PERSONAL_CAR_MOD(PLAYER_ID())
						SET_PED_TO_RAGDOLL(PLAYER_PED_ID(), 3000, 3000, TASK_RELAX)
						SET_BIT(thisBunker.iBS, BS_BUNKER_DATA_RAGDOLLING)
						
						#IF IS_DEBUG_BUILD
						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_BUNKER - MAINTAIN_BUNKER_RAGDOLL - Ragdolling")	
						#ENDIF
						
						EXIT
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ELSE
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF NOT IS_PED_RAGDOLL(PLAYER_PED_ID())
			AND NOT IS_PED_GETTING_UP(PLAYER_PED_ID())
			AND IS_PED_ON_FOOT(PLAYER_PED_ID())
			AND NOT IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())
			AND NOT IS_PLAYER_STARTED_TO_USE_PERSONAL_CAR_MOD(PLAYER_ID())
				CLEAR_BIT(thisBunker.iBS, BS_BUNKER_DATA_RAGDOLLING)
				
				#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_BUNKER - MAINTAIN_BUNKER_RAGDOLL - Finished ragdolling")	
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════╡ BUNKER UPGRADES ╞═════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

FUNC BOOL PERFORM_BUNKER_MAZEBANK_WEBSITE_UPGRADE()
	IF IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_BUNKER_PERSONAL_QUATRS_PURCHASED) != IS_PLAYER_BUNKER_SAVEBED_PURCHASED(thisBunker.pOwner)
	OR IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_BUNKER_CADDY_STANDARD_PURCHASED) != IS_PLAYER_BUNKER_TRANSPORTATION_STANDARD_PURCHASED(thisBunker.pOwner)
	OR IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_BUNKER_CADDY_UPGRADED_PURCHASED) != IS_PLAYER_BUNKER_TRANSPORTATION_UPDATED_PURCHASED(thisBunker.pOwner)
	OR IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_BUNKER_SHOOTING_RANGE_BLACK_PURCHASED) != IS_PLAYER_BUNKER_FIRING_RANGE_BLACK_PURCHASED(thisBunker.pOwner)
	OR IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_BUNKER_SHOOTING_RANGE_WHITE_PURCHASED) != IS_PLAYER_BUNKER_FIRING_RANGE_WHITE_PURCHASED(thisBunker.pOwner)
	OR IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_BUNKER_GUN_LOCKER_PURCHASED) != IS_PLAYER_BUNKER_GUNLOCKER_PURCHASED(thisBunker.pOwner)
	OR IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_BUNKER_BUNKER_STYLE_A_PURCHASED) != IS_PLAYER_BUNKER_STYLE_OPTION_A_PURCHASED(thisBunker.pOwner)
	OR IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_BUNKER_BUNKER_STYLE_B_PURCHASED) != IS_PLAYER_BUNKER_STYLE_OPTION_B_PURCHASED(thisBunker.pOwner)
	OR IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_BUNKER_BUNKER_STYLE_C_PURCHASED) != IS_PLAYER_BUNKER_STYLE_OPTION_C_PURCHASED(thisBunker.pOwner)
		RETURN TRUE
	ENDIF
	
	#IF IS_DEBUG_BUILD
	CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_BUNKER - PERFORM_BUNKER_MAZEBANK_WEBSITE_UPGRADE - Returning FALSE.")
	#ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_PLAYER_WARP_DURING_BUNKER_UPGRADE(PLAYER_INDEX playerID)

	// Only warp owner for Maze Bank upgrades - don't warp for laptop upgrades
	// Warp remote players no matter the upgrade
	
	IF g_bResetAllBunkerPedActivities
		RETURN FALSE
	ENDIF
	
	IF playerID = thisBunker.pOwner
		IF PERFORM_BUNKER_MAZEBANK_WEBSITE_UPGRADE()
			RETURN TRUE
		ENDIF
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_PLAYER_PLAYING_LAPTOP_ANIM(PLAYER_INDEX playerID)
	IF DOES_ENTITY_EXIST(GET_PLAYER_PED(playerID))
	AND NOT IS_PED_INJURED(GET_PLAYER_PED(playerID))
		IF IS_ENTITY_PLAYING_ANIM(GET_PLAYER_PED(playerID), "anim@amb@warehouse@laptop@", "enter")
		OR IS_ENTITY_PLAYING_ANIM(GET_PLAYER_PED(playerID), "anim@amb@warehouse@laptop@", "idle_a")
		OR IS_ENTITY_PLAYING_ANIM(GET_PLAYER_PED(playerID), "anim@amb@warehouse@laptop@", "idle_b")
		OR IS_ENTITY_PLAYING_ANIM(GET_PLAYER_PED(playerID), "anim@amb@warehouse@laptop@", "idle_c")
		OR IS_ENTITY_PLAYING_ANIM(GET_PLAYER_PED(playerID), "anim@amb@warehouse@laptop@", "idle_d")
		OR IS_ENTITY_PLAYING_ANIM(GET_PLAYER_PED(playerID), "anim@amb@warehouse@laptop@", "exit")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL GET_SAFE_BUNKER_SPAWN_POINT(INT iIndex, VECTOR &vSpawnPoint, FLOAT &fSpawnHeading)
	SWITCH thisBunker.eID
		CASE FACTORY_ID_BUNKER_1
		CASE FACTORY_ID_BUNKER_2
		CASE FACTORY_ID_BUNKER_3
		CASE FACTORY_ID_BUNKER_4
		CASE FACTORY_ID_BUNKER_5
		CASE FACTORY_ID_BUNKER_6
		CASE FACTORY_ID_BUNKER_7
//		CASE FACTORY_ID_BUNKER_8
		CASE FACTORY_ID_BUNKER_9
		CASE FACTORY_ID_BUNKER_10
		CASE FACTORY_ID_BUNKER_11
		CASE FACTORY_ID_BUNKER_12
			SWITCH iIndex
				CASE 0
				   	vSpawnPoint 	= <<897.8550, -3237.8262, -99.3738>>
				    fSpawnHeading 	= 95.7457
				    RETURN TRUE
				BREAK
				CASE 1
					vSpawnPoint 	= <<897.2599, -3239.1172, -99.4519>>
				    fSpawnHeading 	= 90.0000
				    RETURN TRUE
				BREAK
				CASE 2
				    vSpawnPoint 	= <<897.1686, -3236.1775, -99.3686>>
				    fSpawnHeading 	= 90.0000
				    RETURN TRUE
				BREAK
				CASE 3
				    vSpawnPoint 	= <<897.2173, -3234.4578, -99.2922>>
				    fSpawnHeading 	= 0.0000
				    RETURN TRUE
				BREAK
				CASE 4
				    vSpawnPoint 	= <<898.9223, -3234.9907, -99.2943>>
				    fSpawnHeading 	= 0.0000
				    RETURN TRUE
				BREAK
				CASE 5
				    vSpawnPoint 	= <<898.5253, -3236.5759, -99.3086>>
				    fSpawnHeading 	= 47.5200
				    RETURN TRUE
				BREAK
				CASE 6
				    vSpawnPoint 	= <<898.4705, -3238.2561, -99.3256>>
				    fSpawnHeading 	= 90.0000
				    RETURN TRUE
				BREAK
				CASE 7
				    vSpawnPoint 	= <<900.2222, -3236.7456, -99.2943>>
				    fSpawnHeading 	= 90.0000
				    RETURN TRUE
				BREAK
				CASE 8
				    vSpawnPoint 	= <<899.9286, -3238.4553, -99.2943>>
				    fSpawnHeading 	= 90.0000
				    RETURN TRUE
				BREAK
				CASE 9
				    vSpawnPoint 	= <<870.4803, -3234.6533, -99.2944>>
				    fSpawnHeading 	= 180.0000
				    RETURN TRUE
				BREAK
				CASE 10
				    vSpawnPoint 	= <<868.6393, -3234.7351, -99.2944>>
				    fSpawnHeading 	= 180.0000
				    RETURN TRUE
				BREAK
				CASE 11
				    vSpawnPoint 	= <<866.8218, -3234.6628, -99.2944>>
				    fSpawnHeading 	= 180.0000
				    RETURN TRUE
				BREAK
				CASE 12
				    vSpawnPoint 	= <<869.2257, -3233.2507, -99.2944>>
				    fSpawnHeading 	= 180.0000
				    RETURN TRUE
				BREAK
				CASE 13
				    vSpawnPoint 	= <<867.4655, -3233.1196, -99.2944>>
				    fSpawnHeading 	= 219.6000
				    RETURN TRUE
				BREAK
				CASE 14
				    vSpawnPoint 	= <<868.7527, -3229.9844, -99.2944>>
				    fSpawnHeading 	= 0.0000
				    RETURN TRUE
				BREAK
				CASE 15
				    vSpawnPoint 	= <<867.6417, -3231.2988, -99.2944>>
				    fSpawnHeading 	= 317.8800
				    RETURN TRUE
				BREAK
				CASE 16
				    vSpawnPoint 	= <<867.3656, -3229.4695, -99.2944>>
				    fSpawnHeading 	= 0.0000
				    RETURN TRUE
				BREAK
				CASE 17
				    vSpawnPoint 	= <<904.7391, -3205.0615, -98.9880>>
				    fSpawnHeading 	= 123.4800
				    RETURN TRUE
				BREAK
				CASE 18
				    vSpawnPoint 	= <<902.1839, -3209.0078, -98.9879>>
				    fSpawnHeading 	= 220.3200
				    RETURN TRUE
				BREAK
				CASE 19
				    vSpawnPoint 	= <<906.0095, -3207.2388, -98.9880>>
				    fSpawnHeading 	= 86.7600
				    RETURN TRUE
				BREAK
				CASE 20
				    vSpawnPoint 	= <<902.4183, -3207.6958, -98.9879>>
				    fSpawnHeading 	= 201.9600
				    RETURN TRUE
				BREAK
				CASE 21
				    vSpawnPoint 	= <<902.6182, -3205.5857, -98.9880>>
				    fSpawnHeading 	= 22.6800
				    RETURN TRUE
				BREAK
				CASE 22
				    vSpawnPoint 	= <<900.3362, -3204.3564, -98.9879>>
				    fSpawnHeading 	= 0.0000
				    RETURN TRUE
				BREAK
				CASE 23
				    vSpawnPoint 	= <<902.2871, -3204.1162, -98.9880>>
				    fSpawnHeading 	= 30.0000
				    RETURN TRUE
				BREAK
				CASE 24
				    vSpawnPoint 	= <<903.5424, -3208.2295, -98.9879>>
				    fSpawnHeading 	= 162.3600
				    RETURN TRUE
				BREAK
				CASE 25
				    vSpawnPoint 	= <<893.9901, -3189.7312, -98.8880>>
				    fSpawnHeading 	= 101.5200
				    RETURN TRUE
				BREAK
				CASE 26
				    vSpawnPoint 	= <<888.3716, -3192.9780, -100.0151>>
				    fSpawnHeading 	= 180.0000
				    RETURN TRUE
				BREAK
				CASE 27
				    vSpawnPoint 	= <<890.4807, -3191.5576, -98.8880>>
				    fSpawnHeading 	= 71.6400
				    RETURN TRUE
				BREAK
				CASE 28
				    vSpawnPoint 	= <<890.7574, -3190.0024, -98.8880>>
				    fSpawnHeading 	= 117.7200
				    RETURN TRUE
				BREAK
				CASE 29
				    vSpawnPoint 	= <<894.2480, -3191.3960, -98.8880>>
				    fSpawnHeading 	= 79.2000
				    RETURN TRUE
				BREAK
				CASE 30
				    vSpawnPoint 	= <<886.9427, -3191.3127, -100.0150>>
				    fSpawnHeading 	= 180.0000
				    RETURN TRUE
				BREAK
				CASE 31
				    vSpawnPoint 	= <<886.8383, -3192.8450, -100.0150>>
				    fSpawnHeading 	= 180.0000
				    RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC SET_UP_CUSTOM_SPAWN_POINTS_AFTER_BUNKER_UPGRADE()
	IF NOT g_SpawnData.bUseCustomSpawnPoints
		INT i
		VECTOR vSpawnPoint
		VECTOR vPlayerPos = GET_PLAYER_COORDS(PLAYER_ID())
		FLOAT fSpawnHeading
		FLOAT fWeight
//		FLOAT fPlayerHeading
		FLOAT fMaxDist
		SIMPLE_INTERIOR_DETAILS details
		
		USE_CUSTOM_SPAWN_POINTS(TRUE, FALSE, TRUE, 0.01, 2, DEFAULT, DEFAULT, 0.01, DEFAULT, DEFAULT, TRUE, TRUE)
		
//		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
//			fPlayerHeading = GET_ENTITY_HEADING(PLAYER_PED_ID())
//		ENDIF
		
		GET_SIMPLE_INTERIOR_DETAILS(thisBunker.eSimpleInteriorID, details)
		
		WHILE GET_SAFE_BUNKER_SPAWN_POINT(i, vSpawnPoint, fSpawnHeading)
			//vSpawnPoint = TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS(vSpawnPoint, details)
			
			IF VDIST(vSpawnPoint, vPlayerPos) > fMaxDist
				fMaxDist = VDIST(vSpawnPoint, vPlayerPos)
			ENDIF
			
			i += 1
		ENDWHILE
		
		i = 0
		
		WHILE GET_SAFE_BUNKER_SPAWN_POINT(i, vSpawnPoint, fSpawnHeading)
			//vSpawnPoint = TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS(vSpawnPoint, details)
			fWeight = 1.0 - (VDIST(vPlayerPos, vSpawnPoint) / fMaxDist)
			
			ADD_CUSTOM_SPAWN_POINT(vSpawnPoint, fSpawnHeading, fWeight)
			
			i += 1
		ENDWHILE
	ENDIF
ENDPROC

PROC PERFORM_BUNKER_UPGRADE(FACTORY_TYPE eFactoryType, FACTORY_UPGRADE_ID eUpgradeID)
	UNUSED_PARAMETER(eUpgradeID)

	SWITCH thisBunker.iUpgradeState
		CASE BUNKER_UPGRADE_STATE_FADE_OUT
			PRINTLN("AM_MP_BUNKER - PERFORM_FACTORY_UPGRADE - BUNKER_UPGRADE_STATE_FADE_OUT")
			
			IF thisBunker.pOwner = PLAYER_ID()
			AND NOT PERFORM_BUNKER_MAZEBANK_WEBSITE_UPGRADE()
				IF NOT IS_AUDIO_SCENE_ACTIVE("FAMILY_1_MOD_SHOP_INTRO_SCENE")
					START_AUDIO_SCENE("FAMILY_1_MOD_SHOP_INTRO_SCENE")
				ENDIF
			ENDIF
			
			IF NOT IS_SCREEN_FADED_OUT()
			AND NOT IS_SCREEN_FADING_OUT()
			AND NOT IS_PLAYER_PLAYING_LAPTOP_ANIM(thisBunker.pOwner)
				PRINTLN("AM_MP_BUNKER - PERFORM_BUNKER_UPGRADE - fade out and remove control")
				
				DO_SCREEN_FADE_OUT(500)
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			ENDIF
			
			IF IS_SCREEN_FADED_OUT()
//				RESET_PED_BUNKER_ACTIVITIES(activityControllerStruct, activityCheck, pedLocalVariationsStruct, interiorStruct)
				g_SimpleInteriorData.bForceCCTVCleanup = TRUE
				g_SimpleInteriorData.bForceShootingRangeCleanup = TRUE
				
				PRINTLN("AM_MP_BUNKER - PERFORM_BUNKER_UPGRADE - Switching state to: BUNKER_UPGRADE_STATE_WARP_PLAYER")
				
				thisBunker.iUpgradeState = BUNKER_UPGRADE_STATE_WARP_PLAYER
			ENDIF
		BREAK
		
		CASE BUNKER_UPGRADE_STATE_WARP_PLAYER
			PRINTLN("AM_MP_BUNKER - PERFORM_BUNKER_UPGRADE - BUNKER_UPGRADE_STATE_WARP_PLAYER")
			
			IF IS_BROWSER_OPEN()
				CLOSE_WEB_BROWSER()
			ENDIF
			
			IF SHOULD_PLAYER_WARP_DURING_BUNKER_UPGRADE(PLAYER_ID())
				SET_UP_CUSTOM_SPAWN_POINTS_AFTER_BUNKER_UPGRADE()
				
				IF WARP_TO_SPAWN_LOCATION(SPAWN_LOCATION_CUSTOM_SPAWN_POINTS, FALSE, FALSE, FALSE, FALSE, DEFAULT, DEFAULT, FALSE)
					CLEAR_CUSTOM_SPAWN_POINTS()
					USE_CUSTOM_SPAWN_POINTS(FALSE)
					
					PRINTLN("AM_MP_BUNKER - PERFORM_BUNKER_UPGRADE - Warp completed - Switching state to: BUNKER_UPGRADE_STATE_ACTIVATE_ENTITY_SETS")
					
					thisBunker.iUpgradeState = BUNKER_UPGRADE_STATE_ACTIVATE_ENTITY_SETS
				ENDIF
			ELSE
				PRINTLN("AM_MP_BUNKER - PERFORM_BUNKER_UPGRADE - Switching state to: BUNKER_UPGRADE_STATE_ACTIVATE_ENTITY_SETS")
				
				thisBunker.iUpgradeState = BUNKER_UPGRADE_STATE_ACTIVATE_ENTITY_SETS
			ENDIF
		BREAK
		
		CASE BUNKER_UPGRADE_STATE_ACTIVATE_ENTITY_SETS
			PRINTLN("AM_MP_BUNKER - PERFORM_BUNKER_UPGRADE - BUNKER_UPGRADE_STATE_ACTIVATE_ENTITY_SETS")
			
			SIMPLE_INTERIOR_SET_UPDATE_EXIT_LOCATE_COORDS(TRUE)
			
			IF IS_VALID_INTERIOR(thisBunker.BunkerInteriorID)
				SWITCH eFactoryType
					CASE FACTORY_TYPE_WEAPONS

						IF IS_PLAYER_BUNKER_STYLE_OPTION_A_PURCHASED(thisBunker.pOwner)
							REMOVE_ENTITY_SET(thisBunker.eID, "bunker_style_b")
							REMOVE_ENTITY_SET(thisBunker.eID, "bunker_style_c")
							ADD_ENTITY_SET(thisBunker.eID, "bunker_style_a")
						ENDIF
						
						IF IS_PLAYER_BUNKER_STYLE_OPTION_B_PURCHASED(thisBunker.pOwner)
							REMOVE_ENTITY_SET(thisBunker.eID, "bunker_style_a")
							REMOVE_ENTITY_SET(thisBunker.eID, "bunker_style_c")
							ADD_ENTITY_SET(thisBunker.eID, "bunker_style_b")
						ENDIF
						
						IF IS_PLAYER_BUNKER_STYLE_OPTION_C_PURCHASED(thisBunker.pOwner)
							REMOVE_ENTITY_SET(thisBunker.eID, "bunker_style_a")
							REMOVE_ENTITY_SET(thisBunker.eID, "bunker_style_b")
							ADD_ENTITY_SET(thisBunker.eID, "bunker_style_c")
						ENDIF

						IF DOES_PLAYER_OWN_FACTORY_UPGRADE(thisBunker.pOwner, thisBunker.eID, UPGRADE_ID_EQUIPMENT)
							REMOVE_ENTITY_SET(thisBunker.eID, "standard_bunker_set")
							ADD_ENTITY_SET(thisBunker.eID, "upgrade_bunker_set")
						ENDIF
					
						IF DOES_PLAYER_OWN_FACTORY_UPGRADE(thisBunker.pOwner, thisBunker.eID, UPGRADE_ID_SECURITY)
							REMOVE_ENTITY_SET(thisBunker.eID, "standard_security_set")
							ADD_ENTITY_SET(thisBunker.eID, "security_upgrade")							
						ENDIF
						
						IF IS_PLAYER_BUNKER_SAVEBED_PURCHASED(thisBunker.pOwner)
							REMOVE_ENTITY_SET(thisBunker.eID, "Office_blocker_set")
							ADD_ENTITY_SET(thisBunker.eID, "Office_Upgrade_set")
						ENDIF
						
						IF IS_PLAYER_BUNKER_FIRING_RANGE_BLACK_PURCHASED(thisBunker.pOwner)
						OR IS_PLAYER_BUNKER_FIRING_RANGE_WHITE_PURCHASED(thisBunker.pOwner)
							REMOVE_ENTITY_SET(thisBunker.eID, "gun_range_blocker_set")
							REMOVE_ENTITY_SET(thisBunker.eID, "gun_wall_blocker")
							ADD_ENTITY_SET(thisBunker.eID, "gun_range_lights")
						ENDIF
						
						IF IS_PLAYER_BUNKER_GUNLOCKER_PURCHASED(thisBunker.pOwner)
							ADD_ENTITY_SET(thisBunker.eID, "gun_locker_upgrade")
						ENDIF
						
						IF GET_FACTORY_PRODUCTION_MODE(thisBunker.pOwner, thisBunker.eID) != PRODUCTION_MODE_GOODS 
							ADD_ENTITY_SET(thisBunker.eID, "Gun_schematic_set")
						ELSE
							REMOVE_ENTITY_SET(thisBunker.eID, "Gun_schematic_set")
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
			
			IF g_bResetAllBunkerPedActivities
				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "RESET_PED_BUNKER_ACTIVITIES here")
				RESET_PED_BUNKER_ACTIVITIES(activityControllerStruct, activityCheck, pedLocalVariationsStruct, interiorStruct, bunkerFactoryActivityStruct, serverBD.serverInteriorStruct)
				g_bResetAllBunkerPedActivities = FALSE
			ENDIF
			
			IF (IS_PLAYER_BUNKER_TRANSPORTATION_STANDARD_PURCHASED(thisBunker.pOwner)
			OR (IS_PLAYER_BUNKER_TRANSPORTATION_UPDATED_PURCHASED(thisBunker.pOwner)
			AND NOT g_sMPTunables.BGR_DISABLE_CADDY_2))
			AND NETWORK_IS_HOST_OF_THIS_SCRIPT()
				PRINTLN("AM_MP_BUNKER - PERFORM_BUNKER_UPGRADE - Switching state to: BUNKER_UPGRADE_STATE_REQUEST_VEHICLE_MODELS")
				thisBunker.iUpgradeState = BUNKER_UPGRADE_STATE_REQUEST_VEHICLE_MODELS
			ELSE
				PRINTLN("AM_MP_BUNKER - PERFORM_BUNKER_UPGRADE - Switching state to: BUNKER_UPGRADE_STATE_REFRESH_INTERIOR")
				thisBunker.iUpgradeState = BUNKER_UPGRADE_STATE_REFRESH_INTERIOR
			ENDIF
		BREAK
		
		CASE BUNKER_UPGRADE_STATE_REQUEST_VEHICLE_MODELS
			PRINTLN("AM_MP_BUNKER - PERFORM_BUNKER_UPGRADE - BUNKER_UPGRADE_STATE_REQUEST_VEHICLE_MODELS")
			
			MODEL_NAMES eCaddyModel
			eCaddyModel = CADDY2
			IF IS_PLAYER_BUNKER_TRANSPORTATION_UPDATED_PURCHASED(thisBunker.pOwner)
				eCaddyModel = CADDY3
			ENDIF
			
			REQUEST_MODEL(eCaddyModel)
			IF HAS_MODEL_LOADED(eCaddyModel)
				IF ARE_CADDIES_CLEANED_UP()
					PRINTLN("AM_MP_BUNKER - PERFORM_BUNKER_UPGRADE - Switching state to: BUNKER_UPGRADE_STATE_CREATE_VEHICLES")
					thisBunker.iUpgradeState = BUNKER_UPGRADE_STATE_CREATE_VEHICLES
				ENDIF
			ENDIF
		BREAK
		
		CASE BUNKER_UPGRADE_STATE_CREATE_VEHICLES
			PRINTLN("AM_MP_BUNKER - PERFORM_BUNKER_UPGRADE - BUNKER_UPGRADE_STATE_CREATE_VEHICLES")
			
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				IF CREATE_BUNKER_CADDIES()
					PRINTLN("AM_MP_BUNKER - PERFORM_BUNKER_UPGRADE - [0] Switching state to: BUNKER_UPGRADE_STATE_REFRESH_INTERIOR")
					thisBunker.iUpgradeState = BUNKER_UPGRADE_STATE_REFRESH_INTERIOR
				ENDIF
			ENDIF
		BREAK
		
		CASE BUNKER_UPGRADE_STATE_REFRESH_INTERIOR
			PRINTLN("AM_MP_BUNKER - PERFORM_BUNKER_UPGRADE - BUNKER_UPGRADE_STATE_REFRESH_INTERIOR")
			
			REFRESH_INTERIOR(thisBunker.BunkerInteriorID)
			
			IF IS_PLAYER_GUNRUNNING_TRUCK_PURCHASED(thisBunker.pOwner)
				PRINTLN("AM_MP_BUNKER - PERFORM_BUNKER_UPGRADE - clear truck area")
				
				CLEAR_AREA(<<847.767,-3241.447,-98.493>>, 5.0, TRUE)	//hauler2
				CLEAR_AREA(<<840.395,-3237.460,-97.055>>, 7.5, TRUE)	//trailerlarge
			ENDIF
			
			PRINTLN("AM_MP_BUNKER - PERFORM_BUNKER_UPGRADE - Switching state to: BUNKER_UPGRADE_STATE_FADE_IN")
			
//			RESET_PED_ACTIVITIES(activityControllerStruct, activityCheck, pedLocalVariationsStruct)
			
			thisBunker.iUpgradeState = BUNKER_UPGRADE_STATE_FADE_IN
		BREAK
		
		CASE BUNKER_UPGRADE_STATE_FADE_IN
			PRINTLN("AM_MP_BUNKER - PERFORM_BUNKER_UPGRADE - BUNKER_UPGRADE_STATE_FADE_IN")
			
			IF IS_INTERIOR_READY(thisBunker.BunkerInteriorID)
				PRINTLN("AM_MP_BUNKER - PERFORM_BUNKER_UPGRADE - interior is ready")
				
				IF IS_SCREEN_FADED_OUT()
					PRINTLN("AM_MP_BUNKER - PERFORM_BUNKER_UPGRADE - screen has faded out")
					
					IF HAS_NET_TIMER_EXPIRED(thisBunker.upgradeTimer, ciBIKER_UPGRADE_DELAY)
						PRINTLN("AM_MP_BUNKER - PERFORM_BUNKER_UPGRADE - net timer has expired")
						
						SET_LOCAL_PLAYER_BROADCAST_BIT(BS_BUNKER_PERSONAL_QUATRS_PURCHASED, IS_PLAYER_BUNKER_SAVEBED_PURCHASED(thisBunker.pOwner))
						SET_LOCAL_PLAYER_BROADCAST_BIT(BS_BUNKER_CADDY_STANDARD_PURCHASED, IS_PLAYER_BUNKER_TRANSPORTATION_STANDARD_PURCHASED(thisBunker.pOwner))
						SET_LOCAL_PLAYER_BROADCAST_BIT(BS_BUNKER_CADDY_UPGRADED_PURCHASED, IS_PLAYER_BUNKER_TRANSPORTATION_UPDATED_PURCHASED(thisBunker.pOwner))
						SET_LOCAL_PLAYER_BROADCAST_BIT(BS_BUNKER_SHOOTING_RANGE_BLACK_PURCHASED, IS_PLAYER_BUNKER_FIRING_RANGE_BLACK_PURCHASED(thisBunker.pOwner))
						SET_LOCAL_PLAYER_BROADCAST_BIT(BS_BUNKER_SHOOTING_RANGE_WHITE_PURCHASED, IS_PLAYER_BUNKER_FIRING_RANGE_WHITE_PURCHASED(thisBunker.pOwner))
						SET_LOCAL_PLAYER_BROADCAST_BIT(BS_BUNKER_GUN_LOCKER_PURCHASED, IS_PLAYER_BUNKER_GUNLOCKER_PURCHASED(thisBunker.pOwner))
						SET_LOCAL_PLAYER_BROADCAST_BIT(BS_BUNKER_BUNKER_STYLE_A_PURCHASED, IS_PLAYER_BUNKER_STYLE_OPTION_A_PURCHASED(thisBunker.pOwner))
						SET_LOCAL_PLAYER_BROADCAST_BIT(BS_BUNKER_BUNKER_STYLE_B_PURCHASED, IS_PLAYER_BUNKER_STYLE_OPTION_B_PURCHASED(thisBunker.pOwner))
						SET_LOCAL_PLAYER_BROADCAST_BIT(BS_BUNKER_BUNKER_STYLE_C_PURCHASED, IS_PLAYER_BUNKER_STYLE_OPTION_C_PURCHASED(thisBunker.pOwner))
						
						DO_SCREEN_FADE_IN(500)
						NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						
						IF thisBunker.pOwner = PLAYER_ID()
							SET_PLAYER_HAS_JUST_PURCHASED_FACTORY_UPGRADE(thisBunker.pOwner, thisBunker.eID, thisBunker.eUpgradeID, FALSE)
						ENDIF
						
						CLEAR_BIT(thisBunker.iBS, BS_BUNKER_CALLED_CLEAR_HELP)
						thisBunker.bApplyUpgrade = FALSE
						thisBunker.iUpgradeState = BUNKER_UPGRADE_STATE_FADE_OUT
						RESET_NET_TIMER(thisBunker.upgradeTimer)
						
						IF IS_AUDIO_SCENE_ACTIVE("FAMILY_1_MOD_SHOP_INTRO_SCENE")
							SET_BIT(thisBunker.iBS2, BS_2_BUNKER_TERMINATE_UPGRADE_AUDIO_SCENE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC RUN_CHECKS_FOR_ENTITY_SET_MISMATCH(PLAYER_INDEX pOwner, FACTORY_ID eFactory)
	IF pOwner != INVALID_PLAYER_INDEX()
	AND pOwner != PLAYER_ID()
		IF (GET_FRAME_COUNT() % 10 = 0)
			IF IS_PLAYER_BUNKER_STYLE_OPTION_A_PURCHASED(thisBunker.pOwner)
			AND NOT IS_INTERIOR_ENTITY_SET_ACTIVE(thisBunker.BunkerInteriorID, "bunker_style_a")
				thisBunker.bApplyUpgrade = TRUE
				
				PRINTLN("AM_MP_BUNKER - RUN_CHECKS_FOR_ENTITY_SET_MISMATCH - Begin style option A.")
			ELIF IS_PLAYER_BUNKER_STYLE_OPTION_B_PURCHASED(thisBunker.pOwner)
			AND NOT IS_INTERIOR_ENTITY_SET_ACTIVE(thisBunker.BunkerInteriorID, "bunker_style_b")
				thisBunker.bApplyUpgrade = TRUE
				
				PRINTLN("AM_MP_BUNKER - RUN_CHECKS_FOR_ENTITY_SET_MISMATCH - Begin style option B.")
			ELIF IS_PLAYER_BUNKER_STYLE_OPTION_C_PURCHASED(thisBunker.pOwner)
			AND NOT IS_INTERIOR_ENTITY_SET_ACTIVE(thisBunker.BunkerInteriorID, "bunker_style_c")
				thisBunker.bApplyUpgrade = TRUE
				
				PRINTLN("AM_MP_BUNKER - RUN_CHECKS_FOR_ENTITY_SET_MISMATCH - Begin style option C.")
			ENDIF
			
			IF DOES_PLAYER_OWN_FACTORY_UPGRADE(pOwner, eFactory, UPGRADE_ID_SECURITY)
			AND NOT IS_INTERIOR_ENTITY_SET_ACTIVE(thisBunker.BunkerInteriorID, "security_upgrade")
				thisBunker.eUpgradeID = UPGRADE_ID_SECURITY
				thisBunker.bApplyUpgrade = TRUE
				SIMPLE_INTERIOR_SET_UPDATE_EXIT_LOCATE_COORDS(TRUE)
				
				PRINTLN("AM_MP_BUNKER - RUN_CHECKS_FOR_ENTITY_SET_MISMATCH - begin weapons security upgrade")
			ENDIF
			IF DOES_PLAYER_OWN_FACTORY_UPGRADE(pOwner, eFactory, UPGRADE_ID_EQUIPMENT)
			AND NOT IS_INTERIOR_ENTITY_SET_ACTIVE(thisBunker.BunkerInteriorID, "Upgrade_Bunker_Set")
				thisBunker.eUpgradeID = UPGRADE_ID_EQUIPMENT
				thisBunker.bApplyUpgrade = TRUE
				
				PRINTLN("AM_MP_BUNKER - RUN_CHECKS_FOR_ENTITY_SET_MISMATCH - begin weapons equipment upgrade")
			ENDIF
			IF IS_PLAYER_BUNKER_SAVEBED_PURCHASED(thisBunker.pOwner)
			AND NOT IS_INTERIOR_ENTITY_SET_ACTIVE(thisBunker.BunkerInteriorID, "Office_Upgrade_set")
				thisBunker.bApplyUpgrade = TRUE
				
				PRINTLN("AM_MP_BUNKER - RUN_CHECKS_FOR_ENTITY_SET_MISMATCH - begin weapons personal quarters")
			ENDIF
			IF (IS_PLAYER_BUNKER_FIRING_RANGE_BLACK_PURCHASED(thisBunker.pOwner)
			OR IS_PLAYER_BUNKER_FIRING_RANGE_WHITE_PURCHASED(thisBunker.pOwner))
			AND (IS_INTERIOR_ENTITY_SET_ACTIVE(thisBunker.BunkerInteriorID, "gun_range_blocker_set")
			OR IS_INTERIOR_ENTITY_SET_ACTIVE(thisBunker.BunkerInteriorID, "gun_wall_blocker")
			OR NOT IS_INTERIOR_ENTITY_SET_ACTIVE(thisBunker.BunkerInteriorID, "gun_range_lights"))
				thisBunker.bApplyUpgrade = TRUE
				
				PRINTLN("AM_MP_BUNKER - RUN_CHECKS_FOR_ENTITY_SET_MISMATCH - begin weapons shooting range")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_BUNKER_UPGRADES()
	IF IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
	OR thisBunker.bPlayerLeftTheFactory
		#IF IS_DEBUG_BUILD
		IF (GET_FRAME_COUNT() % 120) = 0
			CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_BUNKER - MAINTAIN_BUNKER_UPGRADES - Exiting MAINTAIN_BUNKER_UPGRADES")
		ENDIF
		#ENDIF
		EXIT
	ENDIF
	
	IF NOT thisBunker.bApplyUpgrade
		#IF IS_DEBUG_BUILD
		IF (GET_FRAME_COUNT() % 60) = 0
			CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_BUNKER - MAINTAIN_BUNKER_UPGRADES - Running checks for entity set mismatches.")
		ENDIF
		#ENDIF
		RUN_CHECKS_FOR_ENTITY_SET_MISMATCH(thisBunker.pOwner, thisBunker.eID)
	ENDIF
	
	IF NOT thisBunker.bApplyUpgrade
	AND g_bResetAllBunkerPedActivities
		thisBunker.bApplyUpgrade = TRUE
	ENDIF
	
	IF HAS_PLAYER_JUST_PURCHASED_FACTORY_UPGRADE(thisBunker.pOwner, thisBunker.eID, thisBunker.eUpgradeID)
	OR PERFORM_BUNKER_MAZEBANK_WEBSITE_UPGRADE()
		IF NOT thisBunker.bApplyUpgrade
			thisBunker.bApplyUpgrade = TRUE
			SIMPLE_INTERIOR_SET_UPDATE_EXIT_LOCATE_COORDS(TRUE)

			#IF IS_DEBUG_BUILD
			IF (GET_FRAME_COUNT() % 60) = 0
				CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_BUNKER - MAINTAIN_BUNKER_UPGRADES - thisBunker.bApplyUpgrade = ", BOOL_TO_INT(thisBunker.bApplyUpgrade))
			ENDIF
			#ENDIF
		ENDIF
	ENDIF
	
	IF thisBunker.bApplyUpgrade
		#IF IS_DEBUG_BUILD
		IF (GET_FRAME_COUNT() % 60) = 0
			CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_BUNKER - MAINTAIN_BUNKER_UPGRADES - Performing Bunker upgrade.")
		ENDIF
		#ENDIF
		PERFORM_BUNKER_UPGRADE(thisBunker.eFactoryType, thisBunker.eUpgradeID)
	ENDIF
ENDPROC

PROC INITIALISE_BUNKER_ENTITY_SETS(INT iForceStage = -1)
	thisBunker.BunkerInteriorID = GET_INTERIOR_AT_COORDS_WITH_TYPE(GET_FACTORY_COORDS(thisBunker.eID), GET_FACTORY_INTERIOR_NAME(thisBunker.eID))
	
	IF IS_VALID_INTERIOR(thisBunker.BunkerInteriorID)
		INT iStage = serverBD.iProductionStage
		
		IF iForceStage > -1
			iStage = iForceStage
		ENDIF
		
		SWITCH thisBunker.eFactoryType
			CASE FACTORY_TYPE_WEAPONS
				ACTIVATE_WEAPON_STAGE(thisBunker.pOwner, thisBunker.iSaveSlot, iStage)
			BREAK
		ENDSWITCH
		
		//Special check for the Gun_schematic_set as this needs to match even if the player updates their staff asignment whilst inside
		//Do not add this to the entity set mismatch checks. We don't want to do this entity set change whilst inside
		IF HAS_HOST_ASSIGNED_OWNERS_PRODUCTION_MODE()
			
			IF GET_HOST_ASSIGNED_PRODUCTION_MODE() != PRODUCTION_MODE_GOODS
				ADD_ENTITY_SET(thisBunker.eID, "Gun_schematic_set")
			ELSE
				REMOVE_ENTITY_SET(thisBunker.eID, "Gun_schematic_set")
			ENDIF
			
		ELSE
			FACTORY_PRODUCTION_MODE eMode = GET_FACTORY_PRODUCTION_MODE(thisBunker.pOwner, thisBunker.eID)
			
			IF eMode != PRODUCTION_MODE_GOODS
				ADD_ENTITY_SET(thisBunker.eID, "Gun_schematic_set")
			ELSE
				REMOVE_ENTITY_SET(thisBunker.eID, "Gun_schematic_set")
			ENDIF
			
			SET_HOST_ASSIGNED_OWNERS_PRODUCTION_MODE(eMode)
		ENDIF
		
		SIMPLE_INTERIOR_SET_UPDATE_EXIT_LOCATE_COORDS(TRUE)
	ENDIF
ENDPROC

PROC CLEANUP_BUNKER_ENTITY_SETS()
	IF IS_VALID_INTERIOR(thisBunker.BunkerInteriorID)
		INT iESIndex
		TEXT_LABEL_63 tlESName
	
		REPEAT ciMAX_FACTORY_ENTITY_SETS iESIndex
			IF GET_ALL_FACTORY_ENTITY_SETS(thisBunker.eFactoryType, iESIndex, tlESName)
				IF IS_INTERIOR_ENTITY_SET_ACTIVE(thisBunker.BunkerInteriorID, tlESName)
					DEACTIVATE_INTERIOR_ENTITY_SET(thisBunker.BunkerInteriorID, tlESName)
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

PROC INITIALISE_BUNKER_PRODUCTION()
	#IF IS_DEBUG_BUILD
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[SIMPLE_INTERIOR] AM_MP_BUNKER - INITIALISE_BUNKER_PRODUCTION - Initialising bunker production.")
	#ENDIF

	IF IS_FACTORY_OWNER_ID_VALID(NATIVE_TO_INT(thisBunker.pOwner))
		INITIALISE_BUNKER_ENTITY_SETS()
		REFRESH_INTERIOR(thisBunker.BunkerInteriorID)
	ENDIF
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ DEBUG WIDGETS ╞══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

#IF IS_DEBUG_BUILD
PROC CREATE_DEBUG_WIDGETS()
	
	START_WIDGET_GROUP("AM_MP_BUNKER")
		
		ADD_WIDGET_INT_READ_ONLY("BS2_SIMPLE_INTERIOR_BUNKER_CRATES_SPAWNED: ", debugData.iCratesSpawnedBS)
		ADD_WIDGET_INT_READ_ONLY("BS2_SIMPLE_INTERIOR_SHOOTING_RANGE_TARGETS_SPAWNED: ", debugData.iShootingRangeBS)
		
		START_WIDGET_GROUP("Bunker Peds")
			ADD_WIDGET_BOOL("Reset Peds", bResetPeds)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Product Widgets")
			START_WIDGET_GROUP("Totals")
				ADD_WIDGET_INT_READ_ONLY("Stock", thisBunker.iProductSlot)
				ADD_WIDGET_INT_SLIDER("Product Amount To Add", debugData.iProductAmountToAdd, 1, GET_FACTORY_PRODUCT_CAPACITY(thisBunker.eID), 1)
				ADD_WIDGET_BOOL("Add Product", debugData.bAddProduct)
				ADD_WIDGET_INT_SLIDER("Product Amount To Remove", debugData.iProductAmountToRemove, 1, GET_FACTORY_PRODUCT_CAPACITY(thisBunker.eID), 1)
				ADD_WIDGET_BOOL("Remove Product", debugData.bRemoveProduct)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Crates")
				START_WIDGET_GROUP("Spawn Crate")
					ADD_WIDGET_BOOL("Rifles  01a", debugData.bSpawnRifles01a)
					ADD_WIDGET_BOOL("Rifles  02a", debugData.bSpawnRifles02a)
					ADD_WIDGET_BOOL("Rifles  03a", debugData.bSpawnRifles03a)
					ADD_WIDGET_BOOL("Rifles  04a", debugData.bSpawnRifles04a)
					ADD_WIDGET_BOOL("Mixed   01a", debugData.bSpawnMixed01a)
					ADD_WIDGET_BOOL("Pistols 01a", debugData.bSpawnPistols01a)
					ADD_WIDGET_BOOL("Gunsmith Supply 01a", debugData.bSpawnGunsmithSupply01a)
					ADD_WIDGET_BOOL("Gunsmith Supply 02a", debugData.bSpawnGunsmithSupply02a)
					ADD_WIDGET_BOOL("Gunsmith Supply 03a", debugData.bSpawnGunsmithSupply03a)
				STOP_WIDGET_GROUP()
				START_WIDGET_GROUP("Crate Coords")
					ADD_WIDGET_VECTOR_SLIDER("Coords", debugData.vCrateCoords, -10000.0, 10000.0, 0.01)
				STOP_WIDGET_GROUP()
				START_WIDGET_GROUP("Crate Rotation")
					ADD_WIDGET_VECTOR_SLIDER("Rotation", debugData.vCrateRotation, -10000.0, 10000.0, 0.01)
				STOP_WIDGET_GROUP()
				START_WIDGET_GROUP("Spawn Multiple")
					ADD_WIDGET_INT_SLIDER("Amount To Spawn", debugData.iProductAmountToSpawn, 1, GET_FACTORY_PRODUCT_CAPACITY(thisBunker.eID), 1)
					ADD_WIDGET_BOOL("Spawn", debugData.bSpawnProduct)
					ADD_WIDGET_INT_SLIDER("Amount To Clear", debugData.iProductAmountToClear, 1, GET_FACTORY_PRODUCT_CAPACITY(thisBunker.eID), 1)
					ADD_WIDGET_BOOL("Clear", debugData.bClearProduct)
				STOP_WIDGET_GROUP()
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Tutorial")
				ADD_WIDGET_BOOL("Hide All Product", debugData.bHideAllProduct)
				ADD_WIDGET_BOOL("Show All Product", debugData.bShowAllProduct)
				ADD_WIDGET_BOOL("Create Demo Product", debugData.bCreateDemoProduct)
				ADD_WIDGET_BOOL("Activate Demo Product", debugData.bActivateDemoProduct)
				ADD_WIDGET_INT_SLIDER("Demo Product Amount", debugData.iDemoProductAmount, 0, GET_FACTORY_PRODUCT_CAPACITY(thisBunker.eID), 1)
				ADD_WIDGET_INT_SLIDER("Demo Product Time", debugData.iDemoProductTime, 0, 99999, 1)
				ADD_WIDGET_BOOL("Do Demo Product", debugData.bDoDemoProduct)
				ADD_WIDGET_BOOL("Deactivate Demo Product", debugData.bDeactivateDemoProduct)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Layouts")
				ADD_WIDGET_INT_SLIDER("Select Layout", debugData.iLayout, 1, 3, 1)
				ADD_WIDGET_BOOL("Set Layout", debugData.bSetLayout)
				ADD_WIDGET_INT_READ_ONLY("Current Layout", thisBunker.iProductLayout)
				ADD_WIDGET_BOOL("Generate Layout", debugData.bGenerateLayout)
				ADD_WIDGET_BOOL("Generate Random Layout", debugData.bGenerateRandomLayout)
				ADD_WIDGET_BOOL("Clear Layout", debugData.bClearLayout)
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Material Widgets")
			START_WIDGET_GROUP("Spawning")
				ADD_WIDGET_INT_SLIDER("Amount To Spawn", debugData.iMaterialAmountToSpawn, 1, ciMAX_FACTORY_MATERIAL_UNITS, 1)
				ADD_WIDGET_BOOL("Spawn", debugData.bSpawnMaterials)
				ADD_WIDGET_INT_SLIDER("Amount To Clear", debugData.iMaterialAmountToClear, 1, ciMAX_FACTORY_MATERIAL_UNITS, 1)
				ADD_WIDGET_BOOL("Clear", debugData.bClearMaterials)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Set Materials")
				ADD_WIDGET_INT_SLIDER("Amount To Give", debugData.iNewMaterials, 0, 100, 1)
				ADD_WIDGET_BOOL("Give", debugData.bGiveMaterials)
			STOP_WIDGET_GROUP()	
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Research Unlocks")
			ADD_WIDGET_INT_SLIDER("Research Completed Total", debugData.iResearchCompletedTotal, 0, ENUM_TO_INT(RESEARCH_ITEM_MAX), 1)
			ADD_WIDGET_BOOL("Set", debugData.bSetResearchCompletedTotal)
			ADD_WIDGET_INT_READ_ONLY("Current Total", debugData.iCurrentResearchTotal)
			ADD_WIDGET_BOOL("Unlock Item", debugData.bUnlockResearchItem)
			START_WIDGET_GROUP("Flow Unlocks")
				ADD_WIDGET_BOOL("Unlock Ballistic Equipment", debugData.bUnlockBallistic)
				ADD_WIDGET_BOOL("Unlock SAM Battery", debugData.bUnlockSAMBattery)
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		
		SIMPLE_CUTSCENE_CREATE_DEBUG_WIDGETS(thisBunker.introCutscene)
		
		ADD_WIDGET_BOOL("Start spawn activity", debugData.bStartSpawnActivity)
		
		ADD_WIDGET_BOOL("Test fade in player",debugData.bTestPlayerFade)
	STOP_WIDGET_GROUP()
	
ENDPROC

PROC INITIALISE_DEBUG_WIDGET_VALUES()
	debugData.vCrateCoords	 	= <<912.950, -3238.310, -99.290>>
	debugData.vCrateRotation 	= <<0.0, 0.0, 0.0>>
ENDPROC

FUNC MODEL_NAMES GET_BUNKER_PRODUCT_MODEL_DEBUG()

	MODEL_NAMES eCrateModel
	IF debugData.bSpawnRifles01a
		eCrateModel = GR_PROP_GR_CRATES_RIFLES_01A
	ELIF debugData.bSpawnRifles02a
		eCrateModel = GR_PROP_GR_CRATES_RIFLES_02A
	ELIF debugData.bSpawnRifles03a
		eCrateModel = GR_PROP_GR_CRATES_RIFLES_03A
	ELIF debugData.bSpawnRifles04a
		eCrateModel = GR_PROP_GR_CRATES_RIFLES_04A
	ELIF debugData.bSpawnMixed01a
		eCrateModel = GR_PROP_GR_CRATES_SAM_01A
	ELIF debugData.bSpawnPistols01a
		eCrateModel = GR_PROP_GR_CRATES_PISTOLS_01A
	ELIF debugData.bSpawnGunsmithSupply01a
		eCrateModel = GR_PROP_GR_GUNSMITHSUPL_01A
	ELIF debugData.bSpawnGunsmithSupply02a
		eCrateModel = GR_PROP_GR_GUNSMITHSUPL_02A
	ELIF debugData.bSpawnGunsmithSupply03a
		eCrateModel = GR_PROP_GR_GUNSMITHSUPL_03A
	ENDIF
	
	RETURN eCrateModel
ENDFUNC

FUNC BOOL SPAWN_BUNKER_PRODUCT_DEBUG()

	IF debugData.bSpawnRifles01a
	OR debugData.bSpawnRifles02a
	OR debugData.bSpawnRifles03a
	OR debugData.bSpawnRifles04a
	OR debugData.bSpawnMixed01a
	OR debugData.bSpawnPistols01a
	OR debugData.bSpawnGunsmithSupply01a
	OR debugData.bSpawnGunsmithSupply02a
	OR debugData.bSpawnGunsmithSupply03a
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC UPDATE_DEBUG_WIDGETS()
	
	// Bit sets
	debugData.iCratesSpawnedBS = BOOL_TO_INT(IS_BIT_SET(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_BUNKER_CRATES_SPAWNED))
	debugData.iShootingRangeBS = BOOL_TO_INT(IS_BIT_SET(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_SHOOTING_RANGE_TARGETS_SPAWNED))
	
	// Stock
	IF debugData.bAddProduct
		ADD_FINISHED_PRODUCT_TO_FACTORY(thisBunker.eID, debugData.iProductAmountToAdd)
		debugData.bAddProduct = FALSE
	ENDIF
	
	IF debugData.bRemoveProduct
		REMOVE_FINISHED_PRODUCT_FROM_FACTORY(thisBunker.eID, debugData.iProductAmountToRemove)
		debugData.bRemoveProduct = FALSE
	ENDIF
	
	// Crate Placement
	IF SPAWN_BUNKER_PRODUCT_DEBUG()
		IF NOT DOES_ENTITY_EXIST(debugData.oProductObj)
			debugData.oProductObj = CREATE_OBJECT(GET_BUNKER_PRODUCT_MODEL_DEBUG(), debugData.vCrateCoords, FALSE, FALSE)
		ENDIF
		SET_ENTITY_ROTATION(debugData.oProductObj, debugData.vCrateRotation)
		SET_ENTITY_COORDS(debugData.oProductObj, debugData.vCrateCoords, FALSE)
	ELSE
		SAFE_DELETE_OBJECT(debugData.oProductObj)
	ENDIF
	
	IF debugData.bTestPlayerFade
		FORCE_PLAYER_LOCALLY_VISIBLE_FOR_FADE()
		NETWORK_FADE_IN_ENTITY(PLAYER_PED_ID(), TRUE)
		debugData.bTestPlayerFade = FALSE
	ENDIF
	
	// Crate Spawning
	IF debugData.bSpawnProduct
		CREATE_ALL_OWNED_FACTORY_PRODUCTS(thisBunker.eID, debugData.iProductAmountToSpawn)
		debugData.bSpawnProduct = FALSE
	ENDIF
	
	IF debugData.bClearProduct
		CLEANUP_ALL_OWNED_FACTORY_PRODUCTS(debugData.iProductAmountToClear)
		debugData.bClearProduct = FALSE
	ENDIF
	
	// Tutorial
	IF debugData.bHideAllProduct
		HIDE_ALL_FACTORY_PRODUCT()
		debugData.bHideAllProduct = FALSE
	ENDIF
	
	IF debugData.bShowAllProduct
		SHOW_ALL_FACTORY_PRODUCT()
		debugData.bShowAllProduct = FALSE
	ENDIF
	
	IF debugData.bCreateDemoProduct
		CREATE_DEMO_PRODUCT()
		debugData.bCreateDemoProduct = FALSE
	ENDIF
	
	IF debugData.bActivateDemoProduct
		MAINTAIN_DEMO_PRODUCT()
	ENDIF
	
	IF debugData.bDeactivateDemoProduct
		CLEANUP_DEMO_PRODUCT()
		debugData.bDeactivateDemoProduct = FALSE
	ENDIF
	
	IF debugData.bDoDemoProduct
		SHOW_DEMO_PRODUCT(debugData.iDemoProductAmount, debugData.iDemoProductTime)
		debugData.bDoDemoProduct = FALSE
	ENDIF
	
	// Layouts
	IF debugData.bSetLayout
		thisBunker.iProductLayout = debugData.iLayout
		debugData.bSetLayout = FALSE
	ENDIF
	
	IF debugData.bGenerateLayout
		CREATE_ALL_FACTORY_PRODUCTS(thisBunker.eID)
		debugData.bGenerateLayout = FALSE
	ENDIF
	
	IF debugData.bGenerateRandomLayout
		CREATE_ALL_FACTORY_PRODUCTS(thisBunker.eID)
		debugData.bGenerateRandomLayout = FALSE
	ENDIF
	
	IF debugData.bClearLayout
		CLEANUP_ALL_FACTORY_PRODUCTS(thisBunker.eID)
		debugData.bClearLayout = FALSE
	ENDIF
	
	// Material Spawning
	IF debugData.bSpawnMaterials
		CREATE_ALL_OWNED_FACTORY_MATERIAL_PROPS(thisBunker.eID, debugData.iMaterialAmountToSpawn)
		debugData.bSpawnMaterials = FALSE
	ENDIF
	
	IF debugData.bClearMaterials
		CLEANUP_ALL_OWNED_FACTORY_MATERIAL_PROPS(debugData.iMaterialAmountToClear)
		debugData.bClearMaterials = FALSE
	ENDIF
	
	IF debugData.bGiveMaterials
		UPDATE_FACTORY_MATERIALS_TOTAL_BD(thisBunker.iSaveSlot, debugData.iNewMaterials, "AM_MP_BUNKER - UPDATE_DEBUG_WIDGETS()")
		GlobalplayerBD_FM[NATIVE_TO_INT(thisBunker.pOwner)].propertyDetails.bdFactoryData[BUNKER_SAVE_SLOT].iMaterialsTotalBlended = debugData.iNewMaterials
		debugData.bGiveMaterials = FALSE
	ENDIF
	
	// Research Unlocks
	IF debugData.bSetResearchCompletedTotal
		SET_PACKED_STAT_INT(PACKED_MP_INT_BUNKER_RESEARCH_COMPLETED_COUNTER, debugData.iResearchCompletedTotal)
		debugData.iCurrentResearchTotal = GET_PACKED_STAT_INT(PACKED_MP_INT_BUNKER_RESEARCH_COMPLETED_COUNTER)
		PRINTLN("[BUNKER_RESEARCH] USED debugData.bSetResearchCompletedTotal!")
		debugData.bSetResearchCompletedTotal = FALSE
	ENDIF
	
	IF debugData.bUnlockResearchItem
		SET_BUNKER_RESEARCH_ITEM_AS_UNLOCKED()
		debugData.bUnlockResearchItem = FALSE
	ENDIF
	
	IF debugData.bUnlockBallistic
		BUNKER_RESEARCH_ITEMS eItem = RESEARCH_ITEM_BALLISTIC_EQUIPMENT
		IF NOT GET_PACKED_STAT_BOOL(GET_RESEARCH_ITEM_PACKED_STAT_FROM_ENUM(eItem))
			TRIGGER_UNLOCK_MESSAGE_FOR_RESEARCH_ITEM(GET_RESEARCH_ITEM_NAME(eItem), GET_RESEARCH_ITEM_TEXT_MESSAGE_TYPE(eItem), TRUE, TRUE, FALSE)
			SET_PACKED_STAT_BOOL(GET_RESEARCH_ITEM_PACKED_STAT_FROM_ENUM(eItem), TRUE)
		ENDIF
		debugData.bUnlockBallistic = FALSE
	ENDIF
	
	IF debugData.bUnlockSAMBattery
		BUNKER_RESEARCH_ITEMS eItem = RESEARCH_ITEM_SAM_BATTERY
		IF NOT GET_PACKED_STAT_BOOL(GET_RESEARCH_ITEM_PACKED_STAT_FROM_ENUM(eItem))
			TRIGGER_UNLOCK_MESSAGE_FOR_RESEARCH_ITEM(GET_RESEARCH_ITEM_NAME(eItem), GET_RESEARCH_ITEM_TEXT_MESSAGE_TYPE(eItem))
			SET_PACKED_STAT_BOOL(GET_RESEARCH_ITEM_PACKED_STAT_FROM_ENUM(eItem), TRUE)
		ENDIF
		debugData.bUnlockSAMBattery = FALSE
	ENDIF
	
	SIMPLE_CUTSCENE_MAINTAIN_DEBUG_WIDGETS(thisBunker.introCutscene)
	
ENDPROC
#ENDIF

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════════╡ CLEANUP  ╞═══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC CLEANUP_CCTV(BOOL bReturnControl)
	CLEANUP_MP_CCTV_CLIENT(playerBD[NATIVE_TO_INT(PLAYER_ID())].MPCCTVClient, MPCCTVLocal, bReturnControl)
ENDPROC


PROC CLEAN_UP_BUNKER_GUN_LOCKER(VAULT_WEAPON_LOADOUT_CUSTOMIZATION& sWLoadoutCustomization,BOOL bForceCleanUp = FALSE)
	PRINTLN("Vault weapon loadout - CLEAN_UP_BUNKER_GUN_LOCKER")
	
	RELEASE_CONTEXT_INTENTION(sWLoadoutCustomization.iVaultWeaponContext)
	sWLoadoutCustomization.iVaultWeaponContext = NEW_CONTEXT_INTENTION
	sWLoadoutCustomization.iWeaponMenuCurrentItem = 0
	sWLoadoutCustomization.iWeaponMenuTopItem = 0
	sWLoadoutCustomization.iNumAvailableWeaponGroup = 0
	sWLoadoutCustomization.eWeaponCurrentMenu = VM_MAIN_MENU
	sWLoadoutCustomization.bMenuInitialised = FALSE
	sWLoadoutCustomization.bReBuildMenu = FALSE
	
	CLEAR_BIT(sWLoadoutCustomization.iLocalBS,VAULT_DOOR_ANIM_STAGE_FINISHED)
	CLEAR_BIT(sWLoadoutCustomization.iLocalBS,VAULT_GUN_DOOR_ANIM_RUNNING)
	CLEAR_BIT(sWLoadoutCustomization.iLocalBS, BS_GET_NUM_AVAILABLE_WEAPON_GROUP)
	
	INT iNumSubMenu
	FOR iNumSubMenu = 0 TO WEAPON_VAULT_TOTAL - 1
		CLEAR_BIT(sWLoadoutCustomization.iVaultWeaponBS,iNumSubMenu)
		CLEAR_BIT(sWLoadoutCustomization.iAvailableWeaponBS,iNumSubMenu)
		CLEAR_BIT(sWLoadoutCustomization.iHideAllWeaponBS,iNumSubMenu)
	ENDFOR

	IF bForceCleanUp
		INT iNumWeaponModels
		FOR iNumWeaponModels = 0 TO MAX_NUMBER_WEAPON_MODELS -1
			IF DOES_ENTITY_EXIST(sWLoadoutCustomization.weapons[iNumWeaponModels])
				DELETE_OBJECT(sWLoadoutCustomization.weapons[iNumWeaponModels])
			ENDIF
			IF IS_BIT_SET(sWLoadoutCustomization.iWeaponModelBS, iNumWeaponModels)
				CLEAR_BIT(sWLoadoutCustomization.iWeaponModelBS, iNumWeaponModels)
			ENDIF
		ENDFOR
		
		CLEAR_BIT(sWLoadoutCustomization.iLocalBS,VAULT_GUN_DOOR_ANIM_INITIALISED)
		CLEAR_BIT(sWLoadoutCustomization.iLocalBS,VAULT_GUN_DOOR_NET_RESERVED)
		CLEAR_BIT(sWLoadoutCustomization.iLocalBS,VAULT_DOOR_ANIM_STAGE_FINISHED)
		CLEAR_BIT(sWLoadoutCustomization.iLocalBS,OFFICE_GUN_VAULT_DOOR_CREATED)

		sWLoadoutCustomization.eCustomizationStage = VMC_STAGE_INIT	
	ELSE
		sWLoadoutCustomization.eCustomizationStage = VMC_STAGE_INIT
	ENDIF
ENDPROC

PROC OFFICE_SEAT_ACTIVITY_CLEAN_UP_BUNKER()
	
	OFFICE_SEAT_ACTIVITY_CLEAN_UP(activitySeatStruct, TRUE)
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_BUNKER - OFFICE_SEAT_ACTIVITY_CLEAN_UP_BUNKER")
	// Clean up previously here was duplicate but not complete of 
	// above function so we now call this here instead.

ENDPROC

PROC MAINTAIN_BUNKER_LAPTOP_SCREEN()
	
	SWITCH thisBunker.bunkerLaptopData.eLaptopState
		CASE LAPTOP_SCREEN_STATE_INIT
			
			REQUEST_STREAMED_TEXTURE_DICT("Prop_Screen_GR_Disruption")
			IF HAS_STREAMED_TEXTURE_DICT_LOADED("Prop_Screen_GR_Disruption")
				PRINTLN("MAINTAIN_BUNKER_LAPTOP_SCREEN - Changing to state: LAPTOP_SCREEN_STATE_LINK_RT")
				thisBunker.bunkerLaptopData.eLaptopState = LAPTOP_SCREEN_STATE_LINK_RT
			ENDIF
			
		BREAK
		CASE LAPTOP_SCREEN_STATE_LINK_RT
			IF NOT IS_NAMED_RENDERTARGET_REGISTERED("gr_bunker_laptop_01a")
				REGISTER_NAMED_RENDERTARGET("gr_bunker_laptop_01a")
				IF NOT IS_NAMED_RENDERTARGET_LINKED(INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_laptop_01a")))
					LINK_NAMED_RENDERTARGET(INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_laptop_01a")))
					IF thisBunker.bunkerLaptopData.iRenderTargetID = -1
						thisBunker.bunkerLaptopData.iRenderTargetID = GET_NAMED_RENDERTARGET_RENDER_ID("gr_bunker_laptop_01a")
					ENDIF
				ENDIF
			ENDIF
			
			PRINTLN("MAINTAIN_BUNKER_LAPTOP_SCREEN - Changing to state: LAPTOP_SCREEN_STATE_UPDATE_SCALEFORM - iRenderTargetID: ", thisBunker.bunkerLaptopData.iRenderTargetID)
			thisBunker.bunkerLaptopData.eLaptopState = LAPTOP_SCREEN_STATE_UPDATE_SCALEFORM
		BREAK
		CASE LAPTOP_SCREEN_STATE_UPDATE_SCALEFORM
			SET_TEXT_RENDER_ID(thisBunker.bunkerLaptopData.iRenderTargetID)
			SET_SCRIPT_GFX_ALIGN(UI_ALIGN_IGNORE, UI_ALIGN_IGNORE)
			SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
			SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
			
			DRAW_SPRITE_NAMED_RENDERTARGET("Prop_Screen_GR_Disruption", "Prop_Screen_GR_Disruption", 0.5, 0.5, 1.0, 1.0, 0.0, 255, 255, 255, 255)
			
			RESET_SCRIPT_GFX_ALIGN()
			SET_TEXT_RENDER_ID(GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID())
		BREAK
	ENDSWITCH
	
ENDPROC

PROC CLEANUP_BUNKER_LAPTOP_SCREEN()
	IF IS_NAMED_RENDERTARGET_REGISTERED("gr_bunker_laptop_01a")   
		RELEASE_NAMED_RENDERTARGET("gr_bunker_laptop_01a")
	ENDIF
	thisBunker.bunkerLaptopData.iRenderTargetID = -1
	thisBunker.bunkerLaptopData.eLaptopState = LAPTOP_SCREEN_STATE_INIT
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("Prop_Screen_GR_Disruption")
ENDPROC

PROC CLEAN_UP_PERSONAL_CAR_MOD()
	PRINTLN("CLEAN_UP_PERSONAL_CAR_MOD (Bunker) - called")
	g_bCleanUpCarmodShop = TRUE
	g_sShopSettings.bShopScriptLaunched[ENUM_TO_INT(CARMOD_SHOP_PERSONALMOD)] = FALSE
	CLEAR_BIT(thisBunker.iBS, BS_BUNKER_IS_CAR_MOD_SCRIPT_READY)
	RESET_NET_TIMER(thisBunker.sCarModScriptRunTimer)
	IF NOT IS_STRING_NULL_OR_EMPTY(thisBunker.sChildOfChildScript)
		SET_SCRIPT_AS_NO_LONGER_NEEDED(thisBunker.sChildOfChildScript)
	ENDIF	
ENDPROC

#IF FEATURE_DLC_1_2022
PROC CLEANUP_DUNELOADER_CUTSCENE()
	IF DOES_CAM_EXIST(camDuneloader)
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		
		DESTROY_CAM(camDuneloader)
	ENDIF
	
	IF NETWORK_IS_IN_MP_CUTSCENE()
	AND IS_BIT_SET(iDuneloaderBS, 5)
		CLEANUP_MP_CUTSCENE(FALSE, FALSE)
	ENDIF
ENDPROC

PROC CLEANUP_DUNELOADER()
	IF DOES_ENTITY_EXIST(objDuneloaderCrate)
		DELETE_OBJECT(objDuneloaderCrate)
	ENDIF
	
	IF DOES_ENTITY_EXIST(vehDuneloader)
		DELETE_VEHICLE(vehDuneloader)
	ENDIF
	
	iDuneloaderBS = 0
	
	IF iDuneloaderContext != NEW_CONTEXT_INTENTION
		RELEASE_CONTEXT_INTENTION(iDuneloaderContext)
		
		iDuneloaderContext = NEW_CONTEXT_INTENTION
	ENDIF
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("BUNK_DL_HELP3")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("BUNK_DL_HELP4")
		CLEAR_HELP()
	ENDIF
	
	IF DOES_BLIP_EXIST(blipDuneloaderCrate)
		REMOVE_BLIP(blipDuneloaderCrate)
	ENDIF
ENDPROC
#ENDIF

PROC SCRIPT_CLEANUP()
	#IF IS_DEBUG_BUILD
	CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_BUNKER - SCRIPT_CLEANUP")
	#ENDIF
	CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS , BS_BUNKER_OWNER_IS_UNFREEZE_TRUCK_AFTER_ENTERING_BUNKER)
	g_bDrivingOutOfBunker = FALSE
	g_bPlayerLeavingCurrentInteriorInVeh = FALSE
	
	RELEASE_CONTEXT_INTENTION(thisbunker.iContextTruckEntryID)
	FREEMODE_DELIVERY_SCRIPT_PREVENT_DELAY(FALSE)
	CLEANUP_ALL_FACTORY_PRODUCTS(thisBunker.eID)
	CLEANUP_ALL_FACTORY_MATERIAL_PROPS(thisBunker.eID)
	CLEANUP_FACTORY_AUDIO()
	CLEANUP_BUNKER_ENTITY_SETS()
	CLEANUP_DEMO_PRODUCT()
	CLEAN_UP_BUNKER_GUN_LOCKER(sBunkerGunLocker, TRUE)
	CLEAN_UP_PERSONAL_CAR_MOD()
	CLEANUP_CCTV(TRUE)
	#IF FEATURE_DLC_1_2022
	CLEANUP_DUNELOADER_CUTSCENE()
	CLEANUP_DUNELOADER()
	#ENDIF
	
	CLEAR_BIT(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_BUNKER_CRATES_SPAWNED)
	
	IF NOT g_sMPTunables.bDisable_Christmas_Tree_Apartment
		DELETE_CHRISTMAS_DECORATIONS_FOR_SIMPLE_INTERIOR(thisBunker.objChristmasTree)
	ENDIF
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_NotAllowedToJackAnyPlayers, FALSE)
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableStartEngine, FALSE)
		SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
		SET_ENTITY_CAN_BE_DAMAGED(PLAYER_PED_ID(), TRUE)
		SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(PLAYER_PED_ID(), TRUE)
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID() ,PCF_DontActivateRagdollFromVehicleImpact,FALSE)
	ENDIF
	
	IF DOES_ENTITY_EXIST(obChairBlocker)
		DELETE_OBJECT(obChairBlocker)
	ENDIF
	
//	IF IS_PLAYER_USING_OFFICE_SEATID_VALID()
		OFFICE_SEAT_ACTIVITY_CLEAN_UP_BUNKER()
//	ENDIF
	
	IF DOES_BLIP_EXIST(bLaptopBlip)
		REMOVE_BLIP(bLaptopBlip)
	ENDIF
	
	IF DOES_BLIP_EXIST(bModShopBlip)
		REMOVE_BLIP(bModShopBlip)
	ENDIF
	IF DOES_BLIP_EXIST(bAATrailerBlip)
		PRINTLN("SCRIPT_CLEANUP remove AA Trailer blip")
		REMOVE_BLIP(bAATrailerBlip)
	ENDIF
	
	IF IS_BIT_SET(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_PREVENT_BIG_MESSAGE_DISABLE)
		PRINTLN("SCRIPT_CLEANUP remove - BS2_SIMPLE_INTERIOR_PREVENT_BIG_MESSAGE_DISABLE clear")
		CLEAR_BIT(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_PREVENT_BIG_MESSAGE_DISABLE)
	ENDIF
	
	IF IS_BIT_SET(g_SimpleInteriorData.iBSTruckExit, BS_SIMPLE_INTERIOR_GLOBAL_DATA_EXIT_TO_TRUCK_CAB_IN_BUNKER)
		CLEAR_BIT(g_SimpleInteriorData.iBSTruckExit, BS_SIMPLE_INTERIOR_GLOBAL_DATA_EXIT_TO_TRUCK_CAB_IN_BUNKER)
	ENDIF
	
	SET_RADAR_ZOOM_PRECISE(0)
	
	g_bShootingRangeUpgraded = FALSE
	
	CLEANUP_BUNKER_LAPTOP_SCREEN()
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("BUNKER_PICK_1")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("BUNKER_PICK_2")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("BUNKER_PICK_3")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("BUNKER_HELP_1")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("BUNKER_HELP_2")
		CLEAR_HELP()
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("FAMILY_1_MOD_SHOP_INTRO_SCENE")
		STOP_AUDIO_SCENE("FAMILY_1_MOD_SHOP_INTRO_SCENE")
	ENDIF
	
	CLEAR_BIT(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_BUNKER_FINISHED_DELIVERY_MISSION_USING_MOC)
	
	IF IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_BUNKER_OWNER_IS_LEAVING_BUNKER_IN_TRUCK)
		CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_BUNKER_OWNER_IS_LEAVING_BUNKER_IN_TRUCK)
	ENDIF
	
	IF IS_BIT_SET(thisBunker.iBS2, BS_2_BUNKER_BLOCK_MOC_ENTERY)
		CLEAR_BIT(g_SimpleInteriorData.sGlobalController.iBS, SIMPLE_INTERIOR_CONTROL_BS_BLOCK_MOC_ENTRY)
	ENDIF
	
	MPGlobals.iBunkerOwner = -1
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ INITIALISATION  ╞════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC CACHE_FACTORY_STATE()

	interiorStruct.cachedFactoryState.iProductionState = serverBD.iProductionStage 
	CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CACHE_FACTORY_STATE: interiorStruct.cachedFactoryState.iProductionState  = ", interiorStruct.cachedFactoryState.iProductionState  )
	interiorStruct.cachedFactoryState.iProductionTotal = thisBunker.iProductSlot
	CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CACHE_FACTORY_STATE: interiorStruct.cachedFactoryState.iProductionTotal = ", interiorStruct.cachedFactoryState.iProductionTotal )
	interiorStruct.cachedFactoryState.iMaterialsTotalForFactory = thisBunker.iMaterialsSlot
	CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CACHE_FACTORY_STATE: interiorStruct.cachedFactoryState.iMaterialsTotalForFactory = ", interiorStruct.cachedFactoryState.iMaterialsTotalForFactory)

ENDPROC

FUNC BOOL MAINTAIN_PED_RESET_BROADCASTDATA(PlayerBroadcastData &playerBDStruct[NUM_NETWORK_PLAYERS], ACTIVITY_INTERIOR_STRUCT &serverInteriorStruct)
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		INT iPlayer
		PLAYER_INDEX piPlayer
		REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iPlayer
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPlayer))
				piPlayer = INT_TO_PLAYERINDEX(iPlayer)
				IF playerBDStruct[NATIVE_TO_INT(piPlayer)].bDataReset = FALSE
//					CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "MAINTAIN_PED_RESET_BROADCASTDATA: playerBDStruct.bDataReset = FALSE, for player ", GET_PLAYER_NAME(piPlayer))
					RETURN FALSE
				ELSE
					CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "MAINTAIN_PED_RESET_BROADCASTDATA: playerBDStruct.bDataReset = TRUE, for player ", GET_PLAYER_NAME(piPlayer))
				ENDIF
			ENDIF
		ENDREPEAT
		// if we've reached here all peds have reset
		// server can reset his flag back to false
		CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "MAINTAIN_PED_RESET_BROADCASTDATA: All player's reset, serverInteriorStruct.bDataReset = FALSE")
		serverInteriorStruct.bDataReset = FALSE
		
		
	ENDIF
	
	IF serverInteriorStruct.bDataReset = FALSE
		playerBDStruct[NATIVE_TO_INT(PLAYER_ID())].bDataReset = FALSE
		CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "MAINTAIN_PED_RESET_BROADCASTDATA: serverInteriorStruct.bDataReset = FALSE, therefore setting client bDataReset = FALSE")
	ELSE
		
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC RUN_ACTIVITY_LOGIC()
	interiorStruct.pFactoryOwner 	= thisBunker.pOwner
	interiorStruct.eFactoryID	 	= thisBunker.eID
	interiorStruct.vInteriorPos 	= GET_FACTORY_COORDS(thisBunker.eID)
	TEXT_LABEL_63 tlTemp
	FLOAT fTemp
	GET_SIMPLE_INTERIOR_INTERIOR_TYPE_AND_POSITION(thisBunker.eSimpleInteriorID, tlTemp, interiorStruct.vInteriorPos, fTemp)
	
	interiorStruct.iInterior 		= GET_HASH_KEY(GET_FACTORY_INTERIOR_NAME(interiorStruct.eFactoryID))
	
	BED_SCRIPT_LAUNCH_LOGIC(bedStruct)
	
	// Runs activity creator logic
	IF GET_BUNKER_PLAYER_IS_IN(PLAYER_ID()) != FACTORY_ID_INVALID
		MAINTAIN_PED_RESET_BROADCASTDATA(playerBD, serverBD.serverInteriorStruct)
		INIT_ACTIVITY_LAUNCH_SCRIPT_CHECK_WITH_AI_PEDS_BUNKER(activityControllerStruct, activityCheck, serverBD.activityProps, serverBD.activityPeds, pedLocalVariationsStruct, interiorStruct, bunkerFactoryActivityStruct, serverBD.serverInteriorStruct, playerBD[NATIVE_TO_INT(PLAYER_ID())].bDataReset)
		MAINTAIN_APARTMENT_ACTIVITIES_CREATOR(activityControllerStruct, activityCheck, serverBD.activityProps, serverBD.activityPeds)
	ELSE
		CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "SETUP_PED_DATA_FOR_BUNKER: GET_FACTORY_PLAYER_IS_IN(PLAYER_ID()) = FACTORY_ID_INVALID")
	ENDIF
	#IF IS_DEBUG_BUILD
	CHECK_TO_LAUNCH_ACTIVITY_CREATOR_UI_SCRIPT(interiorStruct)
	#ENDIF
ENDPROC

PROC INIT_BED_DATA()
	bedStruct.vScenePos = <<904.915, -3200.100, -97.130>>
	bedStruct.vSceneRot = <<-0.250, -0.000, -29.880>>
	bedStruct.vBoundingBoxA = <<904.256836,-3199.044189,-98.187920>>
	bedStruct.vBoundingBoxB = <<906.607117,-3198.201660,-96.211205>> 
	bedStruct.fWidth = 2.75
	bedStruct.fTargetRadius = 0.1
	bedStruct.iScriptInstanceID = 3
	bedStruct.bEnterRightBedSide = TRUE
ENDPROC

PROC INITIALISE()
	SET_AMBIENT_ZONE_STATE("AZ_DLC_GR_Bunker_Machinery_Zone", FALSE, TRUE)
	SET_AMBIENT_ZONE_STATE("AZ_DLC_GR_Bunker_Zone_00", FALSE, TRUE)
	SET_AMBIENT_ZONE_STATE("AZ_DLC_GR_Bunker_Zone_01", FALSE, TRUE)
	SET_AMBIENT_ZONE_STATE("AZ_DLC_GR_Bunker_Zone_02", FALSE, TRUE)
	SET_AMBIENT_ZONE_STATE("AZ_DLC_GR_Bunker_Zone_03", FALSE, TRUE)
	
	SET_PEDS_IN_MAIN_TRUCK(FALSE)
	//SET_SIMPLE_INTERIOR_CHILD_SCRIPT_ALLOWS_FOR_WARP(TRUE)
	STORE_BUNKER_SEAT_POSITIONS(activitySeatStruct)
	INITIALISE_BUNKER_PRODUCTION()
	INITIALISE_FACTORY_TOTALS()
	CACHE_FACTORY_STATE()
	INIT_BED_DATA()
	
	INITIALISE_INTERIOR_BED_SPAWN_ACTIVITIES(GET_SIMPLE_INTERIOR_TYPE(thisBunker.eSimpleInteriorID))
	
	REQUEST_SCRIPT("AM_MP_SHOOTING_RANGE")
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_NotAllowedToJackAnyPlayers, TRUE)
	ENDIF
	
	HOLSTER_WEAPON_IN_SIMPLE_INTERIOR()
	SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
	SET_ENTITY_CAN_BE_DAMAGED(PLAYER_PED_ID(), FALSE)
	SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(PLAYER_PED_ID(), FALSE)
	SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DontActivateRagdollFromVehicleImpact,TRUE)
	
	g_bLaunchedMissionFrmLaptop = FALSE
	
	thisBunker.CarModThread = NULL
	
	IF thisBunker.pOwner = PLAYER_ID()
		IF NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS , BS_BUNKER_OWNER_IS_UNFREEZE_TRUCK_AFTER_ENTERING_BUNKER)
			SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS , BS_BUNKER_OWNER_IS_UNFREEZE_TRUCK_AFTER_ENTERING_BUNKER)
		ENDIF
		#IF IS_DEBUG_BUILD
		CREATE_DEBUG_WIDGETS()	
		INITIALISE_DEBUG_WIDGET_VALUES()
		IF NOT IS_BIT_SET(thisBunker.iBS, BS_BUNKER_WAIT_FOR_PLAYER_TO_EXIT_VEH)
			SET_BIT(thisBunker.iBS, BS_BUNKER_WAIT_FOR_PLAYER_TO_EXIT_VEH)
		ENDIF
		#ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Usees the iPlayerIJoinedBS to find the owner of the bunker we have entered
/// RETURNS:
///    TRUE when know who the owner is
FUNC BOOL DETERMINE_BUNKER_OWNER()
	//We have to search for the owner as we can't rely on gang membership
	IF IS_LOCAL_PLAYER_IN_BUNKER_THEY_OWN()
		thisBunker.pOwner = PLAYER_ID()
		MPGlobals.iBunkerOwner = NATIVE_TO_INT(thisBunker.pOwner)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_BUNKER - DETERMINE_BUNKER_OWNER - I ", GET_PLAYER_NAME(thisBunker.pOwner), " am the owner. access BS =  ", g_SimpleInteriorData.iAccessBS)
		RETURN TRUE
	ELIF g_SimpleInteriorData.iAccessBS > 0
		IF globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner != INVALID_PLAYER_INDEX()
			
			IF IS_NET_PLAYER_OK(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner, FALSE)
				thisBunker.pOwner = GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner
				MPGlobals.iBunkerOwner = NATIVE_TO_INT(thisBunker.pOwner)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_BUNKER - DETERMINE_BUNKER_OWNER - I am not the owner. It is: ", GET_PLAYER_NAME(thisBunker.pOwner))
				RETURN TRUE
			ELSE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_BUNKER - DETERMINE_BUNKER_OWNER - I am not the owner. It is: ", GET_PLAYER_NAME(thisBunker.pOwner), " holding up the script due to IS_NET_PLAYER_OK")
				
				IF NOT HAS_NET_TIMER_STARTED(thisBunker.tOwnerNotOk)
					START_NET_TIMER(thisBunker.tOwnerNotOk)
				ENDIF
				
				IF HAS_NET_TIMER_EXPIRED(thisBunker.tOwnerNotOk, 5000)
					// If the owner is not ok after 5 seconds just move on. The interior script will run a check on this and kick us once we're in anyway.
					thisBunker.pOwner = GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner
					MPGlobals.iBunkerOwner = NATIVE_TO_INT(thisBunker.pOwner)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_BUNKER - DETERMINE_BUNKER_OWNER - I am not the owner. It is: ", GET_PLAYER_NAME(thisBunker.pOwner), " timer expired for IS_NET_PLAYER_OK. Continuing regardless.")
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF (GET_FRAME_COUNT() % 60) = 0
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_BUNKER - DETERMINE_BUNKER_OWNER - Unable to determine hangar owner, g_SimpleInteriorData.iAccessBS = ", g_SimpleInteriorData.iAccessBS)
		ENDIF
		
		IF NOT HAS_NET_TIMER_STARTED(g_SimpleInteriorData.tIntAccessFallback)
			START_NET_TIMER(g_SimpleInteriorData.tIntAccessFallback)
		ENDIF
		
		IF HAS_NET_TIMER_EXPIRED(g_SimpleInteriorData.tIntAccessFallback, g_SimpleInteriorData.iIntAccessFallbackTime)
			thisBunker.pOwner = INVALID_PLAYER_INDEX()
			RESET_NET_TIMER(g_SimpleInteriorData.tIntAccessFallback)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_BUNKER - DETERMINE_BUNKER_OWNER - Unable to determine bunker owner, returning TRUE")
			RETURN TRUE
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_BUNKER - DETERMINE_BUNKER_OWNER - g_SimpleInteriorData.iAccessBS = ", g_SimpleInteriorData.iAccessBS)
	#ENDIF
	
	IF (GET_FRAME_COUNT() % 60) = 0
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_BUNKER - DETERMINE_BUNKER_OWNER - Trying to find the owner")
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SCRIPT_INITIALISE(SIMPLE_INTERIOR_CHILD_SCRIPT_DATA& scriptData)

	IF DID_I_JOIN_MISSION_AS_SPECTATOR() AND IS_BIT_SET(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_SPAWNING_IN_SIMPLE_INTERIOR)
		SET_SIMPLE_INTERIOR_CHILD_SCRIPT_ALLOWS_FOR_WARP(TRUE)
		PRINTLN("AM_MP_BUNKER - SCRIPT_INITIALISE - We are joining mission as spectator so no need to launch the child script.")
		SCRIPT_CLEANUP()
	ENDIF

	MPGlobals.iBunkerOwner = -1
	
	WHILE NOT DETERMINE_BUNKER_OWNER()
		
		IF NOT NETWORK_IS_GAME_IN_PROGRESS()
			PRINTLN("AM_MP_BUNKER - NETWORK_IS_GAME_IN_PROGRESS = FALSE")
			SCRIPT_CLEANUP()
		ENDIF
		
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			PRINTLN("AM_MP_BUNKER - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE = TRUE")
			SCRIPT_CLEANUP()
		ENDIF
		
		IF SHOULD_THIS_SIMPLE_INTERIOR_CHILD_THREAD_TERMINATE(scriptData.iScriptInstance, scriptData.eSimpleInteriorID)
			PRINTLN("AM_MP_BUNKER - SHOULD_THIS_SIMPLE_INTERIOR_CHILD_THREAD_TERMINATE = TRUE")
			SCRIPT_CLEANUP()
		ENDIF
		
		IF IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_BUNKER_ACCESS_BS_OWNER_LEFT_GAME)
			PRINTLN("AM_MP_BUNKER - SIMPLE_INTERIOR_BUNKER_ACCESS_BS_OWNER_LEFT_GAME = TRUE")
			SCRIPT_CLEANUP()
		ENDIF
		
		WAIT(0)
	ENDWHILE
	
	
	g_bDrivingOutOfBunker = FALSE
	g_bPlayerLeavingCurrentInteriorInVeh = FALSE
	thisBunker.eSimpleInteriorID 	= scriptData.eSimpleInteriorID
	thisBunker.iScriptInstance 		= scriptData.iScriptInstance
	thisBunker.iInvitingPlayer 		= scriptData.iInvitingPlayer
	thisBunker.eID 					= GET_FACTORY_ID_FROM_SIMPLE_INTERIOR_ID(scriptData.eSimpleInteriorID)
	thisBunker.eFactoryType 		= GET_GOODS_CATEGORY_FROM_FACTORY_ID(thisBunker.eID)
	thisBunker.bScriptWasRelaunched = scriptData.bWasRelaunched
	thisBunker.iSaveSlot 			= BUNKER_SAVE_SLOT
	g_ownerOfBunkerPropertyIAmIn 	= thisBunker.pOwner
	
	IF GET_GOODS_CATEGORY_FROM_FACTORY_ID(thisBunker.eID) != FACTORY_TYPE_WEAPONS
		SCRIPT_ASSERT("AM_MP_BUNKER - SCRIPT_INITIALISE - Script launched for a business not of type weapons! Shutting down!")
		PRINTLN("AM_MP_BUNKER - SCRIPT CLEANUP")
		SCRIPT_CLEANUP()
	ENDIF
	
	SET_PLAYER_TRUCK_TO_BUNKER_FRONT_EXIT_TRIGGER(FALSE)
	
	IF IS_BIT_SET(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_SPAWNING_IN_SIMPLE_INTERIOR)
		IF thisBunker.pOwner = INVALID_PLAYER_INDEX()
			FACTORY_ID eOwnedFactoryFromSlot
			INT iSlot
			REPEAT ciMAX_OWNED_FACTORIES iSlot
				IF IS_FACTORY_OF_TYPE_WEAPONS(INT_TO_ENUM(FACTORY_ID, GET_MP_INT_CHARACTER_STAT(GET_STAT_ENUM_FOR_FACTORY_SLOT(iSlot))))
					eOwnedFactoryFromSlot = INT_TO_ENUM(FACTORY_ID, GET_MP_INT_CHARACTER_STAT(GET_STAT_ENUM_FOR_FACTORY_SLOT(iSlot)))
					BREAKLOOP
				ENDIF
			ENDREPEAT
			
			PRINTLN("AM_MP_BUNKER - SCRIPT_INITIALISE - Spawning inside bunker but owner is invalid... Stat says we own bunker ", eOwnedFactoryFromSlot, ", the script is starting for ", GET_FACTORY_ID_FROM_SIMPLE_INTERIOR_ID(thisBunker.eSimpleInteriorID))
			
			IF eOwnedFactoryFromSlot = GET_FACTORY_ID_FROM_SIMPLE_INTERIOR_ID(thisBunker.eSimpleInteriorID)
				PRINTLN("AM_MP_BUNKER - SCRIPT_INITIALISE - We are spawning in IE warehouse that we own so we are the owner.")
				thisBunker.pOwner = PLAYER_ID()
			ENDIF
		ENDIF
	ENDIF
	
	REGISTER_SCRIPT_WITH_AUDIO()
	
	NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE, thisBunker.iScriptInstance)
	
	// This makes sure the net script is active, waits untill it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	RESERVE_LOCAL_NETWORK_MISSION_VEHICLES(NUMBER_OF_BUNKER_VEHICLES + 3) // +3 for AA trailer, and Armory Cab/Trailer
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
	
	// KGM: Wait for the first network broadcast before moving on
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		PRINTLN("AM_MP_BUNKER - FAILED TO RECEIVE INITIAL NETWORK BROADCAST - SCRIPT CLEANUP")
		SCRIPT_CLEANUP()
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		// This script will not be paused if another script calls PAUSE_GAME
		PRINTLN("AM_MP_BUNKER - INITIALISED")
	ELSE
		PRINTLN("AM_MP_BUNKER - NETWORK_IS_GAME_IN_PROGRESS = FALSE - SCRIPT CLEANUP")
		SCRIPT_CLEANUP()
	ENDIF
	
	INITIALISE()
	
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ PROCEDURES  ╞═══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

FUNC BOOL IS_BUNKER_SERVER_STATE(BUNKER_SCRIPT_STATE eState)
	RETURN serverBD.eState = eState
ENDFUNC

PROC SET_BUNKER_SERVER_STATE(BUNKER_SCRIPT_STATE eState)
	CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] AM_MP_BUNKER - SET_BUNKER_SERVER_STATE - New state: ", ENUM_TO_INT(eState))
	serverBD.eState = eState
ENDPROC

FUNC BOOL IS_BUNKER_STATE(BUNKER_SCRIPT_STATE eState)
	RETURN thisBunker.eState = eState
ENDFUNC

PROC SET_BUNKER_STATE(BUNKER_SCRIPT_STATE eState)
	CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] AM_MP_BUNKER - SET_BUNKER_STATE - New state: ", ENUM_TO_INT(eState))
	thisBunker.eState = eState
ENDPROC

FUNC BOOL LAUNCH_SHOOTING_RANGE()
	IF HAS_SCRIPT_LOADED("AM_MP_SHOOTING_RANGE")
		IF NOT NETWORK_IS_SCRIPT_ACTIVE("AM_MP_SHOOTING_RANGE", thisBunker.iScriptInstance, true)
			SIMPLE_INTERIOR_SCRIPT_DATA scriptData
			scriptData.eSimpleInteriorID = thisBunker.eSimpleInteriorID
			scriptData.iScriptInstance = thisBunker.iScriptInstance
			
			START_NEW_SCRIPT_WITH_ARGS("AM_MP_SHOOTING_RANGE", scriptData, SIZE_OF(scriptData), SCRIPT_XML_STACK_SIZE)
			SET_SCRIPT_AS_NO_LONGER_NEEDED("AM_MP_SHOOTING_RANGE")
			
			#IF IS_DEBUG_BUILD
			PRINTLN("[SIMPLE_INTERIOR] AM_MP_BUNKER - LAUNCH_SHOOTING_RANGE - Launched!")
			#ENDIF
			
			RETURN TRUE
		ENDIF
	ELSE
		REQUEST_SCRIPT("AM_MP_SHOOTING_RANGE")
	ENDIF
	
	RETURN FALSE
ENDFUNC

CONST_INT ciRESEARCH_BAR_PULSE_TIME	 	1000	//Research
CONST_INT ciSUPPLY_BAR_PULSE_TIME 		750		//Supplies
CONST_INT ciSTOCK_BAR_PULSE_TIME 		1000	//Stock
CONST_INT ciHUD_BARS_PULSE_TOTAL_TIME 	3000	//Total time to pulse

//LastFrameTotals
INT iLastFrameSupplies 	= -1
INT iLastFrameResearch 	= -1
INT iLastFrameStock		= -1

FUNC BOOL SHOULD_BUNKER_SUPPLIES_BAR_PULSE(INT &iSuppliesTotal)
	IF g_bFlashBunkerSuppliesBar
		IF iSuppliesTotal >= 90
			IF IS_SCREEN_FADED_IN()
			OR IS_SCREEN_FADING_IN()
				IF NOT HAS_NET_TIMER_STARTED(thisBunker.stBunkerSuppliesBarFlash)
					START_NET_TIMER(thisBunker.stBunkerSuppliesBarFlash)
				ELIF HAS_NET_TIMER_EXPIRED(thisBunker.stBunkerSuppliesBarFlash, 10000)
					g_bFlashBunkerSuppliesBar = FALSE
					RESET_NET_TIMER(thisBunker.stBunkerSuppliesBarFlash)
				ENDIF
			ENDIF
			
			iSuppliesTotal = 100
			RETURN TRUE
			
		ELSE
			
			g_bFlashBunkerSuppliesBar = FALSE
			RESET_NET_TIMER(thisBunker.stBunkerSuppliesBarFlash)
		ENDIF
	ELSE
		INT iSupplies = iLastFrameSupplies
	
		//Save the stock total
		iLastFrameSupplies = iSuppliesTotal
		
		//Check if it is different from last frame
		IF iSupplies > iSuppliesTotal
		AND iSupplies != -1
			RESET_NET_TIMER(thisBunker.stBunkerSuppliesBarFlash)
			START_NET_TIMER(thisBunker.stBunkerSuppliesBarFlash)
		ENDIF
		
		IF HAS_NET_TIMER_STARTED(thisBunker.stBunkerSuppliesBarFlash)
			IF NOT HAS_NET_TIMER_EXPIRED(thisBunker.stBunkerSuppliesBarFlash, ciHUD_BARS_PULSE_TOTAL_TIME)			
				RETURN TRUE
			ELSE
				RESET_NET_TIMER(thisBunker.stBunkerSuppliesBarFlash)
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_BUNKER_RESEARCH_BAR_PULSE(INT iResearchCapacity, INT iResearchTotal)
	
	INT iResearch = iLastFrameResearch
	
	//Save the Research total 
	iLastFrameResearch = iResearchTotal
	
	//Check if it is different from last frame
	IF iResearch < iResearchTotal
	AND iResearch != -1
		RESET_NET_TIMER(thisBunker.stBunkerResearchBarFlash)
		START_NET_TIMER(thisBunker.stBunkerResearchBarFlash)
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(thisBunker.stBunkerResearchBarFlash)
		IF NOT HAS_NET_TIMER_EXPIRED(thisBunker.stBunkerResearchBarFlash, ciHUD_BARS_PULSE_TOTAL_TIME)			
			RETURN TRUE
		ELSE
			RESET_NET_TIMER(thisBunker.stBunkerResearchBarFlash)
		ENDIF
	ENDIF
	
	RETURN (iResearchCapacity = iResearchTotal)
ENDFUNC

FUNC BOOL SHOULD_BUNKER_STOCK_BAR_PULSE(INT iStockCapacity, INT iStockTotal)
	
	INT iStock = iLastFrameStock
	
	//Save the stock total
	iLastFrameStock = iStockTotal
	
	//Check if it is different from last frame
	IF iStock < iStockTotal
	AND iStock != -1
		RESET_NET_TIMER(thisBunker.stBunkerStockBarFlash)
		START_NET_TIMER(thisBunker.stBunkerStockBarFlash)
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(thisBunker.stBunkerStockBarFlash)
		IF NOT HAS_NET_TIMER_EXPIRED(thisBunker.stBunkerStockBarFlash, ciHUD_BARS_PULSE_TOTAL_TIME)			
			RETURN TRUE
		ELSE
			RESET_NET_TIMER(thisBunker.stBunkerStockBarFlash)
		ENDIF
	ENDIF
	
	RETURN(iStockCapacity = iStockTotal)
ENDFUNC

PROC MAINTAIN_BUNKER_HUD()
	
	IF thisBunker.pOwner != INVALID_PLAYER_INDEX()
	AND IS_PLAYER_IN_BUNKER(PLAYER_ID())
	AND NOT IS_ANY_TYPE_OF_CUTSCENE_PLAYING()
	AND playerBD[PARTICIPANT_ID_TO_INT()].MPCCTVClient.eStage != MP_CCTV_CLIENT_STAGE_ACTIVATED
	AND NOT AM_I_TRANSITION_SESSIONS_STARTING_ON_CALL()
	AND NOT g_bUsingShootingRange
	AND NOT IS_PLAYER_STARTED_TO_USE_PERSONAL_CAR_MOD(PLAYER_ID())
	AND NOT IS_PLAYER_IN_CORONA()
	AND NOT IS_TRANSITION_SESSION_LAUNCHING()
	#IF IS_DEBUG_BUILD
	AND NOT g_bFactoryHideUI
	#ENDIF
		INT iResearchBarPulseTime	= 0
		INT iSuppliesBarPulseTime	= 0
		INT iStockBarPulseTime		= 0
		INT iProductQuantity   		= GET_PRODUCT_TOTAL_FOR_FACTORY(thisBunker.pOwner, thisBunker.eID)
		INT iProductCapacity   		= GET_FACTORY_PRODUCT_CAPACITY(thisBunker.eID)
		INT iResearchCacity			= GET_FACTORY_RESEARCH_CAPACITY(thisBunker.eID)
		INT iResearchTotal			= GlobalplayerBD_FM[NATIVE_TO_INT(thisBunker.pOwner)].propertyDetails.iBunkerResearchTotal
		INT iTotalProductValue 		= GET_BROADCAST_FACTORY_TOTAL_PRODUCT_VALUE(thisBunker.pOwner, thisBunker.iSaveSlot)
		INT iMaterialTotalBlended 	= GlobalplayerBD_FM[NATIVE_TO_INT(thisBunker.pOwner)].propertyDetails.bdFactoryData[BUNKER_SAVE_SLOT].iMaterialsTotalBlended
		
		IF SHOULD_BUNKER_RESEARCH_BAR_PULSE(iResearchCacity, iResearchTotal)
			iResearchBarPulseTime = ciRESEARCH_BAR_PULSE_TIME
		ENDIF
		
		IF SHOULD_BUNKER_SUPPLIES_BAR_PULSE(iMaterialTotalBlended)			
			iSuppliesBarPulseTime = ciSUPPLY_BAR_PULSE_TIME
		ENDIF
		
		IF SHOULD_BUNKER_STOCK_BAR_PULSE(iProductCapacity, iProductQuantity)
			iStockBarPulseTime = ciSTOCK_BAR_PULSE_TIME
		ENDIF
		
		DRAW_GENERIC_SCORE(iTotalProductValue, "HUD_STOCK", DEFAULT, DEFAULT, HUDORDER_EIGHTHBOTTOM, DEFAULT, "HUD_CASH")
		DRAW_GENERIC_METER(iProductQuantity, iProductCapacity, "HUD_PRODLVL", HUD_COLOUR_BLUE, DEFAULT, HUDORDER_SEVENTHBOTTOM,DEFAULT,DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE, DEFAULT, HUD_COLOUR_MENU_BLUE_EXTRA_DARK, iStockBarPulseTime)
		DRAW_GENERIC_METER(iResearchTotal, iResearchCacity, "HUD_RESEARCH", HUD_COLOUR_GREEN, DEFAULT, HUDORDER_SIXTHBOTTOM, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE, DEFAULT, HUD_COLOUR_PURPLE, iResearchBarPulseTime)
		DRAW_GENERIC_METER(iMaterialTotalBlended, 100, "HUD_SUPPLIES", HUD_COLOUR_ORANGE, DEFAULT, HUDORDER_FIFTHBOTTOM,DEFAULT,DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE, DEFAULT, HUD_COLOUR_YELLOWLIGHT, iSuppliesBarPulseTime)
	ENDIF
	
ENDPROC

PROC MAINTAIN_BUNKER_TRUCK()
	IF thisBunker.pOwner != INVALID_PLAYER_INDEX()
	AND thisBunker.pOwner = PLAYER_ID()
		IF IS_VALID_INTERIOR(GET_INTERIOR_FROM_ENTITY(GET_PLAYER_PED(PLAYER_ID())))
		AND IS_INTERIOR_READY(GET_INTERIOR_FROM_ENTITY(GET_PLAYER_PED(PLAYER_ID()))) 
			IF IS_PLAYER_GUNRUNNING_TRUCK_PURCHASED(thisBunker.pOwner)
				IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0])
				AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1])
//					IF NOT IS_BIT_SET(serverBD.iBS, BS_BUNKER_DATA_RESERVE_VEHICLE_INDEX_FOR_TRUCK)
//						IF CAN_RESERVE_NETWORK_VEHICLES_FOR_THIS_SCRIPT(GET_NUM_RESERVED_MISSION_VEHICLES() + 2, FALSE, TRUE)
//							RESERVE_NETWORK_MISSION_VEHICLES(GET_NUM_RESERVED_MISSION_VEHICLES() + 2)
//							IF CAN_REGISTER_MISSION_VEHICLES(2)
//								SET_BIT(serverBD.iBS, BS_BUNKER_DATA_RESERVE_VEHICLE_INDEX_FOR_TRUCK)
//							ENDIF
//						ENDIF
//					ELSE	
//						IF SPAWN_BOOKED_TRUCK_VEHICLE(vTruckCoord, fTruckHeading, TRUE)
//							CPRINTLN(DEBUG_PROPERTY, "MAINTAIN_BUNKER_TRUCK - created player ", NATIVE_TO_INT(thisBunker.pOwner), " truck")
//							
//							ar_ATTACH_VEHICLE_TO_TRAILER(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0]), NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1]), fTrailerHeadingOffset)
//							
//							FORCE_ROOM_FOR_ENTITY(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0]), GET_INTERIOR_FROM_ENTITY(GET_PLAYER_PED(PLAYER_ID())), GET_HASH_KEY("bunker_ModRoom"))
//							FORCE_ROOM_FOR_ENTITY(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1]), GET_INTERIOR_FROM_ENTITY(GET_PLAYER_PED(PLAYER_ID())), GET_HASH_KEY("bunker_ModRoom"))
//							CPRINTLN(DEBUG_PROPERTY, "MAINTAIN_BUNKER_TRUCK - Forcing truck to room ")
//						ENDIF
//					ENDIF	
				ENDIF	
			ENDIF
		ENDIF	
	ENDIF
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(serverBd.niCloneTruck)
	AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBd.niCloneTruck)
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBd.niCloneTruck))
			SET_CINEMATIC_BUTTON_ACTIVE(FALSE)
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_BUNKER_VEHICLES()
	CREATE_BUNKER_CADDIES()
	
	SWITCH thisTruckBunker.iStage
		CASE 0		//create purchased bunker
			
		BREAK
		CASE 1		//maintain truck
//			IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niBunkerArmoryTruckTrailer)
//			AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niBunkerArmoryTruck)
//				//
//			ELSE
//				//
//			ENDIF
		BREAK
		
		CASE -1		//player doesn't own truck (do we need to monitor/reset?)
		BREAK
	ENDSWITCH
	
ENDPROC


FUNC BOOL SHOULD_ARMORY_TRUCK_SIMPLE_INTERIOR_ENTRY_LOCATE_BE_SHOWN_WHILE_IN_BUNKER()
	
	IF IS_PAUSE_MENU_ACTIVE_EX()
	OR IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
	OR IS_CUSTOM_MENU_ON_SCREEN()
	OR IS_BROWSER_OPEN()
	OR IS_COMMERCE_STORE_OPEN()
	OR IS_STORE_PENDING_NETWORK_SHUTDOWN_TO_OPEN()
	OR IS_INTERACTION_MENU_OPEN()
	OR PROPERTY_HAS_JUST_ACCEPTED_A_MISSION()
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL HAS_ALL_PASSENGERS_FADED_OUT_FOR_TRUCK_INTRO_WITH_VEHICLE()

	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			VEHICLE_INDEX pedVeh 
			pedVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			IF DOES_ENTITY_EXIST(pedVeh)
				IF GET_VEHICLE_NUMBER_OF_PASSENGERS(pedVeh) != 0
					INT i ,iNumWaiting 
					REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
						IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
							PLAYER_INDEX tempPlayer
							tempPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
							IF IS_PED_IN_VEHICLE(GET_PLAYER_PED(tempPlayer),pedVeh)
								IF IS_BIT_SET(playerBD[NATIVE_TO_INT(tempPlayer)].iBS , BS_BUNKER_DATA_FADE_OUT_TO_ENTER_TRUCK_WITH_VEH)
									iNumWaiting++ 
								ENDIF
							ENDIF	
						ENDIF
					ENDREPEAT
					IF iNumWaiting = GET_VEHICLE_NUMBER_OF_PASSENGERS(pedVeh)
						PRINTLN("[SIMPLE_INTERIOR][INT_SCRIPT] - HAS_ALL_PASSENGERS_FADED_OUT_FOR_INTRO - return True iNumWaiting: ", iNumWaiting, " Number of passengers : ", GET_VEHICLE_NUMBER_OF_PASSENGERS(pedVeh) )
						RETURN TRUE
					ENDIF
				ELSE	
					PRINTLN("[SIMPLE_INTERIOR][INT_SCRIPT] - HAS_ALL_PASSENGERS_FADED_OUT_FOR_INTRO GET_VEHICLE_NUMBER_OF_PASSENGERS = 0 ")
					RETURN TRUE
				ENDIF
			ELSE
				PRINTLN("[SIMPLE_INTERIOR][INT_SCRIPT] - return false veh doesn't exist ")
			ENDIF	
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL ARE_ALL_PEDS_IN_VEHICLE_READY_FOR_ENTER_ARMORY_TRUCK_FROM_BUNKER()
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		VEHICLE_INDEX pedVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		INT i
		PLAYER_INDEX thePlayer
		REPEAT NUM_NETWORK_PLAYERS i
			thePlayer = INT_TO_PLAYERINDEX(i)
			
			IF thePlayer != PLAYER_ID()
			AND thePlayer != INVALID_PLAYER_INDEX()
			AND NETWORK_IS_PLAYER_A_PARTICIPANT(thePlayer)
				IF IS_NET_PLAYER_OK(thePlayer)
					IF DOES_ENTITY_EXIST(pedVeh)
						IF IS_PED_IN_VEHICLE(GET_PLAYER_PED(thePlayer), pedVeh, FALSE)
							IF NOT IS_PED_SITTING_IN_ANY_VEHICLE(GET_PLAYER_PED(thePlayer))
							OR g_MultiplayerSettings.g_bSuicide
							OR IS_SELECTOR_ONSCREEN()
							OR  GET_IS_TASK_ACTIVE(GET_PLAYER_PED(thePlayer), CODE_TASK_EXIT_VEHICLE)
								#IF IS_DEBUG_BUILD
									PRINTLN("ARE_ALL_PEDS_IN_VEHICLE_READY_FOR_ENTER_ARMORY_TRUCK_FROM_BUNKER: ", GET_PLAYER_NAME(thePlayer)," not ready not set flag")
								#ENDIF
								
								RETURN FALSE
							ENDIF
						ELSE
							IF IS_PED_IN_VEHICLE(GET_PLAYER_PED(thePlayer), pedVeh, TRUE)
							OR IS_ENTITY_ATTACHED_TO_ENTITY(GET_PLAYER_PED(thePlayer), pedVeh)
								#IF IS_DEBUG_BUILD
									PRINTLN("ARE_ALL_PEDS_IN_VEHICLE_READY_FOR_ENTER_ARMORY_TRUCK_FROM_BUNKER: ", GET_PLAYER_NAME(thePlayer)," entering or attached")
								#ENDIF
								
								RETURN FALSE
							ENDIF
						ENDIF
					ENDIF	
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_SAFE_TO_START_AUTOWARP_TO_TRUCK_IN_VEHICLE()
	IF !HAS_ALL_PASSENGERS_FADED_OUT_FOR_TRUCK_INTRO_WITH_VEHICLE()
		PRINTLN("[SIMPLE_INTERIOR][INT_SCRIPT] - IS_SAFE_TO_START_AUTOWARP_TO_TRUCK_IN_VEHICLE FALSE Passangers hasn't faded out yet")
		RETURN FALSE
	ENDIF
	
	IF !ARE_ALL_PEDS_IN_VEHICLE_READY_FOR_ENTER_ARMORY_TRUCK_FROM_BUNKER()
		PRINTLN("[SIMPLE_INTERIOR][INT_SCRIPT] - IS_SAFE_TO_START_AUTOWARP_TO_TRUCK_IN_VEHICLE FALSE Passangers leaving or entering vehicle")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_SAFE_TO_START_TRUCK_ENTRY_WITH_VEHICLE()
	VEHICLE_INDEX pedVeh
	
	IF !IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		RETURN FALSE
	ELSE
		pedVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(pedVeh)
		RETURN FALSE
	ENDIF	
	
	IF GET_ENTITY_MODEL(pedVeh) = PHANTOM2
	OR GET_ENTITY_MODEL(pedVeh) = HAULER2
	OR GET_ENTITY_MODEL(pedVeh) = PHANTOM3
		RETURN FALSE
	ENDIF
	
	IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) != VS_DRIVER
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_PURCHASED_ANY_CARMOD_SECTION_FOR_TRUCK(PLAYER_ID())
	AND IS_VEHICLE_ALLOWED_TO_ENTER_TRUCK_MOD_SHOP(pedVeh)
		PRINTLN("[SIMPLE_INTERIOR][INT_SCRIPT] - IS_SAFE_TO_START_TRUCK_ENTRY_WITH_VEHICLE IS_VEHICLE_ALLOWED_TO_ENTER_TRUCK_MOD_SHOP TRUE")
		RETURN TRUE
	ENDIF
	
	IF IS_VEHICLE_A_PERSONAL_VEHICLE(pedVeh)
	AND IS_PLAYER_PURCHASED_ANY_VEHICLE_STORAGE_SECTION_FOR_TRUCK(PLAYER_ID())
		PRINTLN("[SIMPLE_INTERIOR][INT_SCRIPT] - IS_SAFE_TO_START_TRUCK_ENTRY_WITH_VEHICLE true IS_VEHICLE_A_PERSONAL_VEHICLE TRUE")
		RETURN TRUE
	ENDIF	

	IF IS_VEHICLE_UPGRADABLE_PEGASUS_VEHICLE(pedVeh)
	AND IS_PLAYER_PURCHASED_ANY_VEHICLE_STORAGE_SECTION_FOR_TRUCK(PLAYER_ID())
		PRINTLN("[SIMPLE_INTERIOR][INT_SCRIPT] - IS_SAFE_TO_START_TRUCK_ENTRY_WITH_VEHICLE true IS_VEHICLE_UPGRADABLE_PEGASUS_VEHICLE TRUE")
		RETURN TRUE
	ENDIF	
	
	RETURN FALSE
ENDFUNC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ CUTSCENE SETUP  ╞════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC INITIALISE_POST_SETUP_CUTSCENE_DATA(SIMPLE_CUTSCENE &cutscene, INT iCutsceneHash)
	TEXT_LABEL_23 helpPrefix
	TEXT_LABEL_23 txtHelps[SIMPLE_CUTSCENE_MAX_HELP]
	
	SWITCH iCutsceneHash
		CASE CI_BUNKER_SETUP_CUTSCENE
			helpPrefix = "BNKER_STP_A"
		
			SIMPLE_CUTSCENE_CLEAR_ALL_DATA(cutscene)
			SIMPLE_CUTSCENE_CREATE(cutscene, "bunker_setup_cutscene", thisBunker.eSimpleInteriorID)
			
			SIMPLE_CUTSCENE_ADD_SCENE(cutscene, 7500, "production", <<-40.561, -36.694, 2.015>>, <<-1.706, -0.100, -7.855>>, 37.149, <<-40.224, -34.809, 2.018>>, <<-2.8790, -0.0182, -4.9988>>, 37.1493, 0.0500, 500)
			SIMPLE_CUTSCENE_ADD_SCENE(cutscene, 7500, "stock_empty", <<-17.590, -19.698, 1.390>>, <<2.236, -0.424, -81.8861>>, 40.7547, <<-15.552, -19.408, 1.470>>, <<2.2107, -0.4241, -80.9770>>, 40.7547, 0.0500)
			SIMPLE_CUTSCENE_ADD_SCENE(cutscene, 7500, "stock_full", <<0.269, -8.628, 1.360>>, <<4.8351, -0.0116, 142.6519>>, 51.0267, <<0.876, -7.804, 1.273>>, <<4.8351, -0.0116, 146.2109>>, 51.0267, 0.0500)
			SIMPLE_CUTSCENE_ADD_SCENE(cutscene, 3000, "research_01", <<-46.181, -13.580, 1.778>>, <<-2.4207, 0.1505, -179.7640>>, 27.4403, <<-46.183, -13.275, 1.783>>, <<-1.3009, 0.1585, -179.7490>>, 28.1104, 0.0500)	
			SIMPLE_CUTSCENE_ADD_SCENE(cutscene, 4500, "research_02", <<-44.049, -9.802, 1.802>>, <<0.0303, 0.3469, 51.0231>>, 35.5843, <<-43.510, -9.591, 1.802>>, <<-0.8372, 0.3469, 41.7104>>, 35.5843, 0.0500)
			SIMPLE_CUTSCENE_ADD_SCENE(cutscene, 7500, "AMU_Contract", << -66.5402, -49.1287, 3.0746 >>, <<-13.0291, 0.0000, 35.0619>>, 46.6716, << -65.1401, -51.1228, 3.6385 >>, <<-13.0291, 0.0000, 35.0619>>, 46.6716, 0.0500)
			SIMPLE_CUTSCENE_ADD_SCENE(cutscene, 4500, "laptop_01", <<-43.174, -8.396, 2.366>>, <<2.1011, 0.1845, -105.7538>>, 20.8594, <<-42.426, -8.616, 2.467>>, <<1.5577, 0.1888, -105.3774>>, 20.8594, 0.0500)
			SIMPLE_CUTSCENE_ADD_SCENE(cutscene, 4500, "laptop_02", <<-30.941, -9.963, 2.783>>, <<-1.2079, 0.1845, -138.7763>>, 20.8594, <<-30.549, -10.264, 2.773>>, <<-0.9091, 0.1815, -141.7274>>, 20.8594, 0.0500, 0, 500, GRAPH_TYPE_DECEL)
		BREAK
	ENDSWITCH

	INT iStartTime = 0
	INT i
	
	REPEAT cutscene.iScenesCount i
		txtHelps[i] = helpPrefix
		txtHelps[i] += i
	
		IF (i = 0)
			SIMPLE_CUTSCENE_ADD_HELP(cutscene, cutscene.sScenes[i].iFadeInTime, cutscene.sScenes[i].iDuration - cutscene.sScenes[i].iFadeInTime, txtHelps[i])
		ELSE
			iStartTime += cutscene.sScenes[i - 1].iDuration
			SIMPLE_CUTSCENE_ADD_HELP(cutscene, iStartTime, cutscene.sScenes[i].iDuration, txtHelps[i])
		ENDIF
	ENDREPEAT
ENDPROC

FUNC TEXT_LABEL_23 GET_DEMO_PRODUCT_SOUND(STRING suffix)
	TEXT_LABEL_23 base = "Cash_Tick_"
	base += suffix
	RETURN base
ENDFUNC

FUNC INT GET_BUNKER_CUTSCENE(FACTORY_ID eFactoryID)	
	UNUSED_PARAMETER(eFactoryID)
	
	IF PLAYER_ID() != INVALID_PLAYER_INDEX()
		IF PLAYER_ID() = thisBunker.pOwner
			IF NOT HAS_LOCAL_PLAYER_VIEWED_INTERIOR_CUTSCENE(biFmCut_Bunker_Cutscene)
				#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_CUTSCENE, "[SIMPLE_INTERIOR] AM_MP_BUNKER - GET_BUNKER_CUTSCENE - Player hasn't viewed cutscene, returning cutscene hash.")
				#ENDIF
				RETURN CI_BUNKER_SETUP_CUTSCENE
			ENDIF
		ENDIF
	ENDIF

	#IF IS_DEBUG_BUILD
	CDEBUG1LN(DEBUG_CUTSCENE, "[SIMPLE_INTERIOR] AM_MP_BUNKER - GET_BUNKER_CUTSCENE - Returning 0.")
	#ENDIF
	
	RETURN 0
ENDFUNC

PROC HIDE_SCRIPTED_HUD_COMPONENTS_DURING_CUTSCENE_PLAYBACK()
	#IF IS_DEBUG_BUILD
	IF (GET_FRAME_COUNT() % 120) = 0
		CDEBUG1LN(DEBUG_CUTSCENE, "[SIMPLE_INTERIOR] AM_MP_BUNKER - HIDE_SCRIPTED_HUD_COMPONENTS_DURING_CUTSCENE_PLAYBACK - Hiding script HUD components this frame.")
	ENDIF
	#ENDIF
	
	HIDE_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_MP_RANK_BAR)
ENDPROC

PROC MAINTAIN_POST_SETUP_CUTSCENE()

	IF IS_BUNKER_STATE(BUNKER_STATE_PLAYING_SETUP_CUTSCENE)
		HIDE_SCRIPTED_HUD_COMPONENTS_DURING_CUTSCENE_PLAYBACK()
	
		#IF IS_DEBUG_BUILD
		IF (GET_FRAME_COUNT() % 120) = 0
			CDEBUG1LN(DEBUG_CUTSCENE, "[SIMPLE_INTERIOR] AM_MP_BUNKER - MAINTAIN_POST_SETUP_CUTSCENE - Cutscene already playing.")
		ENDIF
		#ENDIF
		EXIT
	ENDIF
	
	IF HAS_LOCAL_PLAYER_VIEWED_INTERIOR_CUTSCENE(biFmCut_Bunker_Cutscene)
		#IF IS_DEBUG_BUILD
		IF (GET_FRAME_COUNT() % 120) = 0
			CDEBUG1LN(DEBUG_CUTSCENE, "[SIMPLE_INTERIOR] AM_MP_BUNKER - MAINTAIN_POST_SETUP_CUTSCENE - Player has already viewed bunker setup cutscene.")
		ENDIF
		#ENDIF
		EXIT
	ENDIF
	
	IF HAS_EXIT_BUNKER_FROM_TRUCK_WITH_VEH_TRIGGERED()
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PreventAutoShuffleToDriversSeat, TRUE)
		CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_BUNKER_MOVE_OUT_OF_WAY)
		SET_SIMPLE_INTERIOR_CHILD_SCRIPT_CAN_TAKE_CONTROL(TRUE)
	ENDIF	
	
	IF NOT thisBunker.introCutscene.bPlaying
		BOOL bShouldCheck
		INT iCutsceneHash

		IF IS_LOCAL_PLAYER_WALKING_INTO_SIMPLE_INTERIOR()
			IF NOT HAS_SIMPLE_INTERIOR_CHILD_SCRIPT_TIMED_OUT()
			AND IS_BUNKER_STATE(BUNKER_STATE_LOADING)
				bShouldCheck = TRUE
			ENDIF
		ELSE
			IF IS_BUNKER_STATE(BUNKER_STATE_IDLE)
				IF NOT IS_LOCAL_PLAYER_WALKING_OUT_OF_SIMPLE_INTERIOR()
				AND NOT IS_BROWSER_OPEN()
				AND NOT IS_SCREEN_FADED_OUT()
				AND NOT IS_MOBILE_PHONE_CALL_ONGOING()
					bShouldCheck = (GET_FRAME_COUNT() % 60 = 0)
				ENDIF
			ENDIF
		ENDIF
		
		IF bShouldCheck
			IF NOT HAS_LOCAL_PLAYER_VIEWED_INTERIOR_CUTSCENE(biFmCut_Bunker_Cutscene)
			AND HAS_PLAYER_COMPLETED_FACTORY_SETUP_MISSION(thisBunker.pOwner, GET_FACTORY_ID_FROM_FACTORY_SLOT(thisBunker.pOwner, thisBunker.iSaveSlot))
				IF PLAYER_ID() != INVALID_PLAYER_INDEX()
					IF PLAYER_ID() = thisBunker.pOwner
						IF NOT IS_PLAYER_IN_SIMPLE_CUTSCENE(PLAYER_ID())
							IF NOT IS_BIT_SET(thisBunker.iBS, BS_BUNKER_DATA_BOSS_WAS_WATCHING_CUTSCENE_LAST_FRAME)
								iCutsceneHash = GET_BUNKER_CUTSCENE(thisBunker.eID)
								#IF IS_DEBUG_BUILD
								CDEBUG1LN(DEBUG_CUTSCENE, "[SIMPLE_INTERIOR] AM_MP_BUNKER - MAINTAIN_POST_SETUP_CUTSCENE - Player was assigned cutscene with hash: ", iCutsceneHash)
								#ENDIF
								
								SET_BIT(thisBunker.iBS, BS_BUNKER_DATA_BOSS_WAS_WATCHING_CUTSCENE_LAST_FRAME)
								#IF IS_DEBUG_BUILD
								CDEBUG1LN(DEBUG_CUTSCENE, "[SIMPLE_INTERIOR] AM_MP_BUNKER - MAINTAIN_POST_SETUP_CUTSCENE - Setting BS_BUNKER_DATA_BOSS_WAS_WATCHING_CUTSCENE_LAST_FRAME")
								#ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF

		IF iCutsceneHash != 0
		AND NOT HAS_LOCAL_PLAYER_VIEWED_INTERIOR_CUTSCENE(biFmCut_Bunker_Cutscene)
		AND (PLAYER_ID() != INVALID_PLAYER_INDEX() AND PLAYER_ID() = thisBunker.pOwner)
		
			#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_CUTSCENE, "[SIMPLE_INTERIOR] AM_MP_BUNKER - MAINTAIN_POST_SETUP_CUTSCENE - Starting cutscene with hash: ", iCutsceneHash)
			#ENDIF

			INITIALISE_POST_SETUP_CUTSCENE_DATA(thisBunker.introCutscene, iCutsceneHash)			
			SIMPLE_CUTSCENE_START(thisBunker.introCutscene)

			IF iCutsceneHash = CI_BUNKER_SETUP_CUTSCENE
				SET_BIT(thisBunker.iBS, BS_BUNKER_DATA_PLAYING_SETUP_CUTSCENE)
			ENDIF
			
			IF NOT HAS_SIMPLE_INTERIOR_CHILD_SCRIPT_TIMED_OUT()
			AND IS_BUNKER_STATE(BUNKER_STATE_LOADING)
				SET_SIMPLE_INTERIOR_CHILD_SCRIPT_IS_IN_CONTROL(TRUE)
				SET_SIMPLE_INTERIOR_CHILD_SCRIPT_READY(FALSE)
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BUNKER_STATE(BUNKER_STATE_IDLE)
	AND thisBunker.introCutscene.bPlaying
		
		IF NOT IS_SCREEN_FADED_OUT()
			/*
			IF NOT IS_SCREEN_FADING_OUT()
				DO_SCREEN_FADE_OUT(SIMPLE_INTERIOR_SCREEN_FADE_TIME)
			ENDIF
			*/
		ELSE
			INT iProductionStage = GlobalplayerBD_FM[NATIVE_TO_INT(thisBunker.pOwner)].propertyDetails.bdFactoryData[thisBunker.iSaveSlot].iProductionStage

			#IF IS_DEBUG_BUILD
			IF (GET_FRAME_COUNT() % 60) = 0
				CDEBUG1LN(DEBUG_CUTSCENE, "[SIMPLE_INTERIOR] AM_MP_BUNKER - Current bunker production stage = ", iProductionStage, ", previous stage when we entered was: ", thisBunker.iProductionStageWhenEntering)
			ENDIF
			#ENDIF

			IF thisBunker.iProductionStageWhenEntering != iProductionStage
				INITIALISE_BUNKER_ENTITY_SETS(iProductionStage)
				REPIN_AND_REFRESH_SIMPLE_INTERIOR()
				
				#IF IS_DEBUG_BUILD
				IF (GET_FRAME_COUNT() % 60) = 0
					CDEBUG1LN(DEBUG_CUTSCENE, "[SIMPLE_INTERIOR] AM_MP_BUNKER - Production stage has changed, activating entity sets and refreshing interior.")
				ENDIF
				#ENDIF
				
				SET_BIT(thisBunker.iBS, BS_BUNKER_DATA_DID_INTERIOR_REFRESH_FOR_CUTSCENE)
				thisBunker.iProductionStageWhenEntering = iProductionStage
			ELSE
				IF NOT IS_BIT_SET(thisBunker.iBS, BS_BUNKER_DATA_DID_INTERIOR_REFRESH_FOR_CUTSCENE)
					SET_BUNKER_STATE(BUNKER_STATE_PLAYING_SETUP_CUTSCENE)
					#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_CUTSCENE, "[SIMPLE_INTERIOR] AM_MP_BUNKER - MAINTAIN_POST_SETUP_CUTSCENE - Production stage is unchanged, launching cutscene.")
					#ENDIF
					EXIT
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(thisBunker.iBS, BS_BUNKER_DATA_DID_INTERIOR_REFRESH_FOR_CUTSCENE)
				IF HAS_SIMPLE_INTERIOR_BEEN_REFRESHED()
					IF NOT IS_BIT_SET(thisBunker.iBS, BS_BUNKER_DATA_LOAD_SCENE_FOR_CUTSCENE_STARTED)
						VECTOR vBunkerCoords
						FLOAT fBunkerHeading, fBunkerRadius
						TEXT_LABEL_63 strInteriorType
						
						SIMPLE_INTERIOR_INSIDE_BOUNDING_BOX bBox
						GET_SIMPLE_INTERIOR_INSIDE_BOUNDING_BOX(thisBunker.eSimpleInteriorID, bBox)
						
						fBunkerRadius = GET_SIMPLE_INTERIOR_RADIUS_FROM_BOUNDING_BOX(bBox)
						GET_SIMPLE_INTERIOR_INTERIOR_TYPE_AND_POSITION(thisBunker.eSimpleInteriorID, strInteriorType, vBunkerCoords, fBunkerHeading)
						
						NEW_LOAD_SCENE_START_SPHERE(vBunkerCoords, fBunkerRadius)
						SET_BIT(thisBunker.iBS, BS_BUNKER_DATA_LOAD_SCENE_FOR_CUTSCENE_STARTED)
						
						#IF IS_DEBUG_BUILD
						CDEBUG1LN(DEBUG_CUTSCENE, "[SIMPLE_INTERIOR] AM_MP_BUNKER - MAINTAIN_POST_SETUP_CUTSCENE - Starting load scene.")
						#ENDIF
					ELSE
						IF IS_NEW_LOAD_SCENE_ACTIVE()
							IF IS_NEW_LOAD_SCENE_LOADED()
								NEW_LOAD_SCENE_STOP()
								SET_BUNKER_STATE(BUNKER_STATE_PLAYING_SETUP_CUTSCENE)
								#IF IS_DEBUG_BUILD
								CDEBUG1LN(DEBUG_CUTSCENE, "[SIMPLE_INTERIOR] AM_MP_BUNKER - MAINTAIN_POST_SETUP_CUTSCENE - Setting bunker state to BUNKER_STATE_PLAYING_INTRO")
								#ENDIF
								
								CLEAR_BIT(thisBunker.iBS, BS_BUNKER_DATA_LOAD_SCENE_FOR_CUTSCENE_STARTED)
								CLEAR_BIT(thisBunker.iBS, BS_BUNKER_DATA_DID_INTERIOR_REFRESH_FOR_CUTSCENE)
							ENDIF
						ELSE
							SET_BUNKER_STATE(BUNKER_STATE_PLAYING_SETUP_CUTSCENE)
							#IF IS_DEBUG_BUILD
							CDEBUG1LN(DEBUG_CUTSCENE, "[SIMPLE_INTERIOR] AM_MP_BUNKER - MAINTAIN_POST_SETUP_CUTSCENE - Setting bunker state to BUNKER_STATE_PLAYING_INTRO")
							#ENDIF
							
							CLEAR_BIT(thisBunker.iBS, BS_BUNKER_DATA_LOAD_SCENE_FOR_CUTSCENE_STARTED)
							CLEAR_BIT(thisBunker.iBS, BS_BUNKER_DATA_DID_INTERIOR_REFRESH_FOR_CUTSCENE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL ARE_ALL_PEDS_IN_PV_READY(VEHICLE_INDEX theVeh #IF IS_DEBUG_BUILD , BOOL bOutputDebug = FALSE #ENDIF)
	INT i
	PLAYER_INDEX thePlayer
	REPEAT NUM_NETWORK_PLAYERS i
		thePlayer = INT_TO_PLAYERINDEX(i)
		
		IF thePlayer != PLAYER_ID()
		AND thePlayer != INVALID_PLAYER_INDEX()
		AND NETWORK_IS_PLAYER_A_PARTICIPANT(thePlayer)
			IF IS_NET_PLAYER_OK(thePlayer)
				IF IS_PED_IN_VEHICLE(GET_PLAYER_PED(thePlayer), theVeh, FALSE)
					IF NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(thePlayer)].iBS, BS_BUNKER_READY_TO_WARP_OUT_W_OWNER)
						#IF IS_DEBUG_BUILD
						IF bOutputDebug
							PRINTLN("ARE_ALL_PEDS_IN_PV_READY: ", GET_PLAYER_NAME(thePlayer)," not ready not set flag")
						ENDIF
						#ENDIF
						
						RETURN FALSE
					ENDIF
				ELSE
					IF IS_PED_IN_VEHICLE(GET_PLAYER_PED(thePlayer), theVeh, TRUE)
					OR IS_ENTITY_ATTACHED_TO_ENTITY(GET_PLAYER_PED(thePlayer), theVeh)
						#IF IS_DEBUG_BUILD
						IF bOutputDebug
							PRINTLN("ARE_ALL_PEDS_IN_PV_READY: ", GET_PLAYER_NAME(thePlayer)," entering or attached")
						ENDIF
						#ENDIF
						
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

FUNC BOOL CAN_PLAYER_DRIVE_OUT_OF_BUNKER()
	IF NOT ARE_ALL_PEDS_IN_PV_READY(currentVeh #IF IS_DEBUG_BUILD , TRUE #ENDIF)
		PRINTLN("CAN_PLAYER_DRIVE_OUT_OF_IE_GARAGE - False NOT ARE_ALL_PEDS_IN_PV_READY")
		RETURN FALSE
	ENDIF
	
	IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) = PERFORMING_TASK
	OR GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) = WAITING_TO_START_TASK
		PRINTLN("CAN_PLAYER_DRIVE_OUT_OF_IE_GARAGE - False leaving a vehicle")
		RETURN FALSE
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(pauseMenuInteractionsDelay)
	AND NOT HAS_NET_TIMER_EXPIRED(pauseMenuInteractionsDelay,1000,TRUE)
		PRINTLN("CAN_PLAYER_DRIVE_OUT_OF_BUNKER- False pause menu timer delay active")
		RETURN FALSE
	ENDIF
	
	IF GET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_IsSeatShuffling)
		PRINTLN("CAN_PLAYER_DRIVE_OUT_OF_BUNKER- False player shuffling")
		RETURN FALSE
	ENDIF
	
	IF g_bDisableBunkerVehicleExit
		PRINTLN("CAN_PLAYER_DRIVE_OUT_OF_BUNKER - g_bDisableBunkerVehicleExit is TRUE")
		RETURN FALSE
	ENDIF
	
	IF IS_PHONE_ONSCREEN()
		PRINTLN("CAN_PLAYER_DRIVE_OUT_OF_BUNKER - IS_PHONE_ONSCREEN")
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

PROC MAINTAIN_CAN_DRIVE_OUT_OF_BUNKER()
	IF IS_NET_PLAYER_OK(PLAYER_ID())
	AND IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
	AND NOT g_MultiplayerSettings.g_bSuicide
	AND NOT IS_SELECTOR_ONSCREEN()
		DISABLE_SELECTOR_THIS_FRAME()
		SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_BUNKER_READY_TO_WARP_OUT_W_OWNER)
	ELSE
		CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_BUNKER_READY_TO_WARP_OUT_W_OWNER)
	ENDIF
ENDPROC

PROC MAINTAIN_HIDE_PERSONAL_VEHICLE()

	VEHICLE_INDEX vehPersonal =	PERSONAL_VEHICLE_ID()	
	
	IF DOES_ENTITY_EXIST(vehPersonal)
	AND NOT IS_ENTITY_DEAD(vehPersonal)
		IF IS_PLAYER_IN_BUNKER(PLAYER_ID())
		AND IS_PLAYER_STARTED_TO_USE_PERSONAL_CAR_MOD(PLAYER_ID())
			IF NOT IS_BIT_SET(thisBunker.iBS2, BS_2_BUNKER_DATA_HID_PV)
				IF NOT NETWORK_IS_ENTITY_CONCEALED(vehPersonal)
					PRINTLN("[SIMPLE_INTERIOR] AM_MP_BUNKER - MAINTAIN_HIDE_PERSONAL_VEHICLE, TRUE ")
					NETWORK_CONCEAL_ENTITY(vehPersonal, TRUE)
					SET_BIT(thisBunker.iBS2, BS_2_BUNKER_DATA_HID_PV)
				ENDIF
			ENDIF			
		ELSE
			IF IS_BIT_SET(thisBunker.iBS2, BS_2_BUNKER_DATA_HID_PV)
				IF NETWORK_IS_ENTITY_CONCEALED(vehPersonal)
					PRINTLN("[SIMPLE_INTERIOR] AM_MP_BUNKER - MAINTAIN_HIDE_PERSONAL_VEHICLE, FALSE ")
					NETWORK_CONCEAL_ENTITY(vehPersonal, FALSE)
					CLEAR_BIT(thisBunker.iBS2, BS_2_BUNKER_DATA_HID_PV)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC MAINTAIN_CONTROL_OF_BUNKER_PERSONAL_CARMOD_VEHICLES()
	
	IF IS_PLAYER_STARTED_TO_USE_PERSONAL_CAR_MOD(PLAYER_ID())
		IF NOT IS_BIT_SET(thisBunker.iBS2, BS_2_BUNKER_DATA_TAKE_CONTROL)
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niCloneTruck)
			AND NETWORK_HAS_CONTROL_OF_ENTITY(NET_TO_ENT(serverBD.niCloneTruck))
			AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niCloneTrailer)
			AND NETWORK_HAS_CONTROL_OF_ENTITY(NET_TO_ENT(serverBD.niCloneTrailer))
			AND NETWORK_HAS_ENTITY_BEEN_REGISTERED_WITH_THIS_THREAD(NET_TO_ENT(serverBD.niCloneTruck))
			AND NETWORK_HAS_ENTITY_BEEN_REGISTERED_WITH_THIS_THREAD(NET_TO_ENT(serverBD.niCloneTrailer))
				SET_BIT(thisBunker.iBS2, BS_2_BUNKER_DATA_TAKE_CONTROL)
				SET_NETWORK_ID_CAN_MIGRATE(serverBD.niCloneTruck, FALSE)
				SET_NETWORK_ID_CAN_MIGRATE(serverBD.niCloneTrailer, FALSE)
			ENDIF
		ENDIF	
	ELSE
		IF IS_BIT_SET(thisBunker.iBS2, BS_2_BUNKER_DATA_TAKE_CONTROL)
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niCloneTruck)
			AND NETWORK_HAS_CONTROL_OF_ENTITY(NET_TO_ENT(serverBD.niCloneTruck))
			AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niCloneTrailer)
			AND NETWORK_HAS_CONTROL_OF_ENTITY(NET_TO_ENT(serverBD.niCloneTrailer))
			AND NETWORK_HAS_ENTITY_BEEN_REGISTERED_WITH_THIS_THREAD(NET_TO_ENT(serverBD.niCloneTruck))
			AND NETWORK_HAS_ENTITY_BEEN_REGISTERED_WITH_THIS_THREAD(NET_TO_ENT(serverBD.niCloneTrailer))
				CLEAR_BIT(thisBunker.iBS2, BS_2_BUNKER_DATA_TAKE_CONTROL)
				SET_NETWORK_ID_CAN_MIGRATE(serverBD.niCloneTruck, TRUE)
				SET_NETWORK_ID_CAN_MIGRATE(serverBD.niCloneTrailer, TRUE)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC MAINTAIN_BLOCK_WHEELIES()

	VEHICLE_INDEX vehId
	
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
	AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())

		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		
			vehId = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		
			IF DOES_ENTITY_EXIST(vehId)
			AND NOT IS_ENTITY_DEAD(vehId)
			
				IF IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(vehId))
				OR IS_THIS_MODEL_A_QUADBIKE(GET_ENTITY_MODEL(vehId))
					IF IS_PLAYER_IN_BUNKER(PLAYER_ID())
						IF NOT IS_BIT_SET(thisBunker.iBS2, BS_2_BUNKER_DATA_BLOCKED_WHEELIES)

							PRINTLN("[SIMPLE_INTERIOR] AM_MP_BUNKER - MAINTAIN_BLOCK_WHEELIES, TRUE ")
							IF NETWORK_HAS_CONTROL_OF_ENTITY(vehId)
								SET_WHEELIE_ENABLED(vehId, FALSE)
								SET_BIT(thisBunker.iBS2, BS_2_BUNKER_DATA_BLOCKED_WHEELIES)
							ENDIF
						ENDIF			
					ELSE
						IF IS_BIT_SET(thisBunker.iBS2, BS_2_BUNKER_DATA_BLOCKED_WHEELIES)

							PRINTLN("[SIMPLE_INTERIOR] AM_MP_BUNKER - MAINTAIN_BLOCK_WHEELIES, FALSE ")
							IF NETWORK_HAS_CONTROL_OF_ENTITY(vehId)
								SET_WHEELIE_ENABLED(vehId, TRUE)
								CLEAR_BIT(thisBunker.iBS2, BS_2_BUNKER_DATA_BLOCKED_WHEELIES)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC MAINTAIN_NON_OWNER_VEHICLE_ENTRANCE()
	IF thisBunker.pOwner != PLAYER_ID()
	AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		VEHICLE_INDEX nearbyVehs[1]
		
		GET_PED_NEARBY_VEHICLES(PLAYER_PED_ID(), nearbyVehs)
		
		IF DOES_ENTITY_EXIST(nearbyVehs[0])
		AND IS_ENTITY_A_VEHICLE(nearbyVehs[0])
		AND IS_VEHICLE_EMPTY(nearbyVehs[0], DEFAULT, TRUE)
			IF (GET_ENTITY_MODEL(nearbyVehs[0]) != CADDY2
			AND GET_ENTITY_MODEL(nearbyVehs[0]) != CADDY3)
			OR NOT IS_ENTITY_FACING_COORD_WITHIN_RANGE(PLAYER_PED_ID(), GET_ENTITY_COORDS(nearbyVehs[0], FALSE), 50)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
			ENDIF
		ENDIF
		
		IF GET_VEHICLE_PED_IS_TRYING_TO_ENTER(PLAYER_PED_ID()) != 0
			VEHICLE_INDEX vehTemp = GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID())
			
			IF DOES_ENTITY_EXIST(vehTemp)
			AND IS_VEHICLE_EMPTY(vehTemp, DEFAULT, TRUE)
			AND IS_VEHICLE_A_PERSONAL_VEHICLE(vehTemp)
				CLEAR_PED_TASKS(PLAYER_PED_ID())
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL CAN_EXIT_WITH_THIS_VEHICLE(VEHICLE_INDEX vehIndex)
	#IF FEATURE_GEN9_EXCLUSIVE
	IF IS_PLAYER_ON_MP_INTRO()
		RETURN FALSE
	ENDIF
	#ENDIF
	
	IF IS_VEHICLE_A_PERSONAL_TRUCK(vehIndex)
		RETURN TRUE
	ENDIF
	
	IF GET_ENTITY_MODEL(vehIndex) = OPPRESSOR2
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


PROC MAINTAIN_PERSONAL_VEHICLE_LOCKS()
	g_bPlayerLeavingCurrentInteriorInVeh = FALSE
	IF thisBunker.pOwner != INVALID_PLAYER_INDEX()
		IF thisBunker.pOwner = PLAYER_ID()
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), FALSE)
				currentVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(), TRUE)
				
				IF IS_VEHICLE_MODEL(currentVeh, OPPRESSOR2)
					IF GET_IS_VEHICLE_ENGINE_RUNNING(currentVeh)
					AND NOT g_bPlayerLeavingCurrentInteriorInVeh
						SET_VEHICLE_ENGINE_ON(currentVeh, FALSE, TRUE)
					ENDIF
					
					SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableStartEngine, TRUE)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BRAKE)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
				ELSE
					SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableStartEngine, FALSE)
				ENDIF
				
				IF IS_VEHICLE_A_PERSONAL_VEHICLE(currentVeh)
				OR IS_VEHICLE_A_PERSONAL_TRUCK(currentVeh)
				OR IS_VEHICLE_UPGRADABLE_PEGASUS_VEHICLE(currentVeh)
					IF NOT g_bPlayerLeavingCurrentInteriorInVeh
					AND NOT g_bDrivingOutOfBunker
						IF IS_VEHICLE_A_PERSONAL_VEHICLE(currentVeh)
						OR IS_VEHICLE_UPGRADABLE_PEGASUS_VEHICLE(currentVeh)
							IF PLAYER_PED_ID() = GET_PED_IN_VEHICLE_SEAT(currentVeh)
							AND NETWORK_HAS_CONTROL_OF_ENTITY(currentVeh)
								IF GET_ENTITY_MODEL(currentVeh) = OPPRESSOR2
									SET_VEHICLE_MAX_SPEED(currentVeh,3)
								ELSE
									SET_VEHICLE_MAX_SPEED(currentVeh,7)
								ENDIF
//								#IF FEATURE_GANG_OPS
//								IF GET_ENTITY_MODEL(currentVeh) = DELUXO
//									SET_DISABLE_HOVER_MODE_FLIGHT(currentVeh,TRUE)
//									SET_SPECIAL_FLIGHT_MODE_RATIO(currentVeh,0)
//									SET_SPECIAL_FLIGHT_MODE_ALLOWED(currentVeh,FALSE)
//								ENDIF
//								#ENDIF
							ENDIF	
						ENDIF
					ENDIF
					
					IF currentVeh != lastVeh
						IF NETWORK_HAS_CONTROL_OF_ENTITY(currentVeh)
							lastVeh = currentVeh
							SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(currentVeh, FALSE)
							SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(currentVeh, FALSE)
							PRINTLN("MAINTAIN_PERSONAL_VEHICLE_LOCKS: unlocking doors for remote players")
						ELSE
							NETWORK_REQUEST_CONTROL_OF_ENTITY(currentVeh)
						ENDIF
					ENDIF
					IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
					AND CAN_EXIT_WITH_THIS_VEHICLE(currentVeh)
						IF (GET_IS_VEHICLE_ENGINE_RUNNING(currentVeh)
						OR GET_ENTITY_MODEL(currentVeh) = OPPRESSOR2)
						AND PLAYER_PED_ID() = GET_PED_IN_VEHICLE_SEAT(currentVeh)
						AND NOT IS_PLAYER_WALKING_INTO_SIMPLE_INTERIOR(PLAYER_ID())
							IF CAN_PLAYER_DRIVE_OUT_OF_BUNKER()
								IF (GET_DISABLED_CONTROL_UNBOUND_NORMAL(PLAYER_CONTROL, INPUT_VEH_ACCELERATE) != 0
								OR GET_DISABLED_CONTROL_UNBOUND_NORMAL(PLAYER_CONTROL, INPUT_VEH_BRAKE) != 0
								OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
								OR IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
								OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
								OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_BRAKE)
								OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_BRAKE)
								OR IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_BRAKE)
								OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_BRAKE))
								AND NOT g_MultiplayerSettings.g_bSuicide	
								AND NOT IS_SELECTOR_ONSCREEN()
									IF NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_BUNKER_OWNER_IS_LEAVING_BUNKER_IN_TRUCK)
										SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_BUNKER_OWNER_IS_LEAVING_BUNKER_IN_TRUCK)
									ENDIF
									
									IF IS_VEHICLE_MODEL(currentVeh, OPPRESSOR2)
										SET_VEHICLE_ENGINE_ON(currentVeh, TRUE, TRUE)
									ENDIF
									
									g_bPlayerLeavingCurrentInteriorInVeh = TRUE
									g_bDrivingOutOfBunker = TRUE
									PRINTLN("g_bPlayerLeavingCurrentInteriorInVeh = TRUE BUNKER")
									IF NETWORK_HAS_CONTROL_OF_ENTITY(currentVeh)
										SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(currentVeh, TRUE) 
										SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(currentVeh, TRUE)
										SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(currentVeh, PLAYER_ID(), FALSE)
										DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
									ELSE
										NETWORK_REQUEST_CONTROL_OF_ENTITY(currentVeh)
									ENDIF
								ENDIF
							ELSE
								DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BRAKE)
								DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
							ENDIF
						ENDIF
					ELSE
						IF IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_BUNKER_OWNER_IS_LEAVING_BUNKER_IN_TRUCK)
							CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_BUNKER_OWNER_IS_LEAVING_BUNKER_IN_TRUCK)
						ENDIF	
					ENDIF
				ENDIF
			ELSE
				IF IS_VEHICLE_DRIVEABLE(lastVeh)
					IF NETWORK_HAS_CONTROL_OF_ENTITY(lastVeh)
						#IF FEATURE_GEN9_EXCLUSIVE
						IF IS_PLAYER_ON_MP_INTRO()
							IF NOT GET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(lastVeh, PLAYER_ID())
							AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
								SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(lastVeh, TRUE)
							ENDIF
						ELSE
						#ENDIF
							SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(lastVeh, TRUE)
			        		SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(lastVeh, PLAYER_ID(), FALSE)
							lastVeh = NULL
						#IF FEATURE_GEN9_EXCLUSIVE
						ENDIF
						#ENDIF
					ELSE
						NETWORK_REQUEST_CONTROL_OF_ENTITY(lastVeh)
					ENDIF
				ENDIF
				IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
					IF thisBunker.pOwner != INVALID_PLAYER_INDEX()
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverbd.niCloneTruck)
							IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverbd.niCloneTruck)
								IF NOT IS_ENTITY_DEAD(NET_TO_VEH(serverbd.niCloneTruck))
									SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(NET_TO_VEH(serverbd.niCloneTruck), TRUE)
				        			IF thisBunker.pOwner = PLAYER_ID() 
										SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(NET_TO_VEH(serverbd.niCloneTruck), PLAYER_ID(), FALSE)
										PRINTLN("MAINTAIN_PERSONAL_VEHICLE_LOCKS - I'm the owner locking truck")
									ELSE
										PRINTLN("MAINTAIN_PERSONAL_VEHICLE_LOCKS - I'm not the owner locking truck")
									ENDIF
								ENDIF	
							ELSE
								NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverbd.niCloneTruck)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF IS_NET_PLAYER_OK(PLAYER_ID())
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				AND NOT IS_PLAYER_WALKING_INTO_SIMPLE_INTERIOR(PLAYER_ID())
				AND IS_BUNKER_STATE(BUNKER_STATE_IDLE)
				AND GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()) = HASH("bunker_ModRoom")
					IF IS_VEHICLE_A_PERSONAL_TRUCK(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
						IF NOT IS_PED_TRYING_TO_ENTER_OR_EXIT_VEHICLE(PLAYER_PED_ID())
						AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != PERFORMING_TASK
						AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != WAITING_TO_START_TASK
							IF IS_BIT_SET(playerBD[NATIVE_TO_INT(thisbunker.pOwner)].iBS, BS_BUNKER_OWNER_IS_LEAVING_BUNKER_IN_TRUCK)
								NET_SET_PLAYER_CONTROL(PLAYER_ID() , FALSE)
								SET_PASSENGER_READY_TO_EXIT_BUNKER_IN_TRUCK(TRUE)
								PRINTLN("MAINTAIN_PERSONAL_VEHICLE_LOCKS - I'm exiting bunker with truck driver")
							ENDIF
						ENDIF	
					ENDIF
				ENDIF
				IF IS_PASSENGER_READY_TO_EXIT_BUNKER_IN_TRUCK(PLAYER_ID())
					IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
						IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							NET_SET_PLAYER_CONTROL(PLAYER_ID() , TRUE)
							SET_PASSENGER_READY_TO_EXIT_BUNKER_IN_TRUCK(FALSE)
							PRINTLN("MAINTAIN_PERSONAL_VEHICLE_LOCKS - player no longer in vehicle")
						ENDIF
					ENDIF
				ENDIF
			ENDIF	
		ENDIF
	ENDIF	
ENDPROC

PROC MAINTAIN_GET_OUT_OF_OWNERS_PERSONAL_VEHICLE()
	VEHICLE_INDEX vehIndex
	PED_INDEX DriverPedID
	
	IF thisBunker.pOwner != PLAYER_ID()
		IF IS_NET_PLAYER_OK(PLAYER_ID())
		AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
				vehIndex = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(), TRUE)
				IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBS, BS_SIMPLE_INTERIOR_GLOBAL_PLAYER_BD_DRIVER_ENTERING)
					IF IS_VEHICLE_A_PERSONAL_VEHICLE(vehIndex)
					OR IS_VEHICLE_A_PERSONAL_TRUCK(vehIndex)
					OR IS_VEHICLE_UPGRADABLE_PEGASUS_VEHICLE(vehIndex)
						IF thisBunker.pOwner  != INVALID_PLAYER_INDEX()
							IF IS_NET_PLAYER_OK(thisBunker.pOwner, FALSE, TRUE)
								IF NOT IS_VEHICLE_SEAT_FREE(vehIndex)
									DriverPedID = GET_PED_IN_VEHICLE_SEAT(vehIndex, VS_DRIVER)	
									
									IF DOES_ENTITY_EXIST(DriverPedID)
									AND IS_PED_A_PLAYER(DriverPedID)
										IF NETWORK_GET_PLAYER_INDEX_FROM_PED(DriverPedID) != thisBunker.pOwner
											IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehIndex, FALSE)
												IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != PERFORMING_TASK
												AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != WAITING_TO_START_TASK
													TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
													PRINTLN("MAINTAIN_GET_OUT_OF_OWNERS_PERSONAL_VEHICLE: driver isn't owner, leave vehicle")
												ENDIF
											ELIF GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID()) = vehIndex
												CLEAR_PED_TASKS(PLAYER_PED_ID())
												PRINTLN("MAINTAIN_GET_OUT_OF_OWNERS_PERSONAL_VEHICLE: driver isn't owner, clear tasks")
											ENDIF
										ENDIF
									ENDIF
								ELSE
									IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehIndex, FALSE)
										IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != PERFORMING_TASK
										AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != WAITING_TO_START_TASK
											TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
											PRINTLN("MAINTAIN_GET_OUT_OF_OWNERS_PERSONAL_VEHICLE: no driver, leave vehicle")
											IF !CAN_SIMPLE_INTERIOR_CHILD_SCRIPT_TAKE_CONTROL()
												SET_SIMPLE_INTERIOR_CHILD_SCRIPT_CAN_TAKE_CONTROL(TRUE)
											ENDIF	
										ENDIF
									ELIF GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID()) = vehIndex
										CLEAR_PED_TASKS(PLAYER_PED_ID())
										PRINTLN("MAINTAIN_GET_OUT_OF_OWNERS_PERSONAL_VEHICLE: no driver, clear tasks")
									ENDIF
								ENDIF
							ELSE
								IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehIndex, FALSE)
									IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != PERFORMING_TASK
									AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != WAITING_TO_START_TASK
										TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
										PRINTLN("MAINTAIN_GET_OUT_OF_OWNERS_PERSONAL_VEHICLE: getting out owner is not ok, leave vehicle")
									ENDIF
								ELIF GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID()) = vehIndex
									CLEAR_PED_TASKS(PLAYER_PED_ID())
									PRINTLN("MAINTAIN_GET_OUT_OF_OWNERS_PERSONAL_VEHICLE: no driver, clear tasks")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	#IF FEATURE_GEN9_EXCLUSIVE
	IF thisBunker.pOwner = PLAYER_ID()
	AND IS_PLAYER_ON_MP_INTRO()
		IF IS_NET_PLAYER_OK(PLAYER_ID())
		AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
				vehIndex = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(), TRUE)
				
				IF IS_VEHICLE_A_PERSONAL_VEHICLE(vehIndex)
					IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != PERFORMING_TASK
					AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != WAITING_TO_START_TASK
						TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
						
						PRINTLN("MAINTAIN_GET_OUT_OF_OWNERS_PERSONAL_VEHICLE: drive is on MP Intro")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	#ENDIF
ENDPROC

PROC MAINTAIN_ARMORY_TRUCK_EXIT_TO_CAB_IN_BUNKER()

	IF g_SimpleInteriorData.bTriggerExitFromArmoryTruckToCab
		IF NOT IS_SCREEN_FADED_OUT()
			DO_SCREEN_FADE_OUT(500)
			CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] MAINTAIN_ARMORY_TRUCK_EXIT_TO_CAB_IN_BUNKER - Fading screen out.")
		ELSE
			IF (DOES_ENTITY_EXIST(PLAYER_PED_ID()) AND IS_ENTITY_ALIVE(PLAYER_PED_ID()))
				IF (DOES_ENTITY_EXIST(g_viGunRunTruckInBunker) AND IS_ENTITY_ALIVE(g_viGunRunTruckInBunker))
					
					IF IS_VEHICLE_ATTACHED_TO_TRAILER(g_viGunRunTruckInBunker)
						
						IF IS_VEHICLE_SEAT_FREE(g_viGunRunTruckInBunker, VS_ANY_PASSENGER)
							TASK_ENTER_VEHICLE(PLAYER_PED_ID(), g_viGunRunTruckInBunker, DEFAULT, VS_ANY_PASSENGER, DEFAULT, ECF_WARP_PED)
							CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] MAINTAIN_ARMORY_TRUCK_EXIT_TO_CAB_IN_BUNKER - Tasking player ped to enter armory truck cab, any passenger seat.")

						ELSE
							CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] MAINTAIN_ARMORY_TRUCK_EXIT_TO_CAB_IN_BUNKER - No seats available in cab, aborting exit to cab.")
							g_SimpleInteriorData.bTriggerExitFromArmoryTruckToCab = FALSE
						ENDIF
					ELSE
						CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] MAINTAIN_ARMORY_TRUCK_EXIT_TO_CAB_IN_BUNKER - Armoury truck cab is not attached to its trailer.")
						g_SimpleInteriorData.bTriggerExitFromArmoryTruckToCab = FALSE
					ENDIF
				
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), g_viGunRunTruckInBunker, TRUE)						
						g_SimpleInteriorData.bTriggerExitFromArmoryTruckToCab = FALSE
						CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] MAINTAIN_ARMORY_TRUCK_EXIT_TO_CAB_IN_BUNKER - g_SimpleInteriorData.bTriggerExitFromArmoryTruckToCab = FALSE")
						
						IF IS_SCREEN_FADED_OUT()
							DO_SCREEN_FADE_IN(500)
							CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] MAINTAIN_ARMORY_TRUCK_EXIT_TO_CAB_IN_BUNKER - Fading screen in.")
						ENDIF
					ENDIF
				ELSE
					CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] MAINTAIN_ARMORY_TRUCK_EXIT_TO_CAB_IN_BUNKER - Armoury truck entity does not exist. Cannot set ped into truck cab.")
					g_SimpleInteriorData.bTriggerExitFromArmoryTruckToCab = FALSE
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] MAINTAIN_ARMORY_TRUCK_EXIT_TO_CAB_IN_BUNKER - Player entity does not exist. Cannot set ped into truck cab.")
				g_SimpleInteriorData.bTriggerExitFromArmoryTruckToCab = FALSE
			ENDIF
			
			IF IS_SCREEN_FADED_OUT()
			AND !g_SimpleInteriorData.bTriggerExitFromArmoryTruckToCab
				DO_SCREEN_FADE_IN(500)
				CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] MAINTAIN_ARMORY_TRUCK_EXIT_TO_CAB_IN_BUNKER - Fading screen in.")
			ENDIF
		ENDIF
	ELSE
		IF (DOES_ENTITY_EXIST(PLAYER_PED_ID()) AND IS_ENTITY_ALIVE(PLAYER_PED_ID()))
			IF (DOES_ENTITY_EXIST(g_viGunRunTruckInBunker) AND IS_ENTITY_ALIVE(g_viGunRunTruckInBunker))
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), g_viGunRunTruckInBunker, TRUE)
				AND IS_BIT_SET(g_SimpleInteriorData.iBSTruckExit, BS_SIMPLE_INTERIOR_GLOBAL_DATA_EXIT_TO_TRUCK_CAB_IN_BUNKER)
					CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] MAINTAIN_ARMORY_TRUCK_EXIT_TO_CAB_IN_BUNKER - Clearing BS_SIMPLE_INTERIOR_GLOBAL_DATA_EXIT_TO_TRUCK_CAB_IN_BUNKER")
					CLEAR_BIT(g_SimpleInteriorData.iBSTruckExit, BS_SIMPLE_INTERIOR_GLOBAL_DATA_EXIT_TO_TRUCK_CAB_IN_BUNKER)
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] MAINTAIN_ARMORY_TRUCK_EXIT_TO_CAB_IN_BUNKER - Armoury truck entity does not exist.")
			ENDIF
		ELSE
			CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] MAINTAIN_ARMORY_TRUCK_EXIT_TO_CAB_IN_BUNKER - Player entity does not exist.")
		ENDIF
	ENDIF
ENDPROC

SCRIPT_TIMER sFadeInTimer
PROC KICK_PLAYERS_OUT_OF_VEHICLE_IN_BUNKER_AFTER_AUTOWARP_AS_PASSENGER() 
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF HAS_EXIT_BUNKER_FROM_TRUCK_WITH_VEH_TRIGGERED()
			IF NOT HAS_NET_TIMER_STARTED(sFadeInTimer)
			AND IS_PLAYER_IN_BUNKER(thisBunker.pOwner)
				START_NET_TIMER(sFadeInTimer)
			ELSE
				IF GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()) = HASH("bunker_bigbit")
				AND HAS_NET_TIMER_EXPIRED(sFadeInTimer, 5000)
					IF g_iSimpleInteriorState >= SIMPLE_INT_STATE_ENTER_SCREEN_FADE_IN
						IF IS_SCREEN_FADED_OUT()
							DO_SCREEN_FADE_IN(500)
						ELIF IS_SCREEN_FADED_IN()
						
							IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
								IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
									FREEZE_ENTITY_POSITION(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), FALSE)
									FORCE_PLAYER_LOCALLY_VISIBLE_FOR_FADE()
									NETWORK_FADE_IN_ENTITY(PLAYER_PED_ID(), TRUE)
								ENDIF
							ENDIF
							NET_SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)	
							FORCE_CONCEAL_MY_ACTIVE_PERSONAL_VEHICLE(FALSE)
							SET_DRIVER_ENTERING_SIMPLE_INTERIOR(FALSE)
							SET_EXIT_BUNKER_FROM_TRUCK_WITH_VEH_TRIGGERED(FALSE)
							RESET_NET_TIMER(sFadeInTimer)
							PRINTLN("[SIMPLE_INTERIOR][INT_SCRIPT] KICK_PLAYERS_OUT_OF_VEHICLE_IN_BUNKER_AFTER_AUTOWARP_AS_PASSENGER -  BS_SIMPLE_INTERIOR_GLOBAL_PLAYER_BD_DRIVER_ENTERING FALSE")
						ENDIF	
					ENDIF
				ENDIF
			ENDIF
		ENDIF	
	ENDIF	
ENDPROC

FUNC BOOL MAINTAIN_LAUNCHING_CARMOD_SCRIPT()
	
	IF thisBunker.pOwner != INVALID_PLAYER_INDEX()

		//Make sure the car mod script is not started too early after being killed
		//Make sure the car mod script cleanup flag is false
		IF g_bCleanUpCarmodShop = TRUE
			IF NOT HAS_NET_TIMER_STARTED(thisBunker.sCarModScriptRunTimer)
				START_NET_TIMER(thisBunker.sCarModScriptRunTimer)
			ELSE
				IF HAS_NET_TIMER_EXPIRED(thisBunker.sCarModScriptRunTimer, 8000)
					g_bCleanUpCarmodShop = FALSE
					RESET_NET_TIMER(thisBunker.sCarModScriptRunTimer)
					#IF IS_DEBUG_BUILD
					PRINTLN("MAINTAIN_LAUNCHING_CARMOD_SCRIPT - Setting g_bCleanUpCarmodShop To FALSE")
					#ENDIF
				ENDIF	
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(thisBunker.iBS,BS_BUNKER_IS_CAR_MOD_SCRIPT_READY)
			thisBunker.sChildOfChildScript = "personal_carmod_shop"
			REQUEST_SCRIPT(thisBunker.sChildOfChildScript)
			IF HAS_SCRIPT_LOADED(thisBunker.sChildOfChildScript)
			AND NOT IS_THREAD_ACTIVE(thisBunker.CarModThread)
			AND !g_bCleanUpCarmodShop
				
				g_iCarModInstance = thisBunker.iScriptInstance
				
				IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(thisBunker.sChildOfChildScript)) < 1
					IF NOT NETWORK_IS_SCRIPT_ACTIVE(thisBunker.sChildOfChildScript, g_iCarModInstance, TRUE)
						SHOP_LAUNCHER_STRUCT sShopLauncherData
						sShopLauncherData.bLinkedShop = FALSE
						sShopLauncherData.eShop = CARMOD_SHOP_PERSONALMOD
						sShopLauncherData.iNetInstanceID = g_iCarModInstance
						sShopLauncherData.ePersonalCarModVariation = PERSONAL_CAR_MOD_VARIATION_BUNKER
						
						g_iPersonalCarModVariation = ENUM_TO_INT(sShopLauncherData.ePersonalCarModVariation)
						thisBunker.CarModThread = START_NEW_SCRIPT_WITH_ARGS(thisBunker.sChildOfChildScript, sShopLauncherData, SIZE_OF(sShopLauncherData), CAR_MOD_SHOP_STACK_SIZE)
						SET_SCRIPT_AS_NO_LONGER_NEEDED(thisBunker.sChildOfChildScript)
						SET_BIT(thisBunker.iBS,BS_BUNKER_IS_CAR_MOD_SCRIPT_READY)
						g_sShopSettings.bShopScriptLaunched[ENUM_TO_INT(CARMOD_SHOP_PERSONALMOD)] = TRUE
						g_sShopSettings.bShopScriptLaunchedInMP[ENUM_TO_INT(CARMOD_SHOP_PERSONALMOD)] = NETWORK_IS_GAME_IN_PROGRESS()
						PRINTLN("MAINTAIN_LAUNCHING_CARMOD_SCRIPT - TRUE sShopLauncherData.ePersonalCarModVariation: ", sShopLauncherData.ePersonalCarModVariation)
						RETURN TRUE
					ELSE
						#IF IS_DEBUG_BUILD
							PRINTLN("MAINTAIN_LAUNCHING_CARMOD_SCRIPT: Unable to start shop script for personal_car_mod_shop - last instance still active")
						#ENDIF					
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
					IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
						CLEAN_UP_PERSONAL_CAR_MOD()
						TERMINATE_THREAD(thisBunker.CarModThread)
					ENDIF
					#ENDIF
					
					PRINTLN("MAINTAIN_LAUNCHING_CARMOD_SCRIPT - carmod is already running")
					RETURN TRUE 
				ENDIF
			ELSE
				PRINTLN("MAINTAIN_LAUNCHING_CARMOD_SCRIPT - is car mod thread active: " , IS_THREAD_ACTIVE(thisBunker.CarModThread))
				PRINTLN("MAINTAIN_LAUNCHING_CARMOD_SCRIPT - is car mod loaded: " , HAS_SCRIPT_LOADED(thisBunker.sChildOfChildScript))
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
				CLEAN_UP_PERSONAL_CAR_MOD()
				TERMINATE_THREAD(thisBunker.CarModThread)
			ENDIF
			#ENDIF
			
			RETURN TRUE
		ENDIF
	ELSE
		PRINTLN("MAINTAIN_LAUNCHING_CARMOD_SCRIPT - thisBunker.pOwner is invalid")
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTLN("MAINTAIN_LAUNCHING_CARMOD_SCRIPT - return false")
	#ENDIF
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_BUNKER_POST_SETUP_SCENE_STATE()

	IF NOT IS_LOCAL_PLAYER_WALKING_OUT_OF_SIMPLE_INTERIOR()
		SIMPLE_CUTSCENE_MAINTAIN(thisBunker.introCutscene)
		
		IF thisBunker.introCutscene.bPlaying
			HIDE_SCRIPTED_HUD_COMPONENTS_DURING_CUTSCENE_PLAYBACK()

			IF thisBunker.iProductAnimationStage = -1
				HIDE_ALL_FACTORY_PRODUCT()
				
				IF NOT IS_BIT_SET(thisBunker.iBS, BS_BUNKER_DATA_PLAYING_SETUP_CUTSCENE)
					thisBunker.iProductAnimationStage = 0
					thisBunker.iDemoProductUpSoundID = GET_SOUND_ID()
					thisBunker.iDemoProductDownSoundID = GET_SOUND_ID()
				ENDIF
				
				thisBunker.iProductAnimationStage = 0
				CDEBUG1LN(DEBUG_CUTSCENE, "[SIMPLE_INTERIOR] AM_MP_BUNKER - MAINTAIN_BUNKER_POST_SETUP_SCENE_STATE thisBunker.iProductAnimationStage = -1")
			ELSE
				//CREATE_DEMO_PRODUCT()
				//MAINTAIN_DEMO_PRODUCT()
			ENDIF

			// Scene stock_empty start time: 7500, duration: 7500
			// Scene stock_full start time: 15000, duration: 7500

//			IF thisBunker.introCutscene.iCurrentScene > 0 // Check that the current scene is "stock_empty" or "stock_full"
//			AND thisBunker.iProductAnimationStage > -1
//				
//				TEXT_LABEL_23 soundName
//			
//				SWITCH thisBunker.iProductAnimationStage
//					CASE 0
//						IF GET_GAME_TIMER() >= thisBunker.introCutscene.iCurrentSceneTimer + BUNKER_CUTSCENE_DEMO_PRODUCT_FULL_FILL_DELAY
//							thisBunker.iProductAnimationStage++
//							SHOW_DEMO_PRODUCT(GET_FACTORY_PRODUCT_CAPACITY(thisBunker.eID), BUNKER_CUTSCENE_DEMO_PRODUCT_FULL_FILL_TIME)
//							
//							soundName = GET_DEMO_PRODUCT_SOUND("Up")
//							PLAY_SOUND_FRONTEND(thisBunker.iDemoProductUpSoundID, soundName, "DLC_Biker_Warehouse_Intro_Inventory_Sounds")
//							SET_VARIABLE_ON_SOUND(thisBunker.iDemoProductUpSoundId, "Time", TO_FLOAT(BUNKER_CUTSCENE_DEMO_PRODUCT_FULL_FILL_TIME) / 1000.0)
//						ENDIF
//					BREAK
//					CASE 1						
//						IF thisBunker.introCutscene.iCurrentScene = 2
//						AND GET_GAME_TIMER() >= thisBunker.introCutscene.iCurrentSceneTimer + BUNKER_CUTSCENE_DEMO_PRODUCT_FULL_FILL_SUSTAIN
//							thisBunker.iProductAnimationStage++
//							SHOW_DEMO_PRODUCT(0, BUNKER_CUTSCENE_DEMO_PRODUCT_FULL_EMPTY_TIME)
//							
//							soundName = GET_DEMO_PRODUCT_SOUND("Down")
//							PLAY_SOUND_FRONTEND(thisBunker.iDemoProductDownSoundID, soundName, "DLC_Biker_Warehouse_Intro_Inventory_Sounds")
//							SET_VARIABLE_ON_SOUND(thisBunker.iDemoProductDownSoundID, "Time", TO_FLOAT(BUNKER_CUTSCENE_DEMO_PRODUCT_FULL_EMPTY_TIME) / 1000.0)
//						ENDIF
//					BREAK
//				ENDSWITCH
//			ENDIF
		ELSE
			thisBunker.iProductAnimationStage = -1
			SHOW_ALL_FACTORY_PRODUCT()
			CLEANUP_DEMO_PRODUCT()
			
			DO_SCREEN_FADE_IN(SIMPLE_INTERIOR_SCREEN_FADE_TIME)
			
			IF NOT IS_SIMPLE_INTERIOR_CHILD_SCRIPT_IN_CONTROL()				
				NETWORK_FADE_IN_ENTITY(PLAYER_PED_ID(), TRUE)
				FORCE_PLAYER_LOCALLY_VISIBLE_FOR_FADE()
			ELSE
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					IF IS_ENTITY_VISIBLE(PLAYER_PED_ID())
						#IF IS_DEBUG_BUILD
						CDEBUG1LN(DEBUG_SAFEHOUSE, "[SIMPLE_INTERIOR] AM_MP_BUNKER - MAINTAIN_BUNKER_POST_SETUP_SCENE_STATE - Player is visible after cutscene. Visibility reset, making player invisible again.")
						#ENDIF
						SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT IS_SIMPLE_INTERIOR_CHILD_SCRIPT_READY()
				SET_SIMPLE_INTERIOR_CHILD_SCRIPT_READY(TRUE)
				SET_SIMPLE_INTERIOR_CHILD_SCRIPT_IS_IN_CONTROL(FALSE)
				g_SimpleInteriorData.bDontSimulateInputGaitOnEntrance = TRUE
			ENDIF
				
			IF thisBunker.pOwner = PLAYER_ID()
				IF IS_BIT_SET(thisBunker.iBS, BS_BUNKER_DATA_PLAYING_SETUP_CUTSCENE)
					SET_LOCAL_PLAYER_COMPLETED_BUNKER_SETUP_CUTSCENE(TRUE)
				ENDIF
				
				IF NOT IS_BIT_SET(thisBunker.iBS, BS_BUNKER_DATA_PLAYING_SETUP_CUTSCENE)
					RELEASE_SOUND_ID(thisBunker.iDemoProductUpSoundID)
					RELEASE_SOUND_ID(thisBunker.iDemoProductDownSoundID)
				ENDIF
			ENDIF
				
			IF IS_BIT_SET(thisBunker.iBS, BS_BUNKER_DATA_PLAYING_SETUP_CUTSCENE)
				CLEAR_BIT(thisBunker.iBS, BS_BUNKER_DATA_PLAYING_SETUP_CUTSCENE)
			ENDIF

			#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[SIMPLE_INTERIOR] AM_MP_BUNKER - MAINTAIN_BUNKER_POST_SETUP_SCENE_STATE - Setting bunker state to BUNKER_STATE_IDLE")
			#ENDIF
			SET_BUNKER_STATE(BUNKER_STATE_IDLE)
			SIMPLE_INTERIOR_SET_UPDATE_EXIT_LOCATE_COORDS(TRUE)
		ENDIF
	ELSE
		IF IS_SCREEN_FADED_OUT()
			#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[SIMPLE_INTERIOR] AM_MP_BUNKER - MAINTAIN_BUNKER_POST_SETUP_SCENE_STATE - Stopping cutscene.")
			#ENDIF
			
			SIMPLE_CUTSCENE_STOP(thisBunker.introCutscene)			
			
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_LOCAL_PLAYER_WITHIN_BUNKER_BOUNDS()
	SIMPLE_INTERIOR_INSIDE_BOUNDING_BOX bBox
	GET_SIMPLE_INTERIOR_INSIDE_BOUNDING_BOX(thisBunker.eSimpleInteriorID, bBox)
	//IF NOT thisBunker.details.bInteriorIsSeamless
		BOOL bInBounds = IS_ENTITY_IN_AREA(PLAYER_PED_ID(), bBox.vInsideBBoxMin, bBox.vInsideBBoxMax)
		#IF IS_DEBUG_BUILD
			//IF NOT bInBounds
				VECTOR vPlayerCoords = GET_PLAYER_COORDS(PLAYER_ID())
				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_BUNKER - IS_LOCAL_PLAYER_WITHIN_BUNKER_BOUNDS - Player pos: ", vPlayerCoords)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_BUNKER - IS_LOCAL_PLAYER_WITHIN_BUNKER_BOUNDS - Bounds: ", bBox.vInsideBBoxMin, bBox.vInsideBBoxMax)
			//ENDIF
		#ENDIF
		
		RETURN bInBounds
//	ELSE
//		RETURN thisInterior.interiorIndex = GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID())
//	ENDIF
ENDFUNC

FUNC BOOL IS_SAFE_TO_SPAWN_PV(BOOL bCheckOwner = FALSE)
	PLAYER_INDEX thePlayer
	INT i
	#IF IS_DEBUG_BUILD
	VECTOR vCoords
	#ENDIF
	REPEAT NUM_NETWORK_PLAYERS i
		thePlayer = INT_TO_PLAYERINDEX(i)
		
		IF thePlayer = PLAYER_ID()
		AND NOT bCheckOwner
			RELOOP
		ENDIF
		
		IF thePlayer != INVALID_PLAYER_INDEX()
		AND NETWORK_IS_PLAYER_A_PARTICIPANT(thePlayer)
			IF IS_NET_PLAYER_OK(thePlayer)
				#IF IS_DEBUG_BUILD
				vCoords = GET_ENTITY_COORDS(GET_PLAYER_PED(thePlayer))
				PRINTLN("IS_SAFE_TO_SPAWN_PV: checking player ", GET_PLAYER_NAME(thePlayer),"  they are located at: ",vCoords)
				#ENDIF
				IF IS_ENTITY_IN_ANGLED_AREA(GET_PLAYER_PED(thePlayer), <<883.123779,-3243.632813,-199.273224>>, <<883.033020,-3236.130615,-96.536865>>, 7.5)
					IF IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(thePlayer))
					AND IS_VEHICLE_A_PERSONAL_TRUCK(GET_VEHICLE_PED_IS_IN(GET_PLAYER_PED(thePlayer)))
						// Player is waiting in MOC, skip them
					ELSE
						SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_BUNKER_MOVE_OUT_OF_WAY)
					
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	i = 0
	
	REPEAT NUMBER_OF_BUNKER_VEHICLES i
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niBunkerVehicles[i])
		AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niBunkerVehicles[i])
		AND IS_ENTITY_IN_ANGLED_AREA(NET_TO_VEH(serverBD.niBunkerVehicles[i]), <<883.123779,-3243.632813,-199.273224>>, <<883.033020,-3236.130615,-96.536865>>, 7.5)
			SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_BUNKER_MOVE_OUT_OF_WAY)
			
			RETURN FALSE
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_SAFE_TO_SPAWN_TRUCK()
	PLAYER_INDEX thePlayer
	BOOL bSafeToSpawn = TRUE
	
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		thePlayer = INT_TO_PLAYERINDEX(i)
		
		IF thePlayer != INVALID_PLAYER_INDEX()
		AND NETWORK_IS_PLAYER_A_PARTICIPANT(thePlayer)
			IF IS_NET_PLAYER_OK(thePlayer)
				IF IS_ENTITY_IN_ANGLED_AREA(GET_PLAYER_PED(thePlayer), <<853.945435,-3244.955811,-199.644547>>, <<832.867493,-3233.418701,-95.699135>>, 7.062500)
				OR GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) = PERFORMING_TASK
				OR GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) = WAITING_TO_START_TASK
					CDEBUG3LN(DEBUG_PROPERTY, "IS_SAFE_TO_SPAWN_TRUCK - player ", NATIVE_TO_INT(thePlayer), " in area")
					bSafeToSpawn = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	i = 0
	
	REPEAT NUMBER_OF_BUNKER_VEHICLES i
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niBunkerVehicles[i])
		AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niBunkerVehicles[i])
		AND IS_ENTITY_IN_ANGLED_AREA(NET_TO_VEH(serverBD.niBunkerVehicles[i]), <<853.945435,-3244.955811,-199.644547>>, <<832.867493,-3233.418701,-95.699135>>, 7.062500)
			CDEBUG3LN(DEBUG_PROPERTY, "IS_SAFE_TO_SPAWN_TRUCK - vehicle ", i, " in area")
			bSafeToSpawn = FALSE
		ENDIF
	ENDREPEAT
	
	IF thisBunker.pOwner != INVALID_PLAYER_INDEX()
		IF DOES_ENTITY_EXIST(MPGlobals.RemotePV[NATIVE_TO_INT(thisBunker.pOwner)])
		AND IS_ENTITY_IN_ANGLED_AREA(MPGlobals.RemotePV[NATIVE_TO_INT(thisBunker.pOwner)], <<853.945435,-3244.955811,-199.644547>>, <<832.867493,-3233.418701,-95.699135>>, 7.062500)
			SET_BIT(serverBD.iBS, BS_BUNKER_SERVER_DATA_CLEAR_PV_SPACE)
			CDEBUG3LN(DEBUG_PROPERTY, "IS_SAFE_TO_SPAWN_TRUCK - owner PV in area")
			bSafeToSpawn = FALSE
		ENDIF
		
		IF DOES_ENTITY_EXIST(MPGlobals.RemotePegV[NATIVE_TO_INT(thisBunker.pOwner)])
		AND IS_ENTITY_IN_ANGLED_AREA(MPGlobals.RemotePegV[NATIVE_TO_INT(thisBunker.pOwner)], <<853.945435,-3244.955811,-199.644547>>, <<832.867493,-3233.418701,-95.699135>>, 7.062500)
			SET_BIT(serverBD.iBS, BS_BUNKER_SERVER_DATA_CLEAR_PV_SPACE)
			CDEBUG3LN(DEBUG_PROPERTY, "IS_SAFE_TO_SPAWN_TRUCK - owner Pegasus in area")
			bSafeToSpawn = FALSE
		ENDIF
	ENDIF
	
	RETURN bSafeToSpawn
ENDFUNC

PROC DELETE_CUSTOM_CAR(NETWORK_INDEX theVehNetID,INT iSaveSlot, INT iCallID)
	IF iCallID != 0
	
	ENDIF
	IF NOT HAS_NET_TIMER_STARTED(thisBunker.failSafeClearVehicleDelay)
		START_NET_TIMER(thisBunker.failSafeClearVehicleDelay,TRUE)
	ENDIF
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(theVehNetID)
		IF IS_NET_VEHICLE_DRIVEABLE(theVehNetID)
			IF GET_VEHICLE_DOOR_LOCK_STATUS(NET_TO_VEH(theVehNetID)) != VEHICLELOCK_LOCKED
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(theVehNetID)
					SET_VEHICLE_DOORS_LOCKED(NET_TO_VEH(theVehNetID),VEHICLELOCK_LOCKED)
					PRINTLN("DELETE_CUSTOM_CAR: Locking doors for update on vehicle ",iCallID)
				ELSE
					NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(theVehNetID)
				ENDIF
			ENDIF
			IF IS_VEHICLE_EMPTY(NET_TO_VEH(theVehNetID),TRUE, TRUE)
			OR HAS_NET_TIMER_EXPIRED(thisBunker.failSafeClearVehicleDelay,3000,TRUE)
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(theVehNetID)	
					DELETE_NET_ID(theVehNetID)
					serverBD.AATrailerSaveSlot = -1
					serverBD.iAATrailerVehCreationBS = 0
					CLEAR_BIT(thisBunker.iBS,BS_BUNKER_PERS_VEH_CHECKED_AVAILABLILITY)
					PRINTLN("DELETE_CUSTOM_CAR: deleted vehicle cleared update flag saveSlot# ", iSaveSlot)
					IF iSaveSlot > -1
						CLEAR_BIT(g_MpSavedVehicles[iSaveSlot].iVehicleBS,MP_SAVED_VEHICLE_UPDATE_VEHICLE)
					ENDIF
				ELSE
					PRINTLN("DELETE_CUSTOM_CAR: can't delete not empty! saveSlot# ", iSaveSlot)
					NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(theVehNetID)
				ENDIF
			ELSE
				IF IS_NET_PLAYER_OK(PLAYER_ID())
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),NET_TO_VEH(theVehNetID))
						TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID(),0,ECF_WARP_IF_DOOR_IS_BLOCKED)
					ENDIF
				ENDIF
				PRINTLN("DELETE_CUSTOM_CAR: can't delete not empty! saveSlot# ", iSaveSlot)
			ENDIF
		ELSE
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(theVehNetID)	
				DELETE_NET_ID(theVehNetID)
				serverBD.AATrailerSaveSlot = -1
				serverBD.iAATrailerVehCreationBS = 0
				CLEAR_BIT(thisBunker.iBS,BS_BUNKER_PERS_VEH_CHECKED_AVAILABLILITY)
				PRINTLN("DELETE_CUSTOM_CAR: deleted (NOT DRIVEABLE) vehicle cleared update flag saveSlot# ", iSaveSlot)
				IF iSaveSlot > -1
					CLEAR_BIT(g_MpSavedVehicles[iSaveSlot].iVehicleBS,MP_SAVED_VEHICLE_UPDATE_VEHICLE)
				ENDIF
			ELSE
				NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(theVehNetID)
			ENDIF
		ENDIF
	ELSE
		IF iSaveSlot > -1
			CLEAR_BIT(g_MpSavedVehicles[iSaveSlot].iVehicleBS,MP_SAVED_VEHICLE_UPDATE_VEHICLE)
		ENDIF
		CLEAR_BIT(thisBunker.iBS,BS_BUNKER_PERS_VEH_CHECKED_AVAILABLILITY)
		PRINTLN("DELETE_CUSTOM_CAR: vehicle deleted saveSlot# ", iSaveSlot)
	ENDIF
ENDPROC

FUNC BOOL CREATE_MODDED_CAR_FOR_BUNKER(VEHICLE_SETUP_STRUCT vehicleSetup, INT iSaveSlot,INT iDisplaySlot, BOOL bHasEmblem)
	//MP_PROP_OFFSET_STRUCT offset
	VEHICLE_INDEX tempVehicle
	VECTOR vehCreationLocation
	FLOAT fHeading
//	IF IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, PLAYER_BD_BS_WARPING_OUT_OF_GARAGE)
//		PRINTLN("CREATE_MODDED_CAR_FOR_BUNKER: bypassing as player is in process of leaving")
//		RETURN FALSE
//	ENDIF

	IF serverBD.iAATrailerVehCreationBS != 0
	AND NOT IS_NET_VEHICLE_DRIVEABLE(serverBD.niAATrailer)
		PRINTLN("CREATE_MODDED_CAR_FOR_BUNKER: not trying to create BS still has a value, may not have synced yet")
		RETURN FALSE
	ENDIF
	
	IF NOT IS_NET_VEHICLE_DRIVEABLE(serverBD.niAATrailer)
		IF IS_MODEL_IN_CDIMAGE(vehicleSetup.eModel)
			IF REQUEST_LOAD_MODEL(vehicleSetup.eModel)
//				IF CAN_RESERVE_NETWORK_VEHICLES_FOR_THIS_SCRIPT(GET_NUM_RESERVED_MISSION_VEHICLES()+ 1,false,true)
//					SERVER_CHECK_ANY_PLAYERS_ARE_IN_THE_WAY(serverBD,vehCreationLocation)
//					IF NOT IS_IT_SAFE_TO_CREATE_A_VEHICLE(serverBD)
//						PRINTLN("CREATE_MODDED_CAR_FOR_BUNKER: waiting for players to be out of the way: Vector = ",vehCreationLocation)
//						RETURN FALSE
//					ENDIF
//				ENDIF
					
				//IF IS_BIT_SET(serverBD.iVehicleResBS,i)
					IF CAN_REGISTER_MISSION_ENTITIES(0,1,0,0)
						IF NETWORK_IS_IN_MP_CUTSCENE() 
							SET_NETWORK_CUTSCENE_ENTITIES(TRUE)	
							PRINTLN("CREATE_MODDED_CAR_FOR_BUNKER, in cutscene therefore setting SET_NETWORK_CUTSCENE_ENTITIES")
						ENDIF
						vehCreationLocation = <<849.8635, -3234.3528, -98.8>>
						fHeading = -120.0
						//vehCreationLocation = TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS(vehCreationLocation, thisInterior.details)
						//fHeading = TRANSFORM_SIMPLE_INTERIOR_HEADING_TO_WORLD_HEADING(fHeading, thisInterior.details)
						tempVehicle = CREATE_VEHICLE(vehicleSetup.eModel,vehCreationLocation,fHeading,
												TRUE,TRUE)	
						SET_ENTITY_COORDS_NO_OFFSET(tempVehicle,vehCreationLocation)
						SET_ENTITY_HEADING(tempVehicle,fHeading)
						serverBD.niAATrailer = VEH_TO_NET(tempVehicle)
						g_personalAACarModVeh = tempVehicle
						//SET_NETWORK_ID_EXISTS_ON_ALL_MACHINES(serverBD.niAATrailer, TRUE)
						//NETWORK_SET_VEHICLE_GARAGE_INDEX(playerBD[NATIVE_TO_INT(PLAYER_ID())].customVehicleNetIDs[i],iOwnerID)
						//FREEZE_ENTITY_POSITION(playerBD[NATIVE_TO_INT(PLAYER_ID())].customVehicleNetIDs[i],TRUE)
						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(tempVehicle,TRUE)	
						#IF IS_DEBUG_BUILD
						PRINTLN("CREATE_MODDED_CAR_FOR_BUNKER: HOST Creating vehicle save slot #",iSaveSlot,"at ", vehCreationLocation, "  ")
						#ENDIF
						serverBD.iAATrailerVehCreationBS = 0
						IF bHasEmblem
							MP_SAVE_VEHICLE_APPLY_SAVED_DETAILS_TO_VEHICLE_INC_EMBLEM(tempVehicle,iSaveSlot)//,bLoadingEmblem[i])
						ELSE
							MP_SAVE_VEHICLE_APPLY_SAVED_DETAILS_TO_VEHICLE(tempVehicle,iSaveSlot)
							SET_BIT(serverBD.iAATrailerVehCreationBS,MP_PROP_CREATE_CARS_BS_APPLIED_DETAILS)
						ENDIF
						serverbD.AATrailerSaveSlot = iSaveSlot

						SET_VEHICLE_ON_GROUND_PROPERLY(tempVehicle,0)
						//PRINTLN("CREATE_MODDED_CAR_FOR_BUNKER: after set on ground coords are: ", GET_ENTITY_COORDS(tempVehicle))
						vehCreationLocation = GET_ENTITY_COORDS(tempVehicle)
						PRINTLN("CREATE_MODDED_CAR_FOR_BUNKER: vehicle after set on ground coords are: ", vehCreationLocation)
						//PRINTLN("CREATE_MODDED_CAR_FOR_BUNKER: after set on ground coords are: ", GET_ENTITY_COORDS(tempVehicle))

						//ENDIF
						//SET_VEHICLE_SETUP(tempVehicle, vehicleSetup, FALSE, TRUE)
						//MPGlobalsAmbience.GarageVehicleID[0] = tempVehicle
						NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(tempVehicle,TRUE)
						SET_VEHICLE_NOT_STEALABLE_AMBIENTLY(tempVehicle,FALSE)
						SET_VEHICLE_FULLBEAM(tempVehicle, FALSE)
						SET_VEHICLE_LIGHTS(tempVehicle,FORCE_VEHICLE_LIGHTS_OFF)	
						SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(tempVehicle, TRUE)  
						SET_TRAILER_ATTACHMENT_ENABLED(tempVehicle,FALSE)
						//PRINTLN("CREATE_MODDED_CAR_FOR_BUNKER: setting radio station in car ",i," to be: ",g_MpSavedVehicles[iSaveSlot].tlRadioStationName)
						IF IS_NET_PLAYER_OK(thisBunker.pOwner,FALSE,FALSE)
//			                	SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(tempVehicle, thisBunker.pOwner, FALSE)
						ENDIF
//						IF thisBunker.pOwner != INVALID_PLAYER_INDEX()
//							IF IS_NET_PLAYER_OK(thisBunker.pOwner,FALSE,FALSE)
//								SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(tempVehicle, thisBunker.pOwner, FALSE)
//								PRINTLN("Setting vehicle doors unlocked for ", GET_PLAYER_NAME(thisBunker.pOwner))
//							ENDIF
//						ENDIF
//							ENDIF
						
						SET_VEHICLE_FIXED(tempVehicle) 
				        SET_ENTITY_HEALTH(tempVehicle, 1000) 
				        SET_VEHICLE_ENGINE_HEALTH(tempVehicle, 1000) 
				        SET_VEHICLE_PETROL_TANK_HEALTH(tempVehicle, 1000) 
						SET_VEHICLE_DIRT_LEVEL(tempVehicle,0.0)
		                SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(tempVehicle,TRUE)
						SET_VEHICLE_RESPECTS_LOCKS_WHEN_HAS_DRIVER(tempVehicle,TRUE)
						CLEAR_BIT(serverBD.iAATrailerVehCreationBS,MP_PROP_CREATE_CARS_BS_MODS_LOADED)
						//bLoadingEmblem[i] = FALSE
						SET_ENTITY_CAN_BE_DAMAGED(tempVehicle,FALSE)
						SET_ENTITY_VISIBLE(tempVehicle,FALSE) //hidden for test
						
						
						// set this vehicle as a player_vehicle so no on else can steal it
						IF DECOR_IS_REGISTERED_AS_TYPE("Player_Vehicle", DECOR_TYPE_INT)
							IF NOT DECOR_EXIST_ON(tempVehicle, "Player_Vehicle")
								DECOR_SET_INT(tempVehicle, "Player_Vehicle", NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID()))
							ENDIF
						ENDIF
						
						SET_NETWORK_ID_ALWAYS_EXISTS_FOR_PLAYER(serverBD.niAATrailer,PLAYER_ID(),FALSE)
						PRINTLN("CREATE_MODDED_CAR_FOR_BUNKER: created vehicle ID ")
						IF NETWORK_IS_IN_MP_CUTSCENE() 
							SET_NETWORK_CUTSCENE_ENTITIES(FALSE)	
						ENDIF
						//RESET_NET_TIMER(st_ServerWalkOutTimeout)
					ELSE
						PRINTLN("CREATE_MODDED_CAR_FOR_BUNKER: failing to reserve a mission entity ")	
					ENDIF
				//ELSE
				//	PRINTLN("CREATE_MODDED_CAR_FOR_BUNKER: Waiting for CAN_RESERVE_NETWORK_ENTITIES_FOR_THIS_SCRIPT(",serverBD.iVehicleCreated+ 1 ,")") 
				//ENDIF
			ELSE
				PRINTLN("CREATE_MODDED_CAR_FOR_BUNKER: Waiting for model to load")
				RETURN FALSE
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(serverBD.iAATrailerVehCreationBS,MP_PROP_CREATE_CARS_BS_MODS_LOADED)
			// Now set the specifics
			// Bug #437236 - Set all license plates for all profiles in SP and MP to have the same text when reading from the cloud data.
			IF NOT IS_BIT_SET(serverBD.iAATrailerVehCreationBS,MP_PROP_CREATE_CARS_BS_MODS_LOADED)
				tempVehicle = NET_TO_VEH(serverBD.niAATrailer)
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niAATrailer)
					IF NOT IS_BIT_SET(serverBD.iAATrailerVehCreationBS,MP_PROP_CREATE_CARS_BS_APPLIED_DETAILS)
						//IF MP_SAVE_VEHICLE_APPLY_SAVED_DETAILS_TO_VEHICLE_INC_EMBLEM(tempVehicle,i,)
						IF bHasEmblem
							IF MP_SAVE_VEHICLE_APPLY_SAVED_DETAILS_TO_VEHICLE_INC_EMBLEM(tempVehicle,iSaveSlot)//,bLoadingEmblem[i])
								SET_BIT(serverBD.iAATrailerVehCreationBS,MP_PROP_CREATE_CARS_BS_APPLIED_DETAILS)
								PRINTLN("CREATE_MODDED_CAR_FOR_BUNKER: added all mods for car in slot # ", iSaveSlot)
							#IF IS_DEBUG_BUILD
							ELSE
								PRINTLN("CREATE_MODDED_CAR_FOR_BUNKER: waiting for applying details to car")
							#ENDIF
							ENDIF
						ELSE
							MP_SAVE_VEHICLE_APPLY_SAVED_DETAILS_TO_VEHICLE(tempVehicle,iSaveSlot)
							SET_BIT(serverBD.iAATrailerVehCreationBS,MP_PROP_CREATE_CARS_BS_APPLIED_DETAILS)
							PRINTLN("CREATE_MODDED_CAR_FOR_BUNKER: added all mods for car in slot # ", iSaveSlot)
						ENDIF
					ELSE
						IF HAVE_VEHICLE_MODS_STREAMED_IN(tempVehicle)
							IF InteriorPropStruct.iGarageSize > PROP_GARAGE_SIZE_2
							AND MP_SAVE_VEHICLE_IS_DISPLAY_SLOT_A_CYCLE(iDisplaySlot)
							
							ELSE
								IF NOT SET_VEHICLE_ON_GROUND_PROPERLY(tempVehicle,0)
									PRINTLN("CREATE_MODDED_CAR_FOR_BUNKER: waiting for SET_VEHICLE_ON_GROUND_PROPERLY")
									//RETURN FALSE
								ELSE
									PRINTLN("CREATE_MODDED_CAR_FOR_BUNKER: SET_VEHICLE_ON_GROUND_PROPERLY done")
									vehCreationLocation = GET_ENTITY_COORDS(tempVehicle)
									PRINTLN("CREATE_MODDED_CAR_FOR_BUNKER: vehicle after set on ground coords are: ", vehCreationLocation )
								ENDIF
							ENDIF
							SET_BIT(serverBD.iAATrailerVehCreationBS,MP_PROP_CREATE_CARS_BS_MODS_LOADED)
							SET_ENTITY_VISIBLE(tempVehicle,TRUE) //hidden for test

							SET_HYDRAULIC_SUSPENSION_RAISE_FACTOR(tempVehicle, SC_WHEEL_CAR_FRONT_LEFT, 0.0)
							SET_HYDRAULIC_SUSPENSION_RAISE_FACTOR(tempVehicle, SC_WHEEL_CAR_FRONT_RIGHT, 0.0)
							SET_HYDRAULIC_SUSPENSION_RAISE_FACTOR(tempVehicle, SC_WHEEL_CAR_MID_LEFT, 0.0)
							SET_HYDRAULIC_SUSPENSION_RAISE_FACTOR(tempVehicle, SC_WHEEL_CAR_MID_RIGHT, 0.0)
							SET_HYDRAULIC_SUSPENSION_RAISE_FACTOR(tempVehicle, SC_WHEEL_CAR_REAR_LEFT, 0.0)
							SET_HYDRAULIC_SUSPENSION_RAISE_FACTOR(tempVehicle, SC_WHEEL_CAR_REAR_RIGHT, 0.0)
							SET_CAN_USE_HYDRAULICS(tempVehicle, FALSE)
							PRINTLN("CREATE_MODDED_CAR_FOR_BUNKER: setting vehicle can't use hydraulics")
							FREEZE_ENTITY_POSITION(tempVehicle,TRUE)
//							IF InteriorPropStruct.iGarageSize > PROP_GARAGE_SIZE_2
//							AND MP_SAVE_VEHICLE_IS_DISPLAY_SLOT_A_CYCLE(iDisplaySlot)
//								SET_VEHICLE_DOORS_LOCKED(tempVehicle,VEHICLELOCK_CANNOT_ENTER)
//							ELSE
								SET_VEHICLE_DOORS_LOCKED(tempVehicle,VEHICLELOCK_UNLOCKED)
							//ENDIF

							SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(tempVehicle, TRUE)  
//							IF thisBunker.pOwner != INVALID_PLAYER_INDEX()
//								IF IS_NET_PLAYER_OK(thisBunker.pOwner,FALSE,FALSE)
//									SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(tempVehicle, thisBunker.pOwner, FALSE)
//									PRINTLN("Setting vehicle doors unlocked for ", GET_PLAYER_NAME(thisBunker.pOwner))
//								ENDIF
//							ENDIF
			                SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(tempVehicle,TRUE)
							SET_VEHICLE_RESPECTS_LOCKS_WHEN_HAS_DRIVER(tempVehicle,TRUE)
							PRINTLN("AM_MP_BUNKER: adding a new vehicle to garage")
							//iReplacedVehicle = -1
							PRINTLN("CREATE_MODDED_CAR_FOR_BUNKER: setting server net ID for vehicle")
						#IF IS_DEBUG_BUILD
						ELSE
							PRINTLN("CREATE_MODDED_CAR_FOR_BUNKER: waiting for HAVE_VEHICLE_MODS_STREAMED_IN to car")
						#ENDIF
						ENDIF
					ENDIF
				ELSE
					NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.niAATrailer)
					PRINTLN("CREATE_MODDED_CAR_FOR_BUNKER: waiting for ownership for mods")
				ENDIF
			ELSE
				PRINTLN("CREATE_MODDED_CAR_FOR_BUNKER: MP_PROP_CREATE_CARS_BS_MODS_LOADED for vehicle ")
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_VEHICLE_AVAILABLE_FOR_GAME_IN_BUNKER(INT iSaveSlot)
	IF IS_BIT_SET(thisBunker.iBS,BS_BUNKER_PERS_VEH_CHECKED_AVAILABLILITY)
		RETURN IS_BIT_SET(thisBunker.iBS,BS_BUNKER_PERS_VEH_AVAILABLE)
	ELIF IS_BIT_SET(thisBunker.iBS,BS_BUNKER_PERS_VEH_AVAILABLE)
		RETURN TRUE
	ELSE
		IF IS_VEHICLE_AVAILABLE_FOR_GAME(g_MpSavedVehicles[iSaveSlot].vehicleSetupMP.VehicleSetup.eModel, TRUE) 
			SET_BIT(thisBunker.iBS,BS_BUNKER_PERS_VEH_AVAILABLE)
			PRINTLN("IS_VEHICLE_AVAILABLE_FOR_GAME_IN_BUNKER: setting Available save slot: ",iSaveSlot)
			RETURN TRUE
		ELSE
			PRINTLN("IS_VEHICLE_AVAILABLE_FOR_GAME_IN_BUNKER: setting UN-Available save slot: ",iSaveSlot)
		ENDIF
	ENDIF
	SET_BIT(thisBunker.iBS,BS_BUNKER_PERS_VEH_CHECKED_AVAILABLILITY)
	RETURN FALSE
ENDFUNC 

FUNC BOOL MAINTAIN_AA_TRAILER_CREATION_BUNKER()
	BOOL bDeleteVeh = FALSE
	//VEHICLE_INDEX playerVeh
	INT iDisplaySlot, iSaveSlot
	//INT iInstanceID
	BOOL bSkip
	BOOL bHasEmblem

	IF thisBunker.pOwner  != PLAYER_ID()
		IF NOT serverBD.bOwnerCompletedAATrailerCreation
		AND (NETWORK_IS_PLAYER_A_PARTICIPANT(thisBunker.pOwner)
		OR IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBS, BS_SIMPLE_INTERIOR_GLOBAL_PLAYER_BD_DRIVER_ENTERING))
		
		//AND NETWORK_IS_PLAYER_A_PARTICIPANT(thisBunker.pOwner)
		
			PRINTLN("MAINTAIN_AA_TRAILER_CREATION_BUNKER: waiting for owner to be host/create personal vehicle if nessecary")
			RETURN FALSE
		ENDIF
	ELSE
		IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
			IF NOT HAS_NET_TIMER_STARTED(st_HostRequestTimer)
				IF NOT IS_BIT_SET(serverBD.iBS, BS_BUNKER_SERVER_DATA_ASSIGN_TRUCK_TO_INTERIOR)	// wait till clone MOC is created!
					PRINTLN("MAINTAIN_AA_TRAILER_CREATION_BUNKER: local player (OWNER) requesting to be host of script ")
					NETWORK_REQUEST_TO_BE_HOST_OF_THIS_SCRIPT()
					START_NET_TIMER(st_HostRequestTimer,TRUE)
				ENDIF	
			ELSE
				IF HAS_NET_TIMER_EXPIRED(st_HostRequestTimer,1000,TRUE)
					RESET_NET_TIMER(st_HostRequestTimer)
				ENDIF
			ENDIF
			RETURN FALSE
		ENDIF
		
		IF g_bPlayerLeavingCurrentInteriorInVeh
			PRINTLN("MAINTAIN_AA_TRAILER_CREATION_BUNKER: g_bPlayerLeavingCurrentInteriorInVeh = TRUE exiting creation")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niAATrailer)
		MPGlobalsAmbience.GarageVehicleID[0] = NET_TO_VEH(serverBD.niAATrailer)
	ENDIF

	//i = 0
	IF thisBunker.pOwner  = PLAYER_ID()	
		IF NETWORK_IS_IN_MP_CUTSCENE()
			IF NOT IS_BIT_SET(thisBunker.iBS,BS_BUNKER_SET_CUTSCENE_ENTITIES_TO_BE_NETWORKED)
				SET_NETWORK_CUTSCENE_ENTITIES(TRUE)
				SET_BIT(thisBunker.iBS,BS_BUNKER_SET_CUTSCENE_ENTITIES_TO_BE_NETWORKED)
				PRINTLN("AM_MP_BUNKER: Setting entities created to be networked in CS")
			ENDIF
		ELSE
			CLEAR_BIT(thisBunker.iBS,BS_BUNKER_SET_CUTSCENE_ENTITIES_TO_BE_NETWORKED)
		ENDIF
	ENDIF

	iSaveSlot = -1
	iDisplaySlot = DISPLAY_SLOT_AA_TRAILER
	//VEHICLE_INDEX theVeh
	IF thisBunker.pOwner = PLAYER_ID()
		//IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(),TRUE)
		MPSV_GET_SAVE_SLOT_FROM_DISPLAY_SLOT(iDisplaySlot,iSaveSlot,TRUE) //CDM
		IF iSaveSlot >= 0
			IF IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS,MP_SAVED_VEHICLE_UPDATE_VEHICLE)
				IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niAATrailer)
					DELETE_CUSTOM_CAR(serverBD.niAATrailer,iSaveSlot,5)
				ELSE
					CLEAR_BIT(g_MpSavedVehicles[iSaveSlot].iVehicleBS,MP_SAVED_VEHICLE_UPDATE_VEHICLE)
				ENDIF
				IF DOES_BLIP_EXIST(bAATrailerBlip)
					PRINTLN("MAINTAIN_AA_TRAILER_CREATION_BUNKER remove AA Trailer blip")
					REMOVE_BLIP(bAATrailerBlip)
				ENDIF
			ENDIF
		ENDIF

		MPSV_GET_SAVE_SLOT_FROM_DISPLAY_SLOT(iDisplaySlot,iSaveSlot,TRUE) //CDM
		IF iSaveSlot < 0
		//OR GET_MP_INT_CHARACTER_STAT(MP_STAT_INV_TRUCK_MODEL_0) = 0
			bSkip = TRUE
		ENDIF
		IF NOT bSkip
			//IF iLocalStage <  STAGE_STARTING_EXIT
				IF iSaveSlot >= 0
				AND IS_MODEL_A_PERSONAL_TRAILER(g_MpSavedVehicles[iSaveSlot].vehicleSetupMP.VehicleSetup.eModel)
				AND IS_VEHICLE_AVAILABLE_FOR_GAME_IN_BUNKER(iSaveSlot)
					IF NOT IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS,MP_SAVED_VEHICLE_OUT_GARAGE)
					AND NOT IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS,MP_SAVED_VEHICLE_DESTROYED) 
					AND NOT IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS,MP_SAVED_VEHICLE_IMPOUNDED)
					AND MPGlobalsAmbience.iVDPersonalVehicleSlot != iSaveSlot
						IF IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS,MP_SAVED_VEHICLE_UPDATE_VEHICLE)
							PRINTLN("MAINTAIN_AA_TRAILER_CREATION_BUNKER MP_SAVED_VEHICLE_UPDATE_VEHICLE set on vehicle saveSlot# ", iSaveSlot)
							IF NOT HAS_NET_TIMER_STARTED(thisBunker.failSafeClearVehicleDelay)
								START_NET_TIMER(thisBunker.failSafeClearVehicleDelay,TRUE)
							ENDIF
							DELETE_CUSTOM_CAR(serverBD.niAATrailer,iSaveSlot,1)
							IF DOES_BLIP_EXIST(bAATrailerBlip)
								PRINTLN("MAINTAIN_AA_TRAILER_CREATION_BUNKER remove AA Trailer blip")
								REMOVE_BLIP(bAATrailerBlip)
							ENDIF
						ELIF NOT IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS,MP_SAVED_VEHICLE_JUST_PURCHASED)
							IF IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS,MP_SAVED_VEHICLE_HAS_CREW_EMBLEM)
								bHasEmblem = TRUE
							ELSE
								bHasEmblem = FALSE
							ENDIF
								
							// check this garage vehicle isn't the result of a hack.	
							RUN_COMMERCIAL_VEHICLE_IN_SLOT_CHECK(iSaveSlot)
							IF NOT IS_MODEL_VALID_FOR_PERSONAL_VEHICLE(g_MpSavedVehicles[iSaveSlot].vehicleSetupMP.VehicleSetup.eModel)
								CLEAR_MP_SAVED_VEHICLE_SLOT(iSaveSlot)
							ELSE
								IF NOT CREATE_MODDED_CAR_FOR_BUNKER(g_MpSavedVehicles[iSaveSlot].vehicleSetupMP.VehicleSetup,iSaveSlot, iDisplaySlot,bHasEmblem)
									RETURN FALSE
								ENDIF
							ENDIF
							RESET_NET_TIMER(thisBunker.failSafeClearVehicleDelay)
								
						ELSE
							IF IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS,MP_SAVED_VEHICLE_JUST_PURCHASED)
								PRINTLN("MAINTAIN_AA_TRAILER_CREATION_BUNKER  waiting for MP_SAVED_VEHICLE_JUST_PURCHASED to be cleared on vehicle in save slot # ",iSaveSlot)
							ENDIF
							bDeleteVeh = TRUE
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
						IF IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS,MP_SAVED_VEHICLE_OUT_GARAGE)
							PRINTLN("MAINTAIN_AA_TRAILER_CREATION_BUNKER  Checking iSaveSlot: ",iSaveSlot, " MP_SAVED_VEHICLE_OUT_GARAGE")
						ENDIF
						IF IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS,MP_SAVED_VEHICLE_DESTROYED) 
							PRINTLN("MAINTAIN_AA_TRAILER_CREATION_BUNKER  Checking iSaveSlot: ",iSaveSlot, " MP_SAVED_VEHICLE_DESTROYED")
						ENDIF
						IF IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS,MP_SAVED_VEHICLE_IMPOUNDED)
							PRINTLN("MAINTAIN_AA_TRAILER_CREATION_BUNKER  Checking iSaveSlot: ",iSaveSlot, " MP_SAVED_VEHICLE_IMPOUNDED")
						ENDIF
						IF MPGlobalsAmbience.iVDPersonalVehicleSlot = iSaveSlot
							PRINTLN("MAINTAIN_AA_TRAILER_CREATION_BUNKER  Checking iSaveSlot: ",iSaveSlot, " matches iVDPersonalVehicleSlot")
						ENDIF
						#ENDIF
						bDeleteVeh = TRUE
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
					IF iSaveSlot >= 0
//						IF g_MpSavedVehicles[iSaveSlot].vehicleSetupMP.VehicleSetup.eModel = DUMMY_MODEL_FOR_SCRIPT
//							PRINTLN("MAINTAIN_AA_TRAILER_CREATION_BUNKER  Checking index: iSaveSlot: ",iSaveSlot, " DUMMY_MODEL_FOR_SCRIPT")
//						ENDIF
						IF NOT IS_VEHICLE_AVAILABLE_FOR_GAME_IN_BUNKER(iSaveSlot)
							PRINTLN("MAINTAIN_AA_TRAILER_CREATION_BUNKER  Checking index: iSaveSlot: ",iSaveSlot, " NOT IS_VEHICLE_AVAILABLE_FOR_GAME")
						ENDIF
//					ELSE
//						PRINTLN("MAINTAIN_AA_TRAILER_CREATION_BUNKER  Checking index: iSaveSlot: ",iSaveSlot, " NO SAVE SLOT")
					ENDIF
					#ENDIF
					bDeleteVeh = TRUE
				ENDIF
			//ENDIF
			IF bDeleteVeh
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niAATrailer)
					DELETE_CUSTOM_CAR(serverBD.niAATrailer,iSaveSlot,2)
				ELSE
					RESET_NET_TIMER(thisBunker.failSafeClearVehicleDelay)
				ENDIF
				IF DOES_BLIP_EXIST(bAATrailerBlip)
					PRINTLN("MAINTAIN_AA_TRAILER_CREATION_BUNKER remove AA Trailer blip")
					REMOVE_BLIP(bAATrailerBlip)
				ENDIF
			ENDIF
		ELSE
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niAATrailer)
				DELETE_CUSTOM_CAR(serverBD.niAATrailer,iSaveSlot,3)
			ELSE
				RESET_NET_TIMER(thisBunker.failSafeClearVehicleDelay)
			ENDIF
			IF DOES_BLIP_EXIST(bAATrailerBlip)
				PRINTLN("MAINTAIN_AA_TRAILER_CREATION_BUNKER remove AA Trailer blip")
				REMOVE_BLIP(bAATrailerBlip)
			ENDIF
			IF iSaveSlot >= 0
				IF IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS,MP_SAVED_VEHICLE_UPDATE_VEHICLE)	
					PRINTLN("MAINTAIN_AA_TRAILER_CREATION_BUNKER  skipping creation so clearing MP_SAVED_VEHICLE_UPDATE_VEHICLE on save slot: ",iSaveSlot)
					CLEAR_BIT(g_MpSavedVehicles[iSaveSlot].iVehicleBS,MP_SAVED_VEHICLE_UPDATE_VEHICLE)			
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF thisBunker.pOwner != INVALID_PLAYER_INDEX()
			IF IS_NET_PLAYER_OK(thisBunker.pOwner,FALSE,TRUE)
				IF NOT NETWORK_IS_PLAYER_A_PARTICIPANT(thisBunker.pOwner)
					IF GlobalplayerBD_FM[NATIVE_TO_INT(thisBunker.pOwner)].iCurrentsActivePersonalVehicle = serverbD.AATrailerSaveSlot
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niAATrailer)
							DELETE_CUSTOM_CAR(serverBD.niAATrailer,iSaveSlot,4)
						ELSE
							RESET_NET_TIMER(thisBunker.failSafeClearVehicleDelay)
						ENDIF
						IF DOES_BLIP_EXIST(bAATrailerBlip)
							PRINTLN("MAINTAIN_AA_TRAILER_CREATION_BUNKER remove AA Trailer blip")
							REMOVE_BLIP(bAATrailerBlip)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niAATrailer)
		IF NOT HAS_NET_TIMER_STARTED(vehSyncDelayTimer) //in case there is an issue with syncing
			START_NET_TIMER(vehSyncDelayTimer,TRUE)
		ELIF HAS_NET_TIMER_EXPIRED(vehSyncDelayTimer,5000,TRUE)
			IF IS_BIT_SET(serverBD.iAATrailerVehCreationBS,MP_PROP_CREATE_CARS_BS_MODS_LOADED)
				serverBD.AATrailerSaveSlot = -1
				serverBD.iAATrailerVehCreationBS = 0	
				CLEAR_BIT(serverBD.iAATrailerVehCreationBS,MP_PROP_CREATE_CARS_BS_MODS_LOADED)
				PRINTLN("MAINTAIN_AA_TRAILER_CREATION_BUNKER  a vehicle is now not driveable")
			ENDIF
			RESET_NET_TIMER(vehSyncDelayTimer)
			RESET_NET_TIMER(thisBunker.failSafeClearVehicleDelay)
		ENDIF
	ELSE
		RESET_NET_TIMER(vehSyncDelayTimer)
	ENDIF
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()	
	AND thisBunker.pOwner = PLAYER_ID()
	AND NOT serverBD.bOwnerCompletedAATrailerCreation
		serverBD.bOwnerCompletedAATrailerCreation = TRUE
		PRINTLN("MAINTAIN_AA_TRAILER_CREATION_BUNKER server finished initial creation")
	ENDIF
	IF serverBD.bOwnerCompletedAATrailerCreation
	AND NOT DOES_BLIP_EXIST(bAATrailerBlip)
	AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niAATrailer)
		bAATrailerBlip = ADD_BLIP_FOR_ENTITY(NET_TO_VEH(serverBD.niAATrailer))
		SET_BLIP_SPRITE(bAATrailerBlip, GET_BLIP_SPRITE_FOR_SPECIAL_VEHICLE_MODEL(GET_ENTITY_MODEL(NET_TO_VEH(serverBD.niAATrailer))))
		SET_CUSTOM_BLIP_NAME_FROM_MODEL(bAATrailerBlip, GET_ENTITY_MODEL(NET_TO_VEH(serverBD.niAATrailer)))
		SET_BLIP_DISPLAY(bAATrailerBlip, DISPLAY_BOTH)
		
		PRINTLN("MAINTAIN_AA_TRAILER_CREATION_BUNKER add AA Trailer blip ", GET_BLIP_SPRITE_DEBUG_STRING(GET_BLIP_SPRITE(bAATrailerBlip)))
	ENDIF
	IF serverBD.bOwnerCompletedAATrailerCreation
	AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niAATrailer)
	AND g_personalAACarModVeh != NET_TO_VEH(serverBD.niAATrailer)
		g_personalAACarModVeh = NET_TO_VEH(serverBD.niAATrailer)
	ENDIF
	
	RETURN TRUE
ENDFUNC




PROC MAINTAIN_BUNKER_LOADING_STATE()
	INTERIOR_INSTANCE_INDEX interiorIndex 
	VECTOR vInteriorPosition
	FLOAT fInteriorHeading
	TEXT_LABEL_63 strInteriorType
	IF NOT IS_BIT_SET(thisBunker.iBS, BS_BUNKER_DATA_INTERIOR_REFRESHED_ON_INIT)
		IF NOT thisBunker.bScriptWasRelaunched
			
			
			GET_SIMPLE_INTERIOR_INTERIOR_TYPE_AND_POSITION(thisBunker.eSimpleInteriorID, strInteriorType, vInteriorPosition, fInteriorHeading)
			interiorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(vInteriorPosition, strInteriorType)
			
			IF IS_VALID_INTERIOR(interiorIndex)
			AND IS_INTERIOR_READY(interiorIndex) 
//			OR (HAS_NET_TIMER_STARTED(thisBunker.stPinInMemTimer) 
//			AND HAS_NET_TIMER_EXPIRED(thisBunker.stPinInMemTimer, 12500)))
				BOOL bReady = TRUE
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					IF (IS_LOCAL_PLAYER_IN_PERSONAL_VEHICLE()
					OR IS_VEHICLE_UPGRADABLE_PEGASUS_VEHICLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())))
					AND thisBunker.pOwner = PLAYER_ID()
						IF NOT IS_SAFE_TO_SPAWN_PV()
							bReady = FALSE
							PRINTLN("[BLKSCR] - [SIMPLE_INTERIOR] AM_MP_BUNKER - MAINTAIN_BUNKER_LOADING_STATE -  IS_SAFE_TO_SPAWN_PV = FALSE")
						ELSE
							PRINTLN("[BLKSCR] - [SIMPLE_INTERIOR] AM_MP_BUNKER - MAINTAIN_BUNKER_LOADING_STATE -  IS_SAFE_TO_SPAWN_PV = TRUE")
						ENDIF
					ENDIF
				ENDIF	
				IF bReady
					//CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_BUNKER_MOVE_OUT_OF_WAY)
					
					IF IS_PLAYER_IN_BUNKER(PLAYER_ID())
						FORCE_ROOM_FOR_ENTITY(PLAYER_PED_ID(), interiorIndex, CI_BUNKER_ROOM_KEY)
						PRINTLN("[SIMPLE_INTERIOR] AM_MP_BUNKER - MAINTAIN_BUNKER_LOADING_STATE -  Forcing room for palyer, we are inside bunker")
					ELSE
						PRINTLN("[SIMPLE_INTERIOR] AM_MP_BUNKER - MAINTAIN_BUNKER_LOADING_STATE -  Preventing room force, not inside bunker yet.")
					ENDIF
					REPIN_AND_REFRESH_SIMPLE_INTERIOR()
					
					SET_SIMPLE_INTERIOR_CHILD_SCRIPT_ALLOWS_FOR_WARP(TRUE)
					SET_BIT(thisBunker.iBS, BS_BUNKER_DATA_INTERIOR_REFRESHED_ON_INIT)
					
					RESET_NET_TIMER(thisBunker.stPinInMemTimer)
				ENDIF
			ELSE
				IF NOT HAS_NET_TIMER_STARTED(thisBunker.stPinInMemTimer)
					PRINTLN("[SIMPLE_INTERIOR] AM_MP_BUNKER - MAINTAIN_BUNKER_LOADING_STATE - pinning interior with ID: ", NATIVE_TO_INT(interiorIndex))
					PIN_INTERIOR_IN_MEMORY_VALIDATE(interiorIndex)
					START_NET_TIMER(thisBunker.stPinInMemTimer)
				ELIF HAS_NET_TIMER_EXPIRED(thisBunker.stPinInMemTimer, 12500)
					RESET_NET_TIMER(thisBunker.stPinInMemTimer)
				ENDIF
				
				PRINTLN("[SIMPLE_INTERIOR] AM_MP_BUNKER - MAINTAIN_BUNKER_LOADING_STATE - waiting for a valid and ready interior with ID: ", 
							NATIVE_TO_INT(interiorIndex), " valid? ", IS_VALID_INTERIOR(interiorIndex), " ready? ", IS_INTERIOR_READY(interiorIndex), " disabled? ", IS_INTERIOR_DISABLED(interiorIndex))
				// Wait for interior to validate.
			ENDIF
		ELSE
			// Script was relaunched, skipping interior refresh.
			SET_SIMPLE_INTERIOR_CHILD_SCRIPT_ALLOWS_FOR_WARP(TRUE)
			SET_BIT(thisBunker.iBS, BS_BUNKER_DATA_INTERIOR_REFRESHED_ON_INIT)
		ENDIF
	ENDIF
	
	IF CAN_SIMPLE_INTERIOR_CHILD_SCRIPT_TAKE_CONTROL()
	AND IS_BIT_SET(thisBunker.iBS, BS_BUNKER_DATA_INTERIOR_REFRESHED_ON_INIT)
		
		// TODO: Start any audio scene(s) here.
		
		BOOL bReady = TRUE
		
		IF NOT IS_BIT_SET(thisBunker.iBS, BS_BUNKER_DATA_LOAD_SCENE_STARTED)
			VECTOR vBunkerCoords
			FLOAT fBunkerHeading//, fBunkerRadius
			
			SIMPLE_INTERIOR_INSIDE_BOUNDING_BOX bBox
			GET_SIMPLE_INTERIOR_INSIDE_BOUNDING_BOX(thisBunker.eSimpleInteriorID, bBox)
			
			//fBunkerRadius = GET_SIMPLE_INTERIOR_RADIUS_FROM_BOUNDING_BOX(bBox)
			GET_SIMPLE_INTERIOR_INTERIOR_TYPE_AND_POSITION(thisBunker.eSimpleInteriorID, strInteriorType, vBunkerCoords, fBunkerHeading)
			vBunkerCoords.z = vBunkerCoords.z + 1
			
//			//temp 
//			IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
//				NEW_LOAD_SCENE_START_SPHERE(<<890,-3248.3,-98>>, fBunkerRadius,NEWLOADSCENE_FLAG_REQUIRE_COLLISION)
//			ENDIF
			SET_BIT(thisBunker.iBS, BS_BUNKER_DATA_LOAD_SCENE_STARTED)
			PRINTLN("[SIMPLE_INTERIOR] AM_MP_BUNKER - MAINTAIN_BUNKER_LOADING_STATE -  BS_BUNKER_DATA_LOAD_SCENE_STARTED FALSE")
			bReady = FALSE
		ELSE
			IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBS, BS_SIMPLE_INTERIOR_GLOBAL_PLAYER_BD_DRIVER_ENTERING)
			AND NOT g_bForcedOnFootEntryToSimpleInterior
				PRINTLN("[SIMPLE_INTERIOR] AM_MP_BUNKER - MAINTAIN_BUNKER_LOADING_STATE -  BS_BUNKER_DATA_LOAD_SCENE_STARTED waiting to be warped in as passenger")
				bReady = FALSE
			ELSE
				IF IS_NEW_LOAD_SCENE_ACTIVE()
					IF NOT IS_NEW_LOAD_SCENE_LOADED()
						PRINTLN("[SIMPLE_INTERIOR] AM_MP_BUNKER - MAINTAIN_BUNKER_LOADING_STATE -  BS_BUNKER_DATA_LOAD_SCENE_STARTED IS_NEW_LOAD_SCENE_LOADED FALSE")
						bReady = FALSE
					ELSE
						GET_SIMPLE_INTERIOR_INTERIOR_TYPE_AND_POSITION(thisBunker.eSimpleInteriorID, strInteriorType, vInteriorPosition, fInteriorHeading)
						interiorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(vInteriorPosition, strInteriorType)
			
						IF IS_VALID_INTERIOR(interiorIndex)
						AND IS_INTERIOR_READY(interiorIndex) 
						AND IS_LOCAL_PLAYER_WITHIN_BUNKER_BOUNDS()
							PRINTLN("[SIMPLE_INTERIOR] AM_MP_BUNKER - MAINTAIN_BUNKER_LOADING_STATE -  inside and load scene is ready continuing")
							
							NEW_LOAD_SCENE_STOP()
						ELSE
							PRINTLN("[SIMPLE_INTERIOR] AM_MP_BUNKER - MAINTAIN_BUNKER_LOADING_STATE -  BS_BUNKER_KILL_LOAD_SCENE_WHEN_INSIDE- Set")
							SET_BIT(thisBunker.iBS,BS_BUNKER_KILL_LOAD_SCENE_WHEN_INSIDE)
						ENDIF
					ENDIF
				ELSE
					// Load scene is not active.
				ENDIF
			ENDIF
		ENDIF
		
		IF bReady
			IF thisBunker.bShouldPlayIntroCut
				//-- Need to play intro mocap. Moved here from RUN_MAIN_CLIENT_LOGIC() so the above load scene can finish
				IF CAN_SIMPLE_INTERIOR_CHILD_SCRIPT_TAKE_CONTROL()
					SET_SIMPLE_INTERIOR_CHILD_SCRIPT_IS_IN_CONTROL(TRUE)
					SET_BUNKER_STATE(BUNKER_STATE_PLAYING_INTRO_CUTSCENE)
				ELSE
					PRINTLN("[SIMPLE_INTERIOR] AM_MP_BUNKER - MAINTAIN_BUNKER_LOADING_STATE - Should play intro cut - waiting for CAN_SIMPLE_INTERIOR_CHILD_SCRIPT_TAKE_CONTROL")
				ENDIF
			ELIF thisBunker.introCutscene.bPlaying
				SET_BUNKER_STATE(BUNKER_STATE_PLAYING_SETUP_CUTSCENE)
			ELSE
				// Let internal script know that its child script is ready.
				IF MAINTAIN_AA_TRAILER_CREATION_BUNKER()
				AND MAINTAIN_LAUNCHING_CARMOD_SCRIPT()
					SET_SIMPLE_INTERIOR_CHILD_SCRIPT_READY(TRUE)
					SET_BUNKER_STATE(BUNKER_STATE_IDLE)
					SET_TRANS_DONT_CLEAR_ENTERING_FLAG(FALSE)
					SIMPLE_INTERIOR_SET_UPDATE_EXIT_LOCATE_COORDS(TRUE)
				ELSE
					PRINTLN("[SIMPLE_INTERIOR] AM_MP_BUNKER - MAINTAIN_BUNKER_LOADING_STATE -  waiting for MAINTAIN_AA_TRAILER_CREATION_BUNKER")
				ENDIF
			ENDIF
		ENDIF
	ELSE
		// TODO: Wait for child script to take control.
		PRINTLN("[SIMPLE_INTERIOR] AM_MP_BUNKER - MAINTAIN_BUNKER_LOADING_STATE -  CAN_SIMPLE_INTERIOR_CHILD_SCRIPT_TAKE_CONTROL FALSE AND IS_BIT_SET(thisBunker.iBS, BS_BUNKER_DATA_INTERIOR_REFRESHED_ON_INIT) = ",IS_BIT_SET(thisBunker.iBS, BS_BUNKER_DATA_INTERIOR_REFRESHED_ON_INIT))
	ENDIF
ENDPROC		

PROC MANAGE_BUNKER_GUN_LOCKER_WEAPONS(VAULT_WEAPON_LOADOUT_CUSTOMIZATION& sWLoadoutCustomization)
	IF thisBunker.pOwner != INVALID_PLAYER_INDEX()
		IF IS_BIT_SET(GlobalPlayerBD_FM[NATIVE_TO_INT(thisBunker.pOwner)].propertyDetails.iBS,PROPERTY_BROADCAST_BS_OFFICE_VAULT_OWN_PISTOL)
		AND NOT IS_BIT_SET(sWLoadoutCustomization.iWeaponModelBS,VAULT_PISTOL_CREATED)  
			INT pistolModel = HASH("W_PI_PISTOL")
			IF NOT DOES_ENTITY_EXIST(sWLoadoutCustomization.weapons[0])
				REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES,pistolModel))
				IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES,pistolModel))
					sWLoadoutCustomization.weapons[0] = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES,pistolModel),<<909.1112, -3203.6139, -96.8351>>,FALSE,FALSE)
					SET_ENTITY_COORDS_NO_OFFSET(sWLoadoutCustomization.weapons[0], <<909.1112, -3203.6139, -96.8351>>)
					SET_ENTITY_ROTATION(sWLoadoutCustomization.weapons[0], <<90.0000, 73.4400, 0.0000>>)
					FREEZE_ENTITY_POSITION(sWLoadoutCustomization.weapons[0], TRUE)
					SET_ENTITY_COLLISION(sWLoadoutCustomization.weapons[0], TRUE)
					SET_ENTITY_INVINCIBLE(sWLoadoutCustomization.weapons[0], TRUE)
					SET_ENTITY_VISIBLE(sWLoadoutCustomization.weapons[0], TRUE)
					SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES,pistolModel))
					SET_BIT(sWLoadoutCustomization.iWeaponModelBS,VAULT_PISTOL_CREATED)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_BUNKER_GUN_LOCKER_WEAPONS: successfully created pistol.")
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_BUNKER_GUN_LOCKER_WEAPONS - W_PI_PISTOL model exist")
				ENDIF
			ENDIF
		ENDIF
		IF IS_BIT_SET(GlobalPlayerBD_FM[NATIVE_TO_INT(thisBunker.pOwner)].propertyDetails.iBS,PROPERTY_BROADCAST_BS_OFFICE_VAULT_OWN_RIFLE)
			IF NOT IS_BIT_SET(sWLoadoutCustomization.iWeaponModelBS,VAULT_RIFLE_CREATED)  
				INT rifleModel = HASH("W_AR_SPECIALCARBINE")
				IF NOT DOES_ENTITY_EXIST(sWLoadoutCustomization.weapons[1])
					REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES,rifleModel))
					IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES,rifleModel))

						sWLoadoutCustomization.weapons[1] = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES,rifleModel),<<908.8580, -3203.1649, -97.3851>>, FALSE, FALSE)
						SET_ENTITY_COORDS_NO_OFFSET(sWLoadoutCustomization.weapons[1], <<908.8580, -3203.1649, -97.3851>>)
						SET_ENTITY_ROTATION(sWLoadoutCustomization.weapons[1], <<0.0000, -70.0000, 19.0000>>)

						FREEZE_ENTITY_POSITION(sWLoadoutCustomization.weapons[1], TRUE)
						SET_ENTITY_COLLISION(sWLoadoutCustomization.weapons[1], TRUE)
						SET_ENTITY_INVINCIBLE(sWLoadoutCustomization.weapons[1], TRUE)
						SET_ENTITY_VISIBLE(sWLoadoutCustomization.weapons[1], TRUE)
						SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES,rifleModel))
						SET_BIT(sWLoadoutCustomization.iWeaponModelBS,VAULT_RIFLE_CREATED)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_BUNKER_GUN_LOCKER_WEAPONS: successfully created rifle.")
					ELSE
						CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_BUNKER_GUN_LOCKER_WEAPONS - W_AR_SPECIALCARBINE model is not loaded")
					ENDIF
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_BUNKER_GUN_LOCKER_WEAPONS - W_AR_SPECIALCARBINE model exist")
				ENDIF
			ELIF NOT IS_BIT_SET(sWLoadoutCustomization.iWeaponModelBS, VAULT_ASSRIFLE_CREATED)
				INT rifleModel = HASH("W_AR_ASSAULTRIFLE")
				IF NOT DOES_ENTITY_EXIST(sWLoadoutCustomization.weapons[8])
					REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES,rifleModel))
					IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES,rifleModel))
						sWLoadoutCustomization.weapons[8] = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES,rifleModel), <<908.8580, -3202.9349, -97.4426>>, FALSE, FALSE)
						SET_ENTITY_COORDS_NO_OFFSET(sWLoadoutCustomization.weapons[8], <<908.8580, -3202.9349, -97.4426>>)
						SET_ENTITY_ROTATION(sWLoadoutCustomization.weapons[8], <<0.0000, -70.0000, 19.4400>>)
						FREEZE_ENTITY_POSITION(sWLoadoutCustomization.weapons[8], TRUE)
						SET_ENTITY_COLLISION(sWLoadoutCustomization.weapons[8], TRUE)
						SET_ENTITY_INVINCIBLE(sWLoadoutCustomization.weapons[8], TRUE)
						SET_ENTITY_VISIBLE(sWLoadoutCustomization.weapons[8], TRUE)
						SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES,rifleModel))
						SET_BIT(sWLoadoutCustomization.iWeaponModelBS,VAULT_ASSRIFLE_CREATED)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_BUNKER_GUN_LOCKER_WEAPONS: successfully created assrifle.")
					ELSE
						CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_BUNKER_GUN_LOCKER_WEAPONS - W_AR_ASSAULTRIFLE model is not loaded")
					ENDIF
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_BUNKER_GUN_LOCKER_WEAPONS - W_AR_ASSAULTRIFLE model exist")
				ENDIF
			ENDIF
		ENDIF
		IF IS_BIT_SET(GlobalPlayerBD_FM[NATIVE_TO_INT(thisBunker.pOwner)].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_OFFICE_VAULT_OWN_SHOTGUN)
		AND NOT IS_BIT_SET(sWLoadoutCustomization.iWeaponModelBS,VAULT_SHOTGUN_CREATED) 
			CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_BUNKER_GUN_LOCKER_WEAPONS - start creating shotgun")
			INT shotgunModel = HASH("w_sg_pumpshotgun")
			IF NOT DOES_ENTITY_EXIST(sWLoadoutCustomization.weapons[2])
				REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES,shotgunModel))
				IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES,shotgunModel))

					sWLoadoutCustomization.weapons[2] = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES,shotgunModel), <<908.8880, -3203.2750, -97.4151>>,FALSE,FALSE)
					SET_ENTITY_COORDS_NO_OFFSET(sWLoadoutCustomization.weapons[2], <<908.8880, -3203.2750, -97.4151>>)
					SET_ENTITY_ROTATION(sWLoadoutCustomization.weapons[2], <<0.0000, -70.0000, 19.0000>>)
					FREEZE_ENTITY_POSITION(sWLoadoutCustomization.weapons[2], TRUE)
					SET_ENTITY_COLLISION(sWLoadoutCustomization.weapons[2], TRUE)
					SET_ENTITY_INVINCIBLE(sWLoadoutCustomization.weapons[2], TRUE)
					SET_ENTITY_VISIBLE(sWLoadoutCustomization.weapons[2], TRUE)
					SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES,shotgunModel))
					SET_BIT(sWLoadoutCustomization.iWeaponModelBS,VAULT_SHOTGUN_CREATED)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_BUNKER_GUN_LOCKER_WEAPONS: successfully created shotgun.")
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_BUNKER_GUN_LOCKER_WEAPONS - w_sg_pumpshotgun model is not loaded")
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_BUNKER_GUN_LOCKER_WEAPONS - w_sg_pumpshotgun exist")
			ENDIF
		ENDIF
		IF IS_BIT_SET(GlobalPlayerBD_FM[NATIVE_TO_INT(thisBunker.pOwner)].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_OFFICE_VAULT_OWN_MICROSMG)
		AND NOT IS_BIT_SET(sWLoadoutCustomization.iWeaponModelBS,VAULT_SMG_CREATED) 
			CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_BUNKER_GUN_LOCKER_WEAPONS - start creating smg")
			INT smgModel = HASH("w_sb_microsmg")
			IF NOT DOES_ENTITY_EXIST(sWLoadoutCustomization.weapons[3])
				REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES,smgModel))
				IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES,smgModel))
					
					sWLoadoutCustomization.weapons[3] = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES,smgModel), <<909.0444, -3203.2850, -96.8324>>,FALSE,FALSE)
					SET_ENTITY_COORDS_NO_OFFSET(sWLoadoutCustomization.weapons[3], <<909.0444, -3203.28500, -96.8324>>)
					SET_ENTITY_ROTATION(sWLoadoutCustomization.weapons[3], <<90.0000, 72.0000, 0.0000>>)
					FREEZE_ENTITY_POSITION(sWLoadoutCustomization.weapons[3], TRUE)
					SET_ENTITY_COLLISION(sWLoadoutCustomization.weapons[3], TRUE)
					SET_ENTITY_INVINCIBLE(sWLoadoutCustomization.weapons[3], TRUE)
					SET_ENTITY_VISIBLE(sWLoadoutCustomization.weapons[3], TRUE)
					SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES,smgModel))
					SET_BIT(sWLoadoutCustomization.iWeaponModelBS,VAULT_SMG_CREATED)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_BUNKER_GUN_LOCKER_WEAPONS: successfully created smg.")
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_BUNKER_GUN_LOCKER_WEAPONS - w_sb_microsmg model is not loaded")
				ENDIF
			ENDIF
		ENDIF
		IF IS_BIT_SET(GlobalPlayerBD_FM[NATIVE_TO_INT(thisBunker.pOwner)].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_OFFICE_VAULT_OWN_BOMB)
		AND NOT IS_BIT_SET(sWLoadoutCustomization.iWeaponModelBS,VAULT_BOMB_CREATED) 
			CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_BUNKER_GUN_LOCKER_WEAPONS - start creating bomb")
			INT shotgunModel = HASH("w_ex_pe")
			IF NOT DOES_ENTITY_EXIST(sWLoadoutCustomization.weapons[4])
				REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES,shotgunModel))
				IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES,shotgunModel))
					sWLoadoutCustomization.weapons[4] = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES,shotgunModel),<<908.8055, -3203.1300, -96.8219>>, FALSE, FALSE)
					SET_ENTITY_COORDS_NO_OFFSET(sWLoadoutCustomization.weapons[4], <<908.8055, -3203.1300, -96.8219>>)
					SET_ENTITY_ROTATION(sWLoadoutCustomization.weapons[4],  <<0.0000, -0.0000, 0.0000>>)
					FREEZE_ENTITY_POSITION(sWLoadoutCustomization.weapons[4], TRUE)
					SET_ENTITY_COLLISION(sWLoadoutCustomization.weapons[4], TRUE)
					SET_ENTITY_INVINCIBLE(sWLoadoutCustomization.weapons[4], TRUE)
					SET_ENTITY_VISIBLE(sWLoadoutCustomization.weapons[4], TRUE)
					SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES,shotgunModel))
					SET_BIT(sWLoadoutCustomization.iWeaponModelBS,VAULT_BOMB_CREATED)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_BUNKER_GUN_LOCKER_WEAPONS: successfully created smg.")
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_BUNKER_GUN_LOCKER_WEAPONS - w_ex_pe model is not loaded")
				ENDIF
			ENDIF
		ENDIF
		IF IS_BIT_SET(GlobalPlayerBD_FM[NATIVE_TO_INT(thisBunker.pOwner)].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_OFFICE_VAULT_OWN_GRENADE)
		AND NOT IS_BIT_SET(sWLoadoutCustomization.iWeaponModelBS,VAULT_GRENADE_CREATED) 
			CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_BUNKER_GUN_LOCKER_WEAPONS - start creating grenade")
			INT grenadeModel = HASH("w_ex_grenadefrag")
			IF NOT DOES_ENTITY_EXIST(sWLoadoutCustomization.weapons[5])
				REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES,grenadeModel))
				IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES,grenadeModel))
					sWLoadoutCustomization.weapons[5] = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES,grenadeModel), <<908.9572, -3202.9911, -96.7859>>, FALSE, FALSE)
					SET_ENTITY_COORDS_NO_OFFSET(sWLoadoutCustomization.weapons[5], <<908.9572, -3202.9911, -96.7859>>)
					SET_ENTITY_ROTATION(sWLoadoutCustomization.weapons[5], <<0.0000, 0.0000, -65.5200>>)
					FREEZE_ENTITY_POSITION(sWLoadoutCustomization.weapons[5], TRUE)
					SET_ENTITY_COLLISION(sWLoadoutCustomization.weapons[5], TRUE)
					SET_ENTITY_INVINCIBLE(sWLoadoutCustomization.weapons[5], TRUE)
					SET_ENTITY_VISIBLE(sWLoadoutCustomization.weapons[5], TRUE)
					SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES,grenadeModel))
					SET_BIT(sWLoadoutCustomization.iWeaponModelBS,VAULT_GRENADE_CREATED)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_BUNKER_GUN_LOCKER_WEAPONS: successfully created grenade.")
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_BUNKER_GUN_LOCKER_WEAPONS - w_ex_grenadefrag model is not loaded")
				ENDIF
			ENDIF
		ENDIF
		IF IS_BIT_SET(GlobalPlayerBD_FM[NATIVE_TO_INT(thisBunker.pOwner)].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_OFFICE_VAULT_OWN_MG)
		AND NOT IS_BIT_SET(sWLoadoutCustomization.iWeaponModelBS,VAULT_MG_CREATED) 
			CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_BUNKER_GUN_LOCKER_WEAPONS - start creating mg")
			INT gunModel = HASH("w_mg_combatmg")
			IF NOT DOES_ENTITY_EXIST(sWLoadoutCustomization.weapons[6])
				REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES,gunModel))
				IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES,gunModel))
					sWLoadoutCustomization.weapons[6] = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES,gunModel), <<908.9220, -3203.3649, -97.4251>>, FALSE, FALSE)
					SET_ENTITY_COORDS_NO_OFFSET(sWLoadoutCustomization.weapons[6], <<908.9220, -3203.3649, -97.4251>>)
					SET_ENTITY_ROTATION(sWLoadoutCustomization.weapons[6], <<0.0000, -70.0000, 19.0000>>)
					FREEZE_ENTITY_POSITION(sWLoadoutCustomization.weapons[6], TRUE)
					SET_ENTITY_COLLISION(sWLoadoutCustomization.weapons[6], TRUE)
					SET_ENTITY_INVINCIBLE(sWLoadoutCustomization.weapons[6], TRUE)
					SET_ENTITY_VISIBLE(sWLoadoutCustomization.weapons[6], TRUE)
					SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES,gunModel))
					SET_BIT(sWLoadoutCustomization.iWeaponModelBS,VAULT_MG_CREATED)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_BUNKER_GUN_LOCKER_WEAPONS: successfully created mg.")
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_BUNKER_GUN_LOCKER_WEAPONS - w_mg_combatmg model is not loaded")
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_BUNKER_GUN_LOCKER_WEAPONS - w_mg_combatmg exist")
			ENDIF
		ENDIF
		IF IS_BIT_SET(GlobalPlayerBD_FM[NATIVE_TO_INT(thisBunker.pOwner)].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_OFFICE_VAULT_OWN_SNIPER)
		AND NOT IS_BIT_SET(sWLoadoutCustomization.iWeaponModelBS,VAULT_SNIPER_CREATED) 
			CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_BUNKER_GUN_LOCKER_WEAPONS - start creating snipper")
			INT gunModel = HASH("w_sr_marksmanrifle")
			IF NOT DOES_ENTITY_EXIST(sWLoadoutCustomization.weapons[7])
				REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES,gunModel))
				REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES,HASH("w_at_scope_large")))
				REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES,HASH("w_sr_marksmanrifle_mag1")))
				
				IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES,gunModel))
				AND HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES,HASH("w_at_scope_large")))
				AND HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES,HASH("w_sr_marksmanrifle_mag1")))
					sWLoadoutCustomization.weapons[7] = CREATE_WEAPON_OBJECT(WEAPONTYPE_DLC_MARKSMANRIFLE,0,<<908.8580, -3203.0651, -97.4126>>,TRUE)
					SET_ENTITY_ROTATION(sWLoadoutCustomization.weapons[7], <<0.0000, -70.0000, 19.0000>>)
					SET_ENTITY_COORDS_NO_OFFSET(sWLoadoutCustomization.weapons[7],<<908.8580, -3203.0651, -97.4126>>)
					FREEZE_ENTITY_POSITION(sWLoadoutCustomization.weapons[7], TRUE)
					SET_ENTITY_COLLISION(sWLoadoutCustomization.weapons[7], TRUE)
					SET_ENTITY_INVINCIBLE(sWLoadoutCustomization.weapons[7], TRUE)
					SET_ENTITY_VISIBLE(sWLoadoutCustomization.weapons[7], TRUE)
					SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES,gunModel))
					SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES,HASH("w_at_scope_large")))
					SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES,HASH("w_sr_marksmanrifle_mag1")))
					SET_BIT(sWLoadoutCustomization.iWeaponModelBS,VAULT_SNIPER_CREATED)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_BUNKER_GUN_LOCKER_WEAPONS: successfully created snipper.")
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_BUNKER_GUN_LOCKER_WEAPONS - w_sr_marksmanrifle model is not loaded")
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "MANAGE_BUNKER_GUN_LOCKER_WEAPONS - w_sr_marksmanrifle exist")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL CAN_PLAYER_START_GUN_LOCKER()
	IF thisBunker.pOwner != INVALID_PLAYER_INDEX()
		IF thisBunker.pOwner = PLAYER_ID()
			IF IS_BUNKER_GUNLOCKER_PURCHASED()
			AND NOT IS_PLAYER_USING_OFFICE_SEATID_VALID()
				// allow
			ELSE
				RETURN FALSE
			ENDIF
		ELSE
			IF IS_PLAYER_BUNKER_GUNLOCKER_PURCHASED(thisBunker.pOwner)
				IF (DOES_PLAYER_OWN_ANY_GUN_LOCKER())	// does player own clubhouse gun locker
				AND (NOT NETWORK_IS_IN_MP_CUTSCENE())
				AND NOT IS_PLAYER_USING_OFFICE_SEATID_VALID()
					// allow
				ELSE
					RETURN FALSE
				ENDIF
			ELSE
				RETURN FALSE
			ENDIF	
		ENDIF	
	ENDIF	

	RETURN TRUE
ENDFUNC


/// PURPOSE:
///    Safty check if we can open cun locker menu
/// RETURNS:
///    True if it is safe
FUNC BOOL IS_SAFE_TO_START_BUNKER_GUN_LOCKER_MENUS(BOOL bAllowHeist = FALSE)
	
	IF !CAN_PLAYER_START_GUN_LOCKER()
		RETURN FALSE
	ENDIF 
	
	IF !bAllowHeist
		IF Is_Player_Currently_On_MP_Heist(PLAYER_ID())
		OR Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID())
			RETURN FALSE
		ENDIF
	ENDIF
		
	IF	IS_PHONE_ONSCREEN()
	OR IS_INTERACTION_MENU_OPEN()
	OR IS_BROWSER_OPEN()
	OR IS_SELECTOR_ONSCREEN()
	OR IS_CUSTOM_MENU_ON_SCREEN()
	OR IS_PAUSE_MENU_ACTIVE()
	OR (MPGlobalsAmbience.bTriggerPropertyExitOnFoot)
		RETURN FALSE
	ENDIF
	RETURN TRUE
	
ENDFUNC

//PROC MAINTAIN_GUN_LOCKER_PROP()
//	IF IS_PLAYER_IN_BUNKER(PLAYER_ID())
//		IF thisBunker.pOwner != INVALID_PLAYER_INDEX()
//			IF IS_PLAYER_BUNKER_GUNLOCKER_PURCHASED(thisBunker.pOwner)
//				IF IS_VALID_INTERIOR(GET_INTERIOR_FROM_ENTITY(GET_PLAYER_PED(PLAYER_ID())))
//				AND IS_INTERIOR_READY(GET_INTERIOR_FROM_ENTITY(GET_PLAYER_PED(PLAYER_ID()))) 
//					IF NOT IS_INTERIOR_ENTITY_SET_ACTIVE(GET_INTERIOR_FROM_ENTITY(GET_PLAYER_PED(PLAYER_ID())), "Gun_locker_upgrade")
//						ACTIVATE_INTERIOR_ENTITY_SET(GET_INTERIOR_FROM_ENTITY(GET_PLAYER_PED(PLAYER_ID())), "Gun_locker_upgrade")
//						CDEBUG1LN(DEBUG_SAFEHOUSE, "ADD_ENTITY_SET - Activating: Gun_locker_upgrade")
//					ENDIF
//				ENDIF	
//			ENDIF
//		ENDIF	
//	ENDIF	
//ENDPROC

PROC MAINTAIN_BUNKER_GUN_LOCKER(VAULT_WEAPON_LOADOUT_CUSTOMIZATION& sWLoadoutCustomization)
	IF thisBunker.pOwner != INVALID_PLAYER_INDEX()
		IF IS_PLAYER_BUNKER_GUNLOCKER_PURCHASED(thisBunker.pOwner)
			IF NOT thisBunker.bPlayerLeftTheFactory
			AND NOT thisBunker.bApplyUpgrade
				SWITCH sWLoadoutCustomization.eCustomizationStage
					CASE VMC_STAGE_INIT
						CLEAN_UP_BUNKER_GUN_LOCKER(sWLoadoutCustomization)
						
						sWLoadoutCustomization.eCustomizationStage = VWC_STAGE_WAITING_TO_TRIGGER
					BREAK
					
					CASE VWC_STAGE_WAITING_TO_TRIGGER 				
						GET_NUM_AVAILABLE_WEAPON_GROUP(sWLoadoutCustomization)
						MANAGE_BUNKER_GUN_LOCKER_WEAPONS(sWLoadoutCustomization)
						
						IF IS_SAFE_TO_START_BUNKER_GUN_LOCKER_MENUS(TRUE)
							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<908.238159,-3202.909912,-98.187897>>, <<908.626343,-3204.099609,-96.187897>>, 1.000000)
							AND NOT IS_PED_RUNNING(PLAYER_PED_ID())
							AND DOES_ENTITY_EXIST(PLAYER_PED_ID())
							AND IS_PED_STILL(PLAYER_PED_ID())
								IF sWLoadoutCustomization.iVaultWeaponContext = NEW_CONTEXT_INTENTION
									IF NOT IS_HELP_MESSAGE_ON_SCREEN()
										REGISTER_CONTEXT_INTENTION(sWLoadoutCustomization.iVaultWeaponContext, CP_HIGH_PRIORITY, "OF_VAULT_MENU")
									ENDIF	
								ENDIF
								IF NOT IS_INTERACTION_MENU_OPEN()
									IF HAS_CONTEXT_BUTTON_TRIGGERED(sWLoadoutCustomization.iVaultWeaponContext)
										LOAD_MENU_ASSETS()
										BUILD_VAULT_WEAPON_MAIN_MENU(sWLoadoutCustomization)
										TASK_ACHIEVE_HEADING(PLAYER_PED_ID(), 292.4494, 0)	

										NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_LEAVE_CAMERA_CONTROL_ON)
										SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_UseKinematicModeWhenStationary, TRUE)
										SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DontActivateRagdollFromExplosions, TRUE)

										RELEASE_CONTEXT_INTENTION(sWLoadoutCustomization.iVaultWeaponContext)
										sWLoadoutCustomization.eCustomizationStage = VMC_STAGE_CUSTOMIZING 
									ENDIF
								ENDIF	
							ELSE
								RELEASE_CONTEXT_INTENTION(sWLoadoutCustomization.iVaultWeaponContext)
							ENDIF
						ELSE
							IF IS_CONTEXT_INTENTION_HELP_DISPLAYING(sWLoadoutCustomization.iVaultWeaponContext)
								RELEASE_CONTEXT_INTENTION(sWLoadoutCustomization.iVaultWeaponContext)
							ENDIF	
						ENDIF
					BREAK
					CASE VMC_STAGE_CUSTOMIZING
						PROCESS_VAULT_WEAPON_LOADOUT_MENU(sWLoadoutCustomization)
					BREAK
					CASE VMC_STAGE_CLEANUP
						CLEAN_UP_BUNKER_GUN_LOCKER(sWLoadoutCustomization)
						START_NET_TIMER(sWLoadoutCustomization.sMenuTimer)
					BREAK
				ENDSWITCH
			ELSE
				IF sWLoadoutCustomization.eCustomizationStage > VMC_STAGE_INIT
					CLEAN_UP_BUNKER_GUN_LOCKER(sWLoadoutCustomization)
				ENDIF
			ENDIF
		ENDIF	
	ENDIF	
ENDPROC

/// PURPOSE:
///    Maintain any required bunker help text
PROC MAINTAIN_BUNKER_HELP_TEXT()

	IF NOT IS_BIT_SET(thisBunker.iBS, BS_BUNKER_CALLED_CLEAR_HELP)
		EXIT
	ENDIF
	
	IF thisBunker.pOwner = PLAYER_ID()
	
		//BUNKER HELP TEXT
		IF NOT IS_BIT_SET(thisBunker.iBS2, BS_2_BUNKER_DONE_RESEARCH_HELP)
		#IF FEATURE_DLC_1_2022
		OR IS_BIT_SET(g_SimpleInteriorData.iTwelfthBS, BS12_SIMPLE_INTERIOR_BUNKER_DO_GUNRUN_SETUP_HELP_2)
		#ENDIF
			IF IS_SCREEN_FADED_IN()
			AND NOT IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
			AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
			AND HAS_PLAYER_COMPLETED_FACTORY_SETUP_MISSION(thisBunker.pOwner, GET_FACTORY_ID_FROM_FACTORY_SLOT(thisBunker.pOwner, thisBunker.iSaveSlot))
				
				IF NOT IS_BIT_SET(thisBunker.iBS2, BS_2_BUNKER_DONE_RESEARCH_HELP)
					IF GET_BIKER_RESUPPLY_COMPLETE_TOTAL_FOR_FACTORY(GET_ACTIVE_CHARACTER_SLOT(), BUNKER_SAVE_SLOT) > 2
						SET_BIT(thisBunker.iBS2, BS_2_BUNKER_DONE_RESEARCH_HELP)
						EXIT
					ENDIF
					
					IF GET_FACTORY_PRODUCTION_MODE(thisBunker.pOwner, thisBunker.eID) = PRODUCTION_MODE_GOODS
						IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_ASSIGNED_STAFF_TO_RESEARCH)
							IF GET_PACKED_STAT_INT(PACKED_MP_INT_HELP_BUNKER_STAFF_ASSIGN) < 3
								PRINT_HELP("GR_ASSIGN_RES", 8000)
								SET_BIT(thisBunker.iBS2, BS_2_BUNKER_DONE_RESEARCH_HELP)
								#IF FEATURE_DLC_1_2022
								SET_BIT(g_SimpleInteriorData.iTwelfthBS, BS12_SIMPLE_INTERIOR_BUNKER_DO_GUNRUN_SETUP_HELP_2)
								#ENDIF
								
								INT iCount = GET_PACKED_STAT_INT(PACKED_MP_INT_HELP_BUNKER_STAFF_ASSIGN)
								SET_PACKED_STAT_INT(PACKED_MP_INT_HELP_BUNKER_STAFF_ASSIGN, (iCount + 1))
								EXIT
							ELSE
								SET_BIT(thisBunker.iBS2, BS_2_BUNKER_DONE_RESEARCH_HELP)
							ENDIF
						ELSE
							SET_BIT(thisBunker.iBS2, BS_2_BUNKER_DONE_RESEARCH_HELP)
						ENDIF
					ELSE
						SET_BIT(thisBunker.iBS2, BS_2_BUNKER_DONE_RESEARCH_HELP)
					ENDIF
				ENDIF
				
				#IF FEATURE_DLC_1_2022
				IF IS_BIT_SET(g_SimpleInteriorData.iTwelfthBS, BS12_SIMPLE_INTERIOR_BUNKER_DO_GUNRUN_SETUP_HELP_2)
					PRINT_HELP("GR_ASSIGN_RES2")
					CLEAR_BIT(g_SimpleInteriorData.iTwelfthBS, BS12_SIMPLE_INTERIOR_BUNKER_DO_GUNRUN_SETUP_HELP_2)
					EXIT
				ENDIF
				#ENDIF
			ENDIF
		ENDIF
				
		IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBS, BS_SIMPLE_INTERIOR_GLOBAL_PLAYER_BD_INITIAL_HELP_DONE)
		AND NOT HAS_PLAYER_COMPLETED_FACTORY_SETUP_MISSION(thisBunker.pOwner, GET_FACTORY_ID_FROM_FACTORY_SLOT(thisBunker.pOwner, thisBunker.iSaveSlot))
		AND IS_SCREEN_FADED_IN()
		AND NOT IS_LOCAL_PLAYER_WALKING_OUT_OF_THIS_FACTORY(thisBunker.eID)
		AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
		#IF FEATURE_GEN9_EXCLUSIVE
		AND NOT IS_PLAYER_ON_MP_INTRO_MISSION()
		#ENDIF
			PRINT_HELP("BUNKER_HELP_1")
			
			SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBS, BS_SIMPLE_INTERIOR_GLOBAL_PLAYER_BD_INITIAL_HELP_DONE)
		ENDIF
		
		IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSTwo, BS_SIMPLE_INTERIOR_GLOBAL_PLAYER_BD_INITIAL_HELP_DONE_2)
		AND IS_SCREEN_FADED_IN()
		AND NOT IS_LOCAL_PLAYER_WALKING_OUT_OF_THIS_FACTORY(thisBunker.eID)
		AND DOES_BLIP_EXIST(bModShopBlip)
		AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
		#IF FEATURE_GEN9_EXCLUSIVE
		AND NOT SHOULD_BLOCK_BUNKER_HELP_DURING_GUNRUNING_CAREER_INTRO()
		#ENDIF
			IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_FACTORY_MECHANIC_HELP1)
				IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_FACTORY_MECHANIC_HELP2)
					SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_FACTORY_MECHANIC_HELP1, TRUE)
					PRINT_HELP("BUNKER_HELP_2")
				ELSE
				ENDIF
			ELSE
				IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_FACTORY_MECHANIC_HELP2)
					SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_FACTORY_MECHANIC_HELP2, TRUE)
					PRINT_HELP("BUNKER_HELP_2")
				ELSE
					SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_FACTORY_MECHANIC_HELP1, FALSE)
					PRINT_HELP("BUNKER_HELP_2")
				ENDIF
			ENDIF
			SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSTwo, BS_SIMPLE_INTERIOR_GLOBAL_PLAYER_BD_INITIAL_HELP_DONE_2)
		ENDIF
		
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<905.182861, -3202.962158, -98.178947>>, <<907.158691, -3202.134033, -95.667679>>, 2.5) // Personal Quarters
		AND NOT IS_BUNKER_SAVEBED_PURCHASED()
		AND NOT IS_BROWSER_OPEN()
		#IF FEATURE_GEN9_EXCLUSIVE
		AND NOT IS_PLAYER_ON_MP_INTRO()
		#ENDIF
			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("BUNK_PURCH_PQ")
				PRINT_HELP_FOREVER("BUNK_PURCH_PQ") // Go to foreclosures.maze-bank.com and purchase the Personal Quarters expansion to access this area.
			ENDIF
		ELSE
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("BUNK_PURCH_PQ")
				CLEAR_HELP()
			ENDIF
		ENDIF
		
		IF (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<891.946655,-3190.607910,-98.032509>>, <<895.670410,-3190.536621,-95.463676>>, 4.0) // Shooting Range
		OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<874.644592,-3195.665039,-97.672562>>, <<874.636841,-3193.719238,-95.144218>>, 3.5))
		AND NOT IS_BUNKER_FIRING_RANGE_BLACK_PURCHASED()
		AND NOT IS_BUNKER_FIRING_RANGE_WHITE_PURCHASED()
		AND NOT IS_BROWSER_OPEN()
		#IF FEATURE_GEN9_EXCLUSIVE
		AND NOT IS_PLAYER_ON_MP_INTRO()
		#ENDIF
			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MP_SR_LOCKED_1")
				PRINT_HELP_FOREVER("MP_SR_LOCKED_1") // Go to foreclosures.maze-bank.com and purchase the Shooting Range expansion to access this area.
			ENDIF
		ELSE
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MP_SR_LOCKED_1")
				CLEAR_HELP()
			ENDIF
		ENDIF
		
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<908.824646,-3203.922852,-98.187889>>, <<908.007874,-3201.832520,-95.937889>>, 1.500000)	// Gun locker
		AND NOT IS_BUNKER_GUNLOCKER_PURCHASED()
		AND NOT IS_BROWSER_OPEN()
		#IF FEATURE_GEN9_EXCLUSIVE
		AND NOT IS_PLAYER_ON_MP_INTRO()
		#ENDIF
			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CH_GUN_DIS")
				PRINT_HELP_FOREVER("CH_GUN_DIS") // Go to foreclosures.maze-bank.com and purchase the gun lcoker
			ENDIF
		ELSE
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CH_GUN_DIS")
				CLEAR_HELP()
			ENDIF
		ENDIF
		
		IF IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(), g_viGunRunTruckInBunker)
		AND GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
		AND g_iNumberOfFreemodeMOCS >= g_sMPTunables.IGR_MOC_MAX_AMOUNT
		AND IS_OWNERS_ARMORY_TRUCK_INSIDE_BUNKER(PLAYER_ID())
		AND (IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
		OR IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_BRAKE))
			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PIM_HRTV18")
				PRINT_HELP("PIM_HRTV18")
			ENDIF
		ENDIF
	ELSE
		IF (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<891.946655,-3190.607910,-98.032509>>, <<895.670410,-3190.536621,-95.463676>>, 4.0) // Shooting Range
		OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<874.644592,-3195.665039,-97.672562>>, <<874.636841,-3193.719238,-95.144218>>, 3.5))
		AND NOT IS_PLAYER_BUNKER_FIRING_RANGE_BLACK_PURCHASED(thisBunker.pOwner)
		AND NOT IS_PLAYER_BUNKER_FIRING_RANGE_WHITE_PURCHASED(thisBunker.pOwner)
		AND NOT IS_BROWSER_OPEN()
		#IF FEATURE_GEN9_EXCLUSIVE
		AND NOT IS_PLAYER_ON_MP_INTRO()
		#ENDIF
			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MP_SR_LOCKED_2")
				PRINT_HELP_FOREVER("MP_SR_LOCKED_2") // You do not have access to the Shooting Range.
			ENDIF
		ELSE
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MP_SR_LOCKED_2")
				CLEAR_HELP()
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_BUNKER_BLIPS()	
	IF thisBunker.pOwner = PLAYER_ID()
	#IF FEATURE_GEN9_EXCLUSIVE
	AND NOT IS_PLAYER_ON_MP_INTRO_MISSION()
	#ENDIF
		IF DOES_BLIP_EXIST(bLaptopBlip)
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("BUNKER_HELP_1")
				IF NOT IS_BIT_SET(thisBunker.iBS2, BS_2_BUNKER_FLASHING_LAPTOP_BLIP)
					SET_BLIP_FLASHES(bLaptopBlip, TRUE)
					SET_BIT(thisBunker.iBS2, BS_2_BUNKER_FLASHING_LAPTOP_BLIP)
				ENDIF
			ELIF IS_BIT_SET(thisBunker.iBS2, BS_2_BUNKER_FLASHING_LAPTOP_BLIP)
				CLEAR_BIT(thisBunker.iBS2, BS_2_BUNKER_FLASHING_LAPTOP_BLIP)
				SET_BLIP_FLASHES(bLaptopBlip, FALSE)
			ENDIF
			
			IF IS_PLAYER_IN_ZOOMED_OUT_RADAR_OR_MAP(PLAYER_ID())
				REMOVE_BLIP(bLaptopBlip)
			ENDIF
		ELIF NOT IS_PLAYER_IN_ZOOMED_OUT_RADAR_OR_MAP(PLAYER_ID())
			
			bLaptopBlip = CREATE_BLIP_FOR_COORD(<<908.9161, -3207.8999, -97.2450>>)
			SET_BLIP_SPRITE(bLaptopBlip, RADAR_TRACE_LAPTOP)
			SET_BLIP_NAME_FROM_TEXT_FILE(bLaptopBlip, "OR_PC_BLIP")
			SET_BLIP_DISPLAY(bLaptopBlip, DISPLAY_BOTH)
		ENDIF
	ELSE
		IF DOES_BLIP_EXIST(bLaptopBlip)
			REMOVE_BLIP(bLaptopBlip)
		ENDIF
	ENDIF
	
	IF NOT DOES_BLIP_EXIST(bShootingRangeBlip)
	AND (IS_PLAYER_BUNKER_FIRING_RANGE_BLACK_PURCHASED(thisBunker.pOwner) OR IS_PLAYER_BUNKER_FIRING_RANGE_WHITE_PURCHASED(thisBunker.pOwner))
	AND NOT g_sMPTunables.BGR_SHOOTING_RANGE_DISABLE
	AND NOT IS_PLAYER_IN_ZOOMED_OUT_RADAR_OR_MAP(PLAYER_ID())
		bShootingRangeBlip = CREATE_BLIP_FOR_COORD(<<895.9441, -3172.4500, -98.1236>>)
		SET_BLIP_SPRITE(bShootingRangeBlip, RADAR_TRACE_SHOOTING_RANGE)
		SET_BLIP_NAME_FROM_TEXT_FILE(bShootingRangeBlip, "BLIP_119")
		SET_BLIP_DISPLAY(bShootingRangeBlip, DISPLAY_BOTH)
	ENDIF
	IF DOES_BLIP_EXIST(bShootingRangeBlip)
	AND IS_PLAYER_IN_ZOOMED_OUT_RADAR_OR_MAP(PLAYER_ID())
		REMOVE_BLIP(bShootingRangeBlip)
	ENDIF
	
	
	IF thisBunker.pOwner = PLAYER_ID()
	#IF FEATURE_GEN9_EXCLUSIVE
	AND NOT SHOULD_BLOCK_BUNKER_HELP_DURING_GUNRUNING_CAREER_INTRO()
	#ENDIF
		IF NOT DOES_BLIP_EXIST(bModShopBlip)
		AND NOT IS_PLAYER_IN_ZOOMED_OUT_RADAR_OR_MAP(PLAYER_ID())
			bModShopBlip = CREATE_BLIP_FOR_COORD(<<834.0781, -3243.9580, -98.699>>)
			SET_BLIP_SPRITE(bModShopBlip, RADAR_TRACE_GR_MOC_UPGRADE)
			//SET_BLIP_SCALE(bModShopBlip, 1)
			SET_BLIP_NAME_FROM_TEXT_FILE(bModShopBlip, "BLIP_566")
			SET_BLIP_DISPLAY(bModShopBlip, DISPLAY_BOTH)
		ENDIF
		
		IF DOES_BLIP_EXIST(bModShopBlip)
		AND IS_PLAYER_IN_ZOOMED_OUT_RADAR_OR_MAP(PLAYER_ID())
			REMOVE_BLIP(bModShopBlip)
		ENDIF
	ELSE
		IF DOES_BLIP_EXIST(bModShopBlip)
			REMOVE_BLIP(bModShopBlip)
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_MOVE_OUT_OF_WAY()
	IF IS_NET_PLAYER_OK(PLAYER_ID())
	AND g_ownerOfBunkerPropertyIAmIn != INVALID_PLAYER_INDEX()
	AND ((IS_BIT_SET(playerBD[NATIVE_TO_INT(g_ownerOfBunkerPropertyIAmIn)].iBS, BS_BUNKER_MOVE_OUT_OF_WAY)
	AND g_ownerOfBunkerPropertyIAmIn != PLAYER_ID())
	OR IS_BIT_SET(serverBD.iBS, BS_BUNKER_SERVER_DATA_CLEAR_PV_SPACE))
		IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) != PERFORMING_TASK
		AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) != WAITING_TO_START_TASK
		AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<883.123779,-3243.632813,-99.273224>>, <<883.033020,-3236.130615,-96.536865>>, 7.5)
			VECTOR vPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
			
			IF vPosition.x > 883.0
				vPosition.x = 890.0
			ELSE
				IF vPosition.y <= -3238.5
					vPosition.x = 878.0
					vPosition.y = -3240.4
				ELSE
					vPosition.x = 878.0
				ENDIF
			ENDIF
			
			TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), vPosition, PEDMOVEBLENDRATIO_WALK, DEFAULT, DEFAULT, 2.0)
		ENDIF
	ENDIF
	
	IF IS_NET_PLAYER_OK(PLAYER_ID())
	AND g_ownerOfBunkerPropertyIAmIn != INVALID_PLAYER_INDEX()
	AND (IS_BIT_SET(playerBD[NATIVE_TO_INT(g_ownerOfBunkerPropertyIAmIn)].iBS, BS_BUNKER_MOVE_OUT_OF_WAY)
	OR IS_BIT_SET(serverBD.iBS, BS_BUNKER_SERVER_DATA_CLEAR_PV_SPACE))
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			INT i
			REPEAT NUMBER_OF_BUNKER_VEHICLES i
				IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niBunkerVehicles[i])
				AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niBunkerVehicles[i])
				AND IS_ENTITY_IN_ANGLED_AREA(NET_TO_VEH(serverBD.niBunkerVehicles[i]), <<883.123779,-3243.632813,-99.273224>>, <<883.033020,-3236.130615,-96.536865>>, 7.5)
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niBunkerVehicles[i])
						IF IS_VEHICLE_EMPTY(NET_TO_VEH(serverBD.niBunkerVehicles[i]))
							DELETE_NET_ID(serverBD.niBunkerVehicles[i])
						ENDIF
					ELSE
						NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.niBunkerVehicles[i])
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
	
	IF PLAYER_ID() != INVALID_PLAYER_INDEX()
	AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
	AND g_ownerOfBunkerPropertyIAmIn != INVALID_PLAYER_INDEX()
	AND IS_BIT_SET(serverBD.iBS , BS_BUNKER_SERVER_DATA_REQUEST_TO_MOVE_OUT_OF_WAY_FOR_TRUCK)
		IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) != PERFORMING_TASK
		AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) != WAITING_TO_START_TASK
		AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<853.945435,-3244.955811,-99.644547>>, <<832.867493,-3233.418701,-95.699135>>, 7.062500)
			VECTOR vPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
			
			IF vPosition.x <= 842.0
				IF vPosition.y <= -3236.0
					vPosition = <<833.6961, -3241.8953, -99.6991>>
				ELSE
					vPosition = <<837.2765, -3228.4016, -99.0898>>
				ENDIF
			ELSE
				IF vPosition.y <= -3236.0
					vPosition = <<846.5422, -3246.1133, -99.6887>>
				ELSE
					vPosition = <<853.2507, -3233.4424, -99.6606>>
				ENDIF
			ENDIF
			
			TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), vPosition, PEDMOVEBLENDRATIO_WALK, DEFAULT, DEFAULT, 3.0)
		ENDIF
		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			INT i
			REPEAT NUMBER_OF_BUNKER_VEHICLES i
				IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niBunkerVehicles[i])
				AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niBunkerVehicles[i])
				AND IS_ENTITY_IN_ANGLED_AREA(NET_TO_VEH(serverBD.niBunkerVehicles[i]), <<853.945435,-3244.955811,-99.644547>>, <<832.867493,-3233.418701,-95.699135>>, 7.062500)
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niBunkerVehicles[i])
						IF IS_VEHICLE_EMPTY(NET_TO_VEH(serverBD.niBunkerVehicles[i]))
							DELETE_NET_ID(serverBD.niBunkerVehicles[i])
						ENDIF
					ELSE
						NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.niBunkerVehicles[i])
					ENDIF
				ENDIF
			ENDREPEAT
			
			IF DOES_ENTITY_EXIST(MPGlobals.RemotePV[NATIVE_TO_INT(thisBunker.pOwner)])
			AND IS_ENTITY_IN_ANGLED_AREA(MPGlobals.RemotePV[NATIVE_TO_INT(thisBunker.pOwner)], <<853.945435,-3244.955811,-199.644547>>, <<832.867493,-3233.418701,-95.699135>>, 7.062500)
			AND IS_SAFE_TO_SPAWN_PV(TRUE)
				IF NETWORK_HAS_CONTROL_OF_ENTITY(MPGlobals.RemotePV[NATIVE_TO_INT(thisBunker.pOwner)])
					IF IS_VEHICLE_DRIVEABLE(MPGlobals.RemotePV[NATIVE_TO_INT(thisBunker.pOwner)])
					AND IS_VEHICLE_EMPTY(MPGlobals.RemotePV[NATIVE_TO_INT(thisBunker.pOwner)])
						SET_ENTITY_COORDS(MPGlobals.RemotePV[NATIVE_TO_INT(thisBunker.pOwner)], <<883.0696, -3240.5024, -99.2772>>)
						SET_ENTITY_HEADING(MPGlobals.RemotePV[NATIVE_TO_INT(thisBunker.pOwner)], 180.0)
						SET_VEHICLE_ON_GROUND_PROPERLY(MPGlobals.RemotePV[NATIVE_TO_INT(thisBunker.pOwner)])
					ENDIF
				ELSE
					NETWORK_REQUEST_CONTROL_OF_ENTITY(MPGlobals.RemotePV[NATIVE_TO_INT(thisBunker.pOwner)])
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(MPGlobals.RemotePegV[NATIVE_TO_INT(thisBunker.pOwner)])
			AND IS_ENTITY_IN_ANGLED_AREA(MPGlobals.RemotePegV[NATIVE_TO_INT(thisBunker.pOwner)], <<853.945435,-3244.955811,-199.644547>>, <<832.867493,-3233.418701,-95.699135>>, 7.062500)
			AND IS_SAFE_TO_SPAWN_PV(TRUE)
				IF NETWORK_HAS_CONTROL_OF_ENTITY(MPGlobals.RemotePegV[NATIVE_TO_INT(thisBunker.pOwner)])
					IF IS_VEHICLE_DRIVEABLE(MPGlobals.RemotePegV[NATIVE_TO_INT(thisBunker.pOwner)])
					AND IS_VEHICLE_EMPTY(MPGlobals.RemotePegV[NATIVE_TO_INT(thisBunker.pOwner)])
						SET_ENTITY_COORDS(MPGlobals.RemotePegV[NATIVE_TO_INT(thisBunker.pOwner)], <<883.0696, -3240.5024, -99.2772>>)
						SET_ENTITY_HEADING(MPGlobals.RemotePegV[NATIVE_TO_INT(thisBunker.pOwner)], 180.0)
						SET_VEHICLE_ON_GROUND_PROPERLY(MPGlobals.RemotePegV[NATIVE_TO_INT(thisBunker.pOwner)])
					ENDIF
				ELSE
					NETWORK_REQUEST_CONTROL_OF_ENTITY(MPGlobals.RemotePegV[NATIVE_TO_INT(thisBunker.pOwner)])
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_TRUCK_SPAWN_SAFETY_CHECK()
	IF IS_BIT_SET(serverBD.iBS , BS_BUNKER_SERVER_DATA_REQUEST_TO_MOVE_OUT_OF_WAY_FOR_TRUCK)
		IF IS_SAFE_TO_SPAWN_TRUCK()
			IF NOT IS_BIT_SET(serverBD.iBS, BS_BUNKER_SERVER_DATA_AREA_IS_SAFE_TO_SPAWN_TRUCK)
				SET_BIT(serverBD.iBS, BS_BUNKER_SERVER_DATA_AREA_IS_SAFE_TO_SPAWN_TRUCK)
				CLEAR_BIT(serverBD.iBS, BS_BUNKER_SERVER_DATA_CLEAR_PV_SPACE)
				PRINTLN("[BUNKER_SCRIPT] MAINTAIN_TRUCK_SPAWN_SAFETY_CHECK - BS_BUNKER_SERVER_DATA_AREA_IS_SAFE_TO_SPAWN_TRUCK TRUE")
			ENDIF	
		ELSE
			IF IS_BIT_SET(serverBD.iBS, BS_BUNKER_SERVER_DATA_AREA_IS_SAFE_TO_SPAWN_TRUCK)
				CLEAR_BIT(serverBD.iBS, BS_BUNKER_SERVER_DATA_AREA_IS_SAFE_TO_SPAWN_TRUCK)
				PRINTLN("[BUNKER_SCRIPT] MAINTAIN_TRUCK_SPAWN_SAFETY_CHECK - BS_BUNKER_SERVER_DATA_AREA_IS_SAFE_TO_SPAWN_TRUCK FALSE")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_WANTED_LEVEL()
	IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
		PRINTLN("AM_MP_BUNKER - MAINTAIN_WANTED_LEVEL - Resetting wanted level")
		
		SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
	ENDIF
ENDPROC

PROC MAINTAIN_SHOOTING_RANGE_UPGRADE()
	g_bShootingRangeUpgraded = DOES_PLAYER_OWN_FACTORY_UPGRADE(thisBunker.pOwner, thisBunker.eID, UPGRADE_ID_EQUIPMENT)
ENDPROC

PROC REQUEST_BUNKER_INTRO_CUTSCENE()
	/*
	Scene play order:
	(from //gta5_dlc/art/animation/Cutscene/Renders/Gunrunning/BUNK_INT.mov)
	
	bunk_int_p1_t06
	bunk_int_p2_t03 - Purchased Mobile Unit
	bunk_int_p2_A1_t03 - Not purchased Mobile unit
	bunk_int_p4_t04 - purchased range
	bunk_int_p4_A1_t04 - not purchased range
	bunk_int_p5_t02
	bunk_int_p6_t06
	bunk_int_p7_t03
	bunk_int_p8_t06
	*/
	
	//-- Have I purchased a mobile unit?
	BOOL bPurchasedMobileUnit  
	
	IF IS_PLAYER_GUNRUNNING_TRUCK_PURCHASED(thisBunker.pOwner) 
		PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE] [REQUEST_BUNKER_INTRO_CUTSCENE] Check bPurchasedMobileUnit, IS_PLAYER_GUNRUNNING_TRUCK_PURCHASED true...")
		IF IS_OWNERS_ARMORY_TRUCK_INSIDE_BUNKER(thisBunker.pOwner)
			PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE] [REQUEST_BUNKER_INTRO_CUTSCENE] Check bPurchasedMobileUnit, IS_OWNERS_ARMORY_TRUCK_INSIDE_BUNKER true - will set")
			bPurchasedMobileUnit = TRUE
		ENDIF
	ENDIF
	
	//-- Have I purchased a Shooting range?
	BOOL bPurchasedShootingRange
	
	IF IS_PLAYER_BUNKER_FIRING_RANGE_WHITE_PURCHASED(thisBunker.pOwner)
		bPurchasedShootingRange = TRUE
		PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE] [REQUEST_BUNKER_INTRO_CUTSCENE] Set bPurchasedShootingRange because IS_PLAYER_BUNKER_FIRING_RANGE_WHITE_PURCHASED")
	ENDIF
	
	IF IS_PLAYER_BUNKER_FIRING_RANGE_BLACK_PURCHASED(thisBunker.pOwner)
		bPurchasedShootingRange = TRUE
		PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE] [REQUEST_BUNKER_INTRO_CUTSCENE] Set bPurchasedShootingRange because IS_PLAYER_BUNKER_FIRING_RANGE_BLACK_PURCHASED")
	ENDIF
	
	IF bPurchasedShootingRange ENDIF
	IF bPurchasedMobileUnit ENDIF
	
//	bPurchasedShootingRange = TRUE
//	bPurchasedMobileUnit = TRUE

	IF (bPurchasedShootingRange AND bPurchasedMobileUnit)
		//-- MOC & SR:
		REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("BUNK_INT", CS_SECTION_1 | CS_SECTION_2 | CS_SECTION_4 | CS_SECTION_6 | CS_SECTION_7 | CS_SECTION_8 | CS_SECTION_9 )
	ELIF (bPurchasedShootingRange AND NOT bPurchasedMobileUnit)
		//-- No MOC, have SR
		REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("BUNK_INT", CS_SECTION_1 | CS_SECTION_3 | CS_SECTION_4 | CS_SECTION_6 | CS_SECTION_7 | CS_SECTION_8 | CS_SECTION_9 )
	ELIF (bPurchasedMobileUnit AND NOT bPurchasedShootingRange)
		//-- MOC, no SR
		REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("BUNK_INT", CS_SECTION_1 | CS_SECTION_2 | CS_SECTION_5 | CS_SECTION_6 | CS_SECTION_7 | CS_SECTION_8 | CS_SECTION_9 )
	ELSE
		//-- No MOC, no SR
		REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("BUNK_INT", CS_SECTION_1 | CS_SECTION_3 | CS_SECTION_5 | CS_SECTION_6 | CS_SECTION_7 | CS_SECTION_8 | CS_SECTION_9 )
	ENDIF 

	PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE] [REQUEST_BUNKER_INTRO_CUTSCENE] Called with bPurchasedMobileUnit: ", bPurchasedMobileUnit, " bPurchasedShootingRange: ", bPurchasedShootingRange)
//	REQUEST_CUTSCENE("BUNK_INT")
	

ENDPROC


PROC UPDATE_FACTORY_PED_RESET_LOGIC()
	#IF IS_DEBUG_BUILD
	IF bResetPeds
		RESET_PED_BUNKER_ACTIVITIES(activityControllerStruct, activityCheck, pedLocalVariationsStruct, interiorStruct, bunkerFactoryActivityStruct, serverBD.serverInteriorStruct)
		bResetPeds = FALSE
	ENDIF
	#ENDIF
ENDPROC


PED_INDEX pedPlayer
FUNC BOOL CREATE_PLAYER_PED_COPY_FOR_BUNKER_INTRO_CUT()
	PED_INDEX pedOwner
	IF NOT DOES_ENTITY_EXIST(pedPlayer)
		IF NETWORK_IS_PLAYER_ACTIVE(thisBunker.pOwner)
			pedOwner = GET_PLAYER_PED(thisBunker.pOwner)
			IF NOT IS_PED_INJURED(pedOwner)
				MODEL_NAMES mPlayer = GET_ENTITY_MODEL(pedOwner)
				pedPlayer = CREATE_PED(PEDTYPE_CIVMALE, mPlayer,  <<883.2854, -3238.6978, -99.2829>>, 62.1074, FALSE, FALSE)
				SET_ENTITY_INVINCIBLE(pedPlayer, TRUE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedPlayer, TRUE)
				CLONE_PED_TO_TARGET(pedOwner, pedPlayer)
		//		SET_ENTITY_VISIBLE(pedPlayer, FALSE)
			ELSE
				PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE] [CREATE_PLAYER_PED_COPY_FOR_BUNKER_INTRO_CUT] Owner injured!")
			ENDIF
		ELSE
			PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE] [CREATE_PLAYER_PED_COPY_FOR_BUNKER_INTRO_CUT] Owner not active!")
		ENDIF
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(pedPlayer)
		RETURN FALSE
	ENDIF
	
	IF NOT IS_PED_INJURED(pedPlayer)
		IF NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedPlayer)
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC INIT_ENTITY_SETS_FOR_INTRO_CUTSCENE()
	IF IS_PLAYER_BUNKER_STYLE_OPTION_A_PURCHASED(thisBunker.pOwner)
		PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE] [INIT_ENTITY_SETS_FOR_INTRO_CUTSCENE] IS_PLAYER_BUNKER_STYLE_OPTION_A_PURCHASED...")
		REMOVE_ENTITY_SET(thisBunker.eID, "bunker_style_b")
		REMOVE_ENTITY_SET(thisBunker.eID, "bunker_style_c")
		ADD_ENTITY_SET(thisBunker.eID, "bunker_style_a")
	ENDIF
	
	IF IS_PLAYER_BUNKER_STYLE_OPTION_B_PURCHASED(thisBunker.pOwner)
		PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE] [INIT_ENTITY_SETS_FOR_INTRO_CUTSCENE] IS_PLAYER_BUNKER_STYLE_OPTION_B_PURCHASED...")
		REMOVE_ENTITY_SET(thisBunker.eID, "bunker_style_a")
		REMOVE_ENTITY_SET(thisBunker.eID, "bunker_style_c")
		ADD_ENTITY_SET(thisBunker.eID, "bunker_style_b")
	ENDIF
	
	IF IS_PLAYER_BUNKER_STYLE_OPTION_C_PURCHASED(thisBunker.pOwner)
		PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE] [INIT_ENTITY_SETS_FOR_INTRO_CUTSCENE] IS_PLAYER_BUNKER_STYLE_OPTION_C_PURCHASED...")
		REMOVE_ENTITY_SET(thisBunker.eID, "bunker_style_a")
		REMOVE_ENTITY_SET(thisBunker.eID, "bunker_style_b")
		ADD_ENTITY_SET(thisBunker.eID, "bunker_style_c")
	ENDIF
	

	PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE] [INIT_ENTITY_SETS_FOR_INTRO_CUTSCENE] gun_range_lights set...")
	REMOVE_ENTITY_SET(thisBunker.eID, "gun_range_blocker_set")
	REMOVE_ENTITY_SET(thisBunker.eID, "gun_wall_blocker")
	REMOVE_ENTITY_SET(thisBunker.eID, "security_upgrade")
	ADD_ENTITY_SET(thisBunker.eID, "standard_security_set")
	ADD_ENTITY_SET(thisBunker.eID, "gun_range_lights")
	
	ADD_ENTITY_SET(thisBunker.eID, "Gun_schematic_set")
	
	REFRESH_INTERIOR(thisBunker.BunkerInteriorID)
ENDPROC

PROC RESET_ENTITY_SETS_FOR_INTRO_CUTSCENE(BOOL &bShouldRefresh)
	
	
	PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE] [RESET_ENTITY_SETS_FOR_INTRO_CUTSCENE] Called....") 
	IF NOT IS_PLAYER_BUNKER_FIRING_RANGE_BLACK_PURCHASED(thisBunker.pOwner)
	AND NOT IS_PLAYER_BUNKER_FIRING_RANGE_WHITE_PURCHASED(thisBunker.pOwner)
		ADD_ENTITY_SET(thisBunker.eID, "gun_range_blocker_set")
		ADD_ENTITY_SET(thisBunker.eID, "gun_wall_blocker")
		REMOVE_ENTITY_SET(thisBunker.eID, "gun_range_lights")
		bShouldRefresh = TRUE
		PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE] [RESET_ENTITY_SETS_FOR_INTRO_CUTSCENE] Added blocker")
	ENDIF
	
	IF GET_HOST_ASSIGNED_PRODUCTION_MODE() = PRODUCTION_MODE_GOODS
		REMOVE_ENTITY_SET(thisBunker.eID, "Gun_schematic_set")
		bShouldRefresh = TRUE
		PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE] [RESET_ENTITY_SETS_FOR_INTRO_CUTSCENE] removed Gun_schematic_set")
	ENDIF
	
	IF DOES_PLAYER_OWN_FACTORY_UPGRADE(thisBunker.pOwner, thisBunker.eID, UPGRADE_ID_SECURITY)
		REMOVE_ENTITY_SET(thisBunker.eID, "standard_security_set")
		ADD_ENTITY_SET(thisBunker.eID, "security_upgrade")
		PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE] [RESET_ENTITY_SETS_FOR_INTRO_CUTSCENE] added security_upgrade")
	ENDIF
	
	IF bShouldRefresh
		PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE] [RESET_ENTITY_SETS_FOR_INTRO_CUTSCENE] REFRESH_INTERIOR")
		REFRESH_INTERIOR(thisBunker.BunkerInteriorID)
	ENDIF
ENDPROC

FUNC BOOL REQUEST_AND_LOAD_MOC_MODELS_FOR_INTRO_CUTSCENE()
	BOOL bShouldRequestModels
	
	IF IS_PLAYER_GUNRUNNING_TRUCK_PURCHASED(thisBunker.pOwner) 
		IF IS_OWNERS_ARMORY_TRUCK_INSIDE_BUNKER(thisBunker.pOwner)
			bShouldRequestModels = TRUE
		ENDIF
	ENDIF
	
	IF bShouldRequestModels
		MODEL_NAMES mTruck = GET_PLAYERS_GUNRUNNING_TRUCK_MODEL(thisBunker.pOwner)
		MODEL_NAMES mTrailer = TRAILERLARGE
		
		REQUEST_MODEL(mTruck)
		REQUEST_MODEL(mTrailer)
		
		IF NOT HAS_MODEL_LOADED(mTruck)
			PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE] [REQUEST_AND_LOAD_MOC_MODELS_FOR_INTRO_CUTSCENE] Waiting for truck model")
			RETURN FALSE
		ENDIF
		
		IF NOT HAS_MODEL_LOADED(mTrailer)
			PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE] [REQUEST_AND_LOAD_MOC_MODELS_FOR_INTRO_CUTSCENE] Waiting for trailer model")
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
	
ENDFUNC


FUNC BOOL CREATE_MOC_FOR_INTRO_CUTSCENE()
	BOOL bShouldCreateMoc
	
	//-- Coords taken from MAINTAIN_SPAWN_TRUCK_IN_BUNKER() in am_vehicle_spawn.sc
	VECTOR vSpawnCoord = <<834.2265, -3234.7949, -98.4865>>
	FLOAT fHead = 62.2800	
	
	IF IS_PLAYER_GUNRUNNING_TRUCK_PURCHASED(thisBunker.pOwner) 
		IF IS_OWNERS_ARMORY_TRUCK_INSIDE_BUNKER(thisBunker.pOwner)
			bShouldCreateMoc = TRUE
		ENDIF
	ENDIF
	
	IF bShouldCreateMoc
		IF DOES_ENTITY_EXIST(thisBunker.vehIntroCutMocTruck)
		AND DOES_ENTITY_EXIST(thisBunker.vehIntroCutMocTrailer)
			RETURN TRUE
		ENDIF
		IF REQUEST_AND_LOAD_MOC_MODELS_FOR_INTRO_CUTSCENE()
		
			// Create truck cabin
			IF NOT DOES_ENTITY_EXIST(thisBunker.vehIntroCutMocTruck)
				thisBunker.vehIntroCutMocTruck = CREATE_VEHICLE(GET_PLAYERS_GUNRUNNING_TRUCK_MODEL(thisBunker.pOwner), vSpawnCoord, fHead, FALSE, FALSE) 
				FREEZE_ENTITY_POSITION(thisBunker.vehIntroCutMocTruck, TRUE)
				PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE] [CREATE_MOC_FOR_INTRO_CUTSCENE] Created truck")
			ENDIF
			
			// Create trailer
			IF NOT DOES_ENTITY_EXIST(thisBunker.vehIntroCutMocTrailer)
				thisBunker.vehIntroCutMocTrailer = CREATE_VEHICLE(TRAILERLARGE,  <<842.6267, -3239.2168, -96.8499>>, fHead, FALSE, FALSE) 
				SET_TRAILER_LEGS_RAISED(thisBunker.vehIntroCutMocTrailer)
				FREEZE_ENTITY_POSITION(thisBunker.vehIntroCutMocTrailer, TRUE)
				PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE] [CREATE_MOC_FOR_INTRO_CUTSCENE] Created trailer")
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(thisBunker.vehIntroCutMocTruck)
				RETURN FALSE
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(thisBunker.vehIntroCutMocTrailer)
				RETURN FALSE
			ENDIF
			
			ATTACH_VEHICLE_TO_TRAILER(thisBunker.vehIntroCutMocTruck, thisBunker.vehIntroCutMocTrailer, 0.0)
			
			IF DOES_ENTITY_EXIST(thisBunker.vehIntroCutMocTruck)
				PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE] [CREATE_MOC_FOR_INTRO_CUTSCENE] Intro Cutscene clone vehIntroCutMocTruck - collision disabled.")
				SET_ENTITY_COLLISION(thisBunker.vehIntroCutMocTruck, FALSE)
			ENDIF
			
			IF DOES_ENTITY_EXIST(thisBunker.vehIntroCutMocTrailer)
				PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE] [CREATE_MOC_FOR_INTRO_CUTSCENE] Intro Cutscene clone vehIntroCutMocTrailer - collision disabled.")
				SET_ENTITY_COLLISION(thisBunker.vehIntroCutMocTrailer, FALSE)
			ENDIF
			
			// Setup trailer, as per SET_COMMON_PROPERTIES_FOR_TRUCK_VEHICLE()
			IF DOES_ENTITY_EXIST(MPGlobalsAmbience.vehTruckVehicle[1])
				IF NOT IS_ENTITY_DEAD(MPGlobalsAmbience.vehTruckVehicle[1])
					IF GET_NUM_MOD_KITS(MPGlobalsAmbience.vehTruckVehicle[1]) > 0
						PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE] [CREATE_MOC_FOR_INTRO_CUTSCENE] SET_VEHICLE_MOD_KIT")
						SET_VEHICLE_MOD_KIT(thisBunker.vehIntroCutMocTrailer, 0)
					ENDIF
					
					VEHICLE_SETUP_STRUCT_MP sData
					GET_VEHICLE_SETUP_MP(MPGlobalsAmbience.vehTruckVehicle[1], sData)
					SET_GR_TRAILER_SETUP_MP(thisBunker.vehIntroCutMocTrailer, sData)
					SET_VEHICLE_SETUP_MP(thisBunker.vehIntroCutMocTrailer, sData)
					
					IF GET_MP_TRUCK_MOD_STAT(TRUCK_MOD_TRAILER_MOD_ROOF) != 2
						sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
					ELSE
						sData.VehicleSetup.iModIndex[MOD_ROOF] = 2
					ENDIF
		
					SET_VEHICLE_MOD(thisBunker.vehIntroCutMocTrailer,MOD_ROOF, sData.VehicleSetup.iModIndex[MOD_ROOF])
				ENDIF
			ELSE
				PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE] [CREATE_MOC_FOR_INTRO_CUTSCENE] MPGlobalsAmbience.vehTruckVehicle[1] Doesn't exist!")
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC


PROC MAINTAIN_MOC_IN_BUNKER_FOR_INTRO_CUT()
	INTERIOR_INSTANCE_INDEX interiorTruck
	IF NOT thisBunker.bForceRoomForIntroCut
	//	PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE] [MAINTAIN_MOC_IN_BUNKER_FOR_INTRO_CUT] 1")
		IF DOES_ENTITY_EXIST(thisBunker.vehIntroCutMocTruck)
		AND DOES_ENTITY_EXIST(thisBunker.vehIntroCutMocTrailer)
	//		PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE] [MAINTAIN_MOC_IN_BUNKER_FOR_INTRO_CUT] 2")
			IF NOT IS_ENTITY_DEAD(thisBunker.vehIntroCutMocTruck)
	//			PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE] [MAINTAIN_MOC_IN_BUNKER_FOR_INTRO_CUT] 3")
				IF NOT IS_ENTITY_DEAD(thisBunker.vehIntroCutMocTrailer)
					PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE] [MAINTAIN_MOC_IN_BUNKER_FOR_INTRO_CUT] 4")
					interiorTruck = GET_INTERIOR_FROM_ENTITY(thisBunker.vehIntroCutMocTruck)
				//	interiorTrailer = GET_INTERIOR_FROM_ENTITY(thisBunker.vehIntroCutMocTrailer)
					IF IS_VALID_INTERIOR(interiorTruck)
	//					PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE] [MAINTAIN_MOC_IN_BUNKER_FOR_INTRO_CUT] 5")
					//	IF IS_VALID_INTERIOR(interiorTrailer)
	//						PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE] [MAINTAIN_MOC_IN_BUNKER_FOR_INTRO_CUT] 6")
							IF IS_INTERIOR_READY(interiorTruck)
	//							PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE] [MAINTAIN_MOC_IN_BUNKER_FOR_INTRO_CUT] 7")
							//	IF IS_INTERIOR_READY(interiorTrailer)
	//								PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE] [MAINTAIN_MOC_IN_BUNKER_FOR_INTRO_CUT] 8")
									FORCE_ROOM_FOR_ENTITY(thisBunker.vehIntroCutMocTruck, interiorTruck, GET_HASH_KEY("bunker_ModRoom"))
									FORCE_ROOM_FOR_ENTITY(thisBunker.vehIntroCutMocTrailer, interiorTruck, GET_HASH_KEY("bunker_ModRoom"))	
									thisBunker.bForceRoomForIntroCut = TRUE
									PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE] [MAINTAIN_MOC_IN_BUNKER_FOR_INTRO_CUT] Forced room")
							//	ENDIF
							ENDIF
					//	ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CLEANUP_MOC_FOR_INTRO_CUTSCENE()
	ENTITY_INDEX ent
	IF DOES_ENTITY_EXIST(thisBunker.vehIntroCutMocTruck)
		ent = thisBunker.vehIntroCutMocTruck
		DELETE_ENTITY(ent)
	ENDIF
	
	IF DOES_ENTITY_EXIST(thisBunker.vehIntroCutMocTrailer)
		ent = thisBunker.vehIntroCutMocTrailer
		DELETE_ENTITY(ent)
	ENDIF
ENDPROC

PROC INIT_LOCAL_PLAYER_FOR_INTRO_MOCAP()
	IF NOT thisBunker.bInitLocalPlayerForIntroMocap
		SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iBS, BS_BUNKER_WATCHING_INTRO_MOCAP)
		
		SET_LOCAL_PLAYER_WATCHING_BUNKER_INTRO_MOCAP()
		
		MAKE_PLAYER_SAFE_FOR_MP_CUTSCENE(FALSE, TRUE, TRUE, FALSE )
		
		NETWORK_SET_VOICE_ACTIVE(FALSE)
		PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE] [INIT_LOCAL_PLAYER_FOR_INTRO_MOCAP] Set")
		thisBunker.bInitLocalPlayerForIntroMocap = TRUE
	ENDIF
ENDPROC

PROC CLEANUP_INTRO_CUTSCENE()
	REMOVE_MODEL_HIDE(<<897.0294, -3248.4165, -99.29>>, 5.0, INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_tunnel_gate")))
	REMOVE_MODEL_HIDE(<<855.74, -3230.87, -99>>, 5.0, INT_TO_ENUM(MODEL_NAMES, HASH("gr_bunker_bollards2")))
	IF IS_NEW_LOAD_SCENE_ACTIVE()
		NEW_LOAD_SCENE_STOP()	
	ENDIF
	
	IF IS_CUTSCENE_PLAYING()
		STOP_CUTSCENE_IMMEDIATELY()
		REMOVE_CUTSCENE()
	ENDIF
	
	CLEANUP_MOC_FOR_INTRO_CUTSCENE()
	
	CLEANUP_MP_CUTSCENE(DEFAULT, TRUE)
	
	NETWORK_SET_VOICE_ACTIVE(TRUE)
	
	CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iBS, BS_BUNKER_WATCHING_INTRO_MOCAP)
	
	CLEAR_LOCAL_PLAYER_WATCHING_BUNKER_INTRO_MOCAP()
	
	//-- Remove crates
	CLEANUP_ALL_FACTORY_PRODUCTS(thisBunker.eID)
	thisBunker.iProductLayout = 0
	
	//-- Delete temp cutscene models
	ENTITY_INDEX ent
	ent = pedPlayer
	IF DOES_ENTITY_EXIST(ent)
		DELETE_ENTITY( ent)
	ENDIF

	thisBunker.bShouldPlayIntroCut = false
ENDPROC

FUNC BOOL RUN_BUNKER_INTRO_CUTSCENE()

	IF (thisBunker.iIntroCutsceneProg = 1
	AND HAS_SIMPLE_INTERIOR_CHILD_SCRIPT_TIMED_OUT())
	OR ((IS_SKYSWOOP_MOVING() OR IS_SKYSWOOP_IN_SKY()) AND NOT IS_SKYSWOOP_AT_GROUND())
	OR IS_ANY_TRANSITION_SESSION_INVITE_BEING_ACTIONED()  
	#IF IS_DEBUG_BUILD
	OR IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED()
	#ENDIF
		
		#IF IS_DEBUG_BUILD
		IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED()
			IF thisBunker.pOwner = PLAYER_ID()
				SET_LOCAL_PLAYER_COMPLETED_BUNKER_INTRO_CUTSCENE(TRUE)
			ENDIF
		ENDIF
		#ENDIF
		
		CLEANUP_INTRO_CUTSCENE()
		CDEBUG1LN(DEBUG_SMPL_INTERIOR, "AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE] Child script is ready, mocap has not played yet, cleaning up mocap.") 
		RETURN TRUE
	ENDIF
	
	BOOL bProductCreated = FALSE

	SWITCH thisBunker.iIntroCutsceneProg
		CASE 0
			INIT_LOCAL_PLAYER_FOR_INTRO_MOCAP()
			
			//--Add crates
			thisBunker.iProductLayout = 1
			CREATE_ALL_FACTORY_PRODUCTS(thisBunker.eID) 

			//-- Need to remove specific crates as they're blocking camera shots
			INT iProductSlot
			VECTOR vProductCoords
			REPEAT GET_FACTORY_PRODUCT_CAPACITY(thisBunker.eID) iProductSlot
				IF IS_FACTORY_PRODUCT_SLOT_VALID(thisBunker.eID, iProductSlot)
					vProductCoords 		= GET_FACTORY_PRODUCT_COORDS(thisBunker.pOwner, thisBunker.eID, iProductSlot)
					IF ARE_VECTORS_EQUAL(vProductCoords, <<933.080,		-3219.950,	-99.290>>)
						PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE] Removing product ", iProductSlot) 
						CLEANUP_FACTORY_PRODUCT(iProductSlot)
					ENDIF
				ENDIF
			ENDREPEAT
			
			IF thisBunker.iTotalProductCreated = GET_FACTORY_PRODUCT_CAPACITY(thisBunker.eID)
				bProductCreated = TRUE
				PRINTLN("AM_MP_BUNKER [RUN_BUNKER_INTRO_CUTSCENE] - All product crates have been created, bProductCreated = ", BOOL_TO_INT(bProductCreated))
			ELIF thisBunker.iFallOverAttempts >= 3
				bProductCreated = TRUE
				PRINTLN("AM_MP_BUNKER [RUN_BUNKER_INTRO_CUTSCENE] - Falling over when creating product crates, forcing bProductCreated = TRUE")
			ELSE
				PRINTLN("AM_MP_BUNKER [RUN_BUNKER_INTRO_CUTSCENE] Waiting on all factory product crates being created.")
			ENDIF

			IF bProductCreated	
				thisBunker.iIntroCutsceneProg++
				PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE] thisBunker.iIntroCutsceneProg = ", thisBunker.iIntroCutsceneProg)
			ENDIF
		BREAK
		
		CASE 1
			IF CREATE_PLAYER_PED_COPY_FOR_BUNKER_INTRO_CUT()
				IF CREATE_MOC_FOR_INTRO_CUTSCENE()
					IF IS_SCREEN_FADED_OUT()
						CREATE_MODEL_HIDE(<<897.0294, -3248.4165, -99.29>>, 5.0, INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_tunnel_gate")), TRUE)
						CREATE_MODEL_HIDE(<<855.74, -3230.87, -99>>, 5.0, INT_TO_ENUM(MODEL_NAMES, HASH("gr_bunker_bollards2")), TRUE)
						
						REQUEST_BUNKER_INTRO_CUTSCENE()
						
						//--Turn on appropriate lights / props for this bunker
						INIT_ENTITY_SETS_FOR_INTRO_CUTSCENE()
						
					
						thisBunker.iIntroCutsceneProg++
						PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE] thisBunker.iIntroCutsceneProg = ", thisBunker.iIntroCutsceneProg)
					ELSE
						PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE] Waiting for IS_SCREEN_FADED_OUT")
					ENDIF
				ENDIF
			ELSE
				PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE] Waiting for CREATE_PLAYER_PED_COPY_FOR_BUNKER_INTRO_CUT")
			ENDIF
		BREAK
		
		CASE 2
			IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
				SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_1", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
				thisBunker.iIntroCutsceneProg++
				PRINTLN("[IE_WAREHOUSE_SCRIPT] [RUN_IE_INTRO_CUTSCENE] thisBunker.iIntroCutsceneProg = ", thisBunker.iIntroCutsceneProg)
			ENDIF
		BREAK
		
		CASE 3
			IF HAS_CUTSCENE_LOADED() 
				IF NOT IS_PED_INJURED(pedPlayer)
					PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE] Registered pedPlayer for cut")
					REGISTER_ENTITY_FOR_CUTSCENE(pedPlayer, "MP_1", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
				ENDIF
				
				START_CUTSCENE()
				
				START_MP_CUTSCENE()
				thisBunker.iIntroCutsceneProg++
				PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE] thisBunker.iIntroCutsceneProg = ", thisBunker.iIntroCutsceneProg)
			ELSE
				PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE] Waitng for HAS_CUTSCENE_LOADED")
			ENDIF
		BREAK
		
		CASE 4
			IF IS_CUTSCENE_PLAYING()
				IF IS_SCREEN_FADED_OUT()					
					DO_SCREEN_FADE_IN(500)
				ENDIF
				
				
				thisBunker.iIntroCutsceneProg++
				PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE] thisBunker.iIntroCutsceneProg = ", thisBunker.iIntroCutsceneProg, " GET_CUTSCENE_TOTAL_DURATION(): ", GET_CUTSCENE_TOTAL_DURATION())
			ELSE
				PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE] Waitng for IS_CUTSCENE_PLAYING")
			ENDIF
		BREAK
		
		CASE 5
			IF NOT HAS_NET_TIMER_STARTED(thisBunker.stCutsceneFinishedFailSafe)
				PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE]- thisBunker.stCutsceneFinishedFailSafe, started.")
				START_NET_TIMER(thisBunker.stCutsceneFinishedFailSafe)
			ENDIF
				
			IF HAS_CUTSCENE_FINISHED() 
			OR NOT IS_CUTSCENE_PLAYING()
			OR HAS_NET_TIMER_EXPIRED(thisBunker.stCutsceneFinishedFailSafe, 279000) //Actual duaration - 269100
				PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE] - HAS_CUTSCENE_FINISHED: ", HAS_CUTSCENE_FINISHED(), " IS_CUTSCENE_PLAYING: ", IS_CUTSCENE_PLAYING(), " Timer Exp: ", HAS_NET_TIMER_EXPIRED(thisBunker.stCutsceneFinishedFailSafe, 274000))
				IF NOT IS_SCREEN_FADED_OUT()
				AND NOT IS_SCREEN_FADING_OUT()
					DO_SCREEN_FADE_OUT(0)
					PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE]- Cutscene finished, but the screen is not faded out, fading out.")
				ENDIF
				
				IF NOT HAS_NET_TIMER_STARTED(thisBunker.timeFailSafe)
					PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE]- thisBunker.timeFailSafe, started.")
					START_NET_TIMER(thisBunker.timeFailSafe)
				ENDIF
				
				IF IS_SCREEN_FADED_OUT()
				OR HAS_NET_TIMER_EXPIRED(thisBunker.timeFailSafe, 5000)
					PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE] EXECUTE - IS_SCREEN_FADED_OUT: ", IS_SCREEN_FADED_OUT(), " thisBunker.timeFailSafe: ", ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), thisBunker.timeFailSafe.Timer)))
					
					RESET_NET_TIMER((thisBunker.timeFailSafe))
					
					BOOL bInteriorWasRefreshed
					
					//-- Turn off gun range lights
					RESET_ENTITY_SETS_FOR_INTRO_CUTSCENE(bInteriorWasRefreshed)
					
					IF bInteriorWasRefreshed
						//-- Will need to wait for REFRESH_INTERIOR
						START_NET_TIMER(thisBunker.timeFailSafe)
						PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE] reset timer as bInteriorWasRefreshed")
					ENDIF
					
					RESET_NET_TIMER(thisBunker.stCutsceneFinishedFailSafe)
					
					thisBunker.iIntroCutsceneProg++
					PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE] thisBunker.iIntroCutsceneProg = ", thisBunker.iIntroCutsceneProg)
					
				ELSE
					PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE] WAITING FOR IS_SCREEN_FADED_OUT: ", IS_SCREEN_FADED_OUT(), " thisBunker.timeFailSafe: ", ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), thisBunker.timeFailSafe.Timer)))
				ENDIF
			ELSE
				IF NOT IS_SCREEN_FADED_OUT()
				AND NOT IS_SCREEN_FADING_OUT()
				//	IF GET_CUTSCENE_TOTAL_DURATION() - GET_CUTSCENE_TIME() <= 1000
					ENTITY_INDEX tempEntity
					tempEntity = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("MP_1")
					IF DOES_ENTITY_EXIST(tempEntity)
						IF NOT IS_ENTITY_DEAD(tempEntity)	
							IF HAS_ANIM_EVENT_FIRED(tempEntity, GET_HASH_KEY("fade")) 
								DO_SCREEN_FADE_OUT(1000)
								PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE] Fading out - 1000")
							ENDIF
						ELSE
							PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE] tempEntity - is dead")
						ENDIF
					ELSE
						PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE] tempEntity - does not exist")
					ENDIF
				#IF IS_DEBUG_BUILD
				ELSE
					IF GET_FRAME_COUNT() % 30 = 0
						PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE] - HAS_CUTSCENE_FINISHED ELSE: ", HAS_CUTSCENE_FINISHED(), " IS_CUTSCENE_PLAYING: ", IS_CUTSCENE_PLAYING(), " Timer Exp: ", HAS_NET_TIMER_EXPIRED(thisBunker.stCutsceneFinishedFailSafe, 274000))
						PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE] Screen fade state, IS_SCREEN_FADED_OUT: ", IS_SCREEN_FADED_OUT(), " IS_SCREEN_FADING_OUT: ", IS_SCREEN_FADING_OUT())
					ENDIF
				#ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 6
			IF NOT HAS_NET_TIMER_STARTED(thisBunker.timeFailSafe)
			OR (HAS_NET_TIMER_EXPIRED(thisBunker.timeFailSafe, 1000) AND IS_INTERIOR_READY(thisBunker.BunkerInteriorID))
				
				CLEANUP_INTRO_CUTSCENE()
				
				thisBunker.iTotalProductCreated = 0
				thisBunker.iFallOverAttempts = 0
				
				//-- Set stat
				#IF IS_DEBUG_BUILD
				IF NOT GET_COMMANDLINE_PARAM_EXISTS("sc_NoGrIntroCutSave")
				#ENDIF
				
			//	IF thisBunker.pOwner = PLAYER_ID() // Only mark as seen for the boss					
					SET_MP_SPAWN_POINT_SETTING(MP_SETTING_SPAWN_BUNKER)
					SET_LOCAL_PLAYER_COMPLETED_BUNKER_INTRO_CUTSCENE(TRUE)
					PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE] Setting MP_STAT_FM_CUT_DONE on Boss. and MP_SETTING_SPAWN_BUNKER")
			//	ENDIF
				
				#IF IS_DEBUG_BUILD
				ENDIF
				#ENDIF
				
				IF IS_SCREEN_FADED_OUT()					
					DO_SCREEN_FADE_IN(500)
				ENDIF
			
				thisBunker.bShouldPlayIntroCut = false
				PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE] ******* FINISHED ******")
				RETURN TRUE
			ELSE
				PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE] WAITING FOR REFRESH")
			ENDIF
		BREAK
	ENDSWITCH
	
	MAINTAIN_MOC_IN_BUNKER_FOR_INTRO_CUT()
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_BUNKER_OWNER_WATCHING_INTRO_MOCAP()
	IF thisBunker.pOwner != INVALID_PLAYER_INDEX()
	AND NETWORK_IS_PLAYER_ACTIVE(thisBunker.pOwner)
		IF IS_BIT_SET(playerBD[NATIVE_TO_INT(thisBunker.pOwner)].iBS, BS_BUNKER_WATCHING_INTRO_MOCAP)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_SHOULD_START_INTRO_CUSTCENE()
	IF IS_BUNKER_STATE(BUNKER_STATE_PLAYING_INTRO_CUTSCENE)
		EXIT
	ENDIF
	IF IS_TRANSITION_ACTIVE()
		EXIT
	ENDIF
	
	BOOL bShouldCheck
	BOOL bDoneIntroCut
	
	
	IF IS_LOCAL_PLAYER_WALKING_INTO_SIMPLE_INTERIOR()
		IF NOT HAS_SIMPLE_INTERIOR_CHILD_SCRIPT_TIMED_OUT()
		AND IS_BUNKER_STATE(BUNKER_STATE_LOADING)
			bShouldCheck = TRUE
		ENDIF
	ENDIF
	
	IF bShouldCheck
		IF thisBunker.iIntroCutsceneProg = 0
			IF thisBunker.pOwner = PLAYER_ID()
				//-- I'm the owner of the bunker - play if I haven't seen it already
				bDoneIntroCut = HAS_LOCAL_PLAYER_VIEWED_BUNKER_INTRO_CUTSCENE()
				IF NOT bDoneIntroCut
					//PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE] [MAINTAIN_SHOULD_START_INTRO_CUSTCENE] SHould play - I'm owner")
					INIT_LOCAL_PLAYER_FOR_INTRO_MOCAP()
					thisBunker.bShouldPlayIntroCut = TRUE
				ELSE
					#IF IS_DEBUG_BUILD
					IF GET_FRAME_COUNT() % 60 = 0
						PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE] [MAINTAIN_SHOULD_START_INTRO_CUSTCENE] Not playing - already done")
					ENDIF
					#ENDIF
				ENDIF
			ELIF AM_I_WALKING_INTO_BUNKER_WITH_OWNER()
				IF IS_BUNKER_OWNER_WATCHING_INTRO_MOCAP()
					//- I'm not the owner, but I'm entering the bunker with the owner - play if I haven't seen it already
					bDoneIntroCut = HAS_LOCAL_PLAYER_VIEWED_BUNKER_INTRO_CUTSCENE()
					IF NOT bDoneIntroCut
						PRINTLN("AM_MP_BUNKER [RUN_MAIN_CLIENT_LOGIC] [RUN_BUNKER_INTRO_CUTSCENE] [MAINTAIN_SHOULD_START_INTRO_CUSTCENE] SHould play - owner entering")
						INIT_LOCAL_PLAYER_FOR_INTRO_MOCAP()
						thisBunker.bShouldPlayIntroCut = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SET_TRUCK_AS_PART_OF_THIS_SCRIPT(VEHICLE_INDEX truckID, VEHICLE_INDEX trailerID)
	
	IF DOES_ENTITY_EXIST(truckID)
	AND DOES_ENTITY_EXIST(trailerID)
		INT iInstance
		STRING scriptName 
		IF IS_VEHICLE_DRIVEABLE(truckID)
		AND IS_VEHICLE_DRIVEABLE(trailerID)
			IF IS_VEHICLE_DRIVEABLE(trailerID)
				IF NETWORK_GET_ENTITY_IS_NETWORKED(truckID)
				AND NETWORK_GET_ENTITY_IS_NETWORKED(trailerID)
					IF NETWORK_HAS_CONTROL_OF_ENTITY(truckID)
					AND NETWORK_HAS_CONTROL_OF_ENTITY(trailerID)
						IF CAN_REGISTER_MISSION_VEHICLES(2)
							scriptName = GET_ENTITY_SCRIPT(truckID,iInstance)
							PRINTLN("SET_TRUCK_AS_PART_OF_THIS_SCRIPT: script name = ",scriptName)
							IF NOT IS_STRING_NULL_OR_EMPTY(scriptName)
								IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(scriptName)) <= 0
									IF IS_ENTITY_A_MISSION_ENTITY(truckID)
										NET_PRINT("SET_TRUCK_AS_PART_OF_THIS_SCRIPT waiting for entity to not be a mission entity as script that created it is cleaning up") NET_NL()
										RETURN FALSE
									ELSE
										PRINTLN("SET_TRUCK_AS_PART_OF_THIS_SCRIPT SET_VEHICLE_AS_TRUCK_VEHICLE: NOT a mission Entity")
									ENDIF
								ELSE
									PRINTLN("SET_TRUCK_AS_PART_OF_THIS_SCRIPT: num scripts running = ",GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(scriptName)))
								ENDIF
							ENDIF
							
							scriptName = GET_ENTITY_SCRIPT(trailerID,iInstance)
							PRINTLN("SET_TRUCK_AS_PART_OF_THIS_SCRIPT: script name = ",scriptName)
							IF NOT IS_STRING_NULL_OR_EMPTY(scriptName)
								IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(scriptName)) <= 0
									IF IS_ENTITY_A_MISSION_ENTITY(trailerID)
										NET_PRINT("SET_TRUCK_AS_PART_OF_THIS_SCRIPT - waiting for entity to not be a mission entity as script that created it is cleaning up") NET_NL()
										RETURN FALSE
									ELSE
										PRINTLN("SET_TRUCK_AS_PART_OF_THIS_SCRIPT: NOT a mission Entity")
									ENDIF
								ELSE
									PRINTLN("SET_TRUCK_AS_PART_OF_THIS_SCRIPT: num scripts running = ",GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(scriptName)))
								ENDIF
							ENDIF
							
							IF NOT IS_ENTITY_A_MISSION_ENTITY(truckID)
								SET_ENTITY_AS_MISSION_ENTITY(truckID,FALSE,TRUE)
								NET_PRINT("SET_TRUCK_AS_PART_OF_THIS_SCRIPT - not a mission entity, setting as one.") NET_NL()
							ENDIF
						
							IF NOT IS_ENTITY_A_MISSION_ENTITY(trailerID)
								SET_ENTITY_AS_MISSION_ENTITY(trailerID,FALSE,TRUE)
								NET_PRINT("SET_TRUCK_AS_PART_OF_THIS_SCRIPT - not a mission entity, setting as one.") NET_NL()
							ENDIF
						
							IF IS_ENTITY_A_MISSION_ENTITY(truckID)
							AND IS_ENTITY_A_MISSION_ENTITY(trailerID)
								IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(truckID)						
									SET_ENTITY_AS_MISSION_ENTITY(truckID,FALSE,TRUE)
									NET_PRINT("SET_TRUCK_AS_PART_OF_THIS_SCRIPT - waiting for vehicle to belong to this script.") NET_NL()							
								ENDIF
								IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(trailerID)						
									SET_ENTITY_AS_MISSION_ENTITY(trailerID,FALSE,TRUE)
									NET_PRINT("SET_TRUCK_AS_PART_OF_THIS_SCRIPT- waiting for vehicle to belong to this script.") NET_NL()							
								ENDIF
									
								IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(truckID)						
								AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(trailerID)						
									NET_PRINT("SET_TRUCK_AS_PART_OF_THIS_SCRIPT - Returning TRUE") NET_NL()
									RETURN TRUE
								ENDIF
							ENDIF
						ENDIF
					ELSE
						NETWORK_REQUEST_CONTROL_OF_ENTITY(truckID)
						NETWORK_REQUEST_CONTROL_OF_ENTITY(trailerID)
					ENDIF		
				ENDIF
			ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "SET_TRUCK_AS_PART_OF_THIS_SCRIPT fail as NOT DOES_ENTITY_EXIST")
		#ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC


FUNC BOOL GET_NEARBY_TRUCK_FOR_BUNKER(PLAYER_INDEX pOwnerOfTruck, VEHICLE_INDEX &truckVeh)
	
	INT iTotal, iInstance
	STRING scriptName
	iTotal = GET_ALL_VEHICLES(g_PoolTrucks)
	
	INT i
	REPEAT iTotal i
		IF DOES_ENTITY_EXIST(g_PoolTrucks[i])
		AND NOT IS_ENTITY_DEAD(g_PoolTrucks[i])		
			
			IF IS_ENTITY_A_VEHICLE(g_PoolTrucks[i])
				IF NOT IS_ENTITY_DEAD(g_PoolTrucks[i])
					IF DECOR_IS_REGISTERED_AS_TYPE("Player_Truck", DECOR_TYPE_INT)
						IF GET_ENTITY_MODEL(g_PoolTrucks[i]) = GET_PLAYERS_GUNRUNNING_TRUCK_MODEL(pOwnerOfTruck)
							IF IS_VEHICLE_A_PERSONAL_TRUCK(g_PoolTrucks[i])
								scriptName = GET_ENTITY_SCRIPT(g_PoolTrucks[i],iInstance)
								IF IS_NET_PLAYER_OK(pOwnerOfTruck)
								AND NOT IS_STRING_NULL_OR_EMPTY(scriptName)
									IF GET_OWNER_OF_PERSONAL_TRUCK(g_PoolTrucks[i]) = pOwnerOfTruck
									AND ARE_STRINGS_EQUAL(scriptName,"am_vehicle_spawn")
										truckVeh = g_PoolTrucks[i]
										PRINTLN("GET_NEARBY_TRUCK_FOR_BUNKER - found a vehicle owner is: ", GET_PLAYER_NAME(pOwnerOfTruck) )
										RETURN TRUE
									ENDIF
								ENDIF	
							ENDIF	
						ENDIF	
					ENDIF	
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL GET_NEARBY_TRAILER_FOR_BUNKER(PLAYER_INDEX pOwnerOfTruck, VEHICLE_INDEX &trailerVeh)
	
	INT iTotal, iInstance
	STRING scriptName
	iTotal = GET_ALL_VEHICLES(g_PoolTrucks)
	
	INT i
	REPEAT iTotal i
		IF DOES_ENTITY_EXIST(g_PoolTrucks[i])
		AND NOT IS_ENTITY_DEAD(g_PoolTrucks[i])		
			
			IF IS_ENTITY_A_VEHICLE(g_PoolTrucks[i])
				IF NOT IS_ENTITY_DEAD(g_PoolTrucks[i])
					IF DECOR_IS_REGISTERED_AS_TYPE("Player_Truck", DECOR_TYPE_INT)
						IF GET_ENTITY_MODEL(g_PoolTrucks[i]) = TRAILERLARGE
							IF IS_VEHICLE_A_PERSONAL_TRUCK(g_PoolTrucks[i])
								scriptName = GET_ENTITY_SCRIPT(g_PoolTrucks[i],iInstance)
								IF IS_NET_PLAYER_OK(pOwnerOfTruck)
								AND NOT IS_STRING_NULL_OR_EMPTY(scriptName)
									IF GET_OWNER_OF_PERSONAL_TRUCK(g_PoolTrucks[i]) = pOwnerOfTruck
									AND ARE_STRINGS_EQUAL(scriptName,"am_vehicle_spawn")
										trailerVeh = g_PoolTrucks[i]
										PRINTLN("GET_NEARBY_TRAILER_FOR_BUNKER - found a vehicle owner is: ", GET_PLAYER_NAME(pOwnerOfTruck) )
										RETURN TRUE
									ENDIF
								ENDIF	
							ENDIF	
						ENDIF	
					ENDIF	
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
	
ENDFUNC


FUNC BOOL GET_OWNER_TRUCK_TRAILER_VEHICLE_INDEX(VEHICLE_INDEX &truckVeh, VEHICLE_INDEX &trailerVeh)
	VEHICLE_INDEX localTruckVeh, localTrailerVeh
	
	IF GET_NEARBY_TRUCK_FOR_BUNKER(thisBunker.pOwner , localTruckVeh)
		truckVeh = localTruckVeh
		
	ELSE
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "GET_OWNER_TRUCK_TRAILER_VEHICLE_INDEX localTruckVeh doesn't exist")
		#ENDIF
		IF HAS_START_TO_MOVE_PEDS_IN_CLONE_TRUCK(thisBunker.pOwner)
			IF IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(thisBunker.pOwner))
				localTruckVeh = GET_VEHICLE_PED_IS_IN(GET_PLAYER_PED(thisBunker.pOwner))
				IF GET_OWNER_OF_PERSONAL_TRUCK(localTruckVeh ) = thisBunker.pOwner
					truckVeh = localTruckVeh
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "GET_OWNER_TRUCK_TRAILER_VEHICLE_INDEX owner is driving in truck found his truck")
					#ENDIF
				ENDIF
			ENDIF
		ELSE
			localTruckVeh = GET_CLOSEST_VEHICLE(<<848.867, -3236.171, -150.0>> + <<(NATIVE_TO_INT(thisBunker.pOwner) * 32),0,0>>, 30.0, GET_PLAYERS_GUNRUNNING_TRUCK_MODEL(thisBunker.pOwner)
										, VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES | VEHICLE_SEARCH_FLAG_ALLOW_TRAILERS)
			
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "GET_OWNER_TRUCK_TRAILER_VEHICLE_INDEX can't find owner's truck trying closest vehicle")
			#ENDIF
			
			IF DOES_ENTITY_EXIST(localTruckVeh)
				IF GET_OWNER_OF_PERSONAL_TRUCK(localTruckVeh ) = thisBunker.pOwner
					truckVeh = localTruckVeh
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "GET_OWNER_TRUCK_TRAILER_VEHICLE_INDEX found truck with get_closet_vehicle")
					#ENDIF	
				ENDIF
			ENDIF
		ENDIF	
	ENDIF
	
	IF GET_NEARBY_TRAILER_FOR_BUNKER(thisBunker.pOwner , localTrailerVeh)
		trailerVeh = localTrailerVeh
	ELSE	
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "GET_OWNER_TRUCK_TRAILER_VEHICLE_INDEX localTrailerVeh doesn't exist")
		#ENDIF
		IF !HAS_START_TO_MOVE_PEDS_IN_CLONE_TRUCK(thisBunker.pOwner)
			localTrailerVeh = GET_CLOSEST_VEHICLE(GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(<<848.867, -3236.171, -150.0>> + <<(NATIVE_TO_INT(thisBunker.pOwner) * 32),0,0>>, 62.2800	, << 0, -9, 1.9 >>)
											, 30.0, TRAILERLARGE, VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES | VEHICLE_SEARCH_FLAG_ALLOW_TRAILERS)
		
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "GET_OWNER_TRUCK_TRAILER_VEHICLE_INDEX can't find owner's trailer trying closest vehicle")
			#ENDIF
			
			IF DOES_ENTITY_EXIST(localTrailerVeh)												
				IF GET_OWNER_OF_PERSONAL_TRUCK(localTruckVeh ) = thisBunker.pOwner
					trailerVeh = localTrailerVeh
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "GET_OWNER_TRUCK_TRAILER_VEHICLE_INDEX found trailer with get_closet_vehicle")
					#ENDIF
				ENDIF
			ENDIF	
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(truckVeh)
	AND DOES_ENTITY_EXIST(trailerVeh)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "GET_OWNER_TRUCK_TRAILER_VEHICLE_INDEX true")
		#ENDIF
		RETURN TRUE
	ENDIF	
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CREATE_BUNKER_CLONE_TRUCK_TRAILER()
	
	VECTOR vTruckCoord = <<834.2155, -3234.8008, -98.6259>>
	FLOAT fTruckHeading = 62.2800		
	IF REQUEST_LOAD_MODEL(GET_PLAYERS_GUNRUNNING_TRUCK_MODEL(thisBunker.pOwner))
	AND REQUEST_LOAD_MODEL(TRAILERLARGE)
	AND IS_BIT_SET(serverBD.iBS, BS_BUNKER_SERVER_DATA_AREA_IS_SAFE_TO_SPAWN_TRUCK)
		
		IF NOT IS_BIT_SET(serverBD.iBS, BS_BUNKER_DATA_RESERVE_VEHICLE_INDEX_FOR_TRUCK)
			IF CAN_REGISTER_MISSION_VEHICLES(2)
				SET_BIT(serverBD.iBS, BS_BUNKER_DATA_RESERVE_VEHICLE_INDEX_FOR_TRUCK)
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_NET_AMBIENT, "CREATE_BUNKER_CLONE_TRUCK_TRAILER BS_BUNKER_DATA_RESERVE_VEHICLE_INDEX_FOR_TRUCK true")
				#ENDIF
			ENDIF
		ELSE
			IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niCloneTruck)
			AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niCloneTrailer)
			
				IF CREATE_NET_VEHICLE(	serverBD.niCloneTruck,
						GET_PLAYERS_GUNRUNNING_TRUCK_MODEL(thisBunker.pOwner), vTruckCoord, fTruckHeading, TRUE, TRUE, TRUE,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
				AND CREATE_NET_VEHICLE(	serverBD.niCloneTrailer,
						TRAILERLARGE, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vTruckCoord, fTruckHeading, << 0, -9, 1.9 >>), fTruckHeading, TRUE, TRUE, TRUE,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
						
					IF DECOR_IS_REGISTERED_AS_TYPE("Player_Truck", DECOR_TYPE_INT)
						IF NOT DECOR_EXIST_ON(NET_TO_VEH(serverBD.niCloneTruck), "Player_Truck")
							DECOR_SET_INT(NET_TO_VEH(serverBD.niCloneTruck), "Player_Truck", NETWORK_HASH_FROM_PLAYER_HANDLE(thisBunker.pOwner))
						ENDIF
					ENDIF
					
					IF DECOR_IS_REGISTERED_AS_TYPE("Player_Truck", DECOR_TYPE_INT)
						IF NOT DECOR_EXIST_ON(NET_TO_VEH(serverBD.niCloneTrailer), "Player_Truck")
							DECOR_SET_INT(NET_TO_VEH(serverBD.niCloneTrailer), "Player_Truck", NETWORK_HASH_FROM_PLAYER_HANDLE(thisBunker.pOwner))
						ENDIF
					ENDIF
			
					VEHICLE_SETUP_STRUCT_MP sDataTruck, sDataTrailer
					GET_VEHICLE_SETUP_MP(serverBD.niTruck, sDataTruck)
					SET_VEHICLE_SETUP_MP(NET_TO_VEH(serverBD.niCloneTruck), sDataTruck)
					
					IF PLAYER_ID() = thisBunker.pOwner
						SET_GR_TRAILER_SETUP_MP(serverBD.niTrailer, sDataTrailer)
					ELSE
						GET_VEHICLE_SETUP_MP(serverBD.niTrailer, sDataTrailer)
					ENDIF
					
					CPRINTLN(DEBUG_NET_AMBIENT, "CREATE_BUNKER_CLONE_TRUCK_TRAILER livery:  ",sDataTrailer.VehicleSetup.iModIndex[MOD_LIVERY])
					
					SET_VEHICLE_SETUP_MP(NET_TO_VEH(serverBD.niCloneTrailer), sDataTrailer)
						
					ATTACH_VEHICLE_TO_TRAILER(NET_TO_VEH(serverBD.niCloneTruck),NET_TO_VEH(serverBD.niCloneTrailer))
					SET_VEHICLE_NUMBER_PLATE_TEXT(NET_TO_VEH(serverBD.niCloneTrailer), GET_VEHICLE_NUMBER_PLATE_TEXT(NET_TO_VEH(serverBD.niCloneTruck)))
					
					SET_TRAILER_LEGS_RAISED(NET_TO_VEH(serverBD.niCloneTrailer))
					SET_VEHICLE_ON_GROUND_PROPERLY(NET_TO_VEH(serverBD.niCloneTruck),0)
					
					NETWORK_FADE_IN_ENTITY(NET_TO_VEH(serverBD.niCloneTruck), TRUE, TRUE)
					NETWORK_FADE_IN_ENTITY(NET_TO_VEH(serverBD.niCloneTrailer), TRUE, TRUE)
					
					SET_ENTITY_VISIBLE(NET_TO_VEH(serverBD.niCloneTruck), TRUE)
					SET_ENTITY_VISIBLE(NET_TO_VEH(serverBD.niCloneTrailer), TRUE)
					
					SET_ENTITY_INVINCIBLE(NET_TO_VEH(serverBD.niCloneTruck), TRUE)
					SET_ENTITY_INVINCIBLE(NET_TO_VEH(serverBD.niCloneTrailer), TRUE)
					
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "CREATE_BUNKER_CLONE_TRUCK_TRAILER true")
					#ENDIF
					
					RETURN TRUE
				ENDIF
			ENDIF	
		ENDIF	
	ENDIF	
			
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Used specifically for delivering weapons from resupply missions using the MOC
PROC MAINTAIN_CLONE_MOC_VISIBILITY()
	IF IS_BIT_SET(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_BUNKER_FINISHED_DELIVERY_MISSION_USING_MOC)
		BOOL bClearFlag = FALSE
		
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niCloneTruck)
		AND IS_ENTITY_ALIVE(NET_TO_VEH(serverBD.niCloneTruck))
			IF NETWORK_IS_ENTITY_FADING(NET_TO_VEH(serverBD.niCloneTruck))
				RESET_ENTITY_ALPHA(NET_TO_VEH(serverBD.niCloneTruck))
			ELSE
				bClearFlag = TRUE
			ENDIF
		ENDIF
		
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niCloneTrailer)
		AND IS_ENTITY_ALIVE(NET_TO_VEH(serverBD.niCloneTrailer))
			IF NETWORK_IS_ENTITY_FADING(NET_TO_VEH(serverBD.niCloneTrailer))
				RESET_ENTITY_ALPHA(NET_TO_VEH(serverBD.niCloneTrailer))
				bClearFlag = FALSE
			ELSE
				bClearFlag = TRUE
			ENDIF
		ELSE
			bClearFlag = FALSE
		ENDIF
		
		IF bClearFlag = TRUE
		AND IS_SCREEN_FADED_IN()
			PRINTLN("[MGR_DBG] AM_MP_BUNKER clearing BS2_SIMPLE_INTERIOR_BUNKER_FINISHED_DELIVERY_MISSION_USING_MOC")
			CLEAR_BIT(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_BUNKER_FINISHED_DELIVERY_MISSION_USING_MOC)
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_CREATE_BUNKER_TRUCK_TRAILER()
	IF thisBunker.pOwner != INVALID_PLAYER_INDEX()
		IF IS_OWNERS_ARMORY_TRUCK_INSIDE_BUNKER(thisBunker.pOwner)
		AND IS_ARMORY_TRUCK_UNDER_MAP(thisBunker.pOwner)
		AND NOT IS_PLAYER_REQUESTED_TRUCK_IN_FREEMODE(thisBunker.pOwner)
		AND NOT DO_I_NEED_TO_RENOVATE_TRUCK_CAB(thisBunker.pOwner)
		AND NOT IS_PLAYER_IN_NIGHTCLUB(thisBunker.pOwner)
		AND NOT IS_PLAYER_IN_BUSINESS_HUB(thisBunker.pOwner)
			IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niCloneTruck)
			AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niCloneTrailer)
				IF NOT IS_BIT_SET(serverBD.iBS, BS_BUNKER_SERVER_DATA_ASSIGN_TRUCK_TO_INTERIOR)
					IF GET_OWNER_TRUCK_TRAILER_VEHICLE_INDEX(serverBD.niTruck , serverBD.niTrailer)
						SET_BIT(serverBD.iBS, BS_BUNKER_SERVER_DATA_REQUEST_TO_MOVE_OUT_OF_WAY_FOR_TRUCK)
						SET_BIT(serverBD.iBS, BS_BUNKER_SERVER_DATA_ASSIGN_TRUCK_TO_INTERIOR)
						#IF IS_DEBUG_BUILD
						PRINTLN("MAINTAIN_CREATE_BUNKER_TRUCK_TRAILER BS_BUNKER_SERVER_DATA_ASSIGN_TRUCK_TO_INTERIOR true")
						#ENDIF
					ENDIF	
				ELSE
					IF DOES_ENTITY_EXIST(serverBD.niTruck)
					AND DOES_ENTITY_EXIST(serverBD.niTrailer)
						IF CREATE_BUNKER_CLONE_TRUCK_TRAILER()
							CLEAR_BIT(serverBD.iBS, BS_BUNKER_SERVER_DATA_REQUEST_TO_MOVE_OUT_OF_WAY_FOR_TRUCK)
							CLEAR_BIT(serverBD.iBS, BS_BUNKER_SERVER_DATA_ASSIGN_TRUCK_TO_INTERIOR)
							CLEANUP_MP_SAVED_AVENGER(FALSE, FALSE, TRUE, FALSE, TRUE)
							CLEANUP_MP_SAVED_HACKERTRUCK(FALSE, FALSE, TRUE, FALSE, TRUE)

							#IF IS_DEBUG_BUILD
							PRINTLN("MAINTAIN_CREATE_BUNKER_TRUCK_TRAILER CREATE_BUNKER_CLONE_TRUCK_TRAILER true")
							#ENDIF
						ENDIF
					ELSE
						IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niCloneTruck)
						AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niCloneTrailer)
							CLEAR_BIT(serverBD.iBS, BS_BUNKER_SERVER_DATA_ASSIGN_TRUCK_TO_INTERIOR)
							#IF IS_DEBUG_BUILD
							PRINTLN("MAINTAIN_CREATE_BUNKER_TRUCK_TRAILER BS_BUNKER_SERVER_DATA_ASSIGN_TRUCK_TO_INTERIOR false truck doesn't exist")
							#ENDIF
						ENDIF
					ENDIF	
				ENDIF	
			ENDIF	
		ENDIF
	ENDIF
ENDPROC 

PROC MAINTAIN_CLEANUP_BUNKER_TRUCK_TRAILER()
	IF thisBunker.pOwner != INVALID_PLAYER_INDEX()
		IF (!IS_OWNERS_ARMORY_TRUCK_INSIDE_BUNKER(thisBunker.pOwner)
		AND !IS_ARMORY_TRUCK_UNDER_MAP(thisBunker.pOwner))
		OR IS_PLAYER_REQUESTED_TRUCK_IN_FREEMODE(thisBunker.pOwner)
		OR (DO_I_NEED_TO_RENOVATE_TRUCK_CAB(thisBunker.pOwner)
		AND NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(thisBunker.pOwner)].SimpleInteriorBD.iBSTwo,  BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_DATA_EMPTY_ARMORY_TRUCK_TRAILER))
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niCloneTruck)
			AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niCloneTrailer)
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niCloneTruck)
				AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niCloneTrailer)
					IF NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niCloneTruck))
					AND NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niCloneTrailer))
					AND IS_VEHICLE_EMPTY(NET_TO_VEH(serverBD.niCloneTruck))
						IF NOT NETWORK_IS_ENTITY_FADING(NET_TO_VEH(serverBD.niCloneTruck))
						AND NOT NETWORK_IS_ENTITY_FADING(NET_TO_VEH(serverBD.niCloneTrailer))
						AND IS_BIT_SET(serverBd.iBS, BS_BUNKER_SERVER_DATA_FADE_OUT_TRUCK)
							VEHICLE_INDEX truck = NET_TO_VEH(serverBD.niCloneTruck)
							VEHICLE_INDEX trailer = NET_TO_VEH(serverBD.niCloneTrailer)
							DELETE_VEHICLE(truck)
							DELETE_VEHICLE(trailer)
							CLEAR_BIT(serverBD.iBS, BS_BUNKER_SERVER_DATA_ASSIGN_TRUCK_TO_INTERIOR)
							CLEAR_BIT(serverBd.iBS, BS_BUNKER_SERVER_DATA_FADE_OUT_TRUCK)
							CLEAR_BIT(serverBD.iBS, BS_BUNKER_DATA_RESERVE_VEHICLE_INDEX_FOR_TRUCK)
							CLEAR_BIT(serverBD.iBS, BS_BUNKER_SERVER_DATA_UPDATE_FREEZE_TRUCK_POS)
							RESET_NET_TIMER(serverBD.sTruckFreezeTimer)
							PRINTLN("MAINTAIN_CLEANUP_BUNKER_TRUCK_TRAILER... done")
						ELSE
							IF NOT IS_BIT_SET(serverBd.iBS, BS_BUNKER_SERVER_DATA_FADE_OUT_TRUCK)
								NETWORK_FADE_OUT_ENTITY(NET_TO_VEH(serverBD.niCloneTruck), FALSE, TRUE)
								NETWORK_FADE_OUT_ENTITY(NET_TO_VEH(serverBD.niCloneTrailer), FALSE , TRUE)
								SET_BIT	(serverBd.iBS, BS_BUNKER_SERVER_DATA_FADE_OUT_TRUCK)
							ENDIF	
						ENDIF	
					ENDIF	
				ELSE
					NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.niCloneTruck)
					NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.niCloneTrailer)
				ENDIF
			ENDIF
		ENDIF
	ENDIF	
ENDPROC


PROC MOVE_PALYER_TO_CLONE_TRUCK()
	IF thisBunker.pOwner != INVALID_PLAYER_INDEX()
		IF HAS_START_TO_MOVE_PEDS_IN_CLONE_TRUCK(PLAYER_ID())
		AND IS_SAFE_TO_START_WARP_INTO_CLONE_TRUCK(thisBunker.pOwner)
		AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBd.niCloneTruck)
			IF NOT IS_PLAYER_SITTING_IN_CLONE_TRUCK(PLAYER_ID())
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())	
					IF HAS_START_TO_MOVE_PEDS_IN_CLONE_TRUCK(PLAYER_ID())
						
						IF PLAYER_ID() != g_SimpleInteriorData.truckDriverPlayer
							IF IS_PED_IN_VEHICLE(GET_PLAYER_PED(thisBunker.pOwner), NET_TO_VEH(serverBd.niCloneTruck))
								IF NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBd.niCloneTruck))
								AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
									WarpPlayerIntoCar(PLAYER_PED_ID(), NET_TO_VEH(serverBd.niCloneTruck), VS_FRONT_RIGHT )
									SET_I_AM_SITTING_IN_CLONE_TRUCK(TRUE)
								ENDIF	
							ELSE
								#IF IS_DEBUG_BUILD
									PRINTLN("MOVE_PALYER_TO_CLONE_TRUCK - driver is not in main cab")
								#ENDIF
							ENDIF	
						ELSE	
							IF NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBd.niCloneTruck))
							AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
								WarpPlayerIntoCar(PLAYER_PED_ID(), NET_TO_VEH(serverBd.niCloneTruck), VS_DRIVER)
								SET_I_AM_SITTING_IN_CLONE_TRUCK(TRUE)
							ENDIF	
						ENDIF
						
					ELSE
						#IF IS_DEBUG_BUILD
							PRINTLN("MOVE_PALYER_TO_CLONE_TRUCK - Waiting to move players to main truk")
						#ENDIF
					ENDIF
				ENDIF	
			ELSE
				IF DOES_ENTITY_EXIST(NET_TO_VEH(serverBd.niCloneTruck))
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBd.niCloneTruck))
						START_TO_MOVE_PEDS_IN_CLONE_TRUCK(FALSE)
					ENDIF
				ENDIF	
			ENDIF	
		ENDIF	
	ENDIF	
ENDPROC

FUNC BOOL HAS_EVERYONE_MOVED_TO_CLONE_TRUCK()
	IF thisBunker.pOwner != INVALID_PLAYER_INDEX()
		IF IS_PLAYER_SITTING_IN_CLONE_TRUCK(PLAYER_ID())
		AND NOT HAS_PEDS_MOVED_IN_CLONE_TRUCK(PLAYER_ID())
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			AND GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
			AND PLAYER_ID() = thisBunker.pOwner
				
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBd.niCloneTruck)
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBd.niCloneTruck)
						IF NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBd.niCloneTruck))
							IF GET_VEHICLE_NUMBER_OF_PASSENGERS(NET_TO_VEH(serverBd.niCloneTruck)) != 0
								INT i ,iNumWaiting 
								REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
									IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
										PLAYER_INDEX tempPlayer
										tempPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
										IF IS_PLAYER_SITTING_IN_MAIN_TRUCK(tempPlayer)
											iNumWaiting++ 
										ENDIF	
									ENDIF
								ENDREPEAT
								IF NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBd.niCloneTruck))
									IF iNumWaiting = GET_VEHICLE_NUMBER_OF_PASSENGERS(NET_TO_VEH(serverBd.niCloneTruck))
										PRINTLN("HAS_EVERYONE_MOVED_TO_MAIN_TRUCK - return True iNumWaiting: ", iNumWaiting, " Number of passengers : ", GET_VEHICLE_NUMBER_OF_PASSENGERS(NET_TO_VEH(serverBd.niCloneTruck) ))
										SET_PEDS_IN_CLONE_TRUCK(TRUE)
										RETURN TRUE
									ELSE
										PRINTLN("HAS_EVERYONE_MOVED_TO_MAIN_TRUCK - return false iNumWaiting: ", iNumWaiting, " Number of passengers: ", GET_VEHICLE_NUMBER_OF_PASSENGERS(NET_TO_VEH(serverBd.niCloneTruck) ))
										RETURN FALSE
									ENDIF
								ELSE
									PRINTLN("HAS_EVERYONE_MOVED_TO_MAIN_TRUCK - mainCabIndex dead")
									RETURN TRUE
								ENDIF	
							ELSE	
								IF NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBd.niCloneTruck))
									IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBd.niCloneTruck))
									AND IS_MAIN_TRUCK_EMPTY_AFTER_WARP_INTO_BUNKER(PLAYER_ID())
									AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_ENTER_VEHICLE) != PERFORMING_TASK
									AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_ENTER_VEHICLE) != WAITING_TO_START_TASK
										SET_PEDS_IN_CLONE_TRUCK(TRUE)
										CLEANUP_MP_SAVED_TRUCK(FALSE, FALSE, TRUE, FALSE, TRUE)
										PRINTLN("HAS_EVERYONE_MOVED_TO_MAIN_TRUCK - True no passengers")
										RETURN TRUE
									ELSE
										PRINTLN("HAS_EVERYONE_MOVED_TO_MAIN_TRUCK - IS_PED_IN_VEHICLE(PLAYER_PED_ID(), thisInterior.mainCabIndex) false GET_VEHICLE_NUMBER_OF_PASSENGERS(thisInterior.cloneCabIndex): ", GET_VEHICLE_NUMBER_OF_PASSENGERS(NET_TO_VEH(serverBd.niCloneTruck)))
									ENDIF
								ELSE
									PRINTLN("HAS_EVERYONE_MOVED_TO_MAIN_TRUCK - cloneCabIndex dead")
								ENDIF
							ENDIF
						ENDIF	
					ELSE
						PRINTLN("HAS_EVERYONE_MOVED_TO_MAIN_TRUCK - return false veh doesn't exist ")
					ENDIF		
				ELSE
					PRINTLN("HAS_EVERYONE_MOVED_TO_MAIN_TRUCK - return false thisInterior.mainCabIndex or thisInterior.cloneCabIndex doesn't exist ")
				ENDIF
							
			ENDIF
		ENDIF	
	ENDIF	

	RETURN FALSE
ENDFUNC

PROC MAINTAIN_TRUCK_TRAILER_VEHICLE_INDEXES()
	IF thisBunker.pOwner != INVALID_PLAYER_INDEX()
	AND PLAYER_ID() = thisBunker.pOwner 
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBd.niCloneTruck)
			g_personalBunkerTruck = NET_TO_VEH(serverBd.niCloneTruck)
		ENDIF	
		
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBd.niCloneTrailer)
			g_personalBunkerTrailer = NET_TO_VEH(serverBd.niCloneTrailer)
		ENDIF
	ENDIF
ENDPROC

PROC FORCE_TRUCK_TO_BUNKER_ROOM()
	IF NOT IS_BIT_SET(thisBunker.iBS, BS_2_BUNKER_DATA_FORCE_TRUCK_TO_ROOM)
		IF IS_NET_PLAYER_OK(PLAYER_ID())
		AND IS_PLAYER_IN_BUNKER(PLAYER_ID())
			IF thisBunker.pOwner != INVALID_PLAYER_INDEX()
				IF IS_PLAYER_GUNRUNNING_TRUCK_PURCHASED(thisBunker.pOwner)
				AND IS_OWNERS_ARMORY_TRUCK_INSIDE_BUNKER(thisBunker.pOwner)

					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBd.niCloneTruck)
					AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBd.niCloneTrailer)
						IF IS_VALID_INTERIOR(GET_INTERIOR_FROM_ENTITY(NET_TO_VEH(serverBd.niCloneTruck)))
						
							IF IS_INTERIOR_READY(GET_INTERIOR_FROM_ENTITY(NET_TO_VEH(serverBd.niCloneTruck)))
							AND NOT DO_I_NEED_TO_RENOVATE_TRUCK_CAB(thisBunker.pOwner)

								IF NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBd.niCloneTruck))
								AND NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBd.niCloneTrailer))
									FORCE_ROOM_FOR_ENTITY(NET_TO_VEH(serverBd.niCloneTruck), GET_INTERIOR_FROM_ENTITY(NET_TO_VEH(serverBd.niCloneTruck)), GET_HASH_KEY("bunker_ModRoom"))
									FORCE_ROOM_FOR_ENTITY(NET_TO_VEH(serverBd.niCloneTrailer), GET_INTERIOR_FROM_ENTITY(NET_TO_VEH(serverBd.niCloneTruck)), GET_HASH_KEY("bunker_ModRoom"))	
									SET_BIT(thisBunker.iBS , BS_2_BUNKER_DATA_FORCE_TRUCK_TO_ROOM)
									PRINTLN("FORCE_TRUCK_TO_BUNKER_ROOM BS_BUNKER_DATA_FORCE_TRUCK_TO_ROOM TRUE")	
								ENDIF
							ELSE
								PRINTLN("FORCE_TRUCK_TO_BUNKER_ROOM is truck interior ready: ", IS_INTERIOR_READY(GET_INTERIOR_FROM_ENTITY(NET_TO_VEH(serverBd.niCloneTruck))))
							ENDIF
						ELSE
							PRINTLN("FORCE_TRUCK_TO_BUNKER_ROOM is truck interior valid: ", IS_VALID_INTERIOR(GET_INTERIOR_FROM_ENTITY(NET_TO_VEH(serverBd.niCloneTruck))))
						ENDIF	
					ELSE
						PRINTLN("FORCE_TRUCK_TO_BUNKER_ROOM does truck exist: ", NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBd.niCloneTruck))
					ENDIF		
				ENDIF
			ENDIF	
		ENDIF	
	ELSE
		IF IS_PLAYER_IN_BUNKER(PLAYER_ID())
		AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
			IF GET_FRAME_COUNT() % 120 = 0
				CLEAR_BIT(thisBunker.iBS , BS_2_BUNKER_DATA_FORCE_TRUCK_TO_ROOM)
			ENDIF
		ENDIF
	ENDIF
ENDPROC	

PROC CREATE_OFFICE_CHAIR_BLOCKER()
	IF thisBunker.pOwner = PLAYER_ID()
	AND NOT DOES_ENTITY_EXIST(obChairBlocker)
		IF REQUEST_LOAD_MODEL(PROP_BARREL_02A)
			IF CAN_REGISTER_MISSION_ENTITIES(0,0,1,0)
							
				obChairBlocker = CREATE_OBJECT_NO_OFFSET(PROP_BARREL_02A, <<908.5729, -3207.0327, -97.5028>> ,FALSE,FALSE,TRUE)
				
				SET_ENTITY_ROTATION(obChairBlocker, <<0.0, 0.0, -155.8167>>)
				FREEZE_ENTITY_POSITION(obChairBlocker,TRUE)
				SET_ENTITY_PROOFS(obChairBlocker,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE)
				SET_ENTITY_INVINCIBLE(obChairBlocker,TRUE)
				SET_ENTITY_DYNAMIC(obChairBlocker,TRUE)
				SET_ENTITY_VISIBLE(obChairBlocker,FALSE)
				SET_CAN_CLIMB_ON_ENTITY(obChairBlocker,FALSE)
				
				SET_MODEL_AS_NO_LONGER_NEEDED(PROP_BARREL_02A)				
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC UPDATE_TRUCK_GLOBAL_VEHICLE_INDEX()
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBd.niCloneTruck)
		g_viGunRunTruckInBunker = NET_TO_VEH(serverBd.niCloneTruck)
	ENDIF	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBd.niCloneTrailer)
		g_viGunRunTailerInBunker = NET_TO_VEH(serverBd.niCloneTrailer)
	ENDIF	
ENDPROC

PROC MAINTAIN_CLONE_TRUCK_TURRETS_AFTER_RENOVATE()
	IF thisBunker.pOwner != INVALID_PLAYER_INDEX()
		AND PLAYER_ID() = thisBunker.pOwner
		IF DO_I_NEED_TO_UPDATE_CLONE_TRUCK_TURRETS()
			IF NOT IS_BIT_SET(serverBD.iBS, BS_BUNKER_SERVER_DATA_UPDATE_CLONE_TRUCK_TURRETS_AFTER_RENOVATE)
				SET_BIT(serverBD.iBS, BS_BUNKER_SERVER_DATA_UPDATE_CLONE_TRUCK_TURRETS_AFTER_RENOVATE)
				PRINTLN("MAINTAIN_CLONE_TRUCK_TURRETS_AFTER_RENOVATE - BS_BUNKER_SERVER_DATA_UPDATE_CLONE_TRUCK_TURRETS_AFTER_RENOVATE true")
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(serverBD.iBS, BS_BUNKER_SERVER_DATA_UPDATE_CLONE_TRUCK_TURRETS_AFTER_RENOVATE)
		AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBd.niCloneTrailer)
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBd.niCloneTrailer)
				VEHICLE_SETUP_STRUCT_MP sData
				GET_VEHICLE_SETUP_MP(NET_TO_VEH(serverBd.niCloneTrailer), sData)
				SET_GR_TRAILER_SETUP_MP(NET_TO_VEH(serverBd.niCloneTrailer), sData)
				SET_VEHICLE_SETUP_MP(NET_TO_VEH(serverBd.niCloneTrailer), sData)
				IF !HAS_PLAYER_OWN_TRUCK_COMMAND_CENTER()
					sData.VehicleSetup.iModIndex[MOD_ROOF] = -1
				ELSE
					IF GET_MP_TRUCK_MOD_STAT(TRUCK_MOD_TRAILER_MOD_ROOF) != 2
						SET_MP_TRUCK_MOD_STAT(TRUCK_MOD_TRAILER_MOD_ROOF, 1)
						sData.VehicleSetup.iModIndex[MOD_ROOF] = 0
					ELSE
						SET_MP_TRUCK_MOD_STAT(TRUCK_MOD_TRAILER_MOD_ROOF, 2)
						sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
					ENDIF
				ENDIF
				SET_UPDATE_CLONE_TRUCK_TURRETS(FALSE)
				CPRINTLN(DEBUG_NET_AMBIENT, "MAINTAIN_CLONE_TRUCK_TURRETS_AFTER_RENOVATE roof:  ",sData.VehicleSetup.iModIndex[MOD_ROOF])
				SET_VEHICLE_MOD(NET_TO_VEH(serverBd.niCloneTrailer), MOD_ROOF, sData.VehicleSetup.iModIndex[MOD_ROOF])
				CLEAR_BIT(serverBD.iBS, BS_BUNKER_SERVER_DATA_UPDATE_CLONE_TRUCK_TURRETS_AFTER_RENOVATE)
				PRINTLN("MAINTAIN_CLONE_TRUCK_TURRETS_AFTER_RENOVATE - BS_BUNKER_SERVER_DATA_UPDATE_CLONE_TRUCK_TURRETS_AFTER_RENOVATE false")
			ELSE
				NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBd.niCloneTrailer)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC UN_FREEZE_BUNKER_CLONE_TRUCK()
	IF thisBunker.pOwner != INVALID_PLAYER_INDEX()
		IF IS_BIT_SET(playerBD[NATIVE_TO_INT(thisBunker.pOwner)].iBS , BS_BUNKER_OWNER_IS_UNFREEZE_TRUCK_AFTER_ENTERING_BUNKER)
		AND IS_BIT_SET(serverBD.iBS, BS_BUNKER_SERVER_DATA_UPDATE_FREEZE_TRUCK_POS)
			SET_BIT(serverBD.iBS, BS_BUNKER_SERVER_DATA_UNFREEZE_TRUCK)
			CDEBUG1LN(DEBUG_SHOOTRANGE, "UN_FREEZE_BUNKER_CLONE_TRUCK - BS_BUNKER_SERVER_DATA_UNFREEZE_TRUCK TRUE")
		ENDIF
	ENDIF	
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBd.niCloneTruck)
	AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBd.niCloneTrailer)
	AND IS_BIT_SET(serverBD.iBS, BS_BUNKER_SERVER_DATA_UNFREEZE_TRUCK)
	AND IS_BIT_SET(serverBD.iBS, BS_BUNKER_SERVER_DATA_UPDATE_FREEZE_TRUCK_POS)
		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBd.niCloneTruck)
		AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBd.niCloneTrailer)
			IF NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niCloneTruck))
			AND NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niCloneTrailer))

				FREEZE_ENTITY_POSITION(NET_TO_VEH(serverBD.niCloneTruck), FALSE)
				FREEZE_ENTITY_POSITION(NET_TO_VEH(serverBD.niCloneTrailer), FALSE)
				RESET_NET_TIMER(serverBD.sTruckFreezeTimer)
				CLEAR_BIT(serverBD.iBS, BS_BUNKER_SERVER_DATA_UPDATE_FREEZE_TRUCK_POS)
				CLEAR_BIT(serverBD.iBS, BS_BUNKER_SERVER_DATA_UNFREEZE_TRUCK)
				CDEBUG1LN(DEBUG_SHOOTRANGE, "UN_FREEZE_BUNKER_CLONE_TRUCK - unfreezed truck and trailer")
			ENDIF
		ELSE
			NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBd.niCloneTrailer)
			NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBd.niCloneTruck)
		ENDIF
	ENDIF
ENDPROC 



PROC FREEZE_BUNKER_CLONE_TRUCK()
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBd.niCloneTruck)
	AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBd.niCloneTrailer)
		IF NOT IS_BIT_SET(serverBD.iBS, BS_BUNKER_SERVER_DATA_UPDATE_FREEZE_TRUCK_POS)
		AND NOT IS_BIT_SET(serverBD.iBS, BS_BUNKER_SERVER_DATA_UNFREEZE_TRUCK)
			IF NOT HAS_NET_TIMER_STARTED(serverBD.sTruckFreezeTimer)
				START_NET_TIMER(serverBD.sTruckFreezeTimer)
			ELSE	
				IF HAS_NET_TIMER_EXPIRED(serverBD.sTruckFreezeTimer, 20000)
				AND IS_SCREEN_FADED_IN()
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBd.niCloneTruck)
					AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBd.niCloneTrailer)
						IF NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niCloneTruck))
						AND NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niCloneTrailer))
							IF IS_VEHICLE_ON_ALL_WHEELS(NET_TO_VEH(serverBD.niCloneTruck))
							AND NOT IS_ENTITY_IN_AIR(NET_TO_VEH(serverBD.niCloneTruck))
								FREEZE_ENTITY_POSITION(NET_TO_VEH(serverBD.niCloneTruck), TRUE)
								FREEZE_ENTITY_POSITION(NET_TO_VEH(serverBD.niCloneTrailer), TRUE)
								RESET_NET_TIMER(serverBD.sTruckFreezeTimer)
								SET_BIT(serverBD.iBS, BS_BUNKER_SERVER_DATA_UPDATE_FREEZE_TRUCK_POS)
								CDEBUG1LN(DEBUG_SHOOTRANGE, "FREEZE_BUNKER_CLONE_TRUCK - freezing truck and tariler BS_BUNKER_SERVER_DATA_UPDATE_FREEZE_TRUCK_POS true")
							ELSE
								PRINTLN("FREEZE_BUNKER_CLONE_TRUCK - IS_ENTITY_IN_AIR(NET_TO_VEH(serverBD.niCloneTruck)): ", IS_ENTITY_IN_AIR(NET_TO_VEH(serverBD.niCloneTruck)))
								PRINTLN("FREEZE_BUNKER_CLONE_TRUCK - IS_VEHICLE_ON_ALL_WHEELS(NET_TO_VEH(serverBD.niCloneTruck)): ", IS_VEHICLE_ON_ALL_WHEELS(NET_TO_VEH(serverBD.niCloneTruck)))
							ENDIF	
						ENDIF
					ELSE
						NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBd.niCloneTrailer)
						NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBd.niCloneTruck)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_MOVING_PV_SAFETY_CHECK()
	IF thisBunker.pOwner = PLAYER_ID()
		IF IS_BIT_SET(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_CHECK_SAFE_TO_MOVE_PV)
		AND IS_SAFE_TO_SPAWN_PV(TRUE)
		AND NOT g_bSafeToMovePV
			g_bSafeToMovePV = TRUE
		ENDIF
	ENDIF
ENDPROC

PROC CLEAR_ENTERING_FLAGS_AFTER_UNFREEZE_TRUCK()
	IF IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS , BS_BUNKER_OWNER_IS_UNFREEZE_TRUCK_AFTER_ENTERING_BUNKER)
	AND NOT IS_BIT_SET(serverBD.iBS, BS_BUNKER_SERVER_DATA_UNFREEZE_TRUCK)
	AND NOT IS_BIT_SET(serverBD.iBS, BS_BUNKER_SERVER_DATA_UPDATE_FREEZE_TRUCK_POS)
		CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS , BS_BUNKER_OWNER_IS_UNFREEZE_TRUCK_AFTER_ENTERING_BUNKER)
		PRINTLN("CLEAR_ENTERING_FLAGS_AFTER_UNFREEZE_TRUCK - BS_BUNKER_OWNER_IS_UNFREEZE_TRUCK_AFTER_ENTERING_BUNKER FALSE ")
	ENDIF
ENDPROC

PROC MAINTAIN_BUNKER_CHRISTMAS_DECORATIONS()
	IF NOT IS_BIT_SET(thisBunker.iBSXmas, BS_BUNKER_CHRISTMAS_DECORATIONS_CREATED)
		IF NOT g_sMPTunables.bDisable_Christmas_Tree_Apartment
			IF CREATE_CHRISTMAS_DECORATIONS_FOR_SIMPLE_INTERIOR(thisBunker.eSimpleInteriorID, thisBunker.objChristmasTree)
				#IF IS_DEBUG_BUILD
				PRINTLN("MAINTAIN_BUNKER_CHRISTMAS_DECORATIONS - Christmas decorations created.")
				#ENDIF
				SET_BIT(thisBunker.iBSXmas, BS_BUNKER_CHRISTMAS_DECORATIONS_CREATED)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

SCRIPT_TIMER sMOCEnteryBlock
PROC MAINTAIN_MOC_ENTERY_ON_RENOVATION()
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	AND IS_PLAYER_IN_VEH_SEAT(PLAYER_ID(), VS_DRIVER)
		IF IS_BROWSER_OPEN()
			SET_BIT(thisBunker.iBS2, BS_2_BUNKER_BLOCK_MOC_ENTERY)
			SET_BIT(g_SimpleInteriorData.sGlobalController.iBS, SIMPLE_INTERIOR_CONTROL_BS_BLOCK_MOC_ENTRY)
			PRINTLN("MAINTAIN_MOC_ENTERY_ON_RENOVATION  SIMPLE_INTERIOR_CONTROL_BS_BLOCK_MOC_ENTRY = TRUE")
		ENDIF
	ENDIF
	IF IS_BIT_SET(thisBunker.iBS2, BS_2_BUNKER_BLOCK_MOC_ENTERY)
		IF !IS_BROWSER_OPEN()
		AND IS_SCREEN_FADED_IN()
			IF !HAS_NET_TIMER_STARTED(sMOCEnteryBlock)
				START_NET_TIMER(sMOCEnteryBlock)
			ELSE
				IF HAS_NET_TIMER_EXPIRED(sMOCEnteryBlock, 5000)
					CLEAR_BIT(thisBunker.iBS2, BS_2_BUNKER_BLOCK_MOC_ENTERY)
					CLEAR_BIT(g_SimpleInteriorData.sGlobalController.iBS, SIMPLE_INTERIOR_CONTROL_BS_BLOCK_MOC_ENTRY)
					RESET_NET_TIMER(sMOCEnteryBlock)
					PRINTLN("MAINTAIN_MOC_ENTERY_ON_RENOVATION  SIMPLE_INTERIOR_CONTROL_BS_BLOCK_MOC_ENTRY = FALSE")
				ENDIF
			ENDIF
		ENDIF	
	ENDIF
ENDPROC

SCRIPT_TIMER stDuneloaderTimer

FUNC BOOL MAINTAIN_DUNELOADER_CUTSCENE()
	SWITCH iDuneloaderCutsceneState
		CASE 0
			IF DOES_ENTITY_EXIST(vehDuneloader)
			AND IS_VEHICLE_DRIVEABLE(vehDuneloader)
				camDuneloader = CREATE_CAMERA(CAMTYPE_SCRIPTED, FALSE)
				
				SET_VEHICLE_DOORS_LOCKED(vehDuneloader, VEHICLELOCK_UNLOCKED)
				
				iDuneloaderCutsceneState++
			ENDIF
		BREAK
		
		CASE 1
			IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
			AND DOES_ENTITY_EXIST(vehDuneloader)
			AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
			AND IS_VEHICLE_DRIVEABLE(vehDuneloader)
				IF DOES_CAM_EXIST(camDuneloader)
				AND NOT IS_CAM_ACTIVE(camDuneloader)
					SET_CAM_COORD(camDuneloader, <<870.9714, -3245.8416, -97.1717>>)
					SET_CAM_ROT(camDuneloader, <<-9.8364, 0.0000, 20.7903>>)
					SET_CAM_USE_SHALLOW_DOF_MODE(camDuneloader, TRUE)
					SET_CAM_NEAR_DOF(camDuneloader, 0.15)
					SET_CAM_FAR_DOF(camDuneloader, 10.0)
					SET_CAM_DOF_STRENGTH(camDuneloader, 0.88)
					SET_CAM_FOV(camDuneloader, 42.5)
					SHAKE_CAM(camDuneloader, "HAND_SHAKE", 0.1)
					SET_CAM_ACTIVE(camDuneloader, TRUE)
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
				ENDIF
				
				SET_LOCAL_PLAYER_VISIBLE_IN_CUTSCENE(TRUE, FALSE)
				
				START_MP_CUTSCENE(TRUE)
				
				SET_BIT(iDuneloaderBS, 5)
				
				TASK_ENTER_VEHICLE(PLAYER_PED_ID(), vehDuneloader, DEFAULT, VS_DRIVER, PEDMOVEBLENDRATIO_WALK, ECF_RESUME_IF_INTERRUPTED | ECF_USE_LEFT_ENTRY)
				
				REINIT_NET_TIMER(stDuneloaderTimer)
				
				iDuneloaderCutsceneState++
			ENDIF
		BREAK
		
		CASE 2
			IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
			AND DOES_ENTITY_EXIST(vehDuneloader)
			AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
			AND IS_VEHICLE_DRIVEABLE(vehDuneloader)
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehDuneloader)
				AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_ENTER_VEHICLE) != PERFORMING_TASK
				AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_ENTER_VEHICLE) != WAITING_TO_START_TASK
					SET_VEHICLE_ENGINE_ON(vehDuneloader, TRUE, FALSE)
					
					iDuneloaderCutsceneState++
				ENDIF
			ENDIF
			
			IF (iDuneloaderCutsceneState = 2)
			AND HAS_NET_TIMER_STARTED(stDuneloaderTimer)
			AND HAS_NET_TIMER_EXPIRED(stDuneloaderTimer, 10000)
				iDuneloaderCutsceneState++
			ENDIF
		BREAK
		
		CASE 3
			IF DOES_ENTITY_EXIST(vehDuneloader)
			AND IS_VEHICLE_DRIVEABLE(vehDuneloader)
			AND GET_IS_VEHICLE_ENGINE_RUNNING(vehDuneloader)
				RETURN TRUE
			ELIF HAS_NET_TIMER_STARTED(stDuneloaderTimer)
			AND HAS_NET_TIMER_EXPIRED(stDuneloaderTimer, 10000)
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_DUNELOADER_SPAWNING()
	IF thisBunker.pOwner = INVALID_PLAYER_INDEX()
		EXIT
	ENDIF
	
	IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(thisBunker.pOwner)].iBunkerDuneloaderBS, BUNKER_DUNELOADER_BS_SPAWN_VEHICLE)
		IF NOT IS_BIT_SET(iDuneloaderBS, 0)
			IF NOT DOES_ENTITY_EXIST(vehDuneloader)
				REQUEST_MODEL(DLOADER)
				
				IF HAS_MODEL_LOADED(DLOADER)
					vehDuneloader = CREATE_VEHICLE(DLOADER, <<868.678, -3239.935, -98.584>>, 180.0, FALSE, FALSE, TRUE)
					
					SET_ENTITY_COORDS_NO_OFFSET(vehDuneloader, <<868.678, -3239.935, -98.584>>)
					SET_ENTITY_HEADING(vehDuneloader, 180.0)
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehDuneloader, TRUE)
					SET_VEHICLE_ON_GROUND_PROPERLY(vehDuneloader, 0)
					SET_VEHICLE_NOT_STEALABLE_AMBIENTLY(vehDuneloader, FALSE)
					SET_VEHICLE_FULLBEAM(vehDuneloader, FALSE)
					SET_VEHICLE_LIGHTS(vehDuneloader, FORCE_VEHICLE_LIGHTS_OFF)
					SET_VEHICLE_FIXED(vehDuneloader)
			        SET_ENTITY_HEALTH(vehDuneloader, 1000)
			        SET_VEHICLE_ENGINE_HEALTH(vehDuneloader, 1000)
			        SET_VEHICLE_PETROL_TANK_HEALTH(vehDuneloader, 1000)
					SET_VEHICLE_DIRT_LEVEL(vehDuneloader, 0.0)
					SET_VEHICLE_DOORS_LOCKED(vehDuneloader, VEHICLELOCK_CANNOT_ENTER)
	                SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(vehDuneloader, TRUE)
					SET_VEHICLE_RESPECTS_LOCKS_WHEN_HAS_DRIVER(vehDuneloader, TRUE)
					SET_ENTITY_CAN_BE_DAMAGED(vehDuneloader, FALSE)
					SET_HYDRAULIC_SUSPENSION_RAISE_FACTOR(vehDuneloader, SC_WHEEL_CAR_FRONT_LEFT, 0.0)
					SET_HYDRAULIC_SUSPENSION_RAISE_FACTOR(vehDuneloader, SC_WHEEL_CAR_FRONT_RIGHT, 0.0)
					SET_HYDRAULIC_SUSPENSION_RAISE_FACTOR(vehDuneloader, SC_WHEEL_CAR_MID_LEFT, 0.0)
					SET_HYDRAULIC_SUSPENSION_RAISE_FACTOR(vehDuneloader, SC_WHEEL_CAR_MID_RIGHT, 0.0)
					SET_HYDRAULIC_SUSPENSION_RAISE_FACTOR(vehDuneloader, SC_WHEEL_CAR_REAR_LEFT, 0.0)
					SET_HYDRAULIC_SUSPENSION_RAISE_FACTOR(vehDuneloader, SC_WHEEL_CAR_REAR_RIGHT, 0.0)
					SET_CAN_USE_HYDRAULICS(vehDuneloader, FALSE)
					SET_VEHICLE_EXTRA(vehDuneloader, 2, TRUE)
					SET_VEHICLE_EXTRA(vehDuneloader, 3, TRUE)
					SET_VEHICLE_ENGINE_ON(vehDuneloader, FALSE, TRUE)
					FREEZE_ENTITY_POSITION(vehDuneloader, TRUE)
					
					VEHICLE_SETUP_STRUCT_MP sData
					
					sData.VehicleSetup.eModel = DLOADER
					sData.VehicleSetup.tlPlateText = "29KXR232"
					sData.VehicleSetup.iPlateIndex = 1
					sData.VehicleSetup.iColour1 = 14
					sData.VehicleSetup.iColour2 = 0
					sData.VehicleSetup.iColourExtra1 = 4
					sData.VehicleSetup.iColourExtra2 = 156
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iWheelType = 4
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_9)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_BULLETPROOF_TYRES)
					
					SET_VEHICLE_SETUP_MP(vehDuneloader, sData, FALSE, TRUE, TRUE)
					
					SET_MODEL_AS_NO_LONGER_NEEDED(DLOADER)
					SET_MODEL_AS_NO_LONGER_NEEDED(GR_PROP_GR_RSPLY_CRATE04B)
					
					SET_BIT(iDuneloaderBS, 0)
				ENDIF
			ENDIF
		ELIF NOT IS_BIT_SET(iDuneloaderBS, 1)
		AND IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(thisBunker.pOwner)].iBunkerDuneloaderBS, BUNKER_DUNELOADER_BS_SPAWN_CRATE)
			IF NOT DOES_ENTITY_EXIST(objDuneloaderCrate)
				REQUEST_MODEL(GR_PROP_GR_RSPLY_CRATE04B)
				
				IF HAS_MODEL_LOADED(GR_PROP_GR_RSPLY_CRATE04B)
					objDuneloaderCrate = CREATE_OBJECT(GR_PROP_GR_RSPLY_CRATE04B, <<868.678, -3239.935, -100.584>>, FALSE, FALSE, TRUE)
					
					SET_ENTITY_COORDS_NO_OFFSET(objDuneloaderCrate, <<868.678, -3239.935, -100.584>>)
					SET_ENTITY_HEADING(objDuneloaderCrate, 180.0)
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(objDuneloaderCrate, TRUE)
					SET_ENTITY_INVINCIBLE(objDuneloaderCrate, TRUE)
					SET_ENTITY_PROOFS(objDuneloaderCrate, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE)
					
					SET_BIT(iDuneloaderBS, 1)
				ENDIF
			ENDIF
		ELIF NOT IS_BIT_SET(iDuneloaderBS, 2)
		AND IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(thisBunker.pOwner)].iBunkerDuneloaderBS, BUNKER_DUNELOADER_BS_SPAWN_CRATE)
			IF DOES_ENTITY_EXIST(vehDuneloader)
			AND IS_VEHICLE_DRIVEABLE(vehDuneloader)
			AND DOES_ENTITY_EXIST(objDuneloaderCrate)
				ATTACH_ENTITY_TO_ENTITY(objDuneloaderCrate, vehDuneloader, GET_ENTITY_BONE_INDEX_BY_NAME(vehDuneloader, "chassis_dummy"), <<0.0, -1.4, 0.375>>, <<0.0, 0.0, 90.0>>, TRUE, FALSE, TRUE)
				
				SET_BIT(iDuneloaderBS, 2)
			ENDIF
		ELSE
			IF DOES_ENTITY_EXIST(objDuneloaderCrate)
			AND NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(thisBunker.pOwner)].iBunkerDuneloaderBS, BUNKER_DUNELOADER_BS_SPAWN_CRATE)
				FLASH_FADE_OBJECT(sDuneloaderCrateFadeData, objDuneloaderCrate, 3)
				
				IF sDuneloaderCrateFadeData.bFinished
					DELETE_OBJECT(objDuneloaderCrate)
					
					sDuneloaderCrateFadeData.bFinished = FALSE
					sDuneloaderCrateFadeData.fCurrentFlashFadeTime = 0.0
					
					CLEAR_BIT(iDuneloaderBS, 1)
					CLEAR_BIT(iDuneloaderBS, 2)
				ENDIF
			ENDIF
		ENDIF
	ELIF (iDuneloaderState = 0)
		IF IS_BIT_SET(iDuneloaderBS, 0)
			IF DOES_ENTITY_EXIST(vehDuneloader)
			AND IS_VEHICLE_DRIVEABLE(vehDuneloader)
				FLASH_FADE_VEHICLE(sDuneloaderFadeData, vehDuneloader, 3)
				
				IF IS_BIT_SET(iDuneloaderBS, 1)
					FLASH_FADE_OBJECT(sDuneloaderCrateFadeData, objDuneloaderCrate, 3)
				ENDIF
				
				IF sDuneloaderFadeData.bFinished
					IF IS_BIT_SET(iDuneloaderBS, 1)
						IF DOES_ENTITY_EXIST(objDuneloaderCrate)
							DELETE_OBJECT(objDuneloaderCrate)
						ENDIF
						
						sDuneloaderCrateFadeData.bFinished = FALSE
						sDuneloaderCrateFadeData.fCurrentFlashFadeTime = 0.0
					ENDIF
					
					DELETE_VEHICLE(vehDuneloader)
					
					sDuneloaderFadeData.bFinished = FALSE
					sDuneloaderFadeData.fCurrentFlashFadeTime = 0.0
					
					iDuneloaderBS = 0
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF thisBunker.pOwner = PLAYER_ID()
		IF NOT IS_BIT_SET(thisBunker.iBS2, BS_2_BUNKER_PRINTED_AMMU_NATION_CRATE_HELP)
		AND IS_BIT_SET(thisBunker.iBS, BS_BUNKER_CALLED_CLEAR_HELP)
		AND DOES_BLIP_EXIST(blipDuneloaderCrate)
		AND IS_BIT_SET(iDuneloaderBS, 2)
		AND g_iSimpleInteriorState = SIMPLE_INT_STATE_IDLE
			IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				PRINT_HELP("BUNK_DL_HELP2")
				
				SET_BLIP_FLASHES(blipDuneloaderCrate, TRUE)
				SET_BLIP_FLASH_TIMER(blipDuneloaderCrate, 5000)
				
				SET_BIT(thisBunker.iBS2, BS_2_BUNKER_PRINTED_AMMU_NATION_CRATE_HELP)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(iDuneloaderBS, 0)
		AND NOT IS_PLAYER_IN_ZOOMED_OUT_RADAR_OR_MAP(PLAYER_ID())
			IF IS_BIT_SET(iDuneloaderBS, 2)
				IF NOT DOES_BLIP_EXIST(blipDuneloaderCrate)
					blipDuneloaderCrate = CREATE_BLIP_FOR_COORD(<<868.678, -3239.935, -100.584>>)
					
					SET_BLIP_SPRITE(blipDuneloaderCrate, RADAR_TRACE_SUPPLIES)
					SET_BLIP_NAME_FROM_TEXT_FILE(blipDuneloaderCrate, "BUNK_DL_BLIP")
					SET_BLIP_SCALE(blipDuneloaderCrate, 1.0)
					SET_BLIP_AS_SHORT_RANGE(blipDuneloaderCrate, FALSE)
					SET_BLIP_COLOUR_FROM_HUD_COLOUR(blipDuneloaderCrate, HUD_COLOUR_BLUE)
					
					SET_BIT(iDuneloaderBS, 3)
					
					CLEAR_BIT(iDuneloaderBS, 4)
				ELIF IS_BIT_SET(iDuneloaderBS, 4)
					REMOVE_BLIP(blipDuneloaderCrate)
				ENDIF
			ELSE
				IF NOT DOES_BLIP_EXIST(blipDuneloaderCrate)
					blipDuneloaderCrate = CREATE_BLIP_FOR_COORD(<<868.678, -3239.935, -100.584>>)
					
					SET_BLIP_SPRITE(blipDuneloaderCrate, RADAR_TRACE_SUPPLIES)
					SET_BLIP_NAME_FROM_TEXT_FILE(blipDuneloaderCrate, "BUNK_DL_BLIP")
					SET_BLIP_SCALE(blipDuneloaderCrate, 0.8)
					SET_BLIP_AS_SHORT_RANGE(blipDuneloaderCrate, TRUE)
					SET_BLIP_COLOUR_FROM_HUD_COLOUR(blipDuneloaderCrate, HUD_COLOUR_GREY)
					
					SET_BIT(iDuneloaderBS, 4)
					
					CLEAR_BIT(iDuneloaderBS, 3)
				ELIF IS_BIT_SET(iDuneloaderBS, 3)
					REMOVE_BLIP(blipDuneloaderCrate)
				ENDIF
			ENDIF
		ELSE
			IF DOES_BLIP_EXIST(blipDuneloaderCrate)
				REMOVE_BLIP(blipDuneloaderCrate)
			ENDIF
		ENDIF
		
		SWITCH iDuneloaderState
			CASE 0
				IF IS_NET_PLAYER_OK(PLAYER_ID())
				AND DOES_ENTITY_EXIST(PLAYER_PED_ID())
				AND IS_PLAYER_CONTROL_ON(PLAYER_ID())
				AND NOT IS_ANY_INTERACTION_ANIM_PLAYING()
				AND NOT IS_PAUSE_MENU_ACTIVE_EX()
				AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
				AND Get_Peds_Drunk_Alcohol_Hit_Count(PLAYER_PED_ID()) <= 9
				AND NOT g_SpawnData.bPassedOutDrunk
				AND NOT IS_PED_RAGDOLL(PLAYER_PED_ID())
				AND NOT IS_BIT_SET(g_simpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_INSTANT_PASS_OUT)
				AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				AND DOES_ENTITY_EXIST(vehDuneloader)
				AND NOT GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID())
				AND g_sContactRequestGBMissionLaunch.iType = -1
				AND NOT IS_BIT_SET(g_SimpleInteriorData.sGlobalController.iBS2, SIMPLE_INTERIOR_CONTROL_BS2_BLOCK_DL_BLOCK_MISSION_LAUNCH)
					IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), vehDuneloader) < 3.0
					AND IS_ENTITY_FACING_COORD_WITHIN_RANGE(PLAYER_PED_ID(), <<868.678, -3239.935, -100.584>>, 80.0)
						IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBunkerDuneloaderBS, BUNKER_DUNELOADER_BS_SPAWN_CRATE)
							IF iDuneloaderContext = NEW_CONTEXT_INTENTION
								REGISTER_CONTEXT_INTENTION(iDuneloaderContext, CP_HIGH_PRIORITY, "BUNK_DL_START")
							ELSE
								IF HAS_CONTEXT_BUTTON_TRIGGERED(iDuneloaderContext)
									NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
									
									RELEASE_CONTEXT_INTENTION(iDuneloaderContext)
									
									iDuneloaderContext = NEW_CONTEXT_INTENTION
									
									IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("BUNK_DL_HELP3")
									OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("BUNK_DL_HELP4")
										CLEAR_HELP()
									ENDIF
									
									iDuneloaderState++
								ENDIF
							ENDIF
						ELSE
							IF IS_FACTORY_SHUTDOWN_BY_PLAYER(BUNKER_SAVE_SLOT)
								IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("BUNK_DL_HELP4")
									PRINT_HELP_FOREVER("BUNK_DL_HELP4")
								ENDIF
							ELSE
								IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("BUNK_DL_HELP3")
									PRINT_HELP_FOREVER("BUNK_DL_HELP3")
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF iDuneloaderContext != NEW_CONTEXT_INTENTION
							RELEASE_CONTEXT_INTENTION(iDuneloaderContext)
							
							iDuneloaderContext = NEW_CONTEXT_INTENTION
						ENDIF
						
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("BUNK_DL_HELP3")
						OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("BUNK_DL_HELP4")
							CLEAR_HELP()
						ENDIF
					ENDIF
				ELSE
					IF iDuneloaderContext != NEW_CONTEXT_INTENTION
						RELEASE_CONTEXT_INTENTION(iDuneloaderContext)
						
						iDuneloaderContext = NEW_CONTEXT_INTENTION
					ENDIF
					
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("BUNK_DL_HELP3")
					OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("BUNK_DL_HELP4")
						CLEAR_HELP()
					ENDIF
				ENDIF
			BREAK
			
			CASE 1
				IF MAINTAIN_DUNELOADER_CUTSCENE()
					DO_SCREEN_FADE_OUT(500)
					
					iDuneloaderState++
				ENDIF
			BREAK
			
			CASE 2
				IF IS_SCREEN_FADED_OUT()
					CLEANUP_DUNELOADER_CUTSCENE()
					
					IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
					AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						TASK_LEAVE_VEHICLE(PLAYER_PED_ID(), vehDuneloader, ECF_WARP_PED)
					ENDIF
					
					SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBunkerDuneloaderBS, BUNKER_DUNELOADER_BS_RUNNING_MISSION)
					
					REQUEST_LAUNCH_GB_MISSION(FMMC_TYPE_GUNRUNNING_AMMUNATION, ENUM_TO_INT(GAV_GUNRUNNING_AMMUNATION))
					
					iDuneloaderState++
				ENDIF
			BREAK
			
			CASE 3
				IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
				AND NOT IS_PED_INJURED(PLAYER_PED_ID())
					SET_ENTITY_COORDS_NO_OFFSET(PLAYER_PED_ID(), <<871.7571, -3238.6932, -99.0472>>)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), 176.5893)
					
					NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					
					iDuneloaderState++
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                 MAIN LOGIC PROC                  /////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

PROC RUN_MAIN_CLIENT_LOGIC()
	
	// Fix for B*3951449
	IF IS_ANY_TRANSITION_SESSION_INVITE_BEING_ACTIONED()
	AND NOT AM_I_ACCEPTING_TRANSITION_SESSIONS_INVITE_FROM_BOOT()
	AND g_SimpleInteriorData.bEventAutowarpActive
		CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_BUNKER - Transition session invite being actioned, bailing on bunker script.")
		SET_LOCAL_PLAYER_DOING_SIMPLE_INTERIOR_AUTOWARP(FALSE, thisBunker.eSimpleInteriorID)
		SCRIPT_CLEANUP()
		EXIT
	ENDIF	

	UPDATE_FACTORY_PED_RESET_LOGIC()
	MAINTAIN_BUNKER_HUD()
	MAINTAIN_PRODUCT_SPAWNING()
	MAINTAIN_FACTORY_PRODUCTION()
	MAINTAIN_MATERIAL_PROP_SPAWNING()
	MAINTAIN_POST_SETUP_CUTSCENE()
	MAINTAIN_LAUNCHING_CARMOD_SCRIPT()
	RUN_ACTIVITY_LOGIC()
	MAINTAIN_MOC_ENTERY_ON_RENOVATION()
	MAINTAIN_CAN_DRIVE_OUT_OF_BUNKER()
	MAINTAIN_GET_OUT_OF_OWNERS_PERSONAL_VEHICLE()
	MAINTAIN_BUNKER_CADDIES()
	MAINTAIN_BUNKER_TRUCK()	
	MAINTAIN_MOVE_OUT_OF_WAY()
	MAINTAIN_WANTED_LEVEL()
	MAINTAIN_SHOOTING_RANGE_UPGRADE()
	MAINTAIN_BUNKER_LAPTOP_SCREEN()
	HAS_EVERYONE_MOVED_TO_CLONE_TRUCK()
	MAINTAIN_BUNKER_AUDIO_BANKS()
	MAINTAIN_BUNKER_PICKUPS()
	MAINTAIN_BUNKER_AMBIENT_ZONES()
	MAINTAIN_HIDE_PERSONAL_VEHICLE()
	MAINTAIN_BLOCK_WHEELIES()
	MAINTAIN_CONTROL_OF_BUNKER_PERSONAL_CARMOD_VEHICLES()
	MOVE_PALYER_TO_CLONE_TRUCK()
	MAINTAIN_TRUCK_TRAILER_VEHICLE_INDEXES()
	FORCE_TRUCK_TO_BUNKER_ROOM()
	UPDATE_TRUCK_GLOBAL_VEHICLE_INDEX()
	KICK_PLAYERS_OUT_OF_VEHICLE_IN_BUNKER_AFTER_AUTOWARP_AS_PASSENGER()
	MAINTAIN_DUNELOADER_SPAWNING()
	
	IF IS_PAUSE_MENU_ACTIVE_EX()
	OR IS_SYSTEM_UI_BEING_DISPLAYED()
	OR IS_INTERACTION_MENU_OPEN()
		REINIT_NET_TIMER(pauseMenuInteractionsDelay,TRUE)
	ENDIF
	
	IF NOT thisBunker.bPlayerLeftTheFactory
		IF IS_LOCAL_PLAYER_WALKING_OUT_OF_THIS_FACTORY(thisBunker.eID)
			thisBunker.bPlayerLeftTheFactory = TRUE
		ENDIF
	ENDIF
	
	MAINTAIN_SHOULD_START_INTRO_CUSTCENE()
	
	MAINTAIN_BUNKER_CHRISTMAS_DECORATIONS()
	
	IF IS_BUNKER_STATE(BUNKER_STATE_LOADING)	
		MAINTAIN_BUNKER_LOADING_STATE()
		
		IF NOT IS_BIT_SET(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_PREVENT_BIG_MESSAGE_DISABLE)
			SET_DISABLE_RANK_UP_MESSAGE(TRUE)
			#IF IS_DEBUG_BUILD
			IF (GET_FRAME_COUNT() % 120) = 0
				PRINTLN("[SIMPLE_INTERIOR] RUN_MAIN_CLIENT_LOGIC : BUNKER_STATE_LOADING - SET_DISABLE_RANK_UP_MESSAGE = TRUE")
			ENDIF
			#ENDIF
		ELSE
			PRINTLN("[SIMPLE_INTERIOR] RUN_MAIN_CLIENT_LOGIC : BUNKER_STATE_LOADING - SET_DISABLE_RANK_UP_MESSAGE - not disabling because gunrunning delivery")
		ENDIF
	ELIF IS_BUNKER_STATE(BUNKER_STATE_IDLE)
		CLEAR_ENTERING_FLAGS_AFTER_UNFREEZE_TRUCK()
		MAINTAIN_AA_TRAILER_CREATION_BUNKER()
		MAINTAIN_PERSONAL_VEHICLE_LOCKS()
		MAINTAIN_NON_OWNER_VEHICLE_ENTRANCE()
		MAINTAIN_BUNKER_UPGRADES()
		MAINTAIN_BUNKER_GUN_LOCKER(sBunkerGunLocker)
		MAINTAIN_BUNKER_HELP_TEXT()
		MAINTAIN_BUNKER_BLIPS()
//		MAINTAIN_BUNKER_RAGDOLL()
		CREATE_OFFICE_CHAIR_BLOCKER()
		MAINTAIN_CLONE_MOC_VISIBILITY()
		MAINTAIN_MOVING_PV_SAFETY_CHECK()
		
		IF NOT IS_BIT_SET(thisBunker.iBS2, BS_2_BUNKER_REENABLED_DRIVING)
			IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
			AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					//only driver can call this command, due to ownership.
					IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
						STOP_BRINGING_VEHICLE_TO_HALT(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
						PRINTLN("[SIMPLE_INTERIOR] RUN_MAIN_CLIENT_LOGIC driver, stop bring to halt.")
					ELSE
						PRINTLN("[SIMPLE_INTERIOR] RUN_MAIN_CLIENT_LOGIC not a driver, dont stop bring to halt VEHICLE_SEAT: ", GET_SEAT_PED_IS_IN_AS_INT(PLAYER_PED_ID()))
					ENDIF
				ENDIF
			ENDIF
			SET_BIT(thisBunker.iBS2, BS_2_BUNKER_REENABLED_DRIVING)
		ENDIF
		
		IF IS_PLAYER_IN_THIS_SIMPLE_INTERIOR_ENTRY_LOCATE(thisbunker.eSimpleInteriorID)
			SET_PLAYER_IN_SIMPLE_INTERIOR_ENTRY_LOCATE(FALSE, thisbunker.eSimpleInteriorID)
		ENDIF
		
		IF IS_BIT_SET(thisBunker.iBS,BS_BUNKER_KILL_LOAD_SCENE_WHEN_INSIDE)
			IF IS_LOCAL_PLAYER_WITHIN_BUNKER_BOUNDS()
				NEW_LOAD_SCENE_STOP()
				CLEAR_BIT(thisBunker.iBS,BS_BUNKER_KILL_LOAD_SCENE_WHEN_INSIDE)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(thisBunker.iBS2, BS_2_BUNKER_TERMINATE_UPGRADE_AUDIO_SCENE)
			IF IS_SCREEN_FADED_IN()
				IF IS_AUDIO_SCENE_ACTIVE("FAMILY_1_MOD_SHOP_INTRO_SCENE")
					STOP_AUDIO_SCENE("FAMILY_1_MOD_SHOP_INTRO_SCENE")
				ENDIF
				
				CLEAR_BIT(thisBunker.iBS2, BS_2_BUNKER_TERMINATE_UPGRADE_AUDIO_SCENE)
			ENDIF
		ENDIF
		
		IF NOT thisBunker.bShootingRangeLaunched
			thisBunker.bShootingRangeLaunched = LAUNCH_SHOOTING_RANGE()
		ENDIF
		
		IF NOT IS_BIT_SET(thisBunker.iBS, BS_BUNKER_DATA_INIT_PRODUCT)
			INITIALISE_FACTORY_PRODUCT()
			SET_BIT(thisBunker.iBS, BS_BUNKER_DATA_INIT_PRODUCT)
		ENDIF
		
		IF NOT g_bLaunchedMissionFrmLaptop
			RUN_OFFICE_SEAT_ACTIVITY(activitySeatStruct, serverBD.serverSeatBD)
		ENDIF
		
		IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		AND GET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID())
			SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
		ENDIF
		
		IF PLAYER_ID() = thisBunker.pOwner
		AND NOT IS_BIT_SET(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_CHECK_SAFE_TO_MOVE_PV)
			CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_BUNKER_MOVE_OUT_OF_WAY)
		ENDIF
		
		IF DOES_PLAYER_OWN_FACTORY_UPGRADE(thisBunker.pOwner, thisBunker.eID, UPGRADE_ID_SECURITY)
			INT iBlockerBitSet = 0
			
			IF NOT IS_PLAYER_BUNKER_FIRING_RANGE_BLACK_PURCHASED(thisBunker.pOwner)
			AND NOT IS_PLAYER_BUNKER_FIRING_RANGE_WHITE_PURCHASED(thisBunker.pOwner)
				SET_BIT(iBlockerBitSet, 0)
			ENDIF
			
			CLIENT_MAINTAIN_MP_CCTV(playerBD[NATIVE_TO_INT(PLAYER_ID())].MPCCTVClient, MPCCTVLocal, thisBunker.eSimpleInteriorID, DEFAULT, DEFAULT, iBlockerBitSet)
		ENDIF
		
		IF g_SimpleInteriorData.bForceCCTVCleanup
			IF DOES_PLAYER_OWN_FACTORY_UPGRADE(thisBunker.pOwner, thisBunker.eID, UPGRADE_ID_SECURITY)
				CLEANUP_CCTV(FALSE)
			ENDIF
			
			g_SimpleInteriorData.bForceCCTVCleanup = FALSE
		ENDIF
		
		IF NOT IS_BIT_SET(thisBunker.iBS, BS_BUNKER_CALLED_CLEAR_HELP)
			IF IS_SCREEN_FADED_IN()
			AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
				CLEAR_HELP()
				SET_BIT(thisBunker.iBS, BS_BUNKER_CALLED_CLEAR_HELP)
				PRINTLN("AM_MP_BUNKER - Calling clear help")
			ENDIF
		ENDIF
		
		SET_DISABLE_RANK_UP_MESSAGE(FALSE)
		#IF IS_DEBUG_BUILD
		IF (GET_FRAME_COUNT() % 120) = 0
			PRINTLN("[SIMPLE_INTERIOR] RUN_MAIN_CLIENT_LOGIC : BUNKER_STATE_IDLE - SET_DISABLE_RANK_UP_MESSAGE = FALSE")
		ENDIF
		#ENDIF

	ELIF IS_BUNKER_STATE(BUNKER_STATE_PLAYING_SETUP_CUTSCENE)
		
		SET_DISABLE_RANK_UP_MESSAGE(TRUE)
		#IF IS_DEBUG_BUILD
		IF (GET_FRAME_COUNT() % 120) = 0
			PRINTLN("[SIMPLE_INTERIOR] RUN_MAIN_CLIENT_LOGIC : BUNKER_STATE_PLAYING_SETUP_CUTSCENE - SET_DISABLE_RANK_UP_MESSAGE = TRUE")
		ENDIF
		#ENDIF

		IF SHOULD_LOCAL_PLAYER_BE_AUTOWARPED_INSIDE_THIS_SIMPLE_INTERIOR(thisBunker.eSimpleInteriorID)
			IF NOT IS_SIMPLE_INTERIOR_GLOBAL_BIT_SET(SI_BS_CanFinishAutoWarp, thisBunker.eSimpleInteriorID)
				// Wait until autowarp has been completed before beginning cutscene.
				EXIT
			ENDIF
		ENDIF
		
		MAINTAIN_BUNKER_POST_SETUP_SCENE_STATE()
		
		SET_BIT(thisBunker.iBS2, BS_2_BUNKER_DONE_RESEARCH_HELP)
		
	ELIF IS_BUNKER_STATE(BUNKER_STATE_PLAYING_INTRO_CUTSCENE)
		IF RUN_BUNKER_INTRO_CUTSCENE()
	//		SET_SIMPLE_INTERIOR_CHILD_SCRIPT_READY(TRUE)
			g_SimpleInteriorData.bDontSimulateInputGaitOnEntrance = TRUE
			SET_BUNKER_STATE(BUNKER_STATE_LOADING) // BUNKER_STATE_IDLE
		ENDIF
	ENDIF
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MP_TRUCK_BLCK5")
		CLEAR_HELP()
	ENDIF
ENDPROC


PROC RUN_MAIN_SERVER_LOGIC()
	
	IF IS_BUNKER_SERVER_STATE(BUNKER_STATE_LOADING)
		
		IF CREATE_BUNKER_VEHICLES()
			CLEAR_BIT(serverBD.iBS, BS_BUNKER_DATA_RESERVE_VEHICLE_INDEX_FOR_TRUCK)
			SET_BUNKER_SERVER_STATE(BUNKER_STATE_IDLE)
		ENDIF
	ELIF IS_BUNKER_SERVER_STATE(BUNKER_STATE_IDLE)
		IF NOT thisBunker.bApplyUpgrade
			MAINTAIN_BUNKER_VEHICLES()
		ENDIF
		
		IF IS_BUNKER_STATE(BUNKER_STATE_IDLE)
			CREATE_BOARDROOM_CHAIRS(serverBD.serverSeatBD, activitySeatStruct)
		ENDIF
		FREEZE_BUNKER_CLONE_TRUCK()
	ENDIF
	UN_FREEZE_BUNKER_CLONE_TRUCK()
	MAINTAIN_CLONE_TRUCK_TURRETS_AFTER_RENOVATE()
	MAINTAIN_CREATE_BUNKER_TRUCK_TRAILER()
	MAINTAIN_CLEANUP_BUNKER_TRUCK_TRAILER()
	MAINTAIN_TRUCK_SPAWN_SAFETY_CHECK()
ENDPROC


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                      Main Loop                      //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
SCRIPT  (SIMPLE_INTERIOR_CHILD_SCRIPT_DATA scriptData) 
	
	IF NETWORK_IS_GAME_IN_PROGRESS() 
		SCRIPT_INITIALISE(scriptData)	
	ELSE
		SCRIPT_CLEANUP()
	ENDIF
	
	// Main loop
	WHILE TRUE
		
		// One wait to rule them all. This can be the ONLY wait from here on in.....
		MP_LOOP_WAIT_ZERO()

		// Fix for url:bugstar:3532230
		IF IS_NET_PLAYER_OK(PLAYER_ID())
			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_SuppressNavmeshForEnterVehicleTask, TRUE)
			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisablePlayerAutoVaulting, TRUE)
		ENDIF
		
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			PRINTLN("AM_MP_BUNKER - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE = TRUE")
			SCRIPT_CLEANUP()
		ENDIF
		
		IF SHOULD_THIS_SIMPLE_INTERIOR_CHILD_THREAD_TERMINATE(scriptData.iScriptInstance, thisBunker.eSimpleInteriorID)
			PRINTLN("AM_MP_BUNKER - SHOULD_THIS_SIMPLE_INTERIOR_CHILD_THREAD_TERMINATE = TRUE")
			SCRIPT_CLEANUP()
		ENDIF
		
		RUN_MAIN_CLIENT_LOGIC()
		
		MAINTAIN_INTERIOR_BED_SPAWN_ACTIVITIES()
		
		// -----------------------------------
		// Process server game logic		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			RUN_MAIN_SERVER_LOGIC()
		ENDIF
		
		#IF IS_DEBUG_BUILD
		IF thisBunker.pOwner = PLAYER_ID()
			UPDATE_DEBUG_WIDGETS()
		ENDIF
		#ENDIF
		
	ENDWHILE
	

ENDSCRIPT
