//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        AM_GANG_CALL.sc																				//
// Description: Controls the Gang calls																		//
// Written by:  David Gentles																				//
// Date: 12/12/2012																							//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"

// Game Headers
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_path.sch"
USING "building_control_public.sch"
USING "menu_public.sch"
USING "context_control_public.sch"

// Network Headers
USING "net_events.sch"
USING "net_blips.sch"
USING "net_scoring_common.sch"
USING "net_mission.sch"
USING "shared_hud_displays.sch"
USING "net_entity_icons.sch"
USING "fm_relationships.sch"
USING "net_taxi.sch"
USING "net_spawn.sch"
USING "net_gang_angry.sch"
USING "net_realty_new.sch"

USING "freemode_header.sch"

USING "net_ticker_logic.sch"

USING "net_garages.sch"
USING "achievement_public.sch"

USING "net_contact_requests.sch"

USING "net_wait_zero.sch"

USING "net_cash_transactions.sch"

#IF IS_DEBUG_BUILD
USING "net_debug_log.sch"
USING "profiler.sch"
#ENDIF

///////////////////////////////////
///    		CONSTANTS    		///
///////////////////////////////////

// Game States. 
CONST_INT GAME_STATE_INI 									0
CONST_INT GAME_STATE_RUNNING								1
CONST_INT GAME_STATE_LEAVE									2
CONST_INT GAME_STATE_TERMINATE_DELAY						3
CONST_INT GAME_STATE_END									4

CONST_INT MAX_NUMBER_OF_GANG_MEMBERS						3

CONST_INT THIEF_PUSH_STRENGTH								3

CONST_INT GANG_CALL_FIGHT_TIME								600000	//10min
CONST_INT GANG_CALL_THIEF_ABANDON_TIME						300000	//5min
CONST_INT GANG_CALL_THIEF_ESCAPE_TIME						10000	//10seconds
CONST_INT GANG_CALL_FORCE_SPAWN_TIME						5000	//20sec
CONST_INT GANG_CALL_TIME_FOR_THIEF_TO_SPAWN					5000	//5sec
CONST_INT GANG_CALL_TIME_FOR_ARMY_TO_SPAWN					30000	//5sec
CONST_INT GANG_CALL_TIME_TO_REFUND_MERRYWEATHER				120000	//2min
CONST_INT GANG_CALL_ACCEPTABLE_TARGET_SPEED					10
CONST_INT GANG_CALL_THIEF_NO_PATH_TIME						10000	//10seconds
CONST_INT GANG_CALL_AMBIENT_GRAB_TIME						5000	//5seconds
const_int gang_call_theif_respawn_time						60000

CONST_INT MIN_DISTANCE_THIEF_TO_STEAL						1		//1sqr
CONST_INT MIN_DISTANCE_THIEF_TO_ESCAPE						10000	//100sqr
CONST_INT MIN_DISTANCE_THIEF_TO_AMBIENT_GRAB				625		//25sqr
CONST_INT MIN_DISTANCE_THIEF_TO_BREAK_COVER					25		//5sqr
CONST_INT MIN_DISTANCE_THIEF_TO_SPAWN						20 
CONST_INT MAX_DISTANCE_THIEF_TO_SPAWN						40
CONST_INT MIN_DISTANCE_TO_RESET_TRAVEL_COORD				625		//25sqr
CONST_INT MIN_DISTANCE_GANG_MEMBER_ABANDONED				200
CONST_INT MIN_DISTANCE_GANG_MEMBER_ABANDONEDSQR				40000	//200sqr
CONST_INT THIEF_NO_PATH_CLEAR_SQR							625		//25sqr

CONST_INT MAX_THIEF_STEAL_AMOUNT							2000


///////////////////////////////////
///    		ENUMS 		   		///
///////////////////////////////////

/// PURPOSE: List of IDs for gang member classes
ENUM GANG_CLASS
	GANG_CLASS_THIEF
ENDENUM

/// PURPOSE: List of IDs for gang member stages
ENUM GANG_STAGE
	GANG_STAGE_DO_NOTHING = 0,
	GANG_STAGE_THIEF_APPROACH,
	GANG_STAGE_THIEF_STEAL,
	GANG_STAGE_THIEF_FLEE,
	GANG_STAGE_CLEANUP
ENDENUM


///////////////////////////////////
///    		STRUCT 		   		///
///////////////////////////////////
//iBitSet
CONST_INT GANG_BS_CREATED_FROM_AMBIENT						0
CONST_INT GANG_BS_CREATED_AND_SET_UP						1
CONST_INT GANG_BS_THIEF_BROKEN_COVER						2
CONST_INT GANG_BS_THIEF_SET_UP_SPRINT						3
CONST_INT GANG_BS_THIEF_SET_UP_FLEE							4
CONST_INT GANG_BS_ABANDONED									5
CONST_INT GANG_BS_DEAD										6
CONST_INT GANG_BS_NO_PATH_TIMER_SET							7
CONST_INT GANG_BS_RAW_AMBIENT								8
CONST_INT GANG_BS_ATTRIBUTE_DAMAGE_SET						9
CONST_INT GANG_BS_unlock_blocking_of_non_temporary_events	10


/// PURPOSE: Gang member info for server broadcast data
STRUCT GANG_MEMBER
	NETWORK_INDEX netID
	INT iGang
	INT iBitSet
	TIME_DATATYPE timeNoPath
	GANG_CLASS gangClass
	GANG_STAGE gangStage = GANG_STAGE_DO_NOTHING
	INT iAmbientPedArrayEntry = -1
ENDSTRUCT


///////////////////////////////////
///    		SERVER BD    		///
///////////////////////////////////
//iBitSet
CONST_INT SERVER_BS_GANG_CREATED						0
CONST_INT SERVER_BS_ALL_GANG_MEMBERS_DEAD				1
CONST_INT SERVER_BS_UNABLE_TO_FIND_SPAWN_LOCATION		2
CONST_INT SERVER_BS_SPAWN_LOCATION_FOUND				3
CONST_INT SERVER_BS_TIME_UP								4
CONST_INT SERVER_BS_TIMER_STARTED						5
CONST_INT SERVER_BS_GANG_MODELS_SETUP					6
CONST_INT SERVER_BS_OK_TO_SPAWN_GANG_NOW				7
CONST_INT SERVER_BS_A_GANG_MEMBER_CREATED				8
CONST_INT SERVER_BS_ALL_GANG_MEMBERS_ABANDONED			9
CONST_INT SERVER_BS_INITIAL_RESERVE_VALUES_SET			10
CONST_INT SERVER_BS_PASSIVE_CHANGED						11
CONST_INT SERVER_BS_FOUND_AMBIENT_PEDS					12
CONST_INT SERVER_BS_SPAWN_TIMER_STARTED					13
CONST_INT SERVER_BS_REFUND_TIMER_STARTED				14
CONST_INT SERVER_BS_DUE_REFUND							15
CONST_INT SERVER_BS_TARGET_ATTACKED_BY_MERRYWEATHER		16
CONST_INT SERVER_BS_TRAVEL_TO_COORDS_UNDERWATER			17
CONST_INT SERVER_BS_FIRST_TIME_THEIF_SPAWNED			18

//iThiefBitset
CONST_INT THIEF_BS_AMBIENT_ELEMENT_GRAB_ATTEMPTED		0
CONST_INT THIEF_BS_SOMEONE_ROBBED						1
CONST_INT THIEF_BS_ESCAPED								2
CONST_INT THIEF_BS_ESCAPE_TIMER_STARTED					3
CONST_INT THIEF_BS_ENTIRE_GANG_CREATED_FROM_AMBIENT		4
CONST_INT THIEF_BS_SUCCESSFUL_DEAD						5
CONST_INT THIEF_BS_TARGET_ESCAPED						6
CONST_INT THIEF_BS_ABANDON_TIMER_STARTED				7
CONST_INT THIEF_BS_ABANDON								8
CONST_INT THIEF_BS_NO_MONEY								9
CONST_INT THIEF_BS_NO_PATH								10
CONST_INT THIEF_BS_WAITING_FOR_NO_PATH_RESET			11
CONST_INT THIEF_BS_TARGET_HAS_KILLED					12

//iFailBitset
CONST_INT FAIL_BS_GANG_DEAD								0
CONST_INT FAIL_BS_UNABLE_TO_FIND_SPAWN_LOCATION			1
CONST_INT FAIL_BS_THIEF_ESCAPE							2
CONST_INT FAIL_BS_SUCCESSFUL_THIEF_DEAD					3
CONST_INT FAIL_BS_TARGET_PLAYER_INVALID					4
CONST_INT FAIL_BS_TARGET_NAME_CHANGED					5
CONST_INT FAIL_BS_ARMY_TARGET_DEAD						6
CONST_INT FAIL_BS_ARMY_TARGET_ESCAPE					7
CONST_INT FAIL_BS_THIEF_ABANDON							8
CONST_INT FAIL_BS_THEFT_NO_MONEY						9
CONST_INT FAIL_BS_REFUND								10
CONST_INT FAIL_BS_ARMY_TARGET_SURVIVE					11
const_int fail_bs_gang_attack_on_same_gang_members		12
CONST_INT FAIL_BS_TARGET_ON_FM_EVENT					13

/// PURPOSE: The server broadcast data. Everyone can read this data, only the server can update it
STRUCT ServerBroadcastData
	INT iServerGameState
	INT iBitset
	INT iThiefBitset
	INT iFailBitset
	INT iPassiveBitset
	TIME_DATATYPE timeStarted
	TIME_DATATYPE timeThiefEscape
	TIME_DATATYPE timeThiefAbandon
	TIME_DATATYPE timeAmbientGrab
	TIME_DATATYPE timeRefund
	
	GANG_MEMBER gangMember[MAX_NUMBER_OF_GANG_MEMBERS]
	
	VECTOR vGangSpawn
	FLOAT fGangSpawn
	VECTOR vGangTravel
	
	VECTOR vThiefNoPathCoord
	
	INT iSuccessThiefID = -1
	INT iCashStolen
	
	INT iRelGroupSetupBitset = 0
	
	INT iPedsToReserve = 0
	INT iVehiclesToReserve = 0
	INT iObjectsToReserve = 0
	
	INT iNumberOfThievesKilled = 0
	MODEL_NAMES mnThief
	
	BOOL bIsHitSquad = FALSE
	BOOL bIsAbandonHit = FALSE
	
	BOOL bIsAssassins = FALSE
	ASSASSIN_LEVEL eAssassinLevel = AL_LOW
	
	SCRIPT_TIMER MissionTerminateDelayTimer
ENDSTRUCT
ServerBroadcastData serverBD


///////////////////////////////////
///    		PLAYER BD    		///
///////////////////////////////////
//iBitSet
CONST_INT CLIENT_BS_BEEN_ROBBED							0
CONST_INT CLIENT_BS_THIEVES_SUCCESSFULLY_FLED			1
CONST_INT CLIENT_BS_PASSIVE_CHANGE_RECOGNISED			2
CONST_INT CLIENT_BS_ATTACKED_BY_MERRYWEATHER			3
const_int CLIENT_BS_PROSTITUTE_ACTIVE					4
CONST_INT CLIENT_BS_TRANSACTION_SUCCESS					5
CONST_INT CLIENT_BS_TRANSACTION_FAIL					6

/// PURPOSE: The client broadcast data. Everyone can read this data, only the local player only the server can update it
STRUCT PlayerBroadcastData
	INT iGameState
	INT iBitset
	INT iSuccessThiefID = -1
	INT iCashStolen = 0
	
	INT iRelGroupSetupBitset = 0
	
ENDSTRUCT
PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]


///////////////////////////////////
///    		LOCAL BITSET   		///
///////////////////////////////////
//iLocalBitset
CONST_INT LOCAL_BS_SERVER_STAGGER_AT_LEAST_ONE_GANG_MEMBER_ALIVE 			1
CONST_INT LOCAL_BS_SERVER_STAGGER_AT_LEAST_ONE_GANG_MEMBER_NOT_ABANDONED 	2
CONST_INT LOCAL_BS_SERVER_STAGGER_SOMEONE_NEAR_A_THIEF			 			3
CONST_INT LOCAL_BS_CLIENT_NEAR_A_THIEF							 			4
CONST_INT LOCAL_BS_CLIENT_SUCCESS_THIEF_ESCAPED					 			5
CONST_INT LOCAL_BS_CLIENT_THIEF_BLIP_NAMED									6
CONST_INT LOCAL_BS_CLIENT_SHOULD_PROGRESS_TO_CLEANUP						7

CONST_INT LOCAL_BS_TICKER_THIEF_NO_PATH										8
CONST_INT LOCAL_BS_TICKER_ARMY_TARGET_ESCAPE								9
CONST_INT LOCAL_BS_TICKER_ARMY_TARGET_DEAD									10
CONST_INT LOCAL_BS_TICKER_GANG_SUCCESSFULLY_REQUESTED						11
CONST_INT LOCAL_BS_TICKER_MUGGERS_APPROACHING_TARGET						12
CONST_INT LOCAL_BS_TICKER_TARGET_ESCAPED_MUGGER								13
CONST_INT LOCAL_BS_PHONECALL_THIEF_SUCCESS									14
CONST_INT LOCAL_BS_TRANSACTION_PENDING										15
CONST_INT LOCAL_BS_DO_TRANSACTION											16
CONST_INT LOCAL_BS_SENT_TICKER												17

INT iLocalBitset = 0


CONST_INT LOCAL_GANG_BS_GANG_REL_INITIALISED									0
CONST_INT LOCAL_GANG_BS_ATTRIBUTE_DAMAGE_SET									1

INT iLocalGangMemberBitset[MAX_NUMBER_OF_GANG_MEMBERS]


CONST_INT PHONE_BS_CALL_SETUP														0
CONST_INT PHONE_BS_CALL_RECEIVED													1
   
INT iLocalPhoneBitset = 0 

///////////////////////////////////
///    		VARIABLES    		///
///////////////////////////////////

//Everyone receives from pre-game
INT iGangVariation = -1
PLAYER_INDEX playerGangOwner
PLAYER_INDEX playerGangTarget
INT iGangTargetPlayerNameHash = -1
VECTOR vGangInitialCoords

//Local only
TIME_DATATYPE timeNet
INT iServerGangStaggeredCounter = 0
INT iServerPlayerStaggeredCounter = 0
INT iClientGangStaggeredCounter = 0
AI_BLIP_STRUCT aiBlipThief
structPedsForConversation sSpeech
INCIDENT_INDEX incidentArmy
PED_INDEX pedAmbient[16]

INT iRefundCost = 0

INT iCurrentTransactionIndex = 0
SCRIPT_TIMER transactionTimer
CONST_INT TRANSACTION_TIMEOUT 10000

CONST_INT TRANSACTION_PENDING	-1
CONST_INT TRANSACTION_FAILED	0
CONST_INT TRANSACTION_SUCCESS	1

// Widgets & Debug.
#IF IS_DEBUG_BUILD
INT iDebugTimeLeft = 0
INT iDebugThiefEscapeTimeLeft = 0
INT iDebugThiefAbandonTimeLeft = 0
BOOL bDebugIsScriptHost = FALSE
BOOL bDebugSkipTimeToEnd = FALSE
BOOL bDebugSkipThiefToEscaped = FALSE
BOOL bDebugSkipThiefToAbandon = FALSE
BOOL bDebugServerForceEnd = FALSE
BOOL bDebugBlipGangMembers = FALSE
BOOL bDebugSpawnThief = FALSE
BLIP_INDEX blipDebug[MAX_NUMBER_OF_GANG_MEMBERS]
BOOL bOutPutTransactionDebug
BOOL bForceTransactionFail = FALSE
#ENDIF

#IF IS_DEBUG_BUILD

/// PURPOSE:
///    Gets the current gang nam
/// RETURNS:
///    Current gang name as a string
FUNC STRING DEBUG_GANG_NAME()
	
	SWITCH iGangVariation
		CASE GANG_CALL_TYPE_SPECIAL_THIEF
			RETURN "THIEF"
		BREAK
		CASE GANG_CALL_TYPE_SPECIAL_MERRYWEATHER
			RETURN "MERCS"
		BREAK
	ENDSWITCH
	
	RETURN "AM_GANG_CALL"
ENDFUNC

#ENDIF

/// PURPOSE:
///    Gets the model for the specified gang
/// PARAMS:
///    thisGang - gang ID to check
/// RETURNS:
///    gang ped model as MODEL_NAMES
FUNC MODEL_NAMES GET_GANG_PED_MODEL(INT thisGang)
	SWITCH thisGang
		CASE GANG_CALL_TYPE_SPECIAL_THIEF
			RETURN serverBD.mnThief
		BREAK
	ENDSWITCH
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

/// PURPOSE:
///    Gets the number of gang peds to spawn for a specified gang
/// PARAMS:
///    thisGang - gang ID to check
/// RETURNS:
///    number of gang peds as an INT
FUNC INT GET_NUMBER_OF_GANG_MEMBERS_IN_GANG(INT thisGang)
	SWITCH thisGang
		CASE GANG_CALL_TYPE_SPECIAL_THIEF
			RETURN 1
		BREAK
	ENDSWITCH
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Gets the class of the gang members in the specified gang
/// PARAMS:
///    thisGang - gang ID to check
/// RETURNS:
///    gang class as ID GANG_CLASS
FUNC GANG_CLASS GET_GANG_MEMBER_CLASS(INT thisGang)
	SWITCH thisGang
		CASE GANG_CALL_TYPE_SPECIAL_THIEF
			RETURN GANG_CLASS_THIEF
		BREAK
	ENDSWITCH
	RETURN GANG_CLASS_THIEF
ENDFUNC

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  MAIN PROCS                  //////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////

/// PURPOSE:
///    Returns if a ped is still performing a specific task
/// PARAMS:
///    thisPed - ped ID to check
///    thisTaskName - task to check
/// RETURNS:
///    true/false
FUNC BOOL IS_TASK_ONGOING(PED_INDEX thisPed, SCRIPT_TASK_NAME thisTaskName)
	IF GET_SCRIPT_TASK_STATUS(thisPed, thisTaskName) = PERFORMING_TASK
	OR GET_SCRIPT_TASK_STATUS(thisPed, thisTaskName) = WAITING_TO_START_TASK
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Alternative logic check if a ped has finished a specific task
/// PARAMS:
///    ped - ped to check
///    taskname - task to check
/// RETURNS:
///    true/false
func bool has_ped_task_finished_2(ped_index ped, script_task_name taskname = script_task_perform_sequence)
	
	scripttaskstatus status
	status = get_script_task_status(ped, taskname)
	
	if status = finished_task 
	or status = dormant_task
		return true
	endif
	
	return false
endfunc

/// PURPOSE:
///    Checks if the ped is suitable to be given a new task (not injured, stunned or ragdolling)
/// PARAMS:
///    thisPed - ped ID to check
/// RETURNS:
///    true/false
FUNC BOOL IS_PED_ALRIGHT_FOR_ACTION(PED_INDEX thisPed)
	IF NOT IS_PED_INJURED(thisPed)
	AND NOT IS_PED_BEING_STUNNED(thisPed)
	AND NOT IS_PED_RAGDOLL(thisPed)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Sets the gang member stage for a specified gang member
/// PARAMS:
///    iGangID - gang member to set stage
///    thisStage - desired stage
PROC SET_GANG_MEMBER_STAGE(INT iGangID, GANG_STAGE thisStage)
	#IF IS_DEBUG_BUILD
	STRING stageName
	SWITCH thisStage
		CASE GANG_STAGE_DO_NOTHING
			stageName = "GANG_STAGE_DO_NOTHING"
		BREAK
		CASE GANG_STAGE_THIEF_APPROACH
			stageName = "GANG_STAGE_THIEF_APPROACH"
		BREAK
		CASE GANG_STAGE_THIEF_STEAL
			stageName = "GANG_STAGE_THIEF_STEAL"
		BREAK
		CASE GANG_STAGE_THIEF_FLEE
			stageName = "GANG_STAGE_THIEF_FLEE"
		BREAK
		CASE GANG_STAGE_CLEANUP
			stageName = "GANG_STAGE_CLEANUP"
		BREAK
	ENDSWITCH
	CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === Gang Member ", iGangID, " set stage to ", stageName)
	#ENDIF
	
	serverBD.gangMember[iGangID].gangStage = thisStage
ENDPROC

/// PURPOSE:
///    Starts the server timer that notes when the gang was spawned
PROC START_SERVER_TIMER()
	IF NOT IS_BIT_SET(serverBD.iBitset, SERVER_BS_TIMER_STARTED)
		serverBD.timeStarted = timeNet
		SET_BIT(serverBD.iBitset, SERVER_BS_TIMER_STARTED)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === SET_BIT(serverBD.iBitset, SERVER_BS_TIMER_STARTED)")
		#ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Resets the gang spawned server timer
PROC RESET_SERVER_TIMER()
	IF IS_BIT_SET(serverBD.iBitset, SERVER_BS_TIMER_STARTED)
		CLEAR_BIT(serverBD.iBitset, SERVER_BS_TIMER_STARTED)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === CLEAR_BIT(serverBD.iBitset, SERVER_BS_TIMER_STARTED)")
		#ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Begins the countdown to the thief having escaped from the target player
PROC START_THIEF_ESCAPE_TIMER()
	IF NOT IS_BIT_SET(serverBD.iThiefBitSet, THIEF_BS_ESCAPE_TIMER_STARTED)
		serverBD.timeThiefEscape = timeNet
		SET_BIT(serverBD.iThiefBitSet, THIEF_BS_ESCAPE_TIMER_STARTED)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === SET_BIT(serverBD.iThiefBitSet, THIEF_BS_ESCAPE_TIMER_STARTED)")
		#ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Resets the countdown to the thief having escaped from the target player
PROC RESET_THIEF_ESCAPE_TIMER()
	IF IS_BIT_SET(serverBD.iThiefBitSet, THIEF_BS_ESCAPE_TIMER_STARTED)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === CLEAR_BIT(serverBD.iThiefBitSet, THIEF_BS_ESCAPE_TIMER_STARTED)")
		#ENDIF
		CLEAR_BIT(serverBD.iThiefBitSet, THIEF_BS_ESCAPE_TIMER_STARTED)
	ENDIF
ENDPROC

/// PURPOSE:
///    Begins the countdown to the thief giving up trying to mug the target player
PROC START_THIEF_ABANDON_TIMER()
	IF NOT IS_BIT_SET(serverBD.iThiefBitSet, THIEF_BS_ABANDON_TIMER_STARTED)
		serverBD.timeThiefAbandon = timeNet
		SET_BIT(serverBD.iThiefBitSet, THIEF_BS_ABANDON_TIMER_STARTED)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === SET_BIT(serverBD.iThiefBitSet, THIEF_BS_ABANDON_TIMER_STARTED)")
		#ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Resets the countdown to the thief giving up trying to mug the target player
PROC RESET_THIEF_ABANDON_TIMER()
	IF IS_BIT_SET(serverBD.iThiefBitSet, THIEF_BS_ABANDON_TIMER_STARTED)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === CLEAR_BIT(serverBD.iThiefBitSet, THIEF_BS_ABANDON_TIMER_STARTED)")
		#ENDIF
		CLEAR_BIT(serverBD.iThiefBitSet, THIEF_BS_ABANDON_TIMER_STARTED)
	ENDIF
ENDPROC

/// PURPOSE:
///    Begins the countdown to the player getting a refund
PROC START_REFUND_TIMER()
	IF NOT IS_BIT_SET(serverBD.iBitset, SERVER_BS_REFUND_TIMER_STARTED)
		serverBD.timeRefund = timeNet
		SET_BIT(serverBD.iBitset, SERVER_BS_REFUND_TIMER_STARTED)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === SET_BIT(serverBD.iBitset, SERVER_BS_REFUND_TIMER_STARTED)")
		#ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Refunds the countdown to the player getting a refund
PROC RESET_REFUND_TIMER()
	IF IS_BIT_SET(serverBD.iBitset, SERVER_BS_REFUND_TIMER_STARTED)
		CLEAR_BIT(serverBD.iBitset, SERVER_BS_REFUND_TIMER_STARTED)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === CLEAR_BIT(serverBD.iBitset, SERVER_BS_REFUND_TIMER_STARTED)")
		#ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Gets the current ped move blend ratio requried for the thief based on if they have revealed themselves
/// PARAMS:
///    iGangID - ID of the gang member to get the move blend ratio for
/// RETURNS:
///    Ped move blend ratio ID as a float
FUNC FLOAT GET_THIEF_PED_MOVE_BLEND_RATIO(INT iGangID)
	IF NOT IS_BIT_SET(serverBD.gangMember[iGangID].iBitSet, GANG_BS_THIEF_BROKEN_COVER)
		RETURN PEDMOVEBLENDRATIO_WALK
	ENDIF
	RETURN PEDMOVEBLENDRATIO_SPRINT
ENDFUNC

/// PURPOSE:
///    Returns if two points are within a squared distance of each other
/// PARAMS:
///    vCoord1 - first coordinate to compare
///    vCoord2 - second coordinate to compare
///    fDistanceSquared - squared distance to see if the two points are within
/// RETURNS:
///    true/false
FUNC BOOL ARE_TWO_POINTS_WITHIN_SQUARE_DISTANCE(VECTOR vCoord1, VECTOR vCoord2, FLOAT fDistanceSquared)
	
	IF VDIST2(vCoord1, vCoord2) < fDistanceSquared									//Is within distance
		//IF ((vCoord2.z-vCoord1.z) * (vCoord2.z-vCoord1.z)) < fDistanceSquared/2		//Is within closer distance vertically
		
			/*#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === PRE_GAME DONE")
			#ENDIF*/
		
			RETURN TRUE
		//ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if the thief is within distance of the target to steal from them
/// PARAMS:
///    vThiefCoords - location of the thief
///    vTargetCoords - location of the thief's player target
/// RETURNS:
///    true/false
FUNC BOOL CHECK_THIEF_DISTANCE(VECTOR vThiefCoords, VECTOR vTargetCoords)

	/*#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === ABSF ", ABSF(vTargetCoords.z - vThiefCoords.z))
	#ENDIF
	
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === VDIST2 ", VDIST2(vTestTarget, vTestThief))
	#ENDIF*/

	IF ABSF(vTargetCoords.z - vThiefCoords.z) < 2.0
		VECTOR vTestThief
		vTestThief.x = vThiefCoords.x
		vTestThief.y = vThiefCoords.y
		VECTOR vTestTarget
		vTestTarget.x = vTargetCoords.x
		vTestTarget.y = vTargetCoords.y
		
		IF ARE_TWO_POINTS_WITHIN_SQUARE_DISTANCE(vTestThief, vTestTarget, 2.0)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    This where the gang members group is set to like the players group. Returned int goes into the function SET_GANG_CALL_MERC_PLAYER_RELATIONSHIP(INT iLikeBitset).
/// RETURNS:
///    bitset of what players the current gangs like as an INT
FUNC INT GENERATE_GANG_MERC_PLAYER_RELATIONSHIP_BITSET()
	INT iRelGroupSetupBitset = 0
	INT i = 0
	//GAMER_HANDLE gamerHandle
	
	REPEAT NUM_NETWORK_PLAYERS i
	
		IF iGangVariation = GANG_CALL_TYPE_SPECIAL_MERRYWEATHER
		OR iGangVariation = GANG_CALL_TYPE_SPECIAL_THIEF
			IF playerGangOwner = INT_TO_PLAYERINDEX(i)
				SET_BIT(iRelGroupSetupBitset, i)		//only owner is friendly for thief and army
			ENDIF
		ELSE
			IF PLAYER_ID() = INT_TO_PLAYERINDEX(i)
				SET_BIT(iRelGroupSetupBitset, i)			//It's me, add to friendly list
			ELSE
				IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(i), FALSE)
					//gamerHandle = GET_GAMER_HANDLE_PLAYER(INT_TO_PLAYERINDEX(i))
					//IF IS_PLAYER_A_BUDDY(gamerHandle)
					IF IS_PLAYER_PASSIVE(INT_TO_PLAYERINDEX(i))
						SET_BIT(iRelGroupSetupBitset, i)	//It's a buddy, add to friendly list
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN iRelGroupSetupBitset
	
ENDFUNC

/// PURPOSE:
///    Sets up the current gang to like or dislike players based on a bitset representing the player IDs
/// PARAMS:
///    iLikeBitset - bitset representing like/dislike of the player IDs
PROC SET_GANG_CALL_MERC_PLAYER_RELATIONSHIP(INT iLikeBitset)
	
	SWITCH iGangVariation
		CASE GANG_CALL_TYPE_SPECIAL_THIEF
			SET_AMBIENT_GANG_MERC_PLAYER_RELATIONSHIPS(rgFM_AiAmbientGangMerc[AmbientGangThiefMerc], iLikeBitset)
		BREAK
		CASE GANG_CALL_TYPE_SPECIAL_MERRYWEATHER
			SET_PRIVATE_SECURITY_PLAYER_RELATIONSHIPS(iLikeBitset)
		BREAK
	ENDSWITCH
	
ENDPROC

/// PURPOSE:
///    Clears the gang liking and disliking all the players
PROC CLEAR_GANG_CALL_MERC_PLAYER_RELATIONSHIP()
	
	SWITCH iGangVariation
		CASE GANG_CALL_TYPE_SPECIAL_THIEF
			CLEAR_AMBIENT_GANG_MERC_PLAYER_RELATIONSHIPS(rgFM_AiAmbientGangMerc[AmbientGangThiefMerc])
		BREAK
		CASE GANG_CALL_TYPE_SPECIAL_MERRYWEATHER
			CLEAR_PRIVATE_SECURITY_PLAYER_RELATIONSHIPS()
		BREAK
	ENDSWITCH
ENDPROC

#IF IS_DEBUG_BUILD

/// PURPOSE:
///    Prints out a bitset that represents the gang liking or disliking player IDs
/// PARAMS:
///    iBitSet - bitset of player ID flags being liked or disliked
///    playerSource - player that created the bitset
PROC DEBUG_PRINT_REL_GROUP_BIT_SET(INT iBitSet, PLAYER_INDEX playerSource)
	INT i
	
	CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === ======== NEW LIKE LIST FOR GANG ", iGangVariation, " FROM ",  GET_PLAYER_NAME(playerSource), " ========")
	REPEAT NUM_NETWORK_PLAYERS i
		IF IS_BIT_SET(iBitSet, i)
			IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(i), FALSE)
				CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === Gang ", iGangVariation, " likes ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(i)))
			ENDIF
		ENDIF
	ENDREPEAT
	CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === ======== END LIKE LIST FOR GANG ", iGangVariation, " FROM ",  GET_PLAYER_NAME(playerSource), " ========")
	
ENDPROC

#ENDIF

/// PURPOSE:
///    Checks if anything has changed that would alter who the gang likes or dislikes and reprocesses the gang reactions accordingly
PROC CLIENT_MANAGE_GANG_CALL_MERC_PLAYER_RELATIONSHIPS()

	BOOL bChangedThisFrame = FALSE
	
	IF PLAYER_ID() = playerGangOwner
		
		IF IS_BIT_SET(g_iBuddyChangeBitset, GLOBAL_BUDDY_CHANGE_BS_GANG_CALL)
			CLEAR_BIT(g_iBuddyChangeBitset, GLOBAL_BUDDY_CHANGE_BS_GANG_CALL)
			bChangedThisFrame = TRUE
		ENDIF
		
		IF NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(playerGangOwner)].iBitset, CLIENT_BS_PASSIVE_CHANGE_RECOGNISED)	//if we've not updated passive
			IF IS_BIT_SET(serverBD.iBitset, SERVER_BS_PASSIVE_CHANGED)												//and we should
				SET_BIT(playerBD[NATIVE_TO_INT(playerGangOwner)].iBitset, CLIENT_BS_PASSIVE_CHANGE_RECOGNISED)		//do it
				bChangedThisFrame = TRUE
			ENDIF	
		ELSE																										//if we've updated passive
			IF NOT IS_BIT_SET(serverBD.iBitset, SERVER_BS_PASSIVE_CHANGED)											//and that's been recognised
				CLEAR_BIT(playerBD[NATIVE_TO_INT(playerGangOwner)].iBitset, CLIENT_BS_PASSIVE_CHANGE_RECOGNISED)	//reset
			ENDIF	
		ENDIF
		
		IF bChangedThisFrame
			playerBD[NETWORK_PLAYER_ID_TO_INT()].iRelGroupSetupBitset = GENERATE_GANG_MERC_PLAYER_RELATIONSHIP_BITSET()
			#IF IS_DEBUG_BUILD
			DEBUG_PRINT_REL_GROUP_BIT_SET(playerBD[NETWORK_PLAYER_ID_TO_INT()].iRelGroupSetupBitset, playerGangOwner)
			#ENDIF
			SET_GANG_CALL_MERC_PLAYER_RELATIONSHIP(playerBD[NETWORK_PLAYER_ID_TO_INT()].iRelGroupSetupBitset)
		ENDIF
		
	ELSE
		IF playerBD[NETWORK_PLAYER_ID_TO_INT()].iRelGroupSetupBitset <> serverBD.iRelGroupSetupBitset
			playerBD[NETWORK_PLAYER_ID_TO_INT()].iRelGroupSetupBitset = serverBD.iRelGroupSetupBitset
			
			#IF IS_DEBUG_BUILD
			DEBUG_PRINT_REL_GROUP_BIT_SET(playerBD[NETWORK_PLAYER_ID_TO_INT()].iRelGroupSetupBitset, playerGangOwner)
			#ENDIF
			
			SET_GANG_CALL_MERC_PLAYER_RELATIONSHIP(playerBD[NETWORK_PLAYER_ID_TO_INT()].iRelGroupSetupBitset)
			bChangedThisFrame = TRUE
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Sets the server relationship group bitset equal to the owner player's relationship group bitset if they differ
PROC SERVER_MANAGE_GANG_CALL_MERC_PLAYER_RELATIONSHIPS()
	IF serverBD.iRelGroupSetupBitset <> playerBD[NATIVE_TO_INT(playerGangOwner)].iRelGroupSetupBitset
		serverBD.iRelGroupSetupBitset = playerBD[NATIVE_TO_INT(playerGangOwner)].iRelGroupSetupBitset
	ENDIF
ENDPROC

/// PURPOSE:
///    Cleans up a network ped
/// PARAMS:
///    netID - network ID of the ped to cleanup
///    bClearTasks - clears the ped's tasks before cleanup
PROC CLEANUP_NET_PED(NETWORK_INDEX netID, BOOL bClearTasks = TRUE)
	IF NETWORK_DOES_NETWORK_ID_EXIST(netID)
		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(netID)
			IF NOT IS_NET_PED_INJURED(netID)
				//SET_PED_KEEP_TASK(NET_TO_PED(netID), NOT bClearTasks)
				printstring("cleanup ped")
				printnl()
				IF IS_BIT_SET(serverBD.iThiefBitSet, FAIL_BS_TARGET_ON_FM_EVENT)
					CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === [CLEANUP_NET_PED] - FAIL_BS_TARGET_ON_FM_EVENT = TRUE")
					CLEAR_PED_TASKS(NET_TO_PED(netID))
					SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(netID), CA_ALWAYS_FLEE, TRUE)
					SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(netID), CA_FLEE_WHILST_IN_VEHICLE, FALSE)
					SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(netID), CA_USE_VEHICLE, FALSE)
				ELSE
					IF bClearTasks
						CLEAR_PED_TASKS(NET_TO_PED(netID))
					ENDIF
					SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(netID), CA_FLEE_WHILST_IN_VEHICLE, TRUE)
					SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(netID), CA_USE_VEHICLE, TRUE)
				ENDIF
			ENDIF
		ENDIF
		CLEANUP_NET_ID(netID)
	ENDIF
ENDPROC

/// PURPOSE:
///    Cleans up a network vehicle
/// PARAMS:
///    netID - network ID of the vehicle to cleanup
PROC CLEANUP_NET_VEH(NETWORK_INDEX netID)
	IF NETWORK_DOES_NETWORK_ID_EXIST(netID)
		//IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(netID)
			CLEANUP_NET_ID(netID)
		//ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Cleans up the entities used in the script
PROC CLEANUP_ENTITIES()
	INT pedCount = 0
	
	BOOL bClearTasks
	
	REPEAT GET_NUMBER_OF_GANG_MEMBERS_IN_GANG(iGangVariation) pedCount
		bClearTasks = FALSE
		IF iGangVariation = GANG_CALL_TYPE_SPECIAL_THIEF
			IF (NOT IS_BIT_SET(serverBD.iThiefBitSet, THIEF_BS_SOMEONE_ROBBED))
				bClearTasks = TRUE
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === Cleaning up gang ped ", pedCount, " bClearTasks = ", bClearTasks)
		#ENDIF
		
		CLEANUP_NET_PED(serverBD.gangMember[pedCount].netID, bClearTasks)
	ENDREPEAT
	
ENDPROC

/// PURPOSE:
///   Everything required to clean up, apart from terminating the thread. Used to reset the mission. 
PROC SCRIPT_TIDY_UP()
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === SCRIPT_TIDY_UP CALLED")
	#ENDIF
	
	BOOL bEscaped = IS_BIT_SET(serverBD.iThiefBitSet, THIEF_BS_TARGET_ESCAPED)
	BOOL bNoPath = IS_BIT_SET(serverBD.iThiefBitSet, THIEF_BS_NO_PATH)
	BOOL bGangMemberDead
	
	INT i
	
	CLEANUP_ENTITIES()
	
	IF iGangVariation <> GANG_CALL_TYPE_SPECIAL_MERRYWEATHER		//leave army setup lingering so they dont attack the caller, it's only used in this script
		if IS_BIT_SET(serverBD.iFailBitset, fail_bs_gang_attack_on_same_gang_members) //only want to clear the gang relationship if the player has entered a gang attack with the same gang members
			CLEAR_GANG_CALL_MERC_PLAYER_RELATIONSHIP()
		endif 
	ENDIF
	
	IF DOES_BLIP_EXIST(aiBlipThief.BlipID)
		CLEANUP_AI_PED_BLIP(aiBlipThief)
	ENDIF
	
	REPEAT GET_NUMBER_OF_GANG_MEMBERS_IN_GANG(iGangVariation) i
		iLocalGangMemberBitset[i] = 0
	ENDREPEAT
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()		
		serverBD.iSuccessThiefID = -1
		serverBD.timeStarted = timeNet
		serverBD.timeThiefEscape = timeNet
		serverBD.timeThiefAbandon = timeNet
		serverBD.iBitset = 0				//Wipe the bitsets for reset
		serverBD.iPassiveBitset = 0
		serverBD.iThiefBitset = 0
		REPEAT GET_NUMBER_OF_GANG_MEMBERS_IN_GANG(iGangVariation) i
			bGangMemberDead = IS_BIT_SET(serverBD.gangMember[i].iBitSet, GANG_BS_DEAD)
			serverBD.gangMember[i].iBitSet = 0
			serverBD.gangMember[i].iAmbientPedArrayEntry = -1
			IF bGangMemberDead
				SET_BIT(serverBD.gangMember[i].iBitSet, GANG_BS_DEAD)
			ENDIF
		ENDREPEAT
	ENDIF
	playerBD[NETWORK_PLAYER_ID_TO_INT()].iBitset = 0
	playerBD[NETWORK_PLAYER_ID_TO_INT()].iSuccessThiefID = -1
	iLocalBitset = 0
	
	#IF IS_DEBUG_BUILD
	REPEAT GET_NUMBER_OF_GANG_MEMBERS_IN_GANG(iGangVariation) i
		IF DOES_BLIP_EXIST(blipDebug[i])
			REMOVE_BLIP(blipDebug[i])
		ENDIF
	ENDREPEAT
	CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === SCRIPT_TIDY_UP DONE")
	#ENDIF
	
	IF bEscaped
		SET_BIT(serverBD.iThiefBitSet, THIEF_BS_TARGET_ESCAPED)
	ENDIF
	IF bNoPath
		SET_BIT(serverBD.iThiefBitSet, THIEF_BS_WAITING_FOR_NO_PATH_RESET)
		SET_BIT(iLocalBitset, LOCAL_BS_TICKER_THIEF_NO_PATH)
	ENDIF
	
	SET_BIT(iLocalBitset, LOCAL_BS_CLIENT_NEAR_A_THIEF)
	SET_BIT(iLocalBitset, LOCAL_BS_TICKER_GANG_SUCCESSFULLY_REQUESTED)	
ENDPROC

/// PURPOSE:
///    Terminates the script
PROC SCRIPT_TERMINATE()
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === SCRIPT_TERMINATE DONE")
	#ENDIF
	
	IF serverBD.bIsHitSquad
		IF PLAYER_ID() = playerGangOwner
			GB_CLEAR_GLOBAL_CLIENT_BIT1(eGB_GLOBAL_CLIENT_BITSET_1_LAUNCHING_HIT_SQUAD)
			
		ELIF PLAYER_ID() = playerGangTarget
			GB_CLEAR_GLOBAL_CLIENT_BIT1(eGB_GLOBAL_CLIENT_BITSET_1_ABANDONED_BIKER_GANG)
		ENDIF
	ELIF serverBD.bIsAssassins

	ENDIF
	
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

/// PURPOSE:
///    Cleans up and terminates the script
PROC mission_cleanup()
	
	CLEAR_BIT(GlobalplayerBD_FM[NETWORK_PLAYER_ID_TO_INT()].iGangCallOwnerBitset, iGangVariation)
	GlobalplayerBD_FM[NETWORK_PLAYER_ID_TO_INT()].iGangCallTargetID = -1
	
	IF iGangVariation = GANG_CALL_TYPE_SPECIAL_MERRYWEATHER
		IF IS_INCIDENT_VALID(incidentArmy)
			DELETE_INCIDENT(incidentArmy)
		ENDIF
	ENDIF
	
	SCRIPT_TIDY_UP()
		
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === mission_cleanup DONE")
	#ENDIF
	
	SCRIPT_TERMINATE()
ENDPROC

/// PURPOSE:
///    Checks if the player is performing an action that would abandon a gang member
/// PARAMS:
///    iGangID - gang member ID to check
/// RETURNS:
///    true/false
FUNC BOOL IS_GANG_MEMBER_ABANDONED(INT iGangID)

	IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.gangMember[iGangID].netID)
	
		IF IS_PLAYER_IN_PROPERTY(playerGangTarget, TRUE, TRUE)
		OR IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(playerGangTarget)
			RETURN TRUE
		ENDIF
		
		IF GlobalplayerBD[NATIVE_TO_INT(playerGangTarget)].bUsingTaxi
			RETURN TRUE
		ENDIF
		
	ENDIF
		
	RETURN FALSE
ENDFUNC

#IF IS_DEBUG_BUILD

/// PURPOSE:
///    Gets a string version of a navmesh route result
/// PARAMS:
///    thisResult - navmesh route result to turn to string
/// RETURNS:
///    NAVMESH_ROUTE_RESULT as a STRING
FUNC STRING GET_NAVMESH_ROUTE_RESULT_STRING(NAVMESH_ROUTE_RESULT thisResult)
	SWITCH thisResult
		CASE NAVMESHROUTE_TASK_NOT_FOUND
			RETURN "NAVMESHROUTE_TASK_NOT_FOUND"
		BREAK
		CASE NAVMESHROUTE_ROUTE_NOT_YET_TRIED
			RETURN "NAVMESHROUTE_ROUTE_NOT_YET_TRIED"
		BREAK
		CASE NAVMESHROUTE_ROUTE_NOT_FOUND
			RETURN "NAVMESHROUTE_ROUTE_NOT_FOUND"
		BREAK
		CASE NAVMESHROUTE_ROUTE_FOUND
			RETURN "NAVMESHROUTE_ROUTE_FOUND"
		BREAK
	ENDSWITCH
	RETURN "INVALID"
ENDFUNC

#ENDIF

/// PURPOSE:
///    Processes the server-side decision making for a gang member - expensive, should be staggered
/// PARAMS:
///    iGangID - ID of the gang member to process
PROC MANAGE_GANG_MEMBER_BRAIN(INT iGangID)
	PED_INDEX thisPed
	INT i

	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.gangMember[iGangID].netID)
			
			thisPed = NET_TO_PED(serverBD.gangMember[iGangID].netID)
		
			IF NOT IS_ENTITY_DEAD(thisPed)
				
				IF NOT IS_BIT_SET(serverBD.gangMember[iGangID].iBitSet, GANG_BS_ATTRIBUTE_DAMAGE_SET)
					BOOL bAttributeDamageSet = FALSE
					SWITCH serverBD.gangMember[iGangID].iGang
						CASE GANG_CALL_TYPE_SPECIAL_THIEF
							IF IS_ATTRIBUTE_DAMAGE_DECORATOR_BIT_SET(thisPed, ATTRIBUTE_DAMAGE_BS_THIEVES)
							 	bAttributeDamageSet = TRUE
							ENDIF
						BREAK
					ENDSWITCH
					IF bAttributeDamageSet
						SET_BIT(serverBD.gangMember[iGangID].iBitSet, GANG_BS_ATTRIBUTE_DAMAGE_SET)
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === SET_BIT(serverBD.gangMember[", iGangID, "].iBitSet, GANG_BS_ATTRIBUTE_DAMAGE_SET)")
						#ENDIF
					ENDIF
				ENDIF
				
				SWITCH serverBD.gangMember[iGangID].gangStage
					CASE GANG_STAGE_DO_NOTHING
						
					BREAK
					
					CASE GANG_STAGE_THIEF_APPROACH
					
						IF serverBD.gangMember[iGangID].gangClass = GANG_CLASS_THIEF
							#IF IS_DEBUG_BUILD
							//CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === iGangID = ", iGangID, " GET_NAVMESH_ROUTE_RESULT = ", GET_NAVMESH_ROUTE_RESULT_STRING(GET_NAVMESH_ROUTE_RESULT(thisPed)))
							#ENDIF
							IF NOT IS_BIT_SET(serverBD.gangMember[iGangID].iBitSet, GANG_BS_NO_PATH_TIMER_SET)
								
//								if muggers_route_to_target_found()
//								
//								func bool muggers_route_to_target_not_found()
//
//									if get_script_task_status(thisPed, script_task_enter_vehicle) = performing_task
//	
//										
//										IF GET_NAVMESH_ROUTE_RESULT(thisPed) = NAVMESHROUTE_ROUTE_NOT_FOUND
//								
//										
//										NAVMESHROUTE_TASK
//										
//									endif 
//										
//									return false
//			
//								endfunc 
								
								
								IF GET_NAVMESH_ROUTE_RESULT(thisPed) = NAVMESHROUTE_ROUTE_NOT_FOUND
								//OR (GlobalplayerBD[NATIVE_TO_INT(playerGangOwner)].iCurrentShop != -1 AND GET_SHOP_TYPE_ENUM(INT_TO_ENUM(SHOP_NAME_ENUM, GlobalplayerBD[NATIVE_TO_INT(playerGangOwner)].iCurrentShop)) = SHOP_TYPE_CARMOD)
								OR (GlobalplayerBD[NATIVE_TO_INT(playerGangTarget)].iCurrentShop != -1 AND GET_SHOP_TYPE_ENUM(INT_TO_ENUM(SHOP_NAME_ENUM, GlobalplayerBD[NATIVE_TO_INT(playerGangTarget)].iCurrentShop)) = SHOP_TYPE_CARMOD)
								OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerGangTarget)].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bUsingFerrisWheel)//GlobalplayerBD_FM[NATIVE_TO_INT(playerGangTarget)].g_bUsingFerrisWheel
								OR IS_PLAYER_IN_PERSONAL_VEHICLE(playerGangTarget)
								OR GB_IS_PLAYER_IN_GANG_BOSS_LIMO(playerGangTarget) 
								//OR GET_NAVMESH_ROUTE_RESULT(thisPed) = NAVMESHROUTE_TASK_NOT_FOUND
									serverBD.gangMember[iGangID].timeNoPath = timeNet
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === SET_BIT(serverBD.gangMember[", iGangID, "].iBitSet, GANG_BS_NO_PATH_TIMER_SET)")
									#ENDIF
									SET_BIT(serverBD.gangMember[iGangID].iBitSet, GANG_BS_NO_PATH_TIMER_SET)
								ENDIF
								
							ELSE
								IF GET_NAVMESH_ROUTE_RESULT(thisPed) != NAVMESHROUTE_ROUTE_NOT_FOUND
								//AND (GlobalplayerBD[NATIVE_TO_INT(playerGangOwner)].iCurrentShop = -1)
								AND (GlobalplayerBD[NATIVE_TO_INT(playerGangTarget)].iCurrentShop = -1)
								AND NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerGangTarget)].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bUsingFerrisWheel)//GlobalplayerBD_FM[NATIVE_TO_INT(playerGangTarget)].g_bUsingFerrisWheel
								AND NOT IS_PLAYER_IN_PERSONAL_VEHICLE(playerGangTarget)
								AND NOT GB_IS_PLAYER_IN_GANG_BOSS_LIMO(playerGangTarget) 
								//AND GET_NAVMESH_ROUTE_RESULT(thisPed) <> NAVMESHROUTE_TASK_NOT_FOUND
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === CLEAR_BIT(serverBD.gangMember[", iGangID, "].iBitSet, GANG_BS_NO_PATH_TIMER_SET)")
									#ENDIF
									CLEAR_BIT(serverBD.gangMember[iGangID].iBitSet, GANG_BS_NO_PATH_TIMER_SET)
								ELSE
									IF GET_TIME_DIFFERENCE(timeNet, serverBD.gangMember[iGangID].timeNoPath) > GANG_CALL_THIEF_NO_PATH_TIME
									OR GET_NAVMESH_ROUTE_RESULT(thisPed) != NAVMESHROUTE_ROUTE_NOT_FOUND
										IF NOT IS_BIT_SET(serverBD.iThiefBitSet, THIEF_BS_NO_PATH)
											#IF IS_DEBUG_BUILD
											CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === SET_BIT(serverBD.iThiefBitSet, THIEF_BS_NO_PATH)")
											#ENDIF
											SET_BIT(serverBD.iThiefBitSet, THIEF_BS_NO_PATH)
											
											serverBD.vThiefNoPathCoord = GET_PLAYER_COORDS(playerGangTarget)
											
											i = 0
											REPEAT GET_NUMBER_OF_GANG_MEMBERS_IN_GANG(iGangVariation) i
												SET_GANG_MEMBER_STAGE(i, GANG_STAGE_THIEF_FLEE)
											ENDREPEAT
											
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
//							IF GlobalplayerBD[NATIVE_TO_INT(playerGangOwner)].iCurrentShop != -1
//								CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === GlobalplayerBD[NATIVE_TO_INT(playerGangOwner)].iCurrentShop != -1")
//								IF GET_SHOP_TYPE_ENUM(INT_TO_ENUM(SHOP_NAME_ENUM, GlobalplayerBD[NATIVE_TO_INT(playerGangOwner)].iCurrentShop)) = SHOP_TYPE_CARMOD
//									CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === GET_SHOP_TYPE_ENUM(INT_TO_ENUM(SHOP_NAME_ENUM, GlobalplayerBD[NATIVE_TO_INT(playerGangOwner)].iCurrentShop)) = SHOP_TYPE_CARMOD")
//									SET_BIT(MPGlobalsAmbience.iThiefBitSet, THIEF_RETURN)
//									SET_GANG_MEMBER_STAGE(iGangID, GANG_STAGE_THIEF_FLEE)
//								ENDIF
//							ENDIF
//							
//							IF GlobalplayerBD[NATIVE_TO_INT(playerGangTarget)].iCurrentShop != -1
//								CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === GlobalplayerBD[NATIVE_TO_INT(playerGangTarget)].iCurrentShop != -1")
//								IF GET_SHOP_TYPE_ENUM(INT_TO_ENUM(SHOP_NAME_ENUM, GlobalplayerBD[NATIVE_TO_INT(playerGangTarget)].iCurrentShop)) = SHOP_TYPE_CARMOD
//									CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === GET_SHOP_TYPE_ENUM(INT_TO_ENUM(SHOP_NAME_ENUM, GlobalplayerBD[NATIVE_TO_INT(playerGangTarget)].iCurrentShop)) = SHOP_TYPE_CARMOD")
//									SET_BIT(MPGlobalsAmbience.iThiefBitSet, THIEF_RETURN)
//									SET_GANG_MEMBER_STAGE(iGangID, GANG_STAGE_THIEF_FLEE)
//								ENDIF
//							ENDIF
							
							IF IS_BIT_SET(serverBD.gangMember[iGangID].iBitSet, GANG_BS_THIEF_BROKEN_COVER)		//set up server side sprint
								IF NOT IS_BIT_SET(serverBD.gangMember[iGangID].iBitSet, GANG_BS_THIEF_SET_UP_SPRINT)
									IF GET_PED_DESIRED_MOVE_BLEND_RATIO(thisPed) = PEDMOVEBLENDRATIO_SPRINT
									AND NOT GET_PED_CONFIG_FLAG(thisPed, PCF_RunFromFiresAndExplosions)
										SET_BIT(serverBD.gangMember[iGangID].iBitSet, GANG_BS_THIEF_SET_UP_SPRINT)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					
						IF IS_BIT_SET(serverBD.iThiefBitSet, THIEF_BS_SOMEONE_ROBBED)
							SET_BIT(serverBD.gangMember[iGangID].iBitSet, GANG_BS_THIEF_BROKEN_COVER)
							SET_GANG_MEMBER_STAGE(iGangID, GANG_STAGE_THIEF_FLEE)
						ELSE
							IF IS_NET_PLAYER_OK(playerGangTarget)
								IF NOT IS_BIT_SET(serverBD.gangMember[iGangID].iBitSet, GANG_BS_THIEF_BROKEN_COVER)
									IF ARE_TWO_POINTS_WITHIN_SQUARE_DISTANCE(	GET_ENTITY_COORDS(thisPed), 
																				GET_ENTITY_COORDS(GET_PLAYER_PED(playerGangTarget)), 
																				MIN_DISTANCE_THIEF_TO_BREAK_COVER)
									OR NOT ARE_TWO_POINTS_WITHIN_SQUARE_DISTANCE(	GET_ENTITY_COORDS(thisPed), 
																					GET_ENTITY_COORDS(GET_PLAYER_PED(playerGangTarget)), 
																					MIN_DISTANCE_THIEF_TO_AMBIENT_GRAB)	//if too far from player, start running
										SET_BIT(serverBD.gangMember[iGangID].iBitSet, GANG_BS_THIEF_BROKEN_COVER)
									ENDIF
								ENDIF
								
								
								
								IF CHECK_THIEF_DISTANCE(GET_ENTITY_COORDS(thisPed), GET_ENTITY_COORDS(GET_PLAYER_PED(playerGangTarget)))									
								OR (IS_PED_JACKING(thisPed) AND GET_JACK_TARGET(thisPed) = GET_PLAYER_PED(playerGangTarget))
									SET_GANG_MEMBER_STAGE(iGangID, GANG_STAGE_THIEF_STEAL)
								ENDIF
							ENDIF
						ENDIF
					BREAK
				
					CASE GANG_STAGE_THIEF_STEAL
					
						
						IF IS_BIT_SET(serverBD.iThiefBitSet, THIEF_BS_SOMEONE_ROBBED)
							SET_BIT(serverBD.gangMember[iGangID].iBitSet, GANG_BS_THIEF_BROKEN_COVER)
							SET_GANG_MEMBER_STAGE(iGangID, GANG_STAGE_THIEF_FLEE)
						ELSE
							IF IS_NET_PLAYER_OK(playerGangTarget)
								IF NOT CHECK_THIEF_DISTANCE(GET_ENTITY_COORDS(thisPed), GET_ENTITY_COORDS(GET_PLAYER_PED(playerGangTarget)))
								AND NOT (IS_PED_JACKING(thisPed) AND GET_JACK_TARGET(thisPed) = GET_PLAYER_PED(playerGangTarget))
									SET_GANG_MEMBER_STAGE(iGangID, GANG_STAGE_THIEF_APPROACH)
								ENDIF
							ENDIF
						ENDIF
					BREAK
					
					CASE GANG_STAGE_THIEF_FLEE		//set up server side flee
						IF serverBD.gangMember[iGangID].gangClass = GANG_CLASS_THIEF
						
//							IF IS_BIT_SET(MPGlobalsAmbience.iThiefBitSet, THIEF_RETURN)
//								CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === IS_BIT_SET(MPGlobalsAmbience.iThiefBitSet, THIEF_RETURN)")
//								IF GlobalplayerBD[NATIVE_TO_INT(playerGangTarget)].iCurrentShop = -1
//								AND GlobalplayerBD[NATIVE_TO_INT(playerGangOwner)].iCurrentShop = -1
//									CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === CLEAR_BIT(MPGlobalsAmbience.iThiefBitSet, THIEF_RETURN)")
//									CLEAR_BIT(MPGlobalsAmbience.iThiefBitSet, THIEF_RETURN)
//									SET_GANG_MEMBER_STAGE(iGangID, GANG_STAGE_THIEF_APPROACH)
//								ENDIF
//							ENDIF
						
							IF NOT IS_BIT_SET(serverBD.gangMember[iGangID].iBitSet, GANG_BS_THIEF_SET_UP_FLEE)
								IF GET_PED_CONFIG_FLAG(thisPed, PCF_RunFromFiresAndExplosions)
									SET_BIT(serverBD.gangMember[iGangID].iBitSet, GANG_BS_THIEF_SET_UP_FLEE)
								ENDIF
							ENDIF
						ENDIF
					BREAK
					
					CASE GANG_STAGE_CLEANUP
						
					BREAK
				ENDSWITCH
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Returns if the ped is sitting in a vehicle that has been locally cloned (either owned by this machine, or cloned from another machine)
/// PARAMS:
///    thisPlayer - player to check
/// RETURNS:
///    true/false
FUNC BOOL IS_PLAYER_IN_LOCALLY_CLONED_VEHICLE(PLAYER_INDEX thisPlayer)
	
	IF IS_REMOTE_PLAYER_IN_NON_CLONED_VEHICLE(thisPlayer)
		RETURN FALSE
	ENDIF
	
	//IF IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(thisPlayer), TRUE)
	if is_ped_sitting_in_any_vehicle(GET_PLAYER_PED(thisPlayer))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if the ped supplied is in a vehicle, and if it is suitable for an AI to use it to escape with
/// PARAMS:
///    thisPed - ped to check
/// RETURNS:
///    true/false
FUNC BOOL IS_VEHICLE_PED_IS_STEALING_SUITABLE_TO_ESCAPE_WITH(PED_INDEX thisPed)
	VEHICLE_INDEX thisVehicle
	
	IF IS_PED_IN_ANY_VEHICLE(thisPed, TRUE)
		thisVehicle = GET_VEHICLE_PED_IS_IN(thisPed, TRUE)
	ELSE
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === IS_VEHICLE_PED_IS_STEALING_SUITABLE_TO_ESCAPE_WITH thisped IS_PED_IN_ANY_VEHICLE = FALSE")
		#ENDIF
	ENDIF
	
	IF IS_PED_JACKING(thisPed)
		thisVehicle = GET_VEHICLE_PED_IS_ENTERING(thisPed)
	ELSE
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === IS_VEHICLE_PED_IS_STEALING_SUITABLE_TO_ESCAPE_WITH IS_PED_JACKING = FALSE")
		#ENDIF
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(thisVehicle)
		IF IS_PLAYER_IN_LOCALLY_CLONED_VEHICLE(playerGangTarget)
			thisVehicle = GET_VEHICLE_PED_IS_IN(GET_PLAYER_PED(playerGangTarget), TRUE)
		ELSE
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === IS_VEHICLE_PED_IS_STEALING_SUITABLE_TO_ESCAPE_WITH playerGangTarget IS_PED_IN_ANY_VEHICLE = FALSE")
			#ENDIF
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(thisVehicle)
		IF IS_VEHICLE_FUCKED(thisVehicle)
			RETURN FALSE
		ELSE
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === IS_VEHICLE_PED_IS_STEALING_SUITABLE_TO_ESCAPE_WITH IS_VEHICLE_FUCKED = FALSE")
			#ENDIF
		ENDIF
		
		IF IS_PED_IN_ANY_BOAT(thisPed)
		OR IS_THIS_MODEL_A_BOAT(GET_ENTITY_MODEL(thisVehicle))		//beached boat
			IF NOT IS_ENTITY_IN_WATER(thisVehicle)
				RETURN FALSE
			ELSE
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === IS_VEHICLE_PED_IS_STEALING_SUITABLE_TO_ESCAPE_WITH IS_ENTITY_IN_WATER = TRUE")
				#ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === IS_VEHICLE_PED_IS_STEALING_SUITABLE_TO_ESCAPE_WITH is a boat = FALSE")
			#ENDIF
		ENDIF
		
		IF GET_ENTITY_MODEL(thisVehicle) = TRAILERSMALL2
			CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === IS_VEHICLE_PED_IS_STEALING_SUITABLE_TO_ESCAPE_WITH - FALSE - trailer")
			RETURN FALSE
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === IS_VEHICLE_PED_IS_STEALING_SUITABLE_TO_ESCAPE_WITH DOES_ENTITY_EXIST = FALSE")
		#ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE: To request a cash transaction from the server and wait for the response
///    
/// PARAMS:
///    service - The service being paid for
///    iCost - The cost
/// RETURNS: True once the transaction is complete or has timed out, false otherwise
///    
FUNC BOOL IS_MUGGER_TRANSACTION_READY(INT iCost)

	IF NOT IS_BIT_SET(iLocalBitset, LOCAL_BS_TRANSACTION_PENDING)
		NETWORK_REQUEST_CASH_TRANSACTION(iCurrentTransactionIndex,NET_SHOP_TTYPE_SERVICE,NET_SHOP_ACTION_SPEND,CATEGORY_SERVICE_WITH_THRESHOLD,SERVICE_SPEND_ROBBED_BY_MUGGER,iCost,CATALOG_ITEM_FLAG_BANK_THEN_WALLET)
		REINIT_NET_TIMER(transactionTimer)
		SET_BIT(iLocalBitset, LOCAL_BS_TRANSACTION_PENDING)
		#IF IS_DEBUG_BUILD
		IF bOutPutTransactionDebug
		OR TRUE
			CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === [DEBUG] IS_MUGGER_TRANSACTION_READY: SET_BIT(contactStruct.iBS, CR_BS_TRANSACTION_PENDING)")
		ENDIF
		#ENDIF
	ELSE
		IF IS_CASH_TRANSACTION_COMPLETE(iCurrentTransactionIndex)
		OR HAS_NET_TIMER_EXPIRED(transactionTimer,TRANSACTION_TIMEOUT)
			#IF IS_DEBUG_BUILD
			IF bOutPutTransactionDebug
			OR TRUE
				CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === [DEBUG] IS_MUGGER_TRANSACTION_READY: CLEAR_BIT(contactStruct.iBS, CR_BS_TRANSACTION_PENDING)")
			ENDIF
			#ENDIF
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

/// PURPOSE: To manage contact request transactions that must be approved by the server
///    
/// PARAMS:
///    service - The service being paid for
///    iCost - The cost
///    bContactServer - Whether the contacts request server will need to clear data once done 
/// RETURNS: -1 If transaction still pending
///    		 0 If transaction is unsuccesful
///    		 1 If transaction successful (or not using server transactions)    		 
///    
FUNC INT GET_MUGGER_TRANSACTION_STATUS(INT iCost)

	#IF IS_DEBUG_BUILD
	IF bForceTransactionFail
		iCost = 0
		CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === [DEBUG] IS_CONTACT_REQUEST_TRANSACTION_SUCCESSFUL: Force failing transactions!")
	ENDIF	
	#ENDIF
	
	IF USE_SERVER_TRANSACTIONS()
		IF IS_MUGGER_TRANSACTION_READY(iCost)
			IF GET_CASH_TRANSACTION_STATUS(iCurrentTransactionIndex) = CASH_TRANSACTION_STATUS_SUCCESS
			
				NET_GAMESERVER_SET_TELEMETRY_NONCE_SEED(GET_CASH_TRANSACTION_ID_FROM_INDEX(iCurrentTransactionIndex))
				
				NETWORK_SPENT_ROBBED_BY_MUGGER(iCost)
				
				CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === IS_CONTACT_REQUEST_TRANSACTION_SUCCESSFUL: ",iCurrentTransactionIndex," ",GET_CASH_TRANSACTION_SERVICE_NAME(SERVICE_SPEND_ROBBED_BY_MUGGER)," $",iCost,": Success! [USE_SERVER_TRANSACTIONS]")
				DELETE_CASH_TRANSACTION(iCurrentTransactionIndex)
				RETURN TRANSACTION_SUCCESS //Success
			ELSE
				CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === IS_CONTACT_REQUEST_TRANSACTION_SUCCESSFUL: ",iCurrentTransactionIndex," ",GET_CASH_TRANSACTION_SERVICE_NAME(SERVICE_SPEND_ROBBED_BY_MUGGER)," $",iCost,": Fail! [USE_SERVER_TRANSACTIONS]")
				DELETE_CASH_TRANSACTION(iCurrentTransactionIndex)
				RETURN TRANSACTION_FAILED //Fail
			ENDIF
		ENDIF
	ELSE
		CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === IS_CONTACT_REQUEST_TRANSACTION_SUCCESSFUL: ",GET_CASH_TRANSACTION_SERVICE_NAME(SERVICE_SPEND_ROBBED_BY_MUGGER)," $",iCost,": Success!")
		RETURN TRANSACTION_SUCCESS //Success
	ENDIF
	
	RETURN TRANSACTION_PENDING //Wait
	
ENDFUNC

PROC MAINTAIN_ROB_TRANSACTION(PED_INDEX muggerPed)
	
	IF IS_BIT_SET(iLocalBitset,LOCAL_BS_DO_TRANSACTION)
	
		INT iStolenCash  = playerBD[NETWORK_PLAYER_ID_TO_INT()].iCashStolen

		INT iStatus
		iStatus = GET_MUGGER_TRANSACTION_STATUS(iStolenCash) 
		IF iStatus = TRANSACTION_SUCCESS
			CLEAR_BIT(iLocalBitset, LOCAL_BS_TRANSACTION_PENDING)
			CLEAR_BIT(iLocalBitset,LOCAL_BS_DO_TRANSACTION)
					
			
			IF !USE_SERVER_TRANSACTIONS()			
				NETWORK_SPENT_ROBBED_BY_MUGGER(iStolenCash)
			ENDIF
			
			// Added by kevin to fix 1932608
			IF iStolenCash > g_sMPTunables.iMugger_Cash_Drop_Cap
				iStolenCash = g_sMPTunables.iMugger_Cash_Drop_Cap
				NET_PRINT_TIME() NET_PRINT(" MANAGE_GANG_MEMBER_BODY  - a - CASH ABOVE LIMIT SO CAPPED AT $") NET_PRINT_INT(iStolenCash) NET_NL()
			ENDIF
					
			println("AM_GANG_CALL - MUGGER CASH IS ",iStolenCash)
			SET_PED_MONEY(muggerPed, iStolenCash)			

			SET_BIT(playerBD[NETWORK_PLAYER_ID_TO_INT()].iBitset,CLIENT_BS_TRANSACTION_SUCCESS)

		ELIF iStatus = TRANSACTION_FAILED
			CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === Transaction failed!")
			CLEAR_BIT(iLocalBitset, LOCAL_BS_TRANSACTION_PENDING)
			CLEAR_BIT(iLocalBitset,LOCAL_BS_DO_TRANSACTION)
			
			SET_BIT(playerBD[NETWORK_PLAYER_ID_TO_INT()].iBitset,CLIENT_BS_TRANSACTION_FAIL)
		ELSE
			EXIT
		ENDIF
		
	ENDIF

ENDPROC

/// PURPOSE:
///    Processes the local logic for gang members owned by the local machine (generally ped tasks) - expensive, should be staggered
/// PARAMS:
///    iGangID - gang member ID to process
PROC MANAGE_GANG_MEMBER_BODY(INT iGangID)
	PED_INDEX thisPed
	VEHICLE_INDEX currentVehicle
	INT iStolenCash = 0
	VECTOR vPushDirection = <<0,0,0>>
	
	BOOL bTakeControl = FALSE
	
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.gangMember[iGangID].netID)
	
		thisPed = NET_TO_PED(serverBD.gangMember[iGangID].netID)
	
		//Take control if gang member is thief, I am the target and the thief is set to steal
		IF iGangVariation = GANG_CALL_TYPE_SPECIAL_THIEF
			IF PLAYER_ID() = playerGangTarget
				IF serverBD.gangMember[iGangID].gangClass = GANG_CLASS_THIEF
					IF serverBD.gangMember[iGangID].gangStage = GANG_STAGE_THIEF_STEAL
						bTakeControl = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF PLAYER_ID() = playerGangOwner
			IF IS_BIT_SET(serverBD.gangMember[iGangID].iBitSet, GANG_BS_CREATED_AND_SET_UP)
				IF NOT IS_BIT_SET(iLocalGangMemberBitset[iGangID], LOCAL_GANG_BS_GANG_REL_INITIALISED)
					SET_BIT(g_iBuddyChangeBitset, GLOBAL_BUDDY_CHANGE_BS_GANG_CALL)
					SET_BIT(iLocalGangMemberBitset[iGangID], LOCAL_GANG_BS_GANG_REL_INITIALISED)
				ENDIF
				
				IF NOT IS_BIT_SET(serverBD.gangMember[iGangID].iBitSet, GANG_BS_ATTRIBUTE_DAMAGE_SET)
					IF NOT IS_BIT_SET(iLocalGangMemberBitset[iGangID], LOCAL_GANG_BS_ATTRIBUTE_DAMAGE_SET)
						bTakeControl = TRUE
					ENDIF
				ELSE
					IF IS_BIT_SET(iLocalGangMemberBitset[iGangID], LOCAL_GANG_BS_ATTRIBUTE_DAMAGE_SET)
						CLEAR_BIT(iLocalGangMemberBitset[iGangID], LOCAL_GANG_BS_ATTRIBUTE_DAMAGE_SET)
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
		
		IF bTakeControl
			IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.gangMember[iGangID].netID)
				IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.gangMember[iGangID].netID)
					NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.gangMember[iGangID].netID)
				ENDIF
			ENDIF
		ENDIF
		
		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.gangMember[iGangID].netID)
			
			IF PLAYER_ID() = playerGangOwner
				IF NOT IS_BIT_SET(serverBD.gangMember[iGangID].iBitSet, GANG_BS_ATTRIBUTE_DAMAGE_SET)
				AND NOT IS_BIT_SET(iLocalGangMemberBitset[iGangID], LOCAL_GANG_BS_ATTRIBUTE_DAMAGE_SET)
					IF NETWORK_SET_ATTRIBUTE_DAMAGE_TO_PLAYER(thisPed, PLAYER_ID())
						SWITCH serverBD.gangMember[iGangID].iGang
							CASE GANG_CALL_TYPE_SPECIAL_THIEF
								SET_ATTRIBUTE_DAMAGE_DECORATOR_BIT(thisPed, ATTRIBUTE_DAMAGE_BS_THIEVES)
							BREAK
						ENDSWITCH
						
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === SET_BIT(iLocalGangMemberBitset[", iGangID, "], LOCAL_GANG_BS_ATTRIBUTE_DAMAGE_SET)")
						#ENDIF
						
						SET_BIT(iLocalGangMemberBitset[iGangID], LOCAL_GANG_BS_ATTRIBUTE_DAMAGE_SET)
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(thisPed)
			AND NOT IS_PED_INJURED(thisPed)
				
				printstring("gang ped = ")
				printint(iGangID)
				printstring(" serverBD.gangMember[iGangID].gangStage = ")
				printint(enum_to_int(serverBD.gangMember[iGangID].gangStage))
				printnl()
				
				
							
				SWITCH serverBD.gangMember[iGangID].gangStage
					
					CASE GANG_STAGE_DO_NOTHING
						
					BREAK
					
					CASE GANG_STAGE_THIEF_APPROACH
						SET_PED_RESET_FLAG(thisPed,PRF_PreventAllMeleeTakedowns,TRUE)
					
						//Set up ped sprint
						IF serverBD.gangMember[iGangID].gangClass = GANG_CLASS_THIEF
							IF IS_BIT_SET(serverBD.gangMember[iGangID].iBitSet, GANG_BS_THIEF_BROKEN_COVER)
								IF NOT IS_BIT_SET(serverBD.gangMember[iGangID].iBitSet, GANG_BS_THIEF_SET_UP_SPRINT)
									SET_PED_DESIRED_MOVE_BLEND_RATIO(thisPed, PEDMOVEBLENDRATIO_SPRINT)
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(thisPed, TRUE)
									SET_PED_CONFIG_FLAG(thisPed, PCF_RunFromFiresAndExplosions, FALSE)
								ENDIF
							ENDIF
						ENDIF
						
						IF PLAYER_ID() = playerGangTarget AND ARE_TWO_POINTS_WITHIN_SQUARE_DISTANCE(	GET_ENTITY_COORDS(thisPed), 
																			GET_ENTITY_COORDS(GET_PLAYER_PED(playerGangTarget)), 
																			MIN_DISTANCE_THIEF_TO_BREAK_COVER)
							SET_BIT(MPGlobalsAmbience.iThiefBitSet, THIEF_NEAR_PLAYER)

						ELSE
							CLEAR_BIT(MPGlobalsAmbience.iThiefBitSet, THIEF_NEAR_PLAYER)

						ENDIF
						
						IF NOT IS_PED_FLEEING(thisPed)
							IF NOT IS_PED_RESPONDING_TO_EVENT(thisPed, EVENT_GUN_AIMED_AT)
								IF IS_NET_PLAYER_OK(playerGangTarget)
									IF NOT IS_BIT_SET(serverBD.iThiefBitSet, FAIL_BS_TARGET_ON_FM_EVENT)
										IF IS_PLAYER_IN_LOCALLY_CLONED_VEHICLE(playerGangTarget)
										AND NOT IS_PED_HANGING_ON_TO_VEHICLE(GET_PLAYER_PED(playerGangTarget))
											IF NOT IS_TASK_ONGOING(thisPed, SCRIPT_TASK_ENTER_VEHICLE)
												IF IS_PED_ALRIGHT_FOR_ACTION(thisPed)
													currentVehicle = GET_VEHICLE_PED_IS_IN(GET_PLAYER_PED(playerGangTarget))
													IF DOES_ENTITY_EXIST(currentVehicle)
														IF IS_VEHICLE_DRIVEABLE(currentVehicle)
														ENDIF
														IF NOT IS_SEAT_WARP_ONLY(currentVehicle,GET_SEAT_PED_IS_IN(GET_PLAYER_PED(playerGangTarget)))
															TASK_ENTER_VEHICLE(	thisPed, currentVehicle, DEFAULT_TIME_NEVER_WARP, 
																				GET_PED_VEHICLE_SEAT(GET_PLAYER_PED(playerGangTarget),currentVehicle), 
																				GET_THIEF_PED_MOVE_BLEND_RATIO(iGangID),
																				ECF_BLOCK_SEAT_SHUFFLING|ECF_DONT_CLOSE_DOOR|ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP|
																				ECF_JACK_ANYONE|ECF_RESUME_IF_INTERRUPTED|ECF_USE_LEFT_ENTRY)
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ELSE
											IF NOT (IS_PED_JACKING(thisPed) AND GET_JACK_TARGET(thisPed) = GET_PLAYER_PED(playerGangTarget))
											AND NOT IS_PED_IN_ANY_VEHICLE(thisPed, TRUE)
												
												IF IS_TASK_ONGOING(thisPed, SCRIPT_TASK_ENTER_VEHICLE)
													clear_ped_tasks(thisPed)
												ENDIF
												
												IF NOT IS_TASK_ONGOING(thisPed, SCRIPT_TASK_GO_TO_ENTITY)
													
													IF IS_PED_ALRIGHT_FOR_ACTION(thisPed)
														TASK_GO_TO_ENTITY(thisPed, GET_PLAYER_PED(playerGangTarget), DEFAULT_TIME_NEVER_WARP, 0.0, GET_THIEF_PED_MOVE_BLEND_RATIO(iGangID))
													ENDIF
												
												else 
												
													set_ped_reset_flag(thisPed, PRF_AllowTasksIncompatibleWithMotion, true)
												
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					BREAK
					
					CASE GANG_STAGE_THIEF_STEAL
						println("AM_GANG_CALL == GANG_STAGE_THIEF_STEAL")
						IF PLAYER_ID() = playerGangTarget AND ARE_TWO_POINTS_WITHIN_SQUARE_DISTANCE(	GET_ENTITY_COORDS(thisPed), 
																			GET_ENTITY_COORDS(GET_PLAYER_PED(playerGangTarget)), 
																			MIN_DISTANCE_THIEF_TO_BREAK_COVER)
							SET_BIT(MPGlobalsAmbience.iThiefBitSet, THIEF_NEAR_PLAYER)
						ELSE
							CLEAR_BIT(MPGlobalsAmbience.iThiefBitSet, THIEF_NEAR_PLAYER)
						ENDIF
						
						
						
						IF PLAYER_ID() = playerGangTarget	//is this client equal to the player target (and have grabbed control from above)
							IF NOT IS_BIT_SET(playerBD[NETWORK_PLAYER_ID_TO_INT()].iBitset, CLIENT_BS_BEEN_ROBBED)
								IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
									IF (CHECK_THIEF_DISTANCE(GET_ENTITY_COORDS(thisPed), GET_ENTITY_COORDS(PLAYER_PED_ID())) and (is_ped_on_foot(PLAYER_PED_ID()) OR IS_PED_HANGING_ON_TO_VEHICLE(PLAYER_PED_ID()) ))
									OR (IS_PED_JACKING(thisPed) AND GET_JACK_TARGET(thisPed) = PLAYER_PED_ID())
									
										
										println("AM_GANG_CALL == IS_PED_HANGING_ON_TO_VEHICLE(PLAYER_PED_ID())")
										
								//		int percentage_of_cash 				
								//		percentage_of_cash = g_sMPTunables.iMUGGER_AMOUNT_STEAL / 10 // iMUGGER_AMOUNT_STEAL = 30
										
										iStolenCash = GET_PLAYER_CASH(PLAYER_ID())
											
								//		iStolenCash = round(to_float(iStolenCash) / percentage_of_cash)
								//		iStolenCash = round(round(to_float(iStolenCash) / 2) * 2.0)
										
										println("AM_GANG_CALL Calculating stolen amount, Player Initial Cash = ", iStolenCash, " Tunable iMUGGER_AMOUNT_STEAL = ",  g_sMPTunables.iMUGGER_AMOUNT_STEAL)
										
										FLOAT fTunableAsFloat
										fTunableAsFloat = TO_FLOAT(g_sMPTunables.iMUGGER_AMOUNT_STEAL) / 100.0
										iStolenCash = ROUND(TO_FLOAT(iStolenCash) * fTunableAsFloat)
										
										
										println("AM_GANG_CALL Calculating stolen amount, amount to steal before rank limit check = ", iStolenCash)
										int iPlayerRank 
										iPlayerRank = GET_PLAYER_RANK(PLAYER_ID())
										IF iPlayerRank > 100
											iPlayerRank = 100 		//Cap at 100 for cash purposes
										ENDIF
										
										IF iStolenCash > (iPlayerRank * g_sMPTunables.iMUGGER_STEAL_PERCENTAGE_CAP)
											iStolenCash = (iPlayerRank * g_sMPTunables.iMUGGER_STEAL_PERCENTAGE_CAP)
										ENDIF
		
										IF iStolenCash > 0
											SET_BIT(iLocalBitset,LOCAL_BS_DO_TRANSACTION)
										ENDIF
																				
										//SET_PED_TO_RAGDOLL(PLAYER_PED_ID(), 5000, 10000, TASK_RELAX, TRUE, TRUE, TRUE)
										SET_HIGH_FALL_TASK(PLAYER_PED_ID(), 5000, 10000, HIGHFALL_TEETER_EDGE)
										IF NOT (IS_PED_JACKING(thisPed) AND GET_JACK_TARGET(thisPed) = GET_PLAYER_PED(playerGangTarget))
											vPushDirection = GET_ENTITY_COORDS(PLAYER_PED_ID()) - GET_ENTITY_COORDS(thisPed)
											vPushDirection = NORMALISE_VECTOR(vPushDirection)
											vPushDirection.x *= THIEF_PUSH_STRENGTH
											vPushDirection.y *= THIEF_PUSH_STRENGTH
											vPushDirection.z *= THIEF_PUSH_STRENGTH
											APPLY_FORCE_TO_ENTITY(PLAYER_PED_ID(), APPLY_TYPE_IMPULSE, vPushDirection, <<0,0,0.5>>, 0, FALSE, TRUE, TRUE)
										ENDIF
				                        //SET_PED_TO_RAGDOLL_WITH_FALL() 
				                        //SET_HIGH_FALL_TASK()
										SET_PED_FLEE_ATTRIBUTES(thisPed, FA_NEVER_FLEE, FALSE)
										SET_PED_COMBAT_ATTRIBUTES(thisPed, CA_ALWAYS_FLEE, TRUE)
	
										IF (IS_PED_IN_ANY_VEHICLE(thisPed, TRUE) OR IS_PED_JACKING(thisPed))	//if jacking a car, and it's good
										AND IS_VEHICLE_PED_IS_STEALING_SUITABLE_TO_ESCAPE_WITH(thisPed)
										AND GET_SEAT_PED_IS_IN(thisPed) = VS_DRIVER
			
											#IF IS_DEBUG_BUILD
											CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === GANG_STAGE_THIEF_STEAL IN VEHICLE SUCCESS")
											#ENDIF
											SET_PED_FLEE_ATTRIBUTES(thisPed, FA_USE_VEHICLE, TRUE)
											SET_PED_COMBAT_ATTRIBUTES(thisPed, CA_LEAVE_VEHICLES, FALSE)
											SET_PED_COMBAT_ATTRIBUTES(thisPed, CA_FLEE_WHILST_IN_VEHICLE, TRUE)
										
		
										ELSE
											
											#IF IS_DEBUG_BUILD
											CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === GANG_STAGE_THIEF_STEAL ON FOOT SUCCESS")
											#ENDIF
										
											clear_ped_tasks(thisped)
											TASK_SMART_FLEE_PED(thisPed, PLAYER_PED_ID(), MIN_DISTANCE_THIEF_TO_ESCAPE, -1, TRUE)
											
										ENDIF
										playerBD[NETWORK_PLAYER_ID_TO_INT()].iCashStolen = iStolenCash
										
										IF iStolenCash > 0
											playerBD[NETWORK_PLAYER_ID_TO_INT()].iSuccessThiefID = iGangID
										ELSE
											playerBD[NETWORK_PLAYER_ID_TO_INT()].iSuccessThiefID = -1		//flag that theft has failed
										ENDIF
										
										SET_PED_RELATIONSHIP_GROUP_HASH(thisPed, rgFM_AiHatedByCopsAndMercs)
										
										SET_BIT(playerBD[NETWORK_PLAYER_ID_TO_INT()].iBitset, CLIENT_BS_BEEN_ROBBED)
										SET_BIT(MPGlobalsAmbience.iThiefBitSet,THIEF_MUGGED_PLAYER)
										CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === SET_BIT(MPGlobalsAmbience.iThiefBitSet,THIEF_MUGGED_PLAYER)")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					BREAK
					
					CASE GANG_STAGE_THIEF_FLEE
					
						IF GET_PED_RESET_FLAG(thisPed,PRF_PreventAllMeleeTakedowns)
							SET_PED_RESET_FLAG(thisPed,PRF_PreventAllMeleeTakedowns,FALSE)
						ENDIF
						
						//Set up ped flee
						IF serverBD.gangMember[iGangID].gangClass = GANG_CLASS_THIEF
							IF NOT IS_BIT_SET(serverBD.gangMember[iGangID].iBitSet, GANG_BS_THIEF_SET_UP_FLEE)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(thisPed, FALSE)
								SET_PED_CONFIG_FLAG(thisPed, PCF_RunFromFiresAndExplosions, TRUE)
							ENDIF
						ENDIF
						
						IF NOT (IS_PED_JACKING(thisPed) AND GET_JACK_TARGET(thisPed) = GET_PLAYER_PED(playerGangTarget))
							IF NOT IS_PED_FLEEING(thisPed)
								IF IS_PED_ALRIGHT_FOR_ACTION(thisPed)
									IF (IS_PED_IN_ANY_VEHICLE(thisPed, TRUE) OR IS_PED_JACKING(thisPed))
									AND NOT IS_BIT_SET(serverBD.iThiefBitSet, THIEF_BS_NO_PATH)
										
										IF NOT IS_TASK_ONGOING(thisPed, SCRIPT_TASK_VEHICLE_DRIVE_WANDER)
										and not IS_TASK_ONGOING(thisPed, SCRIPT_TASK_vehicle_mission)
											
											#IF IS_DEBUG_BUILD
											CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === GANG_STAGE_THIEF_FLEE IN CAR FLEE")
											#ENDIF
			
											if is_ped_sitting_in_any_vehicle(thisPed)
											
												vehicle_index mission_vehicle
												model_names mission_vehicle_model
											
												mission_vehicle = get_vehicle_ped_is_in(thisPed, true)
												
												mission_vehicle_model = get_entity_model(mission_vehicle)
												
												if is_bit_set(serverBD.iThiefBitSet, FAIL_BS_TARGET_ON_FM_EVENT)
													SET_PED_COMBAT_ATTRIBUTES(thisPed, CA_ALWAYS_FLEE, TRUE)
													SET_PED_COMBAT_ATTRIBUTES(thisPed, CA_FLEE_WHILST_IN_VEHICLE, FALSE)
													SET_PED_COMBAT_ATTRIBUTES(thisPed, CA_USE_VEHICLE, FALSE)
												else
													if is_this_model_a_heli(mission_vehicle_model)
													
														task_heli_mission(thisPed, mission_vehicle, null, player_ped_id(), <<0.0, 0.0, 0.0>>, mission_flee, 20.0, 0, -1, 80, 80) 
													
													elif is_this_model_a_plane(mission_vehicle_model)
													
														task_plane_mission(thisPed, mission_vehicle, null, player_ped_id(), <<0.0, 0.0, 0.0>>, mission_flee, 25.0, 0, -1, 80, 80) 
													
													elif get_entity_model(mission_vehicle) = taxi
													
														//reset as these are set in GANG_STAGE_THIEF_STEAL
														SET_PED_FLEE_ATTRIBUTES(thisPed, FA_USE_VEHICLE, false)
														SET_PED_COMBAT_ATTRIBUTES(thisPed, CA_LEAVE_VEHICLES, true)
														SET_PED_COMBAT_ATTRIBUTES(thisPed, CA_FLEE_WHILST_IN_VEHICLE, false)
														
														TASK_SMART_FLEE_PED(thisPed, GET_PLAYER_PED(playerGangTarget), MIN_DISTANCE_THIEF_TO_ESCAPE, -1, TRUE)

													else 
			
														TASK_VEHICLE_DRIVE_WANDER(thisPed, GET_VEHICLE_PED_IS_IN(thisPed, TRUE), 60.0, DRIVINGMODE_AVOIDCARS_RECKLESS)
														
													endif 
												endif
												
											endif 
										ENDIF
									ELSE
										IF NOT IS_TASK_ONGOING(thisPed, SCRIPT_TASK_SMART_FLEE_PED)
											IF IS_NET_PLAYER_OK(playerGangTarget)
												#IF IS_DEBUG_BUILD
												CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === GANG_STAGE_THIEF_FLEE ON FOOT FLEE")
												#ENDIF
												TASK_SMART_FLEE_PED(thisPed, GET_PLAYER_PED(playerGangTarget), MIN_DISTANCE_THIEF_TO_ESCAPE,
																	-1, TRUE)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					BREAK
					
					CASE GANG_STAGE_CLEANUP
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === CLEANUP FOR gangMember[",iGangID,"]")
						#ENDIF
						CLEANUP_NET_PED(serverBD.gangMember[iGangID].netID, FALSE)
					BREAK
				ENDSWITCH
				
				IF IS_BIT_SET(serverBD.iThiefBitSet, FAIL_BS_TARGET_ON_FM_EVENT)
					CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === [MANAGE_GANG_MEMBER_BODY] - FAIL_BS_TARGET_ON_FM_EVENT = TRUE")
					CLEAR_PED_TASKS(NET_TO_PED(serverBD.gangMember[iGangID].netID))
					SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(serverBD.gangMember[iGangID].netID), CA_ALWAYS_FLEE, TRUE)
					SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(serverBD.gangMember[iGangID].netID), CA_FLEE_WHILST_IN_VEHICLE, FALSE)
					SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(serverBD.gangMember[iGangID].netID), CA_USE_VEHICLE, FALSE)
				ENDIF
				
				MAINTAIN_ROB_TRANSACTION(thisPed)
			ENDIF
			
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Controls the server staggering through all the gang members, performing server-side logic as well as checking flags
PROC SERVER_STAGGERED_GANG_LOOP()
	IF IS_BIT_SET(serverBD.gangMember[iServerGangStaggeredCounter].iBitSet, GANG_BS_CREATED_AND_SET_UP)
	OR IS_BIT_SET(serverBD.gangMember[iServerGangStaggeredCounter].iBitSet, GANG_BS_DEAD)
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.gangMember[iServerGangStaggeredCounter].netID)
			IF DOES_ENTITY_EXIST(NET_TO_PED(serverBD.gangMember[iServerGangStaggeredCounter].netID))
				IF IS_ENTITY_DEAD(NET_TO_PED(serverBD.gangMember[iServerGangStaggeredCounter].netID))
					IF NOT IS_BIT_SET(serverBD.gangMember[iServerGangStaggeredCounter].iBitSet, GANG_BS_DEAD)
						
						IF iGangVariation = GANG_CALL_TYPE_SPECIAL_THIEF
							
							serverBD.iNumberOfThievesKilled++
							
							IF NOT IS_BIT_SET(serverBD.iThiefBitset, THIEF_BS_TARGET_HAS_KILLED)
								//Check if target was Killer
								WEAPON_TYPE TempWeapon
								IF NETWORK_GET_DESTROYER_OF_NETWORK_ID(serverBD.gangMember[iServerGangStaggeredCounter].netID, TempWeapon) = playerGangTarget
									SET_BIT(serverBD.iThiefBitset, THIEF_BS_TARGET_HAS_KILLED)
								ENDIF
							ENDIF
							
							SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerEventData
							
							IF (serverBD.iSuccessThiefID = iServerGangStaggeredCounter					//if killed the successful thief
								AND NOT IS_BIT_SET(serverBD.iThiefBitSet, THIEF_BS_SUCCESSFUL_DEAD))	//or killed all the thieves
							OR serverBD.iNumberOfThievesKilled = GET_NUMBER_OF_GANG_MEMBERS_IN_GANG(iGangVariation)
								
								TickerEventData.TickerEvent = TICKER_EVENT_GANG_CALL_THIEF_DEFEATED
								TickerEventData.playerID = playerGangTarget
								TickerEventData.playerID2 = playerGangOwner
								IF GET_NUMBER_OF_GANG_MEMBERS_IN_GANG(iGangVariation) > 1
									TickerEventData.playerID3 = INT_TO_PLAYERINDEX(1)
								ELSE
									TickerEventData.playerID3 = INT_TO_PLAYERINDEX(0)
								ENDIF
								TickerEventData.dataInt = serverBD.iCashStolen
								BROADCAST_TICKER_EVENT(TickerEventData, ALL_PLAYERS_ON_SCRIPT(TRUE))
								
								IF serverBD.iSuccessThiefID = iServerGangStaggeredCounter
									SET_BIT(serverBD.iThiefBitSet, THIEF_BS_SUCCESSFUL_DEAD)
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === SET_BIT(serverBD.iThiefBitSet, THIEF_BS_SUCCESSFUL_DEAD)")
									#ENDIF
								ENDIF
							ELSE
								IF NOT IS_BIT_SET(serverBD.iThiefBitSet, THIEF_BS_SUCCESSFUL_DEAD)
									TickerEventData.TickerEvent = TICKER_EVENT_GANG_CALL_THIEF_KILLED
									TickerEventData.playerID2 = playerGangOwner
									BROADCAST_TICKER_EVENT(TickerEventData, ALL_PLAYERS_ON_SCRIPT(TRUE))
								ENDIF
							ENDIF
							#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === SET_BIT(gangMember[", iServerGangStaggeredCounter, ".iBitSet, GANG_BS_DEAD)")
							#ENDIF
						ENDIF
					
						SET_BIT(serverBD.gangMember[iServerGangStaggeredCounter].iBitSet, GANG_BS_DEAD)
					ENDIF
				ELSE
					
					IF NOT IS_BIT_SET(serverBD.gangMember[iServerGangStaggeredCounter].iBitSet, GANG_BS_ABANDONED)
						SET_BIT(iLocalBitset, LOCAL_BS_SERVER_STAGGER_AT_LEAST_ONE_GANG_MEMBER_NOT_ABANDONED)
					ENDIF
									
					SET_BIT(iLocalBitset, LOCAL_BS_SERVER_STAGGER_AT_LEAST_ONE_GANG_MEMBER_ALIVE)
				ENDIF
			ENDIF
		ENDIF
	ELSE
		SET_BIT(iLocalBitset, LOCAL_BS_SERVER_STAGGER_AT_LEAST_ONE_GANG_MEMBER_ALIVE)
		SET_BIT(iLocalBitset, LOCAL_BS_SERVER_STAGGER_AT_LEAST_ONE_GANG_MEMBER_NOT_ABANDONED)
	ENDIF
	
	MANAGE_GANG_MEMBER_BRAIN(iServerGangStaggeredCounter)
	
	iServerGangStaggeredCounter++																//Set this frame's gang stagger value
	IF iServerGangStaggeredCounter >= GET_NUMBER_OF_GANG_MEMBERS_IN_GANG(iGangVariation)		//Reset stagger loop
		iServerGangStaggeredCounter = 0
		
		IF IS_BIT_SET(serverBD.iBitset, SERVER_BS_GANG_CREATED)
			IF NOT IS_BIT_SET(iLocalBitset, LOCAL_BS_SERVER_STAGGER_AT_LEAST_ONE_GANG_MEMBER_ALIVE) //If completed full staggered loop and no-one is alive
				IF NOT IS_BIT_SET(serverBD.iBitset, SERVER_BS_ALL_GANG_MEMBERS_DEAD)
					SET_BIT(serverBD.iBitset, SERVER_BS_ALL_GANG_MEMBERS_DEAD)								//Everyone is dead
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === SET_BIT(serverBD.iBitset, SERVER_BS_ALL_GANG_MEMBERS_DEAD)")
					#ENDIF
				ENDIF
			ENDIF
			
			IF NOT IS_BIT_SET(iLocalBitset, LOCAL_BS_SERVER_STAGGER_AT_LEAST_ONE_GANG_MEMBER_NOT_ABANDONED) //If completed full staggered loop and all abandoned
				IF NOT IS_BIT_SET(serverBD.iBitset, SERVER_BS_ALL_GANG_MEMBERS_ABANDONED)
					SET_BIT(serverBD.iBitset, SERVER_BS_ALL_GANG_MEMBERS_ABANDONED)							//Everyone is abandoned
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === SET_BIT(serverBD.iBitset, SERVER_BS_ALL_GANG_MEMBERS_ABANDONED)")
					#ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		CLEAR_BIT(iLocalBitset, LOCAL_BS_SERVER_STAGGER_AT_LEAST_ONE_GANG_MEMBER_ALIVE)
		CLEAR_BIT(iLocalBitset, LOCAL_BS_SERVER_STAGGER_AT_LEAST_ONE_GANG_MEMBER_NOT_ABANDONED)
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Resets the server information of having tried to spawn a gang.
PROC SERVER_RESET_GANG_ATTEMPT()
	SCRIPT_TIDY_UP()
	serverBD.iServerGameState = GAME_STATE_INI
	
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === SERVER_RESET_GANG_ATTEMPT")
	#ENDIF
ENDPROC

PROC SEND_MUGGED_TICKER()
	SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerEventData
	TickerEventData.TickerEvent = TICKER_EVENT_GANG_CALL_THIEF_MUGGED
	TickerEventData.playerID = playerGangTarget
	TickerEventData.playerID2 = playerGangOwner
	IF GET_NUMBER_OF_GANG_MEMBERS_IN_GANG(iGangVariation) > 1
		TickerEventData.playerID3 = INT_TO_PLAYERINDEX(1)
	ELSE
		TickerEventData.playerID3 = INT_TO_PLAYERINDEX(0)
	ENDIF
	TickerEventData.dataInt = serverBD.iCashStolen
	BROADCAST_TICKER_EVENT(TickerEventData, ALL_PLAYERS_ON_SCRIPT(TRUE))
ENDPROC

PROC SEND_FAILED_TICKER()
	SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerEventData
	TickerEventData.TickerEvent = TICKER_EVENT_GANG_CALL_THIEF_UNSUCCESSFUL
	TickerEventData.playerID = playerGangTarget
	TickerEventData.playerID2 = playerGangOwner
	BROADCAST_TICKER_EVENT(TickerEventData, ALL_PLAYERS_ON_SCRIPT(TRUE))
ENDPROC

/// PURPOSE:
///    Staggered loop for the server processing flags from the players
PROC SERVER_STAGGERED_PlAYER_LOOP()
	
	IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(iServerPlayerStaggeredCounter), FALSE)
		IF iGangVariation = GANG_CALL_TYPE_SPECIAL_MERRYWEATHER
			
			IF INT_TO_PLAYERINDEX(iServerPlayerStaggeredCounter) = playerGangTarget
				IF NOT IS_BIT_SET(serverBD.iBitset, SERVER_BS_TARGET_ATTACKED_BY_MERRYWEATHER)
					IF IS_BIT_SET(playerBD[iServerPlayerStaggeredCounter].iBitset, CLIENT_BS_ATTACKED_BY_MERRYWEATHER)
						SET_BIT(serverBD.iBitset, SERVER_BS_TARGET_ATTACKED_BY_MERRYWEATHER)
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === SET_BIT SERVER_BS_TARGET_ATTACKED_BY_MERRYWEATHER")
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		ELSE
			
			IF NOT IS_BIT_SET(serverBD.iBitset, SERVER_BS_PASSIVE_CHANGED)
				IF IS_PLAYER_PASSIVE(INT_TO_PLAYERINDEX(iServerPlayerStaggeredCounter))
					IF NOT IS_BIT_SET(serverBD.iPassiveBitset, iServerPlayerStaggeredCounter)
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === player ", iServerPlayerStaggeredCounter, " passive newly on")
						#ENDIF
						SET_BIT(serverBD.iBitset, SERVER_BS_PASSIVE_CHANGED)
						SET_BIT(serverBD.iPassiveBitset, iServerPlayerStaggeredCounter)
					ENDIF
				ELSE
					IF IS_BIT_SET(serverBD.iPassiveBitset, iServerPlayerStaggeredCounter)
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === player ", iServerPlayerStaggeredCounter, " passive newly off")
						#ENDIF
						SET_BIT(serverBD.iBitset, SERVER_BS_PASSIVE_CHANGED)
						CLEAR_BIT(serverBD.iPassiveBitset, iServerPlayerStaggeredCounter)
					ENDIF
				ENDIF
			ELSE
				IF IS_BIT_SET(playerBD[iServerPlayerStaggeredCounter].iBitset, CLIENT_BS_PASSIVE_CHANGE_RECOGNISED)
					CLEAR_BIT(serverBD.iBitset, SERVER_BS_PASSIVE_CHANGED)
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === CLEAR_BIT(serverBD.iBitset, SERVER_BS_PASSIVE_CHANGED)")
					#ENDIF
				ENDIF
			ENDIF
			
			
			IF iGangVariation = GANG_CALL_TYPE_SPECIAL_THIEF
				IF IS_BIT_SET(playerBD[iServerPlayerStaggeredCounter].iBitset, CLIENT_BS_BEEN_ROBBED)
					IF playerBD[iServerPlayerStaggeredCounter].iSuccessThiefID <> -1
						IF NOT IS_BIT_SET(serverBD.iThiefBitSet, THIEF_BS_SOMEONE_ROBBED)
							#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === SET_BIT(serverBD.iThiefBitSet, THIEF_BS_SOMEONE_ROBBED)")
							#ENDIF
							SET_BIT(serverBD.iThiefBitSet, THIEF_BS_SOMEONE_ROBBED)
							
							serverBD.iSuccessThiefID = playerBD[iServerPlayerStaggeredCounter].iSuccessThiefID
							serverBD.iCashStolen = playerBD[iServerPlayerStaggeredCounter].iCashStolen
							
							//Delay tickers until transaction is complete
							IF NOT USE_SERVER_TRANSACTIONS()
								SEND_MUGGED_TICKER()
							ENDIF
							
						ENDIF
					ELSE//Theft gone wrong
						IF NOT IS_BIT_SET(serverBD.iThiefBitSet, THIEF_BS_NO_MONEY)
							//Delay tickers until transaction is complete
							IF NOT USE_SERVER_TRANSACTIONS()
								SEND_FAILED_TICKER()
							ENDIF
							INT i
							REPEAT GET_NUMBER_OF_GANG_MEMBERS_IN_GANG(iGangVariation) i
								SET_GANG_MEMBER_STAGE(i, GANG_STAGE_THIEF_FLEE)
							ENDREPEAT

							SET_BIT(serverBD.iThiefBitSet, THIEF_BS_NO_MONEY)
							SET_BIT(serverBD.iThiefBitSet, THIEF_BS_SOMEONE_ROBBED)
						ENDIF
					ENDIF
				ENDIF
				IF USE_SERVER_TRANSACTIONS()
					IF NOT IS_BIT_SET(iLocalBitset,LOCAL_BS_SENT_TICKER)
						IF IS_BIT_SET(playerBD[iServerPlayerStaggeredCounter].iBitset, CLIENT_BS_TRANSACTION_SUCCESS)
							SEND_MUGGED_TICKER()
							SET_BIT(iLocalBitset,LOCAL_BS_SENT_TICKER)
						ELIF IS_BIT_SET(playerBD[iServerPlayerStaggeredCounter].iBitset, CLIENT_BS_TRANSACTION_FAIL)
							SEND_FAILED_TICKER()
							SET_BIT(iLocalBitset,LOCAL_BS_SENT_TICKER)
						ENDIF
					ENDIF
				ENDIF
				IF NOT IS_BIT_SET(playerBD[iServerPlayerStaggeredCounter].iBitset, CLIENT_BS_THIEVES_SUCCESSFULLY_FLED)
					IF NOT IS_BIT_SET(iLocalBitset, LOCAL_BS_SERVER_STAGGER_SOMEONE_NEAR_A_THIEF)
						SET_BIT(iLocalBitset, LOCAL_BS_SERVER_STAGGER_SOMEONE_NEAR_A_THIEF)
					ENDIF
				ELSE										//IF this player not near any thieves
					IF INT_TO_PLAYERINDEX(iServerPlayerStaggeredCounter) = playerGangTarget
						IF iGangVariation = GANG_CALL_TYPE_SPECIAL_THIEF
							IF NOT IS_BIT_SET(serverBD.iThiefBitSet, THIEF_BS_SOMEONE_ROBBED)
								IF NOT IS_BIT_SET(serverBD.iThiefBitSet, THIEF_BS_TARGET_ESCAPED)
									SET_BIT(serverBD.iThiefBitSet, THIEF_BS_TARGET_ESCAPED)
									printstring("mugger fail 0")
									printnl()
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF
		ENDIF
	ENDIF

	iServerPlayerStaggeredCounter++																//Set this frame's gang stagger value
	IF iServerPlayerStaggeredCounter >= NUM_NETWORK_PLAYERS										//Reset stagger loop
		iServerPlayerStaggeredCounter = 0
		IF iGangVariation = GANG_CALL_TYPE_SPECIAL_THIEF
			IF NOT IS_BIT_SET(iLocalBitset, LOCAL_BS_SERVER_STAGGER_SOMEONE_NEAR_A_THIEF)
				START_THIEF_ESCAPE_TIMER()
			ELSE
				RESET_THIEF_ESCAPE_TIMER()
			ENDIF
			CLEAR_BIT(iLocalBitset, LOCAL_BS_SERVER_STAGGER_SOMEONE_NEAR_A_THIEF)
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE:
///    Returns if the specified ped is in a vehicle with the specified model
/// PARAMS:
///    mission_ped - ped to check
///    vehicle_model - model to check for
/// RETURNS:
///    true/false
func bool is_ped_in_specific_vehicle(ped_index mission_ped, model_names vehicle_model)  

	if does_entity_exist(mission_ped)
		if not is_ped_injured(mission_ped)
			if is_ped_sitting_in_any_vehicle(mission_ped)
			
				vehicle_index mission_vehicle = GET_VEHICLE_PED_IS_IN(mission_ped)
				
				if does_entity_exist(mission_vehicle)
					
					if (get_entity_model(mission_vehicle) = vehicle_model)
					
						return true 
						
					endif 
				endif 
			endif 
		endif 
	endif 

	return false 
	
endfunc 

/// PURPOSE:
///    Checks if the target player is doing an activity or reach an area that means the thief cannot access him
/// RETURNS:
///    true/false
FUNC BOOL HAS_TARGET_PLAYER_ESCAPED_THIEF()
	IF IS_BIT_SET(serverBD.iThiefBitSet, THIEF_BS_TARGET_ESCAPED)
	OR IS_PLAYER_DEAD(playerGangTarget)
	OR IS_PLAYER_IN_PROPERTY(playerGangTarget, TRUE, TRUE)
	OR IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(playerGangTarget)
	OR IS_PLAYER_ON_MP_AMBIENT_SCRIPT(playerGangTarget, MPAM_TYPE_CINEMA)
	OR IS_PLAYER_ON_MP_AMBIENT_SCRIPT(playerGangTarget, MPAM_TYPE_STRIPCLUB)
	OR IS_THIS_PLAYER_IN_CORONA(playerGangTarget)
	OR GlobalplayerBD[NATIVE_TO_INT(playerGangTarget)].bInShopMenu
	or is_ped_in_specific_vehicle(GET_PLAYER_PED(playerGangTarget), titan)
		RETURN TRUE
	ENDIF
	
	if native_to_int(playergangtarget) != -1
		IF IS_BIT_SET(playerBD[native_to_int(playergangtarget)].iBitset, CLIENT_BS_PROSTITUTE_ACTIVE)
			return true
		endif 
	endif 

	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if the target player is doing an activity or reach an area that means the army cannot access him
/// RETURNS:
///    true/false
FUNC BOOL HAS_TARGET_PLAYER_ESCAPED_ARMY()
	IF IS_PLAYER_IN_PROPERTY(playerGangTarget, TRUE, TRUE)
	OR IS_PLAYER_ON_MP_AMBIENT_SCRIPT(playerGangTarget, MPAM_TYPE_CINEMA)
	OR IS_PLAYER_ON_MP_AMBIENT_SCRIPT(playerGangTarget, MPAM_TYPE_STRIPCLUB)
	OR IS_THIS_PLAYER_IN_CORONA(playerGangTarget)
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if the target player is in a state or situation where the thief should not be able to spawn at all
/// RETURNS:
///    true/false
FUNC BOOL IS_TARGET_PLAYER_BLOCKING_THIEF()
	PED_INDEX thisPed = GET_PLAYER_PED(playerGangTarget)
	
	IF GET_ENTITY_SPEED(thisPed) > GANG_CALL_ACCEPTABLE_TARGET_SPEED	//then test situational reasons why spawning cant happen
	OR IS_ENTITY_IN_AIR(thisPed)
	OR IS_ENTITY_IN_WATER(thisPed)
	OR IS_PLAYER_IN_PROPERTY(playerGangTarget, TRUE, TRUE)
	OR IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(playerGangTarget)
	OR IS_PLAYER_ON_MP_AMBIENT_SCRIPT(playerGangTarget, MPAM_TYPE_CINEMA)
	OR IS_PLAYER_ON_MP_AMBIENT_SCRIPT(playerGangTarget, MPAM_TYPE_STRIPCLUB)
	OR IS_THIS_PLAYER_IN_CORONA(playerGangTarget)
	OR GlobalplayerBD[NATIVE_TO_INT(playerGangTarget)].bInShopMenu
	or is_ped_in_specific_vehicle(GET_PLAYER_PED(playerGangTarget), titan)
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_ON_ANY_MP_MISSION(playerGangTarget)
		IF NATIVE_TO_INT(playerGangTarget) > -1
			IF GlobalplayerBD_FM[NATIVE_TO_INT(playerGangTarget)].iCurrentMissionType <> FMMC_TYPE_DEATHMATCH //unless it's deathmatch
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	if native_to_int(playergangtarget) != -1
		IF IS_BIT_SET(playerBD[native_to_int(playergangtarget)].iBitset, CLIENT_BS_PROSTITUTE_ACTIVE)
			return true
		endif 
	endif 
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if the target player is in a state or situation where the army should not be able to spawn at all
/// RETURNS:
///    true/false
FUNC BOOL IS_TARGET_PLAYER_BLOCKING_ARMY()
	PED_INDEX thisPed = GET_PLAYER_PED(playerGangTarget)
	
	IF IS_ENTITY_IN_AIR(thisPed)								//then test situational reasons why spawning cant happen
	OR IS_ENTITY_IN_WATER(thisPed)
	OR IS_PLAYER_IN_PROPERTY(playerGangTarget, TRUE, TRUE)
	OR IS_PLAYER_ON_MP_AMBIENT_SCRIPT(playerGangTarget, MPAM_TYPE_CINEMA)
	OR IS_PLAYER_ON_MP_AMBIENT_SCRIPT(playerGangTarget, MPAM_TYPE_STRIPCLUB)
	OR IS_THIS_PLAYER_IN_CORONA(playerGangTarget)
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_ON_ANY_MP_MISSION(playerGangTarget)
		IF NATIVE_TO_INT(playerGangTarget) > -1
			IF GlobalplayerBD_FM[NATIVE_TO_INT(playerGangTarget)].iCurrentMissionType <> FMMC_TYPE_DEATHMATCH //unless it's deathmatch
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Keeps timers running and checks if time has run out for certain flag conditions
PROC SERVER_MAINTAIN_UNIVERSAL_TIMRS()
	START_THIEF_ABANDON_TIMER()
	IF NOT IS_BIT_SET(serverBD.iBitset, SERVER_BS_A_GANG_MEMBER_CREATED)
		IF IS_BIT_SET(serverBD.iThiefBitSet, THIEF_BS_ABANDON_TIMER_STARTED)
			IF GET_TIME_DIFFERENCE(timeNet, serverBD.timeThiefAbandon) > GANG_CALL_THIEF_ABANDON_TIME
				SET_BIT(serverBD.iThiefBitSet, THIEF_BS_ABANDON)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Returns if the script should update the coordinates that the gang is trying to spawn at or get to
/// PARAMS:
///    pedTarget - ped ID of the target ped
/// RETURNS:
///    true/false
FUNC BOOL SHOULD_GRAB_LATEST_TRAVEL_TO_COORDS(PED_INDEX pedTarget)
	
	IF IS_PED_INJURED(pedTarget)
	ENDIF
	
	IF IS_BIT_SET(serverBD.iBitset, SERVER_BS_TRAVEL_TO_COORDS_UNDERWATER)		//if last point was underwater
		IF NOT IS_ENTITY_IN_WATER(pedTarget)									//but now target is totall clear of water
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF NOT ARE_TWO_POINTS_WITHIN_SQUARE_DISTANCE(serverBD.vGangTravel, GET_ENTITY_COORDS(pedTarget), MIN_DISTANCE_TO_RESET_TRAVEL_COORD)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Sets serverBD.vGangTravel to the supplied target ped's coordinates, as well as noting some flags regarding that player's position
/// PARAMS:
///    pedTarget - gang target ped
PROC GRAB_LATEST_TRAVEL_TO_COORDS(PED_INDEX pedTarget)
	
	IF IS_PED_INJURED(pedTarget)
	ENDIF
	
	serverBD.vGangTravel = GET_ENTITY_COORDS(pedTarget)
	
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === serverBD.vGangTravel = ", serverBD.vGangTravel)
	#ENDIF
	
	IF IS_ENTITY_IN_DEEP_WATER(pedTarget)
		SET_BIT(serverBD.iBitset, SERVER_BS_TRAVEL_TO_COORDS_UNDERWATER)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === SET_BIT(serverBD.iBitset, SERVER_BS_TRAVEL_TO_COORDS_UNDERWATER)")
		#ENDIF
	ELSE
		CLEAR_BIT(serverBD.iBitset, SERVER_BS_TRAVEL_TO_COORDS_UNDERWATER)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === CLEAR_BIT(serverBD.iBitset, SERVER_BS_TRAVEL_TO_COORDS_UNDERWATER)")
		#ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Returns if the player has started a gang attack with the current gang
/// RETURNS:
///    true/false
func bool has_player_triggered_gang_attack_on_same_gang_members()
			
	SWITCH serverBD.gangMember[0].iGang
	
		/*case GANG_ATTACK_GANG_VAGOS
		
			if GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iGangBeingFoughtInGangAttack = GANG_ATTACK_GANG_VAGOS
			
				return true 
				
			endif 
		
		break 
		
		case GANG_ATTACK_GANG_LOST
		
			if GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iGangBeingFoughtInGangAttack = GANG_ATTACK_GANG_LOST
			
				return true 
				
			endif 
		
		break */
		
		case GANG_ATTACK_GANG_FAMILIES
		
			if GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iGangBeingFoughtInGangAttack = GANG_ATTACK_GANG_FAMILIES
			
				return true 
				
			endif 
		
		break 
		
		case GANG_ATTACK_GANG_MERRYWEATHER
		
			if GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iGangBeingFoughtInGangAttack = GANG_ATTACK_GANG_MERRYWEATHER
				
				return true 
				
			endif 
		
		break 
		
	endswitch 
	
	return false 
	
endfunc 

/// PURPOSE:
///    Contains conditions which can set fails to occur which will cleanup the script. 
PROC SERVER_LOGIC()

	IF iGangVariation = GANG_CALL_TYPE_SPECIAL_THIEF
		IF IS_BIT_SET(serverBD.iThiefBitSet, THIEF_BS_ESCAPE_TIMER_STARTED)
			IF GET_TIME_DIFFERENCE(timeNet, serverBD.timeThiefEscape) > GANG_CALL_THIEF_ESCAPE_TIME
				IF IS_BIT_SET(serverBD.iThiefBitSet, THIEF_BS_SOMEONE_ROBBED)		//Taken money from someone, so successfully gotten away
					SET_BIT(serverBD.iThiefBitSet, THIEF_BS_ESCAPED)
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(serverBD.iBitset, SERVER_BS_A_GANG_MEMBER_CREATED)
			IF NOT IS_BIT_SET(serverBD.iThiefBitset, THIEF_BS_SOMEONE_ROBBED)
				IF HAS_TARGET_PLAYER_ESCAPED_THIEF()
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === Target got away")
					#ENDIF
					SET_BIT(serverBD.iThiefBitSet, THIEF_BS_TARGET_ESCAPED)
					SERVER_RESET_GANG_ATTEMPT()
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(serverBD.iBitset, SERVER_BS_GANG_CREATED)
			IF IS_NET_PLAYER_OK(playerGangTarget)
				PED_INDEX pedTarget = GET_PLAYER_PED(playerGangTarget)
				IF SHOULD_GRAB_LATEST_TRAVEL_TO_COORDS(pedTarget)
					GRAB_LATEST_TRAVEL_TO_COORDS(pedTarget)			//Do update of position
				ENDIF
			ENDIF
		ENDIF
		
		IF not IS_BIT_SET(serverBD.iThiefBitSet, THIEF_BS_SOMEONE_ROBBED)
			
			if not IS_NET_PLAYER_OK(playerGangTarget)
			or IS_THIS_PLAYER_IN_CORONA(playerGangTarget)

				SET_BIT(serverBD.iBitset, SERVER_BS_DUE_REFUND)
				
			endif 
			
		endif 
		
	ENDIF
	
	IF IS_BIT_SET(serverBD.iBitset, SERVER_BS_GANG_CREATED)
		 
		 IF IS_BIT_SET(serverBD.iBitset, SERVER_BS_TIMER_STARTED)
			IF NOT IS_BIT_SET(serverBD.iBitset, SERVER_BS_TIME_UP)
				IF GET_TIME_DIFFERENCE(timeNet, serverBD.timeStarted) > GANG_CALL_FIGHT_TIME//70000
					SET_BIT(serverBD.iBitset, SERVER_BS_TIME_UP)
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === SET_BIT(serverBD.iBitset, SERVER_BS_TIME_UP)")
					#ENDIF
				ENDIF
			ENDIF
		ENDIF
				
		//bug 1249993
		IF not IS_BIT_SET(serverBD.iFailBitset, fail_bs_gang_attack_on_same_gang_members)
		
			if has_player_triggered_gang_attack_on_same_gang_members()
				SET_BIT(serverBD.iFailBitset, fail_bs_gang_attack_on_same_gang_members)
			endif 
			
		endif 
		
	ENDIF
ENDPROC

/// PURPOSE:
///    Performs the every-frame functionality of the server player
PROC SERVER_PROCESSING()
	
	SERVER_LOGIC()
	IF iGangVariation <> GANG_CALL_TYPE_SPECIAL_MERRYWEATHER
		SERVER_STAGGERED_GANG_LOOP()
	ENDIF
	
	SERVER_STAGGERED_PlAYER_LOOP()
	
//	SERVER_LOGIC()
	
ENDPROC

/// PURPOSE:
///    Prints help with a player name and number
/// PARAMS:
///    TextLabel - label ID of the text
///    PlayerName - player name to print
///    Colour - colour to print the player name with
///    NumberToDisplay - number to print
///    iOverrideTime - time to display help
PROC PRINT_HELP_WITH_PLAYER_NAME_AND_NUMBER(STRING TextLabel, STRING PlayerName, HUD_COLOURS Colour, INT NumberToDisplay, INT iOverrideTime = -1)
	BEGIN_TEXT_COMMAND_DISPLAY_HELP(TextLabel)	
		IF NOT (Colour = HUD_COLOUR_PURE_WHITE)
			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(Colour)
		ENDIF	
		ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(PlayerName)
		SET_COLOUR_OF_NEXT_TEXT_COMPONENT(HUD_COLOUR_PURE_WHITE)
		ADD_TEXT_COMPONENT_INTEGER(NumberToDisplay)
	END_TEXT_COMMAND_DISPLAY_HELP(HELP_TEXT_SLOT_STANDARD, FALSE, TRUE, iOverrideTime)
ENDPROC

/// PURPOSE:
///    Returns if the help message is being displayed with the supplied player name, colour and number
/// PARAMS:
///    TextLabel - label ID of the text to check
///    PlayerName - player name to check
///    Colour - colour of the player name to check
///    NumberToDisplay - number to check
/// RETURNS:
///    true/false
FUNC BOOL IS_THIS_HELP_MESSAGE_WITH_PLAYER_NAME_AND_NUMBER_BEING_DISPLAYED(STRING TextLabel, STRING PlayerName, HUD_COLOURS Colour, INT NumberToDisplay)
	BEGIN_TEXT_COMMAND_IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(TextLabel)	
		IF NOT (Colour = HUD_COLOUR_PURE_WHITE)
			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(Colour)
		ENDIF	
		ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(PlayerName)
		SET_COLOUR_OF_NEXT_TEXT_COMPONENT(HUD_COLOUR_PURE_WHITE)
		ADD_TEXT_COMPONENT_INTEGER(NumberToDisplay)
	RETURN END_TEXT_COMMAND_IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(HELP_TEXT_SLOT_STANDARD)
ENDFUNC

/// PURPOSE:
///    Prints a help message with two player names
/// PARAMS:
///    TextLabel - text label of the help to print
///    PlayerName0 - name of the first player
///    Colour0 - colour to print the first player in
///    PlayerName1 - name of the second player
///    Colour1 - colour to print the second player in
///    iOverrideTime - time to display the help text
PROC PRINT_HELP_WITH_PLAYER_NAMES(STRING TextLabel, STRING PlayerName0, HUD_COLOURS Colour0, STRING PlayerName1, HUD_COLOURS Colour1, INT iOverrideTime = -1)
	BEGIN_TEXT_COMMAND_DISPLAY_HELP(TextLabel)	
		IF NOT (Colour0 = HUD_COLOUR_PURE_WHITE)
			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(Colour0)
		ENDIF	
		ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(PlayerName0)
		SET_COLOUR_OF_NEXT_TEXT_COMPONENT(HUD_COLOUR_PURE_WHITE)
		IF NOT (Colour1 = HUD_COLOUR_PURE_WHITE)
			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(Colour1)
		ENDIF	
		ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(PlayerName1)
	END_TEXT_COMMAND_DISPLAY_HELP(HELP_TEXT_SLOT_STANDARD, FALSE, TRUE, iOverrideTime)
ENDPROC

/// PURPOSE:
///    Gets the text label based on the supplied gang
/// PARAMS:
///    thisGang - gang to check
/// RETURNS:
///    text label as a string
FUNC STRING GET_TICKER_GANG_NAME_LABEL(INT thisGang)
	SWITCH thisGang
		CASE GANG_CALL_TYPE_SPECIAL_THIEF			RETURN "GC_MENU5"		BREAK//Thief
		CASE GANG_CALL_TYPE_SPECIAL_MERRYWEATHER			RETURN "GC_MENU23"		BREAK//Army
	ENDSWITCH
	RETURN ""
ENDFUNC

/// PURPOSE:
///    Controls the local text for the player, mostly ticker messages
PROC CONTROL_LOCAL_TEXT()
	SWITCH iGangVariation
		CASE GANG_CALL_TYPE_SPECIAL_THIEF
			IF NOT IS_BIT_SET(iLocalBitset, LOCAL_BS_TICKER_GANG_SUCCESSFULLY_REQUESTED)
				IF PLAYER_ID() = playerGangOwner
				AND PLAYER_ID() != playerGangTarget
					IF IS_NET_PLAYER_OK(playerGangTarget)
						PRINT_TICKER_WITH_PLAYER_NAME("GC_TCK_60", playerGangTarget)//~s~Muggers successfully acquired. They will approach ~a~~s~ soon.
						SET_BIT(iLocalBitset, LOCAL_BS_TICKER_GANG_SUCCESSFULLY_REQUESTED)
					ENDIF
				ENDIF
			ENDIF
			IF NOT IS_BIT_SET(iLocalBitset, LOCAL_BS_TICKER_MUGGERS_APPROACHING_TARGET)
				IF IS_BIT_SET(serverBD.iBitset, SERVER_BS_A_GANG_MEMBER_CREATED)
					IF PLAYER_ID() = playerGangOwner
					AND PLAYER_ID() != playerGangTarget
						IF IS_NET_PLAYER_OK(playerGangTarget)
							PRINT_TICKER_WITH_PLAYER_NAME("GC_TCK_62", playerGangTarget)//~s~Your muggers are approaching ~a~.~s~
							SET_BIT(iLocalBitset, LOCAL_BS_TICKER_MUGGERS_APPROACHING_TARGET)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			IF IS_BIT_SET(serverBD.iThiefBitSet, THIEF_BS_NO_PATH)
				IF NOT IS_BIT_SET(iLocalBitset, LOCAL_BS_TICKER_THIEF_NO_PATH)
					IF PLAYER_ID() = playerGangOwner
					AND PLAYER_ID() != playerGangTarget
						IF IS_NET_PLAYER_OK(playerGangTarget)
							PRINT_TICKER_WITH_PLAYER_NAME("GC_TCK_65", playerGangTarget)//~s~ Your muggers are unable to reach ~a~'s~s~ current position. They will attempt again soon.
							SET_BIT(iLocalBitset, LOCAL_BS_TICKER_THIEF_NO_PATH)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_BIT_SET(iLocalBitset, LOCAL_BS_TICKER_TARGET_ESCAPED_MUGGER)
					IF NOT IS_BIT_SET(serverBD.iThiefBitSet, THIEF_BS_WAITING_FOR_NO_PATH_RESET)
						IF IS_BIT_SET(serverBD.iBitset, SERVER_BS_A_GANG_MEMBER_CREATED)
							IF IS_BIT_SET(serverBD.iThiefBitSet, THIEF_BS_TARGET_ESCAPED)
								IF PLAYER_ID() = playerGangOwner
								AND PLAYER_ID() != playerGangTarget
									IF IS_NET_PLAYER_OK(playerGangTarget)
										PRINT_TICKER_WITH_PLAYER_NAME("GC_TCK_63", playerGangTarget)//~s~~a~~s~ has escaped from your muggers. They will approach again soon.
										SET_BIT(iLocalBitset, LOCAL_BS_TICKER_TARGET_ESCAPED_MUGGER)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE GANG_CALL_TYPE_SPECIAL_MERRYWEATHER
			
			IF NOT IS_BIT_SET(iLocalBitset, LOCAL_BS_TICKER_GANG_SUCCESSFULLY_REQUESTED)
				//IF IS_BIT_SET(serverBD.iBitset, SERVER_BS_GANG_CREATED)
					IF PLAYER_ID() = playerGangOwner
						IF IS_NET_PLAYER_OK(playerGangTarget)
						AND PLAYER_ID() != playerGangTarget
							IF serverBD.bIsHitSquad
								PRINT_TICKER_WITH_PLAYER_NAME("HS_SUCC", playerGangTarget)//~s~A Hit Squad was successfully sent to attack ~a~.~s~
							ELIF serverBD.bIsAssassins
								PRINT_TICKER_WITH_PLAYER_NAME("GO_ASS_SUCC", playerGangTarget)//s~Assassins were successfully sent to attack ~a~.~s~
							ELSE
								PRINT_TICKER_WITH_PLAYER_NAME("GC_TCK_70", playerGangTarget)//~s~Merryweather was successfully sent to attack ~a~.~s~
							ENDIF
							
							SET_BIT(iLocalBitset, LOCAL_BS_TICKER_GANG_SUCCESSFULLY_REQUESTED)
						ENDIF
					ELIF PLAYER_ID() = playerGangTarget
						IF IS_NET_PLAYER_OK(playerGangOwner)

							IF serverBD.bIsHitSquad
								IF serverBD.bIsAbandonHit
									PRINT_TICKER("HS_A_SUCC") //A Hit Squad has been sent to attack you for abandoning your Motorcycle Club.
								ELSE
									PRINT_TICKER_WITH_PLAYER_NAME("HS_SENT", playerGangOwner)//~s~~a~~s~ has sent a Hit Squad to attack you.
								ENDIF
							ELIF serverBD.bIsAssassins
								PRINT_TICKER_WITH_PLAYER_NAME("GO_ASS_SENT", playerGangOwner)//~s~~a~~s~ has sent Assassins to attack you.
							ELSE
								PRINT_TICKER_WITH_PLAYER_NAME("GC_TCK_71", playerGangOwner)//~s~~a~~s~ has sent Merryweather to attack you.
							ENDIF
								
							SET_BIT(iLocalBitset, LOCAL_BS_TICKER_GANG_SUCCESSFULLY_REQUESTED)
						ENDIF
					ENDIF
				//ENDIF
			ENDIF
			IF NOT IS_BIT_SET(iLocalBitset, LOCAL_BS_TICKER_ARMY_TARGET_DEAD)
				IF IS_BIT_SET(serverBD.iBitset, SERVER_BS_GANG_CREATED)
					IF IS_BIT_SET(serverBD.iFailBitSet, FAIL_BS_ARMY_TARGET_DEAD)
						IF PLAYER_ID() = playerGangOwner
						AND PLAYER_ID() != playerGangTarget
							IF IS_NET_PLAYER_OK(playerGangTarget, FALSE)
								INCREMENT_DEATHMATCH_SCORE()
								
								IF serverBD.bIsHitSquad
									IF NOT serverBD.bIsAbandonHit
										PRINT_TICKER_WITH_PLAYER_NAME("HS_KILLED", playerGangTarget)//~s~~a~~s~ has been killed by the Hit Squad.
									ENDIF
								ELIF serverBD.bIsAssassins
									PRINT_TICKER_WITH_PLAYER_NAME("GO_ASS_KILL", playerGangTarget)//~s~~a~~s~ has been killed by the Assassins.
								ELSE
									PRINT_TICKER_WITH_PLAYER_NAME("GC_TCK_73", playerGangTarget)//~s~~a~~s~ has been killed by Merryweather.
								ENDIF
								
								SET_BIT(iLocalBitset, LOCAL_BS_TICKER_ARMY_TARGET_DEAD)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT IS_BIT_SET(iLocalBitset, LOCAL_BS_TICKER_ARMY_TARGET_ESCAPE)
				IF IS_BIT_SET(serverBD.iBitset, SERVER_BS_GANG_CREATED)
					IF IS_BIT_SET(serverBD.iFailBitSet, FAIL_BS_ARMY_TARGET_ESCAPE)
						IF PLAYER_ID() = playerGangOwner
						AND PLAYER_ID() != playerGangTarget
							IF IS_NET_PLAYER_OK(playerGangTarget, FALSE)
							
								IF serverBD.bIsHitSquad
									PRINT_TICKER_WITH_PLAYER_NAME("HS_ESCAPED", playerGangTarget)//~s~~a~~s~ has escaped the Hit Squad.
								ELIF serverBD.bIsAssassins
									PRINT_TICKER_WITH_PLAYER_NAME("GO_ASS_ESC", playerGangTarget)//~s~~a~~s~ has escaped the Assassins.
								ELSE
									PRINT_TICKER_WITH_PLAYER_NAME("GC_TCK_75", playerGangTarget)//~s~~a~~s~ has escaped the Mercenaries.
								ENDIF
								
								SET_BIT(iLocalBitset, LOCAL_BS_TICKER_ARMY_TARGET_ESCAPE)
								#IF IS_DEBUG_BUILD
								CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === SET_BIT(iLocalBitset, LOCAL_BS_TICKER_ARMY_TARGET_ESCAPE)")
								#ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Returns if the supplied entity has a blip
/// PARAMS:
///    thisEntity - entity to check
/// RETURNS:
///    true/false
FUNC BOOL DOES_ENTITY_HAVE_BLIP(ENTITY_INDEX thisEntity)
	IF DOES_BLIP_EXIST(GET_BLIP_FROM_ENTITY(thisEntity))
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Plays a conversation for one character, handles adding the ped to the dialogue struct
/// PARAMS:
///    thisRootLabel - root label of the speech
///    thisPed - ped to play the conversation on (NULL for frontend, non-directional conversation)
///    thisVoiceID - voice id for the conversation
///    thisSingleCharacterID - character ID for the conversation
/// RETURNS:
///    true if successfully played
FUNC BOOL PLAY_SAFE_ONE_PED_CONVERSATION(STRING thisRootLabel, PED_INDEX thisPed, STRING thisVoiceID, STRING thisSingleCharacterID)

	TEXT_LABEL_15 gangRoot = thisRootLabel 
	gangRoot += iGangVariation

	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === PLAY_SAFE_ONE_PED_CONVERSATION ", gangRoot)
	#ENDIF
	
	REMOVE_PED_FOR_DIALOGUE(sSpeech, ConvertSingleCharacter(thisSingleCharacterID))
	
	IF thisPed = NULL
		ADD_PED_FOR_DIALOGUE(sSpeech, ConvertSingleCharacter(thisSingleCharacterID), NULL, thisVoiceID)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === ONE PED CONVO PED = NULL")
		#ENDIF
	ELSE
		ADD_PED_FOR_DIALOGUE(sSpeech, ConvertSingleCharacter(thisSingleCharacterID), thisPed, thisVoiceID)
	ENDIF
	
	IF CREATE_CONVERSATION_USING_V_CONTENT_IN_DLC(sSpeech, "mpgcsau", gangRoot, CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT)	
		RETURN TRUE
	ENDIF
	
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === Fail to player conversation ", gangRoot)
	#ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Handles phoning and playing the conversation to create a phone call
/// PARAMS:
///    thisCall - root label of the speech
///    thisVoice - voice ID for the conversation
///    thisChar - character ID for the conversation
/// RETURNS:
///    True when complete
FUNC BOOL DO_GANG_CALL_PHONECALL(STRING thisCall, STRING thisVoice, enumCharacterList thisChar)
	
	IF NOT ARE_STRINGS_EQUAL(thisCall, "NULL")
		//Do the phonecall
		IF NOT IS_BIT_SET(iLocalPhoneBitset, PHONE_BS_CALL_SETUP)
			ADD_PED_FOR_DIALOGUE(sSpeech, 3, NULL, thisVoice)
			IF Request_MP_Comms_Message(sSpeech, thisChar, "CT_AUD", thisCall)	
				SET_BIT(iLocalPhoneBitset, PHONE_BS_CALL_SETUP)
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === SET_BIT(iLocalPhoneBitset, PHONE_BS_CALL_SETUP) ", thisCall)
				#ENDIF
			ENDIF
		ELSE
			IF NOT IS_BIT_SET(iLocalPhoneBitset, PHONE_BS_CALL_RECEIVED)
				IF IS_PHONE_ONSCREEN()
					SET_BIT(iLocalPhoneBitset, PHONE_BS_CALL_RECEIVED)
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === SET_BIT(iLocalPhoneBitset, PHONE_BS_CALL_RECEIVED) ", thisCall)
					#ENDIF
				ENDIF
			ELSE
				IF NOT IS_PHONE_ONSCREEN()
					iLocalPhoneBitset = 0
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === PHONECALL CLEANUP")
					#ENDIF
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Performs the refund phonecall for the current gang type
/// RETURNS:
///    True when complete
FUNC BOOL DO_REFUND_PHONECALL()
	STRING thisCall
	STRING thisVoice
	enumCharacterList thisChar
	
	IF serverBD.bIsHitSquad
	OR serverBD.bIsAssassins
		IF NOT serverBD.bIsAbandonHit
			IF NOT IS_BIT_SET(serverBD.iFailBitset, FAIL_BS_ARMY_TARGET_ESCAPE)
				PRINT_TICKER_WITH_PLAYER_NAME("HS_UNABLE", playerGangTarget) //~s~Unable to send a Hit Squad to attack ~a~.~s~
			ENDIF
		ENDIF
		RETURN TRUE
	ENDIF
	
	SWITCH iGangVariation
		CASE GANG_CALL_TYPE_SPECIAL_THIEF
			thisCall = "MPCT_muglost"
			thisVoice = "LAMAR"
			thisChar = CHAR_LAMAR
		BREAK
		CASE GANG_CALL_TYPE_SPECIAL_MERRYWEATHER
			thisCall = "MPCT_MERloc"
			thisVoice = "FM_MERRYWEATHER"
			thisChar = CHAR_MP_MERRYWEATHER
		BREAK
	ENDSWITCH
	
	IF DO_GANG_CALL_PHONECALL(thisCall, thisVoice, thisChar)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Staggers through the list of gang members getting flag information from them, and performing the body logic if the owner
PROC CLIENT_STAGGERED_GANG_LOOP()

	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.gangMember[iClientGangStaggeredCounter].netID)
	AND NOT IS_ENTITY_DEAD(NET_TO_PED(serverBD.gangMember[iClientGangStaggeredCounter].netID))
		
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			
			IF ARE_TWO_POINTS_WITHIN_SQUARE_DISTANCE(	GET_ENTITY_COORDS(NET_TO_PED(serverBD.gangMember[iClientGangStaggeredCounter].netID)), 
														GET_ENTITY_COORDS(PLAYER_PED_ID()), 50625) //MIN_DISTANCE_THIEF_TO_ESCAPE
				IF NOT IS_BIT_SET(iLocalBitset, LOCAL_BS_CLIENT_NEAR_A_THIEF)
					SET_BIT(iLocalBitset, LOCAL_BS_CLIENT_NEAR_A_THIEF)
				ENDIF
				
				printstring("client within 225")
				printnl()
				
			ELSE
				IF NOT IS_BIT_SET(iLocalBitset, LOCAL_BS_CLIENT_SUCCESS_THIEF_ESCAPED)
					IF serverBD.iSuccessThiefID <> -1
						IF iClientGangStaggeredCounter = serverBD.iSuccessThiefID
							IF NOT IS_BIT_SET(iLocalBitset, LOCAL_BS_CLIENT_SUCCESS_THIEF_ESCAPED)
								SET_BIT(iLocalBitset, LOCAL_BS_CLIENT_SUCCESS_THIEF_ESCAPED)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF

		ENDIF

	endif 
		
	MANAGE_GANG_MEMBER_BODY(iClientGangStaggeredCounter)
	
	iClientGangStaggeredCounter++																//Set this frame's gang stagger value
	IF iClientGangStaggeredCounter >= GET_NUMBER_OF_GANG_MEMBERS_IN_GANG(iGangVariation)		//Reset stagger loop
		
		IF (NOT IS_BIT_SET(iLocalBitset, LOCAL_BS_CLIENT_NEAR_A_THIEF)			//If not near a thief and a gang member has been created
			AND IS_BIT_SET(serverBD.iBitSet, SERVER_BS_A_GANG_MEMBER_CREATED))
		OR IS_BIT_SET(iLocalBitset, LOCAL_BS_CLIENT_SUCCESS_THIEF_ESCAPED)		//or has the successful thief gotten away
			IF NOT IS_BIT_SET(playerBD[NETWORK_PLAYER_ID_TO_INT()].iBitset, CLIENT_BS_THIEVES_SUCCESSFULLY_FLED)
				SET_BIT(playerBD[NETWORK_PLAYER_ID_TO_INT()].iBitset, CLIENT_BS_THIEVES_SUCCESSFULLY_FLED)
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === SET_BIT(playerBD[NETWORK_PLAYER_ID_TO_INT()].iBitset, CLIENT_BS_THIEVES_SUCCESSFULLY_FLED)")
				#ENDIF
			ENDIF
		ELSE
			IF IS_BIT_SET(playerBD[NETWORK_PLAYER_ID_TO_INT()].iBitset, CLIENT_BS_THIEVES_SUCCESSFULLY_FLED)
				CLEAR_BIT(playerBD[NETWORK_PLAYER_ID_TO_INT()].iBitset, CLIENT_BS_THIEVES_SUCCESSFULLY_FLED)
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === CLEAR_BIT(playerBD[NETWORK_PLAYER_ID_TO_INT()].iBitset, CLIENT_BS_THIEVES_SUCCESSFULLY_FLED)")
				#ENDIF
			ENDIF
		ENDIF
				
		CLEAR_BIT(iLocalBitset, LOCAL_BS_CLIENT_NEAR_A_THIEF)
		CLEAR_BIT(iLocalBitset, LOCAL_BS_CLIENT_SUCCESS_THIEF_ESCAPED)
		iClientGangStaggeredCounter = 0
	ENDIF
ENDPROC

FUNC BOOL IS_MUGGER_IN_PARCEL_VEHICLE(PED_INDEX muggerPed)

	IF IS_PED_IN_ANY_VEHICLE(muggerPed)
		VEHICLE_INDEX vehID = GET_VEHICLE_PED_IS_IN(muggerPed)
		IF IS_VEHICLE_DRIVEABLE(vehID)
			IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
				INT iDecoratorValue
				IF DECOR_EXIST_ON(vehID, "MPBitset")
					iDecoratorValue = DECOR_GET_INT(vehID, "MPBitset")
				ENDIF
				IF IS_BIT_SET(iDecoratorValue, MP_DECORATOR_BS_HOLD_THE_WHEEL_VEHICLE)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

/// PURPOSE:
///    Performs local processing for creating phonecalls, managing blips etc.
PROC CLIENT_LOGIC()
	INT i
	PED_INDEX thisPed
	
	//Reset flags (need to be called every frame) (also gang fucked up vehicle checks)
	REPEAT GET_NUMBER_OF_GANG_MEMBERS_IN_GANG(iGangVariation) i
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.gangMember[i].netID)
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.gangMember[i].netID)
				thisPed = NET_TO_PED(serverBD.gangMember[i].netID)
				IF NOT IS_ENTITY_DEAD(thisPed)
					SET_PED_RESET_FLAG(thisPed,	PRF_AllowTasksIncompatibleWithMotion, TRUE)			//Blocks ped panicing in water, should allow swimming
				ENDIF
			ENDIF
		ENDIF
		
	ENDREPEAT
	
	printstring("debug test 0")
	printnl()
	
	printstring("iGangVariation = ")
	printint(iGangVariation)
	printnl()
	
	
	//*****debug 
	if MPGlobalsAmbience.bHasBeenDamagedByMercenaries
		printstring("MPGlobalsAmbience.bHasBeenDamagedByMercenaries TRUE")
		printnl()
	else 
		printstring("MPGlobalsAmbience.bHasBeenDamagedByMercenaries FALSE")
		printnl()
	endif 
	//*****
	
	
	IF iGangVariation = GANG_CALL_TYPE_SPECIAL_MERRYWEATHER
	
		printstring("debug test 1")
		printnl()
		
		//client creates the merrywather on it's self
		IF PLAYER_ID() = playerGangTarget
			
			printstring("debug test 2")
			printnl()
			
			IF IS_BIT_SET(serverBD.iBitset, SERVER_BS_GANG_CREATED)
				
				printstring("debug test 3")
				printnl()
				
				IF NOT IS_INCIDENT_VALID(incidentArmy)
					
					IF IS_NET_PLAYER_OK(playerGangTarget)
						IF serverBD.bIsHitSquad
							CREATE_INCIDENT_WITH_ENTITY(DT_BIKER_BACKUP, GET_PLAYER_PED(playerGangTarget), 4, 0.0, incidentArmy, ENUM_TO_INT(RELGROUPHASH_PRIVATE_SECURITY))
						ELIF serverBD.bIsAssassins
							CREATE_INCIDENT_WITH_ENTITY(DT_ASSASSINS, GET_PLAYER_PED(playerGangTarget), 4, 0.0, incidentArmy, ENUM_TO_INT(RELGROUPHASH_PRIVATE_SECURITY),serverBD.eAssassinLevel)
						ELSE
							CREATE_INCIDENT_WITH_ENTITY(DT_ARMY_VEHICLE, GET_PLAYER_PED(playerGangTarget), 4, 0.0, incidentArmy)
						ENDIF
					ENDIF
				
				else 
				
					printstring("incidentArmy exists")
					printnl()
				ENDIF
				
				IF MPGlobalsAmbience.bHasBeenDamagedByMercenaries
					
					printstring("debug test 4")
					printnl()
					
					IF NOT IS_BIT_SET(playerBD[NETWORK_PLAYER_ID_TO_INT()].iBitset, CLIENT_BS_ATTACKED_BY_MERRYWEATHER)
						SET_BIT(playerBD[NETWORK_PLAYER_ID_TO_INT()].iBitset, CLIENT_BS_ATTACKED_BY_MERRYWEATHER)
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === SET_BIT(playerBD[", NETWORK_PLAYER_ID_TO_INT(), "].iBitset, CLIENT_BS_ATTACKED_BY_MERRYWEATHER)")
						#ENDIF
					
						//script_assert("MPGlobalsAmbience.bHasBeenDamagedByMercenaries SET")
					
					ENDIF
				
				else 
				
					printstring("debug test 5")
					printnl()
				
				ENDIF
				
			ENDIF
		ENDIF
	ELIF iGangVariation = GANG_CALL_TYPE_SPECIAL_THIEF
		IF serverBD.iSuccessThiefID <> -1
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.gangMember[serverBD.iSuccessThiefID].netID)
				IF NOT IS_ENTITY_DEAD(NET_TO_PED(serverBD.gangMember[serverBD.iSuccessThiefID].netID))
				AND NOT IS_MUGGER_IN_PARCEL_VEHICLE(NET_TO_PED(serverBD.gangMember[serverBD.iSuccessThiefID].netID))
				
					IF serverBD.gangMember[serverBD.iSuccessThiefID].gangStage = GANG_STAGE_THIEF_FLEE
						UPDATE_ENEMY_NET_PED_BLIP(	serverBD.gangMember[serverBD.iSuccessThiefID].netID, 
													aiBlipThief, 
													DEFAULT,
													FALSE, 
													TRUE )
						
						IF DOES_BLIP_EXIST(aiBlipThief.BlipID)
							IF NOT IS_BIT_SET(iLocalBitset, LOCAL_BS_CLIENT_THIEF_BLIP_NAMED)
								SET_BLIP_NAME_FROM_TEXT_FILE(aiBlipThief.blipId, "FM_MGR_BLP")//Mugger
								SET_BIT(iLocalBitset, LOCAL_BS_CLIENT_THIEF_BLIP_NAMED)
							ENDIF
						ENDIF
						
					ENDIF
				ELSE
					IF DOES_BLIP_EXIST(aiBlipThief.BlipID)
						CLEANUP_AI_PED_BLIP(aiBlipThief)
					ENDIF
				ENDIF
			ENDIF
			
			IF PLAYER_ID() = playerGangOwner
				IF NOT IS_BIT_SET(iLocalBitset, LOCAL_BS_PHONECALL_THIEF_SUCCESS)
					IF DO_GANG_CALL_PHONECALL("MPCT_mugsuc", "LAMAR", CHAR_LAMAR)
						SET_BIT(iLocalBitset, LOCAL_BS_PHONECALL_THIEF_SUCCESS)
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Performs local every-frame logic that every player will run
PROC CLIENT_PROCESSING()
	CLIENT_LOGIC()
	IF iGangVariation <> GANG_CALL_TYPE_SPECIAL_MERRYWEATHER
		CLIENT_STAGGERED_GANG_LOOP()
	ENDIF
ENDPROC

/// PURPOSE:
///    Do necessary pre game start ini
/// PARAMS:
///    missionScriptArgs - 
PROC PROCESS_PRE_GAME( MP_MISSION_DATA missionScriptArgs )
	
	// This marks the script as a net script, and handles any instancing setup. 
	SETUP_MP_MISSION_FOR_NETWORK(NUM_NETWORK_PLAYERS, missionScriptArgs)
	
	// This makes sure the net script is active, waits untill it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
	
	// This script will not be paused if another script calls PAUSE_GAME
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	
	// KGM: Wait for the first network broadcast before moving on - call this as the last instruction in pre-game
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === Failed to receive intial network broadcast. Cleaning up.")
		#ENDIF
		MISSION_CLEANUP()
	ENDIF
	
	iGangVariation = missionScriptArgs.iInstanceId
	
	playerGangOwner  = INT_TO_PLAYERINDEX(missionScriptArgs.mdUniqueID)
	IF NETWORK_IS_PLAYER_ACTIVE(playerGangOwner)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === - Player Gang Owner = ", GET_PLAYER_NAME(playerGangOwner))
	ENDIF
	
	playerGangTarget = INT_TO_PLAYERINDEX(missionScriptArgs.iBitSet)
	IF NETWORK_IS_PLAYER_ACTIVE(playerGangTarget)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === - Player Gang Target = ", GET_PLAYER_NAME(playerGangTarget))
		iGangTargetPlayerNameHash = GET_HASH_KEY(GET_PLAYER_NAME(playerGangTarget))
	ENDIF
	
	IF GB_IS_GLOBAL_CLIENT_BIT1_SET(playerGangOwner, eGB_GLOBAL_CLIENT_BITSET_1_LAUNCHING_HIT_SQUAD)
	AND iGangVariation = GANG_CALL_TYPE_SPECIAL_MERRYWEATHER
		serverBD.bIsHitSquad = TRUE
		CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === - Player is launching a Hit Squad variation")
		IF GB_IS_GLOBAL_CLIENT_BIT1_SET(playerGangTarget, eGB_GLOBAL_CLIENT_BITSET_1_ABANDONED_BIKER_GANG)
			serverBD.bIsAbandonHit = TRUE
			CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === - Player is launching an Abandon Hit Squad variation")
		ENDIF
	ENDIF
	
	IF iGangVariation = GANG_CALL_TYPE_SPECIAL_MERRYWEATHER
		IF HAS_PLAYER_SELECTED_ASSASSINS_TARGET(playerGangOwner)
			serverBD.bIsAssassins = TRUE
			serverBD.eAssassinLevel = GET_SELECTED_ASSASINS_ABILITY_LEVEL(playerGangOwner)
			CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === - Player is launching an Assassin level ",serverBD.eAssassinLevel," variation")
		ENDIF
	ENDIF
	
	vGangInitialCoords = missionScriptArgs.mdPrimaryCoords
	
	IF iGangVariation = GANG_CALL_TYPE_SPECIAL_MERRYWEATHER
		IF PLAYER_ID() = playerGangOwner
			SET_BIT(g_iBuddyChangeBitset, GLOBAL_BUDDY_CHANGE_BS_GANG_CALL)		//if it's the army, everyone set the army setupe
		ENDIF
	ENDIF
	
	g_GangCallFailedToReachLocalPlayerBitset = 0
	g_GangCallReachedLocalPlayerBitset = 0
	
	IF iGangVariation = GANG_CALL_TYPE_SPECIAL_MERRYWEATHER
		MPGlobalsAmbience.bHasBeenDamagedByMercenaries = FALSE
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "=== GANG BACKUP === MPGlobalsAmbience.bHasBeenDamagedByMercenaries = FALSE")
		#ENDIF
	ENDIF
	
	playerBD[NETWORK_PLAYER_ID_TO_INT()].iGameState = GAME_STATE_INI
	
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === PRE_GAME DONE")
	#ENDIF
ENDPROC

FUNC INT GET_ESCAPE_TIME()

	IF serverBD.bIsAssassins
		RETURN 300000
	ENDIF
	
	RETURN GANG_CALL_TIME_TO_REFUND_MERRYWEATHER

ENDFUNC

//
/// PURPOSE:
///    Server only function that checks to see if the game mode should now end.
/// RETURNS:
///    true/false
FUNC BOOL HAVE_MISSION_END_CONDITIONS_BEEN_MET()
	
	IF IS_BIT_SET(ServerBD.iBitset, SERVER_BS_GANG_CREATED)

		IF iGangVariation = GANG_CALL_TYPE_SPECIAL_MERRYWEATHER
			IF IS_BIT_SET(serverBD.iBitset, SERVER_BS_TIME_UP)										//Time's up
				SET_BIT(serverBD.iFailBitSet, FAIL_BS_ARMY_TARGET_SURVIVE)
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === SET_BIT(serverBD.iFailBitSet, FAIL_BS_ARMY_TARGET_SURVIVE)")
				#ENDIF
				RETURN TRUE
			ENDIF
		ELSE
			IF IS_BIT_SET(serverBD.iBitset, SERVER_BS_ALL_GANG_MEMBERS_DEAD)						//Everyone is dead
				SET_BIT(serverBD.iFailBitSet, FAIL_BS_GANG_DEAD)
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === SET_BIT(serverBD.iFailBitSet, FAIL_BS_GANG_DEAD)")
				#ENDIF
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
	
		IF IS_BIT_SET(ServerBD.iBitset, SERVER_BS_UNABLE_TO_FIND_SPAWN_LOCATION)
			SET_BIT(serverBD.iFailBitSet, FAIL_BS_UNABLE_TO_FIND_SPAWN_LOCATION)
			SET_BIT(g_GangCallFailedToReachLocalPlayerBitset, iGangVariation)
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === SET_BIT(g_GangCallFailedToReachLocalPlayerBitset, iGangVariation)")
			#ENDIF
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === SET_BIT(serverBD.iFailBitSet, FAIL_BS_UNABLE_TO_FIND_SPAWN_LOCATION)")
			#ENDIF
			RETURN TRUE
		ENDIF
		
	ENDIF
	
	IF IS_BIT_SET(serverBD.iFailBitset, fail_bs_gang_attack_on_same_gang_members)
	
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === SET_BIT(serverBD.iFailBitSet, fail_bs_gang_attack_on_same_gang_members)")
		#ENDIF
	
		return true 
		
	endif 
	
	IF iGangVariation = GANG_CALL_TYPE_SPECIAL_THIEF
		IF IS_BIT_SET(serverBD.iThiefBitSet, THIEF_BS_ESCAPED)								//Everyone too far from thief
			SET_BIT(serverBD.iFailBitSet, FAIL_BS_THIEF_ESCAPE)
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === SET_BIT(serverBD.iFailBitSet, FAIL_BS_THIEF_ESCAPE)")
			#ENDIF
			RETURN TRUE
		ENDIF
		
		IF IS_BIT_SET(serverBD.iThiefBitSet, THIEF_BS_ABANDON)								//Thief unable to spawn, bail out
			SET_BIT(serverBD.iFailBitSet, FAIL_BS_THIEF_ABANDON)
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === SET_BIT(serverBD.iFailBitSet, FAIL_BS_THIEF_ABANDON)")
			#ENDIF
			RETURN TRUE
		ENDIF
		
		IF IS_BIT_SET(serverBD.iThiefBitSet, THIEF_BS_SUCCESSFUL_DEAD)
			SET_BIT(serverBD.iFailBitSet, FAIL_BS_SUCCESSFUL_THIEF_DEAD)
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === SET_BIT(serverBD.iFailBitSet, FAIL_BS_SUCCESSFUL_THIEF_DEAD)")
			#ENDIF
			RETURN TRUE
		ENDIF
		
		IF IS_BIT_SET(serverBD.iThiefBitSet, THIEF_BS_NO_MONEY)
			SET_BIT(serverBD.iFailBitSet, FAIL_BS_THEFT_NO_MONEY)
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === SET_BIT(serverBD.iFailBitSet, FAIL_BS_THEFT_NO_MONEY)")
			#ENDIF
			RETURN TRUE
		ENDIF
		
		IF IS_BIT_SET(serverBD.iBitset, SERVER_BS_DUE_REFUND)
			SET_BIT(serverBD.iFailBitSet, FAIL_BS_REFUND)
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === THEIF SET_BIT(serverBD.iFailBitSet, FAIL_BS_REFUND)")
			#ENDIF
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF NOT IS_NET_PLAYER_OK(playerGangTarget, FALSE)	//If target player is invalid, or no longer active, or no longer has the same name, then end
		IF iGangVariation <> GANG_CALL_TYPE_SPECIAL_THIEF					//if not a thief
		OR NOT IS_BIT_SET(serverBD.iThiefBitset, THIEF_BS_SOMEONE_ROBBED)	//or not robbed yet
			SET_BIT(serverBD.iFailBitSet, FAIL_BS_TARGET_PLAYER_INVALID)
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === SET_BIT(serverBD.iFailBitSet, FAIL_BS_TARGET_PLAYER_INVALID)")
			#ENDIF
			RETURN TRUE
		ENDIF
	ELSE
		IF IS_FM_PLAYER_CRITICAL_TO_THIS_FM_EVENT(playerGangTarget, FMMC_TYPE_KILL_LIST)
			SET_BIT(serverBD.iThiefBitSet, FAIL_BS_TARGET_ON_FM_EVENT)
			SET_BIT(serverBD.iFailBitSet, FAIL_BS_THIEF_ABANDON)
			CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === [HAVE_MISSION_END_CONDITIONS_BEEN_MET] - FAIL_BS_TARGET_ON_FM_EVENT is SET - KILL LIST")
			CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === [HAVE_MISSION_END_CONDITIONS_BEEN_MET] - THIEF_BS_ABANDON is SET")
			RETURN TRUE
		ENDIF
		
		IF IS_FM_PLAYER_CRITICAL_TO_THIS_FM_EVENT(playerGangTarget, FMMC_TYPE_MOVING_TARGET)
		OR IS_BIT_SET(GlobalplayerBD_FM_3[NATIVE_TO_INT(playerGangTarget)].iFmAmbientEventBitSet, ciSET_PLAYER_IN_MOVING_TARGET_VEHICLE)
			SET_BIT(serverBD.iThiefBitSet, FAIL_BS_TARGET_ON_FM_EVENT)
			SET_BIT(serverBD.iThiefBitSet, THIEF_BS_ABANDON)	
			CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === [HAVE_MISSION_END_CONDITIONS_BEEN_MET] - FAIL_BS_TARGET_ON_FM_EVENT is SET - MOVING TARGET")
			CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === [HAVE_MISSION_END_CONDITIONS_BEEN_MET] - THIEF_BS_ABANDON is SET")
			RETURN TRUE
		ENDIF
		
		// 2428310: Prevent mugger griefing in Penned In
		IF IS_FM_PLAYER_CRITICAL_TO_THIS_FM_EVENT(playerGangTarget, FMMC_TYPE_PENNED_IN)
		OR (IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_PENNED_IN)
		AND FM_EVENT_IS_PLAYER_WAITING_FOR_FM_EVENT_TO_START(playerGangTarget))
			SET_BIT(serverBD.iThiefBitSet, THIEF_BS_ABANDON)	
			RETURN TRUE
		ENDIF
		
		// 2435131: Cancel mugger if target is the beast
		IF IS_FM_PLAYER_CRITICAL_TO_THIS_FM_EVENT(playerGangTarget, FMMC_TYPE_HUNT_THE_BEAST)
			SET_BIT(serverBD.iThiefBitSet, THIEF_BS_ABANDON)
			CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === [HAVE_MISSION_END_CONDITIONS_BEEN_MET] - THIEF_BS_ABANDON is SET - Target is critical to Hunt the Beast.")
			RETURN TRUE
		ENDIF
		
		#IF FEATURE_GEN9_EXCLUSIVE
			IF GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(playerGangTarget) = FMMC_TYPE_HSW_SETUP
				SET_BIT(serverBD.iThiefBitSet, THIEF_BS_ABANDON)
				CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === [HAVE_MISSION_END_CONDITIONS_BEEN_MET] - THIEF_BS_ABANDON is SET - IS_PLAYER_ON_MP_INTRO")
				RETURN TRUE
			ENDIF
		#ENDIF
		
				
		IF GB_IS_PLAYER_CRITICAL_BOSS_ON_JOB(PLAYER_ID())
		AND (GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_GB_BOSS_DEATHMATCH
		OR GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_GB_SIGHTSEER
		OR GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_GB_HUNT_THE_BOSS)
			SET_BIT(serverBD.iThiefBitSet, THIEF_BS_ABANDON)
			CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === [HAVE_MISSION_END_CONDITIONS_BEEN_MET] - THIEF_BS_ABANDON is SET - GB_IS_PLAYER_CRITICAL_BOSS_ON_JOB")
			RETURN TRUE
		ENDIF
		
		IF GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_GB_CHAL_FIVESTAR
			SET_BIT(serverBD.iThiefBitSet, THIEF_BS_ABANDON)
			CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === [HAVE_MISSION_END_CONDITIONS_BEEN_MET] - THIEF_BS_ABANDON is SET - FMMC_TYPE_GB_CHAL_FIVESTAR")
			RETURN TRUE
		ENDIF
		
		IF GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID())
		AND GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_GB_AIRFREIGHT
			SET_BIT(serverBD.iThiefBitSet, THIEF_BS_ABANDON)
			CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === [HAVE_MISSION_END_CONDITIONS_BEEN_MET] - THIEF_BS_ABANDON is SET - FMMC_TYPE_GB_AIRFREIGHT")
			RETURN TRUE
		ENDIF
		
		IF GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_GB_CONTRABAND_BUY
			SET_BIT(serverBD.iThiefBitSet, THIEF_BS_ABANDON)
			CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === [HAVE_MISSION_END_CONDITIONS_BEEN_MET] - THIEF_BS_ABANDON is SET - FMMC_TYPE_GB_CONTRABAND_BUY")
			RETURN TRUE
		ENDIF
		
		IF GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_GB_CONTRABAND_SELL
			SET_BIT(serverBD.iThiefBitSet, THIEF_BS_ABANDON)
			CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === [HAVE_MISSION_END_CONDITIONS_BEEN_MET] - THIEF_BS_ABANDON is SET - FMMC_TYPE_GB_CONTRABAND_SELL")
			RETURN TRUE
		ENDIF
		
		
		IF GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID())
		AND GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_FMBB_DEFEND
			SET_BIT(serverBD.iThiefBitSet, THIEF_BS_ABANDON)
			CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === [HAVE_MISSION_END_CONDITIONS_BEEN_MET] - THIEF_BS_ABANDON is SET - FMMC_TYPE_FMBB_DEFEND")
			RETURN TRUE
		ENDIF
		
		IF GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID())
		AND GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_FMBB_PHONECALL
			SET_BIT(serverBD.iThiefBitSet, THIEF_BS_ABANDON)
			CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === [HAVE_MISSION_END_CONDITIONS_BEEN_MET] - THIEF_BS_ABANDON is SET - FMMC_TYPE_FMBB_PHONECALL")
			RETURN TRUE
		ENDIF
		
		SWITCH GB_GET_GB_CASINO_VARIATION_PLAYER_IS_ON(PLAYER_ID())
			CASE CSV_HIGH_ROLLER
			CASE CSV_HIGH_ROLLER_TOUR
				IF IS_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITITCAL_ENTITY()
					SET_BIT(serverBD.iThiefBitSet, THIEF_BS_ABANDON)
					CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === [HAVE_MISSION_END_CONDITIONS_BEEN_MET] - THIEF_BS_ABANDON is SET - ", GET_GBC_VARIATION_NAME_FOR_DEBUG(GB_GET_GB_CASINO_VARIATION_PLAYER_IS_ON(PLAYER_ID())))
					RETURN TRUE
				ENDIF
			BREAK
		ENDSWITCH
		
		IF IS_PLAYER_AN_ANIMAL(playerGangTarget)
			SET_BIT(serverBD.iThiefBitSet, THIEF_BS_ABANDON)
			CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === [HAVE_MISSION_END_CONDITIONS_BEEN_MET] - IS_PLAYER_AN_ANIMAL ")
			RETURN TRUE
		ENDIF
		
		IF IS_PLAYER_CRITICAL_TO_ANY_RANDOM_EVENT(playerGangTarget)
			SET_BIT(serverBD.iThiefBitSet, THIEF_BS_ABANDON)
			CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === [HAVE_MISSION_END_CONDITIONS_BEEN_MET] - THIEF_BS_ABANDON is SET - IS_PLAYER_CRITICAL_TO_ANY_RANDOM_EVENT")
			RETURN TRUE
		ENDIF
		
		IF GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(playerGangTarget) = FMMC_TYPE_SANDBOX_ACTIVITY
			IF GB_GET_CONTRABAND_VARIATION_PLAYER_IS_ON(playerGangTarget) = ENUM_TO_INT(SAV_SPRINT)
				SET_BIT(serverBD.iThiefBitSet, THIEF_BS_ABANDON)
				CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === [HAVE_MISSION_END_CONDITIONS_BEEN_MET] - THIEF_BS_ABANDON is SET - FMMC_TYPE_SANDBOX_ACTIVITY, SAV_SPRINT")
				RETURN TRUE
			ENDIF
		ENDIF
		
		IF IS_ENTITY_DEAD(GET_PLAYER_PED(playerGangTarget))
			IF iGangVariation = GANG_CALL_TYPE_SPECIAL_MERRYWEATHER
				IF IS_BIT_SET(ServerBD.iBitset, SERVER_BS_GANG_CREATED)
					SET_BIT(serverBD.iFailBitSet, FAIL_BS_ARMY_TARGET_DEAD)
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === SET_BIT(serverBD.iFailBitSet, FAIL_BS_ARMY_TARGET_DEAD)")
					#ENDIF
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE
		
			IF iGangVariation = GANG_CALL_TYPE_SPECIAL_MERRYWEATHER
			
				IF IS_FM_PLAYER_CRITICAL_TO_THIS_FM_EVENT(playerGangTarget, FMMC_TYPE_HUNT_THE_BEAST)
					SET_BIT(serverBD.iFailBitset, FAIL_BS_ARMY_TARGET_ESCAPE)
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === SET_BIT(serverBD.iFailBitset, FAIL_BS_ARMY_TARGET_ESCAPE) due to player being critical to Hunt the Beast")
					#ENDIF
					//script_assert("MPCT_MERloc - REFUND MONEY")
					RETURN TRUE
				ENDIF
			
				IF NOT IS_BIT_SET(serverBD.iBitset, SERVER_BS_TARGET_ATTACKED_BY_MERRYWEATHER)
					IF GET_TIME_DIFFERENCE(timeNet, serverBD.timeRefund) > GET_ESCAPE_TIME()
						SET_BIT(serverBD.iFailBitset, FAIL_BS_ARMY_TARGET_ESCAPE)
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === SET_BIT(serverBD.iFailBitset, FAIL_BS_ARMY_TARGET_ESCAPE) due to not getting hit")
						#ENDIF
						//script_assert("MPCT_MERloc - REFUND MONEY")
						RETURN TRUE
					ENDIF
				ENDIF
			
				IF HAS_TARGET_PLAYER_ESCAPED_ARMY()
					SET_BIT(serverBD.iFailBitSet, FAIL_BS_ARMY_TARGET_ESCAPE)
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === SET_BIT(serverBD.iFailBitSet, FAIL_BS_ARMY_TARGET_ESCAPE)")
					#ENDIF
					RETURN TRUE
				ENDIF
				
				IF GB_IS_PLAYER_MEMBER_OF_THIS_BIKER_GANG(playerGangTarget, GB_GET_THIS_PLAYER_GANG_BOSS(playerGangOwner))
					SET_BIT(serverBD.iFailBitSet, FAIL_BS_ARMY_TARGET_ESCAPE)
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === SET_BIT(serverBD.iFailBitSet, FAIL_BS_ARMY_TARGET_ESCAPE) as target and owner are in the same gang again.")
					#ENDIF
					RETURN TRUE
				ENDIF
			ENDIF
		
			IF iGangTargetPlayerNameHash <> GET_HASH_KEY(GET_PLAYER_NAME(playerGangTarget))
				SET_BIT(serverBD.iFailBitSet, FAIL_BS_TARGET_NAME_CHANGED)
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === SET_BIT(serverBD.iFailBitSet, FAIL_BS_TARGET_NAME_CHANGED)")
				#ENDIF
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Helper function to get a clients game/mission state
/// PARAMS:
///    iPlayer - ID of player to check
/// RETURNS:
///    game state as an INT
FUNC INT GET_CLIENT_MISSION_STATE(INT iPlayer)
	RETURN playerBD[iPlayer].iGameState
ENDFUNC

/// PURPOSE:
///    Helper function to get the servers game/mission state
/// RETURNS:
///    true/false
FUNC INT GET_SERVER_MISSION_STATE()
	RETURN serverBD.iServerGameState
ENDFUNC

#IF IS_DEBUG_BUILD

/// PURPOSE:
///    Creates widgets for the gang script
PROC CREATE_WIDGETS()
	START_WIDGET_GROUP("AM_GANG_CALL")
		SWITCH iGangVariation
			CASE GANG_CALL_TYPE_SPECIAL_THIEF
				ADD_WIDGET_STRING("GANG_CALL_TYPE_SPECIAL_THIEF")
			BREAK
			CASE GANG_CALL_TYPE_SPECIAL_MERRYWEATHER
				ADD_WIDGET_STRING("GANG_CALL_TYPE_SPECIAL_MERRYWEATHER")
			BREAK
		ENDSWITCH
		ADD_WIDGET_INT_READ_ONLY("Time Left", iDebugTimeLeft)
		ADD_WIDGET_INT_READ_ONLY("Thief Escape Timer", iDebugThiefEscapeTimeLeft)
		ADD_WIDGET_INT_READ_ONLY("Thief Abandon Timer", iDebugThiefAbandonTimeLeft)
		ADD_WIDGET_BOOL("Is Script Host", bDebugIsScriptHost)
		ADD_WIDGET_BOOL("Skip Time To End", bDebugSkipTimeToEnd)
		ADD_WIDGET_BOOL("Skip Thief To Escaped", bDebugSkipThiefToEscaped)
		ADD_WIDGET_BOOL("Skip Thief To Abandon", bDebugSkipThiefToAbandon)
		ADD_WIDGET_BOOL("Server Force End", bDebugServerForceEnd)
		ADD_WIDGET_BOOL("Blip Gang Members", bDebugBlipGangMembers)
		ADD_WIDGET_BOOL("Force Spawn Thief", bDebugSpawnThief)
	STOP_WIDGET_GROUP()
ENDPROC		

/// PURPOSE:
///    Updates the widgets for the gang script
PROC UPDATE_WIDGETS()
	INT i
	iDebugTimeLeft = GANG_CALL_FIGHT_TIME - GET_TIME_DIFFERENCE(timeNet, serverBD.timeStarted)
	
	IF IS_BIT_SET(serverBD.iThiefBitSet, THIEF_BS_ESCAPE_TIMER_STARTED)
		iDebugThiefEscapeTimeLeft = GANG_CALL_THIEF_ESCAPE_TIME - GET_TIME_DIFFERENCE(timeNet, serverBD.timeThiefEscape)
	ELSE
		iDebugThiefEscapeTimeLeft = GANG_CALL_THIEF_ESCAPE_TIME
	ENDIF
	
	IF IS_BIT_SET(serverBD.iThiefBitSet, THIEF_BS_ABANDON_TIMER_STARTED)
		iDebugThiefAbandonTimeLeft = GANG_CALL_THIEF_ABANDON_TIME - GET_TIME_DIFFERENCE(timeNet, serverBD.timeThiefAbandon)
	ELSE
		iDebugThiefAbandonTimeLeft = GANG_CALL_THIEF_ABANDON_TIME
	ENDIF
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		bDebugIsScriptHost = TRUE
		IF bDebugSkipTimeToEnd
			SET_BIT(serverBD.iBitset, SERVER_BS_TIME_UP)
//			IF iGangVariation <> GANG_CALL_TYPE_SPECIAL_MERRYWEATHER //forces the gang loop to process the server 
//				SERVER_STAGGERED_GANG_LOOP()
//			ENDIF
		ENDIF
		IF bDebugSkipThiefToEscaped
			SET_BIT(serverBD.iThiefBitSet, THIEF_BS_ESCAPED)
		ENDIF
		IF bDebugSkipThiefToAbandon
			SET_BIT(serverBD.iThiefBitSet, THIEF_BS_ABANDON)
		ENDIF
		IF bDebugServerForceEnd
			serverBD.iServerGameState = GAME_STATE_END
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === bDebugServerForceEnd serverBD.iServerGameState = GAME_STATE_END")
			#ENDIF
		ENDIF
	ELSE
		bDebugIsScriptHost = FALSE
		bDebugSkipTimeToEnd = FALSE
		bDebugSkipThiefToEscaped = FALSE
		bDebugSkipThiefToAbandon = FALSE
	ENDIF
	IF bDebugBlipGangMembers
		REPEAT GET_NUMBER_OF_GANG_MEMBERS_IN_GANG(iGangVariation) i
			IF NOT DOES_BLIP_EXIST(blipDebug[i])
				IF NOT IS_NET_PED_INJURED(serverBD.gangMember[i].netID)
					blipDebug[i] = ADD_BLIP_FOR_ENTITY(NET_TO_PED(serverBD.gangMember[i].netID))
				ENDIF
			ENDIF
		ENDREPEAT
	ELSE
		REPEAT GET_NUMBER_OF_GANG_MEMBERS_IN_GANG(iGangVariation) i
			IF DOES_BLIP_EXIST(blipDebug[i])
				REMOVE_BLIP(blipDebug[i])
			ENDIF
		ENDREPEAT
	ENDIF
	
ENDPROC

#ENDIF

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  MISSION PROCS                  //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

/// PURPOSE:
///    Returns if enough time has passed to spawn the thief
/// RETURNS:
///    true/false
func bool is_timer_met_to_spawn_theif()
						
	IF not IS_BIT_SET(serverBD.iBitset, SERVER_BS_FIRST_TIME_THEIF_SPAWNED)
		
		IF GET_TIME_DIFFERENCE(timeNet, serverBD.timeStarted) > GANG_CALL_TIME_FOR_THIEF_TO_SPAWN
		
			set_bit(serverBD.iBitset, SERVER_BS_FIRST_TIME_THEIF_SPAWNED)
			
			return true 
			
		endif 
		
	else 
	
		IF GET_TIME_DIFFERENCE(timeNet, serverBD.timeStarted) > gang_call_theif_respawn_time
		
			return true 
			
		endif 
		
	endif 
		
	return false 

endfunc 

/// PURPOSE:
///    Returns if now is the correct time to start spawning the current gang
/// RETURNS:
///    true/false
FUNC BOOL IS_OK_TO_SPAWN_GANG_NOW()
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF IS_BIT_SET(serverBd.iBitset, SERVER_BS_OK_TO_SPAWN_GANG_NOW)
			RETURN TRUE
		ELSE
			BOOL bStartTheTimer = FALSE
			SWITCH iGangVariation
				CASE GANG_CALL_TYPE_SPECIAL_THIEF
					IF IS_BIT_SET(serverBD.iThiefBitSet, THIEF_BS_WAITING_FOR_NO_PATH_RESET)		//if thieves failed last time from no path, wait til new position
						IF IS_NET_PLAYER_OK(playerGangTarget)
							IF VDIST2(GET_PLAYER_COORDS(playerGangTarget), serverBD.vThiefNoPathCoord) > THIEF_NO_PATH_CLEAR_SQR
								#IF IS_DEBUG_BUILD
								CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === CLEAR_BIT(serverBD.iThiefBitSet, THIEF_BS_WAITING_FOR_NO_PATH_RESET)")
								#ENDIF
								CLEAR_BIT(serverBD.iThiefBitSet, THIEF_BS_WAITING_FOR_NO_PATH_RESET)
								CLEAR_BIT(iLocalBitset, LOCAL_BS_TICKER_THIEF_NO_PATH)
							ENDIF
						ENDIF
					ELSE
						
						IF IS_NET_PLAYER_OK(playerGangTarget)
						
							bStartTheTimer = TRUE			//Player is active and alive, set as ready to spawn

							//need to sycle through all clients and check them
							
							IF IS_TARGET_PLAYER_BLOCKING_THIEF()
								bStartTheTimer = FALSE
							ENDIF
						ENDIF
								
						IF bStartTheTimer
							START_SERVER_TIMER()
						ELSE	
							RESET_SERVER_TIMER()
						ENDIF

						IF IS_BIT_SET(serverBD.iBitset, SERVER_BS_TIMER_STARTED)
							//IF GET_TIME_DIFFERENCE(timeNet, serverBD.timeStarted) > GANG_CALL_TIME_FOR_THIEF_TO_SPAWN
							if is_timer_met_to_spawn_theif()
								RESET_SERVER_TIMER()
								SET_BIT(serverBd.iBitset, SERVER_BS_OK_TO_SPAWN_GANG_NOW)
								#IF IS_DEBUG_BUILD
								CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === SET_BIT(serverBd.iBitset, SERVER_BS_OK_TO_SPAWN_GANG_NOW)")
								#ENDIF
							ENDIF
						ENDIF
						
					ENDIF
				BREAK
				CASE GANG_CALL_TYPE_SPECIAL_MERRYWEATHER 
					IF IS_NET_PLAYER_OK(playerGangTarget)
					
						bStartTheTimer = TRUE			//Player is active and alive, set as ready to spawn
						
						IF IS_TARGET_PLAYER_BLOCKING_ARMY()
							bStartTheTimer = FALSE
						ENDIF
					ENDIF
							
					IF bStartTheTimer
						START_SERVER_TIMER()
					ELSE	
						RESET_SERVER_TIMER()
					ENDIF
					
					IF IS_BIT_SET(serverBD.iBitset, SERVER_BS_TIMER_STARTED)
						IF GET_TIME_DIFFERENCE(timeNet, serverBD.timeStarted) > GANG_CALL_TIME_FOR_ARMY_TO_SPAWN
							RESET_SERVER_TIMER()
							SET_BIT(serverBd.iBitset, SERVER_BS_OK_TO_SPAWN_GANG_NOW)
							#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === SET_BIT(serverBd.iBitset, SERVER_BS_OK_TO_SPAWN_GANG_NOW)")
							#ENDIF
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Sets the ped_index properties so that the ped will act appropriately as a gang member
/// PARAMS:
///    iGangID - gang member ID to set up
PROC SETUP_GANG_MEMBER(INT iGangID)
	
	IF NOT IS_BIT_SET(serverBD.gangMember[iGangID].iBitSet, GANG_BS_CREATED_AND_SET_UP)
		
		IF NOT IS_ENTITY_DEAD(NET_TO_PED(serverBD.gangMember[iGangID].netID))
			
			//Set Relationship Info
			IF serverBD.gangMember[iGangID].gangClass = GANG_CLASS_THIEF
				
				SET_PED_RELATIONSHIP_GROUP_HASH(NET_TO_PED(serverBD.gangMember[iGangID].netID), rgFM_AiDislike)
				SET_PED_PATH_MAY_ENTER_WATER(NET_TO_PED(serverBD.gangMember[iGangID].netID), TRUE)
				SET_PED_PATH_PREFER_TO_AVOID_WATER(NET_TO_PED(serverBD.gangMember[iGangID].netID), FALSE)
				
			ENDIF
			
			SET_ENTITY_PROOFS(NET_TO_PED(serverBD.gangMember[iGangID].netID), FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, TRUE, FALSE)
			
			SET_PED_COMBAT_ABILITY(NET_TO_PED(serverBD.gangMember[iGangID].netID), CAL_PROFESSIONAL)
			
			SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(serverBD.gangMember[iGangID].netID), CA_AGGRESSIVE, TRUE)
			SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(serverBD.gangMember[iGangID].netID), CA_CAN_COMMANDEER_VEHICLES, TRUE)
			SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(serverBD.gangMember[iGangID].netID), CA_CAN_TAUNT_IN_VEHICLE, TRUE)
			
			SET_PED_CONFIG_FLAG(NET_TO_PED(serverBD.gangMember[iGangID].netID), PCF_KeepRelationshipGroupAfterCleanUp, TRUE)
			SET_PED_CONFIG_FLAG(NET_TO_PED(serverBD.gangMember[iGangID].netID), PCF_RunFromFiresAndExplosions, FALSE)
			SET_PED_CONFIG_FLAG(NET_TO_PED(serverBD.gangMember[iGangID].netID), PCF_ShoutToGroupOnPlayerMelee, TRUE)
			
			SET_PED_TO_INFORM_RESPECTED_FRIENDS(NET_TO_PED(serverBD.gangMember[iGangID].netID), 300, 10)
			
			set_ped_keep_task(NET_TO_PED(serverBD.gangMember[iGangID].netID), true)

			text_label debug_label = "gang "
			debug_label += iGangID
			
			set_ped_name_debug(NET_TO_PED(serverBD.gangMember[iGangID].netID), debug_label) 

			//SET_PED_PATH_MAY_ENTER_WATER(NET_TO_PED(serverBD.gangMember[iGangID].netID), FALSE)
			//SET_PED_PATH_PREFER_TO_AVOID_WATER(NET_TO_PED(serverBD.gangMember[iGangID].netID), FALSE)
			
			SWITCH serverBD.gangMember[iGangID].gangClass
				
				CASE GANG_CLASS_THIEF
					SET_GANG_MEMBER_STAGE(iGangID, GANG_STAGE_THIEF_APPROACH)
					SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(serverBD.gangMember[iGangID].netID), CA_USE_COVER, FALSE)
					SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(serverBD.gangMember[iGangID].netID), CA_DO_DRIVEBYS, FALSE)
					SET_PED_COMBAT_MOVEMENT(NET_TO_PED(serverBD.gangMember[iGangID].netID), CM_WILLADVANCE)
					SET_PED_COMBAT_RANGE(NET_TO_PED(serverBD.gangMember[iGangID].netID),CR_NEAR)
					SET_PED_USING_ACTION_MODE(NET_TO_PED(serverBD.gangMember[iGangID].netID), FALSE)
					SET_PED_CONFIG_FLAG(NET_TO_PED(serverBD.gangMember[iGangID].netID), PCF_WillFlyThroughWindscreen, TRUE)
					SET_PED_FLEE_ATTRIBUTES(NET_TO_PED(serverBD.gangMember[iGangID].netID), FA_CAN_SCREAM, FALSE)
					SET_PED_FLEE_ATTRIBUTES(NET_TO_PED(serverBD.gangMember[iGangID].netID), FA_DISABLE_COWER, TRUE)
					SET_PED_FLEE_ATTRIBUTES(NET_TO_PED(serverBD.gangMember[iGangID].netID), FA_DISABLE_HANDS_UP, TRUE)
					SET_PED_FLEE_ATTRIBUTES(NET_TO_PED(serverBD.gangMember[iGangID].netID), FA_LOOK_FOR_CROWDS, FALSE)
					SET_PED_FLEE_ATTRIBUTES(NET_TO_PED(serverBD.gangMember[iGangID].netID), FA_PREFER_PAVEMENTS, FALSE)
					SET_PED_FLEE_ATTRIBUTES(NET_TO_PED(serverBD.gangMember[iGangID].netID), FA_RETURN_TO_ORIGNAL_POSITION_AFTER_FLEE, FALSE)
					SET_PED_FLEE_ATTRIBUTES(NET_TO_PED(serverBD.gangMember[iGangID].netID), FA_UPDATE_TO_NEAREST_HATED_PED, TRUE)
					SET_PED_FLEE_ATTRIBUTES(NET_TO_PED(serverBD.gangMember[iGangID].netID), FA_USE_COVER, FALSE)
					SET_PED_FLEE_ATTRIBUTES(NET_TO_PED(serverBD.gangMember[iGangID].netID), FA_USE_VEHICLE, TRUE)
					SET_PED_FLEE_ATTRIBUTES(NET_TO_PED(serverBD.gangMember[iGangID].netID), FA_WANDER_AT_END, TRUE)
					
					IF NOT IS_BIT_SET(serverBD.gangMember[iGangID].iBitSet, GANG_BS_CREATED_FROM_AMBIENT)
						SET_BIT(serverBD.gangMember[iGangID].iBitSet, GANG_BS_THIEF_BROKEN_COVER)
					ENDIF
				
					SET_PED_FLEE_ATTRIBUTES(NET_TO_PED(serverBD.gangMember[iGangID].netID), FA_NEVER_FLEE, TRUE)
					SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(serverBD.gangMember[iGangID].netID), CA_ALWAYS_FLEE, FALSE)
					
					GIVE_WEAPON_TO_PED(NET_TO_PED(serverBD.gangMember[iGangID].netID), WEAPONTYPE_KNIFE, 1, TRUE, TRUE)
					SET_PED_MONEY(NET_TO_PED(serverBD.gangMember[iGangID].netID), 0)
					
					TASK_LOOK_AT_ENTITY(NET_TO_PED(serverBD.gangMember[iGangID].netID), GET_PLAYER_PED(playerGangTarget), -1)
					
					SET_NETWORK_ID_PASS_CONTROL_IN_TUTORIAL(serverBD.gangMember[iGangID].netID,TRUE)
				BREAK
			ENDSWITCH
			
			SET_ENTITY_HEALTH(	NET_TO_PED(serverBD.gangMember[iGangID].netID), 		//apply health modifier tuneable
								ROUND(g_sMPTunables.fAiHealthModifier*GET_ENTITY_HEALTH(NET_TO_PED(serverBD.gangMember[iGangID].netID))))
			
			IF NOT IS_BIT_SET(serverBD.iBitSet, SERVER_BS_A_GANG_MEMBER_CREATED)
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === SET_BIT(serverBD.iBitSet, SERVER_BS_A_GANG_MEMBER_CREATED)")
				#ENDIF
				SET_BIT(serverBD.iBitSet, SERVER_BS_A_GANG_MEMBER_CREATED)
			ENDIF
			
			SET_BIT(serverBD.gangMember[iGangID].iBitSet, GANG_BS_CREATED_AND_SET_UP)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Creates a gang member in the gang ID supplied, based on the gang class, coords and heading. Sets up the ped to act like a gang member
/// PARAMS:
///    iGangID - gang ID for the created gang member to have
///    thisGangClass - gang class to create
///    vSpawnCoords - coordinates to spawn the ped at
///    fSpawnHeading - heading to spawn the ped with
PROC CREATE_GANG_MEMBER(INT iGangID, GANG_CLASS thisGangClass, VECTOR vSpawnCoords, FLOAT fSpawnHeading)
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.gangMember[iGangID].netID)
			IF GET_NUM_CREATED_MISSION_PEDS() < GET_NUM_RESERVED_MISSION_PEDS()
				IF CAN_REGISTER_MISSION_PEDS(1)
					IF REQUEST_LOAD_MODEL(GET_GANG_PED_MODEL(iGangVariation))
						IF CREATE_NET_PED(serverBD.gangMember[iGangID].netID, PEDTYPE_GANG1, GET_GANG_PED_MODEL(iGangVariation), vSpawnCoords, fSpawnHeading)
							serverBD.gangMember[iGangID].iGang = iGangVariation
							serverBD.gangMember[iGangID].gangClass = thisGangClass
							SETUP_GANG_MEMBER(iGangID)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Attempts to decide what model to use for the current gang
/// RETURNS:
///    true when complete
FUNC BOOL SETUP_GANG_MODELS()

	IF IS_BIT_SET(serverBD.iBitset, SERVER_BS_GANG_MODELS_SETUP)
		RETURN TRUE
	ELSE
		IF iGangVariation = GANG_CALL_TYPE_SPECIAL_THIEF
			serverBD.mnThief = GET_GANG_MODEL_FOR_AREA(serverBD.vGangTravel)
			SET_BIT(serverBD.iBitset, SERVER_BS_GANG_MODELS_SETUP)
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === mnThief = ", GET_MODEL_NAME_FOR_DEBUG(serverBD.mnThief))
			CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === SET_BIT(serverBD.iBitset, SERVER_BS_GANG_MODELS_SETUP)")
			#ENDIF
			RETURN TRUE
		ELSE
			SET_BIT(serverBD.iBitset, SERVER_BS_GANG_MODELS_SETUP)
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === SET_BIT(serverBD.iBitset, SERVER_BS_GANG_MODELS_SETUP)")
			#ENDIF
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Attempts to locate and grab a suitable nearby ambient ped to turn into a thief
/// RETURNS:
///    true when complete
FUNC BOOL GRAB_AMBIENT_ELEMENTS()
	IF NOT IS_BIT_SET(serverBD.iThiefBitSet, THIEF_BS_AMBIENT_ELEMENT_GRAB_ATTEMPTED)
		SET_BIT(serverBD.iThiefBitSet, THIEF_BS_AMBIENT_ELEMENT_GRAB_ATTEMPTED)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === SET_BIT(serverBD.iThiefBitSet, THIEF_BS_AMBIENT_ELEMENT_GRAB_ATTEMPTED)")
		#ENDIF
	
		IF iGangVariation <> GANG_CALL_TYPE_SPECIAL_THIEF
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === NOT A THIEF, AMBIENT NOT POSSIBLE")
			#ENDIF
			RETURN TRUE
		ELSE
		
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === Attempting ambient grab")
			
			IF bDebugSpawnThief
				CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === DEBUG FORCE SPAWNING THIEF")
				RETURN TRUE
			ENDIF
			#ENDIF
		
			IF IS_NET_PLAYER_OK(playerGangTarget)
				IF (GET_NUM_RESERVED_MISSION_PEDS() - GET_NUM_CREATED_MISSION_PEDS()) >= GET_NUMBER_OF_GANG_MEMBERS_IN_GANG(iGangVariation)
					IF CAN_REGISTER_MISSION_PEDS(GET_NUMBER_OF_GANG_MEMBERS_IN_GANG(iGangVariation))
					
						INT i = 0
						INT j = 0
						PED_INDEX pedTarget = GET_PLAYER_PED(playerGangTarget)
						INT iTotal = GET_PED_NEARBY_PEDS(pedTarget, pedAmbient)
						BOOL bNeedMoreThieves = TRUE
						INT iThiefEntry
						
						REPEAT iTotal i
							IF bNeedMoreThieves
								
								iThiefEntry = -1
								bNeedMoreThieves = FALSE
								REPEAT GET_NUMBER_OF_GANG_MEMBERS_IN_GANG(iGangVariation) j
									IF iThiefEntry = -1
										IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.gangMember[j].netID)
										AND NOT IS_BIT_SET(serverBD.gangMember[j].iBitSet, GANG_BS_CREATED_AND_SET_UP)
										AND NOT IS_BIT_SET(serverBD.gangMember[j].iBitSet, GANG_BS_DEAD)
										AND NOT IS_BIT_SET(serverBD.gangMember[j].iBitSet, GANG_BS_RAW_AMBIENT)
											iThiefEntry = j
											bNeedMoreThieves = TRUE
										ENDIF
									ENDIF
								ENDREPEAT
								
								IF iThiefEntry <> -1
									IF DOES_ENTITY_EXIST(pedAmbient[i])										//Checks to see if ped is suitable
										IF NOT IS_ENTITY_DEAD(pedAmbient[i])
											IF NOT IS_PED_A_PLAYER(pedAmbient[i])
												IF IS_MODEL_SUITABLE_FOR_THIEF(GET_ENTITY_MODEL(pedAmbient[i]))
													IF NOT IS_ENTITY_A_MISSION_ENTITY(pedAmbient[i])
														IF NOT IS_PED_IN_ANY_VEHICLE(pedAmbient[i])
															IF ARE_TWO_POINTS_WITHIN_SQUARE_DISTANCE(	GET_ENTITY_COORDS(pedAmbient[i]), 
																										GET_ENTITY_COORDS(pedTarget), MIN_DISTANCE_THIEF_TO_AMBIENT_GRAB)
																
																SET_ENTITY_AS_MISSION_ENTITY(pedAmbient[i], TRUE)
																serverBD.gangMember[iThiefEntry].iAmbientPedArrayEntry = i
																SET_BIT(serverBD.gangMember[iThiefEntry].iBitSet, GANG_BS_RAW_AMBIENT)
																#IF IS_DEBUG_BUILD
																CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === SET_BIT(serverBD.gangMember[", iThiefEntry, "].iBitSet, GANG_BS_RAW_AMBIENT)")
																#ENDIF
																
																IF NOT IS_BIT_SET(serverBD.iBitset, SERVER_BS_FOUND_AMBIENT_PEDS)
																	#IF IS_DEBUG_BUILD
																	CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === SET_BIT(serverBD.iBitset, SERVER_BS_FOUND_AMBIENT_PEDS)")
																	#ENDIF
																	serverBD.timeAmbientGrab = timeNet
																	SET_BIT(serverBD.iBitset, SERVER_BS_FOUND_AMBIENT_PEDS)
																ENDIF
																	
															ENDIF
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDREPEAT
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
	
		IF IS_BIT_SET(serverBD.iBitset, SERVER_BS_FOUND_AMBIENT_PEDS)
	
			INT i = 0
			INT j = 0
			BOOL bStillRawLeft = FALSE
			REPEAT GET_NUMBER_OF_GANG_MEMBERS_IN_GANG(iGangVariation) i
	
				IF IS_BIT_SET(serverBD.gangMember[i].iBitSet, GANG_BS_CREATED_AND_SET_UP)
				OR IS_BIT_SET(serverBD.gangMember[i].iBitSet, GANG_BS_DEAD)
					j++
				ELSE
					IF IS_BIT_SET(serverBD.gangMember[i].iBitSet, GANG_BS_RAW_AMBIENT)
						
						IF NOT IS_ENTITY_DEAD(pedAmbient[serverBD.gangMember[i].iAmbientPedArrayEntry])
						
							IF IS_ENTITY_A_MISSION_ENTITY(pedAmbient[serverBD.gangMember[i].iAmbientPedArrayEntry])
								
								IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(pedAmbient[serverBD.gangMember[i].iAmbientPedArrayEntry])
									NETWORK_REQUEST_CONTROL_OF_ENTITY(pedAmbient[serverBD.gangMember[i].iAmbientPedArrayEntry])
									bStillRawLeft = TRUE
								ELSE
									REMOVE_ALL_PED_WEAPONS(pedAmbient[serverBD.gangMember[i].iAmbientPedArrayEntry])
									serverBD.gangMember[i].netID = PED_TO_NET(pedAmbient[serverBD.gangMember[i].iAmbientPedArrayEntry])			//Turn ped into net id
									
									SET_NETWORK_ID_EXISTS_ON_ALL_MACHINES(serverBD.gangMember[i].netID, TRUE)
									
									SET_BIT(serverBD.gangMember[i].iBitSet, GANG_BS_CREATED_FROM_AMBIENT)
									
									serverBD.gangMember[i].iGang = iGangVariation
									serverBD.gangMember[i].gangClass = GET_GANG_MEMBER_CLASS(iGangVariation)
									SETUP_GANG_MEMBER(i)
									
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === SET_BIT(serverBD.gangMember[", i, "].iBitSet, GANG_BS_CREATED_FROM_AMBIENT)")
									#ENDIF
									
									CLEAR_BIT(serverBD.gangMember[i].iBitSet, GANG_BS_RAW_AMBIENT)
									
									j++
								ENDIF
							ELSE
								bStillRawLeft = TRUE
							ENDIF
						ENDIF
						
					ENDIF
				ENDIF
			ENDREPEAT
			
			
			IF NOT bStillRawLeft		//final ped assigned
			OR GET_TIME_DIFFERENCE(timeNet, serverBD.timeAmbientGrab) > GANG_CALL_AMBIENT_GRAB_TIME
				IF j >= GET_NUMBER_OF_GANG_MEMBERS_IN_GANG(iGangVariation)
					SET_BIT(serverBD.iThiefBitSet, THIEF_BS_ENTIRE_GANG_CREATED_FROM_AMBIENT)
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === SET_BIT(serverBD.iThiefBitSet, THIEF_BS_ENTIRE_GANG_CREATED_FROM_AMBIENT)")
					#ENDIF
				ENDIF
				
				PED_INDEX nullPed
				
				REPEAT 16 i
					pedAmbient[i] = nullPed
				ENDREPEAT
				
				REPEAT GET_NUMBER_OF_GANG_MEMBERS_IN_GANG(iGangVariation) i
					IF IS_BIT_SET(serverBD.gangMember[i].iBitSet, GANG_BS_RAW_AMBIENT)
						CLEANUP_NET_ID(serverBD.gangMember[i].netID)
						CLEAR_BIT(serverBD.gangMember[i].iBitSet, GANG_BS_RAW_AMBIENT)
					ENDIF
				ENDREPEAT
				
				
				CLEAR_BIT(serverBD.iBitset, SERVER_BS_FOUND_AMBIENT_PEDS)
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === SET_BIT(serverBD.iThiefBitSet, THIEF_BS_ENTIRE_GANG_CREATED_FROM_AMBIENT)")
				#ENDIF
				
				RETURN TRUE
			ENDIF
			
		ELSE
			RETURN TRUE
		ENDIF
		
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns if the gang was created using peds grabbed from the ambient environment
/// RETURNS:
///    
FUNC BOOL IS_ENTIRE_GANG_CREATED_FROM_AMBIENT()
	IF iGangVariation = GANG_CALL_TYPE_SPECIAL_THIEF
		IF IS_BIT_SET(serverBD.iThiefBitSet, THIEF_BS_ENTIRE_GANG_CREATED_FROM_AMBIENT)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Attempts to get safe coordinates and heading to spawn a gang ped
/// RETURNS:
///    true if successful
FUNC BOOL FIND_GANG_SPAWN_LOCATION()

	IF IS_BIT_SET(serverBD.iBitset, SERVER_BS_SPAWN_LOCATION_FOUND)
		RETURN TRUE
	ELSE
	
		SWITCH iGangVariation
			
			CASE GANG_CALL_TYPE_SPECIAL_THIEF
			
				IF NETWORK_IS_PLAYER_ACTIVE(playerGangTarget)
				AND IS_NET_PLAYER_OK(playerGangTarget)
					
					SPAWN_SEARCH_PARAMS SpawnSearchParams
					SpawnSearchParams.vFacingCoords = GET_ENTITY_COORDS(GET_PLAYER_PED(playerGangTarget))
					SpawnSearchParams.fMinDistFromPlayer=MIN_DISTANCE_THIEF_TO_SPAWN
				
					IF GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY(	serverBD.vGangTravel, 
																	MAX_DISTANCE_THIEF_TO_SPAWN, serverBD.vGangSpawn, serverBD.fGangSpawn, 
																	SpawnSearchParams)
						SET_BIT(serverBD.iBitset, SERVER_BS_SPAWN_LOCATION_FOUND)
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === SERVER_BS_SPAWN_LOCATION_FOUND : ", serverBD.vGangSpawn)
						#ENDIF
						RETURN TRUE
					ENDIF
				ELSE
					NETWORK_CANCEL_RESPAWN_SEARCH()	
					SPAWNPOINTS_CANCEL_SEARCH()
					IF (g_SpawnData.bNavMeshRequested)
						REMOVE_NAVMESH_REQUIRED_REGIONS()
						g_SpawnData.bNavMeshRequested = FALSE
					ENDIF
				ENDIF
			BREAK
			
		ENDSWITCH
	
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Creates the current gang, either by spawning or grabbing ambient peds and sets them up with the appropriate properties
/// RETURNS:
///    True when complete
FUNC BOOL CREATE_GANG()
	INT i = 0

	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF IS_BIT_SET(serverBD.iBitset, SERVER_BS_GANG_CREATED)
			RETURN TRUE
		ELSE															//Time to spawn
			
			//Spawn gang members
			SWITCH iGangVariation
				CASE GANG_CALL_TYPE_SPECIAL_THIEF
					REPEAT GET_NUMBER_OF_GANG_MEMBERS_IN_GANG(iGangVariation) i
						IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.gangMember[i].netID)
						AND NOT IS_BIT_SET(serverBD.gangMember[i].iBitSet, GANG_BS_CREATED_AND_SET_UP)
						AND NOT IS_BIT_SET(serverBD.gangMember[i].iBitSet, GANG_BS_DEAD)
							CREATE_GANG_MEMBER(i, GET_GANG_MEMBER_CLASS(iGangVariation), serverBD.vGangSpawn, serverBD.fGangSpawn)
						ENDIF
					ENDREPEAT
				BREAK
			ENDSWITCH
			
			IF iGangVariation <> GANG_CALL_TYPE_SPECIAL_MERRYWEATHER
				REPEAT GET_NUMBER_OF_GANG_MEMBERS_IN_GANG(iGangVariation) i
					IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.gangMember[i].netID)
						IF NOT IS_BIT_SET(serverBD.gangMember[i].iBitSet, GANG_BS_DEAD)		//unless they're dead (from previous life)
							#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === GANG MEMBER NOT YET CREATED : ", i)
							#ENDIF
							RETURN FALSE
						ENDIF
					ENDIF
				ENDREPEAT
			ENDIF
			
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === SERVER_BS_GANG_CREATED : ", serverBD.vGangSpawn)
			#ENDIF
			
			SET_BIT(iLocalBitset, LOCAL_BS_SERVER_STAGGER_AT_LEAST_ONE_GANG_MEMBER_ALIVE)
			SET_BIT(iLocalBitset, LOCAL_BS_SERVER_STAGGER_AT_LEAST_ONE_GANG_MEMBER_NOT_ABANDONED)
			SET_BIT(iLocalBitset, LOCAL_BS_SERVER_STAGGER_SOMEONE_NEAR_A_THIEF)
			SET_BIT(iLocalBitset, LOCAL_BS_CLIENT_NEAR_A_THIEF)
			SET_BIT(serverBD.iBitset, SERVER_BS_GANG_CREATED)
			SET_BIT(g_GangCallReachedLocalPlayerBitset, iGangVariation)
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === SET_BIT(g_GangCallReachedLocalPlayerBitset, ", iGangVariation, ")")
			#ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

//PURPOSE: Runs through the max num participents and does relevant checks
PROC MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER()
	
ENDPROC

/// PURPOSE:
///    Loads and waits for models for gang, decides whether to use ambient peds or not, spawns and sets up all gang member peds
/// RETURNS:
///    
FUNC BOOL MANAGE_SPAWNING_GANG()
	IF IS_BIT_SET(ServerBD.iBitset, SERVER_BS_GANG_CREATED)
		RETURN FALSE
	ELSE
	
		BOOL bProceed = FALSE
	
		IF IS_OK_TO_SPAWN_GANG_NOW()
			IF iGangVariation = GANG_CALL_TYPE_SPECIAL_MERRYWEATHER
				bProceed = TRUE
			ELSE
				IF SETUP_GANG_MODELS()
					IF GRAB_AMBIENT_ELEMENTS()
						IF IS_ENTIRE_GANG_CREATED_FROM_AMBIENT()
							bProceed = TRUE
						ELSE
							IF REQUEST_LOAD_MODEL(GET_GANG_PED_MODEL(iGangVariation))
								IF FIND_GANG_SPAWN_LOCATION()
									bProceed = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF bProceed
			START_SERVER_TIMER()
			IF CREATE_GANG()
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === MANAGE_SPAWNING_GANG = TRUE")
				#ENDIF
				RETURN TRUE
			ENDIF
		ENDIF
		
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Maintains the amount of reservations being broadcast that this script requires
PROC SERVER_MAINTAIN_RESERVATIONS()
	IF NOT IS_BIT_SET(serverBD.iBitset, SERVER_BS_INITIAL_RESERVE_VALUES_SET)
		serverBD.iPedsToReserve = GET_NUMBER_OF_GANG_MEMBERS_IN_GANG(iGangVariation)
		serverBD.iVehiclesToReserve = 0
		serverBD.iObjectsToReserve = 0
		
		SET_BIT(serverBD.iBitset, SERVER_BS_INITIAL_RESERVE_VALUES_SET)
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Checks to see if the current script reservations are different from the server reservations, updates when they are not the same
PROC CLIENT_MAINTAIN_RESERVATIONS()
	IF GET_NUM_RESERVED_MISSION_PEDS() <> serverBD.iPedsToReserve								//IF not the same as server
		IF CAN_RESERVE_NETWORK_PEDS_FOR_THIS_SCRIPT(serverBD.iPedsToReserve, FALSE, TRUE)		//If able to reserve this many
			IF GET_NUM_CREATED_MISSION_PEDS() <= serverBD.iPedsToReserve						//If I have currently created less or same as server reserves
				RESERVE_NETWORK_MISSION_PEDS(serverBD.iPedsToReserve)
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === RESERVE_NETWORK_MISSION_PEDS(", serverBD.iPedsToReserve, ")")
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
	IF GET_NUM_RESERVED_MISSION_VEHICLES() <> serverBD.iVehiclesToReserve
		IF CAN_RESERVE_NETWORK_VEHICLES_FOR_THIS_SCRIPT(serverBD.iPedsToReserve, FALSE, TRUE)
			IF GET_NUM_CREATED_MISSION_VEHICLES() <= serverBD.iVehiclesToReserve
				RESERVE_NETWORK_MISSION_VEHICLES(serverBD.iVehiclesToReserve)
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === RESERVE_NETWORK_MISSION_VEHICLES(", serverBD.iVehiclesToReserve, ")")
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
	IF GET_NUM_RESERVED_MISSION_OBJECTS() <> serverBD.iObjectsToReserve
		IF CAN_RESERVE_NETWORK_OBJECTS_FOR_THIS_SCRIPT(serverBD.iPedsToReserve, FALSE, TRUE)
			IF GET_NUM_CREATED_MISSION_OBJECTS() <= serverBD.iObjectsToReserve
				RESERVE_NETWORK_MISSION_OBJECTS(serverBD.iObjectsToReserve)
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === RESERVE_NETWORK_MISSION_OBJECTS(", serverBD.iObjectsToReserve, ")")
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///       Checks if any of the mission cleanup conditions have been met then puts the script into an end 
///       state so that it will eventually cleanup. GET_SERVER_MISSION_STATE() set to GAME_STATE_END
proc mission_end_system()
	
	IF GET_SERVER_MISSION_STATE() < GAME_STATE_END
		IF HAVE_MISSION_END_CONDITIONS_BEEN_MET()
		
			int i 
			
			iServerGangStaggeredCounter = 0
			
			for i = 0 to (GET_NUMBER_OF_GANG_MEMBERS_IN_GANG(iGangVariation) - 1)
				SERVER_STAGGERED_GANG_LOOP()
			endfor 

			serverBD.iServerGameState = GAME_STATE_END
			
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === serverBD.iServerGameState = GAME_STATE_END 4")
			#ENDIF
			
		ENDIF
	ENDIF

endproc 

/// PURPOSE:
///    Cleans up then terminates the mission
proc gang_call_game_state_end()
	INT iXpAmount

	SET_BIT(iLocalBitset, LOCAL_BS_CLIENT_SHOULD_PROGRESS_TO_CLEANUP)

	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === SCRIPT CLEANUP")
	#ENDIF
	
	IF IS_BIT_SET(serverBD.iFailBitset, FAIL_BS_UNABLE_TO_FIND_SPAWN_LOCATION)
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === FAIL_BS_UNABLE_TO_FIND_SPAWN_LOCATION")
		#ENDIF
	ENDIF
	IF IS_BIT_SET(serverBD.iFailBitset, FAIL_BS_THIEF_ESCAPE)
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === FAIL_BS_THIEF_ESCAPE")
		#ENDIF
	ENDIF
	IF IS_BIT_SET(serverBD.iFailBitset, FAIL_BS_SUCCESSFUL_THIEF_DEAD)
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === FAIL_BS_SUCCESSFUL_THIEF_DEAD")
		#ENDIF
	ENDIF
	IF IS_BIT_SET(serverBD.iFailBitset, FAIL_BS_THEFT_NO_MONEY)
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === FAIL_BS_THEFT_NO_MONEY")
		#ENDIF
	ENDIF
	IF IS_BIT_SET(serverBD.iFailBitset, FAIL_BS_TARGET_PLAYER_INVALID)
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === FAIL_BS_TARGET_PLAYER_INVALID")
		#ENDIF
	ENDIF
	IF IS_BIT_SET(serverBD.iFailBitset, FAIL_BS_TARGET_NAME_CHANGED)
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === FAIL_BS_TARGET_NAME_CHANGED")
		#ENDIF
	ENDIF
	
	IF IS_BIT_SET(serverBD.iFailBitset, FAIL_BS_ARMY_TARGET_SURVIVE)
		IF PLAYER_ID() = playerGangOwner
			CLEAR_BIT(iLocalBitset, LOCAL_BS_CLIENT_SHOULD_PROGRESS_TO_CLEANUP)
			IF serverBD.bIsHitSquad
			OR serverBD.bIsAssassins
				SET_BIT(iLocalBitset, LOCAL_BS_CLIENT_SHOULD_PROGRESS_TO_CLEANUP)
			ELSE
				IF DO_GANG_CALL_PHONECALL("MPCT_MERts", "FM_MERRYWEATHER", CHAR_MP_MERRYWEATHER)
					SET_BIT(iLocalBitset, LOCAL_BS_CLIENT_SHOULD_PROGRESS_TO_CLEANUP)
					//script_assert("MPCT_MERts - player survived NO REFUND")
				ENDIF
			ENDIF
		ENDIF
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === FAIL_BS_ARMY_TARGET_SURVIVE")
		#ENDIF
	ENDIF
	
	IF IS_BIT_SET(serverBD.iFailBitset, FAIL_BS_ARMY_TARGET_DEAD)
		IF PLAYER_ID() = playerGangOwner
			CLEAR_BIT(iLocalBitset, LOCAL_BS_CLIENT_SHOULD_PROGRESS_TO_CLEANUP)
			IF serverBD.bIsHitSquad
			OR serverBD.bIsAssassins
				SET_BIT(iLocalBitset, LOCAL_BS_CLIENT_SHOULD_PROGRESS_TO_CLEANUP)
			ELSE
				IF DO_GANG_CALL_PHONECALL("MPCT_MERtd", "FM_MERRYWEATHER", CHAR_MP_MERRYWEATHER)
					SET_BIT(iLocalBitset, LOCAL_BS_CLIENT_SHOULD_PROGRESS_TO_CLEANUP)
				ENDIF
			ENDIF
		ENDIF
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === FAIL_BS_ARMY_TARGET_DEAD")
		#ENDIF
	ENDIF
	
	IF IS_BIT_SET(serverBD.iFailBitset, FAIL_BS_ARMY_TARGET_ESCAPE)
	
		IF PLAYER_ID() = playerGangOwner
			CLEAR_BIT(iLocalBitset, LOCAL_BS_CLIENT_SHOULD_PROGRESS_TO_CLEANUP)
			IF DO_REFUND_PHONECALL()
				IF NOT serverBD.bIsHitSquad
				
					#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === REFUNDING PLAYER $", GET_CONTACT_REQUEST_COST(REQUEST_MERCENARIES))
					#ENDIF
					IF serverBD.bIsAssassins
						iRefundCost = GET_ASSASINS_ABILITY_ATTACK_LEVEL_COST(serverBD.eAssassinLevel)
					ELSE
						iRefundCost = GET_CONTACT_REQUEST_COST(REQUEST_MERCENARIES)
					ENDIF
					
					IF iRefundCost > 0
						GIVE_LOCAL_PLAYER_FM_CASH(iRefundCost, 1, FALSE, 0)
						NETWORK_REFUND_CASH_TYPE(iRefundCost, MP_REFUND_TYPE_HIRE_MERCENARY, MP_REFUND_REASON_TARGET_ESCAPE, TRUE)
					ENDIF
					SET_BIT(iLocalBitset, LOCAL_BS_CLIENT_SHOULD_PROGRESS_TO_CLEANUP)
					printstring("refund phone call test 0")
					printnl()
					//script_assert("MPCT_MERloc - REFUND MONEY")
					
				ELSE
					SET_BIT(iLocalBitset, LOCAL_BS_CLIENT_SHOULD_PROGRESS_TO_CLEANUP)
					CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === Biker hit squad running. No refund.")
				ENDIF
			ENDIF
		ENDIF
	
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === FAIL_BS_ARMY_TARGET_ESCAPE")
		#ENDIF
	ENDIF
	
	IF IS_BIT_SET(serverBD.iFailBitset, FAIL_BS_GANG_DEAD)
	
		IF PLAYER_ID() = playerGangOwner
			IF NOT IS_BIT_SET(serverBD.iThiefBitset, THIEF_BS_SOMEONE_ROBBED)
				CLEAR_BIT(iLocalBitset, LOCAL_BS_CLIENT_SHOULD_PROGRESS_TO_CLEANUP)
				IF DO_GANG_CALL_PHONECALL("MPCT_mugfail", "LAMAR", CHAR_LAMAR)
					SET_BIT(iLocalBitset, LOCAL_BS_CLIENT_SHOULD_PROGRESS_TO_CLEANUP)
				ENDIF
			ENDIF
		ELIF PLAYER_ID() = playerGangTarget
			IF IS_BIT_SET(serverBD.iThiefBitset, THIEF_BS_SOMEONE_ROBBED)
				IF IS_BIT_SET(serverBD.iThiefBitset, THIEF_BS_TARGET_HAS_KILLED)
					iXpAmount = ROUND(g_sMPTunables.iKILL_A_MUGGER_RP * g_sMPTunables.fxp_tunable_Kill_Muggers)
					GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "XPT_MEDIUMT", XPTYPE_ACTION, XPCATEGORY_ACTION_STOPPED_MUGGER, iXpAmount, 1)
					AWARD_ACHIEVEMENT(ACH47) // Full Refund
				ENDIF
			endif 
		ENDIF
	
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === FAIL_BS_GANG_DEAD")
		#ENDIF
	ENDIF
	
	IF IS_BIT_SET(serverBD.iFailBitset, FAIL_BS_THIEF_ABANDON)
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === FAIL_BS_THIEF_ABANDON")
		#ENDIF
		
		IF PLAYER_ID() = playerGangOwner
			CLEAR_BIT(iLocalBitset, LOCAL_BS_CLIENT_SHOULD_PROGRESS_TO_CLEANUP)
			IF DO_REFUND_PHONECALL()
				MP_REFUND_TYPE refundType
				
				IF iGangVariation = GANG_CALL_TYPE_SPECIAL_THIEF
					iRefundCost = GET_CONTACT_REQUEST_COST(REQUEST_THIEF1) * GET_NUMBER_OF_GANG_MEMBERS_IN_GANG(iGangVariation)
					refundType = MP_REFUND_TYPE_HIRE_MUGGER
				ELSE
					IF serverBD.bIsAssassins
						iRefundCost = GET_ASSASINS_ABILITY_ATTACK_LEVEL_COST(serverBD.eAssassinLevel)
					ELSE
						iRefundCost = GET_CONTACT_REQUEST_COST(REQUEST_MERCENARIES)
					ENDIF
					refundType = MP_REFUND_TYPE_HIRE_MERCENARY
				ENDIF
				
				IF NOT serverBD.bIsHitSquad
				
					#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === REFUNDING PLAYER $", iRefundCost)
					#ENDIF
					IF iRefundCost > 0
						GIVE_LOCAL_PLAYER_FM_CASH(iRefundCost, 1, FALSE, 0)
						NETWORK_REFUND_CASH_TYPE(iRefundCost, refundType, MP_REFUND_REASON_TARGET_ESCAPE)
					ENDIF
					SET_BIT(iLocalBitset, LOCAL_BS_CLIENT_SHOULD_PROGRESS_TO_CLEANUP)
					printstring("refund phone call test 1")
					printnl()
				
				ELSE
					SET_BIT(iLocalBitset, LOCAL_BS_CLIENT_SHOULD_PROGRESS_TO_CLEANUP)
					CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === Biker hit squad running. No refund.")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(serverBD.iFailBitset, FAIL_BS_REFUND)
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === FAIL_BS_REFUND")
		#ENDIF
		
		IF PLAYER_ID() = playerGangOwner
			CLEAR_BIT(iLocalBitset, LOCAL_BS_CLIENT_SHOULD_PROGRESS_TO_CLEANUP)
			IF DO_REFUND_PHONECALL()
			
				MP_REFUND_TYPE refundType
			
				IF iGangVariation = GANG_CALL_TYPE_SPECIAL_THIEF
					iRefundCost = GET_CONTACT_REQUEST_COST(REQUEST_THIEF1) * GET_NUMBER_OF_GANG_MEMBERS_IN_GANG(iGangVariation)
					refundType = MP_REFUND_TYPE_HIRE_MUGGER
				ELSE
					IF serverBD.bIsAssassins
						iRefundCost = GET_ASSASINS_ABILITY_ATTACK_LEVEL_COST(serverBD.eAssassinLevel)
					ELSE
						iRefundCost = GET_CONTACT_REQUEST_COST(REQUEST_MERCENARIES)
					ENDIF
					refundType = MP_REFUND_TYPE_HIRE_MERCENARY
				ENDIF
				
				IF NOT serverBD.bIsHitSquad
				
					#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === REFUNDING PLAYER $", iRefundCost)
					#ENDIF
					IF iRefundCost > 0
						GIVE_LOCAL_PLAYER_FM_CASH(iRefundCost, 1, FALSE, 0)
						NETWORK_REFUND_CASH_TYPE(iRefundCost, refundType, MP_REFUND_REASON_TARGET_ESCAPE)
					ENDIF
					SET_BIT(iLocalBitset, LOCAL_BS_CLIENT_SHOULD_PROGRESS_TO_CLEANUP)
					printstring("refund phone call test 2")
					printnl()
					
				ELSE
					SET_BIT(iLocalBitset, LOCAL_BS_CLIENT_SHOULD_PROGRESS_TO_CLEANUP)
					CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === Biker hit squad running. No refund.")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	
	//Do cleanup when we're done
	IF IS_BIT_SET(iLocalBitset, LOCAL_BS_CLIENT_SHOULD_PROGRESS_TO_CLEANUP)
		mission_cleanup()
	ENDIF
	
endproc 

/// PURPOSE:
///    Manages reaction flags
proc client_process_event_flags_system()
	
	if player_id() = playerGangTarget
	
		if g_bPlayerHasActiveProstitute
			
			IF NOT IS_BIT_SET(playerBD[iServerPlayerStaggeredCounter].iBitset, CLIENT_BS_PROSTITUTE_ACTIVE)
				SET_BIT(playerBD[iServerPlayerStaggeredCounter].iBitset, CLIENT_BS_PROSTITUTE_ACTIVE)
			endif
			
		else 
		
			clear_bit(playerBD[iServerPlayerStaggeredCounter].iBitset, CLIENT_BS_PROSTITUTE_ACTIVE)
			
		endif 

	endif 
	
endproc  

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                      Main Loop                      //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
SCRIPT(MP_MISSION_DATA missionScriptArgs)
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === STARTRING SCRIPT")
		#ENDIF
		
		// initialise the mission here.
		PROCESS_PRE_GAME(missionScriptArgs)
		
		// Setup widgets. Make sure this is done after PROCESS_PRE_GAME()
		#IF IS_DEBUG_BUILD
			CREATE_WIDGETS()
		#ENDIF
	ENDIF
	
	// Main loop
	WHILE TRUE

		MP_LOOP_WAIT_ZERO() 
		
		// If we have a match end event, bail
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE")
			#ENDIF
			
			SCRIPT_TERMINATE()
			
		ENDIF
			
		timeNet = GET_NETWORK_TIME()
		
		// Deal with the debug
		#IF IS_DEBUG_BUILD
			UPDATE_WIDGETS()
		#ENDIF

		//********** CLIENT **********
		// Process the game logic..... 
		
		CLIENT_MAINTAIN_RESERVATIONS()
		
		CONTROL_LOCAL_TEXT()
		
		CLIENT_MANAGE_GANG_CALL_MERC_PLAYER_RELATIONSHIPS()
		
		client_process_event_flags_system()
		
		SWITCH GET_CLIENT_MISSION_STATE(NETWORK_PLAYER_ID_TO_INT())
			
			// Wait until the server gives the all go before moving on
			CASE GAME_STATE_INI
			
				//g_bPlayerHasActiveProstitute
				
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					playerBD[NETWORK_PLAYER_ID_TO_INT()].iGameState = GAME_STATE_RUNNING
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === playerBD[NETWORK_PLAYER_ID_TO_INT()].iGameState = GAME_STATE_RUNNING")
					#ENDIF

				// Look for the server say the mission has ended
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					playerBD[NETWORK_PLAYER_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === playerBD[NETWORK_PLAYER_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY 1")
					#ENDIF
				ENDIF
				
			BREAK
			
			// Main gameplay state.
			CASE GAME_STATE_RUNNING

				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					
					CLIENT_PROCESSING()
				
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_INI
					
					SCRIPT_TIDY_UP()
				
					playerBD[NETWORK_PLAYER_ID_TO_INT()].iGameState = GAME_STATE_INI
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === playerBD[NETWORK_PLAYER_ID_TO_INT()].iGameState = GAME_STATE_INI")
					#ENDIF
				
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					
					playerBD[NETWORK_PLAYER_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === playerBD[NETWORK_PLAYER_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY 2")
					#ENDIF
					
				ENDIF
			BREAK
			
			CASE GAME_STATE_TERMINATE_DELAY
			
				CLIENT_STAGGERED_GANG_LOOP()
			
				SET_MISSION_FINISHED(serverBD.MissionTerminateDelayTimer)
		      	IF IS_MISSION_READY_TO_CLEANUP(serverBD.MissionTerminateDelayTimer)
				
					INT i
					REPEAT GET_NUMBER_OF_GANG_MEMBERS_IN_GANG(iGangVariation) i
						CLIENT_STAGGERED_GANG_LOOP()
					ENDREPEAT
					
		            playerBD[NETWORK_PLAYER_ID_TO_INT()].iGameState = GAME_STATE_END
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === playerBD[NETWORK_PLAYER_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY 3")
					#ENDIF
				ENDIF
			BREAK
			
			// The game stage the local player is placed when we want him to leave a mission
			CASE GAME_STATE_LEAVE
				
				playerBD[NETWORK_PLAYER_ID_TO_INT()].iGameState = GAME_STATE_END
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_NET_AMBIENT, "=== ", DEBUG_GANG_NAME(), " === playerBD[NETWORK_PLAYER_ID_TO_INT()].iGameState = GAME_STATE_END 2")
				#ENDIF
			FALLTHRU
			
			
			CASE GAME_STATE_END

				gang_call_game_state_end()

			BREAK

		ENDSWITCH
		
		// -----------------------------------
		// Process SERVER game logic	
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		
			SERVER_MAINTAIN_UNIVERSAL_TIMRS()
		
			SERVER_MAINTAIN_RESERVATIONS()
			
			SERVER_MANAGE_GANG_CALL_MERC_PLAYER_RELATIONSHIPS()
			
			SWITCH GET_SERVER_MISSION_STATE()
				
				CASE GAME_STATE_INI
					
					START_REFUND_TIMER()	//will only start once, never reset it
				
					IF IS_OK_TO_SPAWN_GANG_NOW()
						IF NOT IS_ENTITY_DEAD(GET_PLAYER_PED(playerGangTarget))
							GRAB_LATEST_TRAVEL_TO_COORDS(GET_PLAYER_PED(playerGangTarget))
						ELSE
							serverBD.vGangTravel = vGangInitialCoords
						ENDIF
						
						CLEAR_BIT(serverBD.iThiefBitSet, THIEF_BS_TARGET_ESCAPED)

						serverBD.iServerGameState = GAME_STATE_RUNNING
					ENDIF
				BREAK
				
				// Look for game end conditions
				CASE GAME_STATE_RUNNING
					
					IF MANAGE_SPAWNING_GANG()
						RESET_SERVER_TIMER()
						START_SERVER_TIMER()
					ENDIF
					
					SERVER_PROCESSING()
					
				BREAK
				
				CASE GAME_STATE_END

				BREAK
				
			ENDSWITCH
			
			mission_end_system()
						
		ENDIF
	ENDWHILE
	
ENDSCRIPT


//1318917 - parachute jump out standing still bug
//1601568 - can't quit out of merryweather menu.
