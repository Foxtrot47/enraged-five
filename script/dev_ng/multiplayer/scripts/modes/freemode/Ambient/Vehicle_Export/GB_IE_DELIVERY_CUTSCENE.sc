//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        																							//
// Description: 																							//
// Written by:  Tymon																						//
// Date:  		14/06/2016																					//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"

USING "commands_network.sch"
USING "script_player.sch"
USING "commands_path.sch"
USING "commands_event.sch"

USING "net_include.sch"
USING "net_events.sch"
USING "net_spawn.sch"
USING "net_ambience.sch"

USING "net_wait_zero.sch"
USING "freemode_events_header.sch"

#IF IS_DEBUG_BUILD
USING "net_debug_log.sch"
#ENDIF


USING "ie_delivery_cutscene_data.sch"
USING "net_simple_cutscene.sch"
USING "net_simple_cutscene_interior.sch"
//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════════╡ VARIABLES  ╞══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

ENUM IE_DELIVERY_VEHICLE_DESTRUCTION_STAGE
	IE_DELIVERY_DESTRUCTION_IDLE,
	IE_DELIVERY_DESTRUCTION_START,
	IE_DELIVERY_DESTRUCTION_WAITING,
	IE_DELIVERY_DESTRUCTION_DESTROYING,
	IE_DELIVERY_DESTRUCTION_DONE
ENDENUM

ENUM IE_DELIVERY_SCRIPT_STATE
	IE_DELIVERY_SCRIPT_STATE_LOADING,
	IE_DELIVERY_SCRIPT_STATE_IDLE,
	IE_DELIVERY_SCRIPT_STATE_FADE_OUT_FOR_START,
	IE_DELIVERY_SCRIPT_STATE_PLAYING_CUTSCENE,
	IE_DELIVERY_SCRIPT_STATE_VEHICLE_CLEANUP,
	IE_DELIVERY_SCRIPT_STATE_WARPING
ENDENUM

CONST_INT BS_SERVER_BD_IE_DELIVERY_STARTED_CAR_FADE_OUT						0
CONST_INT BS_SERVER_BD_IE_DELIVERY_MOVED_CAR_OUT_OF_THE_WAY					1
//CONST_INT BS_SERVER_BD_IE_DELIVERY_STARTED_ATTACH_VEH_FADE_OUT			2
//CONST_INT BS_SERVER_BD_IE_DELIVERY_MOVED_ATTACH_VEH_OUT_OF_THE_WAY		3
CONST_INT BS_SERVER_BD_IE_DELIVERY_SENT_VEH_CLEANUP_BROADCAST				4
CONST_INT BS_SERVER_BD_IE_DELIVERY_SENT_DELIVERED_BROADCAST					5
CONST_INT BS_SERVER_BD_IE_DELIVERY_SET_DELIVERED_FLAG_ON_EXPORT_VEHICLE		6

STRUCT ServerBroadcastData
	INT iBS
	IE_DELIVERY_VEHICLE_DESTRUCTION_STAGE eVehDestructionStage
ENDSTRUCT
ServerBroadcastData serverBD

CONST_INT BS_PLAYER_BD_IE_DELIVERY_STARTED_CUTSCENE			0
CONST_INT BS_PLAYER_BD_IE_DELIVERY_FINISHED_CUTSCENE		1
CONST_INT BS_PLAYER_BD_IE_DELIVERY_FADED_IN_PROXY_VEH		2
CONST_INT BS_PLAYER_BD_IE_DELIVERY_LEAVING_VEH_FOR_CUTSCENE	3
CONST_INT BS_PLAYER_BD_IE_DELIVERY_FADED_OUT_VEH_DELIVERED	4
CONST_INT BS_PLAYER_BD_IE_DELIVERY_MOVED_VEH_OUT_OF_THE_WAY	5
CONST_INT BS_PLAYER_BD_IE_DELIVERY_FADED_OUT_VEH_PROXY		6
CONST_INT BS_PLAYER_BD_IE_DELIVERY_PLAYER_MOVED_OUT			7

STRUCT PlayerBroadcastData
	INT iBS
	IE_DELIVERY_SCRIPT_STATE eState
	IE_DELIVERY_VEHICLE_DESTRUCTION_STAGE eVehDestructionStage
	VEHICLE_SEAT vehicleSeat = VS_ANY_PASSENGER
ENDSTRUCT
PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]

CONST_INT BS_IE_DELIVERY_STATIC_CRANE_HIDDEN			0 // Was crane on the map hidden
CONST_INT BS_IE_DELIVERY_AFTER_DESTRUCTION_WARP_DONE	1 // Was final warp at the end of cutscene done
CONST_INT BS_IE_DELIVERY_AFTER_FINAL_PLAYER_FADE_IN		2
CONST_INT BS_IE_DELIVERY_CREATED_SPAWN_POINTS			3
CONST_INT BS_IE_DELIVERY_REWARD_DONE					4 // Has the reward been processed
CONST_INT BS_IE_DELIVERY_TASKED_TO_LEAVE_VEH			7
CONST_INT BS_IE_DELIVERY_STARTED_WARP_TO_WAREHOUSE		8
CONST_INT BS_IE_DELIVERY_FROZEN_AFTER_LEAVING_VEH		9
CONST_INT BS_IE_DELIVERY_CREATED_CARGOBOB_SPAWN_POINTS	10
CONST_INT BS_IE_DELIVERY_CARGOBOB_WARP_DONE				11
CONST_INT BS_IE_DELIVERY_IPLS_SHOULD_BE_LOADED			12
CONST_INT BS_IE_DELIVERY_IPLS_ARE_LOADED				13
CONST_INT BS_IE_DELIVERY_STARTED_LOAD_SCENE				14
CONST_INT BS_IE_DELIVERY_THE_SCRIPT_IS_CLEANING_UP		15 // Script will cleanup as soon as IPLs are unloaded
CONST_INT BS_IE_DELIVERY_CREATED_SHOWROOM_6_PROPS		16 // Has gate covering entrance to simeons showroom been created
CONST_INT BS_IE_DELIVERY_SHARD_SHOWN					17
CONST_INT BS_IE_DELIVERY_STARTED_AUDIO_SCENE			18
CONST_INT BS_IE_DELIVERY_STARTED_PLAYER_SCENARIO		19

// Cutscene BS
CONST_INT BS_IE_DELIVERY_CUTSCENE_TASK_VEHICLE 			10 // Has a vehicle clone been tasked.
CONST_INT BS_IE_DELIVERY_CUTSCENE_REVERSE_CHECK			11 // Should the vehicles rotation be adjusted.
CONST_INT BS_IE_DELIVERY_CUTSCENE_OPEN_DOOR				12

STRUCT IE_DELIVERY_SCRIPT_DATA
	INT iBS
	
	IE_DROPOFF eDropoffID
	IE_DELIVERY_DROPOFF_CUTSCENE_TYPE dropoffType
	VECTOR vDropoffCoords
	
	STRING sDeliveryAudioStream
	VEHICLE_INDEX vehDelivered
	MODEL_NAMES modelVehDelivered
	VEHICLE_SETUP_STRUCT_MP vehicleSetup
	PLAYER_INDEX pDeliverer
	BOOL bViaCargobob
	BOOL bViaTowtruck
	MODEL_NAMES modelProxyVeh = DUMMY_MODEL_FOR_SCRIPT
	VEHICLE_INDEX vehProxy
	BOOL bWeAreOriginalOwner
	PLAYER_INDEX pOriginalOwner
	BOOL bIsUrgent
	
	//SHAPETEST_INDEX shapeTest
	//INT iShapeTestStatus
	BOOL bGotGarageBounds
	VECTOR vGaragePos, vGarageNormal
	VECTOR vGarageBBox[2]
	FLOAT fGarageBBoxWidth
	VECTOR vModelMaxForFrontTest
	
	STRING sWaypointRecording
	
	SIMPLE_CUTSCENE cutscene
	
	SCRIPT_TIMER timerAfterWarp
	SCRIPT_TIMER timerVehDeliveredCleanup
	SCRIPT_TIMER timerCutsceneDelay
	SCRIPT_TIMER timerBroadcastLeaveVehicle
	SCRIPT_TIMER timerAfterDoorClosed
	SCRIPT_TIMER timerAfterBobFadein
	SCRIPT_TIMER timerRequestServerPermissionToDelete
	
	BOOL bExecuteParallelWarp
	BOOL bDontSimulateInputGaitAfterWarp
	INT iParallelWarpLastSceneCutoffTime
	SPECIAL_INTERACTIONS eSpecialInteraction = SPECIAL_INTERACTION_SHAKE_HEAD
	
	INT iExportVehValue
ENDSTRUCT
IE_DELIVERY_SCRIPT_DATA scriptData

STRUCT IE_DELIVERY_ONGOING_PROP_TRANSITION
	IE_PROP_TRANSITION_DATA sData
	INT iStartTime
ENDSTRUCT

CONST_INT BS_IE_DELIVERY_CUTSCENE_SPREADER_LOAD_ANIM_PLAYED			0
CONST_INT BS_IE_DELIVERY_CUTSCENE_SPREADER_UNLOAD_ANIM_PLAYED		1
CONST_INT BS_IE_DELIVERY_CUTSCENE_SCREEN_FADED_OUT					2

CONST_INT IE_DELIVERY_SOUND_LOOPS_COUNT								2
CONST_INT IE_DELIVERY_SOUNDS_TO_PLAY_COUNT							2

STRUCT IE_DELIVERY_CUTSCENE_DATA
	INT iBS
	VEHICLE_INDEX vehClone
	VEHICLE_INDEX vehProxyClone
	PED_INDEX pedProxyMechanic
	PED_INDEX pedProxyActors[2]
	PED_INDEX pedPlayerActors[4]
	VEHICLE_SEAT vsPedPlayers[4]
	VEHICLE_INDEX vehActors[2]
	PED_INDEX pedActors[4]
	OBJECT_INDEX objProps[4]
	IE_DELIVERY_ONGOING_PROP_TRANSITION sPropsTransitions[4]
	INT iPropsTransitionInitBS // mark if transitions have been initialised per scene
	
	VECTOR vBasePos
	FLOAT fBaseHeading
	
	VECTOR vCachedStartPos, vCachedRot
	
	INT iSyncedSceneID
	VECTOR vSyncedScenePos
	VECTOR vSyncedSceneRot
	TEXT_LABEL_63 strSyncedSceneDict
	TEXT_LABEL_63 strSyncedScenePlayerAnim
	TEXT_LABEL_63 strSyncedScenePedAnim
	
	// For attaching veh clone to various things
	BOOL bVehCloneAttachedToProxy
	VECTOR vVehAttachedOffset
	
	INT iFrozenTimestamp
	FLOAT fFrozenSpeed
	BOOL bDontMoveClone
	
	INT iSoundsStartedBS, iSoundsFinishedBS
	INT iSoundLoopsID[IE_DELIVERY_SOUND_LOOPS_COUNT]
	IE_CUTSCENE_LOOP_SOUND_DETAILS sLoopDetails[IE_DELIVERY_SOUND_LOOPS_COUNT]
	STRING strAudioScene
	INT iSceneForAudioScene = -1 // which scene we want to start audio scene at
	IE_CUTSCENE_SOUND_TO_PLAY_DETAILS sSoundsToPlay[IE_DELIVERY_SOUNDS_TO_PLAY_COUNT]
	INT iSoundsPlayedBS
	
	FLOAT fMaintainZ // Z used when moving frozen car
	VECTOR vCargobobFinalPos // Used for delivery via cargobob, cargobob position is calculated so that the attached vehicle touches the ground at the end of cutscene
ENDSTRUCT
IE_DELIVERY_CUTSCENE_DATA cutsceneData

#IF IS_DEBUG_BUILD
STRUCT IE_DELIVERY_SCRIPT_DEBUG_DATA
	BOOL bKillScript
	BOOL bDontTriggerCutscene
	
	BOOL bDumpSpawnPoints
	BOOL bToggleSpawnPointsVisibility
	
	BOOL bEditMode
	BOOL bEditModeInitialised
	BOOL bFreezeVehClone
	BOOL bVehCloneIsFrozen
	
	BOOL bToggleAttach
	BOOL bFreezeProxyClone
	BOOL bProxyCloneIsFrozen
	
	BOOL bCreateSyncedScene
	
	VECTOR vBasePos
	FLOAT fBaseHeading
	
	VECTOR vVehClonePos, vVehClonePosOLD
	FLOAT fVehCloneHeading, fVehCloneHeadingOLD
	VECTOR vVehActorsPos[2], vVehActorsPosOLD[2]
	VECTOR vObjPropsPos[4], vObjPropsPosOLD[4]
	FLOAT fVehActorsHeading[2], fVehActorsHeadingOLD[2]
	FLOAT fObjProposHeading[4], fObjProposHeadingOLD[4]
	VECTOR vPedActorsPos[4], vPedActorsPosOLD[4]
	FLOAT fPedActorsHeading[4], fPedActorsHeadingOLD[4]
	VECTOR vProxyPos, vProxyPosOLD
	FLOAT fProxyHeading, fProxyHeadingOLD
	VECTOR vSyncedScenePosOLD, vSyncedSceneRotOLD
	FLOAT fSyncedScenePhaseOLD
	VECTOR vMechanicPos, vMechanicPosOLD
	FLOAT fMechanicHeading, fMechanicHeadingOLD
	
	BOOL bToggleIPL
	BOOL bDontSendCleanupBroadcast
	BOOL bSetTruckToLeaveCoords
	//BOOL bShowPedWalkAwayCoords
	
	BOOL bDrawGarageBounds = FALSE
ENDSTRUCT
IE_DELIVERY_SCRIPT_DEBUG_DATA debugData
#ENDIF

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ PROCEDURES  ╞═══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

#IF IS_DEBUG_BUILD
FUNC STRING IE_DELIVERY_GET_SCRIPT_STATE_NAME_FOR_DEBUG(IE_DELIVERY_SCRIPT_STATE eState)
	SWITCH eState
		CASE IE_DELIVERY_SCRIPT_STATE_LOADING RETURN "IE_DELIVERY_SCRIPT_STATE_LOADING" BREAK
		CASE IE_DELIVERY_SCRIPT_STATE_IDLE RETURN "IE_DELIVERY_SCRIPT_STATE_IDLE" BREAK
		CASE IE_DELIVERY_SCRIPT_STATE_FADE_OUT_FOR_START RETURN "IE_DELIVERY_SCRIPT_STATE_FADE_OUT_FOR_START"
		CASE IE_DELIVERY_SCRIPT_STATE_PLAYING_CUTSCENE RETURN "IE_DELIVERY_SCRIPT_STATE_PLAYING_CUTSCENE" BREAK
		CASE IE_DELIVERY_SCRIPT_STATE_VEHICLE_CLEANUP RETURN "IE_DELIVERY_SCRIPT_STATE_VEHICLE_CLEANUP" BREAK
		CASE IE_DELIVERY_SCRIPT_STATE_WARPING RETURN "IE_DELIVERY_SCRIPT_STATE_WARPING" BREAK
	ENDSWITCH
	
	RETURN "*** INVALID STATE ***"
ENDFUNC

FUNC STRING _GET_VEHICLE_SEAT_FOR_DEBUG(VEHICLE_SEAT vs)
	SWITCH vs
		CASE VS_DRIVER RETURN "VS_DRIVER"
		CASE VS_FRONT_RIGHT RETURN "VS_FRONT_RIGHT"
		CASE VS_BACK_LEFT RETURN "VS_BACK_LEFT"
		CASE VS_BACK_RIGHT RETURN "VS_BACK_RIGHT"
	ENDSWITCH
	
	RETURN GET_STRING_FROM_INT(ENUM_TO_INT(vs))
ENDFUNC
#ENDIF

FUNC BOOL IS_IE_DELIVERY_SCRIPT_STATE(IE_DELIVERY_SCRIPT_STATE eState)
	RETURN playerBD[PARTICIPANT_ID_TO_INT()].eState = eState
ENDFUNC

PROC SET_IE_DELIVERY_SCRIPT_STATE(IE_DELIVERY_SCRIPT_STATE eState)
	IF playerBD[PARTICIPANT_ID_TO_INT()].eState != eState
		playerBD[PARTICIPANT_ID_TO_INT()].eState = eState
		#IF IS_DEBUG_BUILD
			PRINTLN("[IE_DELIVERY][CUTSCENE] SET_IE_DELIVERY_SCRIPT_STATE - New state: ", IE_DELIVERY_GET_SCRIPT_STATE_NAME_FOR_DEBUG(eState))
		#ENDIF
	ENDIF
ENDPROC

/*
PROC SET_STATIC_CRANE_VISIBILITY(BOOL bVisible)
	IF bVisible
		IF IS_BIT_SET(scriptData.iBS, BS_IE_DELIVERY_STATIC_CRANE_HIDDEN)
			REMOVE_MODEL_HIDE(<<889.5765, -2928.2358, 29.0808>>, 20.0, PROP_DOCK_CRANE_01)
			REMOVE_MODEL_HIDE(<<889.5765, -2928.2358, 29.0808>>, 20.0,  INT_TO_ENUM(MODEL_NAMES, HASH("po1_06_props_combo_04_lod")))
			CLEAR_BIT(scriptData.iBS, BS_IE_DELIVERY_STATIC_CRANE_HIDDEN)
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(scriptData.iBS, BS_IE_DELIVERY_STATIC_CRANE_HIDDEN)
			CREATE_MODEL_HIDE(<<889.5765, -2928.2358, 29.0808>>, 20.0, PROP_DOCK_CRANE_01, FALSE)
			CREATE_MODEL_HIDE(<<889.5765, -2928.2358, 29.0808>>, 20.0,  INT_TO_ENUM(MODEL_NAMES, HASH("po1_06_props_combo_04_lod")), FALSE)
			SET_BIT(scriptData.iBS, BS_IE_DELIVERY_STATIC_CRANE_HIDDEN)
		ENDIF
	ENDIF
ENDPROC
*/

FUNC VECTOR TRANSFORM_CUTSCENE_COORDS_TO_WORLD_COORDS(VECTOR vCoords)
	RETURN GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(cutsceneData.vBasePos, cutsceneData.fBaseHeading, vCoords)
ENDFUNC

FUNC FLOAT TRANSFORM_CUTSCENE_HEADING_TO_WORLD_HEADING(FLOAT fHeading)
	FLOAT fResult = fHeading + cutsceneData.fBaseHeading
	
	WHILE fResult < 0.0
		fResult += 360.0
	ENDWHILE
	WHILE fResult >= 360.0
		fResult -= 360.0
	ENDWHILE
	
	RETURN fResult
ENDFUNC

FUNC VECTOR TRANSFORM_WORLD_COORDS_TO_CUTSCENE_COORDS(VECTOR vCoords)
	VECTOR vResult = vCoords - cutsceneData.vBasePos
	RotateVec(vResult, <<0.0, 0.0, cutsceneData.fBaseHeading * -1.0 >>)
	RETURN vResult
ENDFUNC

FUNC FLOAT TRANSFORM_WORLD_HEADING_TO_CUTSCENE_HEADING(FLOAT fHeading)
	FLOAT fDiff = fHeading - cutsceneData.fBaseHeading
	
	WHILE fDiff < 0.0
		fDiff += 360.0
	ENDWHILE
	WHILE fDiff >= 360.0
		fDiff -= 360.0
	ENDWHILE
	
	RETURN fDiff
ENDFUNC

FUNC VECTOR GET_CARGOBOB_SPAWN_POINT_AFTER_IE_CUTSCENE(INT iPoint)
	
	FLOAT fSpawnHeight = 30.0
	
	IF iPoint = 0
		// Point 0 is the one directly above the landing pad, most desired position for spawn
		RETURN scriptData.vDropoffCoords + <<0.0, 0.0, fSpawnHeight>>
	ENDIF
	
	// Otherwise... Each next point is located on a circle in 90 degrees intervals.
	// When one circle is complete (4 points) we move onto the next circle with higher radius and
	// again place points in 90 degrees intervals but to make them not align with
	// previous points we offset each angle by 45 degrees.
	
	iPoint = iPoint - 1
	
	INT iLengthIndex = FLOOR(TO_FLOAT(iPoint) * 0.25) + 1
	FLOAT fLengthModifier = 1.0
	
	SWITCH iLengthIndex
		CASE 1 fLengthModifier = 1.2 BREAK // first circle
		CASE 2 fLengthModifier = 1.0 BREAK // second etc.
		CASE 3 fLengthModifier = 0.85 BREAK
		CASE 4 fLengthModifier = 0.8 BREAK
	ENDSWITCH
	
	FLOAT fLength = TO_FLOAT(iLengthIndex) * 23.0 * fLengthModifier
	INT iRot = iPoint % 4
	BOOL bOffsetRotation = FLOOR(TO_FLOAT(iPoint) * 0.25) % 2 = 1
	
	VECTOR vDirection = <<1.0, 0.0, 0.0>> * fLength
	
	IF bOffsetRotation
		vDirection = ROTATE_VECTOR_ABOUT_Z(vDirection, 45)
	ENDIF
	
	SWITCH iRot
		CASE 1
			vDirection = ROTATE_VECTOR_ABOUT_Z_ORTHO(vDirection, ROTSTEP_90)
		BREAK
		CASE 2
			vDirection = ROTATE_VECTOR_ABOUT_Z_ORTHO(vDirection, ROTSTEP_180)
		BREAK
		CASE 3
			vDirection = ROTATE_VECTOR_ABOUT_Z_ORTHO(vDirection, ROTSTEP_270)
		BREAK
	ENDSWITCH
	
	VECTOR vFinalPoint = scriptData.vDropoffCoords + <<0.0, 0.0, fSpawnHeight>> + vDirection
	
	#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_SPAWN_ACTIVITIES, "[IE_DELIVERY][CUTSCENE] GET_CARGOBOB_SPAWN_POINT_AFTER_IE_CUTSCENE - Computed point ", iPoint, " - it's ", vFinalPoint)
	#ENDIF
	
	RETURN vFinalPoint
ENDFUNC

PROC DISABLE_COLLISION_OF_ENTITY_WITH_NEARBY_NETWORKED_VEHS(VEHICLE_INDEX vehMain, PED_INDEX nearbyPed)
	#IF IS_DEBUG_BUILD
		PRINTLN(DEBUG_SAFEHOUSE, "[IE_DELIVERY][CUTSCENE] DISABLE_COLLISION_OF_ENTITY_WITH_NEARBY_NETWORKED_VEHS - Calling this...")
	#ENDIF
	
	IF DOES_ENTITY_EXIST(nearbyPed) AND NOT IS_ENTITY_DEAD(nearbyPed)
		IF DOES_ENTITY_EXIST(vehMain) AND NOT IS_ENTITY_DEAD(vehMain)
		
			#IF IS_DEBUG_BUILD
				PRINTLN(DEBUG_SAFEHOUSE, "[IE_DELIVERY][CUTSCENE] DISABLE_COLLISION_OF_ENTITY_WITH_NEARBY_NETWORKED_VEHS - veh main is ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(vehMain)))
			#ENDIF
		
			// Disable collision with nearby vehicles
			VEHICLE_INDEX nearbyVehs[10]
			INT iNearbyVehsCount = GET_PED_NEARBY_VEHICLES(nearbyPed, nearbyVehs)
			INT i
			
			REPEAT iNearbyVehsCount i
				IF NOT IS_ENTITY_DEAD(nearbyVehs[i])
				AND NETWORK_GET_ENTITY_IS_NETWORKED(nearbyVehs[i])
					#IF IS_DEBUG_BUILD
						PRINTLN(DEBUG_SAFEHOUSE, "[IE_DELIVERY][CUTSCENE] DISABLE_COLLISION_OF_ENTITY_WITH_NEARBY_NETWORKED_VEHS - Disabling collision with ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(nearbyVehs[i])))
					#ENDIF
					SET_ENTITY_NO_COLLISION_ENTITY(vehMain, nearbyVehs[i], FALSE)
				ENDIF
			ENDREPEAT
		ELSE
			#IF IS_DEBUG_BUILD
				PRINTLN(DEBUG_SAFEHOUSE, "[IE_DELIVERY][CUTSCENE] DISABLE_COLLISION_OF_ENTITY_WITH_NEARBY_NETWORKED_VEHS - vehMain is dead.")
			#ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
			PRINTLN(DEBUG_SAFEHOUSE, "[IE_DELIVERY][CUTSCENE] DISABLE_COLLISION_OF_ENTITY_WITH_NEARBY_NETWORKED_VEHS - nearbyPed is dead.")
		#ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Toggles the engine of both clone and the palyer car, so that sounds are togled approprietly
PROC VEHICLE_TOGGLE_ENGINE(BOOL bToggleOn = TRUE, BOOL bNoDelay = TRUE)
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
	AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()) <> NULL
			SET_VEHICLE_ENGINE_ON(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), bToggleOn, bNoDelay)
		ENDIF
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(cutsceneData.vehClone)
		SET_VEHICLE_ENGINE_ON(cutsceneData.vehClone, bToggleOn, bNoDelay)
	ENDIF
ENDPROC

PROC REMOVE_PROJECTILES_FROM_DELIVERED_VEH()
	IF NETWORK_HAS_CONTROL_OF_ENTITY(scriptData.vehDelivered)
	AND NOT IS_ENTITY_DEAD(scriptData.vehDelivered)
		//SET_ENTITY_CAN_BE_DAMAGED(scriptData.vehDelivered, FALSE)
		//SET_ENTITY_PROOFS(scriptData.vehDelivered, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE)
		CLEAR_AREA_OF_PROJECTILES(GET_ENTITY_COORDS(scriptData.vehDelivered), 8.0, TRUE)
		
		#IF IS_DEBUG_BUILD
			PRINTLN("[IE_DELIVERY][CUTSCENE] REMOVE_PROJECTILES_FROM_DELIVERED_VEH - Done.")
		#ENDIF
	ENDIF
	
	STOP_FIRE_IN_RANGE(scriptData.vDropoffCoords, 8.0)
ENDPROC

PROC IE_DELIVERY_SET_IPL_ON(BOOL bOn)
	IF scriptData.eDropoffID != IE_DROPOFF_SHOWROOM_6
		EXIT
	ENDIF
	
	IF bOn
		IF NOT IS_BIT_SET(scriptData.iBS, BS_IE_DELIVERY_IPLS_ARE_LOADED)
			IF NOT IS_BIT_SET(scriptData.iBS, BS_IE_DELIVERY_IPLS_SHOULD_BE_LOADED)
				SET_BIT(scriptData.iBS, BS_IE_DELIVERY_IPLS_SHOULD_BE_LOADED)
				
				#IF IS_DEBUG_BUILD
					PRINTLN("[IE_DELIVERY][CUTSCENE] IE_DELIVERY_SET_IPL_ON - Will load IPLs")
				#ENDIF
			ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(scriptData.iBS, BS_IE_DELIVERY_IPLS_ARE_LOADED)
			IF IS_BIT_SET(scriptData.iBS, BS_IE_DELIVERY_IPLS_SHOULD_BE_LOADED)
				CLEAR_BIT(scriptData.iBS, BS_IE_DELIVERY_IPLS_SHOULD_BE_LOADED)
				
				#IF IS_DEBUG_BUILD
					PRINTLN("[IE_DELIVERY][CUTSCENE] IE_DELIVERY_SET_IPL_ON - Will unload IPLs")
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC _CLOSE_THE_DOOR()
	IF IS_BIT_SET(cutsceneData.iBS, BS_IE_DELIVERY_CUTSCENE_OPEN_DOOR)
		// Close any gates or doors that were opened for the cutscene.
		INT iDoorHash = IE_DELIVERY_GET_DOOR_HASH(scriptData.eDropoffID)
		
		IF iDoorHash != 0
			IF IE_DELIVERY_CLOSE_DOOR(iDoorHash, 0.0, TRUE)
				CLEAR_BIT(cutsceneData.iBS, BS_IE_DELIVERY_CUTSCENE_OPEN_DOOR)
			ELSE
				#IF IS_DEBUG_BUILD
					PRINTLN("[IE_DELIVERY][CUTSCENE] _CLOSE_THE_DOOR - Can't close the door as we don't have control : (")
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL _IE_DELIVERY_LOAD_AUDIO_STREAM(STRING sStreamName)
	IF NOT IS_STRING_NULL_OR_EMPTY(sStreamName)
		#IF IS_DEBUG_BUILD
			PRINTLN("[IE_DELIVERY][CUTSCENE] _IE_DELIVERY_LOAD_AUDIO_STREAM - Called... Loaded? ", LOAD_STREAM(sStreamName))
		#ENDIF
		RETURN LOAD_STREAM(sStreamName)
	ELSE
		PRINTLN("[IE_DELIVERY][CUTSCENE] _IE_DELIVERY_LOAD_AUDIO_STREAM - Called... ")
		RETURN TRUE
	ENDIF
ENDFUNC

PROC _IE_DELIVERY_PLAY_AUDIO_STREAM(STRING sStreamName)
	IF NOT IS_STRING_NULL_OR_EMPTY(sStreamName)
		IF NOT IS_STREAM_PLAYING()
		AND LOAD_STREAM(sStreamName)
			PLAY_STREAM_FRONTEND()
			PRINTLN("[IE_DELIVERY][CUTSCENE] _IE_DELIVERY_PLAY_AUDIO_STREAM - Called...")
		ENDIF
	ENDIF
ENDPROC

PROC _IE_DELIVERY_STOP_AUDIO_STREAM(STRING sStreamName)
	IF NOT IS_STRING_NULL_OR_EMPTY(sStreamName)
		IF IS_STREAM_PLAYING()
		AND LOAD_STREAM(sStreamName)
			STOP_STREAM()
			PRINTLN("[IE_DELIVERY][CUTSCENE] _IE_DELIVERY_STOP_AUDIO_STREAM - Called...")
		ENDIF
	ENDIF
ENDPROC

PROC IE_DELIVERY_CLEANUP_EVERYTHING()
	#IF IS_DEBUG_BUILD
		PRINTLN("[IE_DELIVERY][CUTSCENE] IE_DELIVERY_CLEANUP_EVERYTHING - Calling this.")
	#ENDIF
		
	SIMPLE_CUTSCENE_REINIT(scriptData.cutscene)
	
	IF DOES_ENTITY_EXIST(cutsceneData.vehClone)
		DELETE_VEHICLE(cutsceneData.vehClone)
	ENDIF
	
	INT i
	
	REPEAT COUNT_OF(cutsceneData.pedPlayerActors) i
		IF DOES_ENTITY_EXIST(cutsceneData.pedPlayerActors[i])
			DELETE_PED(cutsceneData.pedPlayerActors[i])
		ENDIF
	ENDREPEAT
	
	REPEAT COUNT_OF(cutsceneData.pedProxyActors) i
		IF DOES_ENTITY_EXIST(cutsceneData.pedProxyActors[i])
			DELETE_PED(cutsceneData.pedProxyActors[i])
		ENDIF
	ENDREPEAT
	
	IF DOES_ENTITY_EXIST(cutsceneData.vehProxyClone)
		DELETE_VEHICLE(cutsceneData.vehProxyClone)
	ENDIF
	
	IF DOES_ENTITY_EXIST(cutsceneData.pedProxyMechanic)
		DELETE_PED(cutsceneData.pedProxyMechanic)
	ENDIF
	
	REPEAT COUNT_OF(cutsceneData.vehActors) i
		IF DOES_ENTITY_EXIST(cutsceneData.vehActors[i])
			DELETE_VEHICLE(cutsceneData.vehActors[i])
		ENDIF
	ENDREPEAT
	
	REPEAT COUNT_OF(cutsceneData.pedActors) i
		IF DOES_ENTITY_EXIST(cutsceneData.pedActors[i])
			DELETE_PED(cutsceneData.pedActors[i])
		ENDIF
	ENDREPEAT
	
	IF NOT IS_BIT_SET(scriptData.iBS, BS_IE_DELIVERY_CREATED_SHOWROOM_6_PROPS)
		REPEAT COUNT_OF(cutsceneData.objProps) i
			IF DOES_ENTITY_EXIST(cutsceneData.objProps[i])
				DELETE_OBJECT(cutsceneData.objProps[i])
			ENDIF
		ENDREPEAT
	ELSE
		MODEL_NAMES modelFoo
		VECTOR vCoords, vRot
		
		REPEAT COUNT_OF(cutsceneData.objProps) i
			// Set the props back to initial state
			IF DOES_ENTITY_EXIST(cutsceneData.objProps[i])
				IE_DELIVERY_GET_DROPOFF_PROP_DATA(scriptData.eDropoffID, i, modelFoo, vCoords, vRot, scriptData.bViaCargobob)
				IF NOT IS_ENTITY_DEAD(cutsceneData.objProps[i])
					SET_ENTITY_COORDS(cutsceneData.objProps[i], vCoords)
					SET_ENTITY_ROTATION(cutsceneData.objProps[i], vRot)
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	cutsceneData.bVehCloneAttachedToProxy = FALSE
	cutsceneData.vVehAttachedOffset = <<0.0, 0.0, 0.0>>
	
	_CLOSE_THE_DOOR()
	
	_IE_DELIVERY_STOP_AUDIO_STREAM(scriptData.sDeliveryAudioStream)
	
	IF IS_BIT_SET(cutsceneData.iBS, BS_IE_DELIVERY_CUTSCENE_TASK_VEHICLE)
		CLEAR_BIT(cutsceneData.iBS, BS_IE_DELIVERY_CUTSCENE_TASK_VEHICLE)
	ENDIF
	
	IF IS_BIT_SET(cutsceneData.iBS, BS_IE_DELIVERY_CUTSCENE_REVERSE_CHECK)
		CLEAR_BIT(cutsceneData.iBS, BS_IE_DELIVERY_CUTSCENE_REVERSE_CHECK)
	ENDIF
	
	IF scriptData.eDropoffID = IE_DROPOFF_SHOWROOM_6
		REMOVE_MODEL_HIDE(<<-29.32, -1086.63, 26.95>>, 1.0, INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("v_15_garg_delta_doordown")), FALSE)
		IE_DELIVERY_SET_IPL_ON(FALSE)
	ENDIF
ENDPROC

PROC _CLEANUP_ALL_ASSETS()
	#IF IS_DEBUG_BUILD
		PRINTLN("[IE_DELIVERY][CUTSCENE] _CLEANUP_ALL_ASSETS - Called...")
	#ENDIF
	
	SET_MODEL_AS_NO_LONGER_NEEDED(scriptData.modelVehDelivered)
	SET_MODEL_AS_NO_LONGER_NEEDED(mp_m_freemode_01)
	SET_MODEL_AS_NO_LONGER_NEEDED(mp_f_freemode_01)
	
	IF NOT IS_STRING_NULL_OR_EMPTY(scriptData.sWaypointRecording)
		REMOVE_WAYPOINT_RECORDING(scriptData.sWaypointRecording)
	ENDIF
	
	INT i
	MODEL_NAMES eModel
	VECTOR vFoo
	PED_TYPE pedType
	TEXT_LABEL_63 txtAnimDict
	
	WHILE IE_DELIVERY_GET_DROPOFF_VEHICLE_ACTOR_DATA(scriptData.eDropoffID, i, eModel, vFoo, vFoo)
		SET_MODEL_AS_NO_LONGER_NEEDED(eModel)
		i += 1
	ENDWHILE
	
	i = 0
	WHILE IE_DELIVERY_GET_DROPOFF_PROP_DATA(scriptData.eDropoffID, i, eModel, vFoo, vFoo, scriptData.bViaCargobob)
		SET_MODEL_AS_NO_LONGER_NEEDED(eModel)
		i += 1
	ENDWHILE
	
	i = 0
	txtAnimDict = ""
	WHILE IE_DELIVERY_GET_DROPOFF_PED_DATA(scriptData.eDropoffID, i, pedType, eModel, vFoo, vFoo, txtAnimDict, scriptData.bViaCargobob)
		SET_MODEL_AS_NO_LONGER_NEEDED(eModel)
		IF NOT IS_STRING_NULL_OR_EMPTY(txtAnimDict)
			REMOVE_ANIM_DICT(txtAnimDict)
		ENDIF
		i += 1
		txtAnimDict = ""
	ENDWHILE
	
	IF scriptData.modelProxyVeh != DUMMY_MODEL_FOR_SCRIPT
		SET_MODEL_AS_NO_LONGER_NEEDED(scriptData.modelProxyVeh)
	ENDIF
	
	IF scriptData.bViaCargobob
		SET_MODEL_AS_NO_LONGER_NEEDED(GET_IE_WAREHOUSE_MECHANIC_MODEL())
	ENDIF
	
	// Unique stuff
	
	IF scriptData.dropoffType = IE_DELIVERY_DROPOFF_CUTSCENE_TYPE_CRANE
		REMOVE_ANIM_DICT("map_objects")
	ENDIF
ENDPROC

PROC SCRIPT_CLEANUP(BOOL bAbortIfIPLLoaded = TRUE)
	IF bAbortIfIPLLoaded
		IF scriptData.eDropoffID = IE_DROPOFF_SHOWROOM_6
			IF NOT IS_BIT_SET(scriptData.iBS, BS_IE_DELIVERY_IPLS_SHOULD_BE_LOADED)
			AND IS_BIT_SET(scriptData.iBS, BS_IE_DELIVERY_IPLS_ARE_LOADED)
				#IF IS_DEBUG_BUILD
					PRINTLN("[IE_DELIVERY][CUTSCENE] SCRIPT_CLEANUP - Halt! Stopping the cleanup, gotta wait for IPLs to unload!")
				#ENDIF
				SET_BIT(scriptData.iBS, BS_IE_DELIVERY_THE_SCRIPT_IS_CLEANING_UP)
				EXIT
			ENDIF
		ENDIF
	ENDIF

	g_IEDeliveryData.bHaltVehicleAndLaunchCutscene = FALSE
	g_IEDeliveryData.bLaunchingScript = FALSE
	g_IEDeliveryData.eDropoffIDForLaunch = IE_DROPOFF_INVALID
	g_IEDeliveryData.bStartCutscene = FALSE
	
	IF NOT IS_LOCAL_PLAYER_WALKING_OUT_OF_SIMPLE_INTERIOR()
		g_bEndScreenSuppressFadeIn = FALSE
		#IF IS_DEBUG_BUILD
			PRINTLN("[IE_DELIVERY][CUTSCENE] Setting g_bEndScreenSuppressFadeIn to FALSE")
		#ENDIF
	ENDIF
	
	IF scriptData.cutscene.bPlaying
		SIMPLE_CUTSCENE_STOP(scriptData.cutscene)
	ENDIF
	
	CLEAR_BIT(scriptData.iBS, BS_IE_DELIVERY_CREATED_SHOWROOM_6_PROPS)
	
	IE_DELIVERY_CLEANUP_EVERYTHING()
	
	IF IS_BIT_SET(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_NO_LOADING_SCREEN_SET)
		// Fix for flickering delivery shards
		SET_NO_LOADING_SCREEN(FALSE)
		CLEAR_BIT(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_NO_LOADING_SCREEN_SET)
		#IF IS_DEBUG_BUILD
			PRINTLN("[IE_DELIVERY][CUTSCENE] SET_NO_LOADING_SCREEN is now set to FALSE")
		#ENDIF
	ENDIF
	
	IE_DELIVERY_CLEAN_GLOBAL_PLAYER_BD()
	
	INT i
	REPEAT IE_DELIVERY_SOUND_LOOPS_COUNT i
		IF IE_DELIVERY_GET_SOUND_LOOP_DETAILS(i, scriptData.eDropoffID, cutsceneData.sLoopDetails[i], scriptData.cutscene, scriptData.bViaCargobob)
		AND cutsceneData.iSoundLoopsID[i] > -1
			RELEASE_SOUND_ID(cutsceneData.iSoundLoopsID[i])
			cutsceneData.iSoundLoopsID[i] = -1
			#IF IS_DEBUG_BUILD
				PRINTLN("[IE_DELIVERY][CUTSCENE] SCRIPT_CLEANUP - Releasing sound id for loop ", i)
			#ENDIF
		ENDIF
	ENDREPEAT
	
	//_CLEANUP_ALL_ASSETS()
	
	_CLOSE_THE_DOOR()
	
	IF IS_BIT_SET(scriptData.iBS, BS_IE_DELIVERY_STARTED_AUDIO_SCENE)
		STOP_AUDIO_SCENE(cutsceneData.strAudioScene)
		CLEAR_BIT(scriptData.iBS, BS_IE_DELIVERY_STARTED_AUDIO_SCENE)
		#IF IS_DEBUG_BUILD
			PRINTLN("[IE_DELIVERY][CUTSCENE] SCRIPT_CLEANUP - Stopped audio scene ", cutsceneData.strAudioScene)
		#ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[IE_DELIVERY][CUTSCENE] SCRIPT_CLEANUP")
	#ENDIF
	
	IF HAS_PLAYER_JUST_DELIVERED_IE_VEHICLE(PLAYER_ID())
		START_TIMER_TO_CLEAR_IE_VEHICLE_I_JUST_DELIVERED()
	ENDIF
	
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════╡ VEHICLE DESTRUCTION  ╞═══════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

FUNC BOOL HAS_EVERYONE_STARTED_DELIVERY_CUTSCENE()
	INT iParticipantCount = NETWORK_GET_MAX_NUM_PARTICIPANTS()
	INT i
	PARTICIPANT_INDEX participant
	REPEAT iParticipantCount i
	
		#IF IS_DEBUG_BUILD
			//PRINTLN("[IE_DELIVERY][CUTSCENE] HAS_EVERYONE_STARTED_DELIVERY_CUTSCENE - Checking participant ", i)
		#ENDIF
	
		participant = INT_TO_PARTICIPANTINDEX(i)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(participant)
			IF NOT IS_BIT_SET(playerBD[i].iBS, BS_PLAYER_BD_IE_DELIVERY_STARTED_CUTSCENE)
			
				#IF IS_DEBUG_BUILD
					PRINTLN("[IE_DELIVERY][CUTSCENE] HAS_EVERYONE_STARTED_DELIVERY_CUTSCENE - ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(participant)), " hasn't yet started the cutscene.")
				#ENDIF
			
				RETURN FALSE
			ELSE
				#IF IS_DEBUG_BUILD
					//PRINTLN("[IE_DELIVERY][CUTSCENE] HAS_EVERYONE_STARTED_DELIVERY_CUTSCENE - ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(participant)), " started the cutscene.")
				#ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
				//PRINTLN("[IE_DELIVERY][CUTSCENE] HAS_EVERYONE_STARTED_DELIVERY_CUTSCENE - Participant ", i, " is not active")
			#ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

FUNC BOOL HAS_EVERYONE_FINISHED_DELIVERY_CUTSCENE()
	INT iParticipantCount = NETWORK_GET_MAX_NUM_PARTICIPANTS()
	INT i
	PARTICIPANT_INDEX participant
	REPEAT iParticipantCount i
		participant = INT_TO_PARTICIPANTINDEX(i)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(participant)
			IF NOT IS_BIT_SET(playerBD[i].iBS, BS_PLAYER_BD_IE_DELIVERY_FINISHED_CUTSCENE)
			
				#IF IS_DEBUG_BUILD
					PRINTLN("[IE_DELIVERY][CUTSCENE] HAS_EVERYONE_FINISHED_DELIVERY_CUTSCENE - ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(participant)), " is still doing the cutscene.")
				#ENDIF
			
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

#IF IS_DEBUG_BUILD
FUNC STRING IE_DELIVERY_DESTRUCTION_GET_DEBUG_STAGE_NAME(IE_DELIVERY_VEHICLE_DESTRUCTION_STAGE stage)
	SWITCH stage
		CASE IE_DELIVERY_DESTRUCTION_IDLE RETURN "IE_DELIVERY_DESTRUCTION_IDLE" BREAK
		CASE IE_DELIVERY_DESTRUCTION_START RETURN "IE_DELIVERY_DESTRUCTION_START" BREAK
		CASE IE_DELIVERY_DESTRUCTION_WAITING RETURN "IE_DELIVERY_DESTRUCTION_WAITING" BREAK
		CASE IE_DELIVERY_DESTRUCTION_DESTROYING RETURN "IE_DELIVERY_DESTRUCTION_DESTROYING" BREAK
		CASE IE_DELIVERY_DESTRUCTION_DONE RETURN "IE_DELIVERY_DESTRUCTION_DONE" BREAK
	ENDSWITCH
	
	RETURN "*** INVALID DESTRUCTION STAGE ***"
ENDFUNC
#ENDIF

PROC IE_DELIVERY_DESTRUCTION_SET_SERVER_STAGE(IE_DELIVERY_VEHICLE_DESTRUCTION_STAGE stage)
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		EXIT
	ENDIF
	
	IF serverBD.eVehDestructionStage != stage
		#IF IS_DEBUG_BUILD
			PRINTLN("[IE_DELIVERY][CUTSCENE] IE_DELIVERY_DESTRUCTION_SET_SERVER_STAGE - New stage: ", IE_DELIVERY_DESTRUCTION_GET_DEBUG_STAGE_NAME(stage))
		#ENDIF
		serverBD.eVehDestructionStage = stage
	ENDIF
ENDPROC

PROC IE_DELIVERY_CONSUME_DELIVERED_VEHICLE()
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		EXIT
	ENDIF
	
	IF serverBD.eVehDestructionStage = IE_DELIVERY_DESTRUCTION_IDLE
		#IF IS_DEBUG_BUILD
			PRINTLN("[IE_DELIVERY][CUTSCENE] IE_DELIVERY_CONSUME_DELIVERED_VEHICLE - Will now consume the vehicle.")
		#ENDIF
	
		serverBD.eVehDestructionStage = IE_DELIVERY_DESTRUCTION_START
	ENDIF
ENDPROC

FUNC BOOL SHOULD_THIS_SCRIPT_CLEANUP_THE_DELIVERED_VEHICLE()
	IF NOT IS_ENTITY_DEAD(scriptData.vehDelivered)
		IF NOT IS_ENTITY_A_MISSION_ENTITY(scriptData.vehDelivered)
		OR DOES_ENTITY_BELONG_TO_THIS_SCRIPT(scriptData.vehDelivered)
		#IF IS_DEBUG_BUILD OR g_IEDeliveryData.d_bForceCleanup #ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════════╡ CUTSCENE  ╞════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC IE_CUTSCENE_ATTACH_VEH_CLONE_TO_CARGOBOB(BOOL bAttach)
	IF NOT scriptData.bViaCargobob
		EXIT
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(cutsceneData.vehProxyClone)
	OR IS_ENTITY_DEAD(cutsceneData.vehProxyClone)
	OR NOT DOES_ENTITY_EXIST(cutsceneData.vehClone)
	OR IS_ENTITY_DEAD(cutsceneData.vehClone)
		EXIT
	ENDIF
	
	BOOL bUseCargobobAttach = TRUE
	
	IF NOT cutsceneData.bVehCloneAttachedToProxy
		IF bAttach
			IF bUseCargobobAttach
				SET_ENTITY_HEADING(cutsceneData.vehClone, GET_ENTITY_HEADING(cutsceneData.vehProxyClone))
				//VECTOR vModelMin, vModelMax
				//GET_MODEL_DIMENSIONS(scriptData.modelVehDelivered, vModelMin, vModelMax)
				
				FLOAT fAttachedHeading = GET_ENTITY_HEADING(cutsceneData.vehProxyClone)
				VECTOR vProxyDir = GET_HEADING_AS_VECTOR(fAttachedHeading)

				VECTOR vAttachedCoords = GET_ENTITY_COORDS(cutsceneData.vehProxyClone) - <<0, 0.0, 3.5>> + vProxyDir * 1.6
				
				//cutsceneData.vehTmp = CREATE_VEHICLE(scriptData.modelVehDelivered, vAttachedCoords, fAttachedHeading, FALSE, FALSE)
				SET_ENTITY_COORDS_NO_OFFSET(cutsceneData.vehClone, vAttachedCoords)
				//SET_ENTITY_COORDS_NO_OFFSET(cutsceneData.vehClone, )
				ATTACH_ENTITY_TO_CARGOBOB(cutsceneData.vehProxyClone, cutsceneData.vehClone, -1, <<0.0, 0.0, 0.0>>)
				
				STABILISE_ENTITY_ATTACHED_TO_HELI(cutsceneData.vehProxyClone, NULL, 0.0)
				//SET_ENTITY_VISIBLE(cutsceneData.vehTmp, FALSE)
				//DELETE_VEHICLE(vehTmp)
			ELSE
				ATTACH_ENTITY_TO_ENTITY(cutsceneData.vehClone, cutsceneData.vehProxyClone, 0, <<0.0, 0.0, -3.0>>, <<0.0, 0.0, 0.0>>)
			ENDIF
			cutsceneData.bVehCloneAttachedToProxy = TRUE
			#IF IS_DEBUG_BUILD
				PRINTLN("[IE_DELIVERY][CUTSCENE] IE_CUTSCENE_ATTACH_VEH_CLONE_TO_CARGOBOB - Attached!")
			#ENDIF
		ENDIF
	ELSE
		IF NOT bAttach
			IF bUseCargobobAttach
				DETACH_ENTITY_FROM_CARGOBOB(cutsceneData.vehProxyClone, cutsceneData.vehClone)
				DETACH_ENTITY(cutsceneData.vehClone)
			ELSE
				DETACH_ENTITY(cutsceneData.vehClone)
			ENDIF
			#IF IS_DEBUG_BUILD
				PRINTLN("[IE_DELIVERY][CUTSCENE] IE_CUTSCENE_ATTACH_VEH_CLONE_TO_CARGOBOB - Detached!")
			#ENDIF
			cutsceneData.bVehCloneAttachedToProxy = FALSE
		ENDIF
	ENDIF
ENDPROC

PROC IE_CUTSCENE_ATTACH_VEH_CLONE_TO_TOWTRUCK(BOOL bAttach)
	IF NOT scriptData.bViaTowtruck
		EXIT
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(cutsceneData.vehProxyClone)
	OR IS_ENTITY_DEAD(cutsceneData.vehProxyClone)
	OR NOT DOES_ENTITY_EXIST(cutsceneData.vehClone)
	OR IS_ENTITY_DEAD(cutsceneData.vehClone)
		EXIT
	ENDIF
	
	IF NOT cutsceneData.bVehCloneAttachedToProxy
		IF bAttach
			SET_ENTITY_HEADING(cutsceneData.vehClone, GET_ENTITY_HEADING(cutsceneData.vehProxyClone))
			VECTOR vAttachedCoords = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(cutsceneData.vehProxyClone, <<0.0, -7.5, 0.0>>)
			SET_ENTITY_COORDS_NO_OFFSET(cutsceneData.vehClone, vAttachedCoords)
			SET_VEHICLE_ON_GROUND_PROPERLY(cutsceneData.vehClone)
			
			ATTACH_VEHICLE_TO_TOW_TRUCK(cutsceneData.vehProxyClone, cutsceneData.vehClone, -1, <<0, 1.5, 0>>)
			SET_VEHICLE_TOW_TRUCK_ARM_POSITION(cutsceneData.vehProxyClone, 1.0)
			
			cutsceneData.bVehCloneAttachedToProxy = TRUE
			
			#IF IS_DEBUG_BUILD
				PRINTLN("[IE_DELIVERY][CUTSCENE] IE_CUTSCENE_ATTACH_VEH_CLONE_TO_TOWTRUCK - Attached!")
			#ENDIF
		ENDIF
	ELSE
		IF NOT bAttach
			DETACH_VEHICLE_FROM_TOW_TRUCK(cutsceneData.vehProxyClone, cutsceneData.vehClone)
			
			#IF IS_DEBUG_BUILD
				PRINTLN("[IE_DELIVERY][CUTSCENE] IE_CUTSCENE_ATTACH_VEH_CLONE_TO_TOWTRUCK - Detached!")
			#ENDIF
			cutsceneData.bVehCloneAttachedToProxy = FALSE
		ENDIF
	ENDIF
ENDPROC

PROC _CREATE_IE_DELIVERY_CUTSCENE_PLAYER_ACTOR_(PED_INDEX &targetPed, PED_INDEX &sourcePed, VECTOR vSpawnPosition)
	IF DOES_ENTITY_EXIST(targetPed)
		#IF IS_DEBUG_BUILD
			PRINTLN("[IE_DELIVERY][CUTSCENE] _CREATE_IE_DELIVERY_CUTSCENE_ACTOR_ - Ped already exists!")
		#ENDIF
		EXIT
	ENDIF
	
	IF IS_PED_FEMALE(sourcePed)
		targetPed = CREATE_PED(PEDTYPE_CIVFEMALE, GET_ENTITY_MODEL(sourcePed), vSpawnPosition, 0.0, FALSE, FALSE)
	ELSE
		targetPed = CREATE_PED(PEDTYPE_CIVMALE, GET_ENTITY_MODEL(sourcePed), vSpawnPosition, 0.0, FALSE, FALSE)
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(targetPed)
	AND NOT IS_ENTITY_DEAD(sourcePed)
		CLONE_PED_TO_TARGET(sourcePed, targetPed)
		SET_ENTITY_NO_COLLISION_ENTITY(targetPed, sourcePed, FALSE)
		SET_ENTITY_CAN_BE_DAMAGED(targetPed, FALSE)
		_SET_ALL_CUTSCENE_PED_PARAMS(targetPed)
	ENDIF
	
ENDPROC

FUNC PLAYER_INDEX _GET_PLAYER_IN_CACHED_SEAT(VEHICLE_SEAT vs)
	INT i
	REPEAT NETWORK_GET_NUM_PARTICIPANTS() i
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
			IF playerBD[i].vehicleSeat = vs
				RETURN NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
			ENDIF
		ENDIF
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
	// To test how walking away works in cutscene create full set of peds if in debug
	IF GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].IEDeliveryBD.db_bAllowAllVehicles
		RETURN scriptData.pDeliverer
	ENDIF
	#ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[IE_DELIVERY][CUTSCENE] _GET_PLAYER_IN_CACHED_SEAT - We dont have player for this cached seat: ", _GET_VEHICLE_SEAT_FOR_DEBUG(vs))
	#ENDIF
	
	// Fallbacks, just in case - worst case scenario we have one ped in the cutscene
	IF vs = VS_DRIVER
		#IF IS_DEBUG_BUILD
			PRINTLN("[IE_DELIVERY][CUTSCENE] _GET_PLAYER_IN_CACHED_SEAT - Using fallback for vs_driver, pdeliverer: ", GET_PLAYER_NAME(scriptData.pDeliverer))
		#ENDIF
		RETURN scriptData.pDeliverer
	ENDIF
	
	RETURN INVALID_PLAYER_INDEX()
ENDFUNC

PROC _CREATE_IE_DELIVERY_PED_PLAYER_ACTORS_()
	IF NOT DOES_ENTITY_EXIST(cutsceneData.vehClone) OR IS_ENTITY_DEAD(cutsceneData.vehClone)
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF g_IEDeliveryData.d_bForceCargobobVariation
	AND scriptData.cutscene.dbg.bPlayedViaDebug
		// Dont create peds in cargobob variation that's debug played
		EXIT
	ENDIF
	#ENDIF
	
	INT i
	VEHICLE_SEAT vehicleSeat
	PED_INDEX pedInSeat
	VECTOR vCreateCoord
	
	PLAYER_INDEX pInSeat
	
	BOOL bUseCachedSeats = FALSE
	IF NOT scriptData.bViaCargobob
	AND scriptData.dropoffType = IE_DELIVERY_DROPOFF_CUTSCENE_TYPE_GOON_PICKUP
	AND NOT g_IEDeliveryData.bCutsceneTriggeredByProxy // We're not kicking anyone out if it was triggered by proxy
		// Cached seats are used if before the start of the cutscene we trigger exit for everyone so we can't check in realtime who's sitting where
		bUseCachedSeats = TRUE
	ENDIF
	
	REPEAT IMIN(GET_VEHICLE_MODEL_NUMBER_OF_SEATS(scriptData.modelVehDelivered), COUNT_OF(cutsceneData.pedPlayerActors)) i
		vehicleSeat = INT_TO_ENUM(VEHICLE_SEAT, i-1)
		
		#IF IS_DEBUG_BUILD
			STRING strVehSeatDebugName
			SWITCH vehicleSeat
				CASE VS_DRIVER strVehSeatDebugName = "VS_DRIVER" BREAK
				CASE VS_FRONT_RIGHT strVehSeatDebugName = "VS_FRONT_RIGHT" BREAK
				CASE VS_BACK_LEFT strVehSeatDebugName = "VS_BACK_LEFT" BREAK
				CASE VS_BACK_RIGHT strVehSeatDebugName = "VS_BACK_RIGHT" BREAK
			ENDSWITCH
		#ENDIF
		
		IF NOT bUseCachedSeats
			cutsceneData.vsPedPlayers[i] = VS_ANY_PASSENGER
	
			IF NOT IS_VEHICLE_SEAT_FREE(scriptData.vehDelivered, vehicleSeat)
				#IF IS_DEBUG_BUILD
					PRINTLN("[IE_DELIVERY][CUTSCENE] _CREATE_IE_DELIVERY_PED_PLAYER_ACTORS_ - Vehicle seat ", strVehSeatDebugName, " isn't empty so creating ped.") 
				#ENDIF
				
				pedInSeat = GET_PED_IN_VEHICLE_SEAT(scriptData.vehDelivered, vehicleSeat)
				
				IF NOT IS_ENTITY_DEAD(pedInSeat)
					vCreateCoord = GET_ENTITY_COORDS(pedInSeat)
					_CREATE_IE_DELIVERY_CUTSCENE_PLAYER_ACTOR_(cutsceneData.pedPlayerActors[i], pedInSeat, vCreateCoord)
					cutsceneData.vsPedPlayers[i] = vehicleSeat
					SET_PED_INTO_VEHICLE(cutsceneData.pedPlayerActors[i], cutsceneData.vehClone, cutsceneData.vsPedPlayers[i])
				ENDIF
			#IF IS_DEBUG_BUILD
			// To test how walking away works in cutscene create full set of peds if in debug
			ELIF GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].IEDeliveryBD.db_bAllowAllVehicles
				pedInSeat = GET_PED_IN_VEHICLE_SEAT(scriptData.vehDelivered, VS_DRIVER)
				
				IF NOT IS_ENTITY_DEAD(pedInSeat)
					vCreateCoord = GET_ENTITY_COORDS(pedInSeat)
					_CREATE_IE_DELIVERY_CUTSCENE_PLAYER_ACTOR_(cutsceneData.pedPlayerActors[i], pedInSeat, vCreateCoord)
					cutsceneData.vsPedPlayers[i] = vehicleSeat
					SET_PED_INTO_VEHICLE(cutsceneData.pedPlayerActors[i], cutsceneData.vehClone, cutsceneData.vsPedPlayers[i])
				ENDIF
			#ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
				PRINTLN("[IE_DELIVERY][CUTSCENE] _GET_PLAYER_IN_CACHED_SEAT - Using cached seats...")
			#ENDIF
		
			pInSeat = _GET_PLAYER_IN_CACHED_SEAT(vehicleSeat)
			
			#IF IS_DEBUG_BUILD
				PRINTLN("[IE_DELIVERY][CUTSCENE] _GET_PLAYER_IN_CACHED_SEAT - Checking seat: ", _GET_VEHICLE_SEAT_FOR_DEBUG(vehicleSeat))
			#ENDIF
		
			IF IS_NET_PLAYER_OK(pInSeat, FALSE)
				pedInSeat = GET_PLAYER_PED(pInSeat)
				IF NOT IS_ENTITY_DEAD(pedInSeat)
					#IF IS_DEBUG_BUILD
						PRINTLN("[IE_DELIVERY][CUTSCENE] _CREATE_IE_DELIVERY_PED_PLAYER_ACTORS_ - Creating ped based on cached seat for seat ", strVehSeatDebugName) 
					#ENDIF
					
					_CREATE_IE_DELIVERY_CUTSCENE_PLAYER_ACTOR_(cutsceneData.pedPlayerActors[i], pedInSeat, scriptData.vDropoffCoords - <<0.0, 0.0, 10.0>>)
					cutsceneData.vsPedPlayers[i] = vehicleSeat
					
					//SET_VEHICLE_DOOR_OPEN(cutsceneData.vehClone, INT_TO_ENUM(SC_DOOR_LIST, ENUM_TO_INT(cutsceneData.vsPedPlayers[i]) + 1), TRUE, TRUE)
					
					IF DOES_ENTITY_EXIST(cutsceneData.pedPlayerActors[i]) AND NOT IS_ENTITY_DEAD(cutsceneData.pedPlayerActors[i])
						SET_ENTITY_COLLISION(cutsceneData.pedPlayerActors[i], FALSE)
						FREEZE_ENTITY_POSITION(cutsceneData.pedPlayerActors[i], TRUE)
					ENDIF
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
					PRINTLN("[IE_DELIVERY][CUTSCENE] _GET_PLAYER_IN_CACHED_SEAT - Player in that seat is not ok!")
				#ENDIF
			ENDIF	
		ENDIF
	ENDREPEAT
ENDPROC

PROC CLONE_VEHICLE_STATE(VEHICLE_INDEX &vehClone, VEHICLE_INDEX vehToClone)
	//Lights
	INT iLightsOn, iFullBeam
	GET_VEHICLE_LIGHTS_STATE(vehToClone, iLightsOn, iFullBeam)
	
	IF iLightsOn = 1
		SET_VEHICLE_LIGHTS(vehClone, FORCE_VEHICLE_LIGHTS_ON)
	ENDIF
	
	IF iFullBeam = 1
		SET_VEHICLE_FULLBEAM(vehClone, TRUE)
	ENDIF
	
	//Engine
	IF GET_IS_VEHICLE_ENGINE_RUNNING(vehToClone)
		SET_VEHICLE_ENGINE_ON(vehClone, TRUE, TRUE)
	ENDIF
ENDPROC

PROC _CREATE_IE_DELIVERY_CUTSCENE_VEH_CLONE_()
	
	VECTOR vCoords
	FLOAT fCloneHeading
	//FLOAT fApproachHeading = 0.0

	IE_DELIVERY_GET_DROPOFF_VEHICLE_CLONE_DATA(scriptData.eDropoffID, vCoords, fCloneHeading)
	
	/*
	// commenting out for now before i come up with a better way of handling this with car recordings
	IF NOT IS_ENTITY_DEAD(scriptData.vehDelivered)
		fApproachHeading = GET_ENTITY_HEADING(scriptData.vehDelivered)
	ENDIF

	// Determine if the vehicle should reverse into the drop off.
	IF NOT IS_HEADING_ACCEPTABLE(fApproachHeading, fDropOffHeading, 150.0)
		IF NOT IS_BIT_SET(cutsceneData.iBS, BS_IE_DELIVERY_CUTSCENE_REVERSE_CHECK)
			SET_BIT(cutsceneData.iBS, BS_IE_DELIVERY_CUTSCENE_REVERSE_CHECK)
		ENDIF
			
		// Reverse the vehicles heading.	
		fCloneHeading = fCloneHeading - 180.0
	ENDIF
	*/
	
	vCoords = TRANSFORM_CUTSCENE_COORDS_TO_WORLD_COORDS(vCoords)
	fCloneHeading = TRANSFORM_CUTSCENE_HEADING_TO_WORLD_HEADING(fCloneHeading)
	
	//BOOL bDontFreeze = TRUE
	VECTOR vCreationCoords = vCoords
	
	IF NOT scriptData.bViaCargobob
		vCreationCoords = vCreationCoords - <<5.0, 5.0, 5.0>>
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(scriptData.vehDelivered)
		IF CREATE_CUTSCENE_VEHICLE_CLONE(cutsceneData.vehClone, scriptData.vehDelivered, vCreationCoords, fCloneHeading)
			IF NOT IS_ENTITY_DEAD(cutsceneData.vehClone)
			
				FREEZE_ENTITY_POSITION(cutsceneData.vehClone, TRUE)
				SET_ENTITY_COLLISION(cutsceneData.vehClone, FALSE)
				SET_ENTITY_CAN_BE_DAMAGED(cutsceneData.vehClone, FALSE)
			
				CLONE_VEHICLE_STATE(cutsceneData.vehClone, scriptData.vehDelivered)
				
				IF scriptData.dropoffType = IE_DELIVERY_DROPOFF_CUTSCENE_TYPE_RAMP
					IF GET_NUM_MOD_KITS(cutsceneData.vehClone) > 0
					AND GET_VEHICLE_MOD_KIT(cutsceneData.vehClone) >= 0
						REMOVE_VEHICLE_MOD(cutsceneData.vehClone, MOD_SUSPENSION)
					ENDIF
				ENDIF
				
				IF scriptData.dropoffType = IE_DELIVERY_DROPOFF_CUTSCENE_TYPE_GARAGE
					SET_VEHICLE_DOORS_SHUT(cutsceneData.vehClone, TRUE)
					SET_VEHICLE_DOORS_LOCKED(cutsceneData.vehClone, VEHICLELOCK_LOCKED)
					SET_VEHICLE_ON_GROUND_PROPERLY(cutsceneData.vehClone)
					SET_VEHICLE_ENGINE_ON(cutsceneData.vehClone, TRUE, TRUE)
					SET_ENTITY_COLLISION(cutsceneData.vehClone, FALSE)
					FREEZE_ENTITY_POSITION(cutsceneData.vehClone, TRUE)
					
					IF IS_VEHICLE_DRIVEABLE(cutsceneData.vehClone)
						SET_VEHICLE_LIGHTS(cutsceneData.vehClone, SET_VEHICLE_LIGHTS_ON)
					ENDIF
					
					#IF IS_DEBUG_BUILD
						PRINTLN("[IE_DELIVERY][CUTSCENE] _CREATE_IE_DELIVERY_CUTSCENE_VEH_CLONE_ - This is a garage scene so freezing veh clone position.")
					#ENDIF
				ENDIF
		
				// Extra clone for when ped is driving away
				IF scriptData.dropoffType = IE_DELIVERY_DROPOFF_CUTSCENE_TYPE_GOON_PICKUP
					VECTOR vExtraClonePos
					FLOAT fExtraCloneHeading
					
					IF scriptData.eDropoffID = IE_DROPOFF_GANGSTER_2
						//vExtraClonePos = <<-1910.0619, 2031.1853, 139.7393>>
						//fExtraCloneHeading = 222.1863
						vExtraClonePos = <<-1896.5269, 2022.5604, 140.4056>>
						fExtraCloneHeading = -141.97
					ELIF scriptData.eDropoffID = IE_DROPOFF_GANGSTER_3
						vExtraClonePos = <<-94.2589, 914.2013, 234.6663>>
						fExtraCloneHeading = 45.5841
					ENDIF
					
					IF CREATE_CUTSCENE_VEHICLE_CLONE(cutsceneData.vehActors[0], cutsceneData.vehClone, vExtraClonePos, fExtraCloneHeading)
						IF NOT IS_ENTITY_DEAD(cutsceneData.vehActors[0])
							SET_VEHICLE_ENGINE_ON(cutsceneData.vehActors[0], TRUE, TRUE)
							SET_ENTITY_COLLISION(cutsceneData.vehActors[0], FALSE)
							SET_ENTITY_VISIBLE(cutsceneData.vehActors[0], FALSE)
							FREEZE_ENTITY_POSITION(cutsceneData.vehActors[0], TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC _CREATE_IE_DELIVERY_CUTSCENE_PROXY_VEH_()
	IF scriptData.modelProxyVeh != DUMMY_MODEL_FOR_SCRIPT
	AND HAS_MODEL_LOADED(scriptData.modelProxyVeh)
		
		VECTOR vCoords, vRot, vFoo
		FLOAT fFoo
		BOOL bForceCoords
		
		IF scriptData.bViaCargobob
			BOOL bHasCoords = IE_DELIVERY_GET_DROPOFF_CARGOBOB_DATA(scriptData.eDropoffID, vCoords, vRot.Z, vFoo, fFoo, vFoo, fFoo, bForceCoords)
			
			IF NOT bHasCoords
				vCoords = scriptData.vDropoffCoords + <<0.0, 0.0, 15.0>>
			ENDIF
			
			#IF IS_DEBUG_BUILD
				PRINTLN("[IE_DELIVERY][CUTSCENE] _CREATE_IE_DELIVERY_CUTSCENE_PROXY_VEH_ - Creating cargobob at: ", vCoords)
			#ENDIF
			
			cutsceneData.vehProxyClone = CREATE_VEHICLE(scriptData.modelProxyVeh, vCoords, vRot.Z, FALSE, FALSE)
			
			IF DOES_ENTITY_EXIST(cutsceneData.vehProxyClone)
			AND NOT IS_ENTITY_DEAD(cutsceneData.vehProxyClone)
				CREATE_PICK_UP_ROPE_FOR_CARGOBOB(cutsceneData.vehProxyClone)
				SET_PICKUP_ROPE_LENGTH_FOR_CARGOBOB(cutsceneData.vehProxyClone, 3.0, 3.0, TRUE)
				
				SET_HELI_BLADES_FULL_SPEED(cutsceneData.vehProxyClone)
				SET_HELI_TURBULENCE_SCALAR(cutsceneData.vehProxyClone, 0.0)
				
				SET_ENTITY_COLLISION(cutsceneData.vehProxyClone, FALSE)
				
				SET_ENTITY_CAN_BE_DAMAGED(cutsceneData.vehProxyClone, FALSE)
				
				IF bHasCoords
					FREEZE_ENTITY_POSITION(cutsceneData.vehProxyClone, TRUE)
					SET_ENTITY_COORDS_NO_OFFSET(cutsceneData.vehProxyClone, vCoords)
					SET_ENTITY_ROTATION(cutsceneData.vehProxyClone, <<0.0, 0.0, GET_ENTITY_HEADING(cutsceneData.vehProxyClone)>>)
				ENDIF
			ENDIF
			
			IE_CUTSCENE_ATTACH_VEH_CLONE_TO_CARGOBOB(TRUE)
		ENDIF
		
		// Create peds who sit in first two seats of cargobob (the rest will never be visible in cutscene)
		IF NOT IS_ENTITY_DEAD(scriptData.vehProxy)
			IF NOT IS_ENTITY_DEAD(cutsceneData.vehProxyClone)
				INT i
				VEHICLE_SEAT vsProxy
				PED_INDEX pedInProxySeat
				
				REPEAT 2 i
					vsProxy = INT_TO_ENUM(VEHICLE_SEAT, i-1)
					IF NOT IS_VEHICLE_SEAT_FREE(scriptData.vehProxy, vsProxy)
						#IF IS_DEBUG_BUILD
							PRINTLN("[IE_DELIVERY][CUTSCENE] _CREATE_IE_DELIVERY_CUTSCENE_PROXY_VEH_ - Cargobob seat not empty (", i-1, ") - creating ped.")
						#ENDIF
						pedInProxySeat = GET_PED_IN_VEHICLE_SEAT(scriptData.vehProxy, vsProxy)
						IF NOT IS_ENTITY_DEAD(pedInProxySeat)
							_CREATE_IE_DELIVERY_CUTSCENE_PLAYER_ACTOR_(cutsceneData.pedProxyActors[i], pedInProxySeat, GET_ENTITY_COORDS(pedInProxySeat))
							IF DOES_ENTITY_EXIST(cutsceneData.pedProxyActors[i]) AND NOT IS_ENTITY_DEAD(cutsceneData.pedProxyActors[i])
								IF IS_VEHICLE_SEAT_FREE(cutsceneData.vehProxyClone, vsProxy)
									SET_PED_INTO_VEHICLE(cutsceneData.pedProxyActors[i], cutsceneData.vehProxyClone, vsProxy)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
			ELSE
				#IF IS_DEBUG_BUILD
					PRINTLN("[IE_DELIVERY][CUTSCENE] _CREATE_IE_DELIVERY_CUTSCENE_PROXY_VEH_ - cutsceneData.vehProxyClone is dead")
				#ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
				PRINTLN("[IE_DELIVERY][CUTSCENE] _CREATE_IE_DELIVERY_CUTSCENE_PROXY_VEH_ - scriptData.vehProxy is dead")
			#ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC _CREATE_IE_DELIVERY_CUTSCENE_VEH_ACTORS_()
	INT i
	MODEL_NAMES eModel
	VECTOR vCoords, vRot
	WHILE IE_DELIVERY_GET_DROPOFF_VEHICLE_ACTOR_DATA(scriptData.eDropoffID, i, eModel, vCoords, vRot)
		IF HAS_MODEL_LOADED(eModel)
			
			vCoords = TRANSFORM_CUTSCENE_COORDS_TO_WORLD_COORDS(vCoords)
			vRot.Z = TRANSFORM_CUTSCENE_HEADING_TO_WORLD_HEADING(vRot.Z)
			
			cutsceneData.vehActors[i] = CREATE_VEHICLE(eModel, vCoords - <<6.0, 6.0, 6.0>>, vRot.Z, FALSE, FALSE)
			IF DOES_ENTITY_EXIST(cutsceneData.vehActors[i]) AND NOT IS_ENTITY_DEAD(cutsceneData.vehActors[i])
				FREEZE_ENTITY_POSITION(cutsceneData.vehActors[i], TRUE)
				SET_ENTITY_COLLISION(cutsceneData.vehActors[i], FALSE)
				SET_ENTITY_CAN_BE_DAMAGED(cutsceneData.vehActors[i], FALSE)
			ENDIF
			
			#IF IS_DEBUG_BUILD
				PRINTLN("[IE_DELIVERY][CUTSCENE] _CREATE_IE_DELIVERY_CUTSCENE_VEH_ACTORS_ - Created actor veh ", i)
			#ENDIF
		ENDIF
		i += 1
	ENDWHILE
	
	/*
	i = 0
	WHILE IE_DELIVERY_GET_DROPOFF_VEHICLE_ACTOR_DATA(scriptData.eDropoffID, i, eModel, vCoords, vRot)
		IF HAS_MODEL_LOADED(eModel)
			SET_MODEL_AS_NO_LONGER_NEEDED(eModel)
		ENDIF
		i += 1
	ENDWHILE
	*/
ENDPROC

PROC _CREATE_IE_DELIVERY_CUTSCENE_PED_ACTORS_()
	INT i
	MODEL_NAMES eModel
	VECTOR vCoords, vRot
	PED_TYPE pedType
	TEXT_LABEL_63 txtAnimDict
	
	WHILE IE_DELIVERY_GET_DROPOFF_PED_DATA(scriptData.eDropoffID, i, pedType, eModel, vCoords, vRot, txtAnimDict, scriptData.bViaCargobob)
		IF HAS_MODEL_LOADED(eModel)
			
			vCoords = TRANSFORM_CUTSCENE_COORDS_TO_WORLD_COORDS(vCoords)
			vRot.Z = TRANSFORM_CUTSCENE_HEADING_TO_WORLD_HEADING(vRot.Z)
				
			cutsceneData.pedActors[i] = CREATE_PED(pedType, eModel, vCoords - <<0.0, 0.0, 7.0>>, vRot.Z, FALSE, FALSE)
			
			FREEZE_ENTITY_POSITION(cutsceneData.pedActors[i], TRUE)
			SET_ENTITY_COLLISION(cutsceneData.pedActors[i], FALSE)
			_SET_ALL_CUTSCENE_PED_PARAMS(cutsceneData.pedActors[i])
			
			#IF IS_DEBUG_BUILD
				PRINTLN("[IE_DELIVERY][CUTSCENE] _CREATE_IE_DELIVERY_CUTSCENE_PED_ACTORS_ - Created actor ped ", i, " at ", vCoords, " with rot ", vRot)
			#ENDIF
		ENDIF
		i += 1
		txtAnimDict = ""
	ENDWHILE
	
	/*
	WHILE IE_DELIVERY_GET_DROPOFF_PED_DATA(scriptData.eDropoffID, i, pedType, eModel, vCoords, vRot, txtAnimDict, scriptData.bViaCargobob)
		IF HAS_MODEL_LOADED(eModel)
			SET_MODEL_AS_NO_LONGER_NEEDED(eModel)
		ENDIF
	ENDWHILE
	*/
	
	// Special cases
	
	IF scriptData.dropoffType = IE_DELIVERY_DROPOFF_CUTSCENE_TYPE_RAMP
		// Trucker
		IF DOES_ENTITY_EXIST(cutsceneData.vehActors[0]) 
		AND NOT IS_ENTITY_DEAD(cutsceneData.vehActors[0])
		AND DOES_ENTITY_EXIST(cutsceneData.pedActors[0])
		AND NOT IS_ENTITY_DEAD(cutsceneData.pedActors[0])
			FREEZE_ENTITY_POSITION(cutsceneData.pedActors[0], FALSE)
			SET_ENTITY_COLLISION(cutsceneData.pedActors[0], TRUE)
			SET_PED_INTO_VEHICLE(cutsceneData.pedActors[0], cutsceneData.vehActors[0])
		ENDIF
		
		// Mechanic
		/*
		IF DOES_ENTITY_EXIST(cutsceneData.pedActors[1])
		AND NOT IS_ENTITY_DEAD(cutsceneData.pedActors[1])
			VECTOR vPedPos = GET_ENTITY_COORDS(cutsceneData.pedActors[1])
			IF GET_GROUND_Z_FOR_3D_COORD(vPedPos, vPedPos.Z)
				SET_ENTITY_COORDS(cutsceneData.pedActors[1], vPedPos)
			ENDIF
			
			IE_DELIVERY_GET_DROPOFF_PED_DATA(scriptData.eDropoffID, 1, pedType, eModel, vCoords, vRot, txtAnimDict, scriptData.bViaCargobob)
			
			TASK_PLAY_ANIM(cutsceneData.pedActors[1], txtAnimDict, "idle_f", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, DEFAULT, AF_LOOPING)
			//FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(cutsceneData.pedActors[1])
			//REMOVE_ANIM_DICT(txtAnimDict)
		ENDIF
		*/
	ELIF scriptData.dropoffType = IE_DELIVERY_DROPOFF_CUTSCENE_TYPE_GOON_PICKUP
		IF IE_DELIVERY_GET_DROPOFF_PED_DATA(scriptData.eDropoffID, 0, pedType, eModel, vCoords, vRot, txtAnimDict, scriptData.bViaCargobob)
			cutsceneData.strSyncedSceneDict = txtAnimDict
			cutsceneData.strSyncedScenePlayerAnim = "return_wallet_positive_a_player"
			cutsceneData.strSyncedScenePedAnim = "return_wallet_positive_a_male"
			
			VECTOR vInitialPedPos
			
			IF scriptData.eDropoffID = IE_DROPOFF_GANGSTER_2
				cutsceneData.vSyncedScenePos = <<-1923.271, 2048.746, 139.735>>
				cutsceneData.vSyncedSceneRot = <<0.0, 0.0, 166.680>>
				vInitialPedPos = <<-1908.0570, 2036.3397, 139.7387>>
			ELIF scriptData.eDropoffID = IE_DROPOFF_GANGSTER_3
				cutsceneData.vSyncedScenePos = <<-63.469, 897.776, 234.760>>
				cutsceneData.vSyncedSceneRot = <<0.0, 0.0, 32.340>>
				vInitialPedPos = <<-80.7345, 906.4164, 234.7204>>
			ENDIF
			
			// Extra clone we use to drive away in the car
			cutsceneData.pedActors[1] = CREATE_PED(pedType, eModel, vInitialPedPos, 0.0, FALSE, FALSE)
			IF NOT IS_ENTITY_DEAD(cutsceneData.pedActors[1])
			AND NOT IS_ENTITY_DEAD(cutsceneData.pedActors[0])
				CLONE_PED_TO_TARGET(cutsceneData.pedActors[0], cutsceneData.pedActors[1])
				_SET_ALL_CUTSCENE_PED_PARAMS(cutsceneData.pedActors[1])
			ENDIF
			
			#IF IS_DEBUG_BUILD
				PRINTLN("[IE_DELIVERY][CUTSCENE] _CREATE_IE_DELIVERY_CUTSCENE_PED_ACTORS_ - Setting anim dict for synced scene: ", cutsceneData.strSyncedSceneDict, " from txtLabel: ", txtAnimDict)
			#ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC _CREATE_IE_DELIVERY_CUTSCENE_PROPS_()
	INT i
	MODEL_NAMES eModel
	VECTOR vCoords, vRot
	WHILE IE_DELIVERY_GET_DROPOFF_PROP_DATA(scriptData.eDropoffID, i, eModel, vCoords, vRot, scriptData.bViaCargobob)
		IF HAS_MODEL_LOADED(eModel)
		AND NOT DOES_ENTITY_EXIST(cutsceneData.objProps[i])
			vCoords = TRANSFORM_CUTSCENE_COORDS_TO_WORLD_COORDS(vCoords)
			vRot.Z = TRANSFORM_CUTSCENE_HEADING_TO_WORLD_HEADING(vRot.Z)
			
			cutsceneData.objProps[i] = CREATE_OBJECT_NO_OFFSET(eModel, vCoords, FALSE, FALSE, TRUE)
			IF NOT IS_ENTITY_DEAD(cutsceneData.objProps[i])
				SET_ENTITY_ROTATION(cutsceneData.objProps[i], vRot)
				SET_ENTITY_VISIBLE(cutsceneData.objProps[i], FALSE)
				SET_ENTITY_COLLISION(cutsceneData.objProps[i], FALSE)
				SET_ENTITY_CAN_BE_DAMAGED(cutsceneData.objProps[i], FALSE)
				//SET_ENTITY_VISIBLE(cutsceneData.objProps[i], FALSE)
			ENDIF
			
			#IF IS_DEBUG_BUILD
				PRINTLN("[IE_DELIVERY][CUTSCENE] _CREATE_IE_DELIVERY_CUTSCENE_PROPS_ - Created prop: ", GET_MODEL_NAME_FOR_DEBUG(eModel), " at ", vCoords, " and rot ", vRot)
			#ENDIF
		ENDIF
		i += 1
	ENDWHILE
	
	IF scriptData.dropoffType = IE_DELIVERY_DROPOFF_CUTSCENE_TYPE_CRANE
		// Attach ropes to the spreader
		IF DOES_ENTITY_EXIST(cutsceneData.objProps[0])
		AND DOES_ENTITY_EXIST(cutsceneData.objProps[2])
			FREEZE_ENTITY_POSITION(cutsceneData.objProps[2], TRUE)
			ATTACH_ENTITY_TO_ENTITY(cutsceneData.objProps[2], cutsceneData.objProps[0], -1, <<0.0, 0.0, 7.41>>, <<0.0, 0.0, 0.0>>)
		ENDIF
		
		// By default spreader is open
		PLAY_ENTITY_ANIM(cutsceneData.objProps[0], "Dock_crane_SLD_unload", "map_objects", INSTANT_BLEND_IN, FALSE, TRUE, FALSE, 1.0)
	ENDIF
	
	// TODO: figure out how to restore this fence
	/*
	IF scriptData.eDropoffID = IE_DROPOFF_WAREHOUSE_8
		OBJECT_INDEX objFence = GET_CLOSEST_OBJECT_OF_TYPE(<<133.4024, -3003.1709, 6.2215>>, 2.0, PROP_FNCCORGM_04A)
		IF DOES_ENTITY_EXIST(objFence)
			SET_ENTITY_HEALTH(objFence, 1000)
			SET_ENTITY_DYNAMIC(objFence, FALSE)
			FREEZE_ENTITY_POSITION(objFence, TRUE)
			#IF IS_DEBUG_BUILD
				PRINTLN("[IE_DELIVERY][CUTSCENE] _CREATE_IE_DELIVERY_CUTSCENE_PROPS_ - We have the fence")
			#ENDIF
		ENDIF
	ENDIF
	*/
ENDPROC

PROC _CREATE_IE_DELIVERY_CARGOBOB_MECHANIC_()
	IF (scriptData.bViaCargobob
	AND NOT IS_ENTITY_DEAD(scriptData.vehDelivered)
	AND IS_VEHICLE_SEAT_FREE(scriptData.vehDelivered, VS_DRIVER))
	#IF IS_DEBUG_BUILD
	OR debugData.bEditMode
	#ENDIF
		// We need a mechanic to come around and pick up the car
		#IF IS_DEBUG_BUILD
			PRINTLN("[IE_DELIVERY][CUTSCENE] _CREATE_IE_DELIVERY_CARGOBOB_MECHANIC_ - There's no one in the delivered vehicle that could drive it so let's have mechanic come around and help us out.")
		#ENDIF
		
		CREATE_IE_WAREHOUSE_MECHANIC(cutsceneData.pedProxyMechanic, scriptData.vDropoffCoords - <<0.0, 0.0, 5.0>>, 0.0, FALSE)
		IF DOES_ENTITY_EXIST(cutsceneData.pedProxyMechanic)
		AND NOT IS_ENTITY_DEAD(cutsceneData.pedProxyMechanic)
			FREEZE_ENTITY_POSITION(cutsceneData.pedProxyMechanic, TRUE)
			SET_ENTITY_VISIBLE(cutsceneData.pedProxyMechanic, FALSE)
			SET_ENTITY_CAN_BE_DAMAGED(cutsceneData.pedProxyMechanic, FALSE)
		ENDIF
	ENDIF
ENDPROC

PROC _MAINTAIN_EXTRA_IPL()
	IF IS_BIT_SET(scriptData.iBS, BS_IE_DELIVERY_IPLS_SHOULD_BE_LOADED)
		IF NOT IS_BIT_SET(scriptData.iBS, BS_IE_DELIVERY_IPLS_ARE_LOADED)
			IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
				REQUEST_IPL("shr_int")
				REMOVE_IPL("fakeint")
				SET_BIT(scriptData.iBS, BS_IE_DELIVERY_IPLS_ARE_LOADED)
				
				#IF IS_DEBUG_BUILD
					PRINTLN("[IE_DELIVERY][CUTSCENE] _MAINTAIN_EXTRA_IPL - IPLs are now loaded.")
				#ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
					PRINTLN("[IE_DELIVERY][CUTSCENE] _MAINTAIN_EXTRA_IPL - Waiting for load scene.")
				#ENDIF
			ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(scriptData.iBS, BS_IE_DELIVERY_IPLS_ARE_LOADED)
			IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
				REMOVE_IPL("shr_int")
				REQUEST_IPL("fakeint")
				CLEAR_BIT(scriptData.iBS, BS_IE_DELIVERY_IPLS_ARE_LOADED)
				
				#IF IS_DEBUG_BUILD
					PRINTLN("[IE_DELIVERY][CUTSCENE] _MAINTAIN_EXTRA_IPL - IPLs are now unloaded.")
				#ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
					PRINTLN("[IE_DELIVERY][CUTSCENE] _MAINTAIN_EXTRA_IPL - Waiting for load scene.")
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL _GET_PRECACHED_GARAGE_BOUNDS(VECTOR &vGaragePos, VECTOR &vGarageBBoxMin, VECTOR &vGarageBBoxMax, VECTOR &vGarageNormal, FLOAT &fGarageBBoxWidth)
	SWITCH scriptData.eDropoffID
		CASE IE_DROPOFF_GANGSTER_1
		    vGaragePos = <<-2596.7314, 1926.7139, 166.8087>>
		    vGarageBBoxMin = <<-2598.7207, 1926.5077, 165.3098>>
		    vGarageBBoxMax = <<-2594.7422, 1926.9200, 168.3076>>
		    vGarageNormal = <<-0.1031, 0.9947, 0.0005>>
		    fGarageBBoxWidth = 1.0000
			RETURN TRUE
		CASE IE_DROPOFF_GANGSTER_2
		    vGaragePos = <<-1925.3583, 2050.2065, 140.8771>>
		    vGarageBBoxMin = <<-1924.8918, 2052.1514, 139.3771>>
		    vGarageBBoxMax = <<-1925.8247, 2048.2617, 142.3771>>
		    vGarageNormal = <<0.9724, -0.2332, 0.0000>>
		    fGarageBBoxWidth = 1.0000
			RETURN TRUE
		CASE IE_DROPOFF_GANGSTER_4
		    vGaragePos = <<1407.6052, 1113.7695, 114.3377>>
		    vGarageBBoxMin = <<1405.6052, 1113.7695, 112.8377>>
		    vGarageBBoxMax = <<1409.6052, 1113.7695, 115.8377>>
		    vGarageNormal = <<0.0000, 1.0000, -0.0000>>
		    fGarageBBoxWidth = 1.0000
			RETURN TRUE
		CASE IE_DROPOFF_GANGSTER_5
		    vGaragePos = <<-1784.7701, 456.7062, 127.8081>>
		    vGarageBBoxMin = <<-1784.2516, 454.7746, 126.3081>>
		    vGarageBBoxMax = <<-1785.2887, 458.6378, 129.3081>>
		    vGarageNormal = <<-0.9658, -0.2593, 0.0000>>
		    fGarageBBoxWidth = 1.0000
			RETURN TRUE
		CASE IE_DROPOFF_GANGSTER_6
		    vGaragePos = <<221.1690, 755.5034, 204.7990>>
		    vGarageBBoxMin = <<220.1471, 753.7842, 203.2994>>
		    vGarageBBoxMax = <<222.1909, 757.2227, 206.2986>>
		    vGarageNormal = <<-0.8596, 0.5109, 0.0002>>
		    fGarageBBoxWidth = 1.0000
			RETURN TRUE
		CASE IE_DROPOFF_GANGSTER_7
		    vGaragePos = <<-1617.3875, 12.8714, 62.5411>>
		    vGarageBBoxMin = <<-1619.1815, 13.7554, 61.0411>>
		    vGarageBBoxMax = <<-1615.5934, 11.9874, 64.0411>>
		    vGarageNormal = <<0.4420, 0.8970, -0.0000>>
		    fGarageBBoxWidth = 1.0000
			RETURN TRUE
		CASE IE_DROPOFF_POLICE_STATION_1
		    vGaragePos = <<877.5331, -1350.1483, 25.5658>>
		    vGarageBBoxMin = <<877.5331, -1352.1483, 24.0658>>
		    vGarageBBoxMax = <<877.5331, -1348.1483, 27.0658>>
		    vGarageNormal = <<-1.0000, 0.0000, 0.0000>>
		    fGarageBBoxWidth = 1.0000
			RETURN TRUE
		CASE IE_DROPOFF_GANGSTER_8
		    vGaragePos = <<-3105.1680, 715.7629, 20.6390>>
		    vGarageBBoxMin = <<-3105.6914, 717.6932, 19.1389>>
		    vGarageBBoxMax = <<-3104.6445, 713.8326, 22.1391>>
		    vGarageNormal = <<0.9651, 0.2617, -0.0000>>
		    fGarageBBoxWidth = 1.0000
			RETURN TRUE
		CASE IE_DROPOFF_POLICE_STATION_4
		    vGaragePos = <<-1118.2515, -840.7108, 12.8767>>
		    vGarageBBoxMin = <<-1116.9886, -842.2617, 11.3767>>
		    vGarageBBoxMax = <<-1119.5143, -839.1599, 14.3767>>
		    vGarageNormal = <<-0.7755, -0.6314, 0.0000>>
		    fGarageBBoxWidth = 1.0000
			RETURN TRUE
		CASE IE_DROPOFF_POLICE_STATION_6
		    vGaragePos = <<-579.1629, -124.7275, 35.9910>>
		    vGarageBBoxMin = <<-577.3208, -123.9486, 34.5016>>
		    vGarageBBoxMax = <<-581.0050, -125.5063, 37.4804>>
		    vGarageNormal = <<0.3894, -0.9210, 0.0053>>
		    fGarageBBoxWidth = 1.0000
			RETURN TRUE
		CASE IE_DROPOFF_POLICE_STATION_7
		    vGaragePos = <<539.5188, -30.2404, 70.2373>>
		    vGarageBBoxMin = <<541.2302, -29.2054, 68.7379>>
		    vGarageBBoxMax = <<537.8074, -31.2753, 71.7367>>
		    vGarageNormal = <<0.5175, -0.8557, 0.0003>>
		    fGarageBBoxWidth = 1.0000
			RETURN TRUE
		CASE IE_DROPOFF_PRIVATE_BUYER_1
		    vGaragePos = <<-1080.8939, -503.8609, 36.6490>>
		    vGarageBBoxMin = <<-1082.6958, -504.7288, 35.1490>>
		    vGarageBBoxMax = <<-1079.0920, -502.9930, 38.1490>>
		    vGarageNormal = <<-0.4340, 0.9009, -0.0000>>
		    fGarageBBoxWidth = 1.0000
			RETURN TRUE
		CASE IE_DROPOFF_PRIVATE_BUYER_2
		    vGaragePos = <<161.1170, 2286.4465, 93.1284>>
		    vGarageBBoxMin = <<161.8113, 2288.3218, 91.6641>>
		    vGarageBBoxMax = <<160.4227, 2284.5713, 94.5928>>
		    vGarageNormal = <<0.9376, -0.3471, 0.0178>>
		    fGarageBBoxWidth = 1.0000
			RETURN TRUE
		CASE IE_DROPOFF_PRIVATE_BUYER_3
		    vGaragePos = <<-2221.1030, 3488.1238, 29.6693>>
		    vGarageBBoxMin = <<-2219.1030, 3488.1245, 28.1692>>
		    vGarageBBoxMax = <<-2223.1030, 3488.1230, 31.1694>>
		    vGarageNormal = <<0.0003, -1.0000, -0.0000>>
		    fGarageBBoxWidth = 1.0000
			RETURN TRUE
		CASE IE_DROPOFF_PRIVATE_BUYER_4
		    vGaragePos = <<2905.4639, 4349.2842, 50.4419>>
		    vGarageBBoxMin = <<2907.2878, 4350.1050, 48.9421>>
		    vGarageBBoxMax = <<2903.6399, 4348.4634, 51.9417>>
		    vGarageNormal = <<0.4104, -0.9119, 0.0001>>
		    fGarageBBoxWidth = 1.0000
			RETURN TRUE
		CASE IE_DROPOFF_SCRAPYARD_1
		    vGaragePos = <<-512.9298, -1739.2076, 18.6364>>
		    vGarageBBoxMin = <<-514.5682, -1738.0607, 17.1364>>
		    vGarageBBoxMax = <<-511.2914, -1740.3546, 20.1364>>
		    vGarageNormal = <<0.5735, 0.8192, 0.0000>>
		    fGarageBBoxWidth = 1.0000
			RETURN TRUE
		CASE IE_DROPOFF_SCRAPYARD_3
		    vGaragePos = <<-192.2852, 6266.4351, 30.9893>>
		    vGarageBBoxMin = <<-193.7903, 6265.1182, 29.4894>>
		    vGarageBBoxMax = <<-190.7802, 6267.7520, 32.4892>>
		    vGarageNormal = <<-0.6585, 0.7525, 0.0001>>
		    fGarageBBoxWidth = 1.0000
			RETURN TRUE
		CASE IE_DROPOFF_SHOWROOM_1
		    vGaragePos = <<473.9202, -1890.5720, 25.5946>>
		    vGarageBBoxMin = <<473.0750, -1888.7594, 24.0946>>
		    vGarageBBoxMax = <<474.7654, -1892.3846, 27.0946>>
		    vGarageNormal = <<0.9063, 0.4226, -0.0000>>
		    fGarageBBoxWidth = 1.0000
			RETURN TRUE
		CASE IE_DROPOFF_SHOWROOM_4
		    vGaragePos = <<-218.8465, 6246.3208, 31.6441>>
		    vGarageBBoxMin = <<-220.2611, 6244.9067, 30.1439>>
		    vGarageBBoxMax = <<-217.4320, 6247.7349, 33.1443>>
		    vGarageNormal = <<-0.7070, 0.7073, -0.0001>>
		    fGarageBBoxWidth = 1.0000
			RETURN TRUE
		CASE IE_DROPOFF_SHOWROOM_5
		    vGaragePos = <<-62.2309, 80.7710, 71.0252>>
		    vGarageBBoxMin = <<-63.0190, 78.9328, 69.5252>>
		    vGarageBBoxMax = <<-61.4428, 82.6091, 72.5252>>
		    vGarageNormal = <<-0.9191, 0.3941, 0.0000>>
		    fGarageBBoxWidth = 1.0000
			RETURN TRUE
		CASE IE_DROPOFF_SHOWROOM_6
		    vGaragePos = <<-29.2047, -1086.6543, 26.7492>>
		    vGarageBBoxMin = <<-31.0841, -1085.9702, 25.2492>>
		    vGarageBBoxMax = <<-27.3254, -1087.3384, 28.2492>>
		    vGarageNormal = <<0.3420, 0.9397, 0.0000>>
		    fGarageBBoxWidth = 1.0000
			RETURN TRUE
		CASE IE_DROPOFF_SHOWROOM_7
		    vGaragePos = <<2513.4470, 4108.5635, 38.7082>>
		    vGarageBBoxMin = <<2512.5603, 4106.7710, 37.2117>>
		    vGarageBBoxMax = <<2514.3337, 4110.3560, 40.2047>>
		    vGarageNormal = <<-0.8963, 0.4434, 0.0018>>
		    fGarageBBoxWidth = 1.0000
			RETURN TRUE
		CASE IE_DROPOFF_SHOWROOM_8
		    vGaragePos = <<-765.6644, -238.1184, 37.3627>>
		    vGarageBBoxMin = <<-763.8099, -237.3695, 35.8629>>
		    vGarageBBoxMax = <<-767.5189, -238.8673, 38.8625>>
		    vGarageNormal = <<0.3744, -0.9272, 0.0001>>
		    fGarageBBoxWidth = 1.0000
			RETURN TRUE
		CASE IE_DROPOFF_WAREHOUSE_1
		    vGaragePos = <<-631.4310, -1778.6383, 24.3370>>
		    vGarageBBoxMin = <<-630.2839, -1780.2766, 22.8372>>
		    vGarageBBoxMax = <<-632.5781, -1777.0000, 25.8368>>
		    vGarageNormal = <<-0.8192, -0.5736, 0.0001>>
		    fGarageBBoxWidth = 1.0000
			RETURN TRUE
		CASE IE_DROPOFF_WAREHOUSE_2
		    vGaragePos = <<995.7529, -1853.8871, 31.0310>>
		    vGarageBBoxMin = <<997.7452, -1854.0615, 29.5310>>
		    vGarageBBoxMax = <<993.7605, -1853.7126, 32.5310>>
		    vGarageNormal = <<-0.0872, -0.9962, -0.0000>>
		    fGarageBBoxWidth = 1.0000
			RETURN TRUE
		CASE IE_DROPOFF_WAREHOUSE_3
		    vGaragePos = <<-72.9623, -1820.5294, 26.4423>>
		    vGarageBBoxMin = <<-71.6767, -1818.9974, 24.9427>>
		    vGarageBBoxMax = <<-74.2479, -1822.0614, 27.9419>>
		    vGarageNormal = <<0.7660, -0.6428, 0.0002>>
		    fGarageBBoxWidth = 1.0000
			RETURN TRUE
		CASE IE_DROPOFF_WAREHOUSE_4
		    vGaragePos = <<35.9920, -1284.0127, 28.6411>>
		    vGarageBBoxMin = <<35.9920, -1282.0127, 27.1411>>
		    vGarageBBoxMax = <<35.9920, -1286.0127, 30.1411>>
		    vGarageNormal = <<1.0000, -0.0000, -0.0000>>
		    fGarageBBoxWidth = 1.0000
			RETURN TRUE
		CASE IE_DROPOFF_WAREHOUSE_5
		    vGaragePos = <<1214.2209, -1262.5192, 34.7267>>
		    vGarageBBoxMin = <<1214.2209, -1264.5192, 33.2267>>
		    vGarageBBoxMax = <<1214.2209, -1260.5192, 36.2267>>
		    vGarageNormal = <<-1.0000, 0.0000, 0.0000>>
		    fGarageBBoxWidth = 1.0000
			RETURN TRUE
		CASE IE_DROPOFF_WAREHOUSE_6
		    vGaragePos = <<804.9728, -2219.1372, 29.0439>>
		    vGarageBBoxMin = <<806.9652, -2219.3118, 27.5439>>
		    vGarageBBoxMax = <<802.9805, -2218.9626, 30.5439>>
		    vGarageNormal = <<-0.0873, -0.9962, 0.0000>>
		    fGarageBBoxWidth = 1.0000
			RETURN TRUE
		CASE IE_DROPOFF_WAREHOUSE_7
		    vGaragePos = <<1763.9326, -1645.3253, 112.1589>>
		    vGarageBBoxMin = <<1765.9022, -1644.9780, 110.6594>>
		    vGarageBBoxMax = <<1761.9630, -1645.6726, 113.6584>>
		    vGarageNormal = <<0.1736, -0.9848, 0.0002>>
		    fGarageBBoxWidth = 1.0000
			RETURN TRUE
		CASE IE_DROPOFF_WAREHOUSE_8
		    vGaragePos = <<144.3095, -3006.6299, 6.5311>>
		    vGarageBBoxMin = <<142.3095, -3006.6299, 5.0311>>
		    vGarageBBoxMax = <<146.3095, -3006.6299, 8.0311>>
		    vGarageNormal = <<-0.0000, 1.0000, -0.0000>>
		    fGarageBBoxWidth = 1.0000
			RETURN TRUE
		CASE IE_DROPOFF_WAREHOUSE_9
		    vGaragePos = <<-515.4924, -2202.7097, 5.8940>>
		    vGarageBBoxMin = <<-517.0246, -2201.4243, 4.3943>>
		    vGarageBBoxMax = <<-513.9603, -2203.9951, 7.3937>>
		    vGarageNormal = <<0.6428, 0.7661, 0.0002>>
		    fGarageBBoxWidth = 1.0000
			RETURN TRUE
		CASE IE_DROPOFF_WAREHOUSE_10
		    vGaragePos = <<-1151.2875, -2170.4719, 13.3580>>
		    vGarageBBoxMin = <<-1149.8730, -2171.8860, 11.8583>>
		    vGarageBBoxMax = <<-1152.7019, -2169.0579, 14.8577>>
		    vGarageNormal = <<-0.7070, -0.7072, 0.0001>>
		    fGarageBBoxWidth = 1.0000
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    This uses shaptest to calculate where the garage is so we know when to stop the car so it doesnt hit the garage door.
PROC _MAINTAIN_CALCULATING_GARAGE_DOOR_BOUNDS()
	
	/*
	// Replace by precached values because shapetest not always returned correct results in time (due to collisions not beeing streamed in etc.)
	IF scriptData.iShapeTestStatus = 0
		VECTOR vStartCoords, vEndCoords
		FLOAT fStartHeading
		IE_DELIVERY_GET_DROPOFF_VEHICLE_CLONE_DATA(scriptData.eDropoffID, vStartCoords, fStartHeading)
		VECTOR vDir = GET_HEADING_AS_VECTOR(fStartHeading)
		vStartCoords = vStartCoords + <<0.0, 0.0, 0.5>>
		vEndCoords = vStartCoords + vDir * 20.0
		
		scriptData.shapeTest = START_SHAPE_TEST_LOS_PROBE(vStartCoords, vEndCoords)
		IF NATIVE_TO_INT(scriptData.shapeTest) != 0
			#IF IS_DEBUG_BUILD
				PRINTLN("[IE_DELIVERY][CUTSCENE] _MAINTAIN_CALCULATING_GARAGE_DOOR_BOUNDS - Started shapetest from ", vStartCoords, " to ", vEndCoords)
			#ENDIF
			scriptData.iShapeTestStatus = 1
		ELSE
			#IF IS_DEBUG_BUILD
				PRINTLN("[IE_DELIVERY][CUTSCENE] _MAINTAIN_CALCULATING_GARAGE_DOOR_BOUNDS - Failed to start shapetest.")
			#ENDIF
		ENDIF
	
	ELIF scriptData.iShapeTestStatus = 1
	
		INT iHitSomething
		VECTOR vHitPos, vHitNormal
		ENTITY_INDEX entityHit
		
		SHAPETEST_STATUS status = GET_SHAPE_TEST_RESULT(scriptData.shapeTest, iHitSomething, vHitPos, vHitNormal, entityHit)
		
		IF status = SHAPETEST_STATUS_RESULTS_READY
			IF iHitSomething > 0
				// We hit the garage door, probably
				scriptData.iShapeTestStatus = 2
				
				VECTOR vDir = ROTATE_VECTOR_ABOUT_Z_ORTHO(vHitNormal, ROTSTEP_90)
				scriptData.vGaragePos = vHitPos
				scriptData.vGarageBBox[0] = vHitPos - <<0.0, 0.0, 1.5>> + vDir * 2.0
				scriptData.vGarageBBox[1] = vHitPos + <<0.0, 0.0, 1.5>> - vDir * 2.0
				scriptData.fGarageBBoxWidth = 1.0
				
				#IF IS_DEBUG_BUILD
					PRINTLN("[IE_DELIVERY][CUTSCENE] _MAINTAIN_CALCULATING_GARAGE_DOOR_BOUNDS - Hit the garage door baby! BBox: ", scriptData.vGarageBBox[0], " and ", scriptData.vGarageBBox[1])
					
					// Drop that to temp file so we can cache all bboxes
					OPEN_DEBUG_FILE()
					SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("CASE ") SAVE_STRING_TO_DEBUG_FILE(IE_DELIVERY_GET_DROPOFF_ENUM_NAME_FOR_DEBUG(scriptData.eDropoffID)) SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("    vGaragePos = ") SAVE_VECTOR_TO_DEBUG_FILE(scriptData.vGaragePos) SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("    vGarageBBoxMin = ") SAVE_VECTOR_TO_DEBUG_FILE(scriptData.vGarageBBox[0]) SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("    vGarageBBoxMax = ") SAVE_VECTOR_TO_DEBUG_FILE(scriptData.vGarageBBox[1]) SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("    vGarageNormal = ") SAVE_VECTOR_TO_DEBUG_FILE(vHitNormal) SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("    fGarageBBoxWidth = ") SAVE_FLOAT_TO_DEBUG_FILE(scriptData.fGarageBBoxWidth) SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("BREAK")  // SAVE_NEWLINE_TO_DEBUG_FILE()
					CLOSE_DEBUG_FILE()
				#ENDIF
			ELSE
				scriptData.iShapeTestStatus = -1
				#IF IS_DEBUG_BUILD
					PRINTLN("[IE_DELIVERY][CUTSCENE] _MAINTAIN_CALCULATING_GARAGE_DOOR_BOUNDS - Didn't hit anything, weird! Is there no garage in front of us or what?")
				#ENDIF
			ENDIF
		ELIF status = SHAPETEST_STATUS_NONEXISTENT
			scriptData.iShapeTestStatus = 0
			scriptData.shapeTest = INT_TO_NATIVE(SHAPETEST_INDEX, 0)
			#IF IS_DEBUG_BUILD
				PRINTLN("[IE_DELIVERY][CUTSCENE] _MAINTAIN_CALCULATING_GARAGE_DOOR_BOUNDS - Shapetest returned SHAPETEST_STATUS_NONEXISTENT, ressetting.")
			#ENDIF
		ENDIF
	ENDIF
	*/
	
	#IF IS_DEBUG_BUILD
	IF scriptData.bGotGarageBounds
	AND debugData.bDrawGarageBounds
		DRAW_DEBUG_ANGLED_AREA(scriptData.vGarageBBox[0], scriptData.vGarageBBox[1], scriptData.fGarageBBoxWidth, 0, 50, 200, 64)
	ENDIF
	#ENDIF
ENDPROC


/// PURPOSE:
///    Checks if every player in a vehicle is running the same script.
/// RETURNS:
///    Returns true if every player is running this script, else false.
FUNC BOOL IS_EVERYONE_IN_THIS_VEHICLE_RUNNING_THIS_SCRIPT(VEHICLE_INDEX vehIndex)
	IF IS_ENTITY_DEAD(vehIndex)
		RETURN FALSE
	ENDIF
	
	VEHICLE_SEAT eSeat
	PLAYER_INDEX playerIndex
	PED_INDEX pedIndex
	
	INT iSeatCount, iSeat
	iSeatCount = GET_VEHICLE_MODEL_NUMBER_OF_SEATS(GET_ENTITY_MODEL(vehIndex))
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[IE_DELIVERY][CUTSCENE] IS_EVERYONE_IN_THIS_VEHICLE_RUNNING_THIS_SCRIPT - Calling this for vehicle ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(vehIndex)))
	#ENDIF

	REPEAT iSeatCount iSeat
		eSeat = INT_TO_ENUM(VEHICLE_SEAT, iSeat - 1)
		
		IF NOT IS_VEHICLE_SEAT_FREE(vehIndex, eSeat)
			pedIndex = GET_PED_IN_VEHICLE_SEAT(vehIndex, eSeat)
			
			IF NOT IS_ENTITY_DEAD(pedIndex) AND IS_PED_A_PLAYER(pedIndex)
				playerIndex = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedIndex)
				
				#IF IS_DEBUG_BUILD
					PRINTLN("[IE_DELIVERY][CUTSCENE] IS_EVERYONE_IN_THIS_VEHICLE_RUNNING_THIS_SCRIPT - Player: ", GET_PLAYER_NAME(playerIndex), " is in the vehicle.")
				#ENDIF
				
				IF playerIndex != INVALID_PLAYER_INDEX() AND IS_NET_PLAYER_OK(playerIndex)
					// Check if the player is not a participant of this script.
					IF NOT NETWORK_IS_PLAYER_A_PARTICIPANT(playerIndex)
						#IF IS_DEBUG_BUILD
							PRINTLN("[IE_DELIVERY][CUTSCENE] IS_EVERYONE_IN_THIS_VEHICLE_RUNNING_THIS_SCRIPT - Player: ", GET_PLAYER_NAME(playerIndex), " is not running script 'GB_IE_DELIVERY_CUTSCENE'.")
						#ENDIF
					
						RETURN FALSE
					ELSE
						IF playerBD[NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(playerIndex))].eState = IE_DELIVERY_SCRIPT_STATE_LOADING
							#IF IS_DEBUG_BUILD
								PRINTLN("[IE_DELIVERY][CUTSCENE] IS_EVERYONE_IN_THIS_VEHICLE_RUNNING_THIS_SCRIPT - Player: ", GET_PLAYER_NAME(playerIndex), " is running 'GB_IE_DELIVERY_CUTSCENE' but is still loading.")
							#ENDIF
						
							RETURN FALSE
						ELSE
							#IF IS_DEBUG_BUILD
								PRINTLN("[IE_DELIVERY][CUTSCENE] IS_EVERYONE_IN_THIS_VEHICLE_RUNNING_THIS_SCRIPT - Player: ", GET_PLAYER_NAME(playerIndex), " is running script 'GB_IE_DELIVERY_CUTSCENE' and is loaded.")
							#ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT	

	RETURN TRUE
ENDFUNC

/*

/// PURPOSE:
///    Checks if every player in a vehicle is a member of the same gang.
/// RETURNS:
///    Returns true if all players are members of the same gang, else false.
FUNC BOOL IS_EVERYONE_IN_DELIVERED_VEHICLE_MEMBER_OF_SAME_GANG()

	VEHICLE_INDEX vehIndex = scriptData.vehDelivered
	VEHICLE_SEAT eSeat
	
	IF IS_ENTITY_DEAD(vehIndex)
		RETURN FALSE
	ENDIF
	
	PLAYER_INDEX playerIndex
	PED_INDEX pedIndex

	INT iSeatCount, iSeat
	iSeatCount = GET_VEHICLE_MODEL_NUMBER_OF_SEATS(GET_ENTITY_MODEL(vehIndex))

	REPEAT iSeatCount iSeat
		eSeat = INT_TO_ENUM(VEHICLE_SEAT, iSeat - 1)
	
		IF NOT IS_VEHICLE_SEAT_FREE(vehIndex, eSeat)
			pedIndex = GET_PED_IN_VEHICLE_SEAT(vehIndex, eSeat)
			
			IF NOT IS_ENTITY_DEAD(pedIndex) AND IS_PED_A_PLAYER(pedIndex)
				playerIndex = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedIndex)
				
				IF playerIndex != INVALID_PLAYER_INDEX() AND IS_NET_PLAYER_OK(playerIndex)
					
					IF NOT GB_IS_PLAYER_MEMBER_OF_THIS_GANG(playerIndex, GB_GET_THIS_PLAYER_GANG_BOSS(PLAYER_ID()))
						#IF IS_DEBUG_BUILD
							PRINTLN("[IE_DELIVERY][CUTSCENE] IS_EVERYONE_IN_DELIVERED_VEHICLE_MEMBER_OF_SAME_GANG - Player: ", GET_PLAYER_NAME(playerIndex), " isn't a member of the same gang.")
						#ENDIF
						
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT

	RETURN TRUE
ENDFUNC
*/

/// PURPOSE:
///    Checks if any player is trying to get in or out of the mission vehicle.
/// RETURNS:
///    Returns true if a player is getting in or out of the mission vehicle, else false.
FUNC BOOL IE_DELIVERY_IS_ANYONE_TRYING_TO_GET_IN_OR_OUT_OF_THIS_VEHICLE(VEHICLE_INDEX vehIndex)
	IF IS_ENTITY_DEAD(vehIndex)
		RETURN FALSE
	ENDIF

	PLAYER_INDEX playerIndex
	PED_INDEX pedIndex

	INT iPlayerCount
	
	FLOAT fMinDistance = 15.0
	
	VECTOR vVehCoords = GET_ENTITY_COORDS(vehIndex)
	VECTOR vPedCoords

	REPEAT NUM_NETWORK_PLAYERS iPlayerCount
		playerIndex = INT_TO_PLAYERINDEX(iPlayerCount)
		
		IF IS_NET_PLAYER_OK(playerIndex)
			pedIndex = GET_PLAYER_PED(playerIndex)
			vPedCoords = GET_ENTITY_COORDS(pedIndex)
			
			// Only check players who are within 10 units of the vehicle.
			IF VDIST(vPedCoords, vVehCoords) <= fMinDistance
				// Consider entering the vehicle as being in the vehicle.
				IF IS_PED_IN_VEHICLE(pedIndex, vehIndex, TRUE) 
				AND NOT IS_PED_SITTING_IN_VEHICLE(pedIndex, vehIndex)
					IF (NETWORK_IS_PLAYER_A_PARTICIPANT(playerIndex) AND NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(playerIndex))].iBS, BS_PLAYER_BD_IE_DELIVERY_LEAVING_VEH_FOR_CUTSCENE))
					OR (NOT NETWORK_IS_PLAYER_A_PARTICIPANT(playerIndex))
						#IF IS_DEBUG_BUILD
							PRINTLN("[IE_DELIVERY][CUTSCENE] IE_DELIVERY_IS_ANYONE_TRYING_TO_GET_IN_OR_OUT_OF_THIS_VEHICLE - Player: ", GET_PLAYER_NAME(playerIndex), " is trying to get in/out of the vehicle.")
						#ENDIF
						RETURN TRUE
					ELSE
						#IF IS_DEBUG_BUILD
							PRINTLN("[IE_DELIVERY][CUTSCENE] IE_DELIVERY_IS_ANYONE_TRYING_TO_GET_IN_OR_OUT_OF_THIS_VEHICLE - Player: ", GET_PLAYER_NAME(playerIndex), " is trying to get in/out but they do it for the cutscene so it's fine.")
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT

	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if every player in the mission vehicle can perform the mission delivery.
/// RETURNS:
///    Returns true if all players can perform the delivery, else false.
FUNC BOOL CAN_EVERYONE_IN_THIS_VEHICLE_DO_DELIVERY(VEHICLE_INDEX vehIndex)
	// Check if the entity is dead.
	IF IS_ENTITY_DEAD(vehIndex)
		#IF IS_DEBUG_BUILD
			PRINTLN("[IE_DELIVERY][CUTSCENE] CAN_EVERYONE_IN_THIS_VEHICLE_DO_DELIVERY - Can't do delivery, entity is dead.")
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	// Check if this player is in our gang.
	/*
	IF NOT IS_EVERYONE_IN_DELIVERED_VEHICLE_MEMBER_OF_SAME_GANG()
		#IF IS_DEBUG_BUILD
			PRINTLN("[IE_DELIVERY][CUTSCENE] CAN_EVERYONE_IN_THIS_VEHICLE_DO_DELIVERY - Can't do delivery, not everyone is member of same gang.")
		#ENDIF
	
		RETURN FALSE
	ENDIF
	*/
	
	// Check if any player is getting in or out of the vehicle.
	IF IE_DELIVERY_IS_ANYONE_TRYING_TO_GET_IN_OR_OUT_OF_THIS_VEHICLE(vehIndex)
		#IF IS_DEBUG_BUILD
			PRINTLN("[IE_DELIVERY][CUTSCENE] CAN_EVERYONE_IN_THIS_VEHICLE_DO_DELIVERY - Can't do delivery, player is trying to get in/out of vehicle.")
		#ENDIF
		
		RETURN FALSE
	ENDIF

	RETURN TRUE
ENDFUNC

FUNC BOOL IE_DELIVERY_SHOULD_CUTSCENE_BE_TRIGGERED()
	IF scriptData.vehDelivered = NULL
	#IF IS_DEBUG_BUILD
	// Dont trigger if triggering is disabled, unless we debug triggered the cutscene itself, duh
	OR ((debugData.bDontTriggerCutscene) AND NOT (scriptData.cutscene.bPlaying AND scriptData.cutscene.dbg.bPlayedViaDebug))
	#ENDIF
		RETURN FALSE
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF scriptData.cutscene.bPlaying
	AND scriptData.cutscene.dbg.bPlayedViaDebug
		RETURN TRUE
	ENDIF
	#ENDIF
	
	// Proxy veh is either cargobob or towtruck, if any of these deliveries happen then we need to do checks for both - delivered and proxy
	BOOL bHasProxyVeh
	PED_INDEX pedDeliverer = GET_PLAYER_PED(scriptData.pDeliverer)
	VEHICLE_INDEX vehProxy
	IF NOT IS_ENTITY_DEAD(pedDeliverer)
		IF IS_PED_IN_ANY_VEHICLE(pedDeliverer)
			vehProxy = GET_VEHICLE_PED_IS_IN(pedDeliverer)
			IF NOT IS_ENTITY_DEAD(vehProxy)
			AND vehProxy != scriptData.vehDelivered
				bHasProxyVeh = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_EVERYONE_IN_THIS_VEHICLE_RUNNING_THIS_SCRIPT(scriptData.vehDelivered)
	AND ((NOT bHasProxyVeh) OR (bHasProxyVeh AND IS_EVERYONE_IN_THIS_VEHICLE_RUNNING_THIS_SCRIPT(vehProxy)))
		IF CAN_EVERYONE_IN_THIS_VEHICLE_DO_DELIVERY(scriptData.vehDelivered)
		AND ((NOT bHasProxyVeh) OR (bHasProxyVeh AND CAN_EVERYONE_IN_THIS_VEHICLE_DO_DELIVERY(vehProxy)))
			IF g_IEDeliveryData.bStartCutscene // Set when deliverer sends an event to start
				RETURN TRUE
			ELSE
				#IF IS_DEBUG_BUILD
					PRINTLN("[IE_DELIVERY][CUTSCENE] IE_DELIVERY_SHOULD_CUTSCENE_BE_TRIGGERED - Waiting for g_IEDeliveryData.bStartCutscene.")
				#ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
				PRINTLN("[IE_DELIVERY][CUTSCENE] IE_DELIVERY_SHOULD_CUTSCENE_BE_TRIGGERED - CAN_EVERYONE_IN_THIS_VEHICLE_DO_DELIVERY returns FALSE.")
			#ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
			PRINTLN("[IE_DELIVERY][CUTSCENE] IE_DELIVERY_SHOULD_CUTSCENE_BE_TRIGGERED - IS_EVERYONE_IN_DELIVERED_VEHICLE_RUNNING_THIS_SCRIPT returns FALSE.")
		#ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC _SET_INITIAL_STATE_FOR_ALL_ASSETS()
	
	INT i
	VECTOR vCoords, vRot
	FLOAT fHeading
	PED_TYPE pedTypeFoo
	MODEL_NAMES modelNameFoo
	TEXT_LABEL_63 textLabelFoo
	
	// Place veh clone at its correct position
	IE_DELIVERY_GET_DROPOFF_VEHICLE_CLONE_DATA(scriptData.eDropoffID, vCoords, fHeading)
	IF DOES_ENTITY_EXIST(cutsceneData.vehClone) AND NOT IS_ENTITY_DEAD(cutsceneData.vehClone)
		IF NOT scriptData.bViaCargobob
			vCoords = TRANSFORM_CUTSCENE_COORDS_TO_WORLD_COORDS(vCoords)
			fHeading = TRANSFORM_CUTSCENE_HEADING_TO_WORLD_HEADING(fHeading)
			
			SET_ENTITY_COORDS_NO_OFFSET(cutsceneData.vehClone, vCoords)
			SET_ENTITY_HEADING(cutsceneData.vehClone, fHeading)
			SET_VEHICLE_ON_GROUND_PROPERLY(cutsceneData.vehClone)
			FREEZE_ENTITY_POSITION(cutsceneData.vehClone, FALSE)
			SET_ENTITY_COLLISION(cutsceneData.vehClone, TRUE)
		ENDIF
	ENDIF
	
	// Place actor peds at start position
	REPEAT COUNT_OF(cutsceneData.pedActors) i
		IF DOES_ENTITY_EXIST(cutsceneData.pedActors[i]) AND NOT IS_ENTITY_DEAD(cutsceneData.pedActors[i])
			IF NOT IS_PED_IN_ANY_VEHICLE(cutsceneData.pedActors[i])
				IF IE_DELIVERY_GET_DROPOFF_PED_DATA(scriptData.eDropoffID, i, pedTypeFoo, modelNameFoo, vCoords, vRot, textLabelFoo, scriptData.bViaCargobob)
					vCoords = TRANSFORM_CUTSCENE_COORDS_TO_WORLD_COORDS(vCoords)
					vRot.Z = TRANSFORM_CUTSCENE_HEADING_TO_WORLD_HEADING(vRot.Z)
					SET_ENTITY_COORDS(cutsceneData.pedActors[i], vCoords)
					SET_ENTITY_HEADING(cutsceneData.pedActors[i], vRot.Z)
					SET_ENTITY_COLLISION(cutsceneData.pedActors[i], TRUE)
				ENDIF
				FREEZE_ENTITY_POSITION(cutsceneData.pedActors[i], FALSE)
				//FORCE_PED_AI_AND_ANIMATION_UPDATE(cutsceneData.pedActors[i])
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Place veh actors
	REPEAT COUNT_OF(cutsceneData.vehActors) i
		IF DOES_ENTITY_EXIST(cutsceneData.vehActors[i]) AND NOT IS_ENTITY_DEAD(cutsceneData.vehActors[i])
			IF IE_DELIVERY_GET_DROPOFF_VEHICLE_ACTOR_DATA(scriptData.eDropoffID, i, modelNameFoo, vCoords, vRot)
				vCoords = TRANSFORM_CUTSCENE_COORDS_TO_WORLD_COORDS(vCoords)
				vRot.Z = TRANSFORM_CUTSCENE_HEADING_TO_WORLD_HEADING(vRot.Z)
				SET_ENTITY_COORDS_NO_OFFSET(cutsceneData.vehActors[i], vCoords)
				SET_ENTITY_HEADING(cutsceneData.vehActors[i], vRot.Z)
				SET_VEHICLE_ON_GROUND_PROPERLY(cutsceneData.vehActors[i])
			ENDIF
			FREEZE_ENTITY_POSITION(cutsceneData.vehActors[i], FALSE)
			SET_ENTITY_COLLISION(cutsceneData.vehActors[i], TRUE)
		ENDIF
	ENDREPEAT
			
	// Enable collisions on props
	REPEAT COUNT_OF(cutsceneData.objProps) i
		IF DOES_ENTITY_EXIST(cutsceneData.objProps[i])
			SET_ENTITY_VISIBLE(cutsceneData.objProps[i], TRUE)
			SET_ENTITY_COLLISION(cutsceneData.objProps[i], TRUE)
		ENDIF
	ENDREPEAT
	
	// Place peds at starting positions
	IF scriptData.dropoffType = IE_DELIVERY_DROPOFF_CUTSCENE_TYPE_GOON_PICKUP
	AND NOT scriptData.bViaCargobob
		IF DOES_ENTITY_EXIST(cutsceneData.vehClone)
		AND NOT IS_ENTITY_DEAD(cutsceneData.vehClone)
			VECTOR vDir, vGotoMeetPoint
			
			IF scriptData.eDropoffID = IE_DROPOFF_GANGSTER_2
				vGotoMeetPoint = <<-1923.4720, 2050.7021, 139.7345>> // where peds come to meet
			ELIF scriptData.eDropoffID = IE_DROPOFF_GANGSTER_3
				vGotoMeetPoint = <<-63.7923, 898.4977, 234.7299>> // where peds come to meet
			ENDIF
			
			// Tell ped who picks up to go to the meeting point
			IF DOES_ENTITY_EXIST(cutsceneData.pedActors[0])
			AND NOT IS_ENTITY_DEAD(cutsceneData.pedActors[0])
				TASK_FOLLOW_NAV_MESH_TO_COORD(cutsceneData.pedActors[0], vGotoMeetPoint, PEDMOVE_WALK * 0.7, DEFAULT, DEFAULT, DEFAULT, 167.9574)
			ENDIF
				
			REPEAT COUNT_OF(cutsceneData.pedPlayerActors) i
				IF NOT DOES_ENTITY_EXIST(cutsceneData.pedPlayerActors[i])
					RELOOP
				ENDIF
				
				#IF IS_DEBUG_BUILD
					PRINTLN("[IE_DELIVERY][CUTSCENE] _SET_INITIAL_STATE_FOR_ALL_ASSETS - Putting peds at car entry point ", i) 
				#ENDIF
						
				// Calculate the heading so they are standing outwards
				vDir = GET_HEADING_AS_VECTOR(GET_ENTITY_HEADING(cutsceneData.vehClone))
				IF cutsceneData.vsPedPlayers[i] = VS_DRIVER
				OR cutsceneData.vsPedPlayers[i] = VS_BACK_LEFT
					vDir = ROTATE_VECTOR_ABOUT_Z_ORTHO(vDir, ROTSTEP_90)
				ELSE
					vDir = ROTATE_VECTOR_ABOUT_Z_ORTHO(vDir, ROTSTEP_NEG_90)
				ENDIF
					
				IF i = 0
					IF scriptData.eDropoffID = IE_DROPOFF_GANGSTER_2
						fHeading = 75.7891
					ELIF scriptData.eDropoffID = IE_DROPOFF_GANGSTER_3
						fHeading = 283.1446
					ENDIF
				ELSE
					fHeading = GET_HEADING_FROM_VECTOR_2D(vDir.X, vDir.Y)
				ENDIF
				
				vCoords = GET_ENTRY_POINT_POSITION(cutsceneData.vehClone, ENUM_TO_INT(cutsceneData.vsPedPlayers[i]) + 1) //+ vDir * PICK_FLOAT(i = 0, 1.5, 0.5)
				GET_GROUND_Z_FOR_3D_COORD(vCoords, vCoords.Z)
				
				SET_ENTITY_COORDS(cutsceneData.pedPlayerActors[i], vCoords)
				SET_ENTITY_HEADING(cutsceneData.pedPlayerActors[i], fHeading)
				SET_ENTITY_COLLISION(cutsceneData.pedPlayerActors[i], TRUE)
				FREEZE_ENTITY_POSITION(cutsceneData.pedPlayerActors[i], FALSE)
					
				IF i = 0
					// Player goes to the meeting point in this cutscene
					TASK_FOLLOW_NAV_MESH_TO_COORD(cutsceneData.pedPlayerActors[0], vGotoMeetPoint, PEDMOVE_WALK * 0.7)
				ELSE
					//TASK_GO_STRAIGHT_TO_COORD(cutsceneData.pedPlayerActors[i], vCoords + vDir * 10.0, PEDMOVE_WALK)
					//SET_ENTITY_VISIBLE(cutsceneData.pedPlayerActors[i], FALSE)
					TASK_START_SCENARIO_IN_PLACE(cutsceneData.pedPlayerActors[i], "WORLD_HUMAN_HANG_OUT_STREET", 0, TRUE)
					//FORCE_PED_AI_AND_ANIMATION_UPDATE(cutsceneData.pedPlayerActors[i], TRUE, TRUE)
				ENDIF
				
				//FORCE_PED_AI_AND_ANIMATION_UPDATE(cutsceneData.pedPlayerActors[i])
			ENDREPEAT
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IE_DELIVERY_IS_EVERYTHING_READY_FOR_CUTSCENE_START()
	INT i
	REPEAT COUNT_OF(cutsceneData.pedPlayerActors) i
		IF DOES_ENTITY_EXIST(cutsceneData.pedPlayerActors[i]) AND NOT IS_ENTITY_DEAD(cutsceneData.pedPlayerActors[i])
			IF NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(cutsceneData.pedPlayerActors[i])
				#IF IS_DEBUG_BUILD
					PRINTLN("[IE_DELIVERY][CUTSCENE] IE_DELIVERY_IS_EVERYTHING_READY_FOR_CUTSCENE_START - Waiting for pedPlayerActor[", i, "] to load.")
				#ENDIF
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF scriptData.eDropoffID = IE_DROPOFF_SHOWROOM_6
		IF IS_BIT_SET(scriptData.iBS, BS_IE_DELIVERY_IPLS_ARE_LOADED)
			IF NOT IS_BIT_SET(scriptData.iBS, BS_IE_DELIVERY_STARTED_LOAD_SCENE)
				IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
					IF NEW_LOAD_SCENE_START_SPHERE(<<-30.2236, -1090.0377, 26.7153>>, 5.0)
						#IF IS_DEBUG_BUILD
							PRINTLN("[IE_DELIVERY][CUTSCENE] IE_DELIVERY_IS_EVERYTHING_READY_FOR_CUTSCENE_START - Started new load scene...")
						#ENDIF
						SET_BIT(scriptData.iBS, BS_IE_DELIVERY_STARTED_LOAD_SCENE)
						
						RETURN FALSE
					ENDIF
				ENDIF
			ELSE
				IF IS_NEW_LOAD_SCENE_ACTIVE()
					IF NOT IS_NEW_LOAD_SCENE_LOADED()
						NEW_LOAD_SCENE_STOP()
						
						#IF IS_DEBUG_BUILD
							PRINTLN("[IE_DELIVERY][CUTSCENE] IE_DELIVERY_IS_EVERYTHING_READY_FOR_CUTSCENE_START - Waiting for load scene to load...")
						#ENDIF
						
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
				PRINTLN("[IE_DELIVERY][CUTSCENE] IE_DELIVERY_IS_EVERYTHING_READY_FOR_CUTSCENE_START - Waiting for IPLs to load.")
			#ENDIF
			
			RETURN FALSE
		ENDIF
		
		CLEAR_BIT(scriptData.iBS, BS_IE_DELIVERY_STARTED_LOAD_SCENE)
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC SET_ALL_IE_DELIVERY_ASSETS_VISIBLE(BOOL bVisible)
	IF DOES_ENTITY_EXIST(cutsceneData.vehClone)
	AND NOT IS_ENTITY_DEAD(cutsceneData.vehClone)
		SET_ENTITY_VISIBLE(cutsceneData.vehClone, bVisible)
	ENDIF
	
	INT i
	REPEAT COUNT_OF(cutsceneData.vehActors) i
		IF DOES_ENTITY_EXIST(cutsceneData.vehActors[i])
			SET_ENTITY_VISIBLE(cutsceneData.vehActors[i], bVisible)
		ENDIF
	ENDREPEAT
	
	REPEAT COUNT_OF(cutsceneData.pedPlayerActors) i
		IF DOES_ENTITY_EXIST(cutsceneData.pedPlayerActors[i])
			SET_ENTITY_VISIBLE(cutsceneData.pedPlayerActors[i], bVisible)
		ENDIF
	ENDREPEAT
	
	REPEAT COUNT_OF(cutsceneData.pedActors) i
		IF DOES_ENTITY_EXIST(cutsceneData.pedActors[i])
			SET_ENTITY_VISIBLE(cutsceneData.pedActors[i], bVisible)
		ENDIF
	ENDREPEAT
	
	IF DOES_ENTITY_EXIST(cutsceneData.vehProxyClone)
	AND NOT IS_ENTITY_DEAD(cutsceneData.vehProxyClone)
		SET_ENTITY_VISIBLE(cutsceneData.vehProxyClone, bVisible)
	ENDIF
	
	IF scriptData.eDropoffID = IE_DROPOFF_SHOWROOM_6
		CREATE_MODEL_HIDE(<<-29.32, -1086.63, 26.95>>, 1.0, INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("v_15_garg_delta_doordown")), FALSE)
	ENDIF
ENDPROC

PROC IE_DELIVERY_CUTSCENE_CREATE_ALL_ASSETS()
	_CREATE_IE_DELIVERY_CUTSCENE_VEH_CLONE_()
	_CREATE_IE_DELIVERY_PED_PLAYER_ACTORS_()
	_CREATE_IE_DELIVERY_CUTSCENE_PROXY_VEH_()
	_CREATE_IE_DELIVERY_CUTSCENE_VEH_ACTORS_()
	_CREATE_IE_DELIVERY_CUTSCENE_PED_ACTORS_()
	_CREATE_IE_DELIVERY_CUTSCENE_PROPS_()
	_CREATE_IE_DELIVERY_CARGOBOB_MECHANIC_()
	
	IF scriptData.eDropoffID = IE_DROPOFF_SHOWROOM_6
		IE_DELIVERY_SET_IPL_ON(TRUE)
	ENDIF
	
	SET_ALL_IE_DELIVERY_ASSETS_VISIBLE(FALSE)
ENDPROC

PROC IE_DELIVERY_START_CUTSCENE()
	#IF IS_DEBUG_BUILD
		PRINTLN("[IE_DELIVERY][CUTSCENE] IE_DELIVERY_START_CUTSCENE - Called this.")
	#ENDIF

	IF NOT scriptData.cutscene.bPlaying
		SIMPLE_CUTSCENE_START(scriptData.cutscene)
		
		#IF IS_DEBUG_BUILD
			PRINTLN("[IE_DELIVERY][CUTSCENE] IE_DELIVERY_START_CUTSCENE - Starting...")
		#ENDIF
	ENDIF
ENDPROC

PROC IE_DELIVERY_CONTROL_VEH_CLONE_IN_DOCKS(INT iShiftScene = 0)

	IF NOT DOES_ENTITY_EXIST(cutsceneData.vehClone) OR IS_ENTITY_DEAD(cutsceneData.vehClone)
		EXIT
	ENDIF
	
	// This will make the car drive in a straight line
	IF scriptData.cutscene.iCurrentScene = 0 + iShiftScene
		_IE_DELIVERY_PLAY_AUDIO_STREAM(scriptData.sDeliveryAudioStream)
		FLOAT fVehicleSpeed = 1.5
		FLOAT fCloneHeading

		VECTOR vStartPos = <<0.0, 0.0, 0.0>>

		IE_DELIVERY_GET_DROPOFF_VEHICLE_CLONE_DATA(scriptData.eDropoffID, vStartPos, fCloneHeading)

		IF IS_BIT_SET(cutsceneData.iBS ,BS_IE_DELIVERY_CUTSCENE_REVERSE_CHECK)
			fVehicleSpeed = -fVehicleSpeed
		ENDIF
		
		IF DOES_ENTITY_EXIST(cutsceneData.vehClone)
			IF NOT IS_ENTITY_DEAD(cutsceneData.pedPlayerActors[0])
				// Fix to prevent bike from adjusting its heading.
				IF IS_PED_ON_ANY_BIKE(cutsceneData.pedPlayerActors[0])
					SET_BIKE_ON_STAND(cutsceneData.vehClone, 0.0, 0.0)
				ENDIF
			ENDIF
			
			SET_VEHICLE_HANDBRAKE(cutsceneData.vehClone, FALSE)
			SET_VEHICLE_BRAKE(cutsceneData.vehClone, 0.0)
			SET_VEHICLE_FORWARD_SPEED(cutsceneData.vehClone, fVehicleSpeed)
			
			VECTOR vRot = GET_ENTITY_ROTATION(cutsceneData.vehClone)
			vRot.z = fCloneHeading
			SET_ENTITY_ROTATION(cutsceneData.vehClone, vRot)
		ENDIF
	ENDIF
ENDPROC

PROC IE_DELIVERY_CONTROL_VEH_CLONE_ON_RAMP(INT iShiftScene = 0)
	IF NOT DOES_ENTITY_EXIST(cutsceneData.vehClone)
	OR IS_ENTITY_DEAD(cutsceneData.vehClone)
		EXIT
	ENDIF
	
	INT iTotalSceneTime = scriptData.cutscene.sScenes[scriptData.cutscene.iCurrentScene].iDuration
	FLOAT fTimeFactor = FMIN(TO_FLOAT(scriptData.cutscene.iCurrentSceneElapsedTime + 1000) / TO_FLOAT(iTotalSceneTime - 1000), 1.0)
	FLOAT fInitialSpeed = 0.0
	FLOAT fMaxSpeed = 2.2
	
	IF NOT scriptData.bViaCargobob
		IF scriptData.eDropoffID = IE_DROPOFF_PRIVATE_BUYER_5
		OR scriptData.eDropoffID = IE_DROPOFF_SHOWROOM_2
		OR scriptData.eDropoffID = IE_DROPOFF_SHOWROOM_3
			iShiftScene += 1
			fInitialSpeed = 1.0
			fMaxSpeed = 2.3
		ENDIF
	ENDIF
	
	IF SIMPLE_CUTSCENE_IS_SCENE_RUNNING(scriptData.cutscene, 0 + iShiftScene)
		IF NOT IS_BIT_SET(cutsceneData.iBS, 0)
			SET_VEHICLE_ENGINE_ON(cutsceneData.vehClone, TRUE, TRUE)
			SET_BIT(cutsceneData.iBS, 0)
		ENDIF
		
		SET_VEHICLE_HANDBRAKE(cutsceneData.vehClone, FALSE)
		SET_VEHICLE_BRAKE(cutsceneData.vehClone, 0.0)
		SET_VEHICLE_FORWARD_SPEED(cutsceneData.vehClone, fInitialSpeed + (fMaxSpeed - fInitialSpeed) * GET_GRAPH_TYPE_SIN_ACCEL_DECEL(fTimeFactor))
		
	ELIF SIMPLE_CUTSCENE_IS_SCENE_RUNNING(scriptData.cutscene, 1 + iShiftScene)
		
		IF NOT IS_BIT_SET(cutsceneData.iBS, 8)
			VECTOR vNewPos
			vNewPos = TRANSFORM_CUTSCENE_COORDS_TO_WORLD_COORDS(<<0.0, 0.8, 0.0>>)
			SET_ENTITY_COORDS(cutsceneData.vehClone, vNewPos)
			SET_BIT(cutsceneData.iBS, 8)
		ENDIF
		
		SET_VEHICLE_HANDBRAKE(cutsceneData.vehClone, FALSE)
		SET_VEHICLE_BRAKE(cutsceneData.vehClone, 0.0)
		SET_VEHICLE_FORWARD_SPEED(cutsceneData.vehClone, 2.2)
	ENDIF
ENDPROC

PROC _ALL_PED_ACTORS_LEAVE()
	INT i
	REPEAT COUNT_OF(cutsceneData.pedPlayerActors) i
		IF DOES_ENTITY_EXIST(cutsceneData.pedPlayerActors[i])
		AND NOT IS_ENTITY_DEAD(cutsceneData.pedPlayerActors[i])
			TASK_LEAVE_VEHICLE(cutsceneData.pedPlayerActors[i], cutsceneData.vehClone)
		ENDIF
	ENDREPEAT
ENDPROC

PROC IE_DELIVERY_CONTROL_VEH_CLONE_IN_CARPARK()
	IF SIMPLE_CUTSCENE_IS_SCENE_RUNNING(scriptData.cutscene, 0)
		IF NOT IS_BIT_SET(cutsceneData.iBS, BS_IE_DELIVERY_CUTSCENE_TASK_VEHICLE)
			IF GET_SCRIPT_TASK_STATUS(cutsceneData.pedPlayerActors[0], SCRIPT_TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING) != PERFORMING_TASK
				IF GET_IS_WAYPOINT_RECORDING_LOADED(scriptData.sWaypointRecording) 					
					// Task vehicle to follow the waypoint recording.
					CLEAR_PED_TASKS(cutsceneData.pedPlayerActors[0])
					TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(cutsceneData.pedPlayerActors[0], cutsceneData.vehClone, scriptData.sWaypointRecording, DRIVINGMODE_PLOUGHTHROUGH, 0, EWAYPOINT_START_FROM_CLOSEST_POINT, -1, 60.0, FALSE, 0.75)		
				ENDIF
			ENDIF

			SET_BIT(cutsceneData.iBS, BS_IE_DELIVERY_CUTSCENE_TASK_VEHICLE)
		ENDIF
		
		IF scriptData.eDropoffID = IE_DROPOFF_POLICE_STATION_5
			VEHICLE_WAYPOINT_PLAYBACK_OVERRIDE_SPEED(cutsceneData.vehClone, 8.0)
			IF NOT IS_BIT_SET(cutsceneData.iBS, 15)
				IF scriptData.cutscene.iTotalElapsedTime > 4500
					_ALL_PED_ACTORS_LEAVE()
					SET_BIT(cutsceneData.iBS, 15)
				ENDIF
			ENDIF
		ELIF scriptData.eDropoffID = IE_DROPOFF_POLICE_STATION_7
		OR scriptData.eDropoffID = IE_DROPOFF_POLICE_STATION_8
			VEHICLE_WAYPOINT_PLAYBACK_OVERRIDE_SPEED(cutsceneData.vehClone, 8.0)
			IF NOT IS_BIT_SET(cutsceneData.iBS, 15)
				IF scriptData.cutscene.iTotalElapsedTime > 3500
					_ALL_PED_ACTORS_LEAVE()
					SET_BIT(cutsceneData.iBS, 15)
				ENDIF
			ENDIF
		ELIF scriptData.eDropoffID = IE_DROPOFF_POLICE_STATION_9
			VEHICLE_WAYPOINT_PLAYBACK_OVERRIDE_SPEED(cutsceneData.vehClone, 7.0)
			IF NOT IS_BIT_SET(cutsceneData.iBS, 15)
				IF scriptData.cutscene.iTotalElapsedTime > 4500
					_ALL_PED_ACTORS_LEAVE()
					SET_BIT(cutsceneData.iBS, 15)
				ENDIF
			ENDIF
		ENDIF
	ELIF SIMPLE_CUTSCENE_IS_SCENE_RUNNING(scriptData.cutscene, 1)
		// Task ped to exit the vehicle via the driver door. 
		CLEAR_PED_TASKS(cutsceneData.pedPlayerActors[0])
		TASK_LEAVE_VEHICLE(cutsceneData.pedPlayerActors[0], cutsceneData.vehClone, ECF_USE_LEFT_ENTRY)
	ENDIF
ENDPROC

PROC IE_DELIVERY_CONTROL_VEH_CLONE_IN_GARAGE()
	IF NOT DOES_ENTITY_EXIST(cutsceneData.vehClone)
	OR IS_ENTITY_DEAD(cutsceneData.vehClone)
		EXIT
	ENDIF
	
	// The only way to look like the car is driving in the garage is to
	// disable the collision on the car which makes it impossible to use
	// tasks/forward speed on the car. Position needs to be set manually.
	// This is the same solution as garages in SP.
	
	IF scriptData.cutscene.iCurrentScene = 0
		INT iCarAnimDelay = 100
		
		IF scriptData.bIsUrgent
			iCarAnimDelay = -1000
		ENDIF
		
		IF scriptData.cutscene.iCurrentSceneElapsedTime > iCarAnimDelay
			INT iTotalSceneTime = scriptData.cutscene.sScenes[scriptData.cutscene.iCurrentScene].iDuration - 1500
			
			FLOAT fTimeFactor = FMIN(TO_FLOAT(scriptData.cutscene.iCurrentSceneElapsedTime) / TO_FLOAT(iTotalSceneTime - iCarAnimDelay), 1.0)
			FLOAT fCloneHeading
			
			VECTOR vStartPos = <<0.0, 0.0, 0.0>>

			IE_DELIVERY_GET_DROPOFF_VEHICLE_CLONE_DATA(scriptData.eDropoffID, vStartPos, fCloneHeading)
			fCloneHeading = TRANSFORM_CUTSCENE_HEADING_TO_WORLD_HEADING(fCloneHeading)
			
			VECTOR vDirection = GET_HEADING_AS_VECTOR(fCloneHeading)
			VECTOR vNewPos = vStartPos + (vDirection) * 8.0 * GET_GRAPH_TYPE_SIN_ACCEL_DECEL(fTimeFactor)
			VECTOR vFakeGaragePlayerCarCoords = GET_ENTITY_COORDS(cutsceneData.vehClone)
	
			FLOAT fFakeVehicleOffset = vFakeGaragePlayerCarCoords.Z - vStartPos.Z
			
			
			SET_ENTITY_COORDS_NO_OFFSET(cutsceneData.vehClone, vNewPos + <<0.0, 0.0, fFakeVehicleOffset>>)
			SET_ENTITY_COLLISION(cutsceneData.vehClone, FALSE)
			FREEZE_ENTITY_POSITION(cutsceneData.vehClone, TRUE)
		ENDIF
		
	ELIF scriptData.cutscene.iCurrentScene = 1
		SET_ENTITY_VISIBLE(cutsceneData.vehClone, FALSE)
	ENDIF
ENDPROC

PROC _GET_FROZEN_CAR_START_AND_END_COORDS(VECTOR &vStartPos, VECTOR &vEndPos)
	SWITCH scriptData.eDropoffID
		CASE IE_DROPOFF_PRIVATE_BUYER_1 // Richard's Majestic
			vStartPos = <<-1081.548, -501.981, 36.150>>
			vEndPos = <<-1079.388, -506.331, 36.150>>
		BREAK
		CASE IE_DROPOFF_PRIVATE_BUYER_2 // Barn
			vStartPos = <<162.162, 2286.065, 93.765>>
			vEndPos = <<158.442, 2287.445, 93.765>>
		BREAK
		CASE IE_DROPOFF_PRIVATE_BUYER_3 // Fort Zancudo
			vStartPos = <<-2221.103, 3485.819, 29.748>>
			vEndPos = <<-2221.103, 3491.039, 29.748>>
		BREAK
		CASE IE_DROPOFF_PRIVATE_BUYER_4
			vStartPos = <<2906.2375, 4347.4976, 49.9395>>
			vEndPos = <<2904.5361, 4351.4268, 49.9361>>
		BREAK
		CASE IE_DROPOFF_SHOWROOM_1
			vStartPos = <<475.700, -1889.710, 25.734>>
			vEndPos = <<471.650, -1891.570, 25.734>>
		BREAK
		CASE IE_DROPOFF_SHOWROOM_4
			vStartPos = <<-219.9556, 6247.3882, 31.1215>>
			vEndPos = <<-216.8938, 6244.4375, 31.0919>>
		BREAK
		CASE IE_DROPOFF_GANGSTER_4
			vStartPos = <<1407.605, 1115.883, 114.477>>
			vEndPos = <<1407.605, 1111.623, 114.477>>
		BREAK
		CASE IE_DROPOFF_SHOWROOM_5
			vStartPos = <<-64.683, 80.942, 71.191>>
			vEndPos = <<-60.093, 78.932, 71.191>>
		BREAK
		CASE IE_DROPOFF_SHOWROOM_7
			vStartPos = <<2511.0786, 4109.6479, 38.2128>>
			vEndPos = <<2515.9521, 4107.3647, 38.0286>>
		BREAK
		CASE IE_DROPOFF_SHOWROOM_8
			vStartPos = <<-764.9470, -239.8099, 36.9093>>
			vEndPos = <<-766.6998, -235.6782, 36.9594>>
		BREAK
		CASE IE_DROPOFF_GANGSTER_5
			vStartPos = <<-1786.982, 455.994, 127.947>>
			vEndPos = <<-1783.232, 457.104, 127.947>>
		BREAK
		CASE IE_DROPOFF_GANGSTER_6
			vStartPos = <<218.969, 756.472, 204.299>>
			vEndPos = <<222.959, 754.192, 204.299>>
		BREAK
		CASE IE_DROPOFF_GANGSTER_8
			vStartPos = <<-3103.7524, 716.0079, 19.9521>>
			vEndPos = <<-3107.9734, 714.8845, 19.6913>>
		BREAK
		CASE IE_DROPOFF_WAREHOUSE_1
			vStartPos = <<-633.2554, -1779.9338, 23.0357>>
			vEndPos = <<-628.9778, -1776.8186, 23.7107>>
		BREAK
		CASE IE_DROPOFF_WAREHOUSE_2
			vStartPos = <<995.5071, -1856.0997, 29.8898>>
			vEndPos = <<995.7976, -1851.0636, 30.5184>>
		BREAK
		CASE IE_DROPOFF_WAREHOUSE_3
			vStartPos = <<-70.9211, -1822.1987, 25.9420>>
			vEndPos = <<-74.9782, -1818.8802, 26.5844>>
		BREAK
		CASE IE_DROPOFF_WAREHOUSE_4
			vStartPos = <<38.4329, -1283.9270, 28.2756>>
			vEndPos = <<33.0802, -1284.0748, 28.9409>>
		BREAK
		CASE IE_DROPOFF_WAREHOUSE_5
			vStartPos = <<1211.8726, -1262.5488, 34.2267>>
			vEndPos = <<1217.3022, -1262.4325, 34.3762>>
		BREAK
		CASE IE_DROPOFF_WAREHOUSE_6
			vStartPos = <<804.452, -2222.441, 29.121>>
			vEndPos = <<805.038, -2216.462, 29.024>>
		BREAK
		CASE IE_DROPOFF_WAREHOUSE_7
			vStartPos = <<1764.2811, -1647.1696, 111.6415>>
			vEndPos = <<1763.3785, -1642.4008, 112.2949>>
		BREAK
		CASE IE_DROPOFF_WAREHOUSE_8
			vStartPos = <<144.2729, -3003.7192, 6.0309>>
			vEndPos = <<144.3404, -3009.1025, 6.6804>>
		BREAK
		CASE IE_DROPOFF_WAREHOUSE_9
			vStartPos = <<-513.8715, -2200.7854, 5.3940>>
			vEndPos = <<-517.2565, -2204.8169, 5.3940>>
		BREAK
		CASE IE_DROPOFF_WAREHOUSE_10
			vStartPos = <<-1153.3678, -2172.3555, 12.2628>>
			vEndPos = <<-1149.0400, -2168.0493, 12.8355>>
		BREAK
		CASE IE_DROPOFF_SCRAPYARD_3
			vStartPos = <<-192.926, 6267.242, 30.3994>>
			vEndPos = <<-190.503, 6264.698, 30.3994>>
		BREAK
		CASE IE_DROPOFF_SCRAPYARD_1
			vStartPos = <<-510.4685, -1735.8154, 18.1364>>
			vEndPos = <<-513.0537, -1739.4868, 18.1364>>
		BREAK
	ENDSWITCH
ENDPROC

PROC _GET_FROZEN_CAR_START_AND_END_COORDS_CARGOBOB(VECTOR &vStartPos, VECTOR &vEndPos)
	_GET_FROZEN_CAR_START_AND_END_COORDS(vStartPos, vEndPos)
	
	SWITCH scriptData.eDropoffID
		CASE IE_DROPOFF_GANGSTER_8
			vStartPos = <<-3098.8669, 716.9344, 20.2340>>
			vEndPos = <<-3107.9641, 714.7106, 20.2340>>
		BREAK
		CASE IE_DROPOFF_PRIVATE_BUYER_1
			vStartPos = <<-1083.3877, -497.7273, 36.0610>>
			vEndPos = <<-1079.0071, -505.5269, 36.0610>>
		BREAK
		CASE IE_DROPOFF_PRIVATE_BUYER_4
			vStartPos = <<2908.0237, 4344.4521, 49.9700>>
			vEndPos = <<2904.5664, 4351.5947, 49.9700>>
		BREAK
		CASE IE_DROPOFF_SHOWROOM_7
			vStartPos = <<2504.7517, 4112.5894, 38.0785>>
			vEndPos = <<2515.7529, 4107.3784, 38.0785>>
		BREAK
		CASE IE_DROPOFF_SHOWROOM_8
			vStartPos = <<-762.6353, -248.2387, 36.6700>>
			vEndPos = <<-766.7435, -236.4778, 36.3700>>
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL TASK_HELI_FLY_AWAY(PED_INDEX pedHeliPilot, VEHICLE_INDEX vehHeli)
	VECTOR vStartLocation, vFinishLocation
	FLOAT fStartHeading, fSpeed 
	INT iHeightAboveTerrain
	
	IF IE_DELIVERY_GET_DROPOFF_CARGOBOB_FLY_AWAY_DATA(scriptData.eDropoffID, vStartLocation, fStartHeading, vFinishLocation, fSpeed, iHeightAboveTerrain)
		SET_ENTITY_COORDS_NO_OFFSET(vehHeli, vStartLocation)
		TASK_HELI_MISSION(pedHeliPilot, vehHeli, NULL, NULL, vFinishLocation, MISSION_GOTO, fSpeed, 10.0, fStartHeading, iHeightAboveTerrain, iHeightAboveTerrain)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC IE_DELIVERY_CONTROL_VEH_CLONE_IN_GARAGE_CARGOBOB()
	INT iScene = 1
	
	IF scriptData.eDropoffID != IE_DROPOFF_POLICE_STATION_2
	AND scriptData.eDropoffID != IE_DROPOFF_POLICE_STATION_5
	AND scriptData.eDropoffID != IE_DROPOFF_POLICE_STATION_6
	AND scriptData.eDropoffID != IE_DROPOFF_POLICE_STATION_8
	AND scriptData.eDropoffID != IE_DROPOFF_POLICE_STATION_9
		IF (scriptData.cutscene.iCurrentScene = iScene AND IS_BIT_SET(scriptData.cutscene.iBSScenesInit, iScene))
			INT iTotalSceneTime = scriptData.cutscene.sScenes[scriptData.cutscene.iCurrentScene].iDuration
			INT iElapsedTime = scriptData.cutscene.iCurrentSceneElapsedTime
			FLOAT fMaxForwardSpeed = 3.5
			FLOAT fTimeFactor
			INT iDelay = 20
			IF scriptData.cutscene.iCurrentSceneElapsedTime >= iDelay
				fTimeFactor = FMIN(TO_FLOAT(iElapsedTime - iDelay) / TO_FLOAT(iTotalSceneTime - iDelay), 1.0)
				IF fTimeFactor <= 1.0
					IF IS_ENTITY_ALIVE(cutsceneData.vehClone)
						SET_VEHICLE_HANDBRAKE(cutsceneData.vehClone, FALSE)
						SET_VEHICLE_BRAKE(cutsceneData.vehClone, 0.0)
						SET_VEHICLE_FORWARD_SPEED(cutsceneData.vehClone, GET_GRAPH_TYPE_ACCEL(fTimeFactor) * fMaxForwardSpeed)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC VECTOR _GET_VEH_CLONE_FRONT_POINT()
	RETURN GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(cutsceneData.vehClone, scriptData.vModelMaxForFrontTest)
ENDFUNC

PROC IE_DELIVERY_CONTROL_VEH_CLONE_IN_GARAGE_ALT(INT iShiftScene = 0)
	IF NOT DOES_ENTITY_EXIST(cutsceneData.vehClone) OR IS_ENTITY_DEAD(cutsceneData.vehClone)
		EXIT
	ENDIF

	FLOAT fTimeFactor
	INT iDelay
	INT iTotalSceneTime = scriptData.cutscene.sScenes[scriptData.cutscene.iCurrentScene].iDuration
	INT iElapsedTime = scriptData.cutscene.iCurrentSceneElapsedTime
	
	BOOL bMoveCarForward, bCarFrozen
	
	FLOAT fInitialSpeed = 0.0
	FLOAT fMaxForwardSpeed = 2.0
	
	VECTOR vCarFront = _GET_VEH_CLONE_FRONT_POINT()
	
	#IF IS_DEBUG_BUILD
	DRAW_DEBUG_SPHERE(vCarFront, 0.1, 255, 0, 0)
	#ENDIF
	
	INT iMaxSceneToMoveForward = scriptData.cutscene.iScenesCount - 1 + iShiftScene
	INT iMaxTotalElapsedMoveForward = -1
	
	IF scriptData.eDropoffID = IE_DROPOFF_POLICE_STATION_1
		iMaxSceneToMoveForward = scriptData.cutscene.iScenesCount
		iMaxTotalElapsedMoveForward = 5000
		iElapsedTime += 2500
		fMaxForwardSpeed = 2.5
	ENDIF
	
	// This allows to spread forward car movement over a couple of scenes, so times are summed up to treat them like single scene
	IF scriptData.cutscene.iScenesCount > 2 + iShiftScene
		IF (((scriptData.cutscene.iCurrentScene + iShiftScene) < iMaxSceneToMoveForward)
		AND IS_BIT_SET(scriptData.cutscene.iBSScenesInit, scriptData.cutscene.iCurrentScene))
		
			INT i
			bMoveCarForward = TRUE
			iTotalSceneTime = 0
			REPEAT iMaxSceneToMoveForward i
				iTotalSceneTime += scriptData.cutscene.sScenes[i].iDuration
				IF i < scriptData.cutscene.iCurrentScene
					iElapsedTime += scriptData.cutscene.sScenes[i].iDuration
				ENDIF
			ENDREPEAT
			
			IF iMaxTotalElapsedMoveForward > -1
			AND iTotalSceneTime > iMaxTotalElapsedMoveForward
				iTotalSceneTime = iMaxTotalElapsedMoveForward
			ENDIF
			
		ELIF (scriptData.cutscene.iCurrentScene + iShiftScene = iMaxSceneToMoveForward)
		AND IS_BIT_SET(scriptData.cutscene.iBSScenesInit, scriptData.cutscene.iCurrentScene) // car's frozen only for the last scene
			bCarFrozen = TRUE
		ENDIF
	ELIF scriptData.cutscene.iScenesCount = 2 + iShiftScene
		IF (scriptData.cutscene.iCurrentScene = (0 + iShiftScene) AND IS_BIT_SET(scriptData.cutscene.iBSScenesInit, 0 + iShiftScene))
			bMoveCarForward = TRUE
		ELIF (scriptData.cutscene.iCurrentScene = (1 + iShiftScene) AND IS_BIT_SET(scriptData.cutscene.iBSScenesInit, 1 + iShiftScene))
			bCarFrozen = TRUE
		ENDIF
	ENDIF
	
	// MAKE THIS CUTSCENE LOOK MORE URGENT
	IF scriptData.bIsUrgent
		fMaxForwardSpeed = 3.25
		fInitialSpeed = 0.3
		iTotalSceneTime = 2000 // by doing this the car has 2000 ms to reach max speed
	ENDIF
	
	IF bMoveCarForward
	AND NOT cutsceneData.bDontMoveClone
		IF scriptData.cutscene.iCurrentScene > 0 + iShiftScene
			IF scriptData.eDropoffID != IE_DROPOFF_POLICE_STATION_1
			AND scriptData.eDropoffID != IE_DROPOFF_PRIVATE_BUYER_2
			
				/*
				INT iSoundDelay = 10
				IF scriptData.eDropoffID = IE_DROPOFF_GANGSTER_4
					iSoundDelay = 600
				ENDIF
				
				
				IF NOT IS_BIT_SET(cutsceneData.iBS, 1)
					IF scriptData.cutscene.iCurrentSceneElapsedTime > iSoundDelay
						//PLAY_SOUND_FROM_COORD(-1, "Door_Open", scriptData.vDropoffCoords, "GTAO_EXEC_WH_GARAGE_DOOR_SOUNDS")
						//PLAY_SOUND_FRONTEND(-1, "Door_Open", "GTAO_EXEC_WH_GARAGE_DOOR_SOUNDS")
						SET_BIT(cutsceneData.iBS, 1)
					ENDIF
				ENDIF
				*/
			ENDIF
		ENDIF
		
		FLOAT fCurrentSpeed
		IF scriptData.cutscene.iTotalElapsedTime >= iDelay
			fTimeFactor = FMIN(TO_FLOAT(iElapsedTime - iDelay) / TO_FLOAT(iTotalSceneTime - iDelay), 1.0)
			fCurrentSpeed = fInitialSpeed + GET_GRAPH_TYPE_ACCEL(fTimeFactor) * (fMaxForwardSpeed - fInitialSpeed)
			SET_VEHICLE_HANDBRAKE(cutsceneData.vehClone, FALSE)
			SET_VEHICLE_BRAKE(cutsceneData.vehClone, 0.0)
			SET_VEHICLE_FORWARD_SPEED(cutsceneData.vehClone, fCurrentSpeed)
		ENDIF
		
		// Monitor if front of the car is not about to hit the garage, if so move to the last scene, save the speed and position so we can maintain it
		IF scriptData.bGotGarageBounds
			IF scriptData.eDropoffID = IE_DROPOFF_POLICE_STATION_1 // Not actual garage door, just wall
				// Just stop
				IF IS_POINT_IN_ANGLED_AREA(vCarFront, scriptData.vGarageBBox[0], scriptData.vGarageBBox[1], scriptData.fGarageBBoxWidth)
					cutsceneData.bDontMoveClone = TRUE
					#IF IS_DEBUG_BUILD
						PRINTLN("[IE_DELIVERY][CUTSCENE] IE_DELIVERY_CONTROL_VEH_CLONE_IN_GARAGE_ALT - Would hit the wall stopping the clone.")
					#ENDIF
					EXIT
				ENDIF
			ELSE
				IF IS_POINT_IN_ANGLED_AREA(vCarFront, scriptData.vGarageBBox[0], scriptData.vGarageBBox[1], scriptData.fGarageBBoxWidth)
					#IF IS_DEBUG_BUILD
						PRINTLN("[IE_DELIVERY][CUTSCENE] IE_DELIVERY_CONTROL_VEH_CLONE_IN_GARAGE_ALT - Front of the car was about to hit the door, moving to last scene")
					#ENDIF
					scriptData.cutscene.iCurrentScene = scriptData.cutscene.iScenesCount - 1
					cutsceneData.fFrozenSpeed = fCurrentSpeed
					cutsceneData.vCachedStartPos = GET_ENTITY_COORDS(cutsceneData.vehClone)
				ENDIF
			ENDIF
		ELSE
			ASSERTLN("[IE_DELIVERY][CUTSCENE] IE_DELIVERY_CONTROL_VEH_CLONE_IN_GARAGE_ALT - We need garage bounds but dont have them! They need to be precomputed.")
			PRINTLN("[IE_DELIVERY][CUTSCENE] IE_DELIVERY_CONTROL_VEH_CLONE_IN_GARAGE_ALT - We need garage bounds but dont have them! They need to be precomputed.")
		ENDIF
		
	ELIF bCarFrozen
		VECTOR vStartPos, vEndPos
		VECTOR vDir = GET_HEADING_AS_VECTOR(GET_ENTITY_HEADING(cutsceneData.vehClone))
		FLOAT fLastRecordedSpeed = fMaxForwardSpeed
		
		IF cutsceneData.fFrozenSpeed > 0.0
			vStartPos = cutsceneData.vCachedStartPos
			fLastRecordedSpeed = cutsceneData.fFrozenSpeed
			FLOAT fDistToMoveCar = 7.0
			vEndPos = vStartPos + vDir * fDistToMoveCar
			FLOAT fTimeToMove = (fDistToMoveCar / fLastRecordedSpeed) * 1000.0
			fTimeFactor = FMIN(TO_FLOAT(scriptData.cutscene.iCurrentSceneElapsedTime) / fTimeToMove, 1.0)
		ELSE
			fTimeFactor = FMIN(TO_FLOAT(scriptData.cutscene.iCurrentSceneElapsedTime) / TO_FLOAT(iTotalSceneTime), 1.0)
			_GET_FROZEN_CAR_START_AND_END_COORDS(vStartPos, vEndPos)
			// Calculate endpos to maintain last speed
			vEndPos = vStartPos + vDir * (TO_FLOAT(iTotalSceneTime) / 1000.0) * fLastRecordedSpeed
		ENDIF
		
		VECTOR vTotalOffset = vEndPos - vStartPos
		VECTOR vCurrentPos = vStartPos + vTotalOffset * GET_GRAPH_TYPE_DECEL(fTimeFactor)
		FLOAT fCloneHeading
		VECTOR vFoo
		IE_DELIVERY_GET_DROPOFF_VEHICLE_CLONE_DATA(scriptData.eDropoffID, vFoo, fCloneHeading)
		fCloneHeading = TRANSFORM_CUTSCENE_HEADING_TO_WORLD_HEADING(fCloneHeading)
		
		IF NOT IS_BIT_SET(cutsceneData.iBS, 3)
			SET_ENTITY_COLLISION(cutsceneData.vehClone, FALSE)
			SET_ENTITY_COORDS(cutsceneData.vehClone, vCurrentPos) // This will set it on ground properly
			SET_ENTITY_ROTATION(cutsceneData.vehClone, <<0.0, 0.0, fCloneHeading>>)
			FREEZE_ENTITY_POSITION(cutsceneData.vehClone, TRUE)
			VECTOR vCurrentCoords = GET_ENTITY_COORDS(cutsceneData.vehClone)
			cutsceneData.fMaintainZ = vCurrentCoords.Z
			SET_BIT(cutsceneData.iBS, 3)
		ENDIF
		
		SET_ENTITY_COORDS_NO_OFFSET(cutsceneData.vehClone, <<vCurrentPos.X, vCurrentPos.Y, cutsceneData.fMaintainZ>>)
		
		/*
		IF scriptData.eDropoffID != IE_DROPOFF_POLICE_STATION_1
		AND scriptData.eDropoffID != IE_DROPOFF_PRIVATE_BUYER_2
			IF NOT IS_BIT_SET(cutsceneData.iBS, 2)
				IF scriptData.cutscene.sScenes[scriptData.cutscene.iCurrentScene].iDuration - scriptData.cutscene.iCurrentSceneElapsedTime < 1500
					//PLAY_SOUND_FRONTEND(-1, "Door_Close", "GTAO_EXEC_WH_GARAGE_DOOR_SOUNDS")
					SET_BIT(cutsceneData.iBS, 2)
				ENDIF
			ENDIF
		ENDIF
		*/
	ENDIF
ENDPROC

PROC IE_DELIVERY_CONTROL_VEH_CLONE_IN_GARAGE_ALT_CARGOBOB()
	IF NOT DOES_ENTITY_EXIST(cutsceneData.vehClone) OR IS_ENTITY_DEAD(cutsceneData.vehClone)
		EXIT
	ENDIF

	FLOAT fTimeFactor
	INT iTotalSceneTime = scriptData.cutscene.sScenes[scriptData.cutscene.iCurrentScene].iDuration
	FLOAT fMaxForwardSpeed = 2.0
		
	IF scriptData.eDropoffID = IE_DROPOFF_POLICE_STATION_1
		fMaxForwardSpeed = 2.5
	ENDIF
				
	IF scriptData.cutscene.iCurrentScene = (1) AND IS_BIT_SET(scriptData.cutscene.iBSScenesInit, 1)
		fTimeFactor = FMIN(TO_FLOAT(scriptData.cutscene.iCurrentSceneElapsedTime) / TO_FLOAT(iTotalSceneTime), 1.0)
		
		VECTOR vStartPos, vEndPos, vFinalEndPos
		VECTOR vDir = GET_HEADING_AS_VECTOR(GET_ENTITY_HEADING(cutsceneData.vehClone))
		_GET_FROZEN_CAR_START_AND_END_COORDS_CARGOBOB(vStartPos, vEndPos)
		vFinalEndPos = vEndPos
		// Calculate endpos to maintain maxspeed
		vEndPos = vStartPos + vDir * (TO_FLOAT(iTotalSceneTime) / 1000.0) * fMaxForwardSpeed
		
		VECTOR vTotalOffset = vEndPos - vStartPos
		VECTOR vCurrentPos = vStartPos + vTotalOffset * GET_GRAPH_TYPE_DECEL(fTimeFactor)
		FLOAT fCloneHeading
		VECTOR vFoo
		IE_DELIVERY_GET_DROPOFF_VEHICLE_CLONE_DATA(scriptData.eDropoffID, vFoo, fCloneHeading)
		fCloneHeading = TRANSFORM_CUTSCENE_HEADING_TO_WORLD_HEADING(fCloneHeading)
		
		IF NOT IS_BIT_SET(cutsceneData.iBS, 3)
			SET_ENTITY_COLLISION(cutsceneData.vehClone, FALSE)
			SET_ENTITY_COORDS(cutsceneData.vehClone, vCurrentPos) // This will set it on ground properly
			SET_ENTITY_ROTATION(cutsceneData.vehClone, <<0.0, 0.0, fCloneHeading>>)
			FREEZE_ENTITY_POSITION(cutsceneData.vehClone, TRUE)
			VECTOR vCurrentCoords = GET_ENTITY_COORDS(cutsceneData.vehClone)
			cutsceneData.fMaintainZ = vCurrentCoords.Z
			SET_BIT(cutsceneData.iBS, 3)
		ENDIF
		
		SET_ENTITY_COORDS_NO_OFFSET(cutsceneData.vehClone, <<vCurrentPos.X, vCurrentPos.Y, cutsceneData.fMaintainZ>>)
		
		IF NOT IS_BIT_SET(cutsceneData.iBS, 2)
			IF ARE_VECTORS_ALMOST_EQUAL(vFinalEndPos, GET_ENTITY_COORDS(cutsceneData.vehClone), 0.5)
			//scriptData.cutscene.sScenes[scriptData.cutscene.iCurrentScene].iDuration - scriptData.cutscene.iCurrentSceneElapsedTime < 1500
				PLAY_SOUND_FRONTEND(-1, "Door_Close", "GTAO_EXEC_WH_GARAGE_DOOR_SOUNDS")
				SET_BIT(cutsceneData.iBS, 2)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC IE_DELIVERY_CONTROL_VEH_CLONE_WITH_CARGOBOB_MECHANIC()
	IF NOT DOES_ENTITY_EXIST(cutsceneData.vehClone) OR IS_ENTITY_DEAD(cutsceneData.vehClone)
	OR NOT DOES_ENTITY_EXIST(cutsceneData.pedProxyMechanic) OR IS_ENTITY_DEAD(cutsceneData.pedProxyMechanic)
		EXIT
	ENDIF

	IF SIMPLE_CUTSCENE_IS_SCENE_RUNNING(scriptData.cutscene, 1)
		IF NOT IS_BIT_SET(cutsceneData.iBS, 29)
			VECTOR vStartCoords, vDir
			FLOAT fStartHeading
			IE_DELIVERY_GET_DROPOFF_VEHICLE_CLONE_DATA(scriptData.eDropoffID, vStartCoords, fStartHeading)
			vDir = GET_HEADING_AS_VECTOR(fStartHeading)
			cutsceneData.vCachedStartPos = vStartCoords + vDir * 1.5
			
			FREEZE_ENTITY_POSITION(cutsceneData.vehClone, TRUE)
			SET_ENTITY_COLLISION(cutsceneData.vehClone, FALSE)
			SET_ENTITY_COORDS(cutsceneData.vehClone, cutsceneData.vCachedStartPos)
			SET_ENTITY_HEADING(cutsceneData.vehClone, fStartHeading)
			
			FREEZE_ENTITY_POSITION(cutsceneData.pedProxyMechanic, FALSE)
			SET_ENTITY_VISIBLE(cutsceneData.pedProxyMechanic, TRUE)
			TASK_ENTER_VEHICLE(cutsceneData.pedProxyMechanic, cutsceneData.vehClone, DEFAULT, VS_DRIVER, DEFAULT, ECF_WARP_ENTRY_POINT)
			VECTOR vCurrentCoords = GET_ENTITY_COORDS(cutsceneData.vehClone)
			cutsceneData.fMaintainZ = vCurrentCoords.Z
			
			SET_BIT(cutsceneData.iBS, 29)
		ENDIF
		
		IF IS_PED_IN_VEHICLE(cutsceneData.pedProxyMechanic, cutsceneData.vehClone)
		AND GET_VEHICLE_DOOR_ANGLE_RATIO(cutsceneData.vehClone, SC_DOOR_FRONT_LEFT) < 0.1
			IF NOT IS_BIT_SET(cutsceneData.iBS, 28)
				cutsceneData.iFrozenTimestamp = scriptData.cutscene.iCurrentSceneElapsedTime
				SET_BIT(cutsceneData.iBS, 28)
			ENDIF
			
			INT iTotalSceneTime = scriptData.cutscene.sScenes[scriptData.cutscene.iCurrentScene].iDuration - cutsceneData.iFrozenTimestamp
			FLOAT fMaxForwardSpeed = 2.5
			VECTOR vDir
			vDir = GET_HEADING_AS_VECTOR(GET_ENTITY_HEADING(cutsceneData.vehClone))

			VECTOR vTotalOffset = vDir * (TO_FLOAT(iTotalSceneTime) / 1000.0) * fMaxForwardSpeed
			VECTOR vCurrentPos = cutsceneData.vCachedStartPos + vTotalOffset * GET_GRAPH_TYPE_ACCEL(TO_FLOAT(scriptData.cutscene.iCurrentSceneElapsedTime - cutsceneData.iFrozenTimestamp) / TO_FLOAT(iTotalSceneTime))
		
			SET_ENTITY_COORDS_NO_OFFSET(cutsceneData.vehClone, <<vCurrentPos.X, vCurrentPos.Y, cutsceneData.fMaintainZ>>)
		ENDIF
	ENDIF
ENDPROC

PROC IE_DELIVERY_CONTROL_VEH_CLONE_IN_GARAGE_ALT_2(BOOL bIgnoreFirstScene = FALSE)
	IF NOT DOES_ENTITY_EXIST(cutsceneData.vehClone) OR IS_ENTITY_DEAD(cutsceneData.vehClone)
		EXIT
	ENDIF
	
	IF scriptData.cutscene.iCurrentScene = 0
	AND bIgnoreFirstScene
		EXIT
	ENDIF
	
	FLOAT fInitialSpeed = 0.0
	FLOAT fTimeFactor
	INT iAccelerationTime = 3000
	FLOAT fMaxForwardSpeed = 2.0
	
	// MAKE THIS CUTSCENE LOOK MORE URGENT
	IF scriptData.bIsUrgent
		fMaxForwardSpeed = 3.25
		fInitialSpeed = 0.3
		iAccelerationTime = 2000 // by doing this the car has 2000 ms to reach max speed
	ENDIF
	
	VECTOR vCarFront = _GET_VEH_CLONE_FRONT_POINT()
	
	#IF IS_DEBUG_BUILD
	DRAW_DEBUG_SPHERE(vCarFront, 0.1, 255, 0, 0)
	#ENDIF
	
	INT iTotalCutsceneTime, i
	REPEAT scriptData.cutscene.iScenesCount i
		IF (i = 0 AND bIgnoreFirstScene)
			RELOOP
		ENDIF
		iTotalCutsceneTime += scriptData.cutscene.sScenes[i].iDuration
	ENDREPEAT
	
	BOOL bCanMoveForward = TRUE
	INT iTotalElapsedTime = scriptData.cutscene.iTotalElapsedTime
	
	IF bIgnoreFirstScene
		iTotalElapsedTime = iTotalElapsedTime - scriptData.cutscene.sScenes[0].iDuration
	ENDIF
	
	IF scriptData.eDropoffID != IE_DROPOFF_POLICE_STATION_3
		IF iTotalElapsedTime > 4000
			bCanMoveForward = FALSE
		ENDIF
	ENDIF
	
	IF NOT bCanMoveForward
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(cutsceneData.iBS, 0)
		fTimeFactor = FMIN(TO_FLOAT(iTotalElapsedTime) / TO_FLOAT(iAccelerationTime), 1.0)
		FLOAT fCurrentSpeed = fInitialSpeed + GET_GRAPH_TYPE_ACCEL(fTimeFactor) * (fMaxForwardSpeed - fInitialSpeed)
		
		IF bCanMoveForward
			SET_VEHICLE_HANDBRAKE(cutsceneData.vehClone, FALSE)
			SET_VEHICLE_BRAKE(cutsceneData.vehClone, 0.0)
			SET_VEHICLE_FORWARD_SPEED(cutsceneData.vehClone, fCurrentSpeed)
		ENDIF
		
		IF scriptData.eDropoffID = IE_DROPOFF_GANGSTER_7 AND IS_POINT_IN_ANGLED_AREA(vCarFront, <<-1620.035522,14.221024,61.177059>>, <<-1614.697388,11.621071,64.427025>>, 2.25)
		OR scriptData.eDropoffID = IE_DROPOFF_GANGSTER_8 AND IS_POINT_IN_ANGLED_AREA(vCarFront, <<-3104.661133,713.178650,19.573444>>, <<-3105.957275,718.135498,22.787096>>, 1.5)
			cutsceneData.vCachedStartPos = GET_ENTITY_COORDS(cutsceneData.vehClone)
			cutsceneData.vCachedRot = GET_ENTITY_ROTATION(cutsceneData.vehClone)
			cutsceneData.iFrozenTimestamp = iTotalElapsedTime
			cutsceneData.fFrozenSpeed = fCurrentSpeed
			SET_BIT(cutsceneData.iBS, 0)
		ENDIF
	ELSE
		fTimeFactor = FMIN(TO_FLOAT(iTotalElapsedTime - cutsceneData.iFrozenTimestamp) / TO_FLOAT(iTotalCutsceneTime - cutsceneData.iFrozenTimestamp), 1.0)
		
		VECTOR vStartPos, vEndPos
		
		vStartPos = cutsceneData.vCachedStartPos
		VECTOR vDir = GET_HEADING_AS_VECTOR(GET_ENTITY_HEADING(cutsceneData.vehClone))
		//vEndPos = vStartPos + vDir * 6.0
		
		// Calculate endpos to maintain maxspeed
		vEndPos = vStartPos + vDir * (TO_FLOAT(iTotalCutsceneTime - cutsceneData.iFrozenTimestamp) / 1000.0) * cutsceneData.fFrozenSpeed
		
		VECTOR vTotalOffset = vEndPos - vStartPos
		VECTOR vCurrentPos = vStartPos + vTotalOffset * fTimeFactor //GET_GRAPH_TYPE_DECEL(fTimeFactor)
		//FLOAT fCloneHeading
		//VECTOR vFoo
		//IE_DELIVERY_GET_DROPOFF_VEHICLE_CLONE_DATA(scriptData.eDropoffID, vFoo, fCloneHeading)
		
		IF DOES_ENTITY_EXIST(cutsceneData.vehClone)
			IF NOT IS_BIT_SET(cutsceneData.iBS, 3)
				SET_ENTITY_COLLISION(cutsceneData.vehClone, FALSE)
				SET_ENTITY_COORDS(cutsceneData.vehClone, vCurrentPos) // This will set it on ground properly
				//SET_ENTITY_ROTATION(cutsceneData.vehClone, <<0.0, 0.0, fCloneHeading>>)
				FREEZE_ENTITY_POSITION(cutsceneData.vehClone, TRUE)
				VECTOR vCurrentCoords = GET_ENTITY_COORDS(cutsceneData.vehClone)
				cutsceneData.fMaintainZ = vCurrentCoords.Z
				SET_BIT(cutsceneData.iBS, 3)
			ENDIF
			SET_ENTITY_COORDS_NO_OFFSET(cutsceneData.vehClone, <<vCurrentPos.X, vCurrentPos.Y, cutsceneData.fMaintainZ>>)
			SET_ENTITY_ROTATION(cutsceneData.vehClone, cutsceneData.vCachedRot)
		ENDIF
	ENDIF
ENDPROC

PROC IE_DELIVERY_CONTROL_VEH_CLONE_IN_SCRAP_GARAGE(INT iShiftScene = 0)
	
	INT iDelay = 250
	FLOAT fTimeFactor
	INT iTotalSceneTime = scriptData.cutscene.sScenes[scriptData.cutscene.iCurrentScene].iDuration
	INT iElapsedTime = scriptData.cutscene.iCurrentSceneElapsedTime
	FLOAT fMaxForwardSpeed = 2.0
	BOOL bMoveCarForward, bCarFrozen
			
	// This allows to spread forward car movement over a couple of scenes, so times are summed up to treat them like single scene
	IF scriptData.cutscene.iCurrentScene = 0 + iShiftScene
		IF IS_BIT_SET(scriptData.cutscene.iBSScenesInit, 0)
			bMoveCarForward = TRUE
		ENDIF
	ELIF scriptData.cutscene.iCurrentScene = 1 + iShiftScene
		IF IS_BIT_SET(scriptData.cutscene.iBSScenesInit, 1)
			bCarFrozen = TRUE
		ENDIF
		
		IE_PROP_TRANSITION_DATA transData
		IE_DELIVERY_GET_DROPOFF_PROP_TRANSITION_DATA(scriptData.eDropoffID, 0, scriptData.cutscene.iCurrentScene, transData)
		
		IF scriptData.cutscene.iCurrentSceneElapsedTime >= transData.iDelay
			//VEHICLE_TOGGLE_ENGINE(FALSE, FALSE)
		ENDIF
	ELIF scriptData.cutscene.iCurrentScene = 2 + iShiftScene
		IF NOT IS_BIT_SET(cutsceneData.iBS, 0)
			IE_PROP_TRANSITION_DATA transData
			IE_DELIVERY_GET_DROPOFF_PROP_TRANSITION_DATA(scriptData.eDropoffID, 0, scriptData.cutscene.iCurrentScene, transData)
			
			IF scriptData.cutscene.iCurrentSceneElapsedTime >= transData.iDelay
				//scrap sound
				PLAY_SOUND_FRONTEND(-1, "Crush_Car", "DLC_IE_Deliver_Vehicle_Scrapyard_Sounds", FALSE)
				SET_BIT(cutsceneData.iBS, 0)
			ENDIF
		ENDIF
	ENDIF
	
	IF bMoveCarForward
	AND scriptData.eDropoffID != IE_DROPOFF_SCRAPYARD_1
		IF scriptData.cutscene.iCurrentScene = 0 + iShiftScene
			//iDelay = 250
			IF NOT IS_BIT_SET(cutsceneData.iBS, 1)
			AND NOT scriptData.bViaCargobob
				IF scriptData.cutscene.iCurrentSceneElapsedTime > 10
					PLAY_SOUND_FRONTEND(-1, "Door_Open", "GTAO_EXEC_WH_GARAGE_DOOR_SOUNDS")
					SET_BIT(cutsceneData.iBS, 1)
				ENDIF
			ENDIF
			
			IF scriptData.cutscene.iCurrentSceneElapsedTime >= iDelay
				fTimeFactor = FMIN(TO_FLOAT(iElapsedTime - iDelay) / TO_FLOAT(iTotalSceneTime - iDelay), 1.0)
				IF fTimeFactor <= 1.0
					IF IS_ENTITY_ALIVE(cutsceneData.vehClone)
						SET_VEHICLE_FORWARD_SPEED(cutsceneData.vehClone, GET_GRAPH_TYPE_ACCEL(fTimeFactor) * fMaxForwardSpeed)
					ENDIF
				ENDIF
			ENDIF
		ENDIF

	ELIF bCarFrozen
		fTimeFactor = FMIN(TO_FLOAT(scriptData.cutscene.iCurrentSceneElapsedTime) / TO_FLOAT(iTotalSceneTime - 1000), 1.0)
		
		VECTOR vStartPos, vEndPos
		_GET_FROZEN_CAR_START_AND_END_COORDS(vStartPos, vEndPos)
		
		VECTOR vTotalOffset = vEndPos - vStartPos
		VECTOR vCurrentPos = vStartPos + vTotalOffset * GET_GRAPH_TYPE_DECEL(fTimeFactor)
		FLOAT fCloneHeading
		VECTOR vFoo
		IE_DELIVERY_GET_DROPOFF_VEHICLE_CLONE_DATA(scriptData.eDropoffID, vFoo, fCloneHeading)
		fCloneHeading = TRANSFORM_CUTSCENE_HEADING_TO_WORLD_HEADING(fCloneHeading)
		
		IF IS_ENTITY_ALIVE(cutsceneData.vehClone)
			IF NOT IS_BIT_SET(cutsceneData.iBS, 3)
				SET_ENTITY_COLLISION(cutsceneData.vehClone, FALSE)
				//SET_ENTITY_COORDS(cutsceneData.vehClone, vCurrentPos) // This will set it on ground properly
				SET_ENTITY_ROTATION(cutsceneData.vehClone, <<0.0, 0.0, fCloneHeading>>)
				FREEZE_ENTITY_POSITION(cutsceneData.vehClone, TRUE)
				VECTOR vCurrentCoords = GET_ENTITY_COORDS(cutsceneData.vehClone)
				cutsceneData.fMaintainZ = vCurrentCoords.Z
				SET_BIT(cutsceneData.iBS, 3)
			ENDIF
			SET_ENTITY_COORDS_NO_OFFSET(cutsceneData.vehClone, <<vCurrentPos.X, vCurrentPos.Y, cutsceneData.fMaintainZ>>)
		ENDIF
	ENDIF
ENDPROC

PROC IE_DELIVERY_CONTROL_VEH_CLONE_IN_SCRAP_GARAGE_CARGOBOB()
	IE_DELIVERY_CONTROL_VEH_CLONE_IN_SCRAP_GARAGE(0)
ENDPROC

PROC IE_DELIVERY_CONTROL_VEH_CLONE_IN_SCRAP(INT iShiftScene = 0)
		
	IE_PROP_TRANSITION_DATA transData
	IE_DELIVERY_GET_DROPOFF_PROP_TRANSITION_DATA(scriptData.eDropoffID, 0, scriptData.cutscene.iCurrentScene, transData)
		
	//after x time , turn the engine off in the cutscene view.
	IF scriptData.cutscene.iCurrentScene = 0 + iShiftScene
		IF NOT IS_BIT_SET(cutsceneData.iBS, 1)
		AND scriptData.cutscene.iCurrentSceneElapsedTime >= transData.iDelay
			//VEHICLE_TOGGLE_ENGINE(FALSE, FALSE)
			SET_VEHICLE_LIGHTS(cutsceneData.vehClone, SET_VEHICLE_LIGHTS_OFF)
			SET_BIT(cutsceneData.iBS, 1)
		ENDIF
	
	//scrapyard sign view.
	ELIF scriptData.cutscene.iCurrentScene = 1 + iShiftScene
		IF NOT IS_BIT_SET(cutsceneData.iBS, 0)
		AND scriptData.cutscene.iCurrentSceneElapsedTime >= transData.iDelay
			PLAY_SOUND_FRONTEND(-1, "Crush_Car", "DLC_IE_Deliver_Vehicle_Scrapyard_Sounds", FALSE)
			SET_BIT(cutsceneData.iBS, 0)
		ENDIF
	ENDIF
ENDPROC

PROC IE_DELIVERY_CONTROL_VEH_CLONE_IN_SCRAP_CARGOBOB()
	INT iScrapSoundDelay = 650
	//scrapyard sign view.
	if scriptData.cutscene.iCurrentScene = 2
		IF NOT IS_BIT_SET(cutsceneData.iBS, 0)
		AND scriptData.cutscene.iCurrentSceneElapsedTime >= iScrapSoundDelay
			PLAY_SOUND_FRONTEND(-1, "Crush_Car", "DLC_IE_Deliver_Vehicle_Scrapyard_Sounds", FALSE)
			SET_BIT(cutsceneData.iBS, 0)
		ENDIF
	ENDIF
ENDPROC

PROC _CREATE_AND_START_SYNCED_SCENE(FLOAT fPhase = 0.0, FLOAT fRate = 0.0, BOOL bHoldFrame = FALSE)
	IF NOT IS_STRING_NULL_OR_EMPTY(cutsceneData.strSyncedScenePlayerAnim)
		IF DOES_ENTITY_EXIST(cutsceneData.pedPlayerActors[0])
		AND NOT IS_ENTITY_DEAD(cutsceneData.pedPlayerActors[0])
		AND DOES_ENTITY_EXIST(cutsceneData.pedActors[0])
		AND NOT IS_ENTITY_DEAD(cutsceneData.pedActors[0])
			IF IS_SYNCHRONIZED_SCENE_RUNNING(cutsceneData.iSyncedSceneID)
				DETACH_SYNCHRONIZED_SCENE(cutsceneData.iSyncedSceneID)
			ENDIF
			
			SET_FEMALE_PED_FEET_TO_FLATS(cutsceneData.pedPlayerActors[0])
			
			cutsceneData.iSyncedSceneID = CREATE_SYNCHRONIZED_SCENE(cutsceneData.vSyncedScenePos, cutsceneData.vSyncedSceneRot)
			
			TASK_SYNCHRONIZED_SCENE(cutsceneData.pedPlayerActors[0], cutsceneData.iSyncedSceneID, cutsceneData.strSyncedSceneDict, cutsceneData.strSyncedScenePlayerAnim, INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
			TASK_SYNCHRONIZED_SCENE(cutsceneData.pedActors[0], cutsceneData.iSyncedSceneID, cutsceneData.strSyncedSceneDict, cutsceneData.strSyncedScenePedAnim, INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
			//FORCE_PED_AI_AND_ANIMATION_UPDATE(cutsceneData.pedPlayerActors[0])
			//FORCE_PED_AI_AND_ANIMATION_UPDATE(cutsceneData.pedActors[0])
			SET_SYNCHRONIZED_SCENE_RATE(cutsceneData.iSyncedSceneID, fRate)
			SET_SYNCHRONIZED_SCENE_PHASE(cutsceneData.iSyncedSceneID, fPhase)
			
			IF bHoldFrame
				SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(cutsceneData.iSyncedSceneID, TRUE)
			ENDIF
			
			ATTACH_ENTITY_TO_ENTITY(cutsceneData.objProps[0], cutsceneData.pedPlayerActors[0], GET_PED_BONE_INDEX(cutsceneData.pedPlayerActors[0], BONETAG_PH_R_HAND), <<0, 0, 0>>, <<0, 0, 0>>)
			
			#IF IS_DEBUG_BUILD
				PRINTLN("[IE_DELIVERY][CUTSCENE] _CREATE_AND_START_SYNCED_SCENE - Started sync scene.")
			#ENDIF
		ENDIF
	ENDIF
ENDPROC


PROC _GET_PLAYER_ACTOR_PEDS_CHILL_POS_WHEN_GOON_DRIVES_AWAY(INT i, VECTOR &vCoords, FLOAT &fHeading)
	IF scriptData.eDropoffID = IE_DROPOFF_GANGSTER_2
		SWITCH i
			CASE 1
				vCoords = <<-1919.6584, 2050.4639, 139.7357>>
    			fHeading = 84.5295
			BREAK
			CASE 2
				vCoords = <<-1919.6578, 2046.5913, 139.7358>>
    			fHeading = 81.3575
			BREAK
			CASE 3
				vCoords = <<-1918.2133, 2049.8391, 139.7362>>
   				fHeading = 355.5679
			BREAK
		ENDSWITCH
	ELIF scriptData.eDropoffID = IE_DROPOFF_GANGSTER_3
		SWITCH i
			CASE 1
				vCoords = <<-66.8902, 893.2983, 234.5449>>
    			fHeading = 319.5974
			BREAK
			CASE 2
				vCoords = <<-70.5256, 895.9586, 234.4818>>
   			 	fHeading = 294.0218
			BREAK
			CASE 3
				vCoords = <<-68.7741, 892.5426, 234.5301>>
    			fHeading = 234.4872
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC


PROC IE_DELIVERY_CONTROL_GOON_DRIVING_AWAY()
	IF IS_ENTITY_DEAD(cutsceneData.pedActors[0])
	OR IS_ENTITY_DEAD(cutsceneData.pedActors[1])
	OR IS_ENTITY_DEAD(cutsceneData.pedPlayerActors[0])
	OR IS_ENTITY_DEAD(cutsceneData.vehClone)
	OR IS_ENTITY_DEAD(cutsceneData.vehActors[0])
		EXIT
	ENDIF
	
	INT iTimeToDriveAway = 4500
	IF scriptData.eDropoffID = IE_DROPOFF_GANGSTER_3
		iTimeToDriveAway = 3500
	ENDIF
	
	IF scriptData.cutscene.iTotalElapsedTime > iTimeToDriveAway
	AND NOT IS_BIT_SET(cutsceneData.iBS, 4)
	
		VECTOR vDriveAwayCoords
		IF scriptData.eDropoffID = IE_DROPOFF_GANGSTER_2
			vDriveAwayCoords = <<-1826.6082, 2031.5760, 131.3825>>
		ELIF scriptData.eDropoffID = IE_DROPOFF_GANGSTER_3
			vDriveAwayCoords = <<-116.1826, 880.4391, 234.7381>>
		ENDIF
	
		//DISABLE_COLLISION_OF_ENTITY_WITH_NEARBY_NETWORKED_VEHS(cutsceneData.vehActors[0], cutsceneData.pedActors[1])
		TASK_VEHICLE_DRIVE_TO_COORD(cutsceneData.pedActors[1], cutsceneData.vehActors[0], vDriveAwayCoords, 6.0, DRIVINGSTYLE_ACCURATE, DUMMY_MODEL_FOR_SCRIPT, DRIVINGMODE_AVOIDCARS, 2.0, -1)
		SET_BIT(cutsceneData.iBS, 4)
	ENDIF
	
	IF SIMPLE_CUTSCENE_IS_SCENE_RUNNING(scriptData.cutscene, 0) // establishing shot
		IF NOT IS_BIT_SET(cutsceneData.iBS, 0)
			//SET_VEHICLE_DOOR_OPEN(cutsceneData.vehClone, SC_DOOR_FRONT_LEFT, FALSE, TRUE)
			SET_PED_INTO_VEHICLE(cutsceneData.pedActors[1], cutsceneData.vehActors[0])
			SET_BIT(cutsceneData.iBS, 0)
		ENDIF
		
	ELIF SIMPLE_CUTSCENE_IS_SCENE_RUNNING(scriptData.cutscene, 1) // handover scene
		IF NOT IS_BIT_SET(cutsceneData.iBS, 1)
			
			INT i
			VECTOR vCoords
			FLOAT fHeading
			FOR i = 1 TO COUNT_OF(cutsceneData.pedPlayerActors) - 1
				IF DOES_ENTITY_EXIST(cutsceneData.pedPlayerActors[i])
				AND NOT IS_ENTITY_DEAD(cutsceneData.pedPlayerActors[i])
					_GET_PLAYER_ACTOR_PEDS_CHILL_POS_WHEN_GOON_DRIVES_AWAY(i, vCoords, fHeading)
					CLEAR_PED_TASKS_IMMEDIATELY(cutsceneData.pedPlayerActors[i])
					SET_ENTITY_COORDS(cutsceneData.pedPlayerActors[i], vCoords)
					SET_ENTITY_HEADING(cutsceneData.pedPlayerActors[i], fHeading)
					TASK_START_SCENARIO_IN_PLACE(cutsceneData.pedPlayerActors[i], "WORLD_HUMAN_HANG_OUT_STREET", 0, TRUE)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(cutsceneData.pedPlayerActors[i], TRUE, TRUE)
					//DELETE_PED(cutsceneData.pedPlayerActors[i])
				ENDIF
			ENDFOR
			
			CLEAR_PED_TASKS_IMMEDIATELY(cutsceneData.pedActors[0])
			_CREATE_AND_START_SYNCED_SCENE(0.355, 0.75)
			
			SET_BIT(cutsceneData.iBS, 1)
		ENDIF
		
		IF NOT IS_BIT_SET(cutsceneData.iBS, 5)
			IF HAS_ANIM_EVENT_FIRED(cutsceneData.pedPlayerActors[0], HASH("Detach"))
				IF IS_ENTITY_ATTACHED_TO_ENTITY(cutsceneData.objProps[0], cutsceneData.pedPlayerActors[0])
					DETACH_ENTITY(cutsceneData.objProps[0], FALSE)
					
					IF IS_PED_MALE(cutsceneData.pedActors[0])
						ATTACH_ENTITY_TO_ENTITY(cutsceneData.objProps[0], cutsceneData.pedActors[0], GET_PED_BONE_INDEX(cutsceneData.pedActors[0], BONETAG_PH_R_HAND), <<0, 0, 0>>, <<0, 0, 0>>)
					ELSE
						ATTACH_ENTITY_TO_ENTITY(cutsceneData.objProps[0], cutsceneData.pedActors[0], GET_PED_BONE_INDEX(cutsceneData.pedActors[0], BONETAG_PH_L_HAND), <<0, 0, 0>>, <<0, 0, 0>>)
					ENDIF
					
					SET_BIT(cutsceneData.iBS, 5)
				ENDIF
			ENDIF
		ENDIF
				
	ELIF SIMPLE_CUTSCENE_IS_SCENE_RUNNING(scriptData.cutscene, 2)
		IF NOT IS_BIT_SET(cutsceneData.iBS, 2)
			SET_ENTITY_VISIBLE(cutsceneData.pedPlayerActors[0], FALSE)
			SET_ENTITY_VISIBLE(cutsceneData.vehClone, FALSE)
			SET_ENTITY_VISIBLE(cutsceneData.pedActors[0], FALSE)
			SET_ENTITY_VISIBLE(cutsceneData.pedActors[1], TRUE)
			SET_ENTITY_VISIBLE(cutsceneData.vehActors[0], TRUE)
			SET_BIT(cutsceneData.iBS, 2)
		ENDIF
	ENDIF
	
ENDPROC

PROC IE_DELIVERY_CONTROL_GOON_DRIVING_AWAY_CARGOBOB()
	IF IS_ENTITY_DEAD(cutsceneData.pedActors[0])
	OR IS_ENTITY_DEAD(cutsceneData.pedActors[1])
	OR IS_ENTITY_DEAD(cutsceneData.pedPlayerActors[0])
	OR IS_ENTITY_DEAD(cutsceneData.vehClone)
	OR IS_ENTITY_DEAD(cutsceneData.vehActors[0])
		EXIT
	ENDIF
	
	IF scriptData.cutscene.iTotalElapsedTime > 5500
	AND NOT IS_BIT_SET(cutsceneData.iBS, 4)
	
		VECTOR vDriveAwayCoords
		IF scriptData.eDropoffID = IE_DROPOFF_GANGSTER_2
			vDriveAwayCoords = <<-1826.6082, 2031.5760, 131.3825>>
		ELIF scriptData.eDropoffID = IE_DROPOFF_GANGSTER_3
			vDriveAwayCoords = <<-120.3186, 937.5264, 234.8124>>
		ENDIF
	
		TASK_VEHICLE_DRIVE_TO_COORD(cutsceneData.pedActors[1], cutsceneData.vehActors[0], vDriveAwayCoords, 6.0, DRIVINGSTYLE_ACCURATE, DUMMY_MODEL_FOR_SCRIPT, DRIVINGMODE_AVOIDCARS, 2.0, -1)
		SET_BIT(cutsceneData.iBS, 4)
	ENDIF
	
	IF SIMPLE_CUTSCENE_IS_SCENE_RUNNING(scriptData.cutscene, 0) // establishing shot
		IF NOT IS_BIT_SET(cutsceneData.iBS, 0)
			SET_PED_INTO_VEHICLE(cutsceneData.pedActors[1], cutsceneData.vehActors[0])
			SET_BIT(cutsceneData.iBS, 0)
		ENDIF
		
	ELIF SIMPLE_CUTSCENE_IS_SCENE_RUNNING(scriptData.cutscene, 1) // handover scene
		IF NOT IS_BIT_SET(cutsceneData.iBS, 5)
			IF IS_PED_IN_ANY_VEHICLE(cutsceneData.pedPlayerActors[0])
				TASK_LEAVE_VEHICLE(cutsceneData.pedPlayerActors[0], cutsceneData.vehClone)
			ENDIF
			SET_BIT(cutsceneData.iBS, 5)
		ENDIF
		
		IF NOT IS_BIT_SET(cutsceneData.iBS, 1)
		AND IS_VEHICLE_EMPTY(cutsceneData.vehClone)
			CLEAR_PED_TASKS_IMMEDIATELY(cutsceneData.pedActors[0])
			TASK_ENTER_VEHICLE(cutsceneData.pedActors[0], cutsceneData.vehClone, DEFAULT_TIME_BEFORE_WARP, VS_ANY_PASSENGER)
			SET_BIT(cutsceneData.iBS, 1)
		ENDIF
				
	ELIF SIMPLE_CUTSCENE_IS_SCENE_RUNNING(scriptData.cutscene, 2)	
		IF NOT IS_BIT_SET(cutsceneData.iBS, 2)
			SET_ENTITY_VISIBLE(cutsceneData.pedPlayerActors[0], FALSE)
			SET_ENTITY_VISIBLE(cutsceneData.vehClone, FALSE)
			SET_ENTITY_VISIBLE(cutsceneData.pedActors[0], FALSE)
			SET_ENTITY_VISIBLE(cutsceneData.pedActors[1], TRUE)
			SET_ENTITY_VISIBLE(cutsceneData.vehActors[0], TRUE)
			SET_BIT(cutsceneData.iBS, 2)
		ENDIF
	ENDIF
	
ENDPROC

PROC GET_AREA_FROM_ENTITY(VECTOR vPos, FLOAT fHeading, MODEL_NAMES eModel, VECTOR &vCoords1, VECTOR &vCoords2, FLOAT &fWidth, FLOAT fMarginOfError = 0.25)
	VECTOR vForward = <<0.0, 1.0, 0.0>>
	RotateVec(vForward, <<0.0, 0.0, fHeading>> )
	vForward /= VMAG(vForward)

	VECTOR vMin, vMax
	SAFE_GET_MODEL_DIMENSIONS(eModel, vMin, vMax)
	//FLOAT fLength, 
	FLOAT fHeight
	
	//fLength = ABSF(vMax.y - vMin.y)
	fWidth = ABSF(vMax.x - vMin.x) + fMarginOfError * 2.0
	fHeight = ABSF(vMax.z - vMin.z)
	
	vCoords1 = vPos + (vForward * (vMax.y + fMarginOfError))
	vCoords1.z = (vCoords1.z - ((0.5 * fHeight) + fMarginOfError)) 
	
	vCoords2 = vPos - (vForward * ((vMin.y * -1.0) + fMarginOfError))
	vCoords2.z = (vCoords2.z + ((0.5 * fHeight) + fMarginOfError))  
ENDPROC

FUNC BOOL IS_POINT_WITHIN_ENTITY_BOUNDS(VECTOR vPoint, VECTOR vEntityPos, FLOAT fEntityHeading, MODEL_NAMES eModel, FLOAT fMarginOfError = 0.1)
	VECTOR vCoords1, vCoords2
	FLOAT fWidth 
		
	GET_AREA_FROM_ENTITY(vEntityPos, fEntityHeading, eModel, vCoords1, vCoords2, fWidth, fMarginOfError)
	IF IS_POINT_IN_ANGLED_AREA(vPoint, vCoords1, vCoords2, fWidth)			
		RETURN TRUE	
	ENDIF

	RETURN FALSE
ENDFUNC

PROC _GET_PED_WALKING_FROM_POS_AFTER_RAMP(INT iPedIndex, VECTOR &vCoords, FLOAT &fHeading)
	IF scriptData.eDropoffID = IE_DROPOFF_PRIVATE_BUYER_5
		SWITCH iPedIndex
			CASE 0
			    vCoords = <<1572.4125, 6443.3989, 23.4058>>
			    fHeading = 294.8358
			BREAK
			CASE 1
			    vCoords = <<1571.5735, 6442.0757, 23.4487>>
			    fHeading = 291.4287
			BREAK
			CASE 2
			    vCoords = <<1573.7751, 6439.9800, 23.5818>>
			    fHeading = 294.9070
			BREAK
			CASE 3
			    vCoords = <<1572.5906, 6441.0493, 23.5172>>
			    fHeading = 291.1812
			BREAK
		ENDSWITCH
	ELIF scriptData.eDropoffID = IE_DROPOFF_SHOWROOM_2
		SWITCH iPedIndex
			CASE 0
			    vCoords = <<2536.8113, 2576.6440, 36.9449>>
			    fHeading = 269.6400
			BREAK
			CASE 1
			    vCoords = <<2537.0215, 2574.8997, 36.9449>>
			    fHeading = 269.6400
			BREAK
			CASE 2
			    vCoords = <<2535.3608, 2575.7786, 36.9449>>
			    fHeading = 269.6400
			BREAK
			CASE 3
			    vCoords = <<2535.8025, 2574.0146, 36.9449>>
			    fHeading = 269.6400
			BREAK
		ENDSWITCH
	ELIF scriptData.eDropoffID = IE_DROPOFF_SHOWROOM_3
		SWITCH iPedIndex
			CASE 0
			    vCoords = <<460.1684, 3582.1206, 32.3333>>
			    fHeading = 169.2000
			BREAK
			CASE 1
			    vCoords = <<458.2932, 3582.2415, 32.3305>>
			    fHeading = 169.2000
			BREAK
			CASE 2
			    vCoords = <<459.2275, 3583.1548, 32.3407>>
			    fHeading = 169.2000
			BREAK
			CASE 3
			    vCoords = <<457.2249, 3583.2527, 32.3372>>
			    fHeading = 169.2000
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

/// PURPOSE:
///    Cords set to truck right before leaving
/// PARAMS:
///    vCoords - 
///    fHeading - 
PROC _GET_TRUCK_COORDS_FOR_LEAVING(VECTOR &vCoords, FLOAT &fHeading)
	IF scriptData.eDropoffID = IE_DROPOFF_PRIVATE_BUYER_5
		vCoords = <<1568.5781, 6449.5093, 23.3028>>
		fHeading = 26.0363
	ELIF scriptData.eDropoffID = IE_DROPOFF_SHOWROOM_2
		vCoords = <<2535.4744, 2583.9180, 38.0613>>
		fHeading = 7.2000
	ELIF scriptData.eDropoffID = IE_DROPOFF_SHOWROOM_3
		vCoords = <<467.0912, 3581.1296, 33.4077>>
		fHeading = -101.5200
	ENDIF
ENDPROC

PROC IE_DELIVERY_CONTROL_VEH_ACTORS_ON_RAMP(INT iSceneShift = 0)
	IF NOT DOES_ENTITY_EXIST(cutsceneData.vehActors[0])
	OR IS_ENTITY_DEAD(cutsceneData.vehActors[0])
		EXIT
	ENDIF
	
	// Truck comes from afar... (not used in cargobob variation)
	IF NOT scriptData.bViaCargobob
		IF SIMPLE_CUTSCENE_IS_SCENE_RUNNING(scriptData.cutscene, 0)
		AND NOT IS_BIT_SET(cutsceneData.iBS, 13)
		AND DOES_ENTITY_EXIST(cutsceneData.pedActors[0])
		AND NOT IS_ENTITY_DEAD(cutsceneData.pedActors[0])
		
			VECTOR vFinalCoords, vFoo, vStartCoords
			FLOAT fStartHeading
			MODEL_NAMES modelFoo
		
			IF scriptData.eDropoffID = IE_DROPOFF_PRIVATE_BUYER_5
				vStartCoords = <<1644.8270, 6399.1289, 28.2834>>
				fStartHeading = 70.5670
				
				// Drive to coords are final truck coords
				IE_DELIVERY_GET_DROPOFF_VEHICLE_ACTOR_DATA(scriptData.eDropoffID, 0, modelFoo, vFinalCoords, vFoo)
				vFinalCoords = TRANSFORM_CUTSCENE_COORDS_TO_WORLD_COORDS(vFinalCoords)
				
			ELIF scriptData.eDropoffID = IE_DROPOFF_SHOWROOM_2
				vStartCoords = <<2556.6047, 2665.8142, 39.9146>>
				fStartHeading = -159.1304
				
				// Predefined final coords so truck maintains left-to-right direction
				//vFinalCoords = <<2596.3965, 2543.6609, 30.7569>>
				IE_DELIVERY_GET_DROPOFF_VEHICLE_ACTOR_DATA(scriptData.eDropoffID, 0, modelFoo, vFinalCoords, vFoo)
				vFinalCoords = TRANSFORM_CUTSCENE_COORDS_TO_WORLD_COORDS(vFinalCoords)
				
			ELIF scriptData.eDropoffID = IE_DROPOFF_SHOWROOM_3
				vStartCoords = <<415.3262, 3510.7429, 33.5012>>
				fStartHeading = 2.4387
				
				// Drive to coords are final truck coords
				IE_DELIVERY_GET_DROPOFF_VEHICLE_ACTOR_DATA(scriptData.eDropoffID, 0, modelFoo, vFinalCoords, vFoo)
				vFinalCoords = TRANSFORM_CUTSCENE_COORDS_TO_WORLD_COORDS(vFinalCoords)
			ENDIF
			
			SET_ENTITY_COORDS_NO_OFFSET(cutsceneData.vehActors[0], vStartCoords)
			SET_ENTITY_HEADING(cutsceneData.vehActors[0], fStartHeading)
			SET_VEHICLE_ON_GROUND_PROPERLY(cutsceneData.vehActors[0])
			
			DISABLE_COLLISION_OF_ENTITY_WITH_NEARBY_NETWORKED_VEHS(cutsceneData.vehActors[0], cutsceneData.pedActors[0])
			TASK_VEHICLE_DRIVE_TO_COORD(cutsceneData.pedActors[0], cutsceneData.vehActors[0], vFinalCoords, 30.0, DRIVINGSTYLE_RACING, DUMMY_MODEL_FOR_SCRIPT, DRIVINGMODE_PLOUGHTHROUGH, 2.0, -1)
			SET_VEHICLE_FORWARD_SPEED(cutsceneData.vehActors[0], 5.0)
			
			SET_BIT(cutsceneData.iBS, 13)
		ENDIF
		
		// Now set the truck in correct position for veh to go on the ramp
		IF SIMPLE_CUTSCENE_IS_SCENE_RUNNING(scriptData.cutscene, 1)
		AND NOT IS_BIT_SET(cutsceneData.iBS, 14)
		AND DOES_ENTITY_EXIST(cutsceneData.pedActors[0])
		AND NOT IS_ENTITY_DEAD(cutsceneData.pedActors[0])
			
			CLEAR_PED_TASKS_IMMEDIATELY(cutsceneData.pedActors[0])
			SET_PED_INTO_VEHICLE(cutsceneData.pedActors[0], cutsceneData.vehActors[0])
			
			VECTOR vFinalCoords, vFinalRot
			MODEL_NAMES modelFoo
			IE_DELIVERY_GET_DROPOFF_VEHICLE_ACTOR_DATA(scriptData.eDropoffID, 0, modelFoo, vFinalCoords, vFinalRot)
			vFinalCoords = TRANSFORM_CUTSCENE_COORDS_TO_WORLD_COORDS(vFinalCoords)
			vFinalRot.Z = TRANSFORM_CUTSCENE_HEADING_TO_WORLD_HEADING(vFinalRot.Z)	
			
			SET_ENTITY_COORDS(cutsceneData.vehActors[0], vFinalCoords)
			SET_ENTITY_HEADING(cutsceneData.vehActors[0], vFinalRot.Z)
			SET_VEHICLE_ON_GROUND_PROPERLY(cutsceneData.vehActors[0])
			
				
			SET_BIT(cutsceneData.iBS, 14)
		ENDIF
	ENDIF
	
	// And this is the truck driving away
	IF SIMPLE_CUTSCENE_IS_SCENE_RUNNING(scriptData.cutscene, 3 + iSceneShift)
		// Set players peds walk away
		IF NOT IS_BIT_SET(cutsceneData.iBS, 9)
			INT i
			VECTOR vCoords, vDir
			FLOAT fHeading
			SEQUENCE_INDEX seqID[4]
			REPEAT COUNT_OF(cutsceneData.pedPlayerActors) i
				IF DOES_ENTITY_EXIST(cutsceneData.pedPlayerActors[i])
				AND NOT IS_ENTITY_DEAD(cutsceneData.pedPlayerActors[i])
					//DELETE_PED(cutsceneData.pedPlayerActors[i])
					_GET_PED_WALKING_FROM_POS_AFTER_RAMP(i, vCoords, fHeading)
					vDir = GET_HEADING_AS_VECTOR(fHeading)
					CLEAR_PED_TASKS_IMMEDIATELY(cutsceneData.pedPlayerActors[i])
					SET_ENTITY_COORDS(cutsceneData.pedPlayerActors[i], vCoords)
					SET_ENTITY_HEADING(cutsceneData.pedPlayerActors[i], fHeading)
					
					OPEN_SEQUENCE_TASK(seqID[i])
						TASK_STAND_STILL(NULL, GET_RANDOM_INT_IN_RANGE(0, 500) + 250)
						TASK_GO_STRAIGHT_TO_COORD(NULL, vCoords + vDir * 30.0, PEDMOVE_WALK)
					CLOSE_SEQUENCE_TASK(seqID[i])
					TASK_PERFORM_SEQUENCE_LOCALLY(cutsceneData.pedPlayerActors[i], seqID[i])
				ENDIF
			ENDREPEAT
			SET_BIT(cutsceneData.iBS, 9)
		ENDIF
		
		// Attach veh on flatbed to flatbed
		IF NOT cutsceneData.bVehCloneAttachedToProxy
		//AND scriptData.cutscene.iCurrentSceneElapsedTime > 100
			IF DOES_ENTITY_EXIST(cutsceneData.vehClone) AND NOT IS_ENTITY_DEAD(cutsceneData.vehClone)
				cutsceneData.vVehAttachedOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(cutsceneData.vehActors[0], GET_ENTITY_COORDS(cutsceneData.vehClone))
				cutsceneData.bVehCloneAttachedToProxy = TRUE
				cutsceneData.vVehAttachedOffset.X = 0.0
				cutsceneData.vVehAttachedOffset.Y = -2.0
				//cutsceneData.vVehAttachedOffset.Z += 0.05
				ATTACH_ENTITY_TO_ENTITY(cutsceneData.vehClone, cutsceneData.vehActors[0], 0, cutsceneData.vVehAttachedOffset, <<0.0, 0.0, 0.0>>)
			ENDIF
		ENDIF
		
		// Task truck with driving away
		IF NOT IS_ENTITY_DEAD(cutsceneData.pedActors[0])
		AND NOT IS_BIT_SET(cutsceneData.iBS, 3)
			SET_VEHICLE_ENGINE_ON(cutsceneData.vehActors[0], TRUE, TRUE)

			VECTOR vCoords
			FLOAT fHeading
			_GET_TRUCK_COORDS_FOR_LEAVING(vCoords, fHeading)
			SET_ENTITY_COORDS_NO_OFFSET(cutsceneData.vehActors[0], vCoords)
			SET_ENTITY_HEADING(cutsceneData.vehActors[0], fHeading)
			SET_VEHICLE_ON_GROUND_PROPERLY(cutsceneData.vehActors[0])
			VECTOR vDir = GET_HEADING_AS_VECTOR(fHeading)
			
			TASK_VEHICLE_DRIVE_TO_COORD(cutsceneData.pedActors[0], cutsceneData.vehActors[0], vCoords + vDir * 40.0, 5.0, DRIVINGSTYLE_STRAIGHTLINE, DUMMY_MODEL_FOR_SCRIPT, DRIVINGMODE_PLOUGHTHROUGH, 2.0, -1)

			SET_BIT(cutsceneData.iBS, 3)
		ENDIF
		
		// Truck horn when driving away
		IF NOT IS_BIT_SET(cutsceneData.iBS, 4)
		AND scriptData.cutscene.iCurrentSceneElapsedTime > 1000
			SET_HORN_PERMANENTLY_ON_TIME(cutsceneData.vehActors[0], 1000)
			SET_BIT(cutsceneData.iBS, 4)
		ENDIF
	ENDIF
ENDPROC

FUNC VECTOR GET_FLATBED_RAIL_GROUND_COORDS(VEHICLE_INDEX vehFlatBed)
	VECTOR vFlatBedTailOffset = <<0.422, 2.9197,  -0.6070>>// -0.3970>>
	
	FLOAT fFlatbedHeading = GET_ENTITY_HEADING(vehFlatBed)
	vFlatBedTailOffset = ROTATE_VECTOR_ABOUT_Z(vFlatBedTailOffset, fFlatbedHeading)
	
	VECTOR vFlatbedRailGround
	vFlatbedRailGround = GET_ENTITY_COORDS(vehFlatBed) - vFlatBedTailOffset
	
	RETURN vFlatbedRailGround
ENDFUNC

PROC IE_DELIVERY_CONTROL_VEH_ACTORS_ON_RAMP_CARGOBOB()
	IF NOT DOES_ENTITY_EXIST(cutsceneData.vehActors[0])
	OR IS_ENTITY_DEAD(cutsceneData.vehActors[0])
		EXIT
	ENDIF
	
	IF scriptData.cutscene.iCurrentScene = 1
	AND IS_BIT_SET(scriptData.cutscene.iBSScenesInit, 1)
		SET_ENTITY_COLLISION(cutsceneData.vehClone, FALSE)
		IF NOT IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(cutsceneData.vehClone)
			FREEZE_ENTITY_POSITION(cutsceneData.vehClone, TRUE)
		ENDIF
		IF NOT cutsceneData.bVehCloneAttachedToProxy
		AND scriptData.cutscene.iCurrentSceneElapsedTime > 100
			IF DOES_ENTITY_EXIST(cutsceneData.vehClone) AND NOT IS_ENTITY_DEAD(cutsceneData.vehClone)
				cutsceneData.vVehAttachedOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(cutsceneData.vehActors[0], GET_ENTITY_COORDS(cutsceneData.vehClone))
				cutsceneData.bVehCloneAttachedToProxy = TRUE
				cutsceneData.vVehAttachedOffset.X = 0.0
				ATTACH_ENTITY_TO_ENTITY(cutsceneData.vehClone, cutsceneData.vehActors[0], 0, cutsceneData.vVehAttachedOffset, <<0.0, 0.0, 0.0>>)
			ENDIF
		ENDIF
		
		IF NOT IS_ENTITY_DEAD(cutsceneData.pedActors[0])
		AND NOT IS_BIT_SET(cutsceneData.iBS, 3)
			SET_VEHICLE_ENGINE_ON(cutsceneData.vehActors[0], TRUE, TRUE)
			TASK_VEHICLE_DRIVE_WANDER(cutsceneData.pedActors[0], cutsceneData.vehActors[0], 15.0, DRIVINGMODE_AVOIDCARS)
			SET_BIT(cutsceneData.iBS, 3)
		ENDIF
	ENDIF
ENDPROC

PROC IE_DELIVERY_CONTROL_PROPS(INT iShiftScene = 0)
	INT i
	IE_PROP_TRANSITION_DATA transData

	// IE_PROP_TRANSITION_DATA sPropsTransitions[4]
	// INT iPropsTransitionInitBS[SIMPLE_CUTSCENE_MAX_SCENES]
	
	IF scriptData.cutscene.iCurrentScene + iShiftScene < 0
		EXIT
	ENDIF
	
	IF IS_BIT_SET(scriptData.cutscene.iBSScenesInit, scriptData.cutscene.iCurrentScene)
		IF NOT IS_BIT_SET(cutsceneData.iPropsTransitionInitBS, scriptData.cutscene.iCurrentScene)
			// Start of new scene - see if anything will be transitioned during this scene
			REPEAT COUNT_OF(cutsceneData.objProps) i
				transData.bDoTranslation = FALSE
				transData.bDoRotation = FALSE
				transData.bVisible = FALSE
				transData.iDelay = 0
			
				IF DOES_ENTITY_EXIST(cutsceneData.objProps[i]) 
					IE_DELIVERY_GET_DROPOFF_PROP_TRANSITION_DATA(scriptData.eDropoffID, i, scriptData.cutscene.iCurrentScene + iShiftScene, transData)
					
					IF transData.bDoTranslation
					OR transData.bDoRotation
						// Overwrite whatever transition was happening if we have a new one
						cutsceneData.sPropsTransitions[i].sData = transData
						cutsceneData.sPropsTransitions[i].iStartTime = scriptData.cutscene.iTotalElapsedTime
					ENDIF
					
					cutsceneData.sPropsTransitions[i].sData.bVisible = transData.bVisible
				ENDIF
			ENDREPEAT
			
			SET_BIT(cutsceneData.iPropsTransitionInitBS, scriptData.cutscene.iCurrentScene)
		ENDIF
	ELSE
		EXIT // dont do any transitions if scene's not init
	ENDIF
		
	REPEAT COUNT_OF(cutsceneData.objProps) i
		IF DOES_ENTITY_EXIST(cutsceneData.objProps[i]) 
			transData = cutsceneData.sPropsTransitions[i].sData
			
			SET_ENTITY_VISIBLE(cutsceneData.objProps[i], transData.bVisible)
			IF NOT IS_ENTITY_ATTACHED(cutsceneData.objProps[i])
				SET_ENTITY_COLLISION(cutsceneData.objProps[i], transData.bVisible)
			ENDIF
			
			IF transData.bDoTranslation OR transData.bDoRotation
				INT iTransitionTime = scriptData.cutscene.iTotalElapsedTime - cutsceneData.sPropsTransitions[i].iStartTime - transData.iDelay
				FLOAT fTimeFactor = FMIN(TO_FLOAT(iTransitionTime) / TO_FLOAT(transData.iDuration), 1.0)
				
				IF (scriptData.cutscene.iTotalElapsedTime - cutsceneData.sPropsTransitions[i].iStartTime) >= transData.iDelay
					IF transData.bDoTranslation
						FLOAT fTranslateFactor = fTimeFactor
						SWITCH transData.eGraphType
							CASE IE_GRAPH_ACCEL fTranslateFactor = GET_GRAPH_TYPE_ACCEL(fTimeFactor) BREAK
							CASE IE_GRAPH_DECEL fTranslateFactor = GET_GRAPH_TYPE_DECEL(fTimeFactor) BREAK
							CASE IE_GRAPH_ACCEL_DECEL fTranslateFactor = GET_GRAPH_TYPE_SIN_ACCEL_DECEL(fTimeFactor) BREAK
						ENDSWITCH
						
						VECTOR vPropOffset = transData.vEndPos - transData.vStartPos
						VECTOR vPropPos = transData.vStartPos + (vPropOffset * fTranslateFactor)
						SET_ENTITY_COORDS(cutsceneData.objProps[i], TRANSFORM_CUTSCENE_COORDS_TO_WORLD_COORDS(vPropPos))
					ENDIF
					
					IF transData.bDoRotation
						FLOAT fRotateFactor = fTimeFactor
						SWITCH transData.eGraphType
							CASE IE_GRAPH_ACCEL fRotateFactor = GET_GRAPH_TYPE_ACCEL(fTimeFactor) BREAK
							CASE IE_GRAPH_DECEL fRotateFactor = GET_GRAPH_TYPE_DECEL(fTimeFactor) BREAK
							CASE IE_GRAPH_ACCEL_DECEL fRotateFactor = GET_GRAPH_TYPE_SIN_ACCEL_DECEL(fTimeFactor) BREAK
						ENDSWITCH
						
						VECTOR vRotOffset = transData.vEndRot - transData.vStartRot
						VECTOR vPropRot = transData.vStartRot + (vRotOffset * fRotateFactor)
						vPropRot.Z = TRANSFORM_CUTSCENE_HEADING_TO_WORLD_HEADING(vPropRot.Z)
						SET_ENTITY_ROTATION(cutsceneData.objProps[i], vPropRot)
					ENDIF
				ELSE
					IF transData.bDoTranslation
						SET_ENTITY_COORDS(cutsceneData.objProps[i], TRANSFORM_CUTSCENE_COORDS_TO_WORLD_COORDS(transData.vStartPos))
					ENDIF
					
					IF transData.bDoRotation
						SET_ENTITY_ROTATION(cutsceneData.objProps[i], transData.vStartRot)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF NOT scriptData.bViaCargobob
		IF scriptDatA.eDropoffID = IE_DROPOFF_POLICE_STATION_2
			IF scriptData.cutscene.iCurrentScene = 0 + iShiftScene
				// Force open any doors, gates and barriers.
				INT iDoorHash = IE_DELIVERY_GET_DOOR_HASH(scriptData.eDropoffID)

				IF iDoorHash != 0
					IF NOT IS_BIT_SET(cutsceneData.iBS, BS_IE_DELIVERY_CUTSCENE_OPEN_DOOR)
						IF IE_DELIVERY_OPEN_DOOR(iDoorHash, 1.0, TRUE, TRUE)
							SET_BIT(cutsceneData.iBS, BS_IE_DELIVERY_CUTSCENE_OPEN_DOOR)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    This controls the cargobob descending with a vehicle attached to the hook.
///    

PROC IE_DELIVERY_CONTROL_VEH_PROXY_CARGOBOB()
	VECTOR vStartCoords, vFinalCoords, vDepartCoords
	FLOAT fStartHeading, fFinalHeading, fDepartHeading
	BOOL bForceCoords
	
	IF IE_DELIVERY_GET_DROPOFF_CARGOBOB_DATA(scriptData.eDropoffID, vStartCoords, fStartHeading, vFinalCoords, fFinalHeading, vDepartCoords, fDepartHeading, bForceCoords)
		
		IF bForceCoords
			cutsceneData.vCargobobFinalPos = vFinalCoords
		ELSE
			IF NOT IS_BIT_SET(cutsceneData.iBS, 30)
				// Calculate cargobob final pos so that delievred vehicle touches the ground
				VECTOR vCargoOffset = vStartCoords - GET_ENTITY_COORDS(cutsceneData.vehClone)
				VECTOR vModelMin, vModelMax
				GET_MODEL_DIMENSIONS(scriptData.modelVehDelivered, vModelMin, vModelMax)
				FLOAT fGroundZ
				IF GET_GROUND_Z_FOR_3D_COORD(vFinalCoords, fGroundZ)
					
					IF DOES_ENTITY_EXIST(cutsceneData.vehActors[0])
						VECTOR vFlatbedGround
						vFlatbedGround = GET_FLATBED_RAIL_GROUND_COORDS(cutsceneData.vehActors[0])
						fGroundZ = vFlatbedGround.z
					ENDIF
					cutsceneData.vCargobobFinalPos = <<vFinalCoords.X, vFinalCoords.Y, fGroundZ - vModelMin.Z + vCargoOffset.Z>>
					vFinalCoords = cutsceneData.vCargobobFinalPos
					SET_BIT(cutsceneData.iBS, 30)
				ENDIF
			ELSE
				vFinalCoords = cutsceneData.vCargobobFinalPos
			ENDIF
		ENDIF
		
		FLOAT fTimeFactor
		INT iMaxTimeDescending = scriptData.cutscene.sScenes[0].iDuration + 250
		
		IF scriptData.cutscene.iCurrentScene = 0
		AND IS_BIT_SET(scriptData.cutscene.iBSScenesInit, scriptData.cutscene.iCurrentScene)
			fTimeFactor = FMIN(TO_FLOAT(scriptData.cutscene.iTotalElapsedTime) / TO_FLOAT(iMaxTimeDescending), 1.0)
			
			VECTOR vTotalOffset = vFinalCoords - vStartCoords
			VECTOR vCurrentPos = vStartCoords + vTotalOffset * GET_GRAPH_TYPE_DECEL(fTimeFactor)
			
			FLOAT fTotalRotOffset = fFinalHeading - fStartHeading
			FLOAT fCurrentHeading = fStartHeading + fTotalRotOffset * GET_GRAPH_TYPE_DECEL(fTimeFactor)
			
			IF DOES_ENTITY_EXIST(cutsceneData.vehProxyClone)
			AND NOT IS_ENTITY_DEAD(cutsceneData.vehProxyClone)
				SET_ENTITY_COLLISION(cutsceneData.vehProxyClone, FALSE)
				SET_ENTITY_HEADING(cutsceneData.vehProxyClone, fCurrentHeading)
				FREEZE_ENTITY_POSITION(cutsceneData.vehProxyClone, TRUE)
				SET_ENTITY_COORDS_NO_OFFSET(cutsceneData.vehProxyClone, vCurrentPos)
			ENDIF
			
			/*
			IF DOES_ENTITY_EXIST(cutsceneData.pedProxyMechanic)
			AND NOT IS_ENTITY_DEAD(cutsceneData.pedProxyMechanic)
			AND NOT IS_BIT_SET(cutsceneData.iBS, 29)
			AND scriptData.cutscene.iCurrentSceneElapsedTime > 0
				IF DOES_ENTITY_EXIST(cutsceneData.vehClone)
				AND NOT IS_ENTITY_DEAD(cutsceneData.vehClone)
					TASK_ENTER_VEHICLE(cutsceneData.pedProxyMechanic, cutsceneData.vehClone)
					SET_BIT(cutsceneData.iBS, 29)
					#IF IS_DEBUG_BUILD
						PRINTLN("[IE_DELIVERY][CUTSCENE] IE_DELIVERY_CONTROL_VEH_PROXY_CARGOBOB - Tasked mechanic with getting into the car.")
					#ENDIF
				ENDIF
			ENDIF
			*/
			
			/*
			IF DOES_ENTITY_EXIST(cutsceneData.pedProxyMechanic)
			AND NOT IS_ENTITY_DEAD(cutsceneData.pedProxyMechanic)
			AND NOT IS_BIT_SET(cutsceneData.iBS, 29)
				IF DOES_ENTITY_EXIST(cutsceneData.vehClone)
				AND NOT IS_ENTITY_DEAD(cutsceneData.vehClone)
					VECTOR vModelMin, vModelMax
					GET_MODEL_DIMENSIONS(scriptData.modelVehDelivered, vModelMin, vModelMax)
					VECTOR vEntityGroundCoords = GET_ENTITY_COORDS(cutsceneData.vehClone) + vModelMin
					VECTOR vFinalGroundPos = GET_ENTITY_COORDS(cutsceneData.vehClone)
					
					IF GET_GROUND_Z_FOR_3D_COORD(vFinalGroundPos, vFinalGroundPos.Z)
					AND vFinalGroundPos.Z + 3.0 >= vEntityGroundCoords.Z
						TASK_ENTER_VEHICLE(cutsceneData.pedProxyMechanic, cutsceneData.vehClone)
						SET_BIT(cutsceneData.iBS, 29)
						
					ENDIF
				ENDIF
			ENDIF
			*/
		
		ELIF IS_BIT_SET(scriptData.cutscene.iBSScenesInit, scriptData.cutscene.iCurrentScene) // Remaining scene use IE_DELIVERY_CONTROL_VEH_CLONE_IN_GARAGE_ALT to control the clone and cargobob just flies away
			IF DOES_ENTITY_EXIST(cutsceneData.vehClone)
			AND NOT IS_ENTITY_DEAD(cutsceneData.vehClone)
				IF DOES_ENTITY_EXIST(cutsceneData.vehProxyClone)
				AND NOT IS_ENTITY_DEAD(cutsceneData.vehProxyClone)
				AND DOES_ENTITY_EXIST(cutsceneData.pedProxyActors[0])
				AND NOT IS_ENTITY_DEAD(cutsceneData.pedProxyActors[0])
					IF NOT IS_BIT_SET(cutsceneData.iBS, 10)
						SET_ENTITY_COLLISION(cutsceneData.vehProxyClone, TRUE)
						FREEZE_ENTITY_POSITION(cutsceneData.vehProxyClone, FALSE)
						REMOVE_PICK_UP_ROPE_FOR_CARGOBOB(cutsceneData.vehProxyClone)
						SET_ENTITY_COORDS_NO_OFFSET(cutsceneData.vehProxyClone, vDepartCoords)
						SET_ENTITY_HEADING(cutsceneData.vehProxyClone, fDepartHeading)
						IE_CUTSCENE_ATTACH_VEH_CLONE_TO_CARGOBOB(FALSE)
						//SET_CARGOBOB_EXCLUDE_FROM_PICKUP_ENTITY(cutsceneData.vehProxyClone, cutsceneData.vehClone)
						
						// Position veh clone in front of the garage door
						VECTOR vNewCloneCoords
						FLOAT fNewCloneRot
						IE_DELIVERY_GET_DROPOFF_VEHICLE_CLONE_DATA_CARGOBOB(scriptData.eDropoffID, vNewCloneCoords, fNewCloneRot)
						SET_ENTITY_COLLISION(cutsceneData.vehClone, TRUE)						
						IF scriptData.eDropoffID = IE_DROPOFF_PRIVATE_BUYER_5
						OR scriptData.eDropoffID = IE_DROPOFF_SHOWROOM_2
						OR scriptData.eDropoffID = IE_DROPOFF_SHOWROOM_3
							FREEZE_ENTITY_POSITION(cutsceneData.vehClone, TRUE)
							SET_ENTITY_COORDS_NO_OFFSET(cutsceneData.vehClone, vNewCloneCoords)
						ELSE
							vNewCloneCoords = TRANSFORM_CUTSCENE_COORDS_TO_WORLD_COORDS(vNewCloneCoords)
							fNewCloneRot = TRANSFORM_CUTSCENE_HEADING_TO_WORLD_HEADING(fNewCloneRot)
							FREEZE_ENTITY_POSITION(cutsceneData.vehClone, FALSE)
							SET_ENTITY_COORDS(cutsceneData.vehClone, vNewCloneCoords)
							SET_ENTITY_HEADING(cutsceneData.vehClone, fNewCloneRot)
						ENDIF
						
						TASK_HELI_MISSION(cutsceneData.pedProxyActors[0], cutsceneData.vehProxyClone, NULL, NULL, scriptData.vDropoffCoords, MISSION_CIRCLE, 2.0, 10.0, 0.0, 100, 100)
						
						#IF IS_DEBUG_BUILD
							PRINTLN("[IE_DELIVERY][CUTSCENE] IE_DELIVERY_CONTROL_VEH_PROXY_CARGOBOB - Heli tasked, car detached.")
						#ENDIF
						
						SET_BIT(cutsceneData.iBS, 10)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(cutsceneData.iBS, 31)
			TASK_HELI_MISSION(cutsceneData.pedProxyActors[0], cutsceneData.vehProxyClone, NULL, NULL, scriptData.vDropoffCoords, MISSION_LAND, 10.0, 2.0, 0.0, 0, 0)
			SET_BIT(cutsceneData.iBS, 31)
		ENDIF
	ENDIF
ENDPROC

FLOAT fMaintainProxyZ

PROC IE_DELIVERY_CONTROL_VEH_PROXY_TOWTRUCK()
	IF NOT DOES_ENTITY_EXIST(cutsceneData.vehClone) OR IS_ENTITY_DEAD(cutsceneData.vehClone)
	OR NOT DOES_ENTITY_EXIST(cutsceneData.vehProxyClone) OR IS_ENTITY_DEAD(cutsceneData.vehProxyClone)
		EXIT
	ENDIF
	
	FLOAT fTimeFactor
	FLOAT fMaxForwardSpeed = -0.7
	INT iTotalSceneTime = scriptData.cutscene.sScenes[0].iDuration
	//INT iMaxTimeLowering = scriptData.cutscene.sScenes[0].iDuration + 250
		
	IF SIMPLE_CUTSCENE_IS_SCENE_RUNNING(scriptData.cutscene, 0)
		SET_VEHICLE_FORWARD_SPEED(cutsceneData.vehProxyClone, fMaxForwardSpeed)
		SET_VEHICLE_FORWARD_SPEED(cutsceneData.vehClone, fMaxForwardSpeed)
		
	ELIF SIMPLE_CUTSCENE_IS_SCENE_RUNNING(scriptData.cutscene, 1)
		IF NOT IS_BIT_SET(cutsceneData.iBS, 1)
			
			cutsceneData.vCachedStartPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(cutsceneData.vehProxyClone, <<0.0, -3.0, 0.0>>)
			VECTOR vCurrentCoords = GET_ENTITY_COORDS(cutsceneData.vehProxyClone)
			fMaintainProxyZ = vCurrentCoords.Z
			vCurrentCoords = GET_ENTITY_COORDS(cutsceneData.vehClone)
			cutsceneData.fMaintainZ = vCurrentCoords.Z
			
			IF NOT IS_ENTITY_DEAD(cutsceneData.vehProxyClone)
				FREEZE_ENTITY_POSITION(cutsceneData.vehProxyClone, TRUE)
				SET_ENTITY_COLLISION(cutsceneData.vehProxyClone, FALSE)
				
				//FREEZE_ENTITY_POSITION(cutsceneData.vehClone, TRUE)
				SET_ENTITY_COLLISION(cutsceneData.vehClone, FALSE)
				//SET_VEHICLE_ON_GROUND_PROPERLY(cutsceneData.vehClone)
			ENDIF
			
			SET_BIT(cutsceneData.iBS, 1)
		ENDIF
		
		fTimeFactor = FMIN(TO_FLOAT(scriptData.cutscene.iCurrentSceneElapsedTime) / TO_FLOAT(iTotalSceneTime), 1.0)
		
		VECTOR vStartPos = cutsceneData.vCachedStartPos
		//FLOAT fStartHeading
		VECTOR vDir = GET_HEADING_AS_VECTOR(GET_ENTITY_HEADING(cutsceneData.vehProxyClone))
		//IE_DELIVERY_GET_DROPOFF_TOWTRUCK_DATA(scriptData.eDropoffID, vStartPos, fStartHeading)
		
		// Calculate endpos to maintain maxspeed
		VECTOR vEndPos = vStartPos + vDir * (TO_FLOAT(iTotalSceneTime) / 1000.0) * fMaxForwardSpeed
		
		VECTOR vTotalOffset = vEndPos - vStartPos
		VECTOR vCurrentPos = vStartPos + vTotalOffset * fTimeFactor//GET_GRAPH_TYPE_DECEL(fTimeFactor)
		
		SET_ENTITY_COORDS_NO_OFFSET(cutsceneData.vehProxyClone, <<vCurrentPos.X, vCurrentPos.Y, fMaintainProxyZ>>)
	
		//fTimeFactor = FMAX(0.0, FMIN(TO_FLOAT(scriptData.cutscene.iTotalElapsedTime) / TO_FLOAT(iMaxTimeLowering), 1.0))
		//SET_VEHICLE_TOW_TRUCK_ARM_POSITION(cutsceneData.vehProxyClone, 1.0 - fTimeFactor)
		
		//IE_DELIVERY_CONTROL_VEH_CLONE_IN_GARAGE_ALT()
	ENDIF
	
ENDPROC

PROC IE_DELIVERY_CONTROL_SPREADER(INT iShiftScene = 0)
	IF scriptData.cutscene.iCurrentScene = 1 + iShiftScene
		IF NOT IS_BIT_SET(cutsceneData.iBS, 1)
		AND scriptData.cutscene.iCurrentSceneElapsedTime >= 1925
			IF DOES_ENTITY_EXIST(cutsceneData.objProps[0])
				PLAY_ENTITY_ANIM(cutsceneData.objProps[0], "Dock_crane_SLD_load", "map_objects", NORMAL_BLEND_IN, FALSE, TRUE)
				SET_BIT(cutsceneData.iBS, 1)
			ELSE
				#IF IS_DEBUG_BUILD
					PRINTLN("[IE_DELIVERY][CUTSCENE] IE_DELIVERY_CONTROL_SPREADER - Unable to play entity anim, crane spreader entity doesn't exist.")
				#ENDIF
			ENDIF
		ENDIF
		
	ELIF scriptData.cutscene.iCurrentScene = 2 + iShiftScene
		IF NOT IS_BIT_SET(cutsceneData.iBS, 0)
			// Attach crate to spreader
			ATTACH_ENTITY_TO_ENTITY(cutsceneData.objProps[1], cutsceneData.objProps[0], -1, <<0.0, 0.0, -3.39>> , <<0.0, 0.0, 0.0>>)
			SET_BIT(cutsceneData.iBS, 0)
		ENDIF
		
		// Open the spreader when spreader transition finishes
		IE_PROP_TRANSITION_DATA transData
		IE_DELIVERY_GET_DROPOFF_PROP_TRANSITION_DATA(scriptData.eDropoffID, 0, 2 - iShiftScene, transData)
		
		IF NOT IS_BIT_SET(cutsceneData.iBS, 3)
		AND scriptData.cutscene.iCurrentSceneElapsedTime >= transData.iDuration
			IF DOES_ENTITY_EXIST(cutsceneData.objProps[0])
				PLAY_ENTITY_ANIM(cutsceneData.objProps[0], "Dock_crane_SLD_unload", "map_objects", INSTANT_BLEND_IN, FALSE, TRUE)
				SET_BIT(cutsceneData.iBS, 3)
			ELSE
				#IF IS_DEBUG_BUILD
					PRINTLN("[IE_DELIVERY][CUTSCENE] IE_DELIVERY_MAINTAIN_CUTSCENE - Unable to play entity anim, crane spreader entity doesn't exist.")
				#ENDIF
			ENDIF
		ENDIF
		
	ELIF scriptData.cutscene.iCurrentScene = 3 + iShiftScene

	ENDIF
ENDPROC

PROC MAINTAIN_CUTSCENE_SOUNDS()
	IF scriptData.cutscene.bPlaying
		INT i, iCurrentTime
		REPEAT IE_DELIVERY_SOUND_LOOPS_COUNT i
		
			iCurrentTime = scriptData.cutscene.iTotalElapsedTime
			
			IF cutsceneData.sLoopDetails[i].iRelativeToScene > -1
			AND cutsceneData.sLoopDetails[i].iRelativeToScene < scriptData.cutscene.iScenesCount
				IF scriptData.cutscene.iCurrentScene = cutsceneData.sLoopDetails[i].iRelativeToScene
					iCurrentTime = scriptData.cutscene.iCurrentSceneElapsedTime
				ELSE
					RELOOP
				ENDIF
			ENDIF
		
			IF NOT IS_BIT_SET(cutsceneData.iSoundsStartedBS, i)
			AND NOT IS_STRING_NULL_OR_EMPTY(cutsceneData.sLoopDetails[i].txtSound)
			
				//#IF IS_DEBUG_BUILD
				//	PRINTLN("[IE_DELIVERY][CUTSCENE] MAINTAIN_CUTSCENE_SOUNDS - Checking ", i, " which starts at ", cutsceneData.sLoopDetails[i].iFrom, " and current time is ", iCurrentTime)
				//#ENDIF
				
				IF iCurrentTime >= cutsceneData.sLoopDetails[i].iFrom
					PLAY_SOUND_FROM_COORD(cutsceneData.iSoundLoopsID[i], cutsceneData.sLoopDetails[i].txtSound, scriptData.vGaragePos, cutsceneData.sLoopDetails[i].txtSoundset)
					SET_BIT(cutsceneData.iSoundsStartedBS, i)
					#IF IS_DEBUG_BUILD
						PRINTLN("[IE_DELIVERY][CUTSCENE] MAINTAIN_CUTSCENE_SOUNDS - Started to play sound loop ", i, " (", cutsceneData.sLoopDetails[i].txtSound ,")")
					#ENDIF
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(cutsceneData.iSoundsStartedBS, i)
			AND NOT IS_BIT_SET(cutsceneData.iSoundsFinishedBS, i)
				IF iCurrentTime >= cutsceneData.sLoopDetails[i].iTo
					STOP_SOUND(cutsceneData.iSoundLoopsID[i])
					SET_BIT(cutsceneData.iSoundsFinishedBS, i)
					#IF IS_DEBUG_BUILD
						PRINTLN("[IE_DELIVERY][CUTSCENE] MAINTAIN_CUTSCENE_SOUNDS - Stopped sound loop ", i, " stopping at ", iCurrentTime, " it's supposed to stop at ", cutsceneData.sLoopDetails[i].iTo)
					#ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		REPEAT IE_DELIVERY_SOUNDS_TO_PLAY_COUNT i
		
			iCurrentTime = scriptData.cutscene.iTotalElapsedTime
			IF cutsceneData.sSoundsToPlay[i].iRelativeToScene > -1
			AND cutsceneData.sSoundsToPlay[i].iRelativeToScene < scriptData.cutscene.iScenesCount
				IF scriptData.cutscene.iCurrentScene = cutsceneData.sSoundsToPlay[i].iRelativeToScene
					iCurrentTime = scriptData.cutscene.iCurrentSceneElapsedTime
				ELSE
					RELOOP
				ENDIF
			ENDIF
		
			IF NOT IS_BIT_SET(cutsceneData.iSoundsPlayedBS, i)
			AND NOT IS_STRING_NULL_OR_EMPTY(cutsceneData.sSoundsToPlay[i].txtSound)
			
				IF cutsceneData.sSoundsToPlay[i].fSyncScenePhase >= 0.0
					IF IS_SYNCHRONIZED_SCENE_RUNNING(cutsceneData.iSyncedSceneID)
						IF GET_SYNCHRONIZED_SCENE_PHASE(cutsceneData.iSyncedSceneID) >= cutsceneData.sSoundsToPlay[i].fSyncScenePhase
							IF DOES_ENTITY_EXIST(cutsceneData.objProps[0])
							AND NOT IS_ENTITY_DEAD(cutsceneData.objProps[0])
								PLAY_SOUND_FROM_ENTITY(-1, cutsceneData.sSoundsToPlay[i].txtSound, cutsceneData.objProps[0], cutsceneData.sSoundsToPlay[i].txtSoundset)
								SET_BIT(cutsceneData.iSoundsPlayedBS, i)
								#IF IS_DEBUG_BUILD
									PRINTLN("[IE_DELIVERY][CUTSCENE] MAINTAIN_CUTSCENE_SOUNDS - Played sound ", i, " (", cutsceneData.sSoundsToPlay[i].txtSound, ") from veh prop 0")
								#ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF iCurrentTime > cutsceneData.sSoundsToPlay[i].iTimeAt
						IF cutsceneData.sSoundsToPlay[i].bPlayFromVehClone
							IF NOT IS_ENTITY_DEAD(cutsceneData.vehClone)
								PLAY_SOUND_FROM_ENTITY(-1, cutsceneData.sSoundsToPlay[i].txtSound, cutsceneData.vehClone, cutsceneData.sSoundsToPlay[i].txtSoundset)
								SET_BIT(cutsceneData.iSoundsPlayedBS, i)
								#IF IS_DEBUG_BUILD
									PRINTLN("[IE_DELIVERY][CUTSCENE] MAINTAIN_CUTSCENE_SOUNDS - Played sound ", i, " (", cutsceneData.sSoundsToPlay[i].txtSound, ") from veh clone")
								#ENDIF
							ENDIF
						ELIF cutsceneData.sSoundsToPlay[i].iPlayFromVehActor > -1
							IF NOT IS_ENTITY_DEAD(cutsceneData.vehActors[cutsceneData.sSoundsToPlay[i].iPlayFromVehActor])
								PLAY_SOUND_FROM_ENTITY(-1, cutsceneData.sSoundsToPlay[i].txtSound, cutsceneData.vehActors[cutsceneData.sSoundsToPlay[i].iPlayFromVehActor], cutsceneData.sSoundsToPlay[i].txtSoundset)
								SET_BIT(cutsceneData.iSoundsPlayedBS, i)
								#IF IS_DEBUG_BUILD
									PRINTLN("[IE_DELIVERY][CUTSCENE] MAINTAIN_CUTSCENE_SOUNDS - Played sound ", i, " (", cutsceneData.sSoundsToPlay[i].txtSound, ") from veh actor ", cutsceneData.sSoundsToPlay[i].iPlayFromVehActor)
								#ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		// Audio scene
		IF NOT IS_BIT_SET(scriptData.iBS, BS_IE_DELIVERY_STARTED_AUDIO_SCENE)
		AND NOT IS_STRING_NULL_OR_EMPTY(cutsceneData.strAudioScene)
			IF scriptData.cutscene.iCurrentScene = cutsceneData.iSceneForAudioScene
				SET_BIT(scriptData.iBS, BS_IE_DELIVERY_STARTED_AUDIO_SCENE)
				START_AUDIO_SCENE(cutsceneData.strAudioScene)
				#IF IS_DEBUG_BUILD
					PRINTLN("[IE_DELIVERY][CUTSCENE] MAINTAIN_CUTSCENE_SOUNDS - Started audio scene ", cutsceneData.strAudioScene)
				#ENDIF
			ENDIF
		ENDIF
	ELSE
		IF cutsceneData.iSoundsStartedBS != cutsceneData.iSoundsFinishedBS
			INT i
			REPEAT IE_DELIVERY_SOUND_LOOPS_COUNT i
				IF IS_BIT_SET(cutsceneData.iSoundsStartedBS, i)
				AND NOT IS_BIT_SET(cutsceneData.iSoundsFinishedBS, i)
					STOP_SOUND(cutsceneData.iSoundLoopsID[i])
					SET_BIT(cutsceneData.iSoundsFinishedBS, i)
					#IF IS_DEBUG_BUILD
						PRINTLN("[IE_DELIVERY][CUTSCENE] MAINTAIN_CUTSCENE_SOUNDS - Stopped sound loop ", i, " because cutscene has finished")
					#ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
		
		IF IS_BIT_SET(scriptData.iBS, BS_IE_DELIVERY_STARTED_AUDIO_SCENE)
			STOP_AUDIO_SCENE(cutsceneData.strAudioScene)
			CLEAR_BIT(scriptData.iBS, BS_IE_DELIVERY_STARTED_AUDIO_SCENE)
			#IF IS_DEBUG_BUILD
				PRINTLN("[IE_DELIVERY][CUTSCENE] MAINTAIN_CUTSCENE_SOUNDS - Stopped audio scene ", cutsceneData.strAudioScene)
			#ENDIF
		ENDIF
		
		IF cutsceneData.iSoundsStartedBS != 0
			cutsceneData.iSoundsStartedBS = 0
		ENDIF
		
		IF cutsceneData.iSoundsFinishedBS != 0
			cutsceneData.iSoundsFinishedBS = 0
		ENDIF
		
		IF cutsceneData.iSoundsPlayedBS != 0
			cutsceneData.iSoundsPlayedBS = 0
		ENDIF
	ENDIF
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════╡ TERMINATE PROCEDURES  ╞════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC TERMINATE_IF_LOCAL_PLAYER_DEAD_OR_TOO_FAR()
	#IF IS_DEBUG_BUILD
		IF debugData.bEditMode
			EXIT
		ENDIF
	#ENDIF
	
	IF IS_ENTITY_DEAD(PLAYER_PED_ID())
		#IF IS_DEBUG_BUILD
			PRINTLN("[IE_DELIVERY][CUTSCENE] TERMINATE_IF_LOCAL_PLAYER_DEAD_OR_TOO_FAR - Player is dead, terminating.")
		#ENDIF
		
		SCRIPT_CLEANUP()
	ELSE
		IF IS_IE_DELIVERY_SCRIPT_STATE(IE_DELIVERY_SCRIPT_STATE_IDLE)
			VECTOR vPlayerPos = GET_PLAYER_COORDS(PLAYER_ID())
			VECTOR vDropoffPos = g_IEDeliveryData.vLaunchPoint
			vPlayerPos.Z = 0.0
			vDropoffPos.Z = 0.0
			FLOAT fDist = VDIST(vPlayerPos, vDropoffPos)
			
			IF fDist > (IE_DELIVERY_SCRIPT_LAUNCH_DISTANCE + 10.0)
				#IF IS_DEBUG_BUILD
					PRINTLN("[IE_DELIVERY][CUTSCENE] TERMINATE_IF_LOCAL_PLAYER_DEAD_OR_TOO_FAR - Player moved too far, terminating (fDist: ", fDist, ").")
				#ENDIF
				
				SCRIPT_CLEANUP()
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC TERMINATE_IF_NO_LONGER_THE_CLOSEST_DROPOFF()
	#IF IS_DEBUG_BUILD
		IF debugData.bEditMode
			EXIT
		ENDIF
	#ENDIF
	
	IF g_IEDeliveryData.eClosestActiveDropoffID != scriptData.eDropoffID
		
		#IF IS_DEBUG_BUILD
			PRINTLN("[IE_DELIVERY][CUTSCENE] TERMINATE_IF_NO_LONGER_THE_CLOSEST_DROPOFF - We're closer to a different dropoff now, terminating and will launch for the other dropoff.")
		#ENDIF
		
		SCRIPT_CLEANUP()
	ENDIF
ENDPROC

PROC TERMINATE_IF_PLAYER_LEAVES_VEHICLE()
	#IF IS_DEBUG_BUILD
		IF debugData.bEditMode
			EXIT
		ENDIF
	#ENDIF
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			#IF IS_DEBUG_BUILD
				PRINTLN("[IE_DELIVERY][CUTSCENE] TERMINATE_IF_PLAYER_LEAVES_VEHICLE - Player left their vehicle, terminating.")
			#ENDIF
			
			SCRIPT_CLEANUP()
		ENDIF
	ENDIF
ENDPROC

PROC TERMINATE_IF_PLAYER_DELIVERER_LEFT_OR_NOT_OK()
	#IF IS_DEBUG_BUILD
		IF debugData.bEditMode
			EXIT
		ENDIF
	#ENDIF
	
	IF NOT IS_NET_PLAYER_OK(scriptData.pDeliverer)
		#IF IS_DEBUG_BUILD
			PRINTLN("[IE_DELIVERY][CUTSCENE] TERMINATE_IF_PLAYER_DELIVERER_LEFT_OR_NOT_OK - Player deliverer is not ok! Terminating.")
		#ENDIF
		
		SCRIPT_CLEANUP()
	ENDIF
	
	IF scriptData.pDeliverer != IE_DELIVERY_GET_PLAYER_IN_CHARGE_OF_DELIVERY_OF_THIS_CAR(scriptData.vehDelivered)
		#IF IS_DEBUG_BUILD
			IF IE_DELIVERY_GET_PLAYER_IN_CHARGE_OF_DELIVERY_OF_THIS_CAR(scriptData.vehDelivered) = INVALID_PLAYER_INDEX()
				PRINTLN("[IE_DELIVERY][CUTSCENE] TERMINATE_IF_PLAYER_DELIVERER_LEFT_OR_NOT_OK - Player deliverer has changed, terminating (old deliverer: ", GET_PLAYER_NAME(scriptData.pDeliverer), ", new deliverer: INVALID")
			ELSE
				PRINTLN("[IE_DELIVERY][CUTSCENE] TERMINATE_IF_PLAYER_DELIVERER_LEFT_OR_NOT_OK - Player deliverer has changed, terminating (old deliverer: ", GET_PLAYER_NAME(scriptData.pDeliverer), ", new deliverer: ", GET_PLAYER_NAME(IE_DELIVERY_GET_PLAYER_IN_CHARGE_OF_DELIVERY_OF_THIS_CAR(scriptData.vehDelivered)), ")")
			ENDIF
		#ENDIF
		
		SCRIPT_CLEANUP()
	ENDIF
	
	PED_INDEX pedDeliverer = GET_PLAYER_PED(scriptData.pDeliverer)
	IF IS_ENTITY_DEAD(pedDeliverer) 
	OR NOT IS_PED_IN_ANY_VEHICLE(pedDeliverer)
		#IF IS_DEBUG_BUILD
			PRINTLN("[IE_DELIVERY][CUTSCENE] TERMINATE_IF_PLAYER_DELIVERER_LEFT_OR_NOT_OK - Player deliverer left vehicle, terminating.")
		#ENDIF
		
		SCRIPT_CLEANUP()
	ENDIF
	
	/*
	// TODO: update this to handle deliveries via cargobob and towtruck
	PED_INDEX pedDriver = GET_PED_IN_VEHICLE_SEAT(scriptData.vehDelivered, VS_DRIVER)
	
	IF IS_ENTITY_DEAD(pedDriver)
		#IF IS_DEBUG_BUILD
			PRINTLN("[IE_DELIVERY][CUTSCENE] TERMINATE_IF_PLAYER_DELIVERER_LEFT_OR_NOT_OK - Player driver entity is dead. Terminating.")
		#ENDIF
		
		SCRIPT_CLEANUP()
	ENDIF
	
	// Fix for B*2992795
	IF NOT IS_ENTITY_DEAD(pedDriver)
		IF NETWORK_GET_PLAYER_INDEX_FROM_PED(pedDriver) != scriptData.pDeliverer
			#IF IS_DEBUG_BUILD
				PRINTLN("[IE_DELIVERY][CUTSCENE] TERMINATE_IF_PLAYER_DELIVERER_LEFT_OR_NOT_OK - Player driver index does not match the ped driver index. Terminating.")
			#ENDIF
			
			SCRIPT_CLEANUP()
		ENDIF
	ENDIF
	*/
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════════╡ DEBUG STUFF  ╞═════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

#IF IS_DEBUG_BUILD

PROC CREATE_DEBUG_WIDGETS()
	TEXT_LABEL_63 str = "GB_IE_DELIVERY_CUTSCENE ("
	str += IE_DELIVERY_GET_DROPOFF_NAME_FOR_DEBUG(scriptData.eDropoffID)
	str += ")"
	TEXT_LABEL_23 strSlider
	INT i
	
	START_WIDGET_GROUP(str)
		ADD_WIDGET_BOOL("Kill script", debugData.bKillScript)
		ADD_WIDGET_BOOL("Dump spawn points", debugData.bDumpSpawnPoints)
		ADD_WIDGET_BOOL("Toggle spawn points visibility", debugData.bToggleSpawnPointsVisibility)
		ADD_WIDGET_BOOL("Edit mode", debugData.bEditMode)
		ADD_WIDGET_BOOL("Don't trigger cutscene", debugData.bDontTriggerCutscene)
		ADD_WIDGET_BOOL("via cargobob", scriptData.bViaCargobob)
		ADD_WIDGET_BOOL("Toggle IPL", debugData.bToggleIPL)
		ADD_WIDGET_BOOL("Dont send cleanup event", debugData.bDontSendCleanupBroadcast)
		ADD_WIDGET_BOOL("Draw garage bounds", debugData.bDrawGarageBounds)
		//ADD_WIDGET_BOOL("via towtruck", scriptData.bViaTowtruck)
		
		START_WIDGET_GROUP("Editing")			
			ADD_WIDGET_VECTOR_SLIDER("Base pos", debugData.vBasePos, -9999.9, 9999.9, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("Base heading", debugData.fBaseHeading, 0.0, 360.0, 0.1)
			
			ADD_WIDGET_VECTOR_SLIDER("Clone pos", debugData.vVehClonePos, -9999.9, 9999.9, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("Clone heading", debugData.fVehCloneHeading, 0.0, 360.0, 0.1)
			ADD_WIDGET_BOOL("Freeze clone", debugData.bFreezeVehClone)
			
			ADD_WIDGET_VECTOR_SLIDER("Proxy pos", debugData.vProxyPos, -9999.9, 9999.9, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("Proxy heading", debugData.fProxyHeading, 0.0, 360.0, 0.1)
			ADD_WIDGET_BOOL("Freeze proxy", debugData.bFreezeProxyClone)
			ADD_WIDGET_BOOL("Toggle attached", debugData.bToggleAttach)
			
			ADD_WIDGET_VECTOR_SLIDER("Mechanic pos", debugData.vMechanicPos, -9999.9, 9999.9, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("Mechanic heading", debugData.fMechanicHeading, 0.0, 360.0, 0.1)
			
			REPEAT COUNT_OF(debugData.vVehActorsPos) i
				strSlider = "VEH ACTORS POS [" strSlider += i strSlider += "]"
				ADD_WIDGET_VECTOR_SLIDER(strSlider, debugData.vVehActorsPos[i], -9999.9, 9999.9, 0.01)
				strSlider = "VEH ACTORS HEADING [" strSlider += i strSlider += "]"
				ADD_WIDGET_FLOAT_SLIDER(strSlider, debugData.fVehActorsHeading[i], 0.0, 360.0, 0.1)
				
				IF i = 0
				AND scriptData.dropoffType = IE_DELIVERY_DROPOFF_CUTSCENE_TYPE_RAMP
					ADD_WIDGET_BOOL("Set truck to leave coords", debugData.bSetTruckToLeaveCoords)
				ENDIF
			ENDREPEAT
			
			REPEAT COUNT_OF(debugData.vObjPropsPos) i
				strSlider = "PROPS POS [" strSlider += i strSlider += "]"
				ADD_WIDGET_VECTOR_SLIDER(strSlider, debugData.vObjPropsPos[i], -9999.9, 9999.9, 0.01)
				strSlider = "PROPS HEADING [" strSlider += i strSlider += "]"
				ADD_WIDGET_FLOAT_SLIDER(strSlider, debugData.fObjProposHeading[i], 0.0, 360.0, 0.1)
			ENDREPEAT
			
			REPEAT COUNT_OF(debugData.vPedActorsPos) i
				strSlider = "PED ACTORS POS [" strSlider += i strSlider += "]"
				ADD_WIDGET_VECTOR_SLIDER(strSlider, debugData.vPedActorsPos[i], -9999.9, 9999.9, 0.01)
				strSlider = "PED ACTORS HEADING [" strSlider += i strSlider += "]"
				ADD_WIDGET_FLOAT_SLIDER(strSlider, debugData.fPedActorsHeading[i], 0.0, 360.0, 0.1)
			ENDREPEAT
			
			ADD_WIDGET_BOOL("Start synced scene", debugData.bCreateSyncedScene)
			ADD_WIDGET_VECTOR_SLIDER("Synced scene pos", cutsceneData.vSyncedScenePos, -9999.9, 9999.9, 0.01)
			ADD_WIDGET_VECTOR_SLIDER("Synced scene rot", cutsceneData.vSyncedSceneRot, -9999.9, 9999.9, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("Phase", debugData.fSyncedScenePhaseOLD, 0.0, 1.0, 0.01)
		STOP_WIDGET_GROUP()
		
		SIMPLE_CUTSCENE_CREATE_DEBUG_WIDGETS(scriptData.cutscene)
		
	STOP_WIDGET_GROUP()
	
ENDPROC

PROC UPDATE_DEBUG_WIDGETS()
	INT i
	
	IF debugData.bKillScript
		PRINTLN("[IE_DELIVERY][CUTSCENE] UPDATE_DEBUG_WIDGETS - bKillScript set to TRUE, terminating.")
		SCRIPT_CLEANUP()
	ENDIF
	
	// Dumping custom spawn points
	IF debugData.bDumpSpawnPoints
		VECTOR vSpawnPoint
		FLOAT fSpawnHeading
	
		OPEN_DEBUG_FILE()
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("		// -- -- OUTPUT CUSTOM SPAWN POINTS AFTER DELIVERY CUTSCENE  -- -- -- //") SAVE_NEWLINE_TO_DEBUG_FILE()
		REPEAT g_SpawnData.CustomSpawnPointInfo.iNumberOfCustomSpawnPoints i
		
			vSpawnPoint = g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[i].vPos
			fSpawnHeading = g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[i].fHeading
			
			SAVE_STRING_TO_DEBUG_FILE("CASE ") SAVE_INT_TO_DEBUG_FILE(i) SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("    vCoords = ") SAVE_VECTOR_TO_DEBUG_FILE(vSpawnPoint) SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("    fHeading = ") SAVE_FLOAT_TO_DEBUG_FILE(fSpawnHeading) SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("    RETURN TRUE") SAVE_NEWLINE_TO_DEBUG_FILE()
			//SAVE_STRING_TO_DEBUG_FILE("BREAK") SAVE_NEWLINE_TO_DEBUG_FILE()
		ENDREPEAT
		SAVE_NEWLINE_TO_DEBUG_FILE()	
		CLOSE_DEBUG_FILE()
	
		debugData.bDumpSpawnPoints = FALSE
	ENDIF
	
	IF debugData.bToggleSpawnPointsVisibility
		IF NOT (g_SpawnData.bUseCustomSpawnPoints)
			USE_CUSTOM_SPAWN_POINTS(TRUE, FALSE, TRUE, 0.01, 2, DEFAULT, DEFAULT, 0.01)
			
			INT iIndex
			VECTOR vSpawnPoint
			FLOAT fSpawnHeading
			
			IF IE_IS_DROPOFF_A_PLAYER_OWNED_WAREHOUSE(scriptData.eDropoffID)
				// Use simple interior spawn points
				IMPORT_EXPORT_GARAGES eIEWarehouseID = GET_IMPORT_EXPORT_WAREHOUSE_FROM_IE_DROPOFF(scriptData.eDropoffID)
				SIMPLE_INTERIORS eSimpleInteriorID = GET_SIMPLE_INTERIOR_ID_FROM_IMPORT_EXPORT_GARAGE_ID(eIEWarehouseID)
						
				WHILE GET_SIMPLE_INTERIOR_OUTSIDE_SPAWN_POINT(eSimpleInteriorID, iIndex, vSpawnPoint, fSpawnHeading, FALSE)
					iIndex += 1
					ADD_CUSTOM_SPAWN_POINT(vSpawnPoint, fSpawnHeading, 0.0 #IF IS_DEBUG_BUILD,0.2 #ENDIF)  
				ENDWHILE
			ELSE
				WHILE IE_DELIVERY_GET_AFTER_DESTRUCTION_SPAWN_POINT(scriptData.eDropoffID, iIndex, vSpawnPoint, fSpawnHeading)
					iIndex += 1
					ADD_CUSTOM_SPAWN_POINT(vSpawnPoint, fSpawnHeading, 0.0 #IF IS_DEBUG_BUILD,0.2 #ENDIF)   
				ENDWHILE
			ENDIF
			
			g_SpawnData.bShowCustomSpawnPoints = TRUE
		ELSE
			g_SpawnData.bShowCustomSpawnPoints = FALSE
			CLEAR_CUSTOM_SPAWN_POINTS()
			USE_CUSTOM_SPAWN_POINTS(FALSE)
		ENDIF
		
		debugData.bToggleSpawnPointsVisibility = FALSE
	ENDIF
	
	IF debugData.bEditMode
		IF NOT debugData.bEditModeInitialised
			
			IE_DELIVERY_CUTSCENE_CREATE_ALL_ASSETS()
			SET_ALL_IE_DELIVERY_ASSETS_VISIBLE(TRUE)
			_SET_INITIAL_STATE_FOR_ALL_ASSETS()
			
			debugData.vBasePos = cutsceneData.vBasePos
			debugData.fBaseHeading = cutsceneData.fBaseHeading
			
			debugData.vVehClonePos = TRANSFORM_WORLD_COORDS_TO_CUTSCENE_COORDS(GET_ENTITY_COORDS(cutsceneData.vehClone))
			debugData.fVehCloneHeading = TRANSFORM_WORLD_HEADING_TO_CUTSCENE_HEADING(GET_ENTITY_HEADING(cutsceneData.vehClone))
			debugData.vVehClonePosOLD = debugData.vVehClonePos
			debugData.fVehCloneHeadingOLD = debugData.fVehCloneHeading
			
			IF DOES_ENTITY_EXIST(cutsceneData.vehProxyClone)
				debugData.vProxyPos = TRANSFORM_WORLD_COORDS_TO_CUTSCENE_COORDS(GET_ENTITY_COORDS(cutsceneData.vehProxyClone))
				debugData.fProxyHeading = TRANSFORM_WORLD_HEADING_TO_CUTSCENE_HEADING(GET_ENTITY_HEADING(cutsceneData.vehProxyClone))
				debugData.vProxyPosOLD = debugData.vProxyPos
				debugData.fProxyHeadingOLD = debugData.fProxyHeading
				//debugData.bFreezeProxyClone = TRUE
			ENDIF
			
			IF DOES_ENTITY_EXIST(cutsceneData.pedProxyMechanic)
				debugData.vMechanicPos = TRANSFORM_WORLD_COORDS_TO_CUTSCENE_COORDS(GET_ENTITY_COORDS(cutsceneData.pedProxyMechanic))
				debugData.fMechanicHeading = TRANSFORM_WORLD_HEADING_TO_CUTSCENE_HEADING(GET_ENTITY_HEADING(cutsceneData.pedProxyMechanic))
				debugData.vMechanicPosOLD = debugData.vProxyPos
				debugData.fMechanicHeadingOLD = debugData.fProxyHeading
			ENDIF
			
			REPEAT COUNT_OF(cutsceneData.vehActors) i
				IF DOES_ENTITY_EXIST(cutsceneData.vehActors[i])
					debugData.vVehActorsPos[i] = TRANSFORM_WORLD_COORDS_TO_CUTSCENE_COORDS(GET_ENTITY_COORDS(cutsceneData.vehActors[i]))
					debugData.fVehActorsHeading[i] = TRANSFORM_WORLD_HEADING_TO_CUTSCENE_HEADING(GET_ENTITY_HEADING(cutsceneData.vehActors[i]))
					debugData.vVehActorsPosOLD[i] = debugData.vVehActorsPos[i]
					debugData.fVehActorsHeadingOLD[i] = debugData.fVehActorsHeading[i]
				ENDIF
			ENDREPEAT
			REPEAT COUNT_OF(cutsceneData.objProps) i
				IF DOES_ENTITY_EXIST(cutsceneData.objProps[i])
					debugData.vObjPropsPos[i] = TRANSFORM_WORLD_COORDS_TO_CUTSCENE_COORDS(GET_ENTITY_COORDS(cutsceneData.objProps[i]))
					debugData.fObjProposHeading[i] = TRANSFORM_WORLD_HEADING_TO_CUTSCENE_HEADING(GET_ENTITY_HEADING(cutsceneData.objProps[i]))
					debugData.vObjPropsPosOLD[i] = debugData.vObjPropsPos[i]
					debugData.fObjProposHeadingOLD[i] = debugData.fObjProposHeading[i]
				ENDIF
			ENDREPEAT
			REPEAT COUNT_OF(cutsceneData.pedActors) i
				IF DOES_ENTITY_EXIST(cutsceneData.pedActors[i])
					debugData.vPedActorsPos[i] = TRANSFORM_WORLD_COORDS_TO_CUTSCENE_COORDS(GET_ENTITY_COORDS(cutsceneData.pedActors[i]))
					debugData.fPedActorsHeading[i] = TRANSFORM_WORLD_HEADING_TO_CUTSCENE_HEADING(GET_ENTITY_HEADING(cutsceneData.pedActors[i]))
					debugData.vPedActorsPosOLD[i] = debugData.vPedActorsPos[i]
					debugData.fPedActorsHeadingOLD[i] = debugData.fPedActorsHeading[i]
				ENDIF
			ENDREPEAT
			
			debugData.vSyncedScenePosOLD = cutsceneData.vSyncedScenePos
			debugData.vSyncedSceneRotOLD = cutsceneData.vSyncedSceneRot
			
			debugData.bEditModeInitialised = TRUE
		ENDIF
		
		// Updating whole cutscene pos
		BOOL bUpdateAllPos
		
		IF NOT ARE_VECTORS_ALMOST_EQUAL(cutsceneData.vBasePos, debugData.vBasePos, 0.001)
			cutsceneData.vBasePos = debugData.vBasePos
			bUpdateAllPos = TRUE
		ENDIF
		IF ABSF(cutsceneData.fBaseHeading - debugData.fBaseHeading) > 0.001
			cutsceneData.fBaseHeading = debugData.fBaseHeading
			bUpdateAllPos = TRUE
		ENDIF
		
		// Veh clone
		IF DOES_ENTITY_EXIST(cutsceneData.vehClone)
			IF NOT ARE_VECTORS_ALMOST_EQUAL(debugData.vVehClonePosOLD, debugData.vVehClonePos, 0.001)
			OR bUpdateAllPos
				SET_ENTITY_COORDS_NO_OFFSET(cutsceneData.vehClone, TRANSFORM_CUTSCENE_COORDS_TO_WORLD_COORDS(debugData.vVehClonePos))
				debugData.vVehClonePosOLD = debugData.vVehClonePos
			ENDIF
			IF ABSF(debugData.fVehCloneHeadingOLD - debugData.fVehCloneHeading) > 0.001
			OR bUpdateAllPos
				SET_ENTITY_HEADING(cutsceneData.vehClone, TRANSFORM_CUTSCENE_HEADING_TO_WORLD_HEADING(debugData.fVehCloneHeading))
				debugData.fVehCloneHeadingOLD = debugData.fVehCloneHeading
			ENDIF
		ENDIF
		
		// Proxy veh clone (cargobob/towtruck)
		IF DOES_ENTITY_EXIST(cutsceneData.vehProxyClone)
			IF NOT ARE_VECTORS_ALMOST_EQUAL(debugData.vProxyPosOLD, debugData.vProxyPos, 0.001)
			OR bUpdateAllPos
				SET_ENTITY_COORDS_NO_OFFSET(cutsceneData.vehProxyClone, TRANSFORM_CUTSCENE_COORDS_TO_WORLD_COORDS(debugData.vProxyPos))
				debugData.vProxyPosOLD = debugData.vProxyPos
			ENDIF
			IF ABSF(debugData.fProxyHeadingOLD - debugData.fProxyHeading) > 0.001
			OR bUpdateAllPos
				SET_ENTITY_HEADING(cutsceneData.vehProxyClone, TRANSFORM_CUTSCENE_HEADING_TO_WORLD_HEADING(debugData.fProxyHeading))
				debugData.fProxyHeadingOLD = debugData.fProxyHeading
			ENDIF
		ENDIF
		
		// Proxy mechanic (only used in cargobob cutscene)
		IF DOES_ENTITY_EXIST(cutsceneData.pedProxyMechanic)
			IF NOT ARE_VECTORS_ALMOST_EQUAL(debugData.vMechanicPos, debugData.vMechanicPosOLD, 0.001)
			OR bUpdateAllPos
				SET_ENTITY_COORDS_NO_OFFSET(cutsceneData.pedProxyMechanic, TRANSFORM_CUTSCENE_COORDS_TO_WORLD_COORDS(debugData.vMechanicPos))
				debugData.vMechanicPosOLD = debugData.vMechanicPos
			ENDIF
			IF ABSF(debugData.fMechanicHeadingOLD - debugData.fMechanicHeading) > 0.001
			OR bUpdateAllPos
				SET_ENTITY_HEADING(cutsceneData.pedProxyMechanic, TRANSFORM_CUTSCENE_HEADING_TO_WORLD_HEADING(debugData.fMechanicHeading))
				debugData.fMechanicHeadingOLD = debugData.fMechanicHeading
			ENDIF
		ENDIF
		
		// All other entities
		REPEAT COUNT_OF(cutsceneData.vehActors) i
			IF DOES_ENTITY_EXIST(cutsceneData.vehActors[i])
				IF NOT ARE_VECTORS_ALMOST_EQUAL(debugData.vVehActorsPos[i], debugData.vVehActorsPosOLD[i], 0.001)
				OR bUpdateAllPos
					SET_ENTITY_COORDS_NO_OFFSET(cutsceneData.vehActors[i], TRANSFORM_CUTSCENE_COORDS_TO_WORLD_COORDS(debugData.vVehActorsPos[i]))
					debugData.vVehActorsPosOLD[i] = debugData.vVehActorsPos[i]
				ENDIF
				IF ABSF(debugData.fVehActorsHeadingOLD[i] - debugData.fVehActorsHeading[i]) > 0.001
				OR bUpdateAllPos
					SET_ENTITY_HEADING(cutsceneData.vehActors[i], TRANSFORM_CUTSCENE_HEADING_TO_WORLD_HEADING(debugData.fVehActorsHeading[i]))
					debugData.fVehActorsHeadingOLD[i] = debugData.fVehActorsHeading[i]
				ENDIF
			ENDIF
		ENDREPEAT
		REPEAT COUNT_OF(cutsceneData.objProps) i
			IF DOES_ENTITY_EXIST(cutsceneData.objProps[i])
				IF NOT ARE_VECTORS_ALMOST_EQUAL(debugData.vObjPropsPos[i], debugData.vObjPropsPosOLD[i], 0.001)
				OR bUpdateAllPos
					SET_ENTITY_COORDS_NO_OFFSET(cutsceneData.objProps[i], TRANSFORM_CUTSCENE_COORDS_TO_WORLD_COORDS(debugData.vObjPropsPos[i]))
					debugData.vObjPropsPosOLD[i] = debugData.vObjPropsPos[i]
				ENDIF
				IF ABSF(debugData.fObjProposHeadingOLD[i] - debugData.fObjProposHeading[i]) > 0.001
				OR bUpdateAllPos
					SET_ENTITY_HEADING(cutsceneData.objProps[i], TRANSFORM_CUTSCENE_HEADING_TO_WORLD_HEADING(debugData.fObjProposHeading[i]))
					debugData.fObjProposHeadingOLD[i] = debugData.fObjProposHeading[i]
				ENDIF
			ENDIF
		ENDREPEAT
		REPEAT COUNT_OF(cutsceneData.pedActors) i
			IF DOES_ENTITY_EXIST(cutsceneData.pedActors[i])
				IF NOT ARE_VECTORS_ALMOST_EQUAL(debugData.vPedActorsPos[i], debugData.vPedActorsPosOLD[i], 0.001)
				OR bUpdateAllPos
					SET_ENTITY_COORDS_NO_OFFSET(cutsceneData.pedActors[i], TRANSFORM_CUTSCENE_COORDS_TO_WORLD_COORDS(debugData.vPedActorsPos[i]))
					debugData.vPedActorsPosOLD[i] = debugData.vPedActorsPos[i]
				ENDIF
				IF ABSF(debugData.fPedActorsHeadingOLD[i] - debugData.fPedActorsHeading[i]) > 0.001
				OR bUpdateAllPos
					SET_ENTITY_HEADING(cutsceneData.pedActors[i], TRANSFORM_CUTSCENE_HEADING_TO_WORLD_HEADING(debugData.fPedActorsHeading[i]))
					debugData.fPedActorsHeadingOLD[i] = debugData.fPedActorsHeading[i]
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF debugData.bToggleAttach
			IF scriptData.bViaCargobob
				IE_CUTSCENE_ATTACH_VEH_CLONE_TO_CARGOBOB(NOT cutsceneData.bVehCloneAttachedToProxy)
			ELIF scriptData.bViaTowtruck
				IE_CUTSCENE_ATTACH_VEH_CLONE_TO_TOWTRUCK(NOT cutsceneData.bVehCloneAttachedToProxy)
			ENDIF
			debugData.bToggleAttach = FALSE
		ENDIF
		
		bUpdateAllPos = FALSE
	ELSE
		IF debugData.bEditModeInitialised
			IE_DELIVERY_CLEANUP_EVERYTHING()
			debugData.bEditModeInitialised = FALSE
		ENDIF
	ENDIF
	
	IF debugData.bFreezeVehClone
		IF NOT debugData.bVehCloneIsFrozen
			IF DOES_ENTITY_EXIST(cutsceneData.vehClone)
				FREEZE_ENTITY_POSITION(cutsceneData.vehClone, TRUE)
				SET_ENTITY_COLLISION(cutsceneData.vehClone, FALSE)
				debugData.bVehCloneIsFrozen = TRUE
			ENDIF
		ENDIF
	ELSE
		IF debugData.bVehCloneIsFrozen
			IF DOES_ENTITY_EXIST(cutsceneData.vehClone)
				FREEZE_ENTITY_POSITION(cutsceneData.vehClone, FALSE)
				SET_ENTITY_COLLISION(cutsceneData.vehClone, TRUE)
				debugData.bVehCloneIsFrozen = FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF debugData.bFreezeProxyClone
		IF NOT debugData.bProxyCloneIsFrozen
			IF DOES_ENTITY_EXIST(cutsceneData.vehProxyClone)
				FREEZE_ENTITY_POSITION(cutsceneData.vehProxyClone, TRUE)
				SET_ENTITY_COLLISION(cutsceneData.vehProxyClone, FALSE)
				SET_ENTITY_ROTATION(cutsceneData.vehProxyClone, <<0.0, 0.0, GET_ENTITY_HEADING(cutsceneData.vehProxyClone)>>)
				debugData.bProxyCloneIsFrozen = TRUE
				PRINTLN("[IE_DELIVERY][CUTSCENE] Proxy is frozen")
			ENDIF
		ENDIF
	ELSE
		IF debugData.bProxyCloneIsFrozen
			IF DOES_ENTITY_EXIST(cutsceneData.vehProxyClone)
				FREEZE_ENTITY_POSITION(cutsceneData.vehProxyClone, FALSE)
				SET_ENTITY_COLLISION(cutsceneData.vehProxyClone, TRUE)
				debugData.bProxyCloneIsFrozen = FALSE
				PRINTLN("[IE_DELIVERY][CUTSCENE] Proxy is unfrozen")
			ENDIF
		ENDIF
	ENDIF
	
	IF debugData.bCreateSyncedScene
		_CREATE_AND_START_SYNCED_SCENE(0.0, 0.0, TRUE)
		SET_SYNCHRONIZED_SCENE_LOOPED(cutsceneData.iSyncedSceneID, TRUE)
		debugData.bCreateSyncedScene = FALSE
	ENDIF
	
	IF NOT scriptData.cutscene.bPlaying
		IF IS_SYNCHRONIZED_SCENE_RUNNING(cutsceneData.iSyncedSceneID)
			IF NOT ARE_VECTORS_ALMOST_EQUAL(debugData.vSyncedScenePosOLD, cutsceneData.vSyncedScenePos, 0.001)
			OR NOT ARE_VECTORS_ALMOST_EQUAL(debugData.vSyncedSceneRotOLD, cutsceneData.vSyncedSceneRot, 0.001)
				SET_SYNCHRONIZED_SCENE_ORIGIN(cutsceneData.iSyncedSceneID, cutsceneData.vSyncedScenePos, cutsceneData.vSyncedSceneRot)
				debugData.vSyncedScenePosOLD = cutsceneData.vSyncedScenePos
				debugData.vSyncedSceneRotOLD = cutsceneData.vSyncedSceneRot
			ENDIF
			
			IF ABSF(GET_SYNCHRONIZED_SCENE_PHASE(cutsceneData.iSyncedSceneID) - debugData.fSyncedScenePhaseOLD) > 0.001
				SET_SYNCHRONIZED_SCENE_PHASE(cutsceneData.iSyncedSceneID, debugData.fSyncedScenePhaseOLD)
			ENDIF
		ENDIF
	ENDIF
	
	IF debugData.bToggleIPL
		IE_DELIVERY_SET_IPL_ON(NOT IS_BIT_SET(scriptData.iBS, BS_IE_DELIVERY_IPLS_ARE_LOADED))
		debugData.bToggleIPL = FALSE
	ENDIF
	
	IF debugData.bSetTruckToLeaveCoords
		IF DOES_ENTITY_EXIST(cutsceneData.vehActors[0])
		AND NOT IS_ENTITY_DEAD(cutsceneData.vehActors[0])
			VECTOR vCoords
			FLOAT fHeading
			_GET_TRUCK_COORDS_FOR_LEAVING(vCoords, fHeading)
			SET_ENTITY_COORDS_NO_OFFSET(cutsceneData.vehActors[0], vCoords)
			SET_ENTITY_HEADING(cutsceneData.vehActors[0], fHeading)
		ENDIF
		debugData.bSetTruckToLeaveCoords = FALSE
	ENDIF
	
	SIMPLE_CUTSCENE_MAINTAIN_DEBUG_WIDGETS(scriptData.cutscene)
ENDPROC
#ENDIF

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ INITIALISATION  ╞════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

FUNC BOOL SHOULD_WE_BE_THE_HOST_OF_THIS_SCRIPT()
	RETURN scriptData.pDeliverer = PLAYER_ID()
ENDFUNC

FUNC BOOL SHOULD_WE_HAVE_CONTROL_OF_DELIVERED_VEHICLE()
	IF scriptData.pDeliverer = PLAYER_ID()
		IF scriptData.bViaCargobob
		OR scriptData.bViaTowtruck
			PED_INDEX pedInDriverSeat = GET_PED_IN_VEHICLE_SEAT(scriptData.vehDelivered, VS_DRIVER)
			IF pedInDriverSeat != NULL
			AND DOES_ENTITY_EXIST(pedInDriverSeat)
			AND NOT IS_ENTITY_DEAD(pedInDriverSeat)
				IF IS_PED_A_PLAYER(pedInDriverSeat)
					// Their driver will keep grabbing control from us, nothing we can do until the veh is empty
					#IF IS_DEBUG_BUILD
						PRINTLN("[IE_DELIVERY][CUTSCENE] SHOULD_WE_HAVE_CONTROL_OF_DELIVERED_VEHICLE - No, ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX_FROM_PED(pedInDriverSeat)), " has got control.")
					#ENDIF
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
			PRINTLN("[IE_DELIVERY][CUTSCENE] SHOULD_WE_HAVE_CONTROL_OF_DELIVERED_VEHICLE - Yes, we are the deliverer.")
		#ENDIF
		RETURN TRUE
	ELSE
		IF scriptData.bViaCargobob
		OR scriptData.bViaTowtruck
			IF GET_PED_IN_VEHICLE_SEAT(scriptData.vehDelivered, VS_DRIVER) = PLAYER_PED_ID()
				#IF IS_DEBUG_BUILD
					PRINTLN("[IE_DELIVERY][CUTSCENE] SHOULD_WE_HAVE_CONTROL_OF_DELIVERED_VEHICLE - Yes, delivering via cargobob or towtruck but we're in the frontseat of veh to deliver.")
				#ENDIF
				RETURN TRUE
			ELSE	
				#IF IS_DEBUG_BUILD
					PRINTLN("[IE_DELIVERY][CUTSCENE] SHOULD_WE_HAVE_CONTROL_OF_DELIVERED_VEHICLE - No, not in driver seat")
				#ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
				PRINTLN("[IE_DELIVERY][CUTSCENE] SHOULD_WE_HAVE_CONTROL_OF_DELIVERED_VEHICLE - No, no cargobob nor towtruck")
			#ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC INITIALISE(IE_DELIVERY_SCRIPT_LAUNCH_DATA &launchData)
	scriptData.eDropoffID = launchData.eDropoffID
	scriptData.vDropoffCoords = IE_DELIVERY_GET_DROPOFF_COORDS(scriptData.eDropoffID)
	scriptData.dropoffType = IE_DELIVERY_GET_DROPOFF_CUTSCENE_TYPE_FROM_DROPOFF_ID(launchData.eDropoffID)
	scriptData.sDeliveryAudioStream = IE_DELIVERY_GET_AUDIO_STREAM(scriptData.eDropoffID)
	
	IF scriptData.dropoffType = IE_DELIVERY_DROPOFF_CUTSCENE_TYPE_INVALID
		#IF IS_DEBUG_BUILD
			PRINTLN("[IE_DELIVERY][CUTSCENE] INITIALISE - Drop-off cutscene type is INVALID, terminating!")
		#ENDIF
		SCRIPT_CLEANUP()
	ENDIF
	
	cutsceneData.vBasePos = IE_DELIVERY_GET_DROPOFF_BASE_COORDS(scriptData.eDropoffID)
	cutsceneData.fBaseHeading = IE_DELIVERY_GET_DROPOFF_BASE_HEADING(scriptData.eDropoffID)
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[IE_DELIVERY][CUTSCENE] INITIALISE - Starting for dropoff ", IE_DELIVERY_GET_DROPOFF_NAME_FOR_DEBUG(scriptData.eDropoffID))
	#ENDIF
	
	scriptData.bGotGarageBounds = _GET_PRECACHED_GARAGE_BOUNDS(scriptData.vGaragePos, scriptData.vGarageBBox[0], scriptData.vGarageBBox[1], scriptData.vGarageNormal, scriptData.fGarageBBoxWidth)
	#IF IS_DEBUG_BUILD
		IF scriptData.bGotGarageBounds
			PRINTLN("[IE_DELIVERY][CUTSCENE] INITIALISE - Got garage bounds: ", scriptData.vGarageBBox[0], ", ", scriptData.vGarageBBox[1], ", ", scriptData.fGarageBBoxWidth)
		ELSE
			PRINTLN("[IE_DELIVERY][CUTSCENE] INITIALISE - Don't have garage bounds!")
		ENDIF
	#ENDIF
	
	IF scriptData.dropoffType = IE_DELIVERY_DROPOFF_CUTSCENE_TYPE_CARPARK
		// Request the waypoint recording assigned to this drop off.
		scriptData.sWaypointRecording = IE_DELIVERY_GET_WAYPOINT_RECORDING_DATA(scriptData.eDropoffID)
		#IF IS_DEBUG_BUILD
			PRINTLN("[IE_DELIVERY][CUTSCENE] INITIALISE - Requested Waypoint Recording: ", scriptData.sWaypointRecording)
		#ENDIF
	ENDIF
	
	scriptData.pDeliverer = launchData.pDeliverer
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[IE_DELIVERY][CUTSCENE] INITIALISE - Player deliverer is ", GET_PLAYER_NAME(scriptData.pDeliverer))
	#ENDIF
	
	scriptData.bIsUrgent = IE_DROPOFF_HAS_INCREASED_DELIVERY_URGENCY(scriptData.eDropoffID)
	
	BOOL bFoo		
	// This script starts only if we are in the car that can be delivered and terminates if we leave that car
	
	IF scriptData.pDeliverer = PLAYER_ID()
		IF NOT IE_DELIVERY_CAN_LOCAL_PLAYER_USE_THIS_DROPOFF_IN_CURRENT_VEHICLE(scriptData.eDropoffID #IF IS_DEBUG_BUILD , TRUE #ENDIF)
			#IF IS_DEBUG_BUILD
				PRINTLN("[IE_DELIVERY][CUTSCENE] INITIALISE - Player cannot deliver to this dropoff, terminating!")
			#ENDIF
			SCRIPT_CLEANUP()
		ENDIF
	ELIF scriptData.pDeliverer != INVALID_PLAYER_INDEX()
		INT iArray, iBit
		IE_DROPOFF_GET_BITSET_ARRAY_AND_INDEX(scriptData.eDropoffID, iArray, iBit)
		IF NOT IS_BIT_SET(GlobalplayerBD_FM_3[NATIVE_TO_INT(scriptData.pDeliverer)].IEDeliveryBD.iActiveDropoffs[iArray], iBit)
			#IF IS_DEBUG_BUILD
				PRINTLN("[IE_DELIVERY][CUTSCENE] INITIALISE - Player driver cant deliver to this dropoff, terminating!")
			#ENDIF
			SCRIPT_CLEANUP()
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
			PRINTLN("[IE_DELIVERY][CUTSCENE] INITIALISE - Player driver is invalid, terminating!")
		#ENDIF
		SCRIPT_CLEANUP()
	ENDIF
	
	bFoo = FALSE
	
	scriptData.vehDelivered = IE_DELIVERY_GET_VEHICLE_PLAYER_IS_DELIVERING(PLAYER_ID(), bFoo, bFoo)
	scriptData.modelVehDelivered = GET_ENTITY_MODEL(scriptData.vehDelivered)
	IF DOES_ENTITY_EXIST(scriptData.vehDelivered) AND NOT IS_ENTITY_DEAD(scriptData.vehDelivered)
		scriptData.bWeAreOriginalOwner = AM_I_ORIGINAL_SELLER_OF_EXPORT_VEHICLE(scriptData.vehDelivered)
		scriptData.pOriginalOwner = GB_GET_OWNER_OF_CONTRABAND(scriptData.vehDelivered)
	ENDIF
	
	PED_INDEX pedDeliverer = GET_PLAYER_PED(scriptData.pDeliverer)
	IF NOT IS_ENTITY_DEAD(pedDeliverer)
	AND IS_PED_IN_ANY_VEHICLE(pedDeliverer)
		VEHICLE_INDEX vehDelivererIsIn = GET_VEHICLE_PED_IS_IN(pedDeliverer)
		IF NOT IS_ENTITY_DEAD(vehDelivererIsIn)
			MODEL_NAMES modelDelivererIsIn = GET_ENTITY_MODEL(vehDelivererIsIn)
			IF modelDelivererIsIn = CARGOBOB
			OR modelDelivererIsIn = CARGOBOB2
			OR modelDelivererIsIn = CARGOBOB3
			OR modelDelivererIsIn = CARGOBOB4
				scriptData.bViaCargobob = TRUE
			ELIF modelDelivererIsIn = TOWTRUCK
			OR modelDelivererIsIn = TOWTRUCK2
				scriptData.bViaTowtruck = TRUE
			ENDIF
		ENDIF
	
		IF scriptData.bViaCargobob
		OR scriptData.bViaTowtruck
			scriptData.modelProxyVeh = GET_ENTITY_MODEL(vehDelivererIsIn)
			scriptData.vehProxy = vehDelivererIsIn
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF g_IEDeliveryData.d_bForceCargobobVariation
		scriptData.bViaCargobob = TRUE
		scriptData.modelProxyVeh = CARGOBOB
		IF NOT IS_ENTITY_DEAD(pedDeliverer)
		AND IS_PED_IN_ANY_VEHICLE(pedDeliverer)
			scriptData.vehProxy = GET_VEHICLE_PED_IS_IN(pedDeliverer)
		ENDIF
	ELIF g_IEDeliveryData.d_bForceTowtruckVariation
		scriptData.bViaTowtruck = TRUE
		scriptData.modelProxyVeh = TOWTRUCK
	ENDIF
	#ENDIF
	
	SIMPLE_CUTSCENE_CREATE(scriptData.cutscene, "cutscene")
	IF scriptData.bViaCargobob
		IE_DELIVERY_SET_UP_CUTSCENE_DATA_FOR_CARGOBOB(scriptData.eDropoffID, scriptData.cutscene)
	ELIF scriptData.bViaTowtruck
		IE_DELIVERY_SET_UP_CUTSCENE_DATA_FOR_TOWTRUCK(scriptData.eDropoffID, scriptData.cutscene)
	ELSE
		IE_DELIVERY_SET_UP_CUTSCENE_DATA(scriptData.eDropoffID, scriptData.cutscene)
	ENDIF
	
	TERMINATE_IF_PLAYER_DELIVERER_LEFT_OR_NOT_OK()
	
	IF SHOULD_WE_BE_THE_HOST_OF_THIS_SCRIPT()
	AND NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		NETWORK_REQUEST_TO_BE_HOST_OF_THIS_SCRIPT()
		#IF IS_DEBUG_BUILD
			PRINTLN("[IE_DELIVERY][CUTSCENE] INITIALISE - We should be the host of this script but we're not! Requesting.")
		#ENDIF
	ENDIF
	
	SET_BIT(GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].IEDeliveryBD.iBS, BS_IE_DELIVERY_GLOBAL_PLAYER_BD_RUNNING_DELIVERY_SCRIPT)
	GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].IEDeliveryBD.eRunningDeliveryScriptForDropoffID = scriptData.eDropoffID
	
	// This is used to computet the front of the veh clone
	VECTOR vModelMin
	GET_MODEL_DIMENSIONS(scriptData.modelVehDelivered, vModelMin, scriptData.vModelMaxForFrontTest)
	scriptData.vModelMaxForFrontTest.X = 0.0
	scriptData.vModelMaxForFrontTest.Z = 0.0
	
	// Garage open/close sounds
	INT i
	REPEAT IE_DELIVERY_SOUND_LOOPS_COUNT i
		IF IE_DELIVERY_GET_SOUND_LOOP_DETAILS(i, scriptData.eDropoffID, cutsceneData.sLoopDetails[i], scriptData.cutscene, scriptData.bViaCargobob)
			cutsceneData.iSoundLoopsID[i] = GET_SOUND_ID()
			#IF IS_DEBUG_BUILD
				PRINTLN("[IE_DELIVERY][CUTSCENE] INITIALISE - Grabbing sound id for loop ", i, " (", cutsceneData.sLoopDetails[i].txtSound, ")")
				PRINTLN("[IE_DELIVERY][CUTSCENE] INITIALISE - Will play sound loop ", cutsceneData.sLoopDetails[i].txtSound, " from ", cutsceneData.sLoopDetails[i].iFrom, " to ", cutsceneData.sLoopDetails[i].iTo)
			#ENDIF
		ENDIF
	ENDREPEAT
	
	cutsceneData.strAudioScene = IE_DELIVERY_GET_AUDIO_SCENE(scriptData.eDropoffID, cutsceneData.iSceneForAudioScene, scriptData.cutscene, scriptData.bViaCargobob)
	
	REPEAT IE_DELIVERY_SOUNDS_TO_PLAY_COUNT i
		IF IE_DELIVERY_GET_SOUND_TO_PLAY_DETAILS(i, scriptData.eDropoffID, cutsceneData.sSoundsToPlay[i], scriptData.cutscene, scriptData.bViaCargobob)
			#IF IS_DEBUG_BUILD
				PRINTLN("[IE_DELIVERY][CUTSCENE] INITIALISE - Got sound to plat at ", cutsceneData.sSoundsToPlay[i].iTimeAt, " id ", i, " (", cutsceneData.sSoundsToPlay[i].txtSound, ")")
			#ENDIF
		ENDIF
	ENDREPEAT
	
	// Now this is a special kind of innovation, completely seamless cutscene where warp is done during the cutscene... Turn it on only for selected cutscenes
	IF NOT scriptData.bViaCargobob
		IF NOT IS_BIT_SET(g_IEDeliveryData.iBS, BS_IE_DELIVERY_GLOBAL_DATA_DONT_USE_PARALLEL_WARP) // so it can be disabled via BG script
			IF scriptData.eDropoffID = IE_DROPOFF_PRIVATE_BUYER_5
				scriptData.bExecuteParallelWarp = TRUE
				scriptData.iParallelWarpLastSceneCutoffTime = 4500
			ELIF scriptData.eDropoffID = IE_DROPOFF_SHOWROOM_2
				scriptData.bExecuteParallelWarp = TRUE
				scriptData.iParallelWarpLastSceneCutoffTime = 4500
			ELIF scriptData.eDropoffID = IE_DROPOFF_SHOWROOM_3
				scriptData.bExecuteParallelWarp = TRUE
				scriptData.iParallelWarpLastSceneCutoffTime = 4500
			ELIF scriptData.eDropoffID = IE_DROPOFF_GANGSTER_2
				scriptData.bExecuteParallelWarp = TRUE
				scriptData.bDontSimulateInputGaitAfterWarp = TRUE
				scriptData.iParallelWarpLastSceneCutoffTime = 8000
			ELIF scriptData.eDropoffID = IE_DROPOFF_GANGSTER_3
				scriptData.bExecuteParallelWarp = TRUE
				scriptData.bDontSimulateInputGaitAfterWarp = TRUE
				scriptData.iParallelWarpLastSceneCutoffTime = 7500
			ENDIF
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		CREATE_DEBUG_WIDGETS()
		PRINTLN("[IE_DELIVERY][CUTSCENE] INITIALISE - Done.")
	#ENDIF
	
ENDPROC

PROC SCRIPT_INITIALISE(IE_DELIVERY_SCRIPT_LAUNCH_DATA &launchData)
	
	NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE, launchData.iInstance)
	
	// This makes sure the net script is active, waits untill it is.
	HANDLE_NET_SCRIPT_INITIALISATION()

	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
	
	// KGM: Wait for the first network broadcast before moving on
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		PRINTLN("[IE_DELIVERY][CUTSCENE] FAILED TO RECEIVE INITIAL NETWORK BROADCAST - SCRIPT CLEANUP")
		SCRIPT_CLEANUP()
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		// This script will not be paused if another script calls PAUSE_GAME
		PRINTLN("[IE_DELIVERY][CUTSCENE] INITIALISED")
	ELSE
		PRINTLN("[IE_DELIVERY][CUTSCENE] NETWORK_IS_GAME_IN_PROGRESS = FALSE - SCRIPT CLEANUP")
		SCRIPT_CLEANUP()
	ENDIF
	
	INITIALISE(launchData)
	
ENDPROC

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                 MAIN LOGIC PROC                  /////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

PROC _REQUEST_ALL_ASSETS()
	#IF IS_DEBUG_BUILD
		PRINTLN("[IE_DELIVERY][CUTSCENE] _REQUEST_ALL_ASSETS - Called...")
	#ENDIF
	
	REQUEST_MODEL(scriptData.modelVehDelivered)
	REQUEST_MODEL(mp_m_freemode_01)
	REQUEST_MODEL(mp_f_freemode_01)
	
	IF NOT IS_STRING_NULL_OR_EMPTY(scriptData.sWaypointRecording)
		REQUEST_WAYPOINT_RECORDING(scriptData.sWaypointRecording)
		#IF IS_DEBUG_BUILD
			PRINTLN("[IE_DELIVERY][CUTSCENE] _REQUEST_ALL_ASSETS - Requested waypoint recording ", scriptData.sWaypointRecording)
		#ENDIF
	ENDIF
	
	INT i
	MODEL_NAMES eModel
	VECTOR vFoo
	PED_TYPE pedType
	TEXT_LABEL_63 txtAnimDict
	
	WHILE IE_DELIVERY_GET_DROPOFF_VEHICLE_ACTOR_DATA(scriptData.eDropoffID, i, eModel, vFoo, vFoo)
		REQUEST_MODEL(eModel)
		#IF IS_DEBUG_BUILD
			PRINTLN("[IE_DELIVERY][CUTSCENE] _REQUEST_ALL_ASSETS - Requested veh model ", GET_MODEL_NAME_FOR_DEBUG(eModel))
		#ENDIF
		i += 1
	ENDWHILE
	
	i = 0
	WHILE IE_DELIVERY_GET_DROPOFF_PROP_DATA(scriptData.eDropoffID, i, eModel, vFoo, vFoo, scriptData.bViaCargobob)
		REQUEST_MODEL(eModel)
		#IF IS_DEBUG_BUILD
			PRINTLN("[IE_DELIVERY][CUTSCENE] _REQUEST_ALL_ASSETS - Requested prop model ", GET_MODEL_NAME_FOR_DEBUG(eModel))
		#ENDIF
		i += 1
	ENDWHILE
	
	i = 0
	txtAnimDict = ""
	WHILE IE_DELIVERY_GET_DROPOFF_PED_DATA(scriptData.eDropoffID, i, pedType, eModel, vFoo, vFoo, txtAnimDict, scriptData.bViaCargobob)
		REQUEST_MODEL(eModel)
		#IF IS_DEBUG_BUILD
			PRINTLN("[IE_DELIVERY][CUTSCENE] _REQUEST_ALL_ASSETS - Requested ped model ", GET_MODEL_NAME_FOR_DEBUG(eModel))
		#ENDIF
		
		IF NOT IS_STRING_NULL_OR_EMPTY(txtAnimDict)
			REQUEST_ANIM_DICT(txtAnimDict)
			#IF IS_DEBUG_BUILD
				PRINTLN("[IE_DELIVERY][CUTSCENE] _REQUEST_ALL_ASSETS - Requested anim dict for ped ", txtAnimDict)
			#ENDIF
		ENDIF
		i += 1
		txtAnimDict = ""
	ENDWHILE
	
	IF scriptData.modelProxyVeh != DUMMY_MODEL_FOR_SCRIPT
		REQUEST_MODEL(scriptData.modelProxyVeh)
		#IF IS_DEBUG_BUILD
			PRINTLN("[IE_DELIVERY][CUTSCENE] _REQUEST_ALL_ASSETS - Requested proxy model veh ", GET_MODEL_NAME_FOR_DEBUG(scriptData.modelProxyVeh))
		#ENDIF
	ENDIF
	
	IF scriptData.bViaCargobob
		REQUEST_MODEL(GET_IE_WAREHOUSE_MECHANIC_MODEL())
		#IF IS_DEBUG_BUILD
			PRINTLN("[IE_DELIVERY][CUTSCENE] _REQUEST_ALL_ASSETS - Requested mechanic model for cargobob variation")
		#ENDIF
	ENDIF
	
	// Unique stuff
	
	IF scriptData.dropoffType = IE_DELIVERY_DROPOFF_CUTSCENE_TYPE_CRANE
		REQUEST_ANIM_DICT("map_objects")
		#IF IS_DEBUG_BUILD
			PRINTLN("[IE_DELIVERY][CUTSCENE] _REQUEST_ALL_ASSETS - Requested stuff for crane cutscene.")
		#ENDIF
	ENDIF
	
	IF scriptData.dropoffType = IE_DELIVERY_DROPOFF_CUTSCENE_TYPE_CRANE
		_IE_DELIVERY_LOAD_AUDIO_STREAM(scriptData.sDeliveryAudioStream)	
		#IF IS_DEBUG_BUILD
			PRINTLN("[IE_DELIVERY][CUTSCENE] _REQUEST_ALL_ASSETS - Requested audio stream for crane cutscene.")
		#ENDIF
	ENDIF
	
	IF scriptData.dropoffType = IE_DELIVERY_DROPOFF_CUTSCENE_TYPE_GOON_PICKUP
		IF scriptData.eSpecialInteraction = SPECIAL_INTERACTION_SHAKE_HEAD
			INT iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 scriptData.eSpecialInteraction = SPECIAL_AFTER_HEIST_IDLE_A BREAK
				CASE 1 scriptData.eSpecialInteraction = SPECIAL_AFTER_HEIST_IDLE_B BREAK
				CASE 2 scriptData.eSpecialInteraction = SPECIAL_AFTER_HEIST_FAIL_A BREAK
			ENDSWITCH
			//scriptData.eSpecialInteraction = SPECIAL_AFTER_HEIST_IDLE_C // Idle C is phone anim
			//SET_INTERACTION_ANIM_TO_LOAD(ENUM_TO_INT(INTERACTION_ANIM_TYPE_SPECIAL), ENUM_TO_INT(scriptData.eSpecialInteraction))
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL _HAVE_ALL_ASSETS_LOADED()
	BOOL bAllLoaded = TRUE
	
	IF NOT HAS_MODEL_LOADED(scriptData.modelVehDelivered)
	OR NOT HAS_MODEL_LOADED(mp_f_freemode_01)
	OR NOT HAS_MODEL_LOADED(mp_m_freemode_01)
		bAllLoaded = FALSE
		#IF IS_DEBUG_BUILD
			PRINTLN("[IE_DELIVERY][CUTSCENE] _HAVE_ALL_ASSETS_LOADED - Waiting for player veh and player model.")
		#ENDIF
	ENDIF
	
	IF bAllLoaded
		IF NOT IS_STRING_NULL_OR_EMPTY(scriptData.sWaypointRecording)
		AND NOT GET_IS_WAYPOINT_RECORDING_LOADED(scriptData.sWaypointRecording)
			bAllLoaded = FALSE
			#IF IS_DEBUG_BUILD
				PRINTLN("[IE_DELIVERY][CUTSCENE] _HAVE_ALL_ASSETS_LOADED - Waiting for waypoint recording ", scriptData.sWaypointRecording)
			#ENDIF
		ENDIF
	ENDIF
	
	INT i
	MODEL_NAMES eModel
	VECTOR vFoo
	PED_TYPE pedType
	TEXT_LABEL_63 txtAnimDict
	
	IF bAllLoaded
		WHILE IE_DELIVERY_GET_DROPOFF_VEHICLE_ACTOR_DATA(scriptData.eDropoffID, i, eModel, vFoo, vFoo)
			IF NOT HAS_MODEL_LOADED(eModel)
				bAllLoaded = FALSE
				#IF IS_DEBUG_BUILD
					PRINTLN("[IE_DELIVERY][CUTSCENE] _HAVE_ALL_ASSETS_LOADED - Veh model hasnt loaded ", GET_MODEL_NAME_FOR_DEBUG(eModel))
				#ENDIF
				BREAKLOOP
			ENDIF
			i += 1
		ENDWHILE
	ENDIF
	
	IF bAllLoaded
		i = 0
		WHILE IE_DELIVERY_GET_DROPOFF_PROP_DATA(scriptData.eDropoffID, i, eModel, vFoo, vFoo, scriptData.bViaCargobob)
			IF NOT HAS_MODEL_LOADED(eModel)
				bAllLoaded = FALSE
				#IF IS_DEBUG_BUILD
					PRINTLN("[IE_DELIVERY][CUTSCENE] _HAVE_ALL_ASSETS_LOADED - Prop model hasnt loaded ", GET_MODEL_NAME_FOR_DEBUG(eModel))
				#ENDIF
				BREAKLOOP
			ENDIF
			i += 1
		ENDWHILE
	ENDIF
	
	IF bAllLoaded
		i = 0
		txtAnimDict = ""
		WHILE IE_DELIVERY_GET_DROPOFF_PED_DATA(scriptData.eDropoffID, i, pedType, eModel, vFoo, vFoo, txtAnimDict, scriptData.bViaCargobob)
			IF NOT HAS_MODEL_LOADED(eModel)
				bAllLoaded = FALSE
				#IF IS_DEBUG_BUILD
					PRINTLN("[IE_DELIVERY][CUTSCENE] _HAVE_ALL_ASSETS_LOADED - Ped model hasnt loaded ", GET_MODEL_NAME_FOR_DEBUG(eModel))
				#ENDIF
				BREAKLOOP
			ENDIF
			
			IF NOT IS_STRING_NULL_OR_EMPTY(txtAnimDict)
				IF NOT HAS_ANIM_DICT_LOADED(txtAnimDict)
					bAllLoaded = FALSE
					#IF IS_DEBUG_BUILD
						PRINTLN("[IE_DELIVERY][CUTSCENE] _HAVE_ALL_ASSETS_LOADED - Anim dict hasnt loaded ", txtAnimDict)
					#ENDIF
					BREAKLOOP
				ENDIF
			ENDIF
			
			i += 1
			txtAnimDict = ""
		ENDWHILE
	ENDIF
	
	IF bAllLoaded
		IF scriptData.modelProxyVeh != DUMMY_MODEL_FOR_SCRIPT
			IF NOT HAS_MODEL_LOADED(scriptData.modelProxyVeh)
				bAllLoaded = FALSE
				#IF IS_DEBUG_BUILD
					PRINTLN("[IE_DELIVERY][CUTSCENE] _HAVE_ALL_ASSETS_LOADED - Proxy model hasnt loaded ", GET_MODEL_NAME_FOR_DEBUG(scriptData.modelProxyVeh))
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bAllLoaded
		IF scriptData.bViaCargobob
			IF NOT HAS_MODEL_LOADED(GET_IE_WAREHOUSE_MECHANIC_MODEL())
				bAllLoaded = FALSE
				#IF IS_DEBUG_BUILD
					PRINTLN("[IE_DELIVERY][CUTSCENE] _HAVE_ALL_ASSETS_LOADED - Mechanic model for cargobob variation hasnt loaded")
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	// Unique stuff
	
	IF bAllLoaded
		IF scriptData.dropoffType = IE_DELIVERY_DROPOFF_CUTSCENE_TYPE_CRANE
			IF NOT HAS_ANIM_DICT_LOADED("map_objects")
				bAllLoaded = FALSE
				#IF IS_DEBUG_BUILD
					PRINTLN("[IE_DELIVERY][CUTSCENE] _HAVE_ALL_ASSETS_LOADED - Crane stuff hasnt loaded")
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bAllLoaded
		IF scriptData.dropoffType = IE_DELIVERY_DROPOFF_CUTSCENE_TYPE_CRANE
			IF NOT _IE_DELIVERY_LOAD_AUDIO_STREAM(scriptData.sDeliveryAudioStream)
				bAllLoaded = FALSE
				#IF IS_DEBUG_BUILD
					PRINTLN("[IE_DELIVERY][CUTSCENE] _HAVE_ALL_ASSETS_LOADED - Crane audio stream hasnt loaded")
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN bAllLoaded
ENDFUNC

/// PURPOSE:
///    Save which seat everyone is using because they might be tasked with leaving the vehicle and sometimes we'd like to know where they were sitting
PROC _MONITOR_SEATS_FOR_GOON_PICKUP_CUTSCENE()
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
	AND NOT IS_ENTITY_DEAD(scriptData.vehDelivered)
	AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), scriptData.vehDelivered)
		VEHICLE_SEAT vsTmp = GET_SEAT_PED_IS_IN_WITH_SAFE_DEFAULT(PLAYER_PED_ID())
		IF vsTmp != VS_ANY_PASSENGER
		AND vsTmp != playerBD[PARTICIPANT_ID_TO_INT()].vehicleSeat
			playerBD[PARTICIPANT_ID_TO_INT()].vehicleSeat = vsTmp
			#IF IS_DEBUG_BUILD
				PRINTLN("[IE_DELIVERY][CUTSCENE] _MONITOR_SEATS_FOR_GOON_PICKUP_CUTSCENE - Setting our seat to ", _GET_VEHICLE_SEAT_FOR_DEBUG(vsTmp))
			#ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks if anyone running this script set the specificed bit on their BD.
/// PARAMS:
///    vehCargobob - 
/// RETURNS:
///    
FUNC BOOL _DID_ANYONE_DO_THE_THING(INT iTheThing)
	INT i
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
		AND IS_BIT_SET(playerBD[i].iBS, iTheThing)
		
			#IF IS_DEBUG_BUILD
				STRING strTheThingName
				SWITCH iTheThing
					CASE BS_PLAYER_BD_IE_DELIVERY_FADED_IN_PROXY_VEH strTheThingName = "BS_PLAYER_BD_IE_DELIVERY_FADED_IN_PROXY_VEH" BREAK
					CASE BS_PLAYER_BD_IE_DELIVERY_FADED_OUT_VEH_DELIVERED strTheThingName = "BS_PLAYER_BD_IE_DELIVERY_FADED_OUT_VEH_DELIVERED" BREAK
					CASE BS_PLAYER_BD_IE_DELIVERY_FADED_OUT_VEH_PROXY strTheThingName = "BS_PLAYER_BD_IE_DELIVERY_FADED_OUT_VEH_PROXY" BREAK
					CASE BS_PLAYER_BD_IE_DELIVERY_MOVED_VEH_OUT_OF_THE_WAY strTheThingName = "BS_PLAYER_BD_IE_DELIVERY_MOVED_VEH_OUT_OF_THE_WAY" BREAK
				ENDSWITCH
				PRINTLN("[IE_DELIVERY][CUTSCENE] _DID_ANYONE_DO_THE_THING - Participant ", i, " did the thing: ", strTheThingName)
			#ENDIF
			
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

FUNC BOOL _DID_EVERYONE_REGISTERED_THE_THING(INT iTheThing)
	INT i
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
		AND NOT IS_BIT_SET(playerBD[i].iBS, iTheThing)
		
			#IF IS_DEBUG_BUILD
				STRING strTheThingName
				SWITCH iTheThing
					CASE BS_PLAYER_BD_IE_DELIVERY_FADED_IN_PROXY_VEH strTheThingName = "BS_PLAYER_BD_IE_DELIVERY_FADED_IN_PROXY_VEH" BREAK
					CASE BS_PLAYER_BD_IE_DELIVERY_FADED_OUT_VEH_DELIVERED strTheThingName = "BS_PLAYER_BD_IE_DELIVERY_FADED_OUT_VEH_DELIVERED" BREAK
					CASE BS_PLAYER_BD_IE_DELIVERY_FADED_OUT_VEH_PROXY strTheThingName = "BS_PLAYER_BD_IE_DELIVERY_FADED_OUT_VEH_PROXY" BREAK
					CASE BS_PLAYER_BD_IE_DELIVERY_MOVED_VEH_OUT_OF_THE_WAY strTheThingName = "BS_PLAYER_BD_IE_DELIVERY_MOVED_VEH_OUT_OF_THE_WAY" BREAK
				ENDSWITCH
				PRINTLN("[IE_DELIVERY][CUTSCENE] _DID_EVERYONE_REGISTERED_THE_THING - Participant ", i, " didn't register the thing: ", strTheThingName)
			#ENDIF
			
			RETURN FALSE
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

PROC MAINTAIN_MOVE_PLAYER_OUT_OF_THE_WAY()
	#IF IS_DEBUG_BUILD
	IF scriptData.cutscene.dbg.bPlayedViaDebug
		EXIT
	ENDIF
	#ENDIF
	
	//IF ENUM_TO_INT(playerBD[PARTICIPANT_ID_TO_INT()].eState) = ENUM_TO_INT(IE_DELIVERY_SCRIPT_STATE_PLAYING_CUTSCENE)
		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBS, BS_PLAYER_BD_IE_DELIVERY_PLAYER_MOVED_OUT)
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				//IF DOES_ENTITY_EXIST(scriptData.vehDelivered) AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), scriptData.vehDelivered)
				IF NOT (DOES_ENTITY_EXIST(scriptData.vehProxy) AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()) = scriptData.vehProxy) // donmt kick them out of cargobob
					IF NET_WARP_TO_COORD(scriptData.vDropoffCoords - <<0.0, 0.0, 22.0>>, 0.0, FALSE, FALSE, FALSE, FALSE, FALSE, TRUE, FALSE, FALSE)
						SET_LOCAL_PLAYER_VISIBLE_IN_CUTSCENE(FALSE, FALSE)
						//CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
						//SET_ENTITY_COORDS(PLAYER_PED_ID(), scriptData.vDropoffCoords - <<0.0, 0.0, 22.0>>)
						SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
						SET_BIT(scriptData.iBS, BS_IE_DELIVERY_FROZEN_AFTER_LEAVING_VEH)
						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iBS, BS_PLAYER_BD_IE_DELIVERY_PLAYER_MOVED_OUT)
						
						#IF IS_DEBUG_BUILD
							PRINTLN("[IE_DELIVERY][CUTSCENE] MAINTAIN_MOVE_PLAYER_OUT_OF_THE_WAY - Done.")
						#ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
							PRINTLN("[IE_DELIVERY][CUTSCENE] MAINTAIN_MOVE_PLAYER_OUT_OF_THE_WAY - Trying to warp.")
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	//ENDIF
ENDPROC

PROC MAINTAIN_DELIVERY_SHARD()
	#IF IS_DEBUG_BUILD
	IF scriptData.cutscene.dbg.bPlayedViaDebug
		EXIT
	ENDIF
	#ENDIF
	
	IF NOT scriptData.bWeAreOriginalOwner
		IF IE_IS_DROPOFF_A_PLAYER_OWNED_WAREHOUSE(scriptData.eDropoffID)
		AND GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID())
		AND scriptData.pOriginalOwner != INVALID_PLAYER_INDEX()
		AND GB_IS_PLAYER_MEMBER_OF_A_GANG(scriptData.pDeliverer)
		AND GB_ARE_PLAYERS_MEMBERS_OF_SAME_GANG(PLAYER_ID(), scriptData.pDeliverer)
			IF IS_NET_PLAYER_OK(scriptData.pOriginalOwner, FALSE)
				IF GB_IS_PLAYER_MEMBER_OF_A_GANG(scriptData.pOriginalOwner)
					IF NOT IS_BIT_SET(scriptData.iBS, BS_IE_DELIVERY_SHARD_SHOWN)
						SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_BUY_END_OF_JOB_SUCCESS, "IE_OFFM_SHRD_T", "IE_OFFM_SHRD_S", GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(scriptData.pOriginalOwner), GB_GET_PLAYER_GANG_HUD_COLOUR(scriptData.pOriginalOwner))
						SET_BIT(scriptData.iBS, BS_IE_DELIVERY_SHARD_SHOWN)
					ENDIF
				ENDIF
			ELIF GB_IS_GLOBAL_CLIENT_BIT1_SET(PLAYER_ID(), eGB_GLOBAL_CLIENT_BITSET_1_VEHICLE_EXPORT_PLAYER_IN_VEHICLE_AFTER_SCRIPT_END)
				IF NOT IS_BIT_SET(scriptData.iBS, BS_IE_DELIVERY_SHARD_SHOWN)
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_BUY_END_OF_JOB_SUCCESS, "IE_OFFM_SHRD_T", "IE_OFFM_SHRD_SL")
					SET_BIT(scriptData.iBS, BS_IE_DELIVERY_SHARD_SHOWN)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC STRING _GET_RANDOM_POST_WARP_SCENARIO()
	//INT iRandom = GET_RANDOM_INT_IN_RANGE(0, 2)
	//SWITCH iRandom
	//	CASE 0 RETURN "WORLD_HUMAN_HANG_OUT_STREET"
	//	CASE 1 RETURN "WORLD_HUMAN_STAND_IMPATIENT"
	//ENDSWITCH
	
	RETURN "WORLD_HUMAN_HANG_OUT_STREET"
ENDFUNC

FUNC BOOL DO_PLAYER_WARP_AFTER_CUTSCENE()

	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			RETURN FALSE
		ENDIF
	ENDIF

	IF NOT IS_BIT_SET(scriptData.iBS, BS_IE_DELIVERY_AFTER_DESTRUCTION_WARP_DONE)
		IF NOT IS_BIT_SET(scriptData.iBS, BS_IE_DELIVERY_CREATED_SPAWN_POINTS)
			USE_CUSTOM_SPAWN_POINTS(TRUE, FALSE, FALSE, 0.2, DEFAULT, DEFAULT, DEFAULT, 0.01)
			
			INT iIndex
			VECTOR vSpawnPoint
			FLOAT fSpawnHeading
			FLOAT fWeight
			
			#IF IS_DEBUG_BUILD
				PRINTLN("[IE_DELIVERY][CUTSCENE] DO_PLAYER_WARP_AFTER_CUTSCENE - scriptData.eDropoffID: ", IE_DELIVERY_GET_DROPOFF_NAME_FOR_DEBUG(scriptData.eDropoffID))
			#ENDIF
			
			IF IE_IS_DROPOFF_A_PLAYER_OWNED_WAREHOUSE(scriptData.eDropoffID)
				// Use simple interior spawn points
				IMPORT_EXPORT_GARAGES eIEWarehouseID = GET_IMPORT_EXPORT_WAREHOUSE_FROM_IE_DROPOFF(scriptData.eDropoffID)
				SIMPLE_INTERIORS eSimpleInteriorID = GET_SIMPLE_INTERIOR_ID_FROM_IMPORT_EXPORT_GARAGE_ID(eIEWarehouseID)
						
				WHILE GET_SIMPLE_INTERIOR_OUTSIDE_SPAWN_POINT(eSimpleInteriorID, iIndex, vSpawnPoint, fSpawnHeading, FALSE)
					iIndex += 1
					fWeight = 1.0 - (TO_FLOAT(iIndex)/TO_FLOAT(NUM_NETWORK_PLAYERS))
					
					ADD_CUSTOM_SPAWN_POINT(vSpawnPoint, fSpawnHeading, fWeight #IF IS_DEBUG_BUILD,0.2 #ENDIF)  
				ENDWHILE
			ELSE
				WHILE IE_DELIVERY_GET_AFTER_DESTRUCTION_SPAWN_POINT(scriptData.eDropoffID, iIndex, vSpawnPoint, fSpawnHeading, scriptData.bExecuteParallelWarp)
					iIndex += 1
					fWeight = 1.0 - (TO_FLOAT(iIndex)/TO_FLOAT(NUM_NETWORK_PLAYERS))
					
					ADD_CUSTOM_SPAWN_POINT(vSpawnPoint, fSpawnHeading, fWeight #IF IS_DEBUG_BUILD,0.2 #ENDIF)   
				ENDWHILE
			ENDIF
			
			SET_BIT(scriptData.iBS, BS_IE_DELIVERY_CREATED_SPAWN_POINTS)
			
			#IF IS_DEBUG_BUILD
				PRINTLN("[IE_DELIVERY][CUTSCENE] DO_PLAYER_WARP_AFTER_CUTSCENE - BS_IE_DELIVERY_CREATED_SPAWN_POINTS set to TRUE")
			#ENDIF
			
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
				//SET_ENTITY_COLLISION(PLAYER_PED_ID(), TRUE)
				//SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
				//TASK_START_SCENARIO_IN_PLACE(PLAYER_PED_ID(), _GET_RANDOM_POST_WARP_SCENARIO())
				//SET_BIT(scriptData.iBS, BS_IE_DELIVERY_STARTED_PLAYER_SCENARIO)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(scriptData.iBS, BS_IE_DELIVERY_CREATED_SPAWN_POINTS)
			IF WARP_TO_SPAWN_LOCATION(SPAWN_LOCATION_CUSTOM_SPAWN_POINTS, FALSE, FALSE, FALSE, FALSE)
			
				#IF IS_DEBUG_BUILD
					PRINTLN("[IE_DELIVERY][CUTSCENE] DO_PLAYER_WARP_AFTER_CUTSCENE - Warp to custom spawn point is done.")
				#ENDIF
				
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				AND IS_BIT_SET(scriptData.iBS, BS_IE_DELIVERY_FROZEN_AFTER_LEAVING_VEH)
					FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
					CLEAR_BIT(scriptData.iBS, BS_IE_DELIVERY_FROZEN_AFTER_LEAVING_VEH)
					#IF IS_DEBUG_BUILD
						PRINTLN("[IE_DELIVERY][CUTSCENE] DO_PLAYER_WARP_AFTER_CUTSCENE - Unfreezing player pos.")
					#ENDIF
				ENDIF
				
				SET_BIT(scriptData.iBS, BS_IE_DELIVERY_AFTER_DESTRUCTION_WARP_DONE)
				
				CLEAR_CUSTOM_SPAWN_POINTS()
				
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
				ENDIF
				
				RETURN TRUE
			ENDIF
		ENDIF	
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_IN_CARGOBOB()
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
		VEHICLE_INDEX veh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(), TRUE)
		
		IF DOES_ENTITY_EXIST(veh)
		AND NOT IS_ENTITY_DEAD(veh)
			MODEL_NAMES vehModel = GET_ENTITY_MODEL(veh)
			IF vehModel = CARGOBOB
			OR vehModel = CARGOBOB2
			OR vehModel = CARGOBOB3
			OR vehModel = CARGOBOB4
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_CLOSE_TO_IE_DROPOFF()
	
	IF NOT DOES_ENTITY_EXIST(PLAYER_PED_ID())
	OR IS_ENTITY_DEAD(PLAYER_PED_ID())
		RETURN FALSE
	ENDIF
	
	//Distance threshold
	CONST_FLOAT fStandardVehThreshold 	100.0
	CONST_FLOAT fCargobobThreshold 		400.0
	
	//We have a dropoff to deliver to
	IF g_IEDeliveryData.eClosestActiveDropoffID != IE_DROPOFF_INVALID
		VECTOR vStartPoint = IE_DELIVERY_GET_DROPOFF_COORDS(g_IEDeliveryData.eClosestActiveDropoffID)
		
		//We have some valid delivery coords
		IF NOT IS_VECTOR_ZERO(vStartPoint)
			FLOAT fDist = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vStartPoint)
			FLOAT fMinDist = fStandardVehThreshold
			
			IF IS_PLAYER_IN_CARGOBOB()
				fMinDist = fCargobobThreshold
			ENDIF
			
			IF fDist < fMinDist
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC _MAINTAIN_BLOCKING_PHONE()
	IF g_IEDeliveryData.bStartCutscene
	OR playerBD[PARTICIPANT_ID_TO_INT()].eState > IE_DELIVERY_SCRIPT_STATE_IDLE
	OR IS_PLAYER_CLOSE_TO_IE_DROPOFF()
		DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	ENDIF
ENDPROC

PROC RUN_MAIN_CLIENT_LOGIC()

	TERMINATE_IF_LOCAL_PLAYER_DEAD_OR_TOO_FAR()
	_MAINTAIN_EXTRA_IPL()
	_MAINTAIN_CALCULATING_GARAGE_DOOR_BOUNDS()
	MAINTAIN_CUTSCENE_SOUNDS()
	_MONITOR_SEATS_FOR_GOON_PICKUP_CUTSCENE()
	_MAINTAIN_BLOCKING_PHONE()
	
	BOOL bPedKickedOutFromCarVariation
	
	IF NOT scriptData.bViaCargobob
	AND scriptData.dropoffType = IE_DELIVERY_DROPOFF_CUTSCENE_TYPE_GOON_PICKUP
	AND NOT g_IEDeliveryData.bCutsceneTriggeredByProxy
	#IF IS_DEBUG_BUILD
	AND NOT scriptData.cutscene.dbg.bPlayedViaDebug
	#ENDIF
		bPedKickedOutFromCarVariation = TRUE
	ENDIF
	
	IF IS_IE_DELIVERY_SCRIPT_STATE(IE_DELIVERY_SCRIPT_STATE_LOADING)
		
		TERMINATE_IF_NO_LONGER_THE_CLOSEST_DROPOFF()
		_REQUEST_ALL_ASSETS()
			
		IF _HAVE_ALL_ASSETS_LOADED()
			SET_IE_DELIVERY_SCRIPT_STATE(IE_DELIVERY_SCRIPT_STATE_IDLE)
		ELSE
			#IF IS_DEBUG_BUILD
				PRINTLN("[IE_DELIVERY][CUTSCENE] RUN_MAIN_CLIENT_LOGIC - Waiting for _HAVE_ALL_ASSETS_LOADED")
			#ENDIF
		ENDIF
		
	ELIF IS_IE_DELIVERY_SCRIPT_STATE(IE_DELIVERY_SCRIPT_STATE_IDLE)
		
		TERMINATE_IF_PLAYER_LEAVES_VEHICLE()
		TERMINATE_IF_PLAYER_DELIVERER_LEFT_OR_NOT_OK()
		TERMINATE_IF_NO_LONGER_THE_CLOSEST_DROPOFF()
		
		IF scriptData.pDeliverer = PLAYER_ID()
			IF GET_IE_VEHICLE_PLAYER_JUST_DELIVERED(PLAYER_ID()) != GB_GET_EXPORT_VEHICLE_ENUM(scriptData.vehDelivered)
				SET_IE_VEHICLE_I_JUST_DELIVERED(GB_GET_EXPORT_VEHICLE_ENUM(scriptData.vehDelivered))
			ENDIF
		ENDIF
		
		IF scriptData.eDropoffID = IE_DROPOFF_SHOWROOM_6
		AND NOT IS_BIT_SET(scriptData.iBS, BS_IE_DELIVERY_CREATED_SHOWROOM_6_PROPS)
			// This is a special case - we need to create a gate that will cover the IPL swap for the interior of Simeon's showroom
			_CREATE_IE_DELIVERY_CUTSCENE_PROPS_()
			SET_ENTITY_VISIBLE(cutsceneData.objProps[0], TRUE)
			SET_ENTITY_VISIBLE(cutsceneData.objProps[1], TRUE)
			SET_BIT(scriptData.iBS, BS_IE_DELIVERY_CREATED_SHOWROOM_6_PROPS)
		ENDIF
		
		IF IE_DELIVERY_SHOULD_CUTSCENE_BE_TRIGGERED()
			SET_IE_DELIVERY_SCRIPT_STATE(IE_DELIVERY_SCRIPT_STATE_FADE_OUT_FOR_START)
			
			// This is to let everyone know that we're playing cutscene!
			SET_BIT(GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].IEDeliveryBD.iBS, BS_IE_DELIVERY_GLOBAL_PLAYER_BD_PLAYING_DELIVERY_CUTSCENE)
			SET_BIT(GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].IEDeliveryBD.iBS, BS_IE_DELIVERY_GLOBAL_PLAYER_BD_DONT_MODIFY_CRATES)
			g_IEDeliveryData.bStartCutscene = FALSE
			
			IF scriptData.pDeliverer = PLAYER_ID()
				BROADCAST_START_DELIVERY_CUTSCENE(scriptData.eDropoffID)
			ENDIF
			
			scriptData.iExportVehValue = g_iVehicleExportCurrentVehicleValue // Freeze the value at the start of cutscene
			
			#IF IS_DEBUG_BUILD
			IF scriptData.cutscene.dbg.bPlayedViaDebug
				IF debugData.bEditMode
					IE_DELIVERY_CLEANUP_EVERYTHING()
				ENDIF
			ENDIF
			#ENDIF
			
			IF bPedKickedOutFromCarVariation
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				AND NOT IS_ENTITY_DEAD(scriptData.vehDelivered)
					IF IS_ENTRY_POINT_FOR_SEAT_CLEAR(PLAYER_PED_ID(), scriptData.vehDelivered, GET_SEAT_PED_IS_IN(PLAYER_PED_ID()))
						NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID(), DEFAULT, ECF_DONT_DEFAULT_WARP_IF_DOOR_BLOCKED | ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP)
						SET_BIT(scriptData.iBS, BS_IE_DELIVERY_TASKED_TO_LEAVE_VEH)
						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iBS, BS_PLAYER_BD_IE_DELIVERY_LEAVING_VEH_FOR_CUTSCENE)
						RESET_NET_TIMER(scriptData.timerAfterDoorClosed)
						#IF IS_DEBUG_BUILD
							PRINTLN("[IE_DELIVERY][CUTSCENE] RUN_MAIN_CLIENT_LOGIC - Tasked with leaving the vehicle.")
						#ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
							PRINTLN("[IE_DELIVERY][CUTSCENE] RUN_MAIN_CLIENT_LOGIC - Would kick out but won't because entry point is not clear.")
						#ENDIF
					ENDIF
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
					PRINTLN("[IE_DELIVERY][CUTSCENE] RUN_MAIN_CLIENT_LOGIC - Not kicking out. (Triggered by proxy: ", GET_BOOL_DISPLAY_STRING_FROM_BOOL(g_IEDeliveryData.bCutsceneTriggeredByProxy), ").")
				#ENDIF
			ENDIF			
			
			IE_DELIVERY_CUTSCENE_CREATE_ALL_ASSETS()
			REMOVE_PROJECTILES_FROM_DELIVERED_VEH()
			
			REINIT_NET_TIMER(scriptData.timerCutsceneDelay)
		ENDIF
		
	ELIF IS_IE_DELIVERY_SCRIPT_STATE(IE_DELIVERY_SCRIPT_STATE_FADE_OUT_FOR_START)
		IF scriptData.cutscene.sScenes[0].iFadeInTime > 0
		OR scriptData.cutscene.iScenesCount = 0
		OR g_IEDeliveryData.bCutsceneTriggeredByProxy
			IF NOT IS_SCREEN_FADING_OUT()
			AND NOT IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_OUT(250)
				#IF IS_DEBUG_BUILD
					PRINTLN("[IE_DELIVERY][CUTSCENE] RUN_MAIN_CLIENT_LOGIC - Fading screen out before the start of cutscene.")
				#ENDIF
			ENDIF
		ENDIF
		
		IF scriptData.cutscene.iScenesCount = 0
			IE_DELIVERY_START_CUTSCENE()
			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iBS, BS_PLAYER_BD_IE_DELIVERY_STARTED_CUTSCENE)
			SET_IE_DELIVERY_SCRIPT_STATE(IE_DELIVERY_SCRIPT_STATE_PLAYING_CUTSCENE)
		ELSE
		
			IF IE_DELIVERY_IS_EVERYTHING_READY_FOR_CUTSCENE_START()
				BOOL bStartCutscene
				
				IF bPedKickedOutFromCarVariation
					IF NOT HAS_NET_TIMER_STARTED(scriptData.timerAfterDoorClosed)
						IF (NOT IS_ENTITY_DEAD(scriptData.vehDelivered) AND GET_VEHICLE_DOOR_ANGLE_RATIO(scriptData.vehDelivered, SC_DOOR_FRONT_LEFT) <= 0.0)
						OR HAS_NET_TIMER_EXPIRED(scriptData.timerCutsceneDelay, 4000) 
							START_NET_TIMER(scriptData.timerAfterDoorClosed)
						ENDIF
					ELSE
						IF HAS_NET_TIMER_EXPIRED(scriptData.timerAfterDoorClosed, 250)
							// Let the door close sound play by introducing this little delay after the door is closed
							bStartCutscene = TRUE
						ENDIF
					ENDIF
				ELSE
					IF (IS_SCREEN_FADED_OUT() AND HAS_NET_TIMER_EXPIRED(scriptData.timerCutsceneDelay, 1000))
					OR (scriptData.cutscene.sScenes[0].iFadeInTime = 0 AND NOT g_IEDeliveryData.bCutsceneTriggeredByProxy)
					OR scriptData.cutscene.iScenesCount = 0
						bStartCutscene = TRUE
					ENDIF
				ENDIF
				
				If bStartCutscene
					START_MP_CUTSCENE()
					SET_ALL_IE_DELIVERY_ASSETS_VISIBLE(TRUE)
					_SET_INITIAL_STATE_FOR_ALL_ASSETS()
					IE_DELIVERY_START_CUTSCENE()
					
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iBS, BS_PLAYER_BD_IE_DELIVERY_STARTED_CUTSCENE)
					SET_IE_DELIVERY_SCRIPT_STATE(IE_DELIVERY_SCRIPT_STATE_PLAYING_CUTSCENE)
					
					IF g_IEDeliveryData.bCutsceneTriggeredByProxy
					AND IS_SCREEN_FADED_OUT()
					AND (scriptData.cutscene.iScenesCount > 0 AND scriptData.cutscene.sScenes[0].iFadeInTime = 0)
						DO_SCREEN_FADE_IN(500)
					ENDIF
					
					#IF IS_DEBUG_BUILD
					IF NOT scriptData.cutscene.dbg.bPlayedViaDebug
					#ENDIF
					SET_NO_LOADING_SCREEN(TRUE)
					SET_BIT(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_NO_LOADING_SCREEN_SET)
					#IF IS_DEBUG_BUILD
						PRINTLN("[IE_DELIVERY][CUTSCENE] SET_NO_LOADING_SCREEN is now set to TRUE")
					#ENDIF
					#IF IS_DEBUG_BUILD
					ENDIF
					#ENDIF 
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_IE_DELIVERY_SCRIPT_STATE(IE_DELIVERY_SCRIPT_STATE_PLAYING_CUTSCENE)
		IF NOT IS_BIT_SET(scriptData.cutscene.iBSScenesInit, 0)
			cutsceneData.iBS = 0
			cutsceneData.iPropsTransitionInitBS = 0
			cutsceneData.fFrozenSpeed = 0.0
			cutsceneData.bDontMoveClone = FALSE
		ENDIF
		
		SIMPLE_CUTSCENE_MAINTAIN(scriptData.cutscene)
		MAINTAIN_MOVE_PLAYER_OUT_OF_THE_WAY()
		MAINTAIN_DELIVERY_SHARD()
		
		IF scriptData.cutscene.bPlaying
			IF scriptData.bViaCargobob		
				IE_DELIVERY_CONTROL_VEH_PROXY_CARGOBOB()
				
				SWITCH IE_DELIVERY_GET_DROPOFF_CUTSCENE_TYPE_FROM_DROPOFF_ID(scriptData.eDropoffID)

					CASE IE_DELIVERY_DROPOFF_CUTSCENE_TYPE_CRANE
						IE_DELIVERY_CONTROL_PROPS(-1)
						IE_DELIVERY_CONTROL_VEH_CLONE_IN_DOCKS(1)
						IE_DELIVERY_CONTROL_SPREADER(1)
					BREAK
					
					CASE IE_DELIVERY_DROPOFF_CUTSCENE_TYPE_RAMP
						IE_DELIVERY_CONTROL_VEH_ACTORS_ON_RAMP_CARGOBOB()
					BREAK
					
					CASE IE_DELIVERY_DROPOFF_CUTSCENE_TYPE_CARPARK
					CASE IE_DELIVERY_DROPOFF_CUTSCENE_TYPE_GARAGE
						IE_DELIVERY_CONTROL_VEH_CLONE_IN_GARAGE_CARGOBOB()
					BREAK
					
					CASE IE_DELIVERY_DORPOFF_CUTSCENE_TYPE_CARPARK_ALT
						IE_DELIVERY_CONTROL_PROPS(1)
						IE_DELIVERY_CONTROL_VEH_CLONE_IN_GARAGE_ALT(1)
					BREAK
					
					CASE IE_DELIVERY_DROPOFF_CUTSCENE_TYPE_GARAGE_ALT
						IF scriptData.eDropoffID = IE_DROPOFF_SHOWROOM_6
							IE_DELIVERY_CONTROL_PROPS()
						ENDIF
						
						IF DOES_ENTITY_EXIST(cutsceneData.pedProxyMechanic)
							IE_DELIVERY_CONTROL_VEH_CLONE_WITH_CARGOBOB_MECHANIC()
						ELSE
							IF scriptData.eDropoffID = IE_DROPOFF_GANGSTER_7
							OR scriptData.eDropoffID = IE_DROPOFF_SHOWROOM_6
								IE_DELIVERY_CONTROL_VEH_CLONE_IN_GARAGE_ALT_2(TRUE)
							ELIF scriptData.eDropoffID = IE_DROPOFF_POLICE_STATION_1
								IE_DELIVERY_CONTROL_VEH_CLONE_IN_GARAGE_CARGOBOB()
							ELSE
								IE_DELIVERY_CONTROL_VEH_CLONE_IN_GARAGE_ALT_CARGOBOB()
							ENDIF
						ENDIF
					BREAK
					CASE IE_DELIVERY_DROPOFF_CUTSCENE_TYPE_SCRAPYARD
						IE_DELIVERY_CONTROL_VEH_CLONE_IN_SCRAP_CARGOBOB()
					BREAK
					CASE IE_DELIVERY_DROPOFF_CUTSCENE_TYPE_SCRAPYARD_GARAGE
						IE_DELIVERY_CONTROL_VEH_CLONE_IN_SCRAP_GARAGE_CARGOBOB()
					BREAK
					CASE IE_DELIVERY_DROPOFF_CUTSCENE_TYPE_GOON_PICKUP
						IE_DELIVERY_CONTROL_GOON_DRIVING_AWAY_CARGOBOB()
					BREAK
				ENDSWITCH
			ELSE
				IE_DELIVERY_CONTROL_PROPS()
				
				SWITCH IE_DELIVERY_GET_DROPOFF_CUTSCENE_TYPE_FROM_DROPOFF_ID(scriptData.eDropoffID)
					CASE IE_DELIVERY_DROPOFF_CUTSCENE_TYPE_RAMP
						IE_DELIVERY_CONTROL_VEH_ACTORS_ON_RAMP()
						IE_DELIVERY_CONTROL_VEH_CLONE_ON_RAMP()
					BREAK
					CASE IE_DELIVERY_DROPOFF_CUTSCENE_TYPE_CRANE
						IE_DELIVERY_CONTROL_VEH_CLONE_IN_DOCKS()
						IE_DELIVERY_CONTROL_SPREADER()
					BREAK
					CASE IE_DELIVERY_DROPOFF_CUTSCENE_TYPE_CARPARK
						IE_DELIVERY_CONTROL_VEH_CLONE_IN_CARPARK()
					BREAK
					CASE IE_DELIVERY_DROPOFF_CUTSCENE_TYPE_GARAGE
						IE_DELIVERY_CONTROL_VEH_CLONE_IN_GARAGE()
					BREAK
					CASE IE_DELIVERY_DROPOFF_CUTSCENE_TYPE_GARAGE_ALT
						IF scriptData.eDropoffID = IE_DROPOFF_GANGSTER_7
						OR scriptData.eDropoffID = IE_DROPOFF_GANGSTER_8
						OR scriptData.eDropoffID = IE_DROPOFF_SHOWROOM_6
						OR scriptData.eDropoffID = IE_DROPOFF_POLICE_STATION_3
						OR scriptData.eDropoffID = IE_DROPOFF_POLICE_STATION_6
							IE_DELIVERY_CONTROL_VEH_CLONE_IN_GARAGE_ALT_2()
						ELSE
							IE_DELIVERY_CONTROL_VEH_CLONE_IN_GARAGE_ALT()
						ENDIF
					BREAK
					//CASE IE_DELIVERY_DORPOFF_CUTSCENE_TYPE_CARPARK_ALT
					//	IE_DELIVERY_CONTROL_VEH_CLONE_IN_CARPARK_ALT()
					//BREAK
					CASE IE_DELIVERY_DROPOFF_CUTSCENE_TYPE_SCRAPYARD
						IE_DELIVERY_CONTROL_VEH_CLONE_IN_SCRAP()
					BREAK
					CASE IE_DELIVERY_DROPOFF_CUTSCENE_TYPE_SCRAPYARD_GARAGE
						IE_DELIVERY_CONTROL_VEH_CLONE_IN_SCRAP_GARAGE()
					BREAK
					CASE IE_DELIVERY_DROPOFF_CUTSCENE_TYPE_GOON_PICKUP
						IE_DELIVERY_CONTROL_GOON_DRIVING_AWAY()
					BREAK
				ENDSWITCH
			ENDIF
			
			#IF IS_DEBUG_BUILD
			IF NOT scriptData.cutscene.dbg.bPlayedViaDebug
			#ENDIF
			IF scriptData.bExecuteParallelWarp
				// If warp should happen in parallel with playing the cutscene, don it here
				IF DO_PLAYER_WARP_AFTER_CUTSCENE()
					IF SIMPLE_CUTSCENE_IS_SCENE_RUNNING(scriptData.cutscene, scriptData.cutscene.iScenesCount - 1)
						IF scriptData.cutscene.iCurrentSceneElapsedTime >= scriptData.iParallelWarpLastSceneCutoffTime
							SET_IE_DELIVERY_SCRIPT_STATE(IE_DELIVERY_SCRIPT_STATE_WARPING)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			#IF IS_DEBUG_BUILD
			ENDIF
			#ENDIF
		ELSE
			SET_BIT(cutsceneData.iBS, BS_IE_DELIVERY_CUTSCENE_SCREEN_FADED_OUT)
			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iBS, BS_PLAYER_BD_IE_DELIVERY_FINISHED_CUTSCENE)
			CLEAR_BIT(GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].IEDeliveryBD.iBS, BS_IE_DELIVERY_GLOBAL_PLAYER_BD_DONT_MODIFY_CRATES)
			
			IE_DELIVERY_CLEANUP_EVERYTHING()
			#IF IS_DEBUG_BUILD
			IF debugData.bEditMode
				IE_DELIVERY_CUTSCENE_CREATE_ALL_ASSETS()
			ENDIF
			#ENDIF
			
			#IF IS_DEBUG_BUILD
			IF NOT scriptData.cutscene.dbg.bPlayedViaDebug
			#ENDIF
			SET_IE_DELIVERY_SCRIPT_STATE(IE_DELIVERY_SCRIPT_STATE_VEHICLE_CLEANUP)
			#IF IS_DEBUG_BUILD
			ELSE
			DO_SCREEN_FADE_IN(500)
			SET_IE_DELIVERY_SCRIPT_STATE(IE_DELIVERY_SCRIPT_STATE_IDLE)
			ENDIF
			#ENDIF
		ENDIF
	ENDIF
	
	IF IS_IE_DELIVERY_SCRIPT_STATE(IE_DELIVERY_SCRIPT_STATE_VEHICLE_CLEANUP)
		
		IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBS, BS_PLAYER_BD_IE_DELIVERY_PLAYER_MOVED_OUT)
			IF NOT IS_BIT_SET(scriptData.iBS, BS_IE_DELIVERY_STARTED_WARP_TO_WAREHOUSE)
			
				// Put all the player who can access the warehouse inside
				
				IF IE_IS_DROPOFF_A_PLAYER_OWNED_WAREHOUSE(scriptData.eDropoffID)
					#IF IS_DEBUG_BUILD
						PRINTLN("[IE_DELIVERY][CUTSCENE] RUN_MAIN_CLIENT_LOGIC - This dropoff is a player's owned warehouse...")
					#ENDIF
								
					IF CAN_PLAYER_USE_PROPERTY()
					AND NOT IS_PLAYER_BLOCKED_FROM_PROPERTY(TRUE, PROPERTY_IE_WAREHOUSE)
						#IF IS_DEBUG_BUILD
							PRINTLN("[IE_DELIVERY][CUTSCENE] RUN_MAIN_CLIENT_LOGIC - We can use the property.")
						#ENDIF
					
						IMPORT_EXPORT_GARAGES eIEWarehouseID = GET_IMPORT_EXPORT_WAREHOUSE_FROM_IE_DROPOFF(scriptData.eDropoffID)
						SIMPLE_INTERIORS eSimpleInteriorID = GET_SIMPLE_INTERIOR_ID_FROM_IMPORT_EXPORT_GARAGE_ID(eIEWarehouseID)
					
						IF CAN_PLAYER_ENTER_SIMPLE_INTERIOR(PLAYER_ID(), eSimpleInteriorID, 0)
							#IF IS_DEBUG_BUILD
								PRINTLN("[IE_DELIVERY][CUTSCENE] RUN_MAIN_CLIENT_LOGIC - And we can use this warehouse.")
							#ENDIF
							
							IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
							AND GB_ARE_PLAYERS_IN_SAME_GANG(PLAYER_ID(), scriptData.pDeliverer)
								#IF IS_DEBUG_BUILD
									PRINTLN("[IE_DELIVERY][CUTSCENE] RUN_MAIN_CLIENT_LOGIC - And deliverer is in the same gang us as - we can enter with them!")
								#ENDIF
								SET_BIT(scriptData.iBS, BS_IE_DELIVERY_STARTED_WARP_TO_WAREHOUSE)
							ELSE
								// If we're the owner of the warehouse to which we delivered warp us inside. (er, you cant deliver to the warehouse if you're not part of the gang so that will never kick in)
								IF scriptData.pDeliverer = PLAYER_ID()
									#IF IS_DEBUG_BUILD
										PRINTLN("[IE_DELIVERY][CUTSCENE] RUN_MAIN_CLIENT_LOGIC - Starting warehouse warp - solo player.")
									#ENDIF
									SET_BIT(scriptData.iBS, BS_IE_DELIVERY_STARTED_WARP_TO_WAREHOUSE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(scriptData.iBS, BS_IE_DELIVERY_STARTED_WARP_TO_WAREHOUSE)
				IF DO_WARP_LOCAL_PLAYER_INSIDE_IE_WAREHOUSE(GET_IMPORT_EXPORT_WAREHOUSE_FROM_IE_DROPOFF(scriptData.eDropoffID))
					#IF IS_DEBUG_BUILD
						PRINTLN("[IE_DELIVERY][CUTSCENE] RUN_MAIN_CLIENT_LOGIC - Warp to warehouse done, cleaning up.")
					#ENDIF
					g_bEndScreenSuppressFadeIn = FALSE
					SCRIPT_CLEANUP()
				ENDIF
			ELSE
				SET_IE_DELIVERY_SCRIPT_STATE(IE_DELIVERY_SCRIPT_STATE_WARPING)
			ENDIF
		ELSE
			// If we weren't tasked to leave the vehicle we are probably in the cargobob so we need to warp the whole thing
			
			INT i
			BOOL bCanFadeIn = FALSE
			BOOL bDoWarpOnFoot = FALSE // revert to warping on foot, there's no cargobob we can warp
			
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				AND scriptData.bViaCargobob
					VEHICLE_INDEX vehPlayer = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()) // This teally is always gonna be cargobob
					
					IF NOT IS_ENTITY_DEAD(vehPlayer)
						BOOL bHasSomeoneFadedBob
						
						IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBS, BS_PLAYER_BD_IE_DELIVERY_FADED_IN_PROXY_VEH)
							IF _DID_ANYONE_DO_THE_THING(BS_PLAYER_BD_IE_DELIVERY_FADED_IN_PROXY_VEH)
								SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iBS, BS_PLAYER_BD_IE_DELIVERY_FADED_IN_PROXY_VEH)
								bHasSomeoneFadedBob = TRUE
							ENDIF
						ELSE
							bHasSomeoneFadedBob = TRUE
						ENDIF
						
						IF NOT bHasSomeoneFadedBob
							IF NETWORK_HAS_CONTROL_OF_ENTITY(vehPlayer)
								// Gotta warp the cargobob
								IF NOT IS_BIT_SET(scriptData.iBS, BS_IE_DELIVERY_CREATED_CARGOBOB_SPAWN_POINTS)
									CLEAR_CUSTOM_SPAWN_POINTS()
									USE_CUSTOM_SPAWN_POINTS(TRUE, FALSE, TRUE, 0.01, 2, DEFAULT, DEFAULT, 0.01)
									
									VECTOR vSpawnPoint
									FLOAT fHeading = GET_ENTITY_HEADING(vehPlayer)
									FLOAT fWeight = 1.0
									REPEAT 17 i
										vSpawnPoint = GET_CARGOBOB_SPAWN_POINT_AFTER_IE_CUTSCENE(i)
										ADD_CUSTOM_SPAWN_POINT(vSpawnPoint, fHeading, fWeight)
										fWeight = fWeight - (1.0/17.0)
									ENDREPEAT
									SET_BIT(scriptData.iBS, BS_IE_DELIVERY_CREATED_CARGOBOB_SPAWN_POINTS)
								ENDIF
								
								IF IS_BIT_SET(scriptData.iBS, BS_IE_DELIVERY_CREATED_CARGOBOB_SPAWN_POINTS)
									IF WARP_TO_SPAWN_LOCATION(SPAWN_LOCATION_CUSTOM_SPAWN_POINTS, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, TRUE)
										#IF IS_DEBUG_BUILD
											PRINTLN("[IE_DELIVERY][CUTSCENE] RUN_MAIN_CLIENT_LOGIC - Warped cargobob, gonna fade in veh proxy.")
										#ENDIF
										NETWORK_FADE_IN_ENTITY(vehPlayer, TRUE)
										FREEZE_ENTITY_POSITION(vehPlayer, FALSE)
										ACTIVATE_PHYSICS(vehPlayer)
										SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iBS, BS_PLAYER_BD_IE_DELIVERY_FADED_IN_PROXY_VEH)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					
						IF NOT HAS_NET_TIMER_STARTED(scriptData.timerAfterBobFadein)
							IF _DID_EVERYONE_REGISTERED_THE_THING(BS_PLAYER_BD_IE_DELIVERY_FADED_IN_PROXY_VEH)
								#IF IS_DEBUG_BUILD
									PRINTLN("[IE_DELIVERY][CUTSCENE] RUN_MAIN_CLIENT_LOGIC - Everyone registered that the bob is fading.")
								#ENDIF
								START_NET_TIMER(scriptData.timerAfterBobFadein)
							ENDIF
						ELSE
							IF HAS_NET_TIMER_EXPIRED(scriptData.timerAfterBobFadein, 1500)
								bCanFadeIn = (NOT NETWORK_IS_ENTITY_FADING(vehPlayer))
							ENDIF
						ENDIF
						
						IF NOT bCanFadeIn
							#IF IS_DEBUG_BUILD
								PRINTLN("[IE_DELIVERY][CUTSCENE] RUN_MAIN_CLIENT_LOGIC - Waiting for cargobob to stop fading: ", GET_STRING_FROM_BOOL(NETWORK_IS_ENTITY_FADING(vehPlayer)), " did everyone register start of fade: ", GET_STRING_FROM_BOOL(_DID_EVERYONE_REGISTERED_THE_THING(BS_PLAYER_BD_IE_DELIVERY_FADED_IN_PROXY_VEH)))
							#ENDIF
						ENDIF
					ELSE
						bDoWarpOnFoot = TRUE
					ENDIF
				ELSE
					bDoWarpOnFoot = TRUE
				ENDIF
			ELSE
				bCanFadeIn = TRUE
			ENDIF
			
			IF bDoWarpOnFoot
				// Oops, there's no cargobob to warp! Probably got destroyed or something, we gotta warp on foot
				#IF IS_DEBUG_BUILD
					PRINTLN("[IE_DELIVERY][CUTSCENE] RUN_MAIN_CLIENT_LOGIC - Something happened to the cargobob we were using, warping on foot.")
				#ENDIF
				SET_IE_DELIVERY_SCRIPT_STATE(IE_DELIVERY_SCRIPT_STATE_WARPING)
				EXIT
			ENDIF
			
			IF bCanFadeIn
				#IF IS_DEBUG_BUILD
					PRINTLN("[IE_DELIVERY][CUTSCENE] RUN_MAIN_CLIENT_LOGIC - bCanFadeIn is now TRUE, doing the fade in.")
				#ENDIF
				
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE, NSPC_PREVENT_VISIBILITY_CHANGES)
				RESET_NET_TIMER(scriptData.timerAfterBobFadein)
				
				// Unfreeze again just in case
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				AND scriptData.bViaCargobob
					VEHICLE_INDEX vehPlayer = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					IF NOT IS_ENTITY_DEAD(vehPlayer)
					AND NETWORK_HAS_CONTROL_OF_ENTITY(vehPlayer)
						FREEZE_ENTITY_POSITION(vehPlayer, FALSE)
						ACTIVATE_PHYSICS(vehPlayer)
						#IF IS_DEBUG_BUILD
							PRINTLN("[IE_DELIVERY][CUTSCENE] RUN_MAIN_CLIENT_LOGIC - Unfreezing cargobob (again).")
						#ENDIF
					ENDIF
				ENDIF
				
				DO_SCREEN_FADE_IN(500)
				SCRIPT_CLEANUP()
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_IE_DELIVERY_SCRIPT_STATE(IE_DELIVERY_SCRIPT_STATE_WARPING)
		
		// Warp player on foot to a spawn point after the cutscene
		
		IF DO_PLAYER_WARP_AFTER_CUTSCENE()
			IF NOT HAS_NET_TIMER_STARTED(scriptData.timerAfterWarp)
				IF NOT scriptData.bDontSimulateInputGaitAfterWarp
					SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 850)
				ELSE
					//FLOAT fStartPhase = 0.4
					//START_INTERACTION_ANIM(ENUM_TO_INT(INTERACTION_ANIM_TYPE_SPECIAL), ENUM_TO_INT(scriptData.eSpecialInteraction), TRUE, DEFAULT, TRUE, FALSE, TRUE, fStartPhase)
				ENDIF
				
				NETWORK_FADE_IN_ENTITY(PLAYER_PED_ID(), TRUE)
				FORCE_PLAYER_LOCALLY_VISIBLE_FOR_FADE()
				
				SET_ENTITY_COLLISION(PLAYER_PED_ID(), TRUE)
				START_NET_TIMER(scriptData.timerAfterWarp)
			ENDIF
			
			IF scriptData.cutscene.bPlaying
			AND (scriptData.bDontSimulateInputGaitAfterWarp OR (scriptData.bDontSimulateInputGaitAfterWarp = FALSE AND HAS_NET_TIMER_EXPIRED(scriptData.timerAfterWarp, 400)))
				SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iBS, BS_PLAYER_BD_IE_DELIVERY_FINISHED_CUTSCENE)
				CLEAR_BIT(GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].IEDeliveryBD.iBS, BS_IE_DELIVERY_GLOBAL_PLAYER_BD_DONT_MODIFY_CRATES)
				SIMPLE_CUTSCENE_STOP(scriptData.cutscene, TRUE, TRUE)
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				IE_DELIVERY_CLEANUP_EVERYTHING()
				SIMPLE_CUTSCENE_KILL_CAMERA(scriptData.cutscene)
				
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
					SET_ENTITY_ALPHA(PLAYER_PED_ID(), 255, FALSE)
				ENDIF
				
				SET_BIT(scriptData.iBS, BS_IE_DELIVERY_AFTER_FINAL_PLAYER_FADE_IN)
				
				#IF IS_DEBUG_BUILD
					PRINTLN("[IE_DELIVERY][CUTSCENE] RUN_MAIN_CLIENT_LOGIC - Stopping cutscene now because it's still playing in state warping.")
				#ENDIF
			ENDIF
			
			IF NOT IS_BIT_SET(scriptData.iBS, BS_IE_DELIVERY_AFTER_FINAL_PLAYER_FADE_IN)
			AND NOT scriptData.bExecuteParallelWarp
				IF scriptData.bDontSimulateInputGaitAfterWarp
				OR HAS_NET_TIMER_EXPIRED(scriptData.timerAfterWarp, 400)
					
					IF MPGlobals.bStartedMPCutscene
						CLEANUP_MP_CUTSCENE(TRUE, FALSE)
					ENDIF
					
					NETWORK_FADE_IN_ENTITY(PLAYER_PED_ID(), TRUE)
					FORCE_PLAYER_LOCALLY_VISIBLE_FOR_FADE()
					
					IF IS_SCREEN_FADED_OUT()
						DO_SCREEN_FADE_IN(500)
					ENDIF
					
					SET_BIT(scriptData.iBS, BS_IE_DELIVERY_AFTER_FINAL_PLAYER_FADE_IN)
					
					#IF IS_DEBUG_BUILD
						PRINTLN("[IE_DELIVERY][CUTSCENE] RUN_MAIN_CLIENT_LOGIC - Doing final fade in.")
					#ENDIF
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(scriptData.iBS, BS_IE_DELIVERY_AFTER_FINAL_PLAYER_FADE_IN)
				IF NOT NETWORK_IS_ENTITY_FADING(PLAYER_PED_ID())
					FORCE_PLAYER_LOCALLY_VISIBLE_FOR_FADE(TRUE)
					
					#IF IS_DEBUG_BUILD
						PRINTLN("[IE_DELIVERY][CUTSCENE] RUN_MAIN_CLIENT_LOGIC - Everything is done. Terminating.")
					#ENDIF
				
					SCRIPT_CLEANUP()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC RUN_MAIN_SERVER_LOGIC()
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF SHOULD_WE_BE_THE_HOST_OF_THIS_SCRIPT()
			NETWORK_REQUEST_TO_BE_HOST_OF_THIS_SCRIPT()
		ENDIF
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF NOT scriptData.cutscene.dbg.bPlayedViaDebug
	#ENDIF
							
	IF ENUM_TO_INT(playerBD[PARTICIPANT_ID_TO_INT()].eState) >= ENUM_TO_INT(IE_DELIVERY_SCRIPT_STATE_PLAYING_CUTSCENE)
	AND HAS_EVERYONE_STARTED_DELIVERY_CUTSCENE()
	
		IF NOT IS_BIT_SET(serverBD.iBS, BS_SERVER_BD_IE_DELIVERY_SENT_DELIVERED_BROADCAST)
			// Send delivery broadcast as soon as the cutscene starts
			IE_DELIVERY_SEND_BROADCAST_LOCAL_PLAYER_DELIVERED_CAR(scriptData.vehDelivered, scriptData.pDeliverer, scriptData.eDropoffID, scriptData.iExportVehValue)
			
			SET_BIT(serverBD.iBS, BS_SERVER_BD_IE_DELIVERY_SENT_DELIVERED_BROADCAST)
			PRINTLN("[IE_DELIVERY][CUTSCENE] RUN_MAIN_SERVER_LOGIC - Sent delivered broadcast")
		ENDIF
		
		IF IS_BIT_SET(serverBD.iBS, BS_SERVER_BD_IE_DELIVERY_SENT_DELIVERED_BROADCAST)
		AND NOT IS_BIT_SET(serverBD.iBS, BS_SERVER_BD_IE_DELIVERY_SET_DELIVERED_FLAG_ON_EXPORT_VEHICLE)
			IF DOES_ENTITY_EXIST(scriptData.vehDelivered) 
			AND NOT IS_ENTITY_DEAD(scriptData.vehDelivered)
				IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(scriptData.vehDelivered)
					NETWORK_REQUEST_CONTROL_OF_ENTITY(scriptData.vehDelivered)
				ENDIF
				
				IF NETWORK_HAS_CONTROL_OF_ENTITY(scriptData.vehDelivered)
					GB_SET_EXPORT_VEHICLE_BITSET_FLAG(scriptData.vehDelivered, CONTRABAND_DECORATOR_BS_BEING_DELIVERED_NOW)
					SET_BIT(serverBD.iBS, BS_SERVER_BD_IE_DELIVERY_SET_DELIVERED_FLAG_ON_EXPORT_VEHICLE)
					PRINTLN("[IE_DELIVERY][CUTSCENE] RUN_MAIN_SERVER_LOGIC - Set a delivered decorator on the car.")
				ELSE
					PRINTLN("[IE_DELIVERY][CUTSCENE] RUN_MAIN_SERVER_LOGIC - Trying to set delivered decorator but don't have control over the car.")
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT HAS_NET_TIMER_STARTED(scriptData.timerRequestServerPermissionToDelete)
			IE_DELIVERY_SEND_BROADCAST_REQUEST_SERVER_PERMISSION_TO_CLEANUP_EXPORT_VEHICLE(scriptData.vehDelivered, scriptData.pDeliverer)
			START_NET_TIMER(scriptData.timerRequestServerPermissionToDelete)
		ENDIF
	
		IF DOES_ENTITY_EXIST(scriptData.vehDelivered) AND NOT IS_ENTITY_DEAD(scriptData.vehDelivered)
		AND (NOT scriptData.bViaCargobob OR (scriptData.bViaCargobob AND DOES_ENTITY_EXIST(scriptData.vehProxy) AND NOT IS_ENTITY_DEAD(scriptData.vehDelivered)))
			IF IS_VEHICLE_EMPTY(scriptData.vehDelivered)
				IF IE_DELIVERY_DOES_SERVER_ACKNOWLEDGE_OUR_DELIVERY(scriptData.vehDelivered)
					IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(scriptData.vehDelivered)
						NETWORK_REQUEST_CONTROL_OF_ENTITY(scriptData.vehDelivered)
					ENDIF
					
					IF scriptData.bViaCargobob
						IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(scriptData.vehProxy)
							NETWORK_REQUEST_CONTROL_OF_ENTITY(scriptData.vehProxy)
						ENDIF
					ENDIF
					
					IF NETWORK_HAS_CONTROL_OF_ENTITY(scriptData.vehDelivered)
					AND (NOT scriptData.bViaCargobob OR (scriptData.bViaCargobob AND NETWORK_HAS_CONTROL_OF_ENTITY(scriptData.vehProxy)))
						IF NOT IS_BIT_SET(serverBD.iBS, BS_SERVER_BD_IE_DELIVERY_STARTED_CAR_FADE_OUT)
							
							IF scriptData.bViaCargobob
								IF IS_VEHICLE_ATTACHED_TO_CARGOBOB(scriptData.vehProxy, scriptData.vehDelivered)
									DETACH_VEHICLE_FROM_CARGOBOB(scriptData.vehProxy, scriptData.vehDelivered)
									#IF IS_DEBUG_BUILD
										PRINTLN("[IE_DELIVERY][CUTSCENE] RUN_MAIN_SERVER_LOGIC - Detaching veh delivered from cargobob...")
									#ENDIF
									EXIT // Wait till this entity detaches before freezing etc.
								ENDIF
							ENDIF
							
							NETWORK_FADE_OUT_ENTITY(scriptData.vehDelivered, TRUE, TRUE)
							SET_ENTITY_COLLISION(scriptData.vehDelivered, FALSE)
							
							FREEZE_ENTITY_POSITION(scriptData.vehDelivered, TRUE)
							
							IF scriptData.bViaCargobob
								NETWORK_FADE_OUT_ENTITY(scriptData.vehProxy, FALSE, TRUE)
								SET_ENTITY_COLLISION(scriptData.vehProxy, FALSE)
								FREEZE_ENTITY_POSITION(scriptData.vehProxy, TRUE)
								#IF IS_DEBUG_BUILD
									PRINTLN("[IE_DELIVERY][CUTSCENE] RUN_MAIN_SERVER_LOGIC - Fading out cargobob...")
								#ENDIF
							ENDIF
							
							SET_BIT(serverBD.iBS, BS_SERVER_BD_IE_DELIVERY_STARTED_CAR_FADE_OUT)
							
							#IF IS_DEBUG_BUILD
								PRINTLN("[IE_DELIVERY][CUTSCENE] RUN_MAIN_SERVER_LOGIC - Started vehicle fade out...")
							#ENDIF
						ENDIF
					ENDIF
					
					IF IS_BIT_SET(serverBD.iBS, BS_SERVER_BD_IE_DELIVERY_STARTED_CAR_FADE_OUT)
					AND NOT NETWORK_IS_ENTITY_FADING(scriptData.vehDelivered)
					
						IF NOT IS_BIT_SET(serverBD.iBS, BS_SERVER_BD_IE_DELIVERY_SENT_VEH_CLEANUP_BROADCAST)
							#IF IS_DEBUG_BUILD
							IF NOT debugData.bDontSendCleanupBroadcast
							#ENDIF
							PLAYER_INDEX pOwner = GB_GET_OWNER_OF_CONTRABAND(scriptData.vehDelivered)
							INT iIndex = GB_GET_INDEX_OF_CONTRABAND(scriptData.vehDelivered)
							BROADCAST_OK_TO_CLEANUP_EXPORT_VEHICLE(pOwner, scriptData.modelVehDelivered, iIndex)
							#IF IS_DEBUG_BUILD
							ENDIF
							#ENDIF
						
							SET_BIT(serverBD.iBS, BS_SERVER_BD_IE_DELIVERY_SENT_VEH_CLEANUP_BROADCAST)
						ENDIF
						
						IF DOES_ENTITY_EXIST(scriptData.vehDelivered)
						AND NETWORK_HAS_CONTROL_OF_ENTITY(scriptData.vehDelivered)
							IF NOT IS_ENTITY_A_MISSION_ENTITY(scriptData.vehDelivered)
							AND NOT GB_IS_EXPORT_ENTITY(scriptData.vehDelivered)
								SET_ENTITY_AS_MISSION_ENTITY(scriptData.vehDelivered, FALSE, TRUE)
								#IF IS_DEBUG_BUILD
									PRINTLN("[IE_DELIVERY][CUTSCENE] RUN_MAIN_SERVER_LOGIC - Mission entity or not export entity.")
								#ENDIF
							ENDIF
							
							IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(scriptData.vehDelivered)
								DELETE_VEHICLE(scriptData.vehDelivered)
								#IF IS_DEBUG_BUILD
									PRINTLN("[IE_DELIVERY][CUTSCENE] RUN_MAIN_SERVER_LOGIC - Did debug cleanup of the vehicle.")
								#ENDIF
							ENDIF
						ELSE
							#IF IS_DEBUG_BUILD
								PRINTLN("[IE_DELIVERY][CUTSCENE] RUN_MAIN_SERVER_LOGIC - Don't have control.")
							#ENDIF
						ENDIF
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
						PRINTLN("[IE_DELIVERY][CUTSCENE] RUN_MAIN_SERVER_LOGIC - Waiting for server to OK this.")
					#ENDIF
					
					IF HAS_NET_TIMER_EXPIRED(scriptData.timerRequestServerPermissionToDelete, 1500)
						IE_DELIVERY_SEND_BROADCAST_REQUEST_SERVER_PERMISSION_TO_CLEANUP_EXPORT_VEHICLE(scriptData.vehDelivered, scriptData.pDeliverer)
						REINIT_NET_TIMER(scriptData.timerRequestServerPermissionToDelete)
						#IF IS_DEBUG_BUILD
							PRINTLN("[IE_DELIVERY][CUTSCENE] RUN_MAIN_SERVER_LOGIC - Resending BROADCAST_REQUEST_SERVER_PERMISSION_TO_CLEANUP_EXPORT_VEHICLE")
						#ENDIF
					ENDIF
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
					PRINTLN("[IE_DELIVERY][CUTSCENE] RUN_MAIN_SERVER_LOGIC - Vehicle is not empty!")
				#ENDIF
				
				IF HAS_NET_TIMER_EXPIRED(scriptData.timerBroadcastLeaveVehicle, 1500)
					BROADCAST_LEAVE_VEHICLE(ALL_PLAYERS_IN_VEHICLE(scriptData.vehDelivered), FALSE, 0.0, 0)
				ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
				//PRINTLN("[IE_DELIVERY][CUTSCENE] RUN_MAIN_SERVER_LOGIC - Veh doesn't exit")
			#ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
			PRINTLN("[IE_DELIVERY][CUTSCENE] RUN_MAIN_SERVER_LOGIC - Not running because state before playing cutscene. Has everyone started? ", GET_STRING_FROM_BOOL(HAS_EVERYONE_STARTED_DELIVERY_CUTSCENE()))
		#ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	ELSE
		#IF IS_DEBUG_BUILD
			PRINTLN("[IE_DELIVERY][CUTSCENE] RUN_MAIN_SERVER_LOGIC - Not running because played by debug.")
		#ENDIF
	ENDIF
	#ENDIF
ENDPROC

/*
PROC PROCESS_VEHICLE_FADE(VEHICLE_INDEX &vehToFade, INT iBitFade, INT iBitMoveHide)
	IF NETWORK_HAS_CONTROL_OF_ENTITY(vehToFade)
		IF HAS_EVERYONE_STARTED_DELIVERY_CUTSCENE()
		//AND NOT WILL_PLAYERS_DOING_ANOTHER_DELIVERY_WITNESS_THE_FADE()
		#IF IS_DEBUG_BUILD
		AND NOT scriptData.cutscene.dbg.bPlayedViaDebug
		#ENDIF
		
			IF NOT IS_BIT_SET(serverBD.iBS, iBitFade)
				NETWORK_FADE_OUT_ENTITY(vehToFade, TRUE, TRUE)
				IF NOT IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(vehToFade)
					FREEZE_ENTITY_POSITION(vehToFade, TRUE)
				ENDIF
				SET_ENTITY_COLLISION(vehToFade, FALSE)
				SET_ENTITY_COMPLETELY_DISABLE_COLLISION(vehToFade, FALSE)
				SET_BIT(serverBD.iBS, iBitFade)
				#IF IS_DEBUG_BUILD
					PRINTLN("[IE_DELIVERY][CUTSCENE] RUN_MAIN_SERVER_LOGIC - Everyone started the cutscene so fading car out.")
				#ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(serverBD.iBS, iBitFade)
		AND NOT IS_BIT_SET(serverBD.iBS, iBitMoveHide)
		AND NOT NETWORK_IS_ENTITY_FADING(vehToFade)
			#IF IS_DEBUG_BUILD
				PRINTLN("[IE_DELIVERY][CUTSCENE] RUN_MAIN_SERVER_LOGIC - Car is faded out so moving it under the map.")
			#ENDIF
			VECTOR vCoordUnderMap = scriptData.vDropoffCoords - <<0.0, 0.0, 20.0>>
			IF NET_WARP_TO_COORD(vCoordUnderMap, 0.0, TRUE, FALSE, TRUE, DEFAULT, DEFAULT, TRUE)
				SET_BIT(serverBD.iBS, iBitMoveHide)
				
				#IF IS_DEBUG_BUILD
					PRINTLN("[IE_DELIVERY][CUTSCENE] RUN_MAIN_SERVER_LOGIC - Car warped under the map.")
				#ENDIF
				
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_ENTITY_DEAD(vehToFade)
		AND NOT NETWORK_HAS_CONTROL_OF_ENTITY(vehToFade)
			IF scriptData.pDeliverer = PLAYER_ID()
				NETWORK_REQUEST_CONTROL_OF_ENTITY(vehToFade)
			ELSE
				#IF IS_DEBUG_BUILD
					PRINTLN("[IE_DELIVERY][CUTSCENE] RUN_MAIN_SERVER_LOGIC - We are the host of this script but we're not a deliverer. Won't request control of vehicle.")
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC RUN_MAIN_SERVER_LOGIC()
	
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF SHOULD_WE_BE_THE_HOST_OF_THIS_SCRIPT()
			NETWORK_REQUEST_TO_BE_HOST_OF_THIS_SCRIPT()
		ENDIF
		EXIT
	ENDIF
	
	VEHICLE_INDEX vehToControl
	VEHICLE_INDEX vehToDeliver	
	IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		#IF IS_DEBUG_BUILD
			PRINTLN("[IE_DELIVERY][CUTSCENE] RUN_MAIN_SERVER_LOGIC - Veh to control is veh delivered because ped is not in any vehicle.")
		#ENDIF
		vehToControl = scriptData.vehDelivered
	ELSE
		BOOL bVehicleAttached
		vehToControl = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		vehToDeliver = IE_DELIVERY_GET_VEHICLE_PLAYER_IS_DELIVERING(scriptData.pDeliverer, bVehicleAttached, bVehicleAttached)
	ENDIF
	
	// Make sure that host always has control of the car we deliver
	IF DOES_ENTITY_EXIST(vehToControl)
		PROCESS_VEHICLE_FADE(vehToControl, BS_SERVER_BD_IE_DELIVERY_STARTED_CAR_FADE_OUT, BS_SERVER_BD_IE_DELIVERY_MOVED_CAR_OUT_OF_THE_WAY)
	ELSE
		IF NOT IS_BIT_SET(serverBD.iBS, BS_SERVER_BD_IE_DELIVERY_STARTED_CAR_FADE_OUT)
			// These bits need to be set, otherwise server side cleanup wont proceed
			SET_BIT(serverBD.iBS, BS_SERVER_BD_IE_DELIVERY_STARTED_CAR_FADE_OUT)
			#IF IS_DEBUG_BUILD
				PRINTLN("[IE_DELIVERY][CUTSCENE] RUN_MAIN_SERVER_LOGIC - Veh to control doesnt exist!")
			#ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(serverBD.iBS, BS_SERVER_BD_IE_DELIVERY_MOVED_CAR_OUT_OF_THE_WAY)
			SET_BIT(serverBD.iBS, BS_SERVER_BD_IE_DELIVERY_MOVED_CAR_OUT_OF_THE_WAY)
		ENDIF
	ENDIF
	
	//Fade out the delivery vehicle if cargobob/towtruck
	IF DOES_ENTITY_EXIST(vehToDeliver)
	AND vehToControl != vehToDeliver
		PROCESS_VEHICLE_FADE(vehToDeliver, BS_SERVER_BD_IE_DELIVERY_STARTED_ATTACH_VEH_FADE_OUT, BS_SERVER_BD_IE_DELIVERY_MOVED_ATTACH_VEH_OUT_OF_THE_WAY)
	ELSE
		IF NOT IS_BIT_SET(serverBD.iBS, BS_SERVER_BD_IE_DELIVERY_STARTED_ATTACH_VEH_FADE_OUT)
			SET_BIT(serverBD.iBS, BS_SERVER_BD_IE_DELIVERY_STARTED_ATTACH_VEH_FADE_OUT)
			#IF IS_DEBUG_BUILD
				PRINTLN("[IE_DELIVERY][CUTSCENE] RUN_MAIN_SERVER_LOGIC - Veh to Deliver doesnt exist!")
			#ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(serverBD.iBS, BS_SERVER_BD_IE_DELIVERY_MOVED_ATTACH_VEH_OUT_OF_THE_WAY)
			SET_BIT(serverBD.iBS, BS_SERVER_BD_IE_DELIVERY_MOVED_ATTACH_VEH_OUT_OF_THE_WAY)
		ENDIF
	ENDIF
	
	IF IS_IE_DELIVERY_SCRIPT_STATE(IE_DELIVERY_SCRIPT_STATE_VEHICLE_CLEANUP)
		IF HAS_EVERYONE_FINISHED_DELIVERY_CUTSCENE()
		AND IS_BIT_SET(serverBD.iBS, BS_SERVER_BD_IE_DELIVERY_MOVED_CAR_OUT_OF_THE_WAY)
		AND IS_BIT_SET(serverBD.iBS, BS_SERVER_BD_IE_DELIVERY_MOVED_ATTACH_VEH_OUT_OF_THE_WAY)
			IE_DELIVERY_CONSUME_DELIVERED_VEHICLE()
		ELSE
			#IF IS_DEBUG_BUILD
				PRINTLN("[IE_DELIVERY][CUTSCENE] RUN_MAIN_SERVER_LOGIC - We are waiting for:")
				PRINTLN("[IE_DELIVERY][CUTSCENE] RUN_MAIN_SERVER_LOGIC - HAS_EVERYONE_FINISHED_DELIVERY_CUTSCENE: ", HAS_EVERYONE_FINISHED_DELIVERY_CUTSCENE())
				PRINTLN("[IE_DELIVERY][CUTSCENE] RUN_MAIN_SERVER_LOGIC - BS_SERVER_BD_IE_DELIVERY_MOVED_CAR_OUT_OF_THE_WAY: ", IS_BIT_SET(serverBD.iBS, BS_SERVER_BD_IE_DELIVERY_MOVED_CAR_OUT_OF_THE_WAY))
			#ENDIF
		ENDIF
		
		IE_DELIVERY_DESTRUCTION_RUN_SERVER_STATE()
	ENDIF
ENDPROC

*/


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                      Main Loop                      //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
SCRIPT  (IE_DELIVERY_SCRIPT_LAUNCH_DATA launchData) 
	
	
	g_IEDeliveryData.bLaunchingScript = FALSE
	
	INT i
	REPEAT IE_DELIVERY_SOUND_LOOPS_COUNT i
		cutsceneData.iSoundLoopsID[i] = -1
	ENDREPEAT
	
	IF NETWORK_IS_GAME_IN_PROGRESS() 
		SCRIPT_INITIALISE(launchData)	
	ELSE
		SCRIPT_CLEANUP(FALSE)
	ENDIF

	// Main loop
	WHILE TRUE

		// One wait to rule them all. This can be the ONLY wait from here on in.....
		MP_LOOP_WAIT_ZERO()
				
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			PRINTLN("[IE_DELIVERY][CUTSCENE] SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE = TRUE")
			SCRIPT_CLEANUP(FALSE)
		ENDIF
		
		IF IS_BIT_SET(scriptData.iBS, BS_IE_DELIVERY_THE_SCRIPT_IS_CLEANING_UP)
			_MAINTAIN_EXTRA_IPL()
			IF NOT IS_BIT_SET(scriptData.iBS, BS_IE_DELIVERY_IPLS_ARE_LOADED)
				SCRIPT_CLEANUP()
			ELSE
				#IF IS_DEBUG_BUILD
					PRINTLN("[IE_DELIVERY][CUTSCENE] This is bad but we gotta wairt for the IPLs to unload before this script is killed...")
				#ENDIF
				
				RELOOP
			ENDIF
		ELSE
			RUN_MAIN_CLIENT_LOGIC()
		ENDIF
		
		// -----------------------------------
		// Process server game logic		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			RUN_MAIN_SERVER_LOGIC()
		ENDIF
		
		#IF IS_DEBUG_BUILD
			UPDATE_DEBUG_WIDGETS()
		#ENDIF
		
	ENDWHILE
	

ENDSCRIPT
