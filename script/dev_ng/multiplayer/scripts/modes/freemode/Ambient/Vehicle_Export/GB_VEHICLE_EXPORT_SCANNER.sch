//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Written by:  Steve Tiley 																							//
// Date: 		28/09/2016																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//----------------------
//	INCLUDES
//----------------------


USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_script.sch"
USING "net_include.sch"
USING "script_ped.sch"


//----------------------
//	ENUMS
//----------------------

ENUM VEHICLE_SCANNER_BITSET0
	eVEHICLESCANNER_BITSET0_IS_TARGET_VEH = 0,
	eVEHICLESCANNER_BITSET0_HAS_BEEN_SCANNED,
	eVEHICLESCANNER_BITSET0_END	// Keep this at the end
ENDENUM

//----------------------
//	CONSTANTS
//----------------------


CONST_INT SCAN_TIME 6000
CONST_INT RESULTS_POSITIVE_TIME 4000
CONST_INT RESULTS_NEGATIVE_TIME 2000

CONST_INT SHAPE_TEST_NOT_FINISHED_YET 	0
CONST_INT SHAPE_TEST_CAN_SEE			1
CONST_INT SHAPE_TEST_CANNOT_SEE			2
CONST_INT SHAPE_TEST_CODE_NONEXISTENT	3

//----------------------
//	STRUCTS
//----------------------

STRUCT STRUCT_SHAPE_TEST_DATA
	VEHICLE_INDEX shapeTestVeh
	SHAPETEST_INDEX shapeTest
	SHAPETEST_STATUS eShapeTestStatus
ENDSTRUCT

//STRUCT SCANNER_AUDIO_STRUCT
//	INT iSFXBackgroundLoop = -1
//	INT iSFXScanLoop = -1
//	INT iSFXScanResult = -1
//ENDSTRUCT
////
//STRUCT VEHICLE_SCANNER_STRUCT
//	NETWORK_INDEX netID[MAX_SCANNER_VEHS]
//	INT iVehicleScannerBitSet[MAX_SCANNER_VEHS]
//	INT iCurrentScanVeh = -1
//	SCRIPT_TIMER ScanTimer
//	SCRIPT_TIMER ScanningTextTimer
//	SCRIPT_TIMER ResultsTimer
//	SCRIPT_TIMER LosBreakTimer
//	BOOL bShowingResults = FALSE
//	//STRUCT_SHAPE_TEST_DATA sShapeTestData
//	BOOL bStartedShapeTest = FALSE
//	BOOL bLoadedTextureDict = FALSE
//	
//	SCANNER_AUDIO_STRUCT sAudio
//ENDSTRUCT

//----------------------
//	ENUM FUNCTIONS
//----------------------

#IF IS_DEBUG_BUILD
FUNC STRING GET_VEHICLE_SCANNER_BIT_NAME(VEHICLE_SCANNER_BITSET0 eBit)
	SWITCH eBit
		CASE eVEHICLESCANNER_BITSET0_IS_TARGET_VEH		RETURN "eVEHICLESCANNER_BITSET0_IS_TARGET_VEH"
		CASE eVEHICLESCANNER_BITSET0_HAS_BEEN_SCANNED	RETURN "eVEHICLESCANNER_BITSET0_HAS_BEEN_SCANNED"
		CASE eVEHICLESCANNER_BITSET0_END				RETURN "eVEHICLESCANNER_BITSET0_END"
	ENDSWITCH
	
	RETURN "*INVALID*"
ENDFUNC
#ENDIF

FUNC BOOL HAS_VEHICLE_BEEN_SCANNED_WITH_VEHICLE_SCANNER(VEHICLE_SCANNER_STRUCT& sVehicleScannerStruct, INT iVeh)
	RETURN IS_BIT_SET(sVehicleScannerStruct.iVehicleScannerBitSet[iVeh], ENUM_TO_INT(eVEHICLESCANNER_BITSET0_HAS_BEEN_SCANNED))
ENDFUNC

PROC SET_VEHICLE_HAS_BEEN_SCANNED_WITH_VEHICLE_SCANNER(VEHICLE_SCANNER_STRUCT& sVehicleScannerStruct, INT iVeh)
	IF NOT IS_BIT_SET(sVehicleScannerStruct.iVehicleScannerBitSet[iVeh], ENUM_TO_INT(eVEHICLESCANNER_BITSET0_HAS_BEEN_SCANNED))
		PRINTLN("[VEHICLE_SCANNER] - SET_VEHICLE_HAS_BEEN_SCANNED_WITH_VEHICLE_SCANNER - Vehicle ", iVeh, " has been scanned, setting eVEHICLESCANNER_BITSET0_HAS_BEEN_SCANNED")
		SET_BIT(sVehicleScannerStruct.iVehicleScannerBitSet[iVeh], ENUM_TO_INT(eVEHICLESCANNER_BITSET0_HAS_BEEN_SCANNED))
	ENDIF
ENDPROC

FUNC BOOL IS_VEHICLE_A_TARGET_FOR_VEHICLE_SCANNER(VEHICLE_SCANNER_STRUCT& sVehicleScannerStruct, INT iVeh)
	RETURN IS_BIT_SET(sVehicleScannerStruct.iVehicleScannerBitSet[iVeh], ENUM_TO_INT(eVEHICLESCANNER_BITSET0_IS_TARGET_VEH))
ENDFUNC

PROC SET_VEHICLE_AS_TARGET_FOR_VEHICLE_SCANNER(VEHICLE_SCANNER_STRUCT& sVehicleScannerStruct, INT iVeh)
	IF NOT IS_BIT_SET(sVehicleScannerStruct.iVehicleScannerBitSet[iVeh], ENUM_TO_INT(eVEHICLESCANNER_BITSET0_IS_TARGET_VEH))
		PRINTLN("[VEHICLE_SCANNER] - SET_VEHICLE_AS_TARGET_FOR_VEHICLE_SCANNER - Vehicle ", iVeh, " is a target vehicle, setting eVEHICLESCANNER_BITSET0_IS_TARGET_VEH")
		SET_BIT(sVehicleScannerStruct.iVehicleScannerBitSet[iVeh], ENUM_TO_INT(eVEHICLESCANNER_BITSET0_IS_TARGET_VEH))
	ENDIF
ENDPROC

//----------------------------------------------------------------
//														 /////////
//	MAIN FUNCTIONS										/////////
//													   /////////
//-------------------------------------------------------------

FUNC STRING VEHICLE_SCANNER_SOUNDSET()
	RETURN "DLC_IE_Steal_EITS_Sounds"
ENDFUNC

// Audio
PROC START_SCAN_BACKGROUND_LOOP_AUDIO(SCANNER_AUDIO_STRUCT& sAudio)
	IF sAudio.iSFXBackgroundLoop = -1
		sAudio.iSFXBackgroundLoop = GET_SOUND_ID()
		PLAY_SOUND_FRONTEND(sAudio.iSFXBackgroundLoop, "Background_Loop", VEHICLE_SCANNER_SOUNDSET(), FALSE)
		PRINTLN("[VEHICLE_SCANNER] - START_SCAN_BACKGROUND_LOOP_AUDIO - Background loop audio started.")
	ENDIF
ENDPROC

PROC STOP_SCAN_BACKGROUND_LOOP_AUDIO(SCANNER_AUDIO_STRUCT& sAudio)
	IF sAudio.iSFXBackgroundLoop != -1
		STOP_SOUND(sAudio.iSFXBackgroundLoop)
		RELEASE_SOUND_ID(sAudio.iSFXBackgroundLoop)
		PRINTLN("[VEHICLE_SCANNER] - STOP_SCAN_BACKGROUND_LOOP_AUDIO - Background loop audio stopped.")
		sAudio.iSFXBackgroundLoop = -1
	ENDIF
ENDPROC

PROC TRIGGER_TARGET_FOUND_BLEEP()
	PLAY_SOUND_FRONTEND(-1, "Bleep", VEHICLE_SCANNER_SOUNDSET(), FALSE)
	PRINTLN("[VEHICLE_SCANNER] - TRIGGER_TARGET_FOUND_BLEEP - Bleep done.")
ENDPROC

PROC START_SCANNING_LOOP_AUDIO(SCANNER_AUDIO_STRUCT& sAudio)
	IF sAudio.iSFXScanLoop = -1
		sAudio.iSFXScanLoop = GET_SOUND_ID()
		PLAY_SOUND_FRONTEND(sAudio.iSFXScanLoop, "Scan_Ped_Loop", VEHICLE_SCANNER_SOUNDSET(), FALSE)
		PRINTLN("[VEHICLE_SCANNER] - START_SCANNING_LOOP_AUDIO - Scanning loop audio started.")
	ENDIF
ENDPROC

PROC STOP_SCANNING_LOOP_AUDIO(SCANNER_AUDIO_STRUCT& sAudio)
	IF sAudio.iSFXScanLoop != -1
		STOP_SOUND(sAudio.iSFXScanLoop)
		RELEASE_SOUND_ID(sAudio.iSFXScanLoop)
		PRINTLN("[VEHICLE_SCANNER] - STOP_SCANNING_LOOP_AUDIO - Scanning loop audio stopped.")
		sAudio.iSFXScanLoop = -1
	ENDIF
ENDPROC

PROC START_SCAN_RESULT_AUDIO(SCANNER_AUDIO_STRUCT& sAudio, BOOL bSuccess)
	IF sAudio.iSFXScanResult = -1
		sAudio.iSFXScanResult = GET_SOUND_ID()
		PLAY_SOUND_FRONTEND(sAudio.iSFXScanResult, PICK_STRING(bSuccess, "Scan_Success", "Scan_Fail"), VEHICLE_SCANNER_SOUNDSET(), FALSE)
		PRINTLN("[VEHICLE_SCANNER] - START_SCAN_RESULT_AUDIO - Scan result audio started.")
	ENDIF
ENDPROC

PROC STOP_SCAN_RESULT_AUDIO(SCANNER_AUDIO_STRUCT& sAudio)
	IF sAudio.iSFXScanResult != -1
		STOP_SOUND(sAudio.iSFXScanResult)
		RELEASE_SOUND_ID(sAudio.iSFXScanResult)
		PRINTLN("[VEHICLE_SCANNER] - STOP_SCAN_RESULT_AUDIO - Scan result audio stopped.")
		sAudio.iSFXScanResult = -1
	ELSE
		// The scan result hasnt played, which means we've been scanning and stopped. We need to trigger a stop scan
		PLAY_SOUND_FRONTEND(-1, "Lost_Target", VEHICLE_SCANNER_SOUNDSET(), FALSE)
		PRINTLN("[VEHICLE_SCANNER] - STOP_SCAN_RESULT_AUDIO - Lost target audio done.")
	ENDIF
ENDPROC

PROC STOP_ALL_SOUNDS(SCANNER_AUDIO_STRUCT& sAudioStruct)
	STOP_SCAN_BACKGROUND_LOOP_AUDIO(sAudioStruct)
	STOP_SCANNING_LOOP_AUDIO(sAudioStruct)
ENDPROC


/// PURPOSE:
///    Calculates a score for how far a screen coord is from the center of the screen by adding together the distances from the center on the x and y axes. 
/// PARAMS:
///    x - x component of screen coord to check.
///    y - y component of screen coord to check.
/// RETURNS:
///    FLOAT - score indicating how close to the center of the screen the coord passed as argument is. 
FUNC FLOAT SCANNER_CALCULATE_NEAR_MIDDLE_OF_SCREEN_SCORE(FLOAT x, FLOAT y, FLOAT &fXScore, FLOAT &fYScore)
	
	FLOAT fDistanceFromCentreX
	FLOAT fDistanceFromCentreY
	
	IF x <= 0.5
		fDistanceFromCentreX = 0.5 - x
	ELSE
		fDistanceFromCentreX = x - 0.5
	ENDIF

	IF y <= 0.5
		fDistanceFromCentreY = 0.5 - y
	ELSE
		fDistanceFromCentreY = y - 0.5
	ENDIF
	
	fXScore = fDistanceFromCentreX
	fYScore = fDistanceFromCentreY
	
	RETURN (fDistanceFromCentreX + fDistanceFromCentreY)

ENDFUNC

/// PURPOSE:
///    Gets of the middle of screen scores are good enough to allow a plate scan.
/// PARAMS:
///    fXScore - X-axis score.
///    fYScore - Y-axis score.
/// RETURNS:
///    TRUE if score good enough, FALSE if not.
FUNC BOOL ARE_MIDDLE_OF_SCREEN_SCORES_OK_TO_START_SCAN(FLOAT fXScore, FLOAT fYScore,FLOAT fScreenCentreRange = 0.2)
	
	IF (fXScore <= fScreenCentreRange)
	AND (fYScore <= fScreenCentreRange)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

//FUNC INT GET_SHAPE_TEST_RESULT_FOR_LINE_OF_SIGHT(VEHICLE_SCANNER_STRUCT& sVehicleScannerStruct)
//	INT iShapeTestResult
//	VECTOR vStartCoords, vEndCoords, vTemp0, vTemp1
//	ENTITY_INDEX hitEntity
//	VEHICLE_INDEX veh = NET_TO_VEH(sVehicleScannerStruct.netID[sVehicleScannerStruct.iCurrentScanVeh])
//	
//	IF (veh != sVehicleScannerStruct.sShapeTestData.shapeTestVeh
//	AND sVehicleScannerStruct.sShapeTestData.shapeTestVeh != NULL)
//	OR HAS_VEHICLE_BEEN_SCANNED_WITH_VEHICLE_SCANNER(sVehicleScannerStruct, sVehicleScannerStruct.iCurrentScanVeh)
//		
//		STRUCT_SHAPE_TEST_DATA sTemp
//		
//		sVehicleScannerStruct.bStartedShapeTest = FALSE
//		sVehicleScannerStruct.sShapeTestData = sTemp
//		
//		PRINTLN("[vehicle scanner spam] - shapetest reset")
//		
//		RETURN SHAPE_TEST_CANNOT_SEE
//		
//	ELSE
//		
//		IF sVehicleScannerStruct.bStartedShapeTest = FALSE
//		
//			vStartCoords = GET_FINAL_RENDERED_CAM_COORD()
//			vEndCoords = GET_ENTITY_COORDS(veh)
//			
//			sVehicleScannerStruct.sShapeTestData.shapeTestVeh = veh
//			sVehicleScannerStruct.sShapeTestData.shapeTest = START_SHAPE_TEST_LOS_PROBE(vStartCoords, vEndCoords, SCRIPT_INCLUDE_MOVER, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
//			
//			sVehicleScannerStruct.bStartedShapeTest = TRUE
//			
//			PRINTLN("[vehicle scanner spam] - shapetest started")
//		ELSE
//			
//			sVehicleScannerStruct.sShapeTestData.eShapeTestStatus = GET_SHAPE_TEST_RESULT(sVehicleScannerStruct.sShapeTestData.shapeTest, iShapeTestResult, vTemp0, vTemp1, hitEntity)
//			
//			IF sVehicleScannerStruct.sShapeTestData.eShapeTestStatus = SHAPETEST_STATUS_RESULTS_READY
//				
//				PRINTLN("[vehicle scanner spam] - got shapetest result")
//				
//				STRUCT_SHAPE_TEST_DATA sTemp
//				
//				sVehicleScannerStruct.sShapeTestData = sTemp
//				sVehicleScannerStruct.bStartedShapeTest = FALSE
//				
//				IF iShapeTestResult > 0
//					PRINTLN("[vehicle scanner spam] - shapetest cant see")
//					RETURN SHAPE_TEST_CANNOT_SEE
//				ENDIF
//				
//				PRINTLN("[vehicle scanner spam] - shapetest can see")
//				RETURN SHAPE_TEST_CAN_SEE
//				
//			ELIF sVehicleScannerStruct.sShapeTestData.eShapeTestStatus = SHAPETEST_STATUS_NONEXISTENT
//				
//				sVehicleScannerStruct.bStartedShapeTest = FALSE
//				PRINTLN("[vehicle scanner spam] - shapetest non existent")
//				RETURN SHAPE_TEST_CODE_NONEXISTENT
//				
//			ENDIF
//			
//		ENDIF
//		
//	ENDIF
//			
//	PRINTLN("[vehicle scanner spam] - shapetest not finished yet")
//	RETURN SHAPE_TEST_NOT_FINISHED_YET
//ENDFUNC

FUNC BOOL VEHICLE_SCAN_CHECKS(VEHICLE_SCANNER_STRUCT& sVehicleScannerStruct, INT iVehicleToScan, INT iScannerRange = 250, FLOAT iScreenCentreRange = 0.2)//, BOOL bDoLosCheck = FALSE)
	
	FLOAT fVehX, fVehY, fVehXScore, fVehYScore
	VEHICLE_INDEX vehicle
	
	IF NOT DOES_ENTITY_EXIST(sVehicleScannerStruct.vehID[iVehicleToScan])
		PRINTLN("[vehicle scanner spam] - doesnt exist")
		RETURN FALSE
	ENDIF
	
	vehicle = sVehicleScannerStruct.vehID[iVehicleToScan]
	
	IF IS_ENTITY_DEAD(vehicle)
	OR NOT IS_VEHICLE_DRIVEABLE(vehicle)
		PRINTLN("[vehicle scanner spam] - dead or not driveable")
		RETURN FALSE
	ENDIF
	
	// Always display if showing the results of the most recent scan if the vehicle is still alive
	IF NOT sVehicleScannerStruct.bShowingResults
		IF HAS_VEHICLE_BEEN_SCANNED_WITH_VEHICLE_SCANNER(sVehicleScannerStruct, iVehicleToScan)
			PRINTLN("[vehicle scanner spam] - has been scanned already")
			RETURN FALSE
		ENDIF
		
		IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), vehicle) > iScannerRange
			PRINTLN("[vehicle scanner spam] - too far away")
			RETURN FALSE
		ENDIF
		
		IF NOT GET_SCREEN_COORD_FROM_WORLD_COORD(GET_ENTITY_COORDS(vehicle), fVehX, fVehY)
			PRINTLN("[vehicle scanner spam] - cant get screen coord from world coord")
			RETURN FALSE
		ENDIF
						
		SCANNER_CALCULATE_NEAR_MIDDLE_OF_SCREEN_SCORE(fVehX, fVehY, fVehXScore, fVehYScore)
		
		IF NOT ARE_MIDDLE_OF_SCREEN_SCORES_OK_TO_START_SCAN(fVehXScore, fVehYScore,iScreenCentreRange)
			PRINTLN("[vehicle scanner spam] - scores not ok to start scan")
			RETURN FALSE
		ENDIF
		
//		IF bDoLosCheck
//			IF GET_SHAPE_TEST_RESULT_FOR_LINE_OF_SIGHT(sVehicleScannerStruct) != SHAPE_TEST_CAN_SEE
//				PRINTLN("[vehicle scanner spam] - vehicle not in line of sight")
//				RETURN FALSE
//			ENDIF

			IF NOT HAS_ENTITY_CLEAR_LOS_TO_ENTITY(PLAYER_PED_ID(), vehicle, SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_VEHICLE|SCRIPT_INCLUDE_RAGDOLL|SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_FOLIAGE)
				IF sVehicleScannerStruct.iCurrentScanVeh != -1
					IF NOT HAS_NET_TIMER_STARTED(sVehicleScannerStruct.LosBreakTimer)
						START_NET_TIMER(sVehicleScannerStruct.LosBreakTimer, TRUE)
					ELSE
						IF HAS_NET_TIMER_EXPIRED(sVehicleScannerStruct.LosBreakTimer, 1500, TRUE)
							PRINTLN("[vehicle scanner spam] - vehicle not in line of sight")
							RETURN FALSE
						ENDIF
					ENDIF
				ELSE
					PRINTLN("[vehicle scanner spam] - vehicle not in line of sight")
					RETURN FALSE
				ENDIF
			ELSE
				IF HAS_NET_TIMER_STARTED(sVehicleScannerStruct.LosBreakTimer)
					RESET_NET_TIMER(sVehicleScannerStruct.LosBreakTimer)
				ENDIF
			ENDIF
//		ENDIF
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

/// PURPOSE:
///    Runs the scan timer while scanning a scan target.
/// PARAMS:
///    sScannerData - struct of scanner data.
/// RETURNS:
///    TRUE if scan is complete, FALSE if not.
FUNC BOOL RUN_SCAN_TIMER(VEHICLE_SCANNER_STRUCT& sVehicleScannerStruct)

	IF NOT HAS_NET_TIMER_STARTED(sVehicleScannerStruct.ScanTimer)
		START_NET_TIMER(sVehicleScannerStruct.ScanTimer)
	ELSE
		IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sVehicleScannerStruct.ScanTimer) >= SCAN_TIME
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL RUN_RESULTS_TIMER(VEHICLE_SCANNER_STRUCT& sVehicleScannerStruct, BOOL bSuccess)
	IF NOT HAS_NET_TIMER_STARTED(sVehicleScannerStruct.ResultsTimer)
		START_NET_TIMER(sVehicleScannerStruct.ResultsTimer)
	ELSE
		IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sVehicleScannerStruct.ResultsTimer) >= PICK_INT(bSuccess, RESULTS_POSITIVE_TIME, RESULTS_NEGATIVE_TIME)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Gets the size the box that surrounds the current scan target should be. Looks at size of target and distance from player.
/// PARAMS:
///    veh - vehicle box is being drawn around.
/// RETURNS:
///    Box size.
FUNC FLOAT GET_BOX_SIZE_FOR_VEH_SCANNER(VEHICLE_INDEX veh)
	
	FLOAT fBoxScaler
	VECTOR vVehCoord = GET_ENTITY_COORDS(veh)
	VECTOR vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
	FLOAT fVehRange = GET_DISTANCE_BETWEEN_COORDS(vVehCoord, vPlayerCoords)
	
	fBoxScaler = 1000 / (GET_GAMEPLAY_CAM_FOV() * fVehRange)
	
	IF fBoxScaler < 3.0
		fBoxScaler = 3.0
	ENDIF
	
	IF fBoxScaler > 3.2
		fBoxScaler = 3.2
	ENDIF

	RETURN fBoxScaler
	
ENDFUNC	

/// PURPOSE:
///    Draws scan lines that move through scan box to indicate scan is in progress.
/// PARAMS:
///    sScannerData - struct of scanner data.
///    fBoxDrawSize - box size.
///    fTargetOnScreenX - target x coords.
///    fTargetOnScreenY - target y coords.
///    r - scan line red value.
///    g - scan line green value.
///    b - scan line blue value.
PROC DRAW_SCANNING_LINE_THAT_MOVES_THROUGH_BOX(INT iScanTime, FLOAT fBoxDrawSize, FLOAT fTargetOnScreenX, FLOAT fTargetOnScreenY, INT r, INT g, INT b)
	INT i
	IF iScanTime <= SCAN_TIME
	
		FOR i = 0 TO 9
		
			FLOAT fY1 = (TO_FLOAT(iScanTime)/TO_FLOAT(SCAN_TIME)) - (TO_FLOAT(i)*0.04)
			
			IF fy1>0
			
				FLOAT fy2 = (TO_FLOAT(iScanTime)/TO_FLOAT(SCAN_TIME)) - ((i+1)*0.04)
				IF fy2<0
					fy2 = 0
				ENDIF
				
				INT alphaOut = 200 - ((200/10)*i)

				DRAW_RECT_FROM_CORNER(fTargetOnScreenX-((fBoxDrawSize*0.65)), 
									fTargetOnScreenY+((fBoxDrawSize*0.9)/2)-((fBoxDrawSize*0.9)*fy1),
									(fBoxDrawSize*0.85)*1.5,
									(fy1-fy2)*((fBoxDrawSize*0.9)), 
									r, 
									g,
									b, 
									alphaOut)
															
			ENDIF

		ENDFOR
			
	ENDIF

ENDPROC

PROC SCANNER_SET_TEXT_FOR_HUD(int r,int g,int b, float fScale, bool restrictScale=false)

	FLOAT fFontScale = 0.3
	
	IF restrictScale = TRUE
		IF fScale < 0.6
			fScale = 0.6
		ENDIF
		IF fScale > 1.0	
			fScale = 1.0
		ENDIF
	ENDIF
	
	SET_TEXT_SCALE(fFontScale*fScale,fFontScale*fScale)
	SET_TEXT_COLOUR(r,g,b,200)
	SET_TEXT_DROPSHADOW(0,0,0,0,0)
	SET_TEXT_WRAP(-2.0, 2.0)
	SET_TEXT_FONT(FONT_STANDARD)
	
ENDPROC

PROC DRAW_BOX_TEXT(TEXT_LABEL_15 tl15Text, FLOAT fXPos, FLOAT fYPos, int iR, int iG, int iB, float fScale, BOOL bRestrictScale = TRUE)
	SCANNER_SET_TEXT_FOR_HUD(iR,iG,iB,fScale,bRestrictScale)
	DISPLAY_TEXT(fXPos,fYPos,tl15Text)
ENDPROC

/// PURPOSE:
///    Draws a box around the current scan target.
/// PARAMS:
///    sScannerData - struct of scanner data.
PROC DRAW_SCAN_BOX_AROUND_TARGET(VEHICLE_SCANNER_STRUCT& sVehicleScannerStruct, BOOL bDoScanLines = TRUE, HUD_COLOURS eColour = HUD_COLOUR_WHITE)

	FLOAT  fBoxScaler, fBoxDrawSize, fPlayerDisFromMin, fPlayerDisFromMax
	VECTOR vVehModelMin, vVehModelMax, vMinCoords, vMaxCoords, vNearestEndCoords, vPlayerCoords
	INT r, g, b, a, iTextTimer
	TEXT_LABEL_15 tl15_boxText
	VEHICLE_INDEX veh = sVehicleScannerStruct.vehID[sVehicleScannerStruct.iCurrentScanVeh]
	
	IF bDoScanLines
		tl15_boxText = "VEX_SCAN"
	ELSE
		IF IS_VEHICLE_A_TARGET_FOR_VEHICLE_SCANNER(sVehicleScannerStruct, sVehicleScannerStruct.iCurrentScanVeh)
			tl15_boxText = "VEX_TARACQ"
		ELSE
			tl15_boxText = "VEX_TARINV"
		ENDIF
	ENDIF
	
	// So box doesn't overlay hud elements (i.e. radar).
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
	
	// Store local player coords.
	vPlayerCoords = GET_PLAYER_COORDS(PLAYER_ID())

	// Get model dimensions.
	GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(veh), vVehModelMin, vVehModelMax)
	
	// Get offsets from vehicle centre to get front and back of vehicle.
	vMinCoords = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(veh, << 0.0, vVehModelMin.Y, 0.0 >>)
	vMaxCoords = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(veh, << 0.0, vVehModelMax.Y, 0.0 >>)

	// Store distance of front and back from player.
	fPlayerDisFromMin = GET_DISTANCE_BETWEEN_COORDS(vPlayerCoords, vMinCoords)
	fPlayerDisFromMax = GET_DISTANCE_BETWEEN_COORDS(vPlayerCoords, vMaxCoords)
	
	// Get if front or back is most facing the player.
	IF fPlayerDisFromMin < fPlayerDisFromMax // Doing greater than seems to return correct closest plate. Perhaps get model dimension is returning the max and min the wrong way around?
		vNearestEndCoords = vMinCoords
	ELSE
		vNearestEndCoords = vMaxCoords
	ENDIF

	// Adjust down a bit for liscense plate.
	vNearestEndCoords.z = (vNearestEndCoords.z - 0.15)

	// So box doesn't lag.
	SET_DRAW_ORIGIN(vNearestEndCoords)
	
	// Get box data.
	fBoxScaler = GET_BOX_SIZE_FOR_VEH_SCANNER(veh)	
	fBoxDrawSize = 0.024 * fBoxScaler
	GET_HUD_COLOUR(eColour, r, g, b, a)
	
	// Draw box.
	DRAW_SPRITE("helicopterhud", "hud_outline", 0.0, 0.0, fBoxDrawSize*1.5, fBoxDrawSize, 0, r, g, b, a)

	// If we want scan lines, draw them.
	// If doing scan lines, set box test to "SCANNING".
	IF bDoScanLines
		DRAW_SCANNING_LINE_THAT_MOVES_THROUGH_BOX(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sVehicleScannerStruct.ScanTimer), fBoxDrawSize, 0.0, 0.0, r, g, b)
		IF NOT HAS_NET_TIMER_STARTED(sVehicleScannerStruct.ScanningTextTimer)
			START_NET_TIMER(sVehicleScannerStruct.ScanningTextTimer)
		ELSE
			iTextTimer = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sVehicleScannerStruct.ScanningTextTimer)
		ENDIF
	ENDIF
	
	// Draw text underneath box. Make flash if doing scanning.
	IF bDoScanLines
		IF sVehicleScannerStruct.bDrawScanText
			IF ( iTextTimer <= 500 )
			OR ( (iTextTimer >= 1000) AND (iTextTimer <= 1500) )
			OR ( (iTextTimer >= 2000) AND (iTextTimer <= 2500) )
			OR ( (iTextTimer >= 3000) AND (iTextTimer <= 3500) )
			OR ( (iTextTimer >= 4000) AND (iTextTimer <= 4500) )
			OR ( (iTextTimer >= 5000) AND (iTextTimer <= 5500) )
			OR ( (iTextTimer >= 6000) AND (iTextTimer <= 6500) )
			OR ( (iTextTimer >= 7000) AND (iTextTimer <= 7500) )
			OR ( (iTextTimer >= 8000) AND (iTextTimer <= 8500) )
				DRAW_BOX_TEXT(tl15_boxText, ((-0.7)*(fBoxDrawSize)+(fBoxDrawSize*0.04)),((fBoxDrawSize/2)+0.005),r, g, b,fBoxScaler+0.2)
			ENDIF
		ENDIF
	ELSE
		IF IS_VEHICLE_A_TARGET_FOR_VEHICLE_SCANNER(sVehicleScannerStruct, sVehicleScannerStruct.iCurrentScanVeh)
			tl15_boxText = "VEX_TARACQ"
		ELSE
			tl15_boxText = "VEX_TARINV"
		ENDIF
		DRAW_BOX_TEXT(tl15_boxText, ((-0.7)*(fBoxDrawSize)+(fBoxDrawSize*0.04)),((fBoxDrawSize/2)+0.005),r, g, b,fBoxScaler+0.2)
	ENDIF
	
	CLEAR_DRAW_ORIGIN()
	
ENDPROC

/// PURPOSE:
///    Scans the current scan target. Handles scan timer and scan UI.
/// PARAMS:
///    sScannerData - struct of scanner data.
/// RETURNS:
///    True if scan has completed, False if not.
FUNC BOOL SCAN_TARGET(VEHICLE_SCANNER_STRUCT& sVehicleScannerStruct)
	
	IF RUN_SCAN_TIMER(sVehicleScannerStruct)
		IF sVehicleScannerStruct.bShowingResults = FALSE
			sVehicleScannerStruct.bShowingResults = TRUE
		ENDIF
		RETURN TRUE
	ELSE
		DRAW_SCAN_BOX_AROUND_TARGET(sVehicleScannerStruct)
		START_SCANNING_LOOP_AUDIO(sVehicleScannerStruct.sAudio)
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC HUD_COLOURS GET_HUD_COLOUR_FROM_SCAN_RESULT(VEHICLE_SCANNER_STRUCT& sVehicleScannerStruct)
	IF IS_VEHICLE_A_TARGET_FOR_VEHICLE_SCANNER(sVehicleScannerStruct, sVehicleScannerStruct.iCurrentScanVeh)
		RETURN HUD_COLOUR_GREEN
	ENDIF
	
	RETURN HUD_COLOUR_RED
ENDFUNC

FUNC BOOL SHOW_RESULTS(VEHICLE_SCANNER_STRUCT& sVehicleScannerStruct)
	
	DRAW_SCAN_BOX_AROUND_TARGET(sVehicleScannerStruct, FALSE, GET_HUD_COLOUR_FROM_SCAN_RESULT(sVehicleScannerStruct))
	STOP_SCANNING_LOOP_AUDIO(sVehicleScannerStruct.sAudio)
	START_SCAN_RESULT_AUDIO(sVehicleScannerStruct.sAudio, IS_VEHICLE_A_TARGET_FOR_VEHICLE_SCANNER(sVehicleScannerStruct, sVehicleScannerStruct.iCurrentScanVeh))

	IF RUN_RESULTS_TIMER(sVehicleScannerStruct, IS_VEHICLE_A_TARGET_FOR_VEHICLE_SCANNER(sVehicleScannerStruct, sVehicleScannerStruct.iCurrentScanVeh))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC RESET_SCAN_DATA(VEHICLE_SCANNER_STRUCT& sVehicleScannerStruct)
	IF HAS_NET_TIMER_STARTED(sVehicleScannerStruct.ScanTimer)
		RESET_NET_TIMER(sVehicleScannerStruct.ScanTimer)
	ENDIF
	IF HAS_NET_TIMER_STARTED(sVehicleScannerStruct.ScanningTextTimer)
		RESET_NET_TIMER(sVehicleScannerStruct.ScanningTextTimer)
	ENDIF
	IF HAS_NET_TIMER_STARTED(sVehicleScannerStruct.ResultsTimer)
		RESET_NET_TIMER(sVehicleScannerStruct.ResultsTimer)
	ENDIF
ENDPROC

FUNC BOOL IS_SAFE_FOR_VEHICLE_SCANNER(VEHICLE_SCANNER_STRUCT& sVehicleScannerStruct)
	
	// Add some other checks here
	IF NOT g_heligunCamActive
		RETURN FALSE
	ENDIF
	
	IF IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
		IF GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())) != POLMAV
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF NOT sVehicleScannerStruct.bLoadedTextureDict
		REQUEST_STREAMED_TEXTURE_DICT("helicopterhud")
		IF HAS_STREAMED_TEXTURE_DICT_LOADED("helicopterhud")
			PRINTLN("[VEHICLE_SCANNER] - IS_SAFE_FOR_VEHICLE_SCANNER - helicopted hud has been loaded, now safe")
			sVehicleScannerStruct.bLoadedTextureDict = TRUE
		ENDIF
	ENDIF
	
	RETURN sVehicleScannerStruct.bLoadedTextureDict
ENDFUNC

PROC CLEANUP_VEHICLE_SCANNER(VEHICLE_SCANNER_STRUCT& sVehicleScannerStruct)
	PRINTLN("[VEHICLE_SCANNER] - CLEANUP_VEHICLE_SCANNER - Called")
	IF sVehicleScannerStruct.bLoadedTextureDict
		SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("helicopterhud")
		PRINTLN("[VEHICLE_SCANNER] - CLEANUP_VEHICLE_SCANNER - helicopterhud set as no longer needed")
		sVehicleScannerStruct.bLoadedTextureDict = FALSE
	ENDIF
	
	STOP_ALL_SOUNDS(sVehicleScannerStruct.sAudio)
ENDPROC

PROC MAINTAIN_SCAN()
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_GUN_LR)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_GUN_UD)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PASSENGER_AIM)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PASSENGER_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON) 
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_PREV_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
	
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WANTED_STARS)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_MP_CASH)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH_CHANGE)
ENDPROC	

// bIgnoreWatchers exists only to prevent changing the original vehicle export mission
// Going forward we won't want to process it for people just watching on the camera
PROC MAINTAIN_VEHICLE_SCANNER(VEHICLE_SCANNER_STRUCT& sVehicleScannerStruct, BOOL bIgnoreWatchers = TRUE)

	// Are we using the heli cam?
	IF IS_SAFE_FOR_VEHICLE_SCANNER(sVehicleScannerStruct)
		MAINTAIN_SCAN()
		START_SCAN_BACKGROUND_LOOP_AUDIO(sVehicleScannerStruct.sAudio)
		// If we're not currently scanning or looking to scan a vehicle, look for one to scan, and let us know which one is being scanned
		IF sVehicleScannerStruct.iCurrentScanVeh = -1
			INT iVehLoop
			REPEAT MAX_SCANNER_VEHS iVehLoop
				IF VEHICLE_SCAN_CHECKS(sVehicleScannerStruct, iVehLoop)
					TRIGGER_TARGET_FOUND_BLEEP()
					// We have a scan vehicle
					sVehicleScannerStruct.iCurrentScanVeh = iVehLoop
					PRINTLN("[VEHICLE_SCANNER] - MAINTAIN_VEHICLE_SCANNER - New Current scan veh! sVehicleScannerStruct.iCurrentScanVeh = ", iVehLoop)
				ENDIF
			ENDREPEAT
		ELSE
			IF VEHICLE_SCAN_CHECKS(sVehicleScannerStruct, sVehicleScannerStruct.iCurrentScanVeh)
				// We have a scan vehicle, lets to some scanning stuff
				IF SCAN_TARGET(sVehicleScannerStruct)

					IF NOT bIgnoreWatchers
					OR NOT g_heligunWatchingOnly
					
						// Set that this vehicle has been scanned, so we now get thrown back to the outside loop after the results have been shown
						IF NOT HAS_VEHICLE_BEEN_SCANNED_WITH_VEHICLE_SCANNER(sVehicleScannerStruct, sVehicleScannerStruct.iCurrentScanVeh)
							SET_VEHICLE_HAS_BEEN_SCANNED_WITH_VEHICLE_SCANNER(sVehicleScannerStruct, sVehicleScannerStruct.iCurrentScanVeh)
						ENDIF
						
					ELSE
						PRINTLN("[VEHICLE_SCANNER] - MAINTAIN_VEHICLE_SCANNER - Not setting vehicle as scanned as player is only watching. bIgnoreWatchers:", GET_STRING_FROM_BOOL(bIgnoreWatchers), " - g_heligunWatchingOnly: ", GET_STRING_FROM_BOOL(g_heligunWatchingOnly))
					ENDIF
				
					// Target has been scanned, time to show the results of the scan to the player
					IF SHOW_RESULTS(sVehicleScannerStruct)
						sVehicleScannerStruct.bShowingResults = FALSE
					ENDIF
				ENDIF
			ELSE
				// We're no longer scanning this vehicle, lets to back to looking for another to scan
				RESET_SCAN_DATA(sVehicleScannerStruct)
				STOP_SCAN_RESULT_AUDIO(sVehicleScannerStruct.sAudio)
				STOP_SCANNING_LOOP_AUDIO(sVehicleScannerStruct.sAudio)
				sVehicleScannerStruct.iCurrentScanVeh = -1
				PRINTLN("[VEHICLE_SCANNER] - MAINTAIN_VEHICLE_SCANNER - No longer looking at vehicle to scan! sVehicleScannerStruct.iCurrentScanVeh = -1")
			ENDIF
		ENDIF
	ELSE
		STOP_ALL_SOUNDS(sVehicleScannerStruct.sAudio)
		RESET_SCAN_DATA(sVehicleScannerStruct)
		IF sVehicleScannerStruct.iCurrentScanVeh = -1
			sVehicleScannerStruct.iCurrentScanVeh = -1
		ENDIF
	ENDIF
ENDPROC


