//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Written by:  Martin McMillan																							//
// Date: 		20/06/2016																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//----------------------
//	INCLUDES
//----------------------

USING "globals.sch"
USING "commands_clock.sch"
USING "commands_stats.sch"
USING "script_network.sch"
USING "net_gang_boss.sch"
USING "net_gang_boss_common.sch"
USING "net_ie_dropoff_public.sch"


//FUNC INT GET_REMOTE_EXPORT_BLIP_RANGE_FUNCTIONALITY()
//
//	RETURN 2
//
//ENDFUNC
//
//PROC STORE_EXPORT_PACKAGE_COORDS(ENTITY_INDEX entID, SCRIPT_TIMER &timer, BOOL bRemove, INT iExport)
//
//	IF PLAYER_ID() = GB_GET_LOCAL_PLAYER_MISSION_HOST()
//		IF DOES_ENTITY_EXIST(entID)
//		AND NOT bRemove
//		AND NOT IS_PLAYER_GANG_OFF_THE_RADAR()
//			IF HAS_NET_TIMER_EXPIRED(timer,1000)
//				SET_EXPORT_BLIP_COORDS(GET_ENTITY_COORDS(entID),iExport)
//				RESET_NET_TIMER(timer)
//			ENDIF
//		ELSE
//			CLEANUP_EXPORT_BLIP_COORDS(iExport)
//		ENDIF
//	ENDIF
//
//ENDPROC
//
//PROC STORE_EXPORT_VEHICLE_COORDS(VEHICLE_INDEX vehID, SCRIPT_TIMER &timer, INT iExport, BOOL bDelivered = FALSE)
//
//	IF PLAYER_ID() = GB_GET_LOCAL_PLAYER_MISSION_HOST()
//		IF DOES_ENTITY_EXIST(vehID)
//		AND IS_VEHICLE_DRIVEABLE(vehID)
//		AND NOT IS_PLAYER_GANG_OFF_THE_RADAR()
//		AND NOT bDelivered
//			IF HAS_NET_TIMER_EXPIRED(timer,1000)
//				SET_EXPORT_BLIP_COORDS(GET_ENTITY_COORDS(vehID),iExport)
//				RESET_NET_TIMER(timer)
//			ENDIF
//		ELSE
//			CLEANUP_EXPORT_BLIP_COORDS(iExport)
//		ENDIF
//	ENDIF
//
//ENDPROC

PROC VEHICLE_EXPORT_CHECK_THE_AVAILABLE_DOORS(VEHICLE_INDEX vehicle, INT &iDoorBitset)
			
	IF GET_ENTITY_BONE_INDEX_BY_NAME(vehicle, "door_dside_f") <> -1		
		SET_BIT(iDoorBitset, enum_to_int(SC_DOOR_FRONT_LEFT))
	ELSE
		CLEAR_BIT(iDoorBitset, enum_to_int(SC_DOOR_FRONT_LEFT))
	ENDIF
	
	IF GET_ENTITY_BONE_INDEX_BY_NAME(vehicle, "door_pside_f") <> -1		
		SET_BIT(iDoorBitset, enum_to_int(SC_DOOR_FRONT_RIGHT))
	ELSE
		CLEAR_BIT(iDoorBitset, enum_to_int(SC_DOOR_FRONT_RIGHT))
	ENDIF
			
	IF GET_ENTITY_BONE_INDEX_BY_NAME(vehicle, "door_dside_r") <> -1		
		SET_BIT(iDoorBitset, enum_to_int(SC_DOOR_REAR_LEFT))
	ELSE
		CLEAR_BIT(iDoorBitset, enum_to_int(SC_DOOR_REAR_LEFT))
	ENDIF
	
	IF GET_ENTITY_BONE_INDEX_BY_NAME(vehicle, "door_pside_r") <> -1		
		SET_BIT(iDoorBitset, enum_to_int(SC_DOOR_REAR_RIGHT))
	ELSE
		CLEAR_BIT(iDoorBitset, enum_to_int(SC_DOOR_REAR_RIGHT))
	ENDIF
			
	IF GET_ENTITY_BONE_INDEX_BY_NAME(vehicle, "bonnet") <> -1		
		SET_BIT(iDoorBitset, enum_to_int(SC_DOOR_BONNET))
	ELSE
		CLEAR_BIT(iDoorBitset, enum_to_int(SC_DOOR_BONNET))
	ENDIF
	
	IF GET_ENTITY_BONE_INDEX_BY_NAME(vehicle, "boot") <> -1		
		SET_BIT(iDoorBitset, enum_to_int(SC_DOOR_BOOT))
	ELSE
		CLEAR_BIT(iDoorBitset, enum_to_int(SC_DOOR_BOOT))
	ENDIF
	
	IF GET_ENTITY_MODEL(vehicle) = MULE
	OR GET_ENTITY_MODEL(vehicle) = BENSON
	OR GET_ENTITY_MODEL(vehicle) = SPEEDO
	OR GET_ENTITY_MODEL(vehicle) = gburrito
	OR GET_ENTITY_MODEL(vehicle) = GRANGER
	OR GET_ENTITY_MODEL(vehicle) = BARRACKS
	OR GET_ENTITY_MODEL(vehicle) = BARRACKS3
	OR GET_ENTITY_MODEL(vehicle) = GBURRITO2
		SET_BIT(iDoorBitset, ciFMMC_VEHICLE_EXTRA1)
	ELSE
		CLEAR_BIT(iDoorBitset, ciFMMC_VEHICLE_EXTRA1)
	ENDIF
	
	IF GET_ENTITY_MODEL(vehicle) = SUBMERSIBLE
		iDoorBitset = 0
	ENDIF

ENDPROC

FUNC BOOL GB_SHOULD_PROCESS_VEHICLE_VALUE_UI(PLAYER_INDEX playerID)
	
	// Mission checks
	IF GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(playerID) = FMMC_TYPE_VEHICLE_EXPORT_SELL
	OR GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(playerID) = FMMC_TYPE_VEHICLE_EXPORT_BUY
	OR (NOT GB_IS_PLAYER_PERMANENT_PARTICIPANT(playerID)
	AND NOT FM_EVENT_IS_PLAYER_PERMANENT_PARTICIPANT_IN_FM_EVENT(playerID)
	AND NOT IS_PLAYER_ON_ANY_FM_MISSION(playerID))
		// UI Checks
		IF NOT NETWORK_IS_IN_MP_CUTSCENE()
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC VEHICLE_EXPORT_MISSION_DIFFICULTY GET_MISSION_DIFFICULTY_FROM_VEHICLE_MODEL(MODEL_NAMES modelToCheck)
	SWITCH modelToCheck
			
		//===12 HARD===
		CASE Prototipo
		CASE tyrus
		CASE T20
		CASE sheava
		CASE OSIRIS
		CASE fmj
		CASE Reaper
		CASE pfister811
		CASE MAMBA
		CASE Btype3
		CASE FELTZER3
		CASE Ztype
			RETURN eMISSION_DIFFICULTY_HARD

		//===10 MEDIUM===
		CASE tropos
		CASE Entityxf
		CASE Sultanrs
		CASE Zentorno
		CASE omnis
		CASE COQUETTE3
		CASE seven70
		CASE VERLIERER2
		CASE COQUETTE2
		CASE Cheetah
			RETURN eMISSION_DIFFICULTY_MEDIUM

		//===10 EASY===
		CASE BestiaGTS
		CASE NIGHTSHADE
		CASE Banshee2
		CASE Sabregt2
		CASE Turismor
		CASE TAMPA
		CASE Massacro
		CASE Jester
		CASE Alpha
		CASE Feltzer2
			RETURN eMISSION_DIFFICULTY_EASY
			
	ENDSWITCH
	
	PRINTLN("[VEH_EXPORT] GET_MISSION_DIFFICULTY_FROM_VEHICLE_MODEL - Invalid model passed. Defaulting to eMISSION_DIFFICULTY_EASY.")
	RETURN eMISSION_DIFFICULTY_EASY
ENDFUNC

FUNC INT GET_SHOWROOM_MOD_COST(INT iPrivateBuyerOffer)
	RETURN CEIL(iPrivateBuyerOffer * g_sMPTunables.fIMPEXP_SELL_BUYER2_MOD_COST_MODIFIER)
ENDFUNC

FUNC INT GET_SPECIALIST_BUYER_MOD_COST(INT iPrivateBuyerOffer)
	RETURN CEIL(iPrivateBuyerOffer * g_sMPTunables.fIMPEXP_SELL_BUYER3_MOD_COST_MODIFIER)
ENDFUNC

FUNC INT GET_PRIVATE_BUYER_VALUE_FROM_MISSION_DIFFICULTY(VEHICLE_EXPORT_MISSION_DIFFICULTY eDifficulty)
	SWITCH eDifficulty
		CASE eMISSION_DIFFICULTY_HARD	RETURN g_sMPTunables.iIMPEXP_SELL_BUYER1_OFFER_HARD 
		CASE eMISSION_DIFFICULTY_MEDIUM	RETURN g_sMPTunables.iIMPEXP_SELL_BUYER1_OFFER_MED
		CASE eMISSION_DIFFICULTY_EASY	RETURN g_sMPTunables.iIMPEXP_SELL_BUYER1_OFFER_EASY
	ENDSWITCH
	
	PRINTLN("[VEH_EXPORT] - GET_PRIVATE_BUYER_VALUE_FROM_MISSION_DIFFICULTY - Unknown difficult passed, returning 0")
	RETURN 0
ENDFUNC

FUNC INT GET_SHOWROOM_VALUE_FROM_MISSION_DIFFICULTY(VEHICLE_EXPORT_MISSION_DIFFICULTY eDifficulty, BOOL bSellingVehSet = FALSE)
	INT iValueBeforeMod = CEIL(GET_PRIVATE_BUYER_VALUE_FROM_MISSION_DIFFICULTY(eDifficulty) * g_sMPTunables.fIMPEXP_SELL_BUYER2_OFFER_MODIFIER)
	INT iModValue 		= 0
	
	IF NOT bSellingVehSet
		iModValue = GET_SHOWROOM_MOD_COST(GET_PRIVATE_BUYER_VALUE_FROM_MISSION_DIFFICULTY(eDifficulty))
	ENDIF
	
	RETURN iValueBeforeMod + iModValue
ENDFUNC

FUNC INT GET_SPECIALIST_BUYER_VALUE_FROM_MISSION_DIFFICULT(VEHICLE_EXPORT_MISSION_DIFFICULTY eDifficulty, BOOL bSellingVehSet = FALSE)
	INT iValueBeforeMod = CEIL(GET_PRIVATE_BUYER_VALUE_FROM_MISSION_DIFFICULTY(eDifficulty) * g_sMPTunables.fIMPEXP_SELL_BUYER3_OFFER_MODIFIER)
	INT iModValue 		= 0
	
	IF NOT bSellingVehSet
		iModValue = GET_SPECIALIST_BUYER_MOD_COST(GET_PRIVATE_BUYER_VALUE_FROM_MISSION_DIFFICULTY(eDifficulty))
	ENDIF
	
	RETURN iValueBeforeMod + iModValue
ENDFUNC

/// Get the vehicle export sale value of the passed vehicle
///    Params:
///    model, the model of the vehicle to get the value of
///    bSellingVehSet - When selling a vehicle collection we need to exclude the extra cash given for having to mod the vehicles
FUNC INT GET_VEHICLE_EXPORT_VEHICLE_BASE_VALUE(MODEL_NAMES model, IE_DROPOFF eDropoff, BOOL bModCostOnly = FALSE, BOOL bGetTotalValue = FALSE, BOOL bSellingVehSet = FALSE)
	
	// Ensure the passed model is actually a valid model
	IF IS_VEHICLE_VALID_FOR_VEHICLE_EXPORT(model)
		
		IF bGetTotalValue
			VEHICLE_VALUE_DATA sData
			IF GET_VEHICLE_VALUE(sData, model)
				RETURN sData.iStandardPrice
			ENDIF
		ELSE
			VEHICLE_EXPORT_MISSION_DIFFICULTY eDifficulty = GET_MISSION_DIFFICULTY_FROM_VEHICLE_MODEL(model)
		
			INT iToReturn = 0
			INT iModCost = 0

			IF IE_IS_DROPOFF_A_SHOWROOM(eDropoff)
			AND NOT bSellingVehSet
				iModCost = GET_SHOWROOM_MOD_COST(GET_PRIVATE_BUYER_VALUE_FROM_MISSION_DIFFICULTY(eDifficulty))
				iToReturn = GET_SHOWROOM_VALUE_FROM_MISSION_DIFFICULTY(eDifficulty, bSellingVehSet)
			ELIF IE_IS_DROPOFF_A_GANGSTER(eDropoff)
			OR bSellingVehSet
				iModCost = GET_SPECIALIST_BUYER_MOD_COST(GET_PRIVATE_BUYER_VALUE_FROM_MISSION_DIFFICULTY(eDifficulty))
				iToReturn = GET_SPECIALIST_BUYER_VALUE_FROM_MISSION_DIFFICULT(eDifficulty, bSellingVehSet)
			ELSE
				iModCost = g_sMPTunables.iIMPEXP_SELL_BUYER1_MOD_COST
				iToReturn = GET_PRIVATE_BUYER_VALUE_FROM_MISSION_DIFFICULTY(eDifficulty)
			ENDIF
			
			IF bModCostOnly
				iToReturn = iModCost
			ENDIF
			
			RETURN iToReturn
		ENDIF
		
	ELSE
		PRINTLN("[VEH_EXPORT] GET_VEHICLE_EXPORT_VEHICLE_BASE_VALUE - vehicle is not valid for vehicle export")
	ENDIF
	
	PRINTLN("[VEH_EXPORT] GET_VEHICLE_EXPORT_VEHICLE_BASE_VALUE - Passed model is not a valid vehicle export model: ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(model), "; returning 0")
	RETURN 0

ENDFUNC

FUNC INT GET_NUM_DOORS_DAMAGED(VEHICLE_INDEX vehicleId)
	INT iDoorBitset, iMaxDoorCount, iFinalDoorCount
	VEHICLE_EXPORT_CHECK_THE_AVAILABLE_DOORS(vehicleId, iDoorBitset)
	
	REPEAT COUNT_OF(SC_DOOR_LIST) iMaxDoorCount
		IF INT_TO_ENUM(SC_DOOR_LIST, iMaxDoorCount) != SC_DOOR_INVALID
			IF IS_BIT_SET(iDoorBitset, iMaxDoorCount)
				IF IS_VEHICLE_DOOR_DAMAGED(vehicleId, INT_TO_ENUM(SC_DOOR_LIST, iMaxDoorCount))
					iFinalDoorCount++
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
	IF MPGlobalsAmbience.sMagnateGangBossData.bWidgetExportMissionAdditionalVehicleDamagePrints
		PRINTLN("[VEH_EXPORT] [DAMAGE_COST] [VEH_INDEX:", NATIVE_TO_INT(vehicleId), "] - Number of Doors Damaged: ", iFinalDoorCount)
	ENDIF
	#ENDIF
	
	RETURN iFinalDoorCount
ENDFUNC

FUNC INT GET_REPAIR_COST_INCREMENTS(MODEL_NAMES eModel)
	SWITCH GET_MISSION_DIFFICULTY_FROM_VEHICLE_MODEL(eModel)
		CASE eMISSION_DIFFICULTY_EASY			RETURN g_sMPTunables.iIMPEXP_STEAL_REDUCTION_EASY
		CASE eMISSION_DIFFICULTY_MEDIUM			RETURN g_sMPTunables.iIMPEXP_STEAL_REDUCTION_MEDIUM
		CASE eMISSION_DIFFICULTY_HARD			RETURN g_sMPTunables.iIMPEXP_STEAL_REDUCTION_HARD
	ENDSWITCH
	RETURN g_sMPTunables.iIMPEXP_STEAL_REDUCTION_EASY
ENDFUNC

FUNC FLOAT GET_REPAIR_COST_REDUCTION_PERCENTAGE()
	RETURN g_sMPTunables.fIMPEXP_STEAL_DAMAGE_PERCENTAGE //X%
ENDFUNC

FUNC FLOAT GET_PER_DOOR_REDUCTION_PERCENTAGE()
	
	#IF IS_DEBUG_BUILD
	IF MPGlobalsAmbience.sMagnateGangBossData.bWidgetExportMissionAdditionalVehicleDamagePrints
		PRINTLN("[VEH_EXPORT] [DAMAGE_COST] - Per door percentage ", g_sMPTunables.fIMPEXP_STEAL_DOOR_REDUCTION_PERCENTAGE)
	ENDIF
	#ENDIF

	RETURN g_sMPTunables.fIMPEXP_STEAL_DOOR_REDUCTION_PERCENTAGE
	
ENDFUNC

FUNC INT GET_DAMAGE_COST_FROM_VEHICLE_DOORS(VEHICLE_INDEX vehicleId, FLOAT fPercentage, INT iCostPerIncrement)
	RETURN FLOOR(GET_NUM_DOORS_DAMAGED(vehicleId) * ((GET_PER_DOOR_REDUCTION_PERCENTAGE()/fPercentage) * iCostPerIncrement) )
ENDFUNC

FUNC FLOAT GET_MINIMUM_MARKET_VALUE_PERCENTAGE(MODEL_NAMES vehicleModel)
	SWITCH GET_MISSION_DIFFICULTY_FROM_VEHICLE_MODEL(vehicleModel)
		CASE eMISSION_DIFFICULTY_EASY				RETURN g_sMPTunables.fIMPEXP_SELL_MINIMUM_MARKET_VALUE_PERCENTAGE_EASY
		CASE eMISSION_DIFFICULTY_MEDIUM				RETURN g_sMPTunables.fIMPEXP_SELL_MINIMUM_MARKET_VALUE_PERCENTAGE_MEDIUM
		CASE eMISSION_DIFFICULTY_HARD				RETURN g_sMPTunables.fIMPEXP_SELL_MINIMUM_MARKET_VALUE_PERCENTAGE_HARD
	ENDSWITCH
	RETURN 30.0
ENDFUNC

PROC VEHICLE_EXPORT_ADJUST_VEHICLE_VALUE_FOR_DAMAGE(VEHICLE_INDEX vehicleId, FLOAT &fBaseValue)
	FLOAT fFinalPercentage, fDamagedCausedToDoors, fMinimumMarketValue
	MODEL_NAMES vehicleModel = GET_ENTITY_MODEL(vehicleId)
	
	// Add for final percentage damage.
	fFinalPercentage = GET_VEHICLE_HEALTH_PERCENTAGE(vehicleId)
	
	fMinimumMarketValue = (fBaseValue/100.0) * GET_MINIMUM_MARKET_VALUE_PERCENTAGE(vehicleModel)

	// Get new value.
	fBaseValue = fBaseValue / 100
	fBaseValue = fBaseValue*fFinalPercentage
	
	fDamagedCausedToDoors = TO_FLOAT(GET_DAMAGE_COST_FROM_VEHICLE_DOORS(vehicleId, GET_REPAIR_COST_REDUCTION_PERCENTAGE(), GET_REPAIR_COST_INCREMENTS(vehicleModel)))
	
	fBaseValue -= fDamagedCausedToDoors
	
	IF fBaseValue < fMinimumMarketValue
		fBaseValue = fMinimumMarketValue
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF MPGlobalsAmbience.sMagnateGangBossData.bWidgetExportMissionAdditionalVehicleDamagePrints
		PRINTLN("[VEH_EXPORT] [MARKET_VALUE] [VEH_INDEX:", NATIVE_TO_INT(vehicleId), "] - Current Market Value: $", fBaseValue, " - Damage Caused to Doors: $", fDamagedCausedToDoors, " - Minimum Market Value: $", fMinimumMarketValue)
	ENDIF
	#ENDIF
	
ENDPROC

FUNC INT GET_FIRST_HITS_BONUS_DAMAGE_VALUE(MODEL_NAMES eModel, INT iNumberOfHits)
	INT iTotalBonusDamage
	SWITCH GET_MISSION_DIFFICULTY_FROM_VEHICLE_MODEL(eModel)
		CASE eMISSION_DIFFICULTY_EASY
			IF iNumberOfHits >= 1
				iTotalBonusDamage += g_sMPTunables.iIMPEXP_STEAL_FIRST_HIT_DAMAGE_1_EASY
			ENDIF
			IF iNumberOfHits >= 2
				iTotalBonusDamage += g_sMPTunables.iIMPEXP_STEAL_FIRST_HIT_DAMAGE_2_EASY
			ENDIF
			IF iNumberOfHits >= 3
				iTotalBonusDamage += g_sMPTunables.iIMPEXP_STEAL_FIRST_HIT_DAMAGE_3_EASY
			ENDIF
		BREAK
		CASE eMISSION_DIFFICULTY_MEDIUM
			IF iNumberOfHits >= 1
				iTotalBonusDamage += g_sMPTunables.iIMPEXP_STEAL_FIRST_HIT_DAMAGE_1_MEDIUM
			ENDIF
			IF iNumberOfHits >= 2
				iTotalBonusDamage += g_sMPTunables.iIMPEXP_STEAL_FIRST_HIT_DAMAGE_2_MEDIUM
			ENDIF
			IF iNumberOfHits >= 3
				iTotalBonusDamage += g_sMPTunables.iIMPEXP_STEAL_FIRST_HIT_DAMAGE_3_MEDIUM
			ENDIF
		BREAK
		CASE eMISSION_DIFFICULTY_HARD
			IF iNumberOfHits >= 1
				iTotalBonusDamage += g_sMPTunables.iIMPEXP_STEAL_FIRST_HIT_DAMAGE_1_HARD
			ENDIF
			IF iNumberOfHits >= 2
				iTotalBonusDamage += g_sMPTunables.iIMPEXP_STEAL_FIRST_HIT_DAMAGE_2_HARD
			ENDIF
			IF iNumberOfHits >= 3
				iTotalBonusDamage += g_sMPTunables.iIMPEXP_STEAL_FIRST_HIT_DAMAGE_3_HARD
			ENDIF
		BREAK
	ENDSWITCH 
	
	RETURN iTotalBonusDamage
ENDFUNC

FUNC INT GET_REPAIR_REDUCTION_DAMAGE_CAP(MODEL_NAMES eModel)
	SWITCH GET_MISSION_DIFFICULTY_FROM_VEHICLE_MODEL(eModel)
		CASE eMISSION_DIFFICULTY_HARD			RETURN g_sMPTunables.iIMPEXP_STEAL_REDUCTION_CAP_HARD
		CASE eMISSION_DIFFICULTY_MEDIUM			RETURN g_sMPTunables.iIMPEXP_STEAL_REDUCTION_CAP_MEDIUM
		CASE eMISSION_DIFFICULTY_EASY			RETURN g_sMPTunables.iIMPEXP_STEAL_REDUCTION_CAP_EASY
	ENDSWITCH
	
	RETURN g_sMPTunables.iIMPEXP_STEAL_REDUCTION_CAP_EASY
ENDFUNC

FUNC INT GET_DAMAGE_COST_DONE_TO_EXPORT_VEHICLE(VEHICLE_INDEX vehicleId, BOOL bCapCost = FALSE, INT iNumberOfHits = 0)
	MODEL_NAMES vehicleModel = GET_ENTITY_MODEL(vehicleId)
	INT iDamageDone, iDamagedCausedToDoors
	
	// Health damage
	FLOAT fHealthPercentage = GET_VEHICLE_HEALTH_PERCENTAGE(vehicleId)
	INT iNumPercentsLost = FLOOR(((100.0 - fHealthPercentage) / GET_REPAIR_COST_REDUCTION_PERCENTAGE()))
	iDamageDone = iNumPercentsLost * GET_REPAIR_COST_INCREMENTS(vehicleModel)
	
	iDamagedCausedToDoors = GET_DAMAGE_COST_FROM_VEHICLE_DOORS(vehicleId, GET_REPAIR_COST_REDUCTION_PERCENTAGE(), GET_REPAIR_COST_INCREMENTS(vehicleModel))
	iDamageDone += iDamagedCausedToDoors 

	// First hit bonus damage
	iDamageDone += GET_FIRST_HITS_BONUS_DAMAGE_VALUE(vehicleModel, iNumberOfHits)
	
	#IF IS_DEBUG_BUILD
	IF MPGlobalsAmbience.sMagnateGangBossData.bWidgetExportMissionAdditionalVehicleDamagePrints
		PRINTLN("[VEH_EXPORT] [DAMAGE_COST] [VEH_INDEX:", NATIVE_TO_INT(vehicleId), "] - Vehicle Health Percentage:", fHealthPercentage, " - Number of ", GET_REPAIR_COST_REDUCTION_PERCENTAGE(), "% Increments Lost: ", iNumPercentsLost, " - Damage Per Increment: $", GET_REPAIR_COST_INCREMENTS(vehicleModel))
		PRINTLN("[VEH_EXPORT] [DAMAGE_COST] [VEH_INDEX:", NATIVE_TO_INT(vehicleId), "] - Total Damage Done: $", iDamageDone, " - Damaged Caused To Doors: $", iDamagedCausedToDoors, " - Number Of Hits: ", iNumberOfHits, " - Damage From First Hit Bonus: $", GET_FIRST_HITS_BONUS_DAMAGE_VALUE(vehicleModel, iNumberOfHits), " - Damage Cap: $", GET_REPAIR_REDUCTION_DAMAGE_CAP(vehicleModel))
	ENDIF
	#ENDIF
	
	// Cap cost
	IF bCapCost
		IF iDamageDone > GET_REPAIR_REDUCTION_DAMAGE_CAP(vehicleModel)
			iDamageDone = GET_REPAIR_REDUCTION_DAMAGE_CAP(vehicleModel)
		ENDIF
	ENDIF
	
	RETURN iDamageDone
ENDFUNC

// THIS IS MARKET VALUE ONLY
FUNC INT GET_VEHICLE_EXPORT_VEHICLE_VALUE(VEHICLE_INDEX vehicleId, INT iExportEntity = EXPORT_ENTITY_VEHICLE, BOOL bGetTotalWorth = FALSE, BOOL bSellingVehSet = FALSE)
	FLOAT fBaseValue
	INT iVehicleValue
	
	MODEL_NAMES vehicleModel = GET_ENTITY_MODEL(vehicleId)
	IE_DROPOFF eDropoff = GB_GET_DROPOFF_ASSIGNED_TO_CAR_INDEX(iExportEntity)
	
	fBaseValue = TO_FLOAT(GET_VEHICLE_EXPORT_VEHICLE_BASE_VALUE(vehicleModel, eDropoff, DEFAULT, bGetTotalWorth, bSellingVehSet))
	VEHICLE_EXPORT_ADJUST_VEHICLE_VALUE_FOR_DAMAGE(vehicleId, fBaseValue)
	iVehicleValue = CEIL(fBaseValue)
	
	RETURN iVehicleValue
ENDFUNC

FUNC INT GET_OFFER_REDUCTION_COST(IE_DROPOFF eDropoff, MODEL_NAMES eModel)
	IF IE_IS_DROPOFF_A_GANGSTER(eDropoff)
		SWITCH GET_MISSION_DIFFICULTY_FROM_VEHICLE_MODEL(eModel)
			CASE eMISSION_DIFFICULTY_EASY			RETURN g_sMPTunables.iIMPEXP_SELL_REDUCTION_BUYER3_EASY
			CASE eMISSION_DIFFICULTY_MEDIUM			RETURN g_sMPTunables.iIMPEXP_SELL_REDUCTION_BUYER3_MEDIUM
			CASE eMISSION_DIFFICULTY_HARD			RETURN g_sMPTunables.iIMPEXP_SELL_REDUCTION_BUYER3_HARD
		ENDSWITCH
	ELIF IE_IS_DROPOFF_A_SHOWROOM(eDropoff)
		SWITCH GET_MISSION_DIFFICULTY_FROM_VEHICLE_MODEL(eModel)
			CASE eMISSION_DIFFICULTY_EASY			RETURN g_sMPTunables.iIMPEXP_SELL_REDUCTION_BUYER2_EASY
			CASE eMISSION_DIFFICULTY_MEDIUM			RETURN g_sMPTunables.iIMPEXP_SELL_REDUCTION_BUYER2_MEDIUM
			CASE eMISSION_DIFFICULTY_HARD			RETURN g_sMPTunables.iIMPEXP_SELL_REDUCTION_BUYER2_HARD
		ENDSWITCH
	ELSE // Private buyer and default
		SWITCH GET_MISSION_DIFFICULTY_FROM_VEHICLE_MODEL(eModel)
			CASE eMISSION_DIFFICULTY_EASY			RETURN g_sMPTunables.iIMPEXP_SELL_REDUCTION_BUYER1_EASY
			CASE eMISSION_DIFFICULTY_MEDIUM			RETURN g_sMPTunables.iIMPEXP_SELL_REDUCTION_BUYER1_MEDIUM
			CASE eMISSION_DIFFICULTY_HARD			RETURN g_sMPTunables.iIMPEXP_SELL_REDUCTION_BUYER1_HARD
		ENDSWITCH
	ENDIF
	RETURN 1000
ENDFUNC

FUNC FLOAT GET_OFFER_REDUCTION_PERCENTAGE()
	RETURN g_sMPTunables.fIMPEXP_SELL_DAMAGE_PERCENTAGE //X%
ENDFUNC

FUNC INT GET_CURRENT_COMMISSION_VALUE_OF_VEHICLE(VEHICLE_INDEX vehicleId, INT iExportEntity, BOOL bSellingVehSet = FALSE)
	INT iNewValue, iDamagedCausedToDoors
	
	MODEL_NAMES vehicleModel = GET_ENTITY_MODEL(vehicleId)
	IE_DROPOFF eDropoff = GB_GET_DROPOFF_ASSIGNED_TO_CAR_INDEX(iExportEntity)

	// Health damage
	FLOAT fHealthPercentage = GET_VEHICLE_HEALTH_PERCENTAGE(vehicleId)
	INT iNumPercentsLost = ROUND(((100.0 - fHealthPercentage) / GET_OFFER_REDUCTION_PERCENTAGE()))
	iNewValue = GET_VEHICLE_EXPORT_VEHICLE_BASE_VALUE(vehicleModel, eDropoff, DEFAULT, DEFAULT, bSellingVehSet) - (iNumPercentsLost * GET_OFFER_REDUCTION_COST(eDropoff, vehicleModel))
	
	// Door Damage
	iDamagedCausedToDoors = GET_DAMAGE_COST_FROM_VEHICLE_DOORS(vehicleId, GET_OFFER_REDUCTION_PERCENTAGE(), GET_OFFER_REDUCTION_COST(eDropoff, vehicleModel))
	iNewValue -= iDamagedCausedToDoors
	
	#IF IS_DEBUG_BUILD
	IF MPGlobalsAmbience.sMagnateGangBossData.bWidgetExportMissionAdditionalVehicleDamagePrints
		PRINTLN("[VEH_EXPORT] [CURRENT_COMMISSION] [VEH_INDEX:", NATIVE_TO_INT(vehicleId), "] - Vehicle Health Percentage:", fHealthPercentage, " - Number of ", GET_OFFER_REDUCTION_PERCENTAGE(), "% Increments Lost: ", iNumPercentsLost, " - Damage Per Increment: $", GET_OFFER_REDUCTION_COST(eDropoff, vehicleModel))
		PRINTLN("[VEH_EXPORT] [CURRENT_COMMISSION] [VEH_INDEX:", NATIVE_TO_INT(vehicleId), "] - Total Commission Left: $", iNewValue, " - Damaged Caused To Doors: $", iDamagedCausedToDoors)
	ENDIF
	#ENDIF
	
	RETURN iNewValue
ENDFUNC

FUNC INT GET_COMMISSION_VALUE_LOST_FROM_VEHICLE(VEHICLE_INDEX vehicleId, INT iExportEntity, BOOL bSellingVehSet = FALSE)
	MODEL_NAMES vehicleModel = GET_ENTITY_MODEL(vehicleId)
	IE_DROPOFF eDropoff = GB_GET_DROPOFF_ASSIGNED_TO_CAR_INDEX(iExportEntity)
	
	RETURN (GET_VEHICLE_EXPORT_VEHICLE_BASE_VALUE(vehicleModel, eDropoff, DEFAULT, DEFAULT, bSellingVehSet) - GET_CURRENT_COMMISSION_VALUE_OF_VEHICLE(vehicleId, iExportEntity, bSellingVehSet))
ENDFUNC

PROC MAINTAIN_VEHICLE_EXPORT_VEHICLE_VALUE(VEHICLE_INDEX vehicleId, BOOL bSellingVehSet = FALSE)
	INT iExportIndex = GB_GET_INDEX_OF_CONTRABAND_VEHICLE_PLAYER_IS_IN(PLAYER_ID())
	
	SET_VEHICLE_EXPORT_CURRENT_VEHICLE_VALUE(GET_CURRENT_COMMISSION_VALUE_OF_VEHICLE(vehicleId, iExportIndex, bSellingVehSet))
ENDPROC

PROC MAINTAIN_VEHICLE_EXPORT_RIVAL_REPAIR_COST(VEHICLE_INDEX vehicleId)
	
	PLAYER_INDEX exportOwner = GB_GET_OWNER_OF_CONTRABAND_VEHICLE_PLAYER_IS_IN(PLAYER_ID())
	
	IF exportOwner != INVALID_PLAYER_INDEX()
		// Get the appropriate drop-off reward for gang members
		IF GB_GET_LOCAL_PLAYER_GANG_BOSS() != exportOwner
			IF GB_GET_LOCAL_PLAYER_GANG_BOSS() != INVALID_PLAYER_INDEX()
			AND DOES_PLAYER_OWN_AN_IE_GARAGE(GB_GET_LOCAL_PLAYER_GANG_BOSS())
			AND NOT GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(GB_GET_LOCAL_PLAYER_GANG_BOSS())
			AND NOT IS_PLAYERS_IE_GARAGE_FULL(GB_GET_LOCAL_PLAYER_GANG_BOSS(), GET_PLAYERS_OWNED_IE_GARAGE(GB_GET_LOCAL_PLAYER_GANG_BOSS()))
				IF NOT g_IEDeliveryData.bStartCutscene
					SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()			// Repair Cost
					SET_VEHICLE_EXPORT_CURRENT_VEHICLE_REPAIR_COST(GET_DAMAGE_COST_DONE_TO_EXPORT_VEHICLE(vehicleId, TRUE))
					DRAW_GENERIC_SCORE(GET_VEHICLE_EXPORT_CURRENT_VEHICLE_REPAIR_COST(), "VEX_VEH_HUDd", Default, Default, HUDORDER_NINETHBOTTOM, Default, "HUD_CASH")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	UNUSED_PARAMETER(vehicleId)
ENDPROC

FUNC BOOL GB_IS_OWNER_OF_CONTRABAND_VEHICLE_PLAYER_IS_IN_STILL_ON_SCRIPT()
	PLAYER_INDEX exportOwner = GB_GET_OWNER_OF_CONTRABAND_VEHICLE_PLAYER_IS_IN(PLAYER_ID())
	
	IF exportOwner != INVALID_PLAYER_INDEX()
	AND NETWORK_IS_PLAYER_ACTIVE(exportOwner)
		IF GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(exportOwner) = FMMC_TYPE_VEHICLE_EXPORT_BUY
		OR GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(exportOwner) = FMMC_TYPE_VEHICLE_EXPORT_SELL
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL GB_AM_I_OWNER_OF_CONTRABAND_VEHICLE_I_AM_IN()
	PLAYER_INDEX exportOwner = GB_GET_OWNER_OF_CONTRABAND_VEHICLE_PLAYER_IS_IN(PLAYER_ID())
	IF exportOwner = PLAYER_ID()
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

CONST_INT ciVEX_TIME_TO_DELIVER_AFTER_SCRIPT_END 180000
PROC MAINTAIN_IN_VEHICLE_AFTER_SCRIPT_END()
	BOOL bClearDecorator = FALSE
	
	IF GB_GET_OWNER_OF_CONTRABAND_VEHICLE_PLAYER_IS_IN(PLAYER_ID()) != INVALID_PLAYER_INDEX()
	AND NOT NETWORK_IS_PLAYER_ACTIVE(GB_GET_OWNER_OF_CONTRABAND_VEHICLE_PLAYER_IS_IN(PLAYER_ID()))
		IF NOT MPGlobalsAmbience.sMagnateGangBossData.bExportVehicleAfterScriptEndOwnerLeft
			MPGlobalsAmbience.sMagnateGangBossData.bExportVehicleAfterScriptEndOwnerLeft = TRUE
			PRINTLN("[VEH_EXPORT] [VEX_AFTER_SCRIPT] - Owner of vehicle left the session. Setting flag to prevent any rejoin issues.")
		ENDIF
	ENDIF
	
	IF GB_IS_GLOBAL_CLIENT_BIT1_SET(PLAYER_ID(), eGB_GLOBAL_CLIENT_BITSET_1_VEHICLE_EXPORT_PLAYER_IN_VEHICLE_AFTER_SCRIPT_END)
	AND NOT GB_AM_I_OWNER_OF_CONTRABAND_VEHICLE_I_AM_IN()
	AND GB_GET_OWNER_OF_CONTRABAND_VEHICLE_PLAYER_IS_IN(PLAYER_ID()) != INVALID_PLAYER_INDEX()
	AND (NOT GB_IS_OWNER_OF_CONTRABAND_VEHICLE_PLAYER_IS_IN_STILL_ON_SCRIPT()
	OR MPGlobalsAmbience.sMagnateGangBossData.bExportVehicleAfterScriptEndOwnerLeft)

		// Timer
		INT iRemainingTime = (ciVEX_TIME_TO_DELIVER_AFTER_SCRIPT_END - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MPGlobalsAmbience.sMagnateGangBossData.timerExportVehicleAfterScriptEnd))
		HUD_COLOURS eColour
		IF iRemainingTime > 30000
			eColour = HUD_COLOUR_WHITE
		ELSE
			eColour = HUD_COLOUR_RED
		ENDIF
		DRAW_GENERIC_TIMER(iRemainingTime, "VEX_DROPOFF", 0, TIMER_STYLE_DONTUSEMILLISECONDS, -1, PODIUMPOS_NONE, HUDORDER_EIGHTHBOTTOM, FALSE, eColour, HUDFLASHING_NONE, 0, FALSE, eColour)

		// Timer expired
		IF HAS_NET_TIMER_EXPIRED(MPGlobalsAmbience.sMagnateGangBossData.timerExportVehicleAfterScriptEnd, ciVEX_TIME_TO_DELIVER_AFTER_SCRIPT_END)
			bClearDecorator = TRUE
			PRINTLN("[VEH_EXPORT] [VEX_AFTER_SCRIPT] - Remaining time: ", iRemainingTime, " - Delivery timer expired. Clear decorator.")
		ENDIF
		
		// Leaving vehicle
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND NOT IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
			bClearDecorator = TRUE
			PRINTLN("[VEH_EXPORT] [VEX_AFTER_SCRIPT] - Remaining time: ", iRemainingTime, " - Local player is leaving vehicle. Clear decorator.")
		ENDIF
		
		// Clear decorator and client bit
		IF bClearDecorator
			VEHICLE_INDEX vehicleID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(), TRUE)
			IF IS_PLAYER_IN_VALID_VEHICLE_FOR_VEHICLE_EXPORT(PLAYER_ID(), vehicleID)
				GB_CLEAR_VEHICLE_AS_CONTRABAND(vehicleID)
				IF DECOR_IS_REGISTERED_AS_TYPE("ExportVehicle", DECOR_TYPE_INT)
					IF DECOR_EXIST_ON(vehicleID,"ExportVehicle")
						DECOR_REMOVE(vehicleID,"ExportVehicle")
						PRINTLN("[VEH_EXPORT] [VEX_AFTER_SCRIPT] - Remaining time: ", iRemainingTime, " - Cleared 'ExportVehicle' decorator.")
					ENDIF
				ENDIF
				
				SET_ENTITY_INVINCIBLE(vehicleID, FALSE)
				SET_VEHICLE_ENGINE_HEALTH(vehicleID, -1000.0)
				
				PRINTLN("[VEH_EXPORT] [VEX_AFTER_SCRIPT] - Remaining time: ", iRemainingTime, " - Cleared vehicle as contraband vehicle and set engine on fire.")
			ENDIF
			GB_CLEAR_GLOBAL_CLIENT_BIT1(eGB_GLOBAL_CLIENT_BITSET_1_VEHICLE_EXPORT_PLAYER_IN_VEHICLE_AFTER_SCRIPT_END)
			RESET_NET_TIMER(MPGlobalsAmbience.sMagnateGangBossData.timerExportVehicleAfterScriptEnd)
			
			PRINTLN("[VEH_EXPORT] [VEX_AFTER_SCRIPT] - Remaining time: ", iRemainingTime, " - Cleared eGB_GLOBAL_CLIENT_BITSET_1_VEHICLE_EXPORT_PLAYER_IN_VEHICLE_AFTER_SCRIPT_END and reset timerExportVehicleAfterScriptEnd.")
		ENDIF
	ELSE
		IF HAS_NET_TIMER_STARTED(MPGlobalsAmbience.sMagnateGangBossData.timerExportVehicleAfterScriptEnd)
			#IF IS_DEBUG_BUILD
			INT iRemainingTime = (ciVEX_TIME_TO_DELIVER_AFTER_SCRIPT_END - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MPGlobalsAmbience.sMagnateGangBossData.timerExportVehicleAfterScriptEnd))
			PRINTLN("[VEH_EXPORT] [VEX_AFTER_SCRIPT] - Remaining time: ", iRemainingTime, " - Resetting net timer because...")
			IF NOT GB_IS_GLOBAL_CLIENT_BIT1_SET(PLAYER_ID(), eGB_GLOBAL_CLIENT_BITSET_1_VEHICLE_EXPORT_PLAYER_IN_VEHICLE_AFTER_SCRIPT_END)
				PRINTLN("[VEH_EXPORT] [VEX_AFTER_SCRIPT] - Remaining time: ", iRemainingTime, " - ... because eGB_GLOBAL_CLIENT_BITSET_1_VEHICLE_EXPORT_PLAYER_IN_VEHICLE_AFTER_SCRIPT_END no longer set.")
			ENDIF
			IF GB_IS_OWNER_OF_CONTRABAND_VEHICLE_PLAYER_IS_IN_STILL_ON_SCRIPT()
				PRINTLN("[VEH_EXPORT] [VEX_AFTER_SCRIPT] - Remaining time: ", iRemainingTime, " - ... because the contraband vehicle owner is still on script.")
			ENDIF
			IF GB_AM_I_OWNER_OF_CONTRABAND_VEHICLE_I_AM_IN()
				PRINTLN("[VEH_EXPORT] [VEX_AFTER_SCRIPT] - Remaining time: ", iRemainingTime, " - ... because I am the owner of the contraband vehicle")
			ENDIF
			#ENDIF
			
			IF MPGlobalsAmbience.sMagnateGangBossData.bExportVehicleAfterScriptEndOwnerLeft
				MPGlobalsAmbience.sMagnateGangBossData.bExportVehicleAfterScriptEndOwnerLeft = FALSE
				PRINTLN("[VEH_EXPORT] [VEX_AFTER_SCRIPT] - Remaining time: ", iRemainingTime, " - Resetting bExportVehicleAfterScriptEndOwnerLeft flag.")
			ENDIF
			
			RESET_NET_TIMER(MPGlobalsAmbience.sMagnateGangBossData.timerExportVehicleAfterScriptEnd)
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_VEHICLE_EXPORT_FREEMODE_LOGIC()
	
	VEHICLE_INDEX vehicleID
	
	// MAINTAIN_VEHICLE_EXPORT_RIVAL_PLAYERS(sMagnateGangBossLocalData)
	MAINTAIN_IN_VEHICLE_AFTER_SCRIPT_END()
	
	// Local player checks
	IF GB_SHOULD_PROCESS_VEHICLE_VALUE_UI(PLAYER_ID())
		
		IF IS_PLAYER_IN_VALID_VEHICLE_FOR_VEHICLE_EXPORT(PLAYER_ID(), vehicleID)
			
			MAINTAIN_VEHICLE_EXPORT_VEHICLE_VALUE(vehicleID, AM_I_OR_MY_BOSS_SELLING_A_VEHICLE_SET())
			MAINTAIN_VEHICLE_EXPORT_RIVAL_REPAIR_COST(vehicleID)
			
			IF NOT GB_AM_I_OWNER_OF_CONTRABAND_VEHICLE_I_AM_IN()
				GB_SET_GLOBAL_CLIENT_BIT1(eGB_GLOBAL_CLIENT_BITSET_1_VEHICLE_EXPORT_PLAYER_IN_VEHICLE_AFTER_SCRIPT_END)
				
				IF NOT IS_ENTITY_A_MISSION_ENTITY(vehicleId)
					SET_ENTITY_AS_MISSION_ENTITY(vehicleId)
				ENDIF
			ENDIF
			
			//Disable Passive mode when player enters Export vehicle - 3274267
			IF IS_MP_PASSIVE_MODE_ENABLED()
				DISABLE_MP_PASSIVE_MODE(PMER_START_OF_A_MATCH, FALSE)
				PRINTLN("[VEH_EXPORT] MAINTAIN_VEHICLE_EXPORT_FREEMODE_LOGIC - IS_PLAYER_IN_VALID_VEHICLE_FOR_VEHICLE_EXPORT - Exiting passive mode.")
			ENDIF
		ELSE
			GB_CLEAR_GLOBAL_CLIENT_BIT1(eGB_GLOBAL_CLIENT_BITSET_1_VEHICLE_EXPORT_PLAYER_IN_VEHICLE_AFTER_SCRIPT_END)
			
			IF NOT g_IEDeliveryData.bStartCutscene
				SET_VEHICLE_EXPORT_CURRENT_VEHICLE_REPAIR_COST(0)
			ENDIF
			SET_VEHICLE_EXPORT_CURRENT_VEHICLE_VALUE(0)
		ENDIF
	ELSE
		GB_CLEAR_GLOBAL_CLIENT_BIT1(eGB_GLOBAL_CLIENT_BITSET_1_VEHICLE_EXPORT_PLAYER_IN_VEHICLE_AFTER_SCRIPT_END)
	ENDIF

ENDPROC

PROC TRIGGER_IE_VEHICLE_MODS_TRANSACTION(IE_VEHICLE_ENUM &eVehicle[MAX_NUM_VEHICLE_EXPORT_ENTITIES],IE_DROPOFF &eDropOff[MAX_NUM_VEHICLE_EXPORT_ENTITIES])
	INT iBossUUIDa, iBossUUIDb, iModsCost, iDropOffHash, i, iVehicleHash[MAX_NUM_VEHICLE_EXPORT_ENTITIES]
		
	GB_GET_GANG_BOSS_UUID_INTS(PLAYER_ID(), iBossUUIDa, iBossUUIDb)
	
	//Calculate the mods cost
	REPEAT MAX_NUM_VEHICLE_EXPORT_ENTITIES i
		
		iVehicleHash[i] = GET_HASH_KEY(GET_STRING_FROM_IE_VEHICLE_ENUM(eVehicle[i]))
		
		IF ENUM_TO_INT(eVehicle[i]) != 0
			iModsCost += GET_VEHICLE_EXPORT_VEHICLE_BASE_VALUE(GET_MODEL_NAME_FROM_IE_VEHICLE(eVehicle[i]), eDropOff[i], TRUE)
		ENDIF
	ENDREPEAT
	
	iDropOffHash = BOOL_TO_INT(IE_IS_DROPOFF_A_GANGSTER(eDropOff[0]))
	
	#IF IS_DEBUG_BUILD
	CDEBUG1LN(DEBUG_AMBIENT, "TRIGGER_IE_VEHICLE_MODS_TRANSACTION Triggering transaction. Part 1: Vehicle 1: ", iVehicleHash[0], " Vehicle 2: ", iVehicleHash[1], " Vehicle 3: ", iVehicleHash[2], " Vehicle 4: ", iVehicleHash[3])
	CDEBUG1LN(DEBUG_AMBIENT, "TRIGGER_IE_VEHICLE_MODS_TRANSACTION Triggering transaction. Part 2: Cost: ", iModsCost, " iBossUUIDa: ", iBossUUIDa, " iBossUUIDb ", iBossUUIDb, " iDropOffHash ", iDropOffHash)
	#ENDIF
	
	IF iModsCost > 0
		IF USE_SERVER_TRANSACTIONS()
			INT iTransactionIndex
			TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_SPEND_VEHICLE_EXPORT_MODS , iModsCost, iTransactionIndex, FALSE, TRUE, FALSE)
			g_cashTransactionData[iTransactionIndex].cashInfo.iItemHash 	= iVehicleHash[0]
			g_cashTransactionData[iTransactionIndex].cashInfo.iVehicle1 	= iVehicleHash[1]
			g_cashTransactionData[iTransactionIndex].cashInfo.iVehicle2 	= iVehicleHash[2]
			g_cashTransactionData[iTransactionIndex].cashInfo.iVehicle3 	= iVehicleHash[3]
			g_cashTransactionData[iTransactionIndex].cashInfo.iGangUUIDA 	= iBossUUIDa
			g_cashTransactionData[iTransactionIndex].cashInfo.iGangUUIDB 	= iBossUUIDb
			g_cashTransactionData[iTransactionIndex].cashInfo.iLocation		= iDropOffHash
		ELSE
			NETWORK_SPENT_VEHICLE_EXPORT_MODS(iModsCost, FALSE, TRUE, iBossUUIDa, iBossUUIDb, iDropOffHash, iVehicleHash[0], iVehicleHash[1], iVehicleHash[2], iVehicleHash[3])
		ENDIF
	ENDIF
ENDPROC


