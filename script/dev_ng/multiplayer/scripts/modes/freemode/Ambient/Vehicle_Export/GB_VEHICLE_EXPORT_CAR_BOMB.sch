//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Written by:  Steve Tiley 																							//
// Date: 		8/11/2016																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//----------------------
//	INCLUDES
//----------------------


USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_script.sch"
USING "net_include.sch"
USING "script_ped.sch"
USING "shared_hud_displays.sch"


//----------------------
//	ENUMS
//----------------------



//----------------------
//	CONSTANTS
//----------------------

CONST_INT iCARBOMB_BITSET0_BOMB_ARMED 			0
CONST_INT iCARBOMB_BITSET0_BOMB_DETONATING		1
CONST_INT iCARBOMB_BITSET0_BOMB_DEFUSED			2
CONST_INT iCARBOMB_BITSET0_IN_CAR_BOMB_CAR		3
CONST_INT iCARBOMB_BITSET0_BOMB_EXPLODED		4


//----------------------
//	STRUCTS
//----------------------

STRUCT CAR_BOMB_STRUCT
	NETWORK_INDEX niVeh
	INT iCarBombBitSet
	SCRIPT_TIMER BombArmingTimer
	SCRIPT_TIMER BombSpeedTimer
	INT iBombActivationTime
	INT iBombDetonationTime
	INT iSpeed
	INT iSpeedLastFrame
	INT iTargetSpeedMPH = 60
	INT iTargetSpeedKPH = 100
	BOOL bBeenInAir
	BOOL bFrameCarry1
	BOOL bFrameCarry2
	
	// Audio
	INT iBombArmingSound = -1
	INT iBombArmedSound = -1
	BOOL bDoneBombArmedSound
	INT iExplosionStartSound = -1
	INT iExplosionLoopSound = -1
	INT iExplosionStoppedSound = -1
	
	// Debug stuff
	#IF IS_DEBUG_BUILD
	BOOL bIgnoreSpeed = FALSE
	#ENDIF
ENDSTRUCT

//----------------------------------------------------------------
//														 /////////
//	MAIN FUNCTIONS										/////////
//													   /////////
//-------------------------------------------------------------

FUNC BOOL IS_CAR_BOMB_ARMED(CAR_BOMB_STRUCT &sCarBombStruct)
	RETURN IS_BIT_SET(sCarBombStruct.iCarBombBitSet, iCARBOMB_BITSET0_BOMB_ARMED)
ENDFUNC

PROC SET_CAR_BOMB_ARMED(CAR_BOMB_STRUCT &sCarBombStruct)
	IF NOT IS_CAR_BOMB_ARMED(sCarBombStruct)
		PRINTLN("[VEH_EXPORT] [CAR_BOMB] - SET_CAR_BOMB_ARMED - Car bomb armed, iCARBOMB_BITSET0_BOMB_ARMED set")
		SET_BIT(sCarBombStruct.iCarBombBitSet, iCARBOMB_BITSET0_BOMB_ARMED)
	ENDIF
ENDPROC

FUNC BOOL IS_CAR_BOMB_DEFUSED(CAR_BOMB_STRUCT &sCarBombStruct)
	RETURN IS_BIT_SET(sCarBombStruct.iCarBombBitSet, iCARBOMB_BITSET0_BOMB_DEFUSED)
ENDFUNC

FUNC BOOL DOES_CAR_BOMB_VEH_EXIST(CAR_BOMB_STRUCT& sCarBombStruct)
	RETURN NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sCarBombStruct.niVeh)
ENDFUNC

FUNC VEHICLE_INDEX CAR_BOMB_VEH_ID(CAR_BOMB_STRUCT& sCarBombStruct)
	IF DOES_CAR_BOMB_VEH_EXIST(sCarBombStruct)
		RETURN NET_TO_VEH(sCarBombStruct.niVeh)
	ENDIF
	
	RETURN NULL
ENDFUNC

FUNC STRING CAR_BOMB_SOUNDSET()
	IF GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_IMPEXP_VELOCITY
		RETURN "GTAO_DLC_IE_Velocity_Sounds"
	ENDIF
	
	RETURN "GTAO_DLC_IE_Bomb_Defuse_Sounds"
ENDFUNC

PROC SET_CAR_BOMB_DEFUSED(CAR_BOMB_STRUCT &sCarBombStruct)
	IF NOT IS_CAR_BOMB_DEFUSED(sCarBombStruct)
		PRINTLN("[VEH_EXPORT] [CAR_BOMB] - SET_CAR_BOMB_DEFUSED - Car bomb defused, iCARBOMB_BITSET0_BOMB_DEFUSED set")
		PLAY_SOUND_FROM_ENTITY(-1, "Bomb_Disarmed", CAR_BOMB_VEH_ID(sCarBombStruct), CAR_BOMB_SOUNDSET(), FALSE, 25)
		SET_BIT(sCarBombStruct.iCarBombBitSet, iCARBOMB_BITSET0_BOMB_DEFUSED)
	ENDIF
ENDPROC

FUNC BOOL IS_CAR_BOMB_VEH_EXPLODED(CAR_BOMB_STRUCT &sCarBombStruct)
	RETURN IS_BIT_SET(sCarBombStruct.iCarBombBitSet, iCARBOMB_BITSET0_BOMB_EXPLODED)
ENDFUNC

PROC SET_CAR_BOMB_VEH_IS_EXPLODED(CAR_BOMB_STRUCT &sCarBombStruct)
	IF NOT IS_CAR_BOMB_VEH_EXPLODED(sCarBombStruct)
		PRINTLN("[VEH_EXPORT] [CAR_BOMB] - SET_CAR_BOMB_VEH_IS_EXPLODED - iCARBOMB_BITSET0_BOMB_EXPLODED set")
		SET_BIT(sCarBombStruct.iCarBombBitSet, iCARBOMB_BITSET0_BOMB_EXPLODED)
	ENDIF
ENDPROC

FUNC FLOAT GET_CAR_BOMB_BEEP_RAMP_VALUE(CAR_BOMB_STRUCT& sCarBombStruct)
	FLOAT fTime, fTarget, fPercent
	
	fTime = TO_FLOAT(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sCarBombStruct.BombSpeedTimer))
	fTarget = TO_FLOAT(sCarBombStruct.iBombDetonationTime)
	fPercent = fTime / fTarget
	
	RETURN fPercent
ENDFUNC

FUNC BOOL IS_CAR_BOMB_VEH_OK(CAR_BOMB_STRUCT& sCarBombStruct)
	IF DOES_CAR_BOMB_VEH_EXIST(sCarBombStruct)
		IF IS_VEHICLE_DRIVEABLE(CAR_BOMB_VEH_ID(sCarBombStruct))
		AND NOT IS_ENTITY_DEAD(CAR_BOMB_VEH_ID(sCarBombStruct))
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_IN_CAR_BOMB_CAR(CAR_BOMB_STRUCT &sCarBombStruct, BOOL bConsiderEnteringAsInVehicle = FALSE)
	IF DOES_CAR_BOMB_VEH_EXIST(sCarBombStruct)
	AND IS_CAR_BOMB_VEH_OK(sCarBombStruct)
		IF IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(), CAR_BOMB_VEH_ID(sCarBombStruct), bConsiderEnteringAsInVehicle)
		AND GET_PED_IN_VEHICLE_SEAT(CAR_BOMB_VEH_ID(sCarBombStruct), VS_DRIVER) = PLAYER_PED_ID()
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

PROC MAINTAIN_CAR_BOMB_SPEED(CAR_BOMB_STRUCT& sCarBombStruct)
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF DOES_CAR_BOMB_VEH_EXIST(sCarBombStruct)
		AND IS_CAR_BOMB_VEH_OK(sCarBombStruct)
			sCarBombStruct.iSpeed = ABSI(ROUND(STAT_GET_CURRENT_SPEED()))
			
			IF IS_ENTITY_IN_AIR(CAR_BOMB_VEH_ID(sCarBombStruct))
				sCarBombStruct.iSpeed = sCarBombStruct.iSpeedLastFrame
				IF sCarBombStruct.bBeenInAir = FALSE
					sCarBombStruct.bBeenInAir = TRUE
					sCarBombStruct.bFrameCarry1 = TRUE
					sCarBombStruct.bFrameCarry2 = TRUE
				ENDIF
			ELSE
				IF sCarBombStruct.bBeenInAir
					IF sCarBombStruct.bFrameCarry1
						sCarBombStruct.iSpeed = sCarBombStruct.iSpeedLastFrame
						sCarBombStruct.bFrameCarry1 = FALSE
					ELSE
						IF sCarBombStruct.bFrameCarry2
							sCarBombStruct.iSpeed = sCarBombStruct.iSpeedLastFrame
							sCarBombStruct.bFrameCarry2 = FALSE
						ELSE
							sCarBombStruct.iSpeed = sCarBombStruct.iSpeedLastFrame
							sCarBombStruct.bBeenInAir = FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT IS_ENTITY_IN_AIR(CAR_BOMB_VEH_ID(sCarBombStruct))
			AND sCarBombStruct.bBeenInAir = FALSE
			AND	sCarBombStruct.bFrameCarry1 = FALSE
			AND	sCarBombStruct.bFrameCarry2 = FALSE
				sCarBombStruct.iSpeedLastFrame = ABSI(ROUND(STAT_GET_CURRENT_SPEED()))
			ENDIF
			
			#IF IS_DEBUG_BUILD
			IF sCarBombStruct.bIgnoreSpeed = TRUE
				sCarBombStruct.iSpeed = sCarBombStruct.iTargetSpeedKPH + 10
			ENDIF
			#ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC INT GET_CAR_BOMB_VEH_MINIMUM_SPEED(CAR_BOMB_STRUCT& sCarBombStruct, BOOL bForBottomRight = FALSE)
	
	IF bForBottomRight = FALSE
		IF NOT SHOULD_USE_METRIC_MEASUREMENTS()
			RETURN sCarBombStruct.iTargetSpeedMPH
		ENDIF
	ENDIF

	RETURN sCarBombStruct.iTargetSpeedKPH
ENDFUNC

PROC EXPLODE_CAR_BOMB_VEHICLE(CAR_BOMB_STRUCT& sCarBombStruct)
	IF DOES_CAR_BOMB_VEH_EXIST(sCarBombStruct)
	AND IS_CAR_BOMB_VEH_OK(sCarBombStruct)
		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(sCarBombStruct.niVeh)
		OR TAKE_CONTROL_OF_NET_ID(sCarBombStruct.niVeh)
			SET_ENTITY_INVINCIBLE(CAR_BOMB_VEH_ID(sCarBombStruct), FALSE)
			SET_ENTITY_CAN_BE_DAMAGED(CAR_BOMB_VEH_ID(sCarBombStruct), TRUE)
			NETWORK_EXPLODE_VEHICLE(CAR_BOMB_VEH_ID(sCarBombStruct), TRUE)
			SET_CAR_BOMB_VEH_IS_EXPLODED(sCarBombStruct)
			PRINTLN("[VEH_EXPORT] [CAR_BOMB] - EXPLODE_CAR_BOMB_VEHICLE - Exploded!")
		ENDIF
	ENDIF
ENDPROC

PROC CLEANUP_ALL_SOUNDS(CAR_BOMB_STRUCT& sCarBombStruct)
	IF sCarBombStruct.iBombArmingSound != -1
		STOP_SOUND(sCarBombStruct.iBombArmingSound)
		RELEASE_SOUND_ID(sCarBombStruct.iBombArmingSound)
		sCarBombStruct.iBombArmingSound = -1
	ENDIF
	IF sCarBombStruct.iExplosionStartSound != -1
		STOP_SOUND(sCarBombStruct.iExplosionStartSound)
		RELEASE_SOUND_ID(sCarBombStruct.iExplosionStartSound)
		sCarBombStruct.iExplosionStartSound = -1
	ENDIF
	IF sCarBombStruct.iExplosionLoopSound != -1
		STOP_SOUND(sCarBombStruct.iExplosionLoopSound)
		RELEASE_SOUND_ID(sCarBombStruct.iExplosionLoopSound)
		sCarBombStruct.iExplosionLoopSound = -1
	ENDIF
	IF sCarBombStruct.iExplosionStoppedSound != -1
		STOP_SOUND(sCarBombStruct.iExplosionStoppedSound)
		RELEASE_SOUND_ID(sCarBombStruct.iExplosionStoppedSound)
		sCarBombStruct.iExplosionStoppedSound = -1
	ENDIF
ENDPROC

PROC MAINTAIN_CAR_BOMB_VEHICLE_EXPLODING(CAR_BOMB_STRUCT& sCarBombStruct)
	IF sCarBombStruct.iSpeed < GET_CAR_BOMB_VEH_MINIMUM_SPEED(sCarBombStruct, TRUE)
		IF NOT HAS_NET_TIMER_STARTED(sCarBombStruct.BombSpeedTimer)
			IF IS_LOCAL_PLAYER_IN_CAR_BOMB_CAR(sCarBombStruct)
				PRINTLN("[VEH_EXPORT] [CAR_BOMB] - MAINTAIN_CAR_BOMB_VEHICLE_EXPLODING - Local player driving too slowly, starting timer")
				START_NET_TIMER(sCarBombStruct.BombSpeedTimer)
			ENDIF
		ELSE
			IF HAS_NET_TIMER_EXPIRED(sCarBombStruct.BombSpeedTimer, sCarBombStruct.iBombDetonationTime)
				PRINTLN("[VEH_EXPORT] [CAR_BOMB] - MAINTAIN_CAR_BOMB_VEHICLE_EXPLODING - Too slow for too long, explode")
				EXPLODE_CAR_BOMB_VEHICLE(sCarBombStruct)
			ENDIF
		ENDIF
		IF sCarBombStruct.iExplosionStoppedSound != -1
			STOP_SOUND(sCarBombStruct.iExplosionStoppedSound)
			RELEASE_SOUND_ID(sCarBombStruct.iExplosionStoppedSound)
			sCarBombStruct.iExplosionStoppedSound = -1
		ENDIF
		IF sCarBombStruct.iExplosionStartSound = -1
			sCarBombStruct.iExplosionStartSound = GET_SOUND_ID()
			PLAY_SOUND_FROM_ENTITY(sCarBombStruct.iExplosionStartSound, "Count_Start", CAR_BOMB_VEH_ID(sCarBombStruct), CAR_BOMB_SOUNDSET(), FALSE, 25)
			PRINTLN("[VEH_EXPORT] [CAR_BOMB] - MAINTAIN_CAR_BOMB_VEHICLE_EXPLODING - Started explode sound")
		ENDIF
		IF sCarBombStruct.iExplosionLoopSound = -1
			sCarBombStruct.iExplosionLoopSound = GET_SOUND_ID()
			INT iTime = sCarBombStruct.iBombDetonationTime - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sCarBombStruct.BombSpeedTimer)
			PLAY_SOUND_FROM_ENTITY(sCarBombStruct.iExplosionLoopSound, "Countdown_Loop", CAR_BOMB_VEH_ID(sCarBombStruct), CAR_BOMB_SOUNDSET(), FALSE, 25)
			SET_VARIABLE_ON_SOUND(sCarBombStruct.iExplosionLoopSound, "Time", TO_FLOAT(iTime/1000))
			PRINTLN("[VEH_EXPORT] [CAR_BOMB] - MAINTAIN_CAR_BOMB_VEHICLE_EXPLODING - Started explode loop sound")
		ELSE
			FLOAT fRampValue = GET_CAR_BOMB_BEEP_RAMP_VALUE(sCarBombStruct)
			SET_VARIABLE_ON_SOUND(sCarBombStruct.iExplosionLoopSound, "Ctrl", fRampValue)
		ENDIF
	ELSE
		IF sCarBombStruct.iExplosionStoppedSound = -1
			sCarBombStruct.iExplosionStoppedSound = GET_SOUND_ID()
			PLAY_SOUND_FROM_ENTITY(sCarBombStruct.iExplosionStoppedSound, "Count_Stop", CAR_BOMB_VEH_ID(sCarBombStruct), CAR_BOMB_SOUNDSET(), FALSE, 25)
			PRINTLN("[VEH_EXPORT] [CAR_BOMB] - MAINTAIN_CAR_BOMB_VEHICLE_EXPLODING - Stopped explode sound")
		ENDIF
		IF sCarBombStruct.iExplosionStartSound != -1
			STOP_SOUND(sCarBombStruct.iExplosionStartSound)
			RELEASE_SOUND_ID(sCarBombStruct.iExplosionStartSound)
			sCarBombStruct.iExplosionStartSound = -1
		ENDIF
		IF sCarBombStruct.iExplosionLoopSound != -1
			STOP_SOUND(sCarBombStruct.iExplosionLoopSound)
			RELEASE_SOUND_ID(sCarBombStruct.iExplosionLoopSound)
			sCarBombStruct.iExplosionLoopSound = -1
		ENDIF
		IF HAS_NET_TIMER_STARTED(sCarBombStruct.BombSpeedTimer)
			PRINTLN("[VEH_EXPORT] [CAR_BOMB] - MAINTAIN_CAR_BOMB_VEHICLE_EXPLODING - Local player driving fast enough, resetting timer")
			RESET_NET_TIMER(sCarBombStruct.BombSpeedTimer)
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_CAR_BOMB_UI(CAR_BOMB_STRUCT& sCarBombStruct)
	IF IS_LOCAL_PLAYER_IN_CAR_BOMB_CAR(sCarBombStruct)
		IF NOT IS_CAR_BOMB_ARMED(sCarBombStruct)
			INT iBombTime = (sCarBombStruct.iBombActivationTime - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sCarBombStruct.BombArmingTimer))
			
			IF sCarBombStruct.iBombArmingSound = -1
				sCarBombStruct.iBombArmingSound = GET_SOUND_ID()
				PLAY_SOUND_FROM_ENTITY(sCarBombStruct.iBombArmingSound, "Arming_Countdown", CAR_BOMB_VEH_ID(sCarBombStruct), CAR_BOMB_SOUNDSET(), FALSE, 25)
				SET_VARIABLE_ON_SOUND(sCarBombStruct.iBombArmingSound, "Time", TO_FLOAT(iBombTime/1000))
				PRINTLN("[VEH_EXPORT] [CAR_BOMB] - MAINTAIN_CAR_BOMB_UI - Started arming sound")
			ENDIF
			
			IF iBombTime < 0
				iBombTime = 0
			ENDIF
			
			IF iBombTime >= 0
			AND iBombTime < sCarBombStruct.iBombActivationTime
				DRAW_GENERIC_TIMER(iBombTime, "IE_VEL_BOMB", DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDORDER_THIRDBOTTOM, DEFAULT, HUD_COLOUR_RED)
			ENDIF
		ELSE
			IF sCarBombStruct.bDoneBombArmedSound = FALSE
				PLAY_SOUND_FROM_ENTITY(-1, "Bomb_Armed", CAR_BOMB_VEH_ID(sCarBombStruct), CAR_BOMB_SOUNDSET(), FALSE, 25)
				PRINTLN("[VEH_EXPORT] [CAR_BOMB] - MAINTAIN_CAR_BOMB_UI - Played bomb armed sound")
				sCarBombStruct.bDoneBombArmedSound = TRUE
			ENDIF
			IF sCarBombStruct.iBombArmingSound != -1
				RELEASE_SOUND_ID(sCarBombStruct.iBombArmingSound)
				PRINTLN("[VEH_EXPORT] [CAR_BOMB] - MAINTAIN_CAR_BOMB_UI - Arming sound stopped")
				sCarBombStruct.iBombArmingSound = -1
			ENDIF
			IF HAS_NET_TIMER_STARTED(sCarBombStruct.BombSpeedTimer)
				DRAW_GENERIC_METER(	GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sCarBombStruct.BombSpeedTimer), 
									sCarBombStruct.iBombDetonationTime, "IE_VEL_EXPL", HUD_COLOUR_RED, DEFAULT, HUDORDER_THIRDBOTTOM)
			ENDIF
		ENDIF
	
		INT iBarMax = CEIL(sCarBombStruct.iTargetSpeedKPH * 1.6)
		PERCENTAGE_METER_LINE percentLine = PERCENTAGE_METER_LINE_60
		HUD_COLOURS eColour = HUD_COLOUR_RED
		
		IF sCarBombStruct.iSpeed > GET_CAR_BOMB_VEH_MINIMUM_SPEED(sCarBombStruct, TRUE)
			eColour = HUD_COLOUR_GREEN
		ENDIF
		
		DRAW_GENERIC_METER(	sCarBombStruct.iSpeed, iBarMax, "IE_VEL_SPEED", eColour, DEFAULT, HUDORDER_SECONDBOTTOM, DEFAULT, 
							DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, percentLine)
	ENDIF
ENDPROC

PROC MAINTAIN_CAR_BOMB(CAR_BOMB_STRUCT& sCarBombStruct)
	IF NOT IS_CAR_BOMB_DEFUSED(sCarBombStruct)
	AND NOT IS_CAR_BOMB_VEH_EXPLODED(sCarBombStruct)
		IF NOT IS_CAR_BOMB_ARMED(sCarBombStruct)
			IF DOES_CAR_BOMB_VEH_EXIST(sCarBombStruct)
			AND IS_CAR_BOMB_VEH_OK(sCarBombStruct)
				IF NOT HAS_NET_TIMER_STARTED(sCarBombStruct.BombArmingTimer)
					IF IS_LOCAL_PLAYER_IN_CAR_BOMB_CAR(sCarBombStruct)
						PRINTLN("[VEH_EXPORT] [CAR_BOMB] - MAINTAIN_CAR_BOMB - Player has entered car, starting the arming timer")
						START_NET_TIMER(sCarBombStruct.BombArmingTimer)
					ENDIF
				ELSE
					// Only show UI and arm if still in the car, blow it up if you get out whilst arming
					IF IS_LOCAL_PLAYER_IN_CAR_BOMB_CAR(sCarBombStruct)
						MAINTAIN_CAR_BOMB_SPEED(sCarBombStruct)
						MAINTAIN_CAR_BOMB_UI(sCarBombStruct)
						
						IF HAS_NET_TIMER_EXPIRED(sCarBombStruct.BombArmingTimer, sCarBombStruct.iBombActivationTime)
							PRINTLN("[VEH_EXPORT] [CAR_BOMB] - MAINTAIN_CAR_BOMB - Arming time has passed, bomb is now armed")
							SET_CAR_BOMB_ARMED(sCarBombStruct)
						ENDIF
					ELSE
						EXPLODE_CAR_BOMB_VEHICLE(sCarBombStruct)
					ENDIF
				ENDIF
			ELSE
				CLEANUP_ALL_SOUNDS(sCarBombStruct)
			ENDIF
		ELSE
			IF DOES_CAR_BOMB_VEH_EXIST(sCarBombStruct)
			AND IS_CAR_BOMB_VEH_OK(sCarBombStruct)
				IF IS_LOCAL_PLAYER_IN_CAR_BOMB_CAR(sCarBombStruct)
					MAINTAIN_CAR_BOMB_SPEED(sCarBombStruct)
					MAINTAIN_CAR_BOMB_VEHICLE_EXPLODING(sCarBombStruct)
					MAINTAIN_CAR_BOMB_UI(sCarBombStruct)
				ELSE
					EXPLODE_CAR_BOMB_VEHICLE(sCarBombStruct)
				ENDIF
			ELSE
				CLEANUP_ALL_SOUNDS(sCarBombStruct)
			ENDIF
		ENDIF
	ELSE
		CLEANUP_ALL_SOUNDS(sCarBombStruct)
	ENDIF
ENDPROC


