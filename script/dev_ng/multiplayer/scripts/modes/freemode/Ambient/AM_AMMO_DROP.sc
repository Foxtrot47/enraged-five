//////////////////////////////////////////////////////////////////////////////////////////
// Name:        AM_AMMO_DROP.sc															//
// Description: Player calls for an Ammo drop to their current location.				//
// Written by:  Ryan Baker																//
// Date: 18/12/2012																		//
//////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"

// Game Headers
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_path.sch"
//USING "commands_interiors.sch" 
USING "commands_event.sch"

// Network Headers
USING "net_include.sch"
USING "net_events.sch"
USING "net_spawn.sch"

USING "net_mission.sch"
//
USING "net_scoring_common.sch"
USING "net_ambience.sch"

//USING "help_at_location.sch"
USING "commands_object.sch"
USING "net_xp_animation.sch"
USING "net_ammo_drop.sch"

USING "net_wait_zero.sch"

#IF IS_DEBUG_BUILD
USING "net_debug_log.sch"
#ENDIF

// Game States. 
CONST_INT GAME_STATE_INI 				0
CONST_INT GAME_STATE_RUNNING			1
CONST_INT GAME_STATE_LEAVE				2
CONST_INT GAME_STATE_TERMINATE_DELAY	3
CONST_INT GAME_STATE_END				4

CONST_FLOAT PLANE_DESTINATION_HEIGHT 	50.0
CONST_FLOAT LEAVE_RANGE					250.0
CONST_FLOAT PLANE_CREATION_HEIGHT 		200.0
CONST_FLOAT PLANE_CREATION_RADIUS 		400.0
CONST_FLOAT CRATE_DROP_START			5.0	//17.5
CONST_INT PLANE_SPEED					60
TWEAK_INT AMMO_CLIPS					4
TWEAK_FLOAT CRATE_DROP_SPEED			-8.0

ENUM AMMO_DROP_STAGE_ENUM
	eAD_DROP_AMMO,
	eAD_PICKUP_AMMO,
	//eAD_REWARD,
	eAD_CLEANUP
ENDENUM

STRUCT DESTRUCTIBLE_CRATE_DATA
	NETWORK_INDEX NetID
	MODEL_NAMES Model = PROP_BOX_WOOD02A_PU	//PROP_BOX_WOOD02A_MWS	***MODELS CAN BE SWAPPED ONCE BUG 1411677 IS FIXED***
ENDSTRUCT

//Server Bitset Data
CONST_INT biS_AnyCrewArrested		0
CONST_INT biS_AllCrewArrested		1
CONST_INT biS_AllCrewDead			2
CONST_INT biS_AllCrewLeftStartArea	3
CONST_INT biS_AnyCrewHasFinished 	4
CONST_INT biS_AllCrewHaveFinished 	5
CONST_INT biS_AllCrewHaveLeft 		6
CONST_INT biS_ReadyForDrop			7
CONST_INT biS_CrateBreakable		8
CONST_INT biS_DoDeathCleanup		9

// The server broadcast data.
// Everyone can read this data, only the server can update it
STRUCT ServerBroadcastData
	INT iServerGameState
		
	INT iServerBitSet
	
	NETWORK_INDEX niPlane
	NETWORK_INDEX niPilot
	NETWORK_INDEX niCrate
	NETWORK_INDEX niChute
		
	VECTOR vDropLocation
	VECTOR vRandomEnd
	AMMO_DROP_STAGE_ENUM eAmmoDropStage = eAD_DROP_AMMO
	
	INT iReservedVehicles = 1
	INT iReservedPeds = 1
	INT iReservedObjects = 3
	
	INT iBleepSoundID = -1
	
	PLAYER_INDEX MainPlayer
	BOOL bisballisticdrop = FALSE
	DESTRUCTIBLE_CRATE_DATA DCrateData
	
	SCRIPT_TIMER MissionTerminateDelayTimer
ENDSTRUCT
ServerBroadcastData serverBD


CONST_INT biP_AmmoPickedUp			0

// The client broadcast data.
// Everyone can read this data, only the local player only the server can update it
STRUCT PlayerBroadcastData
	INT iGameState
	
	INT iPlayerBitSet
	
	AMMO_DROP_STAGE_ENUM eAmmoDropStage = eAD_DROP_AMMO
	
	#IF IS_DEBUG_BUILD
		INT iDebugPassFail = 0
	#ENDIF
ENDSTRUCT
PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]

///// MISSION VARIABLES /////
INT iBoolsBitSet
CONST_INT biInitialSetup			0
CONST_INT biHelpDisplayed			1
CONST_INT biAmmoDropped				2
CONST_INT biAmmoLanded				3
CONST_INT biAmmoPickedUp			4
CONST_INT biAmmoRewardGiven			5
CONST_INT biLandAnimStarted			6
CONST_INT biLandAnimPlaying			7
CONST_INT biLandAnimDone			8
CONST_INT biPickupPrevented			9
CONST_INT biSequencesCreated		10
CONST_INT biInitialHelpDone			11
CONST_INT biPickupPreventedHelpDone		12
CONST_INT biPickupPreventedHelpDone2	13
CONST_INT biDetachDestructibleCrateDone	14
CONST_INT biPilotIncomingLineDone	15
CONST_INT biSetSubmergedSound		16
CONST_INT biInitialHelpDoneBall			17
/*NETWORK_INDEX niPlane
NETWORK_INDEX niPilot
NETWORK_INDEX niCrate
NETWORK_INDEX niChute*/

BLIP_INDEX AmmoCrateBlip

SEQUENCE_INDEX seqPlane

//INT iTotalAmmoCollected

PTFX_ID ptfxBeacon

//INT iPickupSoundID = -1

BOOL bInTutorialSession

SCRIPT_TIMER iSecondHelpTimer
CONST_INT SECOND_HELP_DELAY		20000

SCRIPT_TIMER iStartDelayTimer
CONST_INT START_DELAY_TIME		4000

structPedsForConversation sSpeech

//INT iCinematicShotStage
//BOOL bDisplayedCineCamHelp

// Widgets & Debug.
#IF IS_DEBUG_BUILD
	BOOL bHostEndMissionNow
	BOOL bWarpToEnd
	BOOL debug_bDisplayDebug
#ENDIF

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  MAIN PROCS                            ////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Do necessary pre game start ini.
// Returns FALSE if the script fails to receive its initial network broadcast data.
FUNC BOOL PROCESS_PRE_GAME( MP_MISSION_DATA missionScriptArgs )
	
	// This marks the script as a net script, and handles any instancing setup. 
	SETUP_MP_MISSION_FOR_NETWORK(GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(missionScriptArgs.mdID.idMission) ,  missionScriptArgs)	//NUM_NETWORK_PLAYERS, missionScriptArgs)	//
	//NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE, PARTICIPANT_ID_TO_INT())  
	
	//Reserve Entities
	RESERVE_NETWORK_MISSION_PEDS(1)
	RESERVE_NETWORK_MISSION_VEHICLES(1)
	RESERVE_NETWORK_MISSION_OBJECTS(2)
	
	// This makes sure the net script is active, waits untill it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
	
	// KGM: Wait for the first network broadcast before moving on
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		RETURN FALSE
	ENDIF
			
	// This script will not be paused if another script calls PAUSE_GAME
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		serverBD.MainPlayer = PLAYER_ID()
		NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - MainPlayer = ") NET_PRINT(GET_PLAYER_NAME(serverBD.MainPlayer)) NET_NL()
		
		serverBD.vDropLocation = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<0, CRATE_DROP_START*1.5, 0>>)	//GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
		
		serverBD.iReservedVehicles = 1
		serverBD.iReservedPeds = 1
		serverBD.iReservedObjects = 3
		
		serverBD.vRandomEnd = <<0, 0, 500>>
		
		// Check if its a ballistic drop	
		IF missionScriptArgs.iTruckBitSet = 3
			PRINTLN(" AMMO DROP PROCESS_PRE_GAME this is a ballistic drop serverBD.bisballisticdrop = TRUE ")
			serverBD.bisballisticdrop = TRUE
		ENDIF
		
		
		INT iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
		IF iRand = 1
			serverBD.vRandomEnd.x = 9000
		ELIF iRand = 2
			serverBD.vRandomEnd.x = -9000
		ENDIF
		iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
		IF iRand = 1
			serverBD.vRandomEnd.y = 9000
		ELIF iRand = 2
			serverBD.vRandomEnd.y = -9000
		ENDIF
		IF serverBD.vRandomEnd.x = 0
		AND serverBD.vRandomEnd.y = 0
			serverBD.vRandomEnd = <<-9000, 5000, 500>>
		ENDIF
		NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - RANDOM END DESTINATION = ") NET_PRINT_VECTOR(serverBD.vRandomEnd) NET_NL()
	ENDIF
	
	bInTutorialSession = NETWORK_IS_IN_TUTORIAL_SESSION()
	
	//SCRIPT_PLAYSTATS_MISSION_STARTED(GET_MISSION_NAME_FROM_TYPE(FMMC_TYPE_AMMO_DROP))
	//Stops array over run: 1432766
	IF NETWORK_IS_GAME_IN_PROGRESS()
		playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_INI
	ENDIF
	NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - PRE_GAME DONE - hh     <----------     ") NET_NL()
	
	RETURN TRUE
ENDFUNC

PROC REIMBURST_PLAYER_FOR_BALLISTIC_DROP()
	IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niCrate)
		PRINTLN("REIMBURST_PLAYER_FOR_BALLISTIC_DROP()")			
		IF g_sMPTunables.iballisticsuitcostdelivery > 0
			GIVE_LOCAL_PLAYER_CASH(g_sMPTunables.iballisticsuitcostdelivery)
			NETWORK_REFUND_CASH_TYPE(g_sMPTunables.iballisticsuitcostdelivery, MP_REFUND_TYPE_AMMO_DROP_DELIVERY_FAIL, MP_REFUND_REASON_DELIVERY_FAIL, TRUE)
		ENDIF
	ENDIF
ENDPROC

//Player leaves area
FUNC BOOL MISSION_END_CHECK()
	
	/*IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_RACE
	OR GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_DEATHMATCH
	OR GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_SURVIVAL
		NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - MISSION END - SERVER IN RACE, DEATHMATCH, or SURVIVAL ") NET_NL()
		RETURN TRUE
	ENDIF*/
	
	//Wait till player is alive before cleaning up
	IF IS_BIT_SET(iBoolsBitSet, biLandAnimDone)
		/*IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
			NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - MISSION END - SERVER IS NOT OKAY ") NET_NL()
			RETURN TRUE
		ENDIF*/
		IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_DoDeathCleanup)
				SET_BIT(serverBD.iServerBitSet, biS_DoDeathCleanup)
				NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - MISSION END - biS_DoDeathCleanup SET") NET_NL()
			ENDIF
			RETURN FALSE
		ELSE
			IF IS_BIT_SET(serverBD.iServerBitSet, biS_DoDeathCleanup)
				NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - MISSION END - PLAYER DIED") NET_NL()
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
//	IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
//		NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - MISSION END - SERVER IS NOT OKAY ") NET_NL()
//		RETURN TRUE
//	ENDIF
	
	IF serverBD.eAmmoDropStage = eAD_DROP_AMMO
	OR serverBD.eAmmoDropStage = eAD_PICKUP_AMMO
		//Leave Area
		/*IF IS_BIT_SET(serverBD.iServerBitSet, biS_AllCrewLeftStartArea)
			NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - MISSION END - CREW NOT IN AREA     <----------     ") NET_NL()
			RETURN TRUE
		ENDIF*/
		
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niCrate)
			//Plane is dead
			IF NOT IS_NET_VEHICLE_DRIVEABLE(serverBD.niPlane)
				NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - MISSION END - PLANE NOT DRIVEABLE     <----------     ") NET_NL()
				RETURN TRUE
			ENDIF
			
			//Pilot is dead
			IF IS_NET_PED_INJURED(serverBD.niPilot)
				NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - MISSION END - PILOT DEAD     <----------     ") NET_NL()
				RETURN TRUE
			ENDIF
			
			//Crate Gone
			IF serverBD.eAmmoDropStage > eAD_DROP_AMMO
				NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - MISSION END - CRATE GONE     <----------     ") NET_NL()
				RETURN TRUE
			ENDIF
		ENDIF
		
		// Make the player end the mission if they leave the Area
		IF IS_BIT_SET(iBoolsBitSet, biLandAnimDone)	//serverBD.eAmmoDropStage = eAD_PICKUP_AMMO
			IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), serverBD.vDropLocation, <<LEAVE_RANGE, LEAVE_RANGE, LEAVE_RANGE>>)
				NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - MISSION END - SERVER NOT IN AREA     <----------     ") NET_NL()
				
				IF serverBD.bisballisticdrop = TRUE
					REIMBURST_PLAYER_FOR_BALLISTIC_DROP()
				
				ENDIF
				RETURN TRUE
			ENDIF
			IF serverBD.bisballisticdrop = TRUE
				IF (IS_PLAYER_PERMANENT_PARTICIPANT_TO_ANY_EVENT(PLAYER_ID()) AND FM_EVENT_GET_SESSION_ACTIVE_FM_EVENT() != FMMC_TYPE_GB_TERMINATE)
				OR IS_FM_PLAYER_CRITICAL_TO_THIS_FM_EVENT(PLAYER_ID(), FMMC_TYPE_HUNT_THE_BEAST)
				OR GET_IS_LOCAL_PARTICIPANT_DOING_HUNT_THE_BEAST_OPT_IN_COUNTDOWN()
				OR IS_MP_PASSIVE_MODE_ENABLED()
					
					NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - MISSION END - In transition mode or player in launching event  <----------     ") NET_NL()
					REIMBURST_PLAYER_FOR_BALLISTIC_DROP()
				
					IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niCrate)
						DELETE_NET_ID(serverBD.niCrate)
					ENDIF
					RETURN TRUE
				
				ENDIF
				
			ENDIF
		ENDIF
	ENDIF
		
	RETURN FALSE
ENDFUNC

//Server only function that checks to see if the game mode should now end. For this test script, it if time has passed.
FUNC BOOL HAVE_MISSION_END_CONDITIONS_BEEN_MET()
	
	#IF IS_DEBUG_BUILD
		IF bHostEndMissionNow
			RETURN TRUE
		ENDIF
	#ENDIF	
	
	IF MISSION_END_CHECK()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


//Helper function to get a clients game/mission state
FUNC INT GET_CLIENT_MISSION_STATE(INT iPlayer)
	RETURN playerBD[iPlayer].iGameState
ENDFUNC

//Helper function to get the servers game/mission state
FUNC INT GET_SERVER_MISSION_STATE()
	RETURN serverBD.iServerGameState
ENDFUNC

#IF IS_DEBUG_BUILD
PROC CREATE_WIDGETS()
	INT iPlayer
	TEXT_LABEL_15 tl63

	START_WIDGET_GROUP("AMMO DROP")  
		
		ADD_WIDGET_BOOL("Warp to Ammo Crate", bWarpToEnd)
		ADD_WIDGET_INT_READ_ONLY("Ammo Clips", AMMO_CLIPS)
		ADD_WIDGET_FLOAT_SLIDER("CRATE_DROP_SPEED", CRATE_DROP_SPEED, -100, 0, 0.5)
		
		START_WIDGET_GROUP("Entity Reservations")
			ADD_WIDGET_INT_READ_ONLY("Vehicles Reserved", serverBD.iReservedVehicles)
			ADD_WIDGET_INT_READ_ONLY("Peds Reserved", serverBD.iReservedPeds)
			ADD_WIDGET_INT_READ_ONLY("Objects Reserved", serverBD.iReservedObjects)
		STOP_WIDGET_GROUP()	
		
		// Gameplay debug, sever only.
		START_WIDGET_GROUP("Server Only Gameplay") 
			ADD_WIDGET_BOOL("End mission", bHostEndMissionNow)			
		STOP_WIDGET_GROUP()		
		
		// Data about the server
		START_WIDGET_GROUP("Server BD") 
				ADD_WIDGET_INT_SLIDER("S. Game state", serverBD.iServerGameState,-1, HIGHEST_INT,1)
		STOP_WIDGET_GROUP()	

		// Data about the clients. * = You.
		START_WIDGET_GROUP("Client BD")  				
			REPEAT 2 iPlayer
				tl63 = "Player "
				tl63 += iPlayer
				IF iPlayer = PARTICIPANT_ID_TO_INT()
					tl63 += "*"
				ENDIF
				START_WIDGET_GROUP(tl63)
					ADD_WIDGET_INT_SLIDER("Game state", playerBD[iPlayer].iGameState,-1, HIGHEST_INT,1)
				STOP_WIDGET_GROUP()
			ENDREPEAT				
		STOP_WIDGET_GROUP()	
		
	STOP_WIDGET_GROUP()
ENDPROC		

PROC UPDATE_WIDGETS()
	
	//Warp player to the ammo crate
	IF bWarpToEnd = TRUE
		IF playerBD[PARTICIPANT_ID_TO_INT()].eAmmoDropStage = eAD_PICKUP_AMMO
			IF NET_WARP_TO_COORD(serverBD.vDropLocation+<<0, 0, 1>>, 0, TRUE, FALSE)
				NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - UPDATE_WIDGETS - bWarpToEnd DONE    <----------     ") NET_NL()
				bWarpToEnd = FALSE
			ELSE
				NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - UPDATE_WIDGETS - bWarpToEnd IN PROGRESS    <----------     ") NET_NL()
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

#ENDIF

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  ENTITY CREATION                  /////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////

//PURPOSE: Creates the required sequences for the script
PROC CREATE_SEQUENCES()
	IF NOT IS_BIT_SET(iBoolsBitSet, biSequencesCreated)
		IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niPlane)
			OPEN_SEQUENCE_TASK(seqPlane)
				TASK_VEHICLE_DRIVE_TO_COORD(NULL, NET_TO_VEH(serverBD.niPlane), serverBD.vDropLocation+<<0, 0, PLANE_DESTINATION_HEIGHT>>, PLANE_SPEED, DRIVINGSTYLE_NORMAL, CUBAN800, DRIVINGMODE_PLOUGHTHROUGH, 15, -1)
				TASK_VEHICLE_DRIVE_TO_COORD(NULL, NET_TO_VEH(serverBD.niPlane), serverBD.vRandomEnd, PLANE_SPEED, DRIVINGSTYLE_NORMAL, CUBAN800, DRIVINGMODE_PLOUGHTHROUGH, -1, -1)
			CLOSE_SEQUENCE_TASK(seqPlane)
			
			SET_BIT(iBoolsBitSet, biSequencesCreated)
			NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - CREATE_SEQUENCES - DONE ") NET_NL()
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Clears the sequences created for this script
PROC CLEAR_SEQUENCES()
	IF IS_BIT_SET(iBoolsBitSet, biSequencesCreated)
		CLEAR_SEQUENCE_TASK(seqPlane)
		NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - CLEAR_SEQUENCES - DONE ") NET_NL()
	ENDIF
ENDPROC

/// PURPOSE:
///    Gets a random vector within a donut centred at vPos with a radius of fRadius and a height of fHeight.
/// PARAMS:
///    vPos - The sphere disc
///    fInnerRadius - Radius of the hole in the middle
///    fOuterRadius - Radius of the donut
///    fHeight - Height of the disc (goes both above and below vPos)
/// RETURNS:
///    A random position vector within fRadius of vPos.
FUNC VECTOR GET_RANDOM_POINT_IN_DONUT(VECTOR vPos, FLOAT fInnerRadius, FLOAT fOuterRadius, FLOAT fHeight)
 
	// Pick a random unit direction vector.
	VECTOR vDir = << GET_RANDOM_FLOAT_IN_RANGE(-1,1) , GET_RANDOM_FLOAT_IN_RANGE(-1,1), 0 >>
	FLOAT fHalfHeight = fHeight / 2.0
	
	// Project this vector into the disc some random distance.
	vDir = GET_VECTOR_OF_LENGTH(vDir, GET_RANDOM_FLOAT_IN_RANGE(fInnerRadius, fOuterRadius))
	
	// Grab a random height and add it on.
	vDir.z = GET_RANDOM_FLOAT_IN_RANGE(-fHalfHeight, fHalfHeight)
	
	RETURN vPos + vDir
ENDFUNC

//PURPOSE: Calculates a position to Create the plane at
PROC GET_PLANE_CREATION_POINT(VECTOR &vPos, FLOAT &fHeading)
	vPos = GET_RANDOM_POINT_IN_DONUT(serverBD.vDropLocation+<<0, 0, PLANE_CREATION_HEIGHT>>, PLANE_CREATION_RADIUS/2, PLANE_CREATION_RADIUS, PLANE_CREATION_HEIGHT/2)
	fHeading = GET_HEADING_FROM_VECTOR_2D(serverBD.vDropLocation.x-vPos.x, serverBD.vDropLocation.y-vPos.y) //GET_HEADING_BETWEEN_VECTORS_2D(vPos, serverBD.vDropLocation)
	
	FLOAT fApproxHeight = GET_APPROX_HEIGHT_FOR_AREA(vPos.x-PLANE_CREATION_RADIUS, vPos.y-PLANE_CREATION_RADIUS, vPos.x+PLANE_CREATION_RADIUS, vPos.y+PLANE_CREATION_RADIUS)
	IF vPos.z < fApproxHeight
		vPos.z = fApproxHeight
		NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - GET_PLANE_CREATION_POINT - USE APPROX HEIGHT") NET_NL()
	ENDIF
	
	NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - GET_PLANE_CREATION_POINT - vPos = ") NET_PRINT_VECTOR(vPos) NET_NL()
ENDPROC

///////////////////////////////////////////
///    		PLANE AND PILOT    			///
///////////////////////////////////////////
//PURPOSE: Create the Plane
FUNC BOOL CREATE_PLANE()
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niPlane)
		IF REQUEST_LOAD_MODEL(CUBAN800)
			
			VECTOR vPos
			FLOAT fHeading
			GET_PLANE_CREATION_POINT(vPos, fHeading)
			
			IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(vPos, 6, 1, 1, 5)
							
				IF CREATE_NET_VEHICLE(serverBD.niPlane, CUBAN800, vPos, fHeading, TRUE, TRUE, TRUE)	//FALSE, FALSE, FALSE)	
					//serverBD.niPlane = VEH_TO_NET(CREATE_VEHICLE(CUBAN800, vPos, fHeading, FALSE, FALSE))
				
					//SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(NET_TO_VEH(serverBD.niPlane), TRUE)
					//SET_VEHICLE_TYRES_CAN_BURST(NET_TO_VEH(serverBD.niPlane), FALSE)
					
					SET_VEHICLE_DOORS_LOCKED(NET_TO_VEH(serverBD.niPlane), VEHICLELOCK_LOCKED)
					
					FREEZE_ENTITY_POSITION(NET_TO_VEH(serverBD.niPlane), FALSE)
					SET_ENTITY_DYNAMIC(NET_TO_VEH(serverBD.niPlane), TRUE)
					ACTIVATE_PHYSICS(NET_TO_VEH(serverBD.niPlane))
					SET_VEHICLE_FORWARD_SPEED(NET_TO_VEH(serverBD.niPlane), PLANE_SPEED)     
					SET_HELI_BLADES_FULL_SPEED(NET_TO_VEH(serverBD.niPlane))
					SET_VEHICLE_ENGINE_ON(NET_TO_VEH(serverBD.niPlane), TRUE, TRUE)
					SET_VEHICLE_ENGINE_CAN_DEGRADE(NET_TO_VEH(serverBD.niPlane), FALSE)
					
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(NET_TO_VEH(serverBD.niPlane), FALSE)
															
					CONTROL_LANDING_GEAR(NET_TO_VEH(serverBD.niPlane), LGC_RETRACT_INSTANT)
					OPEN_BOMB_BAY_DOORS(NET_TO_VEH(serverBD.niPlane))
					
					NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - CREATED PLANE ") NET_PRINT_VECTOR(vPos) NET_NL()
				
					IF serverBD.bisballisticdrop = TRUE
						SET_ENTITY_PROOFS(NET_TO_VEH(serverBD.niPlane), TRUE, FALSE, TRUE, FALSE, FALSE, FALSE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//Check we have created the vehicle
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niPlane)
		NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - FAILING ON PLANE CREATION") NET_NL()
		RETURN FALSE
	ENDIF
	
	SET_MODEL_AS_NO_LONGER_NEEDED(CUBAN800)
	RETURN TRUE
ENDFUNC

//PURPOSE: Clears the planes task
PROC CLEAR_PLANE_TASK()
	IF NOT IS_NET_PED_INJURED(serverBD.niPilot)
		CLEAR_PED_TASKS(NET_TO_PED(serverBD.niPilot))
		NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - CLEAR_PLANE_TASK - DONE ") NET_NL()
	ENDIF
ENDPROC

//PURPOSE: Create the Pilot
FUNC BOOL CREATE_PILOT()
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niPilot)
	AND REQUEST_LOAD_MODEL(S_M_M_PILOT_02)
	AND NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niPlane)
		IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niPlane)
			IF CREATE_NET_PED_IN_VEHICLE(serverBD.niPilot, serverBD.niPlane, PEDTYPE_CRIMINAL, S_M_M_PILOT_02, VS_DRIVER)
			
			//serverBD.niPilot = PED_TO_NET(CREATE_PED_INSIDE_VEHICLE(NET_TO_VEH(serverBD.niPlane), PEDTYPE_CRIMINAL, S_M_M_PILOT_02, VS_DRIVER, FALSE, FALSE))
			
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NET_TO_PED(serverBD.niPilot), TRUE)
				SET_PED_RELATIONSHIP_GROUP_HASH(NET_TO_PED(serverBD.niPilot), rgFM_AiLike)
				SET_PED_RANDOM_COMPONENT_VARIATION(NET_TO_PED(serverBD.niPilot))
				
				SET_PED_KEEP_TASK(NET_TO_PED(serverBD.niPilot), TRUE)
				
				SET_ENTITY_HEALTH(NET_TO_PED(serverBD.niPilot), ROUND(200*g_sMPTunables.fAiHealthModifier))
				
				CREATE_SEQUENCES()
				TASK_PERFORM_SEQUENCE(NET_TO_PED(serverBD.niPilot), seqPlane)
				SET_TASK_VEHICLE_GOTO_PLANE_MIN_HEIGHT_ABOVE_TERRAIN(NET_TO_VEH(serverBD.niPlane), ROUND(PLANE_DESTINATION_HEIGHT))
				
				NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - CREATED PILOT ") NET_NL()
			ENDIF
		ENDIF
	ENDIF
	
	//Check we have created the ped
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niPilot)
		NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - FAILING ON PILOT CREATION") NET_NL()
		RETURN FALSE
	ENDIF
	
	SET_MODEL_AS_NO_LONGER_NEEDED(S_M_M_PILOT_02)
	RETURN TRUE
ENDFUNC

//PURPOSE: Creates the Plane and Pilot that will deliver the Ammo Crate
FUNC BOOL CREATE_PLANE_AND_PILOT()
	//IF CAN_REGISTER_MISSION_ENTITIES(1, 1, 0, 0)
	IF REQUEST_LOAD_MODEL(CUBAN800)
	AND REQUEST_LOAD_MODEL(S_M_M_PILOT_02)
		IF HAS_NET_TIMER_EXPIRED(iStartDelayTimer, START_DELAY_TIME)
			IF CREATE_PLANE()
			AND CREATE_PILOT()
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC




///////////////////////////////////////////
///    		CRATE AND CHUTE    			///
///////////////////////////////////////////

FUNC BOOL LOAD_CRATE_ANIMS()
	REQUEST_ANIM_DICT("P_cargo_chute_S")
		
	IF HAS_ANIM_DICT_LOADED("P_cargo_chute_S")
		//NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - LOAD_CRATE_ANIMS - DONE ") NET_NL()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Loads the Crate assets
FUNC BOOL LOAD_CRATE_ASSETS()
	MODEL_NAMES model_crate_drop = PROP_BOX_AMMO02A
	IF serverBD.bisballisticdrop = TRUE
		model_crate_drop = ex_prop_adv_case_sm
	ENDIF
	REQUEST_MODEL(model_crate_drop)
	REQUEST_MODEL(P_CARGO_CHUTE_S)
	
	IF HAS_MODEL_LOADED(model_crate_drop)
	AND HAS_MODEL_LOADED(P_CARGO_CHUTE_S)
	AND LOAD_CRATE_ANIMS()
		//NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - LOAD_CRATE_ASSETS - DONE ") NET_NL()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//Loads all particle effects required
FUNC BOOL LOAD_INTERACT_PARTICLE_FX()
	REQUEST_PTFX_ASSET()

	IF HAS_PTFX_ASSET_LOADED()
		//NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - LOAD_INTERACT_PARTICLE_FX - DONE ") NET_NL()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Add a beacon fx to a crate
FUNC BOOL ADD_CRATE_BEACON()
	IF LOAD_INTERACT_PARTICLE_FX()
		IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(ptfxBeacon)
			ptfxBeacon = START_NETWORKED_PARTICLE_FX_LOOPED_ON_ENTITY("scr_crate_drop_beacon", NET_TO_OBJ(serverBD.niCrate), <<0, 0, 0.2>>, <<0, 0, 0>>)
			SET_PARTICLE_FX_LOOPED_COLOUR(ptfxBeacon, 0.8, 0.18, 0.19)
			NET_PRINT_TIME() NET_PRINT("     ---------->    AMMO DROP - ADD_CRATE_BEACON ")  NET_NL()
		ENDIF
	ENDIF
	
	//Check flares have been created
	IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(ptfxBeacon)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

//PURPOSE: Removes a Crate Beacon
PROC CLEANUP_CRATE_BEACON()
	IF ptfxBeacon != NULL
		IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxBeacon)
			STOP_PARTICLE_FX_LOOPED(ptfxBeacon)
			ptfxBeacon = NULL
			
			NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - CLEANUP_CRATE_BEACON ")  NET_NL()
		ELSE
			NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - CLEANUP_CRATE_BEACON - FX DOESN'T EXIST ")  NET_NL()
		ENDIF
	ELSE
			NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - CLEANUP_CRATE_BEACON - FX IS NULL ")  NET_NL()
	ENDIF
ENDPROC

//PURPOSE: Cleans up the bleeping sound coming from the crate
PROC CLEANUP_CRATE_BLEEP_SOUND()
	IF GET_SOUND_ID_FROM_NETWORK_ID(serverBD.iBleepSoundID) != -1
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niCrate)
			IF NOT HAS_SOUND_FINISHED(GET_SOUND_ID_FROM_NETWORK_ID(serverBD.iBleepSoundID))
				STOP_SOUND(GET_SOUND_ID_FROM_NETWORK_ID(serverBD.iBleepSoundID))
			ENDIF
			RELEASE_SOUND_ID(GET_SOUND_ID_FROM_NETWORK_ID(serverBD.iBleepSoundID))
			NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - CLEANUP_CRATE_BLEEP_SOUND - SOUND FINISHED ")  NET_NL()
		ELSE
			IF NOT HAS_SOUND_FINISHED(GET_SOUND_ID_FROM_NETWORK_ID(serverBD.iBleepSoundID))
				STOP_SOUND(GET_SOUND_ID_FROM_NETWORK_ID(serverBD.iBleepSoundID))
			ENDIF
			RELEASE_SOUND_ID(GET_SOUND_ID_FROM_NETWORK_ID(serverBD.iBleepSoundID))
			NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - CLEANUP_CRATE_BLEEP_SOUND - CRATE NO LONGER EXISTS ")  NET_NL()
		ENDIF
	ENDIF
ENDPROC

PROC GB_ENABLE_PICKUP_FOR_PLAYER_ONLY(OBJECT_INDEX oiPickup)
	PRINTLN("GB_ENABLE_PICKUP_FOR_PLAYER_ONLY FOR BLOCK THIS PLAYER 1")
	
	INT iPlayerFlags
	
	SET_BIT(iPlayerFlags, NETWORK_PLAYER_ID_TO_INT())
	
	BLOCK_PLAYERS_FOR_AMBIENT_PICKUP(oiPickup, iPlayerFlags)
ENDPROC



/*
FUNC BOOL IS_THIS_PICKUP_NETWORKED(object_index theobject)
	IF serverBD.bisballisticdrop = TRUE
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC


FUNC object_index GET_CRATE_DROP_PICKUP_HANDLE(object_index theobject)
	IF IS_THIS_PICKUP_NETWORKED(object_index theobject)
		RETURN serverBD.niCrate
	ELSE
		RETURN NET_TO_OBJ(serverBD.niCrate) 
	ENDIF
	

ENDFUNC
*/
//Creates and drops a weapons cache from the plane
FUNC BOOL CREATE_DROPPED_CRATE()
	MODEL_NAMES model_crate_drop = PROP_BOX_AMMO02A
	IF serverBD.bisballisticdrop = TRUE
		model_crate_drop = ex_prop_adv_case_sm
	ENDIF
	REQUEST_MODEL(model_crate_drop)
	REQUEST_MODEL(P_CARGO_CHUTE_S)
		
	IF HAS_MODEL_LOADED(model_crate_drop)
	AND HAS_MODEL_LOADED(P_CARGO_CHUTE_S)
				
		//Create the Crate
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niCrate)
		AND NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niChute)
			IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niPlane)
			AND CAN_REGISTER_MISSION_ENTITIES(0, 0, 2, 0)						
				
				//Create Cache
				//IF CREATE_NET_OBJ(serverBD.niCrate, model_crate_drop, (GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niPlane))-<<0, 0, 3>>))	//, FALSE)
				//	//SET_NETWORK_ID_EXISTS_ON_ALL_MACHINES(playerBD[PARTICIPANT_ID_TO_INT()].CacheNetID[i], TRUE)

				
				INT iAmmoAmount = 200
					
				IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niCrate)	
					//serverBD.niCrate = OBJ_TO_NET(CREATE_PORTABLE_PICKUP(PICKUP_PORTABLE_CRATE_UNFIXED, (GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niPlane))-<<0, 0, 3>>), FALSE, model_crate_drop)) //PROP_DRUG_PACKAGE))
					INT iPlacementFlags = 0
					PICKUP_TYPE pickuptypeammodrop = PICKUP_AMMO_BULLET_MP
					IF serverBD.bisballisticdrop = TRUE
						PRINTLN(" AMMO DROP CREATE_DROPPED_CRATE this is a ballistic drop serverBD.bisballisticdrop = TRUE ")
						pickuptypeammodrop = PICKUP_WEAPON_MINIGUN
					ENDIF
					
					IF serverBD.bisballisticdrop = FALSE
						SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_PLAYER_GIFT))
					ELSE
						//SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_MAP))
						//SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_LOCAL_ONLY))
						SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_PLAYER_GIFT))
						PRINTLN(" AMMO DROP CREATE_DROPPED_CRATE this is a ballistic Setting flags ")
					ENDIF
					
					
					serverBD.niCrate = OBJ_TO_NET(CREATE_AMBIENT_PICKUP(pickuptypeammodrop, (GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niPlane))-<<0, 0, 3>>), iPlacementFlags, iAmmoAmount, model_crate_drop, TRUE))
					IF serverBD.bisballisticdrop = TRUE
					GB_ENABLE_PICKUP_FOR_PLAYER_ONLY(NET_TO_OBJ(serverBD.niCrate))
					SET_ONLY_ALLOW_AMMO_COLLECTION_WHEN_LOW(FALSE)
					ENDIF
					//serverBD.niCrate = OBJ_TO_NET(CREATE_PICKUP(PICKUP_AMMO_BULLET_MP, (GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niPlane))-<<0, 0, 3>>), 0, 50, TRUE, model_crate_drop))
					//SET_NETWORK_ID_EXISTS_ON_ALL_MACHINES(serverBD.niCrate, TRUE)	//FALSE)
					
					SET_ENTITY_INVINCIBLE(NET_TO_OBJ(serverBD.niCrate), TRUE)
					SET_OBJECT_FORCE_VEHICLES_TO_AVOID(NET_TO_OBJ(serverBD.niCrate), TRUE)
					
					SET_ACTIVATE_OBJECT_PHYSICS_AS_SOON_AS_IT_IS_UNFROZEN(NET_TO_OBJ(serverBD.niCrate), TRUE)
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(NET_TO_OBJ(serverBD.niCrate), FALSE)
					ACTIVATE_PHYSICS(NET_TO_OBJ(serverBD.niCrate))
					//SET_ENTITY_VELOCITY(NET_TO_OBJ(serverBD.niCrate), <<0, 0, -0.2>>)
					
					CLEANUP_CRATE_BLEEP_SOUND()
					INT soundID = GET_SOUND_ID()
					PLAY_SOUND_FROM_ENTITY(soundID, "Crate_Beeps", NET_TO_OBJ(serverBD.niCrate), "MP_CRATE_DROP_SOUNDS", TRUE, 0)
					serverBD.iBleepSoundID = GET_NETWORK_ID_FROM_SOUND_ID(soundID)
					
					NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - CREATED CRATE ")  NET_PRINT(" AT POS ") NET_PRINT_VECTOR((GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niPlane))-<<0, 0, 3>>)) NET_NL()
							
					
				ENDIF
				
				//Create Chute
				IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niCrate)
				AND NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niChute)	
					IF CREATE_NET_OBJ(serverBD.niChute, P_CARGO_CHUTE_S, (GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niPlane))-<<0, 0, 2>>))	//, FALSE)
						//serverBD.niChute = OBJ_TO_NET(CREATE_OBJECT(P_CARGO_CHUTE_S, (GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niPlane))-<<0, 0, 2>>), TRUE, TRUE))//FALSE, FALSE))
						SET_NETWORK_ID_EXISTS_ON_ALL_MACHINES(serverBD.niChute, TRUE)
						
						ATTACH_ENTITY_TO_ENTITY(NET_TO_OBJ(serverBD.niChute), NET_TO_OBJ(serverBD.niCrate), 0, <<0, 0, 0.10>>, <<0, 0, 0>>)
						SET_ACTIVATE_OBJECT_PHYSICS_AS_SOON_AS_IT_IS_UNFROZEN(NET_TO_OBJ(serverBD.niChute), TRUE)
						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(NET_TO_OBJ(serverBD.niChute), FALSE)
						NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - CREATED CHUTE " ) NET_NL()
						
						PLAY_ENTITY_ANIM(NET_TO_OBJ(serverBD.niChute), "P_cargo_chute_S_deploy", "P_cargo_chute_S", INSTANT_BLEND_IN, FALSE, FALSE)
						FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(NET_TO_OBJ(serverBD.niChute))
						NET_PRINT_TIME() NET_PRINT("     ---------->    AMMO DROP - DEPLOY ANIM ") NET_PRINT("P_cargo_chute_S_deploy") NET_PRINT("STARTED - ")  NET_NL()
					ENDIF
				ENDIF
				
				ADD_CRATE_BEACON()
				
				//Delivery Line
				IF NOT IS_BIT_SET(iBoolsBitSet, biPilotIncomingLineDone)
					IF NOT serverBD.bisballisticdrop
					ADD_PED_FOR_DIALOGUE(sSpeech, 3, NET_TO_PED(serverBD.niPilot), "FM_Pilot_Ammo")
					CREATE_CONVERSATION_USING_V_CONTENT_IN_DLC(sSpeech, "CT_AUD", "MPCT_AMOinc", CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN, TRUE)
					ENDIF
					SET_BIT(iBoolsBitSet, biPilotIncomingLineDone)
					NET_PRINT_TIME() NET_PRINT("     ---------->    AMMO DROP - biPilotIncomingLineDone SET ") NET_NL()
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niCrate)
	AND NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niChute)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Creates the Destructible Crate
FUNC BOOL CREATE_DESTRUCTIBLE_CRATE()
		
	REQUEST_MODEL(serverBD.DCrateData.Model)
	
	IF HAS_MODEL_LOADED(serverBD.DCrateData.Model)
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.DCrateData.NetID)
		AND NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niCrate)
			IF CAN_REGISTER_MISSION_OBJECTS(1)
				
				IF CREATE_NET_OBJ(serverBD.DCrateData.NetID, serverBD.DCrateData.Model, GET_ENTITY_COORDS(NET_TO_OBJ(serverBD.niCrate))-<<0, 0, 5>>, TRUE, TRUE, TRUE, FALSE)
					SET_NETWORK_ID_EXISTS_ON_ALL_MACHINES(serverBD.DCrateData.NetID, TRUE)
					SET_ENTITY_HEADING(NET_TO_OBJ(serverBD.DCrateData.NetID), GET_ENTITY_HEADING(NET_TO_OBJ(serverBD.niCrate)))
					
					ATTACH_ENTITY_TO_ENTITY(NET_TO_OBJ(serverBD.niCrate), NET_TO_OBJ(serverBD.DCrateData.NetID), 0, <<0, 0, 0.25>>, <<0, 0, 0>>, FALSE, FALSE, TRUE)
					SET_ACTIVATE_OBJECT_PHYSICS_AS_SOON_AS_IT_IS_UNFROZEN(NET_TO_OBJ(serverBD.DCrateData.NetID), TRUE)
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(NET_TO_OBJ(serverBD.DCrateData.NetID), FALSE)
					ACTIVATE_PHYSICS(NET_TO_OBJ(serverBD.DCrateData.NetID))
					SET_ENTITY_VELOCITY(NET_TO_OBJ(serverBD.DCrateData.NetID), <<0, 0, -0.2>>)
					
					//SET_ENTITY_PROOFS(NET_TO_OBJ(serverBD.DCrateData.NetID), FALSE, FALSE, FALSE, TRUE, FALSE, FALSE)
					SET_DISABLE_BREAKING(NET_TO_OBJ(serverBD.DCrateData.NetID), TRUE)
					
					//FREEZE_ENTITY_POSITION(NET_TO_OBJ(serverBD.SafeData.NetID), TRUE)
					//RETAIN_ENTITY_IN_INTERIOR(NET_TO_OBJ(serverBD.SafeData.NetID), GET_INTERIOR_AT_COORDS(SafeCrackData.vSafeCoord))
					NET_PRINT_TIME() NET_PRINT("     ---------->    AMMO DROP - CREATE_DESTRUCTIBLE_CRATE BODY DONE") NET_NL()
				ENDIF
				
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.DCrateData.NetID)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

//PURPOSE: Adds a blip to a crate
PROC ADD_CRATE_BLIP()
	IF NOT DOES_BLIP_EXIST(AmmoCrateBlip)
		AmmoCrateBlip = ADD_BLIP_FOR_ENTITY(NET_TO_OBJ(serverBD.niCrate))

		IF serverBD.bisballisticdrop
			
			SET_BLIP_SPRITE(AmmoCrateBlip, RADAR_TRACE_RAMPAGE)
			SET_BLIP_SCALE(AmmoCrateBlip, BLIP_SIZE_NETWORK_PICKUP_LARGE)
			SET_BLIP_NAME_FROM_TEXT_FILE(AmmoCrateBlip, "AMD_BLIPBALL")
		ELSE
			SET_BLIP_SPRITE(AmmoCrateBlip, RADAR_TRACE_GANG_ATTACK_PACKAGE)
			SET_BLIP_NAME_FROM_TEXT_FILE(AmmoCrateBlip, "AMD_BLIPN")
			SET_BLIP_SCALE(AmmoCrateBlip, BLIP_SIZE_NETWORK_PICKUP)
		ENDIF
		
		
		SET_BLIP_COLOUR(AmmoCrateBlip, BLIP_COLOUR_GREEN)
		//SHOW_HEIGHT_ON_BLIP(AmmoCrateBlip, TRUE)
		SET_BLIP_ALPHA(AmmoCrateBlip, 120)
		NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - ADD_CRATE_BLIP ") NET_NL()
	ENDIF
ENDPROC

//PURPOSE: Controls the display of help text
PROC CONTROL_HELP_TEXT()
	IF NOT IS_BIT_SET(iBoolsBitSet, biInitialHelpDone)
		IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
			INT iStat = GET_MP_INT_CHARACTER_STAT(MP_STAT_TUTORIAL_BITSET)
			IF NOT IS_BIT_SET(iStat, TUTBS_AMMO_DROP_HELP_1)
				PRINT_HELP("AMD_HELP1")	//~s~The Ammo Crate ~BLIP_GANG_ATTACK_PACKAGE~ will be dropped at your GPS location.~n~Be sure to make it a secluded location to stop other players stealing it.
				SET_BIT(iStat, TUTBS_AMMO_DROP_HELP_1)
				SET_MP_INT_CHARACTER_STAT(MP_STAT_TUTORIAL_BITSET, iStat)
				NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - TUTBS_AMMO_DROP_HELP_1 ") NET_NL()
				SET_BIT(iBoolsBitSet, biInitialHelpDone)
			ELIF NOT IS_BIT_SET(iStat, TUTBS_AMMO_DROP_HELP_2)
				PRINT_HELP("AMD_HELP2")	//~s~Ammo Crates will add ammo to your equipped weapon.~n~You won't pick up the Ammo Crate if you need no ammo for your current weapon.
				SET_BIT(iStat, TUTBS_AMMO_DROP_HELP_2)
				SET_MP_INT_CHARACTER_STAT(MP_STAT_TUTORIAL_BITSET, iStat)
				NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - TUTBS_AMMO_DROP_HELP_2 ") NET_NL()
				SET_BIT(iBoolsBitSet, biInitialHelpDone)
			ELSE
				SET_BIT(iBoolsBitSet, biInitialHelpDone)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CONTROL_HELP_TEXT_BALLISTIC()
	IF NOT IS_BIT_SET(iBoolsBitSet, biInitialHelpDoneBall)
		IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
			INT iStat = GET_MP_INT_CHARACTER_STAT(MP_STAT_TUTORIAL_BITSET)
			IF NOT IS_BIT_SET(iStat, TUTBS_BALL_DROP_HELP_1)
				PRINT_HELP("BALD_HELP1")	//~s~The Ammo Crate ~BLIP_GANG_ATTACK_PACKAGE~ will be dropped at your GPS location.~n~Be sure to make it a secluded location to stop other players stealing it.
				SET_BIT(iStat, TUTBS_BALL_DROP_HELP_1)
				SET_MP_INT_CHARACTER_STAT(MP_STAT_TUTORIAL_BITSET, iStat)
				NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - TUTBS_BALL_DROP_HELP_1 ") NET_NL()
				SET_BIT(iBoolsBitSet, biInitialHelpDoneBall)
			ELIF NOT IS_BIT_SET(iStat, TUTBS_BALL_DROP_HELP_2)
				PRINT_HELP("BALD_HELP2")	//~s~Ammo Crates will add ammo to your equipped weapon.~n~You won't pick up the Ammo Crate if you need no ammo for your current weapon.
				SET_BIT(iStat, TUTBS_BALL_DROP_HELP_2)
				SET_MP_INT_CHARACTER_STAT(MP_STAT_TUTORIAL_BITSET, iStat)
				NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - TUTBS_BALL_DROP_HELP_2 ") NET_NL()
				SET_BIT(iBoolsBitSet, biInitialHelpDoneBall)
			ELSE
				SET_BIT(iBoolsBitSet, biInitialHelpDoneBall)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

///////////////////////////////////////////
///    		DROPPING CRATES    			///
///////////////////////////////////////////
//PURPOSE: Controls when the crates should be dropped
FUNC BOOL CONTROL_CRATE_DROPPING()
	//Check if drop should be done
	IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_ReadyForDrop)
		IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niPlane)
			IF IS_ENTITY_AT_COORD(NET_TO_VEH(serverBD.niPlane), serverBD.vDropLocation, <<CRATE_DROP_START, CRATE_DROP_START, 600>>)
				SET_BIT(serverBD.iServerBitSet, biS_ReadyForDrop)
				NET_PRINT_TIME() NET_PRINT("     ---------->    AMMO DROP - CONTROL_CRATE_DROPPING - biS_ReadyForDrop SET ") NET_NL()
			ENDIF
		ENDIF
		
	//Do Drop
	ELSE
		IF LOAD_CRATE_ASSETS()
			IF CREATE_DROPPED_CRATE()
				IF CREATE_DESTRUCTIBLE_CRATE()
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE:
FUNC BOOL ROTATION_IS_TOO_FAR()
	FLOAT fRot = GET_ENTITY_PITCH(NET_TO_OBJ(serverBD.niChute))
	
	IF fRot > 10
	OR fRot < -10
		NET_PRINT_TIME() NET_PRINT("     ---------->    AMMO DROP - ROTATION_IS_TOO_FAR - PITCH") NET_NL()
		RETURN TRUE
	ENDIF
	
	fRot = GET_ENTITY_ROLL(NET_TO_OBJ(serverBD.niChute))
	IF fRot > 10
	OR fRot < -10
		NET_PRINT_TIME() NET_PRINT("     ---------->    AMMO DROP - ROTATION_IS_TOO_FAR - ROLL") NET_NL()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

///////////////////////////////////////////
///    		CRATE LANDING    			///
///////////////////////////////////////////

//PURPOSE: Plays an anim when the package lands
FUNC BOOL SHOULD_LAND_ANIM_START()
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.DCrateData.NetID)
	//OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.DCrateData.NetID) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
		BOOL bMoveOn
		IF NOT IS_ENTITY_IN_AIR(NET_TO_OBJ(serverBD.DCrateData.NetID))
			bMoveOn = TRUE
			PRINTLN("     ---------->    AMMO DROP - SHOULD_LAND_ANIM_START - DCRATE NOT IN AIR")
		ENDIF
		IF HAS_OBJECT_BEEN_BROKEN(NET_TO_OBJ(serverBD.DCrateData.NetID))
			bMoveOn = TRUE
			PRINTLN("     ---------->    AMMO DROP - SHOULD_LAND_ANIM_START - DCRATE BROKEN")
		ENDIF
		IF IS_ENTITY_IN_WATER(NET_TO_OBJ(serverBD.DCrateData.NetID))
			bMoveOn = TRUE
			PRINTLN("     ---------->    AMMO DROP - SHOULD_LAND_ANIM_START - DCRATE IN WATER")
		ENDIF
		IF ROTATION_IS_TOO_FAR()
			bMoveOn = TRUE
			PRINTLN("     ---------->    AMMO DROP - SHOULD_LAND_ANIM_START - DCRATE ROTATED TOO FAR")
		ENDIF
		
		IF bMoveOn = TRUE
			IF TAKE_CONTROL_OF_NET_ID(serverBD.DCrateData.NetID)
				SET_DISABLE_BREAKING(NET_TO_OBJ(serverBD.DCrateData.NetID), FALSE)
				NET_PRINT_TIME() NET_PRINT("     ---------->    AMMO DROP - SHOULD_LAND_ANIM_START - TRUE - A") NET_NL()
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC DRAW_MARKER_ABOVE_CRATE(NETWORK_INDEX oiTemp, HUD_COLOURS colour)
	VECTOR returnMin, returnMax
	FLOAT fOffset
	INT r, g, b, a
	
	GET_HUD_COLOUR(colour, r, g, b, a)
	
	fOffset = 0.5 
	
	GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(NET_TO_OBJ(oiTemp)), returnMin, returnMax)

	FLOAT fCentreHeight = (returnMax.z - returnMin.z)/2
	FLOAT fZdiff = returnMax.z - fCentreHeight
	
	//-- Dave W - z-offset from centre needs to be at least the max height of the vehicle.
	IF fOffset <= (fZdiff + 0.1)
		fOffset = fZdiff + 0.4
	ENDIF
	
	DRAW_MARKER(MARKER_ARROW, GET_ENTITY_COORDS(NET_TO_OBJ(oiTemp))+<<0,0,((returnMax.z - returnMin.z)/2) + fOffset>>, <<0,0,0>>, <<180,0,0>>, <<0.5,0.5,0.5>>, r, g, b, 100, TRUE, TRUE)
ENDPROC

//PURPOSE: Plays an anim when the package lands
PROC CONTROL_CRATE_LANDING()

	//CRATE CHECKS
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niCrate)
		
		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niCrate)
		OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.niCrate) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
			
			
			//CHUTE CHECKS
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niChute)
				
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niChute)
				OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.niChute) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
					
					IF TAKE_CONTROL_OF_NET_ID(serverBD.niCrate)
					AND TAKE_CONTROL_OF_NET_ID(serverBD.niChute)
						
						//Delete Chute when Crumple is done
						IF NOT IS_BIT_SET(iBoolsBitSet, biLandAnimDone)
							IF IS_BIT_SET(iBoolsBitSet, biLandAnimPlaying)
								//IF HAS_ENTITY_ANIM_FINISHED(NET_TO_OBJ(serverBD.niChute), "P_cargo_chute_S", "P_cargo_chute_S_crumple")
								IF NOT IS_ENTITY_PLAYING_ANIM(NET_TO_OBJ(serverBD.niChute), "P_cargo_chute_S", "P_cargo_chute_S_crumple")
									SET_BIT(iBoolsBitSet, biLandAnimDone)
									DELETE_NET_ID(serverBD.niChute)
									NET_PRINT_TIME() NET_PRINT("     ---------->    AMMO DROP - CONTROL_CRATE_LANDING - LAND ANIM DONE - CHUTE DELETED - ") NET_NL()
									IF serverBD.bisballisticdrop = TRUE
									GB_ENABLE_PICKUP_FOR_PLAYER_ONLY(NET_TO_OBJ(serverBD.niCrate))
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						//Check Crumple is playing
						IF NOT IS_BIT_SET(iBoolsBitSet, biLandAnimPlaying)
							IF IS_BIT_SET(iBoolsBitSet, biLandAnimStarted)
								IF IS_ENTITY_PLAYING_ANIM(NET_TO_OBJ(serverBD.niChute), "P_cargo_chute_S", "P_cargo_chute_S_crumple")
									SET_BIT(iBoolsBitSet, biLandAnimPlaying)
									//SET_BIT(serverBD.CrateDropData.CrateData[iCrate].iBitSet, biCD_Crate_LandAnimStarted)
									NET_PRINT_TIME() NET_PRINT("     ---------->    AMMO DROP - CONTROL_CRATE_LANDING - LAND ANIM PLAYING") NET_NL()
								ENDIF
							ENDIF
						ENDIF
						//Start Crumple
						IF NOT IS_BIT_SET(iBoolsBitSet, biLandAnimStarted)
							
							SET_DAMPING(NET_TO_OBJ(serverBD.niCrate), PHYSICS_DAMPING_LINEAR_V2, 0.0245)
							
							//IF NOT IS_ENTITY_IN_AIR(NET_TO_OBJ(serverBD.niCrate))
							//OR IS_ENTITY_IN_WATER(NET_TO_OBJ(serverBD.niCrate))
							//OR ROTATION_IS_TOO_FAR()
							IF SHOULD_LAND_ANIM_START()
								PLAY_ENTITY_ANIM(NET_TO_OBJ(serverBD.niChute), "P_cargo_chute_S_crumple", "P_cargo_chute_S", INSTANT_BLEND_IN, FALSE, FALSE)	//TRUE)
								SET_BIT(iBoolsBitSet, biLandAnimStarted)
								IF DOES_BLIP_EXIST(AmmoCrateBlip)
									SET_BLIP_ALPHA(AmmoCrateBlip, 255)
								ENDIF
								NET_PRINT_TIME() NET_PRINT("     ---------->    AMMO DROP - CONTROL_CRATE_LANDING - LAND ANIM STARTED ") NET_NL()
								NET_PRINT_TIME() NET_PRINT("     ---------->    AMMO DROP - CONTROL_CRATE_LANDING - NETID = ") NET_PRINT_INT(NATIVE_TO_INT(serverBD.niChute)) NET_NL()
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//DESTRUCTIBLE CRATE CHECKS
			IF NOT IS_BIT_SET(iBoolsBitSet, biDetachDestructibleCrateDone)
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.DCrateData.NetID)
					
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.DCrateData.NetID)
					OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.DCrateData.NetID) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
						
						IF TAKE_CONTROL_OF_NET_ID(serverBD.DCrateData.NetID)
							
							SET_DAMPING(NET_TO_OBJ(serverBD.DCrateData.NetID), PHYSICS_DAMPING_LINEAR_V2, 0.1)
							
							IF HAS_OBJECT_BEEN_BROKEN(NET_TO_OBJ(serverBD.DCrateData.NetID))
								IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niCrate)
									DETACH_ENTITY(NET_TO_OBJ(serverBD.niCrate))
									IF serverBD.bisballisticdrop = TRUE
									GB_ENABLE_PICKUP_FOR_PLAYER_ONLY(NET_TO_OBJ(serverBD.niCrate))
									ENDIF
								ENDIF
								//CLEANUP_CRATE_BLEEP_SOUND()
								//DETACH_ENTITY(NET_TO_OBJ(serverBD.DCrateData[iCrate].NetID))
								SET_BIT(iBoolsBitSet, biDetachDestructibleCrateDone)
								NET_PRINT_TIME() NET_PRINT("     ---------->    AMMO DROP CONTROL_CRATE_LANDING - B - biDetachDestructibleCrateDone SET ") NET_NL()
							ENDIF
						ENDIF
						
						IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_CrateBreakable)
						AND IS_BIT_SET(iBoolsBitSet, biLandAnimDone)
						
							IF TAKE_CONTROL_OF_NET_ID(serverBD.DCrateData.NetID)
							
								IF NOT IS_ENTITY_WAITING_FOR_WORLD_COLLISION(NET_TO_OBJ(serverBD.DCrateData.NetID))
								AND HAS_COLLISION_LOADED_AROUND_ENTITY(NET_TO_OBJ(serverBD.DCrateData.NetID))
								
									BOOL bSetToBreakable
								
									IF HAS_ENTITY_BEEN_DAMAGED_BY_ANY_PED(NET_TO_OBJ(serverBD.DCrateData.NetID))

										bSetToBreakable = TRUE
										NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - CONTROL_CRATE_LANDING - CRATE HAS BEEN DAMAGED BY PED ") NET_NL()

									ELIF NOT IS_ENTITY_IN_AIR(NET_TO_OBJ(serverBD.DCrateData.NetID))
									AND IS_ENTITY_STATIC(NET_TO_OBJ(serverBD.DCrateData.NetID))
									
										bSetToBreakable = TRUE
										NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - CONTROL_CRATE_LANDING - on the ground and static ") NET_NL()	

									ENDIF
									
									IF bSetToBreakable
										SET_DISABLE_BREAKING(NET_TO_OBJ(serverBD.DCrateData.NetID), FALSE)
										SET_BIT(serverBD.iServerBitSet, biS_CrateBreakable)
										NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - CONTROL_CRATE_LANDING - BREAKING ENABLED ") NET_NL()
									ENDIF
									IF serverBD.bisballisticdrop = TRUE
									GB_ENABLE_PICKUP_FOR_PLAYER_ONLY(NET_TO_OBJ(serverBD.niCrate))
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
					ENDIF
					
				ENDIF
			ENDIF
			
			//Check Crate is above ground
			VECTOR vCratePos = GET_ENTITY_COORDS(NET_TO_OBJ(serverBD.niCrate))
			IF vCratePos.z < -50.0
			AND NOT IS_ENTITY_IN_WATER(NET_TO_OBJ(serverBD.niCrate))
				IF TAKE_CONTROL_OF_NET_ID(serverBD.niCrate)
					SET_ENTITY_COORDS(NET_TO_OBJ(serverBD.niCrate), serverBD.vDropLocation)
					IF serverBD.bisballisticdrop = TRUE
					GB_ENABLE_PICKUP_FOR_PLAYER_ONLY(NET_TO_OBJ(serverBD.niCrate))
					ENDIF
					NET_PRINT_TIME() NET_PRINT("     ---------->    AMMO DROP - CONTROL_CRATE_LANDING - FORCE TO GROUND ") NET_NL()
				ENDIF
			ENDIF
			
			OBJECT_INDEX obj_SubmergeCheckThis
			
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niCrate)
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.DCrateData.NetID)
				AND IS_ENTITY_ATTACHED(NET_TO_OBJ(serverBD.niCrate))
					obj_SubmergeCheckThis = NET_TO_OBJ(serverBD.DCrateData.NetID)
				ELSE
					obj_SubmergeCheckThis = NET_TO_OBJ(serverBD.niCrate)
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(obj_SubmergeCheckThis)
			
				//Check if Crate is Submerged
				IF NOT IS_BIT_SET(iBoolsBitSet, biSetSubmergedSound)
					//IF GET_ENTITY_SUBMERGED_LEVEL(NET_TO_OBJ(serverBD.niCrate)) >= 0.9
					IF GET_ENTITY_SUBMERGED_LEVEL(obj_SubmergeCheckThis) >= 0.9
						IF GET_SOUND_ID_FROM_NETWORK_ID(serverBD.iBleepSoundID) != -1
							SET_VARIABLE_ON_SOUND(GET_SOUND_ID_FROM_NETWORK_ID(serverBD.iBleepSoundID), "Crate_Underwater", 1)
							NET_PRINT_TIME() NET_PRINT("     ---------->    AMMO DROP - CONTROL_CRATE_LANDING - Crate_Underwater = 1 ") NET_NL()
						ENDIF
						SET_BIT(iBoolsBitSet, biSetSubmergedSound)
						NET_PRINT_TIME() NET_PRINT("     ---------->    AMMO DROP - CONTROL_CRATE_LANDING - biSetSubmergedSound SET ") NET_NL()
					ENDIF	
				ELSE
					//IF GET_ENTITY_SUBMERGED_LEVEL(NET_TO_OBJ(serverBD.niCrate)) < 0.9
					IF GET_ENTITY_SUBMERGED_LEVEL(obj_SubmergeCheckThis) < 0.9
						IF GET_SOUND_ID_FROM_NETWORK_ID(serverBD.iBleepSoundID) != -1
							SET_VARIABLE_ON_SOUND(GET_SOUND_ID_FROM_NETWORK_ID(serverBD.iBleepSoundID), "Crate_Underwater", 0)
							NET_PRINT_TIME() NET_PRINT("     ---------->    AMMO DROP - CONTROL_CRATE_LANDING - Crate_Underwater = 0 ") NET_NL()
						ENDIF
						CLEAR_BIT(iBoolsBitSet, biSetSubmergedSound)
						NET_PRINT_TIME() NET_PRINT("     ---------->    AMMO DROP - CONTROL_CRATE_LANDING - biSetSubmergedSound CLEARED ") NET_NL()
					ENDIF
				ENDIF
			
			ENDIF
			
			//Control Crate Deletion
			//CONTROL_CRATE_DELETION(iCrate)
		ENDIF
		
	ENDIF
ENDPROC

//PURPOSE: Retursn FALSE if weapon is not okay (i.e. Flare)
FUNC BOOL IS_WEAPON_VALID_FOR_AMMO_DROP(WEAPON_TYPE thisWeapon)
	IF thisWeapon = WEAPONTYPE_FLARE
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC CONTROL_CRATE_PICKUP_STATUS()
	IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niCrate)
		IF IS_NET_PLAYER_OK(PLAYER_ID())
			INT iMaxAmmo
			WEAPON_TYPE thisWeapon = GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID())
			IF thisWeapon = WEAPONTYPE_UNARMED
			OR thisWeapon = WEAPONTYPE_FLARE
			OR thisWeapon = WEAPONTYPE_KNIFE
			OR thisWeapon = WEAPONTYPE_NIGHTSTICK
			OR thisWeapon = WEAPONTYPE_BAT
			OR thisWeapon = WEAPONTYPE_HAMMER
			OR thisWeapon = WEAPONTYPE_GOLFCLUB
			OR thisWeapon = WEAPONTYPE_CROWBAR
			OR thisWeapon = WEAPONTYPE_MOLOTOV
			OR thisWeapon = WEAPONTYPE_GRENADE
			OR thisWeapon = WEAPONTYPE_GRENADELAUNCHER
			OR thisWeapon = WEAPONTYPE_GRENADELAUNCHER_SMOKE
			OR thisWeapon = WEAPONTYPE_PETROLCAN
			OR thisWeapon = WEAPONTYPE_RPG
			OR thisWeapon = WEAPONTYPE_SMOKEGRENADE
			OR thisWeapon = WEAPONTYPE_STICKYBOMB
			OR thisWeapon = WEAPONTYPE_BZGAS
			OR thisWeapon = WEAPONTYPE_DLC_BOTTLE
				thisWeapon = gweapon_type_CurrentlyHeldWeapon
			ENDIF
			GET_MAX_AMMO(PLAYER_PED_ID(), thisWeapon, iMaxAmmo)
			
			IF IS_WEAPON_VALID_FOR_AMMO_DROP(thisWeapon)
			AND DOES_WEAPON_NEED_AMMO(thisWeapon)
			AND GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), thisWeapon) < iMaxAmmo
				/*IF IS_BIT_SET(iBoolsBitSet, biPickupPrevented)
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AMD_HELP3")
						CLEAR_HELP()
						NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - CONTROL_CRATE_PICKUP_STATUS - CLEAR HELP      <----------     ") NET_NL()
					ENDIF
					PREVENT_COLLECTION_OF_PORTABLE_PICKUP(NET_TO_OBJ(serverBD.niCrate), FALSE, TRUE)
					CLEAR_BIT(iBoolsBitSet, biPickupPrevented)
					NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - CONTROL_CRATE_PICKUP_STATUS - CAN PICKUP      <----------     ") NET_NL()
				ENDIF*/
			ELSE
				/*IF NOT IS_BIT_SET(iBoolsBitSet, biPickupPrevented)
					PREVENT_COLLECTION_OF_PORTABLE_PICKUP(NET_TO_OBJ(serverBD.niCrate), TRUE, TRUE)
					SET_BIT(iBoolsBitSet, biPickupPrevented)
					NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - CONTROL_CRATE_PICKUP_STATUS - CANNOT PICKUP      <----------     ") NET_NL()
				ELSE*/
					//HOST HELP
					IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
						IF NOT IS_BIT_SET(iBoolsBitSet, biPickupPreventedHelpDone)
							IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
								IF serverBD.bisballisticdrop
									PRINT_HELP("BALD_HELP3") // ~s~Get to the crate ~HUD_COLOUR_GREEN~~BLIP_GANG_ATTACK_PACKAGE~ ~s~to equip the Ballistic Armor and Minigun.
									
								ELSE
									PRINT_HELP("AMD_HELP3")	//~s~To pick up the Ammo Crate ~HUD_COLOUR_GREEN~~BLIP_GANG_ATTACK_PACKAGE~ you need to equip a weapon that requires ammo.
								ENDIF
								NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - CONTROL_CRATE_PICKUP_STATUS - CANNOT PICKUP HELP - A      <----------     ") NET_NL()
								SET_BIT(iBoolsBitSet, biPickupPreventedHelpDone)
							ENDIF
						
						ELIF NOT IS_BIT_SET(iBoolsBitSet, biPickupPreventedHelpDone2)
							IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
								IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niCrate)
									IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), NET_TO_OBJ(serverBD.niCrate), <<10, 10, 10>>)
										IF HAS_NET_TIMER_EXPIRED(iSecondHelpTimer, SECOND_HELP_DELAY)
											IF NOT serverBD.bisballisticdrop
												PRINT_HELP("AMD_HELP3")	//~s~To pick up the Ammo Crate ~HUD_COLOUR_GREEN~~BLIP_GANG_ATTACK_PACKAGE~ you need to equip a weapon that requires ammo.
											ENDIF
											NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - CONTROL_CRATE_PICKUP_STATUS - CANNOT PICKUP HELP - B      <----------     ") NET_NL()
											SET_BIT(iBoolsBitSet, biPickupPreventedHelpDone2)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
					//CLIENT HELP
					/*ELSE
						IF NOT IS_BIT_SET(iBoolsBitSet, biPickupPreventedHelpDone2)
							IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
								IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niCrate)
									IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), NET_TO_OBJ(serverBD.niCrate), <<10, 10, 10>>)
										PRINT_HELP("AMD_HELP3")	//~s~To pick up the Ammo Crate ~HUD_COLOUR_GREEN~~BLIP_GANG_ATTACK_PACKAGE~ you need to equip a weapon that requires ammo.
										NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - CONTROL_CRATE_PICKUP_STATUS - CANNOT PICKUP HELP - B      <----------     ") NET_NL()
										SET_BIT(iBoolsBitSet, biPickupPreventedHelpDone2)
									ENDIF
								ENDIF
							ENDIF
						ENDIF*/
					ENDIF
				//ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Gives the player XP/Cash Reward
//PROC GIVE_AMMO_CRATE_REWARD()
//	IF NOT IS_BIT_SET(iBoolsBitSet, biAmmoRewardGiven)
//		
//		WEAPON_TYPE thisWeapon = GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID())
//		
//		IF thisWeapon = WEAPONTYPE_UNARMED
//		OR thisWeapon = WEAPONTYPE_FLARE
//			thisWeapon = gweapon_type_CurrentlyHeldWeapon
//		ENDIF
//		ADD_AMMO_CLIPS_FOR_WEAPON(thisWeapon, AMMO_CLIPS)
//		IF IS_MP_WEAPON_UNLOCKED(WEAPONTYPE_MOLOTOV)
//			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_MOLOTOV, 3, FALSE, FALSE)
//			SET_AMMO_REWARD_FOR_ENTITY(PLAYER_PED_ID(), 3)
//			//ADD_AMMO_CLIPS_FOR_WEAPON(WEAPONTYPE_MOLOTOV, 3)
//			NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - biAmmoRewardGiven - WEAPONTYPE_MOLOTOV +3    <----------     ") NET_NL()
//		ELSE
//			NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - biAmmoRewardGiven - WEAPONTYPE_MOLOTOV LOCKED    <----------     ") NET_NL()
//		ENDIF
//		
//		//GIVE_AMMO_FOR_EACH_TYPE(iTotalAmmoCollected)
//		//SET_AMMO_REWARD_FOR_ENTITY(PLAYER_PED_ID(), iTotalAmmoCollected)
//		
//		PRINT_TICKER("AMD_AMMOT")	//~s~Ammo Crate Collected
//		
//		//INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_TOTAL_NO_SHOPS_HELD_UP, 1)
//		SET_PACKED_STAT_BOOL(PACKED_MP_COLLECT_AMMO_DROP, TRUE)
//			
//		IF DOES_BLIP_EXIST(AmmoCrateBlip)
//			REMOVE_BLIP(AmmoCrateBlip)
//		ENDIF
//		
//		NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - biAmmoRewardGiven - DONE    <----------     ") NET_NL()
//		SET_BIT(iBoolsBitSet, biAmmoRewardGiven)
//	ENDIF
//ENDPROC


//PURPOSE: Returns TRUE if the player has picked up the Crate
//FUNC BOOL HAS_PICKED_UP_AMMO()
//	IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niCrate)
//		IF IS_ENTITY_ATTACHED_TO_ENTITY(NET_TO_OBJ(serverBD.niCrate), PLAYER_PED_ID())
//		AND TAKE_CONTROL_OF_NET_ID(serverBD.niCrate)
//			//iPickupSoundID = GET_NETWORK_ID_FROM_SOUND_ID(GET_SOUND_ID())
//			PLAY_SOUND_FROM_ENTITY(-1, "Crate_Collect", NET_TO_OBJ(serverBD.niCrate), "MP_CRATE_DROP_SOUNDS", TRUE, 100)
//			
//			CLEANUP_CRATE_BLEEP_SOUND()
//			CLEANUP_CRATE_BEACON()
//			
//			GIVE_AMMO_CRATE_REWARD()
//			
//			DETACH_ENTITY(NET_TO_OBJ(serverBD.niCrate))
//			DELETE_NET_ID(serverBD.niCrate)
//			NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - DELETE CRATE A    <----------     ") NET_NL()
//			
//			SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerEventData
//			TickerEventData.TickerEvent = TICKER_EVENT_AMMO_DROP_COLLECTED
//			TickerEventData.playerID = PLAYER_ID()
//			BROADCAST_TICKER_EVENT(TickerEventData, ALL_PLAYERS_ON_SCRIPT(FALSE))
//			
//			RETURN TRUE
//		ENDIF
//	ENDIF
//	
//	RETURN FALSE
//ENDFUNC

//PURPOSE: Controls how many entities are reserved based on serverBD
PROC MAINTAIN_RESERVED_ENTITIES()
	INT iCurrentlyReserved = GET_NUM_RESERVED_MISSION_VEHICLES(FALSE)
	IF serverBD.iReservedVehicles != iCurrentlyReserved
		IF serverBD.iReservedVehicles > 0
		OR NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niPlane)
			IF serverBD.iReservedVehicles < iCurrentlyReserved
			OR CAN_RESERVE_NETWORK_VEHICLES_FOR_THIS_SCRIPT(serverBD.iReservedVehicles, FALSE, TRUE)
				RESERVE_NETWORK_MISSION_VEHICLES(serverBD.iReservedVehicles)
				NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - MAINTAIN_RESERVED_ENTITIES - RESERVED VECHILES = ") NET_PRINT_INT(serverBD.iReservedVehicles) NET_NL()
			ENDIF
		ENDIF
	ENDIF
	
	iCurrentlyReserved = GET_NUM_RESERVED_MISSION_PEDS(FALSE)
	IF serverBD.iReservedPeds != iCurrentlyReserved
		IF serverBD.iReservedPeds > 0
		OR NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niPilot)
			IF serverBD.iReservedPeds < iCurrentlyReserved
			OR CAN_RESERVE_NETWORK_PEDS_FOR_THIS_SCRIPT(serverBD.iReservedPeds, FALSE, TRUE)
				RESERVE_NETWORK_MISSION_PEDS(serverBD.iReservedPeds)
				NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - MAINTAIN_RESERVED_ENTITIES - RESERVED PEDS = ") NET_PRINT_INT(serverBD.iReservedPeds) NET_NL()
			ENDIF
		ENDIF
	ENDIF
	
	iCurrentlyReserved = GET_NUM_RESERVED_MISSION_OBJECTS(FALSE)
	IF serverBD.iReservedObjects != iCurrentlyReserved
		IF serverBD.iReservedObjects < iCurrentlyReserved
		OR CAN_RESERVE_NETWORK_OBJECTS_FOR_THIS_SCRIPT(serverBD.iReservedObjects, FALSE, TRUE)
			RESERVE_NETWORK_MISSION_OBJECTS(serverBD.iReservedObjects)
			NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - MAINTAIN_RESERVED_ENTITIES - RESERVED OBJECTS = ") NET_PRINT_INT(serverBD.iReservedObjects) NET_NL()
		ENDIF
	ENDIF
ENDPROC

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  MISSION PROCS                  //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Cleanup Mission Data
PROC SCRIPT_CLEANUP()
	
	IF DOES_BLIP_EXIST(AmmoCrateBlip)
		REMOVE_BLIP(AmmoCrateBlip)
	ENDIF
		
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		//Delete Crate and Chute
		/*IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niCrate)
		AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niCrate)
			DELETE_NET_ID(serverBD.niCrate)
			NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - DELETE CRATE B    <----------     ") NET_NL()
		ENDIF*/
		//IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niChute)
		//AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niChute)
		//	DELETE_NET_ID(serverBD.niChute)
		//ENDIF
		
		INT i
		BOOL bDCrateControl
		BOOL bCrateControl
		BOOL bChuteControl
		//SHOULDN'T REALLY NEED TO HAPPEN. JUST IN AS A FAIL SAFE
		REPEAT 250 i
			IF bDCrateControl = FALSE
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.DCrateData.NetID)
					IF HAS_OBJECT_BEEN_BROKEN(NET_TO_OBJ(serverBD.DCrateData.NetID))
						IF TAKE_CONTROL_OF_NET_ID(serverBD.DCrateData.NetID)
							SET_DISABLE_BREAKING(NET_TO_OBJ(serverBD.DCrateData.NetID), FALSE)
							bDCrateControl = TRUE
							PRINTLN("     ---------->     AMMO DROP - Crate Made Breakable")
						ENDIF
					ELSE
						bDCrateControl = TRUE
					ENDIF
				ELSE
					bDCrateControl = TRUE
				ENDIF
			ENDIF
			
			IF bCrateControl = FALSE
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niCrate)
					IF IS_ENTITY_ATTACHED(NET_TO_OBJ(serverBD.niCrate))
						IF TAKE_CONTROL_OF_NET_ID(serverBD.niCrate)
							DETACH_ENTITY(NET_TO_OBJ(serverBD.niCrate))
							bCrateControl = TRUE
							PRINTLN("     ---------->     AMMO DROP - Crate Detached")
						ENDIF
					ELSE
						bCrateControl = TRUE
					ENDIF
				ELSE
					bCrateControl = TRUE
				ENDIF
			ENDIF
			
			IF bChuteControl = FALSE
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niChute)
					IF TAKE_CONTROL_OF_NET_ID(serverBD.niChute)
						DELETE_NET_ID(serverBD.niChute)
						bChuteControl = TRUE
						PRINTLN("     ---------->     AMMO DROP - Chute Deleted")
					ENDIF
				ELSE
					bChuteControl = TRUE
				ENDIF
			ENDIF
			
			//Bail
			IF bCrateControl = TRUE
			AND bChuteControl = TRUE
			AND bDCrateControl = TRUE
				i = 9999
				PRINTLN("     ---------->     AMMO DROP - Entities Dealt With")
			ELSE
				WAIT(0)
			ENDIF
		ENDREPEAT
		
		serverBD.iServerGameState = GAME_STATE_END
		NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - serverBD.iServerGameState = GAME_STATE_END 999    <----------     ") NET_NL()
		BROADCAST_TELL_SERVER_CONTACT_REQUEST_COMPLETE(ALL_PLAYERS(),REQUEST_AMMO_DROP,INVALID_PLAYER_INDEX())
	ENDIF
	
	CLEANUP_CRATE_BEACON()
	CLEANUP_CRATE_BLEEP_SOUND()
	//SET_ONLY_ALLOW_AMMO_COLLECTION_WHEN_LOW(TRUE)
	//CLEANUP_CINEMATIC_SHOT_OF_ENTITY(eAM_AMMO_DROP, "AMD_CINH")
	
	CLEAR_SEQUENCES()
	
	NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - CLEANUP MISSION      <----------     ") NET_NL()
	#IF IS_DEBUG_BUILD NET_LOG("SCRIPT_CLEANUP")	#ENDIF
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

//PURPOSE: Process the AMMO DROP stages for the Client
PROC PROCESS_AMMO_DROP_CLIENT()
	
	//playerBD[PARTICIPANT_ID_TO_INT()].eAmmoDropStage = serverBD.eAmmoDropStage
	MAINTAIN_RESERVED_ENTITIES()
	
	SWITCH playerBD[PARTICIPANT_ID_TO_INT()].eAmmoDropStage		
		CASE eAD_DROP_AMMO
			//CONTROL_HELP_TEXT()
			
			IF serverBD.eAmmoDropStage > eAD_DROP_AMMO
				//ADD_CRATE_BLIP()
				playerBD[PARTICIPANT_ID_TO_INT()].eAmmoDropStage = eAD_PICKUP_AMMO
				NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - PLAYER STAGE = eAD_PICKUP_AMMO    <----------     ") NET_NL()
			ENDIF
		BREAK
		
		CASE eAD_PICKUP_AMMO
			CONTROL_CRATE_LANDING()
			CONTROL_CRATE_PICKUP_STATUS()
//			IF HAS_PICKED_UP_AMMO()
//				//playerBD[PARTICIPANT_ID_TO_INT()].eAmmoDropStage = eAD_CLEANUP
//				//NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - PLAYER STAGE = eAD_CLEANUP    <----------     ") NET_NL()
//				//SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_AmmoPickedUp)
//				NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - biP_AmmoPickedUp SET    <----------     ") NET_NL()
//			ENDIF
		BREAK
		
		//CASE eAD_REWARD
			//GIVE_AMMO_CRATE_REWARD()
		//BREAK
		
		CASE eAD_CLEANUP
			NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - SCRIPT CLEANUP B     <----------     ") NET_NL()
			SCRIPT_CLEANUP()
		BREAK
	ENDSWITCH
ENDPROC

//PURPOSE: Process the AMMO DROP stages for the Server
PROC PROCESS_AMMO_DROP_SERVER()
	SWITCH serverBD.eAmmoDropStage		
		CASE eAD_DROP_AMMO
			IF serverBD.bisballisticdrop
				CONTROL_HELP_TEXT_BALLISTIC()
			ELSE
				CONTROL_HELP_TEXT()
			ENDIF
			IF CONTROL_CRATE_DROPPING()
				ADD_CRATE_BLIP()
				serverBD.eAmmoDropStage = eAD_PICKUP_AMMO
				NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - SERVER STAGE = eAD_PICKUP_AMMO    <----------     ") NET_NL()
			ENDIF
			
			//IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niPlane)
			//	MAINTAIN_CINEMATIC_SHOT_OF_ENTITY(eAM_AMMO_DROP, serverBD.niPlane, 250.0, "AMD_CINH", iCinematicShotStage, bDisplayedCineCamHelp)
			//ENDIF
		BREAK
		
		CASE eAD_PICKUP_AMMO
			//IF HAS_CRATE_BEEN_PICKED_UP()
			//	serverBD.eAmmoDropStage = eAD_REWARD
			//	NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - SERVER STAGE = eAD_REWARD    <----------     ") NET_NL()
			//ENDIF
			
			IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niCrate)
				DRAW_MARKER_ABOVE_CRATE(serverBD.niCrate, HUD_COLOUR_GREEN)
				/*IF NOT IS_BIT_SET(iBoolsBitSet, biLandAnimStarted)
				OR NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niChute)
					MAINTAIN_CINEMATIC_SHOT_OF_ENTITY(eAM_AMMO_DROP, serverBD.niCrate, 300.0, "AMD_CINH", iCinematicShotStage, bDisplayedCineCamHelp)
				ELSE
					CLEANUP_CINEMATIC_SHOT_OF_ENTITY(eAM_AMMO_DROP, "AMD_CINH")
				ENDIF*/
				
				//Reduce reserved objects when chute is removed.
				IF serverBD.iReservedObjects = 3
					IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niChute)
						serverBD.iReservedObjects = 2
						NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - REDUCED serverBD.iReservedObjects = 2    <----------     ") NET_NL()
					ENDIF
				ENDIF
				//Cleanup Plane and Pilot
				IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niPlane)
					CLEANUP_NET_ID(serverBD.niPlane)
				ELSE
					IF serverBD.iReservedVehicles = 1
						serverBD.iReservedVehicles = 0
						NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - REDUCED serverBD.iReservedVehicles = 0    <----------     ") NET_NL()
					ENDIF
				ENDIF
				IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niPilot)
					CLEANUP_NET_ID(serverBD.niPilot)
				ELSE
					IF serverBD.iReservedPeds = 1
						serverBD.iReservedPeds = 0
						NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - REDUCED serverBD.iReservedPeds = 0    <----------     ") NET_NL()
					ENDIF
				ENDIF
			/*ELSE
				IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niPlane)
					MAINTAIN_CINEMATIC_SHOT_OF_ENTITY(eAM_AMMO_DROP, serverBD.niPlane, 250.0, "AMD_CINH", iCinematicShotStage, bDisplayedCineCamHelp)
				ENDIF*/
			ENDIF
		BREAK
		
		//CASE eAD_REWARD
		//	serverBD.eAmmoDropStage = eAD_CLEANUP
		//	NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - SERVER STAGE = eAD_CLEANUP    <----------     ") NET_NL()
		//BREAK
		
		CASE eAD_CLEANUP
			
		BREAK
	ENDSWITCH
ENDPROC

#IF IS_DEBUG_BUILD
PROC PROCESS_DEBUG_DISPLAY()

	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_HOME)
		debug_bDisplayDebug = !debug_bDisplayDebug
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(debug_bDisplayDebug)
	ENDIF
	
	IF debug_bDisplayDebug
	
		CONST_FLOAT F_LEFT		0.05
		CONST_FLOAT F_TOP		0.1
		CONST_FLOAT F_INDENT	0.02
		CONST_FLOAT F_SPACING	0.02
		
		CONST_INT	I_INDENT	5
		CONST_INT	I_SPACING	10
		
		TEXT_LABEL_63 strDebug
		INT iDebugCountReuse
		
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.DCrateData.NetID)
		
			VECTOR vCoord = GET_ENTITY_COORDS(NET_TO_OBJ(serverBD.DCrateData.NetID))
			
			strDebug = "DESTRUCTIBLE CRATE"
			DRAW_DEBUG_TEXT_WITH_OFFSET(strDebug, vCoord, 0, iDebugCountReuse*I_SPACING, 255,0,0,255)
			iDebugCountReuse++
			
			strDebug = "Health: "
			strDebug += GET_ENTITY_HEALTH(NET_TO_OBJ(serverBD.DCrateData.NetID))
			DRAW_DEBUG_TEXT_WITH_OFFSET(strDebug, vCoord, I_INDENT, iDebugCountReuse*I_SPACING, 255,255,55,255)
			iDebugCountReuse++
			
			strDebug = "Frag health: "
			strDebug += ROUND( GET_OBJECT_FRAGMENT_DAMAGE_HEALTH(NET_TO_OBJ(serverBD.DCrateData.NetID), FALSE) * 100 )
			DRAW_DEBUG_TEXT_WITH_OFFSET(strDebug, vCoord, I_INDENT, iDebugCountReuse*I_SPACING, 255,255,55,255)
			iDebugCountReuse++
			
			strDebug = "Is broken: "
			IF HAS_OBJECT_BEEN_BROKEN(NET_TO_OBJ(serverBD.DCrateData.NetID))
				strDebug += "TRUE"
			ELSE
				strDebug += "FALSE"
			ENDIF
			DRAW_DEBUG_TEXT_WITH_OFFSET(strDebug, vCoord, I_INDENT, iDebugCountReuse*I_SPACING, 255,255,55,255)
			iDebugCountReuse++
			
			strDebug = "Is in air: "
			IF IS_ENTITY_IN_AIR(NET_TO_OBJ(serverBD.DCrateData.NetID))
				strDebug += "TRUE"
			ELSE
				strDebug += "FALSE"
			ENDIF
			DRAW_DEBUG_TEXT_WITH_OFFSET(strDebug, vCoord, I_INDENT, iDebugCountReuse*I_SPACING, 255,255,55,255)
			iDebugCountReuse++
			
			strDebug = "Breaking: "
			IF IS_BIT_SET(serverBD.iServerBitSet, biS_CrateBreakable)
				strDebug += "ENABLED"
			ELSE
				strDebug += "DISABLED"
			ENDIF
			DRAW_DEBUG_TEXT_WITH_OFFSET(strDebug, vCoord, I_INDENT, iDebugCountReuse*I_SPACING, 255,255,55,255)
			iDebugCountReuse++
			
		ENDIF

	ENDIF

ENDPROC
#ENDIF

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                      Main Loop                      //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
SCRIPT(MP_MISSION_DATA missionScriptArgs)
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		#IF IS_DEBUG_BUILD	
			NET_PRINT_TIME() NET_PRINT("MP: Starting mission ") NET_NL()
		#ENDIF
		
		// Carry out all the initial game starting duties. 
		IF NOT PROCESS_PRE_GAME(missionScriptArgs)
			#IF IS_DEBUG_BUILD NET_LOG("FAILED TO RECEIVE INTIAL BROADCAST DATA")	#ENDIF
			NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - MISSION END - FAILED TO RECEIVE INTIAL BROADCAST DATA - SCRIPT CLEANUP E     <----------     ") NET_NL()
			SCRIPT_CLEANUP()
		ENDIF
		
		// Setup widgets. Make sure this is done after PROCESS_PRE_GAME()
		#IF IS_DEBUG_BUILD
			CREATE_WIDGETS()
		#ENDIF
	ENDIF
	
	// Main loop
	WHILE TRUE

		// One wait to rule them all. This can be the ONLY wait from here on in.....
		MP_LOOP_WAIT_ZERO()
				
		// If we have a match end event, bail
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		//OR NOT IS_NET_PLAYER_OK(PLAYER_ID(), TRUE, TRUE)	//FALSE, TRUE)
			#IF IS_DEBUG_BUILD NET_LOG("SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE")	#ENDIF
			NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - MISSION END - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE - SCRIPT CLEANUP B     <----------     ") NET_NL()
			SCRIPT_CLEANUP()
		ENDIF
		
		//Check if AMMO DROP shouldn't start
		/*IF NOT CAN_DO_AMMO_DROP(FALSE)
			#IF IS_DEBUG_BUILD NET_LOG("CAN_DO_AMMO_DROP")	#ENDIF
			NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - MISSION END - CAN_DO_AMMO_DROP = FALSE - SCRIPT CLEANUP D     <----------     ") NET_NL()
			SCRIPT_CLEANUP()
		ENDIF*/
		
		//Check if player has swapped from/to tutorial session and end
		IF NETWORK_IS_IN_TUTORIAL_SESSION() != bInTutorialSession
			#IF IS_DEBUG_BUILD NET_LOG("PLAYER IN TUT SESSION")	#ENDIF
			NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - MISSION END - PLAYER IN TUTORIAL SESSION - SCRIPT CLEANUP C     <----------     ") NET_NL()
			SCRIPT_CLEANUP()
		ENDIF
		
		IF NETWORK_GET_PLAYER_INDEX(NETWORK_GET_HOST_OF_THIS_SCRIPT()) != serverBD.MainPlayer	//MPGlobalsAmbience.HeliPickupPlayer
			#IF IS_DEBUG_BUILD NET_LOG("ORIGINAL CALLER NO LONGER HOST")	#ENDIF
			NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - MISSION END - ORIGINAL CALLER NO LONGER HOST - SCRIPT CLEANUP D     <----------     ") NET_NL()
			SCRIPT_CLEANUP()
		ENDIF
		
		NETWORK_PREVENT_SCRIPT_HOST_MIGRATION()	//ONLY LAUNCHING PLAYER SHOULD BE HOST. IF THEY LEAVE SCRIPT SHOULD END
		
		// Deal with the debug
		#IF IS_DEBUG_BUILD		
			UPDATE_WIDGETS()
		#ENDIF		
	
		// If we need to leave the mission, make sure we just straight to the end stage and terminate the script - DON'T NEED FOR AMBIENT MISSION
		/*IF SHOULD_PLAYER_LEAVE_MP_MISSION() 
			#IF IS_DEBUG_BUILD
			NET_PRINT_TIME() NET_PRINT( GET_MP_MISSION_NAME(thisMission), " Leave mission = TRUE \n")
			#ENDIF
			playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_LEAVE
			#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_LEAVE")	#ENDIF
		ENDIF*/
		
		// -----------------------------------
		// Process your game logic.....
		SWITCH GET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT())
			
			// Wait untill the server gives the all go before moving on
			CASE GAME_STATE_INI
				IF LOAD_CRATE_ASSETS()
					IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
						CREATE_SEQUENCES()
						playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_RUNNING
						NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_RUNNING    <----------     ") NET_NL()
						#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_RUNNING")	#ENDIF
					ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
						playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY
						NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY 1    <----------     ") NET_NL()
						#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_TERMINATE_DELAY")	#ENDIF
					ENDIF
				ENDIF
			BREAK
			
			// Main gameplay state.
			CASE GAME_STATE_RUNNING
				
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					PROCESS_AMMO_DROP_CLIENT()
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY
					NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY 2   <----------     ") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_TERMINATE_DELAY")	#ENDIF
				ENDIF
				
				// Make the player end the mission if they leave the Area
				/*IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), serverBD.vDropLocation, <<LEAVE_RANGE, LEAVE_RANGE, LEAVE_RANGE>>)
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
					NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END 6 - MISSION END - PLAYER NOT IN AREA ") NET_PRINT_VECTOR(serverBD.vDropLocation) NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_END 6")	#ENDIF
				ENDIF*/
			BREAK
			
			CASE GAME_STATE_TERMINATE_DELAY 
				SET_MISSION_FINISHED(serverBD.MissionTerminateDelayTimer)
		      	IF IS_MISSION_READY_TO_CLEANUP(serverBD.MissionTerminateDelayTimer)
		            playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
					NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END 3    <----------     ") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_END 3")	#ENDIF
		      	ENDIF
			BREAK
			
			// The game stage the local player is placed when we want him to leave a mission
			CASE GAME_STATE_LEAVE
				//LEAVE_MY_MP_MISSION(thisMission)
				playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
				NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END 2    <----------     ") NET_NL()
				#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_END 2")	#ENDIF
			FALLTHRU
			
			//Cleans up then terminates the mission
			CASE GAME_STATE_END
				NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - SCRIPT CLEANUP A     <----------     ") NET_NL()
				SCRIPT_CLEANUP()
			BREAK

		ENDSWITCH
		
		// -----------------------------------
		// Process server game logic		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			
			SWITCH GET_SERVER_MISSION_STATE()
				
				// For CnC group missions, we may want to wait for all players to have launched the script
				CASE GAME_STATE_INI
					IF CREATE_PLANE_AND_PILOT()
						serverBD.iServerGameState = GAME_STATE_RUNNING
						NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - serverBD.iServerGameState = GAME_STATE_RUNNING    <----------     ") NET_NL()
						#IF IS_DEBUG_BUILD NET_LOG_Host("iServerGameState = GAME_STATE_RUNNING")	#ENDIF
					ENDIF
				BREAK
				
				// Look for game end conditions
				CASE GAME_STATE_RUNNING
					
					//MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER()
					PROCESS_AMMO_DROP_SERVER()
					
					#IF IS_DEBUG_BUILD
						IF bHostEndMissionNow
							serverBD.iServerGameState = GAME_STATE_END
							NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - serverBD.iServerGameState = GAME_STATE_END 1    <----------     ") NET_NL()
							#IF IS_DEBUG_BUILD NET_LOG_Host("iServerGameState = GAME_STATE_END 1")	#ENDIF
						ENDIF
					#ENDIF	
					
					IF HAVE_MISSION_END_CONDITIONS_BEEN_MET()
						serverBD.iServerGameState = GAME_STATE_END
						NET_PRINT_TIME() NET_PRINT("     ---------->     AMMO DROP - serverBD.iServerGameState = GAME_STATE_END 4    <----------     ") NET_NL()
						#IF IS_DEBUG_BUILD NET_LOG_Host("iServerGameState = GAME_STATE_END 4")	#ENDIF
					ENDIF
					
				BREAK
				
				CASE GAME_STATE_END
				BREAK
				
			ENDSWITCH
			
		ENDIF
		
		#IF IS_DEBUG_BUILD
			PROCESS_DEBUG_DISPLAY()
		#ENDIF
		
	ENDWHILE
	
ENDSCRIPT
