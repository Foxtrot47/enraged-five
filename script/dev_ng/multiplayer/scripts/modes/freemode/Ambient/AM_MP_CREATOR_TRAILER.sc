USING "globals.sch"

USING "commands_network.sch"
USING "script_player.sch"
USING "commands_path.sch"
USING "commands_event.sch"

USING "net_include.sch"
USING "net_events.sch"
USING "net_spawn.sch"
USING "net_ambience.sch"

USING "net_wait_zero.sch"
USING "freemode_events_header.sch"

USING "net_MP_Radio.sch"
USING "net_gun_locker.sch"

USING "net_sliding_doors_lib.sch"
USING "net_property_sections_lib.sch"
USING "net_property_sections_armory_truck.sch"
USING "net_activity_creator_activities.sch"
//USING "mp_bed_high.sch"

// Local BS
CONST_INT BS_CREATOR_TRAILER_IS_CAR_MOD_SCRIPT_READY							0
CONST_INT BS_CREATOR_TRAILER_PERS_VEH_CHECKED_AVAILABLILITY					1
CONST_INT BS_CREATOR_TRAILER_PERS_VEH_AVAILABLE								2
CONST_INT BS_CREATOR_TRAILER_SET_CUTSCENE_ENTITIES_TO_BE_NETWORKED				3
//CONST_INT BS_CREATOR_TRAILER_ENTERED_IN_A_NEW_VEHICLE							4
CONST_INT BS_CREATOR_TRAILER_VEH_TRANSACTION_FINISHED							4
CONST_INT BS_CREATOR_TRAILER_CREATED_TRANSACTION_VEHICLE						5
CONST_INT BS_CREATOR_TRAILER_APPLIED_DETAILS_TO_TRANSACTION_VEHICLE			6
CONST_INT BS_WVM_MISSION_IMAGES_DOWNLOADED									7
CONST_INT BS_CREATOR_TRAILER_PREVENT_JACKING									8
CONST_INT BS_CREATOR_TRAILER_CALLED_CLEAR_HELP									9

// PlayerBD BS
CONST_INT BS_CREATOR_TRAILER_READY_TO_WARP_OUT_W_OWNER							0


//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════════╡ VARIABLES  ╞══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

INT iTruckInteriorHash

SCRIPT_TIMER pauseMenuInteractionsDelay

//BedStructData bedStructLeft[3]
//BedStructData bedStructRight[3]
//BOOL bBedInit[3]

CONST_INT SET_VEH_ON_GROUND_OFFSET		0

CONST_INT CREATOR_TRAILER_MAX_SECONDARY_LOCATES 2


VEHICLE_INDEX transactionVehicle
INT iNewCarTransactionResult

INT iNewVehicleSavedSlot

SERVER_CREATOR_ACTIVITY_PEDS activityPeds[iMaxCreatorActivities]

STRUCT ServerBroadcastData
	INT iBS
	
	// Factory Ped serverBD Data
	SERVER_CREATOR_ACTIVITY_PROPS activityProps[iMaxCreatorActivities]

	NETWORK_INDEX personalVehicle
	INT iPersVehSaveSlot = -1
	INT iPersVehCreationBS = 0 //uses MP_PROP_CREATE_CARS_BS_APPLIED_DETAILS
	BOOL bOwnerCompletedVehicleCreation = FALSE
ENDSTRUCT
ServerBroadcastData serverBD

STRUCT PlayerBroadcastData
	INT iBS
ENDSTRUCT
PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]

// Factory Ped Data
ACTIVITY_INTERIOR_STRUCT interiorStruct
CHECK_CREATOR_ACTIVITIES_STRUCT activityCheck[iMaxCreatorActivities]
ACTIVITY_CONTROLLER_STRUCT activityControllerStruct
BOOL bTintWasRefreshed = FALSE
//PED_VAR_ID_STRUCT pedLocalVariationsStruct[iMaxCreatorActivities]


ENUM CREATOR_TRAILER_SCRIPT_STATE
	CREATOR_TRAILER_STATE_LOADING,
	CREATOR_TRAILER_STATE_IDLE,
	CREATOR_TRAILER_STATE_GOTO_EXIT,
	CREATOR_TRAILER_STATE_EXIT_TO_FREEMODE,
	CREATOR_TRAILER_STATE_EXIT_TO_BUNKER,
	CREATOR_TRAILER_STATE_EXIT_TO_CAB_IN_BUNKER,
	CREATOR_TRAILER_STATE_EXIT_TO_CAB_IN_FREEMODE
ENDENUM

ENUM CREATOR_TRAILER_COVERT_ARMS_STATE
	CA_STATE_IDLE,
	CA_STATE_IN_ANGLED_AREA,
	CA_STATE_TRIGGERED,
	CA_STATE_EXIT	
ENDENUM

STRUCT COVERT_ARMS_STRUCT
	INT				 					ContextIntention	= NEW_CONTEXT_INTENTION
	CREATOR_TRAILER_COVERT_ARMS_STATE 	eUseState			= CA_STATE_IDLE
	ARMORY_TRUCK_SECTION_TYPE_ENUM 		eTVInUse 			= AT_ST_UNDEFINED
	ARMORY_TRUCK_SECTIONS_ENUM			eCurrentSection		= ATS_FIRST_SECTION
	SCRIPT_TIMER stHelpReset
ENDSTRUCT

STRUCT CREATOR_TRAILER_DATA
	CREATOR_TRAILER_SCRIPT_STATE eState
	
	SIMPLE_INTERIORS eSimpleInteriorID
	
	INT iScriptInstance, iLocalBS, iInvitingPlayer
	
	STRING strAudioScene
	STRING sChildOfChildScript
	
	BOOL bScriptWasRelaunched
	
	PLAYER_INDEX pOwner
	
	VEHICLE_INDEX ownerVeh
	
	THREADID CarModThread
	
	STRING sAmbientZone[3]
	
	SCRIPT_TIMER sCarModScriptRunTimer
//	SCRIPT_TIMER sResetBedsDelay
	
	ARMORY_TRUCK_SECTIONS_STRUCT sPropertySections
	
	COVERT_ARMS_STRUCT sCovertArms

	SCRIPT_TIMER failSafeClearVehicleDelay
	
	VECTOR vLoateCoords[CREATOR_TRAILER_MAX_SECONDARY_LOCATES]
	VECTOR vLocateDimensions[CREATOR_TRAILER_MAX_SECONDARY_LOCATES]
	FLOAT fExitLocateHeading
ENDSTRUCT
CREATOR_TRAILER_DATA thisCreatorTrailer

ARMORY_TRUCK_MONITOR_DATA sTruckMonitorData

VAULT_WEAPON_LOADOUT_CUSTOMIZATION sArmoryTruckGunLocker
//STRUCT_DL_PHOTO_VARS_LITE sDownloadWVMPhotoVars[ciWVM_FLOW_MISSION_MAX]

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════════╡ CLEANUP  ╞═══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC CLEANUP_ARMORY_TRUCK_AUDIO()
	INT i
	REPEAT 3 i
		IF NOT IS_STRING_NULL_OR_EMPTY(thisCreatorTrailer.sAmbientZone[i])
			#IF IS_DEBUG_BUILD
			PRINTLN("AM_MP_ARMORY_TRUCK - CLEANUP_ARMORY_TRUCK_AUDIO - setting ambient zone to false ", thisCreatorTrailer.sAmbientZone[i])
			#ENDIF
			
			SET_AMBIENT_ZONE_STATE(thisCreatorTrailer.sAmbientZone[i], FALSE, TRUE)
		ENDIF
	ENDREPEAT
ENDPROC

PROC CLEAN_UP_PERSONAL_CAR_MOD()
	PRINTLN("CLEAN_UP_PERSONAL_CAR_MOD (Armory Truck) - called")
	g_bCleanUpCarmodShop = TRUE
	g_sShopSettings.bShopScriptLaunched[ENUM_TO_INT(CARMOD_SHOP_PERSONALMOD)] = FALSE
	CLEAR_BIT(thisCreatorTrailer.iLocalBS,BS_CREATOR_TRAILER_IS_CAR_MOD_SCRIPT_READY)
	RESET_NET_TIMER(thisCreatorTrailer.sCarModScriptRunTimer)
	IF NOT IS_STRING_NULL_OR_EMPTY(thisCreatorTrailer.sChildOfChildScript)
		SET_SCRIPT_AS_NO_LONGER_NEEDED(thisCreatorTrailer.sChildOfChildScript)
	ENDIF	
ENDPROC

PROC CLEANUP_PLAYER_VEHICLE_FLAGS()
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		VEHICLE_INDEX vTemp = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
		
		IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
			SET_VEHICLE_LIGHTS(vTemp, NO_VEHICLE_LIGHT_OVERRIDE)
			SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(vTemp, FALSE)
			SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(vTemp, FALSE)
		ENDIF
	ENDIF
ENDPROC

PROC CLEAN_UP_CREATOR_TRAILER_GUN_LOCKER(VAULT_WEAPON_LOADOUT_CUSTOMIZATION& sWLoadoutCustomization,BOOL bForceCleanUp = FALSE)
	PRINTLN("Vault weapon loadout - CLEAN_UP_CREATOR_TRAILER_GUN_LOCKER")
	
	RELEASE_CONTEXT_INTENTION(sWLoadoutCustomization.iVaultWeaponContext)
	sWLoadoutCustomization.iVaultWeaponContext = NEW_CONTEXT_INTENTION
	sWLoadoutCustomization.iWeaponMenuCurrentItem = 0
	sWLoadoutCustomization.iWeaponMenuTopItem = 0
	sWLoadoutCustomization.iNumAvailableWeaponGroup = 0
	sWLoadoutCustomization.eWeaponCurrentMenu = VM_MAIN_MENU
	sWLoadoutCustomization.bMenuInitialised = FALSE
	sWLoadoutCustomization.bReBuildMenu = FALSE
	
	CLEAR_BIT(sWLoadoutCustomization.iLocalBS,VAULT_DOOR_ANIM_STAGE_FINISHED)
	CLEAR_BIT(sWLoadoutCustomization.iLocalBS,VAULT_GUN_DOOR_ANIM_RUNNING)
	CLEAR_BIT(sWLoadoutCustomization.iLocalBS, BS_GET_NUM_AVAILABLE_WEAPON_GROUP)
	
	INT iNumSubMenu
	FOR iNumSubMenu = 0 TO WEAPON_VAULT_TOTAL - 1
		CLEAR_BIT(sWLoadoutCustomization.iVaultWeaponBS,iNumSubMenu)
		CLEAR_BIT(sWLoadoutCustomization.iAvailableWeaponBS,iNumSubMenu)
		CLEAR_BIT(sWLoadoutCustomization.iHideAllWeaponBS,iNumSubMenu)
	ENDFOR

	IF bForceCleanUp
		INT iNumWeaponModels
		FOR iNumWeaponModels = 0 TO MAX_NUMBER_WEAPON_MODELS -1
			IF DOES_ENTITY_EXIST(sWLoadoutCustomization.weapons[iNumWeaponModels])
				DELETE_OBJECT(sWLoadoutCustomization.weapons[iNumWeaponModels])
			ENDIF
			IF IS_BIT_SET(sWLoadoutCustomization.iWeaponModelBS, iNumWeaponModels)
				CLEAR_BIT(sWLoadoutCustomization.iWeaponModelBS, iNumWeaponModels)
			ENDIF
		ENDFOR
		
		CLEAR_BIT(sWLoadoutCustomization.iLocalBS,VAULT_GUN_DOOR_ANIM_INITIALISED)
		CLEAR_BIT(sWLoadoutCustomization.iLocalBS,VAULT_GUN_DOOR_NET_RESERVED)
		CLEAR_BIT(sWLoadoutCustomization.iLocalBS,VAULT_DOOR_ANIM_STAGE_FINISHED)
		CLEAR_BIT(sWLoadoutCustomization.iLocalBS,OFFICE_GUN_VAULT_DOOR_CREATED)

		sWLoadoutCustomization.eCustomizationStage = VMC_STAGE_INIT	
	ELSE
		sWLoadoutCustomization.eCustomizationStage = VMC_STAGE_INIT
	ENDIF
ENDPROC

PROC INITALISE_CREATOR_TRAILER_RENDER_TARGET_IDS(ARMORY_TRUCK_MONITOR_DATA &data)
	INT i
	REPEAT COUNT_OF(data.iRenderTargetIDs) i
		data.iRenderTargetIDs[i] = -1
	ENDREPEAT
ENDPROC

PROC CLEANUP_CREATOR_TRAILER_MONITORS(ARMORY_TRUCK_MONITOR_DATA &data)
	INT i
	REPEAT COUNT_OF(data.iRenderTargetIDs) i 
		IF IS_NAMED_RENDERTARGET_REGISTERED(GET_RENDER_TARGET(INT_TO_ENUM(ARMORY_TRUCK_SCREEN_ID, i)))
			RELEASE_NAMED_RENDERTARGET(GET_RENDER_TARGET(INT_TO_ENUM(ARMORY_TRUCK_SCREEN_ID, i)))
		ENDIF
		
		data.iRenderTargetIDs[i] = -1
	ENDREPEAT
	
	data.eMonitorStates = ATP_MONITOR_STATE_DETECT_PROP
	
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(GET_STREAMED_TEXTURE_DICTIONARY(AT_TRAILER_MONITOR_01))
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(GET_STREAMED_TEXTURE_DICTIONARY(AT_COMMAND_CENTER_TV_01))
ENDPROC 

PROC CLEANUP_SECONDARY_EXIT_LOCATE()
	CLEAR_BIT(g_SimpleInteriorData.iBSTruckExit, BS_SIMPLE_INTERIOR_GLOBAL_DATA_REFRESH_MENU_OPTIONS)

	g_SimpleInteriorData.bTriggerExitFromArmoryTruck = FALSE
	g_SimpleInteriorData.bRefreshInteriorMenu = FALSE
	g_SimpleInteriorData.bTruckCabOwnerAccess = FALSE
ENDPROC

PROC CLEAR_EXIT_MENU_OPTION_INDEX()
	g_SimpleInteriorData.iExitMenuOption = -1
	CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_CREATOR_TRAILER - CLEAR_EXIT_MENU_OPTION_INDEX - g_SimpleInteriorData.iExitMenuOption = ", g_SimpleInteriorData.iExitMenuOption)
ENDPROC

PROC SCRIPT_CLEANUP()

	#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_CREATOR_TRAILER - SCRIPT_CLEANUP")
	#ENDIF
	
	IF IS_GAMEPLAY_CAM_SHAKING()
		STOP_GAMEPLAY_CAM_SHAKING(TRUE)
	ENDIF
	
//	IF thisCreatorTrailer.sCovertArms.bAnimDictRequested
//		REMOVE_ANIM_DICT(thisCreatorTrailer.sCovertArms.sCAAnimDict)
//	ENDIF
//	
//	IF thisCreatorTrailer.sCovertArms.ContextIntention != NEW_CONTEXT_INTENTION
//		RELEASE_CONTEXT_INTENTION(thisCreatorTrailer.sCovertArms.ContextIntention)
//	ENDIF
//	
//	g_bRefreshTruckActivities = FALSE
	
	CLEANUP_ARMORY_TRUCK_AUDIO()
	
	CLEANUP_PLAYER_VEHICLE_FLAGS()
	
	CLEAN_UP_PERSONAL_CAR_MOD()
	
	CLEANUP_ARMORY_TRUCK_SECTIONS(thisCreatorTrailer.sPropertySections)
	
	CLEAN_UP_CREATOR_TRAILER_GUN_LOCKER(sArmoryTruckGunLocker)
	
	CLEANUP_CREATOR_TRAILER_MONITORS(sTruckMonitorData)
	
	IF IS_NET_PLAYER_OK(PLAYER_ID())
	AND IS_BIT_SET(thisCreatorTrailer.iLocalBS,BS_CREATOR_TRAILER_PREVENT_JACKING)
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_NotAllowedToJackAnyPlayers, FALSE)
	ENDIF
	
	CLEANUP_SECONDARY_EXIT_LOCATE()
	
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ INITIALISATION  ╞════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC INITIALISE_COMMAND_CENTER_EXIT()
	g_SimpleInteriorData.bRefreshInteriorMenu = FALSE
	g_SimpleInteriorData.bTruckCabOwnerAccess = FALSE

	thisCreatorTrailer.vLoateCoords[0] =  <<1104.434814,-3013.061035,-38.999458>>
	thisCreatorTrailer.vLocateDimensions[0] = <<0.300000,0.550000,1.000000>> 
	thisCreatorTrailer.vLoateCoords[1] =  <<1103.562134,-3013.763916,-38.748753>>
	thisCreatorTrailer.vLocateDimensions[1] = <<0.500000,0.500000,1.250000>> 
	thisCreatorTrailer.fExitLocateHeading = 349.3822
ENDPROC

FUNC STRING GET_AMBIENT_ZONE_FOR_THIS_ARMORY_TRUCK_SECTION(INT iSection)
	ARMORY_TRUCK_SECTIONS_ENUM eSection
	eSection = INT_TO_ENUM(ARMORY_TRUCK_SECTIONS_ENUM, iSection)
	
	IF thisCreatorTrailer.pOwner != INVALID_PLAYER_INDEX()
		SWITCH iSection
			CASE 0
				IF GET_PLAYER_TRUCK_PURCHASED_SECTIONS(eSection, thisCreatorTrailer.pOwner) = AT_ST_LIVING_ROOM
					RETURN "AZ_DLC_GR_MOC_B1_Living"
				ELIF GET_PLAYER_TRUCK_PURCHASED_SECTIONS(eSection, thisCreatorTrailer.pOwner) = AT_ST_COMMAND_CENTER
					RETURN "AZ_DLC_GR_MOC_B1_Command"
				ENDIF
			BREAK
			
			CASE 1
				IF GET_PLAYER_TRUCK_PURCHASED_SECTIONS(eSection, thisCreatorTrailer.pOwner) = AT_ST_CARMOD
					RETURN "AZ_DLC_GR_MOC_B2_B3_Weapons_Vehicle"
				ELIF GET_PLAYER_TRUCK_PURCHASED_SECTIONS(eSection, thisCreatorTrailer.pOwner) = AT_ST_GUNMOD
					RETURN "AZ_DLC_GR_MOC_B2_Weapons"
				ELIF GET_PLAYER_TRUCK_PURCHASED_SECTIONS(eSection, thisCreatorTrailer.pOwner) = AT_ST_LIVING_ROOM
					RETURN "AZ_DLC_GR_MOC_B2_Living"
				ELSE
					RETURN "AZ_DLC_GR_MOC_B2_Empty"
				ENDIF
			BREAK
			
			CASE 2
				IF GET_PLAYER_TRUCK_PURCHASED_SECTIONS(eSection, thisCreatorTrailer.pOwner) = AT_ST_VEHICLE_STORAGE
					RETURN "AZ_DLC_GR_MOC_B3_Vehicle"
				ELIF GET_PLAYER_TRUCK_PURCHASED_SECTIONS(eSection, thisCreatorTrailer.pOwner) = AT_ST_LIVING_ROOM
					RETURN "AZ_DLC_GR_MOC_B3_Living"
				ELIF GET_PLAYER_TRUCK_PURCHASED_SECTIONS(eSection, thisCreatorTrailer.pOwner) = AT_ST_GUNMOD
					RETURN "AZ_DLC_GR_MOC_B3_Weapons"
				ELSE
					IF GET_PLAYER_TRUCK_PURCHASED_SECTIONS(ATS_SECOND_SECTION, thisCreatorTrailer.pOwner) != AT_ST_CARMOD
						RETURN "AZ_DLC_GR_MOC_B3_Empty"
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	
	RETURN ""
ENDFUNC

PROC INITIALISE_ARMORY_TRUCK_AUDIO()
	INT i
	STRING sTempAmbientZone
	
	REPEAT 3 i
		sTempAmbientZone = GET_AMBIENT_ZONE_FOR_THIS_ARMORY_TRUCK_SECTION(i)
		
		IF NOT IS_STRING_NULL_OR_EMPTY(sTempAmbientZone) 
			// Start ambient zone
			thisCreatorTrailer.sAmbientZone[i] = sTempAmbientZone
			
			#IF IS_DEBUG_BUILD
			PRINTLN("AM_MP_ARMORY_TRUCK - INITIALISE_ARMORY_TRUCK_AUDIO - Starting ambient zone ", thisCreatorTrailer.sAmbientZone[i])
			#ENDIF
			
			SET_AMBIENT_ZONE_STATE(thisCreatorTrailer.sAmbientZone[i], TRUE, TRUE)
		ENDIF
	ENDREPEAT
ENDPROC

//PROC INIT_BED_DATA()
//	INT i
//	
//	IF g_bRefreshTruckActivities
//		IF NOT HAS_NET_TIMER_STARTED(thisCreatorTrailer.sResetBedsDelay)
//			REPEAT 3 i
//				bBedInit[i] = FALSE
//			ENDREPEAT
//			
//			START_NET_TIMER(thisCreatorTrailer.sResetBedsDelay)
//		ELIF HAS_NET_TIMER_EXPIRED(thisCreatorTrailer.sResetBedsDelay, 100)
//			g_bRefreshTruckActivities = FALSE
//			
//			RESET_NET_TIMER(thisCreatorTrailer.sResetBedsDelay)
//		ENDIF
//		
//		EXIT
//	ENDIF
//	
//	IF HAVE_ARMORY_TRUCK_SECTIONS_LOADED()
//		MP_PROP_OFFSET_STRUCT tempOffset
//		ARMORY_TRUCK_SECTIONS_ENUM iSection
//		
//		REPEAT 3 i
//			iSection = INT_TO_ENUM(ARMORY_TRUCK_SECTIONS_ENUM, i)
//			
//			IF GET_PLAYER_TRUCK_PURCHASED_SECTIONS(iSection, thisCreatorTrailer.pOwner) = AT_ST_LIVING_ROOM
//				IF NOT bBedInit[i]
//					CDEBUG1LN(DEBUG_SAFEHOUSE, "INIT_BED_DATA: living room section exists: ", iSection)
//					
//					GET_ARMORY_TRUCK_PROP_TRANSFORM(ATP_LEFT_BED_COORD, iSection, tempOffset)
//					bedStructLeft[i].vScenePos = tempOffset.vLoc
//					bedStructLeft[i].vSceneRot = tempOffset.vRot
//					GET_ARMORY_TRUCK_PROP_TRANSFORM(ATP_LEFT_BED_BOX_A, iSection, tempOffset)
//					bedStructLeft[i].vBoundingBoxA = tempOffset.vLoc
//					GET_ARMORY_TRUCK_PROP_TRANSFORM(ATP_LEFT_BED_BOX_B, iSection, tempOffset)
//					bedStructLeft[i].vBoundingBoxB = tempOffset.vLoc
//					bedStructLeft[i].fWidth = 2.8125
//					bedStructLeft[i].iScriptInstanceID = 1
//					bedStructLeft[i].bEnterRightBedSide = TRUE
//					
//					GET_ARMORY_TRUCK_PROP_TRANSFORM(ATP_RIGHT_BED_COORD, iSection, tempOffset)
//					bedStructRight[i].vScenePos = tempOffset.vLoc
//					bedStructRight[i].vSceneRot = tempOffset.vRot
//					GET_ARMORY_TRUCK_PROP_TRANSFORM(ATP_RIGHT_BED_BOX_A, iSection, tempOffset)
//					bedStructRight[i].vBoundingBoxA = tempOffset.vLoc
//					GET_ARMORY_TRUCK_PROP_TRANSFORM(ATP_RIGHT_BED_BOX_B, iSection, tempOffset)
//					bedStructRight[i].vBoundingBoxB = tempOffset.vLoc
//					bedStructRight[i].fWidth = 2.8125
//					bedStructRight[i].iScriptInstanceID = 2
//					bedStructRight[i].bEnterRightBedSide = FALSE
//					
//					bBedInit[i] = TRUE
//				ENDIF
//			ELSE
//				IF bBedInit[i]
//					bBedInit[i] = FALSE
//				ENDIF
//			ENDIF
//		ENDREPEAT
//	ENDIF
//ENDPROC

PROC INITIALISE()
	g_bPlayerLeavingCurrentInteriorInVeh = FALSE
	
	SET_KICK_OUT_PLAYER_FROM_ARMORY_TRUCK(FALSE)
	SET_EMPTY_ARMORY_TRUCK_TRAILER(FALSE)
	IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_PLAYER_INSIDE_BUNKER_ON_TRUCK_ENTRY_CORONA)
		CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_PLAYER_INSIDE_BUNKER_ON_TRUCK_ENTRY_CORONA)
		PRINTLN("INITIALISE BS_SIMPLE_INTERIOR_GLOBAL_DATA_PLAYER_INSIDE_BUNKER_ON_TRUCK_ENTRY_CORONA FALSE")
	ENDIF
	
	INITIALISE_COMMAND_CENTER_EXIT()
	INITIALISE_ARMORY_TRUCK_AUDIO()
	
//	INIT_BED_DATA()
	SET_SIMPLE_INTERIOR_CHILD_SCRIPT_ALLOWS_FOR_WARP(TRUE)
ENDPROC


// INIT FACTORY SERVER DETAILS
PROC INITIALISE_FACTORY_TOTALS()
//	thisCreatorTrailer.iProductSlot = GET_PRODUCT_TOTAL_FOR_FACTORY(thisCreatorTrailer.pOwner, thisCreatorTrailer.eID)
	CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_CREATOR_TRAILER_WAREHOUSE - INITIALISE_FACTORY_TOTALS =- - - - - - - - - =")
//	CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_CREATOR_TRAILER_WAREHOUSE - INITIALISE_FACTORY_TOTALS - Factory: ", thisCreatorTrailer.eID)//, " Product Total: ", thisCreatorTrailer.iProductSlot)
	
//	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
//		serverBD.iMaterialsTotal = GET_MATERIALS_TOTAL_FOR_FACTORY(thisCreatorTrailer.pOwner, thisCreatorTrailer.eID)
//		serverBD.iProductionStage = GET_PRODUCTION_STATE_FOR_FACTORY(thisCreatorTrailer.pOwner, thisCreatorTrailer.eID)
//		serverBD.bActiveState = IS_FACTORY_PRODUCTION_ACTIVE(thisCreatorTrailer.pOwner, thisCreatorTrailer.iSaveSlot)
////		thisCreatorTrailer.iUpgradeState = FACTORY_UPGRADE_STATE_FADE_OUT
//	ENDIF
	
//	CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_CREATOR_TRAILER_WAREHOUSE - INITIALISE_FACTORY_TOTALS - Factory: ", thisCreatorTrailer.eID, " Materials Total: ", serverBD.iMaterialsTotal)
//	CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_CREATOR_TRAILER_WAREHOUSE - INITIALISE_FACTORY_TOTALS - Factory: ", thisCreatorTrailer.eID, " Production Stage: ", serverBD.iProductionStage)
////	CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_CREATOR_TRAILER_WAREHOUSE - INITIALISE_FACTORY_TOTALS - Factory: ", thisCreatorTrailer.eID, " Type: ", GET_FACTORY_TYPE_NAME(thisCreatorTrailer.eFactoryType))
//	CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_CREATOR_TRAILER_WAREHOUSE - INITIALISE_FACTORY_TOTALS - Factory: ", thisCreatorTrailer.eID, " Slot: ", thisCreatorTrailer.iSaveSlot)
//	CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_CREATOR_TRAILER_WAREHOUSE - INITIALISE_FACTORY_TOTALS - Factory: ", thisCreatorTrailer.eID, " Active State: ", serverBD.bActiveState)
//	
//	IF HAS_PLAYER_COMPLETED_FACTORY_SETUP_MISSION(thisCreatorTrailer.pOwner, GET_FACTORY_ID_FROM_FACTORY_SLOT(thisCreatorTrailer.pOwner, thisCreatorTrailer.iSaveSlot))
//		CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_CREATOR_TRAILER_WAREHOUSE - INITIALISE_FACTORY_TOTALS - Factory: ", thisCreatorTrailer.eID, " has completed its setup mission")
//	ELSE
//		CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_CREATOR_TRAILER_WAREHOUSE - INITIALISE_FACTORY_TOTALS - Factory: ", thisCreatorTrailer.eID, " has NOT completed its setup mission")
//	ENDIF
//	IF GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(thisCreatorTrailer.pOwner, TRUE)
//		CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_CREATOR_TRAILER_WAREHOUSE - INITIALISE_FACTORY_TOTALS - Player currently on a gang boss mission")
//	ELSE
//		CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_CREATOR_TRAILER_WAREHOUSE - INITIALISE_FACTORY_TOTALS - Player currently NOT on a gang boss mission")
//	ENDIF
//	IF g_sMPTunables.bBIKER_STOP_PRODUCTION_WHEN_NOT_MC_PRESIDENT
//	AND NOT GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(thisCreatorTrailer.pOwner)
//		CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_CREATOR_TRAILER_WAREHOUSE - INITIALISE_FACTORY_TOTALS - Factory: ", thisCreatorTrailer.eID, " Inactive because player is not MC and bBIKER_STOP_PRODUCTION_WHEN_NOT_MC_PRESIDENT tunable on")
//	ENDIF
ENDPROC

// FACTORY PED PROCEDURES
PROC CACHE_FACTORY_STATE()

//	interiorStruct.cachedFactoryState.iProductionState = serverBD.iProductionStage 
//	CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CACHE_FACTORY_STATE: interiorStruct.cachedFactoryState.iProductionState  = ", interiorStruct.cachedFactoryState.iProductionState  )
//	interiorStruct.cachedFactoryState.iProductionTotal = thisCreatorTrailer.iProductSlot
//	CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CACHE_FACTORY_STATE: interiorStruct.cachedFactoryState.iProductionTotal = ", interiorStruct.cachedFactoryState.iProductionTotal )
//	interiorStruct.cachedFactoryState.iMaterialsTotalForFactory = serverBD.iMaterialsTotal
//	CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CACHE_FACTORY_STATE: interiorStruct.cachedFactoryState.iMaterialsTotalForFactory = ", interiorStruct.cachedFactoryState.iMaterialsTotalForFactory)
	
ENDPROC

//PROC SET_DRIVERS_WANTED_LEVEL(INT iWantedLevel = 0)
//	IF iWantedLevel = 0
//		iWantedLevel = 3
//		CDEBUG1LN(DEBUG_NET_GUN_TURRET, "SET_DRIVERS_WANTED_LEVEL: wanted level set to 0, defaulting to 3")
//	ENDIF
//	
//	IF NOT IS_ENTITY_DEAD(g_viGunRunTruckCabIamIn)
//		IF NOT IS_VEHICLE_SEAT_FREE(g_viGunRunTruckCabIamIn, VS_DRIVER)
//		
//			IF GET_PED_IN_VEHICLE_SEAT(g_viGunRunTruckCabIamIn, VS_DRIVER) = PLAYER_PED_ID()
//		
//				SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), iWantedLevel)
//				CDEBUG1LN(DEBUG_NET_GUN_TURRET, "SET_DRIVERS_WANTED_LEVEL: setting the drivers wanted level to ", iWantedLevel)
//			ENDIF
//		ENDIF
//	ENDIF
//ENDPROC


//FUNC BOOL DO_ANY_PLAYERS_IN_TRUCK_INTERIOR_HAVE_WANTED_LEVELS(INT &iWantedLevel)
//	CDEBUG1LN(DEBUG_NET_GUN_TURRET, "DO_ANY_PLAYERS_IN_TRUCK_INTERIOR_HAVE_WANTED_LEVELS: ")
//	INT iPlayerCount 
//	iPlayerCount = 0
//	BOOL bReturn
//	INT iTempWantedLevel
//	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iPlayerCount
//		IF NETWORK_IS_PARTICIPANT_ACTIVE( INT_TO_PARTICIPANTINDEX(iPlayerCount) )
//			PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPlayerCount))
//			
//			
//			IF NETWORK_IS_PLAYER_A_PARTICIPANT(PlayerId)
//			AND NOT IS_ENTITY_DEAD(GET_PLAYER_PED(PlayerId))
//				iTempWantedLevel = GET_PLAYER_WANTED_LEVEL(PlayerId)
//			ENDIF
//			
//			IF iTempWantedLevel > 0
//				CDEBUG1LN(DEBUG_NET_GUN_TURRET, "DO_ANY_PLAYERS_IN_TRUCK_INTERIOR_HAVE_WANTED_LEVELS: Player ", GET_PLAYER_NAME(PlayerId), " has ", iTempWantedLevel, " wanted level")
//				bReturn = TRUE
//			ENDIF
//			
//			IF iTempWantedLevel > iWantedLevel
//				CDEBUG1LN(DEBUG_NET_GUN_TURRET, "DO_ANY_PLAYERS_IN_TRUCK_INTERIOR_HAVE_WANTED_LEVELS: Player ", GET_PLAYER_NAME(PlayerId), " has a higher wanted level: ", iTempWantedLevel)
//				iWantedLevel = iTempWantedLevel
//			ENDIF
//		ENDIF
//	ENDREPEAT
//	RETURN bReturn
//ENDFUNC 

//PROC MAINTAIN_TRUCK_WANTED_LEVELS()
//	INT iWantedLevel = 0
//	IF DO_ANY_PLAYERS_IN_TRUCK_INTERIOR_HAVE_WANTED_LEVELS(iWantedLevel)
//		SET_DRIVERS_WANTED_LEVEL(iWantedLevel)
//	ENDIF
//ENDPROC

PROC RUN_TRUCK_FOCUS_LOGIC()
	IF g_iShouldLaunchTruckTurret != -1
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciALLOW_MOC_ENTRY_THIS_MISSION)
			IF NOT IS_ENTITY_DEAD(MPGlobalsAmbience.vehCreatorTrailer)
				IF NOT IS_ENTITY_FOCUS(MPGlobalsAmbience.vehCreatorTrailer)
					SET_FOCUS_ENTITY(MPGlobalsAmbience.vehCreatorTrailer)
					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "RUN_TRUCK_FOCUS_LOGIC: SET_FOCUS_ENTITY MPGlobalsAmbience.vehCreatorTrailer")
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_ENTITY_DEAD(g_viGunRunTruckTailerIamIn)
				IF NOT IS_ENTITY_FOCUS(g_viGunRunTruckTailerIamIn)
					SET_FOCUS_ENTITY(g_viGunRunTruckTailerIamIn)
					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "RUN_TRUCK_FOCUS_LOGIC: SET_FOCUS_ENTITY g_viGunRunTruckTailerIamIn")
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciALLOW_MOC_ENTRY_THIS_MISSION)
			IF NOT IS_ENTITY_DEAD(MPGlobalsAmbience.vehCreatorTrailer)
				IF IS_ENTITY_FOCUS(MPGlobalsAmbience.vehCreatorTrailer)
					CLEAR_FOCUS()
					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "RUN_TRUCK_FOCUS_LOGIC: CLEAR_FOCUS")
				ENDIF
			ELSE
				// is there a situation where the entity could be dead and still in focus
				// do I need to clear focus here too?
			ENDIF
		ELSE
			IF NOT IS_ENTITY_DEAD(g_viGunRunTruckTailerIamIn)
				IF IS_ENTITY_FOCUS(g_viGunRunTruckTailerIamIn)
					CLEAR_FOCUS()
					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "RUN_TRUCK_FOCUS_LOGIC: CLEAR_FOCUS")
				ENDIF
			ELSE
				// is there a situation where the entity could be dead and still in focus
				// do I need to clear focus here too?
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC RUN_ACTIVITY_LOGIC()
	interiorStruct.pFactoryOwner 	= thisCreatorTrailer.pOwner
//	interiorStruct.eFactoryID	 	= thisCreatorTrailer.eID
	interiorStruct.vInteriorPos 	= <<1103.562,-3014.000,-40.000>>
	
	TEXT_LABEL_63 tlTemp
	FLOAT fTemp
	GET_SIMPLE_INTERIOR_INTERIOR_TYPE_AND_POSITION(thisCreatorTrailer.eSimpleInteriorID, tlTemp, interiorStruct.vInteriorPos, fTemp)
	
	interiorStruct.iInterior 		= GET_HASH_KEY("FACTORY_TRUCK")
	
//	INT i
//	REPEAT 3 i
//		IF bBedInit[i]
//			BED_SCRIPT_LAUNCH_LOGIC(bedStructLeft[i])
//			BED_SCRIPT_LAUNCH_LOGIC(bedStructRight[i])
//		ENDIF
//	ENDREPEAT
	
	// Runs activity creator logic
	
//	MAINTAIN_TRUCK_WANTED_LEVELS()
	
	IF interiorStruct.iInterior != 0
		INIT_ACTIVITY_LAUNCH_SCRIPT_CHECK_FOR_TRUCK(activityControllerStruct, activityCheck, serverBD.activityProps, interiorStruct)
		MAINTAIN_APARTMENT_ACTIVITIES_CREATOR(activityControllerStruct, activityCheck, serverBD.activityProps, activityPeds)
	ENDIF
	RUN_TRUCK_FOCUS_LOGIC()
	#IF IS_DEBUG_BUILD
	CHECK_TO_LAUNCH_ACTIVITY_CREATOR_UI_SCRIPT(interiorStruct)
	#ENDIF
ENDPROC
// -*

#IF IS_DEBUG_BUILD
FUNC STRING GET_COVERT_ARMS_STATE_DEBUG_STRING(CREATOR_TRAILER_COVERT_ARMS_STATE eState)
	SWITCH eState
		CASE CA_STATE_IDLE						RETURN "CA_STATE_IDLE"
		CASE CA_STATE_IN_ANGLED_AREA			RETURN "CA_STATE_IN_ANGLED_AREA"
		CASE CA_STATE_TRIGGERED					RETURN "CA_STATE_TRIGGERED"
		CASE CA_STATE_EXIT						RETURN "CA_STATE_EXIT"
	ENDSWITCH
	
	RETURN "**State Unknown**"
ENDFUNC
#ENDIF

FUNC CREATOR_TRAILER_COVERT_ARMS_STATE GET_CURRENT_COVERT_ARMS_STATE()
	RETURN thisCreatorTrailer.sCovertArms.eUseState
ENDFUNC

PROC SET_CURRENT_COVERT_ARMS_STATE(CREATOR_TRAILER_COVERT_ARMS_STATE eNewState)
	IF eNewState != thisCreatorTrailer.sCovertArms.eUseState
		PRINTLN("SET_CURRENT_COVERT_ARMS_STATE new state = ", GET_COVERT_ARMS_STATE_DEBUG_STRING(eNewState))
		thisCreatorTrailer.sCovertArms.eUseState = eNewState
	ENDIF
ENDPROC

PROC SET_CURRENT_COVERT_ARMS_TV_IN_USE(ARMORY_TRUCK_SECTION_TYPE_ENUM eSectionType)
	IF eSectionType != thisCreatorTrailer.sCovertArms.eTVInUse
		PRINTLN("SET_CURRENT_COVERT_ARMS_TV_IN_USE new TV = ", eSectionType)
		thisCreatorTrailer.sCovertArms.eTVInUse = eSectionType
	ENDIF
ENDPROC

FUNC ARMORY_TRUCK_SECTION_TYPE_ENUM GET_CURRENT_COVERT_ARMS_TV_IN_USE()
	RETURN thisCreatorTrailer.sCovertArms.eTVInUse
ENDFUNC

PROC SET_CURRENT_COVERT_ARMS_SECTION_IN_USE(ARMORY_TRUCK_SECTIONS_ENUM eSection)
	IF eSection != thisCreatorTrailer.sCovertArms.eCurrentSection
		PRINTLN("SET_CURRENT_COVERT_ARMS_SECTION_TV_IN_USE new section = ", eSection)
		thisCreatorTrailer.sCovertArms.eCurrentSection = eSection
	ENDIF
ENDPROC

FUNC ARMORY_TRUCK_SECTIONS_ENUM GET_CURRENT_COVERT_ARMS_SECTION_IN_USE()
	RETURN thisCreatorTrailer.sCovertArms.eCurrentSection
ENDFUNC

FUNC BOOL DOES_ARMOURY_TRUCK_TV_EXIST(ARMORY_TRUCK_SECTION_TYPE_ENUM eSectionType, ARMORY_TRUCK_SECTIONS_ENUM eSection)
	IF eSectionType = AT_ST_COMMAND_CENTER
		RETURN (DOES_ENTITY_EXIST(thisCreatorTrailer.sPropertySections.obTV[0]) AND NOT IS_ENTITY_DEAD(thisCreatorTrailer.sPropertySections.obTV[0]))
	ELIF eSectionType = AT_ST_LIVING_ROOM
		SWITCH eSection
			CASE ATS_FIRST_SECTION
				RETURN (DOES_ENTITY_EXIST(thisCreatorTrailer.sPropertySections.obTV[0]) AND NOT IS_ENTITY_DEAD(thisCreatorTrailer.sPropertySections.obTV[0]))
			CASE ATS_SECOND_SECTION
				RETURN (DOES_ENTITY_EXIST(thisCreatorTrailer.sPropertySections.obTV[1]) AND NOT IS_ENTITY_DEAD(thisCreatorTrailer.sPropertySections.obTV[1]))
			CASE ATS_THIRD_SECTION
				RETURN (DOES_ENTITY_EXIST(thisCreatorTrailer.sPropertySections.obTV[2]) AND NOT IS_ENTITY_DEAD(thisCreatorTrailer.sPropertySections.obTV[2]))
		ENDSWITCH
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_IN_COVERT_ARMS_TV_LOCATE(ARMORY_TRUCK_SECTION_TYPE_ENUM eSectionType, ARMORY_TRUCK_SECTIONS_ENUM eSection)
	
	VECTOR vLocateCoords
	
	IF eSectionType = AT_ST_COMMAND_CENTER
	AND DOES_ARMOURY_TRUCK_TV_EXIST(AT_ST_COMMAND_CENTER, eSection)
				
		vLocateCoords = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(thisCreatorTrailer.sPropertySections.obTV[0], <<0.711071, 2.427979, 1.000546>>)
		
		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vLocateCoords, <<0.437500,0.625000,1.000000>>)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF eSectionType = AT_ST_LIVING_ROOM
	AND DOES_ARMOURY_TRUCK_TV_EXIST(AT_ST_LIVING_ROOM, eSection)
		SWITCH eSection
			CASE ATS_FIRST_SECTION
				vLocateCoords = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(thisCreatorTrailer.sPropertySections.obTV[0], <<-1.250477, 4.821289, 1.050068>>)
			BREAK
			CASE ATS_SECOND_SECTION
				vLocateCoords = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(thisCreatorTrailer.sPropertySections.obTV[1], <<-1.250477, 4.821289, 1.050068>>)
			BREAK
			CASE ATS_THIRD_SECTION
				vLocateCoords = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(thisCreatorTrailer.sPropertySections.obTV[2], <<-1.250477, 4.821289, 1.050068>>)
			BREAK
		ENDSWITCH
		
		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vLocateCoords, <<0.437500,0.625000,1.000000>>)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC OBJECT_INDEX GET_TV_OBJECT_FROM_TRUCK_SECTION(ARMORY_TRUCK_SECTIONS_ENUM eSection)
	OBJECT_INDEX obReturnObject
	
	SWITCH eSection
		CASE ATS_FIRST_SECTION
			obReturnObject = thisCreatorTrailer.sPropertySections.obTV[0]
		BREAK
		CASE ATS_SECOND_SECTION
			obReturnObject = thisCreatorTrailer.sPropertySections.obTV[1]
		BREAK
		CASE ATS_THIRD_SECTION
			obReturnObject = thisCreatorTrailer.sPropertySections.obTV[2]
		BREAK
		DEFAULT
			CASSERTLN(DEBUG_SAFEHOUSE, "GET_TV_OBJECT_FROM_TRUCK_SECTION passed invalid truck section!")
			obReturnObject = thisCreatorTrailer.sPropertySections.obTV[0]
	ENDSWITCH
	
	RETURN obReturnObject
ENDFUNC

FUNC BOOL IS_PLAYER_IN_ANY_COVERT_ARMS_TV_LOCATE()

	ARMORY_TRUCK_SECTIONS_ENUM eSection = INT_TO_ENUM(ARMORY_TRUCK_SECTIONS_ENUM, (GET_FRAME_COUNT() % 3))
	
	SWITCH eSection
	
		CASE ATS_FIRST_SECTION
			IF IS_LOCAL_PLAYER_IN_COVERT_ARMS_TV_LOCATE(AT_ST_COMMAND_CENTER, eSection)
				SET_CURRENT_COVERT_ARMS_TV_IN_USE(AT_ST_COMMAND_CENTER)
				SET_CURRENT_COVERT_ARMS_SECTION_IN_USE(ATS_FIRST_SECTION)
				RETURN TRUE
			ENDIF
		BREAK
		CASE ATS_SECOND_SECTION
		CASE ATS_THIRD_SECTION
			IF IS_LOCAL_PLAYER_IN_COVERT_ARMS_TV_LOCATE(AT_ST_LIVING_ROOM, eSection)
			
				SET_CURRENT_COVERT_ARMS_TV_IN_USE(AT_ST_LIVING_ROOM)
				SET_CURRENT_COVERT_ARMS_SECTION_IN_USE(eSection)
				RETURN TRUE
				
			ENDIF
		BREAK
	ENDSWITCH
	
	SET_CURRENT_COVERT_ARMS_SECTION_IN_USE(ATS_FIRST_SECTION)
	SET_CURRENT_COVERT_ARMS_TV_IN_USE(AT_ST_UNDEFINED)
	RETURN FALSE
ENDFUNC

// PURPOSE:
//    Function for using the up to 3 TVs to launch the covert arms app
PROC RUN_COVERT_ARMS_LOGIC()
	
	IF NOT DOES_ENTITY_EXIST(PLAYER_PED_ID())
	OR IS_ENTITY_DEAD(PLAYER_PED_ID())
		PRINTLN("AM_MP_CREATOR_TRAILER RUN_COVERT_ARMS_LOGIC - EXIT player ped is dead or does not exist")
		EXIT
	ENDIF
	
	SWITCH GET_CURRENT_COVERT_ARMS_STATE()	
		CASE CA_STATE_IDLE
			IF IS_PLAYER_IN_ANY_COVERT_ARMS_TV_LOCATE()
				SET_CURRENT_COVERT_ARMS_STATE(CA_STATE_IN_ANGLED_AREA)
			ENDIF
		BREAK
		
		CASE CA_STATE_IN_ANGLED_AREA
			IF IS_LOCAL_PLAYER_IN_COVERT_ARMS_TV_LOCATE(GET_CURRENT_COVERT_ARMS_TV_IN_USE(), GET_CURRENT_COVERT_ARMS_SECTION_IN_USE())
				IF thisCreatorTrailer.sCovertArms.ContextIntention = NEW_CONTEXT_INTENTION
				
					REGISTER_CONTEXT_INTENTION(thisCreatorTrailer.sCovertArms.ContextIntention, CP_MEDIUM_PRIORITY, "", TRUE)
					
				ELIF HAS_CONTEXT_BUTTON_TRIGGERED(thisCreatorTrailer.sCovertArms.ContextIntention)
				
					RELEASE_CONTEXT_INTENTION(thisCreatorTrailer.sCovertArms.ContextIntention)
					SET_CURRENT_COVERT_ARMS_STATE(CA_STATE_TRIGGERED)
					
				ENDIF
			ELSE
				IF thisCreatorTrailer.sCovertArms.ContextIntention != NEW_CONTEXT_INTENTION
					RELEASE_CONTEXT_INTENTION(thisCreatorTrailer.sCovertArms.ContextIntention)
					SET_CURRENT_COVERT_ARMS_STATE(CA_STATE_IDLE)
				ENDIF
			ENDIF
		BREAK
		
		CASE CA_STATE_TRIGGERED
			CLEAR_HELP()
			PRINT_HELP("CT_TV_BLOCKED")
			SET_CURRENT_COVERT_ARMS_STATE(CA_STATE_EXIT)
		BREAK
		
		CASE CA_STATE_EXIT
			IF NOT HAS_NET_TIMER_STARTED(thisCreatorTrailer.sCovertArms.stHelpReset)
				START_NET_TIMER(thisCreatorTrailer.sCovertArms.stHelpReset)
			ELIF HAS_NET_TIMER_EXPIRED(thisCreatorTrailer.sCovertArms.stHelpReset, 10500)
				SET_CURRENT_COVERT_ARMS_STATE(CA_STATE_IDLE)
				RESET_NET_TIMER(thisCreatorTrailer.sCovertArms.stHelpReset)
			ENDIF
		BREAK		
				
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Usees the iPlayerIJoinedBS to find the owner of the bunker we have entered
/// RETURNS:
///    TRUE when know who the owner is
FUNC BOOL DETERMINE_CREATOR_TRAILER_OWNER()
	//We have to search for the owner as we can't rely on gang membership
	IF IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_CREATOR_TRAILER_ACCESS_BS_OWNER)
	OR GB_GET_LOCAL_PLAYER_GANG_BOSS() = PLAYER_ID()
		thisCreatorTrailer.pOwner = PLAYER_ID()
		g_ownerOfArmoryTruckPropertyIAmIn = thisCreatorTrailer.pOwner
		PRINTLN("AM_MP_CREATOR_TRAILER - DETERMINE_CREATOR_TRAILER_OWNER - I ", GET_PLAYER_NAME(thisCreatorTrailer.pOwner), " am the owner. access BS =  ", g_SimpleInteriorData.iAccessBS)
		RETURN TRUE
	ELIF g_SimpleInteriorData.iAccessBS > 0
		IF globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner != INVALID_PLAYER_INDEX()
			thisCreatorTrailer.pOwner = globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner
			g_ownerOfArmoryTruckPropertyIAmIn = thisCreatorTrailer.pOwner
			PRINTLN("AM_MP_CREATOR_TRAILER - DETERMINE_CREATOR_TRAILER_OWNER - I am not the owner. The owner is: ", GET_PLAYER_NAME(thisCreatorTrailer.pOwner))
			RETURN TRUE
		ENDIF
		IF IS_PLAYER_MOVING_FROM_BUNKER_TO_TRUCK()
			IF IS_PLAYER_NEED_TO_CHECK_FOR_NEARBY_PLAYERS(PLAYER_ID())
				thisCreatorTrailer.pOwner = PLAYER_ID()
				g_ownerOfArmoryTruckPropertyIAmIn = PLAYER_ID()
				SET_BIT(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_CREATOR_TRAILER_ACCESS_BS_OWNER)
				SET_LOCAL_PLAYER_CHECK_FOR_NEARBY_PLAYERS(FALSE)
				PRINTLN("AM_MP_CREATOR_TRAILER - DETERMINE_CREATOR_TRAILER_OWNER - IS_PLAYER_MOVING_FROM_BUNKER_TO_TRUCK I'm owner.")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	PRINTLN("AM_MP_CREATOR_TRAILER - DETERMINE_CREATOR_TRAILER_OWNER - g_SimpleInteriorData.iAccessBS = ", g_SimpleInteriorData.iAccessBS)
	#ENDIF
	
	IF (GET_FRAME_COUNT() % 60) = 0
		PRINTLN("AM_MP_CREATOR_TRAILER - DETERMINE_CREATOR_TRAILER_OWNER - Trying to find the owner")
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SCRIPT_INITIALISE(SIMPLE_INTERIOR_CHILD_SCRIPT_DATA& scriptData)
	
	INITALISE_CREATOR_TRAILER_RENDER_TARGET_IDS(sTruckMonitorData)
	SET_TRUCK_PERSONAL_CAR_MOD_TO_LEAVE_PROPERTY(FALSE)
	SET_I_NEED_TO_RENOVATE_TRUCK_TRAILER(FALSE)
	
	WHILE NOT DETERMINE_CREATOR_TRAILER_OWNER()
		
		IF NOT NETWORK_IS_GAME_IN_PROGRESS()
			PRINTLN("AM_MP_CREATOR_TRAILER - NETWORK_IS_GAME_IN_PROGRESS = FALSE")
			SCRIPT_CLEANUP()
		ENDIF
		
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			PRINTLN("AM_MP_CREATOR_TRAILER - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE = TRUE")
			SCRIPT_CLEANUP()
		ENDIF
		
		IF SHOULD_THIS_SIMPLE_INTERIOR_CHILD_THREAD_TERMINATE(scriptData.iScriptInstance, scriptData.eSimpleInteriorID)
			PRINTLN("AM_MP_CREATOR_TRAILER - SHOULD_THIS_SIMPLE_INTERIOR_CHILD_THREAD_TERMINATE = TRUE")
			SCRIPT_CLEANUP()
		ENDIF
		
		IF IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_CREATOR_TRAILER_ACCESS_BS_OWNER_LEFT_GAME)
			PRINTLN("AM_MP_CREATOR_TRAILER - SIMPLE_INTERIOR_BUNKER_ACCESS_BS_OWNER_LEFT_GAME = TRUE")
			SCRIPT_CLEANUP()
		ENDIF
		
		WAIT(0)
	ENDWHILE
	
	SET_PLAYER_NEEDS_TO_BE_CONCAEL_FOR_BUNKER_TRUCK_TRANS(FALSE)
	SET_PLAYER_NEEDS_TO_BE_CONCAEL_FOR_TRUCK_TO_CAB_TRANS(FALSE)
	
	thisCreatorTrailer.eSimpleInteriorID = scriptData.eSimpleInteriorID
	thisCreatorTrailer.iScriptInstance = scriptData.iScriptInstance
	thisCreatorTrailer.iInvitingPlayer = scriptData.iInvitingPlayer
//	thisCreatorTrailer.sCovertArms.sCAAnimDict = "anim@amb@trailer@touch_screen@"
	
	NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE, thisCreatorTrailer.iScriptInstance)
	
	// This makes sure the net script is active, waits untill it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	RESERVE_NETWORK_MISSION_VEHICLES(1) //for Personal vehicle

	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))

	// KGM: Wait for the first network broadcast before moving on
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		PRINTLN("AM_MP_CREATOR_TRAILER - FAILED TO RECEIVE INITIAL NETWORK BROADCAST - SCRIPT CLEANUP")
		SCRIPT_CLEANUP()
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		// This script will not be paused if another script calls PAUSE_GAME
		PRINTLN("AM_MP_CREATOR_TRAILER - INITIALISED")
	ELSE
		PRINTLN("AM_MP_CREATOR_TRAILER - NETWORK_IS_GAME_IN_PROGRESS = FALSE - SCRIPT CLEANUP")
		SCRIPT_CLEANUP()
	ENDIF
	
	//Set Portal modifier before inside the property.
	STRING sTimeCycleModifier = GET_ARMORY_TRUCK_TIMECYCLE_MODIFIER(GET_PLAYER_TRUCK_PURCHASED_SECTIONS_TINT(thisCreatorTrailer.pOwner))
	PRINTLN("AM_MP_CREATOR_TRAILER - TimeCycle: ", sTimeCycleModifier, " Owner: ", GET_PLAYER_NAME(thisCreatorTrailer.pOwner))
	ARMORY_TRUCK_SET_TIMECYCLE(sTimeCycleModifier)
	
	INITIALISE()
	
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ PROCEDURES  ╞═══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

FUNC BOOL IS_CREATOR_TRAILER_STATE(CREATOR_TRAILER_SCRIPT_STATE eState)
	RETURN thisCreatorTrailer.eState = eState
ENDFUNC

PROC SET_CREATOR_TRAILER_STATE(CREATOR_TRAILER_SCRIPT_STATE eState)
	CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] AM_MP_CREATOR_TRAILER - SET_CREATOR_TRAILER_STATE - New state: ", ENUM_TO_INT(eState))
	thisCreatorTrailer.eState = eState
	IF eState = CREATOR_TRAILER_STATE_IDLE
		CLEAR_GLOBAL_ENTRY_DATA()
		SET_DRIVER_ENTERING_SIMPLE_INTERIOR(FALSE)
	ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_IN_CREATOR_TRAILER_EXIT_LOCATE(ARMORY_TRUCK_SECTION_TYPE_ENUM eSectionType)

	IF eSectionType = AT_ST_COMMAND_CENTER
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND IS_ENTITY_AT_COORD(PLAYER_PED_ID(), thisCreatorTrailer.vLoateCoords[0], thisCreatorTrailer.vLocateDimensions[0], DEFAULT)
			RETURN TRUE
		ENDIF
	ELIF eSectionType = AT_ST_LIVING_ROOM
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND IS_ENTITY_AT_COORD(PLAYER_PED_ID(), thisCreatorTrailer.vLoateCoords[1], thisCreatorTrailer.vLocateDimensions[1], DEFAULT)
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

PROC START_CREATOR_TRAILER_EXIT()
	g_SimpleInteriorData.bShouldExitMenuBeVisible = FALSE
	
	IF NOT IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
		SET_CREATOR_TRAILER_STATE(CREATOR_TRAILER_STATE_GOTO_EXIT)
	ELSE
		SET_CREATOR_TRAILER_STATE(CREATOR_TRAILER_STATE_IDLE)
	ENDIF
ENDPROC

FUNC BOOL MAINTAIN_LAUNCHING_CARMOD_SCRIPT()
	
	IF thisCreatorTrailer.pOwner != INVALID_PLAYER_INDEX()
		IF IS_PLAYER_PURCHASED_ANY_CARMOD_SECTION_FOR_TRUCK(thisCreatorTrailer.pOwner)
		
			//Make sure the car mod script is not started too early after being killed
			//Make sure the car mod script cleanup flag is false
			IF g_bCleanUpCarmodShop = TRUE
				IF NOT HAS_NET_TIMER_STARTED(thisCreatorTrailer.sCarModScriptRunTimer)
					START_NET_TIMER(thisCreatorTrailer.sCarModScriptRunTimer)
				ELSE
					IF HAS_NET_TIMER_EXPIRED(thisCreatorTrailer.sCarModScriptRunTimer, 8000)
						g_bCleanUpCarmodShop = FALSE
						RESET_NET_TIMER(thisCreatorTrailer.sCarModScriptRunTimer)
						#IF IS_DEBUG_BUILD
						PRINTLN("MAINTAIN_LAUNCHING_CARMOD_SCRIPT - Setting g_bCleanUpCarmodShop To FALSE")
						#ENDIF
					ENDIF	
				ENDIF
			ENDIF
			
			IF NOT IS_BIT_SET(thisCreatorTrailer.iLocalBS,BS_CREATOR_TRAILER_IS_CAR_MOD_SCRIPT_READY)
				thisCreatorTrailer.sChildOfChildScript = "carmod_shop"
				REQUEST_SCRIPT(thisCreatorTrailer.sChildOfChildScript)
				IF HAS_SCRIPT_LOADED(thisCreatorTrailer.sChildOfChildScript)
				AND NOT IS_THREAD_ACTIVE(thisCreatorTrailer.CarModThread)
				AND !g_bCleanUpCarmodShop
					
					g_iCarModInstance = thisCreatorTrailer.iScriptInstance + PERSONAL_CAR_MOD_TRUCK_SCRIPT_INSTANCE_OFFSET
					
					IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(thisCreatorTrailer.sChildOfChildScript)) < 1
						IF NOT NETWORK_IS_SCRIPT_ACTIVE(thisCreatorTrailer.sChildOfChildScript, g_iCarModInstance, TRUE)
							SHOP_LAUNCHER_STRUCT sShopLauncherData
							sShopLauncherData.bLinkedShop = FALSE
							sShopLauncherData.eShop = CARMOD_SHOP_PERSONALMOD
							sShopLauncherData.iNetInstanceID = g_iCarModInstance
							sShopLauncherData.ePersonalCarModVariation = PERSONAL_CAR_MOD_VARIATION_TRUCK
							
							g_iPersonalCarModVariation = ENUM_TO_INT(sShopLauncherData.ePersonalCarModVariation)
							thisCreatorTrailer.CarModThread = START_NEW_SCRIPT_WITH_ARGS(thisCreatorTrailer.sChildOfChildScript, sShopLauncherData, SIZE_OF(sShopLauncherData), CAR_MOD_SHOP_STACK_SIZE)
							SET_SCRIPT_AS_NO_LONGER_NEEDED(thisCreatorTrailer.sChildOfChildScript)
							SET_BIT(thisCreatorTrailer.iLocalBS,BS_CREATOR_TRAILER_IS_CAR_MOD_SCRIPT_READY)
							g_sShopSettings.bShopScriptLaunched[ENUM_TO_INT(CARMOD_SHOP_PERSONALMOD)] = TRUE
							g_sShopSettings.bShopScriptLaunchedInMP[ENUM_TO_INT(CARMOD_SHOP_PERSONALMOD)] = NETWORK_IS_GAME_IN_PROGRESS()
							PRINTLN("MAINTAIN_LAUNCHING_CARMOD_SCRIPT - TRUE sShopLauncherData.ePersonalCarModVariation: ", sShopLauncherData.ePersonalCarModVariation)
							RETURN TRUE
						ELSE
							#IF IS_DEBUG_BUILD
								PRINTLN("MAINTAIN_LAUNCHING_CARMOD_SCRIPT: Unable to start shop script for personal_car_mod_shop - last instance still active")
							#ENDIF					
						ENDIF
					ELSE
						PRINTLN("MAINTAIN_LAUNCHING_CARMOD_SCRIPT - carmod is already running")
						RETURN TRUE 
					ENDIF
				ENDIF
			ELSE
				RETURN TRUE
			ENDIF
		ELSE
			IF IS_BIT_SET(thisCreatorTrailer.iLocalBS,BS_CREATOR_TRAILER_IS_CAR_MOD_SCRIPT_READY)
				g_bCleanUpCarmodShop = TRUE
				CLEAR_BIT(thisCreatorTrailer.iLocalBS,BS_CREATOR_TRAILER_IS_CAR_MOD_SCRIPT_READY)
			ENDIF	
			
			RETURN TRUE
		ENDIF
	ELSE
		PRINTLN("MAINTAIN_LAUNCHING_CARMOD_SCRIPT - thisCreatorTrailer.pOwner is invalid")
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTLN("MAINTAIN_LAUNCHING_CARMOD_SCRIPT - return false")
	#ENDIF
	RETURN FALSE
ENDFUNC	

FUNC BOOL ARE_ALL_PEDS_IN_VEHICLE_READY_FOR_EXIT_CREATOR_TRAILER()
	INT i
	PLAYER_INDEX thePlayer
	REPEAT NUM_NETWORK_PLAYERS i
		thePlayer = INT_TO_PLAYERINDEX(i)
		
		IF thePlayer != PLAYER_ID()
		AND thePlayer != INVALID_PLAYER_INDEX()
		AND NETWORK_IS_PLAYER_A_PARTICIPANT(thePlayer)
			IF IS_NET_PLAYER_OK(thePlayer)
				IF DOES_ENTITY_EXIST(thisCreatorTrailer.ownerVeh)
					IF IS_PED_IN_VEHICLE(GET_PLAYER_PED(thePlayer), thisCreatorTrailer.ownerVeh, FALSE)
						IF NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(thePlayer)].iBS, BS_CREATOR_TRAILER_READY_TO_WARP_OUT_W_OWNER)
							#IF IS_DEBUG_BUILD
								PRINTLN("ARE_ALL_PEDS_IN_VEHICLE_READY_FOR_EXIT_CREATOR_TRAILER: ", GET_PLAYER_NAME(thePlayer)," not ready not set flag")
							#ENDIF
							
							RETURN FALSE
						ENDIF
					ELSE
						IF IS_PED_IN_VEHICLE(GET_PLAYER_PED(thePlayer), thisCreatorTrailer.ownerVeh, TRUE)
						OR IS_ENTITY_ATTACHED_TO_ENTITY(GET_PLAYER_PED(thePlayer), thisCreatorTrailer.ownerVeh)
							#IF IS_DEBUG_BUILD
								PRINTLN("ARE_ALL_PEDS_IN_VEHICLE_READY_FOR_EXIT_CREATOR_TRAILER: ", GET_PLAYER_NAME(thePlayer)," entering or attached")
							#ENDIF
							
							RETURN FALSE
						ENDIF
					ENDIF
				ENDIF	
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

FUNC BOOL CAN_PLAYER_DRIVE_OUT_OF_CREATOR_TRAILER()
	IF NOT ARE_ALL_PEDS_IN_VEHICLE_READY_FOR_EXIT_CREATOR_TRAILER()
		PRINTLN("CAN_PLAYER_DRIVE_OUT_OF_CREATOR_TRAILER - False NOT ARE_ALL_PEDS_IN_VEHICLE_READY_FOR_EXIT_CREATOR_TRAILER")
		RETURN FALSE
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(pauseMenuInteractionsDelay)
	AND NOT HAS_NET_TIMER_EXPIRED(pauseMenuInteractionsDelay,1000,TRUE)
		PRINTLN("CAN_PLAYER_DRIVE_OUT_OF_CREATOR_TRAILER- False pause menu timer delay active")
		RETURN FALSE
	ENDIF
	
	IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) = PERFORMING_TASK
	OR GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) = WAITING_TO_START_TASK
		PRINTLN("CAN_PLAYER_DRIVE_OUT_OF_CREATOR_TRAILER - False leaving a vehicle")
		RETURN FALSE
	ENDIF
	
	IF GET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_IsSeatShuffling)
		PRINTLN("CAN_PLAYER_DRIVE_OUT_OF_CREATOR_TRAILER False player shuffling")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC MAINTAIN_CAN_DRIVE_OUT_OF_CREATOR_TRAILER()
	IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
	AND NOT g_MultiplayerSettings.g_bSuicide
	AND NOT IS_SELECTOR_ONSCREEN()
	
		DISABLE_SELECTOR_THIS_FRAME()
		SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_CREATOR_TRAILER_READY_TO_WARP_OUT_W_OWNER)
	ELSE
		CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, BS_CREATOR_TRAILER_READY_TO_WARP_OUT_W_OWNER)
	ENDIF
ENDPROC

PROC MAINTAIN_PLAYER_VEHICLE_LOCKS()
	g_bPlayerLeavingCurrentInteriorInVeh = FALSE
	IF thisCreatorTrailer.pOwner = PLAYER_ID()
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), FALSE)
			thisCreatorTrailer.ownerVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(), TRUE)
			
			IF DOES_ENTITY_EXIST(thisCreatorTrailer.ownerVeh)
				IF NETWORK_HAS_CONTROL_OF_ENTITY(thisCreatorTrailer.ownerVeh)
					SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(thisCreatorTrailer.ownerVeh, FALSE)
					SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(thisCreatorTrailer.ownerVeh, FALSE)
				ELSE
					NETWORK_REQUEST_CONTROL_OF_ENTITY(thisCreatorTrailer.ownerVeh)
				ENDIF
			ENDIF

			IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF GET_IS_VEHICLE_ENGINE_RUNNING(thisCreatorTrailer.ownerVeh)
				AND PLAYER_PED_ID() = GET_PED_IN_VEHICLE_SEAT(thisCreatorTrailer.ownerVeh)
					IF CAN_PLAYER_DRIVE_OUT_OF_CREATOR_TRAILER()
						IF (GET_CONTROL_UNBOUND_NORMAL(PLAYER_CONTROL, INPUT_VEH_ACCELERATE) != 0
						OR GET_CONTROL_UNBOUND_NORMAL(PLAYER_CONTROL, INPUT_VEH_BRAKE) != 0
						OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
						OR IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
						OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
						OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_BRAKE)
						OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_BRAKE)
						OR IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_BRAKE)
						OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_BRAKE))
						AND NOT g_MultiplayerSettings.g_bSuicide	
						AND NOT IS_SELECTOR_ONSCREEN()
							g_bPlayerLeavingCurrentInteriorInVeh = TRUE
							PRINTLN("g_bPlayerLeavingCurrentInteriorInVeh = TRUE CREATOR_TRAILER")
							IF NETWORK_HAS_CONTROL_OF_ENTITY(thisCreatorTrailer.ownerVeh)
								SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(thisCreatorTrailer.ownerVeh, TRUE) 
								SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(thisCreatorTrailer.ownerVeh, TRUE)
								
								DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
							ELSE
								NETWORK_REQUEST_CONTROL_OF_ENTITY(thisCreatorTrailer.ownerVeh)
							ENDIF
						ENDIF
					ELSE
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BRAKE)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
					ENDIF
				ELSE
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BRAKE)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
				ENDIF
			ENDIF
		ELSE
			IF DOES_ENTITY_EXIST(thisCreatorTrailer.ownerVeh)
				IF IS_VEHICLE_DRIVEABLE(thisCreatorTrailer.ownerVeh)
					IF NETWORK_HAS_CONTROL_OF_ENTITY(thisCreatorTrailer.ownerVeh)
						SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(thisCreatorTrailer.ownerVeh, TRUE)
			        	SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(thisCreatorTrailer.ownerVeh, PLAYER_ID(), FALSE)
					ELSE
						NETWORK_REQUEST_CONTROL_OF_ENTITY(thisCreatorTrailer.ownerVeh)
					ENDIF
				ENDIF
			ENDIF	
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_GET_OUT_OF_OWNERS_VEHICLE_IN_CREATOR_TRAILER()

	PED_INDEX DriverPedID
	
	IF thisCreatorTrailer.pOwner != PLAYER_ID()
		IF IS_NET_PLAYER_OK(PLAYER_ID())
		AND thisCreatorTrailer.eState = CREATOR_TRAILER_STATE_IDLE
		AND NOT IS_PASSENGER_READY_TO_ENTER_TRUCK_WITH_DRIVER(PLAYER_ID())
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				thisCreatorTrailer.ownerVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())

				IF thisCreatorTrailer.pOwner != INVALID_PLAYER_INDEX()
					IF IS_NET_PLAYER_OK(thisCreatorTrailer.pOwner, FALSE, TRUE)
						IF NOT IS_VEHICLE_SEAT_FREE(thisCreatorTrailer.ownerVeh)
							DriverPedID = GET_PED_IN_VEHICLE_SEAT(thisCreatorTrailer.ownerVeh, VS_DRIVER)	
							
							IF DOES_ENTITY_EXIST(DriverPedID)
							AND IS_PED_A_PLAYER(DriverPedID)
								IF NETWORK_GET_PLAYER_INDEX_FROM_PED(DriverPedID) != thisCreatorTrailer.pOwner
									IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != PERFORMING_TASK
									AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != WAITING_TO_START_TASK
										TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != PERFORMING_TASK
							AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != WAITING_TO_START_TASK
								TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
							ENDIF
						ENDIF
					ELSE
						IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != PERFORMING_TASK
						AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != WAITING_TO_START_TASK
							TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF thisCreatorTrailer.pOwner != INVALID_PLAYER_INDEX()
			IF IS_NET_PLAYER_OK(thisCreatorTrailer.pOwner)
			AND NETWORK_IS_PLAYER_A_PARTICIPANT(thisCreatorTrailer.pOwner)
				IF NOT IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(thisCreatorTrailer.pOwner), FALSE)
				OR NOT IS_VEHICLE_SEAT_FREE(GET_VEHICLE_PED_IS_IN(GET_PLAYER_PED(thisCreatorTrailer.pOwner)), VS_ANY_PASSENGER)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
				ENDIF
			ELSE
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_BUNKER_ENTRY_FROM_CREATOR_TRAILER_ON_FOOT()
	IF HAS_EXIT_TO_BUNKER_FROM_TRUCK_TRIGGERED()
	OR IS_BIT_SET(g_SimpleInteriorData.iBS ,BS_SIMPLE_INTERIOR_GLOBAL_DATA_ALL_EXIT_BUNKER_FROM_TRUCK_TRIGGERED)
	OR IS_PLAYER_TRUCK_TO_BUNKER_FRONT_EXIT_TRIGGER(PLAYER_ID())
		IF NOT IS_SCREEN_FADED_OUT()
			DO_SCREEN_FADE_OUT(500)
		ELSE	
			NET_SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
			IF thisCreatorTrailer.pOwner != INVALID_PLAYER_INDEX()
			
				IF IS_PLAYER_TRUCK_TO_BUNKER_FRONT_EXIT_TRIGGER(PLAYER_ID())
					CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_CREATOR_TRAILER - MAINTAIN_BUNKER_ENTRY_FROM_CREATOR_TRAILER_ON_FOOT About to call SET_EXIT_TO_BUNKER_FROM_TRUCK_TRIGGERED(TRUE)")
					SET_EXIT_TO_BUNKER_FROM_TRUCK_TRIGGERED(TRUE)
				ENDIF
			
				IF thisCreatorTrailer.pOwner != PLAYER_ID()
					IF NOT IS_TRANS_ON_FOOT_BETWEEN_TRUCK_BUNKER_STARTED(thisCreatorTrailer.pOwner)
						SET_START_EXIT_TO_BUNKER_FROM_TRUCK(TRUE)
						RESET_TRUCK_EXT_SCRIPT(TRUE)
						globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner = thisCreatorTrailer.pOwner
						SET_SIMPLE_INTERIOR_AUTOWARP_OVERRIDE(thisCreatorTrailer.pOwner)
						PRINTLN("MAINTAIN_BUNKER_ENTRY_FROM_CREATOR_TRAILER_ON_FOOT - g_SimpleInteriorData.iOverrideAutoWarpToPlayersProperty: ", g_SimpleInteriorData.iOverrideAutoWarpToPlayersProperty)
						IF g_SimpleInteriorData.iOverrideAutoWarpToPlayersProperty = -1 
							SET_SIMPLE_INTERIOR_AUTOWARP_OVERRIDE(thisCreatorTrailer.pOwner)
							PRINTLN("MAINTAIN_BUNKER_ENTRY_FROM_CREATOR_TRAILER_ON_FOOT - g_SimpleInteriorData.iOverrideAutoWarpToPlayersProperty is -1 setting to : ", NATIVE_TO_INT(thisCreatorTrailer.pOwner))
						ENDIF	
						g_SimpleInteriorData.bForceTerminateInteriorScript = TRUE
						PRINTLN("MAINTAIN_BUNKER_ENTRY_FROM_CREATOR_TRAILER_ON_FOOT - remote player Triggered")
					ENDIF
				ELSE
					SET_START_EXIT_TO_BUNKER_FROM_TRUCK(TRUE)
					RESET_TRUCK_EXT_SCRIPT(TRUE)
					SET_TRANS_ON_FOOT_BETWEEN_TRUCK_BUNKER_STARTED(TRUE)
					g_SimpleInteriorData.bForceTerminateInteriorScript = TRUE
					PRINTLN("MAINTAIN_BUNKER_ENTRY_FROM_CREATOR_TRAILER_ON_FOOT - owner Triggered")
				ENDIF
			ENDIF	
		ENDIF	
	ENDIF
ENDPROC

FUNC BOOL CAN_PLAYER_START_CREATOR_TRAILER_GUN_LOCKER()
	IF thisCreatorTrailer.pOwner != INVALID_PLAYER_INDEX()
		IF thisCreatorTrailer.pOwner = PLAYER_ID()
			IF IS_PLAYER_PURCHASED_GUN_LOCKER_FOR_TRUCK(PLAYER_ID())
			AND NOT IS_PLAYER_USING_OFFICE_SEATID_VALID()
				// allow
			ELSE
				RETURN FALSE
			ENDIF
		ELSE
			IF IS_PLAYER_PURCHASED_GUN_LOCKER_FOR_TRUCK(thisCreatorTrailer.pOwner)
				IF (DOES_PLAYER_OWN_ANY_GUN_LOCKER())	// does player own truck gun locker
				AND (NOT NETWORK_IS_IN_MP_CUTSCENE())
				AND NOT IS_PLAYER_USING_OFFICE_SEATID_VALID()
					// allow
				ELSE
					RETURN FALSE
				ENDIF
			ELSE
				RETURN FALSE
			ENDIF	
		ENDIF	
	ENDIF	

	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Safty check if we can open run locker menu
/// RETURNS:
///    True if it is safe
FUNC BOOL IS_SAFE_TO_START_CREATOR_TRAILER_GUN_LOCKER_MENUS(BOOL bAllowHeist = FALSE)
	
	IF !CAN_PLAYER_START_CREATOR_TRAILER_GUN_LOCKER()
		RETURN FALSE
	ENDIF 
	
	IF !bAllowHeist
		IF Is_Player_Currently_On_MP_Heist(PLAYER_ID())
		OR Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID())
			RETURN FALSE
		ENDIF
	ENDIF
		
	IF	IS_PHONE_ONSCREEN()
	OR IS_INTERACTION_MENU_OPEN()
	OR IS_BROWSER_OPEN()
	OR IS_SELECTOR_ONSCREEN()
	OR IS_CUSTOM_MENU_ON_SCREEN()
	OR IS_PAUSE_MENU_ACTIVE()
	OR (MPGlobalsAmbience.bTriggerPropertyExitOnFoot)
	OR IS_SCREEN_FADED_OUT()
		RETURN FALSE
	ENDIF
	RETURN TRUE
	
ENDFUNC

FUNC BOOL IS_PLAYER_IN_TRUCK_GUN_LOCKER_LOCATE()
	ARMORY_TRUCK_SECTIONS_ENUM iSection
	MP_PROP_OFFSET_STRUCT tempOffset[2]
	IF IS_PLAYER_PURCHASED_GUN_LOCKER_FOR_TRUCK(PLAYER_ID())
		IF DOES_ARMORY_TRUCK_SECTION_EXIST(AT_ST_GUNMOD, iSection)
			GET_ARMORY_TRUCK_PROP_TRANSFORM(ATP_GUN_LOCKER_ARMORY_LOCATE_ONE, iSection, tempOffset[0])
			GET_ARMORY_TRUCK_PROP_TRANSFORM(ATP_GUN_LOCKER_ARMORY_LOCATE_TWO, iSection, tempOffset[1])
		ELIF DOES_ARMORY_TRUCK_SECTION_EXIST(AT_ST_CARMOD, iSection)
			GET_ARMORY_TRUCK_PROP_TRANSFORM(ATP_GUN_LOCKER_CARMOD_LOCATE_ONE, iSection, tempOffset[0],ATS_SECOND_SECTION)
			GET_ARMORY_TRUCK_PROP_TRANSFORM(ATP_GUN_LOCKER_CARMOD_LOCATE_TWO, iSection, tempOffset[1], ATS_SECOND_SECTION)
		ENDIF	
		RETURN IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), tempOffset[0].vLoc, tempOffset[1].vLoc , 1.0 )
	ENDIF	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_CREATOR_TRAILER_GUN_LOCKER(VAULT_WEAPON_LOADOUT_CUSTOMIZATION& sWLoadoutCustomization)
	IF thisCreatorTrailer.pOwner != INVALID_PLAYER_INDEX()
		IF IS_PLAYER_PURCHASED_GUN_LOCKER_FOR_TRUCK(thisCreatorTrailer.pOwner)
			SWITCH sWLoadoutCustomization.eCustomizationStage
				CASE VMC_STAGE_INIT
					CLEAN_UP_CREATOR_TRAILER_GUN_LOCKER(sWLoadoutCustomization)
					
					sWLoadoutCustomization.eCustomizationStage = VWC_STAGE_WAITING_TO_TRIGGER
				BREAK
				
				CASE VWC_STAGE_WAITING_TO_TRIGGER 				
					GET_NUM_AVAILABLE_WEAPON_GROUP(sWLoadoutCustomization)
					//MANAGE_BUNKER_GUN_LOCKER_WEAPONS(sWLoadoutCustomization)
					//MAINTAIN_GUN_LOCKER_PROP()
					
					IF IS_SAFE_TO_START_CREATOR_TRAILER_GUN_LOCKER_MENUS(TRUE)
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						AND IS_PLAYER_IN_TRUCK_GUN_LOCKER_LOCATE()
						AND NOT IS_PED_RUNNING(PLAYER_PED_ID())
						AND DOES_ENTITY_EXIST(PLAYER_PED_ID())
						AND IS_PED_STILL(PLAYER_PED_ID())
							IF sWLoadoutCustomization.iVaultWeaponContext = NEW_CONTEXT_INTENTION
								IF NOT IS_HELP_MESSAGE_ON_SCREEN()
									REGISTER_CONTEXT_INTENTION(sWLoadoutCustomization.iVaultWeaponContext, CP_HIGH_PRIORITY, "OF_VAULT_MENU")
								ENDIF	
							ENDIF
							IF NOT IS_INTERACTION_MENU_OPEN()
								IF HAS_CONTEXT_BUTTON_TRIGGERED(sWLoadoutCustomization.iVaultWeaponContext)
									LOAD_MENU_ASSETS()
									BUILD_VAULT_WEAPON_MAIN_MENU(sWLoadoutCustomization)
									ARMORY_TRUCK_SECTIONS_ENUM iSection
									IF IS_PLAYER_PURCHASED_GUN_LOCKER_FOR_TRUCK(PLAYER_ID())
										IF DOES_ARMORY_TRUCK_SECTION_EXIST(AT_ST_GUNMOD, iSection)
											TASK_ACHIEVE_HEADING(PLAYER_PED_ID(), 93.3671, 0)
										ELIF  DOES_ARMORY_TRUCK_SECTION_EXIST(AT_ST_CARMOD, iSection)
											TASK_ACHIEVE_HEADING(PLAYER_PED_ID(), 267.4226, 0)
										ENDIF
									ENDIF	
									NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_LEAVE_CAMERA_CONTROL_ON)
									SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_UseKinematicModeWhenStationary, TRUE)
									SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DontActivateRagdollFromExplosions, TRUE)

									RELEASE_CONTEXT_INTENTION(sWLoadoutCustomization.iVaultWeaponContext)
									sWLoadoutCustomization.eCustomizationStage = VMC_STAGE_CUSTOMIZING 
								ENDIF
							ENDIF	
						ELSE
							RELEASE_CONTEXT_INTENTION(sWLoadoutCustomization.iVaultWeaponContext)
						ENDIF
					ELSE
						IF IS_CONTEXT_INTENTION_HELP_DISPLAYING(sWLoadoutCustomization.iVaultWeaponContext)
							RELEASE_CONTEXT_INTENTION(sWLoadoutCustomization.iVaultWeaponContext)
						ENDIF	
					ENDIF
				BREAK
				CASE VMC_STAGE_CUSTOMIZING
					IF IS_SCREEN_FADED_IN()
						PROCESS_VAULT_WEAPON_LOADOUT_MENU(sWLoadoutCustomization)
					ELSE
						sWLoadoutCustomization.eCustomizationStage = VMC_STAGE_CLEANUP 
					ENDIF
				BREAK
				CASE VMC_STAGE_CLEANUP
					CLEAN_UP_CREATOR_TRAILER_GUN_LOCKER(sWLoadoutCustomization)
					START_NET_TIMER(sWLoadoutCustomization.sMenuTimer)
				BREAK
			ENDSWITCH
		ENDIF	
	ENDIF	
ENDPROC


PROC DELETE_CUSTOM_CAR(NETWORK_INDEX theVehNetID,INT iSaveSlot, INT iCallID)
	IF iCallID != 0
	
	ENDIF
	IF NOT HAS_NET_TIMER_STARTED(thisCreatorTrailer.failSafeClearVehicleDelay)
		START_NET_TIMER(thisCreatorTrailer.failSafeClearVehicleDelay,TRUE)
	ENDIF
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(theVehNetID)
		IF IS_NET_VEHICLE_DRIVEABLE(theVehNetID)
			IF GET_VEHICLE_DOOR_LOCK_STATUS(NET_TO_VEH(theVehNetID)) != VEHICLELOCK_LOCKED
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(theVehNetID)
					SET_VEHICLE_DOORS_LOCKED(NET_TO_VEH(theVehNetID),VEHICLELOCK_LOCKED)
					PRINTLN("DELETE_CUSTOM_CAR: Locking doors for update on vehicle ",iCallID)
				ELSE
					NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(theVehNetID)
				ENDIF
			ENDIF
			IF IS_VEHICLE_EMPTY(NET_TO_VEH(theVehNetID),TRUE, TRUE)
			OR HAS_NET_TIMER_EXPIRED(thisCreatorTrailer.failSafeClearVehicleDelay,3000,TRUE)
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(theVehNetID)	
					DELETE_NET_ID(theVehNetID)
					serverBD.iPersVehSaveSlot = -1
					serverBD.iPersVehCreationBS = 0
					CLEAR_BIT(thisCreatorTrailer.iLocalBS,BS_CREATOR_TRAILER_PERS_VEH_CHECKED_AVAILABLILITY)
					PRINTLN("DELETE_CUSTOM_CAR: deleted vehicle cleared update flag saveSlot# ", iSaveSlot)
					IF iSaveSlot > -1
						CLEAR_BIT(g_MpSavedVehicles[iSaveSlot].iVehicleBS,MP_SAVED_VEHICLE_UPDATE_VEHICLE)
					ENDIF
				ELSE
					PRINTLN("DELETE_CUSTOM_CAR: can't delete not empty! saveSlot# ", iSaveSlot)
					NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(theVehNetID)
				ENDIF
			ELSE
				IF IS_NET_PLAYER_OK(PLAYER_ID())
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),NET_TO_VEH(theVehNetID))
						TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID(),0,ECF_WARP_IF_DOOR_IS_BLOCKED)
					ENDIF
				ENDIF
				PRINTLN("DELETE_CUSTOM_CAR: can't delete not empty! saveSlot# ", iSaveSlot)
			ENDIF
		ELSE
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(theVehNetID)	
				DELETE_NET_ID(theVehNetID)
				serverBD.iPersVehSaveSlot = -1
				serverBD.iPersVehCreationBS = 0
				CLEAR_BIT(thisCreatorTrailer.iLocalBS,BS_CREATOR_TRAILER_PERS_VEH_CHECKED_AVAILABLILITY)
				PRINTLN("DELETE_CUSTOM_CAR: deleted (NOT DRIVEABLE) vehicle cleared update flag saveSlot# ", iSaveSlot)
				IF iSaveSlot > -1
					CLEAR_BIT(g_MpSavedVehicles[iSaveSlot].iVehicleBS,MP_SAVED_VEHICLE_UPDATE_VEHICLE)
				ENDIF
			ELSE
				NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(theVehNetID)
			ENDIF
		ENDIF
	ELSE
		IF iSaveSlot > -1
			CLEAR_BIT(g_MpSavedVehicles[iSaveSlot].iVehicleBS,MP_SAVED_VEHICLE_UPDATE_VEHICLE)
		ENDIF
		CLEAR_BIT(thisCreatorTrailer.iLocalBS,BS_CREATOR_TRAILER_PERS_VEH_CHECKED_AVAILABLILITY)
		PRINTLN("DELETE_CUSTOM_CAR: vehicle deleted saveSlot# ", iSaveSlot)
	ENDIF
ENDPROC

FUNC BOOL CREATE_MODDED_CAR_FOR_CREATOR_TRAILER(VEHICLE_SETUP_STRUCT vehicleSetup, INT iSaveSlot,INT iDisplaySlot, BOOL bHasEmblem)
	//MP_PROP_OFFSET_STRUCT offset
	VEHICLE_INDEX tempVehicle
	VECTOR vehCreationLocation
	FLOAT fHeading
//	IF IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, PLAYER_BD_BS_WARPING_OUT_OF_GARAGE)
//		PRINTLN("CREATE_MODDED_CAR_FOR_CREATOR_TRAILER: bypassing as player is in process of leaving")
//		RETURN FALSE
//	ENDIF
	
	IF NOT IS_NET_VEHICLE_DRIVEABLE(serverBD.personalVehicle)
		IF IS_MODEL_IN_CDIMAGE(vehicleSetup.eModel)
			IF REQUEST_LOAD_MODEL(vehicleSetup.eModel)
//				IF CAN_RESERVE_NETWORK_VEHICLES_FOR_THIS_SCRIPT(GET_NUM_RESERVED_MISSION_VEHICLES()+ 1,false,true)
//					SERVER_CHECK_ANY_PLAYERS_ARE_IN_THE_WAY(serverBD,vehCreationLocation)
//					IF NOT IS_IT_SAFE_TO_CREATE_A_VEHICLE(serverBD)
//						PRINTLN("CREATE_MODDED_CAR_FOR_CREATOR_TRAILER: waiting for players to be out of the way: Vector = ",vehCreationLocation)
//						RETURN FALSE
//					ENDIF
//				ENDIF
					
				//IF IS_BIT_SET(serverBD.iVehicleResBS,i)
					IF CAN_REGISTER_MISSION_ENTITIES(0,1,0,0)
						IF NETWORK_IS_IN_MP_CUTSCENE() 
							SET_NETWORK_CUTSCENE_ENTITIES(TRUE)	
							PRINTLN("CREATE_MODDED_CAR_FOR_CREATOR_TRAILER, in cutscene therefore setting SET_NETWORK_CUTSCENE_ENTITIES")
						ENDIF
						GET_CREATOR_TRAILER_SPAWN_POINT(thisCreatorTrailer.eSimpleInteriorID,1,vehCreationLocation,fHeading,TRUE)
						//vehCreationLocation = TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS(vehCreationLocation, thisInterior.details)
						//fHeading = TRANSFORM_SIMPLE_INTERIOR_HEADING_TO_WORLD_HEADING(fHeading, thisInterior.details)
						tempVehicle = CREATE_VEHICLE(vehicleSetup.eModel,vehCreationLocation,fHeading,
												TRUE,TRUE)	
						SET_ENTITY_COORDS_NO_OFFSET(tempVehicle,vehCreationLocation)
						SET_ENTITY_HEADING(tempVehicle,fHeading)
						serverBD.personalVehicle = VEH_TO_NET(tempVehicle)
						//SET_NETWORK_ID_EXISTS_ON_ALL_MACHINES(serverBD.personalVehicle, TRUE)
						//NETWORK_SET_VEHICLE_GARAGE_INDEX(playerBD[NATIVE_TO_INT(PLAYER_ID())].customVehicleNetIDs[i],iOwnerID)
						//FREEZE_ENTITY_POSITION(playerBD[NATIVE_TO_INT(PLAYER_ID())].customVehicleNetIDs[i],TRUE)
						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(tempVehicle,TRUE)	
						#IF IS_DEBUG_BUILD
						PRINTLN("CREATE_MODDED_CAR_FOR_CREATOR_TRAILER: HOST Creating vehicle save slot #",iSaveSlot,"at ", vehCreationLocation, "  ")
						#ENDIF
						serverBD.iPersVehCreationBS = 0
						IF bHasEmblem
							MP_SAVE_VEHICLE_APPLY_SAVED_DETAILS_TO_VEHICLE_INC_EMBLEM(tempVehicle,iSaveSlot)//,bLoadingEmblem[i])
						ELSE
							MP_SAVE_VEHICLE_APPLY_SAVED_DETAILS_TO_VEHICLE(tempVehicle,iSaveSlot)
							SET_BIT(serverBD.iPersVehCreationBS,MP_PROP_CREATE_CARS_BS_APPLIED_DETAILS)
						ENDIF
						serverbD.iPersVehSaveSlot = iSaveSlot

						SET_VEHICLE_ON_GROUND_PROPERLY(tempVehicle,0)
						//PRINTLN("CREATE_MODDED_CAR_FOR_CREATOR_TRAILER: after set on ground coords are: ", GET_ENTITY_COORDS(tempVehicle))
						vehCreationLocation = GET_ENTITY_COORDS(tempVehicle)
						PRINTLN("CREATE_MODDED_CAR_FOR_CREATOR_TRAILER: vehicle after set on ground coords are: ", vehCreationLocation)
						//PRINTLN("CREATE_MODDED_CAR_FOR_CREATOR_TRAILER: after set on ground coords are: ", GET_ENTITY_COORDS(tempVehicle))

						//ENDIF
						//SET_VEHICLE_SETUP(tempVehicle, vehicleSetup, FALSE, TRUE)
						//MPGlobalsAmbience.GarageVehicleID[0] = tempVehicle
						NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(tempVehicle,TRUE)
						SET_VEHICLE_NOT_STEALABLE_AMBIENTLY(tempVehicle,FALSE)
						SET_VEHICLE_FULLBEAM(tempVehicle, FALSE)
						SET_VEHICLE_LIGHTS(tempVehicle,FORCE_VEHICLE_LIGHTS_OFF)	
						SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(tempVehicle, TRUE)  
						//PRINTLN("CREATE_MODDED_CAR_FOR_CREATOR_TRAILER: setting radio station in car ",i," to be: ",g_MpSavedVehicles[iSaveSlot].tlRadioStationName)
						IF IS_NET_PLAYER_OK(g_ownerOfArmoryTruckPropertyIAmIn,FALSE,FALSE)
//			                	SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(tempVehicle, g_ownerOfArmoryTruckPropertyIAmIn, FALSE)
						ENDIF
						IF g_ownerOfArmoryTruckPropertyIAmIn != INVALID_PLAYER_INDEX()
							IF IS_NET_PLAYER_OK(g_ownerOfArmoryTruckPropertyIAmIn,FALSE,FALSE)
								SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(tempVehicle, g_ownerOfArmoryTruckPropertyIAmIn, FALSE)
								PRINTLN("Setting vehicle doors unlocked for ", GET_PLAYER_NAME(g_ownerOfArmoryTruckPropertyIAmIn))
							ENDIF
						ENDIF
//							ENDIF
						
						SET_VEHICLE_FIXED(tempVehicle) 
				        SET_ENTITY_HEALTH(tempVehicle, 1000) 
				        SET_VEHICLE_ENGINE_HEALTH(tempVehicle, 1000) 
				        SET_VEHICLE_PETROL_TANK_HEALTH(tempVehicle, 1000) 
						SET_VEHICLE_DIRT_LEVEL(tempVehicle,0.0)
		                SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(tempVehicle,TRUE)
						SET_VEHICLE_RESPECTS_LOCKS_WHEN_HAS_DRIVER(tempVehicle,TRUE)
						CLEAR_BIT(serverBD.iPersVehCreationBS,MP_PROP_CREATE_CARS_BS_MODS_LOADED)
						MODIFY_VEHICLE_TOP_SPEED(tempVehicle,-95.0)
						//bLoadingEmblem[i] = FALSE
						SET_ENTITY_CAN_BE_DAMAGED(tempVehicle,FALSE)
						SET_ENTITY_VISIBLE(tempVehicle,FALSE) //hidden for test
						
						// set this vehicle as a player_vehicle so no on else can steal it
						IF DECOR_IS_REGISTERED_AS_TYPE("Player_Vehicle", DECOR_TYPE_INT)
							IF NOT DECOR_EXIST_ON(tempVehicle, "Player_Vehicle")
								DECOR_SET_INT(tempVehicle, "Player_Vehicle", NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID()))
							ENDIF
						ENDIF
						
						SET_NETWORK_ID_ALWAYS_EXISTS_FOR_PLAYER(serverBD.personalVehicle,PLAYER_ID(),FALSE)
						PRINTLN("CREATE_MODDED_CAR_FOR_CREATOR_TRAILER: created vehicle ID ")
						IF NETWORK_IS_IN_MP_CUTSCENE() 
							SET_NETWORK_CUTSCENE_ENTITIES(FALSE)	
						ENDIF
						//RESET_NET_TIMER(st_ServerWalkOutTimeout)
					ENDIF
				//ELSE
				//	PRINTLN("CREATE_MODDED_CAR_FOR_CREATOR_TRAILER: Waiting for CAN_RESERVE_NETWORK_ENTITIES_FOR_THIS_SCRIPT(",serverBD.iVehicleCreated+ 1 ,")") 
				//ENDIF
			ELSE
				PRINTLN("CREATE_MODDED_CAR_FOR_CREATOR_TRAILER: Waiting for model to load")
				RETURN FALSE
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(serverBD.iPersVehCreationBS,MP_PROP_CREATE_CARS_BS_MODS_LOADED)
			// Now set the specifics
			// Bug #437236 - Set all license plates for all profiles in SP and MP to have the same text when reading from the cloud data.
			IF NOT IS_BIT_SET(serverBD.iPersVehCreationBS,MP_PROP_CREATE_CARS_BS_MODS_LOADED)
				tempVehicle = NET_TO_VEH(serverBD.personalVehicle)
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.personalVehicle)
					IF NOT IS_BIT_SET(serverBD.iPersVehCreationBS,MP_PROP_CREATE_CARS_BS_APPLIED_DETAILS)
						//IF MP_SAVE_VEHICLE_APPLY_SAVED_DETAILS_TO_VEHICLE_INC_EMBLEM(tempVehicle,i,)
						IF bHasEmblem
							IF MP_SAVE_VEHICLE_APPLY_SAVED_DETAILS_TO_VEHICLE_INC_EMBLEM(tempVehicle,iSaveSlot)//,bLoadingEmblem[i])
								SET_BIT(serverBD.iPersVehCreationBS,MP_PROP_CREATE_CARS_BS_APPLIED_DETAILS)
								PRINTLN("CREATE_MODDED_CAR_FOR_CREATOR_TRAILER: added all mods for car in slot # ", iSaveSlot)
							#IF IS_DEBUG_BUILD
							ELSE
								PRINTLN("CREATE_MODDED_CAR_FOR_CREATOR_TRAILER: waiting for applying details to car")
							#ENDIF
							ENDIF
						ELSE
							MP_SAVE_VEHICLE_APPLY_SAVED_DETAILS_TO_VEHICLE(tempVehicle,iSaveSlot)
							SET_BIT(serverBD.iPersVehCreationBS,MP_PROP_CREATE_CARS_BS_APPLIED_DETAILS)
							PRINTLN("CREATE_MODDED_CAR_FOR_CREATOR_TRAILER: added all mods for car in slot # ", iSaveSlot)
						ENDIF
					ELSE
						IF HAVE_VEHICLE_MODS_STREAMED_IN(tempVehicle)
							IF InteriorPropStruct.iGarageSize > PROP_GARAGE_SIZE_2
							AND MP_SAVE_VEHICLE_IS_DISPLAY_SLOT_A_CYCLE(iDisplaySlot)
							
							ELSE
								IF NOT SET_VEHICLE_ON_GROUND_PROPERLY(tempVehicle,0)
									PRINTLN("CREATE_MODDED_CAR_FOR_CREATOR_TRAILER: waiting for SET_VEHICLE_ON_GROUND_PROPERLY")
									//RETURN FALSE
								ELSE
									PRINTLN("CREATE_MODDED_CAR_FOR_CREATOR_TRAILER: SET_VEHICLE_ON_GROUND_PROPERLY done")
									vehCreationLocation = GET_ENTITY_COORDS(tempVehicle)
									PRINTLN("CREATE_MODDED_CAR_FOR_CREATOR_TRAILER: vehicle after set on ground coords are: ", vehCreationLocation )
								ENDIF
							ENDIF
							SET_BIT(serverBD.iPersVehCreationBS,MP_PROP_CREATE_CARS_BS_MODS_LOADED)
							SET_ENTITY_VISIBLE(tempVehicle,TRUE) //hidden for test

							SET_HYDRAULIC_SUSPENSION_RAISE_FACTOR(tempVehicle, SC_WHEEL_CAR_FRONT_LEFT, 0.0)
							SET_HYDRAULIC_SUSPENSION_RAISE_FACTOR(tempVehicle, SC_WHEEL_CAR_FRONT_RIGHT, 0.0)
							SET_HYDRAULIC_SUSPENSION_RAISE_FACTOR(tempVehicle, SC_WHEEL_CAR_MID_LEFT, 0.0)
							SET_HYDRAULIC_SUSPENSION_RAISE_FACTOR(tempVehicle, SC_WHEEL_CAR_MID_RIGHT, 0.0)
							SET_HYDRAULIC_SUSPENSION_RAISE_FACTOR(tempVehicle, SC_WHEEL_CAR_REAR_LEFT, 0.0)
							SET_HYDRAULIC_SUSPENSION_RAISE_FACTOR(tempVehicle, SC_WHEEL_CAR_REAR_RIGHT, 0.0)
							SET_CAN_USE_HYDRAULICS(tempVehicle, FALSE)
							PRINTLN("CREATE_MODDED_CAR_FOR_CREATOR_TRAILER: setting vehicle can't use hydraulics")
							
//							IF InteriorPropStruct.iGarageSize > PROP_GARAGE_SIZE_2
//							AND MP_SAVE_VEHICLE_IS_DISPLAY_SLOT_A_CYCLE(iDisplaySlot)
//								SET_VEHICLE_DOORS_LOCKED(tempVehicle,VEHICLELOCK_CANNOT_ENTER)
//							ELSE
								SET_VEHICLE_DOORS_LOCKED(tempVehicle,VEHICLELOCK_UNLOCKED)
							//ENDIF

							SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(tempVehicle, TRUE)  
							IF g_ownerOfArmoryTruckPropertyIAmIn != INVALID_PLAYER_INDEX()
								IF IS_NET_PLAYER_OK(g_ownerOfArmoryTruckPropertyIAmIn,FALSE,FALSE)
									SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(tempVehicle, g_ownerOfArmoryTruckPropertyIAmIn, FALSE)
									PRINTLN("Setting vehicle doors unlocked for ", GET_PLAYER_NAME(g_ownerOfArmoryTruckPropertyIAmIn))
								ENDIF
							ENDIF
			                SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(tempVehicle,TRUE)
							SET_VEHICLE_RESPECTS_LOCKS_WHEN_HAS_DRIVER(tempVehicle,TRUE)
							PRINTLN("AM_MP_CREATOR_TRAILER: adding a new vehicle to garage")
							//iReplacedVehicle = -1
							PRINTLN("CREATE_MODDED_CAR_FOR_CREATOR_TRAILER: setting server net ID for vehicle")
						#IF IS_DEBUG_BUILD
						ELSE
							PRINTLN("CREATE_MODDED_CAR_FOR_CREATOR_TRAILER: waiting for HAVE_VEHICLE_MODS_STREAMED_IN to car")
						#ENDIF
						ENDIF
					ENDIF
				ELSE
					NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.personalVehicle)
					PRINTLN("CREATE_MODDED_CAR_FOR_CREATOR_TRAILER: waiting for ownership for mods")
				ENDIF
			ELSE
				PRINTLN("CREATE_MODDED_CAR_FOR_CREATOR_TRAILER: MP_PROP_CREATE_CARS_BS_MODS_LOADED for vehicle ")
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_VEHICLE_AVAILABLE_FOR_GAME_IN_CREATOR_TRAILER(INT iSaveSlot)
	IF IS_BIT_SET(thisCreatorTrailer.iLocalBS,BS_CREATOR_TRAILER_PERS_VEH_CHECKED_AVAILABLILITY)
		RETURN IS_BIT_SET(thisCreatorTrailer.iLocalBS,BS_CREATOR_TRAILER_PERS_VEH_AVAILABLE)
	ELIF IS_BIT_SET(thisCreatorTrailer.iLocalBS,BS_CREATOR_TRAILER_PERS_VEH_AVAILABLE)
		RETURN TRUE
	ELSE
		IF IS_VEHICLE_AVAILABLE_FOR_GAME(g_MpSavedVehicles[iSaveSlot].vehicleSetupMP.VehicleSetup.eModel, TRUE) 
			SET_BIT(thisCreatorTrailer.iLocalBS,BS_CREATOR_TRAILER_PERS_VEH_AVAILABLE)
			PRINTLN("IS_VEHICLE_AVAILABLE_FOR_GAME_IN_PROPERTY: setting Available save slot: ",iSaveSlot)
			RETURN TRUE
		ELSE
			PRINTLN("IS_VEHICLE_AVAILABLE_FOR_GAME_IN_PROPERTY: setting UN-Available save slot: ",iSaveSlot)
		ENDIF
	ENDIF
	SET_BIT(thisCreatorTrailer.iLocalBS,BS_CREATOR_TRAILER_PERS_VEH_CHECKED_AVAILABLILITY)
	RETURN FALSE
ENDFUNC 

FUNC BOOL HANDLE_SAVING_VEHICLE_IN_CREATOR_TRAILER()
	INT iSaveSlot = -1
	INT iDisplaySlot = -1
	IF NOT IS_BIT_SET(thisCreatorTrailer.iLocalBS,BS_CREATOR_TRAILER_VEH_TRANSACTION_FINISHED)
	AND IS_BIT_SET(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_ADDING_NEW_VEHICLE)
		IF NOT IS_BIT_SET(thisCreatorTrailer.iLocalBS,BS_CREATOR_TRAILER_CREATED_TRANSACTION_VEHICLE)
			IF NOT DOES_ENTITY_EXIST(transactionVehicle)
				IF globalPropertyEntryData.replacingVehicle.vehicleSetupMP.VehicleSetup.eModel != DUMMY_MODEL_FOR_SCRIPT
					IF REQUEST_LOAD_MODEL(globalPropertyEntryData.replacingVehicle.vehicleSetupMP.VehicleSetup.eModel)
						transactionVehicle = CREATE_VEHICLE(globalPropertyEntryData.replacingVehicle.vehicleSetupMP.VehicleSetup.eModel,GET_ENTITY_COORDS(PLAYER_PED_ID())+<<0,0,-20>>,0,FALSE,FALSE)
						FREEZE_ENTITY_POSITION(transactionVehicle,TRUE)
						SET_VEHICLE_DOORS_LOCKED(transactionVehicle,VEHICLELOCK_CANNOT_ENTER)
						SET_VEHICLE_FULLBEAM(transactionVehicle, FALSE)
						SET_VEHICLE_LIGHTS(transactionVehicle,FORCE_VEHICLE_LIGHTS_OFF)	
						SET_VEHICLE_DIRT_LEVEL(transactionVehicle,0.0)
						SET_ENTITY_CAN_BE_DAMAGED(transactionVehicle,FALSE)
							
						SET_ENTITY_VISIBLE(transactionVehicle,FALSE) //hidden for test
					ENDIF
				ELSE
					PRINTLN("AM_MP_CREATOR_TRAILER: INVALID VEHICLE MODEL STORED ON ENTRY!!")
					RETURN TRUE
				ENDIF
			ELSE
				IF IS_VEHICLE_DRIVEABLE(transactionVehicle)
					IF NOT IS_BIT_SET(thisCreatorTrailer.iLocalBS,BS_CREATOR_TRAILER_APPLIED_DETAILS_TO_TRANSACTION_VEHICLE)
						IF MP_SAVE_VEHICLE_APPLY_SAVED_DETAILS_TO_VEHICLE_INC_EMBLEM_USING_STRUCT(transactionVehicle,globalPropertyEntryData.replacingVehicle)//,bLoadingEmblem[i])
							SET_BIT(thisCreatorTrailer.iLocalBS,BS_CREATOR_TRAILER_APPLIED_DETAILS_TO_TRANSACTION_VEHICLE)								
							PRINTLN("TRANSACTION_VEHICLE: added all mods for car")
						#IF IS_DEBUG_BUILD
						ELSE
							PRINTLN("HANDLE_SAVING_VEHICLE_IN_CREATOR_TRAILER:TRANSACTION_VEHICLE: waiting for applying details to car")
						#ENDIF
						ENDIF
					ELSE
						IF HAVE_VEHICLE_MODS_STREAMED_IN(transactionVehicle)
							SET_BIT(thisCreatorTrailer.iLocalBS,BS_CREATOR_TRAILER_CREATED_TRANSACTION_VEHICLE)
							SET_MODEL_AS_NO_LONGER_NEEDED(globalPropertyEntryData.replacingVehicle.vehicleSetupMP.VehicleSetup.eModel)
							SET_HYDRAULIC_SUSPENSION_RAISE_FACTOR(transactionVehicle, SC_WHEEL_CAR_FRONT_LEFT, 0.0)
							SET_HYDRAULIC_SUSPENSION_RAISE_FACTOR(transactionVehicle, SC_WHEEL_CAR_FRONT_RIGHT, 0.0)
							SET_HYDRAULIC_SUSPENSION_RAISE_FACTOR(transactionVehicle, SC_WHEEL_CAR_MID_LEFT, 0.0)
							SET_HYDRAULIC_SUSPENSION_RAISE_FACTOR(transactionVehicle, SC_WHEEL_CAR_MID_RIGHT, 0.0)
							SET_HYDRAULIC_SUSPENSION_RAISE_FACTOR(transactionVehicle, SC_WHEEL_CAR_REAR_LEFT, 0.0)
							SET_HYDRAULIC_SUSPENSION_RAISE_FACTOR(transactionVehicle, SC_WHEEL_CAR_REAR_RIGHT, 0.0)
							SET_CAN_USE_HYDRAULICS(transactionVehicle, FALSE)
							PRINTLN("HANDLE_SAVING_VEHICLE_IN_CREATOR_TRAILER: TRANSACTION_VEHICLE: setting transaction vehicle can't use hydraulics")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_BIT_SET(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_REPLACING_TRUCK_VEHICLE)
				iSaveSlot = MP_SAVE_VEHICLE_GET_EMPTY_SAVE_SLOT(MP_SAVE_VEH_SEARCH_EMPTY_FIRST,FALSE, GET_ENTITY_MODEL(transactionVehicle))
			ELSE
				MPSV_GET_SAVE_SLOT_FROM_DISPLAY_SLOT(DISPLAY_SLOT_START_ARMOURY_TRUCK,iSaveSlot)
				PRINTLN("AM_MP_CREATOR_TRAILER:  replacing vehicle in truck as vehicle can't be swapped")
			ENDIF
			IF USE_SERVER_TRANSACTIONS()
				IF PROCESS_TRANSACTION_FOR_NEW_GARAGE_VEHICLE(transactionVehicle, iSaveSlot, iNewCarTransactionResult)
					SET_BIT(thisCreatorTrailer.iLocalBS,BS_CREATOR_TRAILER_VEH_TRANSACTION_FINISHED)
					
					IF iNewCarTransactionResult = GARAGE_VEHICLE_TRANSACTION_STATE_SUCCESS
						PRINTLN("AM_MP_CREATOR_TRAILER: PROCESS_TRANSACTION_FOR_NEW_GARAGE_VEHICLE - SUCCESS!")
						PRINTLN("iSaveSlot: ",iSaveSlot)
						IF iSaveSlot >= 0
							IF IS_BIT_SET(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_REPLACING_TRUCK_VEHICLE)
								MPSV_GET_DISPLAY_SLOT_FROM_SAVE_SLOT(CURRENT_SAVED_VEHICLE_SLOT(),iDisplaySlot)
								IF iDisplaySlot != -1
									PRINTLN("AM_MP_CREATOR_TRAILER: PROCESS_TRANSACTION_FOR_NEW_GARAGE_VEHICLE clearing old display slot for vehicle")
									MPSV_SET_DISPLAY_SLOT(iDisplaySlot,-1)
								ENDIF
							ENDIF
							MPSV_SET_DISPLAY_SLOT(DISPLAY_SLOT_START_ARMOURY_TRUCK,iSaveSlot)
							MP_SAVE_VEHICLE_STORE_CAR_DETAILS_IN_SLOT(transactionVehicle ,iSaveSlot,TRUE,TRUE,TRUE,FALSE)
							iNewVehicleSavedSlot = iSaveSlot
						ENDIF
					ELIF iNewCarTransactionResult = GARAGE_VEHICLE_TRANSACTION_STATE_FAILED
						PRINTLN("AM_MP_CREATOR_TRAILER: PROCESS_TRANSACTION_FOR_NEW_GARAGE_VEHICLE - FAILED! Aborting")
						//SET_LOCAL_STAGE(STAGE_ABORT_TRANSACTION_EXIT)
					ENDIF
					iNewCarTransactionResult = 0			
				ELSE
					PRINTLN("AM_MP_CREATOR_TRAILER: waiting for PROCESS_TRANSACTION_FOR_NEW_GARAGE_VEHICLE")
					RETURN FALSE
				ENDIF
			ELSE
				IF NOT USE_SERVER_TRANSACTIONS()
					PRINTLN("AM_MP_CREATOR_TRAILER: NOT USE_SERVER_TRANSACTIONS, not doing transaction")
				ENDIF
				PRINTLN("AM_MP_CREATOR_TRAILER: setting display slot for Non PC new vehicle")
				
				IF iSaveSlot >= 0
					IF IS_BIT_SET(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_REPLACING_TRUCK_VEHICLE)
						MPSV_GET_DISPLAY_SLOT_FROM_SAVE_SLOT(CURRENT_SAVED_VEHICLE_SLOT(),iDisplaySlot)
						IF iDisplaySlot != -1
							PRINTLN("AM_MP_CREATOR_TRAILER: PROCESS_TRANSACTION_FOR_NEW_GARAGE_VEHICLE clearing old display slot for vehicle")
							MPSV_SET_DISPLAY_SLOT(iDisplaySlot,-1)
						ENDIF
					ENDIF
					MP_SAVE_VEHICLE_STORE_CAR_DETAILS_IN_SLOT(transactionVehicle ,iSaveSlot,TRUE,TRUE,TRUE,FALSE)
					MPSV_SET_DISPLAY_SLOT(DISPLAY_SLOT_START_ARMOURY_TRUCK,iSaveSlot)
					iNewVehicleSavedSlot = iSaveSlot
				ENDIF
				SET_BIT(thisCreatorTrailer.iLocalBS,BS_CREATOR_TRAILER_VEH_TRANSACTION_FINISHED)
			ENDIF
		ENDIF
		RETURN FALSE
	ELSE
		PRINTLN("AM_MP_CREATOR_TRAILER: personal vehicle returned to garage- no transaction")
	ENDIF
	RETURN TRUE
ENDFUNC

FUNC BOOL ASSIGN_CAR_TO_CREATOR_TRAILER(VEHICLE_INDEX theVeh)
	INT iSaveSlot
	//INt iOldSaveSlot
///	MP_PROP_OFFSET_STRUCT offset
//	IF bDoingNewLoadScene
//		IF (NOT IS_NEW_LOAD_SCENE_ACTIVE() OR IS_NEW_LOAD_SCENE_LOADED())
//		OR HAS_NET_TIMER_EXPIRED(st_LoadSceneTimeout,LOAD_SCENE_TIMEOUT_FAIL_SAFE,TRUE)
//			#IF IS_DEBUG_BUILD
//			IF HAS_NET_TIMER_EXPIRED(st_LoadSceneTimeout,LOAD_SCENE_TIMEOUT_FAIL_SAFE,TRUE)
//				CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_INT: Load scene timed out- 1")
//			ENDIF
//			#ENDIF
//		ELSE
//			IF NOT IS_SCREEN_FADED_OUT()
//			AND NOT IS_SCREEN_FADING_OUT()
//				IF HAS_NET_TIMER_EXPIRED(st_LoadSceneTimeout,LOAD_SCENE_TIMEOUT_FAIL_SAFE_FADE,TRUE)
//					SCREEN_FADE_OUT_PROPERTY_WRAP(500)
//					bLoadSceneTimeoutFadeTriggered = TRUE
//					CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_INT: Load scene timed out triggered fade- 1")
//				ENDIF
//			ENDIF
//			PRINTLN("ASSIGN_CAR_TO_CREATOR_TRAILER waiting for new load scene")
//			RETURN FALSE
//		ENDIF
//	ENDIF
//	
	VECTOR vehCreationLocation
	FLOAT fHeading
	
	IF NOT HANDLE_SAVING_VEHICLE_IN_CREATOR_TRAILER()
		PRINTLN("ASSIGN_CAR_TO_CREATOR_TRAILER: Waiting for HANDLE_SAVING_VEHICLE_IN_CREATOR_TRAILER")
		RETURN FALSE
	ENDIF
	PRINTLN("ASSIGN_CAR_TO_CREATOR_TRAILER: trying to create vehicle")
	IF NOT IS_NET_VEHICLE_DRIVEABLE(serverBD.personalVehicle)
//		SERVER_CHECK_ANY_PLAYERS_ARE_IN_THE_WAY(serverBD,vehCreationLocation.vLoc)
//		IF NOT IS_IT_SAFE_TO_CREATE_A_VEHICLE(serverBD)
//			PRINTLN("ASSIGN_CAR_TO_CREATOR_TRAILER: waiting for players to be out of the way: Vector = ",vehCreationLocation.vLoc)
//			RETURN FALSE
//		ENDIF
		IF DOES_ENTITY_EXIST(theVeh)
			IF NETWORK_HAS_CONTROL_OF_ENTITY(theVeh)
				NETWORK_REQUEST_CONTROL_OF_ENTITY(theVeh)
			ELSE
				PRINTLN("ASSIGN_CAR_TO_CREATOR_TRAILER: waiting for control")
				RETURN FALSE
			ENDIF
			IF CAN_REGISTER_MISSION_ENTITIES(0,1,0,0)
				IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.personalVehicle)
					SET_ENTITY_AS_MISSION_ENTITY(theVeh,TRUE,TRUE)
					PRINTLN("ASSIGN_CAR_TO_CREATOR_TRAILER: assigning to script.")
					serverBD.personalVehicle = VEH_TO_NET(theVeh)
				ENDIF

				SET_VEHICLE_DOORS_LOCKED(theVeh,VEHICLELOCK_UNLOCKED)
				FREEZE_ENTITY_POSITION(theVeh,FALSE)
				
				GET_CREATOR_TRAILER_SPAWN_POINT(thisCreatorTrailer.eSimpleInteriorID,1,vehCreationLocation,fHeading,TRUE)
				SET_ENTITY_COORDS_NO_OFFSET(theVeh,vehCreationLocation)
				SET_ENTITY_HEADING(theVeh,fHeading)
				
				//SET_NETWORK_ID_EXISTS_ON_ALL_MACHINES(serverBD.personalVehicle, TRUE)
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(theVeh,TRUE)	
				SET_HYDRAULIC_SUSPENSION_RAISE_FACTOR(theVeh, SC_WHEEL_CAR_FRONT_LEFT, 0.0)
				SET_HYDRAULIC_SUSPENSION_RAISE_FACTOR(theVeh, SC_WHEEL_CAR_FRONT_RIGHT, 0.0)
				SET_HYDRAULIC_SUSPENSION_RAISE_FACTOR(theVeh, SC_WHEEL_CAR_MID_LEFT, 0.0)
				SET_HYDRAULIC_SUSPENSION_RAISE_FACTOR(theVeh, SC_WHEEL_CAR_MID_RIGHT, 0.0)
				SET_HYDRAULIC_SUSPENSION_RAISE_FACTOR(theVeh, SC_WHEEL_CAR_REAR_LEFT, 0.0)
				SET_HYDRAULIC_SUSPENSION_RAISE_FACTOR(theVeh, SC_WHEEL_CAR_REAR_RIGHT, 0.0)
				SET_CAN_USE_HYDRAULICS(theVeh, FALSE)	
				PRINTLN("ASSIGN_CAR_TO_CREATOR_TRAILER: setting vehicle can't use hydraulics")
				#IF IS_DEBUG_BUILD
				PRINTLN("ASSIGN_CAR_TO_CREATOR_TRAILER: HOST Creating vehicle at ", vehCreationLocation, " heading = ",fHeading," in Truck. " )
				#ENDIF
				IF NOT SET_VEHICLE_ON_GROUND_PROPERLY(theVeh,SET_VEH_ON_GROUND_OFFSET)
					PRINTLN("ASSIGN_CAR_TO_CREATOR_TRAILER: waiting for SET_VEHICLE_ON_GROUND_PROPERLY")
					//RETURN FALSE
				ENDIF
				vehCreationLocation = GET_ENTITY_COORDS(theVeh)
				PRINTLN("ASSIGN_CAR_TO_CREATOR_TRAILER: vehicle after set on ground coords are: ", vehCreationLocation)
				
				NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(theVeh,TRUE)
				IF DECOR_IS_REGISTERED_AS_TYPE("Player_Vehicle", DECOR_TYPE_INT)
					//IF NOT DECOR_EXIST_ON(theVeh, "Player_Vehicle")
					DECOR_REMOVE(theVeh,"Player_Vehicle")
					//NET_PRINT("adding personal vehicle decorator") NET_NL()
					//CLEAR_BIT(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_OUT_GARAGE)
				ENDIF
				INT iDecoratorValue
				IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
					IF DECOR_EXIST_ON(theVeh, "MPBitset")	
						iDecoratorValue = DECOR_GET_INT(theVeh, "MPBitset")
						PRINTLN("ASSIGN_CAR_TO_CREATOR_TRAILER: Removing entering garage decorator from vehicle")
						CLEAR_BIT(iDecoratorValue, MP_DECORATOR_BS_ENTERING_INTO_GARAGE)
						DECOR_SET_INT(theVeh, "MPBitset", iDecoratorValue)
					ENDIF
				ENDIF
				SET_VEHICLE_FIXED(theVeh) 
		        SET_ENTITY_HEALTH(theVeh, 1000) 
		        SET_VEHICLE_ENGINE_HEALTH(theVeh, 1000) 
		        SET_VEHICLE_PETROL_TANK_HEALTH(theVeh, 1000) 
		        SET_VEHICLE_DIRT_LEVEL(theVeh,0) 
				SET_ENTITY_CAN_BE_DAMAGED(theVeh,FALSE)
				SET_VEHICLE_NOT_STEALABLE_AMBIENTLY(theVeh,FALSE)
				SET_VEHICLE_FULLBEAM(theVeh, FALSE)
				SET_VEHICLE_LIGHTS(theVeh,FORCE_VEHICLE_LIGHTS_OFF)
				SET_VEHICLE_BRAKE_LIGHTS(theVeh, FALSE)
				SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(theVeh, TRUE)  
				MODIFY_VEHICLE_TOP_SPEED(theVeh,-95.0)

	            SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(theVeh, PLAYER_ID(), FALSE)
				PRINTLN("ASSIGN_CAR_TO_CREATOR_TRAILER: setting doors not locked for owner: ", GET_PLAYER_NAME(PLAYER_ID()))

	            SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(theVeh,TRUE)
				SET_VEHICLE_RESPECTS_LOCKS_WHEN_HAS_DRIVER(theVeh,TRUE)
				serverBD.iPersVehCreationBS = 0	
				SET_BIT(serverBD.iPersVehCreationBS,MP_PROP_CREATE_CARS_BS_APPLIED_DETAILS)
				SET_BIT(serverBD.iPersVehCreationBS,MP_PROP_CREATE_CARS_BS_MODS_LOADED)
				PRINTLN("ASSIGN_CAR_TO_CREATOR_TRAILER: created vehicle")
				
				IF IS_BIT_SET(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_CAR_REPLACEMENT)
				OR NOT IS_BIT_SET(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_ADDING_NEW_VEHICLE)
					MPSV_GET_SAVE_SLOT_FROM_DISPLAY_SLOT(DISPLAY_SLOT_START_ARMOURY_TRUCK,iSaveSlot)
					//CLEAR_BIT(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_OUT_GARAGE)
					//CLEAR_BIT(g_MpSavedVehicles[iSaveSlot].iVehicleBS,MP_SAVED_VEHICLE_OUT_GARAGE)
					//MOVE_VEHICLES_BETWEEN_SLOTS(CURRENT_SAVED_VEHICLE_SLOT(),iSaveSlot)
					IF CURRENT_SAVED_VEHICLE_SLOT() >= 0
						CLEAR_BIT(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_OUT_GARAGE)
						PRINTLN("[personal_vehicle] MP_SAVED_VEHICLE_OUT_GARAGE -15cleared on #",CURRENT_SAVED_VEHICLE_SLOT())
						IF iSaveSlot >= 0
							CLEAR_BIT(g_MpSavedVehicles[iSaveSlot].iVehicleBS,MP_SAVED_VEHICLE_OUT_GARAGE)
							PRINTLN("[personal_vehicle] MP_SAVED_VEHICLE_OUT_GARAGE -15cleared on #",iSaveSlot)
							//SET_LAST_USED_VEHICLE_SLOT(iSaveSlot)
							CLEAR_BIT(g_MpSavedVehicles[iSaveSlot].iVehicleBS,MP_SAVED_VEHICLE_UPDATE_VEHICLE)
							MOVE_VEHICLES_BETWEEN_SLOTS(CURRENT_SAVED_VEHICLE_SLOT(),iSaveSlot)
						ELSE
							//SET_LAST_USED_VEHICLE_SLOT(iOldSaveSlot)
							MOVE_VEHICLES_BETWEEN_SLOTS(CURRENT_SAVED_VEHICLE_SLOT(),iSaveSlot,-1,DISPLAY_SLOT_START_ARMOURY_TRUCK)
							CLEAR_BIT(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_UPDATE_VEHICLE)
						ENDIF
					ENDIF
					CLEAR_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_CAR_REPLACEMENT)
				ENDIF
				IF NOT IS_BIT_SET(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_ADDING_NEW_VEHICLE)
					IF CURRENT_SAVED_VEHICLE_SLOT() > -1
						MP_SAVE_VEHICLE_STORE_CAR_DETAILS_IN_SLOT(theVeh,CURRENT_SAVED_VEHICLE_SLOT(),TRUE,TRUE,FALSE,FALSE)
					ENDIF
				ENDIF
				serverbD.iPersVehSaveSlot = iNewVehicleSavedSlot
				REQUEST_SAVE(SSR_REASON_VEH_STORAGE_UPDATE, STAT_SAVETYPE_IMMEDIATE_FLUSH)
					
				FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
				SET_ENTITY_VISIBLE(theVeh,TRUE) //hidden for test
				CLEAR_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_CAR_MADE_INVISIBLE_ON_EXTERIOR)
				CLEAR_VEHICLE_ROUTE_HISTORY(theVeh)
				// set this vehicle as a player_vehicle so no on else can steal it
				SET_PV_DECORATOR(theVeh, CURRENT_SAVED_VEHICLE_SLOT())	
				SET_NETWORK_ID_ALWAYS_EXISTS_FOR_PLAYER(serverBD.personalVehicle,PLAYER_ID(),FALSE)
				DELETE_VEHICLE(transactionVehicle)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
				//RESET_NET_TIMER(st_ServerWalkOutTimeout)
				RETURN TRUE
			ENDIF
		ELSE
			PRINTLN("ASSIGN_CAR_TO_CREATOR_TRAILER: vehicle does not exist just continuing anyway")
			RETURN TRUE
		ENDIF
	ELSE
		MPSV_GET_SAVE_SLOT_FROM_DISPLAY_SLOT(DISPLAY_SLOT_START_ARMOURY_TRUCK,iSaveSlot,TRUE) //CDM
		IF iSaveSlot >= 0
			SET_BIT(g_MpSavedVehicles[iSaveSlot].iVehicleBS,MP_SAVED_VEHICLE_UPDATE_VEHICLE)
			PRINTLN("ASSIGN_CAR_TO_CREATOR_TRAILER: netID exists for vehicle coming into truck in slot #",iSaveSlot ," Forcable deleting it!")
		ELSE
			PRINTLN("ASSIGN_CAR_TO_CREATOR_TRAILER: netID exists but no save slot???")
		ENDIF
		
		//CLEAR_BIT(iLocalBS2,LOCAL_BS2_FINISHED_VEHICLE_TRANSACTION)
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC INT GET_PLAYERS_TRUCK_PROPERTY_INTERIOR_MAP_HASH(PLAYER_INDEX pOwner)

	IF pOwner = INVALID_PLAYER_INDEX()
		RETURN -1
	ENDIF
	
	ARMORY_TRUCK_SECTION_TYPE_ENUM eSection1 = GET_PLAYER_TRUCK_PURCHASED_SECTIONS(ATS_FIRST_SECTION, pOwner)
	ARMORY_TRUCK_SECTION_TYPE_ENUM eSection2 = GET_PLAYER_TRUCK_PURCHASED_SECTIONS(ATS_SECOND_SECTION, pOwner)
	ARMORY_TRUCK_SECTION_TYPE_ENUM eSection3 = GET_PLAYER_TRUCK_PURCHASED_SECTIONS(ATS_THIRD_SECTION, pOwner)
	
	SWITCH eSection1
		CASE AT_ST_LIVING_ROOM
			SWITCH eSection2
				CASE AT_ST_LIVING_ROOM
					SWITCH eSection3
						CASE AT_ST_LIVING_ROOM
							RETURN HASH("gr_grdlc_trailer_01")
						BREAK
						CASE AT_ST_GUNMOD
							RETURN HASH("gr_grdlc_trailer_02")
						BREAK
						CASE AT_ST_VEHICLE_STORAGE
							RETURN HASH("gr_grdlc_trailer_05")
						BREAK
						CASE AT_ST_EMPTY_SINGLE_DOOR
							RETURN HASH("gr_grdlc_trailer_14")
						BREAK
					ENDSWITCH
				BREAK
				CASE AT_ST_GUNMOD
					SWITCH eSection3
						CASE AT_ST_LIVING_ROOM
							RETURN HASH("gr_grdlc_trailer_03")
						BREAK
						CASE AT_ST_VEHICLE_STORAGE
							RETURN HASH("gr_grdlc_trailer_04")
						BREAK
						CASE AT_ST_EMPTY_SINGLE_DOOR
							RETURN HASH("gr_grdlc_trailer_16")
						BREAK
					ENDSWITCH
				BREAK
				CASE AT_ST_CARMOD
					RETURN HASH("gr_grdlc_trailer_06")
				BREAK
				CASE AT_ST_EMPTY_SINGLE
					SWITCH eSection3
						CASE AT_ST_GUNMOD
							RETURN HASH("gr_grdlc_trailer_17")
						BREAK
						CASE AT_ST_VEHICLE_STORAGE
							RETURN HASH("gr_grdlc_trailer_18")
						BREAK
						CASE AT_ST_LIVING_ROOM
							RETURN HASH("gr_grdlc_trailer_15")
						BREAK
					ENDSWITCH
				BREAK
				CASE AT_ST_EMPTY_DOUBLE
					RETURN HASH("gr_grdlc_trailer_13")
				BREAK
			ENDSWITCH
		BREAK
		CASE AT_ST_COMMAND_CENTER
			SWITCH eSection2
				CASE AT_ST_LIVING_ROOM
					SWITCH eSection3
						CASE AT_ST_LIVING_ROOM
							RETURN HASH("gr_grdlc_trailer_07")
						BREAK
						CASE AT_ST_GUNMOD
							RETURN HASH("gr_grdlc_trailer_08")
						BREAK
						CASE AT_ST_VEHICLE_STORAGE
							RETURN HASH("gr_grdlc_trailer_11")
						BREAK
						CASE AT_ST_EMPTY_SINGLE_DOOR
							RETURN HASH("gr_grdlc_trailer_20")
						BREAK
					ENDSWITCH
				BREAK
				CASE AT_ST_GUNMOD
					SWITCH eSection3
						CASE AT_ST_LIVING_ROOM
							RETURN HASH("gr_grdlc_trailer_09")
						BREAK
						CASE AT_ST_VEHICLE_STORAGE
							RETURN HASH("gr_grdlc_trailer_10")
						BREAK
						CASE AT_ST_EMPTY_SINGLE_DOOR
							RETURN HASH("gr_grdlc_trailer_22")
						BREAK
					ENDSWITCH
				BREAK
				CASE AT_ST_CARMOD
					RETURN HASH("gr_grdlc_trailer_12")
				BREAK
				CASE AT_ST_EMPTY_SINGLE
					SWITCH eSection3
						CASE AT_ST_GUNMOD
							RETURN HASH("gr_grdlc_trailer_23")
						BREAK
						CASE AT_ST_VEHICLE_STORAGE
							RETURN HASH("gr_grdlc_trailer_24")
						BREAK
						CASE AT_ST_LIVING_ROOM
							RETURN HASH("gr_grdlc_trailer_21")
						BREAK
					ENDSWITCH
				BREAK
				CASE AT_ST_EMPTY_DOUBLE
					RETURN HASH("gr_grdlc_trailer_19")
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN 0
ENDFUNC

PROC MAINTAIN_TRUCK_INTERIOR_MAP()

	IF iTruckInteriorHash = 0
	OR HAVE_ARMORY_TRUCK_SECTIONS_UPDATED()
		iTruckInteriorHash = GET_PLAYERS_TRUCK_PROPERTY_INTERIOR_MAP_HASH(thisCreatorTrailer.pOwner)
	ENDIF
	IF iTruckInteriorHash != 0
	AND !g_bActiveInGunTurret
		SET_RADAR_AS_INTERIOR_THIS_FRAME(iTruckInteriorHash, 1103.562, -3000.000)
	ENDIF
ENDPROC

PROC MAINTAIN_ACTIVITY_PROP_TINTS()
	//Update tints of Activity Entities
	
	IF GET_ARMORY_TRUCK_UPDATE_BIT(AT_UB_TINTS_REFRESHED)
		bTintWasRefreshed = TRUE
	ENDIF
	
	IF activityControllerStruct.bAllPropsCreated
	AND bTintWasRefreshed
		//Current owners tint
		ARMORY_TRUCK_SECTION_TINT_ENUM eNewTint
		eNewTint = GET_PLAYER_TRUCK_PURCHASED_SECTIONS_TINT(thisCreatorTrailer.pOwner)
		
		IF eNewTint <> AT_STI_INVALID_INDEX
			INT index
			REPEAT COUNT_OF(serverBD.activityProps) index
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.activityProps[index].niSceneObjects[0])
					IF DOES_ENTITY_EXIST(NET_TO_OBJ(serverBD.activityProps[index].niSceneObjects[0]))
						IF NETWORK_HAS_CONTROL_OF_ENTITY(NET_TO_OBJ(serverBD.activityProps[index].niSceneObjects[0]))
							//Chairs
							IF GET_ENTITY_MODEL(NET_TO_OBJ(serverBD.activityProps[index].niSceneObjects[0])) = GR_PROP_HIGHENDCHAIR_GR_01A
								SET_OBJECT_TINT_INDEX(NET_TO_OBJ(serverBD.activityProps[index].niSceneObjects[0]), ENUM_TO_INT(eNewTint))
								#IF IS_DEBUG_BUILD
								VECTOR vPos
								vPos = GET_ENTITY_COORDS(NET_TO_OBJ(serverBD.activityProps[index].niSceneObjects[0]))
								CDEBUG1LN(DEBUG_SAFEHOUSE, "Update Activity Entitiy Tints - index: ", index , " Position: ", vPos, " eNewTint: ", ENUM_TO_INT(eNewTint), " TintName: ", GET_ARMORY_TRUCK_SECTION_TINT_NAME(eNewTint)) 
								#ENDIF
							ENDIF
						ENDIF
					ELSE
						CDEBUG1LN(DEBUG_SAFEHOUSE, "Update Activity Entitiy Tints - index: ", index , " No Entity")
					ENDIF
				ENDIF
			ENDREPEAT
		ELSE
			CDEBUG1LN(DEBUG_SAFEHOUSE, "Update Activity Entitiy Tints - unable to update tints, Owner tint is invalid")
		ENDIF
		
		bTintWasRefreshed = FALSE
	ENDIF
ENDPROC

PROC MANAGE_PLAYER_JACK_FLAGS()
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF !GET_PED_CONFIG_FLAG(PLAYER_PED_ID(),PCF_NotAllowedToJackAnyPlayers)
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_NotAllowedToJackAnyPlayers, TRUE)
			SET_BIT(thisCreatorTrailer.iLocalBS,BS_CREATOR_TRAILER_PREVENT_JACKING)
			PRINTLN("MANAGE_PLAYER_JACK_FLAGS - SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_NotAllowedToJackAnyPlayers, TRUE)")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_CREATOR_TRAILER_RENDER_TARGET_BE_USED(ARMORY_TRUCK_SCREEN_ID eScreenID)
	SWITCH eScreenID
		CASE AT_TRAILER_MONITOR_01 
		CASE AT_TRAILER_MONITOR_02
		CASE AT_TRAILER_MONITOR_03
		CASE AT_COMMAND_CENTER_TV_01
			//Does the owner have a command centre
			RETURN (GET_PLAYER_TRUCK_PURCHASED_SECTIONS(ATS_FIRST_SECTION, thisCreatorTrailer.pOwner) = AT_ST_COMMAND_CENTER)
		
		CASE AT_LIVING_AREA_TV_01
			//Does the owner have any living quater sections
			RETURN IS_PLAYER_PURCHASED_ANY_LIVING_ROOM_SECTION_FOR_TRUCK(thisCreatorTrailer.pOwner)
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL REGISTER_AND_LINK_RENDER_TARGET(INT &iRenderTargetID, ARMORY_TRUCK_SCREEN_ID eScreenID)
	
	//If we don't have the relevant sections then we shouldn'd try to link the render target
	IF NOT SHOULD_CREATOR_TRAILER_RENDER_TARGET_BE_USED(eScreenID)
		RETURN TRUE
	ENDIF

	IF NOT IS_NAMED_RENDERTARGET_REGISTERED(GET_RENDER_TARGET(eScreenID))
		IF REGISTER_NAMED_RENDERTARGET(GET_RENDER_TARGET(eScreenID))
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[AM_MP_CREATOR_TRAILER] - REGISTER_AND_LINK_RENDER_TARGET - Registered named render target.")
			
			MODEL_NAMES eRenderTarget
			GET_RENDER_TARGET_PROP(eScreenID, eRenderTarget)
			
			IF NOT IS_NAMED_RENDERTARGET_LINKED(eRenderTarget)
				LINK_NAMED_RENDERTARGET(eRenderTarget)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[AM_MP_CREATOR_TRAILER] - REGISTER_AND_LINK_RENDER_TARGET - Linking named render target to command center monitor.")
				
				IF iRenderTargetID = -1
					iRenderTargetID = GET_NAMED_RENDERTARGET_RENDER_ID(GET_RENDER_TARGET(eScreenID))
					CDEBUG1LN(DEBUG_SAFEHOUSE, "[AM_MP_CREATOR_TRAILER] - REGISTER_AND_LINK_RENDER_TARGET - Named render target ID = ", iRenderTargetID)
					
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		IF GET_FRAME_COUNT() % 60 = 0
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[AM_MP_CREATOR_TRAILER] - REGISTER_AND_LINK_RENDER_TARGET - IS_NAMED_RENDERTARGET_REGISTERED = TRUE")
		ENDIF
		#ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC DRAW_TO_RENDER_TARGET(INT iRenderTarget, ARMORY_TRUCK_SCREEN_ID eScreenID)
	
	//This will only be -1 if we haven't linked this render target in the function REGISTER_AND_LINK_RENDER_TARGET
	IF iRenderTarget = -1
		EXIT
	ENDIF	
	
	SET_TEXT_RENDER_ID(iRenderTarget)
	SET_SCRIPT_GFX_ALIGN(UI_ALIGN_IGNORE, UI_ALIGN_IGNORE)
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
	SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[AM_MP_CREATOR_TRAILER] - DRAW_TO_RENDER_TARGET - Setting text render ID and script GFX alignment.")
	
	// TODO: Implement monitor behaviour.
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[AM_MP_CREATOR_TRAILER] - DRAW_TO_RENDER_TARGET - Drawing sprite named render target.")
	DRAW_SPRITE_NAMED_RENDERTARGET(GET_STREAMED_TEXTURE_DICTIONARY(eScreenID), ARMORY_TRUCK_GET_TEXTURE_NAME_FOR_MONITOR(eScreenID), 0.5, 0.5, 1.0, 1.0, 0.0, 255, 255, 255, 255)
		
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[AM_MP_CREATOR_TRAILER] - DRAW_TO_RENDER_TARGET - Reseting script GFX alignment.")
		
	RESET_SCRIPT_GFX_ALIGN()
	SET_TEXT_RENDER_ID(GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID())
ENDPROC

PROC MAINTAIN_CREATOR_TRAILER_MONITORS(ARMORY_TRUCK_MONITOR_DATA &data)

	INT iLoop, iCounter
	
	SWITCH data.eMonitorStates
		CASE ATP_MONITOR_STATE_DETECT_PROP
			
			//We only need to do 2 requsts here as the 5 render targets share the same to textures
			REQUEST_STREAMED_TEXTURE_DICT(GET_STREAMED_TEXTURE_DICTIONARY(AT_TRAILER_MONITOR_01))
			REQUEST_STREAMED_TEXTURE_DICT(GET_STREAMED_TEXTURE_DICTIONARY(AT_COMMAND_CENTER_TV_01))
			
			IF HAS_STREAMED_TEXTURE_DICT_LOADED(GET_STREAMED_TEXTURE_DICTIONARY(AT_TRAILER_MONITOR_01))
			AND HAS_STREAMED_TEXTURE_DICT_LOADED(GET_STREAMED_TEXTURE_DICTIONARY(AT_COMMAND_CENTER_TV_01))
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[AM_MP_CREATOR_TRAILER] - CREATOR_TRAILER - Texture dictionary has loaded, moving to state: ATP_MONITOR_STATE_LINK_RT.") 
				data.eMonitorStates = ATP_MONITOR_STATE_LINK_RT
			ENDIF
		BREAK
		CASE ATP_MONITOR_STATE_LINK_RT
			
			REPEAT AT_MONITORS_MAX iLoop
				IF REGISTER_AND_LINK_RENDER_TARGET(data.iRenderTargetIDs[iLoop], INT_TO_ENUM(ARMORY_TRUCK_SCREEN_ID, iLoop))
					iCounter ++
				ENDIF
			ENDREPEAT
			
			IF iCounter = ENUM_TO_INT(AT_MONITORS_MAX)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[AM_MP_CREATOR_TRAILER] - CREATOR_TRAILER - Moving to state: ATP_MONITOR_STATE_UPDATE_SCALEFORM")
				data.eMonitorStates = ATP_MONITOR_STATE_UPDATE_SCALEFORM
			ENDIF
		BREAK
		CASE ATP_MONITOR_STATE_UPDATE_SCALEFORM
			
			REPEAT AT_MONITORS_MAX iLoop
				DRAW_TO_RENDER_TARGET(data.iRenderTargetIDs[iLoop], INT_TO_ENUM(ARMORY_TRUCK_SCREEN_ID, iLoop))
			ENDREPEAT
		BREAK
	ENDSWITCH
ENDPROC

PROC DISABLE_VEH_CONTROLS()
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HYDRAULICS_CONTROL_TOGGLE)
        DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL)
        DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL_TWO)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DETONATE)
        DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
        DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK1)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK2)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_ALTERNATE)
        
        DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)

	    DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_GUN_LEFT)
	    DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_GUN_RIGHT)
	    DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_GUN_UP)
	    DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_GUN_DOWN)
	    DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_ATTACK)
	    DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_ATTACK2)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_ATTACK)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_PREV_WEAPON)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_JUMP)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ROCKET_BOOST)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BIKE_WINGS)
		
	    DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_HEADLIGHT)
		DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_AIM)
		DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_AIM)
		
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_MELEE_HOLD)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_MELEE_LEFT)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_MELEE_RIGHT)
	ENDIF
ENDPROC

PROC MAINTAIN_COMMAND_CENTER_EXIT_CHECKS()
	ARMORY_TRUCK_SECTION_TYPE_ENUM eSection = GET_PLAYER_TRUCK_PURCHASED_SECTIONS(ATS_FIRST_SECTION, g_ownerOfArmoryTruckPropertyIAmIn)

	IF eSection = AT_ST_COMMAND_CENTER
	OR eSection = AT_ST_LIVING_ROOM
		IF IS_PLAYER_IN_CREATOR_TRAILER_EXIT_LOCATE(eSection)
			IF NOT DOES_ENTITY_EXIST(MPGlobalsAmbience.vehTruckVehicle[0])
				MPGlobalsAmbience.vehTruckVehicle[0] = GET_CREATOR_TRAILER_CAB_VEH_INDEX()
			ENDIF
			
			IF NOT IS_BIT_SET(g_SimpleInteriorData.iBSTruckExit, BS_SIMPLE_INTERIOR_GLOBAL_DATA_REFRESH_MENU_OPTIONS)
				SET_BIT(g_SimpleInteriorData.iBSTruckExit, BS_SIMPLE_INTERIOR_GLOBAL_DATA_REFRESH_MENU_OPTIONS)
				#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_CREATOR_TRAILER - MAINTAIN_COMMAND_CENTER_EXIT_CHECKS - Setting BS_SIMPLE_INTERIOR_GLOBAL_DATA_REFRESH_MENU_OPTIONS")
				#ENDIF
				
				// Set this flag to refresh the simple interiors exit menu text.
				g_SimpleInteriorData.bRefreshInteriorMenu = TRUE
			ENDIF
			
			g_SimpleInteriorData.bShouldExitMenuBeVisible = TRUE
			
//			IF DOES_ENTITY_EXIST(MPGlobalsAmbience.vehTruckVehicle[0]) AND IS_ENTITY_ALIVE(MPGlobalsAmbience.vehTruckVehicle[0])
//				IF PLAYER_ID() != INVALID_PLAYER_INDEX()
//					IF (PLAYER_ID() = g_ownerOfArmoryTruckPropertyIAmIn)
//						IF IS_VEHICLE_ATTACHED_TO_TRAILER(MPGlobalsAmbience.vehTruckVehicle[0])
//						AND (IS_VEHICLE_SEAT_FREE(MPGlobalsAmbience.vehTruckVehicle[0], VS_DRIVER)
//						OR IS_VEHICLE_SEAT_FREE(MPGlobalsAmbience.vehTruckVehicle[0], VS_ANY_PASSENGER))
//							#IF IS_DEBUG_BUILD
//							CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_CREATOR_TRAILER - MAINTAIN_COMMAND_CENTER_EXIT_CHECKS - Player can exit to cab, g_SimpleInteriorData.bTruckCabOwnerAccess = TRUE")
//							#ENDIF
//							g_SimpleInteriorData.bTruckCabOwnerAccess = TRUE
//						ELSE
							// Prevent the 'Exit to Cab' menu option being displayed.
							g_SimpleInteriorData.bTruckCabOwnerAccess = FALSE
						
//							#IF IS_DEBUG_BUILD
//							CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_CREATOR_TRAILER - MAINTAIN_COMMAND_CENTER_EXIT_CHECKS - Armoury truck cab is not accessible at this time.")
//							#ENDIF
//						ENDIF
//					ELSE
//						#IF IS_DEBUG_BUILD
//						CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_CREATOR_TRAILER - MAINTAIN_COMMAND_CENTER_EXIT_CHECKS - Player cannot exit to armoury truck cab as they're not the owner of the truck.")
//						#ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
			
			IF g_SimpleInteriorData.bExitMenuOptionAccepted
				START_CREATOR_TRAILER_EXIT()
			ENDIF
		ELSE
			IF IS_BIT_SET(g_SimpleInteriorData.iBSTruckExit, BS_SIMPLE_INTERIOR_GLOBAL_DATA_REFRESH_MENU_OPTIONS)
				CLEAR_BIT(g_SimpleInteriorData.iBSTruckExit, BS_SIMPLE_INTERIOR_GLOBAL_DATA_REFRESH_MENU_OPTIONS)
				#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_CREATOR_TRAILER - MAINTAIN_COMMAND_CENTER_EXIT_CHECKS - Clearing BS_SIMPLE_INTERIOR_GLOBAL_DATA_REFRESH_MENU_OPTIONS")
				#ENDIF
			ENDIF
			
			g_SimpleInteriorData.bTruckCabOwnerAccess = FALSE
			g_SimpleInteriorData.bShouldExitMenuBeVisible = FALSE
		ENDIF
	ENDIF
ENDPROC

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                 MAIN LOGIC PROC                  /////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

PROC MAINTAIN_BUNKER_ENTRANCE_FADE()

	IF g_iShouldLaunchTruckTurret != -1
		IF DOES_ENTITY_EXIST(g_viGunRunTruckCabIamIn)
			IF g_ownerOfArmoryTruckPropertyIAmIn = NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_PED_IN_VEHICLE_SEAT(g_viGunRunTruckCabIamIn, VS_DRIVER))
			AND g_ownerOfArmoryTruckPropertyIAmIn != INVALID_PLAYER_INDEX()
				IF IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(g_ownerOfArmoryTruckPropertyIAmIn)

					IF NOT IS_SCREEN_FADED_OUT()
						CDEBUG1LN(DEBUG_NET_GUN_TURRET, "IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR: MAINTAIN_BUNKER_ENTRANCE_FADE: DO_SCREEN_FADE_OUT")
						DO_SCREEN_FADE_OUT(500)
					ENDIF
				ELSE
	//				CDEBUG1LN(DEBUG_NET_GUN_TURRET, "IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR: IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR: false")
					IF IS_SCREEN_FADED_OUT()
						CDEBUG1LN(DEBUG_NET_GUN_TURRET, "MAINTAIN_BUNKER_ENTRANCE_FADE: DO_SCREEN_FADE_IN")
						DO_SCREEN_FADE_IN(500)
					ENDIF
				ENDIF
			ENDIF
		ENDIF	
	ENDIF

	
ENDPROC

PROC RUN_MAIN_CLIENT_LOGIC()

	DISABLE_VEH_CONTROLS()
	MAINTAIN_TRUCK_INTERIOR_MAP()
	MAINTAIN_PLAYER_VEHICLE_LOCKS()
	MAINTAIN_CAN_DRIVE_OUT_OF_CREATOR_TRAILER()
	MAINTAIN_GET_OUT_OF_OWNERS_VEHICLE_IN_CREATOR_TRAILER()
	MAINTAIN_BUNKER_ENTRY_FROM_CREATOR_TRAILER_ON_FOOT()
//	INIT_BED_DATA()
	MAINTAIN_BUNKER_ENTRANCE_FADE()
	
	MAINTAIN_CREATOR_TRAILER_GUN_LOCKER(sArmoryTruckGunLocker)
	
	MAINTAIN_CREATOR_TRAILER_MONITORS(sTruckMonitorData)
	
	IF IS_PAUSE_MENU_ACTIVE_EX()
	OR IS_SYSTEM_UI_BEING_DISPLAYED()
	OR IS_INTERACTION_MENU_OPEN()
		REINIT_NET_TIMER(pauseMenuInteractionsDelay,TRUE)
	ENDIF

	
	IF IS_CREATOR_TRAILER_STATE(CREATOR_TRAILER_STATE_LOADING)
		IF IS_PLAYER_IN_CREATOR_TRAILER(PLAYER_ID())
			IF REQUEST_ARMORY_TRUCK_SECTION_ASSETS(thisCreatorTrailer.pOwner)
				IF GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()) = GET_HASH_KEY("GtaMloRoom001") //Model swap for sections should wait until inside the interior
					//Create sections in loading state
					IF NOT HAVE_ARMORY_TRUCK_SECTIONS_LOADED()
						SET_ARMORY_TRUCK_UPDATE_BIT(AT_UB_SECTIONS_REFRESH_REQUIRED)
						ARMORY_TRUCK_REQUEST_INTERIOR_UPDATE()
						IF thisCreatorTrailer.pOwner != INVALID_PLAYER_INDEX()
							UPDATE_ARMORY_TRUCK_SECTIONS(thisCreatorTrailer.sPropertySections, thisCreatorTrailer.pOwner)
							MAINTAIN_ACTIVITY_PROP_TINTS()
						ENDIF
					ENDIF
					//Create car mod and car after the interior sections created.
					IF HAVE_ARMORY_TRUCK_SECTIONS_LOADED()	
						IF MAINTAIN_LAUNCHING_CARMOD_SCRIPT()
							// This is important, let internal script know that its child script is ready
							SET_SIMPLE_INTERIOR_CHILD_SCRIPT_READY(TRUE)
							
							// Move on to the next stage
							SET_TRANS_DONT_CLEAR_ENTERING_FLAG(FALSE)
							SET_CREATOR_TRAILER_STATE(CREATOR_TRAILER_STATE_IDLE)
						ELSE
							#IF IS_DEBUG_BUILD
							IF GET_FRAME_COUNT() % 30 = 0
								PRINTLN("RUN_MAIN_CLIENT_LOGIC- waiting for MAINTAIN_LAUNCHING_CARMOD_SCRIPT")
							ENDIF
							#ENDIF
						ENDIF
					ELSE
						PRINTLN("RUN_MAIN_CLIENT_LOGIC- waiting for MAINTAIN_CUSTOM_CAR_CREATION")
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
					IF GET_FRAME_COUNT() % 30 = 0
						PRINTLN("RUN_MAIN_CLIENT_LOGIC- waiting for RoomKey, Current: ", GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()))
					ENDIF
					#ENDIF
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
				IF GET_FRAME_COUNT() % 30 = 0
					PRINTLN("RUN_MAIN_CLIENT_LOGIC- waiting for REQUEST_CREATOR_TRAILER_SECTION_ASSETS")
				ENDIF
				#ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			IF GET_FRAME_COUNT() % 30 = 0
				PRINTLN("RUN_MAIN_CLIENT_LOGIC- waiting for IS_PLAYER_IN_CREATOR_TRAILER")
			ENDIF
			#ENDIF
		ENDIF
	ELIF IS_CREATOR_TRAILER_STATE(CREATOR_TRAILER_STATE_IDLE)
		
		IF thisCreatorTrailer.pOwner != INVALID_PLAYER_INDEX()
			UPDATE_ARMORY_TRUCK_SECTIONS(thisCreatorTrailer.sPropertySections, thisCreatorTrailer.pOwner)
			MAINTAIN_ACTIVITY_PROP_TINTS()
		ELSE
			PRINTLN("RUN_MAIN_CLIENT_LOGIC- wowner is invalid ")
		ENDIF
		
		IF g_bArmoryTruckCameraTimeSlicing
			USE_ACTIVE_CAMERA_FOR_TIMESLICING_CENTRE()
		ENDIF
		
		MAINTAIN_COMMAND_CENTER_EXIT_CHECKS()

		SET_PLAYER_MOVING_FROM_BUNKER_TO_TRUCK(FALSE)
		
		RUN_ACTIVITY_LOGIC()
		
		MANAGE_PLAYER_JACK_FLAGS()
	
		IF thisCreatorTrailer.pOwner != INVALID_PLAYER_INDEX()
			UPDATE_ARMORY_TRUCK_SECTIONS(thisCreatorTrailer.sPropertySections, thisCreatorTrailer.pOwner)
			MAINTAIN_ACTIVITY_PROP_TINTS()
		ENDIF

		RUN_COVERT_ARMS_LOGIC()
		
		IF NOT IS_BIT_SET(thisCreatorTrailer.iLocalBS, BS_CREATOR_TRAILER_CALLED_CLEAR_HELP)
			IF IS_SCREEN_FADED_IN()
			AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
				CLEAR_HELP()
				SET_BIT(thisCreatorTrailer.iLocalBS, BS_CREATOR_TRAILER_CALLED_CLEAR_HELP)
				PRINTLN("AM_MP_CREATOR_TRAILER - Calling clear help")
			ENDIF
		ENDIF
		
	ELIF IS_CREATOR_TRAILER_STATE(CREATOR_TRAILER_STATE_GOTO_EXIT)

		IF g_SimpleInteriorData.iExitMenuOption = 0
			CLEAR_EXIT_MENU_OPTION_INDEX()
			
			CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_CREATOR_TRAILER - Changing state from CREATOR_TRAILER_STATE_GOTO_EXIT to CREATOR_TRAILER_STATE_EXIT_TO_FREEMODE")
			SET_CREATOR_TRAILER_STATE(CREATOR_TRAILER_STATE_EXIT_TO_FREEMODE)
			
		ELIF g_SimpleInteriorData.iExitMenuOption = 1
				// Disabling 'Exit to Cab' functionality.
//			CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_CREATOR_TRAILER - Changing state from CREATOR_TRAILER_STATE_GOTO_EXIT to CREATOR_TRAILER_STATE_EXIT_TO_CAB_IN_FREEMODE")
//			
//			IF IS_SCREEN_FADED_OUT()
//				CLEAR_EXIT_MENU_OPTION_INDEX()
//				SET_CREATOR_TRAILER_STATE(CREATOR_TRAILER_STATE_EXIT_TO_CAB_IN_FREEMODE)
//			ELSE
//				DO_SCREEN_FADE_OUT(500)
//			ENDIF
			
			// Temporarily replacing 'Exit to Cab' with a normal truck exit.
			CLEAR_EXIT_MENU_OPTION_INDEX()
			CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_CREATOR_TRAILER - Changing state from CREATOR_TRAILER_STATE_GOTO_EXIT to CREATOR_TRAILER_STATE_EXIT_TO_FREEMODE")
			SET_CREATOR_TRAILER_STATE(CREATOR_TRAILER_STATE_EXIT_TO_FREEMODE)
		ENDIF

	ELIF IS_CREATOR_TRAILER_STATE(CREATOR_TRAILER_STATE_EXIT_TO_FREEMODE)
		CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_CREATOR_TRAILER - STATE changed to CREATOR_TRAILER_STATE_EXIT_TO_FREEMODE")

		IF NOT IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
			// Flag used to determine if it's safe to exit the armory truck.
			g_SimpleInteriorData.bTriggerExitFromArmoryTruck = TRUE
		ENDIF	
			
		TRIGGER_EXIT_FROM_SIMPLE_INTERIOR_NOW()

	ELIF IS_CREATOR_TRAILER_STATE(CREATOR_TRAILER_STATE_EXIT_TO_CAB_IN_FREEMODE)
		CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_CREATOR_TRAILER - STATE changed to CREATOR_TRAILER_STATE_EXIT_TO_CAB_IN_FREEMODE")
		
		IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		AND DOES_ENTITY_EXIST(GET_CREATOR_TRAILER_CAB_VEH_INDEX())
		AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		AND NOT IS_ENTITY_DEAD(GET_CREATOR_TRAILER_CAB_VEH_INDEX())
			IF g_SimpleInteriorData.bTruckCabOwnerAccess
				IF PLAYER_ID() != INVALID_PLAYER_INDEX()
					CDEBUG1LN(DEBUG_PROPERTY, "Warping player ", NATIVE_TO_INT(PLAYER_ID()), " into armory truck cab while it is in freemode.")
				ENDIF
				
				SET_BIT(g_SimpleInteriorData.iBSTruckExit, BS_SIMPLE_INTERIOR_GLOBAL_DATA_EXIT_TO_TRUCK_CAB_IN_FREEMODE)
				
				g_SimpleInteriorData.bTriggerArmoryTruckExitToCabFreemode = TRUE				
				g_SimpleInteriorData.bTriggerExitFromArmoryTruck = TRUE
				TRIGGER_EXIT_FROM_SIMPLE_INTERIOR_NOW()
			ENDIF
		ELSE
			CDEBUG1LN(DEBUG_PROPERTY, "AM_MP_CREATOR_TRAILER - CREATOR_TRAILER_STATE_FINISHING_EXIT - Player entity does not exist.")
		ENDIF

		IF IS_SCREEN_FADED_OUT()
			DO_SCREEN_FADE_IN(500)		
		ENDIF
	ENDIF
ENDPROC

PROC RUN_MAIN_SERVER_LOGIC()
	
ENDPROC

#IF IS_DEBUG_BUILD
PROC UPDATE_DEBUG_WIDGETS()
	IF HAVE_ARMORY_TRUCK_SECTIONS_LOADED()
	AND GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()) = GET_HASH_KEY("GtaMloRoom001")
		UPDATE_SECTIONS_DEBUG_WIDGET(g_sArmoryTruckSectionsDebug, thisCreatorTrailer.sPropertySections, thisCreatorTrailer.pOwner)
	ENDIF
ENDPROC
#ENDIF


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                      Main Loop                      //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
SCRIPT  (SIMPLE_INTERIOR_CHILD_SCRIPT_DATA scriptData) 
	
	
	IF NETWORK_IS_GAME_IN_PROGRESS() 
		SCRIPT_INITIALISE(scriptData)	
	ELSE
		SCRIPT_CLEANUP()
	ENDIF
	
	#IF IS_DEBUG_BUILD
	CREATE_SECTION_DEBUG_WIDGET(g_sArmoryTruckSectionsDebug)
	#ENDIF
	// Main loop
	WHILE TRUE
		
		// One wait to rule them all. This can be the ONLY wait from here on in.....
		MP_LOOP_WAIT_ZERO()
				
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			PRINTLN("AM_MP_CREATOR_TRAILER - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE = TRUE")
			SCRIPT_CLEANUP()
		ENDIF
		
		IF SHOULD_THIS_SIMPLE_INTERIOR_CHILD_THREAD_TERMINATE(scriptData.iScriptInstance, thisCreatorTrailer.eSimpleInteriorID)
			PRINTLN("AM_MP_CREATOR_TRAILER - SHOULD_THIS_SIMPLE_INTERIOR_CHILD_THREAD_TERMINATE = TRUE")
			SCRIPT_CLEANUP()
		ENDIF
		
		RUN_MAIN_CLIENT_LOGIC()
		
		// -----------------------------------
		// Process server game logic		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			RUN_MAIN_SERVER_LOGIC()
		ENDIF
		
		#IF IS_DEBUG_BUILD
			UPDATE_DEBUG_WIDGETS()
		#ENDIF
		
	ENDWHILE
	

ENDSCRIPT
