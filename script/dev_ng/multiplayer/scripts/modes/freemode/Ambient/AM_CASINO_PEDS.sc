//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        AM_CASINO_PEDS.sch																						//
// Description: Script for controlling the Casino peds.																	//
// Written by:  Online Technical Team: Scott Ranken																		//
// Date:  		08/02/19																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "net_casino_peds_header.sch"

#IF IS_DEBUG_BUILD
USING "net_casino_peds_debug.sch"
#ENDIF

#IF FEATURE_CASINO
CASINO_STRUCT casinoStruct
SERVER_BROADCAST_DATA serverBD

SCRIPT
	PROCESS_PRE_GAME(serverBD)
	RESET_CASINO_PEDS(casinoStruct)
	INITIALISE_CASINO_PED_GLOBALS()
	REQUEST_INITIAL_CASINO_PED_ASSETS(casinoStruct, serverBD)
	
	// should we trigger one of the rare guards?
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		MAINTAIN_RARE_GUARD_TRIGGER(serverBD, casinoStruct, TRUE, g_sMPtunables.VC_Penthouse_Movie_Variation)
	ENDIF
	
	
	RESERVE_NETWORK_MISSION_PEDS(3) // for the patrol peds
	RESERVE_NETWORK_MISSION_OBJECTS(1) // for waitress tray
	
	WHILE TRUE
		// Call RUN_CASINO_PEDS_UPDATE again if Casino active state has changed
		IF SHOULD_WAIT_ZERO()
		
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			SCRIPT_PROFILER_END_OF_FRAME()
			#ENDIF
			#ENDIF				 			
			WAIT(0)
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			SCRIPT_PROFILER_START_OF_FRAME()
			#ENDIF
			#ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			PRINTLN("[CASINO_PEDS] Main Script - Skipping wait")
			#ENDIF
			SET_LAST_CASINO_ACTIVE_STATE(g_eCasinoState)
		ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("start")
		#ENDIF	
		#ENDIF
		
		IF NETWORK_IS_GAME_IN_PROGRESS()
			
			#IF IS_DEBUG_BUILD
			CREATE_AND_INITIALISE_CASINO_PED_WIDGETS(casinoStruct, serverBD)
			MAINTAIN_CASINO_PED_WIDGETS(casinoStruct, serverBD)
			
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("debug widgets")
			#ENDIF		
			#ENDIF
			
			MAINTAIN_PATROL_PEDS_CLIENT(serverBD.sPatrolPedsBDData, casinoStruct.PatrolPedsLocalData)
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_PATROL_PEDS_CLIENT")
			#ENDIF	
			#ENDIF		
			
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				IF (g_sMPtunables.VC_Penthouse_Movie_Variation != 1)
					MAINTAIN_RARE_GUARD_TRIGGER(serverBD, casinoStruct, FALSE, 0)
				ENDIF
				MAINTAIN_PATROL_PEDS(serverBD.sPatrolPedsBDData, casinoStruct.PatrolPedsLocalData)
			ENDIF
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_PATROL_PEDS")
			#ENDIF	
			#ENDIF	
			
		ENDIF
		
		RUN_CASINO_PEDS_UPDATE(casinoStruct, serverBD)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("RUN_CASINO_PEDS_UPDATE")
		#ENDIF	
		#ENDIF			
	ENDWHILE
ENDSCRIPT
#ENDIF // FEATURE_CASINO
