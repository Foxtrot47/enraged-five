//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        AM_CRATE_DROP.sch																			//
// Description: An AI Cargo Plane flies over various locations and makes a number of crate drops.			//
// Written by:  Ryan Baker																					//
// Date: 10/09/2012																							//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"

// Game Headers
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_path.sch"
USING "building_control_public.sch"
USING "menu_public.sch"
USING "commands_money.sch"

// Network Headers
USING "net_include.sch"
USING "net_events.sch"

USING "net_blips.sch"
// CnC Headers
USING "net_scoring_common.sch"
USING "net_mission.sch"
USING "shared_hud_displays.sch"
USING "net_entity_icons.sch"

USING "net_crate_drop.sch"
USING "net_hud_displays.sch"
USING "net_ammo_drop.sch"
USING "fm_in_corona_header.sch"

USING "net_wait_zero.sch"

USING "net_cash_transactions.sch"

#IF IS_DEBUG_BUILD
USING "net_debug_log.sch"
USING "profiler.sch"
#ENDIF

USING "net_gang_boss.sch"

///////////////////////////////////
///    		CONSTANTS    		///
///////////////////////////////////

// Game States. 
CONST_INT GAME_STATE_INI 				0
CONST_INT GAME_STATE_RUNNING			1
CONST_INT GAME_STATE_LEAVE				2
CONST_INT GAME_STATE_TERMINATE_DELAY	3
CONST_INT GAME_STATE_END				4

ENUM CRATE_DROP_STAGE_ENUM
	eCD_SETUP,
	eCD_DROP_AND_COLLECT,
	eCD_CLEANUP
ENDENUM

CONST_FLOAT CRATE_PICKUP_RANGE 		2.0		//1.5
//CONST_FLOAT PLANE_TARGET_RADIUS		17.5
//CONST_FLOAT PLANE_START_DROP		17.5
//CONST_FLOAT PLANE_NORMAL_SPEED		20.0
//CONST_FLOAT PLANE_SLOW_SPEED		15.0
//CONST_INT PLANE_SLOW_RADIUS			150
//CONST_INT PLANE_HEIGHT_ABOVE_DROP	120		//100		//50
//CONST_INT PLANE_LOW_HEIGHT			60

//CONST_INT PLANE_CREATION_HEIGHT		500
//CONST_INT PLANE_CREATION_RADIUS		2000

CONST_INT CRATE_DELIVERY_TIME	60000

STRUCT DESTRUCTIBLE_CRATE_DATA
	NETWORK_INDEX NetID
	MODEL_NAMES Model = Prop_Box_Wood02A_PU
ENDSTRUCT

///////////////////////////////////
///    		SERVER BD    		///
///////////////////////////////////
//Server Bitset Data
CONST_INT biS_AnyCrewArrested		0
CONST_INT biS_AllCrewArrested		1
CONST_INT biS_AllCrewDead			2
CONST_INT biS_AllCrewLeftStartArea	3
CONST_INT biS_AnyCrewHasFinished 	4
CONST_INT biS_AllCrewHaveFinished 	5
CONST_INT biS_AllCrewHaveLeft 		6
CONST_INT biS_GetWaypointVector		7
CONST_INT biS_WaypointSearchStarted	8
//CONST_INT biS_PlaneReservationReduced	9
//CONST_INT biS_PilotReservationReduced	10
CONST_INT biS_SpecialCrateDrop			11
CONST_INT biS_CrateBreakableTriggered	12
CONST_INT biS_CrateBreakableDone		13
CONST_INT biS_InAdditionalTime			14

// The server broadcast data.
// Everyone can read this data, only the server can update it
STRUCT ServerBroadcastData
	INT iServerGameState
		
	INT iServerBitSet
	
	CRATE_DROP_DATA CrateDropData
	
	INT iDropsDone
	
	//INT iPlaneDestroyer = -1
	
	INT iReservedVehicles = 0	//1
	INT iReservedPeds = 0		//1
	INT iReservedObjects = 3
	
	CRATE_DROP_STAGE_ENUM eCrateDropStage = eCD_SETUP
	
	DESTRUCTIBLE_CRATE_DATA DCrateData[MAX_NUM_CRATE_DROPS]
	
	SCRIPT_TIMER MissionTerminateDelayTimer
	SCRIPT_TIMER CrateDeliveryTimer
	SCRIPT_TIMER CrateCreationTimeoutTimer
ENDSTRUCT
ServerBroadcastData serverBD


///////////////////////////////////
///    		PLAYER BD    		///
///////////////////////////////////
CONST_INT biP_LandAnimStarted			0
CONST_INT biP_LandAnimPlaying			3
CONST_INT biP_LandAnimDone				6
CONST_INT biP_LandBlipChangeDone		9
CONST_INT biP_PickedUpCrate				12
CONST_INT biP_DetachDestructibleCrate	15
//CONST_INT biP_DestroyedPlane			30

// The client broadcast data.
// Everyone can read this data, only the local player only the server can update it
STRUCT PlayerBroadcastData
	INT iGameState
	
	INT iPlayerBitSet
	INT iCashBet
	
	//TAKE_MONEY_CLIENT_STRUCT TakeMoneyClientData[MAX_NUM_CRATE_DROPS]
	
	CRATE_DROP_STAGE_ENUM eCrateDropStage = eCD_SETUP
ENDSTRUCT
PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]


///////////////////////////////////
///    		VARIABLES    		///
///////////////////////////////////
INT iBoolsBitSet
CONST_INT biHelpDisplayed1			0
CONST_INT biDroppedTicker0			1
CONST_INT biLandTicker1				2
CONST_INT biLandTicker2				3
CONST_INT biAllCollectedTicker		10
//CONST_INT biPlaneDestroyedTicker	11
CONST_INT biSequencesCreated		12
CONST_INT biSpawnAtDropSet			13

INT iCrateRewadrGivenBitSet

BLIP_INDEX CrateBlip[MAX_NUM_CRATE_DROPS]
BLIP_INDEX RadiusBlip[MAX_NUM_CRATE_DROPS]
//BLIP_INDEX PlaneBlip
//BLIP_INDEX LocationBlip[MAX_NUM_CRATE_DROPS]

//SEQUENCE_INDEX seqPlaneLeave

AI_BLIP_STRUCT PedBlipData[MAX_NUM_CRATE_DROPS][LOCATION_MAX_NUM_PEDS]
INT iTrackKillBitSet[MAX_NUM_CRATE_DROPS]
CONST_INT biTrackedPedKill	0

SCRIPT_TIMER EndScriptTimer
CONST_INT iEndScriptDelay	60000

OBJECT_INDEX FlareCanister[MAX_NUM_CRATE_DROPS]
MODEL_NAMES FlareCanisterModel = PROP_FLARE_01

//INT PickupSoundID[MAX_NUM_CRATE_DROPS]

//TAKE_MONEY_STRUCT TakeMoneyData[MAX_NUM_CRATE_DROPS]
//SCRIPT_TIMER iTakeMoneyDataUpdateTimer[MAX_NUM_CRATE_DROPS]
//BOOL bTakeMoneyDone[MAX_NUM_CRATE_DROPS]

INT iEarnedXP
INT iEarnedCash
INT iWeaponHash
INT iOtherItemHash
INT iEnemiesKilled
ItemHashArray iSpecialItemHash
BOOL bCollectedArmour

//CDM 4/12/12
BOOL bAddedToActAvailable

///////////////////////////////////////////
///    		GET LOCATION DATA    		///
///////////////////////////////////////////
CONST_INT CRATE_DROP_LOCATION_0		0		//WINDFARM
CONST_INT CRATE_DROP_LOCATION_1		1		//BUILDING SITE (COUNTRY)
CONST_INT CRATE_DROP_LOCATION_2		2		//RIVER (NEAR MILITARY AIRPORT)
CONST_INT CRATE_DROP_LOCATION_3		3		//DAM
CONST_INT CRATE_DROP_LOCATION_4		4		//STABLES

CONST_INT CRATE_DROP_LOCATION_5		5		//VINEWOOD SIGN
CONST_INT CRATE_DROP_LOCATION_6		6		//OIL FIELD
CONST_INT CRATE_DROP_LOCATION_7		7		//DOCKS CONTAINERS
CONST_INT CRATE_DROP_LOCATION_8		8		//DOCKS SMALL
CONST_INT CRATE_DROP_LOCATION_9		9		//NEW ESTATE DEADEND

CONST_INT CRATE_DROP_LOCATION_10	10		//HORSE RACING COURSE
CONST_INT CRATE_DROP_LOCATION_11	11		//SMALL DESERT HOUSE
CONST_INT CRATE_DROP_LOCATION_12	12		//NORTH OF BIG LAKE
CONST_INT CRATE_DROP_LOCATION_13	13		//SECLUDED BEACH - NORTH WEST
CONST_INT CRATE_DROP_LOCATION_14	14		//BEACH - NEAR MILITARY BASE

CONST_INT CRATE_DROP_LOCATION_15	15		//BEACH - NEAR HOUSES - WEST
CONST_INT CRATE_DROP_LOCATION_16	16		//BEACH - NEAR FUNFAIR PIER
CONST_INT CRATE_DROP_LOCATION_17	17		//SMALL PARK - NORTH OF CITY
CONST_INT CRATE_DROP_LOCATION_18	18		//OBSERVATORY CAR PARK
CONST_INT CRATE_DROP_LOCATION_19	19		//STORM DRAIN

CONST_INT CRATE_DROP_LOCATION_20	20		//WAREHOUSE
CONST_INT CRATE_DROP_LOCATION_21	21		//GREEN - OCEAN ARCH
CONST_INT CRATE_DROP_LOCATION_22	22		//QUARRY
CONST_INT CRATE_DROP_LOCATION_23	23		//ALIEN HIPPY
CONST_INT CRATE_DROP_LOCATION_24	24		//FARM - FAR NORTH

CONST_INT CRATE_DROP_LOCATION_25	25		//OIL PUMPS - COUNTRY
CONST_INT CRATE_DROP_LOCATION_26	26		//BEACH RUINS
CONST_INT CRATE_DROP_LOCATION_27	27		//GRAVEYARD
CONST_INT CRATE_DROP_LOCATION_28	28		//RIVER - COVERED BRIDGE
CONST_INT CRATE_DROP_LOCATION_29	29		//RIVER ENTRANCE - COUNTRY

CONST_INT CRATE_DROP_LOCATION_30	30		//MAZE BANK - CITY
CONST_INT CRATE_DROP_LOCATION_31	31		//MIRROR PARK - CITY

CONST_INT CRATE_DROP_LOCATION_MAX	32

// Widgets & Debug.
#IF IS_DEBUG_BUILD
	BOOL bHostEndMissionNow
	//BOOL bBlipPlane
	BOOL bBlipAi
	BOOL bBlipAllLocations
	BLIP_INDEX LocationBlip[CRATE_DROP_LOCATION_MAX]
	BLIP_INDEX AiBlip[MAX_NUM_CRATE_DROPS][LOCATION_MAX_NUM_PEDS]
	BOOL bWarpToDrop[MAX_NUM_CRATE_DROPS]
	BOOL debug_bDisplayDebug
#ENDIF

//PURPOSE: Reutns the VECTOR for the passed in Crate Drop
FUNC VECTOR GET_CRATE_DROP_VECTOR(INT iIdentifier)
	SWITCH iIdentifier
		CASE CRATE_DROP_LOCATION_0		RETURN <<2312.7073, 1531.2584, 59.8792>>	//WINDFARM
		CASE CRATE_DROP_LOCATION_1		RETURN <<1069.5798, 2366.3628, 43.0396>>	//BUILDING SITE (COUNTRY)
		CASE CRATE_DROP_LOCATION_2		RETURN <<-1979.7805, 2584.1609, 2.2587>>	//RIVER (NEAR MILITARY AIRPORT)
		CASE CRATE_DROP_LOCATION_3		RETURN <<1915.9016, 563.9669, 174.4925>>	//DAM
		CASE CRATE_DROP_LOCATION_4		RETURN <<1458.3934, 1113.0664, 113.3340>>	//STABLES
		
		CASE CRATE_DROP_LOCATION_5		RETURN <<817.5361, 1316.5696, 362.0491>>	//VINEWOOD SIGN
		CASE CRATE_DROP_LOCATION_6		RETURN <<1402.8578, -2148.4177, 57.9675>>	//OIL FIELD
		CASE CRATE_DROP_LOCATION_7		RETURN <<1104.8152, -3142.3347, 4.9010>>	//DOCKS CONTAINERS
		CASE CRATE_DROP_LOCATION_8		RETURN <<545.7347, -3025.3271, 5.0591>>		//DOCKS SMALL
		CASE CRATE_DROP_LOCATION_9		RETURN <<1376.0369, -740.2166, 66.2331>>	//NEW ESTATE DEADEND
		
		CASE CRATE_DROP_LOCATION_10		RETURN <<1147.1620, 126.3452, 80.8693>>		//HORSE RACING COURSE
		CASE CRATE_DROP_LOCATION_11		RETURN <<1671.2318, 3041.1763, 53.0351>>	//SMALL DESERT HOUSE
		CASE CRATE_DROP_LOCATION_12		RETURN <<1015.5989, 4350.6685, 41.4840>>	//NORTH OF BIG LAKE
		CASE CRATE_DROP_LOCATION_13		RETURN <<-1731.0601, 4959.4839, 3.8134>>	//SECLUDED BEACH - NORTH WEST
		CASE CRATE_DROP_LOCATION_14		RETURN <<-2436.2498, 4181.9131, 7.7719>>	//BEACH - NEAR MILITARY BASE

		CASE CRATE_DROP_LOCATION_15		RETURN <<-3163.3643, 756.8276, 2.1183>>		//BEACH - NEAR HOUSES - WEST
		CASE CRATE_DROP_LOCATION_16		RETURN <<-1731.0846, -989.8386, 4.4152>>	//BEACH - NEAR FUNFAIR PIER
		CASE CRATE_DROP_LOCATION_17		RETURN <<-799.6782, 885.0139, 202.1319>>	//<<-818.7764, 865.3253, 201.9869>>	//SMALL PARK - NORTH OF CITY
		CASE CRATE_DROP_LOCATION_18		RETURN <<-401.2717, 1211.6768, 324.9297>>	//OBSERVATORY CAR PARK
		CASE CRATE_DROP_LOCATION_19		RETURN <<728.2132, -1532.4799, 18.7348>>	//STORM DRAIN
		
		CASE CRATE_DROP_LOCATION_20		RETURN <<1140.9230, -1285.6388, 33.6091>>	//WAREHOUSE
		CASE CRATE_DROP_LOCATION_21		RETURN <<2909.1543, 770.3181, 21.1684>>		//GREEN - OCEAN ARCH
		CASE CRATE_DROP_LOCATION_22		RETURN <<2778.9092, 2853.2659, 34.7828>>	//QUARRY
		CASE CRATE_DROP_LOCATION_23		RETURN <<2463.1421, 3769.6511, 40.3280>>	//ALIEN HIPPY
		CASE CRATE_DROP_LOCATION_24		RETURN <<1888.6101, 4626.8149, 37.4665>>	//FARM - FAR NORTH
		
		CASE CRATE_DROP_LOCATION_25		RETURN <<585.3987, 2893.7783, 38.5297>>		//OIL PUMPS - COUNTRY
		CASE CRATE_DROP_LOCATION_26		RETURN <<2811.4529, -668.6710, 1.5810>>		//BEACH RUINS
		CASE CRATE_DROP_LOCATION_27		RETURN <<-1706.0131, -183.4387, 56.3712>>	//-1729.3708, -193.2892, 57.4816>>	//GRAVEYARD
		CASE CRATE_DROP_LOCATION_28		RETURN <<-498.3305, 3006.9063, 27.4341>>	//RIVER - COVERED BRIDGE
		CASE CRATE_DROP_LOCATION_29		RETURN <<-388.8584, 4353.4780, 54.3806>>	//RIVER ENTRANCE - COUNTRY
		CASE CRATE_DROP_LOCATION_30		RETURN <<-134.0402, -869.4509, 43.2175>>	//MAZE BANK - CITY					//BUSINESS PACK
		CASE CRATE_DROP_LOCATION_31		RETURN <<1098.2463, -544.2941, 56.4061>>	//MIRROR PARK - CITY				//HIPSTER PACK
	ENDSWITCH
	
	RETURN <<0, 0, 0>>
ENDFUNC

FUNC BOOL SHOULD_HIDE_LOCAL_UI()
//	IF HANDLE_DEPRECATED_HIDE_BLIP(ciPI_HIDE_MENU_DEPRECATED_AMB_CRATE_DROP)
//		RETURN TRUE
//	ENDIF
	
			// Deprecated piece of content
	PRINTLN("     ---------->     CRATE DROP -  Deprecated Content, hiding")
	RETURN TRUE
ENDFUNC

//PURPOSE: Setup the basic data
PROC STORE_LAST_LOCATION()
	INT j
	INT iSlot
	REPEAT CRATE_DROP_MAX_LAST_LOCATIONS j
		iSlot = CRATE_DROP_MAX_LAST_LOCATIONS-1-j
		IF iSlot > 0
			MPGlobalsAmbience.CrateDropLaunchData.vLastLocation[iSlot] = MPGlobalsAmbience.CrateDropLaunchData.vLastLocation[iSlot-1]
		ELSE
			MPGlobalsAmbience.CrateDropLaunchData.vLastLocation[iSlot] = GET_CRATE_DROP_VECTOR(serverBD.CrateDropData.LocationData[0].iIdentifier)
		ENDIF
		NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - STORE_LAST_LOCATION - vLastLocation[") NET_PRINT_INT(iSlot) NET_PRINT("] = ") NET_PRINT_VECTOR(MPGlobalsAmbience.CrateDropLaunchData.vLastLocation[iSlot]) NET_NL()
	ENDREPEAT
ENDPROC

//PURPOSE: Setup the basic data
PROC SETUP_BASIC_DATA()
	
	//TUNABLES TEST
	//g_sMPTunables.iCrateDropLocation	= CRATE_DROP_LOCATION_4		//STABLES
	//g_sMPTunables.iCrateDropLocation2	= CRATE_DROP_LOCATION_0		//WINDFARM
	//g_sMPTunables.iCrateDropLocation3	= CRATE_DROP_LOCATION_5		//VINEWOOD
	//g_sMPTunables.iCrateDropLocation4	= CRATE_DROP_LOCATION_1		//BUILDING SITE (COUNTRY)
	//g_sMPTunables.iCrateDropLocation5	= CRATE_DROP_LOCATION_10	//HORSE RACING COURSE
	//TUNABLES TEST
	
	//Set Location of Crate Drop
	INT j
	IF g_sMPTunables.iCrateDropLocation >= 0
	AND g_sMPTunables.iCrateDropLocation < CRATE_DROP_LOCATION_MAX
		INT iNumLocations = 1
		INT iRandomTunableLocation
		
		IF g_sMPTunables.iCrateDropLocation5 >= 0
		AND g_sMPTunables.iCrateDropLocation5 < CRATE_DROP_LOCATION_MAX
			iNumLocations = 5
			iRandomTunableLocation = GET_RANDOM_INT_IN_RANGE(0, iNumLocations)
			NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - SETUP_BASIC_DATA - RANDOM CHOICE OF 5 TUNABLE SET LOCATIONS - CHOSE ") NET_PRINT_INT(iRandomTunableLocation) NET_NL()
							
		ELIF g_sMPTunables.iCrateDropLocation4 >= 0
		AND g_sMPTunables.iCrateDropLocation4 < CRATE_DROP_LOCATION_MAX
			iNumLocations = 4
			iRandomTunableLocation = GET_RANDOM_INT_IN_RANGE(0, iNumLocations)
			NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - SETUP_BASIC_DATA - RANDOM CHOICE OF 4 TUNABLE SET LOCATIONS - CHOSE ") NET_PRINT_INT(iRandomTunableLocation) NET_NL()
			
		ELIF g_sMPTunables.iCrateDropLocation3 >= 0
		AND g_sMPTunables.iCrateDropLocation3 < CRATE_DROP_LOCATION_MAX
			iNumLocations = 3
			iRandomTunableLocation = GET_RANDOM_INT_IN_RANGE(0, iNumLocations)
			NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - SETUP_BASIC_DATA - RANDOM CHOICE OF 3 TUNABLE SET LOCATIONS - CHOSE ") NET_PRINT_INT(iRandomTunableLocation) NET_NL()
			
		ELIF g_sMPTunables.iCrateDropLocation2 >= 0
		AND g_sMPTunables.iCrateDropLocation2 < CRATE_DROP_LOCATION_MAX
			iNumLocations = 2
			iRandomTunableLocation = GET_RANDOM_INT_IN_RANGE(0, iNumLocations)
			NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - SETUP_BASIC_DATA - RANDOM CHOICE OF 2 TUNABLE SET LOCATIONS - CHOSE ") NET_PRINT_INT(iRandomTunableLocation) NET_NL()
			
		ELSE
			iNumLocations = 1
			iRandomTunableLocation = 0
			NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - SETUP_BASIC_DATA - CHOICE OF 1 TUNABLE SET LOCATION") NET_NL()
		ENDIF
		
		SWITCH iRandomTunableLocation
			CASE 0	serverBD.CrateDropData.LocationData[0].iIdentifier = g_sMPTunables.iCrateDropLocation	BREAK
			CASE 1	serverBD.CrateDropData.LocationData[0].iIdentifier = g_sMPTunables.iCrateDropLocation2	BREAK
			CASE 2	serverBD.CrateDropData.LocationData[0].iIdentifier = g_sMPTunables.iCrateDropLocation3	BREAK
			CASE 3	serverBD.CrateDropData.LocationData[0].iIdentifier = g_sMPTunables.iCrateDropLocation4	BREAK
			CASE 4	serverBD.CrateDropData.LocationData[0].iIdentifier = g_sMPTunables.iCrateDropLocation5	BREAK
		ENDSWITCH
		NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - SETUP_BASIC_DATA - B - CRATE LOCATION = ") NET_PRINT_INT(serverBD.CrateDropData.LocationData[0].iIdentifier) NET_NL()
		
		IF iNumLocations > 1
			REPEAT CRATE_DROP_MAX_LAST_LOCATIONS j
				IF ARE_VECTORS_ALMOST_EQUAL(MPGlobalsAmbience.CrateDropLaunchData.vLastLocation[j], GET_CRATE_DROP_VECTOR(serverBD.CrateDropData.LocationData[0].iIdentifier), 50)
					NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - SETUP_BASIC_DATA - B - vLastLocation = ") NET_PRINT_VECTOR(MPGlobalsAmbience.CrateDropLaunchData.vLastLocation[j]) NET_NL()
					NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - SETUP_BASIC_DATA - B - Chosen Location = ") NET_PRINT_VECTOR(GET_CRATE_DROP_VECTOR(serverBD.CrateDropData.LocationData[0].iIdentifier)) NET_NL()
					NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - SETUP_BASIC_DATA - B - LOCATION++ SAME AS LAST LOCATION ") NET_PRINT_INT(j) NET_NL()
					iRandomTunableLocation++
					IF iRandomTunableLocation >= iNumLocations
						iRandomTunableLocation = 0
					ENDIF
					SWITCH iRandomTunableLocation
						CASE 0	serverBD.CrateDropData.LocationData[0].iIdentifier = g_sMPTunables.iCrateDropLocation	BREAK
						CASE 1	serverBD.CrateDropData.LocationData[0].iIdentifier = g_sMPTunables.iCrateDropLocation2	BREAK
						CASE 2	serverBD.CrateDropData.LocationData[0].iIdentifier = g_sMPTunables.iCrateDropLocation3	BREAK
						CASE 3	serverBD.CrateDropData.LocationData[0].iIdentifier = g_sMPTunables.iCrateDropLocation4	BREAK
						CASE 4	serverBD.CrateDropData.LocationData[0].iIdentifier = g_sMPTunables.iCrateDropLocation5	BREAK
					ENDSWITCH
					NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - SETUP_BASIC_DATA - B - NEW CRATE LOCATION = ") NET_PRINT_INT(serverBD.CrateDropData.LocationData[0].iIdentifier) NET_NL()
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
	
	IF g_sMPTunables.iCrateDropLocation < 0
	OR g_sMPTunables.iCrateDropLocation >= CRATE_DROP_LOCATION_MAX
	OR serverBD.CrateDropData.LocationData[0].iIdentifier < 0
	OR serverBD.CrateDropData.LocationData[0].iIdentifier >= CRATE_DROP_LOCATION_MAX
		serverBD.CrateDropData.LocationData[0].iIdentifier = GET_RANDOM_INT_IN_RANGE(CRATE_DROP_LOCATION_0, CRATE_DROP_LOCATION_MAX)
		NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - SETUP_BASIC_DATA - A - RANDOM FROM ALL - CRATE LOCATION = ") NET_PRINT_INT(serverBD.CrateDropData.LocationData[0].iIdentifier) NET_NL()
		
		REPEAT CRATE_DROP_MAX_LAST_LOCATIONS j
			IF ARE_VECTORS_ALMOST_EQUAL(MPGlobalsAmbience.CrateDropLaunchData.vLastLocation[j], GET_CRATE_DROP_VECTOR(serverBD.CrateDropData.LocationData[0].iIdentifier), 50)
				NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - SETUP_BASIC_DATA - A - vLastLocation = ") NET_PRINT_VECTOR(MPGlobalsAmbience.CrateDropLaunchData.vLastLocation[j]) NET_NL()
				NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - SETUP_BASIC_DATA - A - Chosen Location = ") NET_PRINT_VECTOR(GET_CRATE_DROP_VECTOR(serverBD.CrateDropData.LocationData[0].iIdentifier)) NET_NL()
				NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - SETUP_BASIC_DATA - A - LOCATION++ SAME AS LAST LOCATION ") NET_PRINT_INT(j) NET_NL()
				serverBD.CrateDropData.LocationData[0].iIdentifier++
				IF serverBD.CrateDropData.LocationData[0].iIdentifier >= CRATE_DROP_LOCATION_MAX
					serverBD.CrateDropData.LocationData[0].iIdentifier = 0
				ENDIF
				NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - SETUP_BASIC_DATA - A - NEW CRATE LOCATION = ") NET_PRINT_INT(serverBD.CrateDropData.LocationData[0].iIdentifier) NET_NL()
			ENDIF
		ENDREPEAT
	ENDIF
	
	NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - SETUP_BASIC_DATA - CRATE LOCATION = ") NET_PRINT_INT(serverBD.CrateDropData.LocationData[0].iIdentifier) NET_NL()
	
	
	//Setup Special Crate Specifics
	IF GlobalServerBD_BlockB.CrateDropServerLaunchData.bUseSpecialCrate = TRUE
		SET_BIT(serverBD.iServerBitSet, biS_SpecialCrateDrop)
		NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - SETUP_BASIC_DATA - biS_SpecialCrateDrop SET ") NET_NL()
		
		serverBD.CrateDropData.iNumDrops = 1
				
	//Normal Drop
	ELSE
		//INT i
		serverBD.CrateDropData.iNumDrops = 1	//GET_RANDOM_INT_IN_RANGE(1, (MAX_NUM_CRATE_DROPS+1)) //SET FROM GLOBAL OR JUST RANDOM?
				
		//TEST
		//serverBD.CrateDropData.LocationData[0].iIdentifier = CRATE_DROP_LOCATION_30
		
		//INT iRoute = GET_RANDOM_INT_IN_RANGE(0, (CRATE_DROP_LOCATION_MAX/MAX_NUM_CRATE_DROPS))
		//BOOL bDirection  = GET_RANDOM_BOOL()
		
		//TEMP TEST
		/*serverBD.CrateDropData.iNumDrops = 2
		iRoute=1
		bDirection=TRUE*/
		
		/*REPEAT serverBD.CrateDropData.iNumDrops i
			IF bDirection
				serverBD.CrateDropData.LocationData[i].iIdentifier = (CRATE_DROP_LOCATION_0+(iRoute*3)+i)
			ELSE
				serverBD.CrateDropData.LocationData[i].iIdentifier = (CRATE_DROP_LOCATION_2+(iRoute*3)-i)
			ENDIF
		ENDREPEAT*/
	ENDIF
	
	STORE_LAST_LOCATION()
ENDPROC

///// PURPOSE:
/////    Gets a random vector within a donut centred at vPos with a radius of fRadius and a height of fHeight.
///// PARAMS:
/////    vPos - The sphere disc
/////    fInnerRadius - Radius of the hole in the middle
/////    fOuterRadius - Radius of the donut
/////    fHeight - Height of the disc (goes both above and below vPos)
///// RETURNS:
/////    A random position vector within fRadius of vPos.
//FUNC VECTOR GET_RANDOM_POINT_IN_DONUT(VECTOR vPos, FLOAT fInnerRadius, FLOAT fOuterRadius, FLOAT fHeight)
// 
//	// Pick a random unit direction vector.
//	VECTOR vDir = << GET_RANDOM_FLOAT_IN_RANGE(-1,1) , GET_RANDOM_FLOAT_IN_RANGE(-1,1), 0 >>
//	FLOAT fHalfHeight = fHeight / 2.0
//	
//	// Project this vector into the disc some random distance.
//	vDir = GET_VECTOR_OF_LENGTH(vDir, GET_RANDOM_FLOAT_IN_RANGE(fInnerRadius, fOuterRadius))
//	
//	// Grab a random height and add it on.
//	vDir.z = GET_RANDOM_FLOAT_IN_RANGE(-fHalfHeight, fHalfHeight)
//	
//	RETURN vPos + vDir
//ENDFUNC

////PURPOSE: Calculates a position to Create the plane at
//PROC GET_RANDOM_PLANE_CREATION_POINT(VECTOR vDropLocation, VECTOR &vPlanePos, FLOAT &fPlaneHeading)
//	VECTOR vPos = GET_RANDOM_POINT_IN_DONUT(vDropLocation+<<0, 0, PLANE_CREATION_HEIGHT>>, PLANE_CREATION_RADIUS/2, PLANE_CREATION_RADIUS, PLANE_CREATION_HEIGHT/2)
//	FLOAT fHeading = GET_HEADING_FROM_VECTOR_2D(vDropLocation.x-vPos.x, vDropLocation.y-vPos.y)
//	
//	FLOAT fApproxHeight = GET_APPROX_HEIGHT_FOR_AREA(vPos.x-PLANE_CREATION_RADIUS, vPos.y-PLANE_CREATION_RADIUS, vPos.x+PLANE_CREATION_RADIUS, vPos.y+PLANE_CREATION_RADIUS)
//	IF vPos.z < fApproxHeight
//		vPos.z = fApproxHeight
//		NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - GET_PLANE_CREATION_POINT - USE APPROX HEIGHT") NET_NL()
//	ENDIF
//	
//	IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(vPos, 20, 20, 20, 10)
//		vPlanePos = vPos
//		fPlaneHeading = fHeading
//		NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - GET_PLANE_CREATION_POINT - vPos = ") NET_PRINT_VECTOR(vPos) NET_NL()
//	ELSE
//		NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - GET_PLANE_CREATION_POINT - NOT SAFE LEAVE AS DEFAULT ") NET_PRINT_VECTOR(vPlanePos) NET_NL()
//	ENDIF
//ENDPROC

//PURPOSE: Gets the drop location data
PROC GET_CRATE_DROP_LOCATION_DATA(CD_LOCATION_DATA &LocationData)
	
	//Random Values
	LocationData.iNumPeds = GET_RANDOM_INT_IN_RANGE(2, LOCATION_MAX_NUM_PEDS+1)
	serverBD.iReservedPeds += LocationData.iNumPeds
	
	//Location Specific
	SWITCH LocationData.iIdentifier
		CASE CRATE_DROP_LOCATION_0		//WINDFARM
			NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - GET_CRATE_DROP_LOCATION_DATA - WINDFARM") NET_NL()
			LocationData.vLocation = GET_CRATE_DROP_VECTOR(LocationData.iIdentifier)	//<<2312.7073, 1531.2584, 59.8792>>
			LocationData.eAiSetup = CD_AI_RANDOM
			//Ped 0
			//LocationData.Peds[0].PedModel			=	G_M_Y_MexGoon_02
			LocationData.Peds[0].vCreationPos		=	<<2320.8955, 1452.7068, 62.2404>>
			LocationData.Peds[0].fHeading			=	354.4594
			/*LocationData.Peds[0].vDefensivePos		=	<<0, 0, 0>>
			LocationData.Peds[0].fDefensiveRadius	=	0
			LocationData.Peds[0].bPatrolArea		=	FALSE
			LocationData.Peds[0].RelGroup			=	rgFM_AiPed[0][0][0][0]
			LocationData.Peds[0].eCombatType		=	CD_PCT_AVERAGE
			LocationData.Peds[0].eCombatStyle		=	CD_PCS_NORMAL
			LocationData.Peds[0].Weapon				=	WEAPONTYPE_PISTOL
			LocationData.Peds[0].fAccuracy			=	30
			LocationData.Peds[0].iHealth			=	200
			LocationData.Peds[0].iArmour			=	100*/
			//Ped 1
			LocationData.Peds[1].vCreationPos		=	<<2353.5745, 1510.1249, 53.1555>>	//<<2358.8357, 1512.7664, 53.3249>>
			LocationData.Peds[1].fHeading			=	98.8266 							//97.4521
			//Ped 2
			LocationData.Peds[2].vCreationPos		=	<<2294.4844, 1508.4580, 61.1572>>
			LocationData.Peds[2].fHeading			=	284.4314
			//Ped 3
			LocationData.Peds[3].vCreationPos		=	<<2287.7532, 1476.6920, 69.8952>>
			LocationData.Peds[3].fHeading			=	313.3356
			
			//Vehicle
			//LocationData.Vehicle[0].VehModel		=	CAVALCADE
			LocationData.Vehicle[0].vCreationPos[0]	=	<<2330.3630, 1286.4026, 65.6944>>
			LocationData.Vehicle[0].fHeading[0]		=	1.8055
			LocationData.Vehicle[0].vCreationPos[1]	=	<<2380.0522, 1486.7324, 39.3839>>
			LocationData.Vehicle[0].fHeading[1]		=	173.8279
			LocationData.Vehicle[0].vCreationPos[2]	=	<<2212.9277, 1728.1342, 88.8012>>
			LocationData.Vehicle[0].fHeading[2]		=	261.9384
			//LocationData.Vehicle[0].iHealth			=	
			
			//Plane Start
			LocationData.vPlaneStart	=	<<3319.8752, 1396.2985, 177.9103>>
			LocationData.fPlaneHeading	=	84.3397
		BREAK
		
		CASE CRATE_DROP_LOCATION_1		//BUILDING SITE (COUNTRY)
			NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - GET_CRATE_DROP_LOCATION_DATA - BUILDING SITE (COUNTRY)") NET_NL()
			LocationData.vLocation = GET_CRATE_DROP_VECTOR(LocationData.iIdentifier)	//<<1069.5798, 2366.3628, 43.0396>>
			LocationData.eAiSetup = CD_AI_RANDOM
			//Peds
			LocationData.Peds[0].vCreationPos		=	<<1063.9834, 2338.2354, 48.6123>>
			LocationData.Peds[0].fHeading			=	350.1455
			LocationData.Peds[1].vCreationPos		=	<<1008.1833, 2370.1609, 54.5713>>
			LocationData.Peds[1].fHeading			=	265.6562
			LocationData.Peds[2].vCreationPos		=	<<1128.2953, 2345.0315, 52.2852>>
			LocationData.Peds[2].fHeading			=	72.3582
			LocationData.Peds[3].vCreationPos		=	<<1075.6848, 2394.3579, 51.4952>>
			LocationData.Peds[3].fHeading			=	178.4742
			//Vehicle
			LocationData.Vehicle[0].vCreationPos[0]	=	<<934.3832, 2377.9116, 45.5848>>
			LocationData.Vehicle[0].fHeading[0]		=	302.7182
			LocationData.Vehicle[0].vCreationPos[1]	=	<<1161.8107, 2398.7354, 56.8772>>
			LocationData.Vehicle[0].fHeading[1]		=	3.8970
			LocationData.Vehicle[0].vCreationPos[2]	=	<<1042.2733, 2432.6003, 44.1508>>
			LocationData.Vehicle[0].fHeading[2]		=	275.9303
			//Plane Start
			LocationData.vPlaneStart	=	<<-361.9944, 2414.3076, 471.9749>>
			LocationData.fPlaneHeading	=	263.7351
		BREAK
		
		CASE CRATE_DROP_LOCATION_2		//RIVER (NEAR MILITARY AIRPORT)
			NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - GET_CRATE_DROP_LOCATION_DATA - RIVER (NEAR MILITARY AIRPORT)") NET_NL()
			LocationData.vLocation = GET_CRATE_DROP_VECTOR(LocationData.iIdentifier)	//<<-1979.7805, 2584.1609, 2.2587>>
			LocationData.eAiSetup = CD_AI_RANDOM
			//Peds
			LocationData.Peds[0].vCreationPos		=	<<-1952.5647, 2589.5254, 1.6831>>
			LocationData.Peds[0].fHeading			=	113.6190
			LocationData.Peds[1].vCreationPos		=	<<-1981.1171, 2631.6980, 1.7020>>
			LocationData.Peds[1].fHeading			=	183.9359
			LocationData.Peds[2].vCreationPos		=	<<-1992.6781, 2632.7166, 1.9292>>
			LocationData.Peds[2].fHeading			=	197.8271
			LocationData.Peds[3].vCreationPos		=	<<-1996.6942, 2527.2024, 1.8938>>
			LocationData.Peds[3].fHeading			=	337.1221
			//Vehicle
			LocationData.Vehicle[0].vCreationPos[0]	=	<<-2214.6255, 2820.1028, 2.0340>>
			LocationData.Vehicle[0].fHeading[0]		=	215.3564
			LocationData.Vehicle[0].vCreationPos[1]	=	<<-1770.4023, 2736.6287, 4.4394>>
			LocationData.Vehicle[0].fHeading[1]		=	152.8846 
			LocationData.Vehicle[0].vCreationPos[2]	=	<<-2015.0400, 2702.4565, 2.4495>>
			LocationData.Vehicle[0].fHeading[2]		=	273.5316
			//Plane Start
			LocationData.vPlaneStart	=	<<-3106.7798, 2567.8870, 200>>	//-60.5485>>
			LocationData.fPlaneHeading	=	266.9502
		BREAK
		
		CASE CRATE_DROP_LOCATION_3		//DAM
			NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - GET_CRATE_DROP_LOCATION_DATA - DAM") NET_NL()
			LocationData.vLocation = GET_CRATE_DROP_VECTOR(LocationData.iIdentifier)	//<<1915.9016, 563.9669, 174.4925>>
			LocationData.eAiSetup = CD_AI_RANDOM
			//Peds
			LocationData.Peds[0].vCreationPos		=	<<1892.2335, 592.2417, 189.3747>>
			LocationData.Peds[0].fHeading			=	249.6456
			LocationData.Peds[1].vCreationPos		=	<<1927.4362, 591.7188, 177.8152>>
			LocationData.Peds[1].fHeading			=	159.7268
			LocationData.Peds[2].vCreationPos		=	<<1906.4452, 577.1587, 174.8717>>
			LocationData.Peds[2].fHeading			=	198.8829
			LocationData.Peds[3].vCreationPos		=	<<1896.5363, 584.7686, 177.4033>>
			LocationData.Peds[3].fHeading			=	211.7178
			//Vehicle
			LocationData.Vehicle[0].vCreationPos[0]	=	<<1935.6045, 669.2496, 184.7174>>
			LocationData.Vehicle[0].fHeading[0]		=	224.7734
			LocationData.Vehicle[0].vCreationPos[1]	=	<<1865.4637, 478.8249, 170.5025>>
			LocationData.Vehicle[0].fHeading[1]		=	261.0713
			LocationData.Vehicle[0].vCreationPos[2]	=	<<1796.1036, 298.5035, 170.3443>>
			LocationData.Vehicle[0].fHeading[2]		=	337.5981
			//Plane Start
			LocationData.vPlaneStart	=	<<2961.0596, -102.1822, 483.7540>>
			LocationData.fPlaneHeading	=	67.1375
		BREAK
		
		CASE CRATE_DROP_LOCATION_4		//STABLES
			NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - GET_CRATE_DROP_LOCATION_DATA - STABLES") NET_NL()
			LocationData.vLocation = GET_CRATE_DROP_VECTOR(LocationData.iIdentifier)	//<<1458.3934, 1113.0664, 113.3340>>
			LocationData.eAiSetup = CD_AI_RANDOM
			//Peds
			LocationData.Peds[0].vCreationPos		=	<<1460.8676, 1088.2786, 113.3340>>
			LocationData.Peds[0].fHeading			=	10.3573
			LocationData.Peds[1].vCreationPos		=	<<1455.5161, 1133.9879, 113.3340>>
			LocationData.Peds[1].fHeading			=	186.5074 
			LocationData.Peds[2].vCreationPos		=	<<1482.7466, 1130.0945, 113.3340>>
			LocationData.Peds[2].fHeading			=	116.2890
			LocationData.Peds[3].vCreationPos		=	<<1464.9148, 1134.7462, 113.3225>>
			LocationData.Peds[3].fHeading			=	181.0186
			//Vehicle
			LocationData.Vehicle[0].vCreationPos[0]	=	<<1287.3431, 1210.8739, 106.6704>>
			LocationData.Vehicle[0].fHeading[0]		=	180.3493
			LocationData.Vehicle[0].vCreationPos[1]	=	<<1310.3473, 1073.4104, 104.5521>>
			LocationData.Vehicle[0].fHeading[1]		=	2.8196
			LocationData.Vehicle[0].vCreationPos[2]	=	<<1468.2540, 1176.5675, 113.3340>>
			LocationData.Vehicle[0].fHeading[2]		=	88.0180
			//Plane Start
			LocationData.vPlaneStart	=	<<2718.0764, 844.5446, 650.5612>>
			LocationData.fPlaneHeading	=	86.5865
		BREAK
		
		CASE CRATE_DROP_LOCATION_5		//VINEWOOD SIGN
			NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - GET_CRATE_DROP_LOCATION_DATA - VINEWOOD SIGN") NET_NL()
			LocationData.vLocation = GET_CRATE_DROP_VECTOR(LocationData.iIdentifier)	//<<817.5361, 1316.5696, 362.0491>>		//811.3602, 1308.3302, 361.9326>>
			LocationData.eAiSetup = CD_AI_RANDOM
			//Peds
			LocationData.Peds[0].vCreationPos		=	<<781.7131, 1296.3704, 360.3558>>
			LocationData.Peds[0].fHeading			=	298.7285
			LocationData.Peds[1].vCreationPos		=	<<817.8524, 1342.5442, 356.6494>>
			LocationData.Peds[1].fHeading			=	2.4516
			LocationData.Peds[2].vCreationPos		=	<<837.4887, 1296.4380, 363.3529>>
			LocationData.Peds[2].fHeading			=	235.0403
			LocationData.Peds[3].vCreationPos		=	<<779.8301, 1314.8702, 359.3835>>
			LocationData.Peds[3].fHeading			=	258.0066
			//Vehicle
			LocationData.Vehicle[0].vCreationPos[0]	=	<<842.7767, 1338.9281, 351.5400>>
			LocationData.Vehicle[0].fHeading[0]		=	219.8066
			LocationData.Vehicle[0].vCreationPos[1]	=	<<834.7274, 1247.0288, 352.0706>>
			LocationData.Vehicle[0].fHeading[1]		=	289.2027
			LocationData.Vehicle[0].vCreationPos[2]	=	<<675.3398, 1351.7742, 327.9539>>
			LocationData.Vehicle[0].fHeading[2]		=	256.7171
			//Plane Start
			LocationData.vPlaneStart	=	<<869.2994, 2807.8108, 770.8076 >> 	//-385.1198, 1419.0457, 748.2933>>
			LocationData.fPlaneHeading	=	178.8456							//273.6334
		BREAK
		
		CASE CRATE_DROP_LOCATION_6		//OIL FIELD
			NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - GET_CRATE_DROP_LOCATION_DATA - OIL FIELD") NET_NL()
			LocationData.vLocation = GET_CRATE_DROP_VECTOR(LocationData.iIdentifier)	//<<1402.8578, -2148.4177, 57.9675>>
			LocationData.eAiSetup = CD_AI_RANDOM
			//Peds
			LocationData.Peds[0].vCreationPos		=	<<1422.1013, -2184.8596, 61.8352>>
			LocationData.Peds[0].fHeading			=	30.6842
			LocationData.Peds[1].vCreationPos		=	<<1394.9160, -2087.5803, 53.4358>>
			LocationData.Peds[1].fHeading			=	201.2894
			LocationData.Peds[2].vCreationPos		=	<<1391.6300, -2205.3735, 60.4674>>
			LocationData.Peds[2].fHeading			=	18.6530
			LocationData.Peds[3].vCreationPos		=	<<1357.7871, -2191.3162, 58.6791>>
			LocationData.Peds[3].fHeading			=	108.6812
			//Vehicle
			LocationData.Vehicle[0].vCreationPos[0]	=	<<1347.6450, -2021.7520, 48.4312>>
			LocationData.Vehicle[0].fHeading[0]		=	298.7371
			LocationData.Vehicle[0].vCreationPos[1]	=	<<1504.5457, -1982.4602, 69.9607>>
			LocationData.Vehicle[0].fHeading[1]		=	75.8727
			LocationData.Vehicle[0].vCreationPos[2]	=	<<1524.3169, -2293.6086, 72.1833>>
			LocationData.Vehicle[0].fHeading[2]		=	30.4687
			//Plane Start
			LocationData.vPlaneStart	=	<<2283.1023, -1684.9219, 109.8941>>
			LocationData.fPlaneHeading	=	130.1306
		BREAK
		
		CASE CRATE_DROP_LOCATION_7		//DOCKS CONTAINERS
			NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - GET_CRATE_DROP_LOCATION_DATA - DOCKS CONTAINERS") NET_NL()
			LocationData.vLocation = GET_CRATE_DROP_VECTOR(LocationData.iIdentifier)	//<<1104.8152, -3142.3347, 4.9010>>
			LocationData.eAiSetup = CD_AI_RANDOM
			//Peds
			LocationData.Peds[0].vCreationPos		=	<<1139.8286, -3134.4072, 15.0016>>
			LocationData.Peds[0].fHeading			=	104.6551
			LocationData.Peds[1].vCreationPos		=	<<1139.6536, -3149.9861, 22.1254>>
			LocationData.Peds[1].fHeading			=	86.9869
			LocationData.Peds[2].vCreationPos		=	<<1083.7417, -3163.0815, 4.9010>>
			LocationData.Peds[2].fHeading			=	312.9454
			LocationData.Peds[3].vCreationPos		=	<<1106.5582, -3120.7446, 4.9010>>
			LocationData.Peds[3].fHeading			=	186.7521
			//Vehicle
			LocationData.Vehicle[0].vCreationPos[0]	=	<<983.3778, -3091.9031, 4.9009>>
			LocationData.Vehicle[0].fHeading[0]		=	181.5385
			LocationData.Vehicle[0].vCreationPos[1]	=	<<1241.7163, -3095.0974, 4.9016>>
			LocationData.Vehicle[0].fHeading[1]		=	88.1780
			LocationData.Vehicle[0].vCreationPos[2]	=	<<1020.3882, -3218.3372, 4.8880>>
			LocationData.Vehicle[0].fHeading[2]		=	270.4624
			//Plane Start
			LocationData.vPlaneStart	=	<<1082.7615, -3913.5383, 259.5345>>
			LocationData.fPlaneHeading	=	357.1163
		BREAK
		
		CASE CRATE_DROP_LOCATION_8		//DOCKS SMALL
			NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - GET_CRATE_DROP_LOCATION_DATA - DOCKS SMALL") NET_NL()
			LocationData.vLocation = GET_CRATE_DROP_VECTOR(LocationData.iIdentifier)	//<<545.7347, -3025.3271, 5.0591>>
			LocationData.eAiSetup = CD_AI_RANDOM
			//Peds
			LocationData.Peds[0].vCreationPos		=	<<550.9495, -3046.5769, 12.2886>>
			LocationData.Peds[0].fHeading			=	8.8327
			LocationData.Peds[1].vCreationPos		=	<<542.4202, -3002.0791, 5.0443>>
			LocationData.Peds[1].fHeading			=	197.5590
			LocationData.Peds[2].vCreationPos		=	<<567.1848, -3005.1763, 22.0949>>
			LocationData.Peds[2].fHeading			=	120.1645
			LocationData.Peds[3].vCreationPos		=	<<520.8234, -3053.3242, 5.0696>>
			LocationData.Peds[3].fHeading			=	0.3800
			//Vehicle
			LocationData.Vehicle[0].vCreationPos[0]	=	<<656.7521, -2960.7344, 5.0444>>
			LocationData.Vehicle[0].fHeading[0]		=	158.5572
			LocationData.Vehicle[0].vCreationPos[1]	=	<<586.7590, -2960.0942, 5.0444>>
			LocationData.Vehicle[0].fHeading[1]		=	182.1605
			LocationData.Vehicle[0].vCreationPos[2]	=	<<524.9021, -2962.5217, 5.0453>>
			LocationData.Vehicle[0].fHeading[2]		=	175.0801
			//Plane Start
			LocationData.vPlaneStart	=	<<-378.6478, -3677.8054, 291.4673>>
			LocationData.fPlaneHeading	=	314.9697
		BREAK
		
		CASE CRATE_DROP_LOCATION_9		//NEW ESTATE DEADEND
			LocationData.vLocation = GET_CRATE_DROP_VECTOR(LocationData.iIdentifier)	//<<1376.0369, -740.2166, 66.2331>>
			LocationData.eAiSetup = CD_AI_RANDOM
			//Peds
			LocationData.Peds[0].vCreationPos		=	<<1409.3347, -734.6124, 66.6396>>
			LocationData.Peds[0].fHeading			=	93.3841
			LocationData.Peds[1].vCreationPos		=	<<1395.7267, -701.6596, 66.4683>>
			LocationData.Peds[1].fHeading			=	140.4233
			LocationData.Peds[2].vCreationPos		=	<<1383.7318, -776.3151, 66.4024>>
			LocationData.Peds[2].fHeading			=	11.4037
			LocationData.Peds[3].vCreationPos		=	<<1361.3036, -697.9785, 65.3824>>
			LocationData.Peds[3].fHeading			=	208.0160
			//Vehicle
			LocationData.Vehicle[0].vCreationPos[0]	=	<<1237.5015, -761.1631, 58.9825>>
			LocationData.Vehicle[0].fHeading[0]		=	287.8288
			LocationData.Vehicle[0].vCreationPos[1]	=	<<1268.0238, -615.4786, 67.8305>>
			LocationData.Vehicle[0].fHeading[1]		=	208.6499
			LocationData.Vehicle[0].vCreationPos[2]	=	<<1245.5123, -711.1078, 61.9082>>
			LocationData.Vehicle[0].fHeading[2]		=	180.0621
			//Plane Start
			LocationData.vPlaneStart	=	<<2409.2908, -1098.3512, 496.8289>>
			LocationData.fPlaneHeading	=	70.7044
		BREAK
		
		CASE CRATE_DROP_LOCATION_10		//HORSE RACING COURSE
			LocationData.vLocation = GET_CRATE_DROP_VECTOR(LocationData.iIdentifier)	//<<1147.1620, 126.3452, 80.8693>>
			LocationData.eAiSetup = CD_AI_RANDOM
			//Peds
			LocationData.Peds[0].vCreationPos		=	<<1103.4020, 89.1884, 79.8909>>
			LocationData.Peds[0].fHeading			=	323.5250
			LocationData.Peds[1].vCreationPos		=	<<1193.0503, 149.3631, 79.5353>>
			LocationData.Peds[1].fHeading			=	115.8848
			LocationData.Peds[2].vCreationPos		=	<<1144.8881, 82.5526, 79.8909>>
			LocationData.Peds[2].fHeading			=	345.2191
			LocationData.Peds[3].vCreationPos		=	<<1106.9523, 128.1926, 80.8914>>
			LocationData.Peds[3].fHeading			=	255.0090
			//Vehicle
			LocationData.Vehicle[0].vCreationPos[0]	=	<<1065.0677, 241.1210, 79.8555>>
			LocationData.Vehicle[0].fHeading[0]		=	326.5492
			LocationData.Vehicle[0].vCreationPos[1]	=	<<980.4287, -68.1060, 73.9594>>
			LocationData.Vehicle[0].fHeading[1]		=	297.0119
			LocationData.Vehicle[0].vCreationPos[2]	=	<<967.2383, 184.9293, 79.8308>>
			LocationData.Vehicle[0].fHeading[2]		=	261.3374
			//Plane Start
			LocationData.vPlaneStart	=	<<1742.1299, 1556.5276, 326.8489>>
			LocationData.fPlaneHeading	=	158.5592
		BREAK
		
		CASE CRATE_DROP_LOCATION_11		//SMALL DESERT HOUSE
			LocationData.vLocation = GET_CRATE_DROP_VECTOR(LocationData.iIdentifier)	//<<1671.2318, 3041.1763, 53.0351>>
			LocationData.eAiSetup = CD_AI_RANDOM
			//Peds
			LocationData.Peds[0].vCreationPos		=	<<1725.8370, 3028.0696, 61.2235>>
			LocationData.Peds[0].fHeading			=	67.0086
			LocationData.Peds[1].vCreationPos		=	<<1609.8815, 3047.9028, 46.7991>>
			LocationData.Peds[1].fHeading			=	263.2002
			LocationData.Peds[2].vCreationPos		=	<<1674.1287, 3113.6899, 43.6611>>
			LocationData.Peds[2].fHeading			=	30.7465
			LocationData.Peds[3].vCreationPos		=	<<1731.5975, 3019.9277, 61.4284>>
			LocationData.Peds[3].fHeading			=	157.5899
			//Vehicle
			LocationData.Vehicle[0].vCreationPos[0]	=	<<1741.0596, 3156.4600, 42.1906>>
			LocationData.Vehicle[0].fHeading[0]		=	110.1921
			LocationData.Vehicle[0].vCreationPos[1]	=	<<1601.5193, 2890.4231, 55.9619>>
			LocationData.Vehicle[0].fHeading[1]		=	318.1643
			LocationData.Vehicle[0].vCreationPos[2]	=	<<1571.9320, 3092.0444, 39.8558>>
			LocationData.Vehicle[0].fHeading[2]		=	297.0444
			//Plane Start
			LocationData.vPlaneStart	=	<<768.1101, 4080.2109, 438.6512>>
			LocationData.fPlaneHeading	=	199.9345
		BREAK
		
		CASE CRATE_DROP_LOCATION_12		//NORTH OF BIG LAKE
			LocationData.vLocation = GET_CRATE_DROP_VECTOR(LocationData.iIdentifier)	//<<1015.5989, 4350.6685, 41.4840>>
			LocationData.eAiSetup = CD_AI_RANDOM
			//Peds
			LocationData.Peds[0].vCreationPos		=	<<1002.5171, 4324.6968, 39.1856>>
			LocationData.Peds[0].fHeading			=	3.4408
			LocationData.Peds[1].vCreationPos		=	<<1019.2786, 4405.1626, 42.1815>>
			LocationData.Peds[1].fHeading			=	73.8213
			LocationData.Peds[2].vCreationPos		=	<<995.6335, 4394.1333, 47.8651>>
			LocationData.Peds[2].fHeading			=	355.3108
			LocationData.Peds[3].vCreationPos		=	<<972.6747, 4352.7310, 43.7555>>
			LocationData.Peds[3].fHeading			=	282.5687
			//Vehicle
			LocationData.Vehicle[0].vCreationPos[0]	=	<<1125.1548, 4429.3887, 62.2158>>
			LocationData.Vehicle[0].fHeading[0]		=	82.7581
			LocationData.Vehicle[0].vCreationPos[1]	=	<<885.6168, 4459.2446, 50.8314>>
			LocationData.Vehicle[0].fHeading[1]		=	239.2189
			LocationData.Vehicle[0].vCreationPos[2]	=	<<993.9852, 4460.1172, 49.8088>>
			LocationData.Vehicle[0].fHeading[2]		=	260.9623
			//Plane Start
			LocationData.vPlaneStart	=	<<-865.8170, 4403.5776, 604.2233>>
			LocationData.fPlaneHeading	=	268.7136
		BREAK
		
		CASE CRATE_DROP_LOCATION_13		//SECLUDED BEACH - NORTH WEST
			LocationData.vLocation = GET_CRATE_DROP_VECTOR(LocationData.iIdentifier)	//<<-1731.0601, 4959.4839, 3.8134>>
			LocationData.eAiSetup = CD_AI_RANDOM
			//Peds
			LocationData.Peds[0].vCreationPos		=	<<-1714.5840, 4954.1328, 7.2482>>
			LocationData.Peds[0].fHeading			=	116.3591
			LocationData.Peds[1].vCreationPos		=	<<-1689.0546, 4954.6411, 10.6516>>
			LocationData.Peds[1].fHeading			=	102.0189
			LocationData.Peds[2].vCreationPos		=	<<-1714.6119, 4982.8916, 7.9491>>
			LocationData.Peds[2].fHeading			=	148.2065
			LocationData.Peds[3].vCreationPos		=	<<-1733.6293, 4983.3550, 5.2886>>
			LocationData.Peds[3].fHeading			=	172.3626
			//Vehicle
			LocationData.Vehicle[0].vCreationPos[0]	=	<<-1850.0844, 4774.3184, 3.2752>>
			LocationData.Vehicle[0].fHeading[0]		=	22.5589
			LocationData.Vehicle[0].vCreationPos[1]	=	<<-1582.0511, 5057.1255, 33.7903>>
			LocationData.Vehicle[0].fHeading[1]		=	139.2498
			LocationData.Vehicle[0].vCreationPos[2]	=	<<-1555.5548, 4921.6802, 60.3630>>
			LocationData.Vehicle[0].fHeading[2]		=	307.6337
			//Plane Start
			LocationData.vPlaneStart	=	<<-688.8508, 7607.5288, 349.1056>>
			LocationData.fPlaneHeading	=	172.9232
		BREAK
		
		CASE CRATE_DROP_LOCATION_14		//BEACH - NEAR MILITARY BASE
			LocationData.vLocation = GET_CRATE_DROP_VECTOR(LocationData.iIdentifier)	//<<-2436.2498, 4181.9131, 7.7719>>
			LocationData.eAiSetup = CD_AI_RANDOM
			//Peds
			LocationData.Peds[0].vCreationPos		=	<<-2405.7019, 4210.7070, 10.5095>>
			LocationData.Peds[0].fHeading			=	146.9088
			LocationData.Peds[1].vCreationPos		=	<<-2382.9556, 4205.8691, 21.6201>>
			LocationData.Peds[1].fHeading			=	121.7438
			LocationData.Peds[2].vCreationPos		=	<<-2407.6528, 4137.3716, 17.5630>>
			LocationData.Peds[2].fHeading			=	35.8796
			LocationData.Peds[3].vCreationPos		=	<<-2375.4534, 4184.9668, 22.8146>>
			LocationData.Peds[3].fHeading			=	93.1919
			//Vehicle
			LocationData.Vehicle[0].vCreationPos[0]	=	<<-2309.1948, 4291.0698, 30.5234>>
			LocationData.Vehicle[0].fHeading[0]		=	139.4109
			LocationData.Vehicle[0].vCreationPos[1]	=	<<-2479.6760, 4007.8201, 5.8680>>
			LocationData.Vehicle[0].fHeading[1]		=	345.5383
			LocationData.Vehicle[0].vCreationPos[2]	=	<<-2368.0415, 4077.4194, 30.5281>>
			LocationData.Vehicle[0].fHeading[2]		=	326.4518
			//Plane Start
			LocationData.vPlaneStart	=	<<-3820.0557, 4296.7773, 220.0733>>
			LocationData.fPlaneHeading	=	266.5256
		BREAK
		
		CASE CRATE_DROP_LOCATION_15		//BEACH - NEAR HOUSES - WEST
			LocationData.vLocation = GET_CRATE_DROP_VECTOR(LocationData.iIdentifier)	//<<-3163.3643, 756.8276, 2.1183>>
			LocationData.eAiSetup = CD_AI_RANDOM
			//Peds
			LocationData.Peds[0].vCreationPos		=	<<-3131.2861, 753.4323, 5.3342>>
			LocationData.Peds[0].fHeading			=	94.0004
			LocationData.Peds[1].vCreationPos		=	<<-3177.3867, 790.2584, 3.0064>>
			LocationData.Peds[1].fHeading			=	172.8542
			LocationData.Peds[2].vCreationPos		=	<<-3183.7268, 787.8382, 2.8265>>
			LocationData.Peds[2].fHeading			=	203.5908
			LocationData.Peds[3].vCreationPos		=	<<-3140.7498, 724.6806, 1.2483>>
			LocationData.Peds[3].fHeading			=	56.6538
			//Vehicle
			LocationData.Vehicle[0].vCreationPos[0]	=	<<-3116.0188, 647.4692, 1.1468>>
			LocationData.Vehicle[0].fHeading[0]		=	41.7007
			LocationData.Vehicle[0].vCreationPos[1]	=	<<-3235.4324, 892.0785, 2.3308>>
			LocationData.Vehicle[0].fHeading[1]		=	169.4398
			LocationData.Vehicle[0].vCreationPos[2]	=	<<-3170.8979, 944.9553, 13.5189>>
			LocationData.Vehicle[0].fHeading[2]		=	181.8572
			//Plane Start
			LocationData.vPlaneStart	=	<<-3996.0969, 1914.2437, 219.9833>>
			LocationData.fPlaneHeading	=	232.6708
		BREAK
		
		CASE CRATE_DROP_LOCATION_16		//BEACH - NEAR FUNFAIR PIER
			LocationData.vLocation = GET_CRATE_DROP_VECTOR(LocationData.iIdentifier)	//<<-1731.0846, -989.8386, 4.4152>>
			LocationData.eAiSetup = CD_AI_RANDOM
			//Peds
			LocationData.Peds[0].vCreationPos		=	<<-1744.4012, -966.3932, 6.4026>>
			LocationData.Peds[0].fHeading			=	300.0445
			LocationData.Peds[1].vCreationPos		=	<<-1721.4326, -1013.8647, 4.2361>>
			LocationData.Peds[1].fHeading			=	124.3674
			LocationData.Peds[2].vCreationPos		=	<<-1710.1141, -999.4743, 5.1064>>
			LocationData.Peds[2].fHeading			=	60.6101
			LocationData.Peds[3].vCreationPos		=	<<-1673.7023, -992.2216, 6.3501>>
			LocationData.Peds[3].fHeading			=	53.7375
			//Vehicle
			LocationData.Vehicle[0].vCreationPos[0]	=	<<-1708.4484, -868.3484, 7.4161>>
			LocationData.Vehicle[0].fHeading[0]		=	144.7090
			LocationData.Vehicle[0].vCreationPos[1]	=	<<-1683.4683, -1047.0608, 3.2051>>
			LocationData.Vehicle[0].fHeading[1]		=	55.3434
			LocationData.Vehicle[0].vCreationPos[2]	=	<<-1619.0638, -905.6519, 7.9512>>
			LocationData.Vehicle[0].fHeading[2]		=	141.2538
			//Plane Start
			LocationData.vPlaneStart	=	<<-1902.6805, -1889.5682, 114.6166>>
			LocationData.fPlaneHeading	=	358.7342
		BREAK
		
		CASE CRATE_DROP_LOCATION_17		//SMALL PARK - NORTH OF CITY
			LocationData.vLocation = GET_CRATE_DROP_VECTOR(LocationData.iIdentifier)	//<<-818.7764, 865.3253, 201.9869>>
			LocationData.eAiSetup = CD_AI_RANDOM
			//Peds
			LocationData.Peds[0].vCreationPos		=	<<-813.9835, 835.9732, 201.4461>>
			LocationData.Peds[0].fHeading			=	191.4548
			LocationData.Peds[1].vCreationPos		=	<<-799.1475, 838.1729, 203.5733>>
			LocationData.Peds[1].fHeading			=	198.4863
			LocationData.Peds[2].vCreationPos		=	<<-833.4322, 897.7642, 212.0651>>
			LocationData.Peds[2].fHeading			=	199.7569
			LocationData.Peds[3].vCreationPos		=	<<-798.2678, 886.1758, 202.1105>>
			LocationData.Peds[3].fHeading			=	143.6434
			//Vehicle
			LocationData.Vehicle[0].vCreationPos[0]	=	<<-739.9070, 822.6394, 213.0717>>
			LocationData.Vehicle[0].fHeading[0]		=	38.4355
			LocationData.Vehicle[0].vCreationPos[1]	=	<<-869.4866, 797.1853, 189.8626>>
			LocationData.Vehicle[0].fHeading[1]		=	294.5416
			LocationData.Vehicle[0].vCreationPos[2]	=	<<-707.6155, 897.8837, 228.5368>>
			LocationData.Vehicle[0].fHeading[2]		=	178.5833
			//Plane Start
			LocationData.vPlaneStart	=	<<-2203.2844, 1580.0496, 569.6727>>
			LocationData.fPlaneHeading	=	228.5565
		BREAK
		
		CASE CRATE_DROP_LOCATION_18		//OBSERVATORY CAR PARK
			LocationData.vLocation = GET_CRATE_DROP_VECTOR(LocationData.iIdentifier)	//<<-401.2717, 1211.6768, 324.9297>>
			LocationData.eAiSetup = CD_AI_RANDOM
			//Peds
			LocationData.Peds[0].vCreationPos		=	<<-423.9036, 1226.6632, 324.7588>>
			LocationData.Peds[0].fHeading			=	232.6687
			LocationData.Peds[1].vCreationPos		=	<<-382.1511, 1228.8767, 324.7606>>
			LocationData.Peds[1].fHeading			=	156.8766
			LocationData.Peds[2].vCreationPos		=	<<-385.6131, 1187.1063, 324.7588>>
			LocationData.Peds[2].fHeading			=	154.8572
			LocationData.Peds[3].vCreationPos		=	<<-429.2660, 1214.5746, 324.7588>>
			LocationData.Peds[3].fHeading			=	261.6925
			//Vehicle
			LocationData.Vehicle[0].vCreationPos[0]	=	<<-509.5673, 1228.7190, 320.3937>>
			LocationData.Vehicle[0].fHeading[0]		=	165.9902
			LocationData.Vehicle[0].vCreationPos[1]	=	<<-321.2862, 1201.1703, 320.5964>>
			LocationData.Vehicle[0].fHeading[1]		=	132.4112
			LocationData.Vehicle[0].vCreationPos[2]	=	<<-345.4358, 1303.3269, 335.4418>>
			LocationData.Vehicle[0].fHeading[2]		=	178.3955
			//Plane Start
			LocationData.vPlaneStart	=	<<-1468.0973, 2146.7556, 764.3748>>
			LocationData.fPlaneHeading	=	233.1176
		BREAK
		
		CASE CRATE_DROP_LOCATION_19		//STORM DRAIN
			LocationData.vLocation = GET_CRATE_DROP_VECTOR(LocationData.iIdentifier)	//<<728.2132, -1532.4799, 18.7348>>
			LocationData.eAiSetup = CD_AI_RANDOM
			//Peds
			LocationData.Peds[0].vCreationPos		=	<<721.5088, -1510.8307, 18.6862>>
			LocationData.Peds[0].fHeading			=	196.9149
			LocationData.Peds[1].vCreationPos		=	<<753.9851, -1520.3639, 19.5089>>
			LocationData.Peds[1].fHeading			=	100.5212
			LocationData.Peds[2].vCreationPos		=	<<703.6952, -1539.9088, 8.7089>>
			LocationData.Peds[2].fHeading			=	114.8652
			LocationData.Peds[3].vCreationPos		=	<<733.1053, -1535.6870, 18.6470>>
			LocationData.Peds[3].fHeading			=	173.8219
			//Vehicle
			LocationData.Vehicle[0].vCreationPos[0]	=	<<667.9203, -1410.7107, 8.7295>>
			LocationData.Vehicle[0].fHeading[0]		=	196.4158
			LocationData.Vehicle[0].vCreationPos[1]	=	<<787.4294, -1396.0847, 26.0718>>
			LocationData.Vehicle[0].fHeading[1]		=	179.2513
			LocationData.Vehicle[0].vCreationPos[2]	=	<<653.0773, -1731.3872, 8.6921>>
			LocationData.Vehicle[0].fHeading[2]		=	354.5085
			//Plane Start
			LocationData.vPlaneStart	=	<<2206.5686, -3593.5542, 346.6880>>
			LocationData.fPlaneHeading	=	50.9998
		BREAK
		
		CASE CRATE_DROP_LOCATION_20		//WAREHOUSE
			LocationData.vLocation = GET_CRATE_DROP_VECTOR(LocationData.iIdentifier)	//<<1140.9230, -1285.6388, 33.6091>>
			LocationData.eAiSetup = CD_AI_RANDOM
			//Peds
			LocationData.Peds[0].vCreationPos		=	<<1148.5785, -1295.3044, 33.6676>>
			LocationData.Peds[0].fHeading			=	178.5031
			LocationData.Peds[1].vCreationPos		=	<<1124.7330, -1303.8645, 33.7184>>
			LocationData.Peds[1].fHeading			=	281.0662
			LocationData.Peds[2].vCreationPos		=	<<1149.9839, -1280.6949, 33.6471>>
			LocationData.Peds[2].fHeading			=	356.4931
			LocationData.Peds[3].vCreationPos		=	<<1155.6708, -1277.5096, 39.4394>>
			LocationData.Peds[3].fHeading			=	134.7168
			//Vehicle
			LocationData.Vehicle[0].vCreationPos[0]	=	<<1175.2524, -1284.3097, 33.7757>>
			LocationData.Vehicle[0].fHeading[0]		=	179.5388
			LocationData.Vehicle[0].vCreationPos[1]	=	<<1179.9785, -1373.4880, 33.8700>>
			LocationData.Vehicle[0].fHeading[1]		=	75.5108
			LocationData.Vehicle[0].vCreationPos[2]	=	<<1251.8861, -1335.8915, 34.2108>>
			LocationData.Vehicle[0].fHeading[2]		=	351.2707
			//Plane Start
			LocationData.vPlaneStart	=	<<2534.7761, -1420.1954, 451.8415>>
			LocationData.fPlaneHeading	=	83.0500
		BREAK
		
		CASE CRATE_DROP_LOCATION_21		//GREEN - OCEAN ARCH
			LocationData.vLocation = GET_CRATE_DROP_VECTOR(LocationData.iIdentifier)	//<<2909.1543, 770.3181, 21.1684>>
			LocationData.eAiSetup = CD_AI_RANDOM
			//Peds
			LocationData.Peds[0].vCreationPos		=	<<2932.4224, 790.1230, 24.9383>>
			LocationData.Peds[0].fHeading			=	108.1342
			LocationData.Peds[1].vCreationPos		=	<<2927.7583, 769.4883, 23.6032>>
			LocationData.Peds[1].fHeading			=	105.6544
			LocationData.Peds[2].vCreationPos		=	<<2880.0779, 715.3883, 30.4501>>
			LocationData.Peds[2].fHeading			=	341.9437
			LocationData.Peds[3].vCreationPos		=	<<2896.4841, 764.3184, 18.5266>>
			LocationData.Peds[3].fHeading			=	326.4059
			//Vehicle
			LocationData.Vehicle[0].vCreationPos[0]	=	<<2636.0046, 543.1351, 94.6250>>
			LocationData.Vehicle[0].fHeading[0]		=	0.7481
			LocationData.Vehicle[0].vCreationPos[1]	=	<<2619.1531, 639.2633, 93.4987>>
			LocationData.Vehicle[0].fHeading[1]		=	18.6567
			LocationData.Vehicle[0].vCreationPos[2]	=	<<2554.6047, 405.6805, 107.4526>>
			LocationData.Vehicle[0].fHeading[2]		=	312.7786
			//Plane Start
			LocationData.vPlaneStart	=	<<3882.8372, 1694.2062, 365.5233>>
			LocationData.fPlaneHeading	=	145.8588
		BREAK
		
		CASE CRATE_DROP_LOCATION_22		//QUARRY
			LocationData.vLocation = GET_CRATE_DROP_VECTOR(LocationData.iIdentifier)	//<<2778.9092, 2853.2659, 34.7828>>
			LocationData.eAiSetup = CD_AI_RANDOM
			//Peds
			LocationData.Peds[0].vCreationPos		=	<<2790.8098, 2842.2656, 35.1792>>
			LocationData.Peds[0].fHeading			=	41.1631
			LocationData.Peds[1].vCreationPos		=	<<2796.6125, 2874.4026, 39.1640>>
			LocationData.Peds[1].fHeading			=	131.3994
			LocationData.Peds[2].vCreationPos		=	<<2778.4944, 2831.8777, 39.7195>>
			LocationData.Peds[2].fHeading			=	35.0007 
			LocationData.Peds[3].vCreationPos		=	<<2756.6267, 2855.2629, 36.8614>>
			LocationData.Peds[3].fHeading			=	279.8899
			//Vehicle
			LocationData.Vehicle[0].vCreationPos[0]	=	<<2664.1719, 2850.1699, 38.4106>>
			LocationData.Vehicle[0].fHeading[0]		=	252.0351
			LocationData.Vehicle[0].vCreationPos[1]	=	<<2619.7351, 2726.4878, 40.1266>>
			LocationData.Vehicle[0].fHeading[1]		=	286.5096
			LocationData.Vehicle[0].vCreationPos[2]	=	<<2642.5693, 2948.2598, 37.3319>>
			LocationData.Vehicle[0].fHeading[2]		=	230.3495
			//Plane Start
			LocationData.vPlaneStart	=	<<3896.2703, 2191.8853, 350.8098>>
			LocationData.fPlaneHeading	=	74.6998
		BREAK
		
		CASE CRATE_DROP_LOCATION_23		//ALIEN HIPPY
			LocationData.vLocation = GET_CRATE_DROP_VECTOR(LocationData.iIdentifier)	//<<2463.1421, 3769.6511, 40.3280>>
			LocationData.eAiSetup = CD_AI_RANDOM
			//Peds
			LocationData.Peds[0].vCreationPos		=	<<2466.8137, 3784.9656, 39.9763>>
			LocationData.Peds[0].fHeading			=	150.9192
			LocationData.Peds[1].vCreationPos		=	<<2491.5894, 3769.7510, 43.3539>>
			LocationData.Peds[1].fHeading			=	88.8265
			LocationData.Peds[2].vCreationPos		=	<<2477.9780, 3761.7510, 40.9253>>
			LocationData.Peds[2].fHeading			=	24.5861
			LocationData.Peds[3].vCreationPos		=	<<2450.1968, 3755.6726, 40.9738>>
			LocationData.Peds[3].fHeading			=	67.7862
			//Vehicle
			LocationData.Vehicle[0].vCreationPos[0]	=	<<2310.0176, 3748.4189, 36.9374>>			//2336.1221, 3804.9868, 33.7311>>
			LocationData.Vehicle[0].fHeading[0]		=	311.6797									//208.1995
			LocationData.Vehicle[0].vCreationPos[1]	=	<<2429.7969, 3874.4253, 36.0097>>
			LocationData.Vehicle[0].fHeading[1]		=	142.6340
			LocationData.Vehicle[0].vCreationPos[2]	=	<<2301.9558, 3735.4561, 36.7863>>
			LocationData.Vehicle[0].fHeading[2]		=	339.1366
			//Plane Start
			LocationData.vPlaneStart	=	<<1528.4980, 5012.7031, 555.9046>>
			LocationData.fPlaneHeading	=	223.5092
		BREAK
		
		CASE CRATE_DROP_LOCATION_24		//FARM - FAR NORTH
			LocationData.vLocation = GET_CRATE_DROP_VECTOR(LocationData.iIdentifier)	//<<1888.6101, 4626.8149, 37.4665>>
			LocationData.eAiSetup = CD_AI_RANDOM
			//Peds
			LocationData.Peds[0].vCreationPos		=	<<1917.1881, 4609.0024, 37.6286>>
			LocationData.Peds[0].fHeading			=	154.3076
			LocationData.Peds[1].vCreationPos		=	<<1932.6865, 4649.7534, 39.4995>>
			LocationData.Peds[1].fHeading			=	244.4298
			LocationData.Peds[2].vCreationPos		=	<<1876.0381, 4609.2046, 36.2622>>
			LocationData.Peds[2].fHeading			=	180.3719
			LocationData.Peds[3].vCreationPos		=	<<1927.8698, 4635.5054, 39.4275>>
			LocationData.Peds[3].fHeading			=	88.5296
			//Vehicle
			LocationData.Vehicle[0].vCreationPos[0]	=	<<1970.5175, 4653.9937, 39.8374>>
			LocationData.Vehicle[0].fHeading[0]		=	148.0159
			LocationData.Vehicle[0].vCreationPos[1]	=	<<1797.5081, 4585.4668, 36.0648>>
			LocationData.Vehicle[0].fHeading[1]		=	186.4325
			LocationData.Vehicle[0].vCreationPos[2]	=	<<1990.0415, 4601.6006, 39.5506>>
			LocationData.Vehicle[0].fHeading[2]		=	117.1525
			//Plane Start
			LocationData.vPlaneStart	=	<<3711.5488, 4878.3599, 404.1502>>
			LocationData.fPlaneHeading	=	101.3695
		BREAK
		
		CASE CRATE_DROP_LOCATION_25		//OIL PUMPS - COUNTRY
			LocationData.vLocation = GET_CRATE_DROP_VECTOR(LocationData.iIdentifier)	//<<585.3987, 2893.7783, 38.5297>>
			LocationData.eAiSetup = CD_AI_RANDOM
			//Peds
			LocationData.Peds[0].vCreationPos		=	<<574.2339, 2924.3218, 39.7349>>
			LocationData.Peds[0].fHeading			=	213.2177
			LocationData.Peds[1].vCreationPos		=	<<541.8079, 2870.4058, 42.1283>>
			LocationData.Peds[1].fHeading			=	288.7304
			LocationData.Peds[2].vCreationPos		=	<<625.1206, 2877.8542, 38.1618>>
			LocationData.Peds[2].fHeading			=	87.2171
			LocationData.Peds[3].vCreationPos		=	<<598.0822, 2930.7744, 39.9140>>
			LocationData.Peds[3].fHeading			=	321.3549
			//Vehicle
			LocationData.Vehicle[0].vCreationPos[0]	=	<<591.6580, 2974.4509, 39.7528>>
			LocationData.Vehicle[0].fHeading[0]		=	258.7779
			LocationData.Vehicle[0].vCreationPos[1]	=	<<521.3518, 2887.9089, 42.3507>>
			LocationData.Vehicle[0].fHeading[1]		=	194.5046
			LocationData.Vehicle[0].vCreationPos[2]	=	<<733.1032, 2908.2554, 46.4786>>
			LocationData.Vehicle[0].fHeading[2]		=	27.0771
			//Plane Start
			LocationData.vPlaneStart	=	<<-1071.6088, 2496.1101, 332.6916>>
			LocationData.fPlaneHeading	=	288.7392
		BREAK
		
		CASE CRATE_DROP_LOCATION_26		//BEACH RUINS
			LocationData.vLocation = GET_CRATE_DROP_VECTOR(LocationData.iIdentifier)	//<<2811.4529, -668.6710, 1.5810>>
			LocationData.eAiSetup = CD_AI_RANDOM
			//Peds
			LocationData.Peds[0].vCreationPos		=	<<2805.8994, -684.5618, 2.7772>>
			LocationData.Peds[0].fHeading			=	356.7281
			LocationData.Peds[1].vCreationPos		=	<<2792.4854, -650.1158, 3.6368>>
			LocationData.Peds[1].fHeading			=	212.2862
			LocationData.Peds[2].vCreationPos		=	<<2815.2312, -668.5212, 0.2468>>
			LocationData.Peds[2].fHeading			=	274.1470
			LocationData.Peds[3].vCreationPos		=	<<2824.9724, -694.6227, 0.5617>>
			LocationData.Peds[3].fHeading			=	106.0981
			//Vehicle
			LocationData.Vehicle[0].vCreationPos[0]	=	<<2733.1230, -774.2815, 22.6375>>
			LocationData.Vehicle[0].fHeading[0]		=	7.3256
			LocationData.Vehicle[0].vCreationPos[1]	=	<<2659.8125, -781.9880, 34.7736>>
			LocationData.Vehicle[0].fHeading[1]		=	191.9212
			LocationData.Vehicle[0].vCreationPos[2]	=	<<2669.1050, -686.0164, 39.8963>>
			LocationData.Vehicle[0].fHeading[2]		=	207.9260
			//Plane Start
			LocationData.vPlaneStart	=	<<3835.9045, -1895.4310, 621.9581>>
			LocationData.fPlaneHeading	=	48.6187
		BREAK
		
		CASE CRATE_DROP_LOCATION_27		//GRAVEYARD
			LocationData.vLocation = GET_CRATE_DROP_VECTOR(LocationData.iIdentifier)	//<<-1729.3708, -193.2892, 57.4816>>
			LocationData.eAiSetup = CD_AI_RANDOM
			//Peds
			LocationData.Peds[0].vCreationPos		=	<<-1725.3507, -189.9190, 57.5190>>
			LocationData.Peds[0].fHeading			=	103.1486
			LocationData.Peds[1].vCreationPos		=	<<-1746.5844, -206.3550, 56.4266>>
			LocationData.Peds[1].fHeading			=	132.9578
			LocationData.Peds[2].vCreationPos		=	<<-1676.4740, -199.4280, 56.6720>>
			LocationData.Peds[2].fHeading			=	249.4169
			LocationData.Peds[3].vCreationPos		=	<<-1681.5916, -165.3771, 56.7343>>
			LocationData.Peds[3].fHeading			=	292.0603
			//Vehicle
			LocationData.Vehicle[0].vCreationPos[0]	=	<<-1660.8077, -131.8831, 58.8850>>
			LocationData.Vehicle[0].fHeading[0]		=	92.4984
			LocationData.Vehicle[0].vCreationPos[1]	=	<<-1787.2028, -265.7223, 44.2972>>
			LocationData.Vehicle[0].fHeading[1]		=	231.4039
			LocationData.Vehicle[0].vCreationPos[2]	=	<<-1603.2820, -201.1913, 53.8901>>
			LocationData.Vehicle[0].fHeading[2]		=	143.5364
			//Plane Start
			LocationData.vPlaneStart	=	<<-2497.7766, -1044.2947, 247.0226>>
			LocationData.fPlaneHeading	=	326.3715
		BREAK
		
		CASE CRATE_DROP_LOCATION_28		//RIVER - COVERED BRIDGE
			LocationData.vLocation = GET_CRATE_DROP_VECTOR(LocationData.iIdentifier)	//<<-498.3305, 3006.9063, 27.4341>>
			LocationData.eAiSetup = CD_AI_RANDOM
			//Peds
			LocationData.Peds[0].vCreationPos		=	<<-519.8322, 3036.5803, 34.8224>>
			LocationData.Peds[0].fHeading			=	209.6163
			LocationData.Peds[1].vCreationPos		=	<<-459.0498, 2991.1235, 25.7687>>
			LocationData.Peds[1].fHeading			=	78.4390
			LocationData.Peds[2].vCreationPos		=	<<-531.2136, 2980.9250, 25.1800>>
			LocationData.Peds[2].fHeading			=	168.7811
			LocationData.Peds[3].vCreationPos		=	<<-461.0627, 3029.6101, 27.9430>>
			LocationData.Peds[3].fHeading			=	297.2584
			//Vehicle
			LocationData.Vehicle[0].vCreationPos[0]	=	<<-349.4192, 2959.4827, 24.8059>>
			LocationData.Vehicle[0].fHeading[0]		=	100.0125
			LocationData.Vehicle[0].vCreationPos[1]	=	<<-405.8636, 3068.0999, 29.4591>>
			LocationData.Vehicle[0].fHeading[1]		=	112.6015
			LocationData.Vehicle[0].vCreationPos[2]	=	<<-659.7894, 3011.6826, 23.5569>>
			LocationData.Vehicle[0].fHeading[2]		=	284.9224
			//Plane Start
			LocationData.vPlaneStart	=	<<-2460.6819, 2015.0172, 839.1010>>
			LocationData.fPlaneHeading	=	296.1674
		BREAK
		
		CASE CRATE_DROP_LOCATION_29		//RIVER ENTRANCE - COUNTRY
			LocationData.vLocation = GET_CRATE_DROP_VECTOR(LocationData.iIdentifier)	//<<-388.8584, 4353.4780, 54.3806>>
			LocationData.eAiSetup = CD_AI_RANDOM
			//Peds
			LocationData.Peds[0].vCreationPos		=	<<-417.4350, 4308.3926, 63.3466>>
			LocationData.Peds[0].fHeading			=	234.5174
			LocationData.Peds[1].vCreationPos		=	<<-438.7067, 4340.6475, 61.0735>>
			LocationData.Peds[1].fHeading			=	278.4700
			LocationData.Peds[2].vCreationPos		=	<<-368.8454, 4339.0371, 57.1845>>
			LocationData.Peds[2].fHeading			=	77.0994
			LocationData.Peds[3].vCreationPos		=	<<-393.7048, 4374.4287, 54.2199>>
			LocationData.Peds[3].fHeading			=	64.5219
			//Vehicle
			LocationData.Vehicle[0].vCreationPos[0]	=	<<-465.3588, 4334.7852, 60.7145>>
			LocationData.Vehicle[0].fHeading[0]		=	227.8500
			LocationData.Vehicle[0].vCreationPos[1]	=	<<-338.1241, 4256.8389, 42.4494>>
			LocationData.Vehicle[0].fHeading[1]		=	51.2469
			LocationData.Vehicle[0].vCreationPos[2]	=	<<-542.6022, 4354.6436, 64.1361>>
			LocationData.Vehicle[0].fHeading[2]		=	275.3314
			//Plane Start
			LocationData.vPlaneStart	=	<<-2603.9697, 4864.0469, 631.5000>>
			LocationData.fPlaneHeading	=	253.7774
		BREAK
		
		CASE CRATE_DROP_LOCATION_30		//MAZE BANK - CITY
			LocationData.vLocation = GET_CRATE_DROP_VECTOR(LocationData.iIdentifier)	//<<-134.0402, -869.4509, 43.2175>>
			LocationData.eAiSetup = CD_AI_RANDOM
			//Peds
			LocationData.Peds[0].vCreationPos		=	<<-127.9382, -865.4045, 32.3300>>
			LocationData.Peds[0].fHeading			=	131.1114
			LocationData.Peds[1].vCreationPos		=	<<-120.4006, -839.0247, 43.2158>>
			LocationData.Peds[1].fHeading			=	303.7408
			LocationData.Peds[2].vCreationPos		=	<<-99.9360, -877.1848, 43.2164>>
			LocationData.Peds[2].fHeading			=	313.9738
			LocationData.Peds[3].vCreationPos		=	<<-85.6775, -842.2723, 39.5395>>
			LocationData.Peds[3].fHeading			=	126.8710
			//Vehicle
			LocationData.Vehicle[0].vCreationPos[0]	=	<<-62.7999, -923.7296, 28.1789>>
			LocationData.Vehicle[0].fHeading[0]		=	69.7026
			LocationData.Vehicle[0].vCreationPos[1]	=	<<-215.8648, -1010.7717, 28.1444>>
			LocationData.Vehicle[0].fHeading[1]		=	337.6824
			LocationData.Vehicle[0].vCreationPos[2]	=	<<-270.7842, -887.5016, 30.1010>>
			LocationData.Vehicle[0].fHeading[2]		=	338.2989
			//Plane Start
			LocationData.vPlaneStart	=	<<-1532.0463, -652.9149, 637.4343>>
			LocationData.fPlaneHeading	=	262.2055
			//Force to Ground Pos
			LocationData.vSpecificForceToGroundLocation = <<-133.2166, -871.6094, 43.2173>>
		BREAK
		
		CASE CRATE_DROP_LOCATION_31		//MIRROR PARK - CITY
			LocationData.vLocation = GET_CRATE_DROP_VECTOR(LocationData.iIdentifier)	//<<1098.2463, -544.2941, 56.4061>>
			LocationData.eAiSetup = CD_AI_RANDOM
			//Peds
			LocationData.Peds[0].vCreationPos		=	<<1109.5618, -544.2077, 56.1076>>
			LocationData.Peds[0].fHeading			=	174.9766
			LocationData.Peds[1].vCreationPos		=	<<1100.5092, -529.1635, 61.9093>>
			LocationData.Peds[1].fHeading			=	356.8412
			LocationData.Peds[2].vCreationPos		=	<<1078.5105, -554.7376, 55.9086>>
			LocationData.Peds[2].fHeading			=	263.9750
			LocationData.Peds[3].vCreationPos		=	<<1130.4241, -565.5107, 55.7759>>
			LocationData.Peds[3].fHeading			=	132.5288
			//Vehicle
			LocationData.Vehicle[0].vCreationPos[0]	=	<<1077.1531, -428.0514, 65.6296>>
			LocationData.Vehicle[0].fHeading[0]		=	169.8658
			LocationData.Vehicle[0].vCreationPos[1]	=	<<1176.2926, -465.3253, 65.0413>>
			LocationData.Vehicle[0].fHeading[1]		=	167.1621
			LocationData.Vehicle[0].vCreationPos[2]	=	<<975.8554, -567.0255, 58.0341>>
			LocationData.Vehicle[0].fHeading[2]		=	307.5059
			//Plane Start
			LocationData.vPlaneStart	=	<<1466.3054, -2052.0859, 382.5888>>
			LocationData.fPlaneHeading	=	23.7138
			//Force to Ground Pos
			LocationData.vSpecificForceToGroundLocation = <<1100.4607, -546.2083, 56.2259>>
		BREAK
				
		/*CASE CRATE_DROP_LOCATION_3		//
			LocationData.vLocation = GET_CRATE_DROP_VECTOR(LocationData.iIdentifier)	//<<>>
			LocationData.eAiSetup = CD_AI_RANDOM
			//Peds
			LocationData.Peds[0].vCreationPos		=	<<>>
			LocationData.Peds[0].fHeading			=	
			LocationData.Peds[1].vCreationPos		=	<<>>
			LocationData.Peds[1].fHeading			=	
			LocationData.Peds[2].vCreationPos		=	<<>>
			LocationData.Peds[2].fHeading			=	
			LocationData.Peds[3].vCreationPos		=	<<>>
			LocationData.Peds[3].fHeading			=	
			//Vehicle
			LocationData.Vehicle[0].vCreationPos[0]	=	<<>>
			LocationData.Vehicle[0].fHeading[0]		=	
			LocationData.Vehicle[0].vCreationPos[1]	=	<<>>
			LocationData.Vehicle[0].fHeading[1]		=	
			LocationData.Vehicle[0].vCreationPos[2]	=	<<>>
			LocationData.Vehicle[0].fHeading[2]		=	
			//Plane Start
			LocationData.vPlaneStart	=	<<>>
			LocationData.fPlaneHeading	=	
		BREAK*/
	ENDSWITCH
	
	//GET_RANDOM_PLANE_CREATION_POINT(LocationData.vLocation, LocationData.vPlaneStart, LocationData.fPlaneHeading)
	
	//Set Special Crate Drop Sepcifics
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_SpecialCrateDrop)
		LocationData.iNumPeds = LOCATION_MAX_NUM_PEDS
		LocationData.eAiSetup = CD_AI_ON_FOOT_BEFORE
	ENDIF
	
	//DEBUG TEST
	//LocationData.eAiSetup = CD_AI_ON_FOOT_BEFORE
ENDPROC


///////////////////////////////////////////
///    		GET PLANE DATA    			///
///////////////////////////////////////////
CONST_INT CRATE_DROP_PLANE_0		0		//STANDARD
CONST_INT CRATE_DROP_PLANE_1		1		//SPECIAL

//PURPOSE: Gets the plane data
PROC GET_CRATE_DROP_PLANE_DATA(CD_PLANE_DATA &PlaneData, VECTOR vLocation, FLOAT fHeading)
	
	//IF IS_BIT_SET(serverBD.iServerBitSet, biS_SpecialCrateDrop)
	//	PlaneData.iIdentifier = CRATE_DROP_PLANE_1
	//ENDIF
	
	SWITCH PlaneData.iIdentifier
		CASE CRATE_DROP_PLANE_0
			//Plane
			PlaneData.Plane.VehModel			=	CUBAN800
			PlaneData.Plane.vCreationPos[0]		=	vLocation
			PlaneData.Plane.fHeading[0]			= 	fHeading	//0	//WORK OUT HEADING TO LOCATION 2?
			PlaneData.Plane.iHealth				= 	2000
			//Pilot
			PlaneData.Pilot.PedModel			=	S_M_M_PILOT_02
			PlaneData.Pilot.vCreationPos		=	PlaneData.Plane.vCreationPos[0]
			PlaneData.Pilot.fHeading			=	0
			PlaneData.Pilot.RelGroup			=	rgFM_AiLike
		BREAK
		CASE CRATE_DROP_PLANE_1
			//Plane
			PlaneData.Plane.VehModel			=	TITAN
			PlaneData.Plane.vCreationPos[0]		=	vLocation
			PlaneData.Plane.fHeading[0]			= 	fHeading	//0	//WORK OUT HEADING TO LOCATION 2?
			PlaneData.Plane.iHealth				= 	4000
			//Pilot
			PlaneData.Pilot.PedModel			=	S_M_M_PILOT_02
			PlaneData.Pilot.vCreationPos		=	PlaneData.Plane.vCreationPos[0]
			PlaneData.Pilot.fHeading			=	0
			PlaneData.Pilot.RelGroup			=	rgFM_AiLike
		BREAK
	ENDSWITCH
ENDPROC

FUNC INT GET_CLOTHING_ARRAY_TUNABLE(INT iArrayPos, BOOL bMale = TRUE)
	SWITCH iArrayPos
		CASE 0
			IF bMale
				RETURN g_sMPTunables.iCrateDropClothingMale_collection_1
			ELSE
				RETURN g_sMPTunables.iCrateDropClothingFemale_collection_1
			ENDIF
		BREAK
		CASE 1
			IF bMale
				RETURN g_sMPTunables.iCrateDropClothingMale_collection_2
			ELSE
				RETURN g_sMPTunables.iCrateDropClothingFemale_collection_2
			ENDIF
		BREAK
		CASE 2
			IF bMale
				RETURN g_sMPTunables.iCrateDropClothingMale_collection_3
			ELSE
				RETURN g_sMPTunables.iCrateDropClothingFemale_collection_3
			ENDIF
		BREAK
		CASE 3
			IF bMale
				RETURN g_sMPTunables.iCrateDropClothingMale_collection_4
			ELSE
				RETURN g_sMPTunables.iCrateDropClothingFemale_collection_4
			ENDIF
		BREAK
	ENDSWITCH
	
	//DEFAULT
	IF bMale
		RETURN g_sMPTunables.iCrateDropClothingMale_collection_1
	ENDIF
	RETURN g_sMPTunables.iCrateDropClothingFemale_collection_1
ENDFUNC

//PUTPOSE: Returns TRUE if the clothing item is valid and not already owned
FUNC BOOL IS_CLOTHING_ITEM_VALID(PED_COMP_TYPE_ENUM compType, PED_COMP_NAME_ENUM compName)

	
	TATTOO_DATA_STRUCT sTattooData
	PED_COMP_ITEM_DATA_STRUCT sItemData
	
	// DECL components actually use the tattoo system.
	// Crate drops only award special items so we don't need to check if it has been purchased - we just check the reward unlock stat (normally a packed stat).
	IF compType = COMP_TYPE_DECL
		IF (IS_PLAYER_MALE(PLAYER_ID()) AND GET_TATTOO_DATA(sTattooData, INT_TO_ENUM(TATTOO_NAME_ENUM, ENUM_TO_INT(compName)), TATTOO_MP_FM, PLAYER_PED_ID()))
		OR (NOT IS_PLAYER_MALE(PLAYER_ID()) AND GET_TATTOO_DATA(sTattooData, INT_TO_ENUM(TATTOO_NAME_ENUM, ENUM_TO_INT(compName)), TATTOO_MP_FM_F, PLAYER_PED_ID()))
			STATS_PACKED eUnlockStat = GET_EVENT_ITEM_PACKED_STAT(sTattooData.iPreset)
			IF eUnlockStat != INT_TO_ENUM(STATS_PACKED, -1)
			AND NOT GET_PACKED_STAT_BOOL(eUnlockStat)
				PRINTLN("     ---------->     CRATE DROP - IS_CLOTHING_ITEM_VALID - TRUE: overlay decl = ", sTattooData.sLabel)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF compType != INT_TO_ENUM(PED_COMP_TYPE_ENUM, -1)
		IF compName != INT_TO_ENUM(PED_COMP_NAME_ENUM, -1)
		AND compName != DUMMY_PED_COMP
		AND IS_PED_COMP_ITEM_AVAILABLE_MP(GET_PLAYER_MODEL(), compType, compName)
		
			// When we award the player we don't acquire it immeditately, it just gets unlocked.
			sItemData = GET_PED_COMP_DATA_FOR_ITEM_MP(GET_PLAYER_MODEL(), compType, compName)
			
			IF IS_BIT_SET(sItemData.iProperties, PED_COMPONENT_IS_DLC_BIT)
				STATS_PACKED eUnlockStat = GET_EVENT_ITEM_PACKED_STAT(g_iLastDLCItemNameHash)
				IF eUnlockStat != INT_TO_ENUM(STATS_PACKED, -1)
				AND NOT GET_PACKED_STAT_BOOL(eUnlockStat)
					PRINTLN("     ---------->     CRATE DROP - IS_CLOTHING_ITEM_VALID - TRUE: clothing = ", sItemData.sLabel)
					RETURN TRUE
				ENDIF
			ENDIF
			
			PRINTLN("     ---------->     CRATE DROP - IS_CLOTHING_ITEM_VALID - FALSE: clothing = ", sItemData.sLabel)
			RETURN FALSE
		ENDIF
	ENDIF
	
	PRINTLN("     ---------->     CRATE DROP - IS_CLOTHING_ITEM_VALID - FALSE")
	RETURN FALSE
ENDFUNC

FUNC INT GET_CRATE_STANDARD_CLEANUP_TIME()
	RETURN g_sMPTunables.iCrateDropStandardCleanupTime
ENDFUNC

FUNC INT GET_CRATE_SPECIAL_CLEANUP_TIME()
	RETURN g_sMPTunables.iCrateDropSpecialCleanupTime
ENDFUNC

FUNC INT GET_CRATE_ADDITIONAL_CLEANUP_TIME()
	RETURN g_sMPTunables.iCrateDropAdditionalCleanupTime
ENDFUNC

///////////////////////////////////////////
///    		GET CRATE DATA    			///
///////////////////////////////////////////
CONST_INT CRATE_DROP_CRATE_0		0		//STANDARD

//PURPOSE: Gets the crate data
PROC GET_CRATE_DROP_CRATE_DATA(CD_CRATE_DATA &CrateData)
	
	SWITCH CrateData.iIdentifier
		CASE CRATE_DROP_CRATE_0
			//Crate and Chute
			CrateData.CrateModel	= PROP_DRUG_PACKAGE
			CrateData.ChuteModel	= P_CARGO_CHUTE_S
			CrateData.ChuteOffset	= <<0, 0, 0>>
			
			//Anims
			CrateData.AnimDictionary 	= "P_cargo_chute_S"
			CrateData.AnimDeploy 		= "P_cargo_chute_S_deploy"
			CrateData.AnimCrumple 		= "P_cargo_chute_S_crumple"
			
			//Timeout
			CrateData.iCleanupTimerDelay =  GET_CRATE_STANDARD_CLEANUP_TIME()
			
			//Contents
			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_SpecialCrateDrop)
				//CrateData.eContentsType = INT_TO_ENUM(CD_CRATE_CONTENTS_TYPE, GET_RANDOM_INT_IN_RANGE(ENUM_TO_INT(CD_CONTENTS_CASH), ENUM_TO_INT(CD_CONTENTS_WEAPON)+1))
				IF GET_RANDOM_BOOL()
					CrateData.eContentsType = CD_CONTENTS_CASH
				ELSE
					CrateData.eContentsType = CD_CONTENTS_WEAPON
				ENDIF
			ELSE
				CrateData.eContentsType = CD_CONTENTS_DLC
			ENDIF
//			// TEMP
//			CrateData.eContentsType = CD_CONTENTS_DLC
			
			SWITCH CrateData.eContentsType
				CASE CD_CONTENTS_CASH
					//**TWH - RLB - (Changed the amount of cash given for Crate Drop crate).
					// Added for 1414268. 
					// Further edit 1592000 - set to 500 instead of 1000
					CrateData.iContentsAmount = 1000 * GET_RANDOM_INT_IN_RANGE(g_sMPTunables.iCrateDropCashMinimum , g_sMPTunables.iCrateDropCashMaximum)
					NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - GET_CRATE_DROP_CRATE_DATA - cash for crate = ") NET_PRINT_INT(CrateData.iContentsAmount) NET_NL()
					IF CrateData.iContentsAmount > g_sMPTunables.iCrateDropMaxTotalCash
						CrateData.iContentsAmount = g_sMPTunables.iCrateDropMaxTotalCash
						NET_PRINT("    ---------->     CRATE DROP - GET_CRATE_DROP_CRATE_DATA - cash cap applied ") NET_PRINT_INT(g_sMPTunables.iCrateDropMaxTotalCash) NET_NL()
					ELSE
						NET_PRINT("    ---------->     CRATE DROP - GET_CRATE_DROP_CRATE_DATA - cash cap not applied ") NET_PRINT_INT(g_sMPTunables.iCrateDropMaxTotalCash) NET_NL()
					ENDIF
					
				BREAK
				
				/*CASE CD_CONTENTS_XP
					CrateData.iContentsAmount = (250 * GET_RANDOM_INT_IN_RANGE(2, 11))
					NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - GET_CRATE_DROP_CRATE_DATA - XP ") NET_PRINT_INT(CrateData.iContentsAmount) NET_NL()
				BREAK*/
				
				CASE CD_CONTENTS_WEAPON
					INT iRand
					iRand = GET_RANDOM_INT_IN_RANGE(0, MAX_NUM_CRATE_DROP_WEAPONS)
										
					IF g_sMPTunables.iCrateDropWeapons[iRand] != 0
					AND g_sMPTunables.iCrateDropWeaponClips[iRand] > 0
						CrateData.iContentsAmount =  g_sMPTunables.iCrateDropWeaponClips[iRand]
						CrateData.iContentsWeapon = INT_TO_ENUM(WEAPON_TYPE, g_sMPTunables.iCrateDropWeapons[iRand])
						iWeaponHash = g_sMPTunables.iCrateDropWeapons[iRand]
						NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - GET_CRATE_DROP_CRATE_DATA - WEAPON - TUNEABLE USED - HASH = ") NET_PRINT_INT(g_sMPTunables.iCrateDropWeapons[iRand]) NET_NL()
						NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - GET_CRATE_DROP_CRATE_DATA - WEAPON - TUNEABLE USED - WEAPON NAME = ") NET_PRINT(GET_WEAPON_NAME(CrateData.iContentsWeapon)) NET_NL()
					//Fail Safe
					ELSE
						
						IF iRand = 1
							CrateData.iContentsAmount = 4
							CrateData.iContentsWeapon = WEAPONTYPE_RPG				//-1312131151
						ELIF iRand = 2
							CrateData.iContentsAmount = 4
							CrateData.iContentsWeapon = WEAPONTYPE_HEAVYSNIPER		//205991906
						ELIF iRand = 3
							CrateData.iContentsAmount = 8
							CrateData.iContentsWeapon = WEAPONTYPE_ASSAULTSHOTGUN	//-494615257
						ELIF iRand = 4
							CrateData.iContentsAmount = 4
							CrateData.iContentsWeapon = WEAPONTYPE_STICKYBOMB		//741814745
						ELIF iRand = 5
							CrateData.iContentsAmount = 1
							CrateData.iContentsWeapon = WEAPONTYPE_GRENADELAUNCHER	//-1568386805
						ELIF iRand = 6
							CrateData.iContentsAmount = 4
							CrateData.iContentsWeapon = WEAPONTYPE_ADVANCEDRIFLE	//-1357824103
						ELIF iRand = 7
							CrateData.iContentsAmount = 2
							CrateData.iContentsWeapon = WEAPONTYPE_COMBATMG			//2144741730
						ELIF iRand = 8
							CrateData.iContentsAmount = 4
							CrateData.iContentsWeapon = WEAPONTYPE_MOLOTOV			//615608432
						ELIF iRand = 9
							CrateData.iContentsAmount = 4
							CrateData.iContentsWeapon = WEAPONTYPE_GRENADE			//-1813897027
						ELSE
							CrateData.iContentsAmount = 1
							CrateData.iContentsWeapon = WEAPONTYPE_MINIGUN			//1119849093
						ENDIF
						NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - GET_CRATE_DROP_CRATE_DATA - WEAPON - BACKUP USED")  NET_NL()
					ENDIF
					
					iWeaponHash = ENUM_TO_INT(CrateData.iContentsWeapon)
					
					NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - GET_CRATE_DROP_CRATE_DATA - WEAPON ") NET_PRINT(GET_WEAPON_NAME(CrateData.iContentsWeapon)) NET_NL()
				BREAK
				
				CASE CD_CONTENTS_DLC
					IF g_sMPTunables.iCrateDropSpecialCash > 0
						CrateData.iContentsAmount = g_sMPTunables.iCrateDropSpecialCash 		//(5000)
						NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - GET_CRATE_DROP_CRATE_DATA - SPECIAL CASH = ") NET_PRINT_INT(CrateData.iContentsAmount) NET_NL()
					ELSE
						CrateData.iContentsAmount = 5000
						NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - GET_CRATE_DROP_CRATE_DATA - SPECIAL CASH(BACKUP!) = ") NET_PRINT_INT(CrateData.iContentsAmount) NET_PRINT(" tunable contained = ") NET_PRINT_INT(g_sMPTunables.iCrateDropSpecialCash) NET_NL()
					ENDIF
					
					IF g_sMPTunables.iCrateDropSpecialWeapon != 0
					AND g_sMPTunables.iCrateDropSpecialWeaponClips > 0
						CrateData.iContentsWeapon = INT_TO_ENUM(WEAPON_TYPE, g_sMPTunables.iCrateDropSpecialWeapon)		//WEAPONTYPE_MINIGUN
						NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - GET_CRATE_DROP_CRATE_DATA - SPECIAL WEAPON - TUNEABLE USED  - HASH = ") NET_PRINT_INT(g_sMPTunables.iCrateDropSpecialWeapon) NET_NL()
						NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - GET_CRATE_DROP_CRATE_DATA - SPECIAL WEAPON - TUNEABLE USED - WEAPON NAME = ") NET_PRINT(GET_WEAPON_NAME(CrateData.iContentsWeapon)) NET_NL()
						//CrateData.iContentsAmount = g_sMPTunables.iCrateDropSpecialWeaponClips
					ELSE
						CrateData.iContentsWeapon = WEAPONTYPE_MINIGUN
						//CrateData.iContentsAmount = 1
					ENDIF
					
					iWeaponHash = ENUM_TO_INT(CrateData.iContentsWeapon)
					NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - GET_CRATE_DROP_CRATE_DATA - SPECIAL WEAPON ") NET_PRINT(GET_WEAPON_NAME(CrateData.iContentsWeapon)) NET_NL()
					
					iOtherItemHash = 0 // g_sMPTunables.iCrateDropSpecialItem	//0	//NEED TO SET UP TO BE OTHER ITEM
					
					//SINGLE CLOTHING ITEM
					NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - GET_CRATE_DROP_CRATE_DATA - g_sMPTunables.iCrateDropClothingMale") NET_PRINT_INT(g_sMPTunables.iCrateDropClothingMale) NET_NL()
					NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - GET_CRATE_DROP_CRATE_DATA - g_sMPTunables.iCrateDropClothingFemale") NET_PRINT_INT(g_sMPTunables.iCrateDropClothingFemale) NET_NL()
					// Try and grab male comps
					IF GET_MP_REWARD_CLOTHING_FROM_TUNABLE(MP_M_FREEMODE_01, g_sMPTunables.iCrateDropClothingMale, CrateData.eContentsMaleClothingType, CrateData.eContentsMaleClothingName)
						IF IS_PLAYER_MALE(PLAYER_ID())
							iSpecialItemHash.m_Hashs[0] = g_sMPTunables.iCrateDropClothingMale
							NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - GET_CRATE_DROP_CRATE_DATA - iSpecialItemHash.m_Hashs[0] = ") NET_PRINT_INT(iSpecialItemHash.m_Hashs[0]) NET_NL()
						ENDIF
						NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - GET_CRATE_DROP_CRATE_DATA - grabbed male clothing data from tunable") NET_NL()
					ELSE
						NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - GET_CRATE_DROP_CRATE_DATA - GET_MP_REWARD_CLOTHING_FROM_TUNABLE - FAILED FOR MALE") NET_NL()
					ENDIF
					
					// Try and grab female comps
					IF GET_MP_REWARD_CLOTHING_FROM_TUNABLE(MP_F_FREEMODE_01, g_sMPTunables.iCrateDropClothingFemale, CrateData.eContentsFemaleClothingType, CrateData.eContentsFemaleClothingName)
						IF NOT IS_PLAYER_MALE(PLAYER_ID())
							iSpecialItemHash.m_Hashs[0] = g_sMPTunables.iCrateDropClothingFemale
							NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - GET_CRATE_DROP_CRATE_DATA - iSpecialItemHash.m_Hashs[0] = ") NET_PRINT_INT(iSpecialItemHash.m_Hashs[0]) NET_NL()
						ENDIF
						NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - GET_CRATE_DROP_CRATE_DATA - grabbed female clothing data from tunable") NET_NL()
					ELSE
						NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - GET_CRATE_DROP_CRATE_DATA - GET_MP_REWARD_CLOTHING_FROM_TUNABLE - FAILED FOR FEMALE") NET_NL()
					ENDIF
					
					//ARRAY OF CLOTHING ITEMS
					INT i
					REPEAT CLOTHING_ARRAY_SIZE i
						PRINTLN("     ---------->     CRATE DROP - GET_CRATE_DROP_CRATE_DATA - g_sMPTunables.iCrateDropClothingMale_collection_", i+1, " = ", GET_CLOTHING_ARRAY_TUNABLE(i))
						PRINTLN("     ---------->     CRATE DROP - GET_CRATE_DROP_CRATE_DATA - g_sMPTunables.iCrateDropClothingFemale_collection_", i+1, " = ", GET_CLOTHING_ARRAY_TUNABLE(i, FALSE))
						
						// Try and grab male comps
						IF GET_MP_REWARD_CLOTHING_FROM_TUNABLE(MP_M_FREEMODE_01, GET_CLOTHING_ARRAY_TUNABLE(i), CrateData.eContentsMaleClothingTypeArray[i], CrateData.eContentsMaleClothingNameArray[i])
							IF IS_PLAYER_MALE(PLAYER_ID())
								iSpecialItemHash.m_Hashs[i+1] = GET_CLOTHING_ARRAY_TUNABLE(i)
								NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - GET_CRATE_DROP_CRATE_DATA - iSpecialItemHash.m_Hashs[i+1] = ") NET_PRINT_INT(iSpecialItemHash.m_Hashs[i+1]) NET_NL()
							ENDIF
							NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - GET_CRATE_DROP_CRATE_DATA - grabbed male clothing data from tunable") NET_NL()
						ELSE
							NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - GET_CRATE_DROP_CRATE_DATA - GET_MP_REWARD_CLOTHING_FROM_TUNABLE - FAILED FOR MALE") NET_NL()
						ENDIF
						
						// Try and grab female comps
						IF GET_MP_REWARD_CLOTHING_FROM_TUNABLE(MP_F_FREEMODE_01, GET_CLOTHING_ARRAY_TUNABLE(i, FALSE), CrateData.eContentsFemaleClothingTypeArray[i], CrateData.eContentsFemaleClothingNameArray[i])
							IF NOT IS_PLAYER_MALE(PLAYER_ID())
								iSpecialItemHash.m_Hashs[i+1] = GET_CLOTHING_ARRAY_TUNABLE(i, FALSE)
								NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - GET_CRATE_DROP_CRATE_DATA - iSpecialItemHash.m_Hashs[i+1] = ") NET_PRINT_INT(iSpecialItemHash.m_Hashs[i+1]) NET_NL()
							ENDIF
							NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - GET_CRATE_DROP_CRATE_DATA - grabbed female clothing data from tunable") NET_NL()
						ELSE
							NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - GET_CRATE_DROP_CRATE_DATA - GET_MP_REWARD_CLOTHING_FROM_TUNABLE - FAILED FOR FEMALE") NET_NL()
						ENDIF
					ENDREPEAT
					
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC


///////////////////////////////////
///    		DATA    			///
///////////////////////////////////
//PURPOSE: Gets the Crate Drop Data
FUNC BOOL GET_CRATE_DROP_DATA()
	INT i
	REPEAT serverBD.CrateDropData.iNumDrops i
		GET_CRATE_DROP_LOCATION_DATA(serverBD.CrateDropData.LocationData[i])
		CD_USE_MODEL_SET(serverBD.CrateDropData.LocationData[i], serverBD.CrateDropData.eModelSet)
		GET_CRATE_DROP_CRATE_DATA(serverBD.CrateDropData.CrateData[i])
		serverBD.CrateDropData.CrateData[i].BleepSoundID = -1
		serverBD.CrateDropData.CrateData[i].ptfxBeacon = NULL
	ENDREPEAT
	//GET_CRATE_DROP_PLANE_DATA(serverBD.CrateDropData.PlaneData, serverBD.CrateDropData.LocationData[0].vPlaneStart, serverBD.CrateDropData.LocationData[0].fPlaneHeading)
	
	RETURN TRUE
ENDFUNC














//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  ENTITY CREATION                  /////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
 
///////////////////////////////////////////
///    		PLANE AND PILOT    			///
///////////////////////////////////////////
////PURPOSE: Create the Plane
//FUNC BOOL CREATE_PLANE()
//	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.CrateDropData.PlaneData.Plane.niVeh)
//		IF REQUEST_LOAD_MODEL(serverBD.CrateDropData.PlaneData.Plane.VehModel)
//						
//			IF CREATE_NET_VEHICLE(serverBD.CrateDropData.PlaneData.Plane.niVeh, serverBD.CrateDropData.PlaneData.Plane.VehModel, serverBD.CrateDropData.PlaneData.Plane.vCreationPos[0], serverBD.CrateDropData.PlaneData.Plane.fHeading[0])	
//				
//				//SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(NET_TO_VEH(serverBD.CrateDropData.PlaneData.Plane.niVeh), TRUE)
//				//SET_VEHICLE_TYRES_CAN_BURST(NET_TO_VEH(serverBD.CrateDropData.PlaneData.Plane.niVeh), FALSE)
//				
//				SET_ENTITY_HEALTH(NET_TO_VEH(serverBD.CrateDropData.PlaneData.Plane.niVeh), serverBD.CrateDropData.PlaneData.Plane.iHealth)
//				SET_VEHICLE_ENGINE_HEALTH(NET_TO_VEH(serverBD.CrateDropData.PlaneData.Plane.niVeh), TO_FLOAT(serverBD.CrateDropData.PlaneData.Plane.iHealth))
//				SET_VEHICLE_PETROL_TANK_HEALTH(NET_TO_VEH(serverBD.CrateDropData.PlaneData.Plane.niVeh), TO_FLOAT(serverBD.CrateDropData.PlaneData.Plane.iHealth))
//				
//				SET_VEHICLE_DOORS_LOCKED(NET_TO_VEH(serverBD.CrateDropData.PlaneData.Plane.niVeh), VEHICLELOCK_LOCKED)
//				
//				FREEZE_ENTITY_POSITION(NET_TO_VEH(serverBD.CrateDropData.PlaneData.Plane.niVeh), FALSE)
//				SET_ENTITY_DYNAMIC(NET_TO_VEH(serverBD.CrateDropData.PlaneData.Plane.niVeh), TRUE)
//				ACTIVATE_PHYSICS(NET_TO_VEH(serverBD.CrateDropData.PlaneData.Plane.niVeh))
//				SET_VEHICLE_FORWARD_SPEED(NET_TO_VEH(serverBD.CrateDropData.PlaneData.Plane.niVeh), 15)     
//				SET_HELI_BLADES_FULL_SPEED(NET_TO_VEH(serverBD.CrateDropData.PlaneData.Plane.niVeh))
//				SET_VEHICLE_ENGINE_ON(NET_TO_VEH(serverBD.CrateDropData.PlaneData.Plane.niVeh), TRUE, TRUE)
//				SET_VEHICLE_ENGINE_CAN_DEGRADE(NET_TO_VEH(serverBD.CrateDropData.PlaneData.Plane.niVeh), FALSE)
//				
//				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(NET_TO_VEH(serverBD.CrateDropData.PlaneData.Plane.niVeh), FALSE)
//				
//				CONTROL_LANDING_GEAR(NET_TO_VEH(serverBD.CrateDropData.PlaneData.Plane.niVeh), LGC_RETRACT_INSTANT)
//				OPEN_BOMB_BAY_DOORS(NET_TO_VEH(serverBD.CrateDropData.PlaneData.Plane.niVeh))
//				
//				NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - CREATED PLANE ") NET_PRINT_VECTOR(serverBD.CrateDropData.PlaneData.Plane.vCreationPos[0]) NET_NL()
//			ENDIF
//		ENDIF
//	ENDIF
//	
//	//Check we have created the vehicle
//	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.CrateDropData.PlaneData.Plane.niVeh)
//		RETURN FALSE
//	ENDIF
//	
//	SET_MODEL_AS_NO_LONGER_NEEDED(serverBD.CrateDropData.PlaneData.Plane.VehModel)
//	RETURN TRUE
//ENDFUNC
//
////PURPOSE: Gives the pilot the flight task
//PROC CONTROL_PILOT_FLIGHT_TASK()
//	//IF IS_NET_VEHICLE_DRIVEABLE(serverBD.CrateDropData.PlaneData.Plane.niVeh)
//	IF NOT IS_NET_PED_INJURED(serverBD.CrateDropData.PlaneData.Pilot.niPed)
//		IF IS_PED_IN_VEHICLE(NET_TO_PED(serverBD.CrateDropData.PlaneData.Pilot.niPed), NET_TO_VEH(serverBD.CrateDropData.PlaneData.Plane.niVeh))
//			//IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.CrateDropData.PlaneData.Plane.niVeh)
//			//OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.CrateDropData.PlaneData.Plane.niVeh) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
//				IF serverBD.iDropsDone < serverBD.CrateDropData.iNumDrops
//					IF GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.CrateDropData.PlaneData.Pilot.niPed), SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD) != PERFORMING_TASK
//					AND GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.CrateDropData.PlaneData.Pilot.niPed), SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD) != WAITING_TO_START_TASK
//						TASK_VEHICLE_DRIVE_TO_COORD(NET_TO_PED(serverBD.CrateDropData.PlaneData.Pilot.niPed), NET_TO_VEH(serverBD.CrateDropData.PlaneData.Plane.niVeh), serverBD.CrateDropData.LocationData[serverBD.iDropsDone].vLocation+<<0, 0, PLANE_HEIGHT_ABOVE_DROP>>, PLANE_NORMAL_SPEED, DRIVINGSTYLE_NORMAL, serverBD.CrateDropData.PlaneData.Plane.VehModel, DRIVINGMODE_PLOUGHTHROUGH, PLANE_TARGET_RADIUS, -1) 
//						SET_DRIVE_TASK_CRUISE_SPEED(NET_TO_PED(serverBD.CrateDropData.PlaneData.Pilot.niPed), PLANE_NORMAL_SPEED)
//						SET_TASK_VEHICLE_GOTO_PLANE_MIN_HEIGHT_ABOVE_TERRAIN(NET_TO_VEH(serverBD.CrateDropData.PlaneData.Plane.niVeh), PLANE_LOW_HEIGHT)
//						NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - CONTROL_PILOT_FLIGHT_TASK - ") NET_PRINT_INT(serverBD.iDropsDone) NET_NL()
//					ENDIF
//				ELSE
//					IF GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.CrateDropData.PlaneData.Pilot.niPed), SCRIPT_TASK_VEHICLE_MISSION) != PERFORMING_TASK
//					AND GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.CrateDropData.PlaneData.Pilot.niPed), SCRIPT_TASK_VEHICLE_MISSION) != WAITING_TO_START_TASK
//						IF TAKE_CONTROL_OF_NET_ID(serverBD.CrateDropData.PlaneData.Plane.niVeh)
//							CLOSE_BOMB_BAY_DOORS(NET_TO_VEH(serverBD.CrateDropData.PlaneData.Plane.niVeh))
//							//TASK_PERFORM_SEQUENCE(NET_TO_PED(serverBD.CrateDropData.PlaneData.Pilot.niPed), seqPlaneLeave)
//							
//							//VECTOR vDest = serverBD.CrateDropData.LocationData[serverBD.CrateDropData.iNumDrops-1].vPlaneStart+<<0, 0, PLANE_HEIGHT_ABOVE_DROP>>
//							//vDest.y = 9000.0	//FAR NORTH
//							VECTOR vDest = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(NET_TO_VEH(serverBD.CrateDropData.PlaneData.Plane.niVeh), <<0, 12000, 0>>)
//							
//							IF vDest.x < -3600
//								vDest.x = -3600
//							ELIF vDest.x > 4400
//								vDest.x = 4400	
//							ENDIF
//							IF vDest.y < -3800
//								vDest.y = -3800
//							ELIF vDest.y > 7900
//								vDest.y = 7900	
//							ENDIF
//							vDest.z = 100
//							//TASK_VEHICLE_DRIVE_TO_COORD(NET_TO_PED(serverBD.CrateDropData.PlaneData.Pilot.niPed), NET_TO_VEH(serverBD.CrateDropData.PlaneData.Plane.niVeh), vDest, PLANE_NORMAL_SPEED, DRIVINGSTYLE_NORMAL, serverBD.CrateDropData.PlaneData.Plane.VehModel, DRIVINGMODE_AVOIDCARS, -1, -1)
//							TASK_PLANE_MISSION(NET_TO_PED(serverBD.CrateDropData.PlaneData.Pilot.niPed), NET_TO_VEH(serverBD.CrateDropData.PlaneData.Plane.niVeh), NULL, NULL, vDest, MISSION_GOTO, PLANE_NORMAL_SPEED, -1, -1, 100, 100)
//							
//							SET_DRIVE_TASK_CRUISE_SPEED(NET_TO_PED(serverBD.CrateDropData.PlaneData.Pilot.niPed), PLANE_NORMAL_SPEED)
//							//SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(NET_TO_VEH(serverBD.CrateDropData.PlaneData.Plane.niVeh), TRUE)
//							//CLEANUP_NET_ID(serverBD.CrateDropData.PlaneData.Plane.niVeh)
//							//CLEANUP_NET_ID(serverBD.CrateDropData.PlaneData.Pilot.niPed)
//							NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - CONTROL_PILOT_FLIGHT_TASK - LEAVE SEQUENCE - DEST = ") NET_PRINT_VECTOR(vDest) NET_NL()
//						ENDIF
//					ENDIF
//					/*IF GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.CrateDropData.PlaneData.Pilot.niPed), SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD) != PERFORMING_TASK
//					AND GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.CrateDropData.PlaneData.Pilot.niPed), SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD) != WAITING_TO_START_TASK
//						TASK_VEHICLE_DRIVE_TO_COORD(NET_TO_PED(serverBD.CrateDropData.PlaneData.Pilot.niPed), NET_TO_VEH(serverBD.CrateDropData.PlaneData.Plane.niVeh), serverBD.CrateDropData.LocationData[serverBD.iDropsDone-1].vPlaneStart+<<0, 0, PLANE_HEIGHT_ABOVE_DROP>>, PLANE_NORMAL_SPEED, DRIVINGSTYLE_NORMAL, serverBD.CrateDropData.PlaneData.Plane.VehModel, DRIVINGMODE_PLOUGHTHROUGH, PLANE_TARGET_RADIUS, -1) 
//						NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - CONTROL_PILOT_FLIGHT_TASK - ") NET_PRINT_INT(serverBD.iDropsDone) NET_NL()
//					ENDIF*/
//				ENDIF
//			//ENDIF
//		ENDIF
//	ENDIF
//ENDPROC
//
////PURPOSE: Clears the planes task
//PROC CLEAR_PLANE_TASK()
//	IF NOT IS_NET_PED_INJURED(serverBD.CrateDropData.PlaneData.Pilot.niPed)
//		CLEAR_PED_TASKS(NET_TO_PED(serverBD.CrateDropData.PlaneData.Pilot.niPed))
//		NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - CLEAR_PLANE_TASK - DONE ") NET_NL()
//	ENDIF
//ENDPROC
//
////PURPOSE: Create the Pilot
//FUNC BOOL CREATE_PILOT()
//	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.CrateDropData.PlaneData.Pilot.niPed)
//	AND REQUEST_LOAD_MODEL(serverBD.CrateDropData.PlaneData.Pilot.PedModel)
//	AND NETWORK_DOES_NETWORK_ID_EXIST(serverBD.CrateDropData.PlaneData.Plane.niVeh)
//		IF IS_NET_VEHICLE_DRIVEABLE(serverBD.CrateDropData.PlaneData.Plane.niVeh)
//			IF CREATE_NET_PED_IN_VEHICLE(serverBD.CrateDropData.PlaneData.Pilot.niPed, serverBD.CrateDropData.PlaneData.Plane.niVeh, PEDTYPE_CRIMINAL, serverBD.CrateDropData.PlaneData.Pilot.PedModel, VS_DRIVER)
//				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NET_TO_PED(serverBD.CrateDropData.PlaneData.Pilot.niPed), TRUE)
//				SET_PED_RELATIONSHIP_GROUP_HASH(NET_TO_PED(serverBD.CrateDropData.PlaneData.Pilot.niPed), serverBD.CrateDropData.PlaneData.Pilot.RelGroup)
//				SET_PED_RANDOM_COMPONENT_VARIATION(NET_TO_PED(serverBD.CrateDropData.PlaneData.Pilot.niPed))
//				
//				SET_PED_KEEP_TASK(NET_TO_PED(serverBD.CrateDropData.PlaneData.Pilot.niPed), TRUE)
//				
//				SET_ENTITY_HEALTH(NET_TO_PED(serverBD.CrateDropData.PlaneData.Pilot.niPed), ROUND(200*g_sMPTunables.fAiHealthModifier))
//				
//				//GIVE_PILOT_FLIGHT_SEQUENCE()
//				
//				NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - CREATED PILOT ") NET_NL()
//			ENDIF
//		ENDIF
//	ENDIF
//	
//	//Check we have created the ped
//	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.CrateDropData.PlaneData.Pilot.niPed)
//		RETURN FALSE
//	ENDIF
//	
//	SET_MODEL_AS_NO_LONGER_NEEDED(serverBD.CrateDropData.PlaneData.Pilot.PedModel)
//	RETURN TRUE
//ENDFUNC
//
////PURPOSE: Creates the Plane and Pilot that will deliver the Crates
//FUNC BOOL CREATE_PLANE_AND_PILOT()
//	IF CAN_REGISTER_MISSION_ENTITIES(1, 1, 0, 0)
//		IF CREATE_PLANE()
//		AND CREATE_PILOT()
//			RETURN TRUE
//		ENDIF
//	ENDIF
//	
//	RETURN FALSE
//ENDFUNC

///////////////////////////////////////////
///    		CRATE AND CHUTE    			///
///////////////////////////////////////////

FUNC BOOL LOAD_CRATE_ANIMS()
	REQUEST_ANIM_DICT(serverBD.CrateDropData.CrateData[0].AnimDictionary)
		
	IF HAS_ANIM_DICT_LOADED(serverBD.CrateDropData.CrateData[0].AnimDictionary)
		//NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - LOAD_CRATE_ANIMS - DONE ") NET_NL()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Loads the Crate assets
FUNC BOOL LOAD_CRATE_ASSETS()
	REQUEST_MODEL(serverBD.CrateDropData.CrateData[0].CrateModel)
	REQUEST_MODEL(serverBD.CrateDropData.CrateData[0].ChuteModel)
	
	IF HAS_MODEL_LOADED(serverBD.CrateDropData.CrateData[0].CrateModel)
	AND HAS_MODEL_LOADED(serverBD.CrateDropData.CrateData[0].ChuteModel)
	AND LOAD_CRATE_ANIMS()
		//NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - LOAD_CRATE_ASSETS - DONE ") NET_NL()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//Loads all particle effects required
FUNC BOOL LOAD_INTERACT_PARTICLE_FX()
	REQUEST_PTFX_ASSET()

	IF HAS_PTFX_ASSET_LOADED()
		//NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - LOAD_INTERACT_PARTICLE_FX - DONE ") NET_NL()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Add a beacon fx to a crate
FUNC BOOL ADD_CRATE_BEACON(INT iCrate)
	
	IF LOAD_INTERACT_PARTICLE_FX()
		IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(serverBD.CrateDropData.CrateData[iCrate].ptfxBeacon)
			serverBD.CrateDropData.CrateData[iCrate].ptfxBeacon = START_NETWORKED_PARTICLE_FX_LOOPED_ON_ENTITY("scr_crate_drop_beacon", NET_TO_OBJ(serverBD.CrateDropData.CrateData[iCrate].niCrate), <<0, 0, 0.2>>, <<0, 0, 0>>)	
			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_SpecialCrateDrop) 
				SET_PARTICLE_FX_LOOPED_COLOUR(serverBD.CrateDropData.CrateData[iCrate].ptfxBeacon, 0.8, 0.18, 0.19)
				NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - ADD_CRATE_BEACON - RED") NET_PRINT_INT(iCrate) NET_NL()
			ELSE
				SET_PARTICLE_FX_LOOPED_COLOUR(serverBD.CrateDropData.CrateData[iCrate].ptfxBeacon, 1.0, 0.84, 0.0)
				NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - ADD_CRATE_BEACON - YELLOW") NET_PRINT_INT(iCrate) NET_NL()
			ENDIF
			NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - ADD_CRATE_BEACON ") NET_PRINT_INT(iCrate) NET_NL()
		ENDIF
	ENDIF
	
	//Check flares have been created
	IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(serverBD.CrateDropData.CrateData[iCrate].ptfxBeacon)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

//PURPOSE: Removes a Crate Beacon
PROC CLEANUP_CRATE_BEACON(INT iCrate)
	IF serverBD.CrateDropData.CrateData[iCrate].ptfxBeacon != NULL
		IF DOES_PARTICLE_FX_LOOPED_EXIST(serverBD.CrateDropData.CrateData[iCrate].ptfxBeacon)
			//STOP_PARTICLE_FX_LOOPED(serverBD.CrateDropData.CrateData[iCrate].ptfxBeacon)
			REMOVE_PARTICLE_FX(serverBD.CrateDropData.CrateData[iCrate].ptfxBeacon)
			serverBD.CrateDropData.CrateData[iCrate].ptfxBeacon = NULL
			
			NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - CLEANUP_CRATE_BEACON ") NET_PRINT_INT(iCrate) NET_NL()
		ELSE
			NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - CLEANUP_CRATE_BEACON - FX DOESN'T EXIST ") NET_PRINT_INT(iCrate) NET_NL()
		ENDIF
	ELSE
			NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - CLEANUP_CRATE_BEACON - FX IS NULL ") NET_PRINT_INT(iCrate) NET_NL()
	ENDIF
ENDPROC

//PURPOSE: Cleans up the bleeping sound coming from the crate
PROC CLEANUP_CRATE_BLEEP_SOUND(INT iCrate)
	IF GET_SOUND_ID_FROM_NETWORK_ID(serverBD.CrateDropData.CrateData[iCrate].BleepSoundID) != -1
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.CrateDropData.CrateData[iCrate].niCrate)
			IF NOT HAS_SOUND_FINISHED(GET_SOUND_ID_FROM_NETWORK_ID(serverBD.CrateDropData.CrateData[iCrate].BleepSoundID))
				STOP_SOUND(GET_SOUND_ID_FROM_NETWORK_ID(serverBD.CrateDropData.CrateData[iCrate].BleepSoundID))
				NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - CLEANUP_CRATE_BLEEP_SOUND - A - STOP_SOUND ") NET_PRINT_INT(iCrate) NET_NL()
				RELEASE_SOUND_ID(GET_SOUND_ID_FROM_NETWORK_ID(serverBD.CrateDropData.CrateData[iCrate].BleepSoundID))
				NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - CLEANUP_CRATE_BLEEP_SOUND - SOUND FINISHED ") NET_PRINT_INT(iCrate) NET_NL()
			ENDIF
		ELSE
			IF NOT HAS_SOUND_FINISHED(GET_SOUND_ID_FROM_NETWORK_ID(serverBD.CrateDropData.CrateData[iCrate].BleepSoundID))
				STOP_SOUND(GET_SOUND_ID_FROM_NETWORK_ID(serverBD.CrateDropData.CrateData[iCrate].BleepSoundID))
				NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - CLEANUP_CRATE_BLEEP_SOUND - B - STOP_SOUND ") NET_PRINT_INT(iCrate) NET_NL()
				RELEASE_SOUND_ID(GET_SOUND_ID_FROM_NETWORK_ID(serverBD.CrateDropData.CrateData[iCrate].BleepSoundID))
				NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - CLEANUP_CRATE_BLEEP_SOUND - CRATE NO LONGER EXISTS ") NET_PRINT_INT(iCrate) NET_NL()
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Returns a Vector for the Crate to be created at
FUNC VECTOR GET_CRATE_CREATION_VECTOR(INT iCrate)
	VECTOR vDropLocation = serverBD.CrateDropData.LocationData[iCrate].vLocation
	
	vDropLocation.x += GET_RANDOM_FLOAT_IN_RANGE(0.0, 7.5)
	vDropLocation.y += GET_RANDOM_FLOAT_IN_RANGE(0.0, 7.5)
	vDropLocation.z += (GET_APPROX_HEIGHT_FOR_POINT(vDropLocation.x, vDropLocation.y) + 100.0)
	
	PRINTLN("     ---------->     CRATE DROP - GET_CRATE_CREATION_VECTOR ", vDropLocation)
	RETURN vDropLocation
ENDFUNC

//Creates and drops a weapons cache from the plane
FUNC BOOL CREATE_DROPPED_CRATE(INT iCrate)
	REQUEST_MODEL(serverBD.CrateDropData.CrateData[iCrate].CrateModel)
	REQUEST_MODEL(serverBD.CrateDropData.CrateData[iCrate].ChuteModel)
	
	IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_SpecialCrateDrop)
		REQUEST_MODEL(serverBD.DCrateData[iCrate].Model)
	ENDIF
	
	//Create the Crate
	IF HAS_MODEL_LOADED(serverBD.CrateDropData.CrateData[iCrate].CrateModel)
		SET_BIT(g_sCrateDropF9Data.iBitSet, biCDF9_CRATE_LOADED)
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.CrateDropData.CrateData[iCrate].niCrate)
			SET_BIT(g_sCrateDropF9Data.iBitSet, biCDF9_CRATE_DOESNT_EXIST)
			IF CAN_REGISTER_MISSION_ENTITIES(0, 0, 1, 0)		
				SET_BIT(g_sCrateDropF9Data.iBitSet, biCDF9_CRATE_CAN_REGISTER)				
				//serverBD.CrateDropData.CrateData[iCrate].niCrate = OBJ_TO_NET(CREATE_PORTABLE_PICKUP(PICKUP_PORTABLE_CRATE_UNFIXED, (GET_ENTITY_COORDS(NET_TO_VEH(serverBD.CrateDropData.PlaneData.Plane.niVeh))-<<0, 0, 3>>), FALSE, serverBD.CrateDropData.CrateData[iCrate].CrateModel))
				serverBD.CrateDropData.CrateData[iCrate].niCrate = OBJ_TO_NET(CREATE_PORTABLE_PICKUP(PICKUP_PORTABLE_CRATE_UNFIXED, GET_CRATE_CREATION_VECTOR(iCrate), FALSE, serverBD.CrateDropData.CrateData[iCrate].CrateModel))
				SET_ENTITY_HEADING(NET_TO_OBJ(serverBD.CrateDropData.CrateData[iCrate].niCrate), GET_RANDOM_FLOAT_IN_RANGE(0.0, 360.0))
				SET_NETWORK_ID_EXISTS_ON_ALL_MACHINES(serverBD.CrateDropData.CrateData[iCrate].niCrate, TRUE)
					
				SET_ENTITY_INVINCIBLE(NET_TO_OBJ(serverBD.CrateDropData.CrateData[iCrate].niCrate), TRUE)
				SET_OBJECT_FORCE_VEHICLES_TO_AVOID(NET_TO_OBJ(serverBD.CrateDropData.CrateData[iCrate].niCrate), TRUE)
				
				SET_ACTIVATE_OBJECT_PHYSICS_AS_SOON_AS_IT_IS_UNFROZEN(NET_TO_OBJ(serverBD.CrateDropData.CrateData[iCrate].niCrate), TRUE)
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(NET_TO_OBJ(serverBD.CrateDropData.CrateData[iCrate].niCrate), FALSE)
				ACTIVATE_PHYSICS(NET_TO_OBJ(serverBD.CrateDropData.CrateData[iCrate].niCrate))
				SET_ENTITY_VELOCITY(NET_TO_OBJ(serverBD.CrateDropData.CrateData[iCrate].niCrate), <<0, 0, -0.2>>)
				
				PREVENT_COLLECTION_OF_PORTABLE_PICKUP(NET_TO_OBJ(serverBD.CrateDropData.CrateData[iCrate].niCrate), TRUE)
									
				//TASK_VEHICLE_PLAY_ANIM(PlayerPlane, "va_cuban800", "drophatch")
				
				CLEANUP_CRATE_BLEEP_SOUND(iCrate)
				INT soundID = GET_SOUND_ID()
				PLAY_SOUND_FROM_ENTITY(soundID, "Crate_Beeps", NET_TO_OBJ(serverBD.CrateDropData.CrateData[iCrate].niCrate), "MP_CRATE_DROP_SOUNDS", TRUE, 0)
				serverBD.CrateDropData.CrateData[iCrate].BleepSoundID = GET_NETWORK_ID_FROM_SOUND_ID(soundID)
				
				NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - CREATED CRATE ") NET_PRINT_INT(iCrate) /*NET_PRINT(" AT POS ") NET_PRINT_VECTOR((GET_ENTITY_COORDS(NET_TO_VEH(serverBD.CrateDropData.PlaneData.Plane.niVeh))-<<0, 0, 3>>))*/ NET_NL()
			ENDIF
		ENDIF
	ENDIF
				
				//Create Chute
	IF HAS_MODEL_LOADED(serverBD.CrateDropData.CrateData[iCrate].ChuteModel)	
		SET_BIT(g_sCrateDropF9Data.iBitSet, biCDF9_CHUTE_LOADED)		
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.CrateDropData.CrateData[iCrate].niCrate)
			SET_BIT(g_sCrateDropF9Data.iBitSet, biCDF9_CHUTE_CRATE_DOES_EXIST)		
			IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.CrateDropData.CrateData[iCrate].niChute)
				SET_BIT(g_sCrateDropF9Data.iBitSet, biCDF9_CHUTE_DOESNT_EXIST)		
				IF CAN_REGISTER_MISSION_ENTITIES(0, 0, 1, 0)
					SET_BIT(g_sCrateDropF9Data.iBitSet, biCDF9_CHUTE_CAN_REGISTER)		
					IF CREATE_NET_OBJ(serverBD.CrateDropData.CrateData[iCrate].niChute, serverBD.CrateDropData.CrateData[iCrate].ChuteModel, (GET_ENTITY_COORDS(NET_TO_OBJ(serverBD.CrateDropData.CrateData[iCrate].niCrate))+<<0, 0, 1>>))	//, FALSE)
						SET_BIT(g_sCrateDropF9Data.iBitSet, biCDF9_CHUTE_CREATED)		
						SET_NETWORK_ID_EXISTS_ON_ALL_MACHINES(serverBD.CrateDropData.CrateData[iCrate].niChute, TRUE)
					
						NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - CREATED CHUTE " )NET_PRINT_INT(iCrate) NET_NL()
					
						IF IS_BIT_SET(serverBD.iServerBitSet, biS_SpecialCrateDrop)
							ATTACH_ENTITY_TO_ENTITY(NET_TO_OBJ(serverBD.CrateDropData.CrateData[iCrate].niChute), NET_TO_OBJ(serverBD.CrateDropData.CrateData[iCrate].niCrate), 0, <<0, 0, 0>>, <<0, 0, 0>>)
							SET_ACTIVATE_OBJECT_PHYSICS_AS_SOON_AS_IT_IS_UNFROZEN(NET_TO_OBJ(serverBD.CrateDropData.CrateData[iCrate].niChute), TRUE)
							SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(NET_TO_OBJ(serverBD.CrateDropData.CrateData[iCrate].niChute), FALSE)
							PLAY_ENTITY_ANIM(NET_TO_OBJ(serverBD.CrateDropData.CrateData[iCrate].niChute), serverBD.CrateDropData.CrateData[iCrate].AnimDeploy, serverBD.CrateDropData.CrateData[iCrate].AnimDictionary, INSTANT_BLEND_IN, FALSE, FALSE)
							FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(NET_TO_OBJ(serverBD.CrateDropData.CrateData[iCrate].niChute))
							NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - DEPLOY ANIM ") NET_PRINT(serverBD.CrateDropData.CrateData[iCrate].AnimDeploy) NET_PRINT("STARTED - ") NET_PRINT_INT(iCrate) NET_NL()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.CrateDropData.CrateData[iCrate].niCrate)
	AND NETWORK_DOES_NETWORK_ID_EXIST(serverBD.CrateDropData.CrateData[iCrate].niChute)
		SET_BIT(g_sCrateDropF9Data.iBitSet, biCDF9_CRATE_AND_CHUTE_CREATED)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Creates the Destructible Crate
FUNC BOOL CREATE_DESTRUCTIBLE_CRATE()
	
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_SpecialCrateDrop)
		SET_BIT(g_sCrateDropF9Data.iBitSet, biCDF9_DEST_SKIP_FOR_SPECIAL)
		RETURN TRUE
	ENDIF
	
	INT i
	REPEAT MAX_NUM_CRATE_DROPS i
		REQUEST_MODEL(serverBD.DCrateData[i].Model)
		
		IF HAS_MODEL_LOADED(serverBD.DCrateData[i].Model)
			SET_BIT(g_sCrateDropF9Data.iBitSet, biCDF9_DEST_HAS_LOADED)
			IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.DCrateData[i].NetID)
			AND NETWORK_DOES_NETWORK_ID_EXIST(serverBD.CrateDropData.CrateData[i].niCrate)
			AND NETWORK_DOES_NETWORK_ID_EXIST(serverBD.CrateDropData.CrateData[i].niChute)
				IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.CrateDropData.CrateData[i].niCrate)
					SET_BIT(g_sCrateDropF9Data.iBitSet, biCDF9_DEST_CRATE_EXISTS)
					IF CAN_REGISTER_MISSION_OBJECTS(1)
						SET_BIT(g_sCrateDropF9Data.iBitSet, biCDF9_DEST_CAN_REGISTER)
						IF TAKE_CONTROL_OF_NET_ID(serverBD.CrateDropData.CrateData[i].niCrate)
							SET_BIT(g_sCrateDropF9Data.iBitSet, biCDF9_DEST_CONTROL_CRATE)
							IF CREATE_NET_OBJ(serverBD.DCrateData[i].NetID, serverBD.DCrateData[i].Model, GET_ENTITY_COORDS(NET_TO_OBJ(serverBD.CrateDropData.CrateData[i].niCrate))-<<0, 0, 5>>, TRUE, TRUE, TRUE, FALSE)
								SET_BIT(g_sCrateDropF9Data.iBitSet, biCDF9_DEST_CREATED)			
								SET_NETWORK_ID_EXISTS_ON_ALL_MACHINES(serverBD.DCrateData[i].NetID, TRUE)
								SET_ENTITY_HEADING(NET_TO_OBJ(serverBD.DCrateData[i].NetID), GET_ENTITY_HEADING(NET_TO_OBJ(serverBD.CrateDropData.CrateData[i].niCrate)))
							
								ATTACH_ENTITY_TO_ENTITY(NET_TO_OBJ(serverBD.CrateDropData.CrateData[i].niCrate), NET_TO_OBJ(serverBD.DCrateData[i].NetID), 0, <<0, 0, 0.35>>, <<0, 0, 0>>, TRUE, FALSE, TRUE)
								SET_ACTIVATE_OBJECT_PHYSICS_AS_SOON_AS_IT_IS_UNFROZEN(NET_TO_OBJ(serverBD.DCrateData[i].NetID), TRUE)
								SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(NET_TO_OBJ(serverBD.DCrateData[i].NetID), FALSE)
								ACTIVATE_PHYSICS(NET_TO_OBJ(serverBD.DCrateData[i].NetID))
								SET_ENTITY_VELOCITY(NET_TO_OBJ(serverBD.DCrateData[i].NetID), <<0, 0, -0.2>>)
								//SET_ENTITY_LOAD_COLLISION_FLAG(NET_TO_OBJ(serverBD.DCrateData[i].NetID), TRUE)
							
								//CHUTE
								ATTACH_ENTITY_TO_ENTITY(NET_TO_OBJ(serverBD.CrateDropData.CrateData[i].niChute), NET_TO_OBJ(serverBD.DCrateData[i].NetID), 0, <<0, 0, 0.10>>, <<0, 0, 0>>)
								SET_ACTIVATE_OBJECT_PHYSICS_AS_SOON_AS_IT_IS_UNFROZEN(NET_TO_OBJ(serverBD.CrateDropData.CrateData[i].niChute), TRUE)
								SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(NET_TO_OBJ(serverBD.CrateDropData.CrateData[i].niChute), FALSE)
								PLAY_ENTITY_ANIM(NET_TO_OBJ(serverBD.CrateDropData.CrateData[i].niChute), serverBD.CrateDropData.CrateData[i].AnimDeploy, serverBD.CrateDropData.CrateData[i].AnimDictionary, INSTANT_BLEND_IN, FALSE, FALSE)
								FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(NET_TO_OBJ(serverBD.CrateDropData.CrateData[i].niChute))
								NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - DEPLOY ANIM - STANDARD - ") NET_PRINT(serverBD.CrateDropData.CrateData[i].AnimDeploy) NET_PRINT("STARTED - ") NET_PRINT_INT(i) NET_NL()
								
								SET_ENTITY_NO_COLLISION_ENTITY(NET_TO_OBJ(serverBD.DCrateData[i].NetID), NET_TO_OBJ(serverBD.CrateDropData.CrateData[i].niCrate), FALSE)
								IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.CrateDropData.CrateData[i].niChute)
									SET_ENTITY_NO_COLLISION_ENTITY(NET_TO_OBJ(serverBD.DCrateData[i].NetID), NET_TO_OBJ(serverBD.CrateDropData.CrateData[i].niChute), FALSE)	
								ENDIF
							
								//FREEZE_ENTITY_POSITION(NET_TO_OBJ(serverBD.SafeData.NetID), TRUE)
								//RETAIN_ENTITY_IN_INTERIOR(NET_TO_OBJ(serverBD.SafeData.NetID), GET_INTERIOR_AT_COORDS(SafeCrackData.vSafeCoord))
								NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - CREATE_DESTRUCTIBLE_CRATE BODY DONE - ddd - CRATE = ") NET_PRINT_INT(i) NET_NL()
							ENDIF
						ENDIF
					ELSE
						SET_BIT(g_sCrateDropF9Data.iBitSet, biCDF9_DEST_CANT_REGISTER)
						NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - CREATE_DESTRUCTIBLE_CRATE BODY DONE - COULDN'T REGISTER AN OBJECT FOR THE DESTRUCTIBLE CRATE TO SKIP IT - CRATE = ") NET_PRINT_INT(i) NET_NL()
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	REPEAT MAX_NUM_CRATE_DROPS i
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.DCrateData[i].NetID)
			RETURN FALSE
		ENDIF
	ENDREPEAT
	
	SET_BIT(g_sCrateDropF9Data.iBitSet, biCDF9_DEST_DONE)
	RETURN TRUE
ENDFUNC

///////////////////////////////////////////
///    		ON FOOT PEDS    			///
///////////////////////////////////////////
//Load and Create the Security in the Main Room
FUNC BOOL CREATE_ON_FOOT_PEDS(INT iLocation)
	INT i
		
	REPEAT serverBD.CrateDropData.LocationData[iLocation].iNumPeds i //LOCATION_MAX_NUM_PEDS i
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.CrateDropData.LocationData[iLocation].Peds[i].niPed)
			IF REQUEST_LOAD_MODEL(serverBD.CrateDropData.LocationData[iLocation].Peds[i].PedModel)
				
			 	IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(serverBD.CrateDropData.LocationData[iLocation].Peds[i].vCreationPos)
					IF CREATE_NET_PED(serverBD.CrateDropData.LocationData[iLocation].Peds[i].niPed, PEDTYPE_CRIMINAL, serverBD.CrateDropData.LocationData[iLocation].Peds[i].PedModel, serverBD.CrateDropData.LocationData[iLocation].Peds[i].vCreationPos, serverBD.CrateDropData.LocationData[iLocation].Peds[i].fHeading)
						SET_PED_RELATIONSHIP_GROUP_HASH(NET_TO_PED(serverBD.CrateDropData.LocationData[iLocation].Peds[i].niPed), rgFM_AiHate)//serverBD.CrateDropData.LocationData[iLocation].Peds[i].RelGroup)
						SET_ENTITY_IS_TARGET_PRIORITY(NET_TO_PED(serverBD.CrateDropData.LocationData[iLocation].Peds[i].niPed), TRUE)
						SET_PED_RANDOM_COMPONENT_VARIATION(NET_TO_PED(serverBD.CrateDropData.LocationData[iLocation].Peds[i].niPed))
						
						GIVE_DELAYED_WEAPON_TO_PED(NET_TO_PED(serverBD.CrateDropData.LocationData[iLocation].Peds[i].niPed), WEAPONTYPE_PISTOL, 25000, FALSE)
						GIVE_DELAYED_WEAPON_TO_PED(NET_TO_PED(serverBD.CrateDropData.LocationData[iLocation].Peds[i].niPed), serverBD.CrateDropData.LocationData[iLocation].Peds[i].Weapon, 25000, TRUE)
						SET_PED_ACCURACY(NET_TO_PED(serverBD.CrateDropData.LocationData[iLocation].Peds[i].niPed), serverBD.CrateDropData.LocationData[iLocation].Peds[i].iAccuracy)
						
						//Set Behaviours
						SET_PED_COMBAT_ABILITY(NET_TO_PED(serverBD.CrateDropData.LocationData[iLocation].Peds[i].niPed), CAL_AVERAGE)
						SET_PED_COMBAT_MOVEMENT(NET_TO_PED(serverBD.CrateDropData.LocationData[iLocation].Peds[i].niPed), CM_WILLADVANCE)
						SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(serverBD.CrateDropData.LocationData[iLocation].Peds[i].niPed), CA_USE_COVER, TRUE)
						SET_PED_TARGET_LOSS_RESPONSE(NET_TO_PED(serverBD.CrateDropData.LocationData[iLocation].Peds[i].niPed), TLR_NEVER_LOSE_TARGET)
						SET_PED_HIGHLY_PERCEPTIVE(NET_TO_PED(serverBD.CrateDropData.LocationData[iLocation].Peds[i].niPed), TRUE)
						
						//IF serverBD.CrateDropData.LocationData[iLocation].Peds[i].fDefensiveRadius > 0
						//	SET_PED_SPHERE_DEFENSIVE_AREA(NET_TO_PED(serverBD.CrateDropData.LocationData[iLocation].Peds[i].niPed), serverBD.CrateDropData.LocationData[iLocation].Peds[i].vDefensivePos, serverBD.CrateDropData.LocationData[iLocation].Peds[i].fDefensiveRadius+25)
						//ENDIF
						
						SET_ENTITY_HEALTH(NET_TO_PED(serverBD.CrateDropData.LocationData[iLocation].Peds[i].niPed), ROUND(200*g_sMPTunables.fAiHealthModifier))
						
						//SET_PED_SPHERE_DEFENSIVE_AREA(NET_TO_PED(serverBD.CrateDropData.LocationData[iLocation].Peds[i].niPed), serverBD.CrateDropData.LocationData[iLocation].vLocation, 50)
						
						SET_PED_AS_ENEMY(NET_TO_PED(serverBD.CrateDropData.LocationData[iLocation].Peds[i].niPed), TRUE)
							
						SET_PED_KEEP_TASK(NET_TO_PED(serverBD.CrateDropData.LocationData[iLocation].Peds[i].niPed), TRUE)
						TASK_COMBAT_HATED_TARGETS_IN_AREA(NET_TO_PED(serverBD.CrateDropData.LocationData[iLocation].Peds[i].niPed), serverBD.CrateDropData.LocationData[iLocation].vLocation, 150)
						
						NET_PRINT_TIME() NET_PRINT("     ---------->     ON FOOT PED CREATED - LOCATION ") NET_PRINT_INT(iLocation) NET_PRINT(" PED ") NET_PRINT_INT(i) NET_NL()
					ENDIF
				ENDIF
				
			ENDIF
		ENDIF
	ENDREPEAT

	//Check we have created all Peds
	REPEAT serverBD.CrateDropData.LocationData[iLocation].iNumPeds i //LOCATION_MAX_NUM_PEDS i
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.CrateDropData.LocationData[iLocation].Peds[i].niPed)
			RETURN FALSE
		ENDIF
	ENDREPEAT
	
	//SET_BIT(serverBD.iServerBitSet2, biS2_MainRoomPedsCreated)
	RETURN TRUE
ENDFUNC

///////////////////////////////////////////
///    		VEHICLE AND PEDS   			///
///////////////////////////////////////////
//PURPOSE: Create the Vehicle
FUNC BOOL CREATE_AI_VEHICLE(INT iLocation)
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.CrateDropData.LocationData[iLocation].Vehicle[0].niVeh)
		IF REQUEST_LOAD_MODEL(serverBD.CrateDropData.LocationData[iLocation].Vehicle[0].VehModel)
			VECTOR vSpawn
			FLOAT fHeading
			BOOL bCreationOkay
			
			//Check if creation point is okay
			INT i
			REPEAT MAX_NUM_VEH_SPAWN_POINTS i
				IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(serverBD.CrateDropData.LocationData[iLocation].Vehicle[0].vCreationPos[i])
					vSpawn = serverBD.CrateDropData.LocationData[iLocation].Vehicle[0].vCreationPos[i]
					fHeading = serverBD.CrateDropData.LocationData[iLocation].Vehicle[0].fHeading[i]
					bCreationOkay = TRUE
					i = MAX_NUM_VEH_SPAWN_POINTS
					NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - CREATE_VEHICLE - bCreationOkay = TRUE ") NET_NL()
				ENDIF
			ENDREPEAT
			
			IF bCreationOkay = TRUE
				IF CREATE_NET_VEHICLE(serverBD.CrateDropData.LocationData[iLocation].Vehicle[0].niVeh, serverBD.CrateDropData.LocationData[iLocation].Vehicle[0].VehModel, vSpawn, fHeading)	
					
					//SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(NET_TO_VEH(serverBD.CrateDropData.LocationData[iLocation].Vehicle[0].niVeh), TRUE)
					//SET_VEHICLE_TYRES_CAN_BURST(NET_TO_VEH(serverBD.CrateDropData.LocationData[iLocation].Vehicle[0].niVeh), FALSE)
					
					SET_ENTITY_HEALTH(NET_TO_VEH(serverBD.CrateDropData.LocationData[iLocation].Vehicle[0].niVeh), serverBD.CrateDropData.LocationData[iLocation].Vehicle[0].iHealth)
					SET_VEHICLE_ENGINE_HEALTH(NET_TO_VEH(serverBD.CrateDropData.LocationData[iLocation].Vehicle[0].niVeh), TO_FLOAT(serverBD.CrateDropData.LocationData[iLocation].Vehicle[0].iHealth))
					SET_VEHICLE_PETROL_TANK_HEALTH(NET_TO_VEH(serverBD.CrateDropData.LocationData[iLocation].Vehicle[0].niVeh), TO_FLOAT(serverBD.CrateDropData.LocationData[iLocation].Vehicle[0].iHealth))
					
					//SET_VEHICLE_DOORS_LOCKED(NET_TO_VEH(serverBD.CrateDropData.LocationData[iLocation].Vehicle[0].niVeh), VEHICLELOCK_LOCKED)
					
					IF IS_THIS_MODEL_A_HELI(serverBD.CrateDropData.LocationData[iLocation].Vehicle[0].VehModel)
						SET_HELI_BLADES_FULL_SPEED(NET_TO_VEH(serverBD.CrateDropData.LocationData[iLocation].Vehicle[0].niVeh))
					ENDIF
					SET_VEHICLE_ENGINE_ON(NET_TO_VEH(serverBD.CrateDropData.LocationData[iLocation].Vehicle[0].niVeh), TRUE, TRUE)
					
					NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - CREATED VEHICLE  - Location ") NET_PRINT_VECTOR(vSpawn) NET_NL()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//Check we have created the vehicle
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.CrateDropData.LocationData[iLocation].Vehicle[0].niVeh)
		RETURN FALSE
	ENDIF
	
	SET_MODEL_AS_NO_LONGER_NEEDED(serverBD.CrateDropData.LocationData[iLocation].Vehicle[0].VehModel)
	RETURN TRUE
ENDFUNC

//PURPOSE: Load and Create the Peds in the Vehicle
FUNC BOOL CREATE_IN_VEHICLE_PEDS(INT iLocation)
	
	IF IS_NET_VEHICLE_DRIVEABLE(serverBD.CrateDropData.LocationData[iLocation].Vehicle[0].niVeh)
		IF TAKE_CONTROL_OF_NET_ID(serverBD.CrateDropData.LocationData[iLocation].Vehicle[0].niVeh)
			INT iLoop = (GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(NET_TO_VEH(serverBD.CrateDropData.LocationData[iLocation].Vehicle[0].niVeh)) + 1)
			IF iLoop > serverBD.CrateDropData.LocationData[iLocation].iNumPeds		//LOCATION_MAX_NUM_PEDS
				iLoop = serverBD.CrateDropData.LocationData[iLocation].iNumPeds 	//LOCATION_MAX_NUM_PEDS
			ENDIF
			
			INT i	
			REPEAT iLoop i
				IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.CrateDropData.LocationData[iLocation].Peds[i].niPed)
					IF REQUEST_LOAD_MODEL(serverBD.CrateDropData.LocationData[iLocation].Peds[i].PedModel)
						
						IF CREATE_NET_PED_IN_VEHICLE(serverBD.CrateDropData.LocationData[iLocation].Peds[i].niPed, serverBD.CrateDropData.LocationData[iLocation].Vehicle[0].niVeh, PEDTYPE_CRIMINAL, serverBD.CrateDropData.LocationData[iLocation].Peds[i].PedModel, INT_TO_ENUM(VEHICLE_SEAT, i-1))
							SET_PED_RELATIONSHIP_GROUP_HASH(NET_TO_PED(serverBD.CrateDropData.LocationData[iLocation].Peds[i].niPed), rgFM_AiHate)	//SET_PED_RELATIONSHIP_GROUP_HASH(NET_TO_PED(serverBD.CrateDropData.LocationData[iLocation].Peds[i].niPed), serverBD.CrateDropData.LocationData[iLocation].Peds[i].RelGroup)
							SET_ENTITY_IS_TARGET_PRIORITY(NET_TO_PED(serverBD.CrateDropData.LocationData[iLocation].Peds[i].niPed), TRUE)
							SET_PED_RANDOM_COMPONENT_VARIATION(NET_TO_PED(serverBD.CrateDropData.LocationData[iLocation].Peds[i].niPed))
							
							GIVE_DELAYED_WEAPON_TO_PED(NET_TO_PED(serverBD.CrateDropData.LocationData[iLocation].Peds[i].niPed), WEAPONTYPE_PISTOL, 25000, FALSE)
							GIVE_DELAYED_WEAPON_TO_PED(NET_TO_PED(serverBD.CrateDropData.LocationData[iLocation].Peds[i].niPed), serverBD.CrateDropData.LocationData[iLocation].Peds[i].Weapon, 25000, TRUE)
							SET_PED_ACCURACY(NET_TO_PED(serverBD.CrateDropData.LocationData[iLocation].Peds[i].niPed), serverBD.CrateDropData.LocationData[iLocation].Peds[i].iAccuracy)
							
							//Set Behaviours
							SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(serverBD.CrateDropData.LocationData[iLocation].Peds[i].niPed), CA_DO_DRIVEBYS, TRUE)
							SET_PED_COMBAT_ABILITY(NET_TO_PED(serverBD.CrateDropData.LocationData[iLocation].Peds[i].niPed), CAL_AVERAGE)
							SET_PED_COMBAT_MOVEMENT(NET_TO_PED(serverBD.CrateDropData.LocationData[iLocation].Peds[i].niPed), CM_WILLADVANCE)
							SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(serverBD.CrateDropData.LocationData[iLocation].Peds[i].niPed), CA_USE_COVER, TRUE)
							SET_PED_TARGET_LOSS_RESPONSE(NET_TO_PED(serverBD.CrateDropData.LocationData[iLocation].Peds[i].niPed), TLR_NEVER_LOSE_TARGET)
							SET_PED_HIGHLY_PERCEPTIVE(NET_TO_PED(serverBD.CrateDropData.LocationData[iLocation].Peds[i].niPed), TRUE)
							
							SET_ENTITY_HEALTH(NET_TO_PED(serverBD.CrateDropData.LocationData[iLocation].Peds[i].niPed), ROUND(200*g_sMPTunables.fAiHealthModifier))
							
							SET_PED_AS_ENEMY(NET_TO_PED(serverBD.CrateDropData.LocationData[iLocation].Peds[i].niPed), TRUE)
							
							SET_PED_KEEP_TASK(NET_TO_PED(serverBD.CrateDropData.LocationData[iLocation].Peds[i].niPed), TRUE)
							TASK_COMBAT_HATED_TARGETS_IN_AREA(NET_TO_PED(serverBD.CrateDropData.LocationData[iLocation].Peds[i].niPed), serverBD.CrateDropData.LocationData[iLocation].vLocation, 250)
							
							NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - CREATED PED IN VEHICLE - LOCATION ") NET_PRINT_INT(iLocation) NET_PRINT(" PED ") NET_PRINT_INT(i) NET_NL()
						ENDIF
						
					ENDIF
				ENDIF
			ENDREPEAT
			
			//Check we have created all Peds
			REPEAT iLoop i
				IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.CrateDropData.LocationData[iLocation].Peds[i].niPed)
					RETURN FALSE
				ENDIF
			ENDREPEAT
			
			//SET_BIT(serverBD.iServerBitSet2, biS2_MainRoomPedsCreated)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Creates the Vehicle and the Peds
FUNC BOOL CREATE_VEHICLE_AND_PEDS(INT iLocation)
	IF CAN_REGISTER_MISSION_ENTITIES(4, 1, 0, 0)
		IF CREATE_AI_VEHICLE(iLocation)
		AND CREATE_IN_VEHICLE_PEDS(iLocation)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

///////////////////////////////////////////
///    		LOCAL FLARE CANISTER		///
///////////////////////////////////////////  
//Creates a local flare canister
FUNC BOOL CREATE_FLARE_CANISTER()
	INT i
	REQUEST_MODEL(FlareCanisterModel)
		
	IF HAS_MODEL_LOADED(FlareCanisterModel)
				
		//Create the Object
		REPEAT serverBD.CrateDropData.iNumDrops i
			IF NOT DOES_ENTITY_EXIST(FlareCanister[i])					
				CLEAR_AREA(serverBD.CrateDropData.LocationData[i].vLocation, 1.0, FALSE, TRUE)
				FlareCanister[i] = CREATE_OBJECT(FlareCanisterModel, serverBD.CrateDropData.LocationData[i].vLocation, FALSE, FALSE)
					
				SET_ENTITY_INVINCIBLE(FlareCanister[i], TRUE)
				FREEZE_ENTITY_POSITION(FlareCanister[i], TRUE)
				
				NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - CREATED FLARE CANISTER ") NET_PRINT_INT(i) NET_NL()
			ENDIF
		ENDREPEAT
		
	ENDIF
	
	REPEAT serverBD.CrateDropData.iNumDrops i
		IF NOT DOES_ENTITY_EXIST(FlareCanister[i])	
			RETURN FALSE
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

//PURPOSE: Removes a flare
PROC CLEANUP_CRATE_FLARE(INT iCrate)
	IF serverBD.CrateDropData.CrateData[iCrate].ptfxFlare <> NULL
		IF DOES_PARTICLE_FX_LOOPED_EXIST(serverBD.CrateDropData.CrateData[iCrate].ptfxFlare)
			STOP_PARTICLE_FX_LOOPED(serverBD.CrateDropData.CrateData[iCrate].ptfxFlare)
			NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - CLEANUP_CRATE_FLARE ") NET_PRINT_INT(iCrate) NET_NL()
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Loops through and cleans up all flares
PROC CLEANUP_ALL_CRATE_FLARES()
	INT i
	REPEAT MAX_NUM_CRATE_DROPS i
		CLEANUP_CRATE_FLARE(i)
	ENDREPEAT
ENDPROC


//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  MISC PROCS                  //////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////
///    		DROPPING CRATES    			///
///////////////////////////////////////////
//PURPOSE: Controls when the crates should be dropped
PROC CONTROL_CRATE_DROPPING(INT iCrate)
	//Check if drop should be done
	IF NOT IS_BIT_SET(serverBD.CrateDropData.CrateData[iCrate].iBitSet, biCD_Crate_ReadyForDrop)
		/*IF IS_NET_VEHICLE_DRIVEABLE(serverBD.CrateDropData.PlaneData.Plane.niVeh)
			IF IS_ENTITY_AT_COORD(NET_TO_VEH(serverBD.CrateDropData.PlaneData.Plane.niVeh), serverBD.CrateDropData.LocationData[iCrate].vLocation, <<PLANE_START_DROP, PLANE_START_DROP, 600>>)
				SET_BIT(serverBD.CrateDropData.CrateData[iCrate].iBitSet, biCD_Crate_ReadyForDrop)
				NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - CONTROL_CRATE_DROPPING - biCD_Crate_ReadyForDrop SET ") NET_PRINT_INT(iCrate) NET_NL()
			ELIF NOT IS_BIT_SET(serverBD.CrateDropData.CrateData[iCrate].iBitSet, biCD_Crate_SlowDownForDrop)
				IF IS_ENTITY_AT_COORD(NET_TO_VEH(serverBD.CrateDropData.PlaneData.Plane.niVeh), serverBD.CrateDropData.LocationData[iCrate].vLocation, <<PLANE_SLOW_RADIUS, PLANE_SLOW_RADIUS, 600>>)
					IF NOT IS_NET_PED_INJURED(serverBD.CrateDropData.PlaneData.Pilot.niPed)
						IF IS_PED_IN_VEHICLE(NET_TO_PED(serverBD.CrateDropData.PlaneData.Pilot.niPed), NET_TO_VEH(serverBD.CrateDropData.PlaneData.Plane.niVeh))
							SET_DRIVE_TASK_CRUISE_SPEED(NET_TO_PED(serverBD.CrateDropData.PlaneData.Pilot.niPed), PLANE_SLOW_SPEED)
							SET_BIT(serverBD.CrateDropData.CrateData[iCrate].iBitSet, biCD_Crate_SlowDownForDrop)
							NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - CONTROL_CRATE_DROPPING - biCD_Crate_SlowDownForDrop SET ") NET_PRINT_INT(iCrate) NET_NL()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF*/
		IF HAS_NET_TIMER_EXPIRED(serverBD.CrateDeliveryTimer, CRATE_DELIVERY_TIME)
			SET_BIT(serverBD.CrateDropData.CrateData[iCrate].iBitSet, biCD_Crate_ReadyForDrop)
			NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - CONTROL_CRATE_DROPPING - biCD_Crate_ReadyForDrop SET ") NET_PRINT_INT(iCrate) NET_NL()
		ENDIF
		
	//Do Drop
	ELSE
		IF NOT IS_BIT_SET(serverBD.CrateDropData.CrateData[iCrate].iBitSet, biCD_Crate_DropDone)
			IF CREATE_DROPPED_CRATE(iCrate)
				IF CREATE_DESTRUCTIBLE_CRATE()
					SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerEventData
					IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_SpecialCrateDrop)
						TickerEventData.TickerEvent = TICKER_EVENT_CRATE_DROP_DROPPED
					ELSE
						TickerEventData.TickerEvent = TICKER_EVENT_CRATE_DROP_DROPPED_DLC
					ENDIF
					BROADCAST_TICKER_EVENT(TickerEventData, ALL_PLAYERS_ON_SCRIPT())
					
					ADD_CRATE_BEACON(iCrate)
					
					PLAYSTATS_CRATE_CREATED(serverBD.CrateDropData.LocationData[iCrate].vLocation.x, serverBD.CrateDropData.LocationData[iCrate].vLocation.y, serverBD.CrateDropData.LocationData[iCrate].vLocation.z)
					PRINTLN("     ---------->    CRATE DROP - CONTROL_CRATE_DROPPING - PLAYSTATS_CRATE_CREATED CALLED WITH ", serverBD.CrateDropData.LocationData[iCrate].vLocation)
					
					SET_BIT(serverBD.CrateDropData.CrateData[iCrate].iBitSet, biCD_Crate_DropDone)
					serverBD.iDropsDone++
					//CLEAR_PLANE_TASK()
					NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - CONTROL_CRATE_DROPPING - biCD_Crate_DropDone SET ") NET_PRINT_INT(iCrate) NET_NL()
				ENDIF
			ENDIF
			
			//TIMEOUT CLEANUP
			IF NOT IS_BIT_SET(serverBD.CrateDropData.CrateData[iCrate].iBitSet, biCD_Crate_DropDone)
				IF HAS_NET_TIMER_EXPIRED(serverBD.CrateCreationTimeoutTimer, 60000)
					CLEANUP_ALL_CRATE_FLARES()
					serverBD.eCrateDropStage = eCD_CLEANUP
					NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - SERVER STAGE = eCD_CLEANUP - TIMEOUT - CRATE AND CHUTE CREATION GOT STUCK    <----------     ") NET_NL()
					
					SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerEventData
					TickerEventData.TickerEvent = TICKER_EVENT_CRATE_DROP_TIMEOUT
					BROADCAST_TICKER_EVENT(TickerEventData, ALL_PLAYERS_ON_SCRIPT())
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	///BIT SET CHECKS
	//Set all drops as Done or Not (if Plane is not driveable then all drops have been done)
	//IF IS_NET_VEHICLE_DRIVEABLE(serverBD.CrateDropData.PlaneData.Plane.niVeh)
		IF NOT IS_BIT_SET(serverBD.CrateDropData.CrateData[iCrate].iBitSet, biCD_Crate_DropDone)
			CLEAR_BIT(serverBD.CrateDropData.CrateData[0].iBitSet,  biCD_Crate_AllCratesDropped)
		ENDIF
	/*ELSE
		IF NOT IS_BIT_SET(serverBD.CrateDropData.CrateData[0].iBitSet,  biCD_Crate_AllCratesDropped)
			SET_BIT(serverBD.CrateDropData.CrateData[0].iBitSet,  biCD_Crate_AllCratesDropped)
		ENDIF
	ENDIF*/
	
	//Set all Crates picked up or not
	IF serverBD.CrateDropData.CrateData[iCrate].iParticpantGotCrate = -1	//IF NOT IS_BIT_SET(serverBD.CrateDropData.CrateData[iCrate].iBitSet, biCD_Crate_PickedUp)
		CLEAR_BIT(serverBD.CrateDropData.CrateData[0].iBitSet,  biCD_Crate_AllCratesPickedUp)
	ENDIF
	//Set all Crates exist or not
	IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.CrateDropData.CrateData[iCrate].niCrate)
		CLEAR_BIT(serverBD.CrateDropData.CrateData[0].iBitSet,  biCD_Crate_NoCratesExist)
	ENDIF
ENDPROC

///////////////////////////////////////////
///    		CRATE TIMEOUT    			///
///////////////////////////////////////////	

//PURPOSE: Controls when and if the crates should be cleaned up from a timeout
PROC CONTROL_CRATE_TIMEOUT(INT iCrate)
	
	IF serverBD.CrateDropData.CrateData[iCrate].iParticpantGotCrate = -1
	//AND NOT IS_BIT_SET(serverBD.CrateDropData.CrateData[iCrate].iBitSet, biCD_Crate_PickedUp)
		IF IS_BIT_SET(serverBD.CrateDropData.CrateData[iCrate].iBitSet, biCD_Crate_DropDone)
			
			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_SpecialCrateDrop)
				IF serverBD.CrateDropData.CrateData[iCrate].iCleanupTimerDelay != GET_CRATE_STANDARD_CLEANUP_TIME()
				AND serverBD.CrateDropData.CrateData[iCrate].iCleanupTimerDelay != GET_CRATE_ADDITIONAL_CLEANUP_TIME()
					serverBD.CrateDropData.CrateData[iCrate].iCleanupTimerDelay = GET_CRATE_STANDARD_CLEANUP_TIME()
					NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - CONTROL_CRATE_TIMEOUT - ALTER DELAY TO NORMAL ") NET_PRINT_INT(iCrate) NET_NL()
				ENDIF
			ELSE
				IF serverBD.CrateDropData.CrateData[iCrate].iCleanupTimerDelay != GET_CRATE_SPECIAL_CLEANUP_TIME()
					serverBD.CrateDropData.CrateData[iCrate].iCleanupTimerDelay = GET_CRATE_SPECIAL_CLEANUP_TIME()
					NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - CONTROL_CRATE_TIMEOUT - ALTER DELAY TO SPECIAL ") NET_PRINT_INT(iCrate) NET_NL()
				ENDIF
			ENDIF
			
			IF HAS_NET_TIMER_EXPIRED(serverBD.CrateDropData.CrateData[iCrate].iCleanupTimer, serverBD.CrateDropData.CrateData[iCrate].iCleanupTimerDelay)
				IF IS_BIT_SET(serverBD.CrateDropData.CrateData[iCrate].iBitSet, biCD_Crate_NoPlayerAtDropLocation)
				OR IS_BIT_SET(serverBD.iServerBitSet, biS_InAdditionalTime)
					CLEANUP_CRATE_BEACON(iCrate)
					
					SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerEventData
					TickerEventData.TickerEvent = TICKER_EVENT_CRATE_DROP_AI_PICKUP
					BROADCAST_TICKER_EVENT(TickerEventData, ALL_PLAYERS_ON_SCRIPT())
					
					//SET_BIT(serverBD.CrateDropData.CrateData[iCrate].iBitSet, biCD_Crate_PickedUp)
					serverBD.CrateDropData.CrateData[iCrate].iParticpantGotCrate = 9999
					NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - CONTROL_CRATE_TIMEOUT - DONE ") NET_PRINT_INT(iCrate) NET_NL()
				ELSE
					RESET_NET_TIMER(serverBD.CrateDropData.CrateData[iCrate].iCleanupTimer)
					serverBD.CrateDropData.CrateData[iCrate].iCleanupTimerDelay = GET_CRATE_ADDITIONAL_CLEANUP_TIME()
					NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - CONTROL_CRATE_TIMEOUT - RESET TIME A - DELAY = GET_CRATE_ADDITIONAL_CLEANUP_TIME() ") NET_PRINT_INT(iCrate) NET_NL()
					SET_BIT(serverBD.iServerBitSet, biS_InAdditionalTime)
				ENDIF
			ENDIF
		ENDIF
		SET_BIT(serverBD.CrateDropData.CrateData[iCrate].iBitSet, biCD_Crate_NoPlayerAtDropLocation)
	ENDIF
	
ENDPROC




//PURPOSE: Controls if this player should delete the crate
PROC CONTROL_CRATE_DELETION(INT iCrate)
	//IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.CrateDropData.CrateData[iCrate].niCrate)
		IF serverBD.CrateDropData.CrateData[iCrate].iParticpantGotCrate != -1	//IF IS_BIT_SET(serverBD.CrateDropData.CrateData[iCrate].iBitSet, biCD_Crate_PickedUp)
			//IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.CrateDropData.CrateData[iCrate].niCrate)
			//OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.CrateDropData.CrateData[iCrate].niCrate) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
				IF TAKE_CONTROL_OF_NET_ID(serverBD.CrateDropData.CrateData[iCrate].niCrate)
					//CLEANUP_CRATE_BLEEP_SOUND(iCrate)
					DELETE_NET_ID(serverBD.CrateDropData.CrateData[iCrate].niCrate)
					NET_PRINT_TIME() NET_PRINT("  ---->  CRATE DROP - CRATE DELETED - Crate ") NET_PRINT_INT(iCrate) NET_NL()
				ENDIF
			//ENDIF
		ENDIF
	//ENDIF
ENDPROC

//PURPOSE:
FUNC BOOL ROTATION_IS_TOO_FAR(INT iCrate)
	FLOAT fRot = GET_ENTITY_PITCH(NET_TO_OBJ(serverBD.CrateDropData.CrateData[iCrate].niCrate))
	
	IF fRot > 10
	OR fRot < -10
		NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - ROTATION_IS_TOO_FAR - PITCH") NET_NL()
		RETURN TRUE
	ENDIF
	
	fRot = GET_ENTITY_ROLL(NET_TO_OBJ(serverBD.CrateDropData.CrateData[iCrate].niCrate))
	IF fRot > 10
	OR fRot < -10
		NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - ROTATION_IS_TOO_FAR - ROLL") NET_NL()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

///////////////////////////////////////////
///    		CRATE LANDING    			///
///////////////////////////////////////////
//PURPOSE: Plays an anim when the package lands
FUNC BOOL SHOULD_LAND_ANIM_START(INT iCrate)
	IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_SpecialCrateDrop)
	
		BOOL bIsInAir, bIsInWater, bIsCrateBroken, bIsRotationTooFar
	
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.DCrateData[iCrate].NetID)
			IF NOT IS_ENTITY_IN_AIR(NET_TO_OBJ(serverBD.DCrateData[iCrate].NetID))
			bIsInAir = FALSE
			#IF IS_DEBUG_BUILD
			VECTOR vTemp = GET_ENTITY_COORDS(NET_TO_OBJ(serverBD.DCrateData[iCrate].NetID))
			PRINTLN("     ---------->    CRATE DROP - SHOULD_LAND_ANIM_START - A - NOT IS_ENTITY_IN_AIR ", iCrate, " - ", vTemp)
			#ENDIF
		ELSE
			bIsInAir = TRUE
		ENDIF
			
			IF IS_ENTITY_IN_WATER(NET_TO_OBJ(serverBD.DCrateData[iCrate].NetID))
			bIsInWater = TRUE
			NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - SHOULD_LAND_ANIM_START - A - IS_ENTITY_IN_WATER ") NET_PRINT_INT(iCrate) NET_NL()
		ENDIF
		
		IF HAS_OBJECT_BEEN_BROKEN(NET_TO_OBJ(serverBD.DCrateData[iCrate].NetID))
			bIsCrateBroken = TRUE
			NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - SHOULD_LAND_ANIM_START - A - CRATE BROKEN ") NET_PRINT_INT(iCrate) NET_NL()
		ENDIF
		ENDIF
		
		IF ROTATION_IS_TOO_FAR(iCrate)
			bIsRotationTooFar = TRUE
			NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - SHOULD_LAND_ANIM_START - A - ROTATED TOO FAR ") NET_PRINT_INT(iCrate) NET_NL()
		ENDIF

		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.DCrateData[iCrate].NetID)
		OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.DCrateData[iCrate].NetID) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
			IF NOT bIsInAir
			OR bIsInWater
			OR bIsCrateBroken
			OR bIsRotationTooFar
				IF TAKE_CONTROL_OF_NET_ID(serverBD.DCrateData[iCrate].NetID)
					NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - SHOULD_LAND_ANIM_START - TRUE - A") NET_NL()
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_ENTITY_IN_AIR(NET_TO_OBJ(serverBD.CrateDropData.CrateData[iCrate].niCrate))
		OR IS_ENTITY_IN_WATER(NET_TO_OBJ(serverBD.CrateDropData.CrateData[iCrate].niCrate))
		OR ROTATION_IS_TOO_FAR(iCrate)
			NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - SHOULD_LAND_ANIM_START - TRUE - B") NET_NL()
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Plays an anim when the package lands
PROC CONTROL_CRATE_LANDING(INT iCrate, VECTOR vForceDropLocation)
	
	VECTOR vCratePos
	
	//CRATE CHECKS
	IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.CrateDropData.CrateData[iCrate].niCrate)
				
		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.CrateDropData.CrateData[iCrate].niCrate)
		OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.CrateDropData.CrateData[iCrate].niCrate) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
			
			//CHUTE CHECKS
			IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.CrateDropData.CrateData[iCrate].niChute)
				
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.CrateDropData.CrateData[iCrate].niChute)
				OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.CrateDropData.CrateData[iCrate].niChute) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
					
					IF TAKE_CONTROL_OF_NET_ID(serverBD.CrateDropData.CrateData[iCrate].niCrate)
					AND TAKE_CONTROL_OF_NET_ID(serverBD.CrateDropData.CrateData[iCrate].niChute)
						
						//Force Crate to Ground
						IF NOT IS_BIT_SET(serverBD.CrateDropData.CrateData[iCrate].iBitSet, biCD_Crate_LandAnimStarted)
						AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_LandAnimStarted+iCrate)
							
							OBJECT_INDEX thisObj
							BOOL bHasControl
							IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_SpecialCrateDrop)
								//Make sure we have control of Destructible Crate
								IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.DCrateData[iCrate].NetID)
									IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.DCrateData[iCrate].NetID)
									OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.DCrateData[iCrate].NetID) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
										IF TAKE_CONTROL_OF_NET_ID(serverBD.DCrateData[iCrate].NetID)
											bHasControl = TRUE
										ENDIF
									ENDIF
									thisObj = NET_TO_OBJ(serverBD.DCrateData[iCrate].NetID)
								ENDIF
								
							ELSE IS_BIT_SET(serverBD.iServerBitSet, biS_SpecialCrateDrop)
								thisObj = NET_TO_OBJ(serverBD.CrateDropData.CrateData[iCrate].niCrate)
								bHasControl = TRUE
							ENDIF
							IF IS_BIT_SET(serverBD.CrateDropData.CrateData[iCrate].iBitSet, biCD_Crate_NoPlayerAtDropLocation)
							AND IS_ENTITY_STATIC(thisObj)
							AND bHasControl = TRUE
								SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(NET_TO_OBJ(serverBD.CrateDropData.CrateData[iCrate].niCrate), TRUE)
								SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(NET_TO_OBJ(serverBD.DCrateData[iCrate].NetID), TRUE)
								//SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(thisObj, TRUE)
								SET_ENTITY_VELOCITY(thisObj, <<0, 0, 0>>)
								
								IF ARE_VECTORS_EQUAL(vForceDropLocation, <<0, 0, 0>>)
									VECTOR vMin, vMax
									GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(thisObj), vMin, vMax)
									IF GET_RANDOM_BOOL()
										vMin.x = ABSF((vMin.x*2))
									ELSE
										vMin.x = -ABSF((vMin.x*2))
									ENDIF
									IF GET_RANDOM_BOOL()
										vMin.y = ABSF((vMin.y*2))
									ELSE
										vMin.y = -ABSF((vMin.y*2))
									ENDIF
									vMin.z = ABSF(vMin.z)
									SET_ENTITY_COORDS(thisObj, serverBD.CrateDropData.LocationData[iCrate].vLocation+vMin)	//<<0, ABSF(vMin.z), ABSF(vMin.z)>>)
									NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - CONTROL_CRATE_LANDING - A - FORCE TO GROUND ") NET_PRINT_INT(iCrate) NET_PRINT(" AT POS ") NET_PRINT_VECTOR((serverBD.CrateDropData.LocationData[iCrate].vLocation+vMin))  NET_NL()
								ELSE
									SET_ENTITY_COORDS(thisObj, vForceDropLocation)
									NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - CONTROL_CRATE_LANDING - B - FORCE TO GROUND ") NET_PRINT_INT(iCrate) NET_PRINT(" AT POS ") NET_PRINT_VECTOR(vForceDropLocation)  NET_NL()
								ENDIF
								SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_LandAnimDone+iCrate)
								SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_LandAnimPlaying+iCrate)
								SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_LandAnimStarted+iCrate)
								DELETE_NET_ID(serverBD.CrateDropData.CrateData[iCrate].niChute)
								PREVENT_COLLECTION_OF_PORTABLE_PICKUP(NET_TO_OBJ(serverBD.CrateDropData.CrateData[iCrate].niCrate), FALSE)
								NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - CONTROL_CRATE_LANDING - PREVENT_COLLECTION_OF_PORTABLE_PICKUP - FALSE - A - ") NET_PRINT_INT(iCrate) NET_NL()
							ENDIF
						ENDIF
						
						//Delete Chute when Crumple is done
						IF NOT IS_BIT_SET(serverBD.CrateDropData.CrateData[iCrate].iBitSet, biCD_Crate_LandDone)
						AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_LandAnimDone+iCrate)
							IF IS_BIT_SET(serverBD.CrateDropData.CrateData[iCrate].iBitSet, biCD_Crate_LandAnimPlaying)
							OR IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_LandAnimPlaying+iCrate)
								//IF HAS_ENTITY_ANIM_FINISHED(NET_TO_OBJ(serverBD.CrateDropData.CrateData[iCrate].niChute), serverBD.CrateDropData.CrateData[iCrate].AnimDictionary, serverBD.CrateDropData.CrateData[iCrate].AnimCrumple)
								IF NOT IS_ENTITY_PLAYING_ANIM(NET_TO_OBJ(serverBD.CrateDropData.CrateData[iCrate].niChute), serverBD.CrateDropData.CrateData[iCrate].AnimDictionary,serverBD.CrateDropData.CrateData[iCrate].AnimCrumple)
									SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_LandAnimDone+iCrate)
									//SET_BIT(serverBD.CrateDropData.CrateData[iCrate].iBitSet, biCD_Crate_LandDone)
									DELETE_NET_ID(serverBD.CrateDropData.CrateData[iCrate].niChute)
									PREVENT_COLLECTION_OF_PORTABLE_PICKUP(NET_TO_OBJ(serverBD.CrateDropData.CrateData[iCrate].niCrate), FALSE)
									NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - CONTROL_CRATE_LANDING - PREVENT_COLLECTION_OF_PORTABLE_PICKUP - FALSE - B - ") NET_PRINT_INT(iCrate) NET_NL()
									NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - CONTROL_CRATE_LANDING - LAND ANIM DONE - CHUTE DELETED - ") NET_PRINT_INT(iCrate) NET_NL()
								ENDIF
							ENDIF
						ENDIF
						//Check Crumple is playing
						IF NOT IS_BIT_SET(serverBD.CrateDropData.CrateData[iCrate].iBitSet, biCD_Crate_LandAnimPlaying)
						AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_LandAnimPlaying+iCrate)
							IF IS_BIT_SET(serverBD.CrateDropData.CrateData[iCrate].iBitSet, biCD_Crate_LandAnimStarted)
							OR IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_LandAnimStarted+iCrate)
								IF IS_ENTITY_PLAYING_ANIM(NET_TO_OBJ(serverBD.CrateDropData.CrateData[iCrate].niChute), serverBD.CrateDropData.CrateData[iCrate].AnimDictionary,serverBD.CrateDropData.CrateData[iCrate].AnimCrumple)
									SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_LandAnimPlaying+iCrate)
									//SET_BIT(serverBD.CrateDropData.CrateData[iCrate].iBitSet, biCD_Crate_LandAnimStarted)
									NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - CONTROL_CRATE_LANDING - LAND ANIM ") NET_PRINT(serverBD.CrateDropData.CrateData[iCrate].AnimCrumple) NET_PRINT(" PLAYING - ") NET_PRINT_INT(iCrate) NET_NL()
								ENDIF
							ENDIF
						ENDIF
						//Start Crumple
						IF NOT IS_BIT_SET(serverBD.CrateDropData.CrateData[iCrate].iBitSet, biCD_Crate_LandAnimStarted)
						AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_LandAnimStarted+iCrate)
							IF SHOULD_LAND_ANIM_START(iCrate)
								PLAY_ENTITY_ANIM(NET_TO_OBJ(serverBD.CrateDropData.CrateData[iCrate].niChute), serverBD.CrateDropData.CrateData[iCrate].AnimCrumple, serverBD.CrateDropData.CrateData[iCrate].AnimDictionary, INSTANT_BLEND_IN, FALSE, FALSE)	//TRUE)
								SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_LandAnimStarted+iCrate)
								//SET_BIT(serverBD.CrateDropData.CrateData[iCrate].iBitSet, biCD_Crate_LandAnimStarted)
								NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - CONTROL_CRATE_LANDING - LAND ANIM STARTED ") NET_PRINT(serverBD.CrateDropData.CrateData[iCrate].AnimCrumple) NET_PRINT(" STARTED - ") NET_PRINT_INT(iCrate) NET_NL()
								NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - CONTROL_CRATE_LANDING - NETID = ") NET_PRINT_INT(NATIVE_TO_INT(serverBD.CrateDropData.CrateData[iCrate].niChute)) NET_NL()
							ELSE
								SET_DAMPING(NET_TO_OBJ(serverBD.CrateDropData.CrateData[iCrate].niCrate), PHYSICS_DAMPING_LINEAR_V2, 0.0245)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//DESTRUCTIBLE CRATE CHECKS
			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_SpecialCrateDrop)
			
				IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.DCrateData[iCrate].NetID)
						
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.DCrateData[iCrate].NetID)
					OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.DCrateData[iCrate].NetID) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
	
						IF NOT IS_BIT_SET(serverBD.CrateDropData.CrateData[iCrate].iBitSet, biCD_Crate_DestructibleCrateBroken)
						AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DetachDestructibleCrate+iCrate)
						
							IF TAKE_CONTROL_OF_NET_ID(serverBD.DCrateData[iCrate].NetID)
								//IF IS_BIT_SET(serverBD.CrateDropData.CrateData[iCrate].iBitSet, biCD_Crate_LandAnimStarted)
								//	//DETACH_ENTITY(NET_TO_OBJ(serverBD.DCrateData[iCrate].NetID))
								//	SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DetachDestructibleCrate+iCrate)
								//	NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - CONTROL_CRATE_LANDING - A - biP_DetachDestructibleCrate SET ") NET_PRINT_INT(iCrate) NET_NL()
								//EL
								
								SET_DAMPING(NET_TO_OBJ(serverBD.DCrateData[iCrate].NetID), PHYSICS_DAMPING_LINEAR_V2, 0.1) // 0.0245)
								
								IF HAS_OBJECT_BEEN_BROKEN(NET_TO_OBJ(serverBD.DCrateData[iCrate].NetID))
									IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.CrateDropData.CrateData[iCrate].niCrate)
										IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.CrateDropData.CrateData[iCrate].niCrate)
										OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.CrateDropData.CrateData[iCrate].niCrate) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
											IF TAKE_CONTROL_OF_NET_ID(serverBD.CrateDropData.CrateData[iCrate].niCrate)
												DETACH_ENTITY(NET_TO_OBJ(serverBD.CrateDropData.CrateData[iCrate].niCrate))
												PREVENT_COLLECTION_OF_PORTABLE_PICKUP(NET_TO_OBJ(serverBD.CrateDropData.CrateData[iCrate].niCrate), FALSE)
												NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - CONTROL_CRATE_LANDING - PREVENT_COLLECTION_OF_PORTABLE_PICKUP - FALSE - C - ") NET_PRINT_INT(iCrate) NET_NL()
												
												IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.CrateDropData.CrateData[iCrate].niChute)
													IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.CrateDropData.CrateData[iCrate].niChute)
													OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.CrateDropData.CrateData[iCrate].niChute) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
														DELETE_NET_ID(serverBD.CrateDropData.CrateData[iCrate].niChute)
													ENDIF
												ENDIF
												
												//DETACH_ENTITY(NET_TO_OBJ(serverBD.DCrateData[iCrate].NetID))
												SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DetachDestructibleCrate+iCrate)
												NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - CONTROL_CRATE_LANDING - B - biP_DetachDestructibleCrate SET ") NET_PRINT_INT(iCrate) NET_NL()
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
								//Check Destructible Crate is above ground
								vCratePos = GET_ENTITY_COORDS(NET_TO_OBJ(serverBD.DCrateData[iCrate].NetID))
								IF vCratePos.z < 0	//-2.0
									SET_ENTITY_COORDS(NET_TO_OBJ(serverBD.DCrateData[iCrate].NetID), serverBD.CrateDropData.LocationData[iCrate].vLocation)
									NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - CONTROL_CRATE_LANDING - DESTRUCTIBLE CRATE BELOW 0 - WARP BACK UP ") NET_PRINT_INT(iCrate) NET_PRINT(" TO ") NET_PRINT_VECTOR(serverBD.CrateDropData.LocationData[iCrate].vLocation) NET_NL()
								ENDIF
							ENDIF
						ENDIF
						

						IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_CrateBreakableTriggered)
						
							IF GET_ENTITY_HEIGHT_ABOVE_GROUND(NET_TO_OBJ(serverBD.DCrateData[iCrate].NetID)) < 1.0
							
								IF TAKE_CONTROL_OF_NET_ID(serverBD.DCrateData[iCrate].NetID)
								
									//SET_ENTITY_PROOFS(NET_TO_OBJ(serverBD.DCrateData[iCrate].NetID), FALSE, FALSE, FALSE, TRUE, FALSE, FALSE)
									SET_DISABLE_BREAKING(NET_TO_OBJ(serverBD.DCrateData[iCrate].NetID), TRUE)
									SET_DISABLE_FRAG_DAMAGE(NET_TO_OBJ(serverBD.DCrateData[iCrate].NetID), TRUE)
									
									SET_BIT(serverBD.iServerBitSet, biS_CrateBreakableTriggered)
									NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - CONTROL_CRATE_LANDING - FRAGGING TEMPORARILY DISABLED ") NET_PRINT_INT(iCrate) NET_NL()
								
								ENDIF
								
							ENDIF
						
						ELIF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_CrateBreakableDone)
						
							BOOL bSetToBreakable
							IF HAS_ENTITY_BEEN_DAMAGED_BY_ANY_PED(NET_TO_OBJ(serverBD.DCrateData[iCrate].NetID))
								bSetToBreakable = TRUE
								NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - CONTROL_CRATE_LANDING - CRATE HAS BEEN DAMAGED BY PED ") NET_PRINT_INT(iCrate) NET_NL()									
							ENDIF
						
							IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_LandAnimDone+iCrate)

								IF NOT IS_ENTITY_WAITING_FOR_WORLD_COLLISION(NET_TO_OBJ(serverBD.DCrateData[iCrate].NetID))
								OR HAS_COLLISION_LOADED_AROUND_ENTITY(NET_TO_OBJ(serverBD.DCrateData[iCrate].NetID))

									IF NOT IS_ENTITY_IN_AIR(NET_TO_OBJ(serverBD.DCrateData[iCrate].NetID))
									AND IS_ENTITY_STATIC(NET_TO_OBJ(serverBD.DCrateData[iCrate].NetID))
									
										bSetToBreakable = TRUE
										NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - CONTROL_CRATE_LANDING - on the ground and static ") NET_PRINT_INT(iCrate) NET_NL()	

									ENDIF

								ENDIF
							
							ENDIF
							
							IF bSetToBreakable
							
								IF TAKE_CONTROL_OF_NET_ID(serverBD.DCrateData[iCrate].NetID)
							
									SET_DISABLE_BREAKING(NET_TO_OBJ(serverBD.DCrateData[iCrate].NetID), FALSE)
									SET_DISABLE_FRAG_DAMAGE(NET_TO_OBJ(serverBD.DCrateData[iCrate].NetID), FALSE)
									
									SET_BIT(serverBD.iServerBitSet, biS_CrateBreakableDone)
									NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - CONTROL_CRATE_LANDING - FRAGGING ENABLED AGAIN ") NET_PRINT_INT(iCrate) NET_NL()
								
								ENDIF
								
							ENDIF
						
						ENDIF

					ENDIF
				ENDIF
				
			ENDIF

			
			
			//Check Crate is above ground
			vCratePos = GET_ENTITY_COORDS(NET_TO_OBJ(serverBD.CrateDropData.CrateData[iCrate].niCrate))
			IF vCratePos.z < 0	//-2.0
				IF TAKE_CONTROL_OF_NET_ID(serverBD.CrateDropData.CrateData[iCrate].niCrate)
					SET_ENTITY_COORDS(NET_TO_OBJ(serverBD.CrateDropData.CrateData[iCrate].niCrate), serverBD.CrateDropData.LocationData[iCrate].vLocation)
					NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - CONTROL_CRATE_LANDING - NORMAL CRATE BELOW 0 - WARP BACK UP ") NET_PRINT_INT(iCrate) NET_PRINT(" TO ") NET_PRINT_VECTOR(serverBD.CrateDropData.LocationData[iCrate].vLocation) NET_NL()
				ENDIF
			ENDIF
			
			//Control Crate Deletion
			CONTROL_CRATE_DELETION(iCrate)
		ENDIF
		
	ENDIF
ENDPROC



//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  MAIN PROCS                  //////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////

//PURPOSE: Creates all the sequences needed for this script
//PROC CREATE_SEQUENCES()
//	IF NOT IS_BIT_SET(iBoolsBitSet, biSequencesCreated)
//		IF IS_NET_VEHICLE_DRIVEABLE(serverBD.CrateDropData.PlaneData.Plane.niVeh)
//			VECTOR vDest = serverBD.CrateDropData.LocationData[serverBD.CrateDropData.iNumDrops-1].vPlaneStart+<<0, 0, PLANE_HEIGHT_ABOVE_DROP>>
//			vDest.y = 9000.0	//FAR NORTH
//			OPEN_SEQUENCE_TASK(seqPlaneLeave)
//				TASK_VEHICLE_DRIVE_TO_COORD(NULL, NET_TO_VEH(serverBD.CrateDropData.PlaneData.Plane.niVeh), vDest, PLANE_NORMAL_SPEED, DRIVINGSTYLE_NORMAL, serverBD.CrateDropData.PlaneData.Plane.VehModel, DRIVINGMODE_AVOIDCARS, -1, -1)
//				//TASK_VEHICLE_DRIVE_TO_COORD(NULL, NET_TO_VEH(serverBD.CrateDropData.PlaneData.Plane.niVeh), serverBD.CrateDropData.LocationData[serverBD.CrateDropData.iNumDrops-1].vPlaneStart+<<0, 0, PLANE_HEIGHT_ABOVE_DROP>>, PLANE_NORMAL_SPEED, DRIVINGSTYLE_NORMAL, serverBD.CrateDropData.PlaneData.Plane.VehModel, DRIVINGMODE_PLOUGHTHROUGH, PLANE_TARGET_RADIUS, -1) 
//				//TASK_VEHICLE_DRIVE_WANDER(NULL, NET_TO_VEH(serverBD.CrateDropData.PlaneData.Plane.niVeh), PLANE_NORMAL_SPEED, DRIVINGMODE_PLOUGHTHROUGH) //TASK_VEHICLE_TEMP_ACTION(NULL, NET_TO_VEH(serverBD.CrateDropData.PlaneData.Plane.niVeh), TEMPACT_GOFORWARD, 999999)
//			CLOSE_SEQUENCE_TASK(seqPlaneLeave)
//			SET_BIT(iBoolsBitSet, biSequencesCreated)
//			NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - CREATE_SEQUENCES   <----------     ") NET_NL()
//		ELSE
//			NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - CREATE_SEQUENCES - NO - PLANE IS NOT DRIVEABLE   <----------     ") NET_NL()
//		ENDIF
//	ENDIF
//ENDPROC

//PURPOSE: Clears all the sequences needed for this script
//PROC CLEAR_SEQUENCES()
//	CLEAR_SEQUENCE_TASK(seqPlaneLeave)
//	NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - CLEAR_SEQUENCES   <----------     ") NET_NL()
//ENDPROC

//PURPOSE: Set the player's spawn area
PROC SET_PLAYER_SPAWN_AREA(INT iCrate, FLOAT fRadius)
	IF NOT IS_BIT_SET(iBoolsBitSet, biSpawnAtDropSet)
		IF IS_NET_PLAYER_OK(PLAYER_ID())
			SET_MISSION_SPAWN_SPHERE(serverBD.CrateDropData.LocationData[iCrate].vLocation, fRadius, DEFAULT, DEFAULT, TRUE)
			SET_MISSION_SPAWN_OCCLUSION_SPHERE(serverBD.CrateDropData.LocationData[iCrate].vLocation, (CRATE_DROP_LOCATION_RADIUS/4)*2, DEFAULT, TRUE)
			SET_PLAYER_WILL_SPAWN_FACING_COORDS(serverBD.CrateDropData.LocationData[iCrate].vLocation, TRUE, FALSE)
			SET_BIT(iBoolsBitSet, biSpawnAtDropSet)
			NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - SET_PLAYER_SPAWN_AREA - DONE    <----------     ") NET_NL()
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Clear the player's spawn area
PROC CLEAR_PLAYER_SPAWN_AREA(BOOL bDoAliveCheck = TRUE)
	IF IS_BIT_SET(iBoolsBitSet, biSpawnAtDropSet)
		IF IS_NET_PLAYER_OK(PLAYER_ID())
		OR bDoAliveCheck = FALSE
			CLEAR_SPAWN_AREA()
			CLEAR_BIT(iBoolsBitSet, biSpawnAtDropSet)
			NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - CLEAR_PLAYER_SPAWN_AREA - DONE    <----------     ") NET_NL()
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Retursn TRUE if it is okay to show the blips
FUNC BOOL IS_SAFE_FOR_BLIPS()
	
	IF HIDE_BLIP_BECAUSE_PLAYER_IS_STARTING_RACE()
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_GANGHIDEOUT)
	OR IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_RACETOPOINT)
	OR GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_IMPROMPTU_DM
		RETURN FALSE
	ENDIF
		
	IF NOT IS_FM_TYPE_UNLOCKED(FMMC_TYPE_CRATE_DROP)	//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_GUN_INTRO
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

////PURPOSE: Controls when the Plane should and shouldn't be blipped
//PROC CONTROL_PLANE_BLIP()
//	IF NOT IS_BIT_SET(serverBD.CrateDropData.CrateData[0].iBitSet,  biCD_Crate_AllCratesDropped)
//	AND IS_SAFE_FOR_BLIPS()
//		IF NOT DOES_BLIP_EXIST(PlaneBlip)
//			IF IS_NET_VEHICLE_DRIVEABLE(serverBD.CrateDropData.PlaneData.Plane.niVeh)
//				PlaneBlip = ADD_BLIP_FOR_ENTITY(NET_TO_VEH(serverBD.CrateDropData.PlaneData.Plane.niVeh))
//				IF DOES_BLIP_EXIST(PlaneBlip)
//					SET_BLIP_SCALE(PlaneBlip, BLIP_SIZE_NETWORK_VEHICLE)
//					SET_BLIP_SPRITE(PlaneBlip, RADAR_TRACE_PLANE_DROP)
//					SET_BLIP_NAME_FROM_TEXT_FILE(PlaneBlip, "ACD_BLIPP")
//					SET_BLIP_FLASH_TIMER(PlaneBlip, 7000)
//					SET_BLIP_PRIORITY(PlaneBlip, BLIPPRIORITY_MED_HIGH)
//					//SET_BLIP_COLOUR(PlaneBlip, BLIP_COLOUR_BLUE)
//					NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - PLANE BLIP ADDED ") NET_NL()
//				ENDIF
//			ENDIF
//		ELSE
//			IF IS_NET_VEHICLE_DRIVEABLE(serverBD.CrateDropData.PlaneData.Plane.niVeh)
//				SET_BLIP_ROTATION(PlaneBlip, ROUND(GET_ENTITY_HEADING(NET_TO_VEH(serverBD.CrateDropData.PlaneData.Plane.niVeh))))
//				IF NOT bAddedToActAvailable
//					IF ADD_NAMED_ACTIVITY_TO_DISPLAY_LIST(GET_ENTITY_COORDS(NET_TO_VEH(serverBD.CrateDropData.PlaneData.Plane.niVeh)),GET_ACTIVITY_NAME_FROM_FMMC_TYPE(FMMC_TYPE_CRATE_DROP),"")
//						bAddedToActAvailable = TRUE
//					ENDIF
//				ELSE
//					UPDATE_ACTIVITY_ON_DISPLAY_LIST_EACH_FRAME(GET_ENTITY_COORDS(NET_TO_VEH(serverBD.CrateDropData.PlaneData.Plane.niVeh)),GET_ACTIVITY_NAME_FROM_FMMC_TYPE(FMMC_TYPE_CRATE_DROP),"")
//				ENDIF
//			ENDIF
//		ENDIF	
//	ELSE
//		IF DOES_BLIP_EXIST(PlaneBlip)
//			IF bAddedToActAvailable
//				REMOVE_ACTIVITY_FROM_DISPLAY_LIST(<<0,0,0>>,GET_ACTIVITY_NAME_FROM_FMMC_TYPE(FMMC_TYPE_CRATE_DROP),TRUE)
//			ENDIF
//			REMOVE_BLIP(PlaneBlip)
//			NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - PLANE BLIP REMOVED ") NET_NL()
//		ENDIF
//	ENDIF
//ENDPROC

////PURPOSE: Returns TRUE if the PLane is stuck or not int the air
//FUNC BOOL IS_PLANE_STUCK()
//	IF NOT IS_ENTITY_IN_AIR(NET_TO_VEH(serverBD.CrateDropData.PlaneData.Plane.niVeh))
//		NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - IS_PLANE_STUCK - TRUE - NOT IN AIR ") NET_NL()
//		RETURN TRUE
//	ENDIF
//	
//	IF IS_ENTITY_IN_WATER(NET_TO_VEH(serverBD.CrateDropData.PlaneData.Plane.niVeh))
//		NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - IS_PLANE_STUCK - TRUE - IN WATER ") NET_NL()
//		RETURN TRUE
//	ENDIF
//	
//	IF IS_VEHICLE_STUCK_TIMER_UP(NET_TO_VEH(serverBD.CrateDropData.PlaneData.Plane.niVeh), VEH_STUCK_ON_ROOF, ROOF_TIME)
//		NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - IS_PLANE_STUCK - TRUE - VEH_STUCK_ON_ROOF ") NET_NL()
//		RETURN TRUE
//	ENDIF
//	
//	IF IS_VEHICLE_STUCK_TIMER_UP(NET_TO_VEH(serverBD.CrateDropData.PlaneData.Plane.niVeh), VEH_STUCK_ON_SIDE, SIDE_TIME)
//		NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - IS_PLANE_STUCK - TRUE - VEH_STUCK_ON_SIDE ") NET_NL()
//		RETURN TRUE
//	ENDIF
//	
//	IF IS_VEHICLE_STUCK_TIMER_UP(NET_TO_VEH(serverBD.CrateDropData.PlaneData.Plane.niVeh), VEH_STUCK_JAMMED, JAMMED_TIME)
//		NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - IS_PLANE_STUCK - TRUE - VEH_STUCK_JAMMED ") NET_NL()
//		RETURN TRUE
//	ENDIF
//	
//	IF IS_VEHICLE_STUCK_TIMER_UP(NET_TO_VEH(serverBD.CrateDropData.PlaneData.Plane.niVeh), VEH_STUCK_HUNG_UP, HUNG_UP_TIME)
//		NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - IS_PLANE_STUCK - TRUE -  ") NET_NL()
//		RETURN TRUE
//	ENDIF
//	
//	RETURN FALSE
//ENDFUNC				

////PURPOSE: Checks for the plane (destroyed, stuck, etc)
//PROC CONTROL_PLANE_BODY()
//	IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.CrateDropData.PlaneData.Plane.niVeh)
//		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DestroyedPlane)
//			
//			// this check doesn't return false when the vehicle is on fire which was causing bug 1151056
//			//IF NOT IS_NET_VEHICLE_DRIVEABLE(serverBD.CrateDropData.PlaneData.Plane.niVeh)	
//			
//			IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.CrateDropData.PlaneData.Plane.niVeh)
//			AND DOES_ENTITY_EXIST(NET_TO_VEH(serverBD.CrateDropData.PlaneData.Plane.niVeh))
//			AND NOT IS_VEHICLE_DRIVEABLE(NET_TO_VEH(serverBD.CrateDropData.PlaneData.Plane.niVeh))
//				//Check if player was Killer
//				WEAPON_TYPE TempWeapon
//				IF PLAYER_ID() = NETWORK_GET_DESTROYER_OF_NETWORK_ID(serverBD.CrateDropData.PlaneData.Plane.niVeh, TempWeapon)
//					GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD, PLAYER_PED_ID(), "XPT_KAIE", XPTYPE_SKILL, XPCATEGORY_SKILL_DESTROYED_CRATE_DROP_PLANE, 500, 1)
//					iEarnedXP += 500
//					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DestroyedPlane)
//					NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - CONTROL_PLANE_BODY - I DESTROYED THE PLANE ") NET_NL()
//				ENDIF
//			ELSE
//				
//				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.CrateDropData.PlaneData.Plane.niVeh)
//				OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.CrateDropData.PlaneData.Plane.niVeh) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
//					
//					CONTROL_PILOT_FLIGHT_TASK()
//					
//					//Stuck Checks
//					IF IS_PLANE_STUCK()
//						IF TAKE_CONTROL_OF_NET_ID(serverBD.CrateDropData.PlaneData.Plane.niVeh)
//						AND TAKE_CONTROL_OF_NET_ID(serverBD.CrateDropData.PlaneData.Pilot.niPed) //** TWH - RLB - (fix for B*1417115 - cleanup pilot as well as plane).
//							NETWORK_EXPLODE_VEHICLE(NET_TO_VEH(serverBD.CrateDropData.PlaneData.Plane.niVeh), TRUE, TRUE)
//							NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - CONTROL_PLANE_BODY - NETWORK_EXPLODE_VEHICLE - PLANE STUCK ") NET_PRINT_VECTOR(GET_ENTITY_COORDS(NET_TO_VEH(serverBD.CrateDropData.PlaneData.Plane.niVeh))) NET_NL()
//							IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.CrateDropData.PlaneData.Plane.niVeh)
//								CLEANUP_NET_ID(serverBD.CrateDropData.PlaneData.Plane.niVeh)
//							ENDIF
//							IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.CrateDropData.PlaneData.Pilot.niPed)
//								SET_PED_KEEP_TASK(NET_TO_PED(serverBD.CrateDropData.PlaneData.Pilot.niPed), TRUE)
//								CLEANUP_NET_ID(serverBD.CrateDropData.PlaneData.Pilot.niPed)
//							ENDIF
//						ENDIF
//						
//					//Below Ground Check
//					ELIF NOT IS_BIT_SET(serverBD.CrateDropData.CrateData[0].iBitSet, biCD_Crate_AllCratesDropped)
//						IF NOT IS_ENTITY_IN_WATER(NET_TO_VEH(serverBD.CrateDropData.PlaneData.Plane.niVeh))
//							VECTOR vPlanePos = GET_ENTITY_COORDS(NET_TO_VEH(serverBD.CrateDropData.PlaneData.Plane.niVeh))
//							IF vPlanePos.z < -2.0
//								INT i
//								REPEAT serverBD.CrateDropData.iNumDrops i
//									IF NOT IS_BIT_SET(serverBD.CrateDropData.CrateData[i].iBitSet, biCD_Crate_DropDone)
//										SET_ENTITY_COORDS(NET_TO_VEH(serverBD.CrateDropData.PlaneData.Plane.niVeh), serverBD.CrateDropData.LocationData[i].vPlaneStart)
//										SET_ENTITY_HEADING(NET_TO_VEH(serverBD.CrateDropData.PlaneData.Plane.niVeh), serverBD.CrateDropData.LocationData[i].fPlaneHeading)
//										NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - CONTROL_PLANE_BODY - PLANE BELOW GROUND - WARP TO START POSITION ") NET_PRINT_INT(i) NET_NL()
//									ENDIF
//								ENDREPEAT
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
//ENDPROC

//Helper function to get a clients game/mission state
FUNC INT GET_CLIENT_MISSION_STATE(INT iPlayer)
	RETURN playerBD[iPlayer].iGameState
ENDFUNC

//Helper function to get the servers game/mission state
FUNC INT GET_SERVER_MISSION_STATE()
	RETURN serverBD.iServerGameState
ENDFUNC

//Cleanup Mission Data
PROC SCRIPT_CLEANUP()
	//BOOL bPlayerGotCrate
	INT i
    REPEAT MAX_NUM_CRATE_DROPS i
		IF DOES_BLIP_EXIST(CrateBlip[i])
			REMOVE_BLIP(CrateBlip[i])
		ENDIF
		IF DOES_BLIP_EXIST(RadiusBlip[i])
			REMOVE_BLIP(RadiusBlip[i])
		ENDIF
		CLEANUP_CRATE_BEACON(i)
		IF NETWORK_IS_GAME_IN_PROGRESS()
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					CLEANUP_ALL_CRATE_FLARES()
					CLEANUP_CRATE_BLEEP_SOUND(i)
				ENDIF
			ENDIF
			/*IF IS_NET_PLAYER_OK(PLAYER_ID())
				IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_PickedUpCrate+i)
					//bPlayerGotCrate = TRUE
				ENDIF
			ENDIF*/
		ENDIF
		//CLEAR_TAKE_MONEY_PERSISTING_DATA(TakeMoneyData[i])
		MPGlobalsAmbience.vCrateDropLocations[i] = <<0, 0, 0>>
	ENDREPEAT
	
	/*IF DOES_BLIP_EXIST(PlaneBlip)
		REMOVE_BLIP(PlaneBlip)
	ENDIF*/
	
	//CLEAR_SEQUENCES()
	
	CLEAR_PLAYER_SPAWN_AREA()
	
	SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_CRATEDROP, FALSE)
	
	IF bAddedToActAvailable
		REMOVE_ACTIVITY_FROM_DISPLAY_LIST(<<0,0,0>>,GET_ACTIVITY_NAME_FROM_FMMC_TYPE(FMMC_TYPE_CRATE_DROP),TRUE)
	ENDIF

	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ACD_HELP1")
		CLEAR_HELP()
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF NOT IS_BIT_SET(iCrateRewadrGivenBitSet, 0)
			iWeaponHash = 0
			iOtherItemHash = 0
			iSpecialItemHash.m_Hashs[0] = 0
			bCollectedArmour = FALSE
			PRINTLN("     ---------->     CRATE DROP - DIDN'T PICK UP CRATE - CLEAR SOME DATA BEFORE SENDING")
		ENDIF
		
		IF iEarnedXP != 0
		OR iEarnedCash != 0
		OR iWeaponHash != 0
		OR iOtherItemHash != 0
		OR iEnemiesKilled != 0																		//LET MIGUEL KNOW WHEN WE START TO USE iOtherItemHash
		OR iSpecialItemHash.m_Hashs[0] != 0
		OR bCollectedArmour = TRUE
			PLAYSTATS_CRATE_DROP_MISSION_DONE(FMMC_TYPE_CRATE_DROP, iEarnedXP, iEarnedCash, iWeaponHash, iOtherItemHash, iEnemiesKilled, iSpecialItemHash, bCollectedArmour)
		ENDIF
	ENDIF
	
	/*NO LONGER NEEDED DUE TO ABOVE
	IF bPlayerGotCrate = TRUE
		UPDATE_FMMC_END_OF_MISSION_STATUS(ciFMMC_END_OF_MISSION_STATUS_PASSED, FMMC_TYPE_CRATE_DROP)
	ELSE
		UPDATE_FMMC_END_OF_MISSION_STATUS(ciFMMC_END_OF_MISSION_STATUS_FAILED, FMMC_TYPE_CRATE_DROP)
	ENDIF*/
	
	//One last check to see if we destroyed the Plane
	//CONTROL_PLANE_BODY()
	
	g_sCrateDropF9Data.iBitSet = 0
	
	NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - CLEANUP MISSION      <----------     ") NET_NL()
	#IF IS_DEBUG_BUILD NET_LOG("SCRIPT_CLEANUP")	#ENDIF
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

//Do necessary pre game start ini.
PROC PROCESS_PRE_GAME( MP_MISSION_DATA missionScriptArgs )

	// This marks the script as a net script, and handles any instancing setup. 
	SETUP_MP_MISSION_FOR_NETWORK(NUM_NETWORK_PLAYERS, missionScriptArgs)	//GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(missionScriptArgs.mission) ,  missionScriptArgs)
	//NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE, PARTICIPANT_ID_TO_INT())  
	
	// This makes sure the net script is active, waits until it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
	
	// KGM: Wait for the first network broadcast before moving on - call this as the last instruction in pre-game
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		#IF IS_DEBUG_BUILD NET_LOG("CRATE DROP - PROCESS_PRE_GAME - FAILED TO RECEIVE INITIAL NETWORK BROADCAST - END")	#ENDIF
		NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - MISSION END - FAILED TO RECEIVE INITIAL NETWORK BROADCAST - SCRIPT CLEANUP PRE_GAME     <----------     ") NET_NL()
		SCRIPT_CLEANUP()
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			SETUP_BASIC_DATA()
			GET_CRATE_DROP_DATA()
			//BROADCAST_JOIN_SCRIPT_IF_NOT_ON_SCRIPT(missionScriptArgs, FALSE, ALL_PLAYERS())
			//BROADCAST_JOIN_THIS_AMBIENT_MP_MISSION_EVENT(missionScriptArgs, ALL_PLAYERS())
		ENDIF
		
		//Reserve Entities
		RESERVE_NETWORK_MISSION_PEDS((LOCATION_MAX_NUM_PEDS*serverBD.CrateDropData.iNumDrops))
		RESERVE_NETWORK_MISSION_VEHICLES((LOCATION_MAX_NUM_VEHICLES*serverBD.CrateDropData.iNumDrops))
		RESERVE_NETWORK_MISSION_OBJECTS(3*serverBD.CrateDropData.iNumDrops)
		
		// This script will not be paused if another script calls PAUSE_GAME
		SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
		
		SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_CRATEDROP, TRUE)
			
		#IF IS_DEBUG_BUILD
		g_bFM_IgnoreRankOnNextLaunchedActivity = FALSE
		#ENDIF
		
		/*INT iCrate
		REPEAT MAX_NUM_CRATE_DROPS iCrate
			playerBD[PARTICIPANT_ID_TO_INT()].TakeMoneyClientData[iCrate].bDisplayBarRedWhenNotInMiniGame = TRUE
		ENDREPEAT*/
		
		playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_INI
		NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - PRE_GAME DONE      <----------     ") NET_NL()
	ELSE
		#IF IS_DEBUG_BUILD NET_LOG("CRATE DROP - PROCESS_PRE_GAME - NETWORK_IS_GAME_IN_PROGRESS = FALSE - END")	#ENDIF
		NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - MISSION END - NETWORK_IS_GAME_IN_PROGRESS = FALSE - SCRIPT CLEANUP PRE_GAME     <----------     ") NET_NL()
		SCRIPT_CLEANUP()
	ENDIF
	
ENDPROC

//Player leaves vehicle, dies, runs out of drugs, or the vehicle becomes undriveable
FUNC BOOL MISSION_END_CHECK()
		
	//All Crates Collected
//	IF serverBD.eCrateDropStage >= eCD_SETUP
//		IF IS_BIT_SET(serverBD.iServerBitSet, biS_AllCrewLeftStartArea)
//			NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - MISSION END - CREW NOT IN AREA     <----------     ") NET_NL()
//			RETURN TRUE
//		ENDIF
//	ENDIF

	/*IF serverBD.eCrateDropStage > eCD_SETUP
		IF NOT IS_BIT_SET(serverBD.CrateDropData.CrateData[0].iBitSet,  biCD_Crate_AllCratesPickedUp)
		AND IS_BIT_SET(serverBD.CrateDropData.CrateData[0].iBitSet,  biCD_Crate_NoCratesExist)
			IF NOT IS_NET_VEHICLE_DRIVEABLE(serverBD.CrateDropData.PlaneData.Plane.niVeh)
				NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - MISSION END - NO CRATES AND NO PLANE     <----------     ") NET_NL()
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF*/
	
	RETURN FALSE
ENDFUNC

//Server only function that checks to see if the game mode should now end. For this test script, it if time has passed.
FUNC BOOL HAVE_MISSION_END_CONDITIONS_BEEN_MET()
	
	#IF IS_DEBUG_BUILD
		IF bHostEndMissionNow
			RETURN TRUE
		ENDIF
	#ENDIF	
	
	IF MISSION_END_CHECK()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

#IF IS_DEBUG_BUILD
PROC CREATE_WIDGETS()
	INT iPlayer
	TEXT_LABEL_15 tl63

	START_WIDGET_GROUP("CRATE DROP")  
		
		ADD_WIDGET_BOOL("Warp To Drop 1", bWarpToDrop[0])
		//ADD_WIDGET_BOOL("Warp To Drop 2", bWarpToDrop[1])
		//ADD_WIDGET_BOOL("Warp To Drop 3", bWarpToDrop[2])
		//ADD_WIDGET_BOOL("bBlipPlane", bBlipPlane)
		ADD_WIDGET_BOOL("bBlipAi", bBlipAi)
		ADD_WIDGET_BOOL("bBlipAllLocations", bBlipAllLocations)
		
		START_WIDGET_GROUP("Create Details")
			ADD_WIDGET_INT_READ_ONLY("iParticpantGotCrate", serverBD.CrateDropData.CrateData[0].iParticpantGotCrate)
		STOP_WIDGET_GROUP()	
		
		START_WIDGET_GROUP("Entity Reservations")
			ADD_WIDGET_INT_READ_ONLY("Vehicles Reserved", serverBD.iReservedVehicles)
			ADD_WIDGET_INT_READ_ONLY("Peds Reserved", serverBD.iReservedPeds)
			ADD_WIDGET_INT_READ_ONLY("Objects Reserved", serverBD.iReservedObjects)
		STOP_WIDGET_GROUP()	
		
		// Gameplay debug, sever only.
		START_WIDGET_GROUP("Server Only Gameplay") 
			ADD_WIDGET_BOOL("End mission", bHostEndMissionNow)			
		STOP_WIDGET_GROUP()		
		
		// Data about the server
		START_WIDGET_GROUP("Server BD") 
				ADD_WIDGET_INT_SLIDER("S. Game state", serverBD.iServerGameState,-1, HIGHEST_INT,1)
		STOP_WIDGET_GROUP()	

		// Data about the clients. * = You.
		START_WIDGET_GROUP("Client BD")  				
			REPEAT NUM_NETWORK_PLAYERS iPlayer
				tl63 = "Player "
				tl63 += iPlayer
				IF iPlayer = PARTICIPANT_ID_TO_INT()
					tl63 += "*"
				ENDIF
				START_WIDGET_GROUP(tl63)
					ADD_WIDGET_INT_SLIDER("Game state", playerBD[iPlayer].iGameState,-1, HIGHEST_INT,1)
				STOP_WIDGET_GROUP()
			ENDREPEAT				
		STOP_WIDGET_GROUP()	
		
		#IF SCRIPT_PROFILER_ACTIVE
		CREATE_SCRIPT_PROFILER_WIDGET()
		#ENDIF
		
	STOP_WIDGET_GROUP()
ENDPROC		

PROC UPDATE_WIDGETS()
	//Plane Blip
	/*IF bBlipPlane = TRUE
		IF NOT DOES_BLIP_EXIST(PlaneBlip)
			IF IS_NET_VEHICLE_DRIVEABLE(serverBD.CrateDropData.PlaneData.Plane.niVeh)
				PlaneBlip = ADD_BLIP_FOR_ENTITY(NET_TO_VEH(serverBD.CrateDropData.PlaneData.Plane.niVeh))
				SET_BLIP_SCALE(PlaneBlip, BLIP_SIZE_NETWORK_VEHICLE)
				SET_BLIP_COLOUR(PlaneBlip, BLIP_COLOUR_BLUE)
				NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - PLANE BLIP ADDED ") NET_NL()
			ENDIF
		ENDIF
	ELSE
		IF DOES_BLIP_EXIST(PlaneBlip)
			REMOVE_BLIP(PlaneBlip)
				NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - DEBUG - PLANE BLIP REMOVED ") NET_NL()
		ENDIF
	ENDIF*/
	
	INT i
	REPEAT serverBD.CrateDropData.iNumDrops i
		IF bWarpToDrop[i]
			NET_WARP_TO_COORD(serverBD.CrateDropData.LocationData[i].vLocation, 0)
			bWarpToDrop[i] = FALSE
			NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - DEBUG - WAR TO LOCATION ") NET_PRINT_INT(i) NET_NL()
		ENDIF
		
		IF bBlipAi = TRUE
			INT k
			REPEAT LOCATION_MAX_NUM_PEDS k
				IF NOT DOES_BLIP_EXIST(AiBlip[i][k])
					IF NOT IS_NET_PED_INJURED(serverBD.CrateDropData.LocationData[i].Peds[k].niPed)
						AiBlip[i][k] = ADD_BLIP_FOR_ENTITY(NET_TO_PED(serverBD.CrateDropData.LocationData[i].Peds[k].niPed))
						SET_BLIP_SCALE(AiBlip[i][k], BLIP_SIZE_NETWORK_PED)
						SET_BLIP_COLOUR(AiBlip[i][k], BLIP_COLOUR_RED)
						
						NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - DEBUG - AI BLIP ADDED ") NET_PRINT_INT(k) NET_NL()
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ENDREPEAT
	
	IF bBlipAllLocations = TRUE
		REPEAT CRATE_DROP_LOCATION_MAX i
			IF NOT DOES_BLIP_EXIST(LocationBlip[i])
				LocationBlip[i] = ADD_BLIP_FOR_RADIUS(GET_CRATE_DROP_VECTOR(i), CRATE_DROP_LOCATION_RADIUS)
				SET_BLIP_COLOUR(LocationBlip[i], BLIP_COLOUR_PURPLE)
				SET_BLIP_ALPHA(LocationBlip[i], 220)
				SHOW_HEIGHT_ON_BLIP(LocationBlip[i], FALSE)
			ELSE
				REMOVE_BLIP(LocationBlip[i])
			ENDIF
		ENDREPEAT
		bBlipAllLocations = FALSE
	ENDIF
ENDPROC

#ENDIF

//PURPOSE: Updates the F9 screen Crate Drop Data
PROC UPDATE_F9_DATA()
	//F9 DEBUG
	//Server
	g_sCrateDropF9Data.iServerGameState						= serverBD.iServerGameState
	g_sCrateDropF9Data.eServerCrateDropStage				= ENUM_TO_INT(serverBD.eCrateDropStage)
	g_sCrateDropF9Data.iParticpantGotCrate					= serverBD.CrateDropData.CrateData[0].iParticpantGotCrate
	g_sCrateDropF9Data.biCD_Crate_ReadyForDrop				= IS_BIT_SET(serverBD.CrateDropData.CrateData[0].iBitSet, biCD_Crate_ReadyForDrop)
	g_sCrateDropF9Data.biCD_Crate_DropDone					= IS_BIT_SET(serverBD.CrateDropData.CrateData[0].iBitSet, biCD_Crate_DropDone)
	g_sCrateDropF9Data.biCD_Crate_NoPlayerAtDropLocation	= IS_BIT_SET(serverBD.CrateDropData.CrateData[0].iBitSet, biCD_Crate_NoPlayerAtDropLocation)
	g_sCrateDropF9Data.biCD_Crate_LandDone					= IS_BIT_SET(serverBD.CrateDropData.CrateData[0].iBitSet, biCD_Crate_LandDone)
	g_sCrateDropF9Data.biCD_Crate_AiSetup					= IS_BIT_SET(serverBD.CrateDropData.CrateData[0].iBitSet, biCD_Crate_AiSetup)
	g_sCrateDropF9Data.biS_SpecialCrateDrop					= IS_BIT_SET(serverBD.iServerBitSet, biS_SpecialCrateDrop)
	//Client
	g_sCrateDropF9Data.eClientCrateDropStage				= ENUM_TO_INT(playerBD[PARTICIPANT_ID_TO_INT()].eCrateDropStage)
	
	
	#IF IS_DEBUG_BUILD
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_9)
		PRINTLN(" CRATE DROP - F9 CHECK - iServerGameState", g_sCrateDropF9Data.iServerGameState)
		PRINTLN(" CRATE DROP - F9 CHECK - eServerCrateDropStage", g_sCrateDropF9Data.eServerCrateDropStage)
		PRINTLN(" CRATE DROP - F9 CHECK - iParticpantGotCrate", g_sCrateDropF9Data.iParticpantGotCrate)
		PRINTLN(" CRATE DROP - F9 CHECK - biCD_Crate_ReadyForDrop", g_sCrateDropF9Data.biCD_Crate_ReadyForDrop)
		PRINTLN(" CRATE DROP - F9 CHECK - biCD_Crate_DropDone", g_sCrateDropF9Data.biCD_Crate_DropDone)
		PRINTLN(" CRATE DROP - F9 CHECK - biCD_Crate_NoPlayerAtDropLocation", g_sCrateDropF9Data.biCD_Crate_NoPlayerAtDropLocation)
		PRINTLN(" CRATE DROP - F9 CHECK - biCD_Crate_LandDone", g_sCrateDropF9Data.biCD_Crate_LandDone)
		PRINTLN(" CRATE DROP - F9 CHECK - biCD_Crate_AiSetup", g_sCrateDropF9Data.biCD_Crate_AiSetup)
		PRINTLN(" CRATE DROP - F9 CHECK - biS_SpecialCrateDrop", g_sCrateDropF9Data.biS_SpecialCrateDrop)
		PRINTLN(" CRATE DROP - F9 CHECK - eClientCrateDropStage", g_sCrateDropF9Data.eClientCrateDropStage)
		PRINTLN(" CRATE DROP - F9 CHECK - iBitSet", g_sCrateDropF9Data.iBitSet)
	ENDIF
	#ENDIF
ENDPROC


/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  MISSION PROCS                  //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Controls the help text for the Race
PROC CONTROL_HELP_TEXT()
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		
		//Do initial help text
		IF NOT IS_BIT_SET(MPGlobalsAmbience.CrateDropLaunchData.iBitSet, biCDL_InitialHelpTextDone)	//iBoolsBitSet, biHelpDisplayed1)
			IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
			//AND IS_PLAYER_CONTROL_ON(PLAYER_ID())
			AND NOT IS_CUSTOM_MENU_ON_SCREEN()
			AND NOT IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
			AND NOT IS_FM_MISSION_LAUNCH_IN_PROGRESS()
			AND IS_FM_TYPE_UNLOCKED(FMMC_TYPE_CRATE_DROP)	//AND GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType != FMMC_TYPE_GUN_INTRO
			AND NOT IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID())
			AND NOT IS_PLAYER_IN_CORONA()
				IF HAS_PLAYER_BEEN_IN_SESSION_THIS_LONG(CRATE_DROP_DISPLAY_DELAY)
					IF !SHOULD_HIDE_LOCAL_UI()
						PRINT_HELP("ACD_HELP1")	//A plane is about to deliver crates at various locations marked by flares.~n~Be first to reach the landed crates to get your hands on their valuable contents.
					ENDIF
					SET_BIT(MPGlobalsAmbience.CrateDropLaunchData.iBitSet, biCDL_InitialHelpTextDone)	//iBoolsBitSet, biHelpDisplayed1)
					NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - CONTROL_HELP_TEXT - biHelpDisplayed1 ") NET_NL()
				ELSE
					SET_BIT(MPGlobalsAmbience.CrateDropLaunchData.iBitSet, biCDL_InitialHelpTextDone)	//iBoolsBitSet, biHelpDisplayed1)
					NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - CONTROL_HELP_TEXT - biHelpDisplayed1 NOT DISPLAYED PLAYER HAS ONLY JUST JOINED THE SESSION ") NET_NL()
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF
ENDPROC

//PURPOSE: Controls the display of extra tickers
PROC CONTROL_CRATE_TICKERS(INT i)
	//Crate Landed Ticker
	IF NOT IS_BIT_SET(iBoolsBitSet, biDroppedTicker0+i)
		IF IS_BIT_SET(serverBD.CrateDropData.CrateData[i].iBitSet, biCD_Crate_DropDone)
		AND serverBD.CrateDropData.CrateData[i].iParticpantGotCrate = -1	//NOT IS_BIT_SET(serverBD.CrateDropData.CrateData[i].iBitSet, biCD_Crate_PickedUp)
			INT iNextCrate = (i+1)
			IF iNextCrate > (serverBD.CrateDropData.iNumDrops-1)
				iNextCrate = (serverBD.CrateDropData.iNumDrops-1)
			ENDIF
			IF NOT IS_BIT_SET(serverBD.CrateDropData.CrateData[iNextCrate].iBitSet, biCD_Crate_DropDone)	
				/*IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_SpecialCrateDrop)
					PRINT_TICKER("ACD_TDROP")	//A crate has been dropped.
				ELSE
					PRINT_TICKER("ACD_TSDROP")	//A DLC crate has been dropped.
				ENDIF*/
				SET_BIT(iBoolsBitSet, biDroppedTicker0+i)
				NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - TICKER - biDroppedTicker0 DONE - ") NET_PRINT_INT(i) NET_NL()
			ENDIF
		ENDIF
	ENDIF
	
	//All Crates Taken Ticker
	/*IF NOT IS_BIT_SET(iBoolsBitSet, biAllCollectedTicker)
		IF IS_BIT_SET(serverBD.CrateDropData.CrateData[0].iBitSet, biCD_Crate_AllCratesPickedUp)
			PRINT_TICKER("ACD_TDONE")	//All crates have been collected.
			SET_BIT(iBoolsBitSet, biAllCollectedTicker)
			NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - TICKER - biAllCollectedTicker DONE") NET_NL()
		ENDIF
	ENDIF*/
	
ENDPROC

//PURPOSE: Process non-crate tickers
PROC CONTROL_TICKERS()
	//Plane Destroyed Ticker
	/*IF NOT IS_BIT_SET(iBoolsBitSet, biPlaneDestroyedTicker)
		IF serverBD.iPlaneDestroyer != -1
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(serverBD.iPlaneDestroyer))
				IF serverBD.iPlaneDestroyer != PARTICIPANT_ID_TO_INT()
					PRINT_TICKER_WITH_PLAYER_NAME("ACD_TPLAD", NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(serverBD.iPlaneDestroyer)))	//~a~ destroyed the crate drop plane.
					SET_BIT(iBoolsBitSet, biPlaneDestroyedTicker)
					NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - TICKER - biPlaneDestroyedTicker DONE") NET_NL()
				ENDIF
			ENDIF
		ENDIF
	ENDIF*/
ENDPROC

//PURPOSE: Add a flare to a crate landing position
FUNC BOOL ADD_CRATE_FLARES()
	INT i
	
	IF LOAD_INTERACT_PARTICLE_FX()
		REPEAT serverBD.CrateDropData.iNumDrops i
			IF DOES_ENTITY_EXIST(FlareCanister[i])	
				IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(serverBD.CrateDropData.CrateData[i].ptfxFlare)		//scr_drug_traffic_flare_L
					serverBD.CrateDropData.CrateData[i].ptfxFlare = START_NETWORKED_PARTICLE_FX_LOOPED_ON_ENTITY_BONE("scr_crate_drop_flare", FlareCanister[i], <<0, 0, -0.2>>, <<0, 0, 0>>)
					
					IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_SpecialCrateDrop)
						SET_PARTICLE_FX_LOOPED_COLOUR(serverBD.CrateDropData.CrateData[i].ptfxFlare, 0.8, 0.18, 0.19)
					ELSE
						SET_PARTICLE_FX_LOOPED_COLOUR(serverBD.CrateDropData.CrateData[i].ptfxFlare, 1.0, 0.84, 0.0)
					ENDIF
					NET_PRINT_TIME() NET_PRINT("     ---------->    CRATE DROP - ADD_CRATE_FLARES ") NET_PRINT_INT(i) NET_NL()
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	//Check flares have been created
	REPEAT serverBD.CrateDropData.iNumDrops i
		IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(serverBD.CrateDropData.CrateData[i].ptfxFlare)
			RETURN FALSE
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

/*INT iStaggeredParticipantCounter[MAX_NUM_CRATE_DROPS]
PROC MAINTAIN_STAGGERED_PARTICIPANT_LOOP_CLIENT(INT iCrate)
	
	IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iStaggeredParticipantCounter[iCrate]))
		PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iStaggeredParticipantCounter[iCrate]))
		TAKE_MONEY_DISABLED_CHECK(PlayerId, TakeMoneyData[iCrate], playerBD[iStaggeredParticipantCounter[iCrate]].TakeMoneyClientData[iCrate])
	ENDIF
	
	iStaggeredParticipantCounter[iCrate]++
	IF iStaggeredParticipantCounter[iCrate] >= NETWORK_GET_MAX_NUM_PARTICIPANTS()
		iStaggeredParticipantCounter[iCrate] = 0
		TakeMoneyData[iCrate].bTakeMoneyDisabled = FALSE
	ENDIF
	
ENDPROC*/

//PURPOSE: Runs through the max num participents and does relevant checks
PROC MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER()

	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	OPEN_SCRIPT_PROFILE_MARKER_GROUP("MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER")
	#ENDIF
	#ENDIF

	INT iParticipant
	//SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerEventData
	//FLOAT fClosestToCrate[MAX_NUM_CRATE_DROPS]
	/*INT i
	REPEAT MAX_NUM_CRATE_DROPS i
		//fClosestToCrate[i] = CRATE_PICKUP_RANGE
		//SET_BIT(serverBD.CrateDropData.CrateData[i].iBitSet, biCD_Crate_NoPlayerAtDropLocation)
		serverBD.CrateDropData.CrateData[i].TakeMoneyServerData.iParticipantTakingMoney = -1
	ENDREPEAT*/
	SET_BIT(serverBD.CrateDropData.CrateData[0].iBitSet, biCD_Crate_NoPlayerAtAnyDropLocation)
	
	/*#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("1st repeat")
	#ENDIF
	#ENDIF*/
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	OPEN_SCRIPT_PROFILE_MARKER_GROUP("MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER")
	#ENDIF
	#ENDIF
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_PRE_LOOP_MARKER("loop")
	#ENDIF
	#ENDIF
	
	//Set Bits that can then be cleared in the repeat if necessary
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
		
		#ENDIF
		#ENDIF
		
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
			PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
			PED_INDEX PlayerPedId = GET_PLAYER_PED(PlayerId)
			
			//Check if any player has destroyed the plane
			/*IF serverBD.iPlaneDestroyer = -1
				IF IS_BIT_SET(playerBD[iParticipant].iPlayerBitSet, biP_DestroyedPlane)
					TickerEventData.TickerEvent = TICKER_EVENT_CRATE_DROP_PLANE_DESTROYED
					TickerEventData.playerID = PLAYER_ID()
					BROADCAST_TICKER_EVENT(TickerEventData, ALL_PLAYERS_ON_SCRIPT(FALSE))
					
					serverBD.iPlaneDestroyer = iParticipant
					NET_PRINT_TIME() NET_PRINT("     ---------->     PLANE DESTROYER = ") NET_PRINT(GET_PLAYER_NAME(PlayerId)) NET_NL()
				ENDIF
			ENDIF*/
			
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE 
					ADD_SCRIPT_PROFILE_MARKER("Marker 1")
				#ENDIF
				#ENDIF
				
			//Check for player picking up Crate
			INT iCrate
			//FLOAT fDistance
			REPEAT MAX_NUM_CRATE_DROPS iCrate
				
				//Check Taking Money
				//IF playerBD[iParticipant].TakeMoneyClientData[iCrate].bInTakeMoneyGame = TRUE
				//	serverBD.CrateDropData.CrateData[iCrate].TakeMoneyServerData.iParticipantTakingMoney = iParticipant
				//ENDIF
				//UPDATE_SERVER_MONEY_TAKEN_COUNTER(serverBD.CrateDropData.CrateData[iCrate].TakeMoneyServerData, playerBD[iParticipant].TakeMoneyClientData[iCrate])
				
				//Check if land anim has started
				IF NOT IS_BIT_SET(serverBD.CrateDropData.CrateData[iCrate].iBitSet, biCD_Crate_LandAnimStarted)
					IF IS_BIT_SET(playerBD[iParticipant].iPlayerBitSet, biP_LandAnimStarted+iCrate)
						SET_BIT(serverBD.CrateDropData.CrateData[iCrate].iBitSet, biCD_Crate_LandAnimStarted)
						NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - biCD_Crate_LandAnimStarted SET ") NET_NL() 
					ENDIF
				ENDIF
				//Check if land anim is playing
				IF NOT IS_BIT_SET(serverBD.CrateDropData.CrateData[iCrate].iBitSet, biCD_Crate_LandAnimPlaying)
					IF IS_BIT_SET(playerBD[iParticipant].iPlayerBitSet, biP_LandAnimPlaying+iCrate)
						SET_BIT(serverBD.CrateDropData.CrateData[iCrate].iBitSet, biCD_Crate_LandAnimPlaying)
						NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - biCD_Crate_LandAnimPlaying SET ") NET_NL() 
					ENDIF
				ENDIF
				//Check if land anim is done
				IF NOT IS_BIT_SET(serverBD.CrateDropData.CrateData[iCrate].iBitSet, biCD_Crate_LandDone)
					IF IS_BIT_SET(playerBD[iParticipant].iPlayerBitSet, biP_LandAnimDone+iCrate)
						SET_BIT(serverBD.CrateDropData.CrateData[iCrate].iBitSet, biCD_Crate_LandDone)
						NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - biCD_Crate_LandDone SET ") NET_NL() 
					ENDIF
				ENDIF
				//Check if Destructible Crate has been broken
				IF NOT IS_BIT_SET(serverBD.CrateDropData.CrateData[iCrate].iBitSet, biCD_Crate_DestructibleCrateBroken)
					IF IS_BIT_SET(playerBD[iParticipant].iPlayerBitSet, biP_DetachDestructibleCrate+iCrate)
						SET_BIT(serverBD.CrateDropData.CrateData[iCrate].iBitSet, biCD_Crate_DestructibleCrateBroken)
						NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - biCD_Crate_DestructibleCrateBroken SET ") NET_NL() 
					ENDIF
				ENDIF
				
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE 
					ADD_SCRIPT_PROFILE_MARKER("Marker 2")
				#ENDIF
				#ENDIF
				
				//** CHECKS THAT REQUIRE THE PLAYER TO BE ALIVE **//
				IF IS_NET_PLAYER_OK(PlayerId)
				
					//Check if anybody has picked up the crate
					IF serverBD.CrateDropData.CrateData[iCrate].iParticpantGotCrate = -1
						//IF NOT IS_BIT_SET(serverBD.CrateDropData.CrateData[iCrate].iBitSet, biCD_Crate_PickedUp)
							IF IS_BIT_SET(playerBD[iParticipant].iPlayerBitSet, biP_PickedUpCrate+iCrate)
								
								serverBD.CrateDropData.CrateData[iCrate].iParticpantGotCrate = iParticipant
								
								CLEANUP_CRATE_BLEEP_SOUND(iCrate)
								CLEANUP_CRATE_FLARE(iCrate)
								CLEANUP_CRATE_BEACON(iCrate)
								//SET_BIT(serverBD.CrateDropData.CrateData[k].iBitSet, biCD_Crate_PickedUp)
								NET_PRINT_TIME() NET_PRINT("  ---->  CRATE DROP - CRATE PICKED UP - Crate ") NET_PRINT_INT(iCrate) NET_PRINT(" Player ") NET_PRINT(GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(serverBD.CrateDropData.CrateData[iCrate].iParticpantGotCrate)))) NET_NL()
								
								
							ENDIF
						//ENDIF
					ENDIF
								
					//Reduce Object Reservation
					IF NOT IS_BIT_SET(serverBD.CrateDropData.CrateData[iCrate].iBitSet, biCD_Crate_CrateReservationReduced+iCrate)
					AND IS_BIT_SET(serverBD.CrateDropData.CrateData[iCrate].iBitSet, biCD_Crate_LandDone+iCrate)
						IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.CrateDropData.CrateData[iCrate].niCrate)
							serverBD.iReservedObjects--
							SET_BIT(serverBD.CrateDropData.CrateData[iCrate].iBitSet, biCD_Crate_CrateReservationReduced+iCrate)
							NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - REDUCED - CRATE DOESN'T EXIST - serverBD.iReservedObjects = ") NET_PRINT_INT(serverBD.iReservedObjects) NET_NL()
						ENDIF
					ENDIF
					
					/*IF IS_BIT_SET(serverBD.CrateDropData.CrateData[iCrate].iBitSet, biCD_Crate_LandDone)
						IF NOT IS_BIT_SET(serverBD.CrateDropData.CrateData[iCrate].iBitSet, biCD_Crate_PickedUp)
							IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.CrateDropData.CrateData[iCrate].niCrate)
								IF IS_PED_ON_FOOT(PlayerPedId)
									fDistance = GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PlayerId), GET_ENTITY_COORDS(NET_TO_OBJ(serverBD.CrateDropData.CrateData[iCrate].niCrate), FALSE))
									IF fDistance < fClosestToCrate[iCrate]
										fClosestToCrate[iCrate] = fDistance
										serverBD.CrateDropData.CrateData[iCrate].iParticpantGotCrate = iParticipant
										NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - PLAYER CLOSEST TO CRATE ") NET_PRINT_INT(iCrate) NET_PRINT(" is ") NET_PRINT(GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(serverBD.CrateDropData.CrateData[iCrate].iParticpantGotCrate)))) NET_NL()
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF*/
					
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE 
					ADD_SCRIPT_PROFILE_MARKER("Marker 3")
				#ENDIF
				#ENDIF
				
					//Check if any entity is near drop off location
					IF IS_ENTITY_AT_COORD(PlayerPedId, serverBD.CrateDropData.LocationData[iCrate].vLocation, <<100, 100, 100>>)
						CLEAR_BIT(serverBD.CrateDropData.CrateData[0].iBitSet, biCD_Crate_NoPlayerAtAnyDropLocation)
						CLEAR_BIT(serverBD.CrateDropData.CrateData[iCrate].iBitSet, biCD_Crate_NoPlayerAtDropLocation)
					ELIF IS_ENTITY_AT_COORD(PlayerPedId, serverBD.CrateDropData.LocationData[iCrate].vLocation, <<180, 180, 180>>)
						CLEAR_BIT(serverBD.CrateDropData.CrateData[iCrate].iBitSet, biCD_Crate_NoPlayerAtDropLocation)
					ENDIF
					
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE 
					ADD_SCRIPT_PROFILE_MARKER("Marker 4")
				#ENDIF
				#ENDIF
				
				ENDIF
			ENDREPEAT
			
		ENDIF
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	CLOSE_SCRIPT_PROFILE_MARKER_GROUP()
	#ENDIF
	#ENDIF
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_POST_LOOP_MARKER()
		ADD_SCRIPT_PROFILE_MARKER("2nd repeat")
	#ENDIF
	#ENDIF
	
//	//Check if anybody has picked up the Crate
//	INT k
//	REPEAT MAX_NUM_CRATE_DROPS k
//		IF NOT IS_BIT_SET(serverBD.CrateDropData.CrateData[k].iBitSet, biCD_Crate_PickedUp)
//			IF serverBD.CrateDropData.CrateData[k].iParticpantGotCrate != -1
//				/*IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.CrateDropData.CrateData[k].niCrate)
//					DELETE_NET_ID(serverBD.CrateDropData.CrateData[k].niCrate)
//					NET_PRINT_TIME() NET_PRINT("  ---->  CRATE DROP - CRATE DELETED - Crate ") NET_PRINT_INT(k) NET_NL()
//				ENDIF*/
//				
//				CLEANUP_CRATE_BLEEP_SOUND(k)
//				
//				IF serverBD.CrateDropData.CrateData[k].PickupSoundID <> -1
//					STOP_SOUND(serverBD.CrateDropData.CrateData[k].PickupSoundID)
//					RELEASE_SOUND_ID(serverBD.CrateDropData.CrateData[k].PickupSoundID)
//					serverBD.CrateDropData.CrateData[k].PickupSoundID = -1
//				ENDIF
//				serverBD.CrateDropData.CrateData[k].PickupSoundID = GET_NETWORK_ID_FROM_SOUND_ID(GET_SOUND_ID())
//				PLAY_SOUND_FROM_ENTITY(serverBD.CrateDropData.CrateData[k].PickupSoundID, "Crate_Collect", NET_TO_OBJ(serverBD.CrateDropData.CrateData[k].niCrate), "MP_CRATE_DROP_SOUNDS", TRUE, 15)
//				
//				CLEANUP_CRATE_FLARE(k)
//				CLEANUP_CRATE_BEACON(k)
//				SET_BIT(serverBD.CrateDropData.CrateData[k].iBitSet, biCD_Crate_PickedUp)
//				NET_PRINT_TIME() NET_PRINT("  ---->  CRATE DROP - CRATE PICKED UP - Crate ") NET_PRINT_INT(k) NET_PRINT(" Player ") NET_PRINT(GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(serverBD.CrateDropData.CrateData[k].iParticpantGotCrate)))) NET_NL()
//			ENDIF
//		ENDIF
//	ENDREPEAT
//	
//	#IF IS_DEBUG_BUILD
//	#IF SCRIPT_PROFILER_ACTIVE 
//		ADD_SCRIPT_PROFILE_MARKER("3rd repeat")
//	#ENDIF
//	#ENDIF
	
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	CLOSE_SCRIPT_PROFILE_MARKER_GROUP()
	#ENDIF
	#ENDIF	
	
ENDPROC

//PURPOSE: Adds a blip to a landed crate
PROC ADD_CRATE_BLIP(INT iCrate)
	IF DOES_BLIP_EXIST(CrateBlip[iCrate])
		IF SHOULD_HIDE_LOCAL_UI()
		OR ARE_TEMP_HIDDEN_BLIPS_ACTIVE()
			SET_BLIP_DISPLAY(CrateBlip[iCrate], DISPLAY_NOTHING)
		ELSE
			SET_BLIP_DISPLAY(CrateBlip[iCrate], DISPLAY_BOTH)
		ENDIF
	ELSE
		CrateBlip[iCrate] = ADD_BLIP_FOR_ENTITY(NET_TO_OBJ(serverBD.CrateDropData.CrateData[iCrate].niCrate))
		SET_BLIP_SCALE(CrateBlip[iCrate], BLIP_SIZE_NETWORK_PICKUP_LARGE)	//BLIP_SIZE_NETWORK_PICKUP)
		SET_BLIP_SPRITE(CrateBlip[iCrate], RADAR_TRACE_CRATEDROP)
		SET_BLIP_COLOUR(CrateBlip[iCrate], BLIP_COLOUR_GREEN)
		SET_BLIP_ALPHA(CrateBlip[iCrate], 120)
		SET_BLIP_NAME_FROM_TEXT_FILE(CrateBlip[iCrate], "ACD_BLIPN")
		SET_BLIP_PRIORITY(CrateBlip[iCrate], BLIPPRIORITY_MED)
		IF SHOULD_HIDE_LOCAL_UI()
			SET_BLIP_DISPLAY(CrateBlip[iCrate], DISPLAY_NOTHING)
		ENDIF
		NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - ADD_CRATE_BLIP ") NET_PRINT_INT(iCrate) NET_NL()
	ENDIF
ENDPROC

//PURPOSE: Adds a blip to a landed crate
PROC ADD_DROP_RADIUS_BLIP(INT iCrate)
	IF DOES_BLIP_EXIST(RadiusBlip[iCrate])
		IF SHOULD_HIDE_LOCAL_UI()
		OR ARE_TEMP_HIDDEN_BLIPS_ACTIVE()
			SET_BLIP_DISPLAY(RadiusBlip[iCrate], DISPLAY_NOTHING)
		ELSE
			SET_BLIP_DISPLAY(RadiusBlip[iCrate], DISPLAY_BOTH)
		ENDIF
	ELSE
		RadiusBlip[iCrate] = ADD_BLIP_FOR_RADIUS(serverBD.CrateDropData.LocationData[iCrate].vLocation, CRATE_DROP_LOCATION_RADIUS)
		SET_BLIP_COLOUR(RadiusBlip[iCrate], BLIP_COLOUR_GREEN)
		SET_BLIP_ALPHA(RadiusBlip[iCrate], 220)
		SHOW_HEIGHT_ON_BLIP(RadiusBlip[iCrate], FALSE)
		SET_BLIP_NAME_FROM_TEXT_FILE(RadiusBlip[iCrate], "ACD_BLIPR")
		IF SHOULD_HIDE_LOCAL_UI()
			SET_BLIP_DISPLAY(RadiusBlip[iCrate], DISPLAY_NOTHING)
		ENDIF
		NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - ADD_DROP_RADIUS_BLIP ") NET_PRINT_INT(iCrate) NET_NL()
	ENDIF
ENDPROC

//PURPOSE: Controls adding and removing radius blips for the drop locations
PROC CONTROL_RADIUS_BLIPS(INT iCrate)
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.CrateDropData.CrateData[iCrate].niCrate)
	AND serverBD.CrateDropData.CrateData[iCrate].iParticpantGotCrate = -1
	AND IS_SAFE_FOR_BLIPS()
		ADD_DROP_RADIUS_BLIP(iCrate)
	ELSE
		IF DOES_BLIP_EXIST(RadiusBlip[iCrate])
			REMOVE_BLIP(RadiusBlip[iCrate])
			NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - RADIUS BLIP REMOVED ") NET_PRINT_INT(iCrate) NET_NL()
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Controls adding and removing blips for the Crates
PROC CONTROL_CRATE_BLIPS(INT iCrate)
	IF IS_SAFE_FOR_BLIPS()
	AND NOT IS_ENTITY_ATTACHED_TO_ANY_PED(NET_TO_OBJ(serverBD.CrateDropData.CrateData[iCrate].niCrate))
		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_LandBlipChangeDone+iCrate)
		AND IS_BIT_SET(serverBD.CrateDropData.CrateData[iCrate].iBitSet, biCD_Crate_LandDone)
		AND DOES_BLIP_EXIST(CrateBlip[iCrate])
			SET_BLIP_ALPHA(CrateBlip[iCrate], 255)
			SHOW_HEIGHT_ON_BLIP(CrateBlip[iCrate], TRUE)	//NET_SHOW_HEIGHT_ON_BLIP(CrateBlip[iCrate])
			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_LandBlipChangeDone+iCrate)
			NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - CONTROL_CRATE_BLIPS - SET BLIP LAND CHANGES ") NET_PRINT_INT(iCrate) NET_NL()
		ELIF IS_BIT_SET(serverBD.CrateDropData.CrateData[iCrate].iBitSet, biCD_Crate_DropDone)	//biCD_Crate_LandDone)
			ADD_CRATE_BLIP(iCrate)
		ENDIF
		
		//DRAW_SPRITE_ON_OBJECTIVE_OBJECT_THIS_FRAME(NET_TO_OBJ(serverBD.CrateDropData.CrateData[iCrate].niCrate), MP_TAG_PACKAGES)
	ELSE
		IF DOES_BLIP_EXIST(CrateBlip[iCrate])
			REMOVE_BLIP(CrateBlip[iCrate])
			NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - CRATE BLIP REMOVED ") NET_PRINT_INT(iCrate) NET_NL()
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Tracks the Ai enemies that the player has killed
PROC TRACK_PED_KILLS(INT iCrate, INT iPed)
	IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.CrateDropData.LocationData[iCrate].Peds[iPed].niPed)
		IF NOT IS_BIT_SET(iTrackKillBitSet[iCrate], biTrackedPedKill+iPed)
			IF IS_ENTITY_DEAD(NET_TO_PED(serverBD.CrateDropData.LocationData[iCrate].Peds[iPed].niPed))
				//Check if player was Killer
				WEAPON_TYPE TempWeapon
				IF PLAYER_ID() = NETWORK_GET_DESTROYER_OF_NETWORK_ID(serverBD.CrateDropData.LocationData[iCrate].Peds[iPed].niPed, TempWeapon)
					//iTrackedKills++
					GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD, NET_TO_PED(serverBD.CrateDropData.LocationData[iCrate].Peds[iPed].niPed), "XPT_KAIE", XPTYPE_ACTION, XPCATEGORY_ACTION_KILLS, ROUND(XP_KILL_PED*g_sMPTunables.fxp_tunable_Crate_Drop_enemy_kills), 1)
					iEarnedXP += XP_KILL_PED
					iEnemiesKilled += 1
					SET_BIT(iTrackKillBitSet[iCrate], biTrackedPedKill+iPed)
					NET_PRINT_TIME() NET_PRINT("     ---------->     TRACK_KILLS - ENEMY KILLED ") /*NET_PRINT_INT(iTrackedKills)*/ NET_NL()
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_PED_INJURED(NET_TO_PED(serverBD.CrateDropData.LocationData[iCrate].Peds[iPed].niPed))
				CLEAR_BIT(iTrackKillBitSet[iCrate], biTrackedPedKill+iPed)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Controls the ped blips displaying for when they shoot
PROC CONTROL_PED_LOOP(INT iCrate)
	INT i
	REPEAT serverBD.CrateDropData.LocationData[iCrate].iNumPeds i
		UPDATE_ENEMY_NET_PED_BLIP( 	serverBD.CrateDropData.LocationData[iCrate].Peds[i].niPed, 
									PedBlipData[iCrate][i] )
		TRACK_PED_KILLS(iCrate, i)
	ENDREPEAT
ENDPROC

FUNC BOOL GET_BEST_MATCH_UNLOCKED_WEAPON_FOR_PLAYER(WEAPON_TYPE eWeapon, WEAPON_TYPE &eReturnWeapon)
	
	BOOL bFoundNewWeapon	= FALSE
	eReturnWeapon 			= eWeapon
	
	//WHILE iRank < GET_FM_WEAPON_UNLOCK_RANK(eWeapResult)
	WHILE NOT IS_MP_WEAPON_UNLOCKED(eReturnWeapon)
		
		NET_PRINT_TIME() NET_PRINT("     ---------->     GET_BEST_MATCH_UNLOCKED_WEAPON_FOR_PLAYER - Weapon = ") NET_PRINT(GET_WEAPON_NAME(eReturnWeapon)) NET_PRINT(" not unlocked, looking for an alternative") NET_NL()
	
		SWITCH eReturnWeapon
		
			CASE WEAPONTYPE_MINIGUN					eReturnWeapon = WEAPONTYPE_RPG						BREAK
			CASE WEAPONTYPE_RPG						eReturnWeapon = WEAPONTYPE_GRENADELAUNCHER			BREAK
			CASE WEAPONTYPE_GRENADELAUNCHER			eReturnWeapon = WEAPONTYPE_MG						BREAK
			
			CASE WEAPONTYPE_MOLOTOV					eReturnWeapon = WEAPONTYPE_GRENADE					BREAK
			CASE WEAPONTYPE_GRENADE					eReturnWeapon = WEAPONTYPE_SMOKEGRENADE				BREAK
			CASE WEAPONTYPE_SMOKEGRENADE			eReturnWeapon = WEAPONTYPE_PETROLCAN				BREAK
			CASE WEAPONTYPE_PETROLCAN				eReturnWeapon = WEAPONTYPE_STICKYBOMB				BREAK
			CASE WEAPONTYPE_STICKYBOMB				eReturnWeapon = WEAPONTYPE_PISTOL					BREAK
		
			CASE WEAPONTYPE_COMBATMG				eReturnWeapon = WEAPONTYPE_MG						BREAK
			CASE WEAPONTYPE_MG						eReturnWeapon = WEAPONTYPE_ASSAULTRIFLE				BREAK
			
			CASE WEAPONTYPE_HEAVYSNIPER				eReturnWeapon = WEAPONTYPE_SNIPERRIFLE				BREAK
			CASE WEAPONTYPE_SNIPERRIFLE				eReturnWeapon = WEAPONTYPE_ASSAULTRIFLE				BREAK
		
			CASE WEAPONTYPE_ADVANCEDRIFLE			eReturnWeapon = WEAPONTYPE_CARBINERIFLE				BREAK
			CASE WEAPONTYPE_CARBINERIFLE			eReturnWeapon = WEAPONTYPE_ASSAULTRIFLE				BREAK
			CASE WEAPONTYPE_ASSAULTRIFLE			eReturnWeapon = WEAPONTYPE_SMG						BREAK
			
			CASE WEAPONTYPE_SMG						eReturnWeapon = WEAPONTYPE_MICROSMG					BREAK
			CASE WEAPONTYPE_MICROSMG				eReturnWeapon = WEAPONTYPE_PISTOL					BREAK
			
			CASE WEAPONTYPE_ASSAULTSHOTGUN			eReturnWeapon = WEAPONTYPE_PUMPSHOTGUN				BREAK
			CASE WEAPONTYPE_PUMPSHOTGUN				eReturnWeapon = WEAPONTYPE_SAWNOFFSHOTGUN			BREAK
			CASE WEAPONTYPE_SAWNOFFSHOTGUN			eReturnWeapon = WEAPONTYPE_PISTOL					BREAK
			
			CASE WEAPONTYPE_APPISTOL				eReturnWeapon = WEAPONTYPE_COMBATPISTOL				BREAK
			CASE WEAPONTYPE_COMBATPISTOL			eReturnWeapon = WEAPONTYPE_PISTOL					BREAK
			CASE WEAPONTYPE_PISTOL					eReturnWeapon = WEAPONTYPE_KNIFE					BREAK
			DEFAULT 								eReturnWeapon = WEAPONTYPE_PISTOL					BREAK
		ENDSWITCH
		
		NET_PRINT_TIME() NET_PRINT("     ---------->     GET_BEST_MATCH_UNLOCKED_WEAPON_FOR_PLAYER - Alternative weapon to try = ") NET_PRINT(GET_WEAPON_NAME(eReturnWeapon)) NET_NL()
		bFoundNewWeapon = TRUE
		
	ENDWHILE
	
	IF bFoundNewWeapon
		NET_PRINT_TIME() NET_PRINT("     ---------->     GET_BEST_MATCH_UNLOCKED_WEAPON_FOR_PLAYER - Weapon = ") NET_PRINT(GET_WEAPON_NAME(eWeapon)) NET_PRINT(" not unlocked, replaced with alternative = ") NET_PRINT(GET_WEAPON_NAME(eReturnWeapon)) NET_NL()
		RETURN TRUE
	ENDIF
	
	NET_PRINT_TIME() NET_PRINT("     ---------->     GET_BEST_MATCH_UNLOCKED_WEAPON_FOR_PLAYER - Weapon = ") NET_PRINT(GET_WEAPON_NAME(eWeapon)) NET_PRINT(" was best match and available to use.") NET_NL()
	RETURN FALSE
ENDFUNC

//PURPOSE: Gives the player the contents of the crate
PROC GIVE_CRATE_CONTENTS(INT iCrate)
	
	//Random
	IF serverBD.CrateDropData.CrateData[iCrate].eContentsType = CD_CONTENTS_RANDOM
		serverBD.CrateDropData.CrateData[iCrate].eContentsType = INT_TO_ENUM(CD_CRATE_CONTENTS_TYPE , GET_RANDOM_INT_IN_RANGE(0, ENUM_TO_INT(CD_CONTENTS_RANDOM)))
		NET_PRINT_TIME() NET_PRINT("  ---->  CRATE DROP - GIVE_CRATE_CONTENTS - RANDOMISED") NET_NL()
	ENDIF
	
	INT iAmmo
	INT iCash
	VECTOR vPickupCoords
	IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.CrateDropData.CrateData[iCrate].niCrate)
		vPickupCoords = GET_ENTITY_COORDS(NET_TO_OBJ(serverBD.CrateDropData.CrateData[iCrate].niCrate))
	ELSE
		vPickupCoords = GET_PLAYER_COORDS(PLAYER_ID())
	ENDIF
	
	WEAPON_TYPE thisWeapon = GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID())
	IF thisWeapon = WEAPONTYPE_UNARMED
	OR thisWeapon = WEAPONTYPE_FLARE
		thisWeapon = gweapon_type_CurrentlyHeldWeapon
		PRINTLN("  ---->  CRATE DROP - GIVE_CRATE_CONTENTS - USE HELD WEAPON")
	ENDIF
	PRINTLN("  ---->  CRATE DROP - GIVE_CRATE_CONTENTS - ADD AMMO TO WEAPON ", GET_WEAPON_NAME(thisWeapon))
	ADD_AMMO_CLIPS_FOR_WEAPON(thisWeapon)
	
	//Always give some XP
	INT iXP
	IF serverBD.CrateDropData.CrateData[iCrate].eContentsType != CD_CONTENTS_DLC
		IF g_sMPTunables.iCrateDropXp < 0
			iXP = (250 * GET_RANDOM_INT_IN_RANGE(1, 5))
			NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - GIVE_CRATE_CONTENTS - XP - RANDOM XP = ") NET_PRINT_INT(iXP) NET_NL()
		ELSE
			iXP = g_sMPTunables.iCrateDropXp
			NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - GIVE_CRATE_CONTENTS - XP - TUNABLE XP iCrateDropXp = ") NET_PRINT_INT(iXP) NET_NL()
		ENDIF
	ELSE
		IF g_sMPTunables.iCrateDropSpecialXp < 0
			iXP = (250 * GET_RANDOM_INT_IN_RANGE(1, 5))
			NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - GIVE_CRATE_CONTENTS - XP - SPECIAL RANDOM XP = ") NET_PRINT_INT(iXP) NET_NL()
		ELSE
			iXP = g_sMPTunables.iCrateDropSpecialXp
			NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - GIVE_CRATE_CONTENTS - XP - SPECIAL TUNABLE XP iCrateDropSpecialXp = ") NET_PRINT_INT(iXP) NET_NL()
		ENDIF
	ENDIF
	
	iXP = ROUND(iXP*g_sMPTunables.fxp_tunable_Crate_Drop)
	NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - GIVE_CRATE_CONTENTS - XP MULTIPLIER - fxp_tunable_Crate_Drop = ") NET_PRINT_FLOAT(g_sMPTunables.fxp_tunable_Crate_Drop) NET_NL()
	NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - GIVE_CRATE_CONTENTS - XP - END XP = ") NET_PRINT_INT(iXP) NET_NL()
	
	SET_XP_ANIMATION_MULTIPLIER(XP_TRIPLE_MULT) 
	IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.CrateDropData.CrateData[iCrate].niCrate)
		GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD, NET_TO_OBJ(serverBD.CrateDropData.CrateData[iCrate].niCrate), "XPT_MEDIUMT", XPTYPE_COLLECT, XPCATEGORY_COLLECT_CRATE_DROP, iXP, 1)
	ELSE
		GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD, PLAYER_PED_ID(), "XPT_MEDIUMT",XPTYPE_COLLECT, XPCATEGORY_COLLECT_CRATE_DROP, iXP, 1)
	ENDIF
	iEarnedXP += iXP
	
	//PRINT_TICKER_WITH_INT("ACD_TXP", iXP)	//Crate Contents: ~1~XP
	NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - GIVE_CRATE_CONTENTS - CD_CONTENTS_XP ") NET_PRINT_INT(iCrate) NET_NL()
	
	SWITCH serverBD.CrateDropData.CrateData[iCrate].eContentsType
		CASE CD_CONTENTS_CASH
			iCash = MULTIPLY_CASH_BY_TUNABLE(serverBD.CrateDropData.CrateData[iCrate].iContentsAmount)
			IF iCash > g_sMPTunables.iCrateDropMaxTotalCash
				iCash = g_sMPTunables.iCrateDropMaxTotalCash
				NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - GIVE_CRATE_CONTENTS - CASH ABOVE LIMIT CAP AT $") NET_PRINT_INT(iCash) NET_NL()
			ENDIF
			
			PRINTLN("     ---------->     CRATE DROP - [MAGNATE_GANG_BOSS] - Cash before boss cut: $", iCash)
			GB_HANDLE_GANG_BOSS_CUT(iCash)
			PRINTLN("     ---------->     CRATE DROP - [MAGNATE_GANG_BOSS] - Cash after boss cut: $", iCash)
			
			IF iCash > 0
				IF USE_SERVER_TRANSACTIONS()
					INT iTransactionID
					TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_CRATE_DROP, iCash, iTransactionID)
				ELSE
					NETWORK_EARN_FROM_CRATE_DROP(iCash)
				ENDIF
							
				SET_CASH_REWARD_FOR_LOCATION(vPickupCoords, iCash)
				PRINT_TICKER_WITH_TWO_INTS("ACD_TCONT_CX", iCash, iXP)
				iEarnedCash += iCash
				SET_LAST_JOB_DATA(LAST_JOB_CRATE_DROP, iEarnedCash)
			ENDIF
			NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - GIVE_CRATE_CONTENTS - CD_CONTENTS_CASH ") NET_PRINT_INT(iCrate) NET_NL()
		BREAK
		/*CASE CD_CONTENTS_XP
			IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.CrateDropData.CrateData[iCrate].niCrate)
				GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(NET_TO_OBJ(serverBD.CrateDropData.CrateData[iCrate].niCrate), "XPT_MEDIUMT", serverBD.CrateDropData.CrateData[iCrate].iContentsAmount, 1, FALSE, FALSE)
			ELSE
				GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(PLAYER_PED_ID(), "XPT_MEDIUMT", serverBD.CrateDropData.CrateData[iCrate].iContentsAmount, 1, FALSE, FALSE)
			ENDIF
			PRINT_TICKER_WITH_INT("ACD_TXP", serverBD.CrateDropData.CrateData[iCrate].iContentsAmount)	//Crate Contents: ~1~XP
			NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - GIVE_CRATE_CONTENTS - CD_CONTENTS_XP ") NET_PRINT_INT(iCrate) NET_NL()
		BREAK*/
		CASE CD_CONTENTS_WEAPON
			WEAPON_TYPE eWeapon
			IF g_sMPTunables.bCrateDropDisableWeaponUnlockCheck = FALSE
				IF GET_BEST_MATCH_UNLOCKED_WEAPON_FOR_PLAYER(serverBD.CrateDropData.CrateData[iCrate].iContentsWeapon, eWeapon)
					NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - GIVE_CRATE_CONTENTS - crate weapon: ") NET_PRINT(GET_WEAPON_NAME(serverBD.CrateDropData.CrateData[iCrate].iContentsWeapon)) 
					NET_PRINT(" not unlocked for this player, alternative weapon given: ")	NET_PRINT(GET_WEAPON_NAME(eWeapon)) 
					NET_PRINT_INT(iCrate) NET_NL()
				ENDIF
			ELSE
				eWeapon = serverBD.CrateDropData.CrateData[iCrate].iContentsWeapon
				NET_PRINT("     ---------->     CRATE DROP - GIVE_CRATE_CONTENTS - WEAPON = ") NET_PRINT(GET_WEAPON_NAME(eWeapon)) 
			ENDIF
			
			IF eWeapon = WEAPONTYPE_MINIGUN
				iAmmo = serverBD.CrateDropData.CrateData[iCrate].iContentsAmount * 200
				NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - GIVE_CRATE_CONTENTS - MINIGUN AMMO (CLIPS * 200) = ") NET_PRINT_INT(iAmmo) NET_PRINT_INT(iCrate) NET_NL()
			ELSE
				iAmmo = serverBD.CrateDropData.CrateData[iCrate].iContentsAmount * GET_MAX_AMMO_IN_CLIP(PLAYER_PED_ID(), eWeapon)	
				NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - GIVE_CRATE_CONTENTS - CALCULATED AMMO = ") 
				NET_PRINT_INT(serverBD.CrateDropData.CrateData[iCrate].iContentsAmount) NET_PRINT(" * ") 
				NET_PRINT_INT(GET_MAX_AMMO_IN_CLIP(PLAYER_PED_ID(), eWeapon)) NET_PRINT(" = ") 
				NET_PRINT_INT(iAmmo) NET_PRINT(" ")  NET_PRINT_INT(iCrate) NET_NL()
			ENDIF
			PRINTLN("     ---------->     CRATE DROP - GIVE_CRATE_CONTENTS - iAmmo = ", iAmmo)
			GIVE_DELAYED_WEAPON_TO_PED(PLAYER_PED_ID(), eWeapon, iAmmo, TRUE)
			//PRINT_TICKER_WITH_STRING("ACD_TWEAP", GET_WEAPON_NAME(serverBD.CrateDropData.CrateData[iCrate].iContentsWeapon))	//Crate Contents: ~a~
			PRINT_TICKER_WITH_STRING_AND_INT("ACD_TCONT_XW", GET_WEAPON_NAME(eWeapon), iXP)
			NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - GIVE_CRATE_CONTENTS - CD_CONTENTS_WEAPON ") NET_PRINT_INT(iCrate) NET_NL()
		BREAK
		CASE CD_CONTENTS_DLC
			//Cash
			iCash = MULTIPLY_CASH_BY_TUNABLE(serverBD.CrateDropData.CrateData[iCrate].iContentsAmount)
			IF iCash > g_sMPTunables.iCrateDropSpecialCash
				iCash = g_sMPTunables.iCrateDropSpecialCash
				NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - GIVE_CRATE_CONTENTS - SPECIAL - CASH ABOVE LIMIT CAP AT $") NET_PRINT_INT(iCash) NET_NL()
			ENDIF
			
			PRINTLN("     ---------->     CRATE DROP - [MAGNATE_GANG_BOSS] - Cash before boss cut: $", iCash)
			GB_HANDLE_GANG_BOSS_CUT(iCash)
			PRINTLN("     ---------->     CRATE DROP - [MAGNATE_GANG_BOSS] - Cash after boss cut: $", iCash)
			
			IF iCash > 0
				IF USE_SERVER_TRANSACTIONS()
					INT iTransactionID
					TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_CRATE_DROP, iCash, iTransactionID)
				ELSE
					NETWORK_EARN_FROM_CRATE_DROP(iCash)
				ENDIF
				
				SET_CASH_REWARD_FOR_LOCATION(vPickupCoords, iCash)
			//	INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_MONEY_PICKED_FROM_CRATES, iCash)
				//PRINT_TICKER_WITH_INT("ACD_TCASH", iCash)	//Crate Contents: $~1~
				iEarnedCash += iCash
				SET_LAST_JOB_DATA(LAST_JOB_CRATE_DROP, iEarnedCash)
			ENDIF
			
			//XP
			//IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.CrateDropData.CrateData[iCrate].niCrate)
			//	GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(NET_TO_OBJ(serverBD.CrateDropData.CrateData[iCrate].niCrate), "XPT_MEDIUMT", ROUND(TO_FLOAT(serverBD.CrateDropData.CrateData[iCrate].iContentsAmount)/2), 1, FALSE, FALSE)
			//ELSE
			//	GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(PLAYER_PED_ID(), "XPT_MEDIUMT", ROUND(TO_FLOAT(serverBD.CrateDropData.CrateData[iCrate].iContentsAmount)/2), 1, FALSE, FALSE)
			//ENDIF
			//PRINT_TICKER_WITH_INT("ACD_TXP", ROUND(TO_FLOAT(serverBD.CrateDropData.CrateData[iCrate].iContentsAmount)/2))	//Crate Contents: ~1~XP
			
			//Weapon - Use Tunable (Default MiniGun)
			IF serverBD.CrateDropData.CrateData[iCrate].iContentsWeapon != WEAPONTYPE_INVALID
				IF serverBD.CrateDropData.CrateData[iCrate].iContentsWeapon != WEAPONTYPE_MINIGUN
					iAmmo = (g_sMPTunables.iCrateDropSpecialWeaponClips * GET_MAX_AMMO_IN_CLIP(PLAYER_PED_ID(), serverBD.CrateDropData.CrateData[iCrate].iContentsWeapon))	//4 
				ELSE
					iAmmo = (g_sMPTunables.iCrateDropSpecialWeaponClips * 300)
				ENDIF
				PRINTLN("     ---------->     CRATE DROP - GIVE_CRATE_CONTENTS - iAmmo = ", iAmmo)
				GIVE_DELAYED_WEAPON_TO_PED(PLAYER_PED_ID(), serverBD.CrateDropData.CrateData[iCrate].iContentsWeapon, iAmmo, TRUE)
				//PRINT_TICKER_WITH_STRING("ACD_TWEAP", GET_WEAPON_NAME(serverBD.CrateDropData.CrateData[iCrate].iContentsWeapon))	//Crate Contents: ~a~
			ENDIF
			
			//Armour
			NETWORK_RESTORE_LOCAL_PLAYER_ARMOUR()
			bCollectedArmour = TRUE
			//PRINT_TICKER("ACD_TARMO")	//Crate Contents: Body Armor
			
			
			// Clothing
			//-------------------------------------
			BOOL bItemOkay
			PED_COMP_TYPE_ENUM compType 
			PED_COMP_NAME_ENUM compName 
			compType = INT_TO_ENUM(PED_COMP_TYPE_ENUM, -1)
			compName = INT_TO_ENUM(PED_COMP_NAME_ENUM, -1)
			
			//CHECK SINGLE TUNABLE
			IF IS_PLAYER_MALE(PLAYER_ID())
				compType = serverBD.CrateDropData.CrateData[iCrate].eContentsMaleClothingType
				compName = serverBD.CrateDropData.CrateData[iCrate].eContentsMaleClothingName
			ELSE
				compType = serverBD.CrateDropData.CrateData[iCrate].eContentsFemaleClothingType
				compName = serverBD.CrateDropData.CrateData[iCrate].eContentsFemaleClothingName
			ENDIF
			IF IS_CLOTHING_ITEM_VALID(compType, compName)
				bItemOkay = TRUE
				PRINTLN("     ---------->     CRATE DROP - USE SINGLE TUNABLE FOR CLOTHING")
			ELSE
				PRINTLN("     ---------->     CRATE DROP - SINGLE TUNABLE FOR CLOTHING NOT VALID")
			ENDIF
			
			//CHECK RANDOM ARRAY TUNABLE
			IF bItemOkay = FALSE
			
				INT iAttempt, iRand
				
				// Few attempts at getting a valid item.
				iAttempt = 0
				WHILE iAttempt < 3
				AND bItemOkay = FALSE
					
					iRand = GET_RANDOM_INT_IN_RANGE(0, CLOTHING_ARRAY_SIZE)
					IF IS_PLAYER_MALE(PLAYER_ID())
						compType = serverBD.CrateDropData.CrateData[iCrate].eContentsMaleClothingTypeArray[iRand]
						compName = serverBD.CrateDropData.CrateData[iCrate].eContentsMaleClothingNameArray[iRand]
					ELSE
						compType = serverBD.CrateDropData.CrateData[iCrate].eContentsFemaleClothingTypeArray[iRand]
						compName = serverBD.CrateDropData.CrateData[iCrate].eContentsFemaleClothingNameArray[iRand]
					ENDIF
					IF IS_CLOTHING_ITEM_VALID(compType, compName)
						bItemOkay = TRUE
						PRINTLN("     ---------->     CRATE DROP - USE RANDOM ARRAY TUNABLE FOR CLOTHING - NUM = ", iRand)
					ELSE
						PRINTLN("     ---------->     CRATE DROP - RANDOM ARRAY TUNABLE FOR CLOTHING NOT VALID - NUM = ", iRand)
					ENDIF	
					
					iAttempt++
				ENDWHILE
			ENDIF
			
			//CHECK FULL ARRAY TUNABLES
			IF bItemOkay = FALSE
				INT i
				REPEAT CLOTHING_ARRAY_SIZE i
					IF IS_PLAYER_MALE(PLAYER_ID())
						compType = serverBD.CrateDropData.CrateData[iCrate].eContentsMaleClothingTypeArray[i]
						compName = serverBD.CrateDropData.CrateData[iCrate].eContentsMaleClothingNameArray[i]
					ELSE
						compType = serverBD.CrateDropData.CrateData[iCrate].eContentsFemaleClothingTypeArray[i]
						compName = serverBD.CrateDropData.CrateData[iCrate].eContentsFemaleClothingNameArray[i]
					ENDIF
					IF IS_CLOTHING_ITEM_VALID(compType, compName)
						bItemOkay = TRUE
						i = CLOTHING_ARRAY_SIZE
						PRINTLN("     ---------->     CRATE DROP - USE FULL ARRAY TUNABLE FOR CLOTHING - NUM = ", i)
					ELSE
						PRINTLN("     ---------->     CRATE DROP - FULL ARRAY TUNABLE FOR CLOTHING NOT VALID - NUM = ", i)
					ENDIF
				ENDREPEAT
			ENDIF
			
			// If no valid items, pass dummy to ticker feed.
			IF bItemOkay = FALSE
				compName = DUMMY_PED_COMP
			ENDIF
			
			//GIVE ITEM
			TEXT_LABEL_31 tlRewardLabel
			NET_CLOUD_REWARD_CLOTHING_RESULT eRewardResult
			eRewardResult = GIVE_MP_REWARD_CLOTHING(compType, compName, tlRewardLabel)
			
			SWITCH eRewardResult
				CASE NET_CLOUD_REWARD_CLOTHING_INVALID
					PRINT_TICKER_WITH_STRING_AND_TWO_INTS("ACD_TCONT_CXWA",
								GET_WEAPON_NAME(serverBD.CrateDropData.CrateData[iCrate].iContentsWeapon),
								iCash,
								iXP)
				BREAK
				
				CASE NET_CLOUD_REWARD_CLOTHING_SHIRT_W_DECAL
					PRINT_TICKER_WITH_TWO_STRINGS_AND_TWO_INTS("ACD_TCONT_CXWAC",
								GET_WEAPON_NAME(serverBD.CrateDropData.CrateData[iCrate].iContentsWeapon),
								tlRewardLabel,
								iCash,
								iXP)
				BREAK
				
				CASE NET_CLOUD_REWARD_CLOTHING_VALID_NOT_OWNED
					PRINT_TICKER_WITH_TWO_STRINGS_AND_TWO_INTS("ACD_TCONT_CXWAC",
								GET_WEAPON_NAME(serverBD.CrateDropData.CrateData[iCrate].iContentsWeapon),
								tlRewardLabel,
								iCash,
								iXP)
				BREAK
			ENDSWITCH
			
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_UNIQUECRATES, 1)
			IF GET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_FMPICKUPDLCCRATE1ST) = FALSE
				SET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_FMPICKUPDLCCRATE1ST, TRUE)
				NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - GIVE_CRATE_CONTENTS - CD_CONTENTS_DLC  - AWARD GIVEN - MP_AWARD_FMPICKUPDLCCRATE1ST")NET_NL()
			ENDIF
			NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - GIVE_CRATE_CONTENTS - CD_CONTENTS_DLC ") NET_PRINT_INT(iCrate) NET_NL()
		BREAK
		/*CASE CD_CONTENTS_AMMO
		
		BREAK
		CASE CD_CONTENTS_CLOTHING
			
		BREAK
		CASE CD_CONTENTS_UNLOCK
			
		BREAK
		CASE CASE CD_CONTENTS_DRUGS
			
		BREAK*/
		DEFAULT
			iCash = MULTIPLY_CASH_BY_TUNABLE(serverBD.CrateDropData.CrateData[iCrate].iContentsAmount)
			
			PRINTLN("     ---------->     CRATE DROP - [MAGNATE_GANG_BOSS] - Cash before boss cut: $", iCash)
			GB_HANDLE_GANG_BOSS_CUT(iCash)
			PRINTLN("     ---------->     CRATE DROP - [MAGNATE_GANG_BOSS] - Cash after boss cut: $", iCash)
			
			IF iCash > 0
				IF USE_SERVER_TRANSACTIONS()
					INT iTransactionID
					TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_CRATE_DROP, iCash, iTransactionID)
				ELSE
					NETWORK_EARN_FROM_CRATE_DROP(iCash)
				ENDIF
				SET_CASH_REWARD_FOR_LOCATION(vPickupCoords, iCash)
				//INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_MONEY_PICKED_FROM_CRATES, iCash)
				//PRINT_TICKER_WITH_INT("ACD_TCASH", iCash)
				PRINT_TICKER_WITH_TWO_INTS("ACD_TCONT_CX", iCash, iXP)
				iEarnedCash += iCash
				SET_LAST_JOB_DATA(LAST_JOB_CRATE_DROP, iEarnedCash)
			ENDIF
			NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - GIVE_CRATE_CONTENTS - DEFAULT - USE CASH ") NET_PRINT_INT(iCrate) NET_NL()
		BREAK
	ENDSWITCH
	
	INCREMENT_BY_MP_INT_CHARACTER_AWARD(MP_AWARD_FMCRATEDROPS, 1)
	
	NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - GIVE_CRATE_CONTENTS - DONE ") NET_PRINT_INT(iCrate) NET_NL()

ENDPROC

//PURPOSE: Controls if this player has picked up a crate
PROC CONTROL_CRATE_PICKUP_CLIENT(INT iCrate)
	
	//UPDATE TAKE MONEY DATA
	/*IF IS_BIT_SET(serverBD.CrateDropData.CrateData[iCrate].iBitSet, biCD_Crate_LandDone)
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.CrateDropData.CrateData[iCrate].niCrate)
			IF HAS_NET_TIMER_EXPIRED(iTakeMoneyDataUpdateTimer[iCrate], 1000)
				VECTOR vCratePos = GET_ENTITY_COORDS(NET_TO_OBJ(serverBD.CrateDropData.CrateData[iCrate].niCrate))
				IF NOT ARE_VECTORS_ALMOST_EQUAL(TakeMoneyData[iCrate].vMoneyCoord, vCratePos, 0.25)
					TakeMoneyData[iCrate].vMoneyCoord 				= vCratePos+<<0, 0, 1>>	//<<968.84, -1831.57, 36.41>>
					TakeMoneyData[iCrate].vPlayerTakeMoneyCoord 	= vCratePos+<<0, 0, 1>>	//<<968.7944, -1832.1832, 35.1064>>
					TakeMoneyData[iCrate].fPlayerTakeMoneyHeading	= 0			//3.2327
					TakeMoneyData[iCrate].vPlayerTakeMoneyDimensions = <<0.75, 0.75, 1.2>>		//<<0.75, 0.75, 1.5>>
					TakeMoneyData[iCrate].vFloatingHelpMin			= vCratePos-<<TakeMoneyData[iCrate].vPlayerTakeMoneyDimensions.x, TakeMoneyData[iCrate].vPlayerTakeMoneyDimensions.y, TakeMoneyData[iCrate].vPlayerTakeMoneyDimensions.z>>	//<<0, 10, -5>>	//<<964.221985,-1835.220825,35.106377>>
					TakeMoneyData[iCrate].vFloatingHelpMax			= vCratePos+<<TakeMoneyData[iCrate].vPlayerTakeMoneyDimensions.x, TakeMoneyData[iCrate].vPlayerTakeMoneyDimensions.y, TakeMoneyData[iCrate].vPlayerTakeMoneyDimensions.z>>	//<<0, -10, 5>>	//<<979.696716,-1836.617310,37.533894>>
					TakeMoneyData[iCrate].fFloatingHelpWidth		= (TakeMoneyData[iCrate].vPlayerTakeMoneyDimensions.x*2)				//20
					TakeMoneyData[iCrate].iMoneyToTake				= 15000
					TakeMoneyData[iCrate].iTimeToTake 				= 15000
					TakeMoneyData[iCrate].iMoneyTakenCounterMax		= (TakeMoneyData[iCrate].iTimeToTake/TAKE_MONEY_SPEED_MAX)
					TakeMoneyData[iCrate].iAnimSetToUse				= TM_ANIM_SET_OPEN_CRATE
					NET_PRINT("     ---------->     CRATE DROP - UPDATED TakeMoneyData ")NET_PRINT_INT(iCrate)NET_NL()
					RESET_NET_TIMER(iTakeMoneyDataUpdateTimer[iCrate])
				ENDIF
			ENDIF
		ENDIF
	ENDIF*/
	
	//Check if another player is already Taking Money
	/*IF serverBD.CrateDropData.CrateData[iCrate].TakeMoneyServerData.iParticipantTakingMoney = -1
	OR serverBD.CrateDropData.CrateData[iCrate].TakeMoneyServerData.iParticipantTakingMoney = PARTICIPANT_ID_TO_INT()
		IF TakeMoneyData[iCrate].bTakeMoneyDisabled = TRUE
			TakeMoneyData[iCrate].bTakeMoneyDisabled = FALSE
		ENDIF
	ELSE
		IF TakeMoneyData[iCrate].bTakeMoneyDisabled = FALSE
			TakeMoneyData[iCrate].bTakeMoneyDisabled = TRUE
		ENDIF
	ENDIF*/
	
	//Control Player Taking Money
	/*IF bTakeMoneyDone[iCrate] = FALSE
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.CrateDropData.CrateData[iCrate].niCrate)
			IF HAVE_TAKE_MONEY_ASSETS_LOADED(TakeMoneyData[iCrate])
				IF IS_NET_PLAYER_OK(PLAYER_ID())
					PROCESS_PLAYER_TAKE_MONEY(TakeMoneyData[iCrate], serverBD.CrateDropData.CrateData[iCrate].TakeMoneyServerData, playerBD[PARTICIPANT_ID_TO_INT()].TakeMoneyClientData[iCrate])
					IF HAS_PLAYER_TAKEN_MONEY(TakeMoneyData[iCrate])
						IF TAKE_CONTROL_OF_NET_ID(serverBD.CrateDropData.CrateData[iCrate].niCrate)
							PREVENT_COLLECTION_OF_PORTABLE_PICKUP(NET_TO_OBJ(serverBD.CrateDropData.CrateData[iCrate].niCrate), FALSE)
							ATTACH_PORTABLE_PICKUP_TO_PED(NET_TO_OBJ(serverBD.CrateDropData.CrateData[iCrate].niCrate), PLAYER_PED_ID())
							bTakeMoneyDone[iCrate] = TRUE
							NET_PRINT("     ---------->     CRATE DROP   TAKE MONEY - bTakeMoneyDone = TRUE  ") NET_PRINT_INT(iCrate)NET_NL()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF*/
	
	IF NOT IS_BIT_SET(iCrateRewadrGivenBitSet, iCrate)
	//AND bTakeMoneyDone[iCrate] = TRUE
		IF serverBD.CrateDropData.CrateData[iCrate].iParticpantGotCrate = -1
			//Check if I have picked it up.
			IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_PickedUpCrate+iCrate)
				IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.CrateDropData.CrateData[iCrate].niCrate)
					IF IS_ENTITY_ATTACHED_TO_ENTITY(NET_TO_OBJ(serverBD.CrateDropData.CrateData[iCrate].niCrate), PLAYER_PED_ID())
					//AND TAKE_CONTROL_OF_NET_ID(serverBD.CrateDropData.CrateData[iCrate].niCrate)
						/*IF serverBD.CrateDropData.CrateData[iCrate].PickupSoundID <> -1
							STOP_SOUND(serverBD.CrateDropData.CrateData[iCrate].PickupSoundID)
							RELEASE_SOUND_ID(serverBD.CrateDropData.CrateData[iCrate].PickupSoundID)
							serverBD.CrateDropData.CrateData[iCrate].PickupSoundID = -1
						ENDIF
						serverBD.CrateDropData.CrateData[iCrate].PickupSoundID = GET_NETWORK_ID_FROM_SOUND_ID(GET_SOUND_ID())*/
						CLEANUP_CRATE_BLEEP_SOUND(iCrate)
						CLEANUP_CRATE_BEACON(iCrate)
						
						PLAY_SOUND_FROM_ENTITY(-1, "Crate_Collect", NET_TO_OBJ(serverBD.CrateDropData.CrateData[iCrate].niCrate), "MP_CRATE_DROP_SOUNDS", TRUE, 15)
						
						GIVE_CRATE_CONTENTS(iCrate)
						SET_BIT(iCrateRewadrGivenBitSet, iCrate)
						
						SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerEventData
						IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_SpecialCrateDrop)
							TickerEventData.TickerEvent = TICKER_EVENT_CRATE_DROP_PLAYER_PICKUP
							REQUEST_SYSTEM_ACTIVITY_TYPE_COLLECTED_CRATE_DROP()
						ELSE
							TickerEventData.TickerEvent = TICKER_EVENT_CRATE_DROP_PLAYER_PICKUP_DLC
							REQUEST_SYSTEM_ACTIVITY_TYPE_COLLECTED_SPECIAL_CRATE_DROP()
						ENDIF
						TickerEventData.playerID = PLAYER_ID()
						BROADCAST_TICKER_EVENT(TickerEventData, ALL_PLAYERS_ON_SCRIPT(FALSE))
						
						RESET_REMINDER_TIMER_FOR_ACTIVITY(FMMC_TYPE_CRATE_DROP)
						
						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_PickedUpCrate+iCrate)
						NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - CONTROL_CRATE_PICKUP_CLIENT - PLAYER HAS PICKED UP CRATE ") NET_PRINT_INT(iCrate) NET_NL()
						
					ENDIF
				ENDIF
			ENDIF
			
		ELIF serverBD.CrateDropData.CrateData[iCrate].iParticpantGotCrate = 9999
			//PRINT_TICKER("ACD_TRIVC")
			CLEANUP_CRATE_BLEEP_SOUND(iCrate)
			CLEANUP_CRATE_BEACON(iCrate)
			SET_BIT(iCrateRewadrGivenBitSet, iCrate)
			NET_PRINT_TIME() NET_PRINT("  ---->  CRATE DROP - CONTROL_CRATE_PICKUP_CLIENT - RIVAL PICKED UP CRATE ") NET_PRINT_INT(iCrate) NET_NL()
		/*ELIF serverBD.CrateDropData.CrateData[iCrate].iParticpantGotCrate = PARTICIPANT_ID_TO_INT()
			GIVE_CRATE_CONTENTS(iCrate)
			SET_BIT(iCrateRewadrGivenBitSet, iCrate)*/
		ELSE
			/*IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_SpecialCrateDrop)
				PRINT_TICKER_WITH_PLAYER_NAME("ACD_TPLAY", NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(serverBD.CrateDropData.CrateData[iCrate].iParticpantGotCrate)))	//~a~ ~s~picked up a crate.
			ELSE
				PRINT_TICKER_WITH_PLAYER_NAME("ACD_TSPLAY", NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(serverBD.CrateDropData.CrateData[iCrate].iParticpantGotCrate)))	//~a~ ~s~picked up a DLC crate.
			ENDIF*/
			CLEANUP_CRATE_BLEEP_SOUND(iCrate)
			CLEANUP_CRATE_BEACON(iCrate)
			SET_BIT(iCrateRewadrGivenBitSet, iCrate)
			NET_PRINT_TIME() NET_PRINT("  ---->  CRATE DROP - CONTROL_CRATE_PICKUP_CLIENT - ANOTHER PLAYER PICKED UP CRATE ") NET_PRINT_INT(iCrate) NET_NL()
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Controls the creation of the Ai enemies
PROC CONTROL_AI_CREATION(INT iCrate)
	IF NOT IS_BIT_SET(serverBD.CrateDropData.CrateData[iCrate].iBitSet, biCD_Crate_AiSetup)
		CLEAR_BIT(serverBD.CrateDropData.CrateData[0].iBitSet, biCD_Crate_AllAiSetup)
		
		//Random
		IF serverBD.CrateDropData.LocationData[iCrate].eAiSetup = CD_AI_RANDOM
			//serverBD.CrateDropData.LocationData[iCrate].eAiSetup = INT_TO_ENUM(CD_AI_SETUP , GET_RANDOM_INT_IN_RANGE(0, ENUM_TO_INT(CD_AI_RANDOM)))
			
			IF GET_RANDOM_BOOL()
				serverBD.CrateDropData.LocationData[iCrate].eAiSetup = CD_AI_ON_FOOT_BEFORE
			ELSE
				serverBD.CrateDropData.LocationData[iCrate].eAiSetup = CD_AI_VEHICLE_ARRIVE
				serverBD.iReservedVehicles++
			ENDIF
			
			//No longer do 'No Peds'
			/*INT iRand = GET_RANDOM_INT_IN_RANGE(0, 10)
			IF iRand < 4
				serverBD.CrateDropData.LocationData[iCrate].eAiSetup = CD_AI_ON_FOOT_BEFORE
			ELIF iRand > 5
				serverBD.CrateDropData.LocationData[iCrate].eAiSetup = CD_AI_VEHICLE_ARRIVE
				serverBD.iReservedVehicles++
			ELSE
				serverBD.CrateDropData.LocationData[iCrate].eAiSetup = CD_AI_NONE
				serverBD.iReservedPeds -= serverBD.CrateDropData.LocationData[iCrate].iNumPeds
			ENDIF*/
			
			NET_PRINT_TIME() NET_PRINT("  ---->  CRATE DROP - CONTROL_AI_CREATION - RANDOMISED ") NET_PRINT_INT(iCrate) NET_NL()
		ENDIF
		
		//TEMP TEST
		//serverBD.CrateDropData.LocationData[iCrate].eAiSetup = CD_AI_VEHICLE_ARRIVE
		//serverBD.CrateDropData.LocationData[iCrate].iNumPeds = 4
		
		SWITCH serverBD.CrateDropData.LocationData[iCrate].eAiSetup
			CASE CD_AI_VEHICLE_ARRIVE
				IF IS_BIT_SET(serverBD.CrateDropData.CrateData[iCrate].iBitSet, biCD_Crate_LandAnimStarted)	//biCD_Crate_PickedUp
					IF CREATE_VEHICLE_AND_PEDS(iCrate)
						NET_PRINT_TIME() NET_PRINT("  ---->  CRATE DROP - CONTROL_AI_CREATION - CD_AI_ON_FOOT_BEFORE ") NET_PRINT_INT(iCrate) NET_NL()
						SET_BIT(serverBD.CrateDropData.CrateData[iCrate].iBitSet, biCD_Crate_AiSetup)
					ENDIF
				ENDIF
			BREAK
			CASE CD_AI_ON_FOOT_BEFORE
				IF CREATE_ON_FOOT_PEDS(iCrate)
					NET_PRINT_TIME() NET_PRINT("  ---->  CRATE DROP - CONTROL_AI_CREATION - CD_AI_ON_FOOT_BEFORE ") NET_PRINT_INT(iCrate) NET_NL()
					SET_BIT(serverBD.CrateDropData.CrateData[iCrate].iBitSet, biCD_Crate_AiSetup)
				ENDIF
			BREAK
			//CASE CD_AI_HELICOPTER
			//	NET_PRINT_TIME() NET_PRINT("  ---->  CRATE DROP - CONTROL_AI_CREATION - CD_AI_HELICOPTER - NOT SETUP JUST YET ") NET_PRINT_INT(iCrate) NET_NL()
			//	SET_BIT(serverBD.CrateDropData.CrateData[iCrate].iBitSet, biCD_Crate_AiSetup)
			//BREAK
			DEFAULT
				NET_PRINT_TIME() NET_PRINT("  ---->  CRATE DROP - CONTROL_AI_CREATION - CD_AI_NONE ") NET_PRINT_INT(iCrate) NET_NL()
				SET_BIT(serverBD.CrateDropData.CrateData[iCrate].iBitSet, biCD_Crate_AiSetup)
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

//PURPOSE: Controls how many entities are reserved based on serverBD
PROC MAINTAIN_RESERVED_ENTITIES()
	INT iCurrentlyReserved = GET_NUM_RESERVED_MISSION_VEHICLES(FALSE)
	IF serverBD.iReservedVehicles != iCurrentlyReserved
		IF serverBD.iReservedVehicles < iCurrentlyReserved
		OR CAN_RESERVE_NETWORK_VEHICLES_FOR_THIS_SCRIPT(serverBD.iReservedVehicles, FALSE, TRUE)
			IF serverBD.iReservedVehicles >= GET_NUM_CREATED_MISSION_VEHICLES()
				RESERVE_NETWORK_MISSION_VEHICLES(serverBD.iReservedVehicles)
				NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - MAINTAIN_RESERVED_ENTITIES - RESERVED VECHILES = ") NET_PRINT_INT(serverBD.iReservedVehicles) NET_NL()
			ENDIF
		ENDIF
	ENDIF
	
	iCurrentlyReserved = GET_NUM_RESERVED_MISSION_PEDS(FALSE)
	IF serverBD.iReservedPeds != iCurrentlyReserved
		IF serverBD.iReservedPeds < iCurrentlyReserved
		OR CAN_RESERVE_NETWORK_PEDS_FOR_THIS_SCRIPT(serverBD.iReservedPeds, FALSE, TRUE)
			IF serverBD.iReservedPeds >= GET_NUM_CREATED_MISSION_PEDS()
				RESERVE_NETWORK_MISSION_PEDS(serverBD.iReservedPeds)
				NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - MAINTAIN_RESERVED_ENTITIES - RESERVED PEDS = ") NET_PRINT_INT(serverBD.iReservedPeds) NET_NL()
			ENDIF
		ENDIF
	ENDIF
	
	iCurrentlyReserved = GET_NUM_RESERVED_MISSION_OBJECTS(FALSE)
	IF serverBD.iReservedObjects != iCurrentlyReserved
		IF serverBD.iReservedObjects < iCurrentlyReserved
		OR CAN_RESERVE_NETWORK_OBJECTS_FOR_THIS_SCRIPT(serverBD.iReservedObjects, FALSE, TRUE)
			IF serverBD.iReservedObjects >= GET_NUM_CREATED_MISSION_OBJECTS()
				RESERVE_NETWORK_MISSION_OBJECTS(serverBD.iReservedObjects)
				NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - MAINTAIN_RESERVED_ENTITIES - RESERVED OBJECTS = ") NET_PRINT_INT(serverBD.iReservedObjects) NET_NL()
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Process the CRATE DROP stages for the Client
PROC PROCESS_CRATE_DROP_CLIENT()
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	OPEN_SCRIPT_PROFILE_MARKER_GROUP("PROCESS_CRATE_DROP_CLIENT")
	#ENDIF
	#ENDIF
	
	//playerBD[PARTICIPANT_ID_TO_INT()].eCrateDropStage = serverBD.eCrateDropStage
	MAINTAIN_RESERVED_ENTITIES()
	
	SWITCH playerBD[PARTICIPANT_ID_TO_INT()].eCrateDropStage
		CASE eCD_SETUP
			
			IF serverBD.eCrateDropStage > eCD_SETUP
				//CREATE_SEQUENCES()
				
				INT i
				REPEAT MAX_NUM_CRATE_DROPS i
					MPGlobalsAmbience.vCrateDropLocations[i] = serverBD.CrateDropData.LocationData[i].vLocation
					NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - MPGlobalsAmbience.vCrateDropLocations[") NET_PRINT_INT(i) NET_PRINT("] = ") NET_PRINT_VECTOR(MPGlobalsAmbience.vCrateDropLocations[i]) NET_NL()
				ENDREPEAT
				
				IF LOAD_CRATE_ANIMS()
				AND CREATE_FLARE_CANISTER()
				AND LOAD_INTERACT_PARTICLE_FX()
				//AND REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\MP_CRATE_DROP_SOUNDS")	//		
					IF NOT IS_BIT_SET(serverBD.CrateDropData.CrateData[0].iBitSet, biCD_Crate_DropDone)
					AND IS_FM_TYPE_UNLOCKED(FMMC_TYPE_CRATE_DROP)	//AND GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType != FMMC_TYPE_GUN_INTRO
					AND HAS_PLAYER_BEEN_IN_SESSION_THIS_LONG(CRATE_DROP_DISPLAY_DELAY)
					AND NOT IS_TRANSITION_ACTIVE()
						IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_SpecialCrateDrop)
							PRINT_TICKER("ACD_TICKS")	//Crate drop inbound.
						ELSE
							PRINT_TICKER("ACD_TSC0")	//DLC Crate drop inbound.
						ENDIF
					ENDIF
					STORE_LAST_LOCATION()
					playerBD[PARTICIPANT_ID_TO_INT()].eCrateDropStage = eCD_DROP_AND_COLLECT
					NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - PLAYER STAGE = eCD_DROP_AND_COLLECT    <----------     ") NET_NL()
				ENDIF
			ENDIF
			
		BREAK
		
		CASE eCD_DROP_AND_COLLECT
			
			CONTROL_HELP_TEXT()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("CONTROL_HELP_TEXT")
			#ENDIF
			#ENDIF
				
			/*CONTROL_PLANE_BLIP()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("CONTROL_PLANE_BLIP")
			#ENDIF
			#ENDIF*/
			
			/*CONTROL_PLANE_BODY()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("CONTROL_PLANE_BODY")
			#ENDIF
			#ENDIF*/
			
			CONTROL_TICKERS()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("CONTROL_TICKERS")
			#ENDIF
			#ENDIF
			
			
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_PRE_LOOP_MARKER("loop")
			#ENDIF
			#ENDIF
						
			INT i
			REPEAT serverBD.CrateDropData.iNumDrops i
			
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE
				
				#ENDIF
				#ENDIF
			
				CONTROL_CRATE_PICKUP_CLIENT(i)
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("CONTROL_CRATE_PICKUP_CLIENT")
				#ENDIF
				#ENDIF
				
				IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.CrateDropData.CrateData[i].niCrate)
				AND (NETWORK_DOES_NETWORK_ID_EXIST(serverBD.DCrateData[i].NetID) OR IS_BIT_SET(serverBD.iServerBitSet, biS_SpecialCrateDrop))		//**TWH - RLB - FIX FOR PT 1413614
					CONTROL_CRATE_BLIPS(i)
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE
					ADD_SCRIPT_PROFILE_MARKER("CONTROL_CRATE_BLIPS")
					#ENDIF
					#ENDIF
					
					
					CONTROL_CRATE_TICKERS(i)
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE
					ADD_SCRIPT_PROFILE_MARKER("CONTROL_CRATE_TICKERS")
					#ENDIF
					#ENDIF
					
					CONTROL_CRATE_LANDING(i, serverBD.CrateDropData.LocationData[i].vSpecificForceToGroundLocation)
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE
					ADD_SCRIPT_PROFILE_MARKER("CONTROL_CRATE_LANDING")
					#ENDIF
					#ENDIF
					
					//MAINTAIN_STAGGERED_PARTICIPANT_LOOP_CLIENT(i)
					
					//CONTROL_CRATE_DELETION(i)
				ENDIF
				CONTROL_RADIUS_BLIPS(i)
				
				//Checks to do if the player is in the area
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), serverBD.CrateDropData.LocationData[i].vLocation, <<CRATE_DROP_LOCATION_RADIUS, CRATE_DROP_LOCATION_RADIUS, CRATE_DROP_LOCATION_RADIUS>>)
					
					Enable_Temporary_MP_Comms_Incoming_Calls_Block()
					
					CONTROL_PED_LOOP(i)
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE
					ADD_SCRIPT_PROFILE_MARKER("CONTROL_PED_LOOP")
					#ENDIF
					#ENDIF
					
					//Set Player Spawn Area
					FLOAT fThreeQ
					fThreeQ = (CRATE_DROP_LOCATION_RADIUS/4)*3
					IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), serverBD.CrateDropData.LocationData[i].vLocation, <<fThreeQ, fThreeQ, fThreeQ>>)
						SET_PLAYER_SPAWN_AREA(i, fThreeQ)
					//ELSE
					//	CLEAR_PLAYER_SPAWN_AREA()	
					ENDIF
				ELSE
					CLEAR_PLAYER_SPAWN_AREA()	
				ENDIF
					
			ENDREPEAT
									
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_POST_LOOP_MARKER()
			ADD_SCRIPT_PROFILE_MARKER("Control Loop")
			#ENDIF
			#ENDIF
			
			IF serverBD.eCrateDropStage >= eCD_CLEANUP
				playerBD[PARTICIPANT_ID_TO_INT()].eCrateDropStage = eCD_CLEANUP
				NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - PLAYER STAGE = eCD_CLEANUP    <----------     ") NET_NL()
			ENDIF
		BREAK
				
		CASE eCD_CLEANUP
			NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - SCRIPT CLEANUP B     <----------     ") NET_NL()
			SCRIPT_CLEANUP()
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	CLOSE_SCRIPT_PROFILE_MARKER_GROUP()
	#ENDIF
	#ENDIF	
	
ENDPROC

//PURPOSE: Process the CRATE DROP stages for the Server
PROC PROCESS_CRATE_DROP_SERVER()

	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	OPEN_SCRIPT_PROFILE_MARKER_GROUP("PROCESS_CRATE_DROP_SERVER")
	#ENDIF
	#ENDIF

	SWITCH serverBD.eCrateDropStage
		CASE eCD_SETUP
			//Move on once server has started race
			//IF CREATE_PLANE_AND_PILOT()
			IF LOAD_CRATE_ASSETS()
			AND CREATE_FLARE_CANISTER()
			AND ADD_CRATE_FLARES()
				serverBD.eCrateDropStage = eCD_DROP_AND_COLLECT
				NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - SERVER STAGE = eCD_DROP_AND_COLLECT    <----------     ") NET_NL()
			ENDIF
		BREAK
		
		CASE eCD_DROP_AND_COLLECT
			
			INT i
			SET_BIT(serverBD.CrateDropData.CrateData[0].iBitSet, biCD_Crate_AllCratesDropped)
			SET_BIT(serverBD.CrateDropData.CrateData[0].iBitSet, biCD_Crate_AllCratesPickedUp)
			SET_BIT(serverBD.CrateDropData.CrateData[0].iBitSet, biCD_Crate_NoCratesExist)
			SET_BIT(serverBD.CrateDropData.CrateData[0].iBitSet, biCD_Crate_AllAiSetup)
			REPEAT serverBD.CrateDropData.iNumDrops i
				CONTROL_CRATE_DROPPING(i)
				CONTROL_AI_CREATION(i)
				CONTROL_CRATE_TIMEOUT(i)
			ENDREPEAT
			
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
				ADD_SCRIPT_PROFILE_MARKER("CONTROL_CRATE_DROPPING and AI_CREATION loop")
			#ENDIF
			#ENDIF
			
			//Check for End
			IF IS_BIT_SET(serverBD.CrateDropData.CrateData[0].iBitSet,  biCD_Crate_AllCratesDropped)
			AND IS_BIT_SET(serverBD.CrateDropData.CrateData[0].iBitSet, biCD_Crate_AllAiSetup)
				IF IS_BIT_SET(serverBD.CrateDropData.CrateData[0].iBitSet,  biCD_Crate_AllCratesPickedUp)
					IF IS_BIT_SET(serverBD.CrateDropData.CrateData[0].iBitSet, biCD_Crate_NoPlayerAtAnyDropLocation)
					OR HAS_NET_TIMER_EXPIRED(EndScriptTimer, iEndScriptDelay)
						CLEANUP_ALL_CRATE_FLARES()
						serverBD.eCrateDropStage = eCD_CLEANUP
						NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - SERVER STAGE = eCD_CLEANUP - biCD_Crate_AllCratesPickedUp    <----------     ") NET_NL()
					ENDIF
				ELIF IS_BIT_SET(serverBD.CrateDropData.CrateData[0].iBitSet,  biCD_Crate_NoCratesExist)
					IF IS_BIT_SET(serverBD.CrateDropData.CrateData[0].iBitSet, biCD_Crate_NoPlayerAtAnyDropLocation)
					OR HAS_NET_TIMER_EXPIRED(EndScriptTimer, iEndScriptDelay)
						CLEANUP_ALL_CRATE_FLARES()
						serverBD.eCrateDropStage = eCD_CLEANUP
						NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - SERVER STAGE = eCD_CLEANUP - biCD_Crate_NoCratesExist    <----------     ") NET_NL()
					ENDIF
				ENDIF
			ENDIF
			
			//Lower Reservation Checks
			/*IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_PlaneReservationReduced)
				IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.CrateDropData.PlaneData.Plane.niVeh)
					serverBD.iReservedVehicles--
					SET_BIT(serverBD.iServerBitSet, biS_PlaneReservationReduced)
					NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - REDUCED - PLANE DOESN'T EXIST - serverBD.iReservedVehicles = ") NET_PRINT_INT(serverBD.iReservedVehicles) NET_NL()
				ENDIF
			ENDIF
			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_PilotReservationReduced)
				IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.CrateDropData.PlaneData.Pilot.niPed)
					serverBD.iReservedPeds--
					SET_BIT(serverBD.iServerBitSet, biS_PilotReservationReduced)
					NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - REDUCED - PILOT DOESN'T EXIST - serverBD.iReservedPeds = ") NET_PRINT_INT(serverBD.iReservedPeds) NET_NL()
				ENDIF
			ENDIF*/
			
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
				ADD_SCRIPT_PROFILE_MARKER("Check for end")
			#ENDIF
			#ENDIF
			
		BREAK
				
		CASE eCD_CLEANUP
			//Do Nothing
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	CLOSE_SCRIPT_PROFILE_MARKER_GROUP()
	#ENDIF
	#ENDIF	
	
ENDPROC

#IF IS_DEBUG_BUILD
PROC PROCESS_DEBUG_DISPLAY()

	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_HOME)
		debug_bDisplayDebug = !debug_bDisplayDebug
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(debug_bDisplayDebug)
	ENDIF
	
	IF debug_bDisplayDebug
	
		CONST_FLOAT F_LEFT		0.05
		CONST_FLOAT F_TOP		0.1
		CONST_FLOAT F_INDENT	0.02
		CONST_FLOAT F_SPACING	0.02
		
		CONST_INT	I_INDENT	5
		CONST_INT	I_SPACING	10
		
		TEXT_LABEL_63 strDebug
		INT iDebugCountSceen, iDebugCountReuse
		
		strDebug = "biS_SpecialCrateDrop: "
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_SpecialCrateDrop)
			strDebug += "FALSE"
		ELSE
			strDebug += "TRUE"
		ENDIF
		DRAW_DEBUG_TEXT_2D(strDebug, <<F_LEFT, F_TOP + (F_SPACING*iDebugCountSceen), 0.0>>, 255,0,0,255)
		iDebugCountSceen++
		iDebugCountSceen++
		
		strDebug = "CRATE INFO"
		DRAW_DEBUG_TEXT_2D(strDebug, <<F_LEFT, F_TOP + (F_SPACING*iDebugCountSceen), 0.0>>, 255,0,0,255)
		iDebugCountSceen++
		
		strDebug = "Location iIdentifier: "
		strDebug += serverBD.CrateDropData.LocationData[0].iIdentifier
		DRAW_DEBUG_TEXT_2D(strDebug, <<F_LEFT+F_INDENT, F_TOP + (F_SPACING*iDebugCountSceen), 0.0>>, 255,255,55,255)
		iDebugCountSceen++
		
		strDebug = "eContentsType: "
		SWITCH serverBD.CrateDropData.CrateData[0].eContentsType
			CASE CD_CONTENTS_CASH			strDebug += "CD_CONTENTS_CASH" 				BREAK
			CASE CD_CONTENTS_WEAPON			strDebug += "CD_CONTENTS_WEAPON" 			BREAK
			CASE CD_CONTENTS_DLC			strDebug += "CD_CONTENTS_DLC" 				BREAK
		ENDSWITCH
		DRAW_DEBUG_TEXT_2D(strDebug, <<F_LEFT+F_INDENT, F_TOP + (F_SPACING*iDebugCountSceen), 0.0>>, 255,255,55,255)
		iDebugCountSceen++
		
		strDebug = "iContentsAmount: "
		strDebug += serverBD.CrateDropData.CrateData[0].iContentsAmount
		DRAW_DEBUG_TEXT_2D(strDebug, <<F_LEFT+F_INDENT, F_TOP + (F_SPACING*iDebugCountSceen), 0.0>>, 255,255,55,255)
		iDebugCountSceen++
		
		IF serverBD.CrateDropData.CrateData[0].eContentsType != CD_CONTENTS_CASH
			strDebug = "iContentsWeapon: "
			strDebug += GET_WEAPON_NAME(serverBD.CrateDropData.CrateData[0].iContentsWeapon)
			DRAW_DEBUG_TEXT_2D(strDebug, <<F_LEFT+F_INDENT, F_TOP + (F_SPACING*iDebugCountSceen), 0.0>>, 255,255,55,255)
			iDebugCountSceen++
		ENDIF
		
		IF serverBD.CrateDropData.CrateData[0].eContentsType = CD_CONTENTS_DLC
			strDebug = "iCrateDropSpecialWeaponClips: "
			strDebug += g_sMPTunables.iCrateDropSpecialWeaponClips
			DRAW_DEBUG_TEXT_2D(strDebug, <<F_LEFT+F_INDENT, F_TOP + (F_SPACING*iDebugCountSceen), 0.0>>, 255,255,55,255)
			iDebugCountSceen++
		
			strDebug = "eContentsMaleClothingType: "
			strDebug += GET_PED_COMP_TYPE_STRING(serverBD.CrateDropData.CrateData[0].eContentsMaleClothingType)
			DRAW_DEBUG_TEXT_2D(strDebug, <<F_LEFT+F_INDENT, F_TOP + (F_SPACING*iDebugCountSceen), 0.0>>, 255,255,55,255)
			iDebugCountSceen++
			
			strDebug = "eContentsMaleClothingName: "
			strDebug += ENUM_TO_INT(serverBD.CrateDropData.CrateData[0].eContentsMaleClothingName)
			DRAW_DEBUG_TEXT_2D(strDebug, <<F_LEFT+F_INDENT, F_TOP + (F_SPACING*iDebugCountSceen), 0.0>>, 255,255,55,255)
			iDebugCountSceen++
			
			strDebug = "eContentsFemaleClothingType: "
			strDebug += GET_PED_COMP_TYPE_STRING(serverBD.CrateDropData.CrateData[0].eContentsFemaleClothingType)
			DRAW_DEBUG_TEXT_2D(strDebug, <<F_LEFT+F_INDENT, F_TOP + (F_SPACING*iDebugCountSceen), 0.0>>, 255,255,55,255)
			iDebugCountSceen++
			
			strDebug = "eContentsFemaleClothingName: "
			strDebug += ENUM_TO_INT(serverBD.CrateDropData.CrateData[0].eContentsFemaleClothingName)
			DRAW_DEBUG_TEXT_2D(strDebug, <<F_LEFT+F_INDENT, F_TOP + (F_SPACING*iDebugCountSceen), 0.0>>, 255,255,55,255)
			iDebugCountSceen++
		ENDIF
		
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.DCrateData[0].NetID)
		
			VECTOR vCoord = GET_ENTITY_COORDS(NET_TO_OBJ(serverBD.DCrateData[0].NetID))
			
			strDebug = "DESTRUCTIBLE CRATE"
			DRAW_DEBUG_TEXT_WITH_OFFSET(strDebug, vCoord, 0, iDebugCountReuse*I_SPACING, 255,0,0,255)
			iDebugCountReuse++
			
			strDebug = "Health: "
			strDebug += GET_ENTITY_HEALTH(NET_TO_OBJ(serverBD.DCrateData[0].NetID))
			DRAW_DEBUG_TEXT_WITH_OFFSET(strDebug, vCoord, I_INDENT, iDebugCountReuse*I_SPACING, 255,255,55,255)
			iDebugCountReuse++
			
			strDebug = "Frag health: "
			strDebug += ROUND( GET_OBJECT_FRAGMENT_DAMAGE_HEALTH(NET_TO_OBJ(serverBD.DCrateData[0].NetID), FALSE) * 100 )
			DRAW_DEBUG_TEXT_WITH_OFFSET(strDebug, vCoord, I_INDENT, iDebugCountReuse*I_SPACING, 255,255,55,255)
			iDebugCountReuse++
			
			strDebug = "Is broken: "
			IF HAS_OBJECT_BEEN_BROKEN(NET_TO_OBJ(serverBD.DCrateData[0].NetID))
				strDebug += "TRUE"
			ELSE
				strDebug += "FALSE"
			ENDIF
			DRAW_DEBUG_TEXT_WITH_OFFSET(strDebug, vCoord, I_INDENT, iDebugCountReuse*I_SPACING, 255,255,55,255)
			iDebugCountReuse++
			
			strDebug = "Is in air: "
			IF IS_ENTITY_IN_AIR(NET_TO_OBJ(serverBD.DCrateData[0].NetID))
				strDebug += "TRUE"
			ELSE
				strDebug += "FALSE"
			ENDIF
			DRAW_DEBUG_TEXT_WITH_OFFSET(strDebug, vCoord, I_INDENT, iDebugCountReuse*I_SPACING, 255,255,55,255)
			iDebugCountReuse++
			
			strDebug = "Fragging: "
			IF IS_BIT_SET(serverBD.iServerBitSet, biS_CrateBreakableTriggered)
				IF IS_BIT_SET(serverBD.iServerBitSet, biS_CrateBreakableDone)
					strDebug += "RE-ENABLED"
				ELSE
					strDebug += "DISABLED"
				ENDIF
			ELSE
				strDebug += "ENABLED"
			ENDIF
			DRAW_DEBUG_TEXT_WITH_OFFSET(strDebug, vCoord, I_INDENT, iDebugCountReuse*I_SPACING, 255,255,55,255)
			iDebugCountReuse++
			
		ENDIF

	ENDIF

ENDPROC
#ENDIF

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                      Main Loop                      //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
SCRIPT(MP_MISSION_DATA missionScriptArgs)
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
	AND IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE)
		#IF IS_DEBUG_BUILD	
			NET_PRINT_TIME() NET_PRINT("MP: Starting mission ") NET_NL()
		#ENDIF
		
		// Carry out all the initial game starting duties. 
		PROCESS_PRE_GAME(missionScriptArgs)	
		
		// Setup widgets. Make sure this is done after PROCESS_PRE_GAME()
		#IF IS_DEBUG_BUILD
			CREATE_WIDGETS()
		#ENDIF
	ELSE
		#IF IS_DEBUG_BUILD NET_LOG("CRATE DROP - NETWORK_IS_GAME_IN_PROGRESS = FALSE")	#ENDIF
		NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - MISSION END - NETWORK_IS_GAME_IN_PROGRESS = FALSE - SCRIPT CLEANUP P     <----------     ") NET_NL()
		SCRIPT_CLEANUP()
	ENDIF
	
	// Main loop
	WHILE TRUE

		// One wait to rule them all. This can be the ONLY wait from here on in.....
		MP_LOOP_WAIT_ZERO() 
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		SCRIPT_PROFILER_START_OF_FRAME()
		#ENDIF
		#ENDIF
			
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("very start of frame")
		#ENDIF
		#ENDIF
				
		// If we have a match end event, bail
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		OR SHOULD_MP_AMBIENT_SCRIPT_TERMINATE(MPAM_TYPE_CRATEDROP)
			#IF IS_DEBUG_BUILD NET_LOG("CRATE DROP - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE")	#ENDIF
			NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - MISSION END - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE - SCRIPT CLEANUP B     <----------     ") NET_NL()
			SCRIPT_CLEANUP()
		ENDIF
		
		IF NETWORK_IS_IN_TUTORIAL_SESSION()
			#IF IS_DEBUG_BUILD NET_LOG("CRATE DROP - NETWORK_IS_IN_TUTORIAL_SESSION")	#ENDIF
			NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - MISSION END - NETWORK_IS_IN_TUTORIAL_SESSION - SCRIPT CLEANUP C     <----------     ") NET_NL()
			SCRIPT_CLEANUP()
		ENDIF
		
		IF NETWORK_IS_ACTIVITY_SESSION()
			#IF IS_DEBUG_BUILD NET_LOG("CRATE DROP - NETWORK_IS_ACTIVITY_SESSION")	#ENDIF
			NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - MISSION END - NETWORK_IS_ACTIVITY_SESSION - SCRIPT CLEANUP Z     <----------     ") NET_NL()
			SCRIPT_CLEANUP()
		ENDIF
		
		IF IS_THIS_PLAYER_ACTIVE_IN_CORONA_PAST_TRANSITION_SESSION(PLAYER_ID())
			#IF IS_DEBUG_BUILD NET_LOG("CRATE DROP - IS_THIS_PLAYER_ACTIVE_IN_CORONA_PAST_TRANSITION_SESSION")	#ENDIF
			NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - MISSION END - IS_THIS_PLAYER_ACTIVE_IN_CORONA_PAST_TRANSITION_SESSION - SCRIPT CLEANUP D     <----------     ") NET_NL()
			SCRIPT_CLEANUP()
		ENDIF
		
		IF IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID())
			#IF IS_DEBUG_BUILD NET_LOG("CRATE DROP - IS_PLAYER_ON_A_PLAYLIST")	#ENDIF
			NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - MISSION END - IS_PLAYER_ON_A_PLAYLIST - SCRIPT CLEANUP E     <----------     ") NET_NL()
			SCRIPT_CLEANUP()
		ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("cleanup checks")
		#ENDIF
		#ENDIF
		
		// Deal with the debug
		#IF IS_DEBUG_BUILD		
			UPDATE_WIDGETS()
		#ENDIF
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("UPDATE_WIDGETS")
		#ENDIF
		#ENDIF	
		
		UPDATE_F9_DATA()
		
		// If we need to leave the mission, make sure we just straight to the end stage and terminate the script - DON'T NEED FOR AMBIENT MISSION
		/*IF SHOULD_PLAYER_LEAVE_MP_MISSION() 
			#IF IS_DEBUG_BUILD
			NET_PRINT_TIME() NET_PRINT( GET_MP_MISSION_NAME(thisMission), " Leave mission = TRUE \n")
			#ENDIF
			playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_LEAVE
			#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_LEAVE")	#ENDIF
		ENDIF*/
		
		// -----------------------------------
		// Process your game logic.....
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
		OPEN_SCRIPT_PROFILE_MARKER_GROUP("GET_CLIENT_MISSION_STATE")
		#ENDIF
		#ENDIF
		SWITCH GET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT())
			
			// Wait untill the server gives the all go before moving on
			CASE GAME_STATE_INI
				
				playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_RUNNING
				NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_RUNNING    <----------     ") NET_NL()
				#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_RUNNING")	#ENDIF

				
				// Look for the server say the mission has ended
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY
					NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY 1    <----------     ") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_TERMINATE_DELAY")	#ENDIF
				ENDIF
			BREAK
			
			// Main gameplay state.
			CASE GAME_STATE_RUNNING
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					
					PROCESS_CRATE_DROP_CLIENT()
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("PROCESS_CRATE_DROP_CLIENT")
					#ENDIF
					#ENDIF
					
					/*CONTROL_PILOT_FLIGHT_TASK()
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("CONTROL_PILOT_FLIGHT_TASK")
					#ENDIF
					#ENDIF*/
										
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY
					NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY 2   <----------     ") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_TERMINATE_DELAY")	#ENDIF
				ENDIF
			BREAK
			
			CASE GAME_STATE_TERMINATE_DELAY 
				SET_MISSION_FINISHED(serverBD.MissionTerminateDelayTimer)
		      	IF IS_MISSION_READY_TO_CLEANUP(serverBD.MissionTerminateDelayTimer)
		            playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
					NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END 3    <----------     ") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_END 3")	#ENDIF
		      	ENDIF
			BREAK
			
			// The game stage the local player is placed when we want him to leave a mission
			CASE GAME_STATE_LEAVE
				//LEAVE_MY_MP_MISSION(thisMission)
				playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
				NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END 2    <----------     ") NET_NL()
				#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_END 2")	#ENDIF
			FALLTHRU
			
			//Cleans up then terminates the mission
			CASE GAME_STATE_END
				NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - SCRIPT CLEANUP A     <----------     ") NET_NL()
				SCRIPT_CLEANUP()
			BREAK

		ENDSWITCH
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
		CLOSE_SCRIPT_PROFILE_MARKER_GROUP()
		#ENDIF
		#ENDIF
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("Process logic")
		#ENDIF
		#ENDIF
		
		
		// -----------------------------------
		// Process server game logic	
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
		OPEN_SCRIPT_PROFILE_MARKER_GROUP("GET_SERVER_MISSION_STATE")
		#ENDIF
		#ENDIF
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			
			SWITCH GET_SERVER_MISSION_STATE()
				
				// For CnC group missions, we may want to wait for all players to have launched the script
				CASE GAME_STATE_INI
					IF GET_CRATE_DROP_DATA()
						serverBD.iServerGameState = GAME_STATE_RUNNING
						NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - serverBD.iServerGameState = GAME_STATE_RUNNING    <----------     ") NET_NL()
						#IF IS_DEBUG_BUILD NET_LOG_Host("iServerGameState = GAME_STATE_RUNNING")	#ENDIF
					ENDIF
				BREAK
				
				// Look for game end conditions
				CASE GAME_STATE_RUNNING
					
					PROCESS_CRATE_DROP_SERVER()
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("PROCESS_CRATE_DROP_SERVER")
					#ENDIF
					#ENDIF
					
					MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER()
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER")
					#ENDIF
					#ENDIF
					
					//CONTROL_PLANE_BODY()
					//CONTROL_AI_BODY()
					
					#IF IS_DEBUG_BUILD
						IF bHostEndMissionNow
						OR MPGlobalsAmbience.bKillActiveEvent
						MPGlobalsAmbience.bKillActiveEvent = FALSE
							serverBD.iServerGameState = GAME_STATE_END
							NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - serverBD.iServerGameState = GAME_STATE_END 1    <----------     ") NET_NL()
							#IF IS_DEBUG_BUILD NET_LOG_Host("iServerGameState = GAME_STATE_END 1")	#ENDIF
						ENDIF
					#ENDIF	
					
					IF HAVE_MISSION_END_CONDITIONS_BEEN_MET()
						serverBD.iServerGameState = GAME_STATE_END
						NET_PRINT_TIME() NET_PRINT("     ---------->     CRATE DROP - serverBD.iServerGameState = GAME_STATE_END 4    <----------     ") NET_NL()
						#IF IS_DEBUG_BUILD NET_LOG_Host("iServerGameState = GAME_STATE_END 4")	#ENDIF
					ENDIF
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("HAVE_MISSION_END_CONDITIONS_BEEN_MET")
					#ENDIF
					#ENDIF
					
				BREAK
				
				CASE GAME_STATE_END
				BREAK
				
			ENDSWITCH
			
		ENDIF
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
		CLOSE_SCRIPT_PROFILE_MARKER_GROUP()
		#ENDIF
		#ENDIF		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("server processing")
		#ENDIF
		#ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		SCRIPT_PROFILER_END_OF_FRAME()
		#ENDIF
		#ENDIF
		
		#IF IS_DEBUG_BUILD
			PROCESS_DEBUG_DISPLAY()
		#ENDIF
		
	ENDWHILE
	
ENDSCRIPT
