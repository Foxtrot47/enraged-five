//////////////////////////////////////////////////////////////////////////////////////////
// Name:        Generic.sc																//
// Description: Template multiplayer mission with main client and server loops			//
// Written by:  Martin McMillan															//
// Date: 09/03/2015																		//
//////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"



//----------------------
//	INCLUDES
//----------------------
USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_camera.sch"
USING "commands_pad.sch"
USING "commands_script.sch"
USING "flow_public_core_override.sch"
USING "net_wait_zero.sch"
USING "net_mission.sch"
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_event.sch"
USING "net_include.sch"
USING "net_events.sch"
USING "net_blips.sch"

//----------------------
//	GAME STATE
//----------------------

CONST_INT GAME_STATE_INIT 									0
CONST_INT GAME_STATE_APPLY									1
CONST_INT GAME_STATE_RUNNING								2
CONST_INT GAME_STATE_TERMINATE_DELAY						3
CONST_INT GAME_STATE_END									4

INT iGameState

FUNC STRING GET_MISSION_STATE_STRING(INT iState)

	SWITCH(iState)
		CASE GAME_STATE_INIT 				RETURN "GAME_STATE_INIT"
		CASE GAME_STATE_APPLY				RETURN "GAME_STATE_APPLY"
		CASE GAME_STATE_RUNNING 			RETURN "GAME_STATE_RUNNING"
		CASE GAME_STATE_TERMINATE_DELAY 	RETURN "GAME_STATE_TERMINATE_DELAY"
		CASE GAME_STATE_END 				RETURN "GAME_STATE_END"
	ENDSWITCH

	RETURN "UNKNOWN STATE!"

ENDFUNC



// -----------------------------------------------------------------------------------------------------------
//		Mission Cleanup
// -----------------------------------------------------------------------------------------------------------

PROC DO_CLEAR_BLIPS(PLAYER_INDEX targetRival)
	IF targetRival != INVALID_PLAYER_INDEX() 
		CLEAR_CUSTOM_BLIP_SETTINGS_FOR_PLAYER(targetRival)
	ENDIF
	
	MPGlobalsAmbience.sMagnateGangBossData.TargetRivalPlayer = INVALID_PLAYER_INDEX()
ENDPROC

PROC SCRIPT_CLEANUP()

	CPRINTLN(DEBUG_NET_AMBIENT, "=== [BIKER] [TARGET_RIVAL] === SCRIPT_CLEANUP")
		
	DO_CLEAR_BLIPS(MPGlobalsAmbience.sMagnateGangBossData.TargetRivalPlayer)	
		
	TERMINATE_THIS_THREAD()
	
ENDPROC

FUNC BOOL PROCESS_PRE_GAME()

	CPRINTLN(DEBUG_NET_AMBIENT, "=== [BIKER] [TARGET_RIVAL] === PROCESS_PRE_GAME")
	
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL CREATE_TARGET_BLIP(PLAYER_INDEX playerId)
	IF IS_NET_PLAYER_OK(playerId, FALSE)
		SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(playerId, RADAR_TRACE_TEMP_4, TRUE)
		IF DOES_THIS_THREAD_HAVE_CONTROL_OF_CUSTOM_BLIP_COLOUR_FOR_PLAYER(playerId)
			
			SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(playerId, GET_BLIP_COLOUR_FROM_HUD_COLOUR(GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID())), TRUE)
		ELSE
		
		ENDIF
		IF DOES_THIS_THREAD_HAVE_CONTROL_OF_FORCE_BLIP_PLAYER(playerId)
			FORCE_BLIP_PLAYER(playerId, TRUE, TRUE)
		ENDIF
		DISABLE_SECONDARY_OUTLINES_FOR_PLAYER_BLIP(playerId, TRUE)
		FLASH_BLIP_FOR_PLAYER(playerId, TRUE, 5000)

		
		PRINTLN("[BIKER] [TARGET_RIVAL] - GB_BIKER_BLIP_PLAYER_AS_TARGET_RIVAL - Set custom blip for ", GET_PLAYER_NAME(playerId))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC



FUNC BOOL GB_BIKER_SHOULD_CLEAR_TARGET_RIVAL(PLAYER_INDEX playerId)
/*
	#IF IS_DEBUG_BUILD
	IF MPGlobalsAmbience.sMagnateGangBossData.bSetPlayerAsTargetRival
		PRINTLN("[BIKER] [TARGET_RIVAL] - GB_BIKER_SHOULD_CLEAR_TARGET_RIVAL - RETURN TRUE - MPGlobalsAmbience.sMagnateGangBossData.bSetPlayerAsTargetRival = FALSE")
		RETURN TRUE
	ENDIF
	#ENDIF
	*/
	IF NOT IS_NET_PLAYER_OK(playerId, FALSE)
		PRINTLN("[BIKER] [TARGET_RIVAL] - GB_BIKER_SHOULD_CLEAR_TARGET_RIVAL - RETURN TRUE - IS_NET_PLAYER_OK(playerId, FALSE) = FALSE")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL GB_BIKER_APPLY_TARGET_RIVAL()
	IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_BIKER_GANG(TRUE)
		PLAYER_INDEX targetRival = MPGlobalsAmbience.sMagnateGangBossData.TargetRivalPlayer
		
		IF targetRival != INVALID_PLAYER_INDEX()
			IF CREATE_TARGET_BLIP(targetRival)

				PRINTLN("[BIKER_TARGET_RIVAL] - GB_BIKER_MAINTAIN_TARGET_RIVAL_LOGIC - ALL MEMBERS - targetRival = ", NATIVE_TO_INT(targetRival))
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL GB_BIKER_MAINTAIN_TARGET_RIVAL_LOGIC()
	IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_BIKER_GANG(TRUE)
		PLAYER_INDEX targetRival = MPGlobalsAmbience.sMagnateGangBossData.TargetRivalPlayer
		
		IF targetRival != INVALID_PLAYER_INDEX()

			IF GB_BIKER_SHOULD_CLEAR_TARGET_RIVAL(targetRival)
				DO_CLEAR_BLIPS(targetRival)
				PRINTLN("[BIKER_TARGET_RIVAL] - GB_BIKER_MAINTAIN_TARGET_RIVAL_LOGIC - GB_BIKER_SHOULD_CLEAR_TARGET_RIVAL = TRUE ")
				RETURN FALSE
			ENDIF
	
		ELSE

			DO_CLEAR_BLIPS(targetRival)
			PRINTLN("[BIKER_TARGET_RIVAL] - GB_BIKER_MAINTAIN_TARGET_RIVAL_LOGIC - ALL MEMBERS - targetRival = -1")
			RETURN FALSE
		ENDIF
	ELSE
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

//----------------------
//	MAIN SCRIPT
//----------------------

SCRIPT

	PRINTLN("[BIKER_TARGET_RIVAL] - SCRIPT START")
	
	PROCESS_PRE_GAME()
	
	WHILE (TRUE)
	
		MP_LOOP_WAIT_ZERO()
		
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			PRINTLN("[BIKER_TARGET_RIVAL] - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE.")
			SCRIPT_CLEANUP()
		ENDIF
		
		IF MPGlobalsAmbience.sMagnateGangBossData.bActivateTargetRival = FALSE
			PRINTLN("[BIKER_TARGET_RIVAL] - Cleanup from External call - bActivateTargetRival = FALSE ")
			SCRIPT_CLEANUP()
		ENDIF
		
		SWITCH(iGameState)
		
			CASE GAME_STATE_INIT
				iGameState = GAME_STATE_APPLY
				PRINTLN("[BIKER_TARGET_RIVAL] - Moving to GAME_STATE_APPLY.")
			BREAK
			
			CASE GAME_STATE_APPLY
				IF GB_BIKER_APPLY_TARGET_RIVAL()
					iGameState = GAME_STATE_RUNNING
					PRINTLN("[BIKER_TARGET_RIVAL] - Moving to GAME_STATE_RUNNING.")
				ENDIF
			BREAK
			
			CASE GAME_STATE_RUNNING
				IF NOT GB_BIKER_MAINTAIN_TARGET_RIVAL_LOGIC()
					iGameState = GAME_STATE_END
					PRINTLN("[BIKER_TARGET_RIVAL] - Moving to GAME_STATE_END.")
				ENDIF
			BREAK
			
			CASE GAME_STATE_END
				SCRIPT_CLEANUP()
			BREAK
			
		ENDSWITCH
	
	ENDWHILE

// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT
